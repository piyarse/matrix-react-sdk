"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.recommendationToStable = recommendationToStable;
exports.ListRule = exports.RECOMMENDATION_BAN_TYPES = exports.RECOMMENDATION_BAN = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _MatrixGlob = require("../utils/MatrixGlob");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Inspiration largely taken from Mjolnir itself
const RECOMMENDATION_BAN = "m.ban";
exports.RECOMMENDATION_BAN = RECOMMENDATION_BAN;
const RECOMMENDATION_BAN_TYPES = [RECOMMENDATION_BAN, "org.matrix.mjolnir.ban"];
exports.RECOMMENDATION_BAN_TYPES = RECOMMENDATION_BAN_TYPES;

function recommendationToStable(recommendation
/*: string*/
, unstable = true)
/*: string*/
{
  if (RECOMMENDATION_BAN_TYPES.includes(recommendation)) {
    return unstable ? RECOMMENDATION_BAN_TYPES[RECOMMENDATION_BAN_TYPES.length - 1] : RECOMMENDATION_BAN;
  }

  return null;
}

class ListRule {
  constructor(entity
  /*: string*/
  , action
  /*: string*/
  , reason
  /*: string*/
  , kind
  /*: string*/
  ) {
    (0, _defineProperty2.default)(this, "_glob", void 0);
    (0, _defineProperty2.default)(this, "_entity", void 0);
    (0, _defineProperty2.default)(this, "_action", void 0);
    (0, _defineProperty2.default)(this, "_reason", void 0);
    (0, _defineProperty2.default)(this, "_kind", void 0);
    this._glob = new _MatrixGlob.MatrixGlob(entity);
    this._entity = entity;
    this._action = recommendationToStable(action, false);
    this._reason = reason;
    this._kind = kind;
  }

  get entity()
  /*: string*/
  {
    return this._entity;
  }

  get reason()
  /*: string*/
  {
    return this._reason;
  }

  get kind()
  /*: string*/
  {
    return this._kind;
  }

  get recommendation()
  /*: string*/
  {
    return this._action;
  }

  isMatch(entity
  /*: string*/
  )
  /*: boolean*/
  {
    return this._glob.test(entity);
  }

}

exports.ListRule = ListRule;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tam9sbmlyL0xpc3RSdWxlLmpzIl0sIm5hbWVzIjpbIlJFQ09NTUVOREFUSU9OX0JBTiIsIlJFQ09NTUVOREFUSU9OX0JBTl9UWVBFUyIsInJlY29tbWVuZGF0aW9uVG9TdGFibGUiLCJyZWNvbW1lbmRhdGlvbiIsInVuc3RhYmxlIiwiaW5jbHVkZXMiLCJsZW5ndGgiLCJMaXN0UnVsZSIsImNvbnN0cnVjdG9yIiwiZW50aXR5IiwiYWN0aW9uIiwicmVhc29uIiwia2luZCIsIl9nbG9iIiwiTWF0cml4R2xvYiIsIl9lbnRpdHkiLCJfYWN0aW9uIiwiX3JlYXNvbiIsIl9raW5kIiwiaXNNYXRjaCIsInRlc3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFoQkE7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUVPLE1BQU1BLGtCQUFrQixHQUFHLE9BQTNCOztBQUNBLE1BQU1DLHdCQUF3QixHQUFHLENBQUNELGtCQUFELEVBQXFCLHdCQUFyQixDQUFqQzs7O0FBRUEsU0FBU0Usc0JBQVQsQ0FBZ0NDO0FBQWhDO0FBQUEsRUFBd0RDLFFBQVEsR0FBRyxJQUFuRTtBQUFBO0FBQWlGO0FBQ3BGLE1BQUlILHdCQUF3QixDQUFDSSxRQUF6QixDQUFrQ0YsY0FBbEMsQ0FBSixFQUF1RDtBQUNuRCxXQUFPQyxRQUFRLEdBQUdILHdCQUF3QixDQUFDQSx3QkFBd0IsQ0FBQ0ssTUFBekIsR0FBa0MsQ0FBbkMsQ0FBM0IsR0FBbUVOLGtCQUFsRjtBQUNIOztBQUNELFNBQU8sSUFBUDtBQUNIOztBQUVNLE1BQU1PLFFBQU4sQ0FBZTtBQU9sQkMsRUFBQUEsV0FBVyxDQUFDQztBQUFEO0FBQUEsSUFBaUJDO0FBQWpCO0FBQUEsSUFBaUNDO0FBQWpDO0FBQUEsSUFBaURDO0FBQWpEO0FBQUEsSUFBK0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ3RFLFNBQUtDLEtBQUwsR0FBYSxJQUFJQyxzQkFBSixDQUFlTCxNQUFmLENBQWI7QUFDQSxTQUFLTSxPQUFMLEdBQWVOLE1BQWY7QUFDQSxTQUFLTyxPQUFMLEdBQWVkLHNCQUFzQixDQUFDUSxNQUFELEVBQVMsS0FBVCxDQUFyQztBQUNBLFNBQUtPLE9BQUwsR0FBZU4sTUFBZjtBQUNBLFNBQUtPLEtBQUwsR0FBYU4sSUFBYjtBQUNIOztBQUVELE1BQUlILE1BQUo7QUFBQTtBQUFxQjtBQUNqQixXQUFPLEtBQUtNLE9BQVo7QUFDSDs7QUFFRCxNQUFJSixNQUFKO0FBQUE7QUFBcUI7QUFDakIsV0FBTyxLQUFLTSxPQUFaO0FBQ0g7O0FBRUQsTUFBSUwsSUFBSjtBQUFBO0FBQW1CO0FBQ2YsV0FBTyxLQUFLTSxLQUFaO0FBQ0g7O0FBRUQsTUFBSWYsY0FBSjtBQUFBO0FBQTZCO0FBQ3pCLFdBQU8sS0FBS2EsT0FBWjtBQUNIOztBQUVERyxFQUFBQSxPQUFPLENBQUNWO0FBQUQ7QUFBQTtBQUFBO0FBQTBCO0FBQzdCLFdBQU8sS0FBS0ksS0FBTCxDQUFXTyxJQUFYLENBQWdCWCxNQUFoQixDQUFQO0FBQ0g7O0FBakNpQiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IHtNYXRyaXhHbG9ifSBmcm9tIFwiLi4vdXRpbHMvTWF0cml4R2xvYlwiO1xyXG5cclxuLy8gSW5zcGlyYXRpb24gbGFyZ2VseSB0YWtlbiBmcm9tIE1qb2xuaXIgaXRzZWxmXHJcblxyXG5leHBvcnQgY29uc3QgUkVDT01NRU5EQVRJT05fQkFOID0gXCJtLmJhblwiO1xyXG5leHBvcnQgY29uc3QgUkVDT01NRU5EQVRJT05fQkFOX1RZUEVTID0gW1JFQ09NTUVOREFUSU9OX0JBTiwgXCJvcmcubWF0cml4Lm1qb2xuaXIuYmFuXCJdO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHJlY29tbWVuZGF0aW9uVG9TdGFibGUocmVjb21tZW5kYXRpb246IHN0cmluZywgdW5zdGFibGUgPSB0cnVlKTogc3RyaW5nIHtcclxuICAgIGlmIChSRUNPTU1FTkRBVElPTl9CQU5fVFlQRVMuaW5jbHVkZXMocmVjb21tZW5kYXRpb24pKSB7XHJcbiAgICAgICAgcmV0dXJuIHVuc3RhYmxlID8gUkVDT01NRU5EQVRJT05fQkFOX1RZUEVTW1JFQ09NTUVOREFUSU9OX0JBTl9UWVBFUy5sZW5ndGggLSAxXSA6IFJFQ09NTUVOREFUSU9OX0JBTjtcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTGlzdFJ1bGUge1xyXG4gICAgX2dsb2I6IE1hdHJpeEdsb2I7XHJcbiAgICBfZW50aXR5OiBzdHJpbmc7XHJcbiAgICBfYWN0aW9uOiBzdHJpbmc7XHJcbiAgICBfcmVhc29uOiBzdHJpbmc7XHJcbiAgICBfa2luZDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGVudGl0eTogc3RyaW5nLCBhY3Rpb246IHN0cmluZywgcmVhc29uOiBzdHJpbmcsIGtpbmQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2dsb2IgPSBuZXcgTWF0cml4R2xvYihlbnRpdHkpO1xyXG4gICAgICAgIHRoaXMuX2VudGl0eSA9IGVudGl0eTtcclxuICAgICAgICB0aGlzLl9hY3Rpb24gPSByZWNvbW1lbmRhdGlvblRvU3RhYmxlKGFjdGlvbiwgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMuX3JlYXNvbiA9IHJlYXNvbjtcclxuICAgICAgICB0aGlzLl9raW5kID0ga2luZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZW50aXR5KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VudGl0eTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcmVhc29uKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlYXNvbjtcclxuICAgIH1cclxuXHJcbiAgICBnZXQga2luZCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9raW5kO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCByZWNvbW1lbmRhdGlvbigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hY3Rpb247XHJcbiAgICB9XHJcblxyXG4gICAgaXNNYXRjaChlbnRpdHk6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nbG9iLnRlc3QoZW50aXR5KTtcclxuICAgIH1cclxufVxyXG4iXX0=