"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _commonmark = _interopRequireDefault(require("commonmark"));

var _escape = _interopRequireDefault(require("lodash/escape"));

/*
Copyright 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const ALLOWED_HTML_TAGS = ['sub', 'sup', 'del', 'u']; // These types of node are definitely text

const TEXT_NODES = ['text', 'softbreak', 'linebreak', 'paragraph', 'document'];

function is_allowed_html_tag(node) {
  // Regex won't work for tags with attrs, but we only
  // allow <del> anyway.
  const matches = /^<\/?(.*)>$/.exec(node.literal);

  if (matches && matches.length == 2) {
    const tag = matches[1];
    return ALLOWED_HTML_TAGS.indexOf(tag) > -1;
  }

  return false;
}

function html_if_tag_allowed(node) {
  if (is_allowed_html_tag(node)) {
    this.lit(node.literal);
    return;
  } else {
    this.lit((0, _escape.default)(node.literal));
  }
}
/*
 * Returns true if the parse output containing the node
 * comprises multiple block level elements (ie. lines),
 * or false if it is only a single line.
 */


function is_multi_line(node) {
  let par = node;

  while (par.parent) {
    par = par.parent;
  }

  return par.firstChild != par.lastChild;
}
/**
 * Class that wraps commonmark, adding the ability to see whether
 * a given message actually uses any markdown syntax or whether
 * it's plain text.
 */


class Markdown {
  constructor(input) {
    this.input = input;
    const parser = new _commonmark.default.Parser();
    this.parsed = parser.parse(this.input);
  }

  isPlainText() {
    const walker = this.parsed.walker();
    let ev;

    while (ev = walker.next()) {
      const node = ev.node;

      if (TEXT_NODES.indexOf(node.type) > -1) {
        // definitely text
        continue;
      } else if (node.type == 'html_inline' || node.type == 'html_block') {
        // if it's an allowed html tag, we need to render it and therefore
        // we will need to use HTML. If it's not allowed, it's not HTML since
        // we'll just be treating it as text.
        if (is_allowed_html_tag(node)) {
          return false;
        }
      } else {
        return false;
      }
    }

    return true;
  }

  toHTML({
    externalLinks = false
  } = {}) {
    const renderer = new _commonmark.default.HtmlRenderer({
      safe: false,
      // Set soft breaks to hard HTML breaks: commonmark
      // puts softbreaks in for multiple lines in a blockquote,
      // so if these are just newline characters then the
      // block quote ends up all on one line
      // (https://github.com/vector-im/riot-web/issues/3154)
      softbreak: '<br />'
    }); // Trying to strip out the wrapping <p/> causes a lot more complication
    // than it's worth, i think.  For instance, this code will go and strip
    // out any <p/> tag (no matter where it is in the tree) which doesn't
    // contain \n's.
    // On the flip side, <p/>s are quite opionated and restricted on where
    // you can nest them.
    //
    // Let's try sending with <p/>s anyway for now, though.

    const real_paragraph = renderer.paragraph;

    renderer.paragraph = function (node, entering) {
      // If there is only one top level node, just return the
      // bare text: it's a single line of text and so should be
      // 'inline', rather than unnecessarily wrapped in its own
      // p tag. If, however, we have multiple nodes, each gets
      // its own p tag to keep them as separate paragraphs.
      if (is_multi_line(node)) {
        real_paragraph.call(this, node, entering);
      }
    };

    renderer.link = function (node, entering) {
      const attrs = this.attrs(node);

      if (entering) {
        attrs.push(['href', this.esc(node.destination)]);

        if (node.title) {
          attrs.push(['title', this.esc(node.title)]);
        } // Modified link behaviour to treat them all as external and
        // thus opening in a new tab.


        if (externalLinks) {
          attrs.push(['target', '_blank']);
          attrs.push(['rel', 'noreferrer noopener']);
        }

        this.tag('a', attrs);
      } else {
        this.tag('/a');
      }
    };

    renderer.html_inline = html_if_tag_allowed;

    renderer.html_block = function (node) {
      /*
                  // as with `paragraph`, we only insert line breaks
                  // if there are multiple lines in the markdown.
                  const isMultiLine = is_multi_line(node);
                  if (isMultiLine) this.cr();
      */
      html_if_tag_allowed.call(this, node);
      /*
                  if (isMultiLine) this.cr();
      */
    };

    return renderer.render(this.parsed);
  }
  /*
   * Render the markdown message to plain text. That is, essentially
   * just remove any backslashes escaping what would otherwise be
   * markdown syntax
   * (to fix https://github.com/vector-im/riot-web/issues/2870).
   *
   * N.B. this does **NOT** render arbitrary MD to plain text - only MD
   * which has no formatting.  Otherwise it emits HTML(!).
   */


  toPlaintext() {
    const renderer = new _commonmark.default.HtmlRenderer({
      safe: false
    });
    const real_paragraph = renderer.paragraph; // The default `out` function only sends the input through an XML
    // escaping function, which causes messages to be entity encoded,
    // which we don't want in this case.

    renderer.out = function (s) {
      // The `lit` function adds a string literal to the output buffer.
      this.lit(s);
    };

    renderer.paragraph = function (node, entering) {
      // as with toHTML, only append lines to paragraphs if there are
      // multiple paragraphs
      if (is_multi_line(node)) {
        if (!entering && node.next) {
          this.lit('\n\n');
        }
      }
    };

    renderer.html_block = function (node) {
      this.lit(node.literal);
      if (is_multi_line(node) && node.next) this.lit('\n\n');
    };

    return renderer.render(this.parsed);
  }

}

exports.default = Markdown;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9NYXJrZG93bi5qcyJdLCJuYW1lcyI6WyJBTExPV0VEX0hUTUxfVEFHUyIsIlRFWFRfTk9ERVMiLCJpc19hbGxvd2VkX2h0bWxfdGFnIiwibm9kZSIsIm1hdGNoZXMiLCJleGVjIiwibGl0ZXJhbCIsImxlbmd0aCIsInRhZyIsImluZGV4T2YiLCJodG1sX2lmX3RhZ19hbGxvd2VkIiwibGl0IiwiaXNfbXVsdGlfbGluZSIsInBhciIsInBhcmVudCIsImZpcnN0Q2hpbGQiLCJsYXN0Q2hpbGQiLCJNYXJrZG93biIsImNvbnN0cnVjdG9yIiwiaW5wdXQiLCJwYXJzZXIiLCJjb21tb25tYXJrIiwiUGFyc2VyIiwicGFyc2VkIiwicGFyc2UiLCJpc1BsYWluVGV4dCIsIndhbGtlciIsImV2IiwibmV4dCIsInR5cGUiLCJ0b0hUTUwiLCJleHRlcm5hbExpbmtzIiwicmVuZGVyZXIiLCJIdG1sUmVuZGVyZXIiLCJzYWZlIiwic29mdGJyZWFrIiwicmVhbF9wYXJhZ3JhcGgiLCJwYXJhZ3JhcGgiLCJlbnRlcmluZyIsImNhbGwiLCJsaW5rIiwiYXR0cnMiLCJwdXNoIiwiZXNjIiwiZGVzdGluYXRpb24iLCJ0aXRsZSIsImh0bWxfaW5saW5lIiwiaHRtbF9ibG9jayIsInJlbmRlciIsInRvUGxhaW50ZXh0Iiwib3V0IiwicyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQWpCQTs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE1BQU1BLGlCQUFpQixHQUFHLENBQUMsS0FBRCxFQUFRLEtBQVIsRUFBZSxLQUFmLEVBQXNCLEdBQXRCLENBQTFCLEMsQ0FFQTs7QUFDQSxNQUFNQyxVQUFVLEdBQUcsQ0FBQyxNQUFELEVBQVMsV0FBVCxFQUFzQixXQUF0QixFQUFtQyxXQUFuQyxFQUFnRCxVQUFoRCxDQUFuQjs7QUFFQSxTQUFTQyxtQkFBVCxDQUE2QkMsSUFBN0IsRUFBbUM7QUFDL0I7QUFDQTtBQUNBLFFBQU1DLE9BQU8sR0FBRyxjQUFjQyxJQUFkLENBQW1CRixJQUFJLENBQUNHLE9BQXhCLENBQWhCOztBQUNBLE1BQUlGLE9BQU8sSUFBSUEsT0FBTyxDQUFDRyxNQUFSLElBQWtCLENBQWpDLEVBQW9DO0FBQ2hDLFVBQU1DLEdBQUcsR0FBR0osT0FBTyxDQUFDLENBQUQsQ0FBbkI7QUFDQSxXQUFPSixpQkFBaUIsQ0FBQ1MsT0FBbEIsQ0FBMEJELEdBQTFCLElBQWlDLENBQUMsQ0FBekM7QUFDSDs7QUFDRCxTQUFPLEtBQVA7QUFDSDs7QUFFRCxTQUFTRSxtQkFBVCxDQUE2QlAsSUFBN0IsRUFBbUM7QUFDL0IsTUFBSUQsbUJBQW1CLENBQUNDLElBQUQsQ0FBdkIsRUFBK0I7QUFDM0IsU0FBS1EsR0FBTCxDQUFTUixJQUFJLENBQUNHLE9BQWQ7QUFDQTtBQUNILEdBSEQsTUFHTztBQUNILFNBQUtLLEdBQUwsQ0FBUyxxQkFBT1IsSUFBSSxDQUFDRyxPQUFaLENBQVQ7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7QUFLQSxTQUFTTSxhQUFULENBQXVCVCxJQUF2QixFQUE2QjtBQUN6QixNQUFJVSxHQUFHLEdBQUdWLElBQVY7O0FBQ0EsU0FBT1UsR0FBRyxDQUFDQyxNQUFYLEVBQW1CO0FBQ2ZELElBQUFBLEdBQUcsR0FBR0EsR0FBRyxDQUFDQyxNQUFWO0FBQ0g7O0FBQ0QsU0FBT0QsR0FBRyxDQUFDRSxVQUFKLElBQWtCRixHQUFHLENBQUNHLFNBQTdCO0FBQ0g7QUFFRDs7Ozs7OztBQUtlLE1BQU1DLFFBQU4sQ0FBZTtBQUMxQkMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixTQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFFQSxVQUFNQyxNQUFNLEdBQUcsSUFBSUMsb0JBQVdDLE1BQWYsRUFBZjtBQUNBLFNBQUtDLE1BQUwsR0FBY0gsTUFBTSxDQUFDSSxLQUFQLENBQWEsS0FBS0wsS0FBbEIsQ0FBZDtBQUNIOztBQUVETSxFQUFBQSxXQUFXLEdBQUc7QUFDVixVQUFNQyxNQUFNLEdBQUcsS0FBS0gsTUFBTCxDQUFZRyxNQUFaLEVBQWY7QUFFQSxRQUFJQyxFQUFKOztBQUNBLFdBQVNBLEVBQUUsR0FBR0QsTUFBTSxDQUFDRSxJQUFQLEVBQWQsRUFBK0I7QUFDM0IsWUFBTXpCLElBQUksR0FBR3dCLEVBQUUsQ0FBQ3hCLElBQWhCOztBQUNBLFVBQUlGLFVBQVUsQ0FBQ1EsT0FBWCxDQUFtQk4sSUFBSSxDQUFDMEIsSUFBeEIsSUFBZ0MsQ0FBQyxDQUFyQyxFQUF3QztBQUNwQztBQUNBO0FBQ0gsT0FIRCxNQUdPLElBQUkxQixJQUFJLENBQUMwQixJQUFMLElBQWEsYUFBYixJQUE4QjFCLElBQUksQ0FBQzBCLElBQUwsSUFBYSxZQUEvQyxFQUE2RDtBQUNoRTtBQUNBO0FBQ0E7QUFDQSxZQUFJM0IsbUJBQW1CLENBQUNDLElBQUQsQ0FBdkIsRUFBK0I7QUFDM0IsaUJBQU8sS0FBUDtBQUNIO0FBQ0osT0FQTSxNQU9BO0FBQ0gsZUFBTyxLQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLElBQVA7QUFDSDs7QUFFRDJCLEVBQUFBLE1BQU0sQ0FBQztBQUFFQyxJQUFBQSxhQUFhLEdBQUc7QUFBbEIsTUFBNEIsRUFBN0IsRUFBaUM7QUFDbkMsVUFBTUMsUUFBUSxHQUFHLElBQUlYLG9CQUFXWSxZQUFmLENBQTRCO0FBQ3pDQyxNQUFBQSxJQUFJLEVBQUUsS0FEbUM7QUFHekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxNQUFBQSxTQUFTLEVBQUU7QUFSOEIsS0FBNUIsQ0FBakIsQ0FEbUMsQ0FZbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxVQUFNQyxjQUFjLEdBQUdKLFFBQVEsQ0FBQ0ssU0FBaEM7O0FBRUFMLElBQUFBLFFBQVEsQ0FBQ0ssU0FBVCxHQUFxQixVQUFTbEMsSUFBVCxFQUFlbUMsUUFBZixFQUF5QjtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBSTFCLGFBQWEsQ0FBQ1QsSUFBRCxDQUFqQixFQUF5QjtBQUNyQmlDLFFBQUFBLGNBQWMsQ0FBQ0csSUFBZixDQUFvQixJQUFwQixFQUEwQnBDLElBQTFCLEVBQWdDbUMsUUFBaEM7QUFDSDtBQUNKLEtBVEQ7O0FBV0FOLElBQUFBLFFBQVEsQ0FBQ1EsSUFBVCxHQUFnQixVQUFTckMsSUFBVCxFQUFlbUMsUUFBZixFQUF5QjtBQUNyQyxZQUFNRyxLQUFLLEdBQUcsS0FBS0EsS0FBTCxDQUFXdEMsSUFBWCxDQUFkOztBQUNBLFVBQUltQyxRQUFKLEVBQWM7QUFDVkcsUUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVcsQ0FBQyxNQUFELEVBQVMsS0FBS0MsR0FBTCxDQUFTeEMsSUFBSSxDQUFDeUMsV0FBZCxDQUFULENBQVg7O0FBQ0EsWUFBSXpDLElBQUksQ0FBQzBDLEtBQVQsRUFBZ0I7QUFDWkosVUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVcsQ0FBQyxPQUFELEVBQVUsS0FBS0MsR0FBTCxDQUFTeEMsSUFBSSxDQUFDMEMsS0FBZCxDQUFWLENBQVg7QUFDSCxTQUpTLENBS1Y7QUFDQTs7O0FBQ0EsWUFBSWQsYUFBSixFQUFtQjtBQUNmVSxVQUFBQSxLQUFLLENBQUNDLElBQU4sQ0FBVyxDQUFDLFFBQUQsRUFBVyxRQUFYLENBQVg7QUFDQUQsVUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVcsQ0FBQyxLQUFELEVBQVEscUJBQVIsQ0FBWDtBQUNIOztBQUNELGFBQUtsQyxHQUFMLENBQVMsR0FBVCxFQUFjaUMsS0FBZDtBQUNILE9BWkQsTUFZTztBQUNILGFBQUtqQyxHQUFMLENBQVMsSUFBVDtBQUNIO0FBQ0osS0FqQkQ7O0FBbUJBd0IsSUFBQUEsUUFBUSxDQUFDYyxXQUFULEdBQXVCcEMsbUJBQXZCOztBQUVBc0IsSUFBQUEsUUFBUSxDQUFDZSxVQUFULEdBQXNCLFVBQVM1QyxJQUFULEVBQWU7QUFDN0M7Ozs7OztBQU1ZTyxNQUFBQSxtQkFBbUIsQ0FBQzZCLElBQXBCLENBQXlCLElBQXpCLEVBQStCcEMsSUFBL0I7QUFDWjs7O0FBR1MsS0FYRDs7QUFhQSxXQUFPNkIsUUFBUSxDQUFDZ0IsTUFBVCxDQUFnQixLQUFLekIsTUFBckIsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7O0FBU0EwQixFQUFBQSxXQUFXLEdBQUc7QUFDVixVQUFNakIsUUFBUSxHQUFHLElBQUlYLG9CQUFXWSxZQUFmLENBQTRCO0FBQUNDLE1BQUFBLElBQUksRUFBRTtBQUFQLEtBQTVCLENBQWpCO0FBQ0EsVUFBTUUsY0FBYyxHQUFHSixRQUFRLENBQUNLLFNBQWhDLENBRlUsQ0FJVjtBQUNBO0FBQ0E7O0FBQ0FMLElBQUFBLFFBQVEsQ0FBQ2tCLEdBQVQsR0FBZSxVQUFTQyxDQUFULEVBQVk7QUFDdkI7QUFDQSxXQUFLeEMsR0FBTCxDQUFTd0MsQ0FBVDtBQUNILEtBSEQ7O0FBS0FuQixJQUFBQSxRQUFRLENBQUNLLFNBQVQsR0FBcUIsVUFBU2xDLElBQVQsRUFBZW1DLFFBQWYsRUFBeUI7QUFDMUM7QUFDQTtBQUNBLFVBQUkxQixhQUFhLENBQUNULElBQUQsQ0FBakIsRUFBeUI7QUFDckIsWUFBSSxDQUFDbUMsUUFBRCxJQUFhbkMsSUFBSSxDQUFDeUIsSUFBdEIsRUFBNEI7QUFDeEIsZUFBS2pCLEdBQUwsQ0FBUyxNQUFUO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7O0FBVUFxQixJQUFBQSxRQUFRLENBQUNlLFVBQVQsR0FBc0IsVUFBUzVDLElBQVQsRUFBZTtBQUNqQyxXQUFLUSxHQUFMLENBQVNSLElBQUksQ0FBQ0csT0FBZDtBQUNBLFVBQUlNLGFBQWEsQ0FBQ1QsSUFBRCxDQUFiLElBQXVCQSxJQUFJLENBQUN5QixJQUFoQyxFQUFzQyxLQUFLakIsR0FBTCxDQUFTLE1BQVQ7QUFDekMsS0FIRDs7QUFLQSxXQUFPcUIsUUFBUSxDQUFDZ0IsTUFBVCxDQUFnQixLQUFLekIsTUFBckIsQ0FBUDtBQUNIOztBQTNJeUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBjb21tb25tYXJrIGZyb20gJ2NvbW1vbm1hcmsnO1xyXG5pbXBvcnQgZXNjYXBlIGZyb20gJ2xvZGFzaC9lc2NhcGUnO1xyXG5cclxuY29uc3QgQUxMT1dFRF9IVE1MX1RBR1MgPSBbJ3N1YicsICdzdXAnLCAnZGVsJywgJ3UnXTtcclxuXHJcbi8vIFRoZXNlIHR5cGVzIG9mIG5vZGUgYXJlIGRlZmluaXRlbHkgdGV4dFxyXG5jb25zdCBURVhUX05PREVTID0gWyd0ZXh0JywgJ3NvZnRicmVhaycsICdsaW5lYnJlYWsnLCAncGFyYWdyYXBoJywgJ2RvY3VtZW50J107XHJcblxyXG5mdW5jdGlvbiBpc19hbGxvd2VkX2h0bWxfdGFnKG5vZGUpIHtcclxuICAgIC8vIFJlZ2V4IHdvbid0IHdvcmsgZm9yIHRhZ3Mgd2l0aCBhdHRycywgYnV0IHdlIG9ubHlcclxuICAgIC8vIGFsbG93IDxkZWw+IGFueXdheS5cclxuICAgIGNvbnN0IG1hdGNoZXMgPSAvXjxcXC8/KC4qKT4kLy5leGVjKG5vZGUubGl0ZXJhbCk7XHJcbiAgICBpZiAobWF0Y2hlcyAmJiBtYXRjaGVzLmxlbmd0aCA9PSAyKSB7XHJcbiAgICAgICAgY29uc3QgdGFnID0gbWF0Y2hlc1sxXTtcclxuICAgICAgICByZXR1cm4gQUxMT1dFRF9IVE1MX1RBR1MuaW5kZXhPZih0YWcpID4gLTE7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGh0bWxfaWZfdGFnX2FsbG93ZWQobm9kZSkge1xyXG4gICAgaWYgKGlzX2FsbG93ZWRfaHRtbF90YWcobm9kZSkpIHtcclxuICAgICAgICB0aGlzLmxpdChub2RlLmxpdGVyYWwpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5saXQoZXNjYXBlKG5vZGUubGl0ZXJhbCkpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKlxyXG4gKiBSZXR1cm5zIHRydWUgaWYgdGhlIHBhcnNlIG91dHB1dCBjb250YWluaW5nIHRoZSBub2RlXHJcbiAqIGNvbXByaXNlcyBtdWx0aXBsZSBibG9jayBsZXZlbCBlbGVtZW50cyAoaWUuIGxpbmVzKSxcclxuICogb3IgZmFsc2UgaWYgaXQgaXMgb25seSBhIHNpbmdsZSBsaW5lLlxyXG4gKi9cclxuZnVuY3Rpb24gaXNfbXVsdGlfbGluZShub2RlKSB7XHJcbiAgICBsZXQgcGFyID0gbm9kZTtcclxuICAgIHdoaWxlIChwYXIucGFyZW50KSB7XHJcbiAgICAgICAgcGFyID0gcGFyLnBhcmVudDtcclxuICAgIH1cclxuICAgIHJldHVybiBwYXIuZmlyc3RDaGlsZCAhPSBwYXIubGFzdENoaWxkO1xyXG59XHJcblxyXG4vKipcclxuICogQ2xhc3MgdGhhdCB3cmFwcyBjb21tb25tYXJrLCBhZGRpbmcgdGhlIGFiaWxpdHkgdG8gc2VlIHdoZXRoZXJcclxuICogYSBnaXZlbiBtZXNzYWdlIGFjdHVhbGx5IHVzZXMgYW55IG1hcmtkb3duIHN5bnRheCBvciB3aGV0aGVyXHJcbiAqIGl0J3MgcGxhaW4gdGV4dC5cclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1hcmtkb3duIHtcclxuICAgIGNvbnN0cnVjdG9yKGlucHV0KSB7XHJcbiAgICAgICAgdGhpcy5pbnB1dCA9IGlucHV0O1xyXG5cclxuICAgICAgICBjb25zdCBwYXJzZXIgPSBuZXcgY29tbW9ubWFyay5QYXJzZXIoKTtcclxuICAgICAgICB0aGlzLnBhcnNlZCA9IHBhcnNlci5wYXJzZSh0aGlzLmlucHV0KTtcclxuICAgIH1cclxuXHJcbiAgICBpc1BsYWluVGV4dCgpIHtcclxuICAgICAgICBjb25zdCB3YWxrZXIgPSB0aGlzLnBhcnNlZC53YWxrZXIoKTtcclxuXHJcbiAgICAgICAgbGV0IGV2O1xyXG4gICAgICAgIHdoaWxlICggKGV2ID0gd2Fsa2VyLm5leHQoKSkgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5vZGUgPSBldi5ub2RlO1xyXG4gICAgICAgICAgICBpZiAoVEVYVF9OT0RFUy5pbmRleE9mKG5vZGUudHlwZSkgPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgLy8gZGVmaW5pdGVseSB0ZXh0XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChub2RlLnR5cGUgPT0gJ2h0bWxfaW5saW5lJyB8fCBub2RlLnR5cGUgPT0gJ2h0bWxfYmxvY2snKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBpZiBpdCdzIGFuIGFsbG93ZWQgaHRtbCB0YWcsIHdlIG5lZWQgdG8gcmVuZGVyIGl0IGFuZCB0aGVyZWZvcmVcclxuICAgICAgICAgICAgICAgIC8vIHdlIHdpbGwgbmVlZCB0byB1c2UgSFRNTC4gSWYgaXQncyBub3QgYWxsb3dlZCwgaXQncyBub3QgSFRNTCBzaW5jZVxyXG4gICAgICAgICAgICAgICAgLy8gd2UnbGwganVzdCBiZSB0cmVhdGluZyBpdCBhcyB0ZXh0LlxyXG4gICAgICAgICAgICAgICAgaWYgKGlzX2FsbG93ZWRfaHRtbF90YWcobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgdG9IVE1MKHsgZXh0ZXJuYWxMaW5rcyA9IGZhbHNlIH0gPSB7fSkge1xyXG4gICAgICAgIGNvbnN0IHJlbmRlcmVyID0gbmV3IGNvbW1vbm1hcmsuSHRtbFJlbmRlcmVyKHtcclxuICAgICAgICAgICAgc2FmZTogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICAvLyBTZXQgc29mdCBicmVha3MgdG8gaGFyZCBIVE1MIGJyZWFrczogY29tbW9ubWFya1xyXG4gICAgICAgICAgICAvLyBwdXRzIHNvZnRicmVha3MgaW4gZm9yIG11bHRpcGxlIGxpbmVzIGluIGEgYmxvY2txdW90ZSxcclxuICAgICAgICAgICAgLy8gc28gaWYgdGhlc2UgYXJlIGp1c3QgbmV3bGluZSBjaGFyYWN0ZXJzIHRoZW4gdGhlXHJcbiAgICAgICAgICAgIC8vIGJsb2NrIHF1b3RlIGVuZHMgdXAgYWxsIG9uIG9uZSBsaW5lXHJcbiAgICAgICAgICAgIC8vIChodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8zMTU0KVxyXG4gICAgICAgICAgICBzb2Z0YnJlYWs6ICc8YnIgLz4nLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBUcnlpbmcgdG8gc3RyaXAgb3V0IHRoZSB3cmFwcGluZyA8cC8+IGNhdXNlcyBhIGxvdCBtb3JlIGNvbXBsaWNhdGlvblxyXG4gICAgICAgIC8vIHRoYW4gaXQncyB3b3J0aCwgaSB0aGluay4gIEZvciBpbnN0YW5jZSwgdGhpcyBjb2RlIHdpbGwgZ28gYW5kIHN0cmlwXHJcbiAgICAgICAgLy8gb3V0IGFueSA8cC8+IHRhZyAobm8gbWF0dGVyIHdoZXJlIGl0IGlzIGluIHRoZSB0cmVlKSB3aGljaCBkb2Vzbid0XHJcbiAgICAgICAgLy8gY29udGFpbiBcXG4ncy5cclxuICAgICAgICAvLyBPbiB0aGUgZmxpcCBzaWRlLCA8cC8+cyBhcmUgcXVpdGUgb3Bpb25hdGVkIGFuZCByZXN0cmljdGVkIG9uIHdoZXJlXHJcbiAgICAgICAgLy8geW91IGNhbiBuZXN0IHRoZW0uXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBMZXQncyB0cnkgc2VuZGluZyB3aXRoIDxwLz5zIGFueXdheSBmb3Igbm93LCB0aG91Z2guXHJcblxyXG4gICAgICAgIGNvbnN0IHJlYWxfcGFyYWdyYXBoID0gcmVuZGVyZXIucGFyYWdyYXBoO1xyXG5cclxuICAgICAgICByZW5kZXJlci5wYXJhZ3JhcGggPSBmdW5jdGlvbihub2RlLCBlbnRlcmluZykge1xyXG4gICAgICAgICAgICAvLyBJZiB0aGVyZSBpcyBvbmx5IG9uZSB0b3AgbGV2ZWwgbm9kZSwganVzdCByZXR1cm4gdGhlXHJcbiAgICAgICAgICAgIC8vIGJhcmUgdGV4dDogaXQncyBhIHNpbmdsZSBsaW5lIG9mIHRleHQgYW5kIHNvIHNob3VsZCBiZVxyXG4gICAgICAgICAgICAvLyAnaW5saW5lJywgcmF0aGVyIHRoYW4gdW5uZWNlc3NhcmlseSB3cmFwcGVkIGluIGl0cyBvd25cclxuICAgICAgICAgICAgLy8gcCB0YWcuIElmLCBob3dldmVyLCB3ZSBoYXZlIG11bHRpcGxlIG5vZGVzLCBlYWNoIGdldHNcclxuICAgICAgICAgICAgLy8gaXRzIG93biBwIHRhZyB0byBrZWVwIHRoZW0gYXMgc2VwYXJhdGUgcGFyYWdyYXBocy5cclxuICAgICAgICAgICAgaWYgKGlzX211bHRpX2xpbmUobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgIHJlYWxfcGFyYWdyYXBoLmNhbGwodGhpcywgbm9kZSwgZW50ZXJpbmcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmVuZGVyZXIubGluayA9IGZ1bmN0aW9uKG5vZGUsIGVudGVyaW5nKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGF0dHJzID0gdGhpcy5hdHRycyhub2RlKTtcclxuICAgICAgICAgICAgaWYgKGVudGVyaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBhdHRycy5wdXNoKFsnaHJlZicsIHRoaXMuZXNjKG5vZGUuZGVzdGluYXRpb24pXSk7XHJcbiAgICAgICAgICAgICAgICBpZiAobm9kZS50aXRsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzLnB1c2goWyd0aXRsZScsIHRoaXMuZXNjKG5vZGUudGl0bGUpXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBNb2RpZmllZCBsaW5rIGJlaGF2aW91ciB0byB0cmVhdCB0aGVtIGFsbCBhcyBleHRlcm5hbCBhbmRcclxuICAgICAgICAgICAgICAgIC8vIHRodXMgb3BlbmluZyBpbiBhIG5ldyB0YWIuXHJcbiAgICAgICAgICAgICAgICBpZiAoZXh0ZXJuYWxMaW5rcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzLnB1c2goWyd0YXJnZXQnLCAnX2JsYW5rJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzLnB1c2goWydyZWwnLCAnbm9yZWZlcnJlciBub29wZW5lciddKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMudGFnKCdhJywgYXR0cnMpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YWcoJy9hJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZW5kZXJlci5odG1sX2lubGluZSA9IGh0bWxfaWZfdGFnX2FsbG93ZWQ7XHJcblxyXG4gICAgICAgIHJlbmRlcmVyLmh0bWxfYmxvY2sgPSBmdW5jdGlvbihub2RlKSB7XHJcbi8qXHJcbiAgICAgICAgICAgIC8vIGFzIHdpdGggYHBhcmFncmFwaGAsIHdlIG9ubHkgaW5zZXJ0IGxpbmUgYnJlYWtzXHJcbiAgICAgICAgICAgIC8vIGlmIHRoZXJlIGFyZSBtdWx0aXBsZSBsaW5lcyBpbiB0aGUgbWFya2Rvd24uXHJcbiAgICAgICAgICAgIGNvbnN0IGlzTXVsdGlMaW5lID0gaXNfbXVsdGlfbGluZShub2RlKTtcclxuICAgICAgICAgICAgaWYgKGlzTXVsdGlMaW5lKSB0aGlzLmNyKCk7XHJcbiovXHJcbiAgICAgICAgICAgIGh0bWxfaWZfdGFnX2FsbG93ZWQuY2FsbCh0aGlzLCBub2RlKTtcclxuLypcclxuICAgICAgICAgICAgaWYgKGlzTXVsdGlMaW5lKSB0aGlzLmNyKCk7XHJcbiovXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJlbmRlcmVyLnJlbmRlcih0aGlzLnBhcnNlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLypcclxuICAgICAqIFJlbmRlciB0aGUgbWFya2Rvd24gbWVzc2FnZSB0byBwbGFpbiB0ZXh0LiBUaGF0IGlzLCBlc3NlbnRpYWxseVxyXG4gICAgICoganVzdCByZW1vdmUgYW55IGJhY2tzbGFzaGVzIGVzY2FwaW5nIHdoYXQgd291bGQgb3RoZXJ3aXNlIGJlXHJcbiAgICAgKiBtYXJrZG93biBzeW50YXhcclxuICAgICAqICh0byBmaXggaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvMjg3MCkuXHJcbiAgICAgKlxyXG4gICAgICogTi5CLiB0aGlzIGRvZXMgKipOT1QqKiByZW5kZXIgYXJiaXRyYXJ5IE1EIHRvIHBsYWluIHRleHQgLSBvbmx5IE1EXHJcbiAgICAgKiB3aGljaCBoYXMgbm8gZm9ybWF0dGluZy4gIE90aGVyd2lzZSBpdCBlbWl0cyBIVE1MKCEpLlxyXG4gICAgICovXHJcbiAgICB0b1BsYWludGV4dCgpIHtcclxuICAgICAgICBjb25zdCByZW5kZXJlciA9IG5ldyBjb21tb25tYXJrLkh0bWxSZW5kZXJlcih7c2FmZTogZmFsc2V9KTtcclxuICAgICAgICBjb25zdCByZWFsX3BhcmFncmFwaCA9IHJlbmRlcmVyLnBhcmFncmFwaDtcclxuXHJcbiAgICAgICAgLy8gVGhlIGRlZmF1bHQgYG91dGAgZnVuY3Rpb24gb25seSBzZW5kcyB0aGUgaW5wdXQgdGhyb3VnaCBhbiBYTUxcclxuICAgICAgICAvLyBlc2NhcGluZyBmdW5jdGlvbiwgd2hpY2ggY2F1c2VzIG1lc3NhZ2VzIHRvIGJlIGVudGl0eSBlbmNvZGVkLFxyXG4gICAgICAgIC8vIHdoaWNoIHdlIGRvbid0IHdhbnQgaW4gdGhpcyBjYXNlLlxyXG4gICAgICAgIHJlbmRlcmVyLm91dCA9IGZ1bmN0aW9uKHMpIHtcclxuICAgICAgICAgICAgLy8gVGhlIGBsaXRgIGZ1bmN0aW9uIGFkZHMgYSBzdHJpbmcgbGl0ZXJhbCB0byB0aGUgb3V0cHV0IGJ1ZmZlci5cclxuICAgICAgICAgICAgdGhpcy5saXQocyk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmVuZGVyZXIucGFyYWdyYXBoID0gZnVuY3Rpb24obm9kZSwgZW50ZXJpbmcpIHtcclxuICAgICAgICAgICAgLy8gYXMgd2l0aCB0b0hUTUwsIG9ubHkgYXBwZW5kIGxpbmVzIHRvIHBhcmFncmFwaHMgaWYgdGhlcmUgYXJlXHJcbiAgICAgICAgICAgIC8vIG11bHRpcGxlIHBhcmFncmFwaHNcclxuICAgICAgICAgICAgaWYgKGlzX211bHRpX2xpbmUobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgIGlmICghZW50ZXJpbmcgJiYgbm9kZS5uZXh0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXQoJ1xcblxcbicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmVuZGVyZXIuaHRtbF9ibG9jayA9IGZ1bmN0aW9uKG5vZGUpIHtcclxuICAgICAgICAgICAgdGhpcy5saXQobm9kZS5saXRlcmFsKTtcclxuICAgICAgICAgICAgaWYgKGlzX211bHRpX2xpbmUobm9kZSkgJiYgbm9kZS5uZXh0KSB0aGlzLmxpdCgnXFxuXFxuJyk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJlbmRlcmVyLnJlbmRlcih0aGlzLnBhcnNlZCk7XHJcbiAgICB9XHJcbn1cclxuIl19