/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

function _default(dest, src) {
  for (const i in src) {
    if (src.hasOwnProperty(i)) {
      dest[i] = src[i];
    }
  }

  return dest;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9leHRlbmQuanMiXSwibmFtZXMiOlsiZGVzdCIsInNyYyIsImkiLCJoYXNPd25Qcm9wZXJ0eSJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTs7Ozs7OztBQUVlLGtCQUFTQSxJQUFULEVBQWVDLEdBQWYsRUFBb0I7QUFDL0IsT0FBSyxNQUFNQyxDQUFYLElBQWdCRCxHQUFoQixFQUFxQjtBQUNqQixRQUFJQSxHQUFHLENBQUNFLGNBQUosQ0FBbUJELENBQW5CLENBQUosRUFBMkI7QUFDdkJGLE1BQUFBLElBQUksQ0FBQ0UsQ0FBRCxDQUFKLEdBQVVELEdBQUcsQ0FBQ0MsQ0FBRCxDQUFiO0FBQ0g7QUFDSjs7QUFDRCxTQUFPRixJQUFQO0FBQ0giLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uKGRlc3QsIHNyYykge1xyXG4gICAgZm9yIChjb25zdCBpIGluIHNyYykge1xyXG4gICAgICAgIGlmIChzcmMuaGFzT3duUHJvcGVydHkoaSkpIHtcclxuICAgICAgICAgICAgZGVzdFtpXSA9IHNyY1tpXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZGVzdDtcclxufVxyXG4iXX0=