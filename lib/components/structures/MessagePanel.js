"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _shouldHideEvent = _interopRequireDefault(require("../../shouldHideEvent"));

var _DateUtils = require("../../DateUtils");

var sdk = _interopRequireWildcard(require("../../index"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _SettingsStore = _interopRequireDefault(require("../../settings/SettingsStore"));

var _languageHandler = require("../../languageHandler");

var _EventTile = require("../views/rooms/EventTile");

var _TextForEvent = require("../../TextForEvent");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const CONTINUATION_MAX_INTERVAL = 5 * 60 * 1000; // 5 minutes

const continuedTypes = ['m.sticker', 'm.room.message'];

const isMembershipChange = e => e.getType() === 'm.room.member' || e.getType() === 'm.room.third_party_invite';
/* (almost) stateless UI component which builds the event tiles in the room timeline.
 */


class MessagePanel extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onShowTypingNotificationsChange", () => {
      this.setState({
        showTypingNotifications: _SettingsStore.default.getValue("showTypingNotifications")
      });
    });
    (0, _defineProperty2.default)(this, "_collectGhostReadMarker", node => {
      if (node) {
        // now the element has appeared, change the style which will trigger the CSS transition
        requestAnimationFrame(() => {
          node.style.width = '10%';
          node.style.opacity = '0';
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onGhostTransitionEnd", ev => {
      // we can now clean up the ghost element
      const finishedEventId = ev.target.dataset.eventid;
      this.setState({
        ghostReadMarkers: this.state.ghostReadMarkers.filter(eid => eid !== finishedEventId)
      });
    });
    (0, _defineProperty2.default)(this, "_collectEventNode", (eventId, node) => {
      this.eventNodes[eventId] = node;
    });
    (0, _defineProperty2.default)(this, "_onHeightChanged", () => {
      const scrollPanel = this._scrollPanel.current;

      if (scrollPanel) {
        scrollPanel.checkScroll();
      }
    });
    (0, _defineProperty2.default)(this, "_onTypingShown", () => {
      const scrollPanel = this._scrollPanel.current; // this will make the timeline grow, so checkScroll

      scrollPanel.checkScroll();

      if (scrollPanel && scrollPanel.getScrollState().stuckAtBottom) {
        scrollPanel.preventShrinking();
      }
    });
    (0, _defineProperty2.default)(this, "_onTypingHidden", () => {
      const scrollPanel = this._scrollPanel.current;

      if (scrollPanel) {
        // as hiding the typing notifications doesn't
        // update the scrollPanel, we tell it to apply
        // the shrinking prevention once the typing notifs are hidden
        scrollPanel.updatePreventShrinking(); // order is important here as checkScroll will scroll down to
        // reveal added padding to balance the notifs disappearing.

        scrollPanel.checkScroll();
      }
    });
    this.state = {
      // previous positions the read marker has been in, so we can
      // display 'ghost' read markers that are animating away
      ghostReadMarkers: [],
      showTypingNotifications: _SettingsStore.default.getValue("showTypingNotifications")
    }; // opaque readreceipt info for each userId; used by ReadReceiptMarker
    // to manage its animations

    this._readReceiptMap = {}; // Track read receipts by event ID. For each _shown_ event ID, we store
    // the list of read receipts to display:
    //   [
    //       {
    //           userId: string,
    //           member: RoomMember,
    //           ts: number,
    //       },
    //   ]
    // This is recomputed on each render. It's only stored on the component
    // for ease of passing the data around since it's computed in one pass
    // over all events.

    this._readReceiptsByEvent = {}; // Track read receipts by user ID. For each user ID we've ever shown a
    // a read receipt for, we store an object:
    //   {
    //       lastShownEventId: string,
    //       receipt: {
    //           userId: string,
    //           member: RoomMember,
    //           ts: number,
    //       },
    //   }
    // so that we can always keep receipts displayed by reverting back to
    // the last shown event for that user ID when needed. This may feel like
    // it duplicates the receipt storage in the room, but at this layer, we
    // are tracking _shown_ event IDs, which the JS SDK knows nothing about.
    // This is recomputed on each render, using the data from the previous
    // render as our fallback for any user IDs we can't match a receipt to a
    // displayed event in the current render cycle.

    this._readReceiptsByUserId = {}; // Cache hidden events setting on mount since Settings is expensive to
    // query, and we check this in a hot code path.

    this._showHiddenEventsInTimeline = _SettingsStore.default.getValue("showHiddenEventsInTimeline");
    this._isMounted = false;
    this._readMarkerNode = (0, _react.createRef)();
    this._whoIsTyping = (0, _react.createRef)();
    this._scrollPanel = (0, _react.createRef)();
    this._showTypingNotificationsWatcherRef = _SettingsStore.default.watchSetting("showTypingNotifications", null, this.onShowTypingNotificationsChange);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;

    _SettingsStore.default.unwatchSetting(this._showTypingNotificationsWatcherRef);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.readMarkerVisible && this.props.readMarkerEventId !== prevProps.readMarkerEventId) {
      const ghostReadMarkers = this.state.ghostReadMarkers;
      ghostReadMarkers.push(prevProps.readMarkerEventId);
      this.setState({
        ghostReadMarkers
      });
    }
  }

  /* get the DOM node representing the given event */
  getNodeForEventId(eventId) {
    if (!this.eventNodes) {
      return undefined;
    }

    return this.eventNodes[eventId];
  }
  /* return true if the content is fully scrolled down right now; else false.
   */


  isAtBottom() {
    return this._scrollPanel.current && this._scrollPanel.current.isAtBottom();
  }
  /* get the current scroll state. See ScrollPanel.getScrollState for
   * details.
   *
   * returns null if we are not mounted.
   */


  getScrollState() {
    return this._scrollPanel.current ? this._scrollPanel.current.getScrollState() : null;
  } // returns one of:
  //
  //  null: there is no read marker
  //  -1: read marker is above the window
  //   0: read marker is within the window
  //  +1: read marker is below the window


  getReadMarkerPosition() {
    const readMarker = this._readMarkerNode.current;
    const messageWrapper = this._scrollPanel.current;

    if (!readMarker || !messageWrapper) {
      return null;
    }

    const wrapperRect = _reactDom.default.findDOMNode(messageWrapper).getBoundingClientRect();

    const readMarkerRect = readMarker.getBoundingClientRect(); // the read-marker pretends to have zero height when it is actually
    // two pixels high; +2 here to account for that.

    if (readMarkerRect.bottom + 2 < wrapperRect.top) {
      return -1;
    } else if (readMarkerRect.top < wrapperRect.bottom) {
      return 0;
    } else {
      return 1;
    }
  }
  /* jump to the top of the content.
   */


  scrollToTop() {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.scrollToTop();
    }
  }
  /* jump to the bottom of the content.
   */


  scrollToBottom() {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.scrollToBottom();
    }
  }
  /**
   * Page up/down.
   *
   * @param {number} mult: -1 to page up, +1 to page down
   */


  scrollRelative(mult) {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.scrollRelative(mult);
    }
  }
  /**
   * Scroll up/down in response to a scroll key
   *
   * @param {KeyboardEvent} ev: the keyboard event to handle
   */


  handleScrollKey(ev) {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.handleScrollKey(ev);
    }
  }
  /* jump to the given event id.
   *
   * offsetBase gives the reference point for the pixelOffset. 0 means the
   * top of the container, 1 means the bottom, and fractional values mean
   * somewhere in the middle. If omitted, it defaults to 0.
   *
   * pixelOffset gives the number of pixels *above* the offsetBase that the
   * node (specifically, the bottom of it) will be positioned. If omitted, it
   * defaults to 0.
   */


  scrollToEvent(eventId, pixelOffset, offsetBase) {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.scrollToToken(eventId, pixelOffset, offsetBase);
    }
  }

  scrollToEventIfNeeded(eventId) {
    const node = this.eventNodes[eventId];

    if (node) {
      node.scrollIntoView({
        block: "nearest",
        behavior: "instant"
      });
    }
  }
  /* check the scroll state and send out pagination requests if necessary.
   */


  checkFillState() {
    if (this._scrollPanel.current) {
      this._scrollPanel.current.checkFillState();
    }
  }

  _isUnmounting() {
    return !this._isMounted;
  } // TODO: Implement granular (per-room) hide options


  _shouldShowEvent(mxEv) {
    if (mxEv.sender && _MatrixClientPeg.MatrixClientPeg.get().isUserIgnored(mxEv.sender.userId)) {
      return false; // ignored = no show (only happens if the ignore happens after an event was received)
    }

    if (this._showHiddenEventsInTimeline) {
      return true;
    }

    if (!(0, _EventTile.haveTileForEvent)(mxEv)) {
      return false; // no tile = no show
    } // Always show highlighted event


    if (this.props.highlightedEventId === mxEv.getId()) return true;
    return !(0, _shouldHideEvent.default)(mxEv);
  }

  _readMarkerForEvent(eventId, isLastEvent) {
    const visible = !isLastEvent && this.props.readMarkerVisible;

    if (this.props.readMarkerEventId === eventId) {
      let hr; // if the read marker comes at the end of the timeline (except
      // for local echoes, which are excluded from RMs, because they
      // don't have useful event ids), we don't want to show it, but
      // we still want to create the <li/> for it so that the
      // algorithms which depend on its position on the screen aren't
      // confused.

      if (visible) {
        hr = _react.default.createElement("hr", {
          className: "mx_RoomView_myReadMarker",
          style: {
            opacity: 1,
            width: '99%'
          }
        });
      }

      return _react.default.createElement("li", {
        key: "readMarker_" + eventId,
        ref: this._readMarkerNode,
        className: "mx_RoomView_myReadMarker_container"
      }, hr);
    } else if (this.state.ghostReadMarkers.includes(eventId)) {
      // We render 'ghost' read markers in the DOM while they
      // transition away. This allows the actual read marker
      // to be in the right place straight away without having
      // to wait for the transition to finish.
      // There are probably much simpler ways to do this transition,
      // possibly using react-transition-group which handles keeping
      // elements in the DOM whilst they transition out, although our
      // case is a little more complex because only some of the items
      // transition (ie. the read markers do but the event tiles do not)
      // and TransitionGroup requires that all its children are Transitions.
      const hr = _react.default.createElement("hr", {
        className: "mx_RoomView_myReadMarker",
        ref: this._collectGhostReadMarker,
        onTransitionEnd: this._onGhostTransitionEnd,
        "data-eventid": eventId
      }); // give it a key which depends on the event id. That will ensure that
      // we get a new DOM node (restarting the animation) when the ghost
      // moves to a different event.


      return _react.default.createElement("li", {
        key: "_readuptoghost_" + eventId,
        className: "mx_RoomView_myReadMarker_container"
      }, hr);
    }

    return null;
  }

  _getEventTiles() {
    this.eventNodes = {};
    let i; // first figure out which is the last event in the list which we're
    // actually going to show; this allows us to behave slightly
    // differently for the last event in the list. (eg show timestamp)
    //
    // we also need to figure out which is the last event we show which isn't
    // a local echo, to manage the read-marker.

    let lastShownEvent;
    let lastShownNonLocalEchoIndex = -1;

    for (i = this.props.events.length - 1; i >= 0; i--) {
      const mxEv = this.props.events[i];

      if (!this._shouldShowEvent(mxEv)) {
        continue;
      }

      if (lastShownEvent === undefined) {
        lastShownEvent = mxEv;
      }

      if (mxEv.status) {
        // this is a local echo
        continue;
      }

      lastShownNonLocalEchoIndex = i;
      break;
    }

    const ret = [];
    let prevEvent = null; // the last event we showed

    this._readReceiptsByEvent = {};

    if (this.props.showReadReceipts) {
      this._readReceiptsByEvent = this._getReadReceiptsByShownEvent();
    }

    let grouper = null;

    for (i = 0; i < this.props.events.length; i++) {
      const mxEv = this.props.events[i];
      const eventId = mxEv.getId();
      const last = mxEv === lastShownEvent;

      if (grouper) {
        if (grouper.shouldGroup(mxEv)) {
          grouper.add(mxEv);
          continue;
        } else {
          // not part of group, so get the group tiles, close the
          // group, and continue like a normal event
          ret.push(...grouper.getTiles());
          prevEvent = grouper.getNewPrevEvent();
          grouper = null;
        }
      }

      for (const Grouper of groupers) {
        if (Grouper.canStartGroup(this, mxEv)) {
          grouper = new Grouper(this, mxEv, prevEvent, lastShownEvent);
        }
      }

      if (!grouper) {
        const wantTile = this._shouldShowEvent(mxEv);

        if (wantTile) {
          // make sure we unpack the array returned by _getTilesForEvent,
          // otherwise react will auto-generate keys and we will end up
          // replacing all of the DOM elements every time we paginate.
          ret.push(...this._getTilesForEvent(prevEvent, mxEv, last));
          prevEvent = mxEv;
        }

        const readMarker = this._readMarkerForEvent(eventId, i >= lastShownNonLocalEchoIndex);

        if (readMarker) ret.push(readMarker);
      }
    }

    if (grouper) {
      ret.push(...grouper.getTiles());
    }

    return ret;
  }

  _getTilesForEvent(prevEvent, mxEv, last) {
    const EventTile = sdk.getComponent('rooms.EventTile');
    const DateSeparator = sdk.getComponent('messages.DateSeparator');
    const ret = [];
    const isEditing = this.props.editState && this.props.editState.getEvent().getId() === mxEv.getId(); // is this a continuation of the previous message?

    let continuation = false; // Some events should appear as continuations from previous events of
    // different types.

    const eventTypeContinues = prevEvent !== null && continuedTypes.includes(mxEv.getType()) && continuedTypes.includes(prevEvent.getType()); // if there is a previous event and it has the same sender as this event
    // and the types are the same/is in continuedTypes and the time between them is <= CONTINUATION_MAX_INTERVAL

    if (prevEvent !== null && prevEvent.sender && mxEv.sender && mxEv.sender.userId === prevEvent.sender.userId && // if we don't have tile for previous event then it was shown by showHiddenEvents and has no SenderProfile
    (0, _EventTile.haveTileForEvent)(prevEvent) && (mxEv.getType() === prevEvent.getType() || eventTypeContinues) && mxEv.getTs() - prevEvent.getTs() <= CONTINUATION_MAX_INTERVAL) {
      continuation = true;
    }
    /*
            // Work out if this is still a continuation, as we are now showing commands
            // and /me messages with their own little avatar. The case of a change of
            // event type (commands) is handled above, but we need to handle the /me
            // messages seperately as they have a msgtype of 'm.emote' but are classed
            // as normal messages
            if (prevEvent !== null && prevEvent.sender && mxEv.sender
                    && mxEv.sender.userId === prevEvent.sender.userId
                    && mxEv.getType() == prevEvent.getType()
                    && prevEvent.getContent().msgtype === 'm.emote') {
                continuation = false;
            }
    */
    // local echoes have a fake date, which could even be yesterday. Treat them
    // as 'today' for the date separators.


    let ts1 = mxEv.getTs();
    let eventDate = mxEv.getDate();

    if (mxEv.status) {
      eventDate = new Date();
      ts1 = eventDate.getTime();
    } // do we need a date separator since the last event?


    if (this._wantsDateSeparator(prevEvent, eventDate)) {
      const dateSeparator = _react.default.createElement("li", {
        key: ts1
      }, _react.default.createElement(DateSeparator, {
        key: ts1,
        ts: ts1
      }));

      ret.push(dateSeparator);
      continuation = false;
    }

    const eventId = mxEv.getId();
    const highlight = eventId === this.props.highlightedEventId; // we can't use local echoes as scroll tokens, because their event IDs change.
    // Local echos have a send "status".

    const scrollToken = mxEv.status ? undefined : eventId;
    const readReceipts = this._readReceiptsByEvent[eventId]; // Dev note: `this._isUnmounting.bind(this)` is important - it ensures that
    // the function is run in the context of this class and not EventTile, therefore
    // ensuring the right `this._mounted` variable is used by read receipts (which
    // don't update their position if we, the MessagePanel, is unmounting).

    ret.push(_react.default.createElement("li", {
      key: eventId,
      ref: this._collectEventNode.bind(this, eventId),
      "data-scroll-tokens": scrollToken
    }, _react.default.createElement(EventTile, {
      mxEvent: mxEv,
      continuation: continuation,
      isRedacted: mxEv.isRedacted(),
      replacingEventId: mxEv.replacingEventId(),
      editState: isEditing && this.props.editState,
      onHeightChanged: this._onHeightChanged,
      readReceipts: readReceipts,
      readReceiptMap: this._readReceiptMap,
      showUrlPreview: this.props.showUrlPreview,
      checkUnmounting: this._isUnmounting.bind(this),
      eventSendStatus: mxEv.getAssociatedStatus(),
      tileShape: this.props.tileShape,
      isTwelveHour: this.props.isTwelveHour,
      permalinkCreator: this.props.permalinkCreator,
      last: last,
      isSelectedEvent: highlight,
      getRelationsForEvent: this.props.getRelationsForEvent,
      showReactions: this.props.showReactions
    })));
    return ret;
  }

  _wantsDateSeparator(prevEvent, nextEventDate) {
    if (prevEvent == null) {
      // first event in the panel: depends if we could back-paginate from
      // here.
      return !this.props.suppressFirstDateSeparator;
    }

    return (0, _DateUtils.wantsDateSeparator)(prevEvent.getDate(), nextEventDate);
  } // Get a list of read receipts that should be shown next to this event
  // Receipts are objects which have a 'userId', 'roomMember' and 'ts'.


  _getReadReceiptsForEvent(event) {
    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId; // get list of read receipts, sorted most recent first


    const {
      room
    } = this.props;

    if (!room) {
      return null;
    }

    const receipts = [];
    room.getReceiptsForEvent(event).forEach(r => {
      if (!r.userId || r.type !== "m.read" || r.userId === myUserId) {
        return; // ignore non-read receipts and receipts from self.
      }

      if (_MatrixClientPeg.MatrixClientPeg.get().isUserIgnored(r.userId)) {
        return; // ignore ignored users
      }

      const member = room.getMember(r.userId);
      receipts.push({
        userId: r.userId,
        roomMember: member,
        ts: r.data ? r.data.ts : 0
      });
    });
    return receipts;
  } // Get an object that maps from event ID to a list of read receipts that
  // should be shown next to that event. If a hidden event has read receipts,
  // they are folded into the receipts of the last shown event.


  _getReadReceiptsByShownEvent() {
    const receiptsByEvent = {};
    const receiptsByUserId = {};
    let lastShownEventId;

    for (const event of this.props.events) {
      if (this._shouldShowEvent(event)) {
        lastShownEventId = event.getId();
      }

      if (!lastShownEventId) {
        continue;
      }

      const existingReceipts = receiptsByEvent[lastShownEventId] || [];

      const newReceipts = this._getReadReceiptsForEvent(event);

      receiptsByEvent[lastShownEventId] = existingReceipts.concat(newReceipts); // Record these receipts along with their last shown event ID for
      // each associated user ID.

      for (const receipt of newReceipts) {
        receiptsByUserId[receipt.userId] = {
          lastShownEventId,
          receipt
        };
      }
    } // It's possible in some cases (for example, when a read receipt
    // advances before we have paginated in the new event that it's marking
    // received) that we can temporarily not have a matching event for
    // someone which had one in the last. By looking through our previous
    // mapping of receipts by user ID, we can cover recover any receipts
    // that would have been lost by using the same event ID from last time.


    for (const userId in this._readReceiptsByUserId) {
      if (receiptsByUserId[userId]) {
        continue;
      }

      const {
        lastShownEventId,
        receipt
      } = this._readReceiptsByUserId[userId];
      const existingReceipts = receiptsByEvent[lastShownEventId] || [];
      receiptsByEvent[lastShownEventId] = existingReceipts.concat(receipt);
      receiptsByUserId[userId] = {
        lastShownEventId,
        receipt
      };
    }

    this._readReceiptsByUserId = receiptsByUserId; // After grouping receipts by shown events, do another pass to sort each
    // receipt list.

    for (const eventId in receiptsByEvent) {
      receiptsByEvent[eventId].sort((r1, r2) => {
        return r2.ts - r1.ts;
      });
    }

    return receiptsByEvent;
  }

  updateTimelineMinHeight() {
    const scrollPanel = this._scrollPanel.current;

    if (scrollPanel) {
      const isAtBottom = scrollPanel.isAtBottom();
      const whoIsTyping = this._whoIsTyping.current;
      const isTypingVisible = whoIsTyping && whoIsTyping.isVisible(); // when messages get added to the timeline,
      // but somebody else is still typing,
      // update the min-height, so once the last
      // person stops typing, no jumping occurs

      if (isAtBottom && isTypingVisible) {
        scrollPanel.preventShrinking();
      }
    }
  }

  onTimelineReset() {
    const scrollPanel = this._scrollPanel.current;

    if (scrollPanel) {
      scrollPanel.clearPreventShrinking();
    }
  }

  render() {
    const ScrollPanel = sdk.getComponent("structures.ScrollPanel");
    const WhoIsTypingTile = sdk.getComponent("rooms.WhoIsTypingTile");
    const Spinner = sdk.getComponent("elements.Spinner");
    let topSpinner;
    let bottomSpinner;

    if (this.props.backPaginating) {
      topSpinner = _react.default.createElement("li", {
        key: "_topSpinner"
      }, _react.default.createElement(Spinner, null));
    }

    if (this.props.forwardPaginating) {
      bottomSpinner = _react.default.createElement("li", {
        key: "_bottomSpinner"
      }, _react.default.createElement(Spinner, null));
    }

    const style = this.props.hidden ? {
      display: 'none'
    } : {};
    const className = (0, _classnames.default)(this.props.className, {
      "mx_MessagePanel_alwaysShowTimestamps": this.props.alwaysShowTimestamps
    });
    let whoIsTyping;

    if (this.props.room && !this.props.tileShape && this.state.showTypingNotifications) {
      whoIsTyping = _react.default.createElement(WhoIsTypingTile, {
        room: this.props.room,
        onShown: this._onTypingShown,
        onHidden: this._onTypingHidden,
        ref: this._whoIsTyping
      });
    }

    return _react.default.createElement(ScrollPanel, {
      ref: this._scrollPanel,
      className: className,
      onScroll: this.props.onScroll,
      onResize: this.onResize,
      onFillRequest: this.props.onFillRequest,
      onUnfillRequest: this.props.onUnfillRequest,
      style: style,
      stickyBottom: this.props.stickyBottom,
      resizeNotifier: this.props.resizeNotifier
    }, topSpinner, this._getEventTiles(), whoIsTyping, bottomSpinner);
  }

}
/* Grouper classes determine when events can be grouped together in a summary.
 * Groupers should have the following methods:
 * - canStartGroup (static): determines if a new group should be started with the
 *   given event
 * - shouldGroup: determines if the given event should be added to an existing group
 * - add: adds an event to an existing group (should only be called if shouldGroup
 *   return true)
 * - getTiles: returns the tiles that represent the group
 * - getNewPrevEvent: returns the event that should be used as the new prevEvent
 *   when determining things such as whether a date separator is necessary
 */
// Wrap initial room creation events into an EventListSummary
// Grouping only events sent by the same user that sent the `m.room.create` and only until
// the first non-state event or membership event which is not regarding the sender of the `m.room.create` event


exports.default = MessagePanel;
(0, _defineProperty2.default)(MessagePanel, "propTypes", {
  // true to give the component a 'display: none' style.
  hidden: _propTypes.default.bool,
  // true to show a spinner at the top of the timeline to indicate
  // back-pagination in progress
  backPaginating: _propTypes.default.bool,
  // true to show a spinner at the end of the timeline to indicate
  // forward-pagination in progress
  forwardPaginating: _propTypes.default.bool,
  // the list of MatrixEvents to display
  events: _propTypes.default.array.isRequired,
  // ID of an event to highlight. If undefined, no event will be highlighted.
  highlightedEventId: _propTypes.default.string,
  // The room these events are all in together, if any.
  // (The notification panel won't have a room here, for example.)
  room: _propTypes.default.object,
  // Should we show URL Previews
  showUrlPreview: _propTypes.default.bool,
  // event after which we should show a read marker
  readMarkerEventId: _propTypes.default.string,
  // whether the read marker should be visible
  readMarkerVisible: _propTypes.default.bool,
  // the userid of our user. This is used to suppress the read marker
  // for pending messages.
  ourUserId: _propTypes.default.string,
  // true to suppress the date at the start of the timeline
  suppressFirstDateSeparator: _propTypes.default.bool,
  // whether to show read receipts
  showReadReceipts: _propTypes.default.bool,
  // true if updates to the event list should cause the scroll panel to
  // scroll down when we are at the bottom of the window. See ScrollPanel
  // for more details.
  stickyBottom: _propTypes.default.bool,
  // callback which is called when the panel is scrolled.
  onScroll: _propTypes.default.func,
  // callback which is called when more content is needed.
  onFillRequest: _propTypes.default.func,
  // className for the panel
  className: _propTypes.default.string.isRequired,
  // shape parameter to be passed to EventTiles
  tileShape: _propTypes.default.string,
  // show twelve hour timestamps
  isTwelveHour: _propTypes.default.bool,
  // show timestamps always
  alwaysShowTimestamps: _propTypes.default.bool,
  // helper function to access relations for an event
  getRelationsForEvent: _propTypes.default.func,
  // whether to show reactions for an event
  showReactions: _propTypes.default.bool
});

class CreationGrouper {
  constructor(panel, createEvent, prevEvent, lastShownEvent) {
    this.panel = panel;
    this.createEvent = createEvent;
    this.prevEvent = prevEvent;
    this.lastShownEvent = lastShownEvent;
    this.events = []; // events that we include in the group but then eject out and place
    // above the group.

    this.ejectedEvents = [];
    this.readMarker = panel._readMarkerForEvent(createEvent.getId(), createEvent === lastShownEvent);
  }

  shouldGroup(ev) {
    const panel = this.panel;
    const createEvent = this.createEvent;

    if (!panel._shouldShowEvent(ev)) {
      return true;
    }

    if (panel._wantsDateSeparator(this.createEvent, ev.getDate())) {
      return false;
    }

    if (ev.getType() === "m.room.member" && (ev.getStateKey() !== createEvent.getSender() || ev.getContent()["membership"] !== "join")) {
      return false;
    }

    if (ev.isState() && ev.getSender() === createEvent.getSender()) {
      return true;
    }

    return false;
  }

  add(ev) {
    const panel = this.panel;
    this.readMarker = this.readMarker || panel._readMarkerForEvent(ev.getId(), ev === this.lastShownEvent);

    if (!panel._shouldShowEvent(ev)) {
      return;
    }

    if (ev.getType() === "m.room.encryption") {
      this.ejectedEvents.push(ev);
    } else {
      this.events.push(ev);
    }
  }

  getTiles() {
    // If we don't have any events to group, don't even try to group them. The logic
    // below assumes that we have a group of events to deal with, but we might not if
    // the events we were supposed to group were redacted.
    if (!this.events || !this.events.length) return [];
    const DateSeparator = sdk.getComponent('messages.DateSeparator');
    const EventListSummary = sdk.getComponent('views.elements.EventListSummary');
    const panel = this.panel;
    const ret = [];
    const createEvent = this.createEvent;
    const lastShownEvent = this.lastShownEvent;

    if (panel._wantsDateSeparator(this.prevEvent, createEvent.getDate())) {
      const ts = createEvent.getTs();
      ret.push(_react.default.createElement("li", {
        key: ts + '~'
      }, _react.default.createElement(DateSeparator, {
        key: ts + '~',
        ts: ts
      })));
    } // If this m.room.create event should be shown (room upgrade) then show it before the summary


    if (panel._shouldShowEvent(createEvent)) {
      // pass in the createEvent as prevEvent as well so no extra DateSeparator is rendered
      ret.push(...panel._getTilesForEvent(createEvent, createEvent, false));
    }

    for (const ejected of this.ejectedEvents) {
      ret.push(...panel._getTilesForEvent(createEvent, ejected, createEvent === lastShownEvent));
    }

    const eventTiles = this.events.map(e => {
      // In order to prevent DateSeparators from appearing in the expanded form
      // of EventListSummary, render each member event as if the previous
      // one was itself. This way, the timestamp of the previous event === the
      // timestamp of the current event, and no DateSeparator is inserted.
      return panel._getTilesForEvent(e, e, e === lastShownEvent);
    }).reduce((a, b) => a.concat(b), []); // Get sender profile from the latest event in the summary as the m.room.create doesn't contain one

    const ev = this.events[this.events.length - 1];
    ret.push(_react.default.createElement(EventListSummary, {
      key: "roomcreationsummary",
      events: this.events,
      onToggle: panel._onHeightChanged // Update scroll state
      ,
      summaryMembers: [ev.sender],
      summaryText: (0, _languageHandler._t)("%(creator)s created and configured the room.", {
        creator: ev.sender ? ev.sender.name : ev.getSender()
      })
    }, eventTiles));

    if (this.readMarker) {
      ret.push(this.readMarker);
    }

    return ret;
  }

  getNewPrevEvent() {
    return this.createEvent;
  }

} // Wrap consecutive member events in a ListSummary, ignore if redacted


(0, _defineProperty2.default)(CreationGrouper, "canStartGroup", function (panel, ev) {
  return ev.getType() === "m.room.create";
});

class MemberGrouper {
  constructor(panel, ev, prevEvent, lastShownEvent) {
    this.panel = panel;
    this.readMarker = panel._readMarkerForEvent(ev.getId(), ev === lastShownEvent);
    this.events = [ev];
    this.prevEvent = prevEvent;
    this.lastShownEvent = lastShownEvent;
  }

  shouldGroup(ev) {
    if (this.panel._wantsDateSeparator(this.events[0], ev.getDate())) {
      return false;
    }

    return isMembershipChange(ev);
  }

  add(ev) {
    if (ev.getType() === 'm.room.member') {
      // We'll just double check that it's worth our time to do so, through an
      // ugly hack. If textForEvent returns something, we should group it for
      // rendering but if it doesn't then we'll exclude it.
      const renderText = (0, _TextForEvent.textForEvent)(ev);
      if (!renderText || renderText.trim().length === 0) return; // quietly ignore
    }

    this.readMarker = this.readMarker || this.panel._readMarkerForEvent(ev.getId(), ev === this.lastShownEvent);
    this.events.push(ev);
  }

  getTiles() {
    // If we don't have any events to group, don't even try to group them. The logic
    // below assumes that we have a group of events to deal with, but we might not if
    // the events we were supposed to group were redacted.
    if (!this.events || !this.events.length) return [];
    const DateSeparator = sdk.getComponent('messages.DateSeparator');
    const MemberEventListSummary = sdk.getComponent('views.elements.MemberEventListSummary');
    const panel = this.panel;
    const lastShownEvent = this.lastShownEvent;
    const ret = [];

    if (panel._wantsDateSeparator(this.prevEvent, this.events[0].getDate())) {
      const ts = this.events[0].getTs();
      ret.push(_react.default.createElement("li", {
        key: ts + '~'
      }, _react.default.createElement(DateSeparator, {
        key: ts + '~',
        ts: ts
      })));
    } // Ensure that the key of the MemberEventListSummary does not change with new
    // member events. This will prevent it from being re-created unnecessarily, and
    // instead will allow new props to be provided. In turn, the shouldComponentUpdate
    // method on MELS can be used to prevent unnecessary renderings.
    //
    // Whilst back-paginating with a MELS at the top of the panel, prevEvent will be null,
    // so use the key "membereventlistsummary-initial". Otherwise, use the ID of the first
    // membership event, which will not change during forward pagination.


    const key = "membereventlistsummary-" + (this.prevEvent ? this.events[0].getId() : "initial");
    let highlightInMels;
    let eventTiles = this.events.map(e => {
      if (e.getId() === panel.props.highlightedEventId) {
        highlightInMels = true;
      } // In order to prevent DateSeparators from appearing in the expanded form
      // of MemberEventListSummary, render each member event as if the previous
      // one was itself. This way, the timestamp of the previous event === the
      // timestamp of the current event, and no DateSeparator is inserted.


      return panel._getTilesForEvent(e, e, e === lastShownEvent);
    }).reduce((a, b) => a.concat(b), []);

    if (eventTiles.length === 0) {
      eventTiles = null;
    }

    ret.push(_react.default.createElement(MemberEventListSummary, {
      key: key,
      events: this.events,
      onToggle: panel._onHeightChanged // Update scroll state
      ,
      startExpanded: highlightInMels
    }, eventTiles));

    if (this.readMarker) {
      ret.push(this.readMarker);
    }

    return ret;
  }

  getNewPrevEvent() {
    return this.events[0];
  }

} // all the grouper classes that we use


(0, _defineProperty2.default)(MemberGrouper, "canStartGroup", function (panel, ev) {
  return panel._shouldShowEvent(ev) && isMembershipChange(ev);
});
const groupers = [CreationGrouper, MemberGrouper];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvTWVzc2FnZVBhbmVsLmpzIl0sIm5hbWVzIjpbIkNPTlRJTlVBVElPTl9NQVhfSU5URVJWQUwiLCJjb250aW51ZWRUeXBlcyIsImlzTWVtYmVyc2hpcENoYW5nZSIsImUiLCJnZXRUeXBlIiwiTWVzc2FnZVBhbmVsIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInNldFN0YXRlIiwic2hvd1R5cGluZ05vdGlmaWNhdGlvbnMiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJub2RlIiwicmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwic3R5bGUiLCJ3aWR0aCIsIm9wYWNpdHkiLCJldiIsImZpbmlzaGVkRXZlbnRJZCIsInRhcmdldCIsImRhdGFzZXQiLCJldmVudGlkIiwiZ2hvc3RSZWFkTWFya2VycyIsInN0YXRlIiwiZmlsdGVyIiwiZWlkIiwiZXZlbnRJZCIsImV2ZW50Tm9kZXMiLCJzY3JvbGxQYW5lbCIsIl9zY3JvbGxQYW5lbCIsImN1cnJlbnQiLCJjaGVja1Njcm9sbCIsImdldFNjcm9sbFN0YXRlIiwic3R1Y2tBdEJvdHRvbSIsInByZXZlbnRTaHJpbmtpbmciLCJ1cGRhdGVQcmV2ZW50U2hyaW5raW5nIiwiX3JlYWRSZWNlaXB0TWFwIiwiX3JlYWRSZWNlaXB0c0J5RXZlbnQiLCJfcmVhZFJlY2VpcHRzQnlVc2VySWQiLCJfc2hvd0hpZGRlbkV2ZW50c0luVGltZWxpbmUiLCJfaXNNb3VudGVkIiwiX3JlYWRNYXJrZXJOb2RlIiwiX3dob0lzVHlwaW5nIiwiX3Nob3dUeXBpbmdOb3RpZmljYXRpb25zV2F0Y2hlclJlZiIsIndhdGNoU2V0dGluZyIsIm9uU2hvd1R5cGluZ05vdGlmaWNhdGlvbnNDaGFuZ2UiLCJjb21wb25lbnREaWRNb3VudCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwidW53YXRjaFNldHRpbmciLCJjb21wb25lbnREaWRVcGRhdGUiLCJwcmV2UHJvcHMiLCJwcmV2U3RhdGUiLCJyZWFkTWFya2VyVmlzaWJsZSIsInByb3BzIiwicmVhZE1hcmtlckV2ZW50SWQiLCJwdXNoIiwiZ2V0Tm9kZUZvckV2ZW50SWQiLCJ1bmRlZmluZWQiLCJpc0F0Qm90dG9tIiwiZ2V0UmVhZE1hcmtlclBvc2l0aW9uIiwicmVhZE1hcmtlciIsIm1lc3NhZ2VXcmFwcGVyIiwid3JhcHBlclJlY3QiLCJSZWFjdERPTSIsImZpbmRET01Ob2RlIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwicmVhZE1hcmtlclJlY3QiLCJib3R0b20iLCJ0b3AiLCJzY3JvbGxUb1RvcCIsInNjcm9sbFRvQm90dG9tIiwic2Nyb2xsUmVsYXRpdmUiLCJtdWx0IiwiaGFuZGxlU2Nyb2xsS2V5Iiwic2Nyb2xsVG9FdmVudCIsInBpeGVsT2Zmc2V0Iiwib2Zmc2V0QmFzZSIsInNjcm9sbFRvVG9rZW4iLCJzY3JvbGxUb0V2ZW50SWZOZWVkZWQiLCJzY3JvbGxJbnRvVmlldyIsImJsb2NrIiwiYmVoYXZpb3IiLCJjaGVja0ZpbGxTdGF0ZSIsIl9pc1VubW91bnRpbmciLCJfc2hvdWxkU2hvd0V2ZW50IiwibXhFdiIsInNlbmRlciIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImlzVXNlcklnbm9yZWQiLCJ1c2VySWQiLCJoaWdobGlnaHRlZEV2ZW50SWQiLCJnZXRJZCIsIl9yZWFkTWFya2VyRm9yRXZlbnQiLCJpc0xhc3RFdmVudCIsInZpc2libGUiLCJociIsImluY2x1ZGVzIiwiX2NvbGxlY3RHaG9zdFJlYWRNYXJrZXIiLCJfb25HaG9zdFRyYW5zaXRpb25FbmQiLCJfZ2V0RXZlbnRUaWxlcyIsImkiLCJsYXN0U2hvd25FdmVudCIsImxhc3RTaG93bk5vbkxvY2FsRWNob0luZGV4IiwiZXZlbnRzIiwibGVuZ3RoIiwic3RhdHVzIiwicmV0IiwicHJldkV2ZW50Iiwic2hvd1JlYWRSZWNlaXB0cyIsIl9nZXRSZWFkUmVjZWlwdHNCeVNob3duRXZlbnQiLCJncm91cGVyIiwibGFzdCIsInNob3VsZEdyb3VwIiwiYWRkIiwiZ2V0VGlsZXMiLCJnZXROZXdQcmV2RXZlbnQiLCJHcm91cGVyIiwiZ3JvdXBlcnMiLCJjYW5TdGFydEdyb3VwIiwid2FudFRpbGUiLCJfZ2V0VGlsZXNGb3JFdmVudCIsIkV2ZW50VGlsZSIsInNkayIsImdldENvbXBvbmVudCIsIkRhdGVTZXBhcmF0b3IiLCJpc0VkaXRpbmciLCJlZGl0U3RhdGUiLCJnZXRFdmVudCIsImNvbnRpbnVhdGlvbiIsImV2ZW50VHlwZUNvbnRpbnVlcyIsImdldFRzIiwidHMxIiwiZXZlbnREYXRlIiwiZ2V0RGF0ZSIsIkRhdGUiLCJnZXRUaW1lIiwiX3dhbnRzRGF0ZVNlcGFyYXRvciIsImRhdGVTZXBhcmF0b3IiLCJoaWdobGlnaHQiLCJzY3JvbGxUb2tlbiIsInJlYWRSZWNlaXB0cyIsIl9jb2xsZWN0RXZlbnROb2RlIiwiYmluZCIsImlzUmVkYWN0ZWQiLCJyZXBsYWNpbmdFdmVudElkIiwiX29uSGVpZ2h0Q2hhbmdlZCIsInNob3dVcmxQcmV2aWV3IiwiZ2V0QXNzb2NpYXRlZFN0YXR1cyIsInRpbGVTaGFwZSIsImlzVHdlbHZlSG91ciIsInBlcm1hbGlua0NyZWF0b3IiLCJnZXRSZWxhdGlvbnNGb3JFdmVudCIsInNob3dSZWFjdGlvbnMiLCJuZXh0RXZlbnREYXRlIiwic3VwcHJlc3NGaXJzdERhdGVTZXBhcmF0b3IiLCJfZ2V0UmVhZFJlY2VpcHRzRm9yRXZlbnQiLCJldmVudCIsIm15VXNlcklkIiwiY3JlZGVudGlhbHMiLCJyb29tIiwicmVjZWlwdHMiLCJnZXRSZWNlaXB0c0ZvckV2ZW50IiwiZm9yRWFjaCIsInIiLCJ0eXBlIiwibWVtYmVyIiwiZ2V0TWVtYmVyIiwicm9vbU1lbWJlciIsInRzIiwiZGF0YSIsInJlY2VpcHRzQnlFdmVudCIsInJlY2VpcHRzQnlVc2VySWQiLCJsYXN0U2hvd25FdmVudElkIiwiZXhpc3RpbmdSZWNlaXB0cyIsIm5ld1JlY2VpcHRzIiwiY29uY2F0IiwicmVjZWlwdCIsInNvcnQiLCJyMSIsInIyIiwidXBkYXRlVGltZWxpbmVNaW5IZWlnaHQiLCJ3aG9Jc1R5cGluZyIsImlzVHlwaW5nVmlzaWJsZSIsImlzVmlzaWJsZSIsIm9uVGltZWxpbmVSZXNldCIsImNsZWFyUHJldmVudFNocmlua2luZyIsInJlbmRlciIsIlNjcm9sbFBhbmVsIiwiV2hvSXNUeXBpbmdUaWxlIiwiU3Bpbm5lciIsInRvcFNwaW5uZXIiLCJib3R0b21TcGlubmVyIiwiYmFja1BhZ2luYXRpbmciLCJmb3J3YXJkUGFnaW5hdGluZyIsImhpZGRlbiIsImRpc3BsYXkiLCJjbGFzc05hbWUiLCJhbHdheXNTaG93VGltZXN0YW1wcyIsIl9vblR5cGluZ1Nob3duIiwiX29uVHlwaW5nSGlkZGVuIiwib25TY3JvbGwiLCJvblJlc2l6ZSIsIm9uRmlsbFJlcXVlc3QiLCJvblVuZmlsbFJlcXVlc3QiLCJzdGlja3lCb3R0b20iLCJyZXNpemVOb3RpZmllciIsIlByb3BUeXBlcyIsImJvb2wiLCJhcnJheSIsImlzUmVxdWlyZWQiLCJzdHJpbmciLCJvYmplY3QiLCJvdXJVc2VySWQiLCJmdW5jIiwiQ3JlYXRpb25Hcm91cGVyIiwicGFuZWwiLCJjcmVhdGVFdmVudCIsImVqZWN0ZWRFdmVudHMiLCJnZXRTdGF0ZUtleSIsImdldFNlbmRlciIsImdldENvbnRlbnQiLCJpc1N0YXRlIiwiRXZlbnRMaXN0U3VtbWFyeSIsImVqZWN0ZWQiLCJldmVudFRpbGVzIiwibWFwIiwicmVkdWNlIiwiYSIsImIiLCJjcmVhdG9yIiwibmFtZSIsIk1lbWJlckdyb3VwZXIiLCJyZW5kZXJUZXh0IiwidHJpbSIsIk1lbWJlckV2ZW50TGlzdFN1bW1hcnkiLCJrZXkiLCJoaWdobGlnaHRJbk1lbHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFrQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBOUJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWdDQSxNQUFNQSx5QkFBeUIsR0FBRyxJQUFJLEVBQUosR0FBUyxJQUEzQyxDLENBQWlEOztBQUNqRCxNQUFNQyxjQUFjLEdBQUcsQ0FBQyxXQUFELEVBQWMsZ0JBQWQsQ0FBdkI7O0FBRUEsTUFBTUMsa0JBQWtCLEdBQUlDLENBQUQsSUFBT0EsQ0FBQyxDQUFDQyxPQUFGLE9BQWdCLGVBQWhCLElBQW1DRCxDQUFDLENBQUNDLE9BQUYsT0FBZ0IsMkJBQXJGO0FBRUE7Ozs7QUFFZSxNQUFNQyxZQUFOLFNBQTJCQyxlQUFNQyxTQUFqQyxDQUEyQztBQXdFdERDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUsMkVBaUZvQixNQUFNO0FBQ3BDLFdBQUtDLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSx1QkFBdUIsRUFBRUMsdUJBQWNDLFFBQWQsQ0FBdUIseUJBQXZCO0FBRGYsT0FBZDtBQUdILEtBckZhO0FBQUEsbUVBOFJhQyxJQUFELElBQVU7QUFDaEMsVUFBSUEsSUFBSixFQUFVO0FBQ047QUFDQUMsUUFBQUEscUJBQXFCLENBQUMsTUFBTTtBQUN4QkQsVUFBQUEsSUFBSSxDQUFDRSxLQUFMLENBQVdDLEtBQVgsR0FBbUIsS0FBbkI7QUFDQUgsVUFBQUEsSUFBSSxDQUFDRSxLQUFMLENBQVdFLE9BQVgsR0FBcUIsR0FBckI7QUFDSCxTQUhvQixDQUFyQjtBQUlIO0FBQ0osS0F0U2E7QUFBQSxpRUF3U1dDLEVBQUQsSUFBUTtBQUM1QjtBQUNBLFlBQU1DLGVBQWUsR0FBR0QsRUFBRSxDQUFDRSxNQUFILENBQVVDLE9BQVYsQ0FBa0JDLE9BQTFDO0FBQ0EsV0FBS2IsUUFBTCxDQUFjO0FBQ1ZjLFFBQUFBLGdCQUFnQixFQUFFLEtBQUtDLEtBQUwsQ0FBV0QsZ0JBQVgsQ0FBNEJFLE1BQTVCLENBQW1DQyxHQUFHLElBQUlBLEdBQUcsS0FBS1AsZUFBbEQ7QUFEUixPQUFkO0FBR0gsS0E5U2E7QUFBQSw2REE0a0JNLENBQUNRLE9BQUQsRUFBVWQsSUFBVixLQUFtQjtBQUNuQyxXQUFLZSxVQUFMLENBQWdCRCxPQUFoQixJQUEyQmQsSUFBM0I7QUFDSCxLQTlrQmE7QUFBQSw0REFrbEJLLE1BQU07QUFDckIsWUFBTWdCLFdBQVcsR0FBRyxLQUFLQyxZQUFMLENBQWtCQyxPQUF0Qzs7QUFDQSxVQUFJRixXQUFKLEVBQWlCO0FBQ2JBLFFBQUFBLFdBQVcsQ0FBQ0csV0FBWjtBQUNIO0FBQ0osS0F2bEJhO0FBQUEsMERBeWxCRyxNQUFNO0FBQ25CLFlBQU1ILFdBQVcsR0FBRyxLQUFLQyxZQUFMLENBQWtCQyxPQUF0QyxDQURtQixDQUVuQjs7QUFDQUYsTUFBQUEsV0FBVyxDQUFDRyxXQUFaOztBQUNBLFVBQUlILFdBQVcsSUFBSUEsV0FBVyxDQUFDSSxjQUFaLEdBQTZCQyxhQUFoRCxFQUErRDtBQUMzREwsUUFBQUEsV0FBVyxDQUFDTSxnQkFBWjtBQUNIO0FBQ0osS0FobUJhO0FBQUEsMkRBa21CSSxNQUFNO0FBQ3BCLFlBQU1OLFdBQVcsR0FBRyxLQUFLQyxZQUFMLENBQWtCQyxPQUF0Qzs7QUFDQSxVQUFJRixXQUFKLEVBQWlCO0FBQ2I7QUFDQTtBQUNBO0FBQ0FBLFFBQUFBLFdBQVcsQ0FBQ08sc0JBQVosR0FKYSxDQUtiO0FBQ0E7O0FBQ0FQLFFBQUFBLFdBQVcsQ0FBQ0csV0FBWjtBQUNIO0FBQ0osS0E3bUJhO0FBR1YsU0FBS1IsS0FBTCxHQUFhO0FBQ1Q7QUFDQTtBQUNBRCxNQUFBQSxnQkFBZ0IsRUFBRSxFQUhUO0FBSVRiLE1BQUFBLHVCQUF1QixFQUFFQyx1QkFBY0MsUUFBZCxDQUF1Qix5QkFBdkI7QUFKaEIsS0FBYixDQUhVLENBVVY7QUFDQTs7QUFDQSxTQUFLeUIsZUFBTCxHQUF1QixFQUF2QixDQVpVLENBY1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFNBQUtDLG9CQUFMLEdBQTRCLEVBQTVCLENBMUJVLENBNEJWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsU0FBS0MscUJBQUwsR0FBNkIsRUFBN0IsQ0E3Q1UsQ0ErQ1Y7QUFDQTs7QUFDQSxTQUFLQywyQkFBTCxHQUNJN0IsdUJBQWNDLFFBQWQsQ0FBdUIsNEJBQXZCLENBREo7QUFHQSxTQUFLNkIsVUFBTCxHQUFrQixLQUFsQjtBQUVBLFNBQUtDLGVBQUwsR0FBdUIsdUJBQXZCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQix1QkFBcEI7QUFDQSxTQUFLYixZQUFMLEdBQW9CLHVCQUFwQjtBQUVBLFNBQUtjLGtDQUFMLEdBQ0lqQyx1QkFBY2tDLFlBQWQsQ0FBMkIseUJBQTNCLEVBQXNELElBQXRELEVBQTRELEtBQUtDLCtCQUFqRSxDQURKO0FBRUg7O0FBRURDLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFNBQUtOLFVBQUwsR0FBa0IsSUFBbEI7QUFDSDs7QUFFRE8sRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsU0FBS1AsVUFBTCxHQUFrQixLQUFsQjs7QUFDQTlCLDJCQUFjc0MsY0FBZCxDQUE2QixLQUFLTCxrQ0FBbEM7QUFDSDs7QUFFRE0sRUFBQUEsa0JBQWtCLENBQUNDLFNBQUQsRUFBWUMsU0FBWixFQUF1QjtBQUNyQyxRQUFJRCxTQUFTLENBQUNFLGlCQUFWLElBQStCLEtBQUtDLEtBQUwsQ0FBV0MsaUJBQVgsS0FBaUNKLFNBQVMsQ0FBQ0ksaUJBQTlFLEVBQWlHO0FBQzdGLFlBQU1oQyxnQkFBZ0IsR0FBRyxLQUFLQyxLQUFMLENBQVdELGdCQUFwQztBQUNBQSxNQUFBQSxnQkFBZ0IsQ0FBQ2lDLElBQWpCLENBQXNCTCxTQUFTLENBQUNJLGlCQUFoQztBQUNBLFdBQUs5QyxRQUFMLENBQWM7QUFDVmMsUUFBQUE7QUFEVSxPQUFkO0FBR0g7QUFDSjs7QUFRRDtBQUNBa0MsRUFBQUEsaUJBQWlCLENBQUM5QixPQUFELEVBQVU7QUFDdkIsUUFBSSxDQUFDLEtBQUtDLFVBQVYsRUFBc0I7QUFDbEIsYUFBTzhCLFNBQVA7QUFDSDs7QUFFRCxXQUFPLEtBQUs5QixVQUFMLENBQWdCRCxPQUFoQixDQUFQO0FBQ0g7QUFFRDs7OztBQUVBZ0MsRUFBQUEsVUFBVSxHQUFHO0FBQ1QsV0FBTyxLQUFLN0IsWUFBTCxDQUFrQkMsT0FBbEIsSUFBNkIsS0FBS0QsWUFBTCxDQUFrQkMsT0FBbEIsQ0FBMEI0QixVQUExQixFQUFwQztBQUNIO0FBRUQ7Ozs7Ozs7QUFLQTFCLEVBQUFBLGNBQWMsR0FBRztBQUNiLFdBQU8sS0FBS0gsWUFBTCxDQUFrQkMsT0FBbEIsR0FBNEIsS0FBS0QsWUFBTCxDQUFrQkMsT0FBbEIsQ0FBMEJFLGNBQTFCLEVBQTVCLEdBQXlFLElBQWhGO0FBQ0gsR0FyTHFELENBdUx0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBMkIsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsVUFBTUMsVUFBVSxHQUFHLEtBQUtuQixlQUFMLENBQXFCWCxPQUF4QztBQUNBLFVBQU0rQixjQUFjLEdBQUcsS0FBS2hDLFlBQUwsQ0FBa0JDLE9BQXpDOztBQUVBLFFBQUksQ0FBQzhCLFVBQUQsSUFBZSxDQUFDQyxjQUFwQixFQUFvQztBQUNoQyxhQUFPLElBQVA7QUFDSDs7QUFFRCxVQUFNQyxXQUFXLEdBQUdDLGtCQUFTQyxXQUFULENBQXFCSCxjQUFyQixFQUFxQ0kscUJBQXJDLEVBQXBCOztBQUNBLFVBQU1DLGNBQWMsR0FBR04sVUFBVSxDQUFDSyxxQkFBWCxFQUF2QixDQVRvQixDQVdwQjtBQUNBOztBQUNBLFFBQUlDLGNBQWMsQ0FBQ0MsTUFBZixHQUF3QixDQUF4QixHQUE0QkwsV0FBVyxDQUFDTSxHQUE1QyxFQUFpRDtBQUM3QyxhQUFPLENBQUMsQ0FBUjtBQUNILEtBRkQsTUFFTyxJQUFJRixjQUFjLENBQUNFLEdBQWYsR0FBcUJOLFdBQVcsQ0FBQ0ssTUFBckMsRUFBNkM7QUFDaEQsYUFBTyxDQUFQO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsYUFBTyxDQUFQO0FBQ0g7QUFDSjtBQUVEOzs7O0FBRUFFLEVBQUFBLFdBQVcsR0FBRztBQUNWLFFBQUksS0FBS3hDLFlBQUwsQ0FBa0JDLE9BQXRCLEVBQStCO0FBQzNCLFdBQUtELFlBQUwsQ0FBa0JDLE9BQWxCLENBQTBCdUMsV0FBMUI7QUFDSDtBQUNKO0FBRUQ7Ozs7QUFFQUMsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsUUFBSSxLQUFLekMsWUFBTCxDQUFrQkMsT0FBdEIsRUFBK0I7QUFDM0IsV0FBS0QsWUFBTCxDQUFrQkMsT0FBbEIsQ0FBMEJ3QyxjQUExQjtBQUNIO0FBQ0o7QUFFRDs7Ozs7OztBQUtBQyxFQUFBQSxjQUFjLENBQUNDLElBQUQsRUFBTztBQUNqQixRQUFJLEtBQUszQyxZQUFMLENBQWtCQyxPQUF0QixFQUErQjtBQUMzQixXQUFLRCxZQUFMLENBQWtCQyxPQUFsQixDQUEwQnlDLGNBQTFCLENBQXlDQyxJQUF6QztBQUNIO0FBQ0o7QUFFRDs7Ozs7OztBQUtBQyxFQUFBQSxlQUFlLENBQUN4RCxFQUFELEVBQUs7QUFDaEIsUUFBSSxLQUFLWSxZQUFMLENBQWtCQyxPQUF0QixFQUErQjtBQUMzQixXQUFLRCxZQUFMLENBQWtCQyxPQUFsQixDQUEwQjJDLGVBQTFCLENBQTBDeEQsRUFBMUM7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVBeUQsRUFBQUEsYUFBYSxDQUFDaEQsT0FBRCxFQUFVaUQsV0FBVixFQUF1QkMsVUFBdkIsRUFBbUM7QUFDNUMsUUFBSSxLQUFLL0MsWUFBTCxDQUFrQkMsT0FBdEIsRUFBK0I7QUFDM0IsV0FBS0QsWUFBTCxDQUFrQkMsT0FBbEIsQ0FBMEIrQyxhQUExQixDQUF3Q25ELE9BQXhDLEVBQWlEaUQsV0FBakQsRUFBOERDLFVBQTlEO0FBQ0g7QUFDSjs7QUFFREUsRUFBQUEscUJBQXFCLENBQUNwRCxPQUFELEVBQVU7QUFDM0IsVUFBTWQsSUFBSSxHQUFHLEtBQUtlLFVBQUwsQ0FBZ0JELE9BQWhCLENBQWI7O0FBQ0EsUUFBSWQsSUFBSixFQUFVO0FBQ05BLE1BQUFBLElBQUksQ0FBQ21FLGNBQUwsQ0FBb0I7QUFBQ0MsUUFBQUEsS0FBSyxFQUFFLFNBQVI7QUFBbUJDLFFBQUFBLFFBQVEsRUFBRTtBQUE3QixPQUFwQjtBQUNIO0FBQ0o7QUFFRDs7OztBQUVBQyxFQUFBQSxjQUFjLEdBQUc7QUFDYixRQUFJLEtBQUtyRCxZQUFMLENBQWtCQyxPQUF0QixFQUErQjtBQUMzQixXQUFLRCxZQUFMLENBQWtCQyxPQUFsQixDQUEwQm9ELGNBQTFCO0FBQ0g7QUFDSjs7QUFFREMsRUFBQUEsYUFBYSxHQUFHO0FBQ1osV0FBTyxDQUFDLEtBQUszQyxVQUFiO0FBQ0gsR0ExUnFELENBNFJ0RDs7O0FBQ0E0QyxFQUFBQSxnQkFBZ0IsQ0FBQ0MsSUFBRCxFQUFPO0FBQ25CLFFBQUlBLElBQUksQ0FBQ0MsTUFBTCxJQUFlQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxhQUF0QixDQUFvQ0osSUFBSSxDQUFDQyxNQUFMLENBQVlJLE1BQWhELENBQW5CLEVBQTRFO0FBQ3hFLGFBQU8sS0FBUCxDQUR3RSxDQUMxRDtBQUNqQjs7QUFFRCxRQUFJLEtBQUtuRCwyQkFBVCxFQUFzQztBQUNsQyxhQUFPLElBQVA7QUFDSDs7QUFFRCxRQUFJLENBQUMsaUNBQWlCOEMsSUFBakIsQ0FBTCxFQUE2QjtBQUN6QixhQUFPLEtBQVAsQ0FEeUIsQ0FDWDtBQUNqQixLQVhrQixDQWFuQjs7O0FBQ0EsUUFBSSxLQUFLaEMsS0FBTCxDQUFXc0Msa0JBQVgsS0FBa0NOLElBQUksQ0FBQ08sS0FBTCxFQUF0QyxFQUFvRCxPQUFPLElBQVA7QUFFcEQsV0FBTyxDQUFDLDhCQUFnQlAsSUFBaEIsQ0FBUjtBQUNIOztBQUVEUSxFQUFBQSxtQkFBbUIsQ0FBQ25FLE9BQUQsRUFBVW9FLFdBQVYsRUFBdUI7QUFDdEMsVUFBTUMsT0FBTyxHQUFHLENBQUNELFdBQUQsSUFBZ0IsS0FBS3pDLEtBQUwsQ0FBV0QsaUJBQTNDOztBQUVBLFFBQUksS0FBS0MsS0FBTCxDQUFXQyxpQkFBWCxLQUFpQzVCLE9BQXJDLEVBQThDO0FBQzFDLFVBQUlzRSxFQUFKLENBRDBDLENBRTFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFJRCxPQUFKLEVBQWE7QUFDVEMsUUFBQUEsRUFBRSxHQUFHO0FBQUksVUFBQSxTQUFTLEVBQUMsMEJBQWQ7QUFDRCxVQUFBLEtBQUssRUFBRTtBQUFDaEYsWUFBQUEsT0FBTyxFQUFFLENBQVY7QUFBYUQsWUFBQUEsS0FBSyxFQUFFO0FBQXBCO0FBRE4sVUFBTDtBQUdIOztBQUVELGFBQ0k7QUFBSSxRQUFBLEdBQUcsRUFBRSxnQkFBY1csT0FBdkI7QUFBZ0MsUUFBQSxHQUFHLEVBQUUsS0FBS2UsZUFBMUM7QUFDTSxRQUFBLFNBQVMsRUFBQztBQURoQixTQUVNdUQsRUFGTixDQURKO0FBTUgsS0FwQkQsTUFvQk8sSUFBSSxLQUFLekUsS0FBTCxDQUFXRCxnQkFBWCxDQUE0QjJFLFFBQTVCLENBQXFDdkUsT0FBckMsQ0FBSixFQUFtRDtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQU1zRSxFQUFFLEdBQUc7QUFBSSxRQUFBLFNBQVMsRUFBQywwQkFBZDtBQUNQLFFBQUEsR0FBRyxFQUFFLEtBQUtFLHVCQURIO0FBRVAsUUFBQSxlQUFlLEVBQUUsS0FBS0MscUJBRmY7QUFHUCx3QkFBY3pFO0FBSFAsUUFBWCxDQVhzRCxDQWlCdEQ7QUFDQTtBQUNBOzs7QUFDQSxhQUNJO0FBQUksUUFBQSxHQUFHLEVBQUUsb0JBQWtCQSxPQUEzQjtBQUNNLFFBQUEsU0FBUyxFQUFDO0FBRGhCLFNBRU1zRSxFQUZOLENBREo7QUFNSDs7QUFFRCxXQUFPLElBQVA7QUFDSDs7QUFvQkRJLEVBQUFBLGNBQWMsR0FBRztBQUNiLFNBQUt6RSxVQUFMLEdBQWtCLEVBQWxCO0FBRUEsUUFBSTBFLENBQUosQ0FIYSxDQUtiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFJQyxjQUFKO0FBRUEsUUFBSUMsMEJBQTBCLEdBQUcsQ0FBQyxDQUFsQzs7QUFDQSxTQUFLRixDQUFDLEdBQUcsS0FBS2hELEtBQUwsQ0FBV21ELE1BQVgsQ0FBa0JDLE1BQWxCLEdBQXlCLENBQWxDLEVBQXFDSixDQUFDLElBQUksQ0FBMUMsRUFBNkNBLENBQUMsRUFBOUMsRUFBa0Q7QUFDOUMsWUFBTWhCLElBQUksR0FBRyxLQUFLaEMsS0FBTCxDQUFXbUQsTUFBWCxDQUFrQkgsQ0FBbEIsQ0FBYjs7QUFDQSxVQUFJLENBQUMsS0FBS2pCLGdCQUFMLENBQXNCQyxJQUF0QixDQUFMLEVBQWtDO0FBQzlCO0FBQ0g7O0FBRUQsVUFBSWlCLGNBQWMsS0FBSzdDLFNBQXZCLEVBQWtDO0FBQzlCNkMsUUFBQUEsY0FBYyxHQUFHakIsSUFBakI7QUFDSDs7QUFFRCxVQUFJQSxJQUFJLENBQUNxQixNQUFULEVBQWlCO0FBQ2I7QUFDQTtBQUNIOztBQUVESCxNQUFBQSwwQkFBMEIsR0FBR0YsQ0FBN0I7QUFDQTtBQUNIOztBQUVELFVBQU1NLEdBQUcsR0FBRyxFQUFaO0FBRUEsUUFBSUMsU0FBUyxHQUFHLElBQWhCLENBbkNhLENBbUNTOztBQUV0QixTQUFLdkUsb0JBQUwsR0FBNEIsRUFBNUI7O0FBQ0EsUUFBSSxLQUFLZ0IsS0FBTCxDQUFXd0QsZ0JBQWYsRUFBaUM7QUFDN0IsV0FBS3hFLG9CQUFMLEdBQTRCLEtBQUt5RSw0QkFBTCxFQUE1QjtBQUNIOztBQUVELFFBQUlDLE9BQU8sR0FBRyxJQUFkOztBQUVBLFNBQUtWLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBRyxLQUFLaEQsS0FBTCxDQUFXbUQsTUFBWCxDQUFrQkMsTUFBbEMsRUFBMENKLENBQUMsRUFBM0MsRUFBK0M7QUFDM0MsWUFBTWhCLElBQUksR0FBRyxLQUFLaEMsS0FBTCxDQUFXbUQsTUFBWCxDQUFrQkgsQ0FBbEIsQ0FBYjtBQUNBLFlBQU0zRSxPQUFPLEdBQUcyRCxJQUFJLENBQUNPLEtBQUwsRUFBaEI7QUFDQSxZQUFNb0IsSUFBSSxHQUFJM0IsSUFBSSxLQUFLaUIsY0FBdkI7O0FBRUEsVUFBSVMsT0FBSixFQUFhO0FBQ1QsWUFBSUEsT0FBTyxDQUFDRSxXQUFSLENBQW9CNUIsSUFBcEIsQ0FBSixFQUErQjtBQUMzQjBCLFVBQUFBLE9BQU8sQ0FBQ0csR0FBUixDQUFZN0IsSUFBWjtBQUNBO0FBQ0gsU0FIRCxNQUdPO0FBQ0g7QUFDQTtBQUNBc0IsVUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUFTLEdBQUd3RCxPQUFPLENBQUNJLFFBQVIsRUFBWjtBQUNBUCxVQUFBQSxTQUFTLEdBQUdHLE9BQU8sQ0FBQ0ssZUFBUixFQUFaO0FBQ0FMLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7QUFDSjs7QUFFRCxXQUFLLE1BQU1NLE9BQVgsSUFBc0JDLFFBQXRCLEVBQWdDO0FBQzVCLFlBQUlELE9BQU8sQ0FBQ0UsYUFBUixDQUFzQixJQUF0QixFQUE0QmxDLElBQTVCLENBQUosRUFBdUM7QUFDbkMwQixVQUFBQSxPQUFPLEdBQUcsSUFBSU0sT0FBSixDQUFZLElBQVosRUFBa0JoQyxJQUFsQixFQUF3QnVCLFNBQXhCLEVBQW1DTixjQUFuQyxDQUFWO0FBQ0g7QUFDSjs7QUFDRCxVQUFJLENBQUNTLE9BQUwsRUFBYztBQUNWLGNBQU1TLFFBQVEsR0FBRyxLQUFLcEMsZ0JBQUwsQ0FBc0JDLElBQXRCLENBQWpCOztBQUNBLFlBQUltQyxRQUFKLEVBQWM7QUFDVjtBQUNBO0FBQ0E7QUFDQWIsVUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUFTLEdBQUcsS0FBS2tFLGlCQUFMLENBQXVCYixTQUF2QixFQUFrQ3ZCLElBQWxDLEVBQXdDMkIsSUFBeEMsQ0FBWjtBQUNBSixVQUFBQSxTQUFTLEdBQUd2QixJQUFaO0FBQ0g7O0FBRUQsY0FBTXpCLFVBQVUsR0FBRyxLQUFLaUMsbUJBQUwsQ0FBeUJuRSxPQUF6QixFQUFrQzJFLENBQUMsSUFBSUUsMEJBQXZDLENBQW5COztBQUNBLFlBQUkzQyxVQUFKLEVBQWdCK0MsR0FBRyxDQUFDcEQsSUFBSixDQUFTSyxVQUFUO0FBQ25CO0FBQ0o7O0FBRUQsUUFBSW1ELE9BQUosRUFBYTtBQUNUSixNQUFBQSxHQUFHLENBQUNwRCxJQUFKLENBQVMsR0FBR3dELE9BQU8sQ0FBQ0ksUUFBUixFQUFaO0FBQ0g7O0FBRUQsV0FBT1IsR0FBUDtBQUNIOztBQUVEYyxFQUFBQSxpQkFBaUIsQ0FBQ2IsU0FBRCxFQUFZdkIsSUFBWixFQUFrQjJCLElBQWxCLEVBQXdCO0FBQ3JDLFVBQU1VLFNBQVMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlCQUFqQixDQUFsQjtBQUNBLFVBQU1DLGFBQWEsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF0QjtBQUNBLFVBQU1qQixHQUFHLEdBQUcsRUFBWjtBQUVBLFVBQU1tQixTQUFTLEdBQUcsS0FBS3pFLEtBQUwsQ0FBVzBFLFNBQVgsSUFDZCxLQUFLMUUsS0FBTCxDQUFXMEUsU0FBWCxDQUFxQkMsUUFBckIsR0FBZ0NwQyxLQUFoQyxPQUE0Q1AsSUFBSSxDQUFDTyxLQUFMLEVBRGhELENBTHFDLENBT3JDOztBQUNBLFFBQUlxQyxZQUFZLEdBQUcsS0FBbkIsQ0FScUMsQ0FVckM7QUFDQTs7QUFFQSxVQUFNQyxrQkFBa0IsR0FDcEJ0QixTQUFTLEtBQUssSUFBZCxJQUNBNUcsY0FBYyxDQUFDaUcsUUFBZixDQUF3QlosSUFBSSxDQUFDbEYsT0FBTCxFQUF4QixDQURBLElBRUFILGNBQWMsQ0FBQ2lHLFFBQWYsQ0FBd0JXLFNBQVMsQ0FBQ3pHLE9BQVYsRUFBeEIsQ0FISixDQWJxQyxDQWtCckM7QUFDQTs7QUFDQSxRQUFJeUcsU0FBUyxLQUFLLElBQWQsSUFBc0JBLFNBQVMsQ0FBQ3RCLE1BQWhDLElBQTBDRCxJQUFJLENBQUNDLE1BQS9DLElBQXlERCxJQUFJLENBQUNDLE1BQUwsQ0FBWUksTUFBWixLQUF1QmtCLFNBQVMsQ0FBQ3RCLE1BQVYsQ0FBaUJJLE1BQWpHLElBQ0E7QUFDQSxxQ0FBaUJrQixTQUFqQixDQUZBLEtBRWdDdkIsSUFBSSxDQUFDbEYsT0FBTCxPQUFtQnlHLFNBQVMsQ0FBQ3pHLE9BQVYsRUFBbkIsSUFBMEMrSCxrQkFGMUUsS0FHQzdDLElBQUksQ0FBQzhDLEtBQUwsS0FBZXZCLFNBQVMsQ0FBQ3VCLEtBQVYsRUFBZixJQUFvQ3BJLHlCQUh6QyxFQUdxRTtBQUNqRWtJLE1BQUFBLFlBQVksR0FBRyxJQUFmO0FBQ0g7QUFFVDs7Ozs7Ozs7Ozs7OztBQWNRO0FBQ0E7OztBQUNBLFFBQUlHLEdBQUcsR0FBRy9DLElBQUksQ0FBQzhDLEtBQUwsRUFBVjtBQUNBLFFBQUlFLFNBQVMsR0FBR2hELElBQUksQ0FBQ2lELE9BQUwsRUFBaEI7O0FBQ0EsUUFBSWpELElBQUksQ0FBQ3FCLE1BQVQsRUFBaUI7QUFDYjJCLE1BQUFBLFNBQVMsR0FBRyxJQUFJRSxJQUFKLEVBQVo7QUFDQUgsTUFBQUEsR0FBRyxHQUFHQyxTQUFTLENBQUNHLE9BQVYsRUFBTjtBQUNILEtBaERvQyxDQWtEckM7OztBQUNBLFFBQUksS0FBS0MsbUJBQUwsQ0FBeUI3QixTQUF6QixFQUFvQ3lCLFNBQXBDLENBQUosRUFBb0Q7QUFDaEQsWUFBTUssYUFBYSxHQUFHO0FBQUksUUFBQSxHQUFHLEVBQUVOO0FBQVQsU0FBYyw2QkFBQyxhQUFEO0FBQWUsUUFBQSxHQUFHLEVBQUVBLEdBQXBCO0FBQXlCLFFBQUEsRUFBRSxFQUFFQTtBQUE3QixRQUFkLENBQXRCOztBQUNBekIsTUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUFTbUYsYUFBVDtBQUNBVCxNQUFBQSxZQUFZLEdBQUcsS0FBZjtBQUNIOztBQUVELFVBQU12RyxPQUFPLEdBQUcyRCxJQUFJLENBQUNPLEtBQUwsRUFBaEI7QUFDQSxVQUFNK0MsU0FBUyxHQUFJakgsT0FBTyxLQUFLLEtBQUsyQixLQUFMLENBQVdzQyxrQkFBMUMsQ0ExRHFDLENBNERyQztBQUNBOztBQUNBLFVBQU1pRCxXQUFXLEdBQUd2RCxJQUFJLENBQUNxQixNQUFMLEdBQWNqRCxTQUFkLEdBQTBCL0IsT0FBOUM7QUFFQSxVQUFNbUgsWUFBWSxHQUFHLEtBQUt4RyxvQkFBTCxDQUEwQlgsT0FBMUIsQ0FBckIsQ0FoRXFDLENBa0VyQztBQUNBO0FBQ0E7QUFDQTs7QUFDQWlGLElBQUFBLEdBQUcsQ0FBQ3BELElBQUosQ0FDSTtBQUFJLE1BQUEsR0FBRyxFQUFFN0IsT0FBVDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUtvSCxpQkFBTCxDQUF1QkMsSUFBdkIsQ0FBNEIsSUFBNUIsRUFBa0NySCxPQUFsQyxDQURUO0FBRUksNEJBQW9Ca0g7QUFGeEIsT0FJSSw2QkFBQyxTQUFEO0FBQVcsTUFBQSxPQUFPLEVBQUV2RCxJQUFwQjtBQUNJLE1BQUEsWUFBWSxFQUFFNEMsWUFEbEI7QUFFSSxNQUFBLFVBQVUsRUFBRTVDLElBQUksQ0FBQzJELFVBQUwsRUFGaEI7QUFHSSxNQUFBLGdCQUFnQixFQUFFM0QsSUFBSSxDQUFDNEQsZ0JBQUwsRUFIdEI7QUFJSSxNQUFBLFNBQVMsRUFBRW5CLFNBQVMsSUFBSSxLQUFLekUsS0FBTCxDQUFXMEUsU0FKdkM7QUFLSSxNQUFBLGVBQWUsRUFBRSxLQUFLbUIsZ0JBTDFCO0FBTUksTUFBQSxZQUFZLEVBQUVMLFlBTmxCO0FBT0ksTUFBQSxjQUFjLEVBQUUsS0FBS3pHLGVBUHpCO0FBUUksTUFBQSxjQUFjLEVBQUUsS0FBS2lCLEtBQUwsQ0FBVzhGLGNBUi9CO0FBU0ksTUFBQSxlQUFlLEVBQUUsS0FBS2hFLGFBQUwsQ0FBbUI0RCxJQUFuQixDQUF3QixJQUF4QixDQVRyQjtBQVVJLE1BQUEsZUFBZSxFQUFFMUQsSUFBSSxDQUFDK0QsbUJBQUwsRUFWckI7QUFXSSxNQUFBLFNBQVMsRUFBRSxLQUFLL0YsS0FBTCxDQUFXZ0csU0FYMUI7QUFZSSxNQUFBLFlBQVksRUFBRSxLQUFLaEcsS0FBTCxDQUFXaUcsWUFaN0I7QUFhSSxNQUFBLGdCQUFnQixFQUFFLEtBQUtqRyxLQUFMLENBQVdrRyxnQkFiakM7QUFjSSxNQUFBLElBQUksRUFBRXZDLElBZFY7QUFlSSxNQUFBLGVBQWUsRUFBRTJCLFNBZnJCO0FBZ0JJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS3RGLEtBQUwsQ0FBV21HLG9CQWhCckM7QUFpQkksTUFBQSxhQUFhLEVBQUUsS0FBS25HLEtBQUwsQ0FBV29HO0FBakI5QixNQUpKLENBREo7QUEyQkEsV0FBTzlDLEdBQVA7QUFDSDs7QUFFRDhCLEVBQUFBLG1CQUFtQixDQUFDN0IsU0FBRCxFQUFZOEMsYUFBWixFQUEyQjtBQUMxQyxRQUFJOUMsU0FBUyxJQUFJLElBQWpCLEVBQXVCO0FBQ25CO0FBQ0E7QUFDQSxhQUFPLENBQUMsS0FBS3ZELEtBQUwsQ0FBV3NHLDBCQUFuQjtBQUNIOztBQUNELFdBQU8sbUNBQW1CL0MsU0FBUyxDQUFDMEIsT0FBVixFQUFuQixFQUF3Q29CLGFBQXhDLENBQVA7QUFDSCxHQTVqQnFELENBOGpCdEQ7QUFDQTs7O0FBQ0FFLEVBQUFBLHdCQUF3QixDQUFDQyxLQUFELEVBQVE7QUFDNUIsVUFBTUMsUUFBUSxHQUFHdkUsaUNBQWdCQyxHQUFoQixHQUFzQnVFLFdBQXRCLENBQWtDckUsTUFBbkQsQ0FENEIsQ0FHNUI7OztBQUNBLFVBQU07QUFBRXNFLE1BQUFBO0FBQUYsUUFBVyxLQUFLM0csS0FBdEI7O0FBQ0EsUUFBSSxDQUFDMkcsSUFBTCxFQUFXO0FBQ1AsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsVUFBTUMsUUFBUSxHQUFHLEVBQWpCO0FBQ0FELElBQUFBLElBQUksQ0FBQ0UsbUJBQUwsQ0FBeUJMLEtBQXpCLEVBQWdDTSxPQUFoQyxDQUF5Q0MsQ0FBRCxJQUFPO0FBQzNDLFVBQUksQ0FBQ0EsQ0FBQyxDQUFDMUUsTUFBSCxJQUFhMEUsQ0FBQyxDQUFDQyxJQUFGLEtBQVcsUUFBeEIsSUFBb0NELENBQUMsQ0FBQzFFLE1BQUYsS0FBYW9FLFFBQXJELEVBQStEO0FBQzNELGVBRDJELENBQ25EO0FBQ1g7O0FBQ0QsVUFBSXZFLGlDQUFnQkMsR0FBaEIsR0FBc0JDLGFBQXRCLENBQW9DMkUsQ0FBQyxDQUFDMUUsTUFBdEMsQ0FBSixFQUFtRDtBQUMvQyxlQUQrQyxDQUN2QztBQUNYOztBQUNELFlBQU00RSxNQUFNLEdBQUdOLElBQUksQ0FBQ08sU0FBTCxDQUFlSCxDQUFDLENBQUMxRSxNQUFqQixDQUFmO0FBQ0F1RSxNQUFBQSxRQUFRLENBQUMxRyxJQUFULENBQWM7QUFDVm1DLFFBQUFBLE1BQU0sRUFBRTBFLENBQUMsQ0FBQzFFLE1BREE7QUFFVjhFLFFBQUFBLFVBQVUsRUFBRUYsTUFGRjtBQUdWRyxRQUFBQSxFQUFFLEVBQUVMLENBQUMsQ0FBQ00sSUFBRixHQUFTTixDQUFDLENBQUNNLElBQUYsQ0FBT0QsRUFBaEIsR0FBcUI7QUFIZixPQUFkO0FBS0gsS0FiRDtBQWNBLFdBQU9SLFFBQVA7QUFDSCxHQXhsQnFELENBMGxCdEQ7QUFDQTtBQUNBOzs7QUFDQW5ELEVBQUFBLDRCQUE0QixHQUFHO0FBQzNCLFVBQU02RCxlQUFlLEdBQUcsRUFBeEI7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBRyxFQUF6QjtBQUVBLFFBQUlDLGdCQUFKOztBQUNBLFNBQUssTUFBTWhCLEtBQVgsSUFBb0IsS0FBS3hHLEtBQUwsQ0FBV21ELE1BQS9CLEVBQXVDO0FBQ25DLFVBQUksS0FBS3BCLGdCQUFMLENBQXNCeUUsS0FBdEIsQ0FBSixFQUFrQztBQUM5QmdCLFFBQUFBLGdCQUFnQixHQUFHaEIsS0FBSyxDQUFDakUsS0FBTixFQUFuQjtBQUNIOztBQUNELFVBQUksQ0FBQ2lGLGdCQUFMLEVBQXVCO0FBQ25CO0FBQ0g7O0FBRUQsWUFBTUMsZ0JBQWdCLEdBQUdILGVBQWUsQ0FBQ0UsZ0JBQUQsQ0FBZixJQUFxQyxFQUE5RDs7QUFDQSxZQUFNRSxXQUFXLEdBQUcsS0FBS25CLHdCQUFMLENBQThCQyxLQUE5QixDQUFwQjs7QUFDQWMsTUFBQUEsZUFBZSxDQUFDRSxnQkFBRCxDQUFmLEdBQW9DQyxnQkFBZ0IsQ0FBQ0UsTUFBakIsQ0FBd0JELFdBQXhCLENBQXBDLENBVm1DLENBWW5DO0FBQ0E7O0FBQ0EsV0FBSyxNQUFNRSxPQUFYLElBQXNCRixXQUF0QixFQUFtQztBQUMvQkgsUUFBQUEsZ0JBQWdCLENBQUNLLE9BQU8sQ0FBQ3ZGLE1BQVQsQ0FBaEIsR0FBbUM7QUFDL0JtRixVQUFBQSxnQkFEK0I7QUFFL0JJLFVBQUFBO0FBRitCLFNBQW5DO0FBSUg7QUFDSixLQXpCMEIsQ0EyQjNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBSyxNQUFNdkYsTUFBWCxJQUFxQixLQUFLcEQscUJBQTFCLEVBQWlEO0FBQzdDLFVBQUlzSSxnQkFBZ0IsQ0FBQ2xGLE1BQUQsQ0FBcEIsRUFBOEI7QUFDMUI7QUFDSDs7QUFDRCxZQUFNO0FBQUVtRixRQUFBQSxnQkFBRjtBQUFvQkksUUFBQUE7QUFBcEIsVUFBZ0MsS0FBSzNJLHFCQUFMLENBQTJCb0QsTUFBM0IsQ0FBdEM7QUFDQSxZQUFNb0YsZ0JBQWdCLEdBQUdILGVBQWUsQ0FBQ0UsZ0JBQUQsQ0FBZixJQUFxQyxFQUE5RDtBQUNBRixNQUFBQSxlQUFlLENBQUNFLGdCQUFELENBQWYsR0FBb0NDLGdCQUFnQixDQUFDRSxNQUFqQixDQUF3QkMsT0FBeEIsQ0FBcEM7QUFDQUwsTUFBQUEsZ0JBQWdCLENBQUNsRixNQUFELENBQWhCLEdBQTJCO0FBQUVtRixRQUFBQSxnQkFBRjtBQUFvQkksUUFBQUE7QUFBcEIsT0FBM0I7QUFDSDs7QUFDRCxTQUFLM0kscUJBQUwsR0FBNkJzSSxnQkFBN0IsQ0ExQzJCLENBNEMzQjtBQUNBOztBQUNBLFNBQUssTUFBTWxKLE9BQVgsSUFBc0JpSixlQUF0QixFQUF1QztBQUNuQ0EsTUFBQUEsZUFBZSxDQUFDakosT0FBRCxDQUFmLENBQXlCd0osSUFBekIsQ0FBOEIsQ0FBQ0MsRUFBRCxFQUFLQyxFQUFMLEtBQVk7QUFDdEMsZUFBT0EsRUFBRSxDQUFDWCxFQUFILEdBQVFVLEVBQUUsQ0FBQ1YsRUFBbEI7QUFDSCxPQUZEO0FBR0g7O0FBRUQsV0FBT0UsZUFBUDtBQUNIOztBQXFDRFUsRUFBQUEsdUJBQXVCLEdBQUc7QUFDdEIsVUFBTXpKLFdBQVcsR0FBRyxLQUFLQyxZQUFMLENBQWtCQyxPQUF0Qzs7QUFFQSxRQUFJRixXQUFKLEVBQWlCO0FBQ2IsWUFBTThCLFVBQVUsR0FBRzlCLFdBQVcsQ0FBQzhCLFVBQVosRUFBbkI7QUFDQSxZQUFNNEgsV0FBVyxHQUFHLEtBQUs1SSxZQUFMLENBQWtCWixPQUF0QztBQUNBLFlBQU15SixlQUFlLEdBQUdELFdBQVcsSUFBSUEsV0FBVyxDQUFDRSxTQUFaLEVBQXZDLENBSGEsQ0FJYjtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFJOUgsVUFBVSxJQUFJNkgsZUFBbEIsRUFBbUM7QUFDL0IzSixRQUFBQSxXQUFXLENBQUNNLGdCQUFaO0FBQ0g7QUFDSjtBQUNKOztBQUVEdUosRUFBQUEsZUFBZSxHQUFHO0FBQ2QsVUFBTTdKLFdBQVcsR0FBRyxLQUFLQyxZQUFMLENBQWtCQyxPQUF0Qzs7QUFDQSxRQUFJRixXQUFKLEVBQWlCO0FBQ2JBLE1BQUFBLFdBQVcsQ0FBQzhKLHFCQUFaO0FBQ0g7QUFDSjs7QUFFREMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsV0FBVyxHQUFHakUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUFwQjtBQUNBLFVBQU1pRSxlQUFlLEdBQUdsRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUJBQWpCLENBQXhCO0FBQ0EsVUFBTWtFLE9BQU8sR0FBR25FLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxRQUFJbUUsVUFBSjtBQUNBLFFBQUlDLGFBQUo7O0FBQ0EsUUFBSSxLQUFLM0ksS0FBTCxDQUFXNEksY0FBZixFQUErQjtBQUMzQkYsTUFBQUEsVUFBVSxHQUFHO0FBQUksUUFBQSxHQUFHLEVBQUM7QUFBUixTQUFzQiw2QkFBQyxPQUFELE9BQXRCLENBQWI7QUFDSDs7QUFDRCxRQUFJLEtBQUsxSSxLQUFMLENBQVc2SSxpQkFBZixFQUFrQztBQUM5QkYsTUFBQUEsYUFBYSxHQUFHO0FBQUksUUFBQSxHQUFHLEVBQUM7QUFBUixTQUF5Qiw2QkFBQyxPQUFELE9BQXpCLENBQWhCO0FBQ0g7O0FBRUQsVUFBTWxMLEtBQUssR0FBRyxLQUFLdUMsS0FBTCxDQUFXOEksTUFBWCxHQUFvQjtBQUFFQyxNQUFBQSxPQUFPLEVBQUU7QUFBWCxLQUFwQixHQUEwQyxFQUF4RDtBQUVBLFVBQU1DLFNBQVMsR0FBRyx5QkFDZCxLQUFLaEosS0FBTCxDQUFXZ0osU0FERyxFQUVkO0FBQ0ksOENBQXdDLEtBQUtoSixLQUFMLENBQVdpSjtBQUR2RCxLQUZjLENBQWxCO0FBT0EsUUFBSWhCLFdBQUo7O0FBQ0EsUUFBSSxLQUFLakksS0FBTCxDQUFXMkcsSUFBWCxJQUFtQixDQUFDLEtBQUszRyxLQUFMLENBQVdnRyxTQUEvQixJQUE0QyxLQUFLOUgsS0FBTCxDQUFXZCx1QkFBM0QsRUFBb0Y7QUFDaEY2SyxNQUFBQSxXQUFXLEdBQUksNkJBQUMsZUFBRDtBQUNYLFFBQUEsSUFBSSxFQUFFLEtBQUtqSSxLQUFMLENBQVcyRyxJQUROO0FBRVgsUUFBQSxPQUFPLEVBQUUsS0FBS3VDLGNBRkg7QUFHWCxRQUFBLFFBQVEsRUFBRSxLQUFLQyxlQUhKO0FBSVgsUUFBQSxHQUFHLEVBQUUsS0FBSzlKO0FBSkMsUUFBZjtBQU1IOztBQUVELFdBQ0ksNkJBQUMsV0FBRDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUtiLFlBRGQ7QUFFSSxNQUFBLFNBQVMsRUFBRXdLLFNBRmY7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLaEosS0FBTCxDQUFXb0osUUFIekI7QUFJSSxNQUFBLFFBQVEsRUFBRSxLQUFLQyxRQUpuQjtBQUtJLE1BQUEsYUFBYSxFQUFFLEtBQUtySixLQUFMLENBQVdzSixhQUw5QjtBQU1JLE1BQUEsZUFBZSxFQUFFLEtBQUt0SixLQUFMLENBQVd1SixlQU5oQztBQU9JLE1BQUEsS0FBSyxFQUFFOUwsS0FQWDtBQVFJLE1BQUEsWUFBWSxFQUFFLEtBQUt1QyxLQUFMLENBQVd3SixZQVI3QjtBQVNJLE1BQUEsY0FBYyxFQUFFLEtBQUt4SixLQUFMLENBQVd5SjtBQVQvQixPQVdNZixVQVhOLEVBWU0sS0FBSzNGLGNBQUwsRUFaTixFQWFNa0YsV0FiTixFQWNNVSxhQWROLENBREo7QUFrQkg7O0FBandCcUQ7QUFvd0IxRDs7Ozs7Ozs7Ozs7QUFZQTtBQUNBO0FBQ0E7Ozs7OEJBbHhCcUI1TCxZLGVBQ0U7QUFDZjtBQUNBK0wsRUFBQUEsTUFBTSxFQUFFWSxtQkFBVUMsSUFGSDtBQUlmO0FBQ0E7QUFDQWYsRUFBQUEsY0FBYyxFQUFFYyxtQkFBVUMsSUFOWDtBQVFmO0FBQ0E7QUFDQWQsRUFBQUEsaUJBQWlCLEVBQUVhLG1CQUFVQyxJQVZkO0FBWWY7QUFDQXhHLEVBQUFBLE1BQU0sRUFBRXVHLG1CQUFVRSxLQUFWLENBQWdCQyxVQWJUO0FBZWY7QUFDQXZILEVBQUFBLGtCQUFrQixFQUFFb0gsbUJBQVVJLE1BaEJmO0FBa0JmO0FBQ0E7QUFDQW5ELEVBQUFBLElBQUksRUFBRStDLG1CQUFVSyxNQXBCRDtBQXNCZjtBQUNBakUsRUFBQUEsY0FBYyxFQUFFNEQsbUJBQVVDLElBdkJYO0FBeUJmO0FBQ0ExSixFQUFBQSxpQkFBaUIsRUFBRXlKLG1CQUFVSSxNQTFCZDtBQTRCZjtBQUNBL0osRUFBQUEsaUJBQWlCLEVBQUUySixtQkFBVUMsSUE3QmQ7QUErQmY7QUFDQTtBQUNBSyxFQUFBQSxTQUFTLEVBQUVOLG1CQUFVSSxNQWpDTjtBQW1DZjtBQUNBeEQsRUFBQUEsMEJBQTBCLEVBQUVvRCxtQkFBVUMsSUFwQ3ZCO0FBc0NmO0FBQ0FuRyxFQUFBQSxnQkFBZ0IsRUFBRWtHLG1CQUFVQyxJQXZDYjtBQXlDZjtBQUNBO0FBQ0E7QUFDQUgsRUFBQUEsWUFBWSxFQUFFRSxtQkFBVUMsSUE1Q1Q7QUE4Q2Y7QUFDQVAsRUFBQUEsUUFBUSxFQUFFTSxtQkFBVU8sSUEvQ0w7QUFpRGY7QUFDQVgsRUFBQUEsYUFBYSxFQUFFSSxtQkFBVU8sSUFsRFY7QUFvRGY7QUFDQWpCLEVBQUFBLFNBQVMsRUFBRVUsbUJBQVVJLE1BQVYsQ0FBaUJELFVBckRiO0FBdURmO0FBQ0E3RCxFQUFBQSxTQUFTLEVBQUUwRCxtQkFBVUksTUF4RE47QUEwRGY7QUFDQTdELEVBQUFBLFlBQVksRUFBRXlELG1CQUFVQyxJQTNEVDtBQTZEZjtBQUNBVixFQUFBQSxvQkFBb0IsRUFBRVMsbUJBQVVDLElBOURqQjtBQWdFZjtBQUNBeEQsRUFBQUEsb0JBQW9CLEVBQUV1RCxtQkFBVU8sSUFqRWpCO0FBbUVmO0FBQ0E3RCxFQUFBQSxhQUFhLEVBQUVzRCxtQkFBVUM7QUFwRVYsQzs7QUFreEJ2QixNQUFNTyxlQUFOLENBQXNCO0FBS2xCaE4sRUFBQUEsV0FBVyxDQUFDaU4sS0FBRCxFQUFRQyxXQUFSLEVBQXFCN0csU0FBckIsRUFBZ0NOLGNBQWhDLEVBQWdEO0FBQ3ZELFNBQUtrSCxLQUFMLEdBQWFBLEtBQWI7QUFDQSxTQUFLQyxXQUFMLEdBQW1CQSxXQUFuQjtBQUNBLFNBQUs3RyxTQUFMLEdBQWlCQSxTQUFqQjtBQUNBLFNBQUtOLGNBQUwsR0FBc0JBLGNBQXRCO0FBQ0EsU0FBS0UsTUFBTCxHQUFjLEVBQWQsQ0FMdUQsQ0FNdkQ7QUFDQTs7QUFDQSxTQUFLa0gsYUFBTCxHQUFxQixFQUFyQjtBQUNBLFNBQUs5SixVQUFMLEdBQWtCNEosS0FBSyxDQUFDM0gsbUJBQU4sQ0FDZDRILFdBQVcsQ0FBQzdILEtBQVosRUFEYyxFQUVkNkgsV0FBVyxLQUFLbkgsY0FGRixDQUFsQjtBQUlIOztBQUVEVyxFQUFBQSxXQUFXLENBQUNoRyxFQUFELEVBQUs7QUFDWixVQUFNdU0sS0FBSyxHQUFHLEtBQUtBLEtBQW5CO0FBQ0EsVUFBTUMsV0FBVyxHQUFHLEtBQUtBLFdBQXpCOztBQUNBLFFBQUksQ0FBQ0QsS0FBSyxDQUFDcEksZ0JBQU4sQ0FBdUJuRSxFQUF2QixDQUFMLEVBQWlDO0FBQzdCLGFBQU8sSUFBUDtBQUNIOztBQUNELFFBQUl1TSxLQUFLLENBQUMvRSxtQkFBTixDQUEwQixLQUFLZ0YsV0FBL0IsRUFBNEN4TSxFQUFFLENBQUNxSCxPQUFILEVBQTVDLENBQUosRUFBK0Q7QUFDM0QsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsUUFBSXJILEVBQUUsQ0FBQ2QsT0FBSCxPQUFpQixlQUFqQixLQUNJYyxFQUFFLENBQUMwTSxXQUFILE9BQXFCRixXQUFXLENBQUNHLFNBQVosRUFBckIsSUFBZ0QzTSxFQUFFLENBQUM0TSxVQUFILEdBQWdCLFlBQWhCLE1BQWtDLE1BRHRGLENBQUosRUFDbUc7QUFDL0YsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsUUFBSTVNLEVBQUUsQ0FBQzZNLE9BQUgsTUFBZ0I3TSxFQUFFLENBQUMyTSxTQUFILE9BQW1CSCxXQUFXLENBQUNHLFNBQVosRUFBdkMsRUFBZ0U7QUFDNUQsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsV0FBTyxLQUFQO0FBQ0g7O0FBRUQxRyxFQUFBQSxHQUFHLENBQUNqRyxFQUFELEVBQUs7QUFDSixVQUFNdU0sS0FBSyxHQUFHLEtBQUtBLEtBQW5CO0FBQ0EsU0FBSzVKLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxJQUFtQjRKLEtBQUssQ0FBQzNILG1CQUFOLENBQ2pDNUUsRUFBRSxDQUFDMkUsS0FBSCxFQURpQyxFQUVqQzNFLEVBQUUsS0FBSyxLQUFLcUYsY0FGcUIsQ0FBckM7O0FBSUEsUUFBSSxDQUFDa0gsS0FBSyxDQUFDcEksZ0JBQU4sQ0FBdUJuRSxFQUF2QixDQUFMLEVBQWlDO0FBQzdCO0FBQ0g7O0FBQ0QsUUFBSUEsRUFBRSxDQUFDZCxPQUFILE9BQWlCLG1CQUFyQixFQUEwQztBQUN0QyxXQUFLdU4sYUFBTCxDQUFtQm5LLElBQW5CLENBQXdCdEMsRUFBeEI7QUFDSCxLQUZELE1BRU87QUFDSCxXQUFLdUYsTUFBTCxDQUFZakQsSUFBWixDQUFpQnRDLEVBQWpCO0FBQ0g7QUFDSjs7QUFFRGtHLEVBQUFBLFFBQVEsR0FBRztBQUNQO0FBQ0E7QUFDQTtBQUNBLFFBQUksQ0FBQyxLQUFLWCxNQUFOLElBQWdCLENBQUMsS0FBS0EsTUFBTCxDQUFZQyxNQUFqQyxFQUF5QyxPQUFPLEVBQVA7QUFFekMsVUFBTW9CLGFBQWEsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF0QjtBQUNBLFVBQU1tRyxnQkFBZ0IsR0FBR3BHLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQ0FBakIsQ0FBekI7QUFFQSxVQUFNNEYsS0FBSyxHQUFHLEtBQUtBLEtBQW5CO0FBQ0EsVUFBTTdHLEdBQUcsR0FBRyxFQUFaO0FBQ0EsVUFBTThHLFdBQVcsR0FBRyxLQUFLQSxXQUF6QjtBQUNBLFVBQU1uSCxjQUFjLEdBQUcsS0FBS0EsY0FBNUI7O0FBRUEsUUFBSWtILEtBQUssQ0FBQy9FLG1CQUFOLENBQTBCLEtBQUs3QixTQUEvQixFQUEwQzZHLFdBQVcsQ0FBQ25GLE9BQVosRUFBMUMsQ0FBSixFQUFzRTtBQUNsRSxZQUFNbUMsRUFBRSxHQUFHZ0QsV0FBVyxDQUFDdEYsS0FBWixFQUFYO0FBQ0F4QixNQUFBQSxHQUFHLENBQUNwRCxJQUFKLENBQ0k7QUFBSSxRQUFBLEdBQUcsRUFBRWtILEVBQUUsR0FBQztBQUFaLFNBQWlCLDZCQUFDLGFBQUQ7QUFBZSxRQUFBLEdBQUcsRUFBRUEsRUFBRSxHQUFDLEdBQXZCO0FBQTRCLFFBQUEsRUFBRSxFQUFFQTtBQUFoQyxRQUFqQixDQURKO0FBR0gsS0FuQk0sQ0FxQlA7OztBQUNBLFFBQUkrQyxLQUFLLENBQUNwSSxnQkFBTixDQUF1QnFJLFdBQXZCLENBQUosRUFBeUM7QUFDckM7QUFDQTlHLE1BQUFBLEdBQUcsQ0FBQ3BELElBQUosQ0FBUyxHQUFHaUssS0FBSyxDQUFDL0YsaUJBQU4sQ0FBd0JnRyxXQUF4QixFQUFxQ0EsV0FBckMsRUFBa0QsS0FBbEQsQ0FBWjtBQUNIOztBQUVELFNBQUssTUFBTU8sT0FBWCxJQUFzQixLQUFLTixhQUEzQixFQUEwQztBQUN0Qy9HLE1BQUFBLEdBQUcsQ0FBQ3BELElBQUosQ0FBUyxHQUFHaUssS0FBSyxDQUFDL0YsaUJBQU4sQ0FDUmdHLFdBRFEsRUFDS08sT0FETCxFQUNjUCxXQUFXLEtBQUtuSCxjQUQ5QixDQUFaO0FBR0g7O0FBRUQsVUFBTTJILFVBQVUsR0FBRyxLQUFLekgsTUFBTCxDQUFZMEgsR0FBWixDQUFpQmhPLENBQUQsSUFBTztBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQU9zTixLQUFLLENBQUMvRixpQkFBTixDQUF3QnZILENBQXhCLEVBQTJCQSxDQUEzQixFQUE4QkEsQ0FBQyxLQUFLb0csY0FBcEMsQ0FBUDtBQUNILEtBTmtCLEVBTWhCNkgsTUFOZ0IsQ0FNVCxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUQsQ0FBQyxDQUFDcEQsTUFBRixDQUFTcUQsQ0FBVCxDQU5ELEVBTWMsRUFOZCxDQUFuQixDQWpDTyxDQXdDUDs7QUFDQSxVQUFNcE4sRUFBRSxHQUFHLEtBQUt1RixNQUFMLENBQVksS0FBS0EsTUFBTCxDQUFZQyxNQUFaLEdBQXFCLENBQWpDLENBQVg7QUFDQUUsSUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUNJLDZCQUFDLGdCQUFEO0FBQ0ssTUFBQSxHQUFHLEVBQUMscUJBRFQ7QUFFSyxNQUFBLE1BQU0sRUFBRSxLQUFLaUQsTUFGbEI7QUFHSyxNQUFBLFFBQVEsRUFBRWdILEtBQUssQ0FBQ3RFLGdCQUhyQixDQUd1QztBQUh2QztBQUlLLE1BQUEsY0FBYyxFQUFFLENBQUNqSSxFQUFFLENBQUNxRSxNQUFKLENBSnJCO0FBS0ssTUFBQSxXQUFXLEVBQUUseUJBQUcsOENBQUgsRUFBbUQ7QUFDNURnSixRQUFBQSxPQUFPLEVBQUVyTixFQUFFLENBQUNxRSxNQUFILEdBQVlyRSxFQUFFLENBQUNxRSxNQUFILENBQVVpSixJQUF0QixHQUE2QnROLEVBQUUsQ0FBQzJNLFNBQUg7QUFEc0IsT0FBbkQ7QUFMbEIsT0FTT0ssVUFUUCxDQURKOztBQWNBLFFBQUksS0FBS3JLLFVBQVQsRUFBcUI7QUFDakIrQyxNQUFBQSxHQUFHLENBQUNwRCxJQUFKLENBQVMsS0FBS0ssVUFBZDtBQUNIOztBQUVELFdBQU8rQyxHQUFQO0FBQ0g7O0FBRURTLEVBQUFBLGVBQWUsR0FBRztBQUNkLFdBQU8sS0FBS3FHLFdBQVo7QUFDSDs7QUF4SGlCLEMsQ0EySHRCOzs7OEJBM0hNRixlLG1CQUNxQixVQUFTQyxLQUFULEVBQWdCdk0sRUFBaEIsRUFBb0I7QUFDdkMsU0FBT0EsRUFBRSxDQUFDZCxPQUFILE9BQWlCLGVBQXhCO0FBQ0gsQzs7QUF5SEwsTUFBTXFPLGFBQU4sQ0FBb0I7QUFLaEJqTyxFQUFBQSxXQUFXLENBQUNpTixLQUFELEVBQVF2TSxFQUFSLEVBQVkyRixTQUFaLEVBQXVCTixjQUF2QixFQUF1QztBQUM5QyxTQUFLa0gsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsU0FBSzVKLFVBQUwsR0FBa0I0SixLQUFLLENBQUMzSCxtQkFBTixDQUNkNUUsRUFBRSxDQUFDMkUsS0FBSCxFQURjLEVBRWQzRSxFQUFFLEtBQUtxRixjQUZPLENBQWxCO0FBSUEsU0FBS0UsTUFBTCxHQUFjLENBQUN2RixFQUFELENBQWQ7QUFDQSxTQUFLMkYsU0FBTCxHQUFpQkEsU0FBakI7QUFDQSxTQUFLTixjQUFMLEdBQXNCQSxjQUF0QjtBQUNIOztBQUVEVyxFQUFBQSxXQUFXLENBQUNoRyxFQUFELEVBQUs7QUFDWixRQUFJLEtBQUt1TSxLQUFMLENBQVcvRSxtQkFBWCxDQUErQixLQUFLakMsTUFBTCxDQUFZLENBQVosQ0FBL0IsRUFBK0N2RixFQUFFLENBQUNxSCxPQUFILEVBQS9DLENBQUosRUFBa0U7QUFDOUQsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsV0FBT3JJLGtCQUFrQixDQUFDZ0IsRUFBRCxDQUF6QjtBQUNIOztBQUVEaUcsRUFBQUEsR0FBRyxDQUFDakcsRUFBRCxFQUFLO0FBQ0osUUFBSUEsRUFBRSxDQUFDZCxPQUFILE9BQWlCLGVBQXJCLEVBQXNDO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBLFlBQU1zTyxVQUFVLEdBQUcsZ0NBQWF4TixFQUFiLENBQW5CO0FBQ0EsVUFBSSxDQUFDd04sVUFBRCxJQUFlQSxVQUFVLENBQUNDLElBQVgsR0FBa0JqSSxNQUFsQixLQUE2QixDQUFoRCxFQUFtRCxPQUxqQixDQUt5QjtBQUM5RDs7QUFDRCxTQUFLN0MsVUFBTCxHQUFrQixLQUFLQSxVQUFMLElBQW1CLEtBQUs0SixLQUFMLENBQVczSCxtQkFBWCxDQUNqQzVFLEVBQUUsQ0FBQzJFLEtBQUgsRUFEaUMsRUFFakMzRSxFQUFFLEtBQUssS0FBS3FGLGNBRnFCLENBQXJDO0FBSUEsU0FBS0UsTUFBTCxDQUFZakQsSUFBWixDQUFpQnRDLEVBQWpCO0FBQ0g7O0FBRURrRyxFQUFBQSxRQUFRLEdBQUc7QUFDUDtBQUNBO0FBQ0E7QUFDQSxRQUFJLENBQUMsS0FBS1gsTUFBTixJQUFnQixDQUFDLEtBQUtBLE1BQUwsQ0FBWUMsTUFBakMsRUFBeUMsT0FBTyxFQUFQO0FBRXpDLFVBQU1vQixhQUFhLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7QUFDQSxVQUFNK0csc0JBQXNCLEdBQUdoSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUNBQWpCLENBQS9CO0FBRUEsVUFBTTRGLEtBQUssR0FBRyxLQUFLQSxLQUFuQjtBQUNBLFVBQU1sSCxjQUFjLEdBQUcsS0FBS0EsY0FBNUI7QUFDQSxVQUFNSyxHQUFHLEdBQUcsRUFBWjs7QUFFQSxRQUFJNkcsS0FBSyxDQUFDL0UsbUJBQU4sQ0FBMEIsS0FBSzdCLFNBQS9CLEVBQTBDLEtBQUtKLE1BQUwsQ0FBWSxDQUFaLEVBQWU4QixPQUFmLEVBQTFDLENBQUosRUFBeUU7QUFDckUsWUFBTW1DLEVBQUUsR0FBRyxLQUFLakUsTUFBTCxDQUFZLENBQVosRUFBZTJCLEtBQWYsRUFBWDtBQUNBeEIsTUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUNJO0FBQUksUUFBQSxHQUFHLEVBQUVrSCxFQUFFLEdBQUM7QUFBWixTQUFpQiw2QkFBQyxhQUFEO0FBQWUsUUFBQSxHQUFHLEVBQUVBLEVBQUUsR0FBQyxHQUF2QjtBQUE0QixRQUFBLEVBQUUsRUFBRUE7QUFBaEMsUUFBakIsQ0FESjtBQUdILEtBbEJNLENBb0JQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFVBQU1tRSxHQUFHLEdBQUcsNkJBQ1IsS0FBS2hJLFNBQUwsR0FBaUIsS0FBS0osTUFBTCxDQUFZLENBQVosRUFBZVosS0FBZixFQUFqQixHQUEwQyxTQURsQyxDQUFaO0FBSUEsUUFBSWlKLGVBQUo7QUFDQSxRQUFJWixVQUFVLEdBQUcsS0FBS3pILE1BQUwsQ0FBWTBILEdBQVosQ0FBaUJoTyxDQUFELElBQU87QUFDcEMsVUFBSUEsQ0FBQyxDQUFDMEYsS0FBRixPQUFjNEgsS0FBSyxDQUFDbkssS0FBTixDQUFZc0Msa0JBQTlCLEVBQWtEO0FBQzlDa0osUUFBQUEsZUFBZSxHQUFHLElBQWxCO0FBQ0gsT0FIbUMsQ0FJcEM7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLGFBQU9yQixLQUFLLENBQUMvRixpQkFBTixDQUF3QnZILENBQXhCLEVBQTJCQSxDQUEzQixFQUE4QkEsQ0FBQyxLQUFLb0csY0FBcEMsQ0FBUDtBQUNILEtBVGdCLEVBU2Q2SCxNQVRjLENBU1AsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVVELENBQUMsQ0FBQ3BELE1BQUYsQ0FBU3FELENBQVQsQ0FUSCxFQVNnQixFQVRoQixDQUFqQjs7QUFXQSxRQUFJSixVQUFVLENBQUN4SCxNQUFYLEtBQXNCLENBQTFCLEVBQTZCO0FBQ3pCd0gsTUFBQUEsVUFBVSxHQUFHLElBQWI7QUFDSDs7QUFFRHRILElBQUFBLEdBQUcsQ0FBQ3BELElBQUosQ0FDSSw2QkFBQyxzQkFBRDtBQUF3QixNQUFBLEdBQUcsRUFBRXFMLEdBQTdCO0FBQ0ssTUFBQSxNQUFNLEVBQUUsS0FBS3BJLE1BRGxCO0FBRUssTUFBQSxRQUFRLEVBQUVnSCxLQUFLLENBQUN0RSxnQkFGckIsQ0FFdUM7QUFGdkM7QUFHSyxNQUFBLGFBQWEsRUFBRTJGO0FBSHBCLE9BS09aLFVBTFAsQ0FESjs7QUFVQSxRQUFJLEtBQUtySyxVQUFULEVBQXFCO0FBQ2pCK0MsTUFBQUEsR0FBRyxDQUFDcEQsSUFBSixDQUFTLEtBQUtLLFVBQWQ7QUFDSDs7QUFFRCxXQUFPK0MsR0FBUDtBQUNIOztBQUVEUyxFQUFBQSxlQUFlLEdBQUc7QUFDZCxXQUFPLEtBQUtaLE1BQUwsQ0FBWSxDQUFaLENBQVA7QUFDSDs7QUF6R2UsQyxDQTRHcEI7Ozs4QkE1R01nSSxhLG1CQUNxQixVQUFTaEIsS0FBVCxFQUFnQnZNLEVBQWhCLEVBQW9CO0FBQ3ZDLFNBQU91TSxLQUFLLENBQUNwSSxnQkFBTixDQUF1Qm5FLEVBQXZCLEtBQThCaEIsa0JBQWtCLENBQUNnQixFQUFELENBQXZEO0FBQ0gsQztBQTBHTCxNQUFNcUcsUUFBUSxHQUFHLENBQUNpRyxlQUFELEVBQWtCaUIsYUFBbEIsQ0FBakIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCwge2NyZWF0ZVJlZn0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCBzaG91bGRIaWRlRXZlbnQgZnJvbSAnLi4vLi4vc2hvdWxkSGlkZUV2ZW50JztcclxuaW1wb3J0IHt3YW50c0RhdGVTZXBhcmF0b3J9IGZyb20gJy4uLy4uL0RhdGVVdGlscyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi9pbmRleCc7XHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSAnLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZSc7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IHtoYXZlVGlsZUZvckV2ZW50fSBmcm9tIFwiLi4vdmlld3Mvcm9vbXMvRXZlbnRUaWxlXCI7XHJcbmltcG9ydCB7dGV4dEZvckV2ZW50fSBmcm9tIFwiLi4vLi4vVGV4dEZvckV2ZW50XCI7XHJcblxyXG5jb25zdCBDT05USU5VQVRJT05fTUFYX0lOVEVSVkFMID0gNSAqIDYwICogMTAwMDsgLy8gNSBtaW51dGVzXHJcbmNvbnN0IGNvbnRpbnVlZFR5cGVzID0gWydtLnN0aWNrZXInLCAnbS5yb29tLm1lc3NhZ2UnXTtcclxuXHJcbmNvbnN0IGlzTWVtYmVyc2hpcENoYW5nZSA9IChlKSA9PiBlLmdldFR5cGUoKSA9PT0gJ20ucm9vbS5tZW1iZXInIHx8IGUuZ2V0VHlwZSgpID09PSAnbS5yb29tLnRoaXJkX3BhcnR5X2ludml0ZSc7XHJcblxyXG4vKiAoYWxtb3N0KSBzdGF0ZWxlc3MgVUkgY29tcG9uZW50IHdoaWNoIGJ1aWxkcyB0aGUgZXZlbnQgdGlsZXMgaW4gdGhlIHJvb20gdGltZWxpbmUuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNZXNzYWdlUGFuZWwgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyB0cnVlIHRvIGdpdmUgdGhlIGNvbXBvbmVudCBhICdkaXNwbGF5OiBub25lJyBzdHlsZS5cclxuICAgICAgICBoaWRkZW46IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyB0cnVlIHRvIHNob3cgYSBzcGlubmVyIGF0IHRoZSB0b3Agb2YgdGhlIHRpbWVsaW5lIHRvIGluZGljYXRlXHJcbiAgICAgICAgLy8gYmFjay1wYWdpbmF0aW9uIGluIHByb2dyZXNzXHJcbiAgICAgICAgYmFja1BhZ2luYXRpbmc6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyB0cnVlIHRvIHNob3cgYSBzcGlubmVyIGF0IHRoZSBlbmQgb2YgdGhlIHRpbWVsaW5lIHRvIGluZGljYXRlXHJcbiAgICAgICAgLy8gZm9yd2FyZC1wYWdpbmF0aW9uIGluIHByb2dyZXNzXHJcbiAgICAgICAgZm9yd2FyZFBhZ2luYXRpbmc6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyB0aGUgbGlzdCBvZiBNYXRyaXhFdmVudHMgdG8gZGlzcGxheVxyXG4gICAgICAgIGV2ZW50czogUHJvcFR5cGVzLmFycmF5LmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIC8vIElEIG9mIGFuIGV2ZW50IHRvIGhpZ2hsaWdodC4gSWYgdW5kZWZpbmVkLCBubyBldmVudCB3aWxsIGJlIGhpZ2hsaWdodGVkLlxyXG4gICAgICAgIGhpZ2hsaWdodGVkRXZlbnRJZDogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLy8gVGhlIHJvb20gdGhlc2UgZXZlbnRzIGFyZSBhbGwgaW4gdG9nZXRoZXIsIGlmIGFueS5cclxuICAgICAgICAvLyAoVGhlIG5vdGlmaWNhdGlvbiBwYW5lbCB3b24ndCBoYXZlIGEgcm9vbSBoZXJlLCBmb3IgZXhhbXBsZS4pXHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdCxcclxuXHJcbiAgICAgICAgLy8gU2hvdWxkIHdlIHNob3cgVVJMIFByZXZpZXdzXHJcbiAgICAgICAgc2hvd1VybFByZXZpZXc6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyBldmVudCBhZnRlciB3aGljaCB3ZSBzaG91bGQgc2hvdyBhIHJlYWQgbWFya2VyXHJcbiAgICAgICAgcmVhZE1hcmtlckV2ZW50SWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8vIHdoZXRoZXIgdGhlIHJlYWQgbWFya2VyIHNob3VsZCBiZSB2aXNpYmxlXHJcbiAgICAgICAgcmVhZE1hcmtlclZpc2libGU6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyB0aGUgdXNlcmlkIG9mIG91ciB1c2VyLiBUaGlzIGlzIHVzZWQgdG8gc3VwcHJlc3MgdGhlIHJlYWQgbWFya2VyXHJcbiAgICAgICAgLy8gZm9yIHBlbmRpbmcgbWVzc2FnZXMuXHJcbiAgICAgICAgb3VyVXNlcklkOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyB0cnVlIHRvIHN1cHByZXNzIHRoZSBkYXRlIGF0IHRoZSBzdGFydCBvZiB0aGUgdGltZWxpbmVcclxuICAgICAgICBzdXBwcmVzc0ZpcnN0RGF0ZVNlcGFyYXRvcjogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIHdoZXRoZXIgdG8gc2hvdyByZWFkIHJlY2VpcHRzXHJcbiAgICAgICAgc2hvd1JlYWRSZWNlaXB0czogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIHRydWUgaWYgdXBkYXRlcyB0byB0aGUgZXZlbnQgbGlzdCBzaG91bGQgY2F1c2UgdGhlIHNjcm9sbCBwYW5lbCB0b1xyXG4gICAgICAgIC8vIHNjcm9sbCBkb3duIHdoZW4gd2UgYXJlIGF0IHRoZSBib3R0b20gb2YgdGhlIHdpbmRvdy4gU2VlIFNjcm9sbFBhbmVsXHJcbiAgICAgICAgLy8gZm9yIG1vcmUgZGV0YWlscy5cclxuICAgICAgICBzdGlja3lCb3R0b206IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyBjYWxsYmFjayB3aGljaCBpcyBjYWxsZWQgd2hlbiB0aGUgcGFuZWwgaXMgc2Nyb2xsZWQuXHJcbiAgICAgICAgb25TY3JvbGw6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBjYWxsYmFjayB3aGljaCBpcyBjYWxsZWQgd2hlbiBtb3JlIGNvbnRlbnQgaXMgbmVlZGVkLlxyXG4gICAgICAgIG9uRmlsbFJlcXVlc3Q6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBjbGFzc05hbWUgZm9yIHRoZSBwYW5lbFxyXG4gICAgICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICAvLyBzaGFwZSBwYXJhbWV0ZXIgdG8gYmUgcGFzc2VkIHRvIEV2ZW50VGlsZXNcclxuICAgICAgICB0aWxlU2hhcGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8vIHNob3cgdHdlbHZlIGhvdXIgdGltZXN0YW1wc1xyXG4gICAgICAgIGlzVHdlbHZlSG91cjogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIHNob3cgdGltZXN0YW1wcyBhbHdheXNcclxuICAgICAgICBhbHdheXNTaG93VGltZXN0YW1wczogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIGhlbHBlciBmdW5jdGlvbiB0byBhY2Nlc3MgcmVsYXRpb25zIGZvciBhbiBldmVudFxyXG4gICAgICAgIGdldFJlbGF0aW9uc0ZvckV2ZW50OiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gd2hldGhlciB0byBzaG93IHJlYWN0aW9ucyBmb3IgYW4gZXZlbnRcclxuICAgICAgICBzaG93UmVhY3Rpb25zOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgLy8gcHJldmlvdXMgcG9zaXRpb25zIHRoZSByZWFkIG1hcmtlciBoYXMgYmVlbiBpbiwgc28gd2UgY2FuXHJcbiAgICAgICAgICAgIC8vIGRpc3BsYXkgJ2dob3N0JyByZWFkIG1hcmtlcnMgdGhhdCBhcmUgYW5pbWF0aW5nIGF3YXlcclxuICAgICAgICAgICAgZ2hvc3RSZWFkTWFya2VyczogW10sXHJcbiAgICAgICAgICAgIHNob3dUeXBpbmdOb3RpZmljYXRpb25zOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwic2hvd1R5cGluZ05vdGlmaWNhdGlvbnNcIiksXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gb3BhcXVlIHJlYWRyZWNlaXB0IGluZm8gZm9yIGVhY2ggdXNlcklkOyB1c2VkIGJ5IFJlYWRSZWNlaXB0TWFya2VyXHJcbiAgICAgICAgLy8gdG8gbWFuYWdlIGl0cyBhbmltYXRpb25zXHJcbiAgICAgICAgdGhpcy5fcmVhZFJlY2VpcHRNYXAgPSB7fTtcclxuXHJcbiAgICAgICAgLy8gVHJhY2sgcmVhZCByZWNlaXB0cyBieSBldmVudCBJRC4gRm9yIGVhY2ggX3Nob3duXyBldmVudCBJRCwgd2Ugc3RvcmVcclxuICAgICAgICAvLyB0aGUgbGlzdCBvZiByZWFkIHJlY2VpcHRzIHRvIGRpc3BsYXk6XHJcbiAgICAgICAgLy8gICBbXHJcbiAgICAgICAgLy8gICAgICAge1xyXG4gICAgICAgIC8vICAgICAgICAgICB1c2VySWQ6IHN0cmluZyxcclxuICAgICAgICAvLyAgICAgICAgICAgbWVtYmVyOiBSb29tTWVtYmVyLFxyXG4gICAgICAgIC8vICAgICAgICAgICB0czogbnVtYmVyLFxyXG4gICAgICAgIC8vICAgICAgIH0sXHJcbiAgICAgICAgLy8gICBdXHJcbiAgICAgICAgLy8gVGhpcyBpcyByZWNvbXB1dGVkIG9uIGVhY2ggcmVuZGVyLiBJdCdzIG9ubHkgc3RvcmVkIG9uIHRoZSBjb21wb25lbnRcclxuICAgICAgICAvLyBmb3IgZWFzZSBvZiBwYXNzaW5nIHRoZSBkYXRhIGFyb3VuZCBzaW5jZSBpdCdzIGNvbXB1dGVkIGluIG9uZSBwYXNzXHJcbiAgICAgICAgLy8gb3ZlciBhbGwgZXZlbnRzLlxyXG4gICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0c0J5RXZlbnQgPSB7fTtcclxuXHJcbiAgICAgICAgLy8gVHJhY2sgcmVhZCByZWNlaXB0cyBieSB1c2VyIElELiBGb3IgZWFjaCB1c2VyIElEIHdlJ3ZlIGV2ZXIgc2hvd24gYVxyXG4gICAgICAgIC8vIGEgcmVhZCByZWNlaXB0IGZvciwgd2Ugc3RvcmUgYW4gb2JqZWN0OlxyXG4gICAgICAgIC8vICAge1xyXG4gICAgICAgIC8vICAgICAgIGxhc3RTaG93bkV2ZW50SWQ6IHN0cmluZyxcclxuICAgICAgICAvLyAgICAgICByZWNlaXB0OiB7XHJcbiAgICAgICAgLy8gICAgICAgICAgIHVzZXJJZDogc3RyaW5nLFxyXG4gICAgICAgIC8vICAgICAgICAgICBtZW1iZXI6IFJvb21NZW1iZXIsXHJcbiAgICAgICAgLy8gICAgICAgICAgIHRzOiBudW1iZXIsXHJcbiAgICAgICAgLy8gICAgICAgfSxcclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyBzbyB0aGF0IHdlIGNhbiBhbHdheXMga2VlcCByZWNlaXB0cyBkaXNwbGF5ZWQgYnkgcmV2ZXJ0aW5nIGJhY2sgdG9cclxuICAgICAgICAvLyB0aGUgbGFzdCBzaG93biBldmVudCBmb3IgdGhhdCB1c2VyIElEIHdoZW4gbmVlZGVkLiBUaGlzIG1heSBmZWVsIGxpa2VcclxuICAgICAgICAvLyBpdCBkdXBsaWNhdGVzIHRoZSByZWNlaXB0IHN0b3JhZ2UgaW4gdGhlIHJvb20sIGJ1dCBhdCB0aGlzIGxheWVyLCB3ZVxyXG4gICAgICAgIC8vIGFyZSB0cmFja2luZyBfc2hvd25fIGV2ZW50IElEcywgd2hpY2ggdGhlIEpTIFNESyBrbm93cyBub3RoaW5nIGFib3V0LlxyXG4gICAgICAgIC8vIFRoaXMgaXMgcmVjb21wdXRlZCBvbiBlYWNoIHJlbmRlciwgdXNpbmcgdGhlIGRhdGEgZnJvbSB0aGUgcHJldmlvdXNcclxuICAgICAgICAvLyByZW5kZXIgYXMgb3VyIGZhbGxiYWNrIGZvciBhbnkgdXNlciBJRHMgd2UgY2FuJ3QgbWF0Y2ggYSByZWNlaXB0IHRvIGFcclxuICAgICAgICAvLyBkaXNwbGF5ZWQgZXZlbnQgaW4gdGhlIGN1cnJlbnQgcmVuZGVyIGN5Y2xlLlxyXG4gICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0c0J5VXNlcklkID0ge307XHJcblxyXG4gICAgICAgIC8vIENhY2hlIGhpZGRlbiBldmVudHMgc2V0dGluZyBvbiBtb3VudCBzaW5jZSBTZXR0aW5ncyBpcyBleHBlbnNpdmUgdG9cclxuICAgICAgICAvLyBxdWVyeSwgYW5kIHdlIGNoZWNrIHRoaXMgaW4gYSBob3QgY29kZSBwYXRoLlxyXG4gICAgICAgIHRoaXMuX3Nob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lID1cclxuICAgICAgICAgICAgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lXCIpO1xyXG5cclxuICAgICAgICB0aGlzLl9pc01vdW50ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy5fcmVhZE1hcmtlck5vZGUgPSBjcmVhdGVSZWYoKTtcclxuICAgICAgICB0aGlzLl93aG9Jc1R5cGluZyA9IGNyZWF0ZVJlZigpO1xyXG4gICAgICAgIHRoaXMuX3Njcm9sbFBhbmVsID0gY3JlYXRlUmVmKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX3Nob3dUeXBpbmdOb3RpZmljYXRpb25zV2F0Y2hlclJlZiA9XHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUud2F0Y2hTZXR0aW5nKFwic2hvd1R5cGluZ05vdGlmaWNhdGlvbnNcIiwgbnVsbCwgdGhpcy5vblNob3dUeXBpbmdOb3RpZmljYXRpb25zQ2hhbmdlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICB0aGlzLl9pc01vdW50ZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIHRoaXMuX2lzTW91bnRlZCA9IGZhbHNlO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUudW53YXRjaFNldHRpbmcodGhpcy5fc2hvd1R5cGluZ05vdGlmaWNhdGlvbnNXYXRjaGVyUmVmKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzLCBwcmV2U3RhdGUpIHtcclxuICAgICAgICBpZiAocHJldlByb3BzLnJlYWRNYXJrZXJWaXNpYmxlICYmIHRoaXMucHJvcHMucmVhZE1hcmtlckV2ZW50SWQgIT09IHByZXZQcm9wcy5yZWFkTWFya2VyRXZlbnRJZCkge1xyXG4gICAgICAgICAgICBjb25zdCBnaG9zdFJlYWRNYXJrZXJzID0gdGhpcy5zdGF0ZS5naG9zdFJlYWRNYXJrZXJzO1xyXG4gICAgICAgICAgICBnaG9zdFJlYWRNYXJrZXJzLnB1c2gocHJldlByb3BzLnJlYWRNYXJrZXJFdmVudElkKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBnaG9zdFJlYWRNYXJrZXJzLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25TaG93VHlwaW5nTm90aWZpY2F0aW9uc0NoYW5nZSA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2hvd1R5cGluZ05vdGlmaWNhdGlvbnM6IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJzaG93VHlwaW5nTm90aWZpY2F0aW9uc1wiKSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgLyogZ2V0IHRoZSBET00gbm9kZSByZXByZXNlbnRpbmcgdGhlIGdpdmVuIGV2ZW50ICovXHJcbiAgICBnZXROb2RlRm9yRXZlbnRJZChldmVudElkKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmV2ZW50Tm9kZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmV2ZW50Tm9kZXNbZXZlbnRJZF07XHJcbiAgICB9XHJcblxyXG4gICAgLyogcmV0dXJuIHRydWUgaWYgdGhlIGNvbnRlbnQgaXMgZnVsbHkgc2Nyb2xsZWQgZG93biByaWdodCBub3c7IGVsc2UgZmFsc2UuXHJcbiAgICAgKi9cclxuICAgIGlzQXRCb3R0b20oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQgJiYgdGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudC5pc0F0Qm90dG9tKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogZ2V0IHRoZSBjdXJyZW50IHNjcm9sbCBzdGF0ZS4gU2VlIFNjcm9sbFBhbmVsLmdldFNjcm9sbFN0YXRlIGZvclxyXG4gICAgICogZGV0YWlscy5cclxuICAgICAqXHJcbiAgICAgKiByZXR1cm5zIG51bGwgaWYgd2UgYXJlIG5vdCBtb3VudGVkLlxyXG4gICAgICovXHJcbiAgICBnZXRTY3JvbGxTdGF0ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudCA/IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQuZ2V0U2Nyb2xsU3RhdGUoKSA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gcmV0dXJucyBvbmUgb2Y6XHJcbiAgICAvL1xyXG4gICAgLy8gIG51bGw6IHRoZXJlIGlzIG5vIHJlYWQgbWFya2VyXHJcbiAgICAvLyAgLTE6IHJlYWQgbWFya2VyIGlzIGFib3ZlIHRoZSB3aW5kb3dcclxuICAgIC8vICAgMDogcmVhZCBtYXJrZXIgaXMgd2l0aGluIHRoZSB3aW5kb3dcclxuICAgIC8vICArMTogcmVhZCBtYXJrZXIgaXMgYmVsb3cgdGhlIHdpbmRvd1xyXG4gICAgZ2V0UmVhZE1hcmtlclBvc2l0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJlYWRNYXJrZXIgPSB0aGlzLl9yZWFkTWFya2VyTm9kZS5jdXJyZW50O1xyXG4gICAgICAgIGNvbnN0IG1lc3NhZ2VXcmFwcGVyID0gdGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudDtcclxuXHJcbiAgICAgICAgaWYgKCFyZWFkTWFya2VyIHx8ICFtZXNzYWdlV3JhcHBlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHdyYXBwZXJSZWN0ID0gUmVhY3RET00uZmluZERPTU5vZGUobWVzc2FnZVdyYXBwZXIpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICAgIGNvbnN0IHJlYWRNYXJrZXJSZWN0ID0gcmVhZE1hcmtlci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuXHJcbiAgICAgICAgLy8gdGhlIHJlYWQtbWFya2VyIHByZXRlbmRzIHRvIGhhdmUgemVybyBoZWlnaHQgd2hlbiBpdCBpcyBhY3R1YWxseVxyXG4gICAgICAgIC8vIHR3byBwaXhlbHMgaGlnaDsgKzIgaGVyZSB0byBhY2NvdW50IGZvciB0aGF0LlxyXG4gICAgICAgIGlmIChyZWFkTWFya2VyUmVjdC5ib3R0b20gKyAyIDwgd3JhcHBlclJlY3QudG9wKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAtMTtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlYWRNYXJrZXJSZWN0LnRvcCA8IHdyYXBwZXJSZWN0LmJvdHRvbSkge1xyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gMTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoganVtcCB0byB0aGUgdG9wIG9mIHRoZSBjb250ZW50LlxyXG4gICAgICovXHJcbiAgICBzY3JvbGxUb1RvcCgpIHtcclxuICAgICAgICBpZiAodGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxQYW5lbC5jdXJyZW50LnNjcm9sbFRvVG9wKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qIGp1bXAgdG8gdGhlIGJvdHRvbSBvZiB0aGUgY29udGVudC5cclxuICAgICAqL1xyXG4gICAgc2Nyb2xsVG9Cb3R0b20oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudC5zY3JvbGxUb0JvdHRvbSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhZ2UgdXAvZG93bi5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gbXVsdDogLTEgdG8gcGFnZSB1cCwgKzEgdG8gcGFnZSBkb3duXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFJlbGF0aXZlKG11bHQpIHtcclxuICAgICAgICBpZiAodGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxQYW5lbC5jdXJyZW50LnNjcm9sbFJlbGF0aXZlKG11bHQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNjcm9sbCB1cC9kb3duIGluIHJlc3BvbnNlIHRvIGEgc2Nyb2xsIGtleVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7S2V5Ym9hcmRFdmVudH0gZXY6IHRoZSBrZXlib2FyZCBldmVudCB0byBoYW5kbGVcclxuICAgICAqL1xyXG4gICAgaGFuZGxlU2Nyb2xsS2V5KGV2KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudC5oYW5kbGVTY3JvbGxLZXkoZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKiBqdW1wIHRvIHRoZSBnaXZlbiBldmVudCBpZC5cclxuICAgICAqXHJcbiAgICAgKiBvZmZzZXRCYXNlIGdpdmVzIHRoZSByZWZlcmVuY2UgcG9pbnQgZm9yIHRoZSBwaXhlbE9mZnNldC4gMCBtZWFucyB0aGVcclxuICAgICAqIHRvcCBvZiB0aGUgY29udGFpbmVyLCAxIG1lYW5zIHRoZSBib3R0b20sIGFuZCBmcmFjdGlvbmFsIHZhbHVlcyBtZWFuXHJcbiAgICAgKiBzb21ld2hlcmUgaW4gdGhlIG1pZGRsZS4gSWYgb21pdHRlZCwgaXQgZGVmYXVsdHMgdG8gMC5cclxuICAgICAqXHJcbiAgICAgKiBwaXhlbE9mZnNldCBnaXZlcyB0aGUgbnVtYmVyIG9mIHBpeGVscyAqYWJvdmUqIHRoZSBvZmZzZXRCYXNlIHRoYXQgdGhlXHJcbiAgICAgKiBub2RlIChzcGVjaWZpY2FsbHksIHRoZSBib3R0b20gb2YgaXQpIHdpbGwgYmUgcG9zaXRpb25lZC4gSWYgb21pdHRlZCwgaXRcclxuICAgICAqIGRlZmF1bHRzIHRvIDAuXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFRvRXZlbnQoZXZlbnRJZCwgcGl4ZWxPZmZzZXQsIG9mZnNldEJhc2UpIHtcclxuICAgICAgICBpZiAodGhpcy5fc2Nyb2xsUGFuZWwuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxQYW5lbC5jdXJyZW50LnNjcm9sbFRvVG9rZW4oZXZlbnRJZCwgcGl4ZWxPZmZzZXQsIG9mZnNldEJhc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY3JvbGxUb0V2ZW50SWZOZWVkZWQoZXZlbnRJZCkge1xyXG4gICAgICAgIGNvbnN0IG5vZGUgPSB0aGlzLmV2ZW50Tm9kZXNbZXZlbnRJZF07XHJcbiAgICAgICAgaWYgKG5vZGUpIHtcclxuICAgICAgICAgICAgbm9kZS5zY3JvbGxJbnRvVmlldyh7YmxvY2s6IFwibmVhcmVzdFwiLCBiZWhhdmlvcjogXCJpbnN0YW50XCJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyogY2hlY2sgdGhlIHNjcm9sbCBzdGF0ZSBhbmQgc2VuZCBvdXQgcGFnaW5hdGlvbiByZXF1ZXN0cyBpZiBuZWNlc3NhcnkuXHJcbiAgICAgKi9cclxuICAgIGNoZWNrRmlsbFN0YXRlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zY3JvbGxQYW5lbC5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQuY2hlY2tGaWxsU3RhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX2lzVW5tb3VudGluZygpIHtcclxuICAgICAgICByZXR1cm4gIXRoaXMuX2lzTW91bnRlZDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBJbXBsZW1lbnQgZ3JhbnVsYXIgKHBlci1yb29tKSBoaWRlIG9wdGlvbnNcclxuICAgIF9zaG91bGRTaG93RXZlbnQobXhFdikge1xyXG4gICAgICAgIGlmIChteEV2LnNlbmRlciAmJiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNVc2VySWdub3JlZChteEV2LnNlbmRlci51c2VySWQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTsgLy8gaWdub3JlZCA9IG5vIHNob3cgKG9ubHkgaGFwcGVucyBpZiB0aGUgaWdub3JlIGhhcHBlbnMgYWZ0ZXIgYW4gZXZlbnQgd2FzIHJlY2VpdmVkKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX3Nob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFoYXZlVGlsZUZvckV2ZW50KG14RXYpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTsgLy8gbm8gdGlsZSA9IG5vIHNob3dcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEFsd2F5cyBzaG93IGhpZ2hsaWdodGVkIGV2ZW50XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuaGlnaGxpZ2h0ZWRFdmVudElkID09PSBteEV2LmdldElkKCkpIHJldHVybiB0cnVlO1xyXG5cclxuICAgICAgICByZXR1cm4gIXNob3VsZEhpZGVFdmVudChteEV2KTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVhZE1hcmtlckZvckV2ZW50KGV2ZW50SWQsIGlzTGFzdEV2ZW50KSB7XHJcbiAgICAgICAgY29uc3QgdmlzaWJsZSA9ICFpc0xhc3RFdmVudCAmJiB0aGlzLnByb3BzLnJlYWRNYXJrZXJWaXNpYmxlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5yZWFkTWFya2VyRXZlbnRJZCA9PT0gZXZlbnRJZCkge1xyXG4gICAgICAgICAgICBsZXQgaHI7XHJcbiAgICAgICAgICAgIC8vIGlmIHRoZSByZWFkIG1hcmtlciBjb21lcyBhdCB0aGUgZW5kIG9mIHRoZSB0aW1lbGluZSAoZXhjZXB0XHJcbiAgICAgICAgICAgIC8vIGZvciBsb2NhbCBlY2hvZXMsIHdoaWNoIGFyZSBleGNsdWRlZCBmcm9tIFJNcywgYmVjYXVzZSB0aGV5XHJcbiAgICAgICAgICAgIC8vIGRvbid0IGhhdmUgdXNlZnVsIGV2ZW50IGlkcyksIHdlIGRvbid0IHdhbnQgdG8gc2hvdyBpdCwgYnV0XHJcbiAgICAgICAgICAgIC8vIHdlIHN0aWxsIHdhbnQgdG8gY3JlYXRlIHRoZSA8bGkvPiBmb3IgaXQgc28gdGhhdCB0aGVcclxuICAgICAgICAgICAgLy8gYWxnb3JpdGhtcyB3aGljaCBkZXBlbmQgb24gaXRzIHBvc2l0aW9uIG9uIHRoZSBzY3JlZW4gYXJlbid0XHJcbiAgICAgICAgICAgIC8vIGNvbmZ1c2VkLlxyXG4gICAgICAgICAgICBpZiAodmlzaWJsZSkge1xyXG4gICAgICAgICAgICAgICAgaHIgPSA8aHIgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfbXlSZWFkTWFya2VyXCJcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e29wYWNpdHk6IDEsIHdpZHRoOiAnOTklJ319XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxsaSBrZXk9e1wicmVhZE1hcmtlcl9cIitldmVudElkfSByZWY9e3RoaXMuX3JlYWRNYXJrZXJOb2RlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfbXlSZWFkTWFya2VyX2NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgaHIgfVxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuZ2hvc3RSZWFkTWFya2Vycy5pbmNsdWRlcyhldmVudElkKSkge1xyXG4gICAgICAgICAgICAvLyBXZSByZW5kZXIgJ2dob3N0JyByZWFkIG1hcmtlcnMgaW4gdGhlIERPTSB3aGlsZSB0aGV5XHJcbiAgICAgICAgICAgIC8vIHRyYW5zaXRpb24gYXdheS4gVGhpcyBhbGxvd3MgdGhlIGFjdHVhbCByZWFkIG1hcmtlclxyXG4gICAgICAgICAgICAvLyB0byBiZSBpbiB0aGUgcmlnaHQgcGxhY2Ugc3RyYWlnaHQgYXdheSB3aXRob3V0IGhhdmluZ1xyXG4gICAgICAgICAgICAvLyB0byB3YWl0IGZvciB0aGUgdHJhbnNpdGlvbiB0byBmaW5pc2guXHJcbiAgICAgICAgICAgIC8vIFRoZXJlIGFyZSBwcm9iYWJseSBtdWNoIHNpbXBsZXIgd2F5cyB0byBkbyB0aGlzIHRyYW5zaXRpb24sXHJcbiAgICAgICAgICAgIC8vIHBvc3NpYmx5IHVzaW5nIHJlYWN0LXRyYW5zaXRpb24tZ3JvdXAgd2hpY2ggaGFuZGxlcyBrZWVwaW5nXHJcbiAgICAgICAgICAgIC8vIGVsZW1lbnRzIGluIHRoZSBET00gd2hpbHN0IHRoZXkgdHJhbnNpdGlvbiBvdXQsIGFsdGhvdWdoIG91clxyXG4gICAgICAgICAgICAvLyBjYXNlIGlzIGEgbGl0dGxlIG1vcmUgY29tcGxleCBiZWNhdXNlIG9ubHkgc29tZSBvZiB0aGUgaXRlbXNcclxuICAgICAgICAgICAgLy8gdHJhbnNpdGlvbiAoaWUuIHRoZSByZWFkIG1hcmtlcnMgZG8gYnV0IHRoZSBldmVudCB0aWxlcyBkbyBub3QpXHJcbiAgICAgICAgICAgIC8vIGFuZCBUcmFuc2l0aW9uR3JvdXAgcmVxdWlyZXMgdGhhdCBhbGwgaXRzIGNoaWxkcmVuIGFyZSBUcmFuc2l0aW9ucy5cclxuICAgICAgICAgICAgY29uc3QgaHIgPSA8aHIgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfbXlSZWFkTWFya2VyXCJcclxuICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fY29sbGVjdEdob3N0UmVhZE1hcmtlcn1cclxuICAgICAgICAgICAgICAgIG9uVHJhbnNpdGlvbkVuZD17dGhpcy5fb25HaG9zdFRyYW5zaXRpb25FbmR9XHJcbiAgICAgICAgICAgICAgICBkYXRhLWV2ZW50aWQ9e2V2ZW50SWR9XHJcbiAgICAgICAgICAgIC8+O1xyXG5cclxuICAgICAgICAgICAgLy8gZ2l2ZSBpdCBhIGtleSB3aGljaCBkZXBlbmRzIG9uIHRoZSBldmVudCBpZC4gVGhhdCB3aWxsIGVuc3VyZSB0aGF0XHJcbiAgICAgICAgICAgIC8vIHdlIGdldCBhIG5ldyBET00gbm9kZSAocmVzdGFydGluZyB0aGUgYW5pbWF0aW9uKSB3aGVuIHRoZSBnaG9zdFxyXG4gICAgICAgICAgICAvLyBtb3ZlcyB0byBhIGRpZmZlcmVudCBldmVudC5cclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxsaSBrZXk9e1wiX3JlYWR1cHRvZ2hvc3RfXCIrZXZlbnRJZH1cclxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X215UmVhZE1hcmtlcl9jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGhyIH1cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBfY29sbGVjdEdob3N0UmVhZE1hcmtlciA9IChub2RlKSA9PiB7XHJcbiAgICAgICAgaWYgKG5vZGUpIHtcclxuICAgICAgICAgICAgLy8gbm93IHRoZSBlbGVtZW50IGhhcyBhcHBlYXJlZCwgY2hhbmdlIHRoZSBzdHlsZSB3aGljaCB3aWxsIHRyaWdnZXIgdGhlIENTUyB0cmFuc2l0aW9uXHJcbiAgICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBub2RlLnN0eWxlLndpZHRoID0gJzEwJSc7XHJcbiAgICAgICAgICAgICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uR2hvc3RUcmFuc2l0aW9uRW5kID0gKGV2KSA9PiB7XHJcbiAgICAgICAgLy8gd2UgY2FuIG5vdyBjbGVhbiB1cCB0aGUgZ2hvc3QgZWxlbWVudFxyXG4gICAgICAgIGNvbnN0IGZpbmlzaGVkRXZlbnRJZCA9IGV2LnRhcmdldC5kYXRhc2V0LmV2ZW50aWQ7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGdob3N0UmVhZE1hcmtlcnM6IHRoaXMuc3RhdGUuZ2hvc3RSZWFkTWFya2Vycy5maWx0ZXIoZWlkID0+IGVpZCAhPT0gZmluaXNoZWRFdmVudElkKSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2dldEV2ZW50VGlsZXMoKSB7XHJcbiAgICAgICAgdGhpcy5ldmVudE5vZGVzID0ge307XHJcblxyXG4gICAgICAgIGxldCBpO1xyXG5cclxuICAgICAgICAvLyBmaXJzdCBmaWd1cmUgb3V0IHdoaWNoIGlzIHRoZSBsYXN0IGV2ZW50IGluIHRoZSBsaXN0IHdoaWNoIHdlJ3JlXHJcbiAgICAgICAgLy8gYWN0dWFsbHkgZ29pbmcgdG8gc2hvdzsgdGhpcyBhbGxvd3MgdXMgdG8gYmVoYXZlIHNsaWdodGx5XHJcbiAgICAgICAgLy8gZGlmZmVyZW50bHkgZm9yIHRoZSBsYXN0IGV2ZW50IGluIHRoZSBsaXN0LiAoZWcgc2hvdyB0aW1lc3RhbXApXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyB3ZSBhbHNvIG5lZWQgdG8gZmlndXJlIG91dCB3aGljaCBpcyB0aGUgbGFzdCBldmVudCB3ZSBzaG93IHdoaWNoIGlzbid0XHJcbiAgICAgICAgLy8gYSBsb2NhbCBlY2hvLCB0byBtYW5hZ2UgdGhlIHJlYWQtbWFya2VyLlxyXG4gICAgICAgIGxldCBsYXN0U2hvd25FdmVudDtcclxuXHJcbiAgICAgICAgbGV0IGxhc3RTaG93bk5vbkxvY2FsRWNob0luZGV4ID0gLTE7XHJcbiAgICAgICAgZm9yIChpID0gdGhpcy5wcm9wcy5ldmVudHMubGVuZ3RoLTE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG14RXYgPSB0aGlzLnByb3BzLmV2ZW50c1tpXTtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9zaG91bGRTaG93RXZlbnQobXhFdikpIHtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobGFzdFNob3duRXZlbnQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgbGFzdFNob3duRXZlbnQgPSBteEV2O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobXhFdi5zdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMgaXMgYSBsb2NhbCBlY2hvXHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGFzdFNob3duTm9uTG9jYWxFY2hvSW5kZXggPSBpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHJldCA9IFtdO1xyXG5cclxuICAgICAgICBsZXQgcHJldkV2ZW50ID0gbnVsbDsgLy8gdGhlIGxhc3QgZXZlbnQgd2Ugc2hvd2VkXHJcblxyXG4gICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0c0J5RXZlbnQgPSB7fTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93UmVhZFJlY2VpcHRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0c0J5RXZlbnQgPSB0aGlzLl9nZXRSZWFkUmVjZWlwdHNCeVNob3duRXZlbnQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBncm91cGVyID0gbnVsbDtcclxuXHJcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHRoaXMucHJvcHMuZXZlbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG14RXYgPSB0aGlzLnByb3BzLmV2ZW50c1tpXTtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnRJZCA9IG14RXYuZ2V0SWQoKTtcclxuICAgICAgICAgICAgY29uc3QgbGFzdCA9IChteEV2ID09PSBsYXN0U2hvd25FdmVudCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoZ3JvdXBlcikge1xyXG4gICAgICAgICAgICAgICAgaWYgKGdyb3VwZXIuc2hvdWxkR3JvdXAobXhFdikpIHtcclxuICAgICAgICAgICAgICAgICAgICBncm91cGVyLmFkZChteEV2KTtcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbm90IHBhcnQgb2YgZ3JvdXAsIHNvIGdldCB0aGUgZ3JvdXAgdGlsZXMsIGNsb3NlIHRoZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGdyb3VwLCBhbmQgY29udGludWUgbGlrZSBhIG5vcm1hbCBldmVudFxyXG4gICAgICAgICAgICAgICAgICAgIHJldC5wdXNoKC4uLmdyb3VwZXIuZ2V0VGlsZXMoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJldkV2ZW50ID0gZ3JvdXBlci5nZXROZXdQcmV2RXZlbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICBncm91cGVyID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCBHcm91cGVyIG9mIGdyb3VwZXJzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoR3JvdXBlci5jYW5TdGFydEdyb3VwKHRoaXMsIG14RXYpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBlciA9IG5ldyBHcm91cGVyKHRoaXMsIG14RXYsIHByZXZFdmVudCwgbGFzdFNob3duRXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICghZ3JvdXBlcikge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd2FudFRpbGUgPSB0aGlzLl9zaG91bGRTaG93RXZlbnQobXhFdik7XHJcbiAgICAgICAgICAgICAgICBpZiAod2FudFRpbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBtYWtlIHN1cmUgd2UgdW5wYWNrIHRoZSBhcnJheSByZXR1cm5lZCBieSBfZ2V0VGlsZXNGb3JFdmVudCxcclxuICAgICAgICAgICAgICAgICAgICAvLyBvdGhlcndpc2UgcmVhY3Qgd2lsbCBhdXRvLWdlbmVyYXRlIGtleXMgYW5kIHdlIHdpbGwgZW5kIHVwXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVwbGFjaW5nIGFsbCBvZiB0aGUgRE9NIGVsZW1lbnRzIGV2ZXJ5IHRpbWUgd2UgcGFnaW5hdGUuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0LnB1c2goLi4udGhpcy5fZ2V0VGlsZXNGb3JFdmVudChwcmV2RXZlbnQsIG14RXYsIGxhc3QpKTtcclxuICAgICAgICAgICAgICAgICAgICBwcmV2RXZlbnQgPSBteEV2O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlYWRNYXJrZXIgPSB0aGlzLl9yZWFkTWFya2VyRm9yRXZlbnQoZXZlbnRJZCwgaSA+PSBsYXN0U2hvd25Ob25Mb2NhbEVjaG9JbmRleCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVhZE1hcmtlcikgcmV0LnB1c2gocmVhZE1hcmtlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChncm91cGVyKSB7XHJcbiAgICAgICAgICAgIHJldC5wdXNoKC4uLmdyb3VwZXIuZ2V0VGlsZXMoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgfVxyXG5cclxuICAgIF9nZXRUaWxlc0ZvckV2ZW50KHByZXZFdmVudCwgbXhFdiwgbGFzdCkge1xyXG4gICAgICAgIGNvbnN0IEV2ZW50VGlsZSA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLkV2ZW50VGlsZScpO1xyXG4gICAgICAgIGNvbnN0IERhdGVTZXBhcmF0b3IgPSBzZGsuZ2V0Q29tcG9uZW50KCdtZXNzYWdlcy5EYXRlU2VwYXJhdG9yJyk7XHJcbiAgICAgICAgY29uc3QgcmV0ID0gW107XHJcblxyXG4gICAgICAgIGNvbnN0IGlzRWRpdGluZyA9IHRoaXMucHJvcHMuZWRpdFN0YXRlICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuZWRpdFN0YXRlLmdldEV2ZW50KCkuZ2V0SWQoKSA9PT0gbXhFdi5nZXRJZCgpO1xyXG4gICAgICAgIC8vIGlzIHRoaXMgYSBjb250aW51YXRpb24gb2YgdGhlIHByZXZpb3VzIG1lc3NhZ2U/XHJcbiAgICAgICAgbGV0IGNvbnRpbnVhdGlvbiA9IGZhbHNlO1xyXG5cclxuICAgICAgICAvLyBTb21lIGV2ZW50cyBzaG91bGQgYXBwZWFyIGFzIGNvbnRpbnVhdGlvbnMgZnJvbSBwcmV2aW91cyBldmVudHMgb2ZcclxuICAgICAgICAvLyBkaWZmZXJlbnQgdHlwZXMuXHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50VHlwZUNvbnRpbnVlcyA9XHJcbiAgICAgICAgICAgIHByZXZFdmVudCAhPT0gbnVsbCAmJlxyXG4gICAgICAgICAgICBjb250aW51ZWRUeXBlcy5pbmNsdWRlcyhteEV2LmdldFR5cGUoKSkgJiZcclxuICAgICAgICAgICAgY29udGludWVkVHlwZXMuaW5jbHVkZXMocHJldkV2ZW50LmdldFR5cGUoKSk7XHJcblxyXG4gICAgICAgIC8vIGlmIHRoZXJlIGlzIGEgcHJldmlvdXMgZXZlbnQgYW5kIGl0IGhhcyB0aGUgc2FtZSBzZW5kZXIgYXMgdGhpcyBldmVudFxyXG4gICAgICAgIC8vIGFuZCB0aGUgdHlwZXMgYXJlIHRoZSBzYW1lL2lzIGluIGNvbnRpbnVlZFR5cGVzIGFuZCB0aGUgdGltZSBiZXR3ZWVuIHRoZW0gaXMgPD0gQ09OVElOVUFUSU9OX01BWF9JTlRFUlZBTFxyXG4gICAgICAgIGlmIChwcmV2RXZlbnQgIT09IG51bGwgJiYgcHJldkV2ZW50LnNlbmRlciAmJiBteEV2LnNlbmRlciAmJiBteEV2LnNlbmRlci51c2VySWQgPT09IHByZXZFdmVudC5zZW5kZXIudXNlcklkICYmXHJcbiAgICAgICAgICAgIC8vIGlmIHdlIGRvbid0IGhhdmUgdGlsZSBmb3IgcHJldmlvdXMgZXZlbnQgdGhlbiBpdCB3YXMgc2hvd24gYnkgc2hvd0hpZGRlbkV2ZW50cyBhbmQgaGFzIG5vIFNlbmRlclByb2ZpbGVcclxuICAgICAgICAgICAgaGF2ZVRpbGVGb3JFdmVudChwcmV2RXZlbnQpICYmIChteEV2LmdldFR5cGUoKSA9PT0gcHJldkV2ZW50LmdldFR5cGUoKSB8fCBldmVudFR5cGVDb250aW51ZXMpICYmXHJcbiAgICAgICAgICAgIChteEV2LmdldFRzKCkgLSBwcmV2RXZlbnQuZ2V0VHMoKSA8PSBDT05USU5VQVRJT05fTUFYX0lOVEVSVkFMKSkge1xyXG4gICAgICAgICAgICBjb250aW51YXRpb24gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbi8qXHJcbiAgICAgICAgLy8gV29yayBvdXQgaWYgdGhpcyBpcyBzdGlsbCBhIGNvbnRpbnVhdGlvbiwgYXMgd2UgYXJlIG5vdyBzaG93aW5nIGNvbW1hbmRzXHJcbiAgICAgICAgLy8gYW5kIC9tZSBtZXNzYWdlcyB3aXRoIHRoZWlyIG93biBsaXR0bGUgYXZhdGFyLiBUaGUgY2FzZSBvZiBhIGNoYW5nZSBvZlxyXG4gICAgICAgIC8vIGV2ZW50IHR5cGUgKGNvbW1hbmRzKSBpcyBoYW5kbGVkIGFib3ZlLCBidXQgd2UgbmVlZCB0byBoYW5kbGUgdGhlIC9tZVxyXG4gICAgICAgIC8vIG1lc3NhZ2VzIHNlcGVyYXRlbHkgYXMgdGhleSBoYXZlIGEgbXNndHlwZSBvZiAnbS5lbW90ZScgYnV0IGFyZSBjbGFzc2VkXHJcbiAgICAgICAgLy8gYXMgbm9ybWFsIG1lc3NhZ2VzXHJcbiAgICAgICAgaWYgKHByZXZFdmVudCAhPT0gbnVsbCAmJiBwcmV2RXZlbnQuc2VuZGVyICYmIG14RXYuc2VuZGVyXHJcbiAgICAgICAgICAgICAgICAmJiBteEV2LnNlbmRlci51c2VySWQgPT09IHByZXZFdmVudC5zZW5kZXIudXNlcklkXHJcbiAgICAgICAgICAgICAgICAmJiBteEV2LmdldFR5cGUoKSA9PSBwcmV2RXZlbnQuZ2V0VHlwZSgpXHJcbiAgICAgICAgICAgICAgICAmJiBwcmV2RXZlbnQuZ2V0Q29udGVudCgpLm1zZ3R5cGUgPT09ICdtLmVtb3RlJykge1xyXG4gICAgICAgICAgICBjb250aW51YXRpb24gPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiovXHJcblxyXG4gICAgICAgIC8vIGxvY2FsIGVjaG9lcyBoYXZlIGEgZmFrZSBkYXRlLCB3aGljaCBjb3VsZCBldmVuIGJlIHllc3RlcmRheS4gVHJlYXQgdGhlbVxyXG4gICAgICAgIC8vIGFzICd0b2RheScgZm9yIHRoZSBkYXRlIHNlcGFyYXRvcnMuXHJcbiAgICAgICAgbGV0IHRzMSA9IG14RXYuZ2V0VHMoKTtcclxuICAgICAgICBsZXQgZXZlbnREYXRlID0gbXhFdi5nZXREYXRlKCk7XHJcbiAgICAgICAgaWYgKG14RXYuc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGV2ZW50RGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgIHRzMSA9IGV2ZW50RGF0ZS5nZXRUaW1lKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBkbyB3ZSBuZWVkIGEgZGF0ZSBzZXBhcmF0b3Igc2luY2UgdGhlIGxhc3QgZXZlbnQ/XHJcbiAgICAgICAgaWYgKHRoaXMuX3dhbnRzRGF0ZVNlcGFyYXRvcihwcmV2RXZlbnQsIGV2ZW50RGF0ZSkpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0ZVNlcGFyYXRvciA9IDxsaSBrZXk9e3RzMX0+PERhdGVTZXBhcmF0b3Iga2V5PXt0czF9IHRzPXt0czF9IC8+PC9saT47XHJcbiAgICAgICAgICAgIHJldC5wdXNoKGRhdGVTZXBhcmF0b3IpO1xyXG4gICAgICAgICAgICBjb250aW51YXRpb24gPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50SWQgPSBteEV2LmdldElkKCk7XHJcbiAgICAgICAgY29uc3QgaGlnaGxpZ2h0ID0gKGV2ZW50SWQgPT09IHRoaXMucHJvcHMuaGlnaGxpZ2h0ZWRFdmVudElkKTtcclxuXHJcbiAgICAgICAgLy8gd2UgY2FuJ3QgdXNlIGxvY2FsIGVjaG9lcyBhcyBzY3JvbGwgdG9rZW5zLCBiZWNhdXNlIHRoZWlyIGV2ZW50IElEcyBjaGFuZ2UuXHJcbiAgICAgICAgLy8gTG9jYWwgZWNob3MgaGF2ZSBhIHNlbmQgXCJzdGF0dXNcIi5cclxuICAgICAgICBjb25zdCBzY3JvbGxUb2tlbiA9IG14RXYuc3RhdHVzID8gdW5kZWZpbmVkIDogZXZlbnRJZDtcclxuXHJcbiAgICAgICAgY29uc3QgcmVhZFJlY2VpcHRzID0gdGhpcy5fcmVhZFJlY2VpcHRzQnlFdmVudFtldmVudElkXTtcclxuXHJcbiAgICAgICAgLy8gRGV2IG5vdGU6IGB0aGlzLl9pc1VubW91bnRpbmcuYmluZCh0aGlzKWAgaXMgaW1wb3J0YW50IC0gaXQgZW5zdXJlcyB0aGF0XHJcbiAgICAgICAgLy8gdGhlIGZ1bmN0aW9uIGlzIHJ1biBpbiB0aGUgY29udGV4dCBvZiB0aGlzIGNsYXNzIGFuZCBub3QgRXZlbnRUaWxlLCB0aGVyZWZvcmVcclxuICAgICAgICAvLyBlbnN1cmluZyB0aGUgcmlnaHQgYHRoaXMuX21vdW50ZWRgIHZhcmlhYmxlIGlzIHVzZWQgYnkgcmVhZCByZWNlaXB0cyAod2hpY2hcclxuICAgICAgICAvLyBkb24ndCB1cGRhdGUgdGhlaXIgcG9zaXRpb24gaWYgd2UsIHRoZSBNZXNzYWdlUGFuZWwsIGlzIHVubW91bnRpbmcpLlxyXG4gICAgICAgIHJldC5wdXNoKFxyXG4gICAgICAgICAgICA8bGkga2V5PXtldmVudElkfVxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9jb2xsZWN0RXZlbnROb2RlLmJpbmQodGhpcywgZXZlbnRJZCl9XHJcbiAgICAgICAgICAgICAgICBkYXRhLXNjcm9sbC10b2tlbnM9e3Njcm9sbFRva2VufVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8RXZlbnRUaWxlIG14RXZlbnQ9e214RXZ9XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWF0aW9uPXtjb250aW51YXRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgaXNSZWRhY3RlZD17bXhFdi5pc1JlZGFjdGVkKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVwbGFjaW5nRXZlbnRJZD17bXhFdi5yZXBsYWNpbmdFdmVudElkKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgZWRpdFN0YXRlPXtpc0VkaXRpbmcgJiYgdGhpcy5wcm9wcy5lZGl0U3RhdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25IZWlnaHRDaGFuZ2VkPXt0aGlzLl9vbkhlaWdodENoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZFJlY2VpcHRzPXtyZWFkUmVjZWlwdHN9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZFJlY2VpcHRNYXA9e3RoaXMuX3JlYWRSZWNlaXB0TWFwfVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dVcmxQcmV2aWV3PXt0aGlzLnByb3BzLnNob3dVcmxQcmV2aWV3fVxyXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrVW5tb3VudGluZz17dGhpcy5faXNVbm1vdW50aW5nLmJpbmQodGhpcyl9XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRTZW5kU3RhdHVzPXtteEV2LmdldEFzc29jaWF0ZWRTdGF0dXMoKX1cclxuICAgICAgICAgICAgICAgICAgICB0aWxlU2hhcGU9e3RoaXMucHJvcHMudGlsZVNoYXBlfVxyXG4gICAgICAgICAgICAgICAgICAgIGlzVHdlbHZlSG91cj17dGhpcy5wcm9wcy5pc1R3ZWx2ZUhvdXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgcGVybWFsaW5rQ3JlYXRvcj17dGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yfVxyXG4gICAgICAgICAgICAgICAgICAgIGxhc3Q9e2xhc3R9XHJcbiAgICAgICAgICAgICAgICAgICAgaXNTZWxlY3RlZEV2ZW50PXtoaWdobGlnaHR9XHJcbiAgICAgICAgICAgICAgICAgICAgZ2V0UmVsYXRpb25zRm9yRXZlbnQ9e3RoaXMucHJvcHMuZ2V0UmVsYXRpb25zRm9yRXZlbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd1JlYWN0aW9ucz17dGhpcy5wcm9wcy5zaG93UmVhY3Rpb25zfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9saT4sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJldDtcclxuICAgIH1cclxuXHJcbiAgICBfd2FudHNEYXRlU2VwYXJhdG9yKHByZXZFdmVudCwgbmV4dEV2ZW50RGF0ZSkge1xyXG4gICAgICAgIGlmIChwcmV2RXZlbnQgPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAvLyBmaXJzdCBldmVudCBpbiB0aGUgcGFuZWw6IGRlcGVuZHMgaWYgd2UgY291bGQgYmFjay1wYWdpbmF0ZSBmcm9tXHJcbiAgICAgICAgICAgIC8vIGhlcmUuXHJcbiAgICAgICAgICAgIHJldHVybiAhdGhpcy5wcm9wcy5zdXBwcmVzc0ZpcnN0RGF0ZVNlcGFyYXRvcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHdhbnRzRGF0ZVNlcGFyYXRvcihwcmV2RXZlbnQuZ2V0RGF0ZSgpLCBuZXh0RXZlbnREYXRlKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYSBsaXN0IG9mIHJlYWQgcmVjZWlwdHMgdGhhdCBzaG91bGQgYmUgc2hvd24gbmV4dCB0byB0aGlzIGV2ZW50XHJcbiAgICAvLyBSZWNlaXB0cyBhcmUgb2JqZWN0cyB3aGljaCBoYXZlIGEgJ3VzZXJJZCcsICdyb29tTWVtYmVyJyBhbmQgJ3RzJy5cclxuICAgIF9nZXRSZWFkUmVjZWlwdHNGb3JFdmVudChldmVudCkge1xyXG4gICAgICAgIGNvbnN0IG15VXNlcklkID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZDtcclxuXHJcbiAgICAgICAgLy8gZ2V0IGxpc3Qgb2YgcmVhZCByZWNlaXB0cywgc29ydGVkIG1vc3QgcmVjZW50IGZpcnN0XHJcbiAgICAgICAgY29uc3QgeyByb29tIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGlmICghcm9vbSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgcmVjZWlwdHMgPSBbXTtcclxuICAgICAgICByb29tLmdldFJlY2VpcHRzRm9yRXZlbnQoZXZlbnQpLmZvckVhY2goKHIpID0+IHtcclxuICAgICAgICAgICAgaWYgKCFyLnVzZXJJZCB8fCByLnR5cGUgIT09IFwibS5yZWFkXCIgfHwgci51c2VySWQgPT09IG15VXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vIGlnbm9yZSBub24tcmVhZCByZWNlaXB0cyBhbmQgcmVjZWlwdHMgZnJvbSBzZWxmLlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNVc2VySWdub3JlZChyLnVzZXJJZCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjsgLy8gaWdub3JlIGlnbm9yZWQgdXNlcnNcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBtZW1iZXIgPSByb29tLmdldE1lbWJlcihyLnVzZXJJZCk7XHJcbiAgICAgICAgICAgIHJlY2VpcHRzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgdXNlcklkOiByLnVzZXJJZCxcclxuICAgICAgICAgICAgICAgIHJvb21NZW1iZXI6IG1lbWJlcixcclxuICAgICAgICAgICAgICAgIHRzOiByLmRhdGEgPyByLmRhdGEudHMgOiAwLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gcmVjZWlwdHM7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gR2V0IGFuIG9iamVjdCB0aGF0IG1hcHMgZnJvbSBldmVudCBJRCB0byBhIGxpc3Qgb2YgcmVhZCByZWNlaXB0cyB0aGF0XHJcbiAgICAvLyBzaG91bGQgYmUgc2hvd24gbmV4dCB0byB0aGF0IGV2ZW50LiBJZiBhIGhpZGRlbiBldmVudCBoYXMgcmVhZCByZWNlaXB0cyxcclxuICAgIC8vIHRoZXkgYXJlIGZvbGRlZCBpbnRvIHRoZSByZWNlaXB0cyBvZiB0aGUgbGFzdCBzaG93biBldmVudC5cclxuICAgIF9nZXRSZWFkUmVjZWlwdHNCeVNob3duRXZlbnQoKSB7XHJcbiAgICAgICAgY29uc3QgcmVjZWlwdHNCeUV2ZW50ID0ge307XHJcbiAgICAgICAgY29uc3QgcmVjZWlwdHNCeVVzZXJJZCA9IHt9O1xyXG5cclxuICAgICAgICBsZXQgbGFzdFNob3duRXZlbnRJZDtcclxuICAgICAgICBmb3IgKGNvbnN0IGV2ZW50IG9mIHRoaXMucHJvcHMuZXZlbnRzKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9zaG91bGRTaG93RXZlbnQoZXZlbnQpKSB7XHJcbiAgICAgICAgICAgICAgICBsYXN0U2hvd25FdmVudElkID0gZXZlbnQuZ2V0SWQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIWxhc3RTaG93bkV2ZW50SWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBleGlzdGluZ1JlY2VpcHRzID0gcmVjZWlwdHNCeUV2ZW50W2xhc3RTaG93bkV2ZW50SWRdIHx8IFtdO1xyXG4gICAgICAgICAgICBjb25zdCBuZXdSZWNlaXB0cyA9IHRoaXMuX2dldFJlYWRSZWNlaXB0c0ZvckV2ZW50KGV2ZW50KTtcclxuICAgICAgICAgICAgcmVjZWlwdHNCeUV2ZW50W2xhc3RTaG93bkV2ZW50SWRdID0gZXhpc3RpbmdSZWNlaXB0cy5jb25jYXQobmV3UmVjZWlwdHMpO1xyXG5cclxuICAgICAgICAgICAgLy8gUmVjb3JkIHRoZXNlIHJlY2VpcHRzIGFsb25nIHdpdGggdGhlaXIgbGFzdCBzaG93biBldmVudCBJRCBmb3JcclxuICAgICAgICAgICAgLy8gZWFjaCBhc3NvY2lhdGVkIHVzZXIgSUQuXHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcmVjZWlwdCBvZiBuZXdSZWNlaXB0cykge1xyXG4gICAgICAgICAgICAgICAgcmVjZWlwdHNCeVVzZXJJZFtyZWNlaXB0LnVzZXJJZF0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFzdFNob3duRXZlbnRJZCxcclxuICAgICAgICAgICAgICAgICAgICByZWNlaXB0LFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSXQncyBwb3NzaWJsZSBpbiBzb21lIGNhc2VzIChmb3IgZXhhbXBsZSwgd2hlbiBhIHJlYWQgcmVjZWlwdFxyXG4gICAgICAgIC8vIGFkdmFuY2VzIGJlZm9yZSB3ZSBoYXZlIHBhZ2luYXRlZCBpbiB0aGUgbmV3IGV2ZW50IHRoYXQgaXQncyBtYXJraW5nXHJcbiAgICAgICAgLy8gcmVjZWl2ZWQpIHRoYXQgd2UgY2FuIHRlbXBvcmFyaWx5IG5vdCBoYXZlIGEgbWF0Y2hpbmcgZXZlbnQgZm9yXHJcbiAgICAgICAgLy8gc29tZW9uZSB3aGljaCBoYWQgb25lIGluIHRoZSBsYXN0LiBCeSBsb29raW5nIHRocm91Z2ggb3VyIHByZXZpb3VzXHJcbiAgICAgICAgLy8gbWFwcGluZyBvZiByZWNlaXB0cyBieSB1c2VyIElELCB3ZSBjYW4gY292ZXIgcmVjb3ZlciBhbnkgcmVjZWlwdHNcclxuICAgICAgICAvLyB0aGF0IHdvdWxkIGhhdmUgYmVlbiBsb3N0IGJ5IHVzaW5nIHRoZSBzYW1lIGV2ZW50IElEIGZyb20gbGFzdCB0aW1lLlxyXG4gICAgICAgIGZvciAoY29uc3QgdXNlcklkIGluIHRoaXMuX3JlYWRSZWNlaXB0c0J5VXNlcklkKSB7XHJcbiAgICAgICAgICAgIGlmIChyZWNlaXB0c0J5VXNlcklkW3VzZXJJZF0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHsgbGFzdFNob3duRXZlbnRJZCwgcmVjZWlwdCB9ID0gdGhpcy5fcmVhZFJlY2VpcHRzQnlVc2VySWRbdXNlcklkXTtcclxuICAgICAgICAgICAgY29uc3QgZXhpc3RpbmdSZWNlaXB0cyA9IHJlY2VpcHRzQnlFdmVudFtsYXN0U2hvd25FdmVudElkXSB8fCBbXTtcclxuICAgICAgICAgICAgcmVjZWlwdHNCeUV2ZW50W2xhc3RTaG93bkV2ZW50SWRdID0gZXhpc3RpbmdSZWNlaXB0cy5jb25jYXQocmVjZWlwdCk7XHJcbiAgICAgICAgICAgIHJlY2VpcHRzQnlVc2VySWRbdXNlcklkXSA9IHsgbGFzdFNob3duRXZlbnRJZCwgcmVjZWlwdCB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9yZWFkUmVjZWlwdHNCeVVzZXJJZCA9IHJlY2VpcHRzQnlVc2VySWQ7XHJcblxyXG4gICAgICAgIC8vIEFmdGVyIGdyb3VwaW5nIHJlY2VpcHRzIGJ5IHNob3duIGV2ZW50cywgZG8gYW5vdGhlciBwYXNzIHRvIHNvcnQgZWFjaFxyXG4gICAgICAgIC8vIHJlY2VpcHQgbGlzdC5cclxuICAgICAgICBmb3IgKGNvbnN0IGV2ZW50SWQgaW4gcmVjZWlwdHNCeUV2ZW50KSB7XHJcbiAgICAgICAgICAgIHJlY2VpcHRzQnlFdmVudFtldmVudElkXS5zb3J0KChyMSwgcjIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiByMi50cyAtIHIxLnRzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZWNlaXB0c0J5RXZlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgX2NvbGxlY3RFdmVudE5vZGUgPSAoZXZlbnRJZCwgbm9kZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuZXZlbnROb2Rlc1tldmVudElkXSA9IG5vZGU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gb25jZSBkeW5hbWljIGNvbnRlbnQgaW4gdGhlIGV2ZW50cyBsb2FkLCBtYWtlIHRoZSBzY3JvbGxQYW5lbCBjaGVjayB0aGVcclxuICAgIC8vIHNjcm9sbCBvZmZzZXRzLlxyXG4gICAgX29uSGVpZ2h0Q2hhbmdlZCA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgaWYgKHNjcm9sbFBhbmVsKSB7XHJcbiAgICAgICAgICAgIHNjcm9sbFBhbmVsLmNoZWNrU2Nyb2xsKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfb25UeXBpbmdTaG93biA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgLy8gdGhpcyB3aWxsIG1ha2UgdGhlIHRpbWVsaW5lIGdyb3csIHNvIGNoZWNrU2Nyb2xsXHJcbiAgICAgICAgc2Nyb2xsUGFuZWwuY2hlY2tTY3JvbGwoKTtcclxuICAgICAgICBpZiAoc2Nyb2xsUGFuZWwgJiYgc2Nyb2xsUGFuZWwuZ2V0U2Nyb2xsU3RhdGUoKS5zdHVja0F0Qm90dG9tKSB7XHJcbiAgICAgICAgICAgIHNjcm9sbFBhbmVsLnByZXZlbnRTaHJpbmtpbmcoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblR5cGluZ0hpZGRlbiA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgaWYgKHNjcm9sbFBhbmVsKSB7XHJcbiAgICAgICAgICAgIC8vIGFzIGhpZGluZyB0aGUgdHlwaW5nIG5vdGlmaWNhdGlvbnMgZG9lc24ndFxyXG4gICAgICAgICAgICAvLyB1cGRhdGUgdGhlIHNjcm9sbFBhbmVsLCB3ZSB0ZWxsIGl0IHRvIGFwcGx5XHJcbiAgICAgICAgICAgIC8vIHRoZSBzaHJpbmtpbmcgcHJldmVudGlvbiBvbmNlIHRoZSB0eXBpbmcgbm90aWZzIGFyZSBoaWRkZW5cclxuICAgICAgICAgICAgc2Nyb2xsUGFuZWwudXBkYXRlUHJldmVudFNocmlua2luZygpO1xyXG4gICAgICAgICAgICAvLyBvcmRlciBpcyBpbXBvcnRhbnQgaGVyZSBhcyBjaGVja1Njcm9sbCB3aWxsIHNjcm9sbCBkb3duIHRvXHJcbiAgICAgICAgICAgIC8vIHJldmVhbCBhZGRlZCBwYWRkaW5nIHRvIGJhbGFuY2UgdGhlIG5vdGlmcyBkaXNhcHBlYXJpbmcuXHJcbiAgICAgICAgICAgIHNjcm9sbFBhbmVsLmNoZWNrU2Nyb2xsKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICB1cGRhdGVUaW1lbGluZU1pbkhlaWdodCgpIHtcclxuICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQ7XHJcblxyXG4gICAgICAgIGlmIChzY3JvbGxQYW5lbCkge1xyXG4gICAgICAgICAgICBjb25zdCBpc0F0Qm90dG9tID0gc2Nyb2xsUGFuZWwuaXNBdEJvdHRvbSgpO1xyXG4gICAgICAgICAgICBjb25zdCB3aG9Jc1R5cGluZyA9IHRoaXMuX3dob0lzVHlwaW5nLmN1cnJlbnQ7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzVHlwaW5nVmlzaWJsZSA9IHdob0lzVHlwaW5nICYmIHdob0lzVHlwaW5nLmlzVmlzaWJsZSgpO1xyXG4gICAgICAgICAgICAvLyB3aGVuIG1lc3NhZ2VzIGdldCBhZGRlZCB0byB0aGUgdGltZWxpbmUsXHJcbiAgICAgICAgICAgIC8vIGJ1dCBzb21lYm9keSBlbHNlIGlzIHN0aWxsIHR5cGluZyxcclxuICAgICAgICAgICAgLy8gdXBkYXRlIHRoZSBtaW4taGVpZ2h0LCBzbyBvbmNlIHRoZSBsYXN0XHJcbiAgICAgICAgICAgIC8vIHBlcnNvbiBzdG9wcyB0eXBpbmcsIG5vIGp1bXBpbmcgb2NjdXJzXHJcbiAgICAgICAgICAgIGlmIChpc0F0Qm90dG9tICYmIGlzVHlwaW5nVmlzaWJsZSkge1xyXG4gICAgICAgICAgICAgICAgc2Nyb2xsUGFuZWwucHJldmVudFNocmlua2luZygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uVGltZWxpbmVSZXNldCgpIHtcclxuICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3Njcm9sbFBhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgaWYgKHNjcm9sbFBhbmVsKSB7XHJcbiAgICAgICAgICAgIHNjcm9sbFBhbmVsLmNsZWFyUHJldmVudFNocmlua2luZygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgU2Nyb2xsUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KFwic3RydWN0dXJlcy5TY3JvbGxQYW5lbFwiKTtcclxuICAgICAgICBjb25zdCBXaG9Jc1R5cGluZ1RpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuV2hvSXNUeXBpbmdUaWxlXCIpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuICAgICAgICBsZXQgdG9wU3Bpbm5lcjtcclxuICAgICAgICBsZXQgYm90dG9tU3Bpbm5lcjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5iYWNrUGFnaW5hdGluZykge1xyXG4gICAgICAgICAgICB0b3BTcGlubmVyID0gPGxpIGtleT1cIl90b3BTcGlubmVyXCI+PFNwaW5uZXIgLz48L2xpPjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZm9yd2FyZFBhZ2luYXRpbmcpIHtcclxuICAgICAgICAgICAgYm90dG9tU3Bpbm5lciA9IDxsaSBrZXk9XCJfYm90dG9tU3Bpbm5lclwiPjxTcGlubmVyIC8+PC9saT47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzdHlsZSA9IHRoaXMucHJvcHMuaGlkZGVuID8geyBkaXNwbGF5OiAnbm9uZScgfSA6IHt9O1xyXG5cclxuICAgICAgICBjb25zdCBjbGFzc05hbWUgPSBjbGFzc05hbWVzKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLmNsYXNzTmFtZSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJteF9NZXNzYWdlUGFuZWxfYWx3YXlzU2hvd1RpbWVzdGFtcHNcIjogdGhpcy5wcm9wcy5hbHdheXNTaG93VGltZXN0YW1wcyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBsZXQgd2hvSXNUeXBpbmc7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucm9vbSAmJiAhdGhpcy5wcm9wcy50aWxlU2hhcGUgJiYgdGhpcy5zdGF0ZS5zaG93VHlwaW5nTm90aWZpY2F0aW9ucykge1xyXG4gICAgICAgICAgICB3aG9Jc1R5cGluZyA9ICg8V2hvSXNUeXBpbmdUaWxlXHJcbiAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnJvb219XHJcbiAgICAgICAgICAgICAgICBvblNob3duPXt0aGlzLl9vblR5cGluZ1Nob3dufVxyXG4gICAgICAgICAgICAgICAgb25IaWRkZW49e3RoaXMuX29uVHlwaW5nSGlkZGVufVxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl93aG9Jc1R5cGluZ30gLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxTY3JvbGxQYW5lbFxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9zY3JvbGxQYW5lbH1cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NOYW1lfVxyXG4gICAgICAgICAgICAgICAgb25TY3JvbGw9e3RoaXMucHJvcHMub25TY3JvbGx9XHJcbiAgICAgICAgICAgICAgICBvblJlc2l6ZT17dGhpcy5vblJlc2l6ZX1cclxuICAgICAgICAgICAgICAgIG9uRmlsbFJlcXVlc3Q9e3RoaXMucHJvcHMub25GaWxsUmVxdWVzdH1cclxuICAgICAgICAgICAgICAgIG9uVW5maWxsUmVxdWVzdD17dGhpcy5wcm9wcy5vblVuZmlsbFJlcXVlc3R9XHJcbiAgICAgICAgICAgICAgICBzdHlsZT17c3R5bGV9XHJcbiAgICAgICAgICAgICAgICBzdGlja3lCb3R0b209e3RoaXMucHJvcHMuc3RpY2t5Qm90dG9tfVxyXG4gICAgICAgICAgICAgICAgcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHsgdG9wU3Bpbm5lciB9XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuX2dldEV2ZW50VGlsZXMoKSB9XHJcbiAgICAgICAgICAgICAgICB7IHdob0lzVHlwaW5nIH1cclxuICAgICAgICAgICAgICAgIHsgYm90dG9tU3Bpbm5lciB9XHJcbiAgICAgICAgICAgIDwvU2Nyb2xsUGFuZWw+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuLyogR3JvdXBlciBjbGFzc2VzIGRldGVybWluZSB3aGVuIGV2ZW50cyBjYW4gYmUgZ3JvdXBlZCB0b2dldGhlciBpbiBhIHN1bW1hcnkuXHJcbiAqIEdyb3VwZXJzIHNob3VsZCBoYXZlIHRoZSBmb2xsb3dpbmcgbWV0aG9kczpcclxuICogLSBjYW5TdGFydEdyb3VwIChzdGF0aWMpOiBkZXRlcm1pbmVzIGlmIGEgbmV3IGdyb3VwIHNob3VsZCBiZSBzdGFydGVkIHdpdGggdGhlXHJcbiAqICAgZ2l2ZW4gZXZlbnRcclxuICogLSBzaG91bGRHcm91cDogZGV0ZXJtaW5lcyBpZiB0aGUgZ2l2ZW4gZXZlbnQgc2hvdWxkIGJlIGFkZGVkIHRvIGFuIGV4aXN0aW5nIGdyb3VwXHJcbiAqIC0gYWRkOiBhZGRzIGFuIGV2ZW50IHRvIGFuIGV4aXN0aW5nIGdyb3VwIChzaG91bGQgb25seSBiZSBjYWxsZWQgaWYgc2hvdWxkR3JvdXBcclxuICogICByZXR1cm4gdHJ1ZSlcclxuICogLSBnZXRUaWxlczogcmV0dXJucyB0aGUgdGlsZXMgdGhhdCByZXByZXNlbnQgdGhlIGdyb3VwXHJcbiAqIC0gZ2V0TmV3UHJldkV2ZW50OiByZXR1cm5zIHRoZSBldmVudCB0aGF0IHNob3VsZCBiZSB1c2VkIGFzIHRoZSBuZXcgcHJldkV2ZW50XHJcbiAqICAgd2hlbiBkZXRlcm1pbmluZyB0aGluZ3Mgc3VjaCBhcyB3aGV0aGVyIGEgZGF0ZSBzZXBhcmF0b3IgaXMgbmVjZXNzYXJ5XHJcbiAqL1xyXG5cclxuLy8gV3JhcCBpbml0aWFsIHJvb20gY3JlYXRpb24gZXZlbnRzIGludG8gYW4gRXZlbnRMaXN0U3VtbWFyeVxyXG4vLyBHcm91cGluZyBvbmx5IGV2ZW50cyBzZW50IGJ5IHRoZSBzYW1lIHVzZXIgdGhhdCBzZW50IHRoZSBgbS5yb29tLmNyZWF0ZWAgYW5kIG9ubHkgdW50aWxcclxuLy8gdGhlIGZpcnN0IG5vbi1zdGF0ZSBldmVudCBvciBtZW1iZXJzaGlwIGV2ZW50IHdoaWNoIGlzIG5vdCByZWdhcmRpbmcgdGhlIHNlbmRlciBvZiB0aGUgYG0ucm9vbS5jcmVhdGVgIGV2ZW50XHJcbmNsYXNzIENyZWF0aW9uR3JvdXBlciB7XHJcbiAgICBzdGF0aWMgY2FuU3RhcnRHcm91cCA9IGZ1bmN0aW9uKHBhbmVsLCBldikge1xyXG4gICAgICAgIHJldHVybiBldi5nZXRUeXBlKCkgPT09IFwibS5yb29tLmNyZWF0ZVwiO1xyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwYW5lbCwgY3JlYXRlRXZlbnQsIHByZXZFdmVudCwgbGFzdFNob3duRXZlbnQpIHtcclxuICAgICAgICB0aGlzLnBhbmVsID0gcGFuZWw7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVFdmVudCA9IGNyZWF0ZUV2ZW50O1xyXG4gICAgICAgIHRoaXMucHJldkV2ZW50ID0gcHJldkV2ZW50O1xyXG4gICAgICAgIHRoaXMubGFzdFNob3duRXZlbnQgPSBsYXN0U2hvd25FdmVudDtcclxuICAgICAgICB0aGlzLmV2ZW50cyA9IFtdO1xyXG4gICAgICAgIC8vIGV2ZW50cyB0aGF0IHdlIGluY2x1ZGUgaW4gdGhlIGdyb3VwIGJ1dCB0aGVuIGVqZWN0IG91dCBhbmQgcGxhY2VcclxuICAgICAgICAvLyBhYm92ZSB0aGUgZ3JvdXAuXHJcbiAgICAgICAgdGhpcy5lamVjdGVkRXZlbnRzID0gW107XHJcbiAgICAgICAgdGhpcy5yZWFkTWFya2VyID0gcGFuZWwuX3JlYWRNYXJrZXJGb3JFdmVudChcclxuICAgICAgICAgICAgY3JlYXRlRXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgY3JlYXRlRXZlbnQgPT09IGxhc3RTaG93bkV2ZW50LFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdWxkR3JvdXAoZXYpIHtcclxuICAgICAgICBjb25zdCBwYW5lbCA9IHRoaXMucGFuZWw7XHJcbiAgICAgICAgY29uc3QgY3JlYXRlRXZlbnQgPSB0aGlzLmNyZWF0ZUV2ZW50O1xyXG4gICAgICAgIGlmICghcGFuZWwuX3Nob3VsZFNob3dFdmVudChldikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChwYW5lbC5fd2FudHNEYXRlU2VwYXJhdG9yKHRoaXMuY3JlYXRlRXZlbnQsIGV2LmdldERhdGUoKSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXYuZ2V0VHlwZSgpID09PSBcIm0ucm9vbS5tZW1iZXJcIlxyXG4gICAgICAgICAgICAmJiAoZXYuZ2V0U3RhdGVLZXkoKSAhPT0gY3JlYXRlRXZlbnQuZ2V0U2VuZGVyKCkgfHwgZXYuZ2V0Q29udGVudCgpW1wibWVtYmVyc2hpcFwiXSAhPT0gXCJqb2luXCIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGV2LmlzU3RhdGUoKSAmJiBldi5nZXRTZW5kZXIoKSA9PT0gY3JlYXRlRXZlbnQuZ2V0U2VuZGVyKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGQoZXYpIHtcclxuICAgICAgICBjb25zdCBwYW5lbCA9IHRoaXMucGFuZWw7XHJcbiAgICAgICAgdGhpcy5yZWFkTWFya2VyID0gdGhpcy5yZWFkTWFya2VyIHx8IHBhbmVsLl9yZWFkTWFya2VyRm9yRXZlbnQoXHJcbiAgICAgICAgICAgIGV2LmdldElkKCksXHJcbiAgICAgICAgICAgIGV2ID09PSB0aGlzLmxhc3RTaG93bkV2ZW50LFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKCFwYW5lbC5fc2hvdWxkU2hvd0V2ZW50KGV2KSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgPT09IFwibS5yb29tLmVuY3J5cHRpb25cIikge1xyXG4gICAgICAgICAgICB0aGlzLmVqZWN0ZWRFdmVudHMucHVzaChldik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5ldmVudHMucHVzaChldik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFRpbGVzKCkge1xyXG4gICAgICAgIC8vIElmIHdlIGRvbid0IGhhdmUgYW55IGV2ZW50cyB0byBncm91cCwgZG9uJ3QgZXZlbiB0cnkgdG8gZ3JvdXAgdGhlbS4gVGhlIGxvZ2ljXHJcbiAgICAgICAgLy8gYmVsb3cgYXNzdW1lcyB0aGF0IHdlIGhhdmUgYSBncm91cCBvZiBldmVudHMgdG8gZGVhbCB3aXRoLCBidXQgd2UgbWlnaHQgbm90IGlmXHJcbiAgICAgICAgLy8gdGhlIGV2ZW50cyB3ZSB3ZXJlIHN1cHBvc2VkIHRvIGdyb3VwIHdlcmUgcmVkYWN0ZWQuXHJcbiAgICAgICAgaWYgKCF0aGlzLmV2ZW50cyB8fCAhdGhpcy5ldmVudHMubGVuZ3RoKSByZXR1cm4gW107XHJcblxyXG4gICAgICAgIGNvbnN0IERhdGVTZXBhcmF0b3IgPSBzZGsuZ2V0Q29tcG9uZW50KCdtZXNzYWdlcy5EYXRlU2VwYXJhdG9yJyk7XHJcbiAgICAgICAgY29uc3QgRXZlbnRMaXN0U3VtbWFyeSA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkV2ZW50TGlzdFN1bW1hcnknKTtcclxuXHJcbiAgICAgICAgY29uc3QgcGFuZWwgPSB0aGlzLnBhbmVsO1xyXG4gICAgICAgIGNvbnN0IHJldCA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGNyZWF0ZUV2ZW50ID0gdGhpcy5jcmVhdGVFdmVudDtcclxuICAgICAgICBjb25zdCBsYXN0U2hvd25FdmVudCA9IHRoaXMubGFzdFNob3duRXZlbnQ7XHJcblxyXG4gICAgICAgIGlmIChwYW5lbC5fd2FudHNEYXRlU2VwYXJhdG9yKHRoaXMucHJldkV2ZW50LCBjcmVhdGVFdmVudC5nZXREYXRlKCkpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRzID0gY3JlYXRlRXZlbnQuZ2V0VHMoKTtcclxuICAgICAgICAgICAgcmV0LnB1c2goXHJcbiAgICAgICAgICAgICAgICA8bGkga2V5PXt0cysnfid9PjxEYXRlU2VwYXJhdG9yIGtleT17dHMrJ34nfSB0cz17dHN9IC8+PC9saT4sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZiB0aGlzIG0ucm9vbS5jcmVhdGUgZXZlbnQgc2hvdWxkIGJlIHNob3duIChyb29tIHVwZ3JhZGUpIHRoZW4gc2hvdyBpdCBiZWZvcmUgdGhlIHN1bW1hcnlcclxuICAgICAgICBpZiAocGFuZWwuX3Nob3VsZFNob3dFdmVudChjcmVhdGVFdmVudCkpIHtcclxuICAgICAgICAgICAgLy8gcGFzcyBpbiB0aGUgY3JlYXRlRXZlbnQgYXMgcHJldkV2ZW50IGFzIHdlbGwgc28gbm8gZXh0cmEgRGF0ZVNlcGFyYXRvciBpcyByZW5kZXJlZFxyXG4gICAgICAgICAgICByZXQucHVzaCguLi5wYW5lbC5fZ2V0VGlsZXNGb3JFdmVudChjcmVhdGVFdmVudCwgY3JlYXRlRXZlbnQsIGZhbHNlKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmb3IgKGNvbnN0IGVqZWN0ZWQgb2YgdGhpcy5lamVjdGVkRXZlbnRzKSB7XHJcbiAgICAgICAgICAgIHJldC5wdXNoKC4uLnBhbmVsLl9nZXRUaWxlc0ZvckV2ZW50KFxyXG4gICAgICAgICAgICAgICAgY3JlYXRlRXZlbnQsIGVqZWN0ZWQsIGNyZWF0ZUV2ZW50ID09PSBsYXN0U2hvd25FdmVudCxcclxuICAgICAgICAgICAgKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBldmVudFRpbGVzID0gdGhpcy5ldmVudHMubWFwKChlKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEluIG9yZGVyIHRvIHByZXZlbnQgRGF0ZVNlcGFyYXRvcnMgZnJvbSBhcHBlYXJpbmcgaW4gdGhlIGV4cGFuZGVkIGZvcm1cclxuICAgICAgICAgICAgLy8gb2YgRXZlbnRMaXN0U3VtbWFyeSwgcmVuZGVyIGVhY2ggbWVtYmVyIGV2ZW50IGFzIGlmIHRoZSBwcmV2aW91c1xyXG4gICAgICAgICAgICAvLyBvbmUgd2FzIGl0c2VsZi4gVGhpcyB3YXksIHRoZSB0aW1lc3RhbXAgb2YgdGhlIHByZXZpb3VzIGV2ZW50ID09PSB0aGVcclxuICAgICAgICAgICAgLy8gdGltZXN0YW1wIG9mIHRoZSBjdXJyZW50IGV2ZW50LCBhbmQgbm8gRGF0ZVNlcGFyYXRvciBpcyBpbnNlcnRlZC5cclxuICAgICAgICAgICAgcmV0dXJuIHBhbmVsLl9nZXRUaWxlc0ZvckV2ZW50KGUsIGUsIGUgPT09IGxhc3RTaG93bkV2ZW50KTtcclxuICAgICAgICB9KS5yZWR1Y2UoKGEsIGIpID0+IGEuY29uY2F0KGIpLCBbXSk7XHJcbiAgICAgICAgLy8gR2V0IHNlbmRlciBwcm9maWxlIGZyb20gdGhlIGxhdGVzdCBldmVudCBpbiB0aGUgc3VtbWFyeSBhcyB0aGUgbS5yb29tLmNyZWF0ZSBkb2Vzbid0IGNvbnRhaW4gb25lXHJcbiAgICAgICAgY29uc3QgZXYgPSB0aGlzLmV2ZW50c1t0aGlzLmV2ZW50cy5sZW5ndGggLSAxXTtcclxuICAgICAgICByZXQucHVzaChcclxuICAgICAgICAgICAgPEV2ZW50TGlzdFN1bW1hcnlcclxuICAgICAgICAgICAgICAgICBrZXk9XCJyb29tY3JlYXRpb25zdW1tYXJ5XCJcclxuICAgICAgICAgICAgICAgICBldmVudHM9e3RoaXMuZXZlbnRzfVxyXG4gICAgICAgICAgICAgICAgIG9uVG9nZ2xlPXtwYW5lbC5fb25IZWlnaHRDaGFuZ2VkfSAvLyBVcGRhdGUgc2Nyb2xsIHN0YXRlXHJcbiAgICAgICAgICAgICAgICAgc3VtbWFyeU1lbWJlcnM9e1tldi5zZW5kZXJdfVxyXG4gICAgICAgICAgICAgICAgIHN1bW1hcnlUZXh0PXtfdChcIiUoY3JlYXRvcilzIGNyZWF0ZWQgYW5kIGNvbmZpZ3VyZWQgdGhlIHJvb20uXCIsIHtcclxuICAgICAgICAgICAgICAgICAgICAgY3JlYXRvcjogZXYuc2VuZGVyID8gZXYuc2VuZGVyLm5hbWUgOiBldi5nZXRTZW5kZXIoKSxcclxuICAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgIHsgZXZlbnRUaWxlcyB9XHJcbiAgICAgICAgICAgIDwvRXZlbnRMaXN0U3VtbWFyeT4sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucmVhZE1hcmtlcikge1xyXG4gICAgICAgICAgICByZXQucHVzaCh0aGlzLnJlYWRNYXJrZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJldDtcclxuICAgIH1cclxuXHJcbiAgICBnZXROZXdQcmV2RXZlbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlRXZlbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFdyYXAgY29uc2VjdXRpdmUgbWVtYmVyIGV2ZW50cyBpbiBhIExpc3RTdW1tYXJ5LCBpZ25vcmUgaWYgcmVkYWN0ZWRcclxuY2xhc3MgTWVtYmVyR3JvdXBlciB7XHJcbiAgICBzdGF0aWMgY2FuU3RhcnRHcm91cCA9IGZ1bmN0aW9uKHBhbmVsLCBldikge1xyXG4gICAgICAgIHJldHVybiBwYW5lbC5fc2hvdWxkU2hvd0V2ZW50KGV2KSAmJiBpc01lbWJlcnNoaXBDaGFuZ2UoZXYpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHBhbmVsLCBldiwgcHJldkV2ZW50LCBsYXN0U2hvd25FdmVudCkge1xyXG4gICAgICAgIHRoaXMucGFuZWwgPSBwYW5lbDtcclxuICAgICAgICB0aGlzLnJlYWRNYXJrZXIgPSBwYW5lbC5fcmVhZE1hcmtlckZvckV2ZW50KFxyXG4gICAgICAgICAgICBldi5nZXRJZCgpLFxyXG4gICAgICAgICAgICBldiA9PT0gbGFzdFNob3duRXZlbnQsXHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLmV2ZW50cyA9IFtldl07XHJcbiAgICAgICAgdGhpcy5wcmV2RXZlbnQgPSBwcmV2RXZlbnQ7XHJcbiAgICAgICAgdGhpcy5sYXN0U2hvd25FdmVudCA9IGxhc3RTaG93bkV2ZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHNob3VsZEdyb3VwKGV2KSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFuZWwuX3dhbnRzRGF0ZVNlcGFyYXRvcih0aGlzLmV2ZW50c1swXSwgZXYuZ2V0RGF0ZSgpKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc01lbWJlcnNoaXBDaGFuZ2UoZXYpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZChldikge1xyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgPT09ICdtLnJvb20ubWVtYmVyJykge1xyXG4gICAgICAgICAgICAvLyBXZSdsbCBqdXN0IGRvdWJsZSBjaGVjayB0aGF0IGl0J3Mgd29ydGggb3VyIHRpbWUgdG8gZG8gc28sIHRocm91Z2ggYW5cclxuICAgICAgICAgICAgLy8gdWdseSBoYWNrLiBJZiB0ZXh0Rm9yRXZlbnQgcmV0dXJucyBzb21ldGhpbmcsIHdlIHNob3VsZCBncm91cCBpdCBmb3JcclxuICAgICAgICAgICAgLy8gcmVuZGVyaW5nIGJ1dCBpZiBpdCBkb2Vzbid0IHRoZW4gd2UnbGwgZXhjbHVkZSBpdC5cclxuICAgICAgICAgICAgY29uc3QgcmVuZGVyVGV4dCA9IHRleHRGb3JFdmVudChldik7XHJcbiAgICAgICAgICAgIGlmICghcmVuZGVyVGV4dCB8fCByZW5kZXJUZXh0LnRyaW0oKS5sZW5ndGggPT09IDApIHJldHVybjsgLy8gcXVpZXRseSBpZ25vcmVcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZWFkTWFya2VyID0gdGhpcy5yZWFkTWFya2VyIHx8IHRoaXMucGFuZWwuX3JlYWRNYXJrZXJGb3JFdmVudChcclxuICAgICAgICAgICAgZXYuZ2V0SWQoKSxcclxuICAgICAgICAgICAgZXYgPT09IHRoaXMubGFzdFNob3duRXZlbnQsXHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLmV2ZW50cy5wdXNoKGV2KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUaWxlcygpIHtcclxuICAgICAgICAvLyBJZiB3ZSBkb24ndCBoYXZlIGFueSBldmVudHMgdG8gZ3JvdXAsIGRvbid0IGV2ZW4gdHJ5IHRvIGdyb3VwIHRoZW0uIFRoZSBsb2dpY1xyXG4gICAgICAgIC8vIGJlbG93IGFzc3VtZXMgdGhhdCB3ZSBoYXZlIGEgZ3JvdXAgb2YgZXZlbnRzIHRvIGRlYWwgd2l0aCwgYnV0IHdlIG1pZ2h0IG5vdCBpZlxyXG4gICAgICAgIC8vIHRoZSBldmVudHMgd2Ugd2VyZSBzdXBwb3NlZCB0byBncm91cCB3ZXJlIHJlZGFjdGVkLlxyXG4gICAgICAgIGlmICghdGhpcy5ldmVudHMgfHwgIXRoaXMuZXZlbnRzLmxlbmd0aCkgcmV0dXJuIFtdO1xyXG5cclxuICAgICAgICBjb25zdCBEYXRlU2VwYXJhdG9yID0gc2RrLmdldENvbXBvbmVudCgnbWVzc2FnZXMuRGF0ZVNlcGFyYXRvcicpO1xyXG4gICAgICAgIGNvbnN0IE1lbWJlckV2ZW50TGlzdFN1bW1hcnkgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5NZW1iZXJFdmVudExpc3RTdW1tYXJ5Jyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHBhbmVsID0gdGhpcy5wYW5lbDtcclxuICAgICAgICBjb25zdCBsYXN0U2hvd25FdmVudCA9IHRoaXMubGFzdFNob3duRXZlbnQ7XHJcbiAgICAgICAgY29uc3QgcmV0ID0gW107XHJcblxyXG4gICAgICAgIGlmIChwYW5lbC5fd2FudHNEYXRlU2VwYXJhdG9yKHRoaXMucHJldkV2ZW50LCB0aGlzLmV2ZW50c1swXS5nZXREYXRlKCkpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRzID0gdGhpcy5ldmVudHNbMF0uZ2V0VHMoKTtcclxuICAgICAgICAgICAgcmV0LnB1c2goXHJcbiAgICAgICAgICAgICAgICA8bGkga2V5PXt0cysnfid9PjxEYXRlU2VwYXJhdG9yIGtleT17dHMrJ34nfSB0cz17dHN9IC8+PC9saT4sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBFbnN1cmUgdGhhdCB0aGUga2V5IG9mIHRoZSBNZW1iZXJFdmVudExpc3RTdW1tYXJ5IGRvZXMgbm90IGNoYW5nZSB3aXRoIG5ld1xyXG4gICAgICAgIC8vIG1lbWJlciBldmVudHMuIFRoaXMgd2lsbCBwcmV2ZW50IGl0IGZyb20gYmVpbmcgcmUtY3JlYXRlZCB1bm5lY2Vzc2FyaWx5LCBhbmRcclxuICAgICAgICAvLyBpbnN0ZWFkIHdpbGwgYWxsb3cgbmV3IHByb3BzIHRvIGJlIHByb3ZpZGVkLiBJbiB0dXJuLCB0aGUgc2hvdWxkQ29tcG9uZW50VXBkYXRlXHJcbiAgICAgICAgLy8gbWV0aG9kIG9uIE1FTFMgY2FuIGJlIHVzZWQgdG8gcHJldmVudCB1bm5lY2Vzc2FyeSByZW5kZXJpbmdzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gV2hpbHN0IGJhY2stcGFnaW5hdGluZyB3aXRoIGEgTUVMUyBhdCB0aGUgdG9wIG9mIHRoZSBwYW5lbCwgcHJldkV2ZW50IHdpbGwgYmUgbnVsbCxcclxuICAgICAgICAvLyBzbyB1c2UgdGhlIGtleSBcIm1lbWJlcmV2ZW50bGlzdHN1bW1hcnktaW5pdGlhbFwiLiBPdGhlcndpc2UsIHVzZSB0aGUgSUQgb2YgdGhlIGZpcnN0XHJcbiAgICAgICAgLy8gbWVtYmVyc2hpcCBldmVudCwgd2hpY2ggd2lsbCBub3QgY2hhbmdlIGR1cmluZyBmb3J3YXJkIHBhZ2luYXRpb24uXHJcbiAgICAgICAgY29uc3Qga2V5ID0gXCJtZW1iZXJldmVudGxpc3RzdW1tYXJ5LVwiICsgKFxyXG4gICAgICAgICAgICB0aGlzLnByZXZFdmVudCA/IHRoaXMuZXZlbnRzWzBdLmdldElkKCkgOiBcImluaXRpYWxcIlxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCBoaWdobGlnaHRJbk1lbHM7XHJcbiAgICAgICAgbGV0IGV2ZW50VGlsZXMgPSB0aGlzLmV2ZW50cy5tYXAoKGUpID0+IHtcclxuICAgICAgICAgICAgaWYgKGUuZ2V0SWQoKSA9PT0gcGFuZWwucHJvcHMuaGlnaGxpZ2h0ZWRFdmVudElkKSB7XHJcbiAgICAgICAgICAgICAgICBoaWdobGlnaHRJbk1lbHMgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEluIG9yZGVyIHRvIHByZXZlbnQgRGF0ZVNlcGFyYXRvcnMgZnJvbSBhcHBlYXJpbmcgaW4gdGhlIGV4cGFuZGVkIGZvcm1cclxuICAgICAgICAgICAgLy8gb2YgTWVtYmVyRXZlbnRMaXN0U3VtbWFyeSwgcmVuZGVyIGVhY2ggbWVtYmVyIGV2ZW50IGFzIGlmIHRoZSBwcmV2aW91c1xyXG4gICAgICAgICAgICAvLyBvbmUgd2FzIGl0c2VsZi4gVGhpcyB3YXksIHRoZSB0aW1lc3RhbXAgb2YgdGhlIHByZXZpb3VzIGV2ZW50ID09PSB0aGVcclxuICAgICAgICAgICAgLy8gdGltZXN0YW1wIG9mIHRoZSBjdXJyZW50IGV2ZW50LCBhbmQgbm8gRGF0ZVNlcGFyYXRvciBpcyBpbnNlcnRlZC5cclxuICAgICAgICAgICAgcmV0dXJuIHBhbmVsLl9nZXRUaWxlc0ZvckV2ZW50KGUsIGUsIGUgPT09IGxhc3RTaG93bkV2ZW50KTtcclxuICAgICAgICB9KS5yZWR1Y2UoKGEsIGIpID0+IGEuY29uY2F0KGIpLCBbXSk7XHJcblxyXG4gICAgICAgIGlmIChldmVudFRpbGVzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICBldmVudFRpbGVzID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldC5wdXNoKFxyXG4gICAgICAgICAgICA8TWVtYmVyRXZlbnRMaXN0U3VtbWFyeSBrZXk9e2tleX1cclxuICAgICAgICAgICAgICAgICBldmVudHM9e3RoaXMuZXZlbnRzfVxyXG4gICAgICAgICAgICAgICAgIG9uVG9nZ2xlPXtwYW5lbC5fb25IZWlnaHRDaGFuZ2VkfSAvLyBVcGRhdGUgc2Nyb2xsIHN0YXRlXHJcbiAgICAgICAgICAgICAgICAgc3RhcnRFeHBhbmRlZD17aGlnaGxpZ2h0SW5NZWxzfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgeyBldmVudFRpbGVzIH1cclxuICAgICAgICAgICAgPC9NZW1iZXJFdmVudExpc3RTdW1tYXJ5PixcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5yZWFkTWFya2VyKSB7XHJcbiAgICAgICAgICAgIHJldC5wdXNoKHRoaXMucmVhZE1hcmtlcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5ld1ByZXZFdmVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5ldmVudHNbMF07XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIGFsbCB0aGUgZ3JvdXBlciBjbGFzc2VzIHRoYXQgd2UgdXNlXHJcbmNvbnN0IGdyb3VwZXJzID0gW0NyZWF0aW9uR3JvdXBlciwgTWVtYmVyR3JvdXBlcl07XHJcbiJdfQ==