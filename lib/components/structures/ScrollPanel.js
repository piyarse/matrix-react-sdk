"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Keyboard = require("../../Keyboard");

var _Timer = _interopRequireDefault(require("../../utils/Timer"));

var _AutoHideScrollbar = _interopRequireDefault(require("./AutoHideScrollbar"));

/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const DEBUG_SCROLL = false; // The amount of extra scroll distance to allow prior to unfilling.
// See _getExcessHeight.

const UNPAGINATION_PADDING = 6000; // The number of milliseconds to debounce calls to onUnfillRequest, to prevent
// many scroll events causing many unfilling requests.

const UNFILL_REQUEST_DEBOUNCE_MS = 200; // _updateHeight makes the height a ceiled multiple of this so we
// don't have to update the height too often. It also allows the user
// to scroll past the pagination spinner a bit so they don't feel blocked so
// much while the content loads.

const PAGE_SIZE = 400;
let debuglog;

if (DEBUG_SCROLL) {
  // using bind means that we get to keep useful line numbers in the console
  debuglog = console.log.bind(console, "ScrollPanel debuglog:");
} else {
  debuglog = function () {};
}
/* This component implements an intelligent scrolling list.
 *
 * It wraps a list of <li> children; when items are added to the start or end
 * of the list, the scroll position is updated so that the user still sees the
 * same position in the list.
 *
 * It also provides a hook which allows parents to provide more list elements
 * when we get close to the start or end of the list.
 *
 * Each child element should have a 'data-scroll-tokens'. This string of
 * comma-separated tokens may contain a single token or many, where many indicates
 * that the element contains elements that have scroll tokens themselves. The first
 * token in 'data-scroll-tokens' is used to serialise the scroll state, and returned
 * as the 'trackedScrollToken' attribute by getScrollState().
 *
 * IMPORTANT: INDIVIDUAL TOKENS WITHIN 'data-scroll-tokens' MUST NOT CONTAIN COMMAS.
 *
 * Some notes about the implementation:
 *
 * The saved 'scrollState' can exist in one of two states:
 *
 *   - stuckAtBottom: (the default, and restored by resetScrollState): the
 *     viewport is scrolled down as far as it can be. When the children are
 *     updated, the scroll position will be updated to ensure it is still at
 *     the bottom.
 *
 *   - fixed, in which the viewport is conceptually tied at a specific scroll
 *     offset.  We don't save the absolute scroll offset, because that would be
 *     affected by window width, zoom level, amount of scrollback, etc. Instead
 *     we save an identifier for the last fully-visible message, and the number
 *     of pixels the window was scrolled below it - which is hopefully near
 *     enough.
 *
 * The 'stickyBottom' property controls the behaviour when we reach the bottom
 * of the window (either through a user-initiated scroll, or by calling
 * scrollToBottom). If stickyBottom is enabled, the scrollState will enter
 * 'stuckAtBottom' state - ensuring that new additions cause the window to
 * scroll down further. If stickyBottom is disabled, we just save the scroll
 * offset as normal.
 */


var _default = (0, _createReactClass.default)({
  displayName: 'ScrollPanel',
  propTypes: {
    /* stickyBottom: if set to true, then once the user hits the bottom of
     * the list, any new children added to the list will cause the list to
     * scroll down to show the new element, rather than preserving the
     * existing view.
     */
    stickyBottom: _propTypes.default.bool,

    /* startAtBottom: if set to true, the view is assumed to start
     * scrolled to the bottom.
     * XXX: It's likley this is unecessary and can be derived from
     * stickyBottom, but I'm adding an extra parameter to ensure
     * behaviour stays the same for other uses of ScrollPanel.
     * If so, let's remove this parameter down the line.
     */
    startAtBottom: _propTypes.default.bool,

    /* onFillRequest(backwards): a callback which is called on scroll when
     * the user nears the start (backwards = true) or end (backwards =
     * false) of the list.
     *
     * This should return a promise; no more calls will be made until the
     * promise completes.
     *
     * The promise should resolve to true if there is more data to be
     * retrieved in this direction (in which case onFillRequest may be
     * called again immediately), or false if there is no more data in this
     * directon (at this time) - which will stop the pagination cycle until
     * the user scrolls again.
     */
    onFillRequest: _propTypes.default.func,

    /* onUnfillRequest(backwards): a callback which is called on scroll when
     * there are children elements that are far out of view and could be removed
     * without causing pagination to occur.
     *
     * This function should accept a boolean, which is true to indicate the back/top
     * of the panel and false otherwise, and a scroll token, which refers to the
     * first element to remove if removing from the front/bottom, and last element
     * to remove if removing from the back/top.
     */
    onUnfillRequest: _propTypes.default.func,

    /* onScroll: a callback which is called whenever any scroll happens.
     */
    onScroll: _propTypes.default.func,

    /* className: classnames to add to the top-level div
     */
    className: _propTypes.default.string,

    /* style: styles to add to the top-level div
     */
    style: _propTypes.default.object,

    /* resizeNotifier: ResizeNotifier to know when middle column has changed size
     */
    resizeNotifier: _propTypes.default.object
  },
  getDefaultProps: function () {
    return {
      stickyBottom: true,
      startAtBottom: true,
      onFillRequest: function (backwards) {
        return Promise.resolve(false);
      },
      onUnfillRequest: function (backwards, scrollToken) {},
      onScroll: function () {}
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._pendingFillRequests = {
      b: null,
      f: null
    };

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.on("middlePanelResized", this.onResize);
    }

    this.resetScrollState();
    this._itemlist = (0, _react.createRef)();
  },
  componentDidMount: function () {
    this.checkScroll();
  },
  componentDidUpdate: function () {
    // after adding event tiles, we may need to tweak the scroll (either to
    // keep at the bottom of the timeline, or to maintain the view after
    // adding events to the top).
    //
    // This will also re-check the fill state, in case the paginate was inadequate
    this.checkScroll();
    this.updatePreventShrinking();
  },
  componentWillUnmount: function () {
    // set a boolean to say we've been unmounted, which any pending
    // promises can use to throw away their results.
    //
    // (We could use isMounted(), but facebook have deprecated that.)
    this.unmounted = true;

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.removeListener("middlePanelResized", this.onResize);
    }
  },
  onScroll: function (ev) {
    debuglog("onScroll", this._getScrollNode().scrollTop);

    this._scrollTimeout.restart();

    this._saveScrollState();

    this.updatePreventShrinking();
    this.props.onScroll(ev);
    this.checkFillState();
  },
  onResize: function () {
    this.checkScroll(); // update preventShrinkingState if present

    if (this.preventShrinkingState) {
      this.preventShrinking();
    }
  },
  // after an update to the contents of the panel, check that the scroll is
  // where it ought to be, and set off pagination requests if necessary.
  checkScroll: function () {
    if (this.unmounted) {
      return;
    }

    this._restoreSavedScrollState();

    this.checkFillState();
  },
  // return true if the content is fully scrolled down right now; else false.
  //
  // note that this is independent of the 'stuckAtBottom' state - it is simply
  // about whether the content is scrolled down right now, irrespective of
  // whether it will stay that way when the children update.
  isAtBottom: function () {
    const sn = this._getScrollNode(); // fractional values (both too big and too small)
    // for scrollTop happen on certain browsers/platforms
    // when scrolled all the way down. E.g. Chrome 72 on debian.
    // so check difference <= 1;


    return Math.abs(sn.scrollHeight - (sn.scrollTop + sn.clientHeight)) <= 1;
  },
  // returns the vertical height in the given direction that can be removed from
  // the content box (which has a height of scrollHeight, see checkFillState) without
  // pagination occuring.
  //
  // padding* = UNPAGINATION_PADDING
  //
  // ### Region determined as excess.
  //
  //   .---------.                        -              -
  //   |#########|                        |              |
  //   |#########|   -                    |  scrollTop   |
  //   |         |   | padding*           |              |
  //   |         |   |                    |              |
  // .-+---------+-. -  -                 |              |
  // : |         | :    |                 |              |
  // : |         | :    |  clientHeight   |              |
  // : |         | :    |                 |              |
  // .-+---------+-.    -                 -              |
  // | |         | |    |                                |
  // | |         | |    |  clientHeight                  | scrollHeight
  // | |         | |    |                                |
  // `-+---------+-'    -                                |
  // : |         | :    |                                |
  // : |         | :    |  clientHeight                  |
  // : |         | :    |                                |
  // `-+---------+-' -  -                                |
  //   |         |   | padding*                          |
  //   |         |   |                                   |
  //   |#########|   -                                   |
  //   |#########|                                       |
  //   `---------'                                       -
  _getExcessHeight: function (backwards) {
    const sn = this._getScrollNode();

    const contentHeight = this._getMessagesHeight();

    const listHeight = this._getListHeight();

    const clippedHeight = contentHeight - listHeight;
    const unclippedScrollTop = sn.scrollTop + clippedHeight;

    if (backwards) {
      return unclippedScrollTop - sn.clientHeight - UNPAGINATION_PADDING;
    } else {
      return contentHeight - (unclippedScrollTop + 2 * sn.clientHeight) - UNPAGINATION_PADDING;
    }
  },
  // check the scroll state and send out backfill requests if necessary.
  checkFillState: async function (depth = 0) {
    if (this.unmounted) {
      return;
    }

    const isFirstCall = depth === 0;

    const sn = this._getScrollNode(); // if there is less than a screenful of messages above or below the
    // viewport, try to get some more messages.
    //
    // scrollTop is the number of pixels between the top of the content and
    //     the top of the viewport.
    //
    // scrollHeight is the total height of the content.
    //
    // clientHeight is the height of the viewport (excluding borders,
    // margins, and scrollbars).
    //
    //
    //   .---------.          -                 -
    //   |         |          |  scrollTop      |
    // .-+---------+-.    -   -                 |
    // | |         | |    |                     |
    // | |         | |    |  clientHeight       | scrollHeight
    // | |         | |    |                     |
    // `-+---------+-'    -                     |
    //   |         |                            |
    //   |         |                            |
    //   `---------'                            -
    //
    // as filling is async and recursive,
    // don't allow more than 1 chain of calls concurrently
    // do make a note when a new request comes in while already running one,
    // so we can trigger a new chain of calls once done.


    if (isFirstCall) {
      if (this._isFilling) {
        debuglog("_isFilling: not entering while request is ongoing, marking for a subsequent request");
        this._fillRequestWhileRunning = true;
        return;
      }

      debuglog("_isFilling: setting");
      this._isFilling = true;
    }

    const itemlist = this._itemlist.current;
    const firstTile = itemlist && itemlist.firstElementChild;
    const contentTop = firstTile && firstTile.offsetTop;
    const fillPromises = []; // if scrollTop gets to 1 screen from the top of the first tile,
    // try backward filling

    if (!firstTile || sn.scrollTop - contentTop < sn.clientHeight) {
      // need to back-fill
      fillPromises.push(this._maybeFill(depth, true));
    } // if scrollTop gets to 2 screens from the end (so 1 screen below viewport),
    // try forward filling


    if (sn.scrollHeight - sn.scrollTop < sn.clientHeight * 2) {
      // need to forward-fill
      fillPromises.push(this._maybeFill(depth, false));
    }

    if (fillPromises.length) {
      try {
        await Promise.all(fillPromises);
      } catch (err) {
        console.error(err);
      }
    }

    if (isFirstCall) {
      debuglog("_isFilling: clearing");
      this._isFilling = false;
    }

    if (this._fillRequestWhileRunning) {
      this._fillRequestWhileRunning = false;
      this.checkFillState();
    }
  },
  // check if unfilling is possible and send an unfill request if necessary
  _checkUnfillState: function (backwards) {
    let excessHeight = this._getExcessHeight(backwards);

    if (excessHeight <= 0) {
      return;
    }

    const origExcessHeight = excessHeight;
    const tiles = this._itemlist.current.children; // The scroll token of the first/last tile to be unpaginated

    let markerScrollToken = null; // Subtract heights of tiles to simulate the tiles being unpaginated until the
    // excess height is less than the height of the next tile to subtract. This
    // prevents excessHeight becoming negative, which could lead to future
    // pagination.
    //
    // If backwards is true, we unpaginate (remove) tiles from the back (top).

    let tile;

    for (let i = 0; i < tiles.length; i++) {
      tile = tiles[backwards ? i : tiles.length - 1 - i]; // Subtract height of tile as if it were unpaginated

      excessHeight -= tile.clientHeight; //If removing the tile would lead to future pagination, break before setting scroll token

      if (tile.clientHeight > excessHeight) {
        break;
      } // The tile may not have a scroll token, so guard it


      if (tile.dataset.scrollTokens) {
        markerScrollToken = tile.dataset.scrollTokens.split(',')[0];
      }
    }

    if (markerScrollToken) {
      // Use a debouncer to prevent multiple unfill calls in quick succession
      // This is to make the unfilling process less aggressive
      if (this._unfillDebouncer) {
        clearTimeout(this._unfillDebouncer);
      }

      this._unfillDebouncer = setTimeout(() => {
        this._unfillDebouncer = null;
        debuglog("unfilling now", backwards, origExcessHeight);
        this.props.onUnfillRequest(backwards, markerScrollToken);
      }, UNFILL_REQUEST_DEBOUNCE_MS);
    }
  },
  // check if there is already a pending fill request. If not, set one off.
  _maybeFill: function (depth, backwards) {
    const dir = backwards ? 'b' : 'f';

    if (this._pendingFillRequests[dir]) {
      debuglog("Already a " + dir + " fill in progress - not starting another");
      return;
    }

    debuglog("starting " + dir + " fill"); // onFillRequest can end up calling us recursively (via onScroll
    // events) so make sure we set this before firing off the call.

    this._pendingFillRequests[dir] = true; // wait 1ms before paginating, because otherwise
    // this will block the scroll event handler for +700ms
    // if messages are already cached in memory,
    // This would cause jumping to happen on Chrome/macOS.

    return new Promise(resolve => setTimeout(resolve, 1)).then(() => {
      return this.props.onFillRequest(backwards);
    }).finally(() => {
      this._pendingFillRequests[dir] = false;
    }).then(hasMoreResults => {
      if (this.unmounted) {
        return;
      } // Unpaginate once filling is complete


      this._checkUnfillState(!backwards);

      debuglog("" + dir + " fill complete; hasMoreResults:" + hasMoreResults);

      if (hasMoreResults) {
        // further pagination requests have been disabled until now, so
        // it's time to check the fill state again in case the pagination
        // was insufficient.
        return this.checkFillState(depth + 1);
      }
    });
  },

  /* get the current scroll state. This returns an object with the following
   * properties:
   *
   * boolean stuckAtBottom: true if we are tracking the bottom of the
   *   scroll. false if we are tracking a particular child.
   *
   * string trackedScrollToken: undefined if stuckAtBottom is true; if it is
   *   false, the first token in data-scroll-tokens of the child which we are
   *   tracking.
   *
   * number bottomOffset: undefined if stuckAtBottom is true; if it is false,
   *   the number of pixels the bottom of the tracked child is above the
   *   bottom of the scroll panel.
   */
  getScrollState: function () {
    return this.scrollState;
  },

  /* reset the saved scroll state.
   *
   * This is useful if the list is being replaced, and you don't want to
   * preserve scroll even if new children happen to have the same scroll
   * tokens as old ones.
   *
   * This will cause the viewport to be scrolled down to the bottom on the
   * next update of the child list. This is different to scrollToBottom(),
   * which would save the current bottom-most child as the active one (so is
   * no use if no children exist yet, or if you are about to replace the
   * child list.)
   */
  resetScrollState: function () {
    this.scrollState = {
      stuckAtBottom: this.props.startAtBottom
    };
    this._bottomGrowth = 0;
    this._pages = 0;
    this._scrollTimeout = new _Timer.default(100);
    this._heightUpdateInProgress = false;
  },

  /**
   * jump to the top of the content.
   */
  scrollToTop: function () {
    this._getScrollNode().scrollTop = 0;

    this._saveScrollState();
  },

  /**
   * jump to the bottom of the content.
   */
  scrollToBottom: function () {
    // the easiest way to make sure that the scroll state is correctly
    // saved is to do the scroll, then save the updated state. (Calculating
    // it ourselves is hard, and we can't rely on an onScroll callback
    // happening, since there may be no user-visible change here).
    const sn = this._getScrollNode();

    sn.scrollTop = sn.scrollHeight;

    this._saveScrollState();
  },

  /**
   * Page up/down.
   *
   * @param {number} mult: -1 to page up, +1 to page down
   */
  scrollRelative: function (mult) {
    const scrollNode = this._getScrollNode();

    const delta = mult * scrollNode.clientHeight * 0.5;
    scrollNode.scrollBy(0, delta);

    this._saveScrollState();
  },

  /**
   * Scroll up/down in response to a scroll key
   * @param {object} ev the keyboard event
   */
  handleScrollKey: function (ev) {
    switch (ev.key) {
      case _Keyboard.Key.PAGE_UP:
        if (!ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
          this.scrollRelative(-1);
        }

        break;

      case _Keyboard.Key.PAGE_DOWN:
        if (!ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
          this.scrollRelative(1);
        }

        break;

      case _Keyboard.Key.HOME:
        if (ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
          this.scrollToTop();
        }

        break;

      case _Keyboard.Key.END:
        if (ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
          this.scrollToBottom();
        }

        break;
    }
  },

  /* Scroll the panel to bring the DOM node with the scroll token
   * `scrollToken` into view.
   *
   * offsetBase gives the reference point for the pixelOffset. 0 means the
   * top of the container, 1 means the bottom, and fractional values mean
   * somewhere in the middle. If omitted, it defaults to 0.
   *
   * pixelOffset gives the number of pixels *above* the offsetBase that the
   * node (specifically, the bottom of it) will be positioned. If omitted, it
   * defaults to 0.
   */
  scrollToToken: function (scrollToken, pixelOffset, offsetBase) {
    pixelOffset = pixelOffset || 0;
    offsetBase = offsetBase || 0; // set the trackedScrollToken so we can get the node through _getTrackedNode

    this.scrollState = {
      stuckAtBottom: false,
      trackedScrollToken: scrollToken
    };

    const trackedNode = this._getTrackedNode();

    const scrollNode = this._getScrollNode();

    if (trackedNode) {
      // set the scrollTop to the position we want.
      // note though, that this might not succeed if the combination of offsetBase and pixelOffset
      // would position the trackedNode towards the top of the viewport.
      // This because when setting the scrollTop only 10 or so events might be loaded,
      // not giving enough content below the trackedNode to scroll downwards
      // enough so it ends up in the top of the viewport.
      debuglog("scrollToken: setting scrollTop", {
        offsetBase,
        pixelOffset,
        offsetTop: trackedNode.offsetTop
      });
      scrollNode.scrollTop = trackedNode.offsetTop - scrollNode.clientHeight * offsetBase + pixelOffset;

      this._saveScrollState();
    }
  },
  _saveScrollState: function () {
    if (this.props.stickyBottom && this.isAtBottom()) {
      this.scrollState = {
        stuckAtBottom: true
      };
      debuglog("saved stuckAtBottom state");
      return;
    }

    const scrollNode = this._getScrollNode();

    const viewportBottom = scrollNode.scrollHeight - (scrollNode.scrollTop + scrollNode.clientHeight);
    const itemlist = this._itemlist.current;
    const messages = itemlist.children;
    let node = null; // TODO: do a binary search here, as items are sorted by offsetTop
    // loop backwards, from bottom-most message (as that is the most common case)

    for (let i = messages.length - 1; i >= 0; --i) {
      if (!messages[i].dataset.scrollTokens) {
        continue;
      }

      node = messages[i]; // break at the first message (coming from the bottom)
      // that has it's offsetTop above the bottom of the viewport.

      if (this._topFromBottom(node) > viewportBottom) {
        // Use this node as the scrollToken
        break;
      }
    }

    if (!node) {
      debuglog("unable to save scroll state: found no children in the viewport");
      return;
    }

    const scrollToken = node.dataset.scrollTokens.split(',')[0];
    debuglog("saving anchored scroll state to message", node && node.innerText, scrollToken);

    const bottomOffset = this._topFromBottom(node);

    this.scrollState = {
      stuckAtBottom: false,
      trackedNode: node,
      trackedScrollToken: scrollToken,
      bottomOffset: bottomOffset,
      pixelOffset: bottomOffset - viewportBottom //needed for restoring the scroll position when coming back to the room

    };
  },
  _restoreSavedScrollState: async function () {
    const scrollState = this.scrollState;

    if (scrollState.stuckAtBottom) {
      const sn = this._getScrollNode();

      sn.scrollTop = sn.scrollHeight;
    } else if (scrollState.trackedScrollToken) {
      const itemlist = this._itemlist.current;

      const trackedNode = this._getTrackedNode();

      if (trackedNode) {
        const newBottomOffset = this._topFromBottom(trackedNode);

        const bottomDiff = newBottomOffset - scrollState.bottomOffset;
        this._bottomGrowth += bottomDiff;
        scrollState.bottomOffset = newBottomOffset;
        itemlist.style.height = "".concat(this._getListHeight(), "px");
        debuglog("balancing height because messages below viewport grew by", bottomDiff);
      }
    }

    if (!this._heightUpdateInProgress) {
      this._heightUpdateInProgress = true;

      try {
        await this._updateHeight();
      } finally {
        this._heightUpdateInProgress = false;
      }
    } else {
      debuglog("not updating height because request already in progress");
    }
  },

  // need a better name that also indicates this will change scrollTop? Rebalance height? Reveal content?
  async _updateHeight() {
    // wait until user has stopped scrolling
    if (this._scrollTimeout.isRunning()) {
      debuglog("updateHeight waiting for scrolling to end ... ");
      await this._scrollTimeout.finished();
    } else {
      debuglog("updateHeight getting straight to business, no scrolling going on.");
    } // We might have unmounted since the timer finished, so abort if so.


    if (this.unmounted) {
      return;
    }

    const sn = this._getScrollNode();

    const itemlist = this._itemlist.current;

    const contentHeight = this._getMessagesHeight();

    const minHeight = sn.clientHeight;
    const height = Math.max(minHeight, contentHeight);
    this._pages = Math.ceil(height / PAGE_SIZE);
    this._bottomGrowth = 0;

    const newHeight = this._getListHeight();

    const scrollState = this.scrollState;

    if (scrollState.stuckAtBottom) {
      itemlist.style.height = "".concat(newHeight, "px");
      sn.scrollTop = sn.scrollHeight;
      debuglog("updateHeight to", newHeight);
    } else if (scrollState.trackedScrollToken) {
      const trackedNode = this._getTrackedNode(); // if the timeline has been reloaded
      // this can be called before scrollToBottom or whatever has been called
      // so don't do anything if the node has disappeared from
      // the currently filled piece of the timeline


      if (trackedNode) {
        const oldTop = trackedNode.offsetTop;
        itemlist.style.height = "".concat(newHeight, "px");
        const newTop = trackedNode.offsetTop;
        const topDiff = newTop - oldTop; // important to scroll by a relative amount as
        // reading scrollTop and then setting it might
        // yield out of date values and cause a jump
        // when setting it

        sn.scrollBy(0, topDiff);
        debuglog("updateHeight to", {
          newHeight,
          topDiff
        });
      }
    }
  },

  _getTrackedNode() {
    const scrollState = this.scrollState;
    const trackedNode = scrollState.trackedNode;

    if (!trackedNode || !trackedNode.parentElement) {
      let node;
      const messages = this._itemlist.current.children;
      const scrollToken = scrollState.trackedScrollToken;

      for (let i = messages.length - 1; i >= 0; --i) {
        const m = messages[i]; // 'data-scroll-tokens' is a DOMString of comma-separated scroll tokens
        // There might only be one scroll token

        if (m.dataset.scrollTokens && m.dataset.scrollTokens.split(',').indexOf(scrollToken) !== -1) {
          node = m;
          break;
        }
      }

      if (node) {
        debuglog("had to find tracked node again for " + scrollState.trackedScrollToken);
      }

      scrollState.trackedNode = node;
    }

    if (!scrollState.trackedNode) {
      debuglog("No node with ; '" + scrollState.trackedScrollToken + "'");
      return;
    }

    return scrollState.trackedNode;
  },

  _getListHeight() {
    return this._bottomGrowth + this._pages * PAGE_SIZE;
  },

  _getMessagesHeight() {
    const itemlist = this._itemlist.current;
    const lastNode = itemlist.lastElementChild;
    const lastNodeBottom = lastNode ? lastNode.offsetTop + lastNode.clientHeight : 0;
    const firstNodeTop = itemlist.firstElementChild ? itemlist.firstElementChild.offsetTop : 0; // 18 is itemlist padding

    return lastNodeBottom - firstNodeTop + 18 * 2;
  },

  _topFromBottom(node) {
    // current capped height - distance from top = distance from bottom of container to top of tracked element
    return this._itemlist.current.clientHeight - node.offsetTop;
  },

  /* get the DOM node which has the scrollTop property we care about for our
   * message panel.
   */
  _getScrollNode: function () {
    if (this.unmounted) {
      // this shouldn't happen, but when it does, turn the NPE into
      // something more meaningful.
      throw new Error("ScrollPanel._getScrollNode called when unmounted");
    }

    if (!this._divScroll) {
      // Likewise, we should have the ref by this point, but if not
      // turn the NPE into something meaningful.
      throw new Error("ScrollPanel._getScrollNode called before AutoHideScrollbar ref collected");
    }

    return this._divScroll;
  },
  _collectScroll: function (divScroll) {
    this._divScroll = divScroll;
  },

  /**
  Mark the bottom offset of the last tile so we can balance it out when
  anything below it changes, by calling updatePreventShrinking, to keep
  the same minimum bottom offset, effectively preventing the timeline to shrink.
  */
  preventShrinking: function () {
    const messageList = this._itemlist.current;
    const tiles = messageList && messageList.children;

    if (!messageList) {
      return;
    }

    let lastTileNode;

    for (let i = tiles.length - 1; i >= 0; i--) {
      const node = tiles[i];

      if (node.dataset.scrollTokens) {
        lastTileNode = node;
        break;
      }
    }

    if (!lastTileNode) {
      return;
    }

    this.clearPreventShrinking();
    const offsetFromBottom = messageList.clientHeight - (lastTileNode.offsetTop + lastTileNode.clientHeight);
    this.preventShrinkingState = {
      offsetFromBottom: offsetFromBottom,
      offsetNode: lastTileNode
    };
    debuglog("prevent shrinking, last tile ", offsetFromBottom, "px from bottom");
  },

  /** Clear shrinking prevention. Used internally, and when the timeline is reloaded. */
  clearPreventShrinking: function () {
    const messageList = this._itemlist.current;
    const balanceElement = messageList && messageList.parentElement;
    if (balanceElement) balanceElement.style.paddingBottom = null;
    this.preventShrinkingState = null;
    debuglog("prevent shrinking cleared");
  },

  /**
  update the container padding to balance
  the bottom offset of the last tile since
  preventShrinking was called.
  Clears the prevent-shrinking state ones the offset
  from the bottom of the marked tile grows larger than
  what it was when marking.
  */
  updatePreventShrinking: function () {
    if (this.preventShrinkingState) {
      const sn = this._getScrollNode();

      const scrollState = this.scrollState;
      const messageList = this._itemlist.current;
      const {
        offsetNode,
        offsetFromBottom
      } = this.preventShrinkingState; // element used to set paddingBottom to balance the typing notifs disappearing

      const balanceElement = messageList.parentElement; // if the offsetNode got unmounted, clear

      let shouldClear = !offsetNode.parentElement; // also if 200px from bottom

      if (!shouldClear && !scrollState.stuckAtBottom) {
        const spaceBelowViewport = sn.scrollHeight - (sn.scrollTop + sn.clientHeight);
        shouldClear = spaceBelowViewport >= 200;
      } // try updating if not clearing


      if (!shouldClear) {
        const currentOffset = messageList.clientHeight - (offsetNode.offsetTop + offsetNode.clientHeight);
        const offsetDiff = offsetFromBottom - currentOffset;

        if (offsetDiff > 0) {
          balanceElement.style.paddingBottom = "".concat(offsetDiff, "px");
          debuglog("update prevent shrinking ", offsetDiff, "px from bottom");
        } else if (offsetDiff < 0) {
          shouldClear = true;
        }
      }

      if (shouldClear) {
        this.clearPreventShrinking();
      }
    }
  },
  render: function () {
    // TODO: the classnames on the div and ol could do with being updated to
    // reflect the fact that we don't necessarily contain a list of messages.
    // it's not obvious why we have a separate div and ol anyway.
    // give the <ol> an explicit role=list because Safari+VoiceOver seems to think an ordered-list with
    // list-style-type: none; is no longer a list
    return _react.default.createElement(_AutoHideScrollbar.default, {
      wrappedRef: this._collectScroll,
      onScroll: this.onScroll,
      className: "mx_ScrollPanel ".concat(this.props.className),
      style: this.props.style
    }, _react.default.createElement("div", {
      className: "mx_RoomView_messageListWrapper"
    }, _react.default.createElement("ol", {
      ref: this._itemlist,
      className: "mx_RoomView_MessageList",
      "aria-live": "polite",
      role: "list"
    }, this.props.children)));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvU2Nyb2xsUGFuZWwuanMiXSwibmFtZXMiOlsiREVCVUdfU0NST0xMIiwiVU5QQUdJTkFUSU9OX1BBRERJTkciLCJVTkZJTExfUkVRVUVTVF9ERUJPVU5DRV9NUyIsIlBBR0VfU0laRSIsImRlYnVnbG9nIiwiY29uc29sZSIsImxvZyIsImJpbmQiLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsInN0aWNreUJvdHRvbSIsIlByb3BUeXBlcyIsImJvb2wiLCJzdGFydEF0Qm90dG9tIiwib25GaWxsUmVxdWVzdCIsImZ1bmMiLCJvblVuZmlsbFJlcXVlc3QiLCJvblNjcm9sbCIsImNsYXNzTmFtZSIsInN0cmluZyIsInN0eWxlIiwib2JqZWN0IiwicmVzaXplTm90aWZpZXIiLCJnZXREZWZhdWx0UHJvcHMiLCJiYWNrd2FyZHMiLCJQcm9taXNlIiwicmVzb2x2ZSIsInNjcm9sbFRva2VuIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIl9wZW5kaW5nRmlsbFJlcXVlc3RzIiwiYiIsImYiLCJwcm9wcyIsIm9uIiwib25SZXNpemUiLCJyZXNldFNjcm9sbFN0YXRlIiwiX2l0ZW1saXN0IiwiY29tcG9uZW50RGlkTW91bnQiLCJjaGVja1Njcm9sbCIsImNvbXBvbmVudERpZFVwZGF0ZSIsInVwZGF0ZVByZXZlbnRTaHJpbmtpbmciLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVubW91bnRlZCIsInJlbW92ZUxpc3RlbmVyIiwiZXYiLCJfZ2V0U2Nyb2xsTm9kZSIsInNjcm9sbFRvcCIsIl9zY3JvbGxUaW1lb3V0IiwicmVzdGFydCIsIl9zYXZlU2Nyb2xsU3RhdGUiLCJjaGVja0ZpbGxTdGF0ZSIsInByZXZlbnRTaHJpbmtpbmdTdGF0ZSIsInByZXZlbnRTaHJpbmtpbmciLCJfcmVzdG9yZVNhdmVkU2Nyb2xsU3RhdGUiLCJpc0F0Qm90dG9tIiwic24iLCJNYXRoIiwiYWJzIiwic2Nyb2xsSGVpZ2h0IiwiY2xpZW50SGVpZ2h0IiwiX2dldEV4Y2Vzc0hlaWdodCIsImNvbnRlbnRIZWlnaHQiLCJfZ2V0TWVzc2FnZXNIZWlnaHQiLCJsaXN0SGVpZ2h0IiwiX2dldExpc3RIZWlnaHQiLCJjbGlwcGVkSGVpZ2h0IiwidW5jbGlwcGVkU2Nyb2xsVG9wIiwiZGVwdGgiLCJpc0ZpcnN0Q2FsbCIsIl9pc0ZpbGxpbmciLCJfZmlsbFJlcXVlc3RXaGlsZVJ1bm5pbmciLCJpdGVtbGlzdCIsImN1cnJlbnQiLCJmaXJzdFRpbGUiLCJmaXJzdEVsZW1lbnRDaGlsZCIsImNvbnRlbnRUb3AiLCJvZmZzZXRUb3AiLCJmaWxsUHJvbWlzZXMiLCJwdXNoIiwiX21heWJlRmlsbCIsImxlbmd0aCIsImFsbCIsImVyciIsImVycm9yIiwiX2NoZWNrVW5maWxsU3RhdGUiLCJleGNlc3NIZWlnaHQiLCJvcmlnRXhjZXNzSGVpZ2h0IiwidGlsZXMiLCJjaGlsZHJlbiIsIm1hcmtlclNjcm9sbFRva2VuIiwidGlsZSIsImkiLCJkYXRhc2V0Iiwic2Nyb2xsVG9rZW5zIiwic3BsaXQiLCJfdW5maWxsRGVib3VuY2VyIiwiY2xlYXJUaW1lb3V0Iiwic2V0VGltZW91dCIsImRpciIsInRoZW4iLCJmaW5hbGx5IiwiaGFzTW9yZVJlc3VsdHMiLCJnZXRTY3JvbGxTdGF0ZSIsInNjcm9sbFN0YXRlIiwic3R1Y2tBdEJvdHRvbSIsIl9ib3R0b21Hcm93dGgiLCJfcGFnZXMiLCJUaW1lciIsIl9oZWlnaHRVcGRhdGVJblByb2dyZXNzIiwic2Nyb2xsVG9Ub3AiLCJzY3JvbGxUb0JvdHRvbSIsInNjcm9sbFJlbGF0aXZlIiwibXVsdCIsInNjcm9sbE5vZGUiLCJkZWx0YSIsInNjcm9sbEJ5IiwiaGFuZGxlU2Nyb2xsS2V5Iiwia2V5IiwiS2V5IiwiUEFHRV9VUCIsImN0cmxLZXkiLCJzaGlmdEtleSIsImFsdEtleSIsIm1ldGFLZXkiLCJQQUdFX0RPV04iLCJIT01FIiwiRU5EIiwic2Nyb2xsVG9Ub2tlbiIsInBpeGVsT2Zmc2V0Iiwib2Zmc2V0QmFzZSIsInRyYWNrZWRTY3JvbGxUb2tlbiIsInRyYWNrZWROb2RlIiwiX2dldFRyYWNrZWROb2RlIiwidmlld3BvcnRCb3R0b20iLCJtZXNzYWdlcyIsIm5vZGUiLCJfdG9wRnJvbUJvdHRvbSIsImlubmVyVGV4dCIsImJvdHRvbU9mZnNldCIsIm5ld0JvdHRvbU9mZnNldCIsImJvdHRvbURpZmYiLCJoZWlnaHQiLCJfdXBkYXRlSGVpZ2h0IiwiaXNSdW5uaW5nIiwiZmluaXNoZWQiLCJtaW5IZWlnaHQiLCJtYXgiLCJjZWlsIiwibmV3SGVpZ2h0Iiwib2xkVG9wIiwibmV3VG9wIiwidG9wRGlmZiIsInBhcmVudEVsZW1lbnQiLCJtIiwiaW5kZXhPZiIsImxhc3ROb2RlIiwibGFzdEVsZW1lbnRDaGlsZCIsImxhc3ROb2RlQm90dG9tIiwiZmlyc3ROb2RlVG9wIiwiRXJyb3IiLCJfZGl2U2Nyb2xsIiwiX2NvbGxlY3RTY3JvbGwiLCJkaXZTY3JvbGwiLCJtZXNzYWdlTGlzdCIsImxhc3RUaWxlTm9kZSIsImNsZWFyUHJldmVudFNocmlua2luZyIsIm9mZnNldEZyb21Cb3R0b20iLCJvZmZzZXROb2RlIiwiYmFsYW5jZUVsZW1lbnQiLCJwYWRkaW5nQm90dG9tIiwic2hvdWxkQ2xlYXIiLCJzcGFjZUJlbG93Vmlld3BvcnQiLCJjdXJyZW50T2Zmc2V0Iiwib2Zmc2V0RGlmZiIsInJlbmRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckJBOzs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsTUFBTUEsWUFBWSxHQUFHLEtBQXJCLEMsQ0FFQTtBQUNBOztBQUNBLE1BQU1DLG9CQUFvQixHQUFHLElBQTdCLEMsQ0FDQTtBQUNBOztBQUNBLE1BQU1DLDBCQUEwQixHQUFHLEdBQW5DLEMsQ0FDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxNQUFNQyxTQUFTLEdBQUcsR0FBbEI7QUFFQSxJQUFJQyxRQUFKOztBQUNBLElBQUlKLFlBQUosRUFBa0I7QUFDZDtBQUNBSSxFQUFBQSxRQUFRLEdBQUdDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQUFaLENBQWlCRixPQUFqQixFQUEwQix1QkFBMUIsQ0FBWDtBQUNILENBSEQsTUFHTztBQUNIRCxFQUFBQSxRQUFRLEdBQUcsWUFBVyxDQUFFLENBQXhCO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBeUNlLCtCQUFpQjtBQUM1QkksRUFBQUEsV0FBVyxFQUFFLGFBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQOzs7OztBQUtBQyxJQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxJQU5qQjs7QUFRUDs7Ozs7OztBQU9BQyxJQUFBQSxhQUFhLEVBQUVGLG1CQUFVQyxJQWZsQjs7QUFpQlA7Ozs7Ozs7Ozs7Ozs7QUFhQUUsSUFBQUEsYUFBYSxFQUFFSCxtQkFBVUksSUE5QmxCOztBQWdDUDs7Ozs7Ozs7O0FBU0FDLElBQUFBLGVBQWUsRUFBRUwsbUJBQVVJLElBekNwQjs7QUEyQ1A7O0FBRUFFLElBQUFBLFFBQVEsRUFBRU4sbUJBQVVJLElBN0NiOztBQStDUDs7QUFFQUcsSUFBQUEsU0FBUyxFQUFFUCxtQkFBVVEsTUFqRGQ7O0FBbURQOztBQUVBQyxJQUFBQSxLQUFLLEVBQUVULG1CQUFVVSxNQXJEVjs7QUFzRFA7O0FBRUFDLElBQUFBLGNBQWMsRUFBRVgsbUJBQVVVO0FBeERuQixHQUhpQjtBQThENUJFLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSGIsTUFBQUEsWUFBWSxFQUFFLElBRFg7QUFFSEcsTUFBQUEsYUFBYSxFQUFFLElBRlo7QUFHSEMsTUFBQUEsYUFBYSxFQUFFLFVBQVNVLFNBQVQsRUFBb0I7QUFBRSxlQUFPQyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsS0FBaEIsQ0FBUDtBQUFnQyxPQUhsRTtBQUlIVixNQUFBQSxlQUFlLEVBQUUsVUFBU1EsU0FBVCxFQUFvQkcsV0FBcEIsRUFBaUMsQ0FBRSxDQUpqRDtBQUtIVixNQUFBQSxRQUFRLEVBQUUsWUFBVyxDQUFFO0FBTHBCLEtBQVA7QUFPSCxHQXRFMkI7QUF3RTVCO0FBQ0FXLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsU0FBS0Msb0JBQUwsR0FBNEI7QUFBQ0MsTUFBQUEsQ0FBQyxFQUFFLElBQUo7QUFBVUMsTUFBQUEsQ0FBQyxFQUFFO0FBQWIsS0FBNUI7O0FBRUEsUUFBSSxLQUFLQyxLQUFMLENBQVdWLGNBQWYsRUFBK0I7QUFDM0IsV0FBS1UsS0FBTCxDQUFXVixjQUFYLENBQTBCVyxFQUExQixDQUE2QixvQkFBN0IsRUFBbUQsS0FBS0MsUUFBeEQ7QUFDSDs7QUFFRCxTQUFLQyxnQkFBTDtBQUVBLFNBQUtDLFNBQUwsR0FBaUIsdUJBQWpCO0FBQ0gsR0FuRjJCO0FBcUY1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLQyxXQUFMO0FBQ0gsR0F2RjJCO0FBeUY1QkMsRUFBQUEsa0JBQWtCLEVBQUUsWUFBVztBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBS0QsV0FBTDtBQUNBLFNBQUtFLHNCQUFMO0FBQ0gsR0FqRzJCO0FBbUc1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsSUFBakI7O0FBRUEsUUFBSSxLQUFLVixLQUFMLENBQVdWLGNBQWYsRUFBK0I7QUFDM0IsV0FBS1UsS0FBTCxDQUFXVixjQUFYLENBQTBCcUIsY0FBMUIsQ0FBeUMsb0JBQXpDLEVBQStELEtBQUtULFFBQXBFO0FBQ0g7QUFDSixHQTdHMkI7QUErRzVCakIsRUFBQUEsUUFBUSxFQUFFLFVBQVMyQixFQUFULEVBQWE7QUFDbkJ4QyxJQUFBQSxRQUFRLENBQUMsVUFBRCxFQUFhLEtBQUt5QyxjQUFMLEdBQXNCQyxTQUFuQyxDQUFSOztBQUNBLFNBQUtDLGNBQUwsQ0FBb0JDLE9BQXBCOztBQUNBLFNBQUtDLGdCQUFMOztBQUNBLFNBQUtULHNCQUFMO0FBQ0EsU0FBS1IsS0FBTCxDQUFXZixRQUFYLENBQW9CMkIsRUFBcEI7QUFDQSxTQUFLTSxjQUFMO0FBQ0gsR0F0SDJCO0FBd0g1QmhCLEVBQUFBLFFBQVEsRUFBRSxZQUFXO0FBQ2pCLFNBQUtJLFdBQUwsR0FEaUIsQ0FFakI7O0FBQ0EsUUFBSSxLQUFLYSxxQkFBVCxFQUFnQztBQUM1QixXQUFLQyxnQkFBTDtBQUNIO0FBQ0osR0E5SDJCO0FBZ0k1QjtBQUNBO0FBQ0FkLEVBQUFBLFdBQVcsRUFBRSxZQUFXO0FBQ3BCLFFBQUksS0FBS0ksU0FBVCxFQUFvQjtBQUNoQjtBQUNIOztBQUNELFNBQUtXLHdCQUFMOztBQUNBLFNBQUtILGNBQUw7QUFDSCxHQXhJMkI7QUEwSTVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUksRUFBQUEsVUFBVSxFQUFFLFlBQVc7QUFDbkIsVUFBTUMsRUFBRSxHQUFHLEtBQUtWLGNBQUwsRUFBWCxDQURtQixDQUVuQjtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsV0FBT1csSUFBSSxDQUFDQyxHQUFMLENBQVNGLEVBQUUsQ0FBQ0csWUFBSCxJQUFtQkgsRUFBRSxDQUFDVCxTQUFILEdBQWVTLEVBQUUsQ0FBQ0ksWUFBckMsQ0FBVCxLQUFnRSxDQUF2RTtBQUVILEdBdkoyQjtBQXlKNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU3BDLFNBQVQsRUFBb0I7QUFDbEMsVUFBTStCLEVBQUUsR0FBRyxLQUFLVixjQUFMLEVBQVg7O0FBQ0EsVUFBTWdCLGFBQWEsR0FBRyxLQUFLQyxrQkFBTCxFQUF0Qjs7QUFDQSxVQUFNQyxVQUFVLEdBQUcsS0FBS0MsY0FBTCxFQUFuQjs7QUFDQSxVQUFNQyxhQUFhLEdBQUdKLGFBQWEsR0FBR0UsVUFBdEM7QUFDQSxVQUFNRyxrQkFBa0IsR0FBR1gsRUFBRSxDQUFDVCxTQUFILEdBQWVtQixhQUExQzs7QUFFQSxRQUFJekMsU0FBSixFQUFlO0FBQ1gsYUFBTzBDLGtCQUFrQixHQUFHWCxFQUFFLENBQUNJLFlBQXhCLEdBQXVDMUQsb0JBQTlDO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsYUFBTzRELGFBQWEsSUFBSUssa0JBQWtCLEdBQUcsSUFBRVgsRUFBRSxDQUFDSSxZQUE5QixDQUFiLEdBQTJEMUQsb0JBQWxFO0FBQ0g7QUFDSixHQXBNMkI7QUFzTTVCO0FBQ0FpRCxFQUFBQSxjQUFjLEVBQUUsZ0JBQWVpQixLQUFLLEdBQUMsQ0FBckIsRUFBd0I7QUFDcEMsUUFBSSxLQUFLekIsU0FBVCxFQUFvQjtBQUNoQjtBQUNIOztBQUVELFVBQU0wQixXQUFXLEdBQUdELEtBQUssS0FBSyxDQUE5Qjs7QUFDQSxVQUFNWixFQUFFLEdBQUcsS0FBS1YsY0FBTCxFQUFYLENBTm9DLENBUXBDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSXVCLFdBQUosRUFBaUI7QUFDYixVQUFJLEtBQUtDLFVBQVQsRUFBcUI7QUFDakJqRSxRQUFBQSxRQUFRLENBQUMscUZBQUQsQ0FBUjtBQUNBLGFBQUtrRSx3QkFBTCxHQUFnQyxJQUFoQztBQUNBO0FBQ0g7O0FBQ0RsRSxNQUFBQSxRQUFRLENBQUMscUJBQUQsQ0FBUjtBQUNBLFdBQUtpRSxVQUFMLEdBQWtCLElBQWxCO0FBQ0g7O0FBRUQsVUFBTUUsUUFBUSxHQUFHLEtBQUtuQyxTQUFMLENBQWVvQyxPQUFoQztBQUNBLFVBQU1DLFNBQVMsR0FBR0YsUUFBUSxJQUFJQSxRQUFRLENBQUNHLGlCQUF2QztBQUNBLFVBQU1DLFVBQVUsR0FBR0YsU0FBUyxJQUFJQSxTQUFTLENBQUNHLFNBQTFDO0FBQ0EsVUFBTUMsWUFBWSxHQUFHLEVBQXJCLENBakRvQyxDQW1EcEM7QUFDQTs7QUFDQSxRQUFJLENBQUNKLFNBQUQsSUFBZWxCLEVBQUUsQ0FBQ1QsU0FBSCxHQUFlNkIsVUFBaEIsR0FBOEJwQixFQUFFLENBQUNJLFlBQW5ELEVBQWlFO0FBQzdEO0FBQ0FrQixNQUFBQSxZQUFZLENBQUNDLElBQWIsQ0FBa0IsS0FBS0MsVUFBTCxDQUFnQlosS0FBaEIsRUFBdUIsSUFBdkIsQ0FBbEI7QUFDSCxLQXhEbUMsQ0F5RHBDO0FBQ0E7OztBQUNBLFFBQUtaLEVBQUUsQ0FBQ0csWUFBSCxHQUFrQkgsRUFBRSxDQUFDVCxTQUF0QixHQUFtQ1MsRUFBRSxDQUFDSSxZQUFILEdBQWtCLENBQXpELEVBQTREO0FBQ3hEO0FBQ0FrQixNQUFBQSxZQUFZLENBQUNDLElBQWIsQ0FBa0IsS0FBS0MsVUFBTCxDQUFnQlosS0FBaEIsRUFBdUIsS0FBdkIsQ0FBbEI7QUFDSDs7QUFFRCxRQUFJVSxZQUFZLENBQUNHLE1BQWpCLEVBQXlCO0FBQ3JCLFVBQUk7QUFDQSxjQUFNdkQsT0FBTyxDQUFDd0QsR0FBUixDQUFZSixZQUFaLENBQU47QUFDSCxPQUZELENBRUUsT0FBT0ssR0FBUCxFQUFZO0FBQ1Y3RSxRQUFBQSxPQUFPLENBQUM4RSxLQUFSLENBQWNELEdBQWQ7QUFDSDtBQUNKOztBQUNELFFBQUlkLFdBQUosRUFBaUI7QUFDYmhFLE1BQUFBLFFBQVEsQ0FBQyxzQkFBRCxDQUFSO0FBQ0EsV0FBS2lFLFVBQUwsR0FBa0IsS0FBbEI7QUFDSDs7QUFFRCxRQUFJLEtBQUtDLHdCQUFULEVBQW1DO0FBQy9CLFdBQUtBLHdCQUFMLEdBQWdDLEtBQWhDO0FBQ0EsV0FBS3BCLGNBQUw7QUFDSDtBQUNKLEdBdlIyQjtBQXlSNUI7QUFDQWtDLEVBQUFBLGlCQUFpQixFQUFFLFVBQVM1RCxTQUFULEVBQW9CO0FBQ25DLFFBQUk2RCxZQUFZLEdBQUcsS0FBS3pCLGdCQUFMLENBQXNCcEMsU0FBdEIsQ0FBbkI7O0FBQ0EsUUFBSTZELFlBQVksSUFBSSxDQUFwQixFQUF1QjtBQUNuQjtBQUNIOztBQUVELFVBQU1DLGdCQUFnQixHQUFHRCxZQUF6QjtBQUVBLFVBQU1FLEtBQUssR0FBRyxLQUFLbkQsU0FBTCxDQUFlb0MsT0FBZixDQUF1QmdCLFFBQXJDLENBUm1DLENBVW5DOztBQUNBLFFBQUlDLGlCQUFpQixHQUFHLElBQXhCLENBWG1DLENBYW5DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFJQyxJQUFKOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0osS0FBSyxDQUFDUCxNQUExQixFQUFrQ1csQ0FBQyxFQUFuQyxFQUF1QztBQUNuQ0QsTUFBQUEsSUFBSSxHQUFHSCxLQUFLLENBQUMvRCxTQUFTLEdBQUdtRSxDQUFILEdBQU9KLEtBQUssQ0FBQ1AsTUFBTixHQUFlLENBQWYsR0FBbUJXLENBQXBDLENBQVosQ0FEbUMsQ0FFbkM7O0FBQ0FOLE1BQUFBLFlBQVksSUFBSUssSUFBSSxDQUFDL0IsWUFBckIsQ0FIbUMsQ0FJbkM7O0FBQ0EsVUFBSStCLElBQUksQ0FBQy9CLFlBQUwsR0FBb0IwQixZQUF4QixFQUFzQztBQUNsQztBQUNILE9BUGtDLENBUW5DOzs7QUFDQSxVQUFJSyxJQUFJLENBQUNFLE9BQUwsQ0FBYUMsWUFBakIsRUFBK0I7QUFDM0JKLFFBQUFBLGlCQUFpQixHQUFHQyxJQUFJLENBQUNFLE9BQUwsQ0FBYUMsWUFBYixDQUEwQkMsS0FBMUIsQ0FBZ0MsR0FBaEMsRUFBcUMsQ0FBckMsQ0FBcEI7QUFDSDtBQUNKOztBQUVELFFBQUlMLGlCQUFKLEVBQXVCO0FBQ25CO0FBQ0E7QUFDQSxVQUFJLEtBQUtNLGdCQUFULEVBQTJCO0FBQ3ZCQyxRQUFBQSxZQUFZLENBQUMsS0FBS0QsZ0JBQU4sQ0FBWjtBQUNIOztBQUNELFdBQUtBLGdCQUFMLEdBQXdCRSxVQUFVLENBQUMsTUFBTTtBQUNyQyxhQUFLRixnQkFBTCxHQUF3QixJQUF4QjtBQUNBM0YsUUFBQUEsUUFBUSxDQUFDLGVBQUQsRUFBa0JvQixTQUFsQixFQUE2QjhELGdCQUE3QixDQUFSO0FBQ0EsYUFBS3RELEtBQUwsQ0FBV2hCLGVBQVgsQ0FBMkJRLFNBQTNCLEVBQXNDaUUsaUJBQXRDO0FBQ0gsT0FKaUMsRUFJL0J2RiwwQkFKK0IsQ0FBbEM7QUFLSDtBQUNKLEdBeFUyQjtBQTBVNUI7QUFDQTZFLEVBQUFBLFVBQVUsRUFBRSxVQUFTWixLQUFULEVBQWdCM0MsU0FBaEIsRUFBMkI7QUFDbkMsVUFBTTBFLEdBQUcsR0FBRzFFLFNBQVMsR0FBRyxHQUFILEdBQVMsR0FBOUI7O0FBQ0EsUUFBSSxLQUFLSyxvQkFBTCxDQUEwQnFFLEdBQTFCLENBQUosRUFBb0M7QUFDaEM5RixNQUFBQSxRQUFRLENBQUMsZUFBYThGLEdBQWIsR0FBaUIsMENBQWxCLENBQVI7QUFDQTtBQUNIOztBQUVEOUYsSUFBQUEsUUFBUSxDQUFDLGNBQVk4RixHQUFaLEdBQWdCLE9BQWpCLENBQVIsQ0FQbUMsQ0FTbkM7QUFDQTs7QUFDQSxTQUFLckUsb0JBQUwsQ0FBMEJxRSxHQUExQixJQUFpQyxJQUFqQyxDQVhtQyxDQWFuQztBQUNBO0FBQ0E7QUFDQTs7QUFDQSxXQUFPLElBQUl6RSxPQUFKLENBQVlDLE9BQU8sSUFBSXVFLFVBQVUsQ0FBQ3ZFLE9BQUQsRUFBVSxDQUFWLENBQWpDLEVBQStDeUUsSUFBL0MsQ0FBb0QsTUFBTTtBQUM3RCxhQUFPLEtBQUtuRSxLQUFMLENBQVdsQixhQUFYLENBQXlCVSxTQUF6QixDQUFQO0FBQ0gsS0FGTSxFQUVKNEUsT0FGSSxDQUVJLE1BQU07QUFDYixXQUFLdkUsb0JBQUwsQ0FBMEJxRSxHQUExQixJQUFpQyxLQUFqQztBQUNILEtBSk0sRUFJSkMsSUFKSSxDQUlFRSxjQUFELElBQW9CO0FBQ3hCLFVBQUksS0FBSzNELFNBQVQsRUFBb0I7QUFDaEI7QUFDSCxPQUh1QixDQUl4Qjs7O0FBQ0EsV0FBSzBDLGlCQUFMLENBQXVCLENBQUM1RCxTQUF4Qjs7QUFFQXBCLE1BQUFBLFFBQVEsQ0FBQyxLQUFHOEYsR0FBSCxHQUFPLGlDQUFQLEdBQXlDRyxjQUExQyxDQUFSOztBQUNBLFVBQUlBLGNBQUosRUFBb0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0EsZUFBTyxLQUFLbkQsY0FBTCxDQUFvQmlCLEtBQUssR0FBRyxDQUE1QixDQUFQO0FBQ0g7QUFDSixLQWxCTSxDQUFQO0FBbUJILEdBL1cyQjs7QUFpWDVCOzs7Ozs7Ozs7Ozs7OztBQWNBbUMsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIsV0FBTyxLQUFLQyxXQUFaO0FBQ0gsR0FqWTJCOztBQW1ZNUI7Ozs7Ozs7Ozs7OztBQVlBcEUsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixTQUFLb0UsV0FBTCxHQUFtQjtBQUNmQyxNQUFBQSxhQUFhLEVBQUUsS0FBS3hFLEtBQUwsQ0FBV25CO0FBRFgsS0FBbkI7QUFHQSxTQUFLNEYsYUFBTCxHQUFxQixDQUFyQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxDQUFkO0FBQ0EsU0FBSzNELGNBQUwsR0FBc0IsSUFBSTRELGNBQUosQ0FBVSxHQUFWLENBQXRCO0FBQ0EsU0FBS0MsdUJBQUwsR0FBK0IsS0FBL0I7QUFDSCxHQXZaMkI7O0FBeVo1Qjs7O0FBR0FDLEVBQUFBLFdBQVcsRUFBRSxZQUFXO0FBQ3BCLFNBQUtoRSxjQUFMLEdBQXNCQyxTQUF0QixHQUFrQyxDQUFsQzs7QUFDQSxTQUFLRyxnQkFBTDtBQUNILEdBL1oyQjs7QUFpYTVCOzs7QUFHQTZELEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBTXZELEVBQUUsR0FBRyxLQUFLVixjQUFMLEVBQVg7O0FBQ0FVLElBQUFBLEVBQUUsQ0FBQ1QsU0FBSCxHQUFlUyxFQUFFLENBQUNHLFlBQWxCOztBQUNBLFNBQUtULGdCQUFMO0FBQ0gsR0E1YTJCOztBQThhNUI7Ozs7O0FBS0E4RCxFQUFBQSxjQUFjLEVBQUUsVUFBU0MsSUFBVCxFQUFlO0FBQzNCLFVBQU1DLFVBQVUsR0FBRyxLQUFLcEUsY0FBTCxFQUFuQjs7QUFDQSxVQUFNcUUsS0FBSyxHQUFHRixJQUFJLEdBQUdDLFVBQVUsQ0FBQ3RELFlBQWxCLEdBQWlDLEdBQS9DO0FBQ0FzRCxJQUFBQSxVQUFVLENBQUNFLFFBQVgsQ0FBb0IsQ0FBcEIsRUFBdUJELEtBQXZCOztBQUNBLFNBQUtqRSxnQkFBTDtBQUNILEdBeGIyQjs7QUEwYjVCOzs7O0FBSUFtRSxFQUFBQSxlQUFlLEVBQUUsVUFBU3hFLEVBQVQsRUFBYTtBQUMxQixZQUFRQSxFQUFFLENBQUN5RSxHQUFYO0FBQ0ksV0FBS0MsY0FBSUMsT0FBVDtBQUNJLFlBQUksQ0FBQzNFLEVBQUUsQ0FBQzRFLE9BQUosSUFBZSxDQUFDNUUsRUFBRSxDQUFDNkUsUUFBbkIsSUFBK0IsQ0FBQzdFLEVBQUUsQ0FBQzhFLE1BQW5DLElBQTZDLENBQUM5RSxFQUFFLENBQUMrRSxPQUFyRCxFQUE4RDtBQUMxRCxlQUFLWixjQUFMLENBQW9CLENBQUMsQ0FBckI7QUFDSDs7QUFDRDs7QUFFSixXQUFLTyxjQUFJTSxTQUFUO0FBQ0ksWUFBSSxDQUFDaEYsRUFBRSxDQUFDNEUsT0FBSixJQUFlLENBQUM1RSxFQUFFLENBQUM2RSxRQUFuQixJQUErQixDQUFDN0UsRUFBRSxDQUFDOEUsTUFBbkMsSUFBNkMsQ0FBQzlFLEVBQUUsQ0FBQytFLE9BQXJELEVBQThEO0FBQzFELGVBQUtaLGNBQUwsQ0FBb0IsQ0FBcEI7QUFDSDs7QUFDRDs7QUFFSixXQUFLTyxjQUFJTyxJQUFUO0FBQ0ksWUFBSWpGLEVBQUUsQ0FBQzRFLE9BQUgsSUFBYyxDQUFDNUUsRUFBRSxDQUFDNkUsUUFBbEIsSUFBOEIsQ0FBQzdFLEVBQUUsQ0FBQzhFLE1BQWxDLElBQTRDLENBQUM5RSxFQUFFLENBQUMrRSxPQUFwRCxFQUE2RDtBQUN6RCxlQUFLZCxXQUFMO0FBQ0g7O0FBQ0Q7O0FBRUosV0FBS1MsY0FBSVEsR0FBVDtBQUNJLFlBQUlsRixFQUFFLENBQUM0RSxPQUFILElBQWMsQ0FBQzVFLEVBQUUsQ0FBQzZFLFFBQWxCLElBQThCLENBQUM3RSxFQUFFLENBQUM4RSxNQUFsQyxJQUE0QyxDQUFDOUUsRUFBRSxDQUFDK0UsT0FBcEQsRUFBNkQ7QUFDekQsZUFBS2IsY0FBTDtBQUNIOztBQUNEO0FBdkJSO0FBeUJILEdBeGQyQjs7QUEwZDVCOzs7Ozs7Ozs7OztBQVdBaUIsRUFBQUEsYUFBYSxFQUFFLFVBQVNwRyxXQUFULEVBQXNCcUcsV0FBdEIsRUFBbUNDLFVBQW5DLEVBQStDO0FBQzFERCxJQUFBQSxXQUFXLEdBQUdBLFdBQVcsSUFBSSxDQUE3QjtBQUNBQyxJQUFBQSxVQUFVLEdBQUdBLFVBQVUsSUFBSSxDQUEzQixDQUYwRCxDQUkxRDs7QUFDQSxTQUFLMUIsV0FBTCxHQUFtQjtBQUNmQyxNQUFBQSxhQUFhLEVBQUUsS0FEQTtBQUVmMEIsTUFBQUEsa0JBQWtCLEVBQUV2RztBQUZMLEtBQW5COztBQUlBLFVBQU13RyxXQUFXLEdBQUcsS0FBS0MsZUFBTCxFQUFwQjs7QUFDQSxVQUFNbkIsVUFBVSxHQUFHLEtBQUtwRSxjQUFMLEVBQW5COztBQUNBLFFBQUlzRixXQUFKLEVBQWlCO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EvSCxNQUFBQSxRQUFRLENBQUMsZ0NBQUQsRUFBbUM7QUFBQzZILFFBQUFBLFVBQUQ7QUFBYUQsUUFBQUEsV0FBYjtBQUEwQnBELFFBQUFBLFNBQVMsRUFBRXVELFdBQVcsQ0FBQ3ZEO0FBQWpELE9BQW5DLENBQVI7QUFDQXFDLE1BQUFBLFVBQVUsQ0FBQ25FLFNBQVgsR0FBd0JxRixXQUFXLENBQUN2RCxTQUFaLEdBQXlCcUMsVUFBVSxDQUFDdEQsWUFBWCxHQUEwQnNFLFVBQXBELEdBQW1FRCxXQUExRjs7QUFDQSxXQUFLL0UsZ0JBQUw7QUFDSDtBQUNKLEdBM2YyQjtBQTZmNUJBLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsUUFBSSxLQUFLakIsS0FBTCxDQUFXdEIsWUFBWCxJQUEyQixLQUFLNEMsVUFBTCxFQUEvQixFQUFrRDtBQUM5QyxXQUFLaUQsV0FBTCxHQUFtQjtBQUFFQyxRQUFBQSxhQUFhLEVBQUU7QUFBakIsT0FBbkI7QUFDQXBHLE1BQUFBLFFBQVEsQ0FBQywyQkFBRCxDQUFSO0FBQ0E7QUFDSDs7QUFFRCxVQUFNNkcsVUFBVSxHQUFHLEtBQUtwRSxjQUFMLEVBQW5COztBQUNBLFVBQU13RixjQUFjLEdBQUdwQixVQUFVLENBQUN2RCxZQUFYLElBQTJCdUQsVUFBVSxDQUFDbkUsU0FBWCxHQUF1Qm1FLFVBQVUsQ0FBQ3RELFlBQTdELENBQXZCO0FBRUEsVUFBTVksUUFBUSxHQUFHLEtBQUtuQyxTQUFMLENBQWVvQyxPQUFoQztBQUNBLFVBQU04RCxRQUFRLEdBQUcvRCxRQUFRLENBQUNpQixRQUExQjtBQUNBLFFBQUkrQyxJQUFJLEdBQUcsSUFBWCxDQVp5QixDQWN6QjtBQUNBOztBQUNBLFNBQUssSUFBSTVDLENBQUMsR0FBRzJDLFFBQVEsQ0FBQ3RELE1BQVQsR0FBZ0IsQ0FBN0IsRUFBZ0NXLENBQUMsSUFBSSxDQUFyQyxFQUF3QyxFQUFFQSxDQUExQyxFQUE2QztBQUN6QyxVQUFJLENBQUMyQyxRQUFRLENBQUMzQyxDQUFELENBQVIsQ0FBWUMsT0FBWixDQUFvQkMsWUFBekIsRUFBdUM7QUFDbkM7QUFDSDs7QUFDRDBDLE1BQUFBLElBQUksR0FBR0QsUUFBUSxDQUFDM0MsQ0FBRCxDQUFmLENBSnlDLENBS3pDO0FBQ0E7O0FBQ0EsVUFBSSxLQUFLNkMsY0FBTCxDQUFvQkQsSUFBcEIsSUFBNEJGLGNBQWhDLEVBQWdEO0FBQzVDO0FBQ0E7QUFDSDtBQUNKOztBQUVELFFBQUksQ0FBQ0UsSUFBTCxFQUFXO0FBQ1BuSSxNQUFBQSxRQUFRLENBQUMsZ0VBQUQsQ0FBUjtBQUNBO0FBQ0g7O0FBQ0QsVUFBTXVCLFdBQVcsR0FBRzRHLElBQUksQ0FBQzNDLE9BQUwsQ0FBYUMsWUFBYixDQUEwQkMsS0FBMUIsQ0FBZ0MsR0FBaEMsRUFBcUMsQ0FBckMsQ0FBcEI7QUFDQTFGLElBQUFBLFFBQVEsQ0FBQyx5Q0FBRCxFQUE0Q21JLElBQUksSUFBSUEsSUFBSSxDQUFDRSxTQUF6RCxFQUFvRTlHLFdBQXBFLENBQVI7O0FBQ0EsVUFBTStHLFlBQVksR0FBRyxLQUFLRixjQUFMLENBQW9CRCxJQUFwQixDQUFyQjs7QUFDQSxTQUFLaEMsV0FBTCxHQUFtQjtBQUNmQyxNQUFBQSxhQUFhLEVBQUUsS0FEQTtBQUVmMkIsTUFBQUEsV0FBVyxFQUFFSSxJQUZFO0FBR2ZMLE1BQUFBLGtCQUFrQixFQUFFdkcsV0FITDtBQUlmK0csTUFBQUEsWUFBWSxFQUFFQSxZQUpDO0FBS2ZWLE1BQUFBLFdBQVcsRUFBRVUsWUFBWSxHQUFHTCxjQUxiLENBSzZCOztBQUw3QixLQUFuQjtBQU9ILEdBeGlCMkI7QUEwaUI1QmhGLEVBQUFBLHdCQUF3QixFQUFFLGtCQUFpQjtBQUN2QyxVQUFNa0QsV0FBVyxHQUFHLEtBQUtBLFdBQXpCOztBQUVBLFFBQUlBLFdBQVcsQ0FBQ0MsYUFBaEIsRUFBK0I7QUFDM0IsWUFBTWpELEVBQUUsR0FBRyxLQUFLVixjQUFMLEVBQVg7O0FBQ0FVLE1BQUFBLEVBQUUsQ0FBQ1QsU0FBSCxHQUFlUyxFQUFFLENBQUNHLFlBQWxCO0FBQ0gsS0FIRCxNQUdPLElBQUk2QyxXQUFXLENBQUMyQixrQkFBaEIsRUFBb0M7QUFDdkMsWUFBTTNELFFBQVEsR0FBRyxLQUFLbkMsU0FBTCxDQUFlb0MsT0FBaEM7O0FBQ0EsWUFBTTJELFdBQVcsR0FBRyxLQUFLQyxlQUFMLEVBQXBCOztBQUNBLFVBQUlELFdBQUosRUFBaUI7QUFDYixjQUFNUSxlQUFlLEdBQUcsS0FBS0gsY0FBTCxDQUFvQkwsV0FBcEIsQ0FBeEI7O0FBQ0EsY0FBTVMsVUFBVSxHQUFHRCxlQUFlLEdBQUdwQyxXQUFXLENBQUNtQyxZQUFqRDtBQUNBLGFBQUtqQyxhQUFMLElBQXNCbUMsVUFBdEI7QUFDQXJDLFFBQUFBLFdBQVcsQ0FBQ21DLFlBQVosR0FBMkJDLGVBQTNCO0FBQ0FwRSxRQUFBQSxRQUFRLENBQUNuRCxLQUFULENBQWV5SCxNQUFmLGFBQTJCLEtBQUs3RSxjQUFMLEVBQTNCO0FBQ0E1RCxRQUFBQSxRQUFRLENBQUMsMERBQUQsRUFBNkR3SSxVQUE3RCxDQUFSO0FBQ0g7QUFDSjs7QUFDRCxRQUFJLENBQUMsS0FBS2hDLHVCQUFWLEVBQW1DO0FBQy9CLFdBQUtBLHVCQUFMLEdBQStCLElBQS9COztBQUNBLFVBQUk7QUFDQSxjQUFNLEtBQUtrQyxhQUFMLEVBQU47QUFDSCxPQUZELFNBRVU7QUFDTixhQUFLbEMsdUJBQUwsR0FBK0IsS0FBL0I7QUFDSDtBQUNKLEtBUEQsTUFPTztBQUNIeEcsTUFBQUEsUUFBUSxDQUFDLHlEQUFELENBQVI7QUFDSDtBQUNKLEdBdGtCMkI7O0FBdWtCNUI7QUFDQSxRQUFNMEksYUFBTixHQUFzQjtBQUNsQjtBQUNBLFFBQUksS0FBSy9GLGNBQUwsQ0FBb0JnRyxTQUFwQixFQUFKLEVBQXFDO0FBQ2pDM0ksTUFBQUEsUUFBUSxDQUFDLGdEQUFELENBQVI7QUFDQSxZQUFNLEtBQUsyQyxjQUFMLENBQW9CaUcsUUFBcEIsRUFBTjtBQUNILEtBSEQsTUFHTztBQUNINUksTUFBQUEsUUFBUSxDQUFDLG1FQUFELENBQVI7QUFDSCxLQVBpQixDQVNsQjs7O0FBQ0EsUUFBSSxLQUFLc0MsU0FBVCxFQUFvQjtBQUNoQjtBQUNIOztBQUVELFVBQU1hLEVBQUUsR0FBRyxLQUFLVixjQUFMLEVBQVg7O0FBQ0EsVUFBTTBCLFFBQVEsR0FBRyxLQUFLbkMsU0FBTCxDQUFlb0MsT0FBaEM7O0FBQ0EsVUFBTVgsYUFBYSxHQUFHLEtBQUtDLGtCQUFMLEVBQXRCOztBQUNBLFVBQU1tRixTQUFTLEdBQUcxRixFQUFFLENBQUNJLFlBQXJCO0FBQ0EsVUFBTWtGLE1BQU0sR0FBR3JGLElBQUksQ0FBQzBGLEdBQUwsQ0FBU0QsU0FBVCxFQUFvQnBGLGFBQXBCLENBQWY7QUFDQSxTQUFLNkMsTUFBTCxHQUFjbEQsSUFBSSxDQUFDMkYsSUFBTCxDQUFVTixNQUFNLEdBQUcxSSxTQUFuQixDQUFkO0FBQ0EsU0FBS3NHLGFBQUwsR0FBcUIsQ0FBckI7O0FBQ0EsVUFBTTJDLFNBQVMsR0FBRyxLQUFLcEYsY0FBTCxFQUFsQjs7QUFFQSxVQUFNdUMsV0FBVyxHQUFHLEtBQUtBLFdBQXpCOztBQUNBLFFBQUlBLFdBQVcsQ0FBQ0MsYUFBaEIsRUFBK0I7QUFDM0JqQyxNQUFBQSxRQUFRLENBQUNuRCxLQUFULENBQWV5SCxNQUFmLGFBQTJCTyxTQUEzQjtBQUNBN0YsTUFBQUEsRUFBRSxDQUFDVCxTQUFILEdBQWVTLEVBQUUsQ0FBQ0csWUFBbEI7QUFDQXRELE1BQUFBLFFBQVEsQ0FBQyxpQkFBRCxFQUFvQmdKLFNBQXBCLENBQVI7QUFDSCxLQUpELE1BSU8sSUFBSTdDLFdBQVcsQ0FBQzJCLGtCQUFoQixFQUFvQztBQUN2QyxZQUFNQyxXQUFXLEdBQUcsS0FBS0MsZUFBTCxFQUFwQixDQUR1QyxDQUV2QztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBSUQsV0FBSixFQUFpQjtBQUNiLGNBQU1rQixNQUFNLEdBQUdsQixXQUFXLENBQUN2RCxTQUEzQjtBQUNBTCxRQUFBQSxRQUFRLENBQUNuRCxLQUFULENBQWV5SCxNQUFmLGFBQTJCTyxTQUEzQjtBQUNBLGNBQU1FLE1BQU0sR0FBR25CLFdBQVcsQ0FBQ3ZELFNBQTNCO0FBQ0EsY0FBTTJFLE9BQU8sR0FBR0QsTUFBTSxHQUFHRCxNQUF6QixDQUphLENBS2I7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E5RixRQUFBQSxFQUFFLENBQUM0RCxRQUFILENBQVksQ0FBWixFQUFlb0MsT0FBZjtBQUNBbkosUUFBQUEsUUFBUSxDQUFDLGlCQUFELEVBQW9CO0FBQUNnSixVQUFBQSxTQUFEO0FBQVlHLFVBQUFBO0FBQVosU0FBcEIsQ0FBUjtBQUNIO0FBQ0o7QUFDSixHQXZuQjJCOztBQXluQjVCbkIsRUFBQUEsZUFBZSxHQUFHO0FBQ2QsVUFBTTdCLFdBQVcsR0FBRyxLQUFLQSxXQUF6QjtBQUNBLFVBQU00QixXQUFXLEdBQUc1QixXQUFXLENBQUM0QixXQUFoQzs7QUFFQSxRQUFJLENBQUNBLFdBQUQsSUFBZ0IsQ0FBQ0EsV0FBVyxDQUFDcUIsYUFBakMsRUFBZ0Q7QUFDNUMsVUFBSWpCLElBQUo7QUFDQSxZQUFNRCxRQUFRLEdBQUcsS0FBS2xHLFNBQUwsQ0FBZW9DLE9BQWYsQ0FBdUJnQixRQUF4QztBQUNBLFlBQU03RCxXQUFXLEdBQUc0RSxXQUFXLENBQUMyQixrQkFBaEM7O0FBRUEsV0FBSyxJQUFJdkMsQ0FBQyxHQUFHMkMsUUFBUSxDQUFDdEQsTUFBVCxHQUFnQixDQUE3QixFQUFnQ1csQ0FBQyxJQUFJLENBQXJDLEVBQXdDLEVBQUVBLENBQTFDLEVBQTZDO0FBQ3pDLGNBQU04RCxDQUFDLEdBQUduQixRQUFRLENBQUMzQyxDQUFELENBQWxCLENBRHlDLENBRXpDO0FBQ0E7O0FBQ0EsWUFBSThELENBQUMsQ0FBQzdELE9BQUYsQ0FBVUMsWUFBVixJQUNBNEQsQ0FBQyxDQUFDN0QsT0FBRixDQUFVQyxZQUFWLENBQXVCQyxLQUF2QixDQUE2QixHQUE3QixFQUFrQzRELE9BQWxDLENBQTBDL0gsV0FBMUMsTUFBMkQsQ0FBQyxDQURoRSxFQUNtRTtBQUMvRDRHLFVBQUFBLElBQUksR0FBR2tCLENBQVA7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsVUFBSWxCLElBQUosRUFBVTtBQUNObkksUUFBQUEsUUFBUSxDQUFDLHdDQUF3Q21HLFdBQVcsQ0FBQzJCLGtCQUFyRCxDQUFSO0FBQ0g7O0FBQ0QzQixNQUFBQSxXQUFXLENBQUM0QixXQUFaLEdBQTBCSSxJQUExQjtBQUNIOztBQUVELFFBQUksQ0FBQ2hDLFdBQVcsQ0FBQzRCLFdBQWpCLEVBQThCO0FBQzFCL0gsTUFBQUEsUUFBUSxDQUFDLHFCQUFtQm1HLFdBQVcsQ0FBQzJCLGtCQUEvQixHQUFrRCxHQUFuRCxDQUFSO0FBQ0E7QUFDSDs7QUFFRCxXQUFPM0IsV0FBVyxDQUFDNEIsV0FBbkI7QUFDSCxHQXhwQjJCOztBQTBwQjVCbkUsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsV0FBTyxLQUFLeUMsYUFBTCxHQUFzQixLQUFLQyxNQUFMLEdBQWN2RyxTQUEzQztBQUNILEdBNXBCMkI7O0FBOHBCNUIyRCxFQUFBQSxrQkFBa0IsR0FBRztBQUNqQixVQUFNUyxRQUFRLEdBQUcsS0FBS25DLFNBQUwsQ0FBZW9DLE9BQWhDO0FBQ0EsVUFBTW1GLFFBQVEsR0FBR3BGLFFBQVEsQ0FBQ3FGLGdCQUExQjtBQUNBLFVBQU1DLGNBQWMsR0FBR0YsUUFBUSxHQUFHQSxRQUFRLENBQUMvRSxTQUFULEdBQXFCK0UsUUFBUSxDQUFDaEcsWUFBakMsR0FBZ0QsQ0FBL0U7QUFDQSxVQUFNbUcsWUFBWSxHQUFHdkYsUUFBUSxDQUFDRyxpQkFBVCxHQUE2QkgsUUFBUSxDQUFDRyxpQkFBVCxDQUEyQkUsU0FBeEQsR0FBb0UsQ0FBekYsQ0FKaUIsQ0FLakI7O0FBQ0EsV0FBT2lGLGNBQWMsR0FBR0MsWUFBakIsR0FBaUMsS0FBSyxDQUE3QztBQUNILEdBcnFCMkI7O0FBdXFCNUJ0QixFQUFBQSxjQUFjLENBQUNELElBQUQsRUFBTztBQUNqQjtBQUNBLFdBQU8sS0FBS25HLFNBQUwsQ0FBZW9DLE9BQWYsQ0FBdUJiLFlBQXZCLEdBQXNDNEUsSUFBSSxDQUFDM0QsU0FBbEQ7QUFDSCxHQTFxQjJCOztBQTRxQjVCOzs7QUFHQS9CLEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCLFFBQUksS0FBS0gsU0FBVCxFQUFvQjtBQUNoQjtBQUNBO0FBQ0EsWUFBTSxJQUFJcUgsS0FBSixDQUFVLGtEQUFWLENBQU47QUFDSDs7QUFFRCxRQUFJLENBQUMsS0FBS0MsVUFBVixFQUFzQjtBQUNsQjtBQUNBO0FBQ0EsWUFBTSxJQUFJRCxLQUFKLENBQVUsMEVBQVYsQ0FBTjtBQUNIOztBQUVELFdBQU8sS0FBS0MsVUFBWjtBQUNILEdBN3JCMkI7QUErckI1QkMsRUFBQUEsY0FBYyxFQUFFLFVBQVNDLFNBQVQsRUFBb0I7QUFDaEMsU0FBS0YsVUFBTCxHQUFrQkUsU0FBbEI7QUFDSCxHQWpzQjJCOztBQW1zQjVCOzs7OztBQUtBOUcsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixVQUFNK0csV0FBVyxHQUFHLEtBQUsvSCxTQUFMLENBQWVvQyxPQUFuQztBQUNBLFVBQU1lLEtBQUssR0FBRzRFLFdBQVcsSUFBSUEsV0FBVyxDQUFDM0UsUUFBekM7O0FBQ0EsUUFBSSxDQUFDMkUsV0FBTCxFQUFrQjtBQUNkO0FBQ0g7O0FBQ0QsUUFBSUMsWUFBSjs7QUFDQSxTQUFLLElBQUl6RSxDQUFDLEdBQUdKLEtBQUssQ0FBQ1AsTUFBTixHQUFlLENBQTVCLEVBQStCVyxDQUFDLElBQUksQ0FBcEMsRUFBdUNBLENBQUMsRUFBeEMsRUFBNEM7QUFDeEMsWUFBTTRDLElBQUksR0FBR2hELEtBQUssQ0FBQ0ksQ0FBRCxDQUFsQjs7QUFDQSxVQUFJNEMsSUFBSSxDQUFDM0MsT0FBTCxDQUFhQyxZQUFqQixFQUErQjtBQUMzQnVFLFFBQUFBLFlBQVksR0FBRzdCLElBQWY7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsUUFBSSxDQUFDNkIsWUFBTCxFQUFtQjtBQUNmO0FBQ0g7O0FBQ0QsU0FBS0MscUJBQUw7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR0gsV0FBVyxDQUFDeEcsWUFBWixJQUE0QnlHLFlBQVksQ0FBQ3hGLFNBQWIsR0FBeUJ3RixZQUFZLENBQUN6RyxZQUFsRSxDQUF6QjtBQUNBLFNBQUtSLHFCQUFMLEdBQTZCO0FBQ3pCbUgsTUFBQUEsZ0JBQWdCLEVBQUVBLGdCQURPO0FBRXpCQyxNQUFBQSxVQUFVLEVBQUVIO0FBRmEsS0FBN0I7QUFJQWhLLElBQUFBLFFBQVEsQ0FBQywrQkFBRCxFQUFrQ2tLLGdCQUFsQyxFQUFvRCxnQkFBcEQsQ0FBUjtBQUNILEdBaHVCMkI7O0FBa3VCNUI7QUFDQUQsRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixVQUFNRixXQUFXLEdBQUcsS0FBSy9ILFNBQUwsQ0FBZW9DLE9BQW5DO0FBQ0EsVUFBTWdHLGNBQWMsR0FBR0wsV0FBVyxJQUFJQSxXQUFXLENBQUNYLGFBQWxEO0FBQ0EsUUFBSWdCLGNBQUosRUFBb0JBLGNBQWMsQ0FBQ3BKLEtBQWYsQ0FBcUJxSixhQUFyQixHQUFxQyxJQUFyQztBQUNwQixTQUFLdEgscUJBQUwsR0FBNkIsSUFBN0I7QUFDQS9DLElBQUFBLFFBQVEsQ0FBQywyQkFBRCxDQUFSO0FBQ0gsR0F6dUIyQjs7QUEydUI1Qjs7Ozs7Ozs7QUFRQW9DLEVBQUFBLHNCQUFzQixFQUFFLFlBQVc7QUFDL0IsUUFBSSxLQUFLVyxxQkFBVCxFQUFnQztBQUM1QixZQUFNSSxFQUFFLEdBQUcsS0FBS1YsY0FBTCxFQUFYOztBQUNBLFlBQU0wRCxXQUFXLEdBQUcsS0FBS0EsV0FBekI7QUFDQSxZQUFNNEQsV0FBVyxHQUFHLEtBQUsvSCxTQUFMLENBQWVvQyxPQUFuQztBQUNBLFlBQU07QUFBQytGLFFBQUFBLFVBQUQ7QUFBYUQsUUFBQUE7QUFBYixVQUFpQyxLQUFLbkgscUJBQTVDLENBSjRCLENBSzVCOztBQUNBLFlBQU1xSCxjQUFjLEdBQUdMLFdBQVcsQ0FBQ1gsYUFBbkMsQ0FONEIsQ0FPNUI7O0FBQ0EsVUFBSWtCLFdBQVcsR0FBRyxDQUFDSCxVQUFVLENBQUNmLGFBQTlCLENBUjRCLENBUzVCOztBQUNBLFVBQUksQ0FBQ2tCLFdBQUQsSUFBZ0IsQ0FBQ25FLFdBQVcsQ0FBQ0MsYUFBakMsRUFBZ0Q7QUFDNUMsY0FBTW1FLGtCQUFrQixHQUFHcEgsRUFBRSxDQUFDRyxZQUFILElBQW1CSCxFQUFFLENBQUNULFNBQUgsR0FBZVMsRUFBRSxDQUFDSSxZQUFyQyxDQUEzQjtBQUNBK0csUUFBQUEsV0FBVyxHQUFHQyxrQkFBa0IsSUFBSSxHQUFwQztBQUNILE9BYjJCLENBYzVCOzs7QUFDQSxVQUFJLENBQUNELFdBQUwsRUFBa0I7QUFDZCxjQUFNRSxhQUFhLEdBQUdULFdBQVcsQ0FBQ3hHLFlBQVosSUFBNEI0RyxVQUFVLENBQUMzRixTQUFYLEdBQXVCMkYsVUFBVSxDQUFDNUcsWUFBOUQsQ0FBdEI7QUFDQSxjQUFNa0gsVUFBVSxHQUFHUCxnQkFBZ0IsR0FBR00sYUFBdEM7O0FBQ0EsWUFBSUMsVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2hCTCxVQUFBQSxjQUFjLENBQUNwSixLQUFmLENBQXFCcUosYUFBckIsYUFBd0NJLFVBQXhDO0FBQ0F6SyxVQUFBQSxRQUFRLENBQUMsMkJBQUQsRUFBOEJ5SyxVQUE5QixFQUEwQyxnQkFBMUMsQ0FBUjtBQUNILFNBSEQsTUFHTyxJQUFJQSxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDdkJILFVBQUFBLFdBQVcsR0FBRyxJQUFkO0FBQ0g7QUFDSjs7QUFDRCxVQUFJQSxXQUFKLEVBQWlCO0FBQ2IsYUFBS0wscUJBQUw7QUFDSDtBQUNKO0FBQ0osR0FqeEIyQjtBQW14QjVCUyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQSxXQUFRLDZCQUFDLDBCQUFEO0FBQW1CLE1BQUEsVUFBVSxFQUFFLEtBQUtiLGNBQXBDO0FBQ0EsTUFBQSxRQUFRLEVBQUUsS0FBS2hKLFFBRGY7QUFFQSxNQUFBLFNBQVMsMkJBQW9CLEtBQUtlLEtBQUwsQ0FBV2QsU0FBL0IsQ0FGVDtBQUVxRCxNQUFBLEtBQUssRUFBRSxLQUFLYyxLQUFMLENBQVdaO0FBRnZFLE9BR0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSSxNQUFBLEdBQUcsRUFBRSxLQUFLZ0IsU0FBZDtBQUF5QixNQUFBLFNBQVMsRUFBQyx5QkFBbkM7QUFBNkQsbUJBQVUsUUFBdkU7QUFBZ0YsTUFBQSxJQUFJLEVBQUM7QUFBckYsT0FDTSxLQUFLSixLQUFMLENBQVd3RCxRQURqQixDQURKLENBSEosQ0FBUjtBQVVIO0FBcHlCMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgS2V5IH0gZnJvbSAnLi4vLi4vS2V5Ym9hcmQnO1xyXG5pbXBvcnQgVGltZXIgZnJvbSAnLi4vLi4vdXRpbHMvVGltZXInO1xyXG5pbXBvcnQgQXV0b0hpZGVTY3JvbGxiYXIgZnJvbSBcIi4vQXV0b0hpZGVTY3JvbGxiYXJcIjtcclxuXHJcbmNvbnN0IERFQlVHX1NDUk9MTCA9IGZhbHNlO1xyXG5cclxuLy8gVGhlIGFtb3VudCBvZiBleHRyYSBzY3JvbGwgZGlzdGFuY2UgdG8gYWxsb3cgcHJpb3IgdG8gdW5maWxsaW5nLlxyXG4vLyBTZWUgX2dldEV4Y2Vzc0hlaWdodC5cclxuY29uc3QgVU5QQUdJTkFUSU9OX1BBRERJTkcgPSA2MDAwO1xyXG4vLyBUaGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyB0byBkZWJvdW5jZSBjYWxscyB0byBvblVuZmlsbFJlcXVlc3QsIHRvIHByZXZlbnRcclxuLy8gbWFueSBzY3JvbGwgZXZlbnRzIGNhdXNpbmcgbWFueSB1bmZpbGxpbmcgcmVxdWVzdHMuXHJcbmNvbnN0IFVORklMTF9SRVFVRVNUX0RFQk9VTkNFX01TID0gMjAwO1xyXG4vLyBfdXBkYXRlSGVpZ2h0IG1ha2VzIHRoZSBoZWlnaHQgYSBjZWlsZWQgbXVsdGlwbGUgb2YgdGhpcyBzbyB3ZVxyXG4vLyBkb24ndCBoYXZlIHRvIHVwZGF0ZSB0aGUgaGVpZ2h0IHRvbyBvZnRlbi4gSXQgYWxzbyBhbGxvd3MgdGhlIHVzZXJcclxuLy8gdG8gc2Nyb2xsIHBhc3QgdGhlIHBhZ2luYXRpb24gc3Bpbm5lciBhIGJpdCBzbyB0aGV5IGRvbid0IGZlZWwgYmxvY2tlZCBzb1xyXG4vLyBtdWNoIHdoaWxlIHRoZSBjb250ZW50IGxvYWRzLlxyXG5jb25zdCBQQUdFX1NJWkUgPSA0MDA7XHJcblxyXG5sZXQgZGVidWdsb2c7XHJcbmlmIChERUJVR19TQ1JPTEwpIHtcclxuICAgIC8vIHVzaW5nIGJpbmQgbWVhbnMgdGhhdCB3ZSBnZXQgdG8ga2VlcCB1c2VmdWwgbGluZSBudW1iZXJzIGluIHRoZSBjb25zb2xlXHJcbiAgICBkZWJ1Z2xvZyA9IGNvbnNvbGUubG9nLmJpbmQoY29uc29sZSwgXCJTY3JvbGxQYW5lbCBkZWJ1Z2xvZzpcIik7XHJcbn0gZWxzZSB7XHJcbiAgICBkZWJ1Z2xvZyA9IGZ1bmN0aW9uKCkge307XHJcbn1cclxuXHJcbi8qIFRoaXMgY29tcG9uZW50IGltcGxlbWVudHMgYW4gaW50ZWxsaWdlbnQgc2Nyb2xsaW5nIGxpc3QuXHJcbiAqXHJcbiAqIEl0IHdyYXBzIGEgbGlzdCBvZiA8bGk+IGNoaWxkcmVuOyB3aGVuIGl0ZW1zIGFyZSBhZGRlZCB0byB0aGUgc3RhcnQgb3IgZW5kXHJcbiAqIG9mIHRoZSBsaXN0LCB0aGUgc2Nyb2xsIHBvc2l0aW9uIGlzIHVwZGF0ZWQgc28gdGhhdCB0aGUgdXNlciBzdGlsbCBzZWVzIHRoZVxyXG4gKiBzYW1lIHBvc2l0aW9uIGluIHRoZSBsaXN0LlxyXG4gKlxyXG4gKiBJdCBhbHNvIHByb3ZpZGVzIGEgaG9vayB3aGljaCBhbGxvd3MgcGFyZW50cyB0byBwcm92aWRlIG1vcmUgbGlzdCBlbGVtZW50c1xyXG4gKiB3aGVuIHdlIGdldCBjbG9zZSB0byB0aGUgc3RhcnQgb3IgZW5kIG9mIHRoZSBsaXN0LlxyXG4gKlxyXG4gKiBFYWNoIGNoaWxkIGVsZW1lbnQgc2hvdWxkIGhhdmUgYSAnZGF0YS1zY3JvbGwtdG9rZW5zJy4gVGhpcyBzdHJpbmcgb2ZcclxuICogY29tbWEtc2VwYXJhdGVkIHRva2VucyBtYXkgY29udGFpbiBhIHNpbmdsZSB0b2tlbiBvciBtYW55LCB3aGVyZSBtYW55IGluZGljYXRlc1xyXG4gKiB0aGF0IHRoZSBlbGVtZW50IGNvbnRhaW5zIGVsZW1lbnRzIHRoYXQgaGF2ZSBzY3JvbGwgdG9rZW5zIHRoZW1zZWx2ZXMuIFRoZSBmaXJzdFxyXG4gKiB0b2tlbiBpbiAnZGF0YS1zY3JvbGwtdG9rZW5zJyBpcyB1c2VkIHRvIHNlcmlhbGlzZSB0aGUgc2Nyb2xsIHN0YXRlLCBhbmQgcmV0dXJuZWRcclxuICogYXMgdGhlICd0cmFja2VkU2Nyb2xsVG9rZW4nIGF0dHJpYnV0ZSBieSBnZXRTY3JvbGxTdGF0ZSgpLlxyXG4gKlxyXG4gKiBJTVBPUlRBTlQ6IElORElWSURVQUwgVE9LRU5TIFdJVEhJTiAnZGF0YS1zY3JvbGwtdG9rZW5zJyBNVVNUIE5PVCBDT05UQUlOIENPTU1BUy5cclxuICpcclxuICogU29tZSBub3RlcyBhYm91dCB0aGUgaW1wbGVtZW50YXRpb246XHJcbiAqXHJcbiAqIFRoZSBzYXZlZCAnc2Nyb2xsU3RhdGUnIGNhbiBleGlzdCBpbiBvbmUgb2YgdHdvIHN0YXRlczpcclxuICpcclxuICogICAtIHN0dWNrQXRCb3R0b206ICh0aGUgZGVmYXVsdCwgYW5kIHJlc3RvcmVkIGJ5IHJlc2V0U2Nyb2xsU3RhdGUpOiB0aGVcclxuICogICAgIHZpZXdwb3J0IGlzIHNjcm9sbGVkIGRvd24gYXMgZmFyIGFzIGl0IGNhbiBiZS4gV2hlbiB0aGUgY2hpbGRyZW4gYXJlXHJcbiAqICAgICB1cGRhdGVkLCB0aGUgc2Nyb2xsIHBvc2l0aW9uIHdpbGwgYmUgdXBkYXRlZCB0byBlbnN1cmUgaXQgaXMgc3RpbGwgYXRcclxuICogICAgIHRoZSBib3R0b20uXHJcbiAqXHJcbiAqICAgLSBmaXhlZCwgaW4gd2hpY2ggdGhlIHZpZXdwb3J0IGlzIGNvbmNlcHR1YWxseSB0aWVkIGF0IGEgc3BlY2lmaWMgc2Nyb2xsXHJcbiAqICAgICBvZmZzZXQuICBXZSBkb24ndCBzYXZlIHRoZSBhYnNvbHV0ZSBzY3JvbGwgb2Zmc2V0LCBiZWNhdXNlIHRoYXQgd291bGQgYmVcclxuICogICAgIGFmZmVjdGVkIGJ5IHdpbmRvdyB3aWR0aCwgem9vbSBsZXZlbCwgYW1vdW50IG9mIHNjcm9sbGJhY2ssIGV0Yy4gSW5zdGVhZFxyXG4gKiAgICAgd2Ugc2F2ZSBhbiBpZGVudGlmaWVyIGZvciB0aGUgbGFzdCBmdWxseS12aXNpYmxlIG1lc3NhZ2UsIGFuZCB0aGUgbnVtYmVyXHJcbiAqICAgICBvZiBwaXhlbHMgdGhlIHdpbmRvdyB3YXMgc2Nyb2xsZWQgYmVsb3cgaXQgLSB3aGljaCBpcyBob3BlZnVsbHkgbmVhclxyXG4gKiAgICAgZW5vdWdoLlxyXG4gKlxyXG4gKiBUaGUgJ3N0aWNreUJvdHRvbScgcHJvcGVydHkgY29udHJvbHMgdGhlIGJlaGF2aW91ciB3aGVuIHdlIHJlYWNoIHRoZSBib3R0b21cclxuICogb2YgdGhlIHdpbmRvdyAoZWl0aGVyIHRocm91Z2ggYSB1c2VyLWluaXRpYXRlZCBzY3JvbGwsIG9yIGJ5IGNhbGxpbmdcclxuICogc2Nyb2xsVG9Cb3R0b20pLiBJZiBzdGlja3lCb3R0b20gaXMgZW5hYmxlZCwgdGhlIHNjcm9sbFN0YXRlIHdpbGwgZW50ZXJcclxuICogJ3N0dWNrQXRCb3R0b20nIHN0YXRlIC0gZW5zdXJpbmcgdGhhdCBuZXcgYWRkaXRpb25zIGNhdXNlIHRoZSB3aW5kb3cgdG9cclxuICogc2Nyb2xsIGRvd24gZnVydGhlci4gSWYgc3RpY2t5Qm90dG9tIGlzIGRpc2FibGVkLCB3ZSBqdXN0IHNhdmUgdGhlIHNjcm9sbFxyXG4gKiBvZmZzZXQgYXMgbm9ybWFsLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdTY3JvbGxQYW5lbCcsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLyogc3RpY2t5Qm90dG9tOiBpZiBzZXQgdG8gdHJ1ZSwgdGhlbiBvbmNlIHRoZSB1c2VyIGhpdHMgdGhlIGJvdHRvbSBvZlxyXG4gICAgICAgICAqIHRoZSBsaXN0LCBhbnkgbmV3IGNoaWxkcmVuIGFkZGVkIHRvIHRoZSBsaXN0IHdpbGwgY2F1c2UgdGhlIGxpc3QgdG9cclxuICAgICAgICAgKiBzY3JvbGwgZG93biB0byBzaG93IHRoZSBuZXcgZWxlbWVudCwgcmF0aGVyIHRoYW4gcHJlc2VydmluZyB0aGVcclxuICAgICAgICAgKiBleGlzdGluZyB2aWV3LlxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHN0aWNreUJvdHRvbTogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIHN0YXJ0QXRCb3R0b206IGlmIHNldCB0byB0cnVlLCB0aGUgdmlldyBpcyBhc3N1bWVkIHRvIHN0YXJ0XHJcbiAgICAgICAgICogc2Nyb2xsZWQgdG8gdGhlIGJvdHRvbS5cclxuICAgICAgICAgKiBYWFg6IEl0J3MgbGlrbGV5IHRoaXMgaXMgdW5lY2Vzc2FyeSBhbmQgY2FuIGJlIGRlcml2ZWQgZnJvbVxyXG4gICAgICAgICAqIHN0aWNreUJvdHRvbSwgYnV0IEknbSBhZGRpbmcgYW4gZXh0cmEgcGFyYW1ldGVyIHRvIGVuc3VyZVxyXG4gICAgICAgICAqIGJlaGF2aW91ciBzdGF5cyB0aGUgc2FtZSBmb3Igb3RoZXIgdXNlcyBvZiBTY3JvbGxQYW5lbC5cclxuICAgICAgICAgKiBJZiBzbywgbGV0J3MgcmVtb3ZlIHRoaXMgcGFyYW1ldGVyIGRvd24gdGhlIGxpbmUuXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgc3RhcnRBdEJvdHRvbTogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIG9uRmlsbFJlcXVlc3QoYmFja3dhcmRzKTogYSBjYWxsYmFjayB3aGljaCBpcyBjYWxsZWQgb24gc2Nyb2xsIHdoZW5cclxuICAgICAgICAgKiB0aGUgdXNlciBuZWFycyB0aGUgc3RhcnQgKGJhY2t3YXJkcyA9IHRydWUpIG9yIGVuZCAoYmFja3dhcmRzID1cclxuICAgICAgICAgKiBmYWxzZSkgb2YgdGhlIGxpc3QuXHJcbiAgICAgICAgICpcclxuICAgICAgICAgKiBUaGlzIHNob3VsZCByZXR1cm4gYSBwcm9taXNlOyBubyBtb3JlIGNhbGxzIHdpbGwgYmUgbWFkZSB1bnRpbCB0aGVcclxuICAgICAgICAgKiBwcm9taXNlIGNvbXBsZXRlcy5cclxuICAgICAgICAgKlxyXG4gICAgICAgICAqIFRoZSBwcm9taXNlIHNob3VsZCByZXNvbHZlIHRvIHRydWUgaWYgdGhlcmUgaXMgbW9yZSBkYXRhIHRvIGJlXHJcbiAgICAgICAgICogcmV0cmlldmVkIGluIHRoaXMgZGlyZWN0aW9uIChpbiB3aGljaCBjYXNlIG9uRmlsbFJlcXVlc3QgbWF5IGJlXHJcbiAgICAgICAgICogY2FsbGVkIGFnYWluIGltbWVkaWF0ZWx5KSwgb3IgZmFsc2UgaWYgdGhlcmUgaXMgbm8gbW9yZSBkYXRhIGluIHRoaXNcclxuICAgICAgICAgKiBkaXJlY3RvbiAoYXQgdGhpcyB0aW1lKSAtIHdoaWNoIHdpbGwgc3RvcCB0aGUgcGFnaW5hdGlvbiBjeWNsZSB1bnRpbFxyXG4gICAgICAgICAqIHRoZSB1c2VyIHNjcm9sbHMgYWdhaW4uXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgb25GaWxsUmVxdWVzdDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8qIG9uVW5maWxsUmVxdWVzdChiYWNrd2FyZHMpOiBhIGNhbGxiYWNrIHdoaWNoIGlzIGNhbGxlZCBvbiBzY3JvbGwgd2hlblxyXG4gICAgICAgICAqIHRoZXJlIGFyZSBjaGlsZHJlbiBlbGVtZW50cyB0aGF0IGFyZSBmYXIgb3V0IG9mIHZpZXcgYW5kIGNvdWxkIGJlIHJlbW92ZWRcclxuICAgICAgICAgKiB3aXRob3V0IGNhdXNpbmcgcGFnaW5hdGlvbiB0byBvY2N1ci5cclxuICAgICAgICAgKlxyXG4gICAgICAgICAqIFRoaXMgZnVuY3Rpb24gc2hvdWxkIGFjY2VwdCBhIGJvb2xlYW4sIHdoaWNoIGlzIHRydWUgdG8gaW5kaWNhdGUgdGhlIGJhY2svdG9wXHJcbiAgICAgICAgICogb2YgdGhlIHBhbmVsIGFuZCBmYWxzZSBvdGhlcndpc2UsIGFuZCBhIHNjcm9sbCB0b2tlbiwgd2hpY2ggcmVmZXJzIHRvIHRoZVxyXG4gICAgICAgICAqIGZpcnN0IGVsZW1lbnQgdG8gcmVtb3ZlIGlmIHJlbW92aW5nIGZyb20gdGhlIGZyb250L2JvdHRvbSwgYW5kIGxhc3QgZWxlbWVudFxyXG4gICAgICAgICAqIHRvIHJlbW92ZSBpZiByZW1vdmluZyBmcm9tIHRoZSBiYWNrL3RvcC5cclxuICAgICAgICAgKi9cclxuICAgICAgICBvblVuZmlsbFJlcXVlc3Q6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvKiBvblNjcm9sbDogYSBjYWxsYmFjayB3aGljaCBpcyBjYWxsZWQgd2hlbmV2ZXIgYW55IHNjcm9sbCBoYXBwZW5zLlxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIG9uU2Nyb2xsOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLyogY2xhc3NOYW1lOiBjbGFzc25hbWVzIHRvIGFkZCB0byB0aGUgdG9wLWxldmVsIGRpdlxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLyogc3R5bGU6IHN0eWxlcyB0byBhZGQgdG8gdGhlIHRvcC1sZXZlbCBkaXZcclxuICAgICAgICAgKi9cclxuICAgICAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgICAgICAvKiByZXNpemVOb3RpZmllcjogUmVzaXplTm90aWZpZXIgdG8ga25vdyB3aGVuIG1pZGRsZSBjb2x1bW4gaGFzIGNoYW5nZWQgc2l6ZVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHJlc2l6ZU5vdGlmaWVyOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHN0aWNreUJvdHRvbTogdHJ1ZSxcclxuICAgICAgICAgICAgc3RhcnRBdEJvdHRvbTogdHJ1ZSxcclxuICAgICAgICAgICAgb25GaWxsUmVxdWVzdDogZnVuY3Rpb24oYmFja3dhcmRzKSB7IHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpOyB9LFxyXG4gICAgICAgICAgICBvblVuZmlsbFJlcXVlc3Q6IGZ1bmN0aW9uKGJhY2t3YXJkcywgc2Nyb2xsVG9rZW4pIHt9LFxyXG4gICAgICAgICAgICBvblNjcm9sbDogZnVuY3Rpb24oKSB7fSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3BlbmRpbmdGaWxsUmVxdWVzdHMgPSB7YjogbnVsbCwgZjogbnVsbH07XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlc2l6ZU5vdGlmaWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIub24oXCJtaWRkbGVQYW5lbFJlc2l6ZWRcIiwgdGhpcy5vblJlc2l6ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJlc2V0U2Nyb2xsU3RhdGUoKTtcclxuXHJcbiAgICAgICAgdGhpcy5faXRlbWxpc3QgPSBjcmVhdGVSZWYoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuY2hlY2tTY3JvbGwoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBhZnRlciBhZGRpbmcgZXZlbnQgdGlsZXMsIHdlIG1heSBuZWVkIHRvIHR3ZWFrIHRoZSBzY3JvbGwgKGVpdGhlciB0b1xyXG4gICAgICAgIC8vIGtlZXAgYXQgdGhlIGJvdHRvbSBvZiB0aGUgdGltZWxpbmUsIG9yIHRvIG1haW50YWluIHRoZSB2aWV3IGFmdGVyXHJcbiAgICAgICAgLy8gYWRkaW5nIGV2ZW50cyB0byB0aGUgdG9wKS5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIFRoaXMgd2lsbCBhbHNvIHJlLWNoZWNrIHRoZSBmaWxsIHN0YXRlLCBpbiBjYXNlIHRoZSBwYWdpbmF0ZSB3YXMgaW5hZGVxdWF0ZVxyXG4gICAgICAgIHRoaXMuY2hlY2tTY3JvbGwoKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVByZXZlbnRTaHJpbmtpbmcoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHNldCBhIGJvb2xlYW4gdG8gc2F5IHdlJ3ZlIGJlZW4gdW5tb3VudGVkLCB3aGljaCBhbnkgcGVuZGluZ1xyXG4gICAgICAgIC8vIHByb21pc2VzIGNhbiB1c2UgdG8gdGhyb3cgYXdheSB0aGVpciByZXN1bHRzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gKFdlIGNvdWxkIHVzZSBpc01vdW50ZWQoKSwgYnV0IGZhY2Vib29rIGhhdmUgZGVwcmVjYXRlZCB0aGF0LilcclxuICAgICAgICB0aGlzLnVubW91bnRlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlc2l6ZU5vdGlmaWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIucmVtb3ZlTGlzdGVuZXIoXCJtaWRkbGVQYW5lbFJlc2l6ZWRcIiwgdGhpcy5vblJlc2l6ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblNjcm9sbDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBkZWJ1Z2xvZyhcIm9uU2Nyb2xsXCIsIHRoaXMuX2dldFNjcm9sbE5vZGUoKS5zY3JvbGxUb3ApO1xyXG4gICAgICAgIHRoaXMuX3Njcm9sbFRpbWVvdXQucmVzdGFydCgpO1xyXG4gICAgICAgIHRoaXMuX3NhdmVTY3JvbGxTdGF0ZSgpO1xyXG4gICAgICAgIHRoaXMudXBkYXRlUHJldmVudFNocmlua2luZygpO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25TY3JvbGwoZXYpO1xyXG4gICAgICAgIHRoaXMuY2hlY2tGaWxsU3RhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25SZXNpemU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuY2hlY2tTY3JvbGwoKTtcclxuICAgICAgICAvLyB1cGRhdGUgcHJldmVudFNocmlua2luZ1N0YXRlIGlmIHByZXNlbnRcclxuICAgICAgICBpZiAodGhpcy5wcmV2ZW50U2hyaW5raW5nU3RhdGUpIHtcclxuICAgICAgICAgICAgdGhpcy5wcmV2ZW50U2hyaW5raW5nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBhZnRlciBhbiB1cGRhdGUgdG8gdGhlIGNvbnRlbnRzIG9mIHRoZSBwYW5lbCwgY2hlY2sgdGhhdCB0aGUgc2Nyb2xsIGlzXHJcbiAgICAvLyB3aGVyZSBpdCBvdWdodCB0byBiZSwgYW5kIHNldCBvZmYgcGFnaW5hdGlvbiByZXF1ZXN0cyBpZiBuZWNlc3NhcnkuXHJcbiAgICBjaGVja1Njcm9sbDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fcmVzdG9yZVNhdmVkU2Nyb2xsU3RhdGUoKTtcclxuICAgICAgICB0aGlzLmNoZWNrRmlsbFN0YXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHJldHVybiB0cnVlIGlmIHRoZSBjb250ZW50IGlzIGZ1bGx5IHNjcm9sbGVkIGRvd24gcmlnaHQgbm93OyBlbHNlIGZhbHNlLlxyXG4gICAgLy9cclxuICAgIC8vIG5vdGUgdGhhdCB0aGlzIGlzIGluZGVwZW5kZW50IG9mIHRoZSAnc3R1Y2tBdEJvdHRvbScgc3RhdGUgLSBpdCBpcyBzaW1wbHlcclxuICAgIC8vIGFib3V0IHdoZXRoZXIgdGhlIGNvbnRlbnQgaXMgc2Nyb2xsZWQgZG93biByaWdodCBub3csIGlycmVzcGVjdGl2ZSBvZlxyXG4gICAgLy8gd2hldGhlciBpdCB3aWxsIHN0YXkgdGhhdCB3YXkgd2hlbiB0aGUgY2hpbGRyZW4gdXBkYXRlLlxyXG4gICAgaXNBdEJvdHRvbTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgc24gPSB0aGlzLl9nZXRTY3JvbGxOb2RlKCk7XHJcbiAgICAgICAgLy8gZnJhY3Rpb25hbCB2YWx1ZXMgKGJvdGggdG9vIGJpZyBhbmQgdG9vIHNtYWxsKVxyXG4gICAgICAgIC8vIGZvciBzY3JvbGxUb3AgaGFwcGVuIG9uIGNlcnRhaW4gYnJvd3NlcnMvcGxhdGZvcm1zXHJcbiAgICAgICAgLy8gd2hlbiBzY3JvbGxlZCBhbGwgdGhlIHdheSBkb3duLiBFLmcuIENocm9tZSA3MiBvbiBkZWJpYW4uXHJcbiAgICAgICAgLy8gc28gY2hlY2sgZGlmZmVyZW5jZSA8PSAxO1xyXG4gICAgICAgIHJldHVybiBNYXRoLmFicyhzbi5zY3JvbGxIZWlnaHQgLSAoc24uc2Nyb2xsVG9wICsgc24uY2xpZW50SGVpZ2h0KSkgPD0gMTtcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHJldHVybnMgdGhlIHZlcnRpY2FsIGhlaWdodCBpbiB0aGUgZ2l2ZW4gZGlyZWN0aW9uIHRoYXQgY2FuIGJlIHJlbW92ZWQgZnJvbVxyXG4gICAgLy8gdGhlIGNvbnRlbnQgYm94ICh3aGljaCBoYXMgYSBoZWlnaHQgb2Ygc2Nyb2xsSGVpZ2h0LCBzZWUgY2hlY2tGaWxsU3RhdGUpIHdpdGhvdXRcclxuICAgIC8vIHBhZ2luYXRpb24gb2NjdXJpbmcuXHJcbiAgICAvL1xyXG4gICAgLy8gcGFkZGluZyogPSBVTlBBR0lOQVRJT05fUEFERElOR1xyXG4gICAgLy9cclxuICAgIC8vICMjIyBSZWdpb24gZGV0ZXJtaW5lZCBhcyBleGNlc3MuXHJcbiAgICAvL1xyXG4gICAgLy8gICAuLS0tLS0tLS0tLiAgICAgICAgICAgICAgICAgICAgICAgIC0gICAgICAgICAgICAgIC1cclxuICAgIC8vICAgfCMjIyMjIyMjI3wgICAgICAgICAgICAgICAgICAgICAgICB8ICAgICAgICAgICAgICB8XHJcbiAgICAvLyAgIHwjIyMjIyMjIyN8ICAgLSAgICAgICAgICAgICAgICAgICAgfCAgc2Nyb2xsVG9wICAgfFxyXG4gICAgLy8gICB8ICAgICAgICAgfCAgIHwgcGFkZGluZyogICAgICAgICAgIHwgICAgICAgICAgICAgIHxcclxuICAgIC8vICAgfCAgICAgICAgIHwgICB8ICAgICAgICAgICAgICAgICAgICB8ICAgICAgICAgICAgICB8XHJcbiAgICAvLyAuLSstLS0tLS0tLS0rLS4gLSAgLSAgICAgICAgICAgICAgICAgfCAgICAgICAgICAgICAgfFxyXG4gICAgLy8gOiB8ICAgICAgICAgfCA6ICAgIHwgICAgICAgICAgICAgICAgIHwgICAgICAgICAgICAgIHxcclxuICAgIC8vIDogfCAgICAgICAgIHwgOiAgICB8ICBjbGllbnRIZWlnaHQgICB8ICAgICAgICAgICAgICB8XHJcbiAgICAvLyA6IHwgICAgICAgICB8IDogICAgfCAgICAgICAgICAgICAgICAgfCAgICAgICAgICAgICAgfFxyXG4gICAgLy8gLi0rLS0tLS0tLS0tKy0uICAgIC0gICAgICAgICAgICAgICAgIC0gICAgICAgICAgICAgIHxcclxuICAgIC8vIHwgfCAgICAgICAgIHwgfCAgICB8ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAvLyB8IHwgICAgICAgICB8IHwgICAgfCAgY2xpZW50SGVpZ2h0ICAgICAgICAgICAgICAgICAgfCBzY3JvbGxIZWlnaHRcclxuICAgIC8vIHwgfCAgICAgICAgIHwgfCAgICB8ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAvLyBgLSstLS0tLS0tLS0rLScgICAgLSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfFxyXG4gICAgLy8gOiB8ICAgICAgICAgfCA6ICAgIHwgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcclxuICAgIC8vIDogfCAgICAgICAgIHwgOiAgICB8ICBjbGllbnRIZWlnaHQgICAgICAgICAgICAgICAgICB8XHJcbiAgICAvLyA6IHwgICAgICAgICB8IDogICAgfCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfFxyXG4gICAgLy8gYC0rLS0tLS0tLS0tKy0nIC0gIC0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcclxuICAgIC8vICAgfCAgICAgICAgIHwgICB8IHBhZGRpbmcqICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAvLyAgIHwgICAgICAgICB8ICAgfCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfFxyXG4gICAgLy8gICB8IyMjIyMjIyMjfCAgIC0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcclxuICAgIC8vICAgfCMjIyMjIyMjI3wgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAvLyAgIGAtLS0tLS0tLS0nICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLVxyXG4gICAgX2dldEV4Y2Vzc0hlaWdodDogZnVuY3Rpb24oYmFja3dhcmRzKSB7XHJcbiAgICAgICAgY29uc3Qgc24gPSB0aGlzLl9nZXRTY3JvbGxOb2RlKCk7XHJcbiAgICAgICAgY29uc3QgY29udGVudEhlaWdodCA9IHRoaXMuX2dldE1lc3NhZ2VzSGVpZ2h0KCk7XHJcbiAgICAgICAgY29uc3QgbGlzdEhlaWdodCA9IHRoaXMuX2dldExpc3RIZWlnaHQoKTtcclxuICAgICAgICBjb25zdCBjbGlwcGVkSGVpZ2h0ID0gY29udGVudEhlaWdodCAtIGxpc3RIZWlnaHQ7XHJcbiAgICAgICAgY29uc3QgdW5jbGlwcGVkU2Nyb2xsVG9wID0gc24uc2Nyb2xsVG9wICsgY2xpcHBlZEhlaWdodDtcclxuXHJcbiAgICAgICAgaWYgKGJhY2t3YXJkcykge1xyXG4gICAgICAgICAgICByZXR1cm4gdW5jbGlwcGVkU2Nyb2xsVG9wIC0gc24uY2xpZW50SGVpZ2h0IC0gVU5QQUdJTkFUSU9OX1BBRERJTkc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNvbnRlbnRIZWlnaHQgLSAodW5jbGlwcGVkU2Nyb2xsVG9wICsgMipzbi5jbGllbnRIZWlnaHQpIC0gVU5QQUdJTkFUSU9OX1BBRERJTkc7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBjaGVjayB0aGUgc2Nyb2xsIHN0YXRlIGFuZCBzZW5kIG91dCBiYWNrZmlsbCByZXF1ZXN0cyBpZiBuZWNlc3NhcnkuXHJcbiAgICBjaGVja0ZpbGxTdGF0ZTogYXN5bmMgZnVuY3Rpb24oZGVwdGg9MCkge1xyXG4gICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBpc0ZpcnN0Q2FsbCA9IGRlcHRoID09PSAwO1xyXG4gICAgICAgIGNvbnN0IHNuID0gdGhpcy5fZ2V0U2Nyb2xsTm9kZSgpO1xyXG5cclxuICAgICAgICAvLyBpZiB0aGVyZSBpcyBsZXNzIHRoYW4gYSBzY3JlZW5mdWwgb2YgbWVzc2FnZXMgYWJvdmUgb3IgYmVsb3cgdGhlXHJcbiAgICAgICAgLy8gdmlld3BvcnQsIHRyeSB0byBnZXQgc29tZSBtb3JlIG1lc3NhZ2VzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gc2Nyb2xsVG9wIGlzIHRoZSBudW1iZXIgb2YgcGl4ZWxzIGJldHdlZW4gdGhlIHRvcCBvZiB0aGUgY29udGVudCBhbmRcclxuICAgICAgICAvLyAgICAgdGhlIHRvcCBvZiB0aGUgdmlld3BvcnQuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBzY3JvbGxIZWlnaHQgaXMgdGhlIHRvdGFsIGhlaWdodCBvZiB0aGUgY29udGVudC5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIGNsaWVudEhlaWdodCBpcyB0aGUgaGVpZ2h0IG9mIHRoZSB2aWV3cG9ydCAoZXhjbHVkaW5nIGJvcmRlcnMsXHJcbiAgICAgICAgLy8gbWFyZ2lucywgYW5kIHNjcm9sbGJhcnMpLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyAgIC4tLS0tLS0tLS0uICAgICAgICAgIC0gICAgICAgICAgICAgICAgIC1cclxuICAgICAgICAvLyAgIHwgICAgICAgICB8ICAgICAgICAgIHwgIHNjcm9sbFRvcCAgICAgIHxcclxuICAgICAgICAvLyAuLSstLS0tLS0tLS0rLS4gICAgLSAgIC0gICAgICAgICAgICAgICAgIHxcclxuICAgICAgICAvLyB8IHwgICAgICAgICB8IHwgICAgfCAgICAgICAgICAgICAgICAgICAgIHxcclxuICAgICAgICAvLyB8IHwgICAgICAgICB8IHwgICAgfCAgY2xpZW50SGVpZ2h0ICAgICAgIHwgc2Nyb2xsSGVpZ2h0XHJcbiAgICAgICAgLy8gfCB8ICAgICAgICAgfCB8ICAgIHwgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAgICAgLy8gYC0rLS0tLS0tLS0tKy0nICAgIC0gICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAgICAgLy8gICB8ICAgICAgICAgfCAgICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAgICAgLy8gICB8ICAgICAgICAgfCAgICAgICAgICAgICAgICAgICAgICAgICAgICB8XHJcbiAgICAgICAgLy8gICBgLS0tLS0tLS0tJyAgICAgICAgICAgICAgICAgICAgICAgICAgICAtXHJcbiAgICAgICAgLy9cclxuXHJcbiAgICAgICAgLy8gYXMgZmlsbGluZyBpcyBhc3luYyBhbmQgcmVjdXJzaXZlLFxyXG4gICAgICAgIC8vIGRvbid0IGFsbG93IG1vcmUgdGhhbiAxIGNoYWluIG9mIGNhbGxzIGNvbmN1cnJlbnRseVxyXG4gICAgICAgIC8vIGRvIG1ha2UgYSBub3RlIHdoZW4gYSBuZXcgcmVxdWVzdCBjb21lcyBpbiB3aGlsZSBhbHJlYWR5IHJ1bm5pbmcgb25lLFxyXG4gICAgICAgIC8vIHNvIHdlIGNhbiB0cmlnZ2VyIGEgbmV3IGNoYWluIG9mIGNhbGxzIG9uY2UgZG9uZS5cclxuICAgICAgICBpZiAoaXNGaXJzdENhbGwpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2lzRmlsbGluZykge1xyXG4gICAgICAgICAgICAgICAgZGVidWdsb2coXCJfaXNGaWxsaW5nOiBub3QgZW50ZXJpbmcgd2hpbGUgcmVxdWVzdCBpcyBvbmdvaW5nLCBtYXJraW5nIGZvciBhIHN1YnNlcXVlbnQgcmVxdWVzdFwiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2ZpbGxSZXF1ZXN0V2hpbGVSdW5uaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcIl9pc0ZpbGxpbmc6IHNldHRpbmdcIik7XHJcbiAgICAgICAgICAgIHRoaXMuX2lzRmlsbGluZyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBpdGVtbGlzdCA9IHRoaXMuX2l0ZW1saXN0LmN1cnJlbnQ7XHJcbiAgICAgICAgY29uc3QgZmlyc3RUaWxlID0gaXRlbWxpc3QgJiYgaXRlbWxpc3QuZmlyc3RFbGVtZW50Q2hpbGQ7XHJcbiAgICAgICAgY29uc3QgY29udGVudFRvcCA9IGZpcnN0VGlsZSAmJiBmaXJzdFRpbGUub2Zmc2V0VG9wO1xyXG4gICAgICAgIGNvbnN0IGZpbGxQcm9taXNlcyA9IFtdO1xyXG5cclxuICAgICAgICAvLyBpZiBzY3JvbGxUb3AgZ2V0cyB0byAxIHNjcmVlbiBmcm9tIHRoZSB0b3Agb2YgdGhlIGZpcnN0IHRpbGUsXHJcbiAgICAgICAgLy8gdHJ5IGJhY2t3YXJkIGZpbGxpbmdcclxuICAgICAgICBpZiAoIWZpcnN0VGlsZSB8fCAoc24uc2Nyb2xsVG9wIC0gY29udGVudFRvcCkgPCBzbi5jbGllbnRIZWlnaHQpIHtcclxuICAgICAgICAgICAgLy8gbmVlZCB0byBiYWNrLWZpbGxcclxuICAgICAgICAgICAgZmlsbFByb21pc2VzLnB1c2godGhpcy5fbWF5YmVGaWxsKGRlcHRoLCB0cnVlKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGlmIHNjcm9sbFRvcCBnZXRzIHRvIDIgc2NyZWVucyBmcm9tIHRoZSBlbmQgKHNvIDEgc2NyZWVuIGJlbG93IHZpZXdwb3J0KSxcclxuICAgICAgICAvLyB0cnkgZm9yd2FyZCBmaWxsaW5nXHJcbiAgICAgICAgaWYgKChzbi5zY3JvbGxIZWlnaHQgLSBzbi5zY3JvbGxUb3ApIDwgc24uY2xpZW50SGVpZ2h0ICogMikge1xyXG4gICAgICAgICAgICAvLyBuZWVkIHRvIGZvcndhcmQtZmlsbFxyXG4gICAgICAgICAgICBmaWxsUHJvbWlzZXMucHVzaCh0aGlzLl9tYXliZUZpbGwoZGVwdGgsIGZhbHNlKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoZmlsbFByb21pc2VzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgUHJvbWlzZS5hbGwoZmlsbFByb21pc2VzKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRmlyc3RDYWxsKSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwiX2lzRmlsbGluZzogY2xlYXJpbmdcIik7XHJcbiAgICAgICAgICAgIHRoaXMuX2lzRmlsbGluZyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX2ZpbGxSZXF1ZXN0V2hpbGVSdW5uaW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2ZpbGxSZXF1ZXN0V2hpbGVSdW5uaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY2hlY2tGaWxsU3RhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGNoZWNrIGlmIHVuZmlsbGluZyBpcyBwb3NzaWJsZSBhbmQgc2VuZCBhbiB1bmZpbGwgcmVxdWVzdCBpZiBuZWNlc3NhcnlcclxuICAgIF9jaGVja1VuZmlsbFN0YXRlOiBmdW5jdGlvbihiYWNrd2FyZHMpIHtcclxuICAgICAgICBsZXQgZXhjZXNzSGVpZ2h0ID0gdGhpcy5fZ2V0RXhjZXNzSGVpZ2h0KGJhY2t3YXJkcyk7XHJcbiAgICAgICAgaWYgKGV4Y2Vzc0hlaWdodCA8PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG9yaWdFeGNlc3NIZWlnaHQgPSBleGNlc3NIZWlnaHQ7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpbGVzID0gdGhpcy5faXRlbWxpc3QuY3VycmVudC5jaGlsZHJlbjtcclxuXHJcbiAgICAgICAgLy8gVGhlIHNjcm9sbCB0b2tlbiBvZiB0aGUgZmlyc3QvbGFzdCB0aWxlIHRvIGJlIHVucGFnaW5hdGVkXHJcbiAgICAgICAgbGV0IG1hcmtlclNjcm9sbFRva2VuID0gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gU3VidHJhY3QgaGVpZ2h0cyBvZiB0aWxlcyB0byBzaW11bGF0ZSB0aGUgdGlsZXMgYmVpbmcgdW5wYWdpbmF0ZWQgdW50aWwgdGhlXHJcbiAgICAgICAgLy8gZXhjZXNzIGhlaWdodCBpcyBsZXNzIHRoYW4gdGhlIGhlaWdodCBvZiB0aGUgbmV4dCB0aWxlIHRvIHN1YnRyYWN0LiBUaGlzXHJcbiAgICAgICAgLy8gcHJldmVudHMgZXhjZXNzSGVpZ2h0IGJlY29taW5nIG5lZ2F0aXZlLCB3aGljaCBjb3VsZCBsZWFkIHRvIGZ1dHVyZVxyXG4gICAgICAgIC8vIHBhZ2luYXRpb24uXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBJZiBiYWNrd2FyZHMgaXMgdHJ1ZSwgd2UgdW5wYWdpbmF0ZSAocmVtb3ZlKSB0aWxlcyBmcm9tIHRoZSBiYWNrICh0b3ApLlxyXG4gICAgICAgIGxldCB0aWxlO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGlsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGlsZSA9IHRpbGVzW2JhY2t3YXJkcyA/IGkgOiB0aWxlcy5sZW5ndGggLSAxIC0gaV07XHJcbiAgICAgICAgICAgIC8vIFN1YnRyYWN0IGhlaWdodCBvZiB0aWxlIGFzIGlmIGl0IHdlcmUgdW5wYWdpbmF0ZWRcclxuICAgICAgICAgICAgZXhjZXNzSGVpZ2h0IC09IHRpbGUuY2xpZW50SGVpZ2h0O1xyXG4gICAgICAgICAgICAvL0lmIHJlbW92aW5nIHRoZSB0aWxlIHdvdWxkIGxlYWQgdG8gZnV0dXJlIHBhZ2luYXRpb24sIGJyZWFrIGJlZm9yZSBzZXR0aW5nIHNjcm9sbCB0b2tlblxyXG4gICAgICAgICAgICBpZiAodGlsZS5jbGllbnRIZWlnaHQgPiBleGNlc3NIZWlnaHQpIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIFRoZSB0aWxlIG1heSBub3QgaGF2ZSBhIHNjcm9sbCB0b2tlbiwgc28gZ3VhcmQgaXRcclxuICAgICAgICAgICAgaWYgKHRpbGUuZGF0YXNldC5zY3JvbGxUb2tlbnMpIHtcclxuICAgICAgICAgICAgICAgIG1hcmtlclNjcm9sbFRva2VuID0gdGlsZS5kYXRhc2V0LnNjcm9sbFRva2Vucy5zcGxpdCgnLCcpWzBdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAobWFya2VyU2Nyb2xsVG9rZW4pIHtcclxuICAgICAgICAgICAgLy8gVXNlIGEgZGVib3VuY2VyIHRvIHByZXZlbnQgbXVsdGlwbGUgdW5maWxsIGNhbGxzIGluIHF1aWNrIHN1Y2Nlc3Npb25cclxuICAgICAgICAgICAgLy8gVGhpcyBpcyB0byBtYWtlIHRoZSB1bmZpbGxpbmcgcHJvY2VzcyBsZXNzIGFnZ3Jlc3NpdmVcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VuZmlsbERlYm91bmNlcikge1xyXG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX3VuZmlsbERlYm91bmNlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fdW5maWxsRGVib3VuY2VyID0gc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91bmZpbGxEZWJvdW5jZXIgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgZGVidWdsb2coXCJ1bmZpbGxpbmcgbm93XCIsIGJhY2t3YXJkcywgb3JpZ0V4Y2Vzc0hlaWdodCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uVW5maWxsUmVxdWVzdChiYWNrd2FyZHMsIG1hcmtlclNjcm9sbFRva2VuKTtcclxuICAgICAgICAgICAgfSwgVU5GSUxMX1JFUVVFU1RfREVCT1VOQ0VfTVMpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8gY2hlY2sgaWYgdGhlcmUgaXMgYWxyZWFkeSBhIHBlbmRpbmcgZmlsbCByZXF1ZXN0LiBJZiBub3QsIHNldCBvbmUgb2ZmLlxyXG4gICAgX21heWJlRmlsbDogZnVuY3Rpb24oZGVwdGgsIGJhY2t3YXJkcykge1xyXG4gICAgICAgIGNvbnN0IGRpciA9IGJhY2t3YXJkcyA/ICdiJyA6ICdmJztcclxuICAgICAgICBpZiAodGhpcy5fcGVuZGluZ0ZpbGxSZXF1ZXN0c1tkaXJdKSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwiQWxyZWFkeSBhIFwiK2RpcitcIiBmaWxsIGluIHByb2dyZXNzIC0gbm90IHN0YXJ0aW5nIGFub3RoZXJcIik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGRlYnVnbG9nKFwic3RhcnRpbmcgXCIrZGlyK1wiIGZpbGxcIik7XHJcblxyXG4gICAgICAgIC8vIG9uRmlsbFJlcXVlc3QgY2FuIGVuZCB1cCBjYWxsaW5nIHVzIHJlY3Vyc2l2ZWx5ICh2aWEgb25TY3JvbGxcclxuICAgICAgICAvLyBldmVudHMpIHNvIG1ha2Ugc3VyZSB3ZSBzZXQgdGhpcyBiZWZvcmUgZmlyaW5nIG9mZiB0aGUgY2FsbC5cclxuICAgICAgICB0aGlzLl9wZW5kaW5nRmlsbFJlcXVlc3RzW2Rpcl0gPSB0cnVlO1xyXG5cclxuICAgICAgICAvLyB3YWl0IDFtcyBiZWZvcmUgcGFnaW5hdGluZywgYmVjYXVzZSBvdGhlcndpc2VcclxuICAgICAgICAvLyB0aGlzIHdpbGwgYmxvY2sgdGhlIHNjcm9sbCBldmVudCBoYW5kbGVyIGZvciArNzAwbXNcclxuICAgICAgICAvLyBpZiBtZXNzYWdlcyBhcmUgYWxyZWFkeSBjYWNoZWQgaW4gbWVtb3J5LFxyXG4gICAgICAgIC8vIFRoaXMgd291bGQgY2F1c2UganVtcGluZyB0byBoYXBwZW4gb24gQ2hyb21lL21hY09TLlxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHNldFRpbWVvdXQocmVzb2x2ZSwgMSkpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5vbkZpbGxSZXF1ZXN0KGJhY2t3YXJkcyk7XHJcbiAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BlbmRpbmdGaWxsUmVxdWVzdHNbZGlyXSA9IGZhbHNlO1xyXG4gICAgICAgIH0pLnRoZW4oKGhhc01vcmVSZXN1bHRzKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIFVucGFnaW5hdGUgb25jZSBmaWxsaW5nIGlzIGNvbXBsZXRlXHJcbiAgICAgICAgICAgIHRoaXMuX2NoZWNrVW5maWxsU3RhdGUoIWJhY2t3YXJkcyk7XHJcblxyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcIlwiK2RpcitcIiBmaWxsIGNvbXBsZXRlOyBoYXNNb3JlUmVzdWx0czpcIitoYXNNb3JlUmVzdWx0cyk7XHJcbiAgICAgICAgICAgIGlmIChoYXNNb3JlUmVzdWx0cykge1xyXG4gICAgICAgICAgICAgICAgLy8gZnVydGhlciBwYWdpbmF0aW9uIHJlcXVlc3RzIGhhdmUgYmVlbiBkaXNhYmxlZCB1bnRpbCBub3csIHNvXHJcbiAgICAgICAgICAgICAgICAvLyBpdCdzIHRpbWUgdG8gY2hlY2sgdGhlIGZpbGwgc3RhdGUgYWdhaW4gaW4gY2FzZSB0aGUgcGFnaW5hdGlvblxyXG4gICAgICAgICAgICAgICAgLy8gd2FzIGluc3VmZmljaWVudC5cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNoZWNrRmlsbFN0YXRlKGRlcHRoICsgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLyogZ2V0IHRoZSBjdXJyZW50IHNjcm9sbCBzdGF0ZS4gVGhpcyByZXR1cm5zIGFuIG9iamVjdCB3aXRoIHRoZSBmb2xsb3dpbmdcclxuICAgICAqIHByb3BlcnRpZXM6XHJcbiAgICAgKlxyXG4gICAgICogYm9vbGVhbiBzdHVja0F0Qm90dG9tOiB0cnVlIGlmIHdlIGFyZSB0cmFja2luZyB0aGUgYm90dG9tIG9mIHRoZVxyXG4gICAgICogICBzY3JvbGwuIGZhbHNlIGlmIHdlIGFyZSB0cmFja2luZyBhIHBhcnRpY3VsYXIgY2hpbGQuXHJcbiAgICAgKlxyXG4gICAgICogc3RyaW5nIHRyYWNrZWRTY3JvbGxUb2tlbjogdW5kZWZpbmVkIGlmIHN0dWNrQXRCb3R0b20gaXMgdHJ1ZTsgaWYgaXQgaXNcclxuICAgICAqICAgZmFsc2UsIHRoZSBmaXJzdCB0b2tlbiBpbiBkYXRhLXNjcm9sbC10b2tlbnMgb2YgdGhlIGNoaWxkIHdoaWNoIHdlIGFyZVxyXG4gICAgICogICB0cmFja2luZy5cclxuICAgICAqXHJcbiAgICAgKiBudW1iZXIgYm90dG9tT2Zmc2V0OiB1bmRlZmluZWQgaWYgc3R1Y2tBdEJvdHRvbSBpcyB0cnVlOyBpZiBpdCBpcyBmYWxzZSxcclxuICAgICAqICAgdGhlIG51bWJlciBvZiBwaXhlbHMgdGhlIGJvdHRvbSBvZiB0aGUgdHJhY2tlZCBjaGlsZCBpcyBhYm92ZSB0aGVcclxuICAgICAqICAgYm90dG9tIG9mIHRoZSBzY3JvbGwgcGFuZWwuXHJcbiAgICAgKi9cclxuICAgIGdldFNjcm9sbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zY3JvbGxTdGF0ZTtcclxuICAgIH0sXHJcblxyXG4gICAgLyogcmVzZXQgdGhlIHNhdmVkIHNjcm9sbCBzdGF0ZS5cclxuICAgICAqXHJcbiAgICAgKiBUaGlzIGlzIHVzZWZ1bCBpZiB0aGUgbGlzdCBpcyBiZWluZyByZXBsYWNlZCwgYW5kIHlvdSBkb24ndCB3YW50IHRvXHJcbiAgICAgKiBwcmVzZXJ2ZSBzY3JvbGwgZXZlbiBpZiBuZXcgY2hpbGRyZW4gaGFwcGVuIHRvIGhhdmUgdGhlIHNhbWUgc2Nyb2xsXHJcbiAgICAgKiB0b2tlbnMgYXMgb2xkIG9uZXMuXHJcbiAgICAgKlxyXG4gICAgICogVGhpcyB3aWxsIGNhdXNlIHRoZSB2aWV3cG9ydCB0byBiZSBzY3JvbGxlZCBkb3duIHRvIHRoZSBib3R0b20gb24gdGhlXHJcbiAgICAgKiBuZXh0IHVwZGF0ZSBvZiB0aGUgY2hpbGQgbGlzdC4gVGhpcyBpcyBkaWZmZXJlbnQgdG8gc2Nyb2xsVG9Cb3R0b20oKSxcclxuICAgICAqIHdoaWNoIHdvdWxkIHNhdmUgdGhlIGN1cnJlbnQgYm90dG9tLW1vc3QgY2hpbGQgYXMgdGhlIGFjdGl2ZSBvbmUgKHNvIGlzXHJcbiAgICAgKiBubyB1c2UgaWYgbm8gY2hpbGRyZW4gZXhpc3QgeWV0LCBvciBpZiB5b3UgYXJlIGFib3V0IHRvIHJlcGxhY2UgdGhlXHJcbiAgICAgKiBjaGlsZCBsaXN0LilcclxuICAgICAqL1xyXG4gICAgcmVzZXRTY3JvbGxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zY3JvbGxTdGF0ZSA9IHtcclxuICAgICAgICAgICAgc3R1Y2tBdEJvdHRvbTogdGhpcy5wcm9wcy5zdGFydEF0Qm90dG9tLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fYm90dG9tR3Jvd3RoID0gMDtcclxuICAgICAgICB0aGlzLl9wYWdlcyA9IDA7XHJcbiAgICAgICAgdGhpcy5fc2Nyb2xsVGltZW91dCA9IG5ldyBUaW1lcigxMDApO1xyXG4gICAgICAgIHRoaXMuX2hlaWdodFVwZGF0ZUluUHJvZ3Jlc3MgPSBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBqdW1wIHRvIHRoZSB0b3Agb2YgdGhlIGNvbnRlbnQuXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFRvVG9wOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9nZXRTY3JvbGxOb2RlKCkuc2Nyb2xsVG9wID0gMDtcclxuICAgICAgICB0aGlzLl9zYXZlU2Nyb2xsU3RhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBqdW1wIHRvIHRoZSBib3R0b20gb2YgdGhlIGNvbnRlbnQuXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFRvQm90dG9tOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyB0aGUgZWFzaWVzdCB3YXkgdG8gbWFrZSBzdXJlIHRoYXQgdGhlIHNjcm9sbCBzdGF0ZSBpcyBjb3JyZWN0bHlcclxuICAgICAgICAvLyBzYXZlZCBpcyB0byBkbyB0aGUgc2Nyb2xsLCB0aGVuIHNhdmUgdGhlIHVwZGF0ZWQgc3RhdGUuIChDYWxjdWxhdGluZ1xyXG4gICAgICAgIC8vIGl0IG91cnNlbHZlcyBpcyBoYXJkLCBhbmQgd2UgY2FuJ3QgcmVseSBvbiBhbiBvblNjcm9sbCBjYWxsYmFja1xyXG4gICAgICAgIC8vIGhhcHBlbmluZywgc2luY2UgdGhlcmUgbWF5IGJlIG5vIHVzZXItdmlzaWJsZSBjaGFuZ2UgaGVyZSkuXHJcbiAgICAgICAgY29uc3Qgc24gPSB0aGlzLl9nZXRTY3JvbGxOb2RlKCk7XHJcbiAgICAgICAgc24uc2Nyb2xsVG9wID0gc24uc2Nyb2xsSGVpZ2h0O1xyXG4gICAgICAgIHRoaXMuX3NhdmVTY3JvbGxTdGF0ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhZ2UgdXAvZG93bi5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gbXVsdDogLTEgdG8gcGFnZSB1cCwgKzEgdG8gcGFnZSBkb3duXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFJlbGF0aXZlOiBmdW5jdGlvbihtdWx0KSB7XHJcbiAgICAgICAgY29uc3Qgc2Nyb2xsTm9kZSA9IHRoaXMuX2dldFNjcm9sbE5vZGUoKTtcclxuICAgICAgICBjb25zdCBkZWx0YSA9IG11bHQgKiBzY3JvbGxOb2RlLmNsaWVudEhlaWdodCAqIDAuNTtcclxuICAgICAgICBzY3JvbGxOb2RlLnNjcm9sbEJ5KDAsIGRlbHRhKTtcclxuICAgICAgICB0aGlzLl9zYXZlU2Nyb2xsU3RhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTY3JvbGwgdXAvZG93biBpbiByZXNwb25zZSB0byBhIHNjcm9sbCBrZXlcclxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBldiB0aGUga2V5Ym9hcmQgZXZlbnRcclxuICAgICAqL1xyXG4gICAgaGFuZGxlU2Nyb2xsS2V5OiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIHN3aXRjaCAoZXYua2V5KSB7XHJcbiAgICAgICAgICAgIGNhc2UgS2V5LlBBR0VfVVA6XHJcbiAgICAgICAgICAgICAgICBpZiAoIWV2LmN0cmxLZXkgJiYgIWV2LnNoaWZ0S2V5ICYmICFldi5hbHRLZXkgJiYgIWV2Lm1ldGFLZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbFJlbGF0aXZlKC0xKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBLZXkuUEFHRV9ET1dOOlxyXG4gICAgICAgICAgICAgICAgaWYgKCFldi5jdHJsS2V5ICYmICFldi5zaGlmdEtleSAmJiAhZXYuYWx0S2V5ICYmICFldi5tZXRhS2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxSZWxhdGl2ZSgxKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBLZXkuSE9NRTpcclxuICAgICAgICAgICAgICAgIGlmIChldi5jdHJsS2V5ICYmICFldi5zaGlmdEtleSAmJiAhZXYuYWx0S2V5ICYmICFldi5tZXRhS2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxUb1RvcCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlIEtleS5FTkQ6XHJcbiAgICAgICAgICAgICAgICBpZiAoZXYuY3RybEtleSAmJiAhZXYuc2hpZnRLZXkgJiYgIWV2LmFsdEtleSAmJiAhZXYubWV0YUtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsVG9Cb3R0b20oKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLyogU2Nyb2xsIHRoZSBwYW5lbCB0byBicmluZyB0aGUgRE9NIG5vZGUgd2l0aCB0aGUgc2Nyb2xsIHRva2VuXHJcbiAgICAgKiBgc2Nyb2xsVG9rZW5gIGludG8gdmlldy5cclxuICAgICAqXHJcbiAgICAgKiBvZmZzZXRCYXNlIGdpdmVzIHRoZSByZWZlcmVuY2UgcG9pbnQgZm9yIHRoZSBwaXhlbE9mZnNldC4gMCBtZWFucyB0aGVcclxuICAgICAqIHRvcCBvZiB0aGUgY29udGFpbmVyLCAxIG1lYW5zIHRoZSBib3R0b20sIGFuZCBmcmFjdGlvbmFsIHZhbHVlcyBtZWFuXHJcbiAgICAgKiBzb21ld2hlcmUgaW4gdGhlIG1pZGRsZS4gSWYgb21pdHRlZCwgaXQgZGVmYXVsdHMgdG8gMC5cclxuICAgICAqXHJcbiAgICAgKiBwaXhlbE9mZnNldCBnaXZlcyB0aGUgbnVtYmVyIG9mIHBpeGVscyAqYWJvdmUqIHRoZSBvZmZzZXRCYXNlIHRoYXQgdGhlXHJcbiAgICAgKiBub2RlIChzcGVjaWZpY2FsbHksIHRoZSBib3R0b20gb2YgaXQpIHdpbGwgYmUgcG9zaXRpb25lZC4gSWYgb21pdHRlZCwgaXRcclxuICAgICAqIGRlZmF1bHRzIHRvIDAuXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbFRvVG9rZW46IGZ1bmN0aW9uKHNjcm9sbFRva2VuLCBwaXhlbE9mZnNldCwgb2Zmc2V0QmFzZSkge1xyXG4gICAgICAgIHBpeGVsT2Zmc2V0ID0gcGl4ZWxPZmZzZXQgfHwgMDtcclxuICAgICAgICBvZmZzZXRCYXNlID0gb2Zmc2V0QmFzZSB8fCAwO1xyXG5cclxuICAgICAgICAvLyBzZXQgdGhlIHRyYWNrZWRTY3JvbGxUb2tlbiBzbyB3ZSBjYW4gZ2V0IHRoZSBub2RlIHRocm91Z2ggX2dldFRyYWNrZWROb2RlXHJcbiAgICAgICAgdGhpcy5zY3JvbGxTdGF0ZSA9IHtcclxuICAgICAgICAgICAgc3R1Y2tBdEJvdHRvbTogZmFsc2UsXHJcbiAgICAgICAgICAgIHRyYWNrZWRTY3JvbGxUb2tlbjogc2Nyb2xsVG9rZW4sXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCB0cmFja2VkTm9kZSA9IHRoaXMuX2dldFRyYWNrZWROb2RlKCk7XHJcbiAgICAgICAgY29uc3Qgc2Nyb2xsTm9kZSA9IHRoaXMuX2dldFNjcm9sbE5vZGUoKTtcclxuICAgICAgICBpZiAodHJhY2tlZE5vZGUpIHtcclxuICAgICAgICAgICAgLy8gc2V0IHRoZSBzY3JvbGxUb3AgdG8gdGhlIHBvc2l0aW9uIHdlIHdhbnQuXHJcbiAgICAgICAgICAgIC8vIG5vdGUgdGhvdWdoLCB0aGF0IHRoaXMgbWlnaHQgbm90IHN1Y2NlZWQgaWYgdGhlIGNvbWJpbmF0aW9uIG9mIG9mZnNldEJhc2UgYW5kIHBpeGVsT2Zmc2V0XHJcbiAgICAgICAgICAgIC8vIHdvdWxkIHBvc2l0aW9uIHRoZSB0cmFja2VkTm9kZSB0b3dhcmRzIHRoZSB0b3Agb2YgdGhlIHZpZXdwb3J0LlxyXG4gICAgICAgICAgICAvLyBUaGlzIGJlY2F1c2Ugd2hlbiBzZXR0aW5nIHRoZSBzY3JvbGxUb3Agb25seSAxMCBvciBzbyBldmVudHMgbWlnaHQgYmUgbG9hZGVkLFxyXG4gICAgICAgICAgICAvLyBub3QgZ2l2aW5nIGVub3VnaCBjb250ZW50IGJlbG93IHRoZSB0cmFja2VkTm9kZSB0byBzY3JvbGwgZG93bndhcmRzXHJcbiAgICAgICAgICAgIC8vIGVub3VnaCBzbyBpdCBlbmRzIHVwIGluIHRoZSB0b3Agb2YgdGhlIHZpZXdwb3J0LlxyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcInNjcm9sbFRva2VuOiBzZXR0aW5nIHNjcm9sbFRvcFwiLCB7b2Zmc2V0QmFzZSwgcGl4ZWxPZmZzZXQsIG9mZnNldFRvcDogdHJhY2tlZE5vZGUub2Zmc2V0VG9wfSk7XHJcbiAgICAgICAgICAgIHNjcm9sbE5vZGUuc2Nyb2xsVG9wID0gKHRyYWNrZWROb2RlLm9mZnNldFRvcCAtIChzY3JvbGxOb2RlLmNsaWVudEhlaWdodCAqIG9mZnNldEJhc2UpKSArIHBpeGVsT2Zmc2V0O1xyXG4gICAgICAgICAgICB0aGlzLl9zYXZlU2Nyb2xsU3RhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9zYXZlU2Nyb2xsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnN0aWNreUJvdHRvbSAmJiB0aGlzLmlzQXRCb3R0b20oKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNjcm9sbFN0YXRlID0geyBzdHVja0F0Qm90dG9tOiB0cnVlIH07XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwic2F2ZWQgc3R1Y2tBdEJvdHRvbSBzdGF0ZVwiKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2Nyb2xsTm9kZSA9IHRoaXMuX2dldFNjcm9sbE5vZGUoKTtcclxuICAgICAgICBjb25zdCB2aWV3cG9ydEJvdHRvbSA9IHNjcm9sbE5vZGUuc2Nyb2xsSGVpZ2h0IC0gKHNjcm9sbE5vZGUuc2Nyb2xsVG9wICsgc2Nyb2xsTm9kZS5jbGllbnRIZWlnaHQpO1xyXG5cclxuICAgICAgICBjb25zdCBpdGVtbGlzdCA9IHRoaXMuX2l0ZW1saXN0LmN1cnJlbnQ7XHJcbiAgICAgICAgY29uc3QgbWVzc2FnZXMgPSBpdGVtbGlzdC5jaGlsZHJlbjtcclxuICAgICAgICBsZXQgbm9kZSA9IG51bGw7XHJcblxyXG4gICAgICAgIC8vIFRPRE86IGRvIGEgYmluYXJ5IHNlYXJjaCBoZXJlLCBhcyBpdGVtcyBhcmUgc29ydGVkIGJ5IG9mZnNldFRvcFxyXG4gICAgICAgIC8vIGxvb3AgYmFja3dhcmRzLCBmcm9tIGJvdHRvbS1tb3N0IG1lc3NhZ2UgKGFzIHRoYXQgaXMgdGhlIG1vc3QgY29tbW9uIGNhc2UpXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IG1lc3NhZ2VzLmxlbmd0aC0xOyBpID49IDA7IC0taSkge1xyXG4gICAgICAgICAgICBpZiAoIW1lc3NhZ2VzW2ldLmRhdGFzZXQuc2Nyb2xsVG9rZW5zKSB7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBub2RlID0gbWVzc2FnZXNbaV07XHJcbiAgICAgICAgICAgIC8vIGJyZWFrIGF0IHRoZSBmaXJzdCBtZXNzYWdlIChjb21pbmcgZnJvbSB0aGUgYm90dG9tKVxyXG4gICAgICAgICAgICAvLyB0aGF0IGhhcyBpdCdzIG9mZnNldFRvcCBhYm92ZSB0aGUgYm90dG9tIG9mIHRoZSB2aWV3cG9ydC5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX3RvcEZyb21Cb3R0b20obm9kZSkgPiB2aWV3cG9ydEJvdHRvbSkge1xyXG4gICAgICAgICAgICAgICAgLy8gVXNlIHRoaXMgbm9kZSBhcyB0aGUgc2Nyb2xsVG9rZW5cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIW5vZGUpIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJ1bmFibGUgdG8gc2F2ZSBzY3JvbGwgc3RhdGU6IGZvdW5kIG5vIGNoaWxkcmVuIGluIHRoZSB2aWV3cG9ydFwiKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBzY3JvbGxUb2tlbiA9IG5vZGUuZGF0YXNldC5zY3JvbGxUb2tlbnMuc3BsaXQoJywnKVswXTtcclxuICAgICAgICBkZWJ1Z2xvZyhcInNhdmluZyBhbmNob3JlZCBzY3JvbGwgc3RhdGUgdG8gbWVzc2FnZVwiLCBub2RlICYmIG5vZGUuaW5uZXJUZXh0LCBzY3JvbGxUb2tlbik7XHJcbiAgICAgICAgY29uc3QgYm90dG9tT2Zmc2V0ID0gdGhpcy5fdG9wRnJvbUJvdHRvbShub2RlKTtcclxuICAgICAgICB0aGlzLnNjcm9sbFN0YXRlID0ge1xyXG4gICAgICAgICAgICBzdHVja0F0Qm90dG9tOiBmYWxzZSxcclxuICAgICAgICAgICAgdHJhY2tlZE5vZGU6IG5vZGUsXHJcbiAgICAgICAgICAgIHRyYWNrZWRTY3JvbGxUb2tlbjogc2Nyb2xsVG9rZW4sXHJcbiAgICAgICAgICAgIGJvdHRvbU9mZnNldDogYm90dG9tT2Zmc2V0LFxyXG4gICAgICAgICAgICBwaXhlbE9mZnNldDogYm90dG9tT2Zmc2V0IC0gdmlld3BvcnRCb3R0b20sIC8vbmVlZGVkIGZvciByZXN0b3JpbmcgdGhlIHNjcm9sbCBwb3NpdGlvbiB3aGVuIGNvbWluZyBiYWNrIHRvIHRoZSByb29tXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgX3Jlc3RvcmVTYXZlZFNjcm9sbFN0YXRlOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBzY3JvbGxTdGF0ZSA9IHRoaXMuc2Nyb2xsU3RhdGU7XHJcblxyXG4gICAgICAgIGlmIChzY3JvbGxTdGF0ZS5zdHVja0F0Qm90dG9tKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNuID0gdGhpcy5fZ2V0U2Nyb2xsTm9kZSgpO1xyXG4gICAgICAgICAgICBzbi5zY3JvbGxUb3AgPSBzbi5zY3JvbGxIZWlnaHQ7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzY3JvbGxTdGF0ZS50cmFja2VkU2Nyb2xsVG9rZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgaXRlbWxpc3QgPSB0aGlzLl9pdGVtbGlzdC5jdXJyZW50O1xyXG4gICAgICAgICAgICBjb25zdCB0cmFja2VkTm9kZSA9IHRoaXMuX2dldFRyYWNrZWROb2RlKCk7XHJcbiAgICAgICAgICAgIGlmICh0cmFja2VkTm9kZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbmV3Qm90dG9tT2Zmc2V0ID0gdGhpcy5fdG9wRnJvbUJvdHRvbSh0cmFja2VkTm9kZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBib3R0b21EaWZmID0gbmV3Qm90dG9tT2Zmc2V0IC0gc2Nyb2xsU3RhdGUuYm90dG9tT2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYm90dG9tR3Jvd3RoICs9IGJvdHRvbURpZmY7XHJcbiAgICAgICAgICAgICAgICBzY3JvbGxTdGF0ZS5ib3R0b21PZmZzZXQgPSBuZXdCb3R0b21PZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICBpdGVtbGlzdC5zdHlsZS5oZWlnaHQgPSBgJHt0aGlzLl9nZXRMaXN0SGVpZ2h0KCl9cHhgO1xyXG4gICAgICAgICAgICAgICAgZGVidWdsb2coXCJiYWxhbmNpbmcgaGVpZ2h0IGJlY2F1c2UgbWVzc2FnZXMgYmVsb3cgdmlld3BvcnQgZ3JldyBieVwiLCBib3R0b21EaWZmKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMuX2hlaWdodFVwZGF0ZUluUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgICAgdGhpcy5faGVpZ2h0VXBkYXRlSW5Qcm9ncmVzcyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl91cGRhdGVIZWlnaHQoKTtcclxuICAgICAgICAgICAgfSBmaW5hbGx5IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2hlaWdodFVwZGF0ZUluUHJvZ3Jlc3MgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwibm90IHVwZGF0aW5nIGhlaWdodCBiZWNhdXNlIHJlcXVlc3QgYWxyZWFkeSBpbiBwcm9ncmVzc1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgLy8gbmVlZCBhIGJldHRlciBuYW1lIHRoYXQgYWxzbyBpbmRpY2F0ZXMgdGhpcyB3aWxsIGNoYW5nZSBzY3JvbGxUb3A/IFJlYmFsYW5jZSBoZWlnaHQ/IFJldmVhbCBjb250ZW50P1xyXG4gICAgYXN5bmMgX3VwZGF0ZUhlaWdodCgpIHtcclxuICAgICAgICAvLyB3YWl0IHVudGlsIHVzZXIgaGFzIHN0b3BwZWQgc2Nyb2xsaW5nXHJcbiAgICAgICAgaWYgKHRoaXMuX3Njcm9sbFRpbWVvdXQuaXNSdW5uaW5nKCkpIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJ1cGRhdGVIZWlnaHQgd2FpdGluZyBmb3Igc2Nyb2xsaW5nIHRvIGVuZCAuLi4gXCIpO1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLl9zY3JvbGxUaW1lb3V0LmZpbmlzaGVkKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJ1cGRhdGVIZWlnaHQgZ2V0dGluZyBzdHJhaWdodCB0byBidXNpbmVzcywgbm8gc2Nyb2xsaW5nIGdvaW5nIG9uLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFdlIG1pZ2h0IGhhdmUgdW5tb3VudGVkIHNpbmNlIHRoZSB0aW1lciBmaW5pc2hlZCwgc28gYWJvcnQgaWYgc28uXHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNuID0gdGhpcy5fZ2V0U2Nyb2xsTm9kZSgpO1xyXG4gICAgICAgIGNvbnN0IGl0ZW1saXN0ID0gdGhpcy5faXRlbWxpc3QuY3VycmVudDtcclxuICAgICAgICBjb25zdCBjb250ZW50SGVpZ2h0ID0gdGhpcy5fZ2V0TWVzc2FnZXNIZWlnaHQoKTtcclxuICAgICAgICBjb25zdCBtaW5IZWlnaHQgPSBzbi5jbGllbnRIZWlnaHQ7XHJcbiAgICAgICAgY29uc3QgaGVpZ2h0ID0gTWF0aC5tYXgobWluSGVpZ2h0LCBjb250ZW50SGVpZ2h0KTtcclxuICAgICAgICB0aGlzLl9wYWdlcyA9IE1hdGguY2VpbChoZWlnaHQgLyBQQUdFX1NJWkUpO1xyXG4gICAgICAgIHRoaXMuX2JvdHRvbUdyb3d0aCA9IDA7XHJcbiAgICAgICAgY29uc3QgbmV3SGVpZ2h0ID0gdGhpcy5fZ2V0TGlzdEhlaWdodCgpO1xyXG5cclxuICAgICAgICBjb25zdCBzY3JvbGxTdGF0ZSA9IHRoaXMuc2Nyb2xsU3RhdGU7XHJcbiAgICAgICAgaWYgKHNjcm9sbFN0YXRlLnN0dWNrQXRCb3R0b20pIHtcclxuICAgICAgICAgICAgaXRlbWxpc3Quc3R5bGUuaGVpZ2h0ID0gYCR7bmV3SGVpZ2h0fXB4YDtcclxuICAgICAgICAgICAgc24uc2Nyb2xsVG9wID0gc24uc2Nyb2xsSGVpZ2h0O1xyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcInVwZGF0ZUhlaWdodCB0b1wiLCBuZXdIZWlnaHQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2Nyb2xsU3RhdGUudHJhY2tlZFNjcm9sbFRva2VuKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYWNrZWROb2RlID0gdGhpcy5fZ2V0VHJhY2tlZE5vZGUoKTtcclxuICAgICAgICAgICAgLy8gaWYgdGhlIHRpbWVsaW5lIGhhcyBiZWVuIHJlbG9hZGVkXHJcbiAgICAgICAgICAgIC8vIHRoaXMgY2FuIGJlIGNhbGxlZCBiZWZvcmUgc2Nyb2xsVG9Cb3R0b20gb3Igd2hhdGV2ZXIgaGFzIGJlZW4gY2FsbGVkXHJcbiAgICAgICAgICAgIC8vIHNvIGRvbid0IGRvIGFueXRoaW5nIGlmIHRoZSBub2RlIGhhcyBkaXNhcHBlYXJlZCBmcm9tXHJcbiAgICAgICAgICAgIC8vIHRoZSBjdXJyZW50bHkgZmlsbGVkIHBpZWNlIG9mIHRoZSB0aW1lbGluZVxyXG4gICAgICAgICAgICBpZiAodHJhY2tlZE5vZGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9sZFRvcCA9IHRyYWNrZWROb2RlLm9mZnNldFRvcDtcclxuICAgICAgICAgICAgICAgIGl0ZW1saXN0LnN0eWxlLmhlaWdodCA9IGAke25ld0hlaWdodH1weGA7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdUb3AgPSB0cmFja2VkTm9kZS5vZmZzZXRUb3A7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0b3BEaWZmID0gbmV3VG9wIC0gb2xkVG9wO1xyXG4gICAgICAgICAgICAgICAgLy8gaW1wb3J0YW50IHRvIHNjcm9sbCBieSBhIHJlbGF0aXZlIGFtb3VudCBhc1xyXG4gICAgICAgICAgICAgICAgLy8gcmVhZGluZyBzY3JvbGxUb3AgYW5kIHRoZW4gc2V0dGluZyBpdCBtaWdodFxyXG4gICAgICAgICAgICAgICAgLy8geWllbGQgb3V0IG9mIGRhdGUgdmFsdWVzIGFuZCBjYXVzZSBhIGp1bXBcclxuICAgICAgICAgICAgICAgIC8vIHdoZW4gc2V0dGluZyBpdFxyXG4gICAgICAgICAgICAgICAgc24uc2Nyb2xsQnkoMCwgdG9wRGlmZik7XHJcbiAgICAgICAgICAgICAgICBkZWJ1Z2xvZyhcInVwZGF0ZUhlaWdodCB0b1wiLCB7bmV3SGVpZ2h0LCB0b3BEaWZmfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRUcmFja2VkTm9kZSgpIHtcclxuICAgICAgICBjb25zdCBzY3JvbGxTdGF0ZSA9IHRoaXMuc2Nyb2xsU3RhdGU7XHJcbiAgICAgICAgY29uc3QgdHJhY2tlZE5vZGUgPSBzY3JvbGxTdGF0ZS50cmFja2VkTm9kZTtcclxuXHJcbiAgICAgICAgaWYgKCF0cmFja2VkTm9kZSB8fCAhdHJhY2tlZE5vZGUucGFyZW50RWxlbWVudCkge1xyXG4gICAgICAgICAgICBsZXQgbm9kZTtcclxuICAgICAgICAgICAgY29uc3QgbWVzc2FnZXMgPSB0aGlzLl9pdGVtbGlzdC5jdXJyZW50LmNoaWxkcmVuO1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxUb2tlbiA9IHNjcm9sbFN0YXRlLnRyYWNrZWRTY3JvbGxUb2tlbjtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSBtZXNzYWdlcy5sZW5ndGgtMTsgaSA+PSAwOyAtLWkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG0gPSBtZXNzYWdlc1tpXTtcclxuICAgICAgICAgICAgICAgIC8vICdkYXRhLXNjcm9sbC10b2tlbnMnIGlzIGEgRE9NU3RyaW5nIG9mIGNvbW1hLXNlcGFyYXRlZCBzY3JvbGwgdG9rZW5zXHJcbiAgICAgICAgICAgICAgICAvLyBUaGVyZSBtaWdodCBvbmx5IGJlIG9uZSBzY3JvbGwgdG9rZW5cclxuICAgICAgICAgICAgICAgIGlmIChtLmRhdGFzZXQuc2Nyb2xsVG9rZW5zICYmXHJcbiAgICAgICAgICAgICAgICAgICAgbS5kYXRhc2V0LnNjcm9sbFRva2Vucy5zcGxpdCgnLCcpLmluZGV4T2Yoc2Nyb2xsVG9rZW4pICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG5vZGUgPSBtO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChub2RlKSB7XHJcbiAgICAgICAgICAgICAgICBkZWJ1Z2xvZyhcImhhZCB0byBmaW5kIHRyYWNrZWQgbm9kZSBhZ2FpbiBmb3IgXCIgKyBzY3JvbGxTdGF0ZS50cmFja2VkU2Nyb2xsVG9rZW4pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHNjcm9sbFN0YXRlLnRyYWNrZWROb2RlID0gbm9kZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghc2Nyb2xsU3RhdGUudHJhY2tlZE5vZGUpIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJObyBub2RlIHdpdGggOyAnXCIrc2Nyb2xsU3RhdGUudHJhY2tlZFNjcm9sbFRva2VuK1wiJ1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHNjcm9sbFN0YXRlLnRyYWNrZWROb2RlO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0TGlzdEhlaWdodCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYm90dG9tR3Jvd3RoICsgKHRoaXMuX3BhZ2VzICogUEFHRV9TSVpFKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldE1lc3NhZ2VzSGVpZ2h0KCkge1xyXG4gICAgICAgIGNvbnN0IGl0ZW1saXN0ID0gdGhpcy5faXRlbWxpc3QuY3VycmVudDtcclxuICAgICAgICBjb25zdCBsYXN0Tm9kZSA9IGl0ZW1saXN0Lmxhc3RFbGVtZW50Q2hpbGQ7XHJcbiAgICAgICAgY29uc3QgbGFzdE5vZGVCb3R0b20gPSBsYXN0Tm9kZSA/IGxhc3ROb2RlLm9mZnNldFRvcCArIGxhc3ROb2RlLmNsaWVudEhlaWdodCA6IDA7XHJcbiAgICAgICAgY29uc3QgZmlyc3ROb2RlVG9wID0gaXRlbWxpc3QuZmlyc3RFbGVtZW50Q2hpbGQgPyBpdGVtbGlzdC5maXJzdEVsZW1lbnRDaGlsZC5vZmZzZXRUb3AgOiAwO1xyXG4gICAgICAgIC8vIDE4IGlzIGl0ZW1saXN0IHBhZGRpbmdcclxuICAgICAgICByZXR1cm4gbGFzdE5vZGVCb3R0b20gLSBmaXJzdE5vZGVUb3AgKyAoMTggKiAyKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3RvcEZyb21Cb3R0b20obm9kZSkge1xyXG4gICAgICAgIC8vIGN1cnJlbnQgY2FwcGVkIGhlaWdodCAtIGRpc3RhbmNlIGZyb20gdG9wID0gZGlzdGFuY2UgZnJvbSBib3R0b20gb2YgY29udGFpbmVyIHRvIHRvcCBvZiB0cmFja2VkIGVsZW1lbnRcclxuICAgICAgICByZXR1cm4gdGhpcy5faXRlbWxpc3QuY3VycmVudC5jbGllbnRIZWlnaHQgLSBub2RlLm9mZnNldFRvcDtcclxuICAgIH0sXHJcblxyXG4gICAgLyogZ2V0IHRoZSBET00gbm9kZSB3aGljaCBoYXMgdGhlIHNjcm9sbFRvcCBwcm9wZXJ0eSB3ZSBjYXJlIGFib3V0IGZvciBvdXJcclxuICAgICAqIG1lc3NhZ2UgcGFuZWwuXHJcbiAgICAgKi9cclxuICAgIF9nZXRTY3JvbGxOb2RlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHtcclxuICAgICAgICAgICAgLy8gdGhpcyBzaG91bGRuJ3QgaGFwcGVuLCBidXQgd2hlbiBpdCBkb2VzLCB0dXJuIHRoZSBOUEUgaW50b1xyXG4gICAgICAgICAgICAvLyBzb21ldGhpbmcgbW9yZSBtZWFuaW5nZnVsLlxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJTY3JvbGxQYW5lbC5fZ2V0U2Nyb2xsTm9kZSBjYWxsZWQgd2hlbiB1bm1vdW50ZWRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuX2RpdlNjcm9sbCkge1xyXG4gICAgICAgICAgICAvLyBMaWtld2lzZSwgd2Ugc2hvdWxkIGhhdmUgdGhlIHJlZiBieSB0aGlzIHBvaW50LCBidXQgaWYgbm90XHJcbiAgICAgICAgICAgIC8vIHR1cm4gdGhlIE5QRSBpbnRvIHNvbWV0aGluZyBtZWFuaW5nZnVsLlxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJTY3JvbGxQYW5lbC5fZ2V0U2Nyb2xsTm9kZSBjYWxsZWQgYmVmb3JlIEF1dG9IaWRlU2Nyb2xsYmFyIHJlZiBjb2xsZWN0ZWRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5fZGl2U2Nyb2xsO1xyXG4gICAgfSxcclxuXHJcbiAgICBfY29sbGVjdFNjcm9sbDogZnVuY3Rpb24oZGl2U2Nyb2xsKSB7XHJcbiAgICAgICAgdGhpcy5fZGl2U2Nyb2xsID0gZGl2U2Nyb2xsO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgIE1hcmsgdGhlIGJvdHRvbSBvZmZzZXQgb2YgdGhlIGxhc3QgdGlsZSBzbyB3ZSBjYW4gYmFsYW5jZSBpdCBvdXQgd2hlblxyXG4gICAgYW55dGhpbmcgYmVsb3cgaXQgY2hhbmdlcywgYnkgY2FsbGluZyB1cGRhdGVQcmV2ZW50U2hyaW5raW5nLCB0byBrZWVwXHJcbiAgICB0aGUgc2FtZSBtaW5pbXVtIGJvdHRvbSBvZmZzZXQsIGVmZmVjdGl2ZWx5IHByZXZlbnRpbmcgdGhlIHRpbWVsaW5lIHRvIHNocmluay5cclxuICAgICovXHJcbiAgICBwcmV2ZW50U2hyaW5raW5nOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBtZXNzYWdlTGlzdCA9IHRoaXMuX2l0ZW1saXN0LmN1cnJlbnQ7XHJcbiAgICAgICAgY29uc3QgdGlsZXMgPSBtZXNzYWdlTGlzdCAmJiBtZXNzYWdlTGlzdC5jaGlsZHJlbjtcclxuICAgICAgICBpZiAoIW1lc3NhZ2VMaXN0KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGxhc3RUaWxlTm9kZTtcclxuICAgICAgICBmb3IgKGxldCBpID0gdGlsZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcclxuICAgICAgICAgICAgY29uc3Qgbm9kZSA9IHRpbGVzW2ldO1xyXG4gICAgICAgICAgICBpZiAobm9kZS5kYXRhc2V0LnNjcm9sbFRva2Vucykge1xyXG4gICAgICAgICAgICAgICAgbGFzdFRpbGVOb2RlID0gbm9kZTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbGFzdFRpbGVOb2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jbGVhclByZXZlbnRTaHJpbmtpbmcoKTtcclxuICAgICAgICBjb25zdCBvZmZzZXRGcm9tQm90dG9tID0gbWVzc2FnZUxpc3QuY2xpZW50SGVpZ2h0IC0gKGxhc3RUaWxlTm9kZS5vZmZzZXRUb3AgKyBsYXN0VGlsZU5vZGUuY2xpZW50SGVpZ2h0KTtcclxuICAgICAgICB0aGlzLnByZXZlbnRTaHJpbmtpbmdTdGF0ZSA9IHtcclxuICAgICAgICAgICAgb2Zmc2V0RnJvbUJvdHRvbTogb2Zmc2V0RnJvbUJvdHRvbSxcclxuICAgICAgICAgICAgb2Zmc2V0Tm9kZTogbGFzdFRpbGVOb2RlLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgZGVidWdsb2coXCJwcmV2ZW50IHNocmlua2luZywgbGFzdCB0aWxlIFwiLCBvZmZzZXRGcm9tQm90dG9tLCBcInB4IGZyb20gYm90dG9tXCIpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKiogQ2xlYXIgc2hyaW5raW5nIHByZXZlbnRpb24uIFVzZWQgaW50ZXJuYWxseSwgYW5kIHdoZW4gdGhlIHRpbWVsaW5lIGlzIHJlbG9hZGVkLiAqL1xyXG4gICAgY2xlYXJQcmV2ZW50U2hyaW5raW5nOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBtZXNzYWdlTGlzdCA9IHRoaXMuX2l0ZW1saXN0LmN1cnJlbnQ7XHJcbiAgICAgICAgY29uc3QgYmFsYW5jZUVsZW1lbnQgPSBtZXNzYWdlTGlzdCAmJiBtZXNzYWdlTGlzdC5wYXJlbnRFbGVtZW50O1xyXG4gICAgICAgIGlmIChiYWxhbmNlRWxlbWVudCkgYmFsYW5jZUVsZW1lbnQuc3R5bGUucGFkZGluZ0JvdHRvbSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5wcmV2ZW50U2hyaW5raW5nU3RhdGUgPSBudWxsO1xyXG4gICAgICAgIGRlYnVnbG9nKFwicHJldmVudCBzaHJpbmtpbmcgY2xlYXJlZFwiKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICB1cGRhdGUgdGhlIGNvbnRhaW5lciBwYWRkaW5nIHRvIGJhbGFuY2VcclxuICAgIHRoZSBib3R0b20gb2Zmc2V0IG9mIHRoZSBsYXN0IHRpbGUgc2luY2VcclxuICAgIHByZXZlbnRTaHJpbmtpbmcgd2FzIGNhbGxlZC5cclxuICAgIENsZWFycyB0aGUgcHJldmVudC1zaHJpbmtpbmcgc3RhdGUgb25lcyB0aGUgb2Zmc2V0XHJcbiAgICBmcm9tIHRoZSBib3R0b20gb2YgdGhlIG1hcmtlZCB0aWxlIGdyb3dzIGxhcmdlciB0aGFuXHJcbiAgICB3aGF0IGl0IHdhcyB3aGVuIG1hcmtpbmcuXHJcbiAgICAqL1xyXG4gICAgdXBkYXRlUHJldmVudFNocmlua2luZzogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJldmVudFNocmlua2luZ1N0YXRlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNuID0gdGhpcy5fZ2V0U2Nyb2xsTm9kZSgpO1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxTdGF0ZSA9IHRoaXMuc2Nyb2xsU3RhdGU7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2VMaXN0ID0gdGhpcy5faXRlbWxpc3QuY3VycmVudDtcclxuICAgICAgICAgICAgY29uc3Qge29mZnNldE5vZGUsIG9mZnNldEZyb21Cb3R0b219ID0gdGhpcy5wcmV2ZW50U2hyaW5raW5nU3RhdGU7XHJcbiAgICAgICAgICAgIC8vIGVsZW1lbnQgdXNlZCB0byBzZXQgcGFkZGluZ0JvdHRvbSB0byBiYWxhbmNlIHRoZSB0eXBpbmcgbm90aWZzIGRpc2FwcGVhcmluZ1xyXG4gICAgICAgICAgICBjb25zdCBiYWxhbmNlRWxlbWVudCA9IG1lc3NhZ2VMaXN0LnBhcmVudEVsZW1lbnQ7XHJcbiAgICAgICAgICAgIC8vIGlmIHRoZSBvZmZzZXROb2RlIGdvdCB1bm1vdW50ZWQsIGNsZWFyXHJcbiAgICAgICAgICAgIGxldCBzaG91bGRDbGVhciA9ICFvZmZzZXROb2RlLnBhcmVudEVsZW1lbnQ7XHJcbiAgICAgICAgICAgIC8vIGFsc28gaWYgMjAwcHggZnJvbSBib3R0b21cclxuICAgICAgICAgICAgaWYgKCFzaG91bGRDbGVhciAmJiAhc2Nyb2xsU3RhdGUuc3R1Y2tBdEJvdHRvbSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3BhY2VCZWxvd1ZpZXdwb3J0ID0gc24uc2Nyb2xsSGVpZ2h0IC0gKHNuLnNjcm9sbFRvcCArIHNuLmNsaWVudEhlaWdodCk7XHJcbiAgICAgICAgICAgICAgICBzaG91bGRDbGVhciA9IHNwYWNlQmVsb3dWaWV3cG9ydCA+PSAyMDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gdHJ5IHVwZGF0aW5nIGlmIG5vdCBjbGVhcmluZ1xyXG4gICAgICAgICAgICBpZiAoIXNob3VsZENsZWFyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50T2Zmc2V0ID0gbWVzc2FnZUxpc3QuY2xpZW50SGVpZ2h0IC0gKG9mZnNldE5vZGUub2Zmc2V0VG9wICsgb2Zmc2V0Tm9kZS5jbGllbnRIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb2Zmc2V0RGlmZiA9IG9mZnNldEZyb21Cb3R0b20gLSBjdXJyZW50T2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgaWYgKG9mZnNldERpZmYgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFsYW5jZUVsZW1lbnQuc3R5bGUucGFkZGluZ0JvdHRvbSA9IGAke29mZnNldERpZmZ9cHhgO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlYnVnbG9nKFwidXBkYXRlIHByZXZlbnQgc2hyaW5raW5nIFwiLCBvZmZzZXREaWZmLCBcInB4IGZyb20gYm90dG9tXCIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvZmZzZXREaWZmIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3VsZENsZWFyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc2hvdWxkQ2xlYXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJQcmV2ZW50U2hyaW5raW5nKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gVE9ETzogdGhlIGNsYXNzbmFtZXMgb24gdGhlIGRpdiBhbmQgb2wgY291bGQgZG8gd2l0aCBiZWluZyB1cGRhdGVkIHRvXHJcbiAgICAgICAgLy8gcmVmbGVjdCB0aGUgZmFjdCB0aGF0IHdlIGRvbid0IG5lY2Vzc2FyaWx5IGNvbnRhaW4gYSBsaXN0IG9mIG1lc3NhZ2VzLlxyXG4gICAgICAgIC8vIGl0J3Mgbm90IG9idmlvdXMgd2h5IHdlIGhhdmUgYSBzZXBhcmF0ZSBkaXYgYW5kIG9sIGFueXdheS5cclxuXHJcbiAgICAgICAgLy8gZ2l2ZSB0aGUgPG9sPiBhbiBleHBsaWNpdCByb2xlPWxpc3QgYmVjYXVzZSBTYWZhcmkrVm9pY2VPdmVyIHNlZW1zIHRvIHRoaW5rIGFuIG9yZGVyZWQtbGlzdCB3aXRoXHJcbiAgICAgICAgLy8gbGlzdC1zdHlsZS10eXBlOiBub25lOyBpcyBubyBsb25nZXIgYSBsaXN0XHJcbiAgICAgICAgcmV0dXJuICg8QXV0b0hpZGVTY3JvbGxiYXIgd3JhcHBlZFJlZj17dGhpcy5fY29sbGVjdFNjcm9sbH1cclxuICAgICAgICAgICAgICAgIG9uU2Nyb2xsPXt0aGlzLm9uU2Nyb2xsfVxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgbXhfU2Nyb2xsUGFuZWwgJHt0aGlzLnByb3BzLmNsYXNzTmFtZX1gfSBzdHlsZT17dGhpcy5wcm9wcy5zdHlsZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVmlld19tZXNzYWdlTGlzdFdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPG9sIHJlZj17dGhpcy5faXRlbWxpc3R9IGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X01lc3NhZ2VMaXN0XCIgYXJpYS1saXZlPVwicG9saXRlXCIgcm9sZT1cImxpc3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5jaGlsZHJlbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvb2w+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0F1dG9IaWRlU2Nyb2xsYmFyPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==