"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../index"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var Unread = _interopRequireWildcard(require("../../Unread"));

var RoomNotifs = _interopRequireWildcard(require("../../RoomNotifs"));

var FormattingUtils = _interopRequireWildcard(require("../../utils/FormattingUtils"));

var _IndicatorScrollbar = _interopRequireDefault(require("./IndicatorScrollbar"));

var _Keyboard = require("../../Keyboard");

var _matrixJsSdk = require("matrix-js-sdk");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _RoomTile = _interopRequireDefault(require("../views/rooms/RoomTile"));

var _LazyRenderList = _interopRequireDefault(require("../views/elements/LazyRenderList"));

var _languageHandler = require("../../languageHandler");

var _RovingTabIndex = require("../../accessibility/RovingTabIndex");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// turn this on for drop & drag console debugging galore
const debug = false;

class RoomSubList extends _react.default.PureComponent {
  static getDerivedStateFromProps(props, state) {
    return {
      listLength: props.list.length,
      scrollTop: props.list.length === state.listLength ? state.scrollTop : 0
    };
  }

  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onAction", payload => {
      switch (payload.action) {
        case 'on_room_read':
          // XXX: Previously RoomList would forceUpdate whenever on_room_read is dispatched,
          // but this is no longer true, so we must do it here (and can apply the small
          // optimisation of checking that we care about the room being read).
          //
          // Ultimately we need to transition to a state pushing flow where something
          // explicitly notifies the components concerned that the notif count for a room
          // has change (e.g. a Flux store).
          if (this.props.list.some(r => r.roomId === payload.roomId)) {
            this.forceUpdate();
          }

          break;

        case 'view_room':
          if (this.state.hidden && !this.props.forceExpand && payload.show_room_tile && this.props.list.some(r => r.roomId === payload.room_id)) {
            this.toggle();
          }

      }
    });
    (0, _defineProperty2.default)(this, "toggle", () => {
      if (this.isCollapsibleOnClick()) {
        // The header isCollapsible, so the click is to be interpreted as collapse and truncation logic
        const isHidden = !this.state.hidden;
        this.setState({
          hidden: isHidden
        }, () => {
          this.props.onHeaderClick(isHidden);
        });
      } else {
        // The header is stuck, so the click is to be interpreted as a scroll to the header
        this.props.onHeaderClick(this.state.hidden, this._header.current.dataset.originalPosition);
      }
    });
    (0, _defineProperty2.default)(this, "onClick", ev => {
      this.toggle();
    });
    (0, _defineProperty2.default)(this, "onHeaderKeyDown", ev => {
      switch (ev.key) {
        case _Keyboard.Key.ARROW_LEFT:
          // On ARROW_LEFT collapse the room sublist
          if (!this.state.hidden && !this.props.forceExpand) {
            this.onClick();
          }

          ev.stopPropagation();
          break;

        case _Keyboard.Key.ARROW_RIGHT:
          {
            ev.stopPropagation();

            if (this.state.hidden && !this.props.forceExpand) {
              // sublist is collapsed, expand it
              this.onClick();
            } else if (!this.props.forceExpand) {
              // sublist is expanded, go to first room
              const element = this._subList.current && this._subList.current.querySelector(".mx_RoomTile");

              if (element) {
                element.focus();
              }
            }

            break;
          }
      }
    });
    (0, _defineProperty2.default)(this, "onKeyDown", ev => {
      switch (ev.key) {
        // On ARROW_LEFT go to the sublist header
        case _Keyboard.Key.ARROW_LEFT:
          ev.stopPropagation();

          this._headerButton.current.focus();

          break;
        // Consume ARROW_RIGHT so it doesn't cause focus to get sent to composer

        case _Keyboard.Key.ARROW_RIGHT:
          ev.stopPropagation();
      }
    });
    (0, _defineProperty2.default)(this, "onRoomTileClick", (roomId, ev) => {
      _dispatcher.default.dispatch({
        action: 'view_room',
        show_room_tile: true,
        // to make sure the room gets scrolled into view
        room_id: roomId,
        clear_search: ev && (ev.key === _Keyboard.Key.ENTER || ev.key === _Keyboard.Key.SPACE)
      });
    });
    (0, _defineProperty2.default)(this, "_updateSubListCount", () => {
      // Force an update by setting the state to the current state
      // Doing it this way rather than using forceUpdate(), so that the shouldComponentUpdate()
      // method is honoured
      this.setState(this.state);
    });
    (0, _defineProperty2.default)(this, "makeRoomTile", room => {
      return _react.default.createElement(_RoomTile.default, {
        room: room,
        roomSubList: this,
        tagName: this.props.tagName,
        key: room.roomId,
        collapsed: this.props.collapsed || false,
        unread: Unread.doesRoomHaveUnreadMessages(room),
        highlight: this.props.isInvite || RoomNotifs.getUnreadNotificationCount(room, 'highlight') > 0,
        notificationCount: RoomNotifs.getUnreadNotificationCount(room),
        isInvite: this.props.isInvite,
        refreshSubList: this._updateSubListCount,
        incomingCall: null,
        onClick: this.onRoomTileClick
      });
    });
    (0, _defineProperty2.default)(this, "_onNotifBadgeClick", e => {
      // prevent the roomsublist collapsing
      e.preventDefault();
      e.stopPropagation();
      const room = this.props.list.find(room => RoomNotifs.getRoomHasBadge(room));

      if (room) {
        _dispatcher.default.dispatch({
          action: 'view_room',
          room_id: room.roomId
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onInviteBadgeClick", e => {
      // prevent the roomsublist collapsing
      e.preventDefault();
      e.stopPropagation(); // switch to first room in sortedList as that'll be the top of the list for the user

      if (this.props.list && this.props.list.length > 0) {
        _dispatcher.default.dispatch({
          action: 'view_room',
          room_id: this.props.list[0].roomId
        });
      } else if (this.props.extraTiles && this.props.extraTiles.length > 0) {
        // Group Invites are different in that they are all extra tiles and not rooms
        // XXX: this is a horrible special case because Group Invite sublist is a hack
        if (this.props.extraTiles[0].props && this.props.extraTiles[0].props.group instanceof _matrixJsSdk.Group) {
          _dispatcher.default.dispatch({
            action: 'view_group',
            group_id: this.props.extraTiles[0].props.group.groupId
          });
        }
      }
    });
    (0, _defineProperty2.default)(this, "onAddRoom", e => {
      e.stopPropagation();
      if (this.props.onAddRoom) this.props.onAddRoom();
    });
    (0, _defineProperty2.default)(this, "checkOverflow", () => {
      if (this._scroller.current) {
        this._scroller.current.checkOverflow();
      }
    });
    (0, _defineProperty2.default)(this, "setHeight", height => {
      if (this._subList.current) {
        this._subList.current.style.height = "".concat(height, "px");
      }

      this._updateLazyRenderHeight(height);
    });
    (0, _defineProperty2.default)(this, "_onScroll", () => {
      this.setState({
        scrollTop: this._scroller.current.getScrollTop()
      });
    });
    this.state = {
      hidden: this.props.startAsHidden || false,
      // some values to get LazyRenderList starting
      scrollerHeight: 800,
      scrollTop: 0,
      // React 16's getDerivedStateFromProps(props, state) doesn't give the previous props so
      // we have to store the length of the list here so we can see if it's changed or not...
      listLength: null
    };
    this._header = (0, _react.createRef)();
    this._subList = (0, _react.createRef)();
    this._scroller = (0, _react.createRef)();
    this._headerButton = (0, _react.createRef)();
  }

  componentDidMount() {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this.dispatcherRef);
  } // The header is collapsible if it is hidden or not stuck
  // The dataset elements are added in the RoomList _initAndPositionStickyHeaders method


  isCollapsibleOnClick() {
    const stuck = this._header.current.dataset.stuck;

    if (!this.props.forceExpand && (this.state.hidden || stuck === undefined || stuck === "none")) {
      return true;
    } else {
      return false;
    }
  }

  _getHeaderJsx(isCollapsed) {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const AccessibleTooltipButton = sdk.getComponent('elements.AccessibleTooltipButton');
    const subListNotifications = !this.props.isInvite ? RoomNotifs.aggregateNotificationCount(this.props.list) : {
      count: 0,
      highlight: true
    };
    const subListNotifCount = subListNotifications.count;
    const subListNotifHighlight = subListNotifications.highlight; // When collapsed, allow a long hover on the header to show user
    // the full tag name and room count

    let title;

    if (this.props.collapsed) {
      title = this.props.label;
    }

    let incomingCall;

    if (this.props.incomingCall) {
      // We can assume that if we have an incoming call then it is for this list
      const IncomingCallBox = sdk.getComponent("voip.IncomingCallBox");
      incomingCall = _react.default.createElement(IncomingCallBox, {
        className: "mx_RoomSubList_incomingCall",
        incomingCall: this.props.incomingCall
      });
    }

    const len = this.props.list.length + this.props.extraTiles.length;
    let chevron;

    if (len) {
      const chevronClasses = (0, _classnames.default)({
        'mx_RoomSubList_chevron': true,
        'mx_RoomSubList_chevronRight': isCollapsed,
        'mx_RoomSubList_chevronDown': !isCollapsed
      });
      chevron = _react.default.createElement("div", {
        className: chevronClasses
      });
    }

    return _react.default.createElement(_RovingTabIndex.RovingTabIndexWrapper, {
      inputRef: this._headerButton
    }, ({
      onFocus,
      isActive,
      ref
    }) => {
      const tabIndex = isActive ? 0 : -1;
      let badge;

      if (!this.props.collapsed) {
        const badgeClasses = (0, _classnames.default)({
          'mx_RoomSubList_badge': true,
          'mx_RoomSubList_badgeHighlight': subListNotifHighlight
        }); // Wrap the contents in a div and apply styles to the child div so that the browser default outline works

        if (subListNotifCount > 0) {
          badge = _react.default.createElement(AccessibleButton, {
            tabIndex: tabIndex,
            className: badgeClasses,
            onClick: this._onNotifBadgeClick,
            "aria-label": (0, _languageHandler._t)("Jump to first unread room.")
          }, _react.default.createElement("div", null, FormattingUtils.formatCount(subListNotifCount)));
        } else if (this.props.isInvite && this.props.list.length) {
          // no notifications but highlight anyway because this is an invite badge
          badge = _react.default.createElement(AccessibleButton, {
            tabIndex: tabIndex,
            className: badgeClasses,
            onClick: this._onInviteBadgeClick,
            "aria-label": (0, _languageHandler._t)("Jump to first invite.")
          }, _react.default.createElement("div", null, this.props.list.length));
        }
      }

      let addRoomButton;

      if (this.props.onAddRoom) {
        addRoomButton = _react.default.createElement(AccessibleTooltipButton, {
          tabIndex: tabIndex,
          onClick: this.onAddRoom,
          className: "mx_RoomSubList_addRoom",
          title: this.props.addRoomLabel || (0, _languageHandler._t)("Add room")
        });
      }

      return _react.default.createElement("div", {
        className: "mx_RoomSubList_labelContainer",
        title: title,
        ref: this._header,
        onKeyDown: this.onHeaderKeyDown
      }, _react.default.createElement(AccessibleButton, {
        onFocus: onFocus,
        tabIndex: tabIndex,
        inputRef: ref,
        onClick: this.onClick,
        className: "mx_RoomSubList_label",
        "aria-expanded": !isCollapsed,
        role: "treeitem",
        "aria-level": "1"
      }, chevron, _react.default.createElement("span", null, this.props.label), incomingCall), badge, addRoomButton);
    });
  }

  _updateLazyRenderHeight(height) {
    this.setState({
      scrollerHeight: height
    });
  }

  _canUseLazyListRendering() {
    // for now disable lazy rendering as they are already rendered tiles
    // not rooms like props.list we pass to LazyRenderList
    return !this.props.extraTiles || !this.props.extraTiles.length;
  }

  render() {
    const len = this.props.list.length + this.props.extraTiles.length;
    const isCollapsed = this.state.hidden && !this.props.forceExpand;
    const subListClasses = (0, _classnames.default)({
      "mx_RoomSubList": true,
      "mx_RoomSubList_hidden": len && isCollapsed,
      "mx_RoomSubList_nonEmpty": len && !isCollapsed
    });
    let content;

    if (len) {
      if (isCollapsed) {// no body
      } else if (this._canUseLazyListRendering()) {
        content = _react.default.createElement(_IndicatorScrollbar.default, {
          ref: this._scroller,
          className: "mx_RoomSubList_scroll",
          onScroll: this._onScroll
        }, _react.default.createElement(_LazyRenderList.default, {
          scrollTop: this.state.scrollTop,
          height: this.state.scrollerHeight,
          renderItem: this.makeRoomTile,
          itemHeight: 34,
          items: this.props.list
        }));
      } else {
        const roomTiles = this.props.list.map(r => this.makeRoomTile(r));
        const tiles = roomTiles.concat(this.props.extraTiles);
        content = _react.default.createElement(_IndicatorScrollbar.default, {
          ref: this._scroller,
          className: "mx_RoomSubList_scroll",
          onScroll: this._onScroll
        }, tiles);
      }
    } else {
      if (this.props.showSpinner && !isCollapsed) {
        const Loader = sdk.getComponent("elements.Spinner");
        content = _react.default.createElement(Loader, null);
      }
    }

    return _react.default.createElement("div", {
      ref: this._subList,
      className: subListClasses,
      role: "group",
      "aria-label": this.props.label,
      onKeyDown: this.onKeyDown
    }, this._getHeaderJsx(isCollapsed), content);
  }

}

exports.default = RoomSubList;
(0, _defineProperty2.default)(RoomSubList, "displayName", 'RoomSubList');
(0, _defineProperty2.default)(RoomSubList, "debug", debug);
(0, _defineProperty2.default)(RoomSubList, "propTypes", {
  list: _propTypes.default.arrayOf(_propTypes.default.object).isRequired,
  label: _propTypes.default.string.isRequired,
  tagName: _propTypes.default.string,
  addRoomLabel: _propTypes.default.string,
  // passed through to RoomTile and used to highlight room with `!` regardless of notifications count
  isInvite: _propTypes.default.bool,
  startAsHidden: _propTypes.default.bool,
  showSpinner: _propTypes.default.bool,
  // true to show a spinner if 0 elements when expanded
  collapsed: _propTypes.default.bool.isRequired,
  // is LeftPanel collapsed?
  onHeaderClick: _propTypes.default.func,
  incomingCall: _propTypes.default.object,
  extraTiles: _propTypes.default.arrayOf(_propTypes.default.node),
  // extra elements added beneath tiles
  forceExpand: _propTypes.default.bool
});
(0, _defineProperty2.default)(RoomSubList, "defaultProps", {
  onHeaderClick: function () {},
  // NOP
  extraTiles: [],
  isInvite: false
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvUm9vbVN1Ykxpc3QuanMiXSwibmFtZXMiOlsiZGVidWciLCJSb29tU3ViTGlzdCIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyIsInByb3BzIiwic3RhdGUiLCJsaXN0TGVuZ3RoIiwibGlzdCIsImxlbmd0aCIsInNjcm9sbFRvcCIsImNvbnN0cnVjdG9yIiwicGF5bG9hZCIsImFjdGlvbiIsInNvbWUiLCJyIiwicm9vbUlkIiwiZm9yY2VVcGRhdGUiLCJoaWRkZW4iLCJmb3JjZUV4cGFuZCIsInNob3dfcm9vbV90aWxlIiwicm9vbV9pZCIsInRvZ2dsZSIsImlzQ29sbGFwc2libGVPbkNsaWNrIiwiaXNIaWRkZW4iLCJzZXRTdGF0ZSIsIm9uSGVhZGVyQ2xpY2siLCJfaGVhZGVyIiwiY3VycmVudCIsImRhdGFzZXQiLCJvcmlnaW5hbFBvc2l0aW9uIiwiZXYiLCJrZXkiLCJLZXkiLCJBUlJPV19MRUZUIiwib25DbGljayIsInN0b3BQcm9wYWdhdGlvbiIsIkFSUk9XX1JJR0hUIiwiZWxlbWVudCIsIl9zdWJMaXN0IiwicXVlcnlTZWxlY3RvciIsImZvY3VzIiwiX2hlYWRlckJ1dHRvbiIsImRpcyIsImRpc3BhdGNoIiwiY2xlYXJfc2VhcmNoIiwiRU5URVIiLCJTUEFDRSIsInJvb20iLCJ0YWdOYW1lIiwiY29sbGFwc2VkIiwiVW5yZWFkIiwiZG9lc1Jvb21IYXZlVW5yZWFkTWVzc2FnZXMiLCJpc0ludml0ZSIsIlJvb21Ob3RpZnMiLCJnZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCIsIl91cGRhdGVTdWJMaXN0Q291bnQiLCJvblJvb21UaWxlQ2xpY2siLCJlIiwicHJldmVudERlZmF1bHQiLCJmaW5kIiwiZ2V0Um9vbUhhc0JhZGdlIiwiZXh0cmFUaWxlcyIsImdyb3VwIiwiR3JvdXAiLCJncm91cF9pZCIsImdyb3VwSWQiLCJvbkFkZFJvb20iLCJfc2Nyb2xsZXIiLCJjaGVja092ZXJmbG93IiwiaGVpZ2h0Iiwic3R5bGUiLCJfdXBkYXRlTGF6eVJlbmRlckhlaWdodCIsImdldFNjcm9sbFRvcCIsInN0YXJ0QXNIaWRkZW4iLCJzY3JvbGxlckhlaWdodCIsImNvbXBvbmVudERpZE1vdW50IiwiZGlzcGF0Y2hlclJlZiIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJzdHVjayIsInVuZGVmaW5lZCIsIl9nZXRIZWFkZXJKc3giLCJpc0NvbGxhcHNlZCIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJBY2Nlc3NpYmxlVG9vbHRpcEJ1dHRvbiIsInN1Ykxpc3ROb3RpZmljYXRpb25zIiwiYWdncmVnYXRlTm90aWZpY2F0aW9uQ291bnQiLCJjb3VudCIsImhpZ2hsaWdodCIsInN1Ykxpc3ROb3RpZkNvdW50Iiwic3ViTGlzdE5vdGlmSGlnaGxpZ2h0IiwidGl0bGUiLCJsYWJlbCIsImluY29taW5nQ2FsbCIsIkluY29taW5nQ2FsbEJveCIsImxlbiIsImNoZXZyb24iLCJjaGV2cm9uQ2xhc3NlcyIsIm9uRm9jdXMiLCJpc0FjdGl2ZSIsInJlZiIsInRhYkluZGV4IiwiYmFkZ2UiLCJiYWRnZUNsYXNzZXMiLCJfb25Ob3RpZkJhZGdlQ2xpY2siLCJGb3JtYXR0aW5nVXRpbHMiLCJmb3JtYXRDb3VudCIsIl9vbkludml0ZUJhZGdlQ2xpY2siLCJhZGRSb29tQnV0dG9uIiwiYWRkUm9vbUxhYmVsIiwib25IZWFkZXJLZXlEb3duIiwiX2NhblVzZUxhenlMaXN0UmVuZGVyaW5nIiwicmVuZGVyIiwic3ViTGlzdENsYXNzZXMiLCJjb250ZW50IiwiX29uU2Nyb2xsIiwibWFrZVJvb21UaWxlIiwicm9vbVRpbGVzIiwibWFwIiwidGlsZXMiLCJjb25jYXQiLCJzaG93U3Bpbm5lciIsIkxvYWRlciIsIm9uS2V5RG93biIsIlByb3BUeXBlcyIsImFycmF5T2YiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiYm9vbCIsImZ1bmMiLCJub2RlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBbUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQWpDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUNBO0FBQ0EsTUFBTUEsS0FBSyxHQUFHLEtBQWQ7O0FBRWUsTUFBTUMsV0FBTixTQUEwQkMsZUFBTUMsYUFBaEMsQ0FBOEM7QUE2QnpELFNBQU9DLHdCQUFQLENBQWdDQyxLQUFoQyxFQUF1Q0MsS0FBdkMsRUFBOEM7QUFDMUMsV0FBTztBQUNIQyxNQUFBQSxVQUFVLEVBQUVGLEtBQUssQ0FBQ0csSUFBTixDQUFXQyxNQURwQjtBQUVIQyxNQUFBQSxTQUFTLEVBQUVMLEtBQUssQ0FBQ0csSUFBTixDQUFXQyxNQUFYLEtBQXNCSCxLQUFLLENBQUNDLFVBQTVCLEdBQXlDRCxLQUFLLENBQUNJLFNBQS9DLEdBQTJEO0FBRm5FLEtBQVA7QUFJSDs7QUFFREMsRUFBQUEsV0FBVyxDQUFDTixLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsb0RBc0NQTyxPQUFELElBQWE7QUFDcEIsY0FBUUEsT0FBTyxDQUFDQyxNQUFoQjtBQUNJLGFBQUssY0FBTDtBQUNJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBSSxLQUFLUixLQUFMLENBQVdHLElBQVgsQ0FBZ0JNLElBQWhCLENBQXNCQyxDQUFELElBQU9BLENBQUMsQ0FBQ0MsTUFBRixLQUFhSixPQUFPLENBQUNJLE1BQWpELENBQUosRUFBOEQ7QUFDMUQsaUJBQUtDLFdBQUw7QUFDSDs7QUFDRDs7QUFFSixhQUFLLFdBQUw7QUFDSSxjQUFJLEtBQUtYLEtBQUwsQ0FBV1ksTUFBWCxJQUFxQixDQUFDLEtBQUtiLEtBQUwsQ0FBV2MsV0FBakMsSUFBZ0RQLE9BQU8sQ0FBQ1EsY0FBeEQsSUFDQSxLQUFLZixLQUFMLENBQVdHLElBQVgsQ0FBZ0JNLElBQWhCLENBQXNCQyxDQUFELElBQU9BLENBQUMsQ0FBQ0MsTUFBRixLQUFhSixPQUFPLENBQUNTLE9BQWpELENBREosRUFFRTtBQUNFLGlCQUFLQyxNQUFMO0FBQ0g7O0FBbkJUO0FBcUJILEtBNURrQjtBQUFBLGtEQThEVixNQUFNO0FBQ1gsVUFBSSxLQUFLQyxvQkFBTCxFQUFKLEVBQWlDO0FBQzdCO0FBQ0EsY0FBTUMsUUFBUSxHQUFHLENBQUMsS0FBS2xCLEtBQUwsQ0FBV1ksTUFBN0I7QUFDQSxhQUFLTyxRQUFMLENBQWM7QUFBQ1AsVUFBQUEsTUFBTSxFQUFFTTtBQUFULFNBQWQsRUFBa0MsTUFBTTtBQUNwQyxlQUFLbkIsS0FBTCxDQUFXcUIsYUFBWCxDQUF5QkYsUUFBekI7QUFDSCxTQUZEO0FBR0gsT0FORCxNQU1PO0FBQ0g7QUFDQSxhQUFLbkIsS0FBTCxDQUFXcUIsYUFBWCxDQUF5QixLQUFLcEIsS0FBTCxDQUFXWSxNQUFwQyxFQUE0QyxLQUFLUyxPQUFMLENBQWFDLE9BQWIsQ0FBcUJDLE9BQXJCLENBQTZCQyxnQkFBekU7QUFDSDtBQUNKLEtBekVrQjtBQUFBLG1EQTJFUkMsRUFBRCxJQUFRO0FBQ2QsV0FBS1QsTUFBTDtBQUNILEtBN0VrQjtBQUFBLDJEQStFQVMsRUFBRCxJQUFRO0FBQ3RCLGNBQVFBLEVBQUUsQ0FBQ0MsR0FBWDtBQUNJLGFBQUtDLGNBQUlDLFVBQVQ7QUFDSTtBQUNBLGNBQUksQ0FBQyxLQUFLNUIsS0FBTCxDQUFXWSxNQUFaLElBQXNCLENBQUMsS0FBS2IsS0FBTCxDQUFXYyxXQUF0QyxFQUFtRDtBQUMvQyxpQkFBS2dCLE9BQUw7QUFDSDs7QUFDREosVUFBQUEsRUFBRSxDQUFDSyxlQUFIO0FBQ0E7O0FBQ0osYUFBS0gsY0FBSUksV0FBVDtBQUFzQjtBQUNsQk4sWUFBQUEsRUFBRSxDQUFDSyxlQUFIOztBQUNBLGdCQUFJLEtBQUs5QixLQUFMLENBQVdZLE1BQVgsSUFBcUIsQ0FBQyxLQUFLYixLQUFMLENBQVdjLFdBQXJDLEVBQWtEO0FBQzlDO0FBQ0EsbUJBQUtnQixPQUFMO0FBQ0gsYUFIRCxNQUdPLElBQUksQ0FBQyxLQUFLOUIsS0FBTCxDQUFXYyxXQUFoQixFQUE2QjtBQUNoQztBQUNBLG9CQUFNbUIsT0FBTyxHQUFHLEtBQUtDLFFBQUwsQ0FBY1gsT0FBZCxJQUF5QixLQUFLVyxRQUFMLENBQWNYLE9BQWQsQ0FBc0JZLGFBQXRCLENBQW9DLGNBQXBDLENBQXpDOztBQUNBLGtCQUFJRixPQUFKLEVBQWE7QUFDVEEsZ0JBQUFBLE9BQU8sQ0FBQ0csS0FBUjtBQUNIO0FBQ0o7O0FBQ0Q7QUFDSDtBQXJCTDtBQXVCSCxLQXZHa0I7QUFBQSxxREF5R05WLEVBQUQsSUFBUTtBQUNoQixjQUFRQSxFQUFFLENBQUNDLEdBQVg7QUFDSTtBQUNBLGFBQUtDLGNBQUlDLFVBQVQ7QUFDSUgsVUFBQUEsRUFBRSxDQUFDSyxlQUFIOztBQUNBLGVBQUtNLGFBQUwsQ0FBbUJkLE9BQW5CLENBQTJCYSxLQUEzQjs7QUFDQTtBQUNKOztBQUNBLGFBQUtSLGNBQUlJLFdBQVQ7QUFDSU4sVUFBQUEsRUFBRSxDQUFDSyxlQUFIO0FBUlI7QUFVSCxLQXBIa0I7QUFBQSwyREFzSEQsQ0FBQ3BCLE1BQUQsRUFBU2UsRUFBVCxLQUFnQjtBQUM5QlksMEJBQUlDLFFBQUosQ0FBYTtBQUNUL0IsUUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVE8sUUFBQUEsY0FBYyxFQUFFLElBRlA7QUFFYTtBQUN0QkMsUUFBQUEsT0FBTyxFQUFFTCxNQUhBO0FBSVQ2QixRQUFBQSxZQUFZLEVBQUdkLEVBQUUsS0FBS0EsRUFBRSxDQUFDQyxHQUFILEtBQVdDLGNBQUlhLEtBQWYsSUFBd0JmLEVBQUUsQ0FBQ0MsR0FBSCxLQUFXQyxjQUFJYyxLQUE1QztBQUpSLE9BQWI7QUFNSCxLQTdIa0I7QUFBQSwrREErSEcsTUFBTTtBQUN4QjtBQUNBO0FBQ0E7QUFDQSxXQUFLdEIsUUFBTCxDQUFjLEtBQUtuQixLQUFuQjtBQUNILEtBcElrQjtBQUFBLHdEQXNJSDBDLElBQUQsSUFBVTtBQUNyQixhQUFPLDZCQUFDLGlCQUFEO0FBQ0gsUUFBQSxJQUFJLEVBQUVBLElBREg7QUFFSCxRQUFBLFdBQVcsRUFBRSxJQUZWO0FBR0gsUUFBQSxPQUFPLEVBQUUsS0FBSzNDLEtBQUwsQ0FBVzRDLE9BSGpCO0FBSUgsUUFBQSxHQUFHLEVBQUVELElBQUksQ0FBQ2hDLE1BSlA7QUFLSCxRQUFBLFNBQVMsRUFBRSxLQUFLWCxLQUFMLENBQVc2QyxTQUFYLElBQXdCLEtBTGhDO0FBTUgsUUFBQSxNQUFNLEVBQUVDLE1BQU0sQ0FBQ0MsMEJBQVAsQ0FBa0NKLElBQWxDLENBTkw7QUFPSCxRQUFBLFNBQVMsRUFBRSxLQUFLM0MsS0FBTCxDQUFXZ0QsUUFBWCxJQUF1QkMsVUFBVSxDQUFDQywwQkFBWCxDQUFzQ1AsSUFBdEMsRUFBNEMsV0FBNUMsSUFBMkQsQ0FQMUY7QUFRSCxRQUFBLGlCQUFpQixFQUFFTSxVQUFVLENBQUNDLDBCQUFYLENBQXNDUCxJQUF0QyxDQVJoQjtBQVNILFFBQUEsUUFBUSxFQUFFLEtBQUszQyxLQUFMLENBQVdnRCxRQVRsQjtBQVVILFFBQUEsY0FBYyxFQUFFLEtBQUtHLG1CQVZsQjtBQVdILFFBQUEsWUFBWSxFQUFFLElBWFg7QUFZSCxRQUFBLE9BQU8sRUFBRSxLQUFLQztBQVpYLFFBQVA7QUFjSCxLQXJKa0I7QUFBQSw4REF1SkdDLENBQUQsSUFBTztBQUN4QjtBQUNBQSxNQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDdEIsZUFBRjtBQUNBLFlBQU1ZLElBQUksR0FBRyxLQUFLM0MsS0FBTCxDQUFXRyxJQUFYLENBQWdCb0QsSUFBaEIsQ0FBcUJaLElBQUksSUFBSU0sVUFBVSxDQUFDTyxlQUFYLENBQTJCYixJQUEzQixDQUE3QixDQUFiOztBQUNBLFVBQUlBLElBQUosRUFBVTtBQUNOTCw0QkFBSUMsUUFBSixDQUFhO0FBQ1QvQixVQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUUSxVQUFBQSxPQUFPLEVBQUUyQixJQUFJLENBQUNoQztBQUZMLFNBQWI7QUFJSDtBQUNKLEtBbEtrQjtBQUFBLCtEQW9LSTBDLENBQUQsSUFBTztBQUN6QjtBQUNBQSxNQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDdEIsZUFBRixHQUh5QixDQUl6Qjs7QUFDQSxVQUFJLEtBQUsvQixLQUFMLENBQVdHLElBQVgsSUFBbUIsS0FBS0gsS0FBTCxDQUFXRyxJQUFYLENBQWdCQyxNQUFoQixHQUF5QixDQUFoRCxFQUFtRDtBQUMvQ2tDLDRCQUFJQyxRQUFKLENBQWE7QUFDVC9CLFVBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVRRLFVBQUFBLE9BQU8sRUFBRSxLQUFLaEIsS0FBTCxDQUFXRyxJQUFYLENBQWdCLENBQWhCLEVBQW1CUTtBQUZuQixTQUFiO0FBSUgsT0FMRCxNQUtPLElBQUksS0FBS1gsS0FBTCxDQUFXeUQsVUFBWCxJQUF5QixLQUFLekQsS0FBTCxDQUFXeUQsVUFBWCxDQUFzQnJELE1BQXRCLEdBQStCLENBQTVELEVBQStEO0FBQ2xFO0FBQ0E7QUFDQSxZQUFJLEtBQUtKLEtBQUwsQ0FBV3lELFVBQVgsQ0FBc0IsQ0FBdEIsRUFBeUJ6RCxLQUF6QixJQUFrQyxLQUFLQSxLQUFMLENBQVd5RCxVQUFYLENBQXNCLENBQXRCLEVBQXlCekQsS0FBekIsQ0FBK0IwRCxLQUEvQixZQUFnREMsa0JBQXRGLEVBQTZGO0FBQ3pGckIsOEJBQUlDLFFBQUosQ0FBYTtBQUNUL0IsWUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVG9ELFlBQUFBLFFBQVEsRUFBRSxLQUFLNUQsS0FBTCxDQUFXeUQsVUFBWCxDQUFzQixDQUF0QixFQUF5QnpELEtBQXpCLENBQStCMEQsS0FBL0IsQ0FBcUNHO0FBRnRDLFdBQWI7QUFJSDtBQUNKO0FBQ0osS0F4TGtCO0FBQUEscURBMExOUixDQUFELElBQU87QUFDZkEsTUFBQUEsQ0FBQyxDQUFDdEIsZUFBRjtBQUNBLFVBQUksS0FBSy9CLEtBQUwsQ0FBVzhELFNBQWYsRUFBMEIsS0FBSzlELEtBQUwsQ0FBVzhELFNBQVg7QUFDN0IsS0E3TGtCO0FBQUEseURBK1NILE1BQU07QUFDbEIsVUFBSSxLQUFLQyxTQUFMLENBQWV4QyxPQUFuQixFQUE0QjtBQUN4QixhQUFLd0MsU0FBTCxDQUFleEMsT0FBZixDQUF1QnlDLGFBQXZCO0FBQ0g7QUFDSixLQW5Ua0I7QUFBQSxxREFxVE5DLE1BQUQsSUFBWTtBQUNwQixVQUFJLEtBQUsvQixRQUFMLENBQWNYLE9BQWxCLEVBQTJCO0FBQ3ZCLGFBQUtXLFFBQUwsQ0FBY1gsT0FBZCxDQUFzQjJDLEtBQXRCLENBQTRCRCxNQUE1QixhQUF3Q0EsTUFBeEM7QUFDSDs7QUFDRCxXQUFLRSx1QkFBTCxDQUE2QkYsTUFBN0I7QUFDSCxLQTFUa0I7QUFBQSxxREFnVVAsTUFBTTtBQUNkLFdBQUs3QyxRQUFMLENBQWM7QUFBQ2YsUUFBQUEsU0FBUyxFQUFFLEtBQUswRCxTQUFMLENBQWV4QyxPQUFmLENBQXVCNkMsWUFBdkI7QUFBWixPQUFkO0FBQ0gsS0FsVWtCO0FBR2YsU0FBS25FLEtBQUwsR0FBYTtBQUNUWSxNQUFBQSxNQUFNLEVBQUUsS0FBS2IsS0FBTCxDQUFXcUUsYUFBWCxJQUE0QixLQUQzQjtBQUVUO0FBQ0FDLE1BQUFBLGNBQWMsRUFBRSxHQUhQO0FBSVRqRSxNQUFBQSxTQUFTLEVBQUUsQ0FKRjtBQUtUO0FBQ0E7QUFDQUgsTUFBQUEsVUFBVSxFQUFFO0FBUEgsS0FBYjtBQVVBLFNBQUtvQixPQUFMLEdBQWUsdUJBQWY7QUFDQSxTQUFLWSxRQUFMLEdBQWdCLHVCQUFoQjtBQUNBLFNBQUs2QixTQUFMLEdBQWlCLHVCQUFqQjtBQUNBLFNBQUsxQixhQUFMLEdBQXFCLHVCQUFyQjtBQUNIOztBQUVEa0MsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsU0FBS0MsYUFBTCxHQUFxQmxDLG9CQUFJbUMsUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CckMsd0JBQUlzQyxVQUFKLENBQWUsS0FBS0osYUFBcEI7QUFDSCxHQTdEd0QsQ0ErRHpEO0FBQ0E7OztBQUNBdEQsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsVUFBTTJELEtBQUssR0FBRyxLQUFLdkQsT0FBTCxDQUFhQyxPQUFiLENBQXFCQyxPQUFyQixDQUE2QnFELEtBQTNDOztBQUNBLFFBQUksQ0FBQyxLQUFLN0UsS0FBTCxDQUFXYyxXQUFaLEtBQTRCLEtBQUtiLEtBQUwsQ0FBV1ksTUFBWCxJQUFxQmdFLEtBQUssS0FBS0MsU0FBL0IsSUFBNENELEtBQUssS0FBSyxNQUFsRixDQUFKLEVBQStGO0FBQzNGLGFBQU8sSUFBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sS0FBUDtBQUNIO0FBQ0o7O0FBMkpERSxFQUFBQSxhQUFhLENBQUNDLFdBQUQsRUFBYztBQUN2QixVQUFNQyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU1DLHVCQUF1QixHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0NBQWpCLENBQWhDO0FBQ0EsVUFBTUUsb0JBQW9CLEdBQUcsQ0FBQyxLQUFLckYsS0FBTCxDQUFXZ0QsUUFBWixHQUN6QkMsVUFBVSxDQUFDcUMsMEJBQVgsQ0FBc0MsS0FBS3RGLEtBQUwsQ0FBV0csSUFBakQsQ0FEeUIsR0FFekI7QUFBQ29GLE1BQUFBLEtBQUssRUFBRSxDQUFSO0FBQVdDLE1BQUFBLFNBQVMsRUFBRTtBQUF0QixLQUZKO0FBR0EsVUFBTUMsaUJBQWlCLEdBQUdKLG9CQUFvQixDQUFDRSxLQUEvQztBQUNBLFVBQU1HLHFCQUFxQixHQUFHTCxvQkFBb0IsQ0FBQ0csU0FBbkQsQ0FQdUIsQ0FTdkI7QUFDQTs7QUFDQSxRQUFJRyxLQUFKOztBQUNBLFFBQUksS0FBSzNGLEtBQUwsQ0FBVzZDLFNBQWYsRUFBMEI7QUFDdEI4QyxNQUFBQSxLQUFLLEdBQUcsS0FBSzNGLEtBQUwsQ0FBVzRGLEtBQW5CO0FBQ0g7O0FBRUQsUUFBSUMsWUFBSjs7QUFDQSxRQUFJLEtBQUs3RixLQUFMLENBQVc2RixZQUFmLEVBQTZCO0FBQ3pCO0FBQ0EsWUFBTUMsZUFBZSxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXhCO0FBQ0FVLE1BQUFBLFlBQVksR0FDUiw2QkFBQyxlQUFEO0FBQWlCLFFBQUEsU0FBUyxFQUFDLDZCQUEzQjtBQUF5RCxRQUFBLFlBQVksRUFBRSxLQUFLN0YsS0FBTCxDQUFXNkY7QUFBbEYsUUFESjtBQUVIOztBQUVELFVBQU1FLEdBQUcsR0FBRyxLQUFLL0YsS0FBTCxDQUFXRyxJQUFYLENBQWdCQyxNQUFoQixHQUF5QixLQUFLSixLQUFMLENBQVd5RCxVQUFYLENBQXNCckQsTUFBM0Q7QUFDQSxRQUFJNEYsT0FBSjs7QUFDQSxRQUFJRCxHQUFKLEVBQVM7QUFDTCxZQUFNRSxjQUFjLEdBQUcseUJBQVc7QUFDOUIsa0NBQTBCLElBREk7QUFFOUIsdUNBQStCakIsV0FGRDtBQUc5QixzQ0FBOEIsQ0FBQ0E7QUFIRCxPQUFYLENBQXZCO0FBS0FnQixNQUFBQSxPQUFPLEdBQUk7QUFBSyxRQUFBLFNBQVMsRUFBRUM7QUFBaEIsUUFBWDtBQUNIOztBQUVELFdBQU8sNkJBQUMscUNBQUQ7QUFBdUIsTUFBQSxRQUFRLEVBQUUsS0FBSzVEO0FBQXRDLE9BQ0YsQ0FBQztBQUFDNkQsTUFBQUEsT0FBRDtBQUFVQyxNQUFBQSxRQUFWO0FBQW9CQyxNQUFBQTtBQUFwQixLQUFELEtBQThCO0FBQzNCLFlBQU1DLFFBQVEsR0FBR0YsUUFBUSxHQUFHLENBQUgsR0FBTyxDQUFDLENBQWpDO0FBRUEsVUFBSUcsS0FBSjs7QUFDQSxVQUFJLENBQUMsS0FBS3RHLEtBQUwsQ0FBVzZDLFNBQWhCLEVBQTJCO0FBQ3ZCLGNBQU0wRCxZQUFZLEdBQUcseUJBQVc7QUFDNUIsa0NBQXdCLElBREk7QUFFNUIsMkNBQWlDYjtBQUZMLFNBQVgsQ0FBckIsQ0FEdUIsQ0FLdkI7O0FBQ0EsWUFBSUQsaUJBQWlCLEdBQUcsQ0FBeEIsRUFBMkI7QUFDdkJhLFVBQUFBLEtBQUssR0FDRCw2QkFBQyxnQkFBRDtBQUNJLFlBQUEsUUFBUSxFQUFFRCxRQURkO0FBRUksWUFBQSxTQUFTLEVBQUVFLFlBRmY7QUFHSSxZQUFBLE9BQU8sRUFBRSxLQUFLQyxrQkFIbEI7QUFJSSwwQkFBWSx5QkFBRyw0QkFBSDtBQUpoQixhQU1JLDBDQUNNQyxlQUFlLENBQUNDLFdBQWhCLENBQTRCakIsaUJBQTVCLENBRE4sQ0FOSixDQURKO0FBWUgsU0FiRCxNQWFPLElBQUksS0FBS3pGLEtBQUwsQ0FBV2dELFFBQVgsSUFBdUIsS0FBS2hELEtBQUwsQ0FBV0csSUFBWCxDQUFnQkMsTUFBM0MsRUFBbUQ7QUFDdEQ7QUFDQWtHLFVBQUFBLEtBQUssR0FDRCw2QkFBQyxnQkFBRDtBQUNJLFlBQUEsUUFBUSxFQUFFRCxRQURkO0FBRUksWUFBQSxTQUFTLEVBQUVFLFlBRmY7QUFHSSxZQUFBLE9BQU8sRUFBRSxLQUFLSSxtQkFIbEI7QUFJSSwwQkFBWSx5QkFBRyx1QkFBSDtBQUpoQixhQU1JLDBDQUNNLEtBQUszRyxLQUFMLENBQVdHLElBQVgsQ0FBZ0JDLE1BRHRCLENBTkosQ0FESjtBQVlIO0FBQ0o7O0FBRUQsVUFBSXdHLGFBQUo7O0FBQ0EsVUFBSSxLQUFLNUcsS0FBTCxDQUFXOEQsU0FBZixFQUEwQjtBQUN0QjhDLFFBQUFBLGFBQWEsR0FDVCw2QkFBQyx1QkFBRDtBQUNJLFVBQUEsUUFBUSxFQUFFUCxRQURkO0FBRUksVUFBQSxPQUFPLEVBQUUsS0FBS3ZDLFNBRmxCO0FBR0ksVUFBQSxTQUFTLEVBQUMsd0JBSGQ7QUFJSSxVQUFBLEtBQUssRUFBRSxLQUFLOUQsS0FBTCxDQUFXNkcsWUFBWCxJQUEyQix5QkFBRyxVQUFIO0FBSnRDLFVBREo7QUFRSDs7QUFFRCxhQUNJO0FBQUssUUFBQSxTQUFTLEVBQUMsK0JBQWY7QUFBK0MsUUFBQSxLQUFLLEVBQUVsQixLQUF0RDtBQUE2RCxRQUFBLEdBQUcsRUFBRSxLQUFLckUsT0FBdkU7QUFBZ0YsUUFBQSxTQUFTLEVBQUUsS0FBS3dGO0FBQWhHLFNBQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxRQUFBLE9BQU8sRUFBRVosT0FEYjtBQUVJLFFBQUEsUUFBUSxFQUFFRyxRQUZkO0FBR0ksUUFBQSxRQUFRLEVBQUVELEdBSGQ7QUFJSSxRQUFBLE9BQU8sRUFBRSxLQUFLdEUsT0FKbEI7QUFLSSxRQUFBLFNBQVMsRUFBQyxzQkFMZDtBQU1JLHlCQUFlLENBQUNrRCxXQU5wQjtBQU9JLFFBQUEsSUFBSSxFQUFDLFVBUFQ7QUFRSSxzQkFBVztBQVJmLFNBVU1nQixPQVZOLEVBV0ksMkNBQU8sS0FBS2hHLEtBQUwsQ0FBVzRGLEtBQWxCLENBWEosRUFZTUMsWUFaTixDQURKLEVBZU1TLEtBZk4sRUFnQk1NLGFBaEJOLENBREo7QUFvQkgsS0F6RUUsQ0FBUDtBQTJFSDs7QUFlRHpDLEVBQUFBLHVCQUF1QixDQUFDRixNQUFELEVBQVM7QUFDNUIsU0FBSzdDLFFBQUwsQ0FBYztBQUFDa0QsTUFBQUEsY0FBYyxFQUFFTDtBQUFqQixLQUFkO0FBQ0g7O0FBTUQ4QyxFQUFBQSx3QkFBd0IsR0FBRztBQUN2QjtBQUNBO0FBQ0EsV0FBTyxDQUFDLEtBQUsvRyxLQUFMLENBQVd5RCxVQUFaLElBQTBCLENBQUMsS0FBS3pELEtBQUwsQ0FBV3lELFVBQVgsQ0FBc0JyRCxNQUF4RDtBQUNIOztBQUVENEcsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTWpCLEdBQUcsR0FBRyxLQUFLL0YsS0FBTCxDQUFXRyxJQUFYLENBQWdCQyxNQUFoQixHQUF5QixLQUFLSixLQUFMLENBQVd5RCxVQUFYLENBQXNCckQsTUFBM0Q7QUFDQSxVQUFNNEUsV0FBVyxHQUFHLEtBQUsvRSxLQUFMLENBQVdZLE1BQVgsSUFBcUIsQ0FBQyxLQUFLYixLQUFMLENBQVdjLFdBQXJEO0FBRUEsVUFBTW1HLGNBQWMsR0FBRyx5QkFBVztBQUM5Qix3QkFBa0IsSUFEWTtBQUU5QiwrQkFBeUJsQixHQUFHLElBQUlmLFdBRkY7QUFHOUIsaUNBQTJCZSxHQUFHLElBQUksQ0FBQ2Y7QUFITCxLQUFYLENBQXZCO0FBTUEsUUFBSWtDLE9BQUo7O0FBQ0EsUUFBSW5CLEdBQUosRUFBUztBQUNMLFVBQUlmLFdBQUosRUFBaUIsQ0FDYjtBQUNILE9BRkQsTUFFTyxJQUFJLEtBQUsrQix3QkFBTCxFQUFKLEVBQXFDO0FBQ3hDRyxRQUFBQSxPQUFPLEdBQ0gsNkJBQUMsMkJBQUQ7QUFBb0IsVUFBQSxHQUFHLEVBQUUsS0FBS25ELFNBQTlCO0FBQXlDLFVBQUEsU0FBUyxFQUFDLHVCQUFuRDtBQUEyRSxVQUFBLFFBQVEsRUFBRSxLQUFLb0Q7QUFBMUYsV0FDSSw2QkFBQyx1QkFBRDtBQUNJLFVBQUEsU0FBUyxFQUFFLEtBQUtsSCxLQUFMLENBQVdJLFNBRDFCO0FBRUksVUFBQSxNQUFNLEVBQUcsS0FBS0osS0FBTCxDQUFXcUUsY0FGeEI7QUFHSSxVQUFBLFVBQVUsRUFBRyxLQUFLOEMsWUFIdEI7QUFJSSxVQUFBLFVBQVUsRUFBRSxFQUpoQjtBQUtJLFVBQUEsS0FBSyxFQUFHLEtBQUtwSCxLQUFMLENBQVdHO0FBTHZCLFVBREosQ0FESjtBQVVILE9BWE0sTUFXQTtBQUNILGNBQU1rSCxTQUFTLEdBQUcsS0FBS3JILEtBQUwsQ0FBV0csSUFBWCxDQUFnQm1ILEdBQWhCLENBQW9CNUcsQ0FBQyxJQUFJLEtBQUswRyxZQUFMLENBQWtCMUcsQ0FBbEIsQ0FBekIsQ0FBbEI7QUFDQSxjQUFNNkcsS0FBSyxHQUFHRixTQUFTLENBQUNHLE1BQVYsQ0FBaUIsS0FBS3hILEtBQUwsQ0FBV3lELFVBQTVCLENBQWQ7QUFDQXlELFFBQUFBLE9BQU8sR0FDSCw2QkFBQywyQkFBRDtBQUFvQixVQUFBLEdBQUcsRUFBRSxLQUFLbkQsU0FBOUI7QUFBeUMsVUFBQSxTQUFTLEVBQUMsdUJBQW5EO0FBQTJFLFVBQUEsUUFBUSxFQUFFLEtBQUtvRDtBQUExRixXQUNNSSxLQUROLENBREo7QUFLSDtBQUNKLEtBdkJELE1BdUJPO0FBQ0gsVUFBSSxLQUFLdkgsS0FBTCxDQUFXeUgsV0FBWCxJQUEwQixDQUFDekMsV0FBL0IsRUFBNEM7QUFDeEMsY0FBTTBDLE1BQU0sR0FBR3hDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjtBQUNBK0IsUUFBQUEsT0FBTyxHQUFHLDZCQUFDLE1BQUQsT0FBVjtBQUNIO0FBQ0o7O0FBRUQsV0FDSTtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUtoRixRQURkO0FBRUksTUFBQSxTQUFTLEVBQUUrRSxjQUZmO0FBR0ksTUFBQSxJQUFJLEVBQUMsT0FIVDtBQUlJLG9CQUFZLEtBQUtqSCxLQUFMLENBQVc0RixLQUozQjtBQUtJLE1BQUEsU0FBUyxFQUFFLEtBQUsrQjtBQUxwQixPQU9NLEtBQUs1QyxhQUFMLENBQW1CQyxXQUFuQixDQVBOLEVBUU1rQyxPQVJOLENBREo7QUFZSDs7QUFuYXdEOzs7OEJBQXhDdEgsVyxpQkFDSSxhOzhCQURKQSxXLFdBRUZELEs7OEJBRkVDLFcsZUFJRTtBQUNmTyxFQUFBQSxJQUFJLEVBQUV5SCxtQkFBVUMsT0FBVixDQUFrQkQsbUJBQVVFLE1BQTVCLEVBQW9DQyxVQUQzQjtBQUVmbkMsRUFBQUEsS0FBSyxFQUFFZ0MsbUJBQVVJLE1BQVYsQ0FBaUJELFVBRlQ7QUFHZm5GLEVBQUFBLE9BQU8sRUFBRWdGLG1CQUFVSSxNQUhKO0FBSWZuQixFQUFBQSxZQUFZLEVBQUVlLG1CQUFVSSxNQUpUO0FBTWY7QUFDQWhGLEVBQUFBLFFBQVEsRUFBRTRFLG1CQUFVSyxJQVBMO0FBU2Y1RCxFQUFBQSxhQUFhLEVBQUV1RCxtQkFBVUssSUFUVjtBQVVmUixFQUFBQSxXQUFXLEVBQUVHLG1CQUFVSyxJQVZSO0FBVWM7QUFDN0JwRixFQUFBQSxTQUFTLEVBQUUrRSxtQkFBVUssSUFBVixDQUFlRixVQVhYO0FBV3VCO0FBQ3RDMUcsRUFBQUEsYUFBYSxFQUFFdUcsbUJBQVVNLElBWlY7QUFhZnJDLEVBQUFBLFlBQVksRUFBRStCLG1CQUFVRSxNQWJUO0FBY2ZyRSxFQUFBQSxVQUFVLEVBQUVtRSxtQkFBVUMsT0FBVixDQUFrQkQsbUJBQVVPLElBQTVCLENBZEc7QUFjZ0M7QUFDL0NySCxFQUFBQSxXQUFXLEVBQUU4RyxtQkFBVUs7QUFmUixDOzhCQUpGckksVyxrQkFzQks7QUFDbEJ5QixFQUFBQSxhQUFhLEVBQUUsWUFBVyxDQUN6QixDQUZpQjtBQUVmO0FBQ0hvQyxFQUFBQSxVQUFVLEVBQUUsRUFITTtBQUlsQlQsRUFBQUEsUUFBUSxFQUFFO0FBSlEsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgKiBhcyBVbnJlYWQgZnJvbSAnLi4vLi4vVW5yZWFkJztcclxuaW1wb3J0ICogYXMgUm9vbU5vdGlmcyBmcm9tICcuLi8uLi9Sb29tTm90aWZzJztcclxuaW1wb3J0ICogYXMgRm9ybWF0dGluZ1V0aWxzIGZyb20gJy4uLy4uL3V0aWxzL0Zvcm1hdHRpbmdVdGlscyc7XHJcbmltcG9ydCBJbmRpY2F0b3JTY3JvbGxiYXIgZnJvbSAnLi9JbmRpY2F0b3JTY3JvbGxiYXInO1xyXG5pbXBvcnQge0tleX0gZnJvbSAnLi4vLi4vS2V5Ym9hcmQnO1xyXG5pbXBvcnQgeyBHcm91cCB9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgUm9vbVRpbGUgZnJvbSBcIi4uL3ZpZXdzL3Jvb21zL1Jvb21UaWxlXCI7XHJcbmltcG9ydCBMYXp5UmVuZGVyTGlzdCBmcm9tIFwiLi4vdmlld3MvZWxlbWVudHMvTGF6eVJlbmRlckxpc3RcIjtcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge1JvdmluZ1RhYkluZGV4V3JhcHBlcn0gZnJvbSBcIi4uLy4uL2FjY2Vzc2liaWxpdHkvUm92aW5nVGFiSW5kZXhcIjtcclxuXHJcbi8vIHR1cm4gdGhpcyBvbiBmb3IgZHJvcCAmIGRyYWcgY29uc29sZSBkZWJ1Z2dpbmcgZ2Fsb3JlXHJcbmNvbnN0IGRlYnVnID0gZmFsc2U7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb29tU3ViTGlzdCBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIGRpc3BsYXlOYW1lID0gJ1Jvb21TdWJMaXN0JztcclxuICAgIHN0YXRpYyBkZWJ1ZyA9IGRlYnVnO1xyXG5cclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgbGlzdDogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9iamVjdCkuaXNSZXF1aXJlZCxcclxuICAgICAgICBsYWJlbDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHRhZ05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgYWRkUm9vbUxhYmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyBwYXNzZWQgdGhyb3VnaCB0byBSb29tVGlsZSBhbmQgdXNlZCB0byBoaWdobGlnaHQgcm9vbSB3aXRoIGAhYCByZWdhcmRsZXNzIG9mIG5vdGlmaWNhdGlvbnMgY291bnRcclxuICAgICAgICBpc0ludml0ZTogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIHN0YXJ0QXNIaWRkZW46IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIHNob3dTcGlubmVyOiBQcm9wVHlwZXMuYm9vbCwgLy8gdHJ1ZSB0byBzaG93IGEgc3Bpbm5lciBpZiAwIGVsZW1lbnRzIHdoZW4gZXhwYW5kZWRcclxuICAgICAgICBjb2xsYXBzZWQ6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsIC8vIGlzIExlZnRQYW5lbCBjb2xsYXBzZWQ/XHJcbiAgICAgICAgb25IZWFkZXJDbGljazogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAgICAgaW5jb21pbmdDYWxsOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgICAgIGV4dHJhVGlsZXM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5ub2RlKSwgLy8gZXh0cmEgZWxlbWVudHMgYWRkZWQgYmVuZWF0aCB0aWxlc1xyXG4gICAgICAgIGZvcmNlRXhwYW5kOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICBvbkhlYWRlckNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB9LCAvLyBOT1BcclxuICAgICAgICBleHRyYVRpbGVzOiBbXSxcclxuICAgICAgICBpc0ludml0ZTogZmFsc2UsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMocHJvcHMsIHN0YXRlKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbGlzdExlbmd0aDogcHJvcHMubGlzdC5sZW5ndGgsXHJcbiAgICAgICAgICAgIHNjcm9sbFRvcDogcHJvcHMubGlzdC5sZW5ndGggPT09IHN0YXRlLmxpc3RMZW5ndGggPyBzdGF0ZS5zY3JvbGxUb3AgOiAwLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGhpZGRlbjogdGhpcy5wcm9wcy5zdGFydEFzSGlkZGVuIHx8IGZhbHNlLFxyXG4gICAgICAgICAgICAvLyBzb21lIHZhbHVlcyB0byBnZXQgTGF6eVJlbmRlckxpc3Qgc3RhcnRpbmdcclxuICAgICAgICAgICAgc2Nyb2xsZXJIZWlnaHQ6IDgwMCxcclxuICAgICAgICAgICAgc2Nyb2xsVG9wOiAwLFxyXG4gICAgICAgICAgICAvLyBSZWFjdCAxNidzIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhwcm9wcywgc3RhdGUpIGRvZXNuJ3QgZ2l2ZSB0aGUgcHJldmlvdXMgcHJvcHMgc29cclxuICAgICAgICAgICAgLy8gd2UgaGF2ZSB0byBzdG9yZSB0aGUgbGVuZ3RoIG9mIHRoZSBsaXN0IGhlcmUgc28gd2UgY2FuIHNlZSBpZiBpdCdzIGNoYW5nZWQgb3Igbm90Li4uXHJcbiAgICAgICAgICAgIGxpc3RMZW5ndGg6IG51bGwsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5faGVhZGVyID0gY3JlYXRlUmVmKCk7XHJcbiAgICAgICAgdGhpcy5fc3ViTGlzdCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgICAgIHRoaXMuX3Njcm9sbGVyID0gY3JlYXRlUmVmKCk7XHJcbiAgICAgICAgdGhpcy5faGVhZGVyQnV0dG9uID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVGhlIGhlYWRlciBpcyBjb2xsYXBzaWJsZSBpZiBpdCBpcyBoaWRkZW4gb3Igbm90IHN0dWNrXHJcbiAgICAvLyBUaGUgZGF0YXNldCBlbGVtZW50cyBhcmUgYWRkZWQgaW4gdGhlIFJvb21MaXN0IF9pbml0QW5kUG9zaXRpb25TdGlja3lIZWFkZXJzIG1ldGhvZFxyXG4gICAgaXNDb2xsYXBzaWJsZU9uQ2xpY2soKSB7XHJcbiAgICAgICAgY29uc3Qgc3R1Y2sgPSB0aGlzLl9oZWFkZXIuY3VycmVudC5kYXRhc2V0LnN0dWNrO1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5mb3JjZUV4cGFuZCAmJiAodGhpcy5zdGF0ZS5oaWRkZW4gfHwgc3R1Y2sgPT09IHVuZGVmaW5lZCB8fCBzdHVjayA9PT0gXCJub25lXCIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25BY3Rpb24gPSAocGF5bG9hZCkgPT4ge1xyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgY2FzZSAnb25fcm9vbV9yZWFkJzpcclxuICAgICAgICAgICAgICAgIC8vIFhYWDogUHJldmlvdXNseSBSb29tTGlzdCB3b3VsZCBmb3JjZVVwZGF0ZSB3aGVuZXZlciBvbl9yb29tX3JlYWQgaXMgZGlzcGF0Y2hlZCxcclxuICAgICAgICAgICAgICAgIC8vIGJ1dCB0aGlzIGlzIG5vIGxvbmdlciB0cnVlLCBzbyB3ZSBtdXN0IGRvIGl0IGhlcmUgKGFuZCBjYW4gYXBwbHkgdGhlIHNtYWxsXHJcbiAgICAgICAgICAgICAgICAvLyBvcHRpbWlzYXRpb24gb2YgY2hlY2tpbmcgdGhhdCB3ZSBjYXJlIGFib3V0IHRoZSByb29tIGJlaW5nIHJlYWQpLlxyXG4gICAgICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgICAgIC8vIFVsdGltYXRlbHkgd2UgbmVlZCB0byB0cmFuc2l0aW9uIHRvIGEgc3RhdGUgcHVzaGluZyBmbG93IHdoZXJlIHNvbWV0aGluZ1xyXG4gICAgICAgICAgICAgICAgLy8gZXhwbGljaXRseSBub3RpZmllcyB0aGUgY29tcG9uZW50cyBjb25jZXJuZWQgdGhhdCB0aGUgbm90aWYgY291bnQgZm9yIGEgcm9vbVxyXG4gICAgICAgICAgICAgICAgLy8gaGFzIGNoYW5nZSAoZS5nLiBhIEZsdXggc3RvcmUpLlxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMubGlzdC5zb21lKChyKSA9PiByLnJvb21JZCA9PT0gcGF5bG9hZC5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3Jvb20nOlxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaGlkZGVuICYmICF0aGlzLnByb3BzLmZvcmNlRXhwYW5kICYmIHBheWxvYWQuc2hvd19yb29tX3RpbGUgJiZcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmxpc3Quc29tZSgocikgPT4gci5yb29tSWQgPT09IHBheWxvYWQucm9vbV9pZClcclxuICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICB0b2dnbGUgPSAoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDb2xsYXBzaWJsZU9uQ2xpY2soKSkge1xyXG4gICAgICAgICAgICAvLyBUaGUgaGVhZGVyIGlzQ29sbGFwc2libGUsIHNvIHRoZSBjbGljayBpcyB0byBiZSBpbnRlcnByZXRlZCBhcyBjb2xsYXBzZSBhbmQgdHJ1bmNhdGlvbiBsb2dpY1xyXG4gICAgICAgICAgICBjb25zdCBpc0hpZGRlbiA9ICF0aGlzLnN0YXRlLmhpZGRlbjtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aGlkZGVuOiBpc0hpZGRlbn0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25IZWFkZXJDbGljayhpc0hpZGRlbik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIFRoZSBoZWFkZXIgaXMgc3R1Y2ssIHNvIHRoZSBjbGljayBpcyB0byBiZSBpbnRlcnByZXRlZCBhcyBhIHNjcm9sbCB0byB0aGUgaGVhZGVyXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25IZWFkZXJDbGljayh0aGlzLnN0YXRlLmhpZGRlbiwgdGhpcy5faGVhZGVyLmN1cnJlbnQuZGF0YXNldC5vcmlnaW5hbFBvc2l0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9uQ2xpY2sgPSAoZXYpID0+IHtcclxuICAgICAgICB0aGlzLnRvZ2dsZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvbkhlYWRlcktleURvd24gPSAoZXYpID0+IHtcclxuICAgICAgICBzd2l0Y2ggKGV2LmtleSkge1xyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19MRUZUOlxyXG4gICAgICAgICAgICAgICAgLy8gT24gQVJST1dfTEVGVCBjb2xsYXBzZSB0aGUgcm9vbSBzdWJsaXN0XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUuaGlkZGVuICYmICF0aGlzLnByb3BzLmZvcmNlRXhwYW5kKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19SSUdIVDoge1xyXG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5oaWRkZW4gJiYgIXRoaXMucHJvcHMuZm9yY2VFeHBhbmQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBzdWJsaXN0IGlzIGNvbGxhcHNlZCwgZXhwYW5kIGl0XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnByb3BzLmZvcmNlRXhwYW5kKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc3VibGlzdCBpcyBleHBhbmRlZCwgZ28gdG8gZmlyc3Qgcm9vbVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSB0aGlzLl9zdWJMaXN0LmN1cnJlbnQgJiYgdGhpcy5fc3ViTGlzdC5jdXJyZW50LnF1ZXJ5U2VsZWN0b3IoXCIubXhfUm9vbVRpbGVcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvbktleURvd24gPSAoZXYpID0+IHtcclxuICAgICAgICBzd2l0Y2ggKGV2LmtleSkge1xyXG4gICAgICAgICAgICAvLyBPbiBBUlJPV19MRUZUIGdvIHRvIHRoZSBzdWJsaXN0IGhlYWRlclxyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19MRUZUOlxyXG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9oZWFkZXJCdXR0b24uY3VycmVudC5mb2N1cygpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIC8vIENvbnN1bWUgQVJST1dfUklHSFQgc28gaXQgZG9lc24ndCBjYXVzZSBmb2N1cyB0byBnZXQgc2VudCB0byBjb21wb3NlclxyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19SSUdIVDpcclxuICAgICAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25Sb29tVGlsZUNsaWNrID0gKHJvb21JZCwgZXYpID0+IHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICBzaG93X3Jvb21fdGlsZTogdHJ1ZSwgLy8gdG8gbWFrZSBzdXJlIHRoZSByb29tIGdldHMgc2Nyb2xsZWQgaW50byB2aWV3XHJcbiAgICAgICAgICAgIHJvb21faWQ6IHJvb21JZCxcclxuICAgICAgICAgICAgY2xlYXJfc2VhcmNoOiAoZXYgJiYgKGV2LmtleSA9PT0gS2V5LkVOVEVSIHx8IGV2LmtleSA9PT0gS2V5LlNQQUNFKSksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF91cGRhdGVTdWJMaXN0Q291bnQgPSAoKSA9PiB7XHJcbiAgICAgICAgLy8gRm9yY2UgYW4gdXBkYXRlIGJ5IHNldHRpbmcgdGhlIHN0YXRlIHRvIHRoZSBjdXJyZW50IHN0YXRlXHJcbiAgICAgICAgLy8gRG9pbmcgaXQgdGhpcyB3YXkgcmF0aGVyIHRoYW4gdXNpbmcgZm9yY2VVcGRhdGUoKSwgc28gdGhhdCB0aGUgc2hvdWxkQ29tcG9uZW50VXBkYXRlKClcclxuICAgICAgICAvLyBtZXRob2QgaXMgaG9ub3VyZWRcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHRoaXMuc3RhdGUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBtYWtlUm9vbVRpbGUgPSAocm9vbSkgPT4ge1xyXG4gICAgICAgIHJldHVybiA8Um9vbVRpbGVcclxuICAgICAgICAgICAgcm9vbT17cm9vbX1cclxuICAgICAgICAgICAgcm9vbVN1Ykxpc3Q9e3RoaXN9XHJcbiAgICAgICAgICAgIHRhZ05hbWU9e3RoaXMucHJvcHMudGFnTmFtZX1cclxuICAgICAgICAgICAga2V5PXtyb29tLnJvb21JZH1cclxuICAgICAgICAgICAgY29sbGFwc2VkPXt0aGlzLnByb3BzLmNvbGxhcHNlZCB8fCBmYWxzZX1cclxuICAgICAgICAgICAgdW5yZWFkPXtVbnJlYWQuZG9lc1Jvb21IYXZlVW5yZWFkTWVzc2FnZXMocm9vbSl9XHJcbiAgICAgICAgICAgIGhpZ2hsaWdodD17dGhpcy5wcm9wcy5pc0ludml0ZSB8fCBSb29tTm90aWZzLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KHJvb20sICdoaWdobGlnaHQnKSA+IDB9XHJcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvbkNvdW50PXtSb29tTm90aWZzLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KHJvb20pfVxyXG4gICAgICAgICAgICBpc0ludml0ZT17dGhpcy5wcm9wcy5pc0ludml0ZX1cclxuICAgICAgICAgICAgcmVmcmVzaFN1Ykxpc3Q9e3RoaXMuX3VwZGF0ZVN1Ykxpc3RDb3VudH1cclxuICAgICAgICAgICAgaW5jb21pbmdDYWxsPXtudWxsfVxyXG4gICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uUm9vbVRpbGVDbGlja31cclxuICAgICAgICAvPjtcclxuICAgIH07XHJcblxyXG4gICAgX29uTm90aWZCYWRnZUNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICAvLyBwcmV2ZW50IHRoZSByb29tc3VibGlzdCBjb2xsYXBzaW5nXHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMucHJvcHMubGlzdC5maW5kKHJvb20gPT4gUm9vbU5vdGlmcy5nZXRSb29tSGFzQmFkZ2Uocm9vbSkpO1xyXG4gICAgICAgIGlmIChyb29tKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgcm9vbV9pZDogcm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uSW52aXRlQmFkZ2VDbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgLy8gcHJldmVudCB0aGUgcm9vbXN1Ymxpc3QgY29sbGFwc2luZ1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIC8vIHN3aXRjaCB0byBmaXJzdCByb29tIGluIHNvcnRlZExpc3QgYXMgdGhhdCdsbCBiZSB0aGUgdG9wIG9mIHRoZSBsaXN0IGZvciB0aGUgdXNlclxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmxpc3QgJiYgdGhpcy5wcm9wcy5saXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICByb29tX2lkOiB0aGlzLnByb3BzLmxpc3RbMF0ucm9vbUlkLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuZXh0cmFUaWxlcyAmJiB0aGlzLnByb3BzLmV4dHJhVGlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAvLyBHcm91cCBJbnZpdGVzIGFyZSBkaWZmZXJlbnQgaW4gdGhhdCB0aGV5IGFyZSBhbGwgZXh0cmEgdGlsZXMgYW5kIG5vdCByb29tc1xyXG4gICAgICAgICAgICAvLyBYWFg6IHRoaXMgaXMgYSBob3JyaWJsZSBzcGVjaWFsIGNhc2UgYmVjYXVzZSBHcm91cCBJbnZpdGUgc3VibGlzdCBpcyBhIGhhY2tcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuZXh0cmFUaWxlc1swXS5wcm9wcyAmJiB0aGlzLnByb3BzLmV4dHJhVGlsZXNbMF0ucHJvcHMuZ3JvdXAgaW5zdGFuY2VvZiBHcm91cCkge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X2dyb3VwJyxcclxuICAgICAgICAgICAgICAgICAgICBncm91cF9pZDogdGhpcy5wcm9wcy5leHRyYVRpbGVzWzBdLnByb3BzLmdyb3VwLmdyb3VwSWQsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25BZGRSb29tID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uQWRkUm9vbSkgdGhpcy5wcm9wcy5vbkFkZFJvb20oKTtcclxuICAgIH07XHJcblxyXG4gICAgX2dldEhlYWRlckpzeChpc0NvbGxhcHNlZCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZVRvb2x0aXBCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlVG9vbHRpcEJ1dHRvbicpO1xyXG4gICAgICAgIGNvbnN0IHN1Ykxpc3ROb3RpZmljYXRpb25zID0gIXRoaXMucHJvcHMuaXNJbnZpdGUgP1xyXG4gICAgICAgICAgICBSb29tTm90aWZzLmFnZ3JlZ2F0ZU5vdGlmaWNhdGlvbkNvdW50KHRoaXMucHJvcHMubGlzdCkgOlxyXG4gICAgICAgICAgICB7Y291bnQ6IDAsIGhpZ2hsaWdodDogdHJ1ZX07XHJcbiAgICAgICAgY29uc3Qgc3ViTGlzdE5vdGlmQ291bnQgPSBzdWJMaXN0Tm90aWZpY2F0aW9ucy5jb3VudDtcclxuICAgICAgICBjb25zdCBzdWJMaXN0Tm90aWZIaWdobGlnaHQgPSBzdWJMaXN0Tm90aWZpY2F0aW9ucy5oaWdobGlnaHQ7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gY29sbGFwc2VkLCBhbGxvdyBhIGxvbmcgaG92ZXIgb24gdGhlIGhlYWRlciB0byBzaG93IHVzZXJcclxuICAgICAgICAvLyB0aGUgZnVsbCB0YWcgbmFtZSBhbmQgcm9vbSBjb3VudFxyXG4gICAgICAgIGxldCB0aXRsZTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jb2xsYXBzZWQpIHtcclxuICAgICAgICAgICAgdGl0bGUgPSB0aGlzLnByb3BzLmxhYmVsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGluY29taW5nQ2FsbDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5pbmNvbWluZ0NhbGwpIHtcclxuICAgICAgICAgICAgLy8gV2UgY2FuIGFzc3VtZSB0aGF0IGlmIHdlIGhhdmUgYW4gaW5jb21pbmcgY2FsbCB0aGVuIGl0IGlzIGZvciB0aGlzIGxpc3RcclxuICAgICAgICAgICAgY29uc3QgSW5jb21pbmdDYWxsQm94ID0gc2RrLmdldENvbXBvbmVudChcInZvaXAuSW5jb21pbmdDYWxsQm94XCIpO1xyXG4gICAgICAgICAgICBpbmNvbWluZ0NhbGwgPVxyXG4gICAgICAgICAgICAgICAgPEluY29taW5nQ2FsbEJveCBjbGFzc05hbWU9XCJteF9Sb29tU3ViTGlzdF9pbmNvbWluZ0NhbGxcIiBpbmNvbWluZ0NhbGw9e3RoaXMucHJvcHMuaW5jb21pbmdDYWxsfSAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGxlbiA9IHRoaXMucHJvcHMubGlzdC5sZW5ndGggKyB0aGlzLnByb3BzLmV4dHJhVGlsZXMubGVuZ3RoO1xyXG4gICAgICAgIGxldCBjaGV2cm9uO1xyXG4gICAgICAgIGlmIChsZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgY2hldnJvbkNsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgICAgICdteF9Sb29tU3ViTGlzdF9jaGV2cm9uJzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICdteF9Sb29tU3ViTGlzdF9jaGV2cm9uUmlnaHQnOiBpc0NvbGxhcHNlZCxcclxuICAgICAgICAgICAgICAgICdteF9Sb29tU3ViTGlzdF9jaGV2cm9uRG93bic6ICFpc0NvbGxhcHNlZCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNoZXZyb24gPSAoPGRpdiBjbGFzc05hbWU9e2NoZXZyb25DbGFzc2VzfSAvPik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPFJvdmluZ1RhYkluZGV4V3JhcHBlciBpbnB1dFJlZj17dGhpcy5faGVhZGVyQnV0dG9ufT5cclxuICAgICAgICAgICAgeyh7b25Gb2N1cywgaXNBY3RpdmUsIHJlZn0pID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhYkluZGV4ID0gaXNBY3RpdmUgPyAwIDogLTE7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGJhZGdlO1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnByb3BzLmNvbGxhcHNlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGJhZGdlQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnbXhfUm9vbVN1Ykxpc3RfYmFkZ2UnOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnbXhfUm9vbVN1Ykxpc3RfYmFkZ2VIaWdobGlnaHQnOiBzdWJMaXN0Tm90aWZIaWdobGlnaHQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gV3JhcCB0aGUgY29udGVudHMgaW4gYSBkaXYgYW5kIGFwcGx5IHN0eWxlcyB0byB0aGUgY2hpbGQgZGl2IHNvIHRoYXQgdGhlIGJyb3dzZXIgZGVmYXVsdCBvdXRsaW5lIHdvcmtzXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN1Ykxpc3ROb3RpZkNvdW50ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWRnZSA9IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFiSW5kZXg9e3RhYkluZGV4fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YmFkZ2VDbGFzc2VzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uTm90aWZCYWRnZUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9e190KFwiSnVtcCB0byBmaXJzdCB1bnJlYWQgcm9vbS5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBGb3JtYXR0aW5nVXRpbHMuZm9ybWF0Q291bnQoc3ViTGlzdE5vdGlmQ291bnQpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuaXNJbnZpdGUgJiYgdGhpcy5wcm9wcy5saXN0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBubyBub3RpZmljYXRpb25zIGJ1dCBoaWdobGlnaHQgYW55d2F5IGJlY2F1c2UgdGhpcyBpcyBhbiBpbnZpdGUgYmFkZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFkZ2UgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYkluZGV4PXt0YWJJbmRleH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2JhZGdlQ2xhc3Nlc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkludml0ZUJhZGdlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17X3QoXCJKdW1wIHRvIGZpcnN0IGludml0ZS5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLmxpc3QubGVuZ3RoIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGFkZFJvb21CdXR0b247XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkFkZFJvb20pIHtcclxuICAgICAgICAgICAgICAgICAgICBhZGRSb29tQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZVRvb2x0aXBCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYkluZGV4PXt0YWJJbmRleH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25BZGRSb29tfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUm9vbVN1Ykxpc3RfYWRkUm9vbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17dGhpcy5wcm9wcy5hZGRSb29tTGFiZWwgfHwgX3QoXCJBZGQgcm9vbVwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3ViTGlzdF9sYWJlbENvbnRhaW5lclwiIHRpdGxlPXt0aXRsZX0gcmVmPXt0aGlzLl9oZWFkZXJ9IG9uS2V5RG93bj17dGhpcy5vbkhlYWRlcktleURvd259PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25Gb2N1cz17b25Gb2N1c31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYkluZGV4PXt0YWJJbmRleH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0UmVmPXtyZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tU3ViTGlzdF9sYWJlbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXshaXNDb2xsYXBzZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwidHJlZWl0ZW1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1sZXZlbD1cIjFcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGNoZXZyb24gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e3RoaXMucHJvcHMubGFiZWx9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpbmNvbWluZ0NhbGwgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgYmFkZ2UgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGFkZFJvb21CdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSB9XHJcbiAgICAgICAgPC9Sb3ZpbmdUYWJJbmRleFdyYXBwZXI+O1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrT3ZlcmZsb3cgPSAoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Njcm9sbGVyLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2Nyb2xsZXIuY3VycmVudC5jaGVja092ZXJmbG93KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBzZXRIZWlnaHQgPSAoaGVpZ2h0KSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1Ykxpc3QuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJMaXN0LmN1cnJlbnQuc3R5bGUuaGVpZ2h0ID0gYCR7aGVpZ2h0fXB4YDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlTGF6eVJlbmRlckhlaWdodChoZWlnaHQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfdXBkYXRlTGF6eVJlbmRlckhlaWdodChoZWlnaHQpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtzY3JvbGxlckhlaWdodDogaGVpZ2h0fSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2Nyb2xsID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Njcm9sbFRvcDogdGhpcy5fc2Nyb2xsZXIuY3VycmVudC5nZXRTY3JvbGxUb3AoKX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY2FuVXNlTGF6eUxpc3RSZW5kZXJpbmcoKSB7XHJcbiAgICAgICAgLy8gZm9yIG5vdyBkaXNhYmxlIGxhenkgcmVuZGVyaW5nIGFzIHRoZXkgYXJlIGFscmVhZHkgcmVuZGVyZWQgdGlsZXNcclxuICAgICAgICAvLyBub3Qgcm9vbXMgbGlrZSBwcm9wcy5saXN0IHdlIHBhc3MgdG8gTGF6eVJlbmRlckxpc3RcclxuICAgICAgICByZXR1cm4gIXRoaXMucHJvcHMuZXh0cmFUaWxlcyB8fCAhdGhpcy5wcm9wcy5leHRyYVRpbGVzLmxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgbGVuID0gdGhpcy5wcm9wcy5saXN0Lmxlbmd0aCArIHRoaXMucHJvcHMuZXh0cmFUaWxlcy5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgaXNDb2xsYXBzZWQgPSB0aGlzLnN0YXRlLmhpZGRlbiAmJiAhdGhpcy5wcm9wcy5mb3JjZUV4cGFuZDtcclxuXHJcbiAgICAgICAgY29uc3Qgc3ViTGlzdENsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgXCJteF9Sb29tU3ViTGlzdFwiOiB0cnVlLFxyXG4gICAgICAgICAgICBcIm14X1Jvb21TdWJMaXN0X2hpZGRlblwiOiBsZW4gJiYgaXNDb2xsYXBzZWQsXHJcbiAgICAgICAgICAgIFwibXhfUm9vbVN1Ykxpc3Rfbm9uRW1wdHlcIjogbGVuICYmICFpc0NvbGxhcHNlZCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGNvbnRlbnQ7XHJcbiAgICAgICAgaWYgKGxlbikge1xyXG4gICAgICAgICAgICBpZiAoaXNDb2xsYXBzZWQpIHtcclxuICAgICAgICAgICAgICAgIC8vIG5vIGJvZHlcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9jYW5Vc2VMYXp5TGlzdFJlbmRlcmluZygpKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50ID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxJbmRpY2F0b3JTY3JvbGxiYXIgcmVmPXt0aGlzLl9zY3JvbGxlcn0gY2xhc3NOYW1lPVwibXhfUm9vbVN1Ykxpc3Rfc2Nyb2xsXCIgb25TY3JvbGw9e3RoaXMuX29uU2Nyb2xsfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPExhenlSZW5kZXJMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY3JvbGxUb3A9e3RoaXMuc3RhdGUuc2Nyb2xsVG9wIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD17IHRoaXMuc3RhdGUuc2Nyb2xsZXJIZWlnaHQgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVuZGVySXRlbT17IHRoaXMubWFrZVJvb21UaWxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1IZWlnaHQ9ezM0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbXM9eyB0aGlzLnByb3BzLmxpc3QgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvSW5kaWNhdG9yU2Nyb2xsYmFyPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb21UaWxlcyA9IHRoaXMucHJvcHMubGlzdC5tYXAociA9PiB0aGlzLm1ha2VSb29tVGlsZShyKSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0aWxlcyA9IHJvb21UaWxlcy5jb25jYXQodGhpcy5wcm9wcy5leHRyYVRpbGVzKTtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEluZGljYXRvclNjcm9sbGJhciByZWY9e3RoaXMuX3Njcm9sbGVyfSBjbGFzc05hbWU9XCJteF9Sb29tU3ViTGlzdF9zY3JvbGxcIiBvblNjcm9sbD17dGhpcy5fb25TY3JvbGx9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRpbGVzIH1cclxuICAgICAgICAgICAgICAgICAgICA8L0luZGljYXRvclNjcm9sbGJhcj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93U3Bpbm5lciAmJiAhaXNDb2xsYXBzZWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICAgICAgY29udGVudCA9IDxMb2FkZXIgLz47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fc3ViTGlzdH1cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17c3ViTGlzdENsYXNzZXN9XHJcbiAgICAgICAgICAgICAgICByb2xlPVwiZ3JvdXBcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17dGhpcy5wcm9wcy5sYWJlbH1cclxuICAgICAgICAgICAgICAgIG9uS2V5RG93bj17dGhpcy5vbktleURvd259XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHsgdGhpcy5fZ2V0SGVhZGVySnN4KGlzQ29sbGFwc2VkKSB9XHJcbiAgICAgICAgICAgICAgICB7IGNvbnRlbnQgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==