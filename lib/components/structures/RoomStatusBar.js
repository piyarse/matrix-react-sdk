"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

var _languageHandler = require("../../languageHandler");

var sdk = _interopRequireWildcard(require("../../index"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _Resend = _interopRequireDefault(require("../../Resend"));

var cryptodevices = _interopRequireWildcard(require("../../cryptodevices"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _ErrorUtils = require("../../utils/ErrorUtils");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const STATUS_BAR_HIDDEN = 0;
const STATUS_BAR_EXPANDED = 1;
const STATUS_BAR_EXPANDED_LARGE = 2;

function getUnsentMessages(room) {
  if (!room) {
    return [];
  }

  return room.getPendingEvents().filter(function (ev) {
    return ev.status === _matrixJsSdk.default.EventStatus.NOT_SENT;
  });
}

var _default = (0, _createReactClass.default)({
  displayName: 'RoomStatusBar',
  propTypes: {
    // the room this statusbar is representing.
    room: _propTypes.default.object.isRequired,
    // This is true when the user is alone in the room, but has also sent a message.
    // Used to suggest to the user to invite someone
    sentMessageAndIsAlone: _propTypes.default.bool,
    // true if there is an active call in this room (means we show
    // the 'Active Call' text in the status bar if there is nothing
    // more interesting)
    hasActiveCall: _propTypes.default.bool,
    // true if the room is being peeked at. This affects components that shouldn't
    // logically be shown when peeking, such as a prompt to invite people to a room.
    isPeeking: _propTypes.default.bool,
    // callback for when the user clicks on the 'resend all' button in the
    // 'unsent messages' bar
    onResendAllClick: _propTypes.default.func,
    // callback for when the user clicks on the 'cancel all' button in the
    // 'unsent messages' bar
    onCancelAllClick: _propTypes.default.func,
    // callback for when the user clicks on the 'invite others' button in the
    // 'you are alone' bar
    onInviteClick: _propTypes.default.func,
    // callback for when the user clicks on the 'stop warning me' button in the
    // 'you are alone' bar
    onStopWarningClick: _propTypes.default.func,
    // callback for when we do something that changes the size of the
    // status bar. This is used to trigger a re-layout in the parent
    // component.
    onResize: _propTypes.default.func,
    // callback for when the status bar can be hidden from view, as it is
    // not displaying anything
    onHidden: _propTypes.default.func,
    // callback for when the status bar is displaying something and should
    // be visible
    onVisible: _propTypes.default.func
  },
  getInitialState: function () {
    return {
      syncState: _MatrixClientPeg.MatrixClientPeg.get().getSyncState(),
      syncStateData: _MatrixClientPeg.MatrixClientPeg.get().getSyncStateData(),
      unsentMessages: getUnsentMessages(this.props.room)
    };
  },
  componentDidMount: function () {
    _MatrixClientPeg.MatrixClientPeg.get().on("sync", this.onSyncStateChange);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.localEchoUpdated", this._onRoomLocalEchoUpdated);

    this._checkSize();
  },
  componentDidUpdate: function () {
    this._checkSize();
  },
  componentWillUnmount: function () {
    // we may have entirely lost our client as we're logging out before clicking login on the guest bar...
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client) {
      client.removeListener("sync", this.onSyncStateChange);
      client.removeListener("Room.localEchoUpdated", this._onRoomLocalEchoUpdated);
    }
  },
  onSyncStateChange: function (state, prevState, data) {
    if (state === "SYNCING" && prevState === "SYNCING") {
      return;
    }

    this.setState({
      syncState: state,
      syncStateData: data
    });
  },
  _onSendWithoutVerifyingClick: function () {
    cryptodevices.getUnknownDevicesForRoom(_MatrixClientPeg.MatrixClientPeg.get(), this.props.room).then(devices => {
      cryptodevices.markAllDevicesKnown(_MatrixClientPeg.MatrixClientPeg.get(), devices);

      _Resend.default.resendUnsentEvents(this.props.room);
    });
  },
  _onResendAllClick: function () {
    _Resend.default.resendUnsentEvents(this.props.room);

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  },
  _onCancelAllClick: function () {
    _Resend.default.cancelUnsentEvents(this.props.room);

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  },
  _onShowDevicesClick: function () {
    cryptodevices.showUnknownDeviceDialogForMessages(_MatrixClientPeg.MatrixClientPeg.get(), this.props.room);
  },
  _onRoomLocalEchoUpdated: function (event, room, oldEventId, oldStatus) {
    if (room.roomId !== this.props.room.roomId) return;
    this.setState({
      unsentMessages: getUnsentMessages(this.props.room)
    });
  },
  // Check whether current size is greater than 0, if yes call props.onVisible
  _checkSize: function () {
    if (this._getSize()) {
      if (this.props.onVisible) this.props.onVisible();
    } else {
      if (this.props.onHidden) this.props.onHidden();
    }
  },
  // We don't need the actual height - just whether it is likely to have
  // changed - so we use '0' to indicate normal size, and other values to
  // indicate other sizes.
  _getSize: function () {
    if (this._shouldShowConnectionError() || this.props.hasActiveCall || this.props.sentMessageAndIsAlone) {
      return STATUS_BAR_EXPANDED;
    } else if (this.state.unsentMessages.length > 0) {
      return STATUS_BAR_EXPANDED_LARGE;
    }

    return STATUS_BAR_HIDDEN;
  },
  // return suitable content for the image on the left of the status bar.
  _getIndicator: function () {
    if (this.props.hasActiveCall) {
      const TintableSvg = sdk.getComponent("elements.TintableSvg");
      return _react.default.createElement(TintableSvg, {
        src: require("../../../res/img/sound-indicator.svg"),
        width: "23",
        height: "20"
      });
    }

    if (this._shouldShowConnectionError()) {
      return null;
    }

    return null;
  },
  _shouldShowConnectionError: function () {
    // no conn bar trumps the "some not sent" msg since you can't resend without
    // a connection!
    // There's one situation in which we don't show this 'no connection' bar, and that's
    // if it's a resource limit exceeded error: those are shown in the top bar.
    const errorIsMauError = Boolean(this.state.syncStateData && this.state.syncStateData.error && this.state.syncStateData.error.errcode === 'M_RESOURCE_LIMIT_EXCEEDED');
    return this.state.syncState === "ERROR" && !errorIsMauError;
  },
  _getUnsentMessageContent: function () {
    const unsentMessages = this.state.unsentMessages;
    if (!unsentMessages.length) return null;
    let title;
    let content;
    const hasUDE = unsentMessages.some(m => {
      return m.error && m.error.name === "UnknownDeviceError";
    });

    if (hasUDE) {
      title = (0, _languageHandler._t)("Message not sent due to unknown sessions being present");
      content = (0, _languageHandler._t)("<showSessionsText>Show sessions</showSessionsText>, <sendAnywayText>send anyway</sendAnywayText> or <cancelText>cancel</cancelText>.", {}, {
        'showSessionsText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "resend",
          onClick: this._onShowDevicesClick
        }, sub),
        'sendAnywayText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "sendAnyway",
          onClick: this._onSendWithoutVerifyingClick
        }, sub),
        'cancelText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "cancel",
          onClick: this._onCancelAllClick
        }, sub)
      });
    } else {
      let consentError = null;
      let resourceLimitError = null;

      for (const m of unsentMessages) {
        if (m.error && m.error.errcode === 'M_CONSENT_NOT_GIVEN') {
          consentError = m.error;
          break;
        } else if (m.error && m.error.errcode === 'M_RESOURCE_LIMIT_EXCEEDED') {
          resourceLimitError = m.error;
          break;
        }
      }

      if (consentError) {
        title = (0, _languageHandler._t)("You can't send any messages until you review and agree to " + "<consentLink>our terms and conditions</consentLink>.", {}, {
          'consentLink': sub => _react.default.createElement("a", {
            href: consentError.data && consentError.data.consent_uri,
            target: "_blank"
          }, sub)
        });
      } else if (resourceLimitError) {
        title = (0, _ErrorUtils.messageForResourceLimitError)(resourceLimitError.data.limit_type, resourceLimitError.data.admin_contact, {
          'monthly_active_user': (0, _languageHandler._td)("Your message wasn't sent because this homeserver has hit its Monthly Active User Limit. " + "Please <a>contact your service administrator</a> to continue using the service."),
          '': (0, _languageHandler._td)("Your message wasn't sent because this homeserver has exceeded a resource limit. " + "Please <a>contact your service administrator</a> to continue using the service.")
        });
      } else if (unsentMessages.length === 1 && unsentMessages[0].error && unsentMessages[0].error.data && unsentMessages[0].error.data.error) {
        title = (0, _ErrorUtils.messageForSendError)(unsentMessages[0].error.data) || unsentMessages[0].error.data.error;
      } else {
        title = (0, _languageHandler._t)('%(count)s of your messages have not been sent.', {
          count: unsentMessages.length
        });
      }

      content = (0, _languageHandler._t)("%(count)s <resendText>Resend all</resendText> or <cancelText>cancel all</cancelText> now. " + "You can also select individual messages to resend or cancel.", {
        count: unsentMessages.length
      }, {
        'resendText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "resend",
          onClick: this._onResendAllClick
        }, sub),
        'cancelText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "cancel",
          onClick: this._onCancelAllClick
        }, sub)
      });
    }

    return _react.default.createElement("div", {
      className: "mx_RoomStatusBar_connectionLostBar"
    }, _react.default.createElement("img", {
      src: require("../../../res/img/feather-customised/warning-triangle.svg"),
      width: "24",
      height: "24",
      title: (0, _languageHandler._t)("Warning"),
      alt: ""
    }), _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_RoomStatusBar_connectionLostBar_title"
    }, title), _react.default.createElement("div", {
      className: "mx_RoomStatusBar_connectionLostBar_desc"
    }, content)));
  },
  // return suitable content for the main (text) part of the status bar.
  _getContent: function () {
    if (this._shouldShowConnectionError()) {
      return _react.default.createElement("div", {
        className: "mx_RoomStatusBar_connectionLostBar"
      }, _react.default.createElement("img", {
        src: require("../../../res/img/feather-customised/warning-triangle.svg"),
        width: "24",
        height: "24",
        title: "/!\\ ",
        alt: "/!\\ "
      }), _react.default.createElement("div", null, _react.default.createElement("div", {
        className: "mx_RoomStatusBar_connectionLostBar_title"
      }, (0, _languageHandler._t)('Connectivity to the server has been lost.')), _react.default.createElement("div", {
        className: "mx_RoomStatusBar_connectionLostBar_desc"
      }, (0, _languageHandler._t)('Sent messages will be stored until your connection has returned.'))));
    }

    if (this.state.unsentMessages.length > 0) {
      return this._getUnsentMessageContent();
    }

    if (this.props.hasActiveCall) {
      return _react.default.createElement("div", {
        className: "mx_RoomStatusBar_callBar"
      }, _react.default.createElement("b", null, (0, _languageHandler._t)('Active call')));
    } // If you're alone in the room, and have sent a message, suggest to invite someone


    if (this.props.sentMessageAndIsAlone && !this.props.isPeeking) {
      return _react.default.createElement("div", {
        className: "mx_RoomStatusBar_isAlone"
      }, (0, _languageHandler._t)("There's no one else here! Would you like to <inviteText>invite others</inviteText> " + "or <nowarnText>stop warning about the empty room</nowarnText>?", {}, {
        'inviteText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "invite",
          onClick: this.props.onInviteClick
        }, sub),
        'nowarnText': sub => _react.default.createElement("a", {
          className: "mx_RoomStatusBar_resend_link",
          key: "nowarn",
          onClick: this.props.onStopWarningClick
        }, sub)
      }));
    }

    return null;
  },
  render: function () {
    const content = this._getContent();

    const indicator = this._getIndicator();

    return _react.default.createElement("div", {
      className: "mx_RoomStatusBar"
    }, _react.default.createElement("div", {
      className: "mx_RoomStatusBar_indicator"
    }, indicator), _react.default.createElement("div", {
      role: "alert"
    }, content));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvUm9vbVN0YXR1c0Jhci5qcyJdLCJuYW1lcyI6WyJTVEFUVVNfQkFSX0hJRERFTiIsIlNUQVRVU19CQVJfRVhQQU5ERUQiLCJTVEFUVVNfQkFSX0VYUEFOREVEX0xBUkdFIiwiZ2V0VW5zZW50TWVzc2FnZXMiLCJyb29tIiwiZ2V0UGVuZGluZ0V2ZW50cyIsImZpbHRlciIsImV2Iiwic3RhdHVzIiwiTWF0cml4IiwiRXZlbnRTdGF0dXMiLCJOT1RfU0VOVCIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsInNlbnRNZXNzYWdlQW5kSXNBbG9uZSIsImJvb2wiLCJoYXNBY3RpdmVDYWxsIiwiaXNQZWVraW5nIiwib25SZXNlbmRBbGxDbGljayIsImZ1bmMiLCJvbkNhbmNlbEFsbENsaWNrIiwib25JbnZpdGVDbGljayIsIm9uU3RvcFdhcm5pbmdDbGljayIsIm9uUmVzaXplIiwib25IaWRkZW4iLCJvblZpc2libGUiLCJnZXRJbml0aWFsU3RhdGUiLCJzeW5jU3RhdGUiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJnZXRTeW5jU3RhdGUiLCJzeW5jU3RhdGVEYXRhIiwiZ2V0U3luY1N0YXRlRGF0YSIsInVuc2VudE1lc3NhZ2VzIiwicHJvcHMiLCJjb21wb25lbnREaWRNb3VudCIsIm9uIiwib25TeW5jU3RhdGVDaGFuZ2UiLCJfb25Sb29tTG9jYWxFY2hvVXBkYXRlZCIsIl9jaGVja1NpemUiLCJjb21wb25lbnREaWRVcGRhdGUiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNsaWVudCIsInJlbW92ZUxpc3RlbmVyIiwic3RhdGUiLCJwcmV2U3RhdGUiLCJkYXRhIiwic2V0U3RhdGUiLCJfb25TZW5kV2l0aG91dFZlcmlmeWluZ0NsaWNrIiwiY3J5cHRvZGV2aWNlcyIsImdldFVua25vd25EZXZpY2VzRm9yUm9vbSIsInRoZW4iLCJkZXZpY2VzIiwibWFya0FsbERldmljZXNLbm93biIsIlJlc2VuZCIsInJlc2VuZFVuc2VudEV2ZW50cyIsIl9vblJlc2VuZEFsbENsaWNrIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJfb25DYW5jZWxBbGxDbGljayIsImNhbmNlbFVuc2VudEV2ZW50cyIsIl9vblNob3dEZXZpY2VzQ2xpY2siLCJzaG93VW5rbm93bkRldmljZURpYWxvZ0Zvck1lc3NhZ2VzIiwiZXZlbnQiLCJvbGRFdmVudElkIiwib2xkU3RhdHVzIiwicm9vbUlkIiwiX2dldFNpemUiLCJfc2hvdWxkU2hvd0Nvbm5lY3Rpb25FcnJvciIsImxlbmd0aCIsIl9nZXRJbmRpY2F0b3IiLCJUaW50YWJsZVN2ZyIsInNkayIsImdldENvbXBvbmVudCIsInJlcXVpcmUiLCJlcnJvcklzTWF1RXJyb3IiLCJCb29sZWFuIiwiZXJyb3IiLCJlcnJjb2RlIiwiX2dldFVuc2VudE1lc3NhZ2VDb250ZW50IiwidGl0bGUiLCJjb250ZW50IiwiaGFzVURFIiwic29tZSIsIm0iLCJuYW1lIiwic3ViIiwiY29uc2VudEVycm9yIiwicmVzb3VyY2VMaW1pdEVycm9yIiwiY29uc2VudF91cmkiLCJsaW1pdF90eXBlIiwiYWRtaW5fY29udGFjdCIsImNvdW50IiwiX2dldENvbnRlbnQiLCJyZW5kZXIiLCJpbmRpY2F0b3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTVCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE4QkEsTUFBTUEsaUJBQWlCLEdBQUcsQ0FBMUI7QUFDQSxNQUFNQyxtQkFBbUIsR0FBRyxDQUE1QjtBQUNBLE1BQU1DLHlCQUF5QixHQUFHLENBQWxDOztBQUVBLFNBQVNDLGlCQUFULENBQTJCQyxJQUEzQixFQUFpQztBQUM3QixNQUFJLENBQUNBLElBQUwsRUFBVztBQUFFLFdBQU8sRUFBUDtBQUFZOztBQUN6QixTQUFPQSxJQUFJLENBQUNDLGdCQUFMLEdBQXdCQyxNQUF4QixDQUErQixVQUFTQyxFQUFULEVBQWE7QUFDL0MsV0FBT0EsRUFBRSxDQUFDQyxNQUFILEtBQWNDLHFCQUFPQyxXQUFQLENBQW1CQyxRQUF4QztBQUNILEdBRk0sQ0FBUDtBQUdIOztlQUVjLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLGVBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0FULElBQUFBLElBQUksRUFBRVUsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRmhCO0FBR1A7QUFDQTtBQUNBQyxJQUFBQSxxQkFBcUIsRUFBRUgsbUJBQVVJLElBTDFCO0FBT1A7QUFDQTtBQUNBO0FBQ0FDLElBQUFBLGFBQWEsRUFBRUwsbUJBQVVJLElBVmxCO0FBWVA7QUFDQTtBQUNBRSxJQUFBQSxTQUFTLEVBQUVOLG1CQUFVSSxJQWRkO0FBZ0JQO0FBQ0E7QUFDQUcsSUFBQUEsZ0JBQWdCLEVBQUVQLG1CQUFVUSxJQWxCckI7QUFvQlA7QUFDQTtBQUNBQyxJQUFBQSxnQkFBZ0IsRUFBRVQsbUJBQVVRLElBdEJyQjtBQXdCUDtBQUNBO0FBQ0FFLElBQUFBLGFBQWEsRUFBRVYsbUJBQVVRLElBMUJsQjtBQTRCUDtBQUNBO0FBQ0FHLElBQUFBLGtCQUFrQixFQUFFWCxtQkFBVVEsSUE5QnZCO0FBZ0NQO0FBQ0E7QUFDQTtBQUNBSSxJQUFBQSxRQUFRLEVBQUVaLG1CQUFVUSxJQW5DYjtBQXFDUDtBQUNBO0FBQ0FLLElBQUFBLFFBQVEsRUFBRWIsbUJBQVVRLElBdkNiO0FBeUNQO0FBQ0E7QUFDQU0sSUFBQUEsU0FBUyxFQUFFZCxtQkFBVVE7QUEzQ2QsR0FIaUI7QUFpRDVCTyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLFNBQVMsRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsWUFBdEIsRUFEUjtBQUVIQyxNQUFBQSxhQUFhLEVBQUVILGlDQUFnQkMsR0FBaEIsR0FBc0JHLGdCQUF0QixFQUZaO0FBR0hDLE1BQUFBLGNBQWMsRUFBRWpDLGlCQUFpQixDQUFDLEtBQUtrQyxLQUFMLENBQVdqQyxJQUFaO0FBSDlCLEtBQVA7QUFLSCxHQXZEMkI7QUF5RDVCa0MsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQlAscUNBQWdCQyxHQUFoQixHQUFzQk8sRUFBdEIsQ0FBeUIsTUFBekIsRUFBaUMsS0FBS0MsaUJBQXRDOztBQUNBVCxxQ0FBZ0JDLEdBQWhCLEdBQXNCTyxFQUF0QixDQUF5Qix1QkFBekIsRUFBa0QsS0FBS0UsdUJBQXZEOztBQUVBLFNBQUtDLFVBQUw7QUFDSCxHQTlEMkI7QUFnRTVCQyxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCLFNBQUtELFVBQUw7QUFDSCxHQWxFMkI7QUFvRTVCRSxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCO0FBQ0EsVUFBTUMsTUFBTSxHQUFHZCxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsUUFBSWEsTUFBSixFQUFZO0FBQ1JBLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixNQUF0QixFQUE4QixLQUFLTixpQkFBbkM7QUFDQUssTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLHVCQUF0QixFQUErQyxLQUFLTCx1QkFBcEQ7QUFDSDtBQUNKLEdBM0UyQjtBQTZFNUJELEVBQUFBLGlCQUFpQixFQUFFLFVBQVNPLEtBQVQsRUFBZ0JDLFNBQWhCLEVBQTJCQyxJQUEzQixFQUFpQztBQUNoRCxRQUFJRixLQUFLLEtBQUssU0FBVixJQUF1QkMsU0FBUyxLQUFLLFNBQXpDLEVBQW9EO0FBQ2hEO0FBQ0g7O0FBQ0QsU0FBS0UsUUFBTCxDQUFjO0FBQ1ZwQixNQUFBQSxTQUFTLEVBQUVpQixLQUREO0FBRVZiLE1BQUFBLGFBQWEsRUFBRWU7QUFGTCxLQUFkO0FBSUgsR0FyRjJCO0FBdUY1QkUsRUFBQUEsNEJBQTRCLEVBQUUsWUFBVztBQUNyQ0MsSUFBQUEsYUFBYSxDQUFDQyx3QkFBZCxDQUF1Q3RCLGlDQUFnQkMsR0FBaEIsRUFBdkMsRUFBOEQsS0FBS0ssS0FBTCxDQUFXakMsSUFBekUsRUFBK0VrRCxJQUEvRSxDQUFxRkMsT0FBRCxJQUFhO0FBQzdGSCxNQUFBQSxhQUFhLENBQUNJLG1CQUFkLENBQWtDekIsaUNBQWdCQyxHQUFoQixFQUFsQyxFQUF5RHVCLE9BQXpEOztBQUNBRSxzQkFBT0Msa0JBQVAsQ0FBMEIsS0FBS3JCLEtBQUwsQ0FBV2pDLElBQXJDO0FBQ0gsS0FIRDtBQUlILEdBNUYyQjtBQThGNUJ1RCxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCRixvQkFBT0Msa0JBQVAsQ0FBMEIsS0FBS3JCLEtBQUwsQ0FBV2pDLElBQXJDOztBQUNBd0Qsd0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxNQUFBQSxNQUFNLEVBQUU7QUFBVCxLQUFiO0FBQ0gsR0FqRzJCO0FBbUc1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQk4sb0JBQU9PLGtCQUFQLENBQTBCLEtBQUszQixLQUFMLENBQVdqQyxJQUFyQzs7QUFDQXdELHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjtBQUNILEdBdEcyQjtBQXdHNUJHLEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUJiLElBQUFBLGFBQWEsQ0FBQ2Msa0NBQWQsQ0FBaURuQyxpQ0FBZ0JDLEdBQWhCLEVBQWpELEVBQXdFLEtBQUtLLEtBQUwsQ0FBV2pDLElBQW5GO0FBQ0gsR0ExRzJCO0FBNEc1QnFDLEVBQUFBLHVCQUF1QixFQUFFLFVBQVMwQixLQUFULEVBQWdCL0QsSUFBaEIsRUFBc0JnRSxVQUF0QixFQUFrQ0MsU0FBbEMsRUFBNkM7QUFDbEUsUUFBSWpFLElBQUksQ0FBQ2tFLE1BQUwsS0FBZ0IsS0FBS2pDLEtBQUwsQ0FBV2pDLElBQVgsQ0FBZ0JrRSxNQUFwQyxFQUE0QztBQUU1QyxTQUFLcEIsUUFBTCxDQUFjO0FBQ1ZkLE1BQUFBLGNBQWMsRUFBRWpDLGlCQUFpQixDQUFDLEtBQUtrQyxLQUFMLENBQVdqQyxJQUFaO0FBRHZCLEtBQWQ7QUFHSCxHQWxIMkI7QUFvSDVCO0FBQ0FzQyxFQUFBQSxVQUFVLEVBQUUsWUFBVztBQUNuQixRQUFJLEtBQUs2QixRQUFMLEVBQUosRUFBcUI7QUFDakIsVUFBSSxLQUFLbEMsS0FBTCxDQUFXVCxTQUFmLEVBQTBCLEtBQUtTLEtBQUwsQ0FBV1QsU0FBWDtBQUM3QixLQUZELE1BRU87QUFDSCxVQUFJLEtBQUtTLEtBQUwsQ0FBV1YsUUFBZixFQUF5QixLQUFLVSxLQUFMLENBQVdWLFFBQVg7QUFDNUI7QUFDSixHQTNIMkI7QUE2SDVCO0FBQ0E7QUFDQTtBQUNBNEMsRUFBQUEsUUFBUSxFQUFFLFlBQVc7QUFDakIsUUFBSSxLQUFLQywwQkFBTCxNQUNBLEtBQUtuQyxLQUFMLENBQVdsQixhQURYLElBRUEsS0FBS2tCLEtBQUwsQ0FBV3BCLHFCQUZmLEVBR0U7QUFDRSxhQUFPaEIsbUJBQVA7QUFDSCxLQUxELE1BS08sSUFBSSxLQUFLOEMsS0FBTCxDQUFXWCxjQUFYLENBQTBCcUMsTUFBMUIsR0FBbUMsQ0FBdkMsRUFBMEM7QUFDN0MsYUFBT3ZFLHlCQUFQO0FBQ0g7O0FBQ0QsV0FBT0YsaUJBQVA7QUFDSCxHQTFJMkI7QUE0STVCO0FBQ0EwRSxFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QixRQUFJLEtBQUtyQyxLQUFMLENBQVdsQixhQUFmLEVBQThCO0FBQzFCLFlBQU13RCxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBcEI7QUFDQSxhQUNJLDZCQUFDLFdBQUQ7QUFBYSxRQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLHNDQUFELENBQXpCO0FBQW1FLFFBQUEsS0FBSyxFQUFDLElBQXpFO0FBQThFLFFBQUEsTUFBTSxFQUFDO0FBQXJGLFFBREo7QUFHSDs7QUFFRCxRQUFJLEtBQUtOLDBCQUFMLEVBQUosRUFBdUM7QUFDbkMsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsV0FBTyxJQUFQO0FBQ0gsR0ExSjJCO0FBNEo1QkEsRUFBQUEsMEJBQTBCLEVBQUUsWUFBVztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQU1PLGVBQWUsR0FBR0MsT0FBTyxDQUMzQixLQUFLakMsS0FBTCxDQUFXYixhQUFYLElBQ0EsS0FBS2EsS0FBTCxDQUFXYixhQUFYLENBQXlCK0MsS0FEekIsSUFFQSxLQUFLbEMsS0FBTCxDQUFXYixhQUFYLENBQXlCK0MsS0FBekIsQ0FBK0JDLE9BQS9CLEtBQTJDLDJCQUhoQixDQUEvQjtBQUtBLFdBQU8sS0FBS25DLEtBQUwsQ0FBV2pCLFNBQVgsS0FBeUIsT0FBekIsSUFBb0MsQ0FBQ2lELGVBQTVDO0FBQ0gsR0F2SzJCO0FBeUs1QkksRUFBQUEsd0JBQXdCLEVBQUUsWUFBVztBQUNqQyxVQUFNL0MsY0FBYyxHQUFHLEtBQUtXLEtBQUwsQ0FBV1gsY0FBbEM7QUFDQSxRQUFJLENBQUNBLGNBQWMsQ0FBQ3FDLE1BQXBCLEVBQTRCLE9BQU8sSUFBUDtBQUU1QixRQUFJVyxLQUFKO0FBQ0EsUUFBSUMsT0FBSjtBQUVBLFVBQU1DLE1BQU0sR0FBR2xELGNBQWMsQ0FBQ21ELElBQWYsQ0FBcUJDLENBQUQsSUFBTztBQUN0QyxhQUFPQSxDQUFDLENBQUNQLEtBQUYsSUFBV08sQ0FBQyxDQUFDUCxLQUFGLENBQVFRLElBQVIsS0FBaUIsb0JBQW5DO0FBQ0gsS0FGYyxDQUFmOztBQUlBLFFBQUlILE1BQUosRUFBWTtBQUNSRixNQUFBQSxLQUFLLEdBQUcseUJBQUcsd0RBQUgsQ0FBUjtBQUNBQyxNQUFBQSxPQUFPLEdBQUcseUJBQ04sc0lBRE0sRUFFTixFQUZNLEVBR047QUFDSSw0QkFBcUJLLEdBQUQsSUFBUztBQUFHLFVBQUEsU0FBUyxFQUFDLDhCQUFiO0FBQTRDLFVBQUEsR0FBRyxFQUFDLFFBQWhEO0FBQXlELFVBQUEsT0FBTyxFQUFFLEtBQUt6QjtBQUF2RSxXQUE4RnlCLEdBQTlGLENBRGpDO0FBRUksMEJBQW1CQSxHQUFELElBQVM7QUFBRyxVQUFBLFNBQVMsRUFBQyw4QkFBYjtBQUE0QyxVQUFBLEdBQUcsRUFBQyxZQUFoRDtBQUE2RCxVQUFBLE9BQU8sRUFBRSxLQUFLdkM7QUFBM0UsV0FBMkd1QyxHQUEzRyxDQUYvQjtBQUdJLHNCQUFlQSxHQUFELElBQVM7QUFBRyxVQUFBLFNBQVMsRUFBQyw4QkFBYjtBQUE0QyxVQUFBLEdBQUcsRUFBQyxRQUFoRDtBQUF5RCxVQUFBLE9BQU8sRUFBRSxLQUFLM0I7QUFBdkUsV0FBNEYyQixHQUE1RjtBQUgzQixPQUhNLENBQVY7QUFTSCxLQVhELE1BV087QUFDSCxVQUFJQyxZQUFZLEdBQUcsSUFBbkI7QUFDQSxVQUFJQyxrQkFBa0IsR0FBRyxJQUF6Qjs7QUFDQSxXQUFLLE1BQU1KLENBQVgsSUFBZ0JwRCxjQUFoQixFQUFnQztBQUM1QixZQUFJb0QsQ0FBQyxDQUFDUCxLQUFGLElBQVdPLENBQUMsQ0FBQ1AsS0FBRixDQUFRQyxPQUFSLEtBQW9CLHFCQUFuQyxFQUEwRDtBQUN0RFMsVUFBQUEsWUFBWSxHQUFHSCxDQUFDLENBQUNQLEtBQWpCO0FBQ0E7QUFDSCxTQUhELE1BR08sSUFBSU8sQ0FBQyxDQUFDUCxLQUFGLElBQVdPLENBQUMsQ0FBQ1AsS0FBRixDQUFRQyxPQUFSLEtBQW9CLDJCQUFuQyxFQUFnRTtBQUNuRVUsVUFBQUEsa0JBQWtCLEdBQUdKLENBQUMsQ0FBQ1AsS0FBdkI7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsVUFBSVUsWUFBSixFQUFrQjtBQUNkUCxRQUFBQSxLQUFLLEdBQUcseUJBQ0osK0RBQ0Esc0RBRkksRUFHSixFQUhJLEVBSUo7QUFDSSx5QkFBZ0JNLEdBQUQsSUFDWDtBQUFHLFlBQUEsSUFBSSxFQUFFQyxZQUFZLENBQUMxQyxJQUFiLElBQXFCMEMsWUFBWSxDQUFDMUMsSUFBYixDQUFrQjRDLFdBQWhEO0FBQTZELFlBQUEsTUFBTSxFQUFDO0FBQXBFLGFBQ01ILEdBRE47QUFGUixTQUpJLENBQVI7QUFXSCxPQVpELE1BWU8sSUFBSUUsa0JBQUosRUFBd0I7QUFDM0JSLFFBQUFBLEtBQUssR0FBRyw4Q0FDSlEsa0JBQWtCLENBQUMzQyxJQUFuQixDQUF3QjZDLFVBRHBCLEVBRUpGLGtCQUFrQixDQUFDM0MsSUFBbkIsQ0FBd0I4QyxhQUZwQixFQUVtQztBQUN2QyxpQ0FBdUIsMEJBQ25CLDZGQUNBLGlGQUZtQixDQURnQjtBQUt2QyxjQUFJLDBCQUNBLHFGQUNBLGlGQUZBO0FBTG1DLFNBRm5DLENBQVI7QUFZSCxPQWJNLE1BYUEsSUFDSDNELGNBQWMsQ0FBQ3FDLE1BQWYsS0FBMEIsQ0FBMUIsSUFDQXJDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0I2QyxLQURsQixJQUVBN0MsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQjZDLEtBQWxCLENBQXdCaEMsSUFGeEIsSUFHQWIsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQjZDLEtBQWxCLENBQXdCaEMsSUFBeEIsQ0FBNkJnQyxLQUoxQixFQUtMO0FBQ0VHLFFBQUFBLEtBQUssR0FBRyxxQ0FBb0JoRCxjQUFjLENBQUMsQ0FBRCxDQUFkLENBQWtCNkMsS0FBbEIsQ0FBd0JoQyxJQUE1QyxLQUFxRGIsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQjZDLEtBQWxCLENBQXdCaEMsSUFBeEIsQ0FBNkJnQyxLQUExRjtBQUNILE9BUE0sTUFPQTtBQUNIRyxRQUFBQSxLQUFLLEdBQUcseUJBQUcsZ0RBQUgsRUFBcUQ7QUFBRVksVUFBQUEsS0FBSyxFQUFFNUQsY0FBYyxDQUFDcUM7QUFBeEIsU0FBckQsQ0FBUjtBQUNIOztBQUNEWSxNQUFBQSxPQUFPLEdBQUcseUJBQUcsK0ZBQ1YsOERBRE8sRUFFTjtBQUFFVyxRQUFBQSxLQUFLLEVBQUU1RCxjQUFjLENBQUNxQztBQUF4QixPQUZNLEVBR047QUFDSSxzQkFBZWlCLEdBQUQsSUFDVjtBQUFHLFVBQUEsU0FBUyxFQUFDLDhCQUFiO0FBQTRDLFVBQUEsR0FBRyxFQUFDLFFBQWhEO0FBQXlELFVBQUEsT0FBTyxFQUFFLEtBQUsvQjtBQUF2RSxXQUE0RitCLEdBQTVGLENBRlI7QUFHSSxzQkFBZUEsR0FBRCxJQUNWO0FBQUcsVUFBQSxTQUFTLEVBQUMsOEJBQWI7QUFBNEMsVUFBQSxHQUFHLEVBQUMsUUFBaEQ7QUFBeUQsVUFBQSxPQUFPLEVBQUUsS0FBSzNCO0FBQXZFLFdBQTRGMkIsR0FBNUY7QUFKUixPQUhNLENBQVY7QUFVSDs7QUFFRCxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNIO0FBQUssTUFBQSxHQUFHLEVBQUVaLE9BQU8sQ0FBQywwREFBRCxDQUFqQjtBQUErRSxNQUFBLEtBQUssRUFBQyxJQUFyRjtBQUEwRixNQUFBLE1BQU0sRUFBQyxJQUFqRztBQUFzRyxNQUFBLEtBQUssRUFBRSx5QkFBRyxTQUFILENBQTdHO0FBQTRILE1BQUEsR0FBRyxFQUFDO0FBQWhJLE1BREcsRUFFSCwwQ0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTU0sS0FETixDQURKLEVBSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ01DLE9BRE4sQ0FKSixDQUZHLENBQVA7QUFXSCxHQXJRMkI7QUF1UTVCO0FBQ0FZLEVBQUFBLFdBQVcsRUFBRSxZQUFXO0FBQ3BCLFFBQUksS0FBS3pCLDBCQUFMLEVBQUosRUFBdUM7QUFDbkMsYUFDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFLLFFBQUEsR0FBRyxFQUFFTSxPQUFPLENBQUMsMERBQUQsQ0FBakI7QUFBK0UsUUFBQSxLQUFLLEVBQUMsSUFBckY7QUFBMEYsUUFBQSxNQUFNLEVBQUMsSUFBakc7QUFBc0csUUFBQSxLQUFLLEVBQUMsT0FBNUc7QUFBbUgsUUFBQSxHQUFHLEVBQUM7QUFBdkgsUUFESixFQUVJLDBDQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNLHlCQUFHLDJDQUFILENBRE4sQ0FESixFQUlJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNLHlCQUFHLGtFQUFILENBRE4sQ0FKSixDQUZKLENBREo7QUFhSDs7QUFFRCxRQUFJLEtBQUsvQixLQUFMLENBQVdYLGNBQVgsQ0FBMEJxQyxNQUExQixHQUFtQyxDQUF2QyxFQUEwQztBQUN0QyxhQUFPLEtBQUtVLHdCQUFMLEVBQVA7QUFDSDs7QUFFRCxRQUFJLEtBQUs5QyxLQUFMLENBQVdsQixhQUFmLEVBQThCO0FBQzFCLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksd0NBQUsseUJBQUcsYUFBSCxDQUFMLENBREosQ0FESjtBQUtILEtBM0JtQixDQTZCcEI7OztBQUNBLFFBQUksS0FBS2tCLEtBQUwsQ0FBV3BCLHFCQUFYLElBQW9DLENBQUMsS0FBS29CLEtBQUwsQ0FBV2pCLFNBQXBELEVBQStEO0FBQzNELGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ00seUJBQUcsd0ZBQ0csZ0VBRE4sRUFFRSxFQUZGLEVBR0U7QUFDSSxzQkFBZXNFLEdBQUQsSUFDVjtBQUFHLFVBQUEsU0FBUyxFQUFDLDhCQUFiO0FBQTRDLFVBQUEsR0FBRyxFQUFDLFFBQWhEO0FBQXlELFVBQUEsT0FBTyxFQUFFLEtBQUtyRCxLQUFMLENBQVdiO0FBQTdFLFdBQThGa0UsR0FBOUYsQ0FGUjtBQUdJLHNCQUFlQSxHQUFELElBQ1Y7QUFBRyxVQUFBLFNBQVMsRUFBQyw4QkFBYjtBQUE0QyxVQUFBLEdBQUcsRUFBQyxRQUFoRDtBQUF5RCxVQUFBLE9BQU8sRUFBRSxLQUFLckQsS0FBTCxDQUFXWjtBQUE3RSxXQUFtR2lFLEdBQW5HO0FBSlIsT0FIRixDQUROLENBREo7QUFjSDs7QUFFRCxXQUFPLElBQVA7QUFDSCxHQXhUMkI7QUEwVDVCUSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1iLE9BQU8sR0FBRyxLQUFLWSxXQUFMLEVBQWhCOztBQUNBLFVBQU1FLFNBQVMsR0FBRyxLQUFLekIsYUFBTCxFQUFsQjs7QUFFQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNeUIsU0FETixDQURKLEVBSUk7QUFBSyxNQUFBLElBQUksRUFBQztBQUFWLE9BQ01kLE9BRE4sQ0FKSixDQURKO0FBVUg7QUF4VTJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBNYXRyaXggZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcbmltcG9ydCB7IF90LCBfdGQgfSBmcm9tICcuLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IFJlc2VuZCBmcm9tICcuLi8uLi9SZXNlbmQnO1xyXG5pbXBvcnQgKiBhcyBjcnlwdG9kZXZpY2VzIGZyb20gJy4uLy4uL2NyeXB0b2RldmljZXMnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQge21lc3NhZ2VGb3JSZXNvdXJjZUxpbWl0RXJyb3IsIG1lc3NhZ2VGb3JTZW5kRXJyb3J9IGZyb20gJy4uLy4uL3V0aWxzL0Vycm9yVXRpbHMnO1xyXG5cclxuY29uc3QgU1RBVFVTX0JBUl9ISURERU4gPSAwO1xyXG5jb25zdCBTVEFUVVNfQkFSX0VYUEFOREVEID0gMTtcclxuY29uc3QgU1RBVFVTX0JBUl9FWFBBTkRFRF9MQVJHRSA9IDI7XHJcblxyXG5mdW5jdGlvbiBnZXRVbnNlbnRNZXNzYWdlcyhyb29tKSB7XHJcbiAgICBpZiAoIXJvb20pIHsgcmV0dXJuIFtdOyB9XHJcbiAgICByZXR1cm4gcm9vbS5nZXRQZW5kaW5nRXZlbnRzKCkuZmlsdGVyKGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgcmV0dXJuIGV2LnN0YXR1cyA9PT0gTWF0cml4LkV2ZW50U3RhdHVzLk5PVF9TRU5UO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tU3RhdHVzQmFyJyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICAvLyB0aGUgcm9vbSB0aGlzIHN0YXR1c2JhciBpcyByZXByZXNlbnRpbmcuXHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIC8vIFRoaXMgaXMgdHJ1ZSB3aGVuIHRoZSB1c2VyIGlzIGFsb25lIGluIHRoZSByb29tLCBidXQgaGFzIGFsc28gc2VudCBhIG1lc3NhZ2UuXHJcbiAgICAgICAgLy8gVXNlZCB0byBzdWdnZXN0IHRvIHRoZSB1c2VyIHRvIGludml0ZSBzb21lb25lXHJcbiAgICAgICAgc2VudE1lc3NhZ2VBbmRJc0Fsb25lOiBQcm9wVHlwZXMuYm9vbCxcclxuXHJcbiAgICAgICAgLy8gdHJ1ZSBpZiB0aGVyZSBpcyBhbiBhY3RpdmUgY2FsbCBpbiB0aGlzIHJvb20gKG1lYW5zIHdlIHNob3dcclxuICAgICAgICAvLyB0aGUgJ0FjdGl2ZSBDYWxsJyB0ZXh0IGluIHRoZSBzdGF0dXMgYmFyIGlmIHRoZXJlIGlzIG5vdGhpbmdcclxuICAgICAgICAvLyBtb3JlIGludGVyZXN0aW5nKVxyXG4gICAgICAgIGhhc0FjdGl2ZUNhbGw6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyB0cnVlIGlmIHRoZSByb29tIGlzIGJlaW5nIHBlZWtlZCBhdC4gVGhpcyBhZmZlY3RzIGNvbXBvbmVudHMgdGhhdCBzaG91bGRuJ3RcclxuICAgICAgICAvLyBsb2dpY2FsbHkgYmUgc2hvd24gd2hlbiBwZWVraW5nLCBzdWNoIGFzIGEgcHJvbXB0IHRvIGludml0ZSBwZW9wbGUgdG8gYSByb29tLlxyXG4gICAgICAgIGlzUGVla2luZzogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIGNhbGxiYWNrIGZvciB3aGVuIHRoZSB1c2VyIGNsaWNrcyBvbiB0aGUgJ3Jlc2VuZCBhbGwnIGJ1dHRvbiBpbiB0aGVcclxuICAgICAgICAvLyAndW5zZW50IG1lc3NhZ2VzJyBiYXJcclxuICAgICAgICBvblJlc2VuZEFsbENsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2sgZm9yIHdoZW4gdGhlIHVzZXIgY2xpY2tzIG9uIHRoZSAnY2FuY2VsIGFsbCcgYnV0dG9uIGluIHRoZVxyXG4gICAgICAgIC8vICd1bnNlbnQgbWVzc2FnZXMnIGJhclxyXG4gICAgICAgIG9uQ2FuY2VsQWxsQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBjYWxsYmFjayBmb3Igd2hlbiB0aGUgdXNlciBjbGlja3Mgb24gdGhlICdpbnZpdGUgb3RoZXJzJyBidXR0b24gaW4gdGhlXHJcbiAgICAgICAgLy8gJ3lvdSBhcmUgYWxvbmUnIGJhclxyXG4gICAgICAgIG9uSW52aXRlQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBjYWxsYmFjayBmb3Igd2hlbiB0aGUgdXNlciBjbGlja3Mgb24gdGhlICdzdG9wIHdhcm5pbmcgbWUnIGJ1dHRvbiBpbiB0aGVcclxuICAgICAgICAvLyAneW91IGFyZSBhbG9uZScgYmFyXHJcbiAgICAgICAgb25TdG9wV2FybmluZ0NsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2sgZm9yIHdoZW4gd2UgZG8gc29tZXRoaW5nIHRoYXQgY2hhbmdlcyB0aGUgc2l6ZSBvZiB0aGVcclxuICAgICAgICAvLyBzdGF0dXMgYmFyLiBUaGlzIGlzIHVzZWQgdG8gdHJpZ2dlciBhIHJlLWxheW91dCBpbiB0aGUgcGFyZW50XHJcbiAgICAgICAgLy8gY29tcG9uZW50LlxyXG4gICAgICAgIG9uUmVzaXplOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2sgZm9yIHdoZW4gdGhlIHN0YXR1cyBiYXIgY2FuIGJlIGhpZGRlbiBmcm9tIHZpZXcsIGFzIGl0IGlzXHJcbiAgICAgICAgLy8gbm90IGRpc3BsYXlpbmcgYW55dGhpbmdcclxuICAgICAgICBvbkhpZGRlbjogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIGNhbGxiYWNrIGZvciB3aGVuIHRoZSBzdGF0dXMgYmFyIGlzIGRpc3BsYXlpbmcgc29tZXRoaW5nIGFuZCBzaG91bGRcclxuICAgICAgICAvLyBiZSB2aXNpYmxlXHJcbiAgICAgICAgb25WaXNpYmxlOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBzeW5jU3RhdGU6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRTeW5jU3RhdGUoKSxcclxuICAgICAgICAgICAgc3luY1N0YXRlRGF0YTogTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFN5bmNTdGF0ZURhdGEoKSxcclxuICAgICAgICAgICAgdW5zZW50TWVzc2FnZXM6IGdldFVuc2VudE1lc3NhZ2VzKHRoaXMucHJvcHMucm9vbSksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbihcInN5bmNcIiwgdGhpcy5vblN5bmNTdGF0ZUNoYW5nZSk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS5sb2NhbEVjaG9VcGRhdGVkXCIsIHRoaXMuX29uUm9vbUxvY2FsRWNob1VwZGF0ZWQpO1xyXG5cclxuICAgICAgICB0aGlzLl9jaGVja1NpemUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9jaGVja1NpemUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHdlIG1heSBoYXZlIGVudGlyZWx5IGxvc3Qgb3VyIGNsaWVudCBhcyB3ZSdyZSBsb2dnaW5nIG91dCBiZWZvcmUgY2xpY2tpbmcgbG9naW4gb24gdGhlIGd1ZXN0IGJhci4uLlxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoY2xpZW50KSB7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcInN5bmNcIiwgdGhpcy5vblN5bmNTdGF0ZUNoYW5nZSk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb20ubG9jYWxFY2hvVXBkYXRlZFwiLCB0aGlzLl9vblJvb21Mb2NhbEVjaG9VcGRhdGVkKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU3luY1N0YXRlQ2hhbmdlOiBmdW5jdGlvbihzdGF0ZSwgcHJldlN0YXRlLCBkYXRhKSB7XHJcbiAgICAgICAgaWYgKHN0YXRlID09PSBcIlNZTkNJTkdcIiAmJiBwcmV2U3RhdGUgPT09IFwiU1lOQ0lOR1wiKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHN5bmNTdGF0ZTogc3RhdGUsXHJcbiAgICAgICAgICAgIHN5bmNTdGF0ZURhdGE6IGRhdGEsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblNlbmRXaXRob3V0VmVyaWZ5aW5nQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNyeXB0b2RldmljZXMuZ2V0VW5rbm93bkRldmljZXNGb3JSb29tKE1hdHJpeENsaWVudFBlZy5nZXQoKSwgdGhpcy5wcm9wcy5yb29tKS50aGVuKChkZXZpY2VzKSA9PiB7XHJcbiAgICAgICAgICAgIGNyeXB0b2RldmljZXMubWFya0FsbERldmljZXNLbm93bihNYXRyaXhDbGllbnRQZWcuZ2V0KCksIGRldmljZXMpO1xyXG4gICAgICAgICAgICBSZXNlbmQucmVzZW5kVW5zZW50RXZlbnRzKHRoaXMucHJvcHMucm9vbSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJlc2VuZEFsbENsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBSZXNlbmQucmVzZW5kVW5zZW50RXZlbnRzKHRoaXMucHJvcHMucm9vbSk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdmb2N1c19jb21wb3Nlcid9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uQ2FuY2VsQWxsQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIFJlc2VuZC5jYW5jZWxVbnNlbnRFdmVudHModGhpcy5wcm9wcy5yb29tKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2ZvY3VzX2NvbXBvc2VyJ30pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25TaG93RGV2aWNlc0NsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjcnlwdG9kZXZpY2VzLnNob3dVbmtub3duRGV2aWNlRGlhbG9nRm9yTWVzc2FnZXMoTWF0cml4Q2xpZW50UGVnLmdldCgpLCB0aGlzLnByb3BzLnJvb20pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25Sb29tTG9jYWxFY2hvVXBkYXRlZDogZnVuY3Rpb24oZXZlbnQsIHJvb20sIG9sZEV2ZW50SWQsIG9sZFN0YXR1cykge1xyXG4gICAgICAgIGlmIChyb29tLnJvb21JZCAhPT0gdGhpcy5wcm9wcy5yb29tLnJvb21JZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdW5zZW50TWVzc2FnZXM6IGdldFVuc2VudE1lc3NhZ2VzKHRoaXMucHJvcHMucm9vbSksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIENoZWNrIHdoZXRoZXIgY3VycmVudCBzaXplIGlzIGdyZWF0ZXIgdGhhbiAwLCBpZiB5ZXMgY2FsbCBwcm9wcy5vblZpc2libGVcclxuICAgIF9jaGVja1NpemU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9nZXRTaXplKCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMub25WaXNpYmxlKSB0aGlzLnByb3BzLm9uVmlzaWJsZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uSGlkZGVuKSB0aGlzLnByb3BzLm9uSGlkZGVuKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBXZSBkb24ndCBuZWVkIHRoZSBhY3R1YWwgaGVpZ2h0IC0ganVzdCB3aGV0aGVyIGl0IGlzIGxpa2VseSB0byBoYXZlXHJcbiAgICAvLyBjaGFuZ2VkIC0gc28gd2UgdXNlICcwJyB0byBpbmRpY2F0ZSBub3JtYWwgc2l6ZSwgYW5kIG90aGVyIHZhbHVlcyB0b1xyXG4gICAgLy8gaW5kaWNhdGUgb3RoZXIgc2l6ZXMuXHJcbiAgICBfZ2V0U2l6ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Nob3VsZFNob3dDb25uZWN0aW9uRXJyb3IoKSB8fFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLmhhc0FjdGl2ZUNhbGwgfHxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5zZW50TWVzc2FnZUFuZElzQWxvbmVcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgcmV0dXJuIFNUQVRVU19CQVJfRVhQQU5ERUQ7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnVuc2VudE1lc3NhZ2VzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIFNUQVRVU19CQVJfRVhQQU5ERURfTEFSR0U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBTVEFUVVNfQkFSX0hJRERFTjtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gcmV0dXJuIHN1aXRhYmxlIGNvbnRlbnQgZm9yIHRoZSBpbWFnZSBvbiB0aGUgbGVmdCBvZiB0aGUgc3RhdHVzIGJhci5cclxuICAgIF9nZXRJbmRpY2F0b3I6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmhhc0FjdGl2ZUNhbGwpIHtcclxuICAgICAgICAgICAgY29uc3QgVGludGFibGVTdmcgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVGludGFibGVTdmdcIik7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8VGludGFibGVTdmcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9zb3VuZC1pbmRpY2F0b3Iuc3ZnXCIpfSB3aWR0aD1cIjIzXCIgaGVpZ2h0PVwiMjBcIiAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX3Nob3VsZFNob3dDb25uZWN0aW9uRXJyb3IoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvdWxkU2hvd0Nvbm5lY3Rpb25FcnJvcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gbm8gY29ubiBiYXIgdHJ1bXBzIHRoZSBcInNvbWUgbm90IHNlbnRcIiBtc2cgc2luY2UgeW91IGNhbid0IHJlc2VuZCB3aXRob3V0XHJcbiAgICAgICAgLy8gYSBjb25uZWN0aW9uIVxyXG4gICAgICAgIC8vIFRoZXJlJ3Mgb25lIHNpdHVhdGlvbiBpbiB3aGljaCB3ZSBkb24ndCBzaG93IHRoaXMgJ25vIGNvbm5lY3Rpb24nIGJhciwgYW5kIHRoYXQnc1xyXG4gICAgICAgIC8vIGlmIGl0J3MgYSByZXNvdXJjZSBsaW1pdCBleGNlZWRlZCBlcnJvcjogdGhvc2UgYXJlIHNob3duIGluIHRoZSB0b3AgYmFyLlxyXG4gICAgICAgIGNvbnN0IGVycm9ySXNNYXVFcnJvciA9IEJvb2xlYW4oXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuc3luY1N0YXRlRGF0YSAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnN5bmNTdGF0ZURhdGEuZXJyb3IgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5zeW5jU3RhdGVEYXRhLmVycm9yLmVycmNvZGUgPT09ICdNX1JFU09VUkNFX0xJTUlUX0VYQ0VFREVEJyxcclxuICAgICAgICApO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN0YXRlLnN5bmNTdGF0ZSA9PT0gXCJFUlJPUlwiICYmICFlcnJvcklzTWF1RXJyb3I7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRVbnNlbnRNZXNzYWdlQ29udGVudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgdW5zZW50TWVzc2FnZXMgPSB0aGlzLnN0YXRlLnVuc2VudE1lc3NhZ2VzO1xyXG4gICAgICAgIGlmICghdW5zZW50TWVzc2FnZXMubGVuZ3RoKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgbGV0IHRpdGxlO1xyXG4gICAgICAgIGxldCBjb250ZW50O1xyXG5cclxuICAgICAgICBjb25zdCBoYXNVREUgPSB1bnNlbnRNZXNzYWdlcy5zb21lKChtKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBtLmVycm9yICYmIG0uZXJyb3IubmFtZSA9PT0gXCJVbmtub3duRGV2aWNlRXJyb3JcIjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGhhc1VERSkge1xyXG4gICAgICAgICAgICB0aXRsZSA9IF90KFwiTWVzc2FnZSBub3Qgc2VudCBkdWUgdG8gdW5rbm93biBzZXNzaW9ucyBiZWluZyBwcmVzZW50XCIpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIjxzaG93U2Vzc2lvbnNUZXh0PlNob3cgc2Vzc2lvbnM8L3Nob3dTZXNzaW9uc1RleHQ+LCA8c2VuZEFueXdheVRleHQ+c2VuZCBhbnl3YXk8L3NlbmRBbnl3YXlUZXh0PiBvciA8Y2FuY2VsVGV4dD5jYW5jZWw8L2NhbmNlbFRleHQ+LlwiLFxyXG4gICAgICAgICAgICAgICAge30sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ3Nob3dTZXNzaW9uc1RleHQnOiAoc3ViKSA9PiA8YSBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX3Jlc2VuZF9saW5rXCIga2V5PVwicmVzZW5kXCIgb25DbGljaz17dGhpcy5fb25TaG93RGV2aWNlc0NsaWNrfT57IHN1YiB9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICAnc2VuZEFueXdheVRleHQnOiAoc3ViKSA9PiA8YSBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX3Jlc2VuZF9saW5rXCIga2V5PVwic2VuZEFueXdheVwiIG9uQ2xpY2s9e3RoaXMuX29uU2VuZFdpdGhvdXRWZXJpZnlpbmdDbGlja30+eyBzdWIgfTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgJ2NhbmNlbFRleHQnOiAoc3ViKSA9PiA8YSBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX3Jlc2VuZF9saW5rXCIga2V5PVwiY2FuY2VsXCIgb25DbGljaz17dGhpcy5fb25DYW5jZWxBbGxDbGlja30+eyBzdWIgfTwvYT4sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCBjb25zZW50RXJyb3IgPSBudWxsO1xyXG4gICAgICAgICAgICBsZXQgcmVzb3VyY2VMaW1pdEVycm9yID0gbnVsbDtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBtIG9mIHVuc2VudE1lc3NhZ2VzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobS5lcnJvciAmJiBtLmVycm9yLmVycmNvZGUgPT09ICdNX0NPTlNFTlRfTk9UX0dJVkVOJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNlbnRFcnJvciA9IG0uZXJyb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG0uZXJyb3IgJiYgbS5lcnJvci5lcnJjb2RlID09PSAnTV9SRVNPVVJDRV9MSU1JVF9FWENFRURFRCcpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvdXJjZUxpbWl0RXJyb3IgPSBtLmVycm9yO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChjb25zZW50RXJyb3IpIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJZb3UgY2FuJ3Qgc2VuZCBhbnkgbWVzc2FnZXMgdW50aWwgeW91IHJldmlldyBhbmQgYWdyZWUgdG8gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGNvbnNlbnRMaW5rPm91ciB0ZXJtcyBhbmQgY29uZGl0aW9uczwvY29uc2VudExpbms+LlwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ2NvbnNlbnRMaW5rJzogKHN1YikgPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9e2NvbnNlbnRFcnJvci5kYXRhICYmIGNvbnNlbnRFcnJvci5kYXRhLmNvbnNlbnRfdXJpfSB0YXJnZXQ9XCJfYmxhbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHN1YiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlc291cmNlTGltaXRFcnJvcikge1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBtZXNzYWdlRm9yUmVzb3VyY2VMaW1pdEVycm9yKFxyXG4gICAgICAgICAgICAgICAgICAgIHJlc291cmNlTGltaXRFcnJvci5kYXRhLmxpbWl0X3R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VMaW1pdEVycm9yLmRhdGEuYWRtaW5fY29udGFjdCwge1xyXG4gICAgICAgICAgICAgICAgICAgICdtb250aGx5X2FjdGl2ZV91c2VyJzogX3RkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIllvdXIgbWVzc2FnZSB3YXNuJ3Qgc2VudCBiZWNhdXNlIHRoaXMgaG9tZXNlcnZlciBoYXMgaGl0IGl0cyBNb250aGx5IEFjdGl2ZSBVc2VyIExpbWl0LiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiUGxlYXNlIDxhPmNvbnRhY3QgeW91ciBzZXJ2aWNlIGFkbWluaXN0cmF0b3I8L2E+IHRvIGNvbnRpbnVlIHVzaW5nIHRoZSBzZXJ2aWNlLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICAgICAgJyc6IF90ZChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJZb3VyIG1lc3NhZ2Ugd2Fzbid0IHNlbnQgYmVjYXVzZSB0aGlzIGhvbWVzZXJ2ZXIgaGFzIGV4Y2VlZGVkIGEgcmVzb3VyY2UgbGltaXQuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJQbGVhc2UgPGE+Y29udGFjdCB5b3VyIHNlcnZpY2UgYWRtaW5pc3RyYXRvcjwvYT4gdG8gY29udGludWUgdXNpbmcgdGhlIHNlcnZpY2UuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgICAgICAgICAgdW5zZW50TWVzc2FnZXMubGVuZ3RoID09PSAxICYmXHJcbiAgICAgICAgICAgICAgICB1bnNlbnRNZXNzYWdlc1swXS5lcnJvciAmJlxyXG4gICAgICAgICAgICAgICAgdW5zZW50TWVzc2FnZXNbMF0uZXJyb3IuZGF0YSAmJlxyXG4gICAgICAgICAgICAgICAgdW5zZW50TWVzc2FnZXNbMF0uZXJyb3IuZGF0YS5lcnJvclxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gbWVzc2FnZUZvclNlbmRFcnJvcih1bnNlbnRNZXNzYWdlc1swXS5lcnJvci5kYXRhKSB8fCB1bnNlbnRNZXNzYWdlc1swXS5lcnJvci5kYXRhLmVycm9yO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBfdCgnJShjb3VudClzIG9mIHlvdXIgbWVzc2FnZXMgaGF2ZSBub3QgYmVlbiBzZW50LicsIHsgY291bnQ6IHVuc2VudE1lc3NhZ2VzLmxlbmd0aCB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb250ZW50ID0gX3QoXCIlKGNvdW50KXMgPHJlc2VuZFRleHQ+UmVzZW5kIGFsbDwvcmVzZW5kVGV4dD4gb3IgPGNhbmNlbFRleHQ+Y2FuY2VsIGFsbDwvY2FuY2VsVGV4dD4gbm93LiBcIiArXHJcbiAgICAgICAgICAgICAgIFwiWW91IGNhbiBhbHNvIHNlbGVjdCBpbmRpdmlkdWFsIG1lc3NhZ2VzIHRvIHJlc2VuZCBvciBjYW5jZWwuXCIsXHJcbiAgICAgICAgICAgICAgICB7IGNvdW50OiB1bnNlbnRNZXNzYWdlcy5sZW5ndGggfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAncmVzZW5kVGV4dCc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm14X1Jvb21TdGF0dXNCYXJfcmVzZW5kX2xpbmtcIiBrZXk9XCJyZXNlbmRcIiBvbkNsaWNrPXt0aGlzLl9vblJlc2VuZEFsbENsaWNrfT57IHN1YiB9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICAnY2FuY2VsVGV4dCc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm14X1Jvb21TdGF0dXNCYXJfcmVzZW5kX2xpbmtcIiBrZXk9XCJjYW5jZWxcIiBvbkNsaWNrPXt0aGlzLl9vbkNhbmNlbEFsbENsaWNrfT57IHN1YiB9PC9hPixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX2Nvbm5lY3Rpb25Mb3N0QmFyXCI+XHJcbiAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9mZWF0aGVyLWN1c3RvbWlzZWQvd2FybmluZy10cmlhbmdsZS5zdmdcIil9IHdpZHRoPVwiMjRcIiBoZWlnaHQ9XCIyNFwiIHRpdGxlPXtfdChcIldhcm5pbmdcIil9IGFsdD1cIlwiIC8+XHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21TdGF0dXNCYXJfY29ubmVjdGlvbkxvc3RCYXJfdGl0bGVcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHRpdGxlIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX2Nvbm5lY3Rpb25Mb3N0QmFyX2Rlc2NcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGNvbnRlbnQgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gcmV0dXJuIHN1aXRhYmxlIGNvbnRlbnQgZm9yIHRoZSBtYWluICh0ZXh0KSBwYXJ0IG9mIHRoZSBzdGF0dXMgYmFyLlxyXG4gICAgX2dldENvbnRlbnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zaG91bGRTaG93Q29ubmVjdGlvbkVycm9yKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVN0YXR1c0Jhcl9jb25uZWN0aW9uTG9zdEJhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9mZWF0aGVyLWN1c3RvbWlzZWQvd2FybmluZy10cmlhbmdsZS5zdmdcIil9IHdpZHRoPVwiMjRcIiBoZWlnaHQ9XCIyNFwiIHRpdGxlPVwiLyFcXCBcIiBhbHQ9XCIvIVxcIFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX2Nvbm5lY3Rpb25Mb3N0QmFyX3RpdGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdDb25uZWN0aXZpdHkgdG8gdGhlIHNlcnZlciBoYXMgYmVlbiBsb3N0LicpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVN0YXR1c0Jhcl9jb25uZWN0aW9uTG9zdEJhcl9kZXNjXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdTZW50IG1lc3NhZ2VzIHdpbGwgYmUgc3RvcmVkIHVudGlsIHlvdXIgY29ubmVjdGlvbiBoYXMgcmV0dXJuZWQuJykgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudW5zZW50TWVzc2FnZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fZ2V0VW5zZW50TWVzc2FnZUNvbnRlbnQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmhhc0FjdGl2ZUNhbGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVN0YXR1c0Jhcl9jYWxsQmFyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGI+eyBfdCgnQWN0aXZlIGNhbGwnKSB9PC9iPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZiB5b3UncmUgYWxvbmUgaW4gdGhlIHJvb20sIGFuZCBoYXZlIHNlbnQgYSBtZXNzYWdlLCBzdWdnZXN0IHRvIGludml0ZSBzb21lb25lXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2VudE1lc3NhZ2VBbmRJc0Fsb25lICYmICF0aGlzLnByb3BzLmlzUGVla2luZykge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyX2lzQWxvbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KFwiVGhlcmUncyBubyBvbmUgZWxzZSBoZXJlISBXb3VsZCB5b3UgbGlrZSB0byA8aW52aXRlVGV4dD5pbnZpdGUgb3RoZXJzPC9pbnZpdGVUZXh0PiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm9yIDxub3dhcm5UZXh0PnN0b3Agd2FybmluZyBhYm91dCB0aGUgZW1wdHkgcm9vbTwvbm93YXJuVGV4dD4/XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaW52aXRlVGV4dCc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibXhfUm9vbVN0YXR1c0Jhcl9yZXNlbmRfbGlua1wiIGtleT1cImludml0ZVwiIG9uQ2xpY2s9e3RoaXMucHJvcHMub25JbnZpdGVDbGlja30+eyBzdWIgfTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnbm93YXJuVGV4dCc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibXhfUm9vbVN0YXR1c0Jhcl9yZXNlbmRfbGlua1wiIGtleT1cIm5vd2FyblwiIG9uQ2xpY2s9e3RoaXMucHJvcHMub25TdG9wV2FybmluZ0NsaWNrfT57IHN1YiB9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IHRoaXMuX2dldENvbnRlbnQoKTtcclxuICAgICAgICBjb25zdCBpbmRpY2F0b3IgPSB0aGlzLl9nZXRJbmRpY2F0b3IoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tU3RhdHVzQmFyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21TdGF0dXNCYXJfaW5kaWNhdG9yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBpbmRpY2F0b3IgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgY29udGVudCB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=