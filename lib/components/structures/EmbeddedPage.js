/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _browserRequest = _interopRequireDefault(require("browser-request"));

var _languageHandler = require("../../languageHandler");

var _sanitizeHtml = _interopRequireDefault(require("sanitize-html"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _classnames = _interopRequireDefault(require("classnames"));

var _MatrixClientContext = _interopRequireDefault(require("../../contexts/MatrixClientContext"));

var _AutoHideScrollbar = _interopRequireDefault(require("./AutoHideScrollbar"));

class EmbeddedPage extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onAction", payload => {
      // HACK: Workaround for the context's MatrixClient not being set up at render time.
      if (payload.action === 'client_started') {
        this.forceUpdate();
      }
    });
    this._dispatcherRef = null;
    this.state = {
      page: ''
    };
  }

  translate(s) {
    // default implementation - skins may wish to extend this
    return (0, _sanitizeHtml.default)((0, _languageHandler._t)(s));
  }

  componentDidMount() {
    this._unmounted = false;

    if (!this.props.url) {
      return;
    } // we use request() to inline the page into the react component
    // so that it can inherit CSS and theming easily rather than mess around
    // with iframes and trying to synchronise document.stylesheets.


    (0, _browserRequest.default)({
      method: "GET",
      url: this.props.url
    }, (err, response, body) => {
      if (this._unmounted) {
        return;
      }

      if (err || response.status < 200 || response.status >= 300) {
        console.warn("Error loading page: ".concat(err));
        this.setState({
          page: (0, _languageHandler._t)("Couldn't load page")
        });
        return;
      }

      body = body.replace(/_t\(['"]([\s\S]*?)['"]\)/mg, (match, g1) => this.translate(g1));

      if (this.props.replaceMap) {
        Object.keys(this.props.replaceMap).forEach(key => {
          body = body.split(key).join(this.props.replaceMap[key]);
        });
      }

      this.setState({
        page: body
      });
    });
    this._dispatcherRef = _dispatcher.default.register(this.onAction);
  }

  componentWillUnmount() {
    this._unmounted = true;
    if (this._dispatcherRef !== null) _dispatcher.default.unregister(this._dispatcherRef);
  }

  render() {
    // HACK: Workaround for the context's MatrixClient not updating.
    const client = this.context || _MatrixClientPeg.MatrixClientPeg.get();

    const isGuest = client ? client.isGuest() : true;
    const className = this.props.className;
    const classes = (0, _classnames.default)({
      [className]: true,
      ["".concat(className, "_guest")]: isGuest,
      ["".concat(className, "_loggedIn")]: !!client
    });

    const content = _react.default.createElement("div", {
      className: "".concat(className, "_body"),
      dangerouslySetInnerHTML: {
        __html: this.state.page
      }
    });

    if (this.props.scrollbar) {
      return _react.default.createElement(_AutoHideScrollbar.default, {
        className: classes
      }, content);
    } else {
      return _react.default.createElement("div", {
        className: classes
      }, content);
    }
  }

}

exports.default = EmbeddedPage;
(0, _defineProperty2.default)(EmbeddedPage, "propTypes", {
  // URL to request embedded page content from
  url: _propTypes.default.string,
  // Class name prefix to apply for a given instance
  className: _propTypes.default.string,
  // Whether to wrap the page in a scrollbar
  scrollbar: _propTypes.default.bool,
  // Map of keys to replace with values, e.g {$placeholder: "value"}
  replaceMap: _propTypes.default.object
});
(0, _defineProperty2.default)(EmbeddedPage, "contextType", _MatrixClientContext.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvRW1iZWRkZWRQYWdlLmpzIl0sIm5hbWVzIjpbIkVtYmVkZGVkUGFnZSIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJwYXlsb2FkIiwiYWN0aW9uIiwiZm9yY2VVcGRhdGUiLCJfZGlzcGF0Y2hlclJlZiIsInN0YXRlIiwicGFnZSIsInRyYW5zbGF0ZSIsInMiLCJjb21wb25lbnREaWRNb3VudCIsIl91bm1vdW50ZWQiLCJ1cmwiLCJtZXRob2QiLCJlcnIiLCJyZXNwb25zZSIsImJvZHkiLCJzdGF0dXMiLCJjb25zb2xlIiwid2FybiIsInNldFN0YXRlIiwicmVwbGFjZSIsIm1hdGNoIiwiZzEiLCJyZXBsYWNlTWFwIiwiT2JqZWN0Iiwia2V5cyIsImZvckVhY2giLCJrZXkiLCJzcGxpdCIsImpvaW4iLCJkaXMiLCJyZWdpc3RlciIsIm9uQWN0aW9uIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bnJlZ2lzdGVyIiwicmVuZGVyIiwiY2xpZW50IiwiY29udGV4dCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImlzR3Vlc3QiLCJjbGFzc05hbWUiLCJjbGFzc2VzIiwiY29udGVudCIsIl9faHRtbCIsInNjcm9sbGJhciIsIlByb3BUeXBlcyIsInN0cmluZyIsImJvb2wiLCJvYmplY3QiLCJNYXRyaXhDbGllbnRDb250ZXh0Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7Ozs7Ozs7Ozs7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRWUsTUFBTUEsWUFBTixTQUEyQkMsZUFBTUMsYUFBakMsQ0FBK0M7QUFjMURDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLG9EQTJEUEMsT0FBRCxJQUFhO0FBQ3BCO0FBQ0EsVUFBSUEsT0FBTyxDQUFDQyxNQUFSLEtBQW1CLGdCQUF2QixFQUF5QztBQUNyQyxhQUFLQyxXQUFMO0FBQ0g7QUFDSixLQWhFa0I7QUFHZixTQUFLQyxjQUFMLEdBQXNCLElBQXRCO0FBRUEsU0FBS0MsS0FBTCxHQUFhO0FBQ1RDLE1BQUFBLElBQUksRUFBRTtBQURHLEtBQWI7QUFHSDs7QUFFREMsRUFBQUEsU0FBUyxDQUFDQyxDQUFELEVBQUk7QUFDVDtBQUNBLFdBQU8sMkJBQWEseUJBQUdBLENBQUgsQ0FBYixDQUFQO0FBQ0g7O0FBRURDLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFNBQUtDLFVBQUwsR0FBa0IsS0FBbEI7O0FBRUEsUUFBSSxDQUFDLEtBQUtWLEtBQUwsQ0FBV1csR0FBaEIsRUFBcUI7QUFDakI7QUFDSCxLQUxlLENBT2hCO0FBQ0E7QUFDQTs7O0FBRUEsaUNBQ0k7QUFBRUMsTUFBQUEsTUFBTSxFQUFFLEtBQVY7QUFBaUJELE1BQUFBLEdBQUcsRUFBRSxLQUFLWCxLQUFMLENBQVdXO0FBQWpDLEtBREosRUFFSSxDQUFDRSxHQUFELEVBQU1DLFFBQU4sRUFBZ0JDLElBQWhCLEtBQXlCO0FBQ3JCLFVBQUksS0FBS0wsVUFBVCxFQUFxQjtBQUNqQjtBQUNIOztBQUVELFVBQUlHLEdBQUcsSUFBSUMsUUFBUSxDQUFDRSxNQUFULEdBQWtCLEdBQXpCLElBQWdDRixRQUFRLENBQUNFLE1BQVQsSUFBbUIsR0FBdkQsRUFBNEQ7QUFDeERDLFFBQUFBLE9BQU8sQ0FBQ0MsSUFBUiwrQkFBb0NMLEdBQXBDO0FBQ0EsYUFBS00sUUFBTCxDQUFjO0FBQUViLFVBQUFBLElBQUksRUFBRSx5QkFBRyxvQkFBSDtBQUFSLFNBQWQ7QUFDQTtBQUNIOztBQUVEUyxNQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ0ssT0FBTCxDQUFhLDRCQUFiLEVBQTJDLENBQUNDLEtBQUQsRUFBUUMsRUFBUixLQUFhLEtBQUtmLFNBQUwsQ0FBZWUsRUFBZixDQUF4RCxDQUFQOztBQUVBLFVBQUksS0FBS3RCLEtBQUwsQ0FBV3VCLFVBQWYsRUFBMkI7QUFDdkJDLFFBQUFBLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZLEtBQUt6QixLQUFMLENBQVd1QixVQUF2QixFQUFtQ0csT0FBbkMsQ0FBMkNDLEdBQUcsSUFBSTtBQUM5Q1osVUFBQUEsSUFBSSxHQUFHQSxJQUFJLENBQUNhLEtBQUwsQ0FBV0QsR0FBWCxFQUFnQkUsSUFBaEIsQ0FBcUIsS0FBSzdCLEtBQUwsQ0FBV3VCLFVBQVgsQ0FBc0JJLEdBQXRCLENBQXJCLENBQVA7QUFDSCxTQUZEO0FBR0g7O0FBRUQsV0FBS1IsUUFBTCxDQUFjO0FBQUViLFFBQUFBLElBQUksRUFBRVM7QUFBUixPQUFkO0FBQ0gsS0F0Qkw7QUF5QkEsU0FBS1gsY0FBTCxHQUFzQjBCLG9CQUFJQyxRQUFKLENBQWEsS0FBS0MsUUFBbEIsQ0FBdEI7QUFDSDs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsU0FBS3ZCLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxRQUFJLEtBQUtOLGNBQUwsS0FBd0IsSUFBNUIsRUFBa0MwQixvQkFBSUksVUFBSixDQUFlLEtBQUs5QixjQUFwQjtBQUNyQzs7QUFTRCtCLEVBQUFBLE1BQU0sR0FBRztBQUNMO0FBQ0EsVUFBTUMsTUFBTSxHQUFHLEtBQUtDLE9BQUwsSUFBZ0JDLGlDQUFnQkMsR0FBaEIsRUFBL0I7O0FBQ0EsVUFBTUMsT0FBTyxHQUFHSixNQUFNLEdBQUdBLE1BQU0sQ0FBQ0ksT0FBUCxFQUFILEdBQXNCLElBQTVDO0FBQ0EsVUFBTUMsU0FBUyxHQUFHLEtBQUt6QyxLQUFMLENBQVd5QyxTQUE3QjtBQUNBLFVBQU1DLE9BQU8sR0FBRyx5QkFBVztBQUN2QixPQUFDRCxTQUFELEdBQWEsSUFEVTtBQUV2QixpQkFBSUEsU0FBSixjQUF3QkQsT0FGRDtBQUd2QixpQkFBSUMsU0FBSixpQkFBMkIsQ0FBQyxDQUFDTDtBQUhOLEtBQVgsQ0FBaEI7O0FBTUEsVUFBTU8sT0FBTyxHQUFHO0FBQUssTUFBQSxTQUFTLFlBQUtGLFNBQUwsVUFBZDtBQUNaLE1BQUEsdUJBQXVCLEVBQUU7QUFBRUcsUUFBQUEsTUFBTSxFQUFFLEtBQUt2QyxLQUFMLENBQVdDO0FBQXJCO0FBRGIsTUFBaEI7O0FBS0EsUUFBSSxLQUFLTixLQUFMLENBQVc2QyxTQUFmLEVBQTBCO0FBQ3RCLGFBQU8sNkJBQUMsMEJBQUQ7QUFBbUIsUUFBQSxTQUFTLEVBQUVIO0FBQTlCLFNBQ0ZDLE9BREUsQ0FBUDtBQUdILEtBSkQsTUFJTztBQUNILGFBQU87QUFBSyxRQUFBLFNBQVMsRUFBRUQ7QUFBaEIsU0FDRkMsT0FERSxDQUFQO0FBR0g7QUFDSjs7QUF6R3lEOzs7OEJBQXpDL0MsWSxlQUNFO0FBQ2Y7QUFDQWUsRUFBQUEsR0FBRyxFQUFFbUMsbUJBQVVDLE1BRkE7QUFHZjtBQUNBTixFQUFBQSxTQUFTLEVBQUVLLG1CQUFVQyxNQUpOO0FBS2Y7QUFDQUYsRUFBQUEsU0FBUyxFQUFFQyxtQkFBVUUsSUFOTjtBQU9mO0FBQ0F6QixFQUFBQSxVQUFVLEVBQUV1QixtQkFBVUc7QUFSUCxDOzhCQURGckQsWSxpQkFZSXNELDRCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgcmVxdWVzdCBmcm9tICdicm93c2VyLXJlcXVlc3QnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBzYW5pdGl6ZUh0bWwgZnJvbSAnc2FuaXRpemUtaHRtbCc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuaW1wb3J0IEF1dG9IaWRlU2Nyb2xsYmFyIGZyb20gXCIuL0F1dG9IaWRlU2Nyb2xsYmFyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFbWJlZGRlZFBhZ2UgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLy8gVVJMIHRvIHJlcXVlc3QgZW1iZWRkZWQgcGFnZSBjb250ZW50IGZyb21cclxuICAgICAgICB1cmw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gQ2xhc3MgbmFtZSBwcmVmaXggdG8gYXBwbHkgZm9yIGEgZ2l2ZW4gaW5zdGFuY2VcclxuICAgICAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gV2hldGhlciB0byB3cmFwIHRoZSBwYWdlIGluIGEgc2Nyb2xsYmFyXHJcbiAgICAgICAgc2Nyb2xsYmFyOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICAvLyBNYXAgb2Yga2V5cyB0byByZXBsYWNlIHdpdGggdmFsdWVzLCBlLmcgeyRwbGFjZWhvbGRlcjogXCJ2YWx1ZVwifVxyXG4gICAgICAgIHJlcGxhY2VNYXA6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBjb250ZXh0VHlwZSA9IE1hdHJpeENsaWVudENvbnRleHQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuX2Rpc3BhdGNoZXJSZWYgPSBudWxsO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwYWdlOiAnJyxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHRyYW5zbGF0ZShzKSB7XHJcbiAgICAgICAgLy8gZGVmYXVsdCBpbXBsZW1lbnRhdGlvbiAtIHNraW5zIG1heSB3aXNoIHRvIGV4dGVuZCB0aGlzXHJcbiAgICAgICAgcmV0dXJuIHNhbml0aXplSHRtbChfdChzKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy51cmwpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gd2UgdXNlIHJlcXVlc3QoKSB0byBpbmxpbmUgdGhlIHBhZ2UgaW50byB0aGUgcmVhY3QgY29tcG9uZW50XHJcbiAgICAgICAgLy8gc28gdGhhdCBpdCBjYW4gaW5oZXJpdCBDU1MgYW5kIHRoZW1pbmcgZWFzaWx5IHJhdGhlciB0aGFuIG1lc3MgYXJvdW5kXHJcbiAgICAgICAgLy8gd2l0aCBpZnJhbWVzIGFuZCB0cnlpbmcgdG8gc3luY2hyb25pc2UgZG9jdW1lbnQuc3R5bGVzaGVldHMuXHJcblxyXG4gICAgICAgIHJlcXVlc3QoXHJcbiAgICAgICAgICAgIHsgbWV0aG9kOiBcIkdFVFwiLCB1cmw6IHRoaXMucHJvcHMudXJsIH0sXHJcbiAgICAgICAgICAgIChlcnIsIHJlc3BvbnNlLCBib2R5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChlcnIgfHwgcmVzcG9uc2Uuc3RhdHVzIDwgMjAwIHx8IHJlc3BvbnNlLnN0YXR1cyA+PSAzMDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oYEVycm9yIGxvYWRpbmcgcGFnZTogJHtlcnJ9YCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHBhZ2U6IF90KFwiQ291bGRuJ3QgbG9hZCBwYWdlXCIpIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBib2R5ID0gYm9keS5yZXBsYWNlKC9fdFxcKFsnXCJdKFtcXHNcXFNdKj8pWydcIl1cXCkvbWcsIChtYXRjaCwgZzEpPT50aGlzLnRyYW5zbGF0ZShnMSkpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLnJlcGxhY2VNYXApIHtcclxuICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyh0aGlzLnByb3BzLnJlcGxhY2VNYXApLmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9keSA9IGJvZHkuc3BsaXQoa2V5KS5qb2luKHRoaXMucHJvcHMucmVwbGFjZU1hcFtrZXldKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcGFnZTogYm9keSB9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICB0aGlzLl9kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIHRoaXMuX3VubW91bnRlZCA9IHRydWU7XHJcbiAgICAgICAgaWYgKHRoaXMuX2Rpc3BhdGNoZXJSZWYgIT09IG51bGwpIGRpcy51bnJlZ2lzdGVyKHRoaXMuX2Rpc3BhdGNoZXJSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQWN0aW9uID0gKHBheWxvYWQpID0+IHtcclxuICAgICAgICAvLyBIQUNLOiBXb3JrYXJvdW5kIGZvciB0aGUgY29udGV4dCdzIE1hdHJpeENsaWVudCBub3QgYmVpbmcgc2V0IHVwIGF0IHJlbmRlciB0aW1lLlxyXG4gICAgICAgIGlmIChwYXlsb2FkLmFjdGlvbiA9PT0gJ2NsaWVudF9zdGFydGVkJykge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgLy8gSEFDSzogV29ya2Fyb3VuZCBmb3IgdGhlIGNvbnRleHQncyBNYXRyaXhDbGllbnQgbm90IHVwZGF0aW5nLlxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IHRoaXMuY29udGV4dCB8fCBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3QgaXNHdWVzdCA9IGNsaWVudCA/IGNsaWVudC5pc0d1ZXN0KCkgOiB0cnVlO1xyXG4gICAgICAgIGNvbnN0IGNsYXNzTmFtZSA9IHRoaXMucHJvcHMuY2xhc3NOYW1lO1xyXG4gICAgICAgIGNvbnN0IGNsYXNzZXMgPSBjbGFzc25hbWVzKHtcclxuICAgICAgICAgICAgW2NsYXNzTmFtZV06IHRydWUsXHJcbiAgICAgICAgICAgIFtgJHtjbGFzc05hbWV9X2d1ZXN0YF06IGlzR3Vlc3QsXHJcbiAgICAgICAgICAgIFtgJHtjbGFzc05hbWV9X2xvZ2dlZEluYF06ICEhY2xpZW50LFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50ID0gPGRpdiBjbGFzc05hbWU9e2Ake2NsYXNzTmFtZX1fYm9keWB9XHJcbiAgICAgICAgICAgIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogdGhpcy5zdGF0ZS5wYWdlIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgIDwvZGl2PjtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2Nyb2xsYmFyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiA8QXV0b0hpZGVTY3JvbGxiYXIgY2xhc3NOYW1lPXtjbGFzc2VzfT5cclxuICAgICAgICAgICAgICAgIHtjb250ZW50fVxyXG4gICAgICAgICAgICA8L0F1dG9IaWRlU2Nyb2xsYmFyPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9PlxyXG4gICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19