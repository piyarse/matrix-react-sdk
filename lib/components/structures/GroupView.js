"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../index"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _HostingLink = require("../../utils/HostingLink");

var _HtmlUtils = require("../../HtmlUtils");

var _languageHandler = require("../../languageHandler");

var _AccessibleButton = _interopRequireDefault(require("../views/elements/AccessibleButton"));

var _GroupHeaderButtons = _interopRequireDefault(require("../views/right_panel/GroupHeaderButtons"));

var _MainSplit = _interopRequireDefault(require("./MainSplit"));

var _RightPanel = _interopRequireDefault(require("./RightPanel"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var _classnames = _interopRequireDefault(require("classnames"));

var _GroupStore = _interopRequireDefault(require("../../stores/GroupStore"));

var _FlairStore = _interopRequireDefault(require("../../stores/FlairStore"));

var _GroupAddressPicker = require("../../GroupAddressPicker");

var _Permalinks = require("../../utils/permalinks/Permalinks");

var _matrixJsSdk = require("matrix-js-sdk");

var _promise = require("../../utils/promise");

var _RightPanelStore = _interopRequireDefault(require("../../stores/RightPanelStore"));

var _AutoHideScrollbar = _interopRequireDefault(require("./AutoHideScrollbar"));

/*
Copyright 2017 Vector Creations Ltd.
Copyright 2017, 2018 New Vector Ltd.
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const LONG_DESC_PLACEHOLDER = (0, _languageHandler._td)("<h1>HTML for your community's page</h1>\n<p>\n    Use the long description to introduce new members to the community, or distribute\n    some important <a href=\"foo\">links</a>\n</p>\n<p>\n    You can even use 'img' tags\n</p>\n");

const RoomSummaryType = _propTypes.default.shape({
  room_id: _propTypes.default.string.isRequired,
  profile: _propTypes.default.shape({
    name: _propTypes.default.string,
    avatar_url: _propTypes.default.string,
    canonical_alias: _propTypes.default.string
  }).isRequired
});

const UserSummaryType = _propTypes.default.shape({
  summaryInfo: _propTypes.default.shape({
    user_id: _propTypes.default.string.isRequired,
    role_id: _propTypes.default.string,
    avatar_url: _propTypes.default.string,
    displayname: _propTypes.default.string
  }).isRequired
});

const CategoryRoomList = (0, _createReactClass.default)({
  displayName: 'CategoryRoomList',
  props: {
    rooms: _propTypes.default.arrayOf(RoomSummaryType).isRequired,
    category: _propTypes.default.shape({
      profile: _propTypes.default.shape({
        name: _propTypes.default.string
      }).isRequired
    }),
    groupId: _propTypes.default.string.isRequired,
    // Whether the list should be editable
    editing: _propTypes.default.bool.isRequired
  },
  onAddRoomsToSummaryClicked: function (ev) {
    ev.preventDefault();
    const AddressPickerDialog = sdk.getComponent("dialogs.AddressPickerDialog");

    _Modal.default.createTrackedDialog('Add Rooms to Group Summary', '', AddressPickerDialog, {
      title: (0, _languageHandler._t)('Add rooms to the community summary'),
      description: (0, _languageHandler._t)("Which rooms would you like to add to this summary?"),
      placeholder: (0, _languageHandler._t)("Room name or alias"),
      button: (0, _languageHandler._t)("Add to summary"),
      pickerType: 'room',
      validAddressTypes: ['mx-room-id'],
      groupId: this.props.groupId,
      onFinished: (success, addrs) => {
        if (!success) return;
        const errorList = [];
        (0, _promise.allSettled)(addrs.map(addr => {
          return _GroupStore.default.addRoomToGroupSummary(this.props.groupId, addr.address).catch(() => {
            errorList.push(addr.address);
          });
        })).then(() => {
          if (errorList.length === 0) {
            return;
          }

          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Failed to add the following room to the group summary', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Failed to add the following rooms to the summary of %(groupId)s:", {
              groupId: this.props.groupId
            }),
            description: errorList.join(", ")
          });
        });
      }
    },
    /*className=*/
    null,
    /*isPriority=*/
    false,
    /*isStatic=*/
    true);
  },
  render: function () {
    const TintableSvg = sdk.getComponent("elements.TintableSvg");
    const addButton = this.props.editing ? _react.default.createElement(_AccessibleButton.default, {
      className: "mx_GroupView_featuredThings_addButton",
      onClick: this.onAddRoomsToSummaryClicked
    }, _react.default.createElement(TintableSvg, {
      src: require("../../../res/img/icons-create-room.svg"),
      width: "64",
      height: "64"
    }), _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_addButton_label"
    }, (0, _languageHandler._t)('Add a Room'))) : _react.default.createElement("div", null);
    const roomNodes = this.props.rooms.map(r => {
      return _react.default.createElement(FeaturedRoom, {
        key: r.room_id,
        groupId: this.props.groupId,
        editing: this.props.editing,
        summaryInfo: r
      });
    });

    let catHeader = _react.default.createElement("div", null);

    if (this.props.category && this.props.category.profile) {
      catHeader = _react.default.createElement("div", {
        className: "mx_GroupView_featuredThings_category"
      }, this.props.category.profile.name);
    }

    return _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_container"
    }, catHeader, roomNodes, addButton);
  }
});
const FeaturedRoom = (0, _createReactClass.default)({
  displayName: 'FeaturedRoom',
  props: {
    summaryInfo: RoomSummaryType.isRequired,
    editing: _propTypes.default.bool.isRequired,
    groupId: _propTypes.default.string.isRequired
  },
  onClick: function (e) {
    e.preventDefault();
    e.stopPropagation();

    _dispatcher.default.dispatch({
      action: 'view_room',
      room_alias: this.props.summaryInfo.profile.canonical_alias,
      room_id: this.props.summaryInfo.room_id
    });
  },
  onDeleteClicked: function (e) {
    e.preventDefault();
    e.stopPropagation();

    _GroupStore.default.removeRoomFromGroupSummary(this.props.groupId, this.props.summaryInfo.room_id).catch(err => {
      console.error('Error whilst removing room from group summary', err);
      const roomName = this.props.summaryInfo.name || this.props.summaryInfo.canonical_alias || this.props.summaryInfo.room_id;
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to remove room from group summary', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to remove the room from the summary of %(groupId)s", {
          groupId: this.props.groupId
        }),
        description: (0, _languageHandler._t)("The room '%(roomName)s' could not be removed from the summary.", {
          roomName
        })
      });
    });
  },
  render: function () {
    const RoomAvatar = sdk.getComponent("avatars.RoomAvatar");
    const roomName = this.props.summaryInfo.profile.name || this.props.summaryInfo.profile.canonical_alias || (0, _languageHandler._t)("Unnamed Room");
    const oobData = {
      roomId: this.props.summaryInfo.room_id,
      avatarUrl: this.props.summaryInfo.profile.avatar_url,
      name: roomName
    };
    let permalink = null;

    if (this.props.summaryInfo.profile && this.props.summaryInfo.profile.canonical_alias) {
      permalink = (0, _Permalinks.makeGroupPermalink)(this.props.summaryInfo.profile.canonical_alias);
    }

    let roomNameNode = null;

    if (permalink) {
      roomNameNode = _react.default.createElement("a", {
        href: permalink,
        onClick: this.onClick
      }, roomName);
    } else {
      roomNameNode = _react.default.createElement("span", null, roomName);
    }

    const deleteButton = this.props.editing ? _react.default.createElement("img", {
      className: "mx_GroupView_featuredThing_deleteButton",
      src: require("../../../res/img/cancel-small.svg"),
      width: "14",
      height: "14",
      alt: "Delete",
      onClick: this.onDeleteClicked
    }) : _react.default.createElement("div", null);
    return _react.default.createElement(_AccessibleButton.default, {
      className: "mx_GroupView_featuredThing",
      onClick: this.onClick
    }, _react.default.createElement(RoomAvatar, {
      oobData: oobData,
      width: 64,
      height: 64
    }), _react.default.createElement("div", {
      className: "mx_GroupView_featuredThing_name"
    }, roomNameNode), deleteButton);
  }
});
const RoleUserList = (0, _createReactClass.default)({
  displayName: 'RoleUserList',
  props: {
    users: _propTypes.default.arrayOf(UserSummaryType).isRequired,
    role: _propTypes.default.shape({
      profile: _propTypes.default.shape({
        name: _propTypes.default.string
      }).isRequired
    }),
    groupId: _propTypes.default.string.isRequired,
    // Whether the list should be editable
    editing: _propTypes.default.bool.isRequired
  },
  onAddUsersClicked: function (ev) {
    ev.preventDefault();
    const AddressPickerDialog = sdk.getComponent("dialogs.AddressPickerDialog");

    _Modal.default.createTrackedDialog('Add Users to Group Summary', '', AddressPickerDialog, {
      title: (0, _languageHandler._t)('Add users to the community summary'),
      description: (0, _languageHandler._t)("Who would you like to add to this summary?"),
      placeholder: (0, _languageHandler._t)("Name or Matrix ID"),
      button: (0, _languageHandler._t)("Add to summary"),
      validAddressTypes: ['mx-user-id'],
      groupId: this.props.groupId,
      shouldOmitSelf: false,
      onFinished: (success, addrs) => {
        if (!success) return;
        const errorList = [];
        (0, _promise.allSettled)(addrs.map(addr => {
          return _GroupStore.default.addUserToGroupSummary(addr.address).catch(() => {
            errorList.push(addr.address);
          });
        })).then(() => {
          if (errorList.length === 0) {
            return;
          }

          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Failed to add the following users to the community summary', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Failed to add the following users to the summary of %(groupId)s:", {
              groupId: this.props.groupId
            }),
            description: errorList.join(", ")
          });
        });
      }
    },
    /*className=*/
    null,
    /*isPriority=*/
    false,
    /*isStatic=*/
    true);
  },
  render: function () {
    const TintableSvg = sdk.getComponent("elements.TintableSvg");
    const addButton = this.props.editing ? _react.default.createElement(_AccessibleButton.default, {
      className: "mx_GroupView_featuredThings_addButton",
      onClick: this.onAddUsersClicked
    }, _react.default.createElement(TintableSvg, {
      src: require("../../../res/img/icons-create-room.svg"),
      width: "64",
      height: "64"
    }), _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_addButton_label"
    }, (0, _languageHandler._t)('Add a User'))) : _react.default.createElement("div", null);
    const userNodes = this.props.users.map(u => {
      return _react.default.createElement(FeaturedUser, {
        key: u.user_id,
        summaryInfo: u,
        editing: this.props.editing,
        groupId: this.props.groupId
      });
    });

    let roleHeader = _react.default.createElement("div", null);

    if (this.props.role && this.props.role.profile) {
      roleHeader = _react.default.createElement("div", {
        className: "mx_GroupView_featuredThings_category"
      }, this.props.role.profile.name);
    }

    return _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_container"
    }, roleHeader, userNodes, addButton);
  }
});
const FeaturedUser = (0, _createReactClass.default)({
  displayName: 'FeaturedUser',
  props: {
    summaryInfo: UserSummaryType.isRequired,
    editing: _propTypes.default.bool.isRequired,
    groupId: _propTypes.default.string.isRequired
  },
  onClick: function (e) {
    e.preventDefault();
    e.stopPropagation();

    _dispatcher.default.dispatch({
      action: 'view_start_chat_or_reuse',
      user_id: this.props.summaryInfo.user_id
    });
  },
  onDeleteClicked: function (e) {
    e.preventDefault();
    e.stopPropagation();

    _GroupStore.default.removeUserFromGroupSummary(this.props.groupId, this.props.summaryInfo.user_id).catch(err => {
      console.error('Error whilst removing user from group summary', err);
      const displayName = this.props.summaryInfo.displayname || this.props.summaryInfo.user_id;
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to remove user from community summary', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to remove a user from the summary of %(groupId)s", {
          groupId: this.props.groupId
        }),
        description: (0, _languageHandler._t)("The user '%(displayName)s' could not be removed from the summary.", {
          displayName
        })
      });
    });
  },
  render: function () {
    const BaseAvatar = sdk.getComponent("avatars.BaseAvatar");
    const name = this.props.summaryInfo.displayname || this.props.summaryInfo.user_id;
    const permalink = (0, _Permalinks.makeUserPermalink)(this.props.summaryInfo.user_id);

    const userNameNode = _react.default.createElement("a", {
      href: permalink,
      onClick: this.onClick
    }, name);

    const httpUrl = _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(this.props.summaryInfo.avatar_url, 64, 64);

    const deleteButton = this.props.editing ? _react.default.createElement("img", {
      className: "mx_GroupView_featuredThing_deleteButton",
      src: require("../../../res/img/cancel-small.svg"),
      width: "14",
      height: "14",
      alt: "Delete",
      onClick: this.onDeleteClicked
    }) : _react.default.createElement("div", null);
    return _react.default.createElement(_AccessibleButton.default, {
      className: "mx_GroupView_featuredThing",
      onClick: this.onClick
    }, _react.default.createElement(BaseAvatar, {
      name: name,
      url: httpUrl,
      width: 64,
      height: 64
    }), _react.default.createElement("div", {
      className: "mx_GroupView_featuredThing_name"
    }, userNameNode), deleteButton);
  }
});
const GROUP_JOINPOLICY_OPEN = "open";
const GROUP_JOINPOLICY_INVITE = "invite";

var _default = (0, _createReactClass.default)({
  displayName: 'GroupView',
  propTypes: {
    groupId: _propTypes.default.string.isRequired,
    // Whether this is the first time the group admin is viewing the group
    groupIsNew: _propTypes.default.bool
  },
  getInitialState: function () {
    return {
      summary: null,
      isGroupPublicised: null,
      isUserPrivileged: null,
      groupRooms: null,
      groupRoomsLoading: null,
      error: null,
      editing: false,
      saving: false,
      uploadingAvatar: false,
      avatarChanged: false,
      membershipBusy: false,
      publicityBusy: false,
      inviterProfile: null,
      showRightPanel: _RightPanelStore.default.getSharedInstance().isOpenForGroup
    };
  },
  componentDidMount: function () {
    this._unmounted = false;
    this._matrixClient = _MatrixClientPeg.MatrixClientPeg.get();

    this._matrixClient.on("Group.myMembership", this._onGroupMyMembership);

    this._initGroupStore(this.props.groupId, true);

    this._dispatcherRef = _dispatcher.default.register(this._onAction);
    this._rightPanelStoreToken = _RightPanelStore.default.getSharedInstance().addListener(this._onRightPanelStoreUpdate);
  },
  componentWillUnmount: function () {
    this._unmounted = true;

    this._matrixClient.removeListener("Group.myMembership", this._onGroupMyMembership);

    _dispatcher.default.unregister(this._dispatcherRef); // Remove RightPanelStore listener


    if (this._rightPanelStoreToken) {
      this._rightPanelStoreToken.remove();
    }
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (newProps) {
    if (this.props.groupId !== newProps.groupId) {
      this.setState({
        summary: null,
        error: null
      }, () => {
        this._initGroupStore(newProps.groupId);
      });
    }
  },
  _onRightPanelStoreUpdate: function () {
    this.setState({
      showRightPanel: _RightPanelStore.default.getSharedInstance().isOpenForGroup
    });
  },
  _onGroupMyMembership: function (group) {
    if (this._unmounted || group.groupId !== this.props.groupId) return;

    if (group.myMembership === 'leave') {
      // Leave settings - the user might have clicked the "Leave" button
      this._closeSettings();
    }

    this.setState({
      membershipBusy: false
    });
  },
  _initGroupStore: function (groupId, firstInit) {
    const group = this._matrixClient.getGroup(groupId);

    if (group && group.inviter && group.inviter.userId) {
      this._fetchInviterProfile(group.inviter.userId);
    }

    _GroupStore.default.registerListener(groupId, this.onGroupStoreUpdated.bind(this, firstInit));

    let willDoOnboarding = false; // XXX: This should be more fluxy - let's get the error from GroupStore .getError or something

    _GroupStore.default.on('error', (err, errorGroupId, stateKey) => {
      if (this._unmounted || groupId !== errorGroupId) return;

      if (err.errcode === 'M_GUEST_ACCESS_FORBIDDEN' && !willDoOnboarding) {
        _dispatcher.default.dispatch({
          action: 'do_after_sync_prepared',
          deferred_action: {
            action: 'view_group',
            group_id: groupId
          }
        });

        _dispatcher.default.dispatch({
          action: 'require_registration',
          screen_after: {
            screen: "group/".concat(groupId)
          }
        });

        willDoOnboarding = true;
      }

      if (stateKey === _GroupStore.default.STATE_KEY.Summary) {
        this.setState({
          summary: null,
          error: err,
          editing: false
        });
      }
    });
  },

  onGroupStoreUpdated(firstInit) {
    if (this._unmounted) return;

    const summary = _GroupStore.default.getSummary(this.props.groupId);

    if (summary.profile) {
      // Default profile fields should be "" for later sending to the server (which
      // requires that the fields are strings, not null)
      ["avatar_url", "long_description", "name", "short_description"].forEach(k => {
        summary.profile[k] = summary.profile[k] || "";
      });
    }

    this.setState({
      summary,
      summaryLoading: !_GroupStore.default.isStateReady(this.props.groupId, _GroupStore.default.STATE_KEY.Summary),
      isGroupPublicised: _GroupStore.default.getGroupPublicity(this.props.groupId),
      isUserPrivileged: _GroupStore.default.isUserPrivileged(this.props.groupId),
      groupRooms: _GroupStore.default.getGroupRooms(this.props.groupId),
      groupRoomsLoading: !_GroupStore.default.isStateReady(this.props.groupId, _GroupStore.default.STATE_KEY.GroupRooms),
      isUserMember: _GroupStore.default.getGroupMembers(this.props.groupId).some(m => m.userId === this._matrixClient.credentials.userId)
    }); // XXX: This might not work but this.props.groupIsNew unused anyway

    if (this.props.groupIsNew && firstInit) {
      this._onEditClick();
    }
  },

  _fetchInviterProfile(userId) {
    this.setState({
      inviterProfileBusy: true
    });

    this._matrixClient.getProfileInfo(userId).then(resp => {
      if (this._unmounted) return;
      this.setState({
        inviterProfile: {
          avatarUrl: resp.avatar_url,
          displayName: resp.displayname
        }
      });
    }).catch(e => {
      console.error('Error getting group inviter profile', e);
    }).finally(() => {
      if (this._unmounted) return;
      this.setState({
        inviterProfileBusy: false
      });
    });
  },

  _onEditClick: function () {
    this.setState({
      editing: true,
      profileForm: Object.assign({}, this.state.summary.profile),
      joinableForm: {
        policyType: this.state.summary.profile.is_openly_joinable ? GROUP_JOINPOLICY_OPEN : GROUP_JOINPOLICY_INVITE
      }
    });
  },
  _onShareClick: function () {
    const ShareDialog = sdk.getComponent("dialogs.ShareDialog");

    _Modal.default.createTrackedDialog('share community dialog', '', ShareDialog, {
      target: this._matrixClient.getGroup(this.props.groupId) || new _matrixJsSdk.Group(this.props.groupId)
    });
  },
  _onCancelClick: function () {
    this._closeSettings();
  },

  _onAction(payload) {
    switch (payload.action) {
      // NOTE: close_settings is an app-wide dispatch; as it is dispatched from MatrixChat
      case 'close_settings':
        this.setState({
          editing: false,
          profileForm: null
        });
        break;

      default:
        break;
    }
  },

  _closeSettings() {
    _dispatcher.default.dispatch({
      action: 'close_settings'
    });
  },

  _onNameChange: function (value) {
    const newProfileForm = Object.assign(this.state.profileForm, {
      name: value
    });
    this.setState({
      profileForm: newProfileForm
    });
  },
  _onShortDescChange: function (value) {
    const newProfileForm = Object.assign(this.state.profileForm, {
      short_description: value
    });
    this.setState({
      profileForm: newProfileForm
    });
  },
  _onLongDescChange: function (e) {
    const newProfileForm = Object.assign(this.state.profileForm, {
      long_description: e.target.value
    });
    this.setState({
      profileForm: newProfileForm
    });
  },
  _onAvatarSelected: function (ev) {
    const file = ev.target.files[0];
    if (!file) return;
    this.setState({
      uploadingAvatar: true
    });

    this._matrixClient.uploadContent(file).then(url => {
      const newProfileForm = Object.assign(this.state.profileForm, {
        avatar_url: url
      });
      this.setState({
        uploadingAvatar: false,
        profileForm: newProfileForm,
        // Indicate that FlairStore needs to be poked to show this change
        // in TagTile (TagPanel), Flair and GroupTile (MyGroups).
        avatarChanged: true
      });
    }).catch(e => {
      this.setState({
        uploadingAvatar: false
      });
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Failed to upload avatar image", e);

      _Modal.default.createTrackedDialog('Failed to upload image', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Error'),
        description: (0, _languageHandler._t)('Failed to upload image')
      });
    });
  },
  _onJoinableChange: function (ev) {
    this.setState({
      joinableForm: {
        policyType: ev.target.value
      }
    });
  },
  _onSaveClick: function () {
    this.setState({
      saving: true
    });
    const savePromise = this.state.isUserPrivileged ? this._saveGroup() : Promise.resolve();
    savePromise.then(result => {
      this.setState({
        saving: false,
        editing: false,
        summary: null
      });

      _dispatcher.default.dispatch({
        action: 'panel_disable'
      });

      this._initGroupStore(this.props.groupId);

      if (this.state.avatarChanged) {
        // XXX: Evil - poking a store should be done from an async action
        _FlairStore.default.refreshGroupProfile(this._matrixClient, this.props.groupId);
      }
    }).catch(e => {
      this.setState({
        saving: false
      });
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Failed to save community profile", e);

      _Modal.default.createTrackedDialog('Failed to update group', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Error'),
        description: (0, _languageHandler._t)('Failed to update community')
      });
    }).finally(() => {
      this.setState({
        avatarChanged: false
      });
    });
  },
  _saveGroup: async function () {
    await this._matrixClient.setGroupProfile(this.props.groupId, this.state.profileForm);
    await this._matrixClient.setGroupJoinPolicy(this.props.groupId, {
      type: this.state.joinableForm.policyType
    });
  },
  _onAcceptInviteClick: async function () {
    this.setState({
      membershipBusy: true
    }); // Wait 500ms to prevent flashing. Do this before sending a request otherwise we risk the
    // spinner disappearing after we have fetched new group data.

    await (0, _promise.sleep)(500);

    _GroupStore.default.acceptGroupInvite(this.props.groupId).then(() => {// don't reset membershipBusy here: wait for the membership change to come down the sync
    }).catch(e => {
      this.setState({
        membershipBusy: false
      });
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Error accepting invite', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Unable to accept invite")
      });
    });
  },
  _onRejectInviteClick: async function () {
    this.setState({
      membershipBusy: true
    }); // Wait 500ms to prevent flashing. Do this before sending a request otherwise we risk the
    // spinner disappearing after we have fetched new group data.

    await (0, _promise.sleep)(500);

    _GroupStore.default.leaveGroup(this.props.groupId).then(() => {// don't reset membershipBusy here: wait for the membership change to come down the sync
    }).catch(e => {
      this.setState({
        membershipBusy: false
      });
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Error rejecting invite', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Unable to reject invite")
      });
    });
  },
  _onJoinClick: async function () {
    if (this._matrixClient.isGuest()) {
      _dispatcher.default.dispatch({
        action: 'require_registration',
        screen_after: {
          screen: "group/".concat(this.props.groupId)
        }
      });

      return;
    }

    this.setState({
      membershipBusy: true
    }); // Wait 500ms to prevent flashing. Do this before sending a request otherwise we risk the
    // spinner disappearing after we have fetched new group data.

    await (0, _promise.sleep)(500);

    _GroupStore.default.joinGroup(this.props.groupId).then(() => {// don't reset membershipBusy here: wait for the membership change to come down the sync
    }).catch(e => {
      this.setState({
        membershipBusy: false
      });
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Error joining room', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Unable to join community")
      });
    });
  },
  _leaveGroupWarnings: function () {
    const warnings = [];

    if (this.state.isUserPrivileged) {
      warnings.push(_react.default.createElement("span", {
        className: "warning"
      }, " "
      /* Whitespace, otherwise the sentences get smashed together */
      , (0, _languageHandler._t)("You are an administrator of this community. You will not be " + "able to rejoin without an invite from another administrator.")));
    }

    return warnings;
  },
  _onLeaveClick: function () {
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

    const warnings = this._leaveGroupWarnings();

    _Modal.default.createTrackedDialog('Leave Group', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Leave Community"),
      description: _react.default.createElement("span", null, (0, _languageHandler._t)("Leave %(groupName)s?", {
        groupName: this.props.groupId
      }), warnings),
      button: (0, _languageHandler._t)("Leave"),
      danger: this.state.isUserPrivileged,
      onFinished: async confirmed => {
        if (!confirmed) return;
        this.setState({
          membershipBusy: true
        }); // Wait 500ms to prevent flashing. Do this before sending a request otherwise we risk the
        // spinner disappearing after we have fetched new group data.

        await (0, _promise.sleep)(500);

        _GroupStore.default.leaveGroup(this.props.groupId).then(() => {// don't reset membershipBusy here: wait for the membership change to come down the sync
        }).catch(e => {
          this.setState({
            membershipBusy: false
          });
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Error leaving community', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Error"),
            description: (0, _languageHandler._t)("Unable to leave community")
          });
        });
      }
    });
  },
  _onAddRoomsClick: function () {
    (0, _GroupAddressPicker.showGroupAddRoomDialog)(this.props.groupId);
  },
  _getGroupSection: function () {
    const groupSettingsSectionClasses = (0, _classnames.default)({
      "mx_GroupView_group": this.state.editing,
      "mx_GroupView_group_disabled": this.state.editing && !this.state.isUserPrivileged
    });
    const header = this.state.editing ? _react.default.createElement("h2", null, " ", (0, _languageHandler._t)('Community Settings'), " ") : _react.default.createElement("div", null);
    const hostingSignupLink = (0, _HostingLink.getHostingLink)('community-settings');
    let hostingSignup = null;

    if (hostingSignupLink && this.state.isUserPrivileged) {
      hostingSignup = _react.default.createElement("div", {
        className: "mx_GroupView_hostingSignup"
      }, (0, _languageHandler._t)("Want more than a community? <a>Get your own server</a>", {}, {
        a: sub => _react.default.createElement("a", {
          href: hostingSignupLink,
          target: "_blank",
          rel: "noreferrer noopener"
        }, sub)
      }), _react.default.createElement("a", {
        href: hostingSignupLink,
        target: "_blank",
        rel: "noreferrer noopener"
      }, _react.default.createElement("img", {
        src: require("../../../res/img/external-link.svg"),
        width: "11",
        height: "10",
        alt: ""
      })));
    }

    const changeDelayWarning = this.state.editing && this.state.isUserPrivileged ? _react.default.createElement("div", {
      className: "mx_GroupView_changeDelayWarning"
    }, (0, _languageHandler._t)('Changes made to your community <bold1>name</bold1> and <bold2>avatar</bold2> ' + 'might not be seen by other users for up to 30 minutes.', {}, {
      'bold1': sub => _react.default.createElement("b", null, " ", sub, " "),
      'bold2': sub => _react.default.createElement("b", null, " ", sub, " ")
    })) : _react.default.createElement("div", null);
    return _react.default.createElement("div", {
      className: groupSettingsSectionClasses
    }, header, hostingSignup, changeDelayWarning, this._getJoinableNode(), this._getLongDescriptionNode(), this._getRoomsNode());
  },
  _getRoomsNode: function () {
    const RoomDetailList = sdk.getComponent('rooms.RoomDetailList');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const TintableSvg = sdk.getComponent('elements.TintableSvg');
    const Spinner = sdk.getComponent('elements.Spinner');
    const TooltipButton = sdk.getComponent('elements.TooltipButton');
    const roomsHelpNode = this.state.editing ? _react.default.createElement(TooltipButton, {
      helpText: (0, _languageHandler._t)('These rooms are displayed to community members on the community page. ' + 'Community members can join the rooms by clicking on them.')
    }) : _react.default.createElement("div", null);
    const addRoomRow = this.state.editing ? _react.default.createElement(AccessibleButton, {
      className: "mx_GroupView_rooms_header_addRow",
      onClick: this._onAddRoomsClick
    }, _react.default.createElement("div", {
      className: "mx_GroupView_rooms_header_addRow_button"
    }, _react.default.createElement(TintableSvg, {
      src: require("../../../res/img/icons-room-add.svg"),
      width: "24",
      height: "24"
    })), _react.default.createElement("div", {
      className: "mx_GroupView_rooms_header_addRow_label"
    }, (0, _languageHandler._t)('Add rooms to this community'))) : _react.default.createElement("div", null);
    const roomDetailListClassName = (0, _classnames.default)({
      "mx_fadable": true,
      "mx_fadable_faded": this.state.editing
    });
    return _react.default.createElement("div", {
      className: "mx_GroupView_rooms"
    }, _react.default.createElement("div", {
      className: "mx_GroupView_rooms_header"
    }, _react.default.createElement("h3", null, (0, _languageHandler._t)('Rooms'), roomsHelpNode), addRoomRow), this.state.groupRoomsLoading ? _react.default.createElement(Spinner, null) : _react.default.createElement(RoomDetailList, {
      rooms: this.state.groupRooms,
      className: roomDetailListClassName
    }));
  },
  _getFeaturedRoomsNode: function () {
    const summary = this.state.summary;
    const defaultCategoryRooms = [];
    const categoryRooms = {};
    summary.rooms_section.rooms.forEach(r => {
      if (r.category_id === null) {
        defaultCategoryRooms.push(r);
      } else {
        let list = categoryRooms[r.category_id];

        if (list === undefined) {
          list = [];
          categoryRooms[r.category_id] = list;
        }

        list.push(r);
      }
    });

    const defaultCategoryNode = _react.default.createElement(CategoryRoomList, {
      rooms: defaultCategoryRooms,
      groupId: this.props.groupId,
      editing: this.state.editing
    });

    const categoryRoomNodes = Object.keys(categoryRooms).map(catId => {
      const cat = summary.rooms_section.categories[catId];
      return _react.default.createElement(CategoryRoomList, {
        key: catId,
        rooms: categoryRooms[catId],
        category: cat,
        groupId: this.props.groupId,
        editing: this.state.editing
      });
    });
    return _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings"
    }, _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_header"
    }, (0, _languageHandler._t)('Featured Rooms:')), defaultCategoryNode, categoryRoomNodes);
  },
  _getFeaturedUsersNode: function () {
    const summary = this.state.summary;
    const noRoleUsers = [];
    const roleUsers = {};
    summary.users_section.users.forEach(u => {
      if (u.role_id === null) {
        noRoleUsers.push(u);
      } else {
        let list = roleUsers[u.role_id];

        if (list === undefined) {
          list = [];
          roleUsers[u.role_id] = list;
        }

        list.push(u);
      }
    });

    const noRoleNode = _react.default.createElement(RoleUserList, {
      users: noRoleUsers,
      groupId: this.props.groupId,
      editing: this.state.editing
    });

    const roleUserNodes = Object.keys(roleUsers).map(roleId => {
      const role = summary.users_section.roles[roleId];
      return _react.default.createElement(RoleUserList, {
        key: roleId,
        users: roleUsers[roleId],
        role: role,
        groupId: this.props.groupId,
        editing: this.state.editing
      });
    });
    return _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings"
    }, _react.default.createElement("div", {
      className: "mx_GroupView_featuredThings_header"
    }, (0, _languageHandler._t)('Featured Users:')), noRoleNode, roleUserNodes);
  },
  _getMembershipSection: function () {
    const Spinner = sdk.getComponent("elements.Spinner");
    const BaseAvatar = sdk.getComponent("avatars.BaseAvatar");

    const group = this._matrixClient.getGroup(this.props.groupId);

    if (group && group.myMembership === 'invite') {
      if (this.state.membershipBusy || this.state.inviterProfileBusy) {
        return _react.default.createElement("div", {
          className: "mx_GroupView_membershipSection"
        }, _react.default.createElement(Spinner, null));
      }

      const httpInviterAvatar = this.state.inviterProfile ? this._matrixClient.mxcUrlToHttp(this.state.inviterProfile.avatarUrl, 36, 36) : null;
      let inviterName = group.inviter.userId;

      if (this.state.inviterProfile) {
        inviterName = this.state.inviterProfile.displayName || group.inviter.userId;
      }

      return _react.default.createElement("div", {
        className: "mx_GroupView_membershipSection mx_GroupView_membershipSection_invited"
      }, _react.default.createElement("div", {
        className: "mx_GroupView_membershipSubSection"
      }, _react.default.createElement("div", {
        className: "mx_GroupView_membershipSection_description"
      }, _react.default.createElement(BaseAvatar, {
        url: httpInviterAvatar,
        name: inviterName,
        width: 36,
        height: 36
      }), (0, _languageHandler._t)("%(inviter)s has invited you to join this community", {
        inviter: inviterName
      })), _react.default.createElement("div", {
        className: "mx_GroupView_membership_buttonContainer"
      }, _react.default.createElement(_AccessibleButton.default, {
        className: "mx_GroupView_textButton mx_RoomHeader_textButton",
        onClick: this._onAcceptInviteClick
      }, (0, _languageHandler._t)("Accept")), _react.default.createElement(_AccessibleButton.default, {
        className: "mx_GroupView_textButton mx_RoomHeader_textButton",
        onClick: this._onRejectInviteClick
      }, (0, _languageHandler._t)("Decline")))));
    }

    let membershipContainerExtraClasses;
    let membershipButtonExtraClasses;
    let membershipButtonTooltip;
    let membershipButtonText;
    let membershipButtonOnClick; // User is not in the group

    if ((!group || group.myMembership === 'leave') && this.state.summary && this.state.summary.profile && Boolean(this.state.summary.profile.is_openly_joinable)) {
      membershipButtonText = (0, _languageHandler._t)("Join this community");
      membershipButtonOnClick = this._onJoinClick;
      membershipButtonExtraClasses = 'mx_GroupView_joinButton';
      membershipContainerExtraClasses = 'mx_GroupView_membershipSection_leave';
    } else if (group && group.myMembership === 'join' && this.state.editing) {
      membershipButtonText = (0, _languageHandler._t)("Leave this community");
      membershipButtonOnClick = this._onLeaveClick;
      membershipButtonTooltip = this.state.isUserPrivileged ? (0, _languageHandler._t)("You are an administrator of this community") : (0, _languageHandler._t)("You are a member of this community");
      membershipButtonExtraClasses = {
        'mx_GroupView_leaveButton': true,
        'mx_RoomHeader_textButton_danger': this.state.isUserPrivileged
      };
      membershipContainerExtraClasses = 'mx_GroupView_membershipSection_joined';
    } else {
      return null;
    }

    const membershipButtonClasses = (0, _classnames.default)(['mx_RoomHeader_textButton', 'mx_GroupView_textButton'], membershipButtonExtraClasses);
    const membershipContainerClasses = (0, _classnames.default)('mx_GroupView_membershipSection', membershipContainerExtraClasses);
    return _react.default.createElement("div", {
      className: membershipContainerClasses
    }, _react.default.createElement("div", {
      className: "mx_GroupView_membershipSubSection"
    }, this.state.membershipBusy ? _react.default.createElement(Spinner, null) : _react.default.createElement("div", null), _react.default.createElement("div", {
      className: "mx_GroupView_membership_buttonContainer"
    }, _react.default.createElement(_AccessibleButton.default, {
      className: membershipButtonClasses,
      onClick: membershipButtonOnClick,
      title: membershipButtonTooltip
    }, membershipButtonText))));
  },
  _getJoinableNode: function () {
    const InlineSpinner = sdk.getComponent('elements.InlineSpinner');
    return this.state.editing ? _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)('Who can join this community?'), this.state.groupJoinableLoading ? _react.default.createElement(InlineSpinner, null) : _react.default.createElement("div", null)), _react.default.createElement("div", null, _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      value: GROUP_JOINPOLICY_INVITE,
      checked: this.state.joinableForm.policyType === GROUP_JOINPOLICY_INVITE,
      onChange: this._onJoinableChange
    }), _react.default.createElement("div", {
      className: "mx_GroupView_label_text"
    }, (0, _languageHandler._t)('Only people who have been invited')))), _react.default.createElement("div", null, _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      value: GROUP_JOINPOLICY_OPEN,
      checked: this.state.joinableForm.policyType === GROUP_JOINPOLICY_OPEN,
      onChange: this._onJoinableChange
    }), _react.default.createElement("div", {
      className: "mx_GroupView_label_text"
    }, (0, _languageHandler._t)('Everyone'))))) : null;
  },
  _getLongDescriptionNode: function () {
    const summary = this.state.summary;
    let description = null;

    if (summary.profile && summary.profile.long_description) {
      description = (0, _HtmlUtils.sanitizedHtmlNode)(summary.profile.long_description);
    } else if (this.state.isUserPrivileged) {
      description = _react.default.createElement("div", {
        className: "mx_GroupView_groupDesc_placeholder",
        onClick: this._onEditClick
      }, (0, _languageHandler._t)('Your community hasn\'t got a Long Description, a HTML page to show to community members.<br />' + 'Click here to open settings and give it one!', {}, {
        'br': _react.default.createElement("br", null)
      }));
    }

    const groupDescEditingClasses = (0, _classnames.default)({
      "mx_GroupView_groupDesc": true,
      "mx_GroupView_groupDesc_disabled": !this.state.isUserPrivileged
    });
    return this.state.editing ? _react.default.createElement("div", {
      className: groupDescEditingClasses
    }, _react.default.createElement("h3", null, " ", (0, _languageHandler._t)("Long Description (HTML)"), " "), _react.default.createElement("textarea", {
      value: this.state.profileForm.long_description,
      placeholder: (0, _languageHandler._t)(LONG_DESC_PLACEHOLDER),
      onChange: this._onLongDescChange,
      tabIndex: "4",
      key: "editLongDesc"
    })) : _react.default.createElement("div", {
      className: "mx_GroupView_groupDesc"
    }, description);
  },
  render: function () {
    const GroupAvatar = sdk.getComponent("avatars.GroupAvatar");
    const Spinner = sdk.getComponent("elements.Spinner");

    if (this.state.summaryLoading && this.state.error === null || this.state.saving) {
      return _react.default.createElement(Spinner, null);
    } else if (this.state.summary && !this.state.error) {
      const summary = this.state.summary;
      let avatarNode;
      let nameNode;
      let shortDescNode;
      const rightButtons = [];

      if (this.state.editing && this.state.isUserPrivileged) {
        let avatarImage;

        if (this.state.uploadingAvatar) {
          avatarImage = _react.default.createElement(Spinner, null);
        } else {
          const GroupAvatar = sdk.getComponent('avatars.GroupAvatar');
          avatarImage = _react.default.createElement(GroupAvatar, {
            groupId: this.props.groupId,
            groupName: this.state.profileForm.name,
            groupAvatarUrl: this.state.profileForm.avatar_url,
            width: 28,
            height: 28,
            resizeMethod: "crop"
          });
        }

        avatarNode = _react.default.createElement("div", {
          className: "mx_GroupView_avatarPicker"
        }, _react.default.createElement("label", {
          htmlFor: "avatarInput",
          className: "mx_GroupView_avatarPicker_label"
        }, avatarImage), _react.default.createElement("div", {
          className: "mx_GroupView_avatarPicker_edit"
        }, _react.default.createElement("label", {
          htmlFor: "avatarInput",
          className: "mx_GroupView_avatarPicker_label"
        }, _react.default.createElement("img", {
          src: require("../../../res/img/camera.svg"),
          alt: (0, _languageHandler._t)("Upload avatar"),
          title: (0, _languageHandler._t)("Upload avatar"),
          width: "17",
          height: "15"
        })), _react.default.createElement("input", {
          id: "avatarInput",
          className: "mx_GroupView_uploadInput",
          type: "file",
          onChange: this._onAvatarSelected
        })));
        const EditableText = sdk.getComponent("elements.EditableText");
        nameNode = _react.default.createElement(EditableText, {
          className: "mx_GroupView_editable",
          placeholderClassName: "mx_GroupView_placeholder",
          placeholder: (0, _languageHandler._t)('Community Name'),
          blurToCancel: false,
          initialValue: this.state.profileForm.name,
          onValueChanged: this._onNameChange,
          tabIndex: "0",
          dir: "auto"
        });
        shortDescNode = _react.default.createElement(EditableText, {
          className: "mx_GroupView_editable",
          placeholderClassName: "mx_GroupView_placeholder",
          placeholder: (0, _languageHandler._t)("Description"),
          blurToCancel: false,
          initialValue: this.state.profileForm.short_description,
          onValueChanged: this._onShortDescChange,
          tabIndex: "0",
          dir: "auto"
        });
      } else {
        const onGroupHeaderItemClick = this.state.isUserMember ? this._onEditClick : null;
        const groupAvatarUrl = summary.profile ? summary.profile.avatar_url : null;
        const groupName = summary.profile ? summary.profile.name : null;
        avatarNode = _react.default.createElement(GroupAvatar, {
          groupId: this.props.groupId,
          groupAvatarUrl: groupAvatarUrl,
          groupName: groupName,
          onClick: onGroupHeaderItemClick,
          width: 28,
          height: 28
        });

        if (summary.profile && summary.profile.name) {
          nameNode = _react.default.createElement("div", {
            onClick: onGroupHeaderItemClick
          }, _react.default.createElement("span", null, summary.profile.name), _react.default.createElement("span", {
            className: "mx_GroupView_header_groupid"
          }, "(", this.props.groupId, ")"));
        } else {
          nameNode = _react.default.createElement("span", {
            onClick: onGroupHeaderItemClick
          }, this.props.groupId);
        }

        if (summary.profile && summary.profile.short_description) {
          shortDescNode = _react.default.createElement("span", {
            onClick: onGroupHeaderItemClick
          }, summary.profile.short_description);
        }
      }

      if (this.state.editing) {
        rightButtons.push(_react.default.createElement(_AccessibleButton.default, {
          className: "mx_GroupView_textButton mx_RoomHeader_textButton",
          key: "_saveButton",
          onClick: this._onSaveClick
        }, (0, _languageHandler._t)('Save')));
        rightButtons.push(_react.default.createElement(_AccessibleButton.default, {
          className: "mx_RoomHeader_cancelButton",
          key: "_cancelButton",
          onClick: this._onCancelClick
        }, _react.default.createElement("img", {
          src: require("../../../res/img/cancel.svg"),
          className: "mx_filterFlipColor",
          width: "18",
          height: "18",
          alt: (0, _languageHandler._t)("Cancel")
        })));
      } else {
        if (summary.user && summary.user.membership === 'join') {
          rightButtons.push(_react.default.createElement(_AccessibleButton.default, {
            className: "mx_GroupHeader_button mx_GroupHeader_editButton",
            key: "_editButton",
            onClick: this._onEditClick,
            title: (0, _languageHandler._t)("Community Settings")
          }));
        }

        rightButtons.push(_react.default.createElement(_AccessibleButton.default, {
          className: "mx_GroupHeader_button mx_GroupHeader_shareButton",
          key: "_shareButton",
          onClick: this._onShareClick,
          title: (0, _languageHandler._t)('Share Community')
        }));
      }

      const rightPanel = this.state.showRightPanel ? _react.default.createElement(_RightPanel.default, {
        groupId: this.props.groupId
      }) : undefined;
      const headerClasses = {
        "mx_GroupView_header": true,
        "light-panel": true,
        "mx_GroupView_header_view": !this.state.editing,
        "mx_GroupView_header_isUserMember": this.state.isUserMember
      };
      return _react.default.createElement("main", {
        className: "mx_GroupView"
      }, _react.default.createElement("div", {
        className: (0, _classnames.default)(headerClasses)
      }, _react.default.createElement("div", {
        className: "mx_GroupView_header_leftCol"
      }, _react.default.createElement("div", {
        className: "mx_GroupView_header_avatar"
      }, avatarNode), _react.default.createElement("div", {
        className: "mx_GroupView_header_info"
      }, _react.default.createElement("div", {
        className: "mx_GroupView_header_name"
      }, nameNode), _react.default.createElement("div", {
        className: "mx_GroupView_header_shortDesc"
      }, shortDescNode))), _react.default.createElement("div", {
        className: "mx_GroupView_header_rightCol"
      }, rightButtons), _react.default.createElement(_GroupHeaderButtons.default, null)), _react.default.createElement(_MainSplit.default, {
        panel: rightPanel
      }, _react.default.createElement(_AutoHideScrollbar.default, {
        className: "mx_GroupView_body"
      }, this._getMembershipSection(), this._getGroupSection())));
    } else if (this.state.error) {
      if (this.state.error.httpStatus === 404) {
        return _react.default.createElement("div", {
          className: "mx_GroupView_error"
        }, (0, _languageHandler._t)('Community %(groupId)s not found', {
          groupId: this.props.groupId
        }));
      } else {
        let extraText;

        if (this.state.error.errcode === 'M_UNRECOGNIZED') {
          extraText = _react.default.createElement("div", null, (0, _languageHandler._t)('This homeserver does not support communities'));
        }

        return _react.default.createElement("div", {
          className: "mx_GroupView_error"
        }, (0, _languageHandler._t)('Failed to load %(groupId)s', {
          groupId: this.props.groupId
        }), extraText);
      }
    } else {
      console.error("Invalid state for GroupView");
      return _react.default.createElement("div", null);
    }
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvR3JvdXBWaWV3LmpzIl0sIm5hbWVzIjpbIkxPTkdfREVTQ19QTEFDRUhPTERFUiIsIlJvb21TdW1tYXJ5VHlwZSIsIlByb3BUeXBlcyIsInNoYXBlIiwicm9vbV9pZCIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJwcm9maWxlIiwibmFtZSIsImF2YXRhcl91cmwiLCJjYW5vbmljYWxfYWxpYXMiLCJVc2VyU3VtbWFyeVR5cGUiLCJzdW1tYXJ5SW5mbyIsInVzZXJfaWQiLCJyb2xlX2lkIiwiZGlzcGxheW5hbWUiLCJDYXRlZ29yeVJvb21MaXN0IiwiZGlzcGxheU5hbWUiLCJwcm9wcyIsInJvb21zIiwiYXJyYXlPZiIsImNhdGVnb3J5IiwiZ3JvdXBJZCIsImVkaXRpbmciLCJib29sIiwib25BZGRSb29tc1RvU3VtbWFyeUNsaWNrZWQiLCJldiIsInByZXZlbnREZWZhdWx0IiwiQWRkcmVzc1BpY2tlckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJwbGFjZWhvbGRlciIsImJ1dHRvbiIsInBpY2tlclR5cGUiLCJ2YWxpZEFkZHJlc3NUeXBlcyIsIm9uRmluaXNoZWQiLCJzdWNjZXNzIiwiYWRkcnMiLCJlcnJvckxpc3QiLCJtYXAiLCJhZGRyIiwiR3JvdXBTdG9yZSIsImFkZFJvb21Ub0dyb3VwU3VtbWFyeSIsImFkZHJlc3MiLCJjYXRjaCIsInB1c2giLCJ0aGVuIiwibGVuZ3RoIiwiRXJyb3JEaWFsb2ciLCJqb2luIiwicmVuZGVyIiwiVGludGFibGVTdmciLCJhZGRCdXR0b24iLCJyZXF1aXJlIiwicm9vbU5vZGVzIiwiciIsImNhdEhlYWRlciIsIkZlYXR1cmVkUm9vbSIsIm9uQ2xpY2siLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJyb29tX2FsaWFzIiwib25EZWxldGVDbGlja2VkIiwicmVtb3ZlUm9vbUZyb21Hcm91cFN1bW1hcnkiLCJlcnIiLCJjb25zb2xlIiwiZXJyb3IiLCJyb29tTmFtZSIsIlJvb21BdmF0YXIiLCJvb2JEYXRhIiwicm9vbUlkIiwiYXZhdGFyVXJsIiwicGVybWFsaW5rIiwicm9vbU5hbWVOb2RlIiwiZGVsZXRlQnV0dG9uIiwiUm9sZVVzZXJMaXN0IiwidXNlcnMiLCJyb2xlIiwib25BZGRVc2Vyc0NsaWNrZWQiLCJzaG91bGRPbWl0U2VsZiIsImFkZFVzZXJUb0dyb3VwU3VtbWFyeSIsInVzZXJOb2RlcyIsInUiLCJyb2xlSGVhZGVyIiwiRmVhdHVyZWRVc2VyIiwicmVtb3ZlVXNlckZyb21Hcm91cFN1bW1hcnkiLCJCYXNlQXZhdGFyIiwidXNlck5hbWVOb2RlIiwiaHR0cFVybCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm14Y1VybFRvSHR0cCIsIkdST1VQX0pPSU5QT0xJQ1lfT1BFTiIsIkdST1VQX0pPSU5QT0xJQ1lfSU5WSVRFIiwicHJvcFR5cGVzIiwiZ3JvdXBJc05ldyIsImdldEluaXRpYWxTdGF0ZSIsInN1bW1hcnkiLCJpc0dyb3VwUHVibGljaXNlZCIsImlzVXNlclByaXZpbGVnZWQiLCJncm91cFJvb21zIiwiZ3JvdXBSb29tc0xvYWRpbmciLCJzYXZpbmciLCJ1cGxvYWRpbmdBdmF0YXIiLCJhdmF0YXJDaGFuZ2VkIiwibWVtYmVyc2hpcEJ1c3kiLCJwdWJsaWNpdHlCdXN5IiwiaW52aXRlclByb2ZpbGUiLCJzaG93UmlnaHRQYW5lbCIsIlJpZ2h0UGFuZWxTdG9yZSIsImdldFNoYXJlZEluc3RhbmNlIiwiaXNPcGVuRm9yR3JvdXAiLCJjb21wb25lbnREaWRNb3VudCIsIl91bm1vdW50ZWQiLCJfbWF0cml4Q2xpZW50Iiwib24iLCJfb25Hcm91cE15TWVtYmVyc2hpcCIsIl9pbml0R3JvdXBTdG9yZSIsIl9kaXNwYXRjaGVyUmVmIiwicmVnaXN0ZXIiLCJfb25BY3Rpb24iLCJfcmlnaHRQYW5lbFN0b3JlVG9rZW4iLCJhZGRMaXN0ZW5lciIsIl9vblJpZ2h0UGFuZWxTdG9yZVVwZGF0ZSIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJ1bnJlZ2lzdGVyIiwicmVtb3ZlIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMiLCJuZXdQcm9wcyIsInNldFN0YXRlIiwiZ3JvdXAiLCJteU1lbWJlcnNoaXAiLCJfY2xvc2VTZXR0aW5ncyIsImZpcnN0SW5pdCIsImdldEdyb3VwIiwiaW52aXRlciIsInVzZXJJZCIsIl9mZXRjaEludml0ZXJQcm9maWxlIiwicmVnaXN0ZXJMaXN0ZW5lciIsIm9uR3JvdXBTdG9yZVVwZGF0ZWQiLCJiaW5kIiwid2lsbERvT25ib2FyZGluZyIsImVycm9yR3JvdXBJZCIsInN0YXRlS2V5IiwiZXJyY29kZSIsImRlZmVycmVkX2FjdGlvbiIsImdyb3VwX2lkIiwic2NyZWVuX2FmdGVyIiwic2NyZWVuIiwiU1RBVEVfS0VZIiwiU3VtbWFyeSIsImdldFN1bW1hcnkiLCJmb3JFYWNoIiwiayIsInN1bW1hcnlMb2FkaW5nIiwiaXNTdGF0ZVJlYWR5IiwiZ2V0R3JvdXBQdWJsaWNpdHkiLCJnZXRHcm91cFJvb21zIiwiR3JvdXBSb29tcyIsImlzVXNlck1lbWJlciIsImdldEdyb3VwTWVtYmVycyIsInNvbWUiLCJtIiwiY3JlZGVudGlhbHMiLCJfb25FZGl0Q2xpY2siLCJpbnZpdGVyUHJvZmlsZUJ1c3kiLCJnZXRQcm9maWxlSW5mbyIsInJlc3AiLCJmaW5hbGx5IiwicHJvZmlsZUZvcm0iLCJPYmplY3QiLCJhc3NpZ24iLCJzdGF0ZSIsImpvaW5hYmxlRm9ybSIsInBvbGljeVR5cGUiLCJpc19vcGVubHlfam9pbmFibGUiLCJfb25TaGFyZUNsaWNrIiwiU2hhcmVEaWFsb2ciLCJ0YXJnZXQiLCJHcm91cCIsIl9vbkNhbmNlbENsaWNrIiwicGF5bG9hZCIsIl9vbk5hbWVDaGFuZ2UiLCJ2YWx1ZSIsIm5ld1Byb2ZpbGVGb3JtIiwiX29uU2hvcnREZXNjQ2hhbmdlIiwic2hvcnRfZGVzY3JpcHRpb24iLCJfb25Mb25nRGVzY0NoYW5nZSIsImxvbmdfZGVzY3JpcHRpb24iLCJfb25BdmF0YXJTZWxlY3RlZCIsImZpbGUiLCJmaWxlcyIsInVwbG9hZENvbnRlbnQiLCJ1cmwiLCJfb25Kb2luYWJsZUNoYW5nZSIsIl9vblNhdmVDbGljayIsInNhdmVQcm9taXNlIiwiX3NhdmVHcm91cCIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVzdWx0IiwiRmxhaXJTdG9yZSIsInJlZnJlc2hHcm91cFByb2ZpbGUiLCJzZXRHcm91cFByb2ZpbGUiLCJzZXRHcm91cEpvaW5Qb2xpY3kiLCJ0eXBlIiwiX29uQWNjZXB0SW52aXRlQ2xpY2siLCJhY2NlcHRHcm91cEludml0ZSIsIl9vblJlamVjdEludml0ZUNsaWNrIiwibGVhdmVHcm91cCIsIl9vbkpvaW5DbGljayIsImlzR3Vlc3QiLCJqb2luR3JvdXAiLCJfbGVhdmVHcm91cFdhcm5pbmdzIiwid2FybmluZ3MiLCJfb25MZWF2ZUNsaWNrIiwiUXVlc3Rpb25EaWFsb2ciLCJncm91cE5hbWUiLCJkYW5nZXIiLCJjb25maXJtZWQiLCJfb25BZGRSb29tc0NsaWNrIiwiX2dldEdyb3VwU2VjdGlvbiIsImdyb3VwU2V0dGluZ3NTZWN0aW9uQ2xhc3NlcyIsImhlYWRlciIsImhvc3RpbmdTaWdudXBMaW5rIiwiaG9zdGluZ1NpZ251cCIsImEiLCJzdWIiLCJjaGFuZ2VEZWxheVdhcm5pbmciLCJfZ2V0Sm9pbmFibGVOb2RlIiwiX2dldExvbmdEZXNjcmlwdGlvbk5vZGUiLCJfZ2V0Um9vbXNOb2RlIiwiUm9vbURldGFpbExpc3QiLCJBY2Nlc3NpYmxlQnV0dG9uIiwiU3Bpbm5lciIsIlRvb2x0aXBCdXR0b24iLCJyb29tc0hlbHBOb2RlIiwiYWRkUm9vbVJvdyIsInJvb21EZXRhaWxMaXN0Q2xhc3NOYW1lIiwiX2dldEZlYXR1cmVkUm9vbXNOb2RlIiwiZGVmYXVsdENhdGVnb3J5Um9vbXMiLCJjYXRlZ29yeVJvb21zIiwicm9vbXNfc2VjdGlvbiIsImNhdGVnb3J5X2lkIiwibGlzdCIsInVuZGVmaW5lZCIsImRlZmF1bHRDYXRlZ29yeU5vZGUiLCJjYXRlZ29yeVJvb21Ob2RlcyIsImtleXMiLCJjYXRJZCIsImNhdCIsImNhdGVnb3JpZXMiLCJfZ2V0RmVhdHVyZWRVc2Vyc05vZGUiLCJub1JvbGVVc2VycyIsInJvbGVVc2VycyIsInVzZXJzX3NlY3Rpb24iLCJub1JvbGVOb2RlIiwicm9sZVVzZXJOb2RlcyIsInJvbGVJZCIsInJvbGVzIiwiX2dldE1lbWJlcnNoaXBTZWN0aW9uIiwiaHR0cEludml0ZXJBdmF0YXIiLCJpbnZpdGVyTmFtZSIsIm1lbWJlcnNoaXBDb250YWluZXJFeHRyYUNsYXNzZXMiLCJtZW1iZXJzaGlwQnV0dG9uRXh0cmFDbGFzc2VzIiwibWVtYmVyc2hpcEJ1dHRvblRvb2x0aXAiLCJtZW1iZXJzaGlwQnV0dG9uVGV4dCIsIm1lbWJlcnNoaXBCdXR0b25PbkNsaWNrIiwiQm9vbGVhbiIsIm1lbWJlcnNoaXBCdXR0b25DbGFzc2VzIiwibWVtYmVyc2hpcENvbnRhaW5lckNsYXNzZXMiLCJJbmxpbmVTcGlubmVyIiwiZ3JvdXBKb2luYWJsZUxvYWRpbmciLCJncm91cERlc2NFZGl0aW5nQ2xhc3NlcyIsIkdyb3VwQXZhdGFyIiwiYXZhdGFyTm9kZSIsIm5hbWVOb2RlIiwic2hvcnREZXNjTm9kZSIsInJpZ2h0QnV0dG9ucyIsImF2YXRhckltYWdlIiwiRWRpdGFibGVUZXh0Iiwib25Hcm91cEhlYWRlckl0ZW1DbGljayIsImdyb3VwQXZhdGFyVXJsIiwidXNlciIsIm1lbWJlcnNoaXAiLCJyaWdodFBhbmVsIiwiaGVhZGVyQ2xhc3NlcyIsImh0dHBTdGF0dXMiLCJleHRyYVRleHQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXpDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQ0EsTUFBTUEscUJBQXFCLEdBQUcsa1FBQTlCOztBQVdBLE1BQU1DLGVBQWUsR0FBR0MsbUJBQVVDLEtBQVYsQ0FBZ0I7QUFDcENDLEVBQUFBLE9BQU8sRUFBRUYsbUJBQVVHLE1BQVYsQ0FBaUJDLFVBRFU7QUFFcENDLEVBQUFBLE9BQU8sRUFBRUwsbUJBQVVDLEtBQVYsQ0FBZ0I7QUFDckJLLElBQUFBLElBQUksRUFBRU4sbUJBQVVHLE1BREs7QUFFckJJLElBQUFBLFVBQVUsRUFBRVAsbUJBQVVHLE1BRkQ7QUFHckJLLElBQUFBLGVBQWUsRUFBRVIsbUJBQVVHO0FBSE4sR0FBaEIsRUFJTkM7QUFOaUMsQ0FBaEIsQ0FBeEI7O0FBU0EsTUFBTUssZUFBZSxHQUFHVCxtQkFBVUMsS0FBVixDQUFnQjtBQUNwQ1MsRUFBQUEsV0FBVyxFQUFFVixtQkFBVUMsS0FBVixDQUFnQjtBQUN6QlUsSUFBQUEsT0FBTyxFQUFFWCxtQkFBVUcsTUFBVixDQUFpQkMsVUFERDtBQUV6QlEsSUFBQUEsT0FBTyxFQUFFWixtQkFBVUcsTUFGTTtBQUd6QkksSUFBQUEsVUFBVSxFQUFFUCxtQkFBVUcsTUFIRztBQUl6QlUsSUFBQUEsV0FBVyxFQUFFYixtQkFBVUc7QUFKRSxHQUFoQixFQUtWQztBQU5pQyxDQUFoQixDQUF4Qjs7QUFTQSxNQUFNVSxnQkFBZ0IsR0FBRywrQkFBaUI7QUFDdENDLEVBQUFBLFdBQVcsRUFBRSxrQkFEeUI7QUFHdENDLEVBQUFBLEtBQUssRUFBRTtBQUNIQyxJQUFBQSxLQUFLLEVBQUVqQixtQkFBVWtCLE9BQVYsQ0FBa0JuQixlQUFsQixFQUFtQ0ssVUFEdkM7QUFFSGUsSUFBQUEsUUFBUSxFQUFFbkIsbUJBQVVDLEtBQVYsQ0FBZ0I7QUFDdEJJLE1BQUFBLE9BQU8sRUFBRUwsbUJBQVVDLEtBQVYsQ0FBZ0I7QUFDckJLLFFBQUFBLElBQUksRUFBRU4sbUJBQVVHO0FBREssT0FBaEIsRUFFTkM7QUFIbUIsS0FBaEIsQ0FGUDtBQU9IZ0IsSUFBQUEsT0FBTyxFQUFFcEIsbUJBQVVHLE1BQVYsQ0FBaUJDLFVBUHZCO0FBU0g7QUFDQWlCLElBQUFBLE9BQU8sRUFBRXJCLG1CQUFVc0IsSUFBVixDQUFlbEI7QUFWckIsR0FIK0I7QUFnQnRDbUIsRUFBQUEsMEJBQTBCLEVBQUUsVUFBU0MsRUFBVCxFQUFhO0FBQ3JDQSxJQUFBQSxFQUFFLENBQUNDLGNBQUg7QUFDQSxVQUFNQyxtQkFBbUIsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDZCQUFqQixDQUE1Qjs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLDRCQUExQixFQUF3RCxFQUF4RCxFQUE0REosbUJBQTVELEVBQWlGO0FBQzdFSyxNQUFBQSxLQUFLLEVBQUUseUJBQUcsb0NBQUgsQ0FEc0U7QUFFN0VDLE1BQUFBLFdBQVcsRUFBRSx5QkFBRyxvREFBSCxDQUZnRTtBQUc3RUMsTUFBQUEsV0FBVyxFQUFFLHlCQUFHLG9CQUFILENBSGdFO0FBSTdFQyxNQUFBQSxNQUFNLEVBQUUseUJBQUcsZ0JBQUgsQ0FKcUU7QUFLN0VDLE1BQUFBLFVBQVUsRUFBRSxNQUxpRTtBQU03RUMsTUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxZQUFELENBTjBEO0FBTzdFaEIsTUFBQUEsT0FBTyxFQUFFLEtBQUtKLEtBQUwsQ0FBV0ksT0FQeUQ7QUFRN0VpQixNQUFBQSxVQUFVLEVBQUUsQ0FBQ0MsT0FBRCxFQUFVQyxLQUFWLEtBQW9CO0FBQzVCLFlBQUksQ0FBQ0QsT0FBTCxFQUFjO0FBQ2QsY0FBTUUsU0FBUyxHQUFHLEVBQWxCO0FBQ0EsaUNBQVdELEtBQUssQ0FBQ0UsR0FBTixDQUFXQyxJQUFELElBQVU7QUFDM0IsaUJBQU9DLG9CQUNGQyxxQkFERSxDQUNvQixLQUFLNUIsS0FBTCxDQUFXSSxPQUQvQixFQUN3Q3NCLElBQUksQ0FBQ0csT0FEN0MsRUFFRkMsS0FGRSxDQUVJLE1BQU07QUFBRU4sWUFBQUEsU0FBUyxDQUFDTyxJQUFWLENBQWVMLElBQUksQ0FBQ0csT0FBcEI7QUFBK0IsV0FGM0MsQ0FBUDtBQUdILFNBSlUsQ0FBWCxFQUlJRyxJQUpKLENBSVMsTUFBTTtBQUNYLGNBQUlSLFNBQVMsQ0FBQ1MsTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUN4QjtBQUNIOztBQUNELGdCQUFNQyxXQUFXLEdBQUd2QixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FDSSx1REFESixFQUVJLEVBRkosRUFFUW9CLFdBRlIsRUFHQTtBQUNJbkIsWUFBQUEsS0FBSyxFQUFFLHlCQUNILGtFQURHLEVBRUg7QUFBQ1gsY0FBQUEsT0FBTyxFQUFFLEtBQUtKLEtBQUwsQ0FBV0k7QUFBckIsYUFGRyxDQURYO0FBS0lZLFlBQUFBLFdBQVcsRUFBRVEsU0FBUyxDQUFDVyxJQUFWLENBQWUsSUFBZjtBQUxqQixXQUhBO0FBVUgsU0FuQkQ7QUFvQkg7QUEvQjRFLEtBQWpGO0FBZ0NHO0FBQWMsUUFoQ2pCO0FBZ0N1QjtBQUFlLFNBaEN0QztBQWdDNkM7QUFBYSxRQWhDMUQ7QUFpQ0gsR0FwRHFDO0FBc0R0Q0MsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxXQUFXLEdBQUcxQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXBCO0FBQ0EsVUFBTTBCLFNBQVMsR0FBRyxLQUFLdEMsS0FBTCxDQUFXSyxPQUFYLEdBQ2IsNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsdUNBQTVCO0FBQ0csTUFBQSxPQUFPLEVBQUUsS0FBS0U7QUFEakIsT0FHRyw2QkFBQyxXQUFEO0FBQWEsTUFBQSxHQUFHLEVBQUVnQyxPQUFPLENBQUMsd0NBQUQsQ0FBekI7QUFBcUUsTUFBQSxLQUFLLEVBQUMsSUFBM0U7QUFBZ0YsTUFBQSxNQUFNLEVBQUM7QUFBdkYsTUFISCxFQUlHO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLFlBQUgsQ0FETixDQUpILENBRGEsR0FRUyx5Q0FSM0I7QUFVQSxVQUFNQyxTQUFTLEdBQUcsS0FBS3hDLEtBQUwsQ0FBV0MsS0FBWCxDQUFpQndCLEdBQWpCLENBQXNCZ0IsQ0FBRCxJQUFPO0FBQzFDLGFBQU8sNkJBQUMsWUFBRDtBQUNILFFBQUEsR0FBRyxFQUFFQSxDQUFDLENBQUN2RCxPQURKO0FBRUgsUUFBQSxPQUFPLEVBQUUsS0FBS2MsS0FBTCxDQUFXSSxPQUZqQjtBQUdILFFBQUEsT0FBTyxFQUFFLEtBQUtKLEtBQUwsQ0FBV0ssT0FIakI7QUFJSCxRQUFBLFdBQVcsRUFBRW9DO0FBSlYsUUFBUDtBQUtILEtBTmlCLENBQWxCOztBQVFBLFFBQUlDLFNBQVMsR0FBRyx5Q0FBaEI7O0FBQ0EsUUFBSSxLQUFLMUMsS0FBTCxDQUFXRyxRQUFYLElBQXVCLEtBQUtILEtBQUwsQ0FBV0csUUFBWCxDQUFvQmQsT0FBL0MsRUFBd0Q7QUFDcERxRCxNQUFBQSxTQUFTLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1YsS0FBSzFDLEtBQUwsQ0FBV0csUUFBWCxDQUFvQmQsT0FBcEIsQ0FBNEJDLElBRGxCLENBQVo7QUFHSDs7QUFDRCxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNEb0QsU0FEQyxFQUVERixTQUZDLEVBR0RGLFNBSEMsQ0FBUDtBQUtIO0FBckZxQyxDQUFqQixDQUF6QjtBQXdGQSxNQUFNSyxZQUFZLEdBQUcsK0JBQWlCO0FBQ2xDNUMsRUFBQUEsV0FBVyxFQUFFLGNBRHFCO0FBR2xDQyxFQUFBQSxLQUFLLEVBQUU7QUFDSE4sSUFBQUEsV0FBVyxFQUFFWCxlQUFlLENBQUNLLFVBRDFCO0FBRUhpQixJQUFBQSxPQUFPLEVBQUVyQixtQkFBVXNCLElBQVYsQ0FBZWxCLFVBRnJCO0FBR0hnQixJQUFBQSxPQUFPLEVBQUVwQixtQkFBVUcsTUFBVixDQUFpQkM7QUFIdkIsR0FIMkI7QUFTbEN3RCxFQUFBQSxPQUFPLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQ2pCQSxJQUFBQSxDQUFDLENBQUNwQyxjQUFGO0FBQ0FvQyxJQUFBQSxDQUFDLENBQUNDLGVBQUY7O0FBRUFDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVEMsTUFBQUEsVUFBVSxFQUFFLEtBQUtsRCxLQUFMLENBQVdOLFdBQVgsQ0FBdUJMLE9BQXZCLENBQStCRyxlQUZsQztBQUdUTixNQUFBQSxPQUFPLEVBQUUsS0FBS2MsS0FBTCxDQUFXTixXQUFYLENBQXVCUjtBQUh2QixLQUFiO0FBS0gsR0FsQmlDO0FBb0JsQ2lFLEVBQUFBLGVBQWUsRUFBRSxVQUFTTixDQUFULEVBQVk7QUFDekJBLElBQUFBLENBQUMsQ0FBQ3BDLGNBQUY7QUFDQW9DLElBQUFBLENBQUMsQ0FBQ0MsZUFBRjs7QUFDQW5CLHdCQUFXeUIsMEJBQVgsQ0FDSSxLQUFLcEQsS0FBTCxDQUFXSSxPQURmLEVBRUksS0FBS0osS0FBTCxDQUFXTixXQUFYLENBQXVCUixPQUYzQixFQUdFNEMsS0FIRixDQUdTdUIsR0FBRCxJQUFTO0FBQ2JDLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLCtDQUFkLEVBQStERixHQUEvRDtBQUNBLFlBQU1HLFFBQVEsR0FBRyxLQUFLeEQsS0FBTCxDQUFXTixXQUFYLENBQXVCSixJQUF2QixJQUNiLEtBQUtVLEtBQUwsQ0FBV04sV0FBWCxDQUF1QkYsZUFEVixJQUViLEtBQUtRLEtBQUwsQ0FBV04sV0FBWCxDQUF1QlIsT0FGM0I7QUFHQSxZQUFNZ0QsV0FBVyxHQUFHdkIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQ0ksMENBREosRUFFSSxFQUZKLEVBRVFvQixXQUZSLEVBR0E7QUFDSW5CLFFBQUFBLEtBQUssRUFBRSx5QkFDSCwyREFERyxFQUVIO0FBQUNYLFVBQUFBLE9BQU8sRUFBRSxLQUFLSixLQUFMLENBQVdJO0FBQXJCLFNBRkcsQ0FEWDtBQUtJWSxRQUFBQSxXQUFXLEVBQUUseUJBQUcsZ0VBQUgsRUFBcUU7QUFBQ3dDLFVBQUFBO0FBQUQsU0FBckU7QUFMakIsT0FIQTtBQVVILEtBbkJEO0FBb0JILEdBM0NpQztBQTZDbENwQixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1xQixVQUFVLEdBQUc5QyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsb0JBQWpCLENBQW5CO0FBRUEsVUFBTTRDLFFBQVEsR0FBRyxLQUFLeEQsS0FBTCxDQUFXTixXQUFYLENBQXVCTCxPQUF2QixDQUErQkMsSUFBL0IsSUFDYixLQUFLVSxLQUFMLENBQVdOLFdBQVgsQ0FBdUJMLE9BQXZCLENBQStCRyxlQURsQixJQUViLHlCQUFHLGNBQUgsQ0FGSjtBQUlBLFVBQU1rRSxPQUFPLEdBQUc7QUFDWkMsTUFBQUEsTUFBTSxFQUFFLEtBQUszRCxLQUFMLENBQVdOLFdBQVgsQ0FBdUJSLE9BRG5CO0FBRVowRSxNQUFBQSxTQUFTLEVBQUUsS0FBSzVELEtBQUwsQ0FBV04sV0FBWCxDQUF1QkwsT0FBdkIsQ0FBK0JFLFVBRjlCO0FBR1pELE1BQUFBLElBQUksRUFBRWtFO0FBSE0sS0FBaEI7QUFNQSxRQUFJSyxTQUFTLEdBQUcsSUFBaEI7O0FBQ0EsUUFBSSxLQUFLN0QsS0FBTCxDQUFXTixXQUFYLENBQXVCTCxPQUF2QixJQUFrQyxLQUFLVyxLQUFMLENBQVdOLFdBQVgsQ0FBdUJMLE9BQXZCLENBQStCRyxlQUFyRSxFQUFzRjtBQUNsRnFFLE1BQUFBLFNBQVMsR0FBRyxvQ0FBbUIsS0FBSzdELEtBQUwsQ0FBV04sV0FBWCxDQUF1QkwsT0FBdkIsQ0FBK0JHLGVBQWxELENBQVo7QUFDSDs7QUFFRCxRQUFJc0UsWUFBWSxHQUFHLElBQW5COztBQUNBLFFBQUlELFNBQUosRUFBZTtBQUNYQyxNQUFBQSxZQUFZLEdBQUc7QUFBRyxRQUFBLElBQUksRUFBRUQsU0FBVDtBQUFvQixRQUFBLE9BQU8sRUFBRSxLQUFLakI7QUFBbEMsU0FBOENZLFFBQTlDLENBQWY7QUFDSCxLQUZELE1BRU87QUFDSE0sTUFBQUEsWUFBWSxHQUFHLDJDQUFRTixRQUFSLENBQWY7QUFDSDs7QUFFRCxVQUFNTyxZQUFZLEdBQUcsS0FBSy9ELEtBQUwsQ0FBV0ssT0FBWCxHQUNqQjtBQUNJLE1BQUEsU0FBUyxFQUFDLHlDQURkO0FBRUksTUFBQSxHQUFHLEVBQUVrQyxPQUFPLENBQUMsbUNBQUQsQ0FGaEI7QUFHSSxNQUFBLEtBQUssRUFBQyxJQUhWO0FBSUksTUFBQSxNQUFNLEVBQUMsSUFKWDtBQUtJLE1BQUEsR0FBRyxFQUFDLFFBTFI7QUFNSSxNQUFBLE9BQU8sRUFBRSxLQUFLWTtBQU5sQixNQURpQixHQVFmLHlDQVJOO0FBVUEsV0FBTyw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBQyw0QkFBNUI7QUFBeUQsTUFBQSxPQUFPLEVBQUUsS0FBS1A7QUFBdkUsT0FDSCw2QkFBQyxVQUFEO0FBQVksTUFBQSxPQUFPLEVBQUVjLE9BQXJCO0FBQThCLE1BQUEsS0FBSyxFQUFFLEVBQXJDO0FBQXlDLE1BQUEsTUFBTSxFQUFFO0FBQWpELE1BREcsRUFFSDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBbURJLFlBQW5ELENBRkcsRUFHREMsWUFIQyxDQUFQO0FBS0g7QUFyRmlDLENBQWpCLENBQXJCO0FBd0ZBLE1BQU1DLFlBQVksR0FBRywrQkFBaUI7QUFDbENqRSxFQUFBQSxXQUFXLEVBQUUsY0FEcUI7QUFHbENDLEVBQUFBLEtBQUssRUFBRTtBQUNIaUUsSUFBQUEsS0FBSyxFQUFFakYsbUJBQVVrQixPQUFWLENBQWtCVCxlQUFsQixFQUFtQ0wsVUFEdkM7QUFFSDhFLElBQUFBLElBQUksRUFBRWxGLG1CQUFVQyxLQUFWLENBQWdCO0FBQ2xCSSxNQUFBQSxPQUFPLEVBQUVMLG1CQUFVQyxLQUFWLENBQWdCO0FBQ3JCSyxRQUFBQSxJQUFJLEVBQUVOLG1CQUFVRztBQURLLE9BQWhCLEVBRU5DO0FBSGUsS0FBaEIsQ0FGSDtBQU9IZ0IsSUFBQUEsT0FBTyxFQUFFcEIsbUJBQVVHLE1BQVYsQ0FBaUJDLFVBUHZCO0FBU0g7QUFDQWlCLElBQUFBLE9BQU8sRUFBRXJCLG1CQUFVc0IsSUFBVixDQUFlbEI7QUFWckIsR0FIMkI7QUFnQmxDK0UsRUFBQUEsaUJBQWlCLEVBQUUsVUFBUzNELEVBQVQsRUFBYTtBQUM1QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0EsVUFBTUMsbUJBQW1CLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBNUI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQiw0QkFBMUIsRUFBd0QsRUFBeEQsRUFBNERKLG1CQUE1RCxFQUFpRjtBQUM3RUssTUFBQUEsS0FBSyxFQUFFLHlCQUFHLG9DQUFILENBRHNFO0FBRTdFQyxNQUFBQSxXQUFXLEVBQUUseUJBQUcsNENBQUgsQ0FGZ0U7QUFHN0VDLE1BQUFBLFdBQVcsRUFBRSx5QkFBRyxtQkFBSCxDQUhnRTtBQUk3RUMsTUFBQUEsTUFBTSxFQUFFLHlCQUFHLGdCQUFILENBSnFFO0FBSzdFRSxNQUFBQSxpQkFBaUIsRUFBRSxDQUFDLFlBQUQsQ0FMMEQ7QUFNN0VoQixNQUFBQSxPQUFPLEVBQUUsS0FBS0osS0FBTCxDQUFXSSxPQU55RDtBQU83RWdFLE1BQUFBLGNBQWMsRUFBRSxLQVA2RDtBQVE3RS9DLE1BQUFBLFVBQVUsRUFBRSxDQUFDQyxPQUFELEVBQVVDLEtBQVYsS0FBb0I7QUFDNUIsWUFBSSxDQUFDRCxPQUFMLEVBQWM7QUFDZCxjQUFNRSxTQUFTLEdBQUcsRUFBbEI7QUFDQSxpQ0FBV0QsS0FBSyxDQUFDRSxHQUFOLENBQVdDLElBQUQsSUFBVTtBQUMzQixpQkFBT0Msb0JBQ0YwQyxxQkFERSxDQUNvQjNDLElBQUksQ0FBQ0csT0FEekIsRUFFRkMsS0FGRSxDQUVJLE1BQU07QUFBRU4sWUFBQUEsU0FBUyxDQUFDTyxJQUFWLENBQWVMLElBQUksQ0FBQ0csT0FBcEI7QUFBK0IsV0FGM0MsQ0FBUDtBQUdILFNBSlUsQ0FBWCxFQUlJRyxJQUpKLENBSVMsTUFBTTtBQUNYLGNBQUlSLFNBQVMsQ0FBQ1MsTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUN4QjtBQUNIOztBQUNELGdCQUFNQyxXQUFXLEdBQUd2QixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FDSSw0REFESixFQUVJLEVBRkosRUFFUW9CLFdBRlIsRUFHQTtBQUNJbkIsWUFBQUEsS0FBSyxFQUFFLHlCQUNILGtFQURHLEVBRUg7QUFBQ1gsY0FBQUEsT0FBTyxFQUFFLEtBQUtKLEtBQUwsQ0FBV0k7QUFBckIsYUFGRyxDQURYO0FBS0lZLFlBQUFBLFdBQVcsRUFBRVEsU0FBUyxDQUFDVyxJQUFWLENBQWUsSUFBZjtBQUxqQixXQUhBO0FBVUgsU0FuQkQ7QUFvQkg7QUEvQjRFLEtBQWpGO0FBZ0NHO0FBQWMsUUFoQ2pCO0FBZ0N1QjtBQUFlLFNBaEN0QztBQWdDNkM7QUFBYSxRQWhDMUQ7QUFpQ0gsR0FwRGlDO0FBc0RsQ0MsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxXQUFXLEdBQUcxQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXBCO0FBQ0EsVUFBTTBCLFNBQVMsR0FBRyxLQUFLdEMsS0FBTCxDQUFXSyxPQUFYLEdBQ2IsNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsdUNBQTVCO0FBQW9FLE1BQUEsT0FBTyxFQUFFLEtBQUs4RDtBQUFsRixPQUNJLDZCQUFDLFdBQUQ7QUFBYSxNQUFBLEdBQUcsRUFBRTVCLE9BQU8sQ0FBQyx3Q0FBRCxDQUF6QjtBQUFxRSxNQUFBLEtBQUssRUFBQyxJQUEzRTtBQUFnRixNQUFBLE1BQU0sRUFBQztBQUF2RixNQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ00seUJBQUcsWUFBSCxDQUROLENBRkosQ0FEYSxHQU1VLHlDQU41QjtBQU9BLFVBQU0rQixTQUFTLEdBQUcsS0FBS3RFLEtBQUwsQ0FBV2lFLEtBQVgsQ0FBaUJ4QyxHQUFqQixDQUFzQjhDLENBQUQsSUFBTztBQUMxQyxhQUFPLDZCQUFDLFlBQUQ7QUFDSCxRQUFBLEdBQUcsRUFBRUEsQ0FBQyxDQUFDNUUsT0FESjtBQUVILFFBQUEsV0FBVyxFQUFFNEUsQ0FGVjtBQUdILFFBQUEsT0FBTyxFQUFFLEtBQUt2RSxLQUFMLENBQVdLLE9BSGpCO0FBSUgsUUFBQSxPQUFPLEVBQUUsS0FBS0wsS0FBTCxDQUFXSTtBQUpqQixRQUFQO0FBS0gsS0FOaUIsQ0FBbEI7O0FBT0EsUUFBSW9FLFVBQVUsR0FBRyx5Q0FBakI7O0FBQ0EsUUFBSSxLQUFLeEUsS0FBTCxDQUFXa0UsSUFBWCxJQUFtQixLQUFLbEUsS0FBTCxDQUFXa0UsSUFBWCxDQUFnQjdFLE9BQXZDLEVBQWdEO0FBQzVDbUYsTUFBQUEsVUFBVSxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUF3RCxLQUFLeEUsS0FBTCxDQUFXa0UsSUFBWCxDQUFnQjdFLE9BQWhCLENBQXdCQyxJQUFoRixDQUFiO0FBQ0g7O0FBQ0QsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDRGtGLFVBREMsRUFFREYsU0FGQyxFQUdEaEMsU0FIQyxDQUFQO0FBS0g7QUEvRWlDLENBQWpCLENBQXJCO0FBa0ZBLE1BQU1tQyxZQUFZLEdBQUcsK0JBQWlCO0FBQ2xDMUUsRUFBQUEsV0FBVyxFQUFFLGNBRHFCO0FBR2xDQyxFQUFBQSxLQUFLLEVBQUU7QUFDSE4sSUFBQUEsV0FBVyxFQUFFRCxlQUFlLENBQUNMLFVBRDFCO0FBRUhpQixJQUFBQSxPQUFPLEVBQUVyQixtQkFBVXNCLElBQVYsQ0FBZWxCLFVBRnJCO0FBR0hnQixJQUFBQSxPQUFPLEVBQUVwQixtQkFBVUcsTUFBVixDQUFpQkM7QUFIdkIsR0FIMkI7QUFTbEN3RCxFQUFBQSxPQUFPLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQ2pCQSxJQUFBQSxDQUFDLENBQUNwQyxjQUFGO0FBQ0FvQyxJQUFBQSxDQUFDLENBQUNDLGVBQUY7O0FBRUFDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLDBCQURDO0FBRVR0RCxNQUFBQSxPQUFPLEVBQUUsS0FBS0ssS0FBTCxDQUFXTixXQUFYLENBQXVCQztBQUZ2QixLQUFiO0FBSUgsR0FqQmlDO0FBbUJsQ3dELEVBQUFBLGVBQWUsRUFBRSxVQUFTTixDQUFULEVBQVk7QUFDekJBLElBQUFBLENBQUMsQ0FBQ3BDLGNBQUY7QUFDQW9DLElBQUFBLENBQUMsQ0FBQ0MsZUFBRjs7QUFDQW5CLHdCQUFXK0MsMEJBQVgsQ0FDSSxLQUFLMUUsS0FBTCxDQUFXSSxPQURmLEVBRUksS0FBS0osS0FBTCxDQUFXTixXQUFYLENBQXVCQyxPQUYzQixFQUdFbUMsS0FIRixDQUdTdUIsR0FBRCxJQUFTO0FBQ2JDLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLCtDQUFkLEVBQStERixHQUEvRDtBQUNBLFlBQU10RCxXQUFXLEdBQUcsS0FBS0MsS0FBTCxDQUFXTixXQUFYLENBQXVCRyxXQUF2QixJQUFzQyxLQUFLRyxLQUFMLENBQVdOLFdBQVgsQ0FBdUJDLE9BQWpGO0FBQ0EsWUFBTXVDLFdBQVcsR0FBR3ZCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUNJLDhDQURKLEVBRUksRUFGSixFQUVRb0IsV0FGUixFQUdBO0FBQ0luQixRQUFBQSxLQUFLLEVBQUUseUJBQ0gseURBREcsRUFFSDtBQUFDWCxVQUFBQSxPQUFPLEVBQUUsS0FBS0osS0FBTCxDQUFXSTtBQUFyQixTQUZHLENBRFg7QUFLSVksUUFBQUEsV0FBVyxFQUFFLHlCQUFHLG1FQUFILEVBQXdFO0FBQUNqQixVQUFBQTtBQUFELFNBQXhFO0FBTGpCLE9BSEE7QUFVSCxLQWpCRDtBQWtCSCxHQXhDaUM7QUEwQ2xDcUMsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNdUMsVUFBVSxHQUFHaEUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjtBQUNBLFVBQU10QixJQUFJLEdBQUcsS0FBS1UsS0FBTCxDQUFXTixXQUFYLENBQXVCRyxXQUF2QixJQUFzQyxLQUFLRyxLQUFMLENBQVdOLFdBQVgsQ0FBdUJDLE9BQTFFO0FBRUEsVUFBTWtFLFNBQVMsR0FBRyxtQ0FBa0IsS0FBSzdELEtBQUwsQ0FBV04sV0FBWCxDQUF1QkMsT0FBekMsQ0FBbEI7O0FBQ0EsVUFBTWlGLFlBQVksR0FBRztBQUFHLE1BQUEsSUFBSSxFQUFFZixTQUFUO0FBQW9CLE1BQUEsT0FBTyxFQUFFLEtBQUtqQjtBQUFsQyxPQUE2Q3RELElBQTdDLENBQXJCOztBQUNBLFVBQU11RixPQUFPLEdBQUdDLGlDQUFnQkMsR0FBaEIsR0FDWEMsWUFEVyxDQUNFLEtBQUtoRixLQUFMLENBQVdOLFdBQVgsQ0FBdUJILFVBRHpCLEVBQ3FDLEVBRHJDLEVBQ3lDLEVBRHpDLENBQWhCOztBQUdBLFVBQU13RSxZQUFZLEdBQUcsS0FBSy9ELEtBQUwsQ0FBV0ssT0FBWCxHQUNqQjtBQUNJLE1BQUEsU0FBUyxFQUFDLHlDQURkO0FBRUksTUFBQSxHQUFHLEVBQUVrQyxPQUFPLENBQUMsbUNBQUQsQ0FGaEI7QUFHSSxNQUFBLEtBQUssRUFBQyxJQUhWO0FBSUksTUFBQSxNQUFNLEVBQUMsSUFKWDtBQUtJLE1BQUEsR0FBRyxFQUFDLFFBTFI7QUFNSSxNQUFBLE9BQU8sRUFBRSxLQUFLWTtBQU5sQixNQURpQixHQVFmLHlDQVJOO0FBVUEsV0FBTyw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBQyw0QkFBNUI7QUFBeUQsTUFBQSxPQUFPLEVBQUUsS0FBS1A7QUFBdkUsT0FDSCw2QkFBQyxVQUFEO0FBQVksTUFBQSxJQUFJLEVBQUV0RCxJQUFsQjtBQUF3QixNQUFBLEdBQUcsRUFBRXVGLE9BQTdCO0FBQXNDLE1BQUEsS0FBSyxFQUFFLEVBQTdDO0FBQWlELE1BQUEsTUFBTSxFQUFFO0FBQXpELE1BREcsRUFFSDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBbURELFlBQW5ELENBRkcsRUFHRGIsWUFIQyxDQUFQO0FBS0g7QUFsRWlDLENBQWpCLENBQXJCO0FBcUVBLE1BQU1rQixxQkFBcUIsR0FBRyxNQUE5QjtBQUNBLE1BQU1DLHVCQUF1QixHQUFHLFFBQWhDOztlQUVlLCtCQUFpQjtBQUM1Qm5GLEVBQUFBLFdBQVcsRUFBRSxXQURlO0FBRzVCb0YsRUFBQUEsU0FBUyxFQUFFO0FBQ1AvRSxJQUFBQSxPQUFPLEVBQUVwQixtQkFBVUcsTUFBVixDQUFpQkMsVUFEbkI7QUFFUDtBQUNBZ0csSUFBQUEsVUFBVSxFQUFFcEcsbUJBQVVzQjtBQUhmLEdBSGlCO0FBUzVCK0UsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxPQUFPLEVBQUUsSUFETjtBQUVIQyxNQUFBQSxpQkFBaUIsRUFBRSxJQUZoQjtBQUdIQyxNQUFBQSxnQkFBZ0IsRUFBRSxJQUhmO0FBSUhDLE1BQUFBLFVBQVUsRUFBRSxJQUpUO0FBS0hDLE1BQUFBLGlCQUFpQixFQUFFLElBTGhCO0FBTUhuQyxNQUFBQSxLQUFLLEVBQUUsSUFOSjtBQU9IbEQsTUFBQUEsT0FBTyxFQUFFLEtBUE47QUFRSHNGLE1BQUFBLE1BQU0sRUFBRSxLQVJMO0FBU0hDLE1BQUFBLGVBQWUsRUFBRSxLQVRkO0FBVUhDLE1BQUFBLGFBQWEsRUFBRSxLQVZaO0FBV0hDLE1BQUFBLGNBQWMsRUFBRSxLQVhiO0FBWUhDLE1BQUFBLGFBQWEsRUFBRSxLQVpaO0FBYUhDLE1BQUFBLGNBQWMsRUFBRSxJQWJiO0FBY0hDLE1BQUFBLGNBQWMsRUFBRUMseUJBQWdCQyxpQkFBaEIsR0FBb0NDO0FBZGpELEtBQVA7QUFnQkgsR0ExQjJCO0FBNEI1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLQyxVQUFMLEdBQWtCLEtBQWxCO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQnpCLGlDQUFnQkMsR0FBaEIsRUFBckI7O0FBQ0EsU0FBS3dCLGFBQUwsQ0FBbUJDLEVBQW5CLENBQXNCLG9CQUF0QixFQUE0QyxLQUFLQyxvQkFBakQ7O0FBRUEsU0FBS0MsZUFBTCxDQUFxQixLQUFLMUcsS0FBTCxDQUFXSSxPQUFoQyxFQUF5QyxJQUF6Qzs7QUFFQSxTQUFLdUcsY0FBTCxHQUFzQjVELG9CQUFJNkQsUUFBSixDQUFhLEtBQUtDLFNBQWxCLENBQXRCO0FBQ0EsU0FBS0MscUJBQUwsR0FBNkJaLHlCQUFnQkMsaUJBQWhCLEdBQW9DWSxXQUFwQyxDQUFnRCxLQUFLQyx3QkFBckQsQ0FBN0I7QUFDSCxHQXJDMkI7QUF1QzVCQyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFNBQUtYLFVBQUwsR0FBa0IsSUFBbEI7O0FBQ0EsU0FBS0MsYUFBTCxDQUFtQlcsY0FBbkIsQ0FBa0Msb0JBQWxDLEVBQXdELEtBQUtULG9CQUE3RDs7QUFDQTFELHdCQUFJb0UsVUFBSixDQUFlLEtBQUtSLGNBQXBCLEVBSDZCLENBSzdCOzs7QUFDQSxRQUFJLEtBQUtHLHFCQUFULEVBQWdDO0FBQzVCLFdBQUtBLHFCQUFMLENBQTJCTSxNQUEzQjtBQUNIO0FBQ0osR0FoRDJCO0FBa0Q1QjtBQUNBQyxFQUFBQSxnQ0FBZ0MsRUFBRSxVQUFTQyxRQUFULEVBQW1CO0FBQ2pELFFBQUksS0FBS3RILEtBQUwsQ0FBV0ksT0FBWCxLQUF1QmtILFFBQVEsQ0FBQ2xILE9BQXBDLEVBQTZDO0FBQ3pDLFdBQUttSCxRQUFMLENBQWM7QUFDVmpDLFFBQUFBLE9BQU8sRUFBRSxJQURDO0FBRVYvQixRQUFBQSxLQUFLLEVBQUU7QUFGRyxPQUFkLEVBR0csTUFBTTtBQUNMLGFBQUttRCxlQUFMLENBQXFCWSxRQUFRLENBQUNsSCxPQUE5QjtBQUNILE9BTEQ7QUFNSDtBQUNKLEdBNUQyQjtBQThENUI0RyxFQUFBQSx3QkFBd0IsRUFBRSxZQUFXO0FBQ2pDLFNBQUtPLFFBQUwsQ0FBYztBQUNWdEIsTUFBQUEsY0FBYyxFQUFFQyx5QkFBZ0JDLGlCQUFoQixHQUFvQ0M7QUFEMUMsS0FBZDtBQUdILEdBbEUyQjtBQW9FNUJLLEVBQUFBLG9CQUFvQixFQUFFLFVBQVNlLEtBQVQsRUFBZ0I7QUFDbEMsUUFBSSxLQUFLbEIsVUFBTCxJQUFtQmtCLEtBQUssQ0FBQ3BILE9BQU4sS0FBa0IsS0FBS0osS0FBTCxDQUFXSSxPQUFwRCxFQUE2RDs7QUFDN0QsUUFBSW9ILEtBQUssQ0FBQ0MsWUFBTixLQUF1QixPQUEzQixFQUFvQztBQUNoQztBQUNBLFdBQUtDLGNBQUw7QUFDSDs7QUFDRCxTQUFLSCxRQUFMLENBQWM7QUFBQ3pCLE1BQUFBLGNBQWMsRUFBRTtBQUFqQixLQUFkO0FBQ0gsR0EzRTJCO0FBNkU1QlksRUFBQUEsZUFBZSxFQUFFLFVBQVN0RyxPQUFULEVBQWtCdUgsU0FBbEIsRUFBNkI7QUFDMUMsVUFBTUgsS0FBSyxHQUFHLEtBQUtqQixhQUFMLENBQW1CcUIsUUFBbkIsQ0FBNEJ4SCxPQUE1QixDQUFkOztBQUNBLFFBQUlvSCxLQUFLLElBQUlBLEtBQUssQ0FBQ0ssT0FBZixJQUEwQkwsS0FBSyxDQUFDSyxPQUFOLENBQWNDLE1BQTVDLEVBQW9EO0FBQ2hELFdBQUtDLG9CQUFMLENBQTBCUCxLQUFLLENBQUNLLE9BQU4sQ0FBY0MsTUFBeEM7QUFDSDs7QUFDRG5HLHdCQUFXcUcsZ0JBQVgsQ0FBNEI1SCxPQUE1QixFQUFxQyxLQUFLNkgsbUJBQUwsQ0FBeUJDLElBQXpCLENBQThCLElBQTlCLEVBQW9DUCxTQUFwQyxDQUFyQzs7QUFDQSxRQUFJUSxnQkFBZ0IsR0FBRyxLQUF2QixDQU4wQyxDQU8xQzs7QUFDQXhHLHdCQUFXNkUsRUFBWCxDQUFjLE9BQWQsRUFBdUIsQ0FBQ25ELEdBQUQsRUFBTStFLFlBQU4sRUFBb0JDLFFBQXBCLEtBQWlDO0FBQ3BELFVBQUksS0FBSy9CLFVBQUwsSUFBbUJsRyxPQUFPLEtBQUtnSSxZQUFuQyxFQUFpRDs7QUFDakQsVUFBSS9FLEdBQUcsQ0FBQ2lGLE9BQUosS0FBZ0IsMEJBQWhCLElBQThDLENBQUNILGdCQUFuRCxFQUFxRTtBQUNqRXBGLDRCQUFJQyxRQUFKLENBQWE7QUFDVEMsVUFBQUEsTUFBTSxFQUFFLHdCQURDO0FBRVRzRixVQUFBQSxlQUFlLEVBQUU7QUFDYnRGLFlBQUFBLE1BQU0sRUFBRSxZQURLO0FBRWJ1RixZQUFBQSxRQUFRLEVBQUVwSTtBQUZHO0FBRlIsU0FBYjs7QUFPQTJDLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFLHNCQUFUO0FBQWlDd0YsVUFBQUEsWUFBWSxFQUFFO0FBQUNDLFlBQUFBLE1BQU0sa0JBQVd0SSxPQUFYO0FBQVA7QUFBL0MsU0FBYjs7QUFDQStILFFBQUFBLGdCQUFnQixHQUFHLElBQW5CO0FBQ0g7O0FBQ0QsVUFBSUUsUUFBUSxLQUFLMUcsb0JBQVdnSCxTQUFYLENBQXFCQyxPQUF0QyxFQUErQztBQUMzQyxhQUFLckIsUUFBTCxDQUFjO0FBQ1ZqQyxVQUFBQSxPQUFPLEVBQUUsSUFEQztBQUVWL0IsVUFBQUEsS0FBSyxFQUFFRixHQUZHO0FBR1ZoRCxVQUFBQSxPQUFPLEVBQUU7QUFIQyxTQUFkO0FBS0g7QUFDSixLQXBCRDtBQXFCSCxHQTFHMkI7O0FBNEc1QjRILEVBQUFBLG1CQUFtQixDQUFDTixTQUFELEVBQVk7QUFDM0IsUUFBSSxLQUFLckIsVUFBVCxFQUFxQjs7QUFDckIsVUFBTWhCLE9BQU8sR0FBRzNELG9CQUFXa0gsVUFBWCxDQUFzQixLQUFLN0ksS0FBTCxDQUFXSSxPQUFqQyxDQUFoQjs7QUFDQSxRQUFJa0YsT0FBTyxDQUFDakcsT0FBWixFQUFxQjtBQUNqQjtBQUNBO0FBQ0EsT0FBQyxZQUFELEVBQWUsa0JBQWYsRUFBbUMsTUFBbkMsRUFBMkMsbUJBQTNDLEVBQWdFeUosT0FBaEUsQ0FBeUVDLENBQUQsSUFBTztBQUMzRXpELFFBQUFBLE9BQU8sQ0FBQ2pHLE9BQVIsQ0FBZ0IwSixDQUFoQixJQUFxQnpELE9BQU8sQ0FBQ2pHLE9BQVIsQ0FBZ0IwSixDQUFoQixLQUFzQixFQUEzQztBQUNILE9BRkQ7QUFHSDs7QUFDRCxTQUFLeEIsUUFBTCxDQUFjO0FBQ1ZqQyxNQUFBQSxPQURVO0FBRVYwRCxNQUFBQSxjQUFjLEVBQUUsQ0FBQ3JILG9CQUFXc0gsWUFBWCxDQUF3QixLQUFLakosS0FBTCxDQUFXSSxPQUFuQyxFQUE0Q3VCLG9CQUFXZ0gsU0FBWCxDQUFxQkMsT0FBakUsQ0FGUDtBQUdWckQsTUFBQUEsaUJBQWlCLEVBQUU1RCxvQkFBV3VILGlCQUFYLENBQTZCLEtBQUtsSixLQUFMLENBQVdJLE9BQXhDLENBSFQ7QUFJVm9GLE1BQUFBLGdCQUFnQixFQUFFN0Qsb0JBQVc2RCxnQkFBWCxDQUE0QixLQUFLeEYsS0FBTCxDQUFXSSxPQUF2QyxDQUpSO0FBS1ZxRixNQUFBQSxVQUFVLEVBQUU5RCxvQkFBV3dILGFBQVgsQ0FBeUIsS0FBS25KLEtBQUwsQ0FBV0ksT0FBcEMsQ0FMRjtBQU1Wc0YsTUFBQUEsaUJBQWlCLEVBQUUsQ0FBQy9ELG9CQUFXc0gsWUFBWCxDQUF3QixLQUFLakosS0FBTCxDQUFXSSxPQUFuQyxFQUE0Q3VCLG9CQUFXZ0gsU0FBWCxDQUFxQlMsVUFBakUsQ0FOVjtBQU9WQyxNQUFBQSxZQUFZLEVBQUUxSCxvQkFBVzJILGVBQVgsQ0FBMkIsS0FBS3RKLEtBQUwsQ0FBV0ksT0FBdEMsRUFBK0NtSixJQUEvQyxDQUNUQyxDQUFELElBQU9BLENBQUMsQ0FBQzFCLE1BQUYsS0FBYSxLQUFLdkIsYUFBTCxDQUFtQmtELFdBQW5CLENBQStCM0IsTUFEekM7QUFQSixLQUFkLEVBVjJCLENBcUIzQjs7QUFDQSxRQUFJLEtBQUs5SCxLQUFMLENBQVdvRixVQUFYLElBQXlCdUMsU0FBN0IsRUFBd0M7QUFDcEMsV0FBSytCLFlBQUw7QUFDSDtBQUNKLEdBckkyQjs7QUF1STVCM0IsRUFBQUEsb0JBQW9CLENBQUNELE1BQUQsRUFBUztBQUN6QixTQUFLUCxRQUFMLENBQWM7QUFDVm9DLE1BQUFBLGtCQUFrQixFQUFFO0FBRFYsS0FBZDs7QUFHQSxTQUFLcEQsYUFBTCxDQUFtQnFELGNBQW5CLENBQWtDOUIsTUFBbEMsRUFBMEM5RixJQUExQyxDQUFnRDZILElBQUQsSUFBVTtBQUNyRCxVQUFJLEtBQUt2RCxVQUFULEVBQXFCO0FBQ3JCLFdBQUtpQixRQUFMLENBQWM7QUFDVnZCLFFBQUFBLGNBQWMsRUFBRTtBQUNacEMsVUFBQUEsU0FBUyxFQUFFaUcsSUFBSSxDQUFDdEssVUFESjtBQUVaUSxVQUFBQSxXQUFXLEVBQUU4SixJQUFJLENBQUNoSztBQUZOO0FBRE4sT0FBZDtBQU1ILEtBUkQsRUFRR2lDLEtBUkgsQ0FRVWUsQ0FBRCxJQUFPO0FBQ1pTLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLHFDQUFkLEVBQXFEVixDQUFyRDtBQUNILEtBVkQsRUFVR2lILE9BVkgsQ0FVVyxNQUFNO0FBQ2IsVUFBSSxLQUFLeEQsVUFBVCxFQUFxQjtBQUNyQixXQUFLaUIsUUFBTCxDQUFjO0FBQ1ZvQyxRQUFBQSxrQkFBa0IsRUFBRTtBQURWLE9BQWQ7QUFHSCxLQWZEO0FBZ0JILEdBM0oyQjs7QUE2SjVCRCxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQixTQUFLbkMsUUFBTCxDQUFjO0FBQ1ZsSCxNQUFBQSxPQUFPLEVBQUUsSUFEQztBQUVWMEosTUFBQUEsV0FBVyxFQUFFQyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtDLEtBQUwsQ0FBVzVFLE9BQVgsQ0FBbUJqRyxPQUFyQyxDQUZIO0FBR1Y4SyxNQUFBQSxZQUFZLEVBQUU7QUFDVkMsUUFBQUEsVUFBVSxFQUNOLEtBQUtGLEtBQUwsQ0FBVzVFLE9BQVgsQ0FBbUJqRyxPQUFuQixDQUEyQmdMLGtCQUEzQixHQUNJcEYscUJBREosR0FFSUM7QUFKRTtBQUhKLEtBQWQ7QUFVSCxHQXhLMkI7QUEwSzVCb0YsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEIsVUFBTUMsV0FBVyxHQUFHNUosR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLHdCQUExQixFQUFvRCxFQUFwRCxFQUF3RHlKLFdBQXhELEVBQXFFO0FBQ2pFQyxNQUFBQSxNQUFNLEVBQUUsS0FBS2pFLGFBQUwsQ0FBbUJxQixRQUFuQixDQUE0QixLQUFLNUgsS0FBTCxDQUFXSSxPQUF2QyxLQUFtRCxJQUFJcUssa0JBQUosQ0FBVSxLQUFLekssS0FBTCxDQUFXSSxPQUFyQjtBQURNLEtBQXJFO0FBR0gsR0EvSzJCO0FBaUw1QnNLLEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCLFNBQUtoRCxjQUFMO0FBQ0gsR0FuTDJCOztBQXFMNUJiLEVBQUFBLFNBQVMsQ0FBQzhELE9BQUQsRUFBVTtBQUNmLFlBQVFBLE9BQU8sQ0FBQzFILE1BQWhCO0FBQ0k7QUFDQSxXQUFLLGdCQUFMO0FBQ0ksYUFBS3NFLFFBQUwsQ0FBYztBQUNWbEgsVUFBQUEsT0FBTyxFQUFFLEtBREM7QUFFVjBKLFVBQUFBLFdBQVcsRUFBRTtBQUZILFNBQWQ7QUFJQTs7QUFDSjtBQUNJO0FBVFI7QUFXSCxHQWpNMkI7O0FBbU01QnJDLEVBQUFBLGNBQWMsR0FBRztBQUNiM0Usd0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxNQUFBQSxNQUFNLEVBQUU7QUFBVCxLQUFiO0FBQ0gsR0FyTTJCOztBQXVNNUIySCxFQUFBQSxhQUFhLEVBQUUsVUFBU0MsS0FBVCxFQUFnQjtBQUMzQixVQUFNQyxjQUFjLEdBQUdkLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEtBQUtDLEtBQUwsQ0FBV0gsV0FBekIsRUFBc0M7QUFBRXpLLE1BQUFBLElBQUksRUFBRXVMO0FBQVIsS0FBdEMsQ0FBdkI7QUFDQSxTQUFLdEQsUUFBTCxDQUFjO0FBQ1Z3QyxNQUFBQSxXQUFXLEVBQUVlO0FBREgsS0FBZDtBQUdILEdBNU0yQjtBQThNNUJDLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNGLEtBQVQsRUFBZ0I7QUFDaEMsVUFBTUMsY0FBYyxHQUFHZCxNQUFNLENBQUNDLE1BQVAsQ0FBYyxLQUFLQyxLQUFMLENBQVdILFdBQXpCLEVBQXNDO0FBQUVpQixNQUFBQSxpQkFBaUIsRUFBRUg7QUFBckIsS0FBdEMsQ0FBdkI7QUFDQSxTQUFLdEQsUUFBTCxDQUFjO0FBQ1Z3QyxNQUFBQSxXQUFXLEVBQUVlO0FBREgsS0FBZDtBQUdILEdBbk4yQjtBQXFONUJHLEVBQUFBLGlCQUFpQixFQUFFLFVBQVNwSSxDQUFULEVBQVk7QUFDM0IsVUFBTWlJLGNBQWMsR0FBR2QsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBS0MsS0FBTCxDQUFXSCxXQUF6QixFQUFzQztBQUFFbUIsTUFBQUEsZ0JBQWdCLEVBQUVySSxDQUFDLENBQUMySCxNQUFGLENBQVNLO0FBQTdCLEtBQXRDLENBQXZCO0FBQ0EsU0FBS3RELFFBQUwsQ0FBYztBQUNWd0MsTUFBQUEsV0FBVyxFQUFFZTtBQURILEtBQWQ7QUFHSCxHQTFOMkI7QUE0TjVCSyxFQUFBQSxpQkFBaUIsRUFBRSxVQUFTM0ssRUFBVCxFQUFhO0FBQzVCLFVBQU00SyxJQUFJLEdBQUc1SyxFQUFFLENBQUNnSyxNQUFILENBQVVhLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBYjtBQUNBLFFBQUksQ0FBQ0QsSUFBTCxFQUFXO0FBRVgsU0FBSzdELFFBQUwsQ0FBYztBQUFDM0IsTUFBQUEsZUFBZSxFQUFFO0FBQWxCLEtBQWQ7O0FBQ0EsU0FBS1csYUFBTCxDQUFtQitFLGFBQW5CLENBQWlDRixJQUFqQyxFQUF1Q3BKLElBQXZDLENBQTZDdUosR0FBRCxJQUFTO0FBQ2pELFlBQU1ULGNBQWMsR0FBR2QsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBS0MsS0FBTCxDQUFXSCxXQUF6QixFQUFzQztBQUFFeEssUUFBQUEsVUFBVSxFQUFFZ007QUFBZCxPQUF0QyxDQUF2QjtBQUNBLFdBQUtoRSxRQUFMLENBQWM7QUFDVjNCLFFBQUFBLGVBQWUsRUFBRSxLQURQO0FBRVZtRSxRQUFBQSxXQUFXLEVBQUVlLGNBRkg7QUFJVjtBQUNBO0FBQ0FqRixRQUFBQSxhQUFhLEVBQUU7QUFOTCxPQUFkO0FBUUgsS0FWRCxFQVVHL0QsS0FWSCxDQVVVZSxDQUFELElBQU87QUFDWixXQUFLMEUsUUFBTCxDQUFjO0FBQUMzQixRQUFBQSxlQUFlLEVBQUU7QUFBbEIsT0FBZDtBQUNBLFlBQU0xRCxXQUFXLEdBQUd2QixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EwQyxNQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYywrQkFBZCxFQUErQ1YsQ0FBL0M7O0FBQ0FoQyxxQkFBTUMsbUJBQU4sQ0FBMEIsd0JBQTFCLEVBQW9ELEVBQXBELEVBQXdEb0IsV0FBeEQsRUFBcUU7QUFDakVuQixRQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUQwRDtBQUVqRUMsUUFBQUEsV0FBVyxFQUFFLHlCQUFHLHdCQUFIO0FBRm9ELE9BQXJFO0FBSUgsS0FsQkQ7QUFtQkgsR0FwUDJCO0FBc1A1QndLLEVBQUFBLGlCQUFpQixFQUFFLFVBQVNoTCxFQUFULEVBQWE7QUFDNUIsU0FBSytHLFFBQUwsQ0FBYztBQUNWNEMsTUFBQUEsWUFBWSxFQUFFO0FBQUVDLFFBQUFBLFVBQVUsRUFBRTVKLEVBQUUsQ0FBQ2dLLE1BQUgsQ0FBVUs7QUFBeEI7QUFESixLQUFkO0FBR0gsR0ExUDJCO0FBNFA1QlksRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckIsU0FBS2xFLFFBQUwsQ0FBYztBQUFDNUIsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBZDtBQUNBLFVBQU0rRixXQUFXLEdBQUcsS0FBS3hCLEtBQUwsQ0FBVzFFLGdCQUFYLEdBQThCLEtBQUttRyxVQUFMLEVBQTlCLEdBQWtEQyxPQUFPLENBQUNDLE9BQVIsRUFBdEU7QUFDQUgsSUFBQUEsV0FBVyxDQUFDMUosSUFBWixDQUFrQjhKLE1BQUQsSUFBWTtBQUN6QixXQUFLdkUsUUFBTCxDQUFjO0FBQ1Y1QixRQUFBQSxNQUFNLEVBQUUsS0FERTtBQUVWdEYsUUFBQUEsT0FBTyxFQUFFLEtBRkM7QUFHVmlGLFFBQUFBLE9BQU8sRUFBRTtBQUhDLE9BQWQ7O0FBS0F2QywwQkFBSUMsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRTtBQUFULE9BQWI7O0FBQ0EsV0FBS3lELGVBQUwsQ0FBcUIsS0FBSzFHLEtBQUwsQ0FBV0ksT0FBaEM7O0FBRUEsVUFBSSxLQUFLOEosS0FBTCxDQUFXckUsYUFBZixFQUE4QjtBQUMxQjtBQUNBa0csNEJBQVdDLG1CQUFYLENBQStCLEtBQUt6RixhQUFwQyxFQUFtRCxLQUFLdkcsS0FBTCxDQUFXSSxPQUE5RDtBQUNIO0FBQ0osS0FiRCxFQWFHMEIsS0FiSCxDQWFVZSxDQUFELElBQU87QUFDWixXQUFLMEUsUUFBTCxDQUFjO0FBQ1Y1QixRQUFBQSxNQUFNLEVBQUU7QUFERSxPQUFkO0FBR0EsWUFBTXpELFdBQVcsR0FBR3ZCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQTBDLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLGtDQUFkLEVBQWtEVixDQUFsRDs7QUFDQWhDLHFCQUFNQyxtQkFBTixDQUEwQix3QkFBMUIsRUFBb0QsRUFBcEQsRUFBd0RvQixXQUF4RCxFQUFxRTtBQUNqRW5CLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRDBEO0FBRWpFQyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsNEJBQUg7QUFGb0QsT0FBckU7QUFJSCxLQXZCRCxFQXVCRzhJLE9BdkJILENBdUJXLE1BQU07QUFDYixXQUFLdkMsUUFBTCxDQUFjO0FBQ1YxQixRQUFBQSxhQUFhLEVBQUU7QUFETCxPQUFkO0FBR0gsS0EzQkQ7QUE0QkgsR0EzUjJCO0FBNlI1QjhGLEVBQUFBLFVBQVUsRUFBRSxrQkFBaUI7QUFDekIsVUFBTSxLQUFLcEYsYUFBTCxDQUFtQjBGLGVBQW5CLENBQW1DLEtBQUtqTSxLQUFMLENBQVdJLE9BQTlDLEVBQXVELEtBQUs4SixLQUFMLENBQVdILFdBQWxFLENBQU47QUFDQSxVQUFNLEtBQUt4RCxhQUFMLENBQW1CMkYsa0JBQW5CLENBQXNDLEtBQUtsTSxLQUFMLENBQVdJLE9BQWpELEVBQTBEO0FBQzVEK0wsTUFBQUEsSUFBSSxFQUFFLEtBQUtqQyxLQUFMLENBQVdDLFlBQVgsQ0FBd0JDO0FBRDhCLEtBQTFELENBQU47QUFHSCxHQWxTMkI7QUFvUzVCZ0MsRUFBQUEsb0JBQW9CLEVBQUUsa0JBQWlCO0FBQ25DLFNBQUs3RSxRQUFMLENBQWM7QUFBQ3pCLE1BQUFBLGNBQWMsRUFBRTtBQUFqQixLQUFkLEVBRG1DLENBR25DO0FBQ0E7O0FBQ0EsVUFBTSxvQkFBTSxHQUFOLENBQU47O0FBRUFuRSx3QkFBVzBLLGlCQUFYLENBQTZCLEtBQUtyTSxLQUFMLENBQVdJLE9BQXhDLEVBQWlENEIsSUFBakQsQ0FBc0QsTUFBTSxDQUN4RDtBQUNILEtBRkQsRUFFR0YsS0FGSCxDQUVVZSxDQUFELElBQU87QUFDWixXQUFLMEUsUUFBTCxDQUFjO0FBQUN6QixRQUFBQSxjQUFjLEVBQUU7QUFBakIsT0FBZDtBQUNBLFlBQU01RCxXQUFXLEdBQUd2QixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsd0JBQTFCLEVBQW9ELEVBQXBELEVBQXdEb0IsV0FBeEQsRUFBcUU7QUFDakVuQixRQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUQwRDtBQUVqRUMsUUFBQUEsV0FBVyxFQUFFLHlCQUFHLHlCQUFIO0FBRm9ELE9BQXJFO0FBSUgsS0FURDtBQVVILEdBclQyQjtBQXVUNUJzTCxFQUFBQSxvQkFBb0IsRUFBRSxrQkFBaUI7QUFDbkMsU0FBSy9FLFFBQUwsQ0FBYztBQUFDekIsTUFBQUEsY0FBYyxFQUFFO0FBQWpCLEtBQWQsRUFEbUMsQ0FHbkM7QUFDQTs7QUFDQSxVQUFNLG9CQUFNLEdBQU4sQ0FBTjs7QUFFQW5FLHdCQUFXNEssVUFBWCxDQUFzQixLQUFLdk0sS0FBTCxDQUFXSSxPQUFqQyxFQUEwQzRCLElBQTFDLENBQStDLE1BQU0sQ0FDakQ7QUFDSCxLQUZELEVBRUdGLEtBRkgsQ0FFVWUsQ0FBRCxJQUFPO0FBQ1osV0FBSzBFLFFBQUwsQ0FBYztBQUFDekIsUUFBQUEsY0FBYyxFQUFFO0FBQWpCLE9BQWQ7QUFDQSxZQUFNNUQsV0FBVyxHQUFHdkIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQTBCLHdCQUExQixFQUFvRCxFQUFwRCxFQUF3RG9CLFdBQXhELEVBQXFFO0FBQ2pFbkIsUUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FEMEQ7QUFFakVDLFFBQUFBLFdBQVcsRUFBRSx5QkFBRyx5QkFBSDtBQUZvRCxPQUFyRTtBQUlILEtBVEQ7QUFVSCxHQXhVMkI7QUEwVTVCd0wsRUFBQUEsWUFBWSxFQUFFLGtCQUFpQjtBQUMzQixRQUFJLEtBQUtqRyxhQUFMLENBQW1Ca0csT0FBbkIsRUFBSixFQUFrQztBQUM5QjFKLDBCQUFJQyxRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFLHNCQUFUO0FBQWlDd0YsUUFBQUEsWUFBWSxFQUFFO0FBQUNDLFVBQUFBLE1BQU0sa0JBQVcsS0FBSzFJLEtBQUwsQ0FBV0ksT0FBdEI7QUFBUDtBQUEvQyxPQUFiOztBQUNBO0FBQ0g7O0FBRUQsU0FBS21ILFFBQUwsQ0FBYztBQUFDekIsTUFBQUEsY0FBYyxFQUFFO0FBQWpCLEtBQWQsRUFOMkIsQ0FRM0I7QUFDQTs7QUFDQSxVQUFNLG9CQUFNLEdBQU4sQ0FBTjs7QUFFQW5FLHdCQUFXK0ssU0FBWCxDQUFxQixLQUFLMU0sS0FBTCxDQUFXSSxPQUFoQyxFQUF5QzRCLElBQXpDLENBQThDLE1BQU0sQ0FDaEQ7QUFDSCxLQUZELEVBRUdGLEtBRkgsQ0FFVWUsQ0FBRCxJQUFPO0FBQ1osV0FBSzBFLFFBQUwsQ0FBYztBQUFDekIsUUFBQUEsY0FBYyxFQUFFO0FBQWpCLE9BQWQ7QUFDQSxZQUFNNUQsV0FBVyxHQUFHdkIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQTBCLG9CQUExQixFQUFnRCxFQUFoRCxFQUFvRG9CLFdBQXBELEVBQWlFO0FBQzdEbkIsUUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FEc0Q7QUFFN0RDLFFBQUFBLFdBQVcsRUFBRSx5QkFBRywwQkFBSDtBQUZnRCxPQUFqRTtBQUlILEtBVEQ7QUFVSCxHQWhXMkI7QUFrVzVCMkwsRUFBQUEsbUJBQW1CLEVBQUUsWUFBVztBQUM1QixVQUFNQyxRQUFRLEdBQUcsRUFBakI7O0FBRUEsUUFBSSxLQUFLMUMsS0FBTCxDQUFXMUUsZ0JBQWYsRUFBaUM7QUFDN0JvSCxNQUFBQSxRQUFRLENBQUM3SyxJQUFULENBQ0k7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUNNO0FBQUk7QUFEVixRQUVNLHlCQUFHLGlFQUNBLDhEQURILENBRk4sQ0FESjtBQU9IOztBQUVELFdBQU82SyxRQUFQO0FBQ0gsR0FoWDJCO0FBbVg1QkMsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEIsVUFBTUMsY0FBYyxHQUFHbk0sR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQSxVQUFNZ00sUUFBUSxHQUFHLEtBQUtELG1CQUFMLEVBQWpCOztBQUVBOUwsbUJBQU1DLG1CQUFOLENBQTBCLGFBQTFCLEVBQXlDLEVBQXpDLEVBQTZDZ00sY0FBN0MsRUFBNkQ7QUFDekQvTCxNQUFBQSxLQUFLLEVBQUUseUJBQUcsaUJBQUgsQ0FEa0Q7QUFFekRDLE1BQUFBLFdBQVcsRUFDUCwyQ0FDRSx5QkFBRyxzQkFBSCxFQUEyQjtBQUFDK0wsUUFBQUEsU0FBUyxFQUFFLEtBQUsvTSxLQUFMLENBQVdJO0FBQXZCLE9BQTNCLENBREYsRUFFRXdNLFFBRkYsQ0FIcUQ7QUFRekQxTCxNQUFBQSxNQUFNLEVBQUUseUJBQUcsT0FBSCxDQVJpRDtBQVN6RDhMLE1BQUFBLE1BQU0sRUFBRSxLQUFLOUMsS0FBTCxDQUFXMUUsZ0JBVHNDO0FBVXpEbkUsTUFBQUEsVUFBVSxFQUFFLE1BQU80TCxTQUFQLElBQXFCO0FBQzdCLFlBQUksQ0FBQ0EsU0FBTCxFQUFnQjtBQUVoQixhQUFLMUYsUUFBTCxDQUFjO0FBQUN6QixVQUFBQSxjQUFjLEVBQUU7QUFBakIsU0FBZCxFQUg2QixDQUs3QjtBQUNBOztBQUNBLGNBQU0sb0JBQU0sR0FBTixDQUFOOztBQUVBbkUsNEJBQVc0SyxVQUFYLENBQXNCLEtBQUt2TSxLQUFMLENBQVdJLE9BQWpDLEVBQTBDNEIsSUFBMUMsQ0FBK0MsTUFBTSxDQUNqRDtBQUNILFNBRkQsRUFFR0YsS0FGSCxDQUVVZSxDQUFELElBQU87QUFDWixlQUFLMEUsUUFBTCxDQUFjO0FBQUN6QixZQUFBQSxjQUFjLEVBQUU7QUFBakIsV0FBZDtBQUNBLGdCQUFNNUQsV0FBVyxHQUFHdkIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMseUJBQU1DLG1CQUFOLENBQTBCLHlCQUExQixFQUFxRCxFQUFyRCxFQUF5RG9CLFdBQXpELEVBQXNFO0FBQ2xFbkIsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FEMkQ7QUFFbEVDLFlBQUFBLFdBQVcsRUFBRSx5QkFBRywyQkFBSDtBQUZxRCxXQUF0RTtBQUlILFNBVEQ7QUFVSDtBQTdCd0QsS0FBN0Q7QUErQkgsR0F0WjJCO0FBd1o1QmtNLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsb0RBQXVCLEtBQUtsTixLQUFMLENBQVdJLE9BQWxDO0FBQ0gsR0ExWjJCO0FBNFo1QitNLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsVUFBTUMsMkJBQTJCLEdBQUcseUJBQVc7QUFDM0MsNEJBQXNCLEtBQUtsRCxLQUFMLENBQVc3SixPQURVO0FBRTNDLHFDQUErQixLQUFLNkosS0FBTCxDQUFXN0osT0FBWCxJQUFzQixDQUFDLEtBQUs2SixLQUFMLENBQVcxRTtBQUZ0QixLQUFYLENBQXBDO0FBS0EsVUFBTTZILE1BQU0sR0FBRyxLQUFLbkQsS0FBTCxDQUFXN0osT0FBWCxHQUFxQiw4Q0FBTyx5QkFBRyxvQkFBSCxDQUFQLE1BQXJCLEdBQStELHlDQUE5RTtBQUVBLFVBQU1pTixpQkFBaUIsR0FBRyxpQ0FBZSxvQkFBZixDQUExQjtBQUNBLFFBQUlDLGFBQWEsR0FBRyxJQUFwQjs7QUFDQSxRQUFJRCxpQkFBaUIsSUFBSSxLQUFLcEQsS0FBTCxDQUFXMUUsZ0JBQXBDLEVBQXNEO0FBQ2xEK0gsTUFBQUEsYUFBYSxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNYLHlCQUNHLHdEQURILEVBQzZELEVBRDdELEVBRUc7QUFDSUMsUUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUk7QUFBRyxVQUFBLElBQUksRUFBRUgsaUJBQVQ7QUFBNEIsVUFBQSxNQUFNLEVBQUMsUUFBbkM7QUFBNEMsVUFBQSxHQUFHLEVBQUM7QUFBaEQsV0FBdUVHLEdBQXZFO0FBRGQsT0FGSCxDQURXLEVBT1o7QUFBRyxRQUFBLElBQUksRUFBRUgsaUJBQVQ7QUFBNEIsUUFBQSxNQUFNLEVBQUMsUUFBbkM7QUFBNEMsUUFBQSxHQUFHLEVBQUM7QUFBaEQsU0FDSTtBQUFLLFFBQUEsR0FBRyxFQUFFL0ssT0FBTyxDQUFDLG9DQUFELENBQWpCO0FBQXlELFFBQUEsS0FBSyxFQUFDLElBQS9EO0FBQW9FLFFBQUEsTUFBTSxFQUFDLElBQTNFO0FBQWdGLFFBQUEsR0FBRyxFQUFDO0FBQXBGLFFBREosQ0FQWSxDQUFoQjtBQVdIOztBQUVELFVBQU1tTCxrQkFBa0IsR0FBRyxLQUFLeEQsS0FBTCxDQUFXN0osT0FBWCxJQUFzQixLQUFLNkosS0FBTCxDQUFXMUUsZ0JBQWpDLEdBQ3ZCO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUNFLGtGQUNBLHdEQUZGLEVBR0UsRUFIRixFQUlFO0FBQ0ksZUFBVWlJLEdBQUQsSUFBUyw2Q0FBTUEsR0FBTixNQUR0QjtBQUVJLGVBQVVBLEdBQUQsSUFBUyw2Q0FBTUEsR0FBTjtBQUZ0QixLQUpGLENBRE4sQ0FEdUIsR0FXZCx5Q0FYYjtBQVlBLFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBRUw7QUFBaEIsT0FDREMsTUFEQyxFQUVERSxhQUZDLEVBR0RHLGtCQUhDLEVBSUQsS0FBS0MsZ0JBQUwsRUFKQyxFQUtELEtBQUtDLHVCQUFMLEVBTEMsRUFNRCxLQUFLQyxhQUFMLEVBTkMsQ0FBUDtBQVFILEdBeGMyQjtBQTBjNUJBLEVBQUFBLGFBQWEsRUFBRSxZQUFXO0FBQ3RCLFVBQU1DLGNBQWMsR0FBR25OLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBdkI7QUFDQSxVQUFNbU4sZ0JBQWdCLEdBQUdwTixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTXlCLFdBQVcsR0FBRzFCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBcEI7QUFDQSxVQUFNb04sT0FBTyxHQUFHck4sR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFVBQU1xTixhQUFhLEdBQUd0TixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBRUEsVUFBTXNOLGFBQWEsR0FBRyxLQUFLaEUsS0FBTCxDQUFXN0osT0FBWCxHQUFxQiw2QkFBQyxhQUFEO0FBQWUsTUFBQSxRQUFRLEVBQzlELHlCQUNJLDJFQUNBLDJEQUZKO0FBRHVDLE1BQXJCLEdBS2YseUNBTFA7QUFPQSxVQUFNOE4sVUFBVSxHQUFHLEtBQUtqRSxLQUFMLENBQVc3SixPQUFYLEdBQ2QsNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsa0NBQTVCO0FBQ0csTUFBQSxPQUFPLEVBQUUsS0FBSzZNO0FBRGpCLE9BR0c7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsV0FBRDtBQUFhLE1BQUEsR0FBRyxFQUFFM0ssT0FBTyxDQUFDLHFDQUFELENBQXpCO0FBQWtFLE1BQUEsS0FBSyxFQUFDLElBQXhFO0FBQTZFLE1BQUEsTUFBTSxFQUFDO0FBQXBGLE1BREosQ0FISCxFQU1HO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLDZCQUFILENBRE4sQ0FOSCxDQURjLEdBVVEseUNBVjNCO0FBV0EsVUFBTTZMLHVCQUF1QixHQUFHLHlCQUFXO0FBQ3ZDLG9CQUFjLElBRHlCO0FBRXZDLDBCQUFvQixLQUFLbEUsS0FBTCxDQUFXN0o7QUFGUSxLQUFYLENBQWhDO0FBSUEsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx5Q0FDTSx5QkFBRyxPQUFILENBRE4sRUFFTTZOLGFBRk4sQ0FESixFQUtNQyxVQUxOLENBREcsRUFRRCxLQUFLakUsS0FBTCxDQUFXeEUsaUJBQVgsR0FDRSw2QkFBQyxPQUFELE9BREYsR0FFRSw2QkFBQyxjQUFEO0FBQ0ksTUFBQSxLQUFLLEVBQUUsS0FBS3dFLEtBQUwsQ0FBV3pFLFVBRHRCO0FBRUksTUFBQSxTQUFTLEVBQUUySTtBQUZmLE1BVkQsQ0FBUDtBQWVILEdBdGYyQjtBQXdmNUJDLEVBQUFBLHFCQUFxQixFQUFFLFlBQVc7QUFDOUIsVUFBTS9JLE9BQU8sR0FBRyxLQUFLNEUsS0FBTCxDQUFXNUUsT0FBM0I7QUFFQSxVQUFNZ0osb0JBQW9CLEdBQUcsRUFBN0I7QUFDQSxVQUFNQyxhQUFhLEdBQUcsRUFBdEI7QUFDQWpKLElBQUFBLE9BQU8sQ0FBQ2tKLGFBQVIsQ0FBc0J2TyxLQUF0QixDQUE0QjZJLE9BQTVCLENBQXFDckcsQ0FBRCxJQUFPO0FBQ3ZDLFVBQUlBLENBQUMsQ0FBQ2dNLFdBQUYsS0FBa0IsSUFBdEIsRUFBNEI7QUFDeEJILFFBQUFBLG9CQUFvQixDQUFDdk0sSUFBckIsQ0FBMEJVLENBQTFCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsWUFBSWlNLElBQUksR0FBR0gsYUFBYSxDQUFDOUwsQ0FBQyxDQUFDZ00sV0FBSCxDQUF4Qjs7QUFDQSxZQUFJQyxJQUFJLEtBQUtDLFNBQWIsRUFBd0I7QUFDcEJELFVBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0FILFVBQUFBLGFBQWEsQ0FBQzlMLENBQUMsQ0FBQ2dNLFdBQUgsQ0FBYixHQUErQkMsSUFBL0I7QUFDSDs7QUFDREEsUUFBQUEsSUFBSSxDQUFDM00sSUFBTCxDQUFVVSxDQUFWO0FBQ0g7QUFDSixLQVhEOztBQWFBLFVBQU1tTSxtQkFBbUIsR0FBRyw2QkFBQyxnQkFBRDtBQUN4QixNQUFBLEtBQUssRUFBRU4sb0JBRGlCO0FBRXhCLE1BQUEsT0FBTyxFQUFFLEtBQUt0TyxLQUFMLENBQVdJLE9BRkk7QUFHeEIsTUFBQSxPQUFPLEVBQUUsS0FBSzhKLEtBQUwsQ0FBVzdKO0FBSEksTUFBNUI7O0FBSUEsVUFBTXdPLGlCQUFpQixHQUFHN0UsTUFBTSxDQUFDOEUsSUFBUCxDQUFZUCxhQUFaLEVBQTJCOU0sR0FBM0IsQ0FBZ0NzTixLQUFELElBQVc7QUFDaEUsWUFBTUMsR0FBRyxHQUFHMUosT0FBTyxDQUFDa0osYUFBUixDQUFzQlMsVUFBdEIsQ0FBaUNGLEtBQWpDLENBQVo7QUFDQSxhQUFPLDZCQUFDLGdCQUFEO0FBQ0gsUUFBQSxHQUFHLEVBQUVBLEtBREY7QUFFSCxRQUFBLEtBQUssRUFBRVIsYUFBYSxDQUFDUSxLQUFELENBRmpCO0FBR0gsUUFBQSxRQUFRLEVBQUVDLEdBSFA7QUFJSCxRQUFBLE9BQU8sRUFBRSxLQUFLaFAsS0FBTCxDQUFXSSxPQUpqQjtBQUtILFFBQUEsT0FBTyxFQUFFLEtBQUs4SixLQUFMLENBQVc3SjtBQUxqQixRQUFQO0FBTUgsS0FSeUIsQ0FBMUI7QUFVQSxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLGlCQUFILENBRE4sQ0FERyxFQUlEdU8sbUJBSkMsRUFLREMsaUJBTEMsQ0FBUDtBQU9ILEdBL2hCMkI7QUFpaUI1QkssRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixVQUFNNUosT0FBTyxHQUFHLEtBQUs0RSxLQUFMLENBQVc1RSxPQUEzQjtBQUVBLFVBQU02SixXQUFXLEdBQUcsRUFBcEI7QUFDQSxVQUFNQyxTQUFTLEdBQUcsRUFBbEI7QUFDQTlKLElBQUFBLE9BQU8sQ0FBQytKLGFBQVIsQ0FBc0JwTCxLQUF0QixDQUE0QjZFLE9BQTVCLENBQXFDdkUsQ0FBRCxJQUFPO0FBQ3ZDLFVBQUlBLENBQUMsQ0FBQzNFLE9BQUYsS0FBYyxJQUFsQixFQUF3QjtBQUNwQnVQLFFBQUFBLFdBQVcsQ0FBQ3BOLElBQVosQ0FBaUJ3QyxDQUFqQjtBQUNILE9BRkQsTUFFTztBQUNILFlBQUltSyxJQUFJLEdBQUdVLFNBQVMsQ0FBQzdLLENBQUMsQ0FBQzNFLE9BQUgsQ0FBcEI7O0FBQ0EsWUFBSThPLElBQUksS0FBS0MsU0FBYixFQUF3QjtBQUNwQkQsVUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDQVUsVUFBQUEsU0FBUyxDQUFDN0ssQ0FBQyxDQUFDM0UsT0FBSCxDQUFULEdBQXVCOE8sSUFBdkI7QUFDSDs7QUFDREEsUUFBQUEsSUFBSSxDQUFDM00sSUFBTCxDQUFVd0MsQ0FBVjtBQUNIO0FBQ0osS0FYRDs7QUFhQSxVQUFNK0ssVUFBVSxHQUFHLDZCQUFDLFlBQUQ7QUFDZixNQUFBLEtBQUssRUFBRUgsV0FEUTtBQUVmLE1BQUEsT0FBTyxFQUFFLEtBQUtuUCxLQUFMLENBQVdJLE9BRkw7QUFHZixNQUFBLE9BQU8sRUFBRSxLQUFLOEosS0FBTCxDQUFXN0o7QUFITCxNQUFuQjs7QUFJQSxVQUFNa1AsYUFBYSxHQUFHdkYsTUFBTSxDQUFDOEUsSUFBUCxDQUFZTSxTQUFaLEVBQXVCM04sR0FBdkIsQ0FBNEIrTixNQUFELElBQVk7QUFDekQsWUFBTXRMLElBQUksR0FBR29CLE9BQU8sQ0FBQytKLGFBQVIsQ0FBc0JJLEtBQXRCLENBQTRCRCxNQUE1QixDQUFiO0FBQ0EsYUFBTyw2QkFBQyxZQUFEO0FBQ0gsUUFBQSxHQUFHLEVBQUVBLE1BREY7QUFFSCxRQUFBLEtBQUssRUFBRUosU0FBUyxDQUFDSSxNQUFELENBRmI7QUFHSCxRQUFBLElBQUksRUFBRXRMLElBSEg7QUFJSCxRQUFBLE9BQU8sRUFBRSxLQUFLbEUsS0FBTCxDQUFXSSxPQUpqQjtBQUtILFFBQUEsT0FBTyxFQUFFLEtBQUs4SixLQUFMLENBQVc3SjtBQUxqQixRQUFQO0FBTUgsS0FScUIsQ0FBdEI7QUFVQSxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLGlCQUFILENBRE4sQ0FERyxFQUlEaVAsVUFKQyxFQUtEQyxhQUxDLENBQVA7QUFPSCxHQXhrQjJCO0FBMGtCNUJHLEVBQUFBLHFCQUFxQixFQUFFLFlBQVc7QUFDOUIsVUFBTTFCLE9BQU8sR0FBR3JOLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxVQUFNK0QsVUFBVSxHQUFHaEUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjs7QUFFQSxVQUFNNEcsS0FBSyxHQUFHLEtBQUtqQixhQUFMLENBQW1CcUIsUUFBbkIsQ0FBNEIsS0FBSzVILEtBQUwsQ0FBV0ksT0FBdkMsQ0FBZDs7QUFFQSxRQUFJb0gsS0FBSyxJQUFJQSxLQUFLLENBQUNDLFlBQU4sS0FBdUIsUUFBcEMsRUFBOEM7QUFDMUMsVUFBSSxLQUFLeUMsS0FBTCxDQUFXcEUsY0FBWCxJQUE2QixLQUFLb0UsS0FBTCxDQUFXUCxrQkFBNUMsRUFBZ0U7QUFDNUQsZUFBTztBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDSCw2QkFBQyxPQUFELE9BREcsQ0FBUDtBQUdIOztBQUNELFlBQU1nRyxpQkFBaUIsR0FBRyxLQUFLekYsS0FBTCxDQUFXbEUsY0FBWCxHQUN0QixLQUFLTyxhQUFMLENBQW1CdkIsWUFBbkIsQ0FDSSxLQUFLa0YsS0FBTCxDQUFXbEUsY0FBWCxDQUEwQnBDLFNBRDlCLEVBQ3lDLEVBRHpDLEVBQzZDLEVBRDdDLENBRHNCLEdBR2xCLElBSFI7QUFLQSxVQUFJZ00sV0FBVyxHQUFHcEksS0FBSyxDQUFDSyxPQUFOLENBQWNDLE1BQWhDOztBQUNBLFVBQUksS0FBS29DLEtBQUwsQ0FBV2xFLGNBQWYsRUFBK0I7QUFDM0I0SixRQUFBQSxXQUFXLEdBQUcsS0FBSzFGLEtBQUwsQ0FBV2xFLGNBQVgsQ0FBMEJqRyxXQUExQixJQUF5Q3lILEtBQUssQ0FBQ0ssT0FBTixDQUFjQyxNQUFyRTtBQUNIOztBQUNELGFBQU87QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0g7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsVUFBRDtBQUFZLFFBQUEsR0FBRyxFQUFFNkgsaUJBQWpCO0FBQ0ksUUFBQSxJQUFJLEVBQUVDLFdBRFY7QUFFSSxRQUFBLEtBQUssRUFBRSxFQUZYO0FBR0ksUUFBQSxNQUFNLEVBQUU7QUFIWixRQURKLEVBTU0seUJBQUcsb0RBQUgsRUFBeUQ7QUFDdkQvSCxRQUFBQSxPQUFPLEVBQUUrSDtBQUQ4QyxPQUF6RCxDQU5OLENBREosRUFXSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxrREFBNUI7QUFDSSxRQUFBLE9BQU8sRUFBRSxLQUFLeEQ7QUFEbEIsU0FHTSx5QkFBRyxRQUFILENBSE4sQ0FESixFQU1JLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsU0FBUyxFQUFDLGtEQUE1QjtBQUNJLFFBQUEsT0FBTyxFQUFFLEtBQUtFO0FBRGxCLFNBR00seUJBQUcsU0FBSCxDQUhOLENBTkosQ0FYSixDQURHLENBQVA7QUEwQkg7O0FBRUQsUUFBSXVELCtCQUFKO0FBQ0EsUUFBSUMsNEJBQUo7QUFDQSxRQUFJQyx1QkFBSjtBQUNBLFFBQUlDLG9CQUFKO0FBQ0EsUUFBSUMsdUJBQUosQ0FyRDhCLENBdUQ5Qjs7QUFDQSxRQUFJLENBQUMsQ0FBQ3pJLEtBQUQsSUFBVUEsS0FBSyxDQUFDQyxZQUFOLEtBQXVCLE9BQWxDLEtBQ0EsS0FBS3lDLEtBQUwsQ0FBVzVFLE9BRFgsSUFFQSxLQUFLNEUsS0FBTCxDQUFXNUUsT0FBWCxDQUFtQmpHLE9BRm5CLElBR0E2USxPQUFPLENBQUMsS0FBS2hHLEtBQUwsQ0FBVzVFLE9BQVgsQ0FBbUJqRyxPQUFuQixDQUEyQmdMLGtCQUE1QixDQUhYLEVBSUU7QUFDRTJGLE1BQUFBLG9CQUFvQixHQUFHLHlCQUFHLHFCQUFILENBQXZCO0FBQ0FDLE1BQUFBLHVCQUF1QixHQUFHLEtBQUt6RCxZQUEvQjtBQUVBc0QsTUFBQUEsNEJBQTRCLEdBQUcseUJBQS9CO0FBQ0FELE1BQUFBLCtCQUErQixHQUFHLHNDQUFsQztBQUNILEtBVkQsTUFVTyxJQUNIckksS0FBSyxJQUNMQSxLQUFLLENBQUNDLFlBQU4sS0FBdUIsTUFEdkIsSUFFQSxLQUFLeUMsS0FBTCxDQUFXN0osT0FIUixFQUlMO0FBQ0UyUCxNQUFBQSxvQkFBb0IsR0FBRyx5QkFBRyxzQkFBSCxDQUF2QjtBQUNBQyxNQUFBQSx1QkFBdUIsR0FBRyxLQUFLcEQsYUFBL0I7QUFDQWtELE1BQUFBLHVCQUF1QixHQUFHLEtBQUs3RixLQUFMLENBQVcxRSxnQkFBWCxHQUN0Qix5QkFBRyw0Q0FBSCxDQURzQixHQUV0Qix5QkFBRyxvQ0FBSCxDQUZKO0FBSUFzSyxNQUFBQSw0QkFBNEIsR0FBRztBQUMzQixvQ0FBNEIsSUFERDtBQUUzQiwyQ0FBbUMsS0FBSzVGLEtBQUwsQ0FBVzFFO0FBRm5CLE9BQS9CO0FBSUFxSyxNQUFBQSwrQkFBK0IsR0FBRyx1Q0FBbEM7QUFDSCxLQWhCTSxNQWdCQTtBQUNILGFBQU8sSUFBUDtBQUNIOztBQUVELFVBQU1NLHVCQUF1QixHQUFHLHlCQUFXLENBQ3ZDLDBCQUR1QyxFQUV2Qyx5QkFGdUMsQ0FBWCxFQUk1QkwsNEJBSjRCLENBQWhDO0FBT0EsVUFBTU0sMEJBQTBCLEdBQUcseUJBQy9CLGdDQUQrQixFQUUvQlAsK0JBRitCLENBQW5DO0FBS0EsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFFTztBQUFoQixPQUNIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUVNLEtBQUtsRyxLQUFMLENBQVdwRSxjQUFYLEdBQTRCLDZCQUFDLE9BQUQsT0FBNUIsR0FBMEMseUNBRmhELEVBR0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMseUJBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBRXFLLHVCQURmO0FBRUksTUFBQSxPQUFPLEVBQUVGLHVCQUZiO0FBR0ksTUFBQSxLQUFLLEVBQUVGO0FBSFgsT0FLTUMsb0JBTE4sQ0FESixDQUhKLENBREcsQ0FBUDtBQWVILEdBM3JCMkI7QUE2ckI1QnJDLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsVUFBTTBDLGFBQWEsR0FBRzFQLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7QUFDQSxXQUFPLEtBQUtzSixLQUFMLENBQVc3SixPQUFYLEdBQXFCLDBDQUN4Qix5Q0FDTSx5QkFBRyw4QkFBSCxDQUROLEVBRU0sS0FBSzZKLEtBQUwsQ0FBV29HLG9CQUFYLEdBQ0UsNkJBQUMsYUFBRCxPQURGLEdBQ3NCLHlDQUg1QixDQUR3QixFQU94QiwwQ0FDSSw0Q0FDSTtBQUFPLE1BQUEsSUFBSSxFQUFDLE9BQVo7QUFDSSxNQUFBLEtBQUssRUFBRXBMLHVCQURYO0FBRUksTUFBQSxPQUFPLEVBQUUsS0FBS2dGLEtBQUwsQ0FBV0MsWUFBWCxDQUF3QkMsVUFBeEIsS0FBdUNsRix1QkFGcEQ7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLc0c7QUFIbkIsTUFESixFQU1JO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLG1DQUFILENBRE4sQ0FOSixDQURKLENBUHdCLEVBbUJ4QiwwQ0FDSSw0Q0FDSTtBQUFPLE1BQUEsSUFBSSxFQUFDLE9BQVo7QUFDSSxNQUFBLEtBQUssRUFBRXZHLHFCQURYO0FBRUksTUFBQSxPQUFPLEVBQUUsS0FBS2lGLEtBQUwsQ0FBV0MsWUFBWCxDQUF3QkMsVUFBeEIsS0FBdUNuRixxQkFGcEQ7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLdUc7QUFIbkIsTUFESixFQU1JO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLHlCQUFHLFVBQUgsQ0FETixDQU5KLENBREosQ0FuQndCLENBQXJCLEdBK0JFLElBL0JUO0FBZ0NILEdBL3RCMkI7QUFpdUI1Qm9DLEVBQUFBLHVCQUF1QixFQUFFLFlBQVc7QUFDaEMsVUFBTXRJLE9BQU8sR0FBRyxLQUFLNEUsS0FBTCxDQUFXNUUsT0FBM0I7QUFDQSxRQUFJdEUsV0FBVyxHQUFHLElBQWxCOztBQUNBLFFBQUlzRSxPQUFPLENBQUNqRyxPQUFSLElBQW1CaUcsT0FBTyxDQUFDakcsT0FBUixDQUFnQjZMLGdCQUF2QyxFQUF5RDtBQUNyRGxLLE1BQUFBLFdBQVcsR0FBRyxrQ0FBa0JzRSxPQUFPLENBQUNqRyxPQUFSLENBQWdCNkwsZ0JBQWxDLENBQWQ7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLaEIsS0FBTCxDQUFXMUUsZ0JBQWYsRUFBaUM7QUFDcEN4RSxNQUFBQSxXQUFXLEdBQUc7QUFDVixRQUFBLFNBQVMsRUFBQyxvQ0FEQTtBQUVWLFFBQUEsT0FBTyxFQUFFLEtBQUswSTtBQUZKLFNBSVIseUJBQ0UsbUdBQ0EsOENBRkYsRUFHRSxFQUhGLEVBSUU7QUFBRSxjQUFNO0FBQVIsT0FKRixDQUpRLENBQWQ7QUFXSDs7QUFDRCxVQUFNNkcsdUJBQXVCLEdBQUcseUJBQVc7QUFDdkMsZ0NBQTBCLElBRGE7QUFFdkMseUNBQW1DLENBQUMsS0FBS3JHLEtBQUwsQ0FBVzFFO0FBRlIsS0FBWCxDQUFoQztBQUtBLFdBQU8sS0FBSzBFLEtBQUwsQ0FBVzdKLE9BQVgsR0FDSDtBQUFLLE1BQUEsU0FBUyxFQUFFa1E7QUFBaEIsT0FDSSw4Q0FBTyx5QkFBRyx5QkFBSCxDQUFQLE1BREosRUFFSTtBQUNJLE1BQUEsS0FBSyxFQUFFLEtBQUtyRyxLQUFMLENBQVdILFdBQVgsQ0FBdUJtQixnQkFEbEM7QUFFSSxNQUFBLFdBQVcsRUFBRSx5QkFBR3BNLHFCQUFILENBRmpCO0FBR0ksTUFBQSxRQUFRLEVBQUUsS0FBS21NLGlCQUhuQjtBQUlJLE1BQUEsUUFBUSxFQUFDLEdBSmI7QUFLSSxNQUFBLEdBQUcsRUFBQztBQUxSLE1BRkosQ0FERyxHQVdIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNakssV0FETixDQVhKO0FBY0gsR0F0d0IyQjtBQXd3QjVCb0IsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNb08sV0FBVyxHQUFHN1AsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBLFVBQU1vTixPQUFPLEdBQUdyTixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCOztBQUVBLFFBQUksS0FBS3NKLEtBQUwsQ0FBV2xCLGNBQVgsSUFBNkIsS0FBS2tCLEtBQUwsQ0FBVzNHLEtBQVgsS0FBcUIsSUFBbEQsSUFBMEQsS0FBSzJHLEtBQUwsQ0FBV3ZFLE1BQXpFLEVBQWlGO0FBQzdFLGFBQU8sNkJBQUMsT0FBRCxPQUFQO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBS3VFLEtBQUwsQ0FBVzVFLE9BQVgsSUFBc0IsQ0FBQyxLQUFLNEUsS0FBTCxDQUFXM0csS0FBdEMsRUFBNkM7QUFDaEQsWUFBTStCLE9BQU8sR0FBRyxLQUFLNEUsS0FBTCxDQUFXNUUsT0FBM0I7QUFFQSxVQUFJbUwsVUFBSjtBQUNBLFVBQUlDLFFBQUo7QUFDQSxVQUFJQyxhQUFKO0FBQ0EsWUFBTUMsWUFBWSxHQUFHLEVBQXJCOztBQUNBLFVBQUksS0FBSzFHLEtBQUwsQ0FBVzdKLE9BQVgsSUFBc0IsS0FBSzZKLEtBQUwsQ0FBVzFFLGdCQUFyQyxFQUF1RDtBQUNuRCxZQUFJcUwsV0FBSjs7QUFDQSxZQUFJLEtBQUszRyxLQUFMLENBQVd0RSxlQUFmLEVBQWdDO0FBQzVCaUwsVUFBQUEsV0FBVyxHQUFHLDZCQUFDLE9BQUQsT0FBZDtBQUNILFNBRkQsTUFFTztBQUNILGdCQUFNTCxXQUFXLEdBQUc3UCxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0FpUSxVQUFBQSxXQUFXLEdBQUcsNkJBQUMsV0FBRDtBQUFhLFlBQUEsT0FBTyxFQUFFLEtBQUs3USxLQUFMLENBQVdJLE9BQWpDO0FBQ1YsWUFBQSxTQUFTLEVBQUUsS0FBSzhKLEtBQUwsQ0FBV0gsV0FBWCxDQUF1QnpLLElBRHhCO0FBRVYsWUFBQSxjQUFjLEVBQUUsS0FBSzRLLEtBQUwsQ0FBV0gsV0FBWCxDQUF1QnhLLFVBRjdCO0FBR1YsWUFBQSxLQUFLLEVBQUUsRUFIRztBQUdDLFlBQUEsTUFBTSxFQUFFLEVBSFQ7QUFHYSxZQUFBLFlBQVksRUFBQztBQUgxQixZQUFkO0FBS0g7O0FBRURrUixRQUFBQSxVQUFVLEdBQ047QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0k7QUFBTyxVQUFBLE9BQU8sRUFBQyxhQUFmO0FBQTZCLFVBQUEsU0FBUyxFQUFDO0FBQXZDLFdBQ01JLFdBRE4sQ0FESixFQUlJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNJO0FBQU8sVUFBQSxPQUFPLEVBQUMsYUFBZjtBQUE2QixVQUFBLFNBQVMsRUFBQztBQUF2QyxXQUNJO0FBQUssVUFBQSxHQUFHLEVBQUV0TyxPQUFPLENBQUMsNkJBQUQsQ0FBakI7QUFDSSxVQUFBLEdBQUcsRUFBRSx5QkFBRyxlQUFILENBRFQ7QUFDOEIsVUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSCxDQURyQztBQUVJLFVBQUEsS0FBSyxFQUFDLElBRlY7QUFFZSxVQUFBLE1BQU0sRUFBQztBQUZ0QixVQURKLENBREosRUFNSTtBQUFPLFVBQUEsRUFBRSxFQUFDLGFBQVY7QUFBd0IsVUFBQSxTQUFTLEVBQUMsMEJBQWxDO0FBQTZELFVBQUEsSUFBSSxFQUFDLE1BQWxFO0FBQXlFLFVBQUEsUUFBUSxFQUFFLEtBQUs0STtBQUF4RixVQU5KLENBSkosQ0FESjtBQWdCQSxjQUFNMkYsWUFBWSxHQUFHblEsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFyQjtBQUVBOFAsUUFBQUEsUUFBUSxHQUFHLDZCQUFDLFlBQUQ7QUFDUCxVQUFBLFNBQVMsRUFBQyx1QkFESDtBQUVQLFVBQUEsb0JBQW9CLEVBQUMsMEJBRmQ7QUFHUCxVQUFBLFdBQVcsRUFBRSx5QkFBRyxnQkFBSCxDQUhOO0FBSVAsVUFBQSxZQUFZLEVBQUUsS0FKUDtBQUtQLFVBQUEsWUFBWSxFQUFFLEtBQUt4RyxLQUFMLENBQVdILFdBQVgsQ0FBdUJ6SyxJQUw5QjtBQU1QLFVBQUEsY0FBYyxFQUFFLEtBQUtzTCxhQU5kO0FBT1AsVUFBQSxRQUFRLEVBQUMsR0FQRjtBQVFQLFVBQUEsR0FBRyxFQUFDO0FBUkcsVUFBWDtBQVVBK0YsUUFBQUEsYUFBYSxHQUFHLDZCQUFDLFlBQUQ7QUFDWixVQUFBLFNBQVMsRUFBQyx1QkFERTtBQUVaLFVBQUEsb0JBQW9CLEVBQUMsMEJBRlQ7QUFHWixVQUFBLFdBQVcsRUFBRSx5QkFBRyxhQUFILENBSEQ7QUFJWixVQUFBLFlBQVksRUFBRSxLQUpGO0FBS1osVUFBQSxZQUFZLEVBQUUsS0FBS3pHLEtBQUwsQ0FBV0gsV0FBWCxDQUF1QmlCLGlCQUx6QjtBQU1aLFVBQUEsY0FBYyxFQUFFLEtBQUtELGtCQU5UO0FBT1osVUFBQSxRQUFRLEVBQUMsR0FQRztBQVFaLFVBQUEsR0FBRyxFQUFDO0FBUlEsVUFBaEI7QUFTSCxPQWxERCxNQWtETztBQUNILGNBQU1nRyxzQkFBc0IsR0FBRyxLQUFLN0csS0FBTCxDQUFXYixZQUFYLEdBQTBCLEtBQUtLLFlBQS9CLEdBQThDLElBQTdFO0FBQ0EsY0FBTXNILGNBQWMsR0FBRzFMLE9BQU8sQ0FBQ2pHLE9BQVIsR0FBa0JpRyxPQUFPLENBQUNqRyxPQUFSLENBQWdCRSxVQUFsQyxHQUErQyxJQUF0RTtBQUNBLGNBQU13TixTQUFTLEdBQUd6SCxPQUFPLENBQUNqRyxPQUFSLEdBQWtCaUcsT0FBTyxDQUFDakcsT0FBUixDQUFnQkMsSUFBbEMsR0FBeUMsSUFBM0Q7QUFDQW1SLFFBQUFBLFVBQVUsR0FBRyw2QkFBQyxXQUFEO0FBQ1QsVUFBQSxPQUFPLEVBQUUsS0FBS3pRLEtBQUwsQ0FBV0ksT0FEWDtBQUVULFVBQUEsY0FBYyxFQUFFNFEsY0FGUDtBQUdULFVBQUEsU0FBUyxFQUFFakUsU0FIRjtBQUlULFVBQUEsT0FBTyxFQUFFZ0Usc0JBSkE7QUFLVCxVQUFBLEtBQUssRUFBRSxFQUxFO0FBS0UsVUFBQSxNQUFNLEVBQUU7QUFMVixVQUFiOztBQU9BLFlBQUl6TCxPQUFPLENBQUNqRyxPQUFSLElBQW1CaUcsT0FBTyxDQUFDakcsT0FBUixDQUFnQkMsSUFBdkMsRUFBNkM7QUFDekNvUixVQUFBQSxRQUFRLEdBQUc7QUFBSyxZQUFBLE9BQU8sRUFBRUs7QUFBZCxhQUNQLDJDQUFRekwsT0FBTyxDQUFDakcsT0FBUixDQUFnQkMsSUFBeEIsQ0FETyxFQUVQO0FBQU0sWUFBQSxTQUFTLEVBQUM7QUFBaEIsa0JBQ08sS0FBS1UsS0FBTCxDQUFXSSxPQURsQixNQUZPLENBQVg7QUFNSCxTQVBELE1BT087QUFDSHNRLFVBQUFBLFFBQVEsR0FBRztBQUFNLFlBQUEsT0FBTyxFQUFFSztBQUFmLGFBQXlDLEtBQUsvUSxLQUFMLENBQVdJLE9BQXBELENBQVg7QUFDSDs7QUFDRCxZQUFJa0YsT0FBTyxDQUFDakcsT0FBUixJQUFtQmlHLE9BQU8sQ0FBQ2pHLE9BQVIsQ0FBZ0IyTCxpQkFBdkMsRUFBMEQ7QUFDdEQyRixVQUFBQSxhQUFhLEdBQUc7QUFBTSxZQUFBLE9BQU8sRUFBRUk7QUFBZixhQUF5Q3pMLE9BQU8sQ0FBQ2pHLE9BQVIsQ0FBZ0IyTCxpQkFBekQsQ0FBaEI7QUFDSDtBQUNKOztBQUVELFVBQUksS0FBS2QsS0FBTCxDQUFXN0osT0FBZixFQUF3QjtBQUNwQnVRLFFBQUFBLFlBQVksQ0FBQzdPLElBQWIsQ0FDSSw2QkFBQyx5QkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyxrREFBNUI7QUFDSSxVQUFBLEdBQUcsRUFBQyxhQURSO0FBRUksVUFBQSxPQUFPLEVBQUUsS0FBSzBKO0FBRmxCLFdBSU0seUJBQUcsTUFBSCxDQUpOLENBREo7QUFRQW1GLFFBQUFBLFlBQVksQ0FBQzdPLElBQWIsQ0FDSSw2QkFBQyx5QkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyw0QkFBNUI7QUFDSSxVQUFBLEdBQUcsRUFBQyxlQURSO0FBRUksVUFBQSxPQUFPLEVBQUUsS0FBSzJJO0FBRmxCLFdBSUk7QUFBSyxVQUFBLEdBQUcsRUFBRW5JLE9BQU8sQ0FBQyw2QkFBRCxDQUFqQjtBQUFrRCxVQUFBLFNBQVMsRUFBQyxvQkFBNUQ7QUFDSSxVQUFBLEtBQUssRUFBQyxJQURWO0FBQ2UsVUFBQSxNQUFNLEVBQUMsSUFEdEI7QUFDMkIsVUFBQSxHQUFHLEVBQUUseUJBQUcsUUFBSDtBQURoQyxVQUpKLENBREo7QUFTSCxPQWxCRCxNQWtCTztBQUNILFlBQUkrQyxPQUFPLENBQUMyTCxJQUFSLElBQWdCM0wsT0FBTyxDQUFDMkwsSUFBUixDQUFhQyxVQUFiLEtBQTRCLE1BQWhELEVBQXdEO0FBQ3BETixVQUFBQSxZQUFZLENBQUM3TyxJQUFiLENBQ0ksNkJBQUMseUJBQUQ7QUFBa0IsWUFBQSxTQUFTLEVBQUMsaURBQTVCO0FBQ0ksWUFBQSxHQUFHLEVBQUMsYUFEUjtBQUVJLFlBQUEsT0FBTyxFQUFFLEtBQUsySCxZQUZsQjtBQUdJLFlBQUEsS0FBSyxFQUFFLHlCQUFHLG9CQUFIO0FBSFgsWUFESjtBQVFIOztBQUNEa0gsUUFBQUEsWUFBWSxDQUFDN08sSUFBYixDQUNJLDZCQUFDLHlCQUFEO0FBQWtCLFVBQUEsU0FBUyxFQUFDLGtEQUE1QjtBQUNJLFVBQUEsR0FBRyxFQUFDLGNBRFI7QUFFSSxVQUFBLE9BQU8sRUFBRSxLQUFLdUksYUFGbEI7QUFHSSxVQUFBLEtBQUssRUFBRSx5QkFBRyxpQkFBSDtBQUhYLFVBREo7QUFRSDs7QUFFRCxZQUFNNkcsVUFBVSxHQUFHLEtBQUtqSCxLQUFMLENBQVdqRSxjQUFYLEdBQTRCLDZCQUFDLG1CQUFEO0FBQVksUUFBQSxPQUFPLEVBQUUsS0FBS2pHLEtBQUwsQ0FBV0k7QUFBaEMsUUFBNUIsR0FBMEV1TyxTQUE3RjtBQUVBLFlBQU15QyxhQUFhLEdBQUc7QUFDbEIsK0JBQXVCLElBREw7QUFFbEIsdUJBQWUsSUFGRztBQUdsQixvQ0FBNEIsQ0FBQyxLQUFLbEgsS0FBTCxDQUFXN0osT0FIdEI7QUFJbEIsNENBQW9DLEtBQUs2SixLQUFMLENBQVdiO0FBSjdCLE9BQXRCO0FBT0EsYUFDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBRSx5QkFBVytILGFBQVg7QUFBaEIsU0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDTVgsVUFETixDQURKLEVBSUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ01DLFFBRE4sQ0FESixFQUlJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNQyxhQUROLENBSkosQ0FKSixDQURKLEVBY0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ01DLFlBRE4sQ0FkSixFQWlCSSw2QkFBQywyQkFBRCxPQWpCSixDQURKLEVBb0JJLDZCQUFDLGtCQUFEO0FBQVcsUUFBQSxLQUFLLEVBQUVPO0FBQWxCLFNBQ0ksNkJBQUMsMEJBQUQ7QUFBbUIsUUFBQSxTQUFTLEVBQUM7QUFBN0IsU0FDTSxLQUFLekIscUJBQUwsRUFETixFQUVNLEtBQUt2QyxnQkFBTCxFQUZOLENBREosQ0FwQkosQ0FESjtBQTZCSCxLQWhLTSxNQWdLQSxJQUFJLEtBQUtqRCxLQUFMLENBQVczRyxLQUFmLEVBQXNCO0FBQ3pCLFVBQUksS0FBSzJHLEtBQUwsQ0FBVzNHLEtBQVgsQ0FBaUI4TixVQUFqQixLQUFnQyxHQUFwQyxFQUF5QztBQUNyQyxlQUNJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNNLHlCQUFHLGlDQUFILEVBQXNDO0FBQUNqUixVQUFBQSxPQUFPLEVBQUUsS0FBS0osS0FBTCxDQUFXSTtBQUFyQixTQUF0QyxDQUROLENBREo7QUFLSCxPQU5ELE1BTU87QUFDSCxZQUFJa1IsU0FBSjs7QUFDQSxZQUFJLEtBQUtwSCxLQUFMLENBQVczRyxLQUFYLENBQWlCK0UsT0FBakIsS0FBNkIsZ0JBQWpDLEVBQW1EO0FBQy9DZ0osVUFBQUEsU0FBUyxHQUFHLDBDQUFPLHlCQUFHLDhDQUFILENBQVAsQ0FBWjtBQUNIOztBQUNELGVBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ00seUJBQUcsNEJBQUgsRUFBaUM7QUFBQ2xSLFVBQUFBLE9BQU8sRUFBRSxLQUFLSixLQUFMLENBQVdJO0FBQXJCLFNBQWpDLENBRE4sRUFFTWtSLFNBRk4sQ0FESjtBQU1IO0FBQ0osS0FuQk0sTUFtQkE7QUFDSGhPLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLDZCQUFkO0FBQ0EsYUFBTyx5Q0FBUDtBQUNIO0FBQ0o7QUFyOEIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGQuXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4IE5ldyBWZWN0b3IgTHRkLlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgeyBnZXRIb3N0aW5nTGluayB9IGZyb20gJy4uLy4uL3V0aWxzL0hvc3RpbmdMaW5rJztcclxuaW1wb3J0IHsgc2FuaXRpemVkSHRtbE5vZGUgfSBmcm9tICcuLi8uLi9IdG1sVXRpbHMnO1xyXG5pbXBvcnQgeyBfdCwgX3RkIH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSAnLi4vdmlld3MvZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCBHcm91cEhlYWRlckJ1dHRvbnMgZnJvbSAnLi4vdmlld3MvcmlnaHRfcGFuZWwvR3JvdXBIZWFkZXJCdXR0b25zJztcclxuaW1wb3J0IE1haW5TcGxpdCBmcm9tICcuL01haW5TcGxpdCc7XHJcbmltcG9ydCBSaWdodFBhbmVsIGZyb20gJy4vUmlnaHRQYW5lbCc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xyXG5cclxuaW1wb3J0IEdyb3VwU3RvcmUgZnJvbSAnLi4vLi4vc3RvcmVzL0dyb3VwU3RvcmUnO1xyXG5pbXBvcnQgRmxhaXJTdG9yZSBmcm9tICcuLi8uLi9zdG9yZXMvRmxhaXJTdG9yZSc7XHJcbmltcG9ydCB7IHNob3dHcm91cEFkZFJvb21EaWFsb2cgfSBmcm9tICcuLi8uLi9Hcm91cEFkZHJlc3NQaWNrZXInO1xyXG5pbXBvcnQge21ha2VHcm91cFBlcm1hbGluaywgbWFrZVVzZXJQZXJtYWxpbmt9IGZyb20gXCIuLi8uLi91dGlscy9wZXJtYWxpbmtzL1Blcm1hbGlua3NcIjtcclxuaW1wb3J0IHtHcm91cH0gZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0IHthbGxTZXR0bGVkLCBzbGVlcH0gZnJvbSBcIi4uLy4uL3V0aWxzL3Byb21pc2VcIjtcclxuaW1wb3J0IFJpZ2h0UGFuZWxTdG9yZSBmcm9tIFwiLi4vLi4vc3RvcmVzL1JpZ2h0UGFuZWxTdG9yZVwiO1xyXG5pbXBvcnQgQXV0b0hpZGVTY3JvbGxiYXIgZnJvbSBcIi4vQXV0b0hpZGVTY3JvbGxiYXJcIjtcclxuXHJcbmNvbnN0IExPTkdfREVTQ19QTEFDRUhPTERFUiA9IF90ZChcclxuYDxoMT5IVE1MIGZvciB5b3VyIGNvbW11bml0eSdzIHBhZ2U8L2gxPlxyXG48cD5cclxuICAgIFVzZSB0aGUgbG9uZyBkZXNjcmlwdGlvbiB0byBpbnRyb2R1Y2UgbmV3IG1lbWJlcnMgdG8gdGhlIGNvbW11bml0eSwgb3IgZGlzdHJpYnV0ZVxyXG4gICAgc29tZSBpbXBvcnRhbnQgPGEgaHJlZj1cImZvb1wiPmxpbmtzPC9hPlxyXG48L3A+XHJcbjxwPlxyXG4gICAgWW91IGNhbiBldmVuIHVzZSAnaW1nJyB0YWdzXHJcbjwvcD5cclxuYCk7XHJcblxyXG5jb25zdCBSb29tU3VtbWFyeVR5cGUgPSBQcm9wVHlwZXMuc2hhcGUoe1xyXG4gICAgcm9vbV9pZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgcHJvZmlsZTogUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgICAgICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGF2YXRhcl91cmw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgY2Fub25pY2FsX2FsaWFzOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgfSkuaXNSZXF1aXJlZCxcclxufSk7XHJcblxyXG5jb25zdCBVc2VyU3VtbWFyeVR5cGUgPSBQcm9wVHlwZXMuc2hhcGUoe1xyXG4gICAgc3VtbWFyeUluZm86IFByb3BUeXBlcy5zaGFwZSh7XHJcbiAgICAgICAgdXNlcl9pZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHJvbGVfaWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgYXZhdGFyX3VybDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBkaXNwbGF5bmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0pLmlzUmVxdWlyZWQsXHJcbn0pO1xyXG5cclxuY29uc3QgQ2F0ZWdvcnlSb29tTGlzdCA9IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdDYXRlZ29yeVJvb21MaXN0JyxcclxuXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHJvb21zOiBQcm9wVHlwZXMuYXJyYXlPZihSb29tU3VtbWFyeVR5cGUpLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgY2F0ZWdvcnk6IFByb3BUeXBlcy5zaGFwZSh7XHJcbiAgICAgICAgICAgIHByb2ZpbGU6IFByb3BUeXBlcy5zaGFwZSh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgICAgICB9KS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGdyb3VwSWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLy8gV2hldGhlciB0aGUgbGlzdCBzaG91bGQgYmUgZWRpdGFibGVcclxuICAgICAgICBlZGl0aW5nOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBvbkFkZFJvb21zVG9TdW1tYXJ5Q2xpY2tlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGNvbnN0IEFkZHJlc3NQaWNrZXJEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5BZGRyZXNzUGlja2VyRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0FkZCBSb29tcyB0byBHcm91cCBTdW1tYXJ5JywgJycsIEFkZHJlc3NQaWNrZXJEaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KCdBZGQgcm9vbXMgdG8gdGhlIGNvbW11bml0eSBzdW1tYXJ5JyksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIldoaWNoIHJvb21zIHdvdWxkIHlvdSBsaWtlIHRvIGFkZCB0byB0aGlzIHN1bW1hcnk/XCIpLFxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogX3QoXCJSb29tIG5hbWUgb3IgYWxpYXNcIiksXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJBZGQgdG8gc3VtbWFyeVwiKSxcclxuICAgICAgICAgICAgcGlja2VyVHlwZTogJ3Jvb20nLFxyXG4gICAgICAgICAgICB2YWxpZEFkZHJlc3NUeXBlczogWydteC1yb29tLWlkJ10sXHJcbiAgICAgICAgICAgIGdyb3VwSWQ6IHRoaXMucHJvcHMuZ3JvdXBJZCxcclxuICAgICAgICAgICAgb25GaW5pc2hlZDogKHN1Y2Nlc3MsIGFkZHJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXN1Y2Nlc3MpIHJldHVybjtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGVycm9yTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgYWxsU2V0dGxlZChhZGRycy5tYXAoKGFkZHIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gR3JvdXBTdG9yZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYWRkUm9vbVRvR3JvdXBTdW1tYXJ5KHRoaXMucHJvcHMuZ3JvdXBJZCwgYWRkci5hZGRyZXNzKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKCkgPT4geyBlcnJvckxpc3QucHVzaChhZGRyLmFkZHJlc3MpOyB9KTtcclxuICAgICAgICAgICAgICAgIH0pKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3JMaXN0Lmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0ZhaWxlZCB0byBhZGQgdGhlIGZvbGxvd2luZyByb29tIHRvIHRoZSBncm91cCBzdW1tYXJ5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJycsIEVycm9yRGlhbG9nLFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJGYWlsZWQgdG8gYWRkIHRoZSBmb2xsb3dpbmcgcm9vbXMgdG8gdGhlIHN1bW1hcnkgb2YgJShncm91cElkKXM6XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z3JvdXBJZDogdGhpcy5wcm9wcy5ncm91cElkfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGVycm9yTGlzdC5qb2luKFwiLCBcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9LCAvKmNsYXNzTmFtZT0qL251bGwsIC8qaXNQcmlvcml0eT0qL2ZhbHNlLCAvKmlzU3RhdGljPSovdHJ1ZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgVGludGFibGVTdmcgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVGludGFibGVTdmdcIik7XHJcbiAgICAgICAgY29uc3QgYWRkQnV0dG9uID0gdGhpcy5wcm9wcy5lZGl0aW5nID9cclxuICAgICAgICAgICAgKDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nc19hZGRCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkFkZFJvb21zVG9TdW1tYXJ5Q2xpY2tlZH1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFRpbnRhYmxlU3ZnIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uL3Jlcy9pbWcvaWNvbnMtY3JlYXRlLXJvb20uc3ZnXCIpfSB3aWR0aD1cIjY0XCIgaGVpZ2h0PVwiNjRcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfZmVhdHVyZWRUaGluZ3NfYWRkQnV0dG9uX2xhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnQWRkIGEgUm9vbScpIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+KSA6IDxkaXYgLz47XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21Ob2RlcyA9IHRoaXMucHJvcHMucm9vbXMubWFwKChyKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiA8RmVhdHVyZWRSb29tXHJcbiAgICAgICAgICAgICAgICBrZXk9e3Iucm9vbV9pZH1cclxuICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgIGVkaXRpbmc9e3RoaXMucHJvcHMuZWRpdGluZ31cclxuICAgICAgICAgICAgICAgIHN1bW1hcnlJbmZvPXtyfSAvPjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGNhdEhlYWRlciA9IDxkaXYgLz47XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuY2F0ZWdvcnkgJiYgdGhpcy5wcm9wcy5jYXRlZ29yeS5wcm9maWxlKSB7XHJcbiAgICAgICAgICAgIGNhdEhlYWRlciA9IDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzX2NhdGVnb3J5XCI+XHJcbiAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5jYXRlZ29yeS5wcm9maWxlLm5hbWUgfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzX2NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICB7IGNhdEhlYWRlciB9XHJcbiAgICAgICAgICAgIHsgcm9vbU5vZGVzIH1cclxuICAgICAgICAgICAgeyBhZGRCdXR0b24gfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcbn0pO1xyXG5cclxuY29uc3QgRmVhdHVyZWRSb29tID0gY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0ZlYXR1cmVkUm9vbScsXHJcblxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBzdW1tYXJ5SW5mbzogUm9vbVN1bW1hcnlUeXBlLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZWRpdGluZzogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuICAgICAgICBncm91cElkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICB9LFxyXG5cclxuICAgIG9uQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgcm9vbV9hbGlhczogdGhpcy5wcm9wcy5zdW1tYXJ5SW5mby5wcm9maWxlLmNhbm9uaWNhbF9hbGlhcyxcclxuICAgICAgICAgICAgcm9vbV9pZDogdGhpcy5wcm9wcy5zdW1tYXJ5SW5mby5yb29tX2lkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkRlbGV0ZUNsaWNrZWQ6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBHcm91cFN0b3JlLnJlbW92ZVJvb21Gcm9tR3JvdXBTdW1tYXJ5KFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLmdyb3VwSWQsXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3VtbWFyeUluZm8ucm9vbV9pZCxcclxuICAgICAgICApLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igd2hpbHN0IHJlbW92aW5nIHJvb20gZnJvbSBncm91cCBzdW1tYXJ5JywgZXJyKTtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbU5hbWUgPSB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLm5hbWUgfHxcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuc3VtbWFyeUluZm8uY2Fub25pY2FsX2FsaWFzIHx8XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnJvb21faWQ7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICAgICAnRmFpbGVkIHRvIHJlbW92ZSByb29tIGZyb20gZ3JvdXAgc3VtbWFyeScsXHJcbiAgICAgICAgICAgICAgICAnJywgRXJyb3JEaWFsb2csXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkZhaWxlZCB0byByZW1vdmUgdGhlIHJvb20gZnJvbSB0aGUgc3VtbWFyeSBvZiAlKGdyb3VwSWQpc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHtncm91cElkOiB0aGlzLnByb3BzLmdyb3VwSWR9LFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIlRoZSByb29tICclKHJvb21OYW1lKXMnIGNvdWxkIG5vdCBiZSByZW1vdmVkIGZyb20gdGhlIHN1bW1hcnkuXCIsIHtyb29tTmFtZX0pLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBSb29tQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcImF2YXRhcnMuUm9vbUF2YXRhclwiKTtcclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbU5hbWUgPSB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnByb2ZpbGUubmFtZSB8fFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnByb2ZpbGUuY2Fub25pY2FsX2FsaWFzIHx8XHJcbiAgICAgICAgICAgIF90KFwiVW5uYW1lZCBSb29tXCIpO1xyXG5cclxuICAgICAgICBjb25zdCBvb2JEYXRhID0ge1xyXG4gICAgICAgICAgICByb29tSWQ6IHRoaXMucHJvcHMuc3VtbWFyeUluZm8ucm9vbV9pZCxcclxuICAgICAgICAgICAgYXZhdGFyVXJsOiB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnByb2ZpbGUuYXZhdGFyX3VybCxcclxuICAgICAgICAgICAgbmFtZTogcm9vbU5hbWUsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IHBlcm1hbGluayA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc3VtbWFyeUluZm8ucHJvZmlsZSAmJiB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnByb2ZpbGUuY2Fub25pY2FsX2FsaWFzKSB7XHJcbiAgICAgICAgICAgIHBlcm1hbGluayA9IG1ha2VHcm91cFBlcm1hbGluayh0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnByb2ZpbGUuY2Fub25pY2FsX2FsaWFzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCByb29tTmFtZU5vZGUgPSBudWxsO1xyXG4gICAgICAgIGlmIChwZXJtYWxpbmspIHtcclxuICAgICAgICAgICAgcm9vbU5hbWVOb2RlID0gPGEgaHJlZj17cGVybWFsaW5rfSBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9ID57IHJvb21OYW1lIH08L2E+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJvb21OYW1lTm9kZSA9IDxzcGFuPnsgcm9vbU5hbWUgfTwvc3Bhbj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBkZWxldGVCdXR0b24gPSB0aGlzLnByb3BzLmVkaXRpbmcgP1xyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfZmVhdHVyZWRUaGluZ19kZWxldGVCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwtc21hbGwuc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgd2lkdGg9XCIxNFwiXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxNFwiXHJcbiAgICAgICAgICAgICAgICBhbHQ9XCJEZWxldGVcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkRlbGV0ZUNsaWNrZWR9IC8+XHJcbiAgICAgICAgICAgIDogPGRpdiAvPjtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nXCIgb25DbGljaz17dGhpcy5vbkNsaWNrfT5cclxuICAgICAgICAgICAgPFJvb21BdmF0YXIgb29iRGF0YT17b29iRGF0YX0gd2lkdGg9ezY0fSBoZWlnaHQ9ezY0fSAvPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nX25hbWVcIj57IHJvb21OYW1lTm9kZSB9PC9kaXY+XHJcbiAgICAgICAgICAgIHsgZGVsZXRlQnV0dG9uIH1cclxuICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG4gICAgfSxcclxufSk7XHJcblxyXG5jb25zdCBSb2xlVXNlckxpc3QgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnUm9sZVVzZXJMaXN0JyxcclxuXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHVzZXJzOiBQcm9wVHlwZXMuYXJyYXlPZihVc2VyU3VtbWFyeVR5cGUpLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgcm9sZTogUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgICAgICAgICAgcHJvZmlsZTogUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgICAgIH0pLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgZ3JvdXBJZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICAvLyBXaGV0aGVyIHRoZSBsaXN0IHNob3VsZCBiZSBlZGl0YWJsZVxyXG4gICAgICAgIGVkaXRpbmc6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWRkVXNlcnNDbGlja2VkOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgQWRkcmVzc1BpY2tlckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkFkZHJlc3NQaWNrZXJEaWFsb2dcIik7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnQWRkIFVzZXJzIHRvIEdyb3VwIFN1bW1hcnknLCAnJywgQWRkcmVzc1BpY2tlckRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoJ0FkZCB1c2VycyB0byB0aGUgY29tbXVuaXR5IHN1bW1hcnknKSxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFwiV2hvIHdvdWxkIHlvdSBsaWtlIHRvIGFkZCB0byB0aGlzIHN1bW1hcnk/XCIpLFxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogX3QoXCJOYW1lIG9yIE1hdHJpeCBJRFwiKSxcclxuICAgICAgICAgICAgYnV0dG9uOiBfdChcIkFkZCB0byBzdW1tYXJ5XCIpLFxyXG4gICAgICAgICAgICB2YWxpZEFkZHJlc3NUeXBlczogWydteC11c2VyLWlkJ10sXHJcbiAgICAgICAgICAgIGdyb3VwSWQ6IHRoaXMucHJvcHMuZ3JvdXBJZCxcclxuICAgICAgICAgICAgc2hvdWxkT21pdFNlbGY6IGZhbHNlLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAoc3VjY2VzcywgYWRkcnMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghc3VjY2VzcykgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXJyb3JMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICBhbGxTZXR0bGVkKGFkZHJzLm1hcCgoYWRkcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBHcm91cFN0b3JlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRVc2VyVG9Hcm91cFN1bW1hcnkoYWRkci5hZGRyZXNzKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKCkgPT4geyBlcnJvckxpc3QucHVzaChhZGRyLmFkZHJlc3MpOyB9KTtcclxuICAgICAgICAgICAgICAgIH0pKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3JMaXN0Lmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0ZhaWxlZCB0byBhZGQgdGhlIGZvbGxvd2luZyB1c2VycyB0byB0aGUgY29tbXVuaXR5IHN1bW1hcnknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnJywgRXJyb3JEaWFsb2csXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkZhaWxlZCB0byBhZGQgdGhlIGZvbGxvd2luZyB1c2VycyB0byB0aGUgc3VtbWFyeSBvZiAlKGdyb3VwSWQpczpcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtncm91cElkOiB0aGlzLnByb3BzLmdyb3VwSWR9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogZXJyb3JMaXN0LmpvaW4oXCIsIFwiKSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0sIC8qY2xhc3NOYW1lPSovbnVsbCwgLyppc1ByaW9yaXR5PSovZmFsc2UsIC8qaXNTdGF0aWM9Ki90cnVlKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBUaW50YWJsZVN2ZyA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5UaW50YWJsZVN2Z1wiKTtcclxuICAgICAgICBjb25zdCBhZGRCdXR0b24gPSB0aGlzLnByb3BzLmVkaXRpbmcgP1xyXG4gICAgICAgICAgICAoPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzX2FkZEJ1dHRvblwiIG9uQ2xpY2s9e3RoaXMub25BZGRVc2Vyc0NsaWNrZWR9PlxyXG4gICAgICAgICAgICAgICAgIDxUaW50YWJsZVN2ZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi9yZXMvaW1nL2ljb25zLWNyZWF0ZS1yb29tLnN2Z1wiKX0gd2lkdGg9XCI2NFwiIGhlaWdodD1cIjY0XCIgLz5cclxuICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nc19hZGRCdXR0b25fbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgeyBfdCgnQWRkIGEgVXNlcicpIH1cclxuICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj4pIDogPGRpdiAvPjtcclxuICAgICAgICBjb25zdCB1c2VyTm9kZXMgPSB0aGlzLnByb3BzLnVzZXJzLm1hcCgodSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gPEZlYXR1cmVkVXNlclxyXG4gICAgICAgICAgICAgICAga2V5PXt1LnVzZXJfaWR9XHJcbiAgICAgICAgICAgICAgICBzdW1tYXJ5SW5mbz17dX1cclxuICAgICAgICAgICAgICAgIGVkaXRpbmc9e3RoaXMucHJvcHMuZWRpdGluZ31cclxuICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH0gLz47XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IHJvbGVIZWFkZXIgPSA8ZGl2IC8+O1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJvbGUgJiYgdGhpcy5wcm9wcy5yb2xlLnByb2ZpbGUpIHtcclxuICAgICAgICAgICAgcm9sZUhlYWRlciA9IDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzX2NhdGVnb3J5XCI+eyB0aGlzLnByb3BzLnJvbGUucHJvZmlsZS5uYW1lIH08L2Rpdj47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nc19jb250YWluZXJcIj5cclxuICAgICAgICAgICAgeyByb2xlSGVhZGVyIH1cclxuICAgICAgICAgICAgeyB1c2VyTm9kZXMgfVxyXG4gICAgICAgICAgICB7IGFkZEJ1dHRvbiB9XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfSxcclxufSk7XHJcblxyXG5jb25zdCBGZWF0dXJlZFVzZXIgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnRmVhdHVyZWRVc2VyJyxcclxuXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHN1bW1hcnlJbmZvOiBVc2VyU3VtbWFyeVR5cGUuaXNSZXF1aXJlZCxcclxuICAgICAgICBlZGl0aW5nOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGdyb3VwSWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgIH0sXHJcblxyXG4gICAgb25DbGljazogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3N0YXJ0X2NoYXRfb3JfcmV1c2UnLFxyXG4gICAgICAgICAgICB1c2VyX2lkOiB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnVzZXJfaWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRGVsZXRlQ2xpY2tlZDogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIEdyb3VwU3RvcmUucmVtb3ZlVXNlckZyb21Hcm91cFN1bW1hcnkoXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuZ3JvdXBJZCxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5zdW1tYXJ5SW5mby51c2VyX2lkLFxyXG4gICAgICAgICkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsc3QgcmVtb3ZpbmcgdXNlciBmcm9tIGdyb3VwIHN1bW1hcnknLCBlcnIpO1xyXG4gICAgICAgICAgICBjb25zdCBkaXNwbGF5TmFtZSA9IHRoaXMucHJvcHMuc3VtbWFyeUluZm8uZGlzcGxheW5hbWUgfHwgdGhpcy5wcm9wcy5zdW1tYXJ5SW5mby51c2VyX2lkO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKFxyXG4gICAgICAgICAgICAgICAgJ0ZhaWxlZCB0byByZW1vdmUgdXNlciBmcm9tIGNvbW11bml0eSBzdW1tYXJ5JyxcclxuICAgICAgICAgICAgICAgICcnLCBFcnJvckRpYWxvZyxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiRmFpbGVkIHRvIHJlbW92ZSBhIHVzZXIgZnJvbSB0aGUgc3VtbWFyeSBvZiAlKGdyb3VwSWQpc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHtncm91cElkOiB0aGlzLnByb3BzLmdyb3VwSWR9LFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIlRoZSB1c2VyICclKGRpc3BsYXlOYW1lKXMnIGNvdWxkIG5vdCBiZSByZW1vdmVkIGZyb20gdGhlIHN1bW1hcnkuXCIsIHtkaXNwbGF5TmFtZX0pLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcImF2YXRhcnMuQmFzZUF2YXRhclwiKTtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5wcm9wcy5zdW1tYXJ5SW5mby5kaXNwbGF5bmFtZSB8fCB0aGlzLnByb3BzLnN1bW1hcnlJbmZvLnVzZXJfaWQ7XHJcblxyXG4gICAgICAgIGNvbnN0IHBlcm1hbGluayA9IG1ha2VVc2VyUGVybWFsaW5rKHRoaXMucHJvcHMuc3VtbWFyeUluZm8udXNlcl9pZCk7XHJcbiAgICAgICAgY29uc3QgdXNlck5hbWVOb2RlID0gPGEgaHJlZj17cGVybWFsaW5rfSBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9PnsgbmFtZSB9PC9hPjtcclxuICAgICAgICBjb25zdCBodHRwVXJsID0gTWF0cml4Q2xpZW50UGVnLmdldCgpXHJcbiAgICAgICAgICAgIC5teGNVcmxUb0h0dHAodGhpcy5wcm9wcy5zdW1tYXJ5SW5mby5hdmF0YXJfdXJsLCA2NCwgNjQpO1xyXG5cclxuICAgICAgICBjb25zdCBkZWxldGVCdXR0b24gPSB0aGlzLnByb3BzLmVkaXRpbmcgP1xyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfZmVhdHVyZWRUaGluZ19kZWxldGVCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwtc21hbGwuc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgd2lkdGg9XCIxNFwiXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxNFwiXHJcbiAgICAgICAgICAgICAgICBhbHQ9XCJEZWxldGVcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkRlbGV0ZUNsaWNrZWR9IC8+XHJcbiAgICAgICAgICAgIDogPGRpdiAvPjtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nXCIgb25DbGljaz17dGhpcy5vbkNsaWNrfT5cclxuICAgICAgICAgICAgPEJhc2VBdmF0YXIgbmFtZT17bmFtZX0gdXJsPXtodHRwVXJsfSB3aWR0aD17NjR9IGhlaWdodD17NjR9IC8+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdfbmFtZVwiPnsgdXNlck5hbWVOb2RlIH08L2Rpdj5cclxuICAgICAgICAgICAgeyBkZWxldGVCdXR0b24gfVxyXG4gICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbmNvbnN0IEdST1VQX0pPSU5QT0xJQ1lfT1BFTiA9IFwib3BlblwiO1xyXG5jb25zdCBHUk9VUF9KT0lOUE9MSUNZX0lOVklURSA9IFwiaW52aXRlXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnR3JvdXBWaWV3JyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBncm91cElkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLy8gV2hldGhlciB0aGlzIGlzIHRoZSBmaXJzdCB0aW1lIHRoZSBncm91cCBhZG1pbiBpcyB2aWV3aW5nIHRoZSBncm91cFxyXG4gICAgICAgIGdyb3VwSXNOZXc6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHN1bW1hcnk6IG51bGwsXHJcbiAgICAgICAgICAgIGlzR3JvdXBQdWJsaWNpc2VkOiBudWxsLFxyXG4gICAgICAgICAgICBpc1VzZXJQcml2aWxlZ2VkOiBudWxsLFxyXG4gICAgICAgICAgICBncm91cFJvb21zOiBudWxsLFxyXG4gICAgICAgICAgICBncm91cFJvb21zTG9hZGluZzogbnVsbCxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIGVkaXRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICBzYXZpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICB1cGxvYWRpbmdBdmF0YXI6IGZhbHNlLFxyXG4gICAgICAgICAgICBhdmF0YXJDaGFuZ2VkOiBmYWxzZSxcclxuICAgICAgICAgICAgbWVtYmVyc2hpcEJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICBwdWJsaWNpdHlCdXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgaW52aXRlclByb2ZpbGU6IG51bGwsXHJcbiAgICAgICAgICAgIHNob3dSaWdodFBhbmVsOiBSaWdodFBhbmVsU3RvcmUuZ2V0U2hhcmVkSW5zdGFuY2UoKS5pc09wZW5Gb3JHcm91cCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5vbihcIkdyb3VwLm15TWVtYmVyc2hpcFwiLCB0aGlzLl9vbkdyb3VwTXlNZW1iZXJzaGlwKTtcclxuXHJcbiAgICAgICAgdGhpcy5faW5pdEdyb3VwU3RvcmUodGhpcy5wcm9wcy5ncm91cElkLCB0cnVlKTtcclxuXHJcbiAgICAgICAgdGhpcy5fZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLl9vbkFjdGlvbik7XHJcbiAgICAgICAgdGhpcy5fcmlnaHRQYW5lbFN0b3JlVG9rZW4gPSBSaWdodFBhbmVsU3RvcmUuZ2V0U2hhcmVkSW5zdGFuY2UoKS5hZGRMaXN0ZW5lcih0aGlzLl9vblJpZ2h0UGFuZWxTdG9yZVVwZGF0ZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5yZW1vdmVMaXN0ZW5lcihcIkdyb3VwLm15TWVtYmVyc2hpcFwiLCB0aGlzLl9vbkdyb3VwTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLl9kaXNwYXRjaGVyUmVmKTtcclxuXHJcbiAgICAgICAgLy8gUmVtb3ZlIFJpZ2h0UGFuZWxTdG9yZSBsaXN0ZW5lclxyXG4gICAgICAgIGlmICh0aGlzLl9yaWdodFBhbmVsU3RvcmVUb2tlbikge1xyXG4gICAgICAgICAgICB0aGlzLl9yaWdodFBhbmVsU3RvcmVUb2tlbi5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV3UHJvcHMpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5ncm91cElkICE9PSBuZXdQcm9wcy5ncm91cElkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc3VtbWFyeTogbnVsbCxcclxuICAgICAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9pbml0R3JvdXBTdG9yZShuZXdQcm9wcy5ncm91cElkKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25SaWdodFBhbmVsU3RvcmVVcGRhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzaG93UmlnaHRQYW5lbDogUmlnaHRQYW5lbFN0b3JlLmdldFNoYXJlZEluc3RhbmNlKCkuaXNPcGVuRm9yR3JvdXAsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkdyb3VwTXlNZW1iZXJzaGlwOiBmdW5jdGlvbihncm91cCkge1xyXG4gICAgICAgIGlmICh0aGlzLl91bm1vdW50ZWQgfHwgZ3JvdXAuZ3JvdXBJZCAhPT0gdGhpcy5wcm9wcy5ncm91cElkKSByZXR1cm47XHJcbiAgICAgICAgaWYgKGdyb3VwLm15TWVtYmVyc2hpcCA9PT0gJ2xlYXZlJykge1xyXG4gICAgICAgICAgICAvLyBMZWF2ZSBzZXR0aW5ncyAtIHRoZSB1c2VyIG1pZ2h0IGhhdmUgY2xpY2tlZCB0aGUgXCJMZWF2ZVwiIGJ1dHRvblxyXG4gICAgICAgICAgICB0aGlzLl9jbG9zZVNldHRpbmdzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe21lbWJlcnNoaXBCdXN5OiBmYWxzZX0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaW5pdEdyb3VwU3RvcmU6IGZ1bmN0aW9uKGdyb3VwSWQsIGZpcnN0SW5pdCkge1xyXG4gICAgICAgIGNvbnN0IGdyb3VwID0gdGhpcy5fbWF0cml4Q2xpZW50LmdldEdyb3VwKGdyb3VwSWQpO1xyXG4gICAgICAgIGlmIChncm91cCAmJiBncm91cC5pbnZpdGVyICYmIGdyb3VwLmludml0ZXIudXNlcklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2ZldGNoSW52aXRlclByb2ZpbGUoZ3JvdXAuaW52aXRlci51c2VySWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBHcm91cFN0b3JlLnJlZ2lzdGVyTGlzdGVuZXIoZ3JvdXBJZCwgdGhpcy5vbkdyb3VwU3RvcmVVcGRhdGVkLmJpbmQodGhpcywgZmlyc3RJbml0KSk7XHJcbiAgICAgICAgbGV0IHdpbGxEb09uYm9hcmRpbmcgPSBmYWxzZTtcclxuICAgICAgICAvLyBYWFg6IFRoaXMgc2hvdWxkIGJlIG1vcmUgZmx1eHkgLSBsZXQncyBnZXQgdGhlIGVycm9yIGZyb20gR3JvdXBTdG9yZSAuZ2V0RXJyb3Igb3Igc29tZXRoaW5nXHJcbiAgICAgICAgR3JvdXBTdG9yZS5vbignZXJyb3InLCAoZXJyLCBlcnJvckdyb3VwSWQsIHN0YXRlS2V5KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl91bm1vdW50ZWQgfHwgZ3JvdXBJZCAhPT0gZXJyb3JHcm91cElkKSByZXR1cm47XHJcbiAgICAgICAgICAgIGlmIChlcnIuZXJyY29kZSA9PT0gJ01fR1VFU1RfQUNDRVNTX0ZPUkJJRERFTicgJiYgIXdpbGxEb09uYm9hcmRpbmcpIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnZG9fYWZ0ZXJfc3luY19wcmVwYXJlZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZGVmZXJyZWRfYWN0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfZ3JvdXAnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBncm91cF9pZDogZ3JvdXBJZCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJywgc2NyZWVuX2FmdGVyOiB7c2NyZWVuOiBgZ3JvdXAvJHtncm91cElkfWB9fSk7XHJcbiAgICAgICAgICAgICAgICB3aWxsRG9PbmJvYXJkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc3RhdGVLZXkgPT09IEdyb3VwU3RvcmUuU1RBVEVfS0VZLlN1bW1hcnkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHN1bW1hcnk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGVycixcclxuICAgICAgICAgICAgICAgICAgICBlZGl0aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uR3JvdXBTdG9yZVVwZGF0ZWQoZmlyc3RJbml0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgIGNvbnN0IHN1bW1hcnkgPSBHcm91cFN0b3JlLmdldFN1bW1hcnkodGhpcy5wcm9wcy5ncm91cElkKTtcclxuICAgICAgICBpZiAoc3VtbWFyeS5wcm9maWxlKSB7XHJcbiAgICAgICAgICAgIC8vIERlZmF1bHQgcHJvZmlsZSBmaWVsZHMgc2hvdWxkIGJlIFwiXCIgZm9yIGxhdGVyIHNlbmRpbmcgdG8gdGhlIHNlcnZlciAod2hpY2hcclxuICAgICAgICAgICAgLy8gcmVxdWlyZXMgdGhhdCB0aGUgZmllbGRzIGFyZSBzdHJpbmdzLCBub3QgbnVsbClcclxuICAgICAgICAgICAgW1wiYXZhdGFyX3VybFwiLCBcImxvbmdfZGVzY3JpcHRpb25cIiwgXCJuYW1lXCIsIFwic2hvcnRfZGVzY3JpcHRpb25cIl0uZm9yRWFjaCgoaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgc3VtbWFyeS5wcm9maWxlW2tdID0gc3VtbWFyeS5wcm9maWxlW2tdIHx8IFwiXCI7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc3VtbWFyeSxcclxuICAgICAgICAgICAgc3VtbWFyeUxvYWRpbmc6ICFHcm91cFN0b3JlLmlzU3RhdGVSZWFkeSh0aGlzLnByb3BzLmdyb3VwSWQsIEdyb3VwU3RvcmUuU1RBVEVfS0VZLlN1bW1hcnkpLFxyXG4gICAgICAgICAgICBpc0dyb3VwUHVibGljaXNlZDogR3JvdXBTdG9yZS5nZXRHcm91cFB1YmxpY2l0eSh0aGlzLnByb3BzLmdyb3VwSWQpLFxyXG4gICAgICAgICAgICBpc1VzZXJQcml2aWxlZ2VkOiBHcm91cFN0b3JlLmlzVXNlclByaXZpbGVnZWQodGhpcy5wcm9wcy5ncm91cElkKSxcclxuICAgICAgICAgICAgZ3JvdXBSb29tczogR3JvdXBTdG9yZS5nZXRHcm91cFJvb21zKHRoaXMucHJvcHMuZ3JvdXBJZCksXHJcbiAgICAgICAgICAgIGdyb3VwUm9vbXNMb2FkaW5nOiAhR3JvdXBTdG9yZS5pc1N0YXRlUmVhZHkodGhpcy5wcm9wcy5ncm91cElkLCBHcm91cFN0b3JlLlNUQVRFX0tFWS5Hcm91cFJvb21zKSxcclxuICAgICAgICAgICAgaXNVc2VyTWVtYmVyOiBHcm91cFN0b3JlLmdldEdyb3VwTWVtYmVycyh0aGlzLnByb3BzLmdyb3VwSWQpLnNvbWUoXHJcbiAgICAgICAgICAgICAgICAobSkgPT4gbS51c2VySWQgPT09IHRoaXMuX21hdHJpeENsaWVudC5jcmVkZW50aWFscy51c2VySWQsXHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gWFhYOiBUaGlzIG1pZ2h0IG5vdCB3b3JrIGJ1dCB0aGlzLnByb3BzLmdyb3VwSXNOZXcgdW51c2VkIGFueXdheVxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmdyb3VwSXNOZXcgJiYgZmlyc3RJbml0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uRWRpdENsaWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfZmV0Y2hJbnZpdGVyUHJvZmlsZSh1c2VySWQpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgaW52aXRlclByb2ZpbGVCdXN5OiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5nZXRQcm9maWxlSW5mbyh1c2VySWQpLnRoZW4oKHJlc3ApID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGludml0ZXJQcm9maWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXZhdGFyVXJsOiByZXNwLmF2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJlc3AuZGlzcGxheW5hbWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBnZXR0aW5nIGdyb3VwIGludml0ZXIgcHJvZmlsZScsIGUpO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgaW52aXRlclByb2ZpbGVCdXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkVkaXRDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGVkaXRpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIHByb2ZpbGVGb3JtOiBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlLnN1bW1hcnkucHJvZmlsZSksXHJcbiAgICAgICAgICAgIGpvaW5hYmxlRm9ybToge1xyXG4gICAgICAgICAgICAgICAgcG9saWN5VHlwZTpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnN1bW1hcnkucHJvZmlsZS5pc19vcGVubHlfam9pbmFibGUgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBHUk9VUF9KT0lOUE9MSUNZX09QRU4gOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBHUk9VUF9KT0lOUE9MSUNZX0lOVklURSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uU2hhcmVDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgU2hhcmVEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5TaGFyZURpYWxvZ1wiKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdzaGFyZSBjb21tdW5pdHkgZGlhbG9nJywgJycsIFNoYXJlRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRhcmdldDogdGhpcy5fbWF0cml4Q2xpZW50LmdldEdyb3VwKHRoaXMucHJvcHMuZ3JvdXBJZCkgfHwgbmV3IEdyb3VwKHRoaXMucHJvcHMuZ3JvdXBJZCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNhbmNlbENsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9jbG9zZVNldHRpbmdzKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkFjdGlvbihwYXlsb2FkKSB7XHJcbiAgICAgICAgc3dpdGNoIChwYXlsb2FkLmFjdGlvbikge1xyXG4gICAgICAgICAgICAvLyBOT1RFOiBjbG9zZV9zZXR0aW5ncyBpcyBhbiBhcHAtd2lkZSBkaXNwYXRjaDsgYXMgaXQgaXMgZGlzcGF0Y2hlZCBmcm9tIE1hdHJpeENoYXRcclxuICAgICAgICAgICAgY2FzZSAnY2xvc2Vfc2V0dGluZ3MnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZWRpdGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvZmlsZUZvcm06IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfY2xvc2VTZXR0aW5ncygpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2Nsb3NlX3NldHRpbmdzJ30pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25OYW1lQ2hhbmdlOiBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgICAgIGNvbnN0IG5ld1Byb2ZpbGVGb3JtID0gT2JqZWN0LmFzc2lnbih0aGlzLnN0YXRlLnByb2ZpbGVGb3JtLCB7IG5hbWU6IHZhbHVlIH0pO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwcm9maWxlRm9ybTogbmV3UHJvZmlsZUZvcm0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblNob3J0RGVzY0NoYW5nZTogZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICBjb25zdCBuZXdQcm9maWxlRm9ybSA9IE9iamVjdC5hc3NpZ24odGhpcy5zdGF0ZS5wcm9maWxlRm9ybSwgeyBzaG9ydF9kZXNjcmlwdGlvbjogdmFsdWUgfSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHByb2ZpbGVGb3JtOiBuZXdQcm9maWxlRm9ybSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uTG9uZ0Rlc2NDaGFuZ2U6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBjb25zdCBuZXdQcm9maWxlRm9ybSA9IE9iamVjdC5hc3NpZ24odGhpcy5zdGF0ZS5wcm9maWxlRm9ybSwgeyBsb25nX2Rlc2NyaXB0aW9uOiBlLnRhcmdldC52YWx1ZSB9KTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcHJvZmlsZUZvcm06IG5ld1Byb2ZpbGVGb3JtLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25BdmF0YXJTZWxlY3RlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBjb25zdCBmaWxlID0gZXYudGFyZ2V0LmZpbGVzWzBdO1xyXG4gICAgICAgIGlmICghZmlsZSkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHt1cGxvYWRpbmdBdmF0YXI6IHRydWV9KTtcclxuICAgICAgICB0aGlzLl9tYXRyaXhDbGllbnQudXBsb2FkQ29udGVudChmaWxlKS50aGVuKCh1cmwpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbmV3UHJvZmlsZUZvcm0gPSBPYmplY3QuYXNzaWduKHRoaXMuc3RhdGUucHJvZmlsZUZvcm0sIHsgYXZhdGFyX3VybDogdXJsIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHVwbG9hZGluZ0F2YXRhcjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBwcm9maWxlRm9ybTogbmV3UHJvZmlsZUZvcm0sXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gSW5kaWNhdGUgdGhhdCBGbGFpclN0b3JlIG5lZWRzIHRvIGJlIHBva2VkIHRvIHNob3cgdGhpcyBjaGFuZ2VcclxuICAgICAgICAgICAgICAgIC8vIGluIFRhZ1RpbGUgKFRhZ1BhbmVsKSwgRmxhaXIgYW5kIEdyb3VwVGlsZSAoTXlHcm91cHMpLlxyXG4gICAgICAgICAgICAgICAgYXZhdGFyQ2hhbmdlZDogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dXBsb2FkaW5nQXZhdGFyOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIHVwbG9hZCBhdmF0YXIgaW1hZ2VcIiwgZSk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byB1cGxvYWQgaW1hZ2UnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXJyb3InKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdCgnRmFpbGVkIHRvIHVwbG9hZCBpbWFnZScpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uSm9pbmFibGVDaGFuZ2U6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGpvaW5hYmxlRm9ybTogeyBwb2xpY3lUeXBlOiBldi50YXJnZXQudmFsdWUgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uU2F2ZUNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtzYXZpbmc6IHRydWV9KTtcclxuICAgICAgICBjb25zdCBzYXZlUHJvbWlzZSA9IHRoaXMuc3RhdGUuaXNVc2VyUHJpdmlsZWdlZCA/IHRoaXMuX3NhdmVHcm91cCgpIDogUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgc2F2ZVByb21pc2UudGhlbigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc2F2aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVkaXRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3VtbWFyeTogbnVsbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAncGFuZWxfZGlzYWJsZSd9KTtcclxuICAgICAgICAgICAgdGhpcy5faW5pdEdyb3VwU3RvcmUodGhpcy5wcm9wcy5ncm91cElkKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmF2YXRhckNoYW5nZWQpIHtcclxuICAgICAgICAgICAgICAgIC8vIFhYWDogRXZpbCAtIHBva2luZyBhIHN0b3JlIHNob3VsZCBiZSBkb25lIGZyb20gYW4gYXN5bmMgYWN0aW9uXHJcbiAgICAgICAgICAgICAgICBGbGFpclN0b3JlLnJlZnJlc2hHcm91cFByb2ZpbGUodGhpcy5fbWF0cml4Q2xpZW50LCB0aGlzLnByb3BzLmdyb3VwSWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBzYXZpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBzYXZlIGNvbW11bml0eSBwcm9maWxlXCIsIGUpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gdXBkYXRlIGdyb3VwJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0Vycm9yJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ0ZhaWxlZCB0byB1cGRhdGUgY29tbXVuaXR5JyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGF2YXRhckNoYW5nZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NhdmVHcm91cDogYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5fbWF0cml4Q2xpZW50LnNldEdyb3VwUHJvZmlsZSh0aGlzLnByb3BzLmdyb3VwSWQsIHRoaXMuc3RhdGUucHJvZmlsZUZvcm0pO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuX21hdHJpeENsaWVudC5zZXRHcm91cEpvaW5Qb2xpY3kodGhpcy5wcm9wcy5ncm91cElkLCB7XHJcbiAgICAgICAgICAgIHR5cGU6IHRoaXMuc3RhdGUuam9pbmFibGVGb3JtLnBvbGljeVR5cGUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkFjY2VwdEludml0ZUNsaWNrOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHttZW1iZXJzaGlwQnVzeTogdHJ1ZX0pO1xyXG5cclxuICAgICAgICAvLyBXYWl0IDUwMG1zIHRvIHByZXZlbnQgZmxhc2hpbmcuIERvIHRoaXMgYmVmb3JlIHNlbmRpbmcgYSByZXF1ZXN0IG90aGVyd2lzZSB3ZSByaXNrIHRoZVxyXG4gICAgICAgIC8vIHNwaW5uZXIgZGlzYXBwZWFyaW5nIGFmdGVyIHdlIGhhdmUgZmV0Y2hlZCBuZXcgZ3JvdXAgZGF0YS5cclxuICAgICAgICBhd2FpdCBzbGVlcCg1MDApO1xyXG5cclxuICAgICAgICBHcm91cFN0b3JlLmFjY2VwdEdyb3VwSW52aXRlKHRoaXMucHJvcHMuZ3JvdXBJZCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGRvbid0IHJlc2V0IG1lbWJlcnNoaXBCdXN5IGhlcmU6IHdhaXQgZm9yIHRoZSBtZW1iZXJzaGlwIGNoYW5nZSB0byBjb21lIGRvd24gdGhlIHN5bmNcclxuICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHttZW1iZXJzaGlwQnVzeTogZmFsc2V9KTtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRXJyb3IgYWNjZXB0aW5nIGludml0ZScsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJVbmFibGUgdG8gYWNjZXB0IGludml0ZVwiKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJlamVjdEludml0ZUNsaWNrOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHttZW1iZXJzaGlwQnVzeTogdHJ1ZX0pO1xyXG5cclxuICAgICAgICAvLyBXYWl0IDUwMG1zIHRvIHByZXZlbnQgZmxhc2hpbmcuIERvIHRoaXMgYmVmb3JlIHNlbmRpbmcgYSByZXF1ZXN0IG90aGVyd2lzZSB3ZSByaXNrIHRoZVxyXG4gICAgICAgIC8vIHNwaW5uZXIgZGlzYXBwZWFyaW5nIGFmdGVyIHdlIGhhdmUgZmV0Y2hlZCBuZXcgZ3JvdXAgZGF0YS5cclxuICAgICAgICBhd2FpdCBzbGVlcCg1MDApO1xyXG5cclxuICAgICAgICBHcm91cFN0b3JlLmxlYXZlR3JvdXAodGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgLy8gZG9uJ3QgcmVzZXQgbWVtYmVyc2hpcEJ1c3kgaGVyZTogd2FpdCBmb3IgdGhlIG1lbWJlcnNoaXAgY2hhbmdlIHRvIGNvbWUgZG93biB0aGUgc3luY1xyXG4gICAgICAgIH0pLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21lbWJlcnNoaXBCdXN5OiBmYWxzZX0pO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdFcnJvciByZWplY3RpbmcgaW52aXRlJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJFcnJvclwiKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIlVuYWJsZSB0byByZWplY3QgaW52aXRlXCIpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uSm9pbkNsaWNrOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5fbWF0cml4Q2xpZW50LmlzR3Vlc3QoKSkge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJywgc2NyZWVuX2FmdGVyOiB7c2NyZWVuOiBgZ3JvdXAvJHt0aGlzLnByb3BzLmdyb3VwSWR9YH19KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bWVtYmVyc2hpcEJ1c3k6IHRydWV9KTtcclxuXHJcbiAgICAgICAgLy8gV2FpdCA1MDBtcyB0byBwcmV2ZW50IGZsYXNoaW5nLiBEbyB0aGlzIGJlZm9yZSBzZW5kaW5nIGEgcmVxdWVzdCBvdGhlcndpc2Ugd2UgcmlzayB0aGVcclxuICAgICAgICAvLyBzcGlubmVyIGRpc2FwcGVhcmluZyBhZnRlciB3ZSBoYXZlIGZldGNoZWQgbmV3IGdyb3VwIGRhdGEuXHJcbiAgICAgICAgYXdhaXQgc2xlZXAoNTAwKTtcclxuXHJcbiAgICAgICAgR3JvdXBTdG9yZS5qb2luR3JvdXAodGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgLy8gZG9uJ3QgcmVzZXQgbWVtYmVyc2hpcEJ1c3kgaGVyZTogd2FpdCBmb3IgdGhlIG1lbWJlcnNoaXAgY2hhbmdlIHRvIGNvbWUgZG93biB0aGUgc3luY1xyXG4gICAgICAgIH0pLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21lbWJlcnNoaXBCdXN5OiBmYWxzZX0pO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdFcnJvciBqb2luaW5nIHJvb20nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFwiVW5hYmxlIHRvIGpvaW4gY29tbXVuaXR5XCIpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2xlYXZlR3JvdXBXYXJuaW5nczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgd2FybmluZ3MgPSBbXTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuaXNVc2VyUHJpdmlsZWdlZCkge1xyXG4gICAgICAgICAgICB3YXJuaW5ncy5wdXNoKChcclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIndhcm5pbmdcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IFwiIFwiIC8qIFdoaXRlc3BhY2UsIG90aGVyd2lzZSB0aGUgc2VudGVuY2VzIGdldCBzbWFzaGVkIHRvZ2V0aGVyICovIH1cclxuICAgICAgICAgICAgICAgICAgICB7IF90KFwiWW91IGFyZSBhbiBhZG1pbmlzdHJhdG9yIG9mIHRoaXMgY29tbXVuaXR5LiBZb3Ugd2lsbCBub3QgYmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXCJhYmxlIHRvIHJlam9pbiB3aXRob3V0IGFuIGludml0ZSBmcm9tIGFub3RoZXIgYWRtaW5pc3RyYXRvci5cIikgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICApKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB3YXJuaW5ncztcclxuICAgIH0sXHJcblxyXG5cclxuICAgIF9vbkxlYXZlQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3Qgd2FybmluZ3MgPSB0aGlzLl9sZWF2ZUdyb3VwV2FybmluZ3MoKTtcclxuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTGVhdmUgR3JvdXAnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KFwiTGVhdmUgQ29tbXVuaXR5XCIpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogKFxyXG4gICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICB7IF90KFwiTGVhdmUgJShncm91cE5hbWUpcz9cIiwge2dyb3VwTmFtZTogdGhpcy5wcm9wcy5ncm91cElkfSkgfVxyXG4gICAgICAgICAgICAgICAgeyB3YXJuaW5ncyB9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJMZWF2ZVwiKSxcclxuICAgICAgICAgICAgZGFuZ2VyOiB0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQsXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IGFzeW5jIChjb25maXJtZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghY29uZmlybWVkKSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bWVtYmVyc2hpcEJ1c3k6IHRydWV9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBXYWl0IDUwMG1zIHRvIHByZXZlbnQgZmxhc2hpbmcuIERvIHRoaXMgYmVmb3JlIHNlbmRpbmcgYSByZXF1ZXN0IG90aGVyd2lzZSB3ZSByaXNrIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gc3Bpbm5lciBkaXNhcHBlYXJpbmcgYWZ0ZXIgd2UgaGF2ZSBmZXRjaGVkIG5ldyBncm91cCBkYXRhLlxyXG4gICAgICAgICAgICAgICAgYXdhaXQgc2xlZXAoNTAwKTtcclxuXHJcbiAgICAgICAgICAgICAgICBHcm91cFN0b3JlLmxlYXZlR3JvdXAodGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBkb24ndCByZXNldCBtZW1iZXJzaGlwQnVzeSBoZXJlOiB3YWl0IGZvciB0aGUgbWVtYmVyc2hpcCBjaGFuZ2UgdG8gY29tZSBkb3duIHRoZSBzeW5jXHJcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21lbWJlcnNoaXBCdXN5OiBmYWxzZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRXJyb3IgbGVhdmluZyBjb21tdW5pdHknLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIlVuYWJsZSB0byBsZWF2ZSBjb21tdW5pdHlcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uQWRkUm9vbXNDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgc2hvd0dyb3VwQWRkUm9vbURpYWxvZyh0aGlzLnByb3BzLmdyb3VwSWQpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0R3JvdXBTZWN0aW9uOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBncm91cFNldHRpbmdzU2VjdGlvbkNsYXNzZXMgPSBjbGFzc25hbWVzKHtcclxuICAgICAgICAgICAgXCJteF9Hcm91cFZpZXdfZ3JvdXBcIjogdGhpcy5zdGF0ZS5lZGl0aW5nLFxyXG4gICAgICAgICAgICBcIm14X0dyb3VwVmlld19ncm91cF9kaXNhYmxlZFwiOiB0aGlzLnN0YXRlLmVkaXRpbmcgJiYgIXRoaXMuc3RhdGUuaXNVc2VyUHJpdmlsZWdlZCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgaGVhZGVyID0gdGhpcy5zdGF0ZS5lZGl0aW5nID8gPGgyPiB7IF90KCdDb21tdW5pdHkgU2V0dGluZ3MnKSB9IDwvaDI+IDogPGRpdiAvPjtcclxuXHJcbiAgICAgICAgY29uc3QgaG9zdGluZ1NpZ251cExpbmsgPSBnZXRIb3N0aW5nTGluaygnY29tbXVuaXR5LXNldHRpbmdzJyk7XHJcbiAgICAgICAgbGV0IGhvc3RpbmdTaWdudXAgPSBudWxsO1xyXG4gICAgICAgIGlmIChob3N0aW5nU2lnbnVwTGluayAmJiB0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQpIHtcclxuICAgICAgICAgICAgaG9zdGluZ1NpZ251cCA9IDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2hvc3RpbmdTaWdudXBcIj5cclxuICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICBcIldhbnQgbW9yZSB0aGFuIGEgY29tbXVuaXR5PyA8YT5HZXQgeW91ciBvd24gc2VydmVyPC9hPlwiLCB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGE6IHN1YiA9PiA8YSBocmVmPXtob3N0aW5nU2lnbnVwTGlua30gdGFyZ2V0PVwiX2JsYW5rXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiPntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIDxhIGhyZWY9e2hvc3RpbmdTaWdudXBMaW5rfSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi9yZXMvaW1nL2V4dGVybmFsLWxpbmsuc3ZnXCIpfSB3aWR0aD1cIjExXCIgaGVpZ2h0PVwiMTBcIiBhbHQ9JycgLz5cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY2hhbmdlRGVsYXlXYXJuaW5nID0gdGhpcy5zdGF0ZS5lZGl0aW5nICYmIHRoaXMuc3RhdGUuaXNVc2VyUHJpdmlsZWdlZCA/XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2NoYW5nZURlbGF5V2FybmluZ1wiPlxyXG4gICAgICAgICAgICAgICAgeyBfdChcclxuICAgICAgICAgICAgICAgICAgICAnQ2hhbmdlcyBtYWRlIHRvIHlvdXIgY29tbXVuaXR5IDxib2xkMT5uYW1lPC9ib2xkMT4gYW5kIDxib2xkMj5hdmF0YXI8L2JvbGQyPiAnICtcclxuICAgICAgICAgICAgICAgICAgICAnbWlnaHQgbm90IGJlIHNlZW4gYnkgb3RoZXIgdXNlcnMgZm9yIHVwIHRvIDMwIG1pbnV0ZXMuJyxcclxuICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdib2xkMSc6IChzdWIpID0+IDxiPiB7IHN1YiB9IDwvYj4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdib2xkMic6IChzdWIpID0+IDxiPiB7IHN1YiB9IDwvYj4sXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICA8L2Rpdj4gOiA8ZGl2IC8+O1xyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Z3JvdXBTZXR0aW5nc1NlY3Rpb25DbGFzc2VzfT5cclxuICAgICAgICAgICAgeyBoZWFkZXIgfVxyXG4gICAgICAgICAgICB7IGhvc3RpbmdTaWdudXAgfVxyXG4gICAgICAgICAgICB7IGNoYW5nZURlbGF5V2FybmluZyB9XHJcbiAgICAgICAgICAgIHsgdGhpcy5fZ2V0Sm9pbmFibGVOb2RlKCkgfVxyXG4gICAgICAgICAgICB7IHRoaXMuX2dldExvbmdEZXNjcmlwdGlvbk5vZGUoKSB9XHJcbiAgICAgICAgICAgIHsgdGhpcy5fZ2V0Um9vbXNOb2RlKCkgfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFJvb21zTm9kZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgUm9vbURldGFpbExpc3QgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5Sb29tRGV0YWlsTGlzdCcpO1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgY29uc3QgVGludGFibGVTdmcgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5UaW50YWJsZVN2ZycpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5TcGlubmVyJyk7XHJcbiAgICAgICAgY29uc3QgVG9vbHRpcEJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlRvb2x0aXBCdXR0b24nKTtcclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbXNIZWxwTm9kZSA9IHRoaXMuc3RhdGUuZWRpdGluZyA/IDxUb29sdGlwQnV0dG9uIGhlbHBUZXh0PXtcclxuICAgICAgICAgICAgX3QoXHJcbiAgICAgICAgICAgICAgICAnVGhlc2Ugcm9vbXMgYXJlIGRpc3BsYXllZCB0byBjb21tdW5pdHkgbWVtYmVycyBvbiB0aGUgY29tbXVuaXR5IHBhZ2UuICcrXHJcbiAgICAgICAgICAgICAgICAnQ29tbXVuaXR5IG1lbWJlcnMgY2FuIGpvaW4gdGhlIHJvb21zIGJ5IGNsaWNraW5nIG9uIHRoZW0uJyxcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgIH0gLz4gOiA8ZGl2IC8+O1xyXG5cclxuICAgICAgICBjb25zdCBhZGRSb29tUm93ID0gdGhpcy5zdGF0ZS5lZGl0aW5nID9cclxuICAgICAgICAgICAgKDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19yb29tc19oZWFkZXJfYWRkUm93XCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQWRkUm9vbXNDbGlja31cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfcm9vbXNfaGVhZGVyX2FkZFJvd19idXR0b25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8VGludGFibGVTdmcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9pY29ucy1yb29tLWFkZC5zdmdcIil9IHdpZHRoPVwiMjRcIiBoZWlnaHQ9XCIyNFwiIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X3Jvb21zX2hlYWRlcl9hZGRSb3dfbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdBZGQgcm9vbXMgdG8gdGhpcyBjb21tdW5pdHknKSB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPikgOiA8ZGl2IC8+O1xyXG4gICAgICAgIGNvbnN0IHJvb21EZXRhaWxMaXN0Q2xhc3NOYW1lID0gY2xhc3NuYW1lcyh7XHJcbiAgICAgICAgICAgIFwibXhfZmFkYWJsZVwiOiB0cnVlLFxyXG4gICAgICAgICAgICBcIm14X2ZhZGFibGVfZmFkZWRcIjogdGhpcy5zdGF0ZS5lZGl0aW5nLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19yb29tc1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19yb29tc19oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgIDxoMz5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdSb29tcycpIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHJvb21zSGVscE5vZGUgfVxyXG4gICAgICAgICAgICAgICAgPC9oMz5cclxuICAgICAgICAgICAgICAgIHsgYWRkUm9vbVJvdyB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICB7IHRoaXMuc3RhdGUuZ3JvdXBSb29tc0xvYWRpbmcgP1xyXG4gICAgICAgICAgICAgICAgPFNwaW5uZXIgLz4gOlxyXG4gICAgICAgICAgICAgICAgPFJvb21EZXRhaWxMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbXM9e3RoaXMuc3RhdGUuZ3JvdXBSb29tc31cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Jvb21EZXRhaWxMaXN0Q2xhc3NOYW1lfSAvPlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0RmVhdHVyZWRSb29tc05vZGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHN1bW1hcnkgPSB0aGlzLnN0YXRlLnN1bW1hcnk7XHJcblxyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRDYXRlZ29yeVJvb21zID0gW107XHJcbiAgICAgICAgY29uc3QgY2F0ZWdvcnlSb29tcyA9IHt9O1xyXG4gICAgICAgIHN1bW1hcnkucm9vbXNfc2VjdGlvbi5yb29tcy5mb3JFYWNoKChyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyLmNhdGVnb3J5X2lkID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0Q2F0ZWdvcnlSb29tcy5wdXNoKHIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IGxpc3QgPSBjYXRlZ29yeVJvb21zW3IuY2F0ZWdvcnlfaWRdO1xyXG4gICAgICAgICAgICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeVJvb21zW3IuY2F0ZWdvcnlfaWRdID0gbGlzdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxpc3QucHVzaChyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBkZWZhdWx0Q2F0ZWdvcnlOb2RlID0gPENhdGVnb3J5Um9vbUxpc3RcclxuICAgICAgICAgICAgcm9vbXM9e2RlZmF1bHRDYXRlZ29yeVJvb21zfVxyXG4gICAgICAgICAgICBncm91cElkPXt0aGlzLnByb3BzLmdyb3VwSWR9XHJcbiAgICAgICAgICAgIGVkaXRpbmc9e3RoaXMuc3RhdGUuZWRpdGluZ30gLz47XHJcbiAgICAgICAgY29uc3QgY2F0ZWdvcnlSb29tTm9kZXMgPSBPYmplY3Qua2V5cyhjYXRlZ29yeVJvb21zKS5tYXAoKGNhdElkKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNhdCA9IHN1bW1hcnkucm9vbXNfc2VjdGlvbi5jYXRlZ29yaWVzW2NhdElkXTtcclxuICAgICAgICAgICAgcmV0dXJuIDxDYXRlZ29yeVJvb21MaXN0XHJcbiAgICAgICAgICAgICAgICBrZXk9e2NhdElkfVxyXG4gICAgICAgICAgICAgICAgcm9vbXM9e2NhdGVnb3J5Um9vbXNbY2F0SWRdfVxyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnk9e2NhdH1cclxuICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgIGVkaXRpbmc9e3RoaXMuc3RhdGUuZWRpdGluZ30gLz47XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nc1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19mZWF0dXJlZFRoaW5nc19oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgIHsgX3QoJ0ZlYXR1cmVkIFJvb21zOicpIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIHsgZGVmYXVsdENhdGVnb3J5Tm9kZSB9XHJcbiAgICAgICAgICAgIHsgY2F0ZWdvcnlSb29tTm9kZXMgfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldEZlYXR1cmVkVXNlcnNOb2RlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBzdW1tYXJ5ID0gdGhpcy5zdGF0ZS5zdW1tYXJ5O1xyXG5cclxuICAgICAgICBjb25zdCBub1JvbGVVc2VycyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHJvbGVVc2VycyA9IHt9O1xyXG4gICAgICAgIHN1bW1hcnkudXNlcnNfc2VjdGlvbi51c2Vycy5mb3JFYWNoKCh1KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh1LnJvbGVfaWQgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG5vUm9sZVVzZXJzLnB1c2godSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGlzdCA9IHJvbGVVc2Vyc1t1LnJvbGVfaWRdO1xyXG4gICAgICAgICAgICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICByb2xlVXNlcnNbdS5yb2xlX2lkXSA9IGxpc3Q7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsaXN0LnB1c2godSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3Qgbm9Sb2xlTm9kZSA9IDxSb2xlVXNlckxpc3RcclxuICAgICAgICAgICAgdXNlcnM9e25vUm9sZVVzZXJzfVxyXG4gICAgICAgICAgICBncm91cElkPXt0aGlzLnByb3BzLmdyb3VwSWR9XHJcbiAgICAgICAgICAgIGVkaXRpbmc9e3RoaXMuc3RhdGUuZWRpdGluZ30gLz47XHJcbiAgICAgICAgY29uc3Qgcm9sZVVzZXJOb2RlcyA9IE9iamVjdC5rZXlzKHJvbGVVc2VycykubWFwKChyb2xlSWQpID0+IHtcclxuICAgICAgICAgICAgY29uc3Qgcm9sZSA9IHN1bW1hcnkudXNlcnNfc2VjdGlvbi5yb2xlc1tyb2xlSWRdO1xyXG4gICAgICAgICAgICByZXR1cm4gPFJvbGVVc2VyTGlzdFxyXG4gICAgICAgICAgICAgICAga2V5PXtyb2xlSWR9XHJcbiAgICAgICAgICAgICAgICB1c2Vycz17cm9sZVVzZXJzW3JvbGVJZF19XHJcbiAgICAgICAgICAgICAgICByb2xlPXtyb2xlfVxyXG4gICAgICAgICAgICAgICAgZ3JvdXBJZD17dGhpcy5wcm9wcy5ncm91cElkfVxyXG4gICAgICAgICAgICAgICAgZWRpdGluZz17dGhpcy5zdGF0ZS5lZGl0aW5nfSAvPjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2ZlYXR1cmVkVGhpbmdzX2hlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgeyBfdCgnRmVhdHVyZWQgVXNlcnM6JykgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgeyBub1JvbGVOb2RlIH1cclxuICAgICAgICAgICAgeyByb2xlVXNlck5vZGVzIH1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRNZW1iZXJzaGlwU2VjdGlvbjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgIGNvbnN0IEJhc2VBdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiYXZhdGFycy5CYXNlQXZhdGFyXCIpO1xyXG5cclxuICAgICAgICBjb25zdCBncm91cCA9IHRoaXMuX21hdHJpeENsaWVudC5nZXRHcm91cCh0aGlzLnByb3BzLmdyb3VwSWQpO1xyXG5cclxuICAgICAgICBpZiAoZ3JvdXAgJiYgZ3JvdXAubXlNZW1iZXJzaGlwID09PSAnaW52aXRlJykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5tZW1iZXJzaGlwQnVzeSB8fCB0aGlzLnN0YXRlLmludml0ZXJQcm9maWxlQnVzeSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNwaW5uZXIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBodHRwSW52aXRlckF2YXRhciA9IHRoaXMuc3RhdGUuaW52aXRlclByb2ZpbGUgP1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50Lm14Y1VybFRvSHR0cChcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmludml0ZXJQcm9maWxlLmF2YXRhclVybCwgMzYsIDM2LFxyXG4gICAgICAgICAgICAgICAgKSA6IG51bGw7XHJcblxyXG4gICAgICAgICAgICBsZXQgaW52aXRlck5hbWUgPSBncm91cC5pbnZpdGVyLnVzZXJJZDtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaW52aXRlclByb2ZpbGUpIHtcclxuICAgICAgICAgICAgICAgIGludml0ZXJOYW1lID0gdGhpcy5zdGF0ZS5pbnZpdGVyUHJvZmlsZS5kaXNwbGF5TmFtZSB8fCBncm91cC5pbnZpdGVyLnVzZXJJZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfbWVtYmVyc2hpcFNlY3Rpb24gbXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTZWN0aW9uX2ludml0ZWRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTdWJTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfbWVtYmVyc2hpcFNlY3Rpb25fZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEJhc2VBdmF0YXIgdXJsPXtodHRwSW52aXRlckF2YXRhcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9e2ludml0ZXJOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezM2fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PXszNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIiUoaW52aXRlcilzIGhhcyBpbnZpdGVkIHlvdSB0byBqb2luIHRoaXMgY29tbXVuaXR5XCIsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGludml0ZXI6IGludml0ZXJOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfbWVtYmVyc2hpcF9idXR0b25Db250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X3RleHRCdXR0b24gbXhfUm9vbUhlYWRlcl90ZXh0QnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQWNjZXB0SW52aXRlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJBY2NlcHRcIikgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld190ZXh0QnV0dG9uIG14X1Jvb21IZWFkZXJfdGV4dEJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblJlamVjdEludml0ZUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiRGVjbGluZVwiKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgbWVtYmVyc2hpcENvbnRhaW5lckV4dHJhQ2xhc3NlcztcclxuICAgICAgICBsZXQgbWVtYmVyc2hpcEJ1dHRvbkV4dHJhQ2xhc3NlcztcclxuICAgICAgICBsZXQgbWVtYmVyc2hpcEJ1dHRvblRvb2x0aXA7XHJcbiAgICAgICAgbGV0IG1lbWJlcnNoaXBCdXR0b25UZXh0O1xyXG4gICAgICAgIGxldCBtZW1iZXJzaGlwQnV0dG9uT25DbGljaztcclxuXHJcbiAgICAgICAgLy8gVXNlciBpcyBub3QgaW4gdGhlIGdyb3VwXHJcbiAgICAgICAgaWYgKCghZ3JvdXAgfHwgZ3JvdXAubXlNZW1iZXJzaGlwID09PSAnbGVhdmUnKSAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnN1bW1hcnkgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5zdW1tYXJ5LnByb2ZpbGUgJiZcclxuICAgICAgICAgICAgQm9vbGVhbih0aGlzLnN0YXRlLnN1bW1hcnkucHJvZmlsZS5pc19vcGVubHlfam9pbmFibGUpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIG1lbWJlcnNoaXBCdXR0b25UZXh0ID0gX3QoXCJKb2luIHRoaXMgY29tbXVuaXR5XCIpO1xyXG4gICAgICAgICAgICBtZW1iZXJzaGlwQnV0dG9uT25DbGljayA9IHRoaXMuX29uSm9pbkNsaWNrO1xyXG5cclxuICAgICAgICAgICAgbWVtYmVyc2hpcEJ1dHRvbkV4dHJhQ2xhc3NlcyA9ICdteF9Hcm91cFZpZXdfam9pbkJ1dHRvbic7XHJcbiAgICAgICAgICAgIG1lbWJlcnNoaXBDb250YWluZXJFeHRyYUNsYXNzZXMgPSAnbXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTZWN0aW9uX2xlYXZlJztcclxuICAgICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgICAgICBncm91cCAmJlxyXG4gICAgICAgICAgICBncm91cC5teU1lbWJlcnNoaXAgPT09ICdqb2luJyAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmVkaXRpbmdcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgbWVtYmVyc2hpcEJ1dHRvblRleHQgPSBfdChcIkxlYXZlIHRoaXMgY29tbXVuaXR5XCIpO1xyXG4gICAgICAgICAgICBtZW1iZXJzaGlwQnV0dG9uT25DbGljayA9IHRoaXMuX29uTGVhdmVDbGljaztcclxuICAgICAgICAgICAgbWVtYmVyc2hpcEJ1dHRvblRvb2x0aXAgPSB0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQgP1xyXG4gICAgICAgICAgICAgICAgX3QoXCJZb3UgYXJlIGFuIGFkbWluaXN0cmF0b3Igb2YgdGhpcyBjb21tdW5pdHlcIikgOlxyXG4gICAgICAgICAgICAgICAgX3QoXCJZb3UgYXJlIGEgbWVtYmVyIG9mIHRoaXMgY29tbXVuaXR5XCIpO1xyXG5cclxuICAgICAgICAgICAgbWVtYmVyc2hpcEJ1dHRvbkV4dHJhQ2xhc3NlcyA9IHtcclxuICAgICAgICAgICAgICAgICdteF9Hcm91cFZpZXdfbGVhdmVCdXR0b24nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgJ214X1Jvb21IZWFkZXJfdGV4dEJ1dHRvbl9kYW5nZXInOiB0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIG1lbWJlcnNoaXBDb250YWluZXJFeHRyYUNsYXNzZXMgPSAnbXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTZWN0aW9uX2pvaW5lZCc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBtZW1iZXJzaGlwQnV0dG9uQ2xhc3NlcyA9IGNsYXNzbmFtZXMoW1xyXG4gICAgICAgICAgICAnbXhfUm9vbUhlYWRlcl90ZXh0QnV0dG9uJyxcclxuICAgICAgICAgICAgJ214X0dyb3VwVmlld190ZXh0QnV0dG9uJyxcclxuICAgICAgICBdLFxyXG4gICAgICAgICAgICBtZW1iZXJzaGlwQnV0dG9uRXh0cmFDbGFzc2VzLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1lbWJlcnNoaXBDb250YWluZXJDbGFzc2VzID0gY2xhc3NuYW1lcyhcclxuICAgICAgICAgICAgJ214X0dyb3VwVmlld19tZW1iZXJzaGlwU2VjdGlvbicsXHJcbiAgICAgICAgICAgIG1lbWJlcnNoaXBDb250YWluZXJFeHRyYUNsYXNzZXMsXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXttZW1iZXJzaGlwQ29udGFpbmVyQ2xhc3Nlc30+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X21lbWJlcnNoaXBTdWJTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICB7IC8qIFRoZSA8ZGl2IC8+IGlzIGZvciBmbGV4IGFsaWdubWVudCAqLyB9XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUubWVtYmVyc2hpcEJ1c3kgPyA8U3Bpbm5lciAvPiA6IDxkaXYgLz4gfVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfbWVtYmVyc2hpcF9idXR0b25Db250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e21lbWJlcnNoaXBCdXR0b25DbGFzc2VzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXttZW1iZXJzaGlwQnV0dG9uT25DbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e21lbWJlcnNoaXBCdXR0b25Ub29sdGlwfVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBtZW1iZXJzaGlwQnV0dG9uVGV4dCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldEpvaW5hYmxlTm9kZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgSW5saW5lU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLklubGluZVNwaW5uZXInKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5lZGl0aW5nID8gPGRpdj5cclxuICAgICAgICAgICAgPGgzPlxyXG4gICAgICAgICAgICAgICAgeyBfdCgnV2hvIGNhbiBqb2luIHRoaXMgY29tbXVuaXR5PycpIH1cclxuICAgICAgICAgICAgICAgIHsgdGhpcy5zdGF0ZS5ncm91cEpvaW5hYmxlTG9hZGluZyA/XHJcbiAgICAgICAgICAgICAgICAgICAgPElubGluZVNwaW5uZXIgLz4gOiA8ZGl2IC8+XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDwvaDM+XHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtHUk9VUF9KT0lOUE9MSUNZX0lOVklURX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17dGhpcy5zdGF0ZS5qb2luYWJsZUZvcm0ucG9saWN5VHlwZSA9PT0gR1JPVVBfSk9JTlBPTElDWV9JTlZJVEV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkpvaW5hYmxlQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfbGFiZWxfdGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdPbmx5IHBlb3BsZSB3aG8gaGF2ZSBiZWVuIGludml0ZWQnKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e0dST1VQX0pPSU5QT0xJQ1lfT1BFTn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17dGhpcy5zdGF0ZS5qb2luYWJsZUZvcm0ucG9saWN5VHlwZSA9PT0gR1JPVVBfSk9JTlBPTElDWV9PUEVOfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25Kb2luYWJsZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2xhYmVsX3RleHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdCgnRXZlcnlvbmUnKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj4gOiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0TG9uZ0Rlc2NyaXB0aW9uTm9kZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgc3VtbWFyeSA9IHRoaXMuc3RhdGUuc3VtbWFyeTtcclxuICAgICAgICBsZXQgZGVzY3JpcHRpb24gPSBudWxsO1xyXG4gICAgICAgIGlmIChzdW1tYXJ5LnByb2ZpbGUgJiYgc3VtbWFyeS5wcm9maWxlLmxvbmdfZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgZGVzY3JpcHRpb24gPSBzYW5pdGl6ZWRIdG1sTm9kZShzdW1tYXJ5LnByb2ZpbGUubG9uZ19kZXNjcmlwdGlvbik7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQpIHtcclxuICAgICAgICAgICAgZGVzY3JpcHRpb24gPSA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfZ3JvdXBEZXNjX3BsYWNlaG9sZGVyXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uRWRpdENsaWNrfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICdZb3VyIGNvbW11bml0eSBoYXNuXFwndCBnb3QgYSBMb25nIERlc2NyaXB0aW9uLCBhIEhUTUwgcGFnZSB0byBzaG93IHRvIGNvbW11bml0eSBtZW1iZXJzLjxiciAvPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICdDbGljayBoZXJlIHRvIG9wZW4gc2V0dGluZ3MgYW5kIGdpdmUgaXQgb25lIScsXHJcbiAgICAgICAgICAgICAgICAgICAge30sXHJcbiAgICAgICAgICAgICAgICAgICAgeyAnYnInOiA8YnIgLz4gfSxcclxuICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGdyb3VwRGVzY0VkaXRpbmdDbGFzc2VzID0gY2xhc3NuYW1lcyh7XHJcbiAgICAgICAgICAgIFwibXhfR3JvdXBWaWV3X2dyb3VwRGVzY1wiOiB0cnVlLFxyXG4gICAgICAgICAgICBcIm14X0dyb3VwVmlld19ncm91cERlc2NfZGlzYWJsZWRcIjogIXRoaXMuc3RhdGUuaXNVc2VyUHJpdmlsZWdlZCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuZWRpdGluZyA/XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtncm91cERlc2NFZGl0aW5nQ2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICA8aDM+IHsgX3QoXCJMb25nIERlc2NyaXB0aW9uIChIVE1MKVwiKSB9IDwvaDM+XHJcbiAgICAgICAgICAgICAgICA8dGV4dGFyZWFcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5wcm9maWxlRm9ybS5sb25nX2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtfdChMT05HX0RFU0NfUExBQ0VIT0xERVIpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkxvbmdEZXNjQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHRhYkluZGV4PVwiNFwiXHJcbiAgICAgICAgICAgICAgICAgICAga2V5PVwiZWRpdExvbmdEZXNjXCJcclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PiA6XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2dyb3VwRGVzY1wiPlxyXG4gICAgICAgICAgICAgICAgeyBkZXNjcmlwdGlvbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBHcm91cEF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoXCJhdmF0YXJzLkdyb3VwQXZhdGFyXCIpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuc3VtbWFyeUxvYWRpbmcgJiYgdGhpcy5zdGF0ZS5lcnJvciA9PT0gbnVsbCB8fCB0aGlzLnN0YXRlLnNhdmluZykge1xyXG4gICAgICAgICAgICByZXR1cm4gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnN1bW1hcnkgJiYgIXRoaXMuc3RhdGUuZXJyb3IpIHtcclxuICAgICAgICAgICAgY29uc3Qgc3VtbWFyeSA9IHRoaXMuc3RhdGUuc3VtbWFyeTtcclxuXHJcbiAgICAgICAgICAgIGxldCBhdmF0YXJOb2RlO1xyXG4gICAgICAgICAgICBsZXQgbmFtZU5vZGU7XHJcbiAgICAgICAgICAgIGxldCBzaG9ydERlc2NOb2RlO1xyXG4gICAgICAgICAgICBjb25zdCByaWdodEJ1dHRvbnMgPSBbXTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZWRpdGluZyAmJiB0aGlzLnN0YXRlLmlzVXNlclByaXZpbGVnZWQpIHtcclxuICAgICAgICAgICAgICAgIGxldCBhdmF0YXJJbWFnZTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnVwbG9hZGluZ0F2YXRhcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGF2YXRhckltYWdlID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEdyb3VwQXZhdGFyID0gc2RrLmdldENvbXBvbmVudCgnYXZhdGFycy5Hcm91cEF2YXRhcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIGF2YXRhckltYWdlID0gPEdyb3VwQXZhdGFyIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZ3JvdXBOYW1lPXt0aGlzLnN0YXRlLnByb2ZpbGVGb3JtLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdyb3VwQXZhdGFyVXJsPXt0aGlzLnN0YXRlLnByb2ZpbGVGb3JtLmF2YXRhcl91cmx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPXsyOH0gaGVpZ2h0PXsyOH0gcmVzaXplTWV0aG9kPSdjcm9wJ1xyXG4gICAgICAgICAgICAgICAgICAgIC8+O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGF2YXRhck5vZGUgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfYXZhdGFyUGlja2VyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwiYXZhdGFySW5wdXRcIiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfYXZhdGFyUGlja2VyX2xhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGF2YXRhckltYWdlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfYXZhdGFyUGlja2VyX2VkaXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwiYXZhdGFySW5wdXRcIiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfYXZhdGFyUGlja2VyX2xhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi9yZXMvaW1nL2NhbWVyYS5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17X3QoXCJVcGxvYWQgYXZhdGFyXCIpfSB0aXRsZT17X3QoXCJVcGxvYWQgYXZhdGFyXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjE3XCIgaGVpZ2h0PVwiMTVcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImF2YXRhcklucHV0XCIgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X3VwbG9hZElucHV0XCIgdHlwZT1cImZpbGVcIiBvbkNoYW5nZT17dGhpcy5fb25BdmF0YXJTZWxlY3RlZH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IEVkaXRhYmxlVGV4dCA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5FZGl0YWJsZVRleHRcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgbmFtZU5vZGUgPSA8RWRpdGFibGVUZXh0XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2VkaXRhYmxlXCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlckNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19wbGFjZWhvbGRlclwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e190KCdDb21tdW5pdHkgTmFtZScpfVxyXG4gICAgICAgICAgICAgICAgICAgIGJsdXJUb0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgaW5pdGlhbFZhbHVlPXt0aGlzLnN0YXRlLnByb2ZpbGVGb3JtLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25WYWx1ZUNoYW5nZWQ9e3RoaXMuX29uTmFtZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICB0YWJJbmRleD1cIjBcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRpcj1cImF1dG9cIiAvPjtcclxuXHJcbiAgICAgICAgICAgICAgICBzaG9ydERlc2NOb2RlID0gPEVkaXRhYmxlVGV4dFxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19lZGl0YWJsZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXJDbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfcGxhY2Vob2xkZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtfdChcIkRlc2NyaXB0aW9uXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIGJsdXJUb0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgaW5pdGlhbFZhbHVlPXt0aGlzLnN0YXRlLnByb2ZpbGVGb3JtLnNob3J0X2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uVmFsdWVDaGFuZ2VkPXt0aGlzLl9vblNob3J0RGVzY0NoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICB0YWJJbmRleD1cIjBcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRpcj1cImF1dG9cIiAvPjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9uR3JvdXBIZWFkZXJJdGVtQ2xpY2sgPSB0aGlzLnN0YXRlLmlzVXNlck1lbWJlciA/IHRoaXMuX29uRWRpdENsaWNrIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGdyb3VwQXZhdGFyVXJsID0gc3VtbWFyeS5wcm9maWxlID8gc3VtbWFyeS5wcm9maWxlLmF2YXRhcl91cmwgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZ3JvdXBOYW1lID0gc3VtbWFyeS5wcm9maWxlID8gc3VtbWFyeS5wcm9maWxlLm5hbWUgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgYXZhdGFyTm9kZSA9IDxHcm91cEF2YXRhclxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgICAgICBncm91cEF2YXRhclVybD17Z3JvdXBBdmF0YXJVcmx9XHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBOYW1lPXtncm91cE5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17b25Hcm91cEhlYWRlckl0ZW1DbGlja31cclxuICAgICAgICAgICAgICAgICAgICB3aWR0aD17Mjh9IGhlaWdodD17Mjh9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgICAgIGlmIChzdW1tYXJ5LnByb2ZpbGUgJiYgc3VtbWFyeS5wcm9maWxlLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lTm9kZSA9IDxkaXYgb25DbGljaz17b25Hcm91cEhlYWRlckl0ZW1DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnsgc3VtbWFyeS5wcm9maWxlLm5hbWUgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2hlYWRlcl9ncm91cGlkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyB0aGlzLnByb3BzLmdyb3VwSWQgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZU5vZGUgPSA8c3BhbiBvbkNsaWNrPXtvbkdyb3VwSGVhZGVySXRlbUNsaWNrfT57IHRoaXMucHJvcHMuZ3JvdXBJZCB9PC9zcGFuPjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChzdW1tYXJ5LnByb2ZpbGUgJiYgc3VtbWFyeS5wcm9maWxlLnNob3J0X2Rlc2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvcnREZXNjTm9kZSA9IDxzcGFuIG9uQ2xpY2s9e29uR3JvdXBIZWFkZXJJdGVtQ2xpY2t9Pnsgc3VtbWFyeS5wcm9maWxlLnNob3J0X2Rlc2NyaXB0aW9uIH08L3NwYW4+O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5lZGl0aW5nKSB7XHJcbiAgICAgICAgICAgICAgICByaWdodEJ1dHRvbnMucHVzaChcclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfdGV4dEJ1dHRvbiBteF9Sb29tSGVhZGVyX3RleHRCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk9XCJfc2F2ZUJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uU2F2ZUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdCgnU2F2ZScpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHJpZ2h0QnV0dG9ucy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfY2FuY2VsQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PVwiX2NhbmNlbEJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQ2FuY2VsQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uL3Jlcy9pbWcvY2FuY2VsLnN2Z1wiKX0gY2xhc3NOYW1lPVwibXhfZmlsdGVyRmxpcENvbG9yXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMThcIiBoZWlnaHQ9XCIxOFwiIGFsdD17X3QoXCJDYW5jZWxcIil9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3VtbWFyeS51c2VyICYmIHN1bW1hcnkudXNlci5tZW1iZXJzaGlwID09PSAnam9pbicpIHtcclxuICAgICAgICAgICAgICAgICAgICByaWdodEJ1dHRvbnMucHVzaChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfR3JvdXBIZWFkZXJfYnV0dG9uIG14X0dyb3VwSGVhZGVyX2VkaXRCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PVwiX2VkaXRCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25FZGl0Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17X3QoXCJDb21tdW5pdHkgU2V0dGluZ3NcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPixcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmlnaHRCdXR0b25zLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfR3JvdXBIZWFkZXJfYnV0dG9uIG14X0dyb3VwSGVhZGVyX3NoYXJlQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PVwiX3NoYXJlQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25TaGFyZUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17X3QoJ1NoYXJlIENvbW11bml0eScpfVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgcmlnaHRQYW5lbCA9IHRoaXMuc3RhdGUuc2hvd1JpZ2h0UGFuZWwgPyA8UmlnaHRQYW5lbCBncm91cElkPXt0aGlzLnByb3BzLmdyb3VwSWR9IC8+IDogdW5kZWZpbmVkO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgaGVhZGVyQ2xhc3NlcyA9IHtcclxuICAgICAgICAgICAgICAgIFwibXhfR3JvdXBWaWV3X2hlYWRlclwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJsaWdodC1wYW5lbFwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Hcm91cFZpZXdfaGVhZGVyX3ZpZXdcIjogIXRoaXMuc3RhdGUuZWRpdGluZyxcclxuICAgICAgICAgICAgICAgIFwibXhfR3JvdXBWaWV3X2hlYWRlcl9pc1VzZXJNZW1iZXJcIjogdGhpcy5zdGF0ZS5pc1VzZXJNZW1iZXIsXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPG1haW4gY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzbmFtZXMoaGVhZGVyQ2xhc3Nlcyl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19oZWFkZXJfbGVmdENvbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfaGVhZGVyX2F2YXRhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgYXZhdGFyTm9kZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfR3JvdXBWaWV3X2hlYWRlcl9pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfaGVhZGVyX25hbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBuYW1lTm9kZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFZpZXdfaGVhZGVyX3Nob3J0RGVzY1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHNob3J0RGVzY05vZGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19oZWFkZXJfcmlnaHRDb2xcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgcmlnaHRCdXR0b25zIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxHcm91cEhlYWRlckJ1dHRvbnMgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8TWFpblNwbGl0IHBhbmVsPXtyaWdodFBhbmVsfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEF1dG9IaWRlU2Nyb2xsYmFyIGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuX2dldE1lbWJlcnNoaXBTZWN0aW9uKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9nZXRHcm91cFNlY3Rpb24oKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQXV0b0hpZGVTY3JvbGxiYXI+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9NYWluU3BsaXQ+XHJcbiAgICAgICAgICAgICAgICA8L21haW4+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmVycm9yKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmVycm9yLmh0dHBTdGF0dXMgPT09IDQwNCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19lcnJvclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdDb21tdW5pdHkgJShncm91cElkKXMgbm90IGZvdW5kJywge2dyb3VwSWQ6IHRoaXMucHJvcHMuZ3JvdXBJZH0pIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZXh0cmFUZXh0O1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3IuZXJyY29kZSA9PT0gJ01fVU5SRUNPR05JWkVEJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4dHJhVGV4dCA9IDxkaXY+eyBfdCgnVGhpcyBob21lc2VydmVyIGRvZXMgbm90IHN1cHBvcnQgY29tbXVuaXRpZXMnKSB9PC9kaXY+O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwVmlld19lcnJvclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdGYWlsZWQgdG8gbG9hZCAlKGdyb3VwSWQpcycsIHtncm91cElkOiB0aGlzLnByb3BzLmdyb3VwSWQgfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGV4dHJhVGV4dCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkludmFsaWQgc3RhdGUgZm9yIEdyb3VwVmlld1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYgLz47XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==