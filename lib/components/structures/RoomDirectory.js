"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../index"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var _HtmlUtils = require("../../HtmlUtils");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../languageHandler");

var _DirectoryUtils = require("../../utils/DirectoryUtils");

var _Analytics = _interopRequireDefault(require("../../Analytics"));

var _contentRepo = require("matrix-js-sdk/src/content-repo");

var _NetworkDropdown = require("../views/directory/NetworkDropdown");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const MAX_NAME_LENGTH = 80;
const MAX_TOPIC_LENGTH = 160;

function track(action) {
  _Analytics.default.trackEvent('RoomDirectory', action);
}

var _default = (0, _createReactClass.default)({
  displayName: 'RoomDirectory',
  propTypes: {
    onFinished: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    return {
      publicRooms: [],
      loading: true,
      protocolsLoading: true,
      error: null,
      instanceId: undefined,
      roomServer: _MatrixClientPeg.MatrixClientPeg.getHomeserverName(),
      filterString: null
    };
  },
  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    this._unmounted = false;
    this.nextBatch = null;
    this.filterTimeout = null;
    this.scrollPanel = null;
    this.protocols = null;
    this.setState({
      protocolsLoading: true
    });

    if (!_MatrixClientPeg.MatrixClientPeg.get()) {
      // We may not have a client yet when invoked from welcome page
      this.setState({
        protocolsLoading: false
      });
      return;
    }

    _MatrixClientPeg.MatrixClientPeg.get().getThirdpartyProtocols().then(response => {
      this.protocols = response;
      this.setState({
        protocolsLoading: false
      });
    }, err => {
      console.warn("error loading thirdparty protocols: ".concat(err));
      this.setState({
        protocolsLoading: false
      });

      if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
        // Guests currently aren't allowed to use this API, so
        // ignore this as otherwise this error is literally the
        // thing you see when loading the client!
        return;
      }

      track('Failed to get protocol list from homeserver');
      this.setState({
        error: (0, _languageHandler._t)('Riot failed to get the protocol list from the homeserver. ' + 'The homeserver may be too old to support third party networks.')
      });
    });

    this.refreshRoomList();
  },
  componentWillUnmount: function () {
    if (this.filterTimeout) {
      clearTimeout(this.filterTimeout);
    }

    this._unmounted = true;
  },
  refreshRoomList: function () {
    this.nextBatch = null;
    this.setState({
      publicRooms: [],
      loading: true
    });
    this.getMoreRooms();
  },
  getMoreRooms: function () {
    if (!_MatrixClientPeg.MatrixClientPeg.get()) return Promise.resolve();
    this.setState({
      loading: true
    });
    const my_filter_string = this.state.filterString;
    const my_server = this.state.roomServer; // remember the next batch token when we sent the request
    // too. If it's changed, appending to the list will corrupt it.

    const my_next_batch = this.nextBatch;
    const opts = {
      limit: 20
    };

    if (my_server != _MatrixClientPeg.MatrixClientPeg.getHomeserverName()) {
      opts.server = my_server;
    }

    if (this.state.instanceId === _NetworkDropdown.ALL_ROOMS) {
      opts.include_all_networks = true;
    } else if (this.state.instanceId) {
      opts.third_party_instance_id = this.state.instanceId;
    }

    if (this.nextBatch) opts.since = this.nextBatch;
    if (my_filter_string) opts.filter = {
      generic_search_term: my_filter_string
    };
    return _MatrixClientPeg.MatrixClientPeg.get().publicRooms(opts).then(data => {
      if (my_filter_string != this.state.filterString || my_server != this.state.roomServer || my_next_batch != this.nextBatch) {
        // if the filter or server has changed since this request was sent,
        // throw away the result (don't even clear the busy flag
        // since we must still have a request in flight)
        return;
      }

      if (this._unmounted) {
        // if we've been unmounted, we don't care either.
        return;
      }

      this.nextBatch = data.next_batch;
      this.setState(s => {
        s.publicRooms.push(...(data.chunk || []));
        s.loading = false;
        return s;
      });
      return Boolean(data.next_batch);
    }, err => {
      if (my_filter_string != this.state.filterString || my_server != this.state.roomServer || my_next_batch != this.nextBatch) {
        // as above: we don't care about errors for old
        // requests either
        return;
      }

      if (this._unmounted) {
        // if we've been unmounted, we don't care either.
        return;
      }

      console.error("Failed to get publicRooms: %s", JSON.stringify(err));
      track('Failed to get public room list');
      this.setState({
        loading: false,
        error: "".concat((0, _languageHandler._t)('Riot failed to get the public room list.'), " ") + "".concat(err && err.message ? err.message : (0, _languageHandler._t)('The homeserver may be unavailable or overloaded.'))
      });
    });
  },

  /**
   * A limited interface for removing rooms from the directory.
   * Will set the room to not be publicly visible and delete the
   * default alias. In the long term, it would be better to allow
   * HS admins to do this through the RoomSettings interface, but
   * this needs SPEC-417.
   */
  removeFromDirectory: function (room) {
    const alias = get_display_alias_for_room(room);
    const name = room.name || alias || (0, _languageHandler._t)('Unnamed room');
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    let desc;

    if (alias) {
      desc = (0, _languageHandler._t)('Delete the room alias %(alias)s and remove %(name)s from the directory?', {
        alias: alias,
        name: name
      });
    } else {
      desc = (0, _languageHandler._t)('Remove %(name)s from the directory?', {
        name: name
      });
    }

    _Modal.default.createTrackedDialog('Remove from Directory', '', QuestionDialog, {
      title: (0, _languageHandler._t)('Remove from Directory'),
      description: desc,
      onFinished: should_delete => {
        if (!should_delete) return;
        const Loader = sdk.getComponent("elements.Spinner");

        const modal = _Modal.default.createDialog(Loader);

        let step = (0, _languageHandler._t)('remove %(name)s from the directory.', {
          name: name
        });

        _MatrixClientPeg.MatrixClientPeg.get().setRoomDirectoryVisibility(room.room_id, 'private').then(() => {
          if (!alias) return;
          step = (0, _languageHandler._t)('delete the alias.');
          return _MatrixClientPeg.MatrixClientPeg.get().deleteAlias(alias);
        }).then(() => {
          modal.close();
          this.refreshRoomList();
        }, err => {
          modal.close();
          this.refreshRoomList();
          console.error("Failed to " + step + ": " + err);

          _Modal.default.createTrackedDialog('Remove from Directory Error', '', ErrorDialog, {
            title: (0, _languageHandler._t)('Error'),
            description: err && err.message ? err.message : (0, _languageHandler._t)('The server may be unavailable or overloaded')
          });
        });
      }
    });
  },
  onRoomClicked: function (room, ev) {
    if (ev.shiftKey) {
      ev.preventDefault();
      this.removeFromDirectory(room);
    } else {
      this.showRoom(room);
    }
  },
  onOptionChange: function (server, instanceId) {
    // clear next batch so we don't try to load more rooms
    this.nextBatch = null;
    this.setState({
      // Clear the public rooms out here otherwise we needlessly
      // spend time filtering lots of rooms when we're about to
      // to clear the list anyway.
      publicRooms: [],
      roomServer: server,
      instanceId: instanceId,
      error: null
    }, this.refreshRoomList); // We also refresh the room list each time even though this
    // filtering is client-side. It hopefully won't be client side
    // for very long, and we may have fetched a thousand rooms to
    // find the five gitter ones, at which point we do not want
    // to render all those rooms when switching back to 'all networks'.
    // Easiest to just blow away the state & re-fetch.
  },
  onFillRequest: function (backwards) {
    if (backwards || !this.nextBatch) return Promise.resolve(false);
    return this.getMoreRooms();
  },
  onFilterChange: function (alias) {
    this.setState({
      filterString: alias || null
    }); // don't send the request for a little bit,
    // no point hammering the server with a
    // request for every keystroke, let the
    // user finish typing.

    if (this.filterTimeout) {
      clearTimeout(this.filterTimeout);
    }

    this.filterTimeout = setTimeout(() => {
      this.filterTimeout = null;
      this.refreshRoomList();
    }, 700);
  },
  onFilterClear: function () {
    // update immediately
    this.setState({
      filterString: null
    }, this.refreshRoomList);

    if (this.filterTimeout) {
      clearTimeout(this.filterTimeout);
    }
  },
  onJoinFromSearchClick: function (alias) {
    // If we don't have a particular instance id selected, just show that rooms alias
    if (!this.state.instanceId || this.state.instanceId === _NetworkDropdown.ALL_ROOMS) {
      // If the user specified an alias without a domain, add on whichever server is selected
      // in the dropdown
      if (alias.indexOf(':') == -1) {
        alias = alias + ':' + this.state.roomServer;
      }

      this.showRoomAlias(alias, true);
    } else {
      // This is a 3rd party protocol. Let's see if we can join it
      const protocolName = (0, _DirectoryUtils.protocolNameForInstanceId)(this.protocols, this.state.instanceId);
      const instance = (0, _DirectoryUtils.instanceForInstanceId)(this.protocols, this.state.instanceId);
      const fields = protocolName ? this._getFieldsForThirdPartyLocation(alias, this.protocols[protocolName], instance) : null;

      if (!fields) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Unable to join network', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Unable to join network'),
          description: (0, _languageHandler._t)('Riot does not know how to join a room on this network')
        });

        return;
      }

      _MatrixClientPeg.MatrixClientPeg.get().getThirdpartyLocation(protocolName, fields).then(resp => {
        if (resp.length > 0 && resp[0].alias) {
          this.showRoomAlias(resp[0].alias, true);
        } else {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Room not found', '', ErrorDialog, {
            title: (0, _languageHandler._t)('Room not found'),
            description: (0, _languageHandler._t)('Couldn\'t find a matching Matrix room')
          });
        }
      }, e => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Fetching third party location failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Fetching third party location failed'),
          description: (0, _languageHandler._t)('Unable to look up room ID from server')
        });
      });
    }
  },
  onPreviewClick: function (ev, room) {
    this.props.onFinished();

    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: room.room_id,
      should_peek: true
    });

    ev.stopPropagation();
  },
  onViewClick: function (ev, room) {
    this.props.onFinished();

    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: room.room_id,
      should_peek: false
    });

    ev.stopPropagation();
  },
  onJoinClick: function (ev, room) {
    this.showRoom(room, null, true);
    ev.stopPropagation();
  },
  onCreateRoomClick: function (room) {
    this.props.onFinished();

    _dispatcher.default.dispatch({
      action: 'view_create_room'
    });
  },
  showRoomAlias: function (alias, autoJoin = false) {
    this.showRoom(null, alias, autoJoin);
  },
  showRoom: function (room, room_alias, autoJoin = false) {
    this.props.onFinished();
    const payload = {
      action: 'view_room',
      auto_join: autoJoin
    };

    if (room) {
      // Don't let the user view a room they won't be able to either
      // peek or join: fail earlier so they don't have to click back
      // to the directory.
      if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
        if (!room.world_readable && !room.guest_can_join) {
          _dispatcher.default.dispatch({
            action: 'require_registration'
          });

          return;
        }
      }

      if (!room_alias) {
        room_alias = get_display_alias_for_room(room);
      }

      payload.oob_data = {
        avatarUrl: room.avatar_url,
        // XXX: This logic is duplicated from the JS SDK which
        // would normally decide what the name is.
        name: room.name || room_alias || (0, _languageHandler._t)('Unnamed room')
      };

      if (this.state.roomServer) {
        payload.opts = {
          viaServers: [this.state.roomServer]
        };
      }
    } // It's not really possible to join Matrix rooms by ID because the HS has no way to know
    // which servers to start querying. However, there's no other way to join rooms in
    // this list without aliases at present, so if roomAlias isn't set here we have no
    // choice but to supply the ID.


    if (room_alias) {
      payload.room_alias = room_alias;
    } else {
      payload.room_id = room.room_id;
    }

    _dispatcher.default.dispatch(payload);
  },

  getRow(room) {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const clientRoom = client.getRoom(room.room_id);
    const hasJoinedRoom = clientRoom && clientRoom.getMyMembership() === "join";
    const isGuest = client.isGuest();
    const BaseAvatar = sdk.getComponent('avatars.BaseAvatar');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let previewButton;
    let joinOrViewButton;

    if (room.world_readable && !hasJoinedRoom) {
      previewButton = _react.default.createElement(AccessibleButton, {
        kind: "secondary",
        onClick: ev => this.onPreviewClick(ev, room)
      }, (0, _languageHandler._t)("Preview"));
    }

    if (hasJoinedRoom) {
      joinOrViewButton = _react.default.createElement(AccessibleButton, {
        kind: "secondary",
        onClick: ev => this.onViewClick(ev, room)
      }, (0, _languageHandler._t)("View"));
    } else if (!isGuest || room.guest_can_join) {
      joinOrViewButton = _react.default.createElement(AccessibleButton, {
        kind: "primary",
        onClick: ev => this.onJoinClick(ev, room)
      }, (0, _languageHandler._t)("Join"));
    }

    let name = room.name || get_display_alias_for_room(room) || (0, _languageHandler._t)('Unnamed room');

    if (name.length > MAX_NAME_LENGTH) {
      name = "".concat(name.substring(0, MAX_NAME_LENGTH), "...");
    }

    let topic = room.topic || '';

    if (topic.length > MAX_TOPIC_LENGTH) {
      topic = "".concat(topic.substring(0, MAX_TOPIC_LENGTH), "...");
    }

    topic = (0, _HtmlUtils.linkifyAndSanitizeHtml)(topic);
    const avatarUrl = (0, _contentRepo.getHttpUriForMxc)(_MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl(), room.avatar_url, 32, 32, "crop");
    return _react.default.createElement("tr", {
      key: room.room_id,
      onClick: ev => this.onRoomClicked(room, ev) // cancel onMouseDown otherwise shift-clicking highlights text
      ,
      onMouseDown: ev => {
        ev.preventDefault();
      }
    }, _react.default.createElement("td", {
      className: "mx_RoomDirectory_roomAvatar"
    }, _react.default.createElement(BaseAvatar, {
      width: 32,
      height: 32,
      resizeMethod: "crop",
      name: name,
      idName: name,
      url: avatarUrl
    })), _react.default.createElement("td", {
      className: "mx_RoomDirectory_roomDescription"
    }, _react.default.createElement("div", {
      className: "mx_RoomDirectory_name"
    }, name), "\xA0", _react.default.createElement("div", {
      className: "mx_RoomDirectory_topic",
      onClick: ev => {
        ev.stopPropagation();
      },
      dangerouslySetInnerHTML: {
        __html: topic
      }
    }), _react.default.createElement("div", {
      className: "mx_RoomDirectory_alias"
    }, get_display_alias_for_room(room))), _react.default.createElement("td", {
      className: "mx_RoomDirectory_roomMemberCount"
    }, room.num_joined_members), _react.default.createElement("td", {
      className: "mx_RoomDirectory_preview"
    }, previewButton), _react.default.createElement("td", {
      className: "mx_RoomDirectory_join"
    }, joinOrViewButton));
  },

  collectScrollPanel: function (element) {
    this.scrollPanel = element;
  },
  _stringLooksLikeId: function (s, field_type) {
    let pat = /^#[^\s]+:[^\s]/;

    if (field_type && field_type.regexp) {
      pat = new RegExp(field_type.regexp);
    }

    return pat.test(s);
  },
  _getFieldsForThirdPartyLocation: function (userInput, protocol, instance) {
    // make an object with the fields specified by that protocol. We
    // require that the values of all but the last field come from the
    // instance. The last is the user input.
    const requiredFields = protocol.location_fields;
    if (!requiredFields) return null;
    const fields = {};

    for (let i = 0; i < requiredFields.length - 1; ++i) {
      const thisField = requiredFields[i];
      if (instance.fields[thisField] === undefined) return null;
      fields[thisField] = instance.fields[thisField];
    }

    fields[requiredFields[requiredFields.length - 1]] = userInput;
    return fields;
  },

  /**
   * called by the parent component when PageUp/Down/etc is pressed.
   *
   * We pass it down to the scroll panel.
   */
  handleScrollKey: function (ev) {
    if (this.scrollPanel) {
      this.scrollPanel.handleScrollKey(ev);
    }
  },
  render: function () {
    const Loader = sdk.getComponent("elements.Spinner");
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let content;

    if (this.state.error) {
      content = this.state.error;
    } else if (this.state.protocolsLoading) {
      content = _react.default.createElement(Loader, null);
    } else {
      const rows = (this.state.publicRooms || []).map(room => this.getRow(room)); // we still show the scrollpanel, at least for now, because
      // otherwise we don't fetch more because we don't get a fill
      // request from the scrollpanel because there isn't one

      let spinner;

      if (this.state.loading) {
        spinner = _react.default.createElement(Loader, null);
      }

      let scrollpanel_content;

      if (rows.length === 0 && !this.state.loading) {
        scrollpanel_content = _react.default.createElement("i", null, (0, _languageHandler._t)('No rooms to show'));
      } else {
        scrollpanel_content = _react.default.createElement("table", {
          className: "mx_RoomDirectory_table"
        }, _react.default.createElement("tbody", null, rows));
      }

      const ScrollPanel = sdk.getComponent("structures.ScrollPanel");
      content = _react.default.createElement(ScrollPanel, {
        ref: this.collectScrollPanel,
        className: "mx_RoomDirectory_tableWrapper",
        onFillRequest: this.onFillRequest,
        stickyBottom: false,
        startAtBottom: false
      }, scrollpanel_content, spinner);
    }

    let listHeader;

    if (!this.state.protocolsLoading) {
      const NetworkDropdown = sdk.getComponent('directory.NetworkDropdown');
      const DirectorySearchBox = sdk.getComponent('elements.DirectorySearchBox');
      const protocolName = (0, _DirectoryUtils.protocolNameForInstanceId)(this.protocols, this.state.instanceId);
      let instance_expected_field_type;

      if (protocolName && this.protocols && this.protocols[protocolName] && this.protocols[protocolName].location_fields.length > 0 && this.protocols[protocolName].field_types) {
        const last_field = this.protocols[protocolName].location_fields.slice(-1)[0];
        instance_expected_field_type = this.protocols[protocolName].field_types[last_field];
      }

      let placeholder = (0, _languageHandler._t)('Find a room…');

      if (!this.state.instanceId || this.state.instanceId === _NetworkDropdown.ALL_ROOMS) {
        placeholder = (0, _languageHandler._t)("Find a room… (e.g. %(exampleRoom)s)", {
          exampleRoom: "#example:" + this.state.roomServer
        });
      } else if (instance_expected_field_type) {
        placeholder = instance_expected_field_type.placeholder;
      }

      let showJoinButton = this._stringLooksLikeId(this.state.filterString, instance_expected_field_type);

      if (protocolName) {
        const instance = (0, _DirectoryUtils.instanceForInstanceId)(this.protocols, this.state.instanceId);

        if (this._getFieldsForThirdPartyLocation(this.state.filterString, this.protocols[protocolName], instance) === null) {
          showJoinButton = false;
        }
      }

      listHeader = _react.default.createElement("div", {
        className: "mx_RoomDirectory_listheader"
      }, _react.default.createElement(DirectorySearchBox, {
        className: "mx_RoomDirectory_searchbox",
        onChange: this.onFilterChange,
        onClear: this.onFilterClear,
        onJoinClick: this.onJoinFromSearchClick,
        placeholder: placeholder,
        showJoinButton: showJoinButton
      }), _react.default.createElement(NetworkDropdown, {
        protocols: this.protocols,
        onOptionChange: this.onOptionChange,
        selectedServerName: this.state.roomServer,
        selectedInstanceId: this.state.instanceId
      }));
    }

    const explanation = (0, _languageHandler._t)("If you can't find the room you're looking for, ask for an invite or <a>Create a new room</a>.", null, {
      a: sub => {
        return _react.default.createElement(AccessibleButton, {
          kind: "secondary",
          onClick: this.onCreateRoomClick
        }, sub);
      }
    });
    return _react.default.createElement(BaseDialog, {
      className: 'mx_RoomDirectory_dialog',
      hasCancel: true,
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)("Explore rooms")
    }, _react.default.createElement("div", {
      className: "mx_RoomDirectory"
    }, explanation, _react.default.createElement("div", {
      className: "mx_RoomDirectory_list"
    }, listHeader, content)));
  }
}); // Similar to matrix-react-sdk's MatrixTools.getDisplayAliasForRoom
// but works with the objects we get from the public room list


exports.default = _default;

function get_display_alias_for_room(room) {
  return room.canonical_alias || (room.aliases ? room.aliases[0] : "");
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvUm9vbURpcmVjdG9yeS5qcyJdLCJuYW1lcyI6WyJNQVhfTkFNRV9MRU5HVEgiLCJNQVhfVE9QSUNfTEVOR1RIIiwidHJhY2siLCJhY3Rpb24iLCJBbmFseXRpY3MiLCJ0cmFja0V2ZW50IiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJvbkZpbmlzaGVkIiwiUHJvcFR5cGVzIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJnZXRJbml0aWFsU3RhdGUiLCJwdWJsaWNSb29tcyIsImxvYWRpbmciLCJwcm90b2NvbHNMb2FkaW5nIiwiZXJyb3IiLCJpbnN0YW5jZUlkIiwidW5kZWZpbmVkIiwicm9vbVNlcnZlciIsIk1hdHJpeENsaWVudFBlZyIsImdldEhvbWVzZXJ2ZXJOYW1lIiwiZmlsdGVyU3RyaW5nIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIl91bm1vdW50ZWQiLCJuZXh0QmF0Y2giLCJmaWx0ZXJUaW1lb3V0Iiwic2Nyb2xsUGFuZWwiLCJwcm90b2NvbHMiLCJzZXRTdGF0ZSIsImdldCIsImdldFRoaXJkcGFydHlQcm90b2NvbHMiLCJ0aGVuIiwicmVzcG9uc2UiLCJlcnIiLCJjb25zb2xlIiwid2FybiIsImlzR3Vlc3QiLCJyZWZyZXNoUm9vbUxpc3QiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNsZWFyVGltZW91dCIsImdldE1vcmVSb29tcyIsIlByb21pc2UiLCJyZXNvbHZlIiwibXlfZmlsdGVyX3N0cmluZyIsInN0YXRlIiwibXlfc2VydmVyIiwibXlfbmV4dF9iYXRjaCIsIm9wdHMiLCJsaW1pdCIsInNlcnZlciIsIkFMTF9ST09NUyIsImluY2x1ZGVfYWxsX25ldHdvcmtzIiwidGhpcmRfcGFydHlfaW5zdGFuY2VfaWQiLCJzaW5jZSIsImZpbHRlciIsImdlbmVyaWNfc2VhcmNoX3Rlcm0iLCJkYXRhIiwibmV4dF9iYXRjaCIsInMiLCJwdXNoIiwiY2h1bmsiLCJCb29sZWFuIiwiSlNPTiIsInN0cmluZ2lmeSIsIm1lc3NhZ2UiLCJyZW1vdmVGcm9tRGlyZWN0b3J5Iiwicm9vbSIsImFsaWFzIiwiZ2V0X2Rpc3BsYXlfYWxpYXNfZm9yX3Jvb20iLCJuYW1lIiwiUXVlc3Rpb25EaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJFcnJvckRpYWxvZyIsImRlc2MiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwic2hvdWxkX2RlbGV0ZSIsIkxvYWRlciIsIm1vZGFsIiwiY3JlYXRlRGlhbG9nIiwic3RlcCIsInNldFJvb21EaXJlY3RvcnlWaXNpYmlsaXR5Iiwicm9vbV9pZCIsImRlbGV0ZUFsaWFzIiwiY2xvc2UiLCJvblJvb21DbGlja2VkIiwiZXYiLCJzaGlmdEtleSIsInByZXZlbnREZWZhdWx0Iiwic2hvd1Jvb20iLCJvbk9wdGlvbkNoYW5nZSIsIm9uRmlsbFJlcXVlc3QiLCJiYWNrd2FyZHMiLCJvbkZpbHRlckNoYW5nZSIsInNldFRpbWVvdXQiLCJvbkZpbHRlckNsZWFyIiwib25Kb2luRnJvbVNlYXJjaENsaWNrIiwiaW5kZXhPZiIsInNob3dSb29tQWxpYXMiLCJwcm90b2NvbE5hbWUiLCJpbnN0YW5jZSIsImZpZWxkcyIsIl9nZXRGaWVsZHNGb3JUaGlyZFBhcnR5TG9jYXRpb24iLCJnZXRUaGlyZHBhcnR5TG9jYXRpb24iLCJyZXNwIiwibGVuZ3RoIiwiZSIsIm9uUHJldmlld0NsaWNrIiwicHJvcHMiLCJkaXMiLCJkaXNwYXRjaCIsInNob3VsZF9wZWVrIiwic3RvcFByb3BhZ2F0aW9uIiwib25WaWV3Q2xpY2siLCJvbkpvaW5DbGljayIsIm9uQ3JlYXRlUm9vbUNsaWNrIiwiYXV0b0pvaW4iLCJyb29tX2FsaWFzIiwicGF5bG9hZCIsImF1dG9fam9pbiIsIndvcmxkX3JlYWRhYmxlIiwiZ3Vlc3RfY2FuX2pvaW4iLCJvb2JfZGF0YSIsImF2YXRhclVybCIsImF2YXRhcl91cmwiLCJ2aWFTZXJ2ZXJzIiwiZ2V0Um93IiwiY2xpZW50IiwiY2xpZW50Um9vbSIsImdldFJvb20iLCJoYXNKb2luZWRSb29tIiwiZ2V0TXlNZW1iZXJzaGlwIiwiQmFzZUF2YXRhciIsIkFjY2Vzc2libGVCdXR0b24iLCJwcmV2aWV3QnV0dG9uIiwiam9pbk9yVmlld0J1dHRvbiIsInN1YnN0cmluZyIsInRvcGljIiwiZ2V0SG9tZXNlcnZlclVybCIsIl9faHRtbCIsIm51bV9qb2luZWRfbWVtYmVycyIsImNvbGxlY3RTY3JvbGxQYW5lbCIsImVsZW1lbnQiLCJfc3RyaW5nTG9va3NMaWtlSWQiLCJmaWVsZF90eXBlIiwicGF0IiwicmVnZXhwIiwiUmVnRXhwIiwidGVzdCIsInVzZXJJbnB1dCIsInByb3RvY29sIiwicmVxdWlyZWRGaWVsZHMiLCJsb2NhdGlvbl9maWVsZHMiLCJpIiwidGhpc0ZpZWxkIiwiaGFuZGxlU2Nyb2xsS2V5IiwicmVuZGVyIiwiQmFzZURpYWxvZyIsImNvbnRlbnQiLCJyb3dzIiwibWFwIiwic3Bpbm5lciIsInNjcm9sbHBhbmVsX2NvbnRlbnQiLCJTY3JvbGxQYW5lbCIsImxpc3RIZWFkZXIiLCJOZXR3b3JrRHJvcGRvd24iLCJEaXJlY3RvcnlTZWFyY2hCb3giLCJpbnN0YW5jZV9leHBlY3RlZF9maWVsZF90eXBlIiwiZmllbGRfdHlwZXMiLCJsYXN0X2ZpZWxkIiwic2xpY2UiLCJwbGFjZWhvbGRlciIsImV4YW1wbGVSb29tIiwic2hvd0pvaW5CdXR0b24iLCJleHBsYW5hdGlvbiIsImEiLCJzdWIiLCJjYW5vbmljYWxfYWxpYXMiLCJhbGlhc2VzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0NBLE1BQU1BLGVBQWUsR0FBRyxFQUF4QjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLEdBQXpCOztBQUVBLFNBQVNDLEtBQVQsQ0FBZUMsTUFBZixFQUF1QjtBQUNuQkMscUJBQVVDLFVBQVYsQ0FBcUIsZUFBckIsRUFBc0NGLE1BQXRDO0FBQ0g7O2VBRWMsK0JBQWlCO0FBQzVCRyxFQUFBQSxXQUFXLEVBQUUsZUFEZTtBQUc1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLFVBQVUsRUFBRUMsbUJBQVVDLElBQVYsQ0FBZUM7QUFEcEIsR0FIaUI7QUFPNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsV0FBVyxFQUFFLEVBRFY7QUFFSEMsTUFBQUEsT0FBTyxFQUFFLElBRk47QUFHSEMsTUFBQUEsZ0JBQWdCLEVBQUUsSUFIZjtBQUlIQyxNQUFBQSxLQUFLLEVBQUUsSUFKSjtBQUtIQyxNQUFBQSxVQUFVLEVBQUVDLFNBTFQ7QUFNSEMsTUFBQUEsVUFBVSxFQUFFQyxpQ0FBZ0JDLGlCQUFoQixFQU5UO0FBT0hDLE1BQUFBLFlBQVksRUFBRTtBQVBYLEtBQVA7QUFTSCxHQWpCMkI7QUFtQjVCO0FBQ0FDLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsU0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixJQUFuQjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsSUFBakI7QUFFQSxTQUFLQyxRQUFMLENBQWM7QUFBQ2QsTUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsS0FBZDs7QUFDQSxRQUFJLENBQUNLLGlDQUFnQlUsR0FBaEIsRUFBTCxFQUE0QjtBQUN4QjtBQUNBLFdBQUtELFFBQUwsQ0FBYztBQUFDZCxRQUFBQSxnQkFBZ0IsRUFBRTtBQUFuQixPQUFkO0FBQ0E7QUFDSDs7QUFDREsscUNBQWdCVSxHQUFoQixHQUFzQkMsc0JBQXRCLEdBQStDQyxJQUEvQyxDQUFxREMsUUFBRCxJQUFjO0FBQzlELFdBQUtMLFNBQUwsR0FBaUJLLFFBQWpCO0FBQ0EsV0FBS0osUUFBTCxDQUFjO0FBQUNkLFFBQUFBLGdCQUFnQixFQUFFO0FBQW5CLE9BQWQ7QUFDSCxLQUhELEVBR0ltQixHQUFELElBQVM7QUFDUkMsTUFBQUEsT0FBTyxDQUFDQyxJQUFSLCtDQUFvREYsR0FBcEQ7QUFDQSxXQUFLTCxRQUFMLENBQWM7QUFBQ2QsUUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsT0FBZDs7QUFDQSxVQUFJSyxpQ0FBZ0JVLEdBQWhCLEdBQXNCTyxPQUF0QixFQUFKLEVBQXFDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0g7O0FBQ0RuQyxNQUFBQSxLQUFLLENBQUMsNkNBQUQsQ0FBTDtBQUNBLFdBQUsyQixRQUFMLENBQWM7QUFDVmIsUUFBQUEsS0FBSyxFQUFFLHlCQUNILCtEQUNBLGdFQUZHO0FBREcsT0FBZDtBQU1ILEtBbkJEOztBQXFCQSxTQUFLc0IsZUFBTDtBQUNILEdBdkQyQjtBQXlENUJDLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0IsUUFBSSxLQUFLYixhQUFULEVBQXdCO0FBQ3BCYyxNQUFBQSxZQUFZLENBQUMsS0FBS2QsYUFBTixDQUFaO0FBQ0g7O0FBQ0QsU0FBS0YsVUFBTCxHQUFrQixJQUFsQjtBQUNILEdBOUQyQjtBQWdFNUJjLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFNBQUtiLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLSSxRQUFMLENBQWM7QUFDVmhCLE1BQUFBLFdBQVcsRUFBRSxFQURIO0FBRVZDLE1BQUFBLE9BQU8sRUFBRTtBQUZDLEtBQWQ7QUFJQSxTQUFLMkIsWUFBTDtBQUNILEdBdkUyQjtBQXlFNUJBLEVBQUFBLFlBQVksRUFBRSxZQUFXO0FBQ3JCLFFBQUksQ0FBQ3JCLGlDQUFnQlUsR0FBaEIsRUFBTCxFQUE0QixPQUFPWSxPQUFPLENBQUNDLE9BQVIsRUFBUDtBQUU1QixTQUFLZCxRQUFMLENBQWM7QUFDVmYsTUFBQUEsT0FBTyxFQUFFO0FBREMsS0FBZDtBQUlBLFVBQU04QixnQkFBZ0IsR0FBRyxLQUFLQyxLQUFMLENBQVd2QixZQUFwQztBQUNBLFVBQU13QixTQUFTLEdBQUcsS0FBS0QsS0FBTCxDQUFXMUIsVUFBN0IsQ0FScUIsQ0FTckI7QUFDQTs7QUFDQSxVQUFNNEIsYUFBYSxHQUFHLEtBQUt0QixTQUEzQjtBQUNBLFVBQU11QixJQUFJLEdBQUc7QUFBQ0MsTUFBQUEsS0FBSyxFQUFFO0FBQVIsS0FBYjs7QUFDQSxRQUFJSCxTQUFTLElBQUkxQixpQ0FBZ0JDLGlCQUFoQixFQUFqQixFQUFzRDtBQUNsRDJCLE1BQUFBLElBQUksQ0FBQ0UsTUFBTCxHQUFjSixTQUFkO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLRCxLQUFMLENBQVc1QixVQUFYLEtBQTBCa0MsMEJBQTlCLEVBQXlDO0FBQ3JDSCxNQUFBQSxJQUFJLENBQUNJLG9CQUFMLEdBQTRCLElBQTVCO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBS1AsS0FBTCxDQUFXNUIsVUFBZixFQUEyQjtBQUM5QitCLE1BQUFBLElBQUksQ0FBQ0ssdUJBQUwsR0FBK0IsS0FBS1IsS0FBTCxDQUFXNUIsVUFBMUM7QUFDSDs7QUFDRCxRQUFJLEtBQUtRLFNBQVQsRUFBb0J1QixJQUFJLENBQUNNLEtBQUwsR0FBYSxLQUFLN0IsU0FBbEI7QUFDcEIsUUFBSW1CLGdCQUFKLEVBQXNCSSxJQUFJLENBQUNPLE1BQUwsR0FBYztBQUFFQyxNQUFBQSxtQkFBbUIsRUFBRVo7QUFBdkIsS0FBZDtBQUN0QixXQUFPeEIsaUNBQWdCVSxHQUFoQixHQUFzQmpCLFdBQXRCLENBQWtDbUMsSUFBbEMsRUFBd0NoQixJQUF4QyxDQUE4Q3lCLElBQUQsSUFBVTtBQUMxRCxVQUNJYixnQkFBZ0IsSUFBSSxLQUFLQyxLQUFMLENBQVd2QixZQUEvQixJQUNBd0IsU0FBUyxJQUFJLEtBQUtELEtBQUwsQ0FBVzFCLFVBRHhCLElBRUE0QixhQUFhLElBQUksS0FBS3RCLFNBSDFCLEVBR3FDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0g7O0FBRUQsVUFBSSxLQUFLRCxVQUFULEVBQXFCO0FBQ2pCO0FBQ0E7QUFDSDs7QUFFRCxXQUFLQyxTQUFMLEdBQWlCZ0MsSUFBSSxDQUFDQyxVQUF0QjtBQUNBLFdBQUs3QixRQUFMLENBQWU4QixDQUFELElBQU87QUFDakJBLFFBQUFBLENBQUMsQ0FBQzlDLFdBQUYsQ0FBYytDLElBQWQsQ0FBbUIsSUFBSUgsSUFBSSxDQUFDSSxLQUFMLElBQWMsRUFBbEIsQ0FBbkI7QUFDQUYsUUFBQUEsQ0FBQyxDQUFDN0MsT0FBRixHQUFZLEtBQVo7QUFDQSxlQUFPNkMsQ0FBUDtBQUNILE9BSkQ7QUFLQSxhQUFPRyxPQUFPLENBQUNMLElBQUksQ0FBQ0MsVUFBTixDQUFkO0FBQ0gsS0F2Qk0sRUF1Qkh4QixHQUFELElBQVM7QUFDUixVQUNJVSxnQkFBZ0IsSUFBSSxLQUFLQyxLQUFMLENBQVd2QixZQUEvQixJQUNBd0IsU0FBUyxJQUFJLEtBQUtELEtBQUwsQ0FBVzFCLFVBRHhCLElBRUE0QixhQUFhLElBQUksS0FBS3RCLFNBSDFCLEVBR3FDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNIOztBQUVELFVBQUksS0FBS0QsVUFBVCxFQUFxQjtBQUNqQjtBQUNBO0FBQ0g7O0FBRURXLE1BQUFBLE9BQU8sQ0FBQ25CLEtBQVIsQ0FBYywrQkFBZCxFQUErQytDLElBQUksQ0FBQ0MsU0FBTCxDQUFlOUIsR0FBZixDQUEvQztBQUNBaEMsTUFBQUEsS0FBSyxDQUFDLGdDQUFELENBQUw7QUFDQSxXQUFLMkIsUUFBTCxDQUFjO0FBQ1ZmLFFBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZFLFFBQUFBLEtBQUssRUFDRCxVQUFHLHlCQUFHLDBDQUFILENBQUgsbUJBQ0lrQixHQUFHLElBQUlBLEdBQUcsQ0FBQytCLE9BQVosR0FBdUIvQixHQUFHLENBQUMrQixPQUEzQixHQUFxQyx5QkFBRyxrREFBSCxDQUR4QztBQUhNLE9BQWQ7QUFPSCxLQS9DTSxDQUFQO0FBZ0RILEdBaEoyQjs7QUFrSjVCOzs7Ozs7O0FBT0FDLEVBQUFBLG1CQUFtQixFQUFFLFVBQVNDLElBQVQsRUFBZTtBQUNoQyxVQUFNQyxLQUFLLEdBQUdDLDBCQUEwQixDQUFDRixJQUFELENBQXhDO0FBQ0EsVUFBTUcsSUFBSSxHQUFHSCxJQUFJLENBQUNHLElBQUwsSUFBYUYsS0FBYixJQUFzQix5QkFBRyxjQUFILENBQW5DO0FBRUEsVUFBTUcsY0FBYyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCO0FBQ0EsVUFBTUMsV0FBVyxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBRUEsUUFBSUUsSUFBSjs7QUFDQSxRQUFJUCxLQUFKLEVBQVc7QUFDUE8sTUFBQUEsSUFBSSxHQUFHLHlCQUFHLHlFQUFILEVBQThFO0FBQUNQLFFBQUFBLEtBQUssRUFBRUEsS0FBUjtBQUFlRSxRQUFBQSxJQUFJLEVBQUVBO0FBQXJCLE9BQTlFLENBQVA7QUFDSCxLQUZELE1BRU87QUFDSEssTUFBQUEsSUFBSSxHQUFHLHlCQUFHLHFDQUFILEVBQTBDO0FBQUNMLFFBQUFBLElBQUksRUFBRUE7QUFBUCxPQUExQyxDQUFQO0FBQ0g7O0FBRURNLG1CQUFNQyxtQkFBTixDQUEwQix1QkFBMUIsRUFBbUQsRUFBbkQsRUFBdUROLGNBQXZELEVBQXVFO0FBQ25FTyxNQUFBQSxLQUFLLEVBQUUseUJBQUcsdUJBQUgsQ0FENEQ7QUFFbkVDLE1BQUFBLFdBQVcsRUFBRUosSUFGc0Q7QUFHbkVuRSxNQUFBQSxVQUFVLEVBQUd3RSxhQUFELElBQW1CO0FBQzNCLFlBQUksQ0FBQ0EsYUFBTCxFQUFvQjtBQUVwQixjQUFNQyxNQUFNLEdBQUdULEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjs7QUFDQSxjQUFNUyxLQUFLLEdBQUdOLGVBQU1PLFlBQU4sQ0FBbUJGLE1BQW5CLENBQWQ7O0FBQ0EsWUFBSUcsSUFBSSxHQUFHLHlCQUFHLHFDQUFILEVBQTBDO0FBQUNkLFVBQUFBLElBQUksRUFBRUE7QUFBUCxTQUExQyxDQUFYOztBQUVBbEQseUNBQWdCVSxHQUFoQixHQUFzQnVELDBCQUF0QixDQUFpRGxCLElBQUksQ0FBQ21CLE9BQXRELEVBQStELFNBQS9ELEVBQTBFdEQsSUFBMUUsQ0FBK0UsTUFBTTtBQUNqRixjQUFJLENBQUNvQyxLQUFMLEVBQVk7QUFDWmdCLFVBQUFBLElBQUksR0FBRyx5QkFBRyxtQkFBSCxDQUFQO0FBQ0EsaUJBQU9oRSxpQ0FBZ0JVLEdBQWhCLEdBQXNCeUQsV0FBdEIsQ0FBa0NuQixLQUFsQyxDQUFQO0FBQ0gsU0FKRCxFQUlHcEMsSUFKSCxDQUlRLE1BQU07QUFDVmtELFVBQUFBLEtBQUssQ0FBQ00sS0FBTjtBQUNBLGVBQUtsRCxlQUFMO0FBQ0gsU0FQRCxFQU9JSixHQUFELElBQVM7QUFDUmdELFVBQUFBLEtBQUssQ0FBQ00sS0FBTjtBQUNBLGVBQUtsRCxlQUFMO0FBQ0FILFVBQUFBLE9BQU8sQ0FBQ25CLEtBQVIsQ0FBYyxlQUFlb0UsSUFBZixHQUFzQixJQUF0QixHQUE2QmxELEdBQTNDOztBQUNBMEMseUJBQU1DLG1CQUFOLENBQTBCLDZCQUExQixFQUF5RCxFQUF6RCxFQUE2REgsV0FBN0QsRUFBMEU7QUFDdEVJLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRCtEO0FBRXRFQyxZQUFBQSxXQUFXLEVBQUk3QyxHQUFHLElBQUlBLEdBQUcsQ0FBQytCLE9BQVosR0FBdUIvQixHQUFHLENBQUMrQixPQUEzQixHQUFxQyx5QkFBRyw2Q0FBSDtBQUZtQixXQUExRTtBQUlILFNBZkQ7QUFnQkg7QUExQmtFLEtBQXZFO0FBNEJILEdBbk0yQjtBQXFNNUJ3QixFQUFBQSxhQUFhLEVBQUUsVUFBU3RCLElBQVQsRUFBZXVCLEVBQWYsRUFBbUI7QUFDOUIsUUFBSUEsRUFBRSxDQUFDQyxRQUFQLEVBQWlCO0FBQ2JELE1BQUFBLEVBQUUsQ0FBQ0UsY0FBSDtBQUNBLFdBQUsxQixtQkFBTCxDQUF5QkMsSUFBekI7QUFDSCxLQUhELE1BR087QUFDSCxXQUFLMEIsUUFBTCxDQUFjMUIsSUFBZDtBQUNIO0FBQ0osR0E1TTJCO0FBOE01QjJCLEVBQUFBLGNBQWMsRUFBRSxVQUFTNUMsTUFBVCxFQUFpQmpDLFVBQWpCLEVBQTZCO0FBQ3pDO0FBQ0EsU0FBS1EsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFNBQUtJLFFBQUwsQ0FBYztBQUNWO0FBQ0E7QUFDQTtBQUNBaEIsTUFBQUEsV0FBVyxFQUFFLEVBSkg7QUFLVk0sTUFBQUEsVUFBVSxFQUFFK0IsTUFMRjtBQU1WakMsTUFBQUEsVUFBVSxFQUFFQSxVQU5GO0FBT1ZELE1BQUFBLEtBQUssRUFBRTtBQVBHLEtBQWQsRUFRRyxLQUFLc0IsZUFSUixFQUh5QyxDQVl6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSCxHQWhPMkI7QUFrTzVCeUQsRUFBQUEsYUFBYSxFQUFFLFVBQVNDLFNBQVQsRUFBb0I7QUFDL0IsUUFBSUEsU0FBUyxJQUFJLENBQUMsS0FBS3ZFLFNBQXZCLEVBQWtDLE9BQU9pQixPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsS0FBaEIsQ0FBUDtBQUVsQyxXQUFPLEtBQUtGLFlBQUwsRUFBUDtBQUNILEdBdE8yQjtBQXdPNUJ3RCxFQUFBQSxjQUFjLEVBQUUsVUFBUzdCLEtBQVQsRUFBZ0I7QUFDNUIsU0FBS3ZDLFFBQUwsQ0FBYztBQUNWUCxNQUFBQSxZQUFZLEVBQUU4QyxLQUFLLElBQUk7QUFEYixLQUFkLEVBRDRCLENBSzVCO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFFBQUksS0FBSzFDLGFBQVQsRUFBd0I7QUFDcEJjLE1BQUFBLFlBQVksQ0FBQyxLQUFLZCxhQUFOLENBQVo7QUFDSDs7QUFDRCxTQUFLQSxhQUFMLEdBQXFCd0UsVUFBVSxDQUFDLE1BQU07QUFDbEMsV0FBS3hFLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxXQUFLWSxlQUFMO0FBQ0gsS0FIOEIsRUFHNUIsR0FINEIsQ0FBL0I7QUFJSCxHQXhQMkI7QUEwUDVCNkQsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEI7QUFDQSxTQUFLdEUsUUFBTCxDQUFjO0FBQ1ZQLE1BQUFBLFlBQVksRUFBRTtBQURKLEtBQWQsRUFFRyxLQUFLZ0IsZUFGUjs7QUFJQSxRQUFJLEtBQUtaLGFBQVQsRUFBd0I7QUFDcEJjLE1BQUFBLFlBQVksQ0FBQyxLQUFLZCxhQUFOLENBQVo7QUFDSDtBQUNKLEdBblEyQjtBQXFRNUIwRSxFQUFBQSxxQkFBcUIsRUFBRSxVQUFTaEMsS0FBVCxFQUFnQjtBQUNuQztBQUNBLFFBQUksQ0FBQyxLQUFLdkIsS0FBTCxDQUFXNUIsVUFBWixJQUEwQixLQUFLNEIsS0FBTCxDQUFXNUIsVUFBWCxLQUEwQmtDLDBCQUF4RCxFQUFtRTtBQUMvRDtBQUNBO0FBQ0EsVUFBSWlCLEtBQUssQ0FBQ2lDLE9BQU4sQ0FBYyxHQUFkLEtBQXNCLENBQUMsQ0FBM0IsRUFBOEI7QUFDMUJqQyxRQUFBQSxLQUFLLEdBQUdBLEtBQUssR0FBRyxHQUFSLEdBQWMsS0FBS3ZCLEtBQUwsQ0FBVzFCLFVBQWpDO0FBQ0g7O0FBQ0QsV0FBS21GLGFBQUwsQ0FBbUJsQyxLQUFuQixFQUEwQixJQUExQjtBQUNILEtBUEQsTUFPTztBQUNIO0FBQ0EsWUFBTW1DLFlBQVksR0FBRywrQ0FBMEIsS0FBSzNFLFNBQS9CLEVBQTBDLEtBQUtpQixLQUFMLENBQVc1QixVQUFyRCxDQUFyQjtBQUNBLFlBQU11RixRQUFRLEdBQUcsMkNBQXNCLEtBQUs1RSxTQUEzQixFQUFzQyxLQUFLaUIsS0FBTCxDQUFXNUIsVUFBakQsQ0FBakI7QUFDQSxZQUFNd0YsTUFBTSxHQUFHRixZQUFZLEdBQUcsS0FBS0csK0JBQUwsQ0FBcUN0QyxLQUFyQyxFQUE0QyxLQUFLeEMsU0FBTCxDQUFlMkUsWUFBZixDQUE1QyxFQUEwRUMsUUFBMUUsQ0FBSCxHQUF5RixJQUFwSDs7QUFDQSxVQUFJLENBQUNDLE1BQUwsRUFBYTtBQUNULGNBQU0vQixXQUFXLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FHLHVCQUFNQyxtQkFBTixDQUEwQix3QkFBMUIsRUFBb0QsRUFBcEQsRUFBd0RILFdBQXhELEVBQXFFO0FBQ2pFSSxVQUFBQSxLQUFLLEVBQUUseUJBQUcsd0JBQUgsQ0FEMEQ7QUFFakVDLFVBQUFBLFdBQVcsRUFBRSx5QkFBRyx1REFBSDtBQUZvRCxTQUFyRTs7QUFJQTtBQUNIOztBQUNEM0QsdUNBQWdCVSxHQUFoQixHQUFzQjZFLHFCQUF0QixDQUE0Q0osWUFBNUMsRUFBMERFLE1BQTFELEVBQWtFekUsSUFBbEUsQ0FBd0U0RSxJQUFELElBQVU7QUFDN0UsWUFBSUEsSUFBSSxDQUFDQyxNQUFMLEdBQWMsQ0FBZCxJQUFtQkQsSUFBSSxDQUFDLENBQUQsQ0FBSixDQUFReEMsS0FBL0IsRUFBc0M7QUFDbEMsZUFBS2tDLGFBQUwsQ0FBbUJNLElBQUksQ0FBQyxDQUFELENBQUosQ0FBUXhDLEtBQTNCLEVBQWtDLElBQWxDO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZ0JBQU1NLFdBQVcsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUcseUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxFQUE1QyxFQUFnREgsV0FBaEQsRUFBNkQ7QUFDekRJLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxnQkFBSCxDQURrRDtBQUV6REMsWUFBQUEsV0FBVyxFQUFFLHlCQUFHLHVDQUFIO0FBRjRDLFdBQTdEO0FBSUg7QUFDSixPQVZELEVBVUkrQixDQUFELElBQU87QUFDTixjQUFNcEMsV0FBVyxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBRyx1QkFBTUMsbUJBQU4sQ0FBMEIsc0NBQTFCLEVBQWtFLEVBQWxFLEVBQXNFSCxXQUF0RSxFQUFtRjtBQUMvRUksVUFBQUEsS0FBSyxFQUFFLHlCQUFHLHNDQUFILENBRHdFO0FBRS9FQyxVQUFBQSxXQUFXLEVBQUUseUJBQUcsdUNBQUg7QUFGa0UsU0FBbkY7QUFJSCxPQWhCRDtBQWlCSDtBQUNKLEdBN1MyQjtBQStTNUJnQyxFQUFBQSxjQUFjLEVBQUUsVUFBU3JCLEVBQVQsRUFBYXZCLElBQWIsRUFBbUI7QUFDL0IsU0FBSzZDLEtBQUwsQ0FBV3hHLFVBQVg7O0FBQ0F5Ryx3QkFBSUMsUUFBSixDQUFhO0FBQ1QvRyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUbUYsTUFBQUEsT0FBTyxFQUFFbkIsSUFBSSxDQUFDbUIsT0FGTDtBQUdUNkIsTUFBQUEsV0FBVyxFQUFFO0FBSEosS0FBYjs7QUFLQXpCLElBQUFBLEVBQUUsQ0FBQzBCLGVBQUg7QUFDSCxHQXZUMkI7QUF5VDVCQyxFQUFBQSxXQUFXLEVBQUUsVUFBUzNCLEVBQVQsRUFBYXZCLElBQWIsRUFBbUI7QUFDNUIsU0FBSzZDLEtBQUwsQ0FBV3hHLFVBQVg7O0FBQ0F5Ryx3QkFBSUMsUUFBSixDQUFhO0FBQ1QvRyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUbUYsTUFBQUEsT0FBTyxFQUFFbkIsSUFBSSxDQUFDbUIsT0FGTDtBQUdUNkIsTUFBQUEsV0FBVyxFQUFFO0FBSEosS0FBYjs7QUFLQXpCLElBQUFBLEVBQUUsQ0FBQzBCLGVBQUg7QUFDSCxHQWpVMkI7QUFtVTVCRSxFQUFBQSxXQUFXLEVBQUUsVUFBUzVCLEVBQVQsRUFBYXZCLElBQWIsRUFBbUI7QUFDNUIsU0FBSzBCLFFBQUwsQ0FBYzFCLElBQWQsRUFBb0IsSUFBcEIsRUFBMEIsSUFBMUI7QUFDQXVCLElBQUFBLEVBQUUsQ0FBQzBCLGVBQUg7QUFDSCxHQXRVMkI7QUF3VTVCRyxFQUFBQSxpQkFBaUIsRUFBRSxVQUFTcEQsSUFBVCxFQUFlO0FBQzlCLFNBQUs2QyxLQUFMLENBQVd4RyxVQUFYOztBQUNBeUcsd0JBQUlDLFFBQUosQ0FBYTtBQUFDL0csTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjtBQUNILEdBM1UyQjtBQTZVNUJtRyxFQUFBQSxhQUFhLEVBQUUsVUFBU2xDLEtBQVQsRUFBZ0JvRCxRQUFRLEdBQUMsS0FBekIsRUFBZ0M7QUFDM0MsU0FBSzNCLFFBQUwsQ0FBYyxJQUFkLEVBQW9CekIsS0FBcEIsRUFBMkJvRCxRQUEzQjtBQUNILEdBL1UyQjtBQWlWNUIzQixFQUFBQSxRQUFRLEVBQUUsVUFBUzFCLElBQVQsRUFBZXNELFVBQWYsRUFBMkJELFFBQVEsR0FBQyxLQUFwQyxFQUEyQztBQUNqRCxTQUFLUixLQUFMLENBQVd4RyxVQUFYO0FBQ0EsVUFBTWtILE9BQU8sR0FBRztBQUNadkgsTUFBQUEsTUFBTSxFQUFFLFdBREk7QUFFWndILE1BQUFBLFNBQVMsRUFBRUg7QUFGQyxLQUFoQjs7QUFJQSxRQUFJckQsSUFBSixFQUFVO0FBQ047QUFDQTtBQUNBO0FBQ0EsVUFBSS9DLGlDQUFnQlUsR0FBaEIsR0FBc0JPLE9BQXRCLEVBQUosRUFBcUM7QUFDakMsWUFBSSxDQUFDOEIsSUFBSSxDQUFDeUQsY0FBTixJQUF3QixDQUFDekQsSUFBSSxDQUFDMEQsY0FBbEMsRUFBa0Q7QUFDOUNaLDhCQUFJQyxRQUFKLENBQWE7QUFBQy9HLFlBQUFBLE1BQU0sRUFBRTtBQUFULFdBQWI7O0FBQ0E7QUFDSDtBQUNKOztBQUVELFVBQUksQ0FBQ3NILFVBQUwsRUFBaUI7QUFDYkEsUUFBQUEsVUFBVSxHQUFHcEQsMEJBQTBCLENBQUNGLElBQUQsQ0FBdkM7QUFDSDs7QUFFRHVELE1BQUFBLE9BQU8sQ0FBQ0ksUUFBUixHQUFtQjtBQUNmQyxRQUFBQSxTQUFTLEVBQUU1RCxJQUFJLENBQUM2RCxVQUREO0FBRWY7QUFDQTtBQUNBMUQsUUFBQUEsSUFBSSxFQUFFSCxJQUFJLENBQUNHLElBQUwsSUFBYW1ELFVBQWIsSUFBMkIseUJBQUcsY0FBSDtBQUpsQixPQUFuQjs7QUFPQSxVQUFJLEtBQUs1RSxLQUFMLENBQVcxQixVQUFmLEVBQTJCO0FBQ3ZCdUcsUUFBQUEsT0FBTyxDQUFDMUUsSUFBUixHQUFlO0FBQ1hpRixVQUFBQSxVQUFVLEVBQUUsQ0FBQyxLQUFLcEYsS0FBTCxDQUFXMUIsVUFBWjtBQURELFNBQWY7QUFHSDtBQUNKLEtBakNnRCxDQWtDakQ7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUlzRyxVQUFKLEVBQWdCO0FBQ1pDLE1BQUFBLE9BQU8sQ0FBQ0QsVUFBUixHQUFxQkEsVUFBckI7QUFDSCxLQUZELE1BRU87QUFDSEMsTUFBQUEsT0FBTyxDQUFDcEMsT0FBUixHQUFrQm5CLElBQUksQ0FBQ21CLE9BQXZCO0FBQ0g7O0FBQ0QyQix3QkFBSUMsUUFBSixDQUFhUSxPQUFiO0FBQ0gsR0E3WDJCOztBQStYNUJRLEVBQUFBLE1BQU0sQ0FBQy9ELElBQUQsRUFBTztBQUNULFVBQU1nRSxNQUFNLEdBQUcvRyxpQ0FBZ0JVLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTXNHLFVBQVUsR0FBR0QsTUFBTSxDQUFDRSxPQUFQLENBQWVsRSxJQUFJLENBQUNtQixPQUFwQixDQUFuQjtBQUNBLFVBQU1nRCxhQUFhLEdBQUdGLFVBQVUsSUFBSUEsVUFBVSxDQUFDRyxlQUFYLE9BQWlDLE1BQXJFO0FBQ0EsVUFBTWxHLE9BQU8sR0FBRzhGLE1BQU0sQ0FBQzlGLE9BQVAsRUFBaEI7QUFDQSxVQUFNbUcsVUFBVSxHQUFHaEUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjtBQUNBLFVBQU1nRSxnQkFBZ0IsR0FBR2pFLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxRQUFJaUUsYUFBSjtBQUNBLFFBQUlDLGdCQUFKOztBQUVBLFFBQUl4RSxJQUFJLENBQUN5RCxjQUFMLElBQXVCLENBQUNVLGFBQTVCLEVBQTJDO0FBQ3ZDSSxNQUFBQSxhQUFhLEdBQ1QsNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsV0FBdkI7QUFBbUMsUUFBQSxPQUFPLEVBQUdoRCxFQUFELElBQVEsS0FBS3FCLGNBQUwsQ0FBb0JyQixFQUFwQixFQUF3QnZCLElBQXhCO0FBQXBELFNBQW9GLHlCQUFHLFNBQUgsQ0FBcEYsQ0FESjtBQUdIOztBQUNELFFBQUltRSxhQUFKLEVBQW1CO0FBQ2ZLLE1BQUFBLGdCQUFnQixHQUNaLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsSUFBSSxFQUFDLFdBQXZCO0FBQW1DLFFBQUEsT0FBTyxFQUFHakQsRUFBRCxJQUFRLEtBQUsyQixXQUFMLENBQWlCM0IsRUFBakIsRUFBcUJ2QixJQUFyQjtBQUFwRCxTQUFpRix5QkFBRyxNQUFILENBQWpGLENBREo7QUFHSCxLQUpELE1BSU8sSUFBSSxDQUFDOUIsT0FBRCxJQUFZOEIsSUFBSSxDQUFDMEQsY0FBckIsRUFBcUM7QUFDeENjLE1BQUFBLGdCQUFnQixHQUNaLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsSUFBSSxFQUFDLFNBQXZCO0FBQWlDLFFBQUEsT0FBTyxFQUFHakQsRUFBRCxJQUFRLEtBQUs0QixXQUFMLENBQWlCNUIsRUFBakIsRUFBcUJ2QixJQUFyQjtBQUFsRCxTQUErRSx5QkFBRyxNQUFILENBQS9FLENBREo7QUFHSDs7QUFFRCxRQUFJRyxJQUFJLEdBQUdILElBQUksQ0FBQ0csSUFBTCxJQUFhRCwwQkFBMEIsQ0FBQ0YsSUFBRCxDQUF2QyxJQUFpRCx5QkFBRyxjQUFILENBQTVEOztBQUNBLFFBQUlHLElBQUksQ0FBQ3VDLE1BQUwsR0FBYzdHLGVBQWxCLEVBQW1DO0FBQy9Cc0UsTUFBQUEsSUFBSSxhQUFNQSxJQUFJLENBQUNzRSxTQUFMLENBQWUsQ0FBZixFQUFrQjVJLGVBQWxCLENBQU4sUUFBSjtBQUNIOztBQUVELFFBQUk2SSxLQUFLLEdBQUcxRSxJQUFJLENBQUMwRSxLQUFMLElBQWMsRUFBMUI7O0FBQ0EsUUFBSUEsS0FBSyxDQUFDaEMsTUFBTixHQUFlNUcsZ0JBQW5CLEVBQXFDO0FBQ2pDNEksTUFBQUEsS0FBSyxhQUFNQSxLQUFLLENBQUNELFNBQU4sQ0FBZ0IsQ0FBaEIsRUFBbUIzSSxnQkFBbkIsQ0FBTixRQUFMO0FBQ0g7O0FBQ0Q0SSxJQUFBQSxLQUFLLEdBQUcsdUNBQXVCQSxLQUF2QixDQUFSO0FBQ0EsVUFBTWQsU0FBUyxHQUFHLG1DQUNNM0csaUNBQWdCVSxHQUFoQixHQUFzQmdILGdCQUF0QixFQUROLEVBRU0zRSxJQUFJLENBQUM2RCxVQUZYLEVBRXVCLEVBRnZCLEVBRTJCLEVBRjNCLEVBRStCLE1BRi9CLENBQWxCO0FBSUEsV0FDSTtBQUFJLE1BQUEsR0FBRyxFQUFHN0QsSUFBSSxDQUFDbUIsT0FBZjtBQUNJLE1BQUEsT0FBTyxFQUFHSSxFQUFELElBQVEsS0FBS0QsYUFBTCxDQUFtQnRCLElBQW5CLEVBQXlCdUIsRUFBekIsQ0FEckIsQ0FFSTtBQUZKO0FBR0ksTUFBQSxXQUFXLEVBQUdBLEVBQUQsSUFBUTtBQUFDQSxRQUFBQSxFQUFFLENBQUNFLGNBQUg7QUFBcUI7QUFIL0MsT0FLSTtBQUFJLE1BQUEsU0FBUyxFQUFDO0FBQWQsT0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxLQUFLLEVBQUUsRUFBbkI7QUFBdUIsTUFBQSxNQUFNLEVBQUUsRUFBL0I7QUFBbUMsTUFBQSxZQUFZLEVBQUMsTUFBaEQ7QUFDSSxNQUFBLElBQUksRUFBR3RCLElBRFg7QUFDa0IsTUFBQSxNQUFNLEVBQUdBLElBRDNCO0FBRUksTUFBQSxHQUFHLEVBQUd5RDtBQUZWLE1BREosQ0FMSixFQVVJO0FBQUksTUFBQSxTQUFTLEVBQUM7QUFBZCxPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF5Q3pELElBQXpDLENBREosVUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHdCQUFmO0FBQ0ksTUFBQSxPQUFPLEVBQUlvQixFQUFELElBQVE7QUFBRUEsUUFBQUEsRUFBRSxDQUFDMEIsZUFBSDtBQUF1QixPQUQvQztBQUVJLE1BQUEsdUJBQXVCLEVBQUU7QUFBRTJCLFFBQUFBLE1BQU0sRUFBRUY7QUFBVjtBQUY3QixNQUZKLEVBS0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQTBDeEUsMEJBQTBCLENBQUNGLElBQUQsQ0FBcEUsQ0FMSixDQVZKLEVBaUJJO0FBQUksTUFBQSxTQUFTLEVBQUM7QUFBZCxPQUNNQSxJQUFJLENBQUM2RSxrQkFEWCxDQWpCSixFQW9CSTtBQUFJLE1BQUEsU0FBUyxFQUFDO0FBQWQsT0FBMENOLGFBQTFDLENBcEJKLEVBcUJJO0FBQUksTUFBQSxTQUFTLEVBQUM7QUFBZCxPQUF1Q0MsZ0JBQXZDLENBckJKLENBREo7QUF5QkgsR0EvYjJCOztBQWljNUJNLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNDLE9BQVQsRUFBa0I7QUFDbEMsU0FBS3ZILFdBQUwsR0FBbUJ1SCxPQUFuQjtBQUNILEdBbmMyQjtBQXFjNUJDLEVBQUFBLGtCQUFrQixFQUFFLFVBQVN4RixDQUFULEVBQVl5RixVQUFaLEVBQXdCO0FBQ3hDLFFBQUlDLEdBQUcsR0FBRyxnQkFBVjs7QUFDQSxRQUFJRCxVQUFVLElBQUlBLFVBQVUsQ0FBQ0UsTUFBN0IsRUFBcUM7QUFDakNELE1BQUFBLEdBQUcsR0FBRyxJQUFJRSxNQUFKLENBQVdILFVBQVUsQ0FBQ0UsTUFBdEIsQ0FBTjtBQUNIOztBQUVELFdBQU9ELEdBQUcsQ0FBQ0csSUFBSixDQUFTN0YsQ0FBVCxDQUFQO0FBQ0gsR0E1YzJCO0FBOGM1QitDLEVBQUFBLCtCQUErQixFQUFFLFVBQVMrQyxTQUFULEVBQW9CQyxRQUFwQixFQUE4QmxELFFBQTlCLEVBQXdDO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBLFVBQU1tRCxjQUFjLEdBQUdELFFBQVEsQ0FBQ0UsZUFBaEM7QUFDQSxRQUFJLENBQUNELGNBQUwsRUFBcUIsT0FBTyxJQUFQO0FBQ3JCLFVBQU1sRCxNQUFNLEdBQUcsRUFBZjs7QUFDQSxTQUFLLElBQUlvRCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixjQUFjLENBQUM5QyxNQUFmLEdBQXdCLENBQTVDLEVBQStDLEVBQUVnRCxDQUFqRCxFQUFvRDtBQUNoRCxZQUFNQyxTQUFTLEdBQUdILGNBQWMsQ0FBQ0UsQ0FBRCxDQUFoQztBQUNBLFVBQUlyRCxRQUFRLENBQUNDLE1BQVQsQ0FBZ0JxRCxTQUFoQixNQUErQjVJLFNBQW5DLEVBQThDLE9BQU8sSUFBUDtBQUM5Q3VGLE1BQUFBLE1BQU0sQ0FBQ3FELFNBQUQsQ0FBTixHQUFvQnRELFFBQVEsQ0FBQ0MsTUFBVCxDQUFnQnFELFNBQWhCLENBQXBCO0FBQ0g7O0FBQ0RyRCxJQUFBQSxNQUFNLENBQUNrRCxjQUFjLENBQUNBLGNBQWMsQ0FBQzlDLE1BQWYsR0FBd0IsQ0FBekIsQ0FBZixDQUFOLEdBQW9ENEMsU0FBcEQ7QUFDQSxXQUFPaEQsTUFBUDtBQUNILEdBNWQyQjs7QUE4ZDVCOzs7OztBQUtBc0QsRUFBQUEsZUFBZSxFQUFFLFVBQVNyRSxFQUFULEVBQWE7QUFDMUIsUUFBSSxLQUFLL0QsV0FBVCxFQUFzQjtBQUNsQixXQUFLQSxXQUFMLENBQWlCb0ksZUFBakIsQ0FBaUNyRSxFQUFqQztBQUNIO0FBQ0osR0F2ZTJCO0FBeWU1QnNFLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTS9FLE1BQU0sR0FBR1QsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFmO0FBQ0EsVUFBTXdGLFVBQVUsR0FBR3pGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7QUFDQSxVQUFNZ0UsZ0JBQWdCLEdBQUdqRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBRUEsUUFBSXlGLE9BQUo7O0FBQ0EsUUFBSSxLQUFLckgsS0FBTCxDQUFXN0IsS0FBZixFQUFzQjtBQUNsQmtKLE1BQUFBLE9BQU8sR0FBRyxLQUFLckgsS0FBTCxDQUFXN0IsS0FBckI7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLNkIsS0FBTCxDQUFXOUIsZ0JBQWYsRUFBaUM7QUFDcENtSixNQUFBQSxPQUFPLEdBQUcsNkJBQUMsTUFBRCxPQUFWO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsWUFBTUMsSUFBSSxHQUFHLENBQUMsS0FBS3RILEtBQUwsQ0FBV2hDLFdBQVgsSUFBMEIsRUFBM0IsRUFBK0J1SixHQUEvQixDQUFtQ2pHLElBQUksSUFBSSxLQUFLK0QsTUFBTCxDQUFZL0QsSUFBWixDQUEzQyxDQUFiLENBREcsQ0FFSDtBQUNBO0FBQ0E7O0FBRUEsVUFBSWtHLE9BQUo7O0FBQ0EsVUFBSSxLQUFLeEgsS0FBTCxDQUFXL0IsT0FBZixFQUF3QjtBQUNwQnVKLFFBQUFBLE9BQU8sR0FBRyw2QkFBQyxNQUFELE9BQVY7QUFDSDs7QUFFRCxVQUFJQyxtQkFBSjs7QUFDQSxVQUFJSCxJQUFJLENBQUN0RCxNQUFMLEtBQWdCLENBQWhCLElBQXFCLENBQUMsS0FBS2hFLEtBQUwsQ0FBVy9CLE9BQXJDLEVBQThDO0FBQzFDd0osUUFBQUEsbUJBQW1CLEdBQUcsd0NBQUsseUJBQUcsa0JBQUgsQ0FBTCxDQUF0QjtBQUNILE9BRkQsTUFFTztBQUNIQSxRQUFBQSxtQkFBbUIsR0FBRztBQUFPLFVBQUEsU0FBUyxFQUFDO0FBQWpCLFdBQ2xCLDRDQUNNSCxJQUROLENBRGtCLENBQXRCO0FBS0g7O0FBQ0QsWUFBTUksV0FBVyxHQUFHL0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUFwQjtBQUNBeUYsTUFBQUEsT0FBTyxHQUFHLDZCQUFDLFdBQUQ7QUFBYSxRQUFBLEdBQUcsRUFBRSxLQUFLakIsa0JBQXZCO0FBQ04sUUFBQSxTQUFTLEVBQUMsK0JBREo7QUFFTixRQUFBLGFBQWEsRUFBRyxLQUFLbEQsYUFGZjtBQUdOLFFBQUEsWUFBWSxFQUFFLEtBSFI7QUFJTixRQUFBLGFBQWEsRUFBRTtBQUpULFNBTUp1RSxtQkFOSSxFQU9KRCxPQVBJLENBQVY7QUFTSDs7QUFFRCxRQUFJRyxVQUFKOztBQUNBLFFBQUksQ0FBQyxLQUFLM0gsS0FBTCxDQUFXOUIsZ0JBQWhCLEVBQWtDO0FBQzlCLFlBQU0wSixlQUFlLEdBQUdqRyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXhCO0FBQ0EsWUFBTWlHLGtCQUFrQixHQUFHbEcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDZCQUFqQixDQUEzQjtBQUVBLFlBQU04QixZQUFZLEdBQUcsK0NBQTBCLEtBQUszRSxTQUEvQixFQUEwQyxLQUFLaUIsS0FBTCxDQUFXNUIsVUFBckQsQ0FBckI7QUFDQSxVQUFJMEosNEJBQUo7O0FBQ0EsVUFDSXBFLFlBQVksSUFDWixLQUFLM0UsU0FETCxJQUVBLEtBQUtBLFNBQUwsQ0FBZTJFLFlBQWYsQ0FGQSxJQUdBLEtBQUszRSxTQUFMLENBQWUyRSxZQUFmLEVBQTZCcUQsZUFBN0IsQ0FBNkMvQyxNQUE3QyxHQUFzRCxDQUh0RCxJQUlBLEtBQUtqRixTQUFMLENBQWUyRSxZQUFmLEVBQTZCcUUsV0FMakMsRUFNRTtBQUNFLGNBQU1DLFVBQVUsR0FBRyxLQUFLakosU0FBTCxDQUFlMkUsWUFBZixFQUE2QnFELGVBQTdCLENBQTZDa0IsS0FBN0MsQ0FBbUQsQ0FBQyxDQUFwRCxFQUF1RCxDQUF2RCxDQUFuQjtBQUNBSCxRQUFBQSw0QkFBNEIsR0FBRyxLQUFLL0ksU0FBTCxDQUFlMkUsWUFBZixFQUE2QnFFLFdBQTdCLENBQXlDQyxVQUF6QyxDQUEvQjtBQUNIOztBQUVELFVBQUlFLFdBQVcsR0FBRyx5QkFBRyxjQUFILENBQWxCOztBQUNBLFVBQUksQ0FBQyxLQUFLbEksS0FBTCxDQUFXNUIsVUFBWixJQUEwQixLQUFLNEIsS0FBTCxDQUFXNUIsVUFBWCxLQUEwQmtDLDBCQUF4RCxFQUFtRTtBQUMvRDRILFFBQUFBLFdBQVcsR0FBRyx5QkFBRyxxQ0FBSCxFQUEwQztBQUFDQyxVQUFBQSxXQUFXLEVBQUUsY0FBYyxLQUFLbkksS0FBTCxDQUFXMUI7QUFBdkMsU0FBMUMsQ0FBZDtBQUNILE9BRkQsTUFFTyxJQUFJd0osNEJBQUosRUFBa0M7QUFDckNJLFFBQUFBLFdBQVcsR0FBR0osNEJBQTRCLENBQUNJLFdBQTNDO0FBQ0g7O0FBRUQsVUFBSUUsY0FBYyxHQUFHLEtBQUs5QixrQkFBTCxDQUF3QixLQUFLdEcsS0FBTCxDQUFXdkIsWUFBbkMsRUFBaURxSiw0QkFBakQsQ0FBckI7O0FBQ0EsVUFBSXBFLFlBQUosRUFBa0I7QUFDZCxjQUFNQyxRQUFRLEdBQUcsMkNBQXNCLEtBQUs1RSxTQUEzQixFQUFzQyxLQUFLaUIsS0FBTCxDQUFXNUIsVUFBakQsQ0FBakI7O0FBQ0EsWUFBSSxLQUFLeUYsK0JBQUwsQ0FBcUMsS0FBSzdELEtBQUwsQ0FBV3ZCLFlBQWhELEVBQThELEtBQUtNLFNBQUwsQ0FBZTJFLFlBQWYsQ0FBOUQsRUFBNEZDLFFBQTVGLE1BQTBHLElBQTlHLEVBQW9IO0FBQ2hIeUUsVUFBQUEsY0FBYyxHQUFHLEtBQWpCO0FBQ0g7QUFDSjs7QUFFRFQsTUFBQUEsVUFBVSxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNULDZCQUFDLGtCQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUMsNEJBRGQ7QUFFSSxRQUFBLFFBQVEsRUFBRSxLQUFLdkUsY0FGbkI7QUFHSSxRQUFBLE9BQU8sRUFBRSxLQUFLRSxhQUhsQjtBQUlJLFFBQUEsV0FBVyxFQUFFLEtBQUtDLHFCQUp0QjtBQUtJLFFBQUEsV0FBVyxFQUFFMkUsV0FMakI7QUFNSSxRQUFBLGNBQWMsRUFBRUU7QUFOcEIsUUFEUyxFQVNULDZCQUFDLGVBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBRSxLQUFLckosU0FEcEI7QUFFSSxRQUFBLGNBQWMsRUFBRSxLQUFLa0UsY0FGekI7QUFHSSxRQUFBLGtCQUFrQixFQUFFLEtBQUtqRCxLQUFMLENBQVcxQixVQUhuQztBQUlJLFFBQUEsa0JBQWtCLEVBQUUsS0FBSzBCLEtBQUwsQ0FBVzVCO0FBSm5DLFFBVFMsQ0FBYjtBQWdCSDs7QUFDRCxVQUFNaUssV0FBVyxHQUNiLHlCQUFHLCtGQUFILEVBQW9HLElBQXBHLEVBQ0k7QUFBQ0MsTUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUk7QUFDUCxlQUFRLDZCQUFDLGdCQUFEO0FBQ0osVUFBQSxJQUFJLEVBQUMsV0FERDtBQUVKLFVBQUEsT0FBTyxFQUFFLEtBQUs3RDtBQUZWLFdBR042RCxHQUhNLENBQVI7QUFJSDtBQUxELEtBREosQ0FESjtBQVVBLFdBQ0ksNkJBQUMsVUFBRDtBQUNJLE1BQUEsU0FBUyxFQUFFLHlCQURmO0FBRUksTUFBQSxTQUFTLEVBQUUsSUFGZjtBQUdJLE1BQUEsVUFBVSxFQUFFLEtBQUtwRSxLQUFMLENBQVd4RyxVQUgzQjtBQUlJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUg7QUFKWCxPQU1JO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLMEssV0FETCxFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLVixVQURMLEVBRUtOLE9BRkwsQ0FGSixDQU5KLENBREo7QUFnQkg7QUFobUIyQixDQUFqQixDLEVBbW1CZjtBQUNBOzs7OztBQUNBLFNBQVM3RiwwQkFBVCxDQUFvQ0YsSUFBcEMsRUFBMEM7QUFDdEMsU0FBT0EsSUFBSSxDQUFDa0gsZUFBTCxLQUF5QmxILElBQUksQ0FBQ21ILE9BQUwsR0FBZW5ILElBQUksQ0FBQ21ILE9BQUwsQ0FBYSxDQUFiLENBQWYsR0FBaUMsRUFBMUQsQ0FBUDtBQUNIIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vLi4vTW9kYWxcIjtcclxuaW1wb3J0IHsgbGlua2lmeUFuZFNhbml0aXplSHRtbCB9IGZyb20gJy4uLy4uL0h0bWxVdGlscyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHsgaW5zdGFuY2VGb3JJbnN0YW5jZUlkLCBwcm90b2NvbE5hbWVGb3JJbnN0YW5jZUlkIH0gZnJvbSAnLi4vLi4vdXRpbHMvRGlyZWN0b3J5VXRpbHMnO1xyXG5pbXBvcnQgQW5hbHl0aWNzIGZyb20gJy4uLy4uL0FuYWx5dGljcyc7XHJcbmltcG9ydCB7Z2V0SHR0cFVyaUZvck14Y30gZnJvbSBcIm1hdHJpeC1qcy1zZGsvc3JjL2NvbnRlbnQtcmVwb1wiO1xyXG5pbXBvcnQge0FMTF9ST09NU30gZnJvbSBcIi4uL3ZpZXdzL2RpcmVjdG9yeS9OZXR3b3JrRHJvcGRvd25cIjtcclxuXHJcbmNvbnN0IE1BWF9OQU1FX0xFTkdUSCA9IDgwO1xyXG5jb25zdCBNQVhfVE9QSUNfTEVOR1RIID0gMTYwO1xyXG5cclxuZnVuY3Rpb24gdHJhY2soYWN0aW9uKSB7XHJcbiAgICBBbmFseXRpY3MudHJhY2tFdmVudCgnUm9vbURpcmVjdG9yeScsIGFjdGlvbik7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tRGlyZWN0b3J5JyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHB1YmxpY1Jvb21zOiBbXSxcclxuICAgICAgICAgICAgbG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgcHJvdG9jb2xzTG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIGluc3RhbmNlSWQ6IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgcm9vbVNlcnZlcjogTWF0cml4Q2xpZW50UGVnLmdldEhvbWVzZXJ2ZXJOYW1lKCksXHJcbiAgICAgICAgICAgIGZpbHRlclN0cmluZzogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gTW92ZSB0aGlzIHRvIGNvbnN0cnVjdG9yXHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm5leHRCYXRjaCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5maWx0ZXJUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgICB0aGlzLnNjcm9sbFBhbmVsID0gbnVsbDtcclxuICAgICAgICB0aGlzLnByb3RvY29scyA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Byb3RvY29sc0xvYWRpbmc6IHRydWV9KTtcclxuICAgICAgICBpZiAoIU1hdHJpeENsaWVudFBlZy5nZXQoKSkge1xyXG4gICAgICAgICAgICAvLyBXZSBtYXkgbm90IGhhdmUgYSBjbGllbnQgeWV0IHdoZW4gaW52b2tlZCBmcm9tIHdlbGNvbWUgcGFnZVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwcm90b2NvbHNMb2FkaW5nOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRUaGlyZHBhcnR5UHJvdG9jb2xzKCkudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5wcm90b2NvbHMgPSByZXNwb25zZTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cHJvdG9jb2xzTG9hZGluZzogZmFsc2V9KTtcclxuICAgICAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgZXJyb3IgbG9hZGluZyB0aGlyZHBhcnR5IHByb3RvY29sczogJHtlcnJ9YCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Byb3RvY29sc0xvYWRpbmc6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNHdWVzdCgpKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBHdWVzdHMgY3VycmVudGx5IGFyZW4ndCBhbGxvd2VkIHRvIHVzZSB0aGlzIEFQSSwgc29cclxuICAgICAgICAgICAgICAgIC8vIGlnbm9yZSB0aGlzIGFzIG90aGVyd2lzZSB0aGlzIGVycm9yIGlzIGxpdGVyYWxseSB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHRoaW5nIHlvdSBzZWUgd2hlbiBsb2FkaW5nIHRoZSBjbGllbnQhXHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdHJhY2soJ0ZhaWxlZCB0byBnZXQgcHJvdG9jb2wgbGlzdCBmcm9tIGhvbWVzZXJ2ZXInKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBlcnJvcjogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgJ1Jpb3QgZmFpbGVkIHRvIGdldCB0aGUgcHJvdG9jb2wgbGlzdCBmcm9tIHRoZSBob21lc2VydmVyLiAnICtcclxuICAgICAgICAgICAgICAgICAgICAnVGhlIGhvbWVzZXJ2ZXIgbWF5IGJlIHRvbyBvbGQgdG8gc3VwcG9ydCB0aGlyZCBwYXJ0eSBuZXR3b3Jrcy4nLFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMucmVmcmVzaFJvb21MaXN0KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5maWx0ZXJUaW1lb3V0KSB7XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLmZpbHRlclRpbWVvdXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICByZWZyZXNoUm9vbUxpc3Q6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMubmV4dEJhdGNoID0gbnVsbDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcHVibGljUm9vbXM6IFtdLFxyXG4gICAgICAgICAgICBsb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZ2V0TW9yZVJvb21zKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldE1vcmVSb29tczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkpIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGxvYWRpbmc6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IG15X2ZpbHRlcl9zdHJpbmcgPSB0aGlzLnN0YXRlLmZpbHRlclN0cmluZztcclxuICAgICAgICBjb25zdCBteV9zZXJ2ZXIgPSB0aGlzLnN0YXRlLnJvb21TZXJ2ZXI7XHJcbiAgICAgICAgLy8gcmVtZW1iZXIgdGhlIG5leHQgYmF0Y2ggdG9rZW4gd2hlbiB3ZSBzZW50IHRoZSByZXF1ZXN0XHJcbiAgICAgICAgLy8gdG9vLiBJZiBpdCdzIGNoYW5nZWQsIGFwcGVuZGluZyB0byB0aGUgbGlzdCB3aWxsIGNvcnJ1cHQgaXQuXHJcbiAgICAgICAgY29uc3QgbXlfbmV4dF9iYXRjaCA9IHRoaXMubmV4dEJhdGNoO1xyXG4gICAgICAgIGNvbnN0IG9wdHMgPSB7bGltaXQ6IDIwfTtcclxuICAgICAgICBpZiAobXlfc2VydmVyICE9IE1hdHJpeENsaWVudFBlZy5nZXRIb21lc2VydmVyTmFtZSgpKSB7XHJcbiAgICAgICAgICAgIG9wdHMuc2VydmVyID0gbXlfc2VydmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pbnN0YW5jZUlkID09PSBBTExfUk9PTVMpIHtcclxuICAgICAgICAgICAgb3B0cy5pbmNsdWRlX2FsbF9uZXR3b3JrcyA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmluc3RhbmNlSWQpIHtcclxuICAgICAgICAgICAgb3B0cy50aGlyZF9wYXJ0eV9pbnN0YW5jZV9pZCA9IHRoaXMuc3RhdGUuaW5zdGFuY2VJZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMubmV4dEJhdGNoKSBvcHRzLnNpbmNlID0gdGhpcy5uZXh0QmF0Y2g7XHJcbiAgICAgICAgaWYgKG15X2ZpbHRlcl9zdHJpbmcpIG9wdHMuZmlsdGVyID0geyBnZW5lcmljX3NlYXJjaF90ZXJtOiBteV9maWx0ZXJfc3RyaW5nIH07XHJcbiAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5wdWJsaWNSb29tcyhvcHRzKS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIG15X2ZpbHRlcl9zdHJpbmcgIT0gdGhpcy5zdGF0ZS5maWx0ZXJTdHJpbmcgfHxcclxuICAgICAgICAgICAgICAgIG15X3NlcnZlciAhPSB0aGlzLnN0YXRlLnJvb21TZXJ2ZXIgfHxcclxuICAgICAgICAgICAgICAgIG15X25leHRfYmF0Y2ggIT0gdGhpcy5uZXh0QmF0Y2gpIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBmaWx0ZXIgb3Igc2VydmVyIGhhcyBjaGFuZ2VkIHNpbmNlIHRoaXMgcmVxdWVzdCB3YXMgc2VudCxcclxuICAgICAgICAgICAgICAgIC8vIHRocm93IGF3YXkgdGhlIHJlc3VsdCAoZG9uJ3QgZXZlbiBjbGVhciB0aGUgYnVzeSBmbGFnXHJcbiAgICAgICAgICAgICAgICAvLyBzaW5jZSB3ZSBtdXN0IHN0aWxsIGhhdmUgYSByZXF1ZXN0IGluIGZsaWdodClcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgd2UndmUgYmVlbiB1bm1vdW50ZWQsIHdlIGRvbid0IGNhcmUgZWl0aGVyLlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLm5leHRCYXRjaCA9IGRhdGEubmV4dF9iYXRjaDtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSgocykgPT4ge1xyXG4gICAgICAgICAgICAgICAgcy5wdWJsaWNSb29tcy5wdXNoKC4uLihkYXRhLmNodW5rIHx8IFtdKSk7XHJcbiAgICAgICAgICAgICAgICBzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIEJvb2xlYW4oZGF0YS5uZXh0X2JhdGNoKTtcclxuICAgICAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIG15X2ZpbHRlcl9zdHJpbmcgIT0gdGhpcy5zdGF0ZS5maWx0ZXJTdHJpbmcgfHxcclxuICAgICAgICAgICAgICAgIG15X3NlcnZlciAhPSB0aGlzLnN0YXRlLnJvb21TZXJ2ZXIgfHxcclxuICAgICAgICAgICAgICAgIG15X25leHRfYmF0Y2ggIT0gdGhpcy5uZXh0QmF0Y2gpIHtcclxuICAgICAgICAgICAgICAgIC8vIGFzIGFib3ZlOiB3ZSBkb24ndCBjYXJlIGFib3V0IGVycm9ycyBmb3Igb2xkXHJcbiAgICAgICAgICAgICAgICAvLyByZXF1ZXN0cyBlaXRoZXJcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgd2UndmUgYmVlbiB1bm1vdW50ZWQsIHdlIGRvbid0IGNhcmUgZWl0aGVyLlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIGdldCBwdWJsaWNSb29tczogJXNcIiwgSlNPTi5zdHJpbmdpZnkoZXJyKSk7XHJcbiAgICAgICAgICAgIHRyYWNrKCdGYWlsZWQgdG8gZ2V0IHB1YmxpYyByb29tIGxpc3QnKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOlxyXG4gICAgICAgICAgICAgICAgICAgIGAke190KCdSaW90IGZhaWxlZCB0byBnZXQgdGhlIHB1YmxpYyByb29tIGxpc3QuJyl9IGAgK1xyXG4gICAgICAgICAgICAgICAgICAgIGAkeyhlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdCgnVGhlIGhvbWVzZXJ2ZXIgbWF5IGJlIHVuYXZhaWxhYmxlIG9yIG92ZXJsb2FkZWQuJyl9YFxyXG4gICAgICAgICAgICAgICAgLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGxpbWl0ZWQgaW50ZXJmYWNlIGZvciByZW1vdmluZyByb29tcyBmcm9tIHRoZSBkaXJlY3RvcnkuXHJcbiAgICAgKiBXaWxsIHNldCB0aGUgcm9vbSB0byBub3QgYmUgcHVibGljbHkgdmlzaWJsZSBhbmQgZGVsZXRlIHRoZVxyXG4gICAgICogZGVmYXVsdCBhbGlhcy4gSW4gdGhlIGxvbmcgdGVybSwgaXQgd291bGQgYmUgYmV0dGVyIHRvIGFsbG93XHJcbiAgICAgKiBIUyBhZG1pbnMgdG8gZG8gdGhpcyB0aHJvdWdoIHRoZSBSb29tU2V0dGluZ3MgaW50ZXJmYWNlLCBidXRcclxuICAgICAqIHRoaXMgbmVlZHMgU1BFQy00MTcuXHJcbiAgICAgKi9cclxuICAgIHJlbW92ZUZyb21EaXJlY3Rvcnk6IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICBjb25zdCBhbGlhcyA9IGdldF9kaXNwbGF5X2FsaWFzX2Zvcl9yb29tKHJvb20pO1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSByb29tLm5hbWUgfHwgYWxpYXMgfHwgX3QoJ1VubmFtZWQgcm9vbScpO1xyXG5cclxuICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcblxyXG4gICAgICAgIGxldCBkZXNjO1xyXG4gICAgICAgIGlmIChhbGlhcykge1xyXG4gICAgICAgICAgICBkZXNjID0gX3QoJ0RlbGV0ZSB0aGUgcm9vbSBhbGlhcyAlKGFsaWFzKXMgYW5kIHJlbW92ZSAlKG5hbWUpcyBmcm9tIHRoZSBkaXJlY3Rvcnk/Jywge2FsaWFzOiBhbGlhcywgbmFtZTogbmFtZX0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRlc2MgPSBfdCgnUmVtb3ZlICUobmFtZSlzIGZyb20gdGhlIGRpcmVjdG9yeT8nLCB7bmFtZTogbmFtZX0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnUmVtb3ZlIGZyb20gRGlyZWN0b3J5JywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBfdCgnUmVtb3ZlIGZyb20gRGlyZWN0b3J5JyksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBkZXNjLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAoc2hvdWxkX2RlbGV0ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFzaG91bGRfZGVsZXRlKSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtb2RhbCA9IE1vZGFsLmNyZWF0ZURpYWxvZyhMb2FkZXIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHN0ZXAgPSBfdCgncmVtb3ZlICUobmFtZSlzIGZyb20gdGhlIGRpcmVjdG9yeS4nLCB7bmFtZTogbmFtZX0pO1xyXG5cclxuICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZXRSb29tRGlyZWN0b3J5VmlzaWJpbGl0eShyb29tLnJvb21faWQsICdwcml2YXRlJykudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFhbGlhcykgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIHN0ZXAgPSBfdCgnZGVsZXRlIHRoZSBhbGlhcy4nKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLmRlbGV0ZUFsaWFzKGFsaWFzKTtcclxuICAgICAgICAgICAgICAgIH0pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG1vZGFsLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoUm9vbUxpc3QoKTtcclxuICAgICAgICAgICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBtb2RhbC5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaFJvb21MaXN0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBcIiArIHN0ZXAgKyBcIjogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlbW92ZSBmcm9tIERpcmVjdG9yeSBFcnJvcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0Vycm9yJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KCdUaGUgc2VydmVyIG1heSBiZSB1bmF2YWlsYWJsZSBvciBvdmVybG9hZGVkJykpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbUNsaWNrZWQ6IGZ1bmN0aW9uKHJvb20sIGV2KSB7XHJcbiAgICAgICAgaWYgKGV2LnNoaWZ0S2V5KSB7XHJcbiAgICAgICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlRnJvbURpcmVjdG9yeShyb29tKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dSb29tKHJvb20pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25PcHRpb25DaGFuZ2U6IGZ1bmN0aW9uKHNlcnZlciwgaW5zdGFuY2VJZCkge1xyXG4gICAgICAgIC8vIGNsZWFyIG5leHQgYmF0Y2ggc28gd2UgZG9uJ3QgdHJ5IHRvIGxvYWQgbW9yZSByb29tc1xyXG4gICAgICAgIHRoaXMubmV4dEJhdGNoID0gbnVsbDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgLy8gQ2xlYXIgdGhlIHB1YmxpYyByb29tcyBvdXQgaGVyZSBvdGhlcndpc2Ugd2UgbmVlZGxlc3NseVxyXG4gICAgICAgICAgICAvLyBzcGVuZCB0aW1lIGZpbHRlcmluZyBsb3RzIG9mIHJvb21zIHdoZW4gd2UncmUgYWJvdXQgdG9cclxuICAgICAgICAgICAgLy8gdG8gY2xlYXIgdGhlIGxpc3QgYW55d2F5LlxyXG4gICAgICAgICAgICBwdWJsaWNSb29tczogW10sXHJcbiAgICAgICAgICAgIHJvb21TZXJ2ZXI6IHNlcnZlcixcclxuICAgICAgICAgICAgaW5zdGFuY2VJZDogaW5zdGFuY2VJZCxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgfSwgdGhpcy5yZWZyZXNoUm9vbUxpc3QpO1xyXG4gICAgICAgIC8vIFdlIGFsc28gcmVmcmVzaCB0aGUgcm9vbSBsaXN0IGVhY2ggdGltZSBldmVuIHRob3VnaCB0aGlzXHJcbiAgICAgICAgLy8gZmlsdGVyaW5nIGlzIGNsaWVudC1zaWRlLiBJdCBob3BlZnVsbHkgd29uJ3QgYmUgY2xpZW50IHNpZGVcclxuICAgICAgICAvLyBmb3IgdmVyeSBsb25nLCBhbmQgd2UgbWF5IGhhdmUgZmV0Y2hlZCBhIHRob3VzYW5kIHJvb21zIHRvXHJcbiAgICAgICAgLy8gZmluZCB0aGUgZml2ZSBnaXR0ZXIgb25lcywgYXQgd2hpY2ggcG9pbnQgd2UgZG8gbm90IHdhbnRcclxuICAgICAgICAvLyB0byByZW5kZXIgYWxsIHRob3NlIHJvb21zIHdoZW4gc3dpdGNoaW5nIGJhY2sgdG8gJ2FsbCBuZXR3b3JrcycuXHJcbiAgICAgICAgLy8gRWFzaWVzdCB0byBqdXN0IGJsb3cgYXdheSB0aGUgc3RhdGUgJiByZS1mZXRjaC5cclxuICAgIH0sXHJcblxyXG4gICAgb25GaWxsUmVxdWVzdDogZnVuY3Rpb24oYmFja3dhcmRzKSB7XHJcbiAgICAgICAgaWYgKGJhY2t3YXJkcyB8fCAhdGhpcy5uZXh0QmF0Y2gpIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRNb3JlUm9vbXMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25GaWx0ZXJDaGFuZ2U6IGZ1bmN0aW9uKGFsaWFzKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGZpbHRlclN0cmluZzogYWxpYXMgfHwgbnVsbCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gZG9uJ3Qgc2VuZCB0aGUgcmVxdWVzdCBmb3IgYSBsaXR0bGUgYml0LFxyXG4gICAgICAgIC8vIG5vIHBvaW50IGhhbW1lcmluZyB0aGUgc2VydmVyIHdpdGggYVxyXG4gICAgICAgIC8vIHJlcXVlc3QgZm9yIGV2ZXJ5IGtleXN0cm9rZSwgbGV0IHRoZVxyXG4gICAgICAgIC8vIHVzZXIgZmluaXNoIHR5cGluZy5cclxuICAgICAgICBpZiAodGhpcy5maWx0ZXJUaW1lb3V0KSB7XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLmZpbHRlclRpbWVvdXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmZpbHRlclRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maWx0ZXJUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoUm9vbUxpc3QoKTtcclxuICAgICAgICB9LCA3MDApO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkZpbHRlckNsZWFyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyB1cGRhdGUgaW1tZWRpYXRlbHlcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZmlsdGVyU3RyaW5nOiBudWxsLFxyXG4gICAgICAgIH0sIHRoaXMucmVmcmVzaFJvb21MaXN0KTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZmlsdGVyVGltZW91dCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5maWx0ZXJUaW1lb3V0KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uSm9pbkZyb21TZWFyY2hDbGljazogZnVuY3Rpb24oYWxpYXMpIHtcclxuICAgICAgICAvLyBJZiB3ZSBkb24ndCBoYXZlIGEgcGFydGljdWxhciBpbnN0YW5jZSBpZCBzZWxlY3RlZCwganVzdCBzaG93IHRoYXQgcm9vbXMgYWxpYXNcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuaW5zdGFuY2VJZCB8fCB0aGlzLnN0YXRlLmluc3RhbmNlSWQgPT09IEFMTF9ST09NUykge1xyXG4gICAgICAgICAgICAvLyBJZiB0aGUgdXNlciBzcGVjaWZpZWQgYW4gYWxpYXMgd2l0aG91dCBhIGRvbWFpbiwgYWRkIG9uIHdoaWNoZXZlciBzZXJ2ZXIgaXMgc2VsZWN0ZWRcclxuICAgICAgICAgICAgLy8gaW4gdGhlIGRyb3Bkb3duXHJcbiAgICAgICAgICAgIGlmIChhbGlhcy5pbmRleE9mKCc6JykgPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgIGFsaWFzID0gYWxpYXMgKyAnOicgKyB0aGlzLnN0YXRlLnJvb21TZXJ2ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zaG93Um9vbUFsaWFzKGFsaWFzLCB0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBUaGlzIGlzIGEgM3JkIHBhcnR5IHByb3RvY29sLiBMZXQncyBzZWUgaWYgd2UgY2FuIGpvaW4gaXRcclxuICAgICAgICAgICAgY29uc3QgcHJvdG9jb2xOYW1lID0gcHJvdG9jb2xOYW1lRm9ySW5zdGFuY2VJZCh0aGlzLnByb3RvY29scywgdGhpcy5zdGF0ZS5pbnN0YW5jZUlkKTtcclxuICAgICAgICAgICAgY29uc3QgaW5zdGFuY2UgPSBpbnN0YW5jZUZvckluc3RhbmNlSWQodGhpcy5wcm90b2NvbHMsIHRoaXMuc3RhdGUuaW5zdGFuY2VJZCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpZWxkcyA9IHByb3RvY29sTmFtZSA/IHRoaXMuX2dldEZpZWxkc0ZvclRoaXJkUGFydHlMb2NhdGlvbihhbGlhcywgdGhpcy5wcm90b2NvbHNbcHJvdG9jb2xOYW1lXSwgaW5zdGFuY2UpIDogbnVsbDtcclxuICAgICAgICAgICAgaWYgKCFmaWVsZHMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdVbmFibGUgdG8gam9pbiBuZXR3b3JrJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdVbmFibGUgdG8gam9pbiBuZXR3b3JrJyksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdSaW90IGRvZXMgbm90IGtub3cgaG93IHRvIGpvaW4gYSByb29tIG9uIHRoaXMgbmV0d29yaycpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFRoaXJkcGFydHlMb2NhdGlvbihwcm90b2NvbE5hbWUsIGZpZWxkcykudGhlbigocmVzcCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3AubGVuZ3RoID4gMCAmJiByZXNwWzBdLmFsaWFzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaG93Um9vbUFsaWFzKHJlc3BbMF0uYWxpYXMsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1Jvb20gbm90IGZvdW5kJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnUm9vbSBub3QgZm91bmQnKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdDb3VsZG5cXCd0IGZpbmQgYSBtYXRjaGluZyBNYXRyaXggcm9vbScpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZldGNoaW5nIHRoaXJkIHBhcnR5IGxvY2F0aW9uIGZhaWxlZCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRmV0Y2hpbmcgdGhpcmQgcGFydHkgbG9jYXRpb24gZmFpbGVkJyksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdVbmFibGUgdG8gbG9vayB1cCByb29tIElEIGZyb20gc2VydmVyJyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblByZXZpZXdDbGljazogZnVuY3Rpb24oZXYsIHJvb20pIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICByb29tX2lkOiByb29tLnJvb21faWQsXHJcbiAgICAgICAgICAgIHNob3VsZF9wZWVrOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblZpZXdDbGljazogZnVuY3Rpb24oZXYsIHJvb20pIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICByb29tX2lkOiByb29tLnJvb21faWQsXHJcbiAgICAgICAgICAgIHNob3VsZF9wZWVrOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Kb2luQ2xpY2s6IGZ1bmN0aW9uKGV2LCByb29tKSB7XHJcbiAgICAgICAgdGhpcy5zaG93Um9vbShyb29tLCBudWxsLCB0cnVlKTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DcmVhdGVSb29tQ2xpY2s6IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3ZpZXdfY3JlYXRlX3Jvb20nfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3dSb29tQWxpYXM6IGZ1bmN0aW9uKGFsaWFzLCBhdXRvSm9pbj1mYWxzZSkge1xyXG4gICAgICAgIHRoaXMuc2hvd1Jvb20obnVsbCwgYWxpYXMsIGF1dG9Kb2luKTtcclxuICAgIH0sXHJcblxyXG4gICAgc2hvd1Jvb206IGZ1bmN0aW9uKHJvb20sIHJvb21fYWxpYXMsIGF1dG9Kb2luPWZhbHNlKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgY29uc3QgcGF5bG9hZCA9IHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgYXV0b19qb2luOiBhdXRvSm9pbixcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChyb29tKSB7XHJcbiAgICAgICAgICAgIC8vIERvbid0IGxldCB0aGUgdXNlciB2aWV3IGEgcm9vbSB0aGV5IHdvbid0IGJlIGFibGUgdG8gZWl0aGVyXHJcbiAgICAgICAgICAgIC8vIHBlZWsgb3Igam9pbjogZmFpbCBlYXJsaWVyIHNvIHRoZXkgZG9uJ3QgaGF2ZSB0byBjbGljayBiYWNrXHJcbiAgICAgICAgICAgIC8vIHRvIHRoZSBkaXJlY3RvcnkuXHJcbiAgICAgICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNHdWVzdCgpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXJvb20ud29ybGRfcmVhZGFibGUgJiYgIXJvb20uZ3Vlc3RfY2FuX2pvaW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJ30pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCFyb29tX2FsaWFzKSB7XHJcbiAgICAgICAgICAgICAgICByb29tX2FsaWFzID0gZ2V0X2Rpc3BsYXlfYWxpYXNfZm9yX3Jvb20ocm9vbSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHBheWxvYWQub29iX2RhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBhdmF0YXJVcmw6IHJvb20uYXZhdGFyX3VybCxcclxuICAgICAgICAgICAgICAgIC8vIFhYWDogVGhpcyBsb2dpYyBpcyBkdXBsaWNhdGVkIGZyb20gdGhlIEpTIFNESyB3aGljaFxyXG4gICAgICAgICAgICAgICAgLy8gd291bGQgbm9ybWFsbHkgZGVjaWRlIHdoYXQgdGhlIG5hbWUgaXMuXHJcbiAgICAgICAgICAgICAgICBuYW1lOiByb29tLm5hbWUgfHwgcm9vbV9hbGlhcyB8fCBfdCgnVW5uYW1lZCByb29tJyksXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5yb29tU2VydmVyKSB7XHJcbiAgICAgICAgICAgICAgICBwYXlsb2FkLm9wdHMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmlhU2VydmVyczogW3RoaXMuc3RhdGUucm9vbVNlcnZlcl0sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIEl0J3Mgbm90IHJlYWxseSBwb3NzaWJsZSB0byBqb2luIE1hdHJpeCByb29tcyBieSBJRCBiZWNhdXNlIHRoZSBIUyBoYXMgbm8gd2F5IHRvIGtub3dcclxuICAgICAgICAvLyB3aGljaCBzZXJ2ZXJzIHRvIHN0YXJ0IHF1ZXJ5aW5nLiBIb3dldmVyLCB0aGVyZSdzIG5vIG90aGVyIHdheSB0byBqb2luIHJvb21zIGluXHJcbiAgICAgICAgLy8gdGhpcyBsaXN0IHdpdGhvdXQgYWxpYXNlcyBhdCBwcmVzZW50LCBzbyBpZiByb29tQWxpYXMgaXNuJ3Qgc2V0IGhlcmUgd2UgaGF2ZSBub1xyXG4gICAgICAgIC8vIGNob2ljZSBidXQgdG8gc3VwcGx5IHRoZSBJRC5cclxuICAgICAgICBpZiAocm9vbV9hbGlhcykge1xyXG4gICAgICAgICAgICBwYXlsb2FkLnJvb21fYWxpYXMgPSByb29tX2FsaWFzO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHBheWxvYWQucm9vbV9pZCA9IHJvb20ucm9vbV9pZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHBheWxvYWQpO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRSb3cocm9vbSkge1xyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCBjbGllbnRSb29tID0gY2xpZW50LmdldFJvb20ocm9vbS5yb29tX2lkKTtcclxuICAgICAgICBjb25zdCBoYXNKb2luZWRSb29tID0gY2xpZW50Um9vbSAmJiBjbGllbnRSb29tLmdldE15TWVtYmVyc2hpcCgpID09PSBcImpvaW5cIjtcclxuICAgICAgICBjb25zdCBpc0d1ZXN0ID0gY2xpZW50LmlzR3Vlc3QoKTtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudCgnYXZhdGFycy5CYXNlQXZhdGFyJyk7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgICAgICBsZXQgcHJldmlld0J1dHRvbjtcclxuICAgICAgICBsZXQgam9pbk9yVmlld0J1dHRvbjtcclxuXHJcbiAgICAgICAgaWYgKHJvb20ud29ybGRfcmVhZGFibGUgJiYgIWhhc0pvaW5lZFJvb20pIHtcclxuICAgICAgICAgICAgcHJldmlld0J1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJzZWNvbmRhcnlcIiBvbkNsaWNrPXsoZXYpID0+IHRoaXMub25QcmV2aWV3Q2xpY2soZXYsIHJvb20pfT57X3QoXCJQcmV2aWV3XCIpfTwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGhhc0pvaW5lZFJvb20pIHtcclxuICAgICAgICAgICAgam9pbk9yVmlld0J1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJzZWNvbmRhcnlcIiBvbkNsaWNrPXsoZXYpID0+IHRoaXMub25WaWV3Q2xpY2soZXYsIHJvb20pfT57X3QoXCJWaWV3XCIpfTwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCFpc0d1ZXN0IHx8IHJvb20uZ3Vlc3RfY2FuX2pvaW4pIHtcclxuICAgICAgICAgICAgam9pbk9yVmlld0J1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJwcmltYXJ5XCIgb25DbGljaz17KGV2KSA9PiB0aGlzLm9uSm9pbkNsaWNrKGV2LCByb29tKX0+e190KFwiSm9pblwiKX08L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgbmFtZSA9IHJvb20ubmFtZSB8fCBnZXRfZGlzcGxheV9hbGlhc19mb3Jfcm9vbShyb29tKSB8fCBfdCgnVW5uYW1lZCByb29tJyk7XHJcbiAgICAgICAgaWYgKG5hbWUubGVuZ3RoID4gTUFYX05BTUVfTEVOR1RIKSB7XHJcbiAgICAgICAgICAgIG5hbWUgPSBgJHtuYW1lLnN1YnN0cmluZygwLCBNQVhfTkFNRV9MRU5HVEgpfS4uLmA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdG9waWMgPSByb29tLnRvcGljIHx8ICcnO1xyXG4gICAgICAgIGlmICh0b3BpYy5sZW5ndGggPiBNQVhfVE9QSUNfTEVOR1RIKSB7XHJcbiAgICAgICAgICAgIHRvcGljID0gYCR7dG9waWMuc3Vic3RyaW5nKDAsIE1BWF9UT1BJQ19MRU5HVEgpfS4uLmA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRvcGljID0gbGlua2lmeUFuZFNhbml0aXplSHRtbCh0b3BpYyk7XHJcbiAgICAgICAgY29uc3QgYXZhdGFyVXJsID0gZ2V0SHR0cFVyaUZvck14YyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SG9tZXNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvb20uYXZhdGFyX3VybCwgMzIsIDMyLCBcImNyb3BcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPHRyIGtleT17IHJvb20ucm9vbV9pZCB9XHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZXYpID0+IHRoaXMub25Sb29tQ2xpY2tlZChyb29tLCBldil9XHJcbiAgICAgICAgICAgICAgICAvLyBjYW5jZWwgb25Nb3VzZURvd24gb3RoZXJ3aXNlIHNoaWZ0LWNsaWNraW5nIGhpZ2hsaWdodHMgdGV4dFxyXG4gICAgICAgICAgICAgICAgb25Nb3VzZURvd249eyhldikgPT4ge2V2LnByZXZlbnREZWZhdWx0KCk7fX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPHRkIGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3Rvcnlfcm9vbUF2YXRhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxCYXNlQXZhdGFyIHdpZHRoPXszMn0gaGVpZ2h0PXszMn0gcmVzaXplTWV0aG9kPSdjcm9wJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPXsgbmFtZSB9IGlkTmFtZT17IG5hbWUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw9eyBhdmF0YXJVcmwgfSAvPlxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZCBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3Jvb21EZXNjcmlwdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbURpcmVjdG9yeV9uYW1lXCI+eyBuYW1lIH08L2Rpdj4mbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3RvcnlfdG9waWNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsgKGV2KSA9PiB7IGV2LnN0b3BQcm9wYWdhdGlvbigpOyB9IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiB0b3BpYyB9fSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbURpcmVjdG9yeV9hbGlhc1wiPnsgZ2V0X2Rpc3BsYXlfYWxpYXNfZm9yX3Jvb20ocm9vbSkgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZCBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3Jvb21NZW1iZXJDb3VudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgcm9vbS5udW1fam9pbmVkX21lbWJlcnMgfVxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZCBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3ByZXZpZXdcIj57cHJldmlld0J1dHRvbn08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkIGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3Rvcnlfam9pblwiPntqb2luT3JWaWV3QnV0dG9ufTwvdGQ+XHJcbiAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29sbGVjdFNjcm9sbFBhbmVsOiBmdW5jdGlvbihlbGVtZW50KSB7XHJcbiAgICAgICAgdGhpcy5zY3JvbGxQYW5lbCA9IGVsZW1lbnQ7XHJcbiAgICB9LFxyXG5cclxuICAgIF9zdHJpbmdMb29rc0xpa2VJZDogZnVuY3Rpb24ocywgZmllbGRfdHlwZSkge1xyXG4gICAgICAgIGxldCBwYXQgPSAvXiNbXlxcc10rOlteXFxzXS87XHJcbiAgICAgICAgaWYgKGZpZWxkX3R5cGUgJiYgZmllbGRfdHlwZS5yZWdleHApIHtcclxuICAgICAgICAgICAgcGF0ID0gbmV3IFJlZ0V4cChmaWVsZF90eXBlLnJlZ2V4cCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcGF0LnRlc3Qocyk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRGaWVsZHNGb3JUaGlyZFBhcnR5TG9jYXRpb246IGZ1bmN0aW9uKHVzZXJJbnB1dCwgcHJvdG9jb2wsIGluc3RhbmNlKSB7XHJcbiAgICAgICAgLy8gbWFrZSBhbiBvYmplY3Qgd2l0aCB0aGUgZmllbGRzIHNwZWNpZmllZCBieSB0aGF0IHByb3RvY29sLiBXZVxyXG4gICAgICAgIC8vIHJlcXVpcmUgdGhhdCB0aGUgdmFsdWVzIG9mIGFsbCBidXQgdGhlIGxhc3QgZmllbGQgY29tZSBmcm9tIHRoZVxyXG4gICAgICAgIC8vIGluc3RhbmNlLiBUaGUgbGFzdCBpcyB0aGUgdXNlciBpbnB1dC5cclxuICAgICAgICBjb25zdCByZXF1aXJlZEZpZWxkcyA9IHByb3RvY29sLmxvY2F0aW9uX2ZpZWxkcztcclxuICAgICAgICBpZiAoIXJlcXVpcmVkRmllbGRzKSByZXR1cm4gbnVsbDtcclxuICAgICAgICBjb25zdCBmaWVsZHMgPSB7fTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlcXVpcmVkRmllbGRzLmxlbmd0aCAtIDE7ICsraSkge1xyXG4gICAgICAgICAgICBjb25zdCB0aGlzRmllbGQgPSByZXF1aXJlZEZpZWxkc1tpXTtcclxuICAgICAgICAgICAgaWYgKGluc3RhbmNlLmZpZWxkc1t0aGlzRmllbGRdID09PSB1bmRlZmluZWQpIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICBmaWVsZHNbdGhpc0ZpZWxkXSA9IGluc3RhbmNlLmZpZWxkc1t0aGlzRmllbGRdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmaWVsZHNbcmVxdWlyZWRGaWVsZHNbcmVxdWlyZWRGaWVsZHMubGVuZ3RoIC0gMV1dID0gdXNlcklucHV0O1xyXG4gICAgICAgIHJldHVybiBmaWVsZHM7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogY2FsbGVkIGJ5IHRoZSBwYXJlbnQgY29tcG9uZW50IHdoZW4gUGFnZVVwL0Rvd24vZXRjIGlzIHByZXNzZWQuXHJcbiAgICAgKlxyXG4gICAgICogV2UgcGFzcyBpdCBkb3duIHRvIHRoZSBzY3JvbGwgcGFuZWwuXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZVNjcm9sbEtleTogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBpZiAodGhpcy5zY3JvbGxQYW5lbCkge1xyXG4gICAgICAgICAgICB0aGlzLnNjcm9sbFBhbmVsLmhhbmRsZVNjcm9sbEtleShldik7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICBsZXQgY29udGVudDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvcikge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gdGhpcy5zdGF0ZS5lcnJvcjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUucHJvdG9jb2xzTG9hZGluZykge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPExvYWRlciAvPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCByb3dzID0gKHRoaXMuc3RhdGUucHVibGljUm9vbXMgfHwgW10pLm1hcChyb29tID0+IHRoaXMuZ2V0Um93KHJvb20pKTtcclxuICAgICAgICAgICAgLy8gd2Ugc3RpbGwgc2hvdyB0aGUgc2Nyb2xscGFuZWwsIGF0IGxlYXN0IGZvciBub3csIGJlY2F1c2VcclxuICAgICAgICAgICAgLy8gb3RoZXJ3aXNlIHdlIGRvbid0IGZldGNoIG1vcmUgYmVjYXVzZSB3ZSBkb24ndCBnZXQgYSBmaWxsXHJcbiAgICAgICAgICAgIC8vIHJlcXVlc3QgZnJvbSB0aGUgc2Nyb2xscGFuZWwgYmVjYXVzZSB0aGVyZSBpc24ndCBvbmVcclxuXHJcbiAgICAgICAgICAgIGxldCBzcGlubmVyO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5sb2FkaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBzcGlubmVyID0gPExvYWRlciAvPjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHNjcm9sbHBhbmVsX2NvbnRlbnQ7XHJcbiAgICAgICAgICAgIGlmIChyb3dzLmxlbmd0aCA9PT0gMCAmJiAhdGhpcy5zdGF0ZS5sb2FkaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBzY3JvbGxwYW5lbF9jb250ZW50ID0gPGk+eyBfdCgnTm8gcm9vbXMgdG8gc2hvdycpIH08L2k+O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgc2Nyb2xscGFuZWxfY29udGVudCA9IDx0YWJsZSBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3RhYmxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHJvd3MgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICAgICAgICA8L3RhYmxlPjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBTY3JvbGxQYW5lbCA9IHNkay5nZXRDb21wb25lbnQoXCJzdHJ1Y3R1cmVzLlNjcm9sbFBhbmVsXCIpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPFNjcm9sbFBhbmVsIHJlZj17dGhpcy5jb2xsZWN0U2Nyb2xsUGFuZWx9XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3RhYmxlV3JhcHBlclwiXHJcbiAgICAgICAgICAgICAgICBvbkZpbGxSZXF1ZXN0PXsgdGhpcy5vbkZpbGxSZXF1ZXN0IH1cclxuICAgICAgICAgICAgICAgIHN0aWNreUJvdHRvbT17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICBzdGFydEF0Qm90dG9tPXtmYWxzZX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgeyBzY3JvbGxwYW5lbF9jb250ZW50IH1cclxuICAgICAgICAgICAgICAgIHsgc3Bpbm5lciB9XHJcbiAgICAgICAgICAgIDwvU2Nyb2xsUGFuZWw+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGxpc3RIZWFkZXI7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnByb3RvY29sc0xvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgTmV0d29ya0Ryb3Bkb3duID0gc2RrLmdldENvbXBvbmVudCgnZGlyZWN0b3J5Lk5ldHdvcmtEcm9wZG93bicpO1xyXG4gICAgICAgICAgICBjb25zdCBEaXJlY3RvcnlTZWFyY2hCb3ggPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5EaXJlY3RvcnlTZWFyY2hCb3gnKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb3RvY29sTmFtZSA9IHByb3RvY29sTmFtZUZvckluc3RhbmNlSWQodGhpcy5wcm90b2NvbHMsIHRoaXMuc3RhdGUuaW5zdGFuY2VJZCk7XHJcbiAgICAgICAgICAgIGxldCBpbnN0YW5jZV9leHBlY3RlZF9maWVsZF90eXBlO1xyXG4gICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgICBwcm90b2NvbE5hbWUgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvdG9jb2xzICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3RvY29sc1twcm90b2NvbE5hbWVdICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3RvY29sc1twcm90b2NvbE5hbWVdLmxvY2F0aW9uX2ZpZWxkcy5sZW5ndGggPiAwICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3RvY29sc1twcm90b2NvbE5hbWVdLmZpZWxkX3R5cGVzXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbGFzdF9maWVsZCA9IHRoaXMucHJvdG9jb2xzW3Byb3RvY29sTmFtZV0ubG9jYXRpb25fZmllbGRzLnNsaWNlKC0xKVswXTtcclxuICAgICAgICAgICAgICAgIGluc3RhbmNlX2V4cGVjdGVkX2ZpZWxkX3R5cGUgPSB0aGlzLnByb3RvY29sc1twcm90b2NvbE5hbWVdLmZpZWxkX3R5cGVzW2xhc3RfZmllbGRdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgcGxhY2Vob2xkZXIgPSBfdCgnRmluZCBhIHJvb23igKYnKTtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmluc3RhbmNlSWQgfHwgdGhpcy5zdGF0ZS5pbnN0YW5jZUlkID09PSBBTExfUk9PTVMpIHtcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gX3QoXCJGaW5kIGEgcm9vbeKApiAoZS5nLiAlKGV4YW1wbGVSb29tKXMpXCIsIHtleGFtcGxlUm9vbTogXCIjZXhhbXBsZTpcIiArIHRoaXMuc3RhdGUucm9vbVNlcnZlcn0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGluc3RhbmNlX2V4cGVjdGVkX2ZpZWxkX3R5cGUpIHtcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gaW5zdGFuY2VfZXhwZWN0ZWRfZmllbGRfdHlwZS5wbGFjZWhvbGRlcjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHNob3dKb2luQnV0dG9uID0gdGhpcy5fc3RyaW5nTG9va3NMaWtlSWQodGhpcy5zdGF0ZS5maWx0ZXJTdHJpbmcsIGluc3RhbmNlX2V4cGVjdGVkX2ZpZWxkX3R5cGUpO1xyXG4gICAgICAgICAgICBpZiAocHJvdG9jb2xOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbnN0YW5jZSA9IGluc3RhbmNlRm9ySW5zdGFuY2VJZCh0aGlzLnByb3RvY29scywgdGhpcy5zdGF0ZS5pbnN0YW5jZUlkKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9nZXRGaWVsZHNGb3JUaGlyZFBhcnR5TG9jYXRpb24odGhpcy5zdGF0ZS5maWx0ZXJTdHJpbmcsIHRoaXMucHJvdG9jb2xzW3Byb3RvY29sTmFtZV0sIGluc3RhbmNlKSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3dKb2luQnV0dG9uID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxpc3RIZWFkZXIgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3RvcnlfbGlzdGhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgPERpcmVjdG9yeVNlYXJjaEJveFxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3Rvcnlfc2VhcmNoYm94XCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkZpbHRlckNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsZWFyPXt0aGlzLm9uRmlsdGVyQ2xlYXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Kb2luQ2xpY2s9e3RoaXMub25Kb2luRnJvbVNlYXJjaENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cclxuICAgICAgICAgICAgICAgICAgICBzaG93Sm9pbkJ1dHRvbj17c2hvd0pvaW5CdXR0b259XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPE5ldHdvcmtEcm9wZG93blxyXG4gICAgICAgICAgICAgICAgICAgIHByb3RvY29scz17dGhpcy5wcm90b2NvbHN9XHJcbiAgICAgICAgICAgICAgICAgICAgb25PcHRpb25DaGFuZ2U9e3RoaXMub25PcHRpb25DaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRTZXJ2ZXJOYW1lPXt0aGlzLnN0YXRlLnJvb21TZXJ2ZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRJbnN0YW5jZUlkPXt0aGlzLnN0YXRlLmluc3RhbmNlSWR9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGV4cGxhbmF0aW9uID1cclxuICAgICAgICAgICAgX3QoXCJJZiB5b3UgY2FuJ3QgZmluZCB0aGUgcm9vbSB5b3UncmUgbG9va2luZyBmb3IsIGFzayBmb3IgYW4gaW52aXRlIG9yIDxhPkNyZWF0ZSBhIG5ldyByb29tPC9hPi5cIiwgbnVsbCxcclxuICAgICAgICAgICAgICAgIHthOiBzdWIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAga2luZD1cInNlY29uZGFyeVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DcmVhdGVSb29tQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPntzdWJ9PC9BY2Nlc3NpYmxlQnV0dG9uPik7XHJcbiAgICAgICAgICAgICAgICB9fSxcclxuICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2dcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17J214X1Jvb21EaXJlY3RvcnlfZGlhbG9nJ31cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMucHJvcHMub25GaW5pc2hlZH1cclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIkV4cGxvcmUgcm9vbXNcIil9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbURpcmVjdG9yeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtleHBsYW5hdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21EaXJlY3RvcnlfbGlzdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7bGlzdEhlYWRlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbi8vIFNpbWlsYXIgdG8gbWF0cml4LXJlYWN0LXNkaydzIE1hdHJpeFRvb2xzLmdldERpc3BsYXlBbGlhc0ZvclJvb21cclxuLy8gYnV0IHdvcmtzIHdpdGggdGhlIG9iamVjdHMgd2UgZ2V0IGZyb20gdGhlIHB1YmxpYyByb29tIGxpc3RcclxuZnVuY3Rpb24gZ2V0X2Rpc3BsYXlfYWxpYXNfZm9yX3Jvb20ocm9vbSkge1xyXG4gICAgcmV0dXJuIHJvb20uY2Fub25pY2FsX2FsaWFzIHx8IChyb29tLmFsaWFzZXMgPyByb29tLmFsaWFzZXNbMF0gOiBcIlwiKTtcclxufVxyXG4iXX0=