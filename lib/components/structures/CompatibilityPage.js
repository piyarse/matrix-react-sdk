"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../languageHandler");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'CompatibilityPage',
  propTypes: {
    onAccept: _propTypes.default.func
  },
  getDefaultProps: function () {
    return {
      onAccept: function () {} // NOP

    };
  },
  onAccept: function () {
    this.props.onAccept();
  },
  render: function () {
    return _react.default.createElement("div", {
      className: "mx_CompatibilityPage"
    }, _react.default.createElement("div", {
      className: "mx_CompatibilityPage_box"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Sorry, your browser is <b>not</b> able to run Riot.", {}, {
      'b': sub => _react.default.createElement("b", null, sub)
    }), " "), _react.default.createElement("p", null, (0, _languageHandler._t)("Riot uses many advanced browser features, some of which are not available " + "or experimental in your current browser.")), _react.default.createElement("p", null, (0, _languageHandler._t)('Please install <chromeLink>Chrome</chromeLink>, <firefoxLink>Firefox</firefoxLink>, ' + 'or <safariLink>Safari</safariLink> for the best experience.', {}, {
      'chromeLink': sub => _react.default.createElement("a", {
        href: "https://www.google.com/chrome"
      }, sub),
      'firefoxLink': sub => _react.default.createElement("a", {
        href: "https://firefox.com"
      }, sub),
      'safariLink': sub => _react.default.createElement("a", {
        href: "https://apple.com/safari"
      }, sub)
    })), _react.default.createElement("p", null, (0, _languageHandler._t)("With your current browser, the look and feel of the application may be " + "completely incorrect, and some or all features may not function. " + "If you want to try it anyway you can continue, but you are on your own in terms " + "of any issues you may encounter!")), _react.default.createElement("button", {
      onClick: this.onAccept
    }, (0, _languageHandler._t)("I understand the risks and wish to continue"))));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvQ29tcGF0aWJpbGl0eVBhZ2UuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJvbkFjY2VwdCIsIlByb3BUeXBlcyIsImZ1bmMiLCJnZXREZWZhdWx0UHJvcHMiLCJwcm9wcyIsInJlbmRlciIsInN1YiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUF1QmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsbUJBRGU7QUFFNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxRQUFRLEVBQUVDLG1CQUFVQztBQURiLEdBRmlCO0FBTTVCQyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hILE1BQUFBLFFBQVEsRUFBRSxZQUFXLENBQUUsQ0FEcEIsQ0FDc0I7O0FBRHRCLEtBQVA7QUFHSCxHQVYyQjtBQVk1QkEsRUFBQUEsUUFBUSxFQUFFLFlBQVc7QUFDakIsU0FBS0ksS0FBTCxDQUFXSixRQUFYO0FBQ0gsR0FkMkI7QUFnQjVCSyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFdBQ0E7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksd0NBQUsseUJBQUcscURBQUgsRUFBMEQsRUFBMUQsRUFBOEQ7QUFBRSxXQUFNQyxHQUFELElBQVMsd0NBQUlBLEdBQUo7QUFBaEIsS0FBOUQsQ0FBTCxNQURKLEVBRUksd0NBQ0UseUJBQ0UsK0VBQ0EsMENBRkYsQ0FERixDQUZKLEVBUUksd0NBQ0UseUJBQ0UseUZBQ0EsNkRBRkYsRUFHRSxFQUhGLEVBSUU7QUFDSSxvQkFBZUEsR0FBRCxJQUFTO0FBQUcsUUFBQSxJQUFJLEVBQUM7QUFBUixTQUF5Q0EsR0FBekMsQ0FEM0I7QUFFSSxxQkFBZ0JBLEdBQUQsSUFBUztBQUFHLFFBQUEsSUFBSSxFQUFDO0FBQVIsU0FBK0JBLEdBQS9CLENBRjVCO0FBR0ksb0JBQWVBLEdBQUQsSUFBUztBQUFHLFFBQUEsSUFBSSxFQUFDO0FBQVIsU0FBb0NBLEdBQXBDO0FBSDNCLEtBSkYsQ0FERixDQVJKLEVBb0JJLHdDQUNFLHlCQUNFLDRFQUNBLG1FQURBLEdBRUEsa0ZBRkEsR0FHQSxrQ0FKRixDQURGLENBcEJKLEVBNEJJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS047QUFBdEIsT0FDTSx5QkFBRyw2Q0FBSCxDQUROLENBNUJKLENBREosQ0FEQTtBQW9DSDtBQXJEMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdDb21wYXRpYmlsaXR5UGFnZScsXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBvbkFjY2VwdDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgb25BY2NlcHQ6IGZ1bmN0aW9uKCkge30sIC8vIE5PUFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWNjZXB0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uQWNjZXB0KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NvbXBhdGliaWxpdHlQYWdlXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ29tcGF0aWJpbGl0eVBhZ2VfYm94XCI+XHJcbiAgICAgICAgICAgICAgICA8cD57IF90KFwiU29ycnksIHlvdXIgYnJvd3NlciBpcyA8Yj5ub3Q8L2I+IGFibGUgdG8gcnVuIFJpb3QuXCIsIHt9LCB7ICdiJzogKHN1YikgPT4gPGI+e3N1Yn08L2I+IH0pIH0gPC9wPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICB7IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiUmlvdCB1c2VzIG1hbnkgYWR2YW5jZWQgYnJvd3NlciBmZWF0dXJlcywgc29tZSBvZiB3aGljaCBhcmUgbm90IGF2YWlsYWJsZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvciBleHBlcmltZW50YWwgaW4geW91ciBjdXJyZW50IGJyb3dzZXIuXCIsXHJcbiAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgeyBfdChcclxuICAgICAgICAgICAgICAgICAgICAnUGxlYXNlIGluc3RhbGwgPGNocm9tZUxpbms+Q2hyb21lPC9jaHJvbWVMaW5rPiwgPGZpcmVmb3hMaW5rPkZpcmVmb3g8L2ZpcmVmb3hMaW5rPiwgJyArXHJcbiAgICAgICAgICAgICAgICAgICAgJ29yIDxzYWZhcmlMaW5rPlNhZmFyaTwvc2FmYXJpTGluaz4gZm9yIHRoZSBiZXN0IGV4cGVyaWVuY2UuJyxcclxuICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdjaHJvbWVMaW5rJzogKHN1YikgPT4gPGEgaHJlZj1cImh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vY2hyb21lXCI+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnZmlyZWZveExpbmsnOiAoc3ViKSA9PiA8YSBocmVmPVwiaHR0cHM6Ly9maXJlZm94LmNvbVwiPntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NhZmFyaUxpbmsnOiAoc3ViKSA9PiA8YSBocmVmPVwiaHR0cHM6Ly9hcHBsZS5jb20vc2FmYXJpXCI+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICB7IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiV2l0aCB5b3VyIGN1cnJlbnQgYnJvd3NlciwgdGhlIGxvb2sgYW5kIGZlZWwgb2YgdGhlIGFwcGxpY2F0aW9uIG1heSBiZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJjb21wbGV0ZWx5IGluY29ycmVjdCwgYW5kIHNvbWUgb3IgYWxsIGZlYXR1cmVzIG1heSBub3QgZnVuY3Rpb24uIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIklmIHlvdSB3YW50IHRvIHRyeSBpdCBhbnl3YXkgeW91IGNhbiBjb250aW51ZSwgYnV0IHlvdSBhcmUgb24geW91ciBvd24gaW4gdGVybXMgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwib2YgYW55IGlzc3VlcyB5b3UgbWF5IGVuY291bnRlciFcIixcclxuICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uQWNjZXB0fT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KFwiSSB1bmRlcnN0YW5kIHRoZSByaXNrcyBhbmQgd2lzaCB0byBjb250aW51ZVwiKSB9XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=