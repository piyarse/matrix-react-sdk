"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _SettingsStore = _interopRequireDefault(require("../../settings/SettingsStore"));

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var Matrix = _interopRequireWildcard(require("matrix-js-sdk"));

var _languageHandler = require("../../languageHandler");

var _MatrixClientPeg = require("../../MatrixClientPeg");

var ObjectUtils = _interopRequireWildcard(require("../../ObjectUtils"));

var _UserActivity = _interopRequireDefault(require("../../UserActivity"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var sdk = _interopRequireWildcard(require("../../index"));

var _Keyboard = require("../../Keyboard");

var _Timer = _interopRequireDefault(require("../../utils/Timer"));

var _shouldHideEvent = _interopRequireDefault(require("../../shouldHideEvent"));

var _EditorStateTransfer = _interopRequireDefault(require("../../utils/EditorStateTransfer"));

var _EventTile = require("../views/rooms/EventTile");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 New Vector Ltd
Copyright 2019-2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const PAGINATE_SIZE = 20;
const INITIAL_SIZE = 20;
const READ_RECEIPT_INTERVAL_MS = 500;
const DEBUG = false;

let debuglog = function () {};

if (DEBUG) {
  // using bind means that we get to keep useful line numbers in the console
  debuglog = console.log.bind(console);
}
/*
 * Component which shows the event timeline in a room view.
 *
 * Also responsible for handling and sending read receipts.
 */


const TimelinePanel = (0, _createReactClass.default)({
  displayName: 'TimelinePanel',
  propTypes: {
    // The js-sdk EventTimelineSet object for the timeline sequence we are
    // representing.  This may or may not have a room, depending on what it's
    // a timeline representing.  If it has a room, we maintain RRs etc for
    // that room.
    timelineSet: _propTypes.default.object.isRequired,
    showReadReceipts: _propTypes.default.bool,
    // Enable managing RRs and RMs. These require the timelineSet to have a room.
    manageReadReceipts: _propTypes.default.bool,
    manageReadMarkers: _propTypes.default.bool,
    // true to give the component a 'display: none' style.
    hidden: _propTypes.default.bool,
    // ID of an event to highlight. If undefined, no event will be highlighted.
    // typically this will be either 'eventId' or undefined.
    highlightedEventId: _propTypes.default.string,
    // id of an event to jump to. If not given, will go to the end of the
    // live timeline.
    eventId: _propTypes.default.string,
    // where to position the event given by eventId, in pixels from the
    // bottom of the viewport. If not given, will try to put the event
    // half way down the viewport.
    eventPixelOffset: _propTypes.default.number,
    // Should we show URL Previews
    showUrlPreview: _propTypes.default.bool,
    // callback which is called when the panel is scrolled.
    onScroll: _propTypes.default.func,
    // callback which is called when the read-up-to mark is updated.
    onReadMarkerUpdated: _propTypes.default.func,
    // callback which is called when we wish to paginate the timeline
    // window.
    onPaginationRequest: _propTypes.default.func,
    // maximum number of events to show in a timeline
    timelineCap: _propTypes.default.number,
    // classname to use for the messagepanel
    className: _propTypes.default.string,
    // shape property to be passed to EventTiles
    tileShape: _propTypes.default.string,
    // placeholder text to use if the timeline is empty
    empty: _propTypes.default.string,
    // whether to show reactions for an event
    showReactions: _propTypes.default.bool
  },
  statics: {
    // a map from room id to read marker event timestamp
    roomReadMarkerTsMap: {}
  },
  getDefaultProps: function () {
    return {
      // By default, disable the timelineCap in favour of unpaginating based on
      // event tile heights. (See _unpaginateEvents)
      timelineCap: Number.MAX_VALUE,
      className: 'mx_RoomView_messagePanel'
    };
  },
  getInitialState: function () {
    // XXX: we could track RM per TimelineSet rather than per Room.
    // but for now we just do it per room for simplicity.
    let initialReadMarker = null;

    if (this.props.manageReadMarkers) {
      const readmarker = this.props.timelineSet.room.getAccountData('m.fully_read');

      if (readmarker) {
        initialReadMarker = readmarker.getContent().event_id;
      } else {
        initialReadMarker = this._getCurrentReadReceipt();
      }
    }

    return {
      events: [],
      liveEvents: [],
      timelineLoading: true,
      // track whether our room timeline is loading
      // the index of the first event that is to be shown
      firstVisibleEventIndex: 0,
      // canBackPaginate == false may mean:
      //
      // * we haven't (successfully) loaded the timeline yet, or:
      //
      // * we have got to the point where the room was created, or:
      //
      // * the server indicated that there were no more visible events
      //  (normally implying we got to the start of the room), or:
      //
      // * we gave up asking the server for more events
      canBackPaginate: false,
      // canForwardPaginate == false may mean:
      //
      // * we haven't (successfully) loaded the timeline yet
      //
      // * we have got to the end of time and are now tracking the live
      //   timeline, or:
      //
      // * the server indicated that there were no more visible events
      //   (not sure if this ever happens when we're not at the live
      //   timeline), or:
      //
      // * we are looking at some historical point, but gave up asking
      //   the server for more events
      canForwardPaginate: false,
      // start with the read-marker visible, so that we see its animated
      // disappearance when switching into the room.
      readMarkerVisible: true,
      readMarkerEventId: initialReadMarker,
      backPaginating: false,
      forwardPaginating: false,
      // cache of matrixClient.getSyncState() (but from the 'sync' event)
      clientSyncState: _MatrixClientPeg.MatrixClientPeg.get().getSyncState(),
      // should the event tiles have twelve hour times
      isTwelveHour: _SettingsStore.default.getValue("showTwelveHourTimestamps"),
      // always show timestamps on event tiles?
      alwaysShowTimestamps: _SettingsStore.default.getValue("alwaysShowTimestamps"),
      // how long to show the RM for when it's visible in the window
      readMarkerInViewThresholdMs: _SettingsStore.default.getValue("readMarkerInViewThresholdMs"),
      // how long to show the RM for when it's scrolled off-screen
      readMarkerOutOfViewThresholdMs: _SettingsStore.default.getValue("readMarkerOutOfViewThresholdMs")
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    debuglog("TimelinePanel: mounting");
    this.lastRRSentEventId = undefined;
    this.lastRMSentEventId = undefined;
    this._messagePanel = (0, _react.createRef)();

    if (this.props.manageReadReceipts) {
      this.updateReadReceiptOnUserActivity();
    }

    if (this.props.manageReadMarkers) {
      this.updateReadMarkerOnUserActivity();
    }

    this.dispatcherRef = _dispatcher.default.register(this.onAction);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.timeline", this.onRoomTimeline);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.timelineReset", this.onRoomTimelineReset);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.redaction", this.onRoomRedaction); // same event handler as Room.redaction as for both we just do forceUpdate


    _MatrixClientPeg.MatrixClientPeg.get().on("Room.redactionCancelled", this.onRoomRedaction);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.receipt", this.onRoomReceipt);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.localEchoUpdated", this.onLocalEchoUpdated);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.accountData", this.onAccountData);

    _MatrixClientPeg.MatrixClientPeg.get().on("Event.decrypted", this.onEventDecrypted);

    _MatrixClientPeg.MatrixClientPeg.get().on("Event.replaced", this.onEventReplaced);

    _MatrixClientPeg.MatrixClientPeg.get().on("sync", this.onSync);

    this._initTimeline(this.props);
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (newProps) {
    if (newProps.timelineSet !== this.props.timelineSet) {
      // throw new Error("changing timelineSet on a TimelinePanel is not supported");
      // regrettably, this does happen; in particular, when joining a
      // room with /join. In that case, there are two Rooms in
      // circulation - one which is created by the MatrixClient.joinRoom
      // call and used to create the RoomView, and a second which is
      // created by the sync loop once the room comes back down the /sync
      // pipe. Once the latter happens, our room is replaced with the new one.
      //
      // for now, just warn about this. But we're going to end up paginating
      // both rooms separately, and it's all bad.
      console.warn("Replacing timelineSet on a TimelinePanel - confusion may ensue");
    }

    if (newProps.eventId != this.props.eventId) {
      console.log("TimelinePanel switching to eventId " + newProps.eventId + " (was " + this.props.eventId + ")");
      return this._initTimeline(newProps);
    }
  },
  shouldComponentUpdate: function (nextProps, nextState) {
    if (!ObjectUtils.shallowEqual(this.props, nextProps)) {
      if (DEBUG) {
        console.group("Timeline.shouldComponentUpdate: props change");
        console.log("props before:", this.props);
        console.log("props after:", nextProps);
        console.groupEnd();
      }

      return true;
    }

    if (!ObjectUtils.shallowEqual(this.state, nextState)) {
      if (DEBUG) {
        console.group("Timeline.shouldComponentUpdate: state change");
        console.log("state before:", this.state);
        console.log("state after:", nextState);
        console.groupEnd();
      }

      return true;
    }

    return false;
  },
  componentWillUnmount: function () {
    // set a boolean to say we've been unmounted, which any pending
    // promises can use to throw away their results.
    //
    // (We could use isMounted, but facebook have deprecated that.)
    this.unmounted = true;

    if (this._readReceiptActivityTimer) {
      this._readReceiptActivityTimer.abort();

      this._readReceiptActivityTimer = null;
    }

    if (this._readMarkerActivityTimer) {
      this._readMarkerActivityTimer.abort();

      this._readMarkerActivityTimer = null;
    }

    _dispatcher.default.unregister(this.dispatcherRef);

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client) {
      client.removeListener("Room.timeline", this.onRoomTimeline);
      client.removeListener("Room.timelineReset", this.onRoomTimelineReset);
      client.removeListener("Room.redaction", this.onRoomRedaction);
      client.removeListener("Room.redactionCancelled", this.onRoomRedaction);
      client.removeListener("Room.receipt", this.onRoomReceipt);
      client.removeListener("Room.localEchoUpdated", this.onLocalEchoUpdated);
      client.removeListener("Room.accountData", this.onAccountData);
      client.removeListener("Event.decrypted", this.onEventDecrypted);
      client.removeListener("Event.replaced", this.onEventReplaced);
      client.removeListener("sync", this.onSync);
    }
  },
  onMessageListUnfillRequest: function (backwards, scrollToken) {
    // If backwards, unpaginate from the back (i.e. the start of the timeline)
    const dir = backwards ? Matrix.EventTimeline.BACKWARDS : Matrix.EventTimeline.FORWARDS;
    debuglog("TimelinePanel: unpaginating events in direction", dir); // All tiles are inserted by MessagePanel to have a scrollToken === eventId, and
    // this particular event should be the first or last to be unpaginated.

    const eventId = scrollToken;
    const marker = this.state.events.findIndex(ev => {
      return ev.getId() === eventId;
    });
    const count = backwards ? marker + 1 : this.state.events.length - marker;

    if (count > 0) {
      debuglog("TimelinePanel: Unpaginating", count, "in direction", dir);

      this._timelineWindow.unpaginate(count, backwards); // We can now paginate in the unpaginated direction


      const canPaginateKey = backwards ? 'canBackPaginate' : 'canForwardPaginate';

      const {
        events,
        liveEvents,
        firstVisibleEventIndex
      } = this._getEvents();

      this.setState({
        [canPaginateKey]: true,
        events,
        liveEvents,
        firstVisibleEventIndex
      });
    }
  },

  onPaginationRequest(timelineWindow, direction, size) {
    if (this.props.onPaginationRequest) {
      return this.props.onPaginationRequest(timelineWindow, direction, size);
    } else {
      return timelineWindow.paginate(direction, size);
    }
  },

  // set off a pagination request.
  onMessageListFillRequest: function (backwards) {
    if (!this._shouldPaginate()) return Promise.resolve(false);
    const dir = backwards ? Matrix.EventTimeline.BACKWARDS : Matrix.EventTimeline.FORWARDS;
    const canPaginateKey = backwards ? 'canBackPaginate' : 'canForwardPaginate';
    const paginatingKey = backwards ? 'backPaginating' : 'forwardPaginating';

    if (!this.state[canPaginateKey]) {
      debuglog("TimelinePanel: have given up", dir, "paginating this timeline");
      return Promise.resolve(false);
    }

    if (!this._timelineWindow.canPaginate(dir)) {
      debuglog("TimelinePanel: can't", dir, "paginate any further");
      this.setState({
        [canPaginateKey]: false
      });
      return Promise.resolve(false);
    }

    if (backwards && this.state.firstVisibleEventIndex !== 0) {
      debuglog("TimelinePanel: won't", dir, "paginate past first visible event");
      return Promise.resolve(false);
    }

    debuglog("TimelinePanel: Initiating paginate; backwards:" + backwards);
    this.setState({
      [paginatingKey]: true
    });
    return this.onPaginationRequest(this._timelineWindow, dir, PAGINATE_SIZE).then(r => {
      if (this.unmounted) {
        return;
      }

      debuglog("TimelinePanel: paginate complete backwards:" + backwards + "; success:" + r);

      const {
        events,
        liveEvents,
        firstVisibleEventIndex
      } = this._getEvents();

      const newState = {
        [paginatingKey]: false,
        [canPaginateKey]: r,
        events,
        liveEvents,
        firstVisibleEventIndex
      }; // moving the window in this direction may mean that we can now
      // paginate in the other where we previously could not.

      const otherDirection = backwards ? Matrix.EventTimeline.FORWARDS : Matrix.EventTimeline.BACKWARDS;
      const canPaginateOtherWayKey = backwards ? 'canForwardPaginate' : 'canBackPaginate';

      if (!this.state[canPaginateOtherWayKey] && this._timelineWindow.canPaginate(otherDirection)) {
        debuglog('TimelinePanel: can now', otherDirection, 'paginate again');
        newState[canPaginateOtherWayKey] = true;
      } // Don't resolve until the setState has completed: we need to let
      // the component update before we consider the pagination completed,
      // otherwise we'll end up paginating in all the history the js-sdk
      // has in memory because we never gave the component a chance to scroll
      // itself into the right place


      return new Promise(resolve => {
        this.setState(newState, () => {
          // we can continue paginating in the given direction if:
          // - _timelineWindow.paginate says we can
          // - we're paginating forwards, or we won't be trying to
          //   paginate backwards past the first visible event
          resolve(r && (!backwards || firstVisibleEventIndex === 0));
        });
      });
    });
  },
  onMessageListScroll: function (e) {
    if (this.props.onScroll) {
      this.props.onScroll(e);
    }

    if (this.props.manageReadMarkers) {
      const rmPosition = this.getReadMarkerPosition(); // we hide the read marker when it first comes onto the screen, but if
      // it goes back off the top of the screen (presumably because the user
      // clicks on the 'jump to bottom' button), we need to re-enable it.

      if (rmPosition < 0) {
        this.setState({
          readMarkerVisible: true
        });
      } // if read marker position goes between 0 and -1/1,
      // (and user is active), switch timeout


      const timeout = this._readMarkerTimeout(rmPosition); // NO-OP when timeout already has set to the given value


      this._readMarkerActivityTimer.changeTimeout(timeout);
    }
  },
  onAction: function (payload) {
    if (payload.action === 'ignore_state_changed') {
      this.forceUpdate();
    }

    if (payload.action === "edit_event") {
      const editState = payload.event ? new _EditorStateTransfer.default(payload.event) : null;
      this.setState({
        editState
      }, () => {
        if (payload.event && this._messagePanel.current) {
          this._messagePanel.current.scrollToEventIfNeeded(payload.event.getId());
        }
      });
    }
  },
  onRoomTimeline: function (ev, room, toStartOfTimeline, removed, data) {
    // ignore events for other timeline sets
    if (data.timeline.getTimelineSet() !== this.props.timelineSet) return; // ignore anything but real-time updates at the end of the room:
    // updates from pagination will happen when the paginate completes.

    if (toStartOfTimeline || !data || !data.liveEvent) return;
    if (!this._messagePanel.current) return;

    if (!this._messagePanel.current.getScrollState().stuckAtBottom) {
      // we won't load this event now, because we don't want to push any
      // events off the other end of the timeline. But we need to note
      // that we can now paginate.
      this.setState({
        canForwardPaginate: true
      });
      return;
    } // tell the timeline window to try to advance itself, but not to make
    // an http request to do so.
    //
    // we deliberately avoid going via the ScrollPanel for this call - the
    // ScrollPanel might already have an active pagination promise, which
    // will fail, but would stop us passing the pagination request to the
    // timeline window.
    //
    // see https://github.com/vector-im/vector-web/issues/1035


    this._timelineWindow.paginate(Matrix.EventTimeline.FORWARDS, 1, false).then(() => {
      if (this.unmounted) {
        return;
      }

      const {
        events,
        liveEvents,
        firstVisibleEventIndex
      } = this._getEvents();

      const lastLiveEvent = liveEvents[liveEvents.length - 1];
      const updatedState = {
        events,
        liveEvents,
        firstVisibleEventIndex
      };
      let callRMUpdated;

      if (this.props.manageReadMarkers) {
        // when a new event arrives when the user is not watching the
        // window, but the window is in its auto-scroll mode, make sure the
        // read marker is visible.
        //
        // We ignore events we have sent ourselves; we don't want to see the
        // read-marker when a remote echo of an event we have just sent takes
        // more than the timeout on userActiveRecently.
        //
        const myUserId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId;

        const sender = ev.sender ? ev.sender.userId : null;
        callRMUpdated = false;

        if (sender != myUserId && !_UserActivity.default.sharedInstance().userActiveRecently()) {
          updatedState.readMarkerVisible = true;
        } else if (lastLiveEvent && this.getReadMarkerPosition() === 0) {
          // we know we're stuckAtBottom, so we can advance the RM
          // immediately, to save a later render cycle
          this._setReadMarker(lastLiveEvent.getId(), lastLiveEvent.getTs(), true);

          updatedState.readMarkerVisible = false;
          updatedState.readMarkerEventId = lastLiveEvent.getId();
          callRMUpdated = true;
        }
      }

      this.setState(updatedState, () => {
        this._messagePanel.current.updateTimelineMinHeight();

        if (callRMUpdated) {
          this.props.onReadMarkerUpdated();
        }
      });
    });
  },
  onRoomTimelineReset: function (room, timelineSet) {
    if (timelineSet !== this.props.timelineSet) return;

    if (this._messagePanel.current && this._messagePanel.current.isAtBottom()) {
      this._loadTimeline();
    }
  },
  canResetTimeline: function () {
    return this._messagePanel.current && this._messagePanel.current.isAtBottom();
  },
  onRoomRedaction: function (ev, room) {
    if (this.unmounted) return; // ignore events for other rooms

    if (room !== this.props.timelineSet.room) return; // we could skip an update if the event isn't in our timeline,
    // but that's probably an early optimisation.

    this.forceUpdate();
  },
  onEventReplaced: function (replacedEvent, room) {
    if (this.unmounted) return; // ignore events for other rooms

    if (room !== this.props.timelineSet.room) return; // we could skip an update if the event isn't in our timeline,
    // but that's probably an early optimisation.

    this.forceUpdate();
  },
  onRoomReceipt: function (ev, room) {
    if (this.unmounted) return; // ignore events for other rooms

    if (room !== this.props.timelineSet.room) return;
    this.forceUpdate();
  },
  onLocalEchoUpdated: function (ev, room, oldEventId) {
    if (this.unmounted) return; // ignore events for other rooms

    if (room !== this.props.timelineSet.room) return;

    this._reloadEvents();
  },
  onAccountData: function (ev, room) {
    if (this.unmounted) return; // ignore events for other rooms

    if (room !== this.props.timelineSet.room) return;
    if (ev.getType() !== "m.fully_read") return; // XXX: roomReadMarkerTsMap not updated here so it is now inconsistent. Replace
    // this mechanism of determining where the RM is relative to the view-port with
    // one supported by the server (the client needs more than an event ID).

    this.setState({
      readMarkerEventId: ev.getContent().event_id
    }, this.props.onReadMarkerUpdated);
  },
  onEventDecrypted: function (ev) {
    // Can be null for the notification timeline, etc.
    if (!this.props.timelineSet.room) return; // Need to update as we don't display event tiles for events that
    // haven't yet been decrypted. The event will have just been updated
    // in place so we just need to re-render.
    // TODO: We should restrict this to only events in our timeline,
    // but possibly the event tile itself should just update when this
    // happens to save us re-rendering the whole timeline.

    if (ev.getRoomId() === this.props.timelineSet.room.roomId) {
      this.forceUpdate();
    }
  },
  onSync: function (state, prevState, data) {
    this.setState({
      clientSyncState: state
    });
  },

  _readMarkerTimeout(readMarkerPosition) {
    return readMarkerPosition === 0 ? this.state.readMarkerInViewThresholdMs : this.state.readMarkerOutOfViewThresholdMs;
  },

  updateReadMarkerOnUserActivity: async function () {
    const initialTimeout = this._readMarkerTimeout(this.getReadMarkerPosition());

    this._readMarkerActivityTimer = new _Timer.default(initialTimeout);

    while (this._readMarkerActivityTimer) {
      //unset on unmount
      _UserActivity.default.sharedInstance().timeWhileActiveRecently(this._readMarkerActivityTimer);

      try {
        await this._readMarkerActivityTimer.finished();
      } catch (e) {
        continue;
        /* aborted */
      } // outside of try/catch to not swallow errors


      this.updateReadMarker();
    }
  },
  updateReadReceiptOnUserActivity: async function () {
    this._readReceiptActivityTimer = new _Timer.default(READ_RECEIPT_INTERVAL_MS);

    while (this._readReceiptActivityTimer) {
      //unset on unmount
      _UserActivity.default.sharedInstance().timeWhileActiveNow(this._readReceiptActivityTimer);

      try {
        await this._readReceiptActivityTimer.finished();
      } catch (e) {
        continue;
        /* aborted */
      } // outside of try/catch to not swallow errors


      this.sendReadReceipt();
    }
  },
  sendReadReceipt: function () {
    if (_SettingsStore.default.getValue("lowBandwidth")) return;
    if (!this._messagePanel.current) return;
    if (!this.props.manageReadReceipts) return; // This happens on user_activity_end which is delayed, and it's
    // very possible have logged out within that timeframe, so check
    // we still have a client.

    const cli = _MatrixClientPeg.MatrixClientPeg.get(); // if no client or client is guest don't send RR or RM


    if (!cli || cli.isGuest()) return;
    let shouldSendRR = true;

    const currentRREventId = this._getCurrentReadReceipt(true);

    const currentRREventIndex = this._indexForEventId(currentRREventId); // We want to avoid sending out read receipts when we are looking at
    // events in the past which are before the latest RR.
    //
    // For now, let's apply a heuristic: if (a) the event corresponding to
    // the latest RR (either from the server, or sent by ourselves) doesn't
    // appear in our timeline, and (b) we could forward-paginate the event
    // timeline, then don't send any more RRs.
    //
    // This isn't watertight, as we could be looking at a section of
    // timeline which is *after* the latest RR (so we should actually send
    // RRs) - but that is a bit of a niche case. It will sort itself out when
    // the user eventually hits the live timeline.
    //


    if (currentRREventId && currentRREventIndex === null && this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS)) {
      shouldSendRR = false;
    }

    const lastReadEventIndex = this._getLastDisplayedEventIndex({
      ignoreOwn: true
    });

    if (lastReadEventIndex === null) {
      shouldSendRR = false;
    }

    let lastReadEvent = this.state.events[lastReadEventIndex];
    shouldSendRR = shouldSendRR && // Only send a RR if the last read event is ahead in the timeline relative to
    // the current RR event.
    lastReadEventIndex > currentRREventIndex && // Only send a RR if the last RR set != the one we would send
    this.lastRRSentEventId != lastReadEvent.getId(); // Only send a RM if the last RM sent != the one we would send

    const shouldSendRM = this.lastRMSentEventId != this.state.readMarkerEventId; // we also remember the last read receipt we sent to avoid spamming the
    // same one at the server repeatedly

    if (shouldSendRR || shouldSendRM) {
      if (shouldSendRR) {
        this.lastRRSentEventId = lastReadEvent.getId();
      } else {
        lastReadEvent = null;
      }

      this.lastRMSentEventId = this.state.readMarkerEventId;
      const roomId = this.props.timelineSet.room.roomId;
      const hiddenRR = !_SettingsStore.default.getValue("sendReadReceipts", roomId);
      debuglog('TimelinePanel: Sending Read Markers for ', this.props.timelineSet.room.roomId, 'rm', this.state.readMarkerEventId, lastReadEvent ? 'rr ' + lastReadEvent.getId() : '', ' hidden:' + hiddenRR);

      _MatrixClientPeg.MatrixClientPeg.get().setRoomReadMarkers(this.props.timelineSet.room.roomId, this.state.readMarkerEventId, lastReadEvent, // Could be null, in which case no RR is sent
      {
        hidden: hiddenRR
      }).catch(e => {
        // /read_markers API is not implemented on this HS, fallback to just RR
        if (e.errcode === 'M_UNRECOGNIZED' && lastReadEvent) {
          return _MatrixClientPeg.MatrixClientPeg.get().sendReadReceipt(lastReadEvent, {
            hidden: hiddenRR
          }).catch(e => {
            console.error(e);
            this.lastRRSentEventId = undefined;
          });
        } else {
          console.error(e);
        } // it failed, so allow retries next time the user is active


        this.lastRRSentEventId = undefined;
        this.lastRMSentEventId = undefined;
      }); // do a quick-reset of our unreadNotificationCount to avoid having
      // to wait from the remote echo from the homeserver.
      // we only do this if we're right at the end, because we're just assuming
      // that sending an RR for the latest message will set our notif counter
      // to zero: it may not do this if we send an RR for somewhere before the end.


      if (this.isAtEndOfLiveTimeline()) {
        this.props.timelineSet.room.setUnreadNotificationCount('total', 0);
        this.props.timelineSet.room.setUnreadNotificationCount('highlight', 0);

        _dispatcher.default.dispatch({
          action: 'on_room_read',
          roomId: this.props.timelineSet.room.roomId
        });
      }
    }
  },
  // if the read marker is on the screen, we can now assume we've caught up to the end
  // of the screen, so move the marker down to the bottom of the screen.
  updateReadMarker: function () {
    if (!this.props.manageReadMarkers) return;

    if (this.getReadMarkerPosition() === 1) {
      // the read marker is at an event below the viewport,
      // we don't want to rewind it.
      return;
    } // move the RM to *after* the message at the bottom of the screen. This
    // avoids a problem whereby we never advance the RM if there is a huge
    // message which doesn't fit on the screen.


    const lastDisplayedIndex = this._getLastDisplayedEventIndex({
      allowPartial: true
    });

    if (lastDisplayedIndex === null) {
      return;
    }

    const lastDisplayedEvent = this.state.events[lastDisplayedIndex];

    this._setReadMarker(lastDisplayedEvent.getId(), lastDisplayedEvent.getTs()); // the read-marker should become invisible, so that if the user scrolls
    // down, they don't see it.


    if (this.state.readMarkerVisible) {
      this.setState({
        readMarkerVisible: false
      });
    }
  },
  // advance the read marker past any events we sent ourselves.
  _advanceReadMarkerPastMyEvents: function () {
    if (!this.props.manageReadMarkers) return; // we call `_timelineWindow.getEvents()` rather than using
    // `this.state.liveEvents`, because React batches the update to the
    // latter, so it may not have been updated yet.

    const events = this._timelineWindow.getEvents(); // first find where the current RM is


    let i;

    for (i = 0; i < events.length; i++) {
      if (events[i].getId() == this.state.readMarkerEventId) {
        break;
      }
    }

    if (i >= events.length) {
      return;
    } // now think about advancing it


    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId;

    for (i++; i < events.length; i++) {
      const ev = events[i];

      if (!ev.sender || ev.sender.userId != myUserId) {
        break;
      }
    } // i is now the first unread message which we didn't send ourselves.


    i--;
    const ev = events[i];

    this._setReadMarker(ev.getId(), ev.getTs());
  },

  /* jump down to the bottom of this room, where new events are arriving
   */
  jumpToLiveTimeline: function () {
    // if we can't forward-paginate the existing timeline, then there
    // is no point reloading it - just jump straight to the bottom.
    //
    // Otherwise, reload the timeline rather than trying to paginate
    // through all of space-time.
    if (this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS)) {
      this._loadTimeline();
    } else {
      if (this._messagePanel.current) {
        this._messagePanel.current.scrollToBottom();
      }
    }
  },

  /* scroll to show the read-up-to marker. We put it 1/3 of the way down
   * the container.
   */
  jumpToReadMarker: function () {
    if (!this.props.manageReadMarkers) return;
    if (!this._messagePanel.current) return;
    if (!this.state.readMarkerEventId) return; // we may not have loaded the event corresponding to the read-marker
    // into the _timelineWindow. In that case, attempts to scroll to it
    // will fail.
    //
    // a quick way to figure out if we've loaded the relevant event is
    // simply to check if the messagepanel knows where the read-marker is.

    const ret = this._messagePanel.current.getReadMarkerPosition();

    if (ret !== null) {
      // The messagepanel knows where the RM is, so we must have loaded
      // the relevant event.
      this._messagePanel.current.scrollToEvent(this.state.readMarkerEventId, 0, 1 / 3);

      return;
    } // Looks like we haven't loaded the event corresponding to the read-marker.
    // As with jumpToLiveTimeline, we want to reload the timeline around the
    // read-marker.


    this._loadTimeline(this.state.readMarkerEventId, 0, 1 / 3);
  },

  /* update the read-up-to marker to match the read receipt
   */
  forgetReadMarker: function () {
    if (!this.props.manageReadMarkers) return;

    const rmId = this._getCurrentReadReceipt(); // see if we know the timestamp for the rr event


    const tl = this.props.timelineSet.getTimelineForEvent(rmId);
    let rmTs;

    if (tl) {
      const event = tl.getEvents().find(e => {
        return e.getId() == rmId;
      });

      if (event) {
        rmTs = event.getTs();
      }
    }

    this._setReadMarker(rmId, rmTs);
  },

  /* return true if the content is fully scrolled down and we are
   * at the end of the live timeline.
   */
  isAtEndOfLiveTimeline: function () {
    return this._messagePanel.current && this._messagePanel.current.isAtBottom() && this._timelineWindow && !this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS);
  },

  /* get the current scroll state. See ScrollPanel.getScrollState for
   * details.
   *
   * returns null if we are not mounted.
   */
  getScrollState: function () {
    if (!this._messagePanel.current) {
      return null;
    }

    return this._messagePanel.current.getScrollState();
  },
  // returns one of:
  //
  //  null: there is no read marker
  //  -1: read marker is above the window
  //   0: read marker is visible
  //  +1: read marker is below the window
  getReadMarkerPosition: function () {
    if (!this.props.manageReadMarkers) return null;
    if (!this._messagePanel.current) return null;

    const ret = this._messagePanel.current.getReadMarkerPosition();

    if (ret !== null) {
      return ret;
    } // the messagePanel doesn't know where the read marker is.
    // if we know the timestamp of the read marker, make a guess based on that.


    const rmTs = TimelinePanel.roomReadMarkerTsMap[this.props.timelineSet.room.roomId];

    if (rmTs && this.state.events.length > 0) {
      if (rmTs < this.state.events[0].getTs()) {
        return -1;
      } else {
        return 1;
      }
    }

    return null;
  },
  canJumpToReadMarker: function () {
    // 1. Do not show jump bar if neither the RM nor the RR are set.
    // 3. We want to show the bar if the read-marker is off the top of the screen.
    // 4. Also, if pos === null, the event might not be paginated - show the unread bar
    const pos = this.getReadMarkerPosition();
    const ret = this.state.readMarkerEventId !== null && ( // 1.
    pos < 0 || pos === null); // 3., 4.

    return ret;
  },

  /*
   * called by the parent component when PageUp/Down/etc is pressed.
   *
   * We pass it down to the scroll panel.
   */
  handleScrollKey: function (ev) {
    if (!this._messagePanel.current) {
      return;
    } // jump to the live timeline on ctrl-end, rather than the end of the
    // timeline window.


    if (ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey && ev.key === _Keyboard.Key.END) {
      this.jumpToLiveTimeline();
    } else {
      this._messagePanel.current.handleScrollKey(ev);
    }
  },
  _initTimeline: function (props) {
    const initialEvent = props.eventId;
    const pixelOffset = props.eventPixelOffset; // if a pixelOffset is given, it is relative to the bottom of the
    // container. If not, put the event in the middle of the container.

    let offsetBase = 1;

    if (pixelOffset == null) {
      offsetBase = 0.5;
    }

    return this._loadTimeline(initialEvent, pixelOffset, offsetBase);
  },

  /**
   * (re)-load the event timeline, and initialise the scroll state, centered
   * around the given event.
   *
   * @param {string?}  eventId the event to focus on. If undefined, will
   *    scroll to the bottom of the room.
   *
   * @param {number?} pixelOffset   offset to position the given event at
   *    (pixels from the offsetBase). If omitted, defaults to 0.
   *
   * @param {number?} offsetBase the reference point for the pixelOffset. 0
   *     means the top of the container, 1 means the bottom, and fractional
   *     values mean somewhere in the middle. If omitted, it defaults to 0.
   *
   * returns a promise which will resolve when the load completes.
   */
  _loadTimeline: function (eventId, pixelOffset, offsetBase) {
    this._timelineWindow = new Matrix.TimelineWindow(_MatrixClientPeg.MatrixClientPeg.get(), this.props.timelineSet, {
      windowLimit: this.props.timelineCap
    });

    const onLoaded = () => {
      // clear the timeline min-height when
      // (re)loading the timeline
      if (this._messagePanel.current) {
        this._messagePanel.current.onTimelineReset();
      }

      this._reloadEvents(); // If we switched away from the room while there were pending
      // outgoing events, the read-marker will be before those events.
      // We need to skip over any which have subsequently been sent.


      this._advanceReadMarkerPastMyEvents();

      this.setState({
        canBackPaginate: this._timelineWindow.canPaginate(Matrix.EventTimeline.BACKWARDS),
        canForwardPaginate: this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS),
        timelineLoading: false
      }, () => {
        // initialise the scroll state of the message panel
        if (!this._messagePanel.current) {
          // this shouldn't happen - we know we're mounted because
          // we're in a setState callback, and we know
          // timelineLoading is now false, so render() should have
          // mounted the message panel.
          console.log("can't initialise scroll state because " + "messagePanel didn't load");
          return;
        }

        if (eventId) {
          this._messagePanel.current.scrollToEvent(eventId, pixelOffset, offsetBase);
        } else {
          this._messagePanel.current.scrollToBottom();
        }

        this.sendReadReceipt();
      });
    };

    const onError = error => {
      this.setState({
        timelineLoading: false
      });
      console.error("Error loading timeline panel at ".concat(eventId, ": ").concat(error));
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      let onFinished; // if we were given an event ID, then when the user closes the
      // dialog, let's jump to the end of the timeline. If we weren't,
      // something has gone badly wrong and rather than causing a loop of
      // undismissable dialogs, let's just give up.

      if (eventId) {
        onFinished = () => {
          // go via the dispatcher so that the URL is updated
          _dispatcher.default.dispatch({
            action: 'view_room',
            room_id: this.props.timelineSet.room.roomId
          });
        };
      }

      let message;

      if (error.errcode == 'M_FORBIDDEN') {
        message = (0, _languageHandler._t)("Tried to load a specific point in this room's timeline, but you " + "do not have permission to view the message in question.");
      } else {
        message = (0, _languageHandler._t)("Tried to load a specific point in this room's timeline, but was " + "unable to find it.");
      }

      _Modal.default.createTrackedDialog('Failed to load timeline position', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to load timeline position"),
        description: message,
        onFinished: onFinished
      });
    }; // if we already have the event in question, TimelineWindow.load
    // returns a resolved promise.
    //
    // In this situation, we don't really want to defer the update of the
    // state to the next event loop, because it makes room-switching feel
    // quite slow. So we detect that situation and shortcut straight to
    // calling _reloadEvents and updating the state.


    const timeline = this.props.timelineSet.getTimelineForEvent(eventId);

    if (timeline) {
      // This is a hot-path optimization by skipping a promise tick
      // by repeating a no-op sync branch in TimelineSet.getTimelineForEvent & MatrixClient.getEventTimeline
      this._timelineWindow.load(eventId, INITIAL_SIZE); // in this branch this method will happen in sync time


      onLoaded();
    } else {
      const prom = this._timelineWindow.load(eventId, INITIAL_SIZE);

      this.setState({
        events: [],
        liveEvents: [],
        canBackPaginate: false,
        canForwardPaginate: false,
        timelineLoading: true
      });
      prom.then(onLoaded, onError);
    }
  },
  // handle the completion of a timeline load or localEchoUpdate, by
  // reloading the events from the timelinewindow and pending event list into
  // the state.
  _reloadEvents: function () {
    // we might have switched rooms since the load started - just bin
    // the results if so.
    if (this.unmounted) return;
    this.setState(this._getEvents());
  },
  // get the list of events from the timeline window and the pending event list
  _getEvents: function () {
    const events = this._timelineWindow.getEvents();

    const firstVisibleEventIndex = this._checkForPreJoinUISI(events); // Hold onto the live events separately. The read receipt and read marker
    // should use this list, so that they don't advance into pending events.


    const liveEvents = [...events]; // if we're at the end of the live timeline, append the pending events

    if (!this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS)) {
      events.push(...this.props.timelineSet.getPendingEvents());
    }

    return {
      events,
      liveEvents,
      firstVisibleEventIndex
    };
  },

  /**
   * Check for undecryptable messages that were sent while the user was not in
   * the room.
   *
   * @param {Array<MatrixEvent>} events The timeline events to check
   *
   * @return {Number} The index within `events` of the event after the most recent
   * undecryptable event that was sent while the user was not in the room.  If no
   * such events were found, then it returns 0.
   */
  _checkForPreJoinUISI: function (events) {
    const room = this.props.timelineSet.room;

    if (events.length === 0 || !room || !_MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(room.roomId)) {
      return 0;
    }

    const userId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId; // get the user's membership at the last event by getting the timeline
    // that the event belongs to, and traversing the timeline looking for
    // that event, while keeping track of the user's membership


    let i;
    let userMembership = "leave";

    for (i = events.length - 1; i >= 0; i--) {
      const timeline = room.getTimelineForEvent(events[i].getId());

      if (!timeline) {
        // Somehow, it seems to be possible for live events to not have
        // a timeline, even though that should not happen. :(
        // https://github.com/vector-im/riot-web/issues/12120
        console.warn("Event ".concat(events[i].getId(), " in room ").concat(room.roomId, " is live, ") + "but it does not have a timeline");
        continue;
      }

      const userMembershipEvent = timeline.getState(Matrix.EventTimeline.FORWARDS).getMember(userId);
      userMembership = userMembershipEvent ? userMembershipEvent.membership : "leave";
      const timelineEvents = timeline.getEvents();

      for (let j = timelineEvents.length - 1; j >= 0; j--) {
        const event = timelineEvents[j];

        if (event.getId() === events[i].getId()) {
          break;
        } else if (event.getStateKey() === userId && event.getType() === "m.room.member") {
          const prevContent = event.getPrevContent();
          userMembership = prevContent.membership || "leave";
        }
      }

      break;
    } // now go through the rest of the events and find the first undecryptable
    // one that was sent when the user wasn't in the room


    for (; i >= 0; i--) {
      const event = events[i];

      if (event.getStateKey() === userId && event.getType() === "m.room.member") {
        const prevContent = event.getPrevContent();
        userMembership = prevContent.membership || "leave";
      } else if (userMembership === "leave" && (event.isDecryptionFailure() || event.isBeingDecrypted())) {
        // reached an undecryptable message when the user wasn't in
        // the room -- don't try to load any more
        // Note: for now, we assume that events that are being decrypted are
        // not decryptable
        return i + 1;
      }
    }

    return 0;
  },
  _indexForEventId: function (evId) {
    for (let i = 0; i < this.state.events.length; ++i) {
      if (evId == this.state.events[i].getId()) {
        return i;
      }
    }

    return null;
  },
  _getLastDisplayedEventIndex: function (opts) {
    opts = opts || {};
    const ignoreOwn = opts.ignoreOwn || false;
    const allowPartial = opts.allowPartial || false;
    const messagePanel = this._messagePanel.current;
    if (!messagePanel) return null;

    const messagePanelNode = _reactDom.default.findDOMNode(messagePanel);

    if (!messagePanelNode) return null; // sometimes this happens for fresh rooms/post-sync

    const wrapperRect = messagePanelNode.getBoundingClientRect();

    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId;

    const isNodeInView = node => {
      if (node) {
        const boundingRect = node.getBoundingClientRect();

        if (allowPartial && boundingRect.top < wrapperRect.bottom || !allowPartial && boundingRect.bottom < wrapperRect.bottom) {
          return true;
        }
      }

      return false;
    }; // We keep track of how many of the adjacent events didn't have a tile
    // but should have the read receipt moved past them, so
    // we can include those once we find the last displayed (visible) event.
    // The counter is not started for events we don't want
    // to send a read receipt for (our own events, local echos).


    let adjacentInvisibleEventCount = 0; // Use `liveEvents` here because we don't want the read marker or read
    // receipt to advance into pending events.

    for (let i = this.state.liveEvents.length - 1; i >= 0; --i) {
      const ev = this.state.liveEvents[i];
      const node = messagePanel.getNodeForEventId(ev.getId());
      const isInView = isNodeInView(node); // when we've reached the first visible event, and the previous
      // events were all invisible (with the first one not being ignored),
      // return the index of the first invisible event.

      if (isInView && adjacentInvisibleEventCount !== 0) {
        return i + adjacentInvisibleEventCount;
      }

      if (node && !isInView) {
        // has node but not in view, so reset adjacent invisible events
        adjacentInvisibleEventCount = 0;
      }

      const shouldIgnore = !!ev.status || // local echo
      ignoreOwn && ev.sender && ev.sender.userId == myUserId; // own message

      const isWithoutTile = !(0, _EventTile.haveTileForEvent)(ev) || (0, _shouldHideEvent.default)(ev);

      if (isWithoutTile || !node) {
        // don't start counting if the event should be ignored,
        // but continue counting if we were already so the offset
        // to the previous invisble event that didn't need to be ignored
        // doesn't get messed up
        if (!shouldIgnore || shouldIgnore && adjacentInvisibleEventCount !== 0) {
          ++adjacentInvisibleEventCount;
        }

        continue;
      }

      if (shouldIgnore) {
        continue;
      }

      if (isInView) {
        return i;
      }
    }

    return null;
  },

  /**
   * Get the id of the event corresponding to our user's latest read-receipt.
   *
   * @param {Boolean} ignoreSynthesized If true, return only receipts that
   *                                    have been sent by the server, not
   *                                    implicit ones generated by the JS
   *                                    SDK.
   * @return {String} the event ID
   */
  _getCurrentReadReceipt: function (ignoreSynthesized) {
    const client = _MatrixClientPeg.MatrixClientPeg.get(); // the client can be null on logout


    if (client == null) {
      return null;
    }

    const myUserId = client.credentials.userId;
    return this.props.timelineSet.room.getEventReadUpTo(myUserId, ignoreSynthesized);
  },
  _setReadMarker: function (eventId, eventTs, inhibitSetState) {
    const roomId = this.props.timelineSet.room.roomId; // don't update the state (and cause a re-render) if there is
    // no change to the RM.

    if (eventId === this.state.readMarkerEventId) {
      return;
    } // in order to later figure out if the read marker is
    // above or below the visible timeline, we stash the timestamp.


    TimelinePanel.roomReadMarkerTsMap[roomId] = eventTs;

    if (inhibitSetState) {
      return;
    } // Do the local echo of the RM
    // run the render cycle before calling the callback, so that
    // getReadMarkerPosition() returns the right thing.


    this.setState({
      readMarkerEventId: eventId
    }, this.props.onReadMarkerUpdated);
  },
  _shouldPaginate: function () {
    // don't try to paginate while events in the timeline are
    // still being decrypted. We don't render events while they're
    // being decrypted, so they don't take up space in the timeline.
    // This means we can pull quite a lot of events into the timeline
    // and end up trying to render a lot of events.
    return !this.state.events.some(e => {
      return e.isBeingDecrypted();
    });
  },

  getRelationsForEvent(...args) {
    return this.props.timelineSet.getRelationsForEvent(...args);
  },

  render: function () {
    const MessagePanel = sdk.getComponent("structures.MessagePanel");
    const Loader = sdk.getComponent("elements.Spinner"); // just show a spinner while the timeline loads.
    //
    // put it in a div of the right class (mx_RoomView_messagePanel) so
    // that the order in the roomview flexbox is correct, and
    // mx_RoomView_messageListWrapper to position the inner div in the
    // right place.
    //
    // Note that the click-on-search-result functionality relies on the
    // fact that the messagePanel is hidden while the timeline reloads,
    // but that the RoomHeader (complete with search term) continues to
    // exist.

    if (this.state.timelineLoading) {
      return _react.default.createElement("div", {
        className: "mx_RoomView_messagePanelSpinner"
      }, _react.default.createElement(Loader, null));
    }

    if (this.state.events.length == 0 && !this.state.canBackPaginate && this.props.empty) {
      return _react.default.createElement("div", {
        className: this.props.className + " mx_RoomView_messageListWrapper"
      }, _react.default.createElement("div", {
        className: "mx_RoomView_empty"
      }, this.props.empty));
    } // give the messagepanel a stickybottom if we're at the end of the
    // live timeline, so that the arrival of new events triggers a
    // scroll.
    //
    // Make sure that stickyBottom is *false* if we can paginate
    // forwards, otherwise if somebody hits the bottom of the loaded
    // events when viewing historical messages, we get stuck in a loop
    // of paginating our way through the entire history of the room.


    const stickyBottom = !this._timelineWindow.canPaginate(Matrix.EventTimeline.FORWARDS); // If the state is PREPARED or CATCHUP, we're still waiting for the js-sdk to sync with
    // the HS and fetch the latest events, so we are effectively forward paginating.

    const forwardPaginating = this.state.forwardPaginating || ['PREPARED', 'CATCHUP'].includes(this.state.clientSyncState);
    const events = this.state.firstVisibleEventIndex ? this.state.events.slice(this.state.firstVisibleEventIndex) : this.state.events;
    return _react.default.createElement(MessagePanel, {
      ref: this._messagePanel,
      room: this.props.timelineSet.room,
      permalinkCreator: this.props.permalinkCreator,
      hidden: this.props.hidden,
      backPaginating: this.state.backPaginating,
      forwardPaginating: forwardPaginating,
      events: events,
      highlightedEventId: this.props.highlightedEventId,
      readMarkerEventId: this.state.readMarkerEventId,
      readMarkerVisible: this.state.readMarkerVisible,
      suppressFirstDateSeparator: this.state.canBackPaginate,
      showUrlPreview: this.props.showUrlPreview,
      showReadReceipts: this.props.showReadReceipts,
      ourUserId: _MatrixClientPeg.MatrixClientPeg.get().credentials.userId,
      stickyBottom: stickyBottom,
      onScroll: this.onMessageListScroll,
      onFillRequest: this.onMessageListFillRequest,
      onUnfillRequest: this.onMessageListUnfillRequest,
      isTwelveHour: this.state.isTwelveHour,
      alwaysShowTimestamps: this.state.alwaysShowTimestamps,
      className: this.props.className,
      tileShape: this.props.tileShape,
      resizeNotifier: this.props.resizeNotifier,
      getRelationsForEvent: this.getRelationsForEvent,
      editState: this.state.editState,
      showReactions: this.props.showReactions
    });
  }
});
var _default = TimelinePanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvVGltZWxpbmVQYW5lbC5qcyJdLCJuYW1lcyI6WyJQQUdJTkFURV9TSVpFIiwiSU5JVElBTF9TSVpFIiwiUkVBRF9SRUNFSVBUX0lOVEVSVkFMX01TIiwiREVCVUciLCJkZWJ1Z2xvZyIsImNvbnNvbGUiLCJsb2ciLCJiaW5kIiwiVGltZWxpbmVQYW5lbCIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwidGltZWxpbmVTZXQiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic2hvd1JlYWRSZWNlaXB0cyIsImJvb2wiLCJtYW5hZ2VSZWFkUmVjZWlwdHMiLCJtYW5hZ2VSZWFkTWFya2VycyIsImhpZGRlbiIsImhpZ2hsaWdodGVkRXZlbnRJZCIsInN0cmluZyIsImV2ZW50SWQiLCJldmVudFBpeGVsT2Zmc2V0IiwibnVtYmVyIiwic2hvd1VybFByZXZpZXciLCJvblNjcm9sbCIsImZ1bmMiLCJvblJlYWRNYXJrZXJVcGRhdGVkIiwib25QYWdpbmF0aW9uUmVxdWVzdCIsInRpbWVsaW5lQ2FwIiwiY2xhc3NOYW1lIiwidGlsZVNoYXBlIiwiZW1wdHkiLCJzaG93UmVhY3Rpb25zIiwic3RhdGljcyIsInJvb21SZWFkTWFya2VyVHNNYXAiLCJnZXREZWZhdWx0UHJvcHMiLCJOdW1iZXIiLCJNQVhfVkFMVUUiLCJnZXRJbml0aWFsU3RhdGUiLCJpbml0aWFsUmVhZE1hcmtlciIsInByb3BzIiwicmVhZG1hcmtlciIsInJvb20iLCJnZXRBY2NvdW50RGF0YSIsImdldENvbnRlbnQiLCJldmVudF9pZCIsIl9nZXRDdXJyZW50UmVhZFJlY2VpcHQiLCJldmVudHMiLCJsaXZlRXZlbnRzIiwidGltZWxpbmVMb2FkaW5nIiwiZmlyc3RWaXNpYmxlRXZlbnRJbmRleCIsImNhbkJhY2tQYWdpbmF0ZSIsImNhbkZvcndhcmRQYWdpbmF0ZSIsInJlYWRNYXJrZXJWaXNpYmxlIiwicmVhZE1hcmtlckV2ZW50SWQiLCJiYWNrUGFnaW5hdGluZyIsImZvcndhcmRQYWdpbmF0aW5nIiwiY2xpZW50U3luY1N0YXRlIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0U3luY1N0YXRlIiwiaXNUd2VsdmVIb3VyIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwiYWx3YXlzU2hvd1RpbWVzdGFtcHMiLCJyZWFkTWFya2VySW5WaWV3VGhyZXNob2xkTXMiLCJyZWFkTWFya2VyT3V0T2ZWaWV3VGhyZXNob2xkTXMiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwibGFzdFJSU2VudEV2ZW50SWQiLCJ1bmRlZmluZWQiLCJsYXN0Uk1TZW50RXZlbnRJZCIsIl9tZXNzYWdlUGFuZWwiLCJ1cGRhdGVSZWFkUmVjZWlwdE9uVXNlckFjdGl2aXR5IiwidXBkYXRlUmVhZE1hcmtlck9uVXNlckFjdGl2aXR5IiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJvbiIsIm9uUm9vbVRpbWVsaW5lIiwib25Sb29tVGltZWxpbmVSZXNldCIsIm9uUm9vbVJlZGFjdGlvbiIsIm9uUm9vbVJlY2VpcHQiLCJvbkxvY2FsRWNob1VwZGF0ZWQiLCJvbkFjY291bnREYXRhIiwib25FdmVudERlY3J5cHRlZCIsIm9uRXZlbnRSZXBsYWNlZCIsIm9uU3luYyIsIl9pbml0VGltZWxpbmUiLCJVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyIsIm5ld1Byb3BzIiwid2FybiIsInNob3VsZENvbXBvbmVudFVwZGF0ZSIsIm5leHRQcm9wcyIsIm5leHRTdGF0ZSIsIk9iamVjdFV0aWxzIiwic2hhbGxvd0VxdWFsIiwiZ3JvdXAiLCJncm91cEVuZCIsInN0YXRlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bm1vdW50ZWQiLCJfcmVhZFJlY2VpcHRBY3Rpdml0eVRpbWVyIiwiYWJvcnQiLCJfcmVhZE1hcmtlckFjdGl2aXR5VGltZXIiLCJ1bnJlZ2lzdGVyIiwiY2xpZW50IiwicmVtb3ZlTGlzdGVuZXIiLCJvbk1lc3NhZ2VMaXN0VW5maWxsUmVxdWVzdCIsImJhY2t3YXJkcyIsInNjcm9sbFRva2VuIiwiZGlyIiwiRXZlbnRUaW1lbGluZSIsIkJBQ0tXQVJEUyIsIkZPUldBUkRTIiwibWFya2VyIiwiZmluZEluZGV4IiwiZXYiLCJnZXRJZCIsImNvdW50IiwibGVuZ3RoIiwiX3RpbWVsaW5lV2luZG93IiwidW5wYWdpbmF0ZSIsImNhblBhZ2luYXRlS2V5IiwiX2dldEV2ZW50cyIsInNldFN0YXRlIiwidGltZWxpbmVXaW5kb3ciLCJkaXJlY3Rpb24iLCJzaXplIiwicGFnaW5hdGUiLCJvbk1lc3NhZ2VMaXN0RmlsbFJlcXVlc3QiLCJfc2hvdWxkUGFnaW5hdGUiLCJQcm9taXNlIiwicmVzb2x2ZSIsInBhZ2luYXRpbmdLZXkiLCJjYW5QYWdpbmF0ZSIsInRoZW4iLCJyIiwibmV3U3RhdGUiLCJvdGhlckRpcmVjdGlvbiIsImNhblBhZ2luYXRlT3RoZXJXYXlLZXkiLCJvbk1lc3NhZ2VMaXN0U2Nyb2xsIiwiZSIsInJtUG9zaXRpb24iLCJnZXRSZWFkTWFya2VyUG9zaXRpb24iLCJ0aW1lb3V0IiwiX3JlYWRNYXJrZXJUaW1lb3V0IiwiY2hhbmdlVGltZW91dCIsInBheWxvYWQiLCJhY3Rpb24iLCJmb3JjZVVwZGF0ZSIsImVkaXRTdGF0ZSIsImV2ZW50IiwiRWRpdG9yU3RhdGVUcmFuc2ZlciIsImN1cnJlbnQiLCJzY3JvbGxUb0V2ZW50SWZOZWVkZWQiLCJ0b1N0YXJ0T2ZUaW1lbGluZSIsInJlbW92ZWQiLCJkYXRhIiwidGltZWxpbmUiLCJnZXRUaW1lbGluZVNldCIsImxpdmVFdmVudCIsImdldFNjcm9sbFN0YXRlIiwic3R1Y2tBdEJvdHRvbSIsImxhc3RMaXZlRXZlbnQiLCJ1cGRhdGVkU3RhdGUiLCJjYWxsUk1VcGRhdGVkIiwibXlVc2VySWQiLCJjcmVkZW50aWFscyIsInVzZXJJZCIsInNlbmRlciIsIlVzZXJBY3Rpdml0eSIsInNoYXJlZEluc3RhbmNlIiwidXNlckFjdGl2ZVJlY2VudGx5IiwiX3NldFJlYWRNYXJrZXIiLCJnZXRUcyIsInVwZGF0ZVRpbWVsaW5lTWluSGVpZ2h0IiwiaXNBdEJvdHRvbSIsIl9sb2FkVGltZWxpbmUiLCJjYW5SZXNldFRpbWVsaW5lIiwicmVwbGFjZWRFdmVudCIsIm9sZEV2ZW50SWQiLCJfcmVsb2FkRXZlbnRzIiwiZ2V0VHlwZSIsImdldFJvb21JZCIsInJvb21JZCIsInByZXZTdGF0ZSIsInJlYWRNYXJrZXJQb3NpdGlvbiIsImluaXRpYWxUaW1lb3V0IiwiVGltZXIiLCJ0aW1lV2hpbGVBY3RpdmVSZWNlbnRseSIsImZpbmlzaGVkIiwidXBkYXRlUmVhZE1hcmtlciIsInRpbWVXaGlsZUFjdGl2ZU5vdyIsInNlbmRSZWFkUmVjZWlwdCIsImNsaSIsImlzR3Vlc3QiLCJzaG91bGRTZW5kUlIiLCJjdXJyZW50UlJFdmVudElkIiwiY3VycmVudFJSRXZlbnRJbmRleCIsIl9pbmRleEZvckV2ZW50SWQiLCJsYXN0UmVhZEV2ZW50SW5kZXgiLCJfZ2V0TGFzdERpc3BsYXllZEV2ZW50SW5kZXgiLCJpZ25vcmVPd24iLCJsYXN0UmVhZEV2ZW50Iiwic2hvdWxkU2VuZFJNIiwiaGlkZGVuUlIiLCJzZXRSb29tUmVhZE1hcmtlcnMiLCJjYXRjaCIsImVycmNvZGUiLCJlcnJvciIsImlzQXRFbmRPZkxpdmVUaW1lbGluZSIsInNldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50IiwiZGlzcGF0Y2giLCJsYXN0RGlzcGxheWVkSW5kZXgiLCJhbGxvd1BhcnRpYWwiLCJsYXN0RGlzcGxheWVkRXZlbnQiLCJfYWR2YW5jZVJlYWRNYXJrZXJQYXN0TXlFdmVudHMiLCJnZXRFdmVudHMiLCJpIiwianVtcFRvTGl2ZVRpbWVsaW5lIiwic2Nyb2xsVG9Cb3R0b20iLCJqdW1wVG9SZWFkTWFya2VyIiwicmV0Iiwic2Nyb2xsVG9FdmVudCIsImZvcmdldFJlYWRNYXJrZXIiLCJybUlkIiwidGwiLCJnZXRUaW1lbGluZUZvckV2ZW50Iiwicm1UcyIsImZpbmQiLCJjYW5KdW1wVG9SZWFkTWFya2VyIiwicG9zIiwiaGFuZGxlU2Nyb2xsS2V5IiwiY3RybEtleSIsInNoaWZ0S2V5IiwiYWx0S2V5IiwibWV0YUtleSIsImtleSIsIktleSIsIkVORCIsImluaXRpYWxFdmVudCIsInBpeGVsT2Zmc2V0Iiwib2Zmc2V0QmFzZSIsIk1hdHJpeCIsIlRpbWVsaW5lV2luZG93Iiwid2luZG93TGltaXQiLCJvbkxvYWRlZCIsIm9uVGltZWxpbmVSZXNldCIsIm9uRXJyb3IiLCJFcnJvckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIm9uRmluaXNoZWQiLCJyb29tX2lkIiwibWVzc2FnZSIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJsb2FkIiwicHJvbSIsIl9jaGVja0ZvclByZUpvaW5VSVNJIiwicHVzaCIsImdldFBlbmRpbmdFdmVudHMiLCJpc1Jvb21FbmNyeXB0ZWQiLCJ1c2VyTWVtYmVyc2hpcCIsInVzZXJNZW1iZXJzaGlwRXZlbnQiLCJnZXRTdGF0ZSIsImdldE1lbWJlciIsIm1lbWJlcnNoaXAiLCJ0aW1lbGluZUV2ZW50cyIsImoiLCJnZXRTdGF0ZUtleSIsInByZXZDb250ZW50IiwiZ2V0UHJldkNvbnRlbnQiLCJpc0RlY3J5cHRpb25GYWlsdXJlIiwiaXNCZWluZ0RlY3J5cHRlZCIsImV2SWQiLCJvcHRzIiwibWVzc2FnZVBhbmVsIiwibWVzc2FnZVBhbmVsTm9kZSIsIlJlYWN0RE9NIiwiZmluZERPTU5vZGUiLCJ3cmFwcGVyUmVjdCIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsImlzTm9kZUluVmlldyIsIm5vZGUiLCJib3VuZGluZ1JlY3QiLCJ0b3AiLCJib3R0b20iLCJhZGphY2VudEludmlzaWJsZUV2ZW50Q291bnQiLCJnZXROb2RlRm9yRXZlbnRJZCIsImlzSW5WaWV3Iiwic2hvdWxkSWdub3JlIiwic3RhdHVzIiwiaXNXaXRob3V0VGlsZSIsImlnbm9yZVN5bnRoZXNpemVkIiwiZ2V0RXZlbnRSZWFkVXBUbyIsImV2ZW50VHMiLCJpbmhpYml0U2V0U3RhdGUiLCJzb21lIiwiZ2V0UmVsYXRpb25zRm9yRXZlbnQiLCJhcmdzIiwicmVuZGVyIiwiTWVzc2FnZVBhbmVsIiwiTG9hZGVyIiwic3RpY2t5Qm90dG9tIiwiaW5jbHVkZXMiLCJzbGljZSIsInBlcm1hbGlua0NyZWF0b3IiLCJyZXNpemVOb3RpZmllciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1Q0EsTUFBTUEsYUFBYSxHQUFHLEVBQXRCO0FBQ0EsTUFBTUMsWUFBWSxHQUFHLEVBQXJCO0FBQ0EsTUFBTUMsd0JBQXdCLEdBQUcsR0FBakM7QUFFQSxNQUFNQyxLQUFLLEdBQUcsS0FBZDs7QUFFQSxJQUFJQyxRQUFRLEdBQUcsWUFBVyxDQUFFLENBQTVCOztBQUNBLElBQUlELEtBQUosRUFBVztBQUNQO0FBQ0FDLEVBQUFBLFFBQVEsR0FBR0MsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBQVosQ0FBaUJGLE9BQWpCLENBQVg7QUFDSDtBQUVEOzs7Ozs7O0FBS0EsTUFBTUcsYUFBYSxHQUFHLCtCQUFpQjtBQUNuQ0MsRUFBQUEsV0FBVyxFQUFFLGVBRHNCO0FBR25DQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxJQUFBQSxXQUFXLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQUx2QjtBQU9QQyxJQUFBQSxnQkFBZ0IsRUFBRUgsbUJBQVVJLElBUHJCO0FBUVA7QUFDQUMsSUFBQUEsa0JBQWtCLEVBQUVMLG1CQUFVSSxJQVR2QjtBQVVQRSxJQUFBQSxpQkFBaUIsRUFBRU4sbUJBQVVJLElBVnRCO0FBWVA7QUFDQUcsSUFBQUEsTUFBTSxFQUFFUCxtQkFBVUksSUFiWDtBQWVQO0FBQ0E7QUFDQUksSUFBQUEsa0JBQWtCLEVBQUVSLG1CQUFVUyxNQWpCdkI7QUFtQlA7QUFDQTtBQUNBQyxJQUFBQSxPQUFPLEVBQUVWLG1CQUFVUyxNQXJCWjtBQXVCUDtBQUNBO0FBQ0E7QUFDQUUsSUFBQUEsZ0JBQWdCLEVBQUVYLG1CQUFVWSxNQTFCckI7QUE0QlA7QUFDQUMsSUFBQUEsY0FBYyxFQUFFYixtQkFBVUksSUE3Qm5CO0FBK0JQO0FBQ0FVLElBQUFBLFFBQVEsRUFBRWQsbUJBQVVlLElBaENiO0FBa0NQO0FBQ0FDLElBQUFBLG1CQUFtQixFQUFFaEIsbUJBQVVlLElBbkN4QjtBQXFDUDtBQUNBO0FBQ0FFLElBQUFBLG1CQUFtQixFQUFFakIsbUJBQVVlLElBdkN4QjtBQXlDUDtBQUNBRyxJQUFBQSxXQUFXLEVBQUVsQixtQkFBVVksTUExQ2hCO0FBNENQO0FBQ0FPLElBQUFBLFNBQVMsRUFBRW5CLG1CQUFVUyxNQTdDZDtBQStDUDtBQUNBVyxJQUFBQSxTQUFTLEVBQUVwQixtQkFBVVMsTUFoRGQ7QUFrRFA7QUFDQVksSUFBQUEsS0FBSyxFQUFFckIsbUJBQVVTLE1BbkRWO0FBcURQO0FBQ0FhLElBQUFBLGFBQWEsRUFBRXRCLG1CQUFVSTtBQXREbEIsR0FId0I7QUE0RG5DbUIsRUFBQUEsT0FBTyxFQUFFO0FBQ0w7QUFDQUMsSUFBQUEsbUJBQW1CLEVBQUU7QUFGaEIsR0E1RDBCO0FBaUVuQ0MsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIO0FBQ0E7QUFDQVAsTUFBQUEsV0FBVyxFQUFFUSxNQUFNLENBQUNDLFNBSGpCO0FBSUhSLE1BQUFBLFNBQVMsRUFBRTtBQUpSLEtBQVA7QUFNSCxHQXhFa0M7QUEwRW5DUyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QjtBQUNBO0FBQ0EsUUFBSUMsaUJBQWlCLEdBQUcsSUFBeEI7O0FBQ0EsUUFBSSxLQUFLQyxLQUFMLENBQVd4QixpQkFBZixFQUFrQztBQUM5QixZQUFNeUIsVUFBVSxHQUFHLEtBQUtELEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUF2QixDQUE0QkMsY0FBNUIsQ0FBMkMsY0FBM0MsQ0FBbkI7O0FBQ0EsVUFBSUYsVUFBSixFQUFnQjtBQUNaRixRQUFBQSxpQkFBaUIsR0FBR0UsVUFBVSxDQUFDRyxVQUFYLEdBQXdCQyxRQUE1QztBQUNILE9BRkQsTUFFTztBQUNITixRQUFBQSxpQkFBaUIsR0FBRyxLQUFLTyxzQkFBTCxFQUFwQjtBQUNIO0FBQ0o7O0FBRUQsV0FBTztBQUNIQyxNQUFBQSxNQUFNLEVBQUUsRUFETDtBQUVIQyxNQUFBQSxVQUFVLEVBQUUsRUFGVDtBQUdIQyxNQUFBQSxlQUFlLEVBQUUsSUFIZDtBQUdvQjtBQUV2QjtBQUNBQyxNQUFBQSxzQkFBc0IsRUFBRSxDQU5yQjtBQVFIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLE1BQUFBLGVBQWUsRUFBRSxLQWxCZDtBQW9CSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxNQUFBQSxrQkFBa0IsRUFBRSxLQWpDakI7QUFtQ0g7QUFDQTtBQUNBQyxNQUFBQSxpQkFBaUIsRUFBRSxJQXJDaEI7QUF1Q0hDLE1BQUFBLGlCQUFpQixFQUFFZixpQkF2Q2hCO0FBeUNIZ0IsTUFBQUEsY0FBYyxFQUFFLEtBekNiO0FBMENIQyxNQUFBQSxpQkFBaUIsRUFBRSxLQTFDaEI7QUE0Q0g7QUFDQUMsTUFBQUEsZUFBZSxFQUFFQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxZQUF0QixFQTdDZDtBQStDSDtBQUNBQyxNQUFBQSxZQUFZLEVBQUVDLHVCQUFjQyxRQUFkLENBQXVCLDBCQUF2QixDQWhEWDtBQWtESDtBQUNBQyxNQUFBQSxvQkFBb0IsRUFBRUYsdUJBQWNDLFFBQWQsQ0FBdUIsc0JBQXZCLENBbkRuQjtBQXFESDtBQUNBRSxNQUFBQSwyQkFBMkIsRUFBRUgsdUJBQWNDLFFBQWQsQ0FBdUIsNkJBQXZCLENBdEQxQjtBQXdESDtBQUNBRyxNQUFBQSw4QkFBOEIsRUFBRUosdUJBQWNDLFFBQWQsQ0FBdUIsZ0NBQXZCO0FBekQ3QixLQUFQO0FBMkRILEdBbEprQztBQW9KbkM7QUFDQUksRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQ2pFLElBQUFBLFFBQVEsQ0FBQyx5QkFBRCxDQUFSO0FBRUEsU0FBS2tFLGlCQUFMLEdBQXlCQyxTQUF6QjtBQUNBLFNBQUtDLGlCQUFMLEdBQXlCRCxTQUF6QjtBQUVBLFNBQUtFLGFBQUwsR0FBcUIsdUJBQXJCOztBQUVBLFFBQUksS0FBSy9CLEtBQUwsQ0FBV3pCLGtCQUFmLEVBQW1DO0FBQy9CLFdBQUt5RCwrQkFBTDtBQUNIOztBQUNELFFBQUksS0FBS2hDLEtBQUwsQ0FBV3hCLGlCQUFmLEVBQWtDO0FBQzlCLFdBQUt5RCw4QkFBTDtBQUNIOztBQUdELFNBQUtDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS0MsUUFBbEIsQ0FBckI7O0FBQ0FuQixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsRUFBdEIsQ0FBeUIsZUFBekIsRUFBMEMsS0FBS0MsY0FBL0M7O0FBQ0FyQixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsRUFBdEIsQ0FBeUIsb0JBQXpCLEVBQStDLEtBQUtFLG1CQUFwRDs7QUFDQXRCLHFDQUFnQkMsR0FBaEIsR0FBc0JtQixFQUF0QixDQUF5QixnQkFBekIsRUFBMkMsS0FBS0csZUFBaEQsRUFuQmtDLENBb0JsQzs7O0FBQ0F2QixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsRUFBdEIsQ0FBeUIseUJBQXpCLEVBQW9ELEtBQUtHLGVBQXpEOztBQUNBdkIscUNBQWdCQyxHQUFoQixHQUFzQm1CLEVBQXRCLENBQXlCLGNBQXpCLEVBQXlDLEtBQUtJLGFBQTlDOztBQUNBeEIscUNBQWdCQyxHQUFoQixHQUFzQm1CLEVBQXRCLENBQXlCLHVCQUF6QixFQUFrRCxLQUFLSyxrQkFBdkQ7O0FBQ0F6QixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsRUFBdEIsQ0FBeUIsa0JBQXpCLEVBQTZDLEtBQUtNLGFBQWxEOztBQUNBMUIscUNBQWdCQyxHQUFoQixHQUFzQm1CLEVBQXRCLENBQXlCLGlCQUF6QixFQUE0QyxLQUFLTyxnQkFBakQ7O0FBQ0EzQixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsRUFBdEIsQ0FBeUIsZ0JBQXpCLEVBQTJDLEtBQUtRLGVBQWhEOztBQUNBNUIscUNBQWdCQyxHQUFoQixHQUFzQm1CLEVBQXRCLENBQXlCLE1BQXpCLEVBQWlDLEtBQUtTLE1BQXRDOztBQUVBLFNBQUtDLGFBQUwsQ0FBbUIsS0FBS2hELEtBQXhCO0FBQ0gsR0FuTGtDO0FBcUxuQztBQUNBaUQsRUFBQUEsZ0NBQWdDLEVBQUUsVUFBU0MsUUFBVCxFQUFtQjtBQUNqRCxRQUFJQSxRQUFRLENBQUNqRixXQUFULEtBQXlCLEtBQUsrQixLQUFMLENBQVcvQixXQUF4QyxFQUFxRDtBQUNqRDtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBTixNQUFBQSxPQUFPLENBQUN3RixJQUFSLENBQWEsZ0VBQWI7QUFDSDs7QUFFRCxRQUFJRCxRQUFRLENBQUN0RSxPQUFULElBQW9CLEtBQUtvQixLQUFMLENBQVdwQixPQUFuQyxFQUE0QztBQUN4Q2pCLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHdDQUF3Q3NGLFFBQVEsQ0FBQ3RFLE9BQWpELEdBQ0EsUUFEQSxHQUNXLEtBQUtvQixLQUFMLENBQVdwQixPQUR0QixHQUNnQyxHQUQ1QztBQUVBLGFBQU8sS0FBS29FLGFBQUwsQ0FBbUJFLFFBQW5CLENBQVA7QUFDSDtBQUNKLEdBM01rQztBQTZNbkNFLEVBQUFBLHFCQUFxQixFQUFFLFVBQVNDLFNBQVQsRUFBb0JDLFNBQXBCLEVBQStCO0FBQ2xELFFBQUksQ0FBQ0MsV0FBVyxDQUFDQyxZQUFaLENBQXlCLEtBQUt4RCxLQUE5QixFQUFxQ3FELFNBQXJDLENBQUwsRUFBc0Q7QUFDbEQsVUFBSTVGLEtBQUosRUFBVztBQUNQRSxRQUFBQSxPQUFPLENBQUM4RixLQUFSLENBQWMsOENBQWQ7QUFDQTlGLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGVBQVosRUFBNkIsS0FBS29DLEtBQWxDO0FBQ0FyQyxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxjQUFaLEVBQTRCeUYsU0FBNUI7QUFDQTFGLFFBQUFBLE9BQU8sQ0FBQytGLFFBQVI7QUFDSDs7QUFDRCxhQUFPLElBQVA7QUFDSDs7QUFFRCxRQUFJLENBQUNILFdBQVcsQ0FBQ0MsWUFBWixDQUF5QixLQUFLRyxLQUE5QixFQUFxQ0wsU0FBckMsQ0FBTCxFQUFzRDtBQUNsRCxVQUFJN0YsS0FBSixFQUFXO0FBQ1BFLFFBQUFBLE9BQU8sQ0FBQzhGLEtBQVIsQ0FBYyw4Q0FBZDtBQUNBOUYsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksZUFBWixFQUE2QixLQUFLK0YsS0FBbEM7QUFDQWhHLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGNBQVosRUFBNEIwRixTQUE1QjtBQUNBM0YsUUFBQUEsT0FBTyxDQUFDK0YsUUFBUjtBQUNIOztBQUNELGFBQU8sSUFBUDtBQUNIOztBQUVELFdBQU8sS0FBUDtBQUNILEdBbk9rQztBQXFPbkNFLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFLQyxTQUFMLEdBQWlCLElBQWpCOztBQUNBLFFBQUksS0FBS0MseUJBQVQsRUFBb0M7QUFDaEMsV0FBS0EseUJBQUwsQ0FBK0JDLEtBQS9COztBQUNBLFdBQUtELHlCQUFMLEdBQWlDLElBQWpDO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLRSx3QkFBVCxFQUFtQztBQUMvQixXQUFLQSx3QkFBTCxDQUE4QkQsS0FBOUI7O0FBQ0EsV0FBS0Msd0JBQUwsR0FBZ0MsSUFBaEM7QUFDSDs7QUFFRDdCLHdCQUFJOEIsVUFBSixDQUFlLEtBQUsvQixhQUFwQjs7QUFFQSxVQUFNZ0MsTUFBTSxHQUFHaEQsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFFBQUkrQyxNQUFKLEVBQVk7QUFDUkEsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLGVBQXRCLEVBQXVDLEtBQUs1QixjQUE1QztBQUNBMkIsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLG9CQUF0QixFQUE0QyxLQUFLM0IsbUJBQWpEO0FBQ0EwQixNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsZ0JBQXRCLEVBQXdDLEtBQUsxQixlQUE3QztBQUNBeUIsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLHlCQUF0QixFQUFpRCxLQUFLMUIsZUFBdEQ7QUFDQXlCLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixjQUF0QixFQUFzQyxLQUFLekIsYUFBM0M7QUFDQXdCLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQix1QkFBdEIsRUFBK0MsS0FBS3hCLGtCQUFwRDtBQUNBdUIsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLGtCQUF0QixFQUEwQyxLQUFLdkIsYUFBL0M7QUFDQXNCLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixpQkFBdEIsRUFBeUMsS0FBS3RCLGdCQUE5QztBQUNBcUIsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLGdCQUF0QixFQUF3QyxLQUFLckIsZUFBN0M7QUFDQW9CLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixNQUF0QixFQUE4QixLQUFLcEIsTUFBbkM7QUFDSDtBQUNKLEdBblFrQztBQXFRbkNxQixFQUFBQSwwQkFBMEIsRUFBRSxVQUFTQyxTQUFULEVBQW9CQyxXQUFwQixFQUFpQztBQUN6RDtBQUNBLFVBQU1DLEdBQUcsR0FBR0YsU0FBUyxHQUFHRyxxQkFBY0MsU0FBakIsR0FBNkJELHFCQUFjRSxRQUFoRTtBQUNBaEgsSUFBQUEsUUFBUSxDQUFDLGlEQUFELEVBQW9ENkcsR0FBcEQsQ0FBUixDQUh5RCxDQUt6RDtBQUNBOztBQUNBLFVBQU0zRixPQUFPLEdBQUcwRixXQUFoQjtBQUVBLFVBQU1LLE1BQU0sR0FBRyxLQUFLaEIsS0FBTCxDQUFXcEQsTUFBWCxDQUFrQnFFLFNBQWxCLENBQ1ZDLEVBQUQsSUFBUTtBQUNKLGFBQU9BLEVBQUUsQ0FBQ0MsS0FBSCxPQUFlbEcsT0FBdEI7QUFDSCxLQUhVLENBQWY7QUFNQSxVQUFNbUcsS0FBSyxHQUFHVixTQUFTLEdBQUdNLE1BQU0sR0FBRyxDQUFaLEdBQWdCLEtBQUtoQixLQUFMLENBQVdwRCxNQUFYLENBQWtCeUUsTUFBbEIsR0FBMkJMLE1BQWxFOztBQUVBLFFBQUlJLEtBQUssR0FBRyxDQUFaLEVBQWU7QUFDWHJILE1BQUFBLFFBQVEsQ0FBQyw2QkFBRCxFQUFnQ3FILEtBQWhDLEVBQXVDLGNBQXZDLEVBQXVEUixHQUF2RCxDQUFSOztBQUNBLFdBQUtVLGVBQUwsQ0FBcUJDLFVBQXJCLENBQWdDSCxLQUFoQyxFQUF1Q1YsU0FBdkMsRUFGVyxDQUlYOzs7QUFDQSxZQUFNYyxjQUFjLEdBQUlkLFNBQUQsR0FBYyxpQkFBZCxHQUFrQyxvQkFBekQ7O0FBQ0EsWUFBTTtBQUFFOUQsUUFBQUEsTUFBRjtBQUFVQyxRQUFBQSxVQUFWO0FBQXNCRSxRQUFBQTtBQUF0QixVQUFpRCxLQUFLMEUsVUFBTCxFQUF2RDs7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFDVixTQUFDRixjQUFELEdBQWtCLElBRFI7QUFFVjVFLFFBQUFBLE1BRlU7QUFHVkMsUUFBQUEsVUFIVTtBQUlWRSxRQUFBQTtBQUpVLE9BQWQ7QUFNSDtBQUNKLEdBcFNrQzs7QUFzU25DdkIsRUFBQUEsbUJBQW1CLENBQUNtRyxjQUFELEVBQWlCQyxTQUFqQixFQUE0QkMsSUFBNUIsRUFBa0M7QUFDakQsUUFBSSxLQUFLeEYsS0FBTCxDQUFXYixtQkFBZixFQUFvQztBQUNoQyxhQUFPLEtBQUthLEtBQUwsQ0FBV2IsbUJBQVgsQ0FBK0JtRyxjQUEvQixFQUErQ0MsU0FBL0MsRUFBMERDLElBQTFELENBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPRixjQUFjLENBQUNHLFFBQWYsQ0FBd0JGLFNBQXhCLEVBQW1DQyxJQUFuQyxDQUFQO0FBQ0g7QUFDSixHQTVTa0M7O0FBOFNuQztBQUNBRSxFQUFBQSx3QkFBd0IsRUFBRSxVQUFTckIsU0FBVCxFQUFvQjtBQUMxQyxRQUFJLENBQUMsS0FBS3NCLGVBQUwsRUFBTCxFQUE2QixPQUFPQyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsS0FBaEIsQ0FBUDtBQUU3QixVQUFNdEIsR0FBRyxHQUFHRixTQUFTLEdBQUdHLHFCQUFjQyxTQUFqQixHQUE2QkQscUJBQWNFLFFBQWhFO0FBQ0EsVUFBTVMsY0FBYyxHQUFHZCxTQUFTLEdBQUcsaUJBQUgsR0FBdUIsb0JBQXZEO0FBQ0EsVUFBTXlCLGFBQWEsR0FBR3pCLFNBQVMsR0FBRyxnQkFBSCxHQUFzQixtQkFBckQ7O0FBRUEsUUFBSSxDQUFDLEtBQUtWLEtBQUwsQ0FBV3dCLGNBQVgsQ0FBTCxFQUFpQztBQUM3QnpILE1BQUFBLFFBQVEsQ0FBQyw4QkFBRCxFQUFpQzZHLEdBQWpDLEVBQXNDLDBCQUF0QyxDQUFSO0FBQ0EsYUFBT3FCLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQixLQUFoQixDQUFQO0FBQ0g7O0FBRUQsUUFBSSxDQUFDLEtBQUtaLGVBQUwsQ0FBcUJjLFdBQXJCLENBQWlDeEIsR0FBakMsQ0FBTCxFQUE0QztBQUN4QzdHLE1BQUFBLFFBQVEsQ0FBQyxzQkFBRCxFQUF5QjZHLEdBQXpCLEVBQThCLHNCQUE5QixDQUFSO0FBQ0EsV0FBS2MsUUFBTCxDQUFjO0FBQUMsU0FBQ0YsY0FBRCxHQUFrQjtBQUFuQixPQUFkO0FBQ0EsYUFBT1MsT0FBTyxDQUFDQyxPQUFSLENBQWdCLEtBQWhCLENBQVA7QUFDSDs7QUFFRCxRQUFJeEIsU0FBUyxJQUFJLEtBQUtWLEtBQUwsQ0FBV2pELHNCQUFYLEtBQXNDLENBQXZELEVBQTBEO0FBQ3REaEQsTUFBQUEsUUFBUSxDQUFDLHNCQUFELEVBQXlCNkcsR0FBekIsRUFBOEIsbUNBQTlCLENBQVI7QUFDQSxhQUFPcUIsT0FBTyxDQUFDQyxPQUFSLENBQWdCLEtBQWhCLENBQVA7QUFDSDs7QUFFRG5JLElBQUFBLFFBQVEsQ0FBQyxtREFBaUQyRyxTQUFsRCxDQUFSO0FBQ0EsU0FBS2dCLFFBQUwsQ0FBYztBQUFDLE9BQUNTLGFBQUQsR0FBaUI7QUFBbEIsS0FBZDtBQUVBLFdBQU8sS0FBSzNHLG1CQUFMLENBQXlCLEtBQUs4RixlQUE5QixFQUErQ1YsR0FBL0MsRUFBb0RqSCxhQUFwRCxFQUFtRTBJLElBQW5FLENBQXlFQyxDQUFELElBQU87QUFDbEYsVUFBSSxLQUFLcEMsU0FBVCxFQUFvQjtBQUFFO0FBQVM7O0FBRS9CbkcsTUFBQUEsUUFBUSxDQUFDLGdEQUE4QzJHLFNBQTlDLEdBQXdELFlBQXhELEdBQXFFNEIsQ0FBdEUsQ0FBUjs7QUFFQSxZQUFNO0FBQUUxRixRQUFBQSxNQUFGO0FBQVVDLFFBQUFBLFVBQVY7QUFBc0JFLFFBQUFBO0FBQXRCLFVBQWlELEtBQUswRSxVQUFMLEVBQXZEOztBQUNBLFlBQU1jLFFBQVEsR0FBRztBQUNiLFNBQUNKLGFBQUQsR0FBaUIsS0FESjtBQUViLFNBQUNYLGNBQUQsR0FBa0JjLENBRkw7QUFHYjFGLFFBQUFBLE1BSGE7QUFJYkMsUUFBQUEsVUFKYTtBQUtiRSxRQUFBQTtBQUxhLE9BQWpCLENBTmtGLENBY2xGO0FBQ0E7O0FBQ0EsWUFBTXlGLGNBQWMsR0FBRzlCLFNBQVMsR0FBR0cscUJBQWNFLFFBQWpCLEdBQTRCRixxQkFBY0MsU0FBMUU7QUFDQSxZQUFNMkIsc0JBQXNCLEdBQUcvQixTQUFTLEdBQUcsb0JBQUgsR0FBMEIsaUJBQWxFOztBQUNBLFVBQUksQ0FBQyxLQUFLVixLQUFMLENBQVd5QyxzQkFBWCxDQUFELElBQ0ksS0FBS25CLGVBQUwsQ0FBcUJjLFdBQXJCLENBQWlDSSxjQUFqQyxDQURSLEVBQzBEO0FBQ3REekksUUFBQUEsUUFBUSxDQUFDLHdCQUFELEVBQTJCeUksY0FBM0IsRUFBMkMsZ0JBQTNDLENBQVI7QUFDQUQsUUFBQUEsUUFBUSxDQUFDRSxzQkFBRCxDQUFSLEdBQW1DLElBQW5DO0FBQ0gsT0F0QmlGLENBd0JsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxhQUFPLElBQUlSLE9BQUosQ0FBYUMsT0FBRCxJQUFhO0FBQzVCLGFBQUtSLFFBQUwsQ0FBY2EsUUFBZCxFQUF3QixNQUFNO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0FMLFVBQUFBLE9BQU8sQ0FBQ0ksQ0FBQyxLQUFLLENBQUM1QixTQUFELElBQWMzRCxzQkFBc0IsS0FBSyxDQUE5QyxDQUFGLENBQVA7QUFDSCxTQU5EO0FBT0gsT0FSTSxDQUFQO0FBU0gsS0F0Q00sQ0FBUDtBQXVDSCxHQWhYa0M7QUFrWG5DMkYsRUFBQUEsbUJBQW1CLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQzdCLFFBQUksS0FBS3RHLEtBQUwsQ0FBV2hCLFFBQWYsRUFBeUI7QUFDckIsV0FBS2dCLEtBQUwsQ0FBV2hCLFFBQVgsQ0FBb0JzSCxDQUFwQjtBQUNIOztBQUVELFFBQUksS0FBS3RHLEtBQUwsQ0FBV3hCLGlCQUFmLEVBQWtDO0FBQzlCLFlBQU0rSCxVQUFVLEdBQUcsS0FBS0MscUJBQUwsRUFBbkIsQ0FEOEIsQ0FFOUI7QUFDQTtBQUNBOztBQUNBLFVBQUlELFVBQVUsR0FBRyxDQUFqQixFQUFvQjtBQUNoQixhQUFLbEIsUUFBTCxDQUFjO0FBQUN4RSxVQUFBQSxpQkFBaUIsRUFBRTtBQUFwQixTQUFkO0FBQ0gsT0FQNkIsQ0FTOUI7QUFDQTs7O0FBQ0EsWUFBTTRGLE9BQU8sR0FBRyxLQUFLQyxrQkFBTCxDQUF3QkgsVUFBeEIsQ0FBaEIsQ0FYOEIsQ0FZOUI7OztBQUNBLFdBQUt2Qyx3QkFBTCxDQUE4QjJDLGFBQTlCLENBQTRDRixPQUE1QztBQUNIO0FBQ0osR0F0WWtDO0FBd1luQ3BFLEVBQUFBLFFBQVEsRUFBRSxVQUFTdUUsT0FBVCxFQUFrQjtBQUN4QixRQUFJQSxPQUFPLENBQUNDLE1BQVIsS0FBbUIsc0JBQXZCLEVBQStDO0FBQzNDLFdBQUtDLFdBQUw7QUFDSDs7QUFDRCxRQUFJRixPQUFPLENBQUNDLE1BQVIsS0FBbUIsWUFBdkIsRUFBcUM7QUFDakMsWUFBTUUsU0FBUyxHQUFHSCxPQUFPLENBQUNJLEtBQVIsR0FBZ0IsSUFBSUMsNEJBQUosQ0FBd0JMLE9BQU8sQ0FBQ0ksS0FBaEMsQ0FBaEIsR0FBeUQsSUFBM0U7QUFDQSxXQUFLM0IsUUFBTCxDQUFjO0FBQUMwQixRQUFBQTtBQUFELE9BQWQsRUFBMkIsTUFBTTtBQUM3QixZQUFJSCxPQUFPLENBQUNJLEtBQVIsSUFBaUIsS0FBS2pGLGFBQUwsQ0FBbUJtRixPQUF4QyxFQUFpRDtBQUM3QyxlQUFLbkYsYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCQyxxQkFBM0IsQ0FDSVAsT0FBTyxDQUFDSSxLQUFSLENBQWNsQyxLQUFkLEVBREo7QUFHSDtBQUNKLE9BTkQ7QUFPSDtBQUNKLEdBdFprQztBQXdabkN2QyxFQUFBQSxjQUFjLEVBQUUsVUFBU3NDLEVBQVQsRUFBYTNFLElBQWIsRUFBbUJrSCxpQkFBbkIsRUFBc0NDLE9BQXRDLEVBQStDQyxJQUEvQyxFQUFxRDtBQUNqRTtBQUNBLFFBQUlBLElBQUksQ0FBQ0MsUUFBTCxDQUFjQyxjQUFkLE9BQW1DLEtBQUt4SCxLQUFMLENBQVcvQixXQUFsRCxFQUErRCxPQUZFLENBSWpFO0FBQ0E7O0FBQ0EsUUFBSW1KLGlCQUFpQixJQUFJLENBQUNFLElBQXRCLElBQThCLENBQUNBLElBQUksQ0FBQ0csU0FBeEMsRUFBbUQ7QUFFbkQsUUFBSSxDQUFDLEtBQUsxRixhQUFMLENBQW1CbUYsT0FBeEIsRUFBaUM7O0FBRWpDLFFBQUksQ0FBQyxLQUFLbkYsYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCUSxjQUEzQixHQUE0Q0MsYUFBakQsRUFBZ0U7QUFDNUQ7QUFDQTtBQUNBO0FBQ0EsV0FBS3RDLFFBQUwsQ0FBYztBQUFDekUsUUFBQUEsa0JBQWtCLEVBQUU7QUFBckIsT0FBZDtBQUNBO0FBQ0gsS0FoQmdFLENBa0JqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQUtxRSxlQUFMLENBQXFCUSxRQUFyQixDQUE4QmpCLHFCQUFjRSxRQUE1QyxFQUFzRCxDQUF0RCxFQUF5RCxLQUF6RCxFQUFnRXNCLElBQWhFLENBQXFFLE1BQU07QUFDdkUsVUFBSSxLQUFLbkMsU0FBVCxFQUFvQjtBQUFFO0FBQVM7O0FBRS9CLFlBQU07QUFBRXRELFFBQUFBLE1BQUY7QUFBVUMsUUFBQUEsVUFBVjtBQUFzQkUsUUFBQUE7QUFBdEIsVUFBaUQsS0FBSzBFLFVBQUwsRUFBdkQ7O0FBQ0EsWUFBTXdDLGFBQWEsR0FBR3BILFVBQVUsQ0FBQ0EsVUFBVSxDQUFDd0UsTUFBWCxHQUFvQixDQUFyQixDQUFoQztBQUVBLFlBQU02QyxZQUFZLEdBQUc7QUFDakJ0SCxRQUFBQSxNQURpQjtBQUVqQkMsUUFBQUEsVUFGaUI7QUFHakJFLFFBQUFBO0FBSGlCLE9BQXJCO0FBTUEsVUFBSW9ILGFBQUo7O0FBQ0EsVUFBSSxLQUFLOUgsS0FBTCxDQUFXeEIsaUJBQWYsRUFBa0M7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQU11SixRQUFRLEdBQUc3RyxpQ0FBZ0JDLEdBQWhCLEdBQXNCNkcsV0FBdEIsQ0FBa0NDLE1BQW5EOztBQUNBLGNBQU1DLE1BQU0sR0FBR3JELEVBQUUsQ0FBQ3FELE1BQUgsR0FBWXJELEVBQUUsQ0FBQ3FELE1BQUgsQ0FBVUQsTUFBdEIsR0FBK0IsSUFBOUM7QUFDQUgsUUFBQUEsYUFBYSxHQUFHLEtBQWhCOztBQUNBLFlBQUlJLE1BQU0sSUFBSUgsUUFBVixJQUFzQixDQUFDSSxzQkFBYUMsY0FBYixHQUE4QkMsa0JBQTlCLEVBQTNCLEVBQStFO0FBQzNFUixVQUFBQSxZQUFZLENBQUNoSCxpQkFBYixHQUFpQyxJQUFqQztBQUNILFNBRkQsTUFFTyxJQUFJK0csYUFBYSxJQUFJLEtBQUtwQixxQkFBTCxPQUFpQyxDQUF0RCxFQUF5RDtBQUM1RDtBQUNBO0FBRUEsZUFBSzhCLGNBQUwsQ0FBb0JWLGFBQWEsQ0FBQzlDLEtBQWQsRUFBcEIsRUFBMkM4QyxhQUFhLENBQUNXLEtBQWQsRUFBM0MsRUFBa0UsSUFBbEU7O0FBQ0FWLFVBQUFBLFlBQVksQ0FBQ2hILGlCQUFiLEdBQWlDLEtBQWpDO0FBQ0FnSCxVQUFBQSxZQUFZLENBQUMvRyxpQkFBYixHQUFpQzhHLGFBQWEsQ0FBQzlDLEtBQWQsRUFBakM7QUFDQWdELFVBQUFBLGFBQWEsR0FBRyxJQUFoQjtBQUNIO0FBQ0o7O0FBRUQsV0FBS3pDLFFBQUwsQ0FBY3dDLFlBQWQsRUFBNEIsTUFBTTtBQUM5QixhQUFLOUYsYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCc0IsdUJBQTNCOztBQUNBLFlBQUlWLGFBQUosRUFBbUI7QUFDZixlQUFLOUgsS0FBTCxDQUFXZCxtQkFBWDtBQUNIO0FBQ0osT0FMRDtBQU1ILEtBNUNEO0FBNkNILEdBaGVrQztBQWtlbkNzRCxFQUFBQSxtQkFBbUIsRUFBRSxVQUFTdEMsSUFBVCxFQUFlakMsV0FBZixFQUE0QjtBQUM3QyxRQUFJQSxXQUFXLEtBQUssS0FBSytCLEtBQUwsQ0FBVy9CLFdBQS9CLEVBQTRDOztBQUU1QyxRQUFJLEtBQUs4RCxhQUFMLENBQW1CbUYsT0FBbkIsSUFBOEIsS0FBS25GLGFBQUwsQ0FBbUJtRixPQUFuQixDQUEyQnVCLFVBQTNCLEVBQWxDLEVBQTJFO0FBQ3ZFLFdBQUtDLGFBQUw7QUFDSDtBQUNKLEdBeGVrQztBQTBlbkNDLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsV0FBTyxLQUFLNUcsYUFBTCxDQUFtQm1GLE9BQW5CLElBQThCLEtBQUtuRixhQUFMLENBQW1CbUYsT0FBbkIsQ0FBMkJ1QixVQUEzQixFQUFyQztBQUNILEdBNWVrQztBQThlbkNoRyxFQUFBQSxlQUFlLEVBQUUsVUFBU29DLEVBQVQsRUFBYTNFLElBQWIsRUFBbUI7QUFDaEMsUUFBSSxLQUFLMkQsU0FBVCxFQUFvQixPQURZLENBR2hDOztBQUNBLFFBQUkzRCxJQUFJLEtBQUssS0FBS0YsS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQXBDLEVBQTBDLE9BSlYsQ0FNaEM7QUFDQTs7QUFDQSxTQUFLNEcsV0FBTDtBQUNILEdBdmZrQztBQXlmbkNoRSxFQUFBQSxlQUFlLEVBQUUsVUFBUzhGLGFBQVQsRUFBd0IxSSxJQUF4QixFQUE4QjtBQUMzQyxRQUFJLEtBQUsyRCxTQUFULEVBQW9CLE9BRHVCLENBRzNDOztBQUNBLFFBQUkzRCxJQUFJLEtBQUssS0FBS0YsS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQXBDLEVBQTBDLE9BSkMsQ0FNM0M7QUFDQTs7QUFDQSxTQUFLNEcsV0FBTDtBQUNILEdBbGdCa0M7QUFvZ0JuQ3BFLEVBQUFBLGFBQWEsRUFBRSxVQUFTbUMsRUFBVCxFQUFhM0UsSUFBYixFQUFtQjtBQUM5QixRQUFJLEtBQUsyRCxTQUFULEVBQW9CLE9BRFUsQ0FHOUI7O0FBQ0EsUUFBSTNELElBQUksS0FBSyxLQUFLRixLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBcEMsRUFBMEM7QUFFMUMsU0FBSzRHLFdBQUw7QUFDSCxHQTNnQmtDO0FBNmdCbkNuRSxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTa0MsRUFBVCxFQUFhM0UsSUFBYixFQUFtQjJJLFVBQW5CLEVBQStCO0FBQy9DLFFBQUksS0FBS2hGLFNBQVQsRUFBb0IsT0FEMkIsQ0FHL0M7O0FBQ0EsUUFBSTNELElBQUksS0FBSyxLQUFLRixLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBcEMsRUFBMEM7O0FBRTFDLFNBQUs0SSxhQUFMO0FBQ0gsR0FwaEJrQztBQXNoQm5DbEcsRUFBQUEsYUFBYSxFQUFFLFVBQVNpQyxFQUFULEVBQWEzRSxJQUFiLEVBQW1CO0FBQzlCLFFBQUksS0FBSzJELFNBQVQsRUFBb0IsT0FEVSxDQUc5Qjs7QUFDQSxRQUFJM0QsSUFBSSxLQUFLLEtBQUtGLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUFwQyxFQUEwQztBQUUxQyxRQUFJMkUsRUFBRSxDQUFDa0UsT0FBSCxPQUFpQixjQUFyQixFQUFxQyxPQU5QLENBUTlCO0FBQ0E7QUFDQTs7QUFDQSxTQUFLMUQsUUFBTCxDQUFjO0FBQ1Z2RSxNQUFBQSxpQkFBaUIsRUFBRStELEVBQUUsQ0FBQ3pFLFVBQUgsR0FBZ0JDO0FBRHpCLEtBQWQsRUFFRyxLQUFLTCxLQUFMLENBQVdkLG1CQUZkO0FBR0gsR0FwaUJrQztBQXNpQm5DMkQsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU2dDLEVBQVQsRUFBYTtBQUMzQjtBQUNBLFFBQUksQ0FBQyxLQUFLN0UsS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQTVCLEVBQWtDLE9BRlAsQ0FJM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFFBQUkyRSxFQUFFLENBQUNtRSxTQUFILE9BQW1CLEtBQUtoSixLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIrSSxNQUFuRCxFQUEyRDtBQUN2RCxXQUFLbkMsV0FBTDtBQUNIO0FBQ0osR0FuakJrQztBQXFqQm5DL0QsRUFBQUEsTUFBTSxFQUFFLFVBQVNZLEtBQVQsRUFBZ0J1RixTQUFoQixFQUEyQjVCLElBQTNCLEVBQWlDO0FBQ3JDLFNBQUtqQyxRQUFMLENBQWM7QUFBQ3BFLE1BQUFBLGVBQWUsRUFBRTBDO0FBQWxCLEtBQWQ7QUFDSCxHQXZqQmtDOztBQXlqQm5DK0MsRUFBQUEsa0JBQWtCLENBQUN5QyxrQkFBRCxFQUFxQjtBQUNuQyxXQUFPQSxrQkFBa0IsS0FBSyxDQUF2QixHQUNILEtBQUt4RixLQUFMLENBQVdsQywyQkFEUixHQUVILEtBQUtrQyxLQUFMLENBQVdqQyw4QkFGZjtBQUdILEdBN2pCa0M7O0FBK2pCbkNPLEVBQUFBLDhCQUE4QixFQUFFLGtCQUFpQjtBQUM3QyxVQUFNbUgsY0FBYyxHQUFHLEtBQUsxQyxrQkFBTCxDQUF3QixLQUFLRixxQkFBTCxFQUF4QixDQUF2Qjs7QUFDQSxTQUFLeEMsd0JBQUwsR0FBZ0MsSUFBSXFGLGNBQUosQ0FBVUQsY0FBVixDQUFoQzs7QUFFQSxXQUFPLEtBQUtwRix3QkFBWixFQUFzQztBQUFFO0FBQ3BDbUUsNEJBQWFDLGNBQWIsR0FBOEJrQix1QkFBOUIsQ0FBc0QsS0FBS3RGLHdCQUEzRDs7QUFDQSxVQUFJO0FBQ0EsY0FBTSxLQUFLQSx3QkFBTCxDQUE4QnVGLFFBQTlCLEVBQU47QUFDSCxPQUZELENBRUUsT0FBT2pELENBQVAsRUFBVTtBQUFFO0FBQVU7QUFBZSxPQUpMLENBS2xDOzs7QUFDQSxXQUFLa0QsZ0JBQUw7QUFDSDtBQUNKLEdBM2tCa0M7QUE2a0JuQ3hILEVBQUFBLCtCQUErQixFQUFFLGtCQUFpQjtBQUM5QyxTQUFLOEIseUJBQUwsR0FBaUMsSUFBSXVGLGNBQUosQ0FBVTdMLHdCQUFWLENBQWpDOztBQUNBLFdBQU8sS0FBS3NHLHlCQUFaLEVBQXVDO0FBQUU7QUFDckNxRSw0QkFBYUMsY0FBYixHQUE4QnFCLGtCQUE5QixDQUFpRCxLQUFLM0YseUJBQXREOztBQUNBLFVBQUk7QUFDQSxjQUFNLEtBQUtBLHlCQUFMLENBQStCeUYsUUFBL0IsRUFBTjtBQUNILE9BRkQsQ0FFRSxPQUFPakQsQ0FBUCxFQUFVO0FBQUU7QUFBVTtBQUFlLE9BSkosQ0FLbkM7OztBQUNBLFdBQUtvRCxlQUFMO0FBQ0g7QUFDSixHQXZsQmtDO0FBeWxCbkNBLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFFBQUlwSSx1QkFBY0MsUUFBZCxDQUF1QixjQUF2QixDQUFKLEVBQTRDO0FBRTVDLFFBQUksQ0FBQyxLQUFLUSxhQUFMLENBQW1CbUYsT0FBeEIsRUFBaUM7QUFDakMsUUFBSSxDQUFDLEtBQUtsSCxLQUFMLENBQVd6QixrQkFBaEIsRUFBb0MsT0FKWixDQUt4QjtBQUNBO0FBQ0E7O0FBQ0EsVUFBTW9MLEdBQUcsR0FBR3pJLGlDQUFnQkMsR0FBaEIsRUFBWixDQVJ3QixDQVN4Qjs7O0FBQ0EsUUFBSSxDQUFDd0ksR0FBRCxJQUFRQSxHQUFHLENBQUNDLE9BQUosRUFBWixFQUEyQjtBQUUzQixRQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBRUEsVUFBTUMsZ0JBQWdCLEdBQUcsS0FBS3hKLHNCQUFMLENBQTRCLElBQTVCLENBQXpCOztBQUNBLFVBQU15SixtQkFBbUIsR0FBRyxLQUFLQyxnQkFBTCxDQUFzQkYsZ0JBQXRCLENBQTVCLENBZndCLENBZ0J4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSUEsZ0JBQWdCLElBQUlDLG1CQUFtQixLQUFLLElBQTVDLElBQ0ksS0FBSzlFLGVBQUwsQ0FBcUJjLFdBQXJCLENBQWlDdkIscUJBQWNFLFFBQS9DLENBRFIsRUFDa0U7QUFDOURtRixNQUFBQSxZQUFZLEdBQUcsS0FBZjtBQUNIOztBQUVELFVBQU1JLGtCQUFrQixHQUFHLEtBQUtDLDJCQUFMLENBQWlDO0FBQ3hEQyxNQUFBQSxTQUFTLEVBQUU7QUFENkMsS0FBakMsQ0FBM0I7O0FBR0EsUUFBSUYsa0JBQWtCLEtBQUssSUFBM0IsRUFBaUM7QUFDN0JKLE1BQUFBLFlBQVksR0FBRyxLQUFmO0FBQ0g7O0FBQ0QsUUFBSU8sYUFBYSxHQUFHLEtBQUt6RyxLQUFMLENBQVdwRCxNQUFYLENBQWtCMEosa0JBQWxCLENBQXBCO0FBQ0FKLElBQUFBLFlBQVksR0FBR0EsWUFBWSxJQUN2QjtBQUNBO0FBQ0FJLElBQUFBLGtCQUFrQixHQUFHRixtQkFIVixJQUlYO0FBQ0EsU0FBS25JLGlCQUFMLElBQTBCd0ksYUFBYSxDQUFDdEYsS0FBZCxFQUw5QixDQXpDd0IsQ0FnRHhCOztBQUNBLFVBQU11RixZQUFZLEdBQ2QsS0FBS3ZJLGlCQUFMLElBQTBCLEtBQUs2QixLQUFMLENBQVc3QyxpQkFEekMsQ0FqRHdCLENBb0R4QjtBQUNBOztBQUNBLFFBQUkrSSxZQUFZLElBQUlRLFlBQXBCLEVBQWtDO0FBQzlCLFVBQUlSLFlBQUosRUFBa0I7QUFDZCxhQUFLakksaUJBQUwsR0FBeUJ3SSxhQUFhLENBQUN0RixLQUFkLEVBQXpCO0FBQ0gsT0FGRCxNQUVPO0FBQ0hzRixRQUFBQSxhQUFhLEdBQUcsSUFBaEI7QUFDSDs7QUFDRCxXQUFLdEksaUJBQUwsR0FBeUIsS0FBSzZCLEtBQUwsQ0FBVzdDLGlCQUFwQztBQUVBLFlBQU1tSSxNQUFNLEdBQUcsS0FBS2pKLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUF2QixDQUE0QitJLE1BQTNDO0FBQ0EsWUFBTXFCLFFBQVEsR0FBRyxDQUFDaEosdUJBQWNDLFFBQWQsQ0FBdUIsa0JBQXZCLEVBQTJDMEgsTUFBM0MsQ0FBbEI7QUFFQXZMLE1BQUFBLFFBQVEsQ0FBQywwQ0FBRCxFQUNKLEtBQUtzQyxLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIrSSxNQUR4QixFQUVKLElBRkksRUFFRSxLQUFLdEYsS0FBTCxDQUFXN0MsaUJBRmIsRUFHSnNKLGFBQWEsR0FBRyxRQUFRQSxhQUFhLENBQUN0RixLQUFkLEVBQVgsR0FBbUMsRUFINUMsRUFJSixhQUFhd0YsUUFKVCxDQUFSOztBQU1BcEosdUNBQWdCQyxHQUFoQixHQUFzQm9KLGtCQUF0QixDQUNJLEtBQUt2SyxLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIrSSxNQURoQyxFQUVJLEtBQUt0RixLQUFMLENBQVc3QyxpQkFGZixFQUdJc0osYUFISixFQUdtQjtBQUNmO0FBQUMzTCxRQUFBQSxNQUFNLEVBQUU2TDtBQUFULE9BSkosRUFLRUUsS0FMRixDQUtTbEUsQ0FBRCxJQUFPO0FBQ1g7QUFDQSxZQUFJQSxDQUFDLENBQUNtRSxPQUFGLEtBQWMsZ0JBQWQsSUFBa0NMLGFBQXRDLEVBQXFEO0FBQ2pELGlCQUFPbEosaUNBQWdCQyxHQUFoQixHQUFzQnVJLGVBQXRCLENBQ0hVLGFBREcsRUFFSDtBQUFDM0wsWUFBQUEsTUFBTSxFQUFFNkw7QUFBVCxXQUZHLEVBR0xFLEtBSEssQ0FHRWxFLENBQUQsSUFBTztBQUNYM0ksWUFBQUEsT0FBTyxDQUFDK00sS0FBUixDQUFjcEUsQ0FBZDtBQUNBLGlCQUFLMUUsaUJBQUwsR0FBeUJDLFNBQXpCO0FBQ0gsV0FOTSxDQUFQO0FBT0gsU0FSRCxNQVFPO0FBQ0hsRSxVQUFBQSxPQUFPLENBQUMrTSxLQUFSLENBQWNwRSxDQUFkO0FBQ0gsU0FaVSxDQWFYOzs7QUFDQSxhQUFLMUUsaUJBQUwsR0FBeUJDLFNBQXpCO0FBQ0EsYUFBS0MsaUJBQUwsR0FBeUJELFNBQXpCO0FBQ0gsT0FyQkQsRUFqQjhCLENBd0M5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxVQUFJLEtBQUs4SSxxQkFBTCxFQUFKLEVBQWtDO0FBQzlCLGFBQUszSyxLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIwSywwQkFBNUIsQ0FBdUQsT0FBdkQsRUFBZ0UsQ0FBaEU7QUFDQSxhQUFLNUssS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQXZCLENBQTRCMEssMEJBQTVCLENBQXVELFdBQXZELEVBQW9FLENBQXBFOztBQUNBekksNEJBQUkwSSxRQUFKLENBQWE7QUFDVGhFLFVBQUFBLE1BQU0sRUFBRSxjQURDO0FBRVRvQyxVQUFBQSxNQUFNLEVBQUUsS0FBS2pKLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUF2QixDQUE0QitJO0FBRjNCLFNBQWI7QUFJSDtBQUNKO0FBQ0osR0Fyc0JrQztBQXVzQm5DO0FBQ0E7QUFDQU8sRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixRQUFJLENBQUMsS0FBS3hKLEtBQUwsQ0FBV3hCLGlCQUFoQixFQUFtQzs7QUFDbkMsUUFBSSxLQUFLZ0kscUJBQUwsT0FBaUMsQ0FBckMsRUFBd0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0gsS0FOd0IsQ0FPekI7QUFDQTtBQUNBOzs7QUFDQSxVQUFNc0Usa0JBQWtCLEdBQUcsS0FBS1osMkJBQUwsQ0FBaUM7QUFDeERhLE1BQUFBLFlBQVksRUFBRTtBQUQwQyxLQUFqQyxDQUEzQjs7QUFJQSxRQUFJRCxrQkFBa0IsS0FBSyxJQUEzQixFQUFpQztBQUM3QjtBQUNIOztBQUNELFVBQU1FLGtCQUFrQixHQUFHLEtBQUtySCxLQUFMLENBQVdwRCxNQUFYLENBQWtCdUssa0JBQWxCLENBQTNCOztBQUNBLFNBQUt4QyxjQUFMLENBQW9CMEMsa0JBQWtCLENBQUNsRyxLQUFuQixFQUFwQixFQUNvQmtHLGtCQUFrQixDQUFDekMsS0FBbkIsRUFEcEIsRUFsQnlCLENBcUJ6QjtBQUNBOzs7QUFDQSxRQUFJLEtBQUs1RSxLQUFMLENBQVc5QyxpQkFBZixFQUFrQztBQUM5QixXQUFLd0UsUUFBTCxDQUFjO0FBQ1Z4RSxRQUFBQSxpQkFBaUIsRUFBRTtBQURULE9BQWQ7QUFHSDtBQUNKLEdBcnVCa0M7QUF3dUJuQztBQUNBb0ssRUFBQUEsOEJBQThCLEVBQUUsWUFBVztBQUN2QyxRQUFJLENBQUMsS0FBS2pMLEtBQUwsQ0FBV3hCLGlCQUFoQixFQUFtQyxPQURJLENBR3ZDO0FBQ0E7QUFDQTs7QUFDQSxVQUFNK0IsTUFBTSxHQUFHLEtBQUswRSxlQUFMLENBQXFCaUcsU0FBckIsRUFBZixDQU51QyxDQVF2Qzs7O0FBQ0EsUUFBSUMsQ0FBSjs7QUFDQSxTQUFLQSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUc1SyxNQUFNLENBQUN5RSxNQUF2QixFQUErQm1HLENBQUMsRUFBaEMsRUFBb0M7QUFDaEMsVUFBSTVLLE1BQU0sQ0FBQzRLLENBQUQsQ0FBTixDQUFVckcsS0FBVixNQUFxQixLQUFLbkIsS0FBTCxDQUFXN0MsaUJBQXBDLEVBQXVEO0FBQ25EO0FBQ0g7QUFDSjs7QUFDRCxRQUFJcUssQ0FBQyxJQUFJNUssTUFBTSxDQUFDeUUsTUFBaEIsRUFBd0I7QUFDcEI7QUFDSCxLQWpCc0MsQ0FtQnZDOzs7QUFDQSxVQUFNK0MsUUFBUSxHQUFHN0csaUNBQWdCQyxHQUFoQixHQUFzQjZHLFdBQXRCLENBQWtDQyxNQUFuRDs7QUFDQSxTQUFLa0QsQ0FBQyxFQUFOLEVBQVVBLENBQUMsR0FBRzVLLE1BQU0sQ0FBQ3lFLE1BQXJCLEVBQTZCbUcsQ0FBQyxFQUE5QixFQUFrQztBQUM5QixZQUFNdEcsRUFBRSxHQUFHdEUsTUFBTSxDQUFDNEssQ0FBRCxDQUFqQjs7QUFDQSxVQUFJLENBQUN0RyxFQUFFLENBQUNxRCxNQUFKLElBQWNyRCxFQUFFLENBQUNxRCxNQUFILENBQVVELE1BQVYsSUFBb0JGLFFBQXRDLEVBQWdEO0FBQzVDO0FBQ0g7QUFDSixLQTFCc0MsQ0EyQnZDOzs7QUFDQW9ELElBQUFBLENBQUM7QUFFRCxVQUFNdEcsRUFBRSxHQUFHdEUsTUFBTSxDQUFDNEssQ0FBRCxDQUFqQjs7QUFDQSxTQUFLN0MsY0FBTCxDQUFvQnpELEVBQUUsQ0FBQ0MsS0FBSCxFQUFwQixFQUFnQ0QsRUFBRSxDQUFDMEQsS0FBSCxFQUFoQztBQUNILEdBendCa0M7O0FBMndCbkM7O0FBRUE2QyxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJLEtBQUtuRyxlQUFMLENBQXFCYyxXQUFyQixDQUFpQ3ZCLHFCQUFjRSxRQUEvQyxDQUFKLEVBQThEO0FBQzFELFdBQUtnRSxhQUFMO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsVUFBSSxLQUFLM0csYUFBTCxDQUFtQm1GLE9BQXZCLEVBQWdDO0FBQzVCLGFBQUtuRixhQUFMLENBQW1CbUYsT0FBbkIsQ0FBMkJtRSxjQUEzQjtBQUNIO0FBQ0o7QUFDSixHQTF4QmtDOztBQTR4Qm5DOzs7QUFHQUMsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixRQUFJLENBQUMsS0FBS3RMLEtBQUwsQ0FBV3hCLGlCQUFoQixFQUFtQztBQUNuQyxRQUFJLENBQUMsS0FBS3VELGFBQUwsQ0FBbUJtRixPQUF4QixFQUFpQztBQUNqQyxRQUFJLENBQUMsS0FBS3ZELEtBQUwsQ0FBVzdDLGlCQUFoQixFQUFtQyxPQUhWLENBS3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFNeUssR0FBRyxHQUFHLEtBQUt4SixhQUFMLENBQW1CbUYsT0FBbkIsQ0FBMkJWLHFCQUEzQixFQUFaOztBQUNBLFFBQUkrRSxHQUFHLEtBQUssSUFBWixFQUFrQjtBQUNkO0FBQ0E7QUFDQSxXQUFLeEosYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCc0UsYUFBM0IsQ0FBeUMsS0FBSzdILEtBQUwsQ0FBVzdDLGlCQUFwRCxFQUNxQyxDQURyQyxFQUN3QyxJQUFFLENBRDFDOztBQUVBO0FBQ0gsS0FsQndCLENBb0J6QjtBQUNBO0FBQ0E7OztBQUNBLFNBQUs0SCxhQUFMLENBQW1CLEtBQUsvRSxLQUFMLENBQVc3QyxpQkFBOUIsRUFBaUQsQ0FBakQsRUFBb0QsSUFBRSxDQUF0RDtBQUNILEdBdnpCa0M7O0FBeXpCbkM7O0FBRUEySyxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCLFFBQUksQ0FBQyxLQUFLekwsS0FBTCxDQUFXeEIsaUJBQWhCLEVBQW1DOztBQUVuQyxVQUFNa04sSUFBSSxHQUFHLEtBQUtwTCxzQkFBTCxFQUFiLENBSHlCLENBS3pCOzs7QUFDQSxVQUFNcUwsRUFBRSxHQUFHLEtBQUszTCxLQUFMLENBQVcvQixXQUFYLENBQXVCMk4sbUJBQXZCLENBQTJDRixJQUEzQyxDQUFYO0FBQ0EsUUFBSUcsSUFBSjs7QUFDQSxRQUFJRixFQUFKLEVBQVE7QUFDSixZQUFNM0UsS0FBSyxHQUFHMkUsRUFBRSxDQUFDVCxTQUFILEdBQWVZLElBQWYsQ0FBcUJ4RixDQUFELElBQU87QUFBRSxlQUFPQSxDQUFDLENBQUN4QixLQUFGLE1BQWE0RyxJQUFwQjtBQUEyQixPQUF4RCxDQUFkOztBQUNBLFVBQUkxRSxLQUFKLEVBQVc7QUFDUDZFLFFBQUFBLElBQUksR0FBRzdFLEtBQUssQ0FBQ3VCLEtBQU4sRUFBUDtBQUNIO0FBQ0o7O0FBRUQsU0FBS0QsY0FBTCxDQUFvQm9ELElBQXBCLEVBQTBCRyxJQUExQjtBQUNILEdBMzBCa0M7O0FBNjBCbkM7OztBQUdBbEIsRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixXQUFPLEtBQUs1SSxhQUFMLENBQW1CbUYsT0FBbkIsSUFDQSxLQUFLbkYsYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCdUIsVUFBM0IsRUFEQSxJQUVBLEtBQUt4RCxlQUZMLElBR0EsQ0FBQyxLQUFLQSxlQUFMLENBQXFCYyxXQUFyQixDQUFpQ3ZCLHFCQUFjRSxRQUEvQyxDQUhSO0FBSUgsR0FyMUJrQzs7QUF3MUJuQzs7Ozs7QUFLQWdELEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCLFFBQUksQ0FBQyxLQUFLM0YsYUFBTCxDQUFtQm1GLE9BQXhCLEVBQWlDO0FBQUUsYUFBTyxJQUFQO0FBQWM7O0FBQ2pELFdBQU8sS0FBS25GLGFBQUwsQ0FBbUJtRixPQUFuQixDQUEyQlEsY0FBM0IsRUFBUDtBQUNILEdBaDJCa0M7QUFrMkJuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQWxCLEVBQUFBLHFCQUFxQixFQUFFLFlBQVc7QUFDOUIsUUFBSSxDQUFDLEtBQUt4RyxLQUFMLENBQVd4QixpQkFBaEIsRUFBbUMsT0FBTyxJQUFQO0FBQ25DLFFBQUksQ0FBQyxLQUFLdUQsYUFBTCxDQUFtQm1GLE9BQXhCLEVBQWlDLE9BQU8sSUFBUDs7QUFFakMsVUFBTXFFLEdBQUcsR0FBRyxLQUFLeEosYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCVixxQkFBM0IsRUFBWjs7QUFDQSxRQUFJK0UsR0FBRyxLQUFLLElBQVosRUFBa0I7QUFDZCxhQUFPQSxHQUFQO0FBQ0gsS0FQNkIsQ0FTOUI7QUFDQTs7O0FBQ0EsVUFBTU0sSUFBSSxHQUFHL04sYUFBYSxDQUFDNEIsbUJBQWQsQ0FBa0MsS0FBS00sS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQXZCLENBQTRCK0ksTUFBOUQsQ0FBYjs7QUFDQSxRQUFJNEMsSUFBSSxJQUFJLEtBQUtsSSxLQUFMLENBQVdwRCxNQUFYLENBQWtCeUUsTUFBbEIsR0FBMkIsQ0FBdkMsRUFBMEM7QUFDdEMsVUFBSTZHLElBQUksR0FBRyxLQUFLbEksS0FBTCxDQUFXcEQsTUFBWCxDQUFrQixDQUFsQixFQUFxQmdJLEtBQXJCLEVBQVgsRUFBeUM7QUFDckMsZUFBTyxDQUFDLENBQVI7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLENBQVA7QUFDSDtBQUNKOztBQUVELFdBQU8sSUFBUDtBQUNILEdBNzNCa0M7QUErM0JuQ3dELEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUI7QUFDQTtBQUNBO0FBQ0EsVUFBTUMsR0FBRyxHQUFHLEtBQUt4RixxQkFBTCxFQUFaO0FBQ0EsVUFBTStFLEdBQUcsR0FBRyxLQUFLNUgsS0FBTCxDQUFXN0MsaUJBQVgsS0FBaUMsSUFBakMsTUFBeUM7QUFDaERrTCxJQUFBQSxHQUFHLEdBQUcsQ0FBTixJQUFXQSxHQUFHLEtBQUssSUFEWixDQUFaLENBTDRCLENBTUc7O0FBQy9CLFdBQU9ULEdBQVA7QUFDSCxHQXY0QmtDOztBQXk0Qm5DOzs7OztBQUtBVSxFQUFBQSxlQUFlLEVBQUUsVUFBU3BILEVBQVQsRUFBYTtBQUMxQixRQUFJLENBQUMsS0FBSzlDLGFBQUwsQ0FBbUJtRixPQUF4QixFQUFpQztBQUFFO0FBQVMsS0FEbEIsQ0FHMUI7QUFDQTs7O0FBQ0EsUUFBSXJDLEVBQUUsQ0FBQ3FILE9BQUgsSUFBYyxDQUFDckgsRUFBRSxDQUFDc0gsUUFBbEIsSUFBOEIsQ0FBQ3RILEVBQUUsQ0FBQ3VILE1BQWxDLElBQTRDLENBQUN2SCxFQUFFLENBQUN3SCxPQUFoRCxJQUEyRHhILEVBQUUsQ0FBQ3lILEdBQUgsS0FBV0MsY0FBSUMsR0FBOUUsRUFBbUY7QUFDL0UsV0FBS3BCLGtCQUFMO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsV0FBS3JKLGFBQUwsQ0FBbUJtRixPQUFuQixDQUEyQitFLGVBQTNCLENBQTJDcEgsRUFBM0M7QUFDSDtBQUNKLEdBeDVCa0M7QUEwNUJuQzdCLEVBQUFBLGFBQWEsRUFBRSxVQUFTaEQsS0FBVCxFQUFnQjtBQUMzQixVQUFNeU0sWUFBWSxHQUFHek0sS0FBSyxDQUFDcEIsT0FBM0I7QUFDQSxVQUFNOE4sV0FBVyxHQUFHMU0sS0FBSyxDQUFDbkIsZ0JBQTFCLENBRjJCLENBSTNCO0FBQ0E7O0FBQ0EsUUFBSThOLFVBQVUsR0FBRyxDQUFqQjs7QUFDQSxRQUFJRCxXQUFXLElBQUksSUFBbkIsRUFBeUI7QUFDckJDLE1BQUFBLFVBQVUsR0FBRyxHQUFiO0FBQ0g7O0FBRUQsV0FBTyxLQUFLakUsYUFBTCxDQUFtQitELFlBQW5CLEVBQWlDQyxXQUFqQyxFQUE4Q0MsVUFBOUMsQ0FBUDtBQUNILEdBdDZCa0M7O0FBdzZCbkM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkFqRSxFQUFBQSxhQUFhLEVBQUUsVUFBUzlKLE9BQVQsRUFBa0I4TixXQUFsQixFQUErQkMsVUFBL0IsRUFBMkM7QUFDdEQsU0FBSzFILGVBQUwsR0FBdUIsSUFBSTJILE1BQU0sQ0FBQ0MsY0FBWCxDQUNuQjNMLGlDQUFnQkMsR0FBaEIsRUFEbUIsRUFDSSxLQUFLbkIsS0FBTCxDQUFXL0IsV0FEZixFQUVuQjtBQUFDNk8sTUFBQUEsV0FBVyxFQUFFLEtBQUs5TSxLQUFMLENBQVdaO0FBQXpCLEtBRm1CLENBQXZCOztBQUlBLFVBQU0yTixRQUFRLEdBQUcsTUFBTTtBQUNuQjtBQUNBO0FBQ0EsVUFBSSxLQUFLaEwsYUFBTCxDQUFtQm1GLE9BQXZCLEVBQWdDO0FBQzVCLGFBQUtuRixhQUFMLENBQW1CbUYsT0FBbkIsQ0FBMkI4RixlQUEzQjtBQUNIOztBQUNELFdBQUtsRSxhQUFMLEdBTm1CLENBUW5CO0FBQ0E7QUFDQTs7O0FBQ0EsV0FBS21DLDhCQUFMOztBQUVBLFdBQUs1RixRQUFMLENBQWM7QUFDVjFFLFFBQUFBLGVBQWUsRUFBRSxLQUFLc0UsZUFBTCxDQUFxQmMsV0FBckIsQ0FBaUN2QixxQkFBY0MsU0FBL0MsQ0FEUDtBQUVWN0QsUUFBQUEsa0JBQWtCLEVBQUUsS0FBS3FFLGVBQUwsQ0FBcUJjLFdBQXJCLENBQWlDdkIscUJBQWNFLFFBQS9DLENBRlY7QUFHVmpFLFFBQUFBLGVBQWUsRUFBRTtBQUhQLE9BQWQsRUFJRyxNQUFNO0FBQ0w7QUFDQSxZQUFJLENBQUMsS0FBS3NCLGFBQUwsQ0FBbUJtRixPQUF4QixFQUFpQztBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBdkosVUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksMkNBQ0EsMEJBRFo7QUFFQTtBQUNIOztBQUNELFlBQUlnQixPQUFKLEVBQWE7QUFDVCxlQUFLbUQsYUFBTCxDQUFtQm1GLE9BQW5CLENBQTJCc0UsYUFBM0IsQ0FBeUM1TSxPQUF6QyxFQUFrRDhOLFdBQWxELEVBQ3FDQyxVQURyQztBQUVILFNBSEQsTUFHTztBQUNILGVBQUs1SyxhQUFMLENBQW1CbUYsT0FBbkIsQ0FBMkJtRSxjQUEzQjtBQUNIOztBQUVELGFBQUszQixlQUFMO0FBQ0gsT0F2QkQ7QUF3QkgsS0FyQ0Q7O0FBdUNBLFVBQU11RCxPQUFPLEdBQUl2QyxLQUFELElBQVc7QUFDdkIsV0FBS3JGLFFBQUwsQ0FBYztBQUFFNUUsUUFBQUEsZUFBZSxFQUFFO0FBQW5CLE9BQWQ7QUFDQTlDLE1BQUFBLE9BQU8sQ0FBQytNLEtBQVIsMkNBQ3VDOUwsT0FEdkMsZUFDbUQ4TCxLQURuRDtBQUdBLFlBQU13QyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFFQSxVQUFJQyxVQUFKLENBUHVCLENBU3ZCO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFVBQUl6TyxPQUFKLEVBQWE7QUFDVHlPLFFBQUFBLFVBQVUsR0FBRyxNQUFNO0FBQ2Y7QUFDQWxMLDhCQUFJMEksUUFBSixDQUFhO0FBQ1RoRSxZQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUeUcsWUFBQUEsT0FBTyxFQUFFLEtBQUt0TixLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIrSTtBQUY1QixXQUFiO0FBSUgsU0FORDtBQU9IOztBQUNELFVBQUlzRSxPQUFKOztBQUNBLFVBQUk3QyxLQUFLLENBQUNELE9BQU4sSUFBaUIsYUFBckIsRUFBb0M7QUFDaEM4QyxRQUFBQSxPQUFPLEdBQUcseUJBQ04scUVBQ0EseURBRk0sQ0FBVjtBQUlILE9BTEQsTUFLTztBQUNIQSxRQUFBQSxPQUFPLEdBQUcseUJBQ04scUVBQ0Esb0JBRk0sQ0FBVjtBQUlIOztBQUNEQyxxQkFBTUMsbUJBQU4sQ0FBMEIsa0NBQTFCLEVBQThELEVBQTlELEVBQWtFUCxXQUFsRSxFQUErRTtBQUMzRVEsUUFBQUEsS0FBSyxFQUFFLHlCQUFHLGtDQUFILENBRG9FO0FBRTNFQyxRQUFBQSxXQUFXLEVBQUVKLE9BRjhEO0FBRzNFRixRQUFBQSxVQUFVLEVBQUVBO0FBSCtELE9BQS9FO0FBS0gsS0F2Q0QsQ0E1Q3NELENBcUZ0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsVUFBTTlGLFFBQVEsR0FBRyxLQUFLdkgsS0FBTCxDQUFXL0IsV0FBWCxDQUF1QjJOLG1CQUF2QixDQUEyQ2hOLE9BQTNDLENBQWpCOztBQUNBLFFBQUkySSxRQUFKLEVBQWM7QUFDVjtBQUNBO0FBQ0EsV0FBS3RDLGVBQUwsQ0FBcUIySSxJQUFyQixDQUEwQmhQLE9BQTFCLEVBQW1DckIsWUFBbkMsRUFIVSxDQUd3Qzs7O0FBQ2xEd1AsTUFBQUEsUUFBUTtBQUNYLEtBTEQsTUFLTztBQUNILFlBQU1jLElBQUksR0FBRyxLQUFLNUksZUFBTCxDQUFxQjJJLElBQXJCLENBQTBCaFAsT0FBMUIsRUFBbUNyQixZQUFuQyxDQUFiOztBQUNBLFdBQUs4SCxRQUFMLENBQWM7QUFDVjlFLFFBQUFBLE1BQU0sRUFBRSxFQURFO0FBRVZDLFFBQUFBLFVBQVUsRUFBRSxFQUZGO0FBR1ZHLFFBQUFBLGVBQWUsRUFBRSxLQUhQO0FBSVZDLFFBQUFBLGtCQUFrQixFQUFFLEtBSlY7QUFLVkgsUUFBQUEsZUFBZSxFQUFFO0FBTFAsT0FBZDtBQU9Bb04sTUFBQUEsSUFBSSxDQUFDN0gsSUFBTCxDQUFVK0csUUFBVixFQUFvQkUsT0FBcEI7QUFDSDtBQUNKLEdBdGlDa0M7QUF3aUNuQztBQUNBO0FBQ0E7QUFDQW5FLEVBQUFBLGFBQWEsRUFBRSxZQUFXO0FBQ3RCO0FBQ0E7QUFDQSxRQUFJLEtBQUtqRixTQUFULEVBQW9CO0FBRXBCLFNBQUt3QixRQUFMLENBQWMsS0FBS0QsVUFBTCxFQUFkO0FBQ0gsR0FqakNrQztBQW1qQ25DO0FBQ0FBLEVBQUFBLFVBQVUsRUFBRSxZQUFXO0FBQ25CLFVBQU03RSxNQUFNLEdBQUcsS0FBSzBFLGVBQUwsQ0FBcUJpRyxTQUFyQixFQUFmOztBQUNBLFVBQU14SyxzQkFBc0IsR0FBRyxLQUFLb04sb0JBQUwsQ0FBMEJ2TixNQUExQixDQUEvQixDQUZtQixDQUluQjtBQUNBOzs7QUFDQSxVQUFNQyxVQUFVLEdBQUcsQ0FBQyxHQUFHRCxNQUFKLENBQW5CLENBTm1CLENBUW5COztBQUNBLFFBQUksQ0FBQyxLQUFLMEUsZUFBTCxDQUFxQmMsV0FBckIsQ0FBaUN2QixxQkFBY0UsUUFBL0MsQ0FBTCxFQUErRDtBQUMzRG5FLE1BQUFBLE1BQU0sQ0FBQ3dOLElBQVAsQ0FBWSxHQUFHLEtBQUsvTixLQUFMLENBQVcvQixXQUFYLENBQXVCK1AsZ0JBQXZCLEVBQWY7QUFDSDs7QUFFRCxXQUFPO0FBQ0h6TixNQUFBQSxNQURHO0FBRUhDLE1BQUFBLFVBRkc7QUFHSEUsTUFBQUE7QUFIRyxLQUFQO0FBS0gsR0F0a0NrQzs7QUF3a0NuQzs7Ozs7Ozs7OztBQVVBb04sRUFBQUEsb0JBQW9CLEVBQUUsVUFBU3ZOLE1BQVQsRUFBaUI7QUFDbkMsVUFBTUwsSUFBSSxHQUFHLEtBQUtGLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUFwQzs7QUFFQSxRQUFJSyxNQUFNLENBQUN5RSxNQUFQLEtBQWtCLENBQWxCLElBQXVCLENBQUM5RSxJQUF4QixJQUNBLENBQUNnQixpQ0FBZ0JDLEdBQWhCLEdBQXNCOE0sZUFBdEIsQ0FBc0MvTixJQUFJLENBQUMrSSxNQUEzQyxDQURMLEVBQ3lEO0FBQ3JELGFBQU8sQ0FBUDtBQUNIOztBQUVELFVBQU1oQixNQUFNLEdBQUcvRyxpQ0FBZ0JDLEdBQWhCLEdBQXNCNkcsV0FBdEIsQ0FBa0NDLE1BQWpELENBUm1DLENBVW5DO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSWtELENBQUo7QUFDQSxRQUFJK0MsY0FBYyxHQUFHLE9BQXJCOztBQUNBLFNBQUsvQyxDQUFDLEdBQUc1SyxNQUFNLENBQUN5RSxNQUFQLEdBQWdCLENBQXpCLEVBQTRCbUcsQ0FBQyxJQUFJLENBQWpDLEVBQW9DQSxDQUFDLEVBQXJDLEVBQXlDO0FBQ3JDLFlBQU01RCxRQUFRLEdBQUdySCxJQUFJLENBQUMwTCxtQkFBTCxDQUF5QnJMLE1BQU0sQ0FBQzRLLENBQUQsQ0FBTixDQUFVckcsS0FBVixFQUF6QixDQUFqQjs7QUFDQSxVQUFJLENBQUN5QyxRQUFMLEVBQWU7QUFDWDtBQUNBO0FBQ0E7QUFDQTVKLFFBQUFBLE9BQU8sQ0FBQ3dGLElBQVIsQ0FDSSxnQkFBUzVDLE1BQU0sQ0FBQzRLLENBQUQsQ0FBTixDQUFVckcsS0FBVixFQUFULHNCQUFzQzVFLElBQUksQ0FBQytJLE1BQTNDLG1EQURKO0FBSUE7QUFDSDs7QUFDRCxZQUFNa0YsbUJBQW1CLEdBQ2pCNUcsUUFBUSxDQUFDNkcsUUFBVCxDQUFrQjVKLHFCQUFjRSxRQUFoQyxFQUEwQzJKLFNBQTFDLENBQW9EcEcsTUFBcEQsQ0FEUjtBQUVBaUcsTUFBQUEsY0FBYyxHQUFHQyxtQkFBbUIsR0FBR0EsbUJBQW1CLENBQUNHLFVBQXZCLEdBQW9DLE9BQXhFO0FBQ0EsWUFBTUMsY0FBYyxHQUFHaEgsUUFBUSxDQUFDMkQsU0FBVCxFQUF2Qjs7QUFDQSxXQUFLLElBQUlzRCxDQUFDLEdBQUdELGNBQWMsQ0FBQ3ZKLE1BQWYsR0FBd0IsQ0FBckMsRUFBd0N3SixDQUFDLElBQUksQ0FBN0MsRUFBZ0RBLENBQUMsRUFBakQsRUFBcUQ7QUFDakQsY0FBTXhILEtBQUssR0FBR3VILGNBQWMsQ0FBQ0MsQ0FBRCxDQUE1Qjs7QUFDQSxZQUFJeEgsS0FBSyxDQUFDbEMsS0FBTixPQUFrQnZFLE1BQU0sQ0FBQzRLLENBQUQsQ0FBTixDQUFVckcsS0FBVixFQUF0QixFQUF5QztBQUNyQztBQUNILFNBRkQsTUFFTyxJQUFJa0MsS0FBSyxDQUFDeUgsV0FBTixPQUF3QnhHLE1BQXhCLElBQ0pqQixLQUFLLENBQUMrQixPQUFOLE9BQW9CLGVBRHBCLEVBQ3FDO0FBQ3hDLGdCQUFNMkYsV0FBVyxHQUFHMUgsS0FBSyxDQUFDMkgsY0FBTixFQUFwQjtBQUNBVCxVQUFBQSxjQUFjLEdBQUdRLFdBQVcsQ0FBQ0osVUFBWixJQUEwQixPQUEzQztBQUNIO0FBQ0o7O0FBQ0Q7QUFDSCxLQTFDa0MsQ0E0Q25DO0FBQ0E7OztBQUNBLFdBQU9uRCxDQUFDLElBQUksQ0FBWixFQUFlQSxDQUFDLEVBQWhCLEVBQW9CO0FBQ2hCLFlBQU1uRSxLQUFLLEdBQUd6RyxNQUFNLENBQUM0SyxDQUFELENBQXBCOztBQUNBLFVBQUluRSxLQUFLLENBQUN5SCxXQUFOLE9BQXdCeEcsTUFBeEIsSUFDR2pCLEtBQUssQ0FBQytCLE9BQU4sT0FBb0IsZUFEM0IsRUFDNEM7QUFDeEMsY0FBTTJGLFdBQVcsR0FBRzFILEtBQUssQ0FBQzJILGNBQU4sRUFBcEI7QUFDQVQsUUFBQUEsY0FBYyxHQUFHUSxXQUFXLENBQUNKLFVBQVosSUFBMEIsT0FBM0M7QUFDSCxPQUpELE1BSU8sSUFBSUosY0FBYyxLQUFLLE9BQW5CLEtBQ0NsSCxLQUFLLENBQUM0SCxtQkFBTixNQUErQjVILEtBQUssQ0FBQzZILGdCQUFOLEVBRGhDLENBQUosRUFDK0Q7QUFDbEU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFPMUQsQ0FBQyxHQUFHLENBQVg7QUFDSDtBQUNKOztBQUNELFdBQU8sQ0FBUDtBQUNILEdBaHBDa0M7QUFrcENuQ25CLEVBQUFBLGdCQUFnQixFQUFFLFVBQVM4RSxJQUFULEVBQWU7QUFDN0IsU0FBSyxJQUFJM0QsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxLQUFLeEgsS0FBTCxDQUFXcEQsTUFBWCxDQUFrQnlFLE1BQXRDLEVBQThDLEVBQUVtRyxDQUFoRCxFQUFtRDtBQUMvQyxVQUFJMkQsSUFBSSxJQUFJLEtBQUtuTCxLQUFMLENBQVdwRCxNQUFYLENBQWtCNEssQ0FBbEIsRUFBcUJyRyxLQUFyQixFQUFaLEVBQTBDO0FBQ3RDLGVBQU9xRyxDQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLElBQVA7QUFDSCxHQXpwQ2tDO0FBMnBDbkNqQixFQUFBQSwyQkFBMkIsRUFBRSxVQUFTNkUsSUFBVCxFQUFlO0FBQ3hDQSxJQUFBQSxJQUFJLEdBQUdBLElBQUksSUFBSSxFQUFmO0FBQ0EsVUFBTTVFLFNBQVMsR0FBRzRFLElBQUksQ0FBQzVFLFNBQUwsSUFBa0IsS0FBcEM7QUFDQSxVQUFNWSxZQUFZLEdBQUdnRSxJQUFJLENBQUNoRSxZQUFMLElBQXFCLEtBQTFDO0FBRUEsVUFBTWlFLFlBQVksR0FBRyxLQUFLak4sYUFBTCxDQUFtQm1GLE9BQXhDO0FBQ0EsUUFBSSxDQUFDOEgsWUFBTCxFQUFtQixPQUFPLElBQVA7O0FBRW5CLFVBQU1DLGdCQUFnQixHQUFHQyxrQkFBU0MsV0FBVCxDQUFxQkgsWUFBckIsQ0FBekI7O0FBQ0EsUUFBSSxDQUFDQyxnQkFBTCxFQUF1QixPQUFPLElBQVAsQ0FUaUIsQ0FTSjs7QUFDcEMsVUFBTUcsV0FBVyxHQUFHSCxnQkFBZ0IsQ0FBQ0kscUJBQWpCLEVBQXBCOztBQUNBLFVBQU10SCxRQUFRLEdBQUc3RyxpQ0FBZ0JDLEdBQWhCLEdBQXNCNkcsV0FBdEIsQ0FBa0NDLE1BQW5EOztBQUVBLFVBQU1xSCxZQUFZLEdBQUlDLElBQUQsSUFBVTtBQUMzQixVQUFJQSxJQUFKLEVBQVU7QUFDTixjQUFNQyxZQUFZLEdBQUdELElBQUksQ0FBQ0YscUJBQUwsRUFBckI7O0FBQ0EsWUFBS3RFLFlBQVksSUFBSXlFLFlBQVksQ0FBQ0MsR0FBYixHQUFtQkwsV0FBVyxDQUFDTSxNQUFoRCxJQUNDLENBQUMzRSxZQUFELElBQWlCeUUsWUFBWSxDQUFDRSxNQUFiLEdBQXNCTixXQUFXLENBQUNNLE1BRHhELEVBQ2lFO0FBQzdELGlCQUFPLElBQVA7QUFDSDtBQUNKOztBQUNELGFBQU8sS0FBUDtBQUNILEtBVEQsQ0Fid0MsQ0F3QnhDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUlDLDJCQUEyQixHQUFHLENBQWxDLENBN0J3QyxDQThCeEM7QUFDQTs7QUFDQSxTQUFLLElBQUl4RSxDQUFDLEdBQUcsS0FBS3hILEtBQUwsQ0FBV25ELFVBQVgsQ0FBc0J3RSxNQUF0QixHQUErQixDQUE1QyxFQUErQ21HLENBQUMsSUFBSSxDQUFwRCxFQUF1RCxFQUFFQSxDQUF6RCxFQUE0RDtBQUN4RCxZQUFNdEcsRUFBRSxHQUFHLEtBQUtsQixLQUFMLENBQVduRCxVQUFYLENBQXNCMkssQ0FBdEIsQ0FBWDtBQUVBLFlBQU1vRSxJQUFJLEdBQUdQLFlBQVksQ0FBQ1ksaUJBQWIsQ0FBK0IvSyxFQUFFLENBQUNDLEtBQUgsRUFBL0IsQ0FBYjtBQUNBLFlBQU0rSyxRQUFRLEdBQUdQLFlBQVksQ0FBQ0MsSUFBRCxDQUE3QixDQUp3RCxDQU14RDtBQUNBO0FBQ0E7O0FBQ0EsVUFBSU0sUUFBUSxJQUFJRiwyQkFBMkIsS0FBSyxDQUFoRCxFQUFtRDtBQUMvQyxlQUFPeEUsQ0FBQyxHQUFHd0UsMkJBQVg7QUFDSDs7QUFDRCxVQUFJSixJQUFJLElBQUksQ0FBQ00sUUFBYixFQUF1QjtBQUNuQjtBQUNBRixRQUFBQSwyQkFBMkIsR0FBRyxDQUE5QjtBQUNIOztBQUVELFlBQU1HLFlBQVksR0FBRyxDQUFDLENBQUNqTCxFQUFFLENBQUNrTCxNQUFMLElBQWU7QUFDL0I1RixNQUFBQSxTQUFTLElBQUl0RixFQUFFLENBQUNxRCxNQUFoQixJQUEwQnJELEVBQUUsQ0FBQ3FELE1BQUgsQ0FBVUQsTUFBVixJQUFvQkYsUUFEbkQsQ0FqQndELENBa0JROztBQUNoRSxZQUFNaUksYUFBYSxHQUFHLENBQUMsaUNBQWlCbkwsRUFBakIsQ0FBRCxJQUF5Qiw4QkFBZ0JBLEVBQWhCLENBQS9DOztBQUVBLFVBQUltTCxhQUFhLElBQUksQ0FBQ1QsSUFBdEIsRUFBNEI7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFJLENBQUNPLFlBQUQsSUFBa0JBLFlBQVksSUFBSUgsMkJBQTJCLEtBQUssQ0FBdEUsRUFBMEU7QUFDdEUsWUFBRUEsMkJBQUY7QUFDSDs7QUFDRDtBQUNIOztBQUVELFVBQUlHLFlBQUosRUFBa0I7QUFDZDtBQUNIOztBQUVELFVBQUlELFFBQUosRUFBYztBQUNWLGVBQU8xRSxDQUFQO0FBQ0g7QUFDSjs7QUFFRCxXQUFPLElBQVA7QUFDSCxHQXJ1Q2tDOztBQXV1Q25DOzs7Ozs7Ozs7QUFTQTdLLEVBQUFBLHNCQUFzQixFQUFFLFVBQVMyUCxpQkFBVCxFQUE0QjtBQUNoRCxVQUFNL0wsTUFBTSxHQUFHaEQsaUNBQWdCQyxHQUFoQixFQUFmLENBRGdELENBRWhEOzs7QUFDQSxRQUFJK0MsTUFBTSxJQUFJLElBQWQsRUFBb0I7QUFDaEIsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTTZELFFBQVEsR0FBRzdELE1BQU0sQ0FBQzhELFdBQVAsQ0FBbUJDLE1BQXBDO0FBQ0EsV0FBTyxLQUFLakksS0FBTCxDQUFXL0IsV0FBWCxDQUF1QmlDLElBQXZCLENBQTRCZ1EsZ0JBQTVCLENBQTZDbkksUUFBN0MsRUFBdURrSSxpQkFBdkQsQ0FBUDtBQUNILEdBenZDa0M7QUEydkNuQzNILEVBQUFBLGNBQWMsRUFBRSxVQUFTMUosT0FBVCxFQUFrQnVSLE9BQWxCLEVBQTJCQyxlQUEzQixFQUE0QztBQUN4RCxVQUFNbkgsTUFBTSxHQUFHLEtBQUtqSixLQUFMLENBQVcvQixXQUFYLENBQXVCaUMsSUFBdkIsQ0FBNEIrSSxNQUEzQyxDQUR3RCxDQUd4RDtBQUNBOztBQUNBLFFBQUlySyxPQUFPLEtBQUssS0FBSytFLEtBQUwsQ0FBVzdDLGlCQUEzQixFQUE4QztBQUMxQztBQUNILEtBUHVELENBU3hEO0FBQ0E7OztBQUNBaEQsSUFBQUEsYUFBYSxDQUFDNEIsbUJBQWQsQ0FBa0N1SixNQUFsQyxJQUE0Q2tILE9BQTVDOztBQUVBLFFBQUlDLGVBQUosRUFBcUI7QUFDakI7QUFDSCxLQWZ1RCxDQWlCeEQ7QUFDQTtBQUNBOzs7QUFDQSxTQUFLL0ssUUFBTCxDQUFjO0FBQ1Z2RSxNQUFBQSxpQkFBaUIsRUFBRWxDO0FBRFQsS0FBZCxFQUVHLEtBQUtvQixLQUFMLENBQVdkLG1CQUZkO0FBR0gsR0FseENrQztBQW94Q25DeUcsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQU8sQ0FBQyxLQUFLaEMsS0FBTCxDQUFXcEQsTUFBWCxDQUFrQjhQLElBQWxCLENBQXdCL0osQ0FBRCxJQUFPO0FBQ2xDLGFBQU9BLENBQUMsQ0FBQ3VJLGdCQUFGLEVBQVA7QUFDSCxLQUZPLENBQVI7QUFHSCxHQTd4Q2tDOztBQSt4Q25DeUIsRUFBQUEsb0JBQW9CLENBQUMsR0FBR0MsSUFBSixFQUFVO0FBQzFCLFdBQU8sS0FBS3ZRLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJxUyxvQkFBdkIsQ0FBNEMsR0FBR0MsSUFBL0MsQ0FBUDtBQUNILEdBanlDa0M7O0FBbXlDbkNDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsWUFBWSxHQUFHdEQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlCQUFqQixDQUFyQjtBQUNBLFVBQU1zRCxNQUFNLEdBQUd2RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWYsQ0FGZSxDQUlmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLekosS0FBTCxDQUFXbEQsZUFBZixFQUFnQztBQUM1QixhQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLDZCQUFDLE1BQUQsT0FESixDQURKO0FBS0g7O0FBRUQsUUFBSSxLQUFLa0QsS0FBTCxDQUFXcEQsTUFBWCxDQUFrQnlFLE1BQWxCLElBQTRCLENBQTVCLElBQWlDLENBQUMsS0FBS3JCLEtBQUwsQ0FBV2hELGVBQTdDLElBQWdFLEtBQUtYLEtBQUwsQ0FBV1QsS0FBL0UsRUFBc0Y7QUFDbEYsYUFDSTtBQUFLLFFBQUEsU0FBUyxFQUFFLEtBQUtTLEtBQUwsQ0FBV1gsU0FBWCxHQUF1QjtBQUF2QyxTQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUFvQyxLQUFLVyxLQUFMLENBQVdULEtBQS9DLENBREosQ0FESjtBQUtILEtBN0JjLENBK0JmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFVBQU1vUixZQUFZLEdBQUcsQ0FBQyxLQUFLMUwsZUFBTCxDQUFxQmMsV0FBckIsQ0FBaUN2QixxQkFBY0UsUUFBL0MsQ0FBdEIsQ0F2Q2UsQ0F5Q2Y7QUFDQTs7QUFDQSxVQUFNMUQsaUJBQWlCLEdBQ25CLEtBQUsyQyxLQUFMLENBQVczQyxpQkFBWCxJQUNBLENBQUMsVUFBRCxFQUFhLFNBQWIsRUFBd0I0UCxRQUF4QixDQUFpQyxLQUFLak4sS0FBTCxDQUFXMUMsZUFBNUMsQ0FGSjtBQUlBLFVBQU1WLE1BQU0sR0FBRyxLQUFLb0QsS0FBTCxDQUFXakQsc0JBQVgsR0FDUCxLQUFLaUQsS0FBTCxDQUFXcEQsTUFBWCxDQUFrQnNRLEtBQWxCLENBQXdCLEtBQUtsTixLQUFMLENBQVdqRCxzQkFBbkMsQ0FETyxHQUVQLEtBQUtpRCxLQUFMLENBQVdwRCxNQUZuQjtBQUdBLFdBQ0ksNkJBQUMsWUFBRDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUt3QixhQURkO0FBRUksTUFBQSxJQUFJLEVBQUUsS0FBSy9CLEtBQUwsQ0FBVy9CLFdBQVgsQ0FBdUJpQyxJQUZqQztBQUdJLE1BQUEsZ0JBQWdCLEVBQUUsS0FBS0YsS0FBTCxDQUFXOFEsZ0JBSGpDO0FBSUksTUFBQSxNQUFNLEVBQUUsS0FBSzlRLEtBQUwsQ0FBV3ZCLE1BSnZCO0FBS0ksTUFBQSxjQUFjLEVBQUUsS0FBS2tGLEtBQUwsQ0FBVzVDLGNBTC9CO0FBTUksTUFBQSxpQkFBaUIsRUFBRUMsaUJBTnZCO0FBT0ksTUFBQSxNQUFNLEVBQUVULE1BUFo7QUFRSSxNQUFBLGtCQUFrQixFQUFFLEtBQUtQLEtBQUwsQ0FBV3RCLGtCQVJuQztBQVNJLE1BQUEsaUJBQWlCLEVBQUUsS0FBS2lGLEtBQUwsQ0FBVzdDLGlCQVRsQztBQVVJLE1BQUEsaUJBQWlCLEVBQUUsS0FBSzZDLEtBQUwsQ0FBVzlDLGlCQVZsQztBQVdJLE1BQUEsMEJBQTBCLEVBQUUsS0FBSzhDLEtBQUwsQ0FBV2hELGVBWDNDO0FBWUksTUFBQSxjQUFjLEVBQUUsS0FBS1gsS0FBTCxDQUFXakIsY0FaL0I7QUFhSSxNQUFBLGdCQUFnQixFQUFFLEtBQUtpQixLQUFMLENBQVczQixnQkFiakM7QUFjSSxNQUFBLFNBQVMsRUFBRTZDLGlDQUFnQkMsR0FBaEIsR0FBc0I2RyxXQUF0QixDQUFrQ0MsTUFkakQ7QUFlSSxNQUFBLFlBQVksRUFBRTBJLFlBZmxCO0FBZ0JJLE1BQUEsUUFBUSxFQUFFLEtBQUt0SyxtQkFoQm5CO0FBaUJJLE1BQUEsYUFBYSxFQUFFLEtBQUtYLHdCQWpCeEI7QUFrQkksTUFBQSxlQUFlLEVBQUUsS0FBS3RCLDBCQWxCMUI7QUFtQkksTUFBQSxZQUFZLEVBQUUsS0FBS1QsS0FBTCxDQUFXdEMsWUFuQjdCO0FBb0JJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS3NDLEtBQUwsQ0FBV25DLG9CQXBCckM7QUFxQkksTUFBQSxTQUFTLEVBQUUsS0FBS3hCLEtBQUwsQ0FBV1gsU0FyQjFCO0FBc0JJLE1BQUEsU0FBUyxFQUFFLEtBQUtXLEtBQUwsQ0FBV1YsU0F0QjFCO0FBdUJJLE1BQUEsY0FBYyxFQUFFLEtBQUtVLEtBQUwsQ0FBVytRLGNBdkIvQjtBQXdCSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtULG9CQXhCL0I7QUF5QkksTUFBQSxTQUFTLEVBQUUsS0FBSzNNLEtBQUwsQ0FBV29ELFNBekIxQjtBQTBCSSxNQUFBLGFBQWEsRUFBRSxLQUFLL0csS0FBTCxDQUFXUjtBQTFCOUIsTUFESjtBQThCSDtBQW4zQ2tDLENBQWpCLENBQXRCO2VBczNDZTFCLGEiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOS0yMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBSZWFjdERPTSBmcm9tIFwicmVhY3QtZG9tXCI7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7RXZlbnRUaW1lbGluZX0gZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0ICogYXMgTWF0cml4IGZyb20gXCJtYXRyaXgtanMtc2RrXCI7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0ICogYXMgT2JqZWN0VXRpbHMgZnJvbSBcIi4uLy4uL09iamVjdFV0aWxzXCI7XHJcbmltcG9ydCBVc2VyQWN0aXZpdHkgZnJvbSBcIi4uLy4uL1VzZXJBY3Rpdml0eVwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCBkaXMgZnJvbSBcIi4uLy4uL2Rpc3BhdGNoZXJcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgeyBLZXkgfSBmcm9tICcuLi8uLi9LZXlib2FyZCc7XHJcbmltcG9ydCBUaW1lciBmcm9tICcuLi8uLi91dGlscy9UaW1lcic7XHJcbmltcG9ydCBzaG91bGRIaWRlRXZlbnQgZnJvbSAnLi4vLi4vc2hvdWxkSGlkZUV2ZW50JztcclxuaW1wb3J0IEVkaXRvclN0YXRlVHJhbnNmZXIgZnJvbSAnLi4vLi4vdXRpbHMvRWRpdG9yU3RhdGVUcmFuc2Zlcic7XHJcbmltcG9ydCB7aGF2ZVRpbGVGb3JFdmVudH0gZnJvbSBcIi4uL3ZpZXdzL3Jvb21zL0V2ZW50VGlsZVwiO1xyXG5cclxuY29uc3QgUEFHSU5BVEVfU0laRSA9IDIwO1xyXG5jb25zdCBJTklUSUFMX1NJWkUgPSAyMDtcclxuY29uc3QgUkVBRF9SRUNFSVBUX0lOVEVSVkFMX01TID0gNTAwO1xyXG5cclxuY29uc3QgREVCVUcgPSBmYWxzZTtcclxuXHJcbmxldCBkZWJ1Z2xvZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChERUJVRykge1xyXG4gICAgLy8gdXNpbmcgYmluZCBtZWFucyB0aGF0IHdlIGdldCB0byBrZWVwIHVzZWZ1bCBsaW5lIG51bWJlcnMgaW4gdGhlIGNvbnNvbGVcclxuICAgIGRlYnVnbG9nID0gY29uc29sZS5sb2cuYmluZChjb25zb2xlKTtcclxufVxyXG5cclxuLypcclxuICogQ29tcG9uZW50IHdoaWNoIHNob3dzIHRoZSBldmVudCB0aW1lbGluZSBpbiBhIHJvb20gdmlldy5cclxuICpcclxuICogQWxzbyByZXNwb25zaWJsZSBmb3IgaGFuZGxpbmcgYW5kIHNlbmRpbmcgcmVhZCByZWNlaXB0cy5cclxuICovXHJcbmNvbnN0IFRpbWVsaW5lUGFuZWwgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnVGltZWxpbmVQYW5lbCcsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLy8gVGhlIGpzLXNkayBFdmVudFRpbWVsaW5lU2V0IG9iamVjdCBmb3IgdGhlIHRpbWVsaW5lIHNlcXVlbmNlIHdlIGFyZVxyXG4gICAgICAgIC8vIHJlcHJlc2VudGluZy4gIFRoaXMgbWF5IG9yIG1heSBub3QgaGF2ZSBhIHJvb20sIGRlcGVuZGluZyBvbiB3aGF0IGl0J3NcclxuICAgICAgICAvLyBhIHRpbWVsaW5lIHJlcHJlc2VudGluZy4gIElmIGl0IGhhcyBhIHJvb20sIHdlIG1haW50YWluIFJScyBldGMgZm9yXHJcbiAgICAgICAgLy8gdGhhdCByb29tLlxyXG4gICAgICAgIHRpbWVsaW5lU2V0OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIHNob3dSZWFkUmVjZWlwdHM6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIC8vIEVuYWJsZSBtYW5hZ2luZyBSUnMgYW5kIFJNcy4gVGhlc2UgcmVxdWlyZSB0aGUgdGltZWxpbmVTZXQgdG8gaGF2ZSBhIHJvb20uXHJcbiAgICAgICAgbWFuYWdlUmVhZFJlY2VpcHRzOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBtYW5hZ2VSZWFkTWFya2VyczogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIHRydWUgdG8gZ2l2ZSB0aGUgY29tcG9uZW50IGEgJ2Rpc3BsYXk6IG5vbmUnIHN0eWxlLlxyXG4gICAgICAgIGhpZGRlbjogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIElEIG9mIGFuIGV2ZW50IHRvIGhpZ2hsaWdodC4gSWYgdW5kZWZpbmVkLCBubyBldmVudCB3aWxsIGJlIGhpZ2hsaWdodGVkLlxyXG4gICAgICAgIC8vIHR5cGljYWxseSB0aGlzIHdpbGwgYmUgZWl0aGVyICdldmVudElkJyBvciB1bmRlZmluZWQuXHJcbiAgICAgICAgaGlnaGxpZ2h0ZWRFdmVudElkOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyBpZCBvZiBhbiBldmVudCB0byBqdW1wIHRvLiBJZiBub3QgZ2l2ZW4sIHdpbGwgZ28gdG8gdGhlIGVuZCBvZiB0aGVcclxuICAgICAgICAvLyBsaXZlIHRpbWVsaW5lLlxyXG4gICAgICAgIGV2ZW50SWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8vIHdoZXJlIHRvIHBvc2l0aW9uIHRoZSBldmVudCBnaXZlbiBieSBldmVudElkLCBpbiBwaXhlbHMgZnJvbSB0aGVcclxuICAgICAgICAvLyBib3R0b20gb2YgdGhlIHZpZXdwb3J0LiBJZiBub3QgZ2l2ZW4sIHdpbGwgdHJ5IHRvIHB1dCB0aGUgZXZlbnRcclxuICAgICAgICAvLyBoYWxmIHdheSBkb3duIHRoZSB2aWV3cG9ydC5cclxuICAgICAgICBldmVudFBpeGVsT2Zmc2V0OiBQcm9wVHlwZXMubnVtYmVyLFxyXG5cclxuICAgICAgICAvLyBTaG91bGQgd2Ugc2hvdyBVUkwgUHJldmlld3NcclxuICAgICAgICBzaG93VXJsUHJldmlldzogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIGNhbGxiYWNrIHdoaWNoIGlzIGNhbGxlZCB3aGVuIHRoZSBwYW5lbCBpcyBzY3JvbGxlZC5cclxuICAgICAgICBvblNjcm9sbDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIGNhbGxiYWNrIHdoaWNoIGlzIGNhbGxlZCB3aGVuIHRoZSByZWFkLXVwLXRvIG1hcmsgaXMgdXBkYXRlZC5cclxuICAgICAgICBvblJlYWRNYXJrZXJVcGRhdGVkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2sgd2hpY2ggaXMgY2FsbGVkIHdoZW4gd2Ugd2lzaCB0byBwYWdpbmF0ZSB0aGUgdGltZWxpbmVcclxuICAgICAgICAvLyB3aW5kb3cuXHJcbiAgICAgICAgb25QYWdpbmF0aW9uUmVxdWVzdDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIG1heGltdW0gbnVtYmVyIG9mIGV2ZW50cyB0byBzaG93IGluIGEgdGltZWxpbmVcclxuICAgICAgICB0aW1lbGluZUNhcDogUHJvcFR5cGVzLm51bWJlcixcclxuXHJcbiAgICAgICAgLy8gY2xhc3NuYW1lIHRvIHVzZSBmb3IgdGhlIG1lc3NhZ2VwYW5lbFxyXG4gICAgICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLy8gc2hhcGUgcHJvcGVydHkgdG8gYmUgcGFzc2VkIHRvIEV2ZW50VGlsZXNcclxuICAgICAgICB0aWxlU2hhcGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8vIHBsYWNlaG9sZGVyIHRleHQgdG8gdXNlIGlmIHRoZSB0aW1lbGluZSBpcyBlbXB0eVxyXG4gICAgICAgIGVtcHR5OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyB3aGV0aGVyIHRvIHNob3cgcmVhY3Rpb25zIGZvciBhbiBldmVudFxyXG4gICAgICAgIHNob3dSZWFjdGlvbnM6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBzdGF0aWNzOiB7XHJcbiAgICAgICAgLy8gYSBtYXAgZnJvbSByb29tIGlkIHRvIHJlYWQgbWFya2VyIGV2ZW50IHRpbWVzdGFtcFxyXG4gICAgICAgIHJvb21SZWFkTWFya2VyVHNNYXA6IHt9LFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIC8vIEJ5IGRlZmF1bHQsIGRpc2FibGUgdGhlIHRpbWVsaW5lQ2FwIGluIGZhdm91ciBvZiB1bnBhZ2luYXRpbmcgYmFzZWQgb25cclxuICAgICAgICAgICAgLy8gZXZlbnQgdGlsZSBoZWlnaHRzLiAoU2VlIF91bnBhZ2luYXRlRXZlbnRzKVxyXG4gICAgICAgICAgICB0aW1lbGluZUNhcDogTnVtYmVyLk1BWF9WQUxVRSxcclxuICAgICAgICAgICAgY2xhc3NOYW1lOiAnbXhfUm9vbVZpZXdfbWVzc2FnZVBhbmVsJyxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIFhYWDogd2UgY291bGQgdHJhY2sgUk0gcGVyIFRpbWVsaW5lU2V0IHJhdGhlciB0aGFuIHBlciBSb29tLlxyXG4gICAgICAgIC8vIGJ1dCBmb3Igbm93IHdlIGp1c3QgZG8gaXQgcGVyIHJvb20gZm9yIHNpbXBsaWNpdHkuXHJcbiAgICAgICAgbGV0IGluaXRpYWxSZWFkTWFya2VyID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5tYW5hZ2VSZWFkTWFya2Vycykge1xyXG4gICAgICAgICAgICBjb25zdCByZWFkbWFya2VyID0gdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tLmdldEFjY291bnREYXRhKCdtLmZ1bGx5X3JlYWQnKTtcclxuICAgICAgICAgICAgaWYgKHJlYWRtYXJrZXIpIHtcclxuICAgICAgICAgICAgICAgIGluaXRpYWxSZWFkTWFya2VyID0gcmVhZG1hcmtlci5nZXRDb250ZW50KCkuZXZlbnRfaWQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpbml0aWFsUmVhZE1hcmtlciA9IHRoaXMuX2dldEN1cnJlbnRSZWFkUmVjZWlwdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBldmVudHM6IFtdLFxyXG4gICAgICAgICAgICBsaXZlRXZlbnRzOiBbXSxcclxuICAgICAgICAgICAgdGltZWxpbmVMb2FkaW5nOiB0cnVlLCAvLyB0cmFjayB3aGV0aGVyIG91ciByb29tIHRpbWVsaW5lIGlzIGxvYWRpbmdcclxuXHJcbiAgICAgICAgICAgIC8vIHRoZSBpbmRleCBvZiB0aGUgZmlyc3QgZXZlbnQgdGhhdCBpcyB0byBiZSBzaG93blxyXG4gICAgICAgICAgICBmaXJzdFZpc2libGVFdmVudEluZGV4OiAwLFxyXG5cclxuICAgICAgICAgICAgLy8gY2FuQmFja1BhZ2luYXRlID09IGZhbHNlIG1heSBtZWFuOlxyXG4gICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAvLyAqIHdlIGhhdmVuJ3QgKHN1Y2Nlc3NmdWxseSkgbG9hZGVkIHRoZSB0aW1lbGluZSB5ZXQsIG9yOlxyXG4gICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAvLyAqIHdlIGhhdmUgZ290IHRvIHRoZSBwb2ludCB3aGVyZSB0aGUgcm9vbSB3YXMgY3JlYXRlZCwgb3I6XHJcbiAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgIC8vICogdGhlIHNlcnZlciBpbmRpY2F0ZWQgdGhhdCB0aGVyZSB3ZXJlIG5vIG1vcmUgdmlzaWJsZSBldmVudHNcclxuICAgICAgICAgICAgLy8gIChub3JtYWxseSBpbXBseWluZyB3ZSBnb3QgdG8gdGhlIHN0YXJ0IG9mIHRoZSByb29tKSwgb3I6XHJcbiAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgIC8vICogd2UgZ2F2ZSB1cCBhc2tpbmcgdGhlIHNlcnZlciBmb3IgbW9yZSBldmVudHNcclxuICAgICAgICAgICAgY2FuQmFja1BhZ2luYXRlOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgIC8vIGNhbkZvcndhcmRQYWdpbmF0ZSA9PSBmYWxzZSBtYXkgbWVhbjpcclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gKiB3ZSBoYXZlbid0IChzdWNjZXNzZnVsbHkpIGxvYWRlZCB0aGUgdGltZWxpbmUgeWV0XHJcbiAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgIC8vICogd2UgaGF2ZSBnb3QgdG8gdGhlIGVuZCBvZiB0aW1lIGFuZCBhcmUgbm93IHRyYWNraW5nIHRoZSBsaXZlXHJcbiAgICAgICAgICAgIC8vICAgdGltZWxpbmUsIG9yOlxyXG4gICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAvLyAqIHRoZSBzZXJ2ZXIgaW5kaWNhdGVkIHRoYXQgdGhlcmUgd2VyZSBubyBtb3JlIHZpc2libGUgZXZlbnRzXHJcbiAgICAgICAgICAgIC8vICAgKG5vdCBzdXJlIGlmIHRoaXMgZXZlciBoYXBwZW5zIHdoZW4gd2UncmUgbm90IGF0IHRoZSBsaXZlXHJcbiAgICAgICAgICAgIC8vICAgdGltZWxpbmUpLCBvcjpcclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gKiB3ZSBhcmUgbG9va2luZyBhdCBzb21lIGhpc3RvcmljYWwgcG9pbnQsIGJ1dCBnYXZlIHVwIGFza2luZ1xyXG4gICAgICAgICAgICAvLyAgIHRoZSBzZXJ2ZXIgZm9yIG1vcmUgZXZlbnRzXHJcbiAgICAgICAgICAgIGNhbkZvcndhcmRQYWdpbmF0ZTogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICAvLyBzdGFydCB3aXRoIHRoZSByZWFkLW1hcmtlciB2aXNpYmxlLCBzbyB0aGF0IHdlIHNlZSBpdHMgYW5pbWF0ZWRcclxuICAgICAgICAgICAgLy8gZGlzYXBwZWFyYW5jZSB3aGVuIHN3aXRjaGluZyBpbnRvIHRoZSByb29tLlxyXG4gICAgICAgICAgICByZWFkTWFya2VyVmlzaWJsZTogdHJ1ZSxcclxuXHJcbiAgICAgICAgICAgIHJlYWRNYXJrZXJFdmVudElkOiBpbml0aWFsUmVhZE1hcmtlcixcclxuXHJcbiAgICAgICAgICAgIGJhY2tQYWdpbmF0aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgZm9yd2FyZFBhZ2luYXRpbmc6IGZhbHNlLFxyXG5cclxuICAgICAgICAgICAgLy8gY2FjaGUgb2YgbWF0cml4Q2xpZW50LmdldFN5bmNTdGF0ZSgpIChidXQgZnJvbSB0aGUgJ3N5bmMnIGV2ZW50KVxyXG4gICAgICAgICAgICBjbGllbnRTeW5jU3RhdGU6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRTeW5jU3RhdGUoKSxcclxuXHJcbiAgICAgICAgICAgIC8vIHNob3VsZCB0aGUgZXZlbnQgdGlsZXMgaGF2ZSB0d2VsdmUgaG91ciB0aW1lc1xyXG4gICAgICAgICAgICBpc1R3ZWx2ZUhvdXI6IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJzaG93VHdlbHZlSG91clRpbWVzdGFtcHNcIiksXHJcblxyXG4gICAgICAgICAgICAvLyBhbHdheXMgc2hvdyB0aW1lc3RhbXBzIG9uIGV2ZW50IHRpbGVzP1xyXG4gICAgICAgICAgICBhbHdheXNTaG93VGltZXN0YW1wczogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImFsd2F5c1Nob3dUaW1lc3RhbXBzXCIpLFxyXG5cclxuICAgICAgICAgICAgLy8gaG93IGxvbmcgdG8gc2hvdyB0aGUgUk0gZm9yIHdoZW4gaXQncyB2aXNpYmxlIGluIHRoZSB3aW5kb3dcclxuICAgICAgICAgICAgcmVhZE1hcmtlckluVmlld1RocmVzaG9sZE1zOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwicmVhZE1hcmtlckluVmlld1RocmVzaG9sZE1zXCIpLFxyXG5cclxuICAgICAgICAgICAgLy8gaG93IGxvbmcgdG8gc2hvdyB0aGUgUk0gZm9yIHdoZW4gaXQncyBzY3JvbGxlZCBvZmYtc2NyZWVuXHJcbiAgICAgICAgICAgIHJlYWRNYXJrZXJPdXRPZlZpZXdUaHJlc2hvbGRNczogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInJlYWRNYXJrZXJPdXRPZlZpZXdUaHJlc2hvbGRNc1wiKSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGRlYnVnbG9nKFwiVGltZWxpbmVQYW5lbDogbW91bnRpbmdcIik7XHJcblxyXG4gICAgICAgIHRoaXMubGFzdFJSU2VudEV2ZW50SWQgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgdGhpcy5sYXN0Uk1TZW50RXZlbnRJZCA9IHVuZGVmaW5lZDtcclxuXHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsID0gY3JlYXRlUmVmKCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm1hbmFnZVJlYWRSZWNlaXB0cykge1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVJlYWRSZWNlaXB0T25Vc2VyQWN0aXZpdHkoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVSZWFkTWFya2VyT25Vc2VyQWN0aXZpdHkoKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5vbkFjdGlvbik7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS50aW1lbGluZVwiLCB0aGlzLm9uUm9vbVRpbWVsaW5lKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tLnRpbWVsaW5lUmVzZXRcIiwgdGhpcy5vblJvb21UaW1lbGluZVJlc2V0KTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tLnJlZGFjdGlvblwiLCB0aGlzLm9uUm9vbVJlZGFjdGlvbik7XHJcbiAgICAgICAgLy8gc2FtZSBldmVudCBoYW5kbGVyIGFzIFJvb20ucmVkYWN0aW9uIGFzIGZvciBib3RoIHdlIGp1c3QgZG8gZm9yY2VVcGRhdGVcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tLnJlZGFjdGlvbkNhbmNlbGxlZFwiLCB0aGlzLm9uUm9vbVJlZGFjdGlvbik7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS5sb2NhbEVjaG9VcGRhdGVkXCIsIHRoaXMub25Mb2NhbEVjaG9VcGRhdGVkKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tLmFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiRXZlbnQuZGVjcnlwdGVkXCIsIHRoaXMub25FdmVudERlY3J5cHRlZCk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiRXZlbnQucmVwbGFjZWRcIiwgdGhpcy5vbkV2ZW50UmVwbGFjZWQpO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbihcInN5bmNcIiwgdGhpcy5vblN5bmMpO1xyXG5cclxuICAgICAgICB0aGlzLl9pbml0VGltZWxpbmUodGhpcy5wcm9wcyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV3UHJvcHMpIHtcclxuICAgICAgICBpZiAobmV3UHJvcHMudGltZWxpbmVTZXQgIT09IHRoaXMucHJvcHMudGltZWxpbmVTZXQpIHtcclxuICAgICAgICAgICAgLy8gdGhyb3cgbmV3IEVycm9yKFwiY2hhbmdpbmcgdGltZWxpbmVTZXQgb24gYSBUaW1lbGluZVBhbmVsIGlzIG5vdCBzdXBwb3J0ZWRcIik7XHJcblxyXG4gICAgICAgICAgICAvLyByZWdyZXR0YWJseSwgdGhpcyBkb2VzIGhhcHBlbjsgaW4gcGFydGljdWxhciwgd2hlbiBqb2luaW5nIGFcclxuICAgICAgICAgICAgLy8gcm9vbSB3aXRoIC9qb2luLiBJbiB0aGF0IGNhc2UsIHRoZXJlIGFyZSB0d28gUm9vbXMgaW5cclxuICAgICAgICAgICAgLy8gY2lyY3VsYXRpb24gLSBvbmUgd2hpY2ggaXMgY3JlYXRlZCBieSB0aGUgTWF0cml4Q2xpZW50LmpvaW5Sb29tXHJcbiAgICAgICAgICAgIC8vIGNhbGwgYW5kIHVzZWQgdG8gY3JlYXRlIHRoZSBSb29tVmlldywgYW5kIGEgc2Vjb25kIHdoaWNoIGlzXHJcbiAgICAgICAgICAgIC8vIGNyZWF0ZWQgYnkgdGhlIHN5bmMgbG9vcCBvbmNlIHRoZSByb29tIGNvbWVzIGJhY2sgZG93biB0aGUgL3N5bmNcclxuICAgICAgICAgICAgLy8gcGlwZS4gT25jZSB0aGUgbGF0dGVyIGhhcHBlbnMsIG91ciByb29tIGlzIHJlcGxhY2VkIHdpdGggdGhlIG5ldyBvbmUuXHJcbiAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgIC8vIGZvciBub3csIGp1c3Qgd2FybiBhYm91dCB0aGlzLiBCdXQgd2UncmUgZ29pbmcgdG8gZW5kIHVwIHBhZ2luYXRpbmdcclxuICAgICAgICAgICAgLy8gYm90aCByb29tcyBzZXBhcmF0ZWx5LCBhbmQgaXQncyBhbGwgYmFkLlxyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJSZXBsYWNpbmcgdGltZWxpbmVTZXQgb24gYSBUaW1lbGluZVBhbmVsIC0gY29uZnVzaW9uIG1heSBlbnN1ZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChuZXdQcm9wcy5ldmVudElkICE9IHRoaXMucHJvcHMuZXZlbnRJZCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlRpbWVsaW5lUGFuZWwgc3dpdGNoaW5nIHRvIGV2ZW50SWQgXCIgKyBuZXdQcm9wcy5ldmVudElkICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCIgKHdhcyBcIiArIHRoaXMucHJvcHMuZXZlbnRJZCArIFwiKVwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2luaXRUaW1lbGluZShuZXdQcm9wcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IGZ1bmN0aW9uKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XHJcbiAgICAgICAgaWYgKCFPYmplY3RVdGlscy5zaGFsbG93RXF1YWwodGhpcy5wcm9wcywgbmV4dFByb3BzKSkge1xyXG4gICAgICAgICAgICBpZiAoREVCVUcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXAoXCJUaW1lbGluZS5zaG91bGRDb21wb25lbnRVcGRhdGU6IHByb3BzIGNoYW5nZVwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicHJvcHMgYmVmb3JlOlwiLCB0aGlzLnByb3BzKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicHJvcHMgYWZ0ZXI6XCIsIG5leHRQcm9wcyk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwRW5kKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIU9iamVjdFV0aWxzLnNoYWxsb3dFcXVhbCh0aGlzLnN0YXRlLCBuZXh0U3RhdGUpKSB7XHJcbiAgICAgICAgICAgIGlmIChERUJVRykge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5ncm91cChcIlRpbWVsaW5lLnNob3VsZENvbXBvbmVudFVwZGF0ZTogc3RhdGUgY2hhbmdlXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJzdGF0ZSBiZWZvcmU6XCIsIHRoaXMuc3RhdGUpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJzdGF0ZSBhZnRlcjpcIiwgbmV4dFN0YXRlKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHNldCBhIGJvb2xlYW4gdG8gc2F5IHdlJ3ZlIGJlZW4gdW5tb3VudGVkLCB3aGljaCBhbnkgcGVuZGluZ1xyXG4gICAgICAgIC8vIHByb21pc2VzIGNhbiB1c2UgdG8gdGhyb3cgYXdheSB0aGVpciByZXN1bHRzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gKFdlIGNvdWxkIHVzZSBpc01vdW50ZWQsIGJ1dCBmYWNlYm9vayBoYXZlIGRlcHJlY2F0ZWQgdGhhdC4pXHJcbiAgICAgICAgdGhpcy51bm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLl9yZWFkUmVjZWlwdEFjdGl2aXR5VGltZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVhZFJlY2VpcHRBY3Rpdml0eVRpbWVyLmFib3J0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0QWN0aXZpdHlUaW1lciA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9yZWFkTWFya2VyQWN0aXZpdHlUaW1lcikge1xyXG4gICAgICAgICAgICB0aGlzLl9yZWFkTWFya2VyQWN0aXZpdHlUaW1lci5hYm9ydCgpO1xyXG4gICAgICAgICAgICB0aGlzLl9yZWFkTWFya2VyQWN0aXZpdHlUaW1lciA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLmRpc3BhdGNoZXJSZWYpO1xyXG5cclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaWVudCkge1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLnRpbWVsaW5lXCIsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLnRpbWVsaW5lUmVzZXRcIiwgdGhpcy5vblJvb21UaW1lbGluZVJlc2V0KTtcclxuICAgICAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKFwiUm9vbS5yZWRhY3Rpb25cIiwgdGhpcy5vblJvb21SZWRhY3Rpb24pO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLnJlZGFjdGlvbkNhbmNlbGxlZFwiLCB0aGlzLm9uUm9vbVJlZGFjdGlvbik7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb20ucmVjZWlwdFwiLCB0aGlzLm9uUm9vbVJlY2VpcHQpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLmxvY2FsRWNob1VwZGF0ZWRcIiwgdGhpcy5vbkxvY2FsRWNob1VwZGF0ZWQpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLmFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIkV2ZW50LmRlY3J5cHRlZFwiLCB0aGlzLm9uRXZlbnREZWNyeXB0ZWQpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJFdmVudC5yZXBsYWNlZFwiLCB0aGlzLm9uRXZlbnRSZXBsYWNlZCk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcInN5bmNcIiwgdGhpcy5vblN5bmMpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25NZXNzYWdlTGlzdFVuZmlsbFJlcXVlc3Q6IGZ1bmN0aW9uKGJhY2t3YXJkcywgc2Nyb2xsVG9rZW4pIHtcclxuICAgICAgICAvLyBJZiBiYWNrd2FyZHMsIHVucGFnaW5hdGUgZnJvbSB0aGUgYmFjayAoaS5lLiB0aGUgc3RhcnQgb2YgdGhlIHRpbWVsaW5lKVxyXG4gICAgICAgIGNvbnN0IGRpciA9IGJhY2t3YXJkcyA/IEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTIDogRXZlbnRUaW1lbGluZS5GT1JXQVJEUztcclxuICAgICAgICBkZWJ1Z2xvZyhcIlRpbWVsaW5lUGFuZWw6IHVucGFnaW5hdGluZyBldmVudHMgaW4gZGlyZWN0aW9uXCIsIGRpcik7XHJcblxyXG4gICAgICAgIC8vIEFsbCB0aWxlcyBhcmUgaW5zZXJ0ZWQgYnkgTWVzc2FnZVBhbmVsIHRvIGhhdmUgYSBzY3JvbGxUb2tlbiA9PT0gZXZlbnRJZCwgYW5kXHJcbiAgICAgICAgLy8gdGhpcyBwYXJ0aWN1bGFyIGV2ZW50IHNob3VsZCBiZSB0aGUgZmlyc3Qgb3IgbGFzdCB0byBiZSB1bnBhZ2luYXRlZC5cclxuICAgICAgICBjb25zdCBldmVudElkID0gc2Nyb2xsVG9rZW47XHJcblxyXG4gICAgICAgIGNvbnN0IG1hcmtlciA9IHRoaXMuc3RhdGUuZXZlbnRzLmZpbmRJbmRleChcclxuICAgICAgICAgICAgKGV2KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZXYuZ2V0SWQoKSA9PT0gZXZlbnRJZDtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBjb3VudCA9IGJhY2t3YXJkcyA/IG1hcmtlciArIDEgOiB0aGlzLnN0YXRlLmV2ZW50cy5sZW5ndGggLSBtYXJrZXI7XHJcblxyXG4gICAgICAgIGlmIChjb3VudCA+IDApIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJUaW1lbGluZVBhbmVsOiBVbnBhZ2luYXRpbmdcIiwgY291bnQsIFwiaW4gZGlyZWN0aW9uXCIsIGRpcik7XHJcbiAgICAgICAgICAgIHRoaXMuX3RpbWVsaW5lV2luZG93LnVucGFnaW5hdGUoY291bnQsIGJhY2t3YXJkcyk7XHJcblxyXG4gICAgICAgICAgICAvLyBXZSBjYW4gbm93IHBhZ2luYXRlIGluIHRoZSB1bnBhZ2luYXRlZCBkaXJlY3Rpb25cclxuICAgICAgICAgICAgY29uc3QgY2FuUGFnaW5hdGVLZXkgPSAoYmFja3dhcmRzKSA/ICdjYW5CYWNrUGFnaW5hdGUnIDogJ2NhbkZvcndhcmRQYWdpbmF0ZSc7XHJcbiAgICAgICAgICAgIGNvbnN0IHsgZXZlbnRzLCBsaXZlRXZlbnRzLCBmaXJzdFZpc2libGVFdmVudEluZGV4IH0gPSB0aGlzLl9nZXRFdmVudHMoKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBbY2FuUGFnaW5hdGVLZXldOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZXZlbnRzLFxyXG4gICAgICAgICAgICAgICAgbGl2ZUV2ZW50cyxcclxuICAgICAgICAgICAgICAgIGZpcnN0VmlzaWJsZUV2ZW50SW5kZXgsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25QYWdpbmF0aW9uUmVxdWVzdCh0aW1lbGluZVdpbmRvdywgZGlyZWN0aW9uLCBzaXplKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25QYWdpbmF0aW9uUmVxdWVzdCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5vblBhZ2luYXRpb25SZXF1ZXN0KHRpbWVsaW5lV2luZG93LCBkaXJlY3Rpb24sIHNpemUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aW1lbGluZVdpbmRvdy5wYWdpbmF0ZShkaXJlY3Rpb24sIHNpemUpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8gc2V0IG9mZiBhIHBhZ2luYXRpb24gcmVxdWVzdC5cclxuICAgIG9uTWVzc2FnZUxpc3RGaWxsUmVxdWVzdDogZnVuY3Rpb24oYmFja3dhcmRzKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9zaG91bGRQYWdpbmF0ZSgpKSByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuXHJcbiAgICAgICAgY29uc3QgZGlyID0gYmFja3dhcmRzID8gRXZlbnRUaW1lbGluZS5CQUNLV0FSRFMgOiBFdmVudFRpbWVsaW5lLkZPUldBUkRTO1xyXG4gICAgICAgIGNvbnN0IGNhblBhZ2luYXRlS2V5ID0gYmFja3dhcmRzID8gJ2NhbkJhY2tQYWdpbmF0ZScgOiAnY2FuRm9yd2FyZFBhZ2luYXRlJztcclxuICAgICAgICBjb25zdCBwYWdpbmF0aW5nS2V5ID0gYmFja3dhcmRzID8gJ2JhY2tQYWdpbmF0aW5nJyA6ICdmb3J3YXJkUGFnaW5hdGluZyc7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZVtjYW5QYWdpbmF0ZUtleV0pIHtcclxuICAgICAgICAgICAgZGVidWdsb2coXCJUaW1lbGluZVBhbmVsOiBoYXZlIGdpdmVuIHVwXCIsIGRpciwgXCJwYWdpbmF0aW5nIHRoaXMgdGltZWxpbmVcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLl90aW1lbGluZVdpbmRvdy5jYW5QYWdpbmF0ZShkaXIpKSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwiVGltZWxpbmVQYW5lbDogY2FuJ3RcIiwgZGlyLCBcInBhZ2luYXRlIGFueSBmdXJ0aGVyXCIpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtbY2FuUGFnaW5hdGVLZXldOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChiYWNrd2FyZHMgJiYgdGhpcy5zdGF0ZS5maXJzdFZpc2libGVFdmVudEluZGV4ICE9PSAwKSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwiVGltZWxpbmVQYW5lbDogd29uJ3RcIiwgZGlyLCBcInBhZ2luYXRlIHBhc3QgZmlyc3QgdmlzaWJsZSBldmVudFwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShmYWxzZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkZWJ1Z2xvZyhcIlRpbWVsaW5lUGFuZWw6IEluaXRpYXRpbmcgcGFnaW5hdGU7IGJhY2t3YXJkczpcIitiYWNrd2FyZHMpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1twYWdpbmF0aW5nS2V5XTogdHJ1ZX0pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5vblBhZ2luYXRpb25SZXF1ZXN0KHRoaXMuX3RpbWVsaW5lV2luZG93LCBkaXIsIFBBR0lOQVRFX1NJWkUpLnRoZW4oKHIpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSB7IHJldHVybjsgfVxyXG5cclxuICAgICAgICAgICAgZGVidWdsb2coXCJUaW1lbGluZVBhbmVsOiBwYWdpbmF0ZSBjb21wbGV0ZSBiYWNrd2FyZHM6XCIrYmFja3dhcmRzK1wiOyBzdWNjZXNzOlwiK3IpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgeyBldmVudHMsIGxpdmVFdmVudHMsIGZpcnN0VmlzaWJsZUV2ZW50SW5kZXggfSA9IHRoaXMuX2dldEV2ZW50cygpO1xyXG4gICAgICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHtcclxuICAgICAgICAgICAgICAgIFtwYWdpbmF0aW5nS2V5XTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBbY2FuUGFnaW5hdGVLZXldOiByLFxyXG4gICAgICAgICAgICAgICAgZXZlbnRzLFxyXG4gICAgICAgICAgICAgICAgbGl2ZUV2ZW50cyxcclxuICAgICAgICAgICAgICAgIGZpcnN0VmlzaWJsZUV2ZW50SW5kZXgsXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAvLyBtb3ZpbmcgdGhlIHdpbmRvdyBpbiB0aGlzIGRpcmVjdGlvbiBtYXkgbWVhbiB0aGF0IHdlIGNhbiBub3dcclxuICAgICAgICAgICAgLy8gcGFnaW5hdGUgaW4gdGhlIG90aGVyIHdoZXJlIHdlIHByZXZpb3VzbHkgY291bGQgbm90LlxyXG4gICAgICAgICAgICBjb25zdCBvdGhlckRpcmVjdGlvbiA9IGJhY2t3YXJkcyA/IEV2ZW50VGltZWxpbmUuRk9SV0FSRFMgOiBFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUztcclxuICAgICAgICAgICAgY29uc3QgY2FuUGFnaW5hdGVPdGhlcldheUtleSA9IGJhY2t3YXJkcyA/ICdjYW5Gb3J3YXJkUGFnaW5hdGUnIDogJ2NhbkJhY2tQYWdpbmF0ZSc7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZVtjYW5QYWdpbmF0ZU90aGVyV2F5S2V5XSAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RpbWVsaW5lV2luZG93LmNhblBhZ2luYXRlKG90aGVyRGlyZWN0aW9uKSkge1xyXG4gICAgICAgICAgICAgICAgZGVidWdsb2coJ1RpbWVsaW5lUGFuZWw6IGNhbiBub3cnLCBvdGhlckRpcmVjdGlvbiwgJ3BhZ2luYXRlIGFnYWluJyk7XHJcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZVtjYW5QYWdpbmF0ZU90aGVyV2F5S2V5XSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIERvbid0IHJlc29sdmUgdW50aWwgdGhlIHNldFN0YXRlIGhhcyBjb21wbGV0ZWQ6IHdlIG5lZWQgdG8gbGV0XHJcbiAgICAgICAgICAgIC8vIHRoZSBjb21wb25lbnQgdXBkYXRlIGJlZm9yZSB3ZSBjb25zaWRlciB0aGUgcGFnaW5hdGlvbiBjb21wbGV0ZWQsXHJcbiAgICAgICAgICAgIC8vIG90aGVyd2lzZSB3ZSdsbCBlbmQgdXAgcGFnaW5hdGluZyBpbiBhbGwgdGhlIGhpc3RvcnkgdGhlIGpzLXNka1xyXG4gICAgICAgICAgICAvLyBoYXMgaW4gbWVtb3J5IGJlY2F1c2Ugd2UgbmV2ZXIgZ2F2ZSB0aGUgY29tcG9uZW50IGEgY2hhbmNlIHRvIHNjcm9sbFxyXG4gICAgICAgICAgICAvLyBpdHNlbGYgaW50byB0aGUgcmlnaHQgcGxhY2VcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKG5ld1N0YXRlLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgY2FuIGNvbnRpbnVlIHBhZ2luYXRpbmcgaW4gdGhlIGdpdmVuIGRpcmVjdGlvbiBpZjpcclxuICAgICAgICAgICAgICAgICAgICAvLyAtIF90aW1lbGluZVdpbmRvdy5wYWdpbmF0ZSBzYXlzIHdlIGNhblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIC0gd2UncmUgcGFnaW5hdGluZyBmb3J3YXJkcywgb3Igd2Ugd29uJ3QgYmUgdHJ5aW5nIHRvXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICBwYWdpbmF0ZSBiYWNrd2FyZHMgcGFzdCB0aGUgZmlyc3QgdmlzaWJsZSBldmVudFxyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUociAmJiAoIWJhY2t3YXJkcyB8fCBmaXJzdFZpc2libGVFdmVudEluZGV4ID09PSAwKSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTWVzc2FnZUxpc3RTY3JvbGw6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vblNjcm9sbCkge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uU2Nyb2xsKGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHtcclxuICAgICAgICAgICAgY29uc3Qgcm1Qb3NpdGlvbiA9IHRoaXMuZ2V0UmVhZE1hcmtlclBvc2l0aW9uKCk7XHJcbiAgICAgICAgICAgIC8vIHdlIGhpZGUgdGhlIHJlYWQgbWFya2VyIHdoZW4gaXQgZmlyc3QgY29tZXMgb250byB0aGUgc2NyZWVuLCBidXQgaWZcclxuICAgICAgICAgICAgLy8gaXQgZ29lcyBiYWNrIG9mZiB0aGUgdG9wIG9mIHRoZSBzY3JlZW4gKHByZXN1bWFibHkgYmVjYXVzZSB0aGUgdXNlclxyXG4gICAgICAgICAgICAvLyBjbGlja3Mgb24gdGhlICdqdW1wIHRvIGJvdHRvbScgYnV0dG9uKSwgd2UgbmVlZCB0byByZS1lbmFibGUgaXQuXHJcbiAgICAgICAgICAgIGlmIChybVBvc2l0aW9uIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cmVhZE1hcmtlclZpc2libGU6IHRydWV9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gaWYgcmVhZCBtYXJrZXIgcG9zaXRpb24gZ29lcyBiZXR3ZWVuIDAgYW5kIC0xLzEsXHJcbiAgICAgICAgICAgIC8vIChhbmQgdXNlciBpcyBhY3RpdmUpLCBzd2l0Y2ggdGltZW91dFxyXG4gICAgICAgICAgICBjb25zdCB0aW1lb3V0ID0gdGhpcy5fcmVhZE1hcmtlclRpbWVvdXQocm1Qb3NpdGlvbik7XHJcbiAgICAgICAgICAgIC8vIE5PLU9QIHdoZW4gdGltZW91dCBhbHJlYWR5IGhhcyBzZXQgdG8gdGhlIGdpdmVuIHZhbHVlXHJcbiAgICAgICAgICAgIHRoaXMuX3JlYWRNYXJrZXJBY3Rpdml0eVRpbWVyLmNoYW5nZVRpbWVvdXQodGltZW91dCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbkFjdGlvbjogZnVuY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIGlmIChwYXlsb2FkLmFjdGlvbiA9PT0gJ2lnbm9yZV9zdGF0ZV9jaGFuZ2VkJykge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChwYXlsb2FkLmFjdGlvbiA9PT0gXCJlZGl0X2V2ZW50XCIpIHtcclxuICAgICAgICAgICAgY29uc3QgZWRpdFN0YXRlID0gcGF5bG9hZC5ldmVudCA/IG5ldyBFZGl0b3JTdGF0ZVRyYW5zZmVyKHBheWxvYWQuZXZlbnQpIDogbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZWRpdFN0YXRlfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHBheWxvYWQuZXZlbnQgJiYgdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5zY3JvbGxUb0V2ZW50SWZOZWVkZWQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBheWxvYWQuZXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVRpbWVsaW5lOiBmdW5jdGlvbihldiwgcm9vbSwgdG9TdGFydE9mVGltZWxpbmUsIHJlbW92ZWQsIGRhdGEpIHtcclxuICAgICAgICAvLyBpZ25vcmUgZXZlbnRzIGZvciBvdGhlciB0aW1lbGluZSBzZXRzXHJcbiAgICAgICAgaWYgKGRhdGEudGltZWxpbmUuZ2V0VGltZWxpbmVTZXQoKSAhPT0gdGhpcy5wcm9wcy50aW1lbGluZVNldCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBpZ25vcmUgYW55dGhpbmcgYnV0IHJlYWwtdGltZSB1cGRhdGVzIGF0IHRoZSBlbmQgb2YgdGhlIHJvb206XHJcbiAgICAgICAgLy8gdXBkYXRlcyBmcm9tIHBhZ2luYXRpb24gd2lsbCBoYXBwZW4gd2hlbiB0aGUgcGFnaW5hdGUgY29tcGxldGVzLlxyXG4gICAgICAgIGlmICh0b1N0YXJ0T2ZUaW1lbGluZSB8fCAhZGF0YSB8fCAhZGF0YS5saXZlRXZlbnQpIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50LmdldFNjcm9sbFN0YXRlKCkuc3R1Y2tBdEJvdHRvbSkge1xyXG4gICAgICAgICAgICAvLyB3ZSB3b24ndCBsb2FkIHRoaXMgZXZlbnQgbm93LCBiZWNhdXNlIHdlIGRvbid0IHdhbnQgdG8gcHVzaCBhbnlcclxuICAgICAgICAgICAgLy8gZXZlbnRzIG9mZiB0aGUgb3RoZXIgZW5kIG9mIHRoZSB0aW1lbGluZS4gQnV0IHdlIG5lZWQgdG8gbm90ZVxyXG4gICAgICAgICAgICAvLyB0aGF0IHdlIGNhbiBub3cgcGFnaW5hdGUuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2NhbkZvcndhcmRQYWdpbmF0ZTogdHJ1ZX0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB0ZWxsIHRoZSB0aW1lbGluZSB3aW5kb3cgdG8gdHJ5IHRvIGFkdmFuY2UgaXRzZWxmLCBidXQgbm90IHRvIG1ha2VcclxuICAgICAgICAvLyBhbiBodHRwIHJlcXVlc3QgdG8gZG8gc28uXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyB3ZSBkZWxpYmVyYXRlbHkgYXZvaWQgZ29pbmcgdmlhIHRoZSBTY3JvbGxQYW5lbCBmb3IgdGhpcyBjYWxsIC0gdGhlXHJcbiAgICAgICAgLy8gU2Nyb2xsUGFuZWwgbWlnaHQgYWxyZWFkeSBoYXZlIGFuIGFjdGl2ZSBwYWdpbmF0aW9uIHByb21pc2UsIHdoaWNoXHJcbiAgICAgICAgLy8gd2lsbCBmYWlsLCBidXQgd291bGQgc3RvcCB1cyBwYXNzaW5nIHRoZSBwYWdpbmF0aW9uIHJlcXVlc3QgdG8gdGhlXHJcbiAgICAgICAgLy8gdGltZWxpbmUgd2luZG93LlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vdmVjdG9yLXdlYi9pc3N1ZXMvMTAzNVxyXG4gICAgICAgIHRoaXMuX3RpbWVsaW5lV2luZG93LnBhZ2luYXRlKEV2ZW50VGltZWxpbmUuRk9SV0FSRFMsIDEsIGZhbHNlKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSB7IHJldHVybjsgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgeyBldmVudHMsIGxpdmVFdmVudHMsIGZpcnN0VmlzaWJsZUV2ZW50SW5kZXggfSA9IHRoaXMuX2dldEV2ZW50cygpO1xyXG4gICAgICAgICAgICBjb25zdCBsYXN0TGl2ZUV2ZW50ID0gbGl2ZUV2ZW50c1tsaXZlRXZlbnRzLmxlbmd0aCAtIDFdO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXBkYXRlZFN0YXRlID0ge1xyXG4gICAgICAgICAgICAgICAgZXZlbnRzLFxyXG4gICAgICAgICAgICAgICAgbGl2ZUV2ZW50cyxcclxuICAgICAgICAgICAgICAgIGZpcnN0VmlzaWJsZUV2ZW50SW5kZXgsXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBsZXQgY2FsbFJNVXBkYXRlZDtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHtcclxuICAgICAgICAgICAgICAgIC8vIHdoZW4gYSBuZXcgZXZlbnQgYXJyaXZlcyB3aGVuIHRoZSB1c2VyIGlzIG5vdCB3YXRjaGluZyB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHdpbmRvdywgYnV0IHRoZSB3aW5kb3cgaXMgaW4gaXRzIGF1dG8tc2Nyb2xsIG1vZGUsIG1ha2Ugc3VyZSB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHJlYWQgbWFya2VyIGlzIHZpc2libGUuXHJcbiAgICAgICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAgICAgLy8gV2UgaWdub3JlIGV2ZW50cyB3ZSBoYXZlIHNlbnQgb3Vyc2VsdmVzOyB3ZSBkb24ndCB3YW50IHRvIHNlZSB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHJlYWQtbWFya2VyIHdoZW4gYSByZW1vdGUgZWNobyBvZiBhbiBldmVudCB3ZSBoYXZlIGp1c3Qgc2VudCB0YWtlc1xyXG4gICAgICAgICAgICAgICAgLy8gbW9yZSB0aGFuIHRoZSB0aW1lb3V0IG9uIHVzZXJBY3RpdmVSZWNlbnRseS5cclxuICAgICAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgICAgICBjb25zdCBteVVzZXJJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5jcmVkZW50aWFscy51c2VySWQ7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZW5kZXIgPSBldi5zZW5kZXIgPyBldi5zZW5kZXIudXNlcklkIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIGNhbGxSTVVwZGF0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmIChzZW5kZXIgIT0gbXlVc2VySWQgJiYgIVVzZXJBY3Rpdml0eS5zaGFyZWRJbnN0YW5jZSgpLnVzZXJBY3RpdmVSZWNlbnRseSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXBkYXRlZFN0YXRlLnJlYWRNYXJrZXJWaXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobGFzdExpdmVFdmVudCAmJiB0aGlzLmdldFJlYWRNYXJrZXJQb3NpdGlvbigpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2Uga25vdyB3ZSdyZSBzdHVja0F0Qm90dG9tLCBzbyB3ZSBjYW4gYWR2YW5jZSB0aGUgUk1cclxuICAgICAgICAgICAgICAgICAgICAvLyBpbW1lZGlhdGVseSwgdG8gc2F2ZSBhIGxhdGVyIHJlbmRlciBjeWNsZVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRSZWFkTWFya2VyKGxhc3RMaXZlRXZlbnQuZ2V0SWQoKSwgbGFzdExpdmVFdmVudC5nZXRUcygpLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB1cGRhdGVkU3RhdGUucmVhZE1hcmtlclZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB1cGRhdGVkU3RhdGUucmVhZE1hcmtlckV2ZW50SWQgPSBsYXN0TGl2ZUV2ZW50LmdldElkKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FsbFJNVXBkYXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUodXBkYXRlZFN0YXRlLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC51cGRhdGVUaW1lbGluZU1pbkhlaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxSTVVwZGF0ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uUmVhZE1hcmtlclVwZGF0ZWQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVRpbWVsaW5lUmVzZXQ6IGZ1bmN0aW9uKHJvb20sIHRpbWVsaW5lU2V0KSB7XHJcbiAgICAgICAgaWYgKHRpbWVsaW5lU2V0ICE9PSB0aGlzLnByb3BzLnRpbWVsaW5lU2V0KSByZXR1cm47XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudCAmJiB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5pc0F0Qm90dG9tKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5fbG9hZFRpbWVsaW5lKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjYW5SZXNldFRpbWVsaW5lOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQgJiYgdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQuaXNBdEJvdHRvbSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21SZWRhY3Rpb246IGZ1bmN0aW9uKGV2LCByb29tKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIGlnbm9yZSBldmVudHMgZm9yIG90aGVyIHJvb21zXHJcbiAgICAgICAgaWYgKHJvb20gIT09IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyB3ZSBjb3VsZCBza2lwIGFuIHVwZGF0ZSBpZiB0aGUgZXZlbnQgaXNuJ3QgaW4gb3VyIHRpbWVsaW5lLFxyXG4gICAgICAgIC8vIGJ1dCB0aGF0J3MgcHJvYmFibHkgYW4gZWFybHkgb3B0aW1pc2F0aW9uLlxyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FdmVudFJlcGxhY2VkOiBmdW5jdGlvbihyZXBsYWNlZEV2ZW50LCByb29tKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIGlnbm9yZSBldmVudHMgZm9yIG90aGVyIHJvb21zXHJcbiAgICAgICAgaWYgKHJvb20gIT09IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyB3ZSBjb3VsZCBza2lwIGFuIHVwZGF0ZSBpZiB0aGUgZXZlbnQgaXNuJ3QgaW4gb3VyIHRpbWVsaW5lLFxyXG4gICAgICAgIC8vIGJ1dCB0aGF0J3MgcHJvYmFibHkgYW4gZWFybHkgb3B0aW1pc2F0aW9uLlxyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tUmVjZWlwdDogZnVuY3Rpb24oZXYsIHJvb20pIHtcclxuICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gaWdub3JlIGV2ZW50cyBmb3Igb3RoZXIgcm9vbXNcclxuICAgICAgICBpZiAocm9vbSAhPT0gdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Mb2NhbEVjaG9VcGRhdGVkOiBmdW5jdGlvbihldiwgcm9vbSwgb2xkRXZlbnRJZCkge1xyXG4gICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBpZ25vcmUgZXZlbnRzIGZvciBvdGhlciByb29tc1xyXG4gICAgICAgIGlmIChyb29tICE9PSB0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb20pIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5fcmVsb2FkRXZlbnRzKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWNjb3VudERhdGE6IGZ1bmN0aW9uKGV2LCByb29tKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIGlnbm9yZSBldmVudHMgZm9yIG90aGVyIHJvb21zXHJcbiAgICAgICAgaWYgKHJvb20gIT09IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoZXYuZ2V0VHlwZSgpICE9PSBcIm0uZnVsbHlfcmVhZFwiKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIFhYWDogcm9vbVJlYWRNYXJrZXJUc01hcCBub3QgdXBkYXRlZCBoZXJlIHNvIGl0IGlzIG5vdyBpbmNvbnNpc3RlbnQuIFJlcGxhY2VcclxuICAgICAgICAvLyB0aGlzIG1lY2hhbmlzbSBvZiBkZXRlcm1pbmluZyB3aGVyZSB0aGUgUk0gaXMgcmVsYXRpdmUgdG8gdGhlIHZpZXctcG9ydCB3aXRoXHJcbiAgICAgICAgLy8gb25lIHN1cHBvcnRlZCBieSB0aGUgc2VydmVyICh0aGUgY2xpZW50IG5lZWRzIG1vcmUgdGhhbiBhbiBldmVudCBJRCkuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJlYWRNYXJrZXJFdmVudElkOiBldi5nZXRDb250ZW50KCkuZXZlbnRfaWQsXHJcbiAgICAgICAgfSwgdGhpcy5wcm9wcy5vblJlYWRNYXJrZXJVcGRhdGVkKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FdmVudERlY3J5cHRlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICAvLyBDYW4gYmUgbnVsbCBmb3IgdGhlIG5vdGlmaWNhdGlvbiB0aW1lbGluZSwgZXRjLlxyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIE5lZWQgdG8gdXBkYXRlIGFzIHdlIGRvbid0IGRpc3BsYXkgZXZlbnQgdGlsZXMgZm9yIGV2ZW50cyB0aGF0XHJcbiAgICAgICAgLy8gaGF2ZW4ndCB5ZXQgYmVlbiBkZWNyeXB0ZWQuIFRoZSBldmVudCB3aWxsIGhhdmUganVzdCBiZWVuIHVwZGF0ZWRcclxuICAgICAgICAvLyBpbiBwbGFjZSBzbyB3ZSBqdXN0IG5lZWQgdG8gcmUtcmVuZGVyLlxyXG4gICAgICAgIC8vIFRPRE86IFdlIHNob3VsZCByZXN0cmljdCB0aGlzIHRvIG9ubHkgZXZlbnRzIGluIG91ciB0aW1lbGluZSxcclxuICAgICAgICAvLyBidXQgcG9zc2libHkgdGhlIGV2ZW50IHRpbGUgaXRzZWxmIHNob3VsZCBqdXN0IHVwZGF0ZSB3aGVuIHRoaXNcclxuICAgICAgICAvLyBoYXBwZW5zIHRvIHNhdmUgdXMgcmUtcmVuZGVyaW5nIHRoZSB3aG9sZSB0aW1lbGluZS5cclxuICAgICAgICBpZiAoZXYuZ2V0Um9vbUlkKCkgPT09IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbS5yb29tSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25TeW5jOiBmdW5jdGlvbihzdGF0ZSwgcHJldlN0YXRlLCBkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y2xpZW50U3luY1N0YXRlOiBzdGF0ZX0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVhZE1hcmtlclRpbWVvdXQocmVhZE1hcmtlclBvc2l0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuIHJlYWRNYXJrZXJQb3NpdGlvbiA9PT0gMCA/XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUucmVhZE1hcmtlckluVmlld1RocmVzaG9sZE1zIDpcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5yZWFkTWFya2VyT3V0T2ZWaWV3VGhyZXNob2xkTXM7XHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZVJlYWRNYXJrZXJPblVzZXJBY3Rpdml0eTogYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgaW5pdGlhbFRpbWVvdXQgPSB0aGlzLl9yZWFkTWFya2VyVGltZW91dCh0aGlzLmdldFJlYWRNYXJrZXJQb3NpdGlvbigpKTtcclxuICAgICAgICB0aGlzLl9yZWFkTWFya2VyQWN0aXZpdHlUaW1lciA9IG5ldyBUaW1lcihpbml0aWFsVGltZW91dCk7XHJcblxyXG4gICAgICAgIHdoaWxlICh0aGlzLl9yZWFkTWFya2VyQWN0aXZpdHlUaW1lcikgeyAvL3Vuc2V0IG9uIHVubW91bnRcclxuICAgICAgICAgICAgVXNlckFjdGl2aXR5LnNoYXJlZEluc3RhbmNlKCkudGltZVdoaWxlQWN0aXZlUmVjZW50bHkodGhpcy5fcmVhZE1hcmtlckFjdGl2aXR5VGltZXIpO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fcmVhZE1hcmtlckFjdGl2aXR5VGltZXIuZmluaXNoZWQoKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkgeyBjb250aW51ZTsgLyogYWJvcnRlZCAqLyB9XHJcbiAgICAgICAgICAgIC8vIG91dHNpZGUgb2YgdHJ5L2NhdGNoIHRvIG5vdCBzd2FsbG93IGVycm9yc1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVJlYWRNYXJrZXIoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZVJlYWRSZWNlaXB0T25Vc2VyQWN0aXZpdHk6IGFzeW5jIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3JlYWRSZWNlaXB0QWN0aXZpdHlUaW1lciA9IG5ldyBUaW1lcihSRUFEX1JFQ0VJUFRfSU5URVJWQUxfTVMpO1xyXG4gICAgICAgIHdoaWxlICh0aGlzLl9yZWFkUmVjZWlwdEFjdGl2aXR5VGltZXIpIHsgLy91bnNldCBvbiB1bm1vdW50XHJcbiAgICAgICAgICAgIFVzZXJBY3Rpdml0eS5zaGFyZWRJbnN0YW5jZSgpLnRpbWVXaGlsZUFjdGl2ZU5vdyh0aGlzLl9yZWFkUmVjZWlwdEFjdGl2aXR5VGltZXIpO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fcmVhZFJlY2VpcHRBY3Rpdml0eVRpbWVyLmZpbmlzaGVkKCk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHsgY29udGludWU7IC8qIGFib3J0ZWQgKi8gfVxyXG4gICAgICAgICAgICAvLyBvdXRzaWRlIG9mIHRyeS9jYXRjaCB0byBub3Qgc3dhbGxvdyBlcnJvcnNcclxuICAgICAgICAgICAgdGhpcy5zZW5kUmVhZFJlY2VpcHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHNlbmRSZWFkUmVjZWlwdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJsb3dCYW5kd2lkdGhcIikpIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudCkgcmV0dXJuO1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5tYW5hZ2VSZWFkUmVjZWlwdHMpIHJldHVybjtcclxuICAgICAgICAvLyBUaGlzIGhhcHBlbnMgb24gdXNlcl9hY3Rpdml0eV9lbmQgd2hpY2ggaXMgZGVsYXllZCwgYW5kIGl0J3NcclxuICAgICAgICAvLyB2ZXJ5IHBvc3NpYmxlIGhhdmUgbG9nZ2VkIG91dCB3aXRoaW4gdGhhdCB0aW1lZnJhbWUsIHNvIGNoZWNrXHJcbiAgICAgICAgLy8gd2Ugc3RpbGwgaGF2ZSBhIGNsaWVudC5cclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgLy8gaWYgbm8gY2xpZW50IG9yIGNsaWVudCBpcyBndWVzdCBkb24ndCBzZW5kIFJSIG9yIFJNXHJcbiAgICAgICAgaWYgKCFjbGkgfHwgY2xpLmlzR3Vlc3QoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBsZXQgc2hvdWxkU2VuZFJSID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgY29uc3QgY3VycmVudFJSRXZlbnRJZCA9IHRoaXMuX2dldEN1cnJlbnRSZWFkUmVjZWlwdCh0cnVlKTtcclxuICAgICAgICBjb25zdCBjdXJyZW50UlJFdmVudEluZGV4ID0gdGhpcy5faW5kZXhGb3JFdmVudElkKGN1cnJlbnRSUkV2ZW50SWQpO1xyXG4gICAgICAgIC8vIFdlIHdhbnQgdG8gYXZvaWQgc2VuZGluZyBvdXQgcmVhZCByZWNlaXB0cyB3aGVuIHdlIGFyZSBsb29raW5nIGF0XHJcbiAgICAgICAgLy8gZXZlbnRzIGluIHRoZSBwYXN0IHdoaWNoIGFyZSBiZWZvcmUgdGhlIGxhdGVzdCBSUi5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIEZvciBub3csIGxldCdzIGFwcGx5IGEgaGV1cmlzdGljOiBpZiAoYSkgdGhlIGV2ZW50IGNvcnJlc3BvbmRpbmcgdG9cclxuICAgICAgICAvLyB0aGUgbGF0ZXN0IFJSIChlaXRoZXIgZnJvbSB0aGUgc2VydmVyLCBvciBzZW50IGJ5IG91cnNlbHZlcykgZG9lc24ndFxyXG4gICAgICAgIC8vIGFwcGVhciBpbiBvdXIgdGltZWxpbmUsIGFuZCAoYikgd2UgY291bGQgZm9yd2FyZC1wYWdpbmF0ZSB0aGUgZXZlbnRcclxuICAgICAgICAvLyB0aW1lbGluZSwgdGhlbiBkb24ndCBzZW5kIGFueSBtb3JlIFJScy5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIFRoaXMgaXNuJ3Qgd2F0ZXJ0aWdodCwgYXMgd2UgY291bGQgYmUgbG9va2luZyBhdCBhIHNlY3Rpb24gb2ZcclxuICAgICAgICAvLyB0aW1lbGluZSB3aGljaCBpcyAqYWZ0ZXIqIHRoZSBsYXRlc3QgUlIgKHNvIHdlIHNob3VsZCBhY3R1YWxseSBzZW5kXHJcbiAgICAgICAgLy8gUlJzKSAtIGJ1dCB0aGF0IGlzIGEgYml0IG9mIGEgbmljaGUgY2FzZS4gSXQgd2lsbCBzb3J0IGl0c2VsZiBvdXQgd2hlblxyXG4gICAgICAgIC8vIHRoZSB1c2VyIGV2ZW50dWFsbHkgaGl0cyB0aGUgbGl2ZSB0aW1lbGluZS5cclxuICAgICAgICAvL1xyXG4gICAgICAgIGlmIChjdXJyZW50UlJFdmVudElkICYmIGN1cnJlbnRSUkV2ZW50SW5kZXggPT09IG51bGwgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RpbWVsaW5lV2luZG93LmNhblBhZ2luYXRlKEV2ZW50VGltZWxpbmUuRk9SV0FSRFMpKSB7XHJcbiAgICAgICAgICAgIHNob3VsZFNlbmRSUiA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbGFzdFJlYWRFdmVudEluZGV4ID0gdGhpcy5fZ2V0TGFzdERpc3BsYXllZEV2ZW50SW5kZXgoe1xyXG4gICAgICAgICAgICBpZ25vcmVPd246IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGxhc3RSZWFkRXZlbnRJbmRleCA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICBzaG91bGRTZW5kUlIgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGxhc3RSZWFkRXZlbnQgPSB0aGlzLnN0YXRlLmV2ZW50c1tsYXN0UmVhZEV2ZW50SW5kZXhdO1xyXG4gICAgICAgIHNob3VsZFNlbmRSUiA9IHNob3VsZFNlbmRSUiAmJlxyXG4gICAgICAgICAgICAvLyBPbmx5IHNlbmQgYSBSUiBpZiB0aGUgbGFzdCByZWFkIGV2ZW50IGlzIGFoZWFkIGluIHRoZSB0aW1lbGluZSByZWxhdGl2ZSB0b1xyXG4gICAgICAgICAgICAvLyB0aGUgY3VycmVudCBSUiBldmVudC5cclxuICAgICAgICAgICAgbGFzdFJlYWRFdmVudEluZGV4ID4gY3VycmVudFJSRXZlbnRJbmRleCAmJlxyXG4gICAgICAgICAgICAvLyBPbmx5IHNlbmQgYSBSUiBpZiB0aGUgbGFzdCBSUiBzZXQgIT0gdGhlIG9uZSB3ZSB3b3VsZCBzZW5kXHJcbiAgICAgICAgICAgIHRoaXMubGFzdFJSU2VudEV2ZW50SWQgIT0gbGFzdFJlYWRFdmVudC5nZXRJZCgpO1xyXG5cclxuICAgICAgICAvLyBPbmx5IHNlbmQgYSBSTSBpZiB0aGUgbGFzdCBSTSBzZW50ICE9IHRoZSBvbmUgd2Ugd291bGQgc2VuZFxyXG4gICAgICAgIGNvbnN0IHNob3VsZFNlbmRSTSA9XHJcbiAgICAgICAgICAgIHRoaXMubGFzdFJNU2VudEV2ZW50SWQgIT0gdGhpcy5zdGF0ZS5yZWFkTWFya2VyRXZlbnRJZDtcclxuXHJcbiAgICAgICAgLy8gd2UgYWxzbyByZW1lbWJlciB0aGUgbGFzdCByZWFkIHJlY2VpcHQgd2Ugc2VudCB0byBhdm9pZCBzcGFtbWluZyB0aGVcclxuICAgICAgICAvLyBzYW1lIG9uZSBhdCB0aGUgc2VydmVyIHJlcGVhdGVkbHlcclxuICAgICAgICBpZiAoc2hvdWxkU2VuZFJSIHx8IHNob3VsZFNlbmRSTSkge1xyXG4gICAgICAgICAgICBpZiAoc2hvdWxkU2VuZFJSKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxhc3RSUlNlbnRFdmVudElkID0gbGFzdFJlYWRFdmVudC5nZXRJZCgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGFzdFJlYWRFdmVudCA9IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5sYXN0Uk1TZW50RXZlbnRJZCA9IHRoaXMuc3RhdGUucmVhZE1hcmtlckV2ZW50SWQ7XHJcblxyXG4gICAgICAgICAgICBjb25zdCByb29tSWQgPSB0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb20ucm9vbUlkO1xyXG4gICAgICAgICAgICBjb25zdCBoaWRkZW5SUiA9ICFTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwic2VuZFJlYWRSZWNlaXB0c1wiLCByb29tSWQpO1xyXG5cclxuICAgICAgICAgICAgZGVidWdsb2coJ1RpbWVsaW5lUGFuZWw6IFNlbmRpbmcgUmVhZCBNYXJrZXJzIGZvciAnLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tLnJvb21JZCxcclxuICAgICAgICAgICAgICAgICdybScsIHRoaXMuc3RhdGUucmVhZE1hcmtlckV2ZW50SWQsXHJcbiAgICAgICAgICAgICAgICBsYXN0UmVhZEV2ZW50ID8gJ3JyICcgKyBsYXN0UmVhZEV2ZW50LmdldElkKCkgOiAnJyxcclxuICAgICAgICAgICAgICAgICcgaGlkZGVuOicgKyBoaWRkZW5SUixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldFJvb21SZWFkTWFya2VycyhcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnJlYWRNYXJrZXJFdmVudElkLFxyXG4gICAgICAgICAgICAgICAgbGFzdFJlYWRFdmVudCwgLy8gQ291bGQgYmUgbnVsbCwgaW4gd2hpY2ggY2FzZSBubyBSUiBpcyBzZW50XHJcbiAgICAgICAgICAgICAgICB7aGlkZGVuOiBoaWRkZW5SUn0sXHJcbiAgICAgICAgICAgICkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIC9yZWFkX21hcmtlcnMgQVBJIGlzIG5vdCBpbXBsZW1lbnRlZCBvbiB0aGlzIEhTLCBmYWxsYmFjayB0byBqdXN0IFJSXHJcbiAgICAgICAgICAgICAgICBpZiAoZS5lcnJjb2RlID09PSAnTV9VTlJFQ09HTklaRUQnICYmIGxhc3RSZWFkRXZlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRSZWFkUmVjZWlwdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFzdFJlYWRFdmVudCxcclxuICAgICAgICAgICAgICAgICAgICAgICAge2hpZGRlbjogaGlkZGVuUlJ9LFxyXG4gICAgICAgICAgICAgICAgICAgICkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sYXN0UlJTZW50RXZlbnRJZCA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGl0IGZhaWxlZCwgc28gYWxsb3cgcmV0cmllcyBuZXh0IHRpbWUgdGhlIHVzZXIgaXMgYWN0aXZlXHJcbiAgICAgICAgICAgICAgICB0aGlzLmxhc3RSUlNlbnRFdmVudElkID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sYXN0Uk1TZW50RXZlbnRJZCA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBkbyBhIHF1aWNrLXJlc2V0IG9mIG91ciB1bnJlYWROb3RpZmljYXRpb25Db3VudCB0byBhdm9pZCBoYXZpbmdcclxuICAgICAgICAgICAgLy8gdG8gd2FpdCBmcm9tIHRoZSByZW1vdGUgZWNobyBmcm9tIHRoZSBob21lc2VydmVyLlxyXG4gICAgICAgICAgICAvLyB3ZSBvbmx5IGRvIHRoaXMgaWYgd2UncmUgcmlnaHQgYXQgdGhlIGVuZCwgYmVjYXVzZSB3ZSdyZSBqdXN0IGFzc3VtaW5nXHJcbiAgICAgICAgICAgIC8vIHRoYXQgc2VuZGluZyBhbiBSUiBmb3IgdGhlIGxhdGVzdCBtZXNzYWdlIHdpbGwgc2V0IG91ciBub3RpZiBjb3VudGVyXHJcbiAgICAgICAgICAgIC8vIHRvIHplcm86IGl0IG1heSBub3QgZG8gdGhpcyBpZiB3ZSBzZW5kIGFuIFJSIGZvciBzb21ld2hlcmUgYmVmb3JlIHRoZSBlbmQuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzQXRFbmRPZkxpdmVUaW1lbGluZSgpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb20uc2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQoJ3RvdGFsJywgMCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb20uc2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQoJ2hpZ2hsaWdodCcsIDApO1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdvbl9yb29tX3JlYWQnLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21JZDogdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tLnJvb21JZCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBpZiB0aGUgcmVhZCBtYXJrZXIgaXMgb24gdGhlIHNjcmVlbiwgd2UgY2FuIG5vdyBhc3N1bWUgd2UndmUgY2F1Z2h0IHVwIHRvIHRoZSBlbmRcclxuICAgIC8vIG9mIHRoZSBzY3JlZW4sIHNvIG1vdmUgdGhlIG1hcmtlciBkb3duIHRvIHRoZSBib3R0b20gb2YgdGhlIHNjcmVlbi5cclxuICAgIHVwZGF0ZVJlYWRNYXJrZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5tYW5hZ2VSZWFkTWFya2VycykgcmV0dXJuO1xyXG4gICAgICAgIGlmICh0aGlzLmdldFJlYWRNYXJrZXJQb3NpdGlvbigpID09PSAxKSB7XHJcbiAgICAgICAgICAgIC8vIHRoZSByZWFkIG1hcmtlciBpcyBhdCBhbiBldmVudCBiZWxvdyB0aGUgdmlld3BvcnQsXHJcbiAgICAgICAgICAgIC8vIHdlIGRvbid0IHdhbnQgdG8gcmV3aW5kIGl0LlxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIG1vdmUgdGhlIFJNIHRvICphZnRlciogdGhlIG1lc3NhZ2UgYXQgdGhlIGJvdHRvbSBvZiB0aGUgc2NyZWVuLiBUaGlzXHJcbiAgICAgICAgLy8gYXZvaWRzIGEgcHJvYmxlbSB3aGVyZWJ5IHdlIG5ldmVyIGFkdmFuY2UgdGhlIFJNIGlmIHRoZXJlIGlzIGEgaHVnZVxyXG4gICAgICAgIC8vIG1lc3NhZ2Ugd2hpY2ggZG9lc24ndCBmaXQgb24gdGhlIHNjcmVlbi5cclxuICAgICAgICBjb25zdCBsYXN0RGlzcGxheWVkSW5kZXggPSB0aGlzLl9nZXRMYXN0RGlzcGxheWVkRXZlbnRJbmRleCh7XHJcbiAgICAgICAgICAgIGFsbG93UGFydGlhbDogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGxhc3REaXNwbGF5ZWRJbmRleCA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGxhc3REaXNwbGF5ZWRFdmVudCA9IHRoaXMuc3RhdGUuZXZlbnRzW2xhc3REaXNwbGF5ZWRJbmRleF07XHJcbiAgICAgICAgdGhpcy5fc2V0UmVhZE1hcmtlcihsYXN0RGlzcGxheWVkRXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhc3REaXNwbGF5ZWRFdmVudC5nZXRUcygpKTtcclxuXHJcbiAgICAgICAgLy8gdGhlIHJlYWQtbWFya2VyIHNob3VsZCBiZWNvbWUgaW52aXNpYmxlLCBzbyB0aGF0IGlmIHRoZSB1c2VyIHNjcm9sbHNcclxuICAgICAgICAvLyBkb3duLCB0aGV5IGRvbid0IHNlZSBpdC5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZWFkTWFya2VyVmlzaWJsZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHJlYWRNYXJrZXJWaXNpYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcblxyXG4gICAgLy8gYWR2YW5jZSB0aGUgcmVhZCBtYXJrZXIgcGFzdCBhbnkgZXZlbnRzIHdlIHNlbnQgb3Vyc2VsdmVzLlxyXG4gICAgX2FkdmFuY2VSZWFkTWFya2VyUGFzdE15RXZlbnRzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gd2UgY2FsbCBgX3RpbWVsaW5lV2luZG93LmdldEV2ZW50cygpYCByYXRoZXIgdGhhbiB1c2luZ1xyXG4gICAgICAgIC8vIGB0aGlzLnN0YXRlLmxpdmVFdmVudHNgLCBiZWNhdXNlIFJlYWN0IGJhdGNoZXMgdGhlIHVwZGF0ZSB0byB0aGVcclxuICAgICAgICAvLyBsYXR0ZXIsIHNvIGl0IG1heSBub3QgaGF2ZSBiZWVuIHVwZGF0ZWQgeWV0LlxyXG4gICAgICAgIGNvbnN0IGV2ZW50cyA9IHRoaXMuX3RpbWVsaW5lV2luZG93LmdldEV2ZW50cygpO1xyXG5cclxuICAgICAgICAvLyBmaXJzdCBmaW5kIHdoZXJlIHRoZSBjdXJyZW50IFJNIGlzXHJcbiAgICAgICAgbGV0IGk7XHJcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IGV2ZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnRzW2ldLmdldElkKCkgPT0gdGhpcy5zdGF0ZS5yZWFkTWFya2VyRXZlbnRJZCkge1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGkgPj0gZXZlbnRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBub3cgdGhpbmsgYWJvdXQgYWR2YW5jaW5nIGl0XHJcbiAgICAgICAgY29uc3QgbXlVc2VySWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY3JlZGVudGlhbHMudXNlcklkO1xyXG4gICAgICAgIGZvciAoaSsrOyBpIDwgZXZlbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ID0gZXZlbnRzW2ldO1xyXG4gICAgICAgICAgICBpZiAoIWV2LnNlbmRlciB8fCBldi5zZW5kZXIudXNlcklkICE9IG15VXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBpIGlzIG5vdyB0aGUgZmlyc3QgdW5yZWFkIG1lc3NhZ2Ugd2hpY2ggd2UgZGlkbid0IHNlbmQgb3Vyc2VsdmVzLlxyXG4gICAgICAgIGktLTtcclxuXHJcbiAgICAgICAgY29uc3QgZXYgPSBldmVudHNbaV07XHJcbiAgICAgICAgdGhpcy5fc2V0UmVhZE1hcmtlcihldi5nZXRJZCgpLCBldi5nZXRUcygpKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoganVtcCBkb3duIHRvIHRoZSBib3R0b20gb2YgdGhpcyByb29tLCB3aGVyZSBuZXcgZXZlbnRzIGFyZSBhcnJpdmluZ1xyXG4gICAgICovXHJcbiAgICBqdW1wVG9MaXZlVGltZWxpbmU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIGlmIHdlIGNhbid0IGZvcndhcmQtcGFnaW5hdGUgdGhlIGV4aXN0aW5nIHRpbWVsaW5lLCB0aGVuIHRoZXJlXHJcbiAgICAgICAgLy8gaXMgbm8gcG9pbnQgcmVsb2FkaW5nIGl0IC0ganVzdCBqdW1wIHN0cmFpZ2h0IHRvIHRoZSBib3R0b20uXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBPdGhlcndpc2UsIHJlbG9hZCB0aGUgdGltZWxpbmUgcmF0aGVyIHRoYW4gdHJ5aW5nIHRvIHBhZ2luYXRlXHJcbiAgICAgICAgLy8gdGhyb3VnaCBhbGwgb2Ygc3BhY2UtdGltZS5cclxuICAgICAgICBpZiAodGhpcy5fdGltZWxpbmVXaW5kb3cuY2FuUGFnaW5hdGUoRXZlbnRUaW1lbGluZS5GT1JXQVJEUykpIHtcclxuICAgICAgICAgICAgdGhpcy5fbG9hZFRpbWVsaW5lKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5zY3JvbGxUb0JvdHRvbSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvKiBzY3JvbGwgdG8gc2hvdyB0aGUgcmVhZC11cC10byBtYXJrZXIuIFdlIHB1dCBpdCAxLzMgb2YgdGhlIHdheSBkb3duXHJcbiAgICAgKiB0aGUgY29udGFpbmVyLlxyXG4gICAgICovXHJcbiAgICBqdW1wVG9SZWFkTWFya2VyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHJldHVybjtcclxuICAgICAgICBpZiAoIXRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50KSByZXR1cm47XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJlYWRNYXJrZXJFdmVudElkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIHdlIG1heSBub3QgaGF2ZSBsb2FkZWQgdGhlIGV2ZW50IGNvcnJlc3BvbmRpbmcgdG8gdGhlIHJlYWQtbWFya2VyXHJcbiAgICAgICAgLy8gaW50byB0aGUgX3RpbWVsaW5lV2luZG93LiBJbiB0aGF0IGNhc2UsIGF0dGVtcHRzIHRvIHNjcm9sbCB0byBpdFxyXG4gICAgICAgIC8vIHdpbGwgZmFpbC5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIGEgcXVpY2sgd2F5IHRvIGZpZ3VyZSBvdXQgaWYgd2UndmUgbG9hZGVkIHRoZSByZWxldmFudCBldmVudCBpc1xyXG4gICAgICAgIC8vIHNpbXBseSB0byBjaGVjayBpZiB0aGUgbWVzc2FnZXBhbmVsIGtub3dzIHdoZXJlIHRoZSByZWFkLW1hcmtlciBpcy5cclxuICAgICAgICBjb25zdCByZXQgPSB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5nZXRSZWFkTWFya2VyUG9zaXRpb24oKTtcclxuICAgICAgICBpZiAocmV0ICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIC8vIFRoZSBtZXNzYWdlcGFuZWwga25vd3Mgd2hlcmUgdGhlIFJNIGlzLCBzbyB3ZSBtdXN0IGhhdmUgbG9hZGVkXHJcbiAgICAgICAgICAgIC8vIHRoZSByZWxldmFudCBldmVudC5cclxuICAgICAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQuc2Nyb2xsVG9FdmVudCh0aGlzLnN0YXRlLnJlYWRNYXJrZXJFdmVudElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMCwgMS8zKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gTG9va3MgbGlrZSB3ZSBoYXZlbid0IGxvYWRlZCB0aGUgZXZlbnQgY29ycmVzcG9uZGluZyB0byB0aGUgcmVhZC1tYXJrZXIuXHJcbiAgICAgICAgLy8gQXMgd2l0aCBqdW1wVG9MaXZlVGltZWxpbmUsIHdlIHdhbnQgdG8gcmVsb2FkIHRoZSB0aW1lbGluZSBhcm91bmQgdGhlXHJcbiAgICAgICAgLy8gcmVhZC1tYXJrZXIuXHJcbiAgICAgICAgdGhpcy5fbG9hZFRpbWVsaW5lKHRoaXMuc3RhdGUucmVhZE1hcmtlckV2ZW50SWQsIDAsIDEvMyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qIHVwZGF0ZSB0aGUgcmVhZC11cC10byBtYXJrZXIgdG8gbWF0Y2ggdGhlIHJlYWQgcmVjZWlwdFxyXG4gICAgICovXHJcbiAgICBmb3JnZXRSZWFkTWFya2VyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMubWFuYWdlUmVhZE1hcmtlcnMpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3Qgcm1JZCA9IHRoaXMuX2dldEN1cnJlbnRSZWFkUmVjZWlwdCgpO1xyXG5cclxuICAgICAgICAvLyBzZWUgaWYgd2Uga25vdyB0aGUgdGltZXN0YW1wIGZvciB0aGUgcnIgZXZlbnRcclxuICAgICAgICBjb25zdCB0bCA9IHRoaXMucHJvcHMudGltZWxpbmVTZXQuZ2V0VGltZWxpbmVGb3JFdmVudChybUlkKTtcclxuICAgICAgICBsZXQgcm1UcztcclxuICAgICAgICBpZiAodGwpIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSB0bC5nZXRFdmVudHMoKS5maW5kKChlKSA9PiB7IHJldHVybiBlLmdldElkKCkgPT0gcm1JZDsgfSk7XHJcbiAgICAgICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICAgICAgcm1UcyA9IGV2ZW50LmdldFRzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3NldFJlYWRNYXJrZXIocm1JZCwgcm1Ucyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qIHJldHVybiB0cnVlIGlmIHRoZSBjb250ZW50IGlzIGZ1bGx5IHNjcm9sbGVkIGRvd24gYW5kIHdlIGFyZVxyXG4gICAgICogYXQgdGhlIGVuZCBvZiB0aGUgbGl2ZSB0aW1lbGluZS5cclxuICAgICAqL1xyXG4gICAgaXNBdEVuZE9mTGl2ZVRpbWVsaW5lOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnRcclxuICAgICAgICAgICAgJiYgdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQuaXNBdEJvdHRvbSgpXHJcbiAgICAgICAgICAgICYmIHRoaXMuX3RpbWVsaW5lV2luZG93XHJcbiAgICAgICAgICAgICYmICF0aGlzLl90aW1lbGluZVdpbmRvdy5jYW5QYWdpbmF0ZShFdmVudFRpbWVsaW5lLkZPUldBUkRTKTtcclxuICAgIH0sXHJcblxyXG5cclxuICAgIC8qIGdldCB0aGUgY3VycmVudCBzY3JvbGwgc3RhdGUuIFNlZSBTY3JvbGxQYW5lbC5nZXRTY3JvbGxTdGF0ZSBmb3JcclxuICAgICAqIGRldGFpbHMuXHJcbiAgICAgKlxyXG4gICAgICogcmV0dXJucyBudWxsIGlmIHdlIGFyZSBub3QgbW91bnRlZC5cclxuICAgICAqL1xyXG4gICAgZ2V0U2Nyb2xsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQpIHsgcmV0dXJuIG51bGw7IH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQuZ2V0U2Nyb2xsU3RhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gcmV0dXJucyBvbmUgb2Y6XHJcbiAgICAvL1xyXG4gICAgLy8gIG51bGw6IHRoZXJlIGlzIG5vIHJlYWQgbWFya2VyXHJcbiAgICAvLyAgLTE6IHJlYWQgbWFya2VyIGlzIGFib3ZlIHRoZSB3aW5kb3dcclxuICAgIC8vICAgMDogcmVhZCBtYXJrZXIgaXMgdmlzaWJsZVxyXG4gICAgLy8gICsxOiByZWFkIG1hcmtlciBpcyBiZWxvdyB0aGUgd2luZG93XHJcbiAgICBnZXRSZWFkTWFya2VyUG9zaXRpb246IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5tYW5hZ2VSZWFkTWFya2VycykgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudCkgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIGNvbnN0IHJldCA9IHRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50LmdldFJlYWRNYXJrZXJQb3NpdGlvbigpO1xyXG4gICAgICAgIGlmIChyZXQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJldDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoZSBtZXNzYWdlUGFuZWwgZG9lc24ndCBrbm93IHdoZXJlIHRoZSByZWFkIG1hcmtlciBpcy5cclxuICAgICAgICAvLyBpZiB3ZSBrbm93IHRoZSB0aW1lc3RhbXAgb2YgdGhlIHJlYWQgbWFya2VyLCBtYWtlIGEgZ3Vlc3MgYmFzZWQgb24gdGhhdC5cclxuICAgICAgICBjb25zdCBybVRzID0gVGltZWxpbmVQYW5lbC5yb29tUmVhZE1hcmtlclRzTWFwW3RoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbS5yb29tSWRdO1xyXG4gICAgICAgIGlmIChybVRzICYmIHRoaXMuc3RhdGUuZXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgaWYgKHJtVHMgPCB0aGlzLnN0YXRlLmV2ZW50c1swXS5nZXRUcygpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gLTE7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9LFxyXG5cclxuICAgIGNhbkp1bXBUb1JlYWRNYXJrZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIDEuIERvIG5vdCBzaG93IGp1bXAgYmFyIGlmIG5laXRoZXIgdGhlIFJNIG5vciB0aGUgUlIgYXJlIHNldC5cclxuICAgICAgICAvLyAzLiBXZSB3YW50IHRvIHNob3cgdGhlIGJhciBpZiB0aGUgcmVhZC1tYXJrZXIgaXMgb2ZmIHRoZSB0b3Agb2YgdGhlIHNjcmVlbi5cclxuICAgICAgICAvLyA0LiBBbHNvLCBpZiBwb3MgPT09IG51bGwsIHRoZSBldmVudCBtaWdodCBub3QgYmUgcGFnaW5hdGVkIC0gc2hvdyB0aGUgdW5yZWFkIGJhclxyXG4gICAgICAgIGNvbnN0IHBvcyA9IHRoaXMuZ2V0UmVhZE1hcmtlclBvc2l0aW9uKCk7XHJcbiAgICAgICAgY29uc3QgcmV0ID0gdGhpcy5zdGF0ZS5yZWFkTWFya2VyRXZlbnRJZCAhPT0gbnVsbCAmJiAvLyAxLlxyXG4gICAgICAgICAgICAocG9zIDwgMCB8fCBwb3MgPT09IG51bGwpOyAvLyAzLiwgNC5cclxuICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgfSxcclxuXHJcbiAgICAvKlxyXG4gICAgICogY2FsbGVkIGJ5IHRoZSBwYXJlbnQgY29tcG9uZW50IHdoZW4gUGFnZVVwL0Rvd24vZXRjIGlzIHByZXNzZWQuXHJcbiAgICAgKlxyXG4gICAgICogV2UgcGFzcyBpdCBkb3duIHRvIHRoZSBzY3JvbGwgcGFuZWwuXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZVNjcm9sbEtleTogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBpZiAoIXRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50KSB7IHJldHVybjsgfVxyXG5cclxuICAgICAgICAvLyBqdW1wIHRvIHRoZSBsaXZlIHRpbWVsaW5lIG9uIGN0cmwtZW5kLCByYXRoZXIgdGhhbiB0aGUgZW5kIG9mIHRoZVxyXG4gICAgICAgIC8vIHRpbWVsaW5lIHdpbmRvdy5cclxuICAgICAgICBpZiAoZXYuY3RybEtleSAmJiAhZXYuc2hpZnRLZXkgJiYgIWV2LmFsdEtleSAmJiAhZXYubWV0YUtleSAmJiBldi5rZXkgPT09IEtleS5FTkQpIHtcclxuICAgICAgICAgICAgdGhpcy5qdW1wVG9MaXZlVGltZWxpbmUoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5oYW5kbGVTY3JvbGxLZXkoZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2luaXRUaW1lbGluZTogZnVuY3Rpb24ocHJvcHMpIHtcclxuICAgICAgICBjb25zdCBpbml0aWFsRXZlbnQgPSBwcm9wcy5ldmVudElkO1xyXG4gICAgICAgIGNvbnN0IHBpeGVsT2Zmc2V0ID0gcHJvcHMuZXZlbnRQaXhlbE9mZnNldDtcclxuXHJcbiAgICAgICAgLy8gaWYgYSBwaXhlbE9mZnNldCBpcyBnaXZlbiwgaXQgaXMgcmVsYXRpdmUgdG8gdGhlIGJvdHRvbSBvZiB0aGVcclxuICAgICAgICAvLyBjb250YWluZXIuIElmIG5vdCwgcHV0IHRoZSBldmVudCBpbiB0aGUgbWlkZGxlIG9mIHRoZSBjb250YWluZXIuXHJcbiAgICAgICAgbGV0IG9mZnNldEJhc2UgPSAxO1xyXG4gICAgICAgIGlmIChwaXhlbE9mZnNldCA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIG9mZnNldEJhc2UgPSAwLjU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5fbG9hZFRpbWVsaW5lKGluaXRpYWxFdmVudCwgcGl4ZWxPZmZzZXQsIG9mZnNldEJhc2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIChyZSktbG9hZCB0aGUgZXZlbnQgdGltZWxpbmUsIGFuZCBpbml0aWFsaXNlIHRoZSBzY3JvbGwgc3RhdGUsIGNlbnRlcmVkXHJcbiAgICAgKiBhcm91bmQgdGhlIGdpdmVuIGV2ZW50LlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nP30gIGV2ZW50SWQgdGhlIGV2ZW50IHRvIGZvY3VzIG9uLiBJZiB1bmRlZmluZWQsIHdpbGxcclxuICAgICAqICAgIHNjcm9sbCB0byB0aGUgYm90dG9tIG9mIHRoZSByb29tLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyP30gcGl4ZWxPZmZzZXQgICBvZmZzZXQgdG8gcG9zaXRpb24gdGhlIGdpdmVuIGV2ZW50IGF0XHJcbiAgICAgKiAgICAocGl4ZWxzIGZyb20gdGhlIG9mZnNldEJhc2UpLiBJZiBvbWl0dGVkLCBkZWZhdWx0cyB0byAwLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyP30gb2Zmc2V0QmFzZSB0aGUgcmVmZXJlbmNlIHBvaW50IGZvciB0aGUgcGl4ZWxPZmZzZXQuIDBcclxuICAgICAqICAgICBtZWFucyB0aGUgdG9wIG9mIHRoZSBjb250YWluZXIsIDEgbWVhbnMgdGhlIGJvdHRvbSwgYW5kIGZyYWN0aW9uYWxcclxuICAgICAqICAgICB2YWx1ZXMgbWVhbiBzb21ld2hlcmUgaW4gdGhlIG1pZGRsZS4gSWYgb21pdHRlZCwgaXQgZGVmYXVsdHMgdG8gMC5cclxuICAgICAqXHJcbiAgICAgKiByZXR1cm5zIGEgcHJvbWlzZSB3aGljaCB3aWxsIHJlc29sdmUgd2hlbiB0aGUgbG9hZCBjb21wbGV0ZXMuXHJcbiAgICAgKi9cclxuICAgIF9sb2FkVGltZWxpbmU6IGZ1bmN0aW9uKGV2ZW50SWQsIHBpeGVsT2Zmc2V0LCBvZmZzZXRCYXNlKSB7XHJcbiAgICAgICAgdGhpcy5fdGltZWxpbmVXaW5kb3cgPSBuZXcgTWF0cml4LlRpbWVsaW5lV2luZG93KFxyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCksIHRoaXMucHJvcHMudGltZWxpbmVTZXQsXHJcbiAgICAgICAgICAgIHt3aW5kb3dMaW1pdDogdGhpcy5wcm9wcy50aW1lbGluZUNhcH0pO1xyXG5cclxuICAgICAgICBjb25zdCBvbkxvYWRlZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgLy8gY2xlYXIgdGhlIHRpbWVsaW5lIG1pbi1oZWlnaHQgd2hlblxyXG4gICAgICAgICAgICAvLyAocmUpbG9hZGluZyB0aGUgdGltZWxpbmVcclxuICAgICAgICAgICAgaWYgKHRoaXMuX21lc3NhZ2VQYW5lbC5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5vblRpbWVsaW5lUmVzZXQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLl9yZWxvYWRFdmVudHMoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIElmIHdlIHN3aXRjaGVkIGF3YXkgZnJvbSB0aGUgcm9vbSB3aGlsZSB0aGVyZSB3ZXJlIHBlbmRpbmdcclxuICAgICAgICAgICAgLy8gb3V0Z29pbmcgZXZlbnRzLCB0aGUgcmVhZC1tYXJrZXIgd2lsbCBiZSBiZWZvcmUgdGhvc2UgZXZlbnRzLlxyXG4gICAgICAgICAgICAvLyBXZSBuZWVkIHRvIHNraXAgb3ZlciBhbnkgd2hpY2ggaGF2ZSBzdWJzZXF1ZW50bHkgYmVlbiBzZW50LlxyXG4gICAgICAgICAgICB0aGlzLl9hZHZhbmNlUmVhZE1hcmtlclBhc3RNeUV2ZW50cygpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBjYW5CYWNrUGFnaW5hdGU6IHRoaXMuX3RpbWVsaW5lV2luZG93LmNhblBhZ2luYXRlKEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTKSxcclxuICAgICAgICAgICAgICAgIGNhbkZvcndhcmRQYWdpbmF0ZTogdGhpcy5fdGltZWxpbmVXaW5kb3cuY2FuUGFnaW5hdGUoRXZlbnRUaW1lbGluZS5GT1JXQVJEUyksXHJcbiAgICAgICAgICAgICAgICB0aW1lbGluZUxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBpbml0aWFsaXNlIHRoZSBzY3JvbGwgc3RhdGUgb2YgdGhlIG1lc3NhZ2UgcGFuZWxcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGlzIHNob3VsZG4ndCBoYXBwZW4gLSB3ZSBrbm93IHdlJ3JlIG1vdW50ZWQgYmVjYXVzZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdlJ3JlIGluIGEgc2V0U3RhdGUgY2FsbGJhY2ssIGFuZCB3ZSBrbm93XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGltZWxpbmVMb2FkaW5nIGlzIG5vdyBmYWxzZSwgc28gcmVuZGVyKCkgc2hvdWxkIGhhdmVcclxuICAgICAgICAgICAgICAgICAgICAvLyBtb3VudGVkIHRoZSBtZXNzYWdlIHBhbmVsLlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiY2FuJ3QgaW5pdGlhbGlzZSBzY3JvbGwgc3RhdGUgYmVjYXVzZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJtZXNzYWdlUGFuZWwgZGlkbid0IGxvYWRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50SWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuY3VycmVudC5zY3JvbGxUb0V2ZW50KGV2ZW50SWQsIHBpeGVsT2Zmc2V0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvZmZzZXRCYXNlKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQuc2Nyb2xsVG9Cb3R0b20oKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbmRSZWFkUmVjZWlwdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBvbkVycm9yID0gKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB0aW1lbGluZUxvYWRpbmc6IGZhbHNlIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFxyXG4gICAgICAgICAgICAgICAgYEVycm9yIGxvYWRpbmcgdGltZWxpbmUgcGFuZWwgYXQgJHtldmVudElkfTogJHtlcnJvcn1gLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG5cclxuICAgICAgICAgICAgbGV0IG9uRmluaXNoZWQ7XHJcblxyXG4gICAgICAgICAgICAvLyBpZiB3ZSB3ZXJlIGdpdmVuIGFuIGV2ZW50IElELCB0aGVuIHdoZW4gdGhlIHVzZXIgY2xvc2VzIHRoZVxyXG4gICAgICAgICAgICAvLyBkaWFsb2csIGxldCdzIGp1bXAgdG8gdGhlIGVuZCBvZiB0aGUgdGltZWxpbmUuIElmIHdlIHdlcmVuJ3QsXHJcbiAgICAgICAgICAgIC8vIHNvbWV0aGluZyBoYXMgZ29uZSBiYWRseSB3cm9uZyBhbmQgcmF0aGVyIHRoYW4gY2F1c2luZyBhIGxvb3Agb2ZcclxuICAgICAgICAgICAgLy8gdW5kaXNtaXNzYWJsZSBkaWFsb2dzLCBsZXQncyBqdXN0IGdpdmUgdXAuXHJcbiAgICAgICAgICAgIGlmIChldmVudElkKSB7XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGdvIHZpYSB0aGUgZGlzcGF0Y2hlciBzbyB0aGF0IHRoZSBVUkwgaXMgdXBkYXRlZFxyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvb21faWQ6IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCBtZXNzYWdlO1xyXG4gICAgICAgICAgICBpZiAoZXJyb3IuZXJyY29kZSA9PSAnTV9GT1JCSURERU4nKSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJUcmllZCB0byBsb2FkIGEgc3BlY2lmaWMgcG9pbnQgaW4gdGhpcyByb29tJ3MgdGltZWxpbmUsIGJ1dCB5b3UgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZG8gbm90IGhhdmUgcGVybWlzc2lvbiB0byB2aWV3IHRoZSBtZXNzYWdlIGluIHF1ZXN0aW9uLlwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlRyaWVkIHRvIGxvYWQgYSBzcGVjaWZpYyBwb2ludCBpbiB0aGlzIHJvb20ncyB0aW1lbGluZSwgYnV0IHdhcyBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ1bmFibGUgdG8gZmluZCBpdC5cIixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGxvYWQgdGltZWxpbmUgcG9zaXRpb24nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkZhaWxlZCB0byBsb2FkIHRpbWVsaW5lIHBvc2l0aW9uXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiBvbkZpbmlzaGVkLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBpZiB3ZSBhbHJlYWR5IGhhdmUgdGhlIGV2ZW50IGluIHF1ZXN0aW9uLCBUaW1lbGluZVdpbmRvdy5sb2FkXHJcbiAgICAgICAgLy8gcmV0dXJucyBhIHJlc29sdmVkIHByb21pc2UuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBJbiB0aGlzIHNpdHVhdGlvbiwgd2UgZG9uJ3QgcmVhbGx5IHdhbnQgdG8gZGVmZXIgdGhlIHVwZGF0ZSBvZiB0aGVcclxuICAgICAgICAvLyBzdGF0ZSB0byB0aGUgbmV4dCBldmVudCBsb29wLCBiZWNhdXNlIGl0IG1ha2VzIHJvb20tc3dpdGNoaW5nIGZlZWxcclxuICAgICAgICAvLyBxdWl0ZSBzbG93LiBTbyB3ZSBkZXRlY3QgdGhhdCBzaXR1YXRpb24gYW5kIHNob3J0Y3V0IHN0cmFpZ2h0IHRvXHJcbiAgICAgICAgLy8gY2FsbGluZyBfcmVsb2FkRXZlbnRzIGFuZCB1cGRhdGluZyB0aGUgc3RhdGUuXHJcblxyXG4gICAgICAgIGNvbnN0IHRpbWVsaW5lID0gdGhpcy5wcm9wcy50aW1lbGluZVNldC5nZXRUaW1lbGluZUZvckV2ZW50KGV2ZW50SWQpO1xyXG4gICAgICAgIGlmICh0aW1lbGluZSkge1xyXG4gICAgICAgICAgICAvLyBUaGlzIGlzIGEgaG90LXBhdGggb3B0aW1pemF0aW9uIGJ5IHNraXBwaW5nIGEgcHJvbWlzZSB0aWNrXHJcbiAgICAgICAgICAgIC8vIGJ5IHJlcGVhdGluZyBhIG5vLW9wIHN5bmMgYnJhbmNoIGluIFRpbWVsaW5lU2V0LmdldFRpbWVsaW5lRm9yRXZlbnQgJiBNYXRyaXhDbGllbnQuZ2V0RXZlbnRUaW1lbGluZVxyXG4gICAgICAgICAgICB0aGlzLl90aW1lbGluZVdpbmRvdy5sb2FkKGV2ZW50SWQsIElOSVRJQUxfU0laRSk7IC8vIGluIHRoaXMgYnJhbmNoIHRoaXMgbWV0aG9kIHdpbGwgaGFwcGVuIGluIHN5bmMgdGltZVxyXG4gICAgICAgICAgICBvbkxvYWRlZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHByb20gPSB0aGlzLl90aW1lbGluZVdpbmRvdy5sb2FkKGV2ZW50SWQsIElOSVRJQUxfU0laRSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXZlbnRzOiBbXSxcclxuICAgICAgICAgICAgICAgIGxpdmVFdmVudHM6IFtdLFxyXG4gICAgICAgICAgICAgICAgY2FuQmFja1BhZ2luYXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNhbkZvcndhcmRQYWdpbmF0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB0aW1lbGluZUxvYWRpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBwcm9tLnRoZW4ob25Mb2FkZWQsIG9uRXJyb3IpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8gaGFuZGxlIHRoZSBjb21wbGV0aW9uIG9mIGEgdGltZWxpbmUgbG9hZCBvciBsb2NhbEVjaG9VcGRhdGUsIGJ5XHJcbiAgICAvLyByZWxvYWRpbmcgdGhlIGV2ZW50cyBmcm9tIHRoZSB0aW1lbGluZXdpbmRvdyBhbmQgcGVuZGluZyBldmVudCBsaXN0IGludG9cclxuICAgIC8vIHRoZSBzdGF0ZS5cclxuICAgIF9yZWxvYWRFdmVudHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHdlIG1pZ2h0IGhhdmUgc3dpdGNoZWQgcm9vbXMgc2luY2UgdGhlIGxvYWQgc3RhcnRlZCAtIGp1c3QgYmluXHJcbiAgICAgICAgLy8gdGhlIHJlc3VsdHMgaWYgc28uXHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUodGhpcy5fZ2V0RXZlbnRzKCkpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBnZXQgdGhlIGxpc3Qgb2YgZXZlbnRzIGZyb20gdGhlIHRpbWVsaW5lIHdpbmRvdyBhbmQgdGhlIHBlbmRpbmcgZXZlbnQgbGlzdFxyXG4gICAgX2dldEV2ZW50czogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgZXZlbnRzID0gdGhpcy5fdGltZWxpbmVXaW5kb3cuZ2V0RXZlbnRzKCk7XHJcbiAgICAgICAgY29uc3QgZmlyc3RWaXNpYmxlRXZlbnRJbmRleCA9IHRoaXMuX2NoZWNrRm9yUHJlSm9pblVJU0koZXZlbnRzKTtcclxuXHJcbiAgICAgICAgLy8gSG9sZCBvbnRvIHRoZSBsaXZlIGV2ZW50cyBzZXBhcmF0ZWx5LiBUaGUgcmVhZCByZWNlaXB0IGFuZCByZWFkIG1hcmtlclxyXG4gICAgICAgIC8vIHNob3VsZCB1c2UgdGhpcyBsaXN0LCBzbyB0aGF0IHRoZXkgZG9uJ3QgYWR2YW5jZSBpbnRvIHBlbmRpbmcgZXZlbnRzLlxyXG4gICAgICAgIGNvbnN0IGxpdmVFdmVudHMgPSBbLi4uZXZlbnRzXTtcclxuXHJcbiAgICAgICAgLy8gaWYgd2UncmUgYXQgdGhlIGVuZCBvZiB0aGUgbGl2ZSB0aW1lbGluZSwgYXBwZW5kIHRoZSBwZW5kaW5nIGV2ZW50c1xyXG4gICAgICAgIGlmICghdGhpcy5fdGltZWxpbmVXaW5kb3cuY2FuUGFnaW5hdGUoRXZlbnRUaW1lbGluZS5GT1JXQVJEUykpIHtcclxuICAgICAgICAgICAgZXZlbnRzLnB1c2goLi4udGhpcy5wcm9wcy50aW1lbGluZVNldC5nZXRQZW5kaW5nRXZlbnRzKCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgZXZlbnRzLFxyXG4gICAgICAgICAgICBsaXZlRXZlbnRzLFxyXG4gICAgICAgICAgICBmaXJzdFZpc2libGVFdmVudEluZGV4LFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgZm9yIHVuZGVjcnlwdGFibGUgbWVzc2FnZXMgdGhhdCB3ZXJlIHNlbnQgd2hpbGUgdGhlIHVzZXIgd2FzIG5vdCBpblxyXG4gICAgICogdGhlIHJvb20uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtBcnJheTxNYXRyaXhFdmVudD59IGV2ZW50cyBUaGUgdGltZWxpbmUgZXZlbnRzIHRvIGNoZWNrXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfSBUaGUgaW5kZXggd2l0aGluIGBldmVudHNgIG9mIHRoZSBldmVudCBhZnRlciB0aGUgbW9zdCByZWNlbnRcclxuICAgICAqIHVuZGVjcnlwdGFibGUgZXZlbnQgdGhhdCB3YXMgc2VudCB3aGlsZSB0aGUgdXNlciB3YXMgbm90IGluIHRoZSByb29tLiAgSWYgbm9cclxuICAgICAqIHN1Y2ggZXZlbnRzIHdlcmUgZm91bmQsIHRoZW4gaXQgcmV0dXJucyAwLlxyXG4gICAgICovXHJcbiAgICBfY2hlY2tGb3JQcmVKb2luVUlTSTogZnVuY3Rpb24oZXZlbnRzKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMucHJvcHMudGltZWxpbmVTZXQucm9vbTtcclxuXHJcbiAgICAgICAgaWYgKGV2ZW50cy5sZW5ndGggPT09IDAgfHwgIXJvb20gfHxcclxuICAgICAgICAgICAgIU1hdHJpeENsaWVudFBlZy5nZXQoKS5pc1Jvb21FbmNyeXB0ZWQocm9vbS5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAwO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdXNlcklkID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZDtcclxuXHJcbiAgICAgICAgLy8gZ2V0IHRoZSB1c2VyJ3MgbWVtYmVyc2hpcCBhdCB0aGUgbGFzdCBldmVudCBieSBnZXR0aW5nIHRoZSB0aW1lbGluZVxyXG4gICAgICAgIC8vIHRoYXQgdGhlIGV2ZW50IGJlbG9uZ3MgdG8sIGFuZCB0cmF2ZXJzaW5nIHRoZSB0aW1lbGluZSBsb29raW5nIGZvclxyXG4gICAgICAgIC8vIHRoYXQgZXZlbnQsIHdoaWxlIGtlZXBpbmcgdHJhY2sgb2YgdGhlIHVzZXIncyBtZW1iZXJzaGlwXHJcbiAgICAgICAgbGV0IGk7XHJcbiAgICAgICAgbGV0IHVzZXJNZW1iZXJzaGlwID0gXCJsZWF2ZVwiO1xyXG4gICAgICAgIGZvciAoaSA9IGV2ZW50cy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG4gICAgICAgICAgICBjb25zdCB0aW1lbGluZSA9IHJvb20uZ2V0VGltZWxpbmVGb3JFdmVudChldmVudHNbaV0uZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgIGlmICghdGltZWxpbmUpIHtcclxuICAgICAgICAgICAgICAgIC8vIFNvbWVob3csIGl0IHNlZW1zIHRvIGJlIHBvc3NpYmxlIGZvciBsaXZlIGV2ZW50cyB0byBub3QgaGF2ZVxyXG4gICAgICAgICAgICAgICAgLy8gYSB0aW1lbGluZSwgZXZlbiB0aG91Z2ggdGhhdCBzaG91bGQgbm90IGhhcHBlbi4gOihcclxuICAgICAgICAgICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzEyMTIwXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXHJcbiAgICAgICAgICAgICAgICAgICAgYEV2ZW50ICR7ZXZlbnRzW2ldLmdldElkKCl9IGluIHJvb20gJHtyb29tLnJvb21JZH0gaXMgbGl2ZSwgYCArXHJcbiAgICAgICAgICAgICAgICAgICAgYGJ1dCBpdCBkb2VzIG5vdCBoYXZlIGEgdGltZWxpbmVgLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJNZW1iZXJzaGlwRXZlbnQgPVxyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVsaW5lLmdldFN0YXRlKEV2ZW50VGltZWxpbmUuRk9SV0FSRFMpLmdldE1lbWJlcih1c2VySWQpO1xyXG4gICAgICAgICAgICB1c2VyTWVtYmVyc2hpcCA9IHVzZXJNZW1iZXJzaGlwRXZlbnQgPyB1c2VyTWVtYmVyc2hpcEV2ZW50Lm1lbWJlcnNoaXAgOiBcImxlYXZlXCI7XHJcbiAgICAgICAgICAgIGNvbnN0IHRpbWVsaW5lRXZlbnRzID0gdGltZWxpbmUuZ2V0RXZlbnRzKCk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGogPSB0aW1lbGluZUV2ZW50cy5sZW5ndGggLSAxOyBqID49IDA7IGotLSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnQgPSB0aW1lbGluZUV2ZW50c1tqXTtcclxuICAgICAgICAgICAgICAgIGlmIChldmVudC5nZXRJZCgpID09PSBldmVudHNbaV0uZ2V0SWQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChldmVudC5nZXRTdGF0ZUtleSgpID09PSB1c2VySWRcclxuICAgICAgICAgICAgICAgICAgICAmJiBldmVudC5nZXRUeXBlKCkgPT09IFwibS5yb29tLm1lbWJlclwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJldkNvbnRlbnQgPSBldmVudC5nZXRQcmV2Q29udGVudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJNZW1iZXJzaGlwID0gcHJldkNvbnRlbnQubWVtYmVyc2hpcCB8fCBcImxlYXZlXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBub3cgZ28gdGhyb3VnaCB0aGUgcmVzdCBvZiB0aGUgZXZlbnRzIGFuZCBmaW5kIHRoZSBmaXJzdCB1bmRlY3J5cHRhYmxlXHJcbiAgICAgICAgLy8gb25lIHRoYXQgd2FzIHNlbnQgd2hlbiB0aGUgdXNlciB3YXNuJ3QgaW4gdGhlIHJvb21cclxuICAgICAgICBmb3IgKDsgaSA+PSAwOyBpLS0pIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBldmVudHNbaV07XHJcbiAgICAgICAgICAgIGlmIChldmVudC5nZXRTdGF0ZUtleSgpID09PSB1c2VySWRcclxuICAgICAgICAgICAgICAgICYmIGV2ZW50LmdldFR5cGUoKSA9PT0gXCJtLnJvb20ubWVtYmVyXCIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHByZXZDb250ZW50ID0gZXZlbnQuZ2V0UHJldkNvbnRlbnQoKTtcclxuICAgICAgICAgICAgICAgIHVzZXJNZW1iZXJzaGlwID0gcHJldkNvbnRlbnQubWVtYmVyc2hpcCB8fCBcImxlYXZlXCI7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodXNlck1lbWJlcnNoaXAgPT09IFwibGVhdmVcIiAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgIChldmVudC5pc0RlY3J5cHRpb25GYWlsdXJlKCkgfHwgZXZlbnQuaXNCZWluZ0RlY3J5cHRlZCgpKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gcmVhY2hlZCBhbiB1bmRlY3J5cHRhYmxlIG1lc3NhZ2Ugd2hlbiB0aGUgdXNlciB3YXNuJ3QgaW5cclxuICAgICAgICAgICAgICAgIC8vIHRoZSByb29tIC0tIGRvbid0IHRyeSB0byBsb2FkIGFueSBtb3JlXHJcbiAgICAgICAgICAgICAgICAvLyBOb3RlOiBmb3Igbm93LCB3ZSBhc3N1bWUgdGhhdCBldmVudHMgdGhhdCBhcmUgYmVpbmcgZGVjcnlwdGVkIGFyZVxyXG4gICAgICAgICAgICAgICAgLy8gbm90IGRlY3J5cHRhYmxlXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaSArIDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDA7XHJcbiAgICB9LFxyXG5cclxuICAgIF9pbmRleEZvckV2ZW50SWQ6IGZ1bmN0aW9uKGV2SWQpIHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3RhdGUuZXZlbnRzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGlmIChldklkID09IHRoaXMuc3RhdGUuZXZlbnRzW2ldLmdldElkKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0TGFzdERpc3BsYXllZEV2ZW50SW5kZXg6IGZ1bmN0aW9uKG9wdHMpIHtcclxuICAgICAgICBvcHRzID0gb3B0cyB8fCB7fTtcclxuICAgICAgICBjb25zdCBpZ25vcmVPd24gPSBvcHRzLmlnbm9yZU93biB8fCBmYWxzZTtcclxuICAgICAgICBjb25zdCBhbGxvd1BhcnRpYWwgPSBvcHRzLmFsbG93UGFydGlhbCB8fCBmYWxzZTtcclxuXHJcbiAgICAgICAgY29uc3QgbWVzc2FnZVBhbmVsID0gdGhpcy5fbWVzc2FnZVBhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgaWYgKCFtZXNzYWdlUGFuZWwpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBjb25zdCBtZXNzYWdlUGFuZWxOb2RlID0gUmVhY3RET00uZmluZERPTU5vZGUobWVzc2FnZVBhbmVsKTtcclxuICAgICAgICBpZiAoIW1lc3NhZ2VQYW5lbE5vZGUpIHJldHVybiBudWxsOyAvLyBzb21ldGltZXMgdGhpcyBoYXBwZW5zIGZvciBmcmVzaCByb29tcy9wb3N0LXN5bmNcclxuICAgICAgICBjb25zdCB3cmFwcGVyUmVjdCA9IG1lc3NhZ2VQYW5lbE5vZGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgY29uc3QgbXlVc2VySWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY3JlZGVudGlhbHMudXNlcklkO1xyXG5cclxuICAgICAgICBjb25zdCBpc05vZGVJblZpZXcgPSAobm9kZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYm91bmRpbmdSZWN0ID0gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgICAgICAgICAgIGlmICgoYWxsb3dQYXJ0aWFsICYmIGJvdW5kaW5nUmVjdC50b3AgPCB3cmFwcGVyUmVjdC5ib3R0b20pIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgKCFhbGxvd1BhcnRpYWwgJiYgYm91bmRpbmdSZWN0LmJvdHRvbSA8IHdyYXBwZXJSZWN0LmJvdHRvbSkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gV2Uga2VlcCB0cmFjayBvZiBob3cgbWFueSBvZiB0aGUgYWRqYWNlbnQgZXZlbnRzIGRpZG4ndCBoYXZlIGEgdGlsZVxyXG4gICAgICAgIC8vIGJ1dCBzaG91bGQgaGF2ZSB0aGUgcmVhZCByZWNlaXB0IG1vdmVkIHBhc3QgdGhlbSwgc29cclxuICAgICAgICAvLyB3ZSBjYW4gaW5jbHVkZSB0aG9zZSBvbmNlIHdlIGZpbmQgdGhlIGxhc3QgZGlzcGxheWVkICh2aXNpYmxlKSBldmVudC5cclxuICAgICAgICAvLyBUaGUgY291bnRlciBpcyBub3Qgc3RhcnRlZCBmb3IgZXZlbnRzIHdlIGRvbid0IHdhbnRcclxuICAgICAgICAvLyB0byBzZW5kIGEgcmVhZCByZWNlaXB0IGZvciAob3VyIG93biBldmVudHMsIGxvY2FsIGVjaG9zKS5cclxuICAgICAgICBsZXQgYWRqYWNlbnRJbnZpc2libGVFdmVudENvdW50ID0gMDtcclxuICAgICAgICAvLyBVc2UgYGxpdmVFdmVudHNgIGhlcmUgYmVjYXVzZSB3ZSBkb24ndCB3YW50IHRoZSByZWFkIG1hcmtlciBvciByZWFkXHJcbiAgICAgICAgLy8gcmVjZWlwdCB0byBhZHZhbmNlIGludG8gcGVuZGluZyBldmVudHMuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuc3RhdGUubGl2ZUV2ZW50cy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IHRoaXMuc3RhdGUubGl2ZUV2ZW50c1tpXTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG5vZGUgPSBtZXNzYWdlUGFuZWwuZ2V0Tm9kZUZvckV2ZW50SWQoZXYuZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzSW5WaWV3ID0gaXNOb2RlSW5WaWV3KG5vZGUpO1xyXG5cclxuICAgICAgICAgICAgLy8gd2hlbiB3ZSd2ZSByZWFjaGVkIHRoZSBmaXJzdCB2aXNpYmxlIGV2ZW50LCBhbmQgdGhlIHByZXZpb3VzXHJcbiAgICAgICAgICAgIC8vIGV2ZW50cyB3ZXJlIGFsbCBpbnZpc2libGUgKHdpdGggdGhlIGZpcnN0IG9uZSBub3QgYmVpbmcgaWdub3JlZCksXHJcbiAgICAgICAgICAgIC8vIHJldHVybiB0aGUgaW5kZXggb2YgdGhlIGZpcnN0IGludmlzaWJsZSBldmVudC5cclxuICAgICAgICAgICAgaWYgKGlzSW5WaWV3ICYmIGFkamFjZW50SW52aXNpYmxlRXZlbnRDb3VudCAhPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGkgKyBhZGphY2VudEludmlzaWJsZUV2ZW50Q291bnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKG5vZGUgJiYgIWlzSW5WaWV3KSB7XHJcbiAgICAgICAgICAgICAgICAvLyBoYXMgbm9kZSBidXQgbm90IGluIHZpZXcsIHNvIHJlc2V0IGFkamFjZW50IGludmlzaWJsZSBldmVudHNcclxuICAgICAgICAgICAgICAgIGFkamFjZW50SW52aXNpYmxlRXZlbnRDb3VudCA9IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHNob3VsZElnbm9yZSA9ICEhZXYuc3RhdHVzIHx8IC8vIGxvY2FsIGVjaG9cclxuICAgICAgICAgICAgICAgIChpZ25vcmVPd24gJiYgZXYuc2VuZGVyICYmIGV2LnNlbmRlci51c2VySWQgPT0gbXlVc2VySWQpOyAgIC8vIG93biBtZXNzYWdlXHJcbiAgICAgICAgICAgIGNvbnN0IGlzV2l0aG91dFRpbGUgPSAhaGF2ZVRpbGVGb3JFdmVudChldikgfHwgc2hvdWxkSGlkZUV2ZW50KGV2KTtcclxuXHJcbiAgICAgICAgICAgIGlmIChpc1dpdGhvdXRUaWxlIHx8ICFub2RlKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBkb24ndCBzdGFydCBjb3VudGluZyBpZiB0aGUgZXZlbnQgc2hvdWxkIGJlIGlnbm9yZWQsXHJcbiAgICAgICAgICAgICAgICAvLyBidXQgY29udGludWUgY291bnRpbmcgaWYgd2Ugd2VyZSBhbHJlYWR5IHNvIHRoZSBvZmZzZXRcclxuICAgICAgICAgICAgICAgIC8vIHRvIHRoZSBwcmV2aW91cyBpbnZpc2JsZSBldmVudCB0aGF0IGRpZG4ndCBuZWVkIHRvIGJlIGlnbm9yZWRcclxuICAgICAgICAgICAgICAgIC8vIGRvZXNuJ3QgZ2V0IG1lc3NlZCB1cFxyXG4gICAgICAgICAgICAgICAgaWYgKCFzaG91bGRJZ25vcmUgfHwgKHNob3VsZElnbm9yZSAmJiBhZGphY2VudEludmlzaWJsZUV2ZW50Q291bnQgIT09IDApKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgKythZGphY2VudEludmlzaWJsZUV2ZW50Q291bnQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHNob3VsZElnbm9yZSkge1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChpc0luVmlldykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgaWQgb2YgdGhlIGV2ZW50IGNvcnJlc3BvbmRpbmcgdG8gb3VyIHVzZXIncyBsYXRlc3QgcmVhZC1yZWNlaXB0LlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gaWdub3JlU3ludGhlc2l6ZWQgSWYgdHJ1ZSwgcmV0dXJuIG9ubHkgcmVjZWlwdHMgdGhhdFxyXG4gICAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXZlIGJlZW4gc2VudCBieSB0aGUgc2VydmVyLCBub3RcclxuICAgICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1wbGljaXQgb25lcyBnZW5lcmF0ZWQgYnkgdGhlIEpTXHJcbiAgICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFNESy5cclxuICAgICAqIEByZXR1cm4ge1N0cmluZ30gdGhlIGV2ZW50IElEXHJcbiAgICAgKi9cclxuICAgIF9nZXRDdXJyZW50UmVhZFJlY2VpcHQ6IGZ1bmN0aW9uKGlnbm9yZVN5bnRoZXNpemVkKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIC8vIHRoZSBjbGllbnQgY2FuIGJlIG51bGwgb24gbG9nb3V0XHJcbiAgICAgICAgaWYgKGNsaWVudCA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbXlVc2VySWQgPSBjbGllbnQuY3JlZGVudGlhbHMudXNlcklkO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb20uZ2V0RXZlbnRSZWFkVXBUbyhteVVzZXJJZCwgaWdub3JlU3ludGhlc2l6ZWQpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UmVhZE1hcmtlcjogZnVuY3Rpb24oZXZlbnRJZCwgZXZlbnRUcywgaW5oaWJpdFNldFN0YXRlKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gdGhpcy5wcm9wcy50aW1lbGluZVNldC5yb29tLnJvb21JZDtcclxuXHJcbiAgICAgICAgLy8gZG9uJ3QgdXBkYXRlIHRoZSBzdGF0ZSAoYW5kIGNhdXNlIGEgcmUtcmVuZGVyKSBpZiB0aGVyZSBpc1xyXG4gICAgICAgIC8vIG5vIGNoYW5nZSB0byB0aGUgUk0uXHJcbiAgICAgICAgaWYgKGV2ZW50SWQgPT09IHRoaXMuc3RhdGUucmVhZE1hcmtlckV2ZW50SWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaW4gb3JkZXIgdG8gbGF0ZXIgZmlndXJlIG91dCBpZiB0aGUgcmVhZCBtYXJrZXIgaXNcclxuICAgICAgICAvLyBhYm92ZSBvciBiZWxvdyB0aGUgdmlzaWJsZSB0aW1lbGluZSwgd2Ugc3Rhc2ggdGhlIHRpbWVzdGFtcC5cclxuICAgICAgICBUaW1lbGluZVBhbmVsLnJvb21SZWFkTWFya2VyVHNNYXBbcm9vbUlkXSA9IGV2ZW50VHM7XHJcblxyXG4gICAgICAgIGlmIChpbmhpYml0U2V0U3RhdGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRG8gdGhlIGxvY2FsIGVjaG8gb2YgdGhlIFJNXHJcbiAgICAgICAgLy8gcnVuIHRoZSByZW5kZXIgY3ljbGUgYmVmb3JlIGNhbGxpbmcgdGhlIGNhbGxiYWNrLCBzbyB0aGF0XHJcbiAgICAgICAgLy8gZ2V0UmVhZE1hcmtlclBvc2l0aW9uKCkgcmV0dXJucyB0aGUgcmlnaHQgdGhpbmcuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJlYWRNYXJrZXJFdmVudElkOiBldmVudElkLFxyXG4gICAgICAgIH0sIHRoaXMucHJvcHMub25SZWFkTWFya2VyVXBkYXRlZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9zaG91bGRQYWdpbmF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gZG9uJ3QgdHJ5IHRvIHBhZ2luYXRlIHdoaWxlIGV2ZW50cyBpbiB0aGUgdGltZWxpbmUgYXJlXHJcbiAgICAgICAgLy8gc3RpbGwgYmVpbmcgZGVjcnlwdGVkLiBXZSBkb24ndCByZW5kZXIgZXZlbnRzIHdoaWxlIHRoZXkncmVcclxuICAgICAgICAvLyBiZWluZyBkZWNyeXB0ZWQsIHNvIHRoZXkgZG9uJ3QgdGFrZSB1cCBzcGFjZSBpbiB0aGUgdGltZWxpbmUuXHJcbiAgICAgICAgLy8gVGhpcyBtZWFucyB3ZSBjYW4gcHVsbCBxdWl0ZSBhIGxvdCBvZiBldmVudHMgaW50byB0aGUgdGltZWxpbmVcclxuICAgICAgICAvLyBhbmQgZW5kIHVwIHRyeWluZyB0byByZW5kZXIgYSBsb3Qgb2YgZXZlbnRzLlxyXG4gICAgICAgIHJldHVybiAhdGhpcy5zdGF0ZS5ldmVudHMuc29tZSgoZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZS5pc0JlaW5nRGVjcnlwdGVkKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFJlbGF0aW9uc0ZvckV2ZW50KC4uLmFyZ3MpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy50aW1lbGluZVNldC5nZXRSZWxhdGlvbnNGb3JFdmVudCguLi5hcmdzKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBNZXNzYWdlUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KFwic3RydWN0dXJlcy5NZXNzYWdlUGFuZWxcIik7XHJcbiAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcblxyXG4gICAgICAgIC8vIGp1c3Qgc2hvdyBhIHNwaW5uZXIgd2hpbGUgdGhlIHRpbWVsaW5lIGxvYWRzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gcHV0IGl0IGluIGEgZGl2IG9mIHRoZSByaWdodCBjbGFzcyAobXhfUm9vbVZpZXdfbWVzc2FnZVBhbmVsKSBzb1xyXG4gICAgICAgIC8vIHRoYXQgdGhlIG9yZGVyIGluIHRoZSByb29tdmlldyBmbGV4Ym94IGlzIGNvcnJlY3QsIGFuZFxyXG4gICAgICAgIC8vIG14X1Jvb21WaWV3X21lc3NhZ2VMaXN0V3JhcHBlciB0byBwb3NpdGlvbiB0aGUgaW5uZXIgZGl2IGluIHRoZVxyXG4gICAgICAgIC8vIHJpZ2h0IHBsYWNlLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gTm90ZSB0aGF0IHRoZSBjbGljay1vbi1zZWFyY2gtcmVzdWx0IGZ1bmN0aW9uYWxpdHkgcmVsaWVzIG9uIHRoZVxyXG4gICAgICAgIC8vIGZhY3QgdGhhdCB0aGUgbWVzc2FnZVBhbmVsIGlzIGhpZGRlbiB3aGlsZSB0aGUgdGltZWxpbmUgcmVsb2FkcyxcclxuICAgICAgICAvLyBidXQgdGhhdCB0aGUgUm9vbUhlYWRlciAoY29tcGxldGUgd2l0aCBzZWFyY2ggdGVybSkgY29udGludWVzIHRvXHJcbiAgICAgICAgLy8gZXhpc3QuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudGltZWxpbmVMb2FkaW5nKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X21lc3NhZ2VQYW5lbFNwaW5uZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8TG9hZGVyIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmV2ZW50cy5sZW5ndGggPT0gMCAmJiAhdGhpcy5zdGF0ZS5jYW5CYWNrUGFnaW5hdGUgJiYgdGhpcy5wcm9wcy5lbXB0eSkge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3RoaXMucHJvcHMuY2xhc3NOYW1lICsgXCIgbXhfUm9vbVZpZXdfbWVzc2FnZUxpc3RXcmFwcGVyXCJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfZW1wdHlcIj57dGhpcy5wcm9wcy5lbXB0eX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZ2l2ZSB0aGUgbWVzc2FnZXBhbmVsIGEgc3RpY2t5Ym90dG9tIGlmIHdlJ3JlIGF0IHRoZSBlbmQgb2YgdGhlXHJcbiAgICAgICAgLy8gbGl2ZSB0aW1lbGluZSwgc28gdGhhdCB0aGUgYXJyaXZhbCBvZiBuZXcgZXZlbnRzIHRyaWdnZXJzIGFcclxuICAgICAgICAvLyBzY3JvbGwuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBNYWtlIHN1cmUgdGhhdCBzdGlja3lCb3R0b20gaXMgKmZhbHNlKiBpZiB3ZSBjYW4gcGFnaW5hdGVcclxuICAgICAgICAvLyBmb3J3YXJkcywgb3RoZXJ3aXNlIGlmIHNvbWVib2R5IGhpdHMgdGhlIGJvdHRvbSBvZiB0aGUgbG9hZGVkXHJcbiAgICAgICAgLy8gZXZlbnRzIHdoZW4gdmlld2luZyBoaXN0b3JpY2FsIG1lc3NhZ2VzLCB3ZSBnZXQgc3R1Y2sgaW4gYSBsb29wXHJcbiAgICAgICAgLy8gb2YgcGFnaW5hdGluZyBvdXIgd2F5IHRocm91Z2ggdGhlIGVudGlyZSBoaXN0b3J5IG9mIHRoZSByb29tLlxyXG4gICAgICAgIGNvbnN0IHN0aWNreUJvdHRvbSA9ICF0aGlzLl90aW1lbGluZVdpbmRvdy5jYW5QYWdpbmF0ZShFdmVudFRpbWVsaW5lLkZPUldBUkRTKTtcclxuXHJcbiAgICAgICAgLy8gSWYgdGhlIHN0YXRlIGlzIFBSRVBBUkVEIG9yIENBVENIVVAsIHdlJ3JlIHN0aWxsIHdhaXRpbmcgZm9yIHRoZSBqcy1zZGsgdG8gc3luYyB3aXRoXHJcbiAgICAgICAgLy8gdGhlIEhTIGFuZCBmZXRjaCB0aGUgbGF0ZXN0IGV2ZW50cywgc28gd2UgYXJlIGVmZmVjdGl2ZWx5IGZvcndhcmQgcGFnaW5hdGluZy5cclxuICAgICAgICBjb25zdCBmb3J3YXJkUGFnaW5hdGluZyA9IChcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5mb3J3YXJkUGFnaW5hdGluZyB8fFxyXG4gICAgICAgICAgICBbJ1BSRVBBUkVEJywgJ0NBVENIVVAnXS5pbmNsdWRlcyh0aGlzLnN0YXRlLmNsaWVudFN5bmNTdGF0ZSlcclxuICAgICAgICApO1xyXG4gICAgICAgIGNvbnN0IGV2ZW50cyA9IHRoaXMuc3RhdGUuZmlyc3RWaXNpYmxlRXZlbnRJbmRleFxyXG4gICAgICAgICAgICAgID8gdGhpcy5zdGF0ZS5ldmVudHMuc2xpY2UodGhpcy5zdGF0ZS5maXJzdFZpc2libGVFdmVudEluZGV4KVxyXG4gICAgICAgICAgICAgIDogdGhpcy5zdGF0ZS5ldmVudHM7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPE1lc3NhZ2VQYW5lbFxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9tZXNzYWdlUGFuZWx9XHJcbiAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnRpbWVsaW5lU2V0LnJvb219XHJcbiAgICAgICAgICAgICAgICBwZXJtYWxpbmtDcmVhdG9yPXt0aGlzLnByb3BzLnBlcm1hbGlua0NyZWF0b3J9XHJcbiAgICAgICAgICAgICAgICBoaWRkZW49e3RoaXMucHJvcHMuaGlkZGVufVxyXG4gICAgICAgICAgICAgICAgYmFja1BhZ2luYXRpbmc9e3RoaXMuc3RhdGUuYmFja1BhZ2luYXRpbmd9XHJcbiAgICAgICAgICAgICAgICBmb3J3YXJkUGFnaW5hdGluZz17Zm9yd2FyZFBhZ2luYXRpbmd9XHJcbiAgICAgICAgICAgICAgICBldmVudHM9e2V2ZW50c31cclxuICAgICAgICAgICAgICAgIGhpZ2hsaWdodGVkRXZlbnRJZD17dGhpcy5wcm9wcy5oaWdobGlnaHRlZEV2ZW50SWR9XHJcbiAgICAgICAgICAgICAgICByZWFkTWFya2VyRXZlbnRJZD17dGhpcy5zdGF0ZS5yZWFkTWFya2VyRXZlbnRJZH1cclxuICAgICAgICAgICAgICAgIHJlYWRNYXJrZXJWaXNpYmxlPXt0aGlzLnN0YXRlLnJlYWRNYXJrZXJWaXNpYmxlfVxyXG4gICAgICAgICAgICAgICAgc3VwcHJlc3NGaXJzdERhdGVTZXBhcmF0b3I9e3RoaXMuc3RhdGUuY2FuQmFja1BhZ2luYXRlfVxyXG4gICAgICAgICAgICAgICAgc2hvd1VybFByZXZpZXc9e3RoaXMucHJvcHMuc2hvd1VybFByZXZpZXd9XHJcbiAgICAgICAgICAgICAgICBzaG93UmVhZFJlY2VpcHRzPXt0aGlzLnByb3BzLnNob3dSZWFkUmVjZWlwdHN9XHJcbiAgICAgICAgICAgICAgICBvdXJVc2VySWQ9e01hdHJpeENsaWVudFBlZy5nZXQoKS5jcmVkZW50aWFscy51c2VySWR9XHJcbiAgICAgICAgICAgICAgICBzdGlja3lCb3R0b209e3N0aWNreUJvdHRvbX1cclxuICAgICAgICAgICAgICAgIG9uU2Nyb2xsPXt0aGlzLm9uTWVzc2FnZUxpc3RTY3JvbGx9XHJcbiAgICAgICAgICAgICAgICBvbkZpbGxSZXF1ZXN0PXt0aGlzLm9uTWVzc2FnZUxpc3RGaWxsUmVxdWVzdH1cclxuICAgICAgICAgICAgICAgIG9uVW5maWxsUmVxdWVzdD17dGhpcy5vbk1lc3NhZ2VMaXN0VW5maWxsUmVxdWVzdH1cclxuICAgICAgICAgICAgICAgIGlzVHdlbHZlSG91cj17dGhpcy5zdGF0ZS5pc1R3ZWx2ZUhvdXJ9XHJcbiAgICAgICAgICAgICAgICBhbHdheXNTaG93VGltZXN0YW1wcz17dGhpcy5zdGF0ZS5hbHdheXNTaG93VGltZXN0YW1wc31cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc05hbWV9XHJcbiAgICAgICAgICAgICAgICB0aWxlU2hhcGU9e3RoaXMucHJvcHMudGlsZVNoYXBlfVxyXG4gICAgICAgICAgICAgICAgcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9XHJcbiAgICAgICAgICAgICAgICBnZXRSZWxhdGlvbnNGb3JFdmVudD17dGhpcy5nZXRSZWxhdGlvbnNGb3JFdmVudH1cclxuICAgICAgICAgICAgICAgIGVkaXRTdGF0ZT17dGhpcy5zdGF0ZS5lZGl0U3RhdGV9XHJcbiAgICAgICAgICAgICAgICBzaG93UmVhY3Rpb25zPXt0aGlzLnByb3BzLnNob3dSZWFjdGlvbnN9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgVGltZWxpbmVQYW5lbDtcclxuIl19