"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMenu = createMenu;
exports.default = exports.useContextMenu = exports.aboveLeftOf = exports.toRightOf = exports.MenuItemRadio = exports.MenuItemCheckbox = exports.MenuGroup = exports.MenuItem = exports.ContextMenuButton = exports.ContextMenu = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Keyboard = require("../../Keyboard");

var sdk = _interopRequireWildcard(require("../../index"));

var _AccessibleButton = _interopRequireDefault(require("../views/elements/AccessibleButton"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// Shamelessly ripped off Modal.js.  There's probably a better way
// of doing reusable widgets like dialog boxes & menus where we go and
// pass in a custom control as the actual body.
const ContextualMenuContainerId = "mx_ContextualMenu_Container";

function getOrCreateContainer() {
  let container = document.getElementById(ContextualMenuContainerId);

  if (!container) {
    container = document.createElement("div");
    container.id = ContextualMenuContainerId;
    document.body.appendChild(container);
  }

  return container;
}

const ARIA_MENU_ITEM_ROLES = new Set(["menuitem", "menuitemcheckbox", "menuitemradio"]); // Generic ContextMenu Portal wrapper
// all options inside the menu should be of role=menuitem/menuitemcheckbox/menuitemradiobutton and have tabIndex={-1}
// this will allow the ContextMenu to manage its own focus using arrow keys as per the ARIA guidelines.

class ContextMenu extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "collectContextMenuRect", element => {
      // We don't need to clean up when unmounting, so ignore
      if (!element) return;
      let first = element.querySelector('[role^="menuitem"]');

      if (!first) {
        first = element.querySelector('[tab-index]');
      }

      if (first) {
        first.focus();
      }

      this.setState({
        contextMenuElem: element
      });
    });
    (0, _defineProperty2.default)(this, "onContextMenu", e => {
      if (this.props.onFinished) {
        this.props.onFinished();
        e.preventDefault();
        const x = e.clientX;
        const y = e.clientY; // XXX: This isn't pretty but the only way to allow opening a different context menu on right click whilst
        // a context menu and its click-guard are up without completely rewriting how the context menus work.

        setImmediate(() => {
          const clickEvent = document.createEvent('MouseEvents');
          clickEvent.initMouseEvent('contextmenu', true, true, window, 0, 0, 0, x, y, false, false, false, false, 0, null);
          document.elementFromPoint(x, y).dispatchEvent(clickEvent);
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onMoveFocus", (element, up) => {
      let descending = false; // are we currently descending or ascending through the DOM tree?

      do {
        const child = up ? element.lastElementChild : element.firstElementChild;
        const sibling = up ? element.previousElementSibling : element.nextElementSibling;

        if (descending) {
          if (child) {
            element = child;
          } else if (sibling) {
            element = sibling;
          } else {
            descending = false;
            element = element.parentElement;
          }
        } else {
          if (sibling) {
            element = sibling;
            descending = true;
          } else {
            element = element.parentElement;
          }
        }

        if (element) {
          if (element.classList.contains("mx_ContextualMenu")) {
            // we hit the top
            element = up ? element.lastElementChild : element.firstElementChild;
            descending = true;
          }
        }
      } while (element && !ARIA_MENU_ITEM_ROLES.has(element.getAttribute("role")));

      if (element) {
        element.focus();
      }
    });
    (0, _defineProperty2.default)(this, "_onMoveFocusHomeEnd", (element, up) => {
      let results = element.querySelectorAll('[role^="menuitem"]');

      if (!results) {
        results = element.querySelectorAll('[tab-index]');
      }

      if (results && results.length) {
        if (up) {
          results[0].focus();
        } else {
          results[results.length - 1].focus();
        }
      }
    });
    (0, _defineProperty2.default)(this, "_onKeyDown", ev => {
      if (!this.props.managed) {
        if (ev.key === _Keyboard.Key.ESCAPE) {
          this.props.onFinished();
          ev.stopPropagation();
          ev.preventDefault();
        }

        return;
      }

      let handled = true;

      switch (ev.key) {
        case _Keyboard.Key.TAB:
        case _Keyboard.Key.ESCAPE:
          this.props.onFinished();
          break;

        case _Keyboard.Key.ARROW_UP:
          this._onMoveFocus(ev.target, true);

          break;

        case _Keyboard.Key.ARROW_DOWN:
          this._onMoveFocus(ev.target, false);

          break;

        case _Keyboard.Key.HOME:
          this._onMoveFocusHomeEnd(this.state.contextMenuElem, true);

          break;

        case _Keyboard.Key.END:
          this._onMoveFocusHomeEnd(this.state.contextMenuElem, false);

          break;

        default:
          handled = false;
      }

      if (handled) {
        // consume all other keys in context menu
        ev.stopPropagation();
        ev.preventDefault();
      }
    });
    this.state = {
      contextMenuElem: null
    }; // persist what had focus when we got initialized so we can return it after

    this.initialFocus = document.activeElement;
  }

  componentWillUnmount() {
    // return focus to the thing which had it before us
    this.initialFocus.focus();
  }

  renderMenu(hasBackground = this.props.hasBackground) {
    const position = {};
    let chevronFace = null;
    const props = this.props;

    if (props.top) {
      position.top = props.top;
    } else {
      position.bottom = props.bottom;
    }

    if (props.left) {
      position.left = props.left;
      chevronFace = 'left';
    } else {
      position.right = props.right;
      chevronFace = 'right';
    }

    const contextMenuRect = this.state.contextMenuElem ? this.state.contextMenuElem.getBoundingClientRect() : null;
    const padding = 10;
    const chevronOffset = {};

    if (props.chevronFace) {
      chevronFace = props.chevronFace;
    }

    const hasChevron = chevronFace && chevronFace !== "none";

    if (chevronFace === 'top' || chevronFace === 'bottom') {
      chevronOffset.left = props.chevronOffset;
    } else if (position.top !== undefined) {
      const target = position.top; // By default, no adjustment is made

      let adjusted = target; // If we know the dimensions of the context menu, adjust its position
      // such that it does not leave the (padded) window.

      if (contextMenuRect) {
        adjusted = Math.min(position.top, document.body.clientHeight - contextMenuRect.height - padding);
      }

      position.top = adjusted;
      chevronOffset.top = Math.max(props.chevronOffset, props.chevronOffset + target - adjusted);
    }

    let chevron;

    if (hasChevron) {
      chevron = _react.default.createElement("div", {
        style: chevronOffset,
        className: "mx_ContextualMenu_chevron_" + chevronFace
      });
    }

    const menuClasses = (0, _classnames.default)({
      'mx_ContextualMenu': true,
      'mx_ContextualMenu_left': !hasChevron && position.left,
      'mx_ContextualMenu_right': !hasChevron && position.right,
      'mx_ContextualMenu_top': !hasChevron && position.top,
      'mx_ContextualMenu_bottom': !hasChevron && position.bottom,
      'mx_ContextualMenu_withChevron_left': chevronFace === 'left',
      'mx_ContextualMenu_withChevron_right': chevronFace === 'right',
      'mx_ContextualMenu_withChevron_top': chevronFace === 'top',
      'mx_ContextualMenu_withChevron_bottom': chevronFace === 'bottom'
    });
    const menuStyle = {};

    if (props.menuWidth) {
      menuStyle.width = props.menuWidth;
    }

    if (props.menuHeight) {
      menuStyle.height = props.menuHeight;
    }

    if (!isNaN(Number(props.menuPaddingTop))) {
      menuStyle["paddingTop"] = props.menuPaddingTop;
    }

    if (!isNaN(Number(props.menuPaddingLeft))) {
      menuStyle["paddingLeft"] = props.menuPaddingLeft;
    }

    if (!isNaN(Number(props.menuPaddingBottom))) {
      menuStyle["paddingBottom"] = props.menuPaddingBottom;
    }

    if (!isNaN(Number(props.menuPaddingRight))) {
      menuStyle["paddingRight"] = props.menuPaddingRight;
    }

    const wrapperStyle = {};

    if (!isNaN(Number(props.zIndex))) {
      menuStyle["zIndex"] = props.zIndex + 1;
      wrapperStyle["zIndex"] = props.zIndex;
    }

    let background;

    if (hasBackground) {
      background = _react.default.createElement("div", {
        className: "mx_ContextualMenu_background",
        style: wrapperStyle,
        onClick: props.onFinished,
        onContextMenu: this.onContextMenu
      });
    }

    return _react.default.createElement("div", {
      className: "mx_ContextualMenu_wrapper",
      style: _objectSpread({}, position, {}, wrapperStyle),
      onKeyDown: this._onKeyDown
    }, _react.default.createElement("div", {
      className: menuClasses,
      style: menuStyle,
      ref: this.collectContextMenuRect,
      role: this.props.managed ? "menu" : undefined
    }, chevron, props.children), background);
  }

  render() {
    return _reactDom.default.createPortal(this.renderMenu(), getOrCreateContainer());
  }

} // Semantic component for representing the AccessibleButton which launches a <ContextMenu />


exports.ContextMenu = ContextMenu;
(0, _defineProperty2.default)(ContextMenu, "propTypes", {
  top: _propTypes.default.number,
  bottom: _propTypes.default.number,
  left: _propTypes.default.number,
  right: _propTypes.default.number,
  menuWidth: _propTypes.default.number,
  menuHeight: _propTypes.default.number,
  chevronOffset: _propTypes.default.number,
  chevronFace: _propTypes.default.string,
  // top, bottom, left, right or none
  // Function to be called on menu close
  onFinished: _propTypes.default.func.isRequired,
  menuPaddingTop: _propTypes.default.number,
  menuPaddingRight: _propTypes.default.number,
  menuPaddingBottom: _propTypes.default.number,
  menuPaddingLeft: _propTypes.default.number,
  zIndex: _propTypes.default.number,
  // If true, insert an invisible screen-sized element behind the
  // menu that when clicked will close it.
  hasBackground: _propTypes.default.bool,
  // on resize callback
  windowResize: _propTypes.default.func,
  managed: _propTypes.default.bool // whether this context menu should be focus managed. If false it must handle itself

});
(0, _defineProperty2.default)(ContextMenu, "defaultProps", {
  hasBackground: true,
  managed: true
});

const ContextMenuButton = (_ref) => {
  let {
    label,
    isExpanded,
    children
  } = _ref,
      props = (0, _objectWithoutProperties2.default)(_ref, ["label", "isExpanded", "children"]);
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
  return _react.default.createElement(AccessibleButton, (0, _extends2.default)({}, props, {
    title: label,
    "aria-label": label,
    "aria-haspopup": true,
    "aria-expanded": isExpanded
  }), children);
};

exports.ContextMenuButton = ContextMenuButton;
ContextMenuButton.propTypes = _objectSpread({}, _AccessibleButton.default.propTypes, {
  label: _propTypes.default.string,
  isExpanded: _propTypes.default.bool.isRequired // whether or not the context menu is currently open

}); // Semantic component for representing a role=menuitem

const MenuItem = (_ref2) => {
  let {
    children,
    label
  } = _ref2,
      props = (0, _objectWithoutProperties2.default)(_ref2, ["children", "label"]);
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
  return _react.default.createElement(AccessibleButton, (0, _extends2.default)({}, props, {
    role: "menuitem",
    tabIndex: -1,
    "aria-label": label
  }), children);
};

exports.MenuItem = MenuItem;
MenuItem.propTypes = _objectSpread({}, _AccessibleButton.default.propTypes, {
  label: _propTypes.default.string,
  // optional
  className: _propTypes.default.string,
  // optional
  onClick: _propTypes.default.func.isRequired
}); // Semantic component for representing a role=group for grouping menu radios/checkboxes

const MenuGroup = (_ref3) => {
  let {
    children,
    label
  } = _ref3,
      props = (0, _objectWithoutProperties2.default)(_ref3, ["children", "label"]);
  return _react.default.createElement("div", (0, _extends2.default)({}, props, {
    role: "group",
    "aria-label": label
  }), children);
};

exports.MenuGroup = MenuGroup;
MenuGroup.propTypes = {
  label: _propTypes.default.string.isRequired,
  className: _propTypes.default.string // optional

}; // Semantic component for representing a role=menuitemcheckbox

const MenuItemCheckbox = (_ref4) => {
  let {
    children,
    label,
    active = false,
    disabled = false
  } = _ref4,
      props = (0, _objectWithoutProperties2.default)(_ref4, ["children", "label", "active", "disabled"]);
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
  return _react.default.createElement(AccessibleButton, (0, _extends2.default)({}, props, {
    role: "menuitemcheckbox",
    "aria-checked": active,
    "aria-disabled": disabled,
    tabIndex: -1,
    "aria-label": label
  }), children);
};

exports.MenuItemCheckbox = MenuItemCheckbox;
MenuItemCheckbox.propTypes = _objectSpread({}, _AccessibleButton.default.propTypes, {
  label: _propTypes.default.string,
  // optional
  active: _propTypes.default.bool.isRequired,
  disabled: _propTypes.default.bool,
  // optional
  className: _propTypes.default.string,
  // optional
  onClick: _propTypes.default.func.isRequired
}); // Semantic component for representing a role=menuitemradio

const MenuItemRadio = (_ref5) => {
  let {
    children,
    label,
    active = false,
    disabled = false
  } = _ref5,
      props = (0, _objectWithoutProperties2.default)(_ref5, ["children", "label", "active", "disabled"]);
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
  return _react.default.createElement(AccessibleButton, (0, _extends2.default)({}, props, {
    role: "menuitemradio",
    "aria-checked": active,
    "aria-disabled": disabled,
    tabIndex: -1,
    "aria-label": label
  }), children);
};

exports.MenuItemRadio = MenuItemRadio;
MenuItemRadio.propTypes = _objectSpread({}, _AccessibleButton.default.propTypes, {
  label: _propTypes.default.string,
  // optional
  active: _propTypes.default.bool.isRequired,
  disabled: _propTypes.default.bool,
  // optional
  className: _propTypes.default.string,
  // optional
  onClick: _propTypes.default.func.isRequired
}); // Placement method for <ContextMenu /> to position context menu to right of elementRect with chevronOffset

const toRightOf = (elementRect, chevronOffset = 12) => {
  const left = elementRect.right + window.pageXOffset + 3;
  let top = elementRect.top + elementRect.height / 2 + window.pageYOffset;
  top -= chevronOffset + 8; // where 8 is half the height of the chevron

  return {
    left,
    top,
    chevronOffset
  };
}; // Placement method for <ContextMenu /> to position context menu right-aligned and flowing to the left of elementRect


exports.toRightOf = toRightOf;

const aboveLeftOf = (elementRect, chevronFace = "none") => {
  const menuOptions = {
    chevronFace
  };
  const buttonRight = elementRect.right + window.pageXOffset;
  const buttonBottom = elementRect.bottom + window.pageYOffset;
  const buttonTop = elementRect.top + window.pageYOffset; // Align the right edge of the menu to the right edge of the button

  menuOptions.right = window.innerWidth - buttonRight; // Align the menu vertically on whichever side of the button has more space available.

  if (buttonBottom < window.innerHeight / 2) {
    menuOptions.top = buttonBottom;
  } else {
    menuOptions.bottom = window.innerHeight - buttonTop;
  }

  return menuOptions;
};

exports.aboveLeftOf = aboveLeftOf;

const useContextMenu = () => {
  const button = (0, _react.useRef)(null);
  const [isOpen, setIsOpen] = (0, _react.useState)(false);

  const open = () => {
    setIsOpen(true);
  };

  const close = () => {
    setIsOpen(false);
  };

  return [isOpen, button, open, close, setIsOpen];
};

exports.useContextMenu = useContextMenu;

class LegacyContextMenu extends ContextMenu {
  render() {
    return this.renderMenu(false);
  }

} // XXX: Deprecated, used only for dynamic Tooltips. Avoid using at all costs.


exports.default = LegacyContextMenu;

function createMenu(ElementClass, props) {
  const onFinished = function (...args) {
    _reactDom.default.unmountComponentAtNode(getOrCreateContainer());

    if (props && props.onFinished) {
      props.onFinished.apply(null, args);
    }
  };

  const menu = _react.default.createElement(LegacyContextMenu, (0, _extends2.default)({}, props, {
    onFinished: onFinished // eslint-disable-line react/jsx-no-bind
    ,
    windowResize: onFinished // eslint-disable-line react/jsx-no-bind

  }), _react.default.createElement(ElementClass, (0, _extends2.default)({}, props, {
    onFinished: onFinished
  })));

  _reactDom.default.render(menu, getOrCreateContainer());

  return {
    close: onFinished
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvQ29udGV4dE1lbnUuanMiXSwibmFtZXMiOlsiQ29udGV4dHVhbE1lbnVDb250YWluZXJJZCIsImdldE9yQ3JlYXRlQ29udGFpbmVyIiwiY29udGFpbmVyIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsImNyZWF0ZUVsZW1lbnQiLCJpZCIsImJvZHkiLCJhcHBlbmRDaGlsZCIsIkFSSUFfTUVOVV9JVEVNX1JPTEVTIiwiU2V0IiwiQ29udGV4dE1lbnUiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZWxlbWVudCIsImZpcnN0IiwicXVlcnlTZWxlY3RvciIsImZvY3VzIiwic2V0U3RhdGUiLCJjb250ZXh0TWVudUVsZW0iLCJlIiwicHJvcHMiLCJvbkZpbmlzaGVkIiwicHJldmVudERlZmF1bHQiLCJ4IiwiY2xpZW50WCIsInkiLCJjbGllbnRZIiwic2V0SW1tZWRpYXRlIiwiY2xpY2tFdmVudCIsImNyZWF0ZUV2ZW50IiwiaW5pdE1vdXNlRXZlbnQiLCJ3aW5kb3ciLCJlbGVtZW50RnJvbVBvaW50IiwiZGlzcGF0Y2hFdmVudCIsInVwIiwiZGVzY2VuZGluZyIsImNoaWxkIiwibGFzdEVsZW1lbnRDaGlsZCIsImZpcnN0RWxlbWVudENoaWxkIiwic2libGluZyIsInByZXZpb3VzRWxlbWVudFNpYmxpbmciLCJuZXh0RWxlbWVudFNpYmxpbmciLCJwYXJlbnRFbGVtZW50IiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJoYXMiLCJnZXRBdHRyaWJ1dGUiLCJyZXN1bHRzIiwicXVlcnlTZWxlY3RvckFsbCIsImxlbmd0aCIsImV2IiwibWFuYWdlZCIsImtleSIsIktleSIsIkVTQ0FQRSIsInN0b3BQcm9wYWdhdGlvbiIsImhhbmRsZWQiLCJUQUIiLCJBUlJPV19VUCIsIl9vbk1vdmVGb2N1cyIsInRhcmdldCIsIkFSUk9XX0RPV04iLCJIT01FIiwiX29uTW92ZUZvY3VzSG9tZUVuZCIsInN0YXRlIiwiRU5EIiwiaW5pdGlhbEZvY3VzIiwiYWN0aXZlRWxlbWVudCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVuZGVyTWVudSIsImhhc0JhY2tncm91bmQiLCJwb3NpdGlvbiIsImNoZXZyb25GYWNlIiwidG9wIiwiYm90dG9tIiwibGVmdCIsInJpZ2h0IiwiY29udGV4dE1lbnVSZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwicGFkZGluZyIsImNoZXZyb25PZmZzZXQiLCJoYXNDaGV2cm9uIiwidW5kZWZpbmVkIiwiYWRqdXN0ZWQiLCJNYXRoIiwibWluIiwiY2xpZW50SGVpZ2h0IiwiaGVpZ2h0IiwibWF4IiwiY2hldnJvbiIsIm1lbnVDbGFzc2VzIiwibWVudVN0eWxlIiwibWVudVdpZHRoIiwid2lkdGgiLCJtZW51SGVpZ2h0IiwiaXNOYU4iLCJOdW1iZXIiLCJtZW51UGFkZGluZ1RvcCIsIm1lbnVQYWRkaW5nTGVmdCIsIm1lbnVQYWRkaW5nQm90dG9tIiwibWVudVBhZGRpbmdSaWdodCIsIndyYXBwZXJTdHlsZSIsInpJbmRleCIsImJhY2tncm91bmQiLCJvbkNvbnRleHRNZW51IiwiX29uS2V5RG93biIsImNvbGxlY3RDb250ZXh0TWVudVJlY3QiLCJjaGlsZHJlbiIsInJlbmRlciIsIlJlYWN0RE9NIiwiY3JlYXRlUG9ydGFsIiwiUHJvcFR5cGVzIiwibnVtYmVyIiwic3RyaW5nIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJib29sIiwid2luZG93UmVzaXplIiwiQ29udGV4dE1lbnVCdXR0b24iLCJsYWJlbCIsImlzRXhwYW5kZWQiLCJBY2Nlc3NpYmxlQnV0dG9uIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwicHJvcFR5cGVzIiwiTWVudUl0ZW0iLCJjbGFzc05hbWUiLCJvbkNsaWNrIiwiTWVudUdyb3VwIiwiTWVudUl0ZW1DaGVja2JveCIsImFjdGl2ZSIsImRpc2FibGVkIiwiTWVudUl0ZW1SYWRpbyIsInRvUmlnaHRPZiIsImVsZW1lbnRSZWN0IiwicGFnZVhPZmZzZXQiLCJwYWdlWU9mZnNldCIsImFib3ZlTGVmdE9mIiwibWVudU9wdGlvbnMiLCJidXR0b25SaWdodCIsImJ1dHRvbkJvdHRvbSIsImJ1dHRvblRvcCIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsInVzZUNvbnRleHRNZW51IiwiYnV0dG9uIiwiaXNPcGVuIiwic2V0SXNPcGVuIiwib3BlbiIsImNsb3NlIiwiTGVnYWN5Q29udGV4dE1lbnUiLCJjcmVhdGVNZW51IiwiRWxlbWVudENsYXNzIiwiYXJncyIsInVubW91bnRDb21wb25lbnRBdE5vZGUiLCJhcHBseSIsIm1lbnUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBRUEsTUFBTUEseUJBQXlCLEdBQUcsNkJBQWxDOztBQUVBLFNBQVNDLG9CQUFULEdBQWdDO0FBQzVCLE1BQUlDLFNBQVMsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCSix5QkFBeEIsQ0FBaEI7O0FBRUEsTUFBSSxDQUFDRSxTQUFMLEVBQWdCO0FBQ1pBLElBQUFBLFNBQVMsR0FBR0MsUUFBUSxDQUFDRSxhQUFULENBQXVCLEtBQXZCLENBQVo7QUFDQUgsSUFBQUEsU0FBUyxDQUFDSSxFQUFWLEdBQWVOLHlCQUFmO0FBQ0FHLElBQUFBLFFBQVEsQ0FBQ0ksSUFBVCxDQUFjQyxXQUFkLENBQTBCTixTQUExQjtBQUNIOztBQUVELFNBQU9BLFNBQVA7QUFDSDs7QUFFRCxNQUFNTyxvQkFBb0IsR0FBRyxJQUFJQyxHQUFKLENBQVEsQ0FBQyxVQUFELEVBQWEsa0JBQWIsRUFBaUMsZUFBakMsQ0FBUixDQUE3QixDLENBQ0E7QUFDQTtBQUNBOztBQUNPLE1BQU1DLFdBQU4sU0FBMEJDLGVBQU1DLFNBQWhDLENBQTBDO0FBaUM3Q0MsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSxrRUFlWUMsT0FBRCxJQUFhO0FBQ2xDO0FBQ0EsVUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFFZCxVQUFJQyxLQUFLLEdBQUdELE9BQU8sQ0FBQ0UsYUFBUixDQUFzQixvQkFBdEIsQ0FBWjs7QUFDQSxVQUFJLENBQUNELEtBQUwsRUFBWTtBQUNSQSxRQUFBQSxLQUFLLEdBQUdELE9BQU8sQ0FBQ0UsYUFBUixDQUFzQixhQUF0QixDQUFSO0FBQ0g7O0FBQ0QsVUFBSUQsS0FBSixFQUFXO0FBQ1BBLFFBQUFBLEtBQUssQ0FBQ0UsS0FBTjtBQUNIOztBQUVELFdBQUtDLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSxlQUFlLEVBQUVMO0FBRFAsT0FBZDtBQUdILEtBOUJhO0FBQUEseURBZ0NHTSxDQUFELElBQU87QUFDbkIsVUFBSSxLQUFLQyxLQUFMLENBQVdDLFVBQWYsRUFBMkI7QUFDdkIsYUFBS0QsS0FBTCxDQUFXQyxVQUFYO0FBRUFGLFFBQUFBLENBQUMsQ0FBQ0csY0FBRjtBQUNBLGNBQU1DLENBQUMsR0FBR0osQ0FBQyxDQUFDSyxPQUFaO0FBQ0EsY0FBTUMsQ0FBQyxHQUFHTixDQUFDLENBQUNPLE9BQVosQ0FMdUIsQ0FPdkI7QUFDQTs7QUFDQUMsUUFBQUEsWUFBWSxDQUFDLE1BQU07QUFDZixnQkFBTUMsVUFBVSxHQUFHM0IsUUFBUSxDQUFDNEIsV0FBVCxDQUFxQixhQUFyQixDQUFuQjtBQUNBRCxVQUFBQSxVQUFVLENBQUNFLGNBQVgsQ0FDSSxhQURKLEVBQ21CLElBRG5CLEVBQ3lCLElBRHpCLEVBQytCQyxNQUQvQixFQUN1QyxDQUR2QyxFQUVJLENBRkosRUFFTyxDQUZQLEVBRVVSLENBRlYsRUFFYUUsQ0FGYixFQUVnQixLQUZoQixFQUV1QixLQUZ2QixFQUdJLEtBSEosRUFHVyxLQUhYLEVBR2tCLENBSGxCLEVBR3FCLElBSHJCO0FBS0F4QixVQUFBQSxRQUFRLENBQUMrQixnQkFBVCxDQUEwQlQsQ0FBMUIsRUFBNkJFLENBQTdCLEVBQWdDUSxhQUFoQyxDQUE4Q0wsVUFBOUM7QUFDSCxTQVJXLENBQVo7QUFTSDtBQUNKLEtBcERhO0FBQUEsd0RBc0RDLENBQUNmLE9BQUQsRUFBVXFCLEVBQVYsS0FBaUI7QUFDNUIsVUFBSUMsVUFBVSxHQUFHLEtBQWpCLENBRDRCLENBQ0o7O0FBRXhCLFNBQUc7QUFDQyxjQUFNQyxLQUFLLEdBQUdGLEVBQUUsR0FBR3JCLE9BQU8sQ0FBQ3dCLGdCQUFYLEdBQThCeEIsT0FBTyxDQUFDeUIsaUJBQXREO0FBQ0EsY0FBTUMsT0FBTyxHQUFHTCxFQUFFLEdBQUdyQixPQUFPLENBQUMyQixzQkFBWCxHQUFvQzNCLE9BQU8sQ0FBQzRCLGtCQUE5RDs7QUFFQSxZQUFJTixVQUFKLEVBQWdCO0FBQ1osY0FBSUMsS0FBSixFQUFXO0FBQ1B2QixZQUFBQSxPQUFPLEdBQUd1QixLQUFWO0FBQ0gsV0FGRCxNQUVPLElBQUlHLE9BQUosRUFBYTtBQUNoQjFCLFlBQUFBLE9BQU8sR0FBRzBCLE9BQVY7QUFDSCxXQUZNLE1BRUE7QUFDSEosWUFBQUEsVUFBVSxHQUFHLEtBQWI7QUFDQXRCLFlBQUFBLE9BQU8sR0FBR0EsT0FBTyxDQUFDNkIsYUFBbEI7QUFDSDtBQUNKLFNBVEQsTUFTTztBQUNILGNBQUlILE9BQUosRUFBYTtBQUNUMUIsWUFBQUEsT0FBTyxHQUFHMEIsT0FBVjtBQUNBSixZQUFBQSxVQUFVLEdBQUcsSUFBYjtBQUNILFdBSEQsTUFHTztBQUNIdEIsWUFBQUEsT0FBTyxHQUFHQSxPQUFPLENBQUM2QixhQUFsQjtBQUNIO0FBQ0o7O0FBRUQsWUFBSTdCLE9BQUosRUFBYTtBQUNULGNBQUlBLE9BQU8sQ0FBQzhCLFNBQVIsQ0FBa0JDLFFBQWxCLENBQTJCLG1CQUEzQixDQUFKLEVBQXFEO0FBQUU7QUFDbkQvQixZQUFBQSxPQUFPLEdBQUdxQixFQUFFLEdBQUdyQixPQUFPLENBQUN3QixnQkFBWCxHQUE4QnhCLE9BQU8sQ0FBQ3lCLGlCQUFsRDtBQUNBSCxZQUFBQSxVQUFVLEdBQUcsSUFBYjtBQUNIO0FBQ0o7QUFDSixPQTVCRCxRQTRCU3RCLE9BQU8sSUFBSSxDQUFDTixvQkFBb0IsQ0FBQ3NDLEdBQXJCLENBQXlCaEMsT0FBTyxDQUFDaUMsWUFBUixDQUFxQixNQUFyQixDQUF6QixDQTVCckI7O0FBOEJBLFVBQUlqQyxPQUFKLEVBQWE7QUFDVEEsUUFBQUEsT0FBTyxDQUFDRyxLQUFSO0FBQ0g7QUFDSixLQTFGYTtBQUFBLCtEQTRGUSxDQUFDSCxPQUFELEVBQVVxQixFQUFWLEtBQWlCO0FBQ25DLFVBQUlhLE9BQU8sR0FBR2xDLE9BQU8sQ0FBQ21DLGdCQUFSLENBQXlCLG9CQUF6QixDQUFkOztBQUNBLFVBQUksQ0FBQ0QsT0FBTCxFQUFjO0FBQ1ZBLFFBQUFBLE9BQU8sR0FBR2xDLE9BQU8sQ0FBQ21DLGdCQUFSLENBQXlCLGFBQXpCLENBQVY7QUFDSDs7QUFDRCxVQUFJRCxPQUFPLElBQUlBLE9BQU8sQ0FBQ0UsTUFBdkIsRUFBK0I7QUFDM0IsWUFBSWYsRUFBSixFQUFRO0FBQ0phLFVBQUFBLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBVy9CLEtBQVg7QUFDSCxTQUZELE1BRU87QUFDSCtCLFVBQUFBLE9BQU8sQ0FBQ0EsT0FBTyxDQUFDRSxNQUFSLEdBQWlCLENBQWxCLENBQVAsQ0FBNEJqQyxLQUE1QjtBQUNIO0FBQ0o7QUFDSixLQXhHYTtBQUFBLHNEQTBHQWtDLEVBQUQsSUFBUTtBQUNqQixVQUFJLENBQUMsS0FBSzlCLEtBQUwsQ0FBVytCLE9BQWhCLEVBQXlCO0FBQ3JCLFlBQUlELEVBQUUsQ0FBQ0UsR0FBSCxLQUFXQyxjQUFJQyxNQUFuQixFQUEyQjtBQUN2QixlQUFLbEMsS0FBTCxDQUFXQyxVQUFYO0FBQ0E2QixVQUFBQSxFQUFFLENBQUNLLGVBQUg7QUFDQUwsVUFBQUEsRUFBRSxDQUFDNUIsY0FBSDtBQUNIOztBQUNEO0FBQ0g7O0FBRUQsVUFBSWtDLE9BQU8sR0FBRyxJQUFkOztBQUVBLGNBQVFOLEVBQUUsQ0FBQ0UsR0FBWDtBQUNJLGFBQUtDLGNBQUlJLEdBQVQ7QUFDQSxhQUFLSixjQUFJQyxNQUFUO0FBQ0ksZUFBS2xDLEtBQUwsQ0FBV0MsVUFBWDtBQUNBOztBQUNKLGFBQUtnQyxjQUFJSyxRQUFUO0FBQ0ksZUFBS0MsWUFBTCxDQUFrQlQsRUFBRSxDQUFDVSxNQUFyQixFQUE2QixJQUE3Qjs7QUFDQTs7QUFDSixhQUFLUCxjQUFJUSxVQUFUO0FBQ0ksZUFBS0YsWUFBTCxDQUFrQlQsRUFBRSxDQUFDVSxNQUFyQixFQUE2QixLQUE3Qjs7QUFDQTs7QUFDSixhQUFLUCxjQUFJUyxJQUFUO0FBQ0ksZUFBS0MsbUJBQUwsQ0FBeUIsS0FBS0MsS0FBTCxDQUFXOUMsZUFBcEMsRUFBcUQsSUFBckQ7O0FBQ0E7O0FBQ0osYUFBS21DLGNBQUlZLEdBQVQ7QUFDSSxlQUFLRixtQkFBTCxDQUF5QixLQUFLQyxLQUFMLENBQVc5QyxlQUFwQyxFQUFxRCxLQUFyRDs7QUFDQTs7QUFDSjtBQUNJc0MsVUFBQUEsT0FBTyxHQUFHLEtBQVY7QUFsQlI7O0FBcUJBLFVBQUlBLE9BQUosRUFBYTtBQUNUO0FBQ0FOLFFBQUFBLEVBQUUsQ0FBQ0ssZUFBSDtBQUNBTCxRQUFBQSxFQUFFLENBQUM1QixjQUFIO0FBQ0g7QUFDSixLQWhKYTtBQUVWLFNBQUswQyxLQUFMLEdBQWE7QUFDVDlDLE1BQUFBLGVBQWUsRUFBRTtBQURSLEtBQWIsQ0FGVSxDQU1WOztBQUNBLFNBQUtnRCxZQUFMLEdBQW9CakUsUUFBUSxDQUFDa0UsYUFBN0I7QUFDSDs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkI7QUFDQSxTQUFLRixZQUFMLENBQWtCbEQsS0FBbEI7QUFDSDs7QUFxSURxRCxFQUFBQSxVQUFVLENBQUNDLGFBQWEsR0FBQyxLQUFLbEQsS0FBTCxDQUFXa0QsYUFBMUIsRUFBeUM7QUFDL0MsVUFBTUMsUUFBUSxHQUFHLEVBQWpCO0FBQ0EsUUFBSUMsV0FBVyxHQUFHLElBQWxCO0FBQ0EsVUFBTXBELEtBQUssR0FBRyxLQUFLQSxLQUFuQjs7QUFFQSxRQUFJQSxLQUFLLENBQUNxRCxHQUFWLEVBQWU7QUFDWEYsTUFBQUEsUUFBUSxDQUFDRSxHQUFULEdBQWVyRCxLQUFLLENBQUNxRCxHQUFyQjtBQUNILEtBRkQsTUFFTztBQUNIRixNQUFBQSxRQUFRLENBQUNHLE1BQVQsR0FBa0J0RCxLQUFLLENBQUNzRCxNQUF4QjtBQUNIOztBQUVELFFBQUl0RCxLQUFLLENBQUN1RCxJQUFWLEVBQWdCO0FBQ1pKLE1BQUFBLFFBQVEsQ0FBQ0ksSUFBVCxHQUFnQnZELEtBQUssQ0FBQ3VELElBQXRCO0FBQ0FILE1BQUFBLFdBQVcsR0FBRyxNQUFkO0FBQ0gsS0FIRCxNQUdPO0FBQ0hELE1BQUFBLFFBQVEsQ0FBQ0ssS0FBVCxHQUFpQnhELEtBQUssQ0FBQ3dELEtBQXZCO0FBQ0FKLE1BQUFBLFdBQVcsR0FBRyxPQUFkO0FBQ0g7O0FBRUQsVUFBTUssZUFBZSxHQUFHLEtBQUtiLEtBQUwsQ0FBVzlDLGVBQVgsR0FBNkIsS0FBSzhDLEtBQUwsQ0FBVzlDLGVBQVgsQ0FBMkI0RCxxQkFBM0IsRUFBN0IsR0FBa0YsSUFBMUc7QUFDQSxVQUFNQyxPQUFPLEdBQUcsRUFBaEI7QUFFQSxVQUFNQyxhQUFhLEdBQUcsRUFBdEI7O0FBQ0EsUUFBSTVELEtBQUssQ0FBQ29ELFdBQVYsRUFBdUI7QUFDbkJBLE1BQUFBLFdBQVcsR0FBR3BELEtBQUssQ0FBQ29ELFdBQXBCO0FBQ0g7O0FBQ0QsVUFBTVMsVUFBVSxHQUFHVCxXQUFXLElBQUlBLFdBQVcsS0FBSyxNQUFsRDs7QUFFQSxRQUFJQSxXQUFXLEtBQUssS0FBaEIsSUFBeUJBLFdBQVcsS0FBSyxRQUE3QyxFQUF1RDtBQUNuRFEsTUFBQUEsYUFBYSxDQUFDTCxJQUFkLEdBQXFCdkQsS0FBSyxDQUFDNEQsYUFBM0I7QUFDSCxLQUZELE1BRU8sSUFBSVQsUUFBUSxDQUFDRSxHQUFULEtBQWlCUyxTQUFyQixFQUFnQztBQUNuQyxZQUFNdEIsTUFBTSxHQUFHVyxRQUFRLENBQUNFLEdBQXhCLENBRG1DLENBR25DOztBQUNBLFVBQUlVLFFBQVEsR0FBR3ZCLE1BQWYsQ0FKbUMsQ0FNbkM7QUFDQTs7QUFDQSxVQUFJaUIsZUFBSixFQUFxQjtBQUNqQk0sUUFBQUEsUUFBUSxHQUFHQyxJQUFJLENBQUNDLEdBQUwsQ0FBU2QsUUFBUSxDQUFDRSxHQUFsQixFQUF1QnhFLFFBQVEsQ0FBQ0ksSUFBVCxDQUFjaUYsWUFBZCxHQUE2QlQsZUFBZSxDQUFDVSxNQUE3QyxHQUFzRFIsT0FBN0UsQ0FBWDtBQUNIOztBQUVEUixNQUFBQSxRQUFRLENBQUNFLEdBQVQsR0FBZVUsUUFBZjtBQUNBSCxNQUFBQSxhQUFhLENBQUNQLEdBQWQsR0FBb0JXLElBQUksQ0FBQ0ksR0FBTCxDQUFTcEUsS0FBSyxDQUFDNEQsYUFBZixFQUE4QjVELEtBQUssQ0FBQzRELGFBQU4sR0FBc0JwQixNQUF0QixHQUErQnVCLFFBQTdELENBQXBCO0FBQ0g7O0FBRUQsUUFBSU0sT0FBSjs7QUFDQSxRQUFJUixVQUFKLEVBQWdCO0FBQ1pRLE1BQUFBLE9BQU8sR0FBRztBQUFLLFFBQUEsS0FBSyxFQUFFVCxhQUFaO0FBQTJCLFFBQUEsU0FBUyxFQUFFLCtCQUErQlI7QUFBckUsUUFBVjtBQUNIOztBQUVELFVBQU1rQixXQUFXLEdBQUcseUJBQVc7QUFDM0IsMkJBQXFCLElBRE07QUFFM0IsZ0NBQTBCLENBQUNULFVBQUQsSUFBZVYsUUFBUSxDQUFDSSxJQUZ2QjtBQUczQixpQ0FBMkIsQ0FBQ00sVUFBRCxJQUFlVixRQUFRLENBQUNLLEtBSHhCO0FBSTNCLCtCQUF5QixDQUFDSyxVQUFELElBQWVWLFFBQVEsQ0FBQ0UsR0FKdEI7QUFLM0Isa0NBQTRCLENBQUNRLFVBQUQsSUFBZVYsUUFBUSxDQUFDRyxNQUx6QjtBQU0zQiw0Q0FBc0NGLFdBQVcsS0FBSyxNQU4zQjtBQU8zQiw2Q0FBdUNBLFdBQVcsS0FBSyxPQVA1QjtBQVEzQiwyQ0FBcUNBLFdBQVcsS0FBSyxLQVIxQjtBQVMzQiw4Q0FBd0NBLFdBQVcsS0FBSztBQVQ3QixLQUFYLENBQXBCO0FBWUEsVUFBTW1CLFNBQVMsR0FBRyxFQUFsQjs7QUFDQSxRQUFJdkUsS0FBSyxDQUFDd0UsU0FBVixFQUFxQjtBQUNqQkQsTUFBQUEsU0FBUyxDQUFDRSxLQUFWLEdBQWtCekUsS0FBSyxDQUFDd0UsU0FBeEI7QUFDSDs7QUFFRCxRQUFJeEUsS0FBSyxDQUFDMEUsVUFBVixFQUFzQjtBQUNsQkgsTUFBQUEsU0FBUyxDQUFDSixNQUFWLEdBQW1CbkUsS0FBSyxDQUFDMEUsVUFBekI7QUFDSDs7QUFFRCxRQUFJLENBQUNDLEtBQUssQ0FBQ0MsTUFBTSxDQUFDNUUsS0FBSyxDQUFDNkUsY0FBUCxDQUFQLENBQVYsRUFBMEM7QUFDdENOLE1BQUFBLFNBQVMsQ0FBQyxZQUFELENBQVQsR0FBMEJ2RSxLQUFLLENBQUM2RSxjQUFoQztBQUNIOztBQUNELFFBQUksQ0FBQ0YsS0FBSyxDQUFDQyxNQUFNLENBQUM1RSxLQUFLLENBQUM4RSxlQUFQLENBQVAsQ0FBVixFQUEyQztBQUN2Q1AsTUFBQUEsU0FBUyxDQUFDLGFBQUQsQ0FBVCxHQUEyQnZFLEtBQUssQ0FBQzhFLGVBQWpDO0FBQ0g7O0FBQ0QsUUFBSSxDQUFDSCxLQUFLLENBQUNDLE1BQU0sQ0FBQzVFLEtBQUssQ0FBQytFLGlCQUFQLENBQVAsQ0FBVixFQUE2QztBQUN6Q1IsTUFBQUEsU0FBUyxDQUFDLGVBQUQsQ0FBVCxHQUE2QnZFLEtBQUssQ0FBQytFLGlCQUFuQztBQUNIOztBQUNELFFBQUksQ0FBQ0osS0FBSyxDQUFDQyxNQUFNLENBQUM1RSxLQUFLLENBQUNnRixnQkFBUCxDQUFQLENBQVYsRUFBNEM7QUFDeENULE1BQUFBLFNBQVMsQ0FBQyxjQUFELENBQVQsR0FBNEJ2RSxLQUFLLENBQUNnRixnQkFBbEM7QUFDSDs7QUFFRCxVQUFNQyxZQUFZLEdBQUcsRUFBckI7O0FBQ0EsUUFBSSxDQUFDTixLQUFLLENBQUNDLE1BQU0sQ0FBQzVFLEtBQUssQ0FBQ2tGLE1BQVAsQ0FBUCxDQUFWLEVBQWtDO0FBQzlCWCxNQUFBQSxTQUFTLENBQUMsUUFBRCxDQUFULEdBQXNCdkUsS0FBSyxDQUFDa0YsTUFBTixHQUFlLENBQXJDO0FBQ0FELE1BQUFBLFlBQVksQ0FBQyxRQUFELENBQVosR0FBeUJqRixLQUFLLENBQUNrRixNQUEvQjtBQUNIOztBQUVELFFBQUlDLFVBQUo7O0FBQ0EsUUFBSWpDLGFBQUosRUFBbUI7QUFDZmlDLE1BQUFBLFVBQVUsR0FDTjtBQUFLLFFBQUEsU0FBUyxFQUFDLDhCQUFmO0FBQThDLFFBQUEsS0FBSyxFQUFFRixZQUFyRDtBQUFtRSxRQUFBLE9BQU8sRUFBRWpGLEtBQUssQ0FBQ0MsVUFBbEY7QUFBOEYsUUFBQSxhQUFhLEVBQUUsS0FBS21GO0FBQWxILFFBREo7QUFHSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsMkJBQWY7QUFBMkMsTUFBQSxLQUFLLG9CQUFNakMsUUFBTixNQUFtQjhCLFlBQW5CLENBQWhEO0FBQWtGLE1BQUEsU0FBUyxFQUFFLEtBQUtJO0FBQWxHLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBRWYsV0FBaEI7QUFBNkIsTUFBQSxLQUFLLEVBQUVDLFNBQXBDO0FBQStDLE1BQUEsR0FBRyxFQUFFLEtBQUtlLHNCQUF6RDtBQUFpRixNQUFBLElBQUksRUFBRSxLQUFLdEYsS0FBTCxDQUFXK0IsT0FBWCxHQUFxQixNQUFyQixHQUE4QitCO0FBQXJILE9BQ01PLE9BRE4sRUFFTXJFLEtBQUssQ0FBQ3VGLFFBRlosQ0FESixFQUtNSixVQUxOLENBREo7QUFTSDs7QUFFREssRUFBQUEsTUFBTSxHQUFHO0FBQ0wsV0FBT0Msa0JBQVNDLFlBQVQsQ0FBc0IsS0FBS3pDLFVBQUwsRUFBdEIsRUFBeUN0RSxvQkFBb0IsRUFBN0QsQ0FBUDtBQUNIOztBQWxTNEMsQyxDQXFTakQ7Ozs7OEJBclNhVSxXLGVBQ1U7QUFDZmdFLEVBQUFBLEdBQUcsRUFBRXNDLG1CQUFVQyxNQURBO0FBRWZ0QyxFQUFBQSxNQUFNLEVBQUVxQyxtQkFBVUMsTUFGSDtBQUdmckMsRUFBQUEsSUFBSSxFQUFFb0MsbUJBQVVDLE1BSEQ7QUFJZnBDLEVBQUFBLEtBQUssRUFBRW1DLG1CQUFVQyxNQUpGO0FBS2ZwQixFQUFBQSxTQUFTLEVBQUVtQixtQkFBVUMsTUFMTjtBQU1mbEIsRUFBQUEsVUFBVSxFQUFFaUIsbUJBQVVDLE1BTlA7QUFPZmhDLEVBQUFBLGFBQWEsRUFBRStCLG1CQUFVQyxNQVBWO0FBUWZ4QyxFQUFBQSxXQUFXLEVBQUV1QyxtQkFBVUUsTUFSUjtBQVFnQjtBQUMvQjtBQUNBNUYsRUFBQUEsVUFBVSxFQUFFMEYsbUJBQVVHLElBQVYsQ0FBZUMsVUFWWjtBQVdmbEIsRUFBQUEsY0FBYyxFQUFFYyxtQkFBVUMsTUFYWDtBQVlmWixFQUFBQSxnQkFBZ0IsRUFBRVcsbUJBQVVDLE1BWmI7QUFhZmIsRUFBQUEsaUJBQWlCLEVBQUVZLG1CQUFVQyxNQWJkO0FBY2ZkLEVBQUFBLGVBQWUsRUFBRWEsbUJBQVVDLE1BZFo7QUFlZlYsRUFBQUEsTUFBTSxFQUFFUyxtQkFBVUMsTUFmSDtBQWlCZjtBQUNBO0FBQ0ExQyxFQUFBQSxhQUFhLEVBQUV5QyxtQkFBVUssSUFuQlY7QUFxQmY7QUFDQUMsRUFBQUEsWUFBWSxFQUFFTixtQkFBVUcsSUF0QlQ7QUF3QmYvRCxFQUFBQSxPQUFPLEVBQUU0RCxtQkFBVUssSUF4QkosQ0F3QlU7O0FBeEJWLEM7OEJBRFYzRyxXLGtCQTRCYTtBQUNsQjZELEVBQUFBLGFBQWEsRUFBRSxJQURHO0FBRWxCbkIsRUFBQUEsT0FBTyxFQUFFO0FBRlMsQzs7QUEwUW5CLE1BQU1tRSxpQkFBaUIsR0FBRyxVQUErQztBQUFBLE1BQTlDO0FBQUVDLElBQUFBLEtBQUY7QUFBU0MsSUFBQUEsVUFBVDtBQUFxQmIsSUFBQUE7QUFBckIsR0FBOEM7QUFBQSxNQUFadkYsS0FBWTtBQUM1RSxRQUFNcUcsZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxTQUNJLDZCQUFDLGdCQUFELDZCQUFzQnZHLEtBQXRCO0FBQTZCLElBQUEsS0FBSyxFQUFFbUcsS0FBcEM7QUFBMkMsa0JBQVlBLEtBQXZEO0FBQThELHFCQUFlLElBQTdFO0FBQW1GLHFCQUFlQztBQUFsRyxNQUNNYixRQUROLENBREo7QUFLSCxDQVBNOzs7QUFRUFcsaUJBQWlCLENBQUNNLFNBQWxCLHFCQUNPSCwwQkFBaUJHLFNBRHhCO0FBRUlMLEVBQUFBLEtBQUssRUFBRVIsbUJBQVVFLE1BRnJCO0FBR0lPLEVBQUFBLFVBQVUsRUFBRVQsbUJBQVVLLElBQVYsQ0FBZUQsVUFIL0IsQ0FHMkM7O0FBSDNDLEcsQ0FNQTs7QUFDTyxNQUFNVSxRQUFRLEdBQUcsV0FBaUM7QUFBQSxNQUFoQztBQUFDbEIsSUFBQUEsUUFBRDtBQUFXWSxJQUFBQTtBQUFYLEdBQWdDO0FBQUEsTUFBWG5HLEtBQVc7QUFDckQsUUFBTXFHLGdCQUFnQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsU0FDSSw2QkFBQyxnQkFBRCw2QkFBc0J2RyxLQUF0QjtBQUE2QixJQUFBLElBQUksRUFBQyxVQUFsQztBQUE2QyxJQUFBLFFBQVEsRUFBRSxDQUFDLENBQXhEO0FBQTJELGtCQUFZbUc7QUFBdkUsTUFDTVosUUFETixDQURKO0FBS0gsQ0FQTTs7O0FBUVBrQixRQUFRLENBQUNELFNBQVQscUJBQ09ILDBCQUFpQkcsU0FEeEI7QUFFSUwsRUFBQUEsS0FBSyxFQUFFUixtQkFBVUUsTUFGckI7QUFFNkI7QUFDekJhLEVBQUFBLFNBQVMsRUFBRWYsbUJBQVVFLE1BSHpCO0FBR2lDO0FBQzdCYyxFQUFBQSxPQUFPLEVBQUVoQixtQkFBVUcsSUFBVixDQUFlQztBQUo1QixHLENBT0E7O0FBQ08sTUFBTWEsU0FBUyxHQUFHLFdBQWlDO0FBQUEsTUFBaEM7QUFBQ3JCLElBQUFBLFFBQUQ7QUFBV1ksSUFBQUE7QUFBWCxHQUFnQztBQUFBLE1BQVhuRyxLQUFXO0FBQ3RELFNBQU8sK0RBQVNBLEtBQVQ7QUFBZ0IsSUFBQSxJQUFJLEVBQUMsT0FBckI7QUFBNkIsa0JBQVltRztBQUF6QyxNQUNEWixRQURDLENBQVA7QUFHSCxDQUpNOzs7QUFLUHFCLFNBQVMsQ0FBQ0osU0FBVixHQUFzQjtBQUNsQkwsRUFBQUEsS0FBSyxFQUFFUixtQkFBVUUsTUFBVixDQUFpQkUsVUFETjtBQUVsQlcsRUFBQUEsU0FBUyxFQUFFZixtQkFBVUUsTUFGSCxDQUVXOztBQUZYLENBQXRCLEMsQ0FLQTs7QUFDTyxNQUFNZ0IsZ0JBQWdCLEdBQUcsV0FBK0Q7QUFBQSxNQUE5RDtBQUFDdEIsSUFBQUEsUUFBRDtBQUFXWSxJQUFBQSxLQUFYO0FBQWtCVyxJQUFBQSxNQUFNLEdBQUMsS0FBekI7QUFBZ0NDLElBQUFBLFFBQVEsR0FBQztBQUF6QyxHQUE4RDtBQUFBLE1BQVgvRyxLQUFXO0FBQzNGLFFBQU1xRyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFNBQ0ksNkJBQUMsZ0JBQUQsNkJBQXNCdkcsS0FBdEI7QUFBNkIsSUFBQSxJQUFJLEVBQUMsa0JBQWxDO0FBQXFELG9CQUFjOEcsTUFBbkU7QUFBMkUscUJBQWVDLFFBQTFGO0FBQW9HLElBQUEsUUFBUSxFQUFFLENBQUMsQ0FBL0c7QUFBa0gsa0JBQVlaO0FBQTlILE1BQ01aLFFBRE4sQ0FESjtBQUtILENBUE07OztBQVFQc0IsZ0JBQWdCLENBQUNMLFNBQWpCLHFCQUNPSCwwQkFBaUJHLFNBRHhCO0FBRUlMLEVBQUFBLEtBQUssRUFBRVIsbUJBQVVFLE1BRnJCO0FBRTZCO0FBQ3pCaUIsRUFBQUEsTUFBTSxFQUFFbkIsbUJBQVVLLElBQVYsQ0FBZUQsVUFIM0I7QUFJSWdCLEVBQUFBLFFBQVEsRUFBRXBCLG1CQUFVSyxJQUp4QjtBQUk4QjtBQUMxQlUsRUFBQUEsU0FBUyxFQUFFZixtQkFBVUUsTUFMekI7QUFLaUM7QUFDN0JjLEVBQUFBLE9BQU8sRUFBRWhCLG1CQUFVRyxJQUFWLENBQWVDO0FBTjVCLEcsQ0FTQTs7QUFDTyxNQUFNaUIsYUFBYSxHQUFHLFdBQStEO0FBQUEsTUFBOUQ7QUFBQ3pCLElBQUFBLFFBQUQ7QUFBV1ksSUFBQUEsS0FBWDtBQUFrQlcsSUFBQUEsTUFBTSxHQUFDLEtBQXpCO0FBQWdDQyxJQUFBQSxRQUFRLEdBQUM7QUFBekMsR0FBOEQ7QUFBQSxNQUFYL0csS0FBVztBQUN4RixRQUFNcUcsZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxTQUNJLDZCQUFDLGdCQUFELDZCQUFzQnZHLEtBQXRCO0FBQTZCLElBQUEsSUFBSSxFQUFDLGVBQWxDO0FBQWtELG9CQUFjOEcsTUFBaEU7QUFBd0UscUJBQWVDLFFBQXZGO0FBQWlHLElBQUEsUUFBUSxFQUFFLENBQUMsQ0FBNUc7QUFBK0csa0JBQVlaO0FBQTNILE1BQ01aLFFBRE4sQ0FESjtBQUtILENBUE07OztBQVFQeUIsYUFBYSxDQUFDUixTQUFkLHFCQUNPSCwwQkFBaUJHLFNBRHhCO0FBRUlMLEVBQUFBLEtBQUssRUFBRVIsbUJBQVVFLE1BRnJCO0FBRTZCO0FBQ3pCaUIsRUFBQUEsTUFBTSxFQUFFbkIsbUJBQVVLLElBQVYsQ0FBZUQsVUFIM0I7QUFJSWdCLEVBQUFBLFFBQVEsRUFBRXBCLG1CQUFVSyxJQUp4QjtBQUk4QjtBQUMxQlUsRUFBQUEsU0FBUyxFQUFFZixtQkFBVUUsTUFMekI7QUFLaUM7QUFDN0JjLEVBQUFBLE9BQU8sRUFBRWhCLG1CQUFVRyxJQUFWLENBQWVDO0FBTjVCLEcsQ0FTQTs7QUFDTyxNQUFNa0IsU0FBUyxHQUFHLENBQUNDLFdBQUQsRUFBY3RELGFBQWEsR0FBQyxFQUE1QixLQUFtQztBQUN4RCxRQUFNTCxJQUFJLEdBQUcyRCxXQUFXLENBQUMxRCxLQUFaLEdBQW9CN0MsTUFBTSxDQUFDd0csV0FBM0IsR0FBeUMsQ0FBdEQ7QUFDQSxNQUFJOUQsR0FBRyxHQUFHNkQsV0FBVyxDQUFDN0QsR0FBWixHQUFtQjZELFdBQVcsQ0FBQy9DLE1BQVosR0FBcUIsQ0FBeEMsR0FBNkN4RCxNQUFNLENBQUN5RyxXQUE5RDtBQUNBL0QsRUFBQUEsR0FBRyxJQUFJTyxhQUFhLEdBQUcsQ0FBdkIsQ0FId0QsQ0FHOUI7O0FBQzFCLFNBQU87QUFBQ0wsSUFBQUEsSUFBRDtBQUFPRixJQUFBQSxHQUFQO0FBQVlPLElBQUFBO0FBQVosR0FBUDtBQUNILENBTE0sQyxDQU9QOzs7OztBQUNPLE1BQU15RCxXQUFXLEdBQUcsQ0FBQ0gsV0FBRCxFQUFjOUQsV0FBVyxHQUFDLE1BQTFCLEtBQXFDO0FBQzVELFFBQU1rRSxXQUFXLEdBQUc7QUFBRWxFLElBQUFBO0FBQUYsR0FBcEI7QUFFQSxRQUFNbUUsV0FBVyxHQUFHTCxXQUFXLENBQUMxRCxLQUFaLEdBQW9CN0MsTUFBTSxDQUFDd0csV0FBL0M7QUFDQSxRQUFNSyxZQUFZLEdBQUdOLFdBQVcsQ0FBQzVELE1BQVosR0FBcUIzQyxNQUFNLENBQUN5RyxXQUFqRDtBQUNBLFFBQU1LLFNBQVMsR0FBR1AsV0FBVyxDQUFDN0QsR0FBWixHQUFrQjFDLE1BQU0sQ0FBQ3lHLFdBQTNDLENBTDRELENBTTVEOztBQUNBRSxFQUFBQSxXQUFXLENBQUM5RCxLQUFaLEdBQW9CN0MsTUFBTSxDQUFDK0csVUFBUCxHQUFvQkgsV0FBeEMsQ0FQNEQsQ0FRNUQ7O0FBQ0EsTUFBSUMsWUFBWSxHQUFHN0csTUFBTSxDQUFDZ0gsV0FBUCxHQUFxQixDQUF4QyxFQUEyQztBQUN2Q0wsSUFBQUEsV0FBVyxDQUFDakUsR0FBWixHQUFrQm1FLFlBQWxCO0FBQ0gsR0FGRCxNQUVPO0FBQ0hGLElBQUFBLFdBQVcsQ0FBQ2hFLE1BQVosR0FBcUIzQyxNQUFNLENBQUNnSCxXQUFQLEdBQXFCRixTQUExQztBQUNIOztBQUVELFNBQU9ILFdBQVA7QUFDSCxDQWhCTTs7OztBQWtCQSxNQUFNTSxjQUFjLEdBQUcsTUFBTTtBQUNoQyxRQUFNQyxNQUFNLEdBQUcsbUJBQU8sSUFBUCxDQUFmO0FBQ0EsUUFBTSxDQUFDQyxNQUFELEVBQVNDLFNBQVQsSUFBc0IscUJBQVMsS0FBVCxDQUE1Qjs7QUFDQSxRQUFNQyxJQUFJLEdBQUcsTUFBTTtBQUNmRCxJQUFBQSxTQUFTLENBQUMsSUFBRCxDQUFUO0FBQ0gsR0FGRDs7QUFHQSxRQUFNRSxLQUFLLEdBQUcsTUFBTTtBQUNoQkYsSUFBQUEsU0FBUyxDQUFDLEtBQUQsQ0FBVDtBQUNILEdBRkQ7O0FBSUEsU0FBTyxDQUFDRCxNQUFELEVBQVNELE1BQVQsRUFBaUJHLElBQWpCLEVBQXVCQyxLQUF2QixFQUE4QkYsU0FBOUIsQ0FBUDtBQUNILENBWE07Ozs7QUFhUSxNQUFNRyxpQkFBTixTQUFnQzdJLFdBQWhDLENBQTRDO0FBQ3ZEbUcsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsV0FBTyxLQUFLdkMsVUFBTCxDQUFnQixLQUFoQixDQUFQO0FBQ0g7O0FBSHNELEMsQ0FNM0Q7Ozs7O0FBQ08sU0FBU2tGLFVBQVQsQ0FBb0JDLFlBQXBCLEVBQWtDcEksS0FBbEMsRUFBeUM7QUFDNUMsUUFBTUMsVUFBVSxHQUFHLFVBQVMsR0FBR29JLElBQVosRUFBa0I7QUFDakM1QyxzQkFBUzZDLHNCQUFULENBQWdDM0osb0JBQW9CLEVBQXBEOztBQUVBLFFBQUlxQixLQUFLLElBQUlBLEtBQUssQ0FBQ0MsVUFBbkIsRUFBK0I7QUFDM0JELE1BQUFBLEtBQUssQ0FBQ0MsVUFBTixDQUFpQnNJLEtBQWpCLENBQXVCLElBQXZCLEVBQTZCRixJQUE3QjtBQUNIO0FBQ0osR0FORDs7QUFRQSxRQUFNRyxJQUFJLEdBQUcsNkJBQUMsaUJBQUQsNkJBQ0x4SSxLQURLO0FBRVQsSUFBQSxVQUFVLEVBQUVDLFVBRkgsQ0FFZTtBQUZmO0FBR1QsSUFBQSxZQUFZLEVBQUVBLFVBSEwsQ0FHaUI7O0FBSGpCLE1BS1QsNkJBQUMsWUFBRCw2QkFBa0JELEtBQWxCO0FBQXlCLElBQUEsVUFBVSxFQUFFQztBQUFyQyxLQUxTLENBQWI7O0FBUUF3RixvQkFBU0QsTUFBVCxDQUFnQmdELElBQWhCLEVBQXNCN0osb0JBQW9CLEVBQTFDOztBQUVBLFNBQU87QUFBQ3NKLElBQUFBLEtBQUssRUFBRWhJO0FBQVIsR0FBUDtBQUNIIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHt1c2VSZWYsIHVzZVN0YXRlfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IHtLZXl9IGZyb20gXCIuLi8uLi9LZXlib2FyZFwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi92aWV3cy9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcblxyXG4vLyBTaGFtZWxlc3NseSByaXBwZWQgb2ZmIE1vZGFsLmpzLiAgVGhlcmUncyBwcm9iYWJseSBhIGJldHRlciB3YXlcclxuLy8gb2YgZG9pbmcgcmV1c2FibGUgd2lkZ2V0cyBsaWtlIGRpYWxvZyBib3hlcyAmIG1lbnVzIHdoZXJlIHdlIGdvIGFuZFxyXG4vLyBwYXNzIGluIGEgY3VzdG9tIGNvbnRyb2wgYXMgdGhlIGFjdHVhbCBib2R5LlxyXG5cclxuY29uc3QgQ29udGV4dHVhbE1lbnVDb250YWluZXJJZCA9IFwibXhfQ29udGV4dHVhbE1lbnVfQ29udGFpbmVyXCI7XHJcblxyXG5mdW5jdGlvbiBnZXRPckNyZWF0ZUNvbnRhaW5lcigpIHtcclxuICAgIGxldCBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChDb250ZXh0dWFsTWVudUNvbnRhaW5lcklkKTtcclxuXHJcbiAgICBpZiAoIWNvbnRhaW5lcikge1xyXG4gICAgICAgIGNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgY29udGFpbmVyLmlkID0gQ29udGV4dHVhbE1lbnVDb250YWluZXJJZDtcclxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGNvbnRhaW5lcik7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGNvbnRhaW5lcjtcclxufVxyXG5cclxuY29uc3QgQVJJQV9NRU5VX0lURU1fUk9MRVMgPSBuZXcgU2V0KFtcIm1lbnVpdGVtXCIsIFwibWVudWl0ZW1jaGVja2JveFwiLCBcIm1lbnVpdGVtcmFkaW9cIl0pO1xyXG4vLyBHZW5lcmljIENvbnRleHRNZW51IFBvcnRhbCB3cmFwcGVyXHJcbi8vIGFsbCBvcHRpb25zIGluc2lkZSB0aGUgbWVudSBzaG91bGQgYmUgb2Ygcm9sZT1tZW51aXRlbS9tZW51aXRlbWNoZWNrYm94L21lbnVpdGVtcmFkaW9idXR0b24gYW5kIGhhdmUgdGFiSW5kZXg9ey0xfVxyXG4vLyB0aGlzIHdpbGwgYWxsb3cgdGhlIENvbnRleHRNZW51IHRvIG1hbmFnZSBpdHMgb3duIGZvY3VzIHVzaW5nIGFycm93IGtleXMgYXMgcGVyIHRoZSBBUklBIGd1aWRlbGluZXMuXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHRvcDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBib3R0b206IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAgICAgbGVmdDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICByaWdodDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBtZW51V2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAgICAgbWVudUhlaWdodDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBjaGV2cm9uT2Zmc2V0OiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIGNoZXZyb25GYWNlOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyB0b3AsIGJvdHRvbSwgbGVmdCwgcmlnaHQgb3Igbm9uZVxyXG4gICAgICAgIC8vIEZ1bmN0aW9uIHRvIGJlIGNhbGxlZCBvbiBtZW51IGNsb3NlXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICBtZW51UGFkZGluZ1RvcDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBtZW51UGFkZGluZ1JpZ2h0OiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIG1lbnVQYWRkaW5nQm90dG9tOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIG1lbnVQYWRkaW5nTGVmdDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICB6SW5kZXg6IFByb3BUeXBlcy5udW1iZXIsXHJcblxyXG4gICAgICAgIC8vIElmIHRydWUsIGluc2VydCBhbiBpbnZpc2libGUgc2NyZWVuLXNpemVkIGVsZW1lbnQgYmVoaW5kIHRoZVxyXG4gICAgICAgIC8vIG1lbnUgdGhhdCB3aGVuIGNsaWNrZWQgd2lsbCBjbG9zZSBpdC5cclxuICAgICAgICBoYXNCYWNrZ3JvdW5kOiBQcm9wVHlwZXMuYm9vbCxcclxuXHJcbiAgICAgICAgLy8gb24gcmVzaXplIGNhbGxiYWNrXHJcbiAgICAgICAgd2luZG93UmVzaXplOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgbWFuYWdlZDogUHJvcFR5cGVzLmJvb2wsIC8vIHdoZXRoZXIgdGhpcyBjb250ZXh0IG1lbnUgc2hvdWxkIGJlIGZvY3VzIG1hbmFnZWQuIElmIGZhbHNlIGl0IG11c3QgaGFuZGxlIGl0c2VsZlxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xyXG4gICAgICAgIGhhc0JhY2tncm91bmQ6IHRydWUsXHJcbiAgICAgICAgbWFuYWdlZDogdHJ1ZSxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBjb250ZXh0TWVudUVsZW06IG51bGwsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gcGVyc2lzdCB3aGF0IGhhZCBmb2N1cyB3aGVuIHdlIGdvdCBpbml0aWFsaXplZCBzbyB3ZSBjYW4gcmV0dXJuIGl0IGFmdGVyXHJcbiAgICAgICAgdGhpcy5pbml0aWFsRm9jdXMgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIC8vIHJldHVybiBmb2N1cyB0byB0aGUgdGhpbmcgd2hpY2ggaGFkIGl0IGJlZm9yZSB1c1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbEZvY3VzLmZvY3VzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29sbGVjdENvbnRleHRNZW51UmVjdCA9IChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgLy8gV2UgZG9uJ3QgbmVlZCB0byBjbGVhbiB1cCB3aGVuIHVubW91bnRpbmcsIHNvIGlnbm9yZVxyXG4gICAgICAgIGlmICghZWxlbWVudCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBsZXQgZmlyc3QgPSBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tyb2xlXj1cIm1lbnVpdGVtXCJdJyk7XHJcbiAgICAgICAgaWYgKCFmaXJzdCkge1xyXG4gICAgICAgICAgICBmaXJzdCA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcignW3RhYi1pbmRleF0nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpcnN0KSB7XHJcbiAgICAgICAgICAgIGZpcnN0LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgY29udGV4dE1lbnVFbGVtOiBlbGVtZW50LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBvbkNvbnRleHRNZW51ID0gKGUpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkZpbmlzaGVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG5cclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBjb25zdCB4ID0gZS5jbGllbnRYO1xyXG4gICAgICAgICAgICBjb25zdCB5ID0gZS5jbGllbnRZO1xyXG5cclxuICAgICAgICAgICAgLy8gWFhYOiBUaGlzIGlzbid0IHByZXR0eSBidXQgdGhlIG9ubHkgd2F5IHRvIGFsbG93IG9wZW5pbmcgYSBkaWZmZXJlbnQgY29udGV4dCBtZW51IG9uIHJpZ2h0IGNsaWNrIHdoaWxzdFxyXG4gICAgICAgICAgICAvLyBhIGNvbnRleHQgbWVudSBhbmQgaXRzIGNsaWNrLWd1YXJkIGFyZSB1cCB3aXRob3V0IGNvbXBsZXRlbHkgcmV3cml0aW5nIGhvdyB0aGUgY29udGV4dCBtZW51cyB3b3JrLlxyXG4gICAgICAgICAgICBzZXRJbW1lZGlhdGUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2xpY2tFdmVudCA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdNb3VzZUV2ZW50cycpO1xyXG4gICAgICAgICAgICAgICAgY2xpY2tFdmVudC5pbml0TW91c2VFdmVudChcclxuICAgICAgICAgICAgICAgICAgICAnY29udGV4dG1lbnUnLCB0cnVlLCB0cnVlLCB3aW5kb3csIDAsXHJcbiAgICAgICAgICAgICAgICAgICAgMCwgMCwgeCwgeSwgZmFsc2UsIGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGZhbHNlLCBmYWxzZSwgMCwgbnVsbCxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5lbGVtZW50RnJvbVBvaW50KHgsIHkpLmRpc3BhdGNoRXZlbnQoY2xpY2tFdmVudCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uTW92ZUZvY3VzID0gKGVsZW1lbnQsIHVwKSA9PiB7XHJcbiAgICAgICAgbGV0IGRlc2NlbmRpbmcgPSBmYWxzZTsgLy8gYXJlIHdlIGN1cnJlbnRseSBkZXNjZW5kaW5nIG9yIGFzY2VuZGluZyB0aHJvdWdoIHRoZSBET00gdHJlZT9cclxuXHJcbiAgICAgICAgZG8ge1xyXG4gICAgICAgICAgICBjb25zdCBjaGlsZCA9IHVwID8gZWxlbWVudC5sYXN0RWxlbWVudENoaWxkIDogZWxlbWVudC5maXJzdEVsZW1lbnRDaGlsZDtcclxuICAgICAgICAgICAgY29uc3Qgc2libGluZyA9IHVwID8gZWxlbWVudC5wcmV2aW91c0VsZW1lbnRTaWJsaW5nIDogZWxlbWVudC5uZXh0RWxlbWVudFNpYmxpbmc7XHJcblxyXG4gICAgICAgICAgICBpZiAoZGVzY2VuZGluZykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNoaWxkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudCA9IGNoaWxkO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzaWJsaW5nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudCA9IHNpYmxpbmc7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NlbmRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnRFbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNpYmxpbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50ID0gc2libGluZztcclxuICAgICAgICAgICAgICAgICAgICBkZXNjZW5kaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudCA9IGVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhcIm14X0NvbnRleHR1YWxNZW51XCIpKSB7IC8vIHdlIGhpdCB0aGUgdG9wXHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudCA9IHVwID8gZWxlbWVudC5sYXN0RWxlbWVudENoaWxkIDogZWxlbWVudC5maXJzdEVsZW1lbnRDaGlsZDtcclxuICAgICAgICAgICAgICAgICAgICBkZXNjZW5kaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gd2hpbGUgKGVsZW1lbnQgJiYgIUFSSUFfTUVOVV9JVEVNX1JPTEVTLmhhcyhlbGVtZW50LmdldEF0dHJpYnV0ZShcInJvbGVcIikpKTtcclxuXHJcbiAgICAgICAgaWYgKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgZWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uTW92ZUZvY3VzSG9tZUVuZCA9IChlbGVtZW50LCB1cCkgPT4ge1xyXG4gICAgICAgIGxldCByZXN1bHRzID0gZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbcm9sZV49XCJtZW51aXRlbVwiXScpO1xyXG4gICAgICAgIGlmICghcmVzdWx0cykge1xyXG4gICAgICAgICAgICByZXN1bHRzID0gZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbdGFiLWluZGV4XScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAocmVzdWx0cyAmJiByZXN1bHRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBpZiAodXApIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdHNbMF0uZm9jdXMoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdHNbcmVzdWx0cy5sZW5ndGggLSAxXS5mb2N1cygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfb25LZXlEb3duID0gKGV2KSA9PiB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLm1hbmFnZWQpIHtcclxuICAgICAgICAgICAgaWYgKGV2LmtleSA9PT0gS2V5LkVTQ0FQRSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGhhbmRsZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBzd2l0Y2ggKGV2LmtleSkge1xyXG4gICAgICAgICAgICBjYXNlIEtleS5UQUI6XHJcbiAgICAgICAgICAgIGNhc2UgS2V5LkVTQ0FQRTpcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgS2V5LkFSUk9XX1VQOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fb25Nb3ZlRm9jdXMoZXYudGFyZ2V0LCB0cnVlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19ET1dOOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fb25Nb3ZlRm9jdXMoZXYudGFyZ2V0LCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBLZXkuSE9NRTpcclxuICAgICAgICAgICAgICAgIHRoaXMuX29uTW92ZUZvY3VzSG9tZUVuZCh0aGlzLnN0YXRlLmNvbnRleHRNZW51RWxlbSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBLZXkuRU5EOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fb25Nb3ZlRm9jdXNIb21lRW5kKHRoaXMuc3RhdGUuY29udGV4dE1lbnVFbGVtLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGhhbmRsZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChoYW5kbGVkKSB7XHJcbiAgICAgICAgICAgIC8vIGNvbnN1bWUgYWxsIG90aGVyIGtleXMgaW4gY29udGV4dCBtZW51XHJcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyTWVudShoYXNCYWNrZ3JvdW5kPXRoaXMucHJvcHMuaGFzQmFja2dyb3VuZCkge1xyXG4gICAgICAgIGNvbnN0IHBvc2l0aW9uID0ge307XHJcbiAgICAgICAgbGV0IGNoZXZyb25GYWNlID0gbnVsbDtcclxuICAgICAgICBjb25zdCBwcm9wcyA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGlmIChwcm9wcy50b3ApIHtcclxuICAgICAgICAgICAgcG9zaXRpb24udG9wID0gcHJvcHMudG9wO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uLmJvdHRvbSA9IHByb3BzLmJvdHRvbTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChwcm9wcy5sZWZ0KSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uLmxlZnQgPSBwcm9wcy5sZWZ0O1xyXG4gICAgICAgICAgICBjaGV2cm9uRmFjZSA9ICdsZWZ0JztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBwb3NpdGlvbi5yaWdodCA9IHByb3BzLnJpZ2h0O1xyXG4gICAgICAgICAgICBjaGV2cm9uRmFjZSA9ICdyaWdodCc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb250ZXh0TWVudVJlY3QgPSB0aGlzLnN0YXRlLmNvbnRleHRNZW51RWxlbSA/IHRoaXMuc3RhdGUuY29udGV4dE1lbnVFbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpIDogbnVsbDtcclxuICAgICAgICBjb25zdCBwYWRkaW5nID0gMTA7XHJcblxyXG4gICAgICAgIGNvbnN0IGNoZXZyb25PZmZzZXQgPSB7fTtcclxuICAgICAgICBpZiAocHJvcHMuY2hldnJvbkZhY2UpIHtcclxuICAgICAgICAgICAgY2hldnJvbkZhY2UgPSBwcm9wcy5jaGV2cm9uRmFjZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgaGFzQ2hldnJvbiA9IGNoZXZyb25GYWNlICYmIGNoZXZyb25GYWNlICE9PSBcIm5vbmVcIjtcclxuXHJcbiAgICAgICAgaWYgKGNoZXZyb25GYWNlID09PSAndG9wJyB8fCBjaGV2cm9uRmFjZSA9PT0gJ2JvdHRvbScpIHtcclxuICAgICAgICAgICAgY2hldnJvbk9mZnNldC5sZWZ0ID0gcHJvcHMuY2hldnJvbk9mZnNldDtcclxuICAgICAgICB9IGVsc2UgaWYgKHBvc2l0aW9uLnRvcCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhcmdldCA9IHBvc2l0aW9uLnRvcDtcclxuXHJcbiAgICAgICAgICAgIC8vIEJ5IGRlZmF1bHQsIG5vIGFkanVzdG1lbnQgaXMgbWFkZVxyXG4gICAgICAgICAgICBsZXQgYWRqdXN0ZWQgPSB0YXJnZXQ7XHJcblxyXG4gICAgICAgICAgICAvLyBJZiB3ZSBrbm93IHRoZSBkaW1lbnNpb25zIG9mIHRoZSBjb250ZXh0IG1lbnUsIGFkanVzdCBpdHMgcG9zaXRpb25cclxuICAgICAgICAgICAgLy8gc3VjaCB0aGF0IGl0IGRvZXMgbm90IGxlYXZlIHRoZSAocGFkZGVkKSB3aW5kb3cuXHJcbiAgICAgICAgICAgIGlmIChjb250ZXh0TWVudVJlY3QpIHtcclxuICAgICAgICAgICAgICAgIGFkanVzdGVkID0gTWF0aC5taW4ocG9zaXRpb24udG9wLCBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodCAtIGNvbnRleHRNZW51UmVjdC5oZWlnaHQgLSBwYWRkaW5nKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcG9zaXRpb24udG9wID0gYWRqdXN0ZWQ7XHJcbiAgICAgICAgICAgIGNoZXZyb25PZmZzZXQudG9wID0gTWF0aC5tYXgocHJvcHMuY2hldnJvbk9mZnNldCwgcHJvcHMuY2hldnJvbk9mZnNldCArIHRhcmdldCAtIGFkanVzdGVkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBjaGV2cm9uO1xyXG4gICAgICAgIGlmIChoYXNDaGV2cm9uKSB7XHJcbiAgICAgICAgICAgIGNoZXZyb24gPSA8ZGl2IHN0eWxlPXtjaGV2cm9uT2Zmc2V0fSBjbGFzc05hbWU9e1wibXhfQ29udGV4dHVhbE1lbnVfY2hldnJvbl9cIiArIGNoZXZyb25GYWNlfSAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG1lbnVDbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudSc6IHRydWUsXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV9sZWZ0JzogIWhhc0NoZXZyb24gJiYgcG9zaXRpb24ubGVmdCxcclxuICAgICAgICAgICAgJ214X0NvbnRleHR1YWxNZW51X3JpZ2h0JzogIWhhc0NoZXZyb24gJiYgcG9zaXRpb24ucmlnaHQsXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV90b3AnOiAhaGFzQ2hldnJvbiAmJiBwb3NpdGlvbi50b3AsXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV9ib3R0b20nOiAhaGFzQ2hldnJvbiAmJiBwb3NpdGlvbi5ib3R0b20sXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV93aXRoQ2hldnJvbl9sZWZ0JzogY2hldnJvbkZhY2UgPT09ICdsZWZ0JyxcclxuICAgICAgICAgICAgJ214X0NvbnRleHR1YWxNZW51X3dpdGhDaGV2cm9uX3JpZ2h0JzogY2hldnJvbkZhY2UgPT09ICdyaWdodCcsXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV93aXRoQ2hldnJvbl90b3AnOiBjaGV2cm9uRmFjZSA9PT0gJ3RvcCcsXHJcbiAgICAgICAgICAgICdteF9Db250ZXh0dWFsTWVudV93aXRoQ2hldnJvbl9ib3R0b20nOiBjaGV2cm9uRmFjZSA9PT0gJ2JvdHRvbScsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1lbnVTdHlsZSA9IHt9O1xyXG4gICAgICAgIGlmIChwcm9wcy5tZW51V2lkdGgpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlLndpZHRoID0gcHJvcHMubWVudVdpZHRoO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHByb3BzLm1lbnVIZWlnaHQpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlLmhlaWdodCA9IHByb3BzLm1lbnVIZWlnaHQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWlzTmFOKE51bWJlcihwcm9wcy5tZW51UGFkZGluZ1RvcCkpKSB7XHJcbiAgICAgICAgICAgIG1lbnVTdHlsZVtcInBhZGRpbmdUb3BcIl0gPSBwcm9wcy5tZW51UGFkZGluZ1RvcDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFpc05hTihOdW1iZXIocHJvcHMubWVudVBhZGRpbmdMZWZ0KSkpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlW1wicGFkZGluZ0xlZnRcIl0gPSBwcm9wcy5tZW51UGFkZGluZ0xlZnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghaXNOYU4oTnVtYmVyKHByb3BzLm1lbnVQYWRkaW5nQm90dG9tKSkpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlW1wicGFkZGluZ0JvdHRvbVwiXSA9IHByb3BzLm1lbnVQYWRkaW5nQm90dG9tO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIWlzTmFOKE51bWJlcihwcm9wcy5tZW51UGFkZGluZ1JpZ2h0KSkpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlW1wicGFkZGluZ1JpZ2h0XCJdID0gcHJvcHMubWVudVBhZGRpbmdSaWdodDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHdyYXBwZXJTdHlsZSA9IHt9O1xyXG4gICAgICAgIGlmICghaXNOYU4oTnVtYmVyKHByb3BzLnpJbmRleCkpKSB7XHJcbiAgICAgICAgICAgIG1lbnVTdHlsZVtcInpJbmRleFwiXSA9IHByb3BzLnpJbmRleCArIDE7XHJcbiAgICAgICAgICAgIHdyYXBwZXJTdHlsZVtcInpJbmRleFwiXSA9IHByb3BzLnpJbmRleDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBiYWNrZ3JvdW5kO1xyXG4gICAgICAgIGlmIChoYXNCYWNrZ3JvdW5kKSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NvbnRleHR1YWxNZW51X2JhY2tncm91bmRcIiBzdHlsZT17d3JhcHBlclN0eWxlfSBvbkNsaWNrPXtwcm9wcy5vbkZpbmlzaGVkfSBvbkNvbnRleHRNZW51PXt0aGlzLm9uQ29udGV4dE1lbnV9IC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NvbnRleHR1YWxNZW51X3dyYXBwZXJcIiBzdHlsZT17ey4uLnBvc2l0aW9uLCAuLi53cmFwcGVyU3R5bGV9fSBvbktleURvd249e3RoaXMuX29uS2V5RG93bn0+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17bWVudUNsYXNzZXN9IHN0eWxlPXttZW51U3R5bGV9IHJlZj17dGhpcy5jb2xsZWN0Q29udGV4dE1lbnVSZWN0fSByb2xlPXt0aGlzLnByb3BzLm1hbmFnZWQgPyBcIm1lbnVcIiA6IHVuZGVmaW5lZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBjaGV2cm9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHByb3BzLmNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgeyBiYWNrZ3JvdW5kIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIFJlYWN0RE9NLmNyZWF0ZVBvcnRhbCh0aGlzLnJlbmRlck1lbnUoKSwgZ2V0T3JDcmVhdGVDb250YWluZXIoKSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFNlbWFudGljIGNvbXBvbmVudCBmb3IgcmVwcmVzZW50aW5nIHRoZSBBY2Nlc3NpYmxlQnV0dG9uIHdoaWNoIGxhdW5jaGVzIGEgPENvbnRleHRNZW51IC8+XHJcbmV4cG9ydCBjb25zdCBDb250ZXh0TWVudUJ1dHRvbiA9ICh7IGxhYmVsLCBpc0V4cGFuZGVkLCBjaGlsZHJlbiwgLi4ucHJvcHMgfSkgPT4ge1xyXG4gICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gey4uLnByb3BzfSB0aXRsZT17bGFiZWx9IGFyaWEtbGFiZWw9e2xhYmVsfSBhcmlhLWhhc3BvcHVwPXt0cnVlfSBhcmlhLWV4cGFuZGVkPXtpc0V4cGFuZGVkfT5cclxuICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgKTtcclxufTtcclxuQ29udGV4dE1lbnVCdXR0b24ucHJvcFR5cGVzID0ge1xyXG4gICAgLi4uQWNjZXNzaWJsZUJ1dHRvbi5wcm9wVHlwZXMsXHJcbiAgICBsYWJlbDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGlzRXhwYW5kZWQ6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsIC8vIHdoZXRoZXIgb3Igbm90IHRoZSBjb250ZXh0IG1lbnUgaXMgY3VycmVudGx5IG9wZW5cclxufTtcclxuXHJcbi8vIFNlbWFudGljIGNvbXBvbmVudCBmb3IgcmVwcmVzZW50aW5nIGEgcm9sZT1tZW51aXRlbVxyXG5leHBvcnQgY29uc3QgTWVudUl0ZW0gPSAoe2NoaWxkcmVuLCBsYWJlbCwgLi4ucHJvcHN9KSA9PiB7XHJcbiAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiB7Li4ucHJvcHN9IHJvbGU9XCJtZW51aXRlbVwiIHRhYkluZGV4PXstMX0gYXJpYS1sYWJlbD17bGFiZWx9PlxyXG4gICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICApO1xyXG59O1xyXG5NZW51SXRlbS5wcm9wVHlwZXMgPSB7XHJcbiAgICAuLi5BY2Nlc3NpYmxlQnV0dG9uLnByb3BUeXBlcyxcclxuICAgIGxhYmVsOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBvcHRpb25hbFxyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBvcHRpb25hbFxyXG4gICAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxufTtcclxuXHJcbi8vIFNlbWFudGljIGNvbXBvbmVudCBmb3IgcmVwcmVzZW50aW5nIGEgcm9sZT1ncm91cCBmb3IgZ3JvdXBpbmcgbWVudSByYWRpb3MvY2hlY2tib3hlc1xyXG5leHBvcnQgY29uc3QgTWVudUdyb3VwID0gKHtjaGlsZHJlbiwgbGFiZWwsIC4uLnByb3BzfSkgPT4ge1xyXG4gICAgcmV0dXJuIDxkaXYgey4uLnByb3BzfSByb2xlPVwiZ3JvdXBcIiBhcmlhLWxhYmVsPXtsYWJlbH0+XHJcbiAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICA8L2Rpdj47XHJcbn07XHJcbk1lbnVHcm91cC5wcm9wVHlwZXMgPSB7XHJcbiAgICBsYWJlbDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBvcHRpb25hbFxyXG59O1xyXG5cclxuLy8gU2VtYW50aWMgY29tcG9uZW50IGZvciByZXByZXNlbnRpbmcgYSByb2xlPW1lbnVpdGVtY2hlY2tib3hcclxuZXhwb3J0IGNvbnN0IE1lbnVJdGVtQ2hlY2tib3ggPSAoe2NoaWxkcmVuLCBsYWJlbCwgYWN0aXZlPWZhbHNlLCBkaXNhYmxlZD1mYWxzZSwgLi4ucHJvcHN9KSA9PiB7XHJcbiAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiB7Li4ucHJvcHN9IHJvbGU9XCJtZW51aXRlbWNoZWNrYm94XCIgYXJpYS1jaGVja2VkPXthY3RpdmV9IGFyaWEtZGlzYWJsZWQ9e2Rpc2FibGVkfSB0YWJJbmRleD17LTF9IGFyaWEtbGFiZWw9e2xhYmVsfT5cclxuICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgKTtcclxufTtcclxuTWVudUl0ZW1DaGVja2JveC5wcm9wVHlwZXMgPSB7XHJcbiAgICAuLi5BY2Nlc3NpYmxlQnV0dG9uLnByb3BUeXBlcyxcclxuICAgIGxhYmVsOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBvcHRpb25hbFxyXG4gICAgYWN0aXZlOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLCAvLyBvcHRpb25hbFxyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBvcHRpb25hbFxyXG4gICAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxufTtcclxuXHJcbi8vIFNlbWFudGljIGNvbXBvbmVudCBmb3IgcmVwcmVzZW50aW5nIGEgcm9sZT1tZW51aXRlbXJhZGlvXHJcbmV4cG9ydCBjb25zdCBNZW51SXRlbVJhZGlvID0gKHtjaGlsZHJlbiwgbGFiZWwsIGFjdGl2ZT1mYWxzZSwgZGlzYWJsZWQ9ZmFsc2UsIC4uLnByb3BzfSkgPT4ge1xyXG4gICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gey4uLnByb3BzfSByb2xlPVwibWVudWl0ZW1yYWRpb1wiIGFyaWEtY2hlY2tlZD17YWN0aXZlfSBhcmlhLWRpc2FibGVkPXtkaXNhYmxlZH0gdGFiSW5kZXg9ey0xfSBhcmlhLWxhYmVsPXtsYWJlbH0+XHJcbiAgICAgICAgICAgIHsgY2hpbGRyZW4gfVxyXG4gICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICk7XHJcbn07XHJcbk1lbnVJdGVtUmFkaW8ucHJvcFR5cGVzID0ge1xyXG4gICAgLi4uQWNjZXNzaWJsZUJ1dHRvbi5wcm9wVHlwZXMsXHJcbiAgICBsYWJlbDogUHJvcFR5cGVzLnN0cmluZywgLy8gb3B0aW9uYWxcclxuICAgIGFjdGl2ZTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCwgLy8gb3B0aW9uYWxcclxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZywgLy8gb3B0aW9uYWxcclxuICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbn07XHJcblxyXG4vLyBQbGFjZW1lbnQgbWV0aG9kIGZvciA8Q29udGV4dE1lbnUgLz4gdG8gcG9zaXRpb24gY29udGV4dCBtZW51IHRvIHJpZ2h0IG9mIGVsZW1lbnRSZWN0IHdpdGggY2hldnJvbk9mZnNldFxyXG5leHBvcnQgY29uc3QgdG9SaWdodE9mID0gKGVsZW1lbnRSZWN0LCBjaGV2cm9uT2Zmc2V0PTEyKSA9PiB7XHJcbiAgICBjb25zdCBsZWZ0ID0gZWxlbWVudFJlY3QucmlnaHQgKyB3aW5kb3cucGFnZVhPZmZzZXQgKyAzO1xyXG4gICAgbGV0IHRvcCA9IGVsZW1lbnRSZWN0LnRvcCArIChlbGVtZW50UmVjdC5oZWlnaHQgLyAyKSArIHdpbmRvdy5wYWdlWU9mZnNldDtcclxuICAgIHRvcCAtPSBjaGV2cm9uT2Zmc2V0ICsgODsgLy8gd2hlcmUgOCBpcyBoYWxmIHRoZSBoZWlnaHQgb2YgdGhlIGNoZXZyb25cclxuICAgIHJldHVybiB7bGVmdCwgdG9wLCBjaGV2cm9uT2Zmc2V0fTtcclxufTtcclxuXHJcbi8vIFBsYWNlbWVudCBtZXRob2QgZm9yIDxDb250ZXh0TWVudSAvPiB0byBwb3NpdGlvbiBjb250ZXh0IG1lbnUgcmlnaHQtYWxpZ25lZCBhbmQgZmxvd2luZyB0byB0aGUgbGVmdCBvZiBlbGVtZW50UmVjdFxyXG5leHBvcnQgY29uc3QgYWJvdmVMZWZ0T2YgPSAoZWxlbWVudFJlY3QsIGNoZXZyb25GYWNlPVwibm9uZVwiKSA9PiB7XHJcbiAgICBjb25zdCBtZW51T3B0aW9ucyA9IHsgY2hldnJvbkZhY2UgfTtcclxuXHJcbiAgICBjb25zdCBidXR0b25SaWdodCA9IGVsZW1lbnRSZWN0LnJpZ2h0ICsgd2luZG93LnBhZ2VYT2Zmc2V0O1xyXG4gICAgY29uc3QgYnV0dG9uQm90dG9tID0gZWxlbWVudFJlY3QuYm90dG9tICsgd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG4gICAgY29uc3QgYnV0dG9uVG9wID0gZWxlbWVudFJlY3QudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG4gICAgLy8gQWxpZ24gdGhlIHJpZ2h0IGVkZ2Ugb2YgdGhlIG1lbnUgdG8gdGhlIHJpZ2h0IGVkZ2Ugb2YgdGhlIGJ1dHRvblxyXG4gICAgbWVudU9wdGlvbnMucmlnaHQgPSB3aW5kb3cuaW5uZXJXaWR0aCAtIGJ1dHRvblJpZ2h0O1xyXG4gICAgLy8gQWxpZ24gdGhlIG1lbnUgdmVydGljYWxseSBvbiB3aGljaGV2ZXIgc2lkZSBvZiB0aGUgYnV0dG9uIGhhcyBtb3JlIHNwYWNlIGF2YWlsYWJsZS5cclxuICAgIGlmIChidXR0b25Cb3R0b20gPCB3aW5kb3cuaW5uZXJIZWlnaHQgLyAyKSB7XHJcbiAgICAgICAgbWVudU9wdGlvbnMudG9wID0gYnV0dG9uQm90dG9tO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBtZW51T3B0aW9ucy5ib3R0b20gPSB3aW5kb3cuaW5uZXJIZWlnaHQgLSBidXR0b25Ub3A7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG1lbnVPcHRpb25zO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHVzZUNvbnRleHRNZW51ID0gKCkgPT4ge1xyXG4gICAgY29uc3QgYnV0dG9uID0gdXNlUmVmKG51bGwpO1xyXG4gICAgY29uc3QgW2lzT3Blbiwgc2V0SXNPcGVuXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICAgIGNvbnN0IG9wZW4gPSAoKSA9PiB7XHJcbiAgICAgICAgc2V0SXNPcGVuKHRydWUpO1xyXG4gICAgfTtcclxuICAgIGNvbnN0IGNsb3NlID0gKCkgPT4ge1xyXG4gICAgICAgIHNldElzT3BlbihmYWxzZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiBbaXNPcGVuLCBidXR0b24sIG9wZW4sIGNsb3NlLCBzZXRJc09wZW5dO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGVnYWN5Q29udGV4dE1lbnUgZXh0ZW5kcyBDb250ZXh0TWVudSB7XHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVuZGVyTWVudShmYWxzZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFhYWDogRGVwcmVjYXRlZCwgdXNlZCBvbmx5IGZvciBkeW5hbWljIFRvb2x0aXBzLiBBdm9pZCB1c2luZyBhdCBhbGwgY29zdHMuXHJcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVNZW51KEVsZW1lbnRDbGFzcywgcHJvcHMpIHtcclxuICAgIGNvbnN0IG9uRmluaXNoZWQgPSBmdW5jdGlvbiguLi5hcmdzKSB7XHJcbiAgICAgICAgUmVhY3RET00udW5tb3VudENvbXBvbmVudEF0Tm9kZShnZXRPckNyZWF0ZUNvbnRhaW5lcigpKTtcclxuXHJcbiAgICAgICAgaWYgKHByb3BzICYmIHByb3BzLm9uRmluaXNoZWQpIHtcclxuICAgICAgICAgICAgcHJvcHMub25GaW5pc2hlZC5hcHBseShudWxsLCBhcmdzKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0IG1lbnUgPSA8TGVnYWN5Q29udGV4dE1lbnVcclxuICAgICAgICB7Li4ucHJvcHN9XHJcbiAgICAgICAgb25GaW5pc2hlZD17b25GaW5pc2hlZH0gLy8gZXNsaW50LWRpc2FibGUtbGluZSByZWFjdC9qc3gtbm8tYmluZFxyXG4gICAgICAgIHdpbmRvd1Jlc2l6ZT17b25GaW5pc2hlZH0gLy8gZXNsaW50LWRpc2FibGUtbGluZSByZWFjdC9qc3gtbm8tYmluZFxyXG4gICAgPlxyXG4gICAgICAgIDxFbGVtZW50Q2xhc3Mgey4uLnByb3BzfSBvbkZpbmlzaGVkPXtvbkZpbmlzaGVkfSAvPlxyXG4gICAgPC9MZWdhY3lDb250ZXh0TWVudT47XHJcblxyXG4gICAgUmVhY3RET00ucmVuZGVyKG1lbnUsIGdldE9yQ3JlYXRlQ29udGFpbmVyKCkpO1xyXG5cclxuICAgIHJldHVybiB7Y2xvc2U6IG9uRmluaXNoZWR9O1xyXG59XHJcbiJdfQ==