"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Tab = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var React = _interopRequireWildcard(require("react"));

var _languageHandler = require("../../languageHandler");

var PropTypes = _interopRequireWildcard(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../index"));

var _AutoHideScrollbar = _interopRequireDefault(require("./AutoHideScrollbar"));

/*
Copyright 2017 Travis Ralston
Copyright 2019 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Represents a tab for the TabbedView.
 */
class Tab {
  /**
   * Creates a new tab.
   * @param {string} tabLabel The untranslated tab label.
   * @param {string} tabIconClass The class for the tab icon. This should be a simple mask.
   * @param {React.ReactNode} tabJsx The JSX for the tab container.
   */
  constructor(tabLabel
  /*: string*/
  , tabIconClass
  /*: string*/
  , tabJsx
  /*: React.ReactNode*/
  ) {
    (0, _defineProperty2.default)(this, "label", void 0);
    (0, _defineProperty2.default)(this, "icon", void 0);
    (0, _defineProperty2.default)(this, "body", void 0);
    this.label = tabLabel;
    this.icon = tabIconClass;
    this.body = tabJsx;
  }

}

exports.Tab = Tab;

class TabbedView extends React.Component
/*:: <IProps, IState>*/
{
  constructor(props
  /*: IProps*/
  ) {
    super(props);
    this.state = {
      activeTabIndex: 0
    };
  }

  _getActiveTabIndex() {
    if (!this.state || !this.state.activeTabIndex) return 0;
    return this.state.activeTabIndex;
  }
  /**
   * Shows the given tab
   * @param {Tab} tab the tab to show
   * @private
   */


  _setActiveTab(tab
  /*: Tab*/
  ) {
    const idx = this.props.tabs.indexOf(tab);

    if (idx !== -1) {
      this.setState({
        activeTabIndex: idx
      });
    } else {
      console.error("Could not find tab " + tab.label + " in tabs");
    }
  }

  _renderTabLabel(tab
  /*: Tab*/
  ) {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let classes = "mx_TabbedView_tabLabel ";
    const idx = this.props.tabs.indexOf(tab);
    if (idx === this._getActiveTabIndex()) classes += "mx_TabbedView_tabLabel_active";
    let tabIcon = null;

    if (tab.icon) {
      tabIcon = React.createElement("span", {
        className: "mx_TabbedView_maskedIcon ".concat(tab.icon)
      });
    }

    const onClickHandler = () => this._setActiveTab(tab);

    const label = (0, _languageHandler._t)(tab.label);
    return React.createElement(AccessibleButton, {
      className: classes,
      key: "tab_label_" + tab.label,
      onClick: onClickHandler
    }, tabIcon, React.createElement("span", {
      className: "mx_TabbedView_tabLabel_text"
    }, label));
  }

  _renderTabPanel(tab
  /*: Tab*/
  )
  /*: React.ReactNode*/
  {
    return React.createElement("div", {
      className: "mx_TabbedView_tabPanel",
      key: "mx_tabpanel_" + tab.label
    }, React.createElement(_AutoHideScrollbar.default, {
      className: "mx_TabbedView_tabPanelContent"
    }, tab.body));
  }

  render()
  /*: React.ReactNode*/
  {
    const labels = this.props.tabs.map(tab => this._renderTabLabel(tab));

    const panel = this._renderTabPanel(this.props.tabs[this._getActiveTabIndex()]);

    return React.createElement("div", {
      className: "mx_TabbedView"
    }, React.createElement("div", {
      className: "mx_TabbedView_tabLabels"
    }, labels), panel);
  }

}

exports.default = TabbedView;
(0, _defineProperty2.default)(TabbedView, "propTypes", {
  // The tabs to show
  tabs: PropTypes.arrayOf(PropTypes.instanceOf(Tab)).isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvVGFiYmVkVmlldy50c3giXSwibmFtZXMiOlsiVGFiIiwiY29uc3RydWN0b3IiLCJ0YWJMYWJlbCIsInRhYkljb25DbGFzcyIsInRhYkpzeCIsImxhYmVsIiwiaWNvbiIsImJvZHkiLCJUYWJiZWRWaWV3IiwiUmVhY3QiLCJDb21wb25lbnQiLCJwcm9wcyIsInN0YXRlIiwiYWN0aXZlVGFiSW5kZXgiLCJfZ2V0QWN0aXZlVGFiSW5kZXgiLCJfc2V0QWN0aXZlVGFiIiwidGFiIiwiaWR4IiwidGFicyIsImluZGV4T2YiLCJzZXRTdGF0ZSIsImNvbnNvbGUiLCJlcnJvciIsIl9yZW5kZXJUYWJMYWJlbCIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjbGFzc2VzIiwidGFiSWNvbiIsIm9uQ2xpY2tIYW5kbGVyIiwiX3JlbmRlclRhYlBhbmVsIiwicmVuZGVyIiwibGFiZWxzIiwibWFwIiwicGFuZWwiLCJQcm9wVHlwZXMiLCJhcnJheU9mIiwiaW5zdGFuY2VPZiIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFrQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdEJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkE7OztBQUdPLE1BQU1BLEdBQU4sQ0FBVTtBQUtiOzs7Ozs7QUFNQUMsRUFBQUEsV0FBVyxDQUFDQztBQUFEO0FBQUEsSUFBbUJDO0FBQW5CO0FBQUEsSUFBeUNDO0FBQXpDO0FBQUEsSUFBa0U7QUFBQTtBQUFBO0FBQUE7QUFDekUsU0FBS0MsS0FBTCxHQUFhSCxRQUFiO0FBQ0EsU0FBS0ksSUFBTCxHQUFZSCxZQUFaO0FBQ0EsU0FBS0ksSUFBTCxHQUFZSCxNQUFaO0FBQ0g7O0FBZlk7Ozs7QUEwQkYsTUFBTUksVUFBTixTQUF5QkMsS0FBSyxDQUFDQztBQUEvQjtBQUF5RDtBQU1wRVQsRUFBQUEsV0FBVyxDQUFDVTtBQUFEO0FBQUEsSUFBZ0I7QUFDdkIsVUFBTUEsS0FBTjtBQUVBLFNBQUtDLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxjQUFjLEVBQUU7QUFEUCxLQUFiO0FBR0g7O0FBRU9DLEVBQUFBLGtCQUFSLEdBQTZCO0FBQ3pCLFFBQUksQ0FBQyxLQUFLRixLQUFOLElBQWUsQ0FBQyxLQUFLQSxLQUFMLENBQVdDLGNBQS9CLEVBQStDLE9BQU8sQ0FBUDtBQUMvQyxXQUFPLEtBQUtELEtBQUwsQ0FBV0MsY0FBbEI7QUFDSDtBQUVEOzs7Ozs7O0FBS1FFLEVBQUFBLGFBQVIsQ0FBc0JDO0FBQXRCO0FBQUEsSUFBZ0M7QUFDNUIsVUFBTUMsR0FBRyxHQUFHLEtBQUtOLEtBQUwsQ0FBV08sSUFBWCxDQUFnQkMsT0FBaEIsQ0FBd0JILEdBQXhCLENBQVo7O0FBQ0EsUUFBSUMsR0FBRyxLQUFLLENBQUMsQ0FBYixFQUFnQjtBQUNaLFdBQUtHLFFBQUwsQ0FBYztBQUFDUCxRQUFBQSxjQUFjLEVBQUVJO0FBQWpCLE9BQWQ7QUFDSCxLQUZELE1BRU87QUFDSEksTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsd0JBQXdCTixHQUFHLENBQUNYLEtBQTVCLEdBQW9DLFVBQWxEO0FBQ0g7QUFDSjs7QUFFT2tCLEVBQUFBLGVBQVIsQ0FBd0JQO0FBQXhCO0FBQUEsSUFBa0M7QUFDOUIsVUFBTVEsZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFFQSxRQUFJQyxPQUFPLEdBQUcseUJBQWQ7QUFFQSxVQUFNVixHQUFHLEdBQUcsS0FBS04sS0FBTCxDQUFXTyxJQUFYLENBQWdCQyxPQUFoQixDQUF3QkgsR0FBeEIsQ0FBWjtBQUNBLFFBQUlDLEdBQUcsS0FBSyxLQUFLSCxrQkFBTCxFQUFaLEVBQXVDYSxPQUFPLElBQUksK0JBQVg7QUFFdkMsUUFBSUMsT0FBTyxHQUFHLElBQWQ7O0FBQ0EsUUFBSVosR0FBRyxDQUFDVixJQUFSLEVBQWM7QUFDVnNCLE1BQUFBLE9BQU8sR0FBRztBQUFNLFFBQUEsU0FBUyxxQ0FBOEJaLEdBQUcsQ0FBQ1YsSUFBbEM7QUFBZixRQUFWO0FBQ0g7O0FBRUQsVUFBTXVCLGNBQWMsR0FBRyxNQUFNLEtBQUtkLGFBQUwsQ0FBbUJDLEdBQW5CLENBQTdCOztBQUVBLFVBQU1YLEtBQUssR0FBRyx5QkFBR1csR0FBRyxDQUFDWCxLQUFQLENBQWQ7QUFDQSxXQUNJLG9CQUFDLGdCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFFc0IsT0FBN0I7QUFBc0MsTUFBQSxHQUFHLEVBQUUsZUFBZVgsR0FBRyxDQUFDWCxLQUE5RDtBQUFxRSxNQUFBLE9BQU8sRUFBRXdCO0FBQTlFLE9BQ0tELE9BREwsRUFFSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ012QixLQUROLENBRkosQ0FESjtBQVFIOztBQUVPeUIsRUFBQUEsZUFBUixDQUF3QmQ7QUFBeEI7QUFBQTtBQUFBO0FBQW1EO0FBQy9DLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQyx3QkFBZjtBQUF3QyxNQUFBLEdBQUcsRUFBRSxpQkFBaUJBLEdBQUcsQ0FBQ1g7QUFBbEUsT0FDSSxvQkFBQywwQkFBRDtBQUFtQixNQUFBLFNBQVMsRUFBQztBQUE3QixPQUNLVyxHQUFHLENBQUNULElBRFQsQ0FESixDQURKO0FBT0g7O0FBRU13QixFQUFBQSxNQUFQO0FBQUE7QUFBaUM7QUFDN0IsVUFBTUMsTUFBTSxHQUFHLEtBQUtyQixLQUFMLENBQVdPLElBQVgsQ0FBZ0JlLEdBQWhCLENBQW9CakIsR0FBRyxJQUFJLEtBQUtPLGVBQUwsQ0FBcUJQLEdBQXJCLENBQTNCLENBQWY7O0FBQ0EsVUFBTWtCLEtBQUssR0FBRyxLQUFLSixlQUFMLENBQXFCLEtBQUtuQixLQUFMLENBQVdPLElBQVgsQ0FBZ0IsS0FBS0osa0JBQUwsRUFBaEIsQ0FBckIsQ0FBZDs7QUFFQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLa0IsTUFETCxDQURKLEVBSUtFLEtBSkwsQ0FESjtBQVFIOztBQWpGbUU7Ozs4QkFBbkQxQixVLGVBQ0U7QUFDZjtBQUNBVSxFQUFBQSxJQUFJLEVBQUVpQixTQUFTLENBQUNDLE9BQVYsQ0FBa0JELFNBQVMsQ0FBQ0UsVUFBVixDQUFxQnJDLEdBQXJCLENBQWxCLEVBQTZDc0M7QUFGcEMsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFRyYXZpcyBSYWxzdG9uXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7X3R9IGZyb20gJy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCBBdXRvSGlkZVNjcm9sbGJhciBmcm9tICcuL0F1dG9IaWRlU2Nyb2xsYmFyJztcclxuaW1wb3J0IHsgUmVhY3ROb2RlIH0gZnJvbSBcInJlYWN0XCI7XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyBhIHRhYiBmb3IgdGhlIFRhYmJlZFZpZXcuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgVGFiIHtcclxuICAgIHB1YmxpYyBsYWJlbDogc3RyaW5nO1xyXG4gICAgcHVibGljIGljb246IHN0cmluZztcclxuICAgIHB1YmxpYyBib2R5OiBSZWFjdC5SZWFjdE5vZGU7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgbmV3IHRhYi5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0YWJMYWJlbCBUaGUgdW50cmFuc2xhdGVkIHRhYiBsYWJlbC5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0YWJJY29uQ2xhc3MgVGhlIGNsYXNzIGZvciB0aGUgdGFiIGljb24uIFRoaXMgc2hvdWxkIGJlIGEgc2ltcGxlIG1hc2suXHJcbiAgICAgKiBAcGFyYW0ge1JlYWN0LlJlYWN0Tm9kZX0gdGFiSnN4IFRoZSBKU1ggZm9yIHRoZSB0YWIgY29udGFpbmVyLlxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3Rvcih0YWJMYWJlbDogc3RyaW5nLCB0YWJJY29uQ2xhc3M6IHN0cmluZywgdGFiSnN4OiBSZWFjdC5SZWFjdE5vZGUpIHtcclxuICAgICAgICB0aGlzLmxhYmVsID0gdGFiTGFiZWw7XHJcbiAgICAgICAgdGhpcy5pY29uID0gdGFiSWNvbkNsYXNzO1xyXG4gICAgICAgIHRoaXMuYm9keSA9IHRhYkpzeDtcclxuICAgIH1cclxufVxyXG5cclxuaW50ZXJmYWNlIElQcm9wcyB7XHJcbiAgICB0YWJzOiBUYWJbXTtcclxufVxyXG5cclxuaW50ZXJmYWNlIElTdGF0ZSB7XHJcbiAgICBhY3RpdmVUYWJJbmRleDogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUYWJiZWRWaWV3IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PElQcm9wcywgSVN0YXRlPiB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIFRoZSB0YWJzIHRvIHNob3dcclxuICAgICAgICB0YWJzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMuaW5zdGFuY2VPZihUYWIpKS5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogSVByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBhY3RpdmVUYWJJbmRleDogMCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2dldEFjdGl2ZVRhYkluZGV4KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZSB8fCAhdGhpcy5zdGF0ZS5hY3RpdmVUYWJJbmRleCkgcmV0dXJuIDA7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuYWN0aXZlVGFiSW5kZXg7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTaG93cyB0aGUgZ2l2ZW4gdGFiXHJcbiAgICAgKiBAcGFyYW0ge1RhYn0gdGFiIHRoZSB0YWIgdG8gc2hvd1xyXG4gICAgICogQHByaXZhdGVcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfc2V0QWN0aXZlVGFiKHRhYjogVGFiKSB7XHJcbiAgICAgICAgY29uc3QgaWR4ID0gdGhpcy5wcm9wcy50YWJzLmluZGV4T2YodGFiKTtcclxuICAgICAgICBpZiAoaWR4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHthY3RpdmVUYWJJbmRleDogaWR4fSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkNvdWxkIG5vdCBmaW5kIHRhYiBcIiArIHRhYi5sYWJlbCArIFwiIGluIHRhYnNcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3JlbmRlclRhYkxhYmVsKHRhYjogVGFiKSB7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgbGV0IGNsYXNzZXMgPSBcIm14X1RhYmJlZFZpZXdfdGFiTGFiZWwgXCI7XHJcblxyXG4gICAgICAgIGNvbnN0IGlkeCA9IHRoaXMucHJvcHMudGFicy5pbmRleE9mKHRhYik7XHJcbiAgICAgICAgaWYgKGlkeCA9PT0gdGhpcy5fZ2V0QWN0aXZlVGFiSW5kZXgoKSkgY2xhc3NlcyArPSBcIm14X1RhYmJlZFZpZXdfdGFiTGFiZWxfYWN0aXZlXCI7XHJcblxyXG4gICAgICAgIGxldCB0YWJJY29uID0gbnVsbDtcclxuICAgICAgICBpZiAodGFiLmljb24pIHtcclxuICAgICAgICAgICAgdGFiSWNvbiA9IDxzcGFuIGNsYXNzTmFtZT17YG14X1RhYmJlZFZpZXdfbWFza2VkSWNvbiAke3RhYi5pY29ufWB9IC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgb25DbGlja0hhbmRsZXIgPSAoKSA9PiB0aGlzLl9zZXRBY3RpdmVUYWIodGFiKTtcclxuXHJcbiAgICAgICAgY29uc3QgbGFiZWwgPSBfdCh0YWIubGFiZWwpO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT17Y2xhc3Nlc30ga2V5PXtcInRhYl9sYWJlbF9cIiArIHRhYi5sYWJlbH0gb25DbGljaz17b25DbGlja0hhbmRsZXJ9PlxyXG4gICAgICAgICAgICAgICAge3RhYkljb259XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9UYWJiZWRWaWV3X3RhYkxhYmVsX3RleHRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGxhYmVsIH1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVuZGVyVGFiUGFuZWwodGFiOiBUYWIpOiBSZWFjdC5SZWFjdE5vZGUge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVGFiYmVkVmlld190YWJQYW5lbFwiIGtleT17XCJteF90YWJwYW5lbF9cIiArIHRhYi5sYWJlbH0+XHJcbiAgICAgICAgICAgICAgICA8QXV0b0hpZGVTY3JvbGxiYXIgY2xhc3NOYW1lPSdteF9UYWJiZWRWaWV3X3RhYlBhbmVsQ29udGVudCc+XHJcbiAgICAgICAgICAgICAgICAgICAge3RhYi5ib2R5fVxyXG4gICAgICAgICAgICAgICAgPC9BdXRvSGlkZVNjcm9sbGJhcj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVuZGVyKCk6IFJlYWN0LlJlYWN0Tm9kZSB7XHJcbiAgICAgICAgY29uc3QgbGFiZWxzID0gdGhpcy5wcm9wcy50YWJzLm1hcCh0YWIgPT4gdGhpcy5fcmVuZGVyVGFiTGFiZWwodGFiKSk7XHJcbiAgICAgICAgY29uc3QgcGFuZWwgPSB0aGlzLl9yZW5kZXJUYWJQYW5lbCh0aGlzLnByb3BzLnRhYnNbdGhpcy5fZ2V0QWN0aXZlVGFiSW5kZXgoKV0pO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RhYmJlZFZpZXdcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVGFiYmVkVmlld190YWJMYWJlbHNcIj5cclxuICAgICAgICAgICAgICAgICAgICB7bGFiZWxzfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7cGFuZWx9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19