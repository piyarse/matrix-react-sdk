"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../index"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _ratelimitedfunc = _interopRequireDefault(require("../../ratelimitedfunc"));

var _GroupAddressPicker = require("../../GroupAddressPicker");

var _GroupStore = _interopRequireDefault(require("../../stores/GroupStore"));

var _SettingsStore = _interopRequireDefault(require("../../settings/SettingsStore"));

var _RightPanelStorePhases = require("../../stores/RightPanelStorePhases");

var _RightPanelStore = _interopRequireDefault(require("../../stores/RightPanelStore"));

var _MatrixClientContext = _interopRequireDefault(require("../../contexts/MatrixClientContext"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class RightPanel extends _react.default.Component {
  static get propTypes() {
    return {
      roomId: _propTypes.default.string,
      // if showing panels for a given room, this is set
      groupId: _propTypes.default.string,
      // if showing panels for a given group, this is set
      user: _propTypes.default.object // used if we know the user ahead of opening the panel

    };
  }

  constructor(props) {
    super(props);
    this.state = {
      phase: this._getPhaseFromProps(),
      isUserPrivilegedInGroup: null,
      member: this._getUserForPanel(),
      verificationRequest: _RightPanelStore.default.getSharedInstance().roomPanelPhaseParams.verificationRequest
    };
    this.onAction = this.onAction.bind(this);
    this.onRoomStateMember = this.onRoomStateMember.bind(this);
    this.onGroupStoreUpdated = this.onGroupStoreUpdated.bind(this);
    this.onInviteToGroupButtonClick = this.onInviteToGroupButtonClick.bind(this);
    this.onAddRoomToGroupButtonClick = this.onAddRoomToGroupButtonClick.bind(this);
    this._delayedUpdate = new _ratelimitedfunc.default(() => {
      this.forceUpdate();
    }, 500);
  } // Helper function to split out the logic for _getPhaseFromProps() and the constructor
  // as both are called at the same time in the constructor.


  _getUserForPanel() {
    if (this.state && this.state.member) return this.state.member;

    const lastParams = _RightPanelStore.default.getSharedInstance().roomPanelPhaseParams;

    return this.props.user || lastParams['member'];
  } // gets the current phase from the props and also maybe the store


  _getPhaseFromProps() {
    const rps = _RightPanelStore.default.getSharedInstance();

    const userForPanel = this._getUserForPanel();

    if (this.props.groupId) {
      if (!_RightPanelStorePhases.RIGHT_PANEL_PHASES_NO_ARGS.includes(rps.groupPanelPhase)) {
        _dispatcher.default.dispatch({
          action: "set_right_panel_phase",
          phase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList
        });

        return _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList;
      }

      return rps.groupPanelPhase;
    } else if (userForPanel) {
      // XXX FIXME AAAAAARGH: What is going on with this class!? It takes some of its state
      // from its props and some from a store, except if the contents of the store changes
      // while it's mounted in which case it replaces all of its state with that of the store,
      // except it uses a dispatch instead of a normal store listener?
      // Unfortunately rewriting this would almost certainly break showing the right panel
      // in some of the many cases, and I don't have time to re-architect it and test all
      // the flows now, so adding yet another special case so if the store thinks there is
      // a verification going on for the member we're displaying, we show that, otherwise
      // we race if a verification is started while the panel isn't displayed because we're
      // not mounted in time to get the dispatch.
      // Until then, let this code serve as a warning from history.
      if (rps.roomPanelPhaseParams.member && userForPanel.userId === rps.roomPanelPhaseParams.member.userId && rps.roomPanelPhaseParams.verificationRequest) {
        return rps.roomPanelPhase;
      }

      return _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo;
    } else {
      if (!_RightPanelStorePhases.RIGHT_PANEL_PHASES_NO_ARGS.includes(rps.roomPanelPhase)) {
        _dispatcher.default.dispatch({
          action: "set_right_panel_phase",
          phase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberList
        });

        return _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberList;
      }

      return rps.roomPanelPhase;
    }
  }

  componentDidMount() {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    const cli = this.context;
    cli.on("RoomState.members", this.onRoomStateMember);

    this._initGroupStore(this.props.groupId);
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this.dispatcherRef);

    if (this.context) {
      this.context.removeListener("RoomState.members", this.onRoomStateMember);
    }

    this._unregisterGroupStore(this.props.groupId);
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(newProps) {
    // eslint-disable-line camelcase
    if (newProps.groupId !== this.props.groupId) {
      this._unregisterGroupStore(this.props.groupId);

      this._initGroupStore(newProps.groupId);
    }
  }

  _initGroupStore(groupId) {
    if (!groupId) return;

    _GroupStore.default.registerListener(groupId, this.onGroupStoreUpdated);
  }

  _unregisterGroupStore() {
    _GroupStore.default.unregisterListener(this.onGroupStoreUpdated);
  }

  onGroupStoreUpdated() {
    this.setState({
      isUserPrivilegedInGroup: _GroupStore.default.isUserPrivileged(this.props.groupId)
    });
  }

  onInviteToGroupButtonClick() {
    (0, _GroupAddressPicker.showGroupInviteDialog)(this.props.groupId).then(() => {
      this.setState({
        phase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList
      });
    });
  }

  onAddRoomToGroupButtonClick() {
    (0, _GroupAddressPicker.showGroupAddRoomDialog)(this.props.groupId).then(() => {
      this.forceUpdate();
    });
  }

  onRoomStateMember(ev, state, member) {
    if (member.roomId !== this.props.roomId) {
      return;
    } // redraw the badge on the membership list


    if (this.state.phase === _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberList && member.roomId === this.props.roomId) {
      this._delayedUpdate();
    } else if (this.state.phase === _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo && member.roomId === this.props.roomId && member.userId === this.state.member.userId) {
      // refresh the member info (e.g. new power level)
      this._delayedUpdate();
    }
  }

  onAction(payload) {
    if (payload.action === "after_right_panel_phase_change") {
      this.setState({
        phase: payload.phase,
        groupRoomId: payload.groupRoomId,
        groupId: payload.groupId,
        member: payload.member,
        event: payload.event,
        verificationRequest: payload.verificationRequest,
        verificationRequestPromise: payload.verificationRequestPromise
      });
    }
  }

  render() {
    const MemberList = sdk.getComponent('rooms.MemberList');
    const MemberInfo = sdk.getComponent('rooms.MemberInfo');
    const UserInfo = sdk.getComponent('right_panel.UserInfo');
    const ThirdPartyMemberInfo = sdk.getComponent('rooms.ThirdPartyMemberInfo');
    const NotificationPanel = sdk.getComponent('structures.NotificationPanel');
    const FilePanel = sdk.getComponent('structures.FilePanel');
    const GroupMemberList = sdk.getComponent('groups.GroupMemberList');
    const GroupMemberInfo = sdk.getComponent('groups.GroupMemberInfo');
    const GroupRoomList = sdk.getComponent('groups.GroupRoomList');
    const GroupRoomInfo = sdk.getComponent('groups.GroupRoomInfo');

    let panel = _react.default.createElement("div", null);

    switch (this.state.phase) {
      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberList:
        if (this.props.roomId) {
          panel = _react.default.createElement(MemberList, {
            roomId: this.props.roomId,
            key: this.props.roomId
          });
        }

        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList:
        if (this.props.groupId) {
          panel = _react.default.createElement(GroupMemberList, {
            groupId: this.props.groupId,
            key: this.props.groupId
          });
        }

        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupRoomList:
        panel = _react.default.createElement(GroupRoomList, {
          groupId: this.props.groupId,
          key: this.props.groupId
        });
        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo:
      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel:
        if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
          const onClose = () => {
            _dispatcher.default.dispatch({
              action: "view_user",
              member: this.state.phase === _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel ? this.state.member : null
            });
          };

          panel = _react.default.createElement(UserInfo, {
            user: this.state.member,
            roomId: this.props.roomId,
            key: this.props.roomId || this.state.member.userId,
            onClose: onClose,
            phase: this.state.phase,
            verificationRequest: this.state.verificationRequest,
            verificationRequestPromise: this.state.verificationRequestPromise
          });
        } else {
          panel = _react.default.createElement(MemberInfo, {
            member: this.state.member,
            key: this.props.roomId || this.state.member.userId
          });
        }

        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.Room3pidMemberInfo:
        panel = _react.default.createElement(ThirdPartyMemberInfo, {
          event: this.state.event,
          key: this.props.roomId
        });
        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberInfo:
        if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
          const onClose = () => {
            _dispatcher.default.dispatch({
              action: "view_user",
              member: null
            });
          };

          panel = _react.default.createElement(UserInfo, {
            user: this.state.member,
            groupId: this.props.groupId,
            key: this.state.member.userId,
            onClose: onClose
          });
        } else {
          panel = _react.default.createElement(GroupMemberInfo, {
            groupMember: this.state.member,
            groupId: this.props.groupId,
            key: this.state.member.user_id
          });
        }

        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupRoomInfo:
        panel = _react.default.createElement(GroupRoomInfo, {
          groupRoomId: this.state.groupRoomId,
          groupId: this.props.groupId,
          key: this.state.groupRoomId
        });
        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.NotificationPanel:
        panel = _react.default.createElement(NotificationPanel, null);
        break;

      case _RightPanelStorePhases.RIGHT_PANEL_PHASES.FilePanel:
        panel = _react.default.createElement(FilePanel, {
          roomId: this.props.roomId,
          resizeNotifier: this.props.resizeNotifier
        });
        break;
    }

    const classes = (0, _classnames.default)("mx_RightPanel", "mx_fadable", {
      "collapsed": this.props.collapsed,
      "mx_fadable_faded": this.props.disabled,
      "dark-panel": true
    });
    return _react.default.createElement("aside", {
      className: classes,
      id: "mx_RightPanel"
    }, panel);
  }

}

exports.default = RightPanel;
(0, _defineProperty2.default)(RightPanel, "contextType", _MatrixClientContext.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvUmlnaHRQYW5lbC5qcyJdLCJuYW1lcyI6WyJSaWdodFBhbmVsIiwiUmVhY3QiLCJDb21wb25lbnQiLCJwcm9wVHlwZXMiLCJyb29tSWQiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJncm91cElkIiwidXNlciIsIm9iamVjdCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJzdGF0ZSIsInBoYXNlIiwiX2dldFBoYXNlRnJvbVByb3BzIiwiaXNVc2VyUHJpdmlsZWdlZEluR3JvdXAiLCJtZW1iZXIiLCJfZ2V0VXNlckZvclBhbmVsIiwidmVyaWZpY2F0aW9uUmVxdWVzdCIsIlJpZ2h0UGFuZWxTdG9yZSIsImdldFNoYXJlZEluc3RhbmNlIiwicm9vbVBhbmVsUGhhc2VQYXJhbXMiLCJvbkFjdGlvbiIsImJpbmQiLCJvblJvb21TdGF0ZU1lbWJlciIsIm9uR3JvdXBTdG9yZVVwZGF0ZWQiLCJvbkludml0ZVRvR3JvdXBCdXR0b25DbGljayIsIm9uQWRkUm9vbVRvR3JvdXBCdXR0b25DbGljayIsIl9kZWxheWVkVXBkYXRlIiwiUmF0ZUxpbWl0ZWRGdW5jIiwiZm9yY2VVcGRhdGUiLCJsYXN0UGFyYW1zIiwicnBzIiwidXNlckZvclBhbmVsIiwiUklHSFRfUEFORUxfUEhBU0VTX05PX0FSR1MiLCJpbmNsdWRlcyIsImdyb3VwUGFuZWxQaGFzZSIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwiUklHSFRfUEFORUxfUEhBU0VTIiwiR3JvdXBNZW1iZXJMaXN0IiwidXNlcklkIiwicm9vbVBhbmVsUGhhc2UiLCJSb29tTWVtYmVySW5mbyIsIlJvb21NZW1iZXJMaXN0IiwiY29tcG9uZW50RGlkTW91bnQiLCJkaXNwYXRjaGVyUmVmIiwicmVnaXN0ZXIiLCJjbGkiLCJjb250ZXh0Iiwib24iLCJfaW5pdEdyb3VwU3RvcmUiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJyZW1vdmVMaXN0ZW5lciIsIl91bnJlZ2lzdGVyR3JvdXBTdG9yZSIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV3UHJvcHMiLCJHcm91cFN0b3JlIiwicmVnaXN0ZXJMaXN0ZW5lciIsInVucmVnaXN0ZXJMaXN0ZW5lciIsInNldFN0YXRlIiwiaXNVc2VyUHJpdmlsZWdlZCIsInRoZW4iLCJldiIsInBheWxvYWQiLCJncm91cFJvb21JZCIsImV2ZW50IiwidmVyaWZpY2F0aW9uUmVxdWVzdFByb21pc2UiLCJyZW5kZXIiLCJNZW1iZXJMaXN0Iiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTWVtYmVySW5mbyIsIlVzZXJJbmZvIiwiVGhpcmRQYXJ0eU1lbWJlckluZm8iLCJOb3RpZmljYXRpb25QYW5lbCIsIkZpbGVQYW5lbCIsIkdyb3VwTWVtYmVySW5mbyIsIkdyb3VwUm9vbUxpc3QiLCJHcm91cFJvb21JbmZvIiwicGFuZWwiLCJFbmNyeXB0aW9uUGFuZWwiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsIm9uQ2xvc2UiLCJSb29tM3BpZE1lbWJlckluZm8iLCJ1c2VyX2lkIiwicmVzaXplTm90aWZpZXIiLCJjbGFzc2VzIiwiY29sbGFwc2VkIiwiZGlzYWJsZWQiLCJNYXRyaXhDbGllbnRDb250ZXh0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBb0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQS9CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlDZSxNQUFNQSxVQUFOLFNBQXlCQyxlQUFNQyxTQUEvQixDQUF5QztBQUNwRCxhQUFXQyxTQUFYLEdBQXVCO0FBQ25CLFdBQU87QUFDSEMsTUFBQUEsTUFBTSxFQUFFQyxtQkFBVUMsTUFEZjtBQUN1QjtBQUMxQkMsTUFBQUEsT0FBTyxFQUFFRixtQkFBVUMsTUFGaEI7QUFFd0I7QUFDM0JFLE1BQUFBLElBQUksRUFBRUgsbUJBQVVJLE1BSGIsQ0FHcUI7O0FBSHJCLEtBQVA7QUFLSDs7QUFJREMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBQ0EsU0FBS0MsS0FBTCxHQUFhO0FBQ1RDLE1BQUFBLEtBQUssRUFBRSxLQUFLQyxrQkFBTCxFQURFO0FBRVRDLE1BQUFBLHVCQUF1QixFQUFFLElBRmhCO0FBR1RDLE1BQUFBLE1BQU0sRUFBRSxLQUFLQyxnQkFBTCxFQUhDO0FBSVRDLE1BQUFBLG1CQUFtQixFQUFFQyx5QkFBZ0JDLGlCQUFoQixHQUFvQ0Msb0JBQXBDLENBQXlESDtBQUpyRSxLQUFiO0FBTUEsU0FBS0ksUUFBTCxHQUFnQixLQUFLQSxRQUFMLENBQWNDLElBQWQsQ0FBbUIsSUFBbkIsQ0FBaEI7QUFDQSxTQUFLQyxpQkFBTCxHQUF5QixLQUFLQSxpQkFBTCxDQUF1QkQsSUFBdkIsQ0FBNEIsSUFBNUIsQ0FBekI7QUFDQSxTQUFLRSxtQkFBTCxHQUEyQixLQUFLQSxtQkFBTCxDQUF5QkYsSUFBekIsQ0FBOEIsSUFBOUIsQ0FBM0I7QUFDQSxTQUFLRywwQkFBTCxHQUFrQyxLQUFLQSwwQkFBTCxDQUFnQ0gsSUFBaEMsQ0FBcUMsSUFBckMsQ0FBbEM7QUFDQSxTQUFLSSwyQkFBTCxHQUFtQyxLQUFLQSwyQkFBTCxDQUFpQ0osSUFBakMsQ0FBc0MsSUFBdEMsQ0FBbkM7QUFFQSxTQUFLSyxjQUFMLEdBQXNCLElBQUlDLHdCQUFKLENBQW9CLE1BQU07QUFDNUMsV0FBS0MsV0FBTDtBQUNILEtBRnFCLEVBRW5CLEdBRm1CLENBQXRCO0FBR0gsR0E1Qm1ELENBOEJwRDtBQUNBOzs7QUFDQWIsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixRQUFJLEtBQUtMLEtBQUwsSUFBYyxLQUFLQSxLQUFMLENBQVdJLE1BQTdCLEVBQXFDLE9BQU8sS0FBS0osS0FBTCxDQUFXSSxNQUFsQjs7QUFDckMsVUFBTWUsVUFBVSxHQUFHWix5QkFBZ0JDLGlCQUFoQixHQUFvQ0Msb0JBQXZEOztBQUNBLFdBQU8sS0FBS1YsS0FBTCxDQUFXSCxJQUFYLElBQW1CdUIsVUFBVSxDQUFDLFFBQUQsQ0FBcEM7QUFDSCxHQXBDbUQsQ0FzQ3BEOzs7QUFDQWpCLEVBQUFBLGtCQUFrQixHQUFHO0FBQ2pCLFVBQU1rQixHQUFHLEdBQUdiLHlCQUFnQkMsaUJBQWhCLEVBQVo7O0FBQ0EsVUFBTWEsWUFBWSxHQUFHLEtBQUtoQixnQkFBTCxFQUFyQjs7QUFDQSxRQUFJLEtBQUtOLEtBQUwsQ0FBV0osT0FBZixFQUF3QjtBQUNwQixVQUFJLENBQUMyQixrREFBMkJDLFFBQTNCLENBQW9DSCxHQUFHLENBQUNJLGVBQXhDLENBQUwsRUFBK0Q7QUFDM0RDLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFLHVCQUFUO0FBQWtDMUIsVUFBQUEsS0FBSyxFQUFFMkIsMENBQW1CQztBQUE1RCxTQUFiOztBQUNBLGVBQU9ELDBDQUFtQkMsZUFBMUI7QUFDSDs7QUFDRCxhQUFPVCxHQUFHLENBQUNJLGVBQVg7QUFDSCxLQU5ELE1BTU8sSUFBSUgsWUFBSixFQUFrQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFDSUQsR0FBRyxDQUFDWCxvQkFBSixDQUF5QkwsTUFBekIsSUFDQWlCLFlBQVksQ0FBQ1MsTUFBYixLQUF3QlYsR0FBRyxDQUFDWCxvQkFBSixDQUF5QkwsTUFBekIsQ0FBZ0MwQixNQUR4RCxJQUVBVixHQUFHLENBQUNYLG9CQUFKLENBQXlCSCxtQkFIN0IsRUFJRTtBQUNFLGVBQU9jLEdBQUcsQ0FBQ1csY0FBWDtBQUNIOztBQUNELGFBQU9ILDBDQUFtQkksY0FBMUI7QUFDSCxLQXBCTSxNQW9CQTtBQUNILFVBQUksQ0FBQ1Ysa0RBQTJCQyxRQUEzQixDQUFvQ0gsR0FBRyxDQUFDVyxjQUF4QyxDQUFMLEVBQThEO0FBQzFETiw0QkFBSUMsUUFBSixDQUFhO0FBQUNDLFVBQUFBLE1BQU0sRUFBRSx1QkFBVDtBQUFrQzFCLFVBQUFBLEtBQUssRUFBRTJCLDBDQUFtQks7QUFBNUQsU0FBYjs7QUFDQSxlQUFPTCwwQ0FBbUJLLGNBQTFCO0FBQ0g7O0FBQ0QsYUFBT2IsR0FBRyxDQUFDVyxjQUFYO0FBQ0g7QUFDSjs7QUFFREcsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsU0FBS0MsYUFBTCxHQUFxQlYsb0JBQUlXLFFBQUosQ0FBYSxLQUFLMUIsUUFBbEIsQ0FBckI7QUFDQSxVQUFNMkIsR0FBRyxHQUFHLEtBQUtDLE9BQWpCO0FBQ0FELElBQUFBLEdBQUcsQ0FBQ0UsRUFBSixDQUFPLG1CQUFQLEVBQTRCLEtBQUszQixpQkFBakM7O0FBQ0EsU0FBSzRCLGVBQUwsQ0FBcUIsS0FBS3pDLEtBQUwsQ0FBV0osT0FBaEM7QUFDSDs7QUFFRDhDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CaEIsd0JBQUlpQixVQUFKLENBQWUsS0FBS1AsYUFBcEI7O0FBQ0EsUUFBSSxLQUFLRyxPQUFULEVBQWtCO0FBQ2QsV0FBS0EsT0FBTCxDQUFhSyxjQUFiLENBQTRCLG1CQUE1QixFQUFpRCxLQUFLL0IsaUJBQXREO0FBQ0g7O0FBQ0QsU0FBS2dDLHFCQUFMLENBQTJCLEtBQUs3QyxLQUFMLENBQVdKLE9BQXRDO0FBQ0gsR0ExRm1ELENBNEZwRDs7O0FBQ0FrRCxFQUFBQSxnQ0FBZ0MsQ0FBQ0MsUUFBRCxFQUFXO0FBQUU7QUFDekMsUUFBSUEsUUFBUSxDQUFDbkQsT0FBVCxLQUFxQixLQUFLSSxLQUFMLENBQVdKLE9BQXBDLEVBQTZDO0FBQ3pDLFdBQUtpRCxxQkFBTCxDQUEyQixLQUFLN0MsS0FBTCxDQUFXSixPQUF0Qzs7QUFDQSxXQUFLNkMsZUFBTCxDQUFxQk0sUUFBUSxDQUFDbkQsT0FBOUI7QUFDSDtBQUNKOztBQUVENkMsRUFBQUEsZUFBZSxDQUFDN0MsT0FBRCxFQUFVO0FBQ3JCLFFBQUksQ0FBQ0EsT0FBTCxFQUFjOztBQUNkb0Qsd0JBQVdDLGdCQUFYLENBQTRCckQsT0FBNUIsRUFBcUMsS0FBS2tCLG1CQUExQztBQUNIOztBQUVEK0IsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEJHLHdCQUFXRSxrQkFBWCxDQUE4QixLQUFLcEMsbUJBQW5DO0FBQ0g7O0FBRURBLEVBQUFBLG1CQUFtQixHQUFHO0FBQ2xCLFNBQUtxQyxRQUFMLENBQWM7QUFDVi9DLE1BQUFBLHVCQUF1QixFQUFFNEMsb0JBQVdJLGdCQUFYLENBQTRCLEtBQUtwRCxLQUFMLENBQVdKLE9BQXZDO0FBRGYsS0FBZDtBQUdIOztBQUVEbUIsRUFBQUEsMEJBQTBCLEdBQUc7QUFDekIsbURBQXNCLEtBQUtmLEtBQUwsQ0FBV0osT0FBakMsRUFBMEN5RCxJQUExQyxDQUErQyxNQUFNO0FBQ2pELFdBQUtGLFFBQUwsQ0FBYztBQUNWakQsUUFBQUEsS0FBSyxFQUFFMkIsMENBQW1CQztBQURoQixPQUFkO0FBR0gsS0FKRDtBQUtIOztBQUVEZCxFQUFBQSwyQkFBMkIsR0FBRztBQUMxQixvREFBdUIsS0FBS2hCLEtBQUwsQ0FBV0osT0FBbEMsRUFBMkN5RCxJQUEzQyxDQUFnRCxNQUFNO0FBQ2xELFdBQUtsQyxXQUFMO0FBQ0gsS0FGRDtBQUdIOztBQUVETixFQUFBQSxpQkFBaUIsQ0FBQ3lDLEVBQUQsRUFBS3JELEtBQUwsRUFBWUksTUFBWixFQUFvQjtBQUNqQyxRQUFJQSxNQUFNLENBQUNaLE1BQVAsS0FBa0IsS0FBS08sS0FBTCxDQUFXUCxNQUFqQyxFQUF5QztBQUNyQztBQUNILEtBSGdDLENBSWpDOzs7QUFDQSxRQUFJLEtBQUtRLEtBQUwsQ0FBV0MsS0FBWCxLQUFxQjJCLDBDQUFtQkssY0FBeEMsSUFBMEQ3QixNQUFNLENBQUNaLE1BQVAsS0FBa0IsS0FBS08sS0FBTCxDQUFXUCxNQUEzRixFQUFtRztBQUMvRixXQUFLd0IsY0FBTDtBQUNILEtBRkQsTUFFTyxJQUFJLEtBQUtoQixLQUFMLENBQVdDLEtBQVgsS0FBcUIyQiwwQ0FBbUJJLGNBQXhDLElBQTBENUIsTUFBTSxDQUFDWixNQUFQLEtBQWtCLEtBQUtPLEtBQUwsQ0FBV1AsTUFBdkYsSUFDSFksTUFBTSxDQUFDMEIsTUFBUCxLQUFrQixLQUFLOUIsS0FBTCxDQUFXSSxNQUFYLENBQWtCMEIsTUFEckMsRUFDNkM7QUFDaEQ7QUFDQSxXQUFLZCxjQUFMO0FBQ0g7QUFDSjs7QUFFRE4sRUFBQUEsUUFBUSxDQUFDNEMsT0FBRCxFQUFVO0FBQ2QsUUFBSUEsT0FBTyxDQUFDM0IsTUFBUixLQUFtQixnQ0FBdkIsRUFBeUQ7QUFDckQsV0FBS3VCLFFBQUwsQ0FBYztBQUNWakQsUUFBQUEsS0FBSyxFQUFFcUQsT0FBTyxDQUFDckQsS0FETDtBQUVWc0QsUUFBQUEsV0FBVyxFQUFFRCxPQUFPLENBQUNDLFdBRlg7QUFHVjVELFFBQUFBLE9BQU8sRUFBRTJELE9BQU8sQ0FBQzNELE9BSFA7QUFJVlMsUUFBQUEsTUFBTSxFQUFFa0QsT0FBTyxDQUFDbEQsTUFKTjtBQUtWb0QsUUFBQUEsS0FBSyxFQUFFRixPQUFPLENBQUNFLEtBTEw7QUFNVmxELFFBQUFBLG1CQUFtQixFQUFFZ0QsT0FBTyxDQUFDaEQsbUJBTm5CO0FBT1ZtRCxRQUFBQSwwQkFBMEIsRUFBRUgsT0FBTyxDQUFDRztBQVAxQixPQUFkO0FBU0g7QUFDSjs7QUFFREMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQW5CO0FBQ0EsVUFBTUMsVUFBVSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQW5CO0FBQ0EsVUFBTUUsUUFBUSxHQUFHSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQWpCO0FBQ0EsVUFBTUcsb0JBQW9CLEdBQUdKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw0QkFBakIsQ0FBN0I7QUFDQSxVQUFNSSxpQkFBaUIsR0FBR0wsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUExQjtBQUNBLFVBQU1LLFNBQVMsR0FBR04sR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFsQjtBQUVBLFVBQU1oQyxlQUFlLEdBQUcrQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXhCO0FBQ0EsVUFBTU0sZUFBZSxHQUFHUCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXhCO0FBQ0EsVUFBTU8sYUFBYSxHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXRCO0FBQ0EsVUFBTVEsYUFBYSxHQUFHVCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXRCOztBQUVBLFFBQUlTLEtBQUssR0FBRyx5Q0FBWjs7QUFFQSxZQUFRLEtBQUt0RSxLQUFMLENBQVdDLEtBQW5CO0FBQ0ksV0FBSzJCLDBDQUFtQkssY0FBeEI7QUFDSSxZQUFJLEtBQUtsQyxLQUFMLENBQVdQLE1BQWYsRUFBdUI7QUFDbkI4RSxVQUFBQSxLQUFLLEdBQUcsNkJBQUMsVUFBRDtBQUFZLFlBQUEsTUFBTSxFQUFFLEtBQUt2RSxLQUFMLENBQVdQLE1BQS9CO0FBQXVDLFlBQUEsR0FBRyxFQUFFLEtBQUtPLEtBQUwsQ0FBV1A7QUFBdkQsWUFBUjtBQUNIOztBQUNEOztBQUNKLFdBQUtvQywwQ0FBbUJDLGVBQXhCO0FBQ0ksWUFBSSxLQUFLOUIsS0FBTCxDQUFXSixPQUFmLEVBQXdCO0FBQ3BCMkUsVUFBQUEsS0FBSyxHQUFHLDZCQUFDLGVBQUQ7QUFBaUIsWUFBQSxPQUFPLEVBQUUsS0FBS3ZFLEtBQUwsQ0FBV0osT0FBckM7QUFBOEMsWUFBQSxHQUFHLEVBQUUsS0FBS0ksS0FBTCxDQUFXSjtBQUE5RCxZQUFSO0FBQ0g7O0FBQ0Q7O0FBQ0osV0FBS2lDLDBDQUFtQndDLGFBQXhCO0FBQ0lFLFFBQUFBLEtBQUssR0FBRyw2QkFBQyxhQUFEO0FBQWUsVUFBQSxPQUFPLEVBQUUsS0FBS3ZFLEtBQUwsQ0FBV0osT0FBbkM7QUFBNEMsVUFBQSxHQUFHLEVBQUUsS0FBS0ksS0FBTCxDQUFXSjtBQUE1RCxVQUFSO0FBQ0E7O0FBQ0osV0FBS2lDLDBDQUFtQkksY0FBeEI7QUFDQSxXQUFLSiwwQ0FBbUIyQyxlQUF4QjtBQUNJLFlBQUlDLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6RCxnQkFBTUMsT0FBTyxHQUFHLE1BQU07QUFDbEJqRCxnQ0FBSUMsUUFBSixDQUFhO0FBQ1RDLGNBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVR2QixjQUFBQSxNQUFNLEVBQUUsS0FBS0osS0FBTCxDQUFXQyxLQUFYLEtBQXFCMkIsMENBQW1CMkMsZUFBeEMsR0FBMEQsS0FBS3ZFLEtBQUwsQ0FBV0ksTUFBckUsR0FBOEU7QUFGN0UsYUFBYjtBQUlILFdBTEQ7O0FBTUFrRSxVQUFBQSxLQUFLLEdBQUcsNkJBQUMsUUFBRDtBQUNKLFlBQUEsSUFBSSxFQUFFLEtBQUt0RSxLQUFMLENBQVdJLE1BRGI7QUFFSixZQUFBLE1BQU0sRUFBRSxLQUFLTCxLQUFMLENBQVdQLE1BRmY7QUFHSixZQUFBLEdBQUcsRUFBRSxLQUFLTyxLQUFMLENBQVdQLE1BQVgsSUFBcUIsS0FBS1EsS0FBTCxDQUFXSSxNQUFYLENBQWtCMEIsTUFIeEM7QUFJSixZQUFBLE9BQU8sRUFBRTRDLE9BSkw7QUFLSixZQUFBLEtBQUssRUFBRSxLQUFLMUUsS0FBTCxDQUFXQyxLQUxkO0FBTUosWUFBQSxtQkFBbUIsRUFBRSxLQUFLRCxLQUFMLENBQVdNLG1CQU41QjtBQU9KLFlBQUEsMEJBQTBCLEVBQUUsS0FBS04sS0FBTCxDQUFXeUQ7QUFQbkMsWUFBUjtBQVNILFNBaEJELE1BZ0JPO0FBQ0hhLFVBQUFBLEtBQUssR0FBRyw2QkFBQyxVQUFEO0FBQ0osWUFBQSxNQUFNLEVBQUUsS0FBS3RFLEtBQUwsQ0FBV0ksTUFEZjtBQUVKLFlBQUEsR0FBRyxFQUFFLEtBQUtMLEtBQUwsQ0FBV1AsTUFBWCxJQUFxQixLQUFLUSxLQUFMLENBQVdJLE1BQVgsQ0FBa0IwQjtBQUZ4QyxZQUFSO0FBSUg7O0FBQ0Q7O0FBQ0osV0FBS0YsMENBQW1CK0Msa0JBQXhCO0FBQ0lMLFFBQUFBLEtBQUssR0FBRyw2QkFBQyxvQkFBRDtBQUFzQixVQUFBLEtBQUssRUFBRSxLQUFLdEUsS0FBTCxDQUFXd0QsS0FBeEM7QUFBK0MsVUFBQSxHQUFHLEVBQUUsS0FBS3pELEtBQUwsQ0FBV1A7QUFBL0QsVUFBUjtBQUNBOztBQUNKLFdBQUtvQywwQ0FBbUJ1QyxlQUF4QjtBQUNJLFlBQUlLLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6RCxnQkFBTUMsT0FBTyxHQUFHLE1BQU07QUFDbEJqRCxnQ0FBSUMsUUFBSixDQUFhO0FBQ1RDLGNBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVR2QixjQUFBQSxNQUFNLEVBQUU7QUFGQyxhQUFiO0FBSUgsV0FMRDs7QUFNQWtFLFVBQUFBLEtBQUssR0FBRyw2QkFBQyxRQUFEO0FBQ0osWUFBQSxJQUFJLEVBQUUsS0FBS3RFLEtBQUwsQ0FBV0ksTUFEYjtBQUVKLFlBQUEsT0FBTyxFQUFFLEtBQUtMLEtBQUwsQ0FBV0osT0FGaEI7QUFHSixZQUFBLEdBQUcsRUFBRSxLQUFLSyxLQUFMLENBQVdJLE1BQVgsQ0FBa0IwQixNQUhuQjtBQUlKLFlBQUEsT0FBTyxFQUFFNEM7QUFKTCxZQUFSO0FBS0gsU0FaRCxNQVlPO0FBQ0hKLFVBQUFBLEtBQUssR0FDRCw2QkFBQyxlQUFEO0FBQ0ksWUFBQSxXQUFXLEVBQUUsS0FBS3RFLEtBQUwsQ0FBV0ksTUFENUI7QUFFSSxZQUFBLE9BQU8sRUFBRSxLQUFLTCxLQUFMLENBQVdKLE9BRnhCO0FBR0ksWUFBQSxHQUFHLEVBQUUsS0FBS0ssS0FBTCxDQUFXSSxNQUFYLENBQWtCd0U7QUFIM0IsWUFESjtBQU9IOztBQUNEOztBQUNKLFdBQUtoRCwwQ0FBbUJ5QyxhQUF4QjtBQUNJQyxRQUFBQSxLQUFLLEdBQUcsNkJBQUMsYUFBRDtBQUNKLFVBQUEsV0FBVyxFQUFFLEtBQUt0RSxLQUFMLENBQVd1RCxXQURwQjtBQUVKLFVBQUEsT0FBTyxFQUFFLEtBQUt4RCxLQUFMLENBQVdKLE9BRmhCO0FBR0osVUFBQSxHQUFHLEVBQUUsS0FBS0ssS0FBTCxDQUFXdUQ7QUFIWixVQUFSO0FBSUE7O0FBQ0osV0FBSzNCLDBDQUFtQnFDLGlCQUF4QjtBQUNJSyxRQUFBQSxLQUFLLEdBQUcsNkJBQUMsaUJBQUQsT0FBUjtBQUNBOztBQUNKLFdBQUsxQywwQ0FBbUJzQyxTQUF4QjtBQUNJSSxRQUFBQSxLQUFLLEdBQUcsNkJBQUMsU0FBRDtBQUFXLFVBQUEsTUFBTSxFQUFFLEtBQUt2RSxLQUFMLENBQVdQLE1BQTlCO0FBQXNDLFVBQUEsY0FBYyxFQUFFLEtBQUtPLEtBQUwsQ0FBVzhFO0FBQWpFLFVBQVI7QUFDQTtBQTVFUjs7QUErRUEsVUFBTUMsT0FBTyxHQUFHLHlCQUFXLGVBQVgsRUFBNEIsWUFBNUIsRUFBMEM7QUFDdEQsbUJBQWEsS0FBSy9FLEtBQUwsQ0FBV2dGLFNBRDhCO0FBRXRELDBCQUFvQixLQUFLaEYsS0FBTCxDQUFXaUYsUUFGdUI7QUFHdEQsb0JBQWM7QUFId0MsS0FBMUMsQ0FBaEI7QUFNQSxXQUNJO0FBQU8sTUFBQSxTQUFTLEVBQUVGLE9BQWxCO0FBQTJCLE1BQUEsRUFBRSxFQUFDO0FBQTlCLE9BQ01SLEtBRE4sQ0FESjtBQUtIOztBQXRRbUQ7Ozs4QkFBbkNsRixVLGlCQVNJNkYsNEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IFJhdGVMaW1pdGVkRnVuYyBmcm9tICcuLi8uLi9yYXRlbGltaXRlZGZ1bmMnO1xyXG5pbXBvcnQgeyBzaG93R3JvdXBJbnZpdGVEaWFsb2csIHNob3dHcm91cEFkZFJvb21EaWFsb2cgfSBmcm9tICcuLi8uLi9Hcm91cEFkZHJlc3NQaWNrZXInO1xyXG5pbXBvcnQgR3JvdXBTdG9yZSBmcm9tICcuLi8uLi9zdG9yZXMvR3JvdXBTdG9yZSc7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCB7UklHSFRfUEFORUxfUEhBU0VTLCBSSUdIVF9QQU5FTF9QSEFTRVNfTk9fQVJHU30gZnJvbSBcIi4uLy4uL3N0b3Jlcy9SaWdodFBhbmVsU3RvcmVQaGFzZXNcIjtcclxuaW1wb3J0IFJpZ2h0UGFuZWxTdG9yZSBmcm9tIFwiLi4vLi4vc3RvcmVzL1JpZ2h0UGFuZWxTdG9yZVwiO1xyXG5pbXBvcnQgTWF0cml4Q2xpZW50Q29udGV4dCBmcm9tIFwiLi4vLi4vY29udGV4dHMvTWF0cml4Q2xpZW50Q29udGV4dFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmlnaHRQYW5lbCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgZ2V0IHByb3BUeXBlcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByb29tSWQ6IFByb3BUeXBlcy5zdHJpbmcsIC8vIGlmIHNob3dpbmcgcGFuZWxzIGZvciBhIGdpdmVuIHJvb20sIHRoaXMgaXMgc2V0XHJcbiAgICAgICAgICAgIGdyb3VwSWQ6IFByb3BUeXBlcy5zdHJpbmcsIC8vIGlmIHNob3dpbmcgcGFuZWxzIGZvciBhIGdpdmVuIGdyb3VwLCB0aGlzIGlzIHNldFxyXG4gICAgICAgICAgICB1c2VyOiBQcm9wVHlwZXMub2JqZWN0LCAvLyB1c2VkIGlmIHdlIGtub3cgdGhlIHVzZXIgYWhlYWQgb2Ygb3BlbmluZyB0aGUgcGFuZWxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBjb250ZXh0VHlwZSA9IE1hdHJpeENsaWVudENvbnRleHQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcGhhc2U6IHRoaXMuX2dldFBoYXNlRnJvbVByb3BzKCksXHJcbiAgICAgICAgICAgIGlzVXNlclByaXZpbGVnZWRJbkdyb3VwOiBudWxsLFxyXG4gICAgICAgICAgICBtZW1iZXI6IHRoaXMuX2dldFVzZXJGb3JQYW5lbCgpLFxyXG4gICAgICAgICAgICB2ZXJpZmljYXRpb25SZXF1ZXN0OiBSaWdodFBhbmVsU3RvcmUuZ2V0U2hhcmVkSW5zdGFuY2UoKS5yb29tUGFuZWxQaGFzZVBhcmFtcy52ZXJpZmljYXRpb25SZXF1ZXN0LFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5vbkFjdGlvbiA9IHRoaXMub25BY3Rpb24uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyID0gdGhpcy5vblJvb21TdGF0ZU1lbWJlci5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25Hcm91cFN0b3JlVXBkYXRlZCA9IHRoaXMub25Hcm91cFN0b3JlVXBkYXRlZC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25JbnZpdGVUb0dyb3VwQnV0dG9uQ2xpY2sgPSB0aGlzLm9uSW52aXRlVG9Hcm91cEJ1dHRvbkNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vbkFkZFJvb21Ub0dyb3VwQnV0dG9uQ2xpY2sgPSB0aGlzLm9uQWRkUm9vbVRvR3JvdXBCdXR0b25DbGljay5iaW5kKHRoaXMpO1xyXG5cclxuICAgICAgICB0aGlzLl9kZWxheWVkVXBkYXRlID0gbmV3IFJhdGVMaW1pdGVkRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICB9LCA1MDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEhlbHBlciBmdW5jdGlvbiB0byBzcGxpdCBvdXQgdGhlIGxvZ2ljIGZvciBfZ2V0UGhhc2VGcm9tUHJvcHMoKSBhbmQgdGhlIGNvbnN0cnVjdG9yXHJcbiAgICAvLyBhcyBib3RoIGFyZSBjYWxsZWQgYXQgdGhlIHNhbWUgdGltZSBpbiB0aGUgY29uc3RydWN0b3IuXHJcbiAgICBfZ2V0VXNlckZvclBhbmVsKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlICYmIHRoaXMuc3RhdGUubWVtYmVyKSByZXR1cm4gdGhpcy5zdGF0ZS5tZW1iZXI7XHJcbiAgICAgICAgY29uc3QgbGFzdFBhcmFtcyA9IFJpZ2h0UGFuZWxTdG9yZS5nZXRTaGFyZWRJbnN0YW5jZSgpLnJvb21QYW5lbFBoYXNlUGFyYW1zO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLnVzZXIgfHwgbGFzdFBhcmFtc1snbWVtYmVyJ107XHJcbiAgICB9XHJcblxyXG4gICAgLy8gZ2V0cyB0aGUgY3VycmVudCBwaGFzZSBmcm9tIHRoZSBwcm9wcyBhbmQgYWxzbyBtYXliZSB0aGUgc3RvcmVcclxuICAgIF9nZXRQaGFzZUZyb21Qcm9wcygpIHtcclxuICAgICAgICBjb25zdCBycHMgPSBSaWdodFBhbmVsU3RvcmUuZ2V0U2hhcmVkSW5zdGFuY2UoKTtcclxuICAgICAgICBjb25zdCB1c2VyRm9yUGFuZWwgPSB0aGlzLl9nZXRVc2VyRm9yUGFuZWwoKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5ncm91cElkKSB7XHJcbiAgICAgICAgICAgIGlmICghUklHSFRfUEFORUxfUEhBU0VTX05PX0FSR1MuaW5jbHVkZXMocnBzLmdyb3VwUGFuZWxQaGFzZSkpIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiBcInNldF9yaWdodF9wYW5lbF9waGFzZVwiLCBwaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVyTGlzdH0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFJJR0hUX1BBTkVMX1BIQVNFUy5Hcm91cE1lbWJlckxpc3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJwcy5ncm91cFBhbmVsUGhhc2U7XHJcbiAgICAgICAgfSBlbHNlIGlmICh1c2VyRm9yUGFuZWwpIHtcclxuICAgICAgICAgICAgLy8gWFhYIEZJWE1FIEFBQUFBQVJHSDogV2hhdCBpcyBnb2luZyBvbiB3aXRoIHRoaXMgY2xhc3MhPyBJdCB0YWtlcyBzb21lIG9mIGl0cyBzdGF0ZVxyXG4gICAgICAgICAgICAvLyBmcm9tIGl0cyBwcm9wcyBhbmQgc29tZSBmcm9tIGEgc3RvcmUsIGV4Y2VwdCBpZiB0aGUgY29udGVudHMgb2YgdGhlIHN0b3JlIGNoYW5nZXNcclxuICAgICAgICAgICAgLy8gd2hpbGUgaXQncyBtb3VudGVkIGluIHdoaWNoIGNhc2UgaXQgcmVwbGFjZXMgYWxsIG9mIGl0cyBzdGF0ZSB3aXRoIHRoYXQgb2YgdGhlIHN0b3JlLFxyXG4gICAgICAgICAgICAvLyBleGNlcHQgaXQgdXNlcyBhIGRpc3BhdGNoIGluc3RlYWQgb2YgYSBub3JtYWwgc3RvcmUgbGlzdGVuZXI/XHJcbiAgICAgICAgICAgIC8vIFVuZm9ydHVuYXRlbHkgcmV3cml0aW5nIHRoaXMgd291bGQgYWxtb3N0IGNlcnRhaW5seSBicmVhayBzaG93aW5nIHRoZSByaWdodCBwYW5lbFxyXG4gICAgICAgICAgICAvLyBpbiBzb21lIG9mIHRoZSBtYW55IGNhc2VzLCBhbmQgSSBkb24ndCBoYXZlIHRpbWUgdG8gcmUtYXJjaGl0ZWN0IGl0IGFuZCB0ZXN0IGFsbFxyXG4gICAgICAgICAgICAvLyB0aGUgZmxvd3Mgbm93LCBzbyBhZGRpbmcgeWV0IGFub3RoZXIgc3BlY2lhbCBjYXNlIHNvIGlmIHRoZSBzdG9yZSB0aGlua3MgdGhlcmUgaXNcclxuICAgICAgICAgICAgLy8gYSB2ZXJpZmljYXRpb24gZ29pbmcgb24gZm9yIHRoZSBtZW1iZXIgd2UncmUgZGlzcGxheWluZywgd2Ugc2hvdyB0aGF0LCBvdGhlcndpc2VcclxuICAgICAgICAgICAgLy8gd2UgcmFjZSBpZiBhIHZlcmlmaWNhdGlvbiBpcyBzdGFydGVkIHdoaWxlIHRoZSBwYW5lbCBpc24ndCBkaXNwbGF5ZWQgYmVjYXVzZSB3ZSdyZVxyXG4gICAgICAgICAgICAvLyBub3QgbW91bnRlZCBpbiB0aW1lIHRvIGdldCB0aGUgZGlzcGF0Y2guXHJcbiAgICAgICAgICAgIC8vIFVudGlsIHRoZW4sIGxldCB0aGlzIGNvZGUgc2VydmUgYXMgYSB3YXJuaW5nIGZyb20gaGlzdG9yeS5cclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgcnBzLnJvb21QYW5lbFBoYXNlUGFyYW1zLm1lbWJlciAmJlxyXG4gICAgICAgICAgICAgICAgdXNlckZvclBhbmVsLnVzZXJJZCA9PT0gcnBzLnJvb21QYW5lbFBoYXNlUGFyYW1zLm1lbWJlci51c2VySWQgJiZcclxuICAgICAgICAgICAgICAgIHJwcy5yb29tUGFuZWxQaGFzZVBhcmFtcy52ZXJpZmljYXRpb25SZXF1ZXN0XHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJwcy5yb29tUGFuZWxQaGFzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gUklHSFRfUEFORUxfUEhBU0VTLlJvb21NZW1iZXJJbmZvO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICghUklHSFRfUEFORUxfUEhBU0VTX05PX0FSR1MuaW5jbHVkZXMocnBzLnJvb21QYW5lbFBoYXNlKSkge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246IFwic2V0X3JpZ2h0X3BhbmVsX3BoYXNlXCIsIHBoYXNlOiBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckxpc3R9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckxpc3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJwcy5yb29tUGFuZWxQaGFzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IHRoaXMuY29udGV4dDtcclxuICAgICAgICBjbGkub24oXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyKTtcclxuICAgICAgICB0aGlzLl9pbml0R3JvdXBTdG9yZSh0aGlzLnByb3BzLmdyb3VwSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGV4dCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdW5yZWdpc3Rlckdyb3VwU3RvcmUodGhpcy5wcm9wcy5ncm91cElkKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSB3aXRoIGFwcHJvcHJpYXRlIGxpZmVjeWNsZSBldmVudFxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV3UHJvcHMpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjYW1lbGNhc2VcclxuICAgICAgICBpZiAobmV3UHJvcHMuZ3JvdXBJZCAhPT0gdGhpcy5wcm9wcy5ncm91cElkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VucmVnaXN0ZXJHcm91cFN0b3JlKHRoaXMucHJvcHMuZ3JvdXBJZCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2luaXRHcm91cFN0b3JlKG5ld1Byb3BzLmdyb3VwSWQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfaW5pdEdyb3VwU3RvcmUoZ3JvdXBJZCkge1xyXG4gICAgICAgIGlmICghZ3JvdXBJZCkgcmV0dXJuO1xyXG4gICAgICAgIEdyb3VwU3RvcmUucmVnaXN0ZXJMaXN0ZW5lcihncm91cElkLCB0aGlzLm9uR3JvdXBTdG9yZVVwZGF0ZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIF91bnJlZ2lzdGVyR3JvdXBTdG9yZSgpIHtcclxuICAgICAgICBHcm91cFN0b3JlLnVucmVnaXN0ZXJMaXN0ZW5lcih0aGlzLm9uR3JvdXBTdG9yZVVwZGF0ZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uR3JvdXBTdG9yZVVwZGF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGlzVXNlclByaXZpbGVnZWRJbkdyb3VwOiBHcm91cFN0b3JlLmlzVXNlclByaXZpbGVnZWQodGhpcy5wcm9wcy5ncm91cElkKSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbkludml0ZVRvR3JvdXBCdXR0b25DbGljaygpIHtcclxuICAgICAgICBzaG93R3JvdXBJbnZpdGVEaWFsb2codGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBwaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVyTGlzdCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25BZGRSb29tVG9Hcm91cEJ1dHRvbkNsaWNrKCkge1xyXG4gICAgICAgIHNob3dHcm91cEFkZFJvb21EaWFsb2codGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uUm9vbVN0YXRlTWVtYmVyKGV2LCBzdGF0ZSwgbWVtYmVyKSB7XHJcbiAgICAgICAgaWYgKG1lbWJlci5yb29tSWQgIT09IHRoaXMucHJvcHMucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcmVkcmF3IHRoZSBiYWRnZSBvbiB0aGUgbWVtYmVyc2hpcCBsaXN0XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucGhhc2UgPT09IFJJR0hUX1BBTkVMX1BIQVNFUy5Sb29tTWVtYmVyTGlzdCAmJiBtZW1iZXIucm9vbUlkID09PSB0aGlzLnByb3BzLnJvb21JZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9kZWxheWVkVXBkYXRlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnBoYXNlID09PSBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckluZm8gJiYgbWVtYmVyLnJvb21JZCA9PT0gdGhpcy5wcm9wcy5yb29tSWQgJiZcclxuICAgICAgICAgICAgICAgIG1lbWJlci51c2VySWQgPT09IHRoaXMuc3RhdGUubWVtYmVyLnVzZXJJZCkge1xyXG4gICAgICAgICAgICAvLyByZWZyZXNoIHRoZSBtZW1iZXIgaW5mbyAoZS5nLiBuZXcgcG93ZXIgbGV2ZWwpXHJcbiAgICAgICAgICAgIHRoaXMuX2RlbGF5ZWRVcGRhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25BY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIGlmIChwYXlsb2FkLmFjdGlvbiA9PT0gXCJhZnRlcl9yaWdodF9wYW5lbF9waGFzZV9jaGFuZ2VcIikge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHBoYXNlOiBwYXlsb2FkLnBoYXNlLFxyXG4gICAgICAgICAgICAgICAgZ3JvdXBSb29tSWQ6IHBheWxvYWQuZ3JvdXBSb29tSWQsXHJcbiAgICAgICAgICAgICAgICBncm91cElkOiBwYXlsb2FkLmdyb3VwSWQsXHJcbiAgICAgICAgICAgICAgICBtZW1iZXI6IHBheWxvYWQubWVtYmVyLFxyXG4gICAgICAgICAgICAgICAgZXZlbnQ6IHBheWxvYWQuZXZlbnQsXHJcbiAgICAgICAgICAgICAgICB2ZXJpZmljYXRpb25SZXF1ZXN0OiBwYXlsb2FkLnZlcmlmaWNhdGlvblJlcXVlc3QsXHJcbiAgICAgICAgICAgICAgICB2ZXJpZmljYXRpb25SZXF1ZXN0UHJvbWlzZTogcGF5bG9hZC52ZXJpZmljYXRpb25SZXF1ZXN0UHJvbWlzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBNZW1iZXJMaXN0ID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuTWVtYmVyTGlzdCcpO1xyXG4gICAgICAgIGNvbnN0IE1lbWJlckluZm8gPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5NZW1iZXJJbmZvJyk7XHJcbiAgICAgICAgY29uc3QgVXNlckluZm8gPSBzZGsuZ2V0Q29tcG9uZW50KCdyaWdodF9wYW5lbC5Vc2VySW5mbycpO1xyXG4gICAgICAgIGNvbnN0IFRoaXJkUGFydHlNZW1iZXJJbmZvID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuVGhpcmRQYXJ0eU1lbWJlckluZm8nKTtcclxuICAgICAgICBjb25zdCBOb3RpZmljYXRpb25QYW5lbCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuTm90aWZpY2F0aW9uUGFuZWwnKTtcclxuICAgICAgICBjb25zdCBGaWxlUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLkZpbGVQYW5lbCcpO1xyXG5cclxuICAgICAgICBjb25zdCBHcm91cE1lbWJlckxpc3QgPSBzZGsuZ2V0Q29tcG9uZW50KCdncm91cHMuR3JvdXBNZW1iZXJMaXN0Jyk7XHJcbiAgICAgICAgY29uc3QgR3JvdXBNZW1iZXJJbmZvID0gc2RrLmdldENvbXBvbmVudCgnZ3JvdXBzLkdyb3VwTWVtYmVySW5mbycpO1xyXG4gICAgICAgIGNvbnN0IEdyb3VwUm9vbUxpc3QgPSBzZGsuZ2V0Q29tcG9uZW50KCdncm91cHMuR3JvdXBSb29tTGlzdCcpO1xyXG4gICAgICAgIGNvbnN0IEdyb3VwUm9vbUluZm8gPSBzZGsuZ2V0Q29tcG9uZW50KCdncm91cHMuR3JvdXBSb29tSW5mbycpO1xyXG5cclxuICAgICAgICBsZXQgcGFuZWwgPSA8ZGl2IC8+O1xyXG5cclxuICAgICAgICBzd2l0Y2ggKHRoaXMuc3RhdGUucGhhc2UpIHtcclxuICAgICAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckxpc3Q6XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5yb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBwYW5lbCA9IDxNZW1iZXJMaXN0IHJvb21JZD17dGhpcy5wcm9wcy5yb29tSWR9IGtleT17dGhpcy5wcm9wcy5yb29tSWR9IC8+O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVyTGlzdDpcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmdyb3VwSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBwYW5lbCA9IDxHcm91cE1lbWJlckxpc3QgZ3JvdXBJZD17dGhpcy5wcm9wcy5ncm91cElkfSBrZXk9e3RoaXMucHJvcHMuZ3JvdXBJZH0gLz47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuR3JvdXBSb29tTGlzdDpcclxuICAgICAgICAgICAgICAgIHBhbmVsID0gPEdyb3VwUm9vbUxpc3QgZ3JvdXBJZD17dGhpcy5wcm9wcy5ncm91cElkfSBrZXk9e3RoaXMucHJvcHMuZ3JvdXBJZH0gLz47XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckluZm86XHJcbiAgICAgICAgICAgIGNhc2UgUklHSFRfUEFORUxfUEhBU0VTLkVuY3J5cHRpb25QYW5lbDpcclxuICAgICAgICAgICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBvbkNsb3NlID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiBcInZpZXdfdXNlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyOiB0aGlzLnN0YXRlLnBoYXNlID09PSBSSUdIVF9QQU5FTF9QSEFTRVMuRW5jcnlwdGlvblBhbmVsID8gdGhpcy5zdGF0ZS5tZW1iZXIgOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHBhbmVsID0gPFVzZXJJbmZvXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXI9e3RoaXMuc3RhdGUubWVtYmVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tSWQ9e3RoaXMucHJvcHMucm9vbUlkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk9e3RoaXMucHJvcHMucm9vbUlkIHx8IHRoaXMuc3RhdGUubWVtYmVyLnVzZXJJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbG9zZT17b25DbG9zZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcGhhc2U9e3RoaXMuc3RhdGUucGhhc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZlcmlmaWNhdGlvblJlcXVlc3Q9e3RoaXMuc3RhdGUudmVyaWZpY2F0aW9uUmVxdWVzdH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVyaWZpY2F0aW9uUmVxdWVzdFByb21pc2U9e3RoaXMuc3RhdGUudmVyaWZpY2F0aW9uUmVxdWVzdFByb21pc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgLz47XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhbmVsID0gPE1lbWJlckluZm9cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyPXt0aGlzLnN0YXRlLm1lbWJlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PXt0aGlzLnByb3BzLnJvb21JZCB8fCB0aGlzLnN0YXRlLm1lbWJlci51c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgLz47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbTNwaWRNZW1iZXJJbmZvOlxyXG4gICAgICAgICAgICAgICAgcGFuZWwgPSA8VGhpcmRQYXJ0eU1lbWJlckluZm8gZXZlbnQ9e3RoaXMuc3RhdGUuZXZlbnR9IGtleT17dGhpcy5wcm9wcy5yb29tSWR9IC8+O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVySW5mbzpcclxuICAgICAgICAgICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBvbkNsb3NlID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiBcInZpZXdfdXNlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHBhbmVsID0gPFVzZXJJbmZvXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXI9e3RoaXMuc3RhdGUubWVtYmVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBncm91cElkPXt0aGlzLnByb3BzLmdyb3VwSWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17dGhpcy5zdGF0ZS5tZW1iZXIudXNlcklkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsb3NlPXtvbkNsb3NlfSAvPjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFuZWwgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxHcm91cE1lbWJlckluZm9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdyb3VwTWVtYmVyPXt0aGlzLnN0YXRlLm1lbWJlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17dGhpcy5zdGF0ZS5tZW1iZXIudXNlcl9pZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwUm9vbUluZm86XHJcbiAgICAgICAgICAgICAgICBwYW5lbCA9IDxHcm91cFJvb21JbmZvXHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBSb29tSWQ9e3RoaXMuc3RhdGUuZ3JvdXBSb29tSWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBJZD17dGhpcy5wcm9wcy5ncm91cElkfVxyXG4gICAgICAgICAgICAgICAgICAgIGtleT17dGhpcy5zdGF0ZS5ncm91cFJvb21JZH0gLz47XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuTm90aWZpY2F0aW9uUGFuZWw6XHJcbiAgICAgICAgICAgICAgICBwYW5lbCA9IDxOb3RpZmljYXRpb25QYW5lbCAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFJJR0hUX1BBTkVMX1BIQVNFUy5GaWxlUGFuZWw6XHJcbiAgICAgICAgICAgICAgICBwYW5lbCA9IDxGaWxlUGFuZWwgcm9vbUlkPXt0aGlzLnByb3BzLnJvb21JZH0gcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9IC8+O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyhcIm14X1JpZ2h0UGFuZWxcIiwgXCJteF9mYWRhYmxlXCIsIHtcclxuICAgICAgICAgICAgXCJjb2xsYXBzZWRcIjogdGhpcy5wcm9wcy5jb2xsYXBzZWQsXHJcbiAgICAgICAgICAgIFwibXhfZmFkYWJsZV9mYWRlZFwiOiB0aGlzLnByb3BzLmRpc2FibGVkLFxyXG4gICAgICAgICAgICBcImRhcmstcGFuZWxcIjogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGFzaWRlIGNsYXNzTmFtZT17Y2xhc3Nlc30gaWQ9XCJteF9SaWdodFBhbmVsXCI+XHJcbiAgICAgICAgICAgICAgICB7IHBhbmVsIH1cclxuICAgICAgICAgICAgPC9hc2lkZT5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==