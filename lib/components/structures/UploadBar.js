"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _ContentMessages = _interopRequireDefault(require("../../ContentMessages"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _filesize = _interopRequireDefault(require("filesize"));

var _languageHandler = require("../../languageHandler");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'UploadBar',
  propTypes: {
    room: _propTypes.default.object
  },
  componentDidMount: function () {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    this.mounted = true;
  },
  componentWillUnmount: function () {
    this.mounted = false;

    _dispatcher.default.unregister(this.dispatcherRef);
  },
  onAction: function (payload) {
    switch (payload.action) {
      case 'upload_progress':
      case 'upload_finished':
      case 'upload_canceled':
      case 'upload_failed':
        if (this.mounted) this.forceUpdate();
        break;
    }
  },
  render: function () {
    const uploads = _ContentMessages.default.sharedInstance().getCurrentUploads(); // for testing UI... - also fix up the ContentMessages.getCurrentUploads().length
    // check in RoomView
    //
    // uploads = [{
    //     roomId: this.props.room.roomId,
    //     loaded: 123493,
    //     total: 347534,
    //     fileName: "testing_fooble.jpg",
    // }];


    if (uploads.length == 0) {
      return _react.default.createElement("div", null);
    }

    let upload;

    for (let i = 0; i < uploads.length; ++i) {
      if (uploads[i].roomId == this.props.room.roomId) {
        upload = uploads[i];
        break;
      }
    }

    if (!upload) {
      return _react.default.createElement("div", null);
    }

    const innerProgressStyle = {
      width: upload.loaded / (upload.total || 1) * 100 + '%'
    };
    let uploadedSize = (0, _filesize.default)(upload.loaded);
    const totalSize = (0, _filesize.default)(upload.total);

    if (uploadedSize.replace(/^.* /, '') === totalSize.replace(/^.* /, '')) {
      uploadedSize = uploadedSize.replace(/ .*/, '');
    } // MUST use var name 'count' for pluralization to kick in


    const uploadText = (0, _languageHandler._t)("Uploading %(filename)s and %(count)s others", {
      filename: upload.fileName,
      count: uploads.length - 1
    });
    return _react.default.createElement("div", {
      className: "mx_UploadBar"
    }, _react.default.createElement("div", {
      className: "mx_UploadBar_uploadProgressOuter"
    }, _react.default.createElement("div", {
      className: "mx_UploadBar_uploadProgressInner",
      style: innerProgressStyle
    })), _react.default.createElement("img", {
      className: "mx_UploadBar_uploadIcon mx_filterFlipColor",
      src: require("../../../res/img/fileicon.png"),
      width: "17",
      height: "22"
    }), _react.default.createElement("img", {
      className: "mx_UploadBar_uploadCancel mx_filterFlipColor",
      src: require("../../../res/img/cancel.svg"),
      width: "18",
      height: "18",
      onClick: function () {
        _ContentMessages.default.sharedInstance().cancelUpload(upload.promise);
      }
    }), _react.default.createElement("div", {
      className: "mx_UploadBar_uploadBytes"
    }, uploadedSize, " / ", totalSize), _react.default.createElement("div", {
      className: "mx_UploadBar_uploadFilename"
    }, uploadText));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvVXBsb2FkQmFyLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwicm9vbSIsIlByb3BUeXBlcyIsIm9iamVjdCIsImNvbXBvbmVudERpZE1vdW50IiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJtb3VudGVkIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bnJlZ2lzdGVyIiwicGF5bG9hZCIsImFjdGlvbiIsImZvcmNlVXBkYXRlIiwicmVuZGVyIiwidXBsb2FkcyIsIkNvbnRlbnRNZXNzYWdlcyIsInNoYXJlZEluc3RhbmNlIiwiZ2V0Q3VycmVudFVwbG9hZHMiLCJsZW5ndGgiLCJ1cGxvYWQiLCJpIiwicm9vbUlkIiwicHJvcHMiLCJpbm5lclByb2dyZXNzU3R5bGUiLCJ3aWR0aCIsImxvYWRlZCIsInRvdGFsIiwidXBsb2FkZWRTaXplIiwidG90YWxTaXplIiwicmVwbGFjZSIsInVwbG9hZFRleHQiLCJmaWxlbmFtZSIsImZpbGVOYW1lIiwiY291bnQiLCJyZXF1aXJlIiwiY2FuY2VsVXBsb2FkIiwicHJvbWlzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7OztlQXlCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxXQURlO0FBRTVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsSUFBSSxFQUFFQyxtQkFBVUM7QUFEVCxHQUZpQjtBQU01QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLQyxhQUFMLEdBQXFCQyxvQkFBSUMsUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLElBQWY7QUFDSCxHQVQyQjtBQVc1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QixTQUFLRCxPQUFMLEdBQWUsS0FBZjs7QUFDQUgsd0JBQUlLLFVBQUosQ0FBZSxLQUFLTixhQUFwQjtBQUNILEdBZDJCO0FBZ0I1QkcsRUFBQUEsUUFBUSxFQUFFLFVBQVNJLE9BQVQsRUFBa0I7QUFDeEIsWUFBUUEsT0FBTyxDQUFDQyxNQUFoQjtBQUNJLFdBQUssaUJBQUw7QUFDQSxXQUFLLGlCQUFMO0FBQ0EsV0FBSyxpQkFBTDtBQUNBLFdBQUssZUFBTDtBQUNJLFlBQUksS0FBS0osT0FBVCxFQUFrQixLQUFLSyxXQUFMO0FBQ2xCO0FBTlI7QUFRSCxHQXpCMkI7QUEyQjVCQyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLE9BQU8sR0FBR0MseUJBQWdCQyxjQUFoQixHQUFpQ0MsaUJBQWpDLEVBQWhCLENBRGUsQ0FHZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLFFBQUlILE9BQU8sQ0FBQ0ksTUFBUixJQUFrQixDQUF0QixFQUF5QjtBQUNyQixhQUFPLHlDQUFQO0FBQ0g7O0FBRUQsUUFBSUMsTUFBSjs7QUFDQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdOLE9BQU8sQ0FBQ0ksTUFBNUIsRUFBb0MsRUFBRUUsQ0FBdEMsRUFBeUM7QUFDckMsVUFBSU4sT0FBTyxDQUFDTSxDQUFELENBQVAsQ0FBV0MsTUFBWCxJQUFxQixLQUFLQyxLQUFMLENBQVd2QixJQUFYLENBQWdCc0IsTUFBekMsRUFBaUQ7QUFDN0NGLFFBQUFBLE1BQU0sR0FBR0wsT0FBTyxDQUFDTSxDQUFELENBQWhCO0FBQ0E7QUFDSDtBQUNKOztBQUNELFFBQUksQ0FBQ0QsTUFBTCxFQUFhO0FBQ1QsYUFBTyx5Q0FBUDtBQUNIOztBQUVELFVBQU1JLGtCQUFrQixHQUFHO0FBQ3ZCQyxNQUFBQSxLQUFLLEVBQUlMLE1BQU0sQ0FBQ00sTUFBUCxJQUFpQk4sTUFBTSxDQUFDTyxLQUFQLElBQWdCLENBQWpDLENBQUQsR0FBd0MsR0FBekMsR0FBZ0Q7QUFEaEMsS0FBM0I7QUFHQSxRQUFJQyxZQUFZLEdBQUcsdUJBQVNSLE1BQU0sQ0FBQ00sTUFBaEIsQ0FBbkI7QUFDQSxVQUFNRyxTQUFTLEdBQUcsdUJBQVNULE1BQU0sQ0FBQ08sS0FBaEIsQ0FBbEI7O0FBQ0EsUUFBSUMsWUFBWSxDQUFDRSxPQUFiLENBQXFCLE1BQXJCLEVBQTZCLEVBQTdCLE1BQXFDRCxTQUFTLENBQUNDLE9BQVYsQ0FBa0IsTUFBbEIsRUFBMEIsRUFBMUIsQ0FBekMsRUFBd0U7QUFDcEVGLE1BQUFBLFlBQVksR0FBR0EsWUFBWSxDQUFDRSxPQUFiLENBQXFCLEtBQXJCLEVBQTRCLEVBQTVCLENBQWY7QUFDSCxLQW5DYyxDQXFDZjs7O0FBQ0EsVUFBTUMsVUFBVSxHQUFHLHlCQUFHLDZDQUFILEVBQWtEO0FBQUNDLE1BQUFBLFFBQVEsRUFBRVosTUFBTSxDQUFDYSxRQUFsQjtBQUE0QkMsTUFBQUEsS0FBSyxFQUFHbkIsT0FBTyxDQUFDSSxNQUFSLEdBQWlCO0FBQXJELEtBQWxELENBQW5CO0FBRUEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLGtDQUFmO0FBQWtELE1BQUEsS0FBSyxFQUFFSztBQUF6RCxNQURKLENBREosRUFJSTtBQUFLLE1BQUEsU0FBUyxFQUFDLDRDQUFmO0FBQTRELE1BQUEsR0FBRyxFQUFFVyxPQUFPLENBQUMsK0JBQUQsQ0FBeEU7QUFBMkcsTUFBQSxLQUFLLEVBQUMsSUFBakg7QUFBc0gsTUFBQSxNQUFNLEVBQUM7QUFBN0gsTUFKSixFQUtJO0FBQUssTUFBQSxTQUFTLEVBQUMsOENBQWY7QUFBOEQsTUFBQSxHQUFHLEVBQUVBLE9BQU8sQ0FBQyw2QkFBRCxDQUExRTtBQUEyRyxNQUFBLEtBQUssRUFBQyxJQUFqSDtBQUFzSCxNQUFBLE1BQU0sRUFBQyxJQUE3SDtBQUNJLE1BQUEsT0FBTyxFQUFFLFlBQVc7QUFBRW5CLGlDQUFnQkMsY0FBaEIsR0FBaUNtQixZQUFqQyxDQUE4Q2hCLE1BQU0sQ0FBQ2lCLE9BQXJEO0FBQWdFO0FBRDFGLE1BTEosRUFRSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTVQsWUFETixTQUN5QkMsU0FEekIsQ0FSSixFQVdJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUErQ0UsVUFBL0MsQ0FYSixDQURKO0FBZUg7QUFsRjJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBDb250ZW50TWVzc2FnZXMgZnJvbSAnLi4vLi4vQ29udGVudE1lc3NhZ2VzJztcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQgZmlsZXNpemUgZnJvbSBcImZpbGVzaXplXCI7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdVcGxvYWRCYXInLFxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLm9uQWN0aW9uKTtcclxuICAgICAgICB0aGlzLm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgZGlzLnVucmVnaXN0ZXIodGhpcy5kaXNwYXRjaGVyUmVmKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BY3Rpb246IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3VwbG9hZF9wcm9ncmVzcyc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3VwbG9hZF9maW5pc2hlZCc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3VwbG9hZF9jYW5jZWxlZCc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3VwbG9hZF9mYWlsZWQnOlxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubW91bnRlZCkgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHVwbG9hZHMgPSBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5nZXRDdXJyZW50VXBsb2FkcygpO1xyXG5cclxuICAgICAgICAvLyBmb3IgdGVzdGluZyBVSS4uLiAtIGFsc28gZml4IHVwIHRoZSBDb250ZW50TWVzc2FnZXMuZ2V0Q3VycmVudFVwbG9hZHMoKS5sZW5ndGhcclxuICAgICAgICAvLyBjaGVjayBpbiBSb29tVmlld1xyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gdXBsb2FkcyA9IFt7XHJcbiAgICAgICAgLy8gICAgIHJvb21JZDogdGhpcy5wcm9wcy5yb29tLnJvb21JZCxcclxuICAgICAgICAvLyAgICAgbG9hZGVkOiAxMjM0OTMsXHJcbiAgICAgICAgLy8gICAgIHRvdGFsOiAzNDc1MzQsXHJcbiAgICAgICAgLy8gICAgIGZpbGVOYW1lOiBcInRlc3RpbmdfZm9vYmxlLmpwZ1wiLFxyXG4gICAgICAgIC8vIH1dO1xyXG5cclxuICAgICAgICBpZiAodXBsb2Fkcy5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdiAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB1cGxvYWQ7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB1cGxvYWRzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGlmICh1cGxvYWRzW2ldLnJvb21JZCA9PSB0aGlzLnByb3BzLnJvb20ucm9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWQgPSB1cGxvYWRzW2ldO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF1cGxvYWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBpbm5lclByb2dyZXNzU3R5bGUgPSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAoKHVwbG9hZC5sb2FkZWQgLyAodXBsb2FkLnRvdGFsIHx8IDEpKSAqIDEwMCkgKyAnJScsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBsZXQgdXBsb2FkZWRTaXplID0gZmlsZXNpemUodXBsb2FkLmxvYWRlZCk7XHJcbiAgICAgICAgY29uc3QgdG90YWxTaXplID0gZmlsZXNpemUodXBsb2FkLnRvdGFsKTtcclxuICAgICAgICBpZiAodXBsb2FkZWRTaXplLnJlcGxhY2UoL14uKiAvLCAnJykgPT09IHRvdGFsU2l6ZS5yZXBsYWNlKC9eLiogLywgJycpKSB7XHJcbiAgICAgICAgICAgIHVwbG9hZGVkU2l6ZSA9IHVwbG9hZGVkU2l6ZS5yZXBsYWNlKC8gLiovLCAnJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBNVVNUIHVzZSB2YXIgbmFtZSAnY291bnQnIGZvciBwbHVyYWxpemF0aW9uIHRvIGtpY2sgaW5cclxuICAgICAgICBjb25zdCB1cGxvYWRUZXh0ID0gX3QoXCJVcGxvYWRpbmcgJShmaWxlbmFtZSlzIGFuZCAlKGNvdW50KXMgb3RoZXJzXCIsIHtmaWxlbmFtZTogdXBsb2FkLmZpbGVOYW1lLCBjb3VudDogKHVwbG9hZHMubGVuZ3RoIC0gMSl9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9VcGxvYWRCYXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXBsb2FkQmFyX3VwbG9hZFByb2dyZXNzT3V0ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VwbG9hZEJhcl91cGxvYWRQcm9ncmVzc0lubmVyXCIgc3R5bGU9e2lubmVyUHJvZ3Jlc3NTdHlsZX0+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwibXhfVXBsb2FkQmFyX3VwbG9hZEljb24gbXhfZmlsdGVyRmxpcENvbG9yXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9maWxlaWNvbi5wbmdcIil9IHdpZHRoPVwiMTdcIiBoZWlnaHQ9XCIyMlwiIC8+XHJcbiAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X1VwbG9hZEJhcl91cGxvYWRDYW5jZWwgbXhfZmlsdGVyRmxpcENvbG9yXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwuc3ZnXCIpfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2Z1bmN0aW9uKCkgeyBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5jYW5jZWxVcGxvYWQodXBsb2FkLnByb21pc2UpOyB9fVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXBsb2FkQmFyX3VwbG9hZEJ5dGVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB1cGxvYWRlZFNpemUgfSAvIHsgdG90YWxTaXplIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9VcGxvYWRCYXJfdXBsb2FkRmlsZW5hbWVcIj57IHVwbG9hZFRleHQgfTwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==