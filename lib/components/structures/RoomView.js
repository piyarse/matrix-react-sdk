"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _shouldHideEvent = _interopRequireDefault(require("../../shouldHideEvent"));

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _languageHandler = require("../../languageHandler");

var _Permalinks = require("../../utils/permalinks/Permalinks");

var _ContentMessages = _interopRequireDefault(require("../../ContentMessages"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var sdk = _interopRequireWildcard(require("../../index"));

var _CallHandler = _interopRequireDefault(require("../../CallHandler"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _Tinter = _interopRequireDefault(require("../../Tinter"));

var _ratelimitedfunc = _interopRequireDefault(require("../../ratelimitedfunc"));

var ObjectUtils = _interopRequireWildcard(require("../../ObjectUtils"));

var Rooms = _interopRequireWildcard(require("../../Rooms"));

var _Searching = _interopRequireDefault(require("../../Searching"));

var _Keyboard = require("../../Keyboard");

var _MainSplit = _interopRequireDefault(require("./MainSplit"));

var _RightPanel = _interopRequireDefault(require("./RightPanel"));

var _RoomViewStore = _interopRequireDefault(require("../../stores/RoomViewStore"));

var _RoomScrollStateStore = _interopRequireDefault(require("../../stores/RoomScrollStateStore"));

var _WidgetEchoStore = _interopRequireDefault(require("../../stores/WidgetEchoStore"));

var _SettingsStore = _interopRequireWildcard(require("../../settings/SettingsStore"));

var _WidgetUtils = _interopRequireDefault(require("../../utils/WidgetUtils"));

var _AccessibleButton = _interopRequireDefault(require("../views/elements/AccessibleButton"));

var _RightPanelStore = _interopRequireDefault(require("../../stores/RightPanelStore"));

var _EventTile = require("../views/rooms/EventTile");

var _RoomContext = _interopRequireDefault(require("../../contexts/RoomContext"));

var _MatrixClientContext = _interopRequireDefault(require("../../contexts/MatrixClientContext"));

var _ShieldUtils = require("../../utils/ShieldUtils");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// TODO: This component is enormous! There's several things which could stand-alone:
//  - Search results component
//  - Drag and drop
const DEBUG = false;

let debuglog = function () {};

const BROWSER_SUPPORTS_SANDBOX = 'sandbox' in document.createElement('iframe');

if (DEBUG) {
  // using bind means that we get to keep useful line numbers in the console
  debuglog = console.log.bind(console);
}

var _default = (0, _createReactClass.default)({
  displayName: 'RoomView',
  propTypes: {
    ConferenceHandler: _propTypes.default.any,
    // Called with the credentials of a registered user (if they were a ROU that
    // transitioned to PWLU)
    onRegistered: _propTypes.default.func,
    // An object representing a third party invite to join this room
    // Fields:
    // * inviteSignUrl (string) The URL used to join this room from an email invite
    //                          (given as part of the link in the invite email)
    // * invitedEmail (string) The email address that was invited to this room
    thirdPartyInvite: _propTypes.default.object,
    // Any data about the room that would normally come from the homeserver
    // but has been passed out-of-band, eg. the room name and avatar URL
    // from an email invite (a workaround for the fact that we can't
    // get this information from the HS using an email invite).
    // Fields:
    //  * name (string) The room's name
    //  * avatarUrl (string) The mxc:// avatar URL for the room
    //  * inviterName (string) The display name of the person who
    //  *                      invited us to the room
    oobData: _propTypes.default.object,
    // Servers the RoomView can use to try and assist joins
    viaServers: _propTypes.default.arrayOf(_propTypes.default.string)
  },
  statics: {
    contextType: _MatrixClientContext.default
  },
  getInitialState: function () {
    const llMembers = this.context.hasLazyLoadMembersEnabled();
    return {
      room: null,
      roomId: null,
      roomLoading: true,
      peekLoading: false,
      shouldPeek: true,
      // Media limits for uploading.
      mediaConfig: undefined,
      // used to trigger a rerender in TimelinePanel once the members are loaded,
      // so RR are rendered again (now with the members available), ...
      membersLoaded: !llMembers,
      // The event to be scrolled to initially
      initialEventId: null,
      // The offset in pixels from the event with which to scroll vertically
      initialEventPixelOffset: null,
      // Whether to highlight the event scrolled to
      isInitialEventHighlighted: null,
      forwardingEvent: null,
      numUnreadMessages: 0,
      draggingFile: false,
      searching: false,
      searchResults: null,
      callState: null,
      guestsCanJoin: false,
      canPeek: false,
      showApps: false,
      isAlone: false,
      isPeeking: false,
      showingPinned: false,
      showReadReceipts: true,
      showRightPanel: _RightPanelStore.default.getSharedInstance().isOpenForRoom,
      // error object, as from the matrix client/server API
      // If we failed to load information about the room,
      // store the error here.
      roomLoadError: null,
      // Have we sent a request to join the room that we're waiting to complete?
      joining: false,
      // this is true if we are fully scrolled-down, and are looking at
      // the end of the live timeline. It has the effect of hiding the
      // 'scroll to bottom' knob, among a couple of other things.
      atEndOfLiveTimeline: true,
      atEndOfLiveTimelineInit: false,
      // used by componentDidUpdate to avoid unnecessary checks
      showTopUnreadMessagesBar: false,
      auxPanelMaxHeight: undefined,
      statusBarVisible: false,
      // We load this later by asking the js-sdk to suggest a version for us.
      // This object is the result of Room#getRecommendedVersion()
      upgradeRecommendation: null,
      canReact: false,
      canReply: false
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    this.context.on("Room", this.onRoom);
    this.context.on("Room.timeline", this.onRoomTimeline);
    this.context.on("Room.name", this.onRoomName);
    this.context.on("Room.accountData", this.onRoomAccountData);
    this.context.on("RoomState.events", this.onRoomStateEvents);
    this.context.on("RoomState.members", this.onRoomStateMember);
    this.context.on("Room.myMembership", this.onMyMembership);
    this.context.on("accountData", this.onAccountData);
    this.context.on("crypto.keyBackupStatus", this.onKeyBackupStatus);
    this.context.on("deviceVerificationChanged", this.onDeviceVerificationChanged);
    this.context.on("userTrustStatusChanged", this.onUserVerificationChanged); // Start listening for RoomViewStore updates

    this._roomStoreToken = _RoomViewStore.default.addListener(this._onRoomViewStoreUpdate);
    this._rightPanelStoreToken = _RightPanelStore.default.getSharedInstance().addListener(this._onRightPanelStoreUpdate);

    this._onRoomViewStoreUpdate(true);

    _WidgetEchoStore.default.on('update', this._onWidgetEchoStoreUpdate);

    this._showReadReceiptsWatchRef = _SettingsStore.default.watchSetting("showReadReceipts", null, this._onReadReceiptsChange);
    this._roomView = (0, _react.createRef)();
    this._searchResultsPanel = (0, _react.createRef)();
  },
  _onReadReceiptsChange: function () {
    this.setState({
      showReadReceipts: _SettingsStore.default.getValue("showReadReceipts", this.state.roomId)
    });
  },
  _onRoomViewStoreUpdate: function (initial) {
    if (this.unmounted) {
      return;
    }

    if (!initial && this.state.roomId !== _RoomViewStore.default.getRoomId()) {
      // RoomView explicitly does not support changing what room
      // is being viewed: instead it should just be re-mounted when
      // switching rooms. Therefore, if the room ID changes, we
      // ignore this. We either need to do this or add code to handle
      // saving the scroll position (otherwise we end up saving the
      // scroll position against the wrong room).
      // Given that doing the setState here would cause a bunch of
      // unnecessary work, we just ignore the change since we know
      // that if the current room ID has changed from what we thought
      // it was, it means we're about to be unmounted.
      return;
    }

    const roomId = _RoomViewStore.default.getRoomId();

    const newState = {
      roomId,
      roomAlias: _RoomViewStore.default.getRoomAlias(),
      roomLoading: _RoomViewStore.default.isRoomLoading(),
      roomLoadError: _RoomViewStore.default.getRoomLoadError(),
      joining: _RoomViewStore.default.isJoining(),
      initialEventId: _RoomViewStore.default.getInitialEventId(),
      isInitialEventHighlighted: _RoomViewStore.default.isInitialEventHighlighted(),
      forwardingEvent: _RoomViewStore.default.getForwardingEvent(),
      shouldPeek: _RoomViewStore.default.shouldPeek(),
      showingPinned: _SettingsStore.default.getValue("PinnedEvents.isOpen", roomId),
      showReadReceipts: _SettingsStore.default.getValue("showReadReceipts", roomId)
    };

    if (!initial && this.state.shouldPeek && !newState.shouldPeek) {
      // Stop peeking because we have joined this room now
      this.context.stopPeeking();
    } // Temporary logging to diagnose https://github.com/vector-im/riot-web/issues/4307


    console.log('RVS update:', newState.roomId, newState.roomAlias, 'loading?', newState.roomLoading, 'joining?', newState.joining, 'initial?', initial, 'shouldPeek?', newState.shouldPeek); // NB: This does assume that the roomID will not change for the lifetime of
    // the RoomView instance

    if (initial) {
      newState.room = this.context.getRoom(newState.roomId);

      if (newState.room) {
        newState.showApps = this._shouldShowApps(newState.room);

        this._onRoomLoaded(newState.room);
      }
    }

    if (this.state.roomId === null && newState.roomId !== null) {
      // Get the scroll state for the new room
      // If an event ID wasn't specified, default to the one saved for this room
      // in the scroll state store. Assume initialEventPixelOffset should be set.
      if (!newState.initialEventId) {
        const roomScrollState = _RoomScrollStateStore.default.getScrollState(newState.roomId);

        if (roomScrollState) {
          newState.initialEventId = roomScrollState.focussedEvent;
          newState.initialEventPixelOffset = roomScrollState.pixelOffset;
        }
      }
    } // Clear the search results when clicking a search result (which changes the
    // currently scrolled to event, this.state.initialEventId).


    if (this.state.initialEventId !== newState.initialEventId) {
      newState.searchResults = null;
    }

    this.setState(newState); // At this point, newState.roomId could be null (e.g. the alias might not
    // have been resolved yet) so anything called here must handle this case.
    // We pass the new state into this function for it to read: it needs to
    // observe the new state but we don't want to put it in the setState
    // callback because this would prevent the setStates from being batched,
    // ie. cause it to render RoomView twice rather than the once that is necessary.

    if (initial) {
      this._setupRoom(newState.room, newState.roomId, newState.joining, newState.shouldPeek);
    }
  },

  _getRoomId() {
    // According to `_onRoomViewStoreUpdate`, `state.roomId` can be null
    // if we have a room alias we haven't resolved yet. To work around this,
    // first we'll try the room object if it's there, and then fallback to
    // the bare room ID. (We may want to update `state.roomId` after
    // resolving aliases, so we could always trust it.)
    return this.state.room ? this.state.room.roomId : this.state.roomId;
  },

  _getPermalinkCreatorForRoom: function (room) {
    if (!this._permalinkCreators) this._permalinkCreators = {};
    if (this._permalinkCreators[room.roomId]) return this._permalinkCreators[room.roomId];
    this._permalinkCreators[room.roomId] = new _Permalinks.RoomPermalinkCreator(room);

    if (this.state.room && room.roomId === this.state.room.roomId) {
      // We want to watch for changes in the creator for the primary room in the view, but
      // don't need to do so for search results.
      this._permalinkCreators[room.roomId].start();
    } else {
      this._permalinkCreators[room.roomId].load();
    }

    return this._permalinkCreators[room.roomId];
  },
  _stopAllPermalinkCreators: function () {
    if (!this._permalinkCreators) return;

    for (const roomId of Object.keys(this._permalinkCreators)) {
      this._permalinkCreators[roomId].stop();
    }
  },
  _onWidgetEchoStoreUpdate: function () {
    this.setState({
      showApps: this._shouldShowApps(this.state.room)
    });
  },
  _setupRoom: function (room, roomId, joining, shouldPeek) {
    // if this is an unknown room then we're in one of three states:
    // - This is a room we can peek into (search engine) (we can /peek)
    // - This is a room we can publicly join or were invited to. (we can /join)
    // - This is a room we cannot join at all. (no action can help us)
    // We can't try to /join because this may implicitly accept invites (!)
    // We can /peek though. If it fails then we present the join UI. If it
    // succeeds then great, show the preview (but we still may be able to /join!).
    // Note that peeking works by room ID and room ID only, as opposed to joining
    // which must be by alias or invite wherever possible (peeking currently does
    // not work over federation).
    // NB. We peek if we have never seen the room before (i.e. js-sdk does not know
    // about it). We don't peek in the historical case where we were joined but are
    // now not joined because the js-sdk peeking API will clobber our historical room,
    // making it impossible to indicate a newly joined room.
    if (!joining && roomId) {
      if (this.props.autoJoin) {
        this.onJoinButtonClicked();
      } else if (!room && shouldPeek) {
        console.info("Attempting to peek into room %s", roomId);
        this.setState({
          peekLoading: true,
          isPeeking: true // this will change to false if peeking fails

        });
        this.context.peekInRoom(roomId).then(room => {
          if (this.unmounted) {
            return;
          }

          this.setState({
            room: room,
            peekLoading: false
          });

          this._onRoomLoaded(room);
        }).catch(err => {
          if (this.unmounted) {
            return;
          } // Stop peeking if anything went wrong


          this.setState({
            isPeeking: false
          }); // This won't necessarily be a MatrixError, but we duck-type
          // here and say if it's got an 'errcode' key with the right value,
          // it means we can't peek.

          if (err.errcode === "M_GUEST_ACCESS_FORBIDDEN" || err.errcode === 'M_FORBIDDEN') {
            // This is fine: the room just isn't peekable (we assume).
            this.setState({
              peekLoading: false
            });
          } else {
            throw err;
          }
        });
      } else if (room) {
        // Stop peeking because we have joined this room previously
        this.context.stopPeeking();
        this.setState({
          isPeeking: false
        });
      }
    }
  },
  _shouldShowApps: function (room) {
    if (!BROWSER_SUPPORTS_SANDBOX) return false; // Check if user has previously chosen to hide the app drawer for this
    // room. If so, do not show apps

    const hideWidgetDrawer = localStorage.getItem(room.roomId + "_hide_widget_drawer");

    if (hideWidgetDrawer === "true") {
      return false;
    }

    const widgets = _WidgetEchoStore.default.getEchoedRoomWidgets(room.roomId, _WidgetUtils.default.getRoomWidgets(room));

    return widgets.length > 0 || _WidgetEchoStore.default.roomHasPendingWidgets(room.roomId, _WidgetUtils.default.getRoomWidgets(room));
  },
  componentDidMount: function () {
    const call = this._getCallForRoom();

    const callState = call ? call.call_state : "ended";
    this.setState({
      callState: callState
    });

    this._updateConfCallNotification();

    window.addEventListener('beforeunload', this.onPageUnload);

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.on("middlePanelResized", this.onResize);
    }

    this.onResize();
    document.addEventListener("keydown", this.onKeyDown);
  },
  shouldComponentUpdate: function (nextProps, nextState) {
    return !ObjectUtils.shallowEqual(this.props, nextProps) || !ObjectUtils.shallowEqual(this.state, nextState);
  },
  componentDidUpdate: function () {
    if (this._roomView.current) {
      const roomView = this._roomView.current;

      if (!roomView.ondrop) {
        roomView.addEventListener('drop', this.onDrop);
        roomView.addEventListener('dragover', this.onDragOver);
        roomView.addEventListener('dragleave', this.onDragLeaveOrEnd);
        roomView.addEventListener('dragend', this.onDragLeaveOrEnd);
      }
    } // Note: We check the ref here with a flag because componentDidMount, despite
    // documentation, does not define our messagePanel ref. It looks like our spinner
    // in render() prevents the ref from being set on first mount, so we try and
    // catch the messagePanel when it does mount. Because we only want the ref once,
    // we use a boolean flag to avoid duplicate work.


    if (this._messagePanel && !this.state.atEndOfLiveTimelineInit) {
      this.setState({
        atEndOfLiveTimelineInit: true,
        atEndOfLiveTimeline: this._messagePanel.isAtEndOfLiveTimeline()
      });
    }
  },
  componentWillUnmount: function () {
    // set a boolean to say we've been unmounted, which any pending
    // promises can use to throw away their results.
    //
    // (We could use isMounted, but facebook have deprecated that.)
    this.unmounted = true; // update the scroll map before we get unmounted

    if (this.state.roomId) {
      _RoomScrollStateStore.default.setScrollState(this.state.roomId, this._getScrollState());
    }

    if (this.state.shouldPeek) {
      this.context.stopPeeking();
    } // stop tracking room changes to format permalinks


    this._stopAllPermalinkCreators();

    if (this._roomView.current) {
      // disconnect the D&D event listeners from the room view. This
      // is really just for hygiene - we're going to be
      // deleted anyway, so it doesn't matter if the event listeners
      // don't get cleaned up.
      const roomView = this._roomView.current;
      roomView.removeEventListener('drop', this.onDrop);
      roomView.removeEventListener('dragover', this.onDragOver);
      roomView.removeEventListener('dragleave', this.onDragLeaveOrEnd);
      roomView.removeEventListener('dragend', this.onDragLeaveOrEnd);
    }

    _dispatcher.default.unregister(this.dispatcherRef);

    if (this.context) {
      this.context.removeListener("Room", this.onRoom);
      this.context.removeListener("Room.timeline", this.onRoomTimeline);
      this.context.removeListener("Room.name", this.onRoomName);
      this.context.removeListener("Room.accountData", this.onRoomAccountData);
      this.context.removeListener("RoomState.events", this.onRoomStateEvents);
      this.context.removeListener("Room.myMembership", this.onMyMembership);
      this.context.removeListener("RoomState.members", this.onRoomStateMember);
      this.context.removeListener("accountData", this.onAccountData);
      this.context.removeListener("crypto.keyBackupStatus", this.onKeyBackupStatus);
      this.context.removeListener("deviceVerificationChanged", this.onDeviceVerificationChanged);
      this.context.removeListener("userTrustStatusChanged", this.onUserVerificationChanged);
    }

    window.removeEventListener('beforeunload', this.onPageUnload);

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.removeListener("middlePanelResized", this.onResize);
    }

    document.removeEventListener("keydown", this.onKeyDown); // Remove RoomStore listener

    if (this._roomStoreToken) {
      this._roomStoreToken.remove();
    } // Remove RightPanelStore listener


    if (this._rightPanelStoreToken) {
      this._rightPanelStoreToken.remove();
    }

    _WidgetEchoStore.default.removeListener('update', this._onWidgetEchoStoreUpdate);

    if (this._showReadReceiptsWatchRef) {
      _SettingsStore.default.unwatchSetting(this._showReadReceiptsWatchRef);

      this._showReadReceiptsWatchRef = null;
    } // cancel any pending calls to the rate_limited_funcs


    this._updateRoomMembers.cancelPendingCall(); // no need to do this as Dir & Settings are now overlays. It just burnt CPU.
    // console.log("Tinter.tint from RoomView.unmount");
    // Tinter.tint(); // reset colourscheme

  },
  _onRightPanelStoreUpdate: function () {
    this.setState({
      showRightPanel: _RightPanelStore.default.getSharedInstance().isOpenForRoom
    });
  },

  onPageUnload(event) {
    if (_ContentMessages.default.sharedInstance().getCurrentUploads().length > 0) {
      return event.returnValue = (0, _languageHandler._t)("You seem to be uploading files, are you sure you want to quit?");
    } else if (this._getCallForRoom() && this.state.callState !== 'ended') {
      return event.returnValue = (0, _languageHandler._t)("You seem to be in a call, are you sure you want to quit?");
    }
  },

  onKeyDown: function (ev) {
    let handled = false;
    const ctrlCmdOnly = (0, _Keyboard.isOnlyCtrlOrCmdKeyEvent)(ev);

    switch (ev.key) {
      case _Keyboard.Key.D:
        if (ctrlCmdOnly) {
          this.onMuteAudioClick();
          handled = true;
        }

        break;

      case _Keyboard.Key.E:
        if (ctrlCmdOnly) {
          this.onMuteVideoClick();
          handled = true;
        }

        break;
    }

    if (handled) {
      ev.stopPropagation();
      ev.preventDefault();
    }
  },
  onAction: function (payload) {
    switch (payload.action) {
      case 'message_send_failed':
      case 'message_sent':
        this._checkIfAlone(this.state.room);

        break;

      case 'post_sticker_message':
        this.injectSticker(payload.data.content.url, payload.data.content.info, payload.data.description || payload.data.name);
        break;

      case 'picture_snapshot':
        _ContentMessages.default.sharedInstance().sendContentListToRoom([payload.file], this.state.room.roomId, this.context);

        break;

      case 'notifier_enabled':
      case 'upload_started':
      case 'upload_finished':
      case 'upload_canceled':
        this.forceUpdate();
        break;

      case 'call_state':
        // don't filter out payloads for room IDs other than props.room because
        // we may be interested in the conf 1:1 room
        if (!payload.room_id) {
          return;
        }

        var call = this._getCallForRoom();

        var callState;

        if (call) {
          callState = call.call_state;
        } else {
          callState = "ended";
        } // possibly remove the conf call notification if we're now in
        // the conf


        this._updateConfCallNotification();

        this.setState({
          callState: callState
        });
        break;

      case 'appsDrawer':
        this.setState({
          showApps: payload.show
        });
        break;

      case 'reply_to_event':
        if (this.state.searchResults && payload.event.getRoomId() === this.state.roomId && !this.unmounted) {
          this.onCancelSearchClick();
        }

        break;

      case 'quote':
        if (this.state.searchResults) {
          const roomId = payload.event.getRoomId();

          if (roomId === this.state.roomId) {
            this.onCancelSearchClick();
          }

          setImmediate(() => {
            _dispatcher.default.dispatch({
              action: 'view_room',
              room_id: roomId,
              deferred_action: payload
            });
          });
        }

        break;
    }
  },
  onRoomTimeline: function (ev, room, toStartOfTimeline, removed, data) {
    if (this.unmounted) return; // ignore events for other rooms

    if (!room) return;
    if (!this.state.room || room.roomId != this.state.room.roomId) return; // ignore events from filtered timelines

    if (data.timeline.getTimelineSet() !== room.getUnfilteredTimelineSet()) return;

    if (ev.getType() === "org.matrix.room.preview_urls") {
      this._updatePreviewUrlVisibility(room);
    }

    if (ev.getType() === "m.room.encryption") {
      this._updateE2EStatus(room);
    } // ignore anything but real-time updates at the end of the room:
    // updates from pagination will happen when the paginate completes.


    if (toStartOfTimeline || !data || !data.liveEvent) return; // no point handling anything while we're waiting for the join to finish:
    // we'll only be showing a spinner.

    if (this.state.joining) return;

    if (ev.getSender() !== this.context.credentials.userId) {
      // update unread count when scrolled up
      if (!this.state.searchResults && this.state.atEndOfLiveTimeline) {// no change
      } else if (!(0, _shouldHideEvent.default)(ev)) {
        this.setState((state, props) => {
          return {
            numUnreadMessages: state.numUnreadMessages + 1
          };
        });
      }
    }
  },
  onRoomName: function (room) {
    if (this.state.room && room.roomId == this.state.room.roomId) {
      this.forceUpdate();
    }
  },
  onRoomRecoveryReminderDontAskAgain: function () {
    // Called when the option to not ask again is set:
    // force an update to hide the recovery reminder
    this.forceUpdate();
  },

  onKeyBackupStatus() {
    // Key backup status changes affect whether the in-room recovery
    // reminder is displayed.
    this.forceUpdate();
  },

  canResetTimeline: function () {
    if (!this._messagePanel) {
      return true;
    }

    return this._messagePanel.canResetTimeline();
  },
  // called when state.room is first initialised (either at initial load,
  // after a successful peek, or after we join the room).
  _onRoomLoaded: function (room) {
    this._calculatePeekRules(room);

    this._updatePreviewUrlVisibility(room);

    this._loadMembersIfJoined(room);

    this._calculateRecommendedVersion(room);

    this._updateE2EStatus(room);

    this._updatePermissions(room);
  },
  _calculateRecommendedVersion: async function (room) {
    this.setState({
      upgradeRecommendation: await room.getRecommendedVersion()
    });
  },
  _loadMembersIfJoined: async function (room) {
    // lazy load members if enabled
    if (this.context.hasLazyLoadMembersEnabled()) {
      if (room && room.getMyMembership() === 'join') {
        try {
          await room.loadMembersIfNeeded();

          if (!this.unmounted) {
            this.setState({
              membersLoaded: true
            });
          }
        } catch (err) {
          const errorMessage = "Fetching room members for ".concat(room.roomId, " failed.") + " Room members will appear incomplete.";
          console.error(errorMessage);
          console.error(err);
        }
      }
    }
  },
  _calculatePeekRules: function (room) {
    const guestAccessEvent = room.currentState.getStateEvents("m.room.guest_access", "");

    if (guestAccessEvent && guestAccessEvent.getContent().guest_access === "can_join") {
      this.setState({
        guestsCanJoin: true
      });
    }

    const historyVisibility = room.currentState.getStateEvents("m.room.history_visibility", "");

    if (historyVisibility && historyVisibility.getContent().history_visibility === "world_readable") {
      this.setState({
        canPeek: true
      });
    }
  },
  _updatePreviewUrlVisibility: function ({
    roomId
  }) {
    // URL Previews in E2EE rooms can be a privacy leak so use a different setting which is per-room explicit
    const key = this.context.isRoomEncrypted(roomId) ? 'urlPreviewsEnabled_e2ee' : 'urlPreviewsEnabled';
    this.setState({
      showUrlPreview: _SettingsStore.default.getValue(key, roomId)
    });
  },
  onRoom: function (room) {
    if (!room || room.roomId !== this.state.roomId) {
      return;
    }

    this.setState({
      room: room
    }, () => {
      this._onRoomLoaded(room);
    });
  },
  onDeviceVerificationChanged: function (userId, device) {
    const room = this.state.room;

    if (!room.currentState.getMember(userId)) {
      return;
    }

    this._updateE2EStatus(room);
  },
  onUserVerificationChanged: function (userId, _trustStatus) {
    const room = this.state.room;

    if (!room || !room.currentState.getMember(userId)) {
      return;
    }

    this._updateE2EStatus(room);
  },
  _updateE2EStatus: async function (room) {
    if (!this.context.isRoomEncrypted(room.roomId)) {
      return;
    }

    if (!this.context.isCryptoEnabled()) {
      // If crypto is not currently enabled, we aren't tracking devices at all,
      // so we don't know what the answer is. Let's error on the safe side and show
      // a warning for this case.
      this.setState({
        e2eStatus: "warning"
      });
      return;
    }

    if (!_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      room.hasUnverifiedDevices().then(hasUnverifiedDevices => {
        this.setState({
          e2eStatus: hasUnverifiedDevices ? "warning" : "verified"
        });
      });
      debuglog("e2e check is warning/verified only as cross-signing is off");
      return;
    }
    /* At this point, the user has encryption on and cross-signing on */


    this.setState({
      e2eStatus: await (0, _ShieldUtils.shieldStatusForRoom)(this.context, room)
    });
  },
  updateTint: function () {
    const room = this.state.room;
    if (!room) return;
    console.log("Tinter.tint from updateTint");

    const colorScheme = _SettingsStore.default.getValue("roomColor", room.roomId);

    _Tinter.default.tint(colorScheme.primary_color, colorScheme.secondary_color);
  },
  onAccountData: function (event) {
    const type = event.getType();

    if ((type === "org.matrix.preview_urls" || type === "im.vector.web.settings") && this.state.room) {
      // non-e2ee url previews are stored in legacy event type `org.matrix.room.preview_urls`
      this._updatePreviewUrlVisibility(this.state.room);
    }
  },
  onRoomAccountData: function (event, room) {
    if (room.roomId == this.state.roomId) {
      const type = event.getType();

      if (type === "org.matrix.room.color_scheme") {
        const colorScheme = event.getContent(); // XXX: we should validate the event

        console.log("Tinter.tint from onRoomAccountData");

        _Tinter.default.tint(colorScheme.primary_color, colorScheme.secondary_color);
      } else if (type === "org.matrix.room.preview_urls" || type === "im.vector.web.settings") {
        // non-e2ee url previews are stored in legacy event type `org.matrix.room.preview_urls`
        this._updatePreviewUrlVisibility(room);
      }
    }
  },
  onRoomStateEvents: function (ev, state) {
    // ignore if we don't have a room yet
    if (!this.state.room || this.state.room.roomId !== state.roomId) {
      return;
    }

    this._updatePermissions(this.state.room);
  },
  onRoomStateMember: function (ev, state, member) {
    // ignore if we don't have a room yet
    if (!this.state.room) {
      return;
    } // ignore members in other rooms


    if (member.roomId !== this.state.room.roomId) {
      return;
    }

    this._updateRoomMembers(member);
  },
  onMyMembership: function (room, membership, oldMembership) {
    if (room.roomId === this.state.roomId) {
      this.forceUpdate();

      this._loadMembersIfJoined(room);

      this._updatePermissions(room);
    }
  },
  _updatePermissions: function (room) {
    if (room) {
      const me = this.context.getUserId();
      const canReact = room.getMyMembership() === "join" && room.currentState.maySendEvent("m.reaction", me);
      const canReply = room.maySendMessage();
      this.setState({
        canReact,
        canReply
      });
    }
  },
  // rate limited because a power level change will emit an event for every
  // member in the room.
  _updateRoomMembers: (0, _ratelimitedfunc.default)(function (dueToMember) {
    // a member state changed in this room
    // refresh the conf call notification state
    this._updateConfCallNotification();

    this._updateDMState();

    let memberCountInfluence = 0;

    if (dueToMember && dueToMember.membership === "invite" && this.state.room.getInvitedMemberCount() === 0) {
      // A member got invited, but the room hasn't detected that change yet. Influence the member
      // count by 1 to counteract this.
      memberCountInfluence = 1;
    }

    this._checkIfAlone(this.state.room, memberCountInfluence);

    this._updateE2EStatus(this.state.room);
  }, 500),
  _checkIfAlone: function (room, countInfluence) {
    let warnedAboutLonelyRoom = false;

    if (localStorage) {
      warnedAboutLonelyRoom = localStorage.getItem('mx_user_alone_warned_' + this.state.room.roomId);
    }

    if (warnedAboutLonelyRoom) {
      if (this.state.isAlone) this.setState({
        isAlone: false
      });
      return;
    }

    let joinedOrInvitedMemberCount = room.getJoinedMemberCount() + room.getInvitedMemberCount();
    if (countInfluence) joinedOrInvitedMemberCount += countInfluence;
    this.setState({
      isAlone: joinedOrInvitedMemberCount === 1
    });
  },
  _updateConfCallNotification: function () {
    const room = this.state.room;

    if (!room || !this.props.ConferenceHandler) {
      return;
    }

    const confMember = room.getMember(this.props.ConferenceHandler.getConferenceUserIdForRoom(room.roomId));

    if (!confMember) {
      return;
    }

    const confCall = this.props.ConferenceHandler.getConferenceCallForRoom(confMember.roomId); // A conf call notification should be displayed if there is an ongoing
    // conf call but this cilent isn't a part of it.

    this.setState({
      displayConfCallNotification: (!confCall || confCall.call_state === "ended") && confMember.membership === "join"
    });
  },

  _updateDMState() {
    const room = this.state.room;

    if (room.getMyMembership() != "join") {
      return;
    }

    const dmInviter = room.getDMInviter();

    if (dmInviter) {
      Rooms.setDMRoom(room.roomId, dmInviter);
    }
  },

  onSearchResultsFillRequest: function (backwards) {
    if (!backwards) {
      return Promise.resolve(false);
    }

    if (this.state.searchResults.next_batch) {
      debuglog("requesting more search results");
      const searchPromise = this.context.backPaginateRoomEventsSearch(this.state.searchResults);
      return this._handleSearchResult(searchPromise);
    } else {
      debuglog("no more search results");
      return Promise.resolve(false);
    }
  },
  onInviteButtonClick: function () {
    // call AddressPickerDialog
    _dispatcher.default.dispatch({
      action: 'view_invite',
      roomId: this.state.room.roomId
    });

    this.setState({
      isAlone: false
    }); // there's a good chance they'll invite someone
  },
  onStopAloneWarningClick: function () {
    if (localStorage) {
      localStorage.setItem('mx_user_alone_warned_' + this.state.room.roomId, true);
    }

    this.setState({
      isAlone: false
    });
  },
  onJoinButtonClicked: function (ev) {
    // If the user is a ROU, allow them to transition to a PWLU
    if (this.context && this.context.isGuest()) {
      // Join this room once the user has registered and logged in
      // (If we failed to peek, we may not have a valid room object.)
      _dispatcher.default.dispatch({
        action: 'do_after_sync_prepared',
        deferred_action: {
          action: 'view_room',
          room_id: this._getRoomId()
        }
      }); // Don't peek whilst registering otherwise getPendingEventList complains
      // Do this by indicating our intention to join
      // XXX: ILAG is disabled for now,
      // see https://github.com/vector-im/riot-web/issues/8222


      _dispatcher.default.dispatch({
        action: 'require_registration'
      }); // dis.dispatch({
      //     action: 'will_join',
      // });
      // const SetMxIdDialog = sdk.getComponent('views.dialogs.SetMxIdDialog');
      // const close = Modal.createTrackedDialog('Set MXID', '', SetMxIdDialog, {
      //     homeserverUrl: cli.getHomeserverUrl(),
      //     onFinished: (submitted, credentials) => {
      //         if (submitted) {
      //             this.props.onRegistered(credentials);
      //         } else {
      //             dis.dispatch({
      //                 action: 'cancel_after_sync_prepared',
      //             });
      //             dis.dispatch({
      //                 action: 'cancel_join',
      //             });
      //         }
      //     },
      //     onDifferentServerClicked: (ev) => {
      //         dis.dispatch({action: 'start_registration'});
      //         close();
      //     },
      //     onLoginClick: (ev) => {
      //         dis.dispatch({action: 'start_login'});
      //         close();
      //     },
      // }).close;
      // return;

    } else {
      Promise.resolve().then(() => {
        const signUrl = this.props.thirdPartyInvite ? this.props.thirdPartyInvite.inviteSignUrl : undefined;

        _dispatcher.default.dispatch({
          action: 'join_room',
          opts: {
            inviteSignUrl: signUrl,
            viaServers: this.props.viaServers
          }
        });

        return Promise.resolve();
      });
    }
  },
  onMessageListScroll: function (ev) {
    if (this._messagePanel.isAtEndOfLiveTimeline()) {
      this.setState({
        numUnreadMessages: 0,
        atEndOfLiveTimeline: true
      });
    } else {
      this.setState({
        atEndOfLiveTimeline: false
      });
    }

    this._updateTopUnreadMessagesBar();
  },
  onDragOver: function (ev) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.dataTransfer.dropEffect = 'none';
    const items = [...ev.dataTransfer.items];

    if (items.length >= 1) {
      const isDraggingFiles = items.every(function (item) {
        return item.kind == 'file';
      });

      if (isDraggingFiles) {
        this.setState({
          draggingFile: true
        });
        ev.dataTransfer.dropEffect = 'copy';
      }
    }
  },
  onDrop: function (ev) {
    ev.stopPropagation();
    ev.preventDefault();

    _ContentMessages.default.sharedInstance().sendContentListToRoom(ev.dataTransfer.files, this.state.room.roomId, this.context);

    this.setState({
      draggingFile: false
    });

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  },
  onDragLeaveOrEnd: function (ev) {
    ev.stopPropagation();
    ev.preventDefault();
    this.setState({
      draggingFile: false
    });
  },
  injectSticker: function (url, info, text) {
    if (this.context.isGuest()) {
      _dispatcher.default.dispatch({
        action: 'require_registration'
      });

      return;
    }

    _ContentMessages.default.sharedInstance().sendStickerContentToRoom(url, this.state.room.roomId, info, text, this.context).then(undefined, error => {
      if (error.name === "UnknownDeviceError") {
        // Let the staus bar handle this
        return;
      }
    });
  },
  onSearch: function (term, scope) {
    this.setState({
      searchTerm: term,
      searchScope: scope,
      searchResults: {},
      searchHighlights: []
    }); // if we already have a search panel, we need to tell it to forget
    // about its scroll state.

    if (this._searchResultsPanel.current) {
      this._searchResultsPanel.current.resetScrollState();
    } // make sure that we don't end up showing results from
    // an aborted search by keeping a unique id.
    //
    // todo: should cancel any previous search requests.


    this.searchId = new Date().getTime();
    let roomId;
    if (scope === "Room") roomId = this.state.room.roomId;
    debuglog("sending search request");
    const searchPromise = (0, _Searching.default)(term, roomId);

    this._handleSearchResult(searchPromise);
  },
  _handleSearchResult: function (searchPromise) {
    const self = this; // keep a record of the current search id, so that if the search terms
    // change before we get a response, we can ignore the results.

    const localSearchId = this.searchId;
    this.setState({
      searchInProgress: true
    });
    return searchPromise.then(function (results) {
      debuglog("search complete");

      if (self.unmounted || !self.state.searching || self.searchId != localSearchId) {
        console.error("Discarding stale search results");
        return;
      } // postgres on synapse returns us precise details of the strings
      // which actually got matched for highlighting.
      //
      // In either case, we want to highlight the literal search term
      // whether it was used by the search engine or not.


      let highlights = results.highlights;

      if (highlights.indexOf(self.state.searchTerm) < 0) {
        highlights = highlights.concat(self.state.searchTerm);
      } // For overlapping highlights,
      // favour longer (more specific) terms first


      highlights = highlights.sort(function (a, b) {
        return b.length - a.length;
      });
      self.setState({
        searchHighlights: highlights,
        searchResults: results
      });
    }, function (error) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Search failed: " + error);

      _Modal.default.createTrackedDialog('Search failed', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Search failed"),
        description: error && error.message ? error.message : (0, _languageHandler._t)("Server may be unavailable, overloaded, or search timed out :(")
      });
    }).finally(function () {
      self.setState({
        searchInProgress: false
      });
    });
  },
  getSearchResultTiles: function () {
    const SearchResultTile = sdk.getComponent('rooms.SearchResultTile');
    const Spinner = sdk.getComponent("elements.Spinner"); // XXX: todo: merge overlapping results somehow?
    // XXX: why doesn't searching on name work?

    const ret = [];

    if (this.state.searchInProgress) {
      ret.push(_react.default.createElement("li", {
        key: "search-spinner"
      }, _react.default.createElement(Spinner, null)));
    }

    if (!this.state.searchResults.next_batch) {
      if (this.state.searchResults.results.length == 0) {
        ret.push(_react.default.createElement("li", {
          key: "search-top-marker"
        }, _react.default.createElement("h2", {
          className: "mx_RoomView_topMarker"
        }, (0, _languageHandler._t)("No results"))));
      } else {
        ret.push(_react.default.createElement("li", {
          key: "search-top-marker"
        }, _react.default.createElement("h2", {
          className: "mx_RoomView_topMarker"
        }, (0, _languageHandler._t)("No more results"))));
      }
    } // once dynamic content in the search results load, make the scrollPanel check
    // the scroll offsets.


    const onHeightChanged = () => {
      const scrollPanel = this._searchResultsPanel.current;

      if (scrollPanel) {
        scrollPanel.checkScroll();
      }
    };

    let lastRoomId;

    for (let i = this.state.searchResults.results.length - 1; i >= 0; i--) {
      const result = this.state.searchResults.results[i];
      const mxEv = result.context.getEvent();
      const roomId = mxEv.getRoomId();
      const room = this.context.getRoom(roomId);

      if (!(0, _EventTile.haveTileForEvent)(mxEv)) {
        // XXX: can this ever happen? It will make the result count
        // not match the displayed count.
        continue;
      }

      if (this.state.searchScope === 'All') {
        if (roomId != lastRoomId) {
          // XXX: if we've left the room, we might not know about
          // it. We should tell the js sdk to go and find out about
          // it. But that's not an issue currently, as synapse only
          // returns results for rooms we're joined to.
          const roomName = room ? room.name : (0, _languageHandler._t)("Unknown room %(roomId)s", {
            roomId: roomId
          });
          ret.push(_react.default.createElement("li", {
            key: mxEv.getId() + "-room"
          }, _react.default.createElement("h2", null, (0, _languageHandler._t)("Room"), ": ", roomName)));
          lastRoomId = roomId;
        }
      }

      const resultLink = "#/room/" + roomId + "/" + mxEv.getId();
      ret.push(_react.default.createElement(SearchResultTile, {
        key: mxEv.getId(),
        searchResult: result,
        searchHighlights: this.state.searchHighlights,
        resultLink: resultLink,
        permalinkCreator: this._getPermalinkCreatorForRoom(room),
        onHeightChanged: onHeightChanged
      }));
    }

    return ret;
  },
  onPinnedClick: function () {
    const nowShowingPinned = !this.state.showingPinned;
    const roomId = this.state.room.roomId;
    this.setState({
      showingPinned: nowShowingPinned,
      searching: false
    });

    _SettingsStore.default.setValue("PinnedEvents.isOpen", roomId, _SettingsStore.SettingLevel.ROOM_DEVICE, nowShowingPinned);
  },
  onSettingsClick: function () {
    _dispatcher.default.dispatch({
      action: 'open_room_settings'
    });
  },
  onCancelClick: function () {
    console.log("updateTint from onCancelClick");
    this.updateTint();

    if (this.state.forwardingEvent) {
      _dispatcher.default.dispatch({
        action: 'forward_event',
        event: null
      });
    }

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  },
  onLeaveClick: function () {
    _dispatcher.default.dispatch({
      action: 'leave_room',
      room_id: this.state.room.roomId
    });
  },
  onForgetClick: function () {
    this.context.forget(this.state.room.roomId).then(function () {
      _dispatcher.default.dispatch({
        action: 'view_next_room'
      });
    }, function (err) {
      const errCode = err.errcode || (0, _languageHandler._t)("unknown error code");
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to forget room', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Failed to forget room %(errCode)s", {
          errCode: errCode
        })
      });
    });
  },
  onRejectButtonClicked: function (ev) {
    const self = this;
    this.setState({
      rejecting: true
    });
    this.context.leave(this.state.roomId).then(function () {
      _dispatcher.default.dispatch({
        action: 'view_next_room'
      });

      self.setState({
        rejecting: false
      });
    }, function (error) {
      console.error("Failed to reject invite: %s", error);
      const msg = error.message ? error.message : JSON.stringify(error);
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to reject invite', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to reject invite"),
        description: msg
      });

      self.setState({
        rejecting: false,
        rejectError: error
      });
    });
  },
  onRejectAndIgnoreClick: async function () {
    this.setState({
      rejecting: true
    });

    try {
      const myMember = this.state.room.getMember(this.context.getUserId());
      const inviteEvent = myMember.events.member;
      const ignoredUsers = this.context.getIgnoredUsers();
      ignoredUsers.push(inviteEvent.getSender()); // de-duped internally in the js-sdk

      await this.context.setIgnoredUsers(ignoredUsers);
      await this.context.leave(this.state.roomId);

      _dispatcher.default.dispatch({
        action: 'view_next_room'
      });

      this.setState({
        rejecting: false
      });
    } catch (error) {
      console.error("Failed to reject invite: %s", error);
      const msg = error.message ? error.message : JSON.stringify(error);
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to reject invite', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to reject invite"),
        description: msg
      });

      self.setState({
        rejecting: false,
        rejectError: error
      });
    }
  },
  onRejectThreepidInviteButtonClicked: function (ev) {
    // We can reject 3pid invites in the same way that we accept them,
    // using /leave rather than /join. In the short term though, we
    // just ignore them.
    // https://github.com/vector-im/vector-web/issues/1134
    _dispatcher.default.dispatch({
      action: 'view_room_directory'
    });
  },
  onSearchClick: function () {
    this.setState({
      searching: !this.state.searching,
      showingPinned: false
    });
  },
  onCancelSearchClick: function () {
    this.setState({
      searching: false,
      searchResults: null
    });
  },
  // jump down to the bottom of this room, where new events are arriving
  jumpToLiveTimeline: function () {
    this._messagePanel.jumpToLiveTimeline();

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  },
  // jump up to wherever our read marker is
  jumpToReadMarker: function () {
    this._messagePanel.jumpToReadMarker();
  },
  // update the read marker to match the read-receipt
  forgetReadMarker: function (ev) {
    ev.stopPropagation();

    this._messagePanel.forgetReadMarker();
  },
  // decide whether or not the top 'unread messages' bar should be shown
  _updateTopUnreadMessagesBar: function () {
    if (!this._messagePanel) {
      return;
    }

    const showBar = this._messagePanel.canJumpToReadMarker();

    if (this.state.showTopUnreadMessagesBar != showBar) {
      this.setState({
        showTopUnreadMessagesBar: showBar
      });
    }
  },
  // get the current scroll position of the room, so that it can be
  // restored when we switch back to it.
  //
  _getScrollState: function () {
    const messagePanel = this._messagePanel;
    if (!messagePanel) return null; // if we're following the live timeline, we want to return null; that
    // means that, if we switch back, we will jump to the read-up-to mark.
    //
    // That should be more intuitive than slavishly preserving the current
    // scroll state, in the case where the room advances in the meantime
    // (particularly in the case that the user reads some stuff on another
    // device).
    //

    if (this.state.atEndOfLiveTimeline) {
      return null;
    }

    const scrollState = messagePanel.getScrollState(); // getScrollState on TimelinePanel *may* return null, so guard against that

    if (!scrollState || scrollState.stuckAtBottom) {
      // we don't really expect to be in this state, but it will
      // occasionally happen when no scroll state has been set on the
      // messagePanel (ie, we didn't have an initial event (so it's
      // probably a new room), there has been no user-initiated scroll, and
      // no read-receipts have arrived to update the scroll position).
      //
      // Return null, which will cause us to scroll to last unread on
      // reload.
      return null;
    }

    return {
      focussedEvent: scrollState.trackedScrollToken,
      pixelOffset: scrollState.pixelOffset
    };
  },
  onResize: function () {
    // It seems flexbox doesn't give us a way to constrain the auxPanel height to have
    // a minimum of the height of the video element, whilst also capping it from pushing out the page
    // so we have to do it via JS instead.  In this implementation we cap the height by putting
    // a maxHeight on the underlying remote video tag.
    // header + footer + status + give us at least 120px of scrollback at all times.
    let auxPanelMaxHeight = window.innerHeight - (83 + // height of RoomHeader
    36 + // height of the status area
    72 + // minimum height of the message compmoser
    120); // amount of desired scrollback
    // XXX: this is a bit of a hack and might possibly cause the video to push out the page anyway
    // but it's better than the video going missing entirely

    if (auxPanelMaxHeight < 50) auxPanelMaxHeight = 50;
    this.setState({
      auxPanelMaxHeight: auxPanelMaxHeight
    });
  },
  onFullscreenClick: function () {
    _dispatcher.default.dispatch({
      action: 'video_fullscreen',
      fullscreen: true
    }, true);
  },
  onMuteAudioClick: function () {
    const call = this._getCallForRoom();

    if (!call) {
      return;
    }

    const newState = !call.isMicrophoneMuted();
    call.setMicrophoneMuted(newState);
    this.forceUpdate(); // TODO: just update the voip buttons
  },
  onMuteVideoClick: function () {
    const call = this._getCallForRoom();

    if (!call) {
      return;
    }

    const newState = !call.isLocalVideoMuted();
    call.setLocalVideoMuted(newState);
    this.forceUpdate(); // TODO: just update the voip buttons
  },
  onStatusBarVisible: function () {
    if (this.unmounted) return;
    this.setState({
      statusBarVisible: true
    });
  },
  onStatusBarHidden: function () {
    // This is currently not desired as it is annoying if it keeps expanding and collapsing
    if (this.unmounted) return;
    this.setState({
      statusBarVisible: false
    });
  },

  /**
   * called by the parent component when PageUp/Down/etc is pressed.
   *
   * We pass it down to the scroll panel.
   */
  handleScrollKey: function (ev) {
    let panel;

    if (this._searchResultsPanel.current) {
      panel = this._searchResultsPanel.current;
    } else if (this._messagePanel) {
      panel = this._messagePanel;
    }

    if (panel) {
      panel.handleScrollKey(ev);
    }
  },

  /**
   * get any current call for this room
   */
  _getCallForRoom: function () {
    if (!this.state.room) {
      return null;
    }

    return _CallHandler.default.getCallForRoom(this.state.room.roomId);
  },
  // this has to be a proper method rather than an unnamed function,
  // otherwise react calls it with null on each update.
  _gatherTimelinePanelRef: function (r) {
    this._messagePanel = r;

    if (r) {
      console.log("updateTint from RoomView._gatherTimelinePanelRef");
      this.updateTint();
    }
  },
  _getOldRoom: function () {
    const createEvent = this.state.room.currentState.getStateEvents("m.room.create", "");
    if (!createEvent || !createEvent.getContent()['predecessor']) return null;
    return this.context.getRoom(createEvent.getContent()['predecessor']['room_id']);
  },
  _getHiddenHighlightCount: function () {
    const oldRoom = this._getOldRoom();

    if (!oldRoom) return 0;
    return oldRoom.getUnreadNotificationCount('highlight');
  },
  _onHiddenHighlightsClick: function () {
    const oldRoom = this._getOldRoom();

    if (!oldRoom) return;

    _dispatcher.default.dispatch({
      action: "view_room",
      room_id: oldRoom.roomId
    });
  },
  render: function () {
    const RoomHeader = sdk.getComponent('rooms.RoomHeader');
    const ForwardMessage = sdk.getComponent("rooms.ForwardMessage");
    const AuxPanel = sdk.getComponent("rooms.AuxPanel");
    const SearchBar = sdk.getComponent("rooms.SearchBar");
    const PinnedEventsPanel = sdk.getComponent("rooms.PinnedEventsPanel");
    const ScrollPanel = sdk.getComponent("structures.ScrollPanel");
    const TintableSvg = sdk.getComponent("elements.TintableSvg");
    const RoomPreviewBar = sdk.getComponent("rooms.RoomPreviewBar");
    const TimelinePanel = sdk.getComponent("structures.TimelinePanel");
    const RoomUpgradeWarningBar = sdk.getComponent("rooms.RoomUpgradeWarningBar");
    const RoomRecoveryReminder = sdk.getComponent("rooms.RoomRecoveryReminder");
    const ErrorBoundary = sdk.getComponent("elements.ErrorBoundary");

    if (!this.state.room) {
      const loading = this.state.roomLoading || this.state.peekLoading;

      if (loading) {
        return _react.default.createElement("div", {
          className: "mx_RoomView"
        }, _react.default.createElement(ErrorBoundary, null, _react.default.createElement(RoomPreviewBar, {
          canPreview: false,
          previewLoading: this.state.peekLoading,
          error: this.state.roomLoadError,
          loading: loading,
          joining: this.state.joining,
          oobData: this.props.oobData
        })));
      } else {
        var inviterName = undefined;

        if (this.props.oobData) {
          inviterName = this.props.oobData.inviterName;
        }

        var invitedEmail = undefined;

        if (this.props.thirdPartyInvite) {
          invitedEmail = this.props.thirdPartyInvite.invitedEmail;
        } // We have no room object for this room, only the ID.
        // We've got to this room by following a link, possibly a third party invite.


        const roomAlias = this.state.roomAlias;
        return _react.default.createElement("div", {
          className: "mx_RoomView"
        }, _react.default.createElement(ErrorBoundary, null, _react.default.createElement(RoomPreviewBar, {
          onJoinClick: this.onJoinButtonClicked,
          onForgetClick: this.onForgetClick,
          onRejectClick: this.onRejectThreepidInviteButtonClicked,
          canPreview: false,
          error: this.state.roomLoadError,
          roomAlias: roomAlias,
          joining: this.state.joining,
          inviterName: inviterName,
          invitedEmail: invitedEmail,
          oobData: this.props.oobData,
          signUrl: this.props.thirdPartyInvite ? this.props.thirdPartyInvite.inviteSignUrl : null,
          room: this.state.room
        })));
      }
    }

    const myMembership = this.state.room.getMyMembership();

    if (myMembership == 'invite') {
      if (this.state.joining || this.state.rejecting) {
        return _react.default.createElement(ErrorBoundary, null, _react.default.createElement(RoomPreviewBar, {
          canPreview: false,
          error: this.state.roomLoadError,
          joining: this.state.joining,
          rejecting: this.state.rejecting
        }));
      } else {
        const myUserId = this.context.credentials.userId;
        const myMember = this.state.room.getMember(myUserId);
        const inviteEvent = myMember.events.member;
        var inviterName = inviteEvent.sender ? inviteEvent.sender.name : inviteEvent.getSender(); // We deliberately don't try to peek into invites, even if we have permission to peek
        // as they could be a spam vector.
        // XXX: in future we could give the option of a 'Preview' button which lets them view anyway.
        // We have a regular invite for this room.

        return _react.default.createElement("div", {
          className: "mx_RoomView"
        }, _react.default.createElement(ErrorBoundary, null, _react.default.createElement(RoomPreviewBar, {
          onJoinClick: this.onJoinButtonClicked,
          onForgetClick: this.onForgetClick,
          onRejectClick: this.onRejectButtonClicked,
          onRejectAndIgnoreClick: this.onRejectAndIgnoreClick,
          inviterName: inviterName,
          canPreview: false,
          joining: this.state.joining,
          room: this.state.room
        })));
      }
    } // We have successfully loaded this room, and are not previewing.
    // Display the "normal" room view.


    const call = this._getCallForRoom();

    let inCall = false;

    if (call && this.state.callState !== 'ended' && this.state.callState !== 'ringing') {
      inCall = true;
    }

    const scrollheader_classes = (0, _classnames.default)({
      mx_RoomView_scrollheader: true
    });
    let statusBar;
    let isStatusAreaExpanded = true;

    if (_ContentMessages.default.sharedInstance().getCurrentUploads().length > 0) {
      const UploadBar = sdk.getComponent('structures.UploadBar');
      statusBar = _react.default.createElement(UploadBar, {
        room: this.state.room
      });
    } else if (!this.state.searchResults) {
      const RoomStatusBar = sdk.getComponent('structures.RoomStatusBar');
      isStatusAreaExpanded = this.state.statusBarVisible;
      statusBar = _react.default.createElement(RoomStatusBar, {
        room: this.state.room,
        sentMessageAndIsAlone: this.state.isAlone,
        hasActiveCall: inCall,
        isPeeking: myMembership !== "join",
        onInviteClick: this.onInviteButtonClick,
        onStopWarningClick: this.onStopAloneWarningClick,
        onVisible: this.onStatusBarVisible,
        onHidden: this.onStatusBarHidden
      });
    }

    const roomVersionRecommendation = this.state.upgradeRecommendation;
    const showRoomUpgradeBar = roomVersionRecommendation && roomVersionRecommendation.needsUpgrade && this.state.room.userMayUpgradeRoom(this.context.credentials.userId);
    const showRoomRecoveryReminder = _SettingsStore.default.getValue("showRoomRecoveryReminder") && this.context.isRoomEncrypted(this.state.room.roomId) && !this.context.getKeyBackupEnabled();

    const hiddenHighlightCount = this._getHiddenHighlightCount();

    let aux = null;
    let previewBar;
    let hideCancel = false;
    let forceHideRightPanel = false;

    if (this.state.forwardingEvent !== null) {
      aux = _react.default.createElement(ForwardMessage, {
        onCancelClick: this.onCancelClick
      });
    } else if (this.state.searching) {
      hideCancel = true; // has own cancel

      aux = _react.default.createElement(SearchBar, {
        searchInProgress: this.state.searchInProgress,
        onCancelClick: this.onCancelSearchClick,
        onSearch: this.onSearch
      });
    } else if (showRoomUpgradeBar) {
      aux = _react.default.createElement(RoomUpgradeWarningBar, {
        room: this.state.room,
        recommendation: roomVersionRecommendation
      });
      hideCancel = true;
    } else if (showRoomRecoveryReminder) {
      aux = _react.default.createElement(RoomRecoveryReminder, {
        onDontAskAgainSet: this.onRoomRecoveryReminderDontAskAgain
      });
      hideCancel = true;
    } else if (this.state.showingPinned) {
      hideCancel = true; // has own cancel

      aux = _react.default.createElement(PinnedEventsPanel, {
        room: this.state.room,
        onCancelClick: this.onPinnedClick
      });
    } else if (myMembership !== "join") {
      // We do have a room object for this room, but we're not currently in it.
      // We may have a 3rd party invite to it.
      var inviterName = undefined;

      if (this.props.oobData) {
        inviterName = this.props.oobData.inviterName;
      }

      var invitedEmail = undefined;

      if (this.props.thirdPartyInvite) {
        invitedEmail = this.props.thirdPartyInvite.invitedEmail;
      }

      hideCancel = true;
      previewBar = _react.default.createElement(RoomPreviewBar, {
        onJoinClick: this.onJoinButtonClicked,
        onForgetClick: this.onForgetClick,
        onRejectClick: this.onRejectThreepidInviteButtonClicked,
        joining: this.state.joining,
        inviterName: inviterName,
        invitedEmail: invitedEmail,
        oobData: this.props.oobData,
        canPreview: this.state.canPeek,
        room: this.state.room
      });

      if (!this.state.canPeek) {
        return _react.default.createElement("div", {
          className: "mx_RoomView"
        }, previewBar);
      } else {
        forceHideRightPanel = true;
      }
    } else if (hiddenHighlightCount > 0) {
      aux = _react.default.createElement(_AccessibleButton.default, {
        element: "div",
        className: "mx_RoomView_auxPanel_hiddenHighlights",
        onClick: this._onHiddenHighlightsClick
      }, (0, _languageHandler._t)("You have %(count)s unread notifications in a prior version of this room.", {
        count: hiddenHighlightCount
      }));
    }

    const auxPanel = _react.default.createElement(AuxPanel, {
      room: this.state.room,
      fullHeight: false,
      userId: this.context.credentials.userId,
      conferenceHandler: this.props.ConferenceHandler,
      draggingFile: this.state.draggingFile,
      displayConfCallNotification: this.state.displayConfCallNotification,
      maxHeight: this.state.auxPanelMaxHeight,
      showApps: this.state.showApps,
      hideAppsDrawer: false
    }, aux);

    let messageComposer;
    let searchInfo;
    const canSpeak = // joined and not showing search results
    myMembership === 'join' && !this.state.searchResults;

    if (canSpeak) {
      const MessageComposer = sdk.getComponent('rooms.MessageComposer');
      messageComposer = _react.default.createElement(MessageComposer, {
        room: this.state.room,
        callState: this.state.callState,
        disabled: this.props.disabled,
        showApps: this.state.showApps,
        e2eStatus: this.state.e2eStatus,
        permalinkCreator: this._getPermalinkCreatorForRoom(this.state.room)
      });
    } // TODO: Why aren't we storing the term/scope/count in this format
    // in this.state if this is what RoomHeader desires?


    if (this.state.searchResults) {
      searchInfo = {
        searchTerm: this.state.searchTerm,
        searchScope: this.state.searchScope,
        searchCount: this.state.searchResults.count
      };
    }

    if (inCall) {
      let zoomButton;
      let voiceMuteButton;
      let videoMuteButton;

      if (call.type === "video") {
        zoomButton = _react.default.createElement("div", {
          className: "mx_RoomView_voipButton",
          onClick: this.onFullscreenClick,
          title: (0, _languageHandler._t)("Fill screen")
        }, _react.default.createElement(TintableSvg, {
          src: require("../../../res/img/fullscreen.svg"),
          width: "29",
          height: "22",
          style: {
            marginTop: 1,
            marginRight: 4
          }
        }));
        videoMuteButton = _react.default.createElement("div", {
          className: "mx_RoomView_voipButton",
          onClick: this.onMuteVideoClick
        }, _react.default.createElement(TintableSvg, {
          src: call.isLocalVideoMuted() ? require("../../../res/img/video-unmute.svg") : require("../../../res/img/video-mute.svg"),
          alt: call.isLocalVideoMuted() ? (0, _languageHandler._t)("Click to unmute video") : (0, _languageHandler._t)("Click to mute video"),
          width: "31",
          height: "27"
        }));
      }

      voiceMuteButton = _react.default.createElement("div", {
        className: "mx_RoomView_voipButton",
        onClick: this.onMuteAudioClick
      }, _react.default.createElement(TintableSvg, {
        src: call.isMicrophoneMuted() ? require("../../../res/img/voice-unmute.svg") : require("../../../res/img/voice-mute.svg"),
        alt: call.isMicrophoneMuted() ? (0, _languageHandler._t)("Click to unmute audio") : (0, _languageHandler._t)("Click to mute audio"),
        width: "21",
        height: "26"
      })); // wrap the existing status bar into a 'callStatusBar' which adds more knobs.

      statusBar = _react.default.createElement("div", {
        className: "mx_RoomView_callStatusBar"
      }, voiceMuteButton, videoMuteButton, zoomButton, statusBar, _react.default.createElement(TintableSvg, {
        className: "mx_RoomView_voipChevron",
        src: require("../../../res/img/voip-chevron.svg"),
        width: "22",
        height: "17"
      }));
    } // if we have search results, we keep the messagepanel (so that it preserves its
    // scroll state), but hide it.


    let searchResultsPanel;
    let hideMessagePanel = false;

    if (this.state.searchResults) {
      // show searching spinner
      if (this.state.searchResults.results === undefined) {
        searchResultsPanel = _react.default.createElement("div", {
          className: "mx_RoomView_messagePanel mx_RoomView_messagePanelSearchSpinner"
        });
      } else {
        searchResultsPanel = _react.default.createElement(ScrollPanel, {
          ref: this._searchResultsPanel,
          className: "mx_RoomView_messagePanel mx_RoomView_searchResultsPanel",
          onFillRequest: this.onSearchResultsFillRequest,
          resizeNotifier: this.props.resizeNotifier
        }, _react.default.createElement("li", {
          className: scrollheader_classes
        }), this.getSearchResultTiles());
      }

      hideMessagePanel = true;
    }

    const shouldHighlight = this.state.isInitialEventHighlighted;
    let highlightedEventId = null;

    if (this.state.forwardingEvent) {
      highlightedEventId = this.state.forwardingEvent.getId();
    } else if (shouldHighlight) {
      highlightedEventId = this.state.initialEventId;
    } // console.info("ShowUrlPreview for %s is %s", this.state.room.roomId, this.state.showUrlPreview);


    const messagePanel = _react.default.createElement(TimelinePanel, {
      ref: this._gatherTimelinePanelRef,
      timelineSet: this.state.room.getUnfilteredTimelineSet(),
      showReadReceipts: this.state.showReadReceipts,
      manageReadReceipts: !this.state.isPeeking,
      manageReadMarkers: !this.state.isPeeking,
      hidden: hideMessagePanel,
      highlightedEventId: highlightedEventId,
      eventId: this.state.initialEventId,
      eventPixelOffset: this.state.initialEventPixelOffset,
      onScroll: this.onMessageListScroll,
      onReadMarkerUpdated: this._updateTopUnreadMessagesBar,
      showUrlPreview: this.state.showUrlPreview,
      className: "mx_RoomView_messagePanel",
      membersLoaded: this.state.membersLoaded,
      permalinkCreator: this._getPermalinkCreatorForRoom(this.state.room),
      resizeNotifier: this.props.resizeNotifier,
      showReactions: true
    });

    let topUnreadMessagesBar = null; // Do not show TopUnreadMessagesBar if we have search results showing, it makes no sense

    if (this.state.showTopUnreadMessagesBar && !this.state.searchResults) {
      const TopUnreadMessagesBar = sdk.getComponent('rooms.TopUnreadMessagesBar');
      topUnreadMessagesBar = _react.default.createElement(TopUnreadMessagesBar, {
        onScrollUpClick: this.jumpToReadMarker,
        onCloseClick: this.forgetReadMarker
      });
    }

    let jumpToBottom; // Do not show JumpToBottomButton if we have search results showing, it makes no sense

    if (!this.state.atEndOfLiveTimeline && !this.state.searchResults) {
      const JumpToBottomButton = sdk.getComponent('rooms.JumpToBottomButton');
      jumpToBottom = _react.default.createElement(JumpToBottomButton, {
        numUnreadMessages: this.state.numUnreadMessages,
        onScrollToBottomClick: this.jumpToLiveTimeline
      });
    }

    const statusBarAreaClass = (0, _classnames.default)("mx_RoomView_statusArea", {
      "mx_RoomView_statusArea_expanded": isStatusAreaExpanded
    });
    const fadableSectionClasses = (0, _classnames.default)("mx_RoomView_body", "mx_fadable", {
      "mx_fadable_faded": this.props.disabled
    });
    const showRightPanel = !forceHideRightPanel && this.state.room && this.state.showRightPanel;
    const rightPanel = showRightPanel ? _react.default.createElement(_RightPanel.default, {
      roomId: this.state.room.roomId,
      resizeNotifier: this.props.resizeNotifier
    }) : null;
    const timelineClasses = (0, _classnames.default)("mx_RoomView_timeline", {
      mx_RoomView_timeline_rr_enabled: this.state.showReadReceipts
    });
    return _react.default.createElement(_RoomContext.default.Provider, {
      value: this.state
    }, _react.default.createElement("main", {
      className: "mx_RoomView" + (inCall ? " mx_RoomView_inCall" : ""),
      ref: this._roomView
    }, _react.default.createElement(ErrorBoundary, null, _react.default.createElement(RoomHeader, {
      room: this.state.room,
      searchInfo: searchInfo,
      oobData: this.props.oobData,
      inRoom: myMembership === 'join',
      onSearchClick: this.onSearchClick,
      onSettingsClick: this.onSettingsClick,
      onPinnedClick: this.onPinnedClick,
      onCancelClick: aux && !hideCancel ? this.onCancelClick : null,
      onForgetClick: myMembership === "leave" ? this.onForgetClick : null,
      onLeaveClick: myMembership === "join" ? this.onLeaveClick : null,
      e2eStatus: this.state.e2eStatus
    }), _react.default.createElement(_MainSplit.default, {
      panel: rightPanel,
      resizeNotifier: this.props.resizeNotifier
    }, _react.default.createElement("div", {
      className: fadableSectionClasses
    }, auxPanel, _react.default.createElement("div", {
      className: timelineClasses
    }, topUnreadMessagesBar, jumpToBottom, messagePanel, searchResultsPanel), _react.default.createElement("div", {
      className: statusBarAreaClass
    }, _react.default.createElement("div", {
      className: "mx_RoomView_statusAreaBox"
    }, _react.default.createElement("div", {
      className: "mx_RoomView_statusAreaBox_line"
    }), statusBar)), previewBar, messageComposer)))));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvUm9vbVZpZXcuanMiXSwibmFtZXMiOlsiREVCVUciLCJkZWJ1Z2xvZyIsIkJST1dTRVJfU1VQUE9SVFNfU0FOREJPWCIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImNvbnNvbGUiLCJsb2ciLCJiaW5kIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJDb25mZXJlbmNlSGFuZGxlciIsIlByb3BUeXBlcyIsImFueSIsIm9uUmVnaXN0ZXJlZCIsImZ1bmMiLCJ0aGlyZFBhcnR5SW52aXRlIiwib2JqZWN0Iiwib29iRGF0YSIsInZpYVNlcnZlcnMiLCJhcnJheU9mIiwic3RyaW5nIiwic3RhdGljcyIsImNvbnRleHRUeXBlIiwiTWF0cml4Q2xpZW50Q29udGV4dCIsImdldEluaXRpYWxTdGF0ZSIsImxsTWVtYmVycyIsImNvbnRleHQiLCJoYXNMYXp5TG9hZE1lbWJlcnNFbmFibGVkIiwicm9vbSIsInJvb21JZCIsInJvb21Mb2FkaW5nIiwicGVla0xvYWRpbmciLCJzaG91bGRQZWVrIiwibWVkaWFDb25maWciLCJ1bmRlZmluZWQiLCJtZW1iZXJzTG9hZGVkIiwiaW5pdGlhbEV2ZW50SWQiLCJpbml0aWFsRXZlbnRQaXhlbE9mZnNldCIsImlzSW5pdGlhbEV2ZW50SGlnaGxpZ2h0ZWQiLCJmb3J3YXJkaW5nRXZlbnQiLCJudW1VbnJlYWRNZXNzYWdlcyIsImRyYWdnaW5nRmlsZSIsInNlYXJjaGluZyIsInNlYXJjaFJlc3VsdHMiLCJjYWxsU3RhdGUiLCJndWVzdHNDYW5Kb2luIiwiY2FuUGVlayIsInNob3dBcHBzIiwiaXNBbG9uZSIsImlzUGVla2luZyIsInNob3dpbmdQaW5uZWQiLCJzaG93UmVhZFJlY2VpcHRzIiwic2hvd1JpZ2h0UGFuZWwiLCJSaWdodFBhbmVsU3RvcmUiLCJnZXRTaGFyZWRJbnN0YW5jZSIsImlzT3BlbkZvclJvb20iLCJyb29tTG9hZEVycm9yIiwiam9pbmluZyIsImF0RW5kT2ZMaXZlVGltZWxpbmUiLCJhdEVuZE9mTGl2ZVRpbWVsaW5lSW5pdCIsInNob3dUb3BVbnJlYWRNZXNzYWdlc0JhciIsImF1eFBhbmVsTWF4SGVpZ2h0Iiwic3RhdHVzQmFyVmlzaWJsZSIsInVwZ3JhZGVSZWNvbW1lbmRhdGlvbiIsImNhblJlYWN0IiwiY2FuUmVwbHkiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJvbiIsIm9uUm9vbSIsIm9uUm9vbVRpbWVsaW5lIiwib25Sb29tTmFtZSIsIm9uUm9vbUFjY291bnREYXRhIiwib25Sb29tU3RhdGVFdmVudHMiLCJvblJvb21TdGF0ZU1lbWJlciIsIm9uTXlNZW1iZXJzaGlwIiwib25BY2NvdW50RGF0YSIsIm9uS2V5QmFja3VwU3RhdHVzIiwib25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkIiwib25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCIsIl9yb29tU3RvcmVUb2tlbiIsIlJvb21WaWV3U3RvcmUiLCJhZGRMaXN0ZW5lciIsIl9vblJvb21WaWV3U3RvcmVVcGRhdGUiLCJfcmlnaHRQYW5lbFN0b3JlVG9rZW4iLCJfb25SaWdodFBhbmVsU3RvcmVVcGRhdGUiLCJXaWRnZXRFY2hvU3RvcmUiLCJfb25XaWRnZXRFY2hvU3RvcmVVcGRhdGUiLCJfc2hvd1JlYWRSZWNlaXB0c1dhdGNoUmVmIiwiU2V0dGluZ3NTdG9yZSIsIndhdGNoU2V0dGluZyIsIl9vblJlYWRSZWNlaXB0c0NoYW5nZSIsIl9yb29tVmlldyIsIl9zZWFyY2hSZXN1bHRzUGFuZWwiLCJzZXRTdGF0ZSIsImdldFZhbHVlIiwic3RhdGUiLCJpbml0aWFsIiwidW5tb3VudGVkIiwiZ2V0Um9vbUlkIiwibmV3U3RhdGUiLCJyb29tQWxpYXMiLCJnZXRSb29tQWxpYXMiLCJpc1Jvb21Mb2FkaW5nIiwiZ2V0Um9vbUxvYWRFcnJvciIsImlzSm9pbmluZyIsImdldEluaXRpYWxFdmVudElkIiwiZ2V0Rm9yd2FyZGluZ0V2ZW50Iiwic3RvcFBlZWtpbmciLCJnZXRSb29tIiwiX3Nob3VsZFNob3dBcHBzIiwiX29uUm9vbUxvYWRlZCIsInJvb21TY3JvbGxTdGF0ZSIsIlJvb21TY3JvbGxTdGF0ZVN0b3JlIiwiZ2V0U2Nyb2xsU3RhdGUiLCJmb2N1c3NlZEV2ZW50IiwicGl4ZWxPZmZzZXQiLCJfc2V0dXBSb29tIiwiX2dldFJvb21JZCIsIl9nZXRQZXJtYWxpbmtDcmVhdG9yRm9yUm9vbSIsIl9wZXJtYWxpbmtDcmVhdG9ycyIsIlJvb21QZXJtYWxpbmtDcmVhdG9yIiwic3RhcnQiLCJsb2FkIiwiX3N0b3BBbGxQZXJtYWxpbmtDcmVhdG9ycyIsIk9iamVjdCIsImtleXMiLCJzdG9wIiwicHJvcHMiLCJhdXRvSm9pbiIsIm9uSm9pbkJ1dHRvbkNsaWNrZWQiLCJpbmZvIiwicGVla0luUm9vbSIsInRoZW4iLCJjYXRjaCIsImVyciIsImVycmNvZGUiLCJoaWRlV2lkZ2V0RHJhd2VyIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsIndpZGdldHMiLCJnZXRFY2hvZWRSb29tV2lkZ2V0cyIsIldpZGdldFV0aWxzIiwiZ2V0Um9vbVdpZGdldHMiLCJsZW5ndGgiLCJyb29tSGFzUGVuZGluZ1dpZGdldHMiLCJjb21wb25lbnREaWRNb3VudCIsImNhbGwiLCJfZ2V0Q2FsbEZvclJvb20iLCJjYWxsX3N0YXRlIiwiX3VwZGF0ZUNvbmZDYWxsTm90aWZpY2F0aW9uIiwid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciIsIm9uUGFnZVVubG9hZCIsInJlc2l6ZU5vdGlmaWVyIiwib25SZXNpemUiLCJvbktleURvd24iLCJzaG91bGRDb21wb25lbnRVcGRhdGUiLCJuZXh0UHJvcHMiLCJuZXh0U3RhdGUiLCJPYmplY3RVdGlscyIsInNoYWxsb3dFcXVhbCIsImNvbXBvbmVudERpZFVwZGF0ZSIsImN1cnJlbnQiLCJyb29tVmlldyIsIm9uZHJvcCIsIm9uRHJvcCIsIm9uRHJhZ092ZXIiLCJvbkRyYWdMZWF2ZU9yRW5kIiwiX21lc3NhZ2VQYW5lbCIsImlzQXRFbmRPZkxpdmVUaW1lbGluZSIsImNvbXBvbmVudFdpbGxVbm1vdW50Iiwic2V0U2Nyb2xsU3RhdGUiLCJfZ2V0U2Nyb2xsU3RhdGUiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwidW5yZWdpc3RlciIsInJlbW92ZUxpc3RlbmVyIiwicmVtb3ZlIiwidW53YXRjaFNldHRpbmciLCJfdXBkYXRlUm9vbU1lbWJlcnMiLCJjYW5jZWxQZW5kaW5nQ2FsbCIsImV2ZW50IiwiQ29udGVudE1lc3NhZ2VzIiwic2hhcmVkSW5zdGFuY2UiLCJnZXRDdXJyZW50VXBsb2FkcyIsInJldHVyblZhbHVlIiwiZXYiLCJoYW5kbGVkIiwiY3RybENtZE9ubHkiLCJrZXkiLCJLZXkiLCJEIiwib25NdXRlQXVkaW9DbGljayIsIkUiLCJvbk11dGVWaWRlb0NsaWNrIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJwYXlsb2FkIiwiYWN0aW9uIiwiX2NoZWNrSWZBbG9uZSIsImluamVjdFN0aWNrZXIiLCJkYXRhIiwiY29udGVudCIsInVybCIsImRlc2NyaXB0aW9uIiwibmFtZSIsInNlbmRDb250ZW50TGlzdFRvUm9vbSIsImZpbGUiLCJmb3JjZVVwZGF0ZSIsInJvb21faWQiLCJzaG93Iiwib25DYW5jZWxTZWFyY2hDbGljayIsInNldEltbWVkaWF0ZSIsImRpc3BhdGNoIiwiZGVmZXJyZWRfYWN0aW9uIiwidG9TdGFydE9mVGltZWxpbmUiLCJyZW1vdmVkIiwidGltZWxpbmUiLCJnZXRUaW1lbGluZVNldCIsImdldFVuZmlsdGVyZWRUaW1lbGluZVNldCIsImdldFR5cGUiLCJfdXBkYXRlUHJldmlld1VybFZpc2liaWxpdHkiLCJfdXBkYXRlRTJFU3RhdHVzIiwibGl2ZUV2ZW50IiwiZ2V0U2VuZGVyIiwiY3JlZGVudGlhbHMiLCJ1c2VySWQiLCJvblJvb21SZWNvdmVyeVJlbWluZGVyRG9udEFza0FnYWluIiwiY2FuUmVzZXRUaW1lbGluZSIsIl9jYWxjdWxhdGVQZWVrUnVsZXMiLCJfbG9hZE1lbWJlcnNJZkpvaW5lZCIsIl9jYWxjdWxhdGVSZWNvbW1lbmRlZFZlcnNpb24iLCJfdXBkYXRlUGVybWlzc2lvbnMiLCJnZXRSZWNvbW1lbmRlZFZlcnNpb24iLCJnZXRNeU1lbWJlcnNoaXAiLCJsb2FkTWVtYmVyc0lmTmVlZGVkIiwiZXJyb3JNZXNzYWdlIiwiZXJyb3IiLCJndWVzdEFjY2Vzc0V2ZW50IiwiY3VycmVudFN0YXRlIiwiZ2V0U3RhdGVFdmVudHMiLCJnZXRDb250ZW50IiwiZ3Vlc3RfYWNjZXNzIiwiaGlzdG9yeVZpc2liaWxpdHkiLCJoaXN0b3J5X3Zpc2liaWxpdHkiLCJpc1Jvb21FbmNyeXB0ZWQiLCJzaG93VXJsUHJldmlldyIsImRldmljZSIsImdldE1lbWJlciIsIl90cnVzdFN0YXR1cyIsImlzQ3J5cHRvRW5hYmxlZCIsImUyZVN0YXR1cyIsImlzRmVhdHVyZUVuYWJsZWQiLCJoYXNVbnZlcmlmaWVkRGV2aWNlcyIsInVwZGF0ZVRpbnQiLCJjb2xvclNjaGVtZSIsIlRpbnRlciIsInRpbnQiLCJwcmltYXJ5X2NvbG9yIiwic2Vjb25kYXJ5X2NvbG9yIiwidHlwZSIsIm1lbWJlciIsIm1lbWJlcnNoaXAiLCJvbGRNZW1iZXJzaGlwIiwibWUiLCJnZXRVc2VySWQiLCJtYXlTZW5kRXZlbnQiLCJtYXlTZW5kTWVzc2FnZSIsImR1ZVRvTWVtYmVyIiwiX3VwZGF0ZURNU3RhdGUiLCJtZW1iZXJDb3VudEluZmx1ZW5jZSIsImdldEludml0ZWRNZW1iZXJDb3VudCIsImNvdW50SW5mbHVlbmNlIiwid2FybmVkQWJvdXRMb25lbHlSb29tIiwiam9pbmVkT3JJbnZpdGVkTWVtYmVyQ291bnQiLCJnZXRKb2luZWRNZW1iZXJDb3VudCIsImNvbmZNZW1iZXIiLCJnZXRDb25mZXJlbmNlVXNlcklkRm9yUm9vbSIsImNvbmZDYWxsIiwiZ2V0Q29uZmVyZW5jZUNhbGxGb3JSb29tIiwiZGlzcGxheUNvbmZDYWxsTm90aWZpY2F0aW9uIiwiZG1JbnZpdGVyIiwiZ2V0RE1JbnZpdGVyIiwiUm9vbXMiLCJzZXRETVJvb20iLCJvblNlYXJjaFJlc3VsdHNGaWxsUmVxdWVzdCIsImJhY2t3YXJkcyIsIlByb21pc2UiLCJyZXNvbHZlIiwibmV4dF9iYXRjaCIsInNlYXJjaFByb21pc2UiLCJiYWNrUGFnaW5hdGVSb29tRXZlbnRzU2VhcmNoIiwiX2hhbmRsZVNlYXJjaFJlc3VsdCIsIm9uSW52aXRlQnV0dG9uQ2xpY2siLCJvblN0b3BBbG9uZVdhcm5pbmdDbGljayIsInNldEl0ZW0iLCJpc0d1ZXN0Iiwic2lnblVybCIsImludml0ZVNpZ25VcmwiLCJvcHRzIiwib25NZXNzYWdlTGlzdFNjcm9sbCIsIl91cGRhdGVUb3BVbnJlYWRNZXNzYWdlc0JhciIsImRhdGFUcmFuc2ZlciIsImRyb3BFZmZlY3QiLCJpdGVtcyIsImlzRHJhZ2dpbmdGaWxlcyIsImV2ZXJ5IiwiaXRlbSIsImtpbmQiLCJmaWxlcyIsInRleHQiLCJzZW5kU3RpY2tlckNvbnRlbnRUb1Jvb20iLCJvblNlYXJjaCIsInRlcm0iLCJzY29wZSIsInNlYXJjaFRlcm0iLCJzZWFyY2hTY29wZSIsInNlYXJjaEhpZ2hsaWdodHMiLCJyZXNldFNjcm9sbFN0YXRlIiwic2VhcmNoSWQiLCJEYXRlIiwiZ2V0VGltZSIsInNlbGYiLCJsb2NhbFNlYXJjaElkIiwic2VhcmNoSW5Qcm9ncmVzcyIsInJlc3VsdHMiLCJoaWdobGlnaHRzIiwiaW5kZXhPZiIsImNvbmNhdCIsInNvcnQiLCJhIiwiYiIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJtZXNzYWdlIiwiZmluYWxseSIsImdldFNlYXJjaFJlc3VsdFRpbGVzIiwiU2VhcmNoUmVzdWx0VGlsZSIsIlNwaW5uZXIiLCJyZXQiLCJwdXNoIiwib25IZWlnaHRDaGFuZ2VkIiwic2Nyb2xsUGFuZWwiLCJjaGVja1Njcm9sbCIsImxhc3RSb29tSWQiLCJpIiwicmVzdWx0IiwibXhFdiIsImdldEV2ZW50Iiwicm9vbU5hbWUiLCJnZXRJZCIsInJlc3VsdExpbmsiLCJvblBpbm5lZENsaWNrIiwibm93U2hvd2luZ1Bpbm5lZCIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiUk9PTV9ERVZJQ0UiLCJvblNldHRpbmdzQ2xpY2siLCJvbkNhbmNlbENsaWNrIiwib25MZWF2ZUNsaWNrIiwib25Gb3JnZXRDbGljayIsImZvcmdldCIsImVyckNvZGUiLCJvblJlamVjdEJ1dHRvbkNsaWNrZWQiLCJyZWplY3RpbmciLCJsZWF2ZSIsIm1zZyIsIkpTT04iLCJzdHJpbmdpZnkiLCJyZWplY3RFcnJvciIsIm9uUmVqZWN0QW5kSWdub3JlQ2xpY2siLCJteU1lbWJlciIsImludml0ZUV2ZW50IiwiZXZlbnRzIiwiaWdub3JlZFVzZXJzIiwiZ2V0SWdub3JlZFVzZXJzIiwic2V0SWdub3JlZFVzZXJzIiwib25SZWplY3RUaHJlZXBpZEludml0ZUJ1dHRvbkNsaWNrZWQiLCJvblNlYXJjaENsaWNrIiwianVtcFRvTGl2ZVRpbWVsaW5lIiwianVtcFRvUmVhZE1hcmtlciIsImZvcmdldFJlYWRNYXJrZXIiLCJzaG93QmFyIiwiY2FuSnVtcFRvUmVhZE1hcmtlciIsIm1lc3NhZ2VQYW5lbCIsInNjcm9sbFN0YXRlIiwic3R1Y2tBdEJvdHRvbSIsInRyYWNrZWRTY3JvbGxUb2tlbiIsImlubmVySGVpZ2h0Iiwib25GdWxsc2NyZWVuQ2xpY2siLCJmdWxsc2NyZWVuIiwiaXNNaWNyb3Bob25lTXV0ZWQiLCJzZXRNaWNyb3Bob25lTXV0ZWQiLCJpc0xvY2FsVmlkZW9NdXRlZCIsInNldExvY2FsVmlkZW9NdXRlZCIsIm9uU3RhdHVzQmFyVmlzaWJsZSIsIm9uU3RhdHVzQmFySGlkZGVuIiwiaGFuZGxlU2Nyb2xsS2V5IiwicGFuZWwiLCJDYWxsSGFuZGxlciIsImdldENhbGxGb3JSb29tIiwiX2dhdGhlclRpbWVsaW5lUGFuZWxSZWYiLCJyIiwiX2dldE9sZFJvb20iLCJjcmVhdGVFdmVudCIsIl9nZXRIaWRkZW5IaWdobGlnaHRDb3VudCIsIm9sZFJvb20iLCJnZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCIsIl9vbkhpZGRlbkhpZ2hsaWdodHNDbGljayIsInJlbmRlciIsIlJvb21IZWFkZXIiLCJGb3J3YXJkTWVzc2FnZSIsIkF1eFBhbmVsIiwiU2VhcmNoQmFyIiwiUGlubmVkRXZlbnRzUGFuZWwiLCJTY3JvbGxQYW5lbCIsIlRpbnRhYmxlU3ZnIiwiUm9vbVByZXZpZXdCYXIiLCJUaW1lbGluZVBhbmVsIiwiUm9vbVVwZ3JhZGVXYXJuaW5nQmFyIiwiUm9vbVJlY292ZXJ5UmVtaW5kZXIiLCJFcnJvckJvdW5kYXJ5IiwibG9hZGluZyIsImludml0ZXJOYW1lIiwiaW52aXRlZEVtYWlsIiwibXlNZW1iZXJzaGlwIiwibXlVc2VySWQiLCJzZW5kZXIiLCJpbkNhbGwiLCJzY3JvbGxoZWFkZXJfY2xhc3NlcyIsIm14X1Jvb21WaWV3X3Njcm9sbGhlYWRlciIsInN0YXR1c0JhciIsImlzU3RhdHVzQXJlYUV4cGFuZGVkIiwiVXBsb2FkQmFyIiwiUm9vbVN0YXR1c0JhciIsInJvb21WZXJzaW9uUmVjb21tZW5kYXRpb24iLCJzaG93Um9vbVVwZ3JhZGVCYXIiLCJuZWVkc1VwZ3JhZGUiLCJ1c2VyTWF5VXBncmFkZVJvb20iLCJzaG93Um9vbVJlY292ZXJ5UmVtaW5kZXIiLCJnZXRLZXlCYWNrdXBFbmFibGVkIiwiaGlkZGVuSGlnaGxpZ2h0Q291bnQiLCJhdXgiLCJwcmV2aWV3QmFyIiwiaGlkZUNhbmNlbCIsImZvcmNlSGlkZVJpZ2h0UGFuZWwiLCJjb3VudCIsImF1eFBhbmVsIiwibWVzc2FnZUNvbXBvc2VyIiwic2VhcmNoSW5mbyIsImNhblNwZWFrIiwiTWVzc2FnZUNvbXBvc2VyIiwiZGlzYWJsZWQiLCJzZWFyY2hDb3VudCIsInpvb21CdXR0b24iLCJ2b2ljZU11dGVCdXR0b24iLCJ2aWRlb011dGVCdXR0b24iLCJyZXF1aXJlIiwibWFyZ2luVG9wIiwibWFyZ2luUmlnaHQiLCJzZWFyY2hSZXN1bHRzUGFuZWwiLCJoaWRlTWVzc2FnZVBhbmVsIiwic2hvdWxkSGlnaGxpZ2h0IiwiaGlnaGxpZ2h0ZWRFdmVudElkIiwidG9wVW5yZWFkTWVzc2FnZXNCYXIiLCJUb3BVbnJlYWRNZXNzYWdlc0JhciIsImp1bXBUb0JvdHRvbSIsIkp1bXBUb0JvdHRvbUJ1dHRvbiIsInN0YXR1c0JhckFyZWFDbGFzcyIsImZhZGFibGVTZWN0aW9uQ2xhc3NlcyIsInJpZ2h0UGFuZWwiLCJ0aW1lbGluZUNsYXNzZXMiLCJteF9Sb29tVmlld190aW1lbGluZV9ycl9lbmFibGVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQXVCQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF6REE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFzQ0EsTUFBTUEsS0FBSyxHQUFHLEtBQWQ7O0FBQ0EsSUFBSUMsUUFBUSxHQUFHLFlBQVcsQ0FBRSxDQUE1Qjs7QUFFQSxNQUFNQyx3QkFBd0IsR0FBRyxhQUFhQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBOUM7O0FBRUEsSUFBSUosS0FBSixFQUFXO0FBQ1A7QUFDQUMsRUFBQUEsUUFBUSxHQUFHSSxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsSUFBWixDQUFpQkYsT0FBakIsQ0FBWDtBQUNIOztlQUVjLCtCQUFpQjtBQUM1QkcsRUFBQUEsV0FBVyxFQUFFLFVBRGU7QUFFNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxpQkFBaUIsRUFBRUMsbUJBQVVDLEdBRHRCO0FBR1A7QUFDQTtBQUNBQyxJQUFBQSxZQUFZLEVBQUVGLG1CQUFVRyxJQUxqQjtBQU9QO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsSUFBQUEsZ0JBQWdCLEVBQUVKLG1CQUFVSyxNQVpyQjtBQWNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxJQUFBQSxPQUFPLEVBQUVOLG1CQUFVSyxNQXZCWjtBQXlCUDtBQUNBRSxJQUFBQSxVQUFVLEVBQUVQLG1CQUFVUSxPQUFWLENBQWtCUixtQkFBVVMsTUFBNUI7QUExQkwsR0FGaUI7QUErQjVCQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsV0FBVyxFQUFFQztBQURSLEdBL0JtQjtBQW1DNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFVBQU1DLFNBQVMsR0FBRyxLQUFLQyxPQUFMLENBQWFDLHlCQUFiLEVBQWxCO0FBQ0EsV0FBTztBQUNIQyxNQUFBQSxJQUFJLEVBQUUsSUFESDtBQUVIQyxNQUFBQSxNQUFNLEVBQUUsSUFGTDtBQUdIQyxNQUFBQSxXQUFXLEVBQUUsSUFIVjtBQUlIQyxNQUFBQSxXQUFXLEVBQUUsS0FKVjtBQUtIQyxNQUFBQSxVQUFVLEVBQUUsSUFMVDtBQU9IO0FBQ0FDLE1BQUFBLFdBQVcsRUFBRUMsU0FSVjtBQVVIO0FBQ0E7QUFDQUMsTUFBQUEsYUFBYSxFQUFFLENBQUNWLFNBWmI7QUFhSDtBQUNBVyxNQUFBQSxjQUFjLEVBQUUsSUFkYjtBQWVIO0FBQ0FDLE1BQUFBLHVCQUF1QixFQUFFLElBaEJ0QjtBQWlCSDtBQUNBQyxNQUFBQSx5QkFBeUIsRUFBRSxJQWxCeEI7QUFvQkhDLE1BQUFBLGVBQWUsRUFBRSxJQXBCZDtBQXFCSEMsTUFBQUEsaUJBQWlCLEVBQUUsQ0FyQmhCO0FBc0JIQyxNQUFBQSxZQUFZLEVBQUUsS0F0Qlg7QUF1QkhDLE1BQUFBLFNBQVMsRUFBRSxLQXZCUjtBQXdCSEMsTUFBQUEsYUFBYSxFQUFFLElBeEJaO0FBeUJIQyxNQUFBQSxTQUFTLEVBQUUsSUF6QlI7QUEwQkhDLE1BQUFBLGFBQWEsRUFBRSxLQTFCWjtBQTJCSEMsTUFBQUEsT0FBTyxFQUFFLEtBM0JOO0FBNEJIQyxNQUFBQSxRQUFRLEVBQUUsS0E1QlA7QUE2QkhDLE1BQUFBLE9BQU8sRUFBRSxLQTdCTjtBQThCSEMsTUFBQUEsU0FBUyxFQUFFLEtBOUJSO0FBK0JIQyxNQUFBQSxhQUFhLEVBQUUsS0EvQlo7QUFnQ0hDLE1BQUFBLGdCQUFnQixFQUFFLElBaENmO0FBaUNIQyxNQUFBQSxjQUFjLEVBQUVDLHlCQUFnQkMsaUJBQWhCLEdBQW9DQyxhQWpDakQ7QUFtQ0g7QUFDQTtBQUNBO0FBQ0FDLE1BQUFBLGFBQWEsRUFBRSxJQXRDWjtBQXdDSDtBQUNBQyxNQUFBQSxPQUFPLEVBQUUsS0F6Q047QUEyQ0g7QUFDQTtBQUNBO0FBQ0FDLE1BQUFBLG1CQUFtQixFQUFFLElBOUNsQjtBQStDSEMsTUFBQUEsdUJBQXVCLEVBQUUsS0EvQ3RCO0FBK0M2QjtBQUVoQ0MsTUFBQUEsd0JBQXdCLEVBQUUsS0FqRHZCO0FBbURIQyxNQUFBQSxpQkFBaUIsRUFBRTNCLFNBbkRoQjtBQXFESDRCLE1BQUFBLGdCQUFnQixFQUFFLEtBckRmO0FBdURIO0FBQ0E7QUFDQUMsTUFBQUEscUJBQXFCLEVBQUUsSUF6RHBCO0FBMkRIQyxNQUFBQSxRQUFRLEVBQUUsS0EzRFA7QUE0REhDLE1BQUFBLFFBQVEsRUFBRTtBQTVEUCxLQUFQO0FBOERILEdBbkcyQjtBQXFHNUI7QUFDQUMsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLQyxhQUFMLEdBQXFCQyxvQkFBSUMsUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0EsU0FBSzVDLE9BQUwsQ0FBYTZDLEVBQWIsQ0FBZ0IsTUFBaEIsRUFBd0IsS0FBS0MsTUFBN0I7QUFDQSxTQUFLOUMsT0FBTCxDQUFhNkMsRUFBYixDQUFnQixlQUFoQixFQUFpQyxLQUFLRSxjQUF0QztBQUNBLFNBQUsvQyxPQUFMLENBQWE2QyxFQUFiLENBQWdCLFdBQWhCLEVBQTZCLEtBQUtHLFVBQWxDO0FBQ0EsU0FBS2hELE9BQUwsQ0FBYTZDLEVBQWIsQ0FBZ0Isa0JBQWhCLEVBQW9DLEtBQUtJLGlCQUF6QztBQUNBLFNBQUtqRCxPQUFMLENBQWE2QyxFQUFiLENBQWdCLGtCQUFoQixFQUFvQyxLQUFLSyxpQkFBekM7QUFDQSxTQUFLbEQsT0FBTCxDQUFhNkMsRUFBYixDQUFnQixtQkFBaEIsRUFBcUMsS0FBS00saUJBQTFDO0FBQ0EsU0FBS25ELE9BQUwsQ0FBYTZDLEVBQWIsQ0FBZ0IsbUJBQWhCLEVBQXFDLEtBQUtPLGNBQTFDO0FBQ0EsU0FBS3BELE9BQUwsQ0FBYTZDLEVBQWIsQ0FBZ0IsYUFBaEIsRUFBK0IsS0FBS1EsYUFBcEM7QUFDQSxTQUFLckQsT0FBTCxDQUFhNkMsRUFBYixDQUFnQix3QkFBaEIsRUFBMEMsS0FBS1MsaUJBQS9DO0FBQ0EsU0FBS3RELE9BQUwsQ0FBYTZDLEVBQWIsQ0FBZ0IsMkJBQWhCLEVBQTZDLEtBQUtVLDJCQUFsRDtBQUNBLFNBQUt2RCxPQUFMLENBQWE2QyxFQUFiLENBQWdCLHdCQUFoQixFQUEwQyxLQUFLVyx5QkFBL0MsRUFaa0MsQ0FhbEM7O0FBQ0EsU0FBS0MsZUFBTCxHQUF1QkMsdUJBQWNDLFdBQWQsQ0FBMEIsS0FBS0Msc0JBQS9CLENBQXZCO0FBQ0EsU0FBS0MscUJBQUwsR0FBNkJsQyx5QkFBZ0JDLGlCQUFoQixHQUFvQytCLFdBQXBDLENBQWdELEtBQUtHLHdCQUFyRCxDQUE3Qjs7QUFDQSxTQUFLRixzQkFBTCxDQUE0QixJQUE1Qjs7QUFFQUcsNkJBQWdCbEIsRUFBaEIsQ0FBbUIsUUFBbkIsRUFBNkIsS0FBS21CLHdCQUFsQzs7QUFDQSxTQUFLQyx5QkFBTCxHQUFpQ0MsdUJBQWNDLFlBQWQsQ0FBMkIsa0JBQTNCLEVBQStDLElBQS9DLEVBQzdCLEtBQUtDLHFCQUR3QixDQUFqQztBQUdBLFNBQUtDLFNBQUwsR0FBaUIsdUJBQWpCO0FBQ0EsU0FBS0MsbUJBQUwsR0FBMkIsdUJBQTNCO0FBQ0gsR0E5SDJCO0FBZ0k1QkYsRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixTQUFLRyxRQUFMLENBQWM7QUFDVjlDLE1BQUFBLGdCQUFnQixFQUFFeUMsdUJBQWNNLFFBQWQsQ0FBdUIsa0JBQXZCLEVBQTJDLEtBQUtDLEtBQUwsQ0FBV3RFLE1BQXREO0FBRFIsS0FBZDtBQUdILEdBcEkyQjtBQXNJNUJ5RCxFQUFBQSxzQkFBc0IsRUFBRSxVQUFTYyxPQUFULEVBQWtCO0FBQ3RDLFFBQUksS0FBS0MsU0FBVCxFQUFvQjtBQUNoQjtBQUNIOztBQUVELFFBQUksQ0FBQ0QsT0FBRCxJQUFZLEtBQUtELEtBQUwsQ0FBV3RFLE1BQVgsS0FBc0J1RCx1QkFBY2tCLFNBQWQsRUFBdEMsRUFBaUU7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNIOztBQUVELFVBQU16RSxNQUFNLEdBQUd1RCx1QkFBY2tCLFNBQWQsRUFBZjs7QUFFQSxVQUFNQyxRQUFRLEdBQUc7QUFDYjFFLE1BQUFBLE1BRGE7QUFFYjJFLE1BQUFBLFNBQVMsRUFBRXBCLHVCQUFjcUIsWUFBZCxFQUZFO0FBR2IzRSxNQUFBQSxXQUFXLEVBQUVzRCx1QkFBY3NCLGFBQWQsRUFIQTtBQUlibEQsTUFBQUEsYUFBYSxFQUFFNEIsdUJBQWN1QixnQkFBZCxFQUpGO0FBS2JsRCxNQUFBQSxPQUFPLEVBQUUyQix1QkFBY3dCLFNBQWQsRUFMSTtBQU1ieEUsTUFBQUEsY0FBYyxFQUFFZ0QsdUJBQWN5QixpQkFBZCxFQU5IO0FBT2J2RSxNQUFBQSx5QkFBeUIsRUFBRThDLHVCQUFjOUMseUJBQWQsRUFQZDtBQVFiQyxNQUFBQSxlQUFlLEVBQUU2Qyx1QkFBYzBCLGtCQUFkLEVBUko7QUFTYjlFLE1BQUFBLFVBQVUsRUFBRW9ELHVCQUFjcEQsVUFBZCxFQVRDO0FBVWJrQixNQUFBQSxhQUFhLEVBQUUwQyx1QkFBY00sUUFBZCxDQUF1QixxQkFBdkIsRUFBOENyRSxNQUE5QyxDQVZGO0FBV2JzQixNQUFBQSxnQkFBZ0IsRUFBRXlDLHVCQUFjTSxRQUFkLENBQXVCLGtCQUF2QixFQUEyQ3JFLE1BQTNDO0FBWEwsS0FBakI7O0FBY0EsUUFBSSxDQUFDdUUsT0FBRCxJQUFZLEtBQUtELEtBQUwsQ0FBV25FLFVBQXZCLElBQXFDLENBQUN1RSxRQUFRLENBQUN2RSxVQUFuRCxFQUErRDtBQUMzRDtBQUNBLFdBQUtOLE9BQUwsQ0FBYXFGLFdBQWI7QUFDSCxLQXZDcUMsQ0F5Q3RDOzs7QUFDQTFHLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUNJLGFBREosRUFFSWlHLFFBQVEsQ0FBQzFFLE1BRmIsRUFHSTBFLFFBQVEsQ0FBQ0MsU0FIYixFQUlJLFVBSkosRUFJZ0JELFFBQVEsQ0FBQ3pFLFdBSnpCLEVBS0ksVUFMSixFQUtnQnlFLFFBQVEsQ0FBQzlDLE9BTHpCLEVBTUksVUFOSixFQU1nQjJDLE9BTmhCLEVBT0ksYUFQSixFQU9tQkcsUUFBUSxDQUFDdkUsVUFQNUIsRUExQ3NDLENBb0R0QztBQUNBOztBQUNBLFFBQUlvRSxPQUFKLEVBQWE7QUFDVEcsTUFBQUEsUUFBUSxDQUFDM0UsSUFBVCxHQUFnQixLQUFLRixPQUFMLENBQWFzRixPQUFiLENBQXFCVCxRQUFRLENBQUMxRSxNQUE5QixDQUFoQjs7QUFDQSxVQUFJMEUsUUFBUSxDQUFDM0UsSUFBYixFQUFtQjtBQUNmMkUsUUFBQUEsUUFBUSxDQUFDeEQsUUFBVCxHQUFvQixLQUFLa0UsZUFBTCxDQUFxQlYsUUFBUSxDQUFDM0UsSUFBOUIsQ0FBcEI7O0FBQ0EsYUFBS3NGLGFBQUwsQ0FBbUJYLFFBQVEsQ0FBQzNFLElBQTVCO0FBQ0g7QUFDSjs7QUFFRCxRQUFJLEtBQUt1RSxLQUFMLENBQVd0RSxNQUFYLEtBQXNCLElBQXRCLElBQThCMEUsUUFBUSxDQUFDMUUsTUFBVCxLQUFvQixJQUF0RCxFQUE0RDtBQUN4RDtBQUVBO0FBQ0E7QUFDQSxVQUFJLENBQUMwRSxRQUFRLENBQUNuRSxjQUFkLEVBQThCO0FBQzFCLGNBQU0rRSxlQUFlLEdBQUdDLDhCQUFxQkMsY0FBckIsQ0FBb0NkLFFBQVEsQ0FBQzFFLE1BQTdDLENBQXhCOztBQUNBLFlBQUlzRixlQUFKLEVBQXFCO0FBQ2pCWixVQUFBQSxRQUFRLENBQUNuRSxjQUFULEdBQTBCK0UsZUFBZSxDQUFDRyxhQUExQztBQUNBZixVQUFBQSxRQUFRLENBQUNsRSx1QkFBVCxHQUFtQzhFLGVBQWUsQ0FBQ0ksV0FBbkQ7QUFDSDtBQUNKO0FBQ0osS0ExRXFDLENBNEV0QztBQUNBOzs7QUFDQSxRQUFJLEtBQUtwQixLQUFMLENBQVcvRCxjQUFYLEtBQThCbUUsUUFBUSxDQUFDbkUsY0FBM0MsRUFBMkQ7QUFDdkRtRSxNQUFBQSxRQUFRLENBQUM1RCxhQUFULEdBQXlCLElBQXpCO0FBQ0g7O0FBRUQsU0FBS3NELFFBQUwsQ0FBY00sUUFBZCxFQWxGc0MsQ0FtRnRDO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFJSCxPQUFKLEVBQWE7QUFDVCxXQUFLb0IsVUFBTCxDQUFnQmpCLFFBQVEsQ0FBQzNFLElBQXpCLEVBQStCMkUsUUFBUSxDQUFDMUUsTUFBeEMsRUFBZ0QwRSxRQUFRLENBQUM5QyxPQUF6RCxFQUFrRThDLFFBQVEsQ0FBQ3ZFLFVBQTNFO0FBQ0g7QUFDSixHQW5PMkI7O0FBcU81QnlGLEVBQUFBLFVBQVUsR0FBRztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFPLEtBQUt0QixLQUFMLENBQVd2RSxJQUFYLEdBQWtCLEtBQUt1RSxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUFsQyxHQUEyQyxLQUFLc0UsS0FBTCxDQUFXdEUsTUFBN0Q7QUFDSCxHQTVPMkI7O0FBOE81QjZGLEVBQUFBLDJCQUEyQixFQUFFLFVBQVM5RixJQUFULEVBQWU7QUFDeEMsUUFBSSxDQUFDLEtBQUsrRixrQkFBVixFQUE4QixLQUFLQSxrQkFBTCxHQUEwQixFQUExQjtBQUM5QixRQUFJLEtBQUtBLGtCQUFMLENBQXdCL0YsSUFBSSxDQUFDQyxNQUE3QixDQUFKLEVBQTBDLE9BQU8sS0FBSzhGLGtCQUFMLENBQXdCL0YsSUFBSSxDQUFDQyxNQUE3QixDQUFQO0FBRTFDLFNBQUs4RixrQkFBTCxDQUF3Qi9GLElBQUksQ0FBQ0MsTUFBN0IsSUFBdUMsSUFBSStGLGdDQUFKLENBQXlCaEcsSUFBekIsQ0FBdkM7O0FBQ0EsUUFBSSxLQUFLdUUsS0FBTCxDQUFXdkUsSUFBWCxJQUFtQkEsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEtBQUtzRSxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUF2RCxFQUErRDtBQUMzRDtBQUNBO0FBQ0EsV0FBSzhGLGtCQUFMLENBQXdCL0YsSUFBSSxDQUFDQyxNQUE3QixFQUFxQ2dHLEtBQXJDO0FBQ0gsS0FKRCxNQUlPO0FBQ0gsV0FBS0Ysa0JBQUwsQ0FBd0IvRixJQUFJLENBQUNDLE1BQTdCLEVBQXFDaUcsSUFBckM7QUFDSDs7QUFDRCxXQUFPLEtBQUtILGtCQUFMLENBQXdCL0YsSUFBSSxDQUFDQyxNQUE3QixDQUFQO0FBQ0gsR0EzUDJCO0FBNlA1QmtHLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsUUFBSSxDQUFDLEtBQUtKLGtCQUFWLEVBQThCOztBQUM5QixTQUFLLE1BQU05RixNQUFYLElBQXFCbUcsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBS04sa0JBQWpCLENBQXJCLEVBQTJEO0FBQ3ZELFdBQUtBLGtCQUFMLENBQXdCOUYsTUFBeEIsRUFBZ0NxRyxJQUFoQztBQUNIO0FBQ0osR0FsUTJCO0FBb1E1QnhDLEVBQUFBLHdCQUF3QixFQUFFLFlBQVc7QUFDakMsU0FBS08sUUFBTCxDQUFjO0FBQ1ZsRCxNQUFBQSxRQUFRLEVBQUUsS0FBS2tFLGVBQUwsQ0FBcUIsS0FBS2QsS0FBTCxDQUFXdkUsSUFBaEM7QUFEQSxLQUFkO0FBR0gsR0F4UTJCO0FBMFE1QjRGLEVBQUFBLFVBQVUsRUFBRSxVQUFTNUYsSUFBVCxFQUFlQyxNQUFmLEVBQXVCNEIsT0FBdkIsRUFBZ0N6QixVQUFoQyxFQUE0QztBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSSxDQUFDeUIsT0FBRCxJQUFZNUIsTUFBaEIsRUFBd0I7QUFDcEIsVUFBSSxLQUFLc0csS0FBTCxDQUFXQyxRQUFmLEVBQXlCO0FBQ3JCLGFBQUtDLG1CQUFMO0FBQ0gsT0FGRCxNQUVPLElBQUksQ0FBQ3pHLElBQUQsSUFBU0ksVUFBYixFQUF5QjtBQUM1QjNCLFFBQUFBLE9BQU8sQ0FBQ2lJLElBQVIsQ0FBYSxpQ0FBYixFQUFnRHpHLE1BQWhEO0FBQ0EsYUFBS29FLFFBQUwsQ0FBYztBQUNWbEUsVUFBQUEsV0FBVyxFQUFFLElBREg7QUFFVmtCLFVBQUFBLFNBQVMsRUFBRSxJQUZELENBRU87O0FBRlAsU0FBZDtBQUlBLGFBQUt2QixPQUFMLENBQWE2RyxVQUFiLENBQXdCMUcsTUFBeEIsRUFBZ0MyRyxJQUFoQyxDQUFzQzVHLElBQUQsSUFBVTtBQUMzQyxjQUFJLEtBQUt5RSxTQUFULEVBQW9CO0FBQ2hCO0FBQ0g7O0FBQ0QsZUFBS0osUUFBTCxDQUFjO0FBQ1ZyRSxZQUFBQSxJQUFJLEVBQUVBLElBREk7QUFFVkcsWUFBQUEsV0FBVyxFQUFFO0FBRkgsV0FBZDs7QUFJQSxlQUFLbUYsYUFBTCxDQUFtQnRGLElBQW5CO0FBQ0gsU0FURCxFQVNHNkcsS0FUSCxDQVNVQyxHQUFELElBQVM7QUFDZCxjQUFJLEtBQUtyQyxTQUFULEVBQW9CO0FBQ2hCO0FBQ0gsV0FIYSxDQUtkOzs7QUFDQSxlQUFLSixRQUFMLENBQWM7QUFDVmhELFlBQUFBLFNBQVMsRUFBRTtBQURELFdBQWQsRUFOYyxDQVVkO0FBQ0E7QUFDQTs7QUFDQSxjQUFJeUYsR0FBRyxDQUFDQyxPQUFKLEtBQWdCLDBCQUFoQixJQUE4Q0QsR0FBRyxDQUFDQyxPQUFKLEtBQWdCLGFBQWxFLEVBQWlGO0FBQzdFO0FBQ0EsaUJBQUsxQyxRQUFMLENBQWM7QUFDVmxFLGNBQUFBLFdBQVcsRUFBRTtBQURILGFBQWQ7QUFHSCxXQUxELE1BS087QUFDSCxrQkFBTTJHLEdBQU47QUFDSDtBQUNKLFNBOUJEO0FBK0JILE9BckNNLE1BcUNBLElBQUk5RyxJQUFKLEVBQVU7QUFDYjtBQUNBLGFBQUtGLE9BQUwsQ0FBYXFGLFdBQWI7QUFDQSxhQUFLZCxRQUFMLENBQWM7QUFBQ2hELFVBQUFBLFNBQVMsRUFBRTtBQUFaLFNBQWQ7QUFDSDtBQUNKO0FBQ0osR0F4VTJCO0FBMFU1QmdFLEVBQUFBLGVBQWUsRUFBRSxVQUFTckYsSUFBVCxFQUFlO0FBQzVCLFFBQUksQ0FBQzFCLHdCQUFMLEVBQStCLE9BQU8sS0FBUCxDQURILENBRzVCO0FBQ0E7O0FBQ0EsVUFBTTBJLGdCQUFnQixHQUFHQyxZQUFZLENBQUNDLE9BQWIsQ0FDckJsSCxJQUFJLENBQUNDLE1BQUwsR0FBYyxxQkFETyxDQUF6Qjs7QUFHQSxRQUFJK0csZ0JBQWdCLEtBQUssTUFBekIsRUFBaUM7QUFDN0IsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsVUFBTUcsT0FBTyxHQUFHdEQseUJBQWdCdUQsb0JBQWhCLENBQXFDcEgsSUFBSSxDQUFDQyxNQUExQyxFQUFrRG9ILHFCQUFZQyxjQUFaLENBQTJCdEgsSUFBM0IsQ0FBbEQsQ0FBaEI7O0FBRUEsV0FBT21ILE9BQU8sQ0FBQ0ksTUFBUixHQUFpQixDQUFqQixJQUFzQjFELHlCQUFnQjJELHFCQUFoQixDQUFzQ3hILElBQUksQ0FBQ0MsTUFBM0MsRUFBbURvSCxxQkFBWUMsY0FBWixDQUEyQnRILElBQTNCLENBQW5ELENBQTdCO0FBQ0gsR0F6VjJCO0FBMlY1QnlILEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsVUFBTUMsSUFBSSxHQUFHLEtBQUtDLGVBQUwsRUFBYjs7QUFDQSxVQUFNM0csU0FBUyxHQUFHMEcsSUFBSSxHQUFHQSxJQUFJLENBQUNFLFVBQVIsR0FBcUIsT0FBM0M7QUFDQSxTQUFLdkQsUUFBTCxDQUFjO0FBQ1ZyRCxNQUFBQSxTQUFTLEVBQUVBO0FBREQsS0FBZDs7QUFJQSxTQUFLNkcsMkJBQUw7O0FBRUFDLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsY0FBeEIsRUFBd0MsS0FBS0MsWUFBN0M7O0FBQ0EsUUFBSSxLQUFLekIsS0FBTCxDQUFXMEIsY0FBZixFQUErQjtBQUMzQixXQUFLMUIsS0FBTCxDQUFXMEIsY0FBWCxDQUEwQnRGLEVBQTFCLENBQTZCLG9CQUE3QixFQUFtRCxLQUFLdUYsUUFBeEQ7QUFDSDs7QUFDRCxTQUFLQSxRQUFMO0FBRUEzSixJQUFBQSxRQUFRLENBQUN3SixnQkFBVCxDQUEwQixTQUExQixFQUFxQyxLQUFLSSxTQUExQztBQUNILEdBM1cyQjtBQTZXNUJDLEVBQUFBLHFCQUFxQixFQUFFLFVBQVNDLFNBQVQsRUFBb0JDLFNBQXBCLEVBQStCO0FBQ2xELFdBQVEsQ0FBQ0MsV0FBVyxDQUFDQyxZQUFaLENBQXlCLEtBQUtqQyxLQUE5QixFQUFxQzhCLFNBQXJDLENBQUQsSUFDQSxDQUFDRSxXQUFXLENBQUNDLFlBQVosQ0FBeUIsS0FBS2pFLEtBQTlCLEVBQXFDK0QsU0FBckMsQ0FEVDtBQUVILEdBaFgyQjtBQWtYNUJHLEVBQUFBLGtCQUFrQixFQUFFLFlBQVc7QUFDM0IsUUFBSSxLQUFLdEUsU0FBTCxDQUFldUUsT0FBbkIsRUFBNEI7QUFDeEIsWUFBTUMsUUFBUSxHQUFHLEtBQUt4RSxTQUFMLENBQWV1RSxPQUFoQzs7QUFDQSxVQUFJLENBQUNDLFFBQVEsQ0FBQ0MsTUFBZCxFQUFzQjtBQUNsQkQsUUFBQUEsUUFBUSxDQUFDWixnQkFBVCxDQUEwQixNQUExQixFQUFrQyxLQUFLYyxNQUF2QztBQUNBRixRQUFBQSxRQUFRLENBQUNaLGdCQUFULENBQTBCLFVBQTFCLEVBQXNDLEtBQUtlLFVBQTNDO0FBQ0FILFFBQUFBLFFBQVEsQ0FBQ1osZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsS0FBS2dCLGdCQUE1QztBQUNBSixRQUFBQSxRQUFRLENBQUNaLGdCQUFULENBQTBCLFNBQTFCLEVBQXFDLEtBQUtnQixnQkFBMUM7QUFDSDtBQUNKLEtBVDBCLENBVzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUksS0FBS0MsYUFBTCxJQUFzQixDQUFDLEtBQUt6RSxLQUFMLENBQVd4Qyx1QkFBdEMsRUFBK0Q7QUFDM0QsV0FBS3NDLFFBQUwsQ0FBYztBQUNWdEMsUUFBQUEsdUJBQXVCLEVBQUUsSUFEZjtBQUVWRCxRQUFBQSxtQkFBbUIsRUFBRSxLQUFLa0gsYUFBTCxDQUFtQkMscUJBQW5CO0FBRlgsT0FBZDtBQUlIO0FBQ0osR0F4WTJCO0FBMFk1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQUt6RSxTQUFMLEdBQWlCLElBQWpCLENBTDZCLENBTzdCOztBQUNBLFFBQUksS0FBS0YsS0FBTCxDQUFXdEUsTUFBZixFQUF1QjtBQUNuQnVGLG9DQUFxQjJELGNBQXJCLENBQW9DLEtBQUs1RSxLQUFMLENBQVd0RSxNQUEvQyxFQUF1RCxLQUFLbUosZUFBTCxFQUF2RDtBQUNIOztBQUVELFFBQUksS0FBSzdFLEtBQUwsQ0FBV25FLFVBQWYsRUFBMkI7QUFDdkIsV0FBS04sT0FBTCxDQUFhcUYsV0FBYjtBQUNILEtBZDRCLENBZ0I3Qjs7O0FBQ0EsU0FBS2dCLHlCQUFMOztBQUVBLFFBQUksS0FBS2hDLFNBQUwsQ0FBZXVFLE9BQW5CLEVBQTRCO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBTUMsUUFBUSxHQUFHLEtBQUt4RSxTQUFMLENBQWV1RSxPQUFoQztBQUNBQyxNQUFBQSxRQUFRLENBQUNVLG1CQUFULENBQTZCLE1BQTdCLEVBQXFDLEtBQUtSLE1BQTFDO0FBQ0FGLE1BQUFBLFFBQVEsQ0FBQ1UsbUJBQVQsQ0FBNkIsVUFBN0IsRUFBeUMsS0FBS1AsVUFBOUM7QUFDQUgsTUFBQUEsUUFBUSxDQUFDVSxtQkFBVCxDQUE2QixXQUE3QixFQUEwQyxLQUFLTixnQkFBL0M7QUFDQUosTUFBQUEsUUFBUSxDQUFDVSxtQkFBVCxDQUE2QixTQUE3QixFQUF3QyxLQUFLTixnQkFBN0M7QUFDSDs7QUFDRHZHLHdCQUFJOEcsVUFBSixDQUFlLEtBQUsvRyxhQUFwQjs7QUFDQSxRQUFJLEtBQUt6QyxPQUFULEVBQWtCO0FBQ2QsV0FBS0EsT0FBTCxDQUFheUosY0FBYixDQUE0QixNQUE1QixFQUFvQyxLQUFLM0csTUFBekM7QUFDQSxXQUFLOUMsT0FBTCxDQUFheUosY0FBYixDQUE0QixlQUE1QixFQUE2QyxLQUFLMUcsY0FBbEQ7QUFDQSxXQUFLL0MsT0FBTCxDQUFheUosY0FBYixDQUE0QixXQUE1QixFQUF5QyxLQUFLekcsVUFBOUM7QUFDQSxXQUFLaEQsT0FBTCxDQUFheUosY0FBYixDQUE0QixrQkFBNUIsRUFBZ0QsS0FBS3hHLGlCQUFyRDtBQUNBLFdBQUtqRCxPQUFMLENBQWF5SixjQUFiLENBQTRCLGtCQUE1QixFQUFnRCxLQUFLdkcsaUJBQXJEO0FBQ0EsV0FBS2xELE9BQUwsQ0FBYXlKLGNBQWIsQ0FBNEIsbUJBQTVCLEVBQWlELEtBQUtyRyxjQUF0RDtBQUNBLFdBQUtwRCxPQUFMLENBQWF5SixjQUFiLENBQTRCLG1CQUE1QixFQUFpRCxLQUFLdEcsaUJBQXREO0FBQ0EsV0FBS25ELE9BQUwsQ0FBYXlKLGNBQWIsQ0FBNEIsYUFBNUIsRUFBMkMsS0FBS3BHLGFBQWhEO0FBQ0EsV0FBS3JELE9BQUwsQ0FBYXlKLGNBQWIsQ0FBNEIsd0JBQTVCLEVBQXNELEtBQUtuRyxpQkFBM0Q7QUFDQSxXQUFLdEQsT0FBTCxDQUFheUosY0FBYixDQUE0QiwyQkFBNUIsRUFBeUQsS0FBS2xHLDJCQUE5RDtBQUNBLFdBQUt2RCxPQUFMLENBQWF5SixjQUFiLENBQTRCLHdCQUE1QixFQUFzRCxLQUFLakcseUJBQTNEO0FBQ0g7O0FBRUR3RSxJQUFBQSxNQUFNLENBQUN1QixtQkFBUCxDQUEyQixjQUEzQixFQUEyQyxLQUFLckIsWUFBaEQ7O0FBQ0EsUUFBSSxLQUFLekIsS0FBTCxDQUFXMEIsY0FBZixFQUErQjtBQUMzQixXQUFLMUIsS0FBTCxDQUFXMEIsY0FBWCxDQUEwQnNCLGNBQTFCLENBQXlDLG9CQUF6QyxFQUErRCxLQUFLckIsUUFBcEU7QUFDSDs7QUFFRDNKLElBQUFBLFFBQVEsQ0FBQzhLLG1CQUFULENBQTZCLFNBQTdCLEVBQXdDLEtBQUtsQixTQUE3QyxFQWxENkIsQ0FvRDdCOztBQUNBLFFBQUksS0FBSzVFLGVBQVQsRUFBMEI7QUFDdEIsV0FBS0EsZUFBTCxDQUFxQmlHLE1BQXJCO0FBQ0gsS0F2RDRCLENBd0Q3Qjs7O0FBQ0EsUUFBSSxLQUFLN0YscUJBQVQsRUFBZ0M7QUFDNUIsV0FBS0EscUJBQUwsQ0FBMkI2RixNQUEzQjtBQUNIOztBQUVEM0YsNkJBQWdCMEYsY0FBaEIsQ0FBK0IsUUFBL0IsRUFBeUMsS0FBS3pGLHdCQUE5Qzs7QUFFQSxRQUFJLEtBQUtDLHlCQUFULEVBQW9DO0FBQ2hDQyw2QkFBY3lGLGNBQWQsQ0FBNkIsS0FBSzFGLHlCQUFsQzs7QUFDQSxXQUFLQSx5QkFBTCxHQUFpQyxJQUFqQztBQUNILEtBbEU0QixDQW9FN0I7OztBQUNBLFNBQUsyRixrQkFBTCxDQUF3QkMsaUJBQXhCLEdBckU2QixDQXVFN0I7QUFDQTtBQUNBOztBQUNILEdBcGQyQjtBQXNkNUIvRixFQUFBQSx3QkFBd0IsRUFBRSxZQUFXO0FBQ2pDLFNBQUtTLFFBQUwsQ0FBYztBQUNWN0MsTUFBQUEsY0FBYyxFQUFFQyx5QkFBZ0JDLGlCQUFoQixHQUFvQ0M7QUFEMUMsS0FBZDtBQUdILEdBMWQyQjs7QUE0ZDVCcUcsRUFBQUEsWUFBWSxDQUFDNEIsS0FBRCxFQUFRO0FBQ2hCLFFBQUlDLHlCQUFnQkMsY0FBaEIsR0FBaUNDLGlCQUFqQyxHQUFxRHhDLE1BQXJELEdBQThELENBQWxFLEVBQXFFO0FBQ2pFLGFBQU9xQyxLQUFLLENBQUNJLFdBQU4sR0FDSCx5QkFBRyxnRUFBSCxDQURKO0FBRUgsS0FIRCxNQUdPLElBQUksS0FBS3JDLGVBQUwsTUFBMEIsS0FBS3BELEtBQUwsQ0FBV3ZELFNBQVgsS0FBeUIsT0FBdkQsRUFBZ0U7QUFDbkUsYUFBTzRJLEtBQUssQ0FBQ0ksV0FBTixHQUNILHlCQUFHLDBEQUFILENBREo7QUFFSDtBQUNKLEdBcGUyQjs7QUFzZTVCN0IsRUFBQUEsU0FBUyxFQUFFLFVBQVM4QixFQUFULEVBQWE7QUFDcEIsUUFBSUMsT0FBTyxHQUFHLEtBQWQ7QUFDQSxVQUFNQyxXQUFXLEdBQUcsdUNBQXdCRixFQUF4QixDQUFwQjs7QUFFQSxZQUFRQSxFQUFFLENBQUNHLEdBQVg7QUFDSSxXQUFLQyxjQUFJQyxDQUFUO0FBQ0ksWUFBSUgsV0FBSixFQUFpQjtBQUNiLGVBQUtJLGdCQUFMO0FBQ0FMLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBQ0Q7O0FBRUosV0FBS0csY0FBSUcsQ0FBVDtBQUNJLFlBQUlMLFdBQUosRUFBaUI7QUFDYixlQUFLTSxnQkFBTDtBQUNBUCxVQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEO0FBYlI7O0FBZ0JBLFFBQUlBLE9BQUosRUFBYTtBQUNURCxNQUFBQSxFQUFFLENBQUNTLGVBQUg7QUFDQVQsTUFBQUEsRUFBRSxDQUFDVSxjQUFIO0FBQ0g7QUFDSixHQTlmMkI7QUFnZ0I1QmpJLEVBQUFBLFFBQVEsRUFBRSxVQUFTa0ksT0FBVCxFQUFrQjtBQUN4QixZQUFRQSxPQUFPLENBQUNDLE1BQWhCO0FBQ0ksV0FBSyxxQkFBTDtBQUNBLFdBQUssY0FBTDtBQUNJLGFBQUtDLGFBQUwsQ0FBbUIsS0FBS3ZHLEtBQUwsQ0FBV3ZFLElBQTlCOztBQUNBOztBQUNKLFdBQUssc0JBQUw7QUFDRSxhQUFLK0ssYUFBTCxDQUNJSCxPQUFPLENBQUNJLElBQVIsQ0FBYUMsT0FBYixDQUFxQkMsR0FEekIsRUFFSU4sT0FBTyxDQUFDSSxJQUFSLENBQWFDLE9BQWIsQ0FBcUJ2RSxJQUZ6QixFQUdJa0UsT0FBTyxDQUFDSSxJQUFSLENBQWFHLFdBQWIsSUFBNEJQLE9BQU8sQ0FBQ0ksSUFBUixDQUFhSSxJQUg3QztBQUlBOztBQUNGLFdBQUssa0JBQUw7QUFDSXZCLGlDQUFnQkMsY0FBaEIsR0FBaUN1QixxQkFBakMsQ0FBdUQsQ0FBQ1QsT0FBTyxDQUFDVSxJQUFULENBQXZELEVBQXVFLEtBQUsvRyxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUF2RixFQUErRixLQUFLSCxPQUFwRzs7QUFDQTs7QUFDSixXQUFLLGtCQUFMO0FBQ0EsV0FBSyxnQkFBTDtBQUNBLFdBQUssaUJBQUw7QUFDQSxXQUFLLGlCQUFMO0FBQ0ksYUFBS3lMLFdBQUw7QUFDQTs7QUFDSixXQUFLLFlBQUw7QUFDSTtBQUNBO0FBRUEsWUFBSSxDQUFDWCxPQUFPLENBQUNZLE9BQWIsRUFBc0I7QUFDbEI7QUFDSDs7QUFFRCxZQUFJOUQsSUFBSSxHQUFHLEtBQUtDLGVBQUwsRUFBWDs7QUFDQSxZQUFJM0csU0FBSjs7QUFFQSxZQUFJMEcsSUFBSixFQUFVO0FBQ04xRyxVQUFBQSxTQUFTLEdBQUcwRyxJQUFJLENBQUNFLFVBQWpCO0FBQ0gsU0FGRCxNQUVPO0FBQ0g1RyxVQUFBQSxTQUFTLEdBQUcsT0FBWjtBQUNILFNBZkwsQ0FpQkk7QUFDQTs7O0FBQ0EsYUFBSzZHLDJCQUFMOztBQUVBLGFBQUt4RCxRQUFMLENBQWM7QUFDVnJELFVBQUFBLFNBQVMsRUFBRUE7QUFERCxTQUFkO0FBSUE7O0FBQ0osV0FBSyxZQUFMO0FBQ0ksYUFBS3FELFFBQUwsQ0FBYztBQUNWbEQsVUFBQUEsUUFBUSxFQUFFeUosT0FBTyxDQUFDYTtBQURSLFNBQWQ7QUFHQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0ksWUFBSSxLQUFLbEgsS0FBTCxDQUFXeEQsYUFBWCxJQUE0QjZKLE9BQU8sQ0FBQ2hCLEtBQVIsQ0FBY2xGLFNBQWQsT0FBOEIsS0FBS0gsS0FBTCxDQUFXdEUsTUFBckUsSUFBK0UsQ0FBQyxLQUFLd0UsU0FBekYsRUFBb0c7QUFDaEcsZUFBS2lILG1CQUFMO0FBQ0g7O0FBQ0Q7O0FBQ0osV0FBSyxPQUFMO0FBQ0ksWUFBSSxLQUFLbkgsS0FBTCxDQUFXeEQsYUFBZixFQUE4QjtBQUMxQixnQkFBTWQsTUFBTSxHQUFHMkssT0FBTyxDQUFDaEIsS0FBUixDQUFjbEYsU0FBZCxFQUFmOztBQUNBLGNBQUl6RSxNQUFNLEtBQUssS0FBS3NFLEtBQUwsQ0FBV3RFLE1BQTFCLEVBQWtDO0FBQzlCLGlCQUFLeUwsbUJBQUw7QUFDSDs7QUFFREMsVUFBQUEsWUFBWSxDQUFDLE1BQU07QUFDZm5KLGdDQUFJb0osUUFBSixDQUFhO0FBQ1RmLGNBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVRXLGNBQUFBLE9BQU8sRUFBRXZMLE1BRkE7QUFHVDRMLGNBQUFBLGVBQWUsRUFBRWpCO0FBSFIsYUFBYjtBQUtILFdBTlcsQ0FBWjtBQU9IOztBQUNEO0FBdkVSO0FBeUVILEdBMWtCMkI7QUE0a0I1Qi9ILEVBQUFBLGNBQWMsRUFBRSxVQUFTb0gsRUFBVCxFQUFhakssSUFBYixFQUFtQjhMLGlCQUFuQixFQUFzQ0MsT0FBdEMsRUFBK0NmLElBQS9DLEVBQXFEO0FBQ2pFLFFBQUksS0FBS3ZHLFNBQVQsRUFBb0IsT0FENkMsQ0FHakU7O0FBQ0EsUUFBSSxDQUFDekUsSUFBTCxFQUFXO0FBQ1gsUUFBSSxDQUFDLEtBQUt1RSxLQUFMLENBQVd2RSxJQUFaLElBQW9CQSxJQUFJLENBQUNDLE1BQUwsSUFBZSxLQUFLc0UsS0FBTCxDQUFXdkUsSUFBWCxDQUFnQkMsTUFBdkQsRUFBK0QsT0FMRSxDQU9qRTs7QUFDQSxRQUFJK0ssSUFBSSxDQUFDZ0IsUUFBTCxDQUFjQyxjQUFkLE9BQW1Dak0sSUFBSSxDQUFDa00sd0JBQUwsRUFBdkMsRUFBd0U7O0FBRXhFLFFBQUlqQyxFQUFFLENBQUNrQyxPQUFILE9BQWlCLDhCQUFyQixFQUFxRDtBQUNqRCxXQUFLQywyQkFBTCxDQUFpQ3BNLElBQWpDO0FBQ0g7O0FBRUQsUUFBSWlLLEVBQUUsQ0FBQ2tDLE9BQUgsT0FBaUIsbUJBQXJCLEVBQTBDO0FBQ3RDLFdBQUtFLGdCQUFMLENBQXNCck0sSUFBdEI7QUFDSCxLQWhCZ0UsQ0FrQmpFO0FBQ0E7OztBQUNBLFFBQUk4TCxpQkFBaUIsSUFBSSxDQUFDZCxJQUF0QixJQUE4QixDQUFDQSxJQUFJLENBQUNzQixTQUF4QyxFQUFtRCxPQXBCYyxDQXNCakU7QUFDQTs7QUFDQSxRQUFJLEtBQUsvSCxLQUFMLENBQVcxQyxPQUFmLEVBQXdCOztBQUV4QixRQUFJb0ksRUFBRSxDQUFDc0MsU0FBSCxPQUFtQixLQUFLek0sT0FBTCxDQUFhME0sV0FBYixDQUF5QkMsTUFBaEQsRUFBd0Q7QUFDcEQ7QUFDQSxVQUFJLENBQUMsS0FBS2xJLEtBQUwsQ0FBV3hELGFBQVosSUFBNkIsS0FBS3dELEtBQUwsQ0FBV3pDLG1CQUE1QyxFQUFpRSxDQUM3RDtBQUNILE9BRkQsTUFFTyxJQUFJLENBQUMsOEJBQWdCbUksRUFBaEIsQ0FBTCxFQUEwQjtBQUM3QixhQUFLNUYsUUFBTCxDQUFjLENBQUNFLEtBQUQsRUFBUWdDLEtBQVIsS0FBa0I7QUFDNUIsaUJBQU87QUFBQzNGLFlBQUFBLGlCQUFpQixFQUFFMkQsS0FBSyxDQUFDM0QsaUJBQU4sR0FBMEI7QUFBOUMsV0FBUDtBQUNILFNBRkQ7QUFHSDtBQUNKO0FBQ0osR0FobkIyQjtBQWtuQjVCa0MsRUFBQUEsVUFBVSxFQUFFLFVBQVM5QyxJQUFULEVBQWU7QUFDdkIsUUFBSSxLQUFLdUUsS0FBTCxDQUFXdkUsSUFBWCxJQUFtQkEsSUFBSSxDQUFDQyxNQUFMLElBQWUsS0FBS3NFLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQXRELEVBQThEO0FBQzFELFdBQUtzTCxXQUFMO0FBQ0g7QUFDSixHQXRuQjJCO0FBd25CNUJtQixFQUFBQSxrQ0FBa0MsRUFBRSxZQUFXO0FBQzNDO0FBQ0E7QUFDQSxTQUFLbkIsV0FBTDtBQUNILEdBNW5CMkI7O0FBOG5CNUJuSSxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQjtBQUNBO0FBQ0EsU0FBS21JLFdBQUw7QUFDSCxHQWxvQjJCOztBQW9vQjVCb0IsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixRQUFJLENBQUMsS0FBSzNELGFBQVYsRUFBeUI7QUFDckIsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsV0FBTyxLQUFLQSxhQUFMLENBQW1CMkQsZ0JBQW5CLEVBQVA7QUFDSCxHQXpvQjJCO0FBMm9CNUI7QUFDQTtBQUNBckgsRUFBQUEsYUFBYSxFQUFFLFVBQVN0RixJQUFULEVBQWU7QUFDMUIsU0FBSzRNLG1CQUFMLENBQXlCNU0sSUFBekI7O0FBQ0EsU0FBS29NLDJCQUFMLENBQWlDcE0sSUFBakM7O0FBQ0EsU0FBSzZNLG9CQUFMLENBQTBCN00sSUFBMUI7O0FBQ0EsU0FBSzhNLDRCQUFMLENBQWtDOU0sSUFBbEM7O0FBQ0EsU0FBS3FNLGdCQUFMLENBQXNCck0sSUFBdEI7O0FBQ0EsU0FBSytNLGtCQUFMLENBQXdCL00sSUFBeEI7QUFDSCxHQXBwQjJCO0FBc3BCNUI4TSxFQUFBQSw0QkFBNEIsRUFBRSxnQkFBZTlNLElBQWYsRUFBcUI7QUFDL0MsU0FBS3FFLFFBQUwsQ0FBYztBQUNWbEMsTUFBQUEscUJBQXFCLEVBQUUsTUFBTW5DLElBQUksQ0FBQ2dOLHFCQUFMO0FBRG5CLEtBQWQ7QUFHSCxHQTFwQjJCO0FBNHBCNUJILEVBQUFBLG9CQUFvQixFQUFFLGdCQUFlN00sSUFBZixFQUFxQjtBQUN2QztBQUNBLFFBQUksS0FBS0YsT0FBTCxDQUFhQyx5QkFBYixFQUFKLEVBQThDO0FBQzFDLFVBQUlDLElBQUksSUFBSUEsSUFBSSxDQUFDaU4sZUFBTCxPQUEyQixNQUF2QyxFQUErQztBQUMzQyxZQUFJO0FBQ0EsZ0JBQU1qTixJQUFJLENBQUNrTixtQkFBTCxFQUFOOztBQUNBLGNBQUksQ0FBQyxLQUFLekksU0FBVixFQUFxQjtBQUNqQixpQkFBS0osUUFBTCxDQUFjO0FBQUM5RCxjQUFBQSxhQUFhLEVBQUU7QUFBaEIsYUFBZDtBQUNIO0FBQ0osU0FMRCxDQUtFLE9BQU91RyxHQUFQLEVBQVk7QUFDVixnQkFBTXFHLFlBQVksR0FBRyxvQ0FBNkJuTixJQUFJLENBQUNDLE1BQWxDLGdCQUNqQix1Q0FESjtBQUVBeEIsVUFBQUEsT0FBTyxDQUFDMk8sS0FBUixDQUFjRCxZQUFkO0FBQ0ExTyxVQUFBQSxPQUFPLENBQUMyTyxLQUFSLENBQWN0RyxHQUFkO0FBQ0g7QUFDSjtBQUNKO0FBQ0osR0E3cUIyQjtBQStxQjVCOEYsRUFBQUEsbUJBQW1CLEVBQUUsVUFBUzVNLElBQVQsRUFBZTtBQUNoQyxVQUFNcU4sZ0JBQWdCLEdBQUdyTixJQUFJLENBQUNzTixZQUFMLENBQWtCQyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBekI7O0FBQ0EsUUFBSUYsZ0JBQWdCLElBQUlBLGdCQUFnQixDQUFDRyxVQUFqQixHQUE4QkMsWUFBOUIsS0FBK0MsVUFBdkUsRUFBbUY7QUFDL0UsV0FBS3BKLFFBQUwsQ0FBYztBQUNWcEQsUUFBQUEsYUFBYSxFQUFFO0FBREwsT0FBZDtBQUdIOztBQUVELFVBQU15TSxpQkFBaUIsR0FBRzFOLElBQUksQ0FBQ3NOLFlBQUwsQ0FBa0JDLGNBQWxCLENBQWlDLDJCQUFqQyxFQUE4RCxFQUE5RCxDQUExQjs7QUFDQSxRQUFJRyxpQkFBaUIsSUFBSUEsaUJBQWlCLENBQUNGLFVBQWxCLEdBQStCRyxrQkFBL0IsS0FBc0QsZ0JBQS9FLEVBQWlHO0FBQzdGLFdBQUt0SixRQUFMLENBQWM7QUFDVm5ELFFBQUFBLE9BQU8sRUFBRTtBQURDLE9BQWQ7QUFHSDtBQUNKLEdBN3JCMkI7QUErckI1QmtMLEVBQUFBLDJCQUEyQixFQUFFLFVBQVM7QUFBQ25NLElBQUFBO0FBQUQsR0FBVCxFQUFtQjtBQUM1QztBQUNBLFVBQU1tSyxHQUFHLEdBQUcsS0FBS3RLLE9BQUwsQ0FBYThOLGVBQWIsQ0FBNkIzTixNQUE3QixJQUF1Qyx5QkFBdkMsR0FBbUUsb0JBQS9FO0FBQ0EsU0FBS29FLFFBQUwsQ0FBYztBQUNWd0osTUFBQUEsY0FBYyxFQUFFN0osdUJBQWNNLFFBQWQsQ0FBdUI4RixHQUF2QixFQUE0Qm5LLE1BQTVCO0FBRE4sS0FBZDtBQUdILEdBcnNCMkI7QUF1c0I1QjJDLEVBQUFBLE1BQU0sRUFBRSxVQUFTNUMsSUFBVCxFQUFlO0FBQ25CLFFBQUksQ0FBQ0EsSUFBRCxJQUFTQSxJQUFJLENBQUNDLE1BQUwsS0FBZ0IsS0FBS3NFLEtBQUwsQ0FBV3RFLE1BQXhDLEVBQWdEO0FBQzVDO0FBQ0g7O0FBQ0QsU0FBS29FLFFBQUwsQ0FBYztBQUNWckUsTUFBQUEsSUFBSSxFQUFFQTtBQURJLEtBQWQsRUFFRyxNQUFNO0FBQ0wsV0FBS3NGLGFBQUwsQ0FBbUJ0RixJQUFuQjtBQUNILEtBSkQ7QUFLSCxHQWh0QjJCO0FBa3RCNUJxRCxFQUFBQSwyQkFBMkIsRUFBRSxVQUFTb0osTUFBVCxFQUFpQnFCLE1BQWpCLEVBQXlCO0FBQ2xELFVBQU05TixJQUFJLEdBQUcsS0FBS3VFLEtBQUwsQ0FBV3ZFLElBQXhCOztBQUNBLFFBQUksQ0FBQ0EsSUFBSSxDQUFDc04sWUFBTCxDQUFrQlMsU0FBbEIsQ0FBNEJ0QixNQUE1QixDQUFMLEVBQTBDO0FBQ3RDO0FBQ0g7O0FBQ0QsU0FBS0osZ0JBQUwsQ0FBc0JyTSxJQUF0QjtBQUNILEdBeHRCMkI7QUEwdEI1QnNELEVBQUFBLHlCQUF5QixFQUFFLFVBQVNtSixNQUFULEVBQWlCdUIsWUFBakIsRUFBK0I7QUFDdEQsVUFBTWhPLElBQUksR0FBRyxLQUFLdUUsS0FBTCxDQUFXdkUsSUFBeEI7O0FBQ0EsUUFBSSxDQUFDQSxJQUFELElBQVMsQ0FBQ0EsSUFBSSxDQUFDc04sWUFBTCxDQUFrQlMsU0FBbEIsQ0FBNEJ0QixNQUE1QixDQUFkLEVBQW1EO0FBQy9DO0FBQ0g7O0FBQ0QsU0FBS0osZ0JBQUwsQ0FBc0JyTSxJQUF0QjtBQUNILEdBaHVCMkI7QUFrdUI1QnFNLEVBQUFBLGdCQUFnQixFQUFFLGdCQUFlck0sSUFBZixFQUFxQjtBQUNuQyxRQUFJLENBQUMsS0FBS0YsT0FBTCxDQUFhOE4sZUFBYixDQUE2QjVOLElBQUksQ0FBQ0MsTUFBbEMsQ0FBTCxFQUFnRDtBQUM1QztBQUNIOztBQUNELFFBQUksQ0FBQyxLQUFLSCxPQUFMLENBQWFtTyxlQUFiLEVBQUwsRUFBcUM7QUFDakM7QUFDQTtBQUNBO0FBQ0EsV0FBSzVKLFFBQUwsQ0FBYztBQUNWNkosUUFBQUEsU0FBUyxFQUFFO0FBREQsT0FBZDtBQUdBO0FBQ0g7O0FBQ0QsUUFBSSxDQUFDbEssdUJBQWNtSyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBTCxFQUE4RDtBQUMxRG5PLE1BQUFBLElBQUksQ0FBQ29PLG9CQUFMLEdBQTRCeEgsSUFBNUIsQ0FBa0N3SCxvQkFBRCxJQUEwQjtBQUN2RCxhQUFLL0osUUFBTCxDQUFjO0FBQ1Y2SixVQUFBQSxTQUFTLEVBQUVFLG9CQUFvQixHQUFHLFNBQUgsR0FBZTtBQURwQyxTQUFkO0FBR0gsT0FKRDtBQUtBL1AsTUFBQUEsUUFBUSxDQUFDLDREQUFELENBQVI7QUFDQTtBQUNIO0FBRUQ7OztBQUNBLFNBQUtnRyxRQUFMLENBQWM7QUFDVjZKLE1BQUFBLFNBQVMsRUFBRSxNQUFNLHNDQUFvQixLQUFLcE8sT0FBekIsRUFBa0NFLElBQWxDO0FBRFAsS0FBZDtBQUdILEdBN3ZCMkI7QUErdkI1QnFPLEVBQUFBLFVBQVUsRUFBRSxZQUFXO0FBQ25CLFVBQU1yTyxJQUFJLEdBQUcsS0FBS3VFLEtBQUwsQ0FBV3ZFLElBQXhCO0FBQ0EsUUFBSSxDQUFDQSxJQUFMLEVBQVc7QUFFWHZCLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLDZCQUFaOztBQUNBLFVBQU00UCxXQUFXLEdBQUd0Syx1QkFBY00sUUFBZCxDQUF1QixXQUF2QixFQUFvQ3RFLElBQUksQ0FBQ0MsTUFBekMsQ0FBcEI7O0FBQ0FzTyxvQkFBT0MsSUFBUCxDQUFZRixXQUFXLENBQUNHLGFBQXhCLEVBQXVDSCxXQUFXLENBQUNJLGVBQW5EO0FBQ0gsR0F0d0IyQjtBQXd3QjVCdkwsRUFBQUEsYUFBYSxFQUFFLFVBQVN5RyxLQUFULEVBQWdCO0FBQzNCLFVBQU0rRSxJQUFJLEdBQUcvRSxLQUFLLENBQUN1QyxPQUFOLEVBQWI7O0FBQ0EsUUFBSSxDQUFDd0MsSUFBSSxLQUFLLHlCQUFULElBQXNDQSxJQUFJLEtBQUssd0JBQWhELEtBQTZFLEtBQUtwSyxLQUFMLENBQVd2RSxJQUE1RixFQUFrRztBQUM5RjtBQUNBLFdBQUtvTSwyQkFBTCxDQUFpQyxLQUFLN0gsS0FBTCxDQUFXdkUsSUFBNUM7QUFDSDtBQUNKLEdBOXdCMkI7QUFneEI1QitDLEVBQUFBLGlCQUFpQixFQUFFLFVBQVM2RyxLQUFULEVBQWdCNUosSUFBaEIsRUFBc0I7QUFDckMsUUFBSUEsSUFBSSxDQUFDQyxNQUFMLElBQWUsS0FBS3NFLEtBQUwsQ0FBV3RFLE1BQTlCLEVBQXNDO0FBQ2xDLFlBQU0wTyxJQUFJLEdBQUcvRSxLQUFLLENBQUN1QyxPQUFOLEVBQWI7O0FBQ0EsVUFBSXdDLElBQUksS0FBSyw4QkFBYixFQUE2QztBQUN6QyxjQUFNTCxXQUFXLEdBQUcxRSxLQUFLLENBQUM0RCxVQUFOLEVBQXBCLENBRHlDLENBRXpDOztBQUNBL08sUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksb0NBQVo7O0FBQ0E2UCx3QkFBT0MsSUFBUCxDQUFZRixXQUFXLENBQUNHLGFBQXhCLEVBQXVDSCxXQUFXLENBQUNJLGVBQW5EO0FBQ0gsT0FMRCxNQUtPLElBQUlDLElBQUksS0FBSyw4QkFBVCxJQUEyQ0EsSUFBSSxLQUFLLHdCQUF4RCxFQUFrRjtBQUNyRjtBQUNBLGFBQUt2QywyQkFBTCxDQUFpQ3BNLElBQWpDO0FBQ0g7QUFDSjtBQUNKLEdBN3hCMkI7QUEreEI1QmdELEVBQUFBLGlCQUFpQixFQUFFLFVBQVNpSCxFQUFULEVBQWExRixLQUFiLEVBQW9CO0FBQ25DO0FBQ0EsUUFBSSxDQUFDLEtBQUtBLEtBQUwsQ0FBV3ZFLElBQVosSUFBb0IsS0FBS3VFLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQWhCLEtBQTJCc0UsS0FBSyxDQUFDdEUsTUFBekQsRUFBaUU7QUFDN0Q7QUFDSDs7QUFFRCxTQUFLOE0sa0JBQUwsQ0FBd0IsS0FBS3hJLEtBQUwsQ0FBV3ZFLElBQW5DO0FBQ0gsR0F0eUIyQjtBQXd5QjVCaUQsRUFBQUEsaUJBQWlCLEVBQUUsVUFBU2dILEVBQVQsRUFBYTFGLEtBQWIsRUFBb0JxSyxNQUFwQixFQUE0QjtBQUMzQztBQUNBLFFBQUksQ0FBQyxLQUFLckssS0FBTCxDQUFXdkUsSUFBaEIsRUFBc0I7QUFDbEI7QUFDSCxLQUowQyxDQU0zQzs7O0FBQ0EsUUFBSTRPLE1BQU0sQ0FBQzNPLE1BQVAsS0FBa0IsS0FBS3NFLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQXRDLEVBQThDO0FBQzFDO0FBQ0g7O0FBRUQsU0FBS3lKLGtCQUFMLENBQXdCa0YsTUFBeEI7QUFDSCxHQXB6QjJCO0FBc3pCNUIxTCxFQUFBQSxjQUFjLEVBQUUsVUFBU2xELElBQVQsRUFBZTZPLFVBQWYsRUFBMkJDLGFBQTNCLEVBQTBDO0FBQ3RELFFBQUk5TyxJQUFJLENBQUNDLE1BQUwsS0FBZ0IsS0FBS3NFLEtBQUwsQ0FBV3RFLE1BQS9CLEVBQXVDO0FBQ25DLFdBQUtzTCxXQUFMOztBQUNBLFdBQUtzQixvQkFBTCxDQUEwQjdNLElBQTFCOztBQUNBLFdBQUsrTSxrQkFBTCxDQUF3Qi9NLElBQXhCO0FBQ0g7QUFDSixHQTV6QjJCO0FBOHpCNUIrTSxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTL00sSUFBVCxFQUFlO0FBQy9CLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU0rTyxFQUFFLEdBQUcsS0FBS2pQLE9BQUwsQ0FBYWtQLFNBQWIsRUFBWDtBQUNBLFlBQU01TSxRQUFRLEdBQUdwQyxJQUFJLENBQUNpTixlQUFMLE9BQTJCLE1BQTNCLElBQXFDak4sSUFBSSxDQUFDc04sWUFBTCxDQUFrQjJCLFlBQWxCLENBQStCLFlBQS9CLEVBQTZDRixFQUE3QyxDQUF0RDtBQUNBLFlBQU0xTSxRQUFRLEdBQUdyQyxJQUFJLENBQUNrUCxjQUFMLEVBQWpCO0FBRUEsV0FBSzdLLFFBQUwsQ0FBYztBQUFDakMsUUFBQUEsUUFBRDtBQUFXQyxRQUFBQTtBQUFYLE9BQWQ7QUFDSDtBQUNKLEdBdDBCMkI7QUF3MEI1QjtBQUNBO0FBQ0FxSCxFQUFBQSxrQkFBa0IsRUFBRSw4QkFBa0IsVUFBU3lGLFdBQVQsRUFBc0I7QUFDeEQ7QUFDQTtBQUNBLFNBQUt0SCwyQkFBTDs7QUFDQSxTQUFLdUgsY0FBTDs7QUFFQSxRQUFJQyxvQkFBb0IsR0FBRyxDQUEzQjs7QUFDQSxRQUFJRixXQUFXLElBQUlBLFdBQVcsQ0FBQ04sVUFBWixLQUEyQixRQUExQyxJQUFzRCxLQUFLdEssS0FBTCxDQUFXdkUsSUFBWCxDQUFnQnNQLHFCQUFoQixPQUE0QyxDQUF0RyxFQUF5RztBQUNyRztBQUNBO0FBQ0FELE1BQUFBLG9CQUFvQixHQUFHLENBQXZCO0FBQ0g7O0FBQ0QsU0FBS3ZFLGFBQUwsQ0FBbUIsS0FBS3ZHLEtBQUwsQ0FBV3ZFLElBQTlCLEVBQW9DcVAsb0JBQXBDOztBQUVBLFNBQUtoRCxnQkFBTCxDQUFzQixLQUFLOUgsS0FBTCxDQUFXdkUsSUFBakM7QUFDSCxHQWZtQixFQWVqQixHQWZpQixDQTEwQlE7QUEyMUI1QjhLLEVBQUFBLGFBQWEsRUFBRSxVQUFTOUssSUFBVCxFQUFldVAsY0FBZixFQUErQjtBQUMxQyxRQUFJQyxxQkFBcUIsR0FBRyxLQUE1Qjs7QUFDQSxRQUFJdkksWUFBSixFQUFrQjtBQUNkdUksTUFBQUEscUJBQXFCLEdBQUd2SSxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsMEJBQTBCLEtBQUszQyxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUEvRCxDQUF4QjtBQUNIOztBQUNELFFBQUl1UCxxQkFBSixFQUEyQjtBQUN2QixVQUFJLEtBQUtqTCxLQUFMLENBQVduRCxPQUFmLEVBQXdCLEtBQUtpRCxRQUFMLENBQWM7QUFBQ2pELFFBQUFBLE9BQU8sRUFBRTtBQUFWLE9BQWQ7QUFDeEI7QUFDSDs7QUFFRCxRQUFJcU8sMEJBQTBCLEdBQUd6UCxJQUFJLENBQUMwUCxvQkFBTCxLQUE4QjFQLElBQUksQ0FBQ3NQLHFCQUFMLEVBQS9EO0FBQ0EsUUFBSUMsY0FBSixFQUFvQkUsMEJBQTBCLElBQUlGLGNBQTlCO0FBQ3BCLFNBQUtsTCxRQUFMLENBQWM7QUFBQ2pELE1BQUFBLE9BQU8sRUFBRXFPLDBCQUEwQixLQUFLO0FBQXpDLEtBQWQ7QUFDSCxHQXgyQjJCO0FBMDJCNUI1SCxFQUFBQSwyQkFBMkIsRUFBRSxZQUFXO0FBQ3BDLFVBQU03SCxJQUFJLEdBQUcsS0FBS3VFLEtBQUwsQ0FBV3ZFLElBQXhCOztBQUNBLFFBQUksQ0FBQ0EsSUFBRCxJQUFTLENBQUMsS0FBS3VHLEtBQUwsQ0FBV3pILGlCQUF6QixFQUE0QztBQUN4QztBQUNIOztBQUNELFVBQU02USxVQUFVLEdBQUczUCxJQUFJLENBQUMrTixTQUFMLENBQ2YsS0FBS3hILEtBQUwsQ0FBV3pILGlCQUFYLENBQTZCOFEsMEJBQTdCLENBQXdENVAsSUFBSSxDQUFDQyxNQUE3RCxDQURlLENBQW5COztBQUlBLFFBQUksQ0FBQzBQLFVBQUwsRUFBaUI7QUFDYjtBQUNIOztBQUNELFVBQU1FLFFBQVEsR0FBRyxLQUFLdEosS0FBTCxDQUFXekgsaUJBQVgsQ0FBNkJnUix3QkFBN0IsQ0FBc0RILFVBQVUsQ0FBQzFQLE1BQWpFLENBQWpCLENBWm9DLENBY3BDO0FBQ0E7O0FBQ0EsU0FBS29FLFFBQUwsQ0FBYztBQUNWMEwsTUFBQUEsMkJBQTJCLEVBQ3ZCLENBQUMsQ0FBQ0YsUUFBRCxJQUFhQSxRQUFRLENBQUNqSSxVQUFULEtBQXdCLE9BQXRDLEtBQ0ErSCxVQUFVLENBQUNkLFVBQVgsS0FBMEI7QUFIcEIsS0FBZDtBQU1ILEdBaDRCMkI7O0FBazRCNUJPLEVBQUFBLGNBQWMsR0FBRztBQUNiLFVBQU1wUCxJQUFJLEdBQUcsS0FBS3VFLEtBQUwsQ0FBV3ZFLElBQXhCOztBQUNBLFFBQUlBLElBQUksQ0FBQ2lOLGVBQUwsTUFBMEIsTUFBOUIsRUFBc0M7QUFDbEM7QUFDSDs7QUFDRCxVQUFNK0MsU0FBUyxHQUFHaFEsSUFBSSxDQUFDaVEsWUFBTCxFQUFsQjs7QUFDQSxRQUFJRCxTQUFKLEVBQWU7QUFDWEUsTUFBQUEsS0FBSyxDQUFDQyxTQUFOLENBQWdCblEsSUFBSSxDQUFDQyxNQUFyQixFQUE2QitQLFNBQTdCO0FBQ0g7QUFDSixHQTM0QjJCOztBQTY0QjVCSSxFQUFBQSwwQkFBMEIsRUFBRSxVQUFTQyxTQUFULEVBQW9CO0FBQzVDLFFBQUksQ0FBQ0EsU0FBTCxFQUFnQjtBQUNaLGFBQU9DLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQixLQUFoQixDQUFQO0FBQ0g7O0FBRUQsUUFBSSxLQUFLaE0sS0FBTCxDQUFXeEQsYUFBWCxDQUF5QnlQLFVBQTdCLEVBQXlDO0FBQ3JDblMsTUFBQUEsUUFBUSxDQUFDLGdDQUFELENBQVI7QUFDQSxZQUFNb1MsYUFBYSxHQUFHLEtBQUszUSxPQUFMLENBQWE0USw0QkFBYixDQUNsQixLQUFLbk0sS0FBTCxDQUFXeEQsYUFETyxDQUF0QjtBQUVBLGFBQU8sS0FBSzRQLG1CQUFMLENBQXlCRixhQUF6QixDQUFQO0FBQ0gsS0FMRCxNQUtPO0FBQ0hwUyxNQUFBQSxRQUFRLENBQUMsd0JBQUQsQ0FBUjtBQUNBLGFBQU9pUyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsS0FBaEIsQ0FBUDtBQUNIO0FBQ0osR0EzNUIyQjtBQTY1QjVCSyxFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCO0FBQ0FwTyx3QkFBSW9KLFFBQUosQ0FBYTtBQUNUZixNQUFBQSxNQUFNLEVBQUUsYUFEQztBQUVUNUssTUFBQUEsTUFBTSxFQUFFLEtBQUtzRSxLQUFMLENBQVd2RSxJQUFYLENBQWdCQztBQUZmLEtBQWI7O0FBSUEsU0FBS29FLFFBQUwsQ0FBYztBQUFDakQsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBZCxFQU40QixDQU1LO0FBQ3BDLEdBcDZCMkI7QUFzNkI1QnlQLEVBQUFBLHVCQUF1QixFQUFFLFlBQVc7QUFDaEMsUUFBSTVKLFlBQUosRUFBa0I7QUFDZEEsTUFBQUEsWUFBWSxDQUFDNkosT0FBYixDQUFxQiwwQkFBMEIsS0FBS3ZNLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQS9ELEVBQXVFLElBQXZFO0FBQ0g7O0FBQ0QsU0FBS29FLFFBQUwsQ0FBYztBQUFDakQsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBZDtBQUNILEdBMzZCMkI7QUE2NkI1QnFGLEVBQUFBLG1CQUFtQixFQUFFLFVBQVN3RCxFQUFULEVBQWE7QUFDOUI7QUFDQSxRQUFJLEtBQUtuSyxPQUFMLElBQWdCLEtBQUtBLE9BQUwsQ0FBYWlSLE9BQWIsRUFBcEIsRUFBNEM7QUFDeEM7QUFDQTtBQUNBdk8sMEJBQUlvSixRQUFKLENBQWE7QUFDVGYsUUFBQUEsTUFBTSxFQUFFLHdCQURDO0FBRVRnQixRQUFBQSxlQUFlLEVBQUU7QUFDYmhCLFVBQUFBLE1BQU0sRUFBRSxXQURLO0FBRWJXLFVBQUFBLE9BQU8sRUFBRSxLQUFLM0YsVUFBTDtBQUZJO0FBRlIsT0FBYixFQUh3QyxDQVd4QztBQUNBO0FBRUE7QUFDQTs7O0FBQ0FyRCwwQkFBSW9KLFFBQUosQ0FBYTtBQUFDZixRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiLEVBaEJ3QyxDQWlCeEM7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0gsS0E5Q0QsTUE4Q087QUFDSHlGLE1BQUFBLE9BQU8sQ0FBQ0MsT0FBUixHQUFrQjNKLElBQWxCLENBQXVCLE1BQU07QUFDekIsY0FBTW9LLE9BQU8sR0FBRyxLQUFLekssS0FBTCxDQUFXcEgsZ0JBQVgsR0FDWixLQUFLb0gsS0FBTCxDQUFXcEgsZ0JBQVgsQ0FBNEI4UixhQURoQixHQUNnQzNRLFNBRGhEOztBQUVBa0MsNEJBQUlvSixRQUFKLENBQWE7QUFDVGYsVUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVHFHLFVBQUFBLElBQUksRUFBRTtBQUFFRCxZQUFBQSxhQUFhLEVBQUVELE9BQWpCO0FBQTBCMVIsWUFBQUEsVUFBVSxFQUFFLEtBQUtpSCxLQUFMLENBQVdqSDtBQUFqRDtBQUZHLFNBQWI7O0FBSUEsZUFBT2dSLE9BQU8sQ0FBQ0MsT0FBUixFQUFQO0FBQ0gsT0FSRDtBQVNIO0FBRUosR0F6K0IyQjtBQTIrQjVCWSxFQUFBQSxtQkFBbUIsRUFBRSxVQUFTbEgsRUFBVCxFQUFhO0FBQzlCLFFBQUksS0FBS2pCLGFBQUwsQ0FBbUJDLHFCQUFuQixFQUFKLEVBQWdEO0FBQzVDLFdBQUs1RSxRQUFMLENBQWM7QUFDVnpELFFBQUFBLGlCQUFpQixFQUFFLENBRFQ7QUFFVmtCLFFBQUFBLG1CQUFtQixFQUFFO0FBRlgsT0FBZDtBQUlILEtBTEQsTUFLTztBQUNILFdBQUt1QyxRQUFMLENBQWM7QUFDVnZDLFFBQUFBLG1CQUFtQixFQUFFO0FBRFgsT0FBZDtBQUdIOztBQUNELFNBQUtzUCwyQkFBTDtBQUNILEdBdi9CMkI7QUF5L0I1QnRJLEVBQUFBLFVBQVUsRUFBRSxVQUFTbUIsRUFBVCxFQUFhO0FBQ3JCQSxJQUFBQSxFQUFFLENBQUNTLGVBQUg7QUFDQVQsSUFBQUEsRUFBRSxDQUFDVSxjQUFIO0FBRUFWLElBQUFBLEVBQUUsQ0FBQ29ILFlBQUgsQ0FBZ0JDLFVBQWhCLEdBQTZCLE1BQTdCO0FBRUEsVUFBTUMsS0FBSyxHQUFHLENBQUMsR0FBR3RILEVBQUUsQ0FBQ29ILFlBQUgsQ0FBZ0JFLEtBQXBCLENBQWQ7O0FBQ0EsUUFBSUEsS0FBSyxDQUFDaEssTUFBTixJQUFnQixDQUFwQixFQUF1QjtBQUNuQixZQUFNaUssZUFBZSxHQUFHRCxLQUFLLENBQUNFLEtBQU4sQ0FBWSxVQUFTQyxJQUFULEVBQWU7QUFDL0MsZUFBT0EsSUFBSSxDQUFDQyxJQUFMLElBQWEsTUFBcEI7QUFDSCxPQUZ1QixDQUF4Qjs7QUFJQSxVQUFJSCxlQUFKLEVBQXFCO0FBQ2pCLGFBQUtuTixRQUFMLENBQWM7QUFBRXhELFVBQUFBLFlBQVksRUFBRTtBQUFoQixTQUFkO0FBQ0FvSixRQUFBQSxFQUFFLENBQUNvSCxZQUFILENBQWdCQyxVQUFoQixHQUE2QixNQUE3QjtBQUNIO0FBQ0o7QUFDSixHQTFnQzJCO0FBNGdDNUJ6SSxFQUFBQSxNQUFNLEVBQUUsVUFBU29CLEVBQVQsRUFBYTtBQUNqQkEsSUFBQUEsRUFBRSxDQUFDUyxlQUFIO0FBQ0FULElBQUFBLEVBQUUsQ0FBQ1UsY0FBSDs7QUFDQWQsNkJBQWdCQyxjQUFoQixHQUFpQ3VCLHFCQUFqQyxDQUNJcEIsRUFBRSxDQUFDb0gsWUFBSCxDQUFnQk8sS0FEcEIsRUFDMkIsS0FBS3JOLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BRDNDLEVBQ21ELEtBQUtILE9BRHhEOztBQUdBLFNBQUt1RSxRQUFMLENBQWM7QUFBRXhELE1BQUFBLFlBQVksRUFBRTtBQUFoQixLQUFkOztBQUNBMkIsd0JBQUlvSixRQUFKLENBQWE7QUFBQ2YsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjtBQUNILEdBcGhDMkI7QUFzaEM1QjlCLEVBQUFBLGdCQUFnQixFQUFFLFVBQVNrQixFQUFULEVBQWE7QUFDM0JBLElBQUFBLEVBQUUsQ0FBQ1MsZUFBSDtBQUNBVCxJQUFBQSxFQUFFLENBQUNVLGNBQUg7QUFDQSxTQUFLdEcsUUFBTCxDQUFjO0FBQUV4RCxNQUFBQSxZQUFZLEVBQUU7QUFBaEIsS0FBZDtBQUNILEdBMWhDMkI7QUE0aEM1QmtLLEVBQUFBLGFBQWEsRUFBRSxVQUFTRyxHQUFULEVBQWN4RSxJQUFkLEVBQW9CbUwsSUFBcEIsRUFBMEI7QUFDckMsUUFBSSxLQUFLL1IsT0FBTCxDQUFhaVIsT0FBYixFQUFKLEVBQTRCO0FBQ3hCdk8sMEJBQUlvSixRQUFKLENBQWE7QUFBQ2YsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjs7QUFDQTtBQUNIOztBQUVEaEIsNkJBQWdCQyxjQUFoQixHQUFpQ2dJLHdCQUFqQyxDQUEwRDVHLEdBQTFELEVBQStELEtBQUszRyxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUEvRSxFQUF1RnlHLElBQXZGLEVBQTZGbUwsSUFBN0YsRUFBbUcsS0FBSy9SLE9BQXhHLEVBQ0s4RyxJQURMLENBQ1V0RyxTQURWLEVBQ3NCOE0sS0FBRCxJQUFXO0FBQ3hCLFVBQUlBLEtBQUssQ0FBQ2hDLElBQU4sS0FBZSxvQkFBbkIsRUFBeUM7QUFDckM7QUFDQTtBQUNIO0FBQ0osS0FOTDtBQU9ILEdBemlDMkI7QUEyaUM1QjJHLEVBQUFBLFFBQVEsRUFBRSxVQUFTQyxJQUFULEVBQWVDLEtBQWYsRUFBc0I7QUFDNUIsU0FBSzVOLFFBQUwsQ0FBYztBQUNWNk4sTUFBQUEsVUFBVSxFQUFFRixJQURGO0FBRVZHLE1BQUFBLFdBQVcsRUFBRUYsS0FGSDtBQUdWbFIsTUFBQUEsYUFBYSxFQUFFLEVBSEw7QUFJVnFSLE1BQUFBLGdCQUFnQixFQUFFO0FBSlIsS0FBZCxFQUQ0QixDQVE1QjtBQUNBOztBQUNBLFFBQUksS0FBS2hPLG1CQUFMLENBQXlCc0UsT0FBN0IsRUFBc0M7QUFDbEMsV0FBS3RFLG1CQUFMLENBQXlCc0UsT0FBekIsQ0FBaUMySixnQkFBakM7QUFDSCxLQVoyQixDQWM1QjtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixJQUFJQyxJQUFKLEdBQVdDLE9BQVgsRUFBaEI7QUFFQSxRQUFJdlMsTUFBSjtBQUNBLFFBQUlnUyxLQUFLLEtBQUssTUFBZCxFQUFzQmhTLE1BQU0sR0FBRyxLQUFLc0UsS0FBTCxDQUFXdkUsSUFBWCxDQUFnQkMsTUFBekI7QUFFdEI1QixJQUFBQSxRQUFRLENBQUMsd0JBQUQsQ0FBUjtBQUNBLFVBQU1vUyxhQUFhLEdBQUcsd0JBQVl1QixJQUFaLEVBQWtCL1IsTUFBbEIsQ0FBdEI7O0FBQ0EsU0FBSzBRLG1CQUFMLENBQXlCRixhQUF6QjtBQUNILEdBcmtDMkI7QUF1a0M1QkUsRUFBQUEsbUJBQW1CLEVBQUUsVUFBU0YsYUFBVCxFQUF3QjtBQUN6QyxVQUFNZ0MsSUFBSSxHQUFHLElBQWIsQ0FEeUMsQ0FHekM7QUFDQTs7QUFDQSxVQUFNQyxhQUFhLEdBQUcsS0FBS0osUUFBM0I7QUFFQSxTQUFLak8sUUFBTCxDQUFjO0FBQ1ZzTyxNQUFBQSxnQkFBZ0IsRUFBRTtBQURSLEtBQWQ7QUFJQSxXQUFPbEMsYUFBYSxDQUFDN0osSUFBZCxDQUFtQixVQUFTZ00sT0FBVCxFQUFrQjtBQUN4Q3ZVLE1BQUFBLFFBQVEsQ0FBQyxpQkFBRCxDQUFSOztBQUNBLFVBQUlvVSxJQUFJLENBQUNoTyxTQUFMLElBQWtCLENBQUNnTyxJQUFJLENBQUNsTyxLQUFMLENBQVd6RCxTQUE5QixJQUEyQzJSLElBQUksQ0FBQ0gsUUFBTCxJQUFpQkksYUFBaEUsRUFBK0U7QUFDM0VqVSxRQUFBQSxPQUFPLENBQUMyTyxLQUFSLENBQWMsaUNBQWQ7QUFDQTtBQUNILE9BTHVDLENBT3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLFVBQUl5RixVQUFVLEdBQUdELE9BQU8sQ0FBQ0MsVUFBekI7O0FBQ0EsVUFBSUEsVUFBVSxDQUFDQyxPQUFYLENBQW1CTCxJQUFJLENBQUNsTyxLQUFMLENBQVcyTixVQUE5QixJQUE0QyxDQUFoRCxFQUFtRDtBQUMvQ1csUUFBQUEsVUFBVSxHQUFHQSxVQUFVLENBQUNFLE1BQVgsQ0FBa0JOLElBQUksQ0FBQ2xPLEtBQUwsQ0FBVzJOLFVBQTdCLENBQWI7QUFDSCxPQWhCdUMsQ0FrQnhDO0FBQ0E7OztBQUNBVyxNQUFBQSxVQUFVLEdBQUdBLFVBQVUsQ0FBQ0csSUFBWCxDQUFnQixVQUFTQyxDQUFULEVBQVlDLENBQVosRUFBZTtBQUN4QyxlQUFPQSxDQUFDLENBQUMzTCxNQUFGLEdBQVcwTCxDQUFDLENBQUMxTCxNQUFwQjtBQUNILE9BRlksQ0FBYjtBQUlBa0wsTUFBQUEsSUFBSSxDQUFDcE8sUUFBTCxDQUFjO0FBQ1YrTixRQUFBQSxnQkFBZ0IsRUFBRVMsVUFEUjtBQUVWOVIsUUFBQUEsYUFBYSxFQUFFNlI7QUFGTCxPQUFkO0FBSUgsS0E1Qk0sRUE0QkosVUFBU3hGLEtBQVQsRUFBZ0I7QUFDZixZQUFNK0YsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0E1VSxNQUFBQSxPQUFPLENBQUMyTyxLQUFSLENBQWMsb0JBQW9CQSxLQUFsQzs7QUFDQWtHLHFCQUFNQyxtQkFBTixDQUEwQixlQUExQixFQUEyQyxFQUEzQyxFQUErQ0osV0FBL0MsRUFBNEQ7QUFDeERLLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxlQUFILENBRGlEO0FBRXhEckksUUFBQUEsV0FBVyxFQUFJaUMsS0FBSyxJQUFJQSxLQUFLLENBQUNxRyxPQUFoQixHQUEyQnJHLEtBQUssQ0FBQ3FHLE9BQWpDLEdBQTJDLHlCQUFHLCtEQUFIO0FBRkQsT0FBNUQ7QUFJSCxLQW5DTSxFQW1DSkMsT0FuQ0ksQ0FtQ0ksWUFBVztBQUNsQmpCLE1BQUFBLElBQUksQ0FBQ3BPLFFBQUwsQ0FBYztBQUNWc08sUUFBQUEsZ0JBQWdCLEVBQUU7QUFEUixPQUFkO0FBR0gsS0F2Q00sQ0FBUDtBQXdDSCxHQTFuQzJCO0FBNG5DNUJnQixFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFVBQU1DLGdCQUFnQixHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXpCO0FBQ0EsVUFBTVEsT0FBTyxHQUFHVCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCLENBRjZCLENBSTdCO0FBQ0E7O0FBRUEsVUFBTVMsR0FBRyxHQUFHLEVBQVo7O0FBRUEsUUFBSSxLQUFLdlAsS0FBTCxDQUFXb08sZ0JBQWYsRUFBaUM7QUFDN0JtQixNQUFBQSxHQUFHLENBQUNDLElBQUosQ0FBUztBQUFJLFFBQUEsR0FBRyxFQUFDO0FBQVIsU0FDSiw2QkFBQyxPQUFELE9BREksQ0FBVDtBQUdIOztBQUVELFFBQUksQ0FBQyxLQUFLeFAsS0FBTCxDQUFXeEQsYUFBWCxDQUF5QnlQLFVBQTlCLEVBQTBDO0FBQ3RDLFVBQUksS0FBS2pNLEtBQUwsQ0FBV3hELGFBQVgsQ0FBeUI2UixPQUF6QixDQUFpQ3JMLE1BQWpDLElBQTJDLENBQS9DLEVBQWtEO0FBQzlDdU0sUUFBQUEsR0FBRyxDQUFDQyxJQUFKLENBQVM7QUFBSSxVQUFBLEdBQUcsRUFBQztBQUFSLFdBQ0o7QUFBSSxVQUFBLFNBQVMsRUFBQztBQUFkLFdBQXdDLHlCQUFHLFlBQUgsQ0FBeEMsQ0FESSxDQUFUO0FBSUgsT0FMRCxNQUtPO0FBQ0hELFFBQUFBLEdBQUcsQ0FBQ0MsSUFBSixDQUFTO0FBQUksVUFBQSxHQUFHLEVBQUM7QUFBUixXQUNKO0FBQUksVUFBQSxTQUFTLEVBQUM7QUFBZCxXQUF3Qyx5QkFBRyxpQkFBSCxDQUF4QyxDQURJLENBQVQ7QUFJSDtBQUNKLEtBM0I0QixDQTZCN0I7QUFDQTs7O0FBQ0EsVUFBTUMsZUFBZSxHQUFHLE1BQU07QUFDMUIsWUFBTUMsV0FBVyxHQUFHLEtBQUs3UCxtQkFBTCxDQUF5QnNFLE9BQTdDOztBQUNBLFVBQUl1TCxXQUFKLEVBQWlCO0FBQ2JBLFFBQUFBLFdBQVcsQ0FBQ0MsV0FBWjtBQUNIO0FBQ0osS0FMRDs7QUFPQSxRQUFJQyxVQUFKOztBQUVBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLEtBQUs3UCxLQUFMLENBQVd4RCxhQUFYLENBQXlCNlIsT0FBekIsQ0FBaUNyTCxNQUFqQyxHQUEwQyxDQUF2RCxFQUEwRDZNLENBQUMsSUFBSSxDQUEvRCxFQUFrRUEsQ0FBQyxFQUFuRSxFQUF1RTtBQUNuRSxZQUFNQyxNQUFNLEdBQUcsS0FBSzlQLEtBQUwsQ0FBV3hELGFBQVgsQ0FBeUI2UixPQUF6QixDQUFpQ3dCLENBQWpDLENBQWY7QUFFQSxZQUFNRSxJQUFJLEdBQUdELE1BQU0sQ0FBQ3ZVLE9BQVAsQ0FBZXlVLFFBQWYsRUFBYjtBQUNBLFlBQU10VSxNQUFNLEdBQUdxVSxJQUFJLENBQUM1UCxTQUFMLEVBQWY7QUFDQSxZQUFNMUUsSUFBSSxHQUFHLEtBQUtGLE9BQUwsQ0FBYXNGLE9BQWIsQ0FBcUJuRixNQUFyQixDQUFiOztBQUVBLFVBQUksQ0FBQyxpQ0FBaUJxVSxJQUFqQixDQUFMLEVBQTZCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNIOztBQUVELFVBQUksS0FBSy9QLEtBQUwsQ0FBVzROLFdBQVgsS0FBMkIsS0FBL0IsRUFBc0M7QUFDbEMsWUFBSWxTLE1BQU0sSUFBSWtVLFVBQWQsRUFBMEI7QUFFdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBTUssUUFBUSxHQUFHeFUsSUFBSSxHQUFHQSxJQUFJLENBQUNvTCxJQUFSLEdBQWUseUJBQUcseUJBQUgsRUFBOEI7QUFBRW5MLFlBQUFBLE1BQU0sRUFBRUE7QUFBVixXQUE5QixDQUFwQztBQUVBNlQsVUFBQUEsR0FBRyxDQUFDQyxJQUFKLENBQVM7QUFBSSxZQUFBLEdBQUcsRUFBRU8sSUFBSSxDQUFDRyxLQUFMLEtBQWU7QUFBeEIsYUFDSSx5Q0FBTSx5QkFBRyxNQUFILENBQU4sUUFBc0JELFFBQXRCLENBREosQ0FBVDtBQUdBTCxVQUFBQSxVQUFVLEdBQUdsVSxNQUFiO0FBQ0g7QUFDSjs7QUFFRCxZQUFNeVUsVUFBVSxHQUFHLFlBQVV6VSxNQUFWLEdBQWlCLEdBQWpCLEdBQXFCcVUsSUFBSSxDQUFDRyxLQUFMLEVBQXhDO0FBRUFYLE1BQUFBLEdBQUcsQ0FBQ0MsSUFBSixDQUFTLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsR0FBRyxFQUFFTyxJQUFJLENBQUNHLEtBQUwsRUFBdkI7QUFDQSxRQUFBLFlBQVksRUFBRUosTUFEZDtBQUVBLFFBQUEsZ0JBQWdCLEVBQUUsS0FBSzlQLEtBQUwsQ0FBVzZOLGdCQUY3QjtBQUdBLFFBQUEsVUFBVSxFQUFFc0MsVUFIWjtBQUlBLFFBQUEsZ0JBQWdCLEVBQUUsS0FBSzVPLDJCQUFMLENBQWlDOUYsSUFBakMsQ0FKbEI7QUFLQSxRQUFBLGVBQWUsRUFBRWdVO0FBTGpCLFFBQVQ7QUFNSDs7QUFDRCxXQUFPRixHQUFQO0FBQ0gsR0Ezc0MyQjtBQTZzQzVCYSxFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QixVQUFNQyxnQkFBZ0IsR0FBRyxDQUFDLEtBQUtyUSxLQUFMLENBQVdqRCxhQUFyQztBQUNBLFVBQU1yQixNQUFNLEdBQUcsS0FBS3NFLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQS9CO0FBQ0EsU0FBS29FLFFBQUwsQ0FBYztBQUFDL0MsTUFBQUEsYUFBYSxFQUFFc1QsZ0JBQWhCO0FBQWtDOVQsTUFBQUEsU0FBUyxFQUFFO0FBQTdDLEtBQWQ7O0FBQ0FrRCwyQkFBYzZRLFFBQWQsQ0FBdUIscUJBQXZCLEVBQThDNVUsTUFBOUMsRUFBc0Q2VSw0QkFBYUMsV0FBbkUsRUFBZ0ZILGdCQUFoRjtBQUNILEdBbHRDMkI7QUFvdEM1QkksRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEJ4Uyx3QkFBSW9KLFFBQUosQ0FBYTtBQUFFZixNQUFBQSxNQUFNLEVBQUU7QUFBVixLQUFiO0FBQ0gsR0F0dEMyQjtBQXd0QzVCb0ssRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEJ4VyxJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSwrQkFBWjtBQUNBLFNBQUsyUCxVQUFMOztBQUNBLFFBQUksS0FBSzlKLEtBQUwsQ0FBVzVELGVBQWYsRUFBZ0M7QUFDNUI2QiwwQkFBSW9KLFFBQUosQ0FBYTtBQUNUZixRQUFBQSxNQUFNLEVBQUUsZUFEQztBQUVUakIsUUFBQUEsS0FBSyxFQUFFO0FBRkUsT0FBYjtBQUlIOztBQUNEcEgsd0JBQUlvSixRQUFKLENBQWE7QUFBQ2YsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjtBQUNILEdBbHVDMkI7QUFvdUM1QnFLLEVBQUFBLFlBQVksRUFBRSxZQUFXO0FBQ3JCMVMsd0JBQUlvSixRQUFKLENBQWE7QUFDVGYsTUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVFcsTUFBQUEsT0FBTyxFQUFFLEtBQUtqSCxLQUFMLENBQVd2RSxJQUFYLENBQWdCQztBQUZoQixLQUFiO0FBSUgsR0F6dUMyQjtBQTJ1QzVCa1YsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEIsU0FBS3JWLE9BQUwsQ0FBYXNWLE1BQWIsQ0FBb0IsS0FBSzdRLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQXBDLEVBQTRDMkcsSUFBNUMsQ0FBaUQsWUFBVztBQUN4RHBFLDBCQUFJb0osUUFBSixDQUFhO0FBQUVmLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BQWI7QUFDSCxLQUZELEVBRUcsVUFBUy9ELEdBQVQsRUFBYztBQUNiLFlBQU11TyxPQUFPLEdBQUd2TyxHQUFHLENBQUNDLE9BQUosSUFBZSx5QkFBRyxvQkFBSCxDQUEvQjtBQUNBLFlBQU1vTSxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQix1QkFBMUIsRUFBbUQsRUFBbkQsRUFBdURKLFdBQXZELEVBQW9FO0FBQ2hFSyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUR5RDtBQUVoRXJJLFFBQUFBLFdBQVcsRUFBRSx5QkFBRyxtQ0FBSCxFQUF3QztBQUFFa0ssVUFBQUEsT0FBTyxFQUFFQTtBQUFYLFNBQXhDO0FBRm1ELE9BQXBFO0FBSUgsS0FURDtBQVVILEdBdHZDMkI7QUF3dkM1QkMsRUFBQUEscUJBQXFCLEVBQUUsVUFBU3JMLEVBQVQsRUFBYTtBQUNoQyxVQUFNd0ksSUFBSSxHQUFHLElBQWI7QUFDQSxTQUFLcE8sUUFBTCxDQUFjO0FBQ1ZrUixNQUFBQSxTQUFTLEVBQUU7QUFERCxLQUFkO0FBR0EsU0FBS3pWLE9BQUwsQ0FBYTBWLEtBQWIsQ0FBbUIsS0FBS2pSLEtBQUwsQ0FBV3RFLE1BQTlCLEVBQXNDMkcsSUFBdEMsQ0FBMkMsWUFBVztBQUNsRHBFLDBCQUFJb0osUUFBSixDQUFhO0FBQUVmLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BQWI7O0FBQ0E0SCxNQUFBQSxJQUFJLENBQUNwTyxRQUFMLENBQWM7QUFDVmtSLFFBQUFBLFNBQVMsRUFBRTtBQURELE9BQWQ7QUFHSCxLQUxELEVBS0csVUFBU25JLEtBQVQsRUFBZ0I7QUFDZjNPLE1BQUFBLE9BQU8sQ0FBQzJPLEtBQVIsQ0FBYyw2QkFBZCxFQUE2Q0EsS0FBN0M7QUFFQSxZQUFNcUksR0FBRyxHQUFHckksS0FBSyxDQUFDcUcsT0FBTixHQUFnQnJHLEtBQUssQ0FBQ3FHLE9BQXRCLEdBQWdDaUMsSUFBSSxDQUFDQyxTQUFMLENBQWV2SSxLQUFmLENBQTVDO0FBQ0EsWUFBTStGLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQTBCLHlCQUExQixFQUFxRCxFQUFyRCxFQUF5REosV0FBekQsRUFBc0U7QUFDbEVLLFFBQUFBLEtBQUssRUFBRSx5QkFBRyx5QkFBSCxDQUQyRDtBQUVsRXJJLFFBQUFBLFdBQVcsRUFBRXNLO0FBRnFELE9BQXRFOztBQUtBaEQsTUFBQUEsSUFBSSxDQUFDcE8sUUFBTCxDQUFjO0FBQ1ZrUixRQUFBQSxTQUFTLEVBQUUsS0FERDtBQUVWSyxRQUFBQSxXQUFXLEVBQUV4STtBQUZILE9BQWQ7QUFJSCxLQW5CRDtBQW9CSCxHQWp4QzJCO0FBbXhDNUJ5SSxFQUFBQSxzQkFBc0IsRUFBRSxrQkFBaUI7QUFDckMsU0FBS3hSLFFBQUwsQ0FBYztBQUNWa1IsTUFBQUEsU0FBUyxFQUFFO0FBREQsS0FBZDs7QUFJQSxRQUFJO0FBQ0EsWUFBTU8sUUFBUSxHQUFHLEtBQUt2UixLQUFMLENBQVd2RSxJQUFYLENBQWdCK04sU0FBaEIsQ0FBMEIsS0FBS2pPLE9BQUwsQ0FBYWtQLFNBQWIsRUFBMUIsQ0FBakI7QUFDQSxZQUFNK0csV0FBVyxHQUFHRCxRQUFRLENBQUNFLE1BQVQsQ0FBZ0JwSCxNQUFwQztBQUNBLFlBQU1xSCxZQUFZLEdBQUcsS0FBS25XLE9BQUwsQ0FBYW9XLGVBQWIsRUFBckI7QUFDQUQsTUFBQUEsWUFBWSxDQUFDbEMsSUFBYixDQUFrQmdDLFdBQVcsQ0FBQ3hKLFNBQVosRUFBbEIsRUFKQSxDQUk0Qzs7QUFDNUMsWUFBTSxLQUFLek0sT0FBTCxDQUFhcVcsZUFBYixDQUE2QkYsWUFBN0IsQ0FBTjtBQUVBLFlBQU0sS0FBS25XLE9BQUwsQ0FBYTBWLEtBQWIsQ0FBbUIsS0FBS2pSLEtBQUwsQ0FBV3RFLE1BQTlCLENBQU47O0FBQ0F1QywwQkFBSW9KLFFBQUosQ0FBYTtBQUFFZixRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiOztBQUNBLFdBQUt4RyxRQUFMLENBQWM7QUFDVmtSLFFBQUFBLFNBQVMsRUFBRTtBQURELE9BQWQ7QUFHSCxLQVpELENBWUUsT0FBT25JLEtBQVAsRUFBYztBQUNaM08sTUFBQUEsT0FBTyxDQUFDMk8sS0FBUixDQUFjLDZCQUFkLEVBQTZDQSxLQUE3QztBQUVBLFlBQU1xSSxHQUFHLEdBQUdySSxLQUFLLENBQUNxRyxPQUFOLEdBQWdCckcsS0FBSyxDQUFDcUcsT0FBdEIsR0FBZ0NpQyxJQUFJLENBQUNDLFNBQUwsQ0FBZXZJLEtBQWYsQ0FBNUM7QUFDQSxZQUFNK0YsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIseUJBQTFCLEVBQXFELEVBQXJELEVBQXlESixXQUF6RCxFQUFzRTtBQUNsRUssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLHlCQUFILENBRDJEO0FBRWxFckksUUFBQUEsV0FBVyxFQUFFc0s7QUFGcUQsT0FBdEU7O0FBS0FoRCxNQUFBQSxJQUFJLENBQUNwTyxRQUFMLENBQWM7QUFDVmtSLFFBQUFBLFNBQVMsRUFBRSxLQUREO0FBRVZLLFFBQUFBLFdBQVcsRUFBRXhJO0FBRkgsT0FBZDtBQUlIO0FBQ0osR0FuekMyQjtBQXF6QzVCZ0osRUFBQUEsbUNBQW1DLEVBQUUsVUFBU25NLEVBQVQsRUFBYTtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBekgsd0JBQUlvSixRQUFKLENBQWE7QUFDVGYsTUFBQUEsTUFBTSxFQUFFO0FBREMsS0FBYjtBQUdILEdBN3pDMkI7QUErekM1QndMLEVBQUFBLGFBQWEsRUFBRSxZQUFXO0FBQ3RCLFNBQUtoUyxRQUFMLENBQWM7QUFDVnZELE1BQUFBLFNBQVMsRUFBRSxDQUFDLEtBQUt5RCxLQUFMLENBQVd6RCxTQURiO0FBRVZRLE1BQUFBLGFBQWEsRUFBRTtBQUZMLEtBQWQ7QUFJSCxHQXAwQzJCO0FBczBDNUJvSyxFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCLFNBQUtySCxRQUFMLENBQWM7QUFDVnZELE1BQUFBLFNBQVMsRUFBRSxLQUREO0FBRVZDLE1BQUFBLGFBQWEsRUFBRTtBQUZMLEtBQWQ7QUFJSCxHQTMwQzJCO0FBNjBDNUI7QUFDQXVWLEVBQUFBLGtCQUFrQixFQUFFLFlBQVc7QUFDM0IsU0FBS3ROLGFBQUwsQ0FBbUJzTixrQkFBbkI7O0FBQ0E5VCx3QkFBSW9KLFFBQUosQ0FBYTtBQUFDZixNQUFBQSxNQUFNLEVBQUU7QUFBVCxLQUFiO0FBQ0gsR0FqMUMyQjtBQW0xQzVCO0FBQ0EwTCxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCLFNBQUt2TixhQUFMLENBQW1CdU4sZ0JBQW5CO0FBQ0gsR0F0MUMyQjtBQXcxQzVCO0FBQ0FDLEVBQUFBLGdCQUFnQixFQUFFLFVBQVN2TSxFQUFULEVBQWE7QUFDM0JBLElBQUFBLEVBQUUsQ0FBQ1MsZUFBSDs7QUFDQSxTQUFLMUIsYUFBTCxDQUFtQndOLGdCQUFuQjtBQUNILEdBNTFDMkI7QUE4MUM1QjtBQUNBcEYsRUFBQUEsMkJBQTJCLEVBQUUsWUFBVztBQUNwQyxRQUFJLENBQUMsS0FBS3BJLGFBQVYsRUFBeUI7QUFDckI7QUFDSDs7QUFFRCxVQUFNeU4sT0FBTyxHQUFHLEtBQUt6TixhQUFMLENBQW1CME4sbUJBQW5CLEVBQWhCOztBQUNBLFFBQUksS0FBS25TLEtBQUwsQ0FBV3ZDLHdCQUFYLElBQXVDeVUsT0FBM0MsRUFBb0Q7QUFDaEQsV0FBS3BTLFFBQUwsQ0FBYztBQUFDckMsUUFBQUEsd0JBQXdCLEVBQUV5VTtBQUEzQixPQUFkO0FBQ0g7QUFDSixHQXgyQzJCO0FBMDJDNUI7QUFDQTtBQUNBO0FBQ0FyTixFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixVQUFNdU4sWUFBWSxHQUFHLEtBQUszTixhQUExQjtBQUNBLFFBQUksQ0FBQzJOLFlBQUwsRUFBbUIsT0FBTyxJQUFQLENBRkssQ0FJeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFJLEtBQUtwUyxLQUFMLENBQVd6QyxtQkFBZixFQUFvQztBQUNoQyxhQUFPLElBQVA7QUFDSDs7QUFFRCxVQUFNOFUsV0FBVyxHQUFHRCxZQUFZLENBQUNsUixjQUFiLEVBQXBCLENBaEJ3QixDQWtCeEI7O0FBQ0EsUUFBSSxDQUFDbVIsV0FBRCxJQUFnQkEsV0FBVyxDQUFDQyxhQUFoQyxFQUErQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsV0FBTztBQUNIblIsTUFBQUEsYUFBYSxFQUFFa1IsV0FBVyxDQUFDRSxrQkFEeEI7QUFFSG5SLE1BQUFBLFdBQVcsRUFBRWlSLFdBQVcsQ0FBQ2pSO0FBRnRCLEtBQVA7QUFJSCxHQWg1QzJCO0FBazVDNUJ1QyxFQUFBQSxRQUFRLEVBQUUsWUFBVztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0EsUUFBSWpHLGlCQUFpQixHQUFHNkYsTUFBTSxDQUFDaVAsV0FBUCxJQUNmLEtBQUs7QUFDTCxNQURBLEdBQ0s7QUFDTCxNQUZBLEdBRUs7QUFDTCxPQUplLENBQXhCLENBUGlCLENBV0Y7QUFFZjtBQUNBOztBQUNBLFFBQUk5VSxpQkFBaUIsR0FBRyxFQUF4QixFQUE0QkEsaUJBQWlCLEdBQUcsRUFBcEI7QUFFNUIsU0FBS29DLFFBQUwsQ0FBYztBQUFDcEMsTUFBQUEsaUJBQWlCLEVBQUVBO0FBQXBCLEtBQWQ7QUFDSCxHQXA2QzJCO0FBczZDNUIrVSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCeFUsd0JBQUlvSixRQUFKLENBQWE7QUFDVGYsTUFBQUEsTUFBTSxFQUFFLGtCQURDO0FBRVRvTSxNQUFBQSxVQUFVLEVBQUU7QUFGSCxLQUFiLEVBR0csSUFISDtBQUlILEdBMzZDMkI7QUE2NkM1QjFNLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsVUFBTTdDLElBQUksR0FBRyxLQUFLQyxlQUFMLEVBQWI7O0FBQ0EsUUFBSSxDQUFDRCxJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNELFVBQU0vQyxRQUFRLEdBQUcsQ0FBQytDLElBQUksQ0FBQ3dQLGlCQUFMLEVBQWxCO0FBQ0F4UCxJQUFBQSxJQUFJLENBQUN5UCxrQkFBTCxDQUF3QnhTLFFBQXhCO0FBQ0EsU0FBSzRHLFdBQUwsR0FQeUIsQ0FPTDtBQUN2QixHQXI3QzJCO0FBdTdDNUJkLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsVUFBTS9DLElBQUksR0FBRyxLQUFLQyxlQUFMLEVBQWI7O0FBQ0EsUUFBSSxDQUFDRCxJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNELFVBQU0vQyxRQUFRLEdBQUcsQ0FBQytDLElBQUksQ0FBQzBQLGlCQUFMLEVBQWxCO0FBQ0ExUCxJQUFBQSxJQUFJLENBQUMyUCxrQkFBTCxDQUF3QjFTLFFBQXhCO0FBQ0EsU0FBSzRHLFdBQUwsR0FQeUIsQ0FPTDtBQUN2QixHQS83QzJCO0FBaThDNUIrTCxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCLFFBQUksS0FBSzdTLFNBQVQsRUFBb0I7QUFDcEIsU0FBS0osUUFBTCxDQUFjO0FBQ1ZuQyxNQUFBQSxnQkFBZ0IsRUFBRTtBQURSLEtBQWQ7QUFHSCxHQXQ4QzJCO0FBdzhDNUJxVixFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCO0FBQ0EsUUFBSSxLQUFLOVMsU0FBVCxFQUFvQjtBQUNwQixTQUFLSixRQUFMLENBQWM7QUFDVm5DLE1BQUFBLGdCQUFnQixFQUFFO0FBRFIsS0FBZDtBQUdILEdBOThDMkI7O0FBZzlDNUI7Ozs7O0FBS0FzVixFQUFBQSxlQUFlLEVBQUUsVUFBU3ZOLEVBQVQsRUFBYTtBQUMxQixRQUFJd04sS0FBSjs7QUFDQSxRQUFJLEtBQUtyVCxtQkFBTCxDQUF5QnNFLE9BQTdCLEVBQXNDO0FBQ2xDK08sTUFBQUEsS0FBSyxHQUFHLEtBQUtyVCxtQkFBTCxDQUF5QnNFLE9BQWpDO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBS00sYUFBVCxFQUF3QjtBQUMzQnlPLE1BQUFBLEtBQUssR0FBRyxLQUFLek8sYUFBYjtBQUNIOztBQUVELFFBQUl5TyxLQUFKLEVBQVc7QUFDUEEsTUFBQUEsS0FBSyxDQUFDRCxlQUFOLENBQXNCdk4sRUFBdEI7QUFDSDtBQUNKLEdBaCtDMkI7O0FBaytDNUI7OztBQUdBdEMsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsUUFBSSxDQUFDLEtBQUtwRCxLQUFMLENBQVd2RSxJQUFoQixFQUFzQjtBQUNsQixhQUFPLElBQVA7QUFDSDs7QUFDRCxXQUFPMFgscUJBQVlDLGNBQVosQ0FBMkIsS0FBS3BULEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JDLE1BQTNDLENBQVA7QUFDSCxHQTErQzJCO0FBNCtDNUI7QUFDQTtBQUNBMlgsRUFBQUEsdUJBQXVCLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQ2pDLFNBQUs3TyxhQUFMLEdBQXFCNk8sQ0FBckI7O0FBQ0EsUUFBSUEsQ0FBSixFQUFPO0FBQ0hwWixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxrREFBWjtBQUNBLFdBQUsyUCxVQUFMO0FBQ0g7QUFDSixHQXAvQzJCO0FBcy9DNUJ5SixFQUFBQSxXQUFXLEVBQUUsWUFBVztBQUNwQixVQUFNQyxXQUFXLEdBQUcsS0FBS3hULEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JzTixZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsZUFBNUMsRUFBNkQsRUFBN0QsQ0FBcEI7QUFDQSxRQUFJLENBQUN3SyxXQUFELElBQWdCLENBQUNBLFdBQVcsQ0FBQ3ZLLFVBQVosR0FBeUIsYUFBekIsQ0FBckIsRUFBOEQsT0FBTyxJQUFQO0FBRTlELFdBQU8sS0FBSzFOLE9BQUwsQ0FBYXNGLE9BQWIsQ0FBcUIyUyxXQUFXLENBQUN2SyxVQUFaLEdBQXlCLGFBQXpCLEVBQXdDLFNBQXhDLENBQXJCLENBQVA7QUFDSCxHQTMvQzJCO0FBNi9DNUJ3SyxFQUFBQSx3QkFBd0IsRUFBRSxZQUFXO0FBQ2pDLFVBQU1DLE9BQU8sR0FBRyxLQUFLSCxXQUFMLEVBQWhCOztBQUNBLFFBQUksQ0FBQ0csT0FBTCxFQUFjLE9BQU8sQ0FBUDtBQUNkLFdBQU9BLE9BQU8sQ0FBQ0MsMEJBQVIsQ0FBbUMsV0FBbkMsQ0FBUDtBQUNILEdBamdEMkI7QUFtZ0Q1QkMsRUFBQUEsd0JBQXdCLEVBQUUsWUFBVztBQUNqQyxVQUFNRixPQUFPLEdBQUcsS0FBS0gsV0FBTCxFQUFoQjs7QUFDQSxRQUFJLENBQUNHLE9BQUwsRUFBYzs7QUFDZHpWLHdCQUFJb0osUUFBSixDQUFhO0FBQUNmLE1BQUFBLE1BQU0sRUFBRSxXQUFUO0FBQXNCVyxNQUFBQSxPQUFPLEVBQUV5TSxPQUFPLENBQUNoWTtBQUF2QyxLQUFiO0FBQ0gsR0F2Z0QyQjtBQXlnRDVCbVksRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxVQUFVLEdBQUdqRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQW5CO0FBQ0EsVUFBTWlGLGNBQWMsR0FBR2xGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBdkI7QUFDQSxVQUFNa0YsUUFBUSxHQUFHbkYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGdCQUFqQixDQUFqQjtBQUNBLFVBQU1tRixTQUFTLEdBQUdwRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUJBQWpCLENBQWxCO0FBQ0EsVUFBTW9GLGlCQUFpQixHQUFHckYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlCQUFqQixDQUExQjtBQUNBLFVBQU1xRixXQUFXLEdBQUd0RixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXBCO0FBQ0EsVUFBTXNGLFdBQVcsR0FBR3ZGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBcEI7QUFDQSxVQUFNdUYsY0FBYyxHQUFHeEYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUF2QjtBQUNBLFVBQU13RixhQUFhLEdBQUd6RixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQXRCO0FBQ0EsVUFBTXlGLHFCQUFxQixHQUFHMUYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDZCQUFqQixDQUE5QjtBQUNBLFVBQU0wRixvQkFBb0IsR0FBRzNGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw0QkFBakIsQ0FBN0I7QUFDQSxVQUFNMkYsYUFBYSxHQUFHNUYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF0Qjs7QUFFQSxRQUFJLENBQUMsS0FBSzlPLEtBQUwsQ0FBV3ZFLElBQWhCLEVBQXNCO0FBQ2xCLFlBQU1pWixPQUFPLEdBQUcsS0FBSzFVLEtBQUwsQ0FBV3JFLFdBQVgsSUFBMEIsS0FBS3FFLEtBQUwsQ0FBV3BFLFdBQXJEOztBQUNBLFVBQUk4WSxPQUFKLEVBQWE7QUFDVCxlQUNJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNJLDZCQUFDLGFBQUQsUUFDSSw2QkFBQyxjQUFEO0FBQ0ksVUFBQSxVQUFVLEVBQUUsS0FEaEI7QUFFSSxVQUFBLGNBQWMsRUFBRSxLQUFLMVUsS0FBTCxDQUFXcEUsV0FGL0I7QUFHSSxVQUFBLEtBQUssRUFBRSxLQUFLb0UsS0FBTCxDQUFXM0MsYUFIdEI7QUFJSSxVQUFBLE9BQU8sRUFBRXFYLE9BSmI7QUFLSSxVQUFBLE9BQU8sRUFBRSxLQUFLMVUsS0FBTCxDQUFXMUMsT0FMeEI7QUFNSSxVQUFBLE9BQU8sRUFBRSxLQUFLMEUsS0FBTCxDQUFXbEg7QUFOeEIsVUFESixDQURKLENBREo7QUFjSCxPQWZELE1BZU87QUFDSCxZQUFJNlosV0FBVyxHQUFHNVksU0FBbEI7O0FBQ0EsWUFBSSxLQUFLaUcsS0FBTCxDQUFXbEgsT0FBZixFQUF3QjtBQUNwQjZaLFVBQUFBLFdBQVcsR0FBRyxLQUFLM1MsS0FBTCxDQUFXbEgsT0FBWCxDQUFtQjZaLFdBQWpDO0FBQ0g7O0FBQ0QsWUFBSUMsWUFBWSxHQUFHN1ksU0FBbkI7O0FBQ0EsWUFBSSxLQUFLaUcsS0FBTCxDQUFXcEgsZ0JBQWYsRUFBaUM7QUFDN0JnYSxVQUFBQSxZQUFZLEdBQUcsS0FBSzVTLEtBQUwsQ0FBV3BILGdCQUFYLENBQTRCZ2EsWUFBM0M7QUFDSCxTQVJFLENBVUg7QUFDQTs7O0FBQ0EsY0FBTXZVLFNBQVMsR0FBRyxLQUFLTCxLQUFMLENBQVdLLFNBQTdCO0FBQ0EsZUFDSTtBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDSSw2QkFBQyxhQUFELFFBQ0ksNkJBQUMsY0FBRDtBQUFnQixVQUFBLFdBQVcsRUFBRSxLQUFLNkIsbUJBQWxDO0FBQ0ksVUFBQSxhQUFhLEVBQUUsS0FBSzBPLGFBRHhCO0FBRUksVUFBQSxhQUFhLEVBQUUsS0FBS2lCLG1DQUZ4QjtBQUdJLFVBQUEsVUFBVSxFQUFFLEtBSGhCO0FBR3VCLFVBQUEsS0FBSyxFQUFFLEtBQUs3UixLQUFMLENBQVczQyxhQUh6QztBQUlJLFVBQUEsU0FBUyxFQUFFZ0QsU0FKZjtBQUtJLFVBQUEsT0FBTyxFQUFFLEtBQUtMLEtBQUwsQ0FBVzFDLE9BTHhCO0FBTUksVUFBQSxXQUFXLEVBQUVxWCxXQU5qQjtBQU9JLFVBQUEsWUFBWSxFQUFFQyxZQVBsQjtBQVFJLFVBQUEsT0FBTyxFQUFFLEtBQUs1UyxLQUFMLENBQVdsSCxPQVJ4QjtBQVNJLFVBQUEsT0FBTyxFQUFFLEtBQUtrSCxLQUFMLENBQVdwSCxnQkFBWCxHQUE4QixLQUFLb0gsS0FBTCxDQUFXcEgsZ0JBQVgsQ0FBNEI4UixhQUExRCxHQUEwRSxJQVR2RjtBQVVJLFVBQUEsSUFBSSxFQUFFLEtBQUsxTSxLQUFMLENBQVd2RTtBQVZyQixVQURKLENBREosQ0FESjtBQWtCSDtBQUNKOztBQUVELFVBQU1vWixZQUFZLEdBQUcsS0FBSzdVLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0JpTixlQUFoQixFQUFyQjs7QUFDQSxRQUFJbU0sWUFBWSxJQUFJLFFBQXBCLEVBQThCO0FBQzFCLFVBQUksS0FBSzdVLEtBQUwsQ0FBVzFDLE9BQVgsSUFBc0IsS0FBSzBDLEtBQUwsQ0FBV2dSLFNBQXJDLEVBQWdEO0FBQzVDLGVBQ0ksNkJBQUMsYUFBRCxRQUNJLDZCQUFDLGNBQUQ7QUFDSSxVQUFBLFVBQVUsRUFBRSxLQURoQjtBQUVJLFVBQUEsS0FBSyxFQUFFLEtBQUtoUixLQUFMLENBQVczQyxhQUZ0QjtBQUdJLFVBQUEsT0FBTyxFQUFFLEtBQUsyQyxLQUFMLENBQVcxQyxPQUh4QjtBQUlJLFVBQUEsU0FBUyxFQUFFLEtBQUswQyxLQUFMLENBQVdnUjtBQUoxQixVQURKLENBREo7QUFVSCxPQVhELE1BV087QUFDSCxjQUFNOEQsUUFBUSxHQUFHLEtBQUt2WixPQUFMLENBQWEwTSxXQUFiLENBQXlCQyxNQUExQztBQUNBLGNBQU1xSixRQUFRLEdBQUcsS0FBS3ZSLEtBQUwsQ0FBV3ZFLElBQVgsQ0FBZ0IrTixTQUFoQixDQUEwQnNMLFFBQTFCLENBQWpCO0FBQ0EsY0FBTXRELFdBQVcsR0FBR0QsUUFBUSxDQUFDRSxNQUFULENBQWdCcEgsTUFBcEM7QUFDQSxZQUFJc0ssV0FBVyxHQUFHbkQsV0FBVyxDQUFDdUQsTUFBWixHQUFxQnZELFdBQVcsQ0FBQ3VELE1BQVosQ0FBbUJsTyxJQUF4QyxHQUErQzJLLFdBQVcsQ0FBQ3hKLFNBQVosRUFBakUsQ0FKRyxDQU1IO0FBQ0E7QUFDQTtBQUVBOztBQUNBLGVBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0ksNkJBQUMsYUFBRCxRQUNJLDZCQUFDLGNBQUQ7QUFDSSxVQUFBLFdBQVcsRUFBRSxLQUFLOUYsbUJBRHRCO0FBRUksVUFBQSxhQUFhLEVBQUUsS0FBSzBPLGFBRnhCO0FBR0ksVUFBQSxhQUFhLEVBQUUsS0FBS0cscUJBSHhCO0FBSUksVUFBQSxzQkFBc0IsRUFBRSxLQUFLTyxzQkFKakM7QUFLSSxVQUFBLFdBQVcsRUFBRXFELFdBTGpCO0FBTUksVUFBQSxVQUFVLEVBQUUsS0FOaEI7QUFPSSxVQUFBLE9BQU8sRUFBRSxLQUFLM1UsS0FBTCxDQUFXMUMsT0FQeEI7QUFRSSxVQUFBLElBQUksRUFBRSxLQUFLMEMsS0FBTCxDQUFXdkU7QUFSckIsVUFESixDQURKLENBREo7QUFnQkg7QUFDSixLQTFHYyxDQTRHZjtBQUNBOzs7QUFFQSxVQUFNMEgsSUFBSSxHQUFHLEtBQUtDLGVBQUwsRUFBYjs7QUFDQSxRQUFJNFIsTUFBTSxHQUFHLEtBQWI7O0FBQ0EsUUFBSTdSLElBQUksSUFBSyxLQUFLbkQsS0FBTCxDQUFXdkQsU0FBWCxLQUF5QixPQUF6QixJQUFvQyxLQUFLdUQsS0FBTCxDQUFXdkQsU0FBWCxLQUF5QixTQUExRSxFQUFzRjtBQUNsRnVZLE1BQUFBLE1BQU0sR0FBRyxJQUFUO0FBQ0g7O0FBRUQsVUFBTUMsb0JBQW9CLEdBQUcseUJBQVc7QUFDcENDLE1BQUFBLHdCQUF3QixFQUFFO0FBRFUsS0FBWCxDQUE3QjtBQUlBLFFBQUlDLFNBQUo7QUFDQSxRQUFJQyxvQkFBb0IsR0FBRyxJQUEzQjs7QUFFQSxRQUFJOVAseUJBQWdCQyxjQUFoQixHQUFpQ0MsaUJBQWpDLEdBQXFEeEMsTUFBckQsR0FBOEQsQ0FBbEUsRUFBcUU7QUFDakUsWUFBTXFTLFNBQVMsR0FBR3hHLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBbEI7QUFDQXFHLE1BQUFBLFNBQVMsR0FBRyw2QkFBQyxTQUFEO0FBQVcsUUFBQSxJQUFJLEVBQUUsS0FBS25WLEtBQUwsQ0FBV3ZFO0FBQTVCLFFBQVo7QUFDSCxLQUhELE1BR08sSUFBSSxDQUFDLEtBQUt1RSxLQUFMLENBQVd4RCxhQUFoQixFQUErQjtBQUNsQyxZQUFNOFksYUFBYSxHQUFHekcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUF0QjtBQUNBc0csTUFBQUEsb0JBQW9CLEdBQUcsS0FBS3BWLEtBQUwsQ0FBV3JDLGdCQUFsQztBQUNBd1gsTUFBQUEsU0FBUyxHQUFHLDZCQUFDLGFBQUQ7QUFDUixRQUFBLElBQUksRUFBRSxLQUFLblYsS0FBTCxDQUFXdkUsSUFEVDtBQUVSLFFBQUEscUJBQXFCLEVBQUUsS0FBS3VFLEtBQUwsQ0FBV25ELE9BRjFCO0FBR1IsUUFBQSxhQUFhLEVBQUVtWSxNQUhQO0FBSVIsUUFBQSxTQUFTLEVBQUVILFlBQVksS0FBSyxNQUpwQjtBQUtSLFFBQUEsYUFBYSxFQUFFLEtBQUt4SSxtQkFMWjtBQU1SLFFBQUEsa0JBQWtCLEVBQUUsS0FBS0MsdUJBTmpCO0FBT1IsUUFBQSxTQUFTLEVBQUUsS0FBS3lHLGtCQVBSO0FBUVIsUUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFSUCxRQUFaO0FBVUg7O0FBRUQsVUFBTXVDLHlCQUF5QixHQUFHLEtBQUt2VixLQUFMLENBQVdwQyxxQkFBN0M7QUFDQSxVQUFNNFgsa0JBQWtCLEdBQ3BCRCx5QkFBeUIsSUFDekJBLHlCQUF5QixDQUFDRSxZQUQxQixJQUVBLEtBQUt6VixLQUFMLENBQVd2RSxJQUFYLENBQWdCaWEsa0JBQWhCLENBQW1DLEtBQUtuYSxPQUFMLENBQWEwTSxXQUFiLENBQXlCQyxNQUE1RCxDQUhKO0FBTUEsVUFBTXlOLHdCQUF3QixHQUMxQmxXLHVCQUFjTSxRQUFkLENBQXVCLDBCQUF2QixLQUNBLEtBQUt4RSxPQUFMLENBQWE4TixlQUFiLENBQTZCLEtBQUtySixLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUE3QyxDQURBLElBRUEsQ0FBQyxLQUFLSCxPQUFMLENBQWFxYSxtQkFBYixFQUhMOztBQU1BLFVBQU1DLG9CQUFvQixHQUFHLEtBQUtwQyx3QkFBTCxFQUE3Qjs7QUFFQSxRQUFJcUMsR0FBRyxHQUFHLElBQVY7QUFDQSxRQUFJQyxVQUFKO0FBQ0EsUUFBSUMsVUFBVSxHQUFHLEtBQWpCO0FBQ0EsUUFBSUMsbUJBQW1CLEdBQUcsS0FBMUI7O0FBQ0EsUUFBSSxLQUFLalcsS0FBTCxDQUFXNUQsZUFBWCxLQUErQixJQUFuQyxFQUF5QztBQUNyQzBaLE1BQUFBLEdBQUcsR0FBRyw2QkFBQyxjQUFEO0FBQWdCLFFBQUEsYUFBYSxFQUFFLEtBQUtwRjtBQUFwQyxRQUFOO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBSzFRLEtBQUwsQ0FBV3pELFNBQWYsRUFBMEI7QUFDN0J5WixNQUFBQSxVQUFVLEdBQUcsSUFBYixDQUQ2QixDQUNWOztBQUNuQkYsTUFBQUEsR0FBRyxHQUFHLDZCQUFDLFNBQUQ7QUFBVyxRQUFBLGdCQUFnQixFQUFFLEtBQUs5VixLQUFMLENBQVdvTyxnQkFBeEM7QUFBMEQsUUFBQSxhQUFhLEVBQUUsS0FBS2pILG1CQUE5RTtBQUFtRyxRQUFBLFFBQVEsRUFBRSxLQUFLcUc7QUFBbEgsUUFBTjtBQUNILEtBSE0sTUFHQSxJQUFJZ0ksa0JBQUosRUFBd0I7QUFDM0JNLE1BQUFBLEdBQUcsR0FBRyw2QkFBQyxxQkFBRDtBQUF1QixRQUFBLElBQUksRUFBRSxLQUFLOVYsS0FBTCxDQUFXdkUsSUFBeEM7QUFBOEMsUUFBQSxjQUFjLEVBQUU4WjtBQUE5RCxRQUFOO0FBQ0FTLE1BQUFBLFVBQVUsR0FBRyxJQUFiO0FBQ0gsS0FITSxNQUdBLElBQUlMLHdCQUFKLEVBQThCO0FBQ2pDRyxNQUFBQSxHQUFHLEdBQUcsNkJBQUMsb0JBQUQ7QUFBc0IsUUFBQSxpQkFBaUIsRUFBRSxLQUFLM047QUFBOUMsUUFBTjtBQUNBNk4sTUFBQUEsVUFBVSxHQUFHLElBQWI7QUFDSCxLQUhNLE1BR0EsSUFBSSxLQUFLaFcsS0FBTCxDQUFXakQsYUFBZixFQUE4QjtBQUNqQ2laLE1BQUFBLFVBQVUsR0FBRyxJQUFiLENBRGlDLENBQ2Q7O0FBQ25CRixNQUFBQSxHQUFHLEdBQUcsNkJBQUMsaUJBQUQ7QUFBbUIsUUFBQSxJQUFJLEVBQUUsS0FBSzlWLEtBQUwsQ0FBV3ZFLElBQXBDO0FBQTBDLFFBQUEsYUFBYSxFQUFFLEtBQUsyVTtBQUE5RCxRQUFOO0FBQ0gsS0FITSxNQUdBLElBQUl5RSxZQUFZLEtBQUssTUFBckIsRUFBNkI7QUFDaEM7QUFDQTtBQUNBLFVBQUlGLFdBQVcsR0FBRzVZLFNBQWxCOztBQUNBLFVBQUksS0FBS2lHLEtBQUwsQ0FBV2xILE9BQWYsRUFBd0I7QUFDcEI2WixRQUFBQSxXQUFXLEdBQUcsS0FBSzNTLEtBQUwsQ0FBV2xILE9BQVgsQ0FBbUI2WixXQUFqQztBQUNIOztBQUNELFVBQUlDLFlBQVksR0FBRzdZLFNBQW5COztBQUNBLFVBQUksS0FBS2lHLEtBQUwsQ0FBV3BILGdCQUFmLEVBQWlDO0FBQzdCZ2EsUUFBQUEsWUFBWSxHQUFHLEtBQUs1UyxLQUFMLENBQVdwSCxnQkFBWCxDQUE0QmdhLFlBQTNDO0FBQ0g7O0FBQ0RvQixNQUFBQSxVQUFVLEdBQUcsSUFBYjtBQUNBRCxNQUFBQSxVQUFVLEdBQ04sNkJBQUMsY0FBRDtBQUFnQixRQUFBLFdBQVcsRUFBRSxLQUFLN1QsbUJBQWxDO0FBQ2dCLFFBQUEsYUFBYSxFQUFFLEtBQUswTyxhQURwQztBQUVnQixRQUFBLGFBQWEsRUFBRSxLQUFLaUIsbUNBRnBDO0FBR2dCLFFBQUEsT0FBTyxFQUFFLEtBQUs3UixLQUFMLENBQVcxQyxPQUhwQztBQUlnQixRQUFBLFdBQVcsRUFBRXFYLFdBSjdCO0FBS2dCLFFBQUEsWUFBWSxFQUFFQyxZQUw5QjtBQU1nQixRQUFBLE9BQU8sRUFBRSxLQUFLNVMsS0FBTCxDQUFXbEgsT0FOcEM7QUFPZ0IsUUFBQSxVQUFVLEVBQUUsS0FBS2tGLEtBQUwsQ0FBV3JELE9BUHZDO0FBUWdCLFFBQUEsSUFBSSxFQUFFLEtBQUtxRCxLQUFMLENBQVd2RTtBQVJqQyxRQURKOztBQVlBLFVBQUksQ0FBQyxLQUFLdUUsS0FBTCxDQUFXckQsT0FBaEIsRUFBeUI7QUFDckIsZUFDSTtBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDTW9aLFVBRE4sQ0FESjtBQUtILE9BTkQsTUFNTztBQUNIRSxRQUFBQSxtQkFBbUIsR0FBRyxJQUF0QjtBQUNIO0FBQ0osS0FqQ00sTUFpQ0EsSUFBSUosb0JBQW9CLEdBQUcsQ0FBM0IsRUFBOEI7QUFDakNDLE1BQUFBLEdBQUcsR0FDQyw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBQyxLQUExQjtBQUFnQyxRQUFBLFNBQVMsRUFBQyx1Q0FBMUM7QUFDa0IsUUFBQSxPQUFPLEVBQUUsS0FBS2xDO0FBRGhDLFNBRUsseUJBQ0csMEVBREgsRUFFRztBQUFDc0MsUUFBQUEsS0FBSyxFQUFFTDtBQUFSLE9BRkgsQ0FGTCxDQURKO0FBU0g7O0FBRUQsVUFBTU0sUUFBUSxHQUNWLDZCQUFDLFFBQUQ7QUFBVSxNQUFBLElBQUksRUFBRSxLQUFLblcsS0FBTCxDQUFXdkUsSUFBM0I7QUFDRSxNQUFBLFVBQVUsRUFBRSxLQURkO0FBRUUsTUFBQSxNQUFNLEVBQUUsS0FBS0YsT0FBTCxDQUFhME0sV0FBYixDQUF5QkMsTUFGbkM7QUFHRSxNQUFBLGlCQUFpQixFQUFFLEtBQUtsRyxLQUFMLENBQVd6SCxpQkFIaEM7QUFJRSxNQUFBLFlBQVksRUFBRSxLQUFLeUYsS0FBTCxDQUFXMUQsWUFKM0I7QUFLRSxNQUFBLDJCQUEyQixFQUFFLEtBQUswRCxLQUFMLENBQVd3TCwyQkFMMUM7QUFNRSxNQUFBLFNBQVMsRUFBRSxLQUFLeEwsS0FBTCxDQUFXdEMsaUJBTnhCO0FBT0UsTUFBQSxRQUFRLEVBQUUsS0FBS3NDLEtBQUwsQ0FBV3BELFFBUHZCO0FBUUUsTUFBQSxjQUFjLEVBQUU7QUFSbEIsT0FTTWtaLEdBVE4sQ0FESjs7QUFjQSxRQUFJTSxlQUFKO0FBQXFCLFFBQUlDLFVBQUo7QUFDckIsVUFBTUMsUUFBUSxHQUNWO0FBQ0F6QixJQUFBQSxZQUFZLEtBQUssTUFBakIsSUFBMkIsQ0FBQyxLQUFLN1UsS0FBTCxDQUFXeEQsYUFGM0M7O0FBSUEsUUFBSThaLFFBQUosRUFBYztBQUNWLFlBQU1DLGVBQWUsR0FBRzFILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1QkFBakIsQ0FBeEI7QUFDQXNILE1BQUFBLGVBQWUsR0FDWCw2QkFBQyxlQUFEO0FBQ0ksUUFBQSxJQUFJLEVBQUUsS0FBS3BXLEtBQUwsQ0FBV3ZFLElBRHJCO0FBRUksUUFBQSxTQUFTLEVBQUUsS0FBS3VFLEtBQUwsQ0FBV3ZELFNBRjFCO0FBR0ksUUFBQSxRQUFRLEVBQUUsS0FBS3VGLEtBQUwsQ0FBV3dVLFFBSHpCO0FBSUksUUFBQSxRQUFRLEVBQUUsS0FBS3hXLEtBQUwsQ0FBV3BELFFBSnpCO0FBS0ksUUFBQSxTQUFTLEVBQUUsS0FBS29ELEtBQUwsQ0FBVzJKLFNBTDFCO0FBTUksUUFBQSxnQkFBZ0IsRUFBRSxLQUFLcEksMkJBQUwsQ0FBaUMsS0FBS3ZCLEtBQUwsQ0FBV3ZFLElBQTVDO0FBTnRCLFFBREo7QUFTSCxLQTFQYyxDQTRQZjtBQUNBOzs7QUFDQSxRQUFJLEtBQUt1RSxLQUFMLENBQVd4RCxhQUFmLEVBQThCO0FBQzFCNlosTUFBQUEsVUFBVSxHQUFHO0FBQ1QxSSxRQUFBQSxVQUFVLEVBQUUsS0FBSzNOLEtBQUwsQ0FBVzJOLFVBRGQ7QUFFVEMsUUFBQUEsV0FBVyxFQUFFLEtBQUs1TixLQUFMLENBQVc0TixXQUZmO0FBR1Q2SSxRQUFBQSxXQUFXLEVBQUUsS0FBS3pXLEtBQUwsQ0FBV3hELGFBQVgsQ0FBeUIwWjtBQUg3QixPQUFiO0FBS0g7O0FBRUQsUUFBSWxCLE1BQUosRUFBWTtBQUNSLFVBQUkwQixVQUFKO0FBQWdCLFVBQUlDLGVBQUo7QUFBcUIsVUFBSUMsZUFBSjs7QUFFckMsVUFBSXpULElBQUksQ0FBQ2lILElBQUwsS0FBYyxPQUFsQixFQUEyQjtBQUN2QnNNLFFBQUFBLFVBQVUsR0FDTjtBQUFLLFVBQUEsU0FBUyxFQUFDLHdCQUFmO0FBQXdDLFVBQUEsT0FBTyxFQUFFLEtBQUtqRSxpQkFBdEQ7QUFBeUUsVUFBQSxLQUFLLEVBQUUseUJBQUcsYUFBSDtBQUFoRixXQUNJLDZCQUFDLFdBQUQ7QUFBYSxVQUFBLEdBQUcsRUFBRW9FLE9BQU8sQ0FBQyxpQ0FBRCxDQUF6QjtBQUE4RCxVQUFBLEtBQUssRUFBQyxJQUFwRTtBQUF5RSxVQUFBLE1BQU0sRUFBQyxJQUFoRjtBQUFxRixVQUFBLEtBQUssRUFBRTtBQUFFQyxZQUFBQSxTQUFTLEVBQUUsQ0FBYjtBQUFnQkMsWUFBQUEsV0FBVyxFQUFFO0FBQTdCO0FBQTVGLFVBREosQ0FESjtBQU1BSCxRQUFBQSxlQUFlLEdBQ1g7QUFBSyxVQUFBLFNBQVMsRUFBQyx3QkFBZjtBQUF3QyxVQUFBLE9BQU8sRUFBRSxLQUFLMVE7QUFBdEQsV0FDSSw2QkFBQyxXQUFEO0FBQWEsVUFBQSxHQUFHLEVBQUUvQyxJQUFJLENBQUMwUCxpQkFBTCxLQUEyQmdFLE9BQU8sQ0FBQyxtQ0FBRCxDQUFsQyxHQUEwRUEsT0FBTyxDQUFDLGlDQUFELENBQW5HO0FBQ0ssVUFBQSxHQUFHLEVBQUUxVCxJQUFJLENBQUMwUCxpQkFBTCxLQUEyQix5QkFBRyx1QkFBSCxDQUEzQixHQUF5RCx5QkFBRyxxQkFBSCxDQURuRTtBQUVLLFVBQUEsS0FBSyxFQUFDLElBRlg7QUFFZ0IsVUFBQSxNQUFNLEVBQUM7QUFGdkIsVUFESixDQURKO0FBTUg7O0FBQ0Q4RCxNQUFBQSxlQUFlLEdBQ1g7QUFBSyxRQUFBLFNBQVMsRUFBQyx3QkFBZjtBQUF3QyxRQUFBLE9BQU8sRUFBRSxLQUFLM1E7QUFBdEQsU0FDSSw2QkFBQyxXQUFEO0FBQWEsUUFBQSxHQUFHLEVBQUU3QyxJQUFJLENBQUN3UCxpQkFBTCxLQUEyQmtFLE9BQU8sQ0FBQyxtQ0FBRCxDQUFsQyxHQUEwRUEsT0FBTyxDQUFDLGlDQUFELENBQW5HO0FBQ0ssUUFBQSxHQUFHLEVBQUUxVCxJQUFJLENBQUN3UCxpQkFBTCxLQUEyQix5QkFBRyx1QkFBSCxDQUEzQixHQUF5RCx5QkFBRyxxQkFBSCxDQURuRTtBQUVLLFFBQUEsS0FBSyxFQUFDLElBRlg7QUFFZ0IsUUFBQSxNQUFNLEVBQUM7QUFGdkIsUUFESixDQURKLENBakJRLENBd0JSOztBQUNBd0MsTUFBQUEsU0FBUyxHQUNMO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNd0IsZUFETixFQUVNQyxlQUZOLEVBR01GLFVBSE4sRUFJTXZCLFNBSk4sRUFLSSw2QkFBQyxXQUFEO0FBQWEsUUFBQSxTQUFTLEVBQUMseUJBQXZCO0FBQWlELFFBQUEsR0FBRyxFQUFFMEIsT0FBTyxDQUFDLG1DQUFELENBQTdEO0FBQW9HLFFBQUEsS0FBSyxFQUFDLElBQTFHO0FBQStHLFFBQUEsTUFBTSxFQUFDO0FBQXRILFFBTEosQ0FESjtBQVFILEtBdlNjLENBeVNmO0FBQ0E7OztBQUNBLFFBQUlHLGtCQUFKO0FBQ0EsUUFBSUMsZ0JBQWdCLEdBQUcsS0FBdkI7O0FBRUEsUUFBSSxLQUFLalgsS0FBTCxDQUFXeEQsYUFBZixFQUE4QjtBQUMxQjtBQUNBLFVBQUksS0FBS3dELEtBQUwsQ0FBV3hELGFBQVgsQ0FBeUI2UixPQUF6QixLQUFxQ3RTLFNBQXpDLEVBQW9EO0FBQ2hEaWIsUUFBQUEsa0JBQWtCLEdBQUk7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFVBQXRCO0FBQ0gsT0FGRCxNQUVPO0FBQ0hBLFFBQUFBLGtCQUFrQixHQUNkLDZCQUFDLFdBQUQ7QUFBYSxVQUFBLEdBQUcsRUFBRSxLQUFLblgsbUJBQXZCO0FBQ0ksVUFBQSxTQUFTLEVBQUMseURBRGQ7QUFFSSxVQUFBLGFBQWEsRUFBRSxLQUFLZ00sMEJBRnhCO0FBR0ksVUFBQSxjQUFjLEVBQUUsS0FBSzdKLEtBQUwsQ0FBVzBCO0FBSC9CLFdBS0k7QUFBSSxVQUFBLFNBQVMsRUFBRXVSO0FBQWYsVUFMSixFQU1NLEtBQUs3RixvQkFBTCxFQU5OLENBREo7QUFVSDs7QUFDRDZILE1BQUFBLGdCQUFnQixHQUFHLElBQW5CO0FBQ0g7O0FBRUQsVUFBTUMsZUFBZSxHQUFHLEtBQUtsWCxLQUFMLENBQVc3RCx5QkFBbkM7QUFDQSxRQUFJZ2Isa0JBQWtCLEdBQUcsSUFBekI7O0FBQ0EsUUFBSSxLQUFLblgsS0FBTCxDQUFXNUQsZUFBZixFQUFnQztBQUM1QithLE1BQUFBLGtCQUFrQixHQUFHLEtBQUtuWCxLQUFMLENBQVc1RCxlQUFYLENBQTJCOFQsS0FBM0IsRUFBckI7QUFDSCxLQUZELE1BRU8sSUFBSWdILGVBQUosRUFBcUI7QUFDeEJDLE1BQUFBLGtCQUFrQixHQUFHLEtBQUtuWCxLQUFMLENBQVcvRCxjQUFoQztBQUNILEtBdlVjLENBeVVmOzs7QUFDQSxVQUFNbVcsWUFBWSxHQUNkLDZCQUFDLGFBQUQ7QUFDSSxNQUFBLEdBQUcsRUFBRSxLQUFLaUIsdUJBRGQ7QUFFSSxNQUFBLFdBQVcsRUFBRSxLQUFLclQsS0FBTCxDQUFXdkUsSUFBWCxDQUFnQmtNLHdCQUFoQixFQUZqQjtBQUdJLE1BQUEsZ0JBQWdCLEVBQUUsS0FBSzNILEtBQUwsQ0FBV2hELGdCQUhqQztBQUlJLE1BQUEsa0JBQWtCLEVBQUUsQ0FBQyxLQUFLZ0QsS0FBTCxDQUFXbEQsU0FKcEM7QUFLSSxNQUFBLGlCQUFpQixFQUFFLENBQUMsS0FBS2tELEtBQUwsQ0FBV2xELFNBTG5DO0FBTUksTUFBQSxNQUFNLEVBQUVtYSxnQkFOWjtBQU9JLE1BQUEsa0JBQWtCLEVBQUVFLGtCQVB4QjtBQVFJLE1BQUEsT0FBTyxFQUFFLEtBQUtuWCxLQUFMLENBQVcvRCxjQVJ4QjtBQVNJLE1BQUEsZ0JBQWdCLEVBQUUsS0FBSytELEtBQUwsQ0FBVzlELHVCQVRqQztBQVVJLE1BQUEsUUFBUSxFQUFFLEtBQUswUSxtQkFWbkI7QUFXSSxNQUFBLG1CQUFtQixFQUFFLEtBQUtDLDJCQVg5QjtBQVlJLE1BQUEsY0FBYyxFQUFJLEtBQUs3TSxLQUFMLENBQVdzSixjQVpqQztBQWFJLE1BQUEsU0FBUyxFQUFDLDBCQWJkO0FBY0ksTUFBQSxhQUFhLEVBQUUsS0FBS3RKLEtBQUwsQ0FBV2hFLGFBZDlCO0FBZUksTUFBQSxnQkFBZ0IsRUFBRSxLQUFLdUYsMkJBQUwsQ0FBaUMsS0FBS3ZCLEtBQUwsQ0FBV3ZFLElBQTVDLENBZnRCO0FBZ0JJLE1BQUEsY0FBYyxFQUFFLEtBQUt1RyxLQUFMLENBQVcwQixjQWhCL0I7QUFpQkksTUFBQSxhQUFhLEVBQUU7QUFqQm5CLE1BREo7O0FBcUJBLFFBQUkwVCxvQkFBb0IsR0FBRyxJQUEzQixDQS9WZSxDQWdXZjs7QUFDQSxRQUFJLEtBQUtwWCxLQUFMLENBQVd2Qyx3QkFBWCxJQUF1QyxDQUFDLEtBQUt1QyxLQUFMLENBQVd4RCxhQUF2RCxFQUFzRTtBQUNsRSxZQUFNNmEsb0JBQW9CLEdBQUd4SSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNEJBQWpCLENBQTdCO0FBQ0FzSSxNQUFBQSxvQkFBb0IsR0FBSSw2QkFBQyxvQkFBRDtBQUNHLFFBQUEsZUFBZSxFQUFFLEtBQUtwRixnQkFEekI7QUFFRyxRQUFBLFlBQVksRUFBRSxLQUFLQztBQUZ0QixRQUF4QjtBQUlIOztBQUNELFFBQUlxRixZQUFKLENBeFdlLENBeVdmOztBQUNBLFFBQUksQ0FBQyxLQUFLdFgsS0FBTCxDQUFXekMsbUJBQVosSUFBbUMsQ0FBQyxLQUFLeUMsS0FBTCxDQUFXeEQsYUFBbkQsRUFBa0U7QUFDOUQsWUFBTSthLGtCQUFrQixHQUFHMUksR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUEzQjtBQUNBd0ksTUFBQUEsWUFBWSxHQUFJLDZCQUFDLGtCQUFEO0FBQ1osUUFBQSxpQkFBaUIsRUFBRSxLQUFLdFgsS0FBTCxDQUFXM0QsaUJBRGxCO0FBRVosUUFBQSxxQkFBcUIsRUFBRSxLQUFLMFY7QUFGaEIsUUFBaEI7QUFJSDs7QUFDRCxVQUFNeUYsa0JBQWtCLEdBQUcseUJBQ3ZCLHdCQUR1QixFQUV2QjtBQUNJLHlDQUFtQ3BDO0FBRHZDLEtBRnVCLENBQTNCO0FBT0EsVUFBTXFDLHFCQUFxQixHQUFHLHlCQUMxQixrQkFEMEIsRUFDTixZQURNLEVBRTFCO0FBQ0ksMEJBQW9CLEtBQUt6VixLQUFMLENBQVd3VTtBQURuQyxLQUYwQixDQUE5QjtBQU9BLFVBQU12WixjQUFjLEdBQUcsQ0FBQ2daLG1CQUFELElBQXdCLEtBQUtqVyxLQUFMLENBQVd2RSxJQUFuQyxJQUEyQyxLQUFLdUUsS0FBTCxDQUFXL0MsY0FBN0U7QUFDQSxVQUFNeWEsVUFBVSxHQUFHemEsY0FBYyxHQUMzQiw2QkFBQyxtQkFBRDtBQUFZLE1BQUEsTUFBTSxFQUFFLEtBQUsrQyxLQUFMLENBQVd2RSxJQUFYLENBQWdCQyxNQUFwQztBQUE0QyxNQUFBLGNBQWMsRUFBRSxLQUFLc0csS0FBTCxDQUFXMEI7QUFBdkUsTUFEMkIsR0FFM0IsSUFGTjtBQUlBLFVBQU1pVSxlQUFlLEdBQUcseUJBQVcsc0JBQVgsRUFBbUM7QUFDdkRDLE1BQUFBLCtCQUErQixFQUFFLEtBQUs1WCxLQUFMLENBQVdoRDtBQURXLEtBQW5DLENBQXhCO0FBSUEsV0FDSSw2QkFBQyxvQkFBRCxDQUFhLFFBQWI7QUFBc0IsTUFBQSxLQUFLLEVBQUUsS0FBS2dEO0FBQWxDLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBRSxpQkFBaUJnVixNQUFNLEdBQUcscUJBQUgsR0FBMkIsRUFBbEQsQ0FBakI7QUFBd0UsTUFBQSxHQUFHLEVBQUUsS0FBS3BWO0FBQWxGLE9BQ0ksNkJBQUMsYUFBRCxRQUNJLDZCQUFDLFVBQUQ7QUFDSSxNQUFBLElBQUksRUFBRSxLQUFLSSxLQUFMLENBQVd2RSxJQURyQjtBQUVJLE1BQUEsVUFBVSxFQUFFNGEsVUFGaEI7QUFHSSxNQUFBLE9BQU8sRUFBRSxLQUFLclUsS0FBTCxDQUFXbEgsT0FIeEI7QUFJSSxNQUFBLE1BQU0sRUFBRStaLFlBQVksS0FBSyxNQUo3QjtBQUtJLE1BQUEsYUFBYSxFQUFFLEtBQUsvQyxhQUx4QjtBQU1JLE1BQUEsZUFBZSxFQUFFLEtBQUtyQixlQU4xQjtBQU9JLE1BQUEsYUFBYSxFQUFFLEtBQUtMLGFBUHhCO0FBUUksTUFBQSxhQUFhLEVBQUcwRixHQUFHLElBQUksQ0FBQ0UsVUFBVCxHQUF1QixLQUFLdEYsYUFBNUIsR0FBNEMsSUFSL0Q7QUFTSSxNQUFBLGFBQWEsRUFBR21FLFlBQVksS0FBSyxPQUFsQixHQUE2QixLQUFLakUsYUFBbEMsR0FBa0QsSUFUckU7QUFVSSxNQUFBLFlBQVksRUFBR2lFLFlBQVksS0FBSyxNQUFsQixHQUE0QixLQUFLbEUsWUFBakMsR0FBZ0QsSUFWbEU7QUFXSSxNQUFBLFNBQVMsRUFBRSxLQUFLM1EsS0FBTCxDQUFXMko7QUFYMUIsTUFESixFQWNJLDZCQUFDLGtCQUFEO0FBQ0ksTUFBQSxLQUFLLEVBQUUrTixVQURYO0FBRUksTUFBQSxjQUFjLEVBQUUsS0FBSzFWLEtBQUwsQ0FBVzBCO0FBRi9CLE9BSUk7QUFBSyxNQUFBLFNBQVMsRUFBRStUO0FBQWhCLE9BQ0t0QixRQURMLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBRXdCO0FBQWhCLE9BQ0tQLG9CQURMLEVBRUtFLFlBRkwsRUFHS2xGLFlBSEwsRUFJSzRFLGtCQUpMLENBRkosRUFRSTtBQUFLLE1BQUEsU0FBUyxFQUFFUTtBQUFoQixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixNQURKLEVBRUtyQyxTQUZMLENBREosQ0FSSixFQWNLWSxVQWRMLEVBZUtLLGVBZkwsQ0FKSixDQWRKLENBREosQ0FESixDQURKO0FBMkNIO0FBNTdEMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLy8gVE9ETzogVGhpcyBjb21wb25lbnQgaXMgZW5vcm1vdXMhIFRoZXJlJ3Mgc2V2ZXJhbCB0aGluZ3Mgd2hpY2ggY291bGQgc3RhbmQtYWxvbmU6XHJcbi8vICAtIFNlYXJjaCByZXN1bHRzIGNvbXBvbmVudFxyXG4vLyAgLSBEcmFnIGFuZCBkcm9wXHJcblxyXG5pbXBvcnQgc2hvdWxkSGlkZUV2ZW50IGZyb20gJy4uLy4uL3Nob3VsZEhpZGVFdmVudCc7XHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtSb29tUGVybWFsaW5rQ3JlYXRvcn0gZnJvbSAnLi4vLi4vdXRpbHMvcGVybWFsaW5rcy9QZXJtYWxpbmtzJztcclxuXHJcbmltcG9ydCBDb250ZW50TWVzc2FnZXMgZnJvbSAnLi4vLi4vQ29udGVudE1lc3NhZ2VzJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uL01vZGFsJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IENhbGxIYW5kbGVyIGZyb20gJy4uLy4uL0NhbGxIYW5kbGVyJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IFRpbnRlciBmcm9tICcuLi8uLi9UaW50ZXInO1xyXG5pbXBvcnQgcmF0ZV9saW1pdGVkX2Z1bmMgZnJvbSAnLi4vLi4vcmF0ZWxpbWl0ZWRmdW5jJztcclxuaW1wb3J0ICogYXMgT2JqZWN0VXRpbHMgZnJvbSAnLi4vLi4vT2JqZWN0VXRpbHMnO1xyXG5pbXBvcnQgKiBhcyBSb29tcyBmcm9tICcuLi8uLi9Sb29tcyc7XHJcbmltcG9ydCBldmVudFNlYXJjaCBmcm9tICcuLi8uLi9TZWFyY2hpbmcnO1xyXG5cclxuaW1wb3J0IHtpc09ubHlDdHJsT3JDbWRLZXlFdmVudCwgS2V5fSBmcm9tICcuLi8uLi9LZXlib2FyZCc7XHJcblxyXG5pbXBvcnQgTWFpblNwbGl0IGZyb20gJy4vTWFpblNwbGl0JztcclxuaW1wb3J0IFJpZ2h0UGFuZWwgZnJvbSAnLi9SaWdodFBhbmVsJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi4vLi4vc3RvcmVzL1Jvb21WaWV3U3RvcmUnO1xyXG5pbXBvcnQgUm9vbVNjcm9sbFN0YXRlU3RvcmUgZnJvbSAnLi4vLi4vc3RvcmVzL1Jvb21TY3JvbGxTdGF0ZVN0b3JlJztcclxuaW1wb3J0IFdpZGdldEVjaG9TdG9yZSBmcm9tICcuLi8uLi9zdG9yZXMvV2lkZ2V0RWNob1N0b3JlJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUsIHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBXaWRnZXRVdGlscyBmcm9tICcuLi8uLi91dGlscy9XaWRnZXRVdGlscyc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi92aWV3cy9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCBSaWdodFBhbmVsU3RvcmUgZnJvbSBcIi4uLy4uL3N0b3Jlcy9SaWdodFBhbmVsU3RvcmVcIjtcclxuaW1wb3J0IHtoYXZlVGlsZUZvckV2ZW50fSBmcm9tIFwiLi4vdmlld3Mvcm9vbXMvRXZlbnRUaWxlXCI7XHJcbmltcG9ydCBSb29tQ29udGV4dCBmcm9tIFwiLi4vLi4vY29udGV4dHMvUm9vbUNvbnRleHRcIjtcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuaW1wb3J0IHsgc2hpZWxkU3RhdHVzRm9yUm9vbSB9IGZyb20gJy4uLy4uL3V0aWxzL1NoaWVsZFV0aWxzJztcclxuXHJcbmNvbnN0IERFQlVHID0gZmFsc2U7XHJcbmxldCBkZWJ1Z2xvZyA9IGZ1bmN0aW9uKCkge307XHJcblxyXG5jb25zdCBCUk9XU0VSX1NVUFBPUlRTX1NBTkRCT1ggPSAnc2FuZGJveCcgaW4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XHJcblxyXG5pZiAoREVCVUcpIHtcclxuICAgIC8vIHVzaW5nIGJpbmQgbWVhbnMgdGhhdCB3ZSBnZXQgdG8ga2VlcCB1c2VmdWwgbGluZSBudW1iZXJzIGluIHRoZSBjb25zb2xlXHJcbiAgICBkZWJ1Z2xvZyA9IGNvbnNvbGUubG9nLmJpbmQoY29uc29sZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tVmlldycsXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBDb25mZXJlbmNlSGFuZGxlcjogUHJvcFR5cGVzLmFueSxcclxuXHJcbiAgICAgICAgLy8gQ2FsbGVkIHdpdGggdGhlIGNyZWRlbnRpYWxzIG9mIGEgcmVnaXN0ZXJlZCB1c2VyIChpZiB0aGV5IHdlcmUgYSBST1UgdGhhdFxyXG4gICAgICAgIC8vIHRyYW5zaXRpb25lZCB0byBQV0xVKVxyXG4gICAgICAgIG9uUmVnaXN0ZXJlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIEFuIG9iamVjdCByZXByZXNlbnRpbmcgYSB0aGlyZCBwYXJ0eSBpbnZpdGUgdG8gam9pbiB0aGlzIHJvb21cclxuICAgICAgICAvLyBGaWVsZHM6XHJcbiAgICAgICAgLy8gKiBpbnZpdGVTaWduVXJsIChzdHJpbmcpIFRoZSBVUkwgdXNlZCB0byBqb2luIHRoaXMgcm9vbSBmcm9tIGFuIGVtYWlsIGludml0ZVxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAoZ2l2ZW4gYXMgcGFydCBvZiB0aGUgbGluayBpbiB0aGUgaW52aXRlIGVtYWlsKVxyXG4gICAgICAgIC8vICogaW52aXRlZEVtYWlsIChzdHJpbmcpIFRoZSBlbWFpbCBhZGRyZXNzIHRoYXQgd2FzIGludml0ZWQgdG8gdGhpcyByb29tXHJcbiAgICAgICAgdGhpcmRQYXJ0eUludml0ZTogUHJvcFR5cGVzLm9iamVjdCxcclxuXHJcbiAgICAgICAgLy8gQW55IGRhdGEgYWJvdXQgdGhlIHJvb20gdGhhdCB3b3VsZCBub3JtYWxseSBjb21lIGZyb20gdGhlIGhvbWVzZXJ2ZXJcclxuICAgICAgICAvLyBidXQgaGFzIGJlZW4gcGFzc2VkIG91dC1vZi1iYW5kLCBlZy4gdGhlIHJvb20gbmFtZSBhbmQgYXZhdGFyIFVSTFxyXG4gICAgICAgIC8vIGZyb20gYW4gZW1haWwgaW52aXRlIChhIHdvcmthcm91bmQgZm9yIHRoZSBmYWN0IHRoYXQgd2UgY2FuJ3RcclxuICAgICAgICAvLyBnZXQgdGhpcyBpbmZvcm1hdGlvbiBmcm9tIHRoZSBIUyB1c2luZyBhbiBlbWFpbCBpbnZpdGUpLlxyXG4gICAgICAgIC8vIEZpZWxkczpcclxuICAgICAgICAvLyAgKiBuYW1lIChzdHJpbmcpIFRoZSByb29tJ3MgbmFtZVxyXG4gICAgICAgIC8vICAqIGF2YXRhclVybCAoc3RyaW5nKSBUaGUgbXhjOi8vIGF2YXRhciBVUkwgZm9yIHRoZSByb29tXHJcbiAgICAgICAgLy8gICogaW52aXRlck5hbWUgKHN0cmluZykgVGhlIGRpc3BsYXkgbmFtZSBvZiB0aGUgcGVyc29uIHdob1xyXG4gICAgICAgIC8vICAqICAgICAgICAgICAgICAgICAgICAgIGludml0ZWQgdXMgdG8gdGhlIHJvb21cclxuICAgICAgICBvb2JEYXRhOiBQcm9wVHlwZXMub2JqZWN0LFxyXG5cclxuICAgICAgICAvLyBTZXJ2ZXJzIHRoZSBSb29tVmlldyBjYW4gdXNlIHRvIHRyeSBhbmQgYXNzaXN0IGpvaW5zXHJcbiAgICAgICAgdmlhU2VydmVyczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyksXHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBjb250ZXh0VHlwZTogTWF0cml4Q2xpZW50Q29udGV4dCxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBsbE1lbWJlcnMgPSB0aGlzLmNvbnRleHQuaGFzTGF6eUxvYWRNZW1iZXJzRW5hYmxlZCgpO1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJvb206IG51bGwsXHJcbiAgICAgICAgICAgIHJvb21JZDogbnVsbCxcclxuICAgICAgICAgICAgcm9vbUxvYWRpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIHBlZWtMb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgc2hvdWxkUGVlazogdHJ1ZSxcclxuXHJcbiAgICAgICAgICAgIC8vIE1lZGlhIGxpbWl0cyBmb3IgdXBsb2FkaW5nLlxyXG4gICAgICAgICAgICBtZWRpYUNvbmZpZzogdW5kZWZpbmVkLFxyXG5cclxuICAgICAgICAgICAgLy8gdXNlZCB0byB0cmlnZ2VyIGEgcmVyZW5kZXIgaW4gVGltZWxpbmVQYW5lbCBvbmNlIHRoZSBtZW1iZXJzIGFyZSBsb2FkZWQsXHJcbiAgICAgICAgICAgIC8vIHNvIFJSIGFyZSByZW5kZXJlZCBhZ2FpbiAobm93IHdpdGggdGhlIG1lbWJlcnMgYXZhaWxhYmxlKSwgLi4uXHJcbiAgICAgICAgICAgIG1lbWJlcnNMb2FkZWQ6ICFsbE1lbWJlcnMsXHJcbiAgICAgICAgICAgIC8vIFRoZSBldmVudCB0byBiZSBzY3JvbGxlZCB0byBpbml0aWFsbHlcclxuICAgICAgICAgICAgaW5pdGlhbEV2ZW50SWQ6IG51bGwsXHJcbiAgICAgICAgICAgIC8vIFRoZSBvZmZzZXQgaW4gcGl4ZWxzIGZyb20gdGhlIGV2ZW50IHdpdGggd2hpY2ggdG8gc2Nyb2xsIHZlcnRpY2FsbHlcclxuICAgICAgICAgICAgaW5pdGlhbEV2ZW50UGl4ZWxPZmZzZXQ6IG51bGwsXHJcbiAgICAgICAgICAgIC8vIFdoZXRoZXIgdG8gaGlnaGxpZ2h0IHRoZSBldmVudCBzY3JvbGxlZCB0b1xyXG4gICAgICAgICAgICBpc0luaXRpYWxFdmVudEhpZ2hsaWdodGVkOiBudWxsLFxyXG5cclxuICAgICAgICAgICAgZm9yd2FyZGluZ0V2ZW50OiBudWxsLFxyXG4gICAgICAgICAgICBudW1VbnJlYWRNZXNzYWdlczogMCxcclxuICAgICAgICAgICAgZHJhZ2dpbmdGaWxlOiBmYWxzZSxcclxuICAgICAgICAgICAgc2VhcmNoaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgc2VhcmNoUmVzdWx0czogbnVsbCxcclxuICAgICAgICAgICAgY2FsbFN0YXRlOiBudWxsLFxyXG4gICAgICAgICAgICBndWVzdHNDYW5Kb2luOiBmYWxzZSxcclxuICAgICAgICAgICAgY2FuUGVlazogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dBcHBzOiBmYWxzZSxcclxuICAgICAgICAgICAgaXNBbG9uZTogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzUGVla2luZzogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dpbmdQaW5uZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBzaG93UmVhZFJlY2VpcHRzOiB0cnVlLFxyXG4gICAgICAgICAgICBzaG93UmlnaHRQYW5lbDogUmlnaHRQYW5lbFN0b3JlLmdldFNoYXJlZEluc3RhbmNlKCkuaXNPcGVuRm9yUm9vbSxcclxuXHJcbiAgICAgICAgICAgIC8vIGVycm9yIG9iamVjdCwgYXMgZnJvbSB0aGUgbWF0cml4IGNsaWVudC9zZXJ2ZXIgQVBJXHJcbiAgICAgICAgICAgIC8vIElmIHdlIGZhaWxlZCB0byBsb2FkIGluZm9ybWF0aW9uIGFib3V0IHRoZSByb29tLFxyXG4gICAgICAgICAgICAvLyBzdG9yZSB0aGUgZXJyb3IgaGVyZS5cclxuICAgICAgICAgICAgcm9vbUxvYWRFcnJvcjogbnVsbCxcclxuXHJcbiAgICAgICAgICAgIC8vIEhhdmUgd2Ugc2VudCBhIHJlcXVlc3QgdG8gam9pbiB0aGUgcm9vbSB0aGF0IHdlJ3JlIHdhaXRpbmcgdG8gY29tcGxldGU/XHJcbiAgICAgICAgICAgIGpvaW5pbmc6IGZhbHNlLFxyXG5cclxuICAgICAgICAgICAgLy8gdGhpcyBpcyB0cnVlIGlmIHdlIGFyZSBmdWxseSBzY3JvbGxlZC1kb3duLCBhbmQgYXJlIGxvb2tpbmcgYXRcclxuICAgICAgICAgICAgLy8gdGhlIGVuZCBvZiB0aGUgbGl2ZSB0aW1lbGluZS4gSXQgaGFzIHRoZSBlZmZlY3Qgb2YgaGlkaW5nIHRoZVxyXG4gICAgICAgICAgICAvLyAnc2Nyb2xsIHRvIGJvdHRvbScga25vYiwgYW1vbmcgYSBjb3VwbGUgb2Ygb3RoZXIgdGhpbmdzLlxyXG4gICAgICAgICAgICBhdEVuZE9mTGl2ZVRpbWVsaW5lOiB0cnVlLFxyXG4gICAgICAgICAgICBhdEVuZE9mTGl2ZVRpbWVsaW5lSW5pdDogZmFsc2UsIC8vIHVzZWQgYnkgY29tcG9uZW50RGlkVXBkYXRlIHRvIGF2b2lkIHVubmVjZXNzYXJ5IGNoZWNrc1xyXG5cclxuICAgICAgICAgICAgc2hvd1RvcFVucmVhZE1lc3NhZ2VzQmFyOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgIGF1eFBhbmVsTWF4SGVpZ2h0OiB1bmRlZmluZWQsXHJcblxyXG4gICAgICAgICAgICBzdGF0dXNCYXJWaXNpYmxlOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgIC8vIFdlIGxvYWQgdGhpcyBsYXRlciBieSBhc2tpbmcgdGhlIGpzLXNkayB0byBzdWdnZXN0IGEgdmVyc2lvbiBmb3IgdXMuXHJcbiAgICAgICAgICAgIC8vIFRoaXMgb2JqZWN0IGlzIHRoZSByZXN1bHQgb2YgUm9vbSNnZXRSZWNvbW1lbmRlZFZlcnNpb24oKVxyXG4gICAgICAgICAgICB1cGdyYWRlUmVjb21tZW5kYXRpb246IG51bGwsXHJcblxyXG4gICAgICAgICAgICBjYW5SZWFjdDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNhblJlcGx5OiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLm9uQWN0aW9uKTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oXCJSb29tXCIsIHRoaXMub25Sb29tKTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oXCJSb29tLnRpbWVsaW5lXCIsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5vbihcIlJvb20ubmFtZVwiLCB0aGlzLm9uUm9vbU5hbWUpO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5vbihcIlJvb20uYWNjb3VudERhdGFcIiwgdGhpcy5vblJvb21BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0Lm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyKTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oXCJSb29tLm15TWVtYmVyc2hpcFwiLCB0aGlzLm9uTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oXCJhY2NvdW50RGF0YVwiLCB0aGlzLm9uQWNjb3VudERhdGEpO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5vbihcImNyeXB0by5rZXlCYWNrdXBTdGF0dXNcIiwgdGhpcy5vbktleUJhY2t1cFN0YXR1cyk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0Lm9uKFwiZGV2aWNlVmVyaWZpY2F0aW9uQ2hhbmdlZFwiLCB0aGlzLm9uRGV2aWNlVmVyaWZpY2F0aW9uQ2hhbmdlZCk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0Lm9uKFwidXNlclRydXN0U3RhdHVzQ2hhbmdlZFwiLCB0aGlzLm9uVXNlclZlcmlmaWNhdGlvbkNoYW5nZWQpO1xyXG4gICAgICAgIC8vIFN0YXJ0IGxpc3RlbmluZyBmb3IgUm9vbVZpZXdTdG9yZSB1cGRhdGVzXHJcbiAgICAgICAgdGhpcy5fcm9vbVN0b3JlVG9rZW4gPSBSb29tVmlld1N0b3JlLmFkZExpc3RlbmVyKHRoaXMuX29uUm9vbVZpZXdTdG9yZVVwZGF0ZSk7XHJcbiAgICAgICAgdGhpcy5fcmlnaHRQYW5lbFN0b3JlVG9rZW4gPSBSaWdodFBhbmVsU3RvcmUuZ2V0U2hhcmVkSW5zdGFuY2UoKS5hZGRMaXN0ZW5lcih0aGlzLl9vblJpZ2h0UGFuZWxTdG9yZVVwZGF0ZSk7XHJcbiAgICAgICAgdGhpcy5fb25Sb29tVmlld1N0b3JlVXBkYXRlKHRydWUpO1xyXG5cclxuICAgICAgICBXaWRnZXRFY2hvU3RvcmUub24oJ3VwZGF0ZScsIHRoaXMuX29uV2lkZ2V0RWNob1N0b3JlVXBkYXRlKTtcclxuICAgICAgICB0aGlzLl9zaG93UmVhZFJlY2VpcHRzV2F0Y2hSZWYgPSBTZXR0aW5nc1N0b3JlLndhdGNoU2V0dGluZyhcInNob3dSZWFkUmVjZWlwdHNcIiwgbnVsbCxcclxuICAgICAgICAgICAgdGhpcy5fb25SZWFkUmVjZWlwdHNDaGFuZ2UpO1xyXG5cclxuICAgICAgICB0aGlzLl9yb29tVmlldyA9IGNyZWF0ZVJlZigpO1xyXG4gICAgICAgIHRoaXMuX3NlYXJjaFJlc3VsdHNQYW5lbCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25SZWFkUmVjZWlwdHNDaGFuZ2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzaG93UmVhZFJlY2VpcHRzOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwic2hvd1JlYWRSZWNlaXB0c1wiLCB0aGlzLnN0YXRlLnJvb21JZCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJvb21WaWV3U3RvcmVVcGRhdGU6IGZ1bmN0aW9uKGluaXRpYWwpIHtcclxuICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFpbml0aWFsICYmIHRoaXMuc3RhdGUucm9vbUlkICE9PSBSb29tVmlld1N0b3JlLmdldFJvb21JZCgpKSB7XHJcbiAgICAgICAgICAgIC8vIFJvb21WaWV3IGV4cGxpY2l0bHkgZG9lcyBub3Qgc3VwcG9ydCBjaGFuZ2luZyB3aGF0IHJvb21cclxuICAgICAgICAgICAgLy8gaXMgYmVpbmcgdmlld2VkOiBpbnN0ZWFkIGl0IHNob3VsZCBqdXN0IGJlIHJlLW1vdW50ZWQgd2hlblxyXG4gICAgICAgICAgICAvLyBzd2l0Y2hpbmcgcm9vbXMuIFRoZXJlZm9yZSwgaWYgdGhlIHJvb20gSUQgY2hhbmdlcywgd2VcclxuICAgICAgICAgICAgLy8gaWdub3JlIHRoaXMuIFdlIGVpdGhlciBuZWVkIHRvIGRvIHRoaXMgb3IgYWRkIGNvZGUgdG8gaGFuZGxlXHJcbiAgICAgICAgICAgIC8vIHNhdmluZyB0aGUgc2Nyb2xsIHBvc2l0aW9uIChvdGhlcndpc2Ugd2UgZW5kIHVwIHNhdmluZyB0aGVcclxuICAgICAgICAgICAgLy8gc2Nyb2xsIHBvc2l0aW9uIGFnYWluc3QgdGhlIHdyb25nIHJvb20pLlxyXG5cclxuICAgICAgICAgICAgLy8gR2l2ZW4gdGhhdCBkb2luZyB0aGUgc2V0U3RhdGUgaGVyZSB3b3VsZCBjYXVzZSBhIGJ1bmNoIG9mXHJcbiAgICAgICAgICAgIC8vIHVubmVjZXNzYXJ5IHdvcmssIHdlIGp1c3QgaWdub3JlIHRoZSBjaGFuZ2Ugc2luY2Ugd2Uga25vd1xyXG4gICAgICAgICAgICAvLyB0aGF0IGlmIHRoZSBjdXJyZW50IHJvb20gSUQgaGFzIGNoYW5nZWQgZnJvbSB3aGF0IHdlIHRob3VnaHRcclxuICAgICAgICAgICAgLy8gaXQgd2FzLCBpdCBtZWFucyB3ZSdyZSBhYm91dCB0byBiZSB1bm1vdW50ZWQuXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21JZCA9IFJvb21WaWV3U3RvcmUuZ2V0Um9vbUlkKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xyXG4gICAgICAgICAgICByb29tSWQsXHJcbiAgICAgICAgICAgIHJvb21BbGlhczogUm9vbVZpZXdTdG9yZS5nZXRSb29tQWxpYXMoKSxcclxuICAgICAgICAgICAgcm9vbUxvYWRpbmc6IFJvb21WaWV3U3RvcmUuaXNSb29tTG9hZGluZygpLFxyXG4gICAgICAgICAgICByb29tTG9hZEVycm9yOiBSb29tVmlld1N0b3JlLmdldFJvb21Mb2FkRXJyb3IoKSxcclxuICAgICAgICAgICAgam9pbmluZzogUm9vbVZpZXdTdG9yZS5pc0pvaW5pbmcoKSxcclxuICAgICAgICAgICAgaW5pdGlhbEV2ZW50SWQ6IFJvb21WaWV3U3RvcmUuZ2V0SW5pdGlhbEV2ZW50SWQoKSxcclxuICAgICAgICAgICAgaXNJbml0aWFsRXZlbnRIaWdobGlnaHRlZDogUm9vbVZpZXdTdG9yZS5pc0luaXRpYWxFdmVudEhpZ2hsaWdodGVkKCksXHJcbiAgICAgICAgICAgIGZvcndhcmRpbmdFdmVudDogUm9vbVZpZXdTdG9yZS5nZXRGb3J3YXJkaW5nRXZlbnQoKSxcclxuICAgICAgICAgICAgc2hvdWxkUGVlazogUm9vbVZpZXdTdG9yZS5zaG91bGRQZWVrKCksXHJcbiAgICAgICAgICAgIHNob3dpbmdQaW5uZWQ6IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJQaW5uZWRFdmVudHMuaXNPcGVuXCIsIHJvb21JZCksXHJcbiAgICAgICAgICAgIHNob3dSZWFkUmVjZWlwdHM6IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJzaG93UmVhZFJlY2VpcHRzXCIsIHJvb21JZCksXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKCFpbml0aWFsICYmIHRoaXMuc3RhdGUuc2hvdWxkUGVlayAmJiAhbmV3U3RhdGUuc2hvdWxkUGVlaykge1xyXG4gICAgICAgICAgICAvLyBTdG9wIHBlZWtpbmcgYmVjYXVzZSB3ZSBoYXZlIGpvaW5lZCB0aGlzIHJvb20gbm93XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5zdG9wUGVla2luZygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVGVtcG9yYXJ5IGxvZ2dpbmcgdG8gZGlhZ25vc2UgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvNDMwN1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAnUlZTIHVwZGF0ZTonLFxyXG4gICAgICAgICAgICBuZXdTdGF0ZS5yb29tSWQsXHJcbiAgICAgICAgICAgIG5ld1N0YXRlLnJvb21BbGlhcyxcclxuICAgICAgICAgICAgJ2xvYWRpbmc/JywgbmV3U3RhdGUucm9vbUxvYWRpbmcsXHJcbiAgICAgICAgICAgICdqb2luaW5nPycsIG5ld1N0YXRlLmpvaW5pbmcsXHJcbiAgICAgICAgICAgICdpbml0aWFsPycsIGluaXRpYWwsXHJcbiAgICAgICAgICAgICdzaG91bGRQZWVrPycsIG5ld1N0YXRlLnNob3VsZFBlZWssXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgLy8gTkI6IFRoaXMgZG9lcyBhc3N1bWUgdGhhdCB0aGUgcm9vbUlEIHdpbGwgbm90IGNoYW5nZSBmb3IgdGhlIGxpZmV0aW1lIG9mXHJcbiAgICAgICAgLy8gdGhlIFJvb21WaWV3IGluc3RhbmNlXHJcbiAgICAgICAgaWYgKGluaXRpYWwpIHtcclxuICAgICAgICAgICAgbmV3U3RhdGUucm9vbSA9IHRoaXMuY29udGV4dC5nZXRSb29tKG5ld1N0YXRlLnJvb21JZCk7XHJcbiAgICAgICAgICAgIGlmIChuZXdTdGF0ZS5yb29tKSB7XHJcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5zaG93QXBwcyA9IHRoaXMuX3Nob3VsZFNob3dBcHBzKG5ld1N0YXRlLnJvb20pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fb25Sb29tTG9hZGVkKG5ld1N0YXRlLnJvb20pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yb29tSWQgPT09IG51bGwgJiYgbmV3U3RhdGUucm9vbUlkICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIC8vIEdldCB0aGUgc2Nyb2xsIHN0YXRlIGZvciB0aGUgbmV3IHJvb21cclxuXHJcbiAgICAgICAgICAgIC8vIElmIGFuIGV2ZW50IElEIHdhc24ndCBzcGVjaWZpZWQsIGRlZmF1bHQgdG8gdGhlIG9uZSBzYXZlZCBmb3IgdGhpcyByb29tXHJcbiAgICAgICAgICAgIC8vIGluIHRoZSBzY3JvbGwgc3RhdGUgc3RvcmUuIEFzc3VtZSBpbml0aWFsRXZlbnRQaXhlbE9mZnNldCBzaG91bGQgYmUgc2V0LlxyXG4gICAgICAgICAgICBpZiAoIW5ld1N0YXRlLmluaXRpYWxFdmVudElkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb29tU2Nyb2xsU3RhdGUgPSBSb29tU2Nyb2xsU3RhdGVTdG9yZS5nZXRTY3JvbGxTdGF0ZShuZXdTdGF0ZS5yb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJvb21TY3JvbGxTdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG5ld1N0YXRlLmluaXRpYWxFdmVudElkID0gcm9vbVNjcm9sbFN0YXRlLmZvY3Vzc2VkRXZlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbmV3U3RhdGUuaW5pdGlhbEV2ZW50UGl4ZWxPZmZzZXQgPSByb29tU2Nyb2xsU3RhdGUucGl4ZWxPZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIENsZWFyIHRoZSBzZWFyY2ggcmVzdWx0cyB3aGVuIGNsaWNraW5nIGEgc2VhcmNoIHJlc3VsdCAod2hpY2ggY2hhbmdlcyB0aGVcclxuICAgICAgICAvLyBjdXJyZW50bHkgc2Nyb2xsZWQgdG8gZXZlbnQsIHRoaXMuc3RhdGUuaW5pdGlhbEV2ZW50SWQpLlxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmluaXRpYWxFdmVudElkICE9PSBuZXdTdGF0ZS5pbml0aWFsRXZlbnRJZCkge1xyXG4gICAgICAgICAgICBuZXdTdGF0ZS5zZWFyY2hSZXN1bHRzID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xyXG4gICAgICAgIC8vIEF0IHRoaXMgcG9pbnQsIG5ld1N0YXRlLnJvb21JZCBjb3VsZCBiZSBudWxsIChlLmcuIHRoZSBhbGlhcyBtaWdodCBub3RcclxuICAgICAgICAvLyBoYXZlIGJlZW4gcmVzb2x2ZWQgeWV0KSBzbyBhbnl0aGluZyBjYWxsZWQgaGVyZSBtdXN0IGhhbmRsZSB0aGlzIGNhc2UuXHJcblxyXG4gICAgICAgIC8vIFdlIHBhc3MgdGhlIG5ldyBzdGF0ZSBpbnRvIHRoaXMgZnVuY3Rpb24gZm9yIGl0IHRvIHJlYWQ6IGl0IG5lZWRzIHRvXHJcbiAgICAgICAgLy8gb2JzZXJ2ZSB0aGUgbmV3IHN0YXRlIGJ1dCB3ZSBkb24ndCB3YW50IHRvIHB1dCBpdCBpbiB0aGUgc2V0U3RhdGVcclxuICAgICAgICAvLyBjYWxsYmFjayBiZWNhdXNlIHRoaXMgd291bGQgcHJldmVudCB0aGUgc2V0U3RhdGVzIGZyb20gYmVpbmcgYmF0Y2hlZCxcclxuICAgICAgICAvLyBpZS4gY2F1c2UgaXQgdG8gcmVuZGVyIFJvb21WaWV3IHR3aWNlIHJhdGhlciB0aGFuIHRoZSBvbmNlIHRoYXQgaXMgbmVjZXNzYXJ5LlxyXG4gICAgICAgIGlmIChpbml0aWFsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NldHVwUm9vbShuZXdTdGF0ZS5yb29tLCBuZXdTdGF0ZS5yb29tSWQsIG5ld1N0YXRlLmpvaW5pbmcsIG5ld1N0YXRlLnNob3VsZFBlZWspO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2dldFJvb21JZCgpIHtcclxuICAgICAgICAvLyBBY2NvcmRpbmcgdG8gYF9vblJvb21WaWV3U3RvcmVVcGRhdGVgLCBgc3RhdGUucm9vbUlkYCBjYW4gYmUgbnVsbFxyXG4gICAgICAgIC8vIGlmIHdlIGhhdmUgYSByb29tIGFsaWFzIHdlIGhhdmVuJ3QgcmVzb2x2ZWQgeWV0LiBUbyB3b3JrIGFyb3VuZCB0aGlzLFxyXG4gICAgICAgIC8vIGZpcnN0IHdlJ2xsIHRyeSB0aGUgcm9vbSBvYmplY3QgaWYgaXQncyB0aGVyZSwgYW5kIHRoZW4gZmFsbGJhY2sgdG9cclxuICAgICAgICAvLyB0aGUgYmFyZSByb29tIElELiAoV2UgbWF5IHdhbnQgdG8gdXBkYXRlIGBzdGF0ZS5yb29tSWRgIGFmdGVyXHJcbiAgICAgICAgLy8gcmVzb2x2aW5nIGFsaWFzZXMsIHNvIHdlIGNvdWxkIGFsd2F5cyB0cnVzdCBpdC4pXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUucm9vbSA/IHRoaXMuc3RhdGUucm9vbS5yb29tSWQgOiB0aGlzLnN0YXRlLnJvb21JZDtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFBlcm1hbGlua0NyZWF0b3JGb3JSb29tOiBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9wZXJtYWxpbmtDcmVhdG9ycykgdGhpcy5fcGVybWFsaW5rQ3JlYXRvcnMgPSB7fTtcclxuICAgICAgICBpZiAodGhpcy5fcGVybWFsaW5rQ3JlYXRvcnNbcm9vbS5yb29tSWRdKSByZXR1cm4gdGhpcy5fcGVybWFsaW5rQ3JlYXRvcnNbcm9vbS5yb29tSWRdO1xyXG5cclxuICAgICAgICB0aGlzLl9wZXJtYWxpbmtDcmVhdG9yc1tyb29tLnJvb21JZF0gPSBuZXcgUm9vbVBlcm1hbGlua0NyZWF0b3Iocm9vbSk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucm9vbSAmJiByb29tLnJvb21JZCA9PT0gdGhpcy5zdGF0ZS5yb29tLnJvb21JZCkge1xyXG4gICAgICAgICAgICAvLyBXZSB3YW50IHRvIHdhdGNoIGZvciBjaGFuZ2VzIGluIHRoZSBjcmVhdG9yIGZvciB0aGUgcHJpbWFyeSByb29tIGluIHRoZSB2aWV3LCBidXRcclxuICAgICAgICAgICAgLy8gZG9uJ3QgbmVlZCB0byBkbyBzbyBmb3Igc2VhcmNoIHJlc3VsdHMuXHJcbiAgICAgICAgICAgIHRoaXMuX3Blcm1hbGlua0NyZWF0b3JzW3Jvb20ucm9vbUlkXS5zdGFydCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Blcm1hbGlua0NyZWF0b3JzW3Jvb20ucm9vbUlkXS5sb2FkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9wZXJtYWxpbmtDcmVhdG9yc1tyb29tLnJvb21JZF07XHJcbiAgICB9LFxyXG5cclxuICAgIF9zdG9wQWxsUGVybWFsaW5rQ3JlYXRvcnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fcGVybWFsaW5rQ3JlYXRvcnMpIHJldHVybjtcclxuICAgICAgICBmb3IgKGNvbnN0IHJvb21JZCBvZiBPYmplY3Qua2V5cyh0aGlzLl9wZXJtYWxpbmtDcmVhdG9ycykpIHtcclxuICAgICAgICAgICAgdGhpcy5fcGVybWFsaW5rQ3JlYXRvcnNbcm9vbUlkXS5zdG9wKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25XaWRnZXRFY2hvU3RvcmVVcGRhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzaG93QXBwczogdGhpcy5fc2hvdWxkU2hvd0FwcHModGhpcy5zdGF0ZS5yb29tKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NldHVwUm9vbTogZnVuY3Rpb24ocm9vbSwgcm9vbUlkLCBqb2luaW5nLCBzaG91bGRQZWVrKSB7XHJcbiAgICAgICAgLy8gaWYgdGhpcyBpcyBhbiB1bmtub3duIHJvb20gdGhlbiB3ZSdyZSBpbiBvbmUgb2YgdGhyZWUgc3RhdGVzOlxyXG4gICAgICAgIC8vIC0gVGhpcyBpcyBhIHJvb20gd2UgY2FuIHBlZWsgaW50byAoc2VhcmNoIGVuZ2luZSkgKHdlIGNhbiAvcGVlaylcclxuICAgICAgICAvLyAtIFRoaXMgaXMgYSByb29tIHdlIGNhbiBwdWJsaWNseSBqb2luIG9yIHdlcmUgaW52aXRlZCB0by4gKHdlIGNhbiAvam9pbilcclxuICAgICAgICAvLyAtIFRoaXMgaXMgYSByb29tIHdlIGNhbm5vdCBqb2luIGF0IGFsbC4gKG5vIGFjdGlvbiBjYW4gaGVscCB1cylcclxuICAgICAgICAvLyBXZSBjYW4ndCB0cnkgdG8gL2pvaW4gYmVjYXVzZSB0aGlzIG1heSBpbXBsaWNpdGx5IGFjY2VwdCBpbnZpdGVzICghKVxyXG4gICAgICAgIC8vIFdlIGNhbiAvcGVlayB0aG91Z2guIElmIGl0IGZhaWxzIHRoZW4gd2UgcHJlc2VudCB0aGUgam9pbiBVSS4gSWYgaXRcclxuICAgICAgICAvLyBzdWNjZWVkcyB0aGVuIGdyZWF0LCBzaG93IHRoZSBwcmV2aWV3IChidXQgd2Ugc3RpbGwgbWF5IGJlIGFibGUgdG8gL2pvaW4hKS5cclxuICAgICAgICAvLyBOb3RlIHRoYXQgcGVla2luZyB3b3JrcyBieSByb29tIElEIGFuZCByb29tIElEIG9ubHksIGFzIG9wcG9zZWQgdG8gam9pbmluZ1xyXG4gICAgICAgIC8vIHdoaWNoIG11c3QgYmUgYnkgYWxpYXMgb3IgaW52aXRlIHdoZXJldmVyIHBvc3NpYmxlIChwZWVraW5nIGN1cnJlbnRseSBkb2VzXHJcbiAgICAgICAgLy8gbm90IHdvcmsgb3ZlciBmZWRlcmF0aW9uKS5cclxuXHJcbiAgICAgICAgLy8gTkIuIFdlIHBlZWsgaWYgd2UgaGF2ZSBuZXZlciBzZWVuIHRoZSByb29tIGJlZm9yZSAoaS5lLiBqcy1zZGsgZG9lcyBub3Qga25vd1xyXG4gICAgICAgIC8vIGFib3V0IGl0KS4gV2UgZG9uJ3QgcGVlayBpbiB0aGUgaGlzdG9yaWNhbCBjYXNlIHdoZXJlIHdlIHdlcmUgam9pbmVkIGJ1dCBhcmVcclxuICAgICAgICAvLyBub3cgbm90IGpvaW5lZCBiZWNhdXNlIHRoZSBqcy1zZGsgcGVla2luZyBBUEkgd2lsbCBjbG9iYmVyIG91ciBoaXN0b3JpY2FsIHJvb20sXHJcbiAgICAgICAgLy8gbWFraW5nIGl0IGltcG9zc2libGUgdG8gaW5kaWNhdGUgYSBuZXdseSBqb2luZWQgcm9vbS5cclxuICAgICAgICBpZiAoIWpvaW5pbmcgJiYgcm9vbUlkKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmF1dG9Kb2luKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uSm9pbkJ1dHRvbkNsaWNrZWQoKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICghcm9vbSAmJiBzaG91bGRQZWVrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmluZm8oXCJBdHRlbXB0aW5nIHRvIHBlZWsgaW50byByb29tICVzXCIsIHJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBwZWVrTG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBpc1BlZWtpbmc6IHRydWUsIC8vIHRoaXMgd2lsbCBjaGFuZ2UgdG8gZmFsc2UgaWYgcGVla2luZyBmYWlsc1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHQucGVla0luUm9vbShyb29tSWQpLnRoZW4oKHJvb20pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbTogcm9vbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGVla0xvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX29uUm9vbUxvYWRlZChyb29tKTtcclxuICAgICAgICAgICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gU3RvcCBwZWVraW5nIGlmIGFueXRoaW5nIHdlbnQgd3JvbmdcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNQZWVraW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVGhpcyB3b24ndCBuZWNlc3NhcmlseSBiZSBhIE1hdHJpeEVycm9yLCBidXQgd2UgZHVjay10eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSBhbmQgc2F5IGlmIGl0J3MgZ290IGFuICdlcnJjb2RlJyBrZXkgd2l0aCB0aGUgcmlnaHQgdmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaXQgbWVhbnMgd2UgY2FuJ3QgcGVlay5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyLmVycmNvZGUgPT09IFwiTV9HVUVTVF9BQ0NFU1NfRk9SQklEREVOXCIgfHwgZXJyLmVycmNvZGUgPT09ICdNX0ZPUkJJRERFTicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVGhpcyBpcyBmaW5lOiB0aGUgcm9vbSBqdXN0IGlzbid0IHBlZWthYmxlICh3ZSBhc3N1bWUpLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBlZWtMb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgZXJyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJvb20pIHtcclxuICAgICAgICAgICAgICAgIC8vIFN0b3AgcGVla2luZyBiZWNhdXNlIHdlIGhhdmUgam9pbmVkIHRoaXMgcm9vbSBwcmV2aW91c2x5XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHQuc3RvcFBlZWtpbmcoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzUGVla2luZzogZmFsc2V9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3Nob3VsZFNob3dBcHBzOiBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgaWYgKCFCUk9XU0VSX1NVUFBPUlRTX1NBTkRCT1gpIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgLy8gQ2hlY2sgaWYgdXNlciBoYXMgcHJldmlvdXNseSBjaG9zZW4gdG8gaGlkZSB0aGUgYXBwIGRyYXdlciBmb3IgdGhpc1xyXG4gICAgICAgIC8vIHJvb20uIElmIHNvLCBkbyBub3Qgc2hvdyBhcHBzXHJcbiAgICAgICAgY29uc3QgaGlkZVdpZGdldERyYXdlciA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFxyXG4gICAgICAgICAgICByb29tLnJvb21JZCArIFwiX2hpZGVfd2lkZ2V0X2RyYXdlclwiKTtcclxuXHJcbiAgICAgICAgaWYgKGhpZGVXaWRnZXREcmF3ZXIgPT09IFwidHJ1ZVwiKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHdpZGdldHMgPSBXaWRnZXRFY2hvU3RvcmUuZ2V0RWNob2VkUm9vbVdpZGdldHMocm9vbS5yb29tSWQsIFdpZGdldFV0aWxzLmdldFJvb21XaWRnZXRzKHJvb20pKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHdpZGdldHMubGVuZ3RoID4gMCB8fCBXaWRnZXRFY2hvU3RvcmUucm9vbUhhc1BlbmRpbmdXaWRnZXRzKHJvb20ucm9vbUlkLCBXaWRnZXRVdGlscy5nZXRSb29tV2lkZ2V0cyhyb29tKSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjYWxsID0gdGhpcy5fZ2V0Q2FsbEZvclJvb20oKTtcclxuICAgICAgICBjb25zdCBjYWxsU3RhdGUgPSBjYWxsID8gY2FsbC5jYWxsX3N0YXRlIDogXCJlbmRlZFwiO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjYWxsU3RhdGU6IGNhbGxTdGF0ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQ29uZkNhbGxOb3RpZmljYXRpb24oKTtcclxuXHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2JlZm9yZXVubG9hZCcsIHRoaXMub25QYWdlVW5sb2FkKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5yZXNpemVOb3RpZmllcikge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLnJlc2l6ZU5vdGlmaWVyLm9uKFwibWlkZGxlUGFuZWxSZXNpemVkXCIsIHRoaXMub25SZXNpemUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9uUmVzaXplKCk7XHJcblxyXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIHRoaXMub25LZXlEb3duKTtcclxuICAgIH0sXHJcblxyXG4gICAgc2hvdWxkQ29tcG9uZW50VXBkYXRlOiBmdW5jdGlvbihuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xyXG4gICAgICAgIHJldHVybiAoIU9iamVjdFV0aWxzLnNoYWxsb3dFcXVhbCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpIHx8XHJcbiAgICAgICAgICAgICAgICAhT2JqZWN0VXRpbHMuc2hhbGxvd0VxdWFsKHRoaXMuc3RhdGUsIG5leHRTdGF0ZSkpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9yb29tVmlldy5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21WaWV3ID0gdGhpcy5fcm9vbVZpZXcuY3VycmVudDtcclxuICAgICAgICAgICAgaWYgKCFyb29tVmlldy5vbmRyb3ApIHtcclxuICAgICAgICAgICAgICAgIHJvb21WaWV3LmFkZEV2ZW50TGlzdGVuZXIoJ2Ryb3AnLCB0aGlzLm9uRHJvcCk7XHJcbiAgICAgICAgICAgICAgICByb29tVmlldy5hZGRFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIHRoaXMub25EcmFnT3Zlcik7XHJcbiAgICAgICAgICAgICAgICByb29tVmlldy5hZGRFdmVudExpc3RlbmVyKCdkcmFnbGVhdmUnLCB0aGlzLm9uRHJhZ0xlYXZlT3JFbmQpO1xyXG4gICAgICAgICAgICAgICAgcm9vbVZpZXcuYWRkRXZlbnRMaXN0ZW5lcignZHJhZ2VuZCcsIHRoaXMub25EcmFnTGVhdmVPckVuZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIE5vdGU6IFdlIGNoZWNrIHRoZSByZWYgaGVyZSB3aXRoIGEgZmxhZyBiZWNhdXNlIGNvbXBvbmVudERpZE1vdW50LCBkZXNwaXRlXHJcbiAgICAgICAgLy8gZG9jdW1lbnRhdGlvbiwgZG9lcyBub3QgZGVmaW5lIG91ciBtZXNzYWdlUGFuZWwgcmVmLiBJdCBsb29rcyBsaWtlIG91ciBzcGlubmVyXHJcbiAgICAgICAgLy8gaW4gcmVuZGVyKCkgcHJldmVudHMgdGhlIHJlZiBmcm9tIGJlaW5nIHNldCBvbiBmaXJzdCBtb3VudCwgc28gd2UgdHJ5IGFuZFxyXG4gICAgICAgIC8vIGNhdGNoIHRoZSBtZXNzYWdlUGFuZWwgd2hlbiBpdCBkb2VzIG1vdW50LiBCZWNhdXNlIHdlIG9ubHkgd2FudCB0aGUgcmVmIG9uY2UsXHJcbiAgICAgICAgLy8gd2UgdXNlIGEgYm9vbGVhbiBmbGFnIHRvIGF2b2lkIGR1cGxpY2F0ZSB3b3JrLlxyXG4gICAgICAgIGlmICh0aGlzLl9tZXNzYWdlUGFuZWwgJiYgIXRoaXMuc3RhdGUuYXRFbmRPZkxpdmVUaW1lbGluZUluaXQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBhdEVuZE9mTGl2ZVRpbWVsaW5lSW5pdDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGF0RW5kT2ZMaXZlVGltZWxpbmU6IHRoaXMuX21lc3NhZ2VQYW5lbC5pc0F0RW5kT2ZMaXZlVGltZWxpbmUoKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gc2V0IGEgYm9vbGVhbiB0byBzYXkgd2UndmUgYmVlbiB1bm1vdW50ZWQsIHdoaWNoIGFueSBwZW5kaW5nXHJcbiAgICAgICAgLy8gcHJvbWlzZXMgY2FuIHVzZSB0byB0aHJvdyBhd2F5IHRoZWlyIHJlc3VsdHMuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyAoV2UgY291bGQgdXNlIGlzTW91bnRlZCwgYnV0IGZhY2Vib29rIGhhdmUgZGVwcmVjYXRlZCB0aGF0LilcclxuICAgICAgICB0aGlzLnVubW91bnRlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIC8vIHVwZGF0ZSB0aGUgc2Nyb2xsIG1hcCBiZWZvcmUgd2UgZ2V0IHVubW91bnRlZFxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJvb21JZCkge1xyXG4gICAgICAgICAgICBSb29tU2Nyb2xsU3RhdGVTdG9yZS5zZXRTY3JvbGxTdGF0ZSh0aGlzLnN0YXRlLnJvb21JZCwgdGhpcy5fZ2V0U2Nyb2xsU3RhdGUoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5zaG91bGRQZWVrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5zdG9wUGVla2luZygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gc3RvcCB0cmFja2luZyByb29tIGNoYW5nZXMgdG8gZm9ybWF0IHBlcm1hbGlua3NcclxuICAgICAgICB0aGlzLl9zdG9wQWxsUGVybWFsaW5rQ3JlYXRvcnMoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX3Jvb21WaWV3LmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgLy8gZGlzY29ubmVjdCB0aGUgRCZEIGV2ZW50IGxpc3RlbmVycyBmcm9tIHRoZSByb29tIHZpZXcuIFRoaXNcclxuICAgICAgICAgICAgLy8gaXMgcmVhbGx5IGp1c3QgZm9yIGh5Z2llbmUgLSB3ZSdyZSBnb2luZyB0byBiZVxyXG4gICAgICAgICAgICAvLyBkZWxldGVkIGFueXdheSwgc28gaXQgZG9lc24ndCBtYXR0ZXIgaWYgdGhlIGV2ZW50IGxpc3RlbmVyc1xyXG4gICAgICAgICAgICAvLyBkb24ndCBnZXQgY2xlYW5lZCB1cC5cclxuICAgICAgICAgICAgY29uc3Qgcm9vbVZpZXcgPSB0aGlzLl9yb29tVmlldy5jdXJyZW50O1xyXG4gICAgICAgICAgICByb29tVmlldy5yZW1vdmVFdmVudExpc3RlbmVyKCdkcm9wJywgdGhpcy5vbkRyb3ApO1xyXG4gICAgICAgICAgICByb29tVmlldy5yZW1vdmVFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIHRoaXMub25EcmFnT3Zlcik7XHJcbiAgICAgICAgICAgIHJvb21WaWV3LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIHRoaXMub25EcmFnTGVhdmVPckVuZCk7XHJcbiAgICAgICAgICAgIHJvb21WaWV3LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2RyYWdlbmQnLCB0aGlzLm9uRHJhZ0xlYXZlT3JFbmQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLmRpc3BhdGNoZXJSZWYpO1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRleHQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0LnJlbW92ZUxpc3RlbmVyKFwiUm9vbVwiLCB0aGlzLm9uUm9vbSk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5yZW1vdmVMaXN0ZW5lcihcIlJvb20udGltZWxpbmVcIiwgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5yZW1vdmVMaXN0ZW5lcihcIlJvb20ubmFtZVwiLCB0aGlzLm9uUm9vbU5hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLmFjY291bnREYXRhXCIsIHRoaXMub25Sb29tQWNjb3VudERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMub25Sb29tU3RhdGVFdmVudHMpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLm15TWVtYmVyc2hpcFwiLCB0aGlzLm9uTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0LnJlbW92ZUxpc3RlbmVyKFwiUm9vbVN0YXRlLm1lbWJlcnNcIiwgdGhpcy5vblJvb21TdGF0ZU1lbWJlcik7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5yZW1vdmVMaXN0ZW5lcihcImFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5yZW1vdmVMaXN0ZW5lcihcImNyeXB0by5rZXlCYWNrdXBTdGF0dXNcIiwgdGhpcy5vbktleUJhY2t1cFN0YXR1cyk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dC5yZW1vdmVMaXN0ZW5lcihcImRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWRcIiwgdGhpcy5vbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIHRoaXMub25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignYmVmb3JldW5sb2FkJywgdGhpcy5vblBhZ2VVbmxvYWQpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlc2l6ZU5vdGlmaWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIucmVtb3ZlTGlzdGVuZXIoXCJtaWRkbGVQYW5lbFJlc2l6ZWRcIiwgdGhpcy5vblJlc2l6ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLm9uS2V5RG93bik7XHJcblxyXG4gICAgICAgIC8vIFJlbW92ZSBSb29tU3RvcmUgbGlzdGVuZXJcclxuICAgICAgICBpZiAodGhpcy5fcm9vbVN0b3JlVG9rZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fcm9vbVN0b3JlVG9rZW4ucmVtb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFJlbW92ZSBSaWdodFBhbmVsU3RvcmUgbGlzdGVuZXJcclxuICAgICAgICBpZiAodGhpcy5fcmlnaHRQYW5lbFN0b3JlVG9rZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fcmlnaHRQYW5lbFN0b3JlVG9rZW4ucmVtb3ZlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBXaWRnZXRFY2hvU3RvcmUucmVtb3ZlTGlzdGVuZXIoJ3VwZGF0ZScsIHRoaXMuX29uV2lkZ2V0RWNob1N0b3JlVXBkYXRlKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX3Nob3dSZWFkUmVjZWlwdHNXYXRjaFJlZikge1xyXG4gICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnVud2F0Y2hTZXR0aW5nKHRoaXMuX3Nob3dSZWFkUmVjZWlwdHNXYXRjaFJlZik7XHJcbiAgICAgICAgICAgIHRoaXMuX3Nob3dSZWFkUmVjZWlwdHNXYXRjaFJlZiA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjYW5jZWwgYW55IHBlbmRpbmcgY2FsbHMgdG8gdGhlIHJhdGVfbGltaXRlZF9mdW5jc1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVJvb21NZW1iZXJzLmNhbmNlbFBlbmRpbmdDYWxsKCk7XHJcblxyXG4gICAgICAgIC8vIG5vIG5lZWQgdG8gZG8gdGhpcyBhcyBEaXIgJiBTZXR0aW5ncyBhcmUgbm93IG92ZXJsYXlzLiBJdCBqdXN0IGJ1cm50IENQVS5cclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIlRpbnRlci50aW50IGZyb20gUm9vbVZpZXcudW5tb3VudFwiKTtcclxuICAgICAgICAvLyBUaW50ZXIudGludCgpOyAvLyByZXNldCBjb2xvdXJzY2hlbWVcclxuICAgIH0sXHJcblxyXG4gICAgX29uUmlnaHRQYW5lbFN0b3JlVXBkYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2hvd1JpZ2h0UGFuZWw6IFJpZ2h0UGFuZWxTdG9yZS5nZXRTaGFyZWRJbnN0YW5jZSgpLmlzT3BlbkZvclJvb20sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUGFnZVVubG9hZChldmVudCkge1xyXG4gICAgICAgIGlmIChDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5nZXRDdXJyZW50VXBsb2FkcygpLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGV2ZW50LnJldHVyblZhbHVlID1cclxuICAgICAgICAgICAgICAgIF90KFwiWW91IHNlZW0gdG8gYmUgdXBsb2FkaW5nIGZpbGVzLCBhcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gcXVpdD9cIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9nZXRDYWxsRm9yUm9vbSgpICYmIHRoaXMuc3RhdGUuY2FsbFN0YXRlICE9PSAnZW5kZWQnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBldmVudC5yZXR1cm5WYWx1ZSA9XHJcbiAgICAgICAgICAgICAgICBfdChcIllvdSBzZWVtIHRvIGJlIGluIGEgY2FsbCwgYXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIHF1aXQ/XCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25LZXlEb3duOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGxldCBoYW5kbGVkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgY3RybENtZE9ubHkgPSBpc09ubHlDdHJsT3JDbWRLZXlFdmVudChldik7XHJcblxyXG4gICAgICAgIHN3aXRjaCAoZXYua2V5KSB7XHJcbiAgICAgICAgICAgIGNhc2UgS2V5LkQ6XHJcbiAgICAgICAgICAgICAgICBpZiAoY3RybENtZE9ubHkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uTXV0ZUF1ZGlvQ2xpY2soKTtcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBLZXkuRTpcclxuICAgICAgICAgICAgICAgIGlmIChjdHJsQ21kT25seSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25NdXRlVmlkZW9DbGljaygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoaGFuZGxlZCkge1xyXG4gICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWN0aW9uOiBmdW5jdGlvbihwYXlsb2FkKSB7XHJcbiAgICAgICAgc3dpdGNoIChwYXlsb2FkLmFjdGlvbikge1xyXG4gICAgICAgICAgICBjYXNlICdtZXNzYWdlX3NlbmRfZmFpbGVkJzpcclxuICAgICAgICAgICAgY2FzZSAnbWVzc2FnZV9zZW50JzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX2NoZWNrSWZBbG9uZSh0aGlzLnN0YXRlLnJvb20pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3Bvc3Rfc3RpY2tlcl9tZXNzYWdlJzpcclxuICAgICAgICAgICAgICB0aGlzLmluamVjdFN0aWNrZXIoXHJcbiAgICAgICAgICAgICAgICAgIHBheWxvYWQuZGF0YS5jb250ZW50LnVybCxcclxuICAgICAgICAgICAgICAgICAgcGF5bG9hZC5kYXRhLmNvbnRlbnQuaW5mbyxcclxuICAgICAgICAgICAgICAgICAgcGF5bG9hZC5kYXRhLmRlc2NyaXB0aW9uIHx8IHBheWxvYWQuZGF0YS5uYW1lKTtcclxuICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAncGljdHVyZV9zbmFwc2hvdCc6XHJcbiAgICAgICAgICAgICAgICBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5zZW5kQ29udGVudExpc3RUb1Jvb20oW3BheWxvYWQuZmlsZV0sIHRoaXMuc3RhdGUucm9vbS5yb29tSWQsIHRoaXMuY29udGV4dCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnbm90aWZpZXJfZW5hYmxlZCc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3VwbG9hZF9zdGFydGVkJzpcclxuICAgICAgICAgICAgY2FzZSAndXBsb2FkX2ZpbmlzaGVkJzpcclxuICAgICAgICAgICAgY2FzZSAndXBsb2FkX2NhbmNlbGVkJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdjYWxsX3N0YXRlJzpcclxuICAgICAgICAgICAgICAgIC8vIGRvbid0IGZpbHRlciBvdXQgcGF5bG9hZHMgZm9yIHJvb20gSURzIG90aGVyIHRoYW4gcHJvcHMucm9vbSBiZWNhdXNlXHJcbiAgICAgICAgICAgICAgICAvLyB3ZSBtYXkgYmUgaW50ZXJlc3RlZCBpbiB0aGUgY29uZiAxOjEgcm9vbVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghcGF5bG9hZC5yb29tX2lkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHZhciBjYWxsID0gdGhpcy5fZ2V0Q2FsbEZvclJvb20oKTtcclxuICAgICAgICAgICAgICAgIHZhciBjYWxsU3RhdGU7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGNhbGwpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYWxsU3RhdGUgPSBjYWxsLmNhbGxfc3RhdGU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbGxTdGF0ZSA9IFwiZW5kZWRcIjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBwb3NzaWJseSByZW1vdmUgdGhlIGNvbmYgY2FsbCBub3RpZmljYXRpb24gaWYgd2UncmUgbm93IGluXHJcbiAgICAgICAgICAgICAgICAvLyB0aGUgY29uZlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdXBkYXRlQ29uZkNhbGxOb3RpZmljYXRpb24oKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBjYWxsU3RhdGU6IGNhbGxTdGF0ZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdhcHBzRHJhd2VyJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3dBcHBzOiBwYXlsb2FkLnNob3csXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdyZXBseV90b19ldmVudCc6XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzICYmIHBheWxvYWQuZXZlbnQuZ2V0Um9vbUlkKCkgPT09IHRoaXMuc3RhdGUucm9vbUlkICYmICF0aGlzLnVubW91bnRlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25DYW5jZWxTZWFyY2hDbGljaygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3F1b3RlJzpcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByb29tSWQgPSBwYXlsb2FkLmV2ZW50LmdldFJvb21JZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyb29tSWQgPT09IHRoaXMuc3RhdGUucm9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub25DYW5jZWxTZWFyY2hDbGljaygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0SW1tZWRpYXRlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tX2lkOiByb29tSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZF9hY3Rpb246IHBheWxvYWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21UaW1lbGluZTogZnVuY3Rpb24oZXYsIHJvb20sIHRvU3RhcnRPZlRpbWVsaW5lLCByZW1vdmVkLCBkYXRhKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIGlnbm9yZSBldmVudHMgZm9yIG90aGVyIHJvb21zXHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJvb20gfHwgcm9vbS5yb29tSWQgIT0gdGhpcy5zdGF0ZS5yb29tLnJvb21JZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBpZ25vcmUgZXZlbnRzIGZyb20gZmlsdGVyZWQgdGltZWxpbmVzXHJcbiAgICAgICAgaWYgKGRhdGEudGltZWxpbmUuZ2V0VGltZWxpbmVTZXQoKSAhPT0gcm9vbS5nZXRVbmZpbHRlcmVkVGltZWxpbmVTZXQoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoZXYuZ2V0VHlwZSgpID09PSBcIm9yZy5tYXRyaXgucm9vbS5wcmV2aWV3X3VybHNcIikge1xyXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVQcmV2aWV3VXJsVmlzaWJpbGl0eShyb29tKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgPT09IFwibS5yb29tLmVuY3J5cHRpb25cIikge1xyXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVFMkVTdGF0dXMocm9vbSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZ25vcmUgYW55dGhpbmcgYnV0IHJlYWwtdGltZSB1cGRhdGVzIGF0IHRoZSBlbmQgb2YgdGhlIHJvb206XHJcbiAgICAgICAgLy8gdXBkYXRlcyBmcm9tIHBhZ2luYXRpb24gd2lsbCBoYXBwZW4gd2hlbiB0aGUgcGFnaW5hdGUgY29tcGxldGVzLlxyXG4gICAgICAgIGlmICh0b1N0YXJ0T2ZUaW1lbGluZSB8fCAhZGF0YSB8fCAhZGF0YS5saXZlRXZlbnQpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gbm8gcG9pbnQgaGFuZGxpbmcgYW55dGhpbmcgd2hpbGUgd2UncmUgd2FpdGluZyBmb3IgdGhlIGpvaW4gdG8gZmluaXNoOlxyXG4gICAgICAgIC8vIHdlJ2xsIG9ubHkgYmUgc2hvd2luZyBhIHNwaW5uZXIuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuam9pbmluZykgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoZXYuZ2V0U2VuZGVyKCkgIT09IHRoaXMuY29udGV4dC5jcmVkZW50aWFscy51c2VySWQpIHtcclxuICAgICAgICAgICAgLy8gdXBkYXRlIHVucmVhZCBjb3VudCB3aGVuIHNjcm9sbGVkIHVwXHJcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzICYmIHRoaXMuc3RhdGUuYXRFbmRPZkxpdmVUaW1lbGluZSkge1xyXG4gICAgICAgICAgICAgICAgLy8gbm8gY2hhbmdlXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIXNob3VsZEhpZGVFdmVudChldikpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoKHN0YXRlLCBwcm9wcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7bnVtVW5yZWFkTWVzc2FnZXM6IHN0YXRlLm51bVVucmVhZE1lc3NhZ2VzICsgMX07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tTmFtZTogZnVuY3Rpb24ocm9vbSkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJvb20gJiYgcm9vbS5yb29tSWQgPT0gdGhpcy5zdGF0ZS5yb29tLnJvb21JZCkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21SZWNvdmVyeVJlbWluZGVyRG9udEFza0FnYWluOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBDYWxsZWQgd2hlbiB0aGUgb3B0aW9uIHRvIG5vdCBhc2sgYWdhaW4gaXMgc2V0OlxyXG4gICAgICAgIC8vIGZvcmNlIGFuIHVwZGF0ZSB0byBoaWRlIHRoZSByZWNvdmVyeSByZW1pbmRlclxyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25LZXlCYWNrdXBTdGF0dXMoKSB7XHJcbiAgICAgICAgLy8gS2V5IGJhY2t1cCBzdGF0dXMgY2hhbmdlcyBhZmZlY3Qgd2hldGhlciB0aGUgaW4tcm9vbSByZWNvdmVyeVxyXG4gICAgICAgIC8vIHJlbWluZGVyIGlzIGRpc3BsYXllZC5cclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNhblJlc2V0VGltZWxpbmU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fbWVzc2FnZVBhbmVsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZVBhbmVsLmNhblJlc2V0VGltZWxpbmUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gY2FsbGVkIHdoZW4gc3RhdGUucm9vbSBpcyBmaXJzdCBpbml0aWFsaXNlZCAoZWl0aGVyIGF0IGluaXRpYWwgbG9hZCxcclxuICAgIC8vIGFmdGVyIGEgc3VjY2Vzc2Z1bCBwZWVrLCBvciBhZnRlciB3ZSBqb2luIHRoZSByb29tKS5cclxuICAgIF9vblJvb21Mb2FkZWQ6IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICB0aGlzLl9jYWxjdWxhdGVQZWVrUnVsZXMocm9vbSk7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlUHJldmlld1VybFZpc2liaWxpdHkocm9vbSk7XHJcbiAgICAgICAgdGhpcy5fbG9hZE1lbWJlcnNJZkpvaW5lZChyb29tKTtcclxuICAgICAgICB0aGlzLl9jYWxjdWxhdGVSZWNvbW1lbmRlZFZlcnNpb24ocm9vbSk7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlRTJFU3RhdHVzKHJvb20pO1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVBlcm1pc3Npb25zKHJvb20pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfY2FsY3VsYXRlUmVjb21tZW5kZWRWZXJzaW9uOiBhc3luYyBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHVwZ3JhZGVSZWNvbW1lbmRhdGlvbjogYXdhaXQgcm9vbS5nZXRSZWNvbW1lbmRlZFZlcnNpb24oKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2xvYWRNZW1iZXJzSWZKb2luZWQ6IGFzeW5jIGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICAvLyBsYXp5IGxvYWQgbWVtYmVycyBpZiBlbmFibGVkXHJcbiAgICAgICAgaWYgKHRoaXMuY29udGV4dC5oYXNMYXp5TG9hZE1lbWJlcnNFbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgaWYgKHJvb20gJiYgcm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gJ2pvaW4nKSB7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHJvb20ubG9hZE1lbWJlcnNJZk5lZWRlZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy51bm1vdW50ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bWVtYmVyc0xvYWRlZDogdHJ1ZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVycm9yTWVzc2FnZSA9IGBGZXRjaGluZyByb29tIG1lbWJlcnMgZm9yICR7cm9vbS5yb29tSWR9IGZhaWxlZC5gICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCIgUm9vbSBtZW1iZXJzIHdpbGwgYXBwZWFyIGluY29tcGxldGUuXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2NhbGN1bGF0ZVBlZWtSdWxlczogZnVuY3Rpb24ocm9vbSkge1xyXG4gICAgICAgIGNvbnN0IGd1ZXN0QWNjZXNzRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5ndWVzdF9hY2Nlc3NcIiwgXCJcIik7XHJcbiAgICAgICAgaWYgKGd1ZXN0QWNjZXNzRXZlbnQgJiYgZ3Vlc3RBY2Nlc3NFdmVudC5nZXRDb250ZW50KCkuZ3Vlc3RfYWNjZXNzID09PSBcImNhbl9qb2luXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBndWVzdHNDYW5Kb2luOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGhpc3RvcnlWaXNpYmlsaXR5ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20uaGlzdG9yeV92aXNpYmlsaXR5XCIsIFwiXCIpO1xyXG4gICAgICAgIGlmIChoaXN0b3J5VmlzaWJpbGl0eSAmJiBoaXN0b3J5VmlzaWJpbGl0eS5nZXRDb250ZW50KCkuaGlzdG9yeV92aXNpYmlsaXR5ID09PSBcIndvcmxkX3JlYWRhYmxlXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBjYW5QZWVrOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF91cGRhdGVQcmV2aWV3VXJsVmlzaWJpbGl0eTogZnVuY3Rpb24oe3Jvb21JZH0pIHtcclxuICAgICAgICAvLyBVUkwgUHJldmlld3MgaW4gRTJFRSByb29tcyBjYW4gYmUgYSBwcml2YWN5IGxlYWsgc28gdXNlIGEgZGlmZmVyZW50IHNldHRpbmcgd2hpY2ggaXMgcGVyLXJvb20gZXhwbGljaXRcclxuICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmNvbnRleHQuaXNSb29tRW5jcnlwdGVkKHJvb21JZCkgPyAndXJsUHJldmlld3NFbmFibGVkX2UyZWUnIDogJ3VybFByZXZpZXdzRW5hYmxlZCc7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNob3dVcmxQcmV2aWV3OiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKGtleSwgcm9vbUlkKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tOiBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgaWYgKCFyb29tIHx8IHJvb20ucm9vbUlkICE9PSB0aGlzLnN0YXRlLnJvb21JZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICByb29tOiByb29tLFxyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fb25Sb29tTG9hZGVkKHJvb20pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQ6IGZ1bmN0aW9uKHVzZXJJZCwgZGV2aWNlKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuc3RhdGUucm9vbTtcclxuICAgICAgICBpZiAoIXJvb20uY3VycmVudFN0YXRlLmdldE1lbWJlcih1c2VySWQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlRTJFU3RhdHVzKHJvb20pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblVzZXJWZXJpZmljYXRpb25DaGFuZ2VkOiBmdW5jdGlvbih1c2VySWQsIF90cnVzdFN0YXR1cykge1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSB0aGlzLnN0YXRlLnJvb207XHJcbiAgICAgICAgaWYgKCFyb29tIHx8ICFyb29tLmN1cnJlbnRTdGF0ZS5nZXRNZW1iZXIodXNlcklkKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUUyRVN0YXR1cyhyb29tKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3VwZGF0ZUUyRVN0YXR1czogYXN5bmMgZnVuY3Rpb24ocm9vbSkge1xyXG4gICAgICAgIGlmICghdGhpcy5jb250ZXh0LmlzUm9vbUVuY3J5cHRlZChyb29tLnJvb21JZCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMuY29udGV4dC5pc0NyeXB0b0VuYWJsZWQoKSkge1xyXG4gICAgICAgICAgICAvLyBJZiBjcnlwdG8gaXMgbm90IGN1cnJlbnRseSBlbmFibGVkLCB3ZSBhcmVuJ3QgdHJhY2tpbmcgZGV2aWNlcyBhdCBhbGwsXHJcbiAgICAgICAgICAgIC8vIHNvIHdlIGRvbid0IGtub3cgd2hhdCB0aGUgYW5zd2VyIGlzLiBMZXQncyBlcnJvciBvbiB0aGUgc2FmZSBzaWRlIGFuZCBzaG93XHJcbiAgICAgICAgICAgIC8vIGEgd2FybmluZyBmb3IgdGhpcyBjYXNlLlxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGUyZVN0YXR1czogXCJ3YXJuaW5nXCIsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgICAgIHJvb20uaGFzVW52ZXJpZmllZERldmljZXMoKS50aGVuKChoYXNVbnZlcmlmaWVkRGV2aWNlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZTJlU3RhdHVzOiBoYXNVbnZlcmlmaWVkRGV2aWNlcyA/IFwid2FybmluZ1wiIDogXCJ2ZXJpZmllZFwiLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcImUyZSBjaGVjayBpcyB3YXJuaW5nL3ZlcmlmaWVkIG9ubHkgYXMgY3Jvc3Mtc2lnbmluZyBpcyBvZmZcIik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qIEF0IHRoaXMgcG9pbnQsIHRoZSB1c2VyIGhhcyBlbmNyeXB0aW9uIG9uIGFuZCBjcm9zcy1zaWduaW5nIG9uICovXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGUyZVN0YXR1czogYXdhaXQgc2hpZWxkU3RhdHVzRm9yUm9vbSh0aGlzLmNvbnRleHQsIHJvb20pLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICB1cGRhdGVUaW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCByb29tID0gdGhpcy5zdGF0ZS5yb29tO1xyXG4gICAgICAgIGlmICghcm9vbSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIlRpbnRlci50aW50IGZyb20gdXBkYXRlVGludFwiKTtcclxuICAgICAgICBjb25zdCBjb2xvclNjaGVtZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJyb29tQ29sb3JcIiwgcm9vbS5yb29tSWQpO1xyXG4gICAgICAgIFRpbnRlci50aW50KGNvbG9yU2NoZW1lLnByaW1hcnlfY29sb3IsIGNvbG9yU2NoZW1lLnNlY29uZGFyeV9jb2xvcik7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWNjb3VudERhdGE6IGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgY29uc3QgdHlwZSA9IGV2ZW50LmdldFR5cGUoKTtcclxuICAgICAgICBpZiAoKHR5cGUgPT09IFwib3JnLm1hdHJpeC5wcmV2aWV3X3VybHNcIiB8fCB0eXBlID09PSBcImltLnZlY3Rvci53ZWIuc2V0dGluZ3NcIikgJiYgdGhpcy5zdGF0ZS5yb29tKSB7XHJcbiAgICAgICAgICAgIC8vIG5vbi1lMmVlIHVybCBwcmV2aWV3cyBhcmUgc3RvcmVkIGluIGxlZ2FjeSBldmVudCB0eXBlIGBvcmcubWF0cml4LnJvb20ucHJldmlld191cmxzYFxyXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVQcmV2aWV3VXJsVmlzaWJpbGl0eSh0aGlzLnN0YXRlLnJvb20pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tQWNjb3VudERhdGE6IGZ1bmN0aW9uKGV2ZW50LCByb29tKSB7XHJcbiAgICAgICAgaWYgKHJvb20ucm9vbUlkID09IHRoaXMuc3RhdGUucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHR5cGUgPSBldmVudC5nZXRUeXBlKCk7XHJcbiAgICAgICAgICAgIGlmICh0eXBlID09PSBcIm9yZy5tYXRyaXgucm9vbS5jb2xvcl9zY2hlbWVcIikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY29sb3JTY2hlbWUgPSBldmVudC5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgICAgICAgICAvLyBYWFg6IHdlIHNob3VsZCB2YWxpZGF0ZSB0aGUgZXZlbnRcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiVGludGVyLnRpbnQgZnJvbSBvblJvb21BY2NvdW50RGF0YVwiKTtcclxuICAgICAgICAgICAgICAgIFRpbnRlci50aW50KGNvbG9yU2NoZW1lLnByaW1hcnlfY29sb3IsIGNvbG9yU2NoZW1lLnNlY29uZGFyeV9jb2xvcik7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gXCJvcmcubWF0cml4LnJvb20ucHJldmlld191cmxzXCIgfHwgdHlwZSA9PT0gXCJpbS52ZWN0b3Iud2ViLnNldHRpbmdzXCIpIHtcclxuICAgICAgICAgICAgICAgIC8vIG5vbi1lMmVlIHVybCBwcmV2aWV3cyBhcmUgc3RvcmVkIGluIGxlZ2FjeSBldmVudCB0eXBlIGBvcmcubWF0cml4LnJvb20ucHJldmlld191cmxzYFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdXBkYXRlUHJldmlld1VybFZpc2liaWxpdHkocm9vbSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVN0YXRlRXZlbnRzOiBmdW5jdGlvbihldiwgc3RhdGUpIHtcclxuICAgICAgICAvLyBpZ25vcmUgaWYgd2UgZG9uJ3QgaGF2ZSBhIHJvb20geWV0XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJvb20gfHwgdGhpcy5zdGF0ZS5yb29tLnJvb21JZCAhPT0gc3RhdGUucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVBlcm1pc3Npb25zKHRoaXMuc3RhdGUucm9vbSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVN0YXRlTWVtYmVyOiBmdW5jdGlvbihldiwgc3RhdGUsIG1lbWJlcikge1xyXG4gICAgICAgIC8vIGlnbm9yZSBpZiB3ZSBkb24ndCBoYXZlIGEgcm9vbSB5ZXRcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUucm9vbSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZ25vcmUgbWVtYmVycyBpbiBvdGhlciByb29tc1xyXG4gICAgICAgIGlmIChtZW1iZXIucm9vbUlkICE9PSB0aGlzLnN0YXRlLnJvb20ucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVJvb21NZW1iZXJzKG1lbWJlcik7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTXlNZW1iZXJzaGlwOiBmdW5jdGlvbihyb29tLCBtZW1iZXJzaGlwLCBvbGRNZW1iZXJzaGlwKSB7XHJcbiAgICAgICAgaWYgKHJvb20ucm9vbUlkID09PSB0aGlzLnN0YXRlLnJvb21JZCkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvYWRNZW1iZXJzSWZKb2luZWQocm9vbSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZVBlcm1pc3Npb25zKHJvb20pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3VwZGF0ZVBlcm1pc3Npb25zOiBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgaWYgKHJvb20pIHtcclxuICAgICAgICAgICAgY29uc3QgbWUgPSB0aGlzLmNvbnRleHQuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGNhblJlYWN0ID0gcm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gXCJqb2luXCIgJiYgcm9vbS5jdXJyZW50U3RhdGUubWF5U2VuZEV2ZW50KFwibS5yZWFjdGlvblwiLCBtZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGNhblJlcGx5ID0gcm9vbS5tYXlTZW5kTWVzc2FnZSgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y2FuUmVhY3QsIGNhblJlcGx5fSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyByYXRlIGxpbWl0ZWQgYmVjYXVzZSBhIHBvd2VyIGxldmVsIGNoYW5nZSB3aWxsIGVtaXQgYW4gZXZlbnQgZm9yIGV2ZXJ5XHJcbiAgICAvLyBtZW1iZXIgaW4gdGhlIHJvb20uXHJcbiAgICBfdXBkYXRlUm9vbU1lbWJlcnM6IHJhdGVfbGltaXRlZF9mdW5jKGZ1bmN0aW9uKGR1ZVRvTWVtYmVyKSB7XHJcbiAgICAgICAgLy8gYSBtZW1iZXIgc3RhdGUgY2hhbmdlZCBpbiB0aGlzIHJvb21cclxuICAgICAgICAvLyByZWZyZXNoIHRoZSBjb25mIGNhbGwgbm90aWZpY2F0aW9uIHN0YXRlXHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQ29uZkNhbGxOb3RpZmljYXRpb24oKTtcclxuICAgICAgICB0aGlzLl91cGRhdGVETVN0YXRlKCk7XHJcblxyXG4gICAgICAgIGxldCBtZW1iZXJDb3VudEluZmx1ZW5jZSA9IDA7XHJcbiAgICAgICAgaWYgKGR1ZVRvTWVtYmVyICYmIGR1ZVRvTWVtYmVyLm1lbWJlcnNoaXAgPT09IFwiaW52aXRlXCIgJiYgdGhpcy5zdGF0ZS5yb29tLmdldEludml0ZWRNZW1iZXJDb3VudCgpID09PSAwKSB7XHJcbiAgICAgICAgICAgIC8vIEEgbWVtYmVyIGdvdCBpbnZpdGVkLCBidXQgdGhlIHJvb20gaGFzbid0IGRldGVjdGVkIHRoYXQgY2hhbmdlIHlldC4gSW5mbHVlbmNlIHRoZSBtZW1iZXJcclxuICAgICAgICAgICAgLy8gY291bnQgYnkgMSB0byBjb3VudGVyYWN0IHRoaXMuXHJcbiAgICAgICAgICAgIG1lbWJlckNvdW50SW5mbHVlbmNlID0gMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fY2hlY2tJZkFsb25lKHRoaXMuc3RhdGUucm9vbSwgbWVtYmVyQ291bnRJbmZsdWVuY2UpO1xyXG5cclxuICAgICAgICB0aGlzLl91cGRhdGVFMkVTdGF0dXModGhpcy5zdGF0ZS5yb29tKTtcclxuICAgIH0sIDUwMCksXHJcblxyXG4gICAgX2NoZWNrSWZBbG9uZTogZnVuY3Rpb24ocm9vbSwgY291bnRJbmZsdWVuY2UpIHtcclxuICAgICAgICBsZXQgd2FybmVkQWJvdXRMb25lbHlSb29tID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKGxvY2FsU3RvcmFnZSkge1xyXG4gICAgICAgICAgICB3YXJuZWRBYm91dExvbmVseVJvb20gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbXhfdXNlcl9hbG9uZV93YXJuZWRfJyArIHRoaXMuc3RhdGUucm9vbS5yb29tSWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAod2FybmVkQWJvdXRMb25lbHlSb29tKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmlzQWxvbmUpIHRoaXMuc2V0U3RhdGUoe2lzQWxvbmU6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBqb2luZWRPckludml0ZWRNZW1iZXJDb3VudCA9IHJvb20uZ2V0Sm9pbmVkTWVtYmVyQ291bnQoKSArIHJvb20uZ2V0SW52aXRlZE1lbWJlckNvdW50KCk7XHJcbiAgICAgICAgaWYgKGNvdW50SW5mbHVlbmNlKSBqb2luZWRPckludml0ZWRNZW1iZXJDb3VudCArPSBjb3VudEluZmx1ZW5jZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0Fsb25lOiBqb2luZWRPckludml0ZWRNZW1iZXJDb3VudCA9PT0gMX0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdXBkYXRlQ29uZkNhbGxOb3RpZmljYXRpb246IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSB0aGlzLnN0YXRlLnJvb207XHJcbiAgICAgICAgaWYgKCFyb29tIHx8ICF0aGlzLnByb3BzLkNvbmZlcmVuY2VIYW5kbGVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29uZk1lbWJlciA9IHJvb20uZ2V0TWVtYmVyKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLkNvbmZlcmVuY2VIYW5kbGVyLmdldENvbmZlcmVuY2VVc2VySWRGb3JSb29tKHJvb20ucm9vbUlkKSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBpZiAoIWNvbmZNZW1iZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBjb25mQ2FsbCA9IHRoaXMucHJvcHMuQ29uZmVyZW5jZUhhbmRsZXIuZ2V0Q29uZmVyZW5jZUNhbGxGb3JSb29tKGNvbmZNZW1iZXIucm9vbUlkKTtcclxuXHJcbiAgICAgICAgLy8gQSBjb25mIGNhbGwgbm90aWZpY2F0aW9uIHNob3VsZCBiZSBkaXNwbGF5ZWQgaWYgdGhlcmUgaXMgYW4gb25nb2luZ1xyXG4gICAgICAgIC8vIGNvbmYgY2FsbCBidXQgdGhpcyBjaWxlbnQgaXNuJ3QgYSBwYXJ0IG9mIGl0LlxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBkaXNwbGF5Q29uZkNhbGxOb3RpZmljYXRpb246IChcclxuICAgICAgICAgICAgICAgICghY29uZkNhbGwgfHwgY29uZkNhbGwuY2FsbF9zdGF0ZSA9PT0gXCJlbmRlZFwiKSAmJlxyXG4gICAgICAgICAgICAgICAgY29uZk1lbWJlci5tZW1iZXJzaGlwID09PSBcImpvaW5cIlxyXG4gICAgICAgICAgICApLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdXBkYXRlRE1TdGF0ZSgpIHtcclxuICAgICAgICBjb25zdCByb29tID0gdGhpcy5zdGF0ZS5yb29tO1xyXG4gICAgICAgIGlmIChyb29tLmdldE15TWVtYmVyc2hpcCgpICE9IFwiam9pblwiKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgZG1JbnZpdGVyID0gcm9vbS5nZXRETUludml0ZXIoKTtcclxuICAgICAgICBpZiAoZG1JbnZpdGVyKSB7XHJcbiAgICAgICAgICAgIFJvb21zLnNldERNUm9vbShyb29tLnJvb21JZCwgZG1JbnZpdGVyKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU2VhcmNoUmVzdWx0c0ZpbGxSZXF1ZXN0OiBmdW5jdGlvbihiYWNrd2FyZHMpIHtcclxuICAgICAgICBpZiAoIWJhY2t3YXJkcykge1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMubmV4dF9iYXRjaCkge1xyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcInJlcXVlc3RpbmcgbW9yZSBzZWFyY2ggcmVzdWx0c1wiKTtcclxuICAgICAgICAgICAgY29uc3Qgc2VhcmNoUHJvbWlzZSA9IHRoaXMuY29udGV4dC5iYWNrUGFnaW5hdGVSb29tRXZlbnRzU2VhcmNoKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2hhbmRsZVNlYXJjaFJlc3VsdChzZWFyY2hQcm9taXNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkZWJ1Z2xvZyhcIm5vIG1vcmUgc2VhcmNoIHJlc3VsdHNcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25JbnZpdGVCdXR0b25DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gY2FsbCBBZGRyZXNzUGlja2VyRGlhbG9nXHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19pbnZpdGUnLFxyXG4gICAgICAgICAgICByb29tSWQ6IHRoaXMuc3RhdGUucm9vbS5yb29tSWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNBbG9uZTogZmFsc2V9KTsgLy8gdGhlcmUncyBhIGdvb2QgY2hhbmNlIHRoZXknbGwgaW52aXRlIHNvbWVvbmVcclxuICAgIH0sXHJcblxyXG4gICAgb25TdG9wQWxvbmVXYXJuaW5nQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmIChsb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ214X3VzZXJfYWxvbmVfd2FybmVkXycgKyB0aGlzLnN0YXRlLnJvb20ucm9vbUlkLCB0cnVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNBbG9uZTogZmFsc2V9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Kb2luQnV0dG9uQ2xpY2tlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICAvLyBJZiB0aGUgdXNlciBpcyBhIFJPVSwgYWxsb3cgdGhlbSB0byB0cmFuc2l0aW9uIHRvIGEgUFdMVVxyXG4gICAgICAgIGlmICh0aGlzLmNvbnRleHQgJiYgdGhpcy5jb250ZXh0LmlzR3Vlc3QoKSkge1xyXG4gICAgICAgICAgICAvLyBKb2luIHRoaXMgcm9vbSBvbmNlIHRoZSB1c2VyIGhhcyByZWdpc3RlcmVkIGFuZCBsb2dnZWQgaW5cclxuICAgICAgICAgICAgLy8gKElmIHdlIGZhaWxlZCB0byBwZWVrLCB3ZSBtYXkgbm90IGhhdmUgYSB2YWxpZCByb29tIG9iamVjdC4pXHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdkb19hZnRlcl9zeW5jX3ByZXBhcmVkJyxcclxuICAgICAgICAgICAgICAgIGRlZmVycmVkX2FjdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogdGhpcy5fZ2V0Um9vbUlkKCksXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIERvbid0IHBlZWsgd2hpbHN0IHJlZ2lzdGVyaW5nIG90aGVyd2lzZSBnZXRQZW5kaW5nRXZlbnRMaXN0IGNvbXBsYWluc1xyXG4gICAgICAgICAgICAvLyBEbyB0aGlzIGJ5IGluZGljYXRpbmcgb3VyIGludGVudGlvbiB0byBqb2luXHJcblxyXG4gICAgICAgICAgICAvLyBYWFg6IElMQUcgaXMgZGlzYWJsZWQgZm9yIG5vdyxcclxuICAgICAgICAgICAgLy8gc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzgyMjJcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdyZXF1aXJlX3JlZ2lzdHJhdGlvbid9KTtcclxuICAgICAgICAgICAgLy8gZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgLy8gICAgIGFjdGlvbjogJ3dpbGxfam9pbicsXHJcbiAgICAgICAgICAgIC8vIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gY29uc3QgU2V0TXhJZERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuU2V0TXhJZERpYWxvZycpO1xyXG4gICAgICAgICAgICAvLyBjb25zdCBjbG9zZSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NldCBNWElEJywgJycsIFNldE14SWREaWFsb2csIHtcclxuICAgICAgICAgICAgLy8gICAgIGhvbWVzZXJ2ZXJVcmw6IGNsaS5nZXRIb21lc2VydmVyVXJsKCksXHJcbiAgICAgICAgICAgIC8vICAgICBvbkZpbmlzaGVkOiAoc3VibWl0dGVkLCBjcmVkZW50aWFscykgPT4ge1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIGlmIChzdWJtaXR0ZWQpIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5wcm9wcy5vblJlZ2lzdGVyZWQoY3JlZGVudGlhbHMpO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgICAgICBhY3Rpb246ICdjYW5jZWxfYWZ0ZXJfc3luY19wcmVwYXJlZCcsXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICAgICAgYWN0aW9uOiAnY2FuY2VsX2pvaW4nLFxyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vICAgICB9LFxyXG4gICAgICAgICAgICAvLyAgICAgb25EaWZmZXJlbnRTZXJ2ZXJDbGlja2VkOiAoZXYpID0+IHtcclxuICAgICAgICAgICAgLy8gICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3N0YXJ0X3JlZ2lzdHJhdGlvbid9KTtcclxuICAgICAgICAgICAgLy8gICAgICAgICBjbG9zZSgpO1xyXG4gICAgICAgICAgICAvLyAgICAgfSxcclxuICAgICAgICAgICAgLy8gICAgIG9uTG9naW5DbGljazogKGV2KSA9PiB7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdzdGFydF9sb2dpbid9KTtcclxuICAgICAgICAgICAgLy8gICAgICAgICBjbG9zZSgpO1xyXG4gICAgICAgICAgICAvLyAgICAgfSxcclxuICAgICAgICAgICAgLy8gfSkuY2xvc2U7XHJcbiAgICAgICAgICAgIC8vIHJldHVybjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBQcm9taXNlLnJlc29sdmUoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNpZ25VcmwgPSB0aGlzLnByb3BzLnRoaXJkUGFydHlJbnZpdGUgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMudGhpcmRQYXJ0eUludml0ZS5pbnZpdGVTaWduVXJsIDogdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdqb2luX3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgIG9wdHM6IHsgaW52aXRlU2lnblVybDogc2lnblVybCwgdmlhU2VydmVyczogdGhpcy5wcm9wcy52aWFTZXJ2ZXJzIH0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH0sXHJcblxyXG4gICAgb25NZXNzYWdlTGlzdFNjcm9sbDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBpZiAodGhpcy5fbWVzc2FnZVBhbmVsLmlzQXRFbmRPZkxpdmVUaW1lbGluZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbnVtVW5yZWFkTWVzc2FnZXM6IDAsXHJcbiAgICAgICAgICAgICAgICBhdEVuZE9mTGl2ZVRpbWVsaW5lOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGF0RW5kT2ZMaXZlVGltZWxpbmU6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlVG9wVW5yZWFkTWVzc2FnZXNCYXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25EcmFnT3ZlcjogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBldi5kYXRhVHJhbnNmZXIuZHJvcEVmZmVjdCA9ICdub25lJztcclxuXHJcbiAgICAgICAgY29uc3QgaXRlbXMgPSBbLi4uZXYuZGF0YVRyYW5zZmVyLml0ZW1zXTtcclxuICAgICAgICBpZiAoaXRlbXMubGVuZ3RoID49IDEpIHtcclxuICAgICAgICAgICAgY29uc3QgaXNEcmFnZ2luZ0ZpbGVzID0gaXRlbXMuZXZlcnkoZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0ua2luZCA9PSAnZmlsZSc7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKGlzRHJhZ2dpbmdGaWxlcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGRyYWdnaW5nRmlsZTogdHJ1ZSB9KTtcclxuICAgICAgICAgICAgICAgIGV2LmRhdGFUcmFuc2Zlci5kcm9wRWZmZWN0ID0gJ2NvcHknO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbkRyb3A6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5zZW5kQ29udGVudExpc3RUb1Jvb20oXHJcbiAgICAgICAgICAgIGV2LmRhdGFUcmFuc2Zlci5maWxlcywgdGhpcy5zdGF0ZS5yb29tLnJvb21JZCwgdGhpcy5jb250ZXh0LFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGRyYWdnaW5nRmlsZTogZmFsc2UgfSk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdmb2N1c19jb21wb3Nlcid9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25EcmFnTGVhdmVPckVuZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBkcmFnZ2luZ0ZpbGU6IGZhbHNlIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBpbmplY3RTdGlja2VyOiBmdW5jdGlvbih1cmwsIGluZm8sIHRleHQpIHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZXh0LmlzR3Vlc3QoKSkge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJ30pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5zZW5kU3RpY2tlckNvbnRlbnRUb1Jvb20odXJsLCB0aGlzLnN0YXRlLnJvb20ucm9vbUlkLCBpbmZvLCB0ZXh0LCB0aGlzLmNvbnRleHQpXHJcbiAgICAgICAgICAgIC50aGVuKHVuZGVmaW5lZCwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IubmFtZSA9PT0gXCJVbmtub3duRGV2aWNlRXJyb3JcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIExldCB0aGUgc3RhdXMgYmFyIGhhbmRsZSB0aGlzXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TZWFyY2g6IGZ1bmN0aW9uKHRlcm0sIHNjb3BlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlYXJjaFRlcm06IHRlcm0sXHJcbiAgICAgICAgICAgIHNlYXJjaFNjb3BlOiBzY29wZSxcclxuICAgICAgICAgICAgc2VhcmNoUmVzdWx0czoge30sXHJcbiAgICAgICAgICAgIHNlYXJjaEhpZ2hsaWdodHM6IFtdLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBpZiB3ZSBhbHJlYWR5IGhhdmUgYSBzZWFyY2ggcGFuZWwsIHdlIG5lZWQgdG8gdGVsbCBpdCB0byBmb3JnZXRcclxuICAgICAgICAvLyBhYm91dCBpdHMgc2Nyb2xsIHN0YXRlLlxyXG4gICAgICAgIGlmICh0aGlzLl9zZWFyY2hSZXN1bHRzUGFuZWwuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zZWFyY2hSZXN1bHRzUGFuZWwuY3VycmVudC5yZXNldFNjcm9sbFN0YXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBtYWtlIHN1cmUgdGhhdCB3ZSBkb24ndCBlbmQgdXAgc2hvd2luZyByZXN1bHRzIGZyb21cclxuICAgICAgICAvLyBhbiBhYm9ydGVkIHNlYXJjaCBieSBrZWVwaW5nIGEgdW5pcXVlIGlkLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gdG9kbzogc2hvdWxkIGNhbmNlbCBhbnkgcHJldmlvdXMgc2VhcmNoIHJlcXVlc3RzLlxyXG4gICAgICAgIHRoaXMuc2VhcmNoSWQgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcclxuXHJcbiAgICAgICAgbGV0IHJvb21JZDtcclxuICAgICAgICBpZiAoc2NvcGUgPT09IFwiUm9vbVwiKSByb29tSWQgPSB0aGlzLnN0YXRlLnJvb20ucm9vbUlkO1xyXG5cclxuICAgICAgICBkZWJ1Z2xvZyhcInNlbmRpbmcgc2VhcmNoIHJlcXVlc3RcIik7XHJcbiAgICAgICAgY29uc3Qgc2VhcmNoUHJvbWlzZSA9IGV2ZW50U2VhcmNoKHRlcm0sIHJvb21JZCk7XHJcbiAgICAgICAgdGhpcy5faGFuZGxlU2VhcmNoUmVzdWx0KHNlYXJjaFByb21pc2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaGFuZGxlU2VhcmNoUmVzdWx0OiBmdW5jdGlvbihzZWFyY2hQcm9taXNlKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIC8vIGtlZXAgYSByZWNvcmQgb2YgdGhlIGN1cnJlbnQgc2VhcmNoIGlkLCBzbyB0aGF0IGlmIHRoZSBzZWFyY2ggdGVybXNcclxuICAgICAgICAvLyBjaGFuZ2UgYmVmb3JlIHdlIGdldCBhIHJlc3BvbnNlLCB3ZSBjYW4gaWdub3JlIHRoZSByZXN1bHRzLlxyXG4gICAgICAgIGNvbnN0IGxvY2FsU2VhcmNoSWQgPSB0aGlzLnNlYXJjaElkO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2VhcmNoSW5Qcm9ncmVzczogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHNlYXJjaFByb21pc2UudGhlbihmdW5jdGlvbihyZXN1bHRzKSB7XHJcbiAgICAgICAgICAgIGRlYnVnbG9nKFwic2VhcmNoIGNvbXBsZXRlXCIpO1xyXG4gICAgICAgICAgICBpZiAoc2VsZi51bm1vdW50ZWQgfHwgIXNlbGYuc3RhdGUuc2VhcmNoaW5nIHx8IHNlbGYuc2VhcmNoSWQgIT0gbG9jYWxTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkRpc2NhcmRpbmcgc3RhbGUgc2VhcmNoIHJlc3VsdHNcIik7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHBvc3RncmVzIG9uIHN5bmFwc2UgcmV0dXJucyB1cyBwcmVjaXNlIGRldGFpbHMgb2YgdGhlIHN0cmluZ3NcclxuICAgICAgICAgICAgLy8gd2hpY2ggYWN0dWFsbHkgZ290IG1hdGNoZWQgZm9yIGhpZ2hsaWdodGluZy5cclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gSW4gZWl0aGVyIGNhc2UsIHdlIHdhbnQgdG8gaGlnaGxpZ2h0IHRoZSBsaXRlcmFsIHNlYXJjaCB0ZXJtXHJcbiAgICAgICAgICAgIC8vIHdoZXRoZXIgaXQgd2FzIHVzZWQgYnkgdGhlIHNlYXJjaCBlbmdpbmUgb3Igbm90LlxyXG5cclxuICAgICAgICAgICAgbGV0IGhpZ2hsaWdodHMgPSByZXN1bHRzLmhpZ2hsaWdodHM7XHJcbiAgICAgICAgICAgIGlmIChoaWdobGlnaHRzLmluZGV4T2Yoc2VsZi5zdGF0ZS5zZWFyY2hUZXJtKSA8IDApIHtcclxuICAgICAgICAgICAgICAgIGhpZ2hsaWdodHMgPSBoaWdobGlnaHRzLmNvbmNhdChzZWxmLnN0YXRlLnNlYXJjaFRlcm0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBGb3Igb3ZlcmxhcHBpbmcgaGlnaGxpZ2h0cyxcclxuICAgICAgICAgICAgLy8gZmF2b3VyIGxvbmdlciAobW9yZSBzcGVjaWZpYykgdGVybXMgZmlyc3RcclxuICAgICAgICAgICAgaGlnaGxpZ2h0cyA9IGhpZ2hsaWdodHMuc29ydChmdW5jdGlvbihhLCBiKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYi5sZW5ndGggLSBhLmxlbmd0aDtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaEhpZ2hsaWdodHM6IGhpZ2hsaWdodHMsXHJcbiAgICAgICAgICAgICAgICBzZWFyY2hSZXN1bHRzOiByZXN1bHRzLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnJvcikge1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiU2VhcmNoIGZhaWxlZDogXCIgKyBlcnJvcik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NlYXJjaCBmYWlsZWQnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlNlYXJjaCBmYWlsZWRcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnJvciAmJiBlcnJvci5tZXNzYWdlKSA/IGVycm9yLm1lc3NhZ2UgOiBfdChcIlNlcnZlciBtYXkgYmUgdW5hdmFpbGFibGUsIG92ZXJsb2FkZWQsIG9yIHNlYXJjaCB0aW1lZCBvdXQgOihcIikpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KS5maW5hbGx5KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaEluUHJvZ3Jlc3M6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0U2VhcmNoUmVzdWx0VGlsZXM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFNlYXJjaFJlc3VsdFRpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5TZWFyY2hSZXN1bHRUaWxlJyk7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG5cclxuICAgICAgICAvLyBYWFg6IHRvZG86IG1lcmdlIG92ZXJsYXBwaW5nIHJlc3VsdHMgc29tZWhvdz9cclxuICAgICAgICAvLyBYWFg6IHdoeSBkb2Vzbid0IHNlYXJjaGluZyBvbiBuYW1lIHdvcms/XHJcblxyXG4gICAgICAgIGNvbnN0IHJldCA9IFtdO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5zZWFyY2hJblByb2dyZXNzKSB7XHJcbiAgICAgICAgICAgIHJldC5wdXNoKDxsaSBrZXk9XCJzZWFyY2gtc3Bpbm5lclwiPlxyXG4gICAgICAgICAgICAgICAgIDxTcGlubmVyIC8+XHJcbiAgICAgICAgICAgICA8L2xpPik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2VhcmNoUmVzdWx0cy5uZXh0X2JhdGNoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMucmVzdWx0cy5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0LnB1c2goPGxpIGtleT1cInNlYXJjaC10b3AtbWFya2VyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgIDxoMiBjbGFzc05hbWU9XCJteF9Sb29tVmlld190b3BNYXJrZXJcIj57IF90KFwiTm8gcmVzdWx0c1wiKSB9PC9oMj5cclxuICAgICAgICAgICAgICAgICA8L2xpPixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXQucHVzaCg8bGkga2V5PVwic2VhcmNoLXRvcC1tYXJrZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgPGgyIGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X3RvcE1hcmtlclwiPnsgX3QoXCJObyBtb3JlIHJlc3VsdHNcIikgfTwvaDI+XHJcbiAgICAgICAgICAgICAgICAgPC9saT4sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBvbmNlIGR5bmFtaWMgY29udGVudCBpbiB0aGUgc2VhcmNoIHJlc3VsdHMgbG9hZCwgbWFrZSB0aGUgc2Nyb2xsUGFuZWwgY2hlY2tcclxuICAgICAgICAvLyB0aGUgc2Nyb2xsIG9mZnNldHMuXHJcbiAgICAgICAgY29uc3Qgb25IZWlnaHRDaGFuZ2VkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxQYW5lbCA9IHRoaXMuX3NlYXJjaFJlc3VsdHNQYW5lbC5jdXJyZW50O1xyXG4gICAgICAgICAgICBpZiAoc2Nyb2xsUGFuZWwpIHtcclxuICAgICAgICAgICAgICAgIHNjcm9sbFBhbmVsLmNoZWNrU2Nyb2xsKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBsZXQgbGFzdFJvb21JZDtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuc3RhdGUuc2VhcmNoUmVzdWx0cy5yZXN1bHRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuc3RhdGUuc2VhcmNoUmVzdWx0cy5yZXN1bHRzW2ldO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbXhFdiA9IHJlc3VsdC5jb250ZXh0LmdldEV2ZW50KCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21JZCA9IG14RXYuZ2V0Um9vbUlkKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb20gPSB0aGlzLmNvbnRleHQuZ2V0Um9vbShyb29tSWQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFoYXZlVGlsZUZvckV2ZW50KG14RXYpKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBYWFg6IGNhbiB0aGlzIGV2ZXIgaGFwcGVuPyBJdCB3aWxsIG1ha2UgdGhlIHJlc3VsdCBjb3VudFxyXG4gICAgICAgICAgICAgICAgLy8gbm90IG1hdGNoIHRoZSBkaXNwbGF5ZWQgY291bnQuXHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2VhcmNoU2NvcGUgPT09ICdBbGwnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocm9vbUlkICE9IGxhc3RSb29tSWQpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gWFhYOiBpZiB3ZSd2ZSBsZWZ0IHRoZSByb29tLCB3ZSBtaWdodCBub3Qga25vdyBhYm91dFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGl0LiBXZSBzaG91bGQgdGVsbCB0aGUganMgc2RrIHRvIGdvIGFuZCBmaW5kIG91dCBhYm91dFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGl0LiBCdXQgdGhhdCdzIG5vdCBhbiBpc3N1ZSBjdXJyZW50bHksIGFzIHN5bmFwc2Ugb25seVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJldHVybnMgcmVzdWx0cyBmb3Igcm9vbXMgd2UncmUgam9pbmVkIHRvLlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvb21OYW1lID0gcm9vbSA/IHJvb20ubmFtZSA6IF90KFwiVW5rbm93biByb29tICUocm9vbUlkKXNcIiwgeyByb29tSWQ6IHJvb21JZCB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0LnB1c2goPGxpIGtleT17bXhFdi5nZXRJZCgpICsgXCItcm9vbVwifT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGgyPnsgX3QoXCJSb29tXCIpIH06IHsgcm9vbU5hbWUgfTwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT4pO1xyXG4gICAgICAgICAgICAgICAgICAgIGxhc3RSb29tSWQgPSByb29tSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdExpbmsgPSBcIiMvcm9vbS9cIityb29tSWQrXCIvXCIrbXhFdi5nZXRJZCgpO1xyXG5cclxuICAgICAgICAgICAgcmV0LnB1c2goPFNlYXJjaFJlc3VsdFRpbGUga2V5PXtteEV2LmdldElkKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgIHNlYXJjaFJlc3VsdD17cmVzdWx0fVxyXG4gICAgICAgICAgICAgICAgICAgICBzZWFyY2hIaWdobGlnaHRzPXt0aGlzLnN0YXRlLnNlYXJjaEhpZ2hsaWdodHN9XHJcbiAgICAgICAgICAgICAgICAgICAgIHJlc3VsdExpbms9e3Jlc3VsdExpbmt9XHJcbiAgICAgICAgICAgICAgICAgICAgIHBlcm1hbGlua0NyZWF0b3I9e3RoaXMuX2dldFBlcm1hbGlua0NyZWF0b3JGb3JSb29tKHJvb20pfVxyXG4gICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9e29uSGVpZ2h0Q2hhbmdlZH0gLz4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgfSxcclxuXHJcbiAgICBvblBpbm5lZENsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBub3dTaG93aW5nUGlubmVkID0gIXRoaXMuc3RhdGUuc2hvd2luZ1Bpbm5lZDtcclxuICAgICAgICBjb25zdCByb29tSWQgPSB0aGlzLnN0YXRlLnJvb20ucm9vbUlkO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dpbmdQaW5uZWQ6IG5vd1Nob3dpbmdQaW5uZWQsIHNlYXJjaGluZzogZmFsc2V9KTtcclxuICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiUGlubmVkRXZlbnRzLmlzT3BlblwiLCByb29tSWQsIFNldHRpbmdMZXZlbC5ST09NX0RFVklDRSwgbm93U2hvd2luZ1Bpbm5lZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU2V0dGluZ3NDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAnb3Blbl9yb29tX3NldHRpbmdzJyB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DYW5jZWxDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJ1cGRhdGVUaW50IGZyb20gb25DYW5jZWxDbGlja1wiKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVRpbnQoKTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5mb3J3YXJkaW5nRXZlbnQpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2ZvcndhcmRfZXZlbnQnLFxyXG4gICAgICAgICAgICAgICAgZXZlbnQ6IG51bGwsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2ZvY3VzX2NvbXBvc2VyJ30pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkxlYXZlQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ2xlYXZlX3Jvb20nLFxyXG4gICAgICAgICAgICByb29tX2lkOiB0aGlzLnN0YXRlLnJvb20ucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkZvcmdldENsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLmNvbnRleHQuZm9yZ2V0KHRoaXMuc3RhdGUucm9vbS5yb29tSWQpLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ3ZpZXdfbmV4dF9yb29tJyB9KTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgY29uc3QgZXJyQ29kZSA9IGVyci5lcnJjb2RlIHx8IF90KFwidW5rbm93biBlcnJvciBjb2RlXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gZm9yZ2V0IHJvb20nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFwiRmFpbGVkIHRvIGZvcmdldCByb29tICUoZXJyQ29kZSlzXCIsIHsgZXJyQ29kZTogZXJyQ29kZSB9KSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVqZWN0QnV0dG9uQ2xpY2tlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcmVqZWN0aW5nOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5sZWF2ZSh0aGlzLnN0YXRlLnJvb21JZCkudGhlbihmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAndmlld19uZXh0X3Jvb20nIH0pO1xyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHJlamVjdGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gcmVqZWN0IGludml0ZTogJXNcIiwgZXJyb3IpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbXNnID0gZXJyb3IubWVzc2FnZSA/IGVycm9yLm1lc3NhZ2UgOiBKU09OLnN0cmluZ2lmeShlcnJvcik7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byByZWplY3QgaW52aXRlJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJGYWlsZWQgdG8gcmVqZWN0IGludml0ZVwiKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBtc2csXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgc2VsZi5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICByZWplY3Rpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcmVqZWN0RXJyb3I6IGVycm9yLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25SZWplY3RBbmRJZ25vcmVDbGljazogYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJlamVjdGluZzogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgbXlNZW1iZXIgPSB0aGlzLnN0YXRlLnJvb20uZ2V0TWVtYmVyKHRoaXMuY29udGV4dC5nZXRVc2VySWQoKSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGludml0ZUV2ZW50ID0gbXlNZW1iZXIuZXZlbnRzLm1lbWJlcjtcclxuICAgICAgICAgICAgY29uc3QgaWdub3JlZFVzZXJzID0gdGhpcy5jb250ZXh0LmdldElnbm9yZWRVc2VycygpO1xyXG4gICAgICAgICAgICBpZ25vcmVkVXNlcnMucHVzaChpbnZpdGVFdmVudC5nZXRTZW5kZXIoKSk7IC8vIGRlLWR1cGVkIGludGVybmFsbHkgaW4gdGhlIGpzLXNka1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNvbnRleHQuc2V0SWdub3JlZFVzZXJzKGlnbm9yZWRVc2Vycyk7XHJcblxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNvbnRleHQubGVhdmUodGhpcy5zdGF0ZS5yb29tSWQpO1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICd2aWV3X25leHRfcm9vbScgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byByZWplY3QgaW52aXRlOiAlc1wiLCBlcnJvcik7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBtc2cgPSBlcnJvci5tZXNzYWdlID8gZXJyb3IubWVzc2FnZSA6IEpTT04uc3RyaW5naWZ5KGVycm9yKTtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIHJlamVjdCBpbnZpdGUnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkZhaWxlZCB0byByZWplY3QgaW52aXRlXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG1zZyxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHJlamVjdGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByZWplY3RFcnJvcjogZXJyb3IsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25SZWplY3RUaHJlZXBpZEludml0ZUJ1dHRvbkNsaWNrZWQ6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgLy8gV2UgY2FuIHJlamVjdCAzcGlkIGludml0ZXMgaW4gdGhlIHNhbWUgd2F5IHRoYXQgd2UgYWNjZXB0IHRoZW0sXHJcbiAgICAgICAgLy8gdXNpbmcgL2xlYXZlIHJhdGhlciB0aGFuIC9qb2luLiBJbiB0aGUgc2hvcnQgdGVybSB0aG91Z2gsIHdlXHJcbiAgICAgICAgLy8ganVzdCBpZ25vcmUgdGhlbS5cclxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3ZlY3Rvci13ZWIvaXNzdWVzLzExMzRcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb21fZGlyZWN0b3J5JyxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TZWFyY2hDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlYXJjaGluZzogIXRoaXMuc3RhdGUuc2VhcmNoaW5nLFxyXG4gICAgICAgICAgICBzaG93aW5nUGlubmVkOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DYW5jZWxTZWFyY2hDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlYXJjaGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIHNlYXJjaFJlc3VsdHM6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGp1bXAgZG93biB0byB0aGUgYm90dG9tIG9mIHRoaXMgcm9vbSwgd2hlcmUgbmV3IGV2ZW50cyBhcmUgYXJyaXZpbmdcclxuICAgIGp1bXBUb0xpdmVUaW1lbGluZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsLmp1bXBUb0xpdmVUaW1lbGluZSgpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnZm9jdXNfY29tcG9zZXInfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGp1bXAgdXAgdG8gd2hlcmV2ZXIgb3VyIHJlYWQgbWFya2VyIGlzXHJcbiAgICBqdW1wVG9SZWFkTWFya2VyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9tZXNzYWdlUGFuZWwuanVtcFRvUmVhZE1hcmtlcigpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyB1cGRhdGUgdGhlIHJlYWQgbWFya2VyIHRvIG1hdGNoIHRoZSByZWFkLXJlY2VpcHRcclxuICAgIGZvcmdldFJlYWRNYXJrZXI6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsLmZvcmdldFJlYWRNYXJrZXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gZGVjaWRlIHdoZXRoZXIgb3Igbm90IHRoZSB0b3AgJ3VucmVhZCBtZXNzYWdlcycgYmFyIHNob3VsZCBiZSBzaG93blxyXG4gICAgX3VwZGF0ZVRvcFVucmVhZE1lc3NhZ2VzQmFyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMuX21lc3NhZ2VQYW5lbCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzaG93QmFyID0gdGhpcy5fbWVzc2FnZVBhbmVsLmNhbkp1bXBUb1JlYWRNYXJrZXIoKTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5zaG93VG9wVW5yZWFkTWVzc2FnZXNCYXIgIT0gc2hvd0Jhcikge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzaG93VG9wVW5yZWFkTWVzc2FnZXNCYXI6IHNob3dCYXJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGdldCB0aGUgY3VycmVudCBzY3JvbGwgcG9zaXRpb24gb2YgdGhlIHJvb20sIHNvIHRoYXQgaXQgY2FuIGJlXHJcbiAgICAvLyByZXN0b3JlZCB3aGVuIHdlIHN3aXRjaCBiYWNrIHRvIGl0LlxyXG4gICAgLy9cclxuICAgIF9nZXRTY3JvbGxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgbWVzc2FnZVBhbmVsID0gdGhpcy5fbWVzc2FnZVBhbmVsO1xyXG4gICAgICAgIGlmICghbWVzc2FnZVBhbmVsKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gaWYgd2UncmUgZm9sbG93aW5nIHRoZSBsaXZlIHRpbWVsaW5lLCB3ZSB3YW50IHRvIHJldHVybiBudWxsOyB0aGF0XHJcbiAgICAgICAgLy8gbWVhbnMgdGhhdCwgaWYgd2Ugc3dpdGNoIGJhY2ssIHdlIHdpbGwganVtcCB0byB0aGUgcmVhZC11cC10byBtYXJrLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gVGhhdCBzaG91bGQgYmUgbW9yZSBpbnR1aXRpdmUgdGhhbiBzbGF2aXNobHkgcHJlc2VydmluZyB0aGUgY3VycmVudFxyXG4gICAgICAgIC8vIHNjcm9sbCBzdGF0ZSwgaW4gdGhlIGNhc2Ugd2hlcmUgdGhlIHJvb20gYWR2YW5jZXMgaW4gdGhlIG1lYW50aW1lXHJcbiAgICAgICAgLy8gKHBhcnRpY3VsYXJseSBpbiB0aGUgY2FzZSB0aGF0IHRoZSB1c2VyIHJlYWRzIHNvbWUgc3R1ZmYgb24gYW5vdGhlclxyXG4gICAgICAgIC8vIGRldmljZSkuXHJcbiAgICAgICAgLy9cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5hdEVuZE9mTGl2ZVRpbWVsaW5lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2Nyb2xsU3RhdGUgPSBtZXNzYWdlUGFuZWwuZ2V0U2Nyb2xsU3RhdGUoKTtcclxuXHJcbiAgICAgICAgLy8gZ2V0U2Nyb2xsU3RhdGUgb24gVGltZWxpbmVQYW5lbCAqbWF5KiByZXR1cm4gbnVsbCwgc28gZ3VhcmQgYWdhaW5zdCB0aGF0XHJcbiAgICAgICAgaWYgKCFzY3JvbGxTdGF0ZSB8fCBzY3JvbGxTdGF0ZS5zdHVja0F0Qm90dG9tKSB7XHJcbiAgICAgICAgICAgIC8vIHdlIGRvbid0IHJlYWxseSBleHBlY3QgdG8gYmUgaW4gdGhpcyBzdGF0ZSwgYnV0IGl0IHdpbGxcclxuICAgICAgICAgICAgLy8gb2NjYXNpb25hbGx5IGhhcHBlbiB3aGVuIG5vIHNjcm9sbCBzdGF0ZSBoYXMgYmVlbiBzZXQgb24gdGhlXHJcbiAgICAgICAgICAgIC8vIG1lc3NhZ2VQYW5lbCAoaWUsIHdlIGRpZG4ndCBoYXZlIGFuIGluaXRpYWwgZXZlbnQgKHNvIGl0J3NcclxuICAgICAgICAgICAgLy8gcHJvYmFibHkgYSBuZXcgcm9vbSksIHRoZXJlIGhhcyBiZWVuIG5vIHVzZXItaW5pdGlhdGVkIHNjcm9sbCwgYW5kXHJcbiAgICAgICAgICAgIC8vIG5vIHJlYWQtcmVjZWlwdHMgaGF2ZSBhcnJpdmVkIHRvIHVwZGF0ZSB0aGUgc2Nyb2xsIHBvc2l0aW9uKS5cclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gUmV0dXJuIG51bGwsIHdoaWNoIHdpbGwgY2F1c2UgdXMgdG8gc2Nyb2xsIHRvIGxhc3QgdW5yZWFkIG9uXHJcbiAgICAgICAgICAgIC8vIHJlbG9hZC5cclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBmb2N1c3NlZEV2ZW50OiBzY3JvbGxTdGF0ZS50cmFja2VkU2Nyb2xsVG9rZW4sXHJcbiAgICAgICAgICAgIHBpeGVsT2Zmc2V0OiBzY3JvbGxTdGF0ZS5waXhlbE9mZnNldCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJlc2l6ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gSXQgc2VlbXMgZmxleGJveCBkb2Vzbid0IGdpdmUgdXMgYSB3YXkgdG8gY29uc3RyYWluIHRoZSBhdXhQYW5lbCBoZWlnaHQgdG8gaGF2ZVxyXG4gICAgICAgIC8vIGEgbWluaW11bSBvZiB0aGUgaGVpZ2h0IG9mIHRoZSB2aWRlbyBlbGVtZW50LCB3aGlsc3QgYWxzbyBjYXBwaW5nIGl0IGZyb20gcHVzaGluZyBvdXQgdGhlIHBhZ2VcclxuICAgICAgICAvLyBzbyB3ZSBoYXZlIHRvIGRvIGl0IHZpYSBKUyBpbnN0ZWFkLiAgSW4gdGhpcyBpbXBsZW1lbnRhdGlvbiB3ZSBjYXAgdGhlIGhlaWdodCBieSBwdXR0aW5nXHJcbiAgICAgICAgLy8gYSBtYXhIZWlnaHQgb24gdGhlIHVuZGVybHlpbmcgcmVtb3RlIHZpZGVvIHRhZy5cclxuXHJcbiAgICAgICAgLy8gaGVhZGVyICsgZm9vdGVyICsgc3RhdHVzICsgZ2l2ZSB1cyBhdCBsZWFzdCAxMjBweCBvZiBzY3JvbGxiYWNrIGF0IGFsbCB0aW1lcy5cclxuICAgICAgICBsZXQgYXV4UGFuZWxNYXhIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQgLVxyXG4gICAgICAgICAgICAgICAgKDgzICsgLy8gaGVpZ2h0IG9mIFJvb21IZWFkZXJcclxuICAgICAgICAgICAgICAgICAzNiArIC8vIGhlaWdodCBvZiB0aGUgc3RhdHVzIGFyZWFcclxuICAgICAgICAgICAgICAgICA3MiArIC8vIG1pbmltdW0gaGVpZ2h0IG9mIHRoZSBtZXNzYWdlIGNvbXBtb3NlclxyXG4gICAgICAgICAgICAgICAgIDEyMCk7IC8vIGFtb3VudCBvZiBkZXNpcmVkIHNjcm9sbGJhY2tcclxuXHJcbiAgICAgICAgLy8gWFhYOiB0aGlzIGlzIGEgYml0IG9mIGEgaGFjayBhbmQgbWlnaHQgcG9zc2libHkgY2F1c2UgdGhlIHZpZGVvIHRvIHB1c2ggb3V0IHRoZSBwYWdlIGFueXdheVxyXG4gICAgICAgIC8vIGJ1dCBpdCdzIGJldHRlciB0aGFuIHRoZSB2aWRlbyBnb2luZyBtaXNzaW5nIGVudGlyZWx5XHJcbiAgICAgICAgaWYgKGF1eFBhbmVsTWF4SGVpZ2h0IDwgNTApIGF1eFBhbmVsTWF4SGVpZ2h0ID0gNTA7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2F1eFBhbmVsTWF4SGVpZ2h0OiBhdXhQYW5lbE1heEhlaWdodH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkZ1bGxzY3JlZW5DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlkZW9fZnVsbHNjcmVlbicsXHJcbiAgICAgICAgICAgIGZ1bGxzY3JlZW46IHRydWUsXHJcbiAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTXV0ZUF1ZGlvQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNhbGwgPSB0aGlzLl9nZXRDYWxsRm9yUm9vbSgpO1xyXG4gICAgICAgIGlmICghY2FsbCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0gIWNhbGwuaXNNaWNyb3Bob25lTXV0ZWQoKTtcclxuICAgICAgICBjYWxsLnNldE1pY3JvcGhvbmVNdXRlZChuZXdTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpOyAvLyBUT0RPOiBqdXN0IHVwZGF0ZSB0aGUgdm9pcCBidXR0b25zXHJcbiAgICB9LFxyXG5cclxuICAgIG9uTXV0ZVZpZGVvQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNhbGwgPSB0aGlzLl9nZXRDYWxsRm9yUm9vbSgpO1xyXG4gICAgICAgIGlmICghY2FsbCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0gIWNhbGwuaXNMb2NhbFZpZGVvTXV0ZWQoKTtcclxuICAgICAgICBjYWxsLnNldExvY2FsVmlkZW9NdXRlZChuZXdTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpOyAvLyBUT0RPOiBqdXN0IHVwZGF0ZSB0aGUgdm9pcCBidXR0b25zXHJcbiAgICB9LFxyXG5cclxuICAgIG9uU3RhdHVzQmFyVmlzaWJsZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHN0YXR1c0JhclZpc2libGU6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU3RhdHVzQmFySGlkZGVuOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBUaGlzIGlzIGN1cnJlbnRseSBub3QgZGVzaXJlZCBhcyBpdCBpcyBhbm5veWluZyBpZiBpdCBrZWVwcyBleHBhbmRpbmcgYW5kIGNvbGxhcHNpbmdcclxuICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHJldHVybjtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc3RhdHVzQmFyVmlzaWJsZTogZmFsc2UsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogY2FsbGVkIGJ5IHRoZSBwYXJlbnQgY29tcG9uZW50IHdoZW4gUGFnZVVwL0Rvd24vZXRjIGlzIHByZXNzZWQuXHJcbiAgICAgKlxyXG4gICAgICogV2UgcGFzcyBpdCBkb3duIHRvIHRoZSBzY3JvbGwgcGFuZWwuXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZVNjcm9sbEtleTogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBsZXQgcGFuZWw7XHJcbiAgICAgICAgaWYgKHRoaXMuX3NlYXJjaFJlc3VsdHNQYW5lbC5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIHBhbmVsID0gdGhpcy5fc2VhcmNoUmVzdWx0c1BhbmVsLmN1cnJlbnQ7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9tZXNzYWdlUGFuZWwpIHtcclxuICAgICAgICAgICAgcGFuZWwgPSB0aGlzLl9tZXNzYWdlUGFuZWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAocGFuZWwpIHtcclxuICAgICAgICAgICAgcGFuZWwuaGFuZGxlU2Nyb2xsS2V5KGV2KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogZ2V0IGFueSBjdXJyZW50IGNhbGwgZm9yIHRoaXMgcm9vbVxyXG4gICAgICovXHJcbiAgICBfZ2V0Q2FsbEZvclJvb206IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5yb29tKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gQ2FsbEhhbmRsZXIuZ2V0Q2FsbEZvclJvb20odGhpcy5zdGF0ZS5yb29tLnJvb21JZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHRoaXMgaGFzIHRvIGJlIGEgcHJvcGVyIG1ldGhvZCByYXRoZXIgdGhhbiBhbiB1bm5hbWVkIGZ1bmN0aW9uLFxyXG4gICAgLy8gb3RoZXJ3aXNlIHJlYWN0IGNhbGxzIGl0IHdpdGggbnVsbCBvbiBlYWNoIHVwZGF0ZS5cclxuICAgIF9nYXRoZXJUaW1lbGluZVBhbmVsUmVmOiBmdW5jdGlvbihyKSB7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZVBhbmVsID0gcjtcclxuICAgICAgICBpZiAocikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInVwZGF0ZVRpbnQgZnJvbSBSb29tVmlldy5fZ2F0aGVyVGltZWxpbmVQYW5lbFJlZlwiKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVUaW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0T2xkUm9vbTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgY3JlYXRlRXZlbnQgPSB0aGlzLnN0YXRlLnJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLmNyZWF0ZVwiLCBcIlwiKTtcclxuICAgICAgICBpZiAoIWNyZWF0ZUV2ZW50IHx8ICFjcmVhdGVFdmVudC5nZXRDb250ZW50KClbJ3ByZWRlY2Vzc29yJ10pIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5jb250ZXh0LmdldFJvb20oY3JlYXRlRXZlbnQuZ2V0Q29udGVudCgpWydwcmVkZWNlc3NvciddWydyb29tX2lkJ10pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0SGlkZGVuSGlnaGxpZ2h0Q291bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IG9sZFJvb20gPSB0aGlzLl9nZXRPbGRSb29tKCk7XHJcbiAgICAgICAgaWYgKCFvbGRSb29tKSByZXR1cm4gMDtcclxuICAgICAgICByZXR1cm4gb2xkUm9vbS5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCgnaGlnaGxpZ2h0Jyk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkhpZGRlbkhpZ2hsaWdodHNDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgb2xkUm9vbSA9IHRoaXMuX2dldE9sZFJvb20oKTtcclxuICAgICAgICBpZiAoIW9sZFJvb20pIHJldHVybjtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogXCJ2aWV3X3Jvb21cIiwgcm9vbV9pZDogb2xkUm9vbS5yb29tSWR9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBSb29tSGVhZGVyID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuUm9vbUhlYWRlcicpO1xyXG4gICAgICAgIGNvbnN0IEZvcndhcmRNZXNzYWdlID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLkZvcndhcmRNZXNzYWdlXCIpO1xyXG4gICAgICAgIGNvbnN0IEF1eFBhbmVsID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLkF1eFBhbmVsXCIpO1xyXG4gICAgICAgIGNvbnN0IFNlYXJjaEJhciA9IHNkay5nZXRDb21wb25lbnQoXCJyb29tcy5TZWFyY2hCYXJcIik7XHJcbiAgICAgICAgY29uc3QgUGlubmVkRXZlbnRzUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuUGlubmVkRXZlbnRzUGFuZWxcIik7XHJcbiAgICAgICAgY29uc3QgU2Nyb2xsUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KFwic3RydWN0dXJlcy5TY3JvbGxQYW5lbFwiKTtcclxuICAgICAgICBjb25zdCBUaW50YWJsZVN2ZyA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5UaW50YWJsZVN2Z1wiKTtcclxuICAgICAgICBjb25zdCBSb29tUHJldmlld0JhciA9IHNkay5nZXRDb21wb25lbnQoXCJyb29tcy5Sb29tUHJldmlld0JhclwiKTtcclxuICAgICAgICBjb25zdCBUaW1lbGluZVBhbmVsID0gc2RrLmdldENvbXBvbmVudChcInN0cnVjdHVyZXMuVGltZWxpbmVQYW5lbFwiKTtcclxuICAgICAgICBjb25zdCBSb29tVXBncmFkZVdhcm5pbmdCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuUm9vbVVwZ3JhZGVXYXJuaW5nQmFyXCIpO1xyXG4gICAgICAgIGNvbnN0IFJvb21SZWNvdmVyeVJlbWluZGVyID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLlJvb21SZWNvdmVyeVJlbWluZGVyXCIpO1xyXG4gICAgICAgIGNvbnN0IEVycm9yQm91bmRhcnkgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuRXJyb3JCb3VuZGFyeVwiKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJvb20pIHtcclxuICAgICAgICAgICAgY29uc3QgbG9hZGluZyA9IHRoaXMuc3RhdGUucm9vbUxvYWRpbmcgfHwgdGhpcy5zdGF0ZS5wZWVrTG9hZGluZztcclxuICAgICAgICAgICAgaWYgKGxvYWRpbmcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVmlld1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8RXJyb3JCb3VuZGFyeT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb29tUHJldmlld0JhclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhblByZXZpZXc9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZXZpZXdMb2FkaW5nPXt0aGlzLnN0YXRlLnBlZWtMb2FkaW5nfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yPXt0aGlzLnN0YXRlLnJvb21Mb2FkRXJyb3J9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbG9hZGluZz17bG9hZGluZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqb2luaW5nPXt0aGlzLnN0YXRlLmpvaW5pbmd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb29iRGF0YT17dGhpcy5wcm9wcy5vb2JEYXRhfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9FcnJvckJvdW5kYXJ5PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhciBpbnZpdGVyTmFtZSA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9vYkRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICBpbnZpdGVyTmFtZSA9IHRoaXMucHJvcHMub29iRGF0YS5pbnZpdGVyTmFtZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZhciBpbnZpdGVkRW1haWwgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy50aGlyZFBhcnR5SW52aXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW52aXRlZEVtYWlsID0gdGhpcy5wcm9wcy50aGlyZFBhcnR5SW52aXRlLmludml0ZWRFbWFpbDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBXZSBoYXZlIG5vIHJvb20gb2JqZWN0IGZvciB0aGlzIHJvb20sIG9ubHkgdGhlIElELlxyXG4gICAgICAgICAgICAgICAgLy8gV2UndmUgZ290IHRvIHRoaXMgcm9vbSBieSBmb2xsb3dpbmcgYSBsaW5rLCBwb3NzaWJseSBhIHRoaXJkIHBhcnR5IGludml0ZS5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb21BbGlhcyA9IHRoaXMuc3RhdGUucm9vbUFsaWFzO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxFcnJvckJvdW5kYXJ5PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJvb21QcmV2aWV3QmFyIG9uSm9pbkNsaWNrPXt0aGlzLm9uSm9pbkJ1dHRvbkNsaWNrZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25Gb3JnZXRDbGljaz17dGhpcy5vbkZvcmdldENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uUmVqZWN0Q2xpY2s9e3RoaXMub25SZWplY3RUaHJlZXBpZEludml0ZUJ1dHRvbkNsaWNrZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuUHJldmlldz17ZmFsc2V9IGVycm9yPXt0aGlzLnN0YXRlLnJvb21Mb2FkRXJyb3J9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9vbUFsaWFzPXtyb29tQWxpYXN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgam9pbmluZz17dGhpcy5zdGF0ZS5qb2luaW5nfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGludml0ZXJOYW1lPXtpbnZpdGVyTmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnZpdGVkRW1haWw9e2ludml0ZWRFbWFpbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvb2JEYXRhPXt0aGlzLnByb3BzLm9vYkRhdGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2lnblVybD17dGhpcy5wcm9wcy50aGlyZFBhcnR5SW52aXRlID8gdGhpcy5wcm9wcy50aGlyZFBhcnR5SW52aXRlLmludml0ZVNpZ25VcmwgOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvb209e3RoaXMuc3RhdGUucm9vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvRXJyb3JCb3VuZGFyeT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG15TWVtYmVyc2hpcCA9IHRoaXMuc3RhdGUucm9vbS5nZXRNeU1lbWJlcnNoaXAoKTtcclxuICAgICAgICBpZiAobXlNZW1iZXJzaGlwID09ICdpbnZpdGUnKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmpvaW5pbmcgfHwgdGhpcy5zdGF0ZS5yZWplY3RpbmcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEVycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSb29tUHJldmlld0JhclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuUHJldmlldz17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcj17dGhpcy5zdGF0ZS5yb29tTG9hZEVycm9yfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgam9pbmluZz17dGhpcy5zdGF0ZS5qb2luaW5nfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0aW5nPXt0aGlzLnN0YXRlLnJlamVjdGluZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0Vycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbXlVc2VySWQgPSB0aGlzLmNvbnRleHQuY3JlZGVudGlhbHMudXNlcklkO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbXlNZW1iZXIgPSB0aGlzLnN0YXRlLnJvb20uZ2V0TWVtYmVyKG15VXNlcklkKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGludml0ZUV2ZW50ID0gbXlNZW1iZXIuZXZlbnRzLm1lbWJlcjtcclxuICAgICAgICAgICAgICAgIHZhciBpbnZpdGVyTmFtZSA9IGludml0ZUV2ZW50LnNlbmRlciA/IGludml0ZUV2ZW50LnNlbmRlci5uYW1lIDogaW52aXRlRXZlbnQuZ2V0U2VuZGVyKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2UgZGVsaWJlcmF0ZWx5IGRvbid0IHRyeSB0byBwZWVrIGludG8gaW52aXRlcywgZXZlbiBpZiB3ZSBoYXZlIHBlcm1pc3Npb24gdG8gcGVla1xyXG4gICAgICAgICAgICAgICAgLy8gYXMgdGhleSBjb3VsZCBiZSBhIHNwYW0gdmVjdG9yLlxyXG4gICAgICAgICAgICAgICAgLy8gWFhYOiBpbiBmdXR1cmUgd2UgY291bGQgZ2l2ZSB0aGUgb3B0aW9uIG9mIGEgJ1ByZXZpZXcnIGJ1dHRvbiB3aGljaCBsZXRzIHRoZW0gdmlldyBhbnl3YXkuXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2UgaGF2ZSBhIHJlZ3VsYXIgaW52aXRlIGZvciB0aGlzIHJvb20uXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEVycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Um9vbVByZXZpZXdCYXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkpvaW5DbGljaz17dGhpcy5vbkpvaW5CdXR0b25DbGlja2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRm9yZ2V0Q2xpY2s9e3RoaXMub25Gb3JnZXRDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJlamVjdENsaWNrPXt0aGlzLm9uUmVqZWN0QnV0dG9uQ2xpY2tlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJlamVjdEFuZElnbm9yZUNsaWNrPXt0aGlzLm9uUmVqZWN0QW5kSWdub3JlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW52aXRlck5hbWU9e2ludml0ZXJOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhblByZXZpZXc9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpvaW5pbmc9e3RoaXMuc3RhdGUuam9pbmluZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnN0YXRlLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0Vycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBXZSBoYXZlIHN1Y2Nlc3NmdWxseSBsb2FkZWQgdGhpcyByb29tLCBhbmQgYXJlIG5vdCBwcmV2aWV3aW5nLlxyXG4gICAgICAgIC8vIERpc3BsYXkgdGhlIFwibm9ybWFsXCIgcm9vbSB2aWV3LlxyXG5cclxuICAgICAgICBjb25zdCBjYWxsID0gdGhpcy5fZ2V0Q2FsbEZvclJvb20oKTtcclxuICAgICAgICBsZXQgaW5DYWxsID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKGNhbGwgJiYgKHRoaXMuc3RhdGUuY2FsbFN0YXRlICE9PSAnZW5kZWQnICYmIHRoaXMuc3RhdGUuY2FsbFN0YXRlICE9PSAncmluZ2luZycpKSB7XHJcbiAgICAgICAgICAgIGluQ2FsbCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzY3JvbGxoZWFkZXJfY2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICBteF9Sb29tVmlld19zY3JvbGxoZWFkZXI6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBzdGF0dXNCYXI7XHJcbiAgICAgICAgbGV0IGlzU3RhdHVzQXJlYUV4cGFuZGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKENvbnRlbnRNZXNzYWdlcy5zaGFyZWRJbnN0YW5jZSgpLmdldEN1cnJlbnRVcGxvYWRzKCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBVcGxvYWRCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlVwbG9hZEJhcicpO1xyXG4gICAgICAgICAgICBzdGF0dXNCYXIgPSA8VXBsb2FkQmFyIHJvb209e3RoaXMuc3RhdGUucm9vbX0gLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFJvb21TdGF0dXNCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlJvb21TdGF0dXNCYXInKTtcclxuICAgICAgICAgICAgaXNTdGF0dXNBcmVhRXhwYW5kZWQgPSB0aGlzLnN0YXRlLnN0YXR1c0JhclZpc2libGU7XHJcbiAgICAgICAgICAgIHN0YXR1c0JhciA9IDxSb29tU3RhdHVzQmFyXHJcbiAgICAgICAgICAgICAgICByb29tPXt0aGlzLnN0YXRlLnJvb219XHJcbiAgICAgICAgICAgICAgICBzZW50TWVzc2FnZUFuZElzQWxvbmU9e3RoaXMuc3RhdGUuaXNBbG9uZX1cclxuICAgICAgICAgICAgICAgIGhhc0FjdGl2ZUNhbGw9e2luQ2FsbH1cclxuICAgICAgICAgICAgICAgIGlzUGVla2luZz17bXlNZW1iZXJzaGlwICE9PSBcImpvaW5cIn1cclxuICAgICAgICAgICAgICAgIG9uSW52aXRlQ2xpY2s9e3RoaXMub25JbnZpdGVCdXR0b25DbGlja31cclxuICAgICAgICAgICAgICAgIG9uU3RvcFdhcm5pbmdDbGljaz17dGhpcy5vblN0b3BBbG9uZVdhcm5pbmdDbGlja31cclxuICAgICAgICAgICAgICAgIG9uVmlzaWJsZT17dGhpcy5vblN0YXR1c0JhclZpc2libGV9XHJcbiAgICAgICAgICAgICAgICBvbkhpZGRlbj17dGhpcy5vblN0YXR1c0JhckhpZGRlbn1cclxuICAgICAgICAgICAgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByb29tVmVyc2lvblJlY29tbWVuZGF0aW9uID0gdGhpcy5zdGF0ZS51cGdyYWRlUmVjb21tZW5kYXRpb247XHJcbiAgICAgICAgY29uc3Qgc2hvd1Jvb21VcGdyYWRlQmFyID0gKFxyXG4gICAgICAgICAgICByb29tVmVyc2lvblJlY29tbWVuZGF0aW9uICYmXHJcbiAgICAgICAgICAgIHJvb21WZXJzaW9uUmVjb21tZW5kYXRpb24ubmVlZHNVcGdyYWRlICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUucm9vbS51c2VyTWF5VXBncmFkZVJvb20odGhpcy5jb250ZXh0LmNyZWRlbnRpYWxzLnVzZXJJZClcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBzaG93Um9vbVJlY292ZXJ5UmVtaW5kZXIgPSAoXHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJzaG93Um9vbVJlY292ZXJ5UmVtaW5kZXJcIikgJiZcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0LmlzUm9vbUVuY3J5cHRlZCh0aGlzLnN0YXRlLnJvb20ucm9vbUlkKSAmJlxyXG4gICAgICAgICAgICAhdGhpcy5jb250ZXh0LmdldEtleUJhY2t1cEVuYWJsZWQoKVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGNvbnN0IGhpZGRlbkhpZ2hsaWdodENvdW50ID0gdGhpcy5fZ2V0SGlkZGVuSGlnaGxpZ2h0Q291bnQoKTtcclxuXHJcbiAgICAgICAgbGV0IGF1eCA9IG51bGw7XHJcbiAgICAgICAgbGV0IHByZXZpZXdCYXI7XHJcbiAgICAgICAgbGV0IGhpZGVDYW5jZWwgPSBmYWxzZTtcclxuICAgICAgICBsZXQgZm9yY2VIaWRlUmlnaHRQYW5lbCA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmZvcndhcmRpbmdFdmVudCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBhdXggPSA8Rm9yd2FyZE1lc3NhZ2Ugb25DYW5jZWxDbGljaz17dGhpcy5vbkNhbmNlbENsaWNrfSAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuc2VhcmNoaW5nKSB7XHJcbiAgICAgICAgICAgIGhpZGVDYW5jZWwgPSB0cnVlOyAvLyBoYXMgb3duIGNhbmNlbFxyXG4gICAgICAgICAgICBhdXggPSA8U2VhcmNoQmFyIHNlYXJjaEluUHJvZ3Jlc3M9e3RoaXMuc3RhdGUuc2VhcmNoSW5Qcm9ncmVzc30gb25DYW5jZWxDbGljaz17dGhpcy5vbkNhbmNlbFNlYXJjaENsaWNrfSBvblNlYXJjaD17dGhpcy5vblNlYXJjaH0gLz47XHJcbiAgICAgICAgfSBlbHNlIGlmIChzaG93Um9vbVVwZ3JhZGVCYXIpIHtcclxuICAgICAgICAgICAgYXV4ID0gPFJvb21VcGdyYWRlV2FybmluZ0JhciByb29tPXt0aGlzLnN0YXRlLnJvb219IHJlY29tbWVuZGF0aW9uPXtyb29tVmVyc2lvblJlY29tbWVuZGF0aW9ufSAvPjtcclxuICAgICAgICAgICAgaGlkZUNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzaG93Um9vbVJlY292ZXJ5UmVtaW5kZXIpIHtcclxuICAgICAgICAgICAgYXV4ID0gPFJvb21SZWNvdmVyeVJlbWluZGVyIG9uRG9udEFza0FnYWluU2V0PXt0aGlzLm9uUm9vbVJlY292ZXJ5UmVtaW5kZXJEb250QXNrQWdhaW59IC8+O1xyXG4gICAgICAgICAgICBoaWRlQ2FuY2VsID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuc2hvd2luZ1Bpbm5lZCkge1xyXG4gICAgICAgICAgICBoaWRlQ2FuY2VsID0gdHJ1ZTsgLy8gaGFzIG93biBjYW5jZWxcclxuICAgICAgICAgICAgYXV4ID0gPFBpbm5lZEV2ZW50c1BhbmVsIHJvb209e3RoaXMuc3RhdGUucm9vbX0gb25DYW5jZWxDbGljaz17dGhpcy5vblBpbm5lZENsaWNrfSAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKG15TWVtYmVyc2hpcCAhPT0gXCJqb2luXCIpIHtcclxuICAgICAgICAgICAgLy8gV2UgZG8gaGF2ZSBhIHJvb20gb2JqZWN0IGZvciB0aGlzIHJvb20sIGJ1dCB3ZSdyZSBub3QgY3VycmVudGx5IGluIGl0LlxyXG4gICAgICAgICAgICAvLyBXZSBtYXkgaGF2ZSBhIDNyZCBwYXJ0eSBpbnZpdGUgdG8gaXQuXHJcbiAgICAgICAgICAgIHZhciBpbnZpdGVyTmFtZSA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMub29iRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgaW52aXRlck5hbWUgPSB0aGlzLnByb3BzLm9vYkRhdGEuaW52aXRlck5hbWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIGludml0ZWRFbWFpbCA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMudGhpcmRQYXJ0eUludml0ZSkge1xyXG4gICAgICAgICAgICAgICAgaW52aXRlZEVtYWlsID0gdGhpcy5wcm9wcy50aGlyZFBhcnR5SW52aXRlLmludml0ZWRFbWFpbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoaWRlQ2FuY2VsID0gdHJ1ZTtcclxuICAgICAgICAgICAgcHJldmlld0JhciA9IChcclxuICAgICAgICAgICAgICAgIDxSb29tUHJldmlld0JhciBvbkpvaW5DbGljaz17dGhpcy5vbkpvaW5CdXR0b25DbGlja2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRm9yZ2V0Q2xpY2s9e3RoaXMub25Gb3JnZXRDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJlamVjdENsaWNrPXt0aGlzLm9uUmVqZWN0VGhyZWVwaWRJbnZpdGVCdXR0b25DbGlja2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpvaW5pbmc9e3RoaXMuc3RhdGUuam9pbmluZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnZpdGVyTmFtZT17aW52aXRlck5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW52aXRlZEVtYWlsPXtpbnZpdGVkRW1haWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb29iRGF0YT17dGhpcy5wcm9wcy5vb2JEYXRhfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhblByZXZpZXc9e3RoaXMuc3RhdGUuY2FuUGVla31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnN0YXRlLnJvb219XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY2FuUGVlaykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgcHJldmlld0JhciB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9yY2VIaWRlUmlnaHRQYW5lbCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKGhpZGRlbkhpZ2hsaWdodENvdW50ID4gMCkge1xyXG4gICAgICAgICAgICBhdXggPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBlbGVtZW50PVwiZGl2XCIgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfYXV4UGFuZWxfaGlkZGVuSGlnaGxpZ2h0c1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkhpZGRlbkhpZ2hsaWdodHNDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIllvdSBoYXZlICUoY291bnQpcyB1bnJlYWQgbm90aWZpY2F0aW9ucyBpbiBhIHByaW9yIHZlcnNpb24gb2YgdGhpcyByb29tLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Y291bnQ6IGhpZGRlbkhpZ2hsaWdodENvdW50fSxcclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgYXV4UGFuZWwgPSAoXHJcbiAgICAgICAgICAgIDxBdXhQYW5lbCByb29tPXt0aGlzLnN0YXRlLnJvb219XHJcbiAgICAgICAgICAgICAgZnVsbEhlaWdodD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgdXNlcklkPXt0aGlzLmNvbnRleHQuY3JlZGVudGlhbHMudXNlcklkfVxyXG4gICAgICAgICAgICAgIGNvbmZlcmVuY2VIYW5kbGVyPXt0aGlzLnByb3BzLkNvbmZlcmVuY2VIYW5kbGVyfVxyXG4gICAgICAgICAgICAgIGRyYWdnaW5nRmlsZT17dGhpcy5zdGF0ZS5kcmFnZ2luZ0ZpbGV9XHJcbiAgICAgICAgICAgICAgZGlzcGxheUNvbmZDYWxsTm90aWZpY2F0aW9uPXt0aGlzLnN0YXRlLmRpc3BsYXlDb25mQ2FsbE5vdGlmaWNhdGlvbn1cclxuICAgICAgICAgICAgICBtYXhIZWlnaHQ9e3RoaXMuc3RhdGUuYXV4UGFuZWxNYXhIZWlnaHR9XHJcbiAgICAgICAgICAgICAgc2hvd0FwcHM9e3RoaXMuc3RhdGUuc2hvd0FwcHN9XHJcbiAgICAgICAgICAgICAgaGlkZUFwcHNEcmF3ZXI9e2ZhbHNlfSA+XHJcbiAgICAgICAgICAgICAgICB7IGF1eCB9XHJcbiAgICAgICAgICAgIDwvQXV4UGFuZWw+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgbGV0IG1lc3NhZ2VDb21wb3NlcjsgbGV0IHNlYXJjaEluZm87XHJcbiAgICAgICAgY29uc3QgY2FuU3BlYWsgPSAoXHJcbiAgICAgICAgICAgIC8vIGpvaW5lZCBhbmQgbm90IHNob3dpbmcgc2VhcmNoIHJlc3VsdHNcclxuICAgICAgICAgICAgbXlNZW1iZXJzaGlwID09PSAnam9pbicgJiYgIXRoaXMuc3RhdGUuc2VhcmNoUmVzdWx0c1xyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKGNhblNwZWFrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IE1lc3NhZ2VDb21wb3NlciA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLk1lc3NhZ2VDb21wb3NlcicpO1xyXG4gICAgICAgICAgICBtZXNzYWdlQ29tcG9zZXIgPVxyXG4gICAgICAgICAgICAgICAgPE1lc3NhZ2VDb21wb3NlclxyXG4gICAgICAgICAgICAgICAgICAgIHJvb209e3RoaXMuc3RhdGUucm9vbX1cclxuICAgICAgICAgICAgICAgICAgICBjYWxsU3RhdGU9e3RoaXMuc3RhdGUuY2FsbFN0YXRlfVxyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnByb3BzLmRpc2FibGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dBcHBzPXt0aGlzLnN0YXRlLnNob3dBcHBzfVxyXG4gICAgICAgICAgICAgICAgICAgIGUyZVN0YXR1cz17dGhpcy5zdGF0ZS5lMmVTdGF0dXN9XHJcbiAgICAgICAgICAgICAgICAgICAgcGVybWFsaW5rQ3JlYXRvcj17dGhpcy5fZ2V0UGVybWFsaW5rQ3JlYXRvckZvclJvb20odGhpcy5zdGF0ZS5yb29tKX1cclxuICAgICAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVE9ETzogV2h5IGFyZW4ndCB3ZSBzdG9yaW5nIHRoZSB0ZXJtL3Njb3BlL2NvdW50IGluIHRoaXMgZm9ybWF0XHJcbiAgICAgICAgLy8gaW4gdGhpcy5zdGF0ZSBpZiB0aGlzIGlzIHdoYXQgUm9vbUhlYWRlciBkZXNpcmVzP1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMpIHtcclxuICAgICAgICAgICAgc2VhcmNoSW5mbyA9IHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaFRlcm06IHRoaXMuc3RhdGUuc2VhcmNoVGVybSxcclxuICAgICAgICAgICAgICAgIHNlYXJjaFNjb3BlOiB0aGlzLnN0YXRlLnNlYXJjaFNjb3BlLFxyXG4gICAgICAgICAgICAgICAgc2VhcmNoQ291bnQ6IHRoaXMuc3RhdGUuc2VhcmNoUmVzdWx0cy5jb3VudCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChpbkNhbGwpIHtcclxuICAgICAgICAgICAgbGV0IHpvb21CdXR0b247IGxldCB2b2ljZU11dGVCdXR0b247IGxldCB2aWRlb011dGVCdXR0b247XHJcblxyXG4gICAgICAgICAgICBpZiAoY2FsbC50eXBlID09PSBcInZpZGVvXCIpIHtcclxuICAgICAgICAgICAgICAgIHpvb21CdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVmlld192b2lwQnV0dG9uXCIgb25DbGljaz17dGhpcy5vbkZ1bGxzY3JlZW5DbGlja30gdGl0bGU9e190KFwiRmlsbCBzY3JlZW5cIil9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGludGFibGVTdmcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy9mdWxsc2NyZWVuLnN2Z1wiKX0gd2lkdGg9XCIyOVwiIGhlaWdodD1cIjIyXCIgc3R5bGU9e3sgbWFyZ2luVG9wOiAxLCBtYXJnaW5SaWdodDogNCB9fSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2aWRlb011dGVCdXR0b24gPVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfdm9pcEJ1dHRvblwiIG9uQ2xpY2s9e3RoaXMub25NdXRlVmlkZW9DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUaW50YWJsZVN2ZyBzcmM9e2NhbGwuaXNMb2NhbFZpZGVvTXV0ZWQoKSA/IHJlcXVpcmUoXCIuLi8uLi8uLi9yZXMvaW1nL3ZpZGVvLXVubXV0ZS5zdmdcIikgOiByZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy92aWRlby1tdXRlLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2NhbGwuaXNMb2NhbFZpZGVvTXV0ZWQoKSA/IF90KFwiQ2xpY2sgdG8gdW5tdXRlIHZpZGVvXCIpIDogX3QoXCJDbGljayB0byBtdXRlIHZpZGVvXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMzFcIiBoZWlnaHQ9XCIyN1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZvaWNlTXV0ZUJ1dHRvbiA9XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X3ZvaXBCdXR0b25cIiBvbkNsaWNrPXt0aGlzLm9uTXV0ZUF1ZGlvQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxUaW50YWJsZVN2ZyBzcmM9e2NhbGwuaXNNaWNyb3Bob25lTXV0ZWQoKSA/IHJlcXVpcmUoXCIuLi8uLi8uLi9yZXMvaW1nL3ZvaWNlLXVubXV0ZS5zdmdcIikgOiByZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy92b2ljZS1tdXRlLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17Y2FsbC5pc01pY3JvcGhvbmVNdXRlZCgpID8gX3QoXCJDbGljayB0byB1bm11dGUgYXVkaW9cIikgOiBfdChcIkNsaWNrIHRvIG11dGUgYXVkaW9cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjIxXCIgaGVpZ2h0PVwiMjZcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG5cclxuICAgICAgICAgICAgLy8gd3JhcCB0aGUgZXhpc3Rpbmcgc3RhdHVzIGJhciBpbnRvIGEgJ2NhbGxTdGF0dXNCYXInIHdoaWNoIGFkZHMgbW9yZSBrbm9icy5cclxuICAgICAgICAgICAgc3RhdHVzQmFyID1cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfY2FsbFN0YXR1c0JhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdm9pY2VNdXRlQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHZpZGVvTXV0ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyB6b29tQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHN0YXR1c0JhciB9XHJcbiAgICAgICAgICAgICAgICAgICAgPFRpbnRhYmxlU3ZnIGNsYXNzTmFtZT1cIm14X1Jvb21WaWV3X3ZvaXBDaGV2cm9uXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vcmVzL2ltZy92b2lwLWNoZXZyb24uc3ZnXCIpfSB3aWR0aD1cIjIyXCIgaGVpZ2h0PVwiMTdcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaWYgd2UgaGF2ZSBzZWFyY2ggcmVzdWx0cywgd2Uga2VlcCB0aGUgbWVzc2FnZXBhbmVsIChzbyB0aGF0IGl0IHByZXNlcnZlcyBpdHNcclxuICAgICAgICAvLyBzY3JvbGwgc3RhdGUpLCBidXQgaGlkZSBpdC5cclxuICAgICAgICBsZXQgc2VhcmNoUmVzdWx0c1BhbmVsO1xyXG4gICAgICAgIGxldCBoaWRlTWVzc2FnZVBhbmVsID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMpIHtcclxuICAgICAgICAgICAgLy8gc2hvdyBzZWFyY2hpbmcgc3Bpbm5lclxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzLnJlc3VsdHMgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoUmVzdWx0c1BhbmVsID0gKDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfbWVzc2FnZVBhbmVsIG14X1Jvb21WaWV3X21lc3NhZ2VQYW5lbFNlYXJjaFNwaW5uZXJcIiAvPik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hSZXN1bHRzUGFuZWwgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFNjcm9sbFBhbmVsIHJlZj17dGhpcy5fc2VhcmNoUmVzdWx0c1BhbmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tVmlld19tZXNzYWdlUGFuZWwgbXhfUm9vbVZpZXdfc2VhcmNoUmVzdWx0c1BhbmVsXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25GaWxsUmVxdWVzdD17dGhpcy5vblNlYXJjaFJlc3VsdHNGaWxsUmVxdWVzdH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPXtzY3JvbGxoZWFkZXJfY2xhc3Nlc30+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLmdldFNlYXJjaFJlc3VsdFRpbGVzKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvU2Nyb2xsUGFuZWw+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGhpZGVNZXNzYWdlUGFuZWwgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2hvdWxkSGlnaGxpZ2h0ID0gdGhpcy5zdGF0ZS5pc0luaXRpYWxFdmVudEhpZ2hsaWdodGVkO1xyXG4gICAgICAgIGxldCBoaWdobGlnaHRlZEV2ZW50SWQgPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmZvcndhcmRpbmdFdmVudCkge1xyXG4gICAgICAgICAgICBoaWdobGlnaHRlZEV2ZW50SWQgPSB0aGlzLnN0YXRlLmZvcndhcmRpbmdFdmVudC5nZXRJZCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2hvdWxkSGlnaGxpZ2h0KSB7XHJcbiAgICAgICAgICAgIGhpZ2hsaWdodGVkRXZlbnRJZCA9IHRoaXMuc3RhdGUuaW5pdGlhbEV2ZW50SWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjb25zb2xlLmluZm8oXCJTaG93VXJsUHJldmlldyBmb3IgJXMgaXMgJXNcIiwgdGhpcy5zdGF0ZS5yb29tLnJvb21JZCwgdGhpcy5zdGF0ZS5zaG93VXJsUHJldmlldyk7XHJcbiAgICAgICAgY29uc3QgbWVzc2FnZVBhbmVsID0gKFxyXG4gICAgICAgICAgICA8VGltZWxpbmVQYW5lbFxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9nYXRoZXJUaW1lbGluZVBhbmVsUmVmfVxyXG4gICAgICAgICAgICAgICAgdGltZWxpbmVTZXQ9e3RoaXMuc3RhdGUucm9vbS5nZXRVbmZpbHRlcmVkVGltZWxpbmVTZXQoKX1cclxuICAgICAgICAgICAgICAgIHNob3dSZWFkUmVjZWlwdHM9e3RoaXMuc3RhdGUuc2hvd1JlYWRSZWNlaXB0c31cclxuICAgICAgICAgICAgICAgIG1hbmFnZVJlYWRSZWNlaXB0cz17IXRoaXMuc3RhdGUuaXNQZWVraW5nfVxyXG4gICAgICAgICAgICAgICAgbWFuYWdlUmVhZE1hcmtlcnM9eyF0aGlzLnN0YXRlLmlzUGVla2luZ31cclxuICAgICAgICAgICAgICAgIGhpZGRlbj17aGlkZU1lc3NhZ2VQYW5lbH1cclxuICAgICAgICAgICAgICAgIGhpZ2hsaWdodGVkRXZlbnRJZD17aGlnaGxpZ2h0ZWRFdmVudElkfVxyXG4gICAgICAgICAgICAgICAgZXZlbnRJZD17dGhpcy5zdGF0ZS5pbml0aWFsRXZlbnRJZH1cclxuICAgICAgICAgICAgICAgIGV2ZW50UGl4ZWxPZmZzZXQ9e3RoaXMuc3RhdGUuaW5pdGlhbEV2ZW50UGl4ZWxPZmZzZXR9XHJcbiAgICAgICAgICAgICAgICBvblNjcm9sbD17dGhpcy5vbk1lc3NhZ2VMaXN0U2Nyb2xsfVxyXG4gICAgICAgICAgICAgICAgb25SZWFkTWFya2VyVXBkYXRlZD17dGhpcy5fdXBkYXRlVG9wVW5yZWFkTWVzc2FnZXNCYXJ9XHJcbiAgICAgICAgICAgICAgICBzaG93VXJsUHJldmlldyA9IHt0aGlzLnN0YXRlLnNob3dVcmxQcmV2aWV3fVxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfbWVzc2FnZVBhbmVsXCJcclxuICAgICAgICAgICAgICAgIG1lbWJlcnNMb2FkZWQ9e3RoaXMuc3RhdGUubWVtYmVyc0xvYWRlZH1cclxuICAgICAgICAgICAgICAgIHBlcm1hbGlua0NyZWF0b3I9e3RoaXMuX2dldFBlcm1hbGlua0NyZWF0b3JGb3JSb29tKHRoaXMuc3RhdGUucm9vbSl9XHJcbiAgICAgICAgICAgICAgICByZXNpemVOb3RpZmllcj17dGhpcy5wcm9wcy5yZXNpemVOb3RpZmllcn1cclxuICAgICAgICAgICAgICAgIHNob3dSZWFjdGlvbnM9e3RydWV9XHJcbiAgICAgICAgICAgIC8+KTtcclxuXHJcbiAgICAgICAgbGV0IHRvcFVucmVhZE1lc3NhZ2VzQmFyID0gbnVsbDtcclxuICAgICAgICAvLyBEbyBub3Qgc2hvdyBUb3BVbnJlYWRNZXNzYWdlc0JhciBpZiB3ZSBoYXZlIHNlYXJjaCByZXN1bHRzIHNob3dpbmcsIGl0IG1ha2VzIG5vIHNlbnNlXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2hvd1RvcFVucmVhZE1lc3NhZ2VzQmFyICYmICF0aGlzLnN0YXRlLnNlYXJjaFJlc3VsdHMpIHtcclxuICAgICAgICAgICAgY29uc3QgVG9wVW5yZWFkTWVzc2FnZXNCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5Ub3BVbnJlYWRNZXNzYWdlc0JhcicpO1xyXG4gICAgICAgICAgICB0b3BVbnJlYWRNZXNzYWdlc0JhciA9ICg8VG9wVW5yZWFkTWVzc2FnZXNCYXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25TY3JvbGxVcENsaWNrPXt0aGlzLmp1bXBUb1JlYWRNYXJrZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xvc2VDbGljaz17dGhpcy5mb3JnZXRSZWFkTWFya2VyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBqdW1wVG9Cb3R0b207XHJcbiAgICAgICAgLy8gRG8gbm90IHNob3cgSnVtcFRvQm90dG9tQnV0dG9uIGlmIHdlIGhhdmUgc2VhcmNoIHJlc3VsdHMgc2hvd2luZywgaXQgbWFrZXMgbm8gc2Vuc2VcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuYXRFbmRPZkxpdmVUaW1lbGluZSAmJiAhdGhpcy5zdGF0ZS5zZWFyY2hSZXN1bHRzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEp1bXBUb0JvdHRvbUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLkp1bXBUb0JvdHRvbUJ1dHRvbicpO1xyXG4gICAgICAgICAgICBqdW1wVG9Cb3R0b20gPSAoPEp1bXBUb0JvdHRvbUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgbnVtVW5yZWFkTWVzc2FnZXM9e3RoaXMuc3RhdGUubnVtVW5yZWFkTWVzc2FnZXN9XHJcbiAgICAgICAgICAgICAgICBvblNjcm9sbFRvQm90dG9tQ2xpY2s9e3RoaXMuanVtcFRvTGl2ZVRpbWVsaW5lfVxyXG4gICAgICAgICAgICAvPik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHN0YXR1c0JhckFyZWFDbGFzcyA9IGNsYXNzTmFtZXMoXHJcbiAgICAgICAgICAgIFwibXhfUm9vbVZpZXdfc3RhdHVzQXJlYVwiLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcIm14X1Jvb21WaWV3X3N0YXR1c0FyZWFfZXhwYW5kZWRcIjogaXNTdGF0dXNBcmVhRXhwYW5kZWQsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgY29uc3QgZmFkYWJsZVNlY3Rpb25DbGFzc2VzID0gY2xhc3NOYW1lcyhcclxuICAgICAgICAgICAgXCJteF9Sb29tVmlld19ib2R5XCIsIFwibXhfZmFkYWJsZVwiLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcIm14X2ZhZGFibGVfZmFkZWRcIjogdGhpcy5wcm9wcy5kaXNhYmxlZCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBzaG93UmlnaHRQYW5lbCA9ICFmb3JjZUhpZGVSaWdodFBhbmVsICYmIHRoaXMuc3RhdGUucm9vbSAmJiB0aGlzLnN0YXRlLnNob3dSaWdodFBhbmVsO1xyXG4gICAgICAgIGNvbnN0IHJpZ2h0UGFuZWwgPSBzaG93UmlnaHRQYW5lbFxyXG4gICAgICAgICAgICA/IDxSaWdodFBhbmVsIHJvb21JZD17dGhpcy5zdGF0ZS5yb29tLnJvb21JZH0gcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9IC8+XHJcbiAgICAgICAgICAgIDogbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgdGltZWxpbmVDbGFzc2VzID0gY2xhc3NOYW1lcyhcIm14X1Jvb21WaWV3X3RpbWVsaW5lXCIsIHtcclxuICAgICAgICAgICAgbXhfUm9vbVZpZXdfdGltZWxpbmVfcnJfZW5hYmxlZDogdGhpcy5zdGF0ZS5zaG93UmVhZFJlY2VpcHRzLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8Um9vbUNvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3RoaXMuc3RhdGV9PlxyXG4gICAgICAgICAgICAgICAgPG1haW4gY2xhc3NOYW1lPXtcIm14X1Jvb21WaWV3XCIgKyAoaW5DYWxsID8gXCIgbXhfUm9vbVZpZXdfaW5DYWxsXCIgOiBcIlwiKX0gcmVmPXt0aGlzLl9yb29tVmlld30+XHJcbiAgICAgICAgICAgICAgICAgICAgPEVycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSb29tSGVhZGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnN0YXRlLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hJbmZvPXtzZWFyY2hJbmZvfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb29iRGF0YT17dGhpcy5wcm9wcy5vb2JEYXRhfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5Sb29tPXtteU1lbWJlcnNoaXAgPT09ICdqb2luJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uU2VhcmNoQ2xpY2s9e3RoaXMub25TZWFyY2hDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uU2V0dGluZ3NDbGljaz17dGhpcy5vblNldHRpbmdzQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblBpbm5lZENsaWNrPXt0aGlzLm9uUGlubmVkQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbENsaWNrPXsoYXV4ICYmICFoaWRlQ2FuY2VsKSA/IHRoaXMub25DYW5jZWxDbGljayA6IG51bGx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkZvcmdldENsaWNrPXsobXlNZW1iZXJzaGlwID09PSBcImxlYXZlXCIpID8gdGhpcy5vbkZvcmdldENsaWNrIDogbnVsbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uTGVhdmVDbGljaz17KG15TWVtYmVyc2hpcCA9PT0gXCJqb2luXCIpID8gdGhpcy5vbkxlYXZlQ2xpY2sgOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZTJlU3RhdHVzPXt0aGlzLnN0YXRlLmUyZVN0YXR1c31cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPE1haW5TcGxpdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFuZWw9e3JpZ2h0UGFuZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNpemVOb3RpZmllcj17dGhpcy5wcm9wcy5yZXNpemVOb3RpZmllcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2ZhZGFibGVTZWN0aW9uQ2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2F1eFBhbmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXt0aW1lbGluZUNsYXNzZXN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dG9wVW5yZWFkTWVzc2FnZXNCYXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtqdW1wVG9Cb3R0b219XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHttZXNzYWdlUGFuZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzZWFyY2hSZXN1bHRzUGFuZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3N0YXR1c0JhckFyZWFDbGFzc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVZpZXdfc3RhdHVzQXJlYUJveFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVmlld19zdGF0dXNBcmVhQm94X2xpbmVcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3N0YXR1c0Jhcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3ByZXZpZXdCYXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge21lc3NhZ2VDb21wb3Nlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L01haW5TcGxpdD5cclxuICAgICAgICAgICAgICAgICAgICA8L0Vycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgICAgICA8L21haW4+XHJcbiAgICAgICAgICAgIDwvUm9vbUNvbnRleHQuUHJvdmlkZXI+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=