"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var React = _interopRequireWildcard(require("react"));

var _languageHandler = require("../../languageHandler");

var _ToastStore = _interopRequireDefault(require("../../stores/ToastStore"));

var _classnames = _interopRequireDefault(require("classnames"));

/*
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class ToastContainer extends React.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onToastStoreUpdate", () => {
      this.setState({
        toasts: _ToastStore.default.sharedInstance().getToasts()
      });
    });
    this.state = {
      toasts: _ToastStore.default.sharedInstance().getToasts()
    }; // Start listening here rather than in componentDidMount because
    // toasts may dismiss themselves in their didMount if they find
    // they're already irrelevant by the time they're mounted, and
    // our own componentDidMount is too late.

    _ToastStore.default.sharedInstance().on('update', this._onToastStoreUpdate);
  }

  componentWillUnmount() {
    _ToastStore.default.sharedInstance().removeListener('update', this._onToastStoreUpdate);
  }

  render() {
    const totalCount = this.state.toasts.length;
    const isStacked = totalCount > 1;
    let toast;

    if (totalCount !== 0) {
      const topToast = this.state.toasts[0];
      const {
        title,
        icon,
        key,
        component,
        props
      } = topToast;
      const toastClasses = (0, _classnames.default)("mx_Toast_toast", {
        "mx_Toast_hasIcon": icon,
        ["mx_Toast_icon_".concat(icon)]: icon
      });
      const countIndicator = isStacked ? (0, _languageHandler._t)(" (1/%(totalCount)s)", {
        totalCount
      }) : null;
      const toastProps = Object.assign({}, props, {
        key,
        toastKey: key
      });
      toast = React.createElement("div", {
        className: toastClasses
      }, React.createElement("h2", null, title, countIndicator), React.createElement("div", {
        className: "mx_Toast_body"
      }, React.createElement(component, toastProps)));
    }

    const containerClasses = (0, _classnames.default)("mx_ToastContainer", {
      "mx_ToastContainer_stacked": isStacked
    });
    return React.createElement("div", {
      className: containerClasses,
      role: "alert"
    }, toast);
  }

}

exports.default = ToastContainer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvVG9hc3RDb250YWluZXIuanMiXSwibmFtZXMiOlsiVG9hc3RDb250YWluZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwic2V0U3RhdGUiLCJ0b2FzdHMiLCJUb2FzdFN0b3JlIiwic2hhcmVkSW5zdGFuY2UiLCJnZXRUb2FzdHMiLCJzdGF0ZSIsIm9uIiwiX29uVG9hc3RTdG9yZVVwZGF0ZSIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJyZW5kZXIiLCJ0b3RhbENvdW50IiwibGVuZ3RoIiwiaXNTdGFja2VkIiwidG9hc3QiLCJ0b3BUb2FzdCIsInRpdGxlIiwiaWNvbiIsImtleSIsImNvbXBvbmVudCIsInByb3BzIiwidG9hc3RDbGFzc2VzIiwiY291bnRJbmRpY2F0b3IiLCJ0b2FzdFByb3BzIiwiT2JqZWN0IiwiYXNzaWduIiwidG9hc3RLZXkiLCJjcmVhdGVFbGVtZW50IiwiY29udGFpbmVyQ2xhc3NlcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFuQkE7Ozs7Ozs7Ozs7Ozs7OztBQXFCZSxNQUFNQSxjQUFOLFNBQTZCQyxLQUFLLENBQUNDLFNBQW5DLENBQTZDO0FBQ3hEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLCtEQWVRLE1BQU07QUFDeEIsV0FBS0MsUUFBTCxDQUFjO0FBQUNDLFFBQUFBLE1BQU0sRUFBRUMsb0JBQVdDLGNBQVgsR0FBNEJDLFNBQTVCO0FBQVQsT0FBZDtBQUNILEtBakJhO0FBRVYsU0FBS0MsS0FBTCxHQUFhO0FBQUNKLE1BQUFBLE1BQU0sRUFBRUMsb0JBQVdDLGNBQVgsR0FBNEJDLFNBQTVCO0FBQVQsS0FBYixDQUZVLENBSVY7QUFDQTtBQUNBO0FBQ0E7O0FBQ0FGLHdCQUFXQyxjQUFYLEdBQTRCRyxFQUE1QixDQUErQixRQUEvQixFQUF5QyxLQUFLQyxtQkFBOUM7QUFDSDs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkJOLHdCQUFXQyxjQUFYLEdBQTRCTSxjQUE1QixDQUEyQyxRQUEzQyxFQUFxRCxLQUFLRixtQkFBMUQ7QUFDSDs7QUFNREcsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHLEtBQUtOLEtBQUwsQ0FBV0osTUFBWCxDQUFrQlcsTUFBckM7QUFDQSxVQUFNQyxTQUFTLEdBQUdGLFVBQVUsR0FBRyxDQUEvQjtBQUNBLFFBQUlHLEtBQUo7O0FBQ0EsUUFBSUgsVUFBVSxLQUFLLENBQW5CLEVBQXNCO0FBQ2xCLFlBQU1JLFFBQVEsR0FBRyxLQUFLVixLQUFMLENBQVdKLE1BQVgsQ0FBa0IsQ0FBbEIsQ0FBakI7QUFDQSxZQUFNO0FBQUNlLFFBQUFBLEtBQUQ7QUFBUUMsUUFBQUEsSUFBUjtBQUFjQyxRQUFBQSxHQUFkO0FBQW1CQyxRQUFBQSxTQUFuQjtBQUE4QkMsUUFBQUE7QUFBOUIsVUFBdUNMLFFBQTdDO0FBQ0EsWUFBTU0sWUFBWSxHQUFHLHlCQUFXLGdCQUFYLEVBQTZCO0FBQzlDLDRCQUFvQkosSUFEMEI7QUFFOUMsaUNBQWtCQSxJQUFsQixJQUEyQkE7QUFGbUIsT0FBN0IsQ0FBckI7QUFJQSxZQUFNSyxjQUFjLEdBQUdULFNBQVMsR0FBRyx5QkFBRyxxQkFBSCxFQUEwQjtBQUFDRixRQUFBQTtBQUFELE9BQTFCLENBQUgsR0FBNkMsSUFBN0U7QUFFQSxZQUFNWSxVQUFVLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JMLEtBQWxCLEVBQXlCO0FBQ3hDRixRQUFBQSxHQUR3QztBQUV4Q1EsUUFBQUEsUUFBUSxFQUFFUjtBQUY4QixPQUF6QixDQUFuQjtBQUlBSixNQUFBQSxLQUFLLEdBQUk7QUFBSyxRQUFBLFNBQVMsRUFBRU87QUFBaEIsU0FDTCxnQ0FBS0wsS0FBTCxFQUFZTSxjQUFaLENBREssRUFFTDtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBZ0N6QixLQUFLLENBQUM4QixhQUFOLENBQW9CUixTQUFwQixFQUErQkksVUFBL0IsQ0FBaEMsQ0FGSyxDQUFUO0FBSUg7O0FBRUQsVUFBTUssZ0JBQWdCLEdBQUcseUJBQVcsbUJBQVgsRUFBZ0M7QUFDckQsbUNBQTZCZjtBQUR3QixLQUFoQyxDQUF6QjtBQUlBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBRWUsZ0JBQWhCO0FBQWtDLE1BQUEsSUFBSSxFQUFDO0FBQXZDLE9BQ0tkLEtBREwsQ0FESjtBQUtIOztBQXBEdUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBUb2FzdFN0b3JlIGZyb20gXCIuLi8uLi9zdG9yZXMvVG9hc3RTdG9yZVwiO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tIFwiY2xhc3NuYW1lc1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVG9hc3RDb250YWluZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge3RvYXN0czogVG9hc3RTdG9yZS5zaGFyZWRJbnN0YW5jZSgpLmdldFRvYXN0cygpfTtcclxuXHJcbiAgICAgICAgLy8gU3RhcnQgbGlzdGVuaW5nIGhlcmUgcmF0aGVyIHRoYW4gaW4gY29tcG9uZW50RGlkTW91bnQgYmVjYXVzZVxyXG4gICAgICAgIC8vIHRvYXN0cyBtYXkgZGlzbWlzcyB0aGVtc2VsdmVzIGluIHRoZWlyIGRpZE1vdW50IGlmIHRoZXkgZmluZFxyXG4gICAgICAgIC8vIHRoZXkncmUgYWxyZWFkeSBpcnJlbGV2YW50IGJ5IHRoZSB0aW1lIHRoZXkncmUgbW91bnRlZCwgYW5kXHJcbiAgICAgICAgLy8gb3VyIG93biBjb21wb25lbnREaWRNb3VudCBpcyB0b28gbGF0ZS5cclxuICAgICAgICBUb2FzdFN0b3JlLnNoYXJlZEluc3RhbmNlKCkub24oJ3VwZGF0ZScsIHRoaXMuX29uVG9hc3RTdG9yZVVwZGF0ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgVG9hc3RTdG9yZS5zaGFyZWRJbnN0YW5jZSgpLnJlbW92ZUxpc3RlbmVyKCd1cGRhdGUnLCB0aGlzLl9vblRvYXN0U3RvcmVVcGRhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblRvYXN0U3RvcmVVcGRhdGUgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dG9hc3RzOiBUb2FzdFN0b3JlLnNoYXJlZEluc3RhbmNlKCkuZ2V0VG9hc3RzKCl9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IHRvdGFsQ291bnQgPSB0aGlzLnN0YXRlLnRvYXN0cy5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgaXNTdGFja2VkID0gdG90YWxDb3VudCA+IDE7XHJcbiAgICAgICAgbGV0IHRvYXN0O1xyXG4gICAgICAgIGlmICh0b3RhbENvdW50ICE9PSAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvcFRvYXN0ID0gdGhpcy5zdGF0ZS50b2FzdHNbMF07XHJcbiAgICAgICAgICAgIGNvbnN0IHt0aXRsZSwgaWNvbiwga2V5LCBjb21wb25lbnQsIHByb3BzfSA9IHRvcFRvYXN0O1xyXG4gICAgICAgICAgICBjb25zdCB0b2FzdENsYXNzZXMgPSBjbGFzc05hbWVzKFwibXhfVG9hc3RfdG9hc3RcIiwge1xyXG4gICAgICAgICAgICAgICAgXCJteF9Ub2FzdF9oYXNJY29uXCI6IGljb24sXHJcbiAgICAgICAgICAgICAgICBbYG14X1RvYXN0X2ljb25fJHtpY29ufWBdOiBpY29uLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgY291bnRJbmRpY2F0b3IgPSBpc1N0YWNrZWQgPyBfdChcIiAoMS8lKHRvdGFsQ291bnQpcylcIiwge3RvdGFsQ291bnR9KSA6IG51bGw7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB0b2FzdFByb3BzID0gT2JqZWN0LmFzc2lnbih7fSwgcHJvcHMsIHtcclxuICAgICAgICAgICAgICAgIGtleSxcclxuICAgICAgICAgICAgICAgIHRvYXN0S2V5OiBrZXksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0b2FzdCA9ICg8ZGl2IGNsYXNzTmFtZT17dG9hc3RDbGFzc2VzfT5cclxuICAgICAgICAgICAgICAgIDxoMj57dGl0bGV9e2NvdW50SW5kaWNhdG9yfTwvaDI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvYXN0X2JvZHlcIj57UmVhY3QuY3JlYXRlRWxlbWVudChjb21wb25lbnQsIHRvYXN0UHJvcHMpfTwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyQ2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9Ub2FzdENvbnRhaW5lclwiLCB7XHJcbiAgICAgICAgICAgIFwibXhfVG9hc3RDb250YWluZXJfc3RhY2tlZFwiOiBpc1N0YWNrZWQsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjb250YWluZXJDbGFzc2VzfSByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgIHt0b2FzdH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=