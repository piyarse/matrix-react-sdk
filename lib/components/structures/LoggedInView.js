"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _matrixJsSdk = require("matrix-js-sdk");

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _Keyboard = require("../../Keyboard");

var _PageTypes = _interopRequireDefault(require("../../PageTypes"));

var _CallMediaHandler = _interopRequireDefault(require("../../CallMediaHandler"));

var _FontManager = require("../../utils/FontManager");

var sdk = _interopRequireWildcard(require("../../index"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _SessionStore = _interopRequireDefault(require("../../stores/SessionStore"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _SettingsStore = _interopRequireDefault(require("../../settings/SettingsStore"));

var _RoomListStore = _interopRequireDefault(require("../../stores/RoomListStore"));

var _TagOrderActions = _interopRequireDefault(require("../../actions/TagOrderActions"));

var _RoomListActions = _interopRequireDefault(require("../../actions/RoomListActions"));

var _ResizeHandle = _interopRequireDefault(require("../views/elements/ResizeHandle"));

var _resizer = require("../../resizer");

var _MatrixClientContext = _interopRequireDefault(require("../../contexts/MatrixClientContext"));

var KeyboardShortcuts = _interopRequireWildcard(require("../../accessibility/KeyboardShortcuts"));

var _HomePage = _interopRequireDefault(require("./HomePage"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// We need to fetch each pinned message individually (if we don't already have it)
// so each pinned message may trigger a request. Limit the number per room for sanity.
// NB. this is just for server notices rather than pinned messages in general.
const MAX_PINNED_NOTICES_PER_ROOM = 2;

function canElementReceiveInput(el) {
  return el.tagName === "INPUT" || el.tagName === "TEXTAREA" || el.tagName === "SELECT" || !!el.getAttribute("contenteditable");
}
/**
 * This is what our MatrixChat shows when we are logged in. The precise view is
 * determined by the page_type property.
 *
 * Currently it's very tightly coupled with MatrixChat. We should try to do
 * something about that.
 *
 * Components mounted below us can access the matrix client via the react context.
 */


const LoggedInView = (0, _createReactClass.default)({
  displayName: 'LoggedInView',
  propTypes: {
    matrixClient: _propTypes.default.instanceOf(_matrixJsSdk.MatrixClient).isRequired,
    page_type: _propTypes.default.string.isRequired,
    onRoomCreated: _propTypes.default.func,
    // Called with the credentials of a registered user (if they were a ROU that
    // transitioned to PWLU)
    onRegistered: _propTypes.default.func,
    // Used by the RoomView to handle joining rooms
    viaServers: _propTypes.default.arrayOf(_propTypes.default.string) // and lots and lots of other stuff.

  },
  getInitialState: function () {
    return {
      // use compact timeline view
      useCompactLayout: _SettingsStore.default.getValue('useCompactLayout'),
      // any currently active server notice events
      serverNoticeEvents: []
    };
  },
  componentDidMount: function () {
    this.resizer = this._createResizer();
    this.resizer.attach();

    this._loadResizerPreferences();
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    // stash the MatrixClient in case we log out before we are unmounted
    this._matrixClient = this.props.matrixClient;

    _CallMediaHandler.default.loadDevices();

    document.addEventListener('keydown', this._onNativeKeyDown, false);
    this._sessionStore = _SessionStore.default;
    this._sessionStoreToken = this._sessionStore.addListener(this._setStateFromSessionStore);

    this._setStateFromSessionStore();

    this._updateServerNoticeEvents();

    this._matrixClient.on("accountData", this.onAccountData);

    this._matrixClient.on("sync", this.onSync);

    this._matrixClient.on("RoomState.events", this.onRoomStateEvents);

    (0, _FontManager.fixupColorFonts)();
    this._roomView = (0, _react.createRef)();
  },

  componentDidUpdate(prevProps) {
    // attempt to guess when a banner was opened or closed
    if (prevProps.showCookieBar !== this.props.showCookieBar || prevProps.hasNewVersion !== this.props.hasNewVersion || prevProps.userHasGeneratedPassword !== this.props.userHasGeneratedPassword || prevProps.showNotifierToolbar !== this.props.showNotifierToolbar) {
      this.props.resizeNotifier.notifyBannersChanged();
    }
  },

  componentWillUnmount: function () {
    document.removeEventListener('keydown', this._onNativeKeyDown, false);

    this._matrixClient.removeListener("accountData", this.onAccountData);

    this._matrixClient.removeListener("sync", this.onSync);

    this._matrixClient.removeListener("RoomState.events", this.onRoomStateEvents);

    if (this._sessionStoreToken) {
      this._sessionStoreToken.remove();
    }

    this.resizer.detach();
  },
  // Child components assume that the client peg will not be null, so give them some
  // sort of assurance here by only allowing a re-render if the client is truthy.
  //
  // This is required because `LoggedInView` maintains its own state and if this state
  // updates after the client peg has been made null (during logout), then it will
  // attempt to re-render and the children will throw errors.
  shouldComponentUpdate: function () {
    return Boolean(_MatrixClientPeg.MatrixClientPeg.get());
  },
  canResetTimelineInRoom: function (roomId) {
    if (!this._roomView.current) {
      return true;
    }

    return this._roomView.current.canResetTimeline();
  },

  _setStateFromSessionStore() {
    this.setState({
      userHasGeneratedPassword: Boolean(this._sessionStore.getCachedPassword())
    });
  },

  _createResizer() {
    const classNames = {
      handle: "mx_ResizeHandle",
      vertical: "mx_ResizeHandle_vertical",
      reverse: "mx_ResizeHandle_reverse"
    };
    const collapseConfig = {
      toggleSize: 260 - 50,
      onCollapsed: collapsed => {
        if (collapsed) {
          _dispatcher.default.dispatch({
            action: "hide_left_panel"
          }, true);

          window.localStorage.setItem("mx_lhs_size", '0');
        } else {
          _dispatcher.default.dispatch({
            action: "show_left_panel"
          }, true);
        }
      },
      onResized: size => {
        window.localStorage.setItem("mx_lhs_size", '' + size);
        this.props.resizeNotifier.notifyLeftHandleResized();
      }
    };
    const resizer = new _resizer.Resizer(this.resizeContainer, _resizer.CollapseDistributor, collapseConfig);
    resizer.setClassNames(classNames);
    return resizer;
  },

  _loadResizerPreferences() {
    let lhsSize = window.localStorage.getItem("mx_lhs_size");

    if (lhsSize !== null) {
      lhsSize = parseInt(lhsSize, 10);
    } else {
      lhsSize = 350;
    }

    this.resizer.forHandleAt(0).resize(lhsSize);
  },

  onAccountData: function (event) {
    if (event.getType() === "im.vector.web.settings") {
      this.setState({
        useCompactLayout: event.getContent().useCompactLayout
      });
    }

    if (event.getType() === "m.ignored_user_list") {
      _dispatcher.default.dispatch({
        action: "ignore_state_changed"
      });
    }
  },
  onSync: function (syncState, oldSyncState, data) {
    const oldErrCode = this.state.syncErrorData && this.state.syncErrorData.error && this.state.syncErrorData.error.errcode;
    const newErrCode = data && data.error && data.error.errcode;
    if (syncState === oldSyncState && oldErrCode === newErrCode) return;

    if (syncState === 'ERROR') {
      this.setState({
        syncErrorData: data
      });
    } else {
      this.setState({
        syncErrorData: null
      });
    }

    if (oldSyncState === 'PREPARED' && syncState === 'SYNCING') {
      this._updateServerNoticeEvents();
    }
  },
  onRoomStateEvents: function (ev, state) {
    const roomLists = _RoomListStore.default.getRoomLists();

    if (roomLists['m.server_notice'] && roomLists['m.server_notice'].some(r => r.roomId === ev.getRoomId())) {
      this._updateServerNoticeEvents();
    }
  },
  _updateServerNoticeEvents: async function () {
    const roomLists = _RoomListStore.default.getRoomLists();

    if (!roomLists['m.server_notice']) return [];
    const pinnedEvents = [];

    for (const room of roomLists['m.server_notice']) {
      const pinStateEvent = room.currentState.getStateEvents("m.room.pinned_events", "");
      if (!pinStateEvent || !pinStateEvent.getContent().pinned) continue;
      const pinnedEventIds = pinStateEvent.getContent().pinned.slice(0, MAX_PINNED_NOTICES_PER_ROOM);

      for (const eventId of pinnedEventIds) {
        const timeline = await this._matrixClient.getEventTimeline(room.getUnfilteredTimelineSet(), eventId, 0);
        const ev = timeline.getEvents().find(ev => ev.getId() === eventId);
        if (ev) pinnedEvents.push(ev);
      }
    }

    this.setState({
      serverNoticeEvents: pinnedEvents
    });
  },
  _onPaste: function (ev) {
    let canReceiveInput = false;
    let element = ev.target; // test for all parents because the target can be a child of a contenteditable element

    while (!canReceiveInput && element) {
      canReceiveInput = canElementReceiveInput(element);
      element = element.parentElement;
    }

    if (!canReceiveInput) {
      // refocusing during a paste event will make the
      // paste end up in the newly focused element,
      // so dispatch synchronously before paste happens
      _dispatcher.default.dispatch({
        action: 'focus_composer'
      }, true);
    }
  },

  /*
  SOME HACKERY BELOW:
  React optimizes event handlers, by always attaching only 1 handler to the document for a given type.
  It then internally determines the order in which React event handlers should be called,
  emulating the capture and bubbling phases the DOM also has.
    But, as the native handler for React is always attached on the document,
  it will always run last for bubbling (first for capturing) handlers,
  and thus React basically has its own event phases, and will always run
  after (before for capturing) any native other event handlers (as they tend to be attached last).
    So ideally one wouldn't mix React and native event handlers to have bubbling working as expected,
  but we do need a native event handler here on the document,
  to get keydown events when there is no focused element (target=body).
    We also do need bubbling here to give child components a chance to call `stopPropagation()`,
  for keydown events it can handle itself, and shouldn't be redirected to the composer.
    So we listen with React on this component to get any events on focused elements, and get bubbling working as expected.
  We also listen with a native listener on the document to get keydown events when no element is focused.
  Bubbling is irrelevant here as the target is the body element.
  */
  _onReactKeyDown: function (ev) {
    // events caught while bubbling up on the root element
    // of this component, so something must be focused.
    this._onKeyDown(ev);
  },
  _onNativeKeyDown: function (ev) {
    // only pass this if there is no focused element.
    // if there is, _onKeyDown will be called by the
    // react keydown handler that respects the react bubbling order.
    if (ev.target === document.body) {
      this._onKeyDown(ev);
    }
  },
  _onKeyDown: function (ev) {
    /*
    // Remove this for now as ctrl+alt = alt-gr so this breaks keyboards which rely on alt-gr for numbers
    // Will need to find a better meta key if anyone actually cares about using this.
    if (ev.altKey && ev.ctrlKey && ev.keyCode > 48 && ev.keyCode < 58) {
        dis.dispatch({
            action: 'view_indexed_room',
            roomIndex: ev.keyCode - 49,
        });
        ev.stopPropagation();
        ev.preventDefault();
        return;
    }
    */
    let handled = false;
    const ctrlCmdOnly = (0, _Keyboard.isOnlyCtrlOrCmdKeyEvent)(ev);
    const hasModifier = ev.altKey || ev.ctrlKey || ev.metaKey || ev.shiftKey;
    const isModifier = ev.key === _Keyboard.Key.ALT || ev.key === _Keyboard.Key.CONTROL || ev.key === _Keyboard.Key.META || ev.key === _Keyboard.Key.SHIFT;

    switch (ev.key) {
      case _Keyboard.Key.PAGE_UP:
      case _Keyboard.Key.PAGE_DOWN:
        if (!hasModifier && !isModifier) {
          this._onScrollKeyPressed(ev);

          handled = true;
        }

        break;

      case _Keyboard.Key.HOME:
      case _Keyboard.Key.END:
        if (ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
          this._onScrollKeyPressed(ev);

          handled = true;
        }

        break;

      case _Keyboard.Key.K:
        if (ctrlCmdOnly) {
          _dispatcher.default.dispatch({
            action: 'focus_room_filter'
          });

          handled = true;
        }

        break;

      case _Keyboard.Key.BACKTICK:
        // Ideally this would be CTRL+P for "Profile", but that's
        // taken by the print dialog. CTRL+I for "Information"
        // was previously chosen but conflicted with italics in
        // composer, so CTRL+` it is
        if (ctrlCmdOnly) {
          _dispatcher.default.dispatch({
            action: 'toggle_top_left_menu'
          });

          handled = true;
        }

        break;

      case _Keyboard.Key.SLASH:
        if ((0, _Keyboard.isOnlyCtrlOrCmdIgnoreShiftKeyEvent)(ev)) {
          KeyboardShortcuts.toggleDialog();
          handled = true;
        }

        break;

      case _Keyboard.Key.ARROW_UP:
      case _Keyboard.Key.ARROW_DOWN:
        if (ev.altKey && !ev.ctrlKey && !ev.metaKey) {
          _dispatcher.default.dispatch({
            action: 'view_room_delta',
            delta: ev.key === _Keyboard.Key.ARROW_UP ? -1 : 1,
            unread: ev.shiftKey
          });

          handled = true;
        }

        break;

      case _Keyboard.Key.PERIOD:
        if (ctrlCmdOnly && (this.props.page_type === "room_view" || this.props.page_type === "group_view")) {
          _dispatcher.default.dispatch({
            action: 'toggle_right_panel',
            type: this.props.page_type === "room_view" ? "room" : "group"
          });

          handled = true;
        }

    }

    if (handled) {
      ev.stopPropagation();
      ev.preventDefault();
    } else if (!isModifier && !ev.altKey && !ev.ctrlKey && !ev.metaKey) {
      // The above condition is crafted to _allow_ characters with Shift
      // already pressed (but not the Shift key down itself).
      const isClickShortcut = ev.target !== document.body && (ev.key === _Keyboard.Key.SPACE || ev.key === _Keyboard.Key.ENTER); // Do not capture the context menu key to improve keyboard accessibility

      if (ev.key === _Keyboard.Key.CONTEXT_MENU) {
        return;
      }

      if (!isClickShortcut && ev.key !== _Keyboard.Key.TAB && !canElementReceiveInput(ev.target)) {
        // synchronous dispatch so we focus before key generates input
        _dispatcher.default.dispatch({
          action: 'focus_composer'
        }, true);

        ev.stopPropagation(); // we should *not* preventDefault() here as
        // that would prevent typing in the now-focussed composer
      }
    }
  },

  /**
   * dispatch a page-up/page-down/etc to the appropriate component
   * @param {Object} ev The key event
   */
  _onScrollKeyPressed: function (ev) {
    if (this._roomView.current) {
      this._roomView.current.handleScrollKey(ev);
    }
  },
  _onDragEnd: function (result) {
    // Dragged to an invalid destination, not onto a droppable
    if (!result.destination) {
      return;
    }

    const dest = result.destination.droppableId;

    if (dest === 'tag-panel-droppable') {
      // Could be "GroupTile +groupId:domain"
      const draggableId = result.draggableId.split(' ').pop(); // Dispatch synchronously so that the TagPanel receives an
      // optimistic update from TagOrderStore before the previous
      // state is shown.

      _dispatcher.default.dispatch(_TagOrderActions.default.moveTag(this._matrixClient, draggableId, result.destination.index), true);
    } else if (dest.startsWith('room-sub-list-droppable_')) {
      this._onRoomTileEndDrag(result);
    }
  },
  _onRoomTileEndDrag: function (result) {
    let newTag = result.destination.droppableId.split('_')[1];
    let prevTag = result.source.droppableId.split('_')[1];
    if (newTag === 'undefined') newTag = undefined;
    if (prevTag === 'undefined') prevTag = undefined;
    const roomId = result.draggableId.split('_')[1];
    const oldIndex = result.source.index;
    const newIndex = result.destination.index;

    _dispatcher.default.dispatch(_RoomListActions.default.tagRoom(this._matrixClient, this._matrixClient.getRoom(roomId), prevTag, newTag, oldIndex, newIndex), true);
  },
  _onMouseDown: function (ev) {
    // When the panels are disabled, clicking on them results in a mouse event
    // which bubbles to certain elements in the tree. When this happens, close
    // any settings page that is currently open (user/room/group).
    if (this.props.leftDisabled && this.props.rightDisabled) {
      const targetClasses = new Set(ev.target.className.split(' '));

      if (targetClasses.has('mx_MatrixChat') || targetClasses.has('mx_MatrixChat_middlePanel') || targetClasses.has('mx_RoomView')) {
        this.setState({
          mouseDown: {
            x: ev.pageX,
            y: ev.pageY
          }
        });
      }
    }
  },
  _onMouseUp: function (ev) {
    if (!this.state.mouseDown) return;
    const deltaX = ev.pageX - this.state.mouseDown.x;
    const deltaY = ev.pageY - this.state.mouseDown.y;
    const distance = Math.sqrt(deltaX * deltaX + (deltaY + deltaY));
    const maxRadius = 5; // People shouldn't be straying too far, hopefully
    // Note: we track how far the user moved their mouse to help
    // combat against https://github.com/vector-im/riot-web/issues/7158

    if (distance < maxRadius) {
      // This is probably a real click, and not a drag
      _dispatcher.default.dispatch({
        action: 'close_settings'
      });
    } // Always clear the mouseDown state to ensure we don't accidentally
    // use stale values due to the mouseDown checks.


    this.setState({
      mouseDown: null
    });
  },

  _setResizeContainerRef(div) {
    this.resizeContainer = div;
  },

  render: function () {
    const LeftPanel = sdk.getComponent('structures.LeftPanel');
    const RoomView = sdk.getComponent('structures.RoomView');
    const UserView = sdk.getComponent('structures.UserView');
    const GroupView = sdk.getComponent('structures.GroupView');
    const MyGroups = sdk.getComponent('structures.MyGroups');
    const ToastContainer = sdk.getComponent('structures.ToastContainer');
    const MatrixToolbar = sdk.getComponent('globals.MatrixToolbar');
    const CookieBar = sdk.getComponent('globals.CookieBar');
    const NewVersionBar = sdk.getComponent('globals.NewVersionBar');
    const UpdateCheckBar = sdk.getComponent('globals.UpdateCheckBar');
    const PasswordNagBar = sdk.getComponent('globals.PasswordNagBar');
    const ServerLimitBar = sdk.getComponent('globals.ServerLimitBar');
    let pageElement;

    switch (this.props.page_type) {
      case _PageTypes.default.RoomView:
        pageElement = _react.default.createElement(RoomView, {
          ref: this._roomView,
          autoJoin: this.props.autoJoin,
          onRegistered: this.props.onRegistered,
          thirdPartyInvite: this.props.thirdPartyInvite,
          oobData: this.props.roomOobData,
          viaServers: this.props.viaServers,
          eventPixelOffset: this.props.initialEventPixelOffset,
          key: this.props.currentRoomId || 'roomview',
          disabled: this.props.middleDisabled,
          ConferenceHandler: this.props.ConferenceHandler,
          resizeNotifier: this.props.resizeNotifier
        });
        break;

      case _PageTypes.default.MyGroups:
        pageElement = _react.default.createElement(MyGroups, null);
        break;

      case _PageTypes.default.RoomDirectory:
        // handled by MatrixChat for now
        break;

      case _PageTypes.default.HomePage:
        pageElement = _react.default.createElement(_HomePage.default, null);
        break;

      case _PageTypes.default.UserView:
        pageElement = _react.default.createElement(UserView, {
          userId: this.props.currentUserId
        });
        break;

      case _PageTypes.default.GroupView:
        pageElement = _react.default.createElement(GroupView, {
          groupId: this.props.currentGroupId,
          isNew: this.props.currentGroupIsNew
        });
        break;
    }

    const usageLimitEvent = this.state.serverNoticeEvents.find(e => {
      return e && e.getType() === 'm.room.message' && e.getContent()['server_notice_type'] === 'm.server_notice.usage_limit_reached';
    });
    let topBar;

    if (this.state.syncErrorData && this.state.syncErrorData.error.errcode === 'M_RESOURCE_LIMIT_EXCEEDED') {
      topBar = _react.default.createElement(ServerLimitBar, {
        kind: "hard",
        adminContact: this.state.syncErrorData.error.data.admin_contact,
        limitType: this.state.syncErrorData.error.data.limit_type
      });
    } else if (usageLimitEvent) {
      topBar = _react.default.createElement(ServerLimitBar, {
        kind: "soft",
        adminContact: usageLimitEvent.getContent().admin_contact,
        limitType: usageLimitEvent.getContent().limit_type
      });
    } else if (this.props.showCookieBar && this.props.config.piwik && navigator.doNotTrack !== "1") {
      const policyUrl = this.props.config.piwik.policyUrl || null;
      topBar = _react.default.createElement(CookieBar, {
        policyUrl: policyUrl
      });
    } else if (this.props.hasNewVersion) {
      topBar = _react.default.createElement(NewVersionBar, {
        version: this.props.version,
        newVersion: this.props.newVersion,
        releaseNotes: this.props.newVersionReleaseNotes
      });
    } else if (this.props.checkingForUpdate) {
      topBar = _react.default.createElement(UpdateCheckBar, this.props.checkingForUpdate);
    } else if (this.state.userHasGeneratedPassword) {
      topBar = _react.default.createElement(PasswordNagBar, null);
    } else if (this.props.showNotifierToolbar) {
      topBar = _react.default.createElement(MatrixToolbar, null);
    }

    let bodyClasses = 'mx_MatrixChat';

    if (topBar) {
      bodyClasses += ' mx_MatrixChat_toolbarShowing';
    }

    if (this.state.useCompactLayout) {
      bodyClasses += ' mx_MatrixChat_useCompactLayout';
    }

    return _react.default.createElement(_MatrixClientContext.default.Provider, {
      value: this._matrixClient
    }, _react.default.createElement("div", {
      onPaste: this._onPaste,
      onKeyDown: this._onReactKeyDown,
      className: "mx_MatrixChat_wrapper",
      "aria-hidden": this.props.hideToSRUsers,
      onMouseDown: this._onMouseDown,
      onMouseUp: this._onMouseUp
    }, topBar, _react.default.createElement(ToastContainer, null), _react.default.createElement(_reactBeautifulDnd.DragDropContext, {
      onDragEnd: this._onDragEnd
    }, _react.default.createElement("div", {
      ref: this._setResizeContainerRef,
      className: bodyClasses
    }, _react.default.createElement(LeftPanel, {
      resizeNotifier: this.props.resizeNotifier,
      collapsed: this.props.collapseLhs || false,
      disabled: this.props.leftDisabled
    }), _react.default.createElement(_ResizeHandle.default, null), pageElement))));
  }
});
var _default = LoggedInView;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvTG9nZ2VkSW5WaWV3LmpzIl0sIm5hbWVzIjpbIk1BWF9QSU5ORURfTk9USUNFU19QRVJfUk9PTSIsImNhbkVsZW1lbnRSZWNlaXZlSW5wdXQiLCJlbCIsInRhZ05hbWUiLCJnZXRBdHRyaWJ1dGUiLCJMb2dnZWRJblZpZXciLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsIm1hdHJpeENsaWVudCIsIlByb3BUeXBlcyIsImluc3RhbmNlT2YiLCJNYXRyaXhDbGllbnQiLCJpc1JlcXVpcmVkIiwicGFnZV90eXBlIiwic3RyaW5nIiwib25Sb29tQ3JlYXRlZCIsImZ1bmMiLCJvblJlZ2lzdGVyZWQiLCJ2aWFTZXJ2ZXJzIiwiYXJyYXlPZiIsImdldEluaXRpYWxTdGF0ZSIsInVzZUNvbXBhY3RMYXlvdXQiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJzZXJ2ZXJOb3RpY2VFdmVudHMiLCJjb21wb25lbnREaWRNb3VudCIsInJlc2l6ZXIiLCJfY3JlYXRlUmVzaXplciIsImF0dGFjaCIsIl9sb2FkUmVzaXplclByZWZlcmVuY2VzIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIl9tYXRyaXhDbGllbnQiLCJwcm9wcyIsIkNhbGxNZWRpYUhhbmRsZXIiLCJsb2FkRGV2aWNlcyIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9vbk5hdGl2ZUtleURvd24iLCJfc2Vzc2lvblN0b3JlIiwic2Vzc2lvblN0b3JlIiwiX3Nlc3Npb25TdG9yZVRva2VuIiwiYWRkTGlzdGVuZXIiLCJfc2V0U3RhdGVGcm9tU2Vzc2lvblN0b3JlIiwiX3VwZGF0ZVNlcnZlck5vdGljZUV2ZW50cyIsIm9uIiwib25BY2NvdW50RGF0YSIsIm9uU3luYyIsIm9uUm9vbVN0YXRlRXZlbnRzIiwiX3Jvb21WaWV3IiwiY29tcG9uZW50RGlkVXBkYXRlIiwicHJldlByb3BzIiwic2hvd0Nvb2tpZUJhciIsImhhc05ld1ZlcnNpb24iLCJ1c2VySGFzR2VuZXJhdGVkUGFzc3dvcmQiLCJzaG93Tm90aWZpZXJUb29sYmFyIiwicmVzaXplTm90aWZpZXIiLCJub3RpZnlCYW5uZXJzQ2hhbmdlZCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsInJlbW92ZUxpc3RlbmVyIiwicmVtb3ZlIiwiZGV0YWNoIiwic2hvdWxkQ29tcG9uZW50VXBkYXRlIiwiQm9vbGVhbiIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImNhblJlc2V0VGltZWxpbmVJblJvb20iLCJyb29tSWQiLCJjdXJyZW50IiwiY2FuUmVzZXRUaW1lbGluZSIsInNldFN0YXRlIiwiZ2V0Q2FjaGVkUGFzc3dvcmQiLCJjbGFzc05hbWVzIiwiaGFuZGxlIiwidmVydGljYWwiLCJyZXZlcnNlIiwiY29sbGFwc2VDb25maWciLCJ0b2dnbGVTaXplIiwib25Db2xsYXBzZWQiLCJjb2xsYXBzZWQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsIndpbmRvdyIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJvblJlc2l6ZWQiLCJzaXplIiwibm90aWZ5TGVmdEhhbmRsZVJlc2l6ZWQiLCJSZXNpemVyIiwicmVzaXplQ29udGFpbmVyIiwiQ29sbGFwc2VEaXN0cmlidXRvciIsInNldENsYXNzTmFtZXMiLCJsaHNTaXplIiwiZ2V0SXRlbSIsInBhcnNlSW50IiwiZm9ySGFuZGxlQXQiLCJyZXNpemUiLCJldmVudCIsImdldFR5cGUiLCJnZXRDb250ZW50Iiwic3luY1N0YXRlIiwib2xkU3luY1N0YXRlIiwiZGF0YSIsIm9sZEVyckNvZGUiLCJzdGF0ZSIsInN5bmNFcnJvckRhdGEiLCJlcnJvciIsImVycmNvZGUiLCJuZXdFcnJDb2RlIiwiZXYiLCJyb29tTGlzdHMiLCJSb29tTGlzdFN0b3JlIiwiZ2V0Um9vbUxpc3RzIiwic29tZSIsInIiLCJnZXRSb29tSWQiLCJwaW5uZWRFdmVudHMiLCJyb29tIiwicGluU3RhdGVFdmVudCIsImN1cnJlbnRTdGF0ZSIsImdldFN0YXRlRXZlbnRzIiwicGlubmVkIiwicGlubmVkRXZlbnRJZHMiLCJzbGljZSIsImV2ZW50SWQiLCJ0aW1lbGluZSIsImdldEV2ZW50VGltZWxpbmUiLCJnZXRVbmZpbHRlcmVkVGltZWxpbmVTZXQiLCJnZXRFdmVudHMiLCJmaW5kIiwiZ2V0SWQiLCJwdXNoIiwiX29uUGFzdGUiLCJjYW5SZWNlaXZlSW5wdXQiLCJlbGVtZW50IiwidGFyZ2V0IiwicGFyZW50RWxlbWVudCIsIl9vblJlYWN0S2V5RG93biIsIl9vbktleURvd24iLCJib2R5IiwiaGFuZGxlZCIsImN0cmxDbWRPbmx5IiwiaGFzTW9kaWZpZXIiLCJhbHRLZXkiLCJjdHJsS2V5IiwibWV0YUtleSIsInNoaWZ0S2V5IiwiaXNNb2RpZmllciIsImtleSIsIktleSIsIkFMVCIsIkNPTlRST0wiLCJNRVRBIiwiU0hJRlQiLCJQQUdFX1VQIiwiUEFHRV9ET1dOIiwiX29uU2Nyb2xsS2V5UHJlc3NlZCIsIkhPTUUiLCJFTkQiLCJLIiwiQkFDS1RJQ0siLCJTTEFTSCIsIktleWJvYXJkU2hvcnRjdXRzIiwidG9nZ2xlRGlhbG9nIiwiQVJST1dfVVAiLCJBUlJPV19ET1dOIiwiZGVsdGEiLCJ1bnJlYWQiLCJQRVJJT0QiLCJ0eXBlIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJpc0NsaWNrU2hvcnRjdXQiLCJTUEFDRSIsIkVOVEVSIiwiQ09OVEVYVF9NRU5VIiwiVEFCIiwiaGFuZGxlU2Nyb2xsS2V5IiwiX29uRHJhZ0VuZCIsInJlc3VsdCIsImRlc3RpbmF0aW9uIiwiZGVzdCIsImRyb3BwYWJsZUlkIiwiZHJhZ2dhYmxlSWQiLCJzcGxpdCIsInBvcCIsIlRhZ09yZGVyQWN0aW9ucyIsIm1vdmVUYWciLCJpbmRleCIsInN0YXJ0c1dpdGgiLCJfb25Sb29tVGlsZUVuZERyYWciLCJuZXdUYWciLCJwcmV2VGFnIiwic291cmNlIiwidW5kZWZpbmVkIiwib2xkSW5kZXgiLCJuZXdJbmRleCIsIlJvb21MaXN0QWN0aW9ucyIsInRhZ1Jvb20iLCJnZXRSb29tIiwiX29uTW91c2VEb3duIiwibGVmdERpc2FibGVkIiwicmlnaHREaXNhYmxlZCIsInRhcmdldENsYXNzZXMiLCJTZXQiLCJjbGFzc05hbWUiLCJoYXMiLCJtb3VzZURvd24iLCJ4IiwicGFnZVgiLCJ5IiwicGFnZVkiLCJfb25Nb3VzZVVwIiwiZGVsdGFYIiwiZGVsdGFZIiwiZGlzdGFuY2UiLCJNYXRoIiwic3FydCIsIm1heFJhZGl1cyIsIl9zZXRSZXNpemVDb250YWluZXJSZWYiLCJkaXYiLCJyZW5kZXIiLCJMZWZ0UGFuZWwiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJSb29tVmlldyIsIlVzZXJWaWV3IiwiR3JvdXBWaWV3IiwiTXlHcm91cHMiLCJUb2FzdENvbnRhaW5lciIsIk1hdHJpeFRvb2xiYXIiLCJDb29raWVCYXIiLCJOZXdWZXJzaW9uQmFyIiwiVXBkYXRlQ2hlY2tCYXIiLCJQYXNzd29yZE5hZ0JhciIsIlNlcnZlckxpbWl0QmFyIiwicGFnZUVsZW1lbnQiLCJQYWdlVHlwZXMiLCJhdXRvSm9pbiIsInRoaXJkUGFydHlJbnZpdGUiLCJyb29tT29iRGF0YSIsImluaXRpYWxFdmVudFBpeGVsT2Zmc2V0IiwiY3VycmVudFJvb21JZCIsIm1pZGRsZURpc2FibGVkIiwiQ29uZmVyZW5jZUhhbmRsZXIiLCJSb29tRGlyZWN0b3J5IiwiSG9tZVBhZ2UiLCJjdXJyZW50VXNlcklkIiwiY3VycmVudEdyb3VwSWQiLCJjdXJyZW50R3JvdXBJc05ldyIsInVzYWdlTGltaXRFdmVudCIsImUiLCJ0b3BCYXIiLCJhZG1pbl9jb250YWN0IiwibGltaXRfdHlwZSIsImNvbmZpZyIsInBpd2lrIiwibmF2aWdhdG9yIiwiZG9Ob3RUcmFjayIsInBvbGljeVVybCIsInZlcnNpb24iLCJuZXdWZXJzaW9uIiwibmV3VmVyc2lvblJlbGVhc2VOb3RlcyIsImNoZWNraW5nRm9yVXBkYXRlIiwiYm9keUNsYXNzZXMiLCJoaWRlVG9TUlVzZXJzIiwiY29sbGFwc2VMaHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXpDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTUEsMkJBQTJCLEdBQUcsQ0FBcEM7O0FBRUEsU0FBU0Msc0JBQVQsQ0FBZ0NDLEVBQWhDLEVBQW9DO0FBQ2hDLFNBQU9BLEVBQUUsQ0FBQ0MsT0FBSCxLQUFlLE9BQWYsSUFDSEQsRUFBRSxDQUFDQyxPQUFILEtBQWUsVUFEWixJQUVIRCxFQUFFLENBQUNDLE9BQUgsS0FBZSxRQUZaLElBR0gsQ0FBQyxDQUFDRCxFQUFFLENBQUNFLFlBQUgsQ0FBZ0IsaUJBQWhCLENBSE47QUFJSDtBQUVEOzs7Ozs7Ozs7OztBQVNBLE1BQU1DLFlBQVksR0FBRywrQkFBaUI7QUFDbENDLEVBQUFBLFdBQVcsRUFBRSxjQURxQjtBQUdsQ0MsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLFlBQVksRUFBRUMsbUJBQVVDLFVBQVYsQ0FBcUJDLHlCQUFyQixFQUFtQ0MsVUFEMUM7QUFFUEMsSUFBQUEsU0FBUyxFQUFFSixtQkFBVUssTUFBVixDQUFpQkYsVUFGckI7QUFHUEcsSUFBQUEsYUFBYSxFQUFFTixtQkFBVU8sSUFIbEI7QUFLUDtBQUNBO0FBQ0FDLElBQUFBLFlBQVksRUFBRVIsbUJBQVVPLElBUGpCO0FBU1A7QUFDQUUsSUFBQUEsVUFBVSxFQUFFVCxtQkFBVVUsT0FBVixDQUFrQlYsbUJBQVVLLE1BQTVCLENBVkwsQ0FZUDs7QUFaTyxHQUh1QjtBQWtCbENNLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSDtBQUNBQyxNQUFBQSxnQkFBZ0IsRUFBRUMsdUJBQWNDLFFBQWQsQ0FBdUIsa0JBQXZCLENBRmY7QUFHSDtBQUNBQyxNQUFBQSxrQkFBa0IsRUFBRTtBQUpqQixLQUFQO0FBTUgsR0F6QmlDO0FBMkJsQ0MsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLQyxPQUFMLEdBQWUsS0FBS0MsY0FBTCxFQUFmO0FBQ0EsU0FBS0QsT0FBTCxDQUFhRSxNQUFiOztBQUNBLFNBQUtDLHVCQUFMO0FBQ0gsR0EvQmlDO0FBaUNsQztBQUNBQyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixLQUFLQyxLQUFMLENBQVd4QixZQUFoQzs7QUFFQXlCLDhCQUFpQkMsV0FBakI7O0FBRUFDLElBQUFBLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBcUMsS0FBS0MsZ0JBQTFDLEVBQTRELEtBQTVEO0FBRUEsU0FBS0MsYUFBTCxHQUFxQkMscUJBQXJCO0FBQ0EsU0FBS0Msa0JBQUwsR0FBMEIsS0FBS0YsYUFBTCxDQUFtQkcsV0FBbkIsQ0FDdEIsS0FBS0MseUJBRGlCLENBQTFCOztBQUdBLFNBQUtBLHlCQUFMOztBQUVBLFNBQUtDLHlCQUFMOztBQUVBLFNBQUtaLGFBQUwsQ0FBbUJhLEVBQW5CLENBQXNCLGFBQXRCLEVBQXFDLEtBQUtDLGFBQTFDOztBQUNBLFNBQUtkLGFBQUwsQ0FBbUJhLEVBQW5CLENBQXNCLE1BQXRCLEVBQThCLEtBQUtFLE1BQW5DOztBQUNBLFNBQUtmLGFBQUwsQ0FBbUJhLEVBQW5CLENBQXNCLGtCQUF0QixFQUEwQyxLQUFLRyxpQkFBL0M7O0FBRUE7QUFFQSxTQUFLQyxTQUFMLEdBQWlCLHVCQUFqQjtBQUNILEdBekRpQzs7QUEyRGxDQyxFQUFBQSxrQkFBa0IsQ0FBQ0MsU0FBRCxFQUFZO0FBQzFCO0FBQ0EsUUFDS0EsU0FBUyxDQUFDQyxhQUFWLEtBQTRCLEtBQUtuQixLQUFMLENBQVdtQixhQUF4QyxJQUNDRCxTQUFTLENBQUNFLGFBQVYsS0FBNEIsS0FBS3BCLEtBQUwsQ0FBV29CLGFBRHhDLElBRUNGLFNBQVMsQ0FBQ0csd0JBQVYsS0FBdUMsS0FBS3JCLEtBQUwsQ0FBV3FCLHdCQUZuRCxJQUdDSCxTQUFTLENBQUNJLG1CQUFWLEtBQWtDLEtBQUt0QixLQUFMLENBQVdzQixtQkFKbEQsRUFLRTtBQUNFLFdBQUt0QixLQUFMLENBQVd1QixjQUFYLENBQTBCQyxvQkFBMUI7QUFDSDtBQUNKLEdBckVpQzs7QUF1RWxDQyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCdEIsSUFBQUEsUUFBUSxDQUFDdUIsbUJBQVQsQ0FBNkIsU0FBN0IsRUFBd0MsS0FBS3JCLGdCQUE3QyxFQUErRCxLQUEvRDs7QUFDQSxTQUFLTixhQUFMLENBQW1CNEIsY0FBbkIsQ0FBa0MsYUFBbEMsRUFBaUQsS0FBS2QsYUFBdEQ7O0FBQ0EsU0FBS2QsYUFBTCxDQUFtQjRCLGNBQW5CLENBQWtDLE1BQWxDLEVBQTBDLEtBQUtiLE1BQS9DOztBQUNBLFNBQUtmLGFBQUwsQ0FBbUI0QixjQUFuQixDQUFrQyxrQkFBbEMsRUFBc0QsS0FBS1osaUJBQTNEOztBQUNBLFFBQUksS0FBS1Asa0JBQVQsRUFBNkI7QUFDekIsV0FBS0Esa0JBQUwsQ0FBd0JvQixNQUF4QjtBQUNIOztBQUNELFNBQUtsQyxPQUFMLENBQWFtQyxNQUFiO0FBQ0gsR0FoRmlDO0FBa0ZsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixXQUFPQyxPQUFPLENBQUNDLGlDQUFnQkMsR0FBaEIsRUFBRCxDQUFkO0FBQ0gsR0ExRmlDO0FBNEZsQ0MsRUFBQUEsc0JBQXNCLEVBQUUsVUFBU0MsTUFBVCxFQUFpQjtBQUNyQyxRQUFJLENBQUMsS0FBS25CLFNBQUwsQ0FBZW9CLE9BQXBCLEVBQTZCO0FBQ3pCLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBS3BCLFNBQUwsQ0FBZW9CLE9BQWYsQ0FBdUJDLGdCQUF2QixFQUFQO0FBQ0gsR0FqR2lDOztBQW1HbEMzQixFQUFBQSx5QkFBeUIsR0FBRztBQUN4QixTQUFLNEIsUUFBTCxDQUFjO0FBQ1ZqQixNQUFBQSx3QkFBd0IsRUFBRVUsT0FBTyxDQUFDLEtBQUt6QixhQUFMLENBQW1CaUMsaUJBQW5CLEVBQUQ7QUFEdkIsS0FBZDtBQUdILEdBdkdpQzs7QUF5R2xDNUMsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsVUFBTTZDLFVBQVUsR0FBRztBQUNmQyxNQUFBQSxNQUFNLEVBQUUsaUJBRE87QUFFZkMsTUFBQUEsUUFBUSxFQUFFLDBCQUZLO0FBR2ZDLE1BQUFBLE9BQU8sRUFBRTtBQUhNLEtBQW5CO0FBS0EsVUFBTUMsY0FBYyxHQUFHO0FBQ25CQyxNQUFBQSxVQUFVLEVBQUUsTUFBTSxFQURDO0FBRW5CQyxNQUFBQSxXQUFXLEVBQUdDLFNBQUQsSUFBZTtBQUN4QixZQUFJQSxTQUFKLEVBQWU7QUFDWEMsOEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxZQUFBQSxNQUFNLEVBQUU7QUFBVCxXQUFiLEVBQTBDLElBQTFDOztBQUNBQyxVQUFBQSxNQUFNLENBQUNDLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCLGFBQTVCLEVBQTJDLEdBQTNDO0FBQ0gsU0FIRCxNQUdPO0FBQ0hMLDhCQUFJQyxRQUFKLENBQWE7QUFBQ0MsWUFBQUEsTUFBTSxFQUFFO0FBQVQsV0FBYixFQUEwQyxJQUExQztBQUNIO0FBQ0osT0FUa0I7QUFVbkJJLE1BQUFBLFNBQVMsRUFBR0MsSUFBRCxJQUFVO0FBQ2pCSixRQUFBQSxNQUFNLENBQUNDLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCLGFBQTVCLEVBQTJDLEtBQUtFLElBQWhEO0FBQ0EsYUFBS3ZELEtBQUwsQ0FBV3VCLGNBQVgsQ0FBMEJpQyx1QkFBMUI7QUFDSDtBQWJrQixLQUF2QjtBQWVBLFVBQU05RCxPQUFPLEdBQUcsSUFBSStELGdCQUFKLENBQ1osS0FBS0MsZUFETyxFQUVaQyw0QkFGWSxFQUdaZixjQUhZLENBQWhCO0FBSUFsRCxJQUFBQSxPQUFPLENBQUNrRSxhQUFSLENBQXNCcEIsVUFBdEI7QUFDQSxXQUFPOUMsT0FBUDtBQUNILEdBcElpQzs7QUFzSWxDRyxFQUFBQSx1QkFBdUIsR0FBRztBQUN0QixRQUFJZ0UsT0FBTyxHQUFHVixNQUFNLENBQUNDLFlBQVAsQ0FBb0JVLE9BQXBCLENBQTRCLGFBQTVCLENBQWQ7O0FBQ0EsUUFBSUQsT0FBTyxLQUFLLElBQWhCLEVBQXNCO0FBQ2xCQSxNQUFBQSxPQUFPLEdBQUdFLFFBQVEsQ0FBQ0YsT0FBRCxFQUFVLEVBQVYsQ0FBbEI7QUFDSCxLQUZELE1BRU87QUFDSEEsTUFBQUEsT0FBTyxHQUFHLEdBQVY7QUFDSDs7QUFDRCxTQUFLbkUsT0FBTCxDQUFhc0UsV0FBYixDQUF5QixDQUF6QixFQUE0QkMsTUFBNUIsQ0FBbUNKLE9BQW5DO0FBQ0gsR0E5SWlDOztBQWdKbENoRCxFQUFBQSxhQUFhLEVBQUUsVUFBU3FELEtBQVQsRUFBZ0I7QUFDM0IsUUFBSUEsS0FBSyxDQUFDQyxPQUFOLE9BQW9CLHdCQUF4QixFQUFrRDtBQUM5QyxXQUFLN0IsUUFBTCxDQUFjO0FBQ1ZqRCxRQUFBQSxnQkFBZ0IsRUFBRTZFLEtBQUssQ0FBQ0UsVUFBTixHQUFtQi9FO0FBRDNCLE9BQWQ7QUFHSDs7QUFDRCxRQUFJNkUsS0FBSyxDQUFDQyxPQUFOLE9BQW9CLHFCQUF4QixFQUErQztBQUMzQ25CLDBCQUFJQyxRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjtBQUNIO0FBQ0osR0F6SmlDO0FBMkpsQ3BDLEVBQUFBLE1BQU0sRUFBRSxVQUFTdUQsU0FBVCxFQUFvQkMsWUFBcEIsRUFBa0NDLElBQWxDLEVBQXdDO0FBQzVDLFVBQU1DLFVBQVUsR0FDWixLQUFLQyxLQUFMLENBQVdDLGFBQVgsSUFDQSxLQUFLRCxLQUFMLENBQVdDLGFBQVgsQ0FBeUJDLEtBRHpCLElBRUEsS0FBS0YsS0FBTCxDQUFXQyxhQUFYLENBQXlCQyxLQUF6QixDQUErQkMsT0FIbkM7QUFLQSxVQUFNQyxVQUFVLEdBQUdOLElBQUksSUFBSUEsSUFBSSxDQUFDSSxLQUFiLElBQXNCSixJQUFJLENBQUNJLEtBQUwsQ0FBV0MsT0FBcEQ7QUFDQSxRQUFJUCxTQUFTLEtBQUtDLFlBQWQsSUFBOEJFLFVBQVUsS0FBS0ssVUFBakQsRUFBNkQ7O0FBRTdELFFBQUlSLFNBQVMsS0FBSyxPQUFsQixFQUEyQjtBQUN2QixXQUFLL0IsUUFBTCxDQUFjO0FBQ1ZvQyxRQUFBQSxhQUFhLEVBQUVIO0FBREwsT0FBZDtBQUdILEtBSkQsTUFJTztBQUNILFdBQUtqQyxRQUFMLENBQWM7QUFDVm9DLFFBQUFBLGFBQWEsRUFBRTtBQURMLE9BQWQ7QUFHSDs7QUFFRCxRQUFJSixZQUFZLEtBQUssVUFBakIsSUFBK0JELFNBQVMsS0FBSyxTQUFqRCxFQUE0RDtBQUN4RCxXQUFLMUQseUJBQUw7QUFDSDtBQUNKLEdBakxpQztBQW1MbENJLEVBQUFBLGlCQUFpQixFQUFFLFVBQVMrRCxFQUFULEVBQWFMLEtBQWIsRUFBb0I7QUFDbkMsVUFBTU0sU0FBUyxHQUFHQyx1QkFBY0MsWUFBZCxFQUFsQjs7QUFDQSxRQUFJRixTQUFTLENBQUMsaUJBQUQsQ0FBVCxJQUFnQ0EsU0FBUyxDQUFDLGlCQUFELENBQVQsQ0FBNkJHLElBQTdCLENBQWtDQyxDQUFDLElBQUlBLENBQUMsQ0FBQ2hELE1BQUYsS0FBYTJDLEVBQUUsQ0FBQ00sU0FBSCxFQUFwRCxDQUFwQyxFQUF5RztBQUNyRyxXQUFLekUseUJBQUw7QUFDSDtBQUNKLEdBeExpQztBQTBMbENBLEVBQUFBLHlCQUF5QixFQUFFLGtCQUFpQjtBQUN4QyxVQUFNb0UsU0FBUyxHQUFHQyx1QkFBY0MsWUFBZCxFQUFsQjs7QUFDQSxRQUFJLENBQUNGLFNBQVMsQ0FBQyxpQkFBRCxDQUFkLEVBQW1DLE9BQU8sRUFBUDtBQUVuQyxVQUFNTSxZQUFZLEdBQUcsRUFBckI7O0FBQ0EsU0FBSyxNQUFNQyxJQUFYLElBQW1CUCxTQUFTLENBQUMsaUJBQUQsQ0FBNUIsRUFBaUQ7QUFDN0MsWUFBTVEsYUFBYSxHQUFHRCxJQUFJLENBQUNFLFlBQUwsQ0FBa0JDLGNBQWxCLENBQWlDLHNCQUFqQyxFQUF5RCxFQUF6RCxDQUF0QjtBQUVBLFVBQUksQ0FBQ0YsYUFBRCxJQUFrQixDQUFDQSxhQUFhLENBQUNuQixVQUFkLEdBQTJCc0IsTUFBbEQsRUFBMEQ7QUFFMUQsWUFBTUMsY0FBYyxHQUFHSixhQUFhLENBQUNuQixVQUFkLEdBQTJCc0IsTUFBM0IsQ0FBa0NFLEtBQWxDLENBQXdDLENBQXhDLEVBQTJDNUgsMkJBQTNDLENBQXZCOztBQUNBLFdBQUssTUFBTTZILE9BQVgsSUFBc0JGLGNBQXRCLEVBQXNDO0FBQ2xDLGNBQU1HLFFBQVEsR0FBRyxNQUFNLEtBQUsvRixhQUFMLENBQW1CZ0csZ0JBQW5CLENBQW9DVCxJQUFJLENBQUNVLHdCQUFMLEVBQXBDLEVBQXFFSCxPQUFyRSxFQUE4RSxDQUE5RSxDQUF2QjtBQUNBLGNBQU1mLEVBQUUsR0FBR2dCLFFBQVEsQ0FBQ0csU0FBVCxHQUFxQkMsSUFBckIsQ0FBMEJwQixFQUFFLElBQUlBLEVBQUUsQ0FBQ3FCLEtBQUgsT0FBZU4sT0FBL0MsQ0FBWDtBQUNBLFlBQUlmLEVBQUosRUFBUU8sWUFBWSxDQUFDZSxJQUFiLENBQWtCdEIsRUFBbEI7QUFDWDtBQUNKOztBQUNELFNBQUt4QyxRQUFMLENBQWM7QUFDVjlDLE1BQUFBLGtCQUFrQixFQUFFNkY7QUFEVixLQUFkO0FBR0gsR0E5TWlDO0FBZ05sQ2dCLEVBQUFBLFFBQVEsRUFBRSxVQUFTdkIsRUFBVCxFQUFhO0FBQ25CLFFBQUl3QixlQUFlLEdBQUcsS0FBdEI7QUFDQSxRQUFJQyxPQUFPLEdBQUd6QixFQUFFLENBQUMwQixNQUFqQixDQUZtQixDQUduQjs7QUFDQSxXQUFPLENBQUNGLGVBQUQsSUFBb0JDLE9BQTNCLEVBQW9DO0FBQ2hDRCxNQUFBQSxlQUFlLEdBQUdySSxzQkFBc0IsQ0FBQ3NJLE9BQUQsQ0FBeEM7QUFDQUEsTUFBQUEsT0FBTyxHQUFHQSxPQUFPLENBQUNFLGFBQWxCO0FBQ0g7O0FBQ0QsUUFBSSxDQUFDSCxlQUFMLEVBQXNCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBdEQsMEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiLEVBQXlDLElBQXpDO0FBQ0g7QUFDSixHQTlOaUM7O0FBZ09sQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBc0JBd0QsRUFBQUEsZUFBZSxFQUFFLFVBQVM1QixFQUFULEVBQWE7QUFDMUI7QUFDQTtBQUNBLFNBQUs2QixVQUFMLENBQWdCN0IsRUFBaEI7QUFDSCxHQTFQaUM7QUE0UGxDekUsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU3lFLEVBQVQsRUFBYTtBQUMzQjtBQUNBO0FBQ0E7QUFDQSxRQUFJQSxFQUFFLENBQUMwQixNQUFILEtBQWNyRyxRQUFRLENBQUN5RyxJQUEzQixFQUFpQztBQUM3QixXQUFLRCxVQUFMLENBQWdCN0IsRUFBaEI7QUFDSDtBQUNKLEdBblFpQztBQXFRbEM2QixFQUFBQSxVQUFVLEVBQUUsVUFBUzdCLEVBQVQsRUFBYTtBQUNqQjs7Ozs7Ozs7Ozs7OztBQWNKLFFBQUkrQixPQUFPLEdBQUcsS0FBZDtBQUNBLFVBQU1DLFdBQVcsR0FBRyx1Q0FBd0JoQyxFQUF4QixDQUFwQjtBQUNBLFVBQU1pQyxXQUFXLEdBQUdqQyxFQUFFLENBQUNrQyxNQUFILElBQWFsQyxFQUFFLENBQUNtQyxPQUFoQixJQUEyQm5DLEVBQUUsQ0FBQ29DLE9BQTlCLElBQXlDcEMsRUFBRSxDQUFDcUMsUUFBaEU7QUFDQSxVQUFNQyxVQUFVLEdBQUd0QyxFQUFFLENBQUN1QyxHQUFILEtBQVdDLGNBQUlDLEdBQWYsSUFBc0J6QyxFQUFFLENBQUN1QyxHQUFILEtBQVdDLGNBQUlFLE9BQXJDLElBQWdEMUMsRUFBRSxDQUFDdUMsR0FBSCxLQUFXQyxjQUFJRyxJQUEvRCxJQUF1RTNDLEVBQUUsQ0FBQ3VDLEdBQUgsS0FBV0MsY0FBSUksS0FBekc7O0FBRUEsWUFBUTVDLEVBQUUsQ0FBQ3VDLEdBQVg7QUFDSSxXQUFLQyxjQUFJSyxPQUFUO0FBQ0EsV0FBS0wsY0FBSU0sU0FBVDtBQUNJLFlBQUksQ0FBQ2IsV0FBRCxJQUFnQixDQUFDSyxVQUFyQixFQUFpQztBQUM3QixlQUFLUyxtQkFBTCxDQUF5Qi9DLEVBQXpCOztBQUNBK0IsVUFBQUEsT0FBTyxHQUFHLElBQVY7QUFDSDs7QUFDRDs7QUFFSixXQUFLUyxjQUFJUSxJQUFUO0FBQ0EsV0FBS1IsY0FBSVMsR0FBVDtBQUNJLFlBQUlqRCxFQUFFLENBQUNtQyxPQUFILElBQWMsQ0FBQ25DLEVBQUUsQ0FBQ3FDLFFBQWxCLElBQThCLENBQUNyQyxFQUFFLENBQUNrQyxNQUFsQyxJQUE0QyxDQUFDbEMsRUFBRSxDQUFDb0MsT0FBcEQsRUFBNkQ7QUFDekQsZUFBS1csbUJBQUwsQ0FBeUIvQyxFQUF6Qjs7QUFDQStCLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBQ0Q7O0FBQ0osV0FBS1MsY0FBSVUsQ0FBVDtBQUNJLFlBQUlsQixXQUFKLEVBQWlCO0FBQ2I5RCw4QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFlBQUFBLE1BQU0sRUFBRTtBQURDLFdBQWI7O0FBR0EyRCxVQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEOztBQUNKLFdBQUtTLGNBQUlXLFFBQVQ7QUFDSTtBQUNBO0FBQ0E7QUFDQTtBQUVBLFlBQUluQixXQUFKLEVBQWlCO0FBQ2I5RCw4QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFlBQUFBLE1BQU0sRUFBRTtBQURDLFdBQWI7O0FBR0EyRCxVQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEOztBQUVKLFdBQUtTLGNBQUlZLEtBQVQ7QUFDSSxZQUFJLGtEQUFtQ3BELEVBQW5DLENBQUosRUFBNEM7QUFDeENxRCxVQUFBQSxpQkFBaUIsQ0FBQ0MsWUFBbEI7QUFDQXZCLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBQ0Q7O0FBRUosV0FBS1MsY0FBSWUsUUFBVDtBQUNBLFdBQUtmLGNBQUlnQixVQUFUO0FBQ0ksWUFBSXhELEVBQUUsQ0FBQ2tDLE1BQUgsSUFBYSxDQUFDbEMsRUFBRSxDQUFDbUMsT0FBakIsSUFBNEIsQ0FBQ25DLEVBQUUsQ0FBQ29DLE9BQXBDLEVBQTZDO0FBQ3pDbEUsOEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxZQUFBQSxNQUFNLEVBQUUsaUJBREM7QUFFVHFGLFlBQUFBLEtBQUssRUFBRXpELEVBQUUsQ0FBQ3VDLEdBQUgsS0FBV0MsY0FBSWUsUUFBZixHQUEwQixDQUFDLENBQTNCLEdBQStCLENBRjdCO0FBR1RHLFlBQUFBLE1BQU0sRUFBRTFELEVBQUUsQ0FBQ3FDO0FBSEYsV0FBYjs7QUFLQU4sVUFBQUEsT0FBTyxHQUFHLElBQVY7QUFDSDs7QUFDRDs7QUFFSixXQUFLUyxjQUFJbUIsTUFBVDtBQUNJLFlBQUkzQixXQUFXLEtBQUssS0FBSzlHLEtBQUwsQ0FBV25CLFNBQVgsS0FBeUIsV0FBekIsSUFBd0MsS0FBS21CLEtBQUwsQ0FBV25CLFNBQVgsS0FBeUIsWUFBdEUsQ0FBZixFQUFvRztBQUNoR21FLDhCQUFJQyxRQUFKLENBQWE7QUFDVEMsWUFBQUEsTUFBTSxFQUFFLG9CQURDO0FBRVR3RixZQUFBQSxJQUFJLEVBQUUsS0FBSzFJLEtBQUwsQ0FBV25CLFNBQVgsS0FBeUIsV0FBekIsR0FBdUMsTUFBdkMsR0FBZ0Q7QUFGN0MsV0FBYjs7QUFJQWdJLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBaEVUOztBQW1FQSxRQUFJQSxPQUFKLEVBQWE7QUFDVC9CLE1BQUFBLEVBQUUsQ0FBQzZELGVBQUg7QUFDQTdELE1BQUFBLEVBQUUsQ0FBQzhELGNBQUg7QUFDSCxLQUhELE1BR08sSUFBSSxDQUFDeEIsVUFBRCxJQUFlLENBQUN0QyxFQUFFLENBQUNrQyxNQUFuQixJQUE2QixDQUFDbEMsRUFBRSxDQUFDbUMsT0FBakMsSUFBNEMsQ0FBQ25DLEVBQUUsQ0FBQ29DLE9BQXBELEVBQTZEO0FBQ2hFO0FBQ0E7QUFFQSxZQUFNMkIsZUFBZSxHQUFHL0QsRUFBRSxDQUFDMEIsTUFBSCxLQUFjckcsUUFBUSxDQUFDeUcsSUFBdkIsS0FDbkI5QixFQUFFLENBQUN1QyxHQUFILEtBQVdDLGNBQUl3QixLQUFmLElBQXdCaEUsRUFBRSxDQUFDdUMsR0FBSCxLQUFXQyxjQUFJeUIsS0FEcEIsQ0FBeEIsQ0FKZ0UsQ0FPaEU7O0FBQ0EsVUFBSWpFLEVBQUUsQ0FBQ3VDLEdBQUgsS0FBV0MsY0FBSTBCLFlBQW5CLEVBQWlDO0FBQzdCO0FBQ0g7O0FBRUQsVUFBSSxDQUFDSCxlQUFELElBQW9CL0QsRUFBRSxDQUFDdUMsR0FBSCxLQUFXQyxjQUFJMkIsR0FBbkMsSUFBMEMsQ0FBQ2hMLHNCQUFzQixDQUFDNkcsRUFBRSxDQUFDMEIsTUFBSixDQUFyRSxFQUFrRjtBQUM5RTtBQUNBeEQsNEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiLEVBQXlDLElBQXpDOztBQUNBNEIsUUFBQUEsRUFBRSxDQUFDNkQsZUFBSCxHQUg4RSxDQUk5RTtBQUNBO0FBQ0g7QUFDSjtBQUNKLEdBblhpQzs7QUFxWGxDOzs7O0FBSUFkLEVBQUFBLG1CQUFtQixFQUFFLFVBQVMvQyxFQUFULEVBQWE7QUFDOUIsUUFBSSxLQUFLOUQsU0FBTCxDQUFlb0IsT0FBbkIsRUFBNEI7QUFDeEIsV0FBS3BCLFNBQUwsQ0FBZW9CLE9BQWYsQ0FBdUI4RyxlQUF2QixDQUF1Q3BFLEVBQXZDO0FBQ0g7QUFDSixHQTdYaUM7QUErWGxDcUUsRUFBQUEsVUFBVSxFQUFFLFVBQVNDLE1BQVQsRUFBaUI7QUFDekI7QUFDQSxRQUFJLENBQUNBLE1BQU0sQ0FBQ0MsV0FBWixFQUF5QjtBQUNyQjtBQUNIOztBQUVELFVBQU1DLElBQUksR0FBR0YsTUFBTSxDQUFDQyxXQUFQLENBQW1CRSxXQUFoQzs7QUFFQSxRQUFJRCxJQUFJLEtBQUsscUJBQWIsRUFBb0M7QUFDaEM7QUFDQSxZQUFNRSxXQUFXLEdBQUdKLE1BQU0sQ0FBQ0ksV0FBUCxDQUFtQkMsS0FBbkIsQ0FBeUIsR0FBekIsRUFBOEJDLEdBQTlCLEVBQXBCLENBRmdDLENBSWhDO0FBQ0E7QUFDQTs7QUFDQTFHLDBCQUFJQyxRQUFKLENBQWEwRyx5QkFBZ0JDLE9BQWhCLENBQ1QsS0FBSzdKLGFBREksRUFFVHlKLFdBRlMsRUFHVEosTUFBTSxDQUFDQyxXQUFQLENBQW1CUSxLQUhWLENBQWIsRUFJRyxJQUpIO0FBS0gsS0FaRCxNQVlPLElBQUlQLElBQUksQ0FBQ1EsVUFBTCxDQUFnQiwwQkFBaEIsQ0FBSixFQUFpRDtBQUNwRCxXQUFLQyxrQkFBTCxDQUF3QlgsTUFBeEI7QUFDSDtBQUNKLEdBdFppQztBQXdabENXLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNYLE1BQVQsRUFBaUI7QUFDakMsUUFBSVksTUFBTSxHQUFHWixNQUFNLENBQUNDLFdBQVAsQ0FBbUJFLFdBQW5CLENBQStCRSxLQUEvQixDQUFxQyxHQUFyQyxFQUEwQyxDQUExQyxDQUFiO0FBQ0EsUUFBSVEsT0FBTyxHQUFHYixNQUFNLENBQUNjLE1BQVAsQ0FBY1gsV0FBZCxDQUEwQkUsS0FBMUIsQ0FBZ0MsR0FBaEMsRUFBcUMsQ0FBckMsQ0FBZDtBQUNBLFFBQUlPLE1BQU0sS0FBSyxXQUFmLEVBQTRCQSxNQUFNLEdBQUdHLFNBQVQ7QUFDNUIsUUFBSUYsT0FBTyxLQUFLLFdBQWhCLEVBQTZCQSxPQUFPLEdBQUdFLFNBQVY7QUFFN0IsVUFBTWhJLE1BQU0sR0FBR2lILE1BQU0sQ0FBQ0ksV0FBUCxDQUFtQkMsS0FBbkIsQ0FBeUIsR0FBekIsRUFBOEIsQ0FBOUIsQ0FBZjtBQUVBLFVBQU1XLFFBQVEsR0FBR2hCLE1BQU0sQ0FBQ2MsTUFBUCxDQUFjTCxLQUEvQjtBQUNBLFVBQU1RLFFBQVEsR0FBR2pCLE1BQU0sQ0FBQ0MsV0FBUCxDQUFtQlEsS0FBcEM7O0FBRUE3Ryx3QkFBSUMsUUFBSixDQUFhcUgseUJBQWdCQyxPQUFoQixDQUNULEtBQUt4SyxhQURJLEVBRVQsS0FBS0EsYUFBTCxDQUFtQnlLLE9BQW5CLENBQTJCckksTUFBM0IsQ0FGUyxFQUdUOEgsT0FIUyxFQUdBRCxNQUhBLEVBSVRJLFFBSlMsRUFJQ0MsUUFKRCxDQUFiLEVBS0csSUFMSDtBQU1ILEdBemFpQztBQTJhbENJLEVBQUFBLFlBQVksRUFBRSxVQUFTM0YsRUFBVCxFQUFhO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLFFBQUksS0FBSzlFLEtBQUwsQ0FBVzBLLFlBQVgsSUFBMkIsS0FBSzFLLEtBQUwsQ0FBVzJLLGFBQTFDLEVBQXlEO0FBQ3JELFlBQU1DLGFBQWEsR0FBRyxJQUFJQyxHQUFKLENBQVEvRixFQUFFLENBQUMwQixNQUFILENBQVVzRSxTQUFWLENBQW9CckIsS0FBcEIsQ0FBMEIsR0FBMUIsQ0FBUixDQUF0Qjs7QUFDQSxVQUNJbUIsYUFBYSxDQUFDRyxHQUFkLENBQWtCLGVBQWxCLEtBQ0FILGFBQWEsQ0FBQ0csR0FBZCxDQUFrQiwyQkFBbEIsQ0FEQSxJQUVBSCxhQUFhLENBQUNHLEdBQWQsQ0FBa0IsYUFBbEIsQ0FISixFQUlFO0FBQ0UsYUFBS3pJLFFBQUwsQ0FBYztBQUNWMEksVUFBQUEsU0FBUyxFQUFFO0FBQ1BDLFlBQUFBLENBQUMsRUFBRW5HLEVBQUUsQ0FBQ29HLEtBREM7QUFFUEMsWUFBQUEsQ0FBQyxFQUFFckcsRUFBRSxDQUFDc0c7QUFGQztBQURELFNBQWQ7QUFNSDtBQUNKO0FBQ0osR0E5YmlDO0FBZ2NsQ0MsRUFBQUEsVUFBVSxFQUFFLFVBQVN2RyxFQUFULEVBQWE7QUFDckIsUUFBSSxDQUFDLEtBQUtMLEtBQUwsQ0FBV3VHLFNBQWhCLEVBQTJCO0FBRTNCLFVBQU1NLE1BQU0sR0FBR3hHLEVBQUUsQ0FBQ29HLEtBQUgsR0FBVyxLQUFLekcsS0FBTCxDQUFXdUcsU0FBWCxDQUFxQkMsQ0FBL0M7QUFDQSxVQUFNTSxNQUFNLEdBQUd6RyxFQUFFLENBQUNzRyxLQUFILEdBQVcsS0FBSzNHLEtBQUwsQ0FBV3VHLFNBQVgsQ0FBcUJHLENBQS9DO0FBQ0EsVUFBTUssUUFBUSxHQUFHQyxJQUFJLENBQUNDLElBQUwsQ0FBV0osTUFBTSxHQUFHQSxNQUFWLElBQXFCQyxNQUFNLEdBQUdBLE1BQTlCLENBQVYsQ0FBakI7QUFDQSxVQUFNSSxTQUFTLEdBQUcsQ0FBbEIsQ0FOcUIsQ0FNQTtBQUVyQjtBQUNBOztBQUVBLFFBQUlILFFBQVEsR0FBR0csU0FBZixFQUEwQjtBQUN0QjtBQUNBM0ksMEJBQUlDLFFBQUosQ0FBYTtBQUFFQyxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiO0FBQ0gsS0Fkb0IsQ0FnQnJCO0FBQ0E7OztBQUNBLFNBQUtaLFFBQUwsQ0FBYztBQUFDMEksTUFBQUEsU0FBUyxFQUFFO0FBQVosS0FBZDtBQUNILEdBbmRpQzs7QUFxZGxDWSxFQUFBQSxzQkFBc0IsQ0FBQ0MsR0FBRCxFQUFNO0FBQ3hCLFNBQUtuSSxlQUFMLEdBQXVCbUksR0FBdkI7QUFDSCxHQXZkaUM7O0FBeWRsQ0MsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxTQUFTLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBbEI7QUFDQSxVQUFNQyxRQUFRLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBakI7QUFDQSxVQUFNRSxRQUFRLEdBQUdILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBakI7QUFDQSxVQUFNRyxTQUFTLEdBQUdKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBbEI7QUFDQSxVQUFNSSxRQUFRLEdBQUdMLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBakI7QUFDQSxVQUFNSyxjQUFjLEdBQUdOLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBdkI7QUFDQSxVQUFNTSxhQUFhLEdBQUdQLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1QkFBakIsQ0FBdEI7QUFDQSxVQUFNTyxTQUFTLEdBQUdSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixtQkFBakIsQ0FBbEI7QUFDQSxVQUFNUSxhQUFhLEdBQUdULEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1QkFBakIsQ0FBdEI7QUFDQSxVQUFNUyxjQUFjLEdBQUdWLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7QUFDQSxVQUFNVSxjQUFjLEdBQUdYLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7QUFDQSxVQUFNVyxjQUFjLEdBQUdaLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7QUFFQSxRQUFJWSxXQUFKOztBQUVBLFlBQVEsS0FBSzdNLEtBQUwsQ0FBV25CLFNBQW5CO0FBQ0ksV0FBS2lPLG1CQUFVWixRQUFmO0FBQ0lXLFFBQUFBLFdBQVcsR0FBRyw2QkFBQyxRQUFEO0FBQ04sVUFBQSxHQUFHLEVBQUUsS0FBSzdMLFNBREo7QUFFTixVQUFBLFFBQVEsRUFBRSxLQUFLaEIsS0FBTCxDQUFXK00sUUFGZjtBQUdOLFVBQUEsWUFBWSxFQUFFLEtBQUsvTSxLQUFMLENBQVdmLFlBSG5CO0FBSU4sVUFBQSxnQkFBZ0IsRUFBRSxLQUFLZSxLQUFMLENBQVdnTixnQkFKdkI7QUFLTixVQUFBLE9BQU8sRUFBRSxLQUFLaE4sS0FBTCxDQUFXaU4sV0FMZDtBQU1OLFVBQUEsVUFBVSxFQUFFLEtBQUtqTixLQUFMLENBQVdkLFVBTmpCO0FBT04sVUFBQSxnQkFBZ0IsRUFBRSxLQUFLYyxLQUFMLENBQVdrTix1QkFQdkI7QUFRTixVQUFBLEdBQUcsRUFBRSxLQUFLbE4sS0FBTCxDQUFXbU4sYUFBWCxJQUE0QixVQVIzQjtBQVNOLFVBQUEsUUFBUSxFQUFFLEtBQUtuTixLQUFMLENBQVdvTixjQVRmO0FBVU4sVUFBQSxpQkFBaUIsRUFBRSxLQUFLcE4sS0FBTCxDQUFXcU4saUJBVnhCO0FBV04sVUFBQSxjQUFjLEVBQUUsS0FBS3JOLEtBQUwsQ0FBV3VCO0FBWHJCLFVBQWQ7QUFhQTs7QUFFSixXQUFLdUwsbUJBQVVULFFBQWY7QUFDSVEsUUFBQUEsV0FBVyxHQUFHLDZCQUFDLFFBQUQsT0FBZDtBQUNBOztBQUVKLFdBQUtDLG1CQUFVUSxhQUFmO0FBQ0k7QUFDQTs7QUFFSixXQUFLUixtQkFBVVMsUUFBZjtBQUNJVixRQUFBQSxXQUFXLEdBQUcsNkJBQUMsaUJBQUQsT0FBZDtBQUNBOztBQUVKLFdBQUtDLG1CQUFVWCxRQUFmO0FBQ0lVLFFBQUFBLFdBQVcsR0FBRyw2QkFBQyxRQUFEO0FBQVUsVUFBQSxNQUFNLEVBQUUsS0FBSzdNLEtBQUwsQ0FBV3dOO0FBQTdCLFVBQWQ7QUFDQTs7QUFDSixXQUFLVixtQkFBVVYsU0FBZjtBQUNJUyxRQUFBQSxXQUFXLEdBQUcsNkJBQUMsU0FBRDtBQUNWLFVBQUEsT0FBTyxFQUFFLEtBQUs3TSxLQUFMLENBQVd5TixjQURWO0FBRVYsVUFBQSxLQUFLLEVBQUUsS0FBS3pOLEtBQUwsQ0FBVzBOO0FBRlIsVUFBZDtBQUlBO0FBckNSOztBQXdDQSxVQUFNQyxlQUFlLEdBQUcsS0FBS2xKLEtBQUwsQ0FBV2pGLGtCQUFYLENBQThCMEcsSUFBOUIsQ0FBb0MwSCxDQUFELElBQU87QUFDOUQsYUFDSUEsQ0FBQyxJQUFJQSxDQUFDLENBQUN6SixPQUFGLE9BQWdCLGdCQUFyQixJQUNBeUosQ0FBQyxDQUFDeEosVUFBRixHQUFlLG9CQUFmLE1BQXlDLHFDQUY3QztBQUlILEtBTHVCLENBQXhCO0FBT0EsUUFBSXlKLE1BQUo7O0FBQ0EsUUFBSSxLQUFLcEosS0FBTCxDQUFXQyxhQUFYLElBQTRCLEtBQUtELEtBQUwsQ0FBV0MsYUFBWCxDQUF5QkMsS0FBekIsQ0FBK0JDLE9BQS9CLEtBQTJDLDJCQUEzRSxFQUF3RztBQUNwR2lKLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxjQUFEO0FBQWdCLFFBQUEsSUFBSSxFQUFDLE1BQXJCO0FBQ0wsUUFBQSxZQUFZLEVBQUUsS0FBS3BKLEtBQUwsQ0FBV0MsYUFBWCxDQUF5QkMsS0FBekIsQ0FBK0JKLElBQS9CLENBQW9DdUosYUFEN0M7QUFFTCxRQUFBLFNBQVMsRUFBRSxLQUFLckosS0FBTCxDQUFXQyxhQUFYLENBQXlCQyxLQUF6QixDQUErQkosSUFBL0IsQ0FBb0N3SjtBQUYxQyxRQUFUO0FBSUgsS0FMRCxNQUtPLElBQUlKLGVBQUosRUFBcUI7QUFDeEJFLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxjQUFEO0FBQWdCLFFBQUEsSUFBSSxFQUFDLE1BQXJCO0FBQ0wsUUFBQSxZQUFZLEVBQUVGLGVBQWUsQ0FBQ3ZKLFVBQWhCLEdBQTZCMEosYUFEdEM7QUFFTCxRQUFBLFNBQVMsRUFBRUgsZUFBZSxDQUFDdkosVUFBaEIsR0FBNkIySjtBQUZuQyxRQUFUO0FBSUgsS0FMTSxNQUtBLElBQUksS0FBSy9OLEtBQUwsQ0FBV21CLGFBQVgsSUFDUCxLQUFLbkIsS0FBTCxDQUFXZ08sTUFBWCxDQUFrQkMsS0FEWCxJQUVQQyxTQUFTLENBQUNDLFVBQVYsS0FBeUIsR0FGdEIsRUFHTDtBQUNFLFlBQU1DLFNBQVMsR0FBRyxLQUFLcE8sS0FBTCxDQUFXZ08sTUFBWCxDQUFrQkMsS0FBbEIsQ0FBd0JHLFNBQXhCLElBQXFDLElBQXZEO0FBQ0FQLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxTQUFEO0FBQVcsUUFBQSxTQUFTLEVBQUVPO0FBQXRCLFFBQVQ7QUFDSCxLQU5NLE1BTUEsSUFBSSxLQUFLcE8sS0FBTCxDQUFXb0IsYUFBZixFQUE4QjtBQUNqQ3lNLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxhQUFEO0FBQWUsUUFBQSxPQUFPLEVBQUUsS0FBSzdOLEtBQUwsQ0FBV3FPLE9BQW5DO0FBQTRDLFFBQUEsVUFBVSxFQUFFLEtBQUtyTyxLQUFMLENBQVdzTyxVQUFuRTtBQUNlLFFBQUEsWUFBWSxFQUFFLEtBQUt0TyxLQUFMLENBQVd1TztBQUR4QyxRQUFUO0FBR0gsS0FKTSxNQUlBLElBQUksS0FBS3ZPLEtBQUwsQ0FBV3dPLGlCQUFmLEVBQWtDO0FBQ3JDWCxNQUFBQSxNQUFNLEdBQUcsNkJBQUMsY0FBRCxFQUFvQixLQUFLN04sS0FBTCxDQUFXd08saUJBQS9CLENBQVQ7QUFDSCxLQUZNLE1BRUEsSUFBSSxLQUFLL0osS0FBTCxDQUFXcEQsd0JBQWYsRUFBeUM7QUFDNUN3TSxNQUFBQSxNQUFNLEdBQUcsNkJBQUMsY0FBRCxPQUFUO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBSzdOLEtBQUwsQ0FBV3NCLG1CQUFmLEVBQW9DO0FBQ3ZDdU0sTUFBQUEsTUFBTSxHQUFHLDZCQUFDLGFBQUQsT0FBVDtBQUNIOztBQUVELFFBQUlZLFdBQVcsR0FBRyxlQUFsQjs7QUFDQSxRQUFJWixNQUFKLEVBQVk7QUFDUlksTUFBQUEsV0FBVyxJQUFJLCtCQUFmO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLaEssS0FBTCxDQUFXcEYsZ0JBQWYsRUFBaUM7QUFDN0JvUCxNQUFBQSxXQUFXLElBQUksaUNBQWY7QUFDSDs7QUFFRCxXQUNJLDZCQUFDLDRCQUFELENBQXFCLFFBQXJCO0FBQThCLE1BQUEsS0FBSyxFQUFFLEtBQUsxTztBQUExQyxPQUNJO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBS3NHLFFBRGxCO0FBRUksTUFBQSxTQUFTLEVBQUUsS0FBS0ssZUFGcEI7QUFHSSxNQUFBLFNBQVMsRUFBQyx1QkFIZDtBQUlJLHFCQUFhLEtBQUsxRyxLQUFMLENBQVcwTyxhQUo1QjtBQUtJLE1BQUEsV0FBVyxFQUFFLEtBQUtqRSxZQUx0QjtBQU1JLE1BQUEsU0FBUyxFQUFFLEtBQUtZO0FBTnBCLE9BUU13QyxNQVJOLEVBU0ksNkJBQUMsY0FBRCxPQVRKLEVBVUksNkJBQUMsa0NBQUQ7QUFBaUIsTUFBQSxTQUFTLEVBQUUsS0FBSzFFO0FBQWpDLE9BQ0k7QUFBSyxNQUFBLEdBQUcsRUFBRSxLQUFLeUMsc0JBQWY7QUFBdUMsTUFBQSxTQUFTLEVBQUU2QztBQUFsRCxPQUNJLDZCQUFDLFNBQUQ7QUFDSSxNQUFBLGNBQWMsRUFBRSxLQUFLek8sS0FBTCxDQUFXdUIsY0FEL0I7QUFFSSxNQUFBLFNBQVMsRUFBRSxLQUFLdkIsS0FBTCxDQUFXMk8sV0FBWCxJQUEwQixLQUZ6QztBQUdJLE1BQUEsUUFBUSxFQUFFLEtBQUszTyxLQUFMLENBQVcwSztBQUh6QixNQURKLEVBTUksNkJBQUMscUJBQUQsT0FOSixFQU9NbUMsV0FQTixDQURKLENBVkosQ0FESixDQURKO0FBMEJIO0FBdmxCaUMsQ0FBakIsQ0FBckI7ZUEwbEJleE8sWSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IHsgTWF0cml4Q2xpZW50IH0gZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcbmltcG9ydCBSZWFjdCwge2NyZWF0ZVJlZn0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyBEcmFnRHJvcENvbnRleHQgfSBmcm9tICdyZWFjdC1iZWF1dGlmdWwtZG5kJztcclxuXHJcbmltcG9ydCB7S2V5LCBpc09ubHlDdHJsT3JDbWRLZXlFdmVudCwgaXNPbmx5Q3RybE9yQ21kSWdub3JlU2hpZnRLZXlFdmVudH0gZnJvbSAnLi4vLi4vS2V5Ym9hcmQnO1xyXG5pbXBvcnQgUGFnZVR5cGVzIGZyb20gJy4uLy4uL1BhZ2VUeXBlcyc7XHJcbmltcG9ydCBDYWxsTWVkaWFIYW5kbGVyIGZyb20gJy4uLy4uL0NhbGxNZWRpYUhhbmRsZXInO1xyXG5pbXBvcnQgeyBmaXh1cENvbG9yRm9udHMgfSBmcm9tICcuLi8uLi91dGlscy9Gb250TWFuYWdlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBzZXNzaW9uU3RvcmUgZnJvbSAnLi4vLi4vc3RvcmVzL1Nlc3Npb25TdG9yZSc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgUm9vbUxpc3RTdG9yZSBmcm9tIFwiLi4vLi4vc3RvcmVzL1Jvb21MaXN0U3RvcmVcIjtcclxuXHJcbmltcG9ydCBUYWdPcmRlckFjdGlvbnMgZnJvbSAnLi4vLi4vYWN0aW9ucy9UYWdPcmRlckFjdGlvbnMnO1xyXG5pbXBvcnQgUm9vbUxpc3RBY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMvUm9vbUxpc3RBY3Rpb25zJztcclxuaW1wb3J0IFJlc2l6ZUhhbmRsZSBmcm9tICcuLi92aWV3cy9lbGVtZW50cy9SZXNpemVIYW5kbGUnO1xyXG5pbXBvcnQge1Jlc2l6ZXIsIENvbGxhcHNlRGlzdHJpYnV0b3J9IGZyb20gJy4uLy4uL3Jlc2l6ZXInO1xyXG5pbXBvcnQgTWF0cml4Q2xpZW50Q29udGV4dCBmcm9tIFwiLi4vLi4vY29udGV4dHMvTWF0cml4Q2xpZW50Q29udGV4dFwiO1xyXG5pbXBvcnQgKiBhcyBLZXlib2FyZFNob3J0Y3V0cyBmcm9tIFwiLi4vLi4vYWNjZXNzaWJpbGl0eS9LZXlib2FyZFNob3J0Y3V0c1wiO1xyXG5pbXBvcnQgSG9tZVBhZ2UgZnJvbSBcIi4vSG9tZVBhZ2VcIjtcclxuLy8gV2UgbmVlZCB0byBmZXRjaCBlYWNoIHBpbm5lZCBtZXNzYWdlIGluZGl2aWR1YWxseSAoaWYgd2UgZG9uJ3QgYWxyZWFkeSBoYXZlIGl0KVxyXG4vLyBzbyBlYWNoIHBpbm5lZCBtZXNzYWdlIG1heSB0cmlnZ2VyIGEgcmVxdWVzdC4gTGltaXQgdGhlIG51bWJlciBwZXIgcm9vbSBmb3Igc2FuaXR5LlxyXG4vLyBOQi4gdGhpcyBpcyBqdXN0IGZvciBzZXJ2ZXIgbm90aWNlcyByYXRoZXIgdGhhbiBwaW5uZWQgbWVzc2FnZXMgaW4gZ2VuZXJhbC5cclxuY29uc3QgTUFYX1BJTk5FRF9OT1RJQ0VTX1BFUl9ST09NID0gMjtcclxuXHJcbmZ1bmN0aW9uIGNhbkVsZW1lbnRSZWNlaXZlSW5wdXQoZWwpIHtcclxuICAgIHJldHVybiBlbC50YWdOYW1lID09PSBcIklOUFVUXCIgfHxcclxuICAgICAgICBlbC50YWdOYW1lID09PSBcIlRFWFRBUkVBXCIgfHxcclxuICAgICAgICBlbC50YWdOYW1lID09PSBcIlNFTEVDVFwiIHx8XHJcbiAgICAgICAgISFlbC5nZXRBdHRyaWJ1dGUoXCJjb250ZW50ZWRpdGFibGVcIik7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGlzIGlzIHdoYXQgb3VyIE1hdHJpeENoYXQgc2hvd3Mgd2hlbiB3ZSBhcmUgbG9nZ2VkIGluLiBUaGUgcHJlY2lzZSB2aWV3IGlzXHJcbiAqIGRldGVybWluZWQgYnkgdGhlIHBhZ2VfdHlwZSBwcm9wZXJ0eS5cclxuICpcclxuICogQ3VycmVudGx5IGl0J3MgdmVyeSB0aWdodGx5IGNvdXBsZWQgd2l0aCBNYXRyaXhDaGF0LiBXZSBzaG91bGQgdHJ5IHRvIGRvXHJcbiAqIHNvbWV0aGluZyBhYm91dCB0aGF0LlxyXG4gKlxyXG4gKiBDb21wb25lbnRzIG1vdW50ZWQgYmVsb3cgdXMgY2FuIGFjY2VzcyB0aGUgbWF0cml4IGNsaWVudCB2aWEgdGhlIHJlYWN0IGNvbnRleHQuXHJcbiAqL1xyXG5jb25zdCBMb2dnZWRJblZpZXcgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnTG9nZ2VkSW5WaWV3JyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBtYXRyaXhDbGllbnQ6IFByb3BUeXBlcy5pbnN0YW5jZU9mKE1hdHJpeENsaWVudCkuaXNSZXF1aXJlZCxcclxuICAgICAgICBwYWdlX3R5cGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBvblJvb21DcmVhdGVkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gQ2FsbGVkIHdpdGggdGhlIGNyZWRlbnRpYWxzIG9mIGEgcmVnaXN0ZXJlZCB1c2VyIChpZiB0aGV5IHdlcmUgYSBST1UgdGhhdFxyXG4gICAgICAgIC8vIHRyYW5zaXRpb25lZCB0byBQV0xVKVxyXG4gICAgICAgIG9uUmVnaXN0ZXJlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIFVzZWQgYnkgdGhlIFJvb21WaWV3IHRvIGhhbmRsZSBqb2luaW5nIHJvb21zXHJcbiAgICAgICAgdmlhU2VydmVyczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyksXHJcblxyXG4gICAgICAgIC8vIGFuZCBsb3RzIGFuZCBsb3RzIG9mIG90aGVyIHN0dWZmLlxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIC8vIHVzZSBjb21wYWN0IHRpbWVsaW5lIHZpZXdcclxuICAgICAgICAgICAgdXNlQ29tcGFjdExheW91dDogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZSgndXNlQ29tcGFjdExheW91dCcpLFxyXG4gICAgICAgICAgICAvLyBhbnkgY3VycmVudGx5IGFjdGl2ZSBzZXJ2ZXIgbm90aWNlIGV2ZW50c1xyXG4gICAgICAgICAgICBzZXJ2ZXJOb3RpY2VFdmVudHM6IFtdLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnJlc2l6ZXIgPSB0aGlzLl9jcmVhdGVSZXNpemVyKCk7XHJcbiAgICAgICAgdGhpcy5yZXNpemVyLmF0dGFjaCgpO1xyXG4gICAgICAgIHRoaXMuX2xvYWRSZXNpemVyUHJlZmVyZW5jZXMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2UgY29tcG9uZW50IHdpdGggcmVhbCBjbGFzcywgdXNlIGNvbnN0cnVjdG9yIGZvciByZWZzXHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBzdGFzaCB0aGUgTWF0cml4Q2xpZW50IGluIGNhc2Ugd2UgbG9nIG91dCBiZWZvcmUgd2UgYXJlIHVubW91bnRlZFxyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudCA9IHRoaXMucHJvcHMubWF0cml4Q2xpZW50O1xyXG5cclxuICAgICAgICBDYWxsTWVkaWFIYW5kbGVyLmxvYWREZXZpY2VzKCk7XHJcblxyXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLl9vbk5hdGl2ZUtleURvd24sIGZhbHNlKTtcclxuXHJcbiAgICAgICAgdGhpcy5fc2Vzc2lvblN0b3JlID0gc2Vzc2lvblN0b3JlO1xyXG4gICAgICAgIHRoaXMuX3Nlc3Npb25TdG9yZVRva2VuID0gdGhpcy5fc2Vzc2lvblN0b3JlLmFkZExpc3RlbmVyKFxyXG4gICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZUZyb21TZXNzaW9uU3RvcmUsXHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLl9zZXRTdGF0ZUZyb21TZXNzaW9uU3RvcmUoKTtcclxuXHJcbiAgICAgICAgdGhpcy5fdXBkYXRlU2VydmVyTm90aWNlRXZlbnRzKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5vbihcImFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50Lm9uKFwic3luY1wiLCB0aGlzLm9uU3luYyk7XHJcbiAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50Lm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnRzKTtcclxuXHJcbiAgICAgICAgZml4dXBDb2xvckZvbnRzKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX3Jvb21WaWV3ID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcclxuICAgICAgICAvLyBhdHRlbXB0IHRvIGd1ZXNzIHdoZW4gYSBiYW5uZXIgd2FzIG9wZW5lZCBvciBjbG9zZWRcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIChwcmV2UHJvcHMuc2hvd0Nvb2tpZUJhciAhPT0gdGhpcy5wcm9wcy5zaG93Q29va2llQmFyKSB8fFxyXG4gICAgICAgICAgICAocHJldlByb3BzLmhhc05ld1ZlcnNpb24gIT09IHRoaXMucHJvcHMuaGFzTmV3VmVyc2lvbikgfHxcclxuICAgICAgICAgICAgKHByZXZQcm9wcy51c2VySGFzR2VuZXJhdGVkUGFzc3dvcmQgIT09IHRoaXMucHJvcHMudXNlckhhc0dlbmVyYXRlZFBhc3N3b3JkKSB8fFxyXG4gICAgICAgICAgICAocHJldlByb3BzLnNob3dOb3RpZmllclRvb2xiYXIgIT09IHRoaXMucHJvcHMuc2hvd05vdGlmaWVyVG9vbGJhcilcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5yZXNpemVOb3RpZmllci5ub3RpZnlCYW5uZXJzQ2hhbmdlZCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLl9vbk5hdGl2ZUtleURvd24sIGZhbHNlKTtcclxuICAgICAgICB0aGlzLl9tYXRyaXhDbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJhY2NvdW50RGF0YVwiLCB0aGlzLm9uQWNjb3VudERhdGEpO1xyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5yZW1vdmVMaXN0ZW5lcihcInN5bmNcIiwgdGhpcy5vblN5bmMpO1xyXG4gICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb21TdGF0ZS5ldmVudHNcIiwgdGhpcy5vblJvb21TdGF0ZUV2ZW50cyk7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Nlc3Npb25TdG9yZVRva2VuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Nlc3Npb25TdG9yZVRva2VuLnJlbW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJlc2l6ZXIuZGV0YWNoKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIENoaWxkIGNvbXBvbmVudHMgYXNzdW1lIHRoYXQgdGhlIGNsaWVudCBwZWcgd2lsbCBub3QgYmUgbnVsbCwgc28gZ2l2ZSB0aGVtIHNvbWVcclxuICAgIC8vIHNvcnQgb2YgYXNzdXJhbmNlIGhlcmUgYnkgb25seSBhbGxvd2luZyBhIHJlLXJlbmRlciBpZiB0aGUgY2xpZW50IGlzIHRydXRoeS5cclxuICAgIC8vXHJcbiAgICAvLyBUaGlzIGlzIHJlcXVpcmVkIGJlY2F1c2UgYExvZ2dlZEluVmlld2AgbWFpbnRhaW5zIGl0cyBvd24gc3RhdGUgYW5kIGlmIHRoaXMgc3RhdGVcclxuICAgIC8vIHVwZGF0ZXMgYWZ0ZXIgdGhlIGNsaWVudCBwZWcgaGFzIGJlZW4gbWFkZSBudWxsIChkdXJpbmcgbG9nb3V0KSwgdGhlbiBpdCB3aWxsXHJcbiAgICAvLyBhdHRlbXB0IHRvIHJlLXJlbmRlciBhbmQgdGhlIGNoaWxkcmVuIHdpbGwgdGhyb3cgZXJyb3JzLlxyXG4gICAgc2hvdWxkQ29tcG9uZW50VXBkYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gQm9vbGVhbihNYXRyaXhDbGllbnRQZWcuZ2V0KCkpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjYW5SZXNldFRpbWVsaW5lSW5Sb29tOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICBpZiAoIXRoaXMuX3Jvb21WaWV3LmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9yb29tVmlldy5jdXJyZW50LmNhblJlc2V0VGltZWxpbmUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NldFN0YXRlRnJvbVNlc3Npb25TdG9yZSgpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdXNlckhhc0dlbmVyYXRlZFBhc3N3b3JkOiBCb29sZWFuKHRoaXMuX3Nlc3Npb25TdG9yZS5nZXRDYWNoZWRQYXNzd29yZCgpKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2NyZWF0ZVJlc2l6ZXIoKSB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NOYW1lcyA9IHtcclxuICAgICAgICAgICAgaGFuZGxlOiBcIm14X1Jlc2l6ZUhhbmRsZVwiLFxyXG4gICAgICAgICAgICB2ZXJ0aWNhbDogXCJteF9SZXNpemVIYW5kbGVfdmVydGljYWxcIixcclxuICAgICAgICAgICAgcmV2ZXJzZTogXCJteF9SZXNpemVIYW5kbGVfcmV2ZXJzZVwiLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgY29sbGFwc2VDb25maWcgPSB7XHJcbiAgICAgICAgICAgIHRvZ2dsZVNpemU6IDI2MCAtIDUwLFxyXG4gICAgICAgICAgICBvbkNvbGxhcHNlZDogKGNvbGxhcHNlZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNvbGxhcHNlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiBcImhpZGVfbGVmdF9wYW5lbFwifSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibXhfbGhzX3NpemVcIiwgJzAnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246IFwic2hvd19sZWZ0X3BhbmVsXCJ9LCB0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgb25SZXNpemVkOiAoc2l6ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibXhfbGhzX3NpemVcIiwgJycgKyBzaXplKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIubm90aWZ5TGVmdEhhbmRsZVJlc2l6ZWQoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHJlc2l6ZXIgPSBuZXcgUmVzaXplcihcclxuICAgICAgICAgICAgdGhpcy5yZXNpemVDb250YWluZXIsXHJcbiAgICAgICAgICAgIENvbGxhcHNlRGlzdHJpYnV0b3IsXHJcbiAgICAgICAgICAgIGNvbGxhcHNlQ29uZmlnKTtcclxuICAgICAgICByZXNpemVyLnNldENsYXNzTmFtZXMoY2xhc3NOYW1lcyk7XHJcbiAgICAgICAgcmV0dXJuIHJlc2l6ZXI7XHJcbiAgICB9LFxyXG5cclxuICAgIF9sb2FkUmVzaXplclByZWZlcmVuY2VzKCkge1xyXG4gICAgICAgIGxldCBsaHNTaXplID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibXhfbGhzX3NpemVcIik7XHJcbiAgICAgICAgaWYgKGxoc1NpemUgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgbGhzU2l6ZSA9IHBhcnNlSW50KGxoc1NpemUsIDEwKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsaHNTaXplID0gMzUwO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJlc2l6ZXIuZm9ySGFuZGxlQXQoMCkucmVzaXplKGxoc1NpemUpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkFjY291bnREYXRhOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5nZXRUeXBlKCkgPT09IFwiaW0udmVjdG9yLndlYi5zZXR0aW5nc1wiKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgdXNlQ29tcGFjdExheW91dDogZXZlbnQuZ2V0Q29udGVudCgpLnVzZUNvbXBhY3RMYXlvdXQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQuZ2V0VHlwZSgpID09PSBcIm0uaWdub3JlZF91c2VyX2xpc3RcIikge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogXCJpZ25vcmVfc3RhdGVfY2hhbmdlZFwifSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblN5bmM6IGZ1bmN0aW9uKHN5bmNTdGF0ZSwgb2xkU3luY1N0YXRlLCBkYXRhKSB7XHJcbiAgICAgICAgY29uc3Qgb2xkRXJyQ29kZSA9IChcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5zeW5jRXJyb3JEYXRhICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuc3luY0Vycm9yRGF0YS5lcnJvciAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnN5bmNFcnJvckRhdGEuZXJyb3IuZXJyY29kZVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgY29uc3QgbmV3RXJyQ29kZSA9IGRhdGEgJiYgZGF0YS5lcnJvciAmJiBkYXRhLmVycm9yLmVycmNvZGU7XHJcbiAgICAgICAgaWYgKHN5bmNTdGF0ZSA9PT0gb2xkU3luY1N0YXRlICYmIG9sZEVyckNvZGUgPT09IG5ld0VyckNvZGUpIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKHN5bmNTdGF0ZSA9PT0gJ0VSUk9SJykge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHN5bmNFcnJvckRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc3luY0Vycm9yRGF0YTogbnVsbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAob2xkU3luY1N0YXRlID09PSAnUFJFUEFSRUQnICYmIHN5bmNTdGF0ZSA9PT0gJ1NZTkNJTkcnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZVNlcnZlck5vdGljZUV2ZW50cygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tU3RhdGVFdmVudHM6IGZ1bmN0aW9uKGV2LCBzdGF0ZSkge1xyXG4gICAgICAgIGNvbnN0IHJvb21MaXN0cyA9IFJvb21MaXN0U3RvcmUuZ2V0Um9vbUxpc3RzKCk7XHJcbiAgICAgICAgaWYgKHJvb21MaXN0c1snbS5zZXJ2ZXJfbm90aWNlJ10gJiYgcm9vbUxpc3RzWydtLnNlcnZlcl9ub3RpY2UnXS5zb21lKHIgPT4gci5yb29tSWQgPT09IGV2LmdldFJvb21JZCgpKSkge1xyXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVTZXJ2ZXJOb3RpY2VFdmVudHMoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF91cGRhdGVTZXJ2ZXJOb3RpY2VFdmVudHM6IGFzeW5jIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb21MaXN0cyA9IFJvb21MaXN0U3RvcmUuZ2V0Um9vbUxpc3RzKCk7XHJcbiAgICAgICAgaWYgKCFyb29tTGlzdHNbJ20uc2VydmVyX25vdGljZSddKSByZXR1cm4gW107XHJcblxyXG4gICAgICAgIGNvbnN0IHBpbm5lZEV2ZW50cyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3Qgcm9vbSBvZiByb29tTGlzdHNbJ20uc2VydmVyX25vdGljZSddKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBpblN0YXRlRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5waW5uZWRfZXZlbnRzXCIsIFwiXCIpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFwaW5TdGF0ZUV2ZW50IHx8ICFwaW5TdGF0ZUV2ZW50LmdldENvbnRlbnQoKS5waW5uZWQpIGNvbnRpbnVlO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcGlubmVkRXZlbnRJZHMgPSBwaW5TdGF0ZUV2ZW50LmdldENvbnRlbnQoKS5waW5uZWQuc2xpY2UoMCwgTUFYX1BJTk5FRF9OT1RJQ0VTX1BFUl9ST09NKTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBldmVudElkIG9mIHBpbm5lZEV2ZW50SWRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0aW1lbGluZSA9IGF3YWl0IHRoaXMuX21hdHJpeENsaWVudC5nZXRFdmVudFRpbWVsaW5lKHJvb20uZ2V0VW5maWx0ZXJlZFRpbWVsaW5lU2V0KCksIGV2ZW50SWQsIDApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXYgPSB0aW1lbGluZS5nZXRFdmVudHMoKS5maW5kKGV2ID0+IGV2LmdldElkKCkgPT09IGV2ZW50SWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGV2KSBwaW5uZWRFdmVudHMucHVzaChldik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlcnZlck5vdGljZUV2ZW50czogcGlubmVkRXZlbnRzLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25QYXN0ZTogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBsZXQgY2FuUmVjZWl2ZUlucHV0ID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGVsZW1lbnQgPSBldi50YXJnZXQ7XHJcbiAgICAgICAgLy8gdGVzdCBmb3IgYWxsIHBhcmVudHMgYmVjYXVzZSB0aGUgdGFyZ2V0IGNhbiBiZSBhIGNoaWxkIG9mIGEgY29udGVudGVkaXRhYmxlIGVsZW1lbnRcclxuICAgICAgICB3aGlsZSAoIWNhblJlY2VpdmVJbnB1dCAmJiBlbGVtZW50KSB7XHJcbiAgICAgICAgICAgIGNhblJlY2VpdmVJbnB1dCA9IGNhbkVsZW1lbnRSZWNlaXZlSW5wdXQoZWxlbWVudCk7XHJcbiAgICAgICAgICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudEVsZW1lbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghY2FuUmVjZWl2ZUlucHV0KSB7XHJcbiAgICAgICAgICAgIC8vIHJlZm9jdXNpbmcgZHVyaW5nIGEgcGFzdGUgZXZlbnQgd2lsbCBtYWtlIHRoZVxyXG4gICAgICAgICAgICAvLyBwYXN0ZSBlbmQgdXAgaW4gdGhlIG5ld2x5IGZvY3VzZWQgZWxlbWVudCxcclxuICAgICAgICAgICAgLy8gc28gZGlzcGF0Y2ggc3luY2hyb25vdXNseSBiZWZvcmUgcGFzdGUgaGFwcGVuc1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2ZvY3VzX2NvbXBvc2VyJ30sIHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLypcclxuICAgIFNPTUUgSEFDS0VSWSBCRUxPVzpcclxuICAgIFJlYWN0IG9wdGltaXplcyBldmVudCBoYW5kbGVycywgYnkgYWx3YXlzIGF0dGFjaGluZyBvbmx5IDEgaGFuZGxlciB0byB0aGUgZG9jdW1lbnQgZm9yIGEgZ2l2ZW4gdHlwZS5cclxuICAgIEl0IHRoZW4gaW50ZXJuYWxseSBkZXRlcm1pbmVzIHRoZSBvcmRlciBpbiB3aGljaCBSZWFjdCBldmVudCBoYW5kbGVycyBzaG91bGQgYmUgY2FsbGVkLFxyXG4gICAgZW11bGF0aW5nIHRoZSBjYXB0dXJlIGFuZCBidWJibGluZyBwaGFzZXMgdGhlIERPTSBhbHNvIGhhcy5cclxuXHJcbiAgICBCdXQsIGFzIHRoZSBuYXRpdmUgaGFuZGxlciBmb3IgUmVhY3QgaXMgYWx3YXlzIGF0dGFjaGVkIG9uIHRoZSBkb2N1bWVudCxcclxuICAgIGl0IHdpbGwgYWx3YXlzIHJ1biBsYXN0IGZvciBidWJibGluZyAoZmlyc3QgZm9yIGNhcHR1cmluZykgaGFuZGxlcnMsXHJcbiAgICBhbmQgdGh1cyBSZWFjdCBiYXNpY2FsbHkgaGFzIGl0cyBvd24gZXZlbnQgcGhhc2VzLCBhbmQgd2lsbCBhbHdheXMgcnVuXHJcbiAgICBhZnRlciAoYmVmb3JlIGZvciBjYXB0dXJpbmcpIGFueSBuYXRpdmUgb3RoZXIgZXZlbnQgaGFuZGxlcnMgKGFzIHRoZXkgdGVuZCB0byBiZSBhdHRhY2hlZCBsYXN0KS5cclxuXHJcbiAgICBTbyBpZGVhbGx5IG9uZSB3b3VsZG4ndCBtaXggUmVhY3QgYW5kIG5hdGl2ZSBldmVudCBoYW5kbGVycyB0byBoYXZlIGJ1YmJsaW5nIHdvcmtpbmcgYXMgZXhwZWN0ZWQsXHJcbiAgICBidXQgd2UgZG8gbmVlZCBhIG5hdGl2ZSBldmVudCBoYW5kbGVyIGhlcmUgb24gdGhlIGRvY3VtZW50LFxyXG4gICAgdG8gZ2V0IGtleWRvd24gZXZlbnRzIHdoZW4gdGhlcmUgaXMgbm8gZm9jdXNlZCBlbGVtZW50ICh0YXJnZXQ9Ym9keSkuXHJcblxyXG4gICAgV2UgYWxzbyBkbyBuZWVkIGJ1YmJsaW5nIGhlcmUgdG8gZ2l2ZSBjaGlsZCBjb21wb25lbnRzIGEgY2hhbmNlIHRvIGNhbGwgYHN0b3BQcm9wYWdhdGlvbigpYCxcclxuICAgIGZvciBrZXlkb3duIGV2ZW50cyBpdCBjYW4gaGFuZGxlIGl0c2VsZiwgYW5kIHNob3VsZG4ndCBiZSByZWRpcmVjdGVkIHRvIHRoZSBjb21wb3Nlci5cclxuXHJcbiAgICBTbyB3ZSBsaXN0ZW4gd2l0aCBSZWFjdCBvbiB0aGlzIGNvbXBvbmVudCB0byBnZXQgYW55IGV2ZW50cyBvbiBmb2N1c2VkIGVsZW1lbnRzLCBhbmQgZ2V0IGJ1YmJsaW5nIHdvcmtpbmcgYXMgZXhwZWN0ZWQuXHJcbiAgICBXZSBhbHNvIGxpc3RlbiB3aXRoIGEgbmF0aXZlIGxpc3RlbmVyIG9uIHRoZSBkb2N1bWVudCB0byBnZXQga2V5ZG93biBldmVudHMgd2hlbiBubyBlbGVtZW50IGlzIGZvY3VzZWQuXHJcbiAgICBCdWJibGluZyBpcyBpcnJlbGV2YW50IGhlcmUgYXMgdGhlIHRhcmdldCBpcyB0aGUgYm9keSBlbGVtZW50LlxyXG4gICAgKi9cclxuICAgIF9vblJlYWN0S2V5RG93bjogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICAvLyBldmVudHMgY2F1Z2h0IHdoaWxlIGJ1YmJsaW5nIHVwIG9uIHRoZSByb290IGVsZW1lbnRcclxuICAgICAgICAvLyBvZiB0aGlzIGNvbXBvbmVudCwgc28gc29tZXRoaW5nIG11c3QgYmUgZm9jdXNlZC5cclxuICAgICAgICB0aGlzLl9vbktleURvd24oZXYpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25OYXRpdmVLZXlEb3duOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIC8vIG9ubHkgcGFzcyB0aGlzIGlmIHRoZXJlIGlzIG5vIGZvY3VzZWQgZWxlbWVudC5cclxuICAgICAgICAvLyBpZiB0aGVyZSBpcywgX29uS2V5RG93biB3aWxsIGJlIGNhbGxlZCBieSB0aGVcclxuICAgICAgICAvLyByZWFjdCBrZXlkb3duIGhhbmRsZXIgdGhhdCByZXNwZWN0cyB0aGUgcmVhY3QgYnViYmxpbmcgb3JkZXIuXHJcbiAgICAgICAgaWYgKGV2LnRhcmdldCA9PT0gZG9jdW1lbnQuYm9keSkge1xyXG4gICAgICAgICAgICB0aGlzLl9vbktleURvd24oZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX29uS2V5RG93bjogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICAgICAgLypcclxuICAgICAgICAgICAgLy8gUmVtb3ZlIHRoaXMgZm9yIG5vdyBhcyBjdHJsK2FsdCA9IGFsdC1nciBzbyB0aGlzIGJyZWFrcyBrZXlib2FyZHMgd2hpY2ggcmVseSBvbiBhbHQtZ3IgZm9yIG51bWJlcnNcclxuICAgICAgICAgICAgLy8gV2lsbCBuZWVkIHRvIGZpbmQgYSBiZXR0ZXIgbWV0YSBrZXkgaWYgYW55b25lIGFjdHVhbGx5IGNhcmVzIGFib3V0IHVzaW5nIHRoaXMuXHJcbiAgICAgICAgICAgIGlmIChldi5hbHRLZXkgJiYgZXYuY3RybEtleSAmJiBldi5rZXlDb2RlID4gNDggJiYgZXYua2V5Q29kZSA8IDU4KSB7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfaW5kZXhlZF9yb29tJyxcclxuICAgICAgICAgICAgICAgICAgICByb29tSW5kZXg6IGV2LmtleUNvZGUgLSA0OSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICovXHJcblxyXG4gICAgICAgIGxldCBoYW5kbGVkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgY3RybENtZE9ubHkgPSBpc09ubHlDdHJsT3JDbWRLZXlFdmVudChldik7XHJcbiAgICAgICAgY29uc3QgaGFzTW9kaWZpZXIgPSBldi5hbHRLZXkgfHwgZXYuY3RybEtleSB8fCBldi5tZXRhS2V5IHx8IGV2LnNoaWZ0S2V5O1xyXG4gICAgICAgIGNvbnN0IGlzTW9kaWZpZXIgPSBldi5rZXkgPT09IEtleS5BTFQgfHwgZXYua2V5ID09PSBLZXkuQ09OVFJPTCB8fCBldi5rZXkgPT09IEtleS5NRVRBIHx8IGV2LmtleSA9PT0gS2V5LlNISUZUO1xyXG5cclxuICAgICAgICBzd2l0Y2ggKGV2LmtleSkge1xyXG4gICAgICAgICAgICBjYXNlIEtleS5QQUdFX1VQOlxyXG4gICAgICAgICAgICBjYXNlIEtleS5QQUdFX0RPV046XHJcbiAgICAgICAgICAgICAgICBpZiAoIWhhc01vZGlmaWVyICYmICFpc01vZGlmaWVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb25TY3JvbGxLZXlQcmVzc2VkKGV2KTtcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBLZXkuSE9NRTpcclxuICAgICAgICAgICAgY2FzZSBLZXkuRU5EOlxyXG4gICAgICAgICAgICAgICAgaWYgKGV2LmN0cmxLZXkgJiYgIWV2LnNoaWZ0S2V5ICYmICFldi5hbHRLZXkgJiYgIWV2Lm1ldGFLZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9vblNjcm9sbEtleVByZXNzZWQoZXYpO1xyXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgS2V5Lks6XHJcbiAgICAgICAgICAgICAgICBpZiAoY3RybENtZE9ubHkpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdmb2N1c19yb29tX2ZpbHRlcicsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBLZXkuQkFDS1RJQ0s6XHJcbiAgICAgICAgICAgICAgICAvLyBJZGVhbGx5IHRoaXMgd291bGQgYmUgQ1RSTCtQIGZvciBcIlByb2ZpbGVcIiwgYnV0IHRoYXQnc1xyXG4gICAgICAgICAgICAgICAgLy8gdGFrZW4gYnkgdGhlIHByaW50IGRpYWxvZy4gQ1RSTCtJIGZvciBcIkluZm9ybWF0aW9uXCJcclxuICAgICAgICAgICAgICAgIC8vIHdhcyBwcmV2aW91c2x5IGNob3NlbiBidXQgY29uZmxpY3RlZCB3aXRoIGl0YWxpY3MgaW5cclxuICAgICAgICAgICAgICAgIC8vIGNvbXBvc2VyLCBzbyBDVFJMK2AgaXQgaXNcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoY3RybENtZE9ubHkpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd0b2dnbGVfdG9wX2xlZnRfbWVudScsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgS2V5LlNMQVNIOlxyXG4gICAgICAgICAgICAgICAgaWYgKGlzT25seUN0cmxPckNtZElnbm9yZVNoaWZ0S2V5RXZlbnQoZXYpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgS2V5Ym9hcmRTaG9ydGN1dHMudG9nZ2xlRGlhbG9nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgS2V5LkFSUk9XX1VQOlxyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19ET1dOOlxyXG4gICAgICAgICAgICAgICAgaWYgKGV2LmFsdEtleSAmJiAhZXYuY3RybEtleSAmJiAhZXYubWV0YUtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbV9kZWx0YScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbHRhOiBldi5rZXkgPT09IEtleS5BUlJPV19VUCA/IC0xIDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdW5yZWFkOiBldi5zaGlmdEtleSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBLZXkuUEVSSU9EOlxyXG4gICAgICAgICAgICAgICAgaWYgKGN0cmxDbWRPbmx5ICYmICh0aGlzLnByb3BzLnBhZ2VfdHlwZSA9PT0gXCJyb29tX3ZpZXdcIiB8fCB0aGlzLnByb3BzLnBhZ2VfdHlwZSA9PT0gXCJncm91cF92aWV3XCIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndG9nZ2xlX3JpZ2h0X3BhbmVsJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogdGhpcy5wcm9wcy5wYWdlX3R5cGUgPT09IFwicm9vbV92aWV3XCIgPyBcInJvb21cIiA6IFwiZ3JvdXBcIixcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChoYW5kbGVkKSB7XHJcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIWlzTW9kaWZpZXIgJiYgIWV2LmFsdEtleSAmJiAhZXYuY3RybEtleSAmJiAhZXYubWV0YUtleSkge1xyXG4gICAgICAgICAgICAvLyBUaGUgYWJvdmUgY29uZGl0aW9uIGlzIGNyYWZ0ZWQgdG8gX2FsbG93XyBjaGFyYWN0ZXJzIHdpdGggU2hpZnRcclxuICAgICAgICAgICAgLy8gYWxyZWFkeSBwcmVzc2VkIChidXQgbm90IHRoZSBTaGlmdCBrZXkgZG93biBpdHNlbGYpLlxyXG5cclxuICAgICAgICAgICAgY29uc3QgaXNDbGlja1Nob3J0Y3V0ID0gZXYudGFyZ2V0ICE9PSBkb2N1bWVudC5ib2R5ICYmXHJcbiAgICAgICAgICAgICAgICAoZXYua2V5ID09PSBLZXkuU1BBQ0UgfHwgZXYua2V5ID09PSBLZXkuRU5URVIpO1xyXG5cclxuICAgICAgICAgICAgLy8gRG8gbm90IGNhcHR1cmUgdGhlIGNvbnRleHQgbWVudSBrZXkgdG8gaW1wcm92ZSBrZXlib2FyZCBhY2Nlc3NpYmlsaXR5XHJcbiAgICAgICAgICAgIGlmIChldi5rZXkgPT09IEtleS5DT05URVhUX01FTlUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCFpc0NsaWNrU2hvcnRjdXQgJiYgZXYua2V5ICE9PSBLZXkuVEFCICYmICFjYW5FbGVtZW50UmVjZWl2ZUlucHV0KGV2LnRhcmdldCkpIHtcclxuICAgICAgICAgICAgICAgIC8vIHN5bmNocm9ub3VzIGRpc3BhdGNoIHNvIHdlIGZvY3VzIGJlZm9yZSBrZXkgZ2VuZXJhdGVzIGlucHV0XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2ZvY3VzX2NvbXBvc2VyJ30sIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICAvLyB3ZSBzaG91bGQgKm5vdCogcHJldmVudERlZmF1bHQoKSBoZXJlIGFzXHJcbiAgICAgICAgICAgICAgICAvLyB0aGF0IHdvdWxkIHByZXZlbnQgdHlwaW5nIGluIHRoZSBub3ctZm9jdXNzZWQgY29tcG9zZXJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBkaXNwYXRjaCBhIHBhZ2UtdXAvcGFnZS1kb3duL2V0YyB0byB0aGUgYXBwcm9wcmlhdGUgY29tcG9uZW50XHJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZXYgVGhlIGtleSBldmVudFxyXG4gICAgICovXHJcbiAgICBfb25TY3JvbGxLZXlQcmVzc2VkOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmICh0aGlzLl9yb29tVmlldy5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvb21WaWV3LmN1cnJlbnQuaGFuZGxlU2Nyb2xsS2V5KGV2KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkRyYWdFbmQ6IGZ1bmN0aW9uKHJlc3VsdCkge1xyXG4gICAgICAgIC8vIERyYWdnZWQgdG8gYW4gaW52YWxpZCBkZXN0aW5hdGlvbiwgbm90IG9udG8gYSBkcm9wcGFibGVcclxuICAgICAgICBpZiAoIXJlc3VsdC5kZXN0aW5hdGlvbikge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBkZXN0ID0gcmVzdWx0LmRlc3RpbmF0aW9uLmRyb3BwYWJsZUlkO1xyXG5cclxuICAgICAgICBpZiAoZGVzdCA9PT0gJ3RhZy1wYW5lbC1kcm9wcGFibGUnKSB7XHJcbiAgICAgICAgICAgIC8vIENvdWxkIGJlIFwiR3JvdXBUaWxlICtncm91cElkOmRvbWFpblwiXHJcbiAgICAgICAgICAgIGNvbnN0IGRyYWdnYWJsZUlkID0gcmVzdWx0LmRyYWdnYWJsZUlkLnNwbGl0KCcgJykucG9wKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBEaXNwYXRjaCBzeW5jaHJvbm91c2x5IHNvIHRoYXQgdGhlIFRhZ1BhbmVsIHJlY2VpdmVzIGFuXHJcbiAgICAgICAgICAgIC8vIG9wdGltaXN0aWMgdXBkYXRlIGZyb20gVGFnT3JkZXJTdG9yZSBiZWZvcmUgdGhlIHByZXZpb3VzXHJcbiAgICAgICAgICAgIC8vIHN0YXRlIGlzIHNob3duLlxyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goVGFnT3JkZXJBY3Rpb25zLm1vdmVUYWcoXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tYXRyaXhDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBkcmFnZ2FibGVJZCxcclxuICAgICAgICAgICAgICAgIHJlc3VsdC5kZXN0aW5hdGlvbi5pbmRleCxcclxuICAgICAgICAgICAgKSwgdHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChkZXN0LnN0YXJ0c1dpdGgoJ3Jvb20tc3ViLWxpc3QtZHJvcHBhYmxlXycpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uUm9vbVRpbGVFbmREcmFnKHJlc3VsdCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25Sb29tVGlsZUVuZERyYWc6IGZ1bmN0aW9uKHJlc3VsdCkge1xyXG4gICAgICAgIGxldCBuZXdUYWcgPSByZXN1bHQuZGVzdGluYXRpb24uZHJvcHBhYmxlSWQuc3BsaXQoJ18nKVsxXTtcclxuICAgICAgICBsZXQgcHJldlRhZyA9IHJlc3VsdC5zb3VyY2UuZHJvcHBhYmxlSWQuc3BsaXQoJ18nKVsxXTtcclxuICAgICAgICBpZiAobmV3VGFnID09PSAndW5kZWZpbmVkJykgbmV3VGFnID0gdW5kZWZpbmVkO1xyXG4gICAgICAgIGlmIChwcmV2VGFnID09PSAndW5kZWZpbmVkJykgcHJldlRhZyA9IHVuZGVmaW5lZDtcclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gcmVzdWx0LmRyYWdnYWJsZUlkLnNwbGl0KCdfJylbMV07XHJcblxyXG4gICAgICAgIGNvbnN0IG9sZEluZGV4ID0gcmVzdWx0LnNvdXJjZS5pbmRleDtcclxuICAgICAgICBjb25zdCBuZXdJbmRleCA9IHJlc3VsdC5kZXN0aW5hdGlvbi5pbmRleDtcclxuXHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKFJvb21MaXN0QWN0aW9ucy50YWdSb29tKFxyXG4gICAgICAgICAgICB0aGlzLl9tYXRyaXhDbGllbnQsXHJcbiAgICAgICAgICAgIHRoaXMuX21hdHJpeENsaWVudC5nZXRSb29tKHJvb21JZCksXHJcbiAgICAgICAgICAgIHByZXZUYWcsIG5ld1RhZyxcclxuICAgICAgICAgICAgb2xkSW5kZXgsIG5ld0luZGV4LFxyXG4gICAgICAgICksIHRydWUpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25Nb3VzZURvd246IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgLy8gV2hlbiB0aGUgcGFuZWxzIGFyZSBkaXNhYmxlZCwgY2xpY2tpbmcgb24gdGhlbSByZXN1bHRzIGluIGEgbW91c2UgZXZlbnRcclxuICAgICAgICAvLyB3aGljaCBidWJibGVzIHRvIGNlcnRhaW4gZWxlbWVudHMgaW4gdGhlIHRyZWUuIFdoZW4gdGhpcyBoYXBwZW5zLCBjbG9zZVxyXG4gICAgICAgIC8vIGFueSBzZXR0aW5ncyBwYWdlIHRoYXQgaXMgY3VycmVudGx5IG9wZW4gKHVzZXIvcm9vbS9ncm91cCkuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubGVmdERpc2FibGVkICYmIHRoaXMucHJvcHMucmlnaHREaXNhYmxlZCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXJnZXRDbGFzc2VzID0gbmV3IFNldChldi50YXJnZXQuY2xhc3NOYW1lLnNwbGl0KCcgJykpO1xyXG4gICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgICB0YXJnZXRDbGFzc2VzLmhhcygnbXhfTWF0cml4Q2hhdCcpIHx8XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRDbGFzc2VzLmhhcygnbXhfTWF0cml4Q2hhdF9taWRkbGVQYW5lbCcpIHx8XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRDbGFzc2VzLmhhcygnbXhfUm9vbVZpZXcnKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIG1vdXNlRG93bjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB4OiBldi5wYWdlWCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgeTogZXYucGFnZVksXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25Nb3VzZVVwOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5tb3VzZURvd24pIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgZGVsdGFYID0gZXYucGFnZVggLSB0aGlzLnN0YXRlLm1vdXNlRG93bi54O1xyXG4gICAgICAgIGNvbnN0IGRlbHRhWSA9IGV2LnBhZ2VZIC0gdGhpcy5zdGF0ZS5tb3VzZURvd24ueTtcclxuICAgICAgICBjb25zdCBkaXN0YW5jZSA9IE1hdGguc3FydCgoZGVsdGFYICogZGVsdGFYKSArIChkZWx0YVkgKyBkZWx0YVkpKTtcclxuICAgICAgICBjb25zdCBtYXhSYWRpdXMgPSA1OyAvLyBQZW9wbGUgc2hvdWxkbid0IGJlIHN0cmF5aW5nIHRvbyBmYXIsIGhvcGVmdWxseVxyXG5cclxuICAgICAgICAvLyBOb3RlOiB3ZSB0cmFjayBob3cgZmFyIHRoZSB1c2VyIG1vdmVkIHRoZWlyIG1vdXNlIHRvIGhlbHBcclxuICAgICAgICAvLyBjb21iYXQgYWdhaW5zdCBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy83MTU4XHJcblxyXG4gICAgICAgIGlmIChkaXN0YW5jZSA8IG1heFJhZGl1cykge1xyXG4gICAgICAgICAgICAvLyBUaGlzIGlzIHByb2JhYmx5IGEgcmVhbCBjbGljaywgYW5kIG5vdCBhIGRyYWdcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAnY2xvc2Vfc2V0dGluZ3MnIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQWx3YXlzIGNsZWFyIHRoZSBtb3VzZURvd24gc3RhdGUgdG8gZW5zdXJlIHdlIGRvbid0IGFjY2lkZW50YWxseVxyXG4gICAgICAgIC8vIHVzZSBzdGFsZSB2YWx1ZXMgZHVlIHRvIHRoZSBtb3VzZURvd24gY2hlY2tzLlxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe21vdXNlRG93bjogbnVsbH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UmVzaXplQ29udGFpbmVyUmVmKGRpdikge1xyXG4gICAgICAgIHRoaXMucmVzaXplQ29udGFpbmVyID0gZGl2O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IExlZnRQYW5lbCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuTGVmdFBhbmVsJyk7XHJcbiAgICAgICAgY29uc3QgUm9vbVZpZXcgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlJvb21WaWV3Jyk7XHJcbiAgICAgICAgY29uc3QgVXNlclZpZXcgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlVzZXJWaWV3Jyk7XHJcbiAgICAgICAgY29uc3QgR3JvdXBWaWV3ID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5Hcm91cFZpZXcnKTtcclxuICAgICAgICBjb25zdCBNeUdyb3VwcyA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuTXlHcm91cHMnKTtcclxuICAgICAgICBjb25zdCBUb2FzdENvbnRhaW5lciA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuVG9hc3RDb250YWluZXInKTtcclxuICAgICAgICBjb25zdCBNYXRyaXhUb29sYmFyID0gc2RrLmdldENvbXBvbmVudCgnZ2xvYmFscy5NYXRyaXhUb29sYmFyJyk7XHJcbiAgICAgICAgY29uc3QgQ29va2llQmFyID0gc2RrLmdldENvbXBvbmVudCgnZ2xvYmFscy5Db29raWVCYXInKTtcclxuICAgICAgICBjb25zdCBOZXdWZXJzaW9uQmFyID0gc2RrLmdldENvbXBvbmVudCgnZ2xvYmFscy5OZXdWZXJzaW9uQmFyJyk7XHJcbiAgICAgICAgY29uc3QgVXBkYXRlQ2hlY2tCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdnbG9iYWxzLlVwZGF0ZUNoZWNrQmFyJyk7XHJcbiAgICAgICAgY29uc3QgUGFzc3dvcmROYWdCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdnbG9iYWxzLlBhc3N3b3JkTmFnQmFyJyk7XHJcbiAgICAgICAgY29uc3QgU2VydmVyTGltaXRCYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdnbG9iYWxzLlNlcnZlckxpbWl0QmFyJyk7XHJcblxyXG4gICAgICAgIGxldCBwYWdlRWxlbWVudDtcclxuXHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnByb3BzLnBhZ2VfdHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlIFBhZ2VUeXBlcy5Sb29tVmlldzpcclxuICAgICAgICAgICAgICAgIHBhZ2VFbGVtZW50ID0gPFJvb21WaWV3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fcm9vbVZpZXd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Kb2luPXt0aGlzLnByb3BzLmF1dG9Kb2lufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvblJlZ2lzdGVyZWQ9e3RoaXMucHJvcHMub25SZWdpc3RlcmVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlyZFBhcnR5SW52aXRlPXt0aGlzLnByb3BzLnRoaXJkUGFydHlJbnZpdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9vYkRhdGE9e3RoaXMucHJvcHMucm9vbU9vYkRhdGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpYVNlcnZlcnM9e3RoaXMucHJvcHMudmlhU2VydmVyc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRQaXhlbE9mZnNldD17dGhpcy5wcm9wcy5pbml0aWFsRXZlbnRQaXhlbE9mZnNldH1cclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PXt0aGlzLnByb3BzLmN1cnJlbnRSb29tSWQgfHwgJ3Jvb212aWV3J31cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMucHJvcHMubWlkZGxlRGlzYWJsZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENvbmZlcmVuY2VIYW5kbGVyPXt0aGlzLnByb3BzLkNvbmZlcmVuY2VIYW5kbGVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNpemVOb3RpZmllcj17dGhpcy5wcm9wcy5yZXNpemVOb3RpZmllcn1cclxuICAgICAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBQYWdlVHlwZXMuTXlHcm91cHM6XHJcbiAgICAgICAgICAgICAgICBwYWdlRWxlbWVudCA9IDxNeUdyb3VwcyAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBQYWdlVHlwZXMuUm9vbURpcmVjdG9yeTpcclxuICAgICAgICAgICAgICAgIC8vIGhhbmRsZWQgYnkgTWF0cml4Q2hhdCBmb3Igbm93XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgUGFnZVR5cGVzLkhvbWVQYWdlOlxyXG4gICAgICAgICAgICAgICAgcGFnZUVsZW1lbnQgPSA8SG9tZVBhZ2UgLz47XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgUGFnZVR5cGVzLlVzZXJWaWV3OlxyXG4gICAgICAgICAgICAgICAgcGFnZUVsZW1lbnQgPSA8VXNlclZpZXcgdXNlcklkPXt0aGlzLnByb3BzLmN1cnJlbnRVc2VySWR9IC8+O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUGFnZVR5cGVzLkdyb3VwVmlldzpcclxuICAgICAgICAgICAgICAgIHBhZ2VFbGVtZW50ID0gPEdyb3VwVmlld1xyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwSWQ9e3RoaXMucHJvcHMuY3VycmVudEdyb3VwSWR9XHJcbiAgICAgICAgICAgICAgICAgICAgaXNOZXc9e3RoaXMucHJvcHMuY3VycmVudEdyb3VwSXNOZXd9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdXNhZ2VMaW1pdEV2ZW50ID0gdGhpcy5zdGF0ZS5zZXJ2ZXJOb3RpY2VFdmVudHMuZmluZCgoZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgZSAmJiBlLmdldFR5cGUoKSA9PT0gJ20ucm9vbS5tZXNzYWdlJyAmJlxyXG4gICAgICAgICAgICAgICAgZS5nZXRDb250ZW50KClbJ3NlcnZlcl9ub3RpY2VfdHlwZSddID09PSAnbS5zZXJ2ZXJfbm90aWNlLnVzYWdlX2xpbWl0X3JlYWNoZWQnXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCB0b3BCYXI7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuc3luY0Vycm9yRGF0YSAmJiB0aGlzLnN0YXRlLnN5bmNFcnJvckRhdGEuZXJyb3IuZXJyY29kZSA9PT0gJ01fUkVTT1VSQ0VfTElNSVRfRVhDRUVERUQnKSB7XHJcbiAgICAgICAgICAgIHRvcEJhciA9IDxTZXJ2ZXJMaW1pdEJhciBraW5kPSdoYXJkJ1xyXG4gICAgICAgICAgICAgICAgYWRtaW5Db250YWN0PXt0aGlzLnN0YXRlLnN5bmNFcnJvckRhdGEuZXJyb3IuZGF0YS5hZG1pbl9jb250YWN0fVxyXG4gICAgICAgICAgICAgICAgbGltaXRUeXBlPXt0aGlzLnN0YXRlLnN5bmNFcnJvckRhdGEuZXJyb3IuZGF0YS5saW1pdF90eXBlfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKHVzYWdlTGltaXRFdmVudCkge1xyXG4gICAgICAgICAgICB0b3BCYXIgPSA8U2VydmVyTGltaXRCYXIga2luZD0nc29mdCdcclxuICAgICAgICAgICAgICAgIGFkbWluQ29udGFjdD17dXNhZ2VMaW1pdEV2ZW50LmdldENvbnRlbnQoKS5hZG1pbl9jb250YWN0fVxyXG4gICAgICAgICAgICAgICAgbGltaXRUeXBlPXt1c2FnZUxpbWl0RXZlbnQuZ2V0Q29udGVudCgpLmxpbWl0X3R5cGV9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5zaG93Q29va2llQmFyICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuY29uZmlnLnBpd2lrICYmXHJcbiAgICAgICAgICAgIG5hdmlnYXRvci5kb05vdFRyYWNrICE9PSBcIjFcIlxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICBjb25zdCBwb2xpY3lVcmwgPSB0aGlzLnByb3BzLmNvbmZpZy5waXdpay5wb2xpY3lVcmwgfHwgbnVsbDtcclxuICAgICAgICAgICAgdG9wQmFyID0gPENvb2tpZUJhciBwb2xpY3lVcmw9e3BvbGljeVVybH0gLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByb3BzLmhhc05ld1ZlcnNpb24pIHtcclxuICAgICAgICAgICAgdG9wQmFyID0gPE5ld1ZlcnNpb25CYXIgdmVyc2lvbj17dGhpcy5wcm9wcy52ZXJzaW9ufSBuZXdWZXJzaW9uPXt0aGlzLnByb3BzLm5ld1ZlcnNpb259XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGVhc2VOb3Rlcz17dGhpcy5wcm9wcy5uZXdWZXJzaW9uUmVsZWFzZU5vdGVzfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuY2hlY2tpbmdGb3JVcGRhdGUpIHtcclxuICAgICAgICAgICAgdG9wQmFyID0gPFVwZGF0ZUNoZWNrQmFyIHsuLi50aGlzLnByb3BzLmNoZWNraW5nRm9yVXBkYXRlfSAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUudXNlckhhc0dlbmVyYXRlZFBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgIHRvcEJhciA9IDxQYXNzd29yZE5hZ0JhciAvPjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuc2hvd05vdGlmaWVyVG9vbGJhcikge1xyXG4gICAgICAgICAgICB0b3BCYXIgPSA8TWF0cml4VG9vbGJhciAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBib2R5Q2xhc3NlcyA9ICdteF9NYXRyaXhDaGF0JztcclxuICAgICAgICBpZiAodG9wQmFyKSB7XHJcbiAgICAgICAgICAgIGJvZHlDbGFzc2VzICs9ICcgbXhfTWF0cml4Q2hhdF90b29sYmFyU2hvd2luZyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnVzZUNvbXBhY3RMYXlvdXQpIHtcclxuICAgICAgICAgICAgYm9keUNsYXNzZXMgKz0gJyBteF9NYXRyaXhDaGF0X3VzZUNvbXBhY3RMYXlvdXQnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPE1hdHJpeENsaWVudENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3RoaXMuX21hdHJpeENsaWVudH0+XHJcbiAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgb25QYXN0ZT17dGhpcy5fb25QYXN0ZX1cclxuICAgICAgICAgICAgICAgICAgICBvbktleURvd249e3RoaXMuX29uUmVhY3RLZXlEb3dufVxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0nbXhfTWF0cml4Q2hhdF93cmFwcGVyJ1xyXG4gICAgICAgICAgICAgICAgICAgIGFyaWEtaGlkZGVuPXt0aGlzLnByb3BzLmhpZGVUb1NSVXNlcnN9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Nb3VzZURvd249e3RoaXMuX29uTW91c2VEb3dufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uTW91c2VVcD17dGhpcy5fb25Nb3VzZVVwfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdG9wQmFyIH1cclxuICAgICAgICAgICAgICAgICAgICA8VG9hc3RDb250YWluZXIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8RHJhZ0Ryb3BDb250ZXh0IG9uRHJhZ0VuZD17dGhpcy5fb25EcmFnRW5kfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiByZWY9e3RoaXMuX3NldFJlc2l6ZUNvbnRhaW5lclJlZn0gY2xhc3NOYW1lPXtib2R5Q2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGVmdFBhbmVsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzaXplTm90aWZpZXI9e3RoaXMucHJvcHMucmVzaXplTm90aWZpZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sbGFwc2VkPXt0aGlzLnByb3BzLmNvbGxhcHNlTGhzIHx8IGZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnByb3BzLmxlZnREaXNhYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmVzaXplSGFuZGxlIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHBhZ2VFbGVtZW50IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9EcmFnRHJvcENvbnRleHQ+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9NYXRyaXhDbGllbnRDb250ZXh0LlByb3ZpZGVyPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IExvZ2dlZEluVmlldztcclxuIl19