"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.VIEWS = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var Matrix = _interopRequireWildcard3(require("matrix-js-sdk"));

var _crypto = require("matrix-js-sdk/src/crypto");

require("focus-visible");

require("what-input");

var _Analytics = _interopRequireDefault(require("../../Analytics"));

var _DecryptionFailureTracker = require("../../DecryptionFailureTracker");

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _PlatformPeg = _interopRequireDefault(require("../../PlatformPeg"));

var _SdkConfig = _interopRequireDefault(require("../../SdkConfig"));

var RoomListSorter = _interopRequireWildcard3(require("../../RoomListSorter"));

var _dispatcher = _interopRequireDefault(require("../../dispatcher"));

var _Notifier = _interopRequireDefault(require("../../Notifier"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var _Tinter = _interopRequireDefault(require("../../Tinter"));

var sdk = _interopRequireWildcard3(require("../../index"));

var _RoomInvite = require("../../RoomInvite");

var Rooms = _interopRequireWildcard3(require("../../Rooms"));

var _linkifyMatrix = _interopRequireDefault(require("../../linkify-matrix"));

var Lifecycle = _interopRequireWildcard3(require("../../Lifecycle"));

require("../../stores/LifecycleStore");

var _PageTypes = _interopRequireDefault(require("../../PageTypes"));

var _pages = require("../../utils/pages");

var _createRoom = _interopRequireDefault(require("../../createRoom"));

var _KeyRequestHandler = _interopRequireDefault(require("../../KeyRequestHandler"));

var _languageHandler = require("../../languageHandler");

var _SettingsStore = _interopRequireWildcard3(require("../../settings/SettingsStore"));

var _ThemeController = _interopRequireDefault(require("../../settings/controllers/ThemeController"));

var _Registration = require("../../Registration.js");

var _ErrorUtils = require("../../utils/ErrorUtils");

var _ResizeNotifier = _interopRequireDefault(require("../../utils/ResizeNotifier"));

var _AutoDiscoveryUtils = _interopRequireWildcard3(require("../../utils/AutoDiscoveryUtils"));

var _DMRoomMap = _interopRequireDefault(require("../../utils/DMRoomMap"));

var _RoomNotifs = require("../../RoomNotifs");

var _theme = require("../../theme");

var _RoomAliasCache = require("../../RoomAliasCache");

var _promise = require("../../utils/promise");

var _ToastStore = _interopRequireDefault(require("../../stores/ToastStore"));

var StorageManager = _interopRequireWildcard3(require("../../utils/StorageManager"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2017-2019 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// focus-visible is a Polyfill for the :focus-visible CSS pseudo-attribute used by _AccessibleButton.scss
// what-input helps improve keyboard accessibility
// LifecycleStore is not used but does listen to and dispatch actions

/** constants for MatrixChat.state.view */
const VIEWS = {
  // a special initial state which is only used at startup, while we are
  // trying to re-animate a matrix client or register as a guest.
  LOADING: 0,
  // we are showing the welcome view
  WELCOME: 1,
  // we are showing the login view
  LOGIN: 2,
  // we are showing the registration view
  REGISTER: 3,
  // completing the registration flow
  POST_REGISTRATION: 4,
  // showing the 'forgot password' view
  FORGOT_PASSWORD: 5,
  // showing flow to trust this new device with cross-signing
  COMPLETE_SECURITY: 6,
  // flow to setup SSSS / cross-signing on this account
  E2E_SETUP: 7,
  // we are logged in with an active matrix client.
  LOGGED_IN: 8,
  // We are logged out (invalid token) but have our local state again. The user
  // should log back in to rehydrate the client.
  SOFT_LOGOUT: 9
}; // Actions that are redirected through the onboarding process prior to being
// re-dispatched. NOTE: some actions are non-trivial and would require
// re-factoring to be included in this list in future.

exports.VIEWS = VIEWS;
const ONBOARDING_FLOW_STARTERS = ['view_user_settings', 'view_create_chat', 'view_create_room', 'view_create_group'];

var _default = (0, _createReactClass.default)({
  // we export this so that the integration tests can use it :-S
  statics: {
    VIEWS: VIEWS
  },
  displayName: 'MatrixChat',
  propTypes: {
    config: _propTypes.default.object,
    serverConfig: _propTypes.default.instanceOf(_AutoDiscoveryUtils.ValidatedServerConfig),
    ConferenceHandler: _propTypes.default.any,
    onNewScreen: _propTypes.default.func,
    registrationUrl: _propTypes.default.string,
    enableGuest: _propTypes.default.bool,
    // the queryParams extracted from the [real] query-string of the URI
    realQueryParams: _propTypes.default.object,
    // the initial queryParams extracted from the hash-fragment of the URI
    startingFragmentQueryParams: _propTypes.default.object,
    test: 'test',
    // called when we have completed a token login
    onTokenLoginCompleted: _propTypes.default.func,
    // Represents the screen to display as a result of parsing the initial
    // window.location
    initialScreenAfterLogin: _propTypes.default.shape({
      screen: _propTypes.default.string.isRequired,
      params: _propTypes.default.object
    }),
    // displayname, if any, to set on the device when logging
    // in/registering.
    defaultDeviceDisplayName: _propTypes.default.string,
    // A function that makes a registration URL
    makeRegistrationUrl: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    const s = {
      // the master view we are showing.
      view: VIEWS.LOADING,
      // What the LoggedInView would be showing if visible
      page_type: null,
      // The ID of the room we're viewing. This is either populated directly
      // in the case where we view a room by ID or by RoomView when it resolves
      // what ID an alias points at.
      currentRoomId: null,
      // If we're trying to just view a user ID (i.e. /user URL), this is it
      viewUserId: null,
      // this is persisted as mx_lhs_size, loaded in LoggedInView
      collapseLhs: false,
      leftDisabled: false,
      middleDisabled: false,
      // the right panel's disabled state is tracked in its store.
      version: null,
      newVersion: null,
      hasNewVersion: false,
      newVersionReleaseNotes: null,
      checkingForUpdate: null,
      showCookieBar: false,
      // Parameters used in the registration dance with the IS
      register_client_secret: null,
      register_session_id: null,
      register_id_sid: null,
      // When showing Modal dialogs we need to set aria-hidden on the root app element
      // and disable it when there are no dialogs
      hideToSRUsers: false,
      syncError: null,
      // If the current syncing status is ERROR, the error object, otherwise null.
      resizeNotifier: new _ResizeNotifier.default(),
      showNotifierToolbar: false
    };
    return s;
  },
  getDefaultProps: function () {
    return {
      realQueryParams: {},
      startingFragmentQueryParams: {},
      config: {},
      onTokenLoginCompleted: () => {}
    };
  },
  getFallbackHsUrl: function () {
    if (this.props.serverConfig && this.props.serverConfig.isDefault) {
      return this.props.config.fallback_hs_url;
    } else {
      return null;
    }
  },

  getServerProperties() {
    let props = this.state.serverConfig;
    if (!props) props = this.props.serverConfig; // for unit tests

    if (!props) props = _SdkConfig.default.get()["validated_server_config"];
    return {
      serverConfig: props
    };
  },

  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    _SdkConfig.default.put(this.props.config); // Used by _viewRoom before getting state from sync


    this.firstSyncComplete = false;
    this.firstSyncPromise = (0, _promise.defer)();

    if (this.props.config.sync_timeline_limit) {
      _MatrixClientPeg.MatrixClientPeg.opts.initialSyncLimit = this.props.config.sync_timeline_limit;
    } // a thing to call showScreen with once login completes.  this is kept
    // outside this.state because updating it should never trigger a
    // rerender.


    this._screenAfterLogin = this.props.initialScreenAfterLogin;
    this._windowWidth = 10000;
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
    this._pageChanging = false; // check we have the right tint applied for this theme.
    // N.B. we don't call the whole of setTheme() here as we may be
    // racing with the theme CSS download finishing from index.js

    _Tinter.default.tint(); // For PersistentElement


    this.state.resizeNotifier.on("middlePanelResized", this._dispatchTimelineResize); // Force users to go through the soft logout page if they're soft logged out

    if (Lifecycle.isSoftLogout()) {
      // When the session loads it'll be detected as soft logged out and a dispatch
      // will be sent out to say that, triggering this MatrixChat to show the soft
      // logout page.
      Lifecycle.loadSession({});
    }

    this._accountPassword = null;
    this._accountPasswordTimer = null;
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    this._themeWatcher = new _theme.ThemeWatcher();

    this._themeWatcher.start();

    this.focusComposer = false; // object field used for tracking the status info appended to the title tag.
    // we don't do it as react state as i'm scared about triggering needless react refreshes.

    this.subTitleStatus = ''; // this can technically be done anywhere but doing this here keeps all
    // the routing url path logic together.

    if (this.onAliasClick) {
      _linkifyMatrix.default.onAliasClick = this.onAliasClick;
    }

    if (this.onUserClick) {
      _linkifyMatrix.default.onUserClick = this.onUserClick;
    }

    if (this.onGroupClick) {
      _linkifyMatrix.default.onGroupClick = this.onGroupClick;
    } // the first thing to do is to try the token params in the query-string
    // if the session isn't soft logged out (ie: is a clean session being logged in)


    if (!Lifecycle.isSoftLogout()) {
      Lifecycle.attemptTokenLogin(this.props.realQueryParams, this.props.defaultDeviceDisplayName).then(loggedIn => {
        if (loggedIn) {
          this.props.onTokenLoginCompleted(); // don't do anything else until the page reloads - just stay in
          // the 'loading' state.

          return;
        } // if the user has followed a login or register link, don't reanimate
        // the old creds, but rather go straight to the relevant page


        const firstScreen = this._screenAfterLogin ? this._screenAfterLogin.screen : null;

        if (firstScreen === 'login' || firstScreen === 'register' || firstScreen === 'forgot_password') {
          this._showScreenAfterLogin();

          return;
        }

        return this._loadSession();
      });
    }

    if (_SettingsStore.default.getValue("showCookieBar")) {
      this.setState({
        showCookieBar: true
      });
    }

    if (_SettingsStore.default.getValue("analyticsOptIn")) {
      _Analytics.default.enable();
    }
  },
  _loadSession: function () {
    // the extra Promise.resolve() ensures that synchronous exceptions hit the same codepath as
    // asynchronous ones.
    return Promise.resolve().then(() => {
      return Lifecycle.loadSession({
        fragmentQueryParams: this.props.startingFragmentQueryParams,
        enableGuest: this.props.enableGuest,
        guestHsUrl: this.getServerProperties().serverConfig.hsUrl,
        guestIsUrl: this.getServerProperties().serverConfig.isUrl,
        defaultDeviceDisplayName: this.props.defaultDeviceDisplayName
      });
    }).then(loadedSession => {
      if (!loadedSession) {
        // fall back to showing the welcome screen
        _dispatcher.default.dispatch({
          action: "view_welcome_page"
        });
      }
    }); // Note we don't catch errors from this: we catch everything within
    // loadSession as there's logic there to ask the user if they want
    // to try logging out.
  },
  componentWillUnmount: function () {
    Lifecycle.stopMatrixClient();

    _dispatcher.default.unregister(this.dispatcherRef);

    this._themeWatcher.stop();

    window.removeEventListener("focus", this.onFocus);
    window.removeEventListener('resize', this.handleResize);
    this.state.resizeNotifier.removeListener("middlePanelResized", this._dispatchTimelineResize);
    if (this._accountPasswordTimer !== null) clearTimeout(this._accountPasswordTimer);
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle stage
  UNSAFE_componentWillUpdate: function (props, state) {
    if (this.shouldTrackPageChange(this.state, state)) {
      this.startPageChangeTimer();
    }
  },
  componentDidUpdate: function (prevProps, prevState) {
    if (this.shouldTrackPageChange(prevState, this.state)) {
      const durationMs = this.stopPageChangeTimer();

      _Analytics.default.trackPageChange(durationMs);
    }

    if (this.focusComposer) {
      _dispatcher.default.dispatch({
        action: 'focus_composer'
      });

      this.focusComposer = false;
    }
  },

  startPageChangeTimer() {
    // Tor doesn't support performance
    if (!performance || !performance.mark) return null; // This shouldn't happen because UNSAFE_componentWillUpdate and componentDidUpdate
    // are used.

    if (this._pageChanging) {
      console.warn('MatrixChat.startPageChangeTimer: timer already started');
      return;
    }

    this._pageChanging = true;
    performance.mark('riot_MatrixChat_page_change_start');
  },

  stopPageChangeTimer() {
    // Tor doesn't support performance
    if (!performance || !performance.mark) return null;

    if (!this._pageChanging) {
      console.warn('MatrixChat.stopPageChangeTimer: timer not started');
      return;
    }

    this._pageChanging = false;
    performance.mark('riot_MatrixChat_page_change_stop');
    performance.measure('riot_MatrixChat_page_change_delta', 'riot_MatrixChat_page_change_start', 'riot_MatrixChat_page_change_stop');
    performance.clearMarks('riot_MatrixChat_page_change_start');
    performance.clearMarks('riot_MatrixChat_page_change_stop');
    const measurement = performance.getEntriesByName('riot_MatrixChat_page_change_delta').pop(); // In practice, sometimes the entries list is empty, so we get no measurement

    if (!measurement) return null;
    return measurement.duration;
  },

  shouldTrackPageChange(prevState, state) {
    return prevState.currentRoomId !== state.currentRoomId || prevState.view !== state.view || prevState.page_type !== state.page_type;
  },

  setStateForNewView: function (state) {
    if (state.view === undefined) {
      throw new Error("setStateForNewView with no view!");
    }

    const newState = {
      viewUserId: null
    };
    Object.assign(newState, state);
    this.setState(newState);
  },
  onAction: function (payload) {
    // console.log(`MatrixClientPeg.onAction: ${payload.action}`);
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog"); // Start the onboarding process for certain actions

    if (_MatrixClientPeg.MatrixClientPeg.get() && _MatrixClientPeg.MatrixClientPeg.get().isGuest() && ONBOARDING_FLOW_STARTERS.includes(payload.action)) {
      // This will cause `payload` to be dispatched later, once a
      // sync has reached the "prepared" state. Setting a matrix ID
      // will cause a full login and sync and finally the deferred
      // action will be dispatched.
      _dispatcher.default.dispatch({
        action: 'do_after_sync_prepared',
        deferred_action: payload
      });

      _dispatcher.default.dispatch({
        action: 'require_registration'
      });

      return;
    }

    switch (payload.action) {
      case 'MatrixActions.accountData':
        // XXX: This is a collection of several hacks to solve a minor problem. We want to
        // update our local state when the ID server changes, but don't want to put that in
        // the js-sdk as we'd be then dictating how all consumers need to behave. However,
        // this component is already bloated and we probably don't want this tiny logic in
        // here, but there's no better place in the react-sdk for it. Additionally, we're
        // abusing the MatrixActionCreator stuff to avoid errors on dispatches.
        if (payload.event_type === 'm.identity_server') {
          const fullUrl = payload.event_content ? payload.event_content['base_url'] : null;

          if (!fullUrl) {
            _MatrixClientPeg.MatrixClientPeg.get().setIdentityServerUrl(null);

            localStorage.removeItem("mx_is_access_token");
            localStorage.removeItem("mx_is_url");
          } else {
            _MatrixClientPeg.MatrixClientPeg.get().setIdentityServerUrl(fullUrl);

            localStorage.removeItem("mx_is_access_token"); // clear token

            localStorage.setItem("mx_is_url", fullUrl); // XXX: Do we still need this?
          } // redispatch the change with a more specific action


          _dispatcher.default.dispatch({
            action: 'id_server_changed'
          });
        }

        break;

      case 'logout':
        Lifecycle.logout();
        break;

      case 'require_registration':
        (0, _Registration.startAnyRegistrationFlow)(payload);
        break;

      case 'start_registration':
        if (Lifecycle.isSoftLogout()) {
          this._onSoftLogout();

          break;
        } // This starts the full registration flow


        if (payload.screenAfterLogin) {
          this._screenAfterLogin = payload.screenAfterLogin;
        }

        this._startRegistration(payload.params || {});

        break;

      case 'start_login':
        if (Lifecycle.isSoftLogout()) {
          this._onSoftLogout();

          break;
        }

        if (payload.screenAfterLogin) {
          this._screenAfterLogin = payload.screenAfterLogin;
        }

        this.setStateForNewView({
          view: VIEWS.LOGIN
        });
        this.notifyNewScreen('login');
        _ThemeController.default.isLogin = true;

        this._themeWatcher.recheck();

        break;

      case 'start_post_registration':
        this.setState({
          view: VIEWS.POST_REGISTRATION
        });
        break;

      case 'start_password_recovery':
        this.setStateForNewView({
          view: VIEWS.FORGOT_PASSWORD
        });
        this.notifyNewScreen('forgot_password');
        break;

      case 'start_chat':
        (0, _createRoom.default)({
          dmUserId: payload.user_id
        });
        break;

      case 'leave_room':
        this._leaveRoom(payload.room_id);

        break;

      case 'reject_invite':
        _Modal.default.createTrackedDialog('Reject invitation', '', QuestionDialog, {
          title: (0, _languageHandler._t)('Reject invitation'),
          description: (0, _languageHandler._t)('Are you sure you want to reject the invitation?'),
          onFinished: confirm => {
            if (confirm) {
              // FIXME: controller shouldn't be loading a view :(
              const Loader = sdk.getComponent("elements.Spinner");

              const modal = _Modal.default.createDialog(Loader, null, 'mx_Dialog_spinner');

              _MatrixClientPeg.MatrixClientPeg.get().leave(payload.room_id).then(() => {
                modal.close();

                if (this.state.currentRoomId === payload.room_id) {
                  _dispatcher.default.dispatch({
                    action: 'view_next_room'
                  });
                }
              }, err => {
                modal.close();

                _Modal.default.createTrackedDialog('Failed to reject invitation', '', ErrorDialog, {
                  title: (0, _languageHandler._t)('Failed to reject invitation'),
                  description: err.toString()
                });
              });
            }
          }
        });

        break;

      case 'view_user_info':
        this._viewUser(payload.userId, payload.subAction);

        break;

      case 'view_room':
        {
          // Takes either a room ID or room alias: if switching to a room the client is already
          // known to be in (eg. user clicks on a room in the recents panel), supply the ID
          // If the user is clicking on a room in the context of the alias being presented
          // to them, supply the room alias. If both are supplied, the room ID will be ignored.
          const promise = this._viewRoom(payload);

          if (payload.deferred_action) {
            promise.then(() => {
              _dispatcher.default.dispatch(payload.deferred_action);
            });
          }

          break;
        }

      case 'view_prev_room':
        this._viewNextRoom(-1);

        break;

      case 'view_next_room':
        this._viewNextRoom(1);

        break;

      case 'view_indexed_room':
        this._viewIndexedRoom(payload.roomIndex);

        break;

      case 'view_user_settings':
        {
          const UserSettingsDialog = sdk.getComponent("dialogs.UserSettingsDialog");

          _Modal.default.createTrackedDialog('User settings', '', UserSettingsDialog, {},
          /*className=*/
          null,
          /*isPriority=*/
          false,
          /*isStatic=*/
          true); // View the welcome or home page if we need something to look at


          this._viewSomethingBehindModal();

          break;
        }

      case 'view_create_room':
        this._createRoom();

        break;

      case 'view_create_group':
        {
          const CreateGroupDialog = sdk.getComponent("dialogs.CreateGroupDialog");

          _Modal.default.createTrackedDialog('Create Community', '', CreateGroupDialog);
        }
        break;

      case 'view_room_directory':
        {
          const RoomDirectory = sdk.getComponent("structures.RoomDirectory");

          _Modal.default.createTrackedDialog('Room directory', '', RoomDirectory, {}, 'mx_RoomDirectory_dialogWrapper', false, true); // View the welcome or home page if we need something to look at


          this._viewSomethingBehindModal();
        }
        break;

      case 'view_my_groups':
        this._setPage(_PageTypes.default.MyGroups);

        this.notifyNewScreen('groups');
        break;

      case 'view_group':
        this._viewGroup(payload);

        break;

      case 'view_welcome_page':
        this._viewWelcome();

        break;

      case 'view_home_page':
        this._viewHome();

        break;

      case 'view_set_mxid':
        this._setMxId(payload);

        break;

      case 'view_start_chat_or_reuse':
        this._chatCreateOrReuse(payload.user_id);

        break;

      case 'view_create_chat':
        (0, _RoomInvite.showStartChatInviteDialog)();
        break;

      case 'view_invite':
        (0, _RoomInvite.showRoomInviteDialog)(payload.roomId);
        break;

      case 'view_last_screen':
        // This function does what we want, despite the name. The idea is that it shows
        // the last room we were looking at or some reasonable default/guess. We don't
        // have to worry about email invites or similar being re-triggered because the
        // function will have cleared that state and not execute that path.
        this._showScreenAfterLogin();

        break;

      case 'toggle_my_groups':
        // We just dispatch the page change rather than have to worry about
        // what the logic is for each of these branches.
        if (this.state.page_type === _PageTypes.default.MyGroups) {
          _dispatcher.default.dispatch({
            action: 'view_last_screen'
          });
        } else {
          _dispatcher.default.dispatch({
            action: 'view_my_groups'
          });
        }

        break;

      case 'notifier_enabled':
        {
          this.setState({
            showNotifierToolbar: _Notifier.default.shouldShowToolbar()
          });
        }
        break;

      case 'hide_left_panel':
        this.setState({
          collapseLhs: true
        });
        break;

      case 'focus_room_filter': // for CtrlOrCmd+K to work by expanding the left panel first

      case 'show_left_panel':
        this.setState({
          collapseLhs: false
        });
        break;

      case 'panel_disable':
        {
          this.setState({
            leftDisabled: payload.leftDisabled || payload.sideDisabled || false,
            middleDisabled: payload.middleDisabled || false // We don't track the right panel being disabled here - it's tracked in the store.

          });
          break;
        }

      case 'on_logged_in':
        if (!Lifecycle.isSoftLogout() && this.state.view !== VIEWS.LOGIN && this.state.view !== VIEWS.REGISTER && this.state.view !== VIEWS.COMPLETE_SECURITY && this.state.view !== VIEWS.E2E_SETUP) {
          this._onLoggedIn();
        }

        break;

      case 'on_client_not_viable':
        this._onSoftLogout();

        break;

      case 'on_logged_out':
        this._onLoggedOut();

        break;

      case 'will_start_client':
        this.setState({
          ready: false
        }, () => {
          // if the client is about to start, we are, by definition, not ready.
          // Set ready to false now, then it'll be set to true when the sync
          // listener we set below fires.
          this._onWillStartClient();
        });
        break;

      case 'client_started':
        this._onClientStarted();

        break;

      case 'new_version':
        this.onVersion(payload.currentVersion, payload.newVersion, payload.releaseNotes);
        break;

      case 'check_updates':
        this.setState({
          checkingForUpdate: payload.value
        });
        break;

      case 'send_event':
        this.onSendEvent(payload.room_id, payload.event);
        break;

      case 'aria_hide_main_app':
        this.setState({
          hideToSRUsers: true
        });
        break;

      case 'aria_unhide_main_app':
        this.setState({
          hideToSRUsers: false
        });
        break;

      case 'accept_cookies':
        _SettingsStore.default.setValue("analyticsOptIn", null, _SettingsStore.SettingLevel.DEVICE, true);

        _SettingsStore.default.setValue("showCookieBar", null, _SettingsStore.SettingLevel.DEVICE, false);

        this.setState({
          showCookieBar: false
        });

        _Analytics.default.enable();

        break;

      case 'reject_cookies':
        _SettingsStore.default.setValue("analyticsOptIn", null, _SettingsStore.SettingLevel.DEVICE, false);

        _SettingsStore.default.setValue("showCookieBar", null, _SettingsStore.SettingLevel.DEVICE, false);

        this.setState({
          showCookieBar: false
        });
        break;
    }
  },
  _setPage: function (pageType) {
    this.setState({
      page_type: pageType
    });
  },
  _startRegistration: async function (params) {
    const newState = {
      view: VIEWS.REGISTER
    }; // Only honour params if they are all present, otherwise we reset
    // HS and IS URLs when switching to registration.

    if (params.client_secret && params.session_id && params.hs_url && params.is_url && params.sid) {
      newState.serverConfig = await _AutoDiscoveryUtils.default.validateServerConfigWithStaticUrls(params.hs_url, params.is_url);
      newState.register_client_secret = params.client_secret;
      newState.register_session_id = params.session_id;
      newState.register_id_sid = params.sid;
    }

    this.setStateForNewView(newState);
    _ThemeController.default.isLogin = true;

    this._themeWatcher.recheck();

    this.notifyNewScreen('register');
  },
  // TODO: Move to RoomViewStore
  _viewNextRoom: function (roomIndexDelta) {
    const allRooms = RoomListSorter.mostRecentActivityFirst(_MatrixClientPeg.MatrixClientPeg.get().getRooms()); // If there are 0 rooms or 1 room, view the home page because otherwise
    // if there are 0, we end up trying to index into an empty array, and
    // if there is 1, we end up viewing the same room.

    if (allRooms.length < 2) {
      _dispatcher.default.dispatch({
        action: 'view_home_page'
      });

      return;
    }

    let roomIndex = -1;

    for (let i = 0; i < allRooms.length; ++i) {
      if (allRooms[i].roomId == this.state.currentRoomId) {
        roomIndex = i;
        break;
      }
    }

    roomIndex = (roomIndex + roomIndexDelta) % allRooms.length;
    if (roomIndex < 0) roomIndex = allRooms.length - 1;

    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: allRooms[roomIndex].roomId
    });
  },
  // TODO: Move to RoomViewStore
  _viewIndexedRoom: function (roomIndex) {
    const allRooms = RoomListSorter.mostRecentActivityFirst(_MatrixClientPeg.MatrixClientPeg.get().getRooms());

    if (allRooms[roomIndex]) {
      _dispatcher.default.dispatch({
        action: 'view_room',
        room_id: allRooms[roomIndex].roomId
      });
    }
  },
  // switch view to the given room
  //
  // @param {Object} roomInfo Object containing data about the room to be joined
  // @param {string=} roomInfo.room_id ID of the room to join. One of room_id or room_alias must be given.
  // @param {string=} roomInfo.room_alias Alias of the room to join. One of room_id or room_alias must be given.
  // @param {boolean=} roomInfo.auto_join If true, automatically attempt to join the room if not already a member.
  // @param {string=} roomInfo.event_id ID of the event in this room to show: this will cause a switch to the
  //                                    context of that particular event.
  // @param {boolean=} roomInfo.highlighted If true, add event_id to the hash of the URL
  //                                        and alter the EventTile to appear highlighted.
  // @param {Object=} roomInfo.third_party_invite Object containing data about the third party
  //                                    we received to join the room, if any.
  // @param {string=} roomInfo.third_party_invite.inviteSignUrl 3pid invite sign URL
  // @param {string=} roomInfo.third_party_invite.invitedEmail The email address the invite was sent to
  // @param {Object=} roomInfo.oob_data Object of additional data about the room
  //                               that has been passed out-of-band (eg.
  //                               room name and avatar from an invite email)
  _viewRoom: function (roomInfo) {
    this.focusComposer = true;
    const newState = {
      view: VIEWS.LOGGED_IN,
      currentRoomId: roomInfo.room_id || null,
      page_type: _PageTypes.default.RoomView,
      thirdPartyInvite: roomInfo.third_party_invite,
      roomOobData: roomInfo.oob_data,
      viaServers: roomInfo.via_servers
    };

    if (roomInfo.room_alias) {
      console.log("Switching to room alias ".concat(roomInfo.room_alias, " at event ") + roomInfo.event_id);
    } else {
      console.log("Switching to room id ".concat(roomInfo.room_id, " at event ") + roomInfo.event_id);
    } // Wait for the first sync to complete so that if a room does have an alias,
    // it would have been retrieved.


    let waitFor = Promise.resolve(null);

    if (!this.firstSyncComplete) {
      if (!this.firstSyncPromise) {
        console.warn('Cannot view a room before first sync. room_id:', roomInfo.room_id);
        return;
      }

      waitFor = this.firstSyncPromise.promise;
    }

    return waitFor.then(() => {
      let presentedId = roomInfo.room_alias || roomInfo.room_id;

      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomInfo.room_id);

      if (room) {
        const theAlias = Rooms.getDisplayAliasForRoom(room);

        if (theAlias) {
          presentedId = theAlias; // Store display alias of the presented room in cache to speed future
          // navigation.

          (0, _RoomAliasCache.storeRoomAliasInCache)(theAlias, room.roomId);
        } // Store this as the ID of the last room accessed. This is so that we can
        // persist which room is being stored across refreshes and browser quits.


        if (localStorage) {
          localStorage.setItem('mx_last_room_id', room.roomId);
        }
      }

      if (roomInfo.event_id && roomInfo.highlighted) {
        presentedId += "/" + roomInfo.event_id;
      }

      newState.ready = true;
      this.setState(newState, () => {
        this.notifyNewScreen('room/' + presentedId);
      });
    });
  },
  _viewGroup: function (payload) {
    const groupId = payload.group_id;
    this.setState({
      currentGroupId: groupId,
      currentGroupIsNew: payload.group_is_new
    });

    this._setPage(_PageTypes.default.GroupView);

    this.notifyNewScreen('group/' + groupId);
  },

  _viewSomethingBehindModal() {
    if (this.state.view !== VIEWS.LOGGED_IN) {
      this._viewWelcome();

      return;
    }

    if (!this.state.currentGroupId && !this.state.currentRoomId) {
      this._viewHome();
    }
  },

  _viewWelcome() {
    this.setStateForNewView({
      view: VIEWS.WELCOME
    });
    this.notifyNewScreen('welcome');
    _ThemeController.default.isLogin = true;

    this._themeWatcher.recheck();
  },

  _viewHome: function () {
    // The home page requires the "logged in" view, so we'll set that.
    this.setStateForNewView({
      view: VIEWS.LOGGED_IN
    });

    this._setPage(_PageTypes.default.HomePage);

    this.notifyNewScreen('home');
    _ThemeController.default.isLogin = false;

    this._themeWatcher.recheck();
  },
  _viewUser: function (userId, subAction) {
    // Wait for the first sync so that `getRoom` gives us a room object if it's
    // in the sync response
    const waitForSync = this.firstSyncPromise ? this.firstSyncPromise.promise : Promise.resolve();
    waitForSync.then(() => {
      if (subAction === 'chat') {
        this._chatCreateOrReuse(userId);

        return;
      }

      this.notifyNewScreen('user/' + userId);
      this.setState({
        currentUserId: userId
      });

      this._setPage(_PageTypes.default.UserView);
    });
  },
  _setMxId: function (payload) {
    const SetMxIdDialog = sdk.getComponent('views.dialogs.SetMxIdDialog');

    const close = _Modal.default.createTrackedDialog('Set MXID', '', SetMxIdDialog, {
      homeserverUrl: _MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl(),
      onFinished: (submitted, credentials) => {
        if (!submitted) {
          _dispatcher.default.dispatch({
            action: 'cancel_after_sync_prepared'
          });

          if (payload.go_home_on_cancel) {
            _dispatcher.default.dispatch({
              action: 'view_home_page'
            });
          }

          return;
        }

        _MatrixClientPeg.MatrixClientPeg.setJustRegisteredUserId(credentials.user_id);

        this.onRegistered(credentials);
      },
      onDifferentServerClicked: ev => {
        _dispatcher.default.dispatch({
          action: 'start_registration'
        });

        close();
      },
      onLoginClick: ev => {
        _dispatcher.default.dispatch({
          action: 'start_login'
        });

        close();
      }
    }).close;
  },
  _createRoom: async function () {
    const CreateRoomDialog = sdk.getComponent('dialogs.CreateRoomDialog');

    const modal = _Modal.default.createTrackedDialog('Create Room', '', CreateRoomDialog);

    const [shouldCreate, opts] = await modal.finished;

    if (shouldCreate) {
      (0, _createRoom.default)(opts);
    }
  },
  _chatCreateOrReuse: function (userId) {
    // Use a deferred action to reshow the dialog once the user has registered
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
      // No point in making 2 DMs with welcome bot. This assumes view_set_mxid will
      // result in a new DM with the welcome user.
      if (userId !== this.props.config.welcomeUserId) {
        _dispatcher.default.dispatch({
          action: 'do_after_sync_prepared',
          deferred_action: {
            action: 'view_start_chat_or_reuse',
            user_id: userId
          }
        });
      }

      _dispatcher.default.dispatch({
        action: 'require_registration',
        // If the set_mxid dialog is cancelled, view /welcome because if the
        // browser was pointing at /user/@someone:domain?action=chat, the URL
        // needs to be reset so that they can revisit /user/.. // (and trigger
        // `_chatCreateOrReuse` again)
        go_welcome_on_cancel: true,
        screen_after: {
          screen: "user/".concat(this.props.config.welcomeUserId),
          params: {
            action: 'chat'
          }
        }
      });

      return;
    } // TODO: Immutable DMs replaces this


    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const dmRoomMap = new _DMRoomMap.default(client);
    const dmRooms = dmRoomMap.getDMRoomsForUserId(userId);

    if (dmRooms.length > 0) {
      _dispatcher.default.dispatch({
        action: 'view_room',
        room_id: dmRooms[0]
      });
    } else {
      _dispatcher.default.dispatch({
        action: 'start_chat',
        user_id: userId
      });
    }
  },
  _leaveRoomWarnings: function (roomId) {
    const roomToLeave = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId); // Show a warning if there are additional complications.


    const joinRules = roomToLeave.currentState.getStateEvents('m.room.join_rules', '');
    const warnings = [];

    if (joinRules) {
      const rule = joinRules.getContent().join_rule;

      if (rule !== "public") {
        warnings.push(_react.default.createElement("span", {
          className: "warning",
          key: "non_public_warning"
        }, ' '
        /* Whitespace, otherwise the sentences get smashed together */
        , (0, _languageHandler._t)("This room is not public. You will not be able to rejoin without an invite.")));
      }
    }

    return warnings;
  },
  _leaveRoom: function (roomId) {
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

    const roomToLeave = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

    const warnings = this._leaveRoomWarnings(roomId);

    _Modal.default.createTrackedDialog('Leave room', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Leave room"),
      description: _react.default.createElement("span", null, (0, _languageHandler._t)("Are you sure you want to leave the room '%(roomName)s'?", {
        roomName: roomToLeave.name
      }), warnings),
      button: (0, _languageHandler._t)("Leave"),
      onFinished: shouldLeave => {
        if (shouldLeave) {
          const d = _MatrixClientPeg.MatrixClientPeg.get().leaveRoomChain(roomId); // FIXME: controller shouldn't be loading a view :(


          const Loader = sdk.getComponent("elements.Spinner");

          const modal = _Modal.default.createDialog(Loader, null, 'mx_Dialog_spinner');

          d.then(errors => {
            modal.close();

            for (const leftRoomId of Object.keys(errors)) {
              const err = errors[leftRoomId];
              if (!err) continue;
              console.error("Failed to leave room " + leftRoomId + " " + err);
              let title = (0, _languageHandler._t)("Failed to leave room");
              let message = (0, _languageHandler._t)("Server may be unavailable, overloaded, or you hit a bug.");

              if (err.errcode === 'M_CANNOT_LEAVE_SERVER_NOTICE_ROOM') {
                title = (0, _languageHandler._t)("Can't leave Server Notices room");
                message = (0, _languageHandler._t)("This room is used for important messages from the Homeserver, " + "so you cannot leave it.");
              } else if (err && err.message) {
                message = err.message;
              }

              _Modal.default.createTrackedDialog('Failed to leave room', '', ErrorDialog, {
                title: title,
                description: message
              });

              return;
            }

            if (this.state.currentRoomId === roomId) {
              _dispatcher.default.dispatch({
                action: 'view_next_room'
              });
            }
          }, err => {
            // This should only happen if something went seriously wrong with leaving the chain.
            modal.close();
            console.error("Failed to leave room " + roomId + " " + err);

            _Modal.default.createTrackedDialog('Failed to leave room', '', ErrorDialog, {
              title: (0, _languageHandler._t)("Failed to leave room"),
              description: (0, _languageHandler._t)("Unknown error")
            });
          });
        }
      }
    });
  },

  /**
   * Starts a chat with the welcome user, if the user doesn't already have one
   * @returns {string} The room ID of the new room, or null if no room was created
   */
  async _startWelcomeUserChat() {
    // We can end up with multiple tabs post-registration where the user
    // might then end up with a session and we don't want them all making
    // a chat with the welcome user: try to de-dupe.
    // We need to wait for the first sync to complete for this to
    // work though.
    let waitFor;

    if (!this.firstSyncComplete) {
      waitFor = this.firstSyncPromise.promise;
    } else {
      waitFor = Promise.resolve();
    }

    await waitFor;

    const welcomeUserRooms = _DMRoomMap.default.shared().getDMRoomsForUserId(this.props.config.welcomeUserId);

    if (welcomeUserRooms.length === 0) {
      const roomId = await (0, _createRoom.default)({
        dmUserId: this.props.config.welcomeUserId,
        // Only view the welcome user if we're NOT looking at a room
        andView: !this.state.currentRoomId,
        spinner: false // we're already showing one: we don't need another one

      }); // This is a bit of a hack, but since the deduplication relies
      // on m.direct being up to date, we need to force a sync
      // of the database, otherwise if the user goes to the other
      // tab before the next save happens (a few minutes), the
      // saved sync will be restored from the db and this code will
      // run without the update to m.direct, making another welcome
      // user room (it doesn't wait for new data from the server, just
      // the saved sync to be loaded).

      const saveWelcomeUser = ev => {
        if (ev.getType() == 'm.direct' && ev.getContent() && ev.getContent()[this.props.config.welcomeUserId]) {
          _MatrixClientPeg.MatrixClientPeg.get().store.save(true);

          _MatrixClientPeg.MatrixClientPeg.get().removeListener("accountData", saveWelcomeUser);
        }
      };

      _MatrixClientPeg.MatrixClientPeg.get().on("accountData", saveWelcomeUser);

      return roomId;
    }

    return null;
  },

  /**
   * Called when a new logged in session has started
   */
  _onLoggedIn: async function () {
    _ThemeController.default.isLogin = false;
    this.setStateForNewView({
      view: VIEWS.LOGGED_IN
    }); // If a specific screen is set to be shown after login, show that above
    // all else, as it probably means the user clicked on something already.

    if (this._screenAfterLogin && this._screenAfterLogin.screen) {
      this.showScreen(this._screenAfterLogin.screen, this._screenAfterLogin.params);
      this._screenAfterLogin = null;
    } else if (_MatrixClientPeg.MatrixClientPeg.currentUserIsJustRegistered()) {
      _MatrixClientPeg.MatrixClientPeg.setJustRegisteredUserId(null);

      if (this.props.config.welcomeUserId && (0, _languageHandler.getCurrentLanguage)().startsWith("en")) {
        const welcomeUserRoom = await this._startWelcomeUserChat();

        if (welcomeUserRoom === null) {
          // We didn't redirect to the welcome user room, so show
          // the homepage.
          _dispatcher.default.dispatch({
            action: 'view_home_page'
          });
        }
      } else {
        // The user has just logged in after registering,
        // so show the homepage.
        _dispatcher.default.dispatch({
          action: 'view_home_page'
        });
      }
    } else {
      this._showScreenAfterLogin();
    }

    StorageManager.tryPersistStorage();
  },
  _showScreenAfterLogin: function () {
    // If screenAfterLogin is set, use that, then null it so that a second login will
    // result in view_home_page, _user_settings or _room_directory
    if (this._screenAfterLogin && this._screenAfterLogin.screen) {
      this.showScreen(this._screenAfterLogin.screen, this._screenAfterLogin.params);
      this._screenAfterLogin = null;
    } else if (localStorage && localStorage.getItem('mx_last_room_id')) {
      // Before defaulting to directory, show the last viewed room
      this._viewLastRoom();
    } else {
      if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
        _dispatcher.default.dispatch({
          action: 'view_welcome_page'
        });
      } else if ((0, _pages.getHomePageUrl)(this.props.config)) {
        _dispatcher.default.dispatch({
          action: 'view_home_page'
        });
      } else {
        this.firstSyncPromise.promise.then(() => {
          _dispatcher.default.dispatch({
            action: 'view_next_room'
          });
        });
      }
    }
  },
  _viewLastRoom: function () {
    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: localStorage.getItem('mx_last_room_id')
    });
  },

  /**
   * Called when the session is logged out
   */
  _onLoggedOut: function () {
    this.notifyNewScreen('login');
    this.setStateForNewView({
      view: VIEWS.LOGIN,
      ready: false,
      collapseLhs: false,
      currentRoomId: null
    });
    this.subTitleStatus = '';

    this._setPageSubtitle();

    _ThemeController.default.isLogin = true;

    this._themeWatcher.recheck();
  },

  /**
   * Called when the session is softly logged out
   */
  _onSoftLogout: function () {
    this.notifyNewScreen('soft_logout');
    this.setStateForNewView({
      view: VIEWS.SOFT_LOGOUT,
      ready: false,
      collapseLhs: false,
      currentRoomId: null
    });
    this.subTitleStatus = '';

    this._setPageSubtitle();
  },

  /**
   * Called just before the matrix client is started
   * (useful for setting listeners)
   */
  _onWillStartClient() {
    const self = this; // reset the 'have completed first sync' flag,
    // since we're about to start the client and therefore about
    // to do the first sync

    this.firstSyncComplete = false;
    this.firstSyncPromise = (0, _promise.defer)();

    const cli = _MatrixClientPeg.MatrixClientPeg.get(); // Allow the JS SDK to reap timeline events. This reduces the amount of
    // memory consumed as the JS SDK stores multiple distinct copies of room
    // state (each of which can be 10s of MBs) for each DISJOINT timeline. This is
    // particularly noticeable when there are lots of 'limited' /sync responses
    // such as when laptops unsleep.
    // https://github.com/vector-im/riot-web/issues/3307#issuecomment-282895568


    cli.setCanResetTimelineCallback(function (roomId) {
      console.log("Request to reset timeline in room ", roomId, " viewing:", self.state.currentRoomId);

      if (roomId !== self.state.currentRoomId) {
        // It is safe to remove events from rooms we are not viewing.
        return true;
      } // We are viewing the room which we want to reset. It is only safe to do
      // this if we are not scrolled up in the view. To find out, delegate to
      // the timeline panel. If the timeline panel doesn't exist, then we assume
      // it is safe to reset the timeline.


      if (!self._loggedInView || !self._loggedInView.child) {
        return true;
      }

      return self._loggedInView.child.canResetTimelineInRoom(roomId);
    });
    cli.on('sync', function (state, prevState, data) {
      // LifecycleStore and others cannot directly subscribe to matrix client for
      // events because flux only allows store state changes during flux dispatches.
      // So dispatch directly from here. Ideally we'd use a SyncStateStore that
      // would do this dispatch and expose the sync state itself (by listening to
      // its own dispatch).
      _dispatcher.default.dispatch({
        action: 'sync_state',
        prevState,
        state
      });

      if (state === "ERROR" || state === "RECONNECTING") {
        if (data.error instanceof Matrix.InvalidStoreError) {
          Lifecycle.handleInvalidStoreError(data.error);
        }

        self.setState({
          syncError: data.error || true
        });
      } else if (self.state.syncError) {
        self.setState({
          syncError: null
        });
      }

      self.updateStatusIndicator(state, prevState);

      if (state === "SYNCING" && prevState === "SYNCING") {
        return;
      }

      console.info("MatrixClient sync state => %s", state);

      if (state !== "PREPARED") {
        return;
      }

      self.firstSyncComplete = true;
      self.firstSyncPromise.resolve();

      _dispatcher.default.dispatch({
        action: 'focus_composer'
      });

      self.setState({
        ready: true,
        showNotifierToolbar: _Notifier.default.shouldShowToolbar()
      });
    });
    cli.on('Call.incoming', function (call) {
      // we dispatch this synchronously to make sure that the event
      // handlers on the call are set up immediately (so that if
      // we get an immediate hangup, we don't get a stuck call)
      _dispatcher.default.dispatch({
        action: 'incoming_call',
        call: call
      }, true);
    });
    cli.on('Session.logged_out', function (errObj) {
      if (Lifecycle.isLoggingOut()) return;

      if (errObj.httpStatus === 401 && errObj.data && errObj.data['soft_logout']) {
        console.warn("Soft logout issued by server - avoiding data deletion");
        Lifecycle.softLogout();
        return;
      }

      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Signed out', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Signed Out'),
        description: (0, _languageHandler._t)('For security, this session has been signed out. Please sign in again.')
      });

      _dispatcher.default.dispatch({
        action: 'logout'
      });
    });
    cli.on('no_consent', function (message, consentUri) {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      _Modal.default.createTrackedDialog('No Consent Dialog', '', QuestionDialog, {
        title: (0, _languageHandler._t)('Terms and Conditions'),
        description: _react.default.createElement("div", null, _react.default.createElement("p", null, " ", (0, _languageHandler._t)('To continue using the %(homeserverDomain)s homeserver ' + 'you must review and agree to our terms and conditions.', {
          homeserverDomain: cli.getDomain()
        }))),
        button: (0, _languageHandler._t)('Review terms and conditions'),
        cancelButton: (0, _languageHandler._t)('Dismiss'),
        onFinished: confirmed => {
          if (confirmed) {
            const wnd = window.open(consentUri, '_blank');
            wnd.opener = null;
          }
        }
      }, null, true);
    });
    const dft = new _DecryptionFailureTracker.DecryptionFailureTracker((total, errorCode) => {
      _Analytics.default.trackEvent('E2E', 'Decryption failure', errorCode, total);
    }, errorCode => {
      // Map JS-SDK error codes to tracker codes for aggregation
      switch (errorCode) {
        case 'MEGOLM_UNKNOWN_INBOUND_SESSION_ID':
          return 'olm_keys_not_sent_error';

        case 'OLM_UNKNOWN_MESSAGE_INDEX':
          return 'olm_index_error';

        case undefined:
          return 'unexpected_error';

        default:
          return 'unspecified_error';
      }
    }); // Shelved for later date when we have time to think about persisting history of
    // tracked events across sessions.
    // dft.loadTrackedEventHashMap();

    dft.start(); // When logging out, stop tracking failures and destroy state

    cli.on("Session.logged_out", () => dft.stop());
    cli.on("Event.decrypted", (e, err) => dft.eventDecrypted(e, err)); // TODO: We can remove this once cross-signing is the only way.
    // https://github.com/vector-im/riot-web/issues/11908

    const krh = new _KeyRequestHandler.default(cli);
    cli.on("crypto.roomKeyRequest", req => {
      krh.handleKeyRequest(req);
    });
    cli.on("crypto.roomKeyRequestCancellation", req => {
      krh.handleKeyRequestCancellation(req);
    });
    cli.on("Room", room => {
      if (_MatrixClientPeg.MatrixClientPeg.get().isCryptoEnabled()) {
        const blacklistEnabled = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.ROOM_DEVICE, "blacklistUnverifiedDevices", room.roomId,
        /*explicit=*/
        true);

        room.setBlacklistUnverifiedDevices(blacklistEnabled);
      }
    });
    cli.on("crypto.warning", type => {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      switch (type) {
        case 'CRYPTO_WARNING_OLD_VERSION_DETECTED':
          _Modal.default.createTrackedDialog('Crypto migrated', '', ErrorDialog, {
            title: (0, _languageHandler._t)('Old cryptography data detected'),
            description: (0, _languageHandler._t)("Data from an older version of Riot has been detected. " + "This will have caused end-to-end cryptography to malfunction " + "in the older version. End-to-end encrypted messages exchanged " + "recently whilst using the older version may not be decryptable " + "in this version. This may also cause messages exchanged with this " + "version to fail. If you experience problems, log out and back in " + "again. To retain message history, export and re-import your keys.")
          });

          break;
      }
    });
    cli.on("crypto.keyBackupFailed", async errcode => {
      let haveNewVersion;
      let newVersionInfo; // if key backup is still enabled, there must be a new backup in place

      if (_MatrixClientPeg.MatrixClientPeg.get().getKeyBackupEnabled()) {
        haveNewVersion = true;
      } else {
        // otherwise check the server to see if there's a new one
        try {
          newVersionInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
          if (newVersionInfo !== null) haveNewVersion = true;
        } catch (e) {
          console.error("Saw key backup error but failed to check backup version!", e);
          return;
        }
      }

      if (haveNewVersion) {
        _Modal.default.createTrackedDialogAsync('New Recovery Method', 'New Recovery Method', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../async-components/views/dialogs/keybackup/NewRecoveryMethodDialog'))), {
          newVersionInfo
        });
      } else {
        _Modal.default.createTrackedDialogAsync('Recovery Method Removed', 'Recovery Method Removed', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../async-components/views/dialogs/keybackup/RecoveryMethodRemovedDialog'))));
      }
    });
    cli.on("crypto.keySignatureUploadFailure", (failures, source, continuation) => {
      const KeySignatureUploadFailedDialog = sdk.getComponent('views.dialogs.KeySignatureUploadFailedDialog');

      _Modal.default.createTrackedDialog('Failed to upload key signatures', 'Failed to upload key signatures', KeySignatureUploadFailedDialog, {
        failures,
        source,
        continuation
      });
    });
    cli.on("crypto.verification.request", request => {
      const isFlagOn = _SettingsStore.default.isFeatureEnabled("feature_cross_signing");

      if (!isFlagOn && !request.channel.deviceId) {
        request.cancel({
          code: "m.invalid_message",
          reason: "This client has cross-signing disabled"
        });
        return;
      }

      if (request.verifier) {
        const IncomingSasDialog = sdk.getComponent("views.dialogs.IncomingSasDialog");

        _Modal.default.createTrackedDialog('Incoming Verification', '', IncomingSasDialog, {
          verifier: request.verifier
        }, null,
        /* priority = */
        false,
        /* static = */
        true);
      } else if (request.pending) {
        _ToastStore.default.sharedInstance().addOrReplaceToast({
          key: 'verifreq_' + request.channel.transactionId,
          title: request.isSelfVerification ? (0, _languageHandler._t)("Self-verification request") : (0, _languageHandler._t)("Verification Request"),
          icon: "verification",
          props: {
            request
          },
          component: sdk.getComponent("toasts.VerificationRequestToast"),
          priority: _ToastStore.default.PRIORITY_REALTIME
        });
      }
    }); // Fire the tinter right on startup to ensure the default theme is applied
    // A later sync can/will correct the tint to be the right value for the user

    const colorScheme = _SettingsStore.default.getValue("roomColor");

    _Tinter.default.tint(colorScheme.primary_color, colorScheme.secondary_color);
  },

  /**
   * Called shortly after the matrix client has started. Useful for
   * setting up anything that requires the client to be started.
   * @private
   */
  _onClientStarted: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli.isCryptoEnabled()) {
      const blacklistEnabled = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, "blacklistUnverifiedDevices");

      cli.setGlobalBlacklistUnverifiedDevices(blacklistEnabled); // With cross-signing enabled, we send to unknown devices
      // without prompting. Any bad-device status the user should
      // be aware of will be signalled through the room shield
      // changing colour. More advanced behaviour will come once
      // we implement more settings.

      cli.setGlobalErrorOnUnknownDevices(!_SettingsStore.default.isFeatureEnabled("feature_cross_signing"));
    }
  },
  showScreen: function (screen, params) {
    console.log("----params show screen ----", params);

    if (screen == 'register') {
      _dispatcher.default.dispatch({
        action: 'start_registration',
        params: params
      });
    } else if (screen == 'login') {
      _dispatcher.default.dispatch({
        action: 'start_login' // params: params,

      });
    } else if (screen == 'forgot_password') {
      _dispatcher.default.dispatch({
        action: 'start_password_recovery',
        params: params
      });
    } else if (screen === 'soft_logout') {
      if (_MatrixClientPeg.MatrixClientPeg.get() && _MatrixClientPeg.MatrixClientPeg.get().getUserId() && !Lifecycle.isSoftLogout()) {
        // Logged in - visit a room
        this._viewLastRoom();
      } else {
        // Ultimately triggers soft_logout if needed
        _dispatcher.default.dispatch({
          action: 'start_login',
          params: params
        });
      }
    } else if (screen == 'new') {
      _dispatcher.default.dispatch({
        action: 'view_create_room'
      });
    } else if (screen == 'settings') {
      _dispatcher.default.dispatch({
        action: 'view_user_settings'
      });
    } else if (screen == 'welcome') {
      _dispatcher.default.dispatch({
        action: 'view_welcome_page'
      });
    } else if (screen == 'home') {
      _dispatcher.default.dispatch({
        action: 'view_home_page'
      });
    } else if (screen == 'start') {
      this.showScreen('home');

      _dispatcher.default.dispatch({
        action: 'require_registration'
      });
    } else if (screen == 'directory') {
      _dispatcher.default.dispatch({
        action: 'view_room_directory'
      });
    } else if (screen == 'groups') {
      _dispatcher.default.dispatch({
        action: 'view_my_groups'
      });
    } else if (screen === 'complete_security') {
      _dispatcher.default.dispatch({
        action: 'start_complete_security'
      });
    } else if (screen == 'post_registration') {
      _dispatcher.default.dispatch({
        action: 'start_post_registration'
      });
    } else if (screen.indexOf('room/') == 0) {
      // Rooms can have the following formats:
      // #room_alias:domain or !opaque_id:domain
      const room = screen.substring(5);
      const domainOffset = room.indexOf(':') + 1; // 0 in case room does not contain a :

      let eventOffset = room.length; // room aliases can contain slashes only look for slash after domain

      if (room.substring(domainOffset).indexOf('/') > -1) {
        eventOffset = domainOffset + room.substring(domainOffset).indexOf('/');
      }

      const roomString = room.substring(0, eventOffset);
      let eventId = room.substring(eventOffset + 1); // empty string if no event id given
      // Previously we pulled the eventID from the segments in such a way
      // where if there was no eventId then we'd get undefined. However, we
      // now do a splice and join to handle v3 event IDs which results in
      // an empty string. To maintain our potential contract with the rest
      // of the app, we coerce the eventId to be undefined where applicable.

      if (!eventId) eventId = undefined; // TODO: Handle encoded room/event IDs: https://github.com/vector-im/riot-web/issues/9149
      // FIXME: sort_out caseConsistency

      const thirdPartyInvite = {
        inviteSignUrl: params.signurl,
        invitedEmail: params.email
      };
      const oobData = {
        name: params.room_name,
        avatarUrl: params.room_avatar_url,
        inviterName: params.inviter_name
      }; // on our URLs there might be a ?via=matrix.org or similar to help
      // joins to the room succeed. We'll pass these through as an array
      // to other levels. If there's just one ?via= then params.via is a
      // single string. If someone does something like ?via=one.com&via=two.com
      // then params.via is an array of strings.

      let via = [];

      if (params.via) {
        if (typeof params.via === 'string') via = [params.via];else via = params.via;
      }

      const payload = {
        action: 'view_room',
        event_id: eventId,
        via_servers: via,
        // If an event ID is given in the URL hash, notify RoomViewStore to mark
        // it as highlighted, which will propagate to RoomView and highlight the
        // associated EventTile.
        highlighted: Boolean(eventId),
        third_party_invite: thirdPartyInvite,
        oob_data: oobData
      };

      if (roomString[0] == '#') {
        payload.room_alias = roomString;
      } else {
        payload.room_id = roomString;
      }

      _dispatcher.default.dispatch(payload);
    } else if (screen.indexOf('user/') == 0) {
      const userId = screen.substring(5);

      _dispatcher.default.dispatch({
        action: 'view_user_info',
        userId: userId,
        subAction: params.action
      });
    } else if (screen.indexOf('group/') == 0) {
      const groupId = screen.substring(6); // TODO: Check valid group ID

      _dispatcher.default.dispatch({
        action: 'view_group',
        group_id: groupId
      });
    } else {
      console.info("Ignoring showScreen for '%s'", screen);
    }
  },
  notifyNewScreen: function (screen) {
    if (this.props.onNewScreen) {
      this.props.onNewScreen(screen);
    }

    this._setPageSubtitle();
  },
  onAliasClick: function (event, alias) {
    event.preventDefault();

    _dispatcher.default.dispatch({
      action: 'view_room',
      room_alias: alias
    });
  },
  onUserClick: function (event, userId) {
    event.preventDefault();
    const member = new Matrix.RoomMember(null, userId);

    if (!member) {
      return;
    }

    _dispatcher.default.dispatch({
      action: 'view_user',
      member: member
    });
  },
  onGroupClick: function (event, groupId) {
    event.preventDefault();

    _dispatcher.default.dispatch({
      action: 'view_group',
      group_id: groupId
    });
  },
  onLogoutClick: function (event) {
    _dispatcher.default.dispatch({
      action: 'logout'
    });

    event.stopPropagation();
    event.preventDefault();
  },
  handleResize: function (e) {
    const hideLhsThreshold = 1000;
    const showLhsThreshold = 1000;

    if (this._windowWidth > hideLhsThreshold && window.innerWidth <= hideLhsThreshold) {
      _dispatcher.default.dispatch({
        action: 'hide_left_panel'
      });
    }

    if (this._windowWidth <= showLhsThreshold && window.innerWidth > showLhsThreshold) {
      _dispatcher.default.dispatch({
        action: 'show_left_panel'
      });
    }

    this.state.resizeNotifier.notifyWindowResized();
    this._windowWidth = window.innerWidth;
  },

  _dispatchTimelineResize() {
    _dispatcher.default.dispatch({
      action: 'timeline_resize'
    });
  },

  onRoomCreated: function (roomId) {
    _dispatcher.default.dispatch({
      action: "view_room",
      room_id: roomId
    });
  },
  onRegisterClick: function () {
    this.showScreen("register");
  },
  onLoginClick: function () {
    this.showScreen("login");
  },
  onForgotPasswordClick: function () {
    this.showScreen("forgot_password");
  },
  onRegisterFlowComplete: function (credentials, password) {
    return this.onUserCompletedLoginFlow(credentials, password);
  },
  // returns a promise which resolves to the new MatrixClient
  onRegistered: function (credentials) {
    return Lifecycle.setLoggedIn(credentials);
  },
  onFinishPostRegistration: function () {
    // Don't confuse this with "PageType" which is the middle window to show
    this.setState({
      view: VIEWS.LOGGED_IN
    });
    this.showScreen("settings");
  },
  onVersion: function (current, latest, releaseNotes) {
    this.setState({
      version: current,
      newVersion: latest,
      hasNewVersion: current !== latest,
      newVersionReleaseNotes: releaseNotes,
      checkingForUpdate: null
    });
  },
  onSendEvent: function (roomId, event) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (!cli) {
      _dispatcher.default.dispatch({
        action: 'message_send_failed'
      });

      return;
    }

    cli.sendEvent(roomId, event.getType(), event.getContent()).then(() => {
      _dispatcher.default.dispatch({
        action: 'message_sent'
      });
    }, err => {
      _dispatcher.default.dispatch({
        action: 'message_send_failed'
      });
    });
  },
  _setPageSubtitle: function (subtitle = '') {
    if (this.state.currentRoomId) {
      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const room = client && client.getRoom(this.state.currentRoomId);

      if (room) {
        subtitle = "".concat(this.subTitleStatus, " | ").concat(room.name, " ").concat(subtitle);
      }
    } else {
      subtitle = "".concat(this.subTitleStatus, " ").concat(subtitle);
    }

    document.title = "".concat(_SdkConfig.default.get().brand || 'Riot', " ").concat(subtitle);
  },
  updateStatusIndicator: function (state, prevState) {
    const notifCount = (0, _RoomNotifs.countRoomsWithNotif)(_MatrixClientPeg.MatrixClientPeg.get().getRooms()).count;

    if (_PlatformPeg.default.get()) {
      _PlatformPeg.default.get().setErrorStatus(state === 'ERROR');

      _PlatformPeg.default.get().setNotificationCount(notifCount);
    }

    this.subTitleStatus = '';

    if (state === "ERROR") {
      this.subTitleStatus += "[".concat((0, _languageHandler._t)("Offline"), "] ");
    }

    if (notifCount > 0) {
      this.subTitleStatus += "[".concat(notifCount, "]");
    }

    this._setPageSubtitle();
  },

  onCloseAllSettings() {
    _dispatcher.default.dispatch({
      action: 'close_settings'
    });
  },

  onServerConfigChange(config) {
    this.setState({
      serverConfig: config
    });
  },

  _makeRegistrationUrl: function (params) {
    if (this.props.startingFragmentQueryParams.referrer) {
      params.referrer = this.props.startingFragmentQueryParams.referrer;
    }

    return this.props.makeRegistrationUrl(params);
  },
  _collectLoggedInView: function (ref) {
    this._loggedInView = ref;
  },

  async onUserCompletedLoginFlow(credentials, password) {
    this._accountPassword = password; // self-destruct the password after 5mins

    if (this._accountPasswordTimer !== null) clearTimeout(this._accountPasswordTimer);
    this._accountPasswordTimer = setTimeout(() => {
      this._accountPassword = null;
      this._accountPasswordTimer = null;
    }, 60 * 5 * 1000); // Wait for the client to be logged in (but not started)
    // which is enough to ask the server about account data.

    const loggedIn = new Promise(resolve => {
      const actionHandlerRef = _dispatcher.default.register(payload => {
        if (payload.action !== "on_logged_in") {
          return;
        }

        _dispatcher.default.unregister(actionHandlerRef);

        resolve();
      });
    }); // Create and start the client in the background

    const setLoggedInPromise = Lifecycle.setLoggedIn(credentials);
    await loggedIn;

    const cli = _MatrixClientPeg.MatrixClientPeg.get(); // We're checking `isCryptoAvailable` here instead of `isCryptoEnabled`
    // because the client hasn't been started yet.


    if (!(0, _crypto.isCryptoAvailable)()) {
      this._onLoggedIn();
    } // Test for the master cross-signing key in SSSS as a quick proxy for
    // whether cross-signing has been set up on the account.


    let masterKeyInStorage = false;

    try {
      masterKeyInStorage = !!(await cli.getAccountDataFromServer("m.cross_signing.master"));
    } catch (e) {
      if (e.errcode !== "M_NOT_FOUND") {
        console.warn("Secret storage account data check failed", e);
      }
    }

    if (masterKeyInStorage) {
      // Auto-enable cross-signing for the new session when key found in
      // secret storage.
      _SettingsStore.default.setFeatureEnabled("feature_cross_signing", true);

      this.setStateForNewView({
        view: VIEWS.COMPLETE_SECURITY
      });
    } else if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing") && (await cli.doesServerSupportUnstableFeature("org.matrix.e2e_cross_signing"))) {
      // This will only work if the feature is set to 'enable' in the config,
      // since it's too early in the lifecycle for users to have turned the
      // labs flag on.
      this.setStateForNewView({
        view: VIEWS.E2E_SETUP
      });
    } else {
      this._onLoggedIn();
    }

    return setLoggedInPromise;
  },

  // complete security / e2e setup has finished
  onCompleteSecurityE2eSetupFinished() {
    this._onLoggedIn();
  },

  render: function () {
    console.log("-----matrixChat----", this.props);

    if (this.props.startingFragmentQueryParams) {
      const {
        mx_device_id,
        access_token,
        mx_user_id,
        mx_session
      } = this.props.startingFragmentQueryParams;
      localStorage.setItem('mx_device_id', mx_device_id);
      localStorage.setItem('mx_hs_url', 'https://matrix.piyarse.com');
      localStorage.setItem('mx_access_token', access_token);
      localStorage.setItem('mx_user_id', mx_user_id);
      localStorage.setItem('mx_crypto_initialised', true);
      localStorage.setItem('mx_is_guest', false);
      localStorage.setItem('mx_session', mx_session);
      localStorage.setItem('mx_is_url', 'https://vector.im');
      localStorage.setItem('mxjssdk_memory_filter_FILTER_SYNC_@hamshiikhan:matrix.piyarse.com', 0);
      localStorage.setItem('mx_last_room_id', '!YPHtZRwcbirJtTZeiO:matrix.piyarse.com');
      localStorage.setItem('mx_accepts_unsupported_browser', true);
    } // console.log(`Rendering MatrixChat with view ${this.state.view}`);


    let view;

    if (this.state.view === VIEWS.LOADING) {
      const Spinner = sdk.getComponent('elements.Spinner');
      view = _react.default.createElement("div", {
        className: "mx_MatrixChat_splash"
      }, _react.default.createElement(Spinner, null));
    } else if (this.state.view === VIEWS.COMPLETE_SECURITY) {
      const CompleteSecurity = sdk.getComponent('structures.auth.CompleteSecurity');
      view = _react.default.createElement(CompleteSecurity, {
        onFinished: this.onCompleteSecurityE2eSetupFinished
      });
    } else if (this.state.view === VIEWS.E2E_SETUP) {
      const E2eSetup = sdk.getComponent('structures.auth.E2eSetup');
      view = _react.default.createElement(E2eSetup, {
        onFinished: this.onCompleteSecurityE2eSetupFinished,
        accountPassword: this._accountPassword
      });
    } else if (this.state.view === VIEWS.POST_REGISTRATION) {
      // needs to be before normal PageTypes as you are logged in technically
      const PostRegistration = sdk.getComponent('structures.auth.PostRegistration');
      view = _react.default.createElement(PostRegistration, {
        onComplete: this.onFinishPostRegistration
      });
    } else if (this.state.view === VIEWS.LOGGED_IN) {
      // store errors stop the client syncing and require user intervention, so we'll
      // be showing a dialog. Don't show anything else.
      const isStoreError = this.state.syncError && this.state.syncError instanceof Matrix.InvalidStoreError; // `ready` and `view==LOGGED_IN` may be set before `page_type` (because the
      // latter is set via the dispatcher). If we don't yet have a `page_type`,
      // keep showing the spinner for now.

      if (this.state.ready && this.state.page_type && !isStoreError) {
        /* for now, we stuff the entirety of our props and state into the LoggedInView.
         * we should go through and figure out what we actually need to pass down, as well
         * as using something like redux to avoid having a billion bits of state kicking around.
         */
        const LoggedInView = sdk.getComponent('structures.LoggedInView');
        view = _react.default.createElement(LoggedInView, (0, _extends2.default)({
          ref: this._collectLoggedInView,
          matrixClient: _MatrixClientPeg.MatrixClientPeg.get(),
          onRoomCreated: this.onRoomCreated,
          onCloseAllSettings: this.onCloseAllSettings,
          onRegistered: this.onRegistered,
          currentRoomId: this.state.currentRoomId,
          showCookieBar: this.state.showCookieBar
        }, this.props, this.state));
      } else {
        // we think we are logged in, but are still waiting for the /sync to complete
        const Spinner = sdk.getComponent('elements.Spinner');
        let errorBox;

        if (this.state.syncError && !isStoreError) {
          errorBox = _react.default.createElement("div", {
            className: "mx_MatrixChat_syncError"
          }, (0, _ErrorUtils.messageForSyncError)(this.state.syncError));
        }

        view = _react.default.createElement("div", {
          className: "mx_MatrixChat_splash"
        }, errorBox, _react.default.createElement(Spinner, null), _react.default.createElement("a", {
          href: "#",
          className: "mx_MatrixChat_splashButtons",
          onClick: this.onLogoutClick
        }, (0, _languageHandler._t)('Logout')));
      }
    } else if (this.state.view === VIEWS.WELCOME) {
      const Welcome = sdk.getComponent('auth.Welcome');
      view = _react.default.createElement(Welcome, this.getServerProperties());
    } else if (this.state.view === VIEWS.REGISTER) {
      const Registration = sdk.getComponent('structures.auth.Registration');
      view = _react.default.createElement(Registration, (0, _extends2.default)({
        clientSecret: this.state.register_client_secret,
        sessionId: this.state.register_session_id,
        idSid: this.state.register_id_sid,
        email: this.props.startingFragmentQueryParams.email,
        brand: this.props.config.brand,
        makeRegistrationUrl: this._makeRegistrationUrl,
        onLoggedIn: this.onRegisterFlowComplete,
        onLoginClick: this.onLoginClick,
        onServerConfigChange: this.onServerConfigChange,
        defaultDeviceDisplayName: this.props.defaultDeviceDisplayName
      }, this.getServerProperties()));
    } else if (this.state.view === VIEWS.FORGOT_PASSWORD) {
      const ForgotPassword = sdk.getComponent('structures.auth.ForgotPassword');
      view = _react.default.createElement(ForgotPassword, (0, _extends2.default)({
        onComplete: this.onLoginClick,
        onLoginClick: this.onLoginClick,
        onServerConfigChange: this.onServerConfigChange
      }, this.getServerProperties()));
    } else if (this.state.view === VIEWS.LOGIN) {
      const Login = sdk.getComponent('structures.auth.Login');
      view = _react.default.createElement(Login, (0, _extends2.default)({
        onLoggedIn: this.onUserCompletedLoginFlow,
        onRegisterClick: this.onRegisterClick,
        fallbackHsUrl: this.getFallbackHsUrl(),
        defaultDeviceDisplayName: this.props.defaultDeviceDisplayName,
        onForgotPasswordClick: this.onForgotPasswordClick,
        onServerConfigChange: this.onServerConfigChange,
        startingFragmentQueryParams: this.props.startingFragmentQueryParams
      }, this.getServerProperties()));
    } else if (this.state.view === VIEWS.SOFT_LOGOUT) {
      const SoftLogout = sdk.getComponent('structures.auth.SoftLogout');
      view = _react.default.createElement(SoftLogout, {
        realQueryParams: this.props.realQueryParams,
        onTokenLoginCompleted: this.props.onTokenLoginCompleted
      });
    } else {
      console.error("Unknown view ".concat(this.state.view));
    }

    const ErrorBoundary = sdk.getComponent('elements.ErrorBoundary');
    return _react.default.createElement(ErrorBoundary, null, view);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvTWF0cml4Q2hhdC5qcyJdLCJuYW1lcyI6WyJWSUVXUyIsIkxPQURJTkciLCJXRUxDT01FIiwiTE9HSU4iLCJSRUdJU1RFUiIsIlBPU1RfUkVHSVNUUkFUSU9OIiwiRk9SR09UX1BBU1NXT1JEIiwiQ09NUExFVEVfU0VDVVJJVFkiLCJFMkVfU0VUVVAiLCJMT0dHRURfSU4iLCJTT0ZUX0xPR09VVCIsIk9OQk9BUkRJTkdfRkxPV19TVEFSVEVSUyIsInN0YXRpY3MiLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsImNvbmZpZyIsIlByb3BUeXBlcyIsIm9iamVjdCIsInNlcnZlckNvbmZpZyIsImluc3RhbmNlT2YiLCJWYWxpZGF0ZWRTZXJ2ZXJDb25maWciLCJDb25mZXJlbmNlSGFuZGxlciIsImFueSIsIm9uTmV3U2NyZWVuIiwiZnVuYyIsInJlZ2lzdHJhdGlvblVybCIsInN0cmluZyIsImVuYWJsZUd1ZXN0IiwiYm9vbCIsInJlYWxRdWVyeVBhcmFtcyIsInN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcyIsInRlc3QiLCJvblRva2VuTG9naW5Db21wbGV0ZWQiLCJpbml0aWFsU2NyZWVuQWZ0ZXJMb2dpbiIsInNoYXBlIiwic2NyZWVuIiwiaXNSZXF1aXJlZCIsInBhcmFtcyIsImRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZSIsIm1ha2VSZWdpc3RyYXRpb25VcmwiLCJnZXRJbml0aWFsU3RhdGUiLCJzIiwidmlldyIsInBhZ2VfdHlwZSIsImN1cnJlbnRSb29tSWQiLCJ2aWV3VXNlcklkIiwiY29sbGFwc2VMaHMiLCJsZWZ0RGlzYWJsZWQiLCJtaWRkbGVEaXNhYmxlZCIsInZlcnNpb24iLCJuZXdWZXJzaW9uIiwiaGFzTmV3VmVyc2lvbiIsIm5ld1ZlcnNpb25SZWxlYXNlTm90ZXMiLCJjaGVja2luZ0ZvclVwZGF0ZSIsInNob3dDb29raWVCYXIiLCJyZWdpc3Rlcl9jbGllbnRfc2VjcmV0IiwicmVnaXN0ZXJfc2Vzc2lvbl9pZCIsInJlZ2lzdGVyX2lkX3NpZCIsImhpZGVUb1NSVXNlcnMiLCJzeW5jRXJyb3IiLCJyZXNpemVOb3RpZmllciIsIlJlc2l6ZU5vdGlmaWVyIiwic2hvd05vdGlmaWVyVG9vbGJhciIsImdldERlZmF1bHRQcm9wcyIsImdldEZhbGxiYWNrSHNVcmwiLCJwcm9wcyIsImlzRGVmYXVsdCIsImZhbGxiYWNrX2hzX3VybCIsImdldFNlcnZlclByb3BlcnRpZXMiLCJzdGF0ZSIsIlNka0NvbmZpZyIsImdldCIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJwdXQiLCJmaXJzdFN5bmNDb21wbGV0ZSIsImZpcnN0U3luY1Byb21pc2UiLCJzeW5jX3RpbWVsaW5lX2xpbWl0IiwiTWF0cml4Q2xpZW50UGVnIiwib3B0cyIsImluaXRpYWxTeW5jTGltaXQiLCJfc2NyZWVuQWZ0ZXJMb2dpbiIsIl93aW5kb3dXaWR0aCIsImhhbmRsZVJlc2l6ZSIsIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJfcGFnZUNoYW5naW5nIiwiVGludGVyIiwidGludCIsIm9uIiwiX2Rpc3BhdGNoVGltZWxpbmVSZXNpemUiLCJMaWZlY3ljbGUiLCJpc1NvZnRMb2dvdXQiLCJsb2FkU2Vzc2lvbiIsIl9hY2NvdW50UGFzc3dvcmQiLCJfYWNjb3VudFBhc3N3b3JkVGltZXIiLCJkaXNwYXRjaGVyUmVmIiwiZGlzIiwicmVnaXN0ZXIiLCJvbkFjdGlvbiIsIl90aGVtZVdhdGNoZXIiLCJUaGVtZVdhdGNoZXIiLCJzdGFydCIsImZvY3VzQ29tcG9zZXIiLCJzdWJUaXRsZVN0YXR1cyIsIm9uQWxpYXNDbGljayIsImxpbmtpZnlNYXRyaXgiLCJvblVzZXJDbGljayIsIm9uR3JvdXBDbGljayIsImF0dGVtcHRUb2tlbkxvZ2luIiwidGhlbiIsImxvZ2dlZEluIiwiZmlyc3RTY3JlZW4iLCJfc2hvd1NjcmVlbkFmdGVyTG9naW4iLCJfbG9hZFNlc3Npb24iLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJzZXRTdGF0ZSIsIkFuYWx5dGljcyIsImVuYWJsZSIsIlByb21pc2UiLCJyZXNvbHZlIiwiZnJhZ21lbnRRdWVyeVBhcmFtcyIsImd1ZXN0SHNVcmwiLCJoc1VybCIsImd1ZXN0SXNVcmwiLCJpc1VybCIsImxvYWRlZFNlc3Npb24iLCJkaXNwYXRjaCIsImFjdGlvbiIsImNvbXBvbmVudFdpbGxVbm1vdW50Iiwic3RvcE1hdHJpeENsaWVudCIsInVucmVnaXN0ZXIiLCJzdG9wIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsIm9uRm9jdXMiLCJyZW1vdmVMaXN0ZW5lciIsImNsZWFyVGltZW91dCIsIlVOU0FGRV9jb21wb25lbnRXaWxsVXBkYXRlIiwic2hvdWxkVHJhY2tQYWdlQ2hhbmdlIiwic3RhcnRQYWdlQ2hhbmdlVGltZXIiLCJjb21wb25lbnREaWRVcGRhdGUiLCJwcmV2UHJvcHMiLCJwcmV2U3RhdGUiLCJkdXJhdGlvbk1zIiwic3RvcFBhZ2VDaGFuZ2VUaW1lciIsInRyYWNrUGFnZUNoYW5nZSIsInBlcmZvcm1hbmNlIiwibWFyayIsImNvbnNvbGUiLCJ3YXJuIiwibWVhc3VyZSIsImNsZWFyTWFya3MiLCJtZWFzdXJlbWVudCIsImdldEVudHJpZXNCeU5hbWUiLCJwb3AiLCJkdXJhdGlvbiIsInNldFN0YXRlRm9yTmV3VmlldyIsInVuZGVmaW5lZCIsIkVycm9yIiwibmV3U3RhdGUiLCJPYmplY3QiLCJhc3NpZ24iLCJwYXlsb2FkIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJRdWVzdGlvbkRpYWxvZyIsImlzR3Vlc3QiLCJpbmNsdWRlcyIsImRlZmVycmVkX2FjdGlvbiIsImV2ZW50X3R5cGUiLCJmdWxsVXJsIiwiZXZlbnRfY29udGVudCIsInNldElkZW50aXR5U2VydmVyVXJsIiwibG9jYWxTdG9yYWdlIiwicmVtb3ZlSXRlbSIsInNldEl0ZW0iLCJsb2dvdXQiLCJfb25Tb2Z0TG9nb3V0Iiwic2NyZWVuQWZ0ZXJMb2dpbiIsIl9zdGFydFJlZ2lzdHJhdGlvbiIsIm5vdGlmeU5ld1NjcmVlbiIsIlRoZW1lQ29udHJvbGxlciIsImlzTG9naW4iLCJyZWNoZWNrIiwiZG1Vc2VySWQiLCJ1c2VyX2lkIiwiX2xlYXZlUm9vbSIsInJvb21faWQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwib25GaW5pc2hlZCIsImNvbmZpcm0iLCJMb2FkZXIiLCJtb2RhbCIsImNyZWF0ZURpYWxvZyIsImxlYXZlIiwiY2xvc2UiLCJlcnIiLCJ0b1N0cmluZyIsIl92aWV3VXNlciIsInVzZXJJZCIsInN1YkFjdGlvbiIsInByb21pc2UiLCJfdmlld1Jvb20iLCJfdmlld05leHRSb29tIiwiX3ZpZXdJbmRleGVkUm9vbSIsInJvb21JbmRleCIsIlVzZXJTZXR0aW5nc0RpYWxvZyIsIl92aWV3U29tZXRoaW5nQmVoaW5kTW9kYWwiLCJfY3JlYXRlUm9vbSIsIkNyZWF0ZUdyb3VwRGlhbG9nIiwiUm9vbURpcmVjdG9yeSIsIl9zZXRQYWdlIiwiUGFnZVR5cGVzIiwiTXlHcm91cHMiLCJfdmlld0dyb3VwIiwiX3ZpZXdXZWxjb21lIiwiX3ZpZXdIb21lIiwiX3NldE14SWQiLCJfY2hhdENyZWF0ZU9yUmV1c2UiLCJyb29tSWQiLCJOb3RpZmllciIsInNob3VsZFNob3dUb29sYmFyIiwic2lkZURpc2FibGVkIiwiX29uTG9nZ2VkSW4iLCJfb25Mb2dnZWRPdXQiLCJyZWFkeSIsIl9vbldpbGxTdGFydENsaWVudCIsIl9vbkNsaWVudFN0YXJ0ZWQiLCJvblZlcnNpb24iLCJjdXJyZW50VmVyc2lvbiIsInJlbGVhc2VOb3RlcyIsInZhbHVlIiwib25TZW5kRXZlbnQiLCJldmVudCIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIiwicGFnZVR5cGUiLCJjbGllbnRfc2VjcmV0Iiwic2Vzc2lvbl9pZCIsImhzX3VybCIsImlzX3VybCIsInNpZCIsIkF1dG9EaXNjb3ZlcnlVdGlscyIsInZhbGlkYXRlU2VydmVyQ29uZmlnV2l0aFN0YXRpY1VybHMiLCJyb29tSW5kZXhEZWx0YSIsImFsbFJvb21zIiwiUm9vbUxpc3RTb3J0ZXIiLCJtb3N0UmVjZW50QWN0aXZpdHlGaXJzdCIsImdldFJvb21zIiwibGVuZ3RoIiwiaSIsInJvb21JbmZvIiwiUm9vbVZpZXciLCJ0aGlyZFBhcnR5SW52aXRlIiwidGhpcmRfcGFydHlfaW52aXRlIiwicm9vbU9vYkRhdGEiLCJvb2JfZGF0YSIsInZpYVNlcnZlcnMiLCJ2aWFfc2VydmVycyIsInJvb21fYWxpYXMiLCJsb2ciLCJldmVudF9pZCIsIndhaXRGb3IiLCJwcmVzZW50ZWRJZCIsInJvb20iLCJnZXRSb29tIiwidGhlQWxpYXMiLCJSb29tcyIsImdldERpc3BsYXlBbGlhc0ZvclJvb20iLCJoaWdobGlnaHRlZCIsImdyb3VwSWQiLCJncm91cF9pZCIsImN1cnJlbnRHcm91cElkIiwiY3VycmVudEdyb3VwSXNOZXciLCJncm91cF9pc19uZXciLCJHcm91cFZpZXciLCJIb21lUGFnZSIsIndhaXRGb3JTeW5jIiwiY3VycmVudFVzZXJJZCIsIlVzZXJWaWV3IiwiU2V0TXhJZERpYWxvZyIsImhvbWVzZXJ2ZXJVcmwiLCJnZXRIb21lc2VydmVyVXJsIiwic3VibWl0dGVkIiwiY3JlZGVudGlhbHMiLCJnb19ob21lX29uX2NhbmNlbCIsInNldEp1c3RSZWdpc3RlcmVkVXNlcklkIiwib25SZWdpc3RlcmVkIiwib25EaWZmZXJlbnRTZXJ2ZXJDbGlja2VkIiwiZXYiLCJvbkxvZ2luQ2xpY2siLCJDcmVhdGVSb29tRGlhbG9nIiwic2hvdWxkQ3JlYXRlIiwiZmluaXNoZWQiLCJ3ZWxjb21lVXNlcklkIiwiZ29fd2VsY29tZV9vbl9jYW5jZWwiLCJzY3JlZW5fYWZ0ZXIiLCJjbGllbnQiLCJkbVJvb21NYXAiLCJETVJvb21NYXAiLCJkbVJvb21zIiwiZ2V0RE1Sb29tc0ZvclVzZXJJZCIsIl9sZWF2ZVJvb21XYXJuaW5ncyIsInJvb21Ub0xlYXZlIiwiam9pblJ1bGVzIiwiY3VycmVudFN0YXRlIiwiZ2V0U3RhdGVFdmVudHMiLCJ3YXJuaW5ncyIsInJ1bGUiLCJnZXRDb250ZW50Iiwiam9pbl9ydWxlIiwicHVzaCIsInJvb21OYW1lIiwibmFtZSIsImJ1dHRvbiIsInNob3VsZExlYXZlIiwiZCIsImxlYXZlUm9vbUNoYWluIiwiZXJyb3JzIiwibGVmdFJvb21JZCIsImtleXMiLCJlcnJvciIsIm1lc3NhZ2UiLCJlcnJjb2RlIiwiX3N0YXJ0V2VsY29tZVVzZXJDaGF0Iiwid2VsY29tZVVzZXJSb29tcyIsInNoYXJlZCIsImFuZFZpZXciLCJzcGlubmVyIiwic2F2ZVdlbGNvbWVVc2VyIiwiZ2V0VHlwZSIsInN0b3JlIiwic2F2ZSIsInNob3dTY3JlZW4iLCJjdXJyZW50VXNlcklzSnVzdFJlZ2lzdGVyZWQiLCJzdGFydHNXaXRoIiwid2VsY29tZVVzZXJSb29tIiwiU3RvcmFnZU1hbmFnZXIiLCJ0cnlQZXJzaXN0U3RvcmFnZSIsImdldEl0ZW0iLCJfdmlld0xhc3RSb29tIiwiX3NldFBhZ2VTdWJ0aXRsZSIsInNlbGYiLCJjbGkiLCJzZXRDYW5SZXNldFRpbWVsaW5lQ2FsbGJhY2siLCJfbG9nZ2VkSW5WaWV3IiwiY2hpbGQiLCJjYW5SZXNldFRpbWVsaW5lSW5Sb29tIiwiZGF0YSIsIk1hdHJpeCIsIkludmFsaWRTdG9yZUVycm9yIiwiaGFuZGxlSW52YWxpZFN0b3JlRXJyb3IiLCJ1cGRhdGVTdGF0dXNJbmRpY2F0b3IiLCJpbmZvIiwiY2FsbCIsImVyck9iaiIsImlzTG9nZ2luZ091dCIsImh0dHBTdGF0dXMiLCJzb2Z0TG9nb3V0IiwiY29uc2VudFVyaSIsImhvbWVzZXJ2ZXJEb21haW4iLCJnZXREb21haW4iLCJjYW5jZWxCdXR0b24iLCJjb25maXJtZWQiLCJ3bmQiLCJvcGVuIiwib3BlbmVyIiwiZGZ0IiwiRGVjcnlwdGlvbkZhaWx1cmVUcmFja2VyIiwidG90YWwiLCJlcnJvckNvZGUiLCJ0cmFja0V2ZW50IiwiZSIsImV2ZW50RGVjcnlwdGVkIiwia3JoIiwiS2V5UmVxdWVzdEhhbmRsZXIiLCJyZXEiLCJoYW5kbGVLZXlSZXF1ZXN0IiwiaGFuZGxlS2V5UmVxdWVzdENhbmNlbGxhdGlvbiIsImlzQ3J5cHRvRW5hYmxlZCIsImJsYWNrbGlzdEVuYWJsZWQiLCJnZXRWYWx1ZUF0IiwiUk9PTV9ERVZJQ0UiLCJzZXRCbGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlcyIsInR5cGUiLCJoYXZlTmV3VmVyc2lvbiIsIm5ld1ZlcnNpb25JbmZvIiwiZ2V0S2V5QmFja3VwRW5hYmxlZCIsImdldEtleUJhY2t1cFZlcnNpb24iLCJjcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMiLCJmYWlsdXJlcyIsInNvdXJjZSIsImNvbnRpbnVhdGlvbiIsIktleVNpZ25hdHVyZVVwbG9hZEZhaWxlZERpYWxvZyIsInJlcXVlc3QiLCJpc0ZsYWdPbiIsImlzRmVhdHVyZUVuYWJsZWQiLCJjaGFubmVsIiwiZGV2aWNlSWQiLCJjYW5jZWwiLCJjb2RlIiwicmVhc29uIiwidmVyaWZpZXIiLCJJbmNvbWluZ1Nhc0RpYWxvZyIsInBlbmRpbmciLCJUb2FzdFN0b3JlIiwic2hhcmVkSW5zdGFuY2UiLCJhZGRPclJlcGxhY2VUb2FzdCIsImtleSIsInRyYW5zYWN0aW9uSWQiLCJpc1NlbGZWZXJpZmljYXRpb24iLCJpY29uIiwiY29tcG9uZW50IiwicHJpb3JpdHkiLCJQUklPUklUWV9SRUFMVElNRSIsImNvbG9yU2NoZW1lIiwicHJpbWFyeV9jb2xvciIsInNlY29uZGFyeV9jb2xvciIsInNldEdsb2JhbEJsYWNrbGlzdFVudmVyaWZpZWREZXZpY2VzIiwic2V0R2xvYmFsRXJyb3JPblVua25vd25EZXZpY2VzIiwiZ2V0VXNlcklkIiwiaW5kZXhPZiIsInN1YnN0cmluZyIsImRvbWFpbk9mZnNldCIsImV2ZW50T2Zmc2V0Iiwicm9vbVN0cmluZyIsImV2ZW50SWQiLCJpbnZpdGVTaWduVXJsIiwic2lnbnVybCIsImludml0ZWRFbWFpbCIsImVtYWlsIiwib29iRGF0YSIsInJvb21fbmFtZSIsImF2YXRhclVybCIsInJvb21fYXZhdGFyX3VybCIsImludml0ZXJOYW1lIiwiaW52aXRlcl9uYW1lIiwidmlhIiwiQm9vbGVhbiIsImFsaWFzIiwicHJldmVudERlZmF1bHQiLCJtZW1iZXIiLCJSb29tTWVtYmVyIiwib25Mb2dvdXRDbGljayIsInN0b3BQcm9wYWdhdGlvbiIsImhpZGVMaHNUaHJlc2hvbGQiLCJzaG93TGhzVGhyZXNob2xkIiwiaW5uZXJXaWR0aCIsIm5vdGlmeVdpbmRvd1Jlc2l6ZWQiLCJvblJvb21DcmVhdGVkIiwib25SZWdpc3RlckNsaWNrIiwib25Gb3Jnb3RQYXNzd29yZENsaWNrIiwib25SZWdpc3RlckZsb3dDb21wbGV0ZSIsInBhc3N3b3JkIiwib25Vc2VyQ29tcGxldGVkTG9naW5GbG93Iiwic2V0TG9nZ2VkSW4iLCJvbkZpbmlzaFBvc3RSZWdpc3RyYXRpb24iLCJjdXJyZW50IiwibGF0ZXN0Iiwic2VuZEV2ZW50Iiwic3VidGl0bGUiLCJkb2N1bWVudCIsImJyYW5kIiwibm90aWZDb3VudCIsImNvdW50IiwiUGxhdGZvcm1QZWciLCJzZXRFcnJvclN0YXR1cyIsInNldE5vdGlmaWNhdGlvbkNvdW50Iiwib25DbG9zZUFsbFNldHRpbmdzIiwib25TZXJ2ZXJDb25maWdDaGFuZ2UiLCJfbWFrZVJlZ2lzdHJhdGlvblVybCIsInJlZmVycmVyIiwiX2NvbGxlY3RMb2dnZWRJblZpZXciLCJyZWYiLCJzZXRUaW1lb3V0IiwiYWN0aW9uSGFuZGxlclJlZiIsInNldExvZ2dlZEluUHJvbWlzZSIsIm1hc3RlcktleUluU3RvcmFnZSIsImdldEFjY291bnREYXRhRnJvbVNlcnZlciIsInNldEZlYXR1cmVFbmFibGVkIiwiZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUiLCJvbkNvbXBsZXRlU2VjdXJpdHlFMmVTZXR1cEZpbmlzaGVkIiwicmVuZGVyIiwibXhfZGV2aWNlX2lkIiwiYWNjZXNzX3Rva2VuIiwibXhfdXNlcl9pZCIsIm14X3Nlc3Npb24iLCJTcGlubmVyIiwiQ29tcGxldGVTZWN1cml0eSIsIkUyZVNldHVwIiwiUG9zdFJlZ2lzdHJhdGlvbiIsImlzU3RvcmVFcnJvciIsIkxvZ2dlZEluVmlldyIsImVycm9yQm94IiwiV2VsY29tZSIsIlJlZ2lzdHJhdGlvbiIsIkZvcmdvdFBhc3N3b3JkIiwiTG9naW4iLCJTb2Z0TG9nb3V0IiwiRXJyb3JCb3VuZGFyeSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUdBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQW5FQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBO0FBRUE7QUFtQkE7O0FBdUJBO0FBQ08sTUFBTUEsS0FBSyxHQUFHO0FBQ2pCO0FBQ0E7QUFDQUMsRUFBQUEsT0FBTyxFQUFFLENBSFE7QUFLakI7QUFDQUMsRUFBQUEsT0FBTyxFQUFFLENBTlE7QUFRakI7QUFDQUMsRUFBQUEsS0FBSyxFQUFFLENBVFU7QUFXakI7QUFDQUMsRUFBQUEsUUFBUSxFQUFFLENBWk87QUFjakI7QUFDQUMsRUFBQUEsaUJBQWlCLEVBQUUsQ0FmRjtBQWlCakI7QUFDQUMsRUFBQUEsZUFBZSxFQUFFLENBbEJBO0FBb0JqQjtBQUNBQyxFQUFBQSxpQkFBaUIsRUFBRSxDQXJCRjtBQXVCakI7QUFDQUMsRUFBQUEsU0FBUyxFQUFFLENBeEJNO0FBMEJqQjtBQUNBQyxFQUFBQSxTQUFTLEVBQUUsQ0EzQk07QUE2QmpCO0FBQ0E7QUFDQUMsRUFBQUEsV0FBVyxFQUFFO0FBL0JJLENBQWQsQyxDQWtDUDtBQUNBO0FBQ0E7OztBQUNBLE1BQU1DLHdCQUF3QixHQUFHLENBQzdCLG9CQUQ2QixFQUU3QixrQkFGNkIsRUFHN0Isa0JBSDZCLEVBSTdCLG1CQUo2QixDQUFqQzs7ZUFPZSwrQkFBaUI7QUFDNUI7QUFDQUMsRUFBQUEsT0FBTyxFQUFFO0FBQ0xaLElBQUFBLEtBQUssRUFBRUE7QUFERixHQUZtQjtBQU01QmEsRUFBQUEsV0FBVyxFQUFFLFlBTmU7QUFRNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxNQUFNLEVBQUVDLG1CQUFVQyxNQURYO0FBRVBDLElBQUFBLFlBQVksRUFBRUYsbUJBQVVHLFVBQVYsQ0FBcUJDLHlDQUFyQixDQUZQO0FBR1BDLElBQUFBLGlCQUFpQixFQUFFTCxtQkFBVU0sR0FIdEI7QUFJUEMsSUFBQUEsV0FBVyxFQUFFUCxtQkFBVVEsSUFKaEI7QUFLUEMsSUFBQUEsZUFBZSxFQUFFVCxtQkFBVVUsTUFMcEI7QUFNUEMsSUFBQUEsV0FBVyxFQUFFWCxtQkFBVVksSUFOaEI7QUFRUDtBQUNBQyxJQUFBQSxlQUFlLEVBQUViLG1CQUFVQyxNQVRwQjtBQVdQO0FBQ0FhLElBQUFBLDJCQUEyQixFQUFFZCxtQkFBVUMsTUFaaEM7QUFhUGMsSUFBQUEsSUFBSSxFQUFFLE1BYkM7QUFlUDtBQUNBQyxJQUFBQSxxQkFBcUIsRUFBRWhCLG1CQUFVUSxJQWhCMUI7QUFrQlA7QUFDQTtBQUNBUyxJQUFBQSx1QkFBdUIsRUFBRWpCLG1CQUFVa0IsS0FBVixDQUFnQjtBQUNyQ0MsTUFBQUEsTUFBTSxFQUFFbkIsbUJBQVVVLE1BQVYsQ0FBaUJVLFVBRFk7QUFFckNDLE1BQUFBLE1BQU0sRUFBRXJCLG1CQUFVQztBQUZtQixLQUFoQixDQXBCbEI7QUF5QlA7QUFDQTtBQUNBcUIsSUFBQUEsd0JBQXdCLEVBQUV0QixtQkFBVVUsTUEzQjdCO0FBNkJQO0FBQ0FhLElBQUFBLG1CQUFtQixFQUFFdkIsbUJBQVVRLElBQVYsQ0FBZVk7QUE5QjdCLEdBUmlCO0FBeUM1QkksRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsVUFBTUMsQ0FBQyxHQUFHO0FBQ047QUFDQUMsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDQyxPQUZOO0FBSU47QUFDQTBDLE1BQUFBLFNBQVMsRUFBRSxJQUxMO0FBT047QUFDQTtBQUNBO0FBQ0FDLE1BQUFBLGFBQWEsRUFBRSxJQVZUO0FBWU47QUFDQUMsTUFBQUEsVUFBVSxFQUFFLElBYk47QUFjTjtBQUNBQyxNQUFBQSxXQUFXLEVBQUUsS0FmUDtBQWdCTkMsTUFBQUEsWUFBWSxFQUFFLEtBaEJSO0FBaUJOQyxNQUFBQSxjQUFjLEVBQUUsS0FqQlY7QUFrQk47QUFFQUMsTUFBQUEsT0FBTyxFQUFFLElBcEJIO0FBcUJOQyxNQUFBQSxVQUFVLEVBQUUsSUFyQk47QUFzQk5DLE1BQUFBLGFBQWEsRUFBRSxLQXRCVDtBQXVCTkMsTUFBQUEsc0JBQXNCLEVBQUUsSUF2QmxCO0FBd0JOQyxNQUFBQSxpQkFBaUIsRUFBRSxJQXhCYjtBQTBCTkMsTUFBQUEsYUFBYSxFQUFFLEtBMUJUO0FBNEJOO0FBQ0FDLE1BQUFBLHNCQUFzQixFQUFFLElBN0JsQjtBQThCTkMsTUFBQUEsbUJBQW1CLEVBQUUsSUE5QmY7QUErQk5DLE1BQUFBLGVBQWUsRUFBRSxJQS9CWDtBQWlDTjtBQUNBO0FBQ0FDLE1BQUFBLGFBQWEsRUFBRSxLQW5DVDtBQXFDTkMsTUFBQUEsU0FBUyxFQUFFLElBckNMO0FBcUNXO0FBQ2pCQyxNQUFBQSxjQUFjLEVBQUUsSUFBSUMsdUJBQUosRUF0Q1Y7QUF1Q05DLE1BQUFBLG1CQUFtQixFQUFFO0FBdkNmLEtBQVY7QUF5Q0EsV0FBT3JCLENBQVA7QUFDSCxHQXBGMkI7QUFzRjVCc0IsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIbEMsTUFBQUEsZUFBZSxFQUFFLEVBRGQ7QUFFSEMsTUFBQUEsMkJBQTJCLEVBQUUsRUFGMUI7QUFHSGYsTUFBQUEsTUFBTSxFQUFFLEVBSEw7QUFJSGlCLE1BQUFBLHFCQUFxQixFQUFFLE1BQU0sQ0FBRTtBQUo1QixLQUFQO0FBTUgsR0E3RjJCO0FBK0Y1QmdDLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsUUFBSSxLQUFLQyxLQUFMLENBQVcvQyxZQUFYLElBQTJCLEtBQUsrQyxLQUFMLENBQVcvQyxZQUFYLENBQXdCZ0QsU0FBdkQsRUFBa0U7QUFDOUQsYUFBTyxLQUFLRCxLQUFMLENBQVdsRCxNQUFYLENBQWtCb0QsZUFBekI7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLElBQVA7QUFDSDtBQUNKLEdBckcyQjs7QUF1RzVCQyxFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixRQUFJSCxLQUFLLEdBQUcsS0FBS0ksS0FBTCxDQUFXbkQsWUFBdkI7QUFDQSxRQUFJLENBQUMrQyxLQUFMLEVBQVlBLEtBQUssR0FBRyxLQUFLQSxLQUFMLENBQVcvQyxZQUFuQixDQUZNLENBRTJCOztBQUM3QyxRQUFJLENBQUMrQyxLQUFMLEVBQVlBLEtBQUssR0FBR0ssbUJBQVVDLEdBQVYsR0FBZ0IseUJBQWhCLENBQVI7QUFDWixXQUFPO0FBQUNyRCxNQUFBQSxZQUFZLEVBQUUrQztBQUFmLEtBQVA7QUFDSCxHQTVHMkI7O0FBOEc1QjtBQUNBTyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDRix1QkFBVUcsR0FBVixDQUFjLEtBQUtSLEtBQUwsQ0FBV2xELE1BQXpCLEVBRGtDLENBR2xDOzs7QUFDQSxTQUFLMkQsaUJBQUwsR0FBeUIsS0FBekI7QUFDQSxTQUFLQyxnQkFBTCxHQUF3QixxQkFBeEI7O0FBRUEsUUFBSSxLQUFLVixLQUFMLENBQVdsRCxNQUFYLENBQWtCNkQsbUJBQXRCLEVBQTJDO0FBQ3ZDQyx1Q0FBZ0JDLElBQWhCLENBQXFCQyxnQkFBckIsR0FBd0MsS0FBS2QsS0FBTCxDQUFXbEQsTUFBWCxDQUFrQjZELG1CQUExRDtBQUNILEtBVGlDLENBV2xDO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBS0ksaUJBQUwsR0FBeUIsS0FBS2YsS0FBTCxDQUFXaEMsdUJBQXBDO0FBRUEsU0FBS2dELFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxTQUFLQyxZQUFMO0FBQ0FDLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsS0FBS0YsWUFBdkM7QUFFQSxTQUFLRyxhQUFMLEdBQXFCLEtBQXJCLENBcEJrQyxDQXNCbEM7QUFDQTtBQUNBOztBQUNBQyxvQkFBT0MsSUFBUCxHQXpCa0MsQ0EyQmxDOzs7QUFDQSxTQUFLbEIsS0FBTCxDQUFXVCxjQUFYLENBQTBCNEIsRUFBMUIsQ0FBNkIsb0JBQTdCLEVBQW1ELEtBQUtDLHVCQUF4RCxFQTVCa0MsQ0E4QmxDOztBQUNBLFFBQUlDLFNBQVMsQ0FBQ0MsWUFBVixFQUFKLEVBQThCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBRCxNQUFBQSxTQUFTLENBQUNFLFdBQVYsQ0FBc0IsRUFBdEI7QUFDSDs7QUFFRCxTQUFLQyxnQkFBTCxHQUF3QixJQUF4QjtBQUNBLFNBQUtDLHFCQUFMLEdBQTZCLElBQTdCO0FBRUEsU0FBS0MsYUFBTCxHQUFxQkMsb0JBQUlDLFFBQUosQ0FBYSxLQUFLQyxRQUFsQixDQUFyQjtBQUNBLFNBQUtDLGFBQUwsR0FBcUIsSUFBSUMsbUJBQUosRUFBckI7O0FBQ0EsU0FBS0QsYUFBTCxDQUFtQkUsS0FBbkI7O0FBRUEsU0FBS0MsYUFBTCxHQUFxQixLQUFyQixDQTdDa0MsQ0ErQ2xDO0FBQ0E7O0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixFQUF0QixDQWpEa0MsQ0FtRGxDO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLQyxZQUFULEVBQXVCO0FBQ25CQyw2QkFBY0QsWUFBZCxHQUE2QixLQUFLQSxZQUFsQztBQUNIOztBQUNELFFBQUksS0FBS0UsV0FBVCxFQUFzQjtBQUNsQkQsNkJBQWNDLFdBQWQsR0FBNEIsS0FBS0EsV0FBakM7QUFDSDs7QUFDRCxRQUFJLEtBQUtDLFlBQVQsRUFBdUI7QUFDbkJGLDZCQUFjRSxZQUFkLEdBQTZCLEtBQUtBLFlBQWxDO0FBQ0gsS0E3RGlDLENBK0RsQztBQUNBOzs7QUFDQSxRQUFJLENBQUNqQixTQUFTLENBQUNDLFlBQVYsRUFBTCxFQUErQjtBQUMzQkQsTUFBQUEsU0FBUyxDQUFDa0IsaUJBQVYsQ0FDSSxLQUFLM0MsS0FBTCxDQUFXcEMsZUFEZixFQUVJLEtBQUtvQyxLQUFMLENBQVczQix3QkFGZixFQUdFdUUsSUFIRixDQUdRQyxRQUFELElBQWM7QUFDakIsWUFBSUEsUUFBSixFQUFjO0FBQ1YsZUFBSzdDLEtBQUwsQ0FBV2pDLHFCQUFYLEdBRFUsQ0FHVjtBQUNBOztBQUNBO0FBQ0gsU0FQZ0IsQ0FTakI7QUFDQTs7O0FBQ0EsY0FBTStFLFdBQVcsR0FBRyxLQUFLL0IsaUJBQUwsR0FDaEIsS0FBS0EsaUJBQUwsQ0FBdUI3QyxNQURQLEdBQ2dCLElBRHBDOztBQUdBLFlBQUk0RSxXQUFXLEtBQUssT0FBaEIsSUFDQUEsV0FBVyxLQUFLLFVBRGhCLElBRUFBLFdBQVcsS0FBSyxpQkFGcEIsRUFFdUM7QUFDbkMsZUFBS0MscUJBQUw7O0FBQ0E7QUFDSDs7QUFFRCxlQUFPLEtBQUtDLFlBQUwsRUFBUDtBQUNILE9BekJEO0FBMEJIOztBQUVELFFBQUlDLHVCQUFjQyxRQUFkLENBQXVCLGVBQXZCLENBQUosRUFBNkM7QUFDekMsV0FBS0MsUUFBTCxDQUFjO0FBQ1Y5RCxRQUFBQSxhQUFhLEVBQUU7QUFETCxPQUFkO0FBR0g7O0FBRUQsUUFBSTRELHVCQUFjQyxRQUFkLENBQXVCLGdCQUF2QixDQUFKLEVBQThDO0FBQzFDRSx5QkFBVUMsTUFBVjtBQUNIO0FBQ0osR0F0TjJCO0FBd041QkwsRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckI7QUFDQTtBQUNBLFdBQU9NLE9BQU8sQ0FBQ0MsT0FBUixHQUFrQlgsSUFBbEIsQ0FBdUIsTUFBTTtBQUNoQyxhQUFPbkIsU0FBUyxDQUFDRSxXQUFWLENBQXNCO0FBQ3pCNkIsUUFBQUEsbUJBQW1CLEVBQUUsS0FBS3hELEtBQUwsQ0FBV25DLDJCQURQO0FBRXpCSCxRQUFBQSxXQUFXLEVBQUUsS0FBS3NDLEtBQUwsQ0FBV3RDLFdBRkM7QUFHekIrRixRQUFBQSxVQUFVLEVBQUUsS0FBS3RELG1CQUFMLEdBQTJCbEQsWUFBM0IsQ0FBd0N5RyxLQUgzQjtBQUl6QkMsUUFBQUEsVUFBVSxFQUFFLEtBQUt4RCxtQkFBTCxHQUEyQmxELFlBQTNCLENBQXdDMkcsS0FKM0I7QUFLekJ2RixRQUFBQSx3QkFBd0IsRUFBRSxLQUFLMkIsS0FBTCxDQUFXM0I7QUFMWixPQUF0QixDQUFQO0FBT0gsS0FSTSxFQVFKdUUsSUFSSSxDQVFFaUIsYUFBRCxJQUFtQjtBQUN2QixVQUFJLENBQUNBLGFBQUwsRUFBb0I7QUFDaEI7QUFDQTlCLDRCQUFJK0IsUUFBSixDQUFhO0FBQUNDLFVBQUFBLE1BQU0sRUFBRTtBQUFULFNBQWI7QUFDSDtBQUNKLEtBYk0sQ0FBUCxDQUhxQixDQWlCckI7QUFDQTtBQUNBO0FBQ0gsR0E1TzJCO0FBOE81QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QnZDLElBQUFBLFNBQVMsQ0FBQ3dDLGdCQUFWOztBQUNBbEMsd0JBQUltQyxVQUFKLENBQWUsS0FBS3BDLGFBQXBCOztBQUNBLFNBQUtJLGFBQUwsQ0FBbUJpQyxJQUFuQjs7QUFDQWpELElBQUFBLE1BQU0sQ0FBQ2tELG1CQUFQLENBQTJCLE9BQTNCLEVBQW9DLEtBQUtDLE9BQXpDO0FBQ0FuRCxJQUFBQSxNQUFNLENBQUNrRCxtQkFBUCxDQUEyQixRQUEzQixFQUFxQyxLQUFLbkQsWUFBMUM7QUFDQSxTQUFLYixLQUFMLENBQVdULGNBQVgsQ0FBMEIyRSxjQUExQixDQUF5QyxvQkFBekMsRUFBK0QsS0FBSzlDLHVCQUFwRTtBQUVBLFFBQUksS0FBS0sscUJBQUwsS0FBK0IsSUFBbkMsRUFBeUMwQyxZQUFZLENBQUMsS0FBSzFDLHFCQUFOLENBQVo7QUFDNUMsR0F2UDJCO0FBeVA1QjtBQUNBMkMsRUFBQUEsMEJBQTBCLEVBQUUsVUFBU3hFLEtBQVQsRUFBZ0JJLEtBQWhCLEVBQXVCO0FBQy9DLFFBQUksS0FBS3FFLHFCQUFMLENBQTJCLEtBQUtyRSxLQUFoQyxFQUF1Q0EsS0FBdkMsQ0FBSixFQUFtRDtBQUMvQyxXQUFLc0Usb0JBQUw7QUFDSDtBQUNKLEdBOVAyQjtBQWdRNUJDLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNDLFNBQVQsRUFBb0JDLFNBQXBCLEVBQStCO0FBQy9DLFFBQUksS0FBS0oscUJBQUwsQ0FBMkJJLFNBQTNCLEVBQXNDLEtBQUt6RSxLQUEzQyxDQUFKLEVBQXVEO0FBQ25ELFlBQU0wRSxVQUFVLEdBQUcsS0FBS0MsbUJBQUwsRUFBbkI7O0FBQ0EzQix5QkFBVTRCLGVBQVYsQ0FBMEJGLFVBQTFCO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLekMsYUFBVCxFQUF3QjtBQUNwQk4sMEJBQUkrQixRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjs7QUFDQSxXQUFLMUIsYUFBTCxHQUFxQixLQUFyQjtBQUNIO0FBQ0osR0F6UTJCOztBQTJRNUJxQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQjtBQUNBLFFBQUksQ0FBQ08sV0FBRCxJQUFnQixDQUFDQSxXQUFXLENBQUNDLElBQWpDLEVBQXVDLE9BQU8sSUFBUCxDQUZwQixDQUluQjtBQUNBOztBQUNBLFFBQUksS0FBSzlELGFBQVQsRUFBd0I7QUFDcEIrRCxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSx3REFBYjtBQUNBO0FBQ0g7O0FBQ0QsU0FBS2hFLGFBQUwsR0FBcUIsSUFBckI7QUFDQTZELElBQUFBLFdBQVcsQ0FBQ0MsSUFBWixDQUFpQixtQ0FBakI7QUFDSCxHQXZSMkI7O0FBeVI1QkgsRUFBQUEsbUJBQW1CLEdBQUc7QUFDbEI7QUFDQSxRQUFJLENBQUNFLFdBQUQsSUFBZ0IsQ0FBQ0EsV0FBVyxDQUFDQyxJQUFqQyxFQUF1QyxPQUFPLElBQVA7O0FBRXZDLFFBQUksQ0FBQyxLQUFLOUQsYUFBVixFQUF5QjtBQUNyQitELE1BQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLG1EQUFiO0FBQ0E7QUFDSDs7QUFDRCxTQUFLaEUsYUFBTCxHQUFxQixLQUFyQjtBQUNBNkQsSUFBQUEsV0FBVyxDQUFDQyxJQUFaLENBQWlCLGtDQUFqQjtBQUNBRCxJQUFBQSxXQUFXLENBQUNJLE9BQVosQ0FDSSxtQ0FESixFQUVJLG1DQUZKLEVBR0ksa0NBSEo7QUFLQUosSUFBQUEsV0FBVyxDQUFDSyxVQUFaLENBQXVCLG1DQUF2QjtBQUNBTCxJQUFBQSxXQUFXLENBQUNLLFVBQVosQ0FBdUIsa0NBQXZCO0FBQ0EsVUFBTUMsV0FBVyxHQUFHTixXQUFXLENBQUNPLGdCQUFaLENBQTZCLG1DQUE3QixFQUFrRUMsR0FBbEUsRUFBcEIsQ0FqQmtCLENBbUJsQjs7QUFDQSxRQUFJLENBQUNGLFdBQUwsRUFBa0IsT0FBTyxJQUFQO0FBRWxCLFdBQU9BLFdBQVcsQ0FBQ0csUUFBbkI7QUFDSCxHQWhUMkI7O0FBa1Q1QmpCLEVBQUFBLHFCQUFxQixDQUFDSSxTQUFELEVBQVl6RSxLQUFaLEVBQW1CO0FBQ3BDLFdBQU95RSxTQUFTLENBQUNsRyxhQUFWLEtBQTRCeUIsS0FBSyxDQUFDekIsYUFBbEMsSUFDSGtHLFNBQVMsQ0FBQ3BHLElBQVYsS0FBbUIyQixLQUFLLENBQUMzQixJQUR0QixJQUVIb0csU0FBUyxDQUFDbkcsU0FBVixLQUF3QjBCLEtBQUssQ0FBQzFCLFNBRmxDO0FBR0gsR0F0VDJCOztBQXdUNUJpSCxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTdkYsS0FBVCxFQUFnQjtBQUNoQyxRQUFJQSxLQUFLLENBQUMzQixJQUFOLEtBQWVtSCxTQUFuQixFQUE4QjtBQUMxQixZQUFNLElBQUlDLEtBQUosQ0FBVSxrQ0FBVixDQUFOO0FBQ0g7O0FBQ0QsVUFBTUMsUUFBUSxHQUFHO0FBQ2JsSCxNQUFBQSxVQUFVLEVBQUU7QUFEQyxLQUFqQjtBQUdBbUgsSUFBQUEsTUFBTSxDQUFDQyxNQUFQLENBQWNGLFFBQWQsRUFBd0IxRixLQUF4QjtBQUNBLFNBQUsrQyxRQUFMLENBQWMyQyxRQUFkO0FBQ0gsR0FqVTJCO0FBbVU1QjdELEVBQUFBLFFBQVEsRUFBRSxVQUFTZ0UsT0FBVCxFQUFrQjtBQUN4QjtBQUNBLFVBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBLFVBQU1DLGNBQWMsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2QixDQUh3QixDQUt4Qjs7QUFDQSxRQUFJeEYsaUNBQWdCTixHQUFoQixNQUF5Qk0saUNBQWdCTixHQUFoQixHQUFzQmdHLE9BQXRCLEVBQXpCLElBQ0E1Six3QkFBd0IsQ0FBQzZKLFFBQXpCLENBQWtDTixPQUFPLENBQUNsQyxNQUExQyxDQURKLEVBRUU7QUFDRTtBQUNBO0FBQ0E7QUFDQTtBQUNBaEMsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFLHdCQURDO0FBRVR5QyxRQUFBQSxlQUFlLEVBQUVQO0FBRlIsT0FBYjs7QUFJQWxFLDBCQUFJK0IsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRTtBQUFULE9BQWI7O0FBQ0E7QUFDSDs7QUFFRCxZQUFRa0MsT0FBTyxDQUFDbEMsTUFBaEI7QUFDSSxXQUFLLDJCQUFMO0FBQ0k7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBSWtDLE9BQU8sQ0FBQ1EsVUFBUixLQUF1QixtQkFBM0IsRUFBZ0Q7QUFDNUMsZ0JBQU1DLE9BQU8sR0FBR1QsT0FBTyxDQUFDVSxhQUFSLEdBQXdCVixPQUFPLENBQUNVLGFBQVIsQ0FBc0IsVUFBdEIsQ0FBeEIsR0FBNEQsSUFBNUU7O0FBQ0EsY0FBSSxDQUFDRCxPQUFMLEVBQWM7QUFDVjlGLDZDQUFnQk4sR0FBaEIsR0FBc0JzRyxvQkFBdEIsQ0FBMkMsSUFBM0M7O0FBQ0FDLFlBQUFBLFlBQVksQ0FBQ0MsVUFBYixDQUF3QixvQkFBeEI7QUFDQUQsWUFBQUEsWUFBWSxDQUFDQyxVQUFiLENBQXdCLFdBQXhCO0FBQ0gsV0FKRCxNQUlPO0FBQ0hsRyw2Q0FBZ0JOLEdBQWhCLEdBQXNCc0csb0JBQXRCLENBQTJDRixPQUEzQzs7QUFDQUcsWUFBQUEsWUFBWSxDQUFDQyxVQUFiLENBQXdCLG9CQUF4QixFQUZHLENBRTRDOztBQUMvQ0QsWUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLFdBQXJCLEVBQWtDTCxPQUFsQyxFQUhHLENBR3lDO0FBQy9DLFdBVjJDLENBWTVDOzs7QUFDQTNFLDhCQUFJK0IsUUFBSixDQUFhO0FBQUNDLFlBQUFBLE1BQU0sRUFBRTtBQUFULFdBQWI7QUFDSDs7QUFDRDs7QUFDSixXQUFLLFFBQUw7QUFDSXRDLFFBQUFBLFNBQVMsQ0FBQ3VGLE1BQVY7QUFDQTs7QUFDSixXQUFLLHNCQUFMO0FBQ0ksb0RBQXlCZixPQUF6QjtBQUNBOztBQUNKLFdBQUssb0JBQUw7QUFDSSxZQUFJeEUsU0FBUyxDQUFDQyxZQUFWLEVBQUosRUFBOEI7QUFDMUIsZUFBS3VGLGFBQUw7O0FBQ0E7QUFDSCxTQUpMLENBS0k7OztBQUNBLFlBQUloQixPQUFPLENBQUNpQixnQkFBWixFQUE4QjtBQUMxQixlQUFLbkcsaUJBQUwsR0FBeUJrRixPQUFPLENBQUNpQixnQkFBakM7QUFDSDs7QUFDRCxhQUFLQyxrQkFBTCxDQUF3QmxCLE9BQU8sQ0FBQzdILE1BQVIsSUFBa0IsRUFBMUM7O0FBQ0E7O0FBQ0osV0FBSyxhQUFMO0FBQ0ksWUFBSXFELFNBQVMsQ0FBQ0MsWUFBVixFQUFKLEVBQThCO0FBQzFCLGVBQUt1RixhQUFMOztBQUNBO0FBQ0g7O0FBQ0QsWUFBSWhCLE9BQU8sQ0FBQ2lCLGdCQUFaLEVBQThCO0FBQzFCLGVBQUtuRyxpQkFBTCxHQUF5QmtGLE9BQU8sQ0FBQ2lCLGdCQUFqQztBQUNIOztBQUNELGFBQUt2QixrQkFBTCxDQUF3QjtBQUNwQmxILFVBQUFBLElBQUksRUFBRTFDLEtBQUssQ0FBQ0c7QUFEUSxTQUF4QjtBQUdBLGFBQUtrTCxlQUFMLENBQXFCLE9BQXJCO0FBQ0FDLGlDQUFnQkMsT0FBaEIsR0FBMEIsSUFBMUI7O0FBQ0EsYUFBS3BGLGFBQUwsQ0FBbUJxRixPQUFuQjs7QUFDQTs7QUFDSixXQUFLLHlCQUFMO0FBQ0ksYUFBS3BFLFFBQUwsQ0FBYztBQUNWMUUsVUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDSztBQURGLFNBQWQ7QUFHQTs7QUFDSixXQUFLLHlCQUFMO0FBQ0ksYUFBS3VKLGtCQUFMLENBQXdCO0FBQ3BCbEgsVUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDTTtBQURRLFNBQXhCO0FBR0EsYUFBSytLLGVBQUwsQ0FBcUIsaUJBQXJCO0FBQ0E7O0FBQ0osV0FBSyxZQUFMO0FBQ0ksaUNBQVc7QUFDUEksVUFBQUEsUUFBUSxFQUFFdkIsT0FBTyxDQUFDd0I7QUFEWCxTQUFYO0FBR0E7O0FBQ0osV0FBSyxZQUFMO0FBQ0ksYUFBS0MsVUFBTCxDQUFnQnpCLE9BQU8sQ0FBQzBCLE9BQXhCOztBQUNBOztBQUNKLFdBQUssZUFBTDtBQUNJQyx1QkFBTUMsbUJBQU4sQ0FBMEIsbUJBQTFCLEVBQStDLEVBQS9DLEVBQW1EeEIsY0FBbkQsRUFBbUU7QUFDL0R5QixVQUFBQSxLQUFLLEVBQUUseUJBQUcsbUJBQUgsQ0FEd0Q7QUFFL0RDLFVBQUFBLFdBQVcsRUFBRSx5QkFBRyxpREFBSCxDQUZrRDtBQUcvREMsVUFBQUEsVUFBVSxFQUFHQyxPQUFELElBQWE7QUFDckIsZ0JBQUlBLE9BQUosRUFBYTtBQUNUO0FBQ0Esb0JBQU1DLE1BQU0sR0FBRy9CLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjs7QUFDQSxvQkFBTStCLEtBQUssR0FBR1AsZUFBTVEsWUFBTixDQUFtQkYsTUFBbkIsRUFBMkIsSUFBM0IsRUFBaUMsbUJBQWpDLENBQWQ7O0FBRUF0SCwrQ0FBZ0JOLEdBQWhCLEdBQXNCK0gsS0FBdEIsQ0FBNEJwQyxPQUFPLENBQUMwQixPQUFwQyxFQUE2Qy9FLElBQTdDLENBQWtELE1BQU07QUFDcER1RixnQkFBQUEsS0FBSyxDQUFDRyxLQUFOOztBQUNBLG9CQUFJLEtBQUtsSSxLQUFMLENBQVd6QixhQUFYLEtBQTZCc0gsT0FBTyxDQUFDMEIsT0FBekMsRUFBa0Q7QUFDOUM1RixzQ0FBSStCLFFBQUosQ0FBYTtBQUFDQyxvQkFBQUEsTUFBTSxFQUFFO0FBQVQsbUJBQWI7QUFDSDtBQUNKLGVBTEQsRUFLSXdFLEdBQUQsSUFBUztBQUNSSixnQkFBQUEsS0FBSyxDQUFDRyxLQUFOOztBQUNBViwrQkFBTUMsbUJBQU4sQ0FBMEIsNkJBQTFCLEVBQXlELEVBQXpELEVBQTZEM0IsV0FBN0QsRUFBMEU7QUFDdEU0QixrQkFBQUEsS0FBSyxFQUFFLHlCQUFHLDZCQUFILENBRCtEO0FBRXRFQyxrQkFBQUEsV0FBVyxFQUFFUSxHQUFHLENBQUNDLFFBQUo7QUFGeUQsaUJBQTFFO0FBSUgsZUFYRDtBQVlIO0FBQ0o7QUF0QjhELFNBQW5FOztBQXdCQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0ksYUFBS0MsU0FBTCxDQUFleEMsT0FBTyxDQUFDeUMsTUFBdkIsRUFBK0J6QyxPQUFPLENBQUMwQyxTQUF2Qzs7QUFDQTs7QUFDSixXQUFLLFdBQUw7QUFBa0I7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFNQyxPQUFPLEdBQUcsS0FBS0MsU0FBTCxDQUFlNUMsT0FBZixDQUFoQjs7QUFDQSxjQUFJQSxPQUFPLENBQUNPLGVBQVosRUFBNkI7QUFDekJvQyxZQUFBQSxPQUFPLENBQUNoRyxJQUFSLENBQWEsTUFBTTtBQUNmYixrQ0FBSStCLFFBQUosQ0FBYW1DLE9BQU8sQ0FBQ08sZUFBckI7QUFDSCxhQUZEO0FBR0g7O0FBQ0Q7QUFDSDs7QUFDRCxXQUFLLGdCQUFMO0FBQ0ksYUFBS3NDLGFBQUwsQ0FBbUIsQ0FBQyxDQUFwQjs7QUFDQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0ksYUFBS0EsYUFBTCxDQUFtQixDQUFuQjs7QUFDQTs7QUFDSixXQUFLLG1CQUFMO0FBQ0ksYUFBS0MsZ0JBQUwsQ0FBc0I5QyxPQUFPLENBQUMrQyxTQUE5Qjs7QUFDQTs7QUFDSixXQUFLLG9CQUFMO0FBQTJCO0FBQ3ZCLGdCQUFNQyxrQkFBa0IsR0FBRzlDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw0QkFBakIsQ0FBM0I7O0FBQ0F3Qix5QkFBTUMsbUJBQU4sQ0FBMEIsZUFBMUIsRUFBMkMsRUFBM0MsRUFBK0NvQixrQkFBL0MsRUFBbUUsRUFBbkU7QUFDSTtBQUFjLGNBRGxCO0FBQ3dCO0FBQWUsZUFEdkM7QUFDOEM7QUFBYSxjQUQzRCxFQUZ1QixDQUt2Qjs7O0FBQ0EsZUFBS0MseUJBQUw7O0FBQ0E7QUFDSDs7QUFDRCxXQUFLLGtCQUFMO0FBQ0ksYUFBS0MsV0FBTDs7QUFDQTs7QUFDSixXQUFLLG1CQUFMO0FBQTBCO0FBQ3RCLGdCQUFNQyxpQkFBaUIsR0FBR2pELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBMUI7O0FBQ0F3Qix5QkFBTUMsbUJBQU4sQ0FBMEIsa0JBQTFCLEVBQThDLEVBQTlDLEVBQWtEdUIsaUJBQWxEO0FBQ0g7QUFDRDs7QUFDQSxXQUFLLHFCQUFMO0FBQTRCO0FBQ3hCLGdCQUFNQyxhQUFhLEdBQUdsRCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQXRCOztBQUNBd0IseUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxFQUE1QyxFQUFnRHdCLGFBQWhELEVBQStELEVBQS9ELEVBQ0ksZ0NBREosRUFDc0MsS0FEdEMsRUFDNkMsSUFEN0MsRUFGd0IsQ0FLeEI7OztBQUNBLGVBQUtILHlCQUFMO0FBQ0g7QUFDRDs7QUFDQSxXQUFLLGdCQUFMO0FBQ0ksYUFBS0ksUUFBTCxDQUFjQyxtQkFBVUMsUUFBeEI7O0FBQ0EsYUFBS3BDLGVBQUwsQ0FBcUIsUUFBckI7QUFDQTs7QUFDSixXQUFLLFlBQUw7QUFDSSxhQUFLcUMsVUFBTCxDQUFnQnhELE9BQWhCOztBQUNBOztBQUNKLFdBQUssbUJBQUw7QUFDSSxhQUFLeUQsWUFBTDs7QUFDQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0ksYUFBS0MsU0FBTDs7QUFDQTs7QUFDSixXQUFLLGVBQUw7QUFDSSxhQUFLQyxRQUFMLENBQWMzRCxPQUFkOztBQUNBOztBQUNKLFdBQUssMEJBQUw7QUFDSSxhQUFLNEQsa0JBQUwsQ0FBd0I1RCxPQUFPLENBQUN3QixPQUFoQzs7QUFDQTs7QUFDSixXQUFLLGtCQUFMO0FBQ0k7QUFDQTs7QUFDSixXQUFLLGFBQUw7QUFDSSw4Q0FBcUJ4QixPQUFPLENBQUM2RCxNQUE3QjtBQUNBOztBQUNKLFdBQUssa0JBQUw7QUFDSTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUsvRyxxQkFBTDs7QUFDQTs7QUFDSixXQUFLLGtCQUFMO0FBQ0k7QUFDQTtBQUNBLFlBQUksS0FBSzNDLEtBQUwsQ0FBVzFCLFNBQVgsS0FBeUI2SyxtQkFBVUMsUUFBdkMsRUFBaUQ7QUFDN0N6SCw4QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxZQUFBQSxNQUFNLEVBQUU7QUFBVCxXQUFiO0FBQ0gsU0FGRCxNQUVPO0FBQ0hoQyw4QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxZQUFBQSxNQUFNLEVBQUU7QUFBVCxXQUFiO0FBQ0g7O0FBQ0Q7O0FBQ0osV0FBSyxrQkFBTDtBQUF5QjtBQUNqQixlQUFLWixRQUFMLENBQWM7QUFBQ3RELFlBQUFBLG1CQUFtQixFQUFFa0ssa0JBQVNDLGlCQUFUO0FBQXRCLFdBQWQ7QUFDSDtBQUNEOztBQUNKLFdBQUssaUJBQUw7QUFDSSxhQUFLN0csUUFBTCxDQUFjO0FBQ1Z0RSxVQUFBQSxXQUFXLEVBQUU7QUFESCxTQUFkO0FBR0E7O0FBQ0osV0FBSyxtQkFBTCxDQTFNSixDQTBNOEI7O0FBQzFCLFdBQUssaUJBQUw7QUFDSSxhQUFLc0UsUUFBTCxDQUFjO0FBQ1Z0RSxVQUFBQSxXQUFXLEVBQUU7QUFESCxTQUFkO0FBR0E7O0FBQ0osV0FBSyxlQUFMO0FBQXNCO0FBQ2xCLGVBQUtzRSxRQUFMLENBQWM7QUFDVnJFLFlBQUFBLFlBQVksRUFBRW1ILE9BQU8sQ0FBQ25ILFlBQVIsSUFBd0JtSCxPQUFPLENBQUNnRSxZQUFoQyxJQUFnRCxLQURwRDtBQUVWbEwsWUFBQUEsY0FBYyxFQUFFa0gsT0FBTyxDQUFDbEgsY0FBUixJQUEwQixLQUZoQyxDQUdWOztBQUhVLFdBQWQ7QUFLQTtBQUNIOztBQUNELFdBQUssY0FBTDtBQUNJLFlBQ0ksQ0FBQzBDLFNBQVMsQ0FBQ0MsWUFBVixFQUFELElBQ0EsS0FBS3RCLEtBQUwsQ0FBVzNCLElBQVgsS0FBb0IxQyxLQUFLLENBQUNHLEtBRDFCLElBRUEsS0FBS2tFLEtBQUwsQ0FBVzNCLElBQVgsS0FBb0IxQyxLQUFLLENBQUNJLFFBRjFCLElBR0EsS0FBS2lFLEtBQUwsQ0FBVzNCLElBQVgsS0FBb0IxQyxLQUFLLENBQUNPLGlCQUgxQixJQUlBLEtBQUs4RCxLQUFMLENBQVczQixJQUFYLEtBQW9CMUMsS0FBSyxDQUFDUSxTQUw5QixFQU1FO0FBQ0UsZUFBSzJOLFdBQUw7QUFDSDs7QUFDRDs7QUFDSixXQUFLLHNCQUFMO0FBQ0ksYUFBS2pELGFBQUw7O0FBQ0E7O0FBQ0osV0FBSyxlQUFMO0FBQ0ksYUFBS2tELFlBQUw7O0FBQ0E7O0FBQ0osV0FBSyxtQkFBTDtBQUNJLGFBQUtoSCxRQUFMLENBQWM7QUFBQ2lILFVBQUFBLEtBQUssRUFBRTtBQUFSLFNBQWQsRUFBOEIsTUFBTTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxlQUFLQyxrQkFBTDtBQUNILFNBTEQ7QUFNQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0ksYUFBS0MsZ0JBQUw7O0FBQ0E7O0FBQ0osV0FBSyxhQUFMO0FBQ0ksYUFBS0MsU0FBTCxDQUNJdEUsT0FBTyxDQUFDdUUsY0FEWixFQUM0QnZFLE9BQU8sQ0FBQ2hILFVBRHBDLEVBRUlnSCxPQUFPLENBQUN3RSxZQUZaO0FBSUE7O0FBQ0osV0FBSyxlQUFMO0FBQ0ksYUFBS3RILFFBQUwsQ0FBYztBQUFFL0QsVUFBQUEsaUJBQWlCLEVBQUU2RyxPQUFPLENBQUN5RTtBQUE3QixTQUFkO0FBQ0E7O0FBQ0osV0FBSyxZQUFMO0FBQ0ksYUFBS0MsV0FBTCxDQUFpQjFFLE9BQU8sQ0FBQzBCLE9BQXpCLEVBQWtDMUIsT0FBTyxDQUFDMkUsS0FBMUM7QUFDQTs7QUFDSixXQUFLLG9CQUFMO0FBQ0ksYUFBS3pILFFBQUwsQ0FBYztBQUNWMUQsVUFBQUEsYUFBYSxFQUFFO0FBREwsU0FBZDtBQUdBOztBQUNKLFdBQUssc0JBQUw7QUFDSSxhQUFLMEQsUUFBTCxDQUFjO0FBQ1YxRCxVQUFBQSxhQUFhLEVBQUU7QUFETCxTQUFkO0FBR0E7O0FBQ0osV0FBSyxnQkFBTDtBQUNJd0QsK0JBQWM0SCxRQUFkLENBQXVCLGdCQUF2QixFQUF5QyxJQUF6QyxFQUErQ0MsNEJBQWFDLE1BQTVELEVBQW9FLElBQXBFOztBQUNBOUgsK0JBQWM0SCxRQUFkLENBQXVCLGVBQXZCLEVBQXdDLElBQXhDLEVBQThDQyw0QkFBYUMsTUFBM0QsRUFBbUUsS0FBbkU7O0FBRUEsYUFBSzVILFFBQUwsQ0FBYztBQUNWOUQsVUFBQUEsYUFBYSxFQUFFO0FBREwsU0FBZDs7QUFHQStELDJCQUFVQyxNQUFWOztBQUNBOztBQUNKLFdBQUssZ0JBQUw7QUFDSUosK0JBQWM0SCxRQUFkLENBQXVCLGdCQUF2QixFQUF5QyxJQUF6QyxFQUErQ0MsNEJBQWFDLE1BQTVELEVBQW9FLEtBQXBFOztBQUNBOUgsK0JBQWM0SCxRQUFkLENBQXVCLGVBQXZCLEVBQXdDLElBQXhDLEVBQThDQyw0QkFBYUMsTUFBM0QsRUFBbUUsS0FBbkU7O0FBRUEsYUFBSzVILFFBQUwsQ0FBYztBQUNWOUQsVUFBQUEsYUFBYSxFQUFFO0FBREwsU0FBZDtBQUdBO0FBMVJSO0FBNFJILEdBcG5CMkI7QUFzbkI1QmlLLEVBQUFBLFFBQVEsRUFBRSxVQUFTMEIsUUFBVCxFQUFtQjtBQUN6QixTQUFLN0gsUUFBTCxDQUFjO0FBQ1Z6RSxNQUFBQSxTQUFTLEVBQUVzTTtBQURELEtBQWQ7QUFHSCxHQTFuQjJCO0FBNG5CNUI3RCxFQUFBQSxrQkFBa0IsRUFBRSxnQkFBZS9JLE1BQWYsRUFBdUI7QUFDdkMsVUFBTTBILFFBQVEsR0FBRztBQUNickgsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDSTtBQURDLEtBQWpCLENBRHVDLENBS3ZDO0FBQ0E7O0FBQ0EsUUFBSWlDLE1BQU0sQ0FBQzZNLGFBQVAsSUFDQTdNLE1BQU0sQ0FBQzhNLFVBRFAsSUFFQTlNLE1BQU0sQ0FBQytNLE1BRlAsSUFHQS9NLE1BQU0sQ0FBQ2dOLE1BSFAsSUFJQWhOLE1BQU0sQ0FBQ2lOLEdBSlgsRUFLRTtBQUNFdkYsTUFBQUEsUUFBUSxDQUFDN0ksWUFBVCxHQUF3QixNQUFNcU8sNEJBQW1CQyxrQ0FBbkIsQ0FDMUJuTixNQUFNLENBQUMrTSxNQURtQixFQUNYL00sTUFBTSxDQUFDZ04sTUFESSxDQUE5QjtBQUlBdEYsTUFBQUEsUUFBUSxDQUFDeEcsc0JBQVQsR0FBa0NsQixNQUFNLENBQUM2TSxhQUF6QztBQUNBbkYsTUFBQUEsUUFBUSxDQUFDdkcsbUJBQVQsR0FBK0JuQixNQUFNLENBQUM4TSxVQUF0QztBQUNBcEYsTUFBQUEsUUFBUSxDQUFDdEcsZUFBVCxHQUEyQnBCLE1BQU0sQ0FBQ2lOLEdBQWxDO0FBQ0g7O0FBRUQsU0FBSzFGLGtCQUFMLENBQXdCRyxRQUF4QjtBQUNBdUIsNkJBQWdCQyxPQUFoQixHQUEwQixJQUExQjs7QUFDQSxTQUFLcEYsYUFBTCxDQUFtQnFGLE9BQW5COztBQUNBLFNBQUtILGVBQUwsQ0FBcUIsVUFBckI7QUFDSCxHQXRwQjJCO0FBd3BCNUI7QUFDQTBCLEVBQUFBLGFBQWEsRUFBRSxVQUFTMEMsY0FBVCxFQUF5QjtBQUNwQyxVQUFNQyxRQUFRLEdBQUdDLGNBQWMsQ0FBQ0MsdUJBQWYsQ0FDYi9LLGlDQUFnQk4sR0FBaEIsR0FBc0JzTCxRQUF0QixFQURhLENBQWpCLENBRG9DLENBSXBDO0FBQ0E7QUFDQTs7QUFDQSxRQUFJSCxRQUFRLENBQUNJLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDckI5SiwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUU7QUFEQyxPQUFiOztBQUdBO0FBQ0g7O0FBQ0QsUUFBSWlGLFNBQVMsR0FBRyxDQUFDLENBQWpCOztBQUNBLFNBQUssSUFBSThDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdMLFFBQVEsQ0FBQ0ksTUFBN0IsRUFBcUMsRUFBRUMsQ0FBdkMsRUFBMEM7QUFDdEMsVUFBSUwsUUFBUSxDQUFDSyxDQUFELENBQVIsQ0FBWWhDLE1BQVosSUFBc0IsS0FBSzFKLEtBQUwsQ0FBV3pCLGFBQXJDLEVBQW9EO0FBQ2hEcUssUUFBQUEsU0FBUyxHQUFHOEMsQ0FBWjtBQUNBO0FBQ0g7QUFDSjs7QUFDRDlDLElBQUFBLFNBQVMsR0FBRyxDQUFDQSxTQUFTLEdBQUd3QyxjQUFiLElBQStCQyxRQUFRLENBQUNJLE1BQXBEO0FBQ0EsUUFBSTdDLFNBQVMsR0FBRyxDQUFoQixFQUFtQkEsU0FBUyxHQUFHeUMsUUFBUSxDQUFDSSxNQUFULEdBQWtCLENBQTlCOztBQUNuQjlKLHdCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxXQURDO0FBRVQ0RCxNQUFBQSxPQUFPLEVBQUU4RCxRQUFRLENBQUN6QyxTQUFELENBQVIsQ0FBb0JjO0FBRnBCLEtBQWI7QUFJSCxHQW5yQjJCO0FBcXJCNUI7QUFDQWYsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQjtBQUNsQyxVQUFNeUMsUUFBUSxHQUFHQyxjQUFjLENBQUNDLHVCQUFmLENBQ2IvSyxpQ0FBZ0JOLEdBQWhCLEdBQXNCc0wsUUFBdEIsRUFEYSxDQUFqQjs7QUFHQSxRQUFJSCxRQUFRLENBQUN6QyxTQUFELENBQVosRUFBeUI7QUFDckJqSCwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUNEQsUUFBQUEsT0FBTyxFQUFFOEQsUUFBUSxDQUFDekMsU0FBRCxDQUFSLENBQW9CYztBQUZwQixPQUFiO0FBSUg7QUFDSixHQWhzQjJCO0FBa3NCNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBakIsRUFBQUEsU0FBUyxFQUFFLFVBQVNrRCxRQUFULEVBQW1CO0FBQzFCLFNBQUsxSixhQUFMLEdBQXFCLElBQXJCO0FBRUEsVUFBTXlELFFBQVEsR0FBRztBQUNickgsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDUyxTQURDO0FBRWJtQyxNQUFBQSxhQUFhLEVBQUVvTixRQUFRLENBQUNwRSxPQUFULElBQW9CLElBRnRCO0FBR2JqSixNQUFBQSxTQUFTLEVBQUU2SyxtQkFBVXlDLFFBSFI7QUFJYkMsTUFBQUEsZ0JBQWdCLEVBQUVGLFFBQVEsQ0FBQ0csa0JBSmQ7QUFLYkMsTUFBQUEsV0FBVyxFQUFFSixRQUFRLENBQUNLLFFBTFQ7QUFNYkMsTUFBQUEsVUFBVSxFQUFFTixRQUFRLENBQUNPO0FBTlIsS0FBakI7O0FBU0EsUUFBSVAsUUFBUSxDQUFDUSxVQUFiLEVBQXlCO0FBQ3JCcEgsTUFBQUEsT0FBTyxDQUFDcUgsR0FBUixDQUNJLGtDQUEyQlQsUUFBUSxDQUFDUSxVQUFwQyxrQkFDQVIsUUFBUSxDQUFDVSxRQUZiO0FBSUgsS0FMRCxNQUtPO0FBQ0h0SCxNQUFBQSxPQUFPLENBQUNxSCxHQUFSLENBQVksK0JBQXdCVCxRQUFRLENBQUNwRSxPQUFqQyxrQkFDUm9FLFFBQVEsQ0FBQ1UsUUFEYjtBQUdILEtBckJ5QixDQXVCMUI7QUFDQTs7O0FBQ0EsUUFBSUMsT0FBTyxHQUFHcEosT0FBTyxDQUFDQyxPQUFSLENBQWdCLElBQWhCLENBQWQ7O0FBQ0EsUUFBSSxDQUFDLEtBQUs5QyxpQkFBVixFQUE2QjtBQUN6QixVQUFJLENBQUMsS0FBS0MsZ0JBQVYsRUFBNEI7QUFDeEJ5RSxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxnREFBYixFQUErRDJHLFFBQVEsQ0FBQ3BFLE9BQXhFO0FBQ0E7QUFDSDs7QUFDRCtFLE1BQUFBLE9BQU8sR0FBRyxLQUFLaE0sZ0JBQUwsQ0FBc0JrSSxPQUFoQztBQUNIOztBQUVELFdBQU84RCxPQUFPLENBQUM5SixJQUFSLENBQWEsTUFBTTtBQUN0QixVQUFJK0osV0FBVyxHQUFHWixRQUFRLENBQUNRLFVBQVQsSUFBdUJSLFFBQVEsQ0FBQ3BFLE9BQWxEOztBQUNBLFlBQU1pRixJQUFJLEdBQUdoTSxpQ0FBZ0JOLEdBQWhCLEdBQXNCdU0sT0FBdEIsQ0FBOEJkLFFBQVEsQ0FBQ3BFLE9BQXZDLENBQWI7O0FBQ0EsVUFBSWlGLElBQUosRUFBVTtBQUNOLGNBQU1FLFFBQVEsR0FBR0MsS0FBSyxDQUFDQyxzQkFBTixDQUE2QkosSUFBN0IsQ0FBakI7O0FBQ0EsWUFBSUUsUUFBSixFQUFjO0FBQ1ZILFVBQUFBLFdBQVcsR0FBR0csUUFBZCxDQURVLENBRVY7QUFDQTs7QUFDQSxxREFBc0JBLFFBQXRCLEVBQWdDRixJQUFJLENBQUM5QyxNQUFyQztBQUNILFNBUEssQ0FTTjtBQUNBOzs7QUFDQSxZQUFJakQsWUFBSixFQUFrQjtBQUNkQSxVQUFBQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUIsaUJBQXJCLEVBQXdDNkYsSUFBSSxDQUFDOUMsTUFBN0M7QUFDSDtBQUNKOztBQUVELFVBQUlpQyxRQUFRLENBQUNVLFFBQVQsSUFBcUJWLFFBQVEsQ0FBQ2tCLFdBQWxDLEVBQStDO0FBQzNDTixRQUFBQSxXQUFXLElBQUksTUFBTVosUUFBUSxDQUFDVSxRQUE5QjtBQUNIOztBQUNEM0csTUFBQUEsUUFBUSxDQUFDc0UsS0FBVCxHQUFpQixJQUFqQjtBQUNBLFdBQUtqSCxRQUFMLENBQWMyQyxRQUFkLEVBQXdCLE1BQU07QUFDMUIsYUFBS3NCLGVBQUwsQ0FBcUIsVUFBVXVGLFdBQS9CO0FBQ0gsT0FGRDtBQUdILEtBMUJNLENBQVA7QUEyQkgsR0FoeEIyQjtBQWt4QjVCbEQsRUFBQUEsVUFBVSxFQUFFLFVBQVN4RCxPQUFULEVBQWtCO0FBQzFCLFVBQU1pSCxPQUFPLEdBQUdqSCxPQUFPLENBQUNrSCxRQUF4QjtBQUNBLFNBQUtoSyxRQUFMLENBQWM7QUFDVmlLLE1BQUFBLGNBQWMsRUFBRUYsT0FETjtBQUVWRyxNQUFBQSxpQkFBaUIsRUFBRXBILE9BQU8sQ0FBQ3FIO0FBRmpCLEtBQWQ7O0FBSUEsU0FBS2hFLFFBQUwsQ0FBY0MsbUJBQVVnRSxTQUF4Qjs7QUFDQSxTQUFLbkcsZUFBTCxDQUFxQixXQUFXOEYsT0FBaEM7QUFDSCxHQTF4QjJCOztBQTR4QjVCaEUsRUFBQUEseUJBQXlCLEdBQUc7QUFDeEIsUUFBSSxLQUFLOUksS0FBTCxDQUFXM0IsSUFBWCxLQUFvQjFDLEtBQUssQ0FBQ1MsU0FBOUIsRUFBeUM7QUFDckMsV0FBS2tOLFlBQUw7O0FBQ0E7QUFDSDs7QUFDRCxRQUFJLENBQUMsS0FBS3RKLEtBQUwsQ0FBV2dOLGNBQVosSUFBOEIsQ0FBQyxLQUFLaE4sS0FBTCxDQUFXekIsYUFBOUMsRUFBNkQ7QUFDekQsV0FBS2dMLFNBQUw7QUFDSDtBQUNKLEdBcHlCMkI7O0FBc3lCNUJELEVBQUFBLFlBQVksR0FBRztBQUNYLFNBQUsvRCxrQkFBTCxDQUF3QjtBQUNwQmxILE1BQUFBLElBQUksRUFBRTFDLEtBQUssQ0FBQ0U7QUFEUSxLQUF4QjtBQUdBLFNBQUttTCxlQUFMLENBQXFCLFNBQXJCO0FBQ0FDLDZCQUFnQkMsT0FBaEIsR0FBMEIsSUFBMUI7O0FBQ0EsU0FBS3BGLGFBQUwsQ0FBbUJxRixPQUFuQjtBQUNILEdBN3lCMkI7O0FBK3lCNUJvQyxFQUFBQSxTQUFTLEVBQUUsWUFBVztBQUNsQjtBQUNBLFNBQUtoRSxrQkFBTCxDQUF3QjtBQUNwQmxILE1BQUFBLElBQUksRUFBRTFDLEtBQUssQ0FBQ1M7QUFEUSxLQUF4Qjs7QUFHQSxTQUFLOE0sUUFBTCxDQUFjQyxtQkFBVWlFLFFBQXhCOztBQUNBLFNBQUtwRyxlQUFMLENBQXFCLE1BQXJCO0FBQ0FDLDZCQUFnQkMsT0FBaEIsR0FBMEIsS0FBMUI7O0FBQ0EsU0FBS3BGLGFBQUwsQ0FBbUJxRixPQUFuQjtBQUNILEdBeHpCMkI7QUEwekI1QmtCLEVBQUFBLFNBQVMsRUFBRSxVQUFTQyxNQUFULEVBQWlCQyxTQUFqQixFQUE0QjtBQUNuQztBQUNBO0FBQ0EsVUFBTThFLFdBQVcsR0FBRyxLQUFLL00sZ0JBQUwsR0FDaEIsS0FBS0EsZ0JBQUwsQ0FBc0JrSSxPQUROLEdBQ2dCdEYsT0FBTyxDQUFDQyxPQUFSLEVBRHBDO0FBRUFrSyxJQUFBQSxXQUFXLENBQUM3SyxJQUFaLENBQWlCLE1BQU07QUFDbkIsVUFBSStGLFNBQVMsS0FBSyxNQUFsQixFQUEwQjtBQUN0QixhQUFLa0Isa0JBQUwsQ0FBd0JuQixNQUF4Qjs7QUFDQTtBQUNIOztBQUNELFdBQUt0QixlQUFMLENBQXFCLFVBQVVzQixNQUEvQjtBQUNBLFdBQUt2RixRQUFMLENBQWM7QUFBQ3VLLFFBQUFBLGFBQWEsRUFBRWhGO0FBQWhCLE9BQWQ7O0FBQ0EsV0FBS1ksUUFBTCxDQUFjQyxtQkFBVW9FLFFBQXhCO0FBQ0gsS0FSRDtBQVNILEdBeDBCMkI7QUEwMEI1Qi9ELEVBQUFBLFFBQVEsRUFBRSxVQUFTM0QsT0FBVCxFQUFrQjtBQUN4QixVQUFNMkgsYUFBYSxHQUFHekgsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDZCQUFqQixDQUF0Qjs7QUFDQSxVQUFNa0MsS0FBSyxHQUFHVixlQUFNQyxtQkFBTixDQUEwQixVQUExQixFQUFzQyxFQUF0QyxFQUEwQytGLGFBQTFDLEVBQXlEO0FBQ25FQyxNQUFBQSxhQUFhLEVBQUVqTixpQ0FBZ0JOLEdBQWhCLEdBQXNCd04sZ0JBQXRCLEVBRG9EO0FBRW5FOUYsTUFBQUEsVUFBVSxFQUFFLENBQUMrRixTQUFELEVBQVlDLFdBQVosS0FBNEI7QUFDcEMsWUFBSSxDQUFDRCxTQUFMLEVBQWdCO0FBQ1poTSw4QkFBSStCLFFBQUosQ0FBYTtBQUNUQyxZQUFBQSxNQUFNLEVBQUU7QUFEQyxXQUFiOztBQUdBLGNBQUlrQyxPQUFPLENBQUNnSSxpQkFBWixFQUErQjtBQUMzQmxNLGdDQUFJK0IsUUFBSixDQUFhO0FBQ1RDLGNBQUFBLE1BQU0sRUFBRTtBQURDLGFBQWI7QUFHSDs7QUFDRDtBQUNIOztBQUNEbkQseUNBQWdCc04sdUJBQWhCLENBQXdDRixXQUFXLENBQUN2RyxPQUFwRDs7QUFDQSxhQUFLMEcsWUFBTCxDQUFrQkgsV0FBbEI7QUFDSCxPQWhCa0U7QUFpQm5FSSxNQUFBQSx3QkFBd0IsRUFBR0MsRUFBRCxJQUFRO0FBQzlCdE0sNEJBQUkrQixRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjs7QUFDQXVFLFFBQUFBLEtBQUs7QUFDUixPQXBCa0U7QUFxQm5FZ0csTUFBQUEsWUFBWSxFQUFHRCxFQUFELElBQVE7QUFDbEJ0TSw0QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiOztBQUNBdUUsUUFBQUEsS0FBSztBQUNSO0FBeEJrRSxLQUF6RCxFQXlCWEEsS0F6Qkg7QUEwQkgsR0F0MkIyQjtBQXcyQjVCYSxFQUFBQSxXQUFXLEVBQUUsa0JBQWlCO0FBQzFCLFVBQU1vRixnQkFBZ0IsR0FBR3BJLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBekI7O0FBQ0EsVUFBTStCLEtBQUssR0FBR1AsZUFBTUMsbUJBQU4sQ0FBMEIsYUFBMUIsRUFBeUMsRUFBekMsRUFBNkMwRyxnQkFBN0MsQ0FBZDs7QUFFQSxVQUFNLENBQUNDLFlBQUQsRUFBZTNOLElBQWYsSUFBdUIsTUFBTXNILEtBQUssQ0FBQ3NHLFFBQXpDOztBQUNBLFFBQUlELFlBQUosRUFBa0I7QUFDZCwrQkFBVzNOLElBQVg7QUFDSDtBQUNKLEdBaDNCMkI7QUFrM0I1QmdKLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNuQixNQUFULEVBQWlCO0FBQ2pDO0FBQ0EsUUFBSTlILGlDQUFnQk4sR0FBaEIsR0FBc0JnRyxPQUF0QixFQUFKLEVBQXFDO0FBQ2pDO0FBQ0E7QUFDQSxVQUFJb0MsTUFBTSxLQUFLLEtBQUsxSSxLQUFMLENBQVdsRCxNQUFYLENBQWtCNFIsYUFBakMsRUFBZ0Q7QUFDNUMzTSw0QkFBSStCLFFBQUosQ0FBYTtBQUNUQyxVQUFBQSxNQUFNLEVBQUUsd0JBREM7QUFFVHlDLFVBQUFBLGVBQWUsRUFBRTtBQUNiekMsWUFBQUEsTUFBTSxFQUFFLDBCQURLO0FBRWIwRCxZQUFBQSxPQUFPLEVBQUVpQjtBQUZJO0FBRlIsU0FBYjtBQU9IOztBQUNEM0csMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFLHNCQURDO0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTRLLFFBQUFBLG9CQUFvQixFQUFFLElBTmI7QUFPVEMsUUFBQUEsWUFBWSxFQUFFO0FBQ1YxUSxVQUFBQSxNQUFNLGlCQUFVLEtBQUs4QixLQUFMLENBQVdsRCxNQUFYLENBQWtCNFIsYUFBNUIsQ0FESTtBQUVWdFEsVUFBQUEsTUFBTSxFQUFFO0FBQUUyRixZQUFBQSxNQUFNLEVBQUU7QUFBVjtBQUZFO0FBUEwsT0FBYjs7QUFZQTtBQUNILEtBM0JnQyxDQTZCakM7OztBQUVBLFVBQU04SyxNQUFNLEdBQUdqTyxpQ0FBZ0JOLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTXdPLFNBQVMsR0FBRyxJQUFJQyxrQkFBSixDQUFjRixNQUFkLENBQWxCO0FBQ0EsVUFBTUcsT0FBTyxHQUFHRixTQUFTLENBQUNHLG1CQUFWLENBQThCdkcsTUFBOUIsQ0FBaEI7O0FBRUEsUUFBSXNHLE9BQU8sQ0FBQ25ELE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEI5SiwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUNEQsUUFBQUEsT0FBTyxFQUFFcUgsT0FBTyxDQUFDLENBQUQ7QUFGUCxPQUFiO0FBSUgsS0FMRCxNQUtPO0FBQ0hqTiwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUMEQsUUFBQUEsT0FBTyxFQUFFaUI7QUFGQSxPQUFiO0FBSUg7QUFDSixHQWg2QjJCO0FBazZCNUJ3RyxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTcEYsTUFBVCxFQUFpQjtBQUNqQyxVQUFNcUYsV0FBVyxHQUFHdk8saUNBQWdCTixHQUFoQixHQUFzQnVNLE9BQXRCLENBQThCL0MsTUFBOUIsQ0FBcEIsQ0FEaUMsQ0FFakM7OztBQUNBLFVBQU1zRixTQUFTLEdBQUdELFdBQVcsQ0FBQ0UsWUFBWixDQUF5QkMsY0FBekIsQ0FBd0MsbUJBQXhDLEVBQTZELEVBQTdELENBQWxCO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLEVBQWpCOztBQUNBLFFBQUlILFNBQUosRUFBZTtBQUNYLFlBQU1JLElBQUksR0FBR0osU0FBUyxDQUFDSyxVQUFWLEdBQXVCQyxTQUFwQzs7QUFDQSxVQUFJRixJQUFJLEtBQUssUUFBYixFQUF1QjtBQUNuQkQsUUFBQUEsUUFBUSxDQUFDSSxJQUFULENBQ0k7QUFBTSxVQUFBLFNBQVMsRUFBQyxTQUFoQjtBQUEwQixVQUFBLEdBQUcsRUFBQztBQUE5QixXQUNLO0FBQUc7QUFEUixVQUVNLHlCQUFHLDRFQUFILENBRk4sQ0FESjtBQU1IO0FBQ0o7O0FBQ0QsV0FBT0osUUFBUDtBQUNILEdBbjdCMkI7QUFxN0I1QjdILEVBQUFBLFVBQVUsRUFBRSxVQUFTb0MsTUFBVCxFQUFpQjtBQUN6QixVQUFNekQsY0FBYyxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCO0FBQ0EsVUFBTUYsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBLFVBQU0rSSxXQUFXLEdBQUd2TyxpQ0FBZ0JOLEdBQWhCLEdBQXNCdU0sT0FBdEIsQ0FBOEIvQyxNQUE5QixDQUFwQjs7QUFDQSxVQUFNeUYsUUFBUSxHQUFHLEtBQUtMLGtCQUFMLENBQXdCcEYsTUFBeEIsQ0FBakI7O0FBRUFsQyxtQkFBTUMsbUJBQU4sQ0FBMEIsWUFBMUIsRUFBd0MsRUFBeEMsRUFBNEN4QixjQUE1QyxFQUE0RDtBQUN4RHlCLE1BQUFBLEtBQUssRUFBRSx5QkFBRyxZQUFILENBRGlEO0FBRXhEQyxNQUFBQSxXQUFXLEVBQ1AsMkNBQ0UseUJBQUcseURBQUgsRUFBOEQ7QUFBQzZILFFBQUFBLFFBQVEsRUFBRVQsV0FBVyxDQUFDVTtBQUF2QixPQUE5RCxDQURGLEVBRUVOLFFBRkYsQ0FIb0Q7QUFReERPLE1BQUFBLE1BQU0sRUFBRSx5QkFBRyxPQUFILENBUmdEO0FBU3hEOUgsTUFBQUEsVUFBVSxFQUFHK0gsV0FBRCxJQUFpQjtBQUN6QixZQUFJQSxXQUFKLEVBQWlCO0FBQ2IsZ0JBQU1DLENBQUMsR0FBR3BQLGlDQUFnQk4sR0FBaEIsR0FBc0IyUCxjQUF0QixDQUFxQ25HLE1BQXJDLENBQVYsQ0FEYSxDQUdiOzs7QUFDQSxnQkFBTTVCLE1BQU0sR0FBRy9CLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjs7QUFDQSxnQkFBTStCLEtBQUssR0FBR1AsZUFBTVEsWUFBTixDQUFtQkYsTUFBbkIsRUFBMkIsSUFBM0IsRUFBaUMsbUJBQWpDLENBQWQ7O0FBRUE4SCxVQUFBQSxDQUFDLENBQUNwTixJQUFGLENBQVFzTixNQUFELElBQVk7QUFDZi9ILFlBQUFBLEtBQUssQ0FBQ0csS0FBTjs7QUFFQSxpQkFBSyxNQUFNNkgsVUFBWCxJQUF5QnBLLE1BQU0sQ0FBQ3FLLElBQVAsQ0FBWUYsTUFBWixDQUF6QixFQUE4QztBQUMxQyxvQkFBTTNILEdBQUcsR0FBRzJILE1BQU0sQ0FBQ0MsVUFBRCxDQUFsQjtBQUNBLGtCQUFJLENBQUM1SCxHQUFMLEVBQVU7QUFFVnBELGNBQUFBLE9BQU8sQ0FBQ2tMLEtBQVIsQ0FBYywwQkFBMEJGLFVBQTFCLEdBQXVDLEdBQXZDLEdBQTZDNUgsR0FBM0Q7QUFDQSxrQkFBSVQsS0FBSyxHQUFHLHlCQUFHLHNCQUFILENBQVo7QUFDQSxrQkFBSXdJLE9BQU8sR0FBRyx5QkFBRywwREFBSCxDQUFkOztBQUNBLGtCQUFJL0gsR0FBRyxDQUFDZ0ksT0FBSixLQUFnQixtQ0FBcEIsRUFBeUQ7QUFDckR6SSxnQkFBQUEsS0FBSyxHQUFHLHlCQUFHLGlDQUFILENBQVI7QUFDQXdJLGdCQUFBQSxPQUFPLEdBQUcseUJBQ04sbUVBQ0EseUJBRk0sQ0FBVjtBQUlILGVBTkQsTUFNTyxJQUFJL0gsR0FBRyxJQUFJQSxHQUFHLENBQUMrSCxPQUFmLEVBQXdCO0FBQzNCQSxnQkFBQUEsT0FBTyxHQUFHL0gsR0FBRyxDQUFDK0gsT0FBZDtBQUNIOztBQUNEMUksNkJBQU1DLG1CQUFOLENBQTBCLHNCQUExQixFQUFrRCxFQUFsRCxFQUFzRDNCLFdBQXRELEVBQW1FO0FBQy9ENEIsZ0JBQUFBLEtBQUssRUFBRUEsS0FEd0Q7QUFFL0RDLGdCQUFBQSxXQUFXLEVBQUV1STtBQUZrRCxlQUFuRTs7QUFJQTtBQUNIOztBQUVELGdCQUFJLEtBQUtsUSxLQUFMLENBQVd6QixhQUFYLEtBQTZCbUwsTUFBakMsRUFBeUM7QUFDckMvSCxrQ0FBSStCLFFBQUosQ0FBYTtBQUFDQyxnQkFBQUEsTUFBTSxFQUFFO0FBQVQsZUFBYjtBQUNIO0FBQ0osV0E3QkQsRUE2Qkl3RSxHQUFELElBQVM7QUFDUjtBQUNBSixZQUFBQSxLQUFLLENBQUNHLEtBQU47QUFDQW5ELFlBQUFBLE9BQU8sQ0FBQ2tMLEtBQVIsQ0FBYywwQkFBMEJ2RyxNQUExQixHQUFtQyxHQUFuQyxHQUF5Q3ZCLEdBQXZEOztBQUNBWCwyQkFBTUMsbUJBQU4sQ0FBMEIsc0JBQTFCLEVBQWtELEVBQWxELEVBQXNEM0IsV0FBdEQsRUFBbUU7QUFDL0Q0QixjQUFBQSxLQUFLLEVBQUUseUJBQUcsc0JBQUgsQ0FEd0Q7QUFFL0RDLGNBQUFBLFdBQVcsRUFBRSx5QkFBRyxlQUFIO0FBRmtELGFBQW5FO0FBSUgsV0FyQ0Q7QUFzQ0g7QUFDSjtBQXhEdUQsS0FBNUQ7QUEwREgsR0FyL0IyQjs7QUF1L0I1Qjs7OztBQUlBLFFBQU15SSxxQkFBTixHQUE4QjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSTlELE9BQUo7O0FBQ0EsUUFBSSxDQUFDLEtBQUtqTSxpQkFBVixFQUE2QjtBQUN6QmlNLE1BQUFBLE9BQU8sR0FBRyxLQUFLaE0sZ0JBQUwsQ0FBc0JrSSxPQUFoQztBQUNILEtBRkQsTUFFTztBQUNIOEQsTUFBQUEsT0FBTyxHQUFHcEosT0FBTyxDQUFDQyxPQUFSLEVBQVY7QUFDSDs7QUFDRCxVQUFNbUosT0FBTjs7QUFFQSxVQUFNK0QsZ0JBQWdCLEdBQUcxQixtQkFBVTJCLE1BQVYsR0FBbUJ6QixtQkFBbkIsQ0FDckIsS0FBS2pQLEtBQUwsQ0FBV2xELE1BQVgsQ0FBa0I0UixhQURHLENBQXpCOztBQUdBLFFBQUkrQixnQkFBZ0IsQ0FBQzVFLE1BQWpCLEtBQTRCLENBQWhDLEVBQW1DO0FBQy9CLFlBQU0vQixNQUFNLEdBQUcsTUFBTSx5QkFBVztBQUM1QnRDLFFBQUFBLFFBQVEsRUFBRSxLQUFLeEgsS0FBTCxDQUFXbEQsTUFBWCxDQUFrQjRSLGFBREE7QUFFNUI7QUFDQWlDLFFBQUFBLE9BQU8sRUFBRSxDQUFDLEtBQUt2USxLQUFMLENBQVd6QixhQUhPO0FBSTVCaVMsUUFBQUEsT0FBTyxFQUFFLEtBSm1CLENBSVo7O0FBSlksT0FBWCxDQUFyQixDQUQrQixDQU8vQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFlBQU1DLGVBQWUsR0FBSXhDLEVBQUQsSUFBUTtBQUM1QixZQUNJQSxFQUFFLENBQUN5QyxPQUFILE1BQWdCLFVBQWhCLElBQ0F6QyxFQUFFLENBQUNvQixVQUFILEVBREEsSUFFQXBCLEVBQUUsQ0FBQ29CLFVBQUgsR0FBZ0IsS0FBS3pQLEtBQUwsQ0FBV2xELE1BQVgsQ0FBa0I0UixhQUFsQyxDQUhKLEVBSUU7QUFDRTlOLDJDQUFnQk4sR0FBaEIsR0FBc0J5USxLQUF0QixDQUE0QkMsSUFBNUIsQ0FBaUMsSUFBakM7O0FBQ0FwUSwyQ0FBZ0JOLEdBQWhCLEdBQXNCZ0UsY0FBdEIsQ0FDSSxhQURKLEVBQ21CdU0sZUFEbkI7QUFHSDtBQUNKLE9BWEQ7O0FBWUFqUSx1Q0FBZ0JOLEdBQWhCLEdBQXNCaUIsRUFBdEIsQ0FBeUIsYUFBekIsRUFBd0NzUCxlQUF4Qzs7QUFFQSxhQUFPL0csTUFBUDtBQUNIOztBQUNELFdBQU8sSUFBUDtBQUNILEdBNWlDMkI7O0FBOGlDNUI7OztBQUdBSSxFQUFBQSxXQUFXLEVBQUUsa0JBQWlCO0FBQzFCN0MsNkJBQWdCQyxPQUFoQixHQUEwQixLQUExQjtBQUNBLFNBQUszQixrQkFBTCxDQUF3QjtBQUFFbEgsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDUztBQUFkLEtBQXhCLEVBRjBCLENBRzFCO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLdUUsaUJBQUwsSUFBMEIsS0FBS0EsaUJBQUwsQ0FBdUI3QyxNQUFyRCxFQUE2RDtBQUN6RCxXQUFLK1MsVUFBTCxDQUNJLEtBQUtsUSxpQkFBTCxDQUF1QjdDLE1BRDNCLEVBRUksS0FBSzZDLGlCQUFMLENBQXVCM0MsTUFGM0I7QUFJQSxXQUFLMkMsaUJBQUwsR0FBeUIsSUFBekI7QUFDSCxLQU5ELE1BTU8sSUFBSUgsaUNBQWdCc1EsMkJBQWhCLEVBQUosRUFBbUQ7QUFDdER0USx1Q0FBZ0JzTix1QkFBaEIsQ0FBd0MsSUFBeEM7O0FBRUEsVUFBSSxLQUFLbE8sS0FBTCxDQUFXbEQsTUFBWCxDQUFrQjRSLGFBQWxCLElBQW1DLDJDQUFxQnlDLFVBQXJCLENBQWdDLElBQWhDLENBQXZDLEVBQThFO0FBQzFFLGNBQU1DLGVBQWUsR0FBRyxNQUFNLEtBQUtaLHFCQUFMLEVBQTlCOztBQUNBLFlBQUlZLGVBQWUsS0FBSyxJQUF4QixFQUE4QjtBQUMxQjtBQUNBO0FBQ0FyUCw4QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxZQUFBQSxNQUFNLEVBQUU7QUFBVCxXQUFiO0FBQ0g7QUFDSixPQVBELE1BT087QUFDSDtBQUNBO0FBQ0FoQyw0QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiO0FBQ0g7QUFDSixLQWZNLE1BZUE7QUFDSCxXQUFLaEIscUJBQUw7QUFDSDs7QUFFRHNPLElBQUFBLGNBQWMsQ0FBQ0MsaUJBQWY7QUFDSCxHQWhsQzJCO0FBa2xDNUJ2TyxFQUFBQSxxQkFBcUIsRUFBRSxZQUFXO0FBQzlCO0FBQ0E7QUFDQSxRQUFJLEtBQUtoQyxpQkFBTCxJQUEwQixLQUFLQSxpQkFBTCxDQUF1QjdDLE1BQXJELEVBQTZEO0FBQ3pELFdBQUsrUyxVQUFMLENBQ0ksS0FBS2xRLGlCQUFMLENBQXVCN0MsTUFEM0IsRUFFSSxLQUFLNkMsaUJBQUwsQ0FBdUIzQyxNQUYzQjtBQUlBLFdBQUsyQyxpQkFBTCxHQUF5QixJQUF6QjtBQUNILEtBTkQsTUFNTyxJQUFJOEYsWUFBWSxJQUFJQSxZQUFZLENBQUMwSyxPQUFiLENBQXFCLGlCQUFyQixDQUFwQixFQUE2RDtBQUNoRTtBQUNBLFdBQUtDLGFBQUw7QUFDSCxLQUhNLE1BR0E7QUFDSCxVQUFJNVEsaUNBQWdCTixHQUFoQixHQUFzQmdHLE9BQXRCLEVBQUosRUFBcUM7QUFDakN2RSw0QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiO0FBQ0gsT0FGRCxNQUVPLElBQUksMkJBQWUsS0FBSy9ELEtBQUwsQ0FBV2xELE1BQTFCLENBQUosRUFBdUM7QUFDMUNpRiw0QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiO0FBQ0gsT0FGTSxNQUVBO0FBQ0gsYUFBS3JELGdCQUFMLENBQXNCa0ksT0FBdEIsQ0FBOEJoRyxJQUE5QixDQUFtQyxNQUFNO0FBQ3JDYiw4QkFBSStCLFFBQUosQ0FBYTtBQUFDQyxZQUFBQSxNQUFNLEVBQUU7QUFBVCxXQUFiO0FBQ0gsU0FGRDtBQUdIO0FBQ0o7QUFDSixHQXptQzJCO0FBMm1DNUJ5TixFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QnpQLHdCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxXQURDO0FBRVQ0RCxNQUFBQSxPQUFPLEVBQUVkLFlBQVksQ0FBQzBLLE9BQWIsQ0FBcUIsaUJBQXJCO0FBRkEsS0FBYjtBQUlILEdBaG5DMkI7O0FBa25DNUI7OztBQUdBcEgsRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckIsU0FBSy9DLGVBQUwsQ0FBcUIsT0FBckI7QUFDQSxTQUFLekIsa0JBQUwsQ0FBd0I7QUFDcEJsSCxNQUFBQSxJQUFJLEVBQUUxQyxLQUFLLENBQUNHLEtBRFE7QUFFcEJrTyxNQUFBQSxLQUFLLEVBQUUsS0FGYTtBQUdwQnZMLE1BQUFBLFdBQVcsRUFBRSxLQUhPO0FBSXBCRixNQUFBQSxhQUFhLEVBQUU7QUFKSyxLQUF4QjtBQU1BLFNBQUsyRCxjQUFMLEdBQXNCLEVBQXRCOztBQUNBLFNBQUttUCxnQkFBTDs7QUFDQXBLLDZCQUFnQkMsT0FBaEIsR0FBMEIsSUFBMUI7O0FBQ0EsU0FBS3BGLGFBQUwsQ0FBbUJxRixPQUFuQjtBQUNILEdBam9DMkI7O0FBbW9DNUI7OztBQUdBTixFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QixTQUFLRyxlQUFMLENBQXFCLGFBQXJCO0FBQ0EsU0FBS3pCLGtCQUFMLENBQXdCO0FBQ3BCbEgsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDVSxXQURRO0FBRXBCMk4sTUFBQUEsS0FBSyxFQUFFLEtBRmE7QUFHcEJ2TCxNQUFBQSxXQUFXLEVBQUUsS0FITztBQUlwQkYsTUFBQUEsYUFBYSxFQUFFO0FBSkssS0FBeEI7QUFNQSxTQUFLMkQsY0FBTCxHQUFzQixFQUF0Qjs7QUFDQSxTQUFLbVAsZ0JBQUw7QUFDSCxHQWhwQzJCOztBQWtwQzVCOzs7O0FBSUFwSCxFQUFBQSxrQkFBa0IsR0FBRztBQUNqQixVQUFNcUgsSUFBSSxHQUFHLElBQWIsQ0FEaUIsQ0FHakI7QUFDQTtBQUNBOztBQUNBLFNBQUtqUixpQkFBTCxHQUF5QixLQUF6QjtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLHFCQUF4Qjs7QUFDQSxVQUFNaVIsR0FBRyxHQUFHL1EsaUNBQWdCTixHQUFoQixFQUFaLENBUmlCLENBVWpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0FxUixJQUFBQSxHQUFHLENBQUNDLDJCQUFKLENBQWdDLFVBQVM5SCxNQUFULEVBQWlCO0FBQzdDM0UsTUFBQUEsT0FBTyxDQUFDcUgsR0FBUixDQUFZLG9DQUFaLEVBQWtEMUMsTUFBbEQsRUFBMEQsV0FBMUQsRUFBdUU0SCxJQUFJLENBQUN0UixLQUFMLENBQVd6QixhQUFsRjs7QUFDQSxVQUFJbUwsTUFBTSxLQUFLNEgsSUFBSSxDQUFDdFIsS0FBTCxDQUFXekIsYUFBMUIsRUFBeUM7QUFDckM7QUFDQSxlQUFPLElBQVA7QUFDSCxPQUw0QyxDQU03QztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBSSxDQUFDK1MsSUFBSSxDQUFDRyxhQUFOLElBQXVCLENBQUNILElBQUksQ0FBQ0csYUFBTCxDQUFtQkMsS0FBL0MsRUFBc0Q7QUFDbEQsZUFBTyxJQUFQO0FBQ0g7O0FBQ0QsYUFBT0osSUFBSSxDQUFDRyxhQUFMLENBQW1CQyxLQUFuQixDQUF5QkMsc0JBQXpCLENBQWdEakksTUFBaEQsQ0FBUDtBQUNILEtBZEQ7QUFnQkE2SCxJQUFBQSxHQUFHLENBQUNwUSxFQUFKLENBQU8sTUFBUCxFQUFlLFVBQVNuQixLQUFULEVBQWdCeUUsU0FBaEIsRUFBMkJtTixJQUEzQixFQUFpQztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FqUSwwQkFBSStCLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUUsWUFBVDtBQUF1QmMsUUFBQUEsU0FBdkI7QUFBa0N6RSxRQUFBQTtBQUFsQyxPQUFiOztBQUVBLFVBQUlBLEtBQUssS0FBSyxPQUFWLElBQXFCQSxLQUFLLEtBQUssY0FBbkMsRUFBbUQ7QUFDL0MsWUFBSTRSLElBQUksQ0FBQzNCLEtBQUwsWUFBc0I0QixNQUFNLENBQUNDLGlCQUFqQyxFQUFvRDtBQUNoRHpRLFVBQUFBLFNBQVMsQ0FBQzBRLHVCQUFWLENBQWtDSCxJQUFJLENBQUMzQixLQUF2QztBQUNIOztBQUNEcUIsUUFBQUEsSUFBSSxDQUFDdk8sUUFBTCxDQUFjO0FBQUN6RCxVQUFBQSxTQUFTLEVBQUVzUyxJQUFJLENBQUMzQixLQUFMLElBQWM7QUFBMUIsU0FBZDtBQUNILE9BTEQsTUFLTyxJQUFJcUIsSUFBSSxDQUFDdFIsS0FBTCxDQUFXVixTQUFmLEVBQTBCO0FBQzdCZ1MsUUFBQUEsSUFBSSxDQUFDdk8sUUFBTCxDQUFjO0FBQUN6RCxVQUFBQSxTQUFTLEVBQUU7QUFBWixTQUFkO0FBQ0g7O0FBRURnUyxNQUFBQSxJQUFJLENBQUNVLHFCQUFMLENBQTJCaFMsS0FBM0IsRUFBa0N5RSxTQUFsQzs7QUFDQSxVQUFJekUsS0FBSyxLQUFLLFNBQVYsSUFBdUJ5RSxTQUFTLEtBQUssU0FBekMsRUFBb0Q7QUFDaEQ7QUFDSDs7QUFDRE0sTUFBQUEsT0FBTyxDQUFDa04sSUFBUixDQUFhLCtCQUFiLEVBQThDalMsS0FBOUM7O0FBQ0EsVUFBSUEsS0FBSyxLQUFLLFVBQWQsRUFBMEI7QUFBRTtBQUFTOztBQUVyQ3NSLE1BQUFBLElBQUksQ0FBQ2pSLGlCQUFMLEdBQXlCLElBQXpCO0FBQ0FpUixNQUFBQSxJQUFJLENBQUNoUixnQkFBTCxDQUFzQjZDLE9BQXRCOztBQUVBeEIsMEJBQUkrQixRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjs7QUFDQTJOLE1BQUFBLElBQUksQ0FBQ3ZPLFFBQUwsQ0FBYztBQUNWaUgsUUFBQUEsS0FBSyxFQUFFLElBREc7QUFFVnZLLFFBQUFBLG1CQUFtQixFQUFFa0ssa0JBQVNDLGlCQUFUO0FBRlgsT0FBZDtBQUlILEtBaENEO0FBaUNBMkgsSUFBQUEsR0FBRyxDQUFDcFEsRUFBSixDQUFPLGVBQVAsRUFBd0IsVUFBUytRLElBQVQsRUFBZTtBQUNuQztBQUNBO0FBQ0E7QUFDQXZRLDBCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRSxlQURDO0FBRVR1TyxRQUFBQSxJQUFJLEVBQUVBO0FBRkcsT0FBYixFQUdHLElBSEg7QUFJSCxLQVJEO0FBU0FYLElBQUFBLEdBQUcsQ0FBQ3BRLEVBQUosQ0FBTyxvQkFBUCxFQUE2QixVQUFTZ1IsTUFBVCxFQUFpQjtBQUMxQyxVQUFJOVEsU0FBUyxDQUFDK1EsWUFBVixFQUFKLEVBQThCOztBQUU5QixVQUFJRCxNQUFNLENBQUNFLFVBQVAsS0FBc0IsR0FBdEIsSUFBNkJGLE1BQU0sQ0FBQ1AsSUFBcEMsSUFBNENPLE1BQU0sQ0FBQ1AsSUFBUCxDQUFZLGFBQVosQ0FBaEQsRUFBNEU7QUFDeEU3TSxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSx1REFBYjtBQUNBM0QsUUFBQUEsU0FBUyxDQUFDaVIsVUFBVjtBQUNBO0FBQ0g7O0FBRUQsWUFBTXhNLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQXdCLHFCQUFNQyxtQkFBTixDQUEwQixZQUExQixFQUF3QyxFQUF4QyxFQUE0QzNCLFdBQTVDLEVBQXlEO0FBQ3JENEIsUUFBQUEsS0FBSyxFQUFFLHlCQUFHLFlBQUgsQ0FEOEM7QUFFckRDLFFBQUFBLFdBQVcsRUFBRSx5QkFBRyx1RUFBSDtBQUZ3QyxPQUF6RDs7QUFJQWhHLDBCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRTtBQURDLE9BQWI7QUFHSCxLQWpCRDtBQWtCQTROLElBQUFBLEdBQUcsQ0FBQ3BRLEVBQUosQ0FBTyxZQUFQLEVBQXFCLFVBQVMrTyxPQUFULEVBQWtCcUMsVUFBbEIsRUFBOEI7QUFDL0MsWUFBTXRNLGNBQWMsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQXdCLHFCQUFNQyxtQkFBTixDQUEwQixtQkFBMUIsRUFBK0MsRUFBL0MsRUFBbUR4QixjQUFuRCxFQUFtRTtBQUMvRHlCLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxzQkFBSCxDQUR3RDtBQUUvREMsUUFBQUEsV0FBVyxFQUFFLDBDQUNULDZDQUFNLHlCQUNFLDJEQUNBLHdEQUZGLEVBR0U7QUFBRTZLLFVBQUFBLGdCQUFnQixFQUFFakIsR0FBRyxDQUFDa0IsU0FBSjtBQUFwQixTQUhGLENBQU4sQ0FEUyxDQUZrRDtBQVUvRC9DLFFBQUFBLE1BQU0sRUFBRSx5QkFBRyw2QkFBSCxDQVZ1RDtBQVcvRGdELFFBQUFBLFlBQVksRUFBRSx5QkFBRyxTQUFILENBWGlEO0FBWS9EOUssUUFBQUEsVUFBVSxFQUFHK0ssU0FBRCxJQUFlO0FBQ3ZCLGNBQUlBLFNBQUosRUFBZTtBQUNYLGtCQUFNQyxHQUFHLEdBQUc5UixNQUFNLENBQUMrUixJQUFQLENBQVlOLFVBQVosRUFBd0IsUUFBeEIsQ0FBWjtBQUNBSyxZQUFBQSxHQUFHLENBQUNFLE1BQUosR0FBYSxJQUFiO0FBQ0g7QUFDSjtBQWpCOEQsT0FBbkUsRUFrQkcsSUFsQkgsRUFrQlMsSUFsQlQ7QUFtQkgsS0FyQkQ7QUF1QkEsVUFBTUMsR0FBRyxHQUFHLElBQUlDLGtEQUFKLENBQTZCLENBQUNDLEtBQUQsRUFBUUMsU0FBUixLQUFzQjtBQUMzRGxRLHlCQUFVbVEsVUFBVixDQUFxQixLQUFyQixFQUE0QixvQkFBNUIsRUFBa0RELFNBQWxELEVBQTZERCxLQUE3RDtBQUNILEtBRlcsRUFFUkMsU0FBRCxJQUFlO0FBQ2Q7QUFDQSxjQUFRQSxTQUFSO0FBQ0ksYUFBSyxtQ0FBTDtBQUNJLGlCQUFPLHlCQUFQOztBQUNKLGFBQUssMkJBQUw7QUFDSSxpQkFBTyxpQkFBUDs7QUFDSixhQUFLMU4sU0FBTDtBQUNJLGlCQUFPLGtCQUFQOztBQUNKO0FBQ0ksaUJBQU8sbUJBQVA7QUFSUjtBQVVILEtBZFcsQ0FBWixDQW5IaUIsQ0FtSWpCO0FBQ0E7QUFDQTs7QUFFQXVOLElBQUFBLEdBQUcsQ0FBQy9RLEtBQUosR0F2SWlCLENBeUlqQjs7QUFDQXVQLElBQUFBLEdBQUcsQ0FBQ3BRLEVBQUosQ0FBTyxvQkFBUCxFQUE2QixNQUFNNFIsR0FBRyxDQUFDaFAsSUFBSixFQUFuQztBQUNBd04sSUFBQUEsR0FBRyxDQUFDcFEsRUFBSixDQUFPLGlCQUFQLEVBQTBCLENBQUNpUyxDQUFELEVBQUlqTCxHQUFKLEtBQVk0SyxHQUFHLENBQUNNLGNBQUosQ0FBbUJELENBQW5CLEVBQXNCakwsR0FBdEIsQ0FBdEMsRUEzSWlCLENBNklqQjtBQUNBOztBQUNBLFVBQU1tTCxHQUFHLEdBQUcsSUFBSUMsMEJBQUosQ0FBc0JoQyxHQUF0QixDQUFaO0FBQ0FBLElBQUFBLEdBQUcsQ0FBQ3BRLEVBQUosQ0FBTyx1QkFBUCxFQUFpQ3FTLEdBQUQsSUFBUztBQUNyQ0YsTUFBQUEsR0FBRyxDQUFDRyxnQkFBSixDQUFxQkQsR0FBckI7QUFDSCxLQUZEO0FBR0FqQyxJQUFBQSxHQUFHLENBQUNwUSxFQUFKLENBQU8sbUNBQVAsRUFBNkNxUyxHQUFELElBQVM7QUFDakRGLE1BQUFBLEdBQUcsQ0FBQ0ksNEJBQUosQ0FBaUNGLEdBQWpDO0FBQ0gsS0FGRDtBQUlBakMsSUFBQUEsR0FBRyxDQUFDcFEsRUFBSixDQUFPLE1BQVAsRUFBZ0JxTCxJQUFELElBQVU7QUFDckIsVUFBSWhNLGlDQUFnQk4sR0FBaEIsR0FBc0J5VCxlQUF0QixFQUFKLEVBQTZDO0FBQ3pDLGNBQU1DLGdCQUFnQixHQUFHL1EsdUJBQWNnUixVQUFkLENBQ3JCbkosNEJBQWFvSixXQURRLEVBRXJCLDRCQUZxQixFQUdyQnRILElBQUksQ0FBQzlDLE1BSGdCO0FBSXJCO0FBQWEsWUFKUSxDQUF6Qjs7QUFNQThDLFFBQUFBLElBQUksQ0FBQ3VILDZCQUFMLENBQW1DSCxnQkFBbkM7QUFDSDtBQUNKLEtBVkQ7QUFXQXJDLElBQUFBLEdBQUcsQ0FBQ3BRLEVBQUosQ0FBTyxnQkFBUCxFQUEwQjZTLElBQUQsSUFBVTtBQUMvQixZQUFNbE8sV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBLGNBQVFnTyxJQUFSO0FBQ0ksYUFBSyxxQ0FBTDtBQUNJeE0seUJBQU1DLG1CQUFOLENBQTBCLGlCQUExQixFQUE2QyxFQUE3QyxFQUFpRDNCLFdBQWpELEVBQThEO0FBQzFENEIsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGdDQUFILENBRG1EO0FBRTFEQyxZQUFBQSxXQUFXLEVBQUUseUJBQ1QsMkRBQ0EsK0RBREEsR0FFQSxnRUFGQSxHQUdBLGlFQUhBLEdBSUEsb0VBSkEsR0FLQSxtRUFMQSxHQU1BLG1FQVBTO0FBRjZDLFdBQTlEOztBQVlBO0FBZFI7QUFnQkgsS0FsQkQ7QUFtQkE0SixJQUFBQSxHQUFHLENBQUNwUSxFQUFKLENBQU8sd0JBQVAsRUFBaUMsTUFBT2dQLE9BQVAsSUFBbUI7QUFDaEQsVUFBSThELGNBQUo7QUFDQSxVQUFJQyxjQUFKLENBRmdELENBR2hEOztBQUNBLFVBQUkxVCxpQ0FBZ0JOLEdBQWhCLEdBQXNCaVUsbUJBQXRCLEVBQUosRUFBaUQ7QUFDN0NGLFFBQUFBLGNBQWMsR0FBRyxJQUFqQjtBQUNILE9BRkQsTUFFTztBQUNIO0FBQ0EsWUFBSTtBQUNBQyxVQUFBQSxjQUFjLEdBQUcsTUFBTTFULGlDQUFnQk4sR0FBaEIsR0FBc0JrVSxtQkFBdEIsRUFBdkI7QUFDQSxjQUFJRixjQUFjLEtBQUssSUFBdkIsRUFBNkJELGNBQWMsR0FBRyxJQUFqQjtBQUNoQyxTQUhELENBR0UsT0FBT2IsQ0FBUCxFQUFVO0FBQ1JyTyxVQUFBQSxPQUFPLENBQUNrTCxLQUFSLENBQWMsMERBQWQsRUFBMEVtRCxDQUExRTtBQUNBO0FBQ0g7QUFDSjs7QUFFRCxVQUFJYSxjQUFKLEVBQW9CO0FBQ2hCek0sdUJBQU02TSx3QkFBTixDQUErQixxQkFBL0IsRUFBc0QscUJBQXRELDZFQUNXLHdFQURYLEtBRUk7QUFBRUgsVUFBQUE7QUFBRixTQUZKO0FBSUgsT0FMRCxNQUtPO0FBQ0gxTSx1QkFBTTZNLHdCQUFOLENBQStCLHlCQUEvQixFQUEwRCx5QkFBMUQsNkVBQ1csNEVBRFg7QUFHSDtBQUNKLEtBM0JEO0FBNkJBOUMsSUFBQUEsR0FBRyxDQUFDcFEsRUFBSixDQUFPLGtDQUFQLEVBQTJDLENBQUNtVCxRQUFELEVBQVdDLE1BQVgsRUFBbUJDLFlBQW5CLEtBQW9DO0FBQzNFLFlBQU1DLDhCQUE4QixHQUNoQzFPLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4Q0FBakIsQ0FESjs7QUFFQXdCLHFCQUFNQyxtQkFBTixDQUNJLGlDQURKLEVBRUksaUNBRkosRUFHSWdOLDhCQUhKLEVBSUk7QUFBRUgsUUFBQUEsUUFBRjtBQUFZQyxRQUFBQSxNQUFaO0FBQW9CQyxRQUFBQTtBQUFwQixPQUpKO0FBS0gsS0FSRDtBQVVBakQsSUFBQUEsR0FBRyxDQUFDcFEsRUFBSixDQUFPLDZCQUFQLEVBQXNDdVQsT0FBTyxJQUFJO0FBQzdDLFlBQU1DLFFBQVEsR0FBRzlSLHVCQUFjK1IsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQWpCOztBQUVBLFVBQUksQ0FBQ0QsUUFBRCxJQUFhLENBQUNELE9BQU8sQ0FBQ0csT0FBUixDQUFnQkMsUUFBbEMsRUFBNEM7QUFDeENKLFFBQUFBLE9BQU8sQ0FBQ0ssTUFBUixDQUFlO0FBQUNDLFVBQUFBLElBQUksRUFBRSxtQkFBUDtBQUE0QkMsVUFBQUEsTUFBTSxFQUFFO0FBQXBDLFNBQWY7QUFDQTtBQUNIOztBQUVELFVBQUlQLE9BQU8sQ0FBQ1EsUUFBWixFQUFzQjtBQUNsQixjQUFNQyxpQkFBaUIsR0FBR3BQLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQ0FBakIsQ0FBMUI7O0FBQ0F3Qix1QkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVEME4saUJBQXZELEVBQTBFO0FBQ3RFRCxVQUFBQSxRQUFRLEVBQUVSLE9BQU8sQ0FBQ1E7QUFEb0QsU0FBMUUsRUFFRyxJQUZIO0FBRVM7QUFBaUIsYUFGMUI7QUFFaUM7QUFBZSxZQUZoRDtBQUdILE9BTEQsTUFLTyxJQUFJUixPQUFPLENBQUNVLE9BQVosRUFBcUI7QUFDeEJDLDRCQUFXQyxjQUFYLEdBQTRCQyxpQkFBNUIsQ0FBOEM7QUFDMUNDLFVBQUFBLEdBQUcsRUFBRSxjQUFjZCxPQUFPLENBQUNHLE9BQVIsQ0FBZ0JZLGFBRE87QUFFMUMvTixVQUFBQSxLQUFLLEVBQUVnTixPQUFPLENBQUNnQixrQkFBUixHQUE2Qix5QkFBRywyQkFBSCxDQUE3QixHQUErRCx5QkFBRyxzQkFBSCxDQUY1QjtBQUcxQ0MsVUFBQUEsSUFBSSxFQUFFLGNBSG9DO0FBSTFDL1YsVUFBQUEsS0FBSyxFQUFFO0FBQUM4VSxZQUFBQTtBQUFELFdBSm1DO0FBSzFDa0IsVUFBQUEsU0FBUyxFQUFFN1AsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUwrQjtBQU0xQzZQLFVBQUFBLFFBQVEsRUFBRVIsb0JBQVdTO0FBTnFCLFNBQTlDO0FBUUg7QUFDSixLQXZCRCxFQTVOaUIsQ0FvUGpCO0FBQ0E7O0FBQ0EsVUFBTUMsV0FBVyxHQUFHbFQsdUJBQWNDLFFBQWQsQ0FBdUIsV0FBdkIsQ0FBcEI7O0FBQ0E3QixvQkFBT0MsSUFBUCxDQUFZNlUsV0FBVyxDQUFDQyxhQUF4QixFQUF1Q0QsV0FBVyxDQUFDRSxlQUFuRDtBQUNILEdBOTRDMkI7O0FBZzVDNUI7Ozs7O0FBS0EvTCxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCLFVBQU1xSCxHQUFHLEdBQUcvUSxpQ0FBZ0JOLEdBQWhCLEVBQVo7O0FBRUEsUUFBSXFSLEdBQUcsQ0FBQ29DLGVBQUosRUFBSixFQUEyQjtBQUN2QixZQUFNQyxnQkFBZ0IsR0FBRy9RLHVCQUFjZ1IsVUFBZCxDQUNyQm5KLDRCQUFhQyxNQURRLEVBRXJCLDRCQUZxQixDQUF6Qjs7QUFJQTRHLE1BQUFBLEdBQUcsQ0FBQzJFLG1DQUFKLENBQXdDdEMsZ0JBQXhDLEVBTHVCLENBT3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0FyQyxNQUFBQSxHQUFHLENBQUM0RSw4QkFBSixDQUNJLENBQUN0VCx1QkFBYytSLGdCQUFkLENBQStCLHVCQUEvQixDQURMO0FBR0g7QUFDSixHQXg2QzJCO0FBMDZDNUIvRCxFQUFBQSxVQUFVLEVBQUUsVUFBUy9TLE1BQVQsRUFBaUJFLE1BQWpCLEVBQXlCO0FBQ2pDK0csSUFBQUEsT0FBTyxDQUFDcUgsR0FBUixDQUFZLDZCQUFaLEVBQTJDcE8sTUFBM0M7O0FBQ0EsUUFBSUYsTUFBTSxJQUFJLFVBQWQsRUFBMEI7QUFDdEI2RCwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsb0JBREM7QUFFVDNGLFFBQUFBLE1BQU0sRUFBRUE7QUFGQyxPQUFiO0FBSUgsS0FMRCxNQUtPLElBQUlGLE1BQU0sSUFBSSxPQUFkLEVBQXVCO0FBQzFCNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFLGFBREMsQ0FFVDs7QUFGUyxPQUFiO0FBSUgsS0FMTSxNQUtBLElBQUk3RixNQUFNLElBQUksaUJBQWQsRUFBaUM7QUFDcEM2RCwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUseUJBREM7QUFFVDNGLFFBQUFBLE1BQU0sRUFBRUE7QUFGQyxPQUFiO0FBSUgsS0FMTSxNQUtBLElBQUlGLE1BQU0sS0FBSyxhQUFmLEVBQThCO0FBQ2pDLFVBQUkwQyxpQ0FBZ0JOLEdBQWhCLE1BQXlCTSxpQ0FBZ0JOLEdBQWhCLEdBQXNCa1csU0FBdEIsRUFBekIsSUFBOEQsQ0FBQy9VLFNBQVMsQ0FBQ0MsWUFBVixFQUFuRSxFQUE2RjtBQUN6RjtBQUNBLGFBQUs4UCxhQUFMO0FBQ0gsT0FIRCxNQUdPO0FBQ0g7QUFDQXpQLDRCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxhQURDO0FBRVQzRixVQUFBQSxNQUFNLEVBQUVBO0FBRkMsU0FBYjtBQUlIO0FBQ0osS0FYTSxNQVdBLElBQUlGLE1BQU0sSUFBSSxLQUFkLEVBQXFCO0FBQ3hCNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFO0FBREMsT0FBYjtBQUdILEtBSk0sTUFJQSxJQUFJN0YsTUFBTSxJQUFJLFVBQWQsRUFBMEI7QUFDN0I2RCwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUU7QUFEQyxPQUFiO0FBR0gsS0FKTSxNQUlBLElBQUk3RixNQUFNLElBQUksU0FBZCxFQUF5QjtBQUM1QjZELDBCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRTtBQURDLE9BQWI7QUFHSCxLQUpNLE1BSUEsSUFBSTdGLE1BQU0sSUFBSSxNQUFkLEVBQXNCO0FBQ3pCNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFO0FBREMsT0FBYjtBQUdILEtBSk0sTUFJQSxJQUFJN0YsTUFBTSxJQUFJLE9BQWQsRUFBdUI7QUFDMUIsV0FBSytTLFVBQUwsQ0FBZ0IsTUFBaEI7O0FBQ0FsUCwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUU7QUFEQyxPQUFiO0FBR0gsS0FMTSxNQUtBLElBQUk3RixNQUFNLElBQUksV0FBZCxFQUEyQjtBQUM5QjZELDBCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRTtBQURDLE9BQWI7QUFHSCxLQUpNLE1BSUEsSUFBSTdGLE1BQU0sSUFBSSxRQUFkLEVBQXdCO0FBQzNCNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFO0FBREMsT0FBYjtBQUdILEtBSk0sTUFJQSxJQUFJN0YsTUFBTSxLQUFLLG1CQUFmLEVBQW9DO0FBQ3ZDNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFO0FBREMsT0FBYjtBQUdILEtBSk0sTUFJQSxJQUFJN0YsTUFBTSxJQUFJLG1CQUFkLEVBQW1DO0FBQ3RDNkQsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFO0FBREMsT0FBYjtBQUdILEtBSk0sTUFJQSxJQUFJN0YsTUFBTSxDQUFDdVksT0FBUCxDQUFlLE9BQWYsS0FBMkIsQ0FBL0IsRUFBa0M7QUFDckM7QUFDQTtBQUNBLFlBQU03SixJQUFJLEdBQUcxTyxNQUFNLENBQUN3WSxTQUFQLENBQWlCLENBQWpCLENBQWI7QUFDQSxZQUFNQyxZQUFZLEdBQUcvSixJQUFJLENBQUM2SixPQUFMLENBQWEsR0FBYixJQUFvQixDQUF6QyxDQUpxQyxDQUlPOztBQUM1QyxVQUFJRyxXQUFXLEdBQUdoSyxJQUFJLENBQUNmLE1BQXZCLENBTHFDLENBTXJDOztBQUNBLFVBQUllLElBQUksQ0FBQzhKLFNBQUwsQ0FBZUMsWUFBZixFQUE2QkYsT0FBN0IsQ0FBcUMsR0FBckMsSUFBNEMsQ0FBQyxDQUFqRCxFQUFvRDtBQUNoREcsUUFBQUEsV0FBVyxHQUFHRCxZQUFZLEdBQUcvSixJQUFJLENBQUM4SixTQUFMLENBQWVDLFlBQWYsRUFBNkJGLE9BQTdCLENBQXFDLEdBQXJDLENBQTdCO0FBQ0g7O0FBQ0QsWUFBTUksVUFBVSxHQUFHakssSUFBSSxDQUFDOEosU0FBTCxDQUFlLENBQWYsRUFBa0JFLFdBQWxCLENBQW5CO0FBQ0EsVUFBSUUsT0FBTyxHQUFHbEssSUFBSSxDQUFDOEosU0FBTCxDQUFlRSxXQUFXLEdBQUcsQ0FBN0IsQ0FBZCxDQVhxQyxDQVdVO0FBRS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsVUFBSSxDQUFDRSxPQUFMLEVBQWNBLE9BQU8sR0FBR2xSLFNBQVYsQ0FsQnVCLENBb0JyQztBQUVBOztBQUNBLFlBQU1xRyxnQkFBZ0IsR0FBRztBQUNyQjhLLFFBQUFBLGFBQWEsRUFBRTNZLE1BQU0sQ0FBQzRZLE9BREQ7QUFFckJDLFFBQUFBLFlBQVksRUFBRTdZLE1BQU0sQ0FBQzhZO0FBRkEsT0FBekI7QUFJQSxZQUFNQyxPQUFPLEdBQUc7QUFDWnRILFFBQUFBLElBQUksRUFBRXpSLE1BQU0sQ0FBQ2daLFNBREQ7QUFFWkMsUUFBQUEsU0FBUyxFQUFFalosTUFBTSxDQUFDa1osZUFGTjtBQUdaQyxRQUFBQSxXQUFXLEVBQUVuWixNQUFNLENBQUNvWjtBQUhSLE9BQWhCLENBM0JxQyxDQWlDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFJQyxHQUFHLEdBQUcsRUFBVjs7QUFDQSxVQUFJclosTUFBTSxDQUFDcVosR0FBWCxFQUFnQjtBQUNaLFlBQUksT0FBT3JaLE1BQU0sQ0FBQ3FaLEdBQWQsS0FBdUIsUUFBM0IsRUFBcUNBLEdBQUcsR0FBRyxDQUFDclosTUFBTSxDQUFDcVosR0FBUixDQUFOLENBQXJDLEtBQ0tBLEdBQUcsR0FBR3JaLE1BQU0sQ0FBQ3FaLEdBQWI7QUFDUjs7QUFFRCxZQUFNeFIsT0FBTyxHQUFHO0FBQ1psQyxRQUFBQSxNQUFNLEVBQUUsV0FESTtBQUVaMEksUUFBQUEsUUFBUSxFQUFFcUssT0FGRTtBQUdaeEssUUFBQUEsV0FBVyxFQUFFbUwsR0FIRDtBQUlaO0FBQ0E7QUFDQTtBQUNBeEssUUFBQUEsV0FBVyxFQUFFeUssT0FBTyxDQUFDWixPQUFELENBUFI7QUFRWjVLLFFBQUFBLGtCQUFrQixFQUFFRCxnQkFSUjtBQVNaRyxRQUFBQSxRQUFRLEVBQUUrSztBQVRFLE9BQWhCOztBQVdBLFVBQUlOLFVBQVUsQ0FBQyxDQUFELENBQVYsSUFBaUIsR0FBckIsRUFBMEI7QUFDdEI1USxRQUFBQSxPQUFPLENBQUNzRyxVQUFSLEdBQXFCc0ssVUFBckI7QUFDSCxPQUZELE1BRU87QUFDSDVRLFFBQUFBLE9BQU8sQ0FBQzBCLE9BQVIsR0FBa0JrUCxVQUFsQjtBQUNIOztBQUVEOVUsMEJBQUkrQixRQUFKLENBQWFtQyxPQUFiO0FBQ0gsS0E5RE0sTUE4REEsSUFBSS9ILE1BQU0sQ0FBQ3VZLE9BQVAsQ0FBZSxPQUFmLEtBQTJCLENBQS9CLEVBQWtDO0FBQ3JDLFlBQU0vTixNQUFNLEdBQUd4SyxNQUFNLENBQUN3WSxTQUFQLENBQWlCLENBQWpCLENBQWY7O0FBQ0EzVSwwQkFBSStCLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsZ0JBREM7QUFFVDJFLFFBQUFBLE1BQU0sRUFBRUEsTUFGQztBQUdUQyxRQUFBQSxTQUFTLEVBQUV2SyxNQUFNLENBQUMyRjtBQUhULE9BQWI7QUFLSCxLQVBNLE1BT0EsSUFBSTdGLE1BQU0sQ0FBQ3VZLE9BQVAsQ0FBZSxRQUFmLEtBQTRCLENBQWhDLEVBQW1DO0FBQ3RDLFlBQU12SixPQUFPLEdBQUdoUCxNQUFNLENBQUN3WSxTQUFQLENBQWlCLENBQWpCLENBQWhCLENBRHNDLENBR3RDOztBQUVBM1UsMEJBQUkrQixRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVG9KLFFBQUFBLFFBQVEsRUFBRUQ7QUFGRCxPQUFiO0FBSUgsS0FUTSxNQVNBO0FBQ0gvSCxNQUFBQSxPQUFPLENBQUNrTixJQUFSLENBQWEsOEJBQWIsRUFBNkNuVSxNQUE3QztBQUNIO0FBQ0osR0E1akQyQjtBQThqRDVCa0osRUFBQUEsZUFBZSxFQUFFLFVBQVNsSixNQUFULEVBQWlCO0FBQzlCLFFBQUksS0FBSzhCLEtBQUwsQ0FBVzFDLFdBQWYsRUFBNEI7QUFDeEIsV0FBSzBDLEtBQUwsQ0FBVzFDLFdBQVgsQ0FBdUJZLE1BQXZCO0FBQ0g7O0FBQ0QsU0FBS3VULGdCQUFMO0FBQ0gsR0Fua0QyQjtBQXFrRDVCbFAsRUFBQUEsWUFBWSxFQUFFLFVBQVNxSSxLQUFULEVBQWdCK00sS0FBaEIsRUFBdUI7QUFDakMvTSxJQUFBQSxLQUFLLENBQUNnTixjQUFOOztBQUNBN1Ysd0JBQUkrQixRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFLFdBQVQ7QUFBc0J3SSxNQUFBQSxVQUFVLEVBQUVvTDtBQUFsQyxLQUFiO0FBQ0gsR0F4a0QyQjtBQTBrRDVCbFYsRUFBQUEsV0FBVyxFQUFFLFVBQVNtSSxLQUFULEVBQWdCbEMsTUFBaEIsRUFBd0I7QUFDakNrQyxJQUFBQSxLQUFLLENBQUNnTixjQUFOO0FBRUEsVUFBTUMsTUFBTSxHQUFHLElBQUk1RixNQUFNLENBQUM2RixVQUFYLENBQXNCLElBQXRCLEVBQTRCcFAsTUFBNUIsQ0FBZjs7QUFDQSxRQUFJLENBQUNtUCxNQUFMLEVBQWE7QUFBRTtBQUFTOztBQUN4QjlWLHdCQUFJK0IsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxXQURDO0FBRVQ4VCxNQUFBQSxNQUFNLEVBQUVBO0FBRkMsS0FBYjtBQUlILEdBbmxEMkI7QUFxbEQ1Qm5WLEVBQUFBLFlBQVksRUFBRSxVQUFTa0ksS0FBVCxFQUFnQnNDLE9BQWhCLEVBQXlCO0FBQ25DdEMsSUFBQUEsS0FBSyxDQUFDZ04sY0FBTjs7QUFDQTdWLHdCQUFJK0IsUUFBSixDQUFhO0FBQUNDLE1BQUFBLE1BQU0sRUFBRSxZQUFUO0FBQXVCb0osTUFBQUEsUUFBUSxFQUFFRDtBQUFqQyxLQUFiO0FBQ0gsR0F4bEQyQjtBQTBsRDVCNkssRUFBQUEsYUFBYSxFQUFFLFVBQVNuTixLQUFULEVBQWdCO0FBQzNCN0ksd0JBQUkrQixRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFO0FBREMsS0FBYjs7QUFHQTZHLElBQUFBLEtBQUssQ0FBQ29OLGVBQU47QUFDQXBOLElBQUFBLEtBQUssQ0FBQ2dOLGNBQU47QUFDSCxHQWhtRDJCO0FBa21ENUIzVyxFQUFBQSxZQUFZLEVBQUUsVUFBU3VTLENBQVQsRUFBWTtBQUN0QixVQUFNeUUsZ0JBQWdCLEdBQUcsSUFBekI7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBRyxJQUF6Qjs7QUFFQSxRQUFJLEtBQUtsWCxZQUFMLEdBQW9CaVgsZ0JBQXBCLElBQXdDL1csTUFBTSxDQUFDaVgsVUFBUCxJQUFxQkYsZ0JBQWpFLEVBQW1GO0FBQy9FbFcsMEJBQUkrQixRQUFKLENBQWE7QUFBRUMsUUFBQUEsTUFBTSxFQUFFO0FBQVYsT0FBYjtBQUNIOztBQUNELFFBQUksS0FBSy9DLFlBQUwsSUFBcUJrWCxnQkFBckIsSUFBeUNoWCxNQUFNLENBQUNpWCxVQUFQLEdBQW9CRCxnQkFBakUsRUFBbUY7QUFDL0VuVywwQkFBSStCLFFBQUosQ0FBYTtBQUFFQyxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiO0FBQ0g7O0FBRUQsU0FBSzNELEtBQUwsQ0FBV1QsY0FBWCxDQUEwQnlZLG1CQUExQjtBQUNBLFNBQUtwWCxZQUFMLEdBQW9CRSxNQUFNLENBQUNpWCxVQUEzQjtBQUNILEdBL21EMkI7O0FBaW5ENUIzVyxFQUFBQSx1QkFBdUIsR0FBRztBQUN0Qk8sd0JBQUkrQixRQUFKLENBQWE7QUFBRUMsTUFBQUEsTUFBTSxFQUFFO0FBQVYsS0FBYjtBQUNILEdBbm5EMkI7O0FBcW5ENUJzVSxFQUFBQSxhQUFhLEVBQUUsVUFBU3ZPLE1BQVQsRUFBaUI7QUFDNUIvSCx3QkFBSStCLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUNEQsTUFBQUEsT0FBTyxFQUFFbUM7QUFGQSxLQUFiO0FBSUgsR0ExbkQyQjtBQTRuRDVCd08sRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsU0FBS3JILFVBQUwsQ0FBZ0IsVUFBaEI7QUFDSCxHQTluRDJCO0FBZ29ENUIzQyxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQixTQUFLMkMsVUFBTCxDQUFnQixPQUFoQjtBQUNILEdBbG9EMkI7QUFvb0Q1QnNILEVBQUFBLHFCQUFxQixFQUFFLFlBQVc7QUFDOUIsU0FBS3RILFVBQUwsQ0FBZ0IsaUJBQWhCO0FBQ0gsR0F0b0QyQjtBQXdvRDVCdUgsRUFBQUEsc0JBQXNCLEVBQUUsVUFBU3hLLFdBQVQsRUFBc0J5SyxRQUF0QixFQUFnQztBQUNwRCxXQUFPLEtBQUtDLHdCQUFMLENBQThCMUssV0FBOUIsRUFBMkN5SyxRQUEzQyxDQUFQO0FBQ0gsR0Exb0QyQjtBQTRvRDVCO0FBQ0F0SyxFQUFBQSxZQUFZLEVBQUUsVUFBU0gsV0FBVCxFQUFzQjtBQUNoQyxXQUFPdk0sU0FBUyxDQUFDa1gsV0FBVixDQUFzQjNLLFdBQXRCLENBQVA7QUFDSCxHQS9vRDJCO0FBaXBENUI0SyxFQUFBQSx3QkFBd0IsRUFBRSxZQUFXO0FBQ2pDO0FBQ0EsU0FBS3pWLFFBQUwsQ0FBYztBQUNWMUUsTUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDUztBQURGLEtBQWQ7QUFHQSxTQUFLeVUsVUFBTCxDQUFnQixVQUFoQjtBQUNILEdBdnBEMkI7QUF5cEQ1QjFHLEVBQUFBLFNBQVMsRUFBRSxVQUFTc08sT0FBVCxFQUFrQkMsTUFBbEIsRUFBMEJyTyxZQUExQixFQUF3QztBQUMvQyxTQUFLdEgsUUFBTCxDQUFjO0FBQ1ZuRSxNQUFBQSxPQUFPLEVBQUU2WixPQURDO0FBRVY1WixNQUFBQSxVQUFVLEVBQUU2WixNQUZGO0FBR1Y1WixNQUFBQSxhQUFhLEVBQUUyWixPQUFPLEtBQUtDLE1BSGpCO0FBSVYzWixNQUFBQSxzQkFBc0IsRUFBRXNMLFlBSmQ7QUFLVnJMLE1BQUFBLGlCQUFpQixFQUFFO0FBTFQsS0FBZDtBQU9ILEdBanFEMkI7QUFtcUQ1QnVMLEVBQUFBLFdBQVcsRUFBRSxVQUFTYixNQUFULEVBQWlCYyxLQUFqQixFQUF3QjtBQUNqQyxVQUFNK0csR0FBRyxHQUFHL1EsaUNBQWdCTixHQUFoQixFQUFaOztBQUNBLFFBQUksQ0FBQ3FSLEdBQUwsRUFBVTtBQUNONVAsMEJBQUkrQixRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjs7QUFDQTtBQUNIOztBQUVENE4sSUFBQUEsR0FBRyxDQUFDb0gsU0FBSixDQUFjalAsTUFBZCxFQUFzQmMsS0FBSyxDQUFDa0csT0FBTixFQUF0QixFQUF1Q2xHLEtBQUssQ0FBQzZFLFVBQU4sRUFBdkMsRUFBMkQ3TSxJQUEzRCxDQUFnRSxNQUFNO0FBQ2xFYiwwQkFBSStCLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiO0FBQ0gsS0FGRCxFQUVJd0UsR0FBRCxJQUFTO0FBQ1J4RywwQkFBSStCLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiO0FBQ0gsS0FKRDtBQUtILEdBL3FEMkI7QUFpckQ1QjBOLEVBQUFBLGdCQUFnQixFQUFFLFVBQVN1SCxRQUFRLEdBQUMsRUFBbEIsRUFBc0I7QUFDcEMsUUFBSSxLQUFLNVksS0FBTCxDQUFXekIsYUFBZixFQUE4QjtBQUMxQixZQUFNa1EsTUFBTSxHQUFHak8saUNBQWdCTixHQUFoQixFQUFmOztBQUNBLFlBQU1zTSxJQUFJLEdBQUdpQyxNQUFNLElBQUlBLE1BQU0sQ0FBQ2hDLE9BQVAsQ0FBZSxLQUFLek0sS0FBTCxDQUFXekIsYUFBMUIsQ0FBdkI7O0FBQ0EsVUFBSWlPLElBQUosRUFBVTtBQUNOb00sUUFBQUEsUUFBUSxhQUFNLEtBQUsxVyxjQUFYLGdCQUFnQ3NLLElBQUksQ0FBQ2lELElBQXJDLGNBQThDbUosUUFBOUMsQ0FBUjtBQUNIO0FBQ0osS0FORCxNQU1PO0FBQ0hBLE1BQUFBLFFBQVEsYUFBTSxLQUFLMVcsY0FBWCxjQUE2QjBXLFFBQTdCLENBQVI7QUFDSDs7QUFDREMsSUFBQUEsUUFBUSxDQUFDblIsS0FBVCxhQUFvQnpILG1CQUFVQyxHQUFWLEdBQWdCNFksS0FBaEIsSUFBeUIsTUFBN0MsY0FBdURGLFFBQXZEO0FBQ0gsR0E1ckQyQjtBQThyRDVCNUcsRUFBQUEscUJBQXFCLEVBQUUsVUFBU2hTLEtBQVQsRUFBZ0J5RSxTQUFoQixFQUEyQjtBQUM5QyxVQUFNc1UsVUFBVSxHQUFHLHFDQUFvQnZZLGlDQUFnQk4sR0FBaEIsR0FBc0JzTCxRQUF0QixFQUFwQixFQUFzRHdOLEtBQXpFOztBQUVBLFFBQUlDLHFCQUFZL1ksR0FBWixFQUFKLEVBQXVCO0FBQ25CK1ksMkJBQVkvWSxHQUFaLEdBQWtCZ1osY0FBbEIsQ0FBaUNsWixLQUFLLEtBQUssT0FBM0M7O0FBQ0FpWiwyQkFBWS9ZLEdBQVosR0FBa0JpWixvQkFBbEIsQ0FBdUNKLFVBQXZDO0FBQ0g7O0FBRUQsU0FBSzdXLGNBQUwsR0FBc0IsRUFBdEI7O0FBQ0EsUUFBSWxDLEtBQUssS0FBSyxPQUFkLEVBQXVCO0FBQ25CLFdBQUtrQyxjQUFMLGVBQTJCLHlCQUFHLFNBQUgsQ0FBM0I7QUFDSDs7QUFDRCxRQUFJNlcsVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2hCLFdBQUs3VyxjQUFMLGVBQTJCNlcsVUFBM0I7QUFDSDs7QUFFRCxTQUFLMUgsZ0JBQUw7QUFDSCxHQS9zRDJCOztBQWl0RDVCK0gsRUFBQUEsa0JBQWtCLEdBQUc7QUFDakJ6WCx3QkFBSStCLFFBQUosQ0FBYTtBQUFFQyxNQUFBQSxNQUFNLEVBQUU7QUFBVixLQUFiO0FBQ0gsR0FudEQyQjs7QUFxdEQ1QjBWLEVBQUFBLG9CQUFvQixDQUFDM2MsTUFBRCxFQUFTO0FBQ3pCLFNBQUtxRyxRQUFMLENBQWM7QUFBQ2xHLE1BQUFBLFlBQVksRUFBRUg7QUFBZixLQUFkO0FBQ0gsR0F2dEQyQjs7QUF5dEQ1QjRjLEVBQUFBLG9CQUFvQixFQUFFLFVBQVN0YixNQUFULEVBQWlCO0FBQ25DLFFBQUksS0FBSzRCLEtBQUwsQ0FBV25DLDJCQUFYLENBQXVDOGIsUUFBM0MsRUFBcUQ7QUFDakR2YixNQUFBQSxNQUFNLENBQUN1YixRQUFQLEdBQWtCLEtBQUszWixLQUFMLENBQVduQywyQkFBWCxDQUF1QzhiLFFBQXpEO0FBQ0g7O0FBQ0QsV0FBTyxLQUFLM1osS0FBTCxDQUFXMUIsbUJBQVgsQ0FBK0JGLE1BQS9CLENBQVA7QUFDSCxHQTl0RDJCO0FBZ3VENUJ3YixFQUFBQSxvQkFBb0IsRUFBRSxVQUFTQyxHQUFULEVBQWM7QUFDaEMsU0FBS2hJLGFBQUwsR0FBcUJnSSxHQUFyQjtBQUNILEdBbHVEMkI7O0FBb3VENUIsUUFBTW5CLHdCQUFOLENBQStCMUssV0FBL0IsRUFBNEN5SyxRQUE1QyxFQUFzRDtBQUNsRCxTQUFLN1csZ0JBQUwsR0FBd0I2VyxRQUF4QixDQURrRCxDQUVsRDs7QUFDQSxRQUFJLEtBQUs1VyxxQkFBTCxLQUErQixJQUFuQyxFQUF5QzBDLFlBQVksQ0FBQyxLQUFLMUMscUJBQU4sQ0FBWjtBQUN6QyxTQUFLQSxxQkFBTCxHQUE2QmlZLFVBQVUsQ0FBQyxNQUFNO0FBQzFDLFdBQUtsWSxnQkFBTCxHQUF3QixJQUF4QjtBQUNBLFdBQUtDLHFCQUFMLEdBQTZCLElBQTdCO0FBQ0gsS0FIc0MsRUFHcEMsS0FBSyxDQUFMLEdBQVMsSUFIMkIsQ0FBdkMsQ0FKa0QsQ0FTbEQ7QUFDQTs7QUFDQSxVQUFNZ0IsUUFBUSxHQUFHLElBQUlTLE9BQUosQ0FBWUMsT0FBTyxJQUFJO0FBQ3BDLFlBQU13VyxnQkFBZ0IsR0FBR2hZLG9CQUFJQyxRQUFKLENBQWFpRSxPQUFPLElBQUk7QUFDN0MsWUFBSUEsT0FBTyxDQUFDbEMsTUFBUixLQUFtQixjQUF2QixFQUF1QztBQUNuQztBQUNIOztBQUNEaEMsNEJBQUltQyxVQUFKLENBQWU2VixnQkFBZjs7QUFDQXhXLFFBQUFBLE9BQU87QUFDVixPQU53QixDQUF6QjtBQU9ILEtBUmdCLENBQWpCLENBWGtELENBcUJsRDs7QUFDQSxVQUFNeVcsa0JBQWtCLEdBQUd2WSxTQUFTLENBQUNrWCxXQUFWLENBQXNCM0ssV0FBdEIsQ0FBM0I7QUFDQSxVQUFNbkwsUUFBTjs7QUFFQSxVQUFNOE8sR0FBRyxHQUFHL1EsaUNBQWdCTixHQUFoQixFQUFaLENBekJrRCxDQTBCbEQ7QUFDQTs7O0FBQ0EsUUFBSSxDQUFDLGdDQUFMLEVBQTBCO0FBQ3RCLFdBQUs0SixXQUFMO0FBQ0gsS0E5QmlELENBZ0NsRDtBQUNBOzs7QUFDQSxRQUFJK1Asa0JBQWtCLEdBQUcsS0FBekI7O0FBQ0EsUUFBSTtBQUNBQSxNQUFBQSxrQkFBa0IsR0FBRyxDQUFDLEVBQUMsTUFBTXRJLEdBQUcsQ0FBQ3VJLHdCQUFKLENBQTZCLHdCQUE3QixDQUFQLENBQXRCO0FBQ0gsS0FGRCxDQUVFLE9BQU8xRyxDQUFQLEVBQVU7QUFDUixVQUFJQSxDQUFDLENBQUNqRCxPQUFGLEtBQWMsYUFBbEIsRUFBaUM7QUFDN0JwTCxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSwwQ0FBYixFQUF5RG9PLENBQXpEO0FBQ0g7QUFDSjs7QUFFRCxRQUFJeUcsa0JBQUosRUFBd0I7QUFDcEI7QUFDQTtBQUNBaFgsNkJBQWNrWCxpQkFBZCxDQUFnQyx1QkFBaEMsRUFBeUQsSUFBekQ7O0FBQ0EsV0FBS3hVLGtCQUFMLENBQXdCO0FBQUVsSCxRQUFBQSxJQUFJLEVBQUUxQyxLQUFLLENBQUNPO0FBQWQsT0FBeEI7QUFDSCxLQUxELE1BS08sSUFDSDJHLHVCQUFjK1IsZ0JBQWQsQ0FBK0IsdUJBQS9CLE1BQ0EsTUFBTXJELEdBQUcsQ0FBQ3lJLGdDQUFKLENBQXFDLDhCQUFyQyxDQUROLENBREcsRUFHTDtBQUNFO0FBQ0E7QUFDQTtBQUNBLFdBQUt6VSxrQkFBTCxDQUF3QjtBQUFFbEgsUUFBQUEsSUFBSSxFQUFFMUMsS0FBSyxDQUFDUTtBQUFkLE9BQXhCO0FBQ0gsS0FSTSxNQVFBO0FBQ0gsV0FBSzJOLFdBQUw7QUFDSDs7QUFFRCxXQUFPOFAsa0JBQVA7QUFDSCxHQWp5RDJCOztBQW15RDVCO0FBQ0FLLEVBQUFBLGtDQUFrQyxHQUFHO0FBQ2pDLFNBQUtuUSxXQUFMO0FBQ0gsR0F0eUQyQjs7QUF3eUQ1Qm9RLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2ZuVixJQUFBQSxPQUFPLENBQUNxSCxHQUFSLENBQVkscUJBQVosRUFBbUMsS0FBS3hNLEtBQXhDOztBQUNBLFFBQUcsS0FBS0EsS0FBTCxDQUFXbkMsMkJBQWQsRUFBMkM7QUFDdkMsWUFBTTtBQUFDMGMsUUFBQUEsWUFBRDtBQUFlQyxRQUFBQSxZQUFmO0FBQTZCQyxRQUFBQSxVQUE3QjtBQUF3Q0MsUUFBQUE7QUFBeEMsVUFBc0QsS0FBSzFhLEtBQUwsQ0FBV25DLDJCQUF2RTtBQUNBZ0osTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLGNBQXJCLEVBQXFDd1QsWUFBckM7QUFDQTFULE1BQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQixXQUFyQixFQUFrQyw0QkFBbEM7QUFDQUYsTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLGlCQUFyQixFQUF3Q3lULFlBQXhDO0FBQ0EzVCxNQUFBQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUIsWUFBckIsRUFBbUMwVCxVQUFuQztBQUNBNVQsTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLHVCQUFyQixFQUE4QyxJQUE5QztBQUNBRixNQUFBQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUIsYUFBckIsRUFBb0MsS0FBcEM7QUFDQUYsTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLFlBQXJCLEVBQW1DMlQsVUFBbkM7QUFDQTdULE1BQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQixXQUFyQixFQUFrQyxtQkFBbEM7QUFDQUYsTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCLG1FQUFyQixFQUEwRixDQUExRjtBQUNBRixNQUFBQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUIsaUJBQXJCLEVBQXdDLHdDQUF4QztBQUNBRixNQUFBQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUIsZ0NBQXJCLEVBQXVELElBQXZEO0FBQ0gsS0FmYyxDQWdCZjs7O0FBRUEsUUFBSXRJLElBQUo7O0FBRUEsUUFBSSxLQUFLMkIsS0FBTCxDQUFXM0IsSUFBWCxLQUFvQjFDLEtBQUssQ0FBQ0MsT0FBOUIsRUFBdUM7QUFDbkMsWUFBTTJlLE9BQU8sR0FBR3hVLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQTNILE1BQUFBLElBQUksR0FDQTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyxPQUFELE9BREosQ0FESjtBQUtILEtBUEQsTUFPTyxJQUFJLEtBQUsyQixLQUFMLENBQVczQixJQUFYLEtBQW9CMUMsS0FBSyxDQUFDTyxpQkFBOUIsRUFBaUQ7QUFDcEQsWUFBTXNlLGdCQUFnQixHQUFHelUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtDQUFqQixDQUF6QjtBQUNBM0gsTUFBQUEsSUFBSSxHQUNBLDZCQUFDLGdCQUFEO0FBQ0ksUUFBQSxVQUFVLEVBQUUsS0FBSzRiO0FBRHJCLFFBREo7QUFLSCxLQVBNLE1BT0EsSUFBSSxLQUFLamEsS0FBTCxDQUFXM0IsSUFBWCxLQUFvQjFDLEtBQUssQ0FBQ1EsU0FBOUIsRUFBeUM7QUFDNUMsWUFBTXNlLFFBQVEsR0FBRzFVLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBakI7QUFDQTNILE1BQUFBLElBQUksR0FDQSw2QkFBQyxRQUFEO0FBQ0ksUUFBQSxVQUFVLEVBQUUsS0FBSzRiLGtDQURyQjtBQUVJLFFBQUEsZUFBZSxFQUFFLEtBQUt6WTtBQUYxQixRQURKO0FBTUgsS0FSTSxNQVFBLElBQUksS0FBS3hCLEtBQUwsQ0FBVzNCLElBQVgsS0FBb0IxQyxLQUFLLENBQUNLLGlCQUE5QixFQUFpRDtBQUNwRDtBQUNBLFlBQU0wZSxnQkFBZ0IsR0FBRzNVLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQ0FBakIsQ0FBekI7QUFDQTNILE1BQUFBLElBQUksR0FDQSw2QkFBQyxnQkFBRDtBQUNJLFFBQUEsVUFBVSxFQUFFLEtBQUttYTtBQURyQixRQURKO0FBSUgsS0FQTSxNQU9BLElBQUksS0FBS3hZLEtBQUwsQ0FBVzNCLElBQVgsS0FBb0IxQyxLQUFLLENBQUNTLFNBQTlCLEVBQXlDO0FBQzVDO0FBQ0E7QUFDQSxZQUFNdWUsWUFBWSxHQUFHLEtBQUszYSxLQUFMLENBQVdWLFNBQVgsSUFBd0IsS0FBS1UsS0FBTCxDQUFXVixTQUFYLFlBQWdDdVMsTUFBTSxDQUFDQyxpQkFBcEYsQ0FINEMsQ0FLNUM7QUFDQTtBQUNBOztBQUNBLFVBQUksS0FBSzlSLEtBQUwsQ0FBV2dLLEtBQVgsSUFBb0IsS0FBS2hLLEtBQUwsQ0FBVzFCLFNBQS9CLElBQTRDLENBQUNxYyxZQUFqRCxFQUErRDtBQUMzRDs7OztBQUlBLGNBQU1DLFlBQVksR0FBRzdVLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBckI7QUFDQTNILFFBQUFBLElBQUksR0FDQSw2QkFBQyxZQUFEO0FBQWMsVUFBQSxHQUFHLEVBQUUsS0FBS21iLG9CQUF4QjtBQUE4QyxVQUFBLFlBQVksRUFBRWhaLGlDQUFnQk4sR0FBaEIsRUFBNUQ7QUFDSSxVQUFBLGFBQWEsRUFBRSxLQUFLK1gsYUFEeEI7QUFFSSxVQUFBLGtCQUFrQixFQUFFLEtBQUttQixrQkFGN0I7QUFHSSxVQUFBLFlBQVksRUFBRSxLQUFLckwsWUFIdkI7QUFJSSxVQUFBLGFBQWEsRUFBRSxLQUFLL04sS0FBTCxDQUFXekIsYUFKOUI7QUFLSSxVQUFBLGFBQWEsRUFBRSxLQUFLeUIsS0FBTCxDQUFXZjtBQUw5QixXQU1RLEtBQUtXLEtBTmIsRUFPUSxLQUFLSSxLQVBiLEVBREo7QUFXSCxPQWpCRCxNQWlCTztBQUNIO0FBQ0EsY0FBTXVhLE9BQU8sR0FBR3hVLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxZQUFJNlUsUUFBSjs7QUFDQSxZQUFJLEtBQUs3YSxLQUFMLENBQVdWLFNBQVgsSUFBd0IsQ0FBQ3FiLFlBQTdCLEVBQTJDO0FBQ3ZDRSxVQUFBQSxRQUFRLEdBQUc7QUFBSyxZQUFBLFNBQVMsRUFBQztBQUFmLGFBQ04scUNBQW9CLEtBQUs3YSxLQUFMLENBQVdWLFNBQS9CLENBRE0sQ0FBWDtBQUdIOztBQUNEakIsUUFBQUEsSUFBSSxHQUNBO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNLd2MsUUFETCxFQUVJLDZCQUFDLE9BQUQsT0FGSixFQUdJO0FBQUcsVUFBQSxJQUFJLEVBQUMsR0FBUjtBQUFZLFVBQUEsU0FBUyxFQUFDLDZCQUF0QjtBQUFvRCxVQUFBLE9BQU8sRUFBRSxLQUFLbEQ7QUFBbEUsV0FDSyx5QkFBRyxRQUFILENBREwsQ0FISixDQURKO0FBU0g7QUFDSixLQTVDTSxNQTRDQSxJQUFJLEtBQUszWCxLQUFMLENBQVczQixJQUFYLEtBQW9CMUMsS0FBSyxDQUFDRSxPQUE5QixFQUF1QztBQUMxQyxZQUFNaWYsT0FBTyxHQUFHL1UsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGNBQWpCLENBQWhCO0FBQ0EzSCxNQUFBQSxJQUFJLEdBQUcsNkJBQUMsT0FBRCxFQUFhLEtBQUswQixtQkFBTCxFQUFiLENBQVA7QUFDSCxLQUhNLE1BR0EsSUFBSSxLQUFLQyxLQUFMLENBQVczQixJQUFYLEtBQW9CMUMsS0FBSyxDQUFDSSxRQUE5QixFQUF3QztBQUMzQyxZQUFNZ2YsWUFBWSxHQUFHaFYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUFyQjtBQUNBM0gsTUFBQUEsSUFBSSxHQUNBLDZCQUFDLFlBQUQ7QUFDSSxRQUFBLFlBQVksRUFBRSxLQUFLMkIsS0FBTCxDQUFXZCxzQkFEN0I7QUFFSSxRQUFBLFNBQVMsRUFBRSxLQUFLYyxLQUFMLENBQVdiLG1CQUYxQjtBQUdJLFFBQUEsS0FBSyxFQUFFLEtBQUthLEtBQUwsQ0FBV1osZUFIdEI7QUFJSSxRQUFBLEtBQUssRUFBRSxLQUFLUSxLQUFMLENBQVduQywyQkFBWCxDQUF1Q3FaLEtBSmxEO0FBS0ksUUFBQSxLQUFLLEVBQUUsS0FBS2xYLEtBQUwsQ0FBV2xELE1BQVgsQ0FBa0JvYyxLQUw3QjtBQU1JLFFBQUEsbUJBQW1CLEVBQUUsS0FBS1Esb0JBTjlCO0FBT0ksUUFBQSxVQUFVLEVBQUUsS0FBS2xCLHNCQVByQjtBQVFJLFFBQUEsWUFBWSxFQUFFLEtBQUtsSyxZQVJ2QjtBQVNJLFFBQUEsb0JBQW9CLEVBQUUsS0FBS21MLG9CQVQvQjtBQVVJLFFBQUEsd0JBQXdCLEVBQUUsS0FBS3paLEtBQUwsQ0FBVzNCO0FBVnpDLFNBV1EsS0FBSzhCLG1CQUFMLEVBWFIsRUFESjtBQWVILEtBakJNLE1BaUJBLElBQUksS0FBS0MsS0FBTCxDQUFXM0IsSUFBWCxLQUFvQjFDLEtBQUssQ0FBQ00sZUFBOUIsRUFBK0M7QUFDbEQsWUFBTStlLGNBQWMsR0FBR2pWLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQ0FBakIsQ0FBdkI7QUFDQTNILE1BQUFBLElBQUksR0FDQSw2QkFBQyxjQUFEO0FBQ0ksUUFBQSxVQUFVLEVBQUUsS0FBSzZQLFlBRHJCO0FBRUksUUFBQSxZQUFZLEVBQUUsS0FBS0EsWUFGdkI7QUFHSSxRQUFBLG9CQUFvQixFQUFFLEtBQUttTDtBQUgvQixTQUlRLEtBQUt0WixtQkFBTCxFQUpSLEVBREo7QUFRSCxLQVZNLE1BVUEsSUFBSSxLQUFLQyxLQUFMLENBQVczQixJQUFYLEtBQW9CMUMsS0FBSyxDQUFDRyxLQUE5QixFQUFxQztBQUN4QyxZQUFNbWYsS0FBSyxHQUFHbFYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFkO0FBQ0EzSCxNQUFBQSxJQUFJLEdBQ0EsNkJBQUMsS0FBRDtBQUNJLFFBQUEsVUFBVSxFQUFFLEtBQUtpYSx3QkFEckI7QUFFSSxRQUFBLGVBQWUsRUFBRSxLQUFLSixlQUYxQjtBQUdJLFFBQUEsYUFBYSxFQUFFLEtBQUt2WSxnQkFBTCxFQUhuQjtBQUlJLFFBQUEsd0JBQXdCLEVBQUUsS0FBS0MsS0FBTCxDQUFXM0Isd0JBSnpDO0FBS0ksUUFBQSxxQkFBcUIsRUFBRSxLQUFLa2EscUJBTGhDO0FBTUksUUFBQSxvQkFBb0IsRUFBRSxLQUFLa0Isb0JBTi9CO0FBT0ksUUFBQSwyQkFBMkIsRUFBRyxLQUFLelosS0FBTCxDQUFXbkM7QUFQN0MsU0FRUSxLQUFLc0MsbUJBQUwsRUFSUixFQURKO0FBWUgsS0FkTSxNQWNBLElBQUksS0FBS0MsS0FBTCxDQUFXM0IsSUFBWCxLQUFvQjFDLEtBQUssQ0FBQ1UsV0FBOUIsRUFBMkM7QUFDOUMsWUFBTTZlLFVBQVUsR0FBR25WLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw0QkFBakIsQ0FBbkI7QUFDQTNILE1BQUFBLElBQUksR0FDQSw2QkFBQyxVQUFEO0FBQ0ksUUFBQSxlQUFlLEVBQUUsS0FBS3VCLEtBQUwsQ0FBV3BDLGVBRGhDO0FBRUksUUFBQSxxQkFBcUIsRUFBRSxLQUFLb0MsS0FBTCxDQUFXakM7QUFGdEMsUUFESjtBQU1ILEtBUk0sTUFRQTtBQUNIb0gsTUFBQUEsT0FBTyxDQUFDa0wsS0FBUix3QkFBOEIsS0FBS2pRLEtBQUwsQ0FBVzNCLElBQXpDO0FBQ0g7O0FBRUQsVUFBTThjLGFBQWEsR0FBR3BWLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7QUFDQSxXQUFPLDZCQUFDLGFBQUQsUUFDRjNILElBREUsQ0FBUDtBQUdIO0FBajhEMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE3LTIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTksIDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyBNYXRyaXggZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0IHsgaXNDcnlwdG9BdmFpbGFibGUgfSBmcm9tICdtYXRyaXgtanMtc2RrL3NyYy9jcnlwdG8nO1xyXG5cclxuLy8gZm9jdXMtdmlzaWJsZSBpcyBhIFBvbHlmaWxsIGZvciB0aGUgOmZvY3VzLXZpc2libGUgQ1NTIHBzZXVkby1hdHRyaWJ1dGUgdXNlZCBieSBfQWNjZXNzaWJsZUJ1dHRvbi5zY3NzXHJcbmltcG9ydCAnZm9jdXMtdmlzaWJsZSc7XHJcbi8vIHdoYXQtaW5wdXQgaGVscHMgaW1wcm92ZSBrZXlib2FyZCBhY2Nlc3NpYmlsaXR5XHJcbmltcG9ydCAnd2hhdC1pbnB1dCc7XHJcblxyXG5pbXBvcnQgQW5hbHl0aWNzIGZyb20gXCIuLi8uLi9BbmFseXRpY3NcIjtcclxuaW1wb3J0IHsgRGVjcnlwdGlvbkZhaWx1cmVUcmFja2VyIH0gZnJvbSBcIi4uLy4uL0RlY3J5cHRpb25GYWlsdXJlVHJhY2tlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSBcIi4uLy4uL1BsYXRmb3JtUGVnXCI7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSBcIi4uLy4uL1Nka0NvbmZpZ1wiO1xyXG5pbXBvcnQgKiBhcyBSb29tTGlzdFNvcnRlciBmcm9tIFwiLi4vLi4vUm9vbUxpc3RTb3J0ZXJcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQgTm90aWZpZXIgZnJvbSAnLi4vLi4vTm90aWZpZXInO1xyXG5cclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi9Nb2RhbFwiO1xyXG5pbXBvcnQgVGludGVyIGZyb20gXCIuLi8uLi9UaW50ZXJcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgc2hvd1N0YXJ0Q2hhdEludml0ZURpYWxvZywgc2hvd1Jvb21JbnZpdGVEaWFsb2cgfSBmcm9tICcuLi8uLi9Sb29tSW52aXRlJztcclxuaW1wb3J0ICogYXMgUm9vbXMgZnJvbSAnLi4vLi4vUm9vbXMnO1xyXG5pbXBvcnQgbGlua2lmeU1hdHJpeCBmcm9tIFwiLi4vLi4vbGlua2lmeS1tYXRyaXhcIjtcclxuaW1wb3J0ICogYXMgTGlmZWN5Y2xlIGZyb20gJy4uLy4uL0xpZmVjeWNsZSc7XHJcbi8vIExpZmVjeWNsZVN0b3JlIGlzIG5vdCB1c2VkIGJ1dCBkb2VzIGxpc3RlbiB0byBhbmQgZGlzcGF0Y2ggYWN0aW9uc1xyXG5pbXBvcnQgJy4uLy4uL3N0b3Jlcy9MaWZlY3ljbGVTdG9yZSc7XHJcbmltcG9ydCBQYWdlVHlwZXMgZnJvbSAnLi4vLi4vUGFnZVR5cGVzJztcclxuaW1wb3J0IHsgZ2V0SG9tZVBhZ2VVcmwgfSBmcm9tICcuLi8uLi91dGlscy9wYWdlcyc7XHJcblxyXG5pbXBvcnQgY3JlYXRlUm9vbSBmcm9tIFwiLi4vLi4vY3JlYXRlUm9vbVwiO1xyXG5pbXBvcnQgS2V5UmVxdWVzdEhhbmRsZXIgZnJvbSAnLi4vLi4vS2V5UmVxdWVzdEhhbmRsZXInO1xyXG5pbXBvcnQgeyBfdCwgZ2V0Q3VycmVudExhbmd1YWdlIH0gZnJvbSAnLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUsIHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBUaGVtZUNvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL3NldHRpbmdzL2NvbnRyb2xsZXJzL1RoZW1lQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBzdGFydEFueVJlZ2lzdHJhdGlvbkZsb3cgfSBmcm9tIFwiLi4vLi4vUmVnaXN0cmF0aW9uLmpzXCI7XHJcbmltcG9ydCB7IG1lc3NhZ2VGb3JTeW5jRXJyb3IgfSBmcm9tICcuLi8uLi91dGlscy9FcnJvclV0aWxzJztcclxuaW1wb3J0IFJlc2l6ZU5vdGlmaWVyIGZyb20gXCIuLi8uLi91dGlscy9SZXNpemVOb3RpZmllclwiO1xyXG5pbXBvcnQgeyBWYWxpZGF0ZWRTZXJ2ZXJDb25maWcgfSBmcm9tIFwiLi4vLi4vdXRpbHMvQXV0b0Rpc2NvdmVyeVV0aWxzXCI7XHJcbmltcG9ydCBBdXRvRGlzY292ZXJ5VXRpbHMgZnJvbSBcIi4uLy4uL3V0aWxzL0F1dG9EaXNjb3ZlcnlVdGlsc1wiO1xyXG5pbXBvcnQgRE1Sb29tTWFwIGZyb20gJy4uLy4uL3V0aWxzL0RNUm9vbU1hcCc7XHJcbmltcG9ydCB7IGNvdW50Um9vbXNXaXRoTm90aWYgfSBmcm9tICcuLi8uLi9Sb29tTm90aWZzJztcclxuaW1wb3J0IHsgVGhlbWVXYXRjaGVyIH0gZnJvbSBcIi4uLy4uL3RoZW1lXCI7XHJcbmltcG9ydCB7IHN0b3JlUm9vbUFsaWFzSW5DYWNoZSB9IGZyb20gJy4uLy4uL1Jvb21BbGlhc0NhY2hlJztcclxuaW1wb3J0IHsgZGVmZXIgfSBmcm9tIFwiLi4vLi4vdXRpbHMvcHJvbWlzZVwiO1xyXG5pbXBvcnQgVG9hc3RTdG9yZSBmcm9tIFwiLi4vLi4vc3RvcmVzL1RvYXN0U3RvcmVcIjtcclxuaW1wb3J0ICogYXMgU3RvcmFnZU1hbmFnZXIgZnJvbSBcIi4uLy4uL3V0aWxzL1N0b3JhZ2VNYW5hZ2VyXCI7XHJcblxyXG4vKiogY29uc3RhbnRzIGZvciBNYXRyaXhDaGF0LnN0YXRlLnZpZXcgKi9cclxuZXhwb3J0IGNvbnN0IFZJRVdTID0ge1xyXG4gICAgLy8gYSBzcGVjaWFsIGluaXRpYWwgc3RhdGUgd2hpY2ggaXMgb25seSB1c2VkIGF0IHN0YXJ0dXAsIHdoaWxlIHdlIGFyZVxyXG4gICAgLy8gdHJ5aW5nIHRvIHJlLWFuaW1hdGUgYSBtYXRyaXggY2xpZW50IG9yIHJlZ2lzdGVyIGFzIGEgZ3Vlc3QuXHJcbiAgICBMT0FESU5HOiAwLFxyXG5cclxuICAgIC8vIHdlIGFyZSBzaG93aW5nIHRoZSB3ZWxjb21lIHZpZXdcclxuICAgIFdFTENPTUU6IDEsXHJcblxyXG4gICAgLy8gd2UgYXJlIHNob3dpbmcgdGhlIGxvZ2luIHZpZXdcclxuICAgIExPR0lOOiAyLFxyXG5cclxuICAgIC8vIHdlIGFyZSBzaG93aW5nIHRoZSByZWdpc3RyYXRpb24gdmlld1xyXG4gICAgUkVHSVNURVI6IDMsXHJcblxyXG4gICAgLy8gY29tcGxldGluZyB0aGUgcmVnaXN0cmF0aW9uIGZsb3dcclxuICAgIFBPU1RfUkVHSVNUUkFUSU9OOiA0LFxyXG5cclxuICAgIC8vIHNob3dpbmcgdGhlICdmb3Jnb3QgcGFzc3dvcmQnIHZpZXdcclxuICAgIEZPUkdPVF9QQVNTV09SRDogNSxcclxuXHJcbiAgICAvLyBzaG93aW5nIGZsb3cgdG8gdHJ1c3QgdGhpcyBuZXcgZGV2aWNlIHdpdGggY3Jvc3Mtc2lnbmluZ1xyXG4gICAgQ09NUExFVEVfU0VDVVJJVFk6IDYsXHJcblxyXG4gICAgLy8gZmxvdyB0byBzZXR1cCBTU1NTIC8gY3Jvc3Mtc2lnbmluZyBvbiB0aGlzIGFjY291bnRcclxuICAgIEUyRV9TRVRVUDogNyxcclxuXHJcbiAgICAvLyB3ZSBhcmUgbG9nZ2VkIGluIHdpdGggYW4gYWN0aXZlIG1hdHJpeCBjbGllbnQuXHJcbiAgICBMT0dHRURfSU46IDgsXHJcblxyXG4gICAgLy8gV2UgYXJlIGxvZ2dlZCBvdXQgKGludmFsaWQgdG9rZW4pIGJ1dCBoYXZlIG91ciBsb2NhbCBzdGF0ZSBhZ2Fpbi4gVGhlIHVzZXJcclxuICAgIC8vIHNob3VsZCBsb2cgYmFjayBpbiB0byByZWh5ZHJhdGUgdGhlIGNsaWVudC5cclxuICAgIFNPRlRfTE9HT1VUOiA5LFxyXG59O1xyXG5cclxuLy8gQWN0aW9ucyB0aGF0IGFyZSByZWRpcmVjdGVkIHRocm91Z2ggdGhlIG9uYm9hcmRpbmcgcHJvY2VzcyBwcmlvciB0byBiZWluZ1xyXG4vLyByZS1kaXNwYXRjaGVkLiBOT1RFOiBzb21lIGFjdGlvbnMgYXJlIG5vbi10cml2aWFsIGFuZCB3b3VsZCByZXF1aXJlXHJcbi8vIHJlLWZhY3RvcmluZyB0byBiZSBpbmNsdWRlZCBpbiB0aGlzIGxpc3QgaW4gZnV0dXJlLlxyXG5jb25zdCBPTkJPQVJESU5HX0ZMT1dfU1RBUlRFUlMgPSBbXHJcbiAgICAndmlld191c2VyX3NldHRpbmdzJyxcclxuICAgICd2aWV3X2NyZWF0ZV9jaGF0JyxcclxuICAgICd2aWV3X2NyZWF0ZV9yb29tJyxcclxuICAgICd2aWV3X2NyZWF0ZV9ncm91cCcsXHJcbl07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIC8vIHdlIGV4cG9ydCB0aGlzIHNvIHRoYXQgdGhlIGludGVncmF0aW9uIHRlc3RzIGNhbiB1c2UgaXQgOi1TXHJcbiAgICBzdGF0aWNzOiB7XHJcbiAgICAgICAgVklFV1M6IFZJRVdTLFxyXG4gICAgfSxcclxuXHJcbiAgICBkaXNwbGF5TmFtZTogJ01hdHJpeENoYXQnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIGNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgICAgICBzZXJ2ZXJDb25maWc6IFByb3BUeXBlcy5pbnN0YW5jZU9mKFZhbGlkYXRlZFNlcnZlckNvbmZpZyksXHJcbiAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXI6IFByb3BUeXBlcy5hbnksXHJcbiAgICAgICAgb25OZXdTY3JlZW46IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIHJlZ2lzdHJhdGlvblVybDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBlbmFibGVHdWVzdDogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIHRoZSBxdWVyeVBhcmFtcyBleHRyYWN0ZWQgZnJvbSB0aGUgW3JlYWxdIHF1ZXJ5LXN0cmluZyBvZiB0aGUgVVJJXHJcbiAgICAgICAgcmVhbFF1ZXJ5UGFyYW1zOiBQcm9wVHlwZXMub2JqZWN0LFxyXG5cclxuICAgICAgICAvLyB0aGUgaW5pdGlhbCBxdWVyeVBhcmFtcyBleHRyYWN0ZWQgZnJvbSB0aGUgaGFzaC1mcmFnbWVudCBvZiB0aGUgVVJJXHJcbiAgICAgICAgc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgICAgIHRlc3Q6ICd0ZXN0JyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGVkIHdoZW4gd2UgaGF2ZSBjb21wbGV0ZWQgYSB0b2tlbiBsb2dpblxyXG4gICAgICAgIG9uVG9rZW5Mb2dpbkNvbXBsZXRlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIFJlcHJlc2VudHMgdGhlIHNjcmVlbiB0byBkaXNwbGF5IGFzIGEgcmVzdWx0IG9mIHBhcnNpbmcgdGhlIGluaXRpYWxcclxuICAgICAgICAvLyB3aW5kb3cubG9jYXRpb25cclxuICAgICAgICBpbml0aWFsU2NyZWVuQWZ0ZXJMb2dpbjogUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgICAgICAgICAgc2NyZWVuOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgICAgIHBhcmFtczogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgICAgICB9KSxcclxuXHJcbiAgICAgICAgLy8gZGlzcGxheW5hbWUsIGlmIGFueSwgdG8gc2V0IG9uIHRoZSBkZXZpY2Ugd2hlbiBsb2dnaW5nXHJcbiAgICAgICAgLy8gaW4vcmVnaXN0ZXJpbmcuXHJcbiAgICAgICAgZGVmYXVsdERldmljZURpc3BsYXlOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyBBIGZ1bmN0aW9uIHRoYXQgbWFrZXMgYSByZWdpc3RyYXRpb24gVVJMXHJcbiAgICAgICAgbWFrZVJlZ2lzdHJhdGlvblVybDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBzID0ge1xyXG4gICAgICAgICAgICAvLyB0aGUgbWFzdGVyIHZpZXcgd2UgYXJlIHNob3dpbmcuXHJcbiAgICAgICAgICAgIHZpZXc6IFZJRVdTLkxPQURJTkcsXHJcblxyXG4gICAgICAgICAgICAvLyBXaGF0IHRoZSBMb2dnZWRJblZpZXcgd291bGQgYmUgc2hvd2luZyBpZiB2aXNpYmxlXHJcbiAgICAgICAgICAgIHBhZ2VfdHlwZTogbnVsbCxcclxuXHJcbiAgICAgICAgICAgIC8vIFRoZSBJRCBvZiB0aGUgcm9vbSB3ZSdyZSB2aWV3aW5nLiBUaGlzIGlzIGVpdGhlciBwb3B1bGF0ZWQgZGlyZWN0bHlcclxuICAgICAgICAgICAgLy8gaW4gdGhlIGNhc2Ugd2hlcmUgd2UgdmlldyBhIHJvb20gYnkgSUQgb3IgYnkgUm9vbVZpZXcgd2hlbiBpdCByZXNvbHZlc1xyXG4gICAgICAgICAgICAvLyB3aGF0IElEIGFuIGFsaWFzIHBvaW50cyBhdC5cclxuICAgICAgICAgICAgY3VycmVudFJvb21JZDogbnVsbCxcclxuXHJcbiAgICAgICAgICAgIC8vIElmIHdlJ3JlIHRyeWluZyB0byBqdXN0IHZpZXcgYSB1c2VyIElEIChpLmUuIC91c2VyIFVSTCksIHRoaXMgaXMgaXRcclxuICAgICAgICAgICAgdmlld1VzZXJJZDogbnVsbCxcclxuICAgICAgICAgICAgLy8gdGhpcyBpcyBwZXJzaXN0ZWQgYXMgbXhfbGhzX3NpemUsIGxvYWRlZCBpbiBMb2dnZWRJblZpZXdcclxuICAgICAgICAgICAgY29sbGFwc2VMaHM6IGZhbHNlLFxyXG4gICAgICAgICAgICBsZWZ0RGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBtaWRkbGVEaXNhYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIC8vIHRoZSByaWdodCBwYW5lbCdzIGRpc2FibGVkIHN0YXRlIGlzIHRyYWNrZWQgaW4gaXRzIHN0b3JlLlxyXG5cclxuICAgICAgICAgICAgdmVyc2lvbjogbnVsbCxcclxuICAgICAgICAgICAgbmV3VmVyc2lvbjogbnVsbCxcclxuICAgICAgICAgICAgaGFzTmV3VmVyc2lvbjogZmFsc2UsXHJcbiAgICAgICAgICAgIG5ld1ZlcnNpb25SZWxlYXNlTm90ZXM6IG51bGwsXHJcbiAgICAgICAgICAgIGNoZWNraW5nRm9yVXBkYXRlOiBudWxsLFxyXG5cclxuICAgICAgICAgICAgc2hvd0Nvb2tpZUJhcjogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICAvLyBQYXJhbWV0ZXJzIHVzZWQgaW4gdGhlIHJlZ2lzdHJhdGlvbiBkYW5jZSB3aXRoIHRoZSBJU1xyXG4gICAgICAgICAgICByZWdpc3Rlcl9jbGllbnRfc2VjcmV0OiBudWxsLFxyXG4gICAgICAgICAgICByZWdpc3Rlcl9zZXNzaW9uX2lkOiBudWxsLFxyXG4gICAgICAgICAgICByZWdpc3Rlcl9pZF9zaWQ6IG51bGwsXHJcblxyXG4gICAgICAgICAgICAvLyBXaGVuIHNob3dpbmcgTW9kYWwgZGlhbG9ncyB3ZSBuZWVkIHRvIHNldCBhcmlhLWhpZGRlbiBvbiB0aGUgcm9vdCBhcHAgZWxlbWVudFxyXG4gICAgICAgICAgICAvLyBhbmQgZGlzYWJsZSBpdCB3aGVuIHRoZXJlIGFyZSBubyBkaWFsb2dzXHJcbiAgICAgICAgICAgIGhpZGVUb1NSVXNlcnM6IGZhbHNlLFxyXG5cclxuICAgICAgICAgICAgc3luY0Vycm9yOiBudWxsLCAvLyBJZiB0aGUgY3VycmVudCBzeW5jaW5nIHN0YXR1cyBpcyBFUlJPUiwgdGhlIGVycm9yIG9iamVjdCwgb3RoZXJ3aXNlIG51bGwuXHJcbiAgICAgICAgICAgIHJlc2l6ZU5vdGlmaWVyOiBuZXcgUmVzaXplTm90aWZpZXIoKSxcclxuICAgICAgICAgICAgc2hvd05vdGlmaWVyVG9vbGJhcjogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gcztcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZWFsUXVlcnlQYXJhbXM6IHt9LFxyXG4gICAgICAgICAgICBzdGFydGluZ0ZyYWdtZW50UXVlcnlQYXJhbXM6IHt9LFxyXG4gICAgICAgICAgICBjb25maWc6IHt9LFxyXG4gICAgICAgICAgICBvblRva2VuTG9naW5Db21wbGV0ZWQ6ICgpID0+IHt9LFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEZhbGxiYWNrSHNVcmw6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNlcnZlckNvbmZpZyAmJiB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucHJvcHMuY29uZmlnLmZhbGxiYWNrX2hzX3VybDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFNlcnZlclByb3BlcnRpZXMoKSB7XHJcbiAgICAgICAgbGV0IHByb3BzID0gdGhpcy5zdGF0ZS5zZXJ2ZXJDb25maWc7XHJcbiAgICAgICAgaWYgKCFwcm9wcykgcHJvcHMgPSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZzsgLy8gZm9yIHVuaXQgdGVzdHNcclxuICAgICAgICBpZiAoIXByb3BzKSBwcm9wcyA9IFNka0NvbmZpZy5nZXQoKVtcInZhbGlkYXRlZF9zZXJ2ZXJfY29uZmlnXCJdO1xyXG4gICAgICAgIHJldHVybiB7c2VydmVyQ29uZmlnOiBwcm9wc307XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBNb3ZlIHRoaXMgdG8gY29uc3RydWN0b3JcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIFNka0NvbmZpZy5wdXQodGhpcy5wcm9wcy5jb25maWcpO1xyXG5cclxuICAgICAgICAvLyBVc2VkIGJ5IF92aWV3Um9vbSBiZWZvcmUgZ2V0dGluZyBzdGF0ZSBmcm9tIHN5bmNcclxuICAgICAgICB0aGlzLmZpcnN0U3luY0NvbXBsZXRlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5maXJzdFN5bmNQcm9taXNlID0gZGVmZXIoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuY29uZmlnLnN5bmNfdGltZWxpbmVfbGltaXQpIHtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLm9wdHMuaW5pdGlhbFN5bmNMaW1pdCA9IHRoaXMucHJvcHMuY29uZmlnLnN5bmNfdGltZWxpbmVfbGltaXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBhIHRoaW5nIHRvIGNhbGwgc2hvd1NjcmVlbiB3aXRoIG9uY2UgbG9naW4gY29tcGxldGVzLiAgdGhpcyBpcyBrZXB0XHJcbiAgICAgICAgLy8gb3V0c2lkZSB0aGlzLnN0YXRlIGJlY2F1c2UgdXBkYXRpbmcgaXQgc2hvdWxkIG5ldmVyIHRyaWdnZXIgYVxyXG4gICAgICAgIC8vIHJlcmVuZGVyLlxyXG4gICAgICAgIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4gPSB0aGlzLnByb3BzLmluaXRpYWxTY3JlZW5BZnRlckxvZ2luO1xyXG5cclxuICAgICAgICB0aGlzLl93aW5kb3dXaWR0aCA9IDEwMDAwO1xyXG4gICAgICAgIHRoaXMuaGFuZGxlUmVzaXplKCk7XHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuaGFuZGxlUmVzaXplKTtcclxuXHJcbiAgICAgICAgdGhpcy5fcGFnZUNoYW5naW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgIC8vIGNoZWNrIHdlIGhhdmUgdGhlIHJpZ2h0IHRpbnQgYXBwbGllZCBmb3IgdGhpcyB0aGVtZS5cclxuICAgICAgICAvLyBOLkIuIHdlIGRvbid0IGNhbGwgdGhlIHdob2xlIG9mIHNldFRoZW1lKCkgaGVyZSBhcyB3ZSBtYXkgYmVcclxuICAgICAgICAvLyByYWNpbmcgd2l0aCB0aGUgdGhlbWUgQ1NTIGRvd25sb2FkIGZpbmlzaGluZyBmcm9tIGluZGV4LmpzXHJcbiAgICAgICAgVGludGVyLnRpbnQoKTtcclxuXHJcbiAgICAgICAgLy8gRm9yIFBlcnNpc3RlbnRFbGVtZW50XHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNpemVOb3RpZmllci5vbihcIm1pZGRsZVBhbmVsUmVzaXplZFwiLCB0aGlzLl9kaXNwYXRjaFRpbWVsaW5lUmVzaXplKTtcclxuXHJcbiAgICAgICAgLy8gRm9yY2UgdXNlcnMgdG8gZ28gdGhyb3VnaCB0aGUgc29mdCBsb2dvdXQgcGFnZSBpZiB0aGV5J3JlIHNvZnQgbG9nZ2VkIG91dFxyXG4gICAgICAgIGlmIChMaWZlY3ljbGUuaXNTb2Z0TG9nb3V0KCkpIHtcclxuICAgICAgICAgICAgLy8gV2hlbiB0aGUgc2Vzc2lvbiBsb2FkcyBpdCdsbCBiZSBkZXRlY3RlZCBhcyBzb2Z0IGxvZ2dlZCBvdXQgYW5kIGEgZGlzcGF0Y2hcclxuICAgICAgICAgICAgLy8gd2lsbCBiZSBzZW50IG91dCB0byBzYXkgdGhhdCwgdHJpZ2dlcmluZyB0aGlzIE1hdHJpeENoYXQgdG8gc2hvdyB0aGUgc29mdFxyXG4gICAgICAgICAgICAvLyBsb2dvdXQgcGFnZS5cclxuICAgICAgICAgICAgTGlmZWN5Y2xlLmxvYWRTZXNzaW9uKHt9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX2FjY291bnRQYXNzd29yZCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fYWNjb3VudFBhc3N3b3JkVGltZXIgPSBudWxsO1xyXG5cclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5vbkFjdGlvbik7XHJcbiAgICAgICAgdGhpcy5fdGhlbWVXYXRjaGVyID0gbmV3IFRoZW1lV2F0Y2hlcigpO1xyXG4gICAgICAgIHRoaXMuX3RoZW1lV2F0Y2hlci5zdGFydCgpO1xyXG5cclxuICAgICAgICB0aGlzLmZvY3VzQ29tcG9zZXIgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgLy8gb2JqZWN0IGZpZWxkIHVzZWQgZm9yIHRyYWNraW5nIHRoZSBzdGF0dXMgaW5mbyBhcHBlbmRlZCB0byB0aGUgdGl0bGUgdGFnLlxyXG4gICAgICAgIC8vIHdlIGRvbid0IGRvIGl0IGFzIHJlYWN0IHN0YXRlIGFzIGknbSBzY2FyZWQgYWJvdXQgdHJpZ2dlcmluZyBuZWVkbGVzcyByZWFjdCByZWZyZXNoZXMuXHJcbiAgICAgICAgdGhpcy5zdWJUaXRsZVN0YXR1cyA9ICcnO1xyXG5cclxuICAgICAgICAvLyB0aGlzIGNhbiB0ZWNobmljYWxseSBiZSBkb25lIGFueXdoZXJlIGJ1dCBkb2luZyB0aGlzIGhlcmUga2VlcHMgYWxsXHJcbiAgICAgICAgLy8gdGhlIHJvdXRpbmcgdXJsIHBhdGggbG9naWMgdG9nZXRoZXIuXHJcbiAgICAgICAgaWYgKHRoaXMub25BbGlhc0NsaWNrKSB7XHJcbiAgICAgICAgICAgIGxpbmtpZnlNYXRyaXgub25BbGlhc0NsaWNrID0gdGhpcy5vbkFsaWFzQ2xpY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLm9uVXNlckNsaWNrKSB7XHJcbiAgICAgICAgICAgIGxpbmtpZnlNYXRyaXgub25Vc2VyQ2xpY2sgPSB0aGlzLm9uVXNlckNsaWNrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5vbkdyb3VwQ2xpY2spIHtcclxuICAgICAgICAgICAgbGlua2lmeU1hdHJpeC5vbkdyb3VwQ2xpY2sgPSB0aGlzLm9uR3JvdXBDbGljaztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoZSBmaXJzdCB0aGluZyB0byBkbyBpcyB0byB0cnkgdGhlIHRva2VuIHBhcmFtcyBpbiB0aGUgcXVlcnktc3RyaW5nXHJcbiAgICAgICAgLy8gaWYgdGhlIHNlc3Npb24gaXNuJ3Qgc29mdCBsb2dnZWQgb3V0IChpZTogaXMgYSBjbGVhbiBzZXNzaW9uIGJlaW5nIGxvZ2dlZCBpbilcclxuICAgICAgICBpZiAoIUxpZmVjeWNsZS5pc1NvZnRMb2dvdXQoKSkge1xyXG4gICAgICAgICAgICBMaWZlY3ljbGUuYXR0ZW1wdFRva2VuTG9naW4oXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnJlYWxRdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZGVmYXVsdERldmljZURpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICApLnRoZW4oKGxvZ2dlZEluKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAobG9nZ2VkSW4pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uVG9rZW5Mb2dpbkNvbXBsZXRlZCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBkb24ndCBkbyBhbnl0aGluZyBlbHNlIHVudGlsIHRoZSBwYWdlIHJlbG9hZHMgLSBqdXN0IHN0YXkgaW5cclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGUgJ2xvYWRpbmcnIHN0YXRlLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBpZiB0aGUgdXNlciBoYXMgZm9sbG93ZWQgYSBsb2dpbiBvciByZWdpc3RlciBsaW5rLCBkb24ndCByZWFuaW1hdGVcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBvbGQgY3JlZHMsIGJ1dCByYXRoZXIgZ28gc3RyYWlnaHQgdG8gdGhlIHJlbGV2YW50IHBhZ2VcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpcnN0U2NyZWVuID0gdGhpcy5fc2NyZWVuQWZ0ZXJMb2dpbiA/XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2NyZWVuQWZ0ZXJMb2dpbi5zY3JlZW4gOiBudWxsO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChmaXJzdFNjcmVlbiA9PT0gJ2xvZ2luJyB8fFxyXG4gICAgICAgICAgICAgICAgICAgIGZpcnN0U2NyZWVuID09PSAncmVnaXN0ZXInIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgZmlyc3RTY3JlZW4gPT09ICdmb3Jnb3RfcGFzc3dvcmQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2hvd1NjcmVlbkFmdGVyTG9naW4oKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2xvYWRTZXNzaW9uKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJzaG93Q29va2llQmFyXCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc2hvd0Nvb2tpZUJhcjogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImFuYWx5dGljc09wdEluXCIpKSB7XHJcbiAgICAgICAgICAgIEFuYWx5dGljcy5lbmFibGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9sb2FkU2Vzc2lvbjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gdGhlIGV4dHJhIFByb21pc2UucmVzb2x2ZSgpIGVuc3VyZXMgdGhhdCBzeW5jaHJvbm91cyBleGNlcHRpb25zIGhpdCB0aGUgc2FtZSBjb2RlcGF0aCBhc1xyXG4gICAgICAgIC8vIGFzeW5jaHJvbm91cyBvbmVzLlxyXG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIExpZmVjeWNsZS5sb2FkU2Vzc2lvbih7XHJcbiAgICAgICAgICAgICAgICBmcmFnbWVudFF1ZXJ5UGFyYW1zOiB0aGlzLnByb3BzLnN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgIGVuYWJsZUd1ZXN0OiB0aGlzLnByb3BzLmVuYWJsZUd1ZXN0LFxyXG4gICAgICAgICAgICAgICAgZ3Vlc3RIc1VybDogdGhpcy5nZXRTZXJ2ZXJQcm9wZXJ0aWVzKCkuc2VydmVyQ29uZmlnLmhzVXJsLFxyXG4gICAgICAgICAgICAgICAgZ3Vlc3RJc1VybDogdGhpcy5nZXRTZXJ2ZXJQcm9wZXJ0aWVzKCkuc2VydmVyQ29uZmlnLmlzVXJsLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdERldmljZURpc3BsYXlOYW1lOiB0aGlzLnByb3BzLmRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSkudGhlbigobG9hZGVkU2Vzc2lvbikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIWxvYWRlZFNlc3Npb24pIHtcclxuICAgICAgICAgICAgICAgIC8vIGZhbGwgYmFjayB0byBzaG93aW5nIHRoZSB3ZWxjb21lIHNjcmVlblxyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246IFwidmlld193ZWxjb21lX3BhZ2VcIn0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gTm90ZSB3ZSBkb24ndCBjYXRjaCBlcnJvcnMgZnJvbSB0aGlzOiB3ZSBjYXRjaCBldmVyeXRoaW5nIHdpdGhpblxyXG4gICAgICAgIC8vIGxvYWRTZXNzaW9uIGFzIHRoZXJlJ3MgbG9naWMgdGhlcmUgdG8gYXNrIHRoZSB1c2VyIGlmIHRoZXkgd2FudFxyXG4gICAgICAgIC8vIHRvIHRyeSBsb2dnaW5nIG91dC5cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIExpZmVjeWNsZS5zdG9wTWF0cml4Q2xpZW50KCk7XHJcbiAgICAgICAgZGlzLnVucmVnaXN0ZXIodGhpcy5kaXNwYXRjaGVyUmVmKTtcclxuICAgICAgICB0aGlzLl90aGVtZVdhdGNoZXIuc3RvcCgpO1xyXG4gICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgdGhpcy5vbkZvY3VzKTtcclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5oYW5kbGVSZXNpemUpO1xyXG4gICAgICAgIHRoaXMuc3RhdGUucmVzaXplTm90aWZpZXIucmVtb3ZlTGlzdGVuZXIoXCJtaWRkbGVQYW5lbFJlc2l6ZWRcIiwgdGhpcy5fZGlzcGF0Y2hUaW1lbGluZVJlc2l6ZSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9hY2NvdW50UGFzc3dvcmRUaW1lciAhPT0gbnVsbCkgY2xlYXJUaW1lb3V0KHRoaXMuX2FjY291bnRQYXNzd29yZFRpbWVyKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2Ugd2l0aCBhcHByb3ByaWF0ZSBsaWZlY3ljbGUgc3RhZ2VcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsVXBkYXRlOiBmdW5jdGlvbihwcm9wcywgc3RhdGUpIHtcclxuICAgICAgICBpZiAodGhpcy5zaG91bGRUcmFja1BhZ2VDaGFuZ2UodGhpcy5zdGF0ZSwgc3RhdGUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhcnRQYWdlQ2hhbmdlVGltZXIoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24ocHJldlByb3BzLCBwcmV2U3RhdGUpIHtcclxuICAgICAgICBpZiAodGhpcy5zaG91bGRUcmFja1BhZ2VDaGFuZ2UocHJldlN0YXRlLCB0aGlzLnN0YXRlKSkge1xyXG4gICAgICAgICAgICBjb25zdCBkdXJhdGlvbk1zID0gdGhpcy5zdG9wUGFnZUNoYW5nZVRpbWVyKCk7XHJcbiAgICAgICAgICAgIEFuYWx5dGljcy50cmFja1BhZ2VDaGFuZ2UoZHVyYXRpb25Ncyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmZvY3VzQ29tcG9zZXIpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdmb2N1c19jb21wb3Nlcid9KTtcclxuICAgICAgICAgICAgdGhpcy5mb2N1c0NvbXBvc2VyID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBzdGFydFBhZ2VDaGFuZ2VUaW1lcigpIHtcclxuICAgICAgICAvLyBUb3IgZG9lc24ndCBzdXBwb3J0IHBlcmZvcm1hbmNlXHJcbiAgICAgICAgaWYgKCFwZXJmb3JtYW5jZSB8fCAhcGVyZm9ybWFuY2UubWFyaykgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIC8vIFRoaXMgc2hvdWxkbid0IGhhcHBlbiBiZWNhdXNlIFVOU0FGRV9jb21wb25lbnRXaWxsVXBkYXRlIGFuZCBjb21wb25lbnREaWRVcGRhdGVcclxuICAgICAgICAvLyBhcmUgdXNlZC5cclxuICAgICAgICBpZiAodGhpcy5fcGFnZUNoYW5naW5nKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTWF0cml4Q2hhdC5zdGFydFBhZ2VDaGFuZ2VUaW1lcjogdGltZXIgYWxyZWFkeSBzdGFydGVkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fcGFnZUNoYW5naW5nID0gdHJ1ZTtcclxuICAgICAgICBwZXJmb3JtYW5jZS5tYXJrKCdyaW90X01hdHJpeENoYXRfcGFnZV9jaGFuZ2Vfc3RhcnQnKTtcclxuICAgIH0sXHJcblxyXG4gICAgc3RvcFBhZ2VDaGFuZ2VUaW1lcigpIHtcclxuICAgICAgICAvLyBUb3IgZG9lc24ndCBzdXBwb3J0IHBlcmZvcm1hbmNlXHJcbiAgICAgICAgaWYgKCFwZXJmb3JtYW5jZSB8fCAhcGVyZm9ybWFuY2UubWFyaykgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5fcGFnZUNoYW5naW5nKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTWF0cml4Q2hhdC5zdG9wUGFnZUNoYW5nZVRpbWVyOiB0aW1lciBub3Qgc3RhcnRlZCcpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3BhZ2VDaGFuZ2luZyA9IGZhbHNlO1xyXG4gICAgICAgIHBlcmZvcm1hbmNlLm1hcmsoJ3Jpb3RfTWF0cml4Q2hhdF9wYWdlX2NoYW5nZV9zdG9wJyk7XHJcbiAgICAgICAgcGVyZm9ybWFuY2UubWVhc3VyZShcclxuICAgICAgICAgICAgJ3Jpb3RfTWF0cml4Q2hhdF9wYWdlX2NoYW5nZV9kZWx0YScsXHJcbiAgICAgICAgICAgICdyaW90X01hdHJpeENoYXRfcGFnZV9jaGFuZ2Vfc3RhcnQnLFxyXG4gICAgICAgICAgICAncmlvdF9NYXRyaXhDaGF0X3BhZ2VfY2hhbmdlX3N0b3AnLFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgcGVyZm9ybWFuY2UuY2xlYXJNYXJrcygncmlvdF9NYXRyaXhDaGF0X3BhZ2VfY2hhbmdlX3N0YXJ0Jyk7XHJcbiAgICAgICAgcGVyZm9ybWFuY2UuY2xlYXJNYXJrcygncmlvdF9NYXRyaXhDaGF0X3BhZ2VfY2hhbmdlX3N0b3AnKTtcclxuICAgICAgICBjb25zdCBtZWFzdXJlbWVudCA9IHBlcmZvcm1hbmNlLmdldEVudHJpZXNCeU5hbWUoJ3Jpb3RfTWF0cml4Q2hhdF9wYWdlX2NoYW5nZV9kZWx0YScpLnBvcCgpO1xyXG5cclxuICAgICAgICAvLyBJbiBwcmFjdGljZSwgc29tZXRpbWVzIHRoZSBlbnRyaWVzIGxpc3QgaXMgZW1wdHksIHNvIHdlIGdldCBubyBtZWFzdXJlbWVudFxyXG4gICAgICAgIGlmICghbWVhc3VyZW1lbnQpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICByZXR1cm4gbWVhc3VyZW1lbnQuZHVyYXRpb247XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3VsZFRyYWNrUGFnZUNoYW5nZShwcmV2U3RhdGUsIHN0YXRlKSB7XHJcbiAgICAgICAgcmV0dXJuIHByZXZTdGF0ZS5jdXJyZW50Um9vbUlkICE9PSBzdGF0ZS5jdXJyZW50Um9vbUlkIHx8XHJcbiAgICAgICAgICAgIHByZXZTdGF0ZS52aWV3ICE9PSBzdGF0ZS52aWV3IHx8XHJcbiAgICAgICAgICAgIHByZXZTdGF0ZS5wYWdlX3R5cGUgIT09IHN0YXRlLnBhZ2VfdHlwZTtcclxuICAgIH0sXHJcblxyXG4gICAgc2V0U3RhdGVGb3JOZXdWaWV3OiBmdW5jdGlvbihzdGF0ZSkge1xyXG4gICAgICAgIGlmIChzdGF0ZS52aWV3ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwic2V0U3RhdGVGb3JOZXdWaWV3IHdpdGggbm8gdmlldyFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xyXG4gICAgICAgICAgICB2aWV3VXNlcklkOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgT2JqZWN0LmFzc2lnbihuZXdTdGF0ZSwgc3RhdGUpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkFjdGlvbjogZnVuY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGBNYXRyaXhDbGllbnRQZWcub25BY3Rpb246ICR7cGF5bG9hZC5hY3Rpb259YCk7XHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG5cclxuICAgICAgICAvLyBTdGFydCB0aGUgb25ib2FyZGluZyBwcm9jZXNzIGZvciBjZXJ0YWluIGFjdGlvbnNcclxuICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpICYmIE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkgJiZcclxuICAgICAgICAgICAgT05CT0FSRElOR19GTE9XX1NUQVJURVJTLmluY2x1ZGVzKHBheWxvYWQuYWN0aW9uKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICAvLyBUaGlzIHdpbGwgY2F1c2UgYHBheWxvYWRgIHRvIGJlIGRpc3BhdGNoZWQgbGF0ZXIsIG9uY2UgYVxyXG4gICAgICAgICAgICAvLyBzeW5jIGhhcyByZWFjaGVkIHRoZSBcInByZXBhcmVkXCIgc3RhdGUuIFNldHRpbmcgYSBtYXRyaXggSURcclxuICAgICAgICAgICAgLy8gd2lsbCBjYXVzZSBhIGZ1bGwgbG9naW4gYW5kIHN5bmMgYW5kIGZpbmFsbHkgdGhlIGRlZmVycmVkXHJcbiAgICAgICAgICAgIC8vIGFjdGlvbiB3aWxsIGJlIGRpc3BhdGNoZWQuXHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdkb19hZnRlcl9zeW5jX3ByZXBhcmVkJyxcclxuICAgICAgICAgICAgICAgIGRlZmVycmVkX2FjdGlvbjogcGF5bG9hZCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAncmVxdWlyZV9yZWdpc3RyYXRpb24nfSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgY2FzZSAnTWF0cml4QWN0aW9ucy5hY2NvdW50RGF0YSc6XHJcbiAgICAgICAgICAgICAgICAvLyBYWFg6IFRoaXMgaXMgYSBjb2xsZWN0aW9uIG9mIHNldmVyYWwgaGFja3MgdG8gc29sdmUgYSBtaW5vciBwcm9ibGVtLiBXZSB3YW50IHRvXHJcbiAgICAgICAgICAgICAgICAvLyB1cGRhdGUgb3VyIGxvY2FsIHN0YXRlIHdoZW4gdGhlIElEIHNlcnZlciBjaGFuZ2VzLCBidXQgZG9uJ3Qgd2FudCB0byBwdXQgdGhhdCBpblxyXG4gICAgICAgICAgICAgICAgLy8gdGhlIGpzLXNkayBhcyB3ZSdkIGJlIHRoZW4gZGljdGF0aW5nIGhvdyBhbGwgY29uc3VtZXJzIG5lZWQgdG8gYmVoYXZlLiBIb3dldmVyLFxyXG4gICAgICAgICAgICAgICAgLy8gdGhpcyBjb21wb25lbnQgaXMgYWxyZWFkeSBibG9hdGVkIGFuZCB3ZSBwcm9iYWJseSBkb24ndCB3YW50IHRoaXMgdGlueSBsb2dpYyBpblxyXG4gICAgICAgICAgICAgICAgLy8gaGVyZSwgYnV0IHRoZXJlJ3Mgbm8gYmV0dGVyIHBsYWNlIGluIHRoZSByZWFjdC1zZGsgZm9yIGl0LiBBZGRpdGlvbmFsbHksIHdlJ3JlXHJcbiAgICAgICAgICAgICAgICAvLyBhYnVzaW5nIHRoZSBNYXRyaXhBY3Rpb25DcmVhdG9yIHN0dWZmIHRvIGF2b2lkIGVycm9ycyBvbiBkaXNwYXRjaGVzLlxyXG4gICAgICAgICAgICAgICAgaWYgKHBheWxvYWQuZXZlbnRfdHlwZSA9PT0gJ20uaWRlbnRpdHlfc2VydmVyJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGZ1bGxVcmwgPSBwYXlsb2FkLmV2ZW50X2NvbnRlbnQgPyBwYXlsb2FkLmV2ZW50X2NvbnRlbnRbJ2Jhc2VfdXJsJ10gOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZnVsbFVybCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0SWRlbnRpdHlTZXJ2ZXJVcmwobnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwibXhfaXNfYWNjZXNzX3Rva2VuXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcIm14X2lzX3VybFwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0SWRlbnRpdHlTZXJ2ZXJVcmwoZnVsbFVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwibXhfaXNfYWNjZXNzX3Rva2VuXCIpOyAvLyBjbGVhciB0b2tlblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm14X2lzX3VybFwiLCBmdWxsVXJsKTsgLy8gWFhYOiBEbyB3ZSBzdGlsbCBuZWVkIHRoaXM/XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyByZWRpc3BhdGNoIHRoZSBjaGFuZ2Ugd2l0aCBhIG1vcmUgc3BlY2lmaWMgYWN0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdpZF9zZXJ2ZXJfY2hhbmdlZCd9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdsb2dvdXQnOlxyXG4gICAgICAgICAgICAgICAgTGlmZWN5Y2xlLmxvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJzpcclxuICAgICAgICAgICAgICAgIHN0YXJ0QW55UmVnaXN0cmF0aW9uRmxvdyhwYXlsb2FkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdzdGFydF9yZWdpc3RyYXRpb24nOlxyXG4gICAgICAgICAgICAgICAgaWYgKExpZmVjeWNsZS5pc1NvZnRMb2dvdXQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX29uU29mdExvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBzdGFydHMgdGhlIGZ1bGwgcmVnaXN0cmF0aW9uIGZsb3dcclxuICAgICAgICAgICAgICAgIGlmIChwYXlsb2FkLnNjcmVlbkFmdGVyTG9naW4pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zY3JlZW5BZnRlckxvZ2luID0gcGF5bG9hZC5zY3JlZW5BZnRlckxvZ2luO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc3RhcnRSZWdpc3RyYXRpb24ocGF5bG9hZC5wYXJhbXMgfHwge30pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3N0YXJ0X2xvZ2luJzpcclxuICAgICAgICAgICAgICAgIGlmIChMaWZlY3ljbGUuaXNTb2Z0TG9nb3V0KCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9vblNvZnRMb2dvdXQoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChwYXlsb2FkLnNjcmVlbkFmdGVyTG9naW4pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zY3JlZW5BZnRlckxvZ2luID0gcGF5bG9hZC5zY3JlZW5BZnRlckxvZ2luO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZUZvck5ld1ZpZXcoe1xyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6IFZJRVdTLkxPR0lOLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbignbG9naW4nKTtcclxuICAgICAgICAgICAgICAgIFRoZW1lQ29udHJvbGxlci5pc0xvZ2luID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lV2F0Y2hlci5yZWNoZWNrKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnc3RhcnRfcG9zdF9yZWdpc3RyYXRpb24nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgdmlldzogVklFV1MuUE9TVF9SRUdJU1RSQVRJT04sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdzdGFydF9wYXNzd29yZF9yZWNvdmVyeSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlRm9yTmV3Vmlldyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdmlldzogVklFV1MuRk9SR09UX1BBU1NXT1JELFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbignZm9yZ290X3Bhc3N3b3JkJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnc3RhcnRfY2hhdCc6XHJcbiAgICAgICAgICAgICAgICBjcmVhdGVSb29tKHtcclxuICAgICAgICAgICAgICAgICAgICBkbVVzZXJJZDogcGF5bG9hZC51c2VyX2lkLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnbGVhdmVfcm9vbSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9sZWF2ZVJvb20ocGF5bG9hZC5yb29tX2lkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdyZWplY3RfaW52aXRlJzpcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlamVjdCBpbnZpdGF0aW9uJywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdSZWplY3QgaW52aXRhdGlvbicpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdCgnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIHJlamVjdCB0aGUgaW52aXRhdGlvbj8nKSxcclxuICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiAoY29uZmlybSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY29uZmlybSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRklYTUU6IGNvbnRyb2xsZXIgc2hvdWxkbid0IGJlIGxvYWRpbmcgYSB2aWV3IDooXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBMb2FkZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG1vZGFsID0gTW9kYWwuY3JlYXRlRGlhbG9nKExvYWRlciwgbnVsbCwgJ214X0RpYWxvZ19zcGlubmVyJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmxlYXZlKHBheWxvYWQucm9vbV9pZCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWwuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50Um9vbUlkID09PSBwYXlsb2FkLnJvb21faWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd2aWV3X25leHRfcm9vbSd9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWwuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gcmVqZWN0IGludml0YXRpb24nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gcmVqZWN0IGludml0YXRpb24nKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGVyci50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndmlld191c2VyX2luZm8nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmlld1VzZXIocGF5bG9hZC51c2VySWQsIHBheWxvYWQuc3ViQWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3Jvb20nOiB7XHJcbiAgICAgICAgICAgICAgICAvLyBUYWtlcyBlaXRoZXIgYSByb29tIElEIG9yIHJvb20gYWxpYXM6IGlmIHN3aXRjaGluZyB0byBhIHJvb20gdGhlIGNsaWVudCBpcyBhbHJlYWR5XHJcbiAgICAgICAgICAgICAgICAvLyBrbm93biB0byBiZSBpbiAoZWcuIHVzZXIgY2xpY2tzIG9uIGEgcm9vbSBpbiB0aGUgcmVjZW50cyBwYW5lbCksIHN1cHBseSB0aGUgSURcclxuICAgICAgICAgICAgICAgIC8vIElmIHRoZSB1c2VyIGlzIGNsaWNraW5nIG9uIGEgcm9vbSBpbiB0aGUgY29udGV4dCBvZiB0aGUgYWxpYXMgYmVpbmcgcHJlc2VudGVkXHJcbiAgICAgICAgICAgICAgICAvLyB0byB0aGVtLCBzdXBwbHkgdGhlIHJvb20gYWxpYXMuIElmIGJvdGggYXJlIHN1cHBsaWVkLCB0aGUgcm9vbSBJRCB3aWxsIGJlIGlnbm9yZWQuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5fdmlld1Jvb20ocGF5bG9hZCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocGF5bG9hZC5kZWZlcnJlZF9hY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm9taXNlLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2gocGF5bG9hZC5kZWZlcnJlZF9hY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAndmlld19wcmV2X3Jvb20nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmlld05leHRSb29tKC0xKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X25leHRfcm9vbSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92aWV3TmV4dFJvb20oMSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndmlld19pbmRleGVkX3Jvb20nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmlld0luZGV4ZWRSb29tKHBheWxvYWQucm9vbUluZGV4KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3VzZXJfc2V0dGluZ3MnOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBVc2VyU2V0dGluZ3NEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5Vc2VyU2V0dGluZ3NEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdVc2VyIHNldHRpbmdzJywgJycsIFVzZXJTZXR0aW5nc0RpYWxvZywge30sXHJcbiAgICAgICAgICAgICAgICAgICAgLypjbGFzc05hbWU9Ki9udWxsLCAvKmlzUHJpb3JpdHk9Ki9mYWxzZSwgLyppc1N0YXRpYz0qL3RydWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFZpZXcgdGhlIHdlbGNvbWUgb3IgaG9tZSBwYWdlIGlmIHdlIG5lZWQgc29tZXRoaW5nIHRvIGxvb2sgYXRcclxuICAgICAgICAgICAgICAgIHRoaXMuX3ZpZXdTb21ldGhpbmdCZWhpbmRNb2RhbCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAndmlld19jcmVhdGVfcm9vbSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jcmVhdGVSb29tKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndmlld19jcmVhdGVfZ3JvdXAnOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBDcmVhdGVHcm91cERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkNyZWF0ZUdyb3VwRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnQ3JlYXRlIENvbW11bml0eScsICcnLCBDcmVhdGVHcm91cERpYWxvZyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfcm9vbV9kaXJlY3RvcnknOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBSb29tRGlyZWN0b3J5ID0gc2RrLmdldENvbXBvbmVudChcInN0cnVjdHVyZXMuUm9vbURpcmVjdG9yeVwiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1Jvb20gZGlyZWN0b3J5JywgJycsIFJvb21EaXJlY3RvcnksIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICdteF9Sb29tRGlyZWN0b3J5X2RpYWxvZ1dyYXBwZXInLCBmYWxzZSwgdHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gVmlldyB0aGUgd2VsY29tZSBvciBob21lIHBhZ2UgaWYgd2UgbmVlZCBzb21ldGhpbmcgdG8gbG9vayBhdFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmlld1NvbWV0aGluZ0JlaGluZE1vZGFsKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfbXlfZ3JvdXBzJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFBhZ2UoUGFnZVR5cGVzLk15R3JvdXBzKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZ5TmV3U2NyZWVuKCdncm91cHMnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X2dyb3VwJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3ZpZXdHcm91cChwYXlsb2FkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3dlbGNvbWVfcGFnZSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92aWV3V2VsY29tZSgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfaG9tZV9wYWdlJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3ZpZXdIb21lKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndmlld19zZXRfbXhpZCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXRNeElkKHBheWxvYWQpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfc3RhcnRfY2hhdF9vcl9yZXVzZSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jaGF0Q3JlYXRlT3JSZXVzZShwYXlsb2FkLnVzZXJfaWQpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfY3JlYXRlX2NoYXQnOlxyXG4gICAgICAgICAgICAgICAgc2hvd1N0YXJ0Q2hhdEludml0ZURpYWxvZygpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfaW52aXRlJzpcclxuICAgICAgICAgICAgICAgIHNob3dSb29tSW52aXRlRGlhbG9nKHBheWxvYWQucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X2xhc3Rfc2NyZWVuJzpcclxuICAgICAgICAgICAgICAgIC8vIFRoaXMgZnVuY3Rpb24gZG9lcyB3aGF0IHdlIHdhbnQsIGRlc3BpdGUgdGhlIG5hbWUuIFRoZSBpZGVhIGlzIHRoYXQgaXQgc2hvd3NcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBsYXN0IHJvb20gd2Ugd2VyZSBsb29raW5nIGF0IG9yIHNvbWUgcmVhc29uYWJsZSBkZWZhdWx0L2d1ZXNzLiBXZSBkb24ndFxyXG4gICAgICAgICAgICAgICAgLy8gaGF2ZSB0byB3b3JyeSBhYm91dCBlbWFpbCBpbnZpdGVzIG9yIHNpbWlsYXIgYmVpbmcgcmUtdHJpZ2dlcmVkIGJlY2F1c2UgdGhlXHJcbiAgICAgICAgICAgICAgICAvLyBmdW5jdGlvbiB3aWxsIGhhdmUgY2xlYXJlZCB0aGF0IHN0YXRlIGFuZCBub3QgZXhlY3V0ZSB0aGF0IHBhdGguXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zaG93U2NyZWVuQWZ0ZXJMb2dpbigpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3RvZ2dsZV9teV9ncm91cHMnOlxyXG4gICAgICAgICAgICAgICAgLy8gV2UganVzdCBkaXNwYXRjaCB0aGUgcGFnZSBjaGFuZ2UgcmF0aGVyIHRoYW4gaGF2ZSB0byB3b3JyeSBhYm91dFxyXG4gICAgICAgICAgICAgICAgLy8gd2hhdCB0aGUgbG9naWMgaXMgZm9yIGVhY2ggb2YgdGhlc2UgYnJhbmNoZXMuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5wYWdlX3R5cGUgPT09IFBhZ2VUeXBlcy5NeUdyb3Vwcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19sYXN0X3NjcmVlbid9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd2aWV3X215X2dyb3Vwcyd9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdub3RpZmllcl9lbmFibGVkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dOb3RpZmllclRvb2xiYXI6IE5vdGlmaWVyLnNob3VsZFNob3dUb29sYmFyKCl9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdoaWRlX2xlZnRfcGFuZWwnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sbGFwc2VMaHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdmb2N1c19yb29tX2ZpbHRlcic6IC8vIGZvciBDdHJsT3JDbWQrSyB0byB3b3JrIGJ5IGV4cGFuZGluZyB0aGUgbGVmdCBwYW5lbCBmaXJzdFxyXG4gICAgICAgICAgICBjYXNlICdzaG93X2xlZnRfcGFuZWwnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sbGFwc2VMaHM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAncGFuZWxfZGlzYWJsZSc6IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGxlZnREaXNhYmxlZDogcGF5bG9hZC5sZWZ0RGlzYWJsZWQgfHwgcGF5bG9hZC5zaWRlRGlzYWJsZWQgfHwgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgbWlkZGxlRGlzYWJsZWQ6IHBheWxvYWQubWlkZGxlRGlzYWJsZWQgfHwgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gV2UgZG9uJ3QgdHJhY2sgdGhlIHJpZ2h0IHBhbmVsIGJlaW5nIGRpc2FibGVkIGhlcmUgLSBpdCdzIHRyYWNrZWQgaW4gdGhlIHN0b3JlLlxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlICdvbl9sb2dnZWRfaW4nOlxyXG4gICAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgICAgICFMaWZlY3ljbGUuaXNTb2Z0TG9nb3V0KCkgJiZcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnZpZXcgIT09IFZJRVdTLkxPR0lOICYmXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS52aWV3ICE9PSBWSUVXUy5SRUdJU1RFUiAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUudmlldyAhPT0gVklFV1MuQ09NUExFVEVfU0VDVVJJVFkgJiZcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnZpZXcgIT09IFZJRVdTLkUyRV9TRVRVUFxyXG4gICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb25Mb2dnZWRJbigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ29uX2NsaWVudF9ub3RfdmlhYmxlJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX29uU29mdExvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ29uX2xvZ2dlZF9vdXQnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fb25Mb2dnZWRPdXQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd3aWxsX3N0YXJ0X2NsaWVudCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtyZWFkeTogZmFsc2V9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIGNsaWVudCBpcyBhYm91dCB0byBzdGFydCwgd2UgYXJlLCBieSBkZWZpbml0aW9uLCBub3QgcmVhZHkuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gU2V0IHJlYWR5IHRvIGZhbHNlIG5vdywgdGhlbiBpdCdsbCBiZSBzZXQgdG8gdHJ1ZSB3aGVuIHRoZSBzeW5jXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbGlzdGVuZXIgd2Ugc2V0IGJlbG93IGZpcmVzLlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX29uV2lsbFN0YXJ0Q2xpZW50KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdjbGllbnRfc3RhcnRlZCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9vbkNsaWVudFN0YXJ0ZWQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICduZXdfdmVyc2lvbic6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVmVyc2lvbihcclxuICAgICAgICAgICAgICAgICAgICBwYXlsb2FkLmN1cnJlbnRWZXJzaW9uLCBwYXlsb2FkLm5ld1ZlcnNpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgcGF5bG9hZC5yZWxlYXNlTm90ZXMsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2NoZWNrX3VwZGF0ZXMnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGNoZWNraW5nRm9yVXBkYXRlOiBwYXlsb2FkLnZhbHVlIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3NlbmRfZXZlbnQnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5vblNlbmRFdmVudChwYXlsb2FkLnJvb21faWQsIHBheWxvYWQuZXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2FyaWFfaGlkZV9tYWluX2FwcCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBoaWRlVG9TUlVzZXJzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnYXJpYV91bmhpZGVfbWFpbl9hcHAnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgaGlkZVRvU1JVc2VyczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdhY2NlcHRfY29va2llcyc6XHJcbiAgICAgICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiYW5hbHl0aWNzT3B0SW5cIiwgbnVsbCwgU2V0dGluZ0xldmVsLkRFVklDRSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwic2hvd0Nvb2tpZUJhclwiLCBudWxsLCBTZXR0aW5nTGV2ZWwuREVWSUNFLCBmYWxzZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd0Nvb2tpZUJhcjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIEFuYWx5dGljcy5lbmFibGUoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdyZWplY3RfY29va2llcyc6XHJcbiAgICAgICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiYW5hbHl0aWNzT3B0SW5cIiwgbnVsbCwgU2V0dGluZ0xldmVsLkRFVklDRSwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgU2V0dGluZ3NTdG9yZS5zZXRWYWx1ZShcInNob3dDb29raWVCYXJcIiwgbnVsbCwgU2V0dGluZ0xldmVsLkRFVklDRSwgZmFsc2UpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3dDb29raWVCYXI6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9zZXRQYWdlOiBmdW5jdGlvbihwYWdlVHlwZSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwYWdlX3R5cGU6IHBhZ2VUeXBlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc3RhcnRSZWdpc3RyYXRpb246IGFzeW5jIGZ1bmN0aW9uKHBhcmFtcykge1xyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xyXG4gICAgICAgICAgICB2aWV3OiBWSUVXUy5SRUdJU1RFUixcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBPbmx5IGhvbm91ciBwYXJhbXMgaWYgdGhleSBhcmUgYWxsIHByZXNlbnQsIG90aGVyd2lzZSB3ZSByZXNldFxyXG4gICAgICAgIC8vIEhTIGFuZCBJUyBVUkxzIHdoZW4gc3dpdGNoaW5nIHRvIHJlZ2lzdHJhdGlvbi5cclxuICAgICAgICBpZiAocGFyYW1zLmNsaWVudF9zZWNyZXQgJiZcclxuICAgICAgICAgICAgcGFyYW1zLnNlc3Npb25faWQgJiZcclxuICAgICAgICAgICAgcGFyYW1zLmhzX3VybCAmJlxyXG4gICAgICAgICAgICBwYXJhbXMuaXNfdXJsICYmXHJcbiAgICAgICAgICAgIHBhcmFtcy5zaWRcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgbmV3U3RhdGUuc2VydmVyQ29uZmlnID0gYXdhaXQgQXV0b0Rpc2NvdmVyeVV0aWxzLnZhbGlkYXRlU2VydmVyQ29uZmlnV2l0aFN0YXRpY1VybHMoXHJcbiAgICAgICAgICAgICAgICBwYXJhbXMuaHNfdXJsLCBwYXJhbXMuaXNfdXJsLFxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgbmV3U3RhdGUucmVnaXN0ZXJfY2xpZW50X3NlY3JldCA9IHBhcmFtcy5jbGllbnRfc2VjcmV0O1xyXG4gICAgICAgICAgICBuZXdTdGF0ZS5yZWdpc3Rlcl9zZXNzaW9uX2lkID0gcGFyYW1zLnNlc3Npb25faWQ7XHJcbiAgICAgICAgICAgIG5ld1N0YXRlLnJlZ2lzdGVyX2lkX3NpZCA9IHBhcmFtcy5zaWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlRm9yTmV3VmlldyhuZXdTdGF0ZSk7XHJcbiAgICAgICAgVGhlbWVDb250cm9sbGVyLmlzTG9naW4gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX3RoZW1lV2F0Y2hlci5yZWNoZWNrKCk7XHJcbiAgICAgICAgdGhpcy5ub3RpZnlOZXdTY3JlZW4oJ3JlZ2lzdGVyJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IE1vdmUgdG8gUm9vbVZpZXdTdG9yZVxyXG4gICAgX3ZpZXdOZXh0Um9vbTogZnVuY3Rpb24ocm9vbUluZGV4RGVsdGEpIHtcclxuICAgICAgICBjb25zdCBhbGxSb29tcyA9IFJvb21MaXN0U29ydGVyLm1vc3RSZWNlbnRBY3Rpdml0eUZpcnN0KFxyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbXMoKSxcclxuICAgICAgICApO1xyXG4gICAgICAgIC8vIElmIHRoZXJlIGFyZSAwIHJvb21zIG9yIDEgcm9vbSwgdmlldyB0aGUgaG9tZSBwYWdlIGJlY2F1c2Ugb3RoZXJ3aXNlXHJcbiAgICAgICAgLy8gaWYgdGhlcmUgYXJlIDAsIHdlIGVuZCB1cCB0cnlpbmcgdG8gaW5kZXggaW50byBhbiBlbXB0eSBhcnJheSwgYW5kXHJcbiAgICAgICAgLy8gaWYgdGhlcmUgaXMgMSwgd2UgZW5kIHVwIHZpZXdpbmcgdGhlIHNhbWUgcm9vbS5cclxuICAgICAgICBpZiAoYWxsUm9vbXMubGVuZ3RoIDwgMikge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19ob21lX3BhZ2UnLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgcm9vbUluZGV4ID0gLTE7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxSb29tcy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBpZiAoYWxsUm9vbXNbaV0ucm9vbUlkID09IHRoaXMuc3RhdGUuY3VycmVudFJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgcm9vbUluZGV4ID0gaTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJvb21JbmRleCA9IChyb29tSW5kZXggKyByb29tSW5kZXhEZWx0YSkgJSBhbGxSb29tcy5sZW5ndGg7XHJcbiAgICAgICAgaWYgKHJvb21JbmRleCA8IDApIHJvb21JbmRleCA9IGFsbFJvb21zLmxlbmd0aCAtIDE7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogYWxsUm9vbXNbcm9vbUluZGV4XS5yb29tSWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IE1vdmUgdG8gUm9vbVZpZXdTdG9yZVxyXG4gICAgX3ZpZXdJbmRleGVkUm9vbTogZnVuY3Rpb24ocm9vbUluZGV4KSB7XHJcbiAgICAgICAgY29uc3QgYWxsUm9vbXMgPSBSb29tTGlzdFNvcnRlci5tb3N0UmVjZW50QWN0aXZpdHlGaXJzdChcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb21zKCksXHJcbiAgICAgICAgKTtcclxuICAgICAgICBpZiAoYWxsUm9vbXNbcm9vbUluZGV4XSkge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgICAgIHJvb21faWQ6IGFsbFJvb21zW3Jvb21JbmRleF0ucm9vbUlkLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHN3aXRjaCB2aWV3IHRvIHRoZSBnaXZlbiByb29tXHJcbiAgICAvL1xyXG4gICAgLy8gQHBhcmFtIHtPYmplY3R9IHJvb21JbmZvIE9iamVjdCBjb250YWluaW5nIGRhdGEgYWJvdXQgdGhlIHJvb20gdG8gYmUgam9pbmVkXHJcbiAgICAvLyBAcGFyYW0ge3N0cmluZz19IHJvb21JbmZvLnJvb21faWQgSUQgb2YgdGhlIHJvb20gdG8gam9pbi4gT25lIG9mIHJvb21faWQgb3Igcm9vbV9hbGlhcyBtdXN0IGJlIGdpdmVuLlxyXG4gICAgLy8gQHBhcmFtIHtzdHJpbmc9fSByb29tSW5mby5yb29tX2FsaWFzIEFsaWFzIG9mIHRoZSByb29tIHRvIGpvaW4uIE9uZSBvZiByb29tX2lkIG9yIHJvb21fYWxpYXMgbXVzdCBiZSBnaXZlbi5cclxuICAgIC8vIEBwYXJhbSB7Ym9vbGVhbj19IHJvb21JbmZvLmF1dG9fam9pbiBJZiB0cnVlLCBhdXRvbWF0aWNhbGx5IGF0dGVtcHQgdG8gam9pbiB0aGUgcm9vbSBpZiBub3QgYWxyZWFkeSBhIG1lbWJlci5cclxuICAgIC8vIEBwYXJhbSB7c3RyaW5nPX0gcm9vbUluZm8uZXZlbnRfaWQgSUQgb2YgdGhlIGV2ZW50IGluIHRoaXMgcm9vbSB0byBzaG93OiB0aGlzIHdpbGwgY2F1c2UgYSBzd2l0Y2ggdG8gdGhlXHJcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRleHQgb2YgdGhhdCBwYXJ0aWN1bGFyIGV2ZW50LlxyXG4gICAgLy8gQHBhcmFtIHtib29sZWFuPX0gcm9vbUluZm8uaGlnaGxpZ2h0ZWQgSWYgdHJ1ZSwgYWRkIGV2ZW50X2lkIHRvIHRoZSBoYXNoIG9mIHRoZSBVUkxcclxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZCBhbHRlciB0aGUgRXZlbnRUaWxlIHRvIGFwcGVhciBoaWdobGlnaHRlZC5cclxuICAgIC8vIEBwYXJhbSB7T2JqZWN0PX0gcm9vbUluZm8udGhpcmRfcGFydHlfaW52aXRlIE9iamVjdCBjb250YWluaW5nIGRhdGEgYWJvdXQgdGhlIHRoaXJkIHBhcnR5XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlIHJlY2VpdmVkIHRvIGpvaW4gdGhlIHJvb20sIGlmIGFueS5cclxuICAgIC8vIEBwYXJhbSB7c3RyaW5nPX0gcm9vbUluZm8udGhpcmRfcGFydHlfaW52aXRlLmludml0ZVNpZ25VcmwgM3BpZCBpbnZpdGUgc2lnbiBVUkxcclxuICAgIC8vIEBwYXJhbSB7c3RyaW5nPX0gcm9vbUluZm8udGhpcmRfcGFydHlfaW52aXRlLmludml0ZWRFbWFpbCBUaGUgZW1haWwgYWRkcmVzcyB0aGUgaW52aXRlIHdhcyBzZW50IHRvXHJcbiAgICAvLyBAcGFyYW0ge09iamVjdD19IHJvb21JbmZvLm9vYl9kYXRhIE9iamVjdCBvZiBhZGRpdGlvbmFsIGRhdGEgYWJvdXQgdGhlIHJvb21cclxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQgaGFzIGJlZW4gcGFzc2VkIG91dC1vZi1iYW5kIChlZy5cclxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvb20gbmFtZSBhbmQgYXZhdGFyIGZyb20gYW4gaW52aXRlIGVtYWlsKVxyXG4gICAgX3ZpZXdSb29tOiBmdW5jdGlvbihyb29tSW5mbykge1xyXG4gICAgICAgIHRoaXMuZm9jdXNDb21wb3NlciA9IHRydWU7XHJcblxyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xyXG4gICAgICAgICAgICB2aWV3OiBWSUVXUy5MT0dHRURfSU4sXHJcbiAgICAgICAgICAgIGN1cnJlbnRSb29tSWQ6IHJvb21JbmZvLnJvb21faWQgfHwgbnVsbCxcclxuICAgICAgICAgICAgcGFnZV90eXBlOiBQYWdlVHlwZXMuUm9vbVZpZXcsXHJcbiAgICAgICAgICAgIHRoaXJkUGFydHlJbnZpdGU6IHJvb21JbmZvLnRoaXJkX3BhcnR5X2ludml0ZSxcclxuICAgICAgICAgICAgcm9vbU9vYkRhdGE6IHJvb21JbmZvLm9vYl9kYXRhLFxyXG4gICAgICAgICAgICB2aWFTZXJ2ZXJzOiByb29tSW5mby52aWFfc2VydmVycyxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAocm9vbUluZm8ucm9vbV9hbGlhcykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICAgICAgICAgIGBTd2l0Y2hpbmcgdG8gcm9vbSBhbGlhcyAke3Jvb21JbmZvLnJvb21fYWxpYXN9IGF0IGV2ZW50IGAgK1xyXG4gICAgICAgICAgICAgICAgcm9vbUluZm8uZXZlbnRfaWQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYFN3aXRjaGluZyB0byByb29tIGlkICR7cm9vbUluZm8ucm9vbV9pZH0gYXQgZXZlbnQgYCArXHJcbiAgICAgICAgICAgICAgICByb29tSW5mby5ldmVudF9pZCxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFdhaXQgZm9yIHRoZSBmaXJzdCBzeW5jIHRvIGNvbXBsZXRlIHNvIHRoYXQgaWYgYSByb29tIGRvZXMgaGF2ZSBhbiBhbGlhcyxcclxuICAgICAgICAvLyBpdCB3b3VsZCBoYXZlIGJlZW4gcmV0cmlldmVkLlxyXG4gICAgICAgIGxldCB3YWl0Rm9yID0gUHJvbWlzZS5yZXNvbHZlKG51bGwpO1xyXG4gICAgICAgIGlmICghdGhpcy5maXJzdFN5bmNDb21wbGV0ZSkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuZmlyc3RTeW5jUHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdDYW5ub3QgdmlldyBhIHJvb20gYmVmb3JlIGZpcnN0IHN5bmMuIHJvb21faWQ6Jywgcm9vbUluZm8ucm9vbV9pZCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgd2FpdEZvciA9IHRoaXMuZmlyc3RTeW5jUHJvbWlzZS5wcm9taXNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHdhaXRGb3IudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBwcmVzZW50ZWRJZCA9IHJvb21JbmZvLnJvb21fYWxpYXMgfHwgcm9vbUluZm8ucm9vbV9pZDtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHJvb21JbmZvLnJvb21faWQpO1xyXG4gICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGhlQWxpYXMgPSBSb29tcy5nZXREaXNwbGF5QWxpYXNGb3JSb29tKHJvb20pO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoZUFsaWFzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJlc2VudGVkSWQgPSB0aGVBbGlhcztcclxuICAgICAgICAgICAgICAgICAgICAvLyBTdG9yZSBkaXNwbGF5IGFsaWFzIG9mIHRoZSBwcmVzZW50ZWQgcm9vbSBpbiBjYWNoZSB0byBzcGVlZCBmdXR1cmVcclxuICAgICAgICAgICAgICAgICAgICAvLyBuYXZpZ2F0aW9uLlxyXG4gICAgICAgICAgICAgICAgICAgIHN0b3JlUm9vbUFsaWFzSW5DYWNoZSh0aGVBbGlhcywgcm9vbS5yb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIFN0b3JlIHRoaXMgYXMgdGhlIElEIG9mIHRoZSBsYXN0IHJvb20gYWNjZXNzZWQuIFRoaXMgaXMgc28gdGhhdCB3ZSBjYW5cclxuICAgICAgICAgICAgICAgIC8vIHBlcnNpc3Qgd2hpY2ggcm9vbSBpcyBiZWluZyBzdG9yZWQgYWNyb3NzIHJlZnJlc2hlcyBhbmQgYnJvd3NlciBxdWl0cy5cclxuICAgICAgICAgICAgICAgIGlmIChsb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbXhfbGFzdF9yb29tX2lkJywgcm9vbS5yb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAocm9vbUluZm8uZXZlbnRfaWQgJiYgcm9vbUluZm8uaGlnaGxpZ2h0ZWQpIHtcclxuICAgICAgICAgICAgICAgIHByZXNlbnRlZElkICs9IFwiL1wiICsgcm9vbUluZm8uZXZlbnRfaWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV3U3RhdGUucmVhZHkgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKG5ld1N0YXRlLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbigncm9vbS8nICsgcHJlc2VudGVkSWQpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3ZpZXdHcm91cDogZnVuY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIGNvbnN0IGdyb3VwSWQgPSBwYXlsb2FkLmdyb3VwX2lkO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjdXJyZW50R3JvdXBJZDogZ3JvdXBJZCxcclxuICAgICAgICAgICAgY3VycmVudEdyb3VwSXNOZXc6IHBheWxvYWQuZ3JvdXBfaXNfbmV3LFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuX3NldFBhZ2UoUGFnZVR5cGVzLkdyb3VwVmlldyk7XHJcbiAgICAgICAgdGhpcy5ub3RpZnlOZXdTY3JlZW4oJ2dyb3VwLycgKyBncm91cElkKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3ZpZXdTb21ldGhpbmdCZWhpbmRNb2RhbCgpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS52aWV3ICE9PSBWSUVXUy5MT0dHRURfSU4pIHtcclxuICAgICAgICAgICAgdGhpcy5fdmlld1dlbGNvbWUoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY3VycmVudEdyb3VwSWQgJiYgIXRoaXMuc3RhdGUuY3VycmVudFJvb21JZCkge1xyXG4gICAgICAgICAgICB0aGlzLl92aWV3SG9tZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3ZpZXdXZWxjb21lKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGVGb3JOZXdWaWV3KHtcclxuICAgICAgICAgICAgdmlldzogVklFV1MuV0VMQ09NRSxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbignd2VsY29tZScpO1xyXG4gICAgICAgIFRoZW1lQ29udHJvbGxlci5pc0xvZ2luID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl90aGVtZVdhdGNoZXIucmVjaGVjaygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdmlld0hvbWU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIFRoZSBob21lIHBhZ2UgcmVxdWlyZXMgdGhlIFwibG9nZ2VkIGluXCIgdmlldywgc28gd2UnbGwgc2V0IHRoYXQuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZUZvck5ld1ZpZXcoe1xyXG4gICAgICAgICAgICB2aWV3OiBWSUVXUy5MT0dHRURfSU4sXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fc2V0UGFnZShQYWdlVHlwZXMuSG9tZVBhZ2UpO1xyXG4gICAgICAgIHRoaXMubm90aWZ5TmV3U2NyZWVuKCdob21lJyk7XHJcbiAgICAgICAgVGhlbWVDb250cm9sbGVyLmlzTG9naW4gPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl90aGVtZVdhdGNoZXIucmVjaGVjaygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdmlld1VzZXI6IGZ1bmN0aW9uKHVzZXJJZCwgc3ViQWN0aW9uKSB7XHJcbiAgICAgICAgLy8gV2FpdCBmb3IgdGhlIGZpcnN0IHN5bmMgc28gdGhhdCBgZ2V0Um9vbWAgZ2l2ZXMgdXMgYSByb29tIG9iamVjdCBpZiBpdCdzXHJcbiAgICAgICAgLy8gaW4gdGhlIHN5bmMgcmVzcG9uc2VcclxuICAgICAgICBjb25zdCB3YWl0Rm9yU3luYyA9IHRoaXMuZmlyc3RTeW5jUHJvbWlzZSA/XHJcbiAgICAgICAgICAgIHRoaXMuZmlyc3RTeW5jUHJvbWlzZS5wcm9taXNlIDogUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgd2FpdEZvclN5bmMudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzdWJBY3Rpb24gPT09ICdjaGF0Jykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fY2hhdENyZWF0ZU9yUmV1c2UodXNlcklkKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbigndXNlci8nICsgdXNlcklkKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFVzZXJJZDogdXNlcklkfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3NldFBhZ2UoUGFnZVR5cGVzLlVzZXJWaWV3KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NldE14SWQ6IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICBjb25zdCBTZXRNeElkRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZGlhbG9ncy5TZXRNeElkRGlhbG9nJyk7XHJcbiAgICAgICAgY29uc3QgY2xvc2UgPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdTZXQgTVhJRCcsICcnLCBTZXRNeElkRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIGhvbWVzZXJ2ZXJVcmw6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRIb21lc2VydmVyVXJsKCksXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IChzdWJtaXR0ZWQsIGNyZWRlbnRpYWxzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXN1Ym1pdHRlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ2NhbmNlbF9hZnRlcl9zeW5jX3ByZXBhcmVkJyxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocGF5bG9hZC5nb19ob21lX29uX2NhbmNlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19ob21lX3BhZ2UnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLnNldEp1c3RSZWdpc3RlcmVkVXNlcklkKGNyZWRlbnRpYWxzLnVzZXJfaWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblJlZ2lzdGVyZWQoY3JlZGVudGlhbHMpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBvbkRpZmZlcmVudFNlcnZlckNsaWNrZWQ6IChldikgPT4ge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdzdGFydF9yZWdpc3RyYXRpb24nfSk7XHJcbiAgICAgICAgICAgICAgICBjbG9zZSgpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBvbkxvZ2luQ2xpY2s6IChldikgPT4ge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdzdGFydF9sb2dpbid9KTtcclxuICAgICAgICAgICAgICAgIGNsb3NlKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSkuY2xvc2U7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jcmVhdGVSb29tOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBDcmVhdGVSb29tRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5DcmVhdGVSb29tRGlhbG9nJyk7XHJcbiAgICAgICAgY29uc3QgbW9kYWwgPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDcmVhdGUgUm9vbScsICcnLCBDcmVhdGVSb29tRGlhbG9nKTtcclxuXHJcbiAgICAgICAgY29uc3QgW3Nob3VsZENyZWF0ZSwgb3B0c10gPSBhd2FpdCBtb2RhbC5maW5pc2hlZDtcclxuICAgICAgICBpZiAoc2hvdWxkQ3JlYXRlKSB7XHJcbiAgICAgICAgICAgIGNyZWF0ZVJvb20ob3B0cyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfY2hhdENyZWF0ZU9yUmV1c2U6IGZ1bmN0aW9uKHVzZXJJZCkge1xyXG4gICAgICAgIC8vIFVzZSBhIGRlZmVycmVkIGFjdGlvbiB0byByZXNob3cgdGhlIGRpYWxvZyBvbmNlIHRoZSB1c2VyIGhhcyByZWdpc3RlcmVkXHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAgICAgLy8gTm8gcG9pbnQgaW4gbWFraW5nIDIgRE1zIHdpdGggd2VsY29tZSBib3QuIFRoaXMgYXNzdW1lcyB2aWV3X3NldF9teGlkIHdpbGxcclxuICAgICAgICAgICAgLy8gcmVzdWx0IGluIGEgbmV3IERNIHdpdGggdGhlIHdlbGNvbWUgdXNlci5cclxuICAgICAgICAgICAgaWYgKHVzZXJJZCAhPT0gdGhpcy5wcm9wcy5jb25maWcud2VsY29tZVVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdkb19hZnRlcl9zeW5jX3ByZXBhcmVkJyxcclxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZF9hY3Rpb246IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19zdGFydF9jaGF0X29yX3JldXNlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcl9pZDogdXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAncmVxdWlyZV9yZWdpc3RyYXRpb24nLFxyXG4gICAgICAgICAgICAgICAgLy8gSWYgdGhlIHNldF9teGlkIGRpYWxvZyBpcyBjYW5jZWxsZWQsIHZpZXcgL3dlbGNvbWUgYmVjYXVzZSBpZiB0aGVcclxuICAgICAgICAgICAgICAgIC8vIGJyb3dzZXIgd2FzIHBvaW50aW5nIGF0IC91c2VyL0Bzb21lb25lOmRvbWFpbj9hY3Rpb249Y2hhdCwgdGhlIFVSTFxyXG4gICAgICAgICAgICAgICAgLy8gbmVlZHMgdG8gYmUgcmVzZXQgc28gdGhhdCB0aGV5IGNhbiByZXZpc2l0IC91c2VyLy4uIC8vIChhbmQgdHJpZ2dlclxyXG4gICAgICAgICAgICAgICAgLy8gYF9jaGF0Q3JlYXRlT3JSZXVzZWAgYWdhaW4pXHJcbiAgICAgICAgICAgICAgICBnb193ZWxjb21lX29uX2NhbmNlbDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHNjcmVlbl9hZnRlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNjcmVlbjogYHVzZXIvJHt0aGlzLnByb3BzLmNvbmZpZy53ZWxjb21lVXNlcklkfWAsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7IGFjdGlvbjogJ2NoYXQnIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVE9ETzogSW1tdXRhYmxlIERNcyByZXBsYWNlcyB0aGlzXHJcblxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCBkbVJvb21NYXAgPSBuZXcgRE1Sb29tTWFwKGNsaWVudCk7XHJcbiAgICAgICAgY29uc3QgZG1Sb29tcyA9IGRtUm9vbU1hcC5nZXRETVJvb21zRm9yVXNlcklkKHVzZXJJZCk7XHJcblxyXG4gICAgICAgIGlmIChkbVJvb21zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICByb29tX2lkOiBkbVJvb21zWzBdLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc3RhcnRfY2hhdCcsXHJcbiAgICAgICAgICAgICAgICB1c2VyX2lkOiB1c2VySWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2xlYXZlUm9vbVdhcm5pbmdzOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICBjb25zdCByb29tVG9MZWF2ZSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgLy8gU2hvdyBhIHdhcm5pbmcgaWYgdGhlcmUgYXJlIGFkZGl0aW9uYWwgY29tcGxpY2F0aW9ucy5cclxuICAgICAgICBjb25zdCBqb2luUnVsZXMgPSByb29tVG9MZWF2ZS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5qb2luX3J1bGVzJywgJycpO1xyXG4gICAgICAgIGNvbnN0IHdhcm5pbmdzID0gW107XHJcbiAgICAgICAgaWYgKGpvaW5SdWxlcykge1xyXG4gICAgICAgICAgICBjb25zdCBydWxlID0gam9pblJ1bGVzLmdldENvbnRlbnQoKS5qb2luX3J1bGU7XHJcbiAgICAgICAgICAgIGlmIChydWxlICE9PSBcInB1YmxpY1wiKSB7XHJcbiAgICAgICAgICAgICAgICB3YXJuaW5ncy5wdXNoKChcclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ3YXJuaW5nXCIga2V5PVwibm9uX3B1YmxpY193YXJuaW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsnICcvKiBXaGl0ZXNwYWNlLCBvdGhlcndpc2UgdGhlIHNlbnRlbmNlcyBnZXQgc21hc2hlZCB0b2dldGhlciAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJUaGlzIHJvb20gaXMgbm90IHB1YmxpYy4gWW91IHdpbGwgbm90IGJlIGFibGUgdG8gcmVqb2luIHdpdGhvdXQgYW4gaW52aXRlLlwiKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHdhcm5pbmdzO1xyXG4gICAgfSxcclxuXHJcbiAgICBfbGVhdmVSb29tOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3Qgcm9vbVRvTGVhdmUgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgICAgIGNvbnN0IHdhcm5pbmdzID0gdGhpcy5fbGVhdmVSb29tV2FybmluZ3Mocm9vbUlkKTtcclxuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTGVhdmUgcm9vbScsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoXCJMZWF2ZSByb29tXCIpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogKFxyXG4gICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICB7IF90KFwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGxlYXZlIHRoZSByb29tICclKHJvb21OYW1lKXMnP1wiLCB7cm9vbU5hbWU6IHJvb21Ub0xlYXZlLm5hbWV9KSB9XHJcbiAgICAgICAgICAgICAgICB7IHdhcm5pbmdzIH1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgYnV0dG9uOiBfdChcIkxlYXZlXCIpLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAoc2hvdWxkTGVhdmUpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChzaG91bGRMZWF2ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubGVhdmVSb29tQ2hhaW4ocm9vbUlkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRklYTUU6IGNvbnRyb2xsZXIgc2hvdWxkbid0IGJlIGxvYWRpbmcgYSB2aWV3IDooXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbW9kYWwgPSBNb2RhbC5jcmVhdGVEaWFsb2coTG9hZGVyLCBudWxsLCAnbXhfRGlhbG9nX3NwaW5uZXInKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZC50aGVuKChlcnJvcnMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWwuY2xvc2UoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgbGVmdFJvb21JZCBvZiBPYmplY3Qua2V5cyhlcnJvcnMpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBlcnIgPSBlcnJvcnNbbGVmdFJvb21JZF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWVycikgY29udGludWU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBsZWF2ZSByb29tIFwiICsgbGVmdFJvb21JZCArIFwiIFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB0aXRsZSA9IF90KFwiRmFpbGVkIHRvIGxlYXZlIHJvb21cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgbWVzc2FnZSA9IF90KFwiU2VydmVyIG1heSBiZSB1bmF2YWlsYWJsZSwgb3ZlcmxvYWRlZCwgb3IgeW91IGhpdCBhIGJ1Zy5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyLmVycmNvZGUgPT09ICdNX0NBTk5PVF9MRUFWRV9TRVJWRVJfTk9USUNFX1JPT00nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGUgPSBfdChcIkNhbid0IGxlYXZlIFNlcnZlciBOb3RpY2VzIHJvb21cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZSA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlRoaXMgcm9vbSBpcyB1c2VkIGZvciBpbXBvcnRhbnQgbWVzc2FnZXMgZnJvbSB0aGUgSG9tZXNlcnZlciwgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInNvIHlvdSBjYW5ub3QgbGVhdmUgaXQuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXJyICYmIGVyci5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZSA9IGVyci5tZXNzYWdlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGxlYXZlIHJvb20nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFJvb21JZCA9PT0gcm9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3ZpZXdfbmV4dF9yb29tJ30pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGlzIHNob3VsZCBvbmx5IGhhcHBlbiBpZiBzb21ldGhpbmcgd2VudCBzZXJpb3VzbHkgd3Jvbmcgd2l0aCBsZWF2aW5nIHRoZSBjaGFpbi5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWwuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBsZWF2ZSByb29tIFwiICsgcm9vbUlkICsgXCIgXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gbGVhdmUgcm9vbScsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRmFpbGVkIHRvIGxlYXZlIHJvb21cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJVbmtub3duIGVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdGFydHMgYSBjaGF0IHdpdGggdGhlIHdlbGNvbWUgdXNlciwgaWYgdGhlIHVzZXIgZG9lc24ndCBhbHJlYWR5IGhhdmUgb25lXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfSBUaGUgcm9vbSBJRCBvZiB0aGUgbmV3IHJvb20sIG9yIG51bGwgaWYgbm8gcm9vbSB3YXMgY3JlYXRlZFxyXG4gICAgICovXHJcbiAgICBhc3luYyBfc3RhcnRXZWxjb21lVXNlckNoYXQoKSB7XHJcbiAgICAgICAgLy8gV2UgY2FuIGVuZCB1cCB3aXRoIG11bHRpcGxlIHRhYnMgcG9zdC1yZWdpc3RyYXRpb24gd2hlcmUgdGhlIHVzZXJcclxuICAgICAgICAvLyBtaWdodCB0aGVuIGVuZCB1cCB3aXRoIGEgc2Vzc2lvbiBhbmQgd2UgZG9uJ3Qgd2FudCB0aGVtIGFsbCBtYWtpbmdcclxuICAgICAgICAvLyBhIGNoYXQgd2l0aCB0aGUgd2VsY29tZSB1c2VyOiB0cnkgdG8gZGUtZHVwZS5cclxuICAgICAgICAvLyBXZSBuZWVkIHRvIHdhaXQgZm9yIHRoZSBmaXJzdCBzeW5jIHRvIGNvbXBsZXRlIGZvciB0aGlzIHRvXHJcbiAgICAgICAgLy8gd29yayB0aG91Z2guXHJcbiAgICAgICAgbGV0IHdhaXRGb3I7XHJcbiAgICAgICAgaWYgKCF0aGlzLmZpcnN0U3luY0NvbXBsZXRlKSB7XHJcbiAgICAgICAgICAgIHdhaXRGb3IgPSB0aGlzLmZpcnN0U3luY1Byb21pc2UucHJvbWlzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB3YWl0Rm9yID0gUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF3YWl0IHdhaXRGb3I7XHJcblxyXG4gICAgICAgIGNvbnN0IHdlbGNvbWVVc2VyUm9vbXMgPSBETVJvb21NYXAuc2hhcmVkKCkuZ2V0RE1Sb29tc0ZvclVzZXJJZChcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5jb25maWcud2VsY29tZVVzZXJJZCxcclxuICAgICAgICApO1xyXG4gICAgICAgIGlmICh3ZWxjb21lVXNlclJvb21zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICBjb25zdCByb29tSWQgPSBhd2FpdCBjcmVhdGVSb29tKHtcclxuICAgICAgICAgICAgICAgIGRtVXNlcklkOiB0aGlzLnByb3BzLmNvbmZpZy53ZWxjb21lVXNlcklkLFxyXG4gICAgICAgICAgICAgICAgLy8gT25seSB2aWV3IHRoZSB3ZWxjb21lIHVzZXIgaWYgd2UncmUgTk9UIGxvb2tpbmcgYXQgYSByb29tXHJcbiAgICAgICAgICAgICAgICBhbmRWaWV3OiAhdGhpcy5zdGF0ZS5jdXJyZW50Um9vbUlkLFxyXG4gICAgICAgICAgICAgICAgc3Bpbm5lcjogZmFsc2UsIC8vIHdlJ3JlIGFscmVhZHkgc2hvd2luZyBvbmU6IHdlIGRvbid0IG5lZWQgYW5vdGhlciBvbmVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIC8vIFRoaXMgaXMgYSBiaXQgb2YgYSBoYWNrLCBidXQgc2luY2UgdGhlIGRlZHVwbGljYXRpb24gcmVsaWVzXHJcbiAgICAgICAgICAgIC8vIG9uIG0uZGlyZWN0IGJlaW5nIHVwIHRvIGRhdGUsIHdlIG5lZWQgdG8gZm9yY2UgYSBzeW5jXHJcbiAgICAgICAgICAgIC8vIG9mIHRoZSBkYXRhYmFzZSwgb3RoZXJ3aXNlIGlmIHRoZSB1c2VyIGdvZXMgdG8gdGhlIG90aGVyXHJcbiAgICAgICAgICAgIC8vIHRhYiBiZWZvcmUgdGhlIG5leHQgc2F2ZSBoYXBwZW5zIChhIGZldyBtaW51dGVzKSwgdGhlXHJcbiAgICAgICAgICAgIC8vIHNhdmVkIHN5bmMgd2lsbCBiZSByZXN0b3JlZCBmcm9tIHRoZSBkYiBhbmQgdGhpcyBjb2RlIHdpbGxcclxuICAgICAgICAgICAgLy8gcnVuIHdpdGhvdXQgdGhlIHVwZGF0ZSB0byBtLmRpcmVjdCwgbWFraW5nIGFub3RoZXIgd2VsY29tZVxyXG4gICAgICAgICAgICAvLyB1c2VyIHJvb20gKGl0IGRvZXNuJ3Qgd2FpdCBmb3IgbmV3IGRhdGEgZnJvbSB0aGUgc2VydmVyLCBqdXN0XHJcbiAgICAgICAgICAgIC8vIHRoZSBzYXZlZCBzeW5jIHRvIGJlIGxvYWRlZCkuXHJcbiAgICAgICAgICAgIGNvbnN0IHNhdmVXZWxjb21lVXNlciA9IChldikgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgICAgIGV2LmdldFR5cGUoKSA9PSAnbS5kaXJlY3QnICYmXHJcbiAgICAgICAgICAgICAgICAgICAgZXYuZ2V0Q29udGVudCgpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgZXYuZ2V0Q29udGVudCgpW3RoaXMucHJvcHMuY29uZmlnLndlbGNvbWVVc2VySWRdXHJcbiAgICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc3RvcmUuc2F2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiYWNjb3VudERhdGFcIiwgc2F2ZVdlbGNvbWVVc2VyLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbihcImFjY291bnREYXRhXCIsIHNhdmVXZWxjb21lVXNlcik7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gcm9vbUlkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxsZWQgd2hlbiBhIG5ldyBsb2dnZWQgaW4gc2Vzc2lvbiBoYXMgc3RhcnRlZFxyXG4gICAgICovXHJcbiAgICBfb25Mb2dnZWRJbjogYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgVGhlbWVDb250cm9sbGVyLmlzTG9naW4gPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlRm9yTmV3Vmlldyh7IHZpZXc6IFZJRVdTLkxPR0dFRF9JTiB9KTtcclxuICAgICAgICAvLyBJZiBhIHNwZWNpZmljIHNjcmVlbiBpcyBzZXQgdG8gYmUgc2hvd24gYWZ0ZXIgbG9naW4sIHNob3cgdGhhdCBhYm92ZVxyXG4gICAgICAgIC8vIGFsbCBlbHNlLCBhcyBpdCBwcm9iYWJseSBtZWFucyB0aGUgdXNlciBjbGlja2VkIG9uIHNvbWV0aGluZyBhbHJlYWR5LlxyXG4gICAgICAgIGlmICh0aGlzLl9zY3JlZW5BZnRlckxvZ2luICYmIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4uc2NyZWVuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd1NjcmVlbihcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4uc2NyZWVuLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2NyZWVuQWZ0ZXJMb2dpbi5wYXJhbXMsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4gPSBudWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoTWF0cml4Q2xpZW50UGVnLmN1cnJlbnRVc2VySXNKdXN0UmVnaXN0ZXJlZCgpKSB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5zZXRKdXN0UmVnaXN0ZXJlZFVzZXJJZChudWxsKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmNvbmZpZy53ZWxjb21lVXNlcklkICYmIGdldEN1cnJlbnRMYW5ndWFnZSgpLnN0YXJ0c1dpdGgoXCJlblwiKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd2VsY29tZVVzZXJSb29tID0gYXdhaXQgdGhpcy5fc3RhcnRXZWxjb21lVXNlckNoYXQoKTtcclxuICAgICAgICAgICAgICAgIGlmICh3ZWxjb21lVXNlclJvb20gPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSBkaWRuJ3QgcmVkaXJlY3QgdG8gdGhlIHdlbGNvbWUgdXNlciByb29tLCBzbyBzaG93XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlIGhvbWVwYWdlLlxyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19ob21lX3BhZ2UnfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBUaGUgdXNlciBoYXMganVzdCBsb2dnZWQgaW4gYWZ0ZXIgcmVnaXN0ZXJpbmcsXHJcbiAgICAgICAgICAgICAgICAvLyBzbyBzaG93IHRoZSBob21lcGFnZS5cclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19ob21lX3BhZ2UnfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9zaG93U2NyZWVuQWZ0ZXJMb2dpbigpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgU3RvcmFnZU1hbmFnZXIudHJ5UGVyc2lzdFN0b3JhZ2UoKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3Nob3dTY3JlZW5BZnRlckxvZ2luOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBJZiBzY3JlZW5BZnRlckxvZ2luIGlzIHNldCwgdXNlIHRoYXQsIHRoZW4gbnVsbCBpdCBzbyB0aGF0IGEgc2Vjb25kIGxvZ2luIHdpbGxcclxuICAgICAgICAvLyByZXN1bHQgaW4gdmlld19ob21lX3BhZ2UsIF91c2VyX3NldHRpbmdzIG9yIF9yb29tX2RpcmVjdG9yeVxyXG4gICAgICAgIGlmICh0aGlzLl9zY3JlZW5BZnRlckxvZ2luICYmIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4uc2NyZWVuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd1NjcmVlbihcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4uc2NyZWVuLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2NyZWVuQWZ0ZXJMb2dpbi5wYXJhbXMsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMuX3NjcmVlbkFmdGVyTG9naW4gPSBudWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobG9jYWxTdG9yYWdlICYmIGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdteF9sYXN0X3Jvb21faWQnKSkge1xyXG4gICAgICAgICAgICAvLyBCZWZvcmUgZGVmYXVsdGluZyB0byBkaXJlY3RvcnksIHNob3cgdGhlIGxhc3Qgdmlld2VkIHJvb21cclxuICAgICAgICAgICAgdGhpcy5fdmlld0xhc3RSb29tKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld193ZWxjb21lX3BhZ2UnfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZ2V0SG9tZVBhZ2VVcmwodGhpcy5wcm9wcy5jb25maWcpKSB7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3ZpZXdfaG9tZV9wYWdlJ30pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maXJzdFN5bmNQcm9taXNlLnByb21pc2UudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd2aWV3X25leHRfcm9vbSd9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfdmlld0xhc3RSb29tOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICByb29tX2lkOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbXhfbGFzdF9yb29tX2lkJyksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsbGVkIHdoZW4gdGhlIHNlc3Npb24gaXMgbG9nZ2VkIG91dFxyXG4gICAgICovXHJcbiAgICBfb25Mb2dnZWRPdXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMubm90aWZ5TmV3U2NyZWVuKCdsb2dpbicpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGVGb3JOZXdWaWV3KHtcclxuICAgICAgICAgICAgdmlldzogVklFV1MuTE9HSU4sXHJcbiAgICAgICAgICAgIHJlYWR5OiBmYWxzZSxcclxuICAgICAgICAgICAgY29sbGFwc2VMaHM6IGZhbHNlLFxyXG4gICAgICAgICAgICBjdXJyZW50Um9vbUlkOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc3ViVGl0bGVTdGF0dXMgPSAnJztcclxuICAgICAgICB0aGlzLl9zZXRQYWdlU3VidGl0bGUoKTtcclxuICAgICAgICBUaGVtZUNvbnRyb2xsZXIuaXNMb2dpbiA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fdGhlbWVXYXRjaGVyLnJlY2hlY2soKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxsZWQgd2hlbiB0aGUgc2Vzc2lvbiBpcyBzb2Z0bHkgbG9nZ2VkIG91dFxyXG4gICAgICovXHJcbiAgICBfb25Tb2Z0TG9nb3V0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLm5vdGlmeU5ld1NjcmVlbignc29mdF9sb2dvdXQnKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlRm9yTmV3Vmlldyh7XHJcbiAgICAgICAgICAgIHZpZXc6IFZJRVdTLlNPRlRfTE9HT1VULFxyXG4gICAgICAgICAgICByZWFkeTogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbGxhcHNlTGhzOiBmYWxzZSxcclxuICAgICAgICAgICAgY3VycmVudFJvb21JZDogbnVsbCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN1YlRpdGxlU3RhdHVzID0gJyc7XHJcbiAgICAgICAgdGhpcy5fc2V0UGFnZVN1YnRpdGxlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsbGVkIGp1c3QgYmVmb3JlIHRoZSBtYXRyaXggY2xpZW50IGlzIHN0YXJ0ZWRcclxuICAgICAqICh1c2VmdWwgZm9yIHNldHRpbmcgbGlzdGVuZXJzKVxyXG4gICAgICovXHJcbiAgICBfb25XaWxsU3RhcnRDbGllbnQoKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIC8vIHJlc2V0IHRoZSAnaGF2ZSBjb21wbGV0ZWQgZmlyc3Qgc3luYycgZmxhZyxcclxuICAgICAgICAvLyBzaW5jZSB3ZSdyZSBhYm91dCB0byBzdGFydCB0aGUgY2xpZW50IGFuZCB0aGVyZWZvcmUgYWJvdXRcclxuICAgICAgICAvLyB0byBkbyB0aGUgZmlyc3Qgc3luY1xyXG4gICAgICAgIHRoaXMuZmlyc3RTeW5jQ29tcGxldGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmZpcnN0U3luY1Byb21pc2UgPSBkZWZlcigpO1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuXHJcbiAgICAgICAgLy8gQWxsb3cgdGhlIEpTIFNESyB0byByZWFwIHRpbWVsaW5lIGV2ZW50cy4gVGhpcyByZWR1Y2VzIHRoZSBhbW91bnQgb2ZcclxuICAgICAgICAvLyBtZW1vcnkgY29uc3VtZWQgYXMgdGhlIEpTIFNESyBzdG9yZXMgbXVsdGlwbGUgZGlzdGluY3QgY29waWVzIG9mIHJvb21cclxuICAgICAgICAvLyBzdGF0ZSAoZWFjaCBvZiB3aGljaCBjYW4gYmUgMTBzIG9mIE1CcykgZm9yIGVhY2ggRElTSk9JTlQgdGltZWxpbmUuIFRoaXMgaXNcclxuICAgICAgICAvLyBwYXJ0aWN1bGFybHkgbm90aWNlYWJsZSB3aGVuIHRoZXJlIGFyZSBsb3RzIG9mICdsaW1pdGVkJyAvc3luYyByZXNwb25zZXNcclxuICAgICAgICAvLyBzdWNoIGFzIHdoZW4gbGFwdG9wcyB1bnNsZWVwLlxyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzMzMDcjaXNzdWVjb21tZW50LTI4Mjg5NTU2OFxyXG4gICAgICAgIGNsaS5zZXRDYW5SZXNldFRpbWVsaW5lQ2FsbGJhY2soZnVuY3Rpb24ocm9vbUlkKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVxdWVzdCB0byByZXNldCB0aW1lbGluZSBpbiByb29tIFwiLCByb29tSWQsIFwiIHZpZXdpbmc6XCIsIHNlbGYuc3RhdGUuY3VycmVudFJvb21JZCk7XHJcbiAgICAgICAgICAgIGlmIChyb29tSWQgIT09IHNlbGYuc3RhdGUuY3VycmVudFJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gSXQgaXMgc2FmZSB0byByZW1vdmUgZXZlbnRzIGZyb20gcm9vbXMgd2UgYXJlIG5vdCB2aWV3aW5nLlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gV2UgYXJlIHZpZXdpbmcgdGhlIHJvb20gd2hpY2ggd2Ugd2FudCB0byByZXNldC4gSXQgaXMgb25seSBzYWZlIHRvIGRvXHJcbiAgICAgICAgICAgIC8vIHRoaXMgaWYgd2UgYXJlIG5vdCBzY3JvbGxlZCB1cCBpbiB0aGUgdmlldy4gVG8gZmluZCBvdXQsIGRlbGVnYXRlIHRvXHJcbiAgICAgICAgICAgIC8vIHRoZSB0aW1lbGluZSBwYW5lbC4gSWYgdGhlIHRpbWVsaW5lIHBhbmVsIGRvZXNuJ3QgZXhpc3QsIHRoZW4gd2UgYXNzdW1lXHJcbiAgICAgICAgICAgIC8vIGl0IGlzIHNhZmUgdG8gcmVzZXQgdGhlIHRpbWVsaW5lLlxyXG4gICAgICAgICAgICBpZiAoIXNlbGYuX2xvZ2dlZEluVmlldyB8fCAhc2VsZi5fbG9nZ2VkSW5WaWV3LmNoaWxkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gc2VsZi5fbG9nZ2VkSW5WaWV3LmNoaWxkLmNhblJlc2V0VGltZWxpbmVJblJvb20ocm9vbUlkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2xpLm9uKCdzeW5jJywgZnVuY3Rpb24oc3RhdGUsIHByZXZTdGF0ZSwgZGF0YSkge1xyXG4gICAgICAgICAgICAvLyBMaWZlY3ljbGVTdG9yZSBhbmQgb3RoZXJzIGNhbm5vdCBkaXJlY3RseSBzdWJzY3JpYmUgdG8gbWF0cml4IGNsaWVudCBmb3JcclxuICAgICAgICAgICAgLy8gZXZlbnRzIGJlY2F1c2UgZmx1eCBvbmx5IGFsbG93cyBzdG9yZSBzdGF0ZSBjaGFuZ2VzIGR1cmluZyBmbHV4IGRpc3BhdGNoZXMuXHJcbiAgICAgICAgICAgIC8vIFNvIGRpc3BhdGNoIGRpcmVjdGx5IGZyb20gaGVyZS4gSWRlYWxseSB3ZSdkIHVzZSBhIFN5bmNTdGF0ZVN0b3JlIHRoYXRcclxuICAgICAgICAgICAgLy8gd291bGQgZG8gdGhpcyBkaXNwYXRjaCBhbmQgZXhwb3NlIHRoZSBzeW5jIHN0YXRlIGl0c2VsZiAoYnkgbGlzdGVuaW5nIHRvXHJcbiAgICAgICAgICAgIC8vIGl0cyBvd24gZGlzcGF0Y2gpLlxyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3N5bmNfc3RhdGUnLCBwcmV2U3RhdGUsIHN0YXRlfSk7XHJcblxyXG4gICAgICAgICAgICBpZiAoc3RhdGUgPT09IFwiRVJST1JcIiB8fCBzdGF0ZSA9PT0gXCJSRUNPTk5FQ1RJTkdcIikge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3IgaW5zdGFuY2VvZiBNYXRyaXguSW52YWxpZFN0b3JlRXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBMaWZlY3ljbGUuaGFuZGxlSW52YWxpZFN0b3JlRXJyb3IoZGF0YS5lcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtzeW5jRXJyb3I6IGRhdGEuZXJyb3IgfHwgdHJ1ZX0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHNlbGYuc3RhdGUuc3luY0Vycm9yKSB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtzeW5jRXJyb3I6IG51bGx9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi51cGRhdGVTdGF0dXNJbmRpY2F0b3Ioc3RhdGUsIHByZXZTdGF0ZSk7XHJcbiAgICAgICAgICAgIGlmIChzdGF0ZSA9PT0gXCJTWU5DSU5HXCIgJiYgcHJldlN0YXRlID09PSBcIlNZTkNJTkdcIikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhcIk1hdHJpeENsaWVudCBzeW5jIHN0YXRlID0+ICVzXCIsIHN0YXRlKTtcclxuICAgICAgICAgICAgaWYgKHN0YXRlICE9PSBcIlBSRVBBUkVEXCIpIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgICAgICAgICBzZWxmLmZpcnN0U3luY0NvbXBsZXRlID0gdHJ1ZTtcclxuICAgICAgICAgICAgc2VsZi5maXJzdFN5bmNQcm9taXNlLnJlc29sdmUoKTtcclxuXHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnZm9jdXNfY29tcG9zZXInfSk7XHJcbiAgICAgICAgICAgIHNlbGYuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgcmVhZHk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzaG93Tm90aWZpZXJUb29sYmFyOiBOb3RpZmllci5zaG91bGRTaG93VG9vbGJhcigpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBjbGkub24oJ0NhbGwuaW5jb21pbmcnLCBmdW5jdGlvbihjYWxsKSB7XHJcbiAgICAgICAgICAgIC8vIHdlIGRpc3BhdGNoIHRoaXMgc3luY2hyb25vdXNseSB0byBtYWtlIHN1cmUgdGhhdCB0aGUgZXZlbnRcclxuICAgICAgICAgICAgLy8gaGFuZGxlcnMgb24gdGhlIGNhbGwgYXJlIHNldCB1cCBpbW1lZGlhdGVseSAoc28gdGhhdCBpZlxyXG4gICAgICAgICAgICAvLyB3ZSBnZXQgYW4gaW1tZWRpYXRlIGhhbmd1cCwgd2UgZG9uJ3QgZ2V0IGEgc3R1Y2sgY2FsbClcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2luY29taW5nX2NhbGwnLFxyXG4gICAgICAgICAgICAgICAgY2FsbDogY2FsbCxcclxuICAgICAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY2xpLm9uKCdTZXNzaW9uLmxvZ2dlZF9vdXQnLCBmdW5jdGlvbihlcnJPYmopIHtcclxuICAgICAgICAgICAgaWYgKExpZmVjeWNsZS5pc0xvZ2dpbmdPdXQoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgaWYgKGVyck9iai5odHRwU3RhdHVzID09PSA0MDEgJiYgZXJyT2JqLmRhdGEgJiYgZXJyT2JqLmRhdGFbJ3NvZnRfbG9nb3V0J10pIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcIlNvZnQgbG9nb3V0IGlzc3VlZCBieSBzZXJ2ZXIgLSBhdm9pZGluZyBkYXRhIGRlbGV0aW9uXCIpO1xyXG4gICAgICAgICAgICAgICAgTGlmZWN5Y2xlLnNvZnRMb2dvdXQoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU2lnbmVkIG91dCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdTaWduZWQgT3V0JyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ0ZvciBzZWN1cml0eSwgdGhpcyBzZXNzaW9uIGhhcyBiZWVuIHNpZ25lZCBvdXQuIFBsZWFzZSBzaWduIGluIGFnYWluLicpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2xvZ291dCcsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNsaS5vbignbm9fY29uc2VudCcsIGZ1bmN0aW9uKG1lc3NhZ2UsIGNvbnNlbnRVcmkpIHtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTm8gQ29uc2VudCBEaWFsb2cnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnVGVybXMgYW5kIENvbmRpdGlvbnMnKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPiB7IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1RvIGNvbnRpbnVlIHVzaW5nIHRoZSAlKGhvbWVzZXJ2ZXJEb21haW4pcyBob21lc2VydmVyICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3lvdSBtdXN0IHJldmlldyBhbmQgYWdyZWUgdG8gb3VyIHRlcm1zIGFuZCBjb25kaXRpb25zLicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGhvbWVzZXJ2ZXJEb21haW46IGNsaS5nZXREb21haW4oKSB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICBidXR0b246IF90KCdSZXZpZXcgdGVybXMgYW5kIGNvbmRpdGlvbnMnKSxcclxuICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogX3QoJ0Rpc21pc3MnKSxcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IChjb25maXJtZWQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHduZCA9IHdpbmRvdy5vcGVuKGNvbnNlbnRVcmksICdfYmxhbmsnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd25kLm9wZW5lciA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSwgbnVsbCwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGRmdCA9IG5ldyBEZWNyeXB0aW9uRmFpbHVyZVRyYWNrZXIoKHRvdGFsLCBlcnJvckNvZGUpID0+IHtcclxuICAgICAgICAgICAgQW5hbHl0aWNzLnRyYWNrRXZlbnQoJ0UyRScsICdEZWNyeXB0aW9uIGZhaWx1cmUnLCBlcnJvckNvZGUsIHRvdGFsKTtcclxuICAgICAgICB9LCAoZXJyb3JDb2RlKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIE1hcCBKUy1TREsgZXJyb3IgY29kZXMgdG8gdHJhY2tlciBjb2RlcyBmb3IgYWdncmVnYXRpb25cclxuICAgICAgICAgICAgc3dpdGNoIChlcnJvckNvZGUpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ01FR09MTV9VTktOT1dOX0lOQk9VTkRfU0VTU0lPTl9JRCc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdvbG1fa2V5c19ub3Rfc2VudF9lcnJvcic7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdPTE1fVU5LTk9XTl9NRVNTQUdFX0lOREVYJzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ29sbV9pbmRleF9lcnJvcic7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHVuZGVmaW5lZDpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ3VuZXhwZWN0ZWRfZXJyb3InO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ3Vuc3BlY2lmaWVkX2Vycm9yJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBTaGVsdmVkIGZvciBsYXRlciBkYXRlIHdoZW4gd2UgaGF2ZSB0aW1lIHRvIHRoaW5rIGFib3V0IHBlcnNpc3RpbmcgaGlzdG9yeSBvZlxyXG4gICAgICAgIC8vIHRyYWNrZWQgZXZlbnRzIGFjcm9zcyBzZXNzaW9ucy5cclxuICAgICAgICAvLyBkZnQubG9hZFRyYWNrZWRFdmVudEhhc2hNYXAoKTtcclxuXHJcbiAgICAgICAgZGZ0LnN0YXJ0KCk7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gbG9nZ2luZyBvdXQsIHN0b3AgdHJhY2tpbmcgZmFpbHVyZXMgYW5kIGRlc3Ryb3kgc3RhdGVcclxuICAgICAgICBjbGkub24oXCJTZXNzaW9uLmxvZ2dlZF9vdXRcIiwgKCkgPT4gZGZ0LnN0b3AoKSk7XHJcbiAgICAgICAgY2xpLm9uKFwiRXZlbnQuZGVjcnlwdGVkXCIsIChlLCBlcnIpID0+IGRmdC5ldmVudERlY3J5cHRlZChlLCBlcnIpKTtcclxuXHJcbiAgICAgICAgLy8gVE9ETzogV2UgY2FuIHJlbW92ZSB0aGlzIG9uY2UgY3Jvc3Mtc2lnbmluZyBpcyB0aGUgb25seSB3YXkuXHJcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvMTE5MDhcclxuICAgICAgICBjb25zdCBrcmggPSBuZXcgS2V5UmVxdWVzdEhhbmRsZXIoY2xpKTtcclxuICAgICAgICBjbGkub24oXCJjcnlwdG8ucm9vbUtleVJlcXVlc3RcIiwgKHJlcSkgPT4ge1xyXG4gICAgICAgICAgICBrcmguaGFuZGxlS2V5UmVxdWVzdChyZXEpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNsaS5vbihcImNyeXB0by5yb29tS2V5UmVxdWVzdENhbmNlbGxhdGlvblwiLCAocmVxKSA9PiB7XHJcbiAgICAgICAgICAgIGtyaC5oYW5kbGVLZXlSZXF1ZXN0Q2FuY2VsbGF0aW9uKHJlcSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNsaS5vbihcIlJvb21cIiwgKHJvb20pID0+IHtcclxuICAgICAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0NyeXB0b0VuYWJsZWQoKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYmxhY2tsaXN0RW5hYmxlZCA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWVBdChcclxuICAgICAgICAgICAgICAgICAgICBTZXR0aW5nTGV2ZWwuUk9PTV9ERVZJQ0UsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJibGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb20ucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgIC8qZXhwbGljaXQ9Ki90cnVlLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHJvb20uc2V0QmxhY2tsaXN0VW52ZXJpZmllZERldmljZXMoYmxhY2tsaXN0RW5hYmxlZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBjbGkub24oXCJjcnlwdG8ud2FybmluZ1wiLCAodHlwZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ0NSWVBUT19XQVJOSU5HX09MRF9WRVJTSU9OX0RFVEVDVEVEJzpcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDcnlwdG8gbWlncmF0ZWQnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdPbGQgY3J5cHRvZ3JhcGh5IGRhdGEgZGV0ZWN0ZWQnKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJEYXRhIGZyb20gYW4gb2xkZXIgdmVyc2lvbiBvZiBSaW90IGhhcyBiZWVuIGRldGVjdGVkLiBcIitcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhpcyB3aWxsIGhhdmUgY2F1c2VkIGVuZC10by1lbmQgY3J5cHRvZ3JhcGh5IHRvIG1hbGZ1bmN0aW9uIFwiK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpbiB0aGUgb2xkZXIgdmVyc2lvbi4gRW5kLXRvLWVuZCBlbmNyeXB0ZWQgbWVzc2FnZXMgZXhjaGFuZ2VkIFwiK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJyZWNlbnRseSB3aGlsc3QgdXNpbmcgdGhlIG9sZGVyIHZlcnNpb24gbWF5IG5vdCBiZSBkZWNyeXB0YWJsZSBcIitcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaW4gdGhpcyB2ZXJzaW9uLiBUaGlzIG1heSBhbHNvIGNhdXNlIG1lc3NhZ2VzIGV4Y2hhbmdlZCB3aXRoIHRoaXMgXCIrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInZlcnNpb24gdG8gZmFpbC4gSWYgeW91IGV4cGVyaWVuY2UgcHJvYmxlbXMsIGxvZyBvdXQgYW5kIGJhY2sgaW4gXCIrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFnYWluLiBUbyByZXRhaW4gbWVzc2FnZSBoaXN0b3J5LCBleHBvcnQgYW5kIHJlLWltcG9ydCB5b3VyIGtleXMuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBjbGkub24oXCJjcnlwdG8ua2V5QmFja3VwRmFpbGVkXCIsIGFzeW5jIChlcnJjb2RlKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBoYXZlTmV3VmVyc2lvbjtcclxuICAgICAgICAgICAgbGV0IG5ld1ZlcnNpb25JbmZvO1xyXG4gICAgICAgICAgICAvLyBpZiBrZXkgYmFja3VwIGlzIHN0aWxsIGVuYWJsZWQsIHRoZXJlIG11c3QgYmUgYSBuZXcgYmFja3VwIGluIHBsYWNlXHJcbiAgICAgICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0S2V5QmFja3VwRW5hYmxlZCgpKSB7XHJcbiAgICAgICAgICAgICAgICBoYXZlTmV3VmVyc2lvbiA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBvdGhlcndpc2UgY2hlY2sgdGhlIHNlcnZlciB0byBzZWUgaWYgdGhlcmUncyBhIG5ldyBvbmVcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmV3VmVyc2lvbkluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0S2V5QmFja3VwVmVyc2lvbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChuZXdWZXJzaW9uSW5mbyAhPT0gbnVsbCkgaGF2ZU5ld1ZlcnNpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJTYXcga2V5IGJhY2t1cCBlcnJvciBidXQgZmFpbGVkIHRvIGNoZWNrIGJhY2t1cCB2ZXJzaW9uIVwiLCBlKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChoYXZlTmV3VmVyc2lvbikge1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jKCdOZXcgUmVjb3ZlcnkgTWV0aG9kJywgJ05ldyBSZWNvdmVyeSBNZXRob2QnLFxyXG4gICAgICAgICAgICAgICAgICAgIGltcG9ydCgnLi4vLi4vYXN5bmMtY29tcG9uZW50cy92aWV3cy9kaWFsb2dzL2tleWJhY2t1cC9OZXdSZWNvdmVyeU1ldGhvZERpYWxvZycpLFxyXG4gICAgICAgICAgICAgICAgICAgIHsgbmV3VmVyc2lvbkluZm8gfSxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMoJ1JlY292ZXJ5IE1ldGhvZCBSZW1vdmVkJywgJ1JlY292ZXJ5IE1ldGhvZCBSZW1vdmVkJyxcclxuICAgICAgICAgICAgICAgICAgICBpbXBvcnQoJy4uLy4uL2FzeW5jLWNvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9rZXliYWNrdXAvUmVjb3ZlcnlNZXRob2RSZW1vdmVkRGlhbG9nJyksXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNsaS5vbihcImNyeXB0by5rZXlTaWduYXR1cmVVcGxvYWRGYWlsdXJlXCIsIChmYWlsdXJlcywgc291cmNlLCBjb250aW51YXRpb24pID0+IHtcclxuICAgICAgICAgICAgY29uc3QgS2V5U2lnbmF0dXJlVXBsb2FkRmFpbGVkRGlhbG9nID1cclxuICAgICAgICAgICAgICAgIHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuS2V5U2lnbmF0dXJlVXBsb2FkRmFpbGVkRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICAgICAnRmFpbGVkIHRvIHVwbG9hZCBrZXkgc2lnbmF0dXJlcycsXHJcbiAgICAgICAgICAgICAgICAnRmFpbGVkIHRvIHVwbG9hZCBrZXkgc2lnbmF0dXJlcycsXHJcbiAgICAgICAgICAgICAgICBLZXlTaWduYXR1cmVVcGxvYWRGYWlsZWREaWFsb2csXHJcbiAgICAgICAgICAgICAgICB7IGZhaWx1cmVzLCBzb3VyY2UsIGNvbnRpbnVhdGlvbiB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2xpLm9uKFwiY3J5cHRvLnZlcmlmaWNhdGlvbi5yZXF1ZXN0XCIsIHJlcXVlc3QgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpc0ZsYWdPbiA9IFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKTtcclxuXHJcbiAgICAgICAgICAgIGlmICghaXNGbGFnT24gJiYgIXJlcXVlc3QuY2hhbm5lbC5kZXZpY2VJZCkge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdC5jYW5jZWwoe2NvZGU6IFwibS5pbnZhbGlkX21lc3NhZ2VcIiwgcmVhc29uOiBcIlRoaXMgY2xpZW50IGhhcyBjcm9zcy1zaWduaW5nIGRpc2FibGVkXCJ9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHJlcXVlc3QudmVyaWZpZXIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IEluY29taW5nU2FzRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmRpYWxvZ3MuSW5jb21pbmdTYXNEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdJbmNvbWluZyBWZXJpZmljYXRpb24nLCAnJywgSW5jb21pbmdTYXNEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICB2ZXJpZmllcjogcmVxdWVzdC52ZXJpZmllcixcclxuICAgICAgICAgICAgICAgIH0sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3QucGVuZGluZykge1xyXG4gICAgICAgICAgICAgICAgVG9hc3RTdG9yZS5zaGFyZWRJbnN0YW5jZSgpLmFkZE9yUmVwbGFjZVRvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICBrZXk6ICd2ZXJpZnJlcV8nICsgcmVxdWVzdC5jaGFubmVsLnRyYW5zYWN0aW9uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IHJlcXVlc3QuaXNTZWxmVmVyaWZpY2F0aW9uID8gX3QoXCJTZWxmLXZlcmlmaWNhdGlvbiByZXF1ZXN0XCIpIDogX3QoXCJWZXJpZmljYXRpb24gUmVxdWVzdFwiKSxcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiBcInZlcmlmaWNhdGlvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHByb3BzOiB7cmVxdWVzdH0sXHJcbiAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBzZGsuZ2V0Q29tcG9uZW50KFwidG9hc3RzLlZlcmlmaWNhdGlvblJlcXVlc3RUb2FzdFwiKSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogVG9hc3RTdG9yZS5QUklPUklUWV9SRUFMVElNRSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gRmlyZSB0aGUgdGludGVyIHJpZ2h0IG9uIHN0YXJ0dXAgdG8gZW5zdXJlIHRoZSBkZWZhdWx0IHRoZW1lIGlzIGFwcGxpZWRcclxuICAgICAgICAvLyBBIGxhdGVyIHN5bmMgY2FuL3dpbGwgY29ycmVjdCB0aGUgdGludCB0byBiZSB0aGUgcmlnaHQgdmFsdWUgZm9yIHRoZSB1c2VyXHJcbiAgICAgICAgY29uc3QgY29sb3JTY2hlbWUgPSBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwicm9vbUNvbG9yXCIpO1xyXG4gICAgICAgIFRpbnRlci50aW50KGNvbG9yU2NoZW1lLnByaW1hcnlfY29sb3IsIGNvbG9yU2NoZW1lLnNlY29uZGFyeV9jb2xvcik7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsbGVkIHNob3J0bHkgYWZ0ZXIgdGhlIG1hdHJpeCBjbGllbnQgaGFzIHN0YXJ0ZWQuIFVzZWZ1bCBmb3JcclxuICAgICAqIHNldHRpbmcgdXAgYW55dGhpbmcgdGhhdCByZXF1aXJlcyB0aGUgY2xpZW50IHRvIGJlIHN0YXJ0ZWQuXHJcbiAgICAgKiBAcHJpdmF0ZVxyXG4gICAgICovXHJcbiAgICBfb25DbGllbnRTdGFydGVkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIGlmIChjbGkuaXNDcnlwdG9FbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgY29uc3QgYmxhY2tsaXN0RW5hYmxlZCA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWVBdChcclxuICAgICAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsXHJcbiAgICAgICAgICAgICAgICBcImJsYWNrbGlzdFVudmVyaWZpZWREZXZpY2VzXCIsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNsaS5zZXRHbG9iYWxCbGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlcyhibGFja2xpc3RFbmFibGVkKTtcclxuXHJcbiAgICAgICAgICAgIC8vIFdpdGggY3Jvc3Mtc2lnbmluZyBlbmFibGVkLCB3ZSBzZW5kIHRvIHVua25vd24gZGV2aWNlc1xyXG4gICAgICAgICAgICAvLyB3aXRob3V0IHByb21wdGluZy4gQW55IGJhZC1kZXZpY2Ugc3RhdHVzIHRoZSB1c2VyIHNob3VsZFxyXG4gICAgICAgICAgICAvLyBiZSBhd2FyZSBvZiB3aWxsIGJlIHNpZ25hbGxlZCB0aHJvdWdoIHRoZSByb29tIHNoaWVsZFxyXG4gICAgICAgICAgICAvLyBjaGFuZ2luZyBjb2xvdXIuIE1vcmUgYWR2YW5jZWQgYmVoYXZpb3VyIHdpbGwgY29tZSBvbmNlXHJcbiAgICAgICAgICAgIC8vIHdlIGltcGxlbWVudCBtb3JlIHNldHRpbmdzLlxyXG4gICAgICAgICAgICBjbGkuc2V0R2xvYmFsRXJyb3JPblVua25vd25EZXZpY2VzKFxyXG4gICAgICAgICAgICAgICAgIVNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3dTY3JlZW46IGZ1bmN0aW9uKHNjcmVlbiwgcGFyYW1zKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCItLS0tcGFyYW1zIHNob3cgc2NyZWVuIC0tLS1cIiwgcGFyYW1zKVxyXG4gICAgICAgIGlmIChzY3JlZW4gPT0gJ3JlZ2lzdGVyJykge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc3RhcnRfcmVnaXN0cmF0aW9uJyxcclxuICAgICAgICAgICAgICAgIHBhcmFtczogcGFyYW1zLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PSAnbG9naW4nKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdzdGFydF9sb2dpbicsXHJcbiAgICAgICAgICAgICAgICAvLyBwYXJhbXM6IHBhcmFtcyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzY3JlZW4gPT0gJ2ZvcmdvdF9wYXNzd29yZCcpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3N0YXJ0X3Bhc3N3b3JkX3JlY292ZXJ5JyxcclxuICAgICAgICAgICAgICAgIHBhcmFtczogcGFyYW1zLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PT0gJ3NvZnRfbG9nb3V0Jykge1xyXG4gICAgICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpICYmIE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSAmJiAhTGlmZWN5Y2xlLmlzU29mdExvZ291dCgpKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBMb2dnZWQgaW4gLSB2aXNpdCBhIHJvb21cclxuICAgICAgICAgICAgICAgIHRoaXMuX3ZpZXdMYXN0Um9vbSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gVWx0aW1hdGVseSB0cmlnZ2VycyBzb2Z0X2xvZ291dCBpZiBuZWVkZWRcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnc3RhcnRfbG9naW4nLFxyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczogcGFyYW1zLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PSAnbmV3Jykge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19jcmVhdGVfcm9vbScsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2NyZWVuID09ICdzZXR0aW5ncycpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfdXNlcl9zZXR0aW5ncycsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2NyZWVuID09ICd3ZWxjb21lJykge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld193ZWxjb21lX3BhZ2UnLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PSAnaG9tZScpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfaG9tZV9wYWdlJyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzY3JlZW4gPT0gJ3N0YXJ0Jykge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dTY3JlZW4oJ2hvbWUnKTtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzY3JlZW4gPT0gJ2RpcmVjdG9yeScpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbV9kaXJlY3RvcnknLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PSAnZ3JvdXBzJykge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19teV9ncm91cHMnLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PT0gJ2NvbXBsZXRlX3NlY3VyaXR5Jykge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc3RhcnRfY29tcGxldGVfc2VjdXJpdHknLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjcmVlbiA9PSAncG9zdF9yZWdpc3RyYXRpb24nKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdzdGFydF9wb3N0X3JlZ2lzdHJhdGlvbicsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2NyZWVuLmluZGV4T2YoJ3Jvb20vJykgPT0gMCkge1xyXG4gICAgICAgICAgICAvLyBSb29tcyBjYW4gaGF2ZSB0aGUgZm9sbG93aW5nIGZvcm1hdHM6XHJcbiAgICAgICAgICAgIC8vICNyb29tX2FsaWFzOmRvbWFpbiBvciAhb3BhcXVlX2lkOmRvbWFpblxyXG4gICAgICAgICAgICBjb25zdCByb29tID0gc2NyZWVuLnN1YnN0cmluZyg1KTtcclxuICAgICAgICAgICAgY29uc3QgZG9tYWluT2Zmc2V0ID0gcm9vbS5pbmRleE9mKCc6JykgKyAxOyAvLyAwIGluIGNhc2Ugcm9vbSBkb2VzIG5vdCBjb250YWluIGEgOlxyXG4gICAgICAgICAgICBsZXQgZXZlbnRPZmZzZXQgPSByb29tLmxlbmd0aDtcclxuICAgICAgICAgICAgLy8gcm9vbSBhbGlhc2VzIGNhbiBjb250YWluIHNsYXNoZXMgb25seSBsb29rIGZvciBzbGFzaCBhZnRlciBkb21haW5cclxuICAgICAgICAgICAgaWYgKHJvb20uc3Vic3RyaW5nKGRvbWFpbk9mZnNldCkuaW5kZXhPZignLycpID4gLTEpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50T2Zmc2V0ID0gZG9tYWluT2Zmc2V0ICsgcm9vbS5zdWJzdHJpbmcoZG9tYWluT2Zmc2V0KS5pbmRleE9mKCcvJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3Qgcm9vbVN0cmluZyA9IHJvb20uc3Vic3RyaW5nKDAsIGV2ZW50T2Zmc2V0KTtcclxuICAgICAgICAgICAgbGV0IGV2ZW50SWQgPSByb29tLnN1YnN0cmluZyhldmVudE9mZnNldCArIDEpOyAvLyBlbXB0eSBzdHJpbmcgaWYgbm8gZXZlbnQgaWQgZ2l2ZW5cclxuXHJcbiAgICAgICAgICAgIC8vIFByZXZpb3VzbHkgd2UgcHVsbGVkIHRoZSBldmVudElEIGZyb20gdGhlIHNlZ21lbnRzIGluIHN1Y2ggYSB3YXlcclxuICAgICAgICAgICAgLy8gd2hlcmUgaWYgdGhlcmUgd2FzIG5vIGV2ZW50SWQgdGhlbiB3ZSdkIGdldCB1bmRlZmluZWQuIEhvd2V2ZXIsIHdlXHJcbiAgICAgICAgICAgIC8vIG5vdyBkbyBhIHNwbGljZSBhbmQgam9pbiB0byBoYW5kbGUgdjMgZXZlbnQgSURzIHdoaWNoIHJlc3VsdHMgaW5cclxuICAgICAgICAgICAgLy8gYW4gZW1wdHkgc3RyaW5nLiBUbyBtYWludGFpbiBvdXIgcG90ZW50aWFsIGNvbnRyYWN0IHdpdGggdGhlIHJlc3RcclxuICAgICAgICAgICAgLy8gb2YgdGhlIGFwcCwgd2UgY29lcmNlIHRoZSBldmVudElkIHRvIGJlIHVuZGVmaW5lZCB3aGVyZSBhcHBsaWNhYmxlLlxyXG4gICAgICAgICAgICBpZiAoIWV2ZW50SWQpIGV2ZW50SWQgPSB1bmRlZmluZWQ7XHJcblxyXG4gICAgICAgICAgICAvLyBUT0RPOiBIYW5kbGUgZW5jb2RlZCByb29tL2V2ZW50IElEczogaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvOTE0OVxyXG5cclxuICAgICAgICAgICAgLy8gRklYTUU6IHNvcnRfb3V0IGNhc2VDb25zaXN0ZW5jeVxyXG4gICAgICAgICAgICBjb25zdCB0aGlyZFBhcnR5SW52aXRlID0ge1xyXG4gICAgICAgICAgICAgICAgaW52aXRlU2lnblVybDogcGFyYW1zLnNpZ251cmwsXHJcbiAgICAgICAgICAgICAgICBpbnZpdGVkRW1haWw6IHBhcmFtcy5lbWFpbCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgY29uc3Qgb29iRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHBhcmFtcy5yb29tX25hbWUsXHJcbiAgICAgICAgICAgICAgICBhdmF0YXJVcmw6IHBhcmFtcy5yb29tX2F2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICBpbnZpdGVyTmFtZTogcGFyYW1zLmludml0ZXJfbmFtZSxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIC8vIG9uIG91ciBVUkxzIHRoZXJlIG1pZ2h0IGJlIGEgP3ZpYT1tYXRyaXgub3JnIG9yIHNpbWlsYXIgdG8gaGVscFxyXG4gICAgICAgICAgICAvLyBqb2lucyB0byB0aGUgcm9vbSBzdWNjZWVkLiBXZSdsbCBwYXNzIHRoZXNlIHRocm91Z2ggYXMgYW4gYXJyYXlcclxuICAgICAgICAgICAgLy8gdG8gb3RoZXIgbGV2ZWxzLiBJZiB0aGVyZSdzIGp1c3Qgb25lID92aWE9IHRoZW4gcGFyYW1zLnZpYSBpcyBhXHJcbiAgICAgICAgICAgIC8vIHNpbmdsZSBzdHJpbmcuIElmIHNvbWVvbmUgZG9lcyBzb21ldGhpbmcgbGlrZSA/dmlhPW9uZS5jb20mdmlhPXR3by5jb21cclxuICAgICAgICAgICAgLy8gdGhlbiBwYXJhbXMudmlhIGlzIGFuIGFycmF5IG9mIHN0cmluZ3MuXHJcbiAgICAgICAgICAgIGxldCB2aWEgPSBbXTtcclxuICAgICAgICAgICAgaWYgKHBhcmFtcy52aWEpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YocGFyYW1zLnZpYSkgPT09ICdzdHJpbmcnKSB2aWEgPSBbcGFyYW1zLnZpYV07XHJcbiAgICAgICAgICAgICAgICBlbHNlIHZpYSA9IHBhcmFtcy52aWE7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHBheWxvYWQgPSB7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgZXZlbnRfaWQ6IGV2ZW50SWQsXHJcbiAgICAgICAgICAgICAgICB2aWFfc2VydmVyczogdmlhLFxyXG4gICAgICAgICAgICAgICAgLy8gSWYgYW4gZXZlbnQgSUQgaXMgZ2l2ZW4gaW4gdGhlIFVSTCBoYXNoLCBub3RpZnkgUm9vbVZpZXdTdG9yZSB0byBtYXJrXHJcbiAgICAgICAgICAgICAgICAvLyBpdCBhcyBoaWdobGlnaHRlZCwgd2hpY2ggd2lsbCBwcm9wYWdhdGUgdG8gUm9vbVZpZXcgYW5kIGhpZ2hsaWdodCB0aGVcclxuICAgICAgICAgICAgICAgIC8vIGFzc29jaWF0ZWQgRXZlbnRUaWxlLlxyXG4gICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IEJvb2xlYW4oZXZlbnRJZCksXHJcbiAgICAgICAgICAgICAgICB0aGlyZF9wYXJ0eV9pbnZpdGU6IHRoaXJkUGFydHlJbnZpdGUsXHJcbiAgICAgICAgICAgICAgICBvb2JfZGF0YTogb29iRGF0YSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgaWYgKHJvb21TdHJpbmdbMF0gPT0gJyMnKSB7XHJcbiAgICAgICAgICAgICAgICBwYXlsb2FkLnJvb21fYWxpYXMgPSByb29tU3RyaW5nO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcGF5bG9hZC5yb29tX2lkID0gcm9vbVN0cmluZztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHBheWxvYWQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2NyZWVuLmluZGV4T2YoJ3VzZXIvJykgPT0gMCkge1xyXG4gICAgICAgICAgICBjb25zdCB1c2VySWQgPSBzY3JlZW4uc3Vic3RyaW5nKDUpO1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld191c2VyX2luZm8nLFxyXG4gICAgICAgICAgICAgICAgdXNlcklkOiB1c2VySWQsXHJcbiAgICAgICAgICAgICAgICBzdWJBY3Rpb246IHBhcmFtcy5hY3Rpb24sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2NyZWVuLmluZGV4T2YoJ2dyb3VwLycpID09IDApIHtcclxuICAgICAgICAgICAgY29uc3QgZ3JvdXBJZCA9IHNjcmVlbi5zdWJzdHJpbmcoNik7XHJcblxyXG4gICAgICAgICAgICAvLyBUT0RPOiBDaGVjayB2YWxpZCBncm91cCBJRFxyXG5cclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfZ3JvdXAnLFxyXG4gICAgICAgICAgICAgICAgZ3JvdXBfaWQ6IGdyb3VwSWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhcIklnbm9yaW5nIHNob3dTY3JlZW4gZm9yICclcydcIiwgc2NyZWVuKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG5vdGlmeU5ld1NjcmVlbjogZnVuY3Rpb24oc2NyZWVuKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25OZXdTY3JlZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbk5ld1NjcmVlbihzY3JlZW4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9zZXRQYWdlU3VidGl0bGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BbGlhc0NsaWNrOiBmdW5jdGlvbihldmVudCwgYWxpYXMpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19yb29tJywgcm9vbV9hbGlhczogYWxpYXN9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Vc2VyQ2xpY2s6IGZ1bmN0aW9uKGV2ZW50LCB1c2VySWQpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBjb25zdCBtZW1iZXIgPSBuZXcgTWF0cml4LlJvb21NZW1iZXIobnVsbCwgdXNlcklkKTtcclxuICAgICAgICBpZiAoIW1lbWJlcikgeyByZXR1cm47IH1cclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3VzZXInLFxyXG4gICAgICAgICAgICBtZW1iZXI6IG1lbWJlcixcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Hcm91cENsaWNrOiBmdW5jdGlvbihldmVudCwgZ3JvdXBJZCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd2aWV3X2dyb3VwJywgZ3JvdXBfaWQ6IGdyb3VwSWR9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Mb2dvdXRDbGljazogZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdsb2dvdXQnLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGhhbmRsZVJlc2l6ZTogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGNvbnN0IGhpZGVMaHNUaHJlc2hvbGQgPSAxMDAwO1xyXG4gICAgICAgIGNvbnN0IHNob3dMaHNUaHJlc2hvbGQgPSAxMDAwO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fd2luZG93V2lkdGggPiBoaWRlTGhzVGhyZXNob2xkICYmIHdpbmRvdy5pbm5lcldpZHRoIDw9IGhpZGVMaHNUaHJlc2hvbGQpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAnaGlkZV9sZWZ0X3BhbmVsJyB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3dpbmRvd1dpZHRoIDw9IHNob3dMaHNUaHJlc2hvbGQgJiYgd2luZG93LmlubmVyV2lkdGggPiBzaG93TGhzVGhyZXNob2xkKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ3Nob3dfbGVmdF9wYW5lbCcgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnN0YXRlLnJlc2l6ZU5vdGlmaWVyLm5vdGlmeVdpbmRvd1Jlc2l6ZWQoKTtcclxuICAgICAgICB0aGlzLl93aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZGlzcGF0Y2hUaW1lbGluZVJlc2l6ZSgpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICd0aW1lbGluZV9yZXNpemUnIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21DcmVhdGVkOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246IFwidmlld19yb29tXCIsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHJvb21JZCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25SZWdpc3RlckNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNob3dTY3JlZW4oXCJyZWdpc3RlclwiKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Mb2dpbkNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNob3dTY3JlZW4oXCJsb2dpblwiKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Gb3Jnb3RQYXNzd29yZENsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNob3dTY3JlZW4oXCJmb3Jnb3RfcGFzc3dvcmRcIik7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVnaXN0ZXJGbG93Q29tcGxldGU6IGZ1bmN0aW9uKGNyZWRlbnRpYWxzLCBwYXNzd29yZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9uVXNlckNvbXBsZXRlZExvZ2luRmxvdyhjcmVkZW50aWFscywgcGFzc3dvcmQpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyByZXR1cm5zIGEgcHJvbWlzZSB3aGljaCByZXNvbHZlcyB0byB0aGUgbmV3IE1hdHJpeENsaWVudFxyXG4gICAgb25SZWdpc3RlcmVkOiBmdW5jdGlvbihjcmVkZW50aWFscykge1xyXG4gICAgICAgIHJldHVybiBMaWZlY3ljbGUuc2V0TG9nZ2VkSW4oY3JlZGVudGlhbHMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkZpbmlzaFBvc3RSZWdpc3RyYXRpb246IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIERvbid0IGNvbmZ1c2UgdGhpcyB3aXRoIFwiUGFnZVR5cGVcIiB3aGljaCBpcyB0aGUgbWlkZGxlIHdpbmRvdyB0byBzaG93XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHZpZXc6IFZJRVdTLkxPR0dFRF9JTixcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNob3dTY3JlZW4oXCJzZXR0aW5nc1wiKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25WZXJzaW9uOiBmdW5jdGlvbihjdXJyZW50LCBsYXRlc3QsIHJlbGVhc2VOb3Rlcykge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB2ZXJzaW9uOiBjdXJyZW50LFxyXG4gICAgICAgICAgICBuZXdWZXJzaW9uOiBsYXRlc3QsXHJcbiAgICAgICAgICAgIGhhc05ld1ZlcnNpb246IGN1cnJlbnQgIT09IGxhdGVzdCxcclxuICAgICAgICAgICAgbmV3VmVyc2lvblJlbGVhc2VOb3RlczogcmVsZWFzZU5vdGVzLFxyXG4gICAgICAgICAgICBjaGVja2luZ0ZvclVwZGF0ZTogbnVsbCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TZW5kRXZlbnQ6IGZ1bmN0aW9uKHJvb21JZCwgZXZlbnQpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFjbGkpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdtZXNzYWdlX3NlbmRfZmFpbGVkJ30pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjbGkuc2VuZEV2ZW50KHJvb21JZCwgZXZlbnQuZ2V0VHlwZSgpLCBldmVudC5nZXRDb250ZW50KCkpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ21lc3NhZ2Vfc2VudCd9KTtcclxuICAgICAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnbWVzc2FnZV9zZW5kX2ZhaWxlZCd9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NldFBhZ2VTdWJ0aXRsZTogZnVuY3Rpb24oc3VidGl0bGU9JycpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50Um9vbUlkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbSA9IGNsaWVudCAmJiBjbGllbnQuZ2V0Um9vbSh0aGlzLnN0YXRlLmN1cnJlbnRSb29tSWQpO1xyXG4gICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgc3VidGl0bGUgPSBgJHt0aGlzLnN1YlRpdGxlU3RhdHVzfSB8ICR7IHJvb20ubmFtZSB9ICR7c3VidGl0bGV9YDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHN1YnRpdGxlID0gYCR7dGhpcy5zdWJUaXRsZVN0YXR1c30gJHtzdWJ0aXRsZX1gO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkb2N1bWVudC50aXRsZSA9IGAke1Nka0NvbmZpZy5nZXQoKS5icmFuZCB8fCAnUmlvdCd9ICR7c3VidGl0bGV9YDtcclxuICAgIH0sXHJcblxyXG4gICAgdXBkYXRlU3RhdHVzSW5kaWNhdG9yOiBmdW5jdGlvbihzdGF0ZSwgcHJldlN0YXRlKSB7XHJcbiAgICAgICAgY29uc3Qgbm90aWZDb3VudCA9IGNvdW50Um9vbXNXaXRoTm90aWYoTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb21zKCkpLmNvdW50O1xyXG5cclxuICAgICAgICBpZiAoUGxhdGZvcm1QZWcuZ2V0KCkpIHtcclxuICAgICAgICAgICAgUGxhdGZvcm1QZWcuZ2V0KCkuc2V0RXJyb3JTdGF0dXMoc3RhdGUgPT09ICdFUlJPUicpO1xyXG4gICAgICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5zZXROb3RpZmljYXRpb25Db3VudChub3RpZkNvdW50KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc3ViVGl0bGVTdGF0dXMgPSAnJztcclxuICAgICAgICBpZiAoc3RhdGUgPT09IFwiRVJST1JcIikge1xyXG4gICAgICAgICAgICB0aGlzLnN1YlRpdGxlU3RhdHVzICs9IGBbJHtfdChcIk9mZmxpbmVcIil9XSBgO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobm90aWZDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5zdWJUaXRsZVN0YXR1cyArPSBgWyR7bm90aWZDb3VudH1dYDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3NldFBhZ2VTdWJ0aXRsZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNsb3NlQWxsU2V0dGluZ3MoKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAnY2xvc2Vfc2V0dGluZ3MnIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblNlcnZlckNvbmZpZ0NoYW5nZShjb25maWcpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtzZXJ2ZXJDb25maWc6IGNvbmZpZ30pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfbWFrZVJlZ2lzdHJhdGlvblVybDogZnVuY3Rpb24ocGFyYW1zKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zLnJlZmVycmVyKSB7XHJcbiAgICAgICAgICAgIHBhcmFtcy5yZWZlcnJlciA9IHRoaXMucHJvcHMuc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zLnJlZmVycmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5tYWtlUmVnaXN0cmF0aW9uVXJsKHBhcmFtcyk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jb2xsZWN0TG9nZ2VkSW5WaWV3OiBmdW5jdGlvbihyZWYpIHtcclxuICAgICAgICB0aGlzLl9sb2dnZWRJblZpZXcgPSByZWY7XHJcbiAgICB9LFxyXG5cclxuICAgIGFzeW5jIG9uVXNlckNvbXBsZXRlZExvZ2luRmxvdyhjcmVkZW50aWFscywgcGFzc3dvcmQpIHtcclxuICAgICAgICB0aGlzLl9hY2NvdW50UGFzc3dvcmQgPSBwYXNzd29yZDtcclxuICAgICAgICAvLyBzZWxmLWRlc3RydWN0IHRoZSBwYXNzd29yZCBhZnRlciA1bWluc1xyXG4gICAgICAgIGlmICh0aGlzLl9hY2NvdW50UGFzc3dvcmRUaW1lciAhPT0gbnVsbCkgY2xlYXJUaW1lb3V0KHRoaXMuX2FjY291bnRQYXNzd29yZFRpbWVyKTtcclxuICAgICAgICB0aGlzLl9hY2NvdW50UGFzc3dvcmRUaW1lciA9IHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9hY2NvdW50UGFzc3dvcmQgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLl9hY2NvdW50UGFzc3dvcmRUaW1lciA9IG51bGw7XHJcbiAgICAgICAgfSwgNjAgKiA1ICogMTAwMCk7XHJcblxyXG4gICAgICAgIC8vIFdhaXQgZm9yIHRoZSBjbGllbnQgdG8gYmUgbG9nZ2VkIGluIChidXQgbm90IHN0YXJ0ZWQpXHJcbiAgICAgICAgLy8gd2hpY2ggaXMgZW5vdWdoIHRvIGFzayB0aGUgc2VydmVyIGFib3V0IGFjY291bnQgZGF0YS5cclxuICAgICAgICBjb25zdCBsb2dnZWRJbiA9IG5ldyBQcm9taXNlKHJlc29sdmUgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBhY3Rpb25IYW5kbGVyUmVmID0gZGlzLnJlZ2lzdGVyKHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHBheWxvYWQuYWN0aW9uICE9PSBcIm9uX2xvZ2dlZF9pblwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZGlzLnVucmVnaXN0ZXIoYWN0aW9uSGFuZGxlclJlZik7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBDcmVhdGUgYW5kIHN0YXJ0IHRoZSBjbGllbnQgaW4gdGhlIGJhY2tncm91bmRcclxuICAgICAgICBjb25zdCBzZXRMb2dnZWRJblByb21pc2UgPSBMaWZlY3ljbGUuc2V0TG9nZ2VkSW4oY3JlZGVudGlhbHMpO1xyXG4gICAgICAgIGF3YWl0IGxvZ2dlZEluO1xyXG5cclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgLy8gV2UncmUgY2hlY2tpbmcgYGlzQ3J5cHRvQXZhaWxhYmxlYCBoZXJlIGluc3RlYWQgb2YgYGlzQ3J5cHRvRW5hYmxlZGBcclxuICAgICAgICAvLyBiZWNhdXNlIHRoZSBjbGllbnQgaGFzbid0IGJlZW4gc3RhcnRlZCB5ZXQuXHJcbiAgICAgICAgaWYgKCFpc0NyeXB0b0F2YWlsYWJsZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uTG9nZ2VkSW4oKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRlc3QgZm9yIHRoZSBtYXN0ZXIgY3Jvc3Mtc2lnbmluZyBrZXkgaW4gU1NTUyBhcyBhIHF1aWNrIHByb3h5IGZvclxyXG4gICAgICAgIC8vIHdoZXRoZXIgY3Jvc3Mtc2lnbmluZyBoYXMgYmVlbiBzZXQgdXAgb24gdGhlIGFjY291bnQuXHJcbiAgICAgICAgbGV0IG1hc3RlcktleUluU3RvcmFnZSA9IGZhbHNlO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIG1hc3RlcktleUluU3RvcmFnZSA9ICEhYXdhaXQgY2xpLmdldEFjY291bnREYXRhRnJvbVNlcnZlcihcIm0uY3Jvc3Nfc2lnbmluZy5tYXN0ZXJcIik7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBpZiAoZS5lcnJjb2RlICE9PSBcIk1fTk9UX0ZPVU5EXCIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcIlNlY3JldCBzdG9yYWdlIGFjY291bnQgZGF0YSBjaGVjayBmYWlsZWRcIiwgZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChtYXN0ZXJLZXlJblN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgLy8gQXV0by1lbmFibGUgY3Jvc3Mtc2lnbmluZyBmb3IgdGhlIG5ldyBzZXNzaW9uIHdoZW4ga2V5IGZvdW5kIGluXHJcbiAgICAgICAgICAgIC8vIHNlY3JldCBzdG9yYWdlLlxyXG4gICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldEZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIsIHRydWUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlRm9yTmV3Vmlldyh7IHZpZXc6IFZJRVdTLkNPTVBMRVRFX1NFQ1VSSVRZIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSAmJlxyXG4gICAgICAgICAgICBhd2FpdCBjbGkuZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUoXCJvcmcubWF0cml4LmUyZV9jcm9zc19zaWduaW5nXCIpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIC8vIFRoaXMgd2lsbCBvbmx5IHdvcmsgaWYgdGhlIGZlYXR1cmUgaXMgc2V0IHRvICdlbmFibGUnIGluIHRoZSBjb25maWcsXHJcbiAgICAgICAgICAgIC8vIHNpbmNlIGl0J3MgdG9vIGVhcmx5IGluIHRoZSBsaWZlY3ljbGUgZm9yIHVzZXJzIHRvIGhhdmUgdHVybmVkIHRoZVxyXG4gICAgICAgICAgICAvLyBsYWJzIGZsYWcgb24uXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGVGb3JOZXdWaWV3KHsgdmlldzogVklFV1MuRTJFX1NFVFVQIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uTG9nZ2VkSW4oKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBzZXRMb2dnZWRJblByb21pc2U7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGNvbXBsZXRlIHNlY3VyaXR5IC8gZTJlIHNldHVwIGhhcyBmaW5pc2hlZFxyXG4gICAgb25Db21wbGV0ZVNlY3VyaXR5RTJlU2V0dXBGaW5pc2hlZCgpIHtcclxuICAgICAgICB0aGlzLl9vbkxvZ2dlZEluKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCItLS0tLW1hdHJpeENoYXQtLS0tXCIsIHRoaXMucHJvcHMpXHJcbiAgICAgICAgaWYodGhpcy5wcm9wcy5zdGFydGluZ0ZyYWdtZW50UXVlcnlQYXJhbXMpIHtcclxuICAgICAgICAgICAgY29uc3Qge214X2RldmljZV9pZCwgYWNjZXNzX3Rva2VuLCBteF91c2VyX2lkLG14X3Nlc3Npb259ID0gdGhpcy5wcm9wcy5zdGFydGluZ0ZyYWdtZW50UXVlcnlQYXJhbXM7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdteF9kZXZpY2VfaWQnLCBteF9kZXZpY2VfaWQpXHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdteF9oc191cmwnLCAnaHR0cHM6Ly9tYXRyaXgucGl5YXJzZS5jb20nKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ214X2FjY2Vzc190b2tlbicsIGFjY2Vzc190b2tlbik7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdteF91c2VyX2lkJywgbXhfdXNlcl9pZCk7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdteF9jcnlwdG9faW5pdGlhbGlzZWQnLCB0cnVlKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ214X2lzX2d1ZXN0JywgZmFsc2UpO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbXhfc2Vzc2lvbicsIG14X3Nlc3Npb24pO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbXhfaXNfdXJsJywgJ2h0dHBzOi8vdmVjdG9yLmltJyk7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdteGpzc2RrX21lbW9yeV9maWx0ZXJfRklMVEVSX1NZTkNfQGhhbXNoaWlraGFuOm1hdHJpeC5waXlhcnNlLmNvbScsIDApO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbXhfbGFzdF9yb29tX2lkJywgJyFZUEh0WlJ3Y2Jpckp0VFplaU86bWF0cml4LnBpeWFyc2UuY29tJylcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ214X2FjY2VwdHNfdW5zdXBwb3J0ZWRfYnJvd3NlcicsIHRydWUpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGBSZW5kZXJpbmcgTWF0cml4Q2hhdCB3aXRoIHZpZXcgJHt0aGlzLnN0YXRlLnZpZXd9YCk7XHJcblxyXG4gICAgICAgIGxldCB2aWV3O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS52aWV3ID09PSBWSUVXUy5MT0FESU5HKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5TcGlubmVyJyk7XHJcbiAgICAgICAgICAgIHZpZXcgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01hdHJpeENoYXRfc3BsYXNoXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNwaW5uZXIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52aWV3ID09PSBWSUVXUy5DT01QTEVURV9TRUNVUklUWSkge1xyXG4gICAgICAgICAgICBjb25zdCBDb21wbGV0ZVNlY3VyaXR5ID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5hdXRoLkNvbXBsZXRlU2VjdXJpdHknKTtcclxuICAgICAgICAgICAgdmlldyA9IChcclxuICAgICAgICAgICAgICAgIDxDb21wbGV0ZVNlY3VyaXR5XHJcbiAgICAgICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5vbkNvbXBsZXRlU2VjdXJpdHlFMmVTZXR1cEZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUudmlldyA9PT0gVklFV1MuRTJFX1NFVFVQKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEUyZVNldHVwID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5hdXRoLkUyZVNldHVwJyk7XHJcbiAgICAgICAgICAgIHZpZXcgPSAoXHJcbiAgICAgICAgICAgICAgICA8RTJlU2V0dXBcclxuICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLm9uQ29tcGxldGVTZWN1cml0eUUyZVNldHVwRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjb3VudFBhc3N3b3JkPXt0aGlzLl9hY2NvdW50UGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52aWV3ID09PSBWSUVXUy5QT1NUX1JFR0lTVFJBVElPTikge1xyXG4gICAgICAgICAgICAvLyBuZWVkcyB0byBiZSBiZWZvcmUgbm9ybWFsIFBhZ2VUeXBlcyBhcyB5b3UgYXJlIGxvZ2dlZCBpbiB0ZWNobmljYWxseVxyXG4gICAgICAgICAgICBjb25zdCBQb3N0UmVnaXN0cmF0aW9uID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5hdXRoLlBvc3RSZWdpc3RyYXRpb24nKTtcclxuICAgICAgICAgICAgdmlldyA9IChcclxuICAgICAgICAgICAgICAgIDxQb3N0UmVnaXN0cmF0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZT17dGhpcy5vbkZpbmlzaFBvc3RSZWdpc3RyYXRpb259IC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnZpZXcgPT09IFZJRVdTLkxPR0dFRF9JTikge1xyXG4gICAgICAgICAgICAvLyBzdG9yZSBlcnJvcnMgc3RvcCB0aGUgY2xpZW50IHN5bmNpbmcgYW5kIHJlcXVpcmUgdXNlciBpbnRlcnZlbnRpb24sIHNvIHdlJ2xsXHJcbiAgICAgICAgICAgIC8vIGJlIHNob3dpbmcgYSBkaWFsb2cuIERvbid0IHNob3cgYW55dGhpbmcgZWxzZS5cclxuICAgICAgICAgICAgY29uc3QgaXNTdG9yZUVycm9yID0gdGhpcy5zdGF0ZS5zeW5jRXJyb3IgJiYgdGhpcy5zdGF0ZS5zeW5jRXJyb3IgaW5zdGFuY2VvZiBNYXRyaXguSW52YWxpZFN0b3JlRXJyb3I7XHJcblxyXG4gICAgICAgICAgICAvLyBgcmVhZHlgIGFuZCBgdmlldz09TE9HR0VEX0lOYCBtYXkgYmUgc2V0IGJlZm9yZSBgcGFnZV90eXBlYCAoYmVjYXVzZSB0aGVcclxuICAgICAgICAgICAgLy8gbGF0dGVyIGlzIHNldCB2aWEgdGhlIGRpc3BhdGNoZXIpLiBJZiB3ZSBkb24ndCB5ZXQgaGF2ZSBhIGBwYWdlX3R5cGVgLFxyXG4gICAgICAgICAgICAvLyBrZWVwIHNob3dpbmcgdGhlIHNwaW5uZXIgZm9yIG5vdy5cclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVhZHkgJiYgdGhpcy5zdGF0ZS5wYWdlX3R5cGUgJiYgIWlzU3RvcmVFcnJvcikge1xyXG4gICAgICAgICAgICAgICAgLyogZm9yIG5vdywgd2Ugc3R1ZmYgdGhlIGVudGlyZXR5IG9mIG91ciBwcm9wcyBhbmQgc3RhdGUgaW50byB0aGUgTG9nZ2VkSW5WaWV3LlxyXG4gICAgICAgICAgICAgICAgICogd2Ugc2hvdWxkIGdvIHRocm91Z2ggYW5kIGZpZ3VyZSBvdXQgd2hhdCB3ZSBhY3R1YWxseSBuZWVkIHRvIHBhc3MgZG93biwgYXMgd2VsbFxyXG4gICAgICAgICAgICAgICAgICogYXMgdXNpbmcgc29tZXRoaW5nIGxpa2UgcmVkdXggdG8gYXZvaWQgaGF2aW5nIGEgYmlsbGlvbiBiaXRzIG9mIHN0YXRlIGtpY2tpbmcgYXJvdW5kLlxyXG4gICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICBjb25zdCBMb2dnZWRJblZpZXcgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLkxvZ2dlZEluVmlldycpO1xyXG4gICAgICAgICAgICAgICAgdmlldyA9IChcclxuICAgICAgICAgICAgICAgICAgICA8TG9nZ2VkSW5WaWV3IHJlZj17dGhpcy5fY29sbGVjdExvZ2dlZEluVmlld30gbWF0cml4Q2xpZW50PXtNYXRyaXhDbGllbnRQZWcuZ2V0KCl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uUm9vbUNyZWF0ZWQ9e3RoaXMub25Sb29tQ3JlYXRlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbG9zZUFsbFNldHRpbmdzPXt0aGlzLm9uQ2xvc2VBbGxTZXR0aW5nc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25SZWdpc3RlcmVkPXt0aGlzLm9uUmVnaXN0ZXJlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudFJvb21JZD17dGhpcy5zdGF0ZS5jdXJyZW50Um9vbUlkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaG93Q29va2llQmFyPXt0aGlzLnN0YXRlLnNob3dDb29raWVCYXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsuLi50aGlzLnByb3BzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Li4udGhpcy5zdGF0ZX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIHdlIHRoaW5rIHdlIGFyZSBsb2dnZWQgaW4sIGJ1dCBhcmUgc3RpbGwgd2FpdGluZyBmb3IgdGhlIC9zeW5jIHRvIGNvbXBsZXRlXHJcbiAgICAgICAgICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuU3Bpbm5lcicpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGVycm9yQm94O1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuc3luY0Vycm9yICYmICFpc1N0b3JlRXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvckJveCA9IDxkaXYgY2xhc3NOYW1lPVwibXhfTWF0cml4Q2hhdF9zeW5jRXJyb3JcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge21lc3NhZ2VGb3JTeW5jRXJyb3IodGhpcy5zdGF0ZS5zeW5jRXJyb3IpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZpZXcgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NYXRyaXhDaGF0X3NwbGFzaFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7ZXJyb3JCb3h9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxTcGlubmVyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwibXhfTWF0cml4Q2hhdF9zcGxhc2hCdXR0b25zXCIgb25DbGljaz17dGhpcy5vbkxvZ291dENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdCgnTG9nb3V0Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUudmlldyA9PT0gVklFV1MuV0VMQ09NRSkge1xyXG4gICAgICAgICAgICBjb25zdCBXZWxjb21lID0gc2RrLmdldENvbXBvbmVudCgnYXV0aC5XZWxjb21lJyk7XHJcbiAgICAgICAgICAgIHZpZXcgPSA8V2VsY29tZSB7Li4udGhpcy5nZXRTZXJ2ZXJQcm9wZXJ0aWVzKCl9IC8+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52aWV3ID09PSBWSUVXUy5SRUdJU1RFUikge1xyXG4gICAgICAgICAgICBjb25zdCBSZWdpc3RyYXRpb24gPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLmF1dGguUmVnaXN0cmF0aW9uJyk7XHJcbiAgICAgICAgICAgIHZpZXcgPSAoXHJcbiAgICAgICAgICAgICAgICA8UmVnaXN0cmF0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgY2xpZW50U2VjcmV0PXt0aGlzLnN0YXRlLnJlZ2lzdGVyX2NsaWVudF9zZWNyZXR9XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvbklkPXt0aGlzLnN0YXRlLnJlZ2lzdGVyX3Nlc3Npb25faWR9XHJcbiAgICAgICAgICAgICAgICAgICAgaWRTaWQ9e3RoaXMuc3RhdGUucmVnaXN0ZXJfaWRfc2lkfVxyXG4gICAgICAgICAgICAgICAgICAgIGVtYWlsPXt0aGlzLnByb3BzLnN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcy5lbWFpbH1cclxuICAgICAgICAgICAgICAgICAgICBicmFuZD17dGhpcy5wcm9wcy5jb25maWcuYnJhbmR9XHJcbiAgICAgICAgICAgICAgICAgICAgbWFrZVJlZ2lzdHJhdGlvblVybD17dGhpcy5fbWFrZVJlZ2lzdHJhdGlvblVybH1cclxuICAgICAgICAgICAgICAgICAgICBvbkxvZ2dlZEluPXt0aGlzLm9uUmVnaXN0ZXJGbG93Q29tcGxldGV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Mb2dpbkNsaWNrPXt0aGlzLm9uTG9naW5DbGlja31cclxuICAgICAgICAgICAgICAgICAgICBvblNlcnZlckNvbmZpZ0NoYW5nZT17dGhpcy5vblNlcnZlckNvbmZpZ0NoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWU9e3RoaXMucHJvcHMuZGVmYXVsdERldmljZURpc3BsYXlOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgIHsuLi50aGlzLmdldFNlcnZlclByb3BlcnRpZXMoKX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnZpZXcgPT09IFZJRVdTLkZPUkdPVF9QQVNTV09SRCkge1xyXG4gICAgICAgICAgICBjb25zdCBGb3Jnb3RQYXNzd29yZCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuYXV0aC5Gb3Jnb3RQYXNzd29yZCcpO1xyXG4gICAgICAgICAgICB2aWV3ID0gKFxyXG4gICAgICAgICAgICAgICAgPEZvcmdvdFBhc3N3b3JkXHJcbiAgICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZT17dGhpcy5vbkxvZ2luQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Mb2dpbkNsaWNrPXt0aGlzLm9uTG9naW5DbGlja31cclxuICAgICAgICAgICAgICAgICAgICBvblNlcnZlckNvbmZpZ0NoYW5nZT17dGhpcy5vblNlcnZlckNvbmZpZ0NoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICB7Li4udGhpcy5nZXRTZXJ2ZXJQcm9wZXJ0aWVzKCl9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52aWV3ID09PSBWSUVXUy5MT0dJTikge1xyXG4gICAgICAgICAgICBjb25zdCBMb2dpbiA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuYXV0aC5Mb2dpbicpO1xyXG4gICAgICAgICAgICB2aWV3ID0gKFxyXG4gICAgICAgICAgICAgICAgPExvZ2luXHJcbiAgICAgICAgICAgICAgICAgICAgb25Mb2dnZWRJbj17dGhpcy5vblVzZXJDb21wbGV0ZWRMb2dpbkZsb3d9XHJcbiAgICAgICAgICAgICAgICAgICAgb25SZWdpc3RlckNsaWNrPXt0aGlzLm9uUmVnaXN0ZXJDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBmYWxsYmFja0hzVXJsPXt0aGlzLmdldEZhbGxiYWNrSHNVcmwoKX1cclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWU9e3RoaXMucHJvcHMuZGVmYXVsdERldmljZURpc3BsYXlOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uRm9yZ290UGFzc3dvcmRDbGljaz17dGhpcy5vbkZvcmdvdFBhc3N3b3JkQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgb25TZXJ2ZXJDb25maWdDaGFuZ2U9e3RoaXMub25TZXJ2ZXJDb25maWdDaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zPSB7dGhpcy5wcm9wcy5zdGFydGluZ0ZyYWdtZW50UXVlcnlQYXJhbXN9XHJcbiAgICAgICAgICAgICAgICAgICAgey4uLnRoaXMuZ2V0U2VydmVyUHJvcGVydGllcygpfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUudmlldyA9PT0gVklFV1MuU09GVF9MT0dPVVQpIHtcclxuICAgICAgICAgICAgY29uc3QgU29mdExvZ291dCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuYXV0aC5Tb2Z0TG9nb3V0Jyk7XHJcbiAgICAgICAgICAgIHZpZXcgPSAoXHJcbiAgICAgICAgICAgICAgICA8U29mdExvZ291dFxyXG4gICAgICAgICAgICAgICAgICAgIHJlYWxRdWVyeVBhcmFtcz17dGhpcy5wcm9wcy5yZWFsUXVlcnlQYXJhbXN9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Ub2tlbkxvZ2luQ29tcGxldGVkPXt0aGlzLnByb3BzLm9uVG9rZW5Mb2dpbkNvbXBsZXRlZH1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgVW5rbm93biB2aWV3ICR7dGhpcy5zdGF0ZS52aWV3fWApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgRXJyb3JCb3VuZGFyeSA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkVycm9yQm91bmRhcnknKTtcclxuICAgICAgICByZXR1cm4gPEVycm9yQm91bmRhcnk+XHJcbiAgICAgICAgICAgIHt2aWV3fVxyXG4gICAgICAgIDwvRXJyb3JCb3VuZGFyeT47XHJcbiAgICB9LFxyXG59KTtcclxuIl19