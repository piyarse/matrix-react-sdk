"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _Login = _interopRequireDefault(require("../../../Login"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _ErrorUtils = require("../../../utils/ErrorUtils");

var _AutoDiscoveryUtils = _interopRequireWildcard(require("../../../utils/AutoDiscoveryUtils"));

var _classnames = _interopRequireDefault(require("classnames"));

var _AuthPage = _interopRequireDefault(require("../../views/auth/AuthPage"));

var _SSOButton = _interopRequireDefault(require("../../views/elements/SSOButton"));

var _PlatformPeg = _interopRequireDefault(require("../../../PlatformPeg"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// For validating phone numbers without country codes
const PHONE_NUMBER_REGEX = /^[0-9()\-\s]*$/; // Phases
// Show controls to configure server details

const PHASE_SERVER_DETAILS = 0; // Show the appropriate login flow(s) for the server

const PHASE_LOGIN = 1; // Enable phases for login

const PHASES_ENABLED = true; // These are used in several places, and come from the js-sdk's autodiscovery
// stuff. We define them here so that they'll be picked up by i18n.

(0, _languageHandler._td)("Invalid homeserver discovery response");
(0, _languageHandler._td)("Failed to get autodiscovery configuration from server");
(0, _languageHandler._td)("Invalid base_url for m.homeserver");
(0, _languageHandler._td)("Homeserver URL does not appear to be a valid Matrix homeserver");
(0, _languageHandler._td)("Invalid identity server discovery response");
(0, _languageHandler._td)("Invalid base_url for m.identity_server");
(0, _languageHandler._td)("Identity server URL does not appear to be a valid identity server");
(0, _languageHandler._td)("General failure");
/**
 * A wire component which glues together login UI components and Login logic
 */

var _default = (0, _createReactClass.default)({
  displayName: 'Login',
  propTypes: {
    // Called when the user has logged in. Params:
    // - The object returned by the login API
    // - The user's password, if applicable, (may be cached in memory for a
    //   short time so the user is not required to re-enter their password
    //   for operations like uploading cross-signing keys).
    onLoggedIn: _propTypes.default.func.isRequired,
    // If true, the component will consider itself busy.
    busy: _propTypes.default.bool,
    // Secondary HS which we try to log into if the user is using
    // the default HS but login fails. Useful for migrating to a
    // different homeserver without confusing users.
    fallbackHsUrl: _propTypes.default.string,
    defaultDeviceDisplayName: _propTypes.default.string,
    startingFragmentQueryParams: _propTypes.default.object,
    // login shouldn't know or care how registration, password recovery,
    // etc is done.
    onRegisterClick: _propTypes.default.func.isRequired,
    onForgotPasswordClick: _propTypes.default.func,
    onServerConfigChange: _propTypes.default.func.isRequired,
    serverConfig: _propTypes.default.instanceOf(_AutoDiscoveryUtils.ValidatedServerConfig).isRequired
  },
  getInitialState: function () {
    return {
      busy: false,
      errorText: null,
      loginIncorrect: false,
      canTryLogin: true,
      // can we attempt to log in or are there validation errors?
      // used for preserving form values when changing homeserver
      username: "",
      phoneCountry: null,
      phoneNumber: "",
      // Phase of the overall login dialog.
      phase: PHASE_LOGIN,
      // The current login flow, such as password, SSO, etc.
      currentFlow: null,
      // we need to load the flows from the server
      // We perform liveliness checks later, but for now suppress the errors.
      // We also track the server dead errors independently of the regular errors so
      // that we can render it differently, and override any other error the user may
      // be seeing.
      serverIsAlive: true,
      serverErrorIsFatal: false,
      serverDeadError: ""
    };
  },
  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    this._unmounted = false; // map from login step type to a function which will render a control
    // letting you do that login type

    this._stepRendererMap = {
      'm.login.password': this._renderPasswordStep,
      // CAS and SSO are the same thing, modulo the url we link to
      'm.login.cas': () => this._renderSsoStep("cas"),
      'm.login.sso': () => this._renderSsoStep("sso")
    };

    this._initLoginLogic();
  },
  componentWillUnmount: function () {
    this._unmounted = true;
  },

  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps(newProps) {
    console.log("---this.props---", this.props);
    if (newProps.serverConfig.hsUrl === this.props.serverConfig.hsUrl && newProps.serverConfig.isUrl === this.props.serverConfig.isUrl) return; // Ensure that we end up actually logging in to the right place

    this._initLoginLogic(newProps.serverConfig.hsUrl, newProps.serverConfig.isUrl);
  },

  onPasswordLoginError: function (errorText) {
    this.setState({
      errorText,
      loginIncorrect: Boolean(errorText)
    });
  },
  isBusy: function () {
    return this.state.busy || this.props.busy;
  },
  onPasswordLogin: async function (username, phoneCountry, phoneNumber, password) {
    if (!this.state.serverIsAlive) {
      this.setState({
        busy: true
      }); // Do a quick liveliness check on the URLs

      let aliveAgain = true;

      try {
        await _AutoDiscoveryUtils.default.validateServerConfigWithStaticUrls(this.props.serverConfig.hsUrl, this.props.serverConfig.isUrl);
        this.setState({
          serverIsAlive: true,
          errorText: ""
        });
      } catch (e) {
        const componentState = _AutoDiscoveryUtils.default.authComponentStateForError(e);

        this.setState(_objectSpread({
          busy: false
        }, componentState));
        aliveAgain = !componentState.serverErrorIsFatal;
      } // Prevent people from submitting their password when something isn't right.


      if (!aliveAgain) {
        return;
      }
    }

    this.setState({
      busy: true,
      errorText: null,
      loginIncorrect: false
    });

    this._loginLogic.loginViaPassword(username, phoneCountry, phoneNumber, password).then(data => {
      this.setState({
        serverIsAlive: true
      }); // it must be, we logged in.

      this.props.onLoggedIn(data, password);
    }, error => {
      if (this._unmounted) {
        return;
      }

      let errorText; // Some error strings only apply for logging in

      const usingEmail = username.indexOf("@") > 0;

      if (error.httpStatus === 400 && usingEmail) {
        errorText = (0, _languageHandler._t)('This homeserver does not support login using email address.');
      } else if (error.errcode === 'M_RESOURCE_LIMIT_EXCEEDED') {
        const errorTop = (0, _ErrorUtils.messageForResourceLimitError)(error.data.limit_type, error.data.admin_contact, {
          'monthly_active_user': (0, _languageHandler._td)("This homeserver has hit its Monthly Active User limit."),
          '': (0, _languageHandler._td)("This homeserver has exceeded one of its resource limits.")
        });
        const errorDetail = (0, _ErrorUtils.messageForResourceLimitError)(error.data.limit_type, error.data.admin_contact, {
          '': (0, _languageHandler._td)("Please <a>contact your service administrator</a> to continue using this service.")
        });
        errorText = _react.default.createElement("div", null, _react.default.createElement("div", null, errorTop), _react.default.createElement("div", {
          className: "mx_Login_smallError"
        }, errorDetail));
      } else if (error.httpStatus === 401 || error.httpStatus === 403) {
        if (error.errcode === 'M_USER_DEACTIVATED') {
          errorText = (0, _languageHandler._t)('This account has been deactivated.');
        } else if (_SdkConfig.default.get()['disable_custom_urls']) {
          errorText = _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)('Incorrect username and/or password.')), _react.default.createElement("div", {
            className: "mx_Login_smallError"
          }, (0, _languageHandler._t)('Please note you are logging into the %(hs)s server, not matrix.org.', {
            hs: this.props.serverConfig.hsName
          })));
        } else {
          errorText = (0, _languageHandler._t)('Incorrect username and/or password.');
        }
      } else {
        // other errors, not specific to doing a password login
        errorText = this._errorTextFromError(error);
      }

      this.setState({
        busy: false,
        errorText: errorText,
        // 401 would be the sensible status code for 'incorrect password'
        // but the login API gives a 403 https://matrix.org/jira/browse/SYN-744
        // mentions this (although the bug is for UI auth which is not this)
        // We treat both as an incorrect password
        loginIncorrect: error.httpStatus === 401 || error.httpStatus === 403
      });
    });
  },
  onUsernameChanged: function (username) {
    this.setState({
      username: username
    });
  },
  onUsernameBlur: async function (username) {
    const doWellknownLookup = username[0] === "@";
    this.setState({
      username: username,
      busy: doWellknownLookup,
      errorText: null,
      canTryLogin: true
    });

    if (doWellknownLookup) {
      const serverName = username.split(':').slice(1).join(':');

      try {
        const result = await _AutoDiscoveryUtils.default.validateServerName(serverName);
        this.props.onServerConfigChange(result); // We'd like to rely on new props coming in via `onServerConfigChange`
        // so that we know the servers have definitely updated before clearing
        // the busy state. In the case of a full MXID that resolves to the same
        // HS as Riot's default HS though, there may not be any server change.
        // To avoid this trap, we clear busy here. For cases where the server
        // actually has changed, `_initLoginLogic` will be called and manages
        // busy state for its own liveness check.

        this.setState({
          busy: false
        });
      } catch (e) {
        console.error("Problem parsing URL or unhandled error doing .well-known discovery:", e);
        let message = (0, _languageHandler._t)("Failed to perform homeserver discovery");

        if (e.translatedMessage) {
          message = e.translatedMessage;
        }

        let errorText = message;
        let discoveryState = {};

        if (_AutoDiscoveryUtils.default.isLivelinessError(e)) {
          errorText = this.state.errorText;
          discoveryState = _AutoDiscoveryUtils.default.authComponentStateForError(e);
        }

        this.setState(_objectSpread({
          busy: false,
          errorText
        }, discoveryState));
      }
    }
  },
  onPhoneCountryChanged: function (phoneCountry) {
    this.setState({
      phoneCountry: phoneCountry
    });
  },
  onPhoneNumberChanged: function (phoneNumber) {
    this.setState({
      phoneNumber: phoneNumber
    });
  },
  onPhoneNumberBlur: function (phoneNumber) {
    // Validate the phone number entered
    if (!PHONE_NUMBER_REGEX.test(phoneNumber)) {
      this.setState({
        errorText: (0, _languageHandler._t)('The phone number entered looks invalid'),
        canTryLogin: false
      });
    } else {
      this.setState({
        errorText: null,
        canTryLogin: true
      });
    }
  },
  onRegisterClick: function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.props.onRegisterClick();
  },
  onTryRegisterClick: function (ev) {
    const step = this._getCurrentFlowStep();

    if (step === 'm.login.sso' || step === 'm.login.cas') {
      // If we're showing SSO it means that registration is also probably disabled,
      // so intercept the click and instead pretend the user clicked 'Sign in with SSO'.
      ev.preventDefault();
      ev.stopPropagation();
      const ssoKind = step === 'm.login.sso' ? 'sso' : 'cas';

      _PlatformPeg.default.get().startSingleSignOn(this._loginLogic.createTemporaryClient(), ssoKind);
    } else {
      // Don't intercept - just go through to the register page
      this.onRegisterClick(ev);
    }
  },

  async onServerDetailsNextPhaseClick() {
    this.setState({
      phase: PHASE_LOGIN
    });
  },

  onEditServerDetailsClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.setState({
      phase: PHASE_SERVER_DETAILS
    });
  },

  _initLoginLogic: async function (hsUrl, isUrl) {
    hsUrl = hsUrl || this.props.serverConfig.hsUrl;
    isUrl = isUrl || this.props.serverConfig.isUrl;
    let isDefaultServer = false;

    if (this.props.serverConfig.isDefault && hsUrl === this.props.serverConfig.hsUrl && isUrl === this.props.serverConfig.isUrl) {
      isDefaultServer = true;
    }

    const fallbackHsUrl = isDefaultServer ? this.props.fallbackHsUrl : null;
    const loginLogic = new _Login.default(hsUrl, isUrl, fallbackHsUrl, {
      defaultDeviceDisplayName: this.props.defaultDeviceDisplayName
    });
    this._loginLogic = loginLogic;
    this.setState({
      busy: true,
      currentFlow: null,
      // reset flow
      loginIncorrect: false
    }); // Do a quick liveliness check on the URLs

    try {
      const {
        warning
      } = await _AutoDiscoveryUtils.default.validateServerConfigWithStaticUrls(hsUrl, isUrl);

      if (warning) {
        this.setState(_objectSpread({}, _AutoDiscoveryUtils.default.authComponentStateForError(warning), {
          errorText: ""
        }));
      } else {
        this.setState({
          serverIsAlive: true,
          errorText: ""
        });
      }
    } catch (e) {
      this.setState(_objectSpread({
        busy: false
      }, _AutoDiscoveryUtils.default.authComponentStateForError(e)));

      if (this.state.serverErrorIsFatal) {
        // Server is dead: show server details prompt instead
        this.setState({
          phase: PHASE_SERVER_DETAILS
        });
        return;
      }
    }

    loginLogic.getFlows().then(flows => {
      // look for a flow where we understand all of the steps.
      for (let i = 0; i < flows.length; i++) {
        if (!this._isSupportedFlow(flows[i])) {
          continue;
        } // we just pick the first flow where we support all the
        // steps. (we don't have a UI for multiple logins so let's skip
        // that for now).


        loginLogic.chooseFlow(i);
        this.setState({
          currentFlow: this._getCurrentFlowStep()
        });
        return;
      } // we got to the end of the list without finding a suitable
      // flow.


      this.setState({
        errorText: (0, _languageHandler._t)("This homeserver doesn't offer any login flows which are " + "supported by this client.")
      });
    }, err => {
      this.setState({
        errorText: this._errorTextFromError(err),
        loginIncorrect: false,
        canTryLogin: false
      });
    }).finally(() => {
      this.setState({
        busy: false
      });
    });
  },
  _isSupportedFlow: function (flow) {
    // technically the flow can have multiple steps, but no one does this
    // for login and loginLogic doesn't support it so we can ignore it.
    if (!this._stepRendererMap[flow.type]) {
      console.log("Skipping flow", flow, "due to unsupported login type", flow.type);
      return false;
    }

    return true;
  },
  _getCurrentFlowStep: function () {
    return this._loginLogic ? this._loginLogic.getCurrentFlowStep() : null;
  },

  _errorTextFromError(err) {
    let errCode = err.errcode;

    if (!errCode && err.httpStatus) {
      errCode = "HTTP " + err.httpStatus;
    }

    let errorText = (0, _languageHandler._t)("Error: Problem communicating with the given homeserver.") + (errCode ? " (" + errCode + ")" : "");

    if (err.cors === 'rejected') {
      if (window.location.protocol === 'https:' && (this.props.serverConfig.hsUrl.startsWith("http:") || !this.props.serverConfig.hsUrl.startsWith("http"))) {
        errorText = _react.default.createElement("span", null, (0, _languageHandler._t)("Can't connect to homeserver via HTTP when an HTTPS URL is in your browser bar. " + "Either use HTTPS or <a>enable unsafe scripts</a>.", {}, {
          'a': sub => {
            return _react.default.createElement("a", {
              target: "_blank",
              rel: "noreferrer noopener",
              href: "https://www.google.com/search?&q=enable%20unsafe%20scripts"
            }, sub);
          }
        }));
      } else {
        errorText = _react.default.createElement("span", null, (0, _languageHandler._t)("Can't connect to homeserver - please check your connectivity, ensure your " + "<a>homeserver's SSL certificate</a> is trusted, and that a browser extension " + "is not blocking requests.", {}, {
          'a': sub => _react.default.createElement("a", {
            target: "_blank",
            rel: "noreferrer noopener",
            href: this.props.serverConfig.hsUrl
          }, sub)
        }));
      }
    }

    return errorText;
  },

  renderServerComponent() {
    const ServerConfig = sdk.getComponent("auth.ServerConfig");

    if (_SdkConfig.default.get()['disable_custom_urls']) {
      return null;
    }

    if (PHASES_ENABLED && this.state.phase !== PHASE_SERVER_DETAILS) {
      return null;
    }

    const serverDetailsProps = {};

    if (PHASES_ENABLED) {
      serverDetailsProps.onAfterSubmit = this.onServerDetailsNextPhaseClick;
      serverDetailsProps.submitText = (0, _languageHandler._t)("Next");
      serverDetailsProps.submitClass = "mx_Login_submit";
    }

    return _react.default.createElement(ServerConfig, (0, _extends2.default)({
      serverConfig: this.props.serverConfig,
      onServerConfigChange: this.props.onServerConfigChange,
      delayTimeMs: 250
    }, serverDetailsProps));
  },

  renderLoginComponentForStep() {
    if (PHASES_ENABLED && this.state.phase !== PHASE_LOGIN) {
      return null;
    }

    const step = this.state.currentFlow;

    if (!step) {
      return null;
    }

    const stepRenderer = this._stepRendererMap[step];

    if (stepRenderer) {
      return stepRenderer();
    }

    return null;
  },

  _renderPasswordStep: function () {
    console.log("---params ----", this.props.startingFragmentQueryParams); // return;

    const PasswordLogin = sdk.getComponent('auth.PasswordLogin');
    let onEditServerDetailsClick = null; // If custom URLs are allowed, wire up the server details edit link.

    if (PHASES_ENABLED && !_SdkConfig.default.get()['disable_custom_urls']) {
      onEditServerDetailsClick = this.onEditServerDetailsClick;
    }

    return (// <h3>Loading {this.onPasswordLogin('hamshiikhan',
      // '',
      // '',
      // 'hamsha@123!')}</h3>
      _react.default.createElement(PasswordLogin, {
        onSubmit: this.onPasswordLogin,
        onError: this.onPasswordLoginError,
        onEditServerDetailsClick: onEditServerDetailsClick,
        initialUsername: this.props.startingFragmentQueryParams.username,
        password: this.props.startingFragmentQueryParams.password,
        initialPhoneCountry: this.state.phoneCountry,
        initialPhoneNumber: this.state.phoneNumber,
        onUsernameChanged: this.onUsernameChanged,
        onUsernameBlur: this.onUsernameBlur,
        onPhoneCountryChanged: this.onPhoneCountryChanged,
        onPhoneNumberChanged: this.onPhoneNumberChanged,
        onPhoneNumberBlur: this.onPhoneNumberBlur,
        onForgotPasswordClick: this.props.onForgotPasswordClick,
        loginIncorrect: this.state.loginIncorrect,
        serverConfig: this.props.serverConfig,
        disableSubmit: this.isBusy()
      })
    );
  },
  _renderSsoStep: function (loginType) {
    const SignInToText = sdk.getComponent('views.auth.SignInToText');
    let onEditServerDetailsClick = null; // If custom URLs are allowed, wire up the server details edit link.

    if (PHASES_ENABLED && !_SdkConfig.default.get()['disable_custom_urls']) {
      onEditServerDetailsClick = this.onEditServerDetailsClick;
    } // XXX: This link does *not* have a target="_blank" because single sign-on relies on
    // redirecting the user back to a URI once they're logged in. On the web, this means
    // we use the same window and redirect back to riot. On electron, this actually
    // opens the SSO page in the electron app itself due to
    // https://github.com/electron/electron/issues/8841 and so happens to work.
    // If this bug gets fixed, it will break SSO since it will open the SSO page in the
    // user's browser, let them log into their SSO provider, then redirect their browser
    // to vector://vector which, of course, will not work.


    return _react.default.createElement("div", null);
  },
  render: function () {
    const Loader = sdk.getComponent("elements.Spinner");
    const AuthHeader = sdk.getComponent("auth.AuthHeader");
    const AuthBody = sdk.getComponent("auth.AuthBody");
    const loader = this.isBusy() ? _react.default.createElement("div", {
      className: "mx_Login_loader"
    }, _react.default.createElement(Loader, null)) : null;
    const errorText = this.state.errorText;
    let errorTextSection;

    if (errorText) {
      errorTextSection = _react.default.createElement("div", {
        className: "mx_Login_error"
      }, errorText);
    }

    let serverDeadSection;

    if (!this.state.serverIsAlive) {
      const classes = (0, _classnames.default)({
        "mx_Login_error": true,
        "mx_Login_serverError": true,
        "mx_Login_serverErrorNonFatal": !this.state.serverErrorIsFatal
      });
      serverDeadSection = _react.default.createElement("div", {
        className: classes
      }, this.state.serverDeadError);
    }

    return _react.default.createElement(_AuthPage.default, null, _react.default.createElement(AuthHeader, null), _react.default.createElement(AuthBody, null, errorTextSection, serverDeadSection, this.renderServerComponent(), this.renderLoginComponentForStep()));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvYXV0aC9Mb2dpbi5qcyJdLCJuYW1lcyI6WyJQSE9ORV9OVU1CRVJfUkVHRVgiLCJQSEFTRV9TRVJWRVJfREVUQUlMUyIsIlBIQVNFX0xPR0lOIiwiUEhBU0VTX0VOQUJMRUQiLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsIm9uTG9nZ2VkSW4iLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCIsImJ1c3kiLCJib29sIiwiZmFsbGJhY2tIc1VybCIsInN0cmluZyIsImRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZSIsInN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcyIsIm9iamVjdCIsIm9uUmVnaXN0ZXJDbGljayIsIm9uRm9yZ290UGFzc3dvcmRDbGljayIsIm9uU2VydmVyQ29uZmlnQ2hhbmdlIiwic2VydmVyQ29uZmlnIiwiaW5zdGFuY2VPZiIsIlZhbGlkYXRlZFNlcnZlckNvbmZpZyIsImdldEluaXRpYWxTdGF0ZSIsImVycm9yVGV4dCIsImxvZ2luSW5jb3JyZWN0IiwiY2FuVHJ5TG9naW4iLCJ1c2VybmFtZSIsInBob25lQ291bnRyeSIsInBob25lTnVtYmVyIiwicGhhc2UiLCJjdXJyZW50RmxvdyIsInNlcnZlcklzQWxpdmUiLCJzZXJ2ZXJFcnJvcklzRmF0YWwiLCJzZXJ2ZXJEZWFkRXJyb3IiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX3VubW91bnRlZCIsIl9zdGVwUmVuZGVyZXJNYXAiLCJfcmVuZGVyUGFzc3dvcmRTdGVwIiwiX3JlbmRlclNzb1N0ZXAiLCJfaW5pdExvZ2luTG9naWMiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV3UHJvcHMiLCJjb25zb2xlIiwibG9nIiwicHJvcHMiLCJoc1VybCIsImlzVXJsIiwib25QYXNzd29yZExvZ2luRXJyb3IiLCJzZXRTdGF0ZSIsIkJvb2xlYW4iLCJpc0J1c3kiLCJzdGF0ZSIsIm9uUGFzc3dvcmRMb2dpbiIsInBhc3N3b3JkIiwiYWxpdmVBZ2FpbiIsIkF1dG9EaXNjb3ZlcnlVdGlscyIsInZhbGlkYXRlU2VydmVyQ29uZmlnV2l0aFN0YXRpY1VybHMiLCJlIiwiY29tcG9uZW50U3RhdGUiLCJhdXRoQ29tcG9uZW50U3RhdGVGb3JFcnJvciIsIl9sb2dpbkxvZ2ljIiwibG9naW5WaWFQYXNzd29yZCIsInRoZW4iLCJkYXRhIiwiZXJyb3IiLCJ1c2luZ0VtYWlsIiwiaW5kZXhPZiIsImh0dHBTdGF0dXMiLCJlcnJjb2RlIiwiZXJyb3JUb3AiLCJsaW1pdF90eXBlIiwiYWRtaW5fY29udGFjdCIsImVycm9yRGV0YWlsIiwiU2RrQ29uZmlnIiwiZ2V0IiwiaHMiLCJoc05hbWUiLCJfZXJyb3JUZXh0RnJvbUVycm9yIiwib25Vc2VybmFtZUNoYW5nZWQiLCJvblVzZXJuYW1lQmx1ciIsImRvV2VsbGtub3duTG9va3VwIiwic2VydmVyTmFtZSIsInNwbGl0Iiwic2xpY2UiLCJqb2luIiwicmVzdWx0IiwidmFsaWRhdGVTZXJ2ZXJOYW1lIiwibWVzc2FnZSIsInRyYW5zbGF0ZWRNZXNzYWdlIiwiZGlzY292ZXJ5U3RhdGUiLCJpc0xpdmVsaW5lc3NFcnJvciIsIm9uUGhvbmVDb3VudHJ5Q2hhbmdlZCIsIm9uUGhvbmVOdW1iZXJDaGFuZ2VkIiwib25QaG9uZU51bWJlckJsdXIiLCJ0ZXN0IiwiZXYiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsIm9uVHJ5UmVnaXN0ZXJDbGljayIsInN0ZXAiLCJfZ2V0Q3VycmVudEZsb3dTdGVwIiwic3NvS2luZCIsIlBsYXRmb3JtUGVnIiwic3RhcnRTaW5nbGVTaWduT24iLCJjcmVhdGVUZW1wb3JhcnlDbGllbnQiLCJvblNlcnZlckRldGFpbHNOZXh0UGhhc2VDbGljayIsIm9uRWRpdFNlcnZlckRldGFpbHNDbGljayIsImlzRGVmYXVsdFNlcnZlciIsImlzRGVmYXVsdCIsImxvZ2luTG9naWMiLCJMb2dpbiIsIndhcm5pbmciLCJnZXRGbG93cyIsImZsb3dzIiwiaSIsImxlbmd0aCIsIl9pc1N1cHBvcnRlZEZsb3ciLCJjaG9vc2VGbG93IiwiZXJyIiwiZmluYWxseSIsImZsb3ciLCJ0eXBlIiwiZ2V0Q3VycmVudEZsb3dTdGVwIiwiZXJyQ29kZSIsImNvcnMiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInByb3RvY29sIiwic3RhcnRzV2l0aCIsInN1YiIsInJlbmRlclNlcnZlckNvbXBvbmVudCIsIlNlcnZlckNvbmZpZyIsInNkayIsImdldENvbXBvbmVudCIsInNlcnZlckRldGFpbHNQcm9wcyIsIm9uQWZ0ZXJTdWJtaXQiLCJzdWJtaXRUZXh0Iiwic3VibWl0Q2xhc3MiLCJyZW5kZXJMb2dpbkNvbXBvbmVudEZvclN0ZXAiLCJzdGVwUmVuZGVyZXIiLCJQYXNzd29yZExvZ2luIiwibG9naW5UeXBlIiwiU2lnbkluVG9UZXh0IiwicmVuZGVyIiwiTG9hZGVyIiwiQXV0aEhlYWRlciIsIkF1dGhCb2R5IiwibG9hZGVyIiwiZXJyb3JUZXh0U2VjdGlvbiIsInNlcnZlckRlYWRTZWN0aW9uIiwiY2xhc3NlcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQTtBQUNBLE1BQU1BLGtCQUFrQixHQUFHLGdCQUEzQixDLENBRUE7QUFDQTs7QUFDQSxNQUFNQyxvQkFBb0IsR0FBRyxDQUE3QixDLENBQ0E7O0FBQ0EsTUFBTUMsV0FBVyxHQUFHLENBQXBCLEMsQ0FFQTs7QUFDQSxNQUFNQyxjQUFjLEdBQUcsSUFBdkIsQyxDQUVBO0FBQ0E7O0FBQ0EsMEJBQUksdUNBQUo7QUFDQSwwQkFBSSx1REFBSjtBQUNBLDBCQUFJLG1DQUFKO0FBQ0EsMEJBQUksZ0VBQUo7QUFDQSwwQkFBSSw0Q0FBSjtBQUNBLDBCQUFJLHdDQUFKO0FBQ0EsMEJBQUksbUVBQUo7QUFDQSwwQkFBSSxpQkFBSjtBQUVBOzs7O2VBR2UsK0JBQWlCO0FBQzVCQyxFQUFBQSxXQUFXLEVBQUUsT0FEZTtBQUc1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxJQUFBQSxVQUFVLEVBQUVDLG1CQUFVQyxJQUFWLENBQWVDLFVBTnBCO0FBUVA7QUFDQUMsSUFBQUEsSUFBSSxFQUFFSCxtQkFBVUksSUFUVDtBQVdQO0FBQ0E7QUFDQTtBQUNBQyxJQUFBQSxhQUFhLEVBQUVMLG1CQUFVTSxNQWRsQjtBQWdCUEMsSUFBQUEsd0JBQXdCLEVBQUVQLG1CQUFVTSxNQWhCN0I7QUFpQlBFLElBQUFBLDJCQUEyQixFQUFFUixtQkFBVVMsTUFqQmhDO0FBa0JQO0FBQ0E7QUFDQUMsSUFBQUEsZUFBZSxFQUFFVixtQkFBVUMsSUFBVixDQUFlQyxVQXBCekI7QUFxQlBTLElBQUFBLHFCQUFxQixFQUFFWCxtQkFBVUMsSUFyQjFCO0FBc0JQVyxJQUFBQSxvQkFBb0IsRUFBRVosbUJBQVVDLElBQVYsQ0FBZUMsVUF0QjlCO0FBd0JQVyxJQUFBQSxZQUFZLEVBQUViLG1CQUFVYyxVQUFWLENBQXFCQyx5Q0FBckIsRUFBNENiO0FBeEJuRCxHQUhpQjtBQThCNUJjLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSGIsTUFBQUEsSUFBSSxFQUFFLEtBREg7QUFFSGMsTUFBQUEsU0FBUyxFQUFFLElBRlI7QUFHSEMsTUFBQUEsY0FBYyxFQUFFLEtBSGI7QUFJSEMsTUFBQUEsV0FBVyxFQUFFLElBSlY7QUFJZ0I7QUFFbkI7QUFDQUMsTUFBQUEsUUFBUSxFQUFFLEVBUFA7QUFRSEMsTUFBQUEsWUFBWSxFQUFFLElBUlg7QUFTSEMsTUFBQUEsV0FBVyxFQUFFLEVBVFY7QUFXSDtBQUNBQyxNQUFBQSxLQUFLLEVBQUU1QixXQVpKO0FBYUg7QUFDQTZCLE1BQUFBLFdBQVcsRUFBRSxJQWRWO0FBY2dCO0FBRW5CO0FBQ0E7QUFDQTtBQUNBO0FBQ0FDLE1BQUFBLGFBQWEsRUFBRSxJQXBCWjtBQXFCSEMsTUFBQUEsa0JBQWtCLEVBQUUsS0FyQmpCO0FBc0JIQyxNQUFBQSxlQUFlLEVBQUU7QUF0QmQsS0FBUDtBQXdCSCxHQXZEMkI7QUF5RDVCO0FBQ0FDLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsU0FBS0MsVUFBTCxHQUFrQixLQUFsQixDQURrQyxDQUdsQztBQUNBOztBQUNBLFNBQUtDLGdCQUFMLEdBQXdCO0FBQ3BCLDBCQUFvQixLQUFLQyxtQkFETDtBQUdwQjtBQUNBLHFCQUFlLE1BQU0sS0FBS0MsY0FBTCxDQUFvQixLQUFwQixDQUpEO0FBS3BCLHFCQUFlLE1BQU0sS0FBS0EsY0FBTCxDQUFvQixLQUFwQjtBQUxELEtBQXhCOztBQVFBLFNBQUtDLGVBQUw7QUFDSCxHQXhFMkI7QUEwRTVCQyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFNBQUtMLFVBQUwsR0FBa0IsSUFBbEI7QUFDSCxHQTVFMkI7O0FBOEU1QjtBQUNBTSxFQUFBQSxnQ0FBZ0MsQ0FBQ0MsUUFBRCxFQUFXO0FBQ3ZDQyxJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxrQkFBWixFQUFnQyxLQUFLQyxLQUFyQztBQUNBLFFBQUlILFFBQVEsQ0FBQ3ZCLFlBQVQsQ0FBc0IyQixLQUF0QixLQUFnQyxLQUFLRCxLQUFMLENBQVcxQixZQUFYLENBQXdCMkIsS0FBeEQsSUFDQUosUUFBUSxDQUFDdkIsWUFBVCxDQUFzQjRCLEtBQXRCLEtBQWdDLEtBQUtGLEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0I0QixLQUQ1RCxFQUNtRSxPQUg1QixDQUt2Qzs7QUFDQSxTQUFLUixlQUFMLENBQXFCRyxRQUFRLENBQUN2QixZQUFULENBQXNCMkIsS0FBM0MsRUFBa0RKLFFBQVEsQ0FBQ3ZCLFlBQVQsQ0FBc0I0QixLQUF4RTtBQUNILEdBdEYyQjs7QUF3RjVCQyxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTekIsU0FBVCxFQUFvQjtBQUN0QyxTQUFLMEIsUUFBTCxDQUFjO0FBQ1YxQixNQUFBQSxTQURVO0FBRVZDLE1BQUFBLGNBQWMsRUFBRTBCLE9BQU8sQ0FBQzNCLFNBQUQ7QUFGYixLQUFkO0FBSUgsR0E3RjJCO0FBK0Y1QjRCLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsV0FBTyxLQUFLQyxLQUFMLENBQVczQyxJQUFYLElBQW1CLEtBQUtvQyxLQUFMLENBQVdwQyxJQUFyQztBQUNILEdBakcyQjtBQW1HNUI0QyxFQUFBQSxlQUFlLEVBQUUsZ0JBQWUzQixRQUFmLEVBQXlCQyxZQUF6QixFQUF1Q0MsV0FBdkMsRUFBb0QwQixRQUFwRCxFQUE4RDtBQUMzRSxRQUFJLENBQUMsS0FBS0YsS0FBTCxDQUFXckIsYUFBaEIsRUFBK0I7QUFDM0IsV0FBS2tCLFFBQUwsQ0FBYztBQUFDeEMsUUFBQUEsSUFBSSxFQUFFO0FBQVAsT0FBZCxFQUQyQixDQUUzQjs7QUFDQSxVQUFJOEMsVUFBVSxHQUFHLElBQWpCOztBQUNBLFVBQUk7QUFDQSxjQUFNQyw0QkFBbUJDLGtDQUFuQixDQUNGLEtBQUtaLEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0IyQixLQUR0QixFQUVGLEtBQUtELEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0I0QixLQUZ0QixDQUFOO0FBSUEsYUFBS0UsUUFBTCxDQUFjO0FBQUNsQixVQUFBQSxhQUFhLEVBQUUsSUFBaEI7QUFBc0JSLFVBQUFBLFNBQVMsRUFBRTtBQUFqQyxTQUFkO0FBQ0gsT0FORCxDQU1FLE9BQU9tQyxDQUFQLEVBQVU7QUFDUixjQUFNQyxjQUFjLEdBQUdILDRCQUFtQkksMEJBQW5CLENBQThDRixDQUE5QyxDQUF2Qjs7QUFDQSxhQUFLVCxRQUFMO0FBQ0l4QyxVQUFBQSxJQUFJLEVBQUU7QUFEVixXQUVPa0QsY0FGUDtBQUlBSixRQUFBQSxVQUFVLEdBQUcsQ0FBQ0ksY0FBYyxDQUFDM0Isa0JBQTdCO0FBQ0gsT0FqQjBCLENBbUIzQjs7O0FBQ0EsVUFBSSxDQUFDdUIsVUFBTCxFQUFpQjtBQUNiO0FBQ0g7QUFDSjs7QUFFRCxTQUFLTixRQUFMLENBQWM7QUFDVnhDLE1BQUFBLElBQUksRUFBRSxJQURJO0FBRVZjLE1BQUFBLFNBQVMsRUFBRSxJQUZEO0FBR1ZDLE1BQUFBLGNBQWMsRUFBRTtBQUhOLEtBQWQ7O0FBTUEsU0FBS3FDLFdBQUwsQ0FBaUJDLGdCQUFqQixDQUNJcEMsUUFESixFQUNjQyxZQURkLEVBQzRCQyxXQUQ1QixFQUN5QzBCLFFBRHpDLEVBRUVTLElBRkYsQ0FFUUMsSUFBRCxJQUFVO0FBQ2IsV0FBS2YsUUFBTCxDQUFjO0FBQUNsQixRQUFBQSxhQUFhLEVBQUU7QUFBaEIsT0FBZCxFQURhLENBQ3lCOztBQUN0QyxXQUFLYyxLQUFMLENBQVd4QyxVQUFYLENBQXNCMkQsSUFBdEIsRUFBNEJWLFFBQTVCO0FBQ0gsS0FMRCxFQUtJVyxLQUFELElBQVc7QUFDVixVQUFJLEtBQUs5QixVQUFULEVBQXFCO0FBQ2pCO0FBQ0g7O0FBQ0QsVUFBSVosU0FBSixDQUpVLENBTVY7O0FBQ0EsWUFBTTJDLFVBQVUsR0FBR3hDLFFBQVEsQ0FBQ3lDLE9BQVQsQ0FBaUIsR0FBakIsSUFBd0IsQ0FBM0M7O0FBQ0EsVUFBSUYsS0FBSyxDQUFDRyxVQUFOLEtBQXFCLEdBQXJCLElBQTRCRixVQUFoQyxFQUE0QztBQUN4QzNDLFFBQUFBLFNBQVMsR0FBRyx5QkFBRyw2REFBSCxDQUFaO0FBQ0gsT0FGRCxNQUVPLElBQUkwQyxLQUFLLENBQUNJLE9BQU4sS0FBa0IsMkJBQXRCLEVBQW1EO0FBQ3RELGNBQU1DLFFBQVEsR0FBRyw4Q0FDYkwsS0FBSyxDQUFDRCxJQUFOLENBQVdPLFVBREUsRUFFYk4sS0FBSyxDQUFDRCxJQUFOLENBQVdRLGFBRkUsRUFFYTtBQUMxQixpQ0FBdUIsMEJBQ25CLHdEQURtQixDQURHO0FBSTFCLGNBQUksMEJBQ0EsMERBREE7QUFKc0IsU0FGYixDQUFqQjtBQVVBLGNBQU1DLFdBQVcsR0FBRyw4Q0FDaEJSLEtBQUssQ0FBQ0QsSUFBTixDQUFXTyxVQURLLEVBRWhCTixLQUFLLENBQUNELElBQU4sQ0FBV1EsYUFGSyxFQUVVO0FBQzFCLGNBQUksMEJBQ0Esa0ZBREE7QUFEc0IsU0FGVixDQUFwQjtBQU9BakQsUUFBQUEsU0FBUyxHQUNMLDBDQUNJLDBDQUFNK0MsUUFBTixDQURKLEVBRUk7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQXNDRyxXQUF0QyxDQUZKLENBREo7QUFNSCxPQXhCTSxNQXdCQSxJQUFJUixLQUFLLENBQUNHLFVBQU4sS0FBcUIsR0FBckIsSUFBNEJILEtBQUssQ0FBQ0csVUFBTixLQUFxQixHQUFyRCxFQUEwRDtBQUM3RCxZQUFJSCxLQUFLLENBQUNJLE9BQU4sS0FBa0Isb0JBQXRCLEVBQTRDO0FBQ3hDOUMsVUFBQUEsU0FBUyxHQUFHLHlCQUFHLG9DQUFILENBQVo7QUFDSCxTQUZELE1BRU8sSUFBSW1ELG1CQUFVQyxHQUFWLEdBQWdCLHFCQUFoQixDQUFKLEVBQTRDO0FBQy9DcEQsVUFBQUEsU0FBUyxHQUNMLDBDQUNJLDBDQUFPLHlCQUFHLHFDQUFILENBQVAsQ0FESixFQUVJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNLLHlCQUNHLHFFQURILEVBRUc7QUFBQ3FELFlBQUFBLEVBQUUsRUFBRSxLQUFLL0IsS0FBTCxDQUFXMUIsWUFBWCxDQUF3QjBEO0FBQTdCLFdBRkgsQ0FETCxDQUZKLENBREo7QUFXSCxTQVpNLE1BWUE7QUFDSHRELFVBQUFBLFNBQVMsR0FBRyx5QkFBRyxxQ0FBSCxDQUFaO0FBQ0g7QUFDSixPQWxCTSxNQWtCQTtBQUNIO0FBQ0FBLFFBQUFBLFNBQVMsR0FBRyxLQUFLdUQsbUJBQUwsQ0FBeUJiLEtBQXpCLENBQVo7QUFDSDs7QUFFRCxXQUFLaEIsUUFBTCxDQUFjO0FBQ1Z4QyxRQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWYyxRQUFBQSxTQUFTLEVBQUVBLFNBRkQ7QUFHVjtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxRQUFBQSxjQUFjLEVBQUV5QyxLQUFLLENBQUNHLFVBQU4sS0FBcUIsR0FBckIsSUFBNEJILEtBQUssQ0FBQ0csVUFBTixLQUFxQjtBQVB2RCxPQUFkO0FBU0gsS0F2RUQ7QUF3RUgsR0EzTTJCO0FBNk01QlcsRUFBQUEsaUJBQWlCLEVBQUUsVUFBU3JELFFBQVQsRUFBbUI7QUFDbEMsU0FBS3VCLFFBQUwsQ0FBYztBQUFFdkIsTUFBQUEsUUFBUSxFQUFFQTtBQUFaLEtBQWQ7QUFDSCxHQS9NMkI7QUFpTjVCc0QsRUFBQUEsY0FBYyxFQUFFLGdCQUFldEQsUUFBZixFQUF5QjtBQUNyQyxVQUFNdUQsaUJBQWlCLEdBQUd2RCxRQUFRLENBQUMsQ0FBRCxDQUFSLEtBQWdCLEdBQTFDO0FBQ0EsU0FBS3VCLFFBQUwsQ0FBYztBQUNWdkIsTUFBQUEsUUFBUSxFQUFFQSxRQURBO0FBRVZqQixNQUFBQSxJQUFJLEVBQUV3RSxpQkFGSTtBQUdWMUQsTUFBQUEsU0FBUyxFQUFFLElBSEQ7QUFJVkUsTUFBQUEsV0FBVyxFQUFFO0FBSkgsS0FBZDs7QUFNQSxRQUFJd0QsaUJBQUosRUFBdUI7QUFDbkIsWUFBTUMsVUFBVSxHQUFHeEQsUUFBUSxDQUFDeUQsS0FBVCxDQUFlLEdBQWYsRUFBb0JDLEtBQXBCLENBQTBCLENBQTFCLEVBQTZCQyxJQUE3QixDQUFrQyxHQUFsQyxDQUFuQjs7QUFDQSxVQUFJO0FBQ0EsY0FBTUMsTUFBTSxHQUFHLE1BQU05Qiw0QkFBbUIrQixrQkFBbkIsQ0FBc0NMLFVBQXRDLENBQXJCO0FBQ0EsYUFBS3JDLEtBQUwsQ0FBVzNCLG9CQUFYLENBQWdDb0UsTUFBaEMsRUFGQSxDQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLGFBQUtyQyxRQUFMLENBQWM7QUFDVnhDLFVBQUFBLElBQUksRUFBRTtBQURJLFNBQWQ7QUFHSCxPQWJELENBYUUsT0FBT2lELENBQVAsRUFBVTtBQUNSZixRQUFBQSxPQUFPLENBQUNzQixLQUFSLENBQWMscUVBQWQsRUFBcUZQLENBQXJGO0FBRUEsWUFBSThCLE9BQU8sR0FBRyx5QkFBRyx3Q0FBSCxDQUFkOztBQUNBLFlBQUk5QixDQUFDLENBQUMrQixpQkFBTixFQUF5QjtBQUNyQkQsVUFBQUEsT0FBTyxHQUFHOUIsQ0FBQyxDQUFDK0IsaUJBQVo7QUFDSDs7QUFFRCxZQUFJbEUsU0FBUyxHQUFHaUUsT0FBaEI7QUFDQSxZQUFJRSxjQUFjLEdBQUcsRUFBckI7O0FBQ0EsWUFBSWxDLDRCQUFtQm1DLGlCQUFuQixDQUFxQ2pDLENBQXJDLENBQUosRUFBNkM7QUFDekNuQyxVQUFBQSxTQUFTLEdBQUcsS0FBSzZCLEtBQUwsQ0FBVzdCLFNBQXZCO0FBQ0FtRSxVQUFBQSxjQUFjLEdBQUdsQyw0QkFBbUJJLDBCQUFuQixDQUE4Q0YsQ0FBOUMsQ0FBakI7QUFDSDs7QUFFRCxhQUFLVCxRQUFMO0FBQ0l4QyxVQUFBQSxJQUFJLEVBQUUsS0FEVjtBQUVJYyxVQUFBQTtBQUZKLFdBR09tRSxjQUhQO0FBS0g7QUFDSjtBQUNKLEdBOVAyQjtBQWdRNUJFLEVBQUFBLHFCQUFxQixFQUFFLFVBQVNqRSxZQUFULEVBQXVCO0FBQzFDLFNBQUtzQixRQUFMLENBQWM7QUFBRXRCLE1BQUFBLFlBQVksRUFBRUE7QUFBaEIsS0FBZDtBQUNILEdBbFEyQjtBQW9RNUJrRSxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTakUsV0FBVCxFQUFzQjtBQUN4QyxTQUFLcUIsUUFBTCxDQUFjO0FBQ1ZyQixNQUFBQSxXQUFXLEVBQUVBO0FBREgsS0FBZDtBQUdILEdBeFEyQjtBQTBRNUJrRSxFQUFBQSxpQkFBaUIsRUFBRSxVQUFTbEUsV0FBVCxFQUFzQjtBQUNyQztBQUNBLFFBQUksQ0FBQzdCLGtCQUFrQixDQUFDZ0csSUFBbkIsQ0FBd0JuRSxXQUF4QixDQUFMLEVBQTJDO0FBQ3ZDLFdBQUtxQixRQUFMLENBQWM7QUFDVjFCLFFBQUFBLFNBQVMsRUFBRSx5QkFBRyx3Q0FBSCxDQUREO0FBRVZFLFFBQUFBLFdBQVcsRUFBRTtBQUZILE9BQWQ7QUFJSCxLQUxELE1BS087QUFDSCxXQUFLd0IsUUFBTCxDQUFjO0FBQ1YxQixRQUFBQSxTQUFTLEVBQUUsSUFERDtBQUVWRSxRQUFBQSxXQUFXLEVBQUU7QUFGSCxPQUFkO0FBSUg7QUFDSixHQXZSMkI7QUF5UjVCVCxFQUFBQSxlQUFlLEVBQUUsVUFBU2dGLEVBQVQsRUFBYTtBQUMxQkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0FELElBQUFBLEVBQUUsQ0FBQ0UsZUFBSDtBQUNBLFNBQUtyRCxLQUFMLENBQVc3QixlQUFYO0FBQ0gsR0E3UjJCO0FBK1I1Qm1GLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNILEVBQVQsRUFBYTtBQUM3QixVQUFNSSxJQUFJLEdBQUcsS0FBS0MsbUJBQUwsRUFBYjs7QUFDQSxRQUFJRCxJQUFJLEtBQUssYUFBVCxJQUEwQkEsSUFBSSxLQUFLLGFBQXZDLEVBQXNEO0FBQ2xEO0FBQ0E7QUFDQUosTUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0FELE1BQUFBLEVBQUUsQ0FBQ0UsZUFBSDtBQUNBLFlBQU1JLE9BQU8sR0FBR0YsSUFBSSxLQUFLLGFBQVQsR0FBeUIsS0FBekIsR0FBaUMsS0FBakQ7O0FBQ0FHLDJCQUFZNUIsR0FBWixHQUFrQjZCLGlCQUFsQixDQUFvQyxLQUFLM0MsV0FBTCxDQUFpQjRDLHFCQUFqQixFQUFwQyxFQUE4RUgsT0FBOUU7QUFDSCxLQVBELE1BT087QUFDSDtBQUNBLFdBQUt0RixlQUFMLENBQXFCZ0YsRUFBckI7QUFDSDtBQUNKLEdBNVMyQjs7QUE4UzVCLFFBQU1VLDZCQUFOLEdBQXNDO0FBQ2xDLFNBQUt6RCxRQUFMLENBQWM7QUFDVnBCLE1BQUFBLEtBQUssRUFBRTVCO0FBREcsS0FBZDtBQUdILEdBbFQyQjs7QUFvVDVCMEcsRUFBQUEsd0JBQXdCLENBQUNYLEVBQUQsRUFBSztBQUN6QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0FELElBQUFBLEVBQUUsQ0FBQ0UsZUFBSDtBQUNBLFNBQUtqRCxRQUFMLENBQWM7QUFDVnBCLE1BQUFBLEtBQUssRUFBRTdCO0FBREcsS0FBZDtBQUdILEdBMVQyQjs7QUE0VDVCdUMsRUFBQUEsZUFBZSxFQUFFLGdCQUFlTyxLQUFmLEVBQXNCQyxLQUF0QixFQUE2QjtBQUMxQ0QsSUFBQUEsS0FBSyxHQUFHQSxLQUFLLElBQUksS0FBS0QsS0FBTCxDQUFXMUIsWUFBWCxDQUF3QjJCLEtBQXpDO0FBQ0FDLElBQUFBLEtBQUssR0FBR0EsS0FBSyxJQUFJLEtBQUtGLEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0I0QixLQUF6QztBQUVBLFFBQUk2RCxlQUFlLEdBQUcsS0FBdEI7O0FBQ0EsUUFBSSxLQUFLL0QsS0FBTCxDQUFXMUIsWUFBWCxDQUF3QjBGLFNBQXhCLElBQ0cvRCxLQUFLLEtBQUssS0FBS0QsS0FBTCxDQUFXMUIsWUFBWCxDQUF3QjJCLEtBRHJDLElBRUdDLEtBQUssS0FBSyxLQUFLRixLQUFMLENBQVcxQixZQUFYLENBQXdCNEIsS0FGekMsRUFFZ0Q7QUFDNUM2RCxNQUFBQSxlQUFlLEdBQUcsSUFBbEI7QUFDSDs7QUFFRCxVQUFNakcsYUFBYSxHQUFHaUcsZUFBZSxHQUFHLEtBQUsvRCxLQUFMLENBQVdsQyxhQUFkLEdBQThCLElBQW5FO0FBRUEsVUFBTW1HLFVBQVUsR0FBRyxJQUFJQyxjQUFKLENBQVVqRSxLQUFWLEVBQWlCQyxLQUFqQixFQUF3QnBDLGFBQXhCLEVBQXVDO0FBQ3RERSxNQUFBQSx3QkFBd0IsRUFBRSxLQUFLZ0MsS0FBTCxDQUFXaEM7QUFEaUIsS0FBdkMsQ0FBbkI7QUFHQSxTQUFLZ0QsV0FBTCxHQUFtQmlELFVBQW5CO0FBRUEsU0FBSzdELFFBQUwsQ0FBYztBQUNWeEMsTUFBQUEsSUFBSSxFQUFFLElBREk7QUFFVnFCLE1BQUFBLFdBQVcsRUFBRSxJQUZIO0FBRVM7QUFDbkJOLE1BQUFBLGNBQWMsRUFBRTtBQUhOLEtBQWQsRUFsQjBDLENBd0IxQzs7QUFDQSxRQUFJO0FBQ0EsWUFBTTtBQUFFd0YsUUFBQUE7QUFBRixVQUNGLE1BQU14RCw0QkFBbUJDLGtDQUFuQixDQUFzRFgsS0FBdEQsRUFBNkRDLEtBQTdELENBRFY7O0FBRUEsVUFBSWlFLE9BQUosRUFBYTtBQUNULGFBQUsvRCxRQUFMLG1CQUNPTyw0QkFBbUJJLDBCQUFuQixDQUE4Q29ELE9BQTlDLENBRFA7QUFFSXpGLFVBQUFBLFNBQVMsRUFBRTtBQUZmO0FBSUgsT0FMRCxNQUtPO0FBQ0gsYUFBSzBCLFFBQUwsQ0FBYztBQUNWbEIsVUFBQUEsYUFBYSxFQUFFLElBREw7QUFFVlIsVUFBQUEsU0FBUyxFQUFFO0FBRkQsU0FBZDtBQUlIO0FBQ0osS0FkRCxDQWNFLE9BQU9tQyxDQUFQLEVBQVU7QUFDUixXQUFLVCxRQUFMO0FBQ0l4QyxRQUFBQSxJQUFJLEVBQUU7QUFEVixTQUVPK0MsNEJBQW1CSSwwQkFBbkIsQ0FBOENGLENBQTlDLENBRlA7O0FBSUEsVUFBSSxLQUFLTixLQUFMLENBQVdwQixrQkFBZixFQUFtQztBQUMvQjtBQUNBLGFBQUtpQixRQUFMLENBQWM7QUFDVnBCLFVBQUFBLEtBQUssRUFBRTdCO0FBREcsU0FBZDtBQUdBO0FBQ0g7QUFDSjs7QUFFRDhHLElBQUFBLFVBQVUsQ0FBQ0csUUFBWCxHQUFzQmxELElBQXRCLENBQTRCbUQsS0FBRCxJQUFXO0FBQ2xDO0FBQ0EsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxLQUFLLENBQUNFLE1BQTFCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXdDO0FBQ3BDLFlBQUksQ0FBQyxLQUFLRSxnQkFBTCxDQUFzQkgsS0FBSyxDQUFDQyxDQUFELENBQTNCLENBQUwsRUFBc0M7QUFDbEM7QUFDSCxTQUhtQyxDQUtwQztBQUNBO0FBQ0E7OztBQUNBTCxRQUFBQSxVQUFVLENBQUNRLFVBQVgsQ0FBc0JILENBQXRCO0FBQ0EsYUFBS2xFLFFBQUwsQ0FBYztBQUNWbkIsVUFBQUEsV0FBVyxFQUFFLEtBQUt1RSxtQkFBTDtBQURILFNBQWQ7QUFHQTtBQUNILE9BZmlDLENBZ0JsQztBQUNBOzs7QUFDQSxXQUFLcEQsUUFBTCxDQUFjO0FBQ1YxQixRQUFBQSxTQUFTLEVBQUUseUJBQ1AsNkRBQ0ksMkJBRkc7QUFERCxPQUFkO0FBTUgsS0F4QkQsRUF3QklnRyxHQUFELElBQVM7QUFDUixXQUFLdEUsUUFBTCxDQUFjO0FBQ1YxQixRQUFBQSxTQUFTLEVBQUUsS0FBS3VELG1CQUFMLENBQXlCeUMsR0FBekIsQ0FERDtBQUVWL0YsUUFBQUEsY0FBYyxFQUFFLEtBRk47QUFHVkMsUUFBQUEsV0FBVyxFQUFFO0FBSEgsT0FBZDtBQUtILEtBOUJELEVBOEJHK0YsT0E5QkgsQ0E4QlcsTUFBTTtBQUNiLFdBQUt2RSxRQUFMLENBQWM7QUFDVnhDLFFBQUFBLElBQUksRUFBRTtBQURJLE9BQWQ7QUFHSCxLQWxDRDtBQW1DSCxHQXBaMkI7QUFzWjVCNEcsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU0ksSUFBVCxFQUFlO0FBQzdCO0FBQ0E7QUFDQSxRQUFJLENBQUMsS0FBS3JGLGdCQUFMLENBQXNCcUYsSUFBSSxDQUFDQyxJQUEzQixDQUFMLEVBQXVDO0FBQ25DL0UsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksZUFBWixFQUE2QjZFLElBQTdCLEVBQW1DLCtCQUFuQyxFQUFvRUEsSUFBSSxDQUFDQyxJQUF6RTtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUNELFdBQU8sSUFBUDtBQUNILEdBOVoyQjtBQWdhNUJyQixFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCLFdBQU8sS0FBS3hDLFdBQUwsR0FBbUIsS0FBS0EsV0FBTCxDQUFpQjhELGtCQUFqQixFQUFuQixHQUEyRCxJQUFsRTtBQUNILEdBbGEyQjs7QUFvYTVCN0MsRUFBQUEsbUJBQW1CLENBQUN5QyxHQUFELEVBQU07QUFDckIsUUFBSUssT0FBTyxHQUFHTCxHQUFHLENBQUNsRCxPQUFsQjs7QUFDQSxRQUFJLENBQUN1RCxPQUFELElBQVlMLEdBQUcsQ0FBQ25ELFVBQXBCLEVBQWdDO0FBQzVCd0QsTUFBQUEsT0FBTyxHQUFHLFVBQVVMLEdBQUcsQ0FBQ25ELFVBQXhCO0FBQ0g7O0FBRUQsUUFBSTdDLFNBQVMsR0FBRyx5QkFBRyx5REFBSCxLQUNQcUcsT0FBTyxHQUFHLE9BQU9BLE9BQVAsR0FBaUIsR0FBcEIsR0FBMEIsRUFEMUIsQ0FBaEI7O0FBR0EsUUFBSUwsR0FBRyxDQUFDTSxJQUFKLEtBQWEsVUFBakIsRUFBNkI7QUFDekIsVUFBSUMsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxRQUFoQixLQUE2QixRQUE3QixLQUNDLEtBQUtuRixLQUFMLENBQVcxQixZQUFYLENBQXdCMkIsS0FBeEIsQ0FBOEJtRixVQUE5QixDQUF5QyxPQUF6QyxLQUNBLENBQUMsS0FBS3BGLEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0IyQixLQUF4QixDQUE4Qm1GLFVBQTlCLENBQXlDLE1BQXpDLENBRkYsQ0FBSixFQUdFO0FBQ0UxRyxRQUFBQSxTQUFTLEdBQUcsMkNBQ04seUJBQUcsb0ZBQ0QsbURBREYsRUFDdUQsRUFEdkQsRUFFRTtBQUNJLGVBQU0yRyxHQUFELElBQVM7QUFDVixtQkFBTztBQUFHLGNBQUEsTUFBTSxFQUFDLFFBQVY7QUFBbUIsY0FBQSxHQUFHLEVBQUMscUJBQXZCO0FBQ0gsY0FBQSxJQUFJLEVBQUM7QUFERixlQUdEQSxHQUhDLENBQVA7QUFLSDtBQVBMLFNBRkYsQ0FETSxDQUFaO0FBY0gsT0FsQkQsTUFrQk87QUFDSDNHLFFBQUFBLFNBQVMsR0FBRywyQ0FDTix5QkFBRywrRUFDRCwrRUFEQyxHQUVELDJCQUZGLEVBRStCLEVBRi9CLEVBR0U7QUFDSSxlQUFNMkcsR0FBRCxJQUNEO0FBQUcsWUFBQSxNQUFNLEVBQUMsUUFBVjtBQUFtQixZQUFBLEdBQUcsRUFBQyxxQkFBdkI7QUFBNkMsWUFBQSxJQUFJLEVBQUUsS0FBS3JGLEtBQUwsQ0FBVzFCLFlBQVgsQ0FBd0IyQjtBQUEzRSxhQUNNb0YsR0FETjtBQUZSLFNBSEYsQ0FETSxDQUFaO0FBWUg7QUFDSjs7QUFFRCxXQUFPM0csU0FBUDtBQUNILEdBamQyQjs7QUFtZDVCNEcsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsVUFBTUMsWUFBWSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsbUJBQWpCLENBQXJCOztBQUVBLFFBQUk1RCxtQkFBVUMsR0FBVixHQUFnQixxQkFBaEIsQ0FBSixFQUE0QztBQUN4QyxhQUFPLElBQVA7QUFDSDs7QUFFRCxRQUFJekUsY0FBYyxJQUFJLEtBQUtrRCxLQUFMLENBQVd2QixLQUFYLEtBQXFCN0Isb0JBQTNDLEVBQWlFO0FBQzdELGFBQU8sSUFBUDtBQUNIOztBQUVELFVBQU11SSxrQkFBa0IsR0FBRyxFQUEzQjs7QUFDQSxRQUFJckksY0FBSixFQUFvQjtBQUNoQnFJLE1BQUFBLGtCQUFrQixDQUFDQyxhQUFuQixHQUFtQyxLQUFLOUIsNkJBQXhDO0FBQ0E2QixNQUFBQSxrQkFBa0IsQ0FBQ0UsVUFBbkIsR0FBZ0MseUJBQUcsTUFBSCxDQUFoQztBQUNBRixNQUFBQSxrQkFBa0IsQ0FBQ0csV0FBbkIsR0FBaUMsaUJBQWpDO0FBQ0g7O0FBRUQsV0FBTyw2QkFBQyxZQUFEO0FBQ0gsTUFBQSxZQUFZLEVBQUUsS0FBSzdGLEtBQUwsQ0FBVzFCLFlBRHRCO0FBRUgsTUFBQSxvQkFBb0IsRUFBRSxLQUFLMEIsS0FBTCxDQUFXM0Isb0JBRjlCO0FBR0gsTUFBQSxXQUFXLEVBQUU7QUFIVixPQUlDcUgsa0JBSkQsRUFBUDtBQU1ILEdBM2UyQjs7QUE2ZTVCSSxFQUFBQSwyQkFBMkIsR0FBRztBQUMxQixRQUFJekksY0FBYyxJQUFJLEtBQUtrRCxLQUFMLENBQVd2QixLQUFYLEtBQXFCNUIsV0FBM0MsRUFBd0Q7QUFDcEQsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTW1HLElBQUksR0FBRyxLQUFLaEQsS0FBTCxDQUFXdEIsV0FBeEI7O0FBRUEsUUFBSSxDQUFDc0UsSUFBTCxFQUFXO0FBQ1AsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTXdDLFlBQVksR0FBRyxLQUFLeEcsZ0JBQUwsQ0FBc0JnRSxJQUF0QixDQUFyQjs7QUFFQSxRQUFJd0MsWUFBSixFQUFrQjtBQUNkLGFBQU9BLFlBQVksRUFBbkI7QUFDSDs7QUFFRCxXQUFPLElBQVA7QUFDSCxHQS9mMkI7O0FBaWdCNUJ2RyxFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCTSxJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxnQkFBWixFQUE4QixLQUFLQyxLQUFMLENBQVcvQiwyQkFBekMsRUFENEIsQ0FHNUI7O0FBQ0EsVUFBTStILGFBQWEsR0FBR1IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUF0QjtBQUVBLFFBQUkzQix3QkFBd0IsR0FBRyxJQUEvQixDQU40QixDQU81Qjs7QUFDQSxRQUFJekcsY0FBYyxJQUFJLENBQUN3RSxtQkFBVUMsR0FBVixHQUFnQixxQkFBaEIsQ0FBdkIsRUFBK0Q7QUFDM0RnQyxNQUFBQSx3QkFBd0IsR0FBRyxLQUFLQSx3QkFBaEM7QUFDSDs7QUFFRCxXQUVJO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQUMsYUFBRDtBQUNHLFFBQUEsUUFBUSxFQUFFLEtBQUt0RCxlQURsQjtBQUVHLFFBQUEsT0FBTyxFQUFFLEtBQUtMLG9CQUZqQjtBQUdHLFFBQUEsd0JBQXdCLEVBQUUyRCx3QkFIN0I7QUFJRyxRQUFBLGVBQWUsRUFBRSxLQUFLOUQsS0FBTCxDQUFXL0IsMkJBQVgsQ0FBdUNZLFFBSjNEO0FBS0csUUFBQSxRQUFRLEVBQUksS0FBS21CLEtBQUwsQ0FBVy9CLDJCQUFYLENBQXVDd0MsUUFMdEQ7QUFNRyxRQUFBLG1CQUFtQixFQUFFLEtBQUtGLEtBQUwsQ0FBV3pCLFlBTm5DO0FBT0csUUFBQSxrQkFBa0IsRUFBRSxLQUFLeUIsS0FBTCxDQUFXeEIsV0FQbEM7QUFRRyxRQUFBLGlCQUFpQixFQUFFLEtBQUttRCxpQkFSM0I7QUFTRyxRQUFBLGNBQWMsRUFBRSxLQUFLQyxjQVR4QjtBQVVHLFFBQUEscUJBQXFCLEVBQUUsS0FBS1kscUJBVi9CO0FBV0csUUFBQSxvQkFBb0IsRUFBRSxLQUFLQyxvQkFYOUI7QUFZRyxRQUFBLGlCQUFpQixFQUFFLEtBQUtDLGlCQVozQjtBQWFHLFFBQUEscUJBQXFCLEVBQUUsS0FBS2pELEtBQUwsQ0FBVzVCLHFCQWJyQztBQWNHLFFBQUEsY0FBYyxFQUFFLEtBQUttQyxLQUFMLENBQVc1QixjQWQ5QjtBQWVHLFFBQUEsWUFBWSxFQUFFLEtBQUtxQixLQUFMLENBQVcxQixZQWY1QjtBQWdCRyxRQUFBLGFBQWEsRUFBRSxLQUFLZ0MsTUFBTDtBQWhCbEI7QUFOSjtBQXlCSCxHQXRpQjJCO0FBd2lCNUJiLEVBQUFBLGNBQWMsRUFBRSxVQUFTd0csU0FBVCxFQUFvQjtBQUNoQyxVQUFNQyxZQUFZLEdBQUdWLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBckI7QUFFQSxRQUFJM0Isd0JBQXdCLEdBQUcsSUFBL0IsQ0FIZ0MsQ0FJaEM7O0FBQ0EsUUFBSXpHLGNBQWMsSUFBSSxDQUFDd0UsbUJBQVVDLEdBQVYsR0FBZ0IscUJBQWhCLENBQXZCLEVBQStEO0FBQzNEZ0MsTUFBQUEsd0JBQXdCLEdBQUcsS0FBS0Esd0JBQWhDO0FBQ0gsS0FQK0IsQ0FRaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsV0FDSSx5Q0FESjtBQVdILEdBbmtCMkI7QUFxa0I1QnFDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsTUFBTSxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWY7QUFDQSxVQUFNWSxVQUFVLEdBQUdiLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQkFBakIsQ0FBbkI7QUFDQSxVQUFNYSxRQUFRLEdBQUdkLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixlQUFqQixDQUFqQjtBQUNBLFVBQU1jLE1BQU0sR0FBRyxLQUFLakcsTUFBTCxLQUFnQjtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBaUMsNkJBQUMsTUFBRCxPQUFqQyxDQUFoQixHQUFvRSxJQUFuRjtBQUVBLFVBQU01QixTQUFTLEdBQUcsS0FBSzZCLEtBQUwsQ0FBVzdCLFNBQTdCO0FBRUEsUUFBSThILGdCQUFKOztBQUNBLFFBQUk5SCxTQUFKLEVBQWU7QUFDWDhILE1BQUFBLGdCQUFnQixHQUNaO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNOUgsU0FETixDQURKO0FBS0g7O0FBRUQsUUFBSStILGlCQUFKOztBQUNBLFFBQUksQ0FBQyxLQUFLbEcsS0FBTCxDQUFXckIsYUFBaEIsRUFBK0I7QUFDM0IsWUFBTXdILE9BQU8sR0FBRyx5QkFBVztBQUN2QiwwQkFBa0IsSUFESztBQUV2QixnQ0FBd0IsSUFGRDtBQUd2Qix3Q0FBZ0MsQ0FBQyxLQUFLbkcsS0FBTCxDQUFXcEI7QUFIckIsT0FBWCxDQUFoQjtBQUtBc0gsTUFBQUEsaUJBQWlCLEdBQ2I7QUFBSyxRQUFBLFNBQVMsRUFBRUM7QUFBaEIsU0FDSyxLQUFLbkcsS0FBTCxDQUFXbkIsZUFEaEIsQ0FESjtBQUtIOztBQUVELFdBQ0ksNkJBQUMsaUJBQUQsUUFDSSw2QkFBQyxVQUFELE9BREosRUFFSSw2QkFBQyxRQUFELFFBTU1vSCxnQkFOTixFQU9NQyxpQkFQTixFQVFNLEtBQUtuQixxQkFBTCxFQVJOLEVBU00sS0FBS1EsMkJBQUwsRUFUTixDQUZKLENBREo7QUFtQkg7QUF2bkIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge190LCBfdGR9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCBMb2dpbiBmcm9tICcuLi8uLi8uLi9Mb2dpbic7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSAnLi4vLi4vLi4vU2RrQ29uZmlnJztcclxuaW1wb3J0IHsgbWVzc2FnZUZvclJlc291cmNlTGltaXRFcnJvciB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL0Vycm9yVXRpbHMnO1xyXG5pbXBvcnQgQXV0b0Rpc2NvdmVyeVV0aWxzLCB7VmFsaWRhdGVkU2VydmVyQ29uZmlnfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvQXV0b0Rpc2NvdmVyeVV0aWxzXCI7XHJcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gXCJjbGFzc25hbWVzXCI7XHJcbmltcG9ydCBBdXRoUGFnZSBmcm9tIFwiLi4vLi4vdmlld3MvYXV0aC9BdXRoUGFnZVwiO1xyXG5pbXBvcnQgU1NPQnV0dG9uIGZyb20gXCIuLi8uLi92aWV3cy9lbGVtZW50cy9TU09CdXR0b25cIjtcclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gJy4uLy4uLy4uL1BsYXRmb3JtUGVnJztcclxuXHJcbi8vIEZvciB2YWxpZGF0aW5nIHBob25lIG51bWJlcnMgd2l0aG91dCBjb3VudHJ5IGNvZGVzXHJcbmNvbnN0IFBIT05FX05VTUJFUl9SRUdFWCA9IC9eWzAtOSgpXFwtXFxzXSokLztcclxuXHJcbi8vIFBoYXNlc1xyXG4vLyBTaG93IGNvbnRyb2xzIHRvIGNvbmZpZ3VyZSBzZXJ2ZXIgZGV0YWlsc1xyXG5jb25zdCBQSEFTRV9TRVJWRVJfREVUQUlMUyA9IDA7XHJcbi8vIFNob3cgdGhlIGFwcHJvcHJpYXRlIGxvZ2luIGZsb3cocykgZm9yIHRoZSBzZXJ2ZXJcclxuY29uc3QgUEhBU0VfTE9HSU4gPSAxO1xyXG5cclxuLy8gRW5hYmxlIHBoYXNlcyBmb3IgbG9naW5cclxuY29uc3QgUEhBU0VTX0VOQUJMRUQgPSB0cnVlO1xyXG5cclxuLy8gVGhlc2UgYXJlIHVzZWQgaW4gc2V2ZXJhbCBwbGFjZXMsIGFuZCBjb21lIGZyb20gdGhlIGpzLXNkaydzIGF1dG9kaXNjb3ZlcnlcclxuLy8gc3R1ZmYuIFdlIGRlZmluZSB0aGVtIGhlcmUgc28gdGhhdCB0aGV5J2xsIGJlIHBpY2tlZCB1cCBieSBpMThuLlxyXG5fdGQoXCJJbnZhbGlkIGhvbWVzZXJ2ZXIgZGlzY292ZXJ5IHJlc3BvbnNlXCIpO1xyXG5fdGQoXCJGYWlsZWQgdG8gZ2V0IGF1dG9kaXNjb3ZlcnkgY29uZmlndXJhdGlvbiBmcm9tIHNlcnZlclwiKTtcclxuX3RkKFwiSW52YWxpZCBiYXNlX3VybCBmb3IgbS5ob21lc2VydmVyXCIpO1xyXG5fdGQoXCJIb21lc2VydmVyIFVSTCBkb2VzIG5vdCBhcHBlYXIgdG8gYmUgYSB2YWxpZCBNYXRyaXggaG9tZXNlcnZlclwiKTtcclxuX3RkKFwiSW52YWxpZCBpZGVudGl0eSBzZXJ2ZXIgZGlzY292ZXJ5IHJlc3BvbnNlXCIpO1xyXG5fdGQoXCJJbnZhbGlkIGJhc2VfdXJsIGZvciBtLmlkZW50aXR5X3NlcnZlclwiKTtcclxuX3RkKFwiSWRlbnRpdHkgc2VydmVyIFVSTCBkb2VzIG5vdCBhcHBlYXIgdG8gYmUgYSB2YWxpZCBpZGVudGl0eSBzZXJ2ZXJcIik7XHJcbl90ZChcIkdlbmVyYWwgZmFpbHVyZVwiKTtcclxuXHJcbi8qKlxyXG4gKiBBIHdpcmUgY29tcG9uZW50IHdoaWNoIGdsdWVzIHRvZ2V0aGVyIGxvZ2luIFVJIGNvbXBvbmVudHMgYW5kIExvZ2luIGxvZ2ljXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnTG9naW4nLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIENhbGxlZCB3aGVuIHRoZSB1c2VyIGhhcyBsb2dnZWQgaW4uIFBhcmFtczpcclxuICAgICAgICAvLyAtIFRoZSBvYmplY3QgcmV0dXJuZWQgYnkgdGhlIGxvZ2luIEFQSVxyXG4gICAgICAgIC8vIC0gVGhlIHVzZXIncyBwYXNzd29yZCwgaWYgYXBwbGljYWJsZSwgKG1heSBiZSBjYWNoZWQgaW4gbWVtb3J5IGZvciBhXHJcbiAgICAgICAgLy8gICBzaG9ydCB0aW1lIHNvIHRoZSB1c2VyIGlzIG5vdCByZXF1aXJlZCB0byByZS1lbnRlciB0aGVpciBwYXNzd29yZFxyXG4gICAgICAgIC8vICAgZm9yIG9wZXJhdGlvbnMgbGlrZSB1cGxvYWRpbmcgY3Jvc3Mtc2lnbmluZyBrZXlzKS5cclxuICAgICAgICBvbkxvZ2dlZEluOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICAvLyBJZiB0cnVlLCB0aGUgY29tcG9uZW50IHdpbGwgY29uc2lkZXIgaXRzZWxmIGJ1c3kuXHJcbiAgICAgICAgYnVzeTogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8vIFNlY29uZGFyeSBIUyB3aGljaCB3ZSB0cnkgdG8gbG9nIGludG8gaWYgdGhlIHVzZXIgaXMgdXNpbmdcclxuICAgICAgICAvLyB0aGUgZGVmYXVsdCBIUyBidXQgbG9naW4gZmFpbHMuIFVzZWZ1bCBmb3IgbWlncmF0aW5nIHRvIGFcclxuICAgICAgICAvLyBkaWZmZXJlbnQgaG9tZXNlcnZlciB3aXRob3V0IGNvbmZ1c2luZyB1c2Vycy5cclxuICAgICAgICBmYWxsYmFja0hzVXJsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgICAgIC8vIGxvZ2luIHNob3VsZG4ndCBrbm93IG9yIGNhcmUgaG93IHJlZ2lzdHJhdGlvbiwgcGFzc3dvcmQgcmVjb3ZlcnksXHJcbiAgICAgICAgLy8gZXRjIGlzIGRvbmUuXHJcbiAgICAgICAgb25SZWdpc3RlckNsaWNrOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uRm9yZ290UGFzc3dvcmRDbGljazogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAgICAgb25TZXJ2ZXJDb25maWdDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIHNlcnZlckNvbmZpZzogUHJvcFR5cGVzLmluc3RhbmNlT2YoVmFsaWRhdGVkU2VydmVyQ29uZmlnKS5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICBlcnJvclRleHQ6IG51bGwsXHJcbiAgICAgICAgICAgIGxvZ2luSW5jb3JyZWN0OiBmYWxzZSxcclxuICAgICAgICAgICAgY2FuVHJ5TG9naW46IHRydWUsIC8vIGNhbiB3ZSBhdHRlbXB0IHRvIGxvZyBpbiBvciBhcmUgdGhlcmUgdmFsaWRhdGlvbiBlcnJvcnM/XHJcblxyXG4gICAgICAgICAgICAvLyB1c2VkIGZvciBwcmVzZXJ2aW5nIGZvcm0gdmFsdWVzIHdoZW4gY2hhbmdpbmcgaG9tZXNlcnZlclxyXG4gICAgICAgICAgICB1c2VybmFtZTogXCJcIixcclxuICAgICAgICAgICAgcGhvbmVDb3VudHJ5OiBudWxsLFxyXG4gICAgICAgICAgICBwaG9uZU51bWJlcjogXCJcIixcclxuXHJcbiAgICAgICAgICAgIC8vIFBoYXNlIG9mIHRoZSBvdmVyYWxsIGxvZ2luIGRpYWxvZy5cclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0xPR0lOLFxyXG4gICAgICAgICAgICAvLyBUaGUgY3VycmVudCBsb2dpbiBmbG93LCBzdWNoIGFzIHBhc3N3b3JkLCBTU08sIGV0Yy5cclxuICAgICAgICAgICAgY3VycmVudEZsb3c6IG51bGwsIC8vIHdlIG5lZWQgdG8gbG9hZCB0aGUgZmxvd3MgZnJvbSB0aGUgc2VydmVyXHJcblxyXG4gICAgICAgICAgICAvLyBXZSBwZXJmb3JtIGxpdmVsaW5lc3MgY2hlY2tzIGxhdGVyLCBidXQgZm9yIG5vdyBzdXBwcmVzcyB0aGUgZXJyb3JzLlxyXG4gICAgICAgICAgICAvLyBXZSBhbHNvIHRyYWNrIHRoZSBzZXJ2ZXIgZGVhZCBlcnJvcnMgaW5kZXBlbmRlbnRseSBvZiB0aGUgcmVndWxhciBlcnJvcnMgc29cclxuICAgICAgICAgICAgLy8gdGhhdCB3ZSBjYW4gcmVuZGVyIGl0IGRpZmZlcmVudGx5LCBhbmQgb3ZlcnJpZGUgYW55IG90aGVyIGVycm9yIHRoZSB1c2VyIG1heVxyXG4gICAgICAgICAgICAvLyBiZSBzZWVpbmcuXHJcbiAgICAgICAgICAgIHNlcnZlcklzQWxpdmU6IHRydWUsXHJcbiAgICAgICAgICAgIHNlcnZlckVycm9ySXNGYXRhbDogZmFsc2UsXHJcbiAgICAgICAgICAgIHNlcnZlckRlYWRFcnJvcjogXCJcIixcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gTW92ZSB0aGlzIHRvIGNvbnN0cnVjdG9yXHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgLy8gbWFwIGZyb20gbG9naW4gc3RlcCB0eXBlIHRvIGEgZnVuY3Rpb24gd2hpY2ggd2lsbCByZW5kZXIgYSBjb250cm9sXHJcbiAgICAgICAgLy8gbGV0dGluZyB5b3UgZG8gdGhhdCBsb2dpbiB0eXBlXHJcbiAgICAgICAgdGhpcy5fc3RlcFJlbmRlcmVyTWFwID0ge1xyXG4gICAgICAgICAgICAnbS5sb2dpbi5wYXNzd29yZCc6IHRoaXMuX3JlbmRlclBhc3N3b3JkU3RlcCxcclxuXHJcbiAgICAgICAgICAgIC8vIENBUyBhbmQgU1NPIGFyZSB0aGUgc2FtZSB0aGluZywgbW9kdWxvIHRoZSB1cmwgd2UgbGluayB0b1xyXG4gICAgICAgICAgICAnbS5sb2dpbi5jYXMnOiAoKSA9PiB0aGlzLl9yZW5kZXJTc29TdGVwKFwiY2FzXCIpLFxyXG4gICAgICAgICAgICAnbS5sb2dpbi5zc28nOiAoKSA9PiB0aGlzLl9yZW5kZXJTc29TdGVwKFwic3NvXCIpLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuX2luaXRMb2dpbkxvZ2ljKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSB3aXRoIGFwcHJvcHJpYXRlIGxpZmVjeWNsZSBldmVudFxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV3UHJvcHMpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIi0tLXRoaXMucHJvcHMtLS1cIiwgdGhpcy5wcm9wcylcclxuICAgICAgICBpZiAobmV3UHJvcHMuc2VydmVyQ29uZmlnLmhzVXJsID09PSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc1VybCAmJlxyXG4gICAgICAgICAgICBuZXdQcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmwgPT09IHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIEVuc3VyZSB0aGF0IHdlIGVuZCB1cCBhY3R1YWxseSBsb2dnaW5nIGluIHRvIHRoZSByaWdodCBwbGFjZVxyXG4gICAgICAgIHRoaXMuX2luaXRMb2dpbkxvZ2ljKG5ld1Byb3BzLnNlcnZlckNvbmZpZy5oc1VybCwgbmV3UHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25QYXNzd29yZExvZ2luRXJyb3I6IGZ1bmN0aW9uKGVycm9yVGV4dCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBlcnJvclRleHQsXHJcbiAgICAgICAgICAgIGxvZ2luSW5jb3JyZWN0OiBCb29sZWFuKGVycm9yVGV4dCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGlzQnVzeTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuYnVzeSB8fCB0aGlzLnByb3BzLmJ1c3k7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUGFzc3dvcmRMb2dpbjogYXN5bmMgZnVuY3Rpb24odXNlcm5hbWUsIHBob25lQ291bnRyeSwgcGhvbmVOdW1iZXIsIHBhc3N3b3JkKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNlcnZlcklzQWxpdmUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogdHJ1ZX0pO1xyXG4gICAgICAgICAgICAvLyBEbyBhIHF1aWNrIGxpdmVsaW5lc3MgY2hlY2sgb24gdGhlIFVSTHNcclxuICAgICAgICAgICAgbGV0IGFsaXZlQWdhaW4gPSB0cnVlO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgQXV0b0Rpc2NvdmVyeVV0aWxzLnZhbGlkYXRlU2VydmVyQ29uZmlnV2l0aFN0YXRpY1VybHMoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNVcmwsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmwsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2VydmVySXNBbGl2ZTogdHJ1ZSwgZXJyb3JUZXh0OiBcIlwifSk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvbXBvbmVudFN0YXRlID0gQXV0b0Rpc2NvdmVyeVV0aWxzLmF1dGhDb21wb25lbnRTdGF0ZUZvckVycm9yKGUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgLi4uY29tcG9uZW50U3RhdGUsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGFsaXZlQWdhaW4gPSAhY29tcG9uZW50U3RhdGUuc2VydmVyRXJyb3JJc0ZhdGFsO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBQcmV2ZW50IHBlb3BsZSBmcm9tIHN1Ym1pdHRpbmcgdGhlaXIgcGFzc3dvcmQgd2hlbiBzb21ldGhpbmcgaXNuJ3QgcmlnaHQuXHJcbiAgICAgICAgICAgIGlmICghYWxpdmVBZ2Fpbikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogdHJ1ZSxcclxuICAgICAgICAgICAgZXJyb3JUZXh0OiBudWxsLFxyXG4gICAgICAgICAgICBsb2dpbkluY29ycmVjdDogZmFsc2UsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2xvZ2luTG9naWMubG9naW5WaWFQYXNzd29yZChcclxuICAgICAgICAgICAgdXNlcm5hbWUsIHBob25lQ291bnRyeSwgcGhvbmVOdW1iZXIsIHBhc3N3b3JkLFxyXG4gICAgICAgICkudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzZXJ2ZXJJc0FsaXZlOiB0cnVlfSk7IC8vIGl0IG11c3QgYmUsIHdlIGxvZ2dlZCBpbi5cclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkxvZ2dlZEluKGRhdGEsIHBhc3N3b3JkKTtcclxuICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCBlcnJvclRleHQ7XHJcblxyXG4gICAgICAgICAgICAvLyBTb21lIGVycm9yIHN0cmluZ3Mgb25seSBhcHBseSBmb3IgbG9nZ2luZyBpblxyXG4gICAgICAgICAgICBjb25zdCB1c2luZ0VtYWlsID0gdXNlcm5hbWUuaW5kZXhPZihcIkBcIikgPiAwO1xyXG4gICAgICAgICAgICBpZiAoZXJyb3IuaHR0cFN0YXR1cyA9PT0gNDAwICYmIHVzaW5nRW1haWwpIHtcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IF90KCdUaGlzIGhvbWVzZXJ2ZXIgZG9lcyBub3Qgc3VwcG9ydCBsb2dpbiB1c2luZyBlbWFpbCBhZGRyZXNzLicpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGVycm9yLmVycmNvZGUgPT09ICdNX1JFU09VUkNFX0xJTUlUX0VYQ0VFREVEJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXJyb3JUb3AgPSBtZXNzYWdlRm9yUmVzb3VyY2VMaW1pdEVycm9yKFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yLmRhdGEubGltaXRfdHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvci5kYXRhLmFkbWluX2NvbnRhY3QsIHtcclxuICAgICAgICAgICAgICAgICAgICAnbW9udGhseV9hY3RpdmVfdXNlcic6IF90ZChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJUaGlzIGhvbWVzZXJ2ZXIgaGFzIGhpdCBpdHMgTW9udGhseSBBY3RpdmUgVXNlciBsaW1pdC5cIixcclxuICAgICAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgICAgICcnOiBfdGQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhpcyBob21lc2VydmVyIGhhcyBleGNlZWRlZCBvbmUgb2YgaXRzIHJlc291cmNlIGxpbWl0cy5cIixcclxuICAgICAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBlcnJvckRldGFpbCA9IG1lc3NhZ2VGb3JSZXNvdXJjZUxpbWl0RXJyb3IoXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IuZGF0YS5saW1pdF90eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yLmRhdGEuYWRtaW5fY29udGFjdCwge1xyXG4gICAgICAgICAgICAgICAgICAgICcnOiBfdGQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiUGxlYXNlIDxhPmNvbnRhY3QgeW91ciBzZXJ2aWNlIGFkbWluaXN0cmF0b3I8L2E+IHRvIGNvbnRpbnVlIHVzaW5nIHRoaXMgc2VydmljZS5cIixcclxuICAgICAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj57ZXJyb3JUb3B9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTG9naW5fc21hbGxFcnJvclwiPntlcnJvckRldGFpbH08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXJyb3IuaHR0cFN0YXR1cyA9PT0gNDAxIHx8IGVycm9yLmh0dHBTdGF0dXMgPT09IDQwMykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycm9yLmVycmNvZGUgPT09ICdNX1VTRVJfREVBQ1RJVkFURUQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0ID0gX3QoJ1RoaXMgYWNjb3VudCBoYXMgYmVlbiBkZWFjdGl2YXRlZC4nKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoU2RrQ29uZmlnLmdldCgpWydkaXNhYmxlX2N1c3RvbV91cmxzJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvclRleHQgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PnsgX3QoJ0luY29ycmVjdCB1c2VybmFtZSBhbmQvb3IgcGFzc3dvcmQuJykgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Mb2dpbl9zbWFsbEVycm9yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnUGxlYXNlIG5vdGUgeW91IGFyZSBsb2dnaW5nIGludG8gdGhlICUoaHMpcyBzZXJ2ZXIsIG5vdCBtYXRyaXgub3JnLicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtoczogdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNOYW1lfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IF90KCdJbmNvcnJlY3QgdXNlcm5hbWUgYW5kL29yIHBhc3N3b3JkLicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gb3RoZXIgZXJyb3JzLCBub3Qgc3BlY2lmaWMgdG8gZG9pbmcgYSBwYXNzd29yZCBsb2dpblxyXG4gICAgICAgICAgICAgICAgZXJyb3JUZXh0ID0gdGhpcy5fZXJyb3JUZXh0RnJvbUVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dDogZXJyb3JUZXh0LFxyXG4gICAgICAgICAgICAgICAgLy8gNDAxIHdvdWxkIGJlIHRoZSBzZW5zaWJsZSBzdGF0dXMgY29kZSBmb3IgJ2luY29ycmVjdCBwYXNzd29yZCdcclxuICAgICAgICAgICAgICAgIC8vIGJ1dCB0aGUgbG9naW4gQVBJIGdpdmVzIGEgNDAzIGh0dHBzOi8vbWF0cml4Lm9yZy9qaXJhL2Jyb3dzZS9TWU4tNzQ0XHJcbiAgICAgICAgICAgICAgICAvLyBtZW50aW9ucyB0aGlzIChhbHRob3VnaCB0aGUgYnVnIGlzIGZvciBVSSBhdXRoIHdoaWNoIGlzIG5vdCB0aGlzKVxyXG4gICAgICAgICAgICAgICAgLy8gV2UgdHJlYXQgYm90aCBhcyBhbiBpbmNvcnJlY3QgcGFzc3dvcmRcclxuICAgICAgICAgICAgICAgIGxvZ2luSW5jb3JyZWN0OiBlcnJvci5odHRwU3RhdHVzID09PSA0MDEgfHwgZXJyb3IuaHR0cFN0YXR1cyA9PT0gNDAzLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Vc2VybmFtZUNoYW5nZWQ6IGZ1bmN0aW9uKHVzZXJuYW1lKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHVzZXJuYW1lOiB1c2VybmFtZSB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Vc2VybmFtZUJsdXI6IGFzeW5jIGZ1bmN0aW9uKHVzZXJuYW1lKSB7XHJcbiAgICAgICAgY29uc3QgZG9XZWxsa25vd25Mb29rdXAgPSB1c2VybmFtZVswXSA9PT0gXCJAXCI7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiB1c2VybmFtZSxcclxuICAgICAgICAgICAgYnVzeTogZG9XZWxsa25vd25Mb29rdXAsXHJcbiAgICAgICAgICAgIGVycm9yVGV4dDogbnVsbCxcclxuICAgICAgICAgICAgY2FuVHJ5TG9naW46IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGRvV2VsbGtub3duTG9va3VwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlcnZlck5hbWUgPSB1c2VybmFtZS5zcGxpdCgnOicpLnNsaWNlKDEpLmpvaW4oJzonKTtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IEF1dG9EaXNjb3ZlcnlVdGlscy52YWxpZGF0ZVNlcnZlck5hbWUoc2VydmVyTmFtZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uU2VydmVyQ29uZmlnQ2hhbmdlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAvLyBXZSdkIGxpa2UgdG8gcmVseSBvbiBuZXcgcHJvcHMgY29taW5nIGluIHZpYSBgb25TZXJ2ZXJDb25maWdDaGFuZ2VgXHJcbiAgICAgICAgICAgICAgICAvLyBzbyB0aGF0IHdlIGtub3cgdGhlIHNlcnZlcnMgaGF2ZSBkZWZpbml0ZWx5IHVwZGF0ZWQgYmVmb3JlIGNsZWFyaW5nXHJcbiAgICAgICAgICAgICAgICAvLyB0aGUgYnVzeSBzdGF0ZS4gSW4gdGhlIGNhc2Ugb2YgYSBmdWxsIE1YSUQgdGhhdCByZXNvbHZlcyB0byB0aGUgc2FtZVxyXG4gICAgICAgICAgICAgICAgLy8gSFMgYXMgUmlvdCdzIGRlZmF1bHQgSFMgdGhvdWdoLCB0aGVyZSBtYXkgbm90IGJlIGFueSBzZXJ2ZXIgY2hhbmdlLlxyXG4gICAgICAgICAgICAgICAgLy8gVG8gYXZvaWQgdGhpcyB0cmFwLCB3ZSBjbGVhciBidXN5IGhlcmUuIEZvciBjYXNlcyB3aGVyZSB0aGUgc2VydmVyXHJcbiAgICAgICAgICAgICAgICAvLyBhY3R1YWxseSBoYXMgY2hhbmdlZCwgYF9pbml0TG9naW5Mb2dpY2Agd2lsbCBiZSBjYWxsZWQgYW5kIG1hbmFnZXNcclxuICAgICAgICAgICAgICAgIC8vIGJ1c3kgc3RhdGUgZm9yIGl0cyBvd24gbGl2ZW5lc3MgY2hlY2suXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiUHJvYmxlbSBwYXJzaW5nIFVSTCBvciB1bmhhbmRsZWQgZXJyb3IgZG9pbmcgLndlbGwta25vd24gZGlzY292ZXJ5OlwiLCBlKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgbWVzc2FnZSA9IF90KFwiRmFpbGVkIHRvIHBlcmZvcm0gaG9tZXNlcnZlciBkaXNjb3ZlcnlcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZS50cmFuc2xhdGVkTWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBlLnRyYW5zbGF0ZWRNZXNzYWdlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGxldCBlcnJvclRleHQgPSBtZXNzYWdlO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRpc2NvdmVyeVN0YXRlID0ge307XHJcbiAgICAgICAgICAgICAgICBpZiAoQXV0b0Rpc2NvdmVyeVV0aWxzLmlzTGl2ZWxpbmVzc0Vycm9yKGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0ID0gdGhpcy5zdGF0ZS5lcnJvclRleHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzY292ZXJ5U3RhdGUgPSBBdXRvRGlzY292ZXJ5VXRpbHMuYXV0aENvbXBvbmVudFN0YXRlRm9yRXJyb3IoZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0LFxyXG4gICAgICAgICAgICAgICAgICAgIC4uLmRpc2NvdmVyeVN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUGhvbmVDb3VudHJ5Q2hhbmdlZDogZnVuY3Rpb24ocGhvbmVDb3VudHJ5KSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHBob25lQ291bnRyeTogcGhvbmVDb3VudHJ5IH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblBob25lTnVtYmVyQ2hhbmdlZDogZnVuY3Rpb24ocGhvbmVOdW1iZXIpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhvbmVOdW1iZXI6IHBob25lTnVtYmVyLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblBob25lTnVtYmVyQmx1cjogZnVuY3Rpb24ocGhvbmVOdW1iZXIpIHtcclxuICAgICAgICAvLyBWYWxpZGF0ZSB0aGUgcGhvbmUgbnVtYmVyIGVudGVyZWRcclxuICAgICAgICBpZiAoIVBIT05FX05VTUJFUl9SRUdFWC50ZXN0KHBob25lTnVtYmVyKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dDogX3QoJ1RoZSBwaG9uZSBudW1iZXIgZW50ZXJlZCBsb29rcyBpbnZhbGlkJyksXHJcbiAgICAgICAgICAgICAgICBjYW5UcnlMb2dpbjogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3JUZXh0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY2FuVHJ5TG9naW46IHRydWUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25SZWdpc3RlckNsaWNrOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vblJlZ2lzdGVyQ2xpY2soKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25UcnlSZWdpc3RlckNsaWNrOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGNvbnN0IHN0ZXAgPSB0aGlzLl9nZXRDdXJyZW50Rmxvd1N0ZXAoKTtcclxuICAgICAgICBpZiAoc3RlcCA9PT0gJ20ubG9naW4uc3NvJyB8fCBzdGVwID09PSAnbS5sb2dpbi5jYXMnKSB7XHJcbiAgICAgICAgICAgIC8vIElmIHdlJ3JlIHNob3dpbmcgU1NPIGl0IG1lYW5zIHRoYXQgcmVnaXN0cmF0aW9uIGlzIGFsc28gcHJvYmFibHkgZGlzYWJsZWQsXHJcbiAgICAgICAgICAgIC8vIHNvIGludGVyY2VwdCB0aGUgY2xpY2sgYW5kIGluc3RlYWQgcHJldGVuZCB0aGUgdXNlciBjbGlja2VkICdTaWduIGluIHdpdGggU1NPJy5cclxuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNzb0tpbmQgPSBzdGVwID09PSAnbS5sb2dpbi5zc28nID8gJ3NzbycgOiAnY2FzJztcclxuICAgICAgICAgICAgUGxhdGZvcm1QZWcuZ2V0KCkuc3RhcnRTaW5nbGVTaWduT24odGhpcy5fbG9naW5Mb2dpYy5jcmVhdGVUZW1wb3JhcnlDbGllbnQoKSwgc3NvS2luZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gRG9uJ3QgaW50ZXJjZXB0IC0ganVzdCBnbyB0aHJvdWdoIHRvIHRoZSByZWdpc3RlciBwYWdlXHJcbiAgICAgICAgICAgIHRoaXMub25SZWdpc3RlckNsaWNrKGV2KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGFzeW5jIG9uU2VydmVyRGV0YWlsc05leHRQaGFzZUNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfTE9HSU4sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRWRpdFNlcnZlckRldGFpbHNDbGljayhldikge1xyXG4gICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9TRVJWRVJfREVUQUlMUyxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2luaXRMb2dpbkxvZ2ljOiBhc3luYyBmdW5jdGlvbihoc1VybCwgaXNVcmwpIHtcclxuICAgICAgICBoc1VybCA9IGhzVXJsIHx8IHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmhzVXJsO1xyXG4gICAgICAgIGlzVXJsID0gaXNVcmwgfHwgdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmw7XHJcblxyXG4gICAgICAgIGxldCBpc0RlZmF1bHRTZXJ2ZXIgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNEZWZhdWx0XHJcbiAgICAgICAgICAgICYmIGhzVXJsID09PSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc1VybFxyXG4gICAgICAgICAgICAmJiBpc1VybCA9PT0gdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmwpIHtcclxuICAgICAgICAgICAgaXNEZWZhdWx0U2VydmVyID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZhbGxiYWNrSHNVcmwgPSBpc0RlZmF1bHRTZXJ2ZXIgPyB0aGlzLnByb3BzLmZhbGxiYWNrSHNVcmwgOiBudWxsO1xyXG5cclxuICAgICAgICBjb25zdCBsb2dpbkxvZ2ljID0gbmV3IExvZ2luKGhzVXJsLCBpc1VybCwgZmFsbGJhY2tIc1VybCwge1xyXG4gICAgICAgICAgICBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWU6IHRoaXMucHJvcHMuZGVmYXVsdERldmljZURpc3BsYXlOYW1lLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuX2xvZ2luTG9naWMgPSBsb2dpbkxvZ2ljO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogdHJ1ZSxcclxuICAgICAgICAgICAgY3VycmVudEZsb3c6IG51bGwsIC8vIHJlc2V0IGZsb3dcclxuICAgICAgICAgICAgbG9naW5JbmNvcnJlY3Q6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBEbyBhIHF1aWNrIGxpdmVsaW5lc3MgY2hlY2sgb24gdGhlIFVSTHNcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCB7IHdhcm5pbmcgfSA9XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBBdXRvRGlzY292ZXJ5VXRpbHMudmFsaWRhdGVTZXJ2ZXJDb25maWdXaXRoU3RhdGljVXJscyhoc1VybCwgaXNVcmwpO1xyXG4gICAgICAgICAgICBpZiAod2FybmluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uQXV0b0Rpc2NvdmVyeVV0aWxzLmF1dGhDb21wb25lbnRTdGF0ZUZvckVycm9yKHdhcm5pbmcpLFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yVGV4dDogXCJcIixcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VydmVySXNBbGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvclRleHQ6IFwiXCIsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIC4uLkF1dG9EaXNjb3ZlcnlVdGlscy5hdXRoQ29tcG9uZW50U3RhdGVGb3JFcnJvcihlKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnNlcnZlckVycm9ySXNGYXRhbCkge1xyXG4gICAgICAgICAgICAgICAgLy8gU2VydmVyIGlzIGRlYWQ6IHNob3cgc2VydmVyIGRldGFpbHMgcHJvbXB0IGluc3RlYWRcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9TRVJWRVJfREVUQUlMUyxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsb2dpbkxvZ2ljLmdldEZsb3dzKCkudGhlbigoZmxvd3MpID0+IHtcclxuICAgICAgICAgICAgLy8gbG9vayBmb3IgYSBmbG93IHdoZXJlIHdlIHVuZGVyc3RhbmQgYWxsIG9mIHRoZSBzdGVwcy5cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmbG93cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5faXNTdXBwb3J0ZWRGbG93KGZsb3dzW2ldKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIHdlIGp1c3QgcGljayB0aGUgZmlyc3QgZmxvdyB3aGVyZSB3ZSBzdXBwb3J0IGFsbCB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHN0ZXBzLiAod2UgZG9uJ3QgaGF2ZSBhIFVJIGZvciBtdWx0aXBsZSBsb2dpbnMgc28gbGV0J3Mgc2tpcFxyXG4gICAgICAgICAgICAgICAgLy8gdGhhdCBmb3Igbm93KS5cclxuICAgICAgICAgICAgICAgIGxvZ2luTG9naWMuY2hvb3NlRmxvdyhpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRGbG93OiB0aGlzLl9nZXRDdXJyZW50Rmxvd1N0ZXAoKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHdlIGdvdCB0byB0aGUgZW5kIG9mIHRoZSBsaXN0IHdpdGhvdXQgZmluZGluZyBhIHN1aXRhYmxlXHJcbiAgICAgICAgICAgIC8vIGZsb3cuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3JUZXh0OiBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlRoaXMgaG9tZXNlcnZlciBkb2Vzbid0IG9mZmVyIGFueSBsb2dpbiBmbG93cyB3aGljaCBhcmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInN1cHBvcnRlZCBieSB0aGlzIGNsaWVudC5cIixcclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQ6IHRoaXMuX2Vycm9yVGV4dEZyb21FcnJvcihlcnIpLFxyXG4gICAgICAgICAgICAgICAgbG9naW5JbmNvcnJlY3Q6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY2FuVHJ5TG9naW46IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9pc1N1cHBvcnRlZEZsb3c6IGZ1bmN0aW9uKGZsb3cpIHtcclxuICAgICAgICAvLyB0ZWNobmljYWxseSB0aGUgZmxvdyBjYW4gaGF2ZSBtdWx0aXBsZSBzdGVwcywgYnV0IG5vIG9uZSBkb2VzIHRoaXNcclxuICAgICAgICAvLyBmb3IgbG9naW4gYW5kIGxvZ2luTG9naWMgZG9lc24ndCBzdXBwb3J0IGl0IHNvIHdlIGNhbiBpZ25vcmUgaXQuXHJcbiAgICAgICAgaWYgKCF0aGlzLl9zdGVwUmVuZGVyZXJNYXBbZmxvdy50eXBlXSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNraXBwaW5nIGZsb3dcIiwgZmxvdywgXCJkdWUgdG8gdW5zdXBwb3J0ZWQgbG9naW4gdHlwZVwiLCBmbG93LnR5cGUpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0Q3VycmVudEZsb3dTdGVwOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbG9naW5Mb2dpYyA/IHRoaXMuX2xvZ2luTG9naWMuZ2V0Q3VycmVudEZsb3dTdGVwKCkgOiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZXJyb3JUZXh0RnJvbUVycm9yKGVycikge1xyXG4gICAgICAgIGxldCBlcnJDb2RlID0gZXJyLmVycmNvZGU7XHJcbiAgICAgICAgaWYgKCFlcnJDb2RlICYmIGVyci5odHRwU3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGVyckNvZGUgPSBcIkhUVFAgXCIgKyBlcnIuaHR0cFN0YXR1cztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBlcnJvclRleHQgPSBfdChcIkVycm9yOiBQcm9ibGVtIGNvbW11bmljYXRpbmcgd2l0aCB0aGUgZ2l2ZW4gaG9tZXNlcnZlci5cIikgK1xyXG4gICAgICAgICAgICAgICAgKGVyckNvZGUgPyBcIiAoXCIgKyBlcnJDb2RlICsgXCIpXCIgOiBcIlwiKTtcclxuXHJcbiAgICAgICAgaWYgKGVyci5jb3JzID09PSAncmVqZWN0ZWQnKSB7XHJcbiAgICAgICAgICAgIGlmICh3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgPT09ICdodHRwczonICYmXHJcbiAgICAgICAgICAgICAgICAodGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNVcmwuc3RhcnRzV2l0aChcImh0dHA6XCIpIHx8XHJcbiAgICAgICAgICAgICAgICAgIXRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmhzVXJsLnN0YXJ0c1dpdGgoXCJodHRwXCIpKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IDxzcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJDYW4ndCBjb25uZWN0IHRvIGhvbWVzZXJ2ZXIgdmlhIEhUVFAgd2hlbiBhbiBIVFRQUyBVUkwgaXMgaW4geW91ciBicm93c2VyIGJhci4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIkVpdGhlciB1c2UgSFRUUFMgb3IgPGE+ZW5hYmxlIHVuc2FmZSBzY3JpcHRzPC9hPi5cIiwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdhJzogKHN1YikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8YSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vc2VhcmNoPyZxPWVuYWJsZSUyMHVuc2FmZSUyMHNjcmlwdHNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdWIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IDxzcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJDYW4ndCBjb25uZWN0IHRvIGhvbWVzZXJ2ZXIgLSBwbGVhc2UgY2hlY2sgeW91ciBjb25uZWN0aXZpdHksIGVuc3VyZSB5b3VyIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8YT5ob21lc2VydmVyJ3MgU1NMIGNlcnRpZmljYXRlPC9hPiBpcyB0cnVzdGVkLCBhbmQgdGhhdCBhIGJyb3dzZXIgZXh0ZW5zaW9uIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJpcyBub3QgYmxvY2tpbmcgcmVxdWVzdHMuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYSc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgdGFyZ2V0PVwiX2JsYW5rXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIGhyZWY9e3RoaXMucHJvcHMuc2VydmVyQ29uZmlnLmhzVXJsfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdWIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKSB9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZXJyb3JUZXh0O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJTZXJ2ZXJDb21wb25lbnQoKSB7XHJcbiAgICAgICAgY29uc3QgU2VydmVyQ29uZmlnID0gc2RrLmdldENvbXBvbmVudChcImF1dGguU2VydmVyQ29uZmlnXCIpO1xyXG5cclxuICAgICAgICBpZiAoU2RrQ29uZmlnLmdldCgpWydkaXNhYmxlX2N1c3RvbV91cmxzJ10pIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoUEhBU0VTX0VOQUJMRUQgJiYgdGhpcy5zdGF0ZS5waGFzZSAhPT0gUEhBU0VfU0VSVkVSX0RFVEFJTFMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzZXJ2ZXJEZXRhaWxzUHJvcHMgPSB7fTtcclxuICAgICAgICBpZiAoUEhBU0VTX0VOQUJMRUQpIHtcclxuICAgICAgICAgICAgc2VydmVyRGV0YWlsc1Byb3BzLm9uQWZ0ZXJTdWJtaXQgPSB0aGlzLm9uU2VydmVyRGV0YWlsc05leHRQaGFzZUNsaWNrO1xyXG4gICAgICAgICAgICBzZXJ2ZXJEZXRhaWxzUHJvcHMuc3VibWl0VGV4dCA9IF90KFwiTmV4dFwiKTtcclxuICAgICAgICAgICAgc2VydmVyRGV0YWlsc1Byb3BzLnN1Ym1pdENsYXNzID0gXCJteF9Mb2dpbl9zdWJtaXRcIjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8U2VydmVyQ29uZmlnXHJcbiAgICAgICAgICAgIHNlcnZlckNvbmZpZz17dGhpcy5wcm9wcy5zZXJ2ZXJDb25maWd9XHJcbiAgICAgICAgICAgIG9uU2VydmVyQ29uZmlnQ2hhbmdlPXt0aGlzLnByb3BzLm9uU2VydmVyQ29uZmlnQ2hhbmdlfVxyXG4gICAgICAgICAgICBkZWxheVRpbWVNcz17MjUwfVxyXG4gICAgICAgICAgICB7Li4uc2VydmVyRGV0YWlsc1Byb3BzfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJMb2dpbkNvbXBvbmVudEZvclN0ZXAoKSB7XHJcbiAgICAgICAgaWYgKFBIQVNFU19FTkFCTEVEICYmIHRoaXMuc3RhdGUucGhhc2UgIT09IFBIQVNFX0xPR0lOKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc3RlcCA9IHRoaXMuc3RhdGUuY3VycmVudEZsb3c7XHJcblxyXG4gICAgICAgIGlmICghc3RlcCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHN0ZXBSZW5kZXJlciA9IHRoaXMuX3N0ZXBSZW5kZXJlck1hcFtzdGVwXTtcclxuXHJcbiAgICAgICAgaWYgKHN0ZXBSZW5kZXJlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gc3RlcFJlbmRlcmVyKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0sXHJcblxyXG4gICAgX3JlbmRlclBhc3N3b3JkU3RlcDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCItLS1wYXJhbXMgLS0tLVwiLCB0aGlzLnByb3BzLnN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcylcclxuICAgICAgIFxyXG4gICAgICAgIC8vIHJldHVybjtcclxuICAgICAgICBjb25zdCBQYXNzd29yZExvZ2luID0gc2RrLmdldENvbXBvbmVudCgnYXV0aC5QYXNzd29yZExvZ2luJyk7XHJcblxyXG4gICAgICAgIGxldCBvbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2sgPSBudWxsO1xyXG4gICAgICAgIC8vIElmIGN1c3RvbSBVUkxzIGFyZSBhbGxvd2VkLCB3aXJlIHVwIHRoZSBzZXJ2ZXIgZGV0YWlscyBlZGl0IGxpbmsuXHJcbiAgICAgICAgaWYgKFBIQVNFU19FTkFCTEVEICYmICFTZGtDb25maWcuZ2V0KClbJ2Rpc2FibGVfY3VzdG9tX3VybHMnXSkge1xyXG4gICAgICAgICAgICBvbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2sgPSB0aGlzLm9uRWRpdFNlcnZlckRldGFpbHNDbGljaztcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvLyA8aDM+TG9hZGluZyB7dGhpcy5vblBhc3N3b3JkTG9naW4oJ2hhbXNoaWlraGFuJyxcclxuICAgICAgICAgICAgLy8gJycsXHJcbiAgICAgICAgICAgIC8vICcnLFxyXG4gICAgICAgICAgICAvLyAnaGFtc2hhQDEyMyEnKX08L2gzPlxyXG4gICAgICAgICAgICA8UGFzc3dvcmRMb2dpblxyXG4gICAgICAgICAgICAgICBvblN1Ym1pdD17dGhpcy5vblBhc3N3b3JkTG9naW59XHJcbiAgICAgICAgICAgICAgIG9uRXJyb3I9e3RoaXMub25QYXNzd29yZExvZ2luRXJyb3J9XHJcbiAgICAgICAgICAgICAgIG9uRWRpdFNlcnZlckRldGFpbHNDbGljaz17b25FZGl0U2VydmVyRGV0YWlsc0NsaWNrfVxyXG4gICAgICAgICAgICAgICBpbml0aWFsVXNlcm5hbWU9e3RoaXMucHJvcHMuc3RhcnRpbmdGcmFnbWVudFF1ZXJ5UGFyYW1zLnVzZXJuYW1lfVxyXG4gICAgICAgICAgICAgICBwYXNzd29yZCA9IHt0aGlzLnByb3BzLnN0YXJ0aW5nRnJhZ21lbnRRdWVyeVBhcmFtcy5wYXNzd29yZH1cclxuICAgICAgICAgICAgICAgaW5pdGlhbFBob25lQ291bnRyeT17dGhpcy5zdGF0ZS5waG9uZUNvdW50cnl9XHJcbiAgICAgICAgICAgICAgIGluaXRpYWxQaG9uZU51bWJlcj17dGhpcy5zdGF0ZS5waG9uZU51bWJlcn1cclxuICAgICAgICAgICAgICAgb25Vc2VybmFtZUNoYW5nZWQ9e3RoaXMub25Vc2VybmFtZUNoYW5nZWR9XHJcbiAgICAgICAgICAgICAgIG9uVXNlcm5hbWVCbHVyPXt0aGlzLm9uVXNlcm5hbWVCbHVyfVxyXG4gICAgICAgICAgICAgICBvblBob25lQ291bnRyeUNoYW5nZWQ9e3RoaXMub25QaG9uZUNvdW50cnlDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICBvblBob25lTnVtYmVyQ2hhbmdlZD17dGhpcy5vblBob25lTnVtYmVyQ2hhbmdlZH1cclxuICAgICAgICAgICAgICAgb25QaG9uZU51bWJlckJsdXI9e3RoaXMub25QaG9uZU51bWJlckJsdXJ9XHJcbiAgICAgICAgICAgICAgIG9uRm9yZ290UGFzc3dvcmRDbGljaz17dGhpcy5wcm9wcy5vbkZvcmdvdFBhc3N3b3JkQ2xpY2t9XHJcbiAgICAgICAgICAgICAgIGxvZ2luSW5jb3JyZWN0PXt0aGlzLnN0YXRlLmxvZ2luSW5jb3JyZWN0fVxyXG4gICAgICAgICAgICAgICBzZXJ2ZXJDb25maWc9e3RoaXMucHJvcHMuc2VydmVyQ29uZmlnfVxyXG4gICAgICAgICAgICAgICBkaXNhYmxlU3VibWl0PXt0aGlzLmlzQnVzeSgpfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9yZW5kZXJTc29TdGVwOiBmdW5jdGlvbihsb2dpblR5cGUpIHtcclxuICAgICAgICBjb25zdCBTaWduSW5Ub1RleHQgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5hdXRoLlNpZ25JblRvVGV4dCcpO1xyXG5cclxuICAgICAgICBsZXQgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrID0gbnVsbDtcclxuICAgICAgICAvLyBJZiBjdXN0b20gVVJMcyBhcmUgYWxsb3dlZCwgd2lyZSB1cCB0aGUgc2VydmVyIGRldGFpbHMgZWRpdCBsaW5rLlxyXG4gICAgICAgIGlmIChQSEFTRVNfRU5BQkxFRCAmJiAhU2RrQ29uZmlnLmdldCgpWydkaXNhYmxlX2N1c3RvbV91cmxzJ10pIHtcclxuICAgICAgICAgICAgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrID0gdGhpcy5vbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFhYWDogVGhpcyBsaW5rIGRvZXMgKm5vdCogaGF2ZSBhIHRhcmdldD1cIl9ibGFua1wiIGJlY2F1c2Ugc2luZ2xlIHNpZ24tb24gcmVsaWVzIG9uXHJcbiAgICAgICAgLy8gcmVkaXJlY3RpbmcgdGhlIHVzZXIgYmFjayB0byBhIFVSSSBvbmNlIHRoZXkncmUgbG9nZ2VkIGluLiBPbiB0aGUgd2ViLCB0aGlzIG1lYW5zXHJcbiAgICAgICAgLy8gd2UgdXNlIHRoZSBzYW1lIHdpbmRvdyBhbmQgcmVkaXJlY3QgYmFjayB0byByaW90LiBPbiBlbGVjdHJvbiwgdGhpcyBhY3R1YWxseVxyXG4gICAgICAgIC8vIG9wZW5zIHRoZSBTU08gcGFnZSBpbiB0aGUgZWxlY3Ryb24gYXBwIGl0c2VsZiBkdWUgdG9cclxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vZWxlY3Ryb24vZWxlY3Ryb24vaXNzdWVzLzg4NDEgYW5kIHNvIGhhcHBlbnMgdG8gd29yay5cclxuICAgICAgICAvLyBJZiB0aGlzIGJ1ZyBnZXRzIGZpeGVkLCBpdCB3aWxsIGJyZWFrIFNTTyBzaW5jZSBpdCB3aWxsIG9wZW4gdGhlIFNTTyBwYWdlIGluIHRoZVxyXG4gICAgICAgIC8vIHVzZXIncyBicm93c2VyLCBsZXQgdGhlbSBsb2cgaW50byB0aGVpciBTU08gcHJvdmlkZXIsIHRoZW4gcmVkaXJlY3QgdGhlaXIgYnJvd3NlclxyXG4gICAgICAgIC8vIHRvIHZlY3RvcjovL3ZlY3RvciB3aGljaCwgb2YgY291cnNlLCB3aWxsIG5vdCB3b3JrLlxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7LyogPFNpZ25JblRvVGV4dCBzZXJ2ZXJDb25maWc9e3RoaXMucHJvcHMuc2VydmVyQ29uZmlnfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uRWRpdFNlcnZlckRldGFpbHNDbGljaz17b25FZGl0U2VydmVyRGV0YWlsc0NsaWNrfSAvPlxyXG5cclxuICAgICAgICAgICAgICAgIDxTU09CdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Mb2dpbl9zc29fbGluayBteF9Mb2dpbl9zdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIG1hdHJpeENsaWVudD17dGhpcy5fbG9naW5Mb2dpYy5jcmVhdGVUZW1wb3JhcnlDbGllbnQoKX1cclxuICAgICAgICAgICAgICAgICAgICBsb2dpblR5cGU9e2xvZ2luVHlwZX0gLz4gKi99XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgY29uc3QgQXV0aEhlYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJhdXRoLkF1dGhIZWFkZXJcIik7XHJcbiAgICAgICAgY29uc3QgQXV0aEJvZHkgPSBzZGsuZ2V0Q29tcG9uZW50KFwiYXV0aC5BdXRoQm9keVwiKTtcclxuICAgICAgICBjb25zdCBsb2FkZXIgPSB0aGlzLmlzQnVzeSgpID8gPGRpdiBjbGFzc05hbWU9XCJteF9Mb2dpbl9sb2FkZXJcIj48TG9hZGVyIC8+PC9kaXY+IDogbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgZXJyb3JUZXh0ID0gdGhpcy5zdGF0ZS5lcnJvclRleHQ7XHJcblxyXG4gICAgICAgIGxldCBlcnJvclRleHRTZWN0aW9uO1xyXG4gICAgICAgIGlmIChlcnJvclRleHQpIHtcclxuICAgICAgICAgICAgZXJyb3JUZXh0U2VjdGlvbiA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTG9naW5fZXJyb3JcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGVycm9yVGV4dCB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzZXJ2ZXJEZWFkU2VjdGlvbjtcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2VydmVySXNBbGl2ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICAgICBcIm14X0xvZ2luX2Vycm9yXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBcIm14X0xvZ2luX3NlcnZlckVycm9yXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBcIm14X0xvZ2luX3NlcnZlckVycm9yTm9uRmF0YWxcIjogIXRoaXMuc3RhdGUuc2VydmVyRXJyb3JJc0ZhdGFsLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgc2VydmVyRGVhZFNlY3Rpb24gPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuc2VydmVyRGVhZEVycm9yfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QXV0aFBhZ2U+XHJcbiAgICAgICAgICAgICAgICA8QXV0aEhlYWRlciAvPlxyXG4gICAgICAgICAgICAgICAgPEF1dGhCb2R5PlxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdCgnU2lnbiBpbicpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7bG9hZGVyfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvaDI+ICovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgZXJyb3JUZXh0U2VjdGlvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBzZXJ2ZXJEZWFkU2VjdGlvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnJlbmRlclNlcnZlckNvbXBvbmVudCgpIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMucmVuZGVyTG9naW5Db21wb25lbnRGb3JTdGVwKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiA8YSBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9jaGFuZ2VGbG93XCIgb25DbGljaz17dGhpcy5vblRyeVJlZ2lzdGVyQ2xpY2t9IGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ0NyZWF0ZSBhY2NvdW50JykgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvYT4gKi99XHJcbiAgICAgICAgICAgICAgICA8L0F1dGhCb2R5PlxyXG4gICAgICAgICAgICA8L0F1dGhQYWdlPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19