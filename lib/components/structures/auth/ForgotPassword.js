"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _PasswordReset = _interopRequireDefault(require("../../../PasswordReset"));

var _AutoDiscoveryUtils = _interopRequireWildcard(require("../../../utils/AutoDiscoveryUtils"));

var _classnames = _interopRequireDefault(require("classnames"));

var _AuthPage = _interopRequireDefault(require("../../views/auth/AuthPage"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Phases
// Show controls to configure server details
const PHASE_SERVER_DETAILS = 0; // Show the forgot password inputs

const PHASE_FORGOT = 1; // Email is in the process of being sent

const PHASE_SENDING_EMAIL = 2; // Email has been sent

const PHASE_EMAIL_SENT = 3; // User has clicked the link in email and completed reset

const PHASE_DONE = 4;

var _default = (0, _createReactClass.default)({
  displayName: 'ForgotPassword',
  propTypes: {
    serverConfig: _propTypes.default.instanceOf(_AutoDiscoveryUtils.ValidatedServerConfig).isRequired,
    onServerConfigChange: _propTypes.default.func.isRequired,
    onLoginClick: _propTypes.default.func,
    onComplete: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    return {
      phase: PHASE_FORGOT,
      email: "",
      password: "",
      password2: "",
      errorText: null,
      // We perform liveliness checks later, but for now suppress the errors.
      // We also track the server dead errors independently of the regular errors so
      // that we can render it differently, and override any other error the user may
      // be seeing.
      serverIsAlive: true,
      serverErrorIsFatal: false,
      serverDeadError: "",
      serverRequiresIdServer: null
    };
  },
  componentDidMount: function () {
    this.reset = null;

    this._checkServerLiveliness(this.props.serverConfig);
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (newProps) {
    if (newProps.serverConfig.hsUrl === this.props.serverConfig.hsUrl && newProps.serverConfig.isUrl === this.props.serverConfig.isUrl) return; // Do a liveliness check on the new URLs

    this._checkServerLiveliness(newProps.serverConfig);
  },
  _checkServerLiveliness: async function (serverConfig) {
    try {
      await _AutoDiscoveryUtils.default.validateServerConfigWithStaticUrls(serverConfig.hsUrl, serverConfig.isUrl);
      const pwReset = new _PasswordReset.default(serverConfig.hsUrl, serverConfig.isUrl);
      const serverRequiresIdServer = await pwReset.doesServerRequireIdServerParam();
      this.setState({
        serverIsAlive: true,
        serverRequiresIdServer
      });
    } catch (e) {
      this.setState(_AutoDiscoveryUtils.default.authComponentStateForError(e, "forgot_password"));
    }
  },
  submitPasswordReset: function (email, password) {
    this.setState({
      phase: PHASE_SENDING_EMAIL
    });
    this.reset = new _PasswordReset.default(this.props.serverConfig.hsUrl, this.props.serverConfig.isUrl);
    this.reset.resetPassword(email, password).then(() => {
      this.setState({
        phase: PHASE_EMAIL_SENT
      });
    }, err => {
      this.showErrorDialog((0, _languageHandler._t)('Failed to send email') + ": " + err.message);
      this.setState({
        phase: PHASE_FORGOT
      });
    });
  },
  onVerify: async function (ev) {
    ev.preventDefault();

    if (!this.reset) {
      console.error("onVerify called before submitPasswordReset!");
      return;
    }

    try {
      await this.reset.checkEmailLinkClicked();
      this.setState({
        phase: PHASE_DONE
      });
    } catch (err) {
      this.showErrorDialog(err.message);
    }
  },
  onSubmitForm: async function (ev) {
    ev.preventDefault(); // refresh the server errors, just in case the server came back online

    await this._checkServerLiveliness(this.props.serverConfig);

    if (!this.state.email) {
      this.showErrorDialog((0, _languageHandler._t)('The email address linked to your account must be entered.'));
    } else if (!this.state.password || !this.state.password2) {
      this.showErrorDialog((0, _languageHandler._t)('A new password must be entered.'));
    } else if (this.state.password !== this.state.password2) {
      this.showErrorDialog((0, _languageHandler._t)('New passwords must match each other.'));
    } else {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      _Modal.default.createTrackedDialog('Forgot Password Warning', '', QuestionDialog, {
        title: (0, _languageHandler._t)('Warning!'),
        description: _react.default.createElement("div", null, (0, _languageHandler._t)("Changing your password will reset any end-to-end encryption keys " + "on all of your sessions, making encrypted chat history unreadable. Set up " + "Key Backup or export your room keys from another session before resetting your " + "password.")),
        button: (0, _languageHandler._t)('Continue'),
        onFinished: confirmed => {
          if (confirmed) {
            this.submitPasswordReset(this.state.email, this.state.password);
          }
        }
      });
    }
  },
  onInputChanged: function (stateKey, ev) {
    this.setState({
      [stateKey]: ev.target.value
    });
  },

  async onServerDetailsNextPhaseClick() {
    this.setState({
      phase: PHASE_FORGOT
    });
  },

  onEditServerDetailsClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.setState({
      phase: PHASE_SERVER_DETAILS
    });
  },

  onLoginClick: function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.props.onLoginClick();
  },
  showErrorDialog: function (body, title) {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

    _Modal.default.createTrackedDialog('Forgot Password Error', '', ErrorDialog, {
      title: title,
      description: body
    });
  },

  renderServerDetails() {
    const ServerConfig = sdk.getComponent("auth.ServerConfig");

    if (_SdkConfig.default.get()['disable_custom_urls']) {
      return null;
    }

    return _react.default.createElement(ServerConfig, {
      serverConfig: this.props.serverConfig,
      onServerConfigChange: this.props.onServerConfigChange,
      delayTimeMs: 0,
      showIdentityServerIfRequiredByHomeserver: true,
      onAfterSubmit: this.onServerDetailsNextPhaseClick,
      submitText: (0, _languageHandler._t)("Next"),
      submitClass: "mx_Login_submit"
    });
  },

  renderForgot() {
    const Field = sdk.getComponent('elements.Field');
    let errorText = null;
    const err = this.state.errorText;

    if (err) {
      errorText = _react.default.createElement("div", {
        className: "mx_Login_error"
      }, err);
    }

    let serverDeadSection;

    if (!this.state.serverIsAlive) {
      const classes = (0, _classnames.default)({
        "mx_Login_error": true,
        "mx_Login_serverError": true,
        "mx_Login_serverErrorNonFatal": !this.state.serverErrorIsFatal
      });
      serverDeadSection = _react.default.createElement("div", {
        className: classes
      }, this.state.serverDeadError);
    }

    let yourMatrixAccountText = (0, _languageHandler._t)('Your Matrix account on %(serverName)s', {
      serverName: this.props.serverConfig.hsName
    });

    if (this.props.serverConfig.hsNameIsDifferent) {
      const TextWithTooltip = sdk.getComponent("elements.TextWithTooltip");
      yourMatrixAccountText = (0, _languageHandler._t)('Your Matrix account on <underlinedServerName />', {}, {
        'underlinedServerName': () => {
          return _react.default.createElement(TextWithTooltip, {
            class: "mx_Login_underlinedServerName",
            tooltip: this.props.serverConfig.hsUrl
          }, this.props.serverConfig.hsName);
        }
      });
    } // If custom URLs are allowed, wire up the server details edit link.


    let editLink = null;

    if (!_SdkConfig.default.get()['disable_custom_urls']) {
      editLink = _react.default.createElement("a", {
        className: "mx_AuthBody_editServerDetails",
        href: "#",
        onClick: this.onEditServerDetailsClick
      }, (0, _languageHandler._t)('Change'));
    }

    if (!this.props.serverConfig.isUrl && this.state.serverRequiresIdServer) {
      return _react.default.createElement("div", null, _react.default.createElement("h3", null, yourMatrixAccountText, editLink), (0, _languageHandler._t)("No identity server is configured: " + "add one in server settings to reset your password."), _react.default.createElement("a", {
        className: "mx_AuthBody_changeFlow",
        onClick: this.onLoginClick,
        href: "#"
      }, (0, _languageHandler._t)('Sign in instead')));
    }

    return _react.default.createElement("div", null, errorText, serverDeadSection, _react.default.createElement("h3", null, yourMatrixAccountText, editLink), _react.default.createElement("form", {
      onSubmit: this.onSubmitForm
    }, _react.default.createElement("div", {
      className: "mx_AuthBody_fieldRow"
    }, _react.default.createElement(Field, {
      name: "reset_email" // define a name so browser's password autofill gets less confused
      ,
      type: "text",
      label: (0, _languageHandler._t)('Email'),
      value: this.state.email,
      onChange: this.onInputChanged.bind(this, "email"),
      autoFocus: true
    })), _react.default.createElement("div", {
      className: "mx_AuthBody_fieldRow"
    }, _react.default.createElement(Field, {
      name: "reset_password",
      type: "password",
      label: (0, _languageHandler._t)('Password'),
      value: this.state.password,
      onChange: this.onInputChanged.bind(this, "password")
    }), _react.default.createElement(Field, {
      name: "reset_password_confirm",
      type: "password",
      label: (0, _languageHandler._t)('Confirm'),
      value: this.state.password2,
      onChange: this.onInputChanged.bind(this, "password2")
    })), _react.default.createElement("span", null, (0, _languageHandler._t)('A verification email will be sent to your inbox to confirm ' + 'setting your new password.')), _react.default.createElement("input", {
      className: "mx_Login_submit",
      type: "submit",
      value: (0, _languageHandler._t)('Send Reset Email')
    })), _react.default.createElement("a", {
      className: "mx_AuthBody_changeFlow",
      onClick: this.onLoginClick,
      href: "#"
    }, (0, _languageHandler._t)('Sign in instead')));
  },

  renderSendingEmail() {
    const Spinner = sdk.getComponent("elements.Spinner");
    return _react.default.createElement(Spinner, null);
  },

  renderEmailSent() {
    return _react.default.createElement("div", null, (0, _languageHandler._t)("An email has been sent to %(emailAddress)s. Once you've followed the " + "link it contains, click below.", {
      emailAddress: this.state.email
    }), _react.default.createElement("br", null), _react.default.createElement("input", {
      className: "mx_Login_submit",
      type: "button",
      onClick: this.onVerify,
      value: (0, _languageHandler._t)('I have verified my email address')
    }));
  },

  renderDone() {
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your password has been reset.")), _react.default.createElement("p", null, (0, _languageHandler._t)("You have been logged out of all sessions and will no longer receive " + "push notifications. To re-enable notifications, sign in again on each " + "device.")), _react.default.createElement("input", {
      className: "mx_Login_submit",
      type: "button",
      onClick: this.props.onComplete,
      value: (0, _languageHandler._t)('Return to login screen')
    }));
  },

  render: function () {
    const AuthHeader = sdk.getComponent("auth.AuthHeader");
    const AuthBody = sdk.getComponent("auth.AuthBody");
    let resetPasswordJsx;

    switch (this.state.phase) {
      case PHASE_SERVER_DETAILS:
        resetPasswordJsx = this.renderServerDetails();
        break;

      case PHASE_FORGOT:
        resetPasswordJsx = this.renderForgot();
        break;

      case PHASE_SENDING_EMAIL:
        resetPasswordJsx = this.renderSendingEmail();
        break;

      case PHASE_EMAIL_SENT:
        resetPasswordJsx = this.renderEmailSent();
        break;

      case PHASE_DONE:
        resetPasswordJsx = this.renderDone();
        break;
    }

    return _react.default.createElement(_AuthPage.default, null, _react.default.createElement(AuthHeader, null), _react.default.createElement(AuthBody, null, _react.default.createElement("h2", null, " ", (0, _languageHandler._t)('Set a new password'), " "), resetPasswordJsx));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvYXV0aC9Gb3Jnb3RQYXNzd29yZC5qcyJdLCJuYW1lcyI6WyJQSEFTRV9TRVJWRVJfREVUQUlMUyIsIlBIQVNFX0ZPUkdPVCIsIlBIQVNFX1NFTkRJTkdfRU1BSUwiLCJQSEFTRV9FTUFJTF9TRU5UIiwiUEhBU0VfRE9ORSIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwic2VydmVyQ29uZmlnIiwiUHJvcFR5cGVzIiwiaW5zdGFuY2VPZiIsIlZhbGlkYXRlZFNlcnZlckNvbmZpZyIsImlzUmVxdWlyZWQiLCJvblNlcnZlckNvbmZpZ0NoYW5nZSIsImZ1bmMiLCJvbkxvZ2luQ2xpY2siLCJvbkNvbXBsZXRlIiwiZ2V0SW5pdGlhbFN0YXRlIiwicGhhc2UiLCJlbWFpbCIsInBhc3N3b3JkIiwicGFzc3dvcmQyIiwiZXJyb3JUZXh0Iiwic2VydmVySXNBbGl2ZSIsInNlcnZlckVycm9ySXNGYXRhbCIsInNlcnZlckRlYWRFcnJvciIsInNlcnZlclJlcXVpcmVzSWRTZXJ2ZXIiLCJjb21wb25lbnREaWRNb3VudCIsInJlc2V0IiwiX2NoZWNrU2VydmVyTGl2ZWxpbmVzcyIsInByb3BzIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMiLCJuZXdQcm9wcyIsImhzVXJsIiwiaXNVcmwiLCJBdXRvRGlzY292ZXJ5VXRpbHMiLCJ2YWxpZGF0ZVNlcnZlckNvbmZpZ1dpdGhTdGF0aWNVcmxzIiwicHdSZXNldCIsIlBhc3N3b3JkUmVzZXQiLCJkb2VzU2VydmVyUmVxdWlyZUlkU2VydmVyUGFyYW0iLCJzZXRTdGF0ZSIsImUiLCJhdXRoQ29tcG9uZW50U3RhdGVGb3JFcnJvciIsInN1Ym1pdFBhc3N3b3JkUmVzZXQiLCJyZXNldFBhc3N3b3JkIiwidGhlbiIsImVyciIsInNob3dFcnJvckRpYWxvZyIsIm1lc3NhZ2UiLCJvblZlcmlmeSIsImV2IiwicHJldmVudERlZmF1bHQiLCJjb25zb2xlIiwiZXJyb3IiLCJjaGVja0VtYWlsTGlua0NsaWNrZWQiLCJvblN1Ym1pdEZvcm0iLCJzdGF0ZSIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImJ1dHRvbiIsIm9uRmluaXNoZWQiLCJjb25maXJtZWQiLCJvbklucHV0Q2hhbmdlZCIsInN0YXRlS2V5IiwidGFyZ2V0IiwidmFsdWUiLCJvblNlcnZlckRldGFpbHNOZXh0UGhhc2VDbGljayIsIm9uRWRpdFNlcnZlckRldGFpbHNDbGljayIsInN0b3BQcm9wYWdhdGlvbiIsImJvZHkiLCJFcnJvckRpYWxvZyIsInJlbmRlclNlcnZlckRldGFpbHMiLCJTZXJ2ZXJDb25maWciLCJTZGtDb25maWciLCJnZXQiLCJyZW5kZXJGb3Jnb3QiLCJGaWVsZCIsInNlcnZlckRlYWRTZWN0aW9uIiwiY2xhc3NlcyIsInlvdXJNYXRyaXhBY2NvdW50VGV4dCIsInNlcnZlck5hbWUiLCJoc05hbWUiLCJoc05hbWVJc0RpZmZlcmVudCIsIlRleHRXaXRoVG9vbHRpcCIsImVkaXRMaW5rIiwiYmluZCIsInJlbmRlclNlbmRpbmdFbWFpbCIsIlNwaW5uZXIiLCJyZW5kZXJFbWFpbFNlbnQiLCJlbWFpbEFkZHJlc3MiLCJyZW5kZXJEb25lIiwicmVuZGVyIiwiQXV0aEhlYWRlciIsIkF1dGhCb2R5IiwicmVzZXRQYXNzd29yZEpzeCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFrQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBNUJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQThCQTtBQUNBO0FBQ0EsTUFBTUEsb0JBQW9CLEdBQUcsQ0FBN0IsQyxDQUNBOztBQUNBLE1BQU1DLFlBQVksR0FBRyxDQUFyQixDLENBQ0E7O0FBQ0EsTUFBTUMsbUJBQW1CLEdBQUcsQ0FBNUIsQyxDQUNBOztBQUNBLE1BQU1DLGdCQUFnQixHQUFHLENBQXpCLEMsQ0FDQTs7QUFDQSxNQUFNQyxVQUFVLEdBQUcsQ0FBbkI7O2VBRWUsK0JBQWlCO0FBQzVCQyxFQUFBQSxXQUFXLEVBQUUsZ0JBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxVQUFWLENBQXFCQyx5Q0FBckIsRUFBNENDLFVBRG5EO0FBRVBDLElBQUFBLG9CQUFvQixFQUFFSixtQkFBVUssSUFBVixDQUFlRixVQUY5QjtBQUdQRyxJQUFBQSxZQUFZLEVBQUVOLG1CQUFVSyxJQUhqQjtBQUlQRSxJQUFBQSxVQUFVLEVBQUVQLG1CQUFVSyxJQUFWLENBQWVGO0FBSnBCLEdBSGlCO0FBVTVCSyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLEtBQUssRUFBRWhCLFlBREo7QUFFSGlCLE1BQUFBLEtBQUssRUFBRSxFQUZKO0FBR0hDLE1BQUFBLFFBQVEsRUFBRSxFQUhQO0FBSUhDLE1BQUFBLFNBQVMsRUFBRSxFQUpSO0FBS0hDLE1BQUFBLFNBQVMsRUFBRSxJQUxSO0FBT0g7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsTUFBQUEsYUFBYSxFQUFFLElBWFo7QUFZSEMsTUFBQUEsa0JBQWtCLEVBQUUsS0FaakI7QUFhSEMsTUFBQUEsZUFBZSxFQUFFLEVBYmQ7QUFjSEMsTUFBQUEsc0JBQXNCLEVBQUU7QUFkckIsS0FBUDtBQWdCSCxHQTNCMkI7QUE2QjVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLEtBQUwsR0FBYSxJQUFiOztBQUNBLFNBQUtDLHNCQUFMLENBQTRCLEtBQUtDLEtBQUwsQ0FBV3RCLFlBQXZDO0FBQ0gsR0FoQzJCO0FBa0M1QjtBQUNBdUIsRUFBQUEsZ0NBQWdDLEVBQUUsVUFBU0MsUUFBVCxFQUFtQjtBQUNqRCxRQUFJQSxRQUFRLENBQUN4QixZQUFULENBQXNCeUIsS0FBdEIsS0FBZ0MsS0FBS0gsS0FBTCxDQUFXdEIsWUFBWCxDQUF3QnlCLEtBQXhELElBQ0FELFFBQVEsQ0FBQ3hCLFlBQVQsQ0FBc0IwQixLQUF0QixLQUFnQyxLQUFLSixLQUFMLENBQVd0QixZQUFYLENBQXdCMEIsS0FENUQsRUFDbUUsT0FGbEIsQ0FJakQ7O0FBQ0EsU0FBS0wsc0JBQUwsQ0FBNEJHLFFBQVEsQ0FBQ3hCLFlBQXJDO0FBQ0gsR0F6QzJCO0FBMkM1QnFCLEVBQUFBLHNCQUFzQixFQUFFLGdCQUFlckIsWUFBZixFQUE2QjtBQUNqRCxRQUFJO0FBQ0EsWUFBTTJCLDRCQUFtQkMsa0NBQW5CLENBQ0Y1QixZQUFZLENBQUN5QixLQURYLEVBRUZ6QixZQUFZLENBQUMwQixLQUZYLENBQU47QUFLQSxZQUFNRyxPQUFPLEdBQUcsSUFBSUMsc0JBQUosQ0FBa0I5QixZQUFZLENBQUN5QixLQUEvQixFQUFzQ3pCLFlBQVksQ0FBQzBCLEtBQW5ELENBQWhCO0FBQ0EsWUFBTVIsc0JBQXNCLEdBQUcsTUFBTVcsT0FBTyxDQUFDRSw4QkFBUixFQUFyQztBQUVBLFdBQUtDLFFBQUwsQ0FBYztBQUNWakIsUUFBQUEsYUFBYSxFQUFFLElBREw7QUFFVkcsUUFBQUE7QUFGVSxPQUFkO0FBSUgsS0FiRCxDQWFFLE9BQU9lLENBQVAsRUFBVTtBQUNSLFdBQUtELFFBQUwsQ0FBY0wsNEJBQW1CTywwQkFBbkIsQ0FBOENELENBQTlDLEVBQWlELGlCQUFqRCxDQUFkO0FBQ0g7QUFDSixHQTVEMkI7QUE4RDVCRSxFQUFBQSxtQkFBbUIsRUFBRSxVQUFTeEIsS0FBVCxFQUFnQkMsUUFBaEIsRUFBMEI7QUFDM0MsU0FBS29CLFFBQUwsQ0FBYztBQUNWdEIsTUFBQUEsS0FBSyxFQUFFZjtBQURHLEtBQWQ7QUFHQSxTQUFLeUIsS0FBTCxHQUFhLElBQUlVLHNCQUFKLENBQWtCLEtBQUtSLEtBQUwsQ0FBV3RCLFlBQVgsQ0FBd0J5QixLQUExQyxFQUFpRCxLQUFLSCxLQUFMLENBQVd0QixZQUFYLENBQXdCMEIsS0FBekUsQ0FBYjtBQUNBLFNBQUtOLEtBQUwsQ0FBV2dCLGFBQVgsQ0FBeUJ6QixLQUF6QixFQUFnQ0MsUUFBaEMsRUFBMEN5QixJQUExQyxDQUErQyxNQUFNO0FBQ2pELFdBQUtMLFFBQUwsQ0FBYztBQUNWdEIsUUFBQUEsS0FBSyxFQUFFZDtBQURHLE9BQWQ7QUFHSCxLQUpELEVBSUkwQyxHQUFELElBQVM7QUFDUixXQUFLQyxlQUFMLENBQXFCLHlCQUFHLHNCQUFILElBQTZCLElBQTdCLEdBQW9DRCxHQUFHLENBQUNFLE9BQTdEO0FBQ0EsV0FBS1IsUUFBTCxDQUFjO0FBQ1Z0QixRQUFBQSxLQUFLLEVBQUVoQjtBQURHLE9BQWQ7QUFHSCxLQVREO0FBVUgsR0E3RTJCO0FBK0U1QitDLEVBQUFBLFFBQVEsRUFBRSxnQkFBZUMsRUFBZixFQUFtQjtBQUN6QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIOztBQUNBLFFBQUksQ0FBQyxLQUFLdkIsS0FBVixFQUFpQjtBQUNid0IsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsNkNBQWQ7QUFDQTtBQUNIOztBQUNELFFBQUk7QUFDQSxZQUFNLEtBQUt6QixLQUFMLENBQVcwQixxQkFBWCxFQUFOO0FBQ0EsV0FBS2QsUUFBTCxDQUFjO0FBQUV0QixRQUFBQSxLQUFLLEVBQUViO0FBQVQsT0FBZDtBQUNILEtBSEQsQ0FHRSxPQUFPeUMsR0FBUCxFQUFZO0FBQ1YsV0FBS0MsZUFBTCxDQUFxQkQsR0FBRyxDQUFDRSxPQUF6QjtBQUNIO0FBQ0osR0EzRjJCO0FBNkY1Qk8sRUFBQUEsWUFBWSxFQUFFLGdCQUFlTCxFQUFmLEVBQW1CO0FBQzdCQSxJQUFBQSxFQUFFLENBQUNDLGNBQUgsR0FENkIsQ0FHN0I7O0FBQ0EsVUFBTSxLQUFLdEIsc0JBQUwsQ0FBNEIsS0FBS0MsS0FBTCxDQUFXdEIsWUFBdkMsQ0FBTjs7QUFFQSxRQUFJLENBQUMsS0FBS2dELEtBQUwsQ0FBV3JDLEtBQWhCLEVBQXVCO0FBQ25CLFdBQUs0QixlQUFMLENBQXFCLHlCQUFHLDJEQUFILENBQXJCO0FBQ0gsS0FGRCxNQUVPLElBQUksQ0FBQyxLQUFLUyxLQUFMLENBQVdwQyxRQUFaLElBQXdCLENBQUMsS0FBS29DLEtBQUwsQ0FBV25DLFNBQXhDLEVBQW1EO0FBQ3RELFdBQUswQixlQUFMLENBQXFCLHlCQUFHLGlDQUFILENBQXJCO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBS1MsS0FBTCxDQUFXcEMsUUFBWCxLQUF3QixLQUFLb0MsS0FBTCxDQUFXbkMsU0FBdkMsRUFBa0Q7QUFDckQsV0FBSzBCLGVBQUwsQ0FBcUIseUJBQUcsc0NBQUgsQ0FBckI7QUFDSCxLQUZNLE1BRUE7QUFDSCxZQUFNVSxjQUFjLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQix5QkFBMUIsRUFBcUQsRUFBckQsRUFBeURKLGNBQXpELEVBQXlFO0FBQ3JFSyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsVUFBSCxDQUQ4RDtBQUVyRUMsUUFBQUEsV0FBVyxFQUNQLDBDQUNNLHlCQUNFLHNFQUNBLDRFQURBLEdBRUEsaUZBRkEsR0FHQSxXQUpGLENBRE4sQ0FIaUU7QUFXckVDLFFBQUFBLE1BQU0sRUFBRSx5QkFBRyxVQUFILENBWDZEO0FBWXJFQyxRQUFBQSxVQUFVLEVBQUdDLFNBQUQsSUFBZTtBQUN2QixjQUFJQSxTQUFKLEVBQWU7QUFDWCxpQkFBS3ZCLG1CQUFMLENBQXlCLEtBQUthLEtBQUwsQ0FBV3JDLEtBQXBDLEVBQTJDLEtBQUtxQyxLQUFMLENBQVdwQyxRQUF0RDtBQUNIO0FBQ0o7QUFoQm9FLE9BQXpFO0FBa0JIO0FBQ0osR0E5SDJCO0FBZ0k1QitDLEVBQUFBLGNBQWMsRUFBRSxVQUFTQyxRQUFULEVBQW1CbEIsRUFBbkIsRUFBdUI7QUFDbkMsU0FBS1YsUUFBTCxDQUFjO0FBQ1YsT0FBQzRCLFFBQUQsR0FBWWxCLEVBQUUsQ0FBQ21CLE1BQUgsQ0FBVUM7QUFEWixLQUFkO0FBR0gsR0FwSTJCOztBQXNJNUIsUUFBTUMsNkJBQU4sR0FBc0M7QUFDbEMsU0FBSy9CLFFBQUwsQ0FBYztBQUNWdEIsTUFBQUEsS0FBSyxFQUFFaEI7QUFERyxLQUFkO0FBR0gsR0ExSTJCOztBQTRJNUJzRSxFQUFBQSx3QkFBd0IsQ0FBQ3RCLEVBQUQsRUFBSztBQUN6QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0FELElBQUFBLEVBQUUsQ0FBQ3VCLGVBQUg7QUFDQSxTQUFLakMsUUFBTCxDQUFjO0FBQ1Z0QixNQUFBQSxLQUFLLEVBQUVqQjtBQURHLEtBQWQ7QUFHSCxHQWxKMkI7O0FBb0o1QmMsRUFBQUEsWUFBWSxFQUFFLFVBQVNtQyxFQUFULEVBQWE7QUFDdkJBLElBQUFBLEVBQUUsQ0FBQ0MsY0FBSDtBQUNBRCxJQUFBQSxFQUFFLENBQUN1QixlQUFIO0FBQ0EsU0FBSzNDLEtBQUwsQ0FBV2YsWUFBWDtBQUNILEdBeEoyQjtBQTBKNUJnQyxFQUFBQSxlQUFlLEVBQUUsVUFBUzJCLElBQVQsRUFBZVosS0FBZixFQUFzQjtBQUNuQyxVQUFNYSxXQUFXLEdBQUdqQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVEYyxXQUF2RCxFQUFvRTtBQUNoRWIsTUFBQUEsS0FBSyxFQUFFQSxLQUR5RDtBQUVoRUMsTUFBQUEsV0FBVyxFQUFFVztBQUZtRCxLQUFwRTtBQUlILEdBaEsyQjs7QUFrSzVCRSxFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixVQUFNQyxZQUFZLEdBQUduQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsbUJBQWpCLENBQXJCOztBQUVBLFFBQUltQixtQkFBVUMsR0FBVixHQUFnQixxQkFBaEIsQ0FBSixFQUE0QztBQUN4QyxhQUFPLElBQVA7QUFDSDs7QUFFRCxXQUFPLDZCQUFDLFlBQUQ7QUFDSCxNQUFBLFlBQVksRUFBRSxLQUFLakQsS0FBTCxDQUFXdEIsWUFEdEI7QUFFSCxNQUFBLG9CQUFvQixFQUFFLEtBQUtzQixLQUFMLENBQVdqQixvQkFGOUI7QUFHSCxNQUFBLFdBQVcsRUFBRSxDQUhWO0FBSUgsTUFBQSx3Q0FBd0MsRUFBRSxJQUp2QztBQUtILE1BQUEsYUFBYSxFQUFFLEtBQUswRCw2QkFMakI7QUFNSCxNQUFBLFVBQVUsRUFBRSx5QkFBRyxNQUFILENBTlQ7QUFPSCxNQUFBLFdBQVcsRUFBQztBQVBULE1BQVA7QUFTSCxHQWxMMkI7O0FBb0w1QlMsRUFBQUEsWUFBWSxHQUFHO0FBQ1gsVUFBTUMsS0FBSyxHQUFHdkIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGdCQUFqQixDQUFkO0FBRUEsUUFBSXJDLFNBQVMsR0FBRyxJQUFoQjtBQUNBLFVBQU13QixHQUFHLEdBQUcsS0FBS1UsS0FBTCxDQUFXbEMsU0FBdkI7O0FBQ0EsUUFBSXdCLEdBQUosRUFBUztBQUNMeEIsTUFBQUEsU0FBUyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUFrQ3dCLEdBQWxDLENBQVo7QUFDSDs7QUFFRCxRQUFJb0MsaUJBQUo7O0FBQ0EsUUFBSSxDQUFDLEtBQUsxQixLQUFMLENBQVdqQyxhQUFoQixFQUErQjtBQUMzQixZQUFNNEQsT0FBTyxHQUFHLHlCQUFXO0FBQ3ZCLDBCQUFrQixJQURLO0FBRXZCLGdDQUF3QixJQUZEO0FBR3ZCLHdDQUFnQyxDQUFDLEtBQUszQixLQUFMLENBQVdoQztBQUhyQixPQUFYLENBQWhCO0FBS0EwRCxNQUFBQSxpQkFBaUIsR0FDYjtBQUFLLFFBQUEsU0FBUyxFQUFFQztBQUFoQixTQUNLLEtBQUszQixLQUFMLENBQVcvQixlQURoQixDQURKO0FBS0g7O0FBRUQsUUFBSTJELHFCQUFxQixHQUFHLHlCQUFHLHVDQUFILEVBQTRDO0FBQ3BFQyxNQUFBQSxVQUFVLEVBQUUsS0FBS3ZELEtBQUwsQ0FBV3RCLFlBQVgsQ0FBd0I4RTtBQURnQyxLQUE1QyxDQUE1Qjs7QUFHQSxRQUFJLEtBQUt4RCxLQUFMLENBQVd0QixZQUFYLENBQXdCK0UsaUJBQTVCLEVBQStDO0FBQzNDLFlBQU1DLGVBQWUsR0FBRzlCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBeEI7QUFFQXlCLE1BQUFBLHFCQUFxQixHQUFHLHlCQUFHLGlEQUFILEVBQXNELEVBQXRELEVBQTBEO0FBQzlFLGdDQUF3QixNQUFNO0FBQzFCLGlCQUFPLDZCQUFDLGVBQUQ7QUFDSCxZQUFBLEtBQUssRUFBQywrQkFESDtBQUVILFlBQUEsT0FBTyxFQUFFLEtBQUt0RCxLQUFMLENBQVd0QixZQUFYLENBQXdCeUI7QUFGOUIsYUFJRixLQUFLSCxLQUFMLENBQVd0QixZQUFYLENBQXdCOEUsTUFKdEIsQ0FBUDtBQU1IO0FBUjZFLE9BQTFELENBQXhCO0FBVUgsS0F2Q1UsQ0F5Q1g7OztBQUNBLFFBQUlHLFFBQVEsR0FBRyxJQUFmOztBQUNBLFFBQUksQ0FBQ1gsbUJBQVVDLEdBQVYsR0FBZ0IscUJBQWhCLENBQUwsRUFBNkM7QUFDekNVLE1BQUFBLFFBQVEsR0FBRztBQUFHLFFBQUEsU0FBUyxFQUFDLCtCQUFiO0FBQ1AsUUFBQSxJQUFJLEVBQUMsR0FERTtBQUNFLFFBQUEsT0FBTyxFQUFFLEtBQUtqQjtBQURoQixTQUdOLHlCQUFHLFFBQUgsQ0FITSxDQUFYO0FBS0g7O0FBRUQsUUFBSSxDQUFDLEtBQUsxQyxLQUFMLENBQVd0QixZQUFYLENBQXdCMEIsS0FBekIsSUFBa0MsS0FBS3NCLEtBQUwsQ0FBVzlCLHNCQUFqRCxFQUF5RTtBQUNyRSxhQUFPLDBDQUNILHlDQUNLMEQscUJBREwsRUFFS0ssUUFGTCxDQURHLEVBS0YseUJBQ0csdUNBQ0Esb0RBRkgsQ0FMRSxFQVNIO0FBQUcsUUFBQSxTQUFTLEVBQUMsd0JBQWI7QUFBc0MsUUFBQSxPQUFPLEVBQUUsS0FBSzFFLFlBQXBEO0FBQWtFLFFBQUEsSUFBSSxFQUFDO0FBQXZFLFNBQ0sseUJBQUcsaUJBQUgsQ0FETCxDQVRHLENBQVA7QUFhSDs7QUFFRCxXQUFPLDBDQUNGTyxTQURFLEVBRUY0RCxpQkFGRSxFQUdILHlDQUNLRSxxQkFETCxFQUVLSyxRQUZMLENBSEcsRUFPSDtBQUFNLE1BQUEsUUFBUSxFQUFFLEtBQUtsQztBQUFyQixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLEtBQUQ7QUFDSSxNQUFBLElBQUksRUFBQyxhQURULENBQ3VCO0FBRHZCO0FBRUksTUFBQSxJQUFJLEVBQUMsTUFGVDtBQUdJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FIWDtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUtDLEtBQUwsQ0FBV3JDLEtBSnRCO0FBS0ksTUFBQSxRQUFRLEVBQUUsS0FBS2dELGNBQUwsQ0FBb0J1QixJQUFwQixDQUF5QixJQUF6QixFQUErQixPQUEvQixDQUxkO0FBTUksTUFBQSxTQUFTO0FBTmIsTUFESixDQURKLEVBV0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsS0FBRDtBQUNJLE1BQUEsSUFBSSxFQUFDLGdCQURUO0FBRUksTUFBQSxJQUFJLEVBQUMsVUFGVDtBQUdJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLFVBQUgsQ0FIWDtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUtsQyxLQUFMLENBQVdwQyxRQUp0QjtBQUtJLE1BQUEsUUFBUSxFQUFFLEtBQUsrQyxjQUFMLENBQW9CdUIsSUFBcEIsQ0FBeUIsSUFBekIsRUFBK0IsVUFBL0I7QUFMZCxNQURKLEVBUUksNkJBQUMsS0FBRDtBQUNJLE1BQUEsSUFBSSxFQUFDLHdCQURUO0FBRUksTUFBQSxJQUFJLEVBQUMsVUFGVDtBQUdJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLFNBQUgsQ0FIWDtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUtsQyxLQUFMLENBQVduQyxTQUp0QjtBQUtJLE1BQUEsUUFBUSxFQUFFLEtBQUs4QyxjQUFMLENBQW9CdUIsSUFBcEIsQ0FBeUIsSUFBekIsRUFBK0IsV0FBL0I7QUFMZCxNQVJKLENBWEosRUEyQkksMkNBQU8seUJBQ0gsZ0VBQ0EsNEJBRkcsQ0FBUCxDQTNCSixFQStCSTtBQUNJLE1BQUEsU0FBUyxFQUFDLGlCQURkO0FBRUksTUFBQSxJQUFJLEVBQUMsUUFGVDtBQUdJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFIO0FBSFgsTUEvQkosQ0FQRyxFQTRDSDtBQUFHLE1BQUEsU0FBUyxFQUFDLHdCQUFiO0FBQXNDLE1BQUEsT0FBTyxFQUFFLEtBQUszRSxZQUFwRDtBQUFrRSxNQUFBLElBQUksRUFBQztBQUF2RSxPQUNLLHlCQUFHLGlCQUFILENBREwsQ0E1Q0csQ0FBUDtBQWdESCxHQXZTMkI7O0FBeVM1QjRFLEVBQUFBLGtCQUFrQixHQUFHO0FBQ2pCLFVBQU1DLE9BQU8sR0FBR2xDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxXQUFPLDZCQUFDLE9BQUQsT0FBUDtBQUNILEdBNVMyQjs7QUE4UzVCa0MsRUFBQUEsZUFBZSxHQUFHO0FBQ2QsV0FBTywwQ0FDRix5QkFBRywwRUFDQSxnQ0FESCxFQUNxQztBQUFFQyxNQUFBQSxZQUFZLEVBQUUsS0FBS3RDLEtBQUwsQ0FBV3JDO0FBQTNCLEtBRHJDLENBREUsRUFHSCx3Q0FIRyxFQUlIO0FBQU8sTUFBQSxTQUFTLEVBQUMsaUJBQWpCO0FBQW1DLE1BQUEsSUFBSSxFQUFDLFFBQXhDO0FBQWlELE1BQUEsT0FBTyxFQUFFLEtBQUs4QixRQUEvRDtBQUNJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGtDQUFIO0FBRFgsTUFKRyxDQUFQO0FBT0gsR0F0VDJCOztBQXdUNUI4QyxFQUFBQSxVQUFVLEdBQUc7QUFDVCxXQUFPLDBDQUNILHdDQUFJLHlCQUFHLCtCQUFILENBQUosQ0FERyxFQUVILHdDQUFJLHlCQUNBLHlFQUNBLHdFQURBLEdBRUEsU0FIQSxDQUFKLENBRkcsRUFPSDtBQUFPLE1BQUEsU0FBUyxFQUFDLGlCQUFqQjtBQUFtQyxNQUFBLElBQUksRUFBQyxRQUF4QztBQUFpRCxNQUFBLE9BQU8sRUFBRSxLQUFLakUsS0FBTCxDQUFXZCxVQUFyRTtBQUNJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLHdCQUFIO0FBRFgsTUFQRyxDQUFQO0FBVUgsR0FuVTJCOztBQXFVNUJnRixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFVBQVUsR0FBR3ZDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQkFBakIsQ0FBbkI7QUFDQSxVQUFNdUMsUUFBUSxHQUFHeEMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGVBQWpCLENBQWpCO0FBRUEsUUFBSXdDLGdCQUFKOztBQUNBLFlBQVEsS0FBSzNDLEtBQUwsQ0FBV3RDLEtBQW5CO0FBQ0ksV0FBS2pCLG9CQUFMO0FBQ0lrRyxRQUFBQSxnQkFBZ0IsR0FBRyxLQUFLdkIsbUJBQUwsRUFBbkI7QUFDQTs7QUFDSixXQUFLMUUsWUFBTDtBQUNJaUcsUUFBQUEsZ0JBQWdCLEdBQUcsS0FBS25CLFlBQUwsRUFBbkI7QUFDQTs7QUFDSixXQUFLN0UsbUJBQUw7QUFDSWdHLFFBQUFBLGdCQUFnQixHQUFHLEtBQUtSLGtCQUFMLEVBQW5CO0FBQ0E7O0FBQ0osV0FBS3ZGLGdCQUFMO0FBQ0krRixRQUFBQSxnQkFBZ0IsR0FBRyxLQUFLTixlQUFMLEVBQW5CO0FBQ0E7O0FBQ0osV0FBS3hGLFVBQUw7QUFDSThGLFFBQUFBLGdCQUFnQixHQUFHLEtBQUtKLFVBQUwsRUFBbkI7QUFDQTtBQWZSOztBQWtCQSxXQUNJLDZCQUFDLGlCQUFELFFBQ0ksNkJBQUMsVUFBRCxPQURKLEVBRUksNkJBQUMsUUFBRCxRQUNJLDhDQUFPLHlCQUFHLG9CQUFILENBQVAsTUFESixFQUVLSSxnQkFGTCxDQUZKLENBREo7QUFTSDtBQXJXMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSBcIi4uLy4uLy4uL1Nka0NvbmZpZ1wiO1xyXG5pbXBvcnQgUGFzc3dvcmRSZXNldCBmcm9tIFwiLi4vLi4vLi4vUGFzc3dvcmRSZXNldFwiO1xyXG5pbXBvcnQgQXV0b0Rpc2NvdmVyeVV0aWxzLCB7VmFsaWRhdGVkU2VydmVyQ29uZmlnfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvQXV0b0Rpc2NvdmVyeVV0aWxzXCI7XHJcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xyXG5pbXBvcnQgQXV0aFBhZ2UgZnJvbSBcIi4uLy4uL3ZpZXdzL2F1dGgvQXV0aFBhZ2VcIjtcclxuXHJcbi8vIFBoYXNlc1xyXG4vLyBTaG93IGNvbnRyb2xzIHRvIGNvbmZpZ3VyZSBzZXJ2ZXIgZGV0YWlsc1xyXG5jb25zdCBQSEFTRV9TRVJWRVJfREVUQUlMUyA9IDA7XHJcbi8vIFNob3cgdGhlIGZvcmdvdCBwYXNzd29yZCBpbnB1dHNcclxuY29uc3QgUEhBU0VfRk9SR09UID0gMTtcclxuLy8gRW1haWwgaXMgaW4gdGhlIHByb2Nlc3Mgb2YgYmVpbmcgc2VudFxyXG5jb25zdCBQSEFTRV9TRU5ESU5HX0VNQUlMID0gMjtcclxuLy8gRW1haWwgaGFzIGJlZW4gc2VudFxyXG5jb25zdCBQSEFTRV9FTUFJTF9TRU5UID0gMztcclxuLy8gVXNlciBoYXMgY2xpY2tlZCB0aGUgbGluayBpbiBlbWFpbCBhbmQgY29tcGxldGVkIHJlc2V0XHJcbmNvbnN0IFBIQVNFX0RPTkUgPSA0O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0ZvcmdvdFBhc3N3b3JkJyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBzZXJ2ZXJDb25maWc6IFByb3BUeXBlcy5pbnN0YW5jZU9mKFZhbGlkYXRlZFNlcnZlckNvbmZpZykuaXNSZXF1aXJlZCxcclxuICAgICAgICBvblNlcnZlckNvbmZpZ0NoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICBvbkxvZ2luQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uQ29tcGxldGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0ZPUkdPVCxcclxuICAgICAgICAgICAgZW1haWw6IFwiXCIsXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiBcIlwiLFxyXG4gICAgICAgICAgICBwYXNzd29yZDI6IFwiXCIsXHJcbiAgICAgICAgICAgIGVycm9yVGV4dDogbnVsbCxcclxuXHJcbiAgICAgICAgICAgIC8vIFdlIHBlcmZvcm0gbGl2ZWxpbmVzcyBjaGVja3MgbGF0ZXIsIGJ1dCBmb3Igbm93IHN1cHByZXNzIHRoZSBlcnJvcnMuXHJcbiAgICAgICAgICAgIC8vIFdlIGFsc28gdHJhY2sgdGhlIHNlcnZlciBkZWFkIGVycm9ycyBpbmRlcGVuZGVudGx5IG9mIHRoZSByZWd1bGFyIGVycm9ycyBzb1xyXG4gICAgICAgICAgICAvLyB0aGF0IHdlIGNhbiByZW5kZXIgaXQgZGlmZmVyZW50bHksIGFuZCBvdmVycmlkZSBhbnkgb3RoZXIgZXJyb3IgdGhlIHVzZXIgbWF5XHJcbiAgICAgICAgICAgIC8vIGJlIHNlZWluZy5cclxuICAgICAgICAgICAgc2VydmVySXNBbGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgc2VydmVyRXJyb3JJc0ZhdGFsOiBmYWxzZSxcclxuICAgICAgICAgICAgc2VydmVyRGVhZEVycm9yOiBcIlwiLFxyXG4gICAgICAgICAgICBzZXJ2ZXJSZXF1aXJlc0lkU2VydmVyOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnJlc2V0ID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9jaGVja1NlcnZlckxpdmVsaW5lc3ModGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSB3aXRoIGFwcHJvcHJpYXRlIGxpZmVjeWNsZSBldmVudFxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uKG5ld1Byb3BzKSB7XHJcbiAgICAgICAgaWYgKG5ld1Byb3BzLnNlcnZlckNvbmZpZy5oc1VybCA9PT0gdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNVcmwgJiZcclxuICAgICAgICAgICAgbmV3UHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsID09PSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5pc1VybCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBEbyBhIGxpdmVsaW5lc3MgY2hlY2sgb24gdGhlIG5ldyBVUkxzXHJcbiAgICAgICAgdGhpcy5fY2hlY2tTZXJ2ZXJMaXZlbGluZXNzKG5ld1Byb3BzLnNlcnZlckNvbmZpZyk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jaGVja1NlcnZlckxpdmVsaW5lc3M6IGFzeW5jIGZ1bmN0aW9uKHNlcnZlckNvbmZpZykge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IEF1dG9EaXNjb3ZlcnlVdGlscy52YWxpZGF0ZVNlcnZlckNvbmZpZ1dpdGhTdGF0aWNVcmxzKFxyXG4gICAgICAgICAgICAgICAgc2VydmVyQ29uZmlnLmhzVXJsLFxyXG4gICAgICAgICAgICAgICAgc2VydmVyQ29uZmlnLmlzVXJsLFxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcHdSZXNldCA9IG5ldyBQYXNzd29yZFJlc2V0KHNlcnZlckNvbmZpZy5oc1VybCwgc2VydmVyQ29uZmlnLmlzVXJsKTtcclxuICAgICAgICAgICAgY29uc3Qgc2VydmVyUmVxdWlyZXNJZFNlcnZlciA9IGF3YWl0IHB3UmVzZXQuZG9lc1NlcnZlclJlcXVpcmVJZFNlcnZlclBhcmFtKCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlcnZlcklzQWxpdmU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzZXJ2ZXJSZXF1aXJlc0lkU2VydmVyLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoQXV0b0Rpc2NvdmVyeVV0aWxzLmF1dGhDb21wb25lbnRTdGF0ZUZvckVycm9yKGUsIFwiZm9yZ290X3Bhc3N3b3JkXCIpKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHN1Ym1pdFBhc3N3b3JkUmVzZXQ6IGZ1bmN0aW9uKGVtYWlsLCBwYXNzd29yZCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfU0VORElOR19FTUFJTCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnJlc2V0ID0gbmV3IFBhc3N3b3JkUmVzZXQodGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNVcmwsIHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsKTtcclxuICAgICAgICB0aGlzLnJlc2V0LnJlc2V0UGFzc3dvcmQoZW1haWwsIHBhc3N3b3JkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBwaGFzZTogUEhBU0VfRU1BSUxfU0VOVCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dFcnJvckRpYWxvZyhfdCgnRmFpbGVkIHRvIHNlbmQgZW1haWwnKSArIFwiOiBcIiArIGVyci5tZXNzYWdlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBwaGFzZTogUEhBU0VfRk9SR09ULFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25WZXJpZnk6IGFzeW5jIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBpZiAoIXRoaXMucmVzZXQpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIm9uVmVyaWZ5IGNhbGxlZCBiZWZvcmUgc3VibWl0UGFzc3dvcmRSZXNldCFcIik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5yZXNldC5jaGVja0VtYWlsTGlua0NsaWNrZWQoKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHBoYXNlOiBQSEFTRV9ET05FIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dFcnJvckRpYWxvZyhlcnIubWVzc2FnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblN1Ym1pdEZvcm06IGFzeW5jIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgLy8gcmVmcmVzaCB0aGUgc2VydmVyIGVycm9ycywganVzdCBpbiBjYXNlIHRoZSBzZXJ2ZXIgY2FtZSBiYWNrIG9ubGluZVxyXG4gICAgICAgIGF3YWl0IHRoaXMuX2NoZWNrU2VydmVyTGl2ZWxpbmVzcyh0aGlzLnByb3BzLnNlcnZlckNvbmZpZyk7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5lbWFpbCkge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dFcnJvckRpYWxvZyhfdCgnVGhlIGVtYWlsIGFkZHJlc3MgbGlua2VkIHRvIHlvdXIgYWNjb3VudCBtdXN0IGJlIGVudGVyZWQuJykpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuc3RhdGUucGFzc3dvcmQgfHwgIXRoaXMuc3RhdGUucGFzc3dvcmQyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd0Vycm9yRGlhbG9nKF90KCdBIG5ldyBwYXNzd29yZCBtdXN0IGJlIGVudGVyZWQuJykpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5wYXNzd29yZCAhPT0gdGhpcy5zdGF0ZS5wYXNzd29yZDIpIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93RXJyb3JEaWFsb2coX3QoJ05ldyBwYXNzd29yZHMgbXVzdCBtYXRjaCBlYWNoIG90aGVyLicpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGb3Jnb3QgUGFzc3dvcmQgV2FybmluZycsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdXYXJuaW5nIScpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ2hhbmdpbmcgeW91ciBwYXNzd29yZCB3aWxsIHJlc2V0IGFueSBlbmQtdG8tZW5kIGVuY3J5cHRpb24ga2V5cyBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm9uIGFsbCBvZiB5b3VyIHNlc3Npb25zLCBtYWtpbmcgZW5jcnlwdGVkIGNoYXQgaGlzdG9yeSB1bnJlYWRhYmxlLiBTZXQgdXAgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJLZXkgQmFja3VwIG9yIGV4cG9ydCB5b3VyIHJvb20ga2V5cyBmcm9tIGFub3RoZXIgc2Vzc2lvbiBiZWZvcmUgcmVzZXR0aW5nIHlvdXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXNzd29yZC5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICAgICAgYnV0dG9uOiBfdCgnQ29udGludWUnKSxcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IChjb25maXJtZWQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3VibWl0UGFzc3dvcmRSZXNldCh0aGlzLnN0YXRlLmVtYWlsLCB0aGlzLnN0YXRlLnBhc3N3b3JkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uSW5wdXRDaGFuZ2VkOiBmdW5jdGlvbihzdGF0ZUtleSwgZXYpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgW3N0YXRlS2V5XTogZXYudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBhc3luYyBvblNlcnZlckRldGFpbHNOZXh0UGhhc2VDbGljaygpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0ZPUkdPVCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NFUlZFUl9ERVRBSUxTLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkxvZ2luQ2xpY2s6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLnByb3BzLm9uTG9naW5DbGljaygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBzaG93RXJyb3JEaWFsb2c6IGZ1bmN0aW9uKGJvZHksIHRpdGxlKSB7XHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGb3Jnb3QgUGFzc3dvcmQgRXJyb3InLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IHRpdGxlLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogYm9keSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyU2VydmVyRGV0YWlscygpIHtcclxuICAgICAgICBjb25zdCBTZXJ2ZXJDb25maWcgPSBzZGsuZ2V0Q29tcG9uZW50KFwiYXV0aC5TZXJ2ZXJDb25maWdcIik7XHJcblxyXG4gICAgICAgIGlmIChTZGtDb25maWcuZ2V0KClbJ2Rpc2FibGVfY3VzdG9tX3VybHMnXSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8U2VydmVyQ29uZmlnXHJcbiAgICAgICAgICAgIHNlcnZlckNvbmZpZz17dGhpcy5wcm9wcy5zZXJ2ZXJDb25maWd9XHJcbiAgICAgICAgICAgIG9uU2VydmVyQ29uZmlnQ2hhbmdlPXt0aGlzLnByb3BzLm9uU2VydmVyQ29uZmlnQ2hhbmdlfVxyXG4gICAgICAgICAgICBkZWxheVRpbWVNcz17MH1cclxuICAgICAgICAgICAgc2hvd0lkZW50aXR5U2VydmVySWZSZXF1aXJlZEJ5SG9tZXNlcnZlcj17dHJ1ZX1cclxuICAgICAgICAgICAgb25BZnRlclN1Ym1pdD17dGhpcy5vblNlcnZlckRldGFpbHNOZXh0UGhhc2VDbGlja31cclxuICAgICAgICAgICAgc3VibWl0VGV4dD17X3QoXCJOZXh0XCIpfVxyXG4gICAgICAgICAgICBzdWJtaXRDbGFzcz1cIm14X0xvZ2luX3N1Ym1pdFwiXHJcbiAgICAgICAgLz47XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlckZvcmdvdCgpIHtcclxuICAgICAgICBjb25zdCBGaWVsZCA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkZpZWxkJyk7XHJcblxyXG4gICAgICAgIGxldCBlcnJvclRleHQgPSBudWxsO1xyXG4gICAgICAgIGNvbnN0IGVyciA9IHRoaXMuc3RhdGUuZXJyb3JUZXh0O1xyXG4gICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgZXJyb3JUZXh0ID0gPGRpdiBjbGFzc05hbWU9XCJteF9Mb2dpbl9lcnJvclwiPnsgZXJyIH08L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2VydmVyRGVhZFNlY3Rpb247XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNlcnZlcklzQWxpdmUpIHtcclxuICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9lcnJvclwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9zZXJ2ZXJFcnJvclwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9zZXJ2ZXJFcnJvck5vbkZhdGFsXCI6ICF0aGlzLnN0YXRlLnNlcnZlckVycm9ySXNGYXRhbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHNlcnZlckRlYWRTZWN0aW9uID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLnNlcnZlckRlYWRFcnJvcn1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHlvdXJNYXRyaXhBY2NvdW50VGV4dCA9IF90KCdZb3VyIE1hdHJpeCBhY2NvdW50IG9uICUoc2VydmVyTmFtZSlzJywge1xyXG4gICAgICAgICAgICBzZXJ2ZXJOYW1lOiB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc05hbWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmhzTmFtZUlzRGlmZmVyZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFRleHRXaXRoVG9vbHRpcCA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5UZXh0V2l0aFRvb2x0aXBcIik7XHJcblxyXG4gICAgICAgICAgICB5b3VyTWF0cml4QWNjb3VudFRleHQgPSBfdCgnWW91ciBNYXRyaXggYWNjb3VudCBvbiA8dW5kZXJsaW5lZFNlcnZlck5hbWUgLz4nLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgJ3VuZGVybGluZWRTZXJ2ZXJOYW1lJzogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiA8VGV4dFdpdGhUb29sdGlwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwibXhfTG9naW5fdW5kZXJsaW5lZFNlcnZlck5hbWVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwPXt0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc1VybH1cclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc05hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9UZXh0V2l0aFRvb2x0aXA+O1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZiBjdXN0b20gVVJMcyBhcmUgYWxsb3dlZCwgd2lyZSB1cCB0aGUgc2VydmVyIGRldGFpbHMgZWRpdCBsaW5rLlxyXG4gICAgICAgIGxldCBlZGl0TGluayA9IG51bGw7XHJcbiAgICAgICAgaWYgKCFTZGtDb25maWcuZ2V0KClbJ2Rpc2FibGVfY3VzdG9tX3VybHMnXSkge1xyXG4gICAgICAgICAgICBlZGl0TGluayA9IDxhIGNsYXNzTmFtZT1cIm14X0F1dGhCb2R5X2VkaXRTZXJ2ZXJEZXRhaWxzXCJcclxuICAgICAgICAgICAgICAgIGhyZWY9XCIjXCIgb25DbGljaz17dGhpcy5vbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2t9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtfdCgnQ2hhbmdlJyl9XHJcbiAgICAgICAgICAgIDwvYT47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsICYmIHRoaXMuc3RhdGUuc2VydmVyUmVxdWlyZXNJZFNlcnZlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMz5cclxuICAgICAgICAgICAgICAgICAgICB7eW91ck1hdHJpeEFjY291bnRUZXh0fVxyXG4gICAgICAgICAgICAgICAgICAgIHtlZGl0TGlua31cclxuICAgICAgICAgICAgICAgIDwvaDM+XHJcbiAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJObyBpZGVudGl0eSBzZXJ2ZXIgaXMgY29uZmlndXJlZDogXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiYWRkIG9uZSBpbiBzZXJ2ZXIgc2V0dGluZ3MgdG8gcmVzZXQgeW91ciBwYXNzd29yZC5cIixcclxuICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9jaGFuZ2VGbG93XCIgb25DbGljaz17dGhpcy5vbkxvZ2luQ2xpY2t9IGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge190KCdTaWduIGluIGluc3RlYWQnKX1cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIHtlcnJvclRleHR9XHJcbiAgICAgICAgICAgIHtzZXJ2ZXJEZWFkU2VjdGlvbn1cclxuICAgICAgICAgICAgPGgzPlxyXG4gICAgICAgICAgICAgICAge3lvdXJNYXRyaXhBY2NvdW50VGV4dH1cclxuICAgICAgICAgICAgICAgIHtlZGl0TGlua31cclxuICAgICAgICAgICAgPC9oMz5cclxuICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9e3RoaXMub25TdWJtaXRGb3JtfT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQXV0aEJvZHlfZmllbGRSb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cInJlc2V0X2VtYWlsXCIgLy8gZGVmaW5lIGEgbmFtZSBzbyBicm93c2VyJ3MgcGFzc3dvcmQgYXV0b2ZpbGwgZ2V0cyBsZXNzIGNvbmZ1c2VkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdFbWFpbCcpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5lbWFpbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25JbnB1dENoYW5nZWQuYmluZCh0aGlzLCBcImVtYWlsXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvRm9jdXNcclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0F1dGhCb2R5X2ZpZWxkUm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJyZXNldF9wYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnUGFzc3dvcmQnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uSW5wdXRDaGFuZ2VkLmJpbmQodGhpcywgXCJwYXNzd29yZFwiKX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwicmVzZXRfcGFzc3dvcmRfY29uZmlybVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnQ29uZmlybScpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5wYXNzd29yZDJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uSW5wdXRDaGFuZ2VkLmJpbmQodGhpcywgXCJwYXNzd29yZDJcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPHNwYW4+e190KFxyXG4gICAgICAgICAgICAgICAgICAgICdBIHZlcmlmaWNhdGlvbiBlbWFpbCB3aWxsIGJlIHNlbnQgdG8geW91ciBpbmJveCB0byBjb25maXJtICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICdzZXR0aW5nIHlvdXIgbmV3IHBhc3N3b3JkLicsXHJcbiAgICAgICAgICAgICAgICApfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0xvZ2luX3N1Ym1pdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e190KCdTZW5kIFJlc2V0IEVtYWlsJyl9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm14X0F1dGhCb2R5X2NoYW5nZUZsb3dcIiBvbkNsaWNrPXt0aGlzLm9uTG9naW5DbGlja30gaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICAgIHtfdCgnU2lnbiBpbiBpbnN0ZWFkJyl9XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlclNlbmRpbmdFbWFpbCgpIHtcclxuICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgcmV0dXJuIDxTcGlubmVyIC8+O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJFbWFpbFNlbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIHtfdChcIkFuIGVtYWlsIGhhcyBiZWVuIHNlbnQgdG8gJShlbWFpbEFkZHJlc3Mpcy4gT25jZSB5b3UndmUgZm9sbG93ZWQgdGhlIFwiICtcclxuICAgICAgICAgICAgICAgIFwibGluayBpdCBjb250YWlucywgY2xpY2sgYmVsb3cuXCIsIHsgZW1haWxBZGRyZXNzOiB0aGlzLnN0YXRlLmVtYWlsIH0pfVxyXG4gICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cIm14X0xvZ2luX3N1Ym1pdFwiIHR5cGU9XCJidXR0b25cIiBvbkNsaWNrPXt0aGlzLm9uVmVyaWZ5fVxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e190KCdJIGhhdmUgdmVyaWZpZWQgbXkgZW1haWwgYWRkcmVzcycpfSAvPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyRG9uZSgpIHtcclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPHA+e190KFwiWW91ciBwYXNzd29yZCBoYXMgYmVlbiByZXNldC5cIil9PC9wPlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdSBoYXZlIGJlZW4gbG9nZ2VkIG91dCBvZiBhbGwgc2Vzc2lvbnMgYW5kIHdpbGwgbm8gbG9uZ2VyIHJlY2VpdmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJwdXNoIG5vdGlmaWNhdGlvbnMuIFRvIHJlLWVuYWJsZSBub3RpZmljYXRpb25zLCBzaWduIGluIGFnYWluIG9uIGVhY2ggXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJkZXZpY2UuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8aW5wdXQgY2xhc3NOYW1lPVwibXhfTG9naW5fc3VibWl0XCIgdHlwZT1cImJ1dHRvblwiIG9uQ2xpY2s9e3RoaXMucHJvcHMub25Db21wbGV0ZX1cclxuICAgICAgICAgICAgICAgIHZhbHVlPXtfdCgnUmV0dXJuIHRvIGxvZ2luIHNjcmVlbicpfSAvPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBBdXRoSGVhZGVyID0gc2RrLmdldENvbXBvbmVudChcImF1dGguQXV0aEhlYWRlclwiKTtcclxuICAgICAgICBjb25zdCBBdXRoQm9keSA9IHNkay5nZXRDb21wb25lbnQoXCJhdXRoLkF1dGhCb2R5XCIpO1xyXG5cclxuICAgICAgICBsZXQgcmVzZXRQYXNzd29yZEpzeDtcclxuICAgICAgICBzd2l0Y2ggKHRoaXMuc3RhdGUucGhhc2UpIHtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9TRVJWRVJfREVUQUlMUzpcclxuICAgICAgICAgICAgICAgIHJlc2V0UGFzc3dvcmRKc3ggPSB0aGlzLnJlbmRlclNlcnZlckRldGFpbHMoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX0ZPUkdPVDpcclxuICAgICAgICAgICAgICAgIHJlc2V0UGFzc3dvcmRKc3ggPSB0aGlzLnJlbmRlckZvcmdvdCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfU0VORElOR19FTUFJTDpcclxuICAgICAgICAgICAgICAgIHJlc2V0UGFzc3dvcmRKc3ggPSB0aGlzLnJlbmRlclNlbmRpbmdFbWFpbCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfRU1BSUxfU0VOVDpcclxuICAgICAgICAgICAgICAgIHJlc2V0UGFzc3dvcmRKc3ggPSB0aGlzLnJlbmRlckVtYWlsU2VudCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfRE9ORTpcclxuICAgICAgICAgICAgICAgIHJlc2V0UGFzc3dvcmRKc3ggPSB0aGlzLnJlbmRlckRvbmUoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEF1dGhQYWdlPlxyXG4gICAgICAgICAgICAgICAgPEF1dGhIZWFkZXIgLz5cclxuICAgICAgICAgICAgICAgIDxBdXRoQm9keT5cclxuICAgICAgICAgICAgICAgICAgICA8aDI+IHsgX3QoJ1NldCBhIG5ldyBwYXNzd29yZCcpIH0gPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICB7cmVzZXRQYXNzd29yZEpzeH1cclxuICAgICAgICAgICAgICAgIDwvQXV0aEJvZHk+XHJcbiAgICAgICAgICAgIDwvQXV0aFBhZ2U+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=