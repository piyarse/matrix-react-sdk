"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _ErrorUtils = require("../../../utils/ErrorUtils");

var ServerType = _interopRequireWildcard(require("../../views/auth/ServerTypeSelector"));

var _AutoDiscoveryUtils = _interopRequireWildcard(require("../../../utils/AutoDiscoveryUtils"));

var _classnames = _interopRequireDefault(require("classnames"));

var Lifecycle = _interopRequireWildcard(require("../../../Lifecycle"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _AuthPage = _interopRequireDefault(require("../../views/auth/AuthPage"));

var _Login = _interopRequireDefault(require("../../../Login"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// Phases
// Show controls to configure server details
const PHASE_SERVER_DETAILS = 0; // Show the appropriate registration flow(s) for the server

const PHASE_REGISTRATION = 1; // Enable phases for registration

const PHASES_ENABLED = true;

var _default = (0, _createReactClass.default)({
  displayName: 'Registration',
  propTypes: {
    // Called when the user has logged in. Params:
    // - object with userId, deviceId, homeserverUrl, identityServerUrl, accessToken
    // - The user's password, if available and applicable (may be cached in memory
    //   for a short time so the user is not required to re-enter their password
    //   for operations like uploading cross-signing keys).
    onLoggedIn: _propTypes.default.func.isRequired,
    clientSecret: _propTypes.default.string,
    sessionId: _propTypes.default.string,
    makeRegistrationUrl: _propTypes.default.func.isRequired,
    idSid: _propTypes.default.string,
    serverConfig: _propTypes.default.instanceOf(_AutoDiscoveryUtils.ValidatedServerConfig).isRequired,
    brand: _propTypes.default.string,
    email: _propTypes.default.string,
    // registration shouldn't know or care how login is done.
    onLoginClick: _propTypes.default.func.isRequired,
    onServerConfigChange: _propTypes.default.func.isRequired,
    defaultDeviceDisplayName: _propTypes.default.string
  },
  getInitialState: function () {
    const serverType = ServerType.getTypeFromServerConfig(this.props.serverConfig);
    return {
      busy: false,
      errorText: null,
      // We remember the values entered by the user because
      // the registration form will be unmounted during the
      // course of registration, but if there's an error we
      // want to bring back the registration form with the
      // values the user entered still in it. We can keep
      // them in this component's state since this component
      // persist for the duration of the registration process.
      formVals: {
        email: this.props.email
      },
      // true if we're waiting for the user to complete
      // user-interactive auth
      // If we've been given a session ID, we're resuming
      // straight back into UI auth
      doingUIAuth: Boolean(this.props.sessionId),
      serverType,
      // Phase of the overall registration dialog.
      phase: PHASE_REGISTRATION,
      flows: null,
      // If set, we've registered but are not going to log
      // the user in to their new account automatically.
      completedNoSignin: false,
      // We perform liveliness checks later, but for now suppress the errors.
      // We also track the server dead errors independently of the regular errors so
      // that we can render it differently, and override any other error the user may
      // be seeing.
      serverIsAlive: true,
      serverErrorIsFatal: false,
      serverDeadError: "",
      // Our matrix client - part of state because we can't render the UI auth
      // component without it.
      matrixClient: null,
      // whether the HS requires an ID server to register with a threepid
      serverRequiresIdServer: null,
      // The user ID we've just registered
      registeredUsername: null,
      // if a different user ID to the one we just registered is logged in,
      // this is the user ID that's logged in.
      differentLoggedInUserId: null
    };
  },
  componentDidMount: function () {
    this._unmounted = false;

    this._replaceClient();
  },

  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.serverConfig.hsUrl === this.props.serverConfig.hsUrl && newProps.serverConfig.isUrl === this.props.serverConfig.isUrl) return;

    this._replaceClient(newProps.serverConfig); // Handle cases where the user enters "https://matrix.org" for their server
    // from the advanced option - we should default to FREE at that point.


    const serverType = ServerType.getTypeFromServerConfig(newProps.serverConfig);

    if (serverType !== this.state.serverType) {
      // Reset the phase to default phase for the server type.
      this.setState({
        serverType,
        phase: this.getDefaultPhaseForServerType(serverType)
      });
    }
  },

  getDefaultPhaseForServerType(type) {
    switch (type) {
      case ServerType.FREE:
        {
          // Move directly to the registration phase since the server
          // details are fixed.
          return PHASE_REGISTRATION;
        }

      case ServerType.PREMIUM:
      case ServerType.ADVANCED:
        return PHASE_SERVER_DETAILS;
    }
  },

  onServerTypeChange(type) {
    this.setState({
      serverType: type
    }); // When changing server types, set the HS / IS URLs to reasonable defaults for the
    // the new type.

    switch (type) {
      case ServerType.FREE:
        {
          const {
            serverConfig
          } = ServerType.TYPES.FREE;
          this.props.onServerConfigChange(serverConfig);
          break;
        }

      case ServerType.PREMIUM:
        // We can accept whatever server config was the default here as this essentially
        // acts as a slightly different "custom server"/ADVANCED option.
        break;

      case ServerType.ADVANCED:
        // Use the default config from the config
        this.props.onServerConfigChange(_SdkConfig.default.get()["validated_server_config"]);
        break;
    } // Reset the phase to default phase for the server type.


    this.setState({
      phase: this.getDefaultPhaseForServerType(type)
    });
  },

  _replaceClient: async function (serverConfig) {
    this.setState({
      errorText: null,
      serverDeadError: null,
      serverErrorIsFatal: false,
      // busy while we do liveness check (we need to avoid trying to render
      // the UI auth component while we don't have a matrix client)
      busy: true
    });
    if (!serverConfig) serverConfig = this.props.serverConfig; // Do a liveliness check on the URLs

    try {
      await _AutoDiscoveryUtils.default.validateServerConfigWithStaticUrls(serverConfig.hsUrl, serverConfig.isUrl);
      this.setState({
        serverIsAlive: true,
        serverErrorIsFatal: false
      });
    } catch (e) {
      this.setState(_objectSpread({
        busy: false
      }, _AutoDiscoveryUtils.default.authComponentStateForError(e, "register")));

      if (this.state.serverErrorIsFatal) {
        return; // Server is dead - do not continue.
      }
    }

    const {
      hsUrl,
      isUrl
    } = serverConfig;

    const cli = _matrixJsSdk.default.createClient({
      baseUrl: hsUrl,
      idBaseUrl: isUrl
    });

    let serverRequiresIdServer = true;

    try {
      serverRequiresIdServer = await cli.doesServerRequireIdServerParam();
    } catch (e) {
      console.log("Unable to determine is server needs id_server param", e);
    }

    this.setState({
      matrixClient: cli,
      serverRequiresIdServer,
      busy: false
    });

    const showGenericError = e => {
      this.setState({
        errorText: (0, _languageHandler._t)("Unable to query for supported registration methods."),
        // add empty flows array to get rid of spinner
        flows: []
      });
    };

    try {
      await this._makeRegisterRequest({}); // This should never succeed since we specified an empty
      // auth object.

      console.log("Expecting 401 from register request but got success!");
    } catch (e) {
      if (e.httpStatus === 401) {
        this.setState({
          flows: e.data.flows
        });
      } else if (e.httpStatus === 403 && e.errcode === "M_UNKNOWN") {
        // At this point registration is pretty much disabled, but before we do that let's
        // quickly check to see if the server supports SSO instead. If it does, we'll send
        // the user off to the login page to figure their account out.
        try {
          const loginLogic = new _Login.default(hsUrl, isUrl, null, {
            defaultDeviceDisplayName: "riot login check" // We shouldn't ever be used

          });
          const flows = await loginLogic.getFlows();
          const hasSsoFlow = flows.find(f => f.type === 'm.login.sso' || f.type === 'm.login.cas');

          if (hasSsoFlow) {
            // Redirect to login page - server probably expects SSO only
            _dispatcher.default.dispatch({
              action: 'start_login'
            });
          } else {
            this.setState({
              errorText: (0, _languageHandler._t)("Registration has been disabled on this homeserver."),
              // add empty flows array to get rid of spinner
              flows: []
            });
          }
        } catch (e) {
          console.error("Failed to get login flows to check for SSO support", e);
          showGenericError(e);
        }
      } else {
        console.log("Unable to query for supported registration methods.", e);
        showGenericError(e);
      }
    }
  },
  onFormSubmit: function (formVals) {
    this.setState({
      errorText: "",
      busy: true,
      formVals: formVals,
      doingUIAuth: true
    });
  },
  _requestEmailToken: function (emailAddress, clientSecret, sendAttempt, sessionId) {
    return this.state.matrixClient.requestRegisterEmailToken(emailAddress, clientSecret, sendAttempt, this.props.makeRegistrationUrl({
      client_secret: clientSecret,
      hs_url: this.state.matrixClient.getHomeserverUrl(),
      is_url: this.state.matrixClient.getIdentityServerUrl(),
      session_id: sessionId
    }));
  },
  _onUIAuthFinished: async function (success, response, extra) {
    if (!success) {
      let msg = response.message || response.toString(); // can we give a better error message?

      if (response.errcode === 'M_RESOURCE_LIMIT_EXCEEDED') {
        const errorTop = (0, _ErrorUtils.messageForResourceLimitError)(response.data.limit_type, response.data.admin_contact, {
          'monthly_active_user': (0, _languageHandler._td)("This homeserver has hit its Monthly Active User limit."),
          '': (0, _languageHandler._td)("This homeserver has exceeded one of its resource limits.")
        });
        const errorDetail = (0, _ErrorUtils.messageForResourceLimitError)(response.data.limit_type, response.data.admin_contact, {
          '': (0, _languageHandler._td)("Please <a>contact your service administrator</a> to continue using this service.")
        });
        msg = _react.default.createElement("div", null, _react.default.createElement("p", null, errorTop), _react.default.createElement("p", null, errorDetail));
      } else if (response.required_stages && response.required_stages.indexOf('m.login.msisdn') > -1) {
        let msisdnAvailable = false;

        for (const flow of response.available_flows) {
          msisdnAvailable |= flow.stages.indexOf('m.login.msisdn') > -1;
        }

        if (!msisdnAvailable) {
          msg = (0, _languageHandler._t)('This server does not support authentication with a phone number.');
        }
      }

      this.setState({
        busy: false,
        doingUIAuth: false,
        errorText: msg
      });
      return;
    }

    _MatrixClientPeg.MatrixClientPeg.setJustRegisteredUserId(response.user_id);

    const newState = {
      doingUIAuth: false,
      registeredUsername: response.user_id
    }; // The user came in through an email validation link. To avoid overwriting
    // their session, check to make sure the session isn't someone else, and
    // isn't a guest user since we'll usually have set a guest user session before
    // starting the registration process. This isn't perfect since it's possible
    // the user had a separate guest session they didn't actually mean to replace.

    const sessionOwner = Lifecycle.getStoredSessionOwner();
    const sessionIsGuest = Lifecycle.getStoredSessionIsGuest();

    if (sessionOwner && !sessionIsGuest && sessionOwner !== response.userId) {
      console.log("Found a session for ".concat(sessionOwner, " but ").concat(response.userId, " has just registered."));
      newState.differentLoggedInUserId = sessionOwner;
    } else {
      newState.differentLoggedInUserId = null;
    }

    if (response.access_token) {
      const cli = await this.props.onLoggedIn({
        userId: response.user_id,
        deviceId: response.device_id,
        homeserverUrl: this.state.matrixClient.getHomeserverUrl(),
        identityServerUrl: this.state.matrixClient.getIdentityServerUrl(),
        accessToken: response.access_token
      }, this.state.formVals.password);

      this._setupPushers(cli); // we're still busy until we get unmounted: don't show the registration form again


      newState.busy = true;
    } else {
      newState.busy = false;
      newState.completedNoSignin = true;
    }

    this.setState(newState);
  },
  _setupPushers: function (matrixClient) {
    if (!this.props.brand) {
      return Promise.resolve();
    }

    return matrixClient.getPushers().then(resp => {
      const pushers = resp.pushers;

      for (let i = 0; i < pushers.length; ++i) {
        if (pushers[i].kind === 'email') {
          const emailPusher = pushers[i];
          emailPusher.data = {
            brand: this.props.brand
          };
          matrixClient.setPusher(emailPusher).then(() => {
            console.log("Set email branding to " + this.props.brand);
          }, error => {
            console.error("Couldn't set email branding: " + error);
          });
        }
      }
    }, error => {
      console.error("Couldn't get pushers: " + error);
    });
  },
  onLoginClick: function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.props.onLoginClick();
  },

  onGoToFormClicked(ev) {
    ev.preventDefault();
    ev.stopPropagation();

    this._replaceClient();

    this.setState({
      busy: false,
      doingUIAuth: false,
      phase: PHASE_REGISTRATION
    });
  },

  async onServerDetailsNextPhaseClick() {
    this.setState({
      phase: PHASE_REGISTRATION
    });
  },

  onEditServerDetailsClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.setState({
      phase: PHASE_SERVER_DETAILS
    });
  },

  _makeRegisterRequest: function (auth) {
    // We inhibit login if we're trying to register with an email address: this
    // avoids a lot of complex race conditions that can occur if we try to log
    // the user in one one or both of the tabs they might end up with after
    // clicking the email link.
    let inhibitLogin = Boolean(this.state.formVals.email); // Only send inhibitLogin if we're sending username / pw params
    // (Since we need to send no params at all to use the ones saved in the
    // session).

    if (!this.state.formVals.password) inhibitLogin = null;
    const registerParams = {
      username: this.state.formVals.username,
      password: this.state.formVals.password,
      initial_device_display_name: this.props.defaultDeviceDisplayName
    };
    if (auth) registerParams.auth = auth;
    if (inhibitLogin !== undefined && inhibitLogin !== null) registerParams.inhibit_login = inhibitLogin;
    return this.state.matrixClient.registerRequest(registerParams);
  },
  _getUIAuthInputs: function () {
    return {
      emailAddress: this.state.formVals.email,
      phoneCountry: this.state.formVals.phoneCountry,
      phoneNumber: this.state.formVals.phoneNumber
    };
  },
  // Links to the login page shown after registration is completed are routed through this
  // which checks the user hasn't already logged in somewhere else (perhaps we should do
  // this more generally?)
  _onLoginClickWithCheck: async function (ev) {
    ev.preventDefault();
    const sessionLoaded = await Lifecycle.loadSession({
      ignoreGuest: true
    });

    if (!sessionLoaded) {
      // ok fine, there's still no session: really go to the login page
      this.props.onLoginClick();
    }
  },

  renderServerComponent() {
    const ServerTypeSelector = sdk.getComponent("auth.ServerTypeSelector");
    const ServerConfig = sdk.getComponent("auth.ServerConfig");
    const ModularServerConfig = sdk.getComponent("auth.ModularServerConfig");

    if (_SdkConfig.default.get()['disable_custom_urls']) {
      return null;
    } // If we're on a different phase, we only show the server type selector,
    // which is always shown if we allow custom URLs at all.
    // (if there's a fatal server error, we need to show the full server
    // config as the user may need to change servers to resolve the error).


    if (PHASES_ENABLED && this.state.phase !== PHASE_SERVER_DETAILS && !this.state.serverErrorIsFatal) {
      return _react.default.createElement("div", null, _react.default.createElement(ServerTypeSelector, {
        selected: this.state.serverType,
        onChange: this.onServerTypeChange
      }));
    }

    const serverDetailsProps = {};

    if (PHASES_ENABLED) {
      serverDetailsProps.onAfterSubmit = this.onServerDetailsNextPhaseClick;
      serverDetailsProps.submitText = (0, _languageHandler._t)("Next");
      serverDetailsProps.submitClass = "mx_Login_submit";
    }

    let serverDetails = null;

    switch (this.state.serverType) {
      case ServerType.FREE:
        break;

      case ServerType.PREMIUM:
        serverDetails = _react.default.createElement(ModularServerConfig, (0, _extends2.default)({
          serverConfig: this.props.serverConfig,
          onServerConfigChange: this.props.onServerConfigChange,
          delayTimeMs: 250
        }, serverDetailsProps));
        break;

      case ServerType.ADVANCED:
        serverDetails = _react.default.createElement(ServerConfig, (0, _extends2.default)({
          serverConfig: this.props.serverConfig,
          onServerConfigChange: this.props.onServerConfigChange,
          delayTimeMs: 250,
          showIdentityServerIfRequiredByHomeserver: true
        }, serverDetailsProps));
        break;
    }

    return _react.default.createElement("div", null, _react.default.createElement(ServerTypeSelector, {
      selected: this.state.serverType,
      onChange: this.onServerTypeChange
    }), serverDetails);
  },

  renderRegisterComponent() {
    if (PHASES_ENABLED && this.state.phase !== PHASE_REGISTRATION) {
      return null;
    }

    const InteractiveAuth = sdk.getComponent('structures.InteractiveAuth');
    const Spinner = sdk.getComponent('elements.Spinner');
    const RegistrationForm = sdk.getComponent('auth.RegistrationForm');

    if (this.state.matrixClient && this.state.doingUIAuth) {
      return _react.default.createElement(InteractiveAuth, {
        matrixClient: this.state.matrixClient,
        makeRequest: this._makeRegisterRequest,
        onAuthFinished: this._onUIAuthFinished,
        inputs: this._getUIAuthInputs(),
        requestEmailToken: this._requestEmailToken,
        sessionId: this.props.sessionId,
        clientSecret: this.props.clientSecret,
        emailSid: this.props.idSid,
        poll: true
      });
    } else if (!this.state.matrixClient && !this.state.busy) {
      return null;
    } else if (this.state.busy || !this.state.flows) {
      return _react.default.createElement("div", {
        className: "mx_AuthBody_spinner"
      }, _react.default.createElement(Spinner, null));
    } else if (this.state.flows.length) {
      let onEditServerDetailsClick = null; // If custom URLs are allowed and we haven't selected the Free server type, wire
      // up the server details edit link.

      if (PHASES_ENABLED && !_SdkConfig.default.get()['disable_custom_urls'] && this.state.serverType !== ServerType.FREE) {
        onEditServerDetailsClick = this.onEditServerDetailsClick;
      }

      return _react.default.createElement(RegistrationForm, {
        defaultUsername: this.state.formVals.username,
        defaultEmail: this.state.formVals.email,
        defaultPhoneCountry: this.state.formVals.phoneCountry,
        defaultPhoneNumber: this.state.formVals.phoneNumber,
        defaultPassword: this.state.formVals.password,
        onRegisterClick: this.onFormSubmit,
        onEditServerDetailsClick: onEditServerDetailsClick,
        flows: this.state.flows,
        serverConfig: this.props.serverConfig,
        canSubmit: !this.state.serverErrorIsFatal,
        serverRequiresIdServer: this.state.serverRequiresIdServer
      });
    }
  },

  render: function () {
    console.log("---serverType----", this.state.serverType);
    const AuthHeader = sdk.getComponent('auth.AuthHeader');
    const AuthBody = sdk.getComponent("auth.AuthBody");
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let errorText;
    const err = this.state.errorText;

    if (err) {
      errorText = _react.default.createElement("div", {
        className: "mx_Login_error"
      }, err);
    }

    let serverDeadSection;

    if (!this.state.serverIsAlive) {
      const classes = (0, _classnames.default)({
        "mx_Login_error": true,
        "mx_Login_serverError": true,
        "mx_Login_serverErrorNonFatal": !this.state.serverErrorIsFatal
      });
      serverDeadSection = _react.default.createElement("div", {
        className: classes
      }, this.state.serverDeadError);
    }

    const signIn = _react.default.createElement("a", {
      className: "mx_AuthBody_changeFlow",
      onClick: this.onLoginClick,
      href: "#"
    }, (0, _languageHandler._t)('Sign in instead')); // Only show the 'go back' button if you're not looking at the form


    let goBack;

    if (PHASES_ENABLED && this.state.phase !== PHASE_REGISTRATION || this.state.doingUIAuth) {
      goBack = _react.default.createElement("a", {
        className: "mx_AuthBody_changeFlow",
        onClick: this.onGoToFormClicked,
        href: "#"
      }, (0, _languageHandler._t)('Go back'));
    }

    let body;

    if (this.state.completedNoSignin) {
      let regDoneText;

      if (this.state.differentLoggedInUserId) {
        regDoneText = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your new account (%(newAccountId)s) is registered, but you're already " + "logged into a different account (%(loggedInUserId)s).", {
          newAccountId: this.state.registeredUsername,
          loggedInUserId: this.state.differentLoggedInUserId
        })), _react.default.createElement("p", null, _react.default.createElement(AccessibleButton, {
          element: "span",
          className: "mx_linkButton",
          onClick: this._onLoginClickWithCheck
        }, (0, _languageHandler._t)("Continue with previous account"))));
      } else if (this.state.formVals.password) {
        // We're the client that started the registration
        regDoneText = _react.default.createElement("h3", null, (0, _languageHandler._t)("<a>Log in</a> to your new account.", {}, {
          a: sub => _react.default.createElement("a", {
            href: "#/login",
            onClick: this._onLoginClickWithCheck
          }, sub)
        }));
      } else {
        // We're not the original client: the user probably got to us by clicking the
        // email validation link. We can't offer a 'go straight to your account' link
        // as we don't have the original creds.
        regDoneText = _react.default.createElement("h3", null, (0, _languageHandler._t)("You can now close this window or <a>log in</a> to your new account.", {}, {
          a: sub => _react.default.createElement("a", {
            href: "#/login",
            onClick: this._onLoginClickWithCheck
          }, sub)
        }));
      }

      body = _react.default.createElement("div", null, _react.default.createElement("h2", null, (0, _languageHandler._t)("Registration Successful")), regDoneText);
    } else {
      body = _react.default.createElement("div", null, _react.default.createElement("h2", null, (0, _languageHandler._t)('Create your account')), errorText, serverDeadSection, this.renderServerComponent(), this.renderRegisterComponent(), goBack, signIn);
    }

    return _react.default.createElement(_AuthPage.default, null, _react.default.createElement(AuthHeader, null), _react.default.createElement(AuthBody, null, body));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvYXV0aC9SZWdpc3RyYXRpb24uanMiXSwibmFtZXMiOlsiUEhBU0VfU0VSVkVSX0RFVEFJTFMiLCJQSEFTRV9SRUdJU1RSQVRJT04iLCJQSEFTRVNfRU5BQkxFRCIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwib25Mb2dnZWRJbiIsIlByb3BUeXBlcyIsImZ1bmMiLCJpc1JlcXVpcmVkIiwiY2xpZW50U2VjcmV0Iiwic3RyaW5nIiwic2Vzc2lvbklkIiwibWFrZVJlZ2lzdHJhdGlvblVybCIsImlkU2lkIiwic2VydmVyQ29uZmlnIiwiaW5zdGFuY2VPZiIsIlZhbGlkYXRlZFNlcnZlckNvbmZpZyIsImJyYW5kIiwiZW1haWwiLCJvbkxvZ2luQ2xpY2siLCJvblNlcnZlckNvbmZpZ0NoYW5nZSIsImRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZSIsImdldEluaXRpYWxTdGF0ZSIsInNlcnZlclR5cGUiLCJTZXJ2ZXJUeXBlIiwiZ2V0VHlwZUZyb21TZXJ2ZXJDb25maWciLCJwcm9wcyIsImJ1c3kiLCJlcnJvclRleHQiLCJmb3JtVmFscyIsImRvaW5nVUlBdXRoIiwiQm9vbGVhbiIsInBoYXNlIiwiZmxvd3MiLCJjb21wbGV0ZWROb1NpZ25pbiIsInNlcnZlcklzQWxpdmUiLCJzZXJ2ZXJFcnJvcklzRmF0YWwiLCJzZXJ2ZXJEZWFkRXJyb3IiLCJtYXRyaXhDbGllbnQiLCJzZXJ2ZXJSZXF1aXJlc0lkU2VydmVyIiwicmVnaXN0ZXJlZFVzZXJuYW1lIiwiZGlmZmVyZW50TG9nZ2VkSW5Vc2VySWQiLCJjb21wb25lbnREaWRNb3VudCIsIl91bm1vdW50ZWQiLCJfcmVwbGFjZUNsaWVudCIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV3UHJvcHMiLCJoc1VybCIsImlzVXJsIiwic3RhdGUiLCJzZXRTdGF0ZSIsImdldERlZmF1bHRQaGFzZUZvclNlcnZlclR5cGUiLCJ0eXBlIiwiRlJFRSIsIlBSRU1JVU0iLCJBRFZBTkNFRCIsIm9uU2VydmVyVHlwZUNoYW5nZSIsIlRZUEVTIiwiU2RrQ29uZmlnIiwiZ2V0IiwiQXV0b0Rpc2NvdmVyeVV0aWxzIiwidmFsaWRhdGVTZXJ2ZXJDb25maWdXaXRoU3RhdGljVXJscyIsImUiLCJhdXRoQ29tcG9uZW50U3RhdGVGb3JFcnJvciIsImNsaSIsIk1hdHJpeCIsImNyZWF0ZUNsaWVudCIsImJhc2VVcmwiLCJpZEJhc2VVcmwiLCJkb2VzU2VydmVyUmVxdWlyZUlkU2VydmVyUGFyYW0iLCJjb25zb2xlIiwibG9nIiwic2hvd0dlbmVyaWNFcnJvciIsIl9tYWtlUmVnaXN0ZXJSZXF1ZXN0IiwiaHR0cFN0YXR1cyIsImRhdGEiLCJlcnJjb2RlIiwibG9naW5Mb2dpYyIsIkxvZ2luIiwiZ2V0Rmxvd3MiLCJoYXNTc29GbG93IiwiZmluZCIsImYiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsImVycm9yIiwib25Gb3JtU3VibWl0IiwiX3JlcXVlc3RFbWFpbFRva2VuIiwiZW1haWxBZGRyZXNzIiwic2VuZEF0dGVtcHQiLCJyZXF1ZXN0UmVnaXN0ZXJFbWFpbFRva2VuIiwiY2xpZW50X3NlY3JldCIsImhzX3VybCIsImdldEhvbWVzZXJ2ZXJVcmwiLCJpc191cmwiLCJnZXRJZGVudGl0eVNlcnZlclVybCIsInNlc3Npb25faWQiLCJfb25VSUF1dGhGaW5pc2hlZCIsInN1Y2Nlc3MiLCJyZXNwb25zZSIsImV4dHJhIiwibXNnIiwibWVzc2FnZSIsInRvU3RyaW5nIiwiZXJyb3JUb3AiLCJsaW1pdF90eXBlIiwiYWRtaW5fY29udGFjdCIsImVycm9yRGV0YWlsIiwicmVxdWlyZWRfc3RhZ2VzIiwiaW5kZXhPZiIsIm1zaXNkbkF2YWlsYWJsZSIsImZsb3ciLCJhdmFpbGFibGVfZmxvd3MiLCJzdGFnZXMiLCJNYXRyaXhDbGllbnRQZWciLCJzZXRKdXN0UmVnaXN0ZXJlZFVzZXJJZCIsInVzZXJfaWQiLCJuZXdTdGF0ZSIsInNlc3Npb25Pd25lciIsIkxpZmVjeWNsZSIsImdldFN0b3JlZFNlc3Npb25Pd25lciIsInNlc3Npb25Jc0d1ZXN0IiwiZ2V0U3RvcmVkU2Vzc2lvbklzR3Vlc3QiLCJ1c2VySWQiLCJhY2Nlc3NfdG9rZW4iLCJkZXZpY2VJZCIsImRldmljZV9pZCIsImhvbWVzZXJ2ZXJVcmwiLCJpZGVudGl0eVNlcnZlclVybCIsImFjY2Vzc1Rva2VuIiwicGFzc3dvcmQiLCJfc2V0dXBQdXNoZXJzIiwiUHJvbWlzZSIsInJlc29sdmUiLCJnZXRQdXNoZXJzIiwidGhlbiIsInJlc3AiLCJwdXNoZXJzIiwiaSIsImxlbmd0aCIsImtpbmQiLCJlbWFpbFB1c2hlciIsInNldFB1c2hlciIsImV2IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJvbkdvVG9Gb3JtQ2xpY2tlZCIsIm9uU2VydmVyRGV0YWlsc05leHRQaGFzZUNsaWNrIiwib25FZGl0U2VydmVyRGV0YWlsc0NsaWNrIiwiYXV0aCIsImluaGliaXRMb2dpbiIsInJlZ2lzdGVyUGFyYW1zIiwidXNlcm5hbWUiLCJpbml0aWFsX2RldmljZV9kaXNwbGF5X25hbWUiLCJ1bmRlZmluZWQiLCJpbmhpYml0X2xvZ2luIiwicmVnaXN0ZXJSZXF1ZXN0IiwiX2dldFVJQXV0aElucHV0cyIsInBob25lQ291bnRyeSIsInBob25lTnVtYmVyIiwiX29uTG9naW5DbGlja1dpdGhDaGVjayIsInNlc3Npb25Mb2FkZWQiLCJsb2FkU2Vzc2lvbiIsImlnbm9yZUd1ZXN0IiwicmVuZGVyU2VydmVyQ29tcG9uZW50IiwiU2VydmVyVHlwZVNlbGVjdG9yIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiU2VydmVyQ29uZmlnIiwiTW9kdWxhclNlcnZlckNvbmZpZyIsInNlcnZlckRldGFpbHNQcm9wcyIsIm9uQWZ0ZXJTdWJtaXQiLCJzdWJtaXRUZXh0Iiwic3VibWl0Q2xhc3MiLCJzZXJ2ZXJEZXRhaWxzIiwicmVuZGVyUmVnaXN0ZXJDb21wb25lbnQiLCJJbnRlcmFjdGl2ZUF1dGgiLCJTcGlubmVyIiwiUmVnaXN0cmF0aW9uRm9ybSIsInJlbmRlciIsIkF1dGhIZWFkZXIiLCJBdXRoQm9keSIsIkFjY2Vzc2libGVCdXR0b24iLCJlcnIiLCJzZXJ2ZXJEZWFkU2VjdGlvbiIsImNsYXNzZXMiLCJzaWduSW4iLCJnb0JhY2siLCJib2R5IiwicmVnRG9uZVRleHQiLCJuZXdBY2NvdW50SWQiLCJsb2dnZWRJblVzZXJJZCIsImEiLCJzdWIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7O0FBRUE7QUFDQTtBQUNBLE1BQU1BLG9CQUFvQixHQUFHLENBQTdCLEMsQ0FDQTs7QUFDQSxNQUFNQyxrQkFBa0IsR0FBRyxDQUEzQixDLENBRUE7O0FBQ0EsTUFBTUMsY0FBYyxHQUFHLElBQXZCOztlQUVlLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLGNBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsSUFBQUEsVUFBVSxFQUFFQyxtQkFBVUMsSUFBVixDQUFlQyxVQU5wQjtBQVFQQyxJQUFBQSxZQUFZLEVBQUVILG1CQUFVSSxNQVJqQjtBQVNQQyxJQUFBQSxTQUFTLEVBQUVMLG1CQUFVSSxNQVRkO0FBVVBFLElBQUFBLG1CQUFtQixFQUFFTixtQkFBVUMsSUFBVixDQUFlQyxVQVY3QjtBQVdQSyxJQUFBQSxLQUFLLEVBQUVQLG1CQUFVSSxNQVhWO0FBWVBJLElBQUFBLFlBQVksRUFBRVIsbUJBQVVTLFVBQVYsQ0FBcUJDLHlDQUFyQixFQUE0Q1IsVUFabkQ7QUFhUFMsSUFBQUEsS0FBSyxFQUFFWCxtQkFBVUksTUFiVjtBQWNQUSxJQUFBQSxLQUFLLEVBQUVaLG1CQUFVSSxNQWRWO0FBZVA7QUFDQVMsSUFBQUEsWUFBWSxFQUFFYixtQkFBVUMsSUFBVixDQUFlQyxVQWhCdEI7QUFpQlBZLElBQUFBLG9CQUFvQixFQUFFZCxtQkFBVUMsSUFBVixDQUFlQyxVQWpCOUI7QUFrQlBhLElBQUFBLHdCQUF3QixFQUFFZixtQkFBVUk7QUFsQjdCLEdBSGlCO0FBd0I1QlksRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsVUFBTUMsVUFBVSxHQUFHQyxVQUFVLENBQUNDLHVCQUFYLENBQW1DLEtBQUtDLEtBQUwsQ0FBV1osWUFBOUMsQ0FBbkI7QUFFQSxXQUFPO0FBQ0hhLE1BQUFBLElBQUksRUFBRSxLQURIO0FBRUhDLE1BQUFBLFNBQVMsRUFBRSxJQUZSO0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsTUFBQUEsUUFBUSxFQUFFO0FBQ05YLFFBQUFBLEtBQUssRUFBRSxLQUFLUSxLQUFMLENBQVdSO0FBRFosT0FWUDtBQWFIO0FBQ0E7QUFDQTtBQUNBO0FBQ0FZLE1BQUFBLFdBQVcsRUFBRUMsT0FBTyxDQUFDLEtBQUtMLEtBQUwsQ0FBV2YsU0FBWixDQWpCakI7QUFrQkhZLE1BQUFBLFVBbEJHO0FBbUJIO0FBQ0FTLE1BQUFBLEtBQUssRUFBRS9CLGtCQXBCSjtBQXFCSGdDLE1BQUFBLEtBQUssRUFBRSxJQXJCSjtBQXNCSDtBQUNBO0FBQ0FDLE1BQUFBLGlCQUFpQixFQUFFLEtBeEJoQjtBQTBCSDtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxNQUFBQSxhQUFhLEVBQUUsSUE5Qlo7QUErQkhDLE1BQUFBLGtCQUFrQixFQUFFLEtBL0JqQjtBQWdDSEMsTUFBQUEsZUFBZSxFQUFFLEVBaENkO0FBa0NIO0FBQ0E7QUFDQUMsTUFBQUEsWUFBWSxFQUFFLElBcENYO0FBc0NIO0FBQ0FDLE1BQUFBLHNCQUFzQixFQUFFLElBdkNyQjtBQXlDSDtBQUNBQyxNQUFBQSxrQkFBa0IsRUFBRSxJQTFDakI7QUE0Q0g7QUFDQTtBQUNBQyxNQUFBQSx1QkFBdUIsRUFBRTtBQTlDdEIsS0FBUDtBQWdESCxHQTNFMkI7QUE2RTVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLFVBQUwsR0FBa0IsS0FBbEI7O0FBQ0EsU0FBS0MsY0FBTDtBQUNILEdBaEYyQjs7QUFrRjVCO0FBQ0FDLEVBQUFBLGdDQUFnQyxDQUFDQyxRQUFELEVBQVc7QUFDdkMsUUFBSUEsUUFBUSxDQUFDaEMsWUFBVCxDQUFzQmlDLEtBQXRCLEtBQWdDLEtBQUtyQixLQUFMLENBQVdaLFlBQVgsQ0FBd0JpQyxLQUF4RCxJQUNBRCxRQUFRLENBQUNoQyxZQUFULENBQXNCa0MsS0FBdEIsS0FBZ0MsS0FBS3RCLEtBQUwsQ0FBV1osWUFBWCxDQUF3QmtDLEtBRDVELEVBQ21FOztBQUVuRSxTQUFLSixjQUFMLENBQW9CRSxRQUFRLENBQUNoQyxZQUE3QixFQUp1QyxDQU12QztBQUNBOzs7QUFDQSxVQUFNUyxVQUFVLEdBQUdDLFVBQVUsQ0FBQ0MsdUJBQVgsQ0FBbUNxQixRQUFRLENBQUNoQyxZQUE1QyxDQUFuQjs7QUFDQSxRQUFJUyxVQUFVLEtBQUssS0FBSzBCLEtBQUwsQ0FBVzFCLFVBQTlCLEVBQTBDO0FBQ3RDO0FBQ0EsV0FBSzJCLFFBQUwsQ0FBYztBQUNWM0IsUUFBQUEsVUFEVTtBQUVWUyxRQUFBQSxLQUFLLEVBQUUsS0FBS21CLDRCQUFMLENBQWtDNUIsVUFBbEM7QUFGRyxPQUFkO0FBSUg7QUFDSixHQW5HMkI7O0FBc0c1QjRCLEVBQUFBLDRCQUE0QixDQUFDQyxJQUFELEVBQU87QUFDL0IsWUFBUUEsSUFBUjtBQUNJLFdBQUs1QixVQUFVLENBQUM2QixJQUFoQjtBQUFzQjtBQUNsQjtBQUNBO0FBQ0EsaUJBQU9wRCxrQkFBUDtBQUNIOztBQUNELFdBQUt1QixVQUFVLENBQUM4QixPQUFoQjtBQUNBLFdBQUs5QixVQUFVLENBQUMrQixRQUFoQjtBQUNJLGVBQU92RCxvQkFBUDtBQVJSO0FBVUgsR0FqSDJCOztBQW1INUJ3RCxFQUFBQSxrQkFBa0IsQ0FBQ0osSUFBRCxFQUFPO0FBQ3JCLFNBQUtGLFFBQUwsQ0FBYztBQUNWM0IsTUFBQUEsVUFBVSxFQUFFNkI7QUFERixLQUFkLEVBRHFCLENBS3JCO0FBQ0E7O0FBQ0EsWUFBUUEsSUFBUjtBQUNJLFdBQUs1QixVQUFVLENBQUM2QixJQUFoQjtBQUFzQjtBQUNsQixnQkFBTTtBQUFFdkMsWUFBQUE7QUFBRixjQUFtQlUsVUFBVSxDQUFDaUMsS0FBWCxDQUFpQkosSUFBMUM7QUFDQSxlQUFLM0IsS0FBTCxDQUFXTixvQkFBWCxDQUFnQ04sWUFBaEM7QUFDQTtBQUNIOztBQUNELFdBQUtVLFVBQVUsQ0FBQzhCLE9BQWhCO0FBQ0k7QUFDQTtBQUNBOztBQUNKLFdBQUs5QixVQUFVLENBQUMrQixRQUFoQjtBQUNJO0FBQ0EsYUFBSzdCLEtBQUwsQ0FBV04sb0JBQVgsQ0FBZ0NzQyxtQkFBVUMsR0FBVixHQUFnQix5QkFBaEIsQ0FBaEM7QUFDQTtBQWJSLEtBUHFCLENBdUJyQjs7O0FBQ0EsU0FBS1QsUUFBTCxDQUFjO0FBQ1ZsQixNQUFBQSxLQUFLLEVBQUUsS0FBS21CLDRCQUFMLENBQWtDQyxJQUFsQztBQURHLEtBQWQ7QUFHSCxHQTlJMkI7O0FBZ0o1QlIsRUFBQUEsY0FBYyxFQUFFLGdCQUFlOUIsWUFBZixFQUE2QjtBQUN6QyxTQUFLb0MsUUFBTCxDQUFjO0FBQ1Z0QixNQUFBQSxTQUFTLEVBQUUsSUFERDtBQUVWUyxNQUFBQSxlQUFlLEVBQUUsSUFGUDtBQUdWRCxNQUFBQSxrQkFBa0IsRUFBRSxLQUhWO0FBSVY7QUFDQTtBQUNBVCxNQUFBQSxJQUFJLEVBQUU7QUFOSSxLQUFkO0FBUUEsUUFBSSxDQUFDYixZQUFMLEVBQW1CQSxZQUFZLEdBQUcsS0FBS1ksS0FBTCxDQUFXWixZQUExQixDQVRzQixDQVd6Qzs7QUFDQSxRQUFJO0FBQ0EsWUFBTThDLDRCQUFtQkMsa0NBQW5CLENBQ0YvQyxZQUFZLENBQUNpQyxLQURYLEVBRUZqQyxZQUFZLENBQUNrQyxLQUZYLENBQU47QUFJQSxXQUFLRSxRQUFMLENBQWM7QUFDVmYsUUFBQUEsYUFBYSxFQUFFLElBREw7QUFFVkMsUUFBQUEsa0JBQWtCLEVBQUU7QUFGVixPQUFkO0FBSUgsS0FURCxDQVNFLE9BQU8wQixDQUFQLEVBQVU7QUFDUixXQUFLWixRQUFMO0FBQ0l2QixRQUFBQSxJQUFJLEVBQUU7QUFEVixTQUVPaUMsNEJBQW1CRywwQkFBbkIsQ0FBOENELENBQTlDLEVBQWlELFVBQWpELENBRlA7O0FBSUEsVUFBSSxLQUFLYixLQUFMLENBQVdiLGtCQUFmLEVBQW1DO0FBQy9CLGVBRCtCLENBQ3ZCO0FBQ1g7QUFDSjs7QUFFRCxVQUFNO0FBQUNXLE1BQUFBLEtBQUQ7QUFBUUMsTUFBQUE7QUFBUixRQUFpQmxDLFlBQXZCOztBQUNBLFVBQU1rRCxHQUFHLEdBQUdDLHFCQUFPQyxZQUFQLENBQW9CO0FBQzVCQyxNQUFBQSxPQUFPLEVBQUVwQixLQURtQjtBQUU1QnFCLE1BQUFBLFNBQVMsRUFBRXBCO0FBRmlCLEtBQXBCLENBQVo7O0FBS0EsUUFBSVQsc0JBQXNCLEdBQUcsSUFBN0I7O0FBQ0EsUUFBSTtBQUNBQSxNQUFBQSxzQkFBc0IsR0FBRyxNQUFNeUIsR0FBRyxDQUFDSyw4QkFBSixFQUEvQjtBQUNILEtBRkQsQ0FFRSxPQUFPUCxDQUFQLEVBQVU7QUFDUlEsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkscURBQVosRUFBbUVULENBQW5FO0FBQ0g7O0FBRUQsU0FBS1osUUFBTCxDQUFjO0FBQ1ZaLE1BQUFBLFlBQVksRUFBRTBCLEdBREo7QUFFVnpCLE1BQUFBLHNCQUZVO0FBR1ZaLE1BQUFBLElBQUksRUFBRTtBQUhJLEtBQWQ7O0FBS0EsVUFBTTZDLGdCQUFnQixHQUFJVixDQUFELElBQU87QUFDNUIsV0FBS1osUUFBTCxDQUFjO0FBQ1Z0QixRQUFBQSxTQUFTLEVBQUUseUJBQUcscURBQUgsQ0FERDtBQUVWO0FBQ0FLLFFBQUFBLEtBQUssRUFBRTtBQUhHLE9BQWQ7QUFLSCxLQU5EOztBQU9BLFFBQUk7QUFDQSxZQUFNLEtBQUt3QyxvQkFBTCxDQUEwQixFQUExQixDQUFOLENBREEsQ0FFQTtBQUNBOztBQUNBSCxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxzREFBWjtBQUNILEtBTEQsQ0FLRSxPQUFPVCxDQUFQLEVBQVU7QUFDUixVQUFJQSxDQUFDLENBQUNZLFVBQUYsS0FBaUIsR0FBckIsRUFBMEI7QUFDdEIsYUFBS3hCLFFBQUwsQ0FBYztBQUNWakIsVUFBQUEsS0FBSyxFQUFFNkIsQ0FBQyxDQUFDYSxJQUFGLENBQU8xQztBQURKLFNBQWQ7QUFHSCxPQUpELE1BSU8sSUFBSTZCLENBQUMsQ0FBQ1ksVUFBRixLQUFpQixHQUFqQixJQUF3QlosQ0FBQyxDQUFDYyxPQUFGLEtBQWMsV0FBMUMsRUFBdUQ7QUFDMUQ7QUFDQTtBQUNBO0FBQ0EsWUFBSTtBQUNBLGdCQUFNQyxVQUFVLEdBQUcsSUFBSUMsY0FBSixDQUFVL0IsS0FBVixFQUFpQkMsS0FBakIsRUFBd0IsSUFBeEIsRUFBOEI7QUFDN0MzQixZQUFBQSx3QkFBd0IsRUFBRSxrQkFEbUIsQ0FDQzs7QUFERCxXQUE5QixDQUFuQjtBQUdBLGdCQUFNWSxLQUFLLEdBQUcsTUFBTTRDLFVBQVUsQ0FBQ0UsUUFBWCxFQUFwQjtBQUNBLGdCQUFNQyxVQUFVLEdBQUcvQyxLQUFLLENBQUNnRCxJQUFOLENBQVdDLENBQUMsSUFBSUEsQ0FBQyxDQUFDOUIsSUFBRixLQUFXLGFBQVgsSUFBNEI4QixDQUFDLENBQUM5QixJQUFGLEtBQVcsYUFBdkQsQ0FBbkI7O0FBQ0EsY0FBSTRCLFVBQUosRUFBZ0I7QUFDWjtBQUNBRyxnQ0FBSUMsUUFBSixDQUFhO0FBQUNDLGNBQUFBLE1BQU0sRUFBRTtBQUFULGFBQWI7QUFDSCxXQUhELE1BR087QUFDSCxpQkFBS25DLFFBQUwsQ0FBYztBQUNWdEIsY0FBQUEsU0FBUyxFQUFFLHlCQUFHLG9EQUFILENBREQ7QUFFVjtBQUNBSyxjQUFBQSxLQUFLLEVBQUU7QUFIRyxhQUFkO0FBS0g7QUFDSixTQWhCRCxDQWdCRSxPQUFPNkIsQ0FBUCxFQUFVO0FBQ1JRLFVBQUFBLE9BQU8sQ0FBQ2dCLEtBQVIsQ0FBYyxvREFBZCxFQUFvRXhCLENBQXBFO0FBQ0FVLFVBQUFBLGdCQUFnQixDQUFDVixDQUFELENBQWhCO0FBQ0g7QUFDSixPQXhCTSxNQXdCQTtBQUNIUSxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxxREFBWixFQUFtRVQsQ0FBbkU7QUFDQVUsUUFBQUEsZ0JBQWdCLENBQUNWLENBQUQsQ0FBaEI7QUFDSDtBQUNKO0FBQ0osR0EvTzJCO0FBaVA1QnlCLEVBQUFBLFlBQVksRUFBRSxVQUFTMUQsUUFBVCxFQUFtQjtBQUM3QixTQUFLcUIsUUFBTCxDQUFjO0FBQ1Z0QixNQUFBQSxTQUFTLEVBQUUsRUFERDtBQUVWRCxNQUFBQSxJQUFJLEVBQUUsSUFGSTtBQUdWRSxNQUFBQSxRQUFRLEVBQUVBLFFBSEE7QUFJVkMsTUFBQUEsV0FBVyxFQUFFO0FBSkgsS0FBZDtBQU1ILEdBeFAyQjtBQTBQNUIwRCxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTQyxZQUFULEVBQXVCaEYsWUFBdkIsRUFBcUNpRixXQUFyQyxFQUFrRC9FLFNBQWxELEVBQTZEO0FBQzdFLFdBQU8sS0FBS3NDLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QnFELHlCQUF4QixDQUNIRixZQURHLEVBRUhoRixZQUZHLEVBR0hpRixXQUhHLEVBSUgsS0FBS2hFLEtBQUwsQ0FBV2QsbUJBQVgsQ0FBK0I7QUFDM0JnRixNQUFBQSxhQUFhLEVBQUVuRixZQURZO0FBRTNCb0YsTUFBQUEsTUFBTSxFQUFFLEtBQUs1QyxLQUFMLENBQVdYLFlBQVgsQ0FBd0J3RCxnQkFBeEIsRUFGbUI7QUFHM0JDLE1BQUFBLE1BQU0sRUFBRSxLQUFLOUMsS0FBTCxDQUFXWCxZQUFYLENBQXdCMEQsb0JBQXhCLEVBSG1CO0FBSTNCQyxNQUFBQSxVQUFVLEVBQUV0RjtBQUplLEtBQS9CLENBSkcsQ0FBUDtBQVdILEdBdFEyQjtBQXdRNUJ1RixFQUFBQSxpQkFBaUIsRUFBRSxnQkFBZUMsT0FBZixFQUF3QkMsUUFBeEIsRUFBa0NDLEtBQWxDLEVBQXlDO0FBQ3hELFFBQUksQ0FBQ0YsT0FBTCxFQUFjO0FBQ1YsVUFBSUcsR0FBRyxHQUFHRixRQUFRLENBQUNHLE9BQVQsSUFBb0JILFFBQVEsQ0FBQ0ksUUFBVCxFQUE5QixDQURVLENBRVY7O0FBQ0EsVUFBSUosUUFBUSxDQUFDeEIsT0FBVCxLQUFxQiwyQkFBekIsRUFBc0Q7QUFDbEQsY0FBTTZCLFFBQVEsR0FBRyw4Q0FDYkwsUUFBUSxDQUFDekIsSUFBVCxDQUFjK0IsVUFERCxFQUViTixRQUFRLENBQUN6QixJQUFULENBQWNnQyxhQUZELEVBRWdCO0FBQzdCLGlDQUF1QiwwQkFDbkIsd0RBRG1CLENBRE07QUFJN0IsY0FBSSwwQkFDQSwwREFEQTtBQUp5QixTQUZoQixDQUFqQjtBQVVBLGNBQU1DLFdBQVcsR0FBRyw4Q0FDaEJSLFFBQVEsQ0FBQ3pCLElBQVQsQ0FBYytCLFVBREUsRUFFaEJOLFFBQVEsQ0FBQ3pCLElBQVQsQ0FBY2dDLGFBRkUsRUFFYTtBQUM3QixjQUFJLDBCQUNBLGtGQURBO0FBRHlCLFNBRmIsQ0FBcEI7QUFPQUwsUUFBQUEsR0FBRyxHQUFHLDBDQUNGLHdDQUFJRyxRQUFKLENBREUsRUFFRix3Q0FBSUcsV0FBSixDQUZFLENBQU47QUFJSCxPQXRCRCxNQXNCTyxJQUFJUixRQUFRLENBQUNTLGVBQVQsSUFBNEJULFFBQVEsQ0FBQ1MsZUFBVCxDQUF5QkMsT0FBekIsQ0FBaUMsZ0JBQWpDLElBQXFELENBQUMsQ0FBdEYsRUFBeUY7QUFDNUYsWUFBSUMsZUFBZSxHQUFHLEtBQXRCOztBQUNBLGFBQUssTUFBTUMsSUFBWCxJQUFtQlosUUFBUSxDQUFDYSxlQUE1QixFQUE2QztBQUN6Q0YsVUFBQUEsZUFBZSxJQUFJQyxJQUFJLENBQUNFLE1BQUwsQ0FBWUosT0FBWixDQUFvQixnQkFBcEIsSUFBd0MsQ0FBQyxDQUE1RDtBQUNIOztBQUNELFlBQUksQ0FBQ0MsZUFBTCxFQUFzQjtBQUNsQlQsVUFBQUEsR0FBRyxHQUFHLHlCQUFHLGtFQUFILENBQU47QUFDSDtBQUNKOztBQUNELFdBQUtwRCxRQUFMLENBQWM7QUFDVnZCLFFBQUFBLElBQUksRUFBRSxLQURJO0FBRVZHLFFBQUFBLFdBQVcsRUFBRSxLQUZIO0FBR1ZGLFFBQUFBLFNBQVMsRUFBRTBFO0FBSEQsT0FBZDtBQUtBO0FBQ0g7O0FBRURhLHFDQUFnQkMsdUJBQWhCLENBQXdDaEIsUUFBUSxDQUFDaUIsT0FBakQ7O0FBRUEsVUFBTUMsUUFBUSxHQUFHO0FBQ2J4RixNQUFBQSxXQUFXLEVBQUUsS0FEQTtBQUViVSxNQUFBQSxrQkFBa0IsRUFBRTRELFFBQVEsQ0FBQ2lCO0FBRmhCLEtBQWpCLENBN0N3RCxDQWtEeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFNRSxZQUFZLEdBQUdDLFNBQVMsQ0FBQ0MscUJBQVYsRUFBckI7QUFDQSxVQUFNQyxjQUFjLEdBQUdGLFNBQVMsQ0FBQ0csdUJBQVYsRUFBdkI7O0FBQ0EsUUFBSUosWUFBWSxJQUFJLENBQUNHLGNBQWpCLElBQW1DSCxZQUFZLEtBQUtuQixRQUFRLENBQUN3QixNQUFqRSxFQUF5RTtBQUNyRXRELE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUiwrQkFDMkJnRCxZQUQzQixrQkFDK0NuQixRQUFRLENBQUN3QixNQUR4RDtBQUdBTixNQUFBQSxRQUFRLENBQUM3RSx1QkFBVCxHQUFtQzhFLFlBQW5DO0FBQ0gsS0FMRCxNQUtPO0FBQ0hELE1BQUFBLFFBQVEsQ0FBQzdFLHVCQUFULEdBQW1DLElBQW5DO0FBQ0g7O0FBRUQsUUFBSTJELFFBQVEsQ0FBQ3lCLFlBQWIsRUFBMkI7QUFDdkIsWUFBTTdELEdBQUcsR0FBRyxNQUFNLEtBQUt0QyxLQUFMLENBQVdyQixVQUFYLENBQXNCO0FBQ3BDdUgsUUFBQUEsTUFBTSxFQUFFeEIsUUFBUSxDQUFDaUIsT0FEbUI7QUFFcENTLFFBQUFBLFFBQVEsRUFBRTFCLFFBQVEsQ0FBQzJCLFNBRmlCO0FBR3BDQyxRQUFBQSxhQUFhLEVBQUUsS0FBSy9FLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QndELGdCQUF4QixFQUhxQjtBQUlwQ21DLFFBQUFBLGlCQUFpQixFQUFFLEtBQUtoRixLQUFMLENBQVdYLFlBQVgsQ0FBd0IwRCxvQkFBeEIsRUFKaUI7QUFLcENrQyxRQUFBQSxXQUFXLEVBQUU5QixRQUFRLENBQUN5QjtBQUxjLE9BQXRCLEVBTWYsS0FBSzVFLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JzRyxRQU5MLENBQWxCOztBQVFBLFdBQUtDLGFBQUwsQ0FBbUJwRSxHQUFuQixFQVR1QixDQVV2Qjs7O0FBQ0FzRCxNQUFBQSxRQUFRLENBQUMzRixJQUFULEdBQWdCLElBQWhCO0FBQ0gsS0FaRCxNQVlPO0FBQ0gyRixNQUFBQSxRQUFRLENBQUMzRixJQUFULEdBQWdCLEtBQWhCO0FBQ0EyRixNQUFBQSxRQUFRLENBQUNwRixpQkFBVCxHQUE2QixJQUE3QjtBQUNIOztBQUVELFNBQUtnQixRQUFMLENBQWNvRSxRQUFkO0FBQ0gsR0E1VjJCO0FBOFY1QmMsRUFBQUEsYUFBYSxFQUFFLFVBQVM5RixZQUFULEVBQXVCO0FBQ2xDLFFBQUksQ0FBQyxLQUFLWixLQUFMLENBQVdULEtBQWhCLEVBQXVCO0FBQ25CLGFBQU9vSCxPQUFPLENBQUNDLE9BQVIsRUFBUDtBQUNIOztBQUNELFdBQU9oRyxZQUFZLENBQUNpRyxVQUFiLEdBQTBCQyxJQUExQixDQUFnQ0MsSUFBRCxJQUFRO0FBQzFDLFlBQU1DLE9BQU8sR0FBR0QsSUFBSSxDQUFDQyxPQUFyQjs7QUFDQSxXQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdELE9BQU8sQ0FBQ0UsTUFBNUIsRUFBb0MsRUFBRUQsQ0FBdEMsRUFBeUM7QUFDckMsWUFBSUQsT0FBTyxDQUFDQyxDQUFELENBQVAsQ0FBV0UsSUFBWCxLQUFvQixPQUF4QixFQUFpQztBQUM3QixnQkFBTUMsV0FBVyxHQUFHSixPQUFPLENBQUNDLENBQUQsQ0FBM0I7QUFDQUcsVUFBQUEsV0FBVyxDQUFDbkUsSUFBWixHQUFtQjtBQUFFMUQsWUFBQUEsS0FBSyxFQUFFLEtBQUtTLEtBQUwsQ0FBV1Q7QUFBcEIsV0FBbkI7QUFDQXFCLFVBQUFBLFlBQVksQ0FBQ3lHLFNBQWIsQ0FBdUJELFdBQXZCLEVBQW9DTixJQUFwQyxDQUF5QyxNQUFNO0FBQzNDbEUsWUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksMkJBQTJCLEtBQUs3QyxLQUFMLENBQVdULEtBQWxEO0FBQ0gsV0FGRCxFQUVJcUUsS0FBRCxJQUFXO0FBQ1ZoQixZQUFBQSxPQUFPLENBQUNnQixLQUFSLENBQWMsa0NBQWtDQSxLQUFoRDtBQUNILFdBSkQ7QUFLSDtBQUNKO0FBQ0osS0FiTSxFQWFIQSxLQUFELElBQVc7QUFDVmhCLE1BQUFBLE9BQU8sQ0FBQ2dCLEtBQVIsQ0FBYywyQkFBMkJBLEtBQXpDO0FBQ0gsS0FmTSxDQUFQO0FBZ0JILEdBbFgyQjtBQW9YNUJuRSxFQUFBQSxZQUFZLEVBQUUsVUFBUzZILEVBQVQsRUFBYTtBQUN2QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBQ0FELElBQUFBLEVBQUUsQ0FBQ0UsZUFBSDtBQUNBLFNBQUt4SCxLQUFMLENBQVdQLFlBQVg7QUFDSCxHQXhYMkI7O0FBMFg1QmdJLEVBQUFBLGlCQUFpQixDQUFDSCxFQUFELEVBQUs7QUFDbEJBLElBQUFBLEVBQUUsQ0FBQ0MsY0FBSDtBQUNBRCxJQUFBQSxFQUFFLENBQUNFLGVBQUg7O0FBQ0EsU0FBS3RHLGNBQUw7O0FBQ0EsU0FBS00sUUFBTCxDQUFjO0FBQ1Z2QixNQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWRyxNQUFBQSxXQUFXLEVBQUUsS0FGSDtBQUdWRSxNQUFBQSxLQUFLLEVBQUUvQjtBQUhHLEtBQWQ7QUFLSCxHQW5ZMkI7O0FBcVk1QixRQUFNbUosNkJBQU4sR0FBc0M7QUFDbEMsU0FBS2xHLFFBQUwsQ0FBYztBQUNWbEIsTUFBQUEsS0FBSyxFQUFFL0I7QUFERyxLQUFkO0FBR0gsR0F6WTJCOztBQTJZNUJvSixFQUFBQSx3QkFBd0IsQ0FBQ0wsRUFBRCxFQUFLO0FBQ3pCQSxJQUFBQSxFQUFFLENBQUNDLGNBQUg7QUFDQUQsSUFBQUEsRUFBRSxDQUFDRSxlQUFIO0FBQ0EsU0FBS2hHLFFBQUwsQ0FBYztBQUNWbEIsTUFBQUEsS0FBSyxFQUFFaEM7QUFERyxLQUFkO0FBR0gsR0FqWjJCOztBQW1aNUJ5RSxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTNkUsSUFBVCxFQUFlO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSUMsWUFBWSxHQUFHeEgsT0FBTyxDQUFDLEtBQUtrQixLQUFMLENBQVdwQixRQUFYLENBQW9CWCxLQUFyQixDQUExQixDQUxpQyxDQU9qQztBQUNBO0FBQ0E7O0FBQ0EsUUFBSSxDQUFDLEtBQUsrQixLQUFMLENBQVdwQixRQUFYLENBQW9Cc0csUUFBekIsRUFBbUNvQixZQUFZLEdBQUcsSUFBZjtBQUVuQyxVQUFNQyxjQUFjLEdBQUc7QUFDbkJDLE1BQUFBLFFBQVEsRUFBRSxLQUFLeEcsS0FBTCxDQUFXcEIsUUFBWCxDQUFvQjRILFFBRFg7QUFFbkJ0QixNQUFBQSxRQUFRLEVBQUUsS0FBS2xGLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JzRyxRQUZYO0FBR25CdUIsTUFBQUEsMkJBQTJCLEVBQUUsS0FBS2hJLEtBQUwsQ0FBV0w7QUFIckIsS0FBdkI7QUFLQSxRQUFJaUksSUFBSixFQUFVRSxjQUFjLENBQUNGLElBQWYsR0FBc0JBLElBQXRCO0FBQ1YsUUFBSUMsWUFBWSxLQUFLSSxTQUFqQixJQUE4QkosWUFBWSxLQUFLLElBQW5ELEVBQXlEQyxjQUFjLENBQUNJLGFBQWYsR0FBK0JMLFlBQS9CO0FBQ3pELFdBQU8sS0FBS3RHLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QnVILGVBQXhCLENBQXdDTCxjQUF4QyxDQUFQO0FBQ0gsR0F2YTJCO0FBeWE1Qk0sRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixXQUFPO0FBQ0hyRSxNQUFBQSxZQUFZLEVBQUUsS0FBS3hDLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JYLEtBRC9CO0FBRUg2SSxNQUFBQSxZQUFZLEVBQUUsS0FBSzlHLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JrSSxZQUYvQjtBQUdIQyxNQUFBQSxXQUFXLEVBQUUsS0FBSy9HLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JtSTtBQUg5QixLQUFQO0FBS0gsR0EvYTJCO0FBaWI1QjtBQUNBO0FBQ0E7QUFDQUMsRUFBQUEsc0JBQXNCLEVBQUUsZ0JBQWVqQixFQUFmLEVBQW1CO0FBQ3ZDQSxJQUFBQSxFQUFFLENBQUNDLGNBQUg7QUFFQSxVQUFNaUIsYUFBYSxHQUFHLE1BQU0xQyxTQUFTLENBQUMyQyxXQUFWLENBQXNCO0FBQUNDLE1BQUFBLFdBQVcsRUFBRTtBQUFkLEtBQXRCLENBQTVCOztBQUNBLFFBQUksQ0FBQ0YsYUFBTCxFQUFvQjtBQUNoQjtBQUNBLFdBQUt4SSxLQUFMLENBQVdQLFlBQVg7QUFDSDtBQUNKLEdBNWIyQjs7QUE4YjVCa0osRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsVUFBTUMsa0JBQWtCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBM0I7QUFDQSxVQUFNQyxZQUFZLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixtQkFBakIsQ0FBckI7QUFDQSxVQUFNRSxtQkFBbUIsR0FBR0gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUE1Qjs7QUFFQSxRQUFJOUcsbUJBQVVDLEdBQVYsR0FBZ0IscUJBQWhCLENBQUosRUFBNEM7QUFDeEMsYUFBTyxJQUFQO0FBQ0gsS0FQbUIsQ0FTcEI7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUl6RCxjQUFjLElBQUksS0FBSytDLEtBQUwsQ0FBV2pCLEtBQVgsS0FBcUJoQyxvQkFBdkMsSUFBK0QsQ0FBQyxLQUFLaUQsS0FBTCxDQUFXYixrQkFBL0UsRUFBbUc7QUFDL0YsYUFBTywwQ0FDSCw2QkFBQyxrQkFBRDtBQUNJLFFBQUEsUUFBUSxFQUFFLEtBQUthLEtBQUwsQ0FBVzFCLFVBRHpCO0FBRUksUUFBQSxRQUFRLEVBQUUsS0FBS2lDO0FBRm5CLFFBREcsQ0FBUDtBQU1IOztBQUVELFVBQU1tSCxrQkFBa0IsR0FBRyxFQUEzQjs7QUFDQSxRQUFJekssY0FBSixFQUFvQjtBQUNoQnlLLE1BQUFBLGtCQUFrQixDQUFDQyxhQUFuQixHQUFtQyxLQUFLeEIsNkJBQXhDO0FBQ0F1QixNQUFBQSxrQkFBa0IsQ0FBQ0UsVUFBbkIsR0FBZ0MseUJBQUcsTUFBSCxDQUFoQztBQUNBRixNQUFBQSxrQkFBa0IsQ0FBQ0csV0FBbkIsR0FBaUMsaUJBQWpDO0FBQ0g7O0FBRUQsUUFBSUMsYUFBYSxHQUFHLElBQXBCOztBQUNBLFlBQVEsS0FBSzlILEtBQUwsQ0FBVzFCLFVBQW5CO0FBQ0ksV0FBS0MsVUFBVSxDQUFDNkIsSUFBaEI7QUFDSTs7QUFDSixXQUFLN0IsVUFBVSxDQUFDOEIsT0FBaEI7QUFDSXlILFFBQUFBLGFBQWEsR0FBRyw2QkFBQyxtQkFBRDtBQUNaLFVBQUEsWUFBWSxFQUFFLEtBQUtySixLQUFMLENBQVdaLFlBRGI7QUFFWixVQUFBLG9CQUFvQixFQUFFLEtBQUtZLEtBQUwsQ0FBV04sb0JBRnJCO0FBR1osVUFBQSxXQUFXLEVBQUU7QUFIRCxXQUlSdUosa0JBSlEsRUFBaEI7QUFNQTs7QUFDSixXQUFLbkosVUFBVSxDQUFDK0IsUUFBaEI7QUFDSXdILFFBQUFBLGFBQWEsR0FBRyw2QkFBQyxZQUFEO0FBQ1osVUFBQSxZQUFZLEVBQUUsS0FBS3JKLEtBQUwsQ0FBV1osWUFEYjtBQUVaLFVBQUEsb0JBQW9CLEVBQUUsS0FBS1ksS0FBTCxDQUFXTixvQkFGckI7QUFHWixVQUFBLFdBQVcsRUFBRSxHQUhEO0FBSVosVUFBQSx3Q0FBd0MsRUFBRTtBQUo5QixXQUtSdUosa0JBTFEsRUFBaEI7QUFPQTtBQW5CUjs7QUFzQkEsV0FBTywwQ0FDSCw2QkFBQyxrQkFBRDtBQUNJLE1BQUEsUUFBUSxFQUFFLEtBQUsxSCxLQUFMLENBQVcxQixVQUR6QjtBQUVJLE1BQUEsUUFBUSxFQUFFLEtBQUtpQztBQUZuQixNQURHLEVBS0Z1SCxhQUxFLENBQVA7QUFPSCxHQXpmMkI7O0FBMmY1QkMsRUFBQUEsdUJBQXVCLEdBQUc7QUFDdEIsUUFBSTlLLGNBQWMsSUFBSSxLQUFLK0MsS0FBTCxDQUFXakIsS0FBWCxLQUFxQi9CLGtCQUEzQyxFQUErRDtBQUMzRCxhQUFPLElBQVA7QUFDSDs7QUFFRCxVQUFNZ0wsZUFBZSxHQUFHVixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNEJBQWpCLENBQXhCO0FBQ0EsVUFBTVUsT0FBTyxHQUFHWCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBQ0EsVUFBTVcsZ0JBQWdCLEdBQUdaLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1QkFBakIsQ0FBekI7O0FBRUEsUUFBSSxLQUFLdkgsS0FBTCxDQUFXWCxZQUFYLElBQTJCLEtBQUtXLEtBQUwsQ0FBV25CLFdBQTFDLEVBQXVEO0FBQ25ELGFBQU8sNkJBQUMsZUFBRDtBQUNILFFBQUEsWUFBWSxFQUFFLEtBQUttQixLQUFMLENBQVdYLFlBRHRCO0FBRUgsUUFBQSxXQUFXLEVBQUUsS0FBS21DLG9CQUZmO0FBR0gsUUFBQSxjQUFjLEVBQUUsS0FBS3lCLGlCQUhsQjtBQUlILFFBQUEsTUFBTSxFQUFFLEtBQUs0RCxnQkFBTCxFQUpMO0FBS0gsUUFBQSxpQkFBaUIsRUFBRSxLQUFLdEUsa0JBTHJCO0FBTUgsUUFBQSxTQUFTLEVBQUUsS0FBSzlELEtBQUwsQ0FBV2YsU0FObkI7QUFPSCxRQUFBLFlBQVksRUFBRSxLQUFLZSxLQUFMLENBQVdqQixZQVB0QjtBQVFILFFBQUEsUUFBUSxFQUFFLEtBQUtpQixLQUFMLENBQVdiLEtBUmxCO0FBU0gsUUFBQSxJQUFJLEVBQUU7QUFUSCxRQUFQO0FBV0gsS0FaRCxNQVlPLElBQUksQ0FBQyxLQUFLb0MsS0FBTCxDQUFXWCxZQUFaLElBQTRCLENBQUMsS0FBS1csS0FBTCxDQUFXdEIsSUFBNUMsRUFBa0Q7QUFDckQsYUFBTyxJQUFQO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBS3NCLEtBQUwsQ0FBV3RCLElBQVgsSUFBbUIsQ0FBQyxLQUFLc0IsS0FBTCxDQUFXaEIsS0FBbkMsRUFBMEM7QUFDN0MsYUFBTztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSCw2QkFBQyxPQUFELE9BREcsQ0FBUDtBQUdILEtBSk0sTUFJQSxJQUFJLEtBQUtnQixLQUFMLENBQVdoQixLQUFYLENBQWlCMkcsTUFBckIsRUFBNkI7QUFDaEMsVUFBSVMsd0JBQXdCLEdBQUcsSUFBL0IsQ0FEZ0MsQ0FFaEM7QUFDQTs7QUFDQSxVQUNJbkosY0FBYyxJQUNkLENBQUN3RCxtQkFBVUMsR0FBVixHQUFnQixxQkFBaEIsQ0FERCxJQUVBLEtBQUtWLEtBQUwsQ0FBVzFCLFVBQVgsS0FBMEJDLFVBQVUsQ0FBQzZCLElBSHpDLEVBSUU7QUFDRWdHLFFBQUFBLHdCQUF3QixHQUFHLEtBQUtBLHdCQUFoQztBQUNIOztBQUVELGFBQU8sNkJBQUMsZ0JBQUQ7QUFDSCxRQUFBLGVBQWUsRUFBRSxLQUFLcEcsS0FBTCxDQUFXcEIsUUFBWCxDQUFvQjRILFFBRGxDO0FBRUgsUUFBQSxZQUFZLEVBQUUsS0FBS3hHLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JYLEtBRi9CO0FBR0gsUUFBQSxtQkFBbUIsRUFBRSxLQUFLK0IsS0FBTCxDQUFXcEIsUUFBWCxDQUFvQmtJLFlBSHRDO0FBSUgsUUFBQSxrQkFBa0IsRUFBRSxLQUFLOUcsS0FBTCxDQUFXcEIsUUFBWCxDQUFvQm1JLFdBSnJDO0FBS0gsUUFBQSxlQUFlLEVBQUUsS0FBSy9HLEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JzRyxRQUxsQztBQU1ILFFBQUEsZUFBZSxFQUFFLEtBQUs1QyxZQU5uQjtBQU9ILFFBQUEsd0JBQXdCLEVBQUU4RCx3QkFQdkI7QUFRSCxRQUFBLEtBQUssRUFBRSxLQUFLcEcsS0FBTCxDQUFXaEIsS0FSZjtBQVNILFFBQUEsWUFBWSxFQUFFLEtBQUtQLEtBQUwsQ0FBV1osWUFUdEI7QUFVSCxRQUFBLFNBQVMsRUFBRSxDQUFDLEtBQUttQyxLQUFMLENBQVdiLGtCQVZwQjtBQVdILFFBQUEsc0JBQXNCLEVBQUUsS0FBS2EsS0FBTCxDQUFXVjtBQVhoQyxRQUFQO0FBYUg7QUFDSixHQWhqQjJCOztBQWtqQjVCNkksRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDWDlHLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG1CQUFaLEVBQWlDLEtBQUt0QixLQUFMLENBQVcxQixVQUE1QztBQUVKLFVBQU04SixVQUFVLEdBQUdkLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQkFBakIsQ0FBbkI7QUFDQSxVQUFNYyxRQUFRLEdBQUdmLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixlQUFqQixDQUFqQjtBQUNBLFVBQU1lLGdCQUFnQixHQUFHaEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFFBQUk1SSxTQUFKO0FBQ0EsVUFBTTRKLEdBQUcsR0FBRyxLQUFLdkksS0FBTCxDQUFXckIsU0FBdkI7O0FBQ0EsUUFBSTRKLEdBQUosRUFBUztBQUNMNUosTUFBQUEsU0FBUyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUFrQzRKLEdBQWxDLENBQVo7QUFDSDs7QUFFRCxRQUFJQyxpQkFBSjs7QUFDQSxRQUFJLENBQUMsS0FBS3hJLEtBQUwsQ0FBV2QsYUFBaEIsRUFBK0I7QUFDM0IsWUFBTXVKLE9BQU8sR0FBRyx5QkFBVztBQUN2QiwwQkFBa0IsSUFESztBQUV2QixnQ0FBd0IsSUFGRDtBQUd2Qix3Q0FBZ0MsQ0FBQyxLQUFLekksS0FBTCxDQUFXYjtBQUhyQixPQUFYLENBQWhCO0FBS0FxSixNQUFBQSxpQkFBaUIsR0FDYjtBQUFLLFFBQUEsU0FBUyxFQUFFQztBQUFoQixTQUNLLEtBQUt6SSxLQUFMLENBQVdaLGVBRGhCLENBREo7QUFLSDs7QUFFRCxVQUFNc0osTUFBTSxHQUFHO0FBQUcsTUFBQSxTQUFTLEVBQUMsd0JBQWI7QUFBc0MsTUFBQSxPQUFPLEVBQUUsS0FBS3hLLFlBQXBEO0FBQWtFLE1BQUEsSUFBSSxFQUFDO0FBQXZFLE9BQ1QseUJBQUcsaUJBQUgsQ0FEUyxDQUFmLENBM0JlLENBK0JmOzs7QUFDQSxRQUFJeUssTUFBSjs7QUFDQSxRQUFLMUwsY0FBYyxJQUFJLEtBQUsrQyxLQUFMLENBQVdqQixLQUFYLEtBQXFCL0Isa0JBQXhDLElBQStELEtBQUtnRCxLQUFMLENBQVduQixXQUE5RSxFQUEyRjtBQUN2RjhKLE1BQUFBLE1BQU0sR0FBRztBQUFHLFFBQUEsU0FBUyxFQUFDLHdCQUFiO0FBQXNDLFFBQUEsT0FBTyxFQUFFLEtBQUt6QyxpQkFBcEQ7QUFBdUUsUUFBQSxJQUFJLEVBQUM7QUFBNUUsU0FDSCx5QkFBRyxTQUFILENBREcsQ0FBVDtBQUdIOztBQUVELFFBQUkwQyxJQUFKOztBQUNBLFFBQUksS0FBSzVJLEtBQUwsQ0FBV2YsaUJBQWYsRUFBa0M7QUFDOUIsVUFBSTRKLFdBQUo7O0FBQ0EsVUFBSSxLQUFLN0ksS0FBTCxDQUFXUix1QkFBZixFQUF3QztBQUNwQ3FKLFFBQUFBLFdBQVcsR0FBRywwQ0FDVix3Q0FBSSx5QkFDQSwyRUFDQSx1REFGQSxFQUV5RDtBQUNyREMsVUFBQUEsWUFBWSxFQUFFLEtBQUs5SSxLQUFMLENBQVdULGtCQUQ0QjtBQUVyRHdKLFVBQUFBLGNBQWMsRUFBRSxLQUFLL0ksS0FBTCxDQUFXUjtBQUYwQixTQUZ6RCxDQUFKLENBRFUsRUFRVix3Q0FBRyw2QkFBQyxnQkFBRDtBQUFrQixVQUFBLE9BQU8sRUFBQyxNQUExQjtBQUFpQyxVQUFBLFNBQVMsRUFBQyxlQUEzQztBQUEyRCxVQUFBLE9BQU8sRUFBRSxLQUFLd0g7QUFBekUsV0FDRSx5QkFBRyxnQ0FBSCxDQURGLENBQUgsQ0FSVSxDQUFkO0FBWUgsT0FiRCxNQWFPLElBQUksS0FBS2hILEtBQUwsQ0FBV3BCLFFBQVgsQ0FBb0JzRyxRQUF4QixFQUFrQztBQUNyQztBQUNBMkQsUUFBQUEsV0FBVyxHQUFHLHlDQUFLLHlCQUNmLG9DQURlLEVBQ3VCLEVBRHZCLEVBRWY7QUFDSUcsVUFBQUEsQ0FBQyxFQUFHQyxHQUFELElBQVM7QUFBRyxZQUFBLElBQUksRUFBQyxTQUFSO0FBQWtCLFlBQUEsT0FBTyxFQUFFLEtBQUtqQztBQUFoQyxhQUF5RGlDLEdBQXpEO0FBRGhCLFNBRmUsQ0FBTCxDQUFkO0FBTUgsT0FSTSxNQVFBO0FBQ0g7QUFDQTtBQUNBO0FBQ0FKLFFBQUFBLFdBQVcsR0FBRyx5Q0FBSyx5QkFDZixxRUFEZSxFQUN3RCxFQUR4RCxFQUVmO0FBQ0lHLFVBQUFBLENBQUMsRUFBR0MsR0FBRCxJQUFTO0FBQUcsWUFBQSxJQUFJLEVBQUMsU0FBUjtBQUFrQixZQUFBLE9BQU8sRUFBRSxLQUFLakM7QUFBaEMsYUFBeURpQyxHQUF6RDtBQURoQixTQUZlLENBQUwsQ0FBZDtBQU1IOztBQUNETCxNQUFBQSxJQUFJLEdBQUcsMENBQ0gseUNBQUsseUJBQUcseUJBQUgsQ0FBTCxDQURHLEVBRURDLFdBRkMsQ0FBUDtBQUlILEtBdENELE1Bc0NPO0FBQ0hELE1BQUFBLElBQUksR0FBRywwQ0FDSCx5Q0FBTSx5QkFBRyxxQkFBSCxDQUFOLENBREcsRUFFRGpLLFNBRkMsRUFHRDZKLGlCQUhDLEVBSUQsS0FBS3BCLHFCQUFMLEVBSkMsRUFLRCxLQUFLVyx1QkFBTCxFQUxDLEVBTURZLE1BTkMsRUFPREQsTUFQQyxDQUFQO0FBU0g7O0FBRUQsV0FDSSw2QkFBQyxpQkFBRCxRQUNJLDZCQUFDLFVBQUQsT0FESixFQUVJLDZCQUFDLFFBQUQsUUFDTUUsSUFETixDQUZKLENBREo7QUFRSDtBQXBwQjJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCwgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBNYXRyaXggZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90LCBfdGQgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gJy4uLy4uLy4uL1Nka0NvbmZpZyc7XHJcbmltcG9ydCB7IG1lc3NhZ2VGb3JSZXNvdXJjZUxpbWl0RXJyb3IgfSBmcm9tICcuLi8uLi8uLi91dGlscy9FcnJvclV0aWxzJztcclxuaW1wb3J0ICogYXMgU2VydmVyVHlwZSBmcm9tICcuLi8uLi92aWV3cy9hdXRoL1NlcnZlclR5cGVTZWxlY3Rvcic7XHJcbmltcG9ydCBBdXRvRGlzY292ZXJ5VXRpbHMsIHtWYWxpZGF0ZWRTZXJ2ZXJDb25maWd9IGZyb20gXCIuLi8uLi8uLi91dGlscy9BdXRvRGlzY292ZXJ5VXRpbHNcIjtcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSBcImNsYXNzbmFtZXNcIjtcclxuaW1wb3J0ICogYXMgTGlmZWN5Y2xlIGZyb20gJy4uLy4uLy4uL0xpZmVjeWNsZSc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBBdXRoUGFnZSBmcm9tIFwiLi4vLi4vdmlld3MvYXV0aC9BdXRoUGFnZVwiO1xyXG5pbXBvcnQgTG9naW4gZnJvbSBcIi4uLy4uLy4uL0xvZ2luXCI7XHJcbmltcG9ydCBkaXMgZnJvbSBcIi4uLy4uLy4uL2Rpc3BhdGNoZXJcIjtcclxuXHJcbi8vIFBoYXNlc1xyXG4vLyBTaG93IGNvbnRyb2xzIHRvIGNvbmZpZ3VyZSBzZXJ2ZXIgZGV0YWlsc1xyXG5jb25zdCBQSEFTRV9TRVJWRVJfREVUQUlMUyA9IDA7XHJcbi8vIFNob3cgdGhlIGFwcHJvcHJpYXRlIHJlZ2lzdHJhdGlvbiBmbG93KHMpIGZvciB0aGUgc2VydmVyXHJcbmNvbnN0IFBIQVNFX1JFR0lTVFJBVElPTiA9IDE7XHJcblxyXG4vLyBFbmFibGUgcGhhc2VzIGZvciByZWdpc3RyYXRpb25cclxuY29uc3QgUEhBU0VTX0VOQUJMRUQgPSB0cnVlO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1JlZ2lzdHJhdGlvbicsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHVzZXIgaGFzIGxvZ2dlZCBpbi4gUGFyYW1zOlxyXG4gICAgICAgIC8vIC0gb2JqZWN0IHdpdGggdXNlcklkLCBkZXZpY2VJZCwgaG9tZXNlcnZlclVybCwgaWRlbnRpdHlTZXJ2ZXJVcmwsIGFjY2Vzc1Rva2VuXHJcbiAgICAgICAgLy8gLSBUaGUgdXNlcidzIHBhc3N3b3JkLCBpZiBhdmFpbGFibGUgYW5kIGFwcGxpY2FibGUgKG1heSBiZSBjYWNoZWQgaW4gbWVtb3J5XHJcbiAgICAgICAgLy8gICBmb3IgYSBzaG9ydCB0aW1lIHNvIHRoZSB1c2VyIGlzIG5vdCByZXF1aXJlZCB0byByZS1lbnRlciB0aGVpciBwYXNzd29yZFxyXG4gICAgICAgIC8vICAgZm9yIG9wZXJhdGlvbnMgbGlrZSB1cGxvYWRpbmcgY3Jvc3Mtc2lnbmluZyBrZXlzKS5cclxuICAgICAgICBvbkxvZ2dlZEluOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICBjbGllbnRTZWNyZXQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgc2Vzc2lvbklkOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIG1ha2VSZWdpc3RyYXRpb25Vcmw6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgaWRTaWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgc2VydmVyQ29uZmlnOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihWYWxpZGF0ZWRTZXJ2ZXJDb25maWcpLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgYnJhbmQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZW1haWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gcmVnaXN0cmF0aW9uIHNob3VsZG4ndCBrbm93IG9yIGNhcmUgaG93IGxvZ2luIGlzIGRvbmUuXHJcbiAgICAgICAgb25Mb2dpbkNsaWNrOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uU2VydmVyQ29uZmlnQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBzZXJ2ZXJUeXBlID0gU2VydmVyVHlwZS5nZXRUeXBlRnJvbVNlcnZlckNvbmZpZyh0aGlzLnByb3BzLnNlcnZlckNvbmZpZyk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICBlcnJvclRleHQ6IG51bGwsXHJcbiAgICAgICAgICAgIC8vIFdlIHJlbWVtYmVyIHRoZSB2YWx1ZXMgZW50ZXJlZCBieSB0aGUgdXNlciBiZWNhdXNlXHJcbiAgICAgICAgICAgIC8vIHRoZSByZWdpc3RyYXRpb24gZm9ybSB3aWxsIGJlIHVubW91bnRlZCBkdXJpbmcgdGhlXHJcbiAgICAgICAgICAgIC8vIGNvdXJzZSBvZiByZWdpc3RyYXRpb24sIGJ1dCBpZiB0aGVyZSdzIGFuIGVycm9yIHdlXHJcbiAgICAgICAgICAgIC8vIHdhbnQgdG8gYnJpbmcgYmFjayB0aGUgcmVnaXN0cmF0aW9uIGZvcm0gd2l0aCB0aGVcclxuICAgICAgICAgICAgLy8gdmFsdWVzIHRoZSB1c2VyIGVudGVyZWQgc3RpbGwgaW4gaXQuIFdlIGNhbiBrZWVwXHJcbiAgICAgICAgICAgIC8vIHRoZW0gaW4gdGhpcyBjb21wb25lbnQncyBzdGF0ZSBzaW5jZSB0aGlzIGNvbXBvbmVudFxyXG4gICAgICAgICAgICAvLyBwZXJzaXN0IGZvciB0aGUgZHVyYXRpb24gb2YgdGhlIHJlZ2lzdHJhdGlvbiBwcm9jZXNzLlxyXG4gICAgICAgICAgICBmb3JtVmFsczoge1xyXG4gICAgICAgICAgICAgICAgZW1haWw6IHRoaXMucHJvcHMuZW1haWwsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIC8vIHRydWUgaWYgd2UncmUgd2FpdGluZyBmb3IgdGhlIHVzZXIgdG8gY29tcGxldGVcclxuICAgICAgICAgICAgLy8gdXNlci1pbnRlcmFjdGl2ZSBhdXRoXHJcbiAgICAgICAgICAgIC8vIElmIHdlJ3ZlIGJlZW4gZ2l2ZW4gYSBzZXNzaW9uIElELCB3ZSdyZSByZXN1bWluZ1xyXG4gICAgICAgICAgICAvLyBzdHJhaWdodCBiYWNrIGludG8gVUkgYXV0aFxyXG4gICAgICAgICAgICBkb2luZ1VJQXV0aDogQm9vbGVhbih0aGlzLnByb3BzLnNlc3Npb25JZCksXHJcbiAgICAgICAgICAgIHNlcnZlclR5cGUsXHJcbiAgICAgICAgICAgIC8vIFBoYXNlIG9mIHRoZSBvdmVyYWxsIHJlZ2lzdHJhdGlvbiBkaWFsb2cuXHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9SRUdJU1RSQVRJT04sXHJcbiAgICAgICAgICAgIGZsb3dzOiBudWxsLFxyXG4gICAgICAgICAgICAvLyBJZiBzZXQsIHdlJ3ZlIHJlZ2lzdGVyZWQgYnV0IGFyZSBub3QgZ29pbmcgdG8gbG9nXHJcbiAgICAgICAgICAgIC8vIHRoZSB1c2VyIGluIHRvIHRoZWlyIG5ldyBhY2NvdW50IGF1dG9tYXRpY2FsbHkuXHJcbiAgICAgICAgICAgIGNvbXBsZXRlZE5vU2lnbmluOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgIC8vIFdlIHBlcmZvcm0gbGl2ZWxpbmVzcyBjaGVja3MgbGF0ZXIsIGJ1dCBmb3Igbm93IHN1cHByZXNzIHRoZSBlcnJvcnMuXHJcbiAgICAgICAgICAgIC8vIFdlIGFsc28gdHJhY2sgdGhlIHNlcnZlciBkZWFkIGVycm9ycyBpbmRlcGVuZGVudGx5IG9mIHRoZSByZWd1bGFyIGVycm9ycyBzb1xyXG4gICAgICAgICAgICAvLyB0aGF0IHdlIGNhbiByZW5kZXIgaXQgZGlmZmVyZW50bHksIGFuZCBvdmVycmlkZSBhbnkgb3RoZXIgZXJyb3IgdGhlIHVzZXIgbWF5XHJcbiAgICAgICAgICAgIC8vIGJlIHNlZWluZy5cclxuICAgICAgICAgICAgc2VydmVySXNBbGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgc2VydmVyRXJyb3JJc0ZhdGFsOiBmYWxzZSxcclxuICAgICAgICAgICAgc2VydmVyRGVhZEVycm9yOiBcIlwiLFxyXG5cclxuICAgICAgICAgICAgLy8gT3VyIG1hdHJpeCBjbGllbnQgLSBwYXJ0IG9mIHN0YXRlIGJlY2F1c2Ugd2UgY2FuJ3QgcmVuZGVyIHRoZSBVSSBhdXRoXHJcbiAgICAgICAgICAgIC8vIGNvbXBvbmVudCB3aXRob3V0IGl0LlxyXG4gICAgICAgICAgICBtYXRyaXhDbGllbnQ6IG51bGwsXHJcblxyXG4gICAgICAgICAgICAvLyB3aGV0aGVyIHRoZSBIUyByZXF1aXJlcyBhbiBJRCBzZXJ2ZXIgdG8gcmVnaXN0ZXIgd2l0aCBhIHRocmVlcGlkXHJcbiAgICAgICAgICAgIHNlcnZlclJlcXVpcmVzSWRTZXJ2ZXI6IG51bGwsXHJcblxyXG4gICAgICAgICAgICAvLyBUaGUgdXNlciBJRCB3ZSd2ZSBqdXN0IHJlZ2lzdGVyZWRcclxuICAgICAgICAgICAgcmVnaXN0ZXJlZFVzZXJuYW1lOiBudWxsLFxyXG5cclxuICAgICAgICAgICAgLy8gaWYgYSBkaWZmZXJlbnQgdXNlciBJRCB0byB0aGUgb25lIHdlIGp1c3QgcmVnaXN0ZXJlZCBpcyBsb2dnZWQgaW4sXHJcbiAgICAgICAgICAgIC8vIHRoaXMgaXMgdGhlIHVzZXIgSUQgdGhhdCdzIGxvZ2dlZCBpbi5cclxuICAgICAgICAgICAgZGlmZmVyZW50TG9nZ2VkSW5Vc2VySWQ6IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3VubW91bnRlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuX3JlcGxhY2VDbGllbnQoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2Ugd2l0aCBhcHByb3ByaWF0ZSBsaWZlY3ljbGUgZXZlbnRcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5ld1Byb3BzKSB7XHJcbiAgICAgICAgaWYgKG5ld1Byb3BzLnNlcnZlckNvbmZpZy5oc1VybCA9PT0gdGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNVcmwgJiZcclxuICAgICAgICAgICAgbmV3UHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsID09PSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5pc1VybCkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLl9yZXBsYWNlQ2xpZW50KG5ld1Byb3BzLnNlcnZlckNvbmZpZyk7XHJcblxyXG4gICAgICAgIC8vIEhhbmRsZSBjYXNlcyB3aGVyZSB0aGUgdXNlciBlbnRlcnMgXCJodHRwczovL21hdHJpeC5vcmdcIiBmb3IgdGhlaXIgc2VydmVyXHJcbiAgICAgICAgLy8gZnJvbSB0aGUgYWR2YW5jZWQgb3B0aW9uIC0gd2Ugc2hvdWxkIGRlZmF1bHQgdG8gRlJFRSBhdCB0aGF0IHBvaW50LlxyXG4gICAgICAgIGNvbnN0IHNlcnZlclR5cGUgPSBTZXJ2ZXJUeXBlLmdldFR5cGVGcm9tU2VydmVyQ29uZmlnKG5ld1Byb3BzLnNlcnZlckNvbmZpZyk7XHJcbiAgICAgICAgaWYgKHNlcnZlclR5cGUgIT09IHRoaXMuc3RhdGUuc2VydmVyVHlwZSkge1xyXG4gICAgICAgICAgICAvLyBSZXNldCB0aGUgcGhhc2UgdG8gZGVmYXVsdCBwaGFzZSBmb3IgdGhlIHNlcnZlciB0eXBlLlxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlcnZlclR5cGUsXHJcbiAgICAgICAgICAgICAgICBwaGFzZTogdGhpcy5nZXREZWZhdWx0UGhhc2VGb3JTZXJ2ZXJUeXBlKHNlcnZlclR5cGUpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuXHJcbiAgICBnZXREZWZhdWx0UGhhc2VGb3JTZXJ2ZXJUeXBlKHR5cGUpIHtcclxuICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBTZXJ2ZXJUeXBlLkZSRUU6IHtcclxuICAgICAgICAgICAgICAgIC8vIE1vdmUgZGlyZWN0bHkgdG8gdGhlIHJlZ2lzdHJhdGlvbiBwaGFzZSBzaW5jZSB0aGUgc2VydmVyXHJcbiAgICAgICAgICAgICAgICAvLyBkZXRhaWxzIGFyZSBmaXhlZC5cclxuICAgICAgICAgICAgICAgIHJldHVybiBQSEFTRV9SRUdJU1RSQVRJT047XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBTZXJ2ZXJUeXBlLlBSRU1JVU06XHJcbiAgICAgICAgICAgIGNhc2UgU2VydmVyVHlwZS5BRFZBTkNFRDpcclxuICAgICAgICAgICAgICAgIHJldHVybiBQSEFTRV9TRVJWRVJfREVUQUlMUztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU2VydmVyVHlwZUNoYW5nZSh0eXBlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlcnZlclR5cGU6IHR5cGUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gY2hhbmdpbmcgc2VydmVyIHR5cGVzLCBzZXQgdGhlIEhTIC8gSVMgVVJMcyB0byByZWFzb25hYmxlIGRlZmF1bHRzIGZvciB0aGVcclxuICAgICAgICAvLyB0aGUgbmV3IHR5cGUuXHJcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgU2VydmVyVHlwZS5GUkVFOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB7IHNlcnZlckNvbmZpZyB9ID0gU2VydmVyVHlwZS5UWVBFUy5GUkVFO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vblNlcnZlckNvbmZpZ0NoYW5nZShzZXJ2ZXJDb25maWcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBTZXJ2ZXJUeXBlLlBSRU1JVU06XHJcbiAgICAgICAgICAgICAgICAvLyBXZSBjYW4gYWNjZXB0IHdoYXRldmVyIHNlcnZlciBjb25maWcgd2FzIHRoZSBkZWZhdWx0IGhlcmUgYXMgdGhpcyBlc3NlbnRpYWxseVxyXG4gICAgICAgICAgICAgICAgLy8gYWN0cyBhcyBhIHNsaWdodGx5IGRpZmZlcmVudCBcImN1c3RvbSBzZXJ2ZXJcIi9BRFZBTkNFRCBvcHRpb24uXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBTZXJ2ZXJUeXBlLkFEVkFOQ0VEOlxyXG4gICAgICAgICAgICAgICAgLy8gVXNlIHRoZSBkZWZhdWx0IGNvbmZpZyBmcm9tIHRoZSBjb25maWdcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25TZXJ2ZXJDb25maWdDaGFuZ2UoU2RrQ29uZmlnLmdldCgpW1widmFsaWRhdGVkX3NlcnZlcl9jb25maWdcIl0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZXNldCB0aGUgcGhhc2UgdG8gZGVmYXVsdCBwaGFzZSBmb3IgdGhlIHNlcnZlciB0eXBlLlxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwaGFzZTogdGhpcy5nZXREZWZhdWx0UGhhc2VGb3JTZXJ2ZXJUeXBlKHR5cGUpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVwbGFjZUNsaWVudDogYXN5bmMgZnVuY3Rpb24oc2VydmVyQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGVycm9yVGV4dDogbnVsbCxcclxuICAgICAgICAgICAgc2VydmVyRGVhZEVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBzZXJ2ZXJFcnJvcklzRmF0YWw6IGZhbHNlLFxyXG4gICAgICAgICAgICAvLyBidXN5IHdoaWxlIHdlIGRvIGxpdmVuZXNzIGNoZWNrICh3ZSBuZWVkIHRvIGF2b2lkIHRyeWluZyB0byByZW5kZXJcclxuICAgICAgICAgICAgLy8gdGhlIFVJIGF1dGggY29tcG9uZW50IHdoaWxlIHdlIGRvbid0IGhhdmUgYSBtYXRyaXggY2xpZW50KVxyXG4gICAgICAgICAgICBidXN5OiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICghc2VydmVyQ29uZmlnKSBzZXJ2ZXJDb25maWcgPSB0aGlzLnByb3BzLnNlcnZlckNvbmZpZztcclxuXHJcbiAgICAgICAgLy8gRG8gYSBsaXZlbGluZXNzIGNoZWNrIG9uIHRoZSBVUkxzXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgQXV0b0Rpc2NvdmVyeVV0aWxzLnZhbGlkYXRlU2VydmVyQ29uZmlnV2l0aFN0YXRpY1VybHMoXHJcbiAgICAgICAgICAgICAgICBzZXJ2ZXJDb25maWcuaHNVcmwsXHJcbiAgICAgICAgICAgICAgICBzZXJ2ZXJDb25maWcuaXNVcmwsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc2VydmVySXNBbGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHNlcnZlckVycm9ySXNGYXRhbDogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIC4uLkF1dG9EaXNjb3ZlcnlVdGlscy5hdXRoQ29tcG9uZW50U3RhdGVGb3JFcnJvcihlLCBcInJlZ2lzdGVyXCIpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2VydmVyRXJyb3JJc0ZhdGFsKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vIFNlcnZlciBpcyBkZWFkIC0gZG8gbm90IGNvbnRpbnVlLlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB7aHNVcmwsIGlzVXJsfSA9IHNlcnZlckNvbmZpZztcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXguY3JlYXRlQ2xpZW50KHtcclxuICAgICAgICAgICAgYmFzZVVybDogaHNVcmwsXHJcbiAgICAgICAgICAgIGlkQmFzZVVybDogaXNVcmwsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBzZXJ2ZXJSZXF1aXJlc0lkU2VydmVyID0gdHJ1ZTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBzZXJ2ZXJSZXF1aXJlc0lkU2VydmVyID0gYXdhaXQgY2xpLmRvZXNTZXJ2ZXJSZXF1aXJlSWRTZXJ2ZXJQYXJhbSgpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZGV0ZXJtaW5lIGlzIHNlcnZlciBuZWVkcyBpZF9zZXJ2ZXIgcGFyYW1cIiwgZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbWF0cml4Q2xpZW50OiBjbGksXHJcbiAgICAgICAgICAgIHNlcnZlclJlcXVpcmVzSWRTZXJ2ZXIsXHJcbiAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnN0IHNob3dHZW5lcmljRXJyb3IgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGVycm9yVGV4dDogX3QoXCJVbmFibGUgdG8gcXVlcnkgZm9yIHN1cHBvcnRlZCByZWdpc3RyYXRpb24gbWV0aG9kcy5cIiksXHJcbiAgICAgICAgICAgICAgICAvLyBhZGQgZW1wdHkgZmxvd3MgYXJyYXkgdG8gZ2V0IHJpZCBvZiBzcGlubmVyXHJcbiAgICAgICAgICAgICAgICBmbG93czogW10sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5fbWFrZVJlZ2lzdGVyUmVxdWVzdCh7fSk7XHJcbiAgICAgICAgICAgIC8vIFRoaXMgc2hvdWxkIG5ldmVyIHN1Y2NlZWQgc2luY2Ugd2Ugc3BlY2lmaWVkIGFuIGVtcHR5XHJcbiAgICAgICAgICAgIC8vIGF1dGggb2JqZWN0LlxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV4cGVjdGluZyA0MDEgZnJvbSByZWdpc3RlciByZXF1ZXN0IGJ1dCBnb3Qgc3VjY2VzcyFcIik7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBpZiAoZS5odHRwU3RhdHVzID09PSA0MDEpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGZsb3dzOiBlLmRhdGEuZmxvd3MsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlLmh0dHBTdGF0dXMgPT09IDQwMyAmJiBlLmVycmNvZGUgPT09IFwiTV9VTktOT1dOXCIpIHtcclxuICAgICAgICAgICAgICAgIC8vIEF0IHRoaXMgcG9pbnQgcmVnaXN0cmF0aW9uIGlzIHByZXR0eSBtdWNoIGRpc2FibGVkLCBidXQgYmVmb3JlIHdlIGRvIHRoYXQgbGV0J3NcclxuICAgICAgICAgICAgICAgIC8vIHF1aWNrbHkgY2hlY2sgdG8gc2VlIGlmIHRoZSBzZXJ2ZXIgc3VwcG9ydHMgU1NPIGluc3RlYWQuIElmIGl0IGRvZXMsIHdlJ2xsIHNlbmRcclxuICAgICAgICAgICAgICAgIC8vIHRoZSB1c2VyIG9mZiB0byB0aGUgbG9naW4gcGFnZSB0byBmaWd1cmUgdGhlaXIgYWNjb3VudCBvdXQuXHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxvZ2luTG9naWMgPSBuZXcgTG9naW4oaHNVcmwsIGlzVXJsLCBudWxsLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZTogXCJyaW90IGxvZ2luIGNoZWNrXCIsIC8vIFdlIHNob3VsZG4ndCBldmVyIGJlIHVzZWRcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmbG93cyA9IGF3YWl0IGxvZ2luTG9naWMuZ2V0Rmxvd3MoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBoYXNTc29GbG93ID0gZmxvd3MuZmluZChmID0+IGYudHlwZSA9PT0gJ20ubG9naW4uc3NvJyB8fCBmLnR5cGUgPT09ICdtLmxvZ2luLmNhcycpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChoYXNTc29GbG93KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFJlZGlyZWN0IHRvIGxvZ2luIHBhZ2UgLSBzZXJ2ZXIgcHJvYmFibHkgZXhwZWN0cyBTU08gb25seVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3N0YXJ0X2xvZ2luJ30pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0OiBfdChcIlJlZ2lzdHJhdGlvbiBoYXMgYmVlbiBkaXNhYmxlZCBvbiB0aGlzIGhvbWVzZXJ2ZXIuXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYWRkIGVtcHR5IGZsb3dzIGFycmF5IHRvIGdldCByaWQgb2Ygc3Bpbm5lclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxvd3M6IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBnZXQgbG9naW4gZmxvd3MgdG8gY2hlY2sgZm9yIFNTTyBzdXBwb3J0XCIsIGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3dHZW5lcmljRXJyb3IoZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlVuYWJsZSB0byBxdWVyeSBmb3Igc3VwcG9ydGVkIHJlZ2lzdHJhdGlvbiBtZXRob2RzLlwiLCBlKTtcclxuICAgICAgICAgICAgICAgIHNob3dHZW5lcmljRXJyb3IoZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRm9ybVN1Ym1pdDogZnVuY3Rpb24oZm9ybVZhbHMpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZXJyb3JUZXh0OiBcIlwiLFxyXG4gICAgICAgICAgICBidXN5OiB0cnVlLFxyXG4gICAgICAgICAgICBmb3JtVmFsczogZm9ybVZhbHMsXHJcbiAgICAgICAgICAgIGRvaW5nVUlBdXRoOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVxdWVzdEVtYWlsVG9rZW46IGZ1bmN0aW9uKGVtYWlsQWRkcmVzcywgY2xpZW50U2VjcmV0LCBzZW5kQXR0ZW1wdCwgc2Vzc2lvbklkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUubWF0cml4Q2xpZW50LnJlcXVlc3RSZWdpc3RlckVtYWlsVG9rZW4oXHJcbiAgICAgICAgICAgIGVtYWlsQWRkcmVzcyxcclxuICAgICAgICAgICAgY2xpZW50U2VjcmV0LFxyXG4gICAgICAgICAgICBzZW5kQXR0ZW1wdCxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5tYWtlUmVnaXN0cmF0aW9uVXJsKHtcclxuICAgICAgICAgICAgICAgIGNsaWVudF9zZWNyZXQ6IGNsaWVudFNlY3JldCxcclxuICAgICAgICAgICAgICAgIGhzX3VybDogdGhpcy5zdGF0ZS5tYXRyaXhDbGllbnQuZ2V0SG9tZXNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICAgICAgaXNfdXJsOiB0aGlzLnN0YXRlLm1hdHJpeENsaWVudC5nZXRJZGVudGl0eVNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICAgICAgc2Vzc2lvbl9pZDogc2Vzc2lvbklkLFxyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25VSUF1dGhGaW5pc2hlZDogYXN5bmMgZnVuY3Rpb24oc3VjY2VzcywgcmVzcG9uc2UsIGV4dHJhKSB7XHJcbiAgICAgICAgaWYgKCFzdWNjZXNzKSB7XHJcbiAgICAgICAgICAgIGxldCBtc2cgPSByZXNwb25zZS5tZXNzYWdlIHx8IHJlc3BvbnNlLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgICAgIC8vIGNhbiB3ZSBnaXZlIGEgYmV0dGVyIGVycm9yIG1lc3NhZ2U/XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5lcnJjb2RlID09PSAnTV9SRVNPVVJDRV9MSU1JVF9FWENFRURFRCcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGVycm9yVG9wID0gbWVzc2FnZUZvclJlc291cmNlTGltaXRFcnJvcihcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5kYXRhLmxpbWl0X3R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuZGF0YS5hZG1pbl9jb250YWN0LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ21vbnRobHlfYWN0aXZlX3VzZXInOiBfdGQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhpcyBob21lc2VydmVyIGhhcyBoaXQgaXRzIE1vbnRobHkgQWN0aXZlIFVzZXIgbGltaXQuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICAnJzogX3RkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlRoaXMgaG9tZXNlcnZlciBoYXMgZXhjZWVkZWQgb25lIG9mIGl0cyByZXNvdXJjZSBsaW1pdHMuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXJyb3JEZXRhaWwgPSBtZXNzYWdlRm9yUmVzb3VyY2VMaW1pdEVycm9yKFxyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmRhdGEubGltaXRfdHlwZSxcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5kYXRhLmFkbWluX2NvbnRhY3QsIHtcclxuICAgICAgICAgICAgICAgICAgICAnJzogX3RkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlBsZWFzZSA8YT5jb250YWN0IHlvdXIgc2VydmljZSBhZG1pbmlzdHJhdG9yPC9hPiB0byBjb250aW51ZSB1c2luZyB0aGlzIHNlcnZpY2UuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgbXNnID0gPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8cD57ZXJyb3JUb3B9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntlcnJvckRldGFpbH08L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UucmVxdWlyZWRfc3RhZ2VzICYmIHJlc3BvbnNlLnJlcXVpcmVkX3N0YWdlcy5pbmRleE9mKCdtLmxvZ2luLm1zaXNkbicpID4gLTEpIHtcclxuICAgICAgICAgICAgICAgIGxldCBtc2lzZG5BdmFpbGFibGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgZmxvdyBvZiByZXNwb25zZS5hdmFpbGFibGVfZmxvd3MpIHtcclxuICAgICAgICAgICAgICAgICAgICBtc2lzZG5BdmFpbGFibGUgfD0gZmxvdy5zdGFnZXMuaW5kZXhPZignbS5sb2dpbi5tc2lzZG4nKSA+IC0xO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKCFtc2lzZG5BdmFpbGFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBtc2cgPSBfdCgnVGhpcyBzZXJ2ZXIgZG9lcyBub3Qgc3VwcG9ydCBhdXRoZW50aWNhdGlvbiB3aXRoIGEgcGhvbmUgbnVtYmVyLicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBkb2luZ1VJQXV0aDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQ6IG1zZyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5zZXRKdXN0UmVnaXN0ZXJlZFVzZXJJZChyZXNwb25zZS51c2VyX2lkKTtcclxuXHJcbiAgICAgICAgY29uc3QgbmV3U3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGRvaW5nVUlBdXRoOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVnaXN0ZXJlZFVzZXJuYW1lOiByZXNwb25zZS51c2VyX2lkLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIFRoZSB1c2VyIGNhbWUgaW4gdGhyb3VnaCBhbiBlbWFpbCB2YWxpZGF0aW9uIGxpbmsuIFRvIGF2b2lkIG92ZXJ3cml0aW5nXHJcbiAgICAgICAgLy8gdGhlaXIgc2Vzc2lvbiwgY2hlY2sgdG8gbWFrZSBzdXJlIHRoZSBzZXNzaW9uIGlzbid0IHNvbWVvbmUgZWxzZSwgYW5kXHJcbiAgICAgICAgLy8gaXNuJ3QgYSBndWVzdCB1c2VyIHNpbmNlIHdlJ2xsIHVzdWFsbHkgaGF2ZSBzZXQgYSBndWVzdCB1c2VyIHNlc3Npb24gYmVmb3JlXHJcbiAgICAgICAgLy8gc3RhcnRpbmcgdGhlIHJlZ2lzdHJhdGlvbiBwcm9jZXNzLiBUaGlzIGlzbid0IHBlcmZlY3Qgc2luY2UgaXQncyBwb3NzaWJsZVxyXG4gICAgICAgIC8vIHRoZSB1c2VyIGhhZCBhIHNlcGFyYXRlIGd1ZXN0IHNlc3Npb24gdGhleSBkaWRuJ3QgYWN0dWFsbHkgbWVhbiB0byByZXBsYWNlLlxyXG4gICAgICAgIGNvbnN0IHNlc3Npb25Pd25lciA9IExpZmVjeWNsZS5nZXRTdG9yZWRTZXNzaW9uT3duZXIoKTtcclxuICAgICAgICBjb25zdCBzZXNzaW9uSXNHdWVzdCA9IExpZmVjeWNsZS5nZXRTdG9yZWRTZXNzaW9uSXNHdWVzdCgpO1xyXG4gICAgICAgIGlmIChzZXNzaW9uT3duZXIgJiYgIXNlc3Npb25Jc0d1ZXN0ICYmIHNlc3Npb25Pd25lciAhPT0gcmVzcG9uc2UudXNlcklkKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAgICAgYEZvdW5kIGEgc2Vzc2lvbiBmb3IgJHtzZXNzaW9uT3duZXJ9IGJ1dCAke3Jlc3BvbnNlLnVzZXJJZH0gaGFzIGp1c3QgcmVnaXN0ZXJlZC5gLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBuZXdTdGF0ZS5kaWZmZXJlbnRMb2dnZWRJblVzZXJJZCA9IHNlc3Npb25Pd25lcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBuZXdTdGF0ZS5kaWZmZXJlbnRMb2dnZWRJblVzZXJJZCA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAocmVzcG9uc2UuYWNjZXNzX3Rva2VuKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNsaSA9IGF3YWl0IHRoaXMucHJvcHMub25Mb2dnZWRJbih7XHJcbiAgICAgICAgICAgICAgICB1c2VySWQ6IHJlc3BvbnNlLnVzZXJfaWQsXHJcbiAgICAgICAgICAgICAgICBkZXZpY2VJZDogcmVzcG9uc2UuZGV2aWNlX2lkLFxyXG4gICAgICAgICAgICAgICAgaG9tZXNlcnZlclVybDogdGhpcy5zdGF0ZS5tYXRyaXhDbGllbnQuZ2V0SG9tZXNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICAgICAgaWRlbnRpdHlTZXJ2ZXJVcmw6IHRoaXMuc3RhdGUubWF0cml4Q2xpZW50LmdldElkZW50aXR5U2VydmVyVXJsKCksXHJcbiAgICAgICAgICAgICAgICBhY2Nlc3NUb2tlbjogcmVzcG9uc2UuYWNjZXNzX3Rva2VuLFxyXG4gICAgICAgICAgICB9LCB0aGlzLnN0YXRlLmZvcm1WYWxzLnBhc3N3b3JkKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3NldHVwUHVzaGVycyhjbGkpO1xyXG4gICAgICAgICAgICAvLyB3ZSdyZSBzdGlsbCBidXN5IHVudGlsIHdlIGdldCB1bm1vdW50ZWQ6IGRvbid0IHNob3cgdGhlIHJlZ2lzdHJhdGlvbiBmb3JtIGFnYWluXHJcbiAgICAgICAgICAgIG5ld1N0YXRlLmJ1c3kgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5ld1N0YXRlLmJ1c3kgPSBmYWxzZTtcclxuICAgICAgICAgICAgbmV3U3RhdGUuY29tcGxldGVkTm9TaWduaW4gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9zZXR1cFB1c2hlcnM6IGZ1bmN0aW9uKG1hdHJpeENsaWVudCkge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5icmFuZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBtYXRyaXhDbGllbnQuZ2V0UHVzaGVycygpLnRoZW4oKHJlc3ApPT57XHJcbiAgICAgICAgICAgIGNvbnN0IHB1c2hlcnMgPSByZXNwLnB1c2hlcnM7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHVzaGVycy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHB1c2hlcnNbaV0ua2luZCA9PT0gJ2VtYWlsJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVtYWlsUHVzaGVyID0gcHVzaGVyc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICBlbWFpbFB1c2hlci5kYXRhID0geyBicmFuZDogdGhpcy5wcm9wcy5icmFuZCB9O1xyXG4gICAgICAgICAgICAgICAgICAgIG1hdHJpeENsaWVudC5zZXRQdXNoZXIoZW1haWxQdXNoZXIpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNldCBlbWFpbCBicmFuZGluZyB0byBcIiArIHRoaXMucHJvcHMuYnJhbmQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQ291bGRuJ3Qgc2V0IGVtYWlsIGJyYW5kaW5nOiBcIiArIGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQ291bGRuJ3QgZ2V0IHB1c2hlcnM6IFwiICsgZXJyb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkxvZ2luQ2xpY2s6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLnByb3BzLm9uTG9naW5DbGljaygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkdvVG9Gb3JtQ2xpY2tlZChldikge1xyXG4gICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5fcmVwbGFjZUNsaWVudCgpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgZG9pbmdVSUF1dGg6IGZhbHNlLFxyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfUkVHSVNUUkFUSU9OLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBhc3luYyBvblNlcnZlckRldGFpbHNOZXh0UGhhc2VDbGljaygpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1JFR0lTVFJBVElPTixcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NFUlZFUl9ERVRBSUxTLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfbWFrZVJlZ2lzdGVyUmVxdWVzdDogZnVuY3Rpb24oYXV0aCkge1xyXG4gICAgICAgIC8vIFdlIGluaGliaXQgbG9naW4gaWYgd2UncmUgdHJ5aW5nIHRvIHJlZ2lzdGVyIHdpdGggYW4gZW1haWwgYWRkcmVzczogdGhpc1xyXG4gICAgICAgIC8vIGF2b2lkcyBhIGxvdCBvZiBjb21wbGV4IHJhY2UgY29uZGl0aW9ucyB0aGF0IGNhbiBvY2N1ciBpZiB3ZSB0cnkgdG8gbG9nXHJcbiAgICAgICAgLy8gdGhlIHVzZXIgaW4gb25lIG9uZSBvciBib3RoIG9mIHRoZSB0YWJzIHRoZXkgbWlnaHQgZW5kIHVwIHdpdGggYWZ0ZXJcclxuICAgICAgICAvLyBjbGlja2luZyB0aGUgZW1haWwgbGluay5cclxuICAgICAgICBsZXQgaW5oaWJpdExvZ2luID0gQm9vbGVhbih0aGlzLnN0YXRlLmZvcm1WYWxzLmVtYWlsKTtcclxuXHJcbiAgICAgICAgLy8gT25seSBzZW5kIGluaGliaXRMb2dpbiBpZiB3ZSdyZSBzZW5kaW5nIHVzZXJuYW1lIC8gcHcgcGFyYW1zXHJcbiAgICAgICAgLy8gKFNpbmNlIHdlIG5lZWQgdG8gc2VuZCBubyBwYXJhbXMgYXQgYWxsIHRvIHVzZSB0aGUgb25lcyBzYXZlZCBpbiB0aGVcclxuICAgICAgICAvLyBzZXNzaW9uKS5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuZm9ybVZhbHMucGFzc3dvcmQpIGluaGliaXRMb2dpbiA9IG51bGw7XHJcblxyXG4gICAgICAgIGNvbnN0IHJlZ2lzdGVyUGFyYW1zID0ge1xyXG4gICAgICAgICAgICB1c2VybmFtZTogdGhpcy5zdGF0ZS5mb3JtVmFscy51c2VybmFtZSxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6IHRoaXMuc3RhdGUuZm9ybVZhbHMucGFzc3dvcmQsXHJcbiAgICAgICAgICAgIGluaXRpYWxfZGV2aWNlX2Rpc3BsYXlfbmFtZTogdGhpcy5wcm9wcy5kZWZhdWx0RGV2aWNlRGlzcGxheU5hbWUsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAoYXV0aCkgcmVnaXN0ZXJQYXJhbXMuYXV0aCA9IGF1dGg7XHJcbiAgICAgICAgaWYgKGluaGliaXRMb2dpbiAhPT0gdW5kZWZpbmVkICYmIGluaGliaXRMb2dpbiAhPT0gbnVsbCkgcmVnaXN0ZXJQYXJhbXMuaW5oaWJpdF9sb2dpbiA9IGluaGliaXRMb2dpbjtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5tYXRyaXhDbGllbnQucmVnaXN0ZXJSZXF1ZXN0KHJlZ2lzdGVyUGFyYW1zKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFVJQXV0aElucHV0czogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgZW1haWxBZGRyZXNzOiB0aGlzLnN0YXRlLmZvcm1WYWxzLmVtYWlsLFxyXG4gICAgICAgICAgICBwaG9uZUNvdW50cnk6IHRoaXMuc3RhdGUuZm9ybVZhbHMucGhvbmVDb3VudHJ5LFxyXG4gICAgICAgICAgICBwaG9uZU51bWJlcjogdGhpcy5zdGF0ZS5mb3JtVmFscy5waG9uZU51bWJlcixcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMaW5rcyB0byB0aGUgbG9naW4gcGFnZSBzaG93biBhZnRlciByZWdpc3RyYXRpb24gaXMgY29tcGxldGVkIGFyZSByb3V0ZWQgdGhyb3VnaCB0aGlzXHJcbiAgICAvLyB3aGljaCBjaGVja3MgdGhlIHVzZXIgaGFzbid0IGFscmVhZHkgbG9nZ2VkIGluIHNvbWV3aGVyZSBlbHNlIChwZXJoYXBzIHdlIHNob3VsZCBkb1xyXG4gICAgLy8gdGhpcyBtb3JlIGdlbmVyYWxseT8pXHJcbiAgICBfb25Mb2dpbkNsaWNrV2l0aENoZWNrOiBhc3luYyBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHNlc3Npb25Mb2FkZWQgPSBhd2FpdCBMaWZlY3ljbGUubG9hZFNlc3Npb24oe2lnbm9yZUd1ZXN0OiB0cnVlfSk7XHJcbiAgICAgICAgaWYgKCFzZXNzaW9uTG9hZGVkKSB7XHJcbiAgICAgICAgICAgIC8vIG9rIGZpbmUsIHRoZXJlJ3Mgc3RpbGwgbm8gc2Vzc2lvbjogcmVhbGx5IGdvIHRvIHRoZSBsb2dpbiBwYWdlXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25Mb2dpbkNsaWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJTZXJ2ZXJDb21wb25lbnQoKSB7XHJcbiAgICAgICAgY29uc3QgU2VydmVyVHlwZVNlbGVjdG9yID0gc2RrLmdldENvbXBvbmVudChcImF1dGguU2VydmVyVHlwZVNlbGVjdG9yXCIpO1xyXG4gICAgICAgIGNvbnN0IFNlcnZlckNvbmZpZyA9IHNkay5nZXRDb21wb25lbnQoXCJhdXRoLlNlcnZlckNvbmZpZ1wiKTtcclxuICAgICAgICBjb25zdCBNb2R1bGFyU2VydmVyQ29uZmlnID0gc2RrLmdldENvbXBvbmVudChcImF1dGguTW9kdWxhclNlcnZlckNvbmZpZ1wiKTtcclxuXHJcbiAgICAgICAgaWYgKFNka0NvbmZpZy5nZXQoKVsnZGlzYWJsZV9jdXN0b21fdXJscyddKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgd2UncmUgb24gYSBkaWZmZXJlbnQgcGhhc2UsIHdlIG9ubHkgc2hvdyB0aGUgc2VydmVyIHR5cGUgc2VsZWN0b3IsXHJcbiAgICAgICAgLy8gd2hpY2ggaXMgYWx3YXlzIHNob3duIGlmIHdlIGFsbG93IGN1c3RvbSBVUkxzIGF0IGFsbC5cclxuICAgICAgICAvLyAoaWYgdGhlcmUncyBhIGZhdGFsIHNlcnZlciBlcnJvciwgd2UgbmVlZCB0byBzaG93IHRoZSBmdWxsIHNlcnZlclxyXG4gICAgICAgIC8vIGNvbmZpZyBhcyB0aGUgdXNlciBtYXkgbmVlZCB0byBjaGFuZ2Ugc2VydmVycyB0byByZXNvbHZlIHRoZSBlcnJvcikuXHJcbiAgICAgICAgaWYgKFBIQVNFU19FTkFCTEVEICYmIHRoaXMuc3RhdGUucGhhc2UgIT09IFBIQVNFX1NFUlZFUl9ERVRBSUxTICYmICF0aGlzLnN0YXRlLnNlcnZlckVycm9ySXNGYXRhbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxTZXJ2ZXJUeXBlU2VsZWN0b3JcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZD17dGhpcy5zdGF0ZS5zZXJ2ZXJUeXBlfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uU2VydmVyVHlwZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNlcnZlckRldGFpbHNQcm9wcyA9IHt9O1xyXG4gICAgICAgIGlmIChQSEFTRVNfRU5BQkxFRCkge1xyXG4gICAgICAgICAgICBzZXJ2ZXJEZXRhaWxzUHJvcHMub25BZnRlclN1Ym1pdCA9IHRoaXMub25TZXJ2ZXJEZXRhaWxzTmV4dFBoYXNlQ2xpY2s7XHJcbiAgICAgICAgICAgIHNlcnZlckRldGFpbHNQcm9wcy5zdWJtaXRUZXh0ID0gX3QoXCJOZXh0XCIpO1xyXG4gICAgICAgICAgICBzZXJ2ZXJEZXRhaWxzUHJvcHMuc3VibWl0Q2xhc3MgPSBcIm14X0xvZ2luX3N1Ym1pdFwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHNlcnZlckRldGFpbHMgPSBudWxsO1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5zdGF0ZS5zZXJ2ZXJUeXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgU2VydmVyVHlwZS5GUkVFOlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgU2VydmVyVHlwZS5QUkVNSVVNOlxyXG4gICAgICAgICAgICAgICAgc2VydmVyRGV0YWlscyA9IDxNb2R1bGFyU2VydmVyQ29uZmlnXHJcbiAgICAgICAgICAgICAgICAgICAgc2VydmVyQ29uZmlnPXt0aGlzLnByb3BzLnNlcnZlckNvbmZpZ31cclxuICAgICAgICAgICAgICAgICAgICBvblNlcnZlckNvbmZpZ0NoYW5nZT17dGhpcy5wcm9wcy5vblNlcnZlckNvbmZpZ0NoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICBkZWxheVRpbWVNcz17MjUwfVxyXG4gICAgICAgICAgICAgICAgICAgIHsuLi5zZXJ2ZXJEZXRhaWxzUHJvcHN9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFNlcnZlclR5cGUuQURWQU5DRUQ6XHJcbiAgICAgICAgICAgICAgICBzZXJ2ZXJEZXRhaWxzID0gPFNlcnZlckNvbmZpZ1xyXG4gICAgICAgICAgICAgICAgICAgIHNlcnZlckNvbmZpZz17dGhpcy5wcm9wcy5zZXJ2ZXJDb25maWd9XHJcbiAgICAgICAgICAgICAgICAgICAgb25TZXJ2ZXJDb25maWdDaGFuZ2U9e3RoaXMucHJvcHMub25TZXJ2ZXJDb25maWdDaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsYXlUaW1lTXM9ezI1MH1cclxuICAgICAgICAgICAgICAgICAgICBzaG93SWRlbnRpdHlTZXJ2ZXJJZlJlcXVpcmVkQnlIb21lc2VydmVyPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIHsuLi5zZXJ2ZXJEZXRhaWxzUHJvcHN9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxTZXJ2ZXJUeXBlU2VsZWN0b3JcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkPXt0aGlzLnN0YXRlLnNlcnZlclR5cGV9XHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vblNlcnZlclR5cGVDaGFuZ2V9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIHtzZXJ2ZXJEZXRhaWxzfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyUmVnaXN0ZXJDb21wb25lbnQoKSB7XHJcbiAgICAgICAgaWYgKFBIQVNFU19FTkFCTEVEICYmIHRoaXMuc3RhdGUucGhhc2UgIT09IFBIQVNFX1JFR0lTVFJBVElPTikge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IEludGVyYWN0aXZlQXV0aCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuSW50ZXJhY3RpdmVBdXRoJyk7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlNwaW5uZXInKTtcclxuICAgICAgICBjb25zdCBSZWdpc3RyYXRpb25Gb3JtID0gc2RrLmdldENvbXBvbmVudCgnYXV0aC5SZWdpc3RyYXRpb25Gb3JtJyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1hdHJpeENsaWVudCAmJiB0aGlzLnN0YXRlLmRvaW5nVUlBdXRoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiA8SW50ZXJhY3RpdmVBdXRoXHJcbiAgICAgICAgICAgICAgICBtYXRyaXhDbGllbnQ9e3RoaXMuc3RhdGUubWF0cml4Q2xpZW50fVxyXG4gICAgICAgICAgICAgICAgbWFrZVJlcXVlc3Q9e3RoaXMuX21ha2VSZWdpc3RlclJlcXVlc3R9XHJcbiAgICAgICAgICAgICAgICBvbkF1dGhGaW5pc2hlZD17dGhpcy5fb25VSUF1dGhGaW5pc2hlZH1cclxuICAgICAgICAgICAgICAgIGlucHV0cz17dGhpcy5fZ2V0VUlBdXRoSW5wdXRzKCl9XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0RW1haWxUb2tlbj17dGhpcy5fcmVxdWVzdEVtYWlsVG9rZW59XHJcbiAgICAgICAgICAgICAgICBzZXNzaW9uSWQ9e3RoaXMucHJvcHMuc2Vzc2lvbklkfVxyXG4gICAgICAgICAgICAgICAgY2xpZW50U2VjcmV0PXt0aGlzLnByb3BzLmNsaWVudFNlY3JldH1cclxuICAgICAgICAgICAgICAgIGVtYWlsU2lkPXt0aGlzLnByb3BzLmlkU2lkfVxyXG4gICAgICAgICAgICAgICAgcG9sbD17dHJ1ZX1cclxuICAgICAgICAgICAgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5zdGF0ZS5tYXRyaXhDbGllbnQgJiYgIXRoaXMuc3RhdGUuYnVzeSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuYnVzeSB8fCAhdGhpcy5zdGF0ZS5mbG93cykge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9zcGlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8U3Bpbm5lciAvPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmZsb3dzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBsZXQgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrID0gbnVsbDtcclxuICAgICAgICAgICAgLy8gSWYgY3VzdG9tIFVSTHMgYXJlIGFsbG93ZWQgYW5kIHdlIGhhdmVuJ3Qgc2VsZWN0ZWQgdGhlIEZyZWUgc2VydmVyIHR5cGUsIHdpcmVcclxuICAgICAgICAgICAgLy8gdXAgdGhlIHNlcnZlciBkZXRhaWxzIGVkaXQgbGluay5cclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgUEhBU0VTX0VOQUJMRUQgJiZcclxuICAgICAgICAgICAgICAgICFTZGtDb25maWcuZ2V0KClbJ2Rpc2FibGVfY3VzdG9tX3VybHMnXSAmJlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zZXJ2ZXJUeXBlICE9PSBTZXJ2ZXJUeXBlLkZSRUVcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICBvbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2sgPSB0aGlzLm9uRWRpdFNlcnZlckRldGFpbHNDbGljaztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIDxSZWdpc3RyYXRpb25Gb3JtXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0VXNlcm5hbWU9e3RoaXMuc3RhdGUuZm9ybVZhbHMudXNlcm5hbWV9XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0RW1haWw9e3RoaXMuc3RhdGUuZm9ybVZhbHMuZW1haWx9XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0UGhvbmVDb3VudHJ5PXt0aGlzLnN0YXRlLmZvcm1WYWxzLnBob25lQ291bnRyeX1cclxuICAgICAgICAgICAgICAgIGRlZmF1bHRQaG9uZU51bWJlcj17dGhpcy5zdGF0ZS5mb3JtVmFscy5waG9uZU51bWJlcn1cclxuICAgICAgICAgICAgICAgIGRlZmF1bHRQYXNzd29yZD17dGhpcy5zdGF0ZS5mb3JtVmFscy5wYXNzd29yZH1cclxuICAgICAgICAgICAgICAgIG9uUmVnaXN0ZXJDbGljaz17dGhpcy5vbkZvcm1TdWJtaXR9XHJcbiAgICAgICAgICAgICAgICBvbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2s9e29uRWRpdFNlcnZlckRldGFpbHNDbGlja31cclxuICAgICAgICAgICAgICAgIGZsb3dzPXt0aGlzLnN0YXRlLmZsb3dzfVxyXG4gICAgICAgICAgICAgICAgc2VydmVyQ29uZmlnPXt0aGlzLnByb3BzLnNlcnZlckNvbmZpZ31cclxuICAgICAgICAgICAgICAgIGNhblN1Ym1pdD17IXRoaXMuc3RhdGUuc2VydmVyRXJyb3JJc0ZhdGFsfVxyXG4gICAgICAgICAgICAgICAgc2VydmVyUmVxdWlyZXNJZFNlcnZlcj17dGhpcy5zdGF0ZS5zZXJ2ZXJSZXF1aXJlc0lkU2VydmVyfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiLS0tc2VydmVyVHlwZS0tLS1cIiwgdGhpcy5zdGF0ZS5zZXJ2ZXJUeXBlKVxyXG5cclxuICAgICAgICBjb25zdCBBdXRoSGVhZGVyID0gc2RrLmdldENvbXBvbmVudCgnYXV0aC5BdXRoSGVhZGVyJyk7XHJcbiAgICAgICAgY29uc3QgQXV0aEJvZHkgPSBzZGsuZ2V0Q29tcG9uZW50KFwiYXV0aC5BdXRoQm9keVwiKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICBsZXQgZXJyb3JUZXh0O1xyXG4gICAgICAgIGNvbnN0IGVyciA9IHRoaXMuc3RhdGUuZXJyb3JUZXh0O1xyXG4gICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgZXJyb3JUZXh0ID0gPGRpdiBjbGFzc05hbWU9XCJteF9Mb2dpbl9lcnJvclwiPnsgZXJyIH08L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2VydmVyRGVhZFNlY3Rpb247XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNlcnZlcklzQWxpdmUpIHtcclxuICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9lcnJvclwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9zZXJ2ZXJFcnJvclwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Mb2dpbl9zZXJ2ZXJFcnJvck5vbkZhdGFsXCI6ICF0aGlzLnN0YXRlLnNlcnZlckVycm9ySXNGYXRhbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHNlcnZlckRlYWRTZWN0aW9uID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLnNlcnZlckRlYWRFcnJvcn1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2lnbkluID0gPGEgY2xhc3NOYW1lPVwibXhfQXV0aEJvZHlfY2hhbmdlRmxvd1wiIG9uQ2xpY2s9e3RoaXMub25Mb2dpbkNsaWNrfSBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICB7IF90KCdTaWduIGluIGluc3RlYWQnKSB9XHJcbiAgICAgICAgPC9hPjtcclxuXHJcbiAgICAgICAgLy8gT25seSBzaG93IHRoZSAnZ28gYmFjaycgYnV0dG9uIGlmIHlvdSdyZSBub3QgbG9va2luZyBhdCB0aGUgZm9ybVxyXG4gICAgICAgIGxldCBnb0JhY2s7XHJcbiAgICAgICAgaWYgKChQSEFTRVNfRU5BQkxFRCAmJiB0aGlzLnN0YXRlLnBoYXNlICE9PSBQSEFTRV9SRUdJU1RSQVRJT04pIHx8IHRoaXMuc3RhdGUuZG9pbmdVSUF1dGgpIHtcclxuICAgICAgICAgICAgZ29CYWNrID0gPGEgY2xhc3NOYW1lPVwibXhfQXV0aEJvZHlfY2hhbmdlRmxvd1wiIG9uQ2xpY2s9e3RoaXMub25Hb1RvRm9ybUNsaWNrZWR9IGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgICB7IF90KCdHbyBiYWNrJykgfVxyXG4gICAgICAgICAgICA8L2E+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGJvZHk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY29tcGxldGVkTm9TaWduaW4pIHtcclxuICAgICAgICAgICAgbGV0IHJlZ0RvbmVUZXh0O1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5kaWZmZXJlbnRMb2dnZWRJblVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgcmVnRG9uZVRleHQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJZb3VyIG5ldyBhY2NvdW50ICglKG5ld0FjY291bnRJZClzKSBpcyByZWdpc3RlcmVkLCBidXQgeW91J3JlIGFscmVhZHkgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcImxvZ2dlZCBpbnRvIGEgZGlmZmVyZW50IGFjY291bnQgKCUobG9nZ2VkSW5Vc2VySWQpcykuXCIsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ld0FjY291bnRJZDogdGhpcy5zdGF0ZS5yZWdpc3RlcmVkVXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsb2dnZWRJblVzZXJJZDogdGhpcy5zdGF0ZS5kaWZmZXJlbnRMb2dnZWRJblVzZXJJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD48QWNjZXNzaWJsZUJ1dHRvbiBlbGVtZW50PVwic3BhblwiIGNsYXNzTmFtZT1cIm14X2xpbmtCdXR0b25cIiBvbkNsaWNrPXt0aGlzLl9vbkxvZ2luQ2xpY2tXaXRoQ2hlY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJDb250aW51ZSB3aXRoIHByZXZpb3VzIGFjY291bnRcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjwvcD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmZvcm1WYWxzLnBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBXZSdyZSB0aGUgY2xpZW50IHRoYXQgc3RhcnRlZCB0aGUgcmVnaXN0cmF0aW9uXHJcbiAgICAgICAgICAgICAgICByZWdEb25lVGV4dCA9IDxoMz57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8YT5Mb2cgaW48L2E+IHRvIHlvdXIgbmV3IGFjY291bnQuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYTogKHN1YikgPT4gPGEgaHJlZj1cIiMvbG9naW5cIiBvbkNsaWNrPXt0aGlzLl9vbkxvZ2luQ2xpY2tXaXRoQ2hlY2t9PntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKX08L2gzPjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIFdlJ3JlIG5vdCB0aGUgb3JpZ2luYWwgY2xpZW50OiB0aGUgdXNlciBwcm9iYWJseSBnb3QgdG8gdXMgYnkgY2xpY2tpbmcgdGhlXHJcbiAgICAgICAgICAgICAgICAvLyBlbWFpbCB2YWxpZGF0aW9uIGxpbmsuIFdlIGNhbid0IG9mZmVyIGEgJ2dvIHN0cmFpZ2h0IHRvIHlvdXIgYWNjb3VudCcgbGlua1xyXG4gICAgICAgICAgICAgICAgLy8gYXMgd2UgZG9uJ3QgaGF2ZSB0aGUgb3JpZ2luYWwgY3JlZHMuXHJcbiAgICAgICAgICAgICAgICByZWdEb25lVGV4dCA9IDxoMz57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJZb3UgY2FuIG5vdyBjbG9zZSB0aGlzIHdpbmRvdyBvciA8YT5sb2cgaW48L2E+IHRvIHlvdXIgbmV3IGFjY291bnQuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYTogKHN1YikgPT4gPGEgaHJlZj1cIiMvbG9naW5cIiBvbkNsaWNrPXt0aGlzLl9vbkxvZ2luQ2xpY2tXaXRoQ2hlY2t9PntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKX08L2gzPjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBib2R5ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMj57X3QoXCJSZWdpc3RyYXRpb24gU3VjY2Vzc2Z1bFwiKX08L2gyPlxyXG4gICAgICAgICAgICAgICAgeyByZWdEb25lVGV4dCB9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBib2R5ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMj57IF90KCdDcmVhdGUgeW91ciBhY2NvdW50JykgfTwvaDI+XHJcbiAgICAgICAgICAgICAgICB7IGVycm9yVGV4dCB9XHJcbiAgICAgICAgICAgICAgICB7IHNlcnZlckRlYWRTZWN0aW9uIH1cclxuICAgICAgICAgICAgICAgIHsgdGhpcy5yZW5kZXJTZXJ2ZXJDb21wb25lbnQoKSB9XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMucmVuZGVyUmVnaXN0ZXJDb21wb25lbnQoKSB9XHJcbiAgICAgICAgICAgICAgICB7IGdvQmFjayB9XHJcbiAgICAgICAgICAgICAgICB7IHNpZ25JbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxBdXRoUGFnZT5cclxuICAgICAgICAgICAgICAgIDxBdXRoSGVhZGVyIC8+XHJcbiAgICAgICAgICAgICAgICA8QXV0aEJvZHk+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBib2R5IH1cclxuICAgICAgICAgICAgICAgIDwvQXV0aEJvZHk+XHJcbiAgICAgICAgICAgIDwvQXV0aFBhZ2U+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=