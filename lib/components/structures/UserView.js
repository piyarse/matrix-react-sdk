"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

var _MatrixClientPeg = require("../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../index"));

var _Modal = _interopRequireDefault(require("../../Modal"));

var _languageHandler = require("../../languageHandler");

/*
Copyright 2019 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class UserView extends _react.default.Component {
  static get propTypes() {
    return {
      userId: _propTypes.default.string
    };
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    if (this.props.userId) {
      this._loadProfileInfo();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.userId !== this.props.userId) {
      this._loadProfileInfo();
    }
  }

  async _loadProfileInfo() {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    this.setState({
      loading: true
    });
    let profileInfo;

    try {
      profileInfo = await cli.getProfileInfo(this.props.userId);
    } catch (err) {
      const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

      _Modal.default.createTrackedDialog((0, _languageHandler._t)('Could not load user profile'), '', ErrorDialog, {
        title: (0, _languageHandler._t)('Could not load user profile'),
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });

      this.setState({
        loading: false
      });
      return;
    }

    const fakeEvent = new _matrixJsSdk.default.MatrixEvent({
      type: "m.room.member",
      content: profileInfo
    });
    const member = new _matrixJsSdk.default.RoomMember(null, this.props.userId);
    member.setMembershipEvent(fakeEvent);
    this.setState({
      member,
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      const Spinner = sdk.getComponent("elements.Spinner");
      return _react.default.createElement(Spinner, null);
    } else if (this.state.member) {
      const RightPanel = sdk.getComponent('structures.RightPanel');
      const MainSplit = sdk.getComponent('structures.MainSplit');

      const panel = _react.default.createElement(RightPanel, {
        user: this.state.member
      });

      return _react.default.createElement(MainSplit, {
        panel: panel
      }, _react.default.createElement("div", {
        style: {
          flex: "1"
        }
      }));
    } else {
      return _react.default.createElement("div", null);
    }
  }

}

exports.default = UserView;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3N0cnVjdHVyZXMvVXNlclZpZXcuanMiXSwibmFtZXMiOlsiVXNlclZpZXciLCJSZWFjdCIsIkNvbXBvbmVudCIsInByb3BUeXBlcyIsInVzZXJJZCIsIlByb3BUeXBlcyIsInN0cmluZyIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJzdGF0ZSIsImNvbXBvbmVudERpZE1vdW50IiwiX2xvYWRQcm9maWxlSW5mbyIsImNvbXBvbmVudERpZFVwZGF0ZSIsInByZXZQcm9wcyIsImNsaSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInNldFN0YXRlIiwibG9hZGluZyIsInByb2ZpbGVJbmZvIiwiZ2V0UHJvZmlsZUluZm8iLCJlcnIiLCJFcnJvckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJtZXNzYWdlIiwiZmFrZUV2ZW50IiwiTWF0cml4IiwiTWF0cml4RXZlbnQiLCJ0eXBlIiwiY29udGVudCIsIm1lbWJlciIsIlJvb21NZW1iZXIiLCJzZXRNZW1iZXJzaGlwRXZlbnQiLCJyZW5kZXIiLCJTcGlubmVyIiwiUmlnaHRQYW5lbCIsIk1haW5TcGxpdCIsInBhbmVsIiwiZmxleCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBeUJlLE1BQU1BLFFBQU4sU0FBdUJDLGVBQU1DLFNBQTdCLENBQXVDO0FBQ2xELGFBQVdDLFNBQVgsR0FBdUI7QUFDbkIsV0FBTztBQUNIQyxNQUFBQSxNQUFNLEVBQUVDLG1CQUFVQztBQURmLEtBQVA7QUFHSDs7QUFFREMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBQ0EsU0FBS0MsS0FBTCxHQUFhLEVBQWI7QUFDSDs7QUFFREMsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsUUFBSSxLQUFLRixLQUFMLENBQVdKLE1BQWYsRUFBdUI7QUFDbkIsV0FBS08sZ0JBQUw7QUFDSDtBQUNKOztBQUVEQyxFQUFBQSxrQkFBa0IsQ0FBQ0MsU0FBRCxFQUFZO0FBQzFCLFFBQUlBLFNBQVMsQ0FBQ1QsTUFBVixLQUFxQixLQUFLSSxLQUFMLENBQVdKLE1BQXBDLEVBQTRDO0FBQ3hDLFdBQUtPLGdCQUFMO0FBQ0g7QUFDSjs7QUFFRCxRQUFNQSxnQkFBTixHQUF5QjtBQUNyQixVQUFNRyxHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxTQUFLQyxRQUFMLENBQWM7QUFBQ0MsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBZDtBQUNBLFFBQUlDLFdBQUo7O0FBQ0EsUUFBSTtBQUNBQSxNQUFBQSxXQUFXLEdBQUcsTUFBTUwsR0FBRyxDQUFDTSxjQUFKLENBQW1CLEtBQUtaLEtBQUwsQ0FBV0osTUFBOUIsQ0FBcEI7QUFDSCxLQUZELENBRUUsT0FBT2lCLEdBQVAsRUFBWTtBQUNWLFlBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQTBCLHlCQUFHLDZCQUFILENBQTFCLEVBQTZELEVBQTdELEVBQWlFSixXQUFqRSxFQUE4RTtBQUMxRUssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZCQUFILENBRG1FO0FBRTFFQyxRQUFBQSxXQUFXLEVBQUlQLEdBQUcsSUFBSUEsR0FBRyxDQUFDUSxPQUFaLEdBQXVCUixHQUFHLENBQUNRLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRnVCLE9BQTlFOztBQUlBLFdBQUtaLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxPQUFPLEVBQUU7QUFBVixPQUFkO0FBQ0E7QUFDSDs7QUFDRCxVQUFNWSxTQUFTLEdBQUcsSUFBSUMscUJBQU9DLFdBQVgsQ0FBdUI7QUFBQ0MsTUFBQUEsSUFBSSxFQUFFLGVBQVA7QUFBd0JDLE1BQUFBLE9BQU8sRUFBRWY7QUFBakMsS0FBdkIsQ0FBbEI7QUFDQSxVQUFNZ0IsTUFBTSxHQUFHLElBQUlKLHFCQUFPSyxVQUFYLENBQXNCLElBQXRCLEVBQTRCLEtBQUs1QixLQUFMLENBQVdKLE1BQXZDLENBQWY7QUFDQStCLElBQUFBLE1BQU0sQ0FBQ0Usa0JBQVAsQ0FBMEJQLFNBQTFCO0FBQ0EsU0FBS2IsUUFBTCxDQUFjO0FBQUNrQixNQUFBQSxNQUFEO0FBQVNqQixNQUFBQSxPQUFPLEVBQUU7QUFBbEIsS0FBZDtBQUNIOztBQUVEb0IsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLN0IsS0FBTCxDQUFXUyxPQUFmLEVBQXdCO0FBQ3BCLFlBQU1xQixPQUFPLEdBQUdoQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBQ0EsYUFBTyw2QkFBQyxPQUFELE9BQVA7QUFDSCxLQUhELE1BR08sSUFBSSxLQUFLZixLQUFMLENBQVcwQixNQUFmLEVBQXVCO0FBQzFCLFlBQU1LLFVBQVUsR0FBR2pCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1QkFBakIsQ0FBbkI7QUFDQSxZQUFNaUIsU0FBUyxHQUFHbEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFsQjs7QUFDQSxZQUFNa0IsS0FBSyxHQUFHLDZCQUFDLFVBQUQ7QUFBWSxRQUFBLElBQUksRUFBRSxLQUFLakMsS0FBTCxDQUFXMEI7QUFBN0IsUUFBZDs7QUFDQSxhQUFRLDZCQUFDLFNBQUQ7QUFBVyxRQUFBLEtBQUssRUFBRU87QUFBbEIsU0FBeUI7QUFBSyxRQUFBLEtBQUssRUFBRTtBQUFDQyxVQUFBQSxJQUFJLEVBQUU7QUFBUDtBQUFaLFFBQXpCLENBQVI7QUFDSCxLQUxNLE1BS0E7QUFDSCxhQUFRLHlDQUFSO0FBQ0g7QUFDSjs7QUF6RGlEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XHJcbmltcG9ydCBNYXRyaXggZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVc2VyVmlldyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgZ2V0IHByb3BUeXBlcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB1c2VySWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge307XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMudXNlcklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvYWRQcm9maWxlSW5mbygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XHJcbiAgICAgICAgaWYgKHByZXZQcm9wcy51c2VySWQgIT09IHRoaXMucHJvcHMudXNlcklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvYWRQcm9maWxlSW5mbygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfbG9hZFByb2ZpbGVJbmZvKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtsb2FkaW5nOiB0cnVlfSk7XHJcbiAgICAgICAgbGV0IHByb2ZpbGVJbmZvO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHByb2ZpbGVJbmZvID0gYXdhaXQgY2xpLmdldFByb2ZpbGVJbmZvKHRoaXMucHJvcHMudXNlcklkKTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLkVycm9yRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coX3QoJ0NvdWxkIG5vdCBsb2FkIHVzZXIgcHJvZmlsZScpLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnQ291bGQgbm90IGxvYWQgdXNlciBwcm9maWxlJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bG9hZGluZzogZmFsc2V9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBmYWtlRXZlbnQgPSBuZXcgTWF0cml4Lk1hdHJpeEV2ZW50KHt0eXBlOiBcIm0ucm9vbS5tZW1iZXJcIiwgY29udGVudDogcHJvZmlsZUluZm99KTtcclxuICAgICAgICBjb25zdCBtZW1iZXIgPSBuZXcgTWF0cml4LlJvb21NZW1iZXIobnVsbCwgdGhpcy5wcm9wcy51c2VySWQpO1xyXG4gICAgICAgIG1lbWJlci5zZXRNZW1iZXJzaGlwRXZlbnQoZmFrZUV2ZW50KTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHttZW1iZXIsIGxvYWRpbmc6IGZhbHNlfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmxvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLm1lbWJlcikge1xyXG4gICAgICAgICAgICBjb25zdCBSaWdodFBhbmVsID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5SaWdodFBhbmVsJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IE1haW5TcGxpdCA9IHNkay5nZXRDb21wb25lbnQoJ3N0cnVjdHVyZXMuTWFpblNwbGl0Jyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhbmVsID0gPFJpZ2h0UGFuZWwgdXNlcj17dGhpcy5zdGF0ZS5tZW1iZXJ9IC8+O1xyXG4gICAgICAgICAgICByZXR1cm4gKDxNYWluU3BsaXQgcGFuZWw9e3BhbmVsfT48ZGl2IHN0eWxlPXt7ZmxleDogXCIxXCJ9fSAvPjwvTWFpblNwbGl0Pik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICg8ZGl2IC8+KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19