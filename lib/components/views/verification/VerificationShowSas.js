"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _EncryptionInfo = require("../right_panel/EncryptionInfo");

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _DialogButtons = _interopRequireDefault(require("../elements/DialogButtons"));

/*
Copyright 2019 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function capFirst(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
}

class VerificationShowSas extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onMatchClick", () => {
      this.setState({
        pending: true
      });
      this.props.onDone();
    });
    (0, _defineProperty2.default)(this, "onDontMatchClick", () => {
      this.setState({
        cancelling: true
      });
      this.props.onCancel();
    });
    this.state = {
      pending: false
    };
  }

  render() {
    let sasDisplay;
    let sasCaption;

    if (this.props.sas.emoji) {
      const emojiBlocks = this.props.sas.emoji.map((emoji, i) => _react.default.createElement("div", {
        className: "mx_VerificationShowSas_emojiSas_block",
        key: i
      }, _react.default.createElement("div", {
        className: "mx_VerificationShowSas_emojiSas_emoji"
      }, emoji[0]), _react.default.createElement("div", {
        className: "mx_VerificationShowSas_emojiSas_label"
      }, (0, _languageHandler._t)(capFirst(emoji[1])))));
      sasDisplay = _react.default.createElement("div", {
        className: "mx_VerificationShowSas_emojiSas"
      }, emojiBlocks.slice(0, 4), _react.default.createElement("div", {
        className: "mx_VerificationShowSas_emojiSas_break"
      }), emojiBlocks.slice(4));
      sasCaption = this.props.isSelf ? (0, _languageHandler._t)("Confirm the emoji below are displayed on both sessions, in the same order:") : (0, _languageHandler._t)("Verify this user by confirming the following emoji appear on their screen.");
    } else if (this.props.sas.decimal) {
      const numberBlocks = this.props.sas.decimal.map((num, i) => _react.default.createElement("span", {
        key: i
      }, num));
      sasDisplay = _react.default.createElement("div", {
        className: "mx_VerificationShowSas_decimalSas"
      }, numberBlocks);
      sasCaption = this.props.isSelf ? (0, _languageHandler._t)("Verify this session by confirming the following number appears on its screen.") : (0, _languageHandler._t)("Verify this user by confirming the following number appears on their screen.");
    } else {
      return _react.default.createElement("div", null, (0, _languageHandler._t)("Unable to find a supported verification method."), _react.default.createElement(_AccessibleButton.default, {
        kind: "primary",
        onClick: this.props.onCancel,
        className: "mx_UserInfo_wideButton"
      }, (0, _languageHandler._t)('Cancel')));
    }

    let confirm;

    if (this.state.pending || this.state.cancelling) {
      let text;

      if (this.state.pending) {
        if (this.props.isSelf) {
          text = (0, _languageHandler._t)("Waiting for your other session, %(deviceName)s (%(deviceId)s), to verify…", {
            deviceName: this.props.device.getDisplayName(),
            deviceId: this.props.device.deviceId
          });
        } else {
          const {
            displayName
          } = this.props;
          text = (0, _languageHandler._t)("Waiting for %(displayName)s to verify…", {
            displayName
          });
        }
      } else {
        text = (0, _languageHandler._t)("Cancelling…");
      }

      confirm = _react.default.createElement(_EncryptionInfo.PendingActionSpinner, {
        text: text
      });
    } else if (this.props.inDialog) {
      // FIXME: stop using DialogButtons here once this component is only used in the right panel verification
      confirm = _react.default.createElement(_DialogButtons.default, {
        primaryButton: (0, _languageHandler._t)("They match"),
        onPrimaryButtonClick: this.onMatchClick,
        primaryButtonClass: "mx_UserInfo_wideButton",
        cancelButton: (0, _languageHandler._t)("They don't match"),
        onCancel: this.onDontMatchClick,
        cancelButtonClass: "mx_UserInfo_wideButton"
      });
    } else {
      confirm = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onMatchClick,
        kind: "primary"
      }, (0, _languageHandler._t)("They match")), _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onDontMatchClick,
        kind: "danger"
      }, (0, _languageHandler._t)("They don't match")));
    }

    return _react.default.createElement("div", {
      className: "mx_VerificationShowSas"
    }, _react.default.createElement("p", null, sasCaption), sasDisplay, _react.default.createElement("p", null, this.props.isSelf ? "" : (0, _languageHandler._t)("To be secure, do this in person or use a trusted way to communicate.")), confirm);
  }

} // List of Emoji strings from the js-sdk, for i18n


exports.default = VerificationShowSas;
(0, _defineProperty2.default)(VerificationShowSas, "propTypes", {
  pending: _propTypes.default.bool,
  displayName: _propTypes.default.string,
  // required if pending is true
  onDone: _propTypes.default.func.isRequired,
  onCancel: _propTypes.default.func.isRequired,
  sas: _propTypes.default.object.isRequired,
  isSelf: _propTypes.default.bool,
  inDialog: _propTypes.default.bool // whether this component is being shown in a dialog and to use DialogButtons

});
(0, _languageHandler._td)("Dog");
(0, _languageHandler._td)("Cat");
(0, _languageHandler._td)("Lion");
(0, _languageHandler._td)("Horse");
(0, _languageHandler._td)("Unicorn");
(0, _languageHandler._td)("Pig");
(0, _languageHandler._td)("Elephant");
(0, _languageHandler._td)("Rabbit");
(0, _languageHandler._td)("Panda");
(0, _languageHandler._td)("Rooster");
(0, _languageHandler._td)("Penguin");
(0, _languageHandler._td)("Turtle");
(0, _languageHandler._td)("Fish");
(0, _languageHandler._td)("Octopus");
(0, _languageHandler._td)("Butterfly");
(0, _languageHandler._td)("Flower");
(0, _languageHandler._td)("Tree");
(0, _languageHandler._td)("Cactus");
(0, _languageHandler._td)("Mushroom");
(0, _languageHandler._td)("Globe");
(0, _languageHandler._td)("Moon");
(0, _languageHandler._td)("Cloud");
(0, _languageHandler._td)("Fire");
(0, _languageHandler._td)("Banana");
(0, _languageHandler._td)("Apple");
(0, _languageHandler._td)("Strawberry");
(0, _languageHandler._td)("Corn");
(0, _languageHandler._td)("Pizza");
(0, _languageHandler._td)("Cake");
(0, _languageHandler._td)("Heart");
(0, _languageHandler._td)("Smiley");
(0, _languageHandler._td)("Robot");
(0, _languageHandler._td)("Hat");
(0, _languageHandler._td)("Glasses");
(0, _languageHandler._td)("Spanner");
(0, _languageHandler._td)("Santa");
(0, _languageHandler._td)("Thumbs up");
(0, _languageHandler._td)("Umbrella");
(0, _languageHandler._td)("Hourglass");
(0, _languageHandler._td)("Clock");
(0, _languageHandler._td)("Gift");
(0, _languageHandler._td)("Light bulb");
(0, _languageHandler._td)("Book");
(0, _languageHandler._td)("Pencil");
(0, _languageHandler._td)("Paperclip");
(0, _languageHandler._td)("Scissors");
(0, _languageHandler._td)("Lock");
(0, _languageHandler._td)("Key");
(0, _languageHandler._td)("Hammer");
(0, _languageHandler._td)("Telephone");
(0, _languageHandler._td)("Flag");
(0, _languageHandler._td)("Train");
(0, _languageHandler._td)("Bicycle");
(0, _languageHandler._td)("Aeroplane");
(0, _languageHandler._td)("Rocket");
(0, _languageHandler._td)("Trophy");
(0, _languageHandler._td)("Ball");
(0, _languageHandler._td)("Guitar");
(0, _languageHandler._td)("Trumpet");
(0, _languageHandler._td)("Bell");
(0, _languageHandler._td)("Anchor");
(0, _languageHandler._td)("Headphones");
(0, _languageHandler._td)("Folder");
(0, _languageHandler._td)("Pin");
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3ZlcmlmaWNhdGlvbi9WZXJpZmljYXRpb25TaG93U2FzLmpzIl0sIm5hbWVzIjpbImNhcEZpcnN0IiwicyIsImNoYXJBdCIsInRvVXBwZXJDYXNlIiwic2xpY2UiLCJWZXJpZmljYXRpb25TaG93U2FzIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwic2V0U3RhdGUiLCJwZW5kaW5nIiwib25Eb25lIiwiY2FuY2VsbGluZyIsIm9uQ2FuY2VsIiwic3RhdGUiLCJyZW5kZXIiLCJzYXNEaXNwbGF5Iiwic2FzQ2FwdGlvbiIsInNhcyIsImVtb2ppIiwiZW1vamlCbG9ja3MiLCJtYXAiLCJpIiwiaXNTZWxmIiwiZGVjaW1hbCIsIm51bWJlckJsb2NrcyIsIm51bSIsImNvbmZpcm0iLCJ0ZXh0IiwiZGV2aWNlTmFtZSIsImRldmljZSIsImdldERpc3BsYXlOYW1lIiwiZGV2aWNlSWQiLCJkaXNwbGF5TmFtZSIsImluRGlhbG9nIiwib25NYXRjaENsaWNrIiwib25Eb250TWF0Y2hDbGljayIsIlByb3BUeXBlcyIsImJvb2wiLCJzdHJpbmciLCJmdW5jIiwiaXNSZXF1aXJlZCIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckJBOzs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsU0FBU0EsUUFBVCxDQUFrQkMsQ0FBbEIsRUFBcUI7QUFDakIsU0FBT0EsQ0FBQyxDQUFDQyxNQUFGLENBQVMsQ0FBVCxFQUFZQyxXQUFaLEtBQTRCRixDQUFDLENBQUNHLEtBQUYsQ0FBUSxDQUFSLENBQW5DO0FBQ0g7O0FBRWMsTUFBTUMsbUJBQU4sU0FBa0NDLGVBQU1DLFNBQXhDLENBQWtEO0FBVzdEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSx3REFRSixNQUFNO0FBQ2pCLFdBQUtDLFFBQUwsQ0FBYztBQUFFQyxRQUFBQSxPQUFPLEVBQUU7QUFBWCxPQUFkO0FBQ0EsV0FBS0YsS0FBTCxDQUFXRyxNQUFYO0FBQ0gsS0FYa0I7QUFBQSw0REFhQSxNQUFNO0FBQ3JCLFdBQUtGLFFBQUwsQ0FBYztBQUFFRyxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQUFkO0FBQ0EsV0FBS0osS0FBTCxDQUFXSyxRQUFYO0FBQ0gsS0FoQmtCO0FBR2YsU0FBS0MsS0FBTCxHQUFhO0FBQ1RKLE1BQUFBLE9BQU8sRUFBRTtBQURBLEtBQWI7QUFHSDs7QUFZREssRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSUMsVUFBSjtBQUNBLFFBQUlDLFVBQUo7O0FBQ0EsUUFBSSxLQUFLVCxLQUFMLENBQVdVLEdBQVgsQ0FBZUMsS0FBbkIsRUFBMEI7QUFDdEIsWUFBTUMsV0FBVyxHQUFHLEtBQUtaLEtBQUwsQ0FBV1UsR0FBWCxDQUFlQyxLQUFmLENBQXFCRSxHQUFyQixDQUNoQixDQUFDRixLQUFELEVBQVFHLENBQVIsS0FBYztBQUFLLFFBQUEsU0FBUyxFQUFDLHVDQUFmO0FBQXVELFFBQUEsR0FBRyxFQUFFQTtBQUE1RCxTQUNWO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNSCxLQUFLLENBQUMsQ0FBRCxDQURYLENBRFUsRUFJVjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSyx5QkFBR3BCLFFBQVEsQ0FBQ29CLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBWCxDQURMLENBSlUsQ0FERSxDQUFwQjtBQVVBSCxNQUFBQSxVQUFVLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1JJLFdBQVcsQ0FBQ2pCLEtBQVosQ0FBa0IsQ0FBbEIsRUFBcUIsQ0FBckIsQ0FEUSxFQUVUO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixRQUZTLEVBR1JpQixXQUFXLENBQUNqQixLQUFaLENBQWtCLENBQWxCLENBSFEsQ0FBYjtBQUtBYyxNQUFBQSxVQUFVLEdBQUcsS0FBS1QsS0FBTCxDQUFXZSxNQUFYLEdBQ1QseUJBQ0ksNEVBREosQ0FEUyxHQUlULHlCQUNJLDRFQURKLENBSko7QUFPSCxLQXZCRCxNQXVCTyxJQUFJLEtBQUtmLEtBQUwsQ0FBV1UsR0FBWCxDQUFlTSxPQUFuQixFQUE0QjtBQUMvQixZQUFNQyxZQUFZLEdBQUcsS0FBS2pCLEtBQUwsQ0FBV1UsR0FBWCxDQUFlTSxPQUFmLENBQXVCSCxHQUF2QixDQUEyQixDQUFDSyxHQUFELEVBQU1KLENBQU4sS0FBWTtBQUFNLFFBQUEsR0FBRyxFQUFFQTtBQUFYLFNBQ3ZESSxHQUR1RCxDQUF2QyxDQUFyQjtBQUdBVixNQUFBQSxVQUFVLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1JTLFlBRFEsQ0FBYjtBQUdBUixNQUFBQSxVQUFVLEdBQUcsS0FBS1QsS0FBTCxDQUFXZSxNQUFYLEdBQ1QseUJBQ0ksK0VBREosQ0FEUyxHQUlULHlCQUNJLDhFQURKLENBSko7QUFPSCxLQWRNLE1BY0E7QUFDSCxhQUFPLDBDQUNGLHlCQUFHLGlEQUFILENBREUsRUFFSCw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxRQUFBLE9BQU8sRUFBRSxLQUFLZixLQUFMLENBQVdLLFFBQXJEO0FBQStELFFBQUEsU0FBUyxFQUFDO0FBQXpFLFNBQ0sseUJBQUcsUUFBSCxDQURMLENBRkcsQ0FBUDtBQU1IOztBQUVELFFBQUljLE9BQUo7O0FBQ0EsUUFBSSxLQUFLYixLQUFMLENBQVdKLE9BQVgsSUFBc0IsS0FBS0ksS0FBTCxDQUFXRixVQUFyQyxFQUFpRDtBQUM3QyxVQUFJZ0IsSUFBSjs7QUFDQSxVQUFJLEtBQUtkLEtBQUwsQ0FBV0osT0FBZixFQUF3QjtBQUNwQixZQUFJLEtBQUtGLEtBQUwsQ0FBV2UsTUFBZixFQUF1QjtBQUNuQkssVUFBQUEsSUFBSSxHQUFHLHlCQUFHLDJFQUFILEVBQWdGO0FBQ25GQyxZQUFBQSxVQUFVLEVBQUUsS0FBS3JCLEtBQUwsQ0FBV3NCLE1BQVgsQ0FBa0JDLGNBQWxCLEVBRHVFO0FBRW5GQyxZQUFBQSxRQUFRLEVBQUUsS0FBS3hCLEtBQUwsQ0FBV3NCLE1BQVgsQ0FBa0JFO0FBRnVELFdBQWhGLENBQVA7QUFJSCxTQUxELE1BS087QUFDSCxnQkFBTTtBQUFDQyxZQUFBQTtBQUFELGNBQWdCLEtBQUt6QixLQUEzQjtBQUNBb0IsVUFBQUEsSUFBSSxHQUFHLHlCQUFHLHdDQUFILEVBQTZDO0FBQUNLLFlBQUFBO0FBQUQsV0FBN0MsQ0FBUDtBQUNIO0FBQ0osT0FWRCxNQVVPO0FBQ0hMLFFBQUFBLElBQUksR0FBRyx5QkFBRyxhQUFILENBQVA7QUFDSDs7QUFDREQsTUFBQUEsT0FBTyxHQUFHLDZCQUFDLG9DQUFEO0FBQXNCLFFBQUEsSUFBSSxFQUFFQztBQUE1QixRQUFWO0FBQ0gsS0FoQkQsTUFnQk8sSUFBSSxLQUFLcEIsS0FBTCxDQUFXMEIsUUFBZixFQUF5QjtBQUM1QjtBQUNBUCxNQUFBQSxPQUFPLEdBQUcsNkJBQUMsc0JBQUQ7QUFDTixRQUFBLGFBQWEsRUFBRSx5QkFBRyxZQUFILENBRFQ7QUFFTixRQUFBLG9CQUFvQixFQUFFLEtBQUtRLFlBRnJCO0FBR04sUUFBQSxrQkFBa0IsRUFBQyx3QkFIYjtBQUlOLFFBQUEsWUFBWSxFQUFFLHlCQUFHLGtCQUFILENBSlI7QUFLTixRQUFBLFFBQVEsRUFBRSxLQUFLQyxnQkFMVDtBQU1OLFFBQUEsaUJBQWlCLEVBQUM7QUFOWixRQUFWO0FBUUgsS0FWTSxNQVVBO0FBQ0hULE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxjQUFELENBQU8sUUFBUCxRQUNOLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUtRLFlBQWhDO0FBQThDLFFBQUEsSUFBSSxFQUFDO0FBQW5ELFNBQ00seUJBQUcsWUFBSCxDQUROLENBRE0sRUFJTiw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLQyxnQkFBaEM7QUFBa0QsUUFBQSxJQUFJLEVBQUM7QUFBdkQsU0FDTSx5QkFBRyxrQkFBSCxDQUROLENBSk0sQ0FBVjtBQVFIOztBQUVELFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0gsd0NBQUluQixVQUFKLENBREcsRUFFRkQsVUFGRSxFQUdILHdDQUFJLEtBQUtSLEtBQUwsQ0FBV2UsTUFBWCxHQUNBLEVBREEsR0FFQSx5QkFBRyxzRUFBSCxDQUZKLENBSEcsRUFNRkksT0FORSxDQUFQO0FBUUg7O0FBNUg0RCxDLENBK0hqRTs7Ozs4QkEvSHFCdkIsbUIsZUFDRTtBQUNmTSxFQUFBQSxPQUFPLEVBQUUyQixtQkFBVUMsSUFESjtBQUVmTCxFQUFBQSxXQUFXLEVBQUVJLG1CQUFVRSxNQUZSO0FBRWdCO0FBQy9CNUIsRUFBQUEsTUFBTSxFQUFFMEIsbUJBQVVHLElBQVYsQ0FBZUMsVUFIUjtBQUlmNUIsRUFBQUEsUUFBUSxFQUFFd0IsbUJBQVVHLElBQVYsQ0FBZUMsVUFKVjtBQUtmdkIsRUFBQUEsR0FBRyxFQUFFbUIsbUJBQVVLLE1BQVYsQ0FBaUJELFVBTFA7QUFNZmxCLEVBQUFBLE1BQU0sRUFBRWMsbUJBQVVDLElBTkg7QUFPZkosRUFBQUEsUUFBUSxFQUFFRyxtQkFBVUMsSUFQTCxDQU9XOztBQVBYLEM7QUErSHZCLDBCQUFJLEtBQUo7QUFDQSwwQkFBSSxLQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksS0FBSjtBQUNBLDBCQUFJLFVBQUo7QUFDQSwwQkFBSSxRQUFKO0FBQ0EsMEJBQUksT0FBSjtBQUNBLDBCQUFJLFNBQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksUUFBSjtBQUNBLDBCQUFJLE1BQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksV0FBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxNQUFKO0FBQ0EsMEJBQUksUUFBSjtBQUNBLDBCQUFJLFVBQUo7QUFDQSwwQkFBSSxPQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxNQUFKO0FBQ0EsMEJBQUksUUFBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxZQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxNQUFKO0FBQ0EsMEJBQUksT0FBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxPQUFKO0FBQ0EsMEJBQUksS0FBSjtBQUNBLDBCQUFJLFNBQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksT0FBSjtBQUNBLDBCQUFJLFdBQUo7QUFDQSwwQkFBSSxVQUFKO0FBQ0EsMEJBQUksV0FBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxNQUFKO0FBQ0EsMEJBQUksWUFBSjtBQUNBLDBCQUFJLE1BQUo7QUFDQSwwQkFBSSxRQUFKO0FBQ0EsMEJBQUksV0FBSjtBQUNBLDBCQUFJLFVBQUo7QUFDQSwwQkFBSSxNQUFKO0FBQ0EsMEJBQUksS0FBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxXQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLE9BQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksV0FBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxRQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxTQUFKO0FBQ0EsMEJBQUksTUFBSjtBQUNBLDBCQUFJLFFBQUo7QUFDQSwwQkFBSSxZQUFKO0FBQ0EsMEJBQUksUUFBSjtBQUNBLDBCQUFJLEtBQUoiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90LCBfdGQgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge1BlbmRpbmdBY3Rpb25TcGlubmVyfSBmcm9tIFwiLi4vcmlnaHRfcGFuZWwvRW5jcnlwdGlvbkluZm9cIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuaW1wb3J0IERpYWxvZ0J1dHRvbnMgZnJvbSBcIi4uL2VsZW1lbnRzL0RpYWxvZ0J1dHRvbnNcIjtcclxuXHJcbmZ1bmN0aW9uIGNhcEZpcnN0KHMpIHtcclxuICAgIHJldHVybiBzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcy5zbGljZSgxKTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVmVyaWZpY2F0aW9uU2hvd1NhcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHBlbmRpbmc6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyByZXF1aXJlZCBpZiBwZW5kaW5nIGlzIHRydWVcclxuICAgICAgICBvbkRvbmU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25DYW5jZWw6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc2FzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgaXNTZWxmOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBpbkRpYWxvZzogUHJvcFR5cGVzLmJvb2wsIC8vIHdoZXRoZXIgdGhpcyBjb21wb25lbnQgaXMgYmVpbmcgc2hvd24gaW4gYSBkaWFsb2cgYW5kIHRvIHVzZSBEaWFsb2dCdXR0b25zXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwZW5kaW5nOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIG9uTWF0Y2hDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcGVuZGluZzogdHJ1ZSB9KTtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRG9uZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvbkRvbnRNYXRjaENsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjYW5jZWxsaW5nOiB0cnVlIH0pO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25DYW5jZWwoKTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBzYXNEaXNwbGF5O1xyXG4gICAgICAgIGxldCBzYXNDYXB0aW9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNhcy5lbW9qaSkge1xyXG4gICAgICAgICAgICBjb25zdCBlbW9qaUJsb2NrcyA9IHRoaXMucHJvcHMuc2FzLmVtb2ppLm1hcChcclxuICAgICAgICAgICAgICAgIChlbW9qaSwgaSkgPT4gPGRpdiBjbGFzc05hbWU9XCJteF9WZXJpZmljYXRpb25TaG93U2FzX2Vtb2ppU2FzX2Jsb2NrXCIga2V5PXtpfT5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZlcmlmaWNhdGlvblNob3dTYXNfZW1vamlTYXNfZW1vamlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBlbW9qaVswXSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9WZXJpZmljYXRpb25TaG93U2FzX2Vtb2ppU2FzX2xhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChjYXBGaXJzdChlbW9qaVsxXSkpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBzYXNEaXNwbGF5ID0gPGRpdiBjbGFzc05hbWU9XCJteF9WZXJpZmljYXRpb25TaG93U2FzX2Vtb2ppU2FzXCI+XHJcbiAgICAgICAgICAgICAgICB7ZW1vamlCbG9ja3Muc2xpY2UoMCwgNCl9XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZlcmlmaWNhdGlvblNob3dTYXNfZW1vamlTYXNfYnJlYWtcIiAvPlxyXG4gICAgICAgICAgICAgICAge2Vtb2ppQmxvY2tzLnNsaWNlKDQpfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIHNhc0NhcHRpb24gPSB0aGlzLnByb3BzLmlzU2VsZiA/XHJcbiAgICAgICAgICAgICAgICBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkNvbmZpcm0gdGhlIGVtb2ppIGJlbG93IGFyZSBkaXNwbGF5ZWQgb24gYm90aCBzZXNzaW9ucywgaW4gdGhlIHNhbWUgb3JkZXI6XCIsXHJcbiAgICAgICAgICAgICAgICApOlxyXG4gICAgICAgICAgICAgICAgX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJWZXJpZnkgdGhpcyB1c2VyIGJ5IGNvbmZpcm1pbmcgdGhlIGZvbGxvd2luZyBlbW9qaSBhcHBlYXIgb24gdGhlaXIgc2NyZWVuLlwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuc2FzLmRlY2ltYWwpIHtcclxuICAgICAgICAgICAgY29uc3QgbnVtYmVyQmxvY2tzID0gdGhpcy5wcm9wcy5zYXMuZGVjaW1hbC5tYXAoKG51bSwgaSkgPT4gPHNwYW4ga2V5PXtpfT5cclxuICAgICAgICAgICAgICAgIHtudW19XHJcbiAgICAgICAgICAgIDwvc3Bhbj4pO1xyXG4gICAgICAgICAgICBzYXNEaXNwbGF5ID0gPGRpdiBjbGFzc05hbWU9XCJteF9WZXJpZmljYXRpb25TaG93U2FzX2RlY2ltYWxTYXNcIj5cclxuICAgICAgICAgICAgICAgIHtudW1iZXJCbG9ja3N9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgc2FzQ2FwdGlvbiA9IHRoaXMucHJvcHMuaXNTZWxmID9cclxuICAgICAgICAgICAgICAgIF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVmVyaWZ5IHRoaXMgc2Vzc2lvbiBieSBjb25maXJtaW5nIHRoZSBmb2xsb3dpbmcgbnVtYmVyIGFwcGVhcnMgb24gaXRzIHNjcmVlbi5cIixcclxuICAgICAgICAgICAgICAgICk6XHJcbiAgICAgICAgICAgICAgICBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlZlcmlmeSB0aGlzIHVzZXIgYnkgY29uZmlybWluZyB0aGUgZm9sbG93aW5nIG51bWJlciBhcHBlYXJzIG9uIHRoZWlyIHNjcmVlbi5cIixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7X3QoXCJVbmFibGUgdG8gZmluZCBhIHN1cHBvcnRlZCB2ZXJpZmljYXRpb24gbWV0aG9kLlwiKX1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJwcmltYXJ5XCIgb25DbGljaz17dGhpcy5wcm9wcy5vbkNhbmNlbH0gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fd2lkZUJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdCgnQ2FuY2VsJyl9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBjb25maXJtO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBlbmRpbmcgfHwgdGhpcy5zdGF0ZS5jYW5jZWxsaW5nKSB7XHJcbiAgICAgICAgICAgIGxldCB0ZXh0O1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5wZW5kaW5nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5pc1NlbGYpIHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0ID0gX3QoXCJXYWl0aW5nIGZvciB5b3VyIG90aGVyIHNlc3Npb24sICUoZGV2aWNlTmFtZSlzICglKGRldmljZUlkKXMpLCB0byB2ZXJpZnnigKZcIiwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXZpY2VOYW1lOiB0aGlzLnByb3BzLmRldmljZS5nZXREaXNwbGF5TmFtZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXZpY2VJZDogdGhpcy5wcm9wcy5kZXZpY2UuZGV2aWNlSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHtkaXNwbGF5TmFtZX0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQgPSBfdChcIldhaXRpbmcgZm9yICUoZGlzcGxheU5hbWUpcyB0byB2ZXJpZnnigKZcIiwge2Rpc3BsYXlOYW1lfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0ID0gX3QoXCJDYW5jZWxsaW5n4oCmXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbmZpcm0gPSA8UGVuZGluZ0FjdGlvblNwaW5uZXIgdGV4dD17dGV4dH0gLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByb3BzLmluRGlhbG9nKSB7XHJcbiAgICAgICAgICAgIC8vIEZJWE1FOiBzdG9wIHVzaW5nIERpYWxvZ0J1dHRvbnMgaGVyZSBvbmNlIHRoaXMgY29tcG9uZW50IGlzIG9ubHkgdXNlZCBpbiB0aGUgcmlnaHQgcGFuZWwgdmVyaWZpY2F0aW9uXHJcbiAgICAgICAgICAgIGNvbmZpcm0gPSA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoXCJUaGV5IG1hdGNoXCIpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMub25NYXRjaENsaWNrfVxyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbkNsYXNzPVwibXhfVXNlckluZm9fd2lkZUJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b249e190KFwiVGhleSBkb24ndCBtYXRjaFwiKX1cclxuICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLm9uRG9udE1hdGNoQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b25DbGFzcz1cIm14X1VzZXJJbmZvX3dpZGVCdXR0b25cIlxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25maXJtID0gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5vbk1hdGNoQ2xpY2t9IGtpbmQ9XCJwcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIlRoZXkgbWF0Y2hcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5vbkRvbnRNYXRjaENsaWNrfSBraW5kPVwiZGFuZ2VyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIlRoZXkgZG9uJ3QgbWF0Y2hcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZlcmlmaWNhdGlvblNob3dTYXNcIj5cclxuICAgICAgICAgICAgPHA+e3Nhc0NhcHRpb259PC9wPlxyXG4gICAgICAgICAgICB7c2FzRGlzcGxheX1cclxuICAgICAgICAgICAgPHA+e3RoaXMucHJvcHMuaXNTZWxmID9cclxuICAgICAgICAgICAgICAgIFwiXCI6XHJcbiAgICAgICAgICAgICAgICBfdChcIlRvIGJlIHNlY3VyZSwgZG8gdGhpcyBpbiBwZXJzb24gb3IgdXNlIGEgdHJ1c3RlZCB3YXkgdG8gY29tbXVuaWNhdGUuXCIpfTwvcD5cclxuICAgICAgICAgICAge2NvbmZpcm19XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBMaXN0IG9mIEVtb2ppIHN0cmluZ3MgZnJvbSB0aGUganMtc2RrLCBmb3IgaTE4blxyXG5fdGQoXCJEb2dcIik7XHJcbl90ZChcIkNhdFwiKTtcclxuX3RkKFwiTGlvblwiKTtcclxuX3RkKFwiSG9yc2VcIik7XHJcbl90ZChcIlVuaWNvcm5cIik7XHJcbl90ZChcIlBpZ1wiKTtcclxuX3RkKFwiRWxlcGhhbnRcIik7XHJcbl90ZChcIlJhYmJpdFwiKTtcclxuX3RkKFwiUGFuZGFcIik7XHJcbl90ZChcIlJvb3N0ZXJcIik7XHJcbl90ZChcIlBlbmd1aW5cIik7XHJcbl90ZChcIlR1cnRsZVwiKTtcclxuX3RkKFwiRmlzaFwiKTtcclxuX3RkKFwiT2N0b3B1c1wiKTtcclxuX3RkKFwiQnV0dGVyZmx5XCIpO1xyXG5fdGQoXCJGbG93ZXJcIik7XHJcbl90ZChcIlRyZWVcIik7XHJcbl90ZChcIkNhY3R1c1wiKTtcclxuX3RkKFwiTXVzaHJvb21cIik7XHJcbl90ZChcIkdsb2JlXCIpO1xyXG5fdGQoXCJNb29uXCIpO1xyXG5fdGQoXCJDbG91ZFwiKTtcclxuX3RkKFwiRmlyZVwiKTtcclxuX3RkKFwiQmFuYW5hXCIpO1xyXG5fdGQoXCJBcHBsZVwiKTtcclxuX3RkKFwiU3RyYXdiZXJyeVwiKTtcclxuX3RkKFwiQ29yblwiKTtcclxuX3RkKFwiUGl6emFcIik7XHJcbl90ZChcIkNha2VcIik7XHJcbl90ZChcIkhlYXJ0XCIpO1xyXG5fdGQoXCJTbWlsZXlcIik7XHJcbl90ZChcIlJvYm90XCIpO1xyXG5fdGQoXCJIYXRcIik7XHJcbl90ZChcIkdsYXNzZXNcIik7XHJcbl90ZChcIlNwYW5uZXJcIik7XHJcbl90ZChcIlNhbnRhXCIpO1xyXG5fdGQoXCJUaHVtYnMgdXBcIik7XHJcbl90ZChcIlVtYnJlbGxhXCIpO1xyXG5fdGQoXCJIb3VyZ2xhc3NcIik7XHJcbl90ZChcIkNsb2NrXCIpO1xyXG5fdGQoXCJHaWZ0XCIpO1xyXG5fdGQoXCJMaWdodCBidWxiXCIpO1xyXG5fdGQoXCJCb29rXCIpO1xyXG5fdGQoXCJQZW5jaWxcIik7XHJcbl90ZChcIlBhcGVyY2xpcFwiKTtcclxuX3RkKFwiU2Npc3NvcnNcIik7XHJcbl90ZChcIkxvY2tcIik7XHJcbl90ZChcIktleVwiKTtcclxuX3RkKFwiSGFtbWVyXCIpO1xyXG5fdGQoXCJUZWxlcGhvbmVcIik7XHJcbl90ZChcIkZsYWdcIik7XHJcbl90ZChcIlRyYWluXCIpO1xyXG5fdGQoXCJCaWN5Y2xlXCIpO1xyXG5fdGQoXCJBZXJvcGxhbmVcIik7XHJcbl90ZChcIlJvY2tldFwiKTtcclxuX3RkKFwiVHJvcGh5XCIpO1xyXG5fdGQoXCJCYWxsXCIpO1xyXG5fdGQoXCJHdWl0YXJcIik7XHJcbl90ZChcIlRydW1wZXRcIik7XHJcbl90ZChcIkJlbGxcIik7XHJcbl90ZChcIkFuY2hvclwiKTtcclxuX3RkKFwiSGVhZHBob25lc1wiKTtcclxuX3RkKFwiRm9sZGVyXCIpO1xyXG5fdGQoXCJQaW5cIik7XHJcbiJdfQ==