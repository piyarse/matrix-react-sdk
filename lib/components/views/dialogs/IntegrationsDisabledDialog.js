"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class IntegrationsDisabledDialog extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onAcknowledgeClick", () => {
      this.props.onFinished();
    });
    (0, _defineProperty2.default)(this, "_onOpenSettingsClick", () => {
      this.props.onFinished();

      _dispatcher.default.dispatch({
        action: "view_user_settings"
      });
    });
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement(BaseDialog, {
      className: "mx_IntegrationsDisabledDialog",
      hasCancel: true,
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)("Integrations are disabled")
    }, _react.default.createElement("div", {
      className: "mx_IntegrationsDisabledDialog_content"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Enable 'Manage Integrations' in Settings to do this."))), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Settings"),
      onPrimaryButtonClick: this._onOpenSettingsClick,
      cancelButton: (0, _languageHandler._t)("OK"),
      onCancel: this._onAcknowledgeClick
    }));
  }

}

exports.default = IntegrationsDisabledDialog;
(0, _defineProperty2.default)(IntegrationsDisabledDialog, "propTypes", {
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvSW50ZWdyYXRpb25zRGlzYWJsZWREaWFsb2cuanMiXSwibmFtZXMiOlsiSW50ZWdyYXRpb25zRGlzYWJsZWREaWFsb2ciLCJSZWFjdCIsIkNvbXBvbmVudCIsInByb3BzIiwib25GaW5pc2hlZCIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIkRpYWxvZ0J1dHRvbnMiLCJfb25PcGVuU2V0dGluZ3NDbGljayIsIl9vbkFja25vd2xlZGdlQ2xpY2siLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFwQkE7Ozs7Ozs7Ozs7Ozs7OztBQXNCZSxNQUFNQSwwQkFBTixTQUF5Q0MsZUFBTUMsU0FBL0MsQ0FBeUQ7QUFBQTtBQUFBO0FBQUEsK0RBSzlDLE1BQU07QUFDeEIsV0FBS0MsS0FBTCxDQUFXQyxVQUFYO0FBQ0gsS0FQbUU7QUFBQSxnRUFTN0MsTUFBTTtBQUN6QixXQUFLRCxLQUFMLENBQVdDLFVBQVg7O0FBQ0FDLDBCQUFJQyxRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjtBQUNILEtBWm1FO0FBQUE7O0FBY3BFQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7QUFDQSxVQUFNQyxhQUFhLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFFQSxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQywrQkFBdEI7QUFBc0QsTUFBQSxTQUFTLEVBQUUsSUFBakU7QUFDWSxNQUFBLFVBQVUsRUFBRSxLQUFLUixLQUFMLENBQVdDLFVBRG5DO0FBRVksTUFBQSxLQUFLLEVBQUUseUJBQUcsMkJBQUg7QUFGbkIsT0FHSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx3Q0FBSSx5QkFBRyxzREFBSCxDQUFKLENBREosQ0FISixFQU1JLDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxVQUFILENBRG5CO0FBRUksTUFBQSxvQkFBb0IsRUFBRSxLQUFLUyxvQkFGL0I7QUFHSSxNQUFBLFlBQVksRUFBRSx5QkFBRyxJQUFILENBSGxCO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFKbkIsTUFOSixDQURKO0FBZUg7O0FBakNtRTs7OzhCQUFuRGQsMEIsZUFDRTtBQUNmSSxFQUFBQSxVQUFVLEVBQUVXLG1CQUFVQyxJQUFWLENBQWVDO0FBRFosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbnRlZ3JhdGlvbnNEaXNhYmxlZERpYWxvZyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkFja25vd2xlZGdlQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbk9wZW5TZXR0aW5nc0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiBcInZpZXdfdXNlcl9zZXR0aW5nc1wifSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZGlhbG9ncy5CYXNlRGlhbG9nJyk7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPSdteF9JbnRlZ3JhdGlvbnNEaXNhYmxlZERpYWxvZycgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIkludGVncmF0aW9ucyBhcmUgZGlzYWJsZWRcIil9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludGVncmF0aW9uc0Rpc2FibGVkRGlhbG9nX2NvbnRlbnQnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdChcIkVuYWJsZSAnTWFuYWdlIEludGVncmF0aW9ucycgaW4gU2V0dGluZ3MgdG8gZG8gdGhpcy5cIil9PC9wPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e190KFwiU2V0dGluZ3NcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uT3BlblNldHRpbmdzQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uPXtfdChcIk9LXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLl9vbkFja25vd2xlZGdlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=