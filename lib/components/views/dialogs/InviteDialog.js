"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.KIND_INVITE = exports.KIND_DM = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _Permalinks = require("../../../utils/permalinks/Permalinks");

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var _matrix = require("matrix-js-sdk/src/matrix");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _contentRepo = require("matrix-js-sdk/src/content-repo");

var Email = _interopRequireWildcard(require("../../../email"));

var _IdentityServerUtils = require("../../../utils/IdentityServerUtils");

var _UrlUtils = require("../../../utils/UrlUtils");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _IdentityAuthClient = _interopRequireDefault(require("../../../IdentityAuthClient"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _humanize = require("../../../utils/humanize");

var _createRoom = _interopRequireWildcard(require("../../../createRoom"));

var _RoomInvite = require("../../../RoomInvite");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _RoomListStore = _interopRequireWildcard(require("../../../stores/RoomListStore"));

var _Keyboard = require("../../../Keyboard");

/*
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const KIND_DM = "dm";
exports.KIND_DM = KIND_DM;
const KIND_INVITE = "invite";
exports.KIND_INVITE = KIND_INVITE;
const INITIAL_ROOMS_SHOWN = 3; // Number of rooms to show at first

const INCREMENT_ROOMS_SHOWN = 5; // Number of rooms to add when 'show more' is clicked
// This is the interface that is expected by various components in this file. It is a bit
// awkward because it also matches the RoomMember class from the js-sdk with some extra support
// for 3PIDs/email addresses.
//
// XXX: We should use TypeScript interfaces instead of this weird "abstract" class.

class Member {
  /**
   * The display name of this Member. For users this should be their profile's display
   * name or user ID if none set. For 3PIDs this should be the 3PID address (email).
   */
  get name()
  /*: string*/
  {
    throw new Error("Member class not implemented");
  }
  /**
   * The ID of this Member. For users this should be their user ID. For 3PIDs this should
   * be the 3PID address (email).
   */


  get userId()
  /*: string*/
  {
    throw new Error("Member class not implemented");
  }
  /**
   * Gets the MXC URL of this Member's avatar. For users this should be their profile's
   * avatar MXC URL or null if none set. For 3PIDs this should always be null.
   */


  getMxcAvatarUrl()
  /*: string*/
  {
    throw new Error("Member class not implemented");
  }

}

class DirectoryMember extends Member {
  constructor(userDirResult
  /*: {user_id: string, display_name: string, avatar_url: string}*/
  ) {
    super();
    (0, _defineProperty2.default)(this, "_userId", void 0);
    (0, _defineProperty2.default)(this, "_displayName", void 0);
    (0, _defineProperty2.default)(this, "_avatarUrl", void 0);
    this._userId = userDirResult.user_id;
    this._displayName = userDirResult.display_name;
    this._avatarUrl = userDirResult.avatar_url;
  } // These next class members are for the Member interface


  get name()
  /*: string*/
  {
    return this._displayName || this._userId;
  }

  get userId()
  /*: string*/
  {
    return this._userId;
  }

  getMxcAvatarUrl()
  /*: string*/
  {
    return this._avatarUrl;
  }

}

class ThreepidMember extends Member {
  constructor(id
  /*: string*/
  ) {
    super();
    (0, _defineProperty2.default)(this, "_id", void 0);
    this._id = id;
  } // This is a getter that would be falsey on all other implementations. Until we have
  // better type support in the react-sdk we can use this trick to determine the kind
  // of 3PID we're dealing with, if any.


  get isEmail()
  /*: boolean*/
  {
    return this._id.includes('@');
  } // These next class members are for the Member interface


  get name()
  /*: string*/
  {
    return this._id;
  }

  get userId()
  /*: string*/
  {
    return this._id;
  }

  getMxcAvatarUrl()
  /*: string*/
  {
    return null;
  }

}

class DMUserTile extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onRemove", e => {
      // Stop the browser from highlighting text
      e.preventDefault();
      e.stopPropagation();
      this.props.onRemove(this.props.member);
    });
  }

  render() {
    const BaseAvatar = sdk.getComponent("views.avatars.BaseAvatar");
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    const avatarSize = 20;
    const avatar = this.props.member.isEmail ? _react.default.createElement("img", {
      className: "mx_InviteDialog_userTile_avatar mx_InviteDialog_userTile_threepidAvatar",
      src: require("../../../../res/img/icon-email-pill-avatar.svg"),
      width: avatarSize,
      height: avatarSize
    }) : _react.default.createElement(BaseAvatar, {
      className: "mx_InviteDialog_userTile_avatar",
      url: (0, _contentRepo.getHttpUriForMxc)(_MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl(), this.props.member.getMxcAvatarUrl(), avatarSize, avatarSize, "crop"),
      name: this.props.member.name,
      idName: this.props.member.userId,
      width: avatarSize,
      height: avatarSize
    });
    let closeButton;

    if (this.props.onRemove) {
      closeButton = _react.default.createElement(AccessibleButton, {
        className: "mx_InviteDialog_userTile_remove",
        onClick: this._onRemove
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/icon-pill-remove.svg"),
        alt: (0, _languageHandler._t)('Remove'),
        width: 8,
        height: 8
      }));
    }

    return _react.default.createElement("span", {
      className: "mx_InviteDialog_userTile"
    }, _react.default.createElement("span", {
      className: "mx_InviteDialog_userTile_pill"
    }, avatar, _react.default.createElement("span", {
      className: "mx_InviteDialog_userTile_name"
    }, this.props.member.name)), closeButton);
  }

}

(0, _defineProperty2.default)(DMUserTile, "propTypes", {
  member: _propTypes.default.object.isRequired,
  // Should be a Member (see interface above)
  onRemove: _propTypes.default.func // takes 1 argument, the member being removed

});

class DMRoomTile extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onClick", e => {
      // Stop the browser from highlighting text
      e.preventDefault();
      e.stopPropagation();
      this.props.onToggle(this.props.member);
    });
  }

  _highlightName(str
  /*: string*/
  ) {
    if (!this.props.highlightWord) return str; // We convert things to lowercase for index searching, but pull substrings from
    // the submitted text to preserve case. Note: we don't need to htmlEntities the
    // string because React will safely encode the text for us.

    const lowerStr = str.toLowerCase();
    const filterStr = this.props.highlightWord.toLowerCase();
    const result = [];
    let i = 0;
    let ii;

    while ((ii = lowerStr.indexOf(filterStr, i)) >= 0) {
      // Push any text we missed (first bit/middle of text)
      if (ii > i) {
        // Push any text we aren't highlighting (middle of text match, or beginning of text)
        result.push(_react.default.createElement("span", {
          key: i + 'begin'
        }, str.substring(i, ii)));
      }

      i = ii; // copy over ii only if we have a match (to preserve i for end-of-text matching)
      // Highlight the word the user entered

      const substr = str.substring(i, filterStr.length + i);
      result.push(_react.default.createElement("span", {
        className: "mx_InviteDialog_roomTile_highlight",
        key: i + 'bold'
      }, substr));
      i += substr.length;
    } // Push any text we missed (end of text)


    if (i < str.length) {
      result.push(_react.default.createElement("span", {
        key: i + 'end'
      }, str.substring(i)));
    }

    return result;
  }

  render() {
    const BaseAvatar = sdk.getComponent("views.avatars.BaseAvatar");
    let timestamp = null;

    if (this.props.lastActiveTs) {
      const humanTs = (0, _humanize.humanizeTime)(this.props.lastActiveTs);
      timestamp = _react.default.createElement("span", {
        className: "mx_InviteDialog_roomTile_time"
      }, humanTs);
    }

    const avatarSize = 36;
    const avatar = this.props.member.isEmail ? _react.default.createElement("img", {
      src: require("../../../../res/img/icon-email-pill-avatar.svg"),
      width: avatarSize,
      height: avatarSize
    }) : _react.default.createElement(BaseAvatar, {
      url: (0, _contentRepo.getHttpUriForMxc)(_MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl(), this.props.member.getMxcAvatarUrl(), avatarSize, avatarSize, "crop"),
      name: this.props.member.name,
      idName: this.props.member.userId,
      width: avatarSize,
      height: avatarSize
    });
    let checkmark = null;

    if (this.props.isSelected) {
      // To reduce flickering we put the 'selected' room tile above the real avatar
      checkmark = _react.default.createElement("div", {
        className: "mx_InviteDialog_roomTile_selected"
      });
    } // To reduce flickering we put the checkmark on top of the actual avatar (prevents
    // the browser from reloading the image source when the avatar remounts).


    const stackedAvatar = _react.default.createElement("span", {
      className: "mx_InviteDialog_roomTile_avatarStack"
    }, avatar, checkmark);

    return _react.default.createElement("div", {
      className: "mx_InviteDialog_roomTile",
      onClick: this._onClick
    }, stackedAvatar, _react.default.createElement("span", {
      className: "mx_InviteDialog_roomTile_name"
    }, this._highlightName(this.props.member.name)), _react.default.createElement("span", {
      className: "mx_InviteDialog_roomTile_userId"
    }, this._highlightName(this.props.member.userId)), timestamp);
  }

}

(0, _defineProperty2.default)(DMRoomTile, "propTypes", {
  member: _propTypes.default.object.isRequired,
  // Should be a Member (see interface above)
  lastActiveTs: _propTypes.default.number,
  onToggle: _propTypes.default.func.isRequired,
  // takes 1 argument, the member being toggled
  highlightWord: _propTypes.default.string,
  isSelected: _propTypes.default.bool
});

class InviteDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_debounceTimer", null);
    (0, _defineProperty2.default)(this, "_editorRef", null);
    (0, _defineProperty2.default)(this, "_startDm", async () => {
      this.setState({
        busy: true
      });

      const targets = this._convertFilter();

      const targetIds = targets.map(t => t.userId); // Check if there is already a DM with these people and reuse it if possible.

      const existingRoom = _DMRoomMap.default.shared().getDMRoomForIdentifiers(targetIds);

      if (existingRoom) {
        _dispatcher.default.dispatch({
          action: 'view_room',
          room_id: existingRoom.roomId,
          should_peek: false,
          joining: false
        });

        this.props.onFinished();
        return;
      }

      const createRoomOptions = {
        inlineErrors: true
      };

      if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
        // Check whether all users have uploaded device keys before.
        // If so, enable encryption in the new room.
        const client = _MatrixClientPeg.MatrixClientPeg.get();

        const allHaveDeviceKeys = await (0, _createRoom.canEncryptToAllUsers)(client, targetIds);

        if (allHaveDeviceKeys) {
          createRoomOptions.encryption = true;
        }
      } // Check if it's a traditional DM and create the room if required.
      // TODO: [Canonical DMs] Remove this check and instead just create the multi-person DM


      let createRoomPromise = Promise.resolve();

      const isSelf = targetIds.length === 1 && targetIds[0] === _MatrixClientPeg.MatrixClientPeg.get().getUserId();

      if (targetIds.length === 1 && !isSelf) {
        createRoomOptions.dmUserId = targetIds[0];
        createRoomPromise = (0, _createRoom.default)(createRoomOptions);
      } else if (isSelf) {
        createRoomPromise = (0, _createRoom.default)(createRoomOptions);
      } else {
        // Create a boring room and try to invite the targets manually.
        createRoomPromise = (0, _createRoom.default)(createRoomOptions).then(roomId => {
          return (0, _RoomInvite.inviteMultipleToRoom)(roomId, targetIds);
        }).then(result => {
          if (this._shouldAbortAfterInviteError(result)) {
            return true; // abort
          }
        });
      } // the createRoom call will show the room for us, so we don't need to worry about that.


      createRoomPromise.then(abort => {
        if (abort === true) return; // only abort on true booleans, not roomIds or something

        this.props.onFinished();
      }).catch(err => {
        console.error(err);
        this.setState({
          busy: false,
          errorText: (0, _languageHandler._t)("We couldn't create your DM. Please check the users you want to invite and try again.")
        });
      });
    });
    (0, _defineProperty2.default)(this, "_inviteUsers", () => {
      this.setState({
        busy: true
      });

      this._convertFilter();

      const targets = this._convertFilter();

      const targetIds = targets.map(t => t.userId);

      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.roomId);

      if (!room) {
        console.error("Failed to find the room to invite users to");
        this.setState({
          busy: false,
          errorText: (0, _languageHandler._t)("Something went wrong trying to invite the users.")
        });
        return;
      }

      (0, _RoomInvite.inviteMultipleToRoom)(this.props.roomId, targetIds).then(result => {
        if (!this._shouldAbortAfterInviteError(result)) {
          // handles setting error message too
          this.props.onFinished();
        }
      }).catch(err => {
        console.error(err);
        this.setState({
          busy: false,
          errorText: (0, _languageHandler._t)("We couldn't invite those users. Please check the users you want to invite and try again.")
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onKeyDown", e => {
      // when the field is empty and the user hits backspace remove the right-most target
      if (!e.target.value && !this.state.busy && this.state.targets.length > 0 && e.key === _Keyboard.Key.BACKSPACE && !e.ctrlKey && !e.shiftKey && !e.metaKey) {
        e.preventDefault();

        this._removeMember(this.state.targets[this.state.targets.length - 1]);
      }
    });
    (0, _defineProperty2.default)(this, "_updateFilter", e => {
      const term = e.target.value;
      this.setState({
        filterText: term
      }); // Debounce server lookups to reduce spam. We don't clear the existing server
      // results because they might still be vaguely accurate, likewise for races which
      // could happen here.

      if (this._debounceTimer) {
        clearTimeout(this._debounceTimer);
      }

      this._debounceTimer = setTimeout(async () => {
        _MatrixClientPeg.MatrixClientPeg.get().searchUserDirectory({
          term
        }).then(async r => {
          if (term !== this.state.filterText) {
            // Discard the results - we were probably too slow on the server-side to make
            // these results useful. This is a race we want to avoid because we could overwrite
            // more accurate results.
            return;
          }

          if (!r.results) r.results = []; // While we're here, try and autocomplete a search result for the mxid itself
          // if there's no matches (and the input looks like a mxid).

          if (term[0] === '@' && term.indexOf(':') > 1) {
            try {
              const profile = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(term);

              if (profile) {
                // If we have a profile, we have enough information to assume that
                // the mxid can be invited - add it to the list. We stick it at the
                // top so it is most obviously presented to the user.
                r.results.splice(0, 0, {
                  user_id: term,
                  display_name: profile['displayname'],
                  avatar_url: profile['avatar_url']
                });
              }
            } catch (e) {
              console.warn("Non-fatal error trying to make an invite for a user ID");
              console.warn(e); // Add a result anyways, just without a profile. We stick it at the
              // top so it is most obviously presented to the user.

              r.results.splice(0, 0, {
                user_id: term,
                display_name: term,
                avatar_url: null
              });
            }
          }

          this.setState({
            serverResultsMixin: r.results.map(u => ({
              userId: u.user_id,
              user: new DirectoryMember(u)
            }))
          });
        }).catch(e => {
          console.error("Error searching user directory:");
          console.error(e);
          this.setState({
            serverResultsMixin: []
          }); // clear results because it's moderately fatal
        }); // Whenever we search the directory, also try to search the identity server. It's
        // all debounced the same anyways.


        if (!this.state.canUseIdentityServer) {
          // The user doesn't have an identity server set - warn them of that.
          this.setState({
            tryingIdentityServer: true
          });
          return;
        }

        if (term.indexOf('@') > 0 && Email.looksValid(term)) {
          // Start off by suggesting the plain email while we try and resolve it
          // to a real account.
          this.setState({
            // per above: the userId is a lie here - it's just a regular identifier
            threepidResultsMixin: [{
              user: new ThreepidMember(term),
              userId: term
            }]
          });

          try {
            const authClient = new _IdentityAuthClient.default();
            const token = await authClient.getAccessToken();
            if (term !== this.state.filterText) return; // abandon hope

            const lookup = await _MatrixClientPeg.MatrixClientPeg.get().lookupThreePid('email', term, undefined, // callback
            token);
            if (term !== this.state.filterText) return; // abandon hope

            if (!lookup || !lookup.mxid) {
              // We weren't able to find anyone - we're already suggesting the plain email
              // as an alternative, so do nothing.
              return;
            } // We append the user suggestion to give the user an option to click
            // the email anyways, and so we don't cause things to jump around. In
            // theory, the user would see the user pop up and think "ah yes, that
            // person!"


            const profile = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(lookup.mxid);
            if (term !== this.state.filterText || !profile) return; // abandon hope

            this.setState({
              threepidResultsMixin: [...this.state.threepidResultsMixin, {
                user: new DirectoryMember({
                  user_id: lookup.mxid,
                  display_name: profile.displayname,
                  avatar_url: profile.avatar_url
                }),
                userId: lookup.mxid
              }]
            });
          } catch (e) {
            console.error("Error searching identity server:");
            console.error(e);
            this.setState({
              threepidResultsMixin: []
            }); // clear results because it's moderately fatal
          }
        }
      }, 150); // 150ms debounce (human reaction time + some)
    });
    (0, _defineProperty2.default)(this, "_showMoreRecents", () => {
      this.setState({
        numRecentsShown: this.state.numRecentsShown + INCREMENT_ROOMS_SHOWN
      });
    });
    (0, _defineProperty2.default)(this, "_showMoreSuggestions", () => {
      this.setState({
        numSuggestionsShown: this.state.numSuggestionsShown + INCREMENT_ROOMS_SHOWN
      });
    });
    (0, _defineProperty2.default)(this, "_toggleMember", (member
    /*: Member*/
    ) => {
      let filterText = this.state.filterText;
      const targets = this.state.targets.map(t => t); // cheap clone for mutation

      const idx = targets.indexOf(member);

      if (idx >= 0) {
        targets.splice(idx, 1);
      } else {
        targets.push(member);
        filterText = ""; // clear the filter when the user accepts a suggestion
      }

      this.setState({
        targets,
        filterText
      });
    });
    (0, _defineProperty2.default)(this, "_removeMember", (member
    /*: Member*/
    ) => {
      const targets = this.state.targets.map(t => t); // cheap clone for mutation

      const idx = targets.indexOf(member);

      if (idx >= 0) {
        targets.splice(idx, 1);
        this.setState({
          targets
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onPaste", async e => {
      if (this.state.filterText) {
        // if the user has already typed something, just let them
        // paste normally.
        return;
      } // Prevent the text being pasted into the textarea


      e.preventDefault(); // Process it as a list of addresses to add instead

      const text = e.clipboardData.getData("text");
      const possibleMembers = [// If we can avoid hitting the profile endpoint, we should.
      ...this.state.recents, ...this.state.suggestions, ...this.state.serverResultsMixin, ...this.state.threepidResultsMixin];
      const toAdd = [];
      const failed = [];
      const potentialAddresses = text.split(/[\s,]+/).map(p => p.trim()).filter(p => !!p); // filter empty strings

      for (const address of potentialAddresses) {
        const member = possibleMembers.find(m => m.userId === address);

        if (member) {
          toAdd.push(member.user);
          continue;
        }

        if (address.indexOf('@') > 0 && Email.looksValid(address)) {
          toAdd.push(new ThreepidMember(address));
          continue;
        }

        if (address[0] !== '@') {
          failed.push(address); // not a user ID

          continue;
        }

        try {
          const profile = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(address);
          const displayName = profile ? profile.displayname : null;
          const avatarUrl = profile ? profile.avatar_url : null;
          toAdd.push(new DirectoryMember({
            user_id: address,
            display_name: displayName,
            avatar_url: avatarUrl
          }));
        } catch (e) {
          console.error("Error looking up profile for " + address);
          console.error(e);
          failed.push(address);
        }
      }

      if (failed.length > 0) {
        const QuestionDialog = sdk.getComponent('dialogs.QuestionDialog');

        _Modal.default.createTrackedDialog('Invite Paste Fail', '', QuestionDialog, {
          title: (0, _languageHandler._t)('Failed to find the following users'),
          description: (0, _languageHandler._t)("The following users might not exist or are invalid, and cannot be invited: %(csvNames)s", {
            csvNames: failed.join(", ")
          }),
          button: (0, _languageHandler._t)('OK')
        });
      }

      this.setState({
        targets: [...this.state.targets, ...toAdd]
      });
    });
    (0, _defineProperty2.default)(this, "_onClickInputArea", e => {
      // Stop the browser from highlighting text
      e.preventDefault();
      e.stopPropagation();

      if (this._editorRef && this._editorRef.current) {
        this._editorRef.current.focus();
      }
    });
    (0, _defineProperty2.default)(this, "_onUseDefaultIdentityServerClick", e => {
      e.preventDefault(); // Update the IS in account data. Actually using it may trigger terms.
      // eslint-disable-next-line react-hooks/rules-of-hooks

      (0, _IdentityServerUtils.useDefaultIdentityServer)();
      this.setState({
        canUseIdentityServer: true,
        tryingIdentityServer: false
      });
    });
    (0, _defineProperty2.default)(this, "_onManageSettingsClick", e => {
      e.preventDefault();

      _dispatcher.default.dispatch({
        action: 'view_user_settings'
      });

      this.props.onFinished();
    });

    if (props.kind === KIND_INVITE && !props.roomId) {
      throw new Error("When using KIND_INVITE a roomId is required for an InviteDialog");
    }

    const alreadyInvited = new Set([_MatrixClientPeg.MatrixClientPeg.get().getUserId(), _SdkConfig.default.get()['welcomeUserId']]);

    if (props.roomId) {
      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(props.roomId);

      if (!room) throw new Error("Room ID given to InviteDialog does not look like a room");
      room.getMembersWithMembership('invite').forEach(m => alreadyInvited.add(m.userId));
      room.getMembersWithMembership('join').forEach(m => alreadyInvited.add(m.userId)); // add banned users, so we don't try to invite them

      room.getMembersWithMembership('ban').forEach(m => alreadyInvited.add(m.userId));
    }

    this.state = {
      targets: [],
      // array of Member objects (see interface above)
      filterText: "",
      recents: this._buildRecents(alreadyInvited),
      numRecentsShown: INITIAL_ROOMS_SHOWN,
      suggestions: this._buildSuggestions(alreadyInvited),
      numSuggestionsShown: INITIAL_ROOMS_SHOWN,
      serverResultsMixin: [],
      // { user: DirectoryMember, userId: string }[], like recents and suggestions
      threepidResultsMixin: [],
      // { user: ThreepidMember, userId: string}[], like recents and suggestions
      canUseIdentityServer: !!_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl(),
      tryingIdentityServer: false,
      // These two flags are used for the 'Go' button to communicate what is going on.
      busy: false,
      errorText: null
    };
    this._editorRef = (0, _react.createRef)();
  }

  _buildRecents(excludedTargetIds
  /*: Set<string>*/
  )
  /*: {userId: string, user: RoomMember, lastActive: number}*/
  {
    const rooms = _DMRoomMap.default.shared().getUniqueRoomsWithIndividuals(); // map of userId => js-sdk Room
    // Also pull in all the rooms tagged as TAG_DM so we don't miss anything. Sometimes the
    // room list doesn't tag the room for the DMRoomMap, but does for the room list.


    const taggedRooms = _RoomListStore.default.getRoomLists();

    const dmTaggedRooms = taggedRooms[_RoomListStore.TAG_DM];

    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

    for (const dmRoom of dmTaggedRooms) {
      const otherMembers = dmRoom.getJoinedMembers().filter(u => u.userId !== myUserId);

      for (const member of otherMembers) {
        if (rooms[member.userId]) continue; // already have a room

        console.warn("Adding DM room for ".concat(member.userId, " as ").concat(dmRoom.roomId, " from tag, not DM map"));
        rooms[member.userId] = dmRoom;
      }
    }

    const recents = [];

    for (const userId in rooms) {
      // Filter out user IDs that are already in the room / should be excluded
      if (excludedTargetIds.has(userId)) {
        console.warn("[Invite:Recents] Excluding ".concat(userId, " from recents"));
        continue;
      }

      const room = rooms[userId];
      const member = room.getMember(userId);

      if (!member) {
        // just skip people who don't have memberships for some reason
        console.warn("[Invite:Recents] ".concat(userId, " is missing a member object in their own DM (").concat(room.roomId, ")"));
        continue;
      } // Find the last timestamp for a message event


      const searchTypes = ["m.room.message", "m.room.encrypted", "m.sticker"];
      const maxSearchEvents = 20; // to prevent traversing history

      let lastEventTs = 0;

      if (room.timeline && room.timeline.length) {
        for (let i = room.timeline.length - 1; i >= 0; i--) {
          const ev = room.timeline[i];

          if (searchTypes.includes(ev.getType())) {
            lastEventTs = ev.getTs();
            break;
          }

          if (room.timeline.length - i > maxSearchEvents) break;
        }
      }

      if (!lastEventTs) {
        // something weird is going on with this room
        console.warn("[Invite:Recents] ".concat(userId, " (").concat(room.roomId, ") has a weird last timestamp: ").concat(lastEventTs));
        continue;
      }

      recents.push({
        userId,
        user: member,
        lastActive: lastEventTs
      });
    }

    if (!recents) console.warn("[Invite:Recents] No recents to suggest!"); // Sort the recents by last active to save us time later

    recents.sort((a, b) => b.lastActive - a.lastActive);
    return recents;
  }

  _buildSuggestions(excludedTargetIds
  /*: Set<string>*/
  )
  /*: {userId: string, user: RoomMember}*/
  {
    const maxConsideredMembers = 200;

    const joinedRooms = _MatrixClientPeg.MatrixClientPeg.get().getRooms().filter(r => r.getMyMembership() === 'join' && r.getJoinedMemberCount() <= maxConsideredMembers); // Generates { userId: {member, rooms[]} }


    const memberRooms = joinedRooms.reduce((members, room) => {
      // Filter out DMs (we'll handle these in the recents section)
      if (_DMRoomMap.default.shared().getUserIdForRoomId(room.roomId)) {
        return members; // Do nothing
      }

      const joinedMembers = room.getJoinedMembers().filter(u => !excludedTargetIds.has(u.userId));

      for (const member of joinedMembers) {
        // Filter out user IDs that are already in the room / should be excluded
        if (excludedTargetIds.has(member.userId)) {
          continue;
        }

        if (!members[member.userId]) {
          members[member.userId] = {
            member: member,
            // Track the room size of the 'picked' member so we can use the profile of
            // the smallest room (likely a DM).
            pickedMemberRoomSize: room.getJoinedMemberCount(),
            rooms: []
          };
        }

        members[member.userId].rooms.push(room);

        if (room.getJoinedMemberCount() < members[member.userId].pickedMemberRoomSize) {
          members[member.userId].member = member;
          members[member.userId].pickedMemberRoomSize = room.getJoinedMemberCount();
        }
      }

      return members;
    }, {}); // Generates { userId: {member, numRooms, score} }

    const memberScores = Object.values(memberRooms).reduce((scores, entry) => {
      const numMembersTotal = entry.rooms.reduce((c, r) => c + r.getJoinedMemberCount(), 0);
      const maxRange = maxConsideredMembers * entry.rooms.length;
      scores[entry.member.userId] = {
        member: entry.member,
        numRooms: entry.rooms.length,
        score: Math.max(0, Math.pow(1 - numMembersTotal / maxRange, 5))
      };
      return scores;
    }, {}); // Now that we have scores for being in rooms, boost those people who have sent messages
    // recently, as a way to improve the quality of suggestions. We do this by checking every
    // room to see who has sent a message in the last few hours, and giving them a score
    // which correlates to the freshness of their message. In theory, this results in suggestions
    // which are closer to "continue this conversation" rather than "this person exists".

    const trueJoinedRooms = _MatrixClientPeg.MatrixClientPeg.get().getRooms().filter(r => r.getMyMembership() === 'join');

    const now = new Date().getTime();
    const earliestAgeConsidered = now - 60 * 60 * 1000; // 1 hour ago

    const maxMessagesConsidered = 50; // so we don't iterate over a huge amount of traffic

    const lastSpoke = {}; // userId: timestamp

    const lastSpokeMembers = {}; // userId: room member

    for (const room of trueJoinedRooms) {
      // Skip low priority rooms and DMs
      const isDm = _DMRoomMap.default.shared().getUserIdForRoomId(room.roomId);

      if (Object.keys(room.tags).includes("m.lowpriority") || isDm) {
        continue;
      }

      const events = room.getLiveTimeline().getEvents(); // timelines are most recent last

      for (let i = events.length - 1; i >= Math.max(0, events.length - maxMessagesConsidered); i--) {
        const ev = events[i];

        if (excludedTargetIds.has(ev.getSender())) {
          continue;
        }

        if (ev.getTs() <= earliestAgeConsidered) {
          break; // give up: all events from here on out are too old
        }

        if (!lastSpoke[ev.getSender()] || lastSpoke[ev.getSender()] < ev.getTs()) {
          lastSpoke[ev.getSender()] = ev.getTs();
          lastSpokeMembers[ev.getSender()] = room.getMember(ev.getSender());
        }
      }
    }

    for (const userId in lastSpoke) {
      const ts = lastSpoke[userId];
      const member = lastSpokeMembers[userId];
      if (!member) continue; // skip people we somehow don't have profiles for
      // Scores from being in a room give a 'good' score of about 1.0-1.5, so for our
      // boost we'll try and award at least +1.0 for making the list, with +4.0 being
      // an approximate maximum for being selected.

      const distanceFromNow = Math.abs(now - ts); // abs to account for slight future messages

      const inverseTime = now - earliestAgeConsidered - distanceFromNow;
      const scoreBoost = Math.max(1, inverseTime / (15 * 60 * 1000)); // 15min segments to keep scores sane

      let record = memberScores[userId];
      if (!record) record = memberScores[userId] = {
        score: 0
      };
      record.member = member;
      record.score += scoreBoost;
    }

    const members = Object.values(memberScores);
    members.sort((a, b) => {
      if (a.score === b.score) {
        if (a.numRooms === b.numRooms) {
          return a.member.userId.localeCompare(b.member.userId);
        }

        return b.numRooms - a.numRooms;
      }

      return b.score - a.score;
    });
    return members.map(m => ({
      userId: m.member.userId,
      user: m.member
    }));
  }

  _shouldAbortAfterInviteError(result)
  /*: boolean*/
  {
    const failedUsers = Object.keys(result.states).filter(a => result.states[a] === 'error');

    if (failedUsers.length > 0) {
      console.log("Failed to invite users: ", result);
      this.setState({
        busy: false,
        errorText: (0, _languageHandler._t)("Failed to invite the following users to chat: %(csvUsers)s", {
          csvUsers: failedUsers.join(", ")
        })
      });
      return true; // abort
    }

    return false;
  }

  _convertFilter()
  /*: Member[]*/
  {
    // Check to see if there's anything to convert first
    if (!this.state.filterText || !this.state.filterText.includes('@')) return this.state.targets || [];
    let newMember
    /*: Member*/
    ;

    if (this.state.filterText.startsWith('@')) {
      // Assume mxid
      newMember = new DirectoryMember({
        user_id: this.state.filterText,
        display_name: null,
        avatar_url: null
      });
    } else {
      // Assume email
      newMember = new ThreepidMember(this.state.filterText);
    }

    const newTargets = [...(this.state.targets || []), newMember];
    this.setState({
      targets: newTargets,
      filterText: ''
    });
    return newTargets;
  }

  _renderSection(kind
  /*: "recents"|"suggestions"*/
  ) {
    let sourceMembers = kind === 'recents' ? this.state.recents : this.state.suggestions;
    let showNum = kind === 'recents' ? this.state.numRecentsShown : this.state.numSuggestionsShown;
    const showMoreFn = kind === 'recents' ? this._showMoreRecents.bind(this) : this._showMoreSuggestions.bind(this);

    const lastActive = m => kind === 'recents' ? m.lastActive : null;

    let sectionName = kind === 'recents' ? (0, _languageHandler._t)("Recent Conversations") : (0, _languageHandler._t)("Suggestions");

    if (this.props.kind === KIND_INVITE) {
      sectionName = kind === 'recents' ? (0, _languageHandler._t)("Recently Direct Messaged") : (0, _languageHandler._t)("Suggestions");
    } // Mix in the server results if we have any, but only if we're searching. We track the additional
    // members separately because we want to filter sourceMembers but trust the mixin arrays to have
    // the right members in them.


    let priorityAdditionalMembers = []; // Shows up before our own suggestions, higher quality

    let otherAdditionalMembers = []; // Shows up after our own suggestions, lower quality

    const hasMixins = this.state.serverResultsMixin || this.state.threepidResultsMixin;

    if (this.state.filterText && hasMixins && kind === 'suggestions') {
      // We don't want to duplicate members though, so just exclude anyone we've already seen.
      const notAlreadyExists = (u
      /*: Member*/
      ) =>
      /*: boolean*/
      {
        return !sourceMembers.some(m => m.userId === u.userId) && !priorityAdditionalMembers.some(m => m.userId === u.userId) && !otherAdditionalMembers.some(m => m.userId === u.userId);
      };

      otherAdditionalMembers = this.state.serverResultsMixin.filter(notAlreadyExists);
      priorityAdditionalMembers = this.state.threepidResultsMixin.filter(notAlreadyExists);
    }

    const hasAdditionalMembers = priorityAdditionalMembers.length > 0 || otherAdditionalMembers.length > 0; // Hide the section if there's nothing to filter by

    if (sourceMembers.length === 0 && !hasAdditionalMembers) return null; // Do some simple filtering on the input before going much further. If we get no results, say so.

    if (this.state.filterText) {
      const filterBy = this.state.filterText.toLowerCase();
      sourceMembers = sourceMembers.filter(m => m.user.name.toLowerCase().includes(filterBy) || m.userId.toLowerCase().includes(filterBy));

      if (sourceMembers.length === 0 && !hasAdditionalMembers) {
        return _react.default.createElement("div", {
          className: "mx_InviteDialog_section"
        }, _react.default.createElement("h3", null, sectionName), _react.default.createElement("p", null, (0, _languageHandler._t)("No results")));
      }
    } // Now we mix in the additional members. Again, we presume these have already been filtered. We
    // also assume they are more relevant than our suggestions and prepend them to the list.


    sourceMembers = [...priorityAdditionalMembers, ...sourceMembers, ...otherAdditionalMembers]; // If we're going to hide one member behind 'show more', just use up the space of the button
    // with the member's tile instead.

    if (showNum === sourceMembers.length - 1) showNum++; // .slice() will return an incomplete array but won't error on us if we go too far

    const toRender = sourceMembers.slice(0, showNum);
    const hasMore = toRender.length < sourceMembers.length;
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    let showMore = null;

    if (hasMore) {
      showMore = _react.default.createElement(AccessibleButton, {
        onClick: showMoreFn,
        kind: "link"
      }, (0, _languageHandler._t)("Show more"));
    }

    const tiles = toRender.map(r => _react.default.createElement(DMRoomTile, {
      member: r.user,
      lastActiveTs: lastActive(r),
      key: r.userId,
      onToggle: this._toggleMember,
      highlightWord: this.state.filterText,
      isSelected: this.state.targets.some(t => t.userId === r.userId)
    }));
    return _react.default.createElement("div", {
      className: "mx_InviteDialog_section"
    }, _react.default.createElement("h3", null, sectionName), tiles, showMore);
  }

  _renderEditor() {
    const targets = this.state.targets.map(t => _react.default.createElement(DMUserTile, {
      member: t,
      onRemove: !this.state.busy && this._removeMember,
      key: t.userId
    }));

    const input = _react.default.createElement("textarea", {
      rows: 1,
      onKeyDown: this._onKeyDown,
      onChange: this._updateFilter,
      value: this.state.filterText,
      ref: this._editorRef,
      onPaste: this._onPaste,
      autoFocus: true,
      disabled: this.state.busy
    });

    return _react.default.createElement("div", {
      className: "mx_InviteDialog_editor",
      onClick: this._onClickInputArea
    }, targets, input);
  }

  _renderIdentityServerWarning() {
    if (!this.state.tryingIdentityServer || this.state.canUseIdentityServer) {
      return null;
    }

    const defaultIdentityServerUrl = (0, _IdentityServerUtils.getDefaultIdentityServerUrl)();

    if (defaultIdentityServerUrl) {
      return _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_identityServer"
      }, (0, _languageHandler._t)("Use an identity server to invite by email. " + "<default>Use the default (%(defaultIdentityServerName)s)</default> " + "or manage in <settings>Settings</settings>.", {
        defaultIdentityServerName: (0, _UrlUtils.abbreviateUrl)(defaultIdentityServerUrl)
      }, {
        default: sub => _react.default.createElement("a", {
          href: "#",
          onClick: this._onUseDefaultIdentityServerClick
        }, sub),
        settings: sub => _react.default.createElement("a", {
          href: "#",
          onClick: this._onManageSettingsClick
        }, sub)
      }));
    } else {
      return _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_identityServer"
      }, (0, _languageHandler._t)("Use an identity server to invite by email. " + "Manage in <settings>Settings</settings>.", {}, {
        settings: sub => _react.default.createElement("a", {
          href: "#",
          onClick: this._onManageSettingsClick
        }, sub)
      }));
    }
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    const Spinner = sdk.getComponent("elements.Spinner");
    let spinner = null;

    if (this.state.busy) {
      spinner = _react.default.createElement(Spinner, {
        w: 20,
        h: 20
      });
    }

    let title;
    let helpText;
    let buttonText;
    let goButtonFn;

    if (this.props.kind === KIND_DM) {
      const userId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

      title = (0, _languageHandler._t)("Direct Messages");
      helpText = (0, _languageHandler._t)("Start a conversation with someone using their name, username (like <userId/>) or email address.", {}, {
        userId: () => {
          return _react.default.createElement("a", {
            href: (0, _Permalinks.makeUserPermalink)(userId),
            rel: "noreferrer noopener",
            target: "_blank"
          }, userId);
        }
      });
      buttonText = (0, _languageHandler._t)("Go");
      goButtonFn = this._startDm;
    } else {
      // KIND_INVITE
      title = (0, _languageHandler._t)("Invite to this room");
      helpText = (0, _languageHandler._t)("If you can't find someone, ask them for their username (e.g. @user:server.com) or " + "<a>share this room</a>.", {}, {
        a: sub => _react.default.createElement("a", {
          href: (0, _Permalinks.makeRoomPermalink)(this.props.roomId),
          rel: "noreferrer noopener",
          target: "_blank"
        }, sub)
      });
      buttonText = (0, _languageHandler._t)("Invite");
      goButtonFn = this._inviteUsers;
    }

    const hasSelection = this.state.targets.length > 0 || this.state.filterText && this.state.filterText.includes('@');
    return _react.default.createElement(BaseDialog, {
      className: "mx_InviteDialog",
      hasCancel: true,
      onFinished: this.props.onFinished,
      title: title
    }, _react.default.createElement("div", {
      className: "mx_InviteDialog_content"
    }, _react.default.createElement("p", {
      className: "mx_InviteDialog_helpText"
    }, helpText), _react.default.createElement("div", {
      className: "mx_InviteDialog_addressBar"
    }, this._renderEditor(), _react.default.createElement("div", {
      className: "mx_InviteDialog_buttonAndSpinner"
    }, _react.default.createElement(AccessibleButton, {
      kind: "primary",
      onClick: goButtonFn,
      className: "mx_InviteDialog_goButton",
      disabled: this.state.busy || !hasSelection
    }, buttonText), spinner)), this._renderIdentityServerWarning(), _react.default.createElement("div", {
      className: "error"
    }, this.state.errorText), _react.default.createElement("div", {
      className: "mx_InviteDialog_userSections"
    }, this._renderSection('recents'), this._renderSection('suggestions'))));
  }

}

exports.default = InviteDialog;
(0, _defineProperty2.default)(InviteDialog, "propTypes", {
  // Takes an array of user IDs/emails to invite.
  onFinished: _propTypes.default.func.isRequired,
  // The kind of invite being performed. Assumed to be KIND_DM if
  // not provided.
  kind: _propTypes.default.string,
  // The room ID this dialog is for. Only required for KIND_INVITE.
  roomId: _propTypes.default.string
});
(0, _defineProperty2.default)(InviteDialog, "defaultProps", {
  kind: KIND_DM
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvSW52aXRlRGlhbG9nLmpzIl0sIm5hbWVzIjpbIktJTkRfRE0iLCJLSU5EX0lOVklURSIsIklOSVRJQUxfUk9PTVNfU0hPV04iLCJJTkNSRU1FTlRfUk9PTVNfU0hPV04iLCJNZW1iZXIiLCJuYW1lIiwiRXJyb3IiLCJ1c2VySWQiLCJnZXRNeGNBdmF0YXJVcmwiLCJEaXJlY3RvcnlNZW1iZXIiLCJjb25zdHJ1Y3RvciIsInVzZXJEaXJSZXN1bHQiLCJfdXNlcklkIiwidXNlcl9pZCIsIl9kaXNwbGF5TmFtZSIsImRpc3BsYXlfbmFtZSIsIl9hdmF0YXJVcmwiLCJhdmF0YXJfdXJsIiwiVGhyZWVwaWRNZW1iZXIiLCJpZCIsIl9pZCIsImlzRW1haWwiLCJpbmNsdWRlcyIsIkRNVXNlclRpbGUiLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJwcm9wcyIsIm9uUmVtb3ZlIiwibWVtYmVyIiwicmVuZGVyIiwiQmFzZUF2YXRhciIsInNkayIsImdldENvbXBvbmVudCIsIkFjY2Vzc2libGVCdXR0b24iLCJhdmF0YXJTaXplIiwiYXZhdGFyIiwicmVxdWlyZSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldEhvbWVzZXJ2ZXJVcmwiLCJjbG9zZUJ1dHRvbiIsIl9vblJlbW92ZSIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwiRE1Sb29tVGlsZSIsIm9uVG9nZ2xlIiwiX2hpZ2hsaWdodE5hbWUiLCJzdHIiLCJoaWdobGlnaHRXb3JkIiwibG93ZXJTdHIiLCJ0b0xvd2VyQ2FzZSIsImZpbHRlclN0ciIsInJlc3VsdCIsImkiLCJpaSIsImluZGV4T2YiLCJwdXNoIiwic3Vic3RyaW5nIiwic3Vic3RyIiwibGVuZ3RoIiwidGltZXN0YW1wIiwibGFzdEFjdGl2ZVRzIiwiaHVtYW5UcyIsImNoZWNrbWFyayIsImlzU2VsZWN0ZWQiLCJzdGFja2VkQXZhdGFyIiwiX29uQ2xpY2siLCJudW1iZXIiLCJzdHJpbmciLCJib29sIiwiSW52aXRlRGlhbG9nIiwic2V0U3RhdGUiLCJidXN5IiwidGFyZ2V0cyIsIl9jb252ZXJ0RmlsdGVyIiwidGFyZ2V0SWRzIiwibWFwIiwidCIsImV4aXN0aW5nUm9vbSIsIkRNUm9vbU1hcCIsInNoYXJlZCIsImdldERNUm9vbUZvcklkZW50aWZpZXJzIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJyb29tX2lkIiwicm9vbUlkIiwic2hvdWxkX3BlZWsiLCJqb2luaW5nIiwib25GaW5pc2hlZCIsImNyZWF0ZVJvb21PcHRpb25zIiwiaW5saW5lRXJyb3JzIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJjbGllbnQiLCJhbGxIYXZlRGV2aWNlS2V5cyIsImVuY3J5cHRpb24iLCJjcmVhdGVSb29tUHJvbWlzZSIsIlByb21pc2UiLCJyZXNvbHZlIiwiaXNTZWxmIiwiZ2V0VXNlcklkIiwiZG1Vc2VySWQiLCJ0aGVuIiwiX3Nob3VsZEFib3J0QWZ0ZXJJbnZpdGVFcnJvciIsImFib3J0IiwiY2F0Y2giLCJlcnIiLCJjb25zb2xlIiwiZXJyb3IiLCJlcnJvclRleHQiLCJyb29tIiwiZ2V0Um9vbSIsInRhcmdldCIsInZhbHVlIiwic3RhdGUiLCJrZXkiLCJLZXkiLCJCQUNLU1BBQ0UiLCJjdHJsS2V5Iiwic2hpZnRLZXkiLCJtZXRhS2V5IiwiX3JlbW92ZU1lbWJlciIsInRlcm0iLCJmaWx0ZXJUZXh0IiwiX2RlYm91bmNlVGltZXIiLCJjbGVhclRpbWVvdXQiLCJzZXRUaW1lb3V0Iiwic2VhcmNoVXNlckRpcmVjdG9yeSIsInIiLCJyZXN1bHRzIiwicHJvZmlsZSIsImdldFByb2ZpbGVJbmZvIiwic3BsaWNlIiwid2FybiIsInNlcnZlclJlc3VsdHNNaXhpbiIsInUiLCJ1c2VyIiwiY2FuVXNlSWRlbnRpdHlTZXJ2ZXIiLCJ0cnlpbmdJZGVudGl0eVNlcnZlciIsIkVtYWlsIiwibG9va3NWYWxpZCIsInRocmVlcGlkUmVzdWx0c01peGluIiwiYXV0aENsaWVudCIsIklkZW50aXR5QXV0aENsaWVudCIsInRva2VuIiwiZ2V0QWNjZXNzVG9rZW4iLCJsb29rdXAiLCJsb29rdXBUaHJlZVBpZCIsInVuZGVmaW5lZCIsIm14aWQiLCJkaXNwbGF5bmFtZSIsIm51bVJlY2VudHNTaG93biIsIm51bVN1Z2dlc3Rpb25zU2hvd24iLCJpZHgiLCJ0ZXh0IiwiY2xpcGJvYXJkRGF0YSIsImdldERhdGEiLCJwb3NzaWJsZU1lbWJlcnMiLCJyZWNlbnRzIiwic3VnZ2VzdGlvbnMiLCJ0b0FkZCIsImZhaWxlZCIsInBvdGVudGlhbEFkZHJlc3NlcyIsInNwbGl0IiwicCIsInRyaW0iLCJmaWx0ZXIiLCJhZGRyZXNzIiwiZmluZCIsIm0iLCJkaXNwbGF5TmFtZSIsImF2YXRhclVybCIsIlF1ZXN0aW9uRGlhbG9nIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImNzdk5hbWVzIiwiam9pbiIsImJ1dHRvbiIsIl9lZGl0b3JSZWYiLCJjdXJyZW50IiwiZm9jdXMiLCJraW5kIiwiYWxyZWFkeUludml0ZWQiLCJTZXQiLCJTZGtDb25maWciLCJnZXRNZW1iZXJzV2l0aE1lbWJlcnNoaXAiLCJmb3JFYWNoIiwiYWRkIiwiX2J1aWxkUmVjZW50cyIsIl9idWlsZFN1Z2dlc3Rpb25zIiwiZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwiLCJleGNsdWRlZFRhcmdldElkcyIsInJvb21zIiwiZ2V0VW5pcXVlUm9vbXNXaXRoSW5kaXZpZHVhbHMiLCJ0YWdnZWRSb29tcyIsIlJvb21MaXN0U3RvcmUiLCJnZXRSb29tTGlzdHMiLCJkbVRhZ2dlZFJvb21zIiwiVEFHX0RNIiwibXlVc2VySWQiLCJkbVJvb20iLCJvdGhlck1lbWJlcnMiLCJnZXRKb2luZWRNZW1iZXJzIiwiaGFzIiwiZ2V0TWVtYmVyIiwic2VhcmNoVHlwZXMiLCJtYXhTZWFyY2hFdmVudHMiLCJsYXN0RXZlbnRUcyIsInRpbWVsaW5lIiwiZXYiLCJnZXRUeXBlIiwiZ2V0VHMiLCJsYXN0QWN0aXZlIiwic29ydCIsImEiLCJiIiwibWF4Q29uc2lkZXJlZE1lbWJlcnMiLCJqb2luZWRSb29tcyIsImdldFJvb21zIiwiZ2V0TXlNZW1iZXJzaGlwIiwiZ2V0Sm9pbmVkTWVtYmVyQ291bnQiLCJtZW1iZXJSb29tcyIsInJlZHVjZSIsIm1lbWJlcnMiLCJnZXRVc2VySWRGb3JSb29tSWQiLCJqb2luZWRNZW1iZXJzIiwicGlja2VkTWVtYmVyUm9vbVNpemUiLCJtZW1iZXJTY29yZXMiLCJPYmplY3QiLCJ2YWx1ZXMiLCJzY29yZXMiLCJlbnRyeSIsIm51bU1lbWJlcnNUb3RhbCIsImMiLCJtYXhSYW5nZSIsIm51bVJvb21zIiwic2NvcmUiLCJNYXRoIiwibWF4IiwicG93IiwidHJ1ZUpvaW5lZFJvb21zIiwibm93IiwiRGF0ZSIsImdldFRpbWUiLCJlYXJsaWVzdEFnZUNvbnNpZGVyZWQiLCJtYXhNZXNzYWdlc0NvbnNpZGVyZWQiLCJsYXN0U3Bva2UiLCJsYXN0U3Bva2VNZW1iZXJzIiwiaXNEbSIsImtleXMiLCJ0YWdzIiwiZXZlbnRzIiwiZ2V0TGl2ZVRpbWVsaW5lIiwiZ2V0RXZlbnRzIiwiZ2V0U2VuZGVyIiwidHMiLCJkaXN0YW5jZUZyb21Ob3ciLCJhYnMiLCJpbnZlcnNlVGltZSIsInNjb3JlQm9vc3QiLCJyZWNvcmQiLCJsb2NhbGVDb21wYXJlIiwiZmFpbGVkVXNlcnMiLCJzdGF0ZXMiLCJsb2ciLCJjc3ZVc2VycyIsIm5ld01lbWJlciIsInN0YXJ0c1dpdGgiLCJuZXdUYXJnZXRzIiwiX3JlbmRlclNlY3Rpb24iLCJzb3VyY2VNZW1iZXJzIiwic2hvd051bSIsInNob3dNb3JlRm4iLCJfc2hvd01vcmVSZWNlbnRzIiwiYmluZCIsIl9zaG93TW9yZVN1Z2dlc3Rpb25zIiwic2VjdGlvbk5hbWUiLCJwcmlvcml0eUFkZGl0aW9uYWxNZW1iZXJzIiwib3RoZXJBZGRpdGlvbmFsTWVtYmVycyIsImhhc01peGlucyIsIm5vdEFscmVhZHlFeGlzdHMiLCJzb21lIiwiaGFzQWRkaXRpb25hbE1lbWJlcnMiLCJmaWx0ZXJCeSIsInRvUmVuZGVyIiwic2xpY2UiLCJoYXNNb3JlIiwic2hvd01vcmUiLCJ0aWxlcyIsIl90b2dnbGVNZW1iZXIiLCJfcmVuZGVyRWRpdG9yIiwiaW5wdXQiLCJfb25LZXlEb3duIiwiX3VwZGF0ZUZpbHRlciIsIl9vblBhc3RlIiwiX29uQ2xpY2tJbnB1dEFyZWEiLCJfcmVuZGVySWRlbnRpdHlTZXJ2ZXJXYXJuaW5nIiwiZGVmYXVsdElkZW50aXR5U2VydmVyVXJsIiwiZGVmYXVsdElkZW50aXR5U2VydmVyTmFtZSIsImRlZmF1bHQiLCJzdWIiLCJfb25Vc2VEZWZhdWx0SWRlbnRpdHlTZXJ2ZXJDbGljayIsInNldHRpbmdzIiwiX29uTWFuYWdlU2V0dGluZ3NDbGljayIsIkJhc2VEaWFsb2ciLCJTcGlubmVyIiwic3Bpbm5lciIsImhlbHBUZXh0IiwiYnV0dG9uVGV4dCIsImdvQnV0dG9uRm4iLCJfc3RhcnREbSIsIl9pbnZpdGVVc2VycyIsImhhc1NlbGVjdGlvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQ0E7Ozs7Ozs7Ozs7Ozs7OztBQXVDTyxNQUFNQSxPQUFPLEdBQUcsSUFBaEI7O0FBQ0EsTUFBTUMsV0FBVyxHQUFHLFFBQXBCOztBQUVQLE1BQU1DLG1CQUFtQixHQUFHLENBQTVCLEMsQ0FBK0I7O0FBQy9CLE1BQU1DLHFCQUFxQixHQUFHLENBQTlCLEMsQ0FBaUM7QUFFakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxNQUFNQyxNQUFOLENBQWE7QUFDVDs7OztBQUlBLE1BQUlDLElBQUo7QUFBQTtBQUFtQjtBQUFFLFVBQU0sSUFBSUMsS0FBSixDQUFVLDhCQUFWLENBQU47QUFBa0Q7QUFFdkU7Ozs7OztBQUlBLE1BQUlDLE1BQUo7QUFBQTtBQUFxQjtBQUFFLFVBQU0sSUFBSUQsS0FBSixDQUFVLDhCQUFWLENBQU47QUFBa0Q7QUFFekU7Ozs7OztBQUlBRSxFQUFBQSxlQUFlO0FBQUE7QUFBVztBQUFFLFVBQU0sSUFBSUYsS0FBSixDQUFVLDhCQUFWLENBQU47QUFBa0Q7O0FBakJyRTs7QUFvQmIsTUFBTUcsZUFBTixTQUE4QkwsTUFBOUIsQ0FBcUM7QUFLakNNLEVBQUFBLFdBQVcsQ0FBQ0M7QUFBRDtBQUFBLElBQTZFO0FBQ3BGO0FBRG9GO0FBQUE7QUFBQTtBQUVwRixTQUFLQyxPQUFMLEdBQWVELGFBQWEsQ0FBQ0UsT0FBN0I7QUFDQSxTQUFLQyxZQUFMLEdBQW9CSCxhQUFhLENBQUNJLFlBQWxDO0FBQ0EsU0FBS0MsVUFBTCxHQUFrQkwsYUFBYSxDQUFDTSxVQUFoQztBQUNILEdBVmdDLENBWWpDOzs7QUFDQSxNQUFJWixJQUFKO0FBQUE7QUFBbUI7QUFDZixXQUFPLEtBQUtTLFlBQUwsSUFBcUIsS0FBS0YsT0FBakM7QUFDSDs7QUFFRCxNQUFJTCxNQUFKO0FBQUE7QUFBcUI7QUFDakIsV0FBTyxLQUFLSyxPQUFaO0FBQ0g7O0FBRURKLEVBQUFBLGVBQWU7QUFBQTtBQUFXO0FBQ3RCLFdBQU8sS0FBS1EsVUFBWjtBQUNIOztBQXZCZ0M7O0FBMEJyQyxNQUFNRSxjQUFOLFNBQTZCZCxNQUE3QixDQUFvQztBQUdoQ00sRUFBQUEsV0FBVyxDQUFDUztBQUFEO0FBQUEsSUFBYTtBQUNwQjtBQURvQjtBQUVwQixTQUFLQyxHQUFMLEdBQVdELEVBQVg7QUFDSCxHQU4rQixDQVFoQztBQUNBO0FBQ0E7OztBQUNBLE1BQUlFLE9BQUo7QUFBQTtBQUF1QjtBQUNuQixXQUFPLEtBQUtELEdBQUwsQ0FBU0UsUUFBVCxDQUFrQixHQUFsQixDQUFQO0FBQ0gsR0FiK0IsQ0FlaEM7OztBQUNBLE1BQUlqQixJQUFKO0FBQUE7QUFBbUI7QUFDZixXQUFPLEtBQUtlLEdBQVo7QUFDSDs7QUFFRCxNQUFJYixNQUFKO0FBQUE7QUFBcUI7QUFDakIsV0FBTyxLQUFLYSxHQUFaO0FBQ0g7O0FBRURaLEVBQUFBLGVBQWU7QUFBQTtBQUFXO0FBQ3RCLFdBQU8sSUFBUDtBQUNIOztBQTFCK0I7O0FBNkJwQyxNQUFNZSxVQUFOLFNBQXlCQyxlQUFNQyxhQUEvQixDQUE2QztBQUFBO0FBQUE7QUFBQSxxREFNNUJDLENBQUQsSUFBTztBQUNmO0FBQ0FBLE1BQUFBLENBQUMsQ0FBQ0MsY0FBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGVBQUY7QUFFQSxXQUFLQyxLQUFMLENBQVdDLFFBQVgsQ0FBb0IsS0FBS0QsS0FBTCxDQUFXRSxNQUEvQjtBQUNILEtBWndDO0FBQUE7O0FBY3pDQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFVBQU1FLFVBQVUsR0FBRyxFQUFuQjtBQUNBLFVBQU1DLE1BQU0sR0FBRyxLQUFLVCxLQUFMLENBQVdFLE1BQVgsQ0FBa0JWLE9BQWxCLEdBQ1Q7QUFDRSxNQUFBLFNBQVMsRUFBQyx5RUFEWjtBQUVFLE1BQUEsR0FBRyxFQUFFa0IsT0FBTyxDQUFDLGdEQUFELENBRmQ7QUFHRSxNQUFBLEtBQUssRUFBRUYsVUFIVDtBQUdxQixNQUFBLE1BQU0sRUFBRUE7QUFIN0IsTUFEUyxHQUtULDZCQUFDLFVBQUQ7QUFDRSxNQUFBLFNBQVMsRUFBQyxpQ0FEWjtBQUVFLE1BQUEsR0FBRyxFQUFFLG1DQUNERyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxnQkFBdEIsRUFEQyxFQUN5QyxLQUFLYixLQUFMLENBQVdFLE1BQVgsQ0FBa0J2QixlQUFsQixFQUR6QyxFQUVENkIsVUFGQyxFQUVXQSxVQUZYLEVBRXVCLE1BRnZCLENBRlA7QUFLRSxNQUFBLElBQUksRUFBRSxLQUFLUixLQUFMLENBQVdFLE1BQVgsQ0FBa0IxQixJQUwxQjtBQU1FLE1BQUEsTUFBTSxFQUFFLEtBQUt3QixLQUFMLENBQVdFLE1BQVgsQ0FBa0J4QixNQU41QjtBQU9FLE1BQUEsS0FBSyxFQUFFOEIsVUFQVDtBQVFFLE1BQUEsTUFBTSxFQUFFQTtBQVJWLE1BTE47QUFlQSxRQUFJTSxXQUFKOztBQUNBLFFBQUksS0FBS2QsS0FBTCxDQUFXQyxRQUFmLEVBQXlCO0FBQ3JCYSxNQUFBQSxXQUFXLEdBQ1AsNkJBQUMsZ0JBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxpQ0FEZDtBQUVJLFFBQUEsT0FBTyxFQUFFLEtBQUtDO0FBRmxCLFNBSUk7QUFBSyxRQUFBLEdBQUcsRUFBRUwsT0FBTyxDQUFDLDBDQUFELENBQWpCO0FBQStELFFBQUEsR0FBRyxFQUFFLHlCQUFHLFFBQUgsQ0FBcEU7QUFBa0YsUUFBQSxLQUFLLEVBQUUsQ0FBekY7QUFBNEYsUUFBQSxNQUFNLEVBQUU7QUFBcEcsUUFKSixDQURKO0FBUUg7O0FBRUQsV0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNLRCxNQURMLEVBRUk7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUFpRCxLQUFLVCxLQUFMLENBQVdFLE1BQVgsQ0FBa0IxQixJQUFuRSxDQUZKLENBREosRUFLTXNDLFdBTE4sQ0FESjtBQVNIOztBQXZEd0M7OzhCQUF2Q3BCLFUsZUFDaUI7QUFDZlEsRUFBQUEsTUFBTSxFQUFFYyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEVjtBQUNzQjtBQUNyQ2pCLEVBQUFBLFFBQVEsRUFBRWUsbUJBQVVHLElBRkwsQ0FFVzs7QUFGWCxDOztBQXlEdkIsTUFBTUMsVUFBTixTQUF5QnpCLGVBQU1DLGFBQS9CLENBQTZDO0FBQUE7QUFBQTtBQUFBLG9EQVM3QkMsQ0FBRCxJQUFPO0FBQ2Q7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDQyxjQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsZUFBRjtBQUVBLFdBQUtDLEtBQUwsQ0FBV3FCLFFBQVgsQ0FBb0IsS0FBS3JCLEtBQUwsQ0FBV0UsTUFBL0I7QUFDSCxLQWZ3QztBQUFBOztBQWlCekNvQixFQUFBQSxjQUFjLENBQUNDO0FBQUQ7QUFBQSxJQUFjO0FBQ3hCLFFBQUksQ0FBQyxLQUFLdkIsS0FBTCxDQUFXd0IsYUFBaEIsRUFBK0IsT0FBT0QsR0FBUCxDQURQLENBR3hCO0FBQ0E7QUFDQTs7QUFDQSxVQUFNRSxRQUFRLEdBQUdGLEdBQUcsQ0FBQ0csV0FBSixFQUFqQjtBQUNBLFVBQU1DLFNBQVMsR0FBRyxLQUFLM0IsS0FBTCxDQUFXd0IsYUFBWCxDQUF5QkUsV0FBekIsRUFBbEI7QUFFQSxVQUFNRSxNQUFNLEdBQUcsRUFBZjtBQUVBLFFBQUlDLENBQUMsR0FBRyxDQUFSO0FBQ0EsUUFBSUMsRUFBSjs7QUFDQSxXQUFPLENBQUNBLEVBQUUsR0FBR0wsUUFBUSxDQUFDTSxPQUFULENBQWlCSixTQUFqQixFQUE0QkUsQ0FBNUIsQ0FBTixLQUF5QyxDQUFoRCxFQUFtRDtBQUMvQztBQUNBLFVBQUlDLEVBQUUsR0FBR0QsQ0FBVCxFQUFZO0FBQ1I7QUFDQUQsUUFBQUEsTUFBTSxDQUFDSSxJQUFQLENBQVk7QUFBTSxVQUFBLEdBQUcsRUFBRUgsQ0FBQyxHQUFHO0FBQWYsV0FBeUJOLEdBQUcsQ0FBQ1UsU0FBSixDQUFjSixDQUFkLEVBQWlCQyxFQUFqQixDQUF6QixDQUFaO0FBQ0g7O0FBRURELE1BQUFBLENBQUMsR0FBR0MsRUFBSixDQVArQyxDQU92QztBQUVSOztBQUNBLFlBQU1JLE1BQU0sR0FBR1gsR0FBRyxDQUFDVSxTQUFKLENBQWNKLENBQWQsRUFBaUJGLFNBQVMsQ0FBQ1EsTUFBVixHQUFtQk4sQ0FBcEMsQ0FBZjtBQUNBRCxNQUFBQSxNQUFNLENBQUNJLElBQVAsQ0FBWTtBQUFNLFFBQUEsU0FBUyxFQUFDLG9DQUFoQjtBQUFxRCxRQUFBLEdBQUcsRUFBRUgsQ0FBQyxHQUFHO0FBQTlELFNBQXVFSyxNQUF2RSxDQUFaO0FBQ0FMLE1BQUFBLENBQUMsSUFBSUssTUFBTSxDQUFDQyxNQUFaO0FBQ0gsS0ExQnVCLENBNEJ4Qjs7O0FBQ0EsUUFBSU4sQ0FBQyxHQUFHTixHQUFHLENBQUNZLE1BQVosRUFBb0I7QUFDaEJQLE1BQUFBLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZO0FBQU0sUUFBQSxHQUFHLEVBQUVILENBQUMsR0FBRztBQUFmLFNBQXVCTixHQUFHLENBQUNVLFNBQUosQ0FBY0osQ0FBZCxDQUF2QixDQUFaO0FBQ0g7O0FBRUQsV0FBT0QsTUFBUDtBQUNIOztBQUVEekIsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBRUEsUUFBSThCLFNBQVMsR0FBRyxJQUFoQjs7QUFDQSxRQUFJLEtBQUtwQyxLQUFMLENBQVdxQyxZQUFmLEVBQTZCO0FBQ3pCLFlBQU1DLE9BQU8sR0FBRyw0QkFBYSxLQUFLdEMsS0FBTCxDQUFXcUMsWUFBeEIsQ0FBaEI7QUFDQUQsTUFBQUEsU0FBUyxHQUFHO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBaURFLE9BQWpELENBQVo7QUFDSDs7QUFFRCxVQUFNOUIsVUFBVSxHQUFHLEVBQW5CO0FBQ0EsVUFBTUMsTUFBTSxHQUFHLEtBQUtULEtBQUwsQ0FBV0UsTUFBWCxDQUFrQlYsT0FBbEIsR0FDVDtBQUNFLE1BQUEsR0FBRyxFQUFFa0IsT0FBTyxDQUFDLGdEQUFELENBRGQ7QUFFRSxNQUFBLEtBQUssRUFBRUYsVUFGVDtBQUVxQixNQUFBLE1BQU0sRUFBRUE7QUFGN0IsTUFEUyxHQUlULDZCQUFDLFVBQUQ7QUFDRSxNQUFBLEdBQUcsRUFBRSxtQ0FDREcsaUNBQWdCQyxHQUFoQixHQUFzQkMsZ0JBQXRCLEVBREMsRUFDeUMsS0FBS2IsS0FBTCxDQUFXRSxNQUFYLENBQWtCdkIsZUFBbEIsRUFEekMsRUFFRDZCLFVBRkMsRUFFV0EsVUFGWCxFQUV1QixNQUZ2QixDQURQO0FBSUUsTUFBQSxJQUFJLEVBQUUsS0FBS1IsS0FBTCxDQUFXRSxNQUFYLENBQWtCMUIsSUFKMUI7QUFLRSxNQUFBLE1BQU0sRUFBRSxLQUFLd0IsS0FBTCxDQUFXRSxNQUFYLENBQWtCeEIsTUFMNUI7QUFNRSxNQUFBLEtBQUssRUFBRThCLFVBTlQ7QUFPRSxNQUFBLE1BQU0sRUFBRUE7QUFQVixNQUpOO0FBYUEsUUFBSStCLFNBQVMsR0FBRyxJQUFoQjs7QUFDQSxRQUFJLEtBQUt2QyxLQUFMLENBQVd3QyxVQUFmLEVBQTJCO0FBQ3ZCO0FBQ0FELE1BQUFBLFNBQVMsR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsUUFBWjtBQUNILEtBM0JJLENBNkJMO0FBQ0E7OztBQUNBLFVBQU1FLGFBQWEsR0FDZjtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ0toQyxNQURMLEVBRUs4QixTQUZMLENBREo7O0FBT0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLDBCQUFmO0FBQTBDLE1BQUEsT0FBTyxFQUFFLEtBQUtHO0FBQXhELE9BQ0tELGFBREwsRUFFSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQWlELEtBQUtuQixjQUFMLENBQW9CLEtBQUt0QixLQUFMLENBQVdFLE1BQVgsQ0FBa0IxQixJQUF0QyxDQUFqRCxDQUZKLEVBR0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUFtRCxLQUFLOEMsY0FBTCxDQUFvQixLQUFLdEIsS0FBTCxDQUFXRSxNQUFYLENBQWtCeEIsTUFBdEMsQ0FBbkQsQ0FISixFQUlLMEQsU0FKTCxDQURKO0FBUUg7O0FBbkd3Qzs7OEJBQXZDaEIsVSxlQUNpQjtBQUNmbEIsRUFBQUEsTUFBTSxFQUFFYyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEVjtBQUNzQjtBQUNyQ21CLEVBQUFBLFlBQVksRUFBRXJCLG1CQUFVMkIsTUFGVDtBQUdmdEIsRUFBQUEsUUFBUSxFQUFFTCxtQkFBVUcsSUFBVixDQUFlRCxVQUhWO0FBR3NCO0FBQ3JDTSxFQUFBQSxhQUFhLEVBQUVSLG1CQUFVNEIsTUFKVjtBQUtmSixFQUFBQSxVQUFVLEVBQUV4QixtQkFBVTZCO0FBTFAsQzs7QUFxR1IsTUFBTUMsWUFBTixTQUEyQm5ELGVBQU1DLGFBQWpDLENBQStDO0FBb0IxRGYsRUFBQUEsV0FBVyxDQUFDbUIsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLDBEQUhNLElBR047QUFBQSxzREFGRCxJQUVDO0FBQUEsb0RBMlBSLFlBQVk7QUFDbkIsV0FBSytDLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxJQUFJLEVBQUU7QUFBUCxPQUFkOztBQUNBLFlBQU1DLE9BQU8sR0FBRyxLQUFLQyxjQUFMLEVBQWhCOztBQUNBLFlBQU1DLFNBQVMsR0FBR0YsT0FBTyxDQUFDRyxHQUFSLENBQVlDLENBQUMsSUFBSUEsQ0FBQyxDQUFDM0UsTUFBbkIsQ0FBbEIsQ0FIbUIsQ0FLbkI7O0FBQ0EsWUFBTTRFLFlBQVksR0FBR0MsbUJBQVVDLE1BQVYsR0FBbUJDLHVCQUFuQixDQUEyQ04sU0FBM0MsQ0FBckI7O0FBQ0EsVUFBSUcsWUFBSixFQUFrQjtBQUNkSSw0QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVRDLFVBQUFBLE9BQU8sRUFBRVAsWUFBWSxDQUFDUSxNQUZiO0FBR1RDLFVBQUFBLFdBQVcsRUFBRSxLQUhKO0FBSVRDLFVBQUFBLE9BQU8sRUFBRTtBQUpBLFNBQWI7O0FBTUEsYUFBS2hFLEtBQUwsQ0FBV2lFLFVBQVg7QUFDQTtBQUNIOztBQUVELFlBQU1DLGlCQUFpQixHQUFHO0FBQUNDLFFBQUFBLFlBQVksRUFBRTtBQUFmLE9BQTFCOztBQUVBLFVBQUlDLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6RDtBQUNBO0FBQ0EsY0FBTUMsTUFBTSxHQUFHM0QsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLGNBQU0yRCxpQkFBaUIsR0FBRyxNQUFNLHNDQUFxQkQsTUFBckIsRUFBNkJuQixTQUE3QixDQUFoQzs7QUFDQSxZQUFJb0IsaUJBQUosRUFBdUI7QUFDbkJMLFVBQUFBLGlCQUFpQixDQUFDTSxVQUFsQixHQUErQixJQUEvQjtBQUNIO0FBQ0osT0E1QmtCLENBOEJuQjtBQUNBOzs7QUFDQSxVQUFJQyxpQkFBaUIsR0FBR0MsT0FBTyxDQUFDQyxPQUFSLEVBQXhCOztBQUNBLFlBQU1DLE1BQU0sR0FBR3pCLFNBQVMsQ0FBQ2hCLE1BQVYsS0FBcUIsQ0FBckIsSUFBMEJnQixTQUFTLENBQUMsQ0FBRCxDQUFULEtBQWlCeEMsaUNBQWdCQyxHQUFoQixHQUFzQmlFLFNBQXRCLEVBQTFEOztBQUNBLFVBQUkxQixTQUFTLENBQUNoQixNQUFWLEtBQXFCLENBQXJCLElBQTBCLENBQUN5QyxNQUEvQixFQUF1QztBQUNuQ1YsUUFBQUEsaUJBQWlCLENBQUNZLFFBQWxCLEdBQTZCM0IsU0FBUyxDQUFDLENBQUQsQ0FBdEM7QUFDQXNCLFFBQUFBLGlCQUFpQixHQUFHLHlCQUFXUCxpQkFBWCxDQUFwQjtBQUNILE9BSEQsTUFHTyxJQUFJVSxNQUFKLEVBQVk7QUFDZkgsUUFBQUEsaUJBQWlCLEdBQUcseUJBQVdQLGlCQUFYLENBQXBCO0FBQ0gsT0FGTSxNQUVBO0FBQ0g7QUFDQU8sUUFBQUEsaUJBQWlCLEdBQUcseUJBQVdQLGlCQUFYLEVBQThCYSxJQUE5QixDQUFtQ2pCLE1BQU0sSUFBSTtBQUM3RCxpQkFBTyxzQ0FBcUJBLE1BQXJCLEVBQTZCWCxTQUE3QixDQUFQO0FBQ0gsU0FGbUIsRUFFakI0QixJQUZpQixDQUVabkQsTUFBTSxJQUFJO0FBQ2QsY0FBSSxLQUFLb0QsNEJBQUwsQ0FBa0NwRCxNQUFsQyxDQUFKLEVBQStDO0FBQzNDLG1CQUFPLElBQVAsQ0FEMkMsQ0FDOUI7QUFDaEI7QUFDSixTQU5tQixDQUFwQjtBQU9ILE9BaERrQixDQWtEbkI7OztBQUNBNkMsTUFBQUEsaUJBQWlCLENBQUNNLElBQWxCLENBQXVCRSxLQUFLLElBQUk7QUFDNUIsWUFBSUEsS0FBSyxLQUFLLElBQWQsRUFBb0IsT0FEUSxDQUNBOztBQUM1QixhQUFLakYsS0FBTCxDQUFXaUUsVUFBWDtBQUNILE9BSEQsRUFHR2lCLEtBSEgsQ0FHU0MsR0FBRyxJQUFJO0FBQ1pDLFFBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjRixHQUFkO0FBQ0EsYUFBS3BDLFFBQUwsQ0FBYztBQUNWQyxVQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWc0MsVUFBQUEsU0FBUyxFQUFFLHlCQUFHLHNGQUFIO0FBRkQsU0FBZDtBQUlILE9BVEQ7QUFVSCxLQXhUa0I7QUFBQSx3REEwVEosTUFBTTtBQUNqQixXQUFLdkMsUUFBTCxDQUFjO0FBQUNDLFFBQUFBLElBQUksRUFBRTtBQUFQLE9BQWQ7O0FBQ0EsV0FBS0UsY0FBTDs7QUFDQSxZQUFNRCxPQUFPLEdBQUcsS0FBS0MsY0FBTCxFQUFoQjs7QUFDQSxZQUFNQyxTQUFTLEdBQUdGLE9BQU8sQ0FBQ0csR0FBUixDQUFZQyxDQUFDLElBQUlBLENBQUMsQ0FBQzNFLE1BQW5CLENBQWxCOztBQUVBLFlBQU02RyxJQUFJLEdBQUc1RSxpQ0FBZ0JDLEdBQWhCLEdBQXNCNEUsT0FBdEIsQ0FBOEIsS0FBS3hGLEtBQUwsQ0FBVzhELE1BQXpDLENBQWI7O0FBQ0EsVUFBSSxDQUFDeUIsSUFBTCxFQUFXO0FBQ1BILFFBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLDRDQUFkO0FBQ0EsYUFBS3RDLFFBQUwsQ0FBYztBQUNWQyxVQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWc0MsVUFBQUEsU0FBUyxFQUFFLHlCQUFHLGtEQUFIO0FBRkQsU0FBZDtBQUlBO0FBQ0g7O0FBRUQsNENBQXFCLEtBQUt0RixLQUFMLENBQVc4RCxNQUFoQyxFQUF3Q1gsU0FBeEMsRUFBbUQ0QixJQUFuRCxDQUF3RG5ELE1BQU0sSUFBSTtBQUM5RCxZQUFJLENBQUMsS0FBS29ELDRCQUFMLENBQWtDcEQsTUFBbEMsQ0FBTCxFQUFnRDtBQUFFO0FBQzlDLGVBQUs1QixLQUFMLENBQVdpRSxVQUFYO0FBQ0g7QUFDSixPQUpELEVBSUdpQixLQUpILENBSVNDLEdBQUcsSUFBSTtBQUNaQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBZDtBQUNBLGFBQUtwQyxRQUFMLENBQWM7QUFDVkMsVUFBQUEsSUFBSSxFQUFFLEtBREk7QUFFVnNDLFVBQUFBLFNBQVMsRUFBRSx5QkFDUCwwRkFETztBQUZELFNBQWQ7QUFNSCxPQVpEO0FBYUgsS0F2VmtCO0FBQUEsc0RBeVZMekYsQ0FBRCxJQUFPO0FBQ2hCO0FBQ0EsVUFBSSxDQUFDQSxDQUFDLENBQUM0RixNQUFGLENBQVNDLEtBQVYsSUFBbUIsQ0FBQyxLQUFLQyxLQUFMLENBQVczQyxJQUEvQixJQUF1QyxLQUFLMkMsS0FBTCxDQUFXMUMsT0FBWCxDQUFtQmQsTUFBbkIsR0FBNEIsQ0FBbkUsSUFBd0V0QyxDQUFDLENBQUMrRixHQUFGLEtBQVVDLGNBQUlDLFNBQXRGLElBQ0EsQ0FBQ2pHLENBQUMsQ0FBQ2tHLE9BREgsSUFDYyxDQUFDbEcsQ0FBQyxDQUFDbUcsUUFEakIsSUFDNkIsQ0FBQ25HLENBQUMsQ0FBQ29HLE9BRHBDLEVBRUU7QUFDRXBHLFFBQUFBLENBQUMsQ0FBQ0MsY0FBRjs7QUFDQSxhQUFLb0csYUFBTCxDQUFtQixLQUFLUCxLQUFMLENBQVcxQyxPQUFYLENBQW1CLEtBQUswQyxLQUFMLENBQVcxQyxPQUFYLENBQW1CZCxNQUFuQixHQUE0QixDQUEvQyxDQUFuQjtBQUNIO0FBQ0osS0FqV2tCO0FBQUEseURBbVdGdEMsQ0FBRCxJQUFPO0FBQ25CLFlBQU1zRyxJQUFJLEdBQUd0RyxDQUFDLENBQUM0RixNQUFGLENBQVNDLEtBQXRCO0FBQ0EsV0FBSzNDLFFBQUwsQ0FBYztBQUFDcUQsUUFBQUEsVUFBVSxFQUFFRDtBQUFiLE9BQWQsRUFGbUIsQ0FJbkI7QUFDQTtBQUNBOztBQUNBLFVBQUksS0FBS0UsY0FBVCxFQUF5QjtBQUNyQkMsUUFBQUEsWUFBWSxDQUFDLEtBQUtELGNBQU4sQ0FBWjtBQUNIOztBQUNELFdBQUtBLGNBQUwsR0FBc0JFLFVBQVUsQ0FBQyxZQUFZO0FBQ3pDNUYseUNBQWdCQyxHQUFoQixHQUFzQjRGLG1CQUF0QixDQUEwQztBQUFDTCxVQUFBQTtBQUFELFNBQTFDLEVBQWtEcEIsSUFBbEQsQ0FBdUQsTUFBTTBCLENBQU4sSUFBVztBQUM5RCxjQUFJTixJQUFJLEtBQUssS0FBS1IsS0FBTCxDQUFXUyxVQUF4QixFQUFvQztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNIOztBQUVELGNBQUksQ0FBQ0ssQ0FBQyxDQUFDQyxPQUFQLEVBQWdCRCxDQUFDLENBQUNDLE9BQUYsR0FBWSxFQUFaLENBUjhDLENBVTlEO0FBQ0E7O0FBQ0EsY0FBSVAsSUFBSSxDQUFDLENBQUQsQ0FBSixLQUFZLEdBQVosSUFBbUJBLElBQUksQ0FBQ3BFLE9BQUwsQ0FBYSxHQUFiLElBQW9CLENBQTNDLEVBQThDO0FBQzFDLGdCQUFJO0FBQ0Esb0JBQU00RSxPQUFPLEdBQUcsTUFBTWhHLGlDQUFnQkMsR0FBaEIsR0FBc0JnRyxjQUF0QixDQUFxQ1QsSUFBckMsQ0FBdEI7O0FBQ0Esa0JBQUlRLE9BQUosRUFBYTtBQUNUO0FBQ0E7QUFDQTtBQUNBRixnQkFBQUEsQ0FBQyxDQUFDQyxPQUFGLENBQVVHLE1BQVYsQ0FBaUIsQ0FBakIsRUFBb0IsQ0FBcEIsRUFBdUI7QUFDbkI3SCxrQkFBQUEsT0FBTyxFQUFFbUgsSUFEVTtBQUVuQmpILGtCQUFBQSxZQUFZLEVBQUV5SCxPQUFPLENBQUMsYUFBRCxDQUZGO0FBR25Cdkgsa0JBQUFBLFVBQVUsRUFBRXVILE9BQU8sQ0FBQyxZQUFEO0FBSEEsaUJBQXZCO0FBS0g7QUFDSixhQVpELENBWUUsT0FBTzlHLENBQVAsRUFBVTtBQUNSdUYsY0FBQUEsT0FBTyxDQUFDMEIsSUFBUixDQUFhLHdEQUFiO0FBQ0ExQixjQUFBQSxPQUFPLENBQUMwQixJQUFSLENBQWFqSCxDQUFiLEVBRlEsQ0FJUjtBQUNBOztBQUNBNEcsY0FBQUEsQ0FBQyxDQUFDQyxPQUFGLENBQVVHLE1BQVYsQ0FBaUIsQ0FBakIsRUFBb0IsQ0FBcEIsRUFBdUI7QUFDbkI3SCxnQkFBQUEsT0FBTyxFQUFFbUgsSUFEVTtBQUVuQmpILGdCQUFBQSxZQUFZLEVBQUVpSCxJQUZLO0FBR25CL0csZ0JBQUFBLFVBQVUsRUFBRTtBQUhPLGVBQXZCO0FBS0g7QUFDSjs7QUFFRCxlQUFLMkQsUUFBTCxDQUFjO0FBQ1ZnRSxZQUFBQSxrQkFBa0IsRUFBRU4sQ0FBQyxDQUFDQyxPQUFGLENBQVV0RCxHQUFWLENBQWM0RCxDQUFDLEtBQUs7QUFDcEN0SSxjQUFBQSxNQUFNLEVBQUVzSSxDQUFDLENBQUNoSSxPQUQwQjtBQUVwQ2lJLGNBQUFBLElBQUksRUFBRSxJQUFJckksZUFBSixDQUFvQm9JLENBQXBCO0FBRjhCLGFBQUwsQ0FBZjtBQURWLFdBQWQ7QUFNSCxTQTdDRCxFQTZDRzlCLEtBN0NILENBNkNTckYsQ0FBQyxJQUFJO0FBQ1Z1RixVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxpQ0FBZDtBQUNBRCxVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3hGLENBQWQ7QUFDQSxlQUFLa0QsUUFBTCxDQUFjO0FBQUNnRSxZQUFBQSxrQkFBa0IsRUFBRTtBQUFyQixXQUFkLEVBSFUsQ0FHK0I7QUFDNUMsU0FqREQsRUFEeUMsQ0FvRHpDO0FBQ0E7OztBQUNBLFlBQUksQ0FBQyxLQUFLcEIsS0FBTCxDQUFXdUIsb0JBQWhCLEVBQXNDO0FBQ2xDO0FBQ0EsZUFBS25FLFFBQUwsQ0FBYztBQUFDb0UsWUFBQUEsb0JBQW9CLEVBQUU7QUFBdkIsV0FBZDtBQUNBO0FBQ0g7O0FBQ0QsWUFBSWhCLElBQUksQ0FBQ3BFLE9BQUwsQ0FBYSxHQUFiLElBQW9CLENBQXBCLElBQXlCcUYsS0FBSyxDQUFDQyxVQUFOLENBQWlCbEIsSUFBakIsQ0FBN0IsRUFBcUQ7QUFDakQ7QUFDQTtBQUNBLGVBQUtwRCxRQUFMLENBQWM7QUFDVjtBQUNBdUUsWUFBQUEsb0JBQW9CLEVBQUUsQ0FBQztBQUFDTCxjQUFBQSxJQUFJLEVBQUUsSUFBSTVILGNBQUosQ0FBbUI4RyxJQUFuQixDQUFQO0FBQWlDekgsY0FBQUEsTUFBTSxFQUFFeUg7QUFBekMsYUFBRDtBQUZaLFdBQWQ7O0FBSUEsY0FBSTtBQUNBLGtCQUFNb0IsVUFBVSxHQUFHLElBQUlDLDJCQUFKLEVBQW5CO0FBQ0Esa0JBQU1DLEtBQUssR0FBRyxNQUFNRixVQUFVLENBQUNHLGNBQVgsRUFBcEI7QUFDQSxnQkFBSXZCLElBQUksS0FBSyxLQUFLUixLQUFMLENBQVdTLFVBQXhCLEVBQW9DLE9BSHBDLENBRzRDOztBQUU1QyxrQkFBTXVCLE1BQU0sR0FBRyxNQUFNaEgsaUNBQWdCQyxHQUFoQixHQUFzQmdILGNBQXRCLENBQ2pCLE9BRGlCLEVBRWpCekIsSUFGaUIsRUFHakIwQixTQUhpQixFQUdOO0FBQ1hKLFlBQUFBLEtBSmlCLENBQXJCO0FBTUEsZ0JBQUl0QixJQUFJLEtBQUssS0FBS1IsS0FBTCxDQUFXUyxVQUF4QixFQUFvQyxPQVhwQyxDQVc0Qzs7QUFFNUMsZ0JBQUksQ0FBQ3VCLE1BQUQsSUFBVyxDQUFDQSxNQUFNLENBQUNHLElBQXZCLEVBQTZCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNILGFBakJELENBbUJBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxrQkFBTW5CLE9BQU8sR0FBRyxNQUFNaEcsaUNBQWdCQyxHQUFoQixHQUFzQmdHLGNBQXRCLENBQXFDZSxNQUFNLENBQUNHLElBQTVDLENBQXRCO0FBQ0EsZ0JBQUkzQixJQUFJLEtBQUssS0FBS1IsS0FBTCxDQUFXUyxVQUFwQixJQUFrQyxDQUFDTyxPQUF2QyxFQUFnRCxPQXhCaEQsQ0F3QndEOztBQUN4RCxpQkFBSzVELFFBQUwsQ0FBYztBQUNWdUUsY0FBQUEsb0JBQW9CLEVBQUUsQ0FBQyxHQUFHLEtBQUszQixLQUFMLENBQVcyQixvQkFBZixFQUFxQztBQUN2REwsZ0JBQUFBLElBQUksRUFBRSxJQUFJckksZUFBSixDQUFvQjtBQUN0Qkksa0JBQUFBLE9BQU8sRUFBRTJJLE1BQU0sQ0FBQ0csSUFETTtBQUV0QjVJLGtCQUFBQSxZQUFZLEVBQUV5SCxPQUFPLENBQUNvQixXQUZBO0FBR3RCM0ksa0JBQUFBLFVBQVUsRUFBRXVILE9BQU8sQ0FBQ3ZIO0FBSEUsaUJBQXBCLENBRGlEO0FBTXZEVixnQkFBQUEsTUFBTSxFQUFFaUosTUFBTSxDQUFDRztBQU53QyxlQUFyQztBQURaLGFBQWQ7QUFVSCxXQW5DRCxDQW1DRSxPQUFPakksQ0FBUCxFQUFVO0FBQ1J1RixZQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxrQ0FBZDtBQUNBRCxZQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3hGLENBQWQ7QUFDQSxpQkFBS2tELFFBQUwsQ0FBYztBQUFDdUUsY0FBQUEsb0JBQW9CLEVBQUU7QUFBdkIsYUFBZCxFQUhRLENBR21DO0FBQzlDO0FBQ0o7QUFDSixPQTNHK0IsRUEyRzdCLEdBM0c2QixDQUFoQyxDQVZtQixDQXFIVjtBQUNaLEtBemRrQjtBQUFBLDREQTJkQSxNQUFNO0FBQ3JCLFdBQUt2RSxRQUFMLENBQWM7QUFBQ2lGLFFBQUFBLGVBQWUsRUFBRSxLQUFLckMsS0FBTCxDQUFXcUMsZUFBWCxHQUE2QjFKO0FBQS9DLE9BQWQ7QUFDSCxLQTdka0I7QUFBQSxnRUErZEksTUFBTTtBQUN6QixXQUFLeUUsUUFBTCxDQUFjO0FBQUNrRixRQUFBQSxtQkFBbUIsRUFBRSxLQUFLdEMsS0FBTCxDQUFXc0MsbUJBQVgsR0FBaUMzSjtBQUF2RCxPQUFkO0FBQ0gsS0FqZWtCO0FBQUEseURBbWVILENBQUM0QjtBQUFEO0FBQUEsU0FBb0I7QUFDaEMsVUFBSWtHLFVBQVUsR0FBRyxLQUFLVCxLQUFMLENBQVdTLFVBQTVCO0FBQ0EsWUFBTW5ELE9BQU8sR0FBRyxLQUFLMEMsS0FBTCxDQUFXMUMsT0FBWCxDQUFtQkcsR0FBbkIsQ0FBdUJDLENBQUMsSUFBSUEsQ0FBNUIsQ0FBaEIsQ0FGZ0MsQ0FFZ0I7O0FBQ2hELFlBQU02RSxHQUFHLEdBQUdqRixPQUFPLENBQUNsQixPQUFSLENBQWdCN0IsTUFBaEIsQ0FBWjs7QUFDQSxVQUFJZ0ksR0FBRyxJQUFJLENBQVgsRUFBYztBQUNWakYsUUFBQUEsT0FBTyxDQUFDNEQsTUFBUixDQUFlcUIsR0FBZixFQUFvQixDQUFwQjtBQUNILE9BRkQsTUFFTztBQUNIakYsUUFBQUEsT0FBTyxDQUFDakIsSUFBUixDQUFhOUIsTUFBYjtBQUNBa0csUUFBQUEsVUFBVSxHQUFHLEVBQWIsQ0FGRyxDQUVjO0FBQ3BCOztBQUNELFdBQUtyRCxRQUFMLENBQWM7QUFBQ0UsUUFBQUEsT0FBRDtBQUFVbUQsUUFBQUE7QUFBVixPQUFkO0FBQ0gsS0E5ZWtCO0FBQUEseURBZ2ZILENBQUNsRztBQUFEO0FBQUEsU0FBb0I7QUFDaEMsWUFBTStDLE9BQU8sR0FBRyxLQUFLMEMsS0FBTCxDQUFXMUMsT0FBWCxDQUFtQkcsR0FBbkIsQ0FBdUJDLENBQUMsSUFBSUEsQ0FBNUIsQ0FBaEIsQ0FEZ0MsQ0FDZ0I7O0FBQ2hELFlBQU02RSxHQUFHLEdBQUdqRixPQUFPLENBQUNsQixPQUFSLENBQWdCN0IsTUFBaEIsQ0FBWjs7QUFDQSxVQUFJZ0ksR0FBRyxJQUFJLENBQVgsRUFBYztBQUNWakYsUUFBQUEsT0FBTyxDQUFDNEQsTUFBUixDQUFlcUIsR0FBZixFQUFvQixDQUFwQjtBQUNBLGFBQUtuRixRQUFMLENBQWM7QUFBQ0UsVUFBQUE7QUFBRCxTQUFkO0FBQ0g7QUFDSixLQXZma0I7QUFBQSxvREF5ZlIsTUFBT3BELENBQVAsSUFBYTtBQUNwQixVQUFJLEtBQUs4RixLQUFMLENBQVdTLFVBQWYsRUFBMkI7QUFDdkI7QUFDQTtBQUNBO0FBQ0gsT0FMbUIsQ0FPcEI7OztBQUNBdkcsTUFBQUEsQ0FBQyxDQUFDQyxjQUFGLEdBUm9CLENBVXBCOztBQUNBLFlBQU1xSSxJQUFJLEdBQUd0SSxDQUFDLENBQUN1SSxhQUFGLENBQWdCQyxPQUFoQixDQUF3QixNQUF4QixDQUFiO0FBQ0EsWUFBTUMsZUFBZSxHQUFHLENBQ3BCO0FBQ0EsU0FBRyxLQUFLM0MsS0FBTCxDQUFXNEMsT0FGTSxFQUdwQixHQUFHLEtBQUs1QyxLQUFMLENBQVc2QyxXQUhNLEVBSXBCLEdBQUcsS0FBSzdDLEtBQUwsQ0FBV29CLGtCQUpNLEVBS3BCLEdBQUcsS0FBS3BCLEtBQUwsQ0FBVzJCLG9CQUxNLENBQXhCO0FBT0EsWUFBTW1CLEtBQUssR0FBRyxFQUFkO0FBQ0EsWUFBTUMsTUFBTSxHQUFHLEVBQWY7QUFDQSxZQUFNQyxrQkFBa0IsR0FBR1IsSUFBSSxDQUFDUyxLQUFMLENBQVcsUUFBWCxFQUFxQnhGLEdBQXJCLENBQXlCeUYsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLElBQUYsRUFBOUIsRUFBd0NDLE1BQXhDLENBQStDRixDQUFDLElBQUksQ0FBQyxDQUFDQSxDQUF0RCxDQUEzQixDQXJCb0IsQ0FxQmlFOztBQUNyRixXQUFLLE1BQU1HLE9BQVgsSUFBc0JMLGtCQUF0QixFQUEwQztBQUN0QyxjQUFNekksTUFBTSxHQUFHb0ksZUFBZSxDQUFDVyxJQUFoQixDQUFxQkMsQ0FBQyxJQUFJQSxDQUFDLENBQUN4SyxNQUFGLEtBQWFzSyxPQUF2QyxDQUFmOztBQUNBLFlBQUk5SSxNQUFKLEVBQVk7QUFDUnVJLFVBQUFBLEtBQUssQ0FBQ3pHLElBQU4sQ0FBVzlCLE1BQU0sQ0FBQytHLElBQWxCO0FBQ0E7QUFDSDs7QUFFRCxZQUFJK0IsT0FBTyxDQUFDakgsT0FBUixDQUFnQixHQUFoQixJQUF1QixDQUF2QixJQUE0QnFGLEtBQUssQ0FBQ0MsVUFBTixDQUFpQjJCLE9BQWpCLENBQWhDLEVBQTJEO0FBQ3ZEUCxVQUFBQSxLQUFLLENBQUN6RyxJQUFOLENBQVcsSUFBSTNDLGNBQUosQ0FBbUIySixPQUFuQixDQUFYO0FBQ0E7QUFDSDs7QUFFRCxZQUFJQSxPQUFPLENBQUMsQ0FBRCxDQUFQLEtBQWUsR0FBbkIsRUFBd0I7QUFDcEJOLFVBQUFBLE1BQU0sQ0FBQzFHLElBQVAsQ0FBWWdILE9BQVosRUFEb0IsQ0FDRTs7QUFDdEI7QUFDSDs7QUFFRCxZQUFJO0FBQ0EsZ0JBQU1yQyxPQUFPLEdBQUcsTUFBTWhHLGlDQUFnQkMsR0FBaEIsR0FBc0JnRyxjQUF0QixDQUFxQ29DLE9BQXJDLENBQXRCO0FBQ0EsZ0JBQU1HLFdBQVcsR0FBR3hDLE9BQU8sR0FBR0EsT0FBTyxDQUFDb0IsV0FBWCxHQUF5QixJQUFwRDtBQUNBLGdCQUFNcUIsU0FBUyxHQUFHekMsT0FBTyxHQUFHQSxPQUFPLENBQUN2SCxVQUFYLEdBQXdCLElBQWpEO0FBQ0FxSixVQUFBQSxLQUFLLENBQUN6RyxJQUFOLENBQVcsSUFBSXBELGVBQUosQ0FBb0I7QUFDM0JJLFlBQUFBLE9BQU8sRUFBRWdLLE9BRGtCO0FBRTNCOUosWUFBQUEsWUFBWSxFQUFFaUssV0FGYTtBQUczQi9KLFlBQUFBLFVBQVUsRUFBRWdLO0FBSGUsV0FBcEIsQ0FBWDtBQUtILFNBVEQsQ0FTRSxPQUFPdkosQ0FBUCxFQUFVO0FBQ1J1RixVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxrQ0FBa0MyRCxPQUFoRDtBQUNBNUQsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWN4RixDQUFkO0FBQ0E2SSxVQUFBQSxNQUFNLENBQUMxRyxJQUFQLENBQVlnSCxPQUFaO0FBQ0g7QUFDSjs7QUFFRCxVQUFJTixNQUFNLENBQUN2RyxNQUFQLEdBQWdCLENBQXBCLEVBQXVCO0FBQ25CLGNBQU1rSCxjQUFjLEdBQUdoSixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCOztBQUNBZ0osdUJBQU1DLG1CQUFOLENBQTBCLG1CQUExQixFQUErQyxFQUEvQyxFQUFtREYsY0FBbkQsRUFBbUU7QUFDL0RHLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxvQ0FBSCxDQUR3RDtBQUUvREMsVUFBQUEsV0FBVyxFQUFFLHlCQUNULHlGQURTLEVBRVQ7QUFBQ0MsWUFBQUEsUUFBUSxFQUFFaEIsTUFBTSxDQUFDaUIsSUFBUCxDQUFZLElBQVo7QUFBWCxXQUZTLENBRmtEO0FBTS9EQyxVQUFBQSxNQUFNLEVBQUUseUJBQUcsSUFBSDtBQU51RCxTQUFuRTtBQVFIOztBQUVELFdBQUs3RyxRQUFMLENBQWM7QUFBQ0UsUUFBQUEsT0FBTyxFQUFFLENBQUMsR0FBRyxLQUFLMEMsS0FBTCxDQUFXMUMsT0FBZixFQUF3QixHQUFHd0YsS0FBM0I7QUFBVixPQUFkO0FBQ0gsS0E3akJrQjtBQUFBLDZEQStqQkU1SSxDQUFELElBQU87QUFDdkI7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDQyxjQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsZUFBRjs7QUFFQSxVQUFJLEtBQUs4SixVQUFMLElBQW1CLEtBQUtBLFVBQUwsQ0FBZ0JDLE9BQXZDLEVBQWdEO0FBQzVDLGFBQUtELFVBQUwsQ0FBZ0JDLE9BQWhCLENBQXdCQyxLQUF4QjtBQUNIO0FBQ0osS0F2a0JrQjtBQUFBLDRFQXlrQmlCbEssQ0FBRCxJQUFPO0FBQ3RDQSxNQUFBQSxDQUFDLENBQUNDLGNBQUYsR0FEc0MsQ0FHdEM7QUFDQTs7QUFDQTtBQUNBLFdBQUtpRCxRQUFMLENBQWM7QUFBQ21FLFFBQUFBLG9CQUFvQixFQUFFLElBQXZCO0FBQTZCQyxRQUFBQSxvQkFBb0IsRUFBRTtBQUFuRCxPQUFkO0FBQ0gsS0FobEJrQjtBQUFBLGtFQWtsQk90SCxDQUFELElBQU87QUFDNUJBLE1BQUFBLENBQUMsQ0FBQ0MsY0FBRjs7QUFDQTRELDBCQUFJQyxRQUFKLENBQWE7QUFBRUMsUUFBQUEsTUFBTSxFQUFFO0FBQVYsT0FBYjs7QUFDQSxXQUFLNUQsS0FBTCxDQUFXaUUsVUFBWDtBQUNILEtBdGxCa0I7O0FBR2YsUUFBSWpFLEtBQUssQ0FBQ2dLLElBQU4sS0FBZTVMLFdBQWYsSUFBOEIsQ0FBQzRCLEtBQUssQ0FBQzhELE1BQXpDLEVBQWlEO0FBQzdDLFlBQU0sSUFBSXJGLEtBQUosQ0FBVSxpRUFBVixDQUFOO0FBQ0g7O0FBRUQsVUFBTXdMLGNBQWMsR0FBRyxJQUFJQyxHQUFKLENBQVEsQ0FBQ3ZKLGlDQUFnQkMsR0FBaEIsR0FBc0JpRSxTQUF0QixFQUFELEVBQW9Dc0YsbUJBQVV2SixHQUFWLEdBQWdCLGVBQWhCLENBQXBDLENBQVIsQ0FBdkI7O0FBQ0EsUUFBSVosS0FBSyxDQUFDOEQsTUFBVixFQUFrQjtBQUNkLFlBQU15QixJQUFJLEdBQUc1RSxpQ0FBZ0JDLEdBQWhCLEdBQXNCNEUsT0FBdEIsQ0FBOEJ4RixLQUFLLENBQUM4RCxNQUFwQyxDQUFiOztBQUNBLFVBQUksQ0FBQ3lCLElBQUwsRUFBVyxNQUFNLElBQUk5RyxLQUFKLENBQVUseURBQVYsQ0FBTjtBQUNYOEcsTUFBQUEsSUFBSSxDQUFDNkUsd0JBQUwsQ0FBOEIsUUFBOUIsRUFBd0NDLE9BQXhDLENBQWdEbkIsQ0FBQyxJQUFJZSxjQUFjLENBQUNLLEdBQWYsQ0FBbUJwQixDQUFDLENBQUN4SyxNQUFyQixDQUFyRDtBQUNBNkcsTUFBQUEsSUFBSSxDQUFDNkUsd0JBQUwsQ0FBOEIsTUFBOUIsRUFBc0NDLE9BQXRDLENBQThDbkIsQ0FBQyxJQUFJZSxjQUFjLENBQUNLLEdBQWYsQ0FBbUJwQixDQUFDLENBQUN4SyxNQUFyQixDQUFuRCxFQUpjLENBS2Q7O0FBQ0E2RyxNQUFBQSxJQUFJLENBQUM2RSx3QkFBTCxDQUE4QixLQUE5QixFQUFxQ0MsT0FBckMsQ0FBNkNuQixDQUFDLElBQUllLGNBQWMsQ0FBQ0ssR0FBZixDQUFtQnBCLENBQUMsQ0FBQ3hLLE1BQXJCLENBQWxEO0FBQ0g7O0FBRUQsU0FBS2lILEtBQUwsR0FBYTtBQUNUMUMsTUFBQUEsT0FBTyxFQUFFLEVBREE7QUFDSTtBQUNibUQsTUFBQUEsVUFBVSxFQUFFLEVBRkg7QUFHVG1DLE1BQUFBLE9BQU8sRUFBRSxLQUFLZ0MsYUFBTCxDQUFtQk4sY0FBbkIsQ0FIQTtBQUlUakMsTUFBQUEsZUFBZSxFQUFFM0osbUJBSlI7QUFLVG1LLE1BQUFBLFdBQVcsRUFBRSxLQUFLZ0MsaUJBQUwsQ0FBdUJQLGNBQXZCLENBTEo7QUFNVGhDLE1BQUFBLG1CQUFtQixFQUFFNUosbUJBTlo7QUFPVDBJLE1BQUFBLGtCQUFrQixFQUFFLEVBUFg7QUFPZTtBQUN4Qk8sTUFBQUEsb0JBQW9CLEVBQUUsRUFSYjtBQVFpQjtBQUMxQkosTUFBQUEsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDdkcsaUNBQWdCQyxHQUFoQixHQUFzQjZKLG9CQUF0QixFQVRmO0FBVVR0RCxNQUFBQSxvQkFBb0IsRUFBRSxLQVZiO0FBWVQ7QUFDQW5FLE1BQUFBLElBQUksRUFBRSxLQWJHO0FBY1RzQyxNQUFBQSxTQUFTLEVBQUU7QUFkRixLQUFiO0FBaUJBLFNBQUt1RSxVQUFMLEdBQWtCLHVCQUFsQjtBQUNIOztBQUVEVSxFQUFBQSxhQUFhLENBQUNHO0FBQUQ7QUFBQTtBQUFBO0FBQXlGO0FBQ2xHLFVBQU1DLEtBQUssR0FBR3BILG1CQUFVQyxNQUFWLEdBQW1Cb0gsNkJBQW5CLEVBQWQsQ0FEa0csQ0FDaEM7QUFFbEU7QUFDQTs7O0FBQ0EsVUFBTUMsV0FBVyxHQUFHQyx1QkFBY0MsWUFBZCxFQUFwQjs7QUFDQSxVQUFNQyxhQUFhLEdBQUdILFdBQVcsQ0FBQ0kscUJBQUQsQ0FBakM7O0FBQ0EsVUFBTUMsUUFBUSxHQUFHdkssaUNBQWdCQyxHQUFoQixHQUFzQmlFLFNBQXRCLEVBQWpCOztBQUNBLFNBQUssTUFBTXNHLE1BQVgsSUFBcUJILGFBQXJCLEVBQW9DO0FBQ2hDLFlBQU1JLFlBQVksR0FBR0QsTUFBTSxDQUFDRSxnQkFBUCxHQUEwQnRDLE1BQTFCLENBQWlDL0IsQ0FBQyxJQUFJQSxDQUFDLENBQUN0SSxNQUFGLEtBQWF3TSxRQUFuRCxDQUFyQjs7QUFDQSxXQUFLLE1BQU1oTCxNQUFYLElBQXFCa0wsWUFBckIsRUFBbUM7QUFDL0IsWUFBSVQsS0FBSyxDQUFDekssTUFBTSxDQUFDeEIsTUFBUixDQUFULEVBQTBCLFNBREssQ0FDSzs7QUFFcEMwRyxRQUFBQSxPQUFPLENBQUMwQixJQUFSLDhCQUFtQzVHLE1BQU0sQ0FBQ3hCLE1BQTFDLGlCQUF1RHlNLE1BQU0sQ0FBQ3JILE1BQTlEO0FBQ0E2RyxRQUFBQSxLQUFLLENBQUN6SyxNQUFNLENBQUN4QixNQUFSLENBQUwsR0FBdUJ5TSxNQUF2QjtBQUNIO0FBQ0o7O0FBRUQsVUFBTTVDLE9BQU8sR0FBRyxFQUFoQjs7QUFDQSxTQUFLLE1BQU03SixNQUFYLElBQXFCaU0sS0FBckIsRUFBNEI7QUFDeEI7QUFDQSxVQUFJRCxpQkFBaUIsQ0FBQ1ksR0FBbEIsQ0FBc0I1TSxNQUF0QixDQUFKLEVBQW1DO0FBQy9CMEcsUUFBQUEsT0FBTyxDQUFDMEIsSUFBUixzQ0FBMkNwSSxNQUEzQztBQUNBO0FBQ0g7O0FBRUQsWUFBTTZHLElBQUksR0FBR29GLEtBQUssQ0FBQ2pNLE1BQUQsQ0FBbEI7QUFDQSxZQUFNd0IsTUFBTSxHQUFHcUYsSUFBSSxDQUFDZ0csU0FBTCxDQUFlN00sTUFBZixDQUFmOztBQUNBLFVBQUksQ0FBQ3dCLE1BQUwsRUFBYTtBQUNUO0FBQ0FrRixRQUFBQSxPQUFPLENBQUMwQixJQUFSLDRCQUFpQ3BJLE1BQWpDLDBEQUF1RjZHLElBQUksQ0FBQ3pCLE1BQTVGO0FBQ0E7QUFDSCxPQWJ1QixDQWV4Qjs7O0FBQ0EsWUFBTTBILFdBQVcsR0FBRyxDQUFDLGdCQUFELEVBQW1CLGtCQUFuQixFQUF1QyxXQUF2QyxDQUFwQjtBQUNBLFlBQU1DLGVBQWUsR0FBRyxFQUF4QixDQWpCd0IsQ0FpQkk7O0FBQzVCLFVBQUlDLFdBQVcsR0FBRyxDQUFsQjs7QUFDQSxVQUFJbkcsSUFBSSxDQUFDb0csUUFBTCxJQUFpQnBHLElBQUksQ0FBQ29HLFFBQUwsQ0FBY3hKLE1BQW5DLEVBQTJDO0FBQ3ZDLGFBQUssSUFBSU4sQ0FBQyxHQUFHMEQsSUFBSSxDQUFDb0csUUFBTCxDQUFjeEosTUFBZCxHQUF1QixDQUFwQyxFQUF1Q04sQ0FBQyxJQUFJLENBQTVDLEVBQStDQSxDQUFDLEVBQWhELEVBQW9EO0FBQ2hELGdCQUFNK0osRUFBRSxHQUFHckcsSUFBSSxDQUFDb0csUUFBTCxDQUFjOUosQ0FBZCxDQUFYOztBQUNBLGNBQUkySixXQUFXLENBQUMvTCxRQUFaLENBQXFCbU0sRUFBRSxDQUFDQyxPQUFILEVBQXJCLENBQUosRUFBd0M7QUFDcENILFlBQUFBLFdBQVcsR0FBR0UsRUFBRSxDQUFDRSxLQUFILEVBQWQ7QUFDQTtBQUNIOztBQUNELGNBQUl2RyxJQUFJLENBQUNvRyxRQUFMLENBQWN4SixNQUFkLEdBQXVCTixDQUF2QixHQUEyQjRKLGVBQS9CLEVBQWdEO0FBQ25EO0FBQ0o7O0FBQ0QsVUFBSSxDQUFDQyxXQUFMLEVBQWtCO0FBQ2Q7QUFDQXRHLFFBQUFBLE9BQU8sQ0FBQzBCLElBQVIsNEJBQWlDcEksTUFBakMsZUFBNEM2RyxJQUFJLENBQUN6QixNQUFqRCwyQ0FBd0Y0SCxXQUF4RjtBQUNBO0FBQ0g7O0FBRURuRCxNQUFBQSxPQUFPLENBQUN2RyxJQUFSLENBQWE7QUFBQ3RELFFBQUFBLE1BQUQ7QUFBU3VJLFFBQUFBLElBQUksRUFBRS9HLE1BQWY7QUFBdUI2TCxRQUFBQSxVQUFVLEVBQUVMO0FBQW5DLE9BQWI7QUFDSDs7QUFDRCxRQUFJLENBQUNuRCxPQUFMLEVBQWNuRCxPQUFPLENBQUMwQixJQUFSLENBQWEseUNBQWIsRUF4RG9GLENBMERsRzs7QUFDQXlCLElBQUFBLE9BQU8sQ0FBQ3lELElBQVIsQ0FBYSxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUEsQ0FBQyxDQUFDSCxVQUFGLEdBQWVFLENBQUMsQ0FBQ0YsVUFBeEM7QUFFQSxXQUFPeEQsT0FBUDtBQUNIOztBQUVEaUMsRUFBQUEsaUJBQWlCLENBQUNFO0FBQUQ7QUFBQTtBQUFBO0FBQXFFO0FBQ2xGLFVBQU15QixvQkFBb0IsR0FBRyxHQUE3Qjs7QUFDQSxVQUFNQyxXQUFXLEdBQUd6TCxpQ0FBZ0JDLEdBQWhCLEdBQXNCeUwsUUFBdEIsR0FDZnRELE1BRGUsQ0FDUnRDLENBQUMsSUFBSUEsQ0FBQyxDQUFDNkYsZUFBRixPQUF3QixNQUF4QixJQUFrQzdGLENBQUMsQ0FBQzhGLG9CQUFGLE1BQTRCSixvQkFEM0QsQ0FBcEIsQ0FGa0YsQ0FLbEY7OztBQUNBLFVBQU1LLFdBQVcsR0FBR0osV0FBVyxDQUFDSyxNQUFaLENBQW1CLENBQUNDLE9BQUQsRUFBVW5ILElBQVYsS0FBbUI7QUFDdEQ7QUFDQSxVQUFJaEMsbUJBQVVDLE1BQVYsR0FBbUJtSixrQkFBbkIsQ0FBc0NwSCxJQUFJLENBQUN6QixNQUEzQyxDQUFKLEVBQXdEO0FBQ3BELGVBQU80SSxPQUFQLENBRG9ELENBQ3BDO0FBQ25COztBQUVELFlBQU1FLGFBQWEsR0FBR3JILElBQUksQ0FBQzhGLGdCQUFMLEdBQXdCdEMsTUFBeEIsQ0FBK0IvQixDQUFDLElBQUksQ0FBQzBELGlCQUFpQixDQUFDWSxHQUFsQixDQUFzQnRFLENBQUMsQ0FBQ3RJLE1BQXhCLENBQXJDLENBQXRCOztBQUNBLFdBQUssTUFBTXdCLE1BQVgsSUFBcUIwTSxhQUFyQixFQUFvQztBQUNoQztBQUNBLFlBQUlsQyxpQkFBaUIsQ0FBQ1ksR0FBbEIsQ0FBc0JwTCxNQUFNLENBQUN4QixNQUE3QixDQUFKLEVBQTBDO0FBQ3RDO0FBQ0g7O0FBRUQsWUFBSSxDQUFDZ08sT0FBTyxDQUFDeE0sTUFBTSxDQUFDeEIsTUFBUixDQUFaLEVBQTZCO0FBQ3pCZ08sVUFBQUEsT0FBTyxDQUFDeE0sTUFBTSxDQUFDeEIsTUFBUixDQUFQLEdBQXlCO0FBQ3JCd0IsWUFBQUEsTUFBTSxFQUFFQSxNQURhO0FBRXJCO0FBQ0E7QUFDQTJNLFlBQUFBLG9CQUFvQixFQUFFdEgsSUFBSSxDQUFDZ0gsb0JBQUwsRUFKRDtBQUtyQjVCLFlBQUFBLEtBQUssRUFBRTtBQUxjLFdBQXpCO0FBT0g7O0FBRUQrQixRQUFBQSxPQUFPLENBQUN4TSxNQUFNLENBQUN4QixNQUFSLENBQVAsQ0FBdUJpTSxLQUF2QixDQUE2QjNJLElBQTdCLENBQWtDdUQsSUFBbEM7O0FBRUEsWUFBSUEsSUFBSSxDQUFDZ0gsb0JBQUwsS0FBOEJHLE9BQU8sQ0FBQ3hNLE1BQU0sQ0FBQ3hCLE1BQVIsQ0FBUCxDQUF1Qm1PLG9CQUF6RCxFQUErRTtBQUMzRUgsVUFBQUEsT0FBTyxDQUFDeE0sTUFBTSxDQUFDeEIsTUFBUixDQUFQLENBQXVCd0IsTUFBdkIsR0FBZ0NBLE1BQWhDO0FBQ0F3TSxVQUFBQSxPQUFPLENBQUN4TSxNQUFNLENBQUN4QixNQUFSLENBQVAsQ0FBdUJtTyxvQkFBdkIsR0FBOEN0SCxJQUFJLENBQUNnSCxvQkFBTCxFQUE5QztBQUNIO0FBQ0o7O0FBQ0QsYUFBT0csT0FBUDtBQUNILEtBL0JtQixFQStCakIsRUEvQmlCLENBQXBCLENBTmtGLENBdUNsRjs7QUFDQSxVQUFNSSxZQUFZLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjUixXQUFkLEVBQTJCQyxNQUEzQixDQUFrQyxDQUFDUSxNQUFELEVBQVNDLEtBQVQsS0FBbUI7QUFDdEUsWUFBTUMsZUFBZSxHQUFHRCxLQUFLLENBQUN2QyxLQUFOLENBQVk4QixNQUFaLENBQW1CLENBQUNXLENBQUQsRUFBSTNHLENBQUosS0FBVTJHLENBQUMsR0FBRzNHLENBQUMsQ0FBQzhGLG9CQUFGLEVBQWpDLEVBQTJELENBQTNELENBQXhCO0FBQ0EsWUFBTWMsUUFBUSxHQUFHbEIsb0JBQW9CLEdBQUdlLEtBQUssQ0FBQ3ZDLEtBQU4sQ0FBWXhJLE1BQXBEO0FBQ0E4SyxNQUFBQSxNQUFNLENBQUNDLEtBQUssQ0FBQ2hOLE1BQU4sQ0FBYXhCLE1BQWQsQ0FBTixHQUE4QjtBQUMxQndCLFFBQUFBLE1BQU0sRUFBRWdOLEtBQUssQ0FBQ2hOLE1BRFk7QUFFMUJvTixRQUFBQSxRQUFRLEVBQUVKLEtBQUssQ0FBQ3ZDLEtBQU4sQ0FBWXhJLE1BRkk7QUFHMUJvTCxRQUFBQSxLQUFLLEVBQUVDLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBWUQsSUFBSSxDQUFDRSxHQUFMLENBQVMsSUFBS1AsZUFBZSxHQUFHRSxRQUFoQyxFQUEyQyxDQUEzQyxDQUFaO0FBSG1CLE9BQTlCO0FBS0EsYUFBT0osTUFBUDtBQUNILEtBVG9CLEVBU2xCLEVBVGtCLENBQXJCLENBeENrRixDQW1EbEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFNVSxlQUFlLEdBQUdoTixpQ0FBZ0JDLEdBQWhCLEdBQXNCeUwsUUFBdEIsR0FBaUN0RCxNQUFqQyxDQUF3Q3RDLENBQUMsSUFBSUEsQ0FBQyxDQUFDNkYsZUFBRixPQUF3QixNQUFyRSxDQUF4Qjs7QUFDQSxVQUFNc0IsR0FBRyxHQUFJLElBQUlDLElBQUosRUFBRCxDQUFhQyxPQUFiLEVBQVo7QUFDQSxVQUFNQyxxQkFBcUIsR0FBR0gsR0FBRyxHQUFJLEtBQUssRUFBTCxHQUFVLElBQS9DLENBMURrRixDQTBENUI7O0FBQ3RELFVBQU1JLHFCQUFxQixHQUFHLEVBQTlCLENBM0RrRixDQTJEaEQ7O0FBQ2xDLFVBQU1DLFNBQVMsR0FBRyxFQUFsQixDQTVEa0YsQ0E0RDVEOztBQUN0QixVQUFNQyxnQkFBZ0IsR0FBRyxFQUF6QixDQTdEa0YsQ0E2RHJEOztBQUM3QixTQUFLLE1BQU0zSSxJQUFYLElBQW1Cb0ksZUFBbkIsRUFBb0M7QUFDaEM7QUFDQSxZQUFNUSxJQUFJLEdBQUc1SyxtQkFBVUMsTUFBVixHQUFtQm1KLGtCQUFuQixDQUFzQ3BILElBQUksQ0FBQ3pCLE1BQTNDLENBQWI7O0FBQ0EsVUFBSWlKLE1BQU0sQ0FBQ3FCLElBQVAsQ0FBWTdJLElBQUksQ0FBQzhJLElBQWpCLEVBQXVCNU8sUUFBdkIsQ0FBZ0MsZUFBaEMsS0FBb0QwTyxJQUF4RCxFQUE4RDtBQUMxRDtBQUNIOztBQUVELFlBQU1HLE1BQU0sR0FBRy9JLElBQUksQ0FBQ2dKLGVBQUwsR0FBdUJDLFNBQXZCLEVBQWYsQ0FQZ0MsQ0FPbUI7O0FBQ25ELFdBQUssSUFBSTNNLENBQUMsR0FBR3lNLE1BQU0sQ0FBQ25NLE1BQVAsR0FBZ0IsQ0FBN0IsRUFBZ0NOLENBQUMsSUFBSTJMLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBWWEsTUFBTSxDQUFDbk0sTUFBUCxHQUFnQjZMLHFCQUE1QixDQUFyQyxFQUF5Rm5NLENBQUMsRUFBMUYsRUFBOEY7QUFDMUYsY0FBTStKLEVBQUUsR0FBRzBDLE1BQU0sQ0FBQ3pNLENBQUQsQ0FBakI7O0FBQ0EsWUFBSTZJLGlCQUFpQixDQUFDWSxHQUFsQixDQUFzQk0sRUFBRSxDQUFDNkMsU0FBSCxFQUF0QixDQUFKLEVBQTJDO0FBQ3ZDO0FBQ0g7O0FBQ0QsWUFBSTdDLEVBQUUsQ0FBQ0UsS0FBSCxNQUFjaUMscUJBQWxCLEVBQXlDO0FBQ3JDLGdCQURxQyxDQUM5QjtBQUNWOztBQUVELFlBQUksQ0FBQ0UsU0FBUyxDQUFDckMsRUFBRSxDQUFDNkMsU0FBSCxFQUFELENBQVYsSUFBOEJSLFNBQVMsQ0FBQ3JDLEVBQUUsQ0FBQzZDLFNBQUgsRUFBRCxDQUFULEdBQTRCN0MsRUFBRSxDQUFDRSxLQUFILEVBQTlELEVBQTBFO0FBQ3RFbUMsVUFBQUEsU0FBUyxDQUFDckMsRUFBRSxDQUFDNkMsU0FBSCxFQUFELENBQVQsR0FBNEI3QyxFQUFFLENBQUNFLEtBQUgsRUFBNUI7QUFDQW9DLFVBQUFBLGdCQUFnQixDQUFDdEMsRUFBRSxDQUFDNkMsU0FBSCxFQUFELENBQWhCLEdBQW1DbEosSUFBSSxDQUFDZ0csU0FBTCxDQUFlSyxFQUFFLENBQUM2QyxTQUFILEVBQWYsQ0FBbkM7QUFDSDtBQUNKO0FBQ0o7O0FBQ0QsU0FBSyxNQUFNL1AsTUFBWCxJQUFxQnVQLFNBQXJCLEVBQWdDO0FBQzVCLFlBQU1TLEVBQUUsR0FBR1QsU0FBUyxDQUFDdlAsTUFBRCxDQUFwQjtBQUNBLFlBQU13QixNQUFNLEdBQUdnTyxnQkFBZ0IsQ0FBQ3hQLE1BQUQsQ0FBL0I7QUFDQSxVQUFJLENBQUN3QixNQUFMLEVBQWEsU0FIZSxDQUdMO0FBRXZCO0FBQ0E7QUFDQTs7QUFDQSxZQUFNeU8sZUFBZSxHQUFHbkIsSUFBSSxDQUFDb0IsR0FBTCxDQUFTaEIsR0FBRyxHQUFHYyxFQUFmLENBQXhCLENBUjRCLENBUWdCOztBQUM1QyxZQUFNRyxXQUFXLEdBQUlqQixHQUFHLEdBQUdHLHFCQUFQLEdBQWdDWSxlQUFwRDtBQUNBLFlBQU1HLFVBQVUsR0FBR3RCLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBWW9CLFdBQVcsSUFBSSxLQUFLLEVBQUwsR0FBVSxJQUFkLENBQXZCLENBQW5CLENBVjRCLENBVW9DOztBQUVoRSxVQUFJRSxNQUFNLEdBQUdqQyxZQUFZLENBQUNwTyxNQUFELENBQXpCO0FBQ0EsVUFBSSxDQUFDcVEsTUFBTCxFQUFhQSxNQUFNLEdBQUdqQyxZQUFZLENBQUNwTyxNQUFELENBQVosR0FBdUI7QUFBQzZPLFFBQUFBLEtBQUssRUFBRTtBQUFSLE9BQWhDO0FBQ2J3QixNQUFBQSxNQUFNLENBQUM3TyxNQUFQLEdBQWdCQSxNQUFoQjtBQUNBNk8sTUFBQUEsTUFBTSxDQUFDeEIsS0FBUCxJQUFnQnVCLFVBQWhCO0FBQ0g7O0FBRUQsVUFBTXBDLE9BQU8sR0FBR0ssTUFBTSxDQUFDQyxNQUFQLENBQWNGLFlBQWQsQ0FBaEI7QUFDQUosSUFBQUEsT0FBTyxDQUFDVixJQUFSLENBQWEsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVU7QUFDbkIsVUFBSUQsQ0FBQyxDQUFDc0IsS0FBRixLQUFZckIsQ0FBQyxDQUFDcUIsS0FBbEIsRUFBeUI7QUFDckIsWUFBSXRCLENBQUMsQ0FBQ3FCLFFBQUYsS0FBZXBCLENBQUMsQ0FBQ29CLFFBQXJCLEVBQStCO0FBQzNCLGlCQUFPckIsQ0FBQyxDQUFDL0wsTUFBRixDQUFTeEIsTUFBVCxDQUFnQnNRLGFBQWhCLENBQThCOUMsQ0FBQyxDQUFDaE0sTUFBRixDQUFTeEIsTUFBdkMsQ0FBUDtBQUNIOztBQUVELGVBQU93TixDQUFDLENBQUNvQixRQUFGLEdBQWFyQixDQUFDLENBQUNxQixRQUF0QjtBQUNIOztBQUNELGFBQU9wQixDQUFDLENBQUNxQixLQUFGLEdBQVV0QixDQUFDLENBQUNzQixLQUFuQjtBQUNILEtBVEQ7QUFXQSxXQUFPYixPQUFPLENBQUN0SixHQUFSLENBQVk4RixDQUFDLEtBQUs7QUFBQ3hLLE1BQUFBLE1BQU0sRUFBRXdLLENBQUMsQ0FBQ2hKLE1BQUYsQ0FBU3hCLE1BQWxCO0FBQTBCdUksTUFBQUEsSUFBSSxFQUFFaUMsQ0FBQyxDQUFDaEo7QUFBbEMsS0FBTCxDQUFiLENBQVA7QUFDSDs7QUFFRDhFLEVBQUFBLDRCQUE0QixDQUFDcEQsTUFBRDtBQUFBO0FBQWtCO0FBQzFDLFVBQU1xTixXQUFXLEdBQUdsQyxNQUFNLENBQUNxQixJQUFQLENBQVl4TSxNQUFNLENBQUNzTixNQUFuQixFQUEyQm5HLE1BQTNCLENBQWtDa0QsQ0FBQyxJQUFJckssTUFBTSxDQUFDc04sTUFBUCxDQUFjakQsQ0FBZCxNQUFxQixPQUE1RCxDQUFwQjs7QUFDQSxRQUFJZ0QsV0FBVyxDQUFDOU0sTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QmlELE1BQUFBLE9BQU8sQ0FBQytKLEdBQVIsQ0FBWSwwQkFBWixFQUF3Q3ZOLE1BQXhDO0FBQ0EsV0FBS21CLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWc0MsUUFBQUEsU0FBUyxFQUFFLHlCQUFHLDREQUFILEVBQWlFO0FBQ3hFOEosVUFBQUEsUUFBUSxFQUFFSCxXQUFXLENBQUN0RixJQUFaLENBQWlCLElBQWpCO0FBRDhELFNBQWpFO0FBRkQsT0FBZDtBQU1BLGFBQU8sSUFBUCxDQVJ3QixDQVFYO0FBQ2hCOztBQUNELFdBQU8sS0FBUDtBQUNIOztBQUVEekcsRUFBQUEsY0FBYztBQUFBO0FBQWE7QUFDdkI7QUFDQSxRQUFJLENBQUMsS0FBS3lDLEtBQUwsQ0FBV1MsVUFBWixJQUEwQixDQUFDLEtBQUtULEtBQUwsQ0FBV1MsVUFBWCxDQUFzQjNHLFFBQXRCLENBQStCLEdBQS9CLENBQS9CLEVBQW9FLE9BQU8sS0FBS2tHLEtBQUwsQ0FBVzFDLE9BQVgsSUFBc0IsRUFBN0I7QUFFcEUsUUFBSW9NO0FBQWlCO0FBQXJCOztBQUNBLFFBQUksS0FBSzFKLEtBQUwsQ0FBV1MsVUFBWCxDQUFzQmtKLFVBQXRCLENBQWlDLEdBQWpDLENBQUosRUFBMkM7QUFDdkM7QUFDQUQsTUFBQUEsU0FBUyxHQUFHLElBQUl6USxlQUFKLENBQW9CO0FBQUNJLFFBQUFBLE9BQU8sRUFBRSxLQUFLMkcsS0FBTCxDQUFXUyxVQUFyQjtBQUFpQ2xILFFBQUFBLFlBQVksRUFBRSxJQUEvQztBQUFxREUsUUFBQUEsVUFBVSxFQUFFO0FBQWpFLE9BQXBCLENBQVo7QUFDSCxLQUhELE1BR087QUFDSDtBQUNBaVEsTUFBQUEsU0FBUyxHQUFHLElBQUloUSxjQUFKLENBQW1CLEtBQUtzRyxLQUFMLENBQVdTLFVBQTlCLENBQVo7QUFDSDs7QUFDRCxVQUFNbUosVUFBVSxHQUFHLENBQUMsSUFBSSxLQUFLNUosS0FBTCxDQUFXMUMsT0FBWCxJQUFzQixFQUExQixDQUFELEVBQWdDb00sU0FBaEMsQ0FBbkI7QUFDQSxTQUFLdE0sUUFBTCxDQUFjO0FBQUNFLE1BQUFBLE9BQU8sRUFBRXNNLFVBQVY7QUFBc0JuSixNQUFBQSxVQUFVLEVBQUU7QUFBbEMsS0FBZDtBQUNBLFdBQU9tSixVQUFQO0FBQ0g7O0FBK1ZEQyxFQUFBQSxjQUFjLENBQUN4RjtBQUFEO0FBQUEsSUFBZ0M7QUFDMUMsUUFBSXlGLGFBQWEsR0FBR3pGLElBQUksS0FBSyxTQUFULEdBQXFCLEtBQUtyRSxLQUFMLENBQVc0QyxPQUFoQyxHQUEwQyxLQUFLNUMsS0FBTCxDQUFXNkMsV0FBekU7QUFDQSxRQUFJa0gsT0FBTyxHQUFHMUYsSUFBSSxLQUFLLFNBQVQsR0FBcUIsS0FBS3JFLEtBQUwsQ0FBV3FDLGVBQWhDLEdBQWtELEtBQUtyQyxLQUFMLENBQVdzQyxtQkFBM0U7QUFDQSxVQUFNMEgsVUFBVSxHQUFHM0YsSUFBSSxLQUFLLFNBQVQsR0FBcUIsS0FBSzRGLGdCQUFMLENBQXNCQyxJQUF0QixDQUEyQixJQUEzQixDQUFyQixHQUF3RCxLQUFLQyxvQkFBTCxDQUEwQkQsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBM0U7O0FBQ0EsVUFBTTlELFVBQVUsR0FBSTdDLENBQUQsSUFBT2MsSUFBSSxLQUFLLFNBQVQsR0FBcUJkLENBQUMsQ0FBQzZDLFVBQXZCLEdBQW9DLElBQTlEOztBQUNBLFFBQUlnRSxXQUFXLEdBQUcvRixJQUFJLEtBQUssU0FBVCxHQUFxQix5QkFBRyxzQkFBSCxDQUFyQixHQUFrRCx5QkFBRyxhQUFILENBQXBFOztBQUVBLFFBQUksS0FBS2hLLEtBQUwsQ0FBV2dLLElBQVgsS0FBb0I1TCxXQUF4QixFQUFxQztBQUNqQzJSLE1BQUFBLFdBQVcsR0FBRy9GLElBQUksS0FBSyxTQUFULEdBQXFCLHlCQUFHLDBCQUFILENBQXJCLEdBQXNELHlCQUFHLGFBQUgsQ0FBcEU7QUFDSCxLQVR5QyxDQVcxQztBQUNBO0FBQ0E7OztBQUNBLFFBQUlnRyx5QkFBeUIsR0FBRyxFQUFoQyxDQWQwQyxDQWNOOztBQUNwQyxRQUFJQyxzQkFBc0IsR0FBRyxFQUE3QixDQWYwQyxDQWVUOztBQUNqQyxVQUFNQyxTQUFTLEdBQUcsS0FBS3ZLLEtBQUwsQ0FBV29CLGtCQUFYLElBQWlDLEtBQUtwQixLQUFMLENBQVcyQixvQkFBOUQ7O0FBQ0EsUUFBSSxLQUFLM0IsS0FBTCxDQUFXUyxVQUFYLElBQXlCOEosU0FBekIsSUFBc0NsRyxJQUFJLEtBQUssYUFBbkQsRUFBa0U7QUFDOUQ7QUFDQSxZQUFNbUcsZ0JBQWdCLEdBQUcsQ0FBQ25KO0FBQUQ7QUFBQTtBQUFBO0FBQXdCO0FBQzdDLGVBQU8sQ0FBQ3lJLGFBQWEsQ0FBQ1csSUFBZCxDQUFtQmxILENBQUMsSUFBSUEsQ0FBQyxDQUFDeEssTUFBRixLQUFhc0ksQ0FBQyxDQUFDdEksTUFBdkMsQ0FBRCxJQUNBLENBQUNzUix5QkFBeUIsQ0FBQ0ksSUFBMUIsQ0FBK0JsSCxDQUFDLElBQUlBLENBQUMsQ0FBQ3hLLE1BQUYsS0FBYXNJLENBQUMsQ0FBQ3RJLE1BQW5ELENBREQsSUFFQSxDQUFDdVIsc0JBQXNCLENBQUNHLElBQXZCLENBQTRCbEgsQ0FBQyxJQUFJQSxDQUFDLENBQUN4SyxNQUFGLEtBQWFzSSxDQUFDLENBQUN0SSxNQUFoRCxDQUZSO0FBR0gsT0FKRDs7QUFNQXVSLE1BQUFBLHNCQUFzQixHQUFHLEtBQUt0SyxLQUFMLENBQVdvQixrQkFBWCxDQUE4QmdDLE1BQTlCLENBQXFDb0gsZ0JBQXJDLENBQXpCO0FBQ0FILE1BQUFBLHlCQUF5QixHQUFHLEtBQUtySyxLQUFMLENBQVcyQixvQkFBWCxDQUFnQ3lCLE1BQWhDLENBQXVDb0gsZ0JBQXZDLENBQTVCO0FBQ0g7O0FBQ0QsVUFBTUUsb0JBQW9CLEdBQUdMLHlCQUF5QixDQUFDN04sTUFBMUIsR0FBbUMsQ0FBbkMsSUFBd0M4TixzQkFBc0IsQ0FBQzlOLE1BQXZCLEdBQWdDLENBQXJHLENBNUIwQyxDQThCMUM7O0FBQ0EsUUFBSXNOLGFBQWEsQ0FBQ3ROLE1BQWQsS0FBeUIsQ0FBekIsSUFBOEIsQ0FBQ2tPLG9CQUFuQyxFQUF5RCxPQUFPLElBQVAsQ0EvQmYsQ0FpQzFDOztBQUNBLFFBQUksS0FBSzFLLEtBQUwsQ0FBV1MsVUFBZixFQUEyQjtBQUN2QixZQUFNa0ssUUFBUSxHQUFHLEtBQUszSyxLQUFMLENBQVdTLFVBQVgsQ0FBc0IxRSxXQUF0QixFQUFqQjtBQUNBK04sTUFBQUEsYUFBYSxHQUFHQSxhQUFhLENBQ3hCMUcsTUFEVyxDQUNKRyxDQUFDLElBQUlBLENBQUMsQ0FBQ2pDLElBQUYsQ0FBT3pJLElBQVAsQ0FBWWtELFdBQVosR0FBMEJqQyxRQUExQixDQUFtQzZRLFFBQW5DLEtBQWdEcEgsQ0FBQyxDQUFDeEssTUFBRixDQUFTZ0QsV0FBVCxHQUF1QmpDLFFBQXZCLENBQWdDNlEsUUFBaEMsQ0FEakQsQ0FBaEI7O0FBR0EsVUFBSWIsYUFBYSxDQUFDdE4sTUFBZCxLQUF5QixDQUF6QixJQUE4QixDQUFDa08sb0JBQW5DLEVBQXlEO0FBQ3JELGVBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0kseUNBQUtOLFdBQUwsQ0FESixFQUVJLHdDQUFJLHlCQUFHLFlBQUgsQ0FBSixDQUZKLENBREo7QUFNSDtBQUNKLEtBL0N5QyxDQWlEMUM7QUFDQTs7O0FBQ0FOLElBQUFBLGFBQWEsR0FBRyxDQUFDLEdBQUdPLHlCQUFKLEVBQStCLEdBQUdQLGFBQWxDLEVBQWlELEdBQUdRLHNCQUFwRCxDQUFoQixDQW5EMEMsQ0FxRDFDO0FBQ0E7O0FBQ0EsUUFBSVAsT0FBTyxLQUFLRCxhQUFhLENBQUN0TixNQUFkLEdBQXVCLENBQXZDLEVBQTBDdU4sT0FBTyxHQXZEUCxDQXlEMUM7O0FBQ0EsVUFBTWEsUUFBUSxHQUFHZCxhQUFhLENBQUNlLEtBQWQsQ0FBb0IsQ0FBcEIsRUFBdUJkLE9BQXZCLENBQWpCO0FBQ0EsVUFBTWUsT0FBTyxHQUFHRixRQUFRLENBQUNwTyxNQUFULEdBQWtCc04sYUFBYSxDQUFDdE4sTUFBaEQ7QUFFQSxVQUFNNUIsZ0JBQWdCLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxRQUFJb1EsUUFBUSxHQUFHLElBQWY7O0FBQ0EsUUFBSUQsT0FBSixFQUFhO0FBQ1RDLE1BQUFBLFFBQVEsR0FDSiw2QkFBQyxnQkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRWYsVUFBM0I7QUFBdUMsUUFBQSxJQUFJLEVBQUM7QUFBNUMsU0FDSyx5QkFBRyxXQUFILENBREwsQ0FESjtBQUtIOztBQUVELFVBQU1nQixLQUFLLEdBQUdKLFFBQVEsQ0FBQ25OLEdBQVQsQ0FBYXFELENBQUMsSUFDeEIsNkJBQUMsVUFBRDtBQUNJLE1BQUEsTUFBTSxFQUFFQSxDQUFDLENBQUNRLElBRGQ7QUFFSSxNQUFBLFlBQVksRUFBRThFLFVBQVUsQ0FBQ3RGLENBQUQsQ0FGNUI7QUFHSSxNQUFBLEdBQUcsRUFBRUEsQ0FBQyxDQUFDL0gsTUFIWDtBQUlJLE1BQUEsUUFBUSxFQUFFLEtBQUtrUyxhQUpuQjtBQUtJLE1BQUEsYUFBYSxFQUFFLEtBQUtqTCxLQUFMLENBQVdTLFVBTDlCO0FBTUksTUFBQSxVQUFVLEVBQUUsS0FBS1QsS0FBTCxDQUFXMUMsT0FBWCxDQUFtQm1OLElBQW5CLENBQXdCL00sQ0FBQyxJQUFJQSxDQUFDLENBQUMzRSxNQUFGLEtBQWErSCxDQUFDLENBQUMvSCxNQUE1QztBQU5oQixNQURVLENBQWQ7QUFVQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLHlDQUFLcVIsV0FBTCxDQURKLEVBRUtZLEtBRkwsRUFHS0QsUUFITCxDQURKO0FBT0g7O0FBRURHLEVBQUFBLGFBQWEsR0FBRztBQUNaLFVBQU01TixPQUFPLEdBQUcsS0FBSzBDLEtBQUwsQ0FBVzFDLE9BQVgsQ0FBbUJHLEdBQW5CLENBQXVCQyxDQUFDLElBQ3BDLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLE1BQU0sRUFBRUEsQ0FBcEI7QUFBdUIsTUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLc0MsS0FBTCxDQUFXM0MsSUFBWixJQUFvQixLQUFLa0QsYUFBMUQ7QUFBeUUsTUFBQSxHQUFHLEVBQUU3QyxDQUFDLENBQUMzRTtBQUFoRixNQURZLENBQWhCOztBQUdBLFVBQU1vUyxLQUFLLEdBQ1A7QUFDSSxNQUFBLElBQUksRUFBRSxDQURWO0FBRUksTUFBQSxTQUFTLEVBQUUsS0FBS0MsVUFGcEI7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLQyxhQUhuQjtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUtyTCxLQUFMLENBQVdTLFVBSnRCO0FBS0ksTUFBQSxHQUFHLEVBQUUsS0FBS3lELFVBTGQ7QUFNSSxNQUFBLE9BQU8sRUFBRSxLQUFLb0gsUUFObEI7QUFPSSxNQUFBLFNBQVMsRUFBRSxJQVBmO0FBUUksTUFBQSxRQUFRLEVBQUUsS0FBS3RMLEtBQUwsQ0FBVzNDO0FBUnpCLE1BREo7O0FBWUEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHdCQUFmO0FBQXdDLE1BQUEsT0FBTyxFQUFFLEtBQUtrTztBQUF0RCxPQUNLak8sT0FETCxFQUVLNk4sS0FGTCxDQURKO0FBTUg7O0FBRURLLEVBQUFBLDRCQUE0QixHQUFHO0FBQzNCLFFBQUksQ0FBQyxLQUFLeEwsS0FBTCxDQUFXd0Isb0JBQVosSUFBb0MsS0FBS3hCLEtBQUwsQ0FBV3VCLG9CQUFuRCxFQUF5RTtBQUNyRSxhQUFPLElBQVA7QUFDSDs7QUFFRCxVQUFNa0ssd0JBQXdCLEdBQUcsdURBQWpDOztBQUNBLFFBQUlBLHdCQUFKLEVBQThCO0FBQzFCLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQXdELHlCQUNwRCxnREFDQSxxRUFEQSxHQUVBLDZDQUhvRCxFQUlwRDtBQUNJQyxRQUFBQSx5QkFBeUIsRUFBRSw2QkFBY0Qsd0JBQWQ7QUFEL0IsT0FKb0QsRUFPcEQ7QUFDSUUsUUFBQUEsT0FBTyxFQUFFQyxHQUFHLElBQUk7QUFBRyxVQUFBLElBQUksRUFBQyxHQUFSO0FBQVksVUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBMUIsV0FBNkRELEdBQTdELENBRHBCO0FBRUlFLFFBQUFBLFFBQVEsRUFBRUYsR0FBRyxJQUFJO0FBQUcsVUFBQSxJQUFJLEVBQUMsR0FBUjtBQUFZLFVBQUEsT0FBTyxFQUFFLEtBQUtHO0FBQTFCLFdBQW1ESCxHQUFuRDtBQUZyQixPQVBvRCxDQUF4RCxDQURKO0FBY0gsS0FmRCxNQWVPO0FBQ0gsYUFDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBd0QseUJBQ3BELGdEQUNBLDBDQUZvRCxFQUdwRCxFQUhvRCxFQUdoRDtBQUNBRSxRQUFBQSxRQUFRLEVBQUVGLEdBQUcsSUFBSTtBQUFHLFVBQUEsSUFBSSxFQUFDLEdBQVI7QUFBWSxVQUFBLE9BQU8sRUFBRSxLQUFLRztBQUExQixXQUFtREgsR0FBbkQ7QUFEakIsT0FIZ0QsQ0FBeEQsQ0FESjtBQVNIO0FBQ0o7O0FBRURwUixFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNd1IsVUFBVSxHQUFHdFIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLFVBQU1DLGdCQUFnQixHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTXNSLE9BQU8sR0FBR3ZSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFFQSxRQUFJdVIsT0FBTyxHQUFHLElBQWQ7O0FBQ0EsUUFBSSxLQUFLbE0sS0FBTCxDQUFXM0MsSUFBZixFQUFxQjtBQUNqQjZPLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxPQUFEO0FBQVMsUUFBQSxDQUFDLEVBQUUsRUFBWjtBQUFnQixRQUFBLENBQUMsRUFBRTtBQUFuQixRQUFWO0FBQ0g7O0FBR0QsUUFBSXJJLEtBQUo7QUFDQSxRQUFJc0ksUUFBSjtBQUNBLFFBQUlDLFVBQUo7QUFDQSxRQUFJQyxVQUFKOztBQUVBLFFBQUksS0FBS2hTLEtBQUwsQ0FBV2dLLElBQVgsS0FBb0I3TCxPQUF4QixFQUFpQztBQUM3QixZQUFNTyxNQUFNLEdBQUdpQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCaUUsU0FBdEIsRUFBZjs7QUFFQTJFLE1BQUFBLEtBQUssR0FBRyx5QkFBRyxpQkFBSCxDQUFSO0FBQ0FzSSxNQUFBQSxRQUFRLEdBQUcseUJBQ1AsaUdBRE8sRUFFUCxFQUZPLEVBR1A7QUFBQ3BULFFBQUFBLE1BQU0sRUFBRSxNQUFNO0FBQ1gsaUJBQU87QUFBRyxZQUFBLElBQUksRUFBRSxtQ0FBa0JBLE1BQWxCLENBQVQ7QUFBb0MsWUFBQSxHQUFHLEVBQUMscUJBQXhDO0FBQThELFlBQUEsTUFBTSxFQUFDO0FBQXJFLGFBQStFQSxNQUEvRSxDQUFQO0FBQ0g7QUFGRCxPQUhPLENBQVg7QUFPQXFULE1BQUFBLFVBQVUsR0FBRyx5QkFBRyxJQUFILENBQWI7QUFDQUMsTUFBQUEsVUFBVSxHQUFHLEtBQUtDLFFBQWxCO0FBQ0gsS0FiRCxNQWFPO0FBQUU7QUFDTHpJLE1BQUFBLEtBQUssR0FBRyx5QkFBRyxxQkFBSCxDQUFSO0FBQ0FzSSxNQUFBQSxRQUFRLEdBQUcseUJBQ1AsdUZBQ0EseUJBRk8sRUFFb0IsRUFGcEIsRUFHUDtBQUNJN0YsUUFBQUEsQ0FBQyxFQUFHc0YsR0FBRCxJQUNDO0FBQUcsVUFBQSxJQUFJLEVBQUUsbUNBQWtCLEtBQUt2UixLQUFMLENBQVc4RCxNQUE3QixDQUFUO0FBQStDLFVBQUEsR0FBRyxFQUFDLHFCQUFuRDtBQUF5RSxVQUFBLE1BQU0sRUFBQztBQUFoRixXQUEwRnlOLEdBQTFGO0FBRlIsT0FITyxDQUFYO0FBUUFRLE1BQUFBLFVBQVUsR0FBRyx5QkFBRyxRQUFILENBQWI7QUFDQUMsTUFBQUEsVUFBVSxHQUFHLEtBQUtFLFlBQWxCO0FBQ0g7O0FBRUQsVUFBTUMsWUFBWSxHQUFHLEtBQUt4TSxLQUFMLENBQVcxQyxPQUFYLENBQW1CZCxNQUFuQixHQUE0QixDQUE1QixJQUNiLEtBQUt3RCxLQUFMLENBQVdTLFVBQVgsSUFBeUIsS0FBS1QsS0FBTCxDQUFXUyxVQUFYLENBQXNCM0csUUFBdEIsQ0FBK0IsR0FBL0IsQ0FEakM7QUFFQSxXQUNJLDZCQUFDLFVBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBQyxpQkFEZDtBQUVJLE1BQUEsU0FBUyxFQUFFLElBRmY7QUFHSSxNQUFBLFVBQVUsRUFBRSxLQUFLTyxLQUFMLENBQVdpRSxVQUgzQjtBQUlJLE1BQUEsS0FBSyxFQUFFdUY7QUFKWCxPQU1JO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUcsTUFBQSxTQUFTLEVBQUM7QUFBYixPQUF5Q3NJLFFBQXpDLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLakIsYUFBTCxFQURMLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxNQUFBLElBQUksRUFBQyxTQURUO0FBRUksTUFBQSxPQUFPLEVBQUVtQixVQUZiO0FBR0ksTUFBQSxTQUFTLEVBQUMsMEJBSGQ7QUFJSSxNQUFBLFFBQVEsRUFBRSxLQUFLck0sS0FBTCxDQUFXM0MsSUFBWCxJQUFtQixDQUFDbVA7QUFKbEMsT0FNS0osVUFOTCxDQURKLEVBU0tGLE9BVEwsQ0FGSixDQUZKLEVBZ0JLLEtBQUtWLDRCQUFMLEVBaEJMLEVBaUJJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF3QixLQUFLeEwsS0FBTCxDQUFXTCxTQUFuQyxDQWpCSixFQWtCSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLa0ssY0FBTCxDQUFvQixTQUFwQixDQURMLEVBRUssS0FBS0EsY0FBTCxDQUFvQixhQUFwQixDQUZMLENBbEJKLENBTkosQ0FESjtBQWdDSDs7QUE3MEJ5RDs7OzhCQUF6QzFNLFksZUFDRTtBQUNmO0FBQ0FtQixFQUFBQSxVQUFVLEVBQUVqRCxtQkFBVUcsSUFBVixDQUFlRCxVQUZaO0FBSWY7QUFDQTtBQUNBOEksRUFBQUEsSUFBSSxFQUFFaEosbUJBQVU0QixNQU5EO0FBUWY7QUFDQWtCLEVBQUFBLE1BQU0sRUFBRTlDLG1CQUFVNEI7QUFUSCxDOzhCQURGRSxZLGtCQWFLO0FBQ2xCa0gsRUFBQUEsSUFBSSxFQUFFN0w7QUFEWSxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTksIDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCB7bWFrZVJvb21QZXJtYWxpbmssIG1ha2VVc2VyUGVybWFsaW5rfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvcGVybWFsaW5rcy9QZXJtYWxpbmtzXCI7XHJcbmltcG9ydCBETVJvb21NYXAgZnJvbSBcIi4uLy4uLy4uL3V0aWxzL0RNUm9vbU1hcFwiO1xyXG5pbXBvcnQge1Jvb21NZW1iZXJ9IGZyb20gXCJtYXRyaXgtanMtc2RrL3NyYy9tYXRyaXhcIjtcclxuaW1wb3J0IFNka0NvbmZpZyBmcm9tIFwiLi4vLi4vLi4vU2RrQ29uZmlnXCI7XHJcbmltcG9ydCB7Z2V0SHR0cFVyaUZvck14Y30gZnJvbSBcIm1hdHJpeC1qcy1zZGsvc3JjL2NvbnRlbnQtcmVwb1wiO1xyXG5pbXBvcnQgKiBhcyBFbWFpbCBmcm9tIFwiLi4vLi4vLi4vZW1haWxcIjtcclxuaW1wb3J0IHtnZXREZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwsIHVzZURlZmF1bHRJZGVudGl0eVNlcnZlcn0gZnJvbSBcIi4uLy4uLy4uL3V0aWxzL0lkZW50aXR5U2VydmVyVXRpbHNcIjtcclxuaW1wb3J0IHthYmJyZXZpYXRlVXJsfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvVXJsVXRpbHNcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQgSWRlbnRpdHlBdXRoQ2xpZW50IGZyb20gXCIuLi8uLi8uLi9JZGVudGl0eUF1dGhDbGllbnRcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi9Nb2RhbFwiO1xyXG5pbXBvcnQge2h1bWFuaXplVGltZX0gZnJvbSBcIi4uLy4uLy4uL3V0aWxzL2h1bWFuaXplXCI7XHJcbmltcG9ydCBjcmVhdGVSb29tLCB7Y2FuRW5jcnlwdFRvQWxsVXNlcnN9IGZyb20gXCIuLi8uLi8uLi9jcmVhdGVSb29tXCI7XHJcbmltcG9ydCB7aW52aXRlTXVsdGlwbGVUb1Jvb219IGZyb20gXCIuLi8uLi8uLi9Sb29tSW52aXRlXCI7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gJy4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmUnO1xyXG5pbXBvcnQgUm9vbUxpc3RTdG9yZSwge1RBR19ETX0gZnJvbSBcIi4uLy4uLy4uL3N0b3Jlcy9Sb29tTGlzdFN0b3JlXCI7XHJcbmltcG9ydCB7S2V5fSBmcm9tIFwiLi4vLi4vLi4vS2V5Ym9hcmRcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBLSU5EX0RNID0gXCJkbVwiO1xyXG5leHBvcnQgY29uc3QgS0lORF9JTlZJVEUgPSBcImludml0ZVwiO1xyXG5cclxuY29uc3QgSU5JVElBTF9ST09NU19TSE9XTiA9IDM7IC8vIE51bWJlciBvZiByb29tcyB0byBzaG93IGF0IGZpcnN0XHJcbmNvbnN0IElOQ1JFTUVOVF9ST09NU19TSE9XTiA9IDU7IC8vIE51bWJlciBvZiByb29tcyB0byBhZGQgd2hlbiAnc2hvdyBtb3JlJyBpcyBjbGlja2VkXHJcblxyXG4vLyBUaGlzIGlzIHRoZSBpbnRlcmZhY2UgdGhhdCBpcyBleHBlY3RlZCBieSB2YXJpb3VzIGNvbXBvbmVudHMgaW4gdGhpcyBmaWxlLiBJdCBpcyBhIGJpdFxyXG4vLyBhd2t3YXJkIGJlY2F1c2UgaXQgYWxzbyBtYXRjaGVzIHRoZSBSb29tTWVtYmVyIGNsYXNzIGZyb20gdGhlIGpzLXNkayB3aXRoIHNvbWUgZXh0cmEgc3VwcG9ydFxyXG4vLyBmb3IgM1BJRHMvZW1haWwgYWRkcmVzc2VzLlxyXG4vL1xyXG4vLyBYWFg6IFdlIHNob3VsZCB1c2UgVHlwZVNjcmlwdCBpbnRlcmZhY2VzIGluc3RlYWQgb2YgdGhpcyB3ZWlyZCBcImFic3RyYWN0XCIgY2xhc3MuXHJcbmNsYXNzIE1lbWJlciB7XHJcbiAgICAvKipcclxuICAgICAqIFRoZSBkaXNwbGF5IG5hbWUgb2YgdGhpcyBNZW1iZXIuIEZvciB1c2VycyB0aGlzIHNob3VsZCBiZSB0aGVpciBwcm9maWxlJ3MgZGlzcGxheVxyXG4gICAgICogbmFtZSBvciB1c2VyIElEIGlmIG5vbmUgc2V0LiBGb3IgM1BJRHMgdGhpcyBzaG91bGQgYmUgdGhlIDNQSUQgYWRkcmVzcyAoZW1haWwpLlxyXG4gICAgICovXHJcbiAgICBnZXQgbmFtZSgpOiBzdHJpbmcgeyB0aHJvdyBuZXcgRXJyb3IoXCJNZW1iZXIgY2xhc3Mgbm90IGltcGxlbWVudGVkXCIpOyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgSUQgb2YgdGhpcyBNZW1iZXIuIEZvciB1c2VycyB0aGlzIHNob3VsZCBiZSB0aGVpciB1c2VyIElELiBGb3IgM1BJRHMgdGhpcyBzaG91bGRcclxuICAgICAqIGJlIHRoZSAzUElEIGFkZHJlc3MgKGVtYWlsKS5cclxuICAgICAqL1xyXG4gICAgZ2V0IHVzZXJJZCgpOiBzdHJpbmcgeyB0aHJvdyBuZXcgRXJyb3IoXCJNZW1iZXIgY2xhc3Mgbm90IGltcGxlbWVudGVkXCIpOyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBNWEMgVVJMIG9mIHRoaXMgTWVtYmVyJ3MgYXZhdGFyLiBGb3IgdXNlcnMgdGhpcyBzaG91bGQgYmUgdGhlaXIgcHJvZmlsZSdzXHJcbiAgICAgKiBhdmF0YXIgTVhDIFVSTCBvciBudWxsIGlmIG5vbmUgc2V0LiBGb3IgM1BJRHMgdGhpcyBzaG91bGQgYWx3YXlzIGJlIG51bGwuXHJcbiAgICAgKi9cclxuICAgIGdldE14Y0F2YXRhclVybCgpOiBzdHJpbmcgeyB0aHJvdyBuZXcgRXJyb3IoXCJNZW1iZXIgY2xhc3Mgbm90IGltcGxlbWVudGVkXCIpOyB9XHJcbn1cclxuXHJcbmNsYXNzIERpcmVjdG9yeU1lbWJlciBleHRlbmRzIE1lbWJlciB7XHJcbiAgICBfdXNlcklkOiBzdHJpbmc7XHJcbiAgICBfZGlzcGxheU5hbWU6IHN0cmluZztcclxuICAgIF9hdmF0YXJVcmw6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcih1c2VyRGlyUmVzdWx0OiB7dXNlcl9pZDogc3RyaW5nLCBkaXNwbGF5X25hbWU6IHN0cmluZywgYXZhdGFyX3VybDogc3RyaW5nfSkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5fdXNlcklkID0gdXNlckRpclJlc3VsdC51c2VyX2lkO1xyXG4gICAgICAgIHRoaXMuX2Rpc3BsYXlOYW1lID0gdXNlckRpclJlc3VsdC5kaXNwbGF5X25hbWU7XHJcbiAgICAgICAgdGhpcy5fYXZhdGFyVXJsID0gdXNlckRpclJlc3VsdC5hdmF0YXJfdXJsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZXNlIG5leHQgY2xhc3MgbWVtYmVycyBhcmUgZm9yIHRoZSBNZW1iZXIgaW50ZXJmYWNlXHJcbiAgICBnZXQgbmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kaXNwbGF5TmFtZSB8fCB0aGlzLl91c2VySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHVzZXJJZCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl91c2VySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TXhjQXZhdGFyVXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2F2YXRhclVybDtcclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgVGhyZWVwaWRNZW1iZXIgZXh0ZW5kcyBNZW1iZXIge1xyXG4gICAgX2lkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5faWQgPSBpZDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUaGlzIGlzIGEgZ2V0dGVyIHRoYXQgd291bGQgYmUgZmFsc2V5IG9uIGFsbCBvdGhlciBpbXBsZW1lbnRhdGlvbnMuIFVudGlsIHdlIGhhdmVcclxuICAgIC8vIGJldHRlciB0eXBlIHN1cHBvcnQgaW4gdGhlIHJlYWN0LXNkayB3ZSBjYW4gdXNlIHRoaXMgdHJpY2sgdG8gZGV0ZXJtaW5lIHRoZSBraW5kXHJcbiAgICAvLyBvZiAzUElEIHdlJ3JlIGRlYWxpbmcgd2l0aCwgaWYgYW55LlxyXG4gICAgZ2V0IGlzRW1haWwoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lkLmluY2x1ZGVzKCdAJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVGhlc2UgbmV4dCBjbGFzcyBtZW1iZXJzIGFyZSBmb3IgdGhlIE1lbWJlciBpbnRlcmZhY2VcclxuICAgIGdldCBuYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB1c2VySWQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TXhjQXZhdGFyVXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNsYXNzIERNVXNlclRpbGUgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgbWVtYmVyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsIC8vIFNob3VsZCBiZSBhIE1lbWJlciAoc2VlIGludGVyZmFjZSBhYm92ZSlcclxuICAgICAgICBvblJlbW92ZTogUHJvcFR5cGVzLmZ1bmMsIC8vIHRha2VzIDEgYXJndW1lbnQsIHRoZSBtZW1iZXIgYmVpbmcgcmVtb3ZlZFxyXG4gICAgfTtcclxuXHJcbiAgICBfb25SZW1vdmUgPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIFN0b3AgdGhlIGJyb3dzZXIgZnJvbSBoaWdobGlnaHRpbmcgdGV4dFxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICB0aGlzLnByb3BzLm9uUmVtb3ZlKHRoaXMucHJvcHMubWVtYmVyKTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VBdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuYXZhdGFycy5CYXNlQXZhdGFyXCIpO1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvblwiKTtcclxuXHJcbiAgICAgICAgY29uc3QgYXZhdGFyU2l6ZSA9IDIwO1xyXG4gICAgICAgIGNvbnN0IGF2YXRhciA9IHRoaXMucHJvcHMubWVtYmVyLmlzRW1haWxcclxuICAgICAgICAgICAgPyA8aW1nXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ191c2VyVGlsZV9hdmF0YXIgbXhfSW52aXRlRGlhbG9nX3VzZXJUaWxlX3RocmVlcGlkQXZhdGFyJ1xyXG4gICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uLWVtYWlsLXBpbGwtYXZhdGFyLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgIHdpZHRoPXthdmF0YXJTaXplfSBoZWlnaHQ9e2F2YXRhclNpemV9IC8+XHJcbiAgICAgICAgICAgIDogPEJhc2VBdmF0YXJcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX3VzZXJUaWxlX2F2YXRhcidcclxuICAgICAgICAgICAgICAgIHVybD17Z2V0SHR0cFVyaUZvck14YyhcclxuICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SG9tZXNlcnZlclVybCgpLCB0aGlzLnByb3BzLm1lbWJlci5nZXRNeGNBdmF0YXJVcmwoKSxcclxuICAgICAgICAgICAgICAgICAgICBhdmF0YXJTaXplLCBhdmF0YXJTaXplLCBcImNyb3BcIil9XHJcbiAgICAgICAgICAgICAgICBuYW1lPXt0aGlzLnByb3BzLm1lbWJlci5uYW1lfVxyXG4gICAgICAgICAgICAgICAgaWROYW1lPXt0aGlzLnByb3BzLm1lbWJlci51c2VySWR9XHJcbiAgICAgICAgICAgICAgICB3aWR0aD17YXZhdGFyU2l6ZX1cclxuICAgICAgICAgICAgICAgIGhlaWdodD17YXZhdGFyU2l6ZX0gLz47XHJcblxyXG4gICAgICAgIGxldCBjbG9zZUJ1dHRvbjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vblJlbW92ZSkge1xyXG4gICAgICAgICAgICBjbG9zZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfdXNlclRpbGVfcmVtb3ZlJ1xyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uUmVtb3ZlfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uLXBpbGwtcmVtb3ZlLnN2Z1wiKX0gYWx0PXtfdCgnUmVtb3ZlJyl9IHdpZHRoPXs4fSBoZWlnaHQ9ezh9IC8+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ191c2VyVGlsZSc+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ191c2VyVGlsZV9waWxsJz5cclxuICAgICAgICAgICAgICAgICAgICB7YXZhdGFyfVxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX3VzZXJUaWxlX25hbWUnPnt0aGlzLnByb3BzLm1lbWJlci5uYW1lfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIHsgY2xvc2VCdXR0b24gfVxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgRE1Sb29tVGlsZSBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBtZW1iZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCwgLy8gU2hvdWxkIGJlIGEgTWVtYmVyIChzZWUgaW50ZXJmYWNlIGFib3ZlKVxyXG4gICAgICAgIGxhc3RBY3RpdmVUczogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBvblRvZ2dsZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCwgLy8gdGFrZXMgMSBhcmd1bWVudCwgdGhlIG1lbWJlciBiZWluZyB0b2dnbGVkXHJcbiAgICAgICAgaGlnaGxpZ2h0V29yZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBpc1NlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH07XHJcblxyXG4gICAgX29uQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIFN0b3AgdGhlIGJyb3dzZXIgZnJvbSBoaWdobGlnaHRpbmcgdGV4dFxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICB0aGlzLnByb3BzLm9uVG9nZ2xlKHRoaXMucHJvcHMubWVtYmVyKTtcclxuICAgIH07XHJcblxyXG4gICAgX2hpZ2hsaWdodE5hbWUoc3RyOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuaGlnaGxpZ2h0V29yZCkgcmV0dXJuIHN0cjtcclxuXHJcbiAgICAgICAgLy8gV2UgY29udmVydCB0aGluZ3MgdG8gbG93ZXJjYXNlIGZvciBpbmRleCBzZWFyY2hpbmcsIGJ1dCBwdWxsIHN1YnN0cmluZ3MgZnJvbVxyXG4gICAgICAgIC8vIHRoZSBzdWJtaXR0ZWQgdGV4dCB0byBwcmVzZXJ2ZSBjYXNlLiBOb3RlOiB3ZSBkb24ndCBuZWVkIHRvIGh0bWxFbnRpdGllcyB0aGVcclxuICAgICAgICAvLyBzdHJpbmcgYmVjYXVzZSBSZWFjdCB3aWxsIHNhZmVseSBlbmNvZGUgdGhlIHRleHQgZm9yIHVzLlxyXG4gICAgICAgIGNvbnN0IGxvd2VyU3RyID0gc3RyLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyU3RyID0gdGhpcy5wcm9wcy5oaWdobGlnaHRXb3JkLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IFtdO1xyXG5cclxuICAgICAgICBsZXQgaSA9IDA7XHJcbiAgICAgICAgbGV0IGlpO1xyXG4gICAgICAgIHdoaWxlICgoaWkgPSBsb3dlclN0ci5pbmRleE9mKGZpbHRlclN0ciwgaSkpID49IDApIHtcclxuICAgICAgICAgICAgLy8gUHVzaCBhbnkgdGV4dCB3ZSBtaXNzZWQgKGZpcnN0IGJpdC9taWRkbGUgb2YgdGV4dClcclxuICAgICAgICAgICAgaWYgKGlpID4gaSkge1xyXG4gICAgICAgICAgICAgICAgLy8gUHVzaCBhbnkgdGV4dCB3ZSBhcmVuJ3QgaGlnaGxpZ2h0aW5nIChtaWRkbGUgb2YgdGV4dCBtYXRjaCwgb3IgYmVnaW5uaW5nIG9mIHRleHQpXHJcbiAgICAgICAgICAgICAgICByZXN1bHQucHVzaCg8c3BhbiBrZXk9e2kgKyAnYmVnaW4nfT57c3RyLnN1YnN0cmluZyhpLCBpaSl9PC9zcGFuPik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGkgPSBpaTsgLy8gY29weSBvdmVyIGlpIG9ubHkgaWYgd2UgaGF2ZSBhIG1hdGNoICh0byBwcmVzZXJ2ZSBpIGZvciBlbmQtb2YtdGV4dCBtYXRjaGluZylcclxuXHJcbiAgICAgICAgICAgIC8vIEhpZ2hsaWdodCB0aGUgd29yZCB0aGUgdXNlciBlbnRlcmVkXHJcbiAgICAgICAgICAgIGNvbnN0IHN1YnN0ciA9IHN0ci5zdWJzdHJpbmcoaSwgZmlsdGVyU3RyLmxlbmd0aCArIGkpO1xyXG4gICAgICAgICAgICByZXN1bHQucHVzaCg8c3BhbiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ19yb29tVGlsZV9oaWdobGlnaHQnIGtleT17aSArICdib2xkJ30+e3N1YnN0cn08L3NwYW4+KTtcclxuICAgICAgICAgICAgaSArPSBzdWJzdHIubGVuZ3RoO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gUHVzaCBhbnkgdGV4dCB3ZSBtaXNzZWQgKGVuZCBvZiB0ZXh0KVxyXG4gICAgICAgIGlmIChpIDwgc3RyLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXN1bHQucHVzaCg8c3BhbiBrZXk9e2kgKyAnZW5kJ30+e3N0ci5zdWJzdHJpbmcoaSl9PC9zcGFuPik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmF2YXRhcnMuQmFzZUF2YXRhclwiKTtcclxuXHJcbiAgICAgICAgbGV0IHRpbWVzdGFtcCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubGFzdEFjdGl2ZVRzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGh1bWFuVHMgPSBodW1hbml6ZVRpbWUodGhpcy5wcm9wcy5sYXN0QWN0aXZlVHMpO1xyXG4gICAgICAgICAgICB0aW1lc3RhbXAgPSA8c3BhbiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ19yb29tVGlsZV90aW1lJz57aHVtYW5Uc308L3NwYW4+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgYXZhdGFyU2l6ZSA9IDM2O1xyXG4gICAgICAgIGNvbnN0IGF2YXRhciA9IHRoaXMucHJvcHMubWVtYmVyLmlzRW1haWxcclxuICAgICAgICAgICAgPyA8aW1nXHJcbiAgICAgICAgICAgICAgICBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ljb24tZW1haWwtcGlsbC1hdmF0YXIuc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgd2lkdGg9e2F2YXRhclNpemV9IGhlaWdodD17YXZhdGFyU2l6ZX0gLz5cclxuICAgICAgICAgICAgOiA8QmFzZUF2YXRhclxyXG4gICAgICAgICAgICAgICAgdXJsPXtnZXRIdHRwVXJpRm9yTXhjKFxyXG4gICAgICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRIb21lc2VydmVyVXJsKCksIHRoaXMucHJvcHMubWVtYmVyLmdldE14Y0F2YXRhclVybCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGF2YXRhclNpemUsIGF2YXRhclNpemUsIFwiY3JvcFwiKX1cclxuICAgICAgICAgICAgICAgIG5hbWU9e3RoaXMucHJvcHMubWVtYmVyLm5hbWV9XHJcbiAgICAgICAgICAgICAgICBpZE5hbWU9e3RoaXMucHJvcHMubWVtYmVyLnVzZXJJZH1cclxuICAgICAgICAgICAgICAgIHdpZHRoPXthdmF0YXJTaXplfVxyXG4gICAgICAgICAgICAgICAgaGVpZ2h0PXthdmF0YXJTaXplfSAvPjtcclxuXHJcbiAgICAgICAgbGV0IGNoZWNrbWFyayA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuaXNTZWxlY3RlZCkge1xyXG4gICAgICAgICAgICAvLyBUbyByZWR1Y2UgZmxpY2tlcmluZyB3ZSBwdXQgdGhlICdzZWxlY3RlZCcgcm9vbSB0aWxlIGFib3ZlIHRoZSByZWFsIGF2YXRhclxyXG4gICAgICAgICAgICBjaGVja21hcmsgPSA8ZGl2IGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX3Jvb21UaWxlX3NlbGVjdGVkJyAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRvIHJlZHVjZSBmbGlja2VyaW5nIHdlIHB1dCB0aGUgY2hlY2ttYXJrIG9uIHRvcCBvZiB0aGUgYWN0dWFsIGF2YXRhciAocHJldmVudHNcclxuICAgICAgICAvLyB0aGUgYnJvd3NlciBmcm9tIHJlbG9hZGluZyB0aGUgaW1hZ2Ugc291cmNlIHdoZW4gdGhlIGF2YXRhciByZW1vdW50cykuXHJcbiAgICAgICAgY29uc3Qgc3RhY2tlZEF2YXRhciA9IChcclxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfcm9vbVRpbGVfYXZhdGFyU3RhY2snPlxyXG4gICAgICAgICAgICAgICAge2F2YXRhcn1cclxuICAgICAgICAgICAgICAgIHtjaGVja21hcmt9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX3Jvb21UaWxlJyBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrfT5cclxuICAgICAgICAgICAgICAgIHtzdGFja2VkQXZhdGFyfVxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfcm9vbVRpbGVfbmFtZSc+e3RoaXMuX2hpZ2hsaWdodE5hbWUodGhpcy5wcm9wcy5tZW1iZXIubmFtZSl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfcm9vbVRpbGVfdXNlcklkJz57dGhpcy5faGlnaGxpZ2h0TmFtZSh0aGlzLnByb3BzLm1lbWJlci51c2VySWQpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIHt0aW1lc3RhbXB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEludml0ZURpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyBUYWtlcyBhbiBhcnJheSBvZiB1c2VyIElEcy9lbWFpbHMgdG8gaW52aXRlLlxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIC8vIFRoZSBraW5kIG9mIGludml0ZSBiZWluZyBwZXJmb3JtZWQuIEFzc3VtZWQgdG8gYmUgS0lORF9ETSBpZlxyXG4gICAgICAgIC8vIG5vdCBwcm92aWRlZC5cclxuICAgICAgICBraW5kOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyBUaGUgcm9vbSBJRCB0aGlzIGRpYWxvZyBpcyBmb3IuIE9ubHkgcmVxdWlyZWQgZm9yIEtJTkRfSU5WSVRFLlxyXG4gICAgICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICBraW5kOiBLSU5EX0RNLFxyXG4gICAgfTtcclxuXHJcbiAgICBfZGVib3VuY2VUaW1lcjogbnVtYmVyID0gbnVsbDtcclxuICAgIF9lZGl0b3JSZWY6IGFueSA9IG51bGw7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIGlmIChwcm9wcy5raW5kID09PSBLSU5EX0lOVklURSAmJiAhcHJvcHMucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIldoZW4gdXNpbmcgS0lORF9JTlZJVEUgYSByb29tSWQgaXMgcmVxdWlyZWQgZm9yIGFuIEludml0ZURpYWxvZ1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGFscmVhZHlJbnZpdGVkID0gbmV3IFNldChbTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXJJZCgpLCBTZGtDb25maWcuZ2V0KClbJ3dlbGNvbWVVc2VySWQnXV0pO1xyXG4gICAgICAgIGlmIChwcm9wcy5yb29tSWQpIHtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgICAgIGlmICghcm9vbSkgdGhyb3cgbmV3IEVycm9yKFwiUm9vbSBJRCBnaXZlbiB0byBJbnZpdGVEaWFsb2cgZG9lcyBub3QgbG9vayBsaWtlIGEgcm9vbVwiKTtcclxuICAgICAgICAgICAgcm9vbS5nZXRNZW1iZXJzV2l0aE1lbWJlcnNoaXAoJ2ludml0ZScpLmZvckVhY2gobSA9PiBhbHJlYWR5SW52aXRlZC5hZGQobS51c2VySWQpKTtcclxuICAgICAgICAgICAgcm9vbS5nZXRNZW1iZXJzV2l0aE1lbWJlcnNoaXAoJ2pvaW4nKS5mb3JFYWNoKG0gPT4gYWxyZWFkeUludml0ZWQuYWRkKG0udXNlcklkKSk7XHJcbiAgICAgICAgICAgIC8vIGFkZCBiYW5uZWQgdXNlcnMsIHNvIHdlIGRvbid0IHRyeSB0byBpbnZpdGUgdGhlbVxyXG4gICAgICAgICAgICByb29tLmdldE1lbWJlcnNXaXRoTWVtYmVyc2hpcCgnYmFuJykuZm9yRWFjaChtID0+IGFscmVhZHlJbnZpdGVkLmFkZChtLnVzZXJJZCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgdGFyZ2V0czogW10sIC8vIGFycmF5IG9mIE1lbWJlciBvYmplY3RzIChzZWUgaW50ZXJmYWNlIGFib3ZlKVxyXG4gICAgICAgICAgICBmaWx0ZXJUZXh0OiBcIlwiLFxyXG4gICAgICAgICAgICByZWNlbnRzOiB0aGlzLl9idWlsZFJlY2VudHMoYWxyZWFkeUludml0ZWQpLFxyXG4gICAgICAgICAgICBudW1SZWNlbnRzU2hvd246IElOSVRJQUxfUk9PTVNfU0hPV04sXHJcbiAgICAgICAgICAgIHN1Z2dlc3Rpb25zOiB0aGlzLl9idWlsZFN1Z2dlc3Rpb25zKGFscmVhZHlJbnZpdGVkKSxcclxuICAgICAgICAgICAgbnVtU3VnZ2VzdGlvbnNTaG93bjogSU5JVElBTF9ST09NU19TSE9XTixcclxuICAgICAgICAgICAgc2VydmVyUmVzdWx0c01peGluOiBbXSwgLy8geyB1c2VyOiBEaXJlY3RvcnlNZW1iZXIsIHVzZXJJZDogc3RyaW5nIH1bXSwgbGlrZSByZWNlbnRzIGFuZCBzdWdnZXN0aW9uc1xyXG4gICAgICAgICAgICB0aHJlZXBpZFJlc3VsdHNNaXhpbjogW10sIC8vIHsgdXNlcjogVGhyZWVwaWRNZW1iZXIsIHVzZXJJZDogc3RyaW5nfVtdLCBsaWtlIHJlY2VudHMgYW5kIHN1Z2dlc3Rpb25zXHJcbiAgICAgICAgICAgIGNhblVzZUlkZW50aXR5U2VydmVyOiAhIU1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRJZGVudGl0eVNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICB0cnlpbmdJZGVudGl0eVNlcnZlcjogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICAvLyBUaGVzZSB0d28gZmxhZ3MgYXJlIHVzZWQgZm9yIHRoZSAnR28nIGJ1dHRvbiB0byBjb21tdW5pY2F0ZSB3aGF0IGlzIGdvaW5nIG9uLlxyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgZXJyb3JUZXh0OiBudWxsLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZiA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfVxyXG5cclxuICAgIF9idWlsZFJlY2VudHMoZXhjbHVkZWRUYXJnZXRJZHM6IFNldDxzdHJpbmc+KToge3VzZXJJZDogc3RyaW5nLCB1c2VyOiBSb29tTWVtYmVyLCBsYXN0QWN0aXZlOiBudW1iZXJ9IHtcclxuICAgICAgICBjb25zdCByb29tcyA9IERNUm9vbU1hcC5zaGFyZWQoKS5nZXRVbmlxdWVSb29tc1dpdGhJbmRpdmlkdWFscygpOyAvLyBtYXAgb2YgdXNlcklkID0+IGpzLXNkayBSb29tXHJcblxyXG4gICAgICAgIC8vIEFsc28gcHVsbCBpbiBhbGwgdGhlIHJvb21zIHRhZ2dlZCBhcyBUQUdfRE0gc28gd2UgZG9uJ3QgbWlzcyBhbnl0aGluZy4gU29tZXRpbWVzIHRoZVxyXG4gICAgICAgIC8vIHJvb20gbGlzdCBkb2Vzbid0IHRhZyB0aGUgcm9vbSBmb3IgdGhlIERNUm9vbU1hcCwgYnV0IGRvZXMgZm9yIHRoZSByb29tIGxpc3QuXHJcbiAgICAgICAgY29uc3QgdGFnZ2VkUm9vbXMgPSBSb29tTGlzdFN0b3JlLmdldFJvb21MaXN0cygpO1xyXG4gICAgICAgIGNvbnN0IGRtVGFnZ2VkUm9vbXMgPSB0YWdnZWRSb29tc1tUQUdfRE1dO1xyXG4gICAgICAgIGNvbnN0IG15VXNlcklkID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXJJZCgpO1xyXG4gICAgICAgIGZvciAoY29uc3QgZG1Sb29tIG9mIGRtVGFnZ2VkUm9vbXMpIHtcclxuICAgICAgICAgICAgY29uc3Qgb3RoZXJNZW1iZXJzID0gZG1Sb29tLmdldEpvaW5lZE1lbWJlcnMoKS5maWx0ZXIodSA9PiB1LnVzZXJJZCAhPT0gbXlVc2VySWQpO1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IG1lbWJlciBvZiBvdGhlck1lbWJlcnMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChyb29tc1ttZW1iZXIudXNlcklkXSkgY29udGludWU7IC8vIGFscmVhZHkgaGF2ZSBhIHJvb21cclxuXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oYEFkZGluZyBETSByb29tIGZvciAke21lbWJlci51c2VySWR9IGFzICR7ZG1Sb29tLnJvb21JZH0gZnJvbSB0YWcsIG5vdCBETSBtYXBgKTtcclxuICAgICAgICAgICAgICAgIHJvb21zW21lbWJlci51c2VySWRdID0gZG1Sb29tO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByZWNlbnRzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCB1c2VySWQgaW4gcm9vbXMpIHtcclxuICAgICAgICAgICAgLy8gRmlsdGVyIG91dCB1c2VyIElEcyB0aGF0IGFyZSBhbHJlYWR5IGluIHRoZSByb29tIC8gc2hvdWxkIGJlIGV4Y2x1ZGVkXHJcbiAgICAgICAgICAgIGlmIChleGNsdWRlZFRhcmdldElkcy5oYXModXNlcklkKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS53YXJuKGBbSW52aXRlOlJlY2VudHNdIEV4Y2x1ZGluZyAke3VzZXJJZH0gZnJvbSByZWNlbnRzYCk7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3Qgcm9vbSA9IHJvb21zW3VzZXJJZF07XHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IHJvb20uZ2V0TWVtYmVyKHVzZXJJZCk7XHJcbiAgICAgICAgICAgIGlmICghbWVtYmVyKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBqdXN0IHNraXAgcGVvcGxlIHdobyBkb24ndCBoYXZlIG1lbWJlcnNoaXBzIGZvciBzb21lIHJlYXNvblxyXG4gICAgICAgICAgICAgICAgY29uc29sZS53YXJuKGBbSW52aXRlOlJlY2VudHNdICR7dXNlcklkfSBpcyBtaXNzaW5nIGEgbWVtYmVyIG9iamVjdCBpbiB0aGVpciBvd24gRE0gKCR7cm9vbS5yb29tSWR9KWApO1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIEZpbmQgdGhlIGxhc3QgdGltZXN0YW1wIGZvciBhIG1lc3NhZ2UgZXZlbnRcclxuICAgICAgICAgICAgY29uc3Qgc2VhcmNoVHlwZXMgPSBbXCJtLnJvb20ubWVzc2FnZVwiLCBcIm0ucm9vbS5lbmNyeXB0ZWRcIiwgXCJtLnN0aWNrZXJcIl07XHJcbiAgICAgICAgICAgIGNvbnN0IG1heFNlYXJjaEV2ZW50cyA9IDIwOyAvLyB0byBwcmV2ZW50IHRyYXZlcnNpbmcgaGlzdG9yeVxyXG4gICAgICAgICAgICBsZXQgbGFzdEV2ZW50VHMgPSAwO1xyXG4gICAgICAgICAgICBpZiAocm9vbS50aW1lbGluZSAmJiByb29tLnRpbWVsaW5lLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IHJvb20udGltZWxpbmUubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldiA9IHJvb20udGltZWxpbmVbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaFR5cGVzLmluY2x1ZGVzKGV2LmdldFR5cGUoKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFzdEV2ZW50VHMgPSBldi5nZXRUcygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJvb20udGltZWxpbmUubGVuZ3RoIC0gaSA+IG1heFNlYXJjaEV2ZW50cykgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCFsYXN0RXZlbnRUcykge1xyXG4gICAgICAgICAgICAgICAgLy8gc29tZXRoaW5nIHdlaXJkIGlzIGdvaW5nIG9uIHdpdGggdGhpcyByb29tXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oYFtJbnZpdGU6UmVjZW50c10gJHt1c2VySWR9ICgke3Jvb20ucm9vbUlkfSkgaGFzIGEgd2VpcmQgbGFzdCB0aW1lc3RhbXA6ICR7bGFzdEV2ZW50VHN9YCk7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmVjZW50cy5wdXNoKHt1c2VySWQsIHVzZXI6IG1lbWJlciwgbGFzdEFjdGl2ZTogbGFzdEV2ZW50VHN9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFyZWNlbnRzKSBjb25zb2xlLndhcm4oXCJbSW52aXRlOlJlY2VudHNdIE5vIHJlY2VudHMgdG8gc3VnZ2VzdCFcIik7XHJcblxyXG4gICAgICAgIC8vIFNvcnQgdGhlIHJlY2VudHMgYnkgbGFzdCBhY3RpdmUgdG8gc2F2ZSB1cyB0aW1lIGxhdGVyXHJcbiAgICAgICAgcmVjZW50cy5zb3J0KChhLCBiKSA9PiBiLmxhc3RBY3RpdmUgLSBhLmxhc3RBY3RpdmUpO1xyXG5cclxuICAgICAgICByZXR1cm4gcmVjZW50cztcclxuICAgIH1cclxuXHJcbiAgICBfYnVpbGRTdWdnZXN0aW9ucyhleGNsdWRlZFRhcmdldElkczogU2V0PHN0cmluZz4pOiB7dXNlcklkOiBzdHJpbmcsIHVzZXI6IFJvb21NZW1iZXJ9IHtcclxuICAgICAgICBjb25zdCBtYXhDb25zaWRlcmVkTWVtYmVycyA9IDIwMDtcclxuICAgICAgICBjb25zdCBqb2luZWRSb29tcyA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tcygpXHJcbiAgICAgICAgICAgIC5maWx0ZXIociA9PiByLmdldE15TWVtYmVyc2hpcCgpID09PSAnam9pbicgJiYgci5nZXRKb2luZWRNZW1iZXJDb3VudCgpIDw9IG1heENvbnNpZGVyZWRNZW1iZXJzKTtcclxuXHJcbiAgICAgICAgLy8gR2VuZXJhdGVzIHsgdXNlcklkOiB7bWVtYmVyLCByb29tc1tdfSB9XHJcbiAgICAgICAgY29uc3QgbWVtYmVyUm9vbXMgPSBqb2luZWRSb29tcy5yZWR1Y2UoKG1lbWJlcnMsIHJvb20pID0+IHtcclxuICAgICAgICAgICAgLy8gRmlsdGVyIG91dCBETXMgKHdlJ2xsIGhhbmRsZSB0aGVzZSBpbiB0aGUgcmVjZW50cyBzZWN0aW9uKVxyXG4gICAgICAgICAgICBpZiAoRE1Sb29tTWFwLnNoYXJlZCgpLmdldFVzZXJJZEZvclJvb21JZChyb29tLnJvb21JZCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBtZW1iZXJzOyAvLyBEbyBub3RoaW5nXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGpvaW5lZE1lbWJlcnMgPSByb29tLmdldEpvaW5lZE1lbWJlcnMoKS5maWx0ZXIodSA9PiAhZXhjbHVkZWRUYXJnZXRJZHMuaGFzKHUudXNlcklkKSk7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgbWVtYmVyIG9mIGpvaW5lZE1lbWJlcnMpIHtcclxuICAgICAgICAgICAgICAgIC8vIEZpbHRlciBvdXQgdXNlciBJRHMgdGhhdCBhcmUgYWxyZWFkeSBpbiB0aGUgcm9vbSAvIHNob3VsZCBiZSBleGNsdWRlZFxyXG4gICAgICAgICAgICAgICAgaWYgKGV4Y2x1ZGVkVGFyZ2V0SWRzLmhhcyhtZW1iZXIudXNlcklkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghbWVtYmVyc1ttZW1iZXIudXNlcklkXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lbWJlcnNbbWVtYmVyLnVzZXJJZF0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbWJlcjogbWVtYmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBUcmFjayB0aGUgcm9vbSBzaXplIG9mIHRoZSAncGlja2VkJyBtZW1iZXIgc28gd2UgY2FuIHVzZSB0aGUgcHJvZmlsZSBvZlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGUgc21hbGxlc3Qgcm9vbSAobGlrZWx5IGEgRE0pLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwaWNrZWRNZW1iZXJSb29tU2l6ZTogcm9vbS5nZXRKb2luZWRNZW1iZXJDb3VudCgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tczogW10sXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBtZW1iZXJzW21lbWJlci51c2VySWRdLnJvb21zLnB1c2gocm9vbSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJvb20uZ2V0Sm9pbmVkTWVtYmVyQ291bnQoKSA8IG1lbWJlcnNbbWVtYmVyLnVzZXJJZF0ucGlja2VkTWVtYmVyUm9vbVNpemUpIHtcclxuICAgICAgICAgICAgICAgICAgICBtZW1iZXJzW21lbWJlci51c2VySWRdLm1lbWJlciA9IG1lbWJlcjtcclxuICAgICAgICAgICAgICAgICAgICBtZW1iZXJzW21lbWJlci51c2VySWRdLnBpY2tlZE1lbWJlclJvb21TaXplID0gcm9vbS5nZXRKb2luZWRNZW1iZXJDb3VudCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBtZW1iZXJzO1xyXG4gICAgICAgIH0sIHt9KTtcclxuXHJcbiAgICAgICAgLy8gR2VuZXJhdGVzIHsgdXNlcklkOiB7bWVtYmVyLCBudW1Sb29tcywgc2NvcmV9IH1cclxuICAgICAgICBjb25zdCBtZW1iZXJTY29yZXMgPSBPYmplY3QudmFsdWVzKG1lbWJlclJvb21zKS5yZWR1Y2UoKHNjb3JlcywgZW50cnkpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbnVtTWVtYmVyc1RvdGFsID0gZW50cnkucm9vbXMucmVkdWNlKChjLCByKSA9PiBjICsgci5nZXRKb2luZWRNZW1iZXJDb3VudCgpLCAwKTtcclxuICAgICAgICAgICAgY29uc3QgbWF4UmFuZ2UgPSBtYXhDb25zaWRlcmVkTWVtYmVycyAqIGVudHJ5LnJvb21zLmxlbmd0aDtcclxuICAgICAgICAgICAgc2NvcmVzW2VudHJ5Lm1lbWJlci51c2VySWRdID0ge1xyXG4gICAgICAgICAgICAgICAgbWVtYmVyOiBlbnRyeS5tZW1iZXIsXHJcbiAgICAgICAgICAgICAgICBudW1Sb29tczogZW50cnkucm9vbXMubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgc2NvcmU6IE1hdGgubWF4KDAsIE1hdGgucG93KDEgLSAobnVtTWVtYmVyc1RvdGFsIC8gbWF4UmFuZ2UpLCA1KSksXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHJldHVybiBzY29yZXM7XHJcbiAgICAgICAgfSwge30pO1xyXG5cclxuICAgICAgICAvLyBOb3cgdGhhdCB3ZSBoYXZlIHNjb3JlcyBmb3IgYmVpbmcgaW4gcm9vbXMsIGJvb3N0IHRob3NlIHBlb3BsZSB3aG8gaGF2ZSBzZW50IG1lc3NhZ2VzXHJcbiAgICAgICAgLy8gcmVjZW50bHksIGFzIGEgd2F5IHRvIGltcHJvdmUgdGhlIHF1YWxpdHkgb2Ygc3VnZ2VzdGlvbnMuIFdlIGRvIHRoaXMgYnkgY2hlY2tpbmcgZXZlcnlcclxuICAgICAgICAvLyByb29tIHRvIHNlZSB3aG8gaGFzIHNlbnQgYSBtZXNzYWdlIGluIHRoZSBsYXN0IGZldyBob3VycywgYW5kIGdpdmluZyB0aGVtIGEgc2NvcmVcclxuICAgICAgICAvLyB3aGljaCBjb3JyZWxhdGVzIHRvIHRoZSBmcmVzaG5lc3Mgb2YgdGhlaXIgbWVzc2FnZS4gSW4gdGhlb3J5LCB0aGlzIHJlc3VsdHMgaW4gc3VnZ2VzdGlvbnNcclxuICAgICAgICAvLyB3aGljaCBhcmUgY2xvc2VyIHRvIFwiY29udGludWUgdGhpcyBjb252ZXJzYXRpb25cIiByYXRoZXIgdGhhbiBcInRoaXMgcGVyc29uIGV4aXN0c1wiLlxyXG4gICAgICAgIGNvbnN0IHRydWVKb2luZWRSb29tcyA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tcygpLmZpbHRlcihyID0+IHIuZ2V0TXlNZW1iZXJzaGlwKCkgPT09ICdqb2luJyk7XHJcbiAgICAgICAgY29uc3Qgbm93ID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcclxuICAgICAgICBjb25zdCBlYXJsaWVzdEFnZUNvbnNpZGVyZWQgPSBub3cgLSAoNjAgKiA2MCAqIDEwMDApOyAvLyAxIGhvdXIgYWdvXHJcbiAgICAgICAgY29uc3QgbWF4TWVzc2FnZXNDb25zaWRlcmVkID0gNTA7IC8vIHNvIHdlIGRvbid0IGl0ZXJhdGUgb3ZlciBhIGh1Z2UgYW1vdW50IG9mIHRyYWZmaWNcclxuICAgICAgICBjb25zdCBsYXN0U3Bva2UgPSB7fTsgLy8gdXNlcklkOiB0aW1lc3RhbXBcclxuICAgICAgICBjb25zdCBsYXN0U3Bva2VNZW1iZXJzID0ge307IC8vIHVzZXJJZDogcm9vbSBtZW1iZXJcclxuICAgICAgICBmb3IgKGNvbnN0IHJvb20gb2YgdHJ1ZUpvaW5lZFJvb21zKSB7XHJcbiAgICAgICAgICAgIC8vIFNraXAgbG93IHByaW9yaXR5IHJvb21zIGFuZCBETXNcclxuICAgICAgICAgICAgY29uc3QgaXNEbSA9IERNUm9vbU1hcC5zaGFyZWQoKS5nZXRVc2VySWRGb3JSb29tSWQocm9vbS5yb29tSWQpO1xyXG4gICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMocm9vbS50YWdzKS5pbmNsdWRlcyhcIm0ubG93cHJpb3JpdHlcIikgfHwgaXNEbSkge1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50cyA9IHJvb20uZ2V0TGl2ZVRpbWVsaW5lKCkuZ2V0RXZlbnRzKCk7IC8vIHRpbWVsaW5lcyBhcmUgbW9zdCByZWNlbnQgbGFzdFxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gZXZlbnRzLmxlbmd0aCAtIDE7IGkgPj0gTWF0aC5tYXgoMCwgZXZlbnRzLmxlbmd0aCAtIG1heE1lc3NhZ2VzQ29uc2lkZXJlZCk7IGktLSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXYgPSBldmVudHNbaV07XHJcbiAgICAgICAgICAgICAgICBpZiAoZXhjbHVkZWRUYXJnZXRJZHMuaGFzKGV2LmdldFNlbmRlcigpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKGV2LmdldFRzKCkgPD0gZWFybGllc3RBZ2VDb25zaWRlcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7IC8vIGdpdmUgdXA6IGFsbCBldmVudHMgZnJvbSBoZXJlIG9uIG91dCBhcmUgdG9vIG9sZFxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghbGFzdFNwb2tlW2V2LmdldFNlbmRlcigpXSB8fCBsYXN0U3Bva2VbZXYuZ2V0U2VuZGVyKCldIDwgZXYuZ2V0VHMoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxhc3RTcG9rZVtldi5nZXRTZW5kZXIoKV0gPSBldi5nZXRUcygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxhc3RTcG9rZU1lbWJlcnNbZXYuZ2V0U2VuZGVyKCldID0gcm9vbS5nZXRNZW1iZXIoZXYuZ2V0U2VuZGVyKCkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoY29uc3QgdXNlcklkIGluIGxhc3RTcG9rZSkge1xyXG4gICAgICAgICAgICBjb25zdCB0cyA9IGxhc3RTcG9rZVt1c2VySWRdO1xyXG4gICAgICAgICAgICBjb25zdCBtZW1iZXIgPSBsYXN0U3Bva2VNZW1iZXJzW3VzZXJJZF07XHJcbiAgICAgICAgICAgIGlmICghbWVtYmVyKSBjb250aW51ZTsgLy8gc2tpcCBwZW9wbGUgd2Ugc29tZWhvdyBkb24ndCBoYXZlIHByb2ZpbGVzIGZvclxyXG5cclxuICAgICAgICAgICAgLy8gU2NvcmVzIGZyb20gYmVpbmcgaW4gYSByb29tIGdpdmUgYSAnZ29vZCcgc2NvcmUgb2YgYWJvdXQgMS4wLTEuNSwgc28gZm9yIG91clxyXG4gICAgICAgICAgICAvLyBib29zdCB3ZSdsbCB0cnkgYW5kIGF3YXJkIGF0IGxlYXN0ICsxLjAgZm9yIG1ha2luZyB0aGUgbGlzdCwgd2l0aCArNC4wIGJlaW5nXHJcbiAgICAgICAgICAgIC8vIGFuIGFwcHJveGltYXRlIG1heGltdW0gZm9yIGJlaW5nIHNlbGVjdGVkLlxyXG4gICAgICAgICAgICBjb25zdCBkaXN0YW5jZUZyb21Ob3cgPSBNYXRoLmFicyhub3cgLSB0cyk7IC8vIGFicyB0byBhY2NvdW50IGZvciBzbGlnaHQgZnV0dXJlIG1lc3NhZ2VzXHJcbiAgICAgICAgICAgIGNvbnN0IGludmVyc2VUaW1lID0gKG5vdyAtIGVhcmxpZXN0QWdlQ29uc2lkZXJlZCkgLSBkaXN0YW5jZUZyb21Ob3c7XHJcbiAgICAgICAgICAgIGNvbnN0IHNjb3JlQm9vc3QgPSBNYXRoLm1heCgxLCBpbnZlcnNlVGltZSAvICgxNSAqIDYwICogMTAwMCkpOyAvLyAxNW1pbiBzZWdtZW50cyB0byBrZWVwIHNjb3JlcyBzYW5lXHJcblxyXG4gICAgICAgICAgICBsZXQgcmVjb3JkID0gbWVtYmVyU2NvcmVzW3VzZXJJZF07XHJcbiAgICAgICAgICAgIGlmICghcmVjb3JkKSByZWNvcmQgPSBtZW1iZXJTY29yZXNbdXNlcklkXSA9IHtzY29yZTogMH07XHJcbiAgICAgICAgICAgIHJlY29yZC5tZW1iZXIgPSBtZW1iZXI7XHJcbiAgICAgICAgICAgIHJlY29yZC5zY29yZSArPSBzY29yZUJvb3N0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbWVtYmVycyA9IE9iamVjdC52YWx1ZXMobWVtYmVyU2NvcmVzKTtcclxuICAgICAgICBtZW1iZXJzLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgaWYgKGEuc2NvcmUgPT09IGIuc2NvcmUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChhLm51bVJvb21zID09PSBiLm51bVJvb21zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEubWVtYmVyLnVzZXJJZC5sb2NhbGVDb21wYXJlKGIubWVtYmVyLnVzZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGIubnVtUm9vbXMgLSBhLm51bVJvb21zO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBiLnNjb3JlIC0gYS5zY29yZTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG1lbWJlcnMubWFwKG0gPT4gKHt1c2VySWQ6IG0ubWVtYmVyLnVzZXJJZCwgdXNlcjogbS5tZW1iZXJ9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3Nob3VsZEFib3J0QWZ0ZXJJbnZpdGVFcnJvcihyZXN1bHQpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCBmYWlsZWRVc2VycyA9IE9iamVjdC5rZXlzKHJlc3VsdC5zdGF0ZXMpLmZpbHRlcihhID0+IHJlc3VsdC5zdGF0ZXNbYV0gPT09ICdlcnJvcicpO1xyXG4gICAgICAgIGlmIChmYWlsZWRVc2Vycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRmFpbGVkIHRvIGludml0ZSB1c2VyczogXCIsIHJlc3VsdCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQ6IF90KFwiRmFpbGVkIHRvIGludml0ZSB0aGUgZm9sbG93aW5nIHVzZXJzIHRvIGNoYXQ6ICUoY3N2VXNlcnMpc1wiLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3N2VXNlcnM6IGZhaWxlZFVzZXJzLmpvaW4oXCIsIFwiKSxcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7IC8vIGFib3J0XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBfY29udmVydEZpbHRlcigpOiBNZW1iZXJbXSB7XHJcbiAgICAgICAgLy8gQ2hlY2sgdG8gc2VlIGlmIHRoZXJlJ3MgYW55dGhpbmcgdG8gY29udmVydCBmaXJzdFxyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5maWx0ZXJUZXh0IHx8ICF0aGlzLnN0YXRlLmZpbHRlclRleHQuaW5jbHVkZXMoJ0AnKSkgcmV0dXJuIHRoaXMuc3RhdGUudGFyZ2V0cyB8fCBbXTtcclxuXHJcbiAgICAgICAgbGV0IG5ld01lbWJlcjogTWVtYmVyO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmZpbHRlclRleHQuc3RhcnRzV2l0aCgnQCcpKSB7XHJcbiAgICAgICAgICAgIC8vIEFzc3VtZSBteGlkXHJcbiAgICAgICAgICAgIG5ld01lbWJlciA9IG5ldyBEaXJlY3RvcnlNZW1iZXIoe3VzZXJfaWQ6IHRoaXMuc3RhdGUuZmlsdGVyVGV4dCwgZGlzcGxheV9uYW1lOiBudWxsLCBhdmF0YXJfdXJsOiBudWxsfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gQXNzdW1lIGVtYWlsXHJcbiAgICAgICAgICAgIG5ld01lbWJlciA9IG5ldyBUaHJlZXBpZE1lbWJlcih0aGlzLnN0YXRlLmZpbHRlclRleHQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBuZXdUYXJnZXRzID0gWy4uLih0aGlzLnN0YXRlLnRhcmdldHMgfHwgW10pLCBuZXdNZW1iZXJdO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3RhcmdldHM6IG5ld1RhcmdldHMsIGZpbHRlclRleHQ6ICcnfSk7XHJcbiAgICAgICAgcmV0dXJuIG5ld1RhcmdldHM7XHJcbiAgICB9XHJcblxyXG4gICAgX3N0YXJ0RG0gPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogdHJ1ZX0pO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldHMgPSB0aGlzLl9jb252ZXJ0RmlsdGVyKCk7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0SWRzID0gdGFyZ2V0cy5tYXAodCA9PiB0LnVzZXJJZCk7XHJcblxyXG4gICAgICAgIC8vIENoZWNrIGlmIHRoZXJlIGlzIGFscmVhZHkgYSBETSB3aXRoIHRoZXNlIHBlb3BsZSBhbmQgcmV1c2UgaXQgaWYgcG9zc2libGUuXHJcbiAgICAgICAgY29uc3QgZXhpc3RpbmdSb29tID0gRE1Sb29tTWFwLnNoYXJlZCgpLmdldERNUm9vbUZvcklkZW50aWZpZXJzKHRhcmdldElkcyk7XHJcbiAgICAgICAgaWYgKGV4aXN0aW5nUm9vbSkge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgICAgIHJvb21faWQ6IGV4aXN0aW5nUm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICBzaG91bGRfcGVlazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBqb2luaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjcmVhdGVSb29tT3B0aW9ucyA9IHtpbmxpbmVFcnJvcnM6IHRydWV9O1xyXG5cclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgICAgIC8vIENoZWNrIHdoZXRoZXIgYWxsIHVzZXJzIGhhdmUgdXBsb2FkZWQgZGV2aWNlIGtleXMgYmVmb3JlLlxyXG4gICAgICAgICAgICAvLyBJZiBzbywgZW5hYmxlIGVuY3J5cHRpb24gaW4gdGhlIG5ldyByb29tLlxyXG4gICAgICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFsbEhhdmVEZXZpY2VLZXlzID0gYXdhaXQgY2FuRW5jcnlwdFRvQWxsVXNlcnMoY2xpZW50LCB0YXJnZXRJZHMpO1xyXG4gICAgICAgICAgICBpZiAoYWxsSGF2ZURldmljZUtleXMpIHtcclxuICAgICAgICAgICAgICAgIGNyZWF0ZVJvb21PcHRpb25zLmVuY3J5cHRpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBDaGVjayBpZiBpdCdzIGEgdHJhZGl0aW9uYWwgRE0gYW5kIGNyZWF0ZSB0aGUgcm9vbSBpZiByZXF1aXJlZC5cclxuICAgICAgICAvLyBUT0RPOiBbQ2Fub25pY2FsIERNc10gUmVtb3ZlIHRoaXMgY2hlY2sgYW5kIGluc3RlYWQganVzdCBjcmVhdGUgdGhlIG11bHRpLXBlcnNvbiBETVxyXG4gICAgICAgIGxldCBjcmVhdGVSb29tUHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgICAgIGNvbnN0IGlzU2VsZiA9IHRhcmdldElkcy5sZW5ndGggPT09IDEgJiYgdGFyZ2V0SWRzWzBdID09PSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgaWYgKHRhcmdldElkcy5sZW5ndGggPT09IDEgJiYgIWlzU2VsZikge1xyXG4gICAgICAgICAgICBjcmVhdGVSb29tT3B0aW9ucy5kbVVzZXJJZCA9IHRhcmdldElkc1swXTtcclxuICAgICAgICAgICAgY3JlYXRlUm9vbVByb21pc2UgPSBjcmVhdGVSb29tKGNyZWF0ZVJvb21PcHRpb25zKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGlzU2VsZikge1xyXG4gICAgICAgICAgICBjcmVhdGVSb29tUHJvbWlzZSA9IGNyZWF0ZVJvb20oY3JlYXRlUm9vbU9wdGlvbnMpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIENyZWF0ZSBhIGJvcmluZyByb29tIGFuZCB0cnkgdG8gaW52aXRlIHRoZSB0YXJnZXRzIG1hbnVhbGx5LlxyXG4gICAgICAgICAgICBjcmVhdGVSb29tUHJvbWlzZSA9IGNyZWF0ZVJvb20oY3JlYXRlUm9vbU9wdGlvbnMpLnRoZW4ocm9vbUlkID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpbnZpdGVNdWx0aXBsZVRvUm9vbShyb29tSWQsIHRhcmdldElkcyk7XHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9zaG91bGRBYm9ydEFmdGVySW52aXRlRXJyb3IocmVzdWx0KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlOyAvLyBhYm9ydFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoZSBjcmVhdGVSb29tIGNhbGwgd2lsbCBzaG93IHRoZSByb29tIGZvciB1cywgc28gd2UgZG9uJ3QgbmVlZCB0byB3b3JyeSBhYm91dCB0aGF0LlxyXG4gICAgICAgIGNyZWF0ZVJvb21Qcm9taXNlLnRoZW4oYWJvcnQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoYWJvcnQgPT09IHRydWUpIHJldHVybjsgLy8gb25seSBhYm9ydCBvbiB0cnVlIGJvb2xlYW5zLCBub3Qgcm9vbUlkcyBvciBzb21ldGhpbmdcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZXJyb3JUZXh0OiBfdChcIldlIGNvdWxkbid0IGNyZWF0ZSB5b3VyIERNLiBQbGVhc2UgY2hlY2sgdGhlIHVzZXJzIHlvdSB3YW50IHRvIGludml0ZSBhbmQgdHJ5IGFnYWluLlwiKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9pbnZpdGVVc2VycyA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtidXN5OiB0cnVlfSk7XHJcbiAgICAgICAgdGhpcy5fY29udmVydEZpbHRlcigpO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldHMgPSB0aGlzLl9jb252ZXJ0RmlsdGVyKCk7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0SWRzID0gdGFyZ2V0cy5tYXAodCA9PiB0LnVzZXJJZCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gZmluZCB0aGUgcm9vbSB0byBpbnZpdGUgdXNlcnMgdG9cIik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQ6IF90KFwiU29tZXRoaW5nIHdlbnQgd3JvbmcgdHJ5aW5nIHRvIGludml0ZSB0aGUgdXNlcnMuXCIpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW52aXRlTXVsdGlwbGVUb1Jvb20odGhpcy5wcm9wcy5yb29tSWQsIHRhcmdldElkcykudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuX3Nob3VsZEFib3J0QWZ0ZXJJbnZpdGVFcnJvcihyZXN1bHQpKSB7IC8vIGhhbmRsZXMgc2V0dGluZyBlcnJvciBtZXNzYWdlIHRvb1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvclRleHQ6IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiV2UgY291bGRuJ3QgaW52aXRlIHRob3NlIHVzZXJzLiBQbGVhc2UgY2hlY2sgdGhlIHVzZXJzIHlvdSB3YW50IHRvIGludml0ZSBhbmQgdHJ5IGFnYWluLlwiLFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbktleURvd24gPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIHdoZW4gdGhlIGZpZWxkIGlzIGVtcHR5IGFuZCB0aGUgdXNlciBoaXRzIGJhY2tzcGFjZSByZW1vdmUgdGhlIHJpZ2h0LW1vc3QgdGFyZ2V0XHJcbiAgICAgICAgaWYgKCFlLnRhcmdldC52YWx1ZSAmJiAhdGhpcy5zdGF0ZS5idXN5ICYmIHRoaXMuc3RhdGUudGFyZ2V0cy5sZW5ndGggPiAwICYmIGUua2V5ID09PSBLZXkuQkFDS1NQQUNFICYmXHJcbiAgICAgICAgICAgICFlLmN0cmxLZXkgJiYgIWUuc2hpZnRLZXkgJiYgIWUubWV0YUtleVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbW92ZU1lbWJlcih0aGlzLnN0YXRlLnRhcmdldHNbdGhpcy5zdGF0ZS50YXJnZXRzLmxlbmd0aCAtIDFdKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF91cGRhdGVGaWx0ZXIgPSAoZSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHRlcm0gPSBlLnRhcmdldC52YWx1ZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtmaWx0ZXJUZXh0OiB0ZXJtfSk7XHJcblxyXG4gICAgICAgIC8vIERlYm91bmNlIHNlcnZlciBsb29rdXBzIHRvIHJlZHVjZSBzcGFtLiBXZSBkb24ndCBjbGVhciB0aGUgZXhpc3Rpbmcgc2VydmVyXHJcbiAgICAgICAgLy8gcmVzdWx0cyBiZWNhdXNlIHRoZXkgbWlnaHQgc3RpbGwgYmUgdmFndWVseSBhY2N1cmF0ZSwgbGlrZXdpc2UgZm9yIHJhY2VzIHdoaWNoXHJcbiAgICAgICAgLy8gY291bGQgaGFwcGVuIGhlcmUuXHJcbiAgICAgICAgaWYgKHRoaXMuX2RlYm91bmNlVGltZXIpIHtcclxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX2RlYm91bmNlVGltZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9kZWJvdW5jZVRpbWVyID0gc2V0VGltZW91dChhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZWFyY2hVc2VyRGlyZWN0b3J5KHt0ZXJtfSkudGhlbihhc3luYyByID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0ZXJtICE9PSB0aGlzLnN0YXRlLmZpbHRlclRleHQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBEaXNjYXJkIHRoZSByZXN1bHRzIC0gd2Ugd2VyZSBwcm9iYWJseSB0b28gc2xvdyBvbiB0aGUgc2VydmVyLXNpZGUgdG8gbWFrZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZXNlIHJlc3VsdHMgdXNlZnVsLiBUaGlzIGlzIGEgcmFjZSB3ZSB3YW50IHRvIGF2b2lkIGJlY2F1c2Ugd2UgY291bGQgb3ZlcndyaXRlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbW9yZSBhY2N1cmF0ZSByZXN1bHRzLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIXIucmVzdWx0cykgci5yZXN1bHRzID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2hpbGUgd2UncmUgaGVyZSwgdHJ5IGFuZCBhdXRvY29tcGxldGUgYSBzZWFyY2ggcmVzdWx0IGZvciB0aGUgbXhpZCBpdHNlbGZcclxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZXJlJ3Mgbm8gbWF0Y2hlcyAoYW5kIHRoZSBpbnB1dCBsb29rcyBsaWtlIGEgbXhpZCkuXHJcbiAgICAgICAgICAgICAgICBpZiAodGVybVswXSA9PT0gJ0AnICYmIHRlcm0uaW5kZXhPZignOicpID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb2ZpbGUgPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0UHJvZmlsZUluZm8odGVybSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcm9maWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiB3ZSBoYXZlIGEgcHJvZmlsZSwgd2UgaGF2ZSBlbm91Z2ggaW5mb3JtYXRpb24gdG8gYXNzdW1lIHRoYXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSBteGlkIGNhbiBiZSBpbnZpdGVkIC0gYWRkIGl0IHRvIHRoZSBsaXN0LiBXZSBzdGljayBpdCBhdCB0aGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRvcCBzbyBpdCBpcyBtb3N0IG9idmlvdXNseSBwcmVzZW50ZWQgdG8gdGhlIHVzZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByLnJlc3VsdHMuc3BsaWNlKDAsIDAsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyX2lkOiB0ZXJtLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlfbmFtZTogcHJvZmlsZVsnZGlzcGxheW5hbWUnXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJfdXJsOiBwcm9maWxlWydhdmF0YXJfdXJsJ10sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKFwiTm9uLWZhdGFsIGVycm9yIHRyeWluZyB0byBtYWtlIGFuIGludml0ZSBmb3IgYSB1c2VyIElEXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBBZGQgYSByZXN1bHQgYW55d2F5cywganVzdCB3aXRob3V0IGEgcHJvZmlsZS4gV2Ugc3RpY2sgaXQgYXQgdGhlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRvcCBzbyBpdCBpcyBtb3N0IG9idmlvdXNseSBwcmVzZW50ZWQgdG8gdGhlIHVzZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHIucmVzdWx0cy5zcGxpY2UoMCwgMCwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlcl9pZDogdGVybSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlfbmFtZTogdGVybSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF2YXRhcl91cmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBzZXJ2ZXJSZXN1bHRzTWl4aW46IHIucmVzdWx0cy5tYXAodSA9PiAoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHUudXNlcl9pZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcjogbmV3IERpcmVjdG9yeU1lbWJlcih1KSxcclxuICAgICAgICAgICAgICAgICAgICB9KSksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSkuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igc2VhcmNoaW5nIHVzZXIgZGlyZWN0b3J5OlwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzZXJ2ZXJSZXN1bHRzTWl4aW46IFtdfSk7IC8vIGNsZWFyIHJlc3VsdHMgYmVjYXVzZSBpdCdzIG1vZGVyYXRlbHkgZmF0YWxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBXaGVuZXZlciB3ZSBzZWFyY2ggdGhlIGRpcmVjdG9yeSwgYWxzbyB0cnkgdG8gc2VhcmNoIHRoZSBpZGVudGl0eSBzZXJ2ZXIuIEl0J3NcclxuICAgICAgICAgICAgLy8gYWxsIGRlYm91bmNlZCB0aGUgc2FtZSBhbnl3YXlzLlxyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY2FuVXNlSWRlbnRpdHlTZXJ2ZXIpIHtcclxuICAgICAgICAgICAgICAgIC8vIFRoZSB1c2VyIGRvZXNuJ3QgaGF2ZSBhbiBpZGVudGl0eSBzZXJ2ZXIgc2V0IC0gd2FybiB0aGVtIG9mIHRoYXQuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt0cnlpbmdJZGVudGl0eVNlcnZlcjogdHJ1ZX0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0ZXJtLmluZGV4T2YoJ0AnKSA+IDAgJiYgRW1haWwubG9va3NWYWxpZCh0ZXJtKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gU3RhcnQgb2ZmIGJ5IHN1Z2dlc3RpbmcgdGhlIHBsYWluIGVtYWlsIHdoaWxlIHdlIHRyeSBhbmQgcmVzb2x2ZSBpdFxyXG4gICAgICAgICAgICAgICAgLy8gdG8gYSByZWFsIGFjY291bnQuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBwZXIgYWJvdmU6IHRoZSB1c2VySWQgaXMgYSBsaWUgaGVyZSAtIGl0J3MganVzdCBhIHJlZ3VsYXIgaWRlbnRpZmllclxyXG4gICAgICAgICAgICAgICAgICAgIHRocmVlcGlkUmVzdWx0c01peGluOiBbe3VzZXI6IG5ldyBUaHJlZXBpZE1lbWJlcih0ZXJtKSwgdXNlcklkOiB0ZXJtfV0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXV0aENsaWVudCA9IG5ldyBJZGVudGl0eUF1dGhDbGllbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0b2tlbiA9IGF3YWl0IGF1dGhDbGllbnQuZ2V0QWNjZXNzVG9rZW4oKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGVybSAhPT0gdGhpcy5zdGF0ZS5maWx0ZXJUZXh0KSByZXR1cm47IC8vIGFiYW5kb24gaG9wZVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBsb29rdXAgPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubG9va3VwVGhyZWVQaWQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdlbWFpbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlcm0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZCwgLy8gY2FsbGJhY2tcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9rZW4sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGVybSAhPT0gdGhpcy5zdGF0ZS5maWx0ZXJUZXh0KSByZXR1cm47IC8vIGFiYW5kb24gaG9wZVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWxvb2t1cCB8fCAhbG9va3VwLm14aWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2Ugd2VyZW4ndCBhYmxlIHRvIGZpbmQgYW55b25lIC0gd2UncmUgYWxyZWFkeSBzdWdnZXN0aW5nIHRoZSBwbGFpbiBlbWFpbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBhcyBhbiBhbHRlcm5hdGl2ZSwgc28gZG8gbm90aGluZy5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gV2UgYXBwZW5kIHRoZSB1c2VyIHN1Z2dlc3Rpb24gdG8gZ2l2ZSB0aGUgdXNlciBhbiBvcHRpb24gdG8gY2xpY2tcclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGUgZW1haWwgYW55d2F5cywgYW5kIHNvIHdlIGRvbid0IGNhdXNlIHRoaW5ncyB0byBqdW1wIGFyb3VuZC4gSW5cclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGVvcnksIHRoZSB1c2VyIHdvdWxkIHNlZSB0aGUgdXNlciBwb3AgdXAgYW5kIHRoaW5rIFwiYWggeWVzLCB0aGF0XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcGVyc29uIVwiXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvZmlsZSA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRQcm9maWxlSW5mbyhsb29rdXAubXhpZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRlcm0gIT09IHRoaXMuc3RhdGUuZmlsdGVyVGV4dCB8fCAhcHJvZmlsZSkgcmV0dXJuOyAvLyBhYmFuZG9uIGhvcGVcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhyZWVwaWRSZXN1bHRzTWl4aW46IFsuLi50aGlzLnN0YXRlLnRocmVlcGlkUmVzdWx0c01peGluLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyOiBuZXcgRGlyZWN0b3J5TWVtYmVyKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyX2lkOiBsb29rdXAubXhpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5X25hbWU6IHByb2ZpbGUuZGlzcGxheW5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXZhdGFyX3VybDogcHJvZmlsZS5hdmF0YXJfdXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IGxvb2t1cC5teGlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igc2VhcmNoaW5nIGlkZW50aXR5IHNlcnZlcjpcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt0aHJlZXBpZFJlc3VsdHNNaXhpbjogW119KTsgLy8gY2xlYXIgcmVzdWx0cyBiZWNhdXNlIGl0J3MgbW9kZXJhdGVseSBmYXRhbFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMTUwKTsgLy8gMTUwbXMgZGVib3VuY2UgKGh1bWFuIHJlYWN0aW9uIHRpbWUgKyBzb21lKVxyXG4gICAgfTtcclxuXHJcbiAgICBfc2hvd01vcmVSZWNlbnRzID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe251bVJlY2VudHNTaG93bjogdGhpcy5zdGF0ZS5udW1SZWNlbnRzU2hvd24gKyBJTkNSRU1FTlRfUk9PTVNfU0hPV059KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Nob3dNb3JlU3VnZ2VzdGlvbnMgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bnVtU3VnZ2VzdGlvbnNTaG93bjogdGhpcy5zdGF0ZS5udW1TdWdnZXN0aW9uc1Nob3duICsgSU5DUkVNRU5UX1JPT01TX1NIT1dOfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF90b2dnbGVNZW1iZXIgPSAobWVtYmVyOiBNZW1iZXIpID0+IHtcclxuICAgICAgICBsZXQgZmlsdGVyVGV4dCA9IHRoaXMuc3RhdGUuZmlsdGVyVGV4dDtcclxuICAgICAgICBjb25zdCB0YXJnZXRzID0gdGhpcy5zdGF0ZS50YXJnZXRzLm1hcCh0ID0+IHQpOyAvLyBjaGVhcCBjbG9uZSBmb3IgbXV0YXRpb25cclxuICAgICAgICBjb25zdCBpZHggPSB0YXJnZXRzLmluZGV4T2YobWVtYmVyKTtcclxuICAgICAgICBpZiAoaWR4ID49IDApIHtcclxuICAgICAgICAgICAgdGFyZ2V0cy5zcGxpY2UoaWR4LCAxKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0YXJnZXRzLnB1c2gobWVtYmVyKTtcclxuICAgICAgICAgICAgZmlsdGVyVGV4dCA9IFwiXCI7IC8vIGNsZWFyIHRoZSBmaWx0ZXIgd2hlbiB0aGUgdXNlciBhY2NlcHRzIGEgc3VnZ2VzdGlvblxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHt0YXJnZXRzLCBmaWx0ZXJUZXh0fSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9yZW1vdmVNZW1iZXIgPSAobWVtYmVyOiBNZW1iZXIpID0+IHtcclxuICAgICAgICBjb25zdCB0YXJnZXRzID0gdGhpcy5zdGF0ZS50YXJnZXRzLm1hcCh0ID0+IHQpOyAvLyBjaGVhcCBjbG9uZSBmb3IgbXV0YXRpb25cclxuICAgICAgICBjb25zdCBpZHggPSB0YXJnZXRzLmluZGV4T2YobWVtYmVyKTtcclxuICAgICAgICBpZiAoaWR4ID49IDApIHtcclxuICAgICAgICAgICAgdGFyZ2V0cy5zcGxpY2UoaWR4LCAxKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dGFyZ2V0c30pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uUGFzdGUgPSBhc3luYyAoZSkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmZpbHRlclRleHQpIHtcclxuICAgICAgICAgICAgLy8gaWYgdGhlIHVzZXIgaGFzIGFscmVhZHkgdHlwZWQgc29tZXRoaW5nLCBqdXN0IGxldCB0aGVtXHJcbiAgICAgICAgICAgIC8vIHBhc3RlIG5vcm1hbGx5LlxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBQcmV2ZW50IHRoZSB0ZXh0IGJlaW5nIHBhc3RlZCBpbnRvIHRoZSB0ZXh0YXJlYVxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgLy8gUHJvY2VzcyBpdCBhcyBhIGxpc3Qgb2YgYWRkcmVzc2VzIHRvIGFkZCBpbnN0ZWFkXHJcbiAgICAgICAgY29uc3QgdGV4dCA9IGUuY2xpcGJvYXJkRGF0YS5nZXREYXRhKFwidGV4dFwiKTtcclxuICAgICAgICBjb25zdCBwb3NzaWJsZU1lbWJlcnMgPSBbXHJcbiAgICAgICAgICAgIC8vIElmIHdlIGNhbiBhdm9pZCBoaXR0aW5nIHRoZSBwcm9maWxlIGVuZHBvaW50LCB3ZSBzaG91bGQuXHJcbiAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUucmVjZW50cyxcclxuICAgICAgICAgICAgLi4udGhpcy5zdGF0ZS5zdWdnZXN0aW9ucyxcclxuICAgICAgICAgICAgLi4udGhpcy5zdGF0ZS5zZXJ2ZXJSZXN1bHRzTWl4aW4sXHJcbiAgICAgICAgICAgIC4uLnRoaXMuc3RhdGUudGhyZWVwaWRSZXN1bHRzTWl4aW4sXHJcbiAgICAgICAgXTtcclxuICAgICAgICBjb25zdCB0b0FkZCA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGZhaWxlZCA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHBvdGVudGlhbEFkZHJlc3NlcyA9IHRleHQuc3BsaXQoL1tcXHMsXSsvKS5tYXAocCA9PiBwLnRyaW0oKSkuZmlsdGVyKHAgPT4gISFwKTsgLy8gZmlsdGVyIGVtcHR5IHN0cmluZ3NcclxuICAgICAgICBmb3IgKGNvbnN0IGFkZHJlc3Mgb2YgcG90ZW50aWFsQWRkcmVzc2VzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IHBvc3NpYmxlTWVtYmVycy5maW5kKG0gPT4gbS51c2VySWQgPT09IGFkZHJlc3MpO1xyXG4gICAgICAgICAgICBpZiAobWVtYmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0b0FkZC5wdXNoKG1lbWJlci51c2VyKTtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoYWRkcmVzcy5pbmRleE9mKCdAJykgPiAwICYmIEVtYWlsLmxvb2tzVmFsaWQoYWRkcmVzcykpIHtcclxuICAgICAgICAgICAgICAgIHRvQWRkLnB1c2gobmV3IFRocmVlcGlkTWVtYmVyKGFkZHJlc3MpKTtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoYWRkcmVzc1swXSAhPT0gJ0AnKSB7XHJcbiAgICAgICAgICAgICAgICBmYWlsZWQucHVzaChhZGRyZXNzKTsgLy8gbm90IGEgdXNlciBJRFxyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwcm9maWxlID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFByb2ZpbGVJbmZvKGFkZHJlc3MpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGlzcGxheU5hbWUgPSBwcm9maWxlID8gcHJvZmlsZS5kaXNwbGF5bmFtZSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhdmF0YXJVcmwgPSBwcm9maWxlID8gcHJvZmlsZS5hdmF0YXJfdXJsIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIHRvQWRkLnB1c2gobmV3IERpcmVjdG9yeU1lbWJlcih7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcl9pZDogYWRkcmVzcyxcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5X25hbWU6IGRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIGF2YXRhcl91cmw6IGF2YXRhclVybCxcclxuICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIGxvb2tpbmcgdXAgcHJvZmlsZSBmb3IgXCIgKyBhZGRyZXNzKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgICAgICBmYWlsZWQucHVzaChhZGRyZXNzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGZhaWxlZC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5RdWVzdGlvbkRpYWxvZycpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdJbnZpdGUgUGFzdGUgRmFpbCcsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gZmluZCB0aGUgZm9sbG93aW5nIHVzZXJzJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJUaGUgZm9sbG93aW5nIHVzZXJzIG1pZ2h0IG5vdCBleGlzdCBvciBhcmUgaW52YWxpZCwgYW5kIGNhbm5vdCBiZSBpbnZpdGVkOiAlKGNzdk5hbWVzKXNcIixcclxuICAgICAgICAgICAgICAgICAgICB7Y3N2TmFtZXM6IGZhaWxlZC5qb2luKFwiLCBcIil9LFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIGJ1dHRvbjogX3QoJ09LJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dGFyZ2V0czogWy4uLnRoaXMuc3RhdGUudGFyZ2V0cywgLi4udG9BZGRdfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkNsaWNrSW5wdXRBcmVhID0gKGUpID0+IHtcclxuICAgICAgICAvLyBTdG9wIHRoZSBicm93c2VyIGZyb20gaGlnaGxpZ2h0aW5nIHRleHRcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX2VkaXRvclJlZiAmJiB0aGlzLl9lZGl0b3JSZWYuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9lZGl0b3JSZWYuY3VycmVudC5mb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uVXNlRGVmYXVsdElkZW50aXR5U2VydmVyQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBJUyBpbiBhY2NvdW50IGRhdGEuIEFjdHVhbGx5IHVzaW5nIGl0IG1heSB0cmlnZ2VyIHRlcm1zLlxyXG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC1ob29rcy9ydWxlcy1vZi1ob29rc1xyXG4gICAgICAgIHVzZURlZmF1bHRJZGVudGl0eVNlcnZlcigpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2NhblVzZUlkZW50aXR5U2VydmVyOiB0cnVlLCB0cnlpbmdJZGVudGl0eVNlcnZlcjogZmFsc2V9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uTWFuYWdlU2V0dGluZ3NDbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ3ZpZXdfdXNlcl9zZXR0aW5ncycgfSk7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9yZW5kZXJTZWN0aW9uKGtpbmQ6IFwicmVjZW50c1wifFwic3VnZ2VzdGlvbnNcIikge1xyXG4gICAgICAgIGxldCBzb3VyY2VNZW1iZXJzID0ga2luZCA9PT0gJ3JlY2VudHMnID8gdGhpcy5zdGF0ZS5yZWNlbnRzIDogdGhpcy5zdGF0ZS5zdWdnZXN0aW9ucztcclxuICAgICAgICBsZXQgc2hvd051bSA9IGtpbmQgPT09ICdyZWNlbnRzJyA/IHRoaXMuc3RhdGUubnVtUmVjZW50c1Nob3duIDogdGhpcy5zdGF0ZS5udW1TdWdnZXN0aW9uc1Nob3duO1xyXG4gICAgICAgIGNvbnN0IHNob3dNb3JlRm4gPSBraW5kID09PSAncmVjZW50cycgPyB0aGlzLl9zaG93TW9yZVJlY2VudHMuYmluZCh0aGlzKSA6IHRoaXMuX3Nob3dNb3JlU3VnZ2VzdGlvbnMuYmluZCh0aGlzKTtcclxuICAgICAgICBjb25zdCBsYXN0QWN0aXZlID0gKG0pID0+IGtpbmQgPT09ICdyZWNlbnRzJyA/IG0ubGFzdEFjdGl2ZSA6IG51bGw7XHJcbiAgICAgICAgbGV0IHNlY3Rpb25OYW1lID0ga2luZCA9PT0gJ3JlY2VudHMnID8gX3QoXCJSZWNlbnQgQ29udmVyc2F0aW9uc1wiKSA6IF90KFwiU3VnZ2VzdGlvbnNcIik7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmtpbmQgPT09IEtJTkRfSU5WSVRFKSB7XHJcbiAgICAgICAgICAgIHNlY3Rpb25OYW1lID0ga2luZCA9PT0gJ3JlY2VudHMnID8gX3QoXCJSZWNlbnRseSBEaXJlY3QgTWVzc2FnZWRcIikgOiBfdChcIlN1Z2dlc3Rpb25zXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gTWl4IGluIHRoZSBzZXJ2ZXIgcmVzdWx0cyBpZiB3ZSBoYXZlIGFueSwgYnV0IG9ubHkgaWYgd2UncmUgc2VhcmNoaW5nLiBXZSB0cmFjayB0aGUgYWRkaXRpb25hbFxyXG4gICAgICAgIC8vIG1lbWJlcnMgc2VwYXJhdGVseSBiZWNhdXNlIHdlIHdhbnQgdG8gZmlsdGVyIHNvdXJjZU1lbWJlcnMgYnV0IHRydXN0IHRoZSBtaXhpbiBhcnJheXMgdG8gaGF2ZVxyXG4gICAgICAgIC8vIHRoZSByaWdodCBtZW1iZXJzIGluIHRoZW0uXHJcbiAgICAgICAgbGV0IHByaW9yaXR5QWRkaXRpb25hbE1lbWJlcnMgPSBbXTsgLy8gU2hvd3MgdXAgYmVmb3JlIG91ciBvd24gc3VnZ2VzdGlvbnMsIGhpZ2hlciBxdWFsaXR5XHJcbiAgICAgICAgbGV0IG90aGVyQWRkaXRpb25hbE1lbWJlcnMgPSBbXTsgLy8gU2hvd3MgdXAgYWZ0ZXIgb3VyIG93biBzdWdnZXN0aW9ucywgbG93ZXIgcXVhbGl0eVxyXG4gICAgICAgIGNvbnN0IGhhc01peGlucyA9IHRoaXMuc3RhdGUuc2VydmVyUmVzdWx0c01peGluIHx8IHRoaXMuc3RhdGUudGhyZWVwaWRSZXN1bHRzTWl4aW47XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZmlsdGVyVGV4dCAmJiBoYXNNaXhpbnMgJiYga2luZCA9PT0gJ3N1Z2dlc3Rpb25zJykge1xyXG4gICAgICAgICAgICAvLyBXZSBkb24ndCB3YW50IHRvIGR1cGxpY2F0ZSBtZW1iZXJzIHRob3VnaCwgc28ganVzdCBleGNsdWRlIGFueW9uZSB3ZSd2ZSBhbHJlYWR5IHNlZW4uXHJcbiAgICAgICAgICAgIGNvbnN0IG5vdEFscmVhZHlFeGlzdHMgPSAodTogTWVtYmVyKTogYm9vbGVhbiA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gIXNvdXJjZU1lbWJlcnMuc29tZShtID0+IG0udXNlcklkID09PSB1LnVzZXJJZClcclxuICAgICAgICAgICAgICAgICAgICAmJiAhcHJpb3JpdHlBZGRpdGlvbmFsTWVtYmVycy5zb21lKG0gPT4gbS51c2VySWQgPT09IHUudXNlcklkKVxyXG4gICAgICAgICAgICAgICAgICAgICYmICFvdGhlckFkZGl0aW9uYWxNZW1iZXJzLnNvbWUobSA9PiBtLnVzZXJJZCA9PT0gdS51c2VySWQpO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgb3RoZXJBZGRpdGlvbmFsTWVtYmVycyA9IHRoaXMuc3RhdGUuc2VydmVyUmVzdWx0c01peGluLmZpbHRlcihub3RBbHJlYWR5RXhpc3RzKTtcclxuICAgICAgICAgICAgcHJpb3JpdHlBZGRpdGlvbmFsTWVtYmVycyA9IHRoaXMuc3RhdGUudGhyZWVwaWRSZXN1bHRzTWl4aW4uZmlsdGVyKG5vdEFscmVhZHlFeGlzdHMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBoYXNBZGRpdGlvbmFsTWVtYmVycyA9IHByaW9yaXR5QWRkaXRpb25hbE1lbWJlcnMubGVuZ3RoID4gMCB8fCBvdGhlckFkZGl0aW9uYWxNZW1iZXJzLmxlbmd0aCA+IDA7XHJcblxyXG4gICAgICAgIC8vIEhpZGUgdGhlIHNlY3Rpb24gaWYgdGhlcmUncyBub3RoaW5nIHRvIGZpbHRlciBieVxyXG4gICAgICAgIGlmIChzb3VyY2VNZW1iZXJzLmxlbmd0aCA9PT0gMCAmJiAhaGFzQWRkaXRpb25hbE1lbWJlcnMpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICAvLyBEbyBzb21lIHNpbXBsZSBmaWx0ZXJpbmcgb24gdGhlIGlucHV0IGJlZm9yZSBnb2luZyBtdWNoIGZ1cnRoZXIuIElmIHdlIGdldCBubyByZXN1bHRzLCBzYXkgc28uXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZmlsdGVyVGV4dCkge1xyXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJCeSA9IHRoaXMuc3RhdGUuZmlsdGVyVGV4dC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICBzb3VyY2VNZW1iZXJzID0gc291cmNlTWVtYmVyc1xyXG4gICAgICAgICAgICAgICAgLmZpbHRlcihtID0+IG0udXNlci5uYW1lLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoZmlsdGVyQnkpIHx8IG0udXNlcklkLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoZmlsdGVyQnkpKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChzb3VyY2VNZW1iZXJzLmxlbmd0aCA9PT0gMCAmJiAhaGFzQWRkaXRpb25hbE1lbWJlcnMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ19zZWN0aW9uJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgzPntzZWN0aW9uTmFtZX08L2gzPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD57X3QoXCJObyByZXN1bHRzXCIpfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIE5vdyB3ZSBtaXggaW4gdGhlIGFkZGl0aW9uYWwgbWVtYmVycy4gQWdhaW4sIHdlIHByZXN1bWUgdGhlc2UgaGF2ZSBhbHJlYWR5IGJlZW4gZmlsdGVyZWQuIFdlXHJcbiAgICAgICAgLy8gYWxzbyBhc3N1bWUgdGhleSBhcmUgbW9yZSByZWxldmFudCB0aGFuIG91ciBzdWdnZXN0aW9ucyBhbmQgcHJlcGVuZCB0aGVtIHRvIHRoZSBsaXN0LlxyXG4gICAgICAgIHNvdXJjZU1lbWJlcnMgPSBbLi4ucHJpb3JpdHlBZGRpdGlvbmFsTWVtYmVycywgLi4uc291cmNlTWVtYmVycywgLi4ub3RoZXJBZGRpdGlvbmFsTWVtYmVyc107XHJcblxyXG4gICAgICAgIC8vIElmIHdlJ3JlIGdvaW5nIHRvIGhpZGUgb25lIG1lbWJlciBiZWhpbmQgJ3Nob3cgbW9yZScsIGp1c3QgdXNlIHVwIHRoZSBzcGFjZSBvZiB0aGUgYnV0dG9uXHJcbiAgICAgICAgLy8gd2l0aCB0aGUgbWVtYmVyJ3MgdGlsZSBpbnN0ZWFkLlxyXG4gICAgICAgIGlmIChzaG93TnVtID09PSBzb3VyY2VNZW1iZXJzLmxlbmd0aCAtIDEpIHNob3dOdW0rKztcclxuXHJcbiAgICAgICAgLy8gLnNsaWNlKCkgd2lsbCByZXR1cm4gYW4gaW5jb21wbGV0ZSBhcnJheSBidXQgd29uJ3QgZXJyb3Igb24gdXMgaWYgd2UgZ28gdG9vIGZhclxyXG4gICAgICAgIGNvbnN0IHRvUmVuZGVyID0gc291cmNlTWVtYmVycy5zbGljZSgwLCBzaG93TnVtKTtcclxuICAgICAgICBjb25zdCBoYXNNb3JlID0gdG9SZW5kZXIubGVuZ3RoIDwgc291cmNlTWVtYmVycy5sZW5ndGg7XHJcblxyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvblwiKTtcclxuICAgICAgICBsZXQgc2hvd01vcmUgPSBudWxsO1xyXG4gICAgICAgIGlmIChoYXNNb3JlKSB7XHJcbiAgICAgICAgICAgIHNob3dNb3JlID0gKFxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17c2hvd01vcmVGbn0ga2luZD1cImxpbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJTaG93IG1vcmVcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0aWxlcyA9IHRvUmVuZGVyLm1hcChyID0+IChcclxuICAgICAgICAgICAgPERNUm9vbVRpbGVcclxuICAgICAgICAgICAgICAgIG1lbWJlcj17ci51c2VyfVxyXG4gICAgICAgICAgICAgICAgbGFzdEFjdGl2ZVRzPXtsYXN0QWN0aXZlKHIpfVxyXG4gICAgICAgICAgICAgICAga2V5PXtyLnVzZXJJZH1cclxuICAgICAgICAgICAgICAgIG9uVG9nZ2xlPXt0aGlzLl90b2dnbGVNZW1iZXJ9XHJcbiAgICAgICAgICAgICAgICBoaWdobGlnaHRXb3JkPXt0aGlzLnN0YXRlLmZpbHRlclRleHR9XHJcbiAgICAgICAgICAgICAgICBpc1NlbGVjdGVkPXt0aGlzLnN0YXRlLnRhcmdldHMuc29tZSh0ID0+IHQudXNlcklkID09PSByLnVzZXJJZCl9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKSk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ19zZWN0aW9uJz5cclxuICAgICAgICAgICAgICAgIDxoMz57c2VjdGlvbk5hbWV9PC9oMz5cclxuICAgICAgICAgICAgICAgIHt0aWxlc31cclxuICAgICAgICAgICAgICAgIHtzaG93TW9yZX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyRWRpdG9yKCkge1xyXG4gICAgICAgIGNvbnN0IHRhcmdldHMgPSB0aGlzLnN0YXRlLnRhcmdldHMubWFwKHQgPT4gKFxyXG4gICAgICAgICAgICA8RE1Vc2VyVGlsZSBtZW1iZXI9e3R9IG9uUmVtb3ZlPXshdGhpcy5zdGF0ZS5idXN5ICYmIHRoaXMuX3JlbW92ZU1lbWJlcn0ga2V5PXt0LnVzZXJJZH0gLz5cclxuICAgICAgICApKTtcclxuICAgICAgICBjb25zdCBpbnB1dCA9IChcclxuICAgICAgICAgICAgPHRleHRhcmVhXHJcbiAgICAgICAgICAgICAgICByb3dzPXsxfVxyXG4gICAgICAgICAgICAgICAgb25LZXlEb3duPXt0aGlzLl9vbktleURvd259XHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fdXBkYXRlRmlsdGVyfVxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUuZmlsdGVyVGV4dH1cclxuICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fZWRpdG9yUmVmfVxyXG4gICAgICAgICAgICAgICAgb25QYXN0ZT17dGhpcy5fb25QYXN0ZX1cclxuICAgICAgICAgICAgICAgIGF1dG9Gb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmJ1c3l9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX2VkaXRvcicgb25DbGljaz17dGhpcy5fb25DbGlja0lucHV0QXJlYX0+XHJcbiAgICAgICAgICAgICAgICB7dGFyZ2V0c31cclxuICAgICAgICAgICAgICAgIHtpbnB1dH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVySWRlbnRpdHlTZXJ2ZXJXYXJuaW5nKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS50cnlpbmdJZGVudGl0eVNlcnZlciB8fCB0aGlzLnN0YXRlLmNhblVzZUlkZW50aXR5U2VydmVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZGVmYXVsdElkZW50aXR5U2VydmVyVXJsID0gZ2V0RGVmYXVsdElkZW50aXR5U2VydmVyVXJsKCk7XHJcbiAgICAgICAgaWYgKGRlZmF1bHRJZGVudGl0eVNlcnZlclVybCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nX2lkZW50aXR5U2VydmVyXCI+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVXNlIGFuIGlkZW50aXR5IHNlcnZlciB0byBpbnZpdGUgYnkgZW1haWwuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkZWZhdWx0PlVzZSB0aGUgZGVmYXVsdCAoJShkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lKXMpPC9kZWZhdWx0PiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvciBtYW5hZ2UgaW4gPHNldHRpbmdzPlNldHRpbmdzPC9zZXR0aW5ncz4uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lOiBhYmJyZXZpYXRlVXJsKGRlZmF1bHRJZGVudGl0eVNlcnZlclVybCksXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IHN1YiA9PiA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9e3RoaXMuX29uVXNlRGVmYXVsdElkZW50aXR5U2VydmVyQ2xpY2t9PntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHN1YiA9PiA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9e3RoaXMuX29uTWFuYWdlU2V0dGluZ3NDbGlja30+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICApfTwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NQaWNrZXJEaWFsb2dfaWRlbnRpdHlTZXJ2ZXJcIj57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJVc2UgYW4gaWRlbnRpdHkgc2VydmVyIHRvIGludml0ZSBieSBlbWFpbC4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiTWFuYWdlIGluIDxzZXR0aW5ncz5TZXR0aW5nczwvc2V0dGluZ3M+LlwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiBzdWIgPT4gPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXt0aGlzLl9vbk1hbmFnZVNldHRpbmdzQ2xpY2t9PntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKX08L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b25cIik7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG5cclxuICAgICAgICBsZXQgc3Bpbm5lciA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuYnVzeSkge1xyXG4gICAgICAgICAgICBzcGlubmVyID0gPFNwaW5uZXIgdz17MjB9IGg9ezIwfSAvPjtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBsZXQgdGl0bGU7XHJcbiAgICAgICAgbGV0IGhlbHBUZXh0O1xyXG4gICAgICAgIGxldCBidXR0b25UZXh0O1xyXG4gICAgICAgIGxldCBnb0J1dHRvbkZuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5raW5kID09PSBLSU5EX0RNKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKTtcclxuXHJcbiAgICAgICAgICAgIHRpdGxlID0gX3QoXCJEaXJlY3QgTWVzc2FnZXNcIik7XHJcbiAgICAgICAgICAgIGhlbHBUZXh0ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIlN0YXJ0IGEgY29udmVyc2F0aW9uIHdpdGggc29tZW9uZSB1c2luZyB0aGVpciBuYW1lLCB1c2VybmFtZSAobGlrZSA8dXNlcklkLz4pIG9yIGVtYWlsIGFkZHJlc3MuXCIsXHJcbiAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgIHt1c2VySWQ6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gPGEgaHJlZj17bWFrZVVzZXJQZXJtYWxpbmsodXNlcklkKX0gcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPnt1c2VySWR9PC9hPjtcclxuICAgICAgICAgICAgICAgIH19LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBidXR0b25UZXh0ID0gX3QoXCJHb1wiKTtcclxuICAgICAgICAgICAgZ29CdXR0b25GbiA9IHRoaXMuX3N0YXJ0RG07XHJcbiAgICAgICAgfSBlbHNlIHsgLy8gS0lORF9JTlZJVEVcclxuICAgICAgICAgICAgdGl0bGUgPSBfdChcIkludml0ZSB0byB0aGlzIHJvb21cIik7XHJcbiAgICAgICAgICAgIGhlbHBUZXh0ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIklmIHlvdSBjYW4ndCBmaW5kIHNvbWVvbmUsIGFzayB0aGVtIGZvciB0aGVpciB1c2VybmFtZSAoZS5nLiBAdXNlcjpzZXJ2ZXIuY29tKSBvciBcIiArXHJcbiAgICAgICAgICAgICAgICBcIjxhPnNoYXJlIHRoaXMgcm9vbTwvYT4uXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGE6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9e21ha2VSb29tUGVybWFsaW5rKHRoaXMucHJvcHMucm9vbUlkKX0gcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGJ1dHRvblRleHQgPSBfdChcIkludml0ZVwiKTtcclxuICAgICAgICAgICAgZ29CdXR0b25GbiA9IHRoaXMuX2ludml0ZVVzZXJzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaGFzU2VsZWN0aW9uID0gdGhpcy5zdGF0ZS50YXJnZXRzLmxlbmd0aCA+IDBcclxuICAgICAgICAgICAgfHwgKHRoaXMuc3RhdGUuZmlsdGVyVGV4dCAmJiB0aGlzLnN0YXRlLmZpbHRlclRleHQuaW5jbHVkZXMoJ0AnKSk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2dcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nJ1xyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9e3RpdGxlfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX2NvbnRlbnQnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX2hlbHBUZXh0Jz57aGVscFRleHR9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfYWRkcmVzc0Jhcic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJFZGl0b3IoKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludml0ZURpYWxvZ19idXR0b25BbmRTcGlubmVyJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2luZD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2dvQnV0dG9uRm59XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdteF9JbnZpdGVEaWFsb2dfZ29CdXR0b24nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUuYnVzeSB8fCAhaGFzU2VsZWN0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtidXR0b25UZXh0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3NwaW5uZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJJZGVudGl0eVNlcnZlcldhcm5pbmcoKX1cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nZXJyb3InPnt0aGlzLnN0YXRlLmVycm9yVGV4dH08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfSW52aXRlRGlhbG9nX3VzZXJTZWN0aW9ucyc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJTZWN0aW9uKCdyZWNlbnRzJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJTZWN0aW9uKCdzdWdnZXN0aW9ucycpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==