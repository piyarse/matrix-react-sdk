"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _TabbedView = _interopRequireWildcard(require("../../structures/TabbedView"));

var _languageHandler = require("../../../languageHandler");

var _GeneralUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/GeneralUserSettingsTab"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _LabsUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/LabsUserSettingsTab"));

var _SecurityUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/SecurityUserSettingsTab"));

var _NotificationUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/NotificationUserSettingsTab"));

var _PreferencesUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/PreferencesUserSettingsTab"));

var _VoiceUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/VoiceUserSettingsTab"));

var _HelpUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/HelpUserSettingsTab"));

var _FlairUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/FlairUserSettingsTab"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _MjolnirUserSettingsTab = _interopRequireDefault(require("../settings/tabs/user/MjolnirUserSettingsTab"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class UserSettingsDialog extends _react.default.Component {
  constructor() {
    super();
    this.state = {
      mjolnirEnabled: _SettingsStore.default.isFeatureEnabled("feature_mjolnir")
    };
  }

  componentDidMount()
  /*: void*/
  {
    this._mjolnirWatcher = _SettingsStore.default.watchSetting("feature_mjolnir", null, this._mjolnirChanged.bind(this));
  }

  componentWillUnmount()
  /*: void*/
  {
    _SettingsStore.default.unwatchSetting(this._mjolnirWatcher);
  }

  _mjolnirChanged(settingName, roomId, atLevel, newValue) {
    // We can cheat because we know what levels a feature is tracked at, and how it is tracked
    this.setState({
      mjolnirEnabled: newValue
    });
  }

  _getTabs() {
    const tabs = [];
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("General"), "mx_UserSettingsDialog_settingsIcon", _react.default.createElement(_GeneralUserSettingsTab.default, {
      closeSettingsFn: this.props.onFinished
    })));
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Flair"), "mx_UserSettingsDialog_flairIcon", _react.default.createElement(_FlairUserSettingsTab.default, null)));
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Notifications"), "mx_UserSettingsDialog_bellIcon", _react.default.createElement(_NotificationUserSettingsTab.default, null)));
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Preferences"), "mx_UserSettingsDialog_preferencesIcon", _react.default.createElement(_PreferencesUserSettingsTab.default, null)));
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Voice & Video"), "mx_UserSettingsDialog_voiceIcon", _react.default.createElement(_VoiceUserSettingsTab.default, null)));
    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Security & Privacy"), "mx_UserSettingsDialog_securityIcon", _react.default.createElement(_SecurityUserSettingsTab.default, null)));

    if (_SdkConfig.default.get()['showLabsSettings'] || _SettingsStore.default.getLabsFeatures().length > 0) {
      tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Labs"), "mx_UserSettingsDialog_labsIcon", _react.default.createElement(_LabsUserSettingsTab.default, null)));
    }

    if (this.state.mjolnirEnabled) {
      tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Ignored users"), "mx_UserSettingsDialog_mjolnirIcon", _react.default.createElement(_MjolnirUserSettingsTab.default, null)));
    }

    tabs.push(new _TabbedView.Tab((0, _languageHandler._td)("Help & About"), "mx_UserSettingsDialog_helpIcon", _react.default.createElement(_HelpUserSettingsTab.default, {
      closeSettingsFn: this.props.onFinished
    })));
    return tabs;
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    return _react.default.createElement(BaseDialog, {
      className: "mx_UserSettingsDialog",
      hasCancel: true,
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)("Settings")
    }, _react.default.createElement("div", {
      className: "ms_SettingsDialog_content"
    }, _react.default.createElement(_TabbedView.default, {
      tabs: this._getTabs()
    })));
  }

}

exports.default = UserSettingsDialog;
(0, _defineProperty2.default)(UserSettingsDialog, "propTypes", {
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvVXNlclNldHRpbmdzRGlhbG9nLmpzIl0sIm5hbWVzIjpbIlVzZXJTZXR0aW5nc0RpYWxvZyIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJzdGF0ZSIsIm1qb2xuaXJFbmFibGVkIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJjb21wb25lbnREaWRNb3VudCIsIl9tam9sbmlyV2F0Y2hlciIsIndhdGNoU2V0dGluZyIsIl9tam9sbmlyQ2hhbmdlZCIsImJpbmQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVud2F0Y2hTZXR0aW5nIiwic2V0dGluZ05hbWUiLCJyb29tSWQiLCJhdExldmVsIiwibmV3VmFsdWUiLCJzZXRTdGF0ZSIsIl9nZXRUYWJzIiwidGFicyIsInB1c2giLCJUYWIiLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJTZGtDb25maWciLCJnZXQiLCJnZXRMYWJzRmVhdHVyZXMiLCJsZW5ndGgiLCJyZW5kZXIiLCJCYXNlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiUHJvcFR5cGVzIiwiZnVuYyIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBaENBOzs7Ozs7Ozs7Ozs7Ozs7O0FBa0NlLE1BQU1BLGtCQUFOLFNBQWlDQyxlQUFNQyxTQUF2QyxDQUFpRDtBQUs1REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFFQSxTQUFLQyxLQUFMLEdBQWE7QUFDVEMsTUFBQUEsY0FBYyxFQUFFQyx1QkFBY0MsZ0JBQWQsQ0FBK0IsaUJBQS9CO0FBRFAsS0FBYjtBQUdIOztBQUVEQyxFQUFBQSxpQkFBaUI7QUFBQTtBQUFTO0FBQ3RCLFNBQUtDLGVBQUwsR0FBdUJILHVCQUFjSSxZQUFkLENBQTJCLGlCQUEzQixFQUE4QyxJQUE5QyxFQUFvRCxLQUFLQyxlQUFMLENBQXFCQyxJQUFyQixDQUEwQixJQUExQixDQUFwRCxDQUF2QjtBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0I7QUFBQTtBQUFTO0FBQ3pCUCwyQkFBY1EsY0FBZCxDQUE2QixLQUFLTCxlQUFsQztBQUNIOztBQUVERSxFQUFBQSxlQUFlLENBQUNJLFdBQUQsRUFBY0MsTUFBZCxFQUFzQkMsT0FBdEIsRUFBK0JDLFFBQS9CLEVBQXlDO0FBQ3BEO0FBQ0EsU0FBS0MsUUFBTCxDQUFjO0FBQUNkLE1BQUFBLGNBQWMsRUFBRWE7QUFBakIsS0FBZDtBQUNIOztBQUVERSxFQUFBQSxRQUFRLEdBQUc7QUFDUCxVQUFNQyxJQUFJLEdBQUcsRUFBYjtBQUVBQSxJQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVSxJQUFJQyxlQUFKLENBQ04sMEJBQUksU0FBSixDQURNLEVBRU4sb0NBRk0sRUFHTiw2QkFBQywrQkFBRDtBQUF3QixNQUFBLGVBQWUsRUFBRSxLQUFLQyxLQUFMLENBQVdDO0FBQXBELE1BSE0sQ0FBVjtBQUtBSixJQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVSxJQUFJQyxlQUFKLENBQ04sMEJBQUksT0FBSixDQURNLEVBRU4saUNBRk0sRUFHTiw2QkFBQyw2QkFBRCxPQUhNLENBQVY7QUFLQUYsSUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVUsSUFBSUMsZUFBSixDQUNOLDBCQUFJLGVBQUosQ0FETSxFQUVOLGdDQUZNLEVBR04sNkJBQUMsb0NBQUQsT0FITSxDQUFWO0FBS0FGLElBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVLElBQUlDLGVBQUosQ0FDTiwwQkFBSSxhQUFKLENBRE0sRUFFTix1Q0FGTSxFQUdOLDZCQUFDLG1DQUFELE9BSE0sQ0FBVjtBQUtBRixJQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVSxJQUFJQyxlQUFKLENBQ04sMEJBQUksZUFBSixDQURNLEVBRU4saUNBRk0sRUFHTiw2QkFBQyw2QkFBRCxPQUhNLENBQVY7QUFLQUYsSUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVUsSUFBSUMsZUFBSixDQUNOLDBCQUFJLG9CQUFKLENBRE0sRUFFTixvQ0FGTSxFQUdOLDZCQUFDLGdDQUFELE9BSE0sQ0FBVjs7QUFLQSxRQUFJRyxtQkFBVUMsR0FBVixHQUFnQixrQkFBaEIsS0FBdUNyQix1QkFBY3NCLGVBQWQsR0FBZ0NDLE1BQWhDLEdBQXlDLENBQXBGLEVBQXVGO0FBQ25GUixNQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVSxJQUFJQyxlQUFKLENBQ04sMEJBQUksTUFBSixDQURNLEVBRU4sZ0NBRk0sRUFHTiw2QkFBQyw0QkFBRCxPQUhNLENBQVY7QUFLSDs7QUFDRCxRQUFJLEtBQUtuQixLQUFMLENBQVdDLGNBQWYsRUFBK0I7QUFDM0JnQixNQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVSxJQUFJQyxlQUFKLENBQ04sMEJBQUksZUFBSixDQURNLEVBRU4sbUNBRk0sRUFHTiw2QkFBQywrQkFBRCxPQUhNLENBQVY7QUFLSDs7QUFDREYsSUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVUsSUFBSUMsZUFBSixDQUNOLDBCQUFJLGNBQUosQ0FETSxFQUVOLGdDQUZNLEVBR04sNkJBQUMsNEJBQUQ7QUFBcUIsTUFBQSxlQUFlLEVBQUUsS0FBS0MsS0FBTCxDQUFXQztBQUFqRCxNQUhNLENBQVY7QUFNQSxXQUFPSixJQUFQO0FBQ0g7O0FBRURTLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUVBLFdBQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsU0FBUyxFQUFDLHVCQUF0QjtBQUE4QyxNQUFBLFNBQVMsRUFBRSxJQUF6RDtBQUNZLE1BQUEsVUFBVSxFQUFFLEtBQUtULEtBQUwsQ0FBV0MsVUFEbkM7QUFDK0MsTUFBQSxLQUFLLEVBQUUseUJBQUcsVUFBSDtBQUR0RCxPQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLG1CQUFEO0FBQVksTUFBQSxJQUFJLEVBQUUsS0FBS0wsUUFBTDtBQUFsQixNQURKLENBRkosQ0FESjtBQVFIOztBQTdGMkQ7Ozs4QkFBM0NwQixrQixlQUNFO0FBQ2Z5QixFQUFBQSxVQUFVLEVBQUVTLG1CQUFVQyxJQUFWLENBQWVDO0FBRFosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IFRhYmJlZFZpZXcsIHtUYWJ9IGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL1RhYmJlZFZpZXdcIjtcclxuaW1wb3J0IHtfdCwgX3RkfSBmcm9tIFwiLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCBHZW5lcmFsVXNlclNldHRpbmdzVGFiIGZyb20gXCIuLi9zZXR0aW5ncy90YWJzL3VzZXIvR2VuZXJhbFVzZXJTZXR0aW5nc1RhYlwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgTGFic1VzZXJTZXR0aW5nc1RhYiBmcm9tIFwiLi4vc2V0dGluZ3MvdGFicy91c2VyL0xhYnNVc2VyU2V0dGluZ3NUYWJcIjtcclxuaW1wb3J0IFNlY3VyaXR5VXNlclNldHRpbmdzVGFiIGZyb20gXCIuLi9zZXR0aW5ncy90YWJzL3VzZXIvU2VjdXJpdHlVc2VyU2V0dGluZ3NUYWJcIjtcclxuaW1wb3J0IE5vdGlmaWNhdGlvblVzZXJTZXR0aW5nc1RhYiBmcm9tIFwiLi4vc2V0dGluZ3MvdGFicy91c2VyL05vdGlmaWNhdGlvblVzZXJTZXR0aW5nc1RhYlwiO1xyXG5pbXBvcnQgUHJlZmVyZW5jZXNVc2VyU2V0dGluZ3NUYWIgZnJvbSBcIi4uL3NldHRpbmdzL3RhYnMvdXNlci9QcmVmZXJlbmNlc1VzZXJTZXR0aW5nc1RhYlwiO1xyXG5pbXBvcnQgVm9pY2VVc2VyU2V0dGluZ3NUYWIgZnJvbSBcIi4uL3NldHRpbmdzL3RhYnMvdXNlci9Wb2ljZVVzZXJTZXR0aW5nc1RhYlwiO1xyXG5pbXBvcnQgSGVscFVzZXJTZXR0aW5nc1RhYiBmcm9tIFwiLi4vc2V0dGluZ3MvdGFicy91c2VyL0hlbHBVc2VyU2V0dGluZ3NUYWJcIjtcclxuaW1wb3J0IEZsYWlyVXNlclNldHRpbmdzVGFiIGZyb20gXCIuLi9zZXR0aW5ncy90YWJzL3VzZXIvRmxhaXJVc2VyU2V0dGluZ3NUYWJcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gXCIuLi8uLi8uLi9TZGtDb25maWdcIjtcclxuaW1wb3J0IE1qb2xuaXJVc2VyU2V0dGluZ3NUYWIgZnJvbSBcIi4uL3NldHRpbmdzL3RhYnMvdXNlci9Nam9sbmlyVXNlclNldHRpbmdzVGFiXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVc2VyU2V0dGluZ3NEaWFsb2cgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBtam9sbmlyRW5hYmxlZDogU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9tam9sbmlyXCIpLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fbWpvbG5pcldhdGNoZXIgPSBTZXR0aW5nc1N0b3JlLndhdGNoU2V0dGluZyhcImZlYXR1cmVfbWpvbG5pclwiLCBudWxsLCB0aGlzLl9tam9sbmlyQ2hhbmdlZC5iaW5kKHRoaXMpKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpOiB2b2lkIHtcclxuICAgICAgICBTZXR0aW5nc1N0b3JlLnVud2F0Y2hTZXR0aW5nKHRoaXMuX21qb2xuaXJXYXRjaGVyKTtcclxuICAgIH1cclxuXHJcbiAgICBfbWpvbG5pckNoYW5nZWQoc2V0dGluZ05hbWUsIHJvb21JZCwgYXRMZXZlbCwgbmV3VmFsdWUpIHtcclxuICAgICAgICAvLyBXZSBjYW4gY2hlYXQgYmVjYXVzZSB3ZSBrbm93IHdoYXQgbGV2ZWxzIGEgZmVhdHVyZSBpcyB0cmFja2VkIGF0LCBhbmQgaG93IGl0IGlzIHRyYWNrZWRcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHttam9sbmlyRW5hYmxlZDogbmV3VmFsdWV9KTtcclxuICAgIH1cclxuXHJcbiAgICBfZ2V0VGFicygpIHtcclxuICAgICAgICBjb25zdCB0YWJzID0gW107XHJcblxyXG4gICAgICAgIHRhYnMucHVzaChuZXcgVGFiKFxyXG4gICAgICAgICAgICBfdGQoXCJHZW5lcmFsXCIpLFxyXG4gICAgICAgICAgICBcIm14X1VzZXJTZXR0aW5nc0RpYWxvZ19zZXR0aW5nc0ljb25cIixcclxuICAgICAgICAgICAgPEdlbmVyYWxVc2VyU2V0dGluZ3NUYWIgY2xvc2VTZXR0aW5nc0ZuPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9IC8+LFxyXG4gICAgICAgICkpO1xyXG4gICAgICAgIHRhYnMucHVzaChuZXcgVGFiKFxyXG4gICAgICAgICAgICBfdGQoXCJGbGFpclwiKSxcclxuICAgICAgICAgICAgXCJteF9Vc2VyU2V0dGluZ3NEaWFsb2dfZmxhaXJJY29uXCIsXHJcbiAgICAgICAgICAgIDxGbGFpclVzZXJTZXR0aW5nc1RhYiAvPixcclxuICAgICAgICApKTtcclxuICAgICAgICB0YWJzLnB1c2gobmV3IFRhYihcclxuICAgICAgICAgICAgX3RkKFwiTm90aWZpY2F0aW9uc1wiKSxcclxuICAgICAgICAgICAgXCJteF9Vc2VyU2V0dGluZ3NEaWFsb2dfYmVsbEljb25cIixcclxuICAgICAgICAgICAgPE5vdGlmaWNhdGlvblVzZXJTZXR0aW5nc1RhYiAvPixcclxuICAgICAgICApKTtcclxuICAgICAgICB0YWJzLnB1c2gobmV3IFRhYihcclxuICAgICAgICAgICAgX3RkKFwiUHJlZmVyZW5jZXNcIiksXHJcbiAgICAgICAgICAgIFwibXhfVXNlclNldHRpbmdzRGlhbG9nX3ByZWZlcmVuY2VzSWNvblwiLFxyXG4gICAgICAgICAgICA8UHJlZmVyZW5jZXNVc2VyU2V0dGluZ3NUYWIgLz4sXHJcbiAgICAgICAgKSk7XHJcbiAgICAgICAgdGFicy5wdXNoKG5ldyBUYWIoXHJcbiAgICAgICAgICAgIF90ZChcIlZvaWNlICYgVmlkZW9cIiksXHJcbiAgICAgICAgICAgIFwibXhfVXNlclNldHRpbmdzRGlhbG9nX3ZvaWNlSWNvblwiLFxyXG4gICAgICAgICAgICA8Vm9pY2VVc2VyU2V0dGluZ3NUYWIgLz4sXHJcbiAgICAgICAgKSk7XHJcbiAgICAgICAgdGFicy5wdXNoKG5ldyBUYWIoXHJcbiAgICAgICAgICAgIF90ZChcIlNlY3VyaXR5ICYgUHJpdmFjeVwiKSxcclxuICAgICAgICAgICAgXCJteF9Vc2VyU2V0dGluZ3NEaWFsb2dfc2VjdXJpdHlJY29uXCIsXHJcbiAgICAgICAgICAgIDxTZWN1cml0eVVzZXJTZXR0aW5nc1RhYiAvPixcclxuICAgICAgICApKTtcclxuICAgICAgICBpZiAoU2RrQ29uZmlnLmdldCgpWydzaG93TGFic1NldHRpbmdzJ10gfHwgU2V0dGluZ3NTdG9yZS5nZXRMYWJzRmVhdHVyZXMoKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRhYnMucHVzaChuZXcgVGFiKFxyXG4gICAgICAgICAgICAgICAgX3RkKFwiTGFic1wiKSxcclxuICAgICAgICAgICAgICAgIFwibXhfVXNlclNldHRpbmdzRGlhbG9nX2xhYnNJY29uXCIsXHJcbiAgICAgICAgICAgICAgICA8TGFic1VzZXJTZXR0aW5nc1RhYiAvPixcclxuICAgICAgICAgICAgKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1qb2xuaXJFbmFibGVkKSB7XHJcbiAgICAgICAgICAgIHRhYnMucHVzaChuZXcgVGFiKFxyXG4gICAgICAgICAgICAgICAgX3RkKFwiSWdub3JlZCB1c2Vyc1wiKSxcclxuICAgICAgICAgICAgICAgIFwibXhfVXNlclNldHRpbmdzRGlhbG9nX21qb2xuaXJJY29uXCIsXHJcbiAgICAgICAgICAgICAgICA8TWpvbG5pclVzZXJTZXR0aW5nc1RhYiAvPixcclxuICAgICAgICAgICAgKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRhYnMucHVzaChuZXcgVGFiKFxyXG4gICAgICAgICAgICBfdGQoXCJIZWxwICYgQWJvdXRcIiksXHJcbiAgICAgICAgICAgIFwibXhfVXNlclNldHRpbmdzRGlhbG9nX2hlbHBJY29uXCIsXHJcbiAgICAgICAgICAgIDxIZWxwVXNlclNldHRpbmdzVGFiIGNsb3NlU2V0dGluZ3NGbj17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfSAvPixcclxuICAgICAgICApKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRhYnM7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPSdteF9Vc2VyU2V0dGluZ3NEaWFsb2cnIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfSB0aXRsZT17X3QoXCJTZXR0aW5nc1wiKX0+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXNfU2V0dGluZ3NEaWFsb2dfY29udGVudCc+XHJcbiAgICAgICAgICAgICAgICAgICAgPFRhYmJlZFZpZXcgdGFicz17dGhpcy5fZ2V0VGFicygpfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==