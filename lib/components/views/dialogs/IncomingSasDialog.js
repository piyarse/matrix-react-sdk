"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const PHASE_START = 0;
const PHASE_SHOW_SAS = 1;
const PHASE_WAIT_FOR_PARTNER_TO_CONFIRM = 2;
const PHASE_VERIFIED = 3;
const PHASE_CANCELLED = 4;

class IncomingSasDialog extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onFinished", () => {
      this.props.onFinished(this.state.phase === PHASE_VERIFIED);
    });
    (0, _defineProperty2.default)(this, "_onCancelClick", () => {
      this.props.onFinished(this.state.phase === PHASE_VERIFIED);
    });
    (0, _defineProperty2.default)(this, "_onContinueClick", () => {
      this.setState({
        phase: PHASE_WAIT_FOR_PARTNER_TO_CONFIRM
      });
      this.props.verifier.verify().then(() => {
        this.setState({
          phase: PHASE_VERIFIED
        });
      }).catch(e => {
        console.log("Verification failed", e);
      });
    });
    (0, _defineProperty2.default)(this, "_onVerifierShowSas", e => {
      this._showSasEvent = e;
      this.setState({
        phase: PHASE_SHOW_SAS,
        sas: e.sas
      });
    });
    (0, _defineProperty2.default)(this, "_onVerifierCancel", e => {
      this.setState({
        phase: PHASE_CANCELLED
      });
    });
    (0, _defineProperty2.default)(this, "_onSasMatchesClick", () => {
      this._showSasEvent.confirm();

      this.setState({
        phase: PHASE_WAIT_FOR_PARTNER_TO_CONFIRM
      });
    });
    (0, _defineProperty2.default)(this, "_onVerifiedDoneClick", () => {
      this.props.onFinished(true);
    });
    let phase = PHASE_START;

    if (this.props.verifier.cancelled) {
      console.log("Verifier was cancelled in the background.");
      phase = PHASE_CANCELLED;
    }

    this._showSasEvent = null;
    this.state = {
      phase: phase,
      sasVerified: false,
      opponentProfile: null,
      opponentProfileError: null
    };
    this.props.verifier.on('show_sas', this._onVerifierShowSas);
    this.props.verifier.on('cancel', this._onVerifierCancel);

    this._fetchOpponentProfile();
  }

  componentWillUnmount() {
    if (this.state.phase !== PHASE_CANCELLED && this.state.phase !== PHASE_VERIFIED) {
      this.props.verifier.cancel('User cancel');
    }

    this.props.verifier.removeListener('show_sas', this._onVerifierShowSas);
  }

  async _fetchOpponentProfile() {
    try {
      const prof = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(this.props.verifier.userId);
      this.setState({
        opponentProfile: prof
      });
    } catch (e) {
      this.setState({
        opponentProfileError: e
      });
    }
  }

  _renderPhaseStart() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const Spinner = sdk.getComponent("views.elements.Spinner");
    const BaseAvatar = sdk.getComponent("avatars.BaseAvatar");

    const isSelf = this.props.verifier.userId == _MatrixClientPeg.MatrixClientPeg.get().getUserId();

    let profile;

    if (this.state.opponentProfile) {
      profile = _react.default.createElement("div", {
        className: "mx_IncomingSasDialog_opponentProfile"
      }, _react.default.createElement(BaseAvatar, {
        name: this.state.opponentProfile.displayname,
        idName: this.props.verifier.userId,
        url: _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(this.state.opponentProfile.avatar_url, Math.floor(48 * window.devicePixelRatio), Math.floor(48 * window.devicePixelRatio), 'crop'),
        width: 48,
        height: 48,
        resizeMethod: "crop"
      }), _react.default.createElement("h2", null, this.state.opponentProfile.displayname));
    } else if (this.state.opponentProfileError) {
      profile = _react.default.createElement("div", null, _react.default.createElement(BaseAvatar, {
        name: this.props.verifier.userId.slice(1),
        idName: this.props.verifier.userId,
        width: 48,
        height: 48
      }), _react.default.createElement("h2", null, this.props.verifier.userId));
    } else {
      profile = _react.default.createElement(Spinner, null);
    }

    const userDetailText = [_react.default.createElement("p", {
      key: "p1"
    }, (0, _languageHandler._t)("Verify this user to mark them as trusted. " + "Trusting users gives you extra peace of mind when using " + "end-to-end encrypted messages.")), _react.default.createElement("p", {
      key: "p2"
    }, (0, _languageHandler._t)( // NB. Below wording adjusted to singular 'session' until we have
    // cross-signing
    "Verifying this user will mark their session as trusted, and " + "also mark your session as trusted to them."))];
    const selfDetailText = [_react.default.createElement("p", {
      key: "p1"
    }, (0, _languageHandler._t)("Verify this device to mark it as trusted. " + "Trusting this device gives you and other users extra peace of mind when using " + "end-to-end encrypted messages.")), _react.default.createElement("p", {
      key: "p2"
    }, (0, _languageHandler._t)("Verifying this device will mark it as trusted, and users who have verified with " + "you will trust this device."))];
    return _react.default.createElement("div", null, profile, isSelf ? selfDetailText : userDetailText, _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Continue'),
      hasCancel: true,
      onPrimaryButtonClick: this._onContinueClick,
      onCancel: this._onCancelClick
    }));
  }

  _renderPhaseShowSas() {
    const VerificationShowSas = sdk.getComponent('views.verification.VerificationShowSas');
    return _react.default.createElement(VerificationShowSas, {
      sas: this._showSasEvent.sas,
      onCancel: this._onCancelClick,
      onDone: this._onSasMatchesClick,
      isSelf: this.props.verifier.userId === _MatrixClientPeg.MatrixClientPeg.get().getUserId(),
      inDialog: true
    });
  }

  _renderPhaseWaitForPartnerToConfirm() {
    const Spinner = sdk.getComponent("views.elements.Spinner");
    return _react.default.createElement("div", null, _react.default.createElement(Spinner, null), _react.default.createElement("p", null, (0, _languageHandler._t)("Waiting for partner to confirm...")));
  }

  _renderPhaseVerified() {
    const VerificationComplete = sdk.getComponent('views.verification.VerificationComplete');
    return _react.default.createElement(VerificationComplete, {
      onDone: this._onVerifiedDoneClick
    });
  }

  _renderPhaseCancelled() {
    const VerificationCancelled = sdk.getComponent('views.verification.VerificationCancelled');
    return _react.default.createElement(VerificationCancelled, {
      onDone: this._onCancelClick
    });
  }

  render() {
    let body;

    switch (this.state.phase) {
      case PHASE_START:
        body = this._renderPhaseStart();
        break;

      case PHASE_SHOW_SAS:
        body = this._renderPhaseShowSas();
        break;

      case PHASE_WAIT_FOR_PARTNER_TO_CONFIRM:
        body = this._renderPhaseWaitForPartnerToConfirm();
        break;

      case PHASE_VERIFIED:
        body = this._renderPhaseVerified();
        break;

      case PHASE_CANCELLED:
        body = this._renderPhaseCancelled();
        break;
    }

    const BaseDialog = sdk.getComponent("dialogs.BaseDialog");
    return _react.default.createElement(BaseDialog, {
      title: (0, _languageHandler._t)("Incoming Verification Request"),
      onFinished: this._onFinished,
      fixedWidth: false
    }, body);
  }

}

exports.default = IncomingSasDialog;
(0, _defineProperty2.default)(IncomingSasDialog, "propTypes", {
  verifier: _propTypes.default.object.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvSW5jb21pbmdTYXNEaWFsb2cuanMiXSwibmFtZXMiOlsiUEhBU0VfU1RBUlQiLCJQSEFTRV9TSE9XX1NBUyIsIlBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQ09ORklSTSIsIlBIQVNFX1ZFUklGSUVEIiwiUEhBU0VfQ0FOQ0VMTEVEIiwiSW5jb21pbmdTYXNEaWFsb2ciLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJvbkZpbmlzaGVkIiwic3RhdGUiLCJwaGFzZSIsInNldFN0YXRlIiwidmVyaWZpZXIiLCJ2ZXJpZnkiLCJ0aGVuIiwiY2F0Y2giLCJlIiwiY29uc29sZSIsImxvZyIsIl9zaG93U2FzRXZlbnQiLCJzYXMiLCJjb25maXJtIiwiY2FuY2VsbGVkIiwic2FzVmVyaWZpZWQiLCJvcHBvbmVudFByb2ZpbGUiLCJvcHBvbmVudFByb2ZpbGVFcnJvciIsIm9uIiwiX29uVmVyaWZpZXJTaG93U2FzIiwiX29uVmVyaWZpZXJDYW5jZWwiLCJfZmV0Y2hPcHBvbmVudFByb2ZpbGUiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNhbmNlbCIsInJlbW92ZUxpc3RlbmVyIiwicHJvZiIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFByb2ZpbGVJbmZvIiwidXNlcklkIiwiX3JlbmRlclBoYXNlU3RhcnQiLCJEaWFsb2dCdXR0b25zIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiU3Bpbm5lciIsIkJhc2VBdmF0YXIiLCJpc1NlbGYiLCJnZXRVc2VySWQiLCJwcm9maWxlIiwiZGlzcGxheW5hbWUiLCJteGNVcmxUb0h0dHAiLCJhdmF0YXJfdXJsIiwiTWF0aCIsImZsb29yIiwid2luZG93IiwiZGV2aWNlUGl4ZWxSYXRpbyIsInNsaWNlIiwidXNlckRldGFpbFRleHQiLCJzZWxmRGV0YWlsVGV4dCIsIl9vbkNvbnRpbnVlQ2xpY2siLCJfb25DYW5jZWxDbGljayIsIl9yZW5kZXJQaGFzZVNob3dTYXMiLCJWZXJpZmljYXRpb25TaG93U2FzIiwiX29uU2FzTWF0Y2hlc0NsaWNrIiwiX3JlbmRlclBoYXNlV2FpdEZvclBhcnRuZXJUb0NvbmZpcm0iLCJfcmVuZGVyUGhhc2VWZXJpZmllZCIsIlZlcmlmaWNhdGlvbkNvbXBsZXRlIiwiX29uVmVyaWZpZWREb25lQ2xpY2siLCJfcmVuZGVyUGhhc2VDYW5jZWxsZWQiLCJWZXJpZmljYXRpb25DYW5jZWxsZWQiLCJyZW5kZXIiLCJib2R5IiwiQmFzZURpYWxvZyIsIl9vbkZpbmlzaGVkIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFwQkE7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSxNQUFNQSxXQUFXLEdBQUcsQ0FBcEI7QUFDQSxNQUFNQyxjQUFjLEdBQUcsQ0FBdkI7QUFDQSxNQUFNQyxpQ0FBaUMsR0FBRyxDQUExQztBQUNBLE1BQU1DLGNBQWMsR0FBRyxDQUF2QjtBQUNBLE1BQU1DLGVBQWUsR0FBRyxDQUF4Qjs7QUFFZSxNQUFNQyxpQkFBTixTQUFnQ0MsZUFBTUMsU0FBdEMsQ0FBZ0Q7QUFLM0RDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLHVEQTJDTCxNQUFNO0FBQ2hCLFdBQUtBLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixLQUFLQyxLQUFMLENBQVdDLEtBQVgsS0FBcUJULGNBQTNDO0FBQ0gsS0E3Q2tCO0FBQUEsMERBK0NGLE1BQU07QUFDbkIsV0FBS00sS0FBTCxDQUFXQyxVQUFYLENBQXNCLEtBQUtDLEtBQUwsQ0FBV0MsS0FBWCxLQUFxQlQsY0FBM0M7QUFDSCxLQWpEa0I7QUFBQSw0REFtREEsTUFBTTtBQUNyQixXQUFLVSxRQUFMLENBQWM7QUFBQ0QsUUFBQUEsS0FBSyxFQUFFVjtBQUFSLE9BQWQ7QUFDQSxXQUFLTyxLQUFMLENBQVdLLFFBQVgsQ0FBb0JDLE1BQXBCLEdBQTZCQyxJQUE3QixDQUFrQyxNQUFNO0FBQ3BDLGFBQUtILFFBQUwsQ0FBYztBQUFDRCxVQUFBQSxLQUFLLEVBQUVUO0FBQVIsU0FBZDtBQUNILE9BRkQsRUFFR2MsS0FGSCxDQUVVQyxDQUFELElBQU87QUFDWkMsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkscUJBQVosRUFBbUNGLENBQW5DO0FBQ0gsT0FKRDtBQUtILEtBMURrQjtBQUFBLDhEQTRER0EsQ0FBRCxJQUFPO0FBQ3hCLFdBQUtHLGFBQUwsR0FBcUJILENBQXJCO0FBQ0EsV0FBS0wsUUFBTCxDQUFjO0FBQ1ZELFFBQUFBLEtBQUssRUFBRVgsY0FERztBQUVWcUIsUUFBQUEsR0FBRyxFQUFFSixDQUFDLENBQUNJO0FBRkcsT0FBZDtBQUlILEtBbEVrQjtBQUFBLDZEQW9FRUosQ0FBRCxJQUFPO0FBQ3ZCLFdBQUtMLFFBQUwsQ0FBYztBQUNWRCxRQUFBQSxLQUFLLEVBQUVSO0FBREcsT0FBZDtBQUdILEtBeEVrQjtBQUFBLDhEQTBFRSxNQUFNO0FBQ3ZCLFdBQUtpQixhQUFMLENBQW1CRSxPQUFuQjs7QUFDQSxXQUFLVixRQUFMLENBQWM7QUFDVkQsUUFBQUEsS0FBSyxFQUFFVjtBQURHLE9BQWQ7QUFHSCxLQS9Fa0I7QUFBQSxnRUFpRkksTUFBTTtBQUN6QixXQUFLTyxLQUFMLENBQVdDLFVBQVgsQ0FBc0IsSUFBdEI7QUFDSCxLQW5Ga0I7QUFHZixRQUFJRSxLQUFLLEdBQUdaLFdBQVo7O0FBQ0EsUUFBSSxLQUFLUyxLQUFMLENBQVdLLFFBQVgsQ0FBb0JVLFNBQXhCLEVBQW1DO0FBQy9CTCxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSwyQ0FBWjtBQUNBUixNQUFBQSxLQUFLLEdBQUdSLGVBQVI7QUFDSDs7QUFFRCxTQUFLaUIsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFNBQUtWLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxLQUFLLEVBQUVBLEtBREU7QUFFVGEsTUFBQUEsV0FBVyxFQUFFLEtBRko7QUFHVEMsTUFBQUEsZUFBZSxFQUFFLElBSFI7QUFJVEMsTUFBQUEsb0JBQW9CLEVBQUU7QUFKYixLQUFiO0FBTUEsU0FBS2xCLEtBQUwsQ0FBV0ssUUFBWCxDQUFvQmMsRUFBcEIsQ0FBdUIsVUFBdkIsRUFBbUMsS0FBS0Msa0JBQXhDO0FBQ0EsU0FBS3BCLEtBQUwsQ0FBV0ssUUFBWCxDQUFvQmMsRUFBcEIsQ0FBdUIsUUFBdkIsRUFBaUMsS0FBS0UsaUJBQXRDOztBQUNBLFNBQUtDLHFCQUFMO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFFBQUksS0FBS3JCLEtBQUwsQ0FBV0MsS0FBWCxLQUFxQlIsZUFBckIsSUFBd0MsS0FBS08sS0FBTCxDQUFXQyxLQUFYLEtBQXFCVCxjQUFqRSxFQUFpRjtBQUM3RSxXQUFLTSxLQUFMLENBQVdLLFFBQVgsQ0FBb0JtQixNQUFwQixDQUEyQixhQUEzQjtBQUNIOztBQUNELFNBQUt4QixLQUFMLENBQVdLLFFBQVgsQ0FBb0JvQixjQUFwQixDQUFtQyxVQUFuQyxFQUErQyxLQUFLTCxrQkFBcEQ7QUFDSDs7QUFFRCxRQUFNRSxxQkFBTixHQUE4QjtBQUMxQixRQUFJO0FBQ0EsWUFBTUksSUFBSSxHQUFHLE1BQU1DLGlDQUFnQkMsR0FBaEIsR0FBc0JDLGNBQXRCLENBQ2YsS0FBSzdCLEtBQUwsQ0FBV0ssUUFBWCxDQUFvQnlCLE1BREwsQ0FBbkI7QUFHQSxXQUFLMUIsUUFBTCxDQUFjO0FBQ1ZhLFFBQUFBLGVBQWUsRUFBRVM7QUFEUCxPQUFkO0FBR0gsS0FQRCxDQU9FLE9BQU9qQixDQUFQLEVBQVU7QUFDUixXQUFLTCxRQUFMLENBQWM7QUFDVmMsUUFBQUEsb0JBQW9CLEVBQUVUO0FBRFosT0FBZDtBQUdIO0FBQ0o7O0FBNENEc0IsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsVUFBTUMsYUFBYSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsVUFBTUMsT0FBTyxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQWhCO0FBQ0EsVUFBTUUsVUFBVSxHQUFHSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsb0JBQWpCLENBQW5COztBQUVBLFVBQU1HLE1BQU0sR0FBRyxLQUFLckMsS0FBTCxDQUFXSyxRQUFYLENBQW9CeUIsTUFBcEIsSUFBOEJILGlDQUFnQkMsR0FBaEIsR0FBc0JVLFNBQXRCLEVBQTdDOztBQUVBLFFBQUlDLE9BQUo7O0FBQ0EsUUFBSSxLQUFLckMsS0FBTCxDQUFXZSxlQUFmLEVBQWdDO0FBQzVCc0IsTUFBQUEsT0FBTyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNOLDZCQUFDLFVBQUQ7QUFBWSxRQUFBLElBQUksRUFBRSxLQUFLckMsS0FBTCxDQUFXZSxlQUFYLENBQTJCdUIsV0FBN0M7QUFDSSxRQUFBLE1BQU0sRUFBRSxLQUFLeEMsS0FBTCxDQUFXSyxRQUFYLENBQW9CeUIsTUFEaEM7QUFFSSxRQUFBLEdBQUcsRUFBRUgsaUNBQWdCQyxHQUFoQixHQUFzQmEsWUFBdEIsQ0FDRCxLQUFLdkMsS0FBTCxDQUFXZSxlQUFYLENBQTJCeUIsVUFEMUIsRUFFREMsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsTUFBTSxDQUFDQyxnQkFBdkIsQ0FGQyxFQUdESCxJQUFJLENBQUNDLEtBQUwsQ0FBVyxLQUFLQyxNQUFNLENBQUNDLGdCQUF2QixDQUhDLEVBSUQsTUFKQyxDQUZUO0FBUUksUUFBQSxLQUFLLEVBQUUsRUFSWDtBQVFlLFFBQUEsTUFBTSxFQUFFLEVBUnZCO0FBUTJCLFFBQUEsWUFBWSxFQUFDO0FBUnhDLFFBRE0sRUFXTix5Q0FBSyxLQUFLNUMsS0FBTCxDQUFXZSxlQUFYLENBQTJCdUIsV0FBaEMsQ0FYTSxDQUFWO0FBYUgsS0FkRCxNQWNPLElBQUksS0FBS3RDLEtBQUwsQ0FBV2dCLG9CQUFmLEVBQXFDO0FBQ3hDcUIsTUFBQUEsT0FBTyxHQUFHLDBDQUNOLDZCQUFDLFVBQUQ7QUFBWSxRQUFBLElBQUksRUFBRSxLQUFLdkMsS0FBTCxDQUFXSyxRQUFYLENBQW9CeUIsTUFBcEIsQ0FBMkJpQixLQUEzQixDQUFpQyxDQUFqQyxDQUFsQjtBQUNJLFFBQUEsTUFBTSxFQUFFLEtBQUsvQyxLQUFMLENBQVdLLFFBQVgsQ0FBb0J5QixNQURoQztBQUVJLFFBQUEsS0FBSyxFQUFFLEVBRlg7QUFFZSxRQUFBLE1BQU0sRUFBRTtBQUZ2QixRQURNLEVBS04seUNBQUssS0FBSzlCLEtBQUwsQ0FBV0ssUUFBWCxDQUFvQnlCLE1BQXpCLENBTE0sQ0FBVjtBQU9ILEtBUk0sTUFRQTtBQUNIUyxNQUFBQSxPQUFPLEdBQUcsNkJBQUMsT0FBRCxPQUFWO0FBQ0g7O0FBRUQsVUFBTVMsY0FBYyxHQUFHLENBQ25CO0FBQUcsTUFBQSxHQUFHLEVBQUM7QUFBUCxPQUFhLHlCQUNULCtDQUNBLDBEQURBLEdBRUEsZ0NBSFMsQ0FBYixDQURtQixFQU1uQjtBQUFHLE1BQUEsR0FBRyxFQUFDO0FBQVAsT0FBYSwwQkFDVDtBQUNBO0FBQ0EscUVBQ0EsNENBSlMsQ0FBYixDQU5tQixDQUF2QjtBQWNBLFVBQU1DLGNBQWMsR0FBRyxDQUNuQjtBQUFHLE1BQUEsR0FBRyxFQUFDO0FBQVAsT0FBYSx5QkFDVCwrQ0FDQSxnRkFEQSxHQUVBLGdDQUhTLENBQWIsQ0FEbUIsRUFNbkI7QUFBRyxNQUFBLEdBQUcsRUFBQztBQUFQLE9BQWEseUJBQ1QscUZBQ0EsNkJBRlMsQ0FBYixDQU5tQixDQUF2QjtBQVlBLFdBQ0ksMENBQ0tWLE9BREwsRUFFS0YsTUFBTSxHQUFHWSxjQUFILEdBQW9CRCxjQUYvQixFQUdJLDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxVQUFILENBRG5CO0FBRUksTUFBQSxTQUFTLEVBQUUsSUFGZjtBQUdJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS0UsZ0JBSC9CO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFKbkIsTUFISixDQURKO0FBWUg7O0FBRURDLEVBQUFBLG1CQUFtQixHQUFHO0FBQ2xCLFVBQU1DLG1CQUFtQixHQUFHcEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdDQUFqQixDQUE1QjtBQUNBLFdBQU8sNkJBQUMsbUJBQUQ7QUFDSCxNQUFBLEdBQUcsRUFBRSxLQUFLdEIsYUFBTCxDQUFtQkMsR0FEckI7QUFFSCxNQUFBLFFBQVEsRUFBRSxLQUFLc0MsY0FGWjtBQUdILE1BQUEsTUFBTSxFQUFFLEtBQUtHLGtCQUhWO0FBSUgsTUFBQSxNQUFNLEVBQUUsS0FBS3RELEtBQUwsQ0FBV0ssUUFBWCxDQUFvQnlCLE1BQXBCLEtBQStCSCxpQ0FBZ0JDLEdBQWhCLEdBQXNCVSxTQUF0QixFQUpwQztBQUtILE1BQUEsUUFBUSxFQUFFO0FBTFAsTUFBUDtBQU9IOztBQUVEaUIsRUFBQUEsbUNBQW1DLEdBQUc7QUFDbEMsVUFBTXBCLE9BQU8sR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUFoQjtBQUVBLFdBQ0ksMENBQ0ksNkJBQUMsT0FBRCxPQURKLEVBRUksd0NBQUkseUJBQUcsbUNBQUgsQ0FBSixDQUZKLENBREo7QUFNSDs7QUFFRHNCLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFVBQU1DLG9CQUFvQixHQUFHeEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlDQUFqQixDQUE3QjtBQUNBLFdBQU8sNkJBQUMsb0JBQUQ7QUFBc0IsTUFBQSxNQUFNLEVBQUUsS0FBS3dCO0FBQW5DLE1BQVA7QUFDSDs7QUFFREMsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsVUFBTUMscUJBQXFCLEdBQUczQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMENBQWpCLENBQTlCO0FBQ0EsV0FBTyw2QkFBQyxxQkFBRDtBQUF1QixNQUFBLE1BQU0sRUFBRSxLQUFLaUI7QUFBcEMsTUFBUDtBQUNIOztBQUVEVSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJQyxJQUFKOztBQUNBLFlBQVEsS0FBSzVELEtBQUwsQ0FBV0MsS0FBbkI7QUFDSSxXQUFLWixXQUFMO0FBQ0l1RSxRQUFBQSxJQUFJLEdBQUcsS0FBSy9CLGlCQUFMLEVBQVA7QUFDQTs7QUFDSixXQUFLdkMsY0FBTDtBQUNJc0UsUUFBQUEsSUFBSSxHQUFHLEtBQUtWLG1CQUFMLEVBQVA7QUFDQTs7QUFDSixXQUFLM0QsaUNBQUw7QUFDSXFFLFFBQUFBLElBQUksR0FBRyxLQUFLUCxtQ0FBTCxFQUFQO0FBQ0E7O0FBQ0osV0FBSzdELGNBQUw7QUFDSW9FLFFBQUFBLElBQUksR0FBRyxLQUFLTixvQkFBTCxFQUFQO0FBQ0E7O0FBQ0osV0FBSzdELGVBQUw7QUFDSW1FLFFBQUFBLElBQUksR0FBRyxLQUFLSCxxQkFBTCxFQUFQO0FBQ0E7QUFmUjs7QUFrQkEsVUFBTUksVUFBVSxHQUFHOUIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjtBQUNBLFdBQ0ksNkJBQUMsVUFBRDtBQUNJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLCtCQUFILENBRFg7QUFFSSxNQUFBLFVBQVUsRUFBRSxLQUFLOEIsV0FGckI7QUFHSSxNQUFBLFVBQVUsRUFBRTtBQUhoQixPQUtLRixJQUxMLENBREo7QUFTSDs7QUFsTzBEOzs7OEJBQTFDbEUsaUIsZUFDRTtBQUNmUyxFQUFBQSxRQUFRLEVBQUU0RCxtQkFBVUMsTUFBVixDQUFpQkM7QUFEWixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuY29uc3QgUEhBU0VfU1RBUlQgPSAwO1xyXG5jb25zdCBQSEFTRV9TSE9XX1NBUyA9IDE7XHJcbmNvbnN0IFBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQ09ORklSTSA9IDI7XHJcbmNvbnN0IFBIQVNFX1ZFUklGSUVEID0gMztcclxuY29uc3QgUEhBU0VfQ0FOQ0VMTEVEID0gNDtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEluY29taW5nU2FzRGlhbG9nIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgdmVyaWZpZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIGxldCBwaGFzZSA9IFBIQVNFX1NUQVJUO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnZlcmlmaWVyLmNhbmNlbGxlZCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlZlcmlmaWVyIHdhcyBjYW5jZWxsZWQgaW4gdGhlIGJhY2tncm91bmQuXCIpO1xyXG4gICAgICAgICAgICBwaGFzZSA9IFBIQVNFX0NBTkNFTExFRDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3Nob3dTYXNFdmVudCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcGhhc2U6IHBoYXNlLFxyXG4gICAgICAgICAgICBzYXNWZXJpZmllZDogZmFsc2UsXHJcbiAgICAgICAgICAgIG9wcG9uZW50UHJvZmlsZTogbnVsbCxcclxuICAgICAgICAgICAgb3Bwb25lbnRQcm9maWxlRXJyb3I6IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnByb3BzLnZlcmlmaWVyLm9uKCdzaG93X3NhcycsIHRoaXMuX29uVmVyaWZpZXJTaG93U2FzKTtcclxuICAgICAgICB0aGlzLnByb3BzLnZlcmlmaWVyLm9uKCdjYW5jZWwnLCB0aGlzLl9vblZlcmlmaWVyQ2FuY2VsKTtcclxuICAgICAgICB0aGlzLl9mZXRjaE9wcG9uZW50UHJvZmlsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBoYXNlICE9PSBQSEFTRV9DQU5DRUxMRUQgJiYgdGhpcy5zdGF0ZS5waGFzZSAhPT0gUEhBU0VfVkVSSUZJRUQpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy52ZXJpZmllci5jYW5jZWwoJ1VzZXIgY2FuY2VsJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucHJvcHMudmVyaWZpZXIucmVtb3ZlTGlzdGVuZXIoJ3Nob3dfc2FzJywgdGhpcy5fb25WZXJpZmllclNob3dTYXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9mZXRjaE9wcG9uZW50UHJvZmlsZSgpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBwcm9mID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFByb2ZpbGVJbmZvKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy52ZXJpZmllci51c2VySWQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgb3Bwb25lbnRQcm9maWxlOiBwcm9mLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgb3Bwb25lbnRQcm9maWxlRXJyb3I6IGUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25GaW5pc2hlZCA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodGhpcy5zdGF0ZS5waGFzZSA9PT0gUEhBU0VfVkVSSUZJRUQpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNhbmNlbENsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0aGlzLnN0YXRlLnBoYXNlID09PSBQSEFTRV9WRVJJRklFRCk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ29udGludWVDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfV0FJVF9GT1JfUEFSVE5FUl9UT19DT05GSVJNfSk7XHJcbiAgICAgICAgdGhpcy5wcm9wcy52ZXJpZmllci52ZXJpZnkoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cGhhc2U6IFBIQVNFX1ZFUklGSUVEfSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJWZXJpZmljYXRpb24gZmFpbGVkXCIsIGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblZlcmlmaWVyU2hvd1NhcyA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2hvd1Nhc0V2ZW50ID0gZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dfU0FTLFxyXG4gICAgICAgICAgICBzYXM6IGUuc2FzLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblZlcmlmaWVyQ2FuY2VsID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0NBTkNFTExFRCxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25TYXNNYXRjaGVzQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2hvd1Nhc0V2ZW50LmNvbmZpcm0oKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQ09ORklSTSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25WZXJpZmllZERvbmVDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlU3RhcnQoKSB7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgY29uc3QgQmFzZUF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoXCJhdmF0YXJzLkJhc2VBdmF0YXJcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IGlzU2VsZiA9IHRoaXMucHJvcHMudmVyaWZpZXIudXNlcklkID09IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKTtcclxuXHJcbiAgICAgICAgbGV0IHByb2ZpbGU7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUub3Bwb25lbnRQcm9maWxlKSB7XHJcbiAgICAgICAgICAgIHByb2ZpbGUgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0luY29taW5nU2FzRGlhbG9nX29wcG9uZW50UHJvZmlsZVwiPlxyXG4gICAgICAgICAgICAgICAgPEJhc2VBdmF0YXIgbmFtZT17dGhpcy5zdGF0ZS5vcHBvbmVudFByb2ZpbGUuZGlzcGxheW5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgaWROYW1lPXt0aGlzLnByb3BzLnZlcmlmaWVyLnVzZXJJZH1cclxuICAgICAgICAgICAgICAgICAgICB1cmw9e01hdHJpeENsaWVudFBlZy5nZXQoKS5teGNVcmxUb0h0dHAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUub3Bwb25lbnRQcm9maWxlLmF2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1hdGguZmxvb3IoNDggKiB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1hdGguZmxvb3IoNDggKiB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdjcm9wJyxcclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPXs0OH0gaGVpZ2h0PXs0OH0gcmVzaXplTWV0aG9kPSdjcm9wJ1xyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxoMj57dGhpcy5zdGF0ZS5vcHBvbmVudFByb2ZpbGUuZGlzcGxheW5hbWV9PC9oMj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5vcHBvbmVudFByb2ZpbGVFcnJvcikge1xyXG4gICAgICAgICAgICBwcm9maWxlID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxCYXNlQXZhdGFyIG5hbWU9e3RoaXMucHJvcHMudmVyaWZpZXIudXNlcklkLnNsaWNlKDEpfVxyXG4gICAgICAgICAgICAgICAgICAgIGlkTmFtZT17dGhpcy5wcm9wcy52ZXJpZmllci51c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezQ4fSBoZWlnaHQ9ezQ4fVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxoMj57dGhpcy5wcm9wcy52ZXJpZmllci51c2VySWR9PC9oMj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHByb2ZpbGUgPSA8U3Bpbm5lciAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHVzZXJEZXRhaWxUZXh0ID0gW1xyXG4gICAgICAgICAgICA8cCBrZXk9XCJwMVwiPntfdChcclxuICAgICAgICAgICAgICAgIFwiVmVyaWZ5IHRoaXMgdXNlciB0byBtYXJrIHRoZW0gYXMgdHJ1c3RlZC4gXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJUcnVzdGluZyB1c2VycyBnaXZlcyB5b3UgZXh0cmEgcGVhY2Ugb2YgbWluZCB3aGVuIHVzaW5nIFwiICtcclxuICAgICAgICAgICAgICAgIFwiZW5kLXRvLWVuZCBlbmNyeXB0ZWQgbWVzc2FnZXMuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPixcclxuICAgICAgICAgICAgPHAga2V5PVwicDJcIj57X3QoXHJcbiAgICAgICAgICAgICAgICAvLyBOQi4gQmVsb3cgd29yZGluZyBhZGp1c3RlZCB0byBzaW5ndWxhciAnc2Vzc2lvbicgdW50aWwgd2UgaGF2ZVxyXG4gICAgICAgICAgICAgICAgLy8gY3Jvc3Mtc2lnbmluZ1xyXG4gICAgICAgICAgICAgICAgXCJWZXJpZnlpbmcgdGhpcyB1c2VyIHdpbGwgbWFyayB0aGVpciBzZXNzaW9uIGFzIHRydXN0ZWQsIGFuZCBcIiArXHJcbiAgICAgICAgICAgICAgICBcImFsc28gbWFyayB5b3VyIHNlc3Npb24gYXMgdHJ1c3RlZCB0byB0aGVtLlwiLFxyXG4gICAgICAgICAgICApfTwvcD4sXHJcbiAgICAgICAgXTtcclxuXHJcbiAgICAgICAgY29uc3Qgc2VsZkRldGFpbFRleHQgPSBbXHJcbiAgICAgICAgICAgIDxwIGtleT1cInAxXCI+e190KFxyXG4gICAgICAgICAgICAgICAgXCJWZXJpZnkgdGhpcyBkZXZpY2UgdG8gbWFyayBpdCBhcyB0cnVzdGVkLiBcIiArXHJcbiAgICAgICAgICAgICAgICBcIlRydXN0aW5nIHRoaXMgZGV2aWNlIGdpdmVzIHlvdSBhbmQgb3RoZXIgdXNlcnMgZXh0cmEgcGVhY2Ugb2YgbWluZCB3aGVuIHVzaW5nIFwiICtcclxuICAgICAgICAgICAgICAgIFwiZW5kLXRvLWVuZCBlbmNyeXB0ZWQgbWVzc2FnZXMuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPixcclxuICAgICAgICAgICAgPHAga2V5PVwicDJcIj57X3QoXHJcbiAgICAgICAgICAgICAgICBcIlZlcmlmeWluZyB0aGlzIGRldmljZSB3aWxsIG1hcmsgaXQgYXMgdHJ1c3RlZCwgYW5kIHVzZXJzIHdobyBoYXZlIHZlcmlmaWVkIHdpdGggXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJ5b3Ugd2lsbCB0cnVzdCB0aGlzIGRldmljZS5cIixcclxuICAgICAgICAgICAgKX08L3A+LFxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7cHJvZmlsZX1cclxuICAgICAgICAgICAgICAgIHtpc1NlbGYgPyBzZWxmRGV0YWlsVGV4dCA6IHVzZXJEZXRhaWxUZXh0fVxyXG4gICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdCgnQ29udGludWUnKX1cclxuICAgICAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uQ29udGludWVDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlU2hvd1NhcygpIHtcclxuICAgICAgICBjb25zdCBWZXJpZmljYXRpb25TaG93U2FzID0gc2RrLmdldENvbXBvbmVudCgndmlld3MudmVyaWZpY2F0aW9uLlZlcmlmaWNhdGlvblNob3dTYXMnKTtcclxuICAgICAgICByZXR1cm4gPFZlcmlmaWNhdGlvblNob3dTYXNcclxuICAgICAgICAgICAgc2FzPXt0aGlzLl9zaG93U2FzRXZlbnQuc2FzfVxyXG4gICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgb25Eb25lPXt0aGlzLl9vblNhc01hdGNoZXNDbGlja31cclxuICAgICAgICAgICAgaXNTZWxmPXt0aGlzLnByb3BzLnZlcmlmaWVyLnVzZXJJZCA9PT0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXJJZCgpfVxyXG4gICAgICAgICAgICBpbkRpYWxvZz17dHJ1ZX1cclxuICAgICAgICAvPjtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyUGhhc2VXYWl0Rm9yUGFydG5lclRvQ29uZmlybSgpIHtcclxuICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLlNwaW5uZXJcIik7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8U3Bpbm5lciAvPlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFwiV2FpdGluZyBmb3IgcGFydG5lciB0byBjb25maXJtLi4uXCIpfTwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyUGhhc2VWZXJpZmllZCgpIHtcclxuICAgICAgICBjb25zdCBWZXJpZmljYXRpb25Db21wbGV0ZSA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLnZlcmlmaWNhdGlvbi5WZXJpZmljYXRpb25Db21wbGV0ZScpO1xyXG4gICAgICAgIHJldHVybiA8VmVyaWZpY2F0aW9uQ29tcGxldGUgb25Eb25lPXt0aGlzLl9vblZlcmlmaWVkRG9uZUNsaWNrfSAvPjtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyUGhhc2VDYW5jZWxsZWQoKSB7XHJcbiAgICAgICAgY29uc3QgVmVyaWZpY2F0aW9uQ2FuY2VsbGVkID0gc2RrLmdldENvbXBvbmVudCgndmlld3MudmVyaWZpY2F0aW9uLlZlcmlmaWNhdGlvbkNhbmNlbGxlZCcpO1xyXG4gICAgICAgIHJldHVybiA8VmVyaWZpY2F0aW9uQ2FuY2VsbGVkIG9uRG9uZT17dGhpcy5fb25DYW5jZWxDbGlja30gLz47XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBib2R5O1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5zdGF0ZS5waGFzZSkge1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX1NUQVJUOlxyXG4gICAgICAgICAgICAgICAgYm9keSA9IHRoaXMuX3JlbmRlclBoYXNlU3RhcnQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX1NIT1dfU0FTOlxyXG4gICAgICAgICAgICAgICAgYm9keSA9IHRoaXMuX3JlbmRlclBoYXNlU2hvd1NhcygpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfV0FJVF9GT1JfUEFSVE5FUl9UT19DT05GSVJNOlxyXG4gICAgICAgICAgICAgICAgYm9keSA9IHRoaXMuX3JlbmRlclBoYXNlV2FpdEZvclBhcnRuZXJUb0NvbmZpcm0oKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX1ZFUklGSUVEOlxyXG4gICAgICAgICAgICAgICAgYm9keSA9IHRoaXMuX3JlbmRlclBoYXNlVmVyaWZpZWQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX0NBTkNFTExFRDpcclxuICAgICAgICAgICAgICAgIGJvZHkgPSB0aGlzLl9yZW5kZXJQaGFzZUNhbmNlbGxlZCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuQmFzZURpYWxvZ1wiKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZURpYWxvZ1xyXG4gICAgICAgICAgICAgICAgdGl0bGU9e190KFwiSW5jb21pbmcgVmVyaWZpY2F0aW9uIFJlcXVlc3RcIil9XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLl9vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgZml4ZWRXaWR0aD17ZmFsc2V9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtib2R5fVxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuIl19