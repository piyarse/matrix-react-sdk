"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _SyntaxHighlight = _interopRequireDefault(require("../elements/SyntaxHighlight"));

var _languageHandler = require("../../../languageHandler");

var _matrixJsSdk = require("matrix-js-sdk");

var _Field = _interopRequireDefault(require("../elements/Field"));

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

var _useEventEmitter = require("../../../hooks/useEventEmitter");

var _VerificationRequest = require("matrix-js-sdk/src/crypto/verification/request/VerificationRequest");

/*
Copyright 2017 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class GenericEditor extends _react.default.PureComponent {
  // static propTypes = {onBack: PropTypes.func.isRequired};
  constructor(props) {
    super(props);
    this._onChange = this._onChange.bind(this);
    this.onBack = this.onBack.bind(this);
  }

  onBack() {
    if (this.state.message) {
      this.setState({
        message: null
      });
    } else {
      this.props.onBack();
    }
  }

  _onChange(e) {
    this.setState({
      [e.target.id]: e.target.type === 'checkbox' ? e.target.checked : e.target.value
    });
  }

  _buttons() {
    return _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.onBack
    }, (0, _languageHandler._t)('Back')), !this.state.message && _react.default.createElement("button", {
      onClick: this._send
    }, (0, _languageHandler._t)('Send')));
  }

  textInput(id, label) {
    return _react.default.createElement(_Field.default, {
      id: id,
      label: label,
      size: "42",
      autoFocus: true,
      type: "text",
      autoComplete: "on",
      value: this.state[id],
      onChange: this._onChange
    });
  }

}

class SendCustomEvent extends GenericEditor {
  static getLabel() {
    return (0, _languageHandler._t)('Send Custom Event');
  }

  constructor(props) {
    super(props);
    this._send = this._send.bind(this);
    const {
      eventType,
      stateKey,
      evContent
    } = Object.assign({
      eventType: '',
      stateKey: '',
      evContent: '{\n\n}'
    }, this.props.inputs);
    this.state = {
      isStateEvent: Boolean(this.props.forceStateEvent),
      eventType,
      stateKey,
      evContent
    };
  }

  send(content) {
    const cli = this.context;

    if (this.state.isStateEvent) {
      return cli.sendStateEvent(this.props.room.roomId, this.state.eventType, content, this.state.stateKey);
    } else {
      return cli.sendEvent(this.props.room.roomId, this.state.eventType, content);
    }
  }

  async _send() {
    if (this.state.eventType === '') {
      this.setState({
        message: (0, _languageHandler._t)('You must specify an event type!')
      });
      return;
    }

    let message;

    try {
      const content = JSON.parse(this.state.evContent);
      await this.send(content);
      message = (0, _languageHandler._t)('Event sent!');
    } catch (e) {
      message = (0, _languageHandler._t)('Failed to send custom event.') + ' (' + e.toString() + ')';
    }

    this.setState({
      message
    });
  }

  render() {
    if (this.state.message) {
      return _react.default.createElement("div", null, _react.default.createElement("div", {
        className: "mx_Dialog_content"
      }, this.state.message), this._buttons());
    }

    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_DevTools_content"
    }, _react.default.createElement("div", {
      className: "mx_DevTools_eventTypeStateKeyGroup"
    }, this.textInput('eventType', (0, _languageHandler._t)('Event Type')), this.state.isStateEvent && this.textInput('stateKey', (0, _languageHandler._t)('State Key'))), _react.default.createElement("br", null), _react.default.createElement(_Field.default, {
      id: "evContent",
      label: (0, _languageHandler._t)("Event Content"),
      type: "text",
      className: "mx_DevTools_textarea",
      autoComplete: "off",
      value: this.state.evContent,
      onChange: this._onChange,
      element: "textarea"
    })), _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.onBack
    }, (0, _languageHandler._t)('Back')), !this.state.message && _react.default.createElement("button", {
      onClick: this._send
    }, (0, _languageHandler._t)('Send')), !this.state.message && !this.props.forceStateEvent && _react.default.createElement("div", {
      style: {
        float: "right"
      }
    }, _react.default.createElement("input", {
      id: "isStateEvent",
      className: "mx_DevTools_tgl mx_DevTools_tgl-flip",
      type: "checkbox",
      onChange: this._onChange,
      checked: this.state.isStateEvent
    }), _react.default.createElement("label", {
      className: "mx_DevTools_tgl-btn",
      "data-tg-off": "Event",
      "data-tg-on": "State Event",
      htmlFor: "isStateEvent"
    }))));
  }

}

(0, _defineProperty2.default)(SendCustomEvent, "propTypes", {
  onBack: _propTypes.default.func.isRequired,
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired,
  forceStateEvent: _propTypes.default.bool,
  inputs: _propTypes.default.object
});
(0, _defineProperty2.default)(SendCustomEvent, "contextType", _MatrixClientContext.default);

class SendAccountData extends GenericEditor {
  static getLabel() {
    return (0, _languageHandler._t)('Send Account Data');
  }

  constructor(props) {
    super(props);
    this._send = this._send.bind(this);
    const {
      eventType,
      evContent
    } = Object.assign({
      eventType: '',
      evContent: '{\n\n}'
    }, this.props.inputs);
    this.state = {
      isRoomAccountData: Boolean(this.props.isRoomAccountData),
      eventType,
      evContent
    };
  }

  send(content) {
    const cli = this.context;

    if (this.state.isRoomAccountData) {
      return cli.setRoomAccountData(this.props.room.roomId, this.state.eventType, content);
    }

    return cli.setAccountData(this.state.eventType, content);
  }

  async _send() {
    if (this.state.eventType === '') {
      this.setState({
        message: (0, _languageHandler._t)('You must specify an event type!')
      });
      return;
    }

    let message;

    try {
      const content = JSON.parse(this.state.evContent);
      await this.send(content);
      message = (0, _languageHandler._t)('Event sent!');
    } catch (e) {
      message = (0, _languageHandler._t)('Failed to send custom event.') + ' (' + e.toString() + ')';
    }

    this.setState({
      message
    });
  }

  render() {
    if (this.state.message) {
      return _react.default.createElement("div", null, _react.default.createElement("div", {
        className: "mx_Dialog_content"
      }, this.state.message), this._buttons());
    }

    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_DevTools_content"
    }, this.textInput('eventType', (0, _languageHandler._t)('Event Type')), _react.default.createElement("br", null), _react.default.createElement(_Field.default, {
      id: "evContent",
      label: (0, _languageHandler._t)("Event Content"),
      type: "text",
      className: "mx_DevTools_textarea",
      autoComplete: "off",
      value: this.state.evContent,
      onChange: this._onChange,
      element: "textarea"
    })), _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.onBack
    }, (0, _languageHandler._t)('Back')), !this.state.message && _react.default.createElement("button", {
      onClick: this._send
    }, (0, _languageHandler._t)('Send')), !this.state.message && _react.default.createElement("div", {
      style: {
        float: "right"
      }
    }, _react.default.createElement("input", {
      id: "isRoomAccountData",
      className: "mx_DevTools_tgl mx_DevTools_tgl-flip",
      type: "checkbox",
      onChange: this._onChange,
      checked: this.state.isRoomAccountData,
      disabled: this.props.forceMode
    }), _react.default.createElement("label", {
      className: "mx_DevTools_tgl-btn",
      "data-tg-off": "Account Data",
      "data-tg-on": "Room Data",
      htmlFor: "isRoomAccountData"
    }))));
  }

}

(0, _defineProperty2.default)(SendAccountData, "propTypes", {
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired,
  isRoomAccountData: _propTypes.default.bool,
  forceMode: _propTypes.default.bool,
  inputs: _propTypes.default.object
});
(0, _defineProperty2.default)(SendAccountData, "contextType", _MatrixClientContext.default);
const INITIAL_LOAD_TILES = 20;
const LOAD_TILES_STEP_SIZE = 50;

class FilteredList extends _react.default.PureComponent {
  static filterChildren(children, query) {
    if (!query) return children;
    const lcQuery = query.toLowerCase();
    return children.filter(child => child.key.toLowerCase().includes(lcQuery));
  }

  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "showAll", () => {
      this.setState({
        truncateAt: this.state.truncateAt + LOAD_TILES_STEP_SIZE
      });
    });
    (0, _defineProperty2.default)(this, "createOverflowElement", (overflowCount
    /*: number*/
    , totalCount
    /*: number*/
    ) => {
      return _react.default.createElement("button", {
        className: "mx_DevTools_RoomStateExplorer_button",
        onClick: this.showAll
      }, (0, _languageHandler._t)("and %(count)s others...", {
        count: overflowCount
      }));
    });
    (0, _defineProperty2.default)(this, "onQuery", ev => {
      if (this.props.onChange) this.props.onChange(ev.target.value);
    });
    (0, _defineProperty2.default)(this, "getChildren", (start
    /*: number*/
    , end
    /*: number*/
    ) => {
      return this.state.filteredChildren.slice(start, end);
    });
    (0, _defineProperty2.default)(this, "getChildCount", () =>
    /*: number*/
    {
      return this.state.filteredChildren.length;
    });
    this.state = {
      filteredChildren: FilteredList.filterChildren(this.props.children, this.props.query),
      truncateAt: INITIAL_LOAD_TILES
    };
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(nextProps) {
    // eslint-disable-line camelcase
    if (this.props.children === nextProps.children && this.props.query === nextProps.query) return;
    this.setState({
      filteredChildren: FilteredList.filterChildren(nextProps.children, nextProps.query),
      truncateAt: INITIAL_LOAD_TILES
    });
  }

  render() {
    const TruncatedList = sdk.getComponent("elements.TruncatedList");
    return _react.default.createElement("div", null, _react.default.createElement(_Field.default, {
      label: (0, _languageHandler._t)('Filter results'),
      autoFocus: true,
      size: 64,
      type: "text",
      autoComplete: "off",
      value: this.props.query,
      onChange: this.onQuery,
      className: "mx_TextInputDialog_input mx_DevTools_RoomStateExplorer_query" // force re-render so that autoFocus is applied when this component is re-used
      ,
      key: this.props.children[0] ? this.props.children[0].key : ''
    }), _react.default.createElement(TruncatedList, {
      getChildren: this.getChildren,
      getChildCount: this.getChildCount,
      truncateAt: this.state.truncateAt,
      createOverflowElement: this.createOverflowElement
    }));
  }

}

(0, _defineProperty2.default)(FilteredList, "propTypes", {
  children: _propTypes.default.any,
  query: _propTypes.default.string,
  onChange: _propTypes.default.func
});

class RoomStateExplorer extends _react.default.PureComponent {
  static getLabel() {
    return (0, _languageHandler._t)('Explore Room State');
  }

  constructor(props) {
    super(props);
    this.roomStateEvents = this.props.room.currentState.events;
    this.onBack = this.onBack.bind(this);
    this.editEv = this.editEv.bind(this);
    this.onQueryEventType = this.onQueryEventType.bind(this);
    this.onQueryStateKey = this.onQueryStateKey.bind(this);
    this.state = {
      eventType: null,
      event: null,
      editing: false,
      queryEventType: '',
      queryStateKey: ''
    };
  }

  browseEventType(eventType) {
    return () => {
      this.setState({
        eventType
      });
    };
  }

  onViewSourceClick(event) {
    return () => {
      this.setState({
        event
      });
    };
  }

  onBack() {
    if (this.state.editing) {
      this.setState({
        editing: false
      });
    } else if (this.state.event) {
      this.setState({
        event: null
      });
    } else if (this.state.eventType) {
      this.setState({
        eventType: null
      });
    } else {
      this.props.onBack();
    }
  }

  editEv() {
    this.setState({
      editing: true
    });
  }

  onQueryEventType(filterEventType) {
    this.setState({
      queryEventType: filterEventType
    });
  }

  onQueryStateKey(filterStateKey) {
    this.setState({
      queryStateKey: filterStateKey
    });
  }

  render() {
    if (this.state.event) {
      if (this.state.editing) {
        return _react.default.createElement(SendCustomEvent, {
          room: this.props.room,
          forceStateEvent: true,
          onBack: this.onBack,
          inputs: {
            eventType: this.state.event.getType(),
            evContent: JSON.stringify(this.state.event.getContent(), null, '\t'),
            stateKey: this.state.event.getStateKey()
          }
        });
      }

      return _react.default.createElement("div", {
        className: "mx_ViewSource"
      }, _react.default.createElement("div", {
        className: "mx_Dialog_content"
      }, _react.default.createElement(_SyntaxHighlight.default, {
        className: "json"
      }, JSON.stringify(this.state.event.event, null, 2))), _react.default.createElement("div", {
        className: "mx_Dialog_buttons"
      }, _react.default.createElement("button", {
        onClick: this.onBack
      }, (0, _languageHandler._t)('Back')), _react.default.createElement("button", {
        onClick: this.editEv
      }, (0, _languageHandler._t)('Edit'))));
    }

    let list = null;
    const classes = 'mx_DevTools_RoomStateExplorer_button';

    if (this.state.eventType === null) {
      list = _react.default.createElement(FilteredList, {
        query: this.state.queryEventType,
        onChange: this.onQueryEventType
      }, Object.keys(this.roomStateEvents).map(evType => {
        const stateGroup = this.roomStateEvents[evType];
        const stateKeys = Object.keys(stateGroup);
        let onClickFn;

        if (stateKeys.length === 1 && stateKeys[0] === '') {
          onClickFn = this.onViewSourceClick(stateGroup[stateKeys[0]]);
        } else {
          onClickFn = this.browseEventType(evType);
        }

        return _react.default.createElement("button", {
          className: classes,
          key: evType,
          onClick: onClickFn
        }, evType);
      }));
    } else {
      const stateGroup = this.roomStateEvents[this.state.eventType];
      list = _react.default.createElement(FilteredList, {
        query: this.state.queryStateKey,
        onChange: this.onQueryStateKey
      }, Object.keys(stateGroup).map(stateKey => {
        const ev = stateGroup[stateKey];
        return _react.default.createElement("button", {
          className: classes,
          key: stateKey,
          onClick: this.onViewSourceClick(ev)
        }, stateKey);
      }));
    }

    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, list), _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.onBack
    }, (0, _languageHandler._t)('Back'))));
  }

}

(0, _defineProperty2.default)(RoomStateExplorer, "propTypes", {
  onBack: _propTypes.default.func.isRequired,
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired
});
(0, _defineProperty2.default)(RoomStateExplorer, "contextType", _MatrixClientContext.default);

class AccountDataExplorer extends _react.default.PureComponent {
  static getLabel() {
    return (0, _languageHandler._t)('Explore Account Data');
  }

  constructor(props) {
    super(props);
    this.onBack = this.onBack.bind(this);
    this.editEv = this.editEv.bind(this);
    this._onChange = this._onChange.bind(this);
    this.onQueryEventType = this.onQueryEventType.bind(this);
    this.state = {
      isRoomAccountData: false,
      event: null,
      editing: false,
      queryEventType: ''
    };
  }

  getData() {
    if (this.state.isRoomAccountData) {
      return this.props.room.accountData;
    }

    return this.context.store.accountData;
  }

  onViewSourceClick(event) {
    return () => {
      this.setState({
        event
      });
    };
  }

  onBack() {
    if (this.state.editing) {
      this.setState({
        editing: false
      });
    } else if (this.state.event) {
      this.setState({
        event: null
      });
    } else {
      this.props.onBack();
    }
  }

  _onChange(e) {
    this.setState({
      [e.target.id]: e.target.type === 'checkbox' ? e.target.checked : e.target.value
    });
  }

  editEv() {
    this.setState({
      editing: true
    });
  }

  onQueryEventType(queryEventType) {
    this.setState({
      queryEventType
    });
  }

  render() {
    if (this.state.event) {
      if (this.state.editing) {
        return _react.default.createElement(SendAccountData, {
          room: this.props.room,
          isRoomAccountData: this.state.isRoomAccountData,
          onBack: this.onBack,
          inputs: {
            eventType: this.state.event.getType(),
            evContent: JSON.stringify(this.state.event.getContent(), null, '\t')
          },
          forceMode: true
        });
      }

      return _react.default.createElement("div", {
        className: "mx_ViewSource"
      }, _react.default.createElement("div", {
        className: "mx_DevTools_content"
      }, _react.default.createElement(_SyntaxHighlight.default, {
        className: "json"
      }, JSON.stringify(this.state.event.event, null, 2))), _react.default.createElement("div", {
        className: "mx_Dialog_buttons"
      }, _react.default.createElement("button", {
        onClick: this.onBack
      }, (0, _languageHandler._t)('Back')), _react.default.createElement("button", {
        onClick: this.editEv
      }, (0, _languageHandler._t)('Edit'))));
    }

    const rows = [];
    const classes = 'mx_DevTools_RoomStateExplorer_button';
    const data = this.getData();
    Object.keys(data).forEach(evType => {
      const ev = data[evType];
      rows.push(_react.default.createElement("button", {
        className: classes,
        key: evType,
        onClick: this.onViewSourceClick(ev)
      }, evType));
    });
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, _react.default.createElement(FilteredList, {
      query: this.state.queryEventType,
      onChange: this.onQueryEventType
    }, rows)), _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.onBack
    }, (0, _languageHandler._t)('Back')), !this.state.message && _react.default.createElement("div", {
      style: {
        float: "right"
      }
    }, _react.default.createElement("input", {
      id: "isRoomAccountData",
      className: "mx_DevTools_tgl mx_DevTools_tgl-flip",
      type: "checkbox",
      onChange: this._onChange,
      checked: this.state.isRoomAccountData
    }), _react.default.createElement("label", {
      className: "mx_DevTools_tgl-btn",
      "data-tg-off": "Account Data",
      "data-tg-on": "Room Data",
      htmlFor: "isRoomAccountData"
    }))));
  }

}

(0, _defineProperty2.default)(AccountDataExplorer, "propTypes", {
  onBack: _propTypes.default.func.isRequired,
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired
});
(0, _defineProperty2.default)(AccountDataExplorer, "contextType", _MatrixClientContext.default);

class ServersInRoomList extends _react.default.PureComponent {
  static getLabel() {
    return (0, _languageHandler._t)('View Servers in Room');
  }

  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onQuery", query => {
      this.setState({
        query
      });
    });
    const room = this.props.room;
    const servers = new Set();
    room.currentState.getStateEvents("m.room.member").forEach(ev => servers.add(ev.getSender().split(":")[1]));
    this.servers = Array.from(servers).map(s => _react.default.createElement("button", {
      key: s,
      className: "mx_DevTools_ServersInRoomList_button"
    }, s));
    this.state = {
      query: ''
    };
  }

  render() {
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, _react.default.createElement(FilteredList, {
      query: this.state.query,
      onChange: this.onQuery
    }, this.servers)), _react.default.createElement("div", {
      className: "mx_Dialog_buttons"
    }, _react.default.createElement("button", {
      onClick: this.props.onBack
    }, (0, _languageHandler._t)('Back'))));
  }

}

(0, _defineProperty2.default)(ServersInRoomList, "propTypes", {
  onBack: _propTypes.default.func.isRequired,
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired
});
(0, _defineProperty2.default)(ServersInRoomList, "contextType", _MatrixClientContext.default);
const PHASE_MAP = {
  [_VerificationRequest.PHASE_UNSENT]: "unsent",
  [_VerificationRequest.PHASE_REQUESTED]: "requested",
  [_VerificationRequest.PHASE_READY]: "ready",
  [_VerificationRequest.PHASE_DONE]: "done",
  [_VerificationRequest.PHASE_STARTED]: "started",
  [_VerificationRequest.PHASE_CANCELLED]: "cancelled"
};

function VerificationRequest({
  txnId,
  request
}) {
  const [, updateState] = (0, _react.useState)();
  const [timeout, setRequestTimeout] = (0, _react.useState)(request.timeout);
  /* Re-render if something changes state */

  (0, _useEventEmitter.useEventEmitter)(request, "change", updateState);
  /* Keep re-rendering if there's a timeout */

  (0, _react.useEffect)(() => {
    if (request.timeout == 0) return;
    /* Note that request.timeout is a getter, so its value changes */

    const id = setInterval(() => {
      setRequestTimeout(request.timeout);
    }, 500);
    return () => {
      clearInterval(id);
    };
  }, [request]);
  return _react.default.createElement("div", {
    className: "mx_DevTools_VerificationRequest"
  }, _react.default.createElement("dl", null, _react.default.createElement("dt", null, "Transaction"), _react.default.createElement("dd", null, txnId), _react.default.createElement("dt", null, "Phase"), _react.default.createElement("dd", null, PHASE_MAP[request.phase] || request.phase), _react.default.createElement("dt", null, "Timeout"), _react.default.createElement("dd", null, Math.floor(timeout / 1000)), _react.default.createElement("dt", null, "Methods"), _react.default.createElement("dd", null, request.methods && request.methods.join(", ")), _react.default.createElement("dt", null, "requestingUserId"), _react.default.createElement("dd", null, request.requestingUserId), _react.default.createElement("dt", null, "observeOnly"), _react.default.createElement("dd", null, JSON.stringify(request.observeOnly))));
}

class VerificationExplorer extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onNewRequest", () => {
      this.forceUpdate();
    });
  }

  static getLabel() {
    return (0, _languageHandler._t)("Verification Requests");
  }
  /* Ensure this.context is the cli */


  componentDidMount() {
    const cli = this.context;
    cli.on("crypto.verification.request", this.onNewRequest);
  }

  componentWillUnmount() {
    const cli = this.context;
    cli.off("crypto.verification.request", this.onNewRequest);
  }

  render() {
    const cli = this.context;
    const room = this.props.room;
    const inRoomChannel = cli._crypto._inRoomVerificationRequests;
    const inRoomRequests = (inRoomChannel._requestsByRoomId || new Map()).get(room.roomId) || new Map();
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, Array.from(inRoomRequests.entries()).reverse().map(([txnId, request]) => _react.default.createElement(VerificationRequest, {
      txnId: txnId,
      request: request,
      key: txnId
    }))));
  }

}

(0, _defineProperty2.default)(VerificationExplorer, "contextType", _MatrixClientContext.default);
const Entries = [SendCustomEvent, RoomStateExplorer, SendAccountData, AccountDataExplorer, ServersInRoomList, VerificationExplorer];

class DevtoolsDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    this.onBack = this.onBack.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.state = {
      mode: null
    };
  }

  componentWillUnmount() {
    this._unmounted = true;
  }

  _setMode(mode) {
    return () => {
      this.setState({
        mode
      });
    };
  }

  onBack() {
    if (this.prevMode) {
      this.setState({
        mode: this.prevMode
      });
      this.prevMode = null;
    } else {
      this.setState({
        mode: null
      });
    }
  }

  onCancel() {
    this.props.onFinished(false);
  }

  render() {
    let body;

    if (this.state.mode) {
      body = _react.default.createElement(_MatrixClientContext.default.Consumer, null, cli => _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
        className: "mx_DevTools_label_left"
      }, this.state.mode.getLabel()), _react.default.createElement("div", {
        className: "mx_DevTools_label_right"
      }, "Room ID: ", this.props.roomId), _react.default.createElement("div", {
        className: "mx_DevTools_label_bottom"
      }), _react.default.createElement(this.state.mode, {
        onBack: this.onBack,
        room: cli.getRoom(this.props.roomId)
      })));
    } else {
      const classes = "mx_DevTools_RoomStateExplorer_button";
      body = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", null, _react.default.createElement("div", {
        className: "mx_DevTools_label_left"
      }, (0, _languageHandler._t)('Toolbox')), _react.default.createElement("div", {
        className: "mx_DevTools_label_right"
      }, "Room ID: ", this.props.roomId), _react.default.createElement("div", {
        className: "mx_DevTools_label_bottom"
      }), _react.default.createElement("div", {
        className: "mx_Dialog_content"
      }, Entries.map(Entry => {
        const label = Entry.getLabel();

        const onClick = this._setMode(Entry);

        return _react.default.createElement("button", {
          className: classes,
          key: label,
          onClick: onClick
        }, label);
      }))), _react.default.createElement("div", {
        className: "mx_Dialog_buttons"
      }, _react.default.createElement("button", {
        onClick: this.onCancel
      }, (0, _languageHandler._t)('Cancel'))));
    }

    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    return _react.default.createElement(BaseDialog, {
      className: "mx_QuestionDialog",
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)('Developer Tools')
    }, body);
  }

}

exports.default = DevtoolsDialog;
(0, _defineProperty2.default)(DevtoolsDialog, "propTypes", {
  roomId: _propTypes.default.string.isRequired,
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvRGV2dG9vbHNEaWFsb2cuanMiXSwibmFtZXMiOlsiR2VuZXJpY0VkaXRvciIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJfb25DaGFuZ2UiLCJiaW5kIiwib25CYWNrIiwic3RhdGUiLCJtZXNzYWdlIiwic2V0U3RhdGUiLCJlIiwidGFyZ2V0IiwiaWQiLCJ0eXBlIiwiY2hlY2tlZCIsInZhbHVlIiwiX2J1dHRvbnMiLCJfc2VuZCIsInRleHRJbnB1dCIsImxhYmVsIiwiU2VuZEN1c3RvbUV2ZW50IiwiZ2V0TGFiZWwiLCJldmVudFR5cGUiLCJzdGF0ZUtleSIsImV2Q29udGVudCIsIk9iamVjdCIsImFzc2lnbiIsImlucHV0cyIsImlzU3RhdGVFdmVudCIsIkJvb2xlYW4iLCJmb3JjZVN0YXRlRXZlbnQiLCJzZW5kIiwiY29udGVudCIsImNsaSIsImNvbnRleHQiLCJzZW5kU3RhdGVFdmVudCIsInJvb20iLCJyb29tSWQiLCJzZW5kRXZlbnQiLCJKU09OIiwicGFyc2UiLCJ0b1N0cmluZyIsInJlbmRlciIsImZsb2F0IiwiUHJvcFR5cGVzIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJpbnN0YW5jZU9mIiwiUm9vbSIsImJvb2wiLCJvYmplY3QiLCJNYXRyaXhDbGllbnRDb250ZXh0IiwiU2VuZEFjY291bnREYXRhIiwiaXNSb29tQWNjb3VudERhdGEiLCJzZXRSb29tQWNjb3VudERhdGEiLCJzZXRBY2NvdW50RGF0YSIsImZvcmNlTW9kZSIsIklOSVRJQUxfTE9BRF9USUxFUyIsIkxPQURfVElMRVNfU1RFUF9TSVpFIiwiRmlsdGVyZWRMaXN0IiwiZmlsdGVyQ2hpbGRyZW4iLCJjaGlsZHJlbiIsInF1ZXJ5IiwibGNRdWVyeSIsInRvTG93ZXJDYXNlIiwiZmlsdGVyIiwiY2hpbGQiLCJrZXkiLCJpbmNsdWRlcyIsInRydW5jYXRlQXQiLCJvdmVyZmxvd0NvdW50IiwidG90YWxDb3VudCIsInNob3dBbGwiLCJjb3VudCIsImV2Iiwib25DaGFuZ2UiLCJzdGFydCIsImVuZCIsImZpbHRlcmVkQ2hpbGRyZW4iLCJzbGljZSIsImxlbmd0aCIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV4dFByb3BzIiwiVHJ1bmNhdGVkTGlzdCIsInNkayIsImdldENvbXBvbmVudCIsIm9uUXVlcnkiLCJnZXRDaGlsZHJlbiIsImdldENoaWxkQ291bnQiLCJjcmVhdGVPdmVyZmxvd0VsZW1lbnQiLCJhbnkiLCJzdHJpbmciLCJSb29tU3RhdGVFeHBsb3JlciIsInJvb21TdGF0ZUV2ZW50cyIsImN1cnJlbnRTdGF0ZSIsImV2ZW50cyIsImVkaXRFdiIsIm9uUXVlcnlFdmVudFR5cGUiLCJvblF1ZXJ5U3RhdGVLZXkiLCJldmVudCIsImVkaXRpbmciLCJxdWVyeUV2ZW50VHlwZSIsInF1ZXJ5U3RhdGVLZXkiLCJicm93c2VFdmVudFR5cGUiLCJvblZpZXdTb3VyY2VDbGljayIsImZpbHRlckV2ZW50VHlwZSIsImZpbHRlclN0YXRlS2V5IiwiZ2V0VHlwZSIsInN0cmluZ2lmeSIsImdldENvbnRlbnQiLCJnZXRTdGF0ZUtleSIsImxpc3QiLCJjbGFzc2VzIiwia2V5cyIsIm1hcCIsImV2VHlwZSIsInN0YXRlR3JvdXAiLCJzdGF0ZUtleXMiLCJvbkNsaWNrRm4iLCJBY2NvdW50RGF0YUV4cGxvcmVyIiwiZ2V0RGF0YSIsImFjY291bnREYXRhIiwic3RvcmUiLCJyb3dzIiwiZGF0YSIsImZvckVhY2giLCJwdXNoIiwiU2VydmVyc0luUm9vbUxpc3QiLCJzZXJ2ZXJzIiwiU2V0IiwiZ2V0U3RhdGVFdmVudHMiLCJhZGQiLCJnZXRTZW5kZXIiLCJzcGxpdCIsIkFycmF5IiwiZnJvbSIsInMiLCJQSEFTRV9NQVAiLCJQSEFTRV9VTlNFTlQiLCJQSEFTRV9SRVFVRVNURUQiLCJQSEFTRV9SRUFEWSIsIlBIQVNFX0RPTkUiLCJQSEFTRV9TVEFSVEVEIiwiUEhBU0VfQ0FOQ0VMTEVEIiwiVmVyaWZpY2F0aW9uUmVxdWVzdCIsInR4bklkIiwicmVxdWVzdCIsInVwZGF0ZVN0YXRlIiwidGltZW91dCIsInNldFJlcXVlc3RUaW1lb3V0Iiwic2V0SW50ZXJ2YWwiLCJjbGVhckludGVydmFsIiwicGhhc2UiLCJNYXRoIiwiZmxvb3IiLCJtZXRob2RzIiwiam9pbiIsInJlcXVlc3RpbmdVc2VySWQiLCJvYnNlcnZlT25seSIsIlZlcmlmaWNhdGlvbkV4cGxvcmVyIiwiQ29tcG9uZW50IiwiZm9yY2VVcGRhdGUiLCJjb21wb25lbnREaWRNb3VudCIsIm9uIiwib25OZXdSZXF1ZXN0IiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJvZmYiLCJpblJvb21DaGFubmVsIiwiX2NyeXB0byIsIl9pblJvb21WZXJpZmljYXRpb25SZXF1ZXN0cyIsImluUm9vbVJlcXVlc3RzIiwiX3JlcXVlc3RzQnlSb29tSWQiLCJNYXAiLCJnZXQiLCJlbnRyaWVzIiwicmV2ZXJzZSIsIkVudHJpZXMiLCJEZXZ0b29sc0RpYWxvZyIsIm9uQ2FuY2VsIiwibW9kZSIsIl91bm1vdW50ZWQiLCJfc2V0TW9kZSIsInByZXZNb2RlIiwib25GaW5pc2hlZCIsImJvZHkiLCJnZXRSb29tIiwiRW50cnkiLCJvbkNsaWNrIiwiQmFzZURpYWxvZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUExQkE7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxNQUFNQSxhQUFOLFNBQTRCQyxlQUFNQyxhQUFsQyxDQUFnRDtBQUM1QztBQUVBQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFDQSxTQUFLQyxTQUFMLEdBQWlCLEtBQUtBLFNBQUwsQ0FBZUMsSUFBZixDQUFvQixJQUFwQixDQUFqQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlELElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNIOztBQUVEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtDLEtBQUwsQ0FBV0MsT0FBZixFQUF3QjtBQUNwQixXQUFLQyxRQUFMLENBQWM7QUFBRUQsUUFBQUEsT0FBTyxFQUFFO0FBQVgsT0FBZDtBQUNILEtBRkQsTUFFTztBQUNILFdBQUtMLEtBQUwsQ0FBV0csTUFBWDtBQUNIO0FBQ0o7O0FBRURGLEVBQUFBLFNBQVMsQ0FBQ00sQ0FBRCxFQUFJO0FBQ1QsU0FBS0QsUUFBTCxDQUFjO0FBQUMsT0FBQ0MsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEVBQVYsR0FBZUYsQ0FBQyxDQUFDQyxNQUFGLENBQVNFLElBQVQsS0FBa0IsVUFBbEIsR0FBK0JILENBQUMsQ0FBQ0MsTUFBRixDQUFTRyxPQUF4QyxHQUFrREosQ0FBQyxDQUFDQyxNQUFGLENBQVNJO0FBQTNFLEtBQWQ7QUFDSDs7QUFFREMsRUFBQUEsUUFBUSxHQUFHO0FBQ1AsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSDtBQUFRLE1BQUEsT0FBTyxFQUFFLEtBQUtWO0FBQXRCLE9BQWdDLHlCQUFHLE1BQUgsQ0FBaEMsQ0FERyxFQUVELENBQUMsS0FBS0MsS0FBTCxDQUFXQyxPQUFaLElBQXVCO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS1M7QUFBdEIsT0FBK0IseUJBQUcsTUFBSCxDQUEvQixDQUZ0QixDQUFQO0FBSUg7O0FBRURDLEVBQUFBLFNBQVMsQ0FBQ04sRUFBRCxFQUFLTyxLQUFMLEVBQVk7QUFDakIsV0FBTyw2QkFBQyxjQUFEO0FBQU8sTUFBQSxFQUFFLEVBQUVQLEVBQVg7QUFBZSxNQUFBLEtBQUssRUFBRU8sS0FBdEI7QUFBNkIsTUFBQSxJQUFJLEVBQUMsSUFBbEM7QUFBdUMsTUFBQSxTQUFTLEVBQUUsSUFBbEQ7QUFBd0QsTUFBQSxJQUFJLEVBQUMsTUFBN0Q7QUFBb0UsTUFBQSxZQUFZLEVBQUMsSUFBakY7QUFDTyxNQUFBLEtBQUssRUFBRSxLQUFLWixLQUFMLENBQVdLLEVBQVgsQ0FEZDtBQUM4QixNQUFBLFFBQVEsRUFBRSxLQUFLUjtBQUQ3QyxNQUFQO0FBRUg7O0FBL0IyQzs7QUFrQ2hELE1BQU1nQixlQUFOLFNBQThCckIsYUFBOUIsQ0FBNEM7QUFDeEMsU0FBT3NCLFFBQVAsR0FBa0I7QUFBRSxXQUFPLHlCQUFHLG1CQUFILENBQVA7QUFBaUM7O0FBV3JEbkIsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBQ0EsU0FBS2MsS0FBTCxHQUFhLEtBQUtBLEtBQUwsQ0FBV1osSUFBWCxDQUFnQixJQUFoQixDQUFiO0FBRUEsVUFBTTtBQUFDaUIsTUFBQUEsU0FBRDtBQUFZQyxNQUFBQSxRQUFaO0FBQXNCQyxNQUFBQTtBQUF0QixRQUFtQ0MsTUFBTSxDQUFDQyxNQUFQLENBQWM7QUFDbkRKLE1BQUFBLFNBQVMsRUFBRSxFQUR3QztBQUVuREMsTUFBQUEsUUFBUSxFQUFFLEVBRnlDO0FBR25EQyxNQUFBQSxTQUFTLEVBQUU7QUFId0MsS0FBZCxFQUl0QyxLQUFLckIsS0FBTCxDQUFXd0IsTUFKMkIsQ0FBekM7QUFNQSxTQUFLcEIsS0FBTCxHQUFhO0FBQ1RxQixNQUFBQSxZQUFZLEVBQUVDLE9BQU8sQ0FBQyxLQUFLMUIsS0FBTCxDQUFXMkIsZUFBWixDQURaO0FBR1RSLE1BQUFBLFNBSFM7QUFJVEMsTUFBQUEsUUFKUztBQUtUQyxNQUFBQTtBQUxTLEtBQWI7QUFPSDs7QUFFRE8sRUFBQUEsSUFBSSxDQUFDQyxPQUFELEVBQVU7QUFDVixVQUFNQyxHQUFHLEdBQUcsS0FBS0MsT0FBakI7O0FBQ0EsUUFBSSxLQUFLM0IsS0FBTCxDQUFXcUIsWUFBZixFQUE2QjtBQUN6QixhQUFPSyxHQUFHLENBQUNFLGNBQUosQ0FBbUIsS0FBS2hDLEtBQUwsQ0FBV2lDLElBQVgsQ0FBZ0JDLE1BQW5DLEVBQTJDLEtBQUs5QixLQUFMLENBQVdlLFNBQXRELEVBQWlFVSxPQUFqRSxFQUEwRSxLQUFLekIsS0FBTCxDQUFXZ0IsUUFBckYsQ0FBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU9VLEdBQUcsQ0FBQ0ssU0FBSixDQUFjLEtBQUtuQyxLQUFMLENBQVdpQyxJQUFYLENBQWdCQyxNQUE5QixFQUFzQyxLQUFLOUIsS0FBTCxDQUFXZSxTQUFqRCxFQUE0RFUsT0FBNUQsQ0FBUDtBQUNIO0FBQ0o7O0FBRUQsUUFBTWYsS0FBTixHQUFjO0FBQ1YsUUFBSSxLQUFLVixLQUFMLENBQVdlLFNBQVgsS0FBeUIsRUFBN0IsRUFBaUM7QUFDN0IsV0FBS2IsUUFBTCxDQUFjO0FBQUVELFFBQUFBLE9BQU8sRUFBRSx5QkFBRyxpQ0FBSDtBQUFYLE9BQWQ7QUFDQTtBQUNIOztBQUVELFFBQUlBLE9BQUo7O0FBQ0EsUUFBSTtBQUNBLFlBQU13QixPQUFPLEdBQUdPLElBQUksQ0FBQ0MsS0FBTCxDQUFXLEtBQUtqQyxLQUFMLENBQVdpQixTQUF0QixDQUFoQjtBQUNBLFlBQU0sS0FBS08sSUFBTCxDQUFVQyxPQUFWLENBQU47QUFDQXhCLE1BQUFBLE9BQU8sR0FBRyx5QkFBRyxhQUFILENBQVY7QUFDSCxLQUpELENBSUUsT0FBT0UsQ0FBUCxFQUFVO0FBQ1JGLE1BQUFBLE9BQU8sR0FBRyx5QkFBRyw4QkFBSCxJQUFxQyxJQUFyQyxHQUE0Q0UsQ0FBQyxDQUFDK0IsUUFBRixFQUE1QyxHQUEyRCxHQUFyRTtBQUNIOztBQUNELFNBQUtoQyxRQUFMLENBQWM7QUFBRUQsTUFBQUE7QUFBRixLQUFkO0FBQ0g7O0FBRURrQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtuQyxLQUFMLENBQVdDLE9BQWYsRUFBd0I7QUFDcEIsYUFBTywwQ0FDSDtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDTSxLQUFLRCxLQUFMLENBQVdDLE9BRGpCLENBREcsRUFJRCxLQUFLUSxRQUFMLEVBSkMsQ0FBUDtBQU1IOztBQUVELFdBQU8sMENBQ0g7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ00sS0FBS0UsU0FBTCxDQUFlLFdBQWYsRUFBNEIseUJBQUcsWUFBSCxDQUE1QixDQUROLEVBRU0sS0FBS1gsS0FBTCxDQUFXcUIsWUFBWCxJQUEyQixLQUFLVixTQUFMLENBQWUsVUFBZixFQUEyQix5QkFBRyxXQUFILENBQTNCLENBRmpDLENBREosRUFNSSx3Q0FOSixFQVFJLDZCQUFDLGNBQUQ7QUFBTyxNQUFBLEVBQUUsRUFBQyxXQUFWO0FBQXNCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FBN0I7QUFBa0QsTUFBQSxJQUFJLEVBQUMsTUFBdkQ7QUFBOEQsTUFBQSxTQUFTLEVBQUMsc0JBQXhFO0FBQ08sTUFBQSxZQUFZLEVBQUMsS0FEcEI7QUFDMEIsTUFBQSxLQUFLLEVBQUUsS0FBS1gsS0FBTCxDQUFXaUIsU0FENUM7QUFDdUQsTUFBQSxRQUFRLEVBQUUsS0FBS3BCLFNBRHRFO0FBQ2lGLE1BQUEsT0FBTyxFQUFDO0FBRHpGLE1BUkosQ0FERyxFQVlIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS0U7QUFBdEIsT0FBZ0MseUJBQUcsTUFBSCxDQUFoQyxDQURKLEVBRU0sQ0FBQyxLQUFLQyxLQUFMLENBQVdDLE9BQVosSUFBdUI7QUFBUSxNQUFBLE9BQU8sRUFBRSxLQUFLUztBQUF0QixPQUErQix5QkFBRyxNQUFILENBQS9CLENBRjdCLEVBR00sQ0FBQyxLQUFLVixLQUFMLENBQVdDLE9BQVosSUFBdUIsQ0FBQyxLQUFLTCxLQUFMLENBQVcyQixlQUFuQyxJQUFzRDtBQUFLLE1BQUEsS0FBSyxFQUFFO0FBQUNhLFFBQUFBLEtBQUssRUFBRTtBQUFSO0FBQVosT0FDcEQ7QUFBTyxNQUFBLEVBQUUsRUFBQyxjQUFWO0FBQXlCLE1BQUEsU0FBUyxFQUFDLHNDQUFuQztBQUEwRSxNQUFBLElBQUksRUFBQyxVQUEvRTtBQUEwRixNQUFBLFFBQVEsRUFBRSxLQUFLdkMsU0FBekc7QUFBb0gsTUFBQSxPQUFPLEVBQUUsS0FBS0csS0FBTCxDQUFXcUI7QUFBeEksTUFEb0QsRUFFcEQ7QUFBTyxNQUFBLFNBQVMsRUFBQyxxQkFBakI7QUFBdUMscUJBQVksT0FBbkQ7QUFBMkQsb0JBQVcsYUFBdEU7QUFBb0YsTUFBQSxPQUFPLEVBQUM7QUFBNUYsTUFGb0QsQ0FINUQsQ0FaRyxDQUFQO0FBcUJIOztBQXhGdUM7OzhCQUF0Q1IsZSxlQUdpQjtBQUNmZCxFQUFBQSxNQUFNLEVBQUVzQyxtQkFBVUMsSUFBVixDQUFlQyxVQURSO0FBRWZWLEVBQUFBLElBQUksRUFBRVEsbUJBQVVHLFVBQVYsQ0FBcUJDLGlCQUFyQixFQUEyQkYsVUFGbEI7QUFHZmhCLEVBQUFBLGVBQWUsRUFBRWMsbUJBQVVLLElBSFo7QUFJZnRCLEVBQUFBLE1BQU0sRUFBRWlCLG1CQUFVTTtBQUpILEM7OEJBSGpCOUIsZSxpQkFVbUIrQiw0Qjs7QUFpRnpCLE1BQU1DLGVBQU4sU0FBOEJyRCxhQUE5QixDQUE0QztBQUN4QyxTQUFPc0IsUUFBUCxHQUFrQjtBQUFFLFdBQU8seUJBQUcsbUJBQUgsQ0FBUDtBQUFpQzs7QUFXckRuQixFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFDQSxTQUFLYyxLQUFMLEdBQWEsS0FBS0EsS0FBTCxDQUFXWixJQUFYLENBQWdCLElBQWhCLENBQWI7QUFFQSxVQUFNO0FBQUNpQixNQUFBQSxTQUFEO0FBQVlFLE1BQUFBO0FBQVosUUFBeUJDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjO0FBQ3pDSixNQUFBQSxTQUFTLEVBQUUsRUFEOEI7QUFFekNFLE1BQUFBLFNBQVMsRUFBRTtBQUY4QixLQUFkLEVBRzVCLEtBQUtyQixLQUFMLENBQVd3QixNQUhpQixDQUEvQjtBQUtBLFNBQUtwQixLQUFMLEdBQWE7QUFDVDhDLE1BQUFBLGlCQUFpQixFQUFFeEIsT0FBTyxDQUFDLEtBQUsxQixLQUFMLENBQVdrRCxpQkFBWixDQURqQjtBQUdUL0IsTUFBQUEsU0FIUztBQUlURSxNQUFBQTtBQUpTLEtBQWI7QUFNSDs7QUFFRE8sRUFBQUEsSUFBSSxDQUFDQyxPQUFELEVBQVU7QUFDVixVQUFNQyxHQUFHLEdBQUcsS0FBS0MsT0FBakI7O0FBQ0EsUUFBSSxLQUFLM0IsS0FBTCxDQUFXOEMsaUJBQWYsRUFBa0M7QUFDOUIsYUFBT3BCLEdBQUcsQ0FBQ3FCLGtCQUFKLENBQXVCLEtBQUtuRCxLQUFMLENBQVdpQyxJQUFYLENBQWdCQyxNQUF2QyxFQUErQyxLQUFLOUIsS0FBTCxDQUFXZSxTQUExRCxFQUFxRVUsT0FBckUsQ0FBUDtBQUNIOztBQUNELFdBQU9DLEdBQUcsQ0FBQ3NCLGNBQUosQ0FBbUIsS0FBS2hELEtBQUwsQ0FBV2UsU0FBOUIsRUFBeUNVLE9BQXpDLENBQVA7QUFDSDs7QUFFRCxRQUFNZixLQUFOLEdBQWM7QUFDVixRQUFJLEtBQUtWLEtBQUwsQ0FBV2UsU0FBWCxLQUF5QixFQUE3QixFQUFpQztBQUM3QixXQUFLYixRQUFMLENBQWM7QUFBRUQsUUFBQUEsT0FBTyxFQUFFLHlCQUFHLGlDQUFIO0FBQVgsT0FBZDtBQUNBO0FBQ0g7O0FBRUQsUUFBSUEsT0FBSjs7QUFDQSxRQUFJO0FBQ0EsWUFBTXdCLE9BQU8sR0FBR08sSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS2pDLEtBQUwsQ0FBV2lCLFNBQXRCLENBQWhCO0FBQ0EsWUFBTSxLQUFLTyxJQUFMLENBQVVDLE9BQVYsQ0FBTjtBQUNBeEIsTUFBQUEsT0FBTyxHQUFHLHlCQUFHLGFBQUgsQ0FBVjtBQUNILEtBSkQsQ0FJRSxPQUFPRSxDQUFQLEVBQVU7QUFDUkYsTUFBQUEsT0FBTyxHQUFHLHlCQUFHLDhCQUFILElBQXFDLElBQXJDLEdBQTRDRSxDQUFDLENBQUMrQixRQUFGLEVBQTVDLEdBQTJELEdBQXJFO0FBQ0g7O0FBQ0QsU0FBS2hDLFFBQUwsQ0FBYztBQUFFRCxNQUFBQTtBQUFGLEtBQWQ7QUFDSDs7QUFFRGtDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUksS0FBS25DLEtBQUwsQ0FBV0MsT0FBZixFQUF3QjtBQUNwQixhQUFPLDBDQUNIO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNLEtBQUtELEtBQUwsQ0FBV0MsT0FEakIsQ0FERyxFQUlELEtBQUtRLFFBQUwsRUFKQyxDQUFQO0FBTUg7O0FBRUQsV0FBTywwQ0FDSDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTSxLQUFLRSxTQUFMLENBQWUsV0FBZixFQUE0Qix5QkFBRyxZQUFILENBQTVCLENBRE4sRUFFSSx3Q0FGSixFQUlJLDZCQUFDLGNBQUQ7QUFBTyxNQUFBLEVBQUUsRUFBQyxXQUFWO0FBQXNCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FBN0I7QUFBa0QsTUFBQSxJQUFJLEVBQUMsTUFBdkQ7QUFBOEQsTUFBQSxTQUFTLEVBQUMsc0JBQXhFO0FBQ08sTUFBQSxZQUFZLEVBQUMsS0FEcEI7QUFDMEIsTUFBQSxLQUFLLEVBQUUsS0FBS1gsS0FBTCxDQUFXaUIsU0FENUM7QUFDdUQsTUFBQSxRQUFRLEVBQUUsS0FBS3BCLFNBRHRFO0FBQ2lGLE1BQUEsT0FBTyxFQUFDO0FBRHpGLE1BSkosQ0FERyxFQVFIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS0U7QUFBdEIsT0FBZ0MseUJBQUcsTUFBSCxDQUFoQyxDQURKLEVBRU0sQ0FBQyxLQUFLQyxLQUFMLENBQVdDLE9BQVosSUFBdUI7QUFBUSxNQUFBLE9BQU8sRUFBRSxLQUFLUztBQUF0QixPQUErQix5QkFBRyxNQUFILENBQS9CLENBRjdCLEVBR00sQ0FBQyxLQUFLVixLQUFMLENBQVdDLE9BQVosSUFBdUI7QUFBSyxNQUFBLEtBQUssRUFBRTtBQUFDbUMsUUFBQUEsS0FBSyxFQUFFO0FBQVI7QUFBWixPQUNyQjtBQUFPLE1BQUEsRUFBRSxFQUFDLG1CQUFWO0FBQThCLE1BQUEsU0FBUyxFQUFDLHNDQUF4QztBQUErRSxNQUFBLElBQUksRUFBQyxVQUFwRjtBQUErRixNQUFBLFFBQVEsRUFBRSxLQUFLdkMsU0FBOUc7QUFBeUgsTUFBQSxPQUFPLEVBQUUsS0FBS0csS0FBTCxDQUFXOEMsaUJBQTdJO0FBQWdLLE1BQUEsUUFBUSxFQUFFLEtBQUtsRCxLQUFMLENBQVdxRDtBQUFyTCxNQURxQixFQUVyQjtBQUFPLE1BQUEsU0FBUyxFQUFDLHFCQUFqQjtBQUF1QyxxQkFBWSxjQUFuRDtBQUFrRSxvQkFBVyxXQUE3RTtBQUF5RixNQUFBLE9BQU8sRUFBQztBQUFqRyxNQUZxQixDQUg3QixDQVJHLENBQVA7QUFpQkg7O0FBakZ1Qzs7OEJBQXRDSixlLGVBR2lCO0FBQ2ZoQixFQUFBQSxJQUFJLEVBQUVRLG1CQUFVRyxVQUFWLENBQXFCQyxpQkFBckIsRUFBMkJGLFVBRGxCO0FBRWZPLEVBQUFBLGlCQUFpQixFQUFFVCxtQkFBVUssSUFGZDtBQUdmTyxFQUFBQSxTQUFTLEVBQUVaLG1CQUFVSyxJQUhOO0FBSWZ0QixFQUFBQSxNQUFNLEVBQUVpQixtQkFBVU07QUFKSCxDOzhCQUhqQkUsZSxpQkFVbUJELDRCO0FBMEV6QixNQUFNTSxrQkFBa0IsR0FBRyxFQUEzQjtBQUNBLE1BQU1DLG9CQUFvQixHQUFHLEVBQTdCOztBQUVBLE1BQU1DLFlBQU4sU0FBMkIzRCxlQUFNQyxhQUFqQyxDQUErQztBQU8zQyxTQUFPMkQsY0FBUCxDQUFzQkMsUUFBdEIsRUFBZ0NDLEtBQWhDLEVBQXVDO0FBQ25DLFFBQUksQ0FBQ0EsS0FBTCxFQUFZLE9BQU9ELFFBQVA7QUFDWixVQUFNRSxPQUFPLEdBQUdELEtBQUssQ0FBQ0UsV0FBTixFQUFoQjtBQUNBLFdBQU9ILFFBQVEsQ0FBQ0ksTUFBVCxDQUFpQkMsS0FBRCxJQUFXQSxLQUFLLENBQUNDLEdBQU4sQ0FBVUgsV0FBVixHQUF3QkksUUFBeEIsQ0FBaUNMLE9BQWpDLENBQTNCLENBQVA7QUFDSDs7QUFFRDdELEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLG1EQWtCVCxNQUFNO0FBQ1osV0FBS00sUUFBTCxDQUFjO0FBQ1Y0RCxRQUFBQSxVQUFVLEVBQUUsS0FBSzlELEtBQUwsQ0FBVzhELFVBQVgsR0FBd0JYO0FBRDFCLE9BQWQ7QUFHSCxLQXRCa0I7QUFBQSxpRUF3QkssQ0FBQ1k7QUFBRDtBQUFBLE1BQXdCQztBQUF4QjtBQUFBLFNBQStDO0FBQ25FLGFBQU87QUFBUSxRQUFBLFNBQVMsRUFBQyxzQ0FBbEI7QUFBeUQsUUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBdkUsU0FDRCx5QkFBRyx5QkFBSCxFQUE4QjtBQUFFQyxRQUFBQSxLQUFLLEVBQUVIO0FBQVQsT0FBOUIsQ0FEQyxDQUFQO0FBR0gsS0E1QmtCO0FBQUEsbURBOEJSSSxFQUFELElBQVE7QUFDZCxVQUFJLEtBQUt2RSxLQUFMLENBQVd3RSxRQUFmLEVBQXlCLEtBQUt4RSxLQUFMLENBQVd3RSxRQUFYLENBQW9CRCxFQUFFLENBQUMvRCxNQUFILENBQVVJLEtBQTlCO0FBQzVCLEtBaENrQjtBQUFBLHVEQWtDTCxDQUFDNkQ7QUFBRDtBQUFBLE1BQWdCQztBQUFoQjtBQUFBLFNBQWdDO0FBQzFDLGFBQU8sS0FBS3RFLEtBQUwsQ0FBV3VFLGdCQUFYLENBQTRCQyxLQUE1QixDQUFrQ0gsS0FBbEMsRUFBeUNDLEdBQXpDLENBQVA7QUFDSCxLQXBDa0I7QUFBQSx5REFzQ0g7QUFBQTtBQUFjO0FBQzFCLGFBQU8sS0FBS3RFLEtBQUwsQ0FBV3VFLGdCQUFYLENBQTRCRSxNQUFuQztBQUNILEtBeENrQjtBQUdmLFNBQUt6RSxLQUFMLEdBQWE7QUFDVHVFLE1BQUFBLGdCQUFnQixFQUFFbkIsWUFBWSxDQUFDQyxjQUFiLENBQTRCLEtBQUt6RCxLQUFMLENBQVcwRCxRQUF2QyxFQUFpRCxLQUFLMUQsS0FBTCxDQUFXMkQsS0FBNUQsQ0FEVDtBQUVUTyxNQUFBQSxVQUFVLEVBQUVaO0FBRkgsS0FBYjtBQUlILEdBcEIwQyxDQXNCM0M7OztBQUNBd0IsRUFBQUEsZ0NBQWdDLENBQUNDLFNBQUQsRUFBWTtBQUFFO0FBQzFDLFFBQUksS0FBSy9FLEtBQUwsQ0FBVzBELFFBQVgsS0FBd0JxQixTQUFTLENBQUNyQixRQUFsQyxJQUE4QyxLQUFLMUQsS0FBTCxDQUFXMkQsS0FBWCxLQUFxQm9CLFNBQVMsQ0FBQ3BCLEtBQWpGLEVBQXdGO0FBQ3hGLFNBQUtyRCxRQUFMLENBQWM7QUFDVnFFLE1BQUFBLGdCQUFnQixFQUFFbkIsWUFBWSxDQUFDQyxjQUFiLENBQTRCc0IsU0FBUyxDQUFDckIsUUFBdEMsRUFBZ0RxQixTQUFTLENBQUNwQixLQUExRCxDQURSO0FBRVZPLE1BQUFBLFVBQVUsRUFBRVo7QUFGRixLQUFkO0FBSUg7O0FBMEJEZixFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNeUMsYUFBYSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBQ0EsV0FBTywwQ0FDSCw2QkFBQyxjQUFEO0FBQU8sTUFBQSxLQUFLLEVBQUUseUJBQUcsZ0JBQUgsQ0FBZDtBQUFvQyxNQUFBLFNBQVMsRUFBRSxJQUEvQztBQUFxRCxNQUFBLElBQUksRUFBRSxFQUEzRDtBQUNPLE1BQUEsSUFBSSxFQUFDLE1BRFo7QUFDbUIsTUFBQSxZQUFZLEVBQUMsS0FEaEM7QUFDc0MsTUFBQSxLQUFLLEVBQUUsS0FBS2xGLEtBQUwsQ0FBVzJELEtBRHhEO0FBQytELE1BQUEsUUFBUSxFQUFFLEtBQUt3QixPQUQ5RTtBQUVPLE1BQUEsU0FBUyxFQUFDLDhEQUZqQixDQUdPO0FBSFA7QUFJTyxNQUFBLEdBQUcsRUFBRSxLQUFLbkYsS0FBTCxDQUFXMEQsUUFBWCxDQUFvQixDQUFwQixJQUF5QixLQUFLMUQsS0FBTCxDQUFXMEQsUUFBWCxDQUFvQixDQUFwQixFQUF1Qk0sR0FBaEQsR0FBc0Q7QUFKbEUsTUFERyxFQU9ILDZCQUFDLGFBQUQ7QUFBZSxNQUFBLFdBQVcsRUFBRSxLQUFLb0IsV0FBakM7QUFDZSxNQUFBLGFBQWEsRUFBRSxLQUFLQyxhQURuQztBQUVlLE1BQUEsVUFBVSxFQUFFLEtBQUtqRixLQUFMLENBQVc4RCxVQUZ0QztBQUdlLE1BQUEscUJBQXFCLEVBQUUsS0FBS29CO0FBSDNDLE1BUEcsQ0FBUDtBQVlIOztBQXJFMEM7OzhCQUF6QzlCLFksZUFDaUI7QUFDZkUsRUFBQUEsUUFBUSxFQUFFakIsbUJBQVU4QyxHQURMO0FBRWY1QixFQUFBQSxLQUFLLEVBQUVsQixtQkFBVStDLE1BRkY7QUFHZmhCLEVBQUFBLFFBQVEsRUFBRS9CLG1CQUFVQztBQUhMLEM7O0FBdUV2QixNQUFNK0MsaUJBQU4sU0FBZ0M1RixlQUFNQyxhQUF0QyxDQUFvRDtBQUNoRCxTQUFPb0IsUUFBUCxHQUFrQjtBQUFFLFdBQU8seUJBQUcsb0JBQUgsQ0FBUDtBQUFrQzs7QUFTdERuQixFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFFQSxTQUFLMEYsZUFBTCxHQUF1QixLQUFLMUYsS0FBTCxDQUFXaUMsSUFBWCxDQUFnQjBELFlBQWhCLENBQTZCQyxNQUFwRDtBQUVBLFNBQUt6RixNQUFMLEdBQWMsS0FBS0EsTUFBTCxDQUFZRCxJQUFaLENBQWlCLElBQWpCLENBQWQ7QUFDQSxTQUFLMkYsTUFBTCxHQUFjLEtBQUtBLE1BQUwsQ0FBWTNGLElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNBLFNBQUs0RixnQkFBTCxHQUF3QixLQUFLQSxnQkFBTCxDQUFzQjVGLElBQXRCLENBQTJCLElBQTNCLENBQXhCO0FBQ0EsU0FBSzZGLGVBQUwsR0FBdUIsS0FBS0EsZUFBTCxDQUFxQjdGLElBQXJCLENBQTBCLElBQTFCLENBQXZCO0FBRUEsU0FBS0UsS0FBTCxHQUFhO0FBQ1RlLE1BQUFBLFNBQVMsRUFBRSxJQURGO0FBRVQ2RSxNQUFBQSxLQUFLLEVBQUUsSUFGRTtBQUdUQyxNQUFBQSxPQUFPLEVBQUUsS0FIQTtBQUtUQyxNQUFBQSxjQUFjLEVBQUUsRUFMUDtBQU1UQyxNQUFBQSxhQUFhLEVBQUU7QUFOTixLQUFiO0FBUUg7O0FBRURDLEVBQUFBLGVBQWUsQ0FBQ2pGLFNBQUQsRUFBWTtBQUN2QixXQUFPLE1BQU07QUFDVCxXQUFLYixRQUFMLENBQWM7QUFBRWEsUUFBQUE7QUFBRixPQUFkO0FBQ0gsS0FGRDtBQUdIOztBQUVEa0YsRUFBQUEsaUJBQWlCLENBQUNMLEtBQUQsRUFBUTtBQUNyQixXQUFPLE1BQU07QUFDVCxXQUFLMUYsUUFBTCxDQUFjO0FBQUUwRixRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQUZEO0FBR0g7O0FBRUQ3RixFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtDLEtBQUwsQ0FBVzZGLE9BQWYsRUFBd0I7QUFDcEIsV0FBSzNGLFFBQUwsQ0FBYztBQUFFMkYsUUFBQUEsT0FBTyxFQUFFO0FBQVgsT0FBZDtBQUNILEtBRkQsTUFFTyxJQUFJLEtBQUs3RixLQUFMLENBQVc0RixLQUFmLEVBQXNCO0FBQ3pCLFdBQUsxRixRQUFMLENBQWM7QUFBRTBGLFFBQUFBLEtBQUssRUFBRTtBQUFULE9BQWQ7QUFDSCxLQUZNLE1BRUEsSUFBSSxLQUFLNUYsS0FBTCxDQUFXZSxTQUFmLEVBQTBCO0FBQzdCLFdBQUtiLFFBQUwsQ0FBYztBQUFFYSxRQUFBQSxTQUFTLEVBQUU7QUFBYixPQUFkO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsV0FBS25CLEtBQUwsQ0FBV0csTUFBWDtBQUNIO0FBQ0o7O0FBRUQwRixFQUFBQSxNQUFNLEdBQUc7QUFDTCxTQUFLdkYsUUFBTCxDQUFjO0FBQUUyRixNQUFBQSxPQUFPLEVBQUU7QUFBWCxLQUFkO0FBQ0g7O0FBRURILEVBQUFBLGdCQUFnQixDQUFDUSxlQUFELEVBQWtCO0FBQzlCLFNBQUtoRyxRQUFMLENBQWM7QUFBRTRGLE1BQUFBLGNBQWMsRUFBRUk7QUFBbEIsS0FBZDtBQUNIOztBQUVEUCxFQUFBQSxlQUFlLENBQUNRLGNBQUQsRUFBaUI7QUFDNUIsU0FBS2pHLFFBQUwsQ0FBYztBQUFFNkYsTUFBQUEsYUFBYSxFQUFFSTtBQUFqQixLQUFkO0FBQ0g7O0FBRURoRSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtuQyxLQUFMLENBQVc0RixLQUFmLEVBQXNCO0FBQ2xCLFVBQUksS0FBSzVGLEtBQUwsQ0FBVzZGLE9BQWYsRUFBd0I7QUFDcEIsZUFBTyw2QkFBQyxlQUFEO0FBQWlCLFVBQUEsSUFBSSxFQUFFLEtBQUtqRyxLQUFMLENBQVdpQyxJQUFsQztBQUF3QyxVQUFBLGVBQWUsRUFBRSxJQUF6RDtBQUErRCxVQUFBLE1BQU0sRUFBRSxLQUFLOUIsTUFBNUU7QUFBb0YsVUFBQSxNQUFNLEVBQUU7QUFDL0ZnQixZQUFBQSxTQUFTLEVBQUUsS0FBS2YsS0FBTCxDQUFXNEYsS0FBWCxDQUFpQlEsT0FBakIsRUFEb0Y7QUFFL0ZuRixZQUFBQSxTQUFTLEVBQUVlLElBQUksQ0FBQ3FFLFNBQUwsQ0FBZSxLQUFLckcsS0FBTCxDQUFXNEYsS0FBWCxDQUFpQlUsVUFBakIsRUFBZixFQUE4QyxJQUE5QyxFQUFvRCxJQUFwRCxDQUZvRjtBQUcvRnRGLFlBQUFBLFFBQVEsRUFBRSxLQUFLaEIsS0FBTCxDQUFXNEYsS0FBWCxDQUFpQlcsV0FBakI7QUFIcUY7QUFBNUYsVUFBUDtBQUtIOztBQUVELGFBQU87QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0g7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsd0JBQUQ7QUFBaUIsUUFBQSxTQUFTLEVBQUM7QUFBM0IsU0FDTXZFLElBQUksQ0FBQ3FFLFNBQUwsQ0FBZSxLQUFLckcsS0FBTCxDQUFXNEYsS0FBWCxDQUFpQkEsS0FBaEMsRUFBdUMsSUFBdkMsRUFBNkMsQ0FBN0MsQ0FETixDQURKLENBREcsRUFNSDtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFRLFFBQUEsT0FBTyxFQUFFLEtBQUs3RjtBQUF0QixTQUFnQyx5QkFBRyxNQUFILENBQWhDLENBREosRUFFSTtBQUFRLFFBQUEsT0FBTyxFQUFFLEtBQUswRjtBQUF0QixTQUFnQyx5QkFBRyxNQUFILENBQWhDLENBRkosQ0FORyxDQUFQO0FBV0g7O0FBRUQsUUFBSWUsSUFBSSxHQUFHLElBQVg7QUFFQSxVQUFNQyxPQUFPLEdBQUcsc0NBQWhCOztBQUNBLFFBQUksS0FBS3pHLEtBQUwsQ0FBV2UsU0FBWCxLQUF5QixJQUE3QixFQUFtQztBQUMvQnlGLE1BQUFBLElBQUksR0FBRyw2QkFBQyxZQUFEO0FBQWMsUUFBQSxLQUFLLEVBQUUsS0FBS3hHLEtBQUwsQ0FBVzhGLGNBQWhDO0FBQWdELFFBQUEsUUFBUSxFQUFFLEtBQUtKO0FBQS9ELFNBRUN4RSxNQUFNLENBQUN3RixJQUFQLENBQVksS0FBS3BCLGVBQWpCLEVBQWtDcUIsR0FBbEMsQ0FBdUNDLE1BQUQsSUFBWTtBQUM5QyxjQUFNQyxVQUFVLEdBQUcsS0FBS3ZCLGVBQUwsQ0FBcUJzQixNQUFyQixDQUFuQjtBQUNBLGNBQU1FLFNBQVMsR0FBRzVGLE1BQU0sQ0FBQ3dGLElBQVAsQ0FBWUcsVUFBWixDQUFsQjtBQUVBLFlBQUlFLFNBQUo7O0FBQ0EsWUFBSUQsU0FBUyxDQUFDckMsTUFBVixLQUFxQixDQUFyQixJQUEwQnFDLFNBQVMsQ0FBQyxDQUFELENBQVQsS0FBaUIsRUFBL0MsRUFBbUQ7QUFDL0NDLFVBQUFBLFNBQVMsR0FBRyxLQUFLZCxpQkFBTCxDQUF1QlksVUFBVSxDQUFDQyxTQUFTLENBQUMsQ0FBRCxDQUFWLENBQWpDLENBQVo7QUFDSCxTQUZELE1BRU87QUFDSEMsVUFBQUEsU0FBUyxHQUFHLEtBQUtmLGVBQUwsQ0FBcUJZLE1BQXJCLENBQVo7QUFDSDs7QUFFRCxlQUFPO0FBQVEsVUFBQSxTQUFTLEVBQUVILE9BQW5CO0FBQTRCLFVBQUEsR0FBRyxFQUFFRyxNQUFqQztBQUF5QyxVQUFBLE9BQU8sRUFBRUc7QUFBbEQsV0FDREgsTUFEQyxDQUFQO0FBR0gsT0FkRCxDQUZELENBQVA7QUFtQkgsS0FwQkQsTUFvQk87QUFDSCxZQUFNQyxVQUFVLEdBQUcsS0FBS3ZCLGVBQUwsQ0FBcUIsS0FBS3RGLEtBQUwsQ0FBV2UsU0FBaEMsQ0FBbkI7QUFFQXlGLE1BQUFBLElBQUksR0FBRyw2QkFBQyxZQUFEO0FBQWMsUUFBQSxLQUFLLEVBQUUsS0FBS3hHLEtBQUwsQ0FBVytGLGFBQWhDO0FBQStDLFFBQUEsUUFBUSxFQUFFLEtBQUtKO0FBQTlELFNBRUN6RSxNQUFNLENBQUN3RixJQUFQLENBQVlHLFVBQVosRUFBd0JGLEdBQXhCLENBQTZCM0YsUUFBRCxJQUFjO0FBQ3RDLGNBQU1tRCxFQUFFLEdBQUcwQyxVQUFVLENBQUM3RixRQUFELENBQXJCO0FBQ0EsZUFBTztBQUFRLFVBQUEsU0FBUyxFQUFFeUYsT0FBbkI7QUFBNEIsVUFBQSxHQUFHLEVBQUV6RixRQUFqQztBQUEyQyxVQUFBLE9BQU8sRUFBRSxLQUFLaUYsaUJBQUwsQ0FBdUI5QixFQUF2QjtBQUFwRCxXQUNEbkQsUUFEQyxDQUFQO0FBR0gsT0FMRCxDQUZELENBQVA7QUFVSDs7QUFFRCxXQUFPLDBDQUNIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNd0YsSUFETixDQURHLEVBSUg7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBUSxNQUFBLE9BQU8sRUFBRSxLQUFLekc7QUFBdEIsT0FBZ0MseUJBQUcsTUFBSCxDQUFoQyxDQURKLENBSkcsQ0FBUDtBQVFIOztBQXZJK0M7OzhCQUE5Q3NGLGlCLGVBR2lCO0FBQ2Z0RixFQUFBQSxNQUFNLEVBQUVzQyxtQkFBVUMsSUFBVixDQUFlQyxVQURSO0FBRWZWLEVBQUFBLElBQUksRUFBRVEsbUJBQVVHLFVBQVYsQ0FBcUJDLGlCQUFyQixFQUEyQkY7QUFGbEIsQzs4QkFIakI4QyxpQixpQkFRbUJ6Qyw0Qjs7QUFrSXpCLE1BQU1vRSxtQkFBTixTQUFrQ3ZILGVBQU1DLGFBQXhDLENBQXNEO0FBQ2xELFNBQU9vQixRQUFQLEdBQWtCO0FBQUUsV0FBTyx5QkFBRyxzQkFBSCxDQUFQO0FBQW9DOztBQVN4RG5CLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUVBLFNBQUtHLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlELElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNBLFNBQUsyRixNQUFMLEdBQWMsS0FBS0EsTUFBTCxDQUFZM0YsSUFBWixDQUFpQixJQUFqQixDQUFkO0FBQ0EsU0FBS0QsU0FBTCxHQUFpQixLQUFLQSxTQUFMLENBQWVDLElBQWYsQ0FBb0IsSUFBcEIsQ0FBakI7QUFDQSxTQUFLNEYsZ0JBQUwsR0FBd0IsS0FBS0EsZ0JBQUwsQ0FBc0I1RixJQUF0QixDQUEyQixJQUEzQixDQUF4QjtBQUVBLFNBQUtFLEtBQUwsR0FBYTtBQUNUOEMsTUFBQUEsaUJBQWlCLEVBQUUsS0FEVjtBQUVUOEMsTUFBQUEsS0FBSyxFQUFFLElBRkU7QUFHVEMsTUFBQUEsT0FBTyxFQUFFLEtBSEE7QUFLVEMsTUFBQUEsY0FBYyxFQUFFO0FBTFAsS0FBYjtBQU9IOztBQUVEbUIsRUFBQUEsT0FBTyxHQUFHO0FBQ04sUUFBSSxLQUFLakgsS0FBTCxDQUFXOEMsaUJBQWYsRUFBa0M7QUFDOUIsYUFBTyxLQUFLbEQsS0FBTCxDQUFXaUMsSUFBWCxDQUFnQnFGLFdBQXZCO0FBQ0g7O0FBQ0QsV0FBTyxLQUFLdkYsT0FBTCxDQUFhd0YsS0FBYixDQUFtQkQsV0FBMUI7QUFDSDs7QUFFRGpCLEVBQUFBLGlCQUFpQixDQUFDTCxLQUFELEVBQVE7QUFDckIsV0FBTyxNQUFNO0FBQ1QsV0FBSzFGLFFBQUwsQ0FBYztBQUFFMEYsUUFBQUE7QUFBRixPQUFkO0FBQ0gsS0FGRDtBQUdIOztBQUVEN0YsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLQyxLQUFMLENBQVc2RixPQUFmLEVBQXdCO0FBQ3BCLFdBQUszRixRQUFMLENBQWM7QUFBRTJGLFFBQUFBLE9BQU8sRUFBRTtBQUFYLE9BQWQ7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLN0YsS0FBTCxDQUFXNEYsS0FBZixFQUFzQjtBQUN6QixXQUFLMUYsUUFBTCxDQUFjO0FBQUUwRixRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQUFkO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsV0FBS2hHLEtBQUwsQ0FBV0csTUFBWDtBQUNIO0FBQ0o7O0FBRURGLEVBQUFBLFNBQVMsQ0FBQ00sQ0FBRCxFQUFJO0FBQ1QsU0FBS0QsUUFBTCxDQUFjO0FBQUMsT0FBQ0MsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEVBQVYsR0FBZUYsQ0FBQyxDQUFDQyxNQUFGLENBQVNFLElBQVQsS0FBa0IsVUFBbEIsR0FBK0JILENBQUMsQ0FBQ0MsTUFBRixDQUFTRyxPQUF4QyxHQUFrREosQ0FBQyxDQUFDQyxNQUFGLENBQVNJO0FBQTNFLEtBQWQ7QUFDSDs7QUFFRGlGLEVBQUFBLE1BQU0sR0FBRztBQUNMLFNBQUt2RixRQUFMLENBQWM7QUFBRTJGLE1BQUFBLE9BQU8sRUFBRTtBQUFYLEtBQWQ7QUFDSDs7QUFFREgsRUFBQUEsZ0JBQWdCLENBQUNJLGNBQUQsRUFBaUI7QUFDN0IsU0FBSzVGLFFBQUwsQ0FBYztBQUFFNEYsTUFBQUE7QUFBRixLQUFkO0FBQ0g7O0FBRUQzRCxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtuQyxLQUFMLENBQVc0RixLQUFmLEVBQXNCO0FBQ2xCLFVBQUksS0FBSzVGLEtBQUwsQ0FBVzZGLE9BQWYsRUFBd0I7QUFDcEIsZUFBTyw2QkFBQyxlQUFEO0FBQ0gsVUFBQSxJQUFJLEVBQUUsS0FBS2pHLEtBQUwsQ0FBV2lDLElBRGQ7QUFFSCxVQUFBLGlCQUFpQixFQUFFLEtBQUs3QixLQUFMLENBQVc4QyxpQkFGM0I7QUFHSCxVQUFBLE1BQU0sRUFBRSxLQUFLL0MsTUFIVjtBQUlILFVBQUEsTUFBTSxFQUFFO0FBQ0pnQixZQUFBQSxTQUFTLEVBQUUsS0FBS2YsS0FBTCxDQUFXNEYsS0FBWCxDQUFpQlEsT0FBakIsRUFEUDtBQUVKbkYsWUFBQUEsU0FBUyxFQUFFZSxJQUFJLENBQUNxRSxTQUFMLENBQWUsS0FBS3JHLEtBQUwsQ0FBVzRGLEtBQVgsQ0FBaUJVLFVBQWpCLEVBQWYsRUFBOEMsSUFBOUMsRUFBb0QsSUFBcEQ7QUFGUCxXQUpMO0FBT0EsVUFBQSxTQUFTLEVBQUU7QUFQWCxVQUFQO0FBUUg7O0FBRUQsYUFBTztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSDtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyx3QkFBRDtBQUFpQixRQUFBLFNBQVMsRUFBQztBQUEzQixTQUNNdEUsSUFBSSxDQUFDcUUsU0FBTCxDQUFlLEtBQUtyRyxLQUFMLENBQVc0RixLQUFYLENBQWlCQSxLQUFoQyxFQUF1QyxJQUF2QyxFQUE2QyxDQUE3QyxDQUROLENBREosQ0FERyxFQU1IO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQVEsUUFBQSxPQUFPLEVBQUUsS0FBSzdGO0FBQXRCLFNBQWdDLHlCQUFHLE1BQUgsQ0FBaEMsQ0FESixFQUVJO0FBQVEsUUFBQSxPQUFPLEVBQUUsS0FBSzBGO0FBQXRCLFNBQWdDLHlCQUFHLE1BQUgsQ0FBaEMsQ0FGSixDQU5HLENBQVA7QUFXSDs7QUFFRCxVQUFNMkIsSUFBSSxHQUFHLEVBQWI7QUFFQSxVQUFNWCxPQUFPLEdBQUcsc0NBQWhCO0FBRUEsVUFBTVksSUFBSSxHQUFHLEtBQUtKLE9BQUwsRUFBYjtBQUNBL0YsSUFBQUEsTUFBTSxDQUFDd0YsSUFBUCxDQUFZVyxJQUFaLEVBQWtCQyxPQUFsQixDQUEyQlYsTUFBRCxJQUFZO0FBQ2xDLFlBQU16QyxFQUFFLEdBQUdrRCxJQUFJLENBQUNULE1BQUQsQ0FBZjtBQUNBUSxNQUFBQSxJQUFJLENBQUNHLElBQUwsQ0FBVTtBQUFRLFFBQUEsU0FBUyxFQUFFZCxPQUFuQjtBQUE0QixRQUFBLEdBQUcsRUFBRUcsTUFBakM7QUFBeUMsUUFBQSxPQUFPLEVBQUUsS0FBS1gsaUJBQUwsQ0FBdUI5QixFQUF2QjtBQUFsRCxTQUNKeUMsTUFESSxDQUFWO0FBR0gsS0FMRDtBQU9BLFdBQU8sMENBQ0g7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsWUFBRDtBQUFjLE1BQUEsS0FBSyxFQUFFLEtBQUs1RyxLQUFMLENBQVc4RixjQUFoQztBQUFnRCxNQUFBLFFBQVEsRUFBRSxLQUFLSjtBQUEvRCxPQUNNMEIsSUFETixDQURKLENBREcsRUFNSDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFRLE1BQUEsT0FBTyxFQUFFLEtBQUtySDtBQUF0QixPQUFnQyx5QkFBRyxNQUFILENBQWhDLENBREosRUFFTSxDQUFDLEtBQUtDLEtBQUwsQ0FBV0MsT0FBWixJQUF1QjtBQUFLLE1BQUEsS0FBSyxFQUFFO0FBQUNtQyxRQUFBQSxLQUFLLEVBQUU7QUFBUjtBQUFaLE9BQ3JCO0FBQU8sTUFBQSxFQUFFLEVBQUMsbUJBQVY7QUFBOEIsTUFBQSxTQUFTLEVBQUMsc0NBQXhDO0FBQStFLE1BQUEsSUFBSSxFQUFDLFVBQXBGO0FBQStGLE1BQUEsUUFBUSxFQUFFLEtBQUt2QyxTQUE5RztBQUF5SCxNQUFBLE9BQU8sRUFBRSxLQUFLRyxLQUFMLENBQVc4QztBQUE3SSxNQURxQixFQUVyQjtBQUFPLE1BQUEsU0FBUyxFQUFDLHFCQUFqQjtBQUF1QyxxQkFBWSxjQUFuRDtBQUFrRSxvQkFBVyxXQUE3RTtBQUF5RixNQUFBLE9BQU8sRUFBQztBQUFqRyxNQUZxQixDQUY3QixDQU5HLENBQVA7QUFjSDs7QUFsSGlEOzs4QkFBaERrRSxtQixlQUdpQjtBQUNmakgsRUFBQUEsTUFBTSxFQUFFc0MsbUJBQVVDLElBQVYsQ0FBZUMsVUFEUjtBQUVmVixFQUFBQSxJQUFJLEVBQUVRLG1CQUFVRyxVQUFWLENBQXFCQyxpQkFBckIsRUFBMkJGO0FBRmxCLEM7OEJBSGpCeUUsbUIsaUJBUW1CcEUsNEI7O0FBNkd6QixNQUFNNEUsaUJBQU4sU0FBZ0MvSCxlQUFNQyxhQUF0QyxDQUFvRDtBQUNoRCxTQUFPb0IsUUFBUCxHQUFrQjtBQUFFLFdBQU8seUJBQUcsc0JBQUgsQ0FBUDtBQUFvQzs7QUFTeERuQixFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSxtREFnQlIyRCxLQUFELElBQVc7QUFDakIsV0FBS3JELFFBQUwsQ0FBYztBQUFFcUQsUUFBQUE7QUFBRixPQUFkO0FBQ0gsS0FsQmtCO0FBR2YsVUFBTTFCLElBQUksR0FBRyxLQUFLakMsS0FBTCxDQUFXaUMsSUFBeEI7QUFDQSxVQUFNNEYsT0FBTyxHQUFHLElBQUlDLEdBQUosRUFBaEI7QUFDQTdGLElBQUFBLElBQUksQ0FBQzBELFlBQUwsQ0FBa0JvQyxjQUFsQixDQUFpQyxlQUFqQyxFQUFrREwsT0FBbEQsQ0FBMERuRCxFQUFFLElBQUlzRCxPQUFPLENBQUNHLEdBQVIsQ0FBWXpELEVBQUUsQ0FBQzBELFNBQUgsR0FBZUMsS0FBZixDQUFxQixHQUFyQixFQUEwQixDQUExQixDQUFaLENBQWhFO0FBQ0EsU0FBS0wsT0FBTCxHQUFlTSxLQUFLLENBQUNDLElBQU4sQ0FBV1AsT0FBWCxFQUFvQmQsR0FBcEIsQ0FBd0JzQixDQUFDLElBQ3BDO0FBQVEsTUFBQSxHQUFHLEVBQUVBLENBQWI7QUFBZ0IsTUFBQSxTQUFTLEVBQUM7QUFBMUIsT0FDTUEsQ0FETixDQURXLENBQWY7QUFLQSxTQUFLakksS0FBTCxHQUFhO0FBQ1R1RCxNQUFBQSxLQUFLLEVBQUU7QUFERSxLQUFiO0FBR0g7O0FBTURwQixFQUFBQSxNQUFNLEdBQUc7QUFDTCxXQUFPLDBDQUNIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLFlBQUQ7QUFBYyxNQUFBLEtBQUssRUFBRSxLQUFLbkMsS0FBTCxDQUFXdUQsS0FBaEM7QUFBdUMsTUFBQSxRQUFRLEVBQUUsS0FBS3dCO0FBQXRELE9BQ00sS0FBSzBDLE9BRFgsQ0FESixDQURHLEVBTUg7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBUSxNQUFBLE9BQU8sRUFBRSxLQUFLN0gsS0FBTCxDQUFXRztBQUE1QixPQUFzQyx5QkFBRyxNQUFILENBQXRDLENBREosQ0FORyxDQUFQO0FBVUg7O0FBekMrQzs7OEJBQTlDeUgsaUIsZUFHaUI7QUFDZnpILEVBQUFBLE1BQU0sRUFBRXNDLG1CQUFVQyxJQUFWLENBQWVDLFVBRFI7QUFFZlYsRUFBQUEsSUFBSSxFQUFFUSxtQkFBVUcsVUFBVixDQUFxQkMsaUJBQXJCLEVBQTJCRjtBQUZsQixDOzhCQUhqQmlGLGlCLGlCQVFtQjVFLDRCO0FBb0N6QixNQUFNc0YsU0FBUyxHQUFHO0FBQ2QsR0FBQ0MsaUNBQUQsR0FBZ0IsUUFERjtBQUVkLEdBQUNDLG9DQUFELEdBQW1CLFdBRkw7QUFHZCxHQUFDQyxnQ0FBRCxHQUFlLE9BSEQ7QUFJZCxHQUFDQywrQkFBRCxHQUFjLE1BSkE7QUFLZCxHQUFDQyxrQ0FBRCxHQUFpQixTQUxIO0FBTWQsR0FBQ0Msb0NBQUQsR0FBbUI7QUFOTCxDQUFsQjs7QUFTQSxTQUFTQyxtQkFBVCxDQUE2QjtBQUFDQyxFQUFBQSxLQUFEO0FBQVFDLEVBQUFBO0FBQVIsQ0FBN0IsRUFBK0M7QUFDM0MsUUFBTSxHQUFHQyxXQUFILElBQWtCLHNCQUF4QjtBQUNBLFFBQU0sQ0FBQ0MsT0FBRCxFQUFVQyxpQkFBVixJQUErQixxQkFBU0gsT0FBTyxDQUFDRSxPQUFqQixDQUFyQztBQUVBOztBQUNBLHdDQUFnQkYsT0FBaEIsRUFBeUIsUUFBekIsRUFBbUNDLFdBQW5DO0FBRUE7O0FBQ0Esd0JBQVUsTUFBTTtBQUNaLFFBQUlELE9BQU8sQ0FBQ0UsT0FBUixJQUFtQixDQUF2QixFQUEwQjtBQUUxQjs7QUFDQSxVQUFNeEksRUFBRSxHQUFHMEksV0FBVyxDQUFDLE1BQU07QUFDMUJELE1BQUFBLGlCQUFpQixDQUFDSCxPQUFPLENBQUNFLE9BQVQsQ0FBakI7QUFDRixLQUZxQixFQUVuQixHQUZtQixDQUF0QjtBQUlBLFdBQU8sTUFBTTtBQUFFRyxNQUFBQSxhQUFhLENBQUMzSSxFQUFELENBQWI7QUFBb0IsS0FBbkM7QUFDSCxHQVRELEVBU0csQ0FBQ3NJLE9BQUQsQ0FUSDtBQVdBLFNBQVE7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0oseUNBQ0ksdURBREosRUFFSSx5Q0FBS0QsS0FBTCxDQUZKLEVBR0ksaURBSEosRUFJSSx5Q0FBS1IsU0FBUyxDQUFDUyxPQUFPLENBQUNNLEtBQVQsQ0FBVCxJQUE0Qk4sT0FBTyxDQUFDTSxLQUF6QyxDQUpKLEVBS0ksbURBTEosRUFNSSx5Q0FBS0MsSUFBSSxDQUFDQyxLQUFMLENBQVdOLE9BQU8sR0FBRyxJQUFyQixDQUFMLENBTkosRUFPSSxtREFQSixFQVFJLHlDQUFLRixPQUFPLENBQUNTLE9BQVIsSUFBbUJULE9BQU8sQ0FBQ1MsT0FBUixDQUFnQkMsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBeEIsQ0FSSixFQVNJLDREQVRKLEVBVUkseUNBQUtWLE9BQU8sQ0FBQ1csZ0JBQWIsQ0FWSixFQVdJLHVEQVhKLEVBWUkseUNBQUt0SCxJQUFJLENBQUNxRSxTQUFMLENBQWVzQyxPQUFPLENBQUNZLFdBQXZCLENBQUwsQ0FaSixDQURJLENBQVI7QUFnQkg7O0FBRUQsTUFBTUMsb0JBQU4sU0FBbUMvSixlQUFNZ0ssU0FBekMsQ0FBbUQ7QUFBQTtBQUFBO0FBQUEsd0RBUWhDLE1BQU07QUFDakIsV0FBS0MsV0FBTDtBQUNILEtBVjhDO0FBQUE7O0FBQy9DLFNBQU81SSxRQUFQLEdBQWtCO0FBQ2QsV0FBTyx5QkFBRyx1QkFBSCxDQUFQO0FBQ0g7QUFFRDs7O0FBT0E2SSxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixVQUFNakksR0FBRyxHQUFHLEtBQUtDLE9BQWpCO0FBQ0FELElBQUFBLEdBQUcsQ0FBQ2tJLEVBQUosQ0FBTyw2QkFBUCxFQUFzQyxLQUFLQyxZQUEzQztBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixVQUFNcEksR0FBRyxHQUFHLEtBQUtDLE9BQWpCO0FBQ0FELElBQUFBLEdBQUcsQ0FBQ3FJLEdBQUosQ0FBUSw2QkFBUixFQUF1QyxLQUFLRixZQUE1QztBQUNIOztBQUVEMUgsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTVQsR0FBRyxHQUFHLEtBQUtDLE9BQWpCO0FBQ0EsVUFBTUUsSUFBSSxHQUFHLEtBQUtqQyxLQUFMLENBQVdpQyxJQUF4QjtBQUNBLFVBQU1tSSxhQUFhLEdBQUd0SSxHQUFHLENBQUN1SSxPQUFKLENBQVlDLDJCQUFsQztBQUNBLFVBQU1DLGNBQWMsR0FBRyxDQUFDSCxhQUFhLENBQUNJLGlCQUFkLElBQW1DLElBQUlDLEdBQUosRUFBcEMsRUFBK0NDLEdBQS9DLENBQW1EekksSUFBSSxDQUFDQyxNQUF4RCxLQUFtRSxJQUFJdUksR0FBSixFQUExRjtBQUVBLFdBQVEsMENBQ0o7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0t0QyxLQUFLLENBQUNDLElBQU4sQ0FBV21DLGNBQWMsQ0FBQ0ksT0FBZixFQUFYLEVBQXFDQyxPQUFyQyxHQUErQzdELEdBQS9DLENBQW1ELENBQUMsQ0FBQytCLEtBQUQsRUFBUUMsT0FBUixDQUFELEtBQ2hELDZCQUFDLG1CQUFEO0FBQXFCLE1BQUEsS0FBSyxFQUFFRCxLQUE1QjtBQUFtQyxNQUFBLE9BQU8sRUFBRUMsT0FBNUM7QUFBcUQsTUFBQSxHQUFHLEVBQUVEO0FBQTFELE1BREgsQ0FETCxDQURJLENBQVI7QUFPSDs7QUFuQzhDOzs4QkFBN0NjLG9CLGlCQU1tQjVHLDRCO0FBZ0N6QixNQUFNNkgsT0FBTyxHQUFHLENBQ1o1SixlQURZLEVBRVp3RSxpQkFGWSxFQUdaeEMsZUFIWSxFQUlabUUsbUJBSlksRUFLWlEsaUJBTFksRUFNWmdDLG9CQU5ZLENBQWhCOztBQVNlLE1BQU1rQixjQUFOLFNBQTZCakwsZUFBTUMsYUFBbkMsQ0FBaUQ7QUFNNURDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUtHLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlELElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNBLFNBQUs2SyxRQUFMLEdBQWdCLEtBQUtBLFFBQUwsQ0FBYzdLLElBQWQsQ0FBbUIsSUFBbkIsQ0FBaEI7QUFFQSxTQUFLRSxLQUFMLEdBQWE7QUFDVDRLLE1BQUFBLElBQUksRUFBRTtBQURHLEtBQWI7QUFHSDs7QUFFRGQsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsU0FBS2UsVUFBTCxHQUFrQixJQUFsQjtBQUNIOztBQUVEQyxFQUFBQSxRQUFRLENBQUNGLElBQUQsRUFBTztBQUNYLFdBQU8sTUFBTTtBQUNULFdBQUsxSyxRQUFMLENBQWM7QUFBRTBLLFFBQUFBO0FBQUYsT0FBZDtBQUNILEtBRkQ7QUFHSDs7QUFFRDdLLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUksS0FBS2dMLFFBQVQsRUFBbUI7QUFDZixXQUFLN0ssUUFBTCxDQUFjO0FBQUUwSyxRQUFBQSxJQUFJLEVBQUUsS0FBS0c7QUFBYixPQUFkO0FBQ0EsV0FBS0EsUUFBTCxHQUFnQixJQUFoQjtBQUNILEtBSEQsTUFHTztBQUNILFdBQUs3SyxRQUFMLENBQWM7QUFBRTBLLFFBQUFBLElBQUksRUFBRTtBQUFSLE9BQWQ7QUFDSDtBQUNKOztBQUVERCxFQUFBQSxRQUFRLEdBQUc7QUFDUCxTQUFLL0ssS0FBTCxDQUFXb0wsVUFBWCxDQUFzQixLQUF0QjtBQUNIOztBQUVEN0ksRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSThJLElBQUo7O0FBRUEsUUFBSSxLQUFLakwsS0FBTCxDQUFXNEssSUFBZixFQUFxQjtBQUNqQkssTUFBQUEsSUFBSSxHQUFHLDZCQUFDLDRCQUFELENBQXFCLFFBQXJCLFFBQ0R2SixHQUFELElBQVMsNkJBQUMsY0FBRCxDQUFPLFFBQVAsUUFDTjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBMEMsS0FBSzFCLEtBQUwsQ0FBVzRLLElBQVgsQ0FBZ0I5SixRQUFoQixFQUExQyxDQURNLEVBRU47QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLHNCQUFvRCxLQUFLbEIsS0FBTCxDQUFXa0MsTUFBL0QsQ0FGTSxFQUdOO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixRQUhNLEVBSU4sa0NBQU0sS0FBTixDQUFZLElBQVo7QUFBaUIsUUFBQSxNQUFNLEVBQUUsS0FBSy9CLE1BQTlCO0FBQXNDLFFBQUEsSUFBSSxFQUFFMkIsR0FBRyxDQUFDd0osT0FBSixDQUFZLEtBQUt0TCxLQUFMLENBQVdrQyxNQUF2QjtBQUE1QyxRQUpNLENBRFAsQ0FBUDtBQVFILEtBVEQsTUFTTztBQUNILFlBQU0yRSxPQUFPLEdBQUcsc0NBQWhCO0FBQ0F3RSxNQUFBQSxJQUFJLEdBQUcsNkJBQUMsY0FBRCxDQUFPLFFBQVAsUUFDSCwwQ0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBMEMseUJBQUcsU0FBSCxDQUExQyxDQURKLEVBRUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLHNCQUFvRCxLQUFLckwsS0FBTCxDQUFXa0MsTUFBL0QsQ0FGSixFQUdJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixRQUhKLEVBS0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ00ySSxPQUFPLENBQUM5RCxHQUFSLENBQWF3RSxLQUFELElBQVc7QUFDckIsY0FBTXZLLEtBQUssR0FBR3VLLEtBQUssQ0FBQ3JLLFFBQU4sRUFBZDs7QUFDQSxjQUFNc0ssT0FBTyxHQUFHLEtBQUtOLFFBQUwsQ0FBY0ssS0FBZCxDQUFoQjs7QUFDQSxlQUFPO0FBQVEsVUFBQSxTQUFTLEVBQUUxRSxPQUFuQjtBQUE0QixVQUFBLEdBQUcsRUFBRTdGLEtBQWpDO0FBQXdDLFVBQUEsT0FBTyxFQUFFd0s7QUFBakQsV0FBNER4SyxLQUE1RCxDQUFQO0FBQ0gsT0FKQyxDQUROLENBTEosQ0FERyxFQWNIO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQVEsUUFBQSxPQUFPLEVBQUUsS0FBSytKO0FBQXRCLFNBQWtDLHlCQUFHLFFBQUgsQ0FBbEMsQ0FESixDQWRHLENBQVA7QUFrQkg7O0FBRUQsVUFBTVUsVUFBVSxHQUFHeEcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLFdBQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsU0FBUyxFQUFDLG1CQUF0QjtBQUEwQyxNQUFBLFVBQVUsRUFBRSxLQUFLbEYsS0FBTCxDQUFXb0wsVUFBakU7QUFBNkUsTUFBQSxLQUFLLEVBQUUseUJBQUcsaUJBQUg7QUFBcEYsT0FDTUMsSUFETixDQURKO0FBS0g7O0FBL0UyRDs7OzhCQUEzQ1AsYyxlQUNFO0FBQ2Y1SSxFQUFBQSxNQUFNLEVBQUVPLG1CQUFVK0MsTUFBVixDQUFpQjdDLFVBRFY7QUFFZnlJLEVBQUFBLFVBQVUsRUFBRTNJLG1CQUFVQyxJQUFWLENBQWVDO0FBRlosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCwge3VzZVN0YXRlLCB1c2VFZmZlY3R9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IFN5bnRheEhpZ2hsaWdodCBmcm9tICcuLi9lbGVtZW50cy9TeW50YXhIaWdobGlnaHQnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7IFJvb20gfSBmcm9tIFwibWF0cml4LWpzLXNka1wiO1xyXG5pbXBvcnQgRmllbGQgZnJvbSBcIi4uL2VsZW1lbnRzL0ZpZWxkXCI7XHJcbmltcG9ydCBNYXRyaXhDbGllbnRDb250ZXh0IGZyb20gXCIuLi8uLi8uLi9jb250ZXh0cy9NYXRyaXhDbGllbnRDb250ZXh0XCI7XHJcbmltcG9ydCB7dXNlRXZlbnRFbWl0dGVyfSBmcm9tIFwiLi4vLi4vLi4vaG9va3MvdXNlRXZlbnRFbWl0dGVyXCI7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgUEhBU0VfVU5TRU5ULFxyXG4gICAgUEhBU0VfUkVRVUVTVEVELFxyXG4gICAgUEhBU0VfUkVBRFksXHJcbiAgICBQSEFTRV9ET05FLFxyXG4gICAgUEhBU0VfU1RBUlRFRCxcclxuICAgIFBIQVNFX0NBTkNFTExFRCxcclxufSBmcm9tIFwibWF0cml4LWpzLXNkay9zcmMvY3J5cHRvL3ZlcmlmaWNhdGlvbi9yZXF1ZXN0L1ZlcmlmaWNhdGlvblJlcXVlc3RcIjtcclxuXHJcbmNsYXNzIEdlbmVyaWNFZGl0b3IgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIC8vIHN0YXRpYyBwcm9wVHlwZXMgPSB7b25CYWNrOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLl9vbkNoYW5nZSA9IHRoaXMuX29uQ2hhbmdlLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vbkJhY2sgPSB0aGlzLm9uQmFjay5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQmFjaygpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiBudWxsIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25CYWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbkNoYW5nZShlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7W2UudGFyZ2V0LmlkXTogZS50YXJnZXQudHlwZSA9PT0gJ2NoZWNrYm94JyA/IGUudGFyZ2V0LmNoZWNrZWQgOiBlLnRhcmdldC52YWx1ZX0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9idXR0b25zKCkge1xyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19idXR0b25zXCI+XHJcbiAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy5vbkJhY2t9PnsgX3QoJ0JhY2snKSB9PC9idXR0b24+XHJcbiAgICAgICAgICAgIHsgIXRoaXMuc3RhdGUubWVzc2FnZSAmJiA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMuX3NlbmR9PnsgX3QoJ1NlbmQnKSB9PC9idXR0b24+IH1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcblxyXG4gICAgdGV4dElucHV0KGlkLCBsYWJlbCkge1xyXG4gICAgICAgIHJldHVybiA8RmllbGQgaWQ9e2lkfSBsYWJlbD17bGFiZWx9IHNpemU9XCI0MlwiIGF1dG9Gb2N1cz17dHJ1ZX0gdHlwZT1cInRleHRcIiBhdXRvQ29tcGxldGU9XCJvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZVtpZF19IG9uQ2hhbmdlPXt0aGlzLl9vbkNoYW5nZX0gLz47XHJcbiAgICB9XHJcbn1cclxuXHJcbmNsYXNzIFNlbmRDdXN0b21FdmVudCBleHRlbmRzIEdlbmVyaWNFZGl0b3Ige1xyXG4gICAgc3RhdGljIGdldExhYmVsKCkgeyByZXR1cm4gX3QoJ1NlbmQgQ3VzdG9tIEV2ZW50Jyk7IH1cclxuXHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG9uQmFjazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICByb29tOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihSb29tKS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGZvcmNlU3RhdGVFdmVudDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgaW5wdXRzOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgY29udGV4dFR5cGUgPSBNYXRyaXhDbGllbnRDb250ZXh0O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICAgIHRoaXMuX3NlbmQgPSB0aGlzLl9zZW5kLmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHtldmVudFR5cGUsIHN0YXRlS2V5LCBldkNvbnRlbnR9ID0gT2JqZWN0LmFzc2lnbih7XHJcbiAgICAgICAgICAgIGV2ZW50VHlwZTogJycsXHJcbiAgICAgICAgICAgIHN0YXRlS2V5OiAnJyxcclxuICAgICAgICAgICAgZXZDb250ZW50OiAne1xcblxcbn0nLFxyXG4gICAgICAgIH0sIHRoaXMucHJvcHMuaW5wdXRzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaXNTdGF0ZUV2ZW50OiBCb29sZWFuKHRoaXMucHJvcHMuZm9yY2VTdGF0ZUV2ZW50KSxcclxuXHJcbiAgICAgICAgICAgIGV2ZW50VHlwZSxcclxuICAgICAgICAgICAgc3RhdGVLZXksXHJcbiAgICAgICAgICAgIGV2Q29udGVudCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHNlbmQoY29udGVudCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IHRoaXMuY29udGV4dDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pc1N0YXRlRXZlbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNsaS5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb20ucm9vbUlkLCB0aGlzLnN0YXRlLmV2ZW50VHlwZSwgY29udGVudCwgdGhpcy5zdGF0ZS5zdGF0ZUtleSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNsaS5zZW5kRXZlbnQodGhpcy5wcm9wcy5yb29tLnJvb21JZCwgdGhpcy5zdGF0ZS5ldmVudFR5cGUsIGNvbnRlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfc2VuZCgpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5ldmVudFR5cGUgPT09ICcnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiBfdCgnWW91IG11c3Qgc3BlY2lmeSBhbiBldmVudCB0eXBlIScpIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgbWVzc2FnZTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBjb250ZW50ID0gSlNPTi5wYXJzZSh0aGlzLnN0YXRlLmV2Q29udGVudCk7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VuZChjb250ZW50KTtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IF90KCdFdmVudCBzZW50IScpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IF90KCdGYWlsZWQgdG8gc2VuZCBjdXN0b20gZXZlbnQuJykgKyAnICgnICsgZS50b1N0cmluZygpICsgJyknO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUubWVzc2FnZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUubWVzc2FnZSB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIHsgdGhpcy5fYnV0dG9ucygpIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EZXZUb29sc19ldmVudFR5cGVTdGF0ZUtleUdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnRleHRJbnB1dCgnZXZlbnRUeXBlJywgX3QoJ0V2ZW50IFR5cGUnKSkgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5zdGF0ZS5pc1N0YXRlRXZlbnQgJiYgdGhpcy50ZXh0SW5wdXQoJ3N0YXRlS2V5JywgX3QoJ1N0YXRlIEtleScpKSB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8YnIgLz5cclxuXHJcbiAgICAgICAgICAgICAgICA8RmllbGQgaWQ9XCJldkNvbnRlbnRcIiBsYWJlbD17X3QoXCJFdmVudCBDb250ZW50XCIpfSB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX3RleHRhcmVhXCJcclxuICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJvZmZcIiB2YWx1ZT17dGhpcy5zdGF0ZS5ldkNvbnRlbnR9IG9uQ2hhbmdlPXt0aGlzLl9vbkNoYW5nZX0gZWxlbWVudD1cInRleHRhcmVhXCIgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy5vbkJhY2t9PnsgX3QoJ0JhY2snKSB9PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICB7ICF0aGlzLnN0YXRlLm1lc3NhZ2UgJiYgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9zZW5kfT57IF90KCdTZW5kJykgfTwvYnV0dG9uPiB9XHJcbiAgICAgICAgICAgICAgICB7ICF0aGlzLnN0YXRlLm1lc3NhZ2UgJiYgIXRoaXMucHJvcHMuZm9yY2VTdGF0ZUV2ZW50ICYmIDxkaXYgc3R5bGU9e3tmbG9hdDogXCJyaWdodFwifX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGlkPVwiaXNTdGF0ZUV2ZW50XCIgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfdGdsIG14X0RldlRvb2xzX3RnbC1mbGlwXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfSBjaGVja2VkPXt0aGlzLnN0YXRlLmlzU3RhdGVFdmVudH0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfdGdsLWJ0blwiIGRhdGEtdGctb2ZmPVwiRXZlbnRcIiBkYXRhLXRnLW9uPVwiU3RhdGUgRXZlbnRcIiBodG1sRm9yPVwiaXNTdGF0ZUV2ZW50XCIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgU2VuZEFjY291bnREYXRhIGV4dGVuZHMgR2VuZXJpY0VkaXRvciB7XHJcbiAgICBzdGF0aWMgZ2V0TGFiZWwoKSB7IHJldHVybiBfdCgnU2VuZCBBY2NvdW50IERhdGEnKTsgfVxyXG5cclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLmluc3RhbmNlT2YoUm9vbSkuaXNSZXF1aXJlZCxcclxuICAgICAgICBpc1Jvb21BY2NvdW50RGF0YTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgZm9yY2VNb2RlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBpbnB1dHM6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBjb250ZXh0VHlwZSA9IE1hdHJpeENsaWVudENvbnRleHQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5fc2VuZCA9IHRoaXMuX3NlbmQuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgY29uc3Qge2V2ZW50VHlwZSwgZXZDb250ZW50fSA9IE9iamVjdC5hc3NpZ24oe1xyXG4gICAgICAgICAgICBldmVudFR5cGU6ICcnLFxyXG4gICAgICAgICAgICBldkNvbnRlbnQ6ICd7XFxuXFxufScsXHJcbiAgICAgICAgfSwgdGhpcy5wcm9wcy5pbnB1dHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBpc1Jvb21BY2NvdW50RGF0YTogQm9vbGVhbih0aGlzLnByb3BzLmlzUm9vbUFjY291bnREYXRhKSxcclxuXHJcbiAgICAgICAgICAgIGV2ZW50VHlwZSxcclxuICAgICAgICAgICAgZXZDb250ZW50LFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgc2VuZChjb250ZW50KSB7XHJcbiAgICAgICAgY29uc3QgY2xpID0gdGhpcy5jb250ZXh0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmlzUm9vbUFjY291bnREYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBjbGkuc2V0Um9vbUFjY291bnREYXRhKHRoaXMucHJvcHMucm9vbS5yb29tSWQsIHRoaXMuc3RhdGUuZXZlbnRUeXBlLCBjb250ZW50KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNsaS5zZXRBY2NvdW50RGF0YSh0aGlzLnN0YXRlLmV2ZW50VHlwZSwgY29udGVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3NlbmQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXZlbnRUeXBlID09PSAnJykge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogX3QoJ1lvdSBtdXN0IHNwZWNpZnkgYW4gZXZlbnQgdHlwZSEnKSB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG1lc3NhZ2U7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudCA9IEpTT04ucGFyc2UodGhpcy5zdGF0ZS5ldkNvbnRlbnQpO1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmQoY29udGVudCk7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UgPSBfdCgnRXZlbnQgc2VudCEnKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UgPSBfdCgnRmFpbGVkIHRvIHNlbmQgY3VzdG9tIGV2ZW50LicpICsgJyAoJyArIGUudG9TdHJpbmcoKSArICcpJztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2UgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnN0YXRlLm1lc3NhZ2UgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuX2J1dHRvbnMoKSB9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIHsgdGhpcy50ZXh0SW5wdXQoJ2V2ZW50VHlwZScsIF90KCdFdmVudCBUeXBlJykpIH1cclxuICAgICAgICAgICAgICAgIDxiciAvPlxyXG5cclxuICAgICAgICAgICAgICAgIDxGaWVsZCBpZD1cImV2Q29udGVudFwiIGxhYmVsPXtfdChcIkV2ZW50IENvbnRlbnRcIil9IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfdGV4dGFyZWFcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIGF1dG9Db21wbGV0ZT1cIm9mZlwiIHZhbHVlPXt0aGlzLnN0YXRlLmV2Q29udGVudH0gb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfSBlbGVtZW50PVwidGV4dGFyZWFcIiAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uQmFja30+eyBfdCgnQmFjaycpIH08L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIHsgIXRoaXMuc3RhdGUubWVzc2FnZSAmJiA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMuX3NlbmR9PnsgX3QoJ1NlbmQnKSB9PC9idXR0b24+IH1cclxuICAgICAgICAgICAgICAgIHsgIXRoaXMuc3RhdGUubWVzc2FnZSAmJiA8ZGl2IHN0eWxlPXt7ZmxvYXQ6IFwicmlnaHRcIn19PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImlzUm9vbUFjY291bnREYXRhXCIgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfdGdsIG14X0RldlRvb2xzX3RnbC1mbGlwXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfSBjaGVja2VkPXt0aGlzLnN0YXRlLmlzUm9vbUFjY291bnREYXRhfSBkaXNhYmxlZD17dGhpcy5wcm9wcy5mb3JjZU1vZGV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX3RnbC1idG5cIiBkYXRhLXRnLW9mZj1cIkFjY291bnQgRGF0YVwiIGRhdGEtdGctb249XCJSb29tIERhdGFcIiBodG1sRm9yPVwiaXNSb29tQWNjb3VudERhdGFcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+IH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBJTklUSUFMX0xPQURfVElMRVMgPSAyMDtcclxuY29uc3QgTE9BRF9USUxFU19TVEVQX1NJWkUgPSA1MDtcclxuXHJcbmNsYXNzIEZpbHRlcmVkTGlzdCBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBjaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcclxuICAgICAgICBxdWVyeTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBmaWx0ZXJDaGlsZHJlbihjaGlsZHJlbiwgcXVlcnkpIHtcclxuICAgICAgICBpZiAoIXF1ZXJ5KSByZXR1cm4gY2hpbGRyZW47XHJcbiAgICAgICAgY29uc3QgbGNRdWVyeSA9IHF1ZXJ5LnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgcmV0dXJuIGNoaWxkcmVuLmZpbHRlcigoY2hpbGQpID0+IGNoaWxkLmtleS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGxjUXVlcnkpKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgZmlsdGVyZWRDaGlsZHJlbjogRmlsdGVyZWRMaXN0LmZpbHRlckNoaWxkcmVuKHRoaXMucHJvcHMuY2hpbGRyZW4sIHRoaXMucHJvcHMucXVlcnkpLFxyXG4gICAgICAgICAgICB0cnVuY2F0ZUF0OiBJTklUSUFMX0xPQURfVElMRVMsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSB3aXRoIGFwcHJvcHJpYXRlIGxpZmVjeWNsZSBldmVudFxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzKSB7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgY2FtZWxjYXNlXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuY2hpbGRyZW4gPT09IG5leHRQcm9wcy5jaGlsZHJlbiAmJiB0aGlzLnByb3BzLnF1ZXJ5ID09PSBuZXh0UHJvcHMucXVlcnkpIHJldHVybjtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZmlsdGVyZWRDaGlsZHJlbjogRmlsdGVyZWRMaXN0LmZpbHRlckNoaWxkcmVuKG5leHRQcm9wcy5jaGlsZHJlbiwgbmV4dFByb3BzLnF1ZXJ5KSxcclxuICAgICAgICAgICAgdHJ1bmNhdGVBdDogSU5JVElBTF9MT0FEX1RJTEVTLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dBbGwgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHRydW5jYXRlQXQ6IHRoaXMuc3RhdGUudHJ1bmNhdGVBdCArIExPQURfVElMRVNfU1RFUF9TSVpFLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBjcmVhdGVPdmVyZmxvd0VsZW1lbnQgPSAob3ZlcmZsb3dDb3VudDogbnVtYmVyLCB0b3RhbENvdW50OiBudW1iZXIpID0+IHtcclxuICAgICAgICByZXR1cm4gPGJ1dHRvbiBjbGFzc05hbWU9XCJteF9EZXZUb29sc19Sb29tU3RhdGVFeHBsb3Jlcl9idXR0b25cIiBvbkNsaWNrPXt0aGlzLnNob3dBbGx9PlxyXG4gICAgICAgICAgICB7IF90KFwiYW5kICUoY291bnQpcyBvdGhlcnMuLi5cIiwgeyBjb3VudDogb3ZlcmZsb3dDb3VudCB9KSB9XHJcbiAgICAgICAgPC9idXR0b24+O1xyXG4gICAgfTtcclxuXHJcbiAgICBvblF1ZXJ5ID0gKGV2KSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25DaGFuZ2UpIHRoaXMucHJvcHMub25DaGFuZ2UoZXYudGFyZ2V0LnZhbHVlKTtcclxuICAgIH07XHJcblxyXG4gICAgZ2V0Q2hpbGRyZW4gPSAoc3RhcnQ6IG51bWJlciwgZW5kOiBudW1iZXIpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5maWx0ZXJlZENoaWxkcmVuLnNsaWNlKHN0YXJ0LCBlbmQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBnZXRDaGlsZENvdW50ID0gKCk6IG51bWJlciA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuZmlsdGVyZWRDaGlsZHJlbi5sZW5ndGg7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBUcnVuY2F0ZWRMaXN0ID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlRydW5jYXRlZExpc3RcIik7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxGaWVsZCBsYWJlbD17X3QoJ0ZpbHRlciByZXN1bHRzJyl9IGF1dG9Gb2N1cz17dHJ1ZX0gc2l6ZT17NjR9XHJcbiAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiIGF1dG9Db21wbGV0ZT1cIm9mZlwiIHZhbHVlPXt0aGlzLnByb3BzLnF1ZXJ5fSBvbkNoYW5nZT17dGhpcy5vblF1ZXJ5fVxyXG4gICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfVGV4dElucHV0RGlhbG9nX2lucHV0IG14X0RldlRvb2xzX1Jvb21TdGF0ZUV4cGxvcmVyX3F1ZXJ5XCJcclxuICAgICAgICAgICAgICAgICAgIC8vIGZvcmNlIHJlLXJlbmRlciBzbyB0aGF0IGF1dG9Gb2N1cyBpcyBhcHBsaWVkIHdoZW4gdGhpcyBjb21wb25lbnQgaXMgcmUtdXNlZFxyXG4gICAgICAgICAgICAgICAgICAga2V5PXt0aGlzLnByb3BzLmNoaWxkcmVuWzBdID8gdGhpcy5wcm9wcy5jaGlsZHJlblswXS5rZXkgOiAnJ30gLz5cclxuXHJcbiAgICAgICAgICAgIDxUcnVuY2F0ZWRMaXN0IGdldENoaWxkcmVuPXt0aGlzLmdldENoaWxkcmVufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBnZXRDaGlsZENvdW50PXt0aGlzLmdldENoaWxkQ291bnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydW5jYXRlQXQ9e3RoaXMuc3RhdGUudHJ1bmNhdGVBdH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlT3ZlcmZsb3dFbGVtZW50PXt0aGlzLmNyZWF0ZU92ZXJmbG93RWxlbWVudH0gLz5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuXHJcbmNsYXNzIFJvb21TdGF0ZUV4cGxvcmVyIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgZ2V0TGFiZWwoKSB7IHJldHVybiBfdCgnRXhwbG9yZSBSb29tIFN0YXRlJyk7IH1cclxuXHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG9uQmFjazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICByb29tOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihSb29tKS5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgY29udGV4dFR5cGUgPSBNYXRyaXhDbGllbnRDb250ZXh0O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnJvb21TdGF0ZUV2ZW50cyA9IHRoaXMucHJvcHMucm9vbS5jdXJyZW50U3RhdGUuZXZlbnRzO1xyXG5cclxuICAgICAgICB0aGlzLm9uQmFjayA9IHRoaXMub25CYWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5lZGl0RXYgPSB0aGlzLmVkaXRFdi5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25RdWVyeUV2ZW50VHlwZSA9IHRoaXMub25RdWVyeUV2ZW50VHlwZS5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25RdWVyeVN0YXRlS2V5ID0gdGhpcy5vblF1ZXJ5U3RhdGVLZXkuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgZXZlbnRUeXBlOiBudWxsLFxyXG4gICAgICAgICAgICBldmVudDogbnVsbCxcclxuICAgICAgICAgICAgZWRpdGluZzogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICBxdWVyeUV2ZW50VHlwZTogJycsXHJcbiAgICAgICAgICAgIHF1ZXJ5U3RhdGVLZXk6ICcnLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgYnJvd3NlRXZlbnRUeXBlKGV2ZW50VHlwZSkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBldmVudFR5cGUgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBvblZpZXdTb3VyY2VDbGljayhldmVudCkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBldmVudCB9KTtcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIG9uQmFjaygpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lZGl0aW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0aW5nOiBmYWxzZSB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuZXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV2ZW50OiBudWxsIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5ldmVudFR5cGUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV2ZW50VHlwZTogbnVsbCB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uQmFjaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0RXYoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXRpbmc6IHRydWUgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25RdWVyeUV2ZW50VHlwZShmaWx0ZXJFdmVudFR5cGUpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcXVlcnlFdmVudFR5cGU6IGZpbHRlckV2ZW50VHlwZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvblF1ZXJ5U3RhdGVLZXkoZmlsdGVyU3RhdGVLZXkpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcXVlcnlTdGF0ZUtleTogZmlsdGVyU3RhdGVLZXkgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmV2ZW50KSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmVkaXRpbmcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiA8U2VuZEN1c3RvbUV2ZW50IHJvb209e3RoaXMucHJvcHMucm9vbX0gZm9yY2VTdGF0ZUV2ZW50PXt0cnVlfSBvbkJhY2s9e3RoaXMub25CYWNrfSBpbnB1dHM9e3tcclxuICAgICAgICAgICAgICAgICAgICBldmVudFR5cGU6IHRoaXMuc3RhdGUuZXZlbnQuZ2V0VHlwZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGV2Q29udGVudDogSlNPTi5zdHJpbmdpZnkodGhpcy5zdGF0ZS5ldmVudC5nZXRDb250ZW50KCksIG51bGwsICdcXHQnKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0ZUtleTogdGhpcy5zdGF0ZS5ldmVudC5nZXRTdGF0ZUtleSgpLFxyXG4gICAgICAgICAgICAgICAgfX0gLz47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZpZXdTb3VyY2VcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8U3ludGF4SGlnaGxpZ2h0IGNsYXNzTmFtZT1cImpzb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBKU09OLnN0cmluZ2lmeSh0aGlzLnN0YXRlLmV2ZW50LmV2ZW50LCBudWxsLCAyKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9TeW50YXhIaWdobGlnaHQ+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMub25CYWNrfT57IF90KCdCYWNrJykgfTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy5lZGl0RXZ9PnsgX3QoJ0VkaXQnKSB9PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGxpc3QgPSBudWxsO1xyXG5cclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gJ214X0RldlRvb2xzX1Jvb21TdGF0ZUV4cGxvcmVyX2J1dHRvbic7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXZlbnRUeXBlID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGxpc3QgPSA8RmlsdGVyZWRMaXN0IHF1ZXJ5PXt0aGlzLnN0YXRlLnF1ZXJ5RXZlbnRUeXBlfSBvbkNoYW5nZT17dGhpcy5vblF1ZXJ5RXZlbnRUeXBlfT5cclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyh0aGlzLnJvb21TdGF0ZUV2ZW50cykubWFwKChldlR5cGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdGVHcm91cCA9IHRoaXMucm9vbVN0YXRlRXZlbnRzW2V2VHlwZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHN0YXRlS2V5cyA9IE9iamVjdC5rZXlzKHN0YXRlR3JvdXApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG9uQ2xpY2tGbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXRlS2V5cy5sZW5ndGggPT09IDEgJiYgc3RhdGVLZXlzWzBdID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGlja0ZuID0gdGhpcy5vblZpZXdTb3VyY2VDbGljayhzdGF0ZUdyb3VwW3N0YXRlS2V5c1swXV0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGlja0ZuID0gdGhpcy5icm93c2VFdmVudFR5cGUoZXZUeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxidXR0b24gY2xhc3NOYW1lPXtjbGFzc2VzfSBrZXk9e2V2VHlwZX0gb25DbGljaz17b25DbGlja0ZufT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgZXZUeXBlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+O1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDwvRmlsdGVyZWRMaXN0PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0ZUdyb3VwID0gdGhpcy5yb29tU3RhdGVFdmVudHNbdGhpcy5zdGF0ZS5ldmVudFR5cGVdO1xyXG5cclxuICAgICAgICAgICAgbGlzdCA9IDxGaWx0ZXJlZExpc3QgcXVlcnk9e3RoaXMuc3RhdGUucXVlcnlTdGF0ZUtleX0gb25DaGFuZ2U9e3RoaXMub25RdWVyeVN0YXRlS2V5fT5cclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhzdGF0ZUdyb3VwKS5tYXAoKHN0YXRlS2V5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ID0gc3RhdGVHcm91cFtzdGF0ZUtleV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8YnV0dG9uIGNsYXNzTmFtZT17Y2xhc3Nlc30ga2V5PXtzdGF0ZUtleX0gb25DbGljaz17dGhpcy5vblZpZXdTb3VyY2VDbGljayhldil9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzdGF0ZUtleSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPjtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L0ZpbHRlcmVkTGlzdD47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgeyBsaXN0IH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy5vbkJhY2t9PnsgX3QoJ0JhY2snKSB9PC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgQWNjb3VudERhdGFFeHBsb3JlciBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIGdldExhYmVsKCkgeyByZXR1cm4gX3QoJ0V4cGxvcmUgQWNjb3VudCBEYXRhJyk7IH1cclxuXHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG9uQmFjazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICByb29tOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihSb29tKS5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgY29udGV4dFR5cGUgPSBNYXRyaXhDbGllbnRDb250ZXh0O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLm9uQmFjayA9IHRoaXMub25CYWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5lZGl0RXYgPSB0aGlzLmVkaXRFdi5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uQ2hhbmdlID0gdGhpcy5fb25DaGFuZ2UuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLm9uUXVlcnlFdmVudFR5cGUgPSB0aGlzLm9uUXVlcnlFdmVudFR5cGUuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaXNSb29tQWNjb3VudERhdGE6IGZhbHNlLFxyXG4gICAgICAgICAgICBldmVudDogbnVsbCxcclxuICAgICAgICAgICAgZWRpdGluZzogZmFsc2UsXHJcblxyXG4gICAgICAgICAgICBxdWVyeUV2ZW50VHlwZTogJycsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRhKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmlzUm9vbUFjY291bnREYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BzLnJvb20uYWNjb3VudERhdGE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRleHQuc3RvcmUuYWNjb3VudERhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgb25WaWV3U291cmNlQ2xpY2soZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgZXZlbnQgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBvbkJhY2soKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZWRpdGluZykge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgZWRpdGluZzogZmFsc2UgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBldmVudDogbnVsbCB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uQmFjaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25DaGFuZ2UoZSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1tlLnRhcmdldC5pZF06IGUudGFyZ2V0LnR5cGUgPT09ICdjaGVja2JveCcgPyBlLnRhcmdldC5jaGVja2VkIDogZS50YXJnZXQudmFsdWV9KTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0RXYoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXRpbmc6IHRydWUgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25RdWVyeUV2ZW50VHlwZShxdWVyeUV2ZW50VHlwZSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBxdWVyeUV2ZW50VHlwZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXZlbnQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZWRpdGluZykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxTZW5kQWNjb3VudERhdGFcclxuICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgaXNSb29tQWNjb3VudERhdGE9e3RoaXMuc3RhdGUuaXNSb29tQWNjb3VudERhdGF9XHJcbiAgICAgICAgICAgICAgICAgICAgb25CYWNrPXt0aGlzLm9uQmFja31cclxuICAgICAgICAgICAgICAgICAgICBpbnB1dHM9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRUeXBlOiB0aGlzLnN0YXRlLmV2ZW50LmdldFR5cGUoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZDb250ZW50OiBKU09OLnN0cmluZ2lmeSh0aGlzLnN0YXRlLmV2ZW50LmdldENvbnRlbnQoKSwgbnVsbCwgJ1xcdCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIH19IGZvcmNlTW9kZT17dHJ1ZX0gLz47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZpZXdTb3VyY2VcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxTeW50YXhIaWdobGlnaHQgY2xhc3NOYW1lPVwianNvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IEpTT04uc3RyaW5naWZ5KHRoaXMuc3RhdGUuZXZlbnQuZXZlbnQsIG51bGwsIDIpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L1N5bnRheEhpZ2hsaWdodD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy5vbkJhY2t9PnsgX3QoJ0JhY2snKSB9PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLmVkaXRFdn0+eyBfdCgnRWRpdCcpIH08L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByb3dzID0gW107XHJcblxyXG4gICAgICAgIGNvbnN0IGNsYXNzZXMgPSAnbXhfRGV2VG9vbHNfUm9vbVN0YXRlRXhwbG9yZXJfYnV0dG9uJztcclxuXHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZ2V0RGF0YSgpO1xyXG4gICAgICAgIE9iamVjdC5rZXlzKGRhdGEpLmZvckVhY2goKGV2VHlwZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IGRhdGFbZXZUeXBlXTtcclxuICAgICAgICAgICAgcm93cy5wdXNoKDxidXR0b24gY2xhc3NOYW1lPXtjbGFzc2VzfSBrZXk9e2V2VHlwZX0gb25DbGljaz17dGhpcy5vblZpZXdTb3VyY2VDbGljayhldil9PlxyXG4gICAgICAgICAgICAgICAgeyBldlR5cGUgfVxyXG4gICAgICAgICAgICA8L2J1dHRvbj4pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPEZpbHRlcmVkTGlzdCBxdWVyeT17dGhpcy5zdGF0ZS5xdWVyeUV2ZW50VHlwZX0gb25DaGFuZ2U9e3RoaXMub25RdWVyeUV2ZW50VHlwZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgeyByb3dzIH1cclxuICAgICAgICAgICAgICAgIDwvRmlsdGVyZWRMaXN0PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uQmFja30+eyBfdCgnQmFjaycpIH08L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIHsgIXRoaXMuc3RhdGUubWVzc2FnZSAmJiA8ZGl2IHN0eWxlPXt7ZmxvYXQ6IFwicmlnaHRcIn19PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImlzUm9vbUFjY291bnREYXRhXCIgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfdGdsIG14X0RldlRvb2xzX3RnbC1mbGlwXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfSBjaGVja2VkPXt0aGlzLnN0YXRlLmlzUm9vbUFjY291bnREYXRhfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJteF9EZXZUb29sc190Z2wtYnRuXCIgZGF0YS10Zy1vZmY9XCJBY2NvdW50IERhdGFcIiBkYXRhLXRnLW9uPVwiUm9vbSBEYXRhXCIgaHRtbEZvcj1cImlzUm9vbUFjY291bnREYXRhXCIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgU2VydmVyc0luUm9vbUxpc3QgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBnZXRMYWJlbCgpIHsgcmV0dXJuIF90KCdWaWV3IFNlcnZlcnMgaW4gUm9vbScpOyB9XHJcblxyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBvbkJhY2s6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLmluc3RhbmNlT2YoUm9vbSkuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGNvbnRleHRUeXBlID0gTWF0cml4Q2xpZW50Q29udGV4dDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMucHJvcHMucm9vbTtcclxuICAgICAgICBjb25zdCBzZXJ2ZXJzID0gbmV3IFNldCgpO1xyXG4gICAgICAgIHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLm1lbWJlclwiKS5mb3JFYWNoKGV2ID0+IHNlcnZlcnMuYWRkKGV2LmdldFNlbmRlcigpLnNwbGl0KFwiOlwiKVsxXSkpO1xyXG4gICAgICAgIHRoaXMuc2VydmVycyA9IEFycmF5LmZyb20oc2VydmVycykubWFwKHMgPT5cclxuICAgICAgICAgICAgPGJ1dHRvbiBrZXk9e3N9IGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX1NlcnZlcnNJblJvb21MaXN0X2J1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgeyBzIH1cclxuICAgICAgICAgICAgPC9idXR0b24+KTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcXVlcnk6ICcnLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgb25RdWVyeSA9IChxdWVyeSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBxdWVyeSB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxGaWx0ZXJlZExpc3QgcXVlcnk9e3RoaXMuc3RhdGUucXVlcnl9IG9uQ2hhbmdlPXt0aGlzLm9uUXVlcnl9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5zZXJ2ZXJzIH1cclxuICAgICAgICAgICAgICAgIDwvRmlsdGVyZWRMaXN0PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLnByb3BzLm9uQmFja30+eyBfdCgnQmFjaycpIH08L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBQSEFTRV9NQVAgPSB7XHJcbiAgICBbUEhBU0VfVU5TRU5UXTogXCJ1bnNlbnRcIixcclxuICAgIFtQSEFTRV9SRVFVRVNURURdOiBcInJlcXVlc3RlZFwiLFxyXG4gICAgW1BIQVNFX1JFQURZXTogXCJyZWFkeVwiLFxyXG4gICAgW1BIQVNFX0RPTkVdOiBcImRvbmVcIixcclxuICAgIFtQSEFTRV9TVEFSVEVEXTogXCJzdGFydGVkXCIsXHJcbiAgICBbUEhBU0VfQ0FOQ0VMTEVEXTogXCJjYW5jZWxsZWRcIixcclxufTtcclxuXHJcbmZ1bmN0aW9uIFZlcmlmaWNhdGlvblJlcXVlc3Qoe3R4bklkLCByZXF1ZXN0fSkge1xyXG4gICAgY29uc3QgWywgdXBkYXRlU3RhdGVdID0gdXNlU3RhdGUoKTtcclxuICAgIGNvbnN0IFt0aW1lb3V0LCBzZXRSZXF1ZXN0VGltZW91dF0gPSB1c2VTdGF0ZShyZXF1ZXN0LnRpbWVvdXQpO1xyXG5cclxuICAgIC8qIFJlLXJlbmRlciBpZiBzb21ldGhpbmcgY2hhbmdlcyBzdGF0ZSAqL1xyXG4gICAgdXNlRXZlbnRFbWl0dGVyKHJlcXVlc3QsIFwiY2hhbmdlXCIsIHVwZGF0ZVN0YXRlKTtcclxuXHJcbiAgICAvKiBLZWVwIHJlLXJlbmRlcmluZyBpZiB0aGVyZSdzIGEgdGltZW91dCAqL1xyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBpZiAocmVxdWVzdC50aW1lb3V0ID09IDApIHJldHVybjtcclxuXHJcbiAgICAgICAgLyogTm90ZSB0aGF0IHJlcXVlc3QudGltZW91dCBpcyBhIGdldHRlciwgc28gaXRzIHZhbHVlIGNoYW5nZXMgKi9cclxuICAgICAgICBjb25zdCBpZCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICBzZXRSZXF1ZXN0VGltZW91dChyZXF1ZXN0LnRpbWVvdXQpO1xyXG4gICAgICAgIH0sIDUwMCk7XHJcblxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7IGNsZWFySW50ZXJ2YWwoaWQpOyB9O1xyXG4gICAgfSwgW3JlcXVlc3RdKTtcclxuXHJcbiAgICByZXR1cm4gKDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfVmVyaWZpY2F0aW9uUmVxdWVzdFwiPlxyXG4gICAgICAgIDxkbD5cclxuICAgICAgICAgICAgPGR0PlRyYW5zYWN0aW9uPC9kdD5cclxuICAgICAgICAgICAgPGRkPnt0eG5JZH08L2RkPlxyXG4gICAgICAgICAgICA8ZHQ+UGhhc2U8L2R0PlxyXG4gICAgICAgICAgICA8ZGQ+e1BIQVNFX01BUFtyZXF1ZXN0LnBoYXNlXSB8fCByZXF1ZXN0LnBoYXNlfTwvZGQ+XHJcbiAgICAgICAgICAgIDxkdD5UaW1lb3V0PC9kdD5cclxuICAgICAgICAgICAgPGRkPntNYXRoLmZsb29yKHRpbWVvdXQgLyAxMDAwKX08L2RkPlxyXG4gICAgICAgICAgICA8ZHQ+TWV0aG9kczwvZHQ+XHJcbiAgICAgICAgICAgIDxkZD57cmVxdWVzdC5tZXRob2RzICYmIHJlcXVlc3QubWV0aG9kcy5qb2luKFwiLCBcIil9PC9kZD5cclxuICAgICAgICAgICAgPGR0PnJlcXVlc3RpbmdVc2VySWQ8L2R0PlxyXG4gICAgICAgICAgICA8ZGQ+e3JlcXVlc3QucmVxdWVzdGluZ1VzZXJJZH08L2RkPlxyXG4gICAgICAgICAgICA8ZHQ+b2JzZXJ2ZU9ubHk8L2R0PlxyXG4gICAgICAgICAgICA8ZGQ+e0pTT04uc3RyaW5naWZ5KHJlcXVlc3Qub2JzZXJ2ZU9ubHkpfTwvZGQ+XHJcbiAgICAgICAgPC9kbD5cclxuICAgIDwvZGl2Pik7XHJcbn1cclxuXHJcbmNsYXNzIFZlcmlmaWNhdGlvbkV4cGxvcmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBnZXRMYWJlbCgpIHtcclxuICAgICAgICByZXR1cm4gX3QoXCJWZXJpZmljYXRpb24gUmVxdWVzdHNcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyogRW5zdXJlIHRoaXMuY29udGV4dCBpcyB0aGUgY2xpICovXHJcbiAgICBzdGF0aWMgY29udGV4dFR5cGUgPSBNYXRyaXhDbGllbnRDb250ZXh0O1xyXG5cclxuICAgIG9uTmV3UmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpID0gdGhpcy5jb250ZXh0O1xyXG4gICAgICAgIGNsaS5vbihcImNyeXB0by52ZXJpZmljYXRpb24ucmVxdWVzdFwiLCB0aGlzLm9uTmV3UmVxdWVzdCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpID0gdGhpcy5jb250ZXh0O1xyXG4gICAgICAgIGNsaS5vZmYoXCJjcnlwdG8udmVyaWZpY2F0aW9uLnJlcXVlc3RcIiwgdGhpcy5vbk5ld1JlcXVlc3QpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSB0aGlzLmNvbnRleHQ7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMucHJvcHMucm9vbTtcclxuICAgICAgICBjb25zdCBpblJvb21DaGFubmVsID0gY2xpLl9jcnlwdG8uX2luUm9vbVZlcmlmaWNhdGlvblJlcXVlc3RzO1xyXG4gICAgICAgIGNvbnN0IGluUm9vbVJlcXVlc3RzID0gKGluUm9vbUNoYW5uZWwuX3JlcXVlc3RzQnlSb29tSWQgfHwgbmV3IE1hcCgpKS5nZXQocm9vbS5yb29tSWQpIHx8IG5ldyBNYXAoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuICg8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICB7QXJyYXkuZnJvbShpblJvb21SZXF1ZXN0cy5lbnRyaWVzKCkpLnJldmVyc2UoKS5tYXAoKFt0eG5JZCwgcmVxdWVzdF0pID0+XHJcbiAgICAgICAgICAgICAgICAgICAgPFZlcmlmaWNhdGlvblJlcXVlc3QgdHhuSWQ9e3R4bklkfSByZXF1ZXN0PXtyZXF1ZXN0fSBrZXk9e3R4bklkfSAvPixcclxuICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2Pik7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IEVudHJpZXMgPSBbXHJcbiAgICBTZW5kQ3VzdG9tRXZlbnQsXHJcbiAgICBSb29tU3RhdGVFeHBsb3JlcixcclxuICAgIFNlbmRBY2NvdW50RGF0YSxcclxuICAgIEFjY291bnREYXRhRXhwbG9yZXIsXHJcbiAgICBTZXJ2ZXJzSW5Sb29tTGlzdCxcclxuICAgIFZlcmlmaWNhdGlvbkV4cGxvcmVyLFxyXG5dO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRGV2dG9vbHNEaWFsb2cgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgcm9vbUlkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5vbkJhY2sgPSB0aGlzLm9uQmFjay5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25DYW5jZWwgPSB0aGlzLm9uQ2FuY2VsLmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIG1vZGU6IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXRNb2RlKG1vZGUpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgbW9kZSB9KTtcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIG9uQmFjaygpIHtcclxuICAgICAgICBpZiAodGhpcy5wcmV2TW9kZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgbW9kZTogdGhpcy5wcmV2TW9kZSB9KTtcclxuICAgICAgICAgICAgdGhpcy5wcmV2TW9kZSA9IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGU6IG51bGwgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2FuY2VsKCkge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZChmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBib2R5O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5tb2RlKSB7XHJcbiAgICAgICAgICAgIGJvZHkgPSA8TWF0cml4Q2xpZW50Q29udGV4dC5Db25zdW1lcj5cclxuICAgICAgICAgICAgICAgIHsoY2xpKSA9PiA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EZXZUb29sc19sYWJlbF9sZWZ0XCI+eyB0aGlzLnN0YXRlLm1vZGUuZ2V0TGFiZWwoKSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EZXZUb29sc19sYWJlbF9yaWdodFwiPlJvb20gSUQ6IHsgdGhpcy5wcm9wcy5yb29tSWQgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfbGFiZWxfYm90dG9tXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8dGhpcy5zdGF0ZS5tb2RlIG9uQmFjaz17dGhpcy5vbkJhY2t9IHJvb209e2NsaS5nZXRSb29tKHRoaXMucHJvcHMucm9vbUlkKX0gLz5cclxuICAgICAgICAgICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+fVxyXG4gICAgICAgICAgICA8L01hdHJpeENsaWVudENvbnRleHQuQ29uc3VtZXI+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNsYXNzZXMgPSBcIm14X0RldlRvb2xzX1Jvb21TdGF0ZUV4cGxvcmVyX2J1dHRvblwiO1xyXG4gICAgICAgICAgICBib2R5ID0gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX2xhYmVsX2xlZnRcIj57IF90KCdUb29sYm94JykgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2VG9vbHNfbGFiZWxfcmlnaHRcIj5Sb29tIElEOiB7IHRoaXMucHJvcHMucm9vbUlkIH08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RldlRvb2xzX2xhYmVsX2JvdHRvbVwiIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBFbnRyaWVzLm1hcCgoRW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGxhYmVsID0gRW50cnkuZ2V0TGFiZWwoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG9uQ2xpY2sgPSB0aGlzLl9zZXRNb2RlKEVudHJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8YnV0dG9uIGNsYXNzTmFtZT17Y2xhc3Nlc30ga2V5PXtsYWJlbH0gb25DbGljaz17b25DbGlja30+eyBsYWJlbCB9PC9idXR0b24+O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMub25DYW5jZWx9PnsgX3QoJ0NhbmNlbCcpIH08L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZURpYWxvZyBjbGFzc05hbWU9XCJteF9RdWVzdGlvbkRpYWxvZ1wiIG9uRmluaXNoZWQ9e3RoaXMucHJvcHMub25GaW5pc2hlZH0gdGl0bGU9e190KCdEZXZlbG9wZXIgVG9vbHMnKX0+XHJcbiAgICAgICAgICAgICAgICB7IGJvZHkgfVxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=