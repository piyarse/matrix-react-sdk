"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * A dialog for confirming a redaction.
 * Also shows a spinner (and possible error) while the redaction is ongoing,
 * and only closes the dialog when the redaction is done or failed.
 *
 * This is done to prevent the edit history dialog racing with the redaction:
 * if this dialog closes and the MessageEditHistoryDialog is shown again,
 * it will fetch the relations again, which will race with the ongoing /redact request.
 * which will cause the edit to appear unredacted.
 *
 * To avoid this, we keep the dialog open as long as /redact is in progress.
 */
class ConfirmAndWaitRedactDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onParentFinished", async proceed => {
      if (proceed) {
        this.setState({
          isRedacting: true
        });

        try {
          await this.props.redact();
          this.props.onFinished(true);
        } catch (error) {
          const code = error.errcode || error.statusCode;

          if (typeof code !== "undefined") {
            this.setState({
              redactionErrorCode: code
            });
          } else {
            this.props.onFinished(true);
          }
        }
      } else {
        this.props.onFinished(false);
      }
    });
    this.state = {
      isRedacting: false,
      redactionErrorCode: null
    };
  }

  render() {
    if (this.state.isRedacting) {
      if (this.state.redactionErrorCode) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        const code = this.state.redactionErrorCode;
        return _react.default.createElement(ErrorDialog, {
          onFinished: this.props.onFinished,
          title: (0, _languageHandler._t)('Error'),
          description: (0, _languageHandler._t)('You cannot delete this message. (%(code)s)', {
            code
          })
        });
      } else {
        const BaseDialog = sdk.getComponent("dialogs.BaseDialog");
        const Spinner = sdk.getComponent('elements.Spinner');
        return _react.default.createElement(BaseDialog, {
          onFinished: this.props.onFinished,
          hasCancel: false,
          title: (0, _languageHandler._t)("Removing…")
        }, _react.default.createElement(Spinner, null));
      }
    } else {
      const ConfirmRedactDialog = sdk.getComponent("dialogs.ConfirmRedactDialog");
      return _react.default.createElement(ConfirmRedactDialog, {
        onFinished: this.onParentFinished
      });
    }
  }

}

exports.default = ConfirmAndWaitRedactDialog;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvQ29uZmlybUFuZFdhaXRSZWRhY3REaWFsb2cuanMiXSwibmFtZXMiOlsiQ29uZmlybUFuZFdhaXRSZWRhY3REaWFsb2ciLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwicHJvY2VlZCIsInNldFN0YXRlIiwiaXNSZWRhY3RpbmciLCJyZWRhY3QiLCJvbkZpbmlzaGVkIiwiZXJyb3IiLCJjb2RlIiwiZXJyY29kZSIsInN0YXR1c0NvZGUiLCJyZWRhY3Rpb25FcnJvckNvZGUiLCJzdGF0ZSIsInJlbmRlciIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiQmFzZURpYWxvZyIsIlNwaW5uZXIiLCJDb25maXJtUmVkYWN0RGlhbG9nIiwib25QYXJlbnRGaW5pc2hlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFsQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkE7Ozs7Ozs7Ozs7OztBQVllLE1BQU1BLDBCQUFOLFNBQXlDQyxlQUFNQyxhQUEvQyxDQUE2RDtBQUN4RUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsNERBUUEsTUFBT0MsT0FBUCxJQUFtQjtBQUNsQyxVQUFJQSxPQUFKLEVBQWE7QUFDVCxhQUFLQyxRQUFMLENBQWM7QUFBQ0MsVUFBQUEsV0FBVyxFQUFFO0FBQWQsU0FBZDs7QUFDQSxZQUFJO0FBQ0EsZ0JBQU0sS0FBS0gsS0FBTCxDQUFXSSxNQUFYLEVBQU47QUFDQSxlQUFLSixLQUFMLENBQVdLLFVBQVgsQ0FBc0IsSUFBdEI7QUFDSCxTQUhELENBR0UsT0FBT0MsS0FBUCxFQUFjO0FBQ1osZ0JBQU1DLElBQUksR0FBR0QsS0FBSyxDQUFDRSxPQUFOLElBQWlCRixLQUFLLENBQUNHLFVBQXBDOztBQUNBLGNBQUksT0FBT0YsSUFBUCxLQUFnQixXQUFwQixFQUFpQztBQUM3QixpQkFBS0wsUUFBTCxDQUFjO0FBQUNRLGNBQUFBLGtCQUFrQixFQUFFSDtBQUFyQixhQUFkO0FBQ0gsV0FGRCxNQUVPO0FBQ0gsaUJBQUtQLEtBQUwsQ0FBV0ssVUFBWCxDQUFzQixJQUF0QjtBQUNIO0FBQ0o7QUFDSixPQWJELE1BYU87QUFDSCxhQUFLTCxLQUFMLENBQVdLLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSDtBQUNKLEtBekJrQjtBQUVmLFNBQUtNLEtBQUwsR0FBYTtBQUNUUixNQUFBQSxXQUFXLEVBQUUsS0FESjtBQUVUTyxNQUFBQSxrQkFBa0IsRUFBRTtBQUZYLEtBQWI7QUFJSDs7QUFxQkRFLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUksS0FBS0QsS0FBTCxDQUFXUixXQUFmLEVBQTRCO0FBQ3hCLFVBQUksS0FBS1EsS0FBTCxDQUFXRCxrQkFBZixFQUFtQztBQUMvQixjQUFNRyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQSxjQUFNUixJQUFJLEdBQUcsS0FBS0ksS0FBTCxDQUFXRCxrQkFBeEI7QUFDQSxlQUNJLDZCQUFDLFdBQUQ7QUFDSSxVQUFBLFVBQVUsRUFBRSxLQUFLVixLQUFMLENBQVdLLFVBRDNCO0FBRUksVUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUZYO0FBR0ksVUFBQSxXQUFXLEVBQUUseUJBQUcsNENBQUgsRUFBaUQ7QUFBQ0UsWUFBQUE7QUFBRCxXQUFqRDtBQUhqQixVQURKO0FBT0gsT0FWRCxNQVVPO0FBQ0gsY0FBTVMsVUFBVSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsb0JBQWpCLENBQW5CO0FBQ0EsY0FBTUUsT0FBTyxHQUFHSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBQ0EsZUFDSSw2QkFBQyxVQUFEO0FBQ0ksVUFBQSxVQUFVLEVBQUUsS0FBS2YsS0FBTCxDQUFXSyxVQUQzQjtBQUVJLFVBQUEsU0FBUyxFQUFFLEtBRmY7QUFHSSxVQUFBLEtBQUssRUFBRSx5QkFBRyxXQUFIO0FBSFgsV0FJSSw2QkFBQyxPQUFELE9BSkosQ0FESjtBQVFIO0FBQ0osS0F2QkQsTUF1Qk87QUFDSCxZQUFNYSxtQkFBbUIsR0FBR0osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDZCQUFqQixDQUE1QjtBQUNBLGFBQU8sNkJBQUMsbUJBQUQ7QUFBcUIsUUFBQSxVQUFVLEVBQUUsS0FBS0k7QUFBdEMsUUFBUDtBQUNIO0FBQ0o7O0FBeER1RSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuLypcclxuICogQSBkaWFsb2cgZm9yIGNvbmZpcm1pbmcgYSByZWRhY3Rpb24uXHJcbiAqIEFsc28gc2hvd3MgYSBzcGlubmVyIChhbmQgcG9zc2libGUgZXJyb3IpIHdoaWxlIHRoZSByZWRhY3Rpb24gaXMgb25nb2luZyxcclxuICogYW5kIG9ubHkgY2xvc2VzIHRoZSBkaWFsb2cgd2hlbiB0aGUgcmVkYWN0aW9uIGlzIGRvbmUgb3IgZmFpbGVkLlxyXG4gKlxyXG4gKiBUaGlzIGlzIGRvbmUgdG8gcHJldmVudCB0aGUgZWRpdCBoaXN0b3J5IGRpYWxvZyByYWNpbmcgd2l0aCB0aGUgcmVkYWN0aW9uOlxyXG4gKiBpZiB0aGlzIGRpYWxvZyBjbG9zZXMgYW5kIHRoZSBNZXNzYWdlRWRpdEhpc3RvcnlEaWFsb2cgaXMgc2hvd24gYWdhaW4sXHJcbiAqIGl0IHdpbGwgZmV0Y2ggdGhlIHJlbGF0aW9ucyBhZ2Fpbiwgd2hpY2ggd2lsbCByYWNlIHdpdGggdGhlIG9uZ29pbmcgL3JlZGFjdCByZXF1ZXN0LlxyXG4gKiB3aGljaCB3aWxsIGNhdXNlIHRoZSBlZGl0IHRvIGFwcGVhciB1bnJlZGFjdGVkLlxyXG4gKlxyXG4gKiBUbyBhdm9pZCB0aGlzLCB3ZSBrZWVwIHRoZSBkaWFsb2cgb3BlbiBhcyBsb25nIGFzIC9yZWRhY3QgaXMgaW4gcHJvZ3Jlc3MuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb25maXJtQW5kV2FpdFJlZGFjdERpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaXNSZWRhY3Rpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICByZWRhY3Rpb25FcnJvckNvZGU6IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBvblBhcmVudEZpbmlzaGVkID0gYXN5bmMgKHByb2NlZWQpID0+IHtcclxuICAgICAgICBpZiAocHJvY2VlZCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc1JlZGFjdGluZzogdHJ1ZX0pO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5wcm9wcy5yZWRhY3QoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0cnVlKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvZGUgPSBlcnJvci5lcnJjb2RlIHx8IGVycm9yLnN0YXR1c0NvZGU7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGNvZGUgIT09IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtyZWRhY3Rpb25FcnJvckNvZGU6IGNvZGV9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pc1JlZGFjdGluZykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZWRhY3Rpb25FcnJvckNvZGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjb2RlID0gdGhpcy5zdGF0ZS5yZWRhY3Rpb25FcnJvckNvZGU7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxFcnJvckRpYWxvZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdCgnRXJyb3InKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb249e190KCdZb3UgY2Fubm90IGRlbGV0ZSB0aGlzIG1lc3NhZ2UuICglKGNvZGUpcyknLCB7Y29kZX0pfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkJhc2VEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuU3Bpbm5lcicpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8QmFzZURpYWxvZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlJlbW92aW5n4oCmXCIpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFNwaW5uZXIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgQ29uZmlybVJlZGFjdERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkNvbmZpcm1SZWRhY3REaWFsb2dcIik7XHJcbiAgICAgICAgICAgIHJldHVybiA8Q29uZmlybVJlZGFjdERpYWxvZyBvbkZpbmlzaGVkPXt0aGlzLm9uUGFyZW50RmluaXNoZWR9IC8+O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=