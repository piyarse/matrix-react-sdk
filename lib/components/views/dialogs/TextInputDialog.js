"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Field = _interopRequireDefault(require("../elements/Field"));

/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'TextInputDialog',
  propTypes: {
    title: _propTypes.default.string,
    description: _propTypes.default.oneOfType([_propTypes.default.element, _propTypes.default.string]),
    value: _propTypes.default.string,
    placeholder: _propTypes.default.string,
    button: _propTypes.default.string,
    focus: _propTypes.default.bool,
    onFinished: _propTypes.default.func.isRequired,
    hasCancel: _propTypes.default.bool,
    validator: _propTypes.default.func,
    // result of withValidation
    fixedWidth: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      title: "",
      value: "",
      description: "",
      focus: true,
      hasCancel: true
    };
  },
  getInitialState: function () {
    return {
      value: this.props.value,
      valid: false
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._field = (0, _react.createRef)();
  },
  componentDidMount: function () {
    if (this.props.focus) {
      // Set the cursor at the end of the text input
      // this._field.current.value = this.props.value;
      this._field.current.focus();
    }
  },
  onOk: async function (ev) {
    ev.preventDefault();

    if (this.props.validator) {
      await this._field.current.validate({
        allowEmpty: false
      });

      if (!this._field.current.state.valid) {
        this._field.current.focus();

        this._field.current.validate({
          allowEmpty: false,
          focused: true
        });

        return;
      }
    }

    this.props.onFinished(true, this.state.value);
  },
  onCancel: function () {
    this.props.onFinished(false);
  },
  onChange: function (ev) {
    this.setState({
      value: ev.target.value
    });
  },
  onValidate: async function (fieldState) {
    const result = await this.props.validator(fieldState);
    this.setState({
      valid: result.valid
    });
    return result;
  },
  render: function () {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement(BaseDialog, {
      className: "mx_TextInputDialog",
      onFinished: this.props.onFinished,
      title: this.props.title,
      fixedWidth: this.props.fixedWidth
    }, _react.default.createElement("form", {
      onSubmit: this.onOk
    }, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, _react.default.createElement("div", {
      className: "mx_TextInputDialog_label"
    }, _react.default.createElement("label", {
      htmlFor: "textinput"
    }, " ", this.props.description, " ")), _react.default.createElement("div", null, _react.default.createElement(_Field.default, {
      className: "mx_TextInputDialog_input",
      ref: this._field,
      type: "text",
      label: this.props.placeholder,
      value: this.state.value,
      onChange: this.onChange,
      onValidate: this.props.validator ? this.onValidate : undefined,
      size: "64"
    })))), _react.default.createElement(DialogButtons, {
      primaryButton: this.props.button,
      onPrimaryButtonClick: this.onOk,
      onCancel: this.onCancel,
      hasCancel: this.props.hasCancel
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvVGV4dElucHV0RGlhbG9nLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwidGl0bGUiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJkZXNjcmlwdGlvbiIsIm9uZU9mVHlwZSIsImVsZW1lbnQiLCJ2YWx1ZSIsInBsYWNlaG9sZGVyIiwiYnV0dG9uIiwiZm9jdXMiLCJib29sIiwib25GaW5pc2hlZCIsImZ1bmMiLCJpc1JlcXVpcmVkIiwiaGFzQ2FuY2VsIiwidmFsaWRhdG9yIiwiZml4ZWRXaWR0aCIsImdldERlZmF1bHRQcm9wcyIsImdldEluaXRpYWxTdGF0ZSIsInByb3BzIiwidmFsaWQiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX2ZpZWxkIiwiY29tcG9uZW50RGlkTW91bnQiLCJjdXJyZW50Iiwib25PayIsImV2IiwicHJldmVudERlZmF1bHQiLCJ2YWxpZGF0ZSIsImFsbG93RW1wdHkiLCJzdGF0ZSIsImZvY3VzZWQiLCJvbkNhbmNlbCIsIm9uQ2hhbmdlIiwic2V0U3RhdGUiLCJ0YXJnZXQiLCJvblZhbGlkYXRlIiwiZmllbGRTdGF0ZSIsInJlc3VsdCIsInJlbmRlciIsIkJhc2VEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJEaWFsb2dCdXR0b25zIiwidW5kZWZpbmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFwQkE7Ozs7Ozs7Ozs7Ozs7OztlQXNCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxpQkFEZTtBQUU1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLEtBQUssRUFBRUMsbUJBQVVDLE1BRFY7QUFFUEMsSUFBQUEsV0FBVyxFQUFFRixtQkFBVUcsU0FBVixDQUFvQixDQUM3QkgsbUJBQVVJLE9BRG1CLEVBRTdCSixtQkFBVUMsTUFGbUIsQ0FBcEIsQ0FGTjtBQU1QSSxJQUFBQSxLQUFLLEVBQUVMLG1CQUFVQyxNQU5WO0FBT1BLLElBQUFBLFdBQVcsRUFBRU4sbUJBQVVDLE1BUGhCO0FBUVBNLElBQUFBLE1BQU0sRUFBRVAsbUJBQVVDLE1BUlg7QUFTUE8sSUFBQUEsS0FBSyxFQUFFUixtQkFBVVMsSUFUVjtBQVVQQyxJQUFBQSxVQUFVLEVBQUVWLG1CQUFVVyxJQUFWLENBQWVDLFVBVnBCO0FBV1BDLElBQUFBLFNBQVMsRUFBRWIsbUJBQVVTLElBWGQ7QUFZUEssSUFBQUEsU0FBUyxFQUFFZCxtQkFBVVcsSUFaZDtBQVlvQjtBQUMzQkksSUFBQUEsVUFBVSxFQUFFZixtQkFBVVM7QUFiZixHQUZpQjtBQWtCNUJPLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSGpCLE1BQUFBLEtBQUssRUFBRSxFQURKO0FBRUhNLE1BQUFBLEtBQUssRUFBRSxFQUZKO0FBR0hILE1BQUFBLFdBQVcsRUFBRSxFQUhWO0FBSUhNLE1BQUFBLEtBQUssRUFBRSxJQUpKO0FBS0hLLE1BQUFBLFNBQVMsRUFBRTtBQUxSLEtBQVA7QUFPSCxHQTFCMkI7QUE0QjVCSSxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0haLE1BQUFBLEtBQUssRUFBRSxLQUFLYSxLQUFMLENBQVdiLEtBRGY7QUFFSGMsTUFBQUEsS0FBSyxFQUFFO0FBRkosS0FBUDtBQUlILEdBakMyQjtBQW1DNUI7QUFDQUMsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLQyxNQUFMLEdBQWMsdUJBQWQ7QUFDSCxHQXRDMkI7QUF3QzVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFFBQUksS0FBS0osS0FBTCxDQUFXVixLQUFmLEVBQXNCO0FBQ2xCO0FBQ0E7QUFDQSxXQUFLYSxNQUFMLENBQVlFLE9BQVosQ0FBb0JmLEtBQXBCO0FBQ0g7QUFDSixHQTlDMkI7QUFnRDVCZ0IsRUFBQUEsSUFBSSxFQUFFLGdCQUFlQyxFQUFmLEVBQW1CO0FBQ3JCQSxJQUFBQSxFQUFFLENBQUNDLGNBQUg7O0FBQ0EsUUFBSSxLQUFLUixLQUFMLENBQVdKLFNBQWYsRUFBMEI7QUFDdEIsWUFBTSxLQUFLTyxNQUFMLENBQVlFLE9BQVosQ0FBb0JJLFFBQXBCLENBQTZCO0FBQUVDLFFBQUFBLFVBQVUsRUFBRTtBQUFkLE9BQTdCLENBQU47O0FBRUEsVUFBSSxDQUFDLEtBQUtQLE1BQUwsQ0FBWUUsT0FBWixDQUFvQk0sS0FBcEIsQ0FBMEJWLEtBQS9CLEVBQXNDO0FBQ2xDLGFBQUtFLE1BQUwsQ0FBWUUsT0FBWixDQUFvQmYsS0FBcEI7O0FBQ0EsYUFBS2EsTUFBTCxDQUFZRSxPQUFaLENBQW9CSSxRQUFwQixDQUE2QjtBQUFFQyxVQUFBQSxVQUFVLEVBQUUsS0FBZDtBQUFxQkUsVUFBQUEsT0FBTyxFQUFFO0FBQTlCLFNBQTdCOztBQUNBO0FBQ0g7QUFDSjs7QUFDRCxTQUFLWixLQUFMLENBQVdSLFVBQVgsQ0FBc0IsSUFBdEIsRUFBNEIsS0FBS21CLEtBQUwsQ0FBV3hCLEtBQXZDO0FBQ0gsR0E1RDJCO0FBOEQ1QjBCLEVBQUFBLFFBQVEsRUFBRSxZQUFXO0FBQ2pCLFNBQUtiLEtBQUwsQ0FBV1IsVUFBWCxDQUFzQixLQUF0QjtBQUNILEdBaEUyQjtBQWtFNUJzQixFQUFBQSxRQUFRLEVBQUUsVUFBU1AsRUFBVCxFQUFhO0FBQ25CLFNBQUtRLFFBQUwsQ0FBYztBQUNWNUIsTUFBQUEsS0FBSyxFQUFFb0IsRUFBRSxDQUFDUyxNQUFILENBQVU3QjtBQURQLEtBQWQ7QUFHSCxHQXRFMkI7QUF3RTVCOEIsRUFBQUEsVUFBVSxFQUFFLGdCQUFlQyxVQUFmLEVBQTJCO0FBQ25DLFVBQU1DLE1BQU0sR0FBRyxNQUFNLEtBQUtuQixLQUFMLENBQVdKLFNBQVgsQ0FBcUJzQixVQUFyQixDQUFyQjtBQUNBLFNBQUtILFFBQUwsQ0FBYztBQUNWZCxNQUFBQSxLQUFLLEVBQUVrQixNQUFNLENBQUNsQjtBQURKLEtBQWQ7QUFHQSxXQUFPa0IsTUFBUDtBQUNILEdBOUUyQjtBQWdGNUJDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FDSSw2QkFBQyxVQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsb0JBRGQ7QUFFSSxNQUFBLFVBQVUsRUFBRSxLQUFLdkIsS0FBTCxDQUFXUixVQUYzQjtBQUdJLE1BQUEsS0FBSyxFQUFFLEtBQUtRLEtBQUwsQ0FBV25CLEtBSHRCO0FBSUksTUFBQSxVQUFVLEVBQUUsS0FBS21CLEtBQUwsQ0FBV0g7QUFKM0IsT0FNSTtBQUFNLE1BQUEsUUFBUSxFQUFFLEtBQUtTO0FBQXJCLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTyxNQUFBLE9BQU8sRUFBQztBQUFmLFlBQThCLEtBQUtOLEtBQUwsQ0FBV2hCLFdBQXpDLE1BREosQ0FESixFQUlJLDBDQUNJLDZCQUFDLGNBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBQywwQkFEZDtBQUVJLE1BQUEsR0FBRyxFQUFFLEtBQUttQixNQUZkO0FBR0ksTUFBQSxJQUFJLEVBQUMsTUFIVDtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUtILEtBQUwsQ0FBV1osV0FKdEI7QUFLSSxNQUFBLEtBQUssRUFBRSxLQUFLdUIsS0FBTCxDQUFXeEIsS0FMdEI7QUFNSSxNQUFBLFFBQVEsRUFBRSxLQUFLMkIsUUFObkI7QUFPSSxNQUFBLFVBQVUsRUFBRSxLQUFLZCxLQUFMLENBQVdKLFNBQVgsR0FBdUIsS0FBS3FCLFVBQTVCLEdBQXlDUSxTQVB6RDtBQVFJLE1BQUEsSUFBSSxFQUFDO0FBUlQsTUFESixDQUpKLENBREosQ0FOSixFQXlCSSw2QkFBQyxhQUFEO0FBQ0ksTUFBQSxhQUFhLEVBQUUsS0FBS3pCLEtBQUwsQ0FBV1gsTUFEOUI7QUFFSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtpQixJQUYvQjtBQUdJLE1BQUEsUUFBUSxFQUFFLEtBQUtPLFFBSG5CO0FBSUksTUFBQSxTQUFTLEVBQUUsS0FBS2IsS0FBTCxDQUFXTDtBQUoxQixNQXpCSixDQURKO0FBa0NIO0FBckgyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IEZpZWxkIGZyb20gXCIuLi9lbGVtZW50cy9GaWVsZFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1RleHRJbnB1dERpYWxvZycsXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbXHJcbiAgICAgICAgICAgIFByb3BUeXBlcy5lbGVtZW50LFxyXG4gICAgICAgICAgICBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIF0pLFxyXG4gICAgICAgIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGJ1dHRvbjogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBmb2N1czogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgICAgICBoYXNDYW5jZWw6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIHZhbGlkYXRvcjogUHJvcFR5cGVzLmZ1bmMsIC8vIHJlc3VsdCBvZiB3aXRoVmFsaWRhdGlvblxyXG4gICAgICAgIGZpeGVkV2lkdGg6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogXCJcIixcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiXCIsXHJcbiAgICAgICAgICAgIGZvY3VzOiB0cnVlLFxyXG4gICAgICAgICAgICBoYXNDYW5jZWw6IHRydWUsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB2YWx1ZTogdGhpcy5wcm9wcy52YWx1ZSxcclxuICAgICAgICAgICAgdmFsaWQ6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIGNvbXBvbmVudCB3aXRoIHJlYWwgY2xhc3MsIHVzZSBjb25zdHJ1Y3RvciBmb3IgcmVmc1xyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fZmllbGQgPSBjcmVhdGVSZWYoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmZvY3VzKSB7XHJcbiAgICAgICAgICAgIC8vIFNldCB0aGUgY3Vyc29yIGF0IHRoZSBlbmQgb2YgdGhlIHRleHQgaW5wdXRcclxuICAgICAgICAgICAgLy8gdGhpcy5fZmllbGQuY3VycmVudC52YWx1ZSA9IHRoaXMucHJvcHMudmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMuX2ZpZWxkLmN1cnJlbnQuZm9jdXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uT2s6IGFzeW5jIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy52YWxpZGF0b3IpIHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5fZmllbGQuY3VycmVudC52YWxpZGF0ZSh7IGFsbG93RW1wdHk6IGZhbHNlIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9maWVsZC5jdXJyZW50LnN0YXRlLnZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9maWVsZC5jdXJyZW50LmZvY3VzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9maWVsZC5jdXJyZW50LnZhbGlkYXRlKHsgYWxsb3dFbXB0eTogZmFsc2UsIGZvY3VzZWQ6IHRydWUgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUsIHRoaXMuc3RhdGUudmFsdWUpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNhbmNlbDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DaGFuZ2U6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHZhbHVlOiBldi50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uVmFsaWRhdGU6IGFzeW5jIGZ1bmN0aW9uKGZpZWxkU3RhdGUpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLnByb3BzLnZhbGlkYXRvcihmaWVsZFN0YXRlKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdmFsaWQ6IHJlc3VsdC52YWxpZCxcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9UZXh0SW5wdXREaWFsb2dcIlxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9e3RoaXMucHJvcHMudGl0bGV9XHJcbiAgICAgICAgICAgICAgICBmaXhlZFdpZHRoPXt0aGlzLnByb3BzLmZpeGVkV2lkdGh9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLm9uT2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9UZXh0SW5wdXREaWFsb2dfbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwidGV4dGlucHV0XCI+IHsgdGhpcy5wcm9wcy5kZXNjcmlwdGlvbiB9IDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfVGV4dElucHV0RGlhbG9nX2lucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX2ZpZWxkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dGhpcy5wcm9wcy5wbGFjZWhvbGRlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS52YWx1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblZhbGlkYXRlPXt0aGlzLnByb3BzLnZhbGlkYXRvciA/IHRoaXMub25WYWxpZGF0ZSA6IHVuZGVmaW5lZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaXplPVwiNjRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e3RoaXMucHJvcHMuYnV0dG9ufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLm9uT2t9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMub25DYW5jZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0aGlzLnByb3BzLmhhc0NhbmNlbH1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==