"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _QuestionDialog = _interopRequireDefault(require("./QuestionDialog"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = props => {
  const existingIssuesUrl = "https://github.com/vector-im/riot-web/issues" + "?q=is%3Aopen+is%3Aissue+sort%3Areactions-%2B1-desc";
  const newIssueUrl = "https://github.com/vector-im/riot-web/issues/new";
  const description1 = (0, _languageHandler._t)("If you run into any bugs or have feedback you'd like to share, " + "please let us know on GitHub.");
  const description2 = (0, _languageHandler._t)("To help avoid duplicate issues, " + "please <existingIssuesLink>view existing issues</existingIssuesLink> " + "first (and add a +1) or <newIssueLink>create a new issue</newIssueLink> " + "if you can't find it.", {}, {
    existingIssuesLink: sub => {
      return _react.default.createElement("a", {
        target: "_blank",
        rel: "noreferrer noopener",
        href: existingIssuesUrl
      }, sub);
    },
    newIssueLink: sub => {
      return _react.default.createElement("a", {
        target: "_blank",
        rel: "noreferrer noopener",
        href: newIssueUrl
      }, sub);
    }
  });
  return _react.default.createElement(_QuestionDialog.default, {
    hasCancelButton: false,
    title: (0, _languageHandler._t)("Report bugs & give feedback"),
    description: _react.default.createElement("div", null, _react.default.createElement("p", null, description1), _react.default.createElement("p", null, description2)),
    button: (0, _languageHandler._t)("Go back"),
    onFinished: props.onFinished
  });
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvUmVkZXNpZ25GZWVkYmFja0RpYWxvZy5qcyJdLCJuYW1lcyI6WyJwcm9wcyIsImV4aXN0aW5nSXNzdWVzVXJsIiwibmV3SXNzdWVVcmwiLCJkZXNjcmlwdGlvbjEiLCJkZXNjcmlwdGlvbjIiLCJleGlzdGluZ0lzc3Vlc0xpbmsiLCJzdWIiLCJuZXdJc3N1ZUxpbmsiLCJvbkZpbmlzaGVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBbEJBOzs7Ozs7Ozs7Ozs7Ozs7ZUFvQmdCQSxLQUFELElBQVc7QUFDdEIsUUFBTUMsaUJBQWlCLEdBQUcsaURBQ3RCLG9EQURKO0FBRUEsUUFBTUMsV0FBVyxHQUFHLGtEQUFwQjtBQUVBLFFBQU1DLFlBQVksR0FDZCx5QkFBRyxvRUFDQSwrQkFESCxDQURKO0FBR0EsUUFBTUMsWUFBWSxHQUFHLHlCQUFHLHFDQUNwQix1RUFEb0IsR0FFcEIsMEVBRm9CLEdBR3BCLHVCQUhpQixFQUdRLEVBSFIsRUFJakI7QUFDSUMsSUFBQUEsa0JBQWtCLEVBQUdDLEdBQUQsSUFBUztBQUN6QixhQUFPO0FBQUcsUUFBQSxNQUFNLEVBQUMsUUFBVjtBQUFtQixRQUFBLEdBQUcsRUFBQyxxQkFBdkI7QUFBNkMsUUFBQSxJQUFJLEVBQUVMO0FBQW5ELFNBQXdFSyxHQUF4RSxDQUFQO0FBQ0gsS0FITDtBQUlJQyxJQUFBQSxZQUFZLEVBQUdELEdBQUQsSUFBUztBQUNuQixhQUFPO0FBQUcsUUFBQSxNQUFNLEVBQUMsUUFBVjtBQUFtQixRQUFBLEdBQUcsRUFBQyxxQkFBdkI7QUFBNkMsUUFBQSxJQUFJLEVBQUVKO0FBQW5ELFNBQWtFSSxHQUFsRSxDQUFQO0FBQ0g7QUFOTCxHQUppQixDQUFyQjtBQWFBLFNBQVEsNkJBQUMsdUJBQUQ7QUFDSixJQUFBLGVBQWUsRUFBRSxLQURiO0FBRUosSUFBQSxLQUFLLEVBQUUseUJBQUcsNkJBQUgsQ0FGSDtBQUdKLElBQUEsV0FBVyxFQUFFLDBDQUFLLHdDQUFJSCxZQUFKLENBQUwsRUFBMEIsd0NBQUlDLFlBQUosQ0FBMUIsQ0FIVDtBQUlKLElBQUEsTUFBTSxFQUFFLHlCQUFHLFNBQUgsQ0FKSjtBQUtKLElBQUEsVUFBVSxFQUFFSixLQUFLLENBQUNRO0FBTGQsSUFBUjtBQU9ILEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBRdWVzdGlvbkRpYWxvZyBmcm9tICcuL1F1ZXN0aW9uRGlhbG9nJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgKHByb3BzKSA9PiB7XHJcbiAgICBjb25zdCBleGlzdGluZ0lzc3Vlc1VybCA9IFwiaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXNcIiArXHJcbiAgICAgICAgXCI/cT1pcyUzQW9wZW4raXMlM0Fpc3N1ZStzb3J0JTNBcmVhY3Rpb25zLSUyQjEtZGVzY1wiO1xyXG4gICAgY29uc3QgbmV3SXNzdWVVcmwgPSBcImh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzL25ld1wiO1xyXG5cclxuICAgIGNvbnN0IGRlc2NyaXB0aW9uMSA9XHJcbiAgICAgICAgX3QoXCJJZiB5b3UgcnVuIGludG8gYW55IGJ1Z3Mgb3IgaGF2ZSBmZWVkYmFjayB5b3UnZCBsaWtlIHRvIHNoYXJlLCBcIiArXHJcbiAgICAgICAgICAgXCJwbGVhc2UgbGV0IHVzIGtub3cgb24gR2l0SHViLlwiKTtcclxuICAgIGNvbnN0IGRlc2NyaXB0aW9uMiA9IF90KFwiVG8gaGVscCBhdm9pZCBkdXBsaWNhdGUgaXNzdWVzLCBcIiArXHJcbiAgICAgICAgXCJwbGVhc2UgPGV4aXN0aW5nSXNzdWVzTGluaz52aWV3IGV4aXN0aW5nIGlzc3VlczwvZXhpc3RpbmdJc3N1ZXNMaW5rPiBcIiArXHJcbiAgICAgICAgXCJmaXJzdCAoYW5kIGFkZCBhICsxKSBvciA8bmV3SXNzdWVMaW5rPmNyZWF0ZSBhIG5ldyBpc3N1ZTwvbmV3SXNzdWVMaW5rPiBcIiArXHJcbiAgICAgICAgXCJpZiB5b3UgY2FuJ3QgZmluZCBpdC5cIiwge30sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBleGlzdGluZ0lzc3Vlc0xpbms6IChzdWIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiA8YSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgaHJlZj17ZXhpc3RpbmdJc3N1ZXNVcmx9Pnsgc3ViIH08L2E+O1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBuZXdJc3N1ZUxpbms6IChzdWIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiA8YSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgaHJlZj17bmV3SXNzdWVVcmx9Pnsgc3ViIH08L2E+O1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIHJldHVybiAoPFF1ZXN0aW9uRGlhbG9nXHJcbiAgICAgICAgaGFzQ2FuY2VsQnV0dG9uPXtmYWxzZX1cclxuICAgICAgICB0aXRsZT17X3QoXCJSZXBvcnQgYnVncyAmIGdpdmUgZmVlZGJhY2tcIil9XHJcbiAgICAgICAgZGVzY3JpcHRpb249ezxkaXY+PHA+e2Rlc2NyaXB0aW9uMX08L3A+PHA+e2Rlc2NyaXB0aW9uMn08L3A+PC9kaXY+fVxyXG4gICAgICAgIGJ1dHRvbj17X3QoXCJHbyBiYWNrXCIpfVxyXG4gICAgICAgIG9uRmluaXNoZWQ9e3Byb3BzLm9uRmluaXNoZWR9XHJcbiAgICAvPik7XHJcbn07XHJcbiJdfQ==