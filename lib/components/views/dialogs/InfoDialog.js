"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _classnames = _interopRequireDefault(require("classnames"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd.
Copyright 2019 Bastian Masanek, Noxware IT <matrix@noxware.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'InfoDialog',
  propTypes: {
    className: _propTypes.default.string,
    title: _propTypes.default.string,
    description: _propTypes.default.node,
    button: _propTypes.default.string,
    onFinished: _propTypes.default.func,
    hasCloseButton: _propTypes.default.bool,
    onKeyDown: _propTypes.default.func
  },
  getDefaultProps: function () {
    return {
      title: '',
      description: '',
      hasCloseButton: false
    };
  },
  onFinished: function () {
    this.props.onFinished();
  },
  render: function () {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement(BaseDialog, {
      className: "mx_InfoDialog",
      onFinished: this.props.onFinished,
      title: this.props.title,
      contentId: "mx_Dialog_content",
      hasCancel: this.props.hasCloseButton,
      onKeyDown: this.props.onKeyDown
    }, _react.default.createElement("div", {
      className: (0, _classnames.default)("mx_Dialog_content", this.props.className),
      id: "mx_Dialog_content"
    }, this.props.description), _react.default.createElement(DialogButtons, {
      primaryButton: this.props.button || (0, _languageHandler._t)('OK'),
      onPrimaryButtonClick: this.onFinished,
      hasCancel: false
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvSW5mb0RpYWxvZy5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsImNsYXNzTmFtZSIsIlByb3BUeXBlcyIsInN0cmluZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJub2RlIiwiYnV0dG9uIiwib25GaW5pc2hlZCIsImZ1bmMiLCJoYXNDbG9zZUJ1dHRvbiIsImJvb2wiLCJvbktleURvd24iLCJnZXREZWZhdWx0UHJvcHMiLCJwcm9wcyIsInJlbmRlciIsIkJhc2VEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJEaWFsb2dCdXR0b25zIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBeUJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLFlBRGU7QUFFNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxTQUFTLEVBQUVDLG1CQUFVQyxNQURkO0FBRVBDLElBQUFBLEtBQUssRUFBRUYsbUJBQVVDLE1BRlY7QUFHUEUsSUFBQUEsV0FBVyxFQUFFSCxtQkFBVUksSUFIaEI7QUFJUEMsSUFBQUEsTUFBTSxFQUFFTCxtQkFBVUMsTUFKWDtBQUtQSyxJQUFBQSxVQUFVLEVBQUVOLG1CQUFVTyxJQUxmO0FBTVBDLElBQUFBLGNBQWMsRUFBRVIsbUJBQVVTLElBTm5CO0FBT1BDLElBQUFBLFNBQVMsRUFBRVYsbUJBQVVPO0FBUGQsR0FGaUI7QUFZNUJJLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSFQsTUFBQUEsS0FBSyxFQUFFLEVBREo7QUFFSEMsTUFBQUEsV0FBVyxFQUFFLEVBRlY7QUFHSEssTUFBQUEsY0FBYyxFQUFFO0FBSGIsS0FBUDtBQUtILEdBbEIyQjtBQW9CNUJGLEVBQUFBLFVBQVUsRUFBRSxZQUFXO0FBQ25CLFNBQUtNLEtBQUwsQ0FBV04sVUFBWDtBQUNILEdBdEIyQjtBQXdCNUJPLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FDSSw2QkFBQyxVQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsZUFEZDtBQUVJLE1BQUEsVUFBVSxFQUFFLEtBQUtKLEtBQUwsQ0FBV04sVUFGM0I7QUFHSSxNQUFBLEtBQUssRUFBRSxLQUFLTSxLQUFMLENBQVdWLEtBSHRCO0FBSUksTUFBQSxTQUFTLEVBQUMsbUJBSmQ7QUFLSSxNQUFBLFNBQVMsRUFBRSxLQUFLVSxLQUFMLENBQVdKLGNBTDFCO0FBTUksTUFBQSxTQUFTLEVBQUUsS0FBS0ksS0FBTCxDQUFXRjtBQU4xQixPQVFJO0FBQUssTUFBQSxTQUFTLEVBQUUseUJBQVcsbUJBQVgsRUFBZ0MsS0FBS0UsS0FBTCxDQUFXYixTQUEzQyxDQUFoQjtBQUF1RSxNQUFBLEVBQUUsRUFBQztBQUExRSxPQUNNLEtBQUthLEtBQUwsQ0FBV1QsV0FEakIsQ0FSSixFQVdJLDZCQUFDLGFBQUQ7QUFBZSxNQUFBLGFBQWEsRUFBRSxLQUFLUyxLQUFMLENBQVdQLE1BQVgsSUFBcUIseUJBQUcsSUFBSCxDQUFuRDtBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS0MsVUFEL0I7QUFFSSxNQUFBLFNBQVMsRUFBRTtBQUZmLE1BWEosQ0FESjtBQW1CSDtBQTlDMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkLlxyXG5Db3B5cmlnaHQgMjAxOSBCYXN0aWFuIE1hc2FuZWssIE5veHdhcmUgSVQgPG1hdHJpeEBub3h3YXJlLmRlPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSBcImNsYXNzbmFtZXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdJbmZvRGlhbG9nJyxcclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogUHJvcFR5cGVzLm5vZGUsXHJcbiAgICAgICAgYnV0dG9uOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIGhhc0Nsb3NlQnV0dG9uOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBvbktleURvd246IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRpdGxlOiAnJyxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246ICcnLFxyXG4gICAgICAgICAgICBoYXNDbG9zZUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgb25GaW5pc2hlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2dcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0luZm9EaWFsb2dcIlxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9e3RoaXMucHJvcHMudGl0bGV9XHJcbiAgICAgICAgICAgICAgICBjb250ZW50SWQ9J214X0RpYWxvZ19jb250ZW50J1xyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0aGlzLnByb3BzLmhhc0Nsb3NlQnV0dG9ufVxyXG4gICAgICAgICAgICAgICAgb25LZXlEb3duPXt0aGlzLnByb3BzLm9uS2V5RG93bn1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoXCJteF9EaWFsb2dfY29udGVudFwiLCB0aGlzLnByb3BzLmNsYXNzTmFtZSl9IGlkPVwibXhfRGlhbG9nX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMucHJvcHMuZGVzY3JpcHRpb24gfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXt0aGlzLnByb3BzLmJ1dHRvbiB8fCBfdCgnT0snKX1cclxuICAgICAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8L0RpYWxvZ0J1dHRvbnM+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==