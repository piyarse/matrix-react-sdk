"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class StorageEvictedDialog extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_sendBugReport", ev => {
      ev.preventDefault();
      const BugReportDialog = sdk.getComponent("dialogs.BugReportDialog");

      _Modal.default.createTrackedDialog('Storage evicted', 'Send Bug Report Dialog', BugReportDialog, {});
    });
    (0, _defineProperty2.default)(this, "_onSignOutClick", () => {
      this.props.onFinished(true);
    });
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    let logRequest;

    if (_SdkConfig.default.get().bug_report_endpoint_url) {
      logRequest = (0, _languageHandler._t)("To help us prevent this in future, please <a>send us logs</a>.", {}, {
        a: text => _react.default.createElement("a", {
          href: "#",
          onClick: this._sendBugReport
        }, text)
      });
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_ErrorDialog",
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)('Missing session data'),
      contentId: "mx_Dialog_content",
      hasCancel: false
    }, _react.default.createElement("div", {
      className: "mx_Dialog_content",
      id: "mx_Dialog_content"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Some session data, including encrypted message keys, is " + "missing. Sign out and sign in to fix this, restoring keys " + "from backup.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Your browser likely removed this data when running low on " + "disk space."), " ", logRequest)), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Sign out"),
      onPrimaryButtonClick: this._onSignOutClick,
      focus: true,
      hasCancel: false
    }));
  }

}

exports.default = StorageEvictedDialog;
(0, _defineProperty2.default)(StorageEvictedDialog, "propTypes", {
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvU3RvcmFnZUV2aWN0ZWREaWFsb2cuanMiXSwibmFtZXMiOlsiU3RvcmFnZUV2aWN0ZWREaWFsb2ciLCJSZWFjdCIsIkNvbXBvbmVudCIsImV2IiwicHJldmVudERlZmF1bHQiLCJCdWdSZXBvcnREaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJyZW5kZXIiLCJCYXNlRGlhbG9nIiwiRGlhbG9nQnV0dG9ucyIsImxvZ1JlcXVlc3QiLCJTZGtDb25maWciLCJnZXQiLCJidWdfcmVwb3J0X2VuZHBvaW50X3VybCIsImEiLCJ0ZXh0IiwiX3NlbmRCdWdSZXBvcnQiLCJfb25TaWduT3V0Q2xpY2siLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQkE7Ozs7Ozs7Ozs7Ozs7OztBQXVCZSxNQUFNQSxvQkFBTixTQUFtQ0MsZUFBTUMsU0FBekMsQ0FBbUQ7QUFBQTtBQUFBO0FBQUEsMERBSzdDQyxFQUFFLElBQUk7QUFDbkJBLE1BQUFBLEVBQUUsQ0FBQ0MsY0FBSDtBQUNBLFlBQU1DLGVBQWUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlCQUFqQixDQUF4Qjs7QUFDQUMscUJBQU1DLG1CQUFOLENBQTBCLGlCQUExQixFQUE2Qyx3QkFBN0MsRUFBdUVKLGVBQXZFLEVBQXdGLEVBQXhGO0FBQ0gsS0FUNkQ7QUFBQSwyREFXNUMsTUFBTTtBQUNwQixXQUFLSyxLQUFMLENBQVdDLFVBQVgsQ0FBc0IsSUFBdEI7QUFDSCxLQWI2RDtBQUFBOztBQWU5REMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHUCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTU8sYUFBYSxHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBRUEsUUFBSVEsVUFBSjs7QUFDQSxRQUFJQyxtQkFBVUMsR0FBVixHQUFnQkMsdUJBQXBCLEVBQTZDO0FBQ3pDSCxNQUFBQSxVQUFVLEdBQUcseUJBQ1QsZ0VBRFMsRUFDeUQsRUFEekQsRUFFYjtBQUNJSSxRQUFBQSxDQUFDLEVBQUVDLElBQUksSUFBSTtBQUFHLFVBQUEsSUFBSSxFQUFDLEdBQVI7QUFBWSxVQUFBLE9BQU8sRUFBRSxLQUFLQztBQUExQixXQUEyQ0QsSUFBM0M7QUFEZixPQUZhLENBQWI7QUFLSDs7QUFFRCxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQyxnQkFBdEI7QUFBdUMsTUFBQSxVQUFVLEVBQUUsS0FBS1YsS0FBTCxDQUFXQyxVQUE5RDtBQUNJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLHNCQUFILENBRFg7QUFFSSxNQUFBLFNBQVMsRUFBQyxtQkFGZDtBQUdJLE1BQUEsU0FBUyxFQUFFO0FBSGYsT0FLSTtBQUFLLE1BQUEsU0FBUyxFQUFDLG1CQUFmO0FBQW1DLE1BQUEsRUFBRSxFQUFDO0FBQXRDLE9BQ0ksd0NBQUkseUJBQ0EsNkRBQ0EsNERBREEsR0FFQSxjQUhBLENBQUosQ0FESixFQU1JLHdDQUFJLHlCQUNBLCtEQUNBLGFBRkEsQ0FBSixPQUdJSSxVQUhKLENBTkosQ0FMSixFQWdCSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxhQUFhLEVBQUUseUJBQUcsVUFBSCxDQUE5QjtBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS08sZUFEL0I7QUFFSSxNQUFBLEtBQUssRUFBRSxJQUZYO0FBR0ksTUFBQSxTQUFTLEVBQUU7QUFIZixNQWhCSixDQURKO0FBd0JIOztBQXBENkQ7Ozs4QkFBN0N0QixvQixlQUNFO0FBQ2ZXLEVBQUFBLFVBQVUsRUFBRVksbUJBQVVDLElBQVYsQ0FBZUM7QUFEWixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gJy4uLy4uLy4uL1Nka0NvbmZpZyc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0b3JhZ2VFdmljdGVkRGlhbG9nIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgX3NlbmRCdWdSZXBvcnQgPSBldiA9PiB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBCdWdSZXBvcnREaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5CdWdSZXBvcnREaWFsb2dcIik7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU3RvcmFnZSBldmljdGVkJywgJ1NlbmQgQnVnIFJlcG9ydCBEaWFsb2cnLCBCdWdSZXBvcnREaWFsb2csIHt9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uU2lnbk91dENsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0cnVlKTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG5cclxuICAgICAgICBsZXQgbG9nUmVxdWVzdDtcclxuICAgICAgICBpZiAoU2RrQ29uZmlnLmdldCgpLmJ1Z19yZXBvcnRfZW5kcG9pbnRfdXJsKSB7XHJcbiAgICAgICAgICAgIGxvZ1JlcXVlc3QgPSBfdChcclxuICAgICAgICAgICAgICAgIFwiVG8gaGVscCB1cyBwcmV2ZW50IHRoaXMgaW4gZnV0dXJlLCBwbGVhc2UgPGE+c2VuZCB1cyBsb2dzPC9hPi5cIiwge30sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGE6IHRleHQgPT4gPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXt0aGlzLl9zZW5kQnVnUmVwb3J0fT57dGV4dH08L2E+LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nIGNsYXNzTmFtZT1cIm14X0Vycm9yRGlhbG9nXCIgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9e190KCdNaXNzaW5nIHNlc3Npb24gZGF0YScpfVxyXG4gICAgICAgICAgICAgICAgY29udGVudElkPSdteF9EaWFsb2dfY29udGVudCdcclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2NvbnRlbnRcIiBpZD0nbXhfRGlhbG9nX2NvbnRlbnQnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJTb21lIHNlc3Npb24gZGF0YSwgaW5jbHVkaW5nIGVuY3J5cHRlZCBtZXNzYWdlIGtleXMsIGlzIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJtaXNzaW5nLiBTaWduIG91dCBhbmQgc2lnbiBpbiB0byBmaXggdGhpcywgcmVzdG9yaW5nIGtleXMgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcImZyb20gYmFja3VwLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJZb3VyIGJyb3dzZXIgbGlrZWx5IHJlbW92ZWQgdGhpcyBkYXRhIHdoZW4gcnVubmluZyBsb3cgb24gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRpc2sgc3BhY2UuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKX0ge2xvZ1JlcXVlc3R9PC9wPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdChcIlNpZ24gb3V0XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vblNpZ25PdXRDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBmb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19