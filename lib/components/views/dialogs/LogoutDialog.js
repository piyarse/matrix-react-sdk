"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var sdk = _interopRequireWildcard3(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class LogoutDialog extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "defaultProps", {
      onFinished: function () {}
    });
    this._onSettingsLinkClick = this._onSettingsLinkClick.bind(this);
    this._onExportE2eKeysClicked = this._onExportE2eKeysClicked.bind(this);
    this._onFinished = this._onFinished.bind(this);
    this._onSetRecoveryMethodClick = this._onSetRecoveryMethodClick.bind(this);
    this._onLogoutConfirm = this._onLogoutConfirm.bind(this);

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const shouldLoadBackupStatus = cli.isCryptoEnabled() && !cli.getKeyBackupEnabled();
    this.state = {
      shouldLoadBackupStatus: shouldLoadBackupStatus,
      loading: shouldLoadBackupStatus,
      backupInfo: null,
      error: null
    };

    if (shouldLoadBackupStatus) {
      this._loadBackupStatus();
    }
  }

  async _loadBackupStatus() {
    try {
      const backupInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
      this.setState({
        loading: false,
        backupInfo
      });
    } catch (e) {
      console.log("Unable to fetch key backup status", e);
      this.setState({
        loading: false,
        error: e
      });
    }
  }

  _onSettingsLinkClick() {
    // close dialog
    this.props.onFinished();
  }

  _onExportE2eKeysClicked() {
    _Modal.default.createTrackedDialogAsync('Export E2E Keys', '', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../async-components/views/dialogs/ExportE2eKeysDialog'))), {
      matrixClient: _MatrixClientPeg.MatrixClientPeg.get()
    });
  }

  _onFinished(confirmed) {
    if (confirmed) {
      _dispatcher.default.dispatch({
        action: 'logout'
      });
    } // close dialog


    this.props.onFinished();
  }

  _onSetRecoveryMethodClick() {
    if (this.state.backupInfo) {
      // A key backup exists for this account, but the creating device is not
      // verified, so restore the backup which will give us the keys from it and
      // allow us to trust it (ie. upload keys to it)
      const RestoreKeyBackupDialog = sdk.getComponent('dialogs.keybackup.RestoreKeyBackupDialog');

      _Modal.default.createTrackedDialog('Restore Backup', '', RestoreKeyBackupDialog, null, null,
      /* priority = */
      false,
      /* static = */
      true);
    } else {
      _Modal.default.createTrackedDialogAsync("Key Backup", "Key Backup", Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require("../../../async-components/views/dialogs/keybackup/CreateKeyBackupDialog"))), null, null,
      /* priority = */
      false,
      /* static = */
      true);
    } // close dialog


    this.props.onFinished();
  }

  _onLogoutConfirm() {
    _dispatcher.default.dispatch({
      action: 'logout'
    }); // close dialog


    this.props.onFinished();
  }

  render() {
    if (this.state.shouldLoadBackupStatus) {
      const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');

      const description = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Encrypted messages are secured with end-to-end encryption. " + "Only you and the recipient(s) have the keys to read these messages.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Back up your keys before signing out to avoid losing them.")));

      let dialogContent;

      if (this.state.loading) {
        const Spinner = sdk.getComponent('views.elements.Spinner');
        dialogContent = _react.default.createElement(Spinner, null);
      } else {
        const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
        let setupButtonCaption;

        if (this.state.backupInfo) {
          setupButtonCaption = (0, _languageHandler._t)("Connect this session to Key Backup");
        } else {
          // if there's an error fetching the backup info, we'll just assume there's
          // no backup for the purpose of the button caption
          setupButtonCaption = (0, _languageHandler._t)("Start using Key Backup");
        }

        dialogContent = _react.default.createElement("div", null, _react.default.createElement("div", {
          className: "mx_Dialog_content",
          id: "mx_Dialog_content"
        }, description), _react.default.createElement(DialogButtons, {
          primaryButton: setupButtonCaption,
          hasCancel: false,
          onPrimaryButtonClick: this._onSetRecoveryMethodClick,
          focus: true
        }, _react.default.createElement("button", {
          onClick: this._onLogoutConfirm
        }, (0, _languageHandler._t)("I don't want my encrypted messages"))), _react.default.createElement("details", null, _react.default.createElement("summary", null, (0, _languageHandler._t)("Advanced")), _react.default.createElement("p", null, _react.default.createElement("button", {
          onClick: this._onExportE2eKeysClicked
        }, (0, _languageHandler._t)("Manually export keys")))));
      } // Not quite a standard question dialog as the primary button cancels
      // the action and does something else instead, whilst non-default button
      // confirms the action.


      return _react.default.createElement(BaseDialog, {
        title: (0, _languageHandler._t)("You'll lose access to your encrypted messages"),
        contentId: "mx_Dialog_content",
        hasCancel: true,
        onFinished: this._onFinished
      }, dialogContent);
    } else {
      const QuestionDialog = sdk.getComponent('views.dialogs.QuestionDialog');
      return _react.default.createElement(QuestionDialog, {
        hasCancelButton: true,
        title: (0, _languageHandler._t)("Sign out"),
        description: (0, _languageHandler._t)("Are you sure you want to sign out?"),
        button: (0, _languageHandler._t)("Sign out"),
        onFinished: this._onFinished
      });
    }
  }

}

exports.default = LogoutDialog;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvTG9nb3V0RGlhbG9nLmpzIl0sIm5hbWVzIjpbIkxvZ291dERpYWxvZyIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJvbkZpbmlzaGVkIiwiX29uU2V0dGluZ3NMaW5rQ2xpY2siLCJiaW5kIiwiX29uRXhwb3J0RTJlS2V5c0NsaWNrZWQiLCJfb25GaW5pc2hlZCIsIl9vblNldFJlY292ZXJ5TWV0aG9kQ2xpY2siLCJfb25Mb2dvdXRDb25maXJtIiwiY2xpIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwic2hvdWxkTG9hZEJhY2t1cFN0YXR1cyIsImlzQ3J5cHRvRW5hYmxlZCIsImdldEtleUJhY2t1cEVuYWJsZWQiLCJzdGF0ZSIsImxvYWRpbmciLCJiYWNrdXBJbmZvIiwiZXJyb3IiLCJfbG9hZEJhY2t1cFN0YXR1cyIsImdldEtleUJhY2t1cFZlcnNpb24iLCJzZXRTdGF0ZSIsImUiLCJjb25zb2xlIiwibG9nIiwicHJvcHMiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2dBc3luYyIsIm1hdHJpeENsaWVudCIsImNvbmZpcm1lZCIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwiUmVzdG9yZUtleUJhY2t1cERpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJyZW5kZXIiLCJCYXNlRGlhbG9nIiwiZGVzY3JpcHRpb24iLCJkaWFsb2dDb250ZW50IiwiU3Bpbm5lciIsIkRpYWxvZ0J1dHRvbnMiLCJzZXR1cEJ1dHRvbkNhcHRpb24iLCJRdWVzdGlvbkRpYWxvZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXdCZSxNQUFNQSxZQUFOLFNBQTJCQyxlQUFNQyxTQUFqQyxDQUEyQztBQUt0REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSx3REFKQztBQUNYQyxNQUFBQSxVQUFVLEVBQUUsWUFBVyxDQUFFO0FBRGQsS0FJRDtBQUVWLFNBQUtDLG9CQUFMLEdBQTRCLEtBQUtBLG9CQUFMLENBQTBCQyxJQUExQixDQUErQixJQUEvQixDQUE1QjtBQUNBLFNBQUtDLHVCQUFMLEdBQStCLEtBQUtBLHVCQUFMLENBQTZCRCxJQUE3QixDQUFrQyxJQUFsQyxDQUEvQjtBQUNBLFNBQUtFLFdBQUwsR0FBbUIsS0FBS0EsV0FBTCxDQUFpQkYsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbkI7QUFDQSxTQUFLRyx5QkFBTCxHQUFpQyxLQUFLQSx5QkFBTCxDQUErQkgsSUFBL0IsQ0FBb0MsSUFBcEMsQ0FBakM7QUFDQSxTQUFLSSxnQkFBTCxHQUF3QixLQUFLQSxnQkFBTCxDQUFzQkosSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBeEI7O0FBRUEsVUFBTUssR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsVUFBTUMsc0JBQXNCLEdBQUdILEdBQUcsQ0FBQ0ksZUFBSixNQUF5QixDQUFDSixHQUFHLENBQUNLLG1CQUFKLEVBQXpEO0FBRUEsU0FBS0MsS0FBTCxHQUFhO0FBQ1RILE1BQUFBLHNCQUFzQixFQUFFQSxzQkFEZjtBQUVUSSxNQUFBQSxPQUFPLEVBQUVKLHNCQUZBO0FBR1RLLE1BQUFBLFVBQVUsRUFBRSxJQUhIO0FBSVRDLE1BQUFBLEtBQUssRUFBRTtBQUpFLEtBQWI7O0FBT0EsUUFBSU4sc0JBQUosRUFBNEI7QUFDeEIsV0FBS08saUJBQUw7QUFDSDtBQUNKOztBQUVELFFBQU1BLGlCQUFOLEdBQTBCO0FBQ3RCLFFBQUk7QUFDQSxZQUFNRixVQUFVLEdBQUcsTUFBTVAsaUNBQWdCQyxHQUFoQixHQUFzQlMsbUJBQXRCLEVBQXpCO0FBQ0EsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZMLFFBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZDLFFBQUFBO0FBRlUsT0FBZDtBQUlILEtBTkQsQ0FNRSxPQUFPSyxDQUFQLEVBQVU7QUFDUkMsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksbUNBQVosRUFBaURGLENBQWpEO0FBQ0EsV0FBS0QsUUFBTCxDQUFjO0FBQ1ZMLFFBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZFLFFBQUFBLEtBQUssRUFBRUk7QUFGRyxPQUFkO0FBSUg7QUFDSjs7QUFFRG5CLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CO0FBQ0EsU0FBS3NCLEtBQUwsQ0FBV3ZCLFVBQVg7QUFDSDs7QUFFREcsRUFBQUEsdUJBQXVCLEdBQUc7QUFDdEJxQixtQkFBTUMsd0JBQU4sQ0FBK0IsaUJBQS9CLEVBQWtELEVBQWxELDZFQUNXLDZEQURYLEtBRUk7QUFDSUMsTUFBQUEsWUFBWSxFQUFFbEIsaUNBQWdCQyxHQUFoQjtBQURsQixLQUZKO0FBTUg7O0FBRURMLEVBQUFBLFdBQVcsQ0FBQ3VCLFNBQUQsRUFBWTtBQUNuQixRQUFJQSxTQUFKLEVBQWU7QUFDWEMsMEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiO0FBQ0gsS0FIa0IsQ0FJbkI7OztBQUNBLFNBQUtQLEtBQUwsQ0FBV3ZCLFVBQVg7QUFDSDs7QUFFREssRUFBQUEseUJBQXlCLEdBQUc7QUFDeEIsUUFBSSxLQUFLUSxLQUFMLENBQVdFLFVBQWYsRUFBMkI7QUFDdkI7QUFDQTtBQUNBO0FBQ0EsWUFBTWdCLHNCQUFzQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMENBQWpCLENBQS9COztBQUNBVCxxQkFBTVUsbUJBQU4sQ0FDSSxnQkFESixFQUNzQixFQUR0QixFQUMwQkgsc0JBRDFCLEVBQ2tELElBRGxELEVBQ3dELElBRHhEO0FBRUk7QUFBaUIsV0FGckI7QUFFNEI7QUFBZSxVQUYzQztBQUlILEtBVEQsTUFTTztBQUNIUCxxQkFBTUMsd0JBQU4sQ0FBK0IsWUFBL0IsRUFBNkMsWUFBN0MsNkVBQ1cseUVBRFgsS0FFSSxJQUZKLEVBRVUsSUFGVjtBQUVnQjtBQUFpQixXQUZqQztBQUV3QztBQUFlLFVBRnZEO0FBSUgsS0FmdUIsQ0FpQnhCOzs7QUFDQSxTQUFLRixLQUFMLENBQVd2QixVQUFYO0FBQ0g7O0FBRURNLEVBQUFBLGdCQUFnQixHQUFHO0FBQ2ZzQix3QkFBSUMsUUFBSixDQUFhO0FBQUNDLE1BQUFBLE1BQU0sRUFBRTtBQUFULEtBQWIsRUFEZSxDQUdmOzs7QUFDQSxTQUFLUCxLQUFMLENBQVd2QixVQUFYO0FBQ0g7O0FBRURtQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUt0QixLQUFMLENBQVdILHNCQUFmLEVBQXVDO0FBQ25DLFlBQU0wQixVQUFVLEdBQUdKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7O0FBRUEsWUFBTUksV0FBVyxHQUFHLDBDQUNoQix3Q0FBSSx5QkFDQSxnRUFDQSxxRUFGQSxDQUFKLENBRGdCLEVBS2hCLHdDQUFJLHlCQUFHLDREQUFILENBQUosQ0FMZ0IsQ0FBcEI7O0FBUUEsVUFBSUMsYUFBSjs7QUFDQSxVQUFJLEtBQUt6QixLQUFMLENBQVdDLE9BQWYsRUFBd0I7QUFDcEIsY0FBTXlCLE9BQU8sR0FBR1AsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUFoQjtBQUVBSyxRQUFBQSxhQUFhLEdBQUcsNkJBQUMsT0FBRCxPQUFoQjtBQUNILE9BSkQsTUFJTztBQUNILGNBQU1FLGFBQWEsR0FBR1IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFlBQUlRLGtCQUFKOztBQUNBLFlBQUksS0FBSzVCLEtBQUwsQ0FBV0UsVUFBZixFQUEyQjtBQUN2QjBCLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLG9DQUFILENBQXJCO0FBQ0gsU0FGRCxNQUVPO0FBQ0g7QUFDQTtBQUNBQSxVQUFBQSxrQkFBa0IsR0FBRyx5QkFBRyx3QkFBSCxDQUFyQjtBQUNIOztBQUVESCxRQUFBQSxhQUFhLEdBQUcsMENBQ1o7QUFBSyxVQUFBLFNBQVMsRUFBQyxtQkFBZjtBQUFtQyxVQUFBLEVBQUUsRUFBQztBQUF0QyxXQUNNRCxXQUROLENBRFksRUFJWiw2QkFBQyxhQUFEO0FBQWUsVUFBQSxhQUFhLEVBQUVJLGtCQUE5QjtBQUNJLFVBQUEsU0FBUyxFQUFFLEtBRGY7QUFFSSxVQUFBLG9CQUFvQixFQUFFLEtBQUtwQyx5QkFGL0I7QUFHSSxVQUFBLEtBQUssRUFBRTtBQUhYLFdBS0k7QUFBUSxVQUFBLE9BQU8sRUFBRSxLQUFLQztBQUF0QixXQUNLLHlCQUFHLG9DQUFILENBREwsQ0FMSixDQUpZLEVBYVosOENBQ0ksOENBQVUseUJBQUcsVUFBSCxDQUFWLENBREosRUFFSSx3Q0FBRztBQUFRLFVBQUEsT0FBTyxFQUFFLEtBQUtIO0FBQXRCLFdBQ0UseUJBQUcsc0JBQUgsQ0FERixDQUFILENBRkosQ0FiWSxDQUFoQjtBQW9CSCxPQS9Da0MsQ0FnRG5DO0FBQ0E7QUFDQTs7O0FBQ0EsYUFBUSw2QkFBQyxVQUFEO0FBQ0osUUFBQSxLQUFLLEVBQUUseUJBQUcsK0NBQUgsQ0FESDtBQUVKLFFBQUEsU0FBUyxFQUFDLG1CQUZOO0FBR0osUUFBQSxTQUFTLEVBQUUsSUFIUDtBQUlKLFFBQUEsVUFBVSxFQUFFLEtBQUtDO0FBSmIsU0FNSGtDLGFBTkcsQ0FBUjtBQVFILEtBM0RELE1BMkRPO0FBQ0gsWUFBTUksY0FBYyxHQUFHVixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXZCO0FBQ0EsYUFBUSw2QkFBQyxjQUFEO0FBQ0osUUFBQSxlQUFlLEVBQUUsSUFEYjtBQUVKLFFBQUEsS0FBSyxFQUFFLHlCQUFHLFVBQUgsQ0FGSDtBQUdKLFFBQUEsV0FBVyxFQUFFLHlCQUNULG9DQURTLENBSFQ7QUFNSixRQUFBLE1BQU0sRUFBRSx5QkFBRyxVQUFILENBTko7QUFPSixRQUFBLFVBQVUsRUFBRSxLQUFLN0I7QUFQYixRQUFSO0FBU0g7QUFDSjs7QUF0S3FEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTG9nb3V0RGlhbG9nIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICBvbkZpbmlzaGVkOiBmdW5jdGlvbigpIHt9LFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuX29uU2V0dGluZ3NMaW5rQ2xpY2sgPSB0aGlzLl9vblNldHRpbmdzTGlua0NsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25FeHBvcnRFMmVLZXlzQ2xpY2tlZCA9IHRoaXMuX29uRXhwb3J0RTJlS2V5c0NsaWNrZWQuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbkZpbmlzaGVkID0gdGhpcy5fb25GaW5pc2hlZC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uU2V0UmVjb3ZlcnlNZXRob2RDbGljayA9IHRoaXMuX29uU2V0UmVjb3ZlcnlNZXRob2RDbGljay5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uTG9nb3V0Q29uZmlybSA9IHRoaXMuX29uTG9nb3V0Q29uZmlybS5iaW5kKHRoaXMpO1xyXG5cclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3Qgc2hvdWxkTG9hZEJhY2t1cFN0YXR1cyA9IGNsaS5pc0NyeXB0b0VuYWJsZWQoKSAmJiAhY2xpLmdldEtleUJhY2t1cEVuYWJsZWQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgc2hvdWxkTG9hZEJhY2t1cFN0YXR1czogc2hvdWxkTG9hZEJhY2t1cFN0YXR1cyxcclxuICAgICAgICAgICAgbG9hZGluZzogc2hvdWxkTG9hZEJhY2t1cFN0YXR1cyxcclxuICAgICAgICAgICAgYmFja3VwSW5mbzogbnVsbCxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKHNob3VsZExvYWRCYWNrdXBTdGF0dXMpIHtcclxuICAgICAgICAgICAgdGhpcy5fbG9hZEJhY2t1cFN0YXR1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfbG9hZEJhY2t1cFN0YXR1cygpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBiYWNrdXBJbmZvID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldEtleUJhY2t1cFZlcnNpb24oKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZmV0Y2gga2V5IGJhY2t1cCBzdGF0dXNcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vblNldHRpbmdzTGlua0NsaWNrKCkge1xyXG4gICAgICAgIC8vIGNsb3NlIGRpYWxvZ1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkV4cG9ydEUyZUtleXNDbGlja2VkKCkge1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2dBc3luYygnRXhwb3J0IEUyRSBLZXlzJywgJycsXHJcbiAgICAgICAgICAgIGltcG9ydCgnLi4vLi4vLi4vYXN5bmMtY29tcG9uZW50cy92aWV3cy9kaWFsb2dzL0V4cG9ydEUyZUtleXNEaWFsb2cnKSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbWF0cml4Q2xpZW50OiBNYXRyaXhDbGllbnRQZWcuZ2V0KCksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25GaW5pc2hlZChjb25maXJtZWQpIHtcclxuICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnbG9nb3V0J30pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBjbG9zZSBkaWFsb2dcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25TZXRSZWNvdmVyeU1ldGhvZENsaWNrKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmJhY2t1cEluZm8pIHtcclxuICAgICAgICAgICAgLy8gQSBrZXkgYmFja3VwIGV4aXN0cyBmb3IgdGhpcyBhY2NvdW50LCBidXQgdGhlIGNyZWF0aW5nIGRldmljZSBpcyBub3RcclxuICAgICAgICAgICAgLy8gdmVyaWZpZWQsIHNvIHJlc3RvcmUgdGhlIGJhY2t1cCB3aGljaCB3aWxsIGdpdmUgdXMgdGhlIGtleXMgZnJvbSBpdCBhbmRcclxuICAgICAgICAgICAgLy8gYWxsb3cgdXMgdG8gdHJ1c3QgaXQgKGllLiB1cGxvYWQga2V5cyB0byBpdClcclxuICAgICAgICAgICAgY29uc3QgUmVzdG9yZUtleUJhY2t1cERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3Mua2V5YmFja3VwLlJlc3RvcmVLZXlCYWNrdXBEaWFsb2cnKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgICAgICdSZXN0b3JlIEJhY2t1cCcsICcnLCBSZXN0b3JlS2V5QmFja3VwRGlhbG9nLCBudWxsLCBudWxsLFxyXG4gICAgICAgICAgICAgICAgLyogcHJpb3JpdHkgPSAqLyBmYWxzZSwgLyogc3RhdGljID0gKi8gdHJ1ZSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMoXCJLZXkgQmFja3VwXCIsIFwiS2V5IEJhY2t1cFwiLFxyXG4gICAgICAgICAgICAgICAgaW1wb3J0KFwiLi4vLi4vLi4vYXN5bmMtY29tcG9uZW50cy92aWV3cy9kaWFsb2dzL2tleWJhY2t1cC9DcmVhdGVLZXlCYWNrdXBEaWFsb2dcIiksXHJcbiAgICAgICAgICAgICAgICBudWxsLCBudWxsLCAvKiBwcmlvcml0eSA9ICovIGZhbHNlLCAvKiBzdGF0aWMgPSAqLyB0cnVlLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY2xvc2UgZGlhbG9nXHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uTG9nb3V0Q29uZmlybSgpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2xvZ291dCd9KTtcclxuXHJcbiAgICAgICAgLy8gY2xvc2UgZGlhbG9nXHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNob3VsZExvYWRCYWNrdXBTdGF0dXMpIHtcclxuICAgICAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZGVzY3JpcHRpb24gPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiRW5jcnlwdGVkIG1lc3NhZ2VzIGFyZSBzZWN1cmVkIHdpdGggZW5kLXRvLWVuZCBlbmNyeXB0aW9uLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJPbmx5IHlvdSBhbmQgdGhlIHJlY2lwaWVudChzKSBoYXZlIHRoZSBrZXlzIHRvIHJlYWQgdGhlc2UgbWVzc2FnZXMuXCIsXHJcbiAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcIkJhY2sgdXAgeW91ciBrZXlzIGJlZm9yZSBzaWduaW5nIG91dCB0byBhdm9pZCBsb3NpbmcgdGhlbS5cIil9PC9wPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcblxyXG4gICAgICAgICAgICBsZXQgZGlhbG9nQ29udGVudDtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUubG9hZGluZykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLlNwaW5uZXInKTtcclxuXHJcbiAgICAgICAgICAgICAgICBkaWFsb2dDb250ZW50ID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHNldHVwQnV0dG9uQ2FwdGlvbjtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmJhY2t1cEluZm8pIHtcclxuICAgICAgICAgICAgICAgICAgICBzZXR1cEJ1dHRvbkNhcHRpb24gPSBfdChcIkNvbm5lY3QgdGhpcyBzZXNzaW9uIHRvIEtleSBCYWNrdXBcIik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZXJlJ3MgYW4gZXJyb3IgZmV0Y2hpbmcgdGhlIGJhY2t1cCBpbmZvLCB3ZSdsbCBqdXN0IGFzc3VtZSB0aGVyZSdzXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbm8gYmFja3VwIGZvciB0aGUgcHVycG9zZSBvZiB0aGUgYnV0dG9uIGNhcHRpb25cclxuICAgICAgICAgICAgICAgICAgICBzZXR1cEJ1dHRvbkNhcHRpb24gPSBfdChcIlN0YXJ0IHVzaW5nIEtleSBCYWNrdXBcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgZGlhbG9nQ29udGVudCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfY29udGVudFwiIGlkPSdteF9EaWFsb2dfY29udGVudCc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgZGVzY3JpcHRpb24gfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e3NldHVwQnV0dG9uQ2FwdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uU2V0UmVjb3ZlcnlNZXRob2RDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uTG9nb3V0Q29uZmlybX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJJIGRvbid0IHdhbnQgbXkgZW5jcnlwdGVkIG1lc3NhZ2VzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L0RpYWxvZ0J1dHRvbnM+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRldGFpbHM+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzdW1tYXJ5PntfdChcIkFkdmFuY2VkXCIpfTwvc3VtbWFyeT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHA+PGJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkV4cG9ydEUyZUtleXNDbGlja2VkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIk1hbnVhbGx5IGV4cG9ydCBrZXlzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kZXRhaWxzPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIE5vdCBxdWl0ZSBhIHN0YW5kYXJkIHF1ZXN0aW9uIGRpYWxvZyBhcyB0aGUgcHJpbWFyeSBidXR0b24gY2FuY2Vsc1xyXG4gICAgICAgICAgICAvLyB0aGUgYWN0aW9uIGFuZCBkb2VzIHNvbWV0aGluZyBlbHNlIGluc3RlYWQsIHdoaWxzdCBub24tZGVmYXVsdCBidXR0b25cclxuICAgICAgICAgICAgLy8gY29uZmlybXMgdGhlIGFjdGlvbi5cclxuICAgICAgICAgICAgcmV0dXJuICg8QmFzZURpYWxvZ1xyXG4gICAgICAgICAgICAgICAgdGl0bGU9e190KFwiWW91J2xsIGxvc2UgYWNjZXNzIHRvIHlvdXIgZW5jcnlwdGVkIG1lc3NhZ2VzXCIpfVxyXG4gICAgICAgICAgICAgICAgY29udGVudElkPSdteF9EaWFsb2dfY29udGVudCdcclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMuX29uRmluaXNoZWR9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtkaWFsb2dDb250ZW50fVxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuUXVlc3Rpb25EaWFsb2cnKTtcclxuICAgICAgICAgICAgcmV0dXJuICg8UXVlc3Rpb25EaWFsb2dcclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbEJ1dHRvbj17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlNpZ24gb3V0XCIpfVxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb249e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIHNpZ24gb3V0P1wiLFxyXG4gICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIGJ1dHRvbj17X3QoXCJTaWduIG91dFwiKX1cclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMuX29uRmluaXNoZWR9XHJcbiAgICAgICAgICAgIC8+KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19