"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2017 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * A dialog for confirming a redaction.
 */
var _default = (0, _createReactClass.default)({
  displayName: 'ConfirmRedactDialog',
  render: function () {
    const QuestionDialog = sdk.getComponent('views.dialogs.QuestionDialog');
    return _react.default.createElement(QuestionDialog, {
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)("Confirm Removal"),
      description: (0, _languageHandler._t)("Are you sure you wish to remove (delete) this event? " + "Note that if you delete a room name or topic change, it could undo the change."),
      button: (0, _languageHandler._t)("Remove")
    });
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvQ29uZmlybVJlZGFjdERpYWxvZy5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsInJlbmRlciIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwicHJvcHMiLCJvbkZpbmlzaGVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFuQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkE7OztlQUdlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLHFCQURlO0FBRzVCQyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF2QjtBQUNBLFdBQ0ksNkJBQUMsY0FBRDtBQUFnQixNQUFBLFVBQVUsRUFBRSxLQUFLQyxLQUFMLENBQVdDLFVBQXZDO0FBQ0ksTUFBQSxLQUFLLEVBQUUseUJBQUcsaUJBQUgsQ0FEWDtBQUVJLE1BQUEsV0FBVyxFQUNQLHlCQUFHLDBEQUNBLGdGQURILENBSFI7QUFLSSxNQUFBLE1BQU0sRUFBRSx5QkFBRyxRQUFIO0FBTFosTUFESjtBQVNIO0FBZDJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuXHJcbi8qXHJcbiAqIEEgZGlhbG9nIGZvciBjb25maXJtaW5nIGEgcmVkYWN0aW9uLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0NvbmZpcm1SZWRhY3REaWFsb2cnLFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLlF1ZXN0aW9uRGlhbG9nJyk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFF1ZXN0aW9uRGlhbG9nIG9uRmluaXNoZWQ9e3RoaXMucHJvcHMub25GaW5pc2hlZH1cclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIkNvbmZpcm0gUmVtb3ZhbFwiKX1cclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uPXtcclxuICAgICAgICAgICAgICAgICAgICBfdChcIkFyZSB5b3Ugc3VyZSB5b3Ugd2lzaCB0byByZW1vdmUgKGRlbGV0ZSkgdGhpcyBldmVudD8gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgIFwiTm90ZSB0aGF0IGlmIHlvdSBkZWxldGUgYSByb29tIG5hbWUgb3IgdG9waWMgY2hhbmdlLCBpdCBjb3VsZCB1bmRvIHRoZSBjaGFuZ2UuXCIpfVxyXG4gICAgICAgICAgICAgICAgYnV0dG9uPXtfdChcIlJlbW92ZVwiKX0+XHJcbiAgICAgICAgICAgIDwvUXVlc3Rpb25EaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=