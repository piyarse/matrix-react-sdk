"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var FormattingUtils = _interopRequireWildcard(require("../../../utils/FormattingUtils"));

var _languageHandler = require("../../../languageHandler");

var _crypto = require("matrix-js-sdk/src/crypto");

var _createRoom = require("../../../createRoom");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _QRCode = require("matrix-js-sdk/src/crypto/verification/QRCode");

var _VerificationQREmojiOptions = _interopRequireDefault(require("../verification/VerificationQREmojiOptions"));

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const MODE_LEGACY = 'legacy';
const MODE_SAS = 'sas';
const PHASE_START = 0;
const PHASE_WAIT_FOR_PARTNER_TO_ACCEPT = 1;
const PHASE_PICK_VERIFICATION_OPTION = 2;
const PHASE_SHOW_SAS = 3;
const PHASE_WAIT_FOR_PARTNER_TO_CONFIRM = 4;
const PHASE_VERIFIED = 5;
const PHASE_CANCELLED = 6;

class DeviceVerifyDialog extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onSwitchToLegacyClick", () => {
      if (this._verifier) {
        this._verifier.removeListener('show_sas', this._onVerifierShowSas);

        this._verifier.cancel('User cancel');

        this._verifier = null;
      }

      this.setState({
        mode: MODE_LEGACY
      });
    });
    (0, _defineProperty2.default)(this, "_onSwitchToSasClick", () => {
      this.setState({
        mode: MODE_SAS
      });
    });
    (0, _defineProperty2.default)(this, "_onCancelClick", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onUseSasClick", async () => {
      try {
        this._verifier = this._request.beginKeyVerification(_crypto.verificationMethods.SAS);

        this._verifier.on('show_sas', this._onVerifierShowSas); // throws upon cancellation


        await this._verifier.verify();
        this.setState({
          phase: PHASE_VERIFIED
        });

        this._verifier.removeListener('show_sas', this._onVerifierShowSas);

        this._verifier = null;
      } catch (e) {
        console.log("Verification failed", e);
        this.setState({
          phase: PHASE_CANCELLED
        });
        this._verifier = null;
        this._request = null;
      }
    });
    (0, _defineProperty2.default)(this, "_onLegacyFinished", confirm => {
      if (confirm) {
        _MatrixClientPeg.MatrixClientPeg.get().setDeviceVerified(this.props.userId, this.props.device.deviceId, true);
      }

      this.props.onFinished(confirm);
    });
    (0, _defineProperty2.default)(this, "_onSasRequestClick", async () => {
      this.setState({
        phase: PHASE_WAIT_FOR_PARTNER_TO_ACCEPT
      });

      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const verifyingOwnDevice = this.props.userId === client.getUserId();

      try {
        if (!verifyingOwnDevice && _SettingsStore.default.getValue("feature_cross_signing")) {
          const roomId = await ensureDMExistsAndOpen(this.props.userId); // throws upon cancellation before having started

          const request = await client.requestVerificationDM(this.props.userId, roomId);
          await request.waitFor(r => r.ready || r.started);

          if (request.ready) {
            this._verifier = request.beginKeyVerification(_crypto.verificationMethods.SAS);
          } else {
            this._verifier = request.verifier;
          }
        } else if (verifyingOwnDevice && _SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
          this._request = await client.requestVerification(this.props.userId, [_crypto.verificationMethods.SAS, _QRCode.SHOW_QR_CODE_METHOD, _crypto.verificationMethods.RECIPROCATE_QR_CODE]);
          await this._request.waitFor(r => r.ready || r.started);
          this.setState({
            phase: PHASE_PICK_VERIFICATION_OPTION
          });
        } else {
          this._verifier = client.beginKeyVerification(_crypto.verificationMethods.SAS, this.props.userId, this.props.device.deviceId);
        }

        if (!this._verifier) return;

        this._verifier.on('show_sas', this._onVerifierShowSas); // throws upon cancellation


        await this._verifier.verify();
        this.setState({
          phase: PHASE_VERIFIED
        });

        this._verifier.removeListener('show_sas', this._onVerifierShowSas);

        this._verifier = null;
      } catch (e) {
        console.log("Verification failed", e);
        this.setState({
          phase: PHASE_CANCELLED
        });
        this._verifier = null;
      }
    });
    (0, _defineProperty2.default)(this, "_onSasMatchesClick", () => {
      this._showSasEvent.confirm();

      this.setState({
        phase: PHASE_WAIT_FOR_PARTNER_TO_CONFIRM
      });
    });
    (0, _defineProperty2.default)(this, "_onVerifiedDoneClick", () => {
      this.props.onFinished(true);
    });
    (0, _defineProperty2.default)(this, "_onVerifierShowSas", e => {
      this._showSasEvent = e;
      this.setState({
        phase: PHASE_SHOW_SAS
      });
    });
    this._verifier = null;
    this._showSasEvent = null;
    this._request = null;
    this.state = {
      phase: PHASE_START,
      mode: MODE_SAS,
      sasVerified: false
    };
  }

  componentWillUnmount() {
    if (this._verifier) {
      this._verifier.removeListener('show_sas', this._onVerifierShowSas);

      this._verifier.cancel('User cancel');
    }
  }

  _renderSasVerification() {
    let body;

    switch (this.state.phase) {
      case PHASE_START:
        body = this._renderVerificationPhaseStart();
        break;

      case PHASE_WAIT_FOR_PARTNER_TO_ACCEPT:
        body = this._renderVerificationPhaseWaitAccept();
        break;

      case PHASE_PICK_VERIFICATION_OPTION:
        body = this._renderVerificationPhasePick();
        break;

      case PHASE_SHOW_SAS:
        body = this._renderSasVerificationPhaseShowSas();
        break;

      case PHASE_WAIT_FOR_PARTNER_TO_CONFIRM:
        body = this._renderSasVerificationPhaseWaitForPartnerToConfirm();
        break;

      case PHASE_VERIFIED:
        body = this._renderVerificationPhaseVerified();
        break;

      case PHASE_CANCELLED:
        body = this._renderVerificationPhaseCancelled();
        break;
    }

    const BaseDialog = sdk.getComponent("dialogs.BaseDialog");
    return _react.default.createElement(BaseDialog, {
      title: (0, _languageHandler._t)("Verify session"),
      onFinished: this._onCancelClick
    }, body);
  }

  _renderVerificationPhaseStart() {
    const AccessibleButton = sdk.getComponent('views.elements.AccessibleButton');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, _react.default.createElement(AccessibleButton, {
      element: "span",
      className: "mx_linkButton",
      onClick: this._onSwitchToLegacyClick
    }, (0, _languageHandler._t)("Use Legacy Verification (for older clients)")), _react.default.createElement("p", null, (0, _languageHandler._t)("Verify by comparing a short text string.")), _react.default.createElement("p", null, (0, _languageHandler._t)("To be secure, do this in person or use a trusted way to communicate.")), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Begin Verifying'),
      hasCancel: true,
      onPrimaryButtonClick: this._onSasRequestClick,
      onCancel: this._onCancelClick
    }));
  }

  _renderVerificationPhaseWaitAccept() {
    const Spinner = sdk.getComponent("views.elements.Spinner");
    const AccessibleButton = sdk.getComponent('views.elements.AccessibleButton');
    return _react.default.createElement("div", null, _react.default.createElement(Spinner, null), _react.default.createElement("p", null, (0, _languageHandler._t)("Waiting for partner to accept...")), _react.default.createElement("p", null, (0, _languageHandler._t)("Nothing appearing? Not all clients support interactive verification yet. " + "<button>Use legacy verification</button>.", {}, {
      button: sub => _react.default.createElement(AccessibleButton, {
        element: "span",
        className: "mx_linkButton",
        onClick: this._onSwitchToLegacyClick
      }, sub)
    })));
  }

  _renderVerificationPhasePick() {
    return _react.default.createElement(_VerificationQREmojiOptions.default, {
      request: this._request,
      onCancel: this._onCancelClick,
      onStartEmoji: this._onUseSasClick
    });
  }

  _renderSasVerificationPhaseShowSas() {
    const VerificationShowSas = sdk.getComponent('views.verification.VerificationShowSas');
    return _react.default.createElement(VerificationShowSas, {
      sas: this._showSasEvent.sas,
      onCancel: this._onCancelClick,
      onDone: this._onSasMatchesClick,
      isSelf: _MatrixClientPeg.MatrixClientPeg.get().getUserId() === this.props.userId,
      onStartEmoji: this._onUseSasClick,
      inDialog: true
    });
  }

  _renderSasVerificationPhaseWaitForPartnerToConfirm() {
    const Spinner = sdk.getComponent('views.elements.Spinner');
    return _react.default.createElement("div", null, _react.default.createElement(Spinner, null), _react.default.createElement("p", null, (0, _languageHandler._t)("Waiting for %(userId)s to confirm...", {
      userId: this.props.userId
    })));
  }

  _renderVerificationPhaseVerified() {
    const VerificationComplete = sdk.getComponent('views.verification.VerificationComplete');
    return _react.default.createElement(VerificationComplete, {
      onDone: this._onVerifiedDoneClick
    });
  }

  _renderVerificationPhaseCancelled() {
    const VerificationCancelled = sdk.getComponent('views.verification.VerificationCancelled');
    return _react.default.createElement(VerificationCancelled, {
      onDone: this._onCancelClick
    });
  }

  _renderLegacyVerification() {
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
    const AccessibleButton = sdk.getComponent('views.elements.AccessibleButton');
    let text;

    if (_MatrixClientPeg.MatrixClientPeg.get().getUserId() === this.props.userId) {
      text = (0, _languageHandler._t)("To verify that this session can be trusted, please check that the key you see " + "in User Settings on that device matches the key below:");
    } else {
      text = (0, _languageHandler._t)("To verify that this session can be trusted, please contact its owner using some other " + "means (e.g. in person or a phone call) and ask them whether the key they see in their User Settings " + "for this session matches the key below:");
    }

    const key = FormattingUtils.formatCryptoKey(this.props.device.getFingerprint());

    const body = _react.default.createElement("div", null, _react.default.createElement(AccessibleButton, {
      element: "span",
      className: "mx_linkButton",
      onClick: this._onSwitchToSasClick
    }, (0, _languageHandler._t)("Use two-way text verification")), _react.default.createElement("p", null, text), _react.default.createElement("div", {
      className: "mx_DeviceVerifyDialog_cryptoSection"
    }, _react.default.createElement("ul", null, _react.default.createElement("li", null, _react.default.createElement("label", null, (0, _languageHandler._t)("Session name"), ":"), " ", _react.default.createElement("span", null, this.props.device.getDisplayName())), _react.default.createElement("li", null, _react.default.createElement("label", null, (0, _languageHandler._t)("Session ID"), ":"), " ", _react.default.createElement("span", null, _react.default.createElement("code", null, this.props.device.deviceId))), _react.default.createElement("li", null, _react.default.createElement("label", null, (0, _languageHandler._t)("Session key"), ":"), " ", _react.default.createElement("span", null, _react.default.createElement("code", null, _react.default.createElement("b", null, key)))))), _react.default.createElement("p", null, (0, _languageHandler._t)("If it matches, press the verify button below. " + "If it doesn't, then someone else is intercepting this session " + "and you probably want to press the blacklist button instead.")));

    return _react.default.createElement(QuestionDialog, {
      title: (0, _languageHandler._t)("Verify session"),
      description: body,
      button: (0, _languageHandler._t)("I verify that the keys match"),
      onFinished: this._onLegacyFinished
    });
  }

  render() {
    if (this.state.mode === MODE_LEGACY) {
      return this._renderLegacyVerification();
    } else {
      return _react.default.createElement("div", null, this._renderSasVerification());
    }
  }

}

exports.default = DeviceVerifyDialog;
(0, _defineProperty2.default)(DeviceVerifyDialog, "propTypes", {
  userId: _propTypes.default.string.isRequired,
  device: _propTypes.default.object.isRequired,
  onFinished: _propTypes.default.func.isRequired
});

async function ensureDMExistsAndOpen(userId) {
  const roomId = await (0, _createRoom.ensureDMExists)(_MatrixClientPeg.MatrixClientPeg.get(), userId); // don't use andView and spinner in createRoom, together, they cause this dialog to close and reopen,
  // we causes us to loose the verifier and restart, and we end up having two verification requests

  _dispatcher.default.dispatch({
    action: 'view_room',
    room_id: roomId,
    should_peek: false
  });

  return roomId;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvRGV2aWNlVmVyaWZ5RGlhbG9nLmpzIl0sIm5hbWVzIjpbIk1PREVfTEVHQUNZIiwiTU9ERV9TQVMiLCJQSEFTRV9TVEFSVCIsIlBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQUNDRVBUIiwiUEhBU0VfUElDS19WRVJJRklDQVRJT05fT1BUSU9OIiwiUEhBU0VfU0hPV19TQVMiLCJQSEFTRV9XQUlUX0ZPUl9QQVJUTkVSX1RPX0NPTkZJUk0iLCJQSEFTRV9WRVJJRklFRCIsIlBIQVNFX0NBTkNFTExFRCIsIkRldmljZVZlcmlmeURpYWxvZyIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJfdmVyaWZpZXIiLCJyZW1vdmVMaXN0ZW5lciIsIl9vblZlcmlmaWVyU2hvd1NhcyIsImNhbmNlbCIsInNldFN0YXRlIiwibW9kZSIsInByb3BzIiwib25GaW5pc2hlZCIsIl9yZXF1ZXN0IiwiYmVnaW5LZXlWZXJpZmljYXRpb24iLCJ2ZXJpZmljYXRpb25NZXRob2RzIiwiU0FTIiwib24iLCJ2ZXJpZnkiLCJwaGFzZSIsImUiLCJjb25zb2xlIiwibG9nIiwiY29uZmlybSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInNldERldmljZVZlcmlmaWVkIiwidXNlcklkIiwiZGV2aWNlIiwiZGV2aWNlSWQiLCJjbGllbnQiLCJ2ZXJpZnlpbmdPd25EZXZpY2UiLCJnZXRVc2VySWQiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJyb29tSWQiLCJlbnN1cmVETUV4aXN0c0FuZE9wZW4iLCJyZXF1ZXN0IiwicmVxdWVzdFZlcmlmaWNhdGlvbkRNIiwid2FpdEZvciIsInIiLCJyZWFkeSIsInN0YXJ0ZWQiLCJ2ZXJpZmllciIsImlzRmVhdHVyZUVuYWJsZWQiLCJyZXF1ZXN0VmVyaWZpY2F0aW9uIiwiU0hPV19RUl9DT0RFX01FVEhPRCIsIlJFQ0lQUk9DQVRFX1FSX0NPREUiLCJfc2hvd1Nhc0V2ZW50Iiwic3RhdGUiLCJzYXNWZXJpZmllZCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwiX3JlbmRlclNhc1ZlcmlmaWNhdGlvbiIsImJvZHkiLCJfcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VTdGFydCIsIl9yZW5kZXJWZXJpZmljYXRpb25QaGFzZVdhaXRBY2NlcHQiLCJfcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VQaWNrIiwiX3JlbmRlclNhc1ZlcmlmaWNhdGlvblBoYXNlU2hvd1NhcyIsIl9yZW5kZXJTYXNWZXJpZmljYXRpb25QaGFzZVdhaXRGb3JQYXJ0bmVyVG9Db25maXJtIiwiX3JlbmRlclZlcmlmaWNhdGlvblBoYXNlVmVyaWZpZWQiLCJfcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VDYW5jZWxsZWQiLCJCYXNlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiX29uQ2FuY2VsQ2xpY2siLCJBY2Nlc3NpYmxlQnV0dG9uIiwiRGlhbG9nQnV0dG9ucyIsIl9vblN3aXRjaFRvTGVnYWN5Q2xpY2siLCJfb25TYXNSZXF1ZXN0Q2xpY2siLCJTcGlubmVyIiwiYnV0dG9uIiwic3ViIiwiX29uVXNlU2FzQ2xpY2siLCJWZXJpZmljYXRpb25TaG93U2FzIiwic2FzIiwiX29uU2FzTWF0Y2hlc0NsaWNrIiwiVmVyaWZpY2F0aW9uQ29tcGxldGUiLCJfb25WZXJpZmllZERvbmVDbGljayIsIlZlcmlmaWNhdGlvbkNhbmNlbGxlZCIsIl9yZW5kZXJMZWdhY3lWZXJpZmljYXRpb24iLCJRdWVzdGlvbkRpYWxvZyIsInRleHQiLCJrZXkiLCJGb3JtYXR0aW5nVXRpbHMiLCJmb3JtYXRDcnlwdG9LZXkiLCJnZXRGaW5nZXJwcmludCIsIl9vblN3aXRjaFRvU2FzQ2xpY2siLCJnZXREaXNwbGF5TmFtZSIsIl9vbkxlZ2FjeUZpbmlzaGVkIiwicmVuZGVyIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm9iamVjdCIsImZ1bmMiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJvb21faWQiLCJzaG91bGRfcGVlayJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWdDQSxNQUFNQSxXQUFXLEdBQUcsUUFBcEI7QUFDQSxNQUFNQyxRQUFRLEdBQUcsS0FBakI7QUFFQSxNQUFNQyxXQUFXLEdBQUcsQ0FBcEI7QUFDQSxNQUFNQyxnQ0FBZ0MsR0FBRyxDQUF6QztBQUNBLE1BQU1DLDhCQUE4QixHQUFHLENBQXZDO0FBQ0EsTUFBTUMsY0FBYyxHQUFHLENBQXZCO0FBQ0EsTUFBTUMsaUNBQWlDLEdBQUcsQ0FBMUM7QUFDQSxNQUFNQyxjQUFjLEdBQUcsQ0FBdkI7QUFDQSxNQUFNQyxlQUFlLEdBQUcsQ0FBeEI7O0FBRWUsTUFBTUMsa0JBQU4sU0FBaUNDLGVBQU1DLFNBQXZDLENBQWlEO0FBTzVEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLGtFQW1CVyxNQUFNO0FBQzNCLFVBQUksS0FBS0MsU0FBVCxFQUFvQjtBQUNoQixhQUFLQSxTQUFMLENBQWVDLGNBQWYsQ0FBOEIsVUFBOUIsRUFBMEMsS0FBS0Msa0JBQS9DOztBQUNBLGFBQUtGLFNBQUwsQ0FBZUcsTUFBZixDQUFzQixhQUF0Qjs7QUFDQSxhQUFLSCxTQUFMLEdBQWlCLElBQWpCO0FBQ0g7O0FBQ0QsV0FBS0ksUUFBTCxDQUFjO0FBQUNDLFFBQUFBLElBQUksRUFBRWxCO0FBQVAsT0FBZDtBQUNILEtBMUJhO0FBQUEsK0RBNEJRLE1BQU07QUFDeEIsV0FBS2lCLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxJQUFJLEVBQUVqQjtBQUFQLE9BQWQ7QUFDSCxLQTlCYTtBQUFBLDBEQWdDRyxNQUFNO0FBQ25CLFdBQUtrQixLQUFMLENBQVdDLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQWxDYTtBQUFBLDBEQW9DRyxZQUFZO0FBQ3pCLFVBQUk7QUFDQSxhQUFLUCxTQUFMLEdBQWlCLEtBQUtRLFFBQUwsQ0FBY0Msb0JBQWQsQ0FBbUNDLDRCQUFvQkMsR0FBdkQsQ0FBakI7O0FBQ0EsYUFBS1gsU0FBTCxDQUFlWSxFQUFmLENBQWtCLFVBQWxCLEVBQThCLEtBQUtWLGtCQUFuQyxFQUZBLENBR0E7OztBQUNBLGNBQU0sS0FBS0YsU0FBTCxDQUFlYSxNQUFmLEVBQU47QUFDQSxhQUFLVCxRQUFMLENBQWM7QUFBQ1UsVUFBQUEsS0FBSyxFQUFFcEI7QUFBUixTQUFkOztBQUNBLGFBQUtNLFNBQUwsQ0FBZUMsY0FBZixDQUE4QixVQUE5QixFQUEwQyxLQUFLQyxrQkFBL0M7O0FBQ0EsYUFBS0YsU0FBTCxHQUFpQixJQUFqQjtBQUNILE9BUkQsQ0FRRSxPQUFPZSxDQUFQLEVBQVU7QUFDUkMsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkscUJBQVosRUFBbUNGLENBQW5DO0FBQ0EsYUFBS1gsUUFBTCxDQUFjO0FBQ1ZVLFVBQUFBLEtBQUssRUFBRW5CO0FBREcsU0FBZDtBQUdBLGFBQUtLLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxhQUFLUSxRQUFMLEdBQWdCLElBQWhCO0FBQ0g7QUFDSixLQXJEYTtBQUFBLDZEQXVET1UsT0FBRCxJQUFhO0FBQzdCLFVBQUlBLE9BQUosRUFBYTtBQUNUQyx5Q0FBZ0JDLEdBQWhCLEdBQXNCQyxpQkFBdEIsQ0FDSSxLQUFLZixLQUFMLENBQVdnQixNQURmLEVBQ3VCLEtBQUtoQixLQUFMLENBQVdpQixNQUFYLENBQWtCQyxRQUR6QyxFQUNtRCxJQURuRDtBQUdIOztBQUNELFdBQUtsQixLQUFMLENBQVdDLFVBQVgsQ0FBc0JXLE9BQXRCO0FBQ0gsS0E5RGE7QUFBQSw4REFnRU8sWUFBWTtBQUM3QixXQUFLZCxRQUFMLENBQWM7QUFDVlUsUUFBQUEsS0FBSyxFQUFFeEI7QUFERyxPQUFkOztBQUdBLFlBQU1tQyxNQUFNLEdBQUdOLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxZQUFNTSxrQkFBa0IsR0FBRyxLQUFLcEIsS0FBTCxDQUFXZ0IsTUFBWCxLQUFzQkcsTUFBTSxDQUFDRSxTQUFQLEVBQWpEOztBQUNBLFVBQUk7QUFDQSxZQUFJLENBQUNELGtCQUFELElBQXVCRSx1QkFBY0MsUUFBZCxDQUF1Qix1QkFBdkIsQ0FBM0IsRUFBNEU7QUFDeEUsZ0JBQU1DLE1BQU0sR0FBRyxNQUFNQyxxQkFBcUIsQ0FBQyxLQUFLekIsS0FBTCxDQUFXZ0IsTUFBWixDQUExQyxDQUR3RSxDQUV4RTs7QUFDQSxnQkFBTVUsT0FBTyxHQUFHLE1BQU1QLE1BQU0sQ0FBQ1EscUJBQVAsQ0FDbEIsS0FBSzNCLEtBQUwsQ0FBV2dCLE1BRE8sRUFDQ1EsTUFERCxDQUF0QjtBQUdBLGdCQUFNRSxPQUFPLENBQUNFLE9BQVIsQ0FBZ0JDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFGLElBQVdELENBQUMsQ0FBQ0UsT0FBbEMsQ0FBTjs7QUFDQSxjQUFJTCxPQUFPLENBQUNJLEtBQVosRUFBbUI7QUFDZixpQkFBS3BDLFNBQUwsR0FBaUJnQyxPQUFPLENBQUN2QixvQkFBUixDQUE2QkMsNEJBQW9CQyxHQUFqRCxDQUFqQjtBQUNILFdBRkQsTUFFTztBQUNILGlCQUFLWCxTQUFMLEdBQWlCZ0MsT0FBTyxDQUFDTSxRQUF6QjtBQUNIO0FBQ0osU0FaRCxNQVlPLElBQUlaLGtCQUFrQixJQUFJRSx1QkFBY1csZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQTFCLEVBQW1GO0FBQ3RGLGVBQUsvQixRQUFMLEdBQWdCLE1BQU1pQixNQUFNLENBQUNlLG1CQUFQLENBQTJCLEtBQUtsQyxLQUFMLENBQVdnQixNQUF0QyxFQUE4QyxDQUNoRVosNEJBQW9CQyxHQUQ0QyxFQUVoRThCLDJCQUZnRSxFQUdoRS9CLDRCQUFvQmdDLG1CQUg0QyxDQUE5QyxDQUF0QjtBQU1BLGdCQUFNLEtBQUtsQyxRQUFMLENBQWMwQixPQUFkLENBQXNCQyxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixJQUFXRCxDQUFDLENBQUNFLE9BQXhDLENBQU47QUFDQSxlQUFLakMsUUFBTCxDQUFjO0FBQUNVLFlBQUFBLEtBQUssRUFBRXZCO0FBQVIsV0FBZDtBQUNILFNBVE0sTUFTQTtBQUNILGVBQUtTLFNBQUwsR0FBaUJ5QixNQUFNLENBQUNoQixvQkFBUCxDQUNiQyw0QkFBb0JDLEdBRFAsRUFDWSxLQUFLTCxLQUFMLENBQVdnQixNQUR2QixFQUMrQixLQUFLaEIsS0FBTCxDQUFXaUIsTUFBWCxDQUFrQkMsUUFEakQsQ0FBakI7QUFHSDs7QUFDRCxZQUFJLENBQUMsS0FBS3hCLFNBQVYsRUFBcUI7O0FBQ3JCLGFBQUtBLFNBQUwsQ0FBZVksRUFBZixDQUFrQixVQUFsQixFQUE4QixLQUFLVixrQkFBbkMsRUE1QkEsQ0E2QkE7OztBQUNBLGNBQU0sS0FBS0YsU0FBTCxDQUFlYSxNQUFmLEVBQU47QUFDQSxhQUFLVCxRQUFMLENBQWM7QUFBQ1UsVUFBQUEsS0FBSyxFQUFFcEI7QUFBUixTQUFkOztBQUNBLGFBQUtNLFNBQUwsQ0FBZUMsY0FBZixDQUE4QixVQUE5QixFQUEwQyxLQUFLQyxrQkFBL0M7O0FBQ0EsYUFBS0YsU0FBTCxHQUFpQixJQUFqQjtBQUNILE9BbENELENBa0NFLE9BQU9lLENBQVAsRUFBVTtBQUNSQyxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxxQkFBWixFQUFtQ0YsQ0FBbkM7QUFDQSxhQUFLWCxRQUFMLENBQWM7QUFDVlUsVUFBQUEsS0FBSyxFQUFFbkI7QUFERyxTQUFkO0FBR0EsYUFBS0ssU0FBTCxHQUFpQixJQUFqQjtBQUNIO0FBQ0osS0EvR2E7QUFBQSw4REFpSE8sTUFBTTtBQUN2QixXQUFLMkMsYUFBTCxDQUFtQnpCLE9BQW5COztBQUNBLFdBQUtkLFFBQUwsQ0FBYztBQUNWVSxRQUFBQSxLQUFLLEVBQUVyQjtBQURHLE9BQWQ7QUFHSCxLQXRIYTtBQUFBLGdFQXdIUyxNQUFNO0FBQ3pCLFdBQUthLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixJQUF0QjtBQUNILEtBMUhhO0FBQUEsOERBNEhRUSxDQUFELElBQU87QUFDeEIsV0FBSzRCLGFBQUwsR0FBcUI1QixDQUFyQjtBQUNBLFdBQUtYLFFBQUwsQ0FBYztBQUNWVSxRQUFBQSxLQUFLLEVBQUV0QjtBQURHLE9BQWQ7QUFHSCxLQWpJYTtBQUVWLFNBQUtRLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLMkMsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFNBQUtuQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsU0FBS29DLEtBQUwsR0FBYTtBQUNUOUIsTUFBQUEsS0FBSyxFQUFFekIsV0FERTtBQUVUZ0IsTUFBQUEsSUFBSSxFQUFFakIsUUFGRztBQUdUeUQsTUFBQUEsV0FBVyxFQUFFO0FBSEosS0FBYjtBQUtIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixRQUFJLEtBQUs5QyxTQUFULEVBQW9CO0FBQ2hCLFdBQUtBLFNBQUwsQ0FBZUMsY0FBZixDQUE4QixVQUE5QixFQUEwQyxLQUFLQyxrQkFBL0M7O0FBQ0EsV0FBS0YsU0FBTCxDQUFlRyxNQUFmLENBQXNCLGFBQXRCO0FBQ0g7QUFDSjs7QUFrSEQ0QyxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQixRQUFJQyxJQUFKOztBQUNBLFlBQVEsS0FBS0osS0FBTCxDQUFXOUIsS0FBbkI7QUFDSSxXQUFLekIsV0FBTDtBQUNJMkQsUUFBQUEsSUFBSSxHQUFHLEtBQUtDLDZCQUFMLEVBQVA7QUFDQTs7QUFDSixXQUFLM0QsZ0NBQUw7QUFDSTBELFFBQUFBLElBQUksR0FBRyxLQUFLRSxrQ0FBTCxFQUFQO0FBQ0E7O0FBQ0osV0FBSzNELDhCQUFMO0FBQ0l5RCxRQUFBQSxJQUFJLEdBQUcsS0FBS0csNEJBQUwsRUFBUDtBQUNBOztBQUNKLFdBQUszRCxjQUFMO0FBQ0l3RCxRQUFBQSxJQUFJLEdBQUcsS0FBS0ksa0NBQUwsRUFBUDtBQUNBOztBQUNKLFdBQUszRCxpQ0FBTDtBQUNJdUQsUUFBQUEsSUFBSSxHQUFHLEtBQUtLLGtEQUFMLEVBQVA7QUFDQTs7QUFDSixXQUFLM0QsY0FBTDtBQUNJc0QsUUFBQUEsSUFBSSxHQUFHLEtBQUtNLGdDQUFMLEVBQVA7QUFDQTs7QUFDSixXQUFLM0QsZUFBTDtBQUNJcUQsUUFBQUEsSUFBSSxHQUFHLEtBQUtPLGlDQUFMLEVBQVA7QUFDQTtBQXJCUjs7QUF3QkEsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsb0JBQWpCLENBQW5CO0FBQ0EsV0FDSSw2QkFBQyxVQUFEO0FBQ0ksTUFBQSxLQUFLLEVBQUUseUJBQUcsZ0JBQUgsQ0FEWDtBQUVJLE1BQUEsVUFBVSxFQUFFLEtBQUtDO0FBRnJCLE9BSUtYLElBSkwsQ0FESjtBQVFIOztBQUVEQyxFQUFBQSw2QkFBNkIsR0FBRztBQUM1QixVQUFNVyxnQkFBZ0IsR0FBR0gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUF6QjtBQUNBLFVBQU1HLGFBQWEsR0FBR0osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFdBQ0ksMENBQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBQyxNQURaO0FBQ21CLE1BQUEsU0FBUyxFQUFDLGVBRDdCO0FBQzZDLE1BQUEsT0FBTyxFQUFFLEtBQUtJO0FBRDNELE9BR0sseUJBQUcsNkNBQUgsQ0FITCxDQURKLEVBTUksd0NBQ00seUJBQUcsMENBQUgsQ0FETixDQU5KLEVBU0ksd0NBQ0sseUJBQUcsc0VBQUgsQ0FETCxDQVRKLEVBWUksNkJBQUMsYUFBRDtBQUNJLE1BQUEsYUFBYSxFQUFFLHlCQUFHLGlCQUFILENBRG5CO0FBRUksTUFBQSxTQUFTLEVBQUUsSUFGZjtBQUdJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS0Msa0JBSC9CO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS0o7QUFKbkIsTUFaSixDQURKO0FBcUJIOztBQUVEVCxFQUFBQSxrQ0FBa0MsR0FBRztBQUNqQyxVQUFNYyxPQUFPLEdBQUdQLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBaEI7QUFDQSxVQUFNRSxnQkFBZ0IsR0FBR0gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUF6QjtBQUVBLFdBQ0ksMENBQ0ksNkJBQUMsT0FBRCxPQURKLEVBRUksd0NBQUkseUJBQUcsa0NBQUgsQ0FBSixDQUZKLEVBR0ksd0NBQUkseUJBQ0EsOEVBQ0EsMkNBRkEsRUFHQSxFQUhBLEVBR0k7QUFBQ08sTUFBQUEsTUFBTSxFQUFFQyxHQUFHLElBQUksNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUMsTUFBMUI7QUFBaUMsUUFBQSxTQUFTLEVBQUMsZUFBM0M7QUFDaEIsUUFBQSxPQUFPLEVBQUUsS0FBS0o7QUFERSxTQUdmSSxHQUhlO0FBQWhCLEtBSEosQ0FBSixDQUhKLENBREo7QUFlSDs7QUFFRGYsRUFBQUEsNEJBQTRCLEdBQUc7QUFDM0IsV0FBTyw2QkFBQyxtQ0FBRDtBQUNILE1BQUEsT0FBTyxFQUFFLEtBQUszQyxRQURYO0FBRUgsTUFBQSxRQUFRLEVBQUUsS0FBS21ELGNBRlo7QUFHSCxNQUFBLFlBQVksRUFBRSxLQUFLUTtBQUhoQixNQUFQO0FBS0g7O0FBRURmLEVBQUFBLGtDQUFrQyxHQUFHO0FBQ2pDLFVBQU1nQixtQkFBbUIsR0FBR1gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdDQUFqQixDQUE1QjtBQUNBLFdBQU8sNkJBQUMsbUJBQUQ7QUFDSCxNQUFBLEdBQUcsRUFBRSxLQUFLZixhQUFMLENBQW1CMEIsR0FEckI7QUFFSCxNQUFBLFFBQVEsRUFBRSxLQUFLVixjQUZaO0FBR0gsTUFBQSxNQUFNLEVBQUUsS0FBS1csa0JBSFY7QUFJSCxNQUFBLE1BQU0sRUFBRW5ELGlDQUFnQkMsR0FBaEIsR0FBc0JPLFNBQXRCLE9BQXNDLEtBQUtyQixLQUFMLENBQVdnQixNQUp0RDtBQUtILE1BQUEsWUFBWSxFQUFFLEtBQUs2QyxjQUxoQjtBQU1ILE1BQUEsUUFBUSxFQUFFO0FBTlAsTUFBUDtBQVFIOztBQUVEZCxFQUFBQSxrREFBa0QsR0FBRztBQUNqRCxVQUFNVyxPQUFPLEdBQUdQLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBaEI7QUFDQSxXQUFPLDBDQUNILDZCQUFDLE9BQUQsT0FERyxFQUVILHdDQUFJLHlCQUNBLHNDQURBLEVBQ3dDO0FBQUNwQyxNQUFBQSxNQUFNLEVBQUUsS0FBS2hCLEtBQUwsQ0FBV2dCO0FBQXBCLEtBRHhDLENBQUosQ0FGRyxDQUFQO0FBTUg7O0FBRURnQyxFQUFBQSxnQ0FBZ0MsR0FBRztBQUMvQixVQUFNaUIsb0JBQW9CLEdBQUdkLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5Q0FBakIsQ0FBN0I7QUFDQSxXQUFPLDZCQUFDLG9CQUFEO0FBQXNCLE1BQUEsTUFBTSxFQUFFLEtBQUtjO0FBQW5DLE1BQVA7QUFDSDs7QUFFRGpCLEVBQUFBLGlDQUFpQyxHQUFHO0FBQ2hDLFVBQU1rQixxQkFBcUIsR0FBR2hCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQ0FBakIsQ0FBOUI7QUFDQSxXQUFPLDZCQUFDLHFCQUFEO0FBQXVCLE1BQUEsTUFBTSxFQUFFLEtBQUtDO0FBQXBDLE1BQVA7QUFDSDs7QUFFRGUsRUFBQUEseUJBQXlCLEdBQUc7QUFDeEIsVUFBTUMsY0FBYyxHQUFHbEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2QjtBQUNBLFVBQU1FLGdCQUFnQixHQUFHSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQXpCO0FBRUEsUUFBSWtCLElBQUo7O0FBQ0EsUUFBSXpELGlDQUFnQkMsR0FBaEIsR0FBc0JPLFNBQXRCLE9BQXNDLEtBQUtyQixLQUFMLENBQVdnQixNQUFyRCxFQUE2RDtBQUN6RHNELE1BQUFBLElBQUksR0FBRyx5QkFBRyxtRkFDTix3REFERyxDQUFQO0FBRUgsS0FIRCxNQUdPO0FBQ0hBLE1BQUFBLElBQUksR0FBRyx5QkFBRywyRkFDTixzR0FETSxHQUVOLHlDQUZHLENBQVA7QUFHSDs7QUFFRCxVQUFNQyxHQUFHLEdBQUdDLGVBQWUsQ0FBQ0MsZUFBaEIsQ0FBZ0MsS0FBS3pFLEtBQUwsQ0FBV2lCLE1BQVgsQ0FBa0J5RCxjQUFsQixFQUFoQyxDQUFaOztBQUNBLFVBQU1oQyxJQUFJLEdBQ04sMENBQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBQyxNQURaO0FBQ21CLE1BQUEsU0FBUyxFQUFDLGVBRDdCO0FBQzZDLE1BQUEsT0FBTyxFQUFFLEtBQUtpQztBQUQzRCxPQUdLLHlCQUFHLCtCQUFILENBSEwsQ0FESixFQU1JLHdDQUNNTCxJQUROLENBTkosRUFTSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx5Q0FDSSx5Q0FBSSw0Q0FBUyx5QkFBRyxjQUFILENBQVQsTUFBSixPQUEyQywyQ0FBUSxLQUFLdEUsS0FBTCxDQUFXaUIsTUFBWCxDQUFrQjJELGNBQWxCLEVBQVIsQ0FBM0MsQ0FESixFQUVJLHlDQUFJLDRDQUFTLHlCQUFHLFlBQUgsQ0FBVCxNQUFKLE9BQXlDLDJDQUFNLDJDQUFRLEtBQUs1RSxLQUFMLENBQVdpQixNQUFYLENBQWtCQyxRQUExQixDQUFOLENBQXpDLENBRkosRUFHSSx5Q0FBSSw0Q0FBUyx5QkFBRyxhQUFILENBQVQsTUFBSixPQUEwQywyQ0FBTSwyQ0FBTSx3Q0FBS3FELEdBQUwsQ0FBTixDQUFOLENBQTFDLENBSEosQ0FESixDQVRKLEVBZ0JJLHdDQUNNLHlCQUFHLG1EQUNELGdFQURDLEdBRUQsOERBRkYsQ0FETixDQWhCSixDQURKOztBQXlCQSxXQUNJLDZCQUFDLGNBQUQ7QUFDSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxnQkFBSCxDQURYO0FBRUksTUFBQSxXQUFXLEVBQUU3QixJQUZqQjtBQUdJLE1BQUEsTUFBTSxFQUFFLHlCQUFHLDhCQUFILENBSFo7QUFJSSxNQUFBLFVBQVUsRUFBRSxLQUFLbUM7QUFKckIsTUFESjtBQVFIOztBQUVEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUt4QyxLQUFMLENBQVd2QyxJQUFYLEtBQW9CbEIsV0FBeEIsRUFBcUM7QUFDakMsYUFBTyxLQUFLdUYseUJBQUwsRUFBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sMENBQ0YsS0FBSzNCLHNCQUFMLEVBREUsQ0FBUDtBQUdIO0FBQ0o7O0FBaFUyRDs7OzhCQUEzQ25ELGtCLGVBQ0U7QUFDZjBCLEVBQUFBLE1BQU0sRUFBRStELG1CQUFVQyxNQUFWLENBQWlCQyxVQURWO0FBRWZoRSxFQUFBQSxNQUFNLEVBQUU4RCxtQkFBVUcsTUFBVixDQUFpQkQsVUFGVjtBQUdmaEYsRUFBQUEsVUFBVSxFQUFFOEUsbUJBQVVJLElBQVYsQ0FBZUY7QUFIWixDOztBQWtVdkIsZUFBZXhELHFCQUFmLENBQXFDVCxNQUFyQyxFQUE2QztBQUN6QyxRQUFNUSxNQUFNLEdBQUcsTUFBTSxnQ0FBZVgsaUNBQWdCQyxHQUFoQixFQUFmLEVBQXNDRSxNQUF0QyxDQUFyQixDQUR5QyxDQUV6QztBQUNBOztBQUNBb0Usc0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxJQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxJQUFBQSxPQUFPLEVBQUUvRCxNQUZBO0FBR1RnRSxJQUFBQSxXQUFXLEVBQUU7QUFISixHQUFiOztBQUtBLFNBQU9oRSxNQUFQO0FBQ0giLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0ICogYXMgRm9ybWF0dGluZ1V0aWxzIGZyb20gJy4uLy4uLy4uL3V0aWxzL0Zvcm1hdHRpbmdVdGlscyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHt2ZXJpZmljYXRpb25NZXRob2RzfSBmcm9tICdtYXRyaXgtanMtc2RrL3NyYy9jcnlwdG8nO1xyXG5pbXBvcnQge2Vuc3VyZURNRXhpc3RzfSBmcm9tIFwiLi4vLi4vLi4vY3JlYXRlUm9vbVwiO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gJy4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmUnO1xyXG5pbXBvcnQge1NIT1dfUVJfQ09ERV9NRVRIT0R9IGZyb20gXCJtYXRyaXgtanMtc2RrL3NyYy9jcnlwdG8vdmVyaWZpY2F0aW9uL1FSQ29kZVwiO1xyXG5pbXBvcnQgVmVyaWZpY2F0aW9uUVJFbW9qaU9wdGlvbnMgZnJvbSBcIi4uL3ZlcmlmaWNhdGlvbi9WZXJpZmljYXRpb25RUkVtb2ppT3B0aW9uc1wiO1xyXG5cclxuY29uc3QgTU9ERV9MRUdBQ1kgPSAnbGVnYWN5JztcclxuY29uc3QgTU9ERV9TQVMgPSAnc2FzJztcclxuXHJcbmNvbnN0IFBIQVNFX1NUQVJUID0gMDtcclxuY29uc3QgUEhBU0VfV0FJVF9GT1JfUEFSVE5FUl9UT19BQ0NFUFQgPSAxO1xyXG5jb25zdCBQSEFTRV9QSUNLX1ZFUklGSUNBVElPTl9PUFRJT04gPSAyO1xyXG5jb25zdCBQSEFTRV9TSE9XX1NBUyA9IDM7XHJcbmNvbnN0IFBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQ09ORklSTSA9IDQ7XHJcbmNvbnN0IFBIQVNFX1ZFUklGSUVEID0gNTtcclxuY29uc3QgUEhBU0VfQ0FOQ0VMTEVEID0gNjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERldmljZVZlcmlmeURpYWxvZyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHVzZXJJZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGRldmljZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5fdmVyaWZpZXIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3Nob3dTYXNFdmVudCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fcmVxdWVzdCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NUQVJULFxyXG4gICAgICAgICAgICBtb2RlOiBNT0RFX1NBUyxcclxuICAgICAgICAgICAgc2FzVmVyaWZpZWQ6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3ZlcmlmaWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyLnJlbW92ZUxpc3RlbmVyKCdzaG93X3NhcycsIHRoaXMuX29uVmVyaWZpZXJTaG93U2FzKTtcclxuICAgICAgICAgICAgdGhpcy5fdmVyaWZpZXIuY2FuY2VsKCdVc2VyIGNhbmNlbCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25Td2l0Y2hUb0xlZ2FjeUNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLl92ZXJpZmllcikge1xyXG4gICAgICAgICAgICB0aGlzLl92ZXJpZmllci5yZW1vdmVMaXN0ZW5lcignc2hvd19zYXMnLCB0aGlzLl9vblZlcmlmaWVyU2hvd1Nhcyk7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyLmNhbmNlbCgnVXNlciBjYW5jZWwnKTtcclxuICAgICAgICAgICAgdGhpcy5fdmVyaWZpZXIgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHttb2RlOiBNT0RFX0xFR0FDWX0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblN3aXRjaFRvU2FzQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bW9kZTogTU9ERV9TQVN9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25DYW5jZWxDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblVzZVNhc0NsaWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyID0gdGhpcy5fcmVxdWVzdC5iZWdpbktleVZlcmlmaWNhdGlvbih2ZXJpZmljYXRpb25NZXRob2RzLlNBUyk7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyLm9uKCdzaG93X3NhcycsIHRoaXMuX29uVmVyaWZpZXJTaG93U2FzKTtcclxuICAgICAgICAgICAgLy8gdGhyb3dzIHVwb24gY2FuY2VsbGF0aW9uXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuX3ZlcmlmaWVyLnZlcmlmeSgpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfVkVSSUZJRUR9KTtcclxuICAgICAgICAgICAgdGhpcy5fdmVyaWZpZXIucmVtb3ZlTGlzdGVuZXIoJ3Nob3dfc2FzJywgdGhpcy5fb25WZXJpZmllclNob3dTYXMpO1xyXG4gICAgICAgICAgICB0aGlzLl92ZXJpZmllciA9IG51bGw7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlZlcmlmaWNhdGlvbiBmYWlsZWRcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0NBTkNFTExFRCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5fcmVxdWVzdCA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfb25MZWdhY3lGaW5pc2hlZCA9IChjb25maXJtKSA9PiB7XHJcbiAgICAgICAgaWYgKGNvbmZpcm0pIHtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldERldmljZVZlcmlmaWVkKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy51c2VySWQsIHRoaXMucHJvcHMuZGV2aWNlLmRldmljZUlkLCB0cnVlLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoY29uZmlybSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2FzUmVxdWVzdENsaWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfV0FJVF9GT1JfUEFSVE5FUl9UT19BQ0NFUFQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHZlcmlmeWluZ093bkRldmljZSA9IHRoaXMucHJvcHMudXNlcklkID09PSBjbGllbnQuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKCF2ZXJpZnlpbmdPd25EZXZpY2UgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbUlkID0gYXdhaXQgZW5zdXJlRE1FeGlzdHNBbmRPcGVuKHRoaXMucHJvcHMudXNlcklkKTtcclxuICAgICAgICAgICAgICAgIC8vIHRocm93cyB1cG9uIGNhbmNlbGxhdGlvbiBiZWZvcmUgaGF2aW5nIHN0YXJ0ZWRcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlcXVlc3QgPSBhd2FpdCBjbGllbnQucmVxdWVzdFZlcmlmaWNhdGlvbkRNKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMudXNlcklkLCByb29tSWQsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgcmVxdWVzdC53YWl0Rm9yKHIgPT4gci5yZWFkeSB8fCByLnN0YXJ0ZWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlcXVlc3QucmVhZHkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl92ZXJpZmllciA9IHJlcXVlc3QuYmVnaW5LZXlWZXJpZmljYXRpb24odmVyaWZpY2F0aW9uTWV0aG9kcy5TQVMpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl92ZXJpZmllciA9IHJlcXVlc3QudmVyaWZpZXI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodmVyaWZ5aW5nT3duRGV2aWNlICYmIFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcmVxdWVzdCA9IGF3YWl0IGNsaWVudC5yZXF1ZXN0VmVyaWZpY2F0aW9uKHRoaXMucHJvcHMudXNlcklkLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgdmVyaWZpY2F0aW9uTWV0aG9kcy5TQVMsXHJcbiAgICAgICAgICAgICAgICAgICAgU0hPV19RUl9DT0RFX01FVEhPRCxcclxuICAgICAgICAgICAgICAgICAgICB2ZXJpZmljYXRpb25NZXRob2RzLlJFQ0lQUk9DQVRFX1FSX0NPREUsXHJcbiAgICAgICAgICAgICAgICBdKTtcclxuXHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9yZXF1ZXN0LndhaXRGb3IociA9PiByLnJlYWR5IHx8IHIuc3RhcnRlZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfUElDS19WRVJJRklDQVRJT05fT1BUSU9OfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92ZXJpZmllciA9IGNsaWVudC5iZWdpbktleVZlcmlmaWNhdGlvbihcclxuICAgICAgICAgICAgICAgICAgICB2ZXJpZmljYXRpb25NZXRob2RzLlNBUywgdGhpcy5wcm9wcy51c2VySWQsIHRoaXMucHJvcHMuZGV2aWNlLmRldmljZUlkLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIXRoaXMuX3ZlcmlmaWVyKSByZXR1cm47XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyLm9uKCdzaG93X3NhcycsIHRoaXMuX29uVmVyaWZpZXJTaG93U2FzKTtcclxuICAgICAgICAgICAgLy8gdGhyb3dzIHVwb24gY2FuY2VsbGF0aW9uXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuX3ZlcmlmaWVyLnZlcmlmeSgpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfVkVSSUZJRUR9KTtcclxuICAgICAgICAgICAgdGhpcy5fdmVyaWZpZXIucmVtb3ZlTGlzdGVuZXIoJ3Nob3dfc2FzJywgdGhpcy5fb25WZXJpZmllclNob3dTYXMpO1xyXG4gICAgICAgICAgICB0aGlzLl92ZXJpZmllciA9IG51bGw7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlZlcmlmaWNhdGlvbiBmYWlsZWRcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0NBTkNFTExFRCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZlcmlmaWVyID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2FzTWF0Y2hlc0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX3Nob3dTYXNFdmVudC5jb25maXJtKCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9XQUlUX0ZPUl9QQVJUTkVSX1RPX0NPTkZJUk0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uVmVyaWZpZWREb25lQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblZlcmlmaWVyU2hvd1NhcyA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2hvd1Nhc0V2ZW50ID0gZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dfU0FTLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJTYXNWZXJpZmljYXRpb24oKSB7XHJcbiAgICAgICAgbGV0IGJvZHk7XHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnN0YXRlLnBoYXNlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfU1RBUlQ6XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gdGhpcy5fcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VTdGFydCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfV0FJVF9GT1JfUEFSVE5FUl9UT19BQ0NFUFQ6XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gdGhpcy5fcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VXYWl0QWNjZXB0KCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9QSUNLX1ZFUklGSUNBVElPTl9PUFRJT046XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gdGhpcy5fcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VQaWNrKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9TSE9XX1NBUzpcclxuICAgICAgICAgICAgICAgIGJvZHkgPSB0aGlzLl9yZW5kZXJTYXNWZXJpZmljYXRpb25QaGFzZVNob3dTYXMoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX1dBSVRfRk9SX1BBUlRORVJfVE9fQ09ORklSTTpcclxuICAgICAgICAgICAgICAgIGJvZHkgPSB0aGlzLl9yZW5kZXJTYXNWZXJpZmljYXRpb25QaGFzZVdhaXRGb3JQYXJ0bmVyVG9Db25maXJtKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9WRVJJRklFRDpcclxuICAgICAgICAgICAgICAgIGJvZHkgPSB0aGlzLl9yZW5kZXJWZXJpZmljYXRpb25QaGFzZVZlcmlmaWVkKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9DQU5DRUxMRUQ6XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gdGhpcy5fcmVuZGVyVmVyaWZpY2F0aW9uUGhhc2VDYW5jZWxsZWQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkJhc2VEaWFsb2dcIik7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2dcclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlZlcmlmeSBzZXNzaW9uXCIpfVxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAge2JvZHl9XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJWZXJpZmljYXRpb25QaGFzZVN0YXJ0KCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50PVwic3BhblwiIGNsYXNzTmFtZT1cIm14X2xpbmtCdXR0b25cIiBvbkNsaWNrPXt0aGlzLl9vblN3aXRjaFRvTGVnYWN5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiVXNlIExlZ2FjeSBWZXJpZmljYXRpb24gKGZvciBvbGRlciBjbGllbnRzKVwiKX1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJWZXJpZnkgYnkgY29tcGFyaW5nIGEgc2hvcnQgdGV4dCBzdHJpbmcuXCIpIH1cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIlRvIGJlIHNlY3VyZSwgZG8gdGhpcyBpbiBwZXJzb24gb3IgdXNlIGEgdHJ1c3RlZCB3YXkgdG8gY29tbXVuaWNhdGUuXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdCgnQmVnaW4gVmVyaWZ5aW5nJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vblNhc1JlcXVlc3RDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclZlcmlmaWNhdGlvblBoYXNlV2FpdEFjY2VwdCgpIHtcclxuICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxTcGlubmVyIC8+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXCJXYWl0aW5nIGZvciBwYXJ0bmVyIHRvIGFjY2VwdC4uLlwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJOb3RoaW5nIGFwcGVhcmluZz8gTm90IGFsbCBjbGllbnRzIHN1cHBvcnQgaW50ZXJhY3RpdmUgdmVyaWZpY2F0aW9uIHlldC4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGJ1dHRvbj5Vc2UgbGVnYWN5IHZlcmlmaWNhdGlvbjwvYnV0dG9uPi5cIixcclxuICAgICAgICAgICAgICAgICAgICB7fSwge2J1dHRvbjogc3ViID0+IDxBY2Nlc3NpYmxlQnV0dG9uIGVsZW1lbnQ9J3NwYW4nIGNsYXNzTmFtZT1cIm14X2xpbmtCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblN3aXRjaFRvTGVnYWN5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c3VifVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj59LFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclZlcmlmaWNhdGlvblBoYXNlUGljaygpIHtcclxuICAgICAgICByZXR1cm4gPFZlcmlmaWNhdGlvblFSRW1vamlPcHRpb25zXHJcbiAgICAgICAgICAgIHJlcXVlc3Q9e3RoaXMuX3JlcXVlc3R9XHJcbiAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLl9vbkNhbmNlbENsaWNrfVxyXG4gICAgICAgICAgICBvblN0YXJ0RW1vamk9e3RoaXMuX29uVXNlU2FzQ2xpY2t9XHJcbiAgICAgICAgLz47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclNhc1ZlcmlmaWNhdGlvblBoYXNlU2hvd1NhcygpIHtcclxuICAgICAgICBjb25zdCBWZXJpZmljYXRpb25TaG93U2FzID0gc2RrLmdldENvbXBvbmVudCgndmlld3MudmVyaWZpY2F0aW9uLlZlcmlmaWNhdGlvblNob3dTYXMnKTtcclxuICAgICAgICByZXR1cm4gPFZlcmlmaWNhdGlvblNob3dTYXNcclxuICAgICAgICAgICAgc2FzPXt0aGlzLl9zaG93U2FzRXZlbnQuc2FzfVxyXG4gICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgb25Eb25lPXt0aGlzLl9vblNhc01hdGNoZXNDbGlja31cclxuICAgICAgICAgICAgaXNTZWxmPXtNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCkgPT09IHRoaXMucHJvcHMudXNlcklkfVxyXG4gICAgICAgICAgICBvblN0YXJ0RW1vamk9e3RoaXMuX29uVXNlU2FzQ2xpY2t9XHJcbiAgICAgICAgICAgIGluRGlhbG9nPXt0cnVlfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJTYXNWZXJpZmljYXRpb25QaGFzZVdhaXRGb3JQYXJ0bmVyVG9Db25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5TcGlubmVyJyk7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxTcGlubmVyIC8+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiV2FpdGluZyBmb3IgJSh1c2VySWQpcyB0byBjb25maXJtLi4uXCIsIHt1c2VySWQ6IHRoaXMucHJvcHMudXNlcklkfSxcclxuICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJWZXJpZmljYXRpb25QaGFzZVZlcmlmaWVkKCkge1xyXG4gICAgICAgIGNvbnN0IFZlcmlmaWNhdGlvbkNvbXBsZXRlID0gc2RrLmdldENvbXBvbmVudCgndmlld3MudmVyaWZpY2F0aW9uLlZlcmlmaWNhdGlvbkNvbXBsZXRlJyk7XHJcbiAgICAgICAgcmV0dXJuIDxWZXJpZmljYXRpb25Db21wbGV0ZSBvbkRvbmU9e3RoaXMuX29uVmVyaWZpZWREb25lQ2xpY2t9IC8+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJWZXJpZmljYXRpb25QaGFzZUNhbmNlbGxlZCgpIHtcclxuICAgICAgICBjb25zdCBWZXJpZmljYXRpb25DYW5jZWxsZWQgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy52ZXJpZmljYXRpb24uVmVyaWZpY2F0aW9uQ2FuY2VsbGVkJyk7XHJcbiAgICAgICAgcmV0dXJuIDxWZXJpZmljYXRpb25DYW5jZWxsZWQgb25Eb25lPXt0aGlzLl9vbkNhbmNlbENsaWNrfSAvPjtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyTGVnYWN5VmVyaWZpY2F0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgbGV0IHRleHQ7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSA9PT0gdGhpcy5wcm9wcy51c2VySWQpIHtcclxuICAgICAgICAgICAgdGV4dCA9IF90KFwiVG8gdmVyaWZ5IHRoYXQgdGhpcyBzZXNzaW9uIGNhbiBiZSB0cnVzdGVkLCBwbGVhc2UgY2hlY2sgdGhhdCB0aGUga2V5IHlvdSBzZWUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJpbiBVc2VyIFNldHRpbmdzIG9uIHRoYXQgZGV2aWNlIG1hdGNoZXMgdGhlIGtleSBiZWxvdzpcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGV4dCA9IF90KFwiVG8gdmVyaWZ5IHRoYXQgdGhpcyBzZXNzaW9uIGNhbiBiZSB0cnVzdGVkLCBwbGVhc2UgY29udGFjdCBpdHMgb3duZXIgdXNpbmcgc29tZSBvdGhlciBcIiArXHJcbiAgICAgICAgICAgICAgICBcIm1lYW5zIChlLmcuIGluIHBlcnNvbiBvciBhIHBob25lIGNhbGwpIGFuZCBhc2sgdGhlbSB3aGV0aGVyIHRoZSBrZXkgdGhleSBzZWUgaW4gdGhlaXIgVXNlciBTZXR0aW5ncyBcIiArXHJcbiAgICAgICAgICAgICAgICBcImZvciB0aGlzIHNlc3Npb24gbWF0Y2hlcyB0aGUga2V5IGJlbG93OlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGtleSA9IEZvcm1hdHRpbmdVdGlscy5mb3JtYXRDcnlwdG9LZXkodGhpcy5wcm9wcy5kZXZpY2UuZ2V0RmluZ2VycHJpbnQoKSk7XHJcbiAgICAgICAgY29uc3QgYm9keSA9IChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudD1cInNwYW5cIiBjbGFzc05hbWU9XCJteF9saW5rQnV0dG9uXCIgb25DbGljaz17dGhpcy5fb25Td2l0Y2hUb1Nhc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIlVzZSB0d28td2F5IHRleHQgdmVyaWZpY2F0aW9uXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0ZXh0IH1cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGV2aWNlVmVyaWZ5RGlhbG9nX2NyeXB0b1NlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48bGFiZWw+eyBfdChcIlNlc3Npb24gbmFtZVwiKSB9OjwvbGFiZWw+IDxzcGFuPnsgdGhpcy5wcm9wcy5kZXZpY2UuZ2V0RGlzcGxheU5hbWUoKSB9PC9zcGFuPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48bGFiZWw+eyBfdChcIlNlc3Npb24gSURcIikgfTo8L2xhYmVsPiA8c3Bhbj48Y29kZT57IHRoaXMucHJvcHMuZGV2aWNlLmRldmljZUlkIH08L2NvZGU+PC9zcGFuPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48bGFiZWw+eyBfdChcIlNlc3Npb24ga2V5XCIpIH06PC9sYWJlbD4gPHNwYW4+PGNvZGU+PGI+eyBrZXkgfTwvYj48L2NvZGU+PC9zcGFuPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIklmIGl0IG1hdGNoZXMsIHByZXNzIHRoZSB2ZXJpZnkgYnV0dG9uIGJlbG93LiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiSWYgaXQgZG9lc24ndCwgdGhlbiBzb21lb25lIGVsc2UgaXMgaW50ZXJjZXB0aW5nIHRoaXMgc2Vzc2lvbiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiYW5kIHlvdSBwcm9iYWJseSB3YW50IHRvIHByZXNzIHRoZSBibGFja2xpc3QgYnV0dG9uIGluc3RlYWQuXCIpIH1cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFF1ZXN0aW9uRGlhbG9nXHJcbiAgICAgICAgICAgICAgICB0aXRsZT17X3QoXCJWZXJpZnkgc2Vzc2lvblwiKX1cclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uPXtib2R5fVxyXG4gICAgICAgICAgICAgICAgYnV0dG9uPXtfdChcIkkgdmVyaWZ5IHRoYXQgdGhlIGtleXMgbWF0Y2hcIil9XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLl9vbkxlZ2FjeUZpbmlzaGVkfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1vZGUgPT09IE1PREVfTEVHQUNZKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9yZW5kZXJMZWdhY3lWZXJpZmljYXRpb24oKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJTYXNWZXJpZmljYXRpb24oKX1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuYXN5bmMgZnVuY3Rpb24gZW5zdXJlRE1FeGlzdHNBbmRPcGVuKHVzZXJJZCkge1xyXG4gICAgY29uc3Qgcm9vbUlkID0gYXdhaXQgZW5zdXJlRE1FeGlzdHMoTWF0cml4Q2xpZW50UGVnLmdldCgpLCB1c2VySWQpO1xyXG4gICAgLy8gZG9uJ3QgdXNlIGFuZFZpZXcgYW5kIHNwaW5uZXIgaW4gY3JlYXRlUm9vbSwgdG9nZXRoZXIsIHRoZXkgY2F1c2UgdGhpcyBkaWFsb2cgdG8gY2xvc2UgYW5kIHJlb3BlbixcclxuICAgIC8vIHdlIGNhdXNlcyB1cyB0byBsb29zZSB0aGUgdmVyaWZpZXIgYW5kIHJlc3RhcnQsIGFuZCB3ZSBlbmQgdXAgaGF2aW5nIHR3byB2ZXJpZmljYXRpb24gcmVxdWVzdHNcclxuICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICByb29tX2lkOiByb29tSWQsXHJcbiAgICAgICAgc2hvdWxkX3BlZWs6IGZhbHNlLFxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gcm9vbUlkO1xyXG59XHJcbiJdfQ==