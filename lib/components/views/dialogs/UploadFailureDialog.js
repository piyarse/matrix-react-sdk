"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _filesize = _interopRequireDefault(require("filesize"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _ContentMessages = _interopRequireDefault(require("../../../ContentMessages"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Tells the user about files we know cannot be uploaded before we even try uploading
 * them. This is named fairly generically but the only thing we check right now is
 * the size of the file.
 */
class UploadFailureDialog extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onCancelClick", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onUploadClick", () => {
      this.props.onFinished(true);
    });
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    let message;
    let preview;
    let buttons;

    if (this.props.totalFiles === 1 && this.props.badFiles.length === 1) {
      message = (0, _languageHandler._t)("This file is <b>too large</b> to upload. " + "The file size limit is %(limit)s but this file is %(sizeOfThisFile)s.", {
        limit: (0, _filesize.default)(this.props.contentMessages.getUploadLimit()),
        sizeOfThisFile: (0, _filesize.default)(this.props.badFiles[0].size)
      }, {
        b: sub => _react.default.createElement("b", null, sub)
      });
      buttons = _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('OK'),
        hasCancel: false,
        onPrimaryButtonClick: this._onCancelClick,
        focus: true
      });
    } else if (this.props.totalFiles === this.props.badFiles.length) {
      message = (0, _languageHandler._t)("These files are <b>too large</b> to upload. " + "The file size limit is %(limit)s.", {
        limit: (0, _filesize.default)(this.props.contentMessages.getUploadLimit())
      }, {
        b: sub => _react.default.createElement("b", null, sub)
      });
      buttons = _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('OK'),
        hasCancel: false,
        onPrimaryButtonClick: this._onCancelClick,
        focus: true
      });
    } else {
      message = (0, _languageHandler._t)("Some files are <b>too large</b> to be uploaded. " + "The file size limit is %(limit)s.", {
        limit: (0, _filesize.default)(this.props.contentMessages.getUploadLimit())
      }, {
        b: sub => _react.default.createElement("b", null, sub)
      });
      const howManyOthers = this.props.totalFiles - this.props.badFiles.length;
      buttons = _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Upload %(count)s other files', {
          count: howManyOthers
        }),
        onPrimaryButtonClick: this._onUploadClick,
        hasCancel: true,
        cancelButton: (0, _languageHandler._t)("Cancel All"),
        onCancel: this._onCancelClick,
        focus: true
      });
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_UploadFailureDialog",
      onFinished: this._onCancelClick,
      title: (0, _languageHandler._t)("Upload Error"),
      contentId: "mx_Dialog_content"
    }, _react.default.createElement("div", {
      id: "mx_Dialog_content"
    }, message, preview), buttons);
  }

}

exports.default = UploadFailureDialog;
(0, _defineProperty2.default)(UploadFailureDialog, "propTypes", {
  badFiles: _propTypes.default.arrayOf(_propTypes.default.object).isRequired,
  totalFiles: _propTypes.default.number.isRequired,
  contentMessages: _propTypes.default.instanceOf(_ContentMessages.default).isRequired,
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvVXBsb2FkRmFpbHVyZURpYWxvZy5qcyJdLCJuYW1lcyI6WyJVcGxvYWRGYWlsdXJlRGlhbG9nIiwiUmVhY3QiLCJDb21wb25lbnQiLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJyZW5kZXIiLCJCYXNlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiRGlhbG9nQnV0dG9ucyIsIm1lc3NhZ2UiLCJwcmV2aWV3IiwiYnV0dG9ucyIsInRvdGFsRmlsZXMiLCJiYWRGaWxlcyIsImxlbmd0aCIsImxpbWl0IiwiY29udGVudE1lc3NhZ2VzIiwiZ2V0VXBsb2FkTGltaXQiLCJzaXplT2ZUaGlzRmlsZSIsInNpemUiLCJiIiwic3ViIiwiX29uQ2FuY2VsQ2xpY2siLCJob3dNYW55T3RoZXJzIiwiY291bnQiLCJfb25VcGxvYWRDbGljayIsIlByb3BUeXBlcyIsImFycmF5T2YiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwibnVtYmVyIiwiaW5zdGFuY2VPZiIsIkNvbnRlbnRNZXNzYWdlcyIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdEJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBOzs7OztBQUtlLE1BQU1BLG1CQUFOLFNBQWtDQyxlQUFNQyxTQUF4QyxDQUFrRDtBQUFBO0FBQUE7QUFBQSwwREFRNUMsTUFBTTtBQUNuQixXQUFLQyxLQUFMLENBQVdDLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQVY0RDtBQUFBLDBEQVk1QyxNQUFNO0FBQ25CLFdBQUtELEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixJQUF0QjtBQUNILEtBZDREO0FBQUE7O0FBZ0I3REMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBRUEsUUFBSUUsT0FBSjtBQUNBLFFBQUlDLE9BQUo7QUFDQSxRQUFJQyxPQUFKOztBQUNBLFFBQUksS0FBS1QsS0FBTCxDQUFXVSxVQUFYLEtBQTBCLENBQTFCLElBQStCLEtBQUtWLEtBQUwsQ0FBV1csUUFBWCxDQUFvQkMsTUFBcEIsS0FBK0IsQ0FBbEUsRUFBcUU7QUFDakVMLE1BQUFBLE9BQU8sR0FBRyx5QkFDTiw4Q0FDQSx1RUFGTSxFQUdOO0FBQ0lNLFFBQUFBLEtBQUssRUFBRSx1QkFBUyxLQUFLYixLQUFMLENBQVdjLGVBQVgsQ0FBMkJDLGNBQTNCLEVBQVQsQ0FEWDtBQUVJQyxRQUFBQSxjQUFjLEVBQUUsdUJBQVMsS0FBS2hCLEtBQUwsQ0FBV1csUUFBWCxDQUFvQixDQUFwQixFQUF1Qk0sSUFBaEM7QUFGcEIsT0FITSxFQU1IO0FBQ0NDLFFBQUFBLENBQUMsRUFBRUMsR0FBRyxJQUFJLHdDQUFJQSxHQUFKO0FBRFgsT0FORyxDQUFWO0FBVUFWLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxhQUFEO0FBQWUsUUFBQSxhQUFhLEVBQUUseUJBQUcsSUFBSCxDQUE5QjtBQUNOLFFBQUEsU0FBUyxFQUFFLEtBREw7QUFFTixRQUFBLG9CQUFvQixFQUFFLEtBQUtXLGNBRnJCO0FBR04sUUFBQSxLQUFLLEVBQUU7QUFIRCxRQUFWO0FBS0gsS0FoQkQsTUFnQk8sSUFBSSxLQUFLcEIsS0FBTCxDQUFXVSxVQUFYLEtBQTBCLEtBQUtWLEtBQUwsQ0FBV1csUUFBWCxDQUFvQkMsTUFBbEQsRUFBMEQ7QUFDN0RMLE1BQUFBLE9BQU8sR0FBRyx5QkFDTixpREFDQSxtQ0FGTSxFQUdOO0FBQ0lNLFFBQUFBLEtBQUssRUFBRSx1QkFBUyxLQUFLYixLQUFMLENBQVdjLGVBQVgsQ0FBMkJDLGNBQTNCLEVBQVQ7QUFEWCxPQUhNLEVBS0g7QUFDQ0csUUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFEWCxPQUxHLENBQVY7QUFTQVYsTUFBQUEsT0FBTyxHQUFHLDZCQUFDLGFBQUQ7QUFBZSxRQUFBLGFBQWEsRUFBRSx5QkFBRyxJQUFILENBQTlCO0FBQ04sUUFBQSxTQUFTLEVBQUUsS0FETDtBQUVOLFFBQUEsb0JBQW9CLEVBQUUsS0FBS1csY0FGckI7QUFHTixRQUFBLEtBQUssRUFBRTtBQUhELFFBQVY7QUFLSCxLQWZNLE1BZUE7QUFDSGIsTUFBQUEsT0FBTyxHQUFHLHlCQUNOLHFEQUNBLG1DQUZNLEVBR047QUFDSU0sUUFBQUEsS0FBSyxFQUFFLHVCQUFTLEtBQUtiLEtBQUwsQ0FBV2MsZUFBWCxDQUEyQkMsY0FBM0IsRUFBVDtBQURYLE9BSE0sRUFLSDtBQUNDRyxRQUFBQSxDQUFDLEVBQUVDLEdBQUcsSUFBSSx3Q0FBSUEsR0FBSjtBQURYLE9BTEcsQ0FBVjtBQVNBLFlBQU1FLGFBQWEsR0FBRyxLQUFLckIsS0FBTCxDQUFXVSxVQUFYLEdBQXdCLEtBQUtWLEtBQUwsQ0FBV1csUUFBWCxDQUFvQkMsTUFBbEU7QUFDQUgsTUFBQUEsT0FBTyxHQUFHLDZCQUFDLGFBQUQ7QUFDTixRQUFBLGFBQWEsRUFBRSx5QkFBRyw4QkFBSCxFQUFtQztBQUFFYSxVQUFBQSxLQUFLLEVBQUVEO0FBQVQsU0FBbkMsQ0FEVDtBQUVOLFFBQUEsb0JBQW9CLEVBQUUsS0FBS0UsY0FGckI7QUFHTixRQUFBLFNBQVMsRUFBRSxJQUhMO0FBSU4sUUFBQSxZQUFZLEVBQUUseUJBQUcsWUFBSCxDQUpSO0FBS04sUUFBQSxRQUFRLEVBQUUsS0FBS0gsY0FMVDtBQU1OLFFBQUEsS0FBSyxFQUFFO0FBTkQsUUFBVjtBQVFIOztBQUVELFdBQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsU0FBUyxFQUFDLHdCQUF0QjtBQUNJLE1BQUEsVUFBVSxFQUFFLEtBQUtBLGNBRHJCO0FBRUksTUFBQSxLQUFLLEVBQUUseUJBQUcsY0FBSCxDQUZYO0FBR0ksTUFBQSxTQUFTLEVBQUM7QUFIZCxPQUtJO0FBQUssTUFBQSxFQUFFLEVBQUM7QUFBUixPQUNLYixPQURMLEVBRUtDLE9BRkwsQ0FMSixFQVVLQyxPQVZMLENBREo7QUFjSDs7QUF6RjREOzs7OEJBQTVDWixtQixlQUNFO0FBQ2ZjLEVBQUFBLFFBQVEsRUFBRWEsbUJBQVVDLE9BQVYsQ0FBa0JELG1CQUFVRSxNQUE1QixFQUFvQ0MsVUFEL0I7QUFFZmpCLEVBQUFBLFVBQVUsRUFBRWMsbUJBQVVJLE1BQVYsQ0FBaUJELFVBRmQ7QUFHZmIsRUFBQUEsZUFBZSxFQUFFVSxtQkFBVUssVUFBVixDQUFxQkMsd0JBQXJCLEVBQXNDSCxVQUh4QztBQUlmMUIsRUFBQUEsVUFBVSxFQUFFdUIsbUJBQVVPLElBQVYsQ0FBZUo7QUFKWixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgZmlsZXNpemUgZnJvbSAnZmlsZXNpemUnO1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgQ29udGVudE1lc3NhZ2VzIGZyb20gJy4uLy4uLy4uL0NvbnRlbnRNZXNzYWdlcyc7XHJcblxyXG4vKlxyXG4gKiBUZWxscyB0aGUgdXNlciBhYm91dCBmaWxlcyB3ZSBrbm93IGNhbm5vdCBiZSB1cGxvYWRlZCBiZWZvcmUgd2UgZXZlbiB0cnkgdXBsb2FkaW5nXHJcbiAqIHRoZW0uIFRoaXMgaXMgbmFtZWQgZmFpcmx5IGdlbmVyaWNhbGx5IGJ1dCB0aGUgb25seSB0aGluZyB3ZSBjaGVjayByaWdodCBub3cgaXNcclxuICogdGhlIHNpemUgb2YgdGhlIGZpbGUuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVcGxvYWRGYWlsdXJlRGlhbG9nIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgYmFkRmlsZXM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5vYmplY3QpLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgdG90YWxGaWxlczogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGNvbnRlbnRNZXNzYWdlczogUHJvcFR5cGVzLmluc3RhbmNlT2YoQ29udGVudE1lc3NhZ2VzKS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9XHJcblxyXG4gICAgX29uQ2FuY2VsQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25VcGxvYWRDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG5cclxuICAgICAgICBsZXQgbWVzc2FnZTtcclxuICAgICAgICBsZXQgcHJldmlldztcclxuICAgICAgICBsZXQgYnV0dG9ucztcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy50b3RhbEZpbGVzID09PSAxICYmIHRoaXMucHJvcHMuYmFkRmlsZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UgPSBfdChcclxuICAgICAgICAgICAgICAgIFwiVGhpcyBmaWxlIGlzIDxiPnRvbyBsYXJnZTwvYj4gdG8gdXBsb2FkLiBcIiArXHJcbiAgICAgICAgICAgICAgICBcIlRoZSBmaWxlIHNpemUgbGltaXQgaXMgJShsaW1pdClzIGJ1dCB0aGlzIGZpbGUgaXMgJShzaXplT2ZUaGlzRmlsZSlzLlwiLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpbWl0OiBmaWxlc2l6ZSh0aGlzLnByb3BzLmNvbnRlbnRNZXNzYWdlcy5nZXRVcGxvYWRMaW1pdCgpKSxcclxuICAgICAgICAgICAgICAgICAgICBzaXplT2ZUaGlzRmlsZTogZmlsZXNpemUodGhpcy5wcm9wcy5iYWRGaWxlc1swXS5zaXplKSxcclxuICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICBiOiBzdWIgPT4gPGI+e3N1Yn08L2I+LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgYnV0dG9ucyA9IDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e190KCdPSycpfVxyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vbkNhbmNlbENsaWNrfVxyXG4gICAgICAgICAgICAgICAgZm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy50b3RhbEZpbGVzID09PSB0aGlzLnByb3BzLmJhZEZpbGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIlRoZXNlIGZpbGVzIGFyZSA8Yj50b28gbGFyZ2U8L2I+IHRvIHVwbG9hZC4gXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJUaGUgZmlsZSBzaXplIGxpbWl0IGlzICUobGltaXQpcy5cIixcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBsaW1pdDogZmlsZXNpemUodGhpcy5wcm9wcy5jb250ZW50TWVzc2FnZXMuZ2V0VXBsb2FkTGltaXQoKSksXHJcbiAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYjogc3ViID0+IDxiPntzdWJ9PC9iPixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGJ1dHRvbnMgPSA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdCgnT0snKX1cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgICAgIGZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIlNvbWUgZmlsZXMgYXJlIDxiPnRvbyBsYXJnZTwvYj4gdG8gYmUgdXBsb2FkZWQuIFwiICtcclxuICAgICAgICAgICAgICAgIFwiVGhlIGZpbGUgc2l6ZSBsaW1pdCBpcyAlKGxpbWl0KXMuXCIsXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGltaXQ6IGZpbGVzaXplKHRoaXMucHJvcHMuY29udGVudE1lc3NhZ2VzLmdldFVwbG9hZExpbWl0KCkpLFxyXG4gICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgIGI6IHN1YiA9PiA8Yj57c3VifTwvYj4sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCBob3dNYW55T3RoZXJzID0gdGhpcy5wcm9wcy50b3RhbEZpbGVzIC0gdGhpcy5wcm9wcy5iYWRGaWxlcy5sZW5ndGg7XHJcbiAgICAgICAgICAgIGJ1dHRvbnMgPSA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoJ1VwbG9hZCAlKGNvdW50KXMgb3RoZXIgZmlsZXMnLCB7IGNvdW50OiBob3dNYW55T3RoZXJzIH0pfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uVXBsb2FkQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e3RydWV9XHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b249e190KFwiQ2FuY2VsIEFsbFwiKX1cclxuICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLl9vbkNhbmNlbENsaWNrfVxyXG4gICAgICAgICAgICAgICAgZm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPSdteF9VcGxvYWRGYWlsdXJlRGlhbG9nJ1xyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5fb25DYW5jZWxDbGlja31cclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlVwbG9hZCBFcnJvclwiKX1cclxuICAgICAgICAgICAgICAgIGNvbnRlbnRJZD0nbXhfRGlhbG9nX2NvbnRlbnQnXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9J214X0RpYWxvZ19jb250ZW50Jz5cclxuICAgICAgICAgICAgICAgICAgICB7bWVzc2FnZX1cclxuICAgICAgICAgICAgICAgICAgICB7cHJldmlld31cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIHtidXR0b25zfVxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=