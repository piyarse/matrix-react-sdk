"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _url = _interopRequireDefault(require("url"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class TermsCheckbox extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onChange", ev => {
      this.props.onChange(this.props.url, ev.target.checked);
    });
  }

  render() {
    return _react.default.createElement("input", {
      type: "checkbox",
      onChange: this.onChange,
      checked: this.props.checked
    });
  }

}

(0, _defineProperty2.default)(TermsCheckbox, "propTypes", {
  onChange: _propTypes.default.func.isRequired,
  url: _propTypes.default.string.isRequired,
  checked: _propTypes.default.bool.isRequired
});

class TermsDialog extends _react.default.PureComponent {
  constructor(props) {
    super();
    (0, _defineProperty2.default)(this, "_onCancelClick", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onNextClick", () => {
      this.props.onFinished(true, Object.keys(this.state.agreedUrls).filter(url => this.state.agreedUrls[url]));
    });
    (0, _defineProperty2.default)(this, "_onTermsCheckboxChange", (url, checked) => {
      this.setState({
        agreedUrls: Object.assign({}, this.state.agreedUrls, {
          [url]: checked
        })
      });
    });
    this.state = {
      // url -> boolean
      agreedUrls: {}
    };

    for (const url of props.agreedUrls) {
      this.state.agreedUrls[url] = true;
    }
  }

  _nameForServiceType(serviceType, host) {
    switch (serviceType) {
      case _matrixJsSdk.default.SERVICE_TYPES.IS:
        return _react.default.createElement("div", null, (0, _languageHandler._t)("Identity Server"), _react.default.createElement("br", null), "(", host, ")");

      case _matrixJsSdk.default.SERVICE_TYPES.IM:
        return _react.default.createElement("div", null, (0, _languageHandler._t)("Integration Manager"), _react.default.createElement("br", null), "(", host, ")");
    }
  }

  _summaryForServiceType(serviceType) {
    switch (serviceType) {
      case _matrixJsSdk.default.SERVICE_TYPES.IS:
        return _react.default.createElement("div", null, (0, _languageHandler._t)("Find others by phone or email"), _react.default.createElement("br", null), (0, _languageHandler._t)("Be found by phone or email"));

      case _matrixJsSdk.default.SERVICE_TYPES.IM:
        return _react.default.createElement("div", null, (0, _languageHandler._t)("Use bots, bridges, widgets and sticker packs"));
    }
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const rows = [];

    for (const policiesAndService of this.props.policiesAndServicePairs) {
      const parsedBaseUrl = _url.default.parse(policiesAndService.service.baseUrl);

      const policyValues = Object.values(policiesAndService.policies);

      for (let i = 0; i < policyValues.length; ++i) {
        const termDoc = policyValues[i];
        const termsLang = (0, _languageHandler.pickBestLanguage)(Object.keys(termDoc).filter(k => k !== 'version'));
        let serviceName;
        let summary;

        if (i === 0) {
          serviceName = this._nameForServiceType(policiesAndService.service.serviceType, parsedBaseUrl.host);
          summary = this._summaryForServiceType(policiesAndService.service.serviceType);
        }

        rows.push(_react.default.createElement("tr", {
          key: termDoc[termsLang].url
        }, _react.default.createElement("td", {
          className: "mx_TermsDialog_service"
        }, serviceName), _react.default.createElement("td", {
          className: "mx_TermsDialog_summary"
        }, summary), _react.default.createElement("td", null, termDoc[termsLang].name, " ", _react.default.createElement("a", {
          rel: "noreferrer noopener",
          target: "_blank",
          href: termDoc[termsLang].url
        }, _react.default.createElement("span", {
          className: "mx_TermsDialog_link"
        }))), _react.default.createElement("td", null, _react.default.createElement(TermsCheckbox, {
          url: termDoc[termsLang].url,
          onChange: this._onTermsCheckboxChange,
          checked: Boolean(this.state.agreedUrls[termDoc[termsLang].url])
        }))));
      }
    } // if all the documents for at least one service have been checked, we can enable
    // the submit button


    let enableSubmit = false;

    for (const policiesAndService of this.props.policiesAndServicePairs) {
      let docsAgreedForService = 0;

      for (const terms of Object.values(policiesAndService.policies)) {
        let docAgreed = false;

        for (const lang of Object.keys(terms)) {
          if (lang === 'version') continue;

          if (this.state.agreedUrls[terms[lang].url]) {
            docAgreed = true;
            break;
          }
        }

        if (docAgreed) {
          ++docsAgreedForService;
        }
      }

      if (docsAgreedForService === Object.keys(policiesAndService.policies).length) {
        enableSubmit = true;
        break;
      }
    }

    return _react.default.createElement(BaseDialog, {
      fixedWidth: false,
      onFinished: this._onCancelClick,
      title: (0, _languageHandler._t)("Terms of Service"),
      contentId: "mx_Dialog_content",
      hasCancel: false
    }, _react.default.createElement("div", {
      id: "mx_Dialog_content"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("To continue you need to accept the terms of this service.")), _react.default.createElement("table", {
      className: "mx_TermsDialog_termsTable"
    }, _react.default.createElement("tbody", null, _react.default.createElement("tr", {
      className: "mx_TermsDialog_termsTableHeader"
    }, _react.default.createElement("th", null, (0, _languageHandler._t)("Service")), _react.default.createElement("th", null, (0, _languageHandler._t)("Summary")), _react.default.createElement("th", null, (0, _languageHandler._t)("Document")), _react.default.createElement("th", null, (0, _languageHandler._t)("Accept"))), rows))), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Next'),
      hasCancel: true,
      onCancel: this._onCancelClick,
      onPrimaryButtonClick: this._onNextClick,
      primaryDisabled: !enableSubmit
    }));
  }

}

exports.default = TermsDialog;
(0, _defineProperty2.default)(TermsDialog, "propTypes", {
  /**
   * Array of [Service, policies] pairs, where policies is the response from the
   * /terms endpoint for that service
   */
  policiesAndServicePairs: _propTypes.default.array.isRequired,

  /**
   * urls that the user has already agreed to
   */
  agreedUrls: _propTypes.default.arrayOf(_propTypes.default.string),

  /**
   * Called with:
   *     * success {bool} True if the user accepted any douments, false if cancelled
   *     * agreedUrls {string[]} List of agreed URLs
   */
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvVGVybXNEaWFsb2cuanMiXSwibmFtZXMiOlsiVGVybXNDaGVja2JveCIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImV2IiwicHJvcHMiLCJvbkNoYW5nZSIsInVybCIsInRhcmdldCIsImNoZWNrZWQiLCJyZW5kZXIiLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCIsInN0cmluZyIsImJvb2wiLCJUZXJtc0RpYWxvZyIsImNvbnN0cnVjdG9yIiwib25GaW5pc2hlZCIsIk9iamVjdCIsImtleXMiLCJzdGF0ZSIsImFncmVlZFVybHMiLCJmaWx0ZXIiLCJzZXRTdGF0ZSIsImFzc2lnbiIsIl9uYW1lRm9yU2VydmljZVR5cGUiLCJzZXJ2aWNlVHlwZSIsImhvc3QiLCJNYXRyaXgiLCJTRVJWSUNFX1RZUEVTIiwiSVMiLCJJTSIsIl9zdW1tYXJ5Rm9yU2VydmljZVR5cGUiLCJCYXNlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiRGlhbG9nQnV0dG9ucyIsInJvd3MiLCJwb2xpY2llc0FuZFNlcnZpY2UiLCJwb2xpY2llc0FuZFNlcnZpY2VQYWlycyIsInBhcnNlZEJhc2VVcmwiLCJwYXJzZSIsInNlcnZpY2UiLCJiYXNlVXJsIiwicG9saWN5VmFsdWVzIiwidmFsdWVzIiwicG9saWNpZXMiLCJpIiwibGVuZ3RoIiwidGVybURvYyIsInRlcm1zTGFuZyIsImsiLCJzZXJ2aWNlTmFtZSIsInN1bW1hcnkiLCJwdXNoIiwibmFtZSIsIl9vblRlcm1zQ2hlY2tib3hDaGFuZ2UiLCJCb29sZWFuIiwiZW5hYmxlU3VibWl0IiwiZG9jc0FncmVlZEZvclNlcnZpY2UiLCJ0ZXJtcyIsImRvY0FncmVlZCIsImxhbmciLCJfb25DYW5jZWxDbGljayIsIl9vbk5leHRDbGljayIsImFycmF5IiwiYXJyYXlPZiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUF0QkE7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxNQUFNQSxhQUFOLFNBQTRCQyxlQUFNQyxhQUFsQyxDQUFnRDtBQUFBO0FBQUE7QUFBQSxvREFPaENDLEVBQUQsSUFBUTtBQUNmLFdBQUtDLEtBQUwsQ0FBV0MsUUFBWCxDQUFvQixLQUFLRCxLQUFMLENBQVdFLEdBQS9CLEVBQW9DSCxFQUFFLENBQUNJLE1BQUgsQ0FBVUMsT0FBOUM7QUFDSCxLQVQyQztBQUFBOztBQVc1Q0MsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsV0FBTztBQUFPLE1BQUEsSUFBSSxFQUFDLFVBQVo7QUFDSCxNQUFBLFFBQVEsRUFBRSxLQUFLSixRQURaO0FBRUgsTUFBQSxPQUFPLEVBQUUsS0FBS0QsS0FBTCxDQUFXSTtBQUZqQixNQUFQO0FBSUg7O0FBaEIyQzs7OEJBQTFDUixhLGVBQ2lCO0FBQ2ZLLEVBQUFBLFFBQVEsRUFBRUssbUJBQVVDLElBQVYsQ0FBZUMsVUFEVjtBQUVmTixFQUFBQSxHQUFHLEVBQUVJLG1CQUFVRyxNQUFWLENBQWlCRCxVQUZQO0FBR2ZKLEVBQUFBLE9BQU8sRUFBRUUsbUJBQVVJLElBQVYsQ0FBZUY7QUFIVCxDOztBQWtCUixNQUFNRyxXQUFOLFNBQTBCZCxlQUFNQyxhQUFoQyxDQUE4QztBQXFCekRjLEVBQUFBLFdBQVcsQ0FBQ1osS0FBRCxFQUFRO0FBQ2Y7QUFEZSwwREFXRixNQUFNO0FBQ25CLFdBQUtBLEtBQUwsQ0FBV2EsVUFBWCxDQUFzQixLQUF0QjtBQUNILEtBYmtCO0FBQUEsd0RBZUosTUFBTTtBQUNqQixXQUFLYixLQUFMLENBQVdhLFVBQVgsQ0FBc0IsSUFBdEIsRUFBNEJDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZLEtBQUtDLEtBQUwsQ0FBV0MsVUFBdkIsRUFBbUNDLE1BQW5DLENBQTJDaEIsR0FBRCxJQUFTLEtBQUtjLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQmYsR0FBdEIsQ0FBbkQsQ0FBNUI7QUFDSCxLQWpCa0I7QUFBQSxrRUEyQ00sQ0FBQ0EsR0FBRCxFQUFNRSxPQUFOLEtBQWtCO0FBQ3ZDLFdBQUtlLFFBQUwsQ0FBYztBQUNWRixRQUFBQSxVQUFVLEVBQUVILE1BQU0sQ0FBQ00sTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS0osS0FBTCxDQUFXQyxVQUE3QixFQUF5QztBQUFFLFdBQUNmLEdBQUQsR0FBT0U7QUFBVCxTQUF6QztBQURGLE9BQWQ7QUFHSCxLQS9Da0I7QUFFZixTQUFLWSxLQUFMLEdBQWE7QUFDVDtBQUNBQyxNQUFBQSxVQUFVLEVBQUU7QUFGSCxLQUFiOztBQUlBLFNBQUssTUFBTWYsR0FBWCxJQUFrQkYsS0FBSyxDQUFDaUIsVUFBeEIsRUFBb0M7QUFDaEMsV0FBS0QsS0FBTCxDQUFXQyxVQUFYLENBQXNCZixHQUF0QixJQUE2QixJQUE3QjtBQUNIO0FBQ0o7O0FBVURtQixFQUFBQSxtQkFBbUIsQ0FBQ0MsV0FBRCxFQUFjQyxJQUFkLEVBQW9CO0FBQ25DLFlBQVFELFdBQVI7QUFDSSxXQUFLRSxxQkFBT0MsYUFBUCxDQUFxQkMsRUFBMUI7QUFDSSxlQUFPLDBDQUFNLHlCQUFHLGlCQUFILENBQU4sRUFBNEIsd0NBQTVCLE9BQW9DSCxJQUFwQyxNQUFQOztBQUNKLFdBQUtDLHFCQUFPQyxhQUFQLENBQXFCRSxFQUExQjtBQUNJLGVBQU8sMENBQU0seUJBQUcscUJBQUgsQ0FBTixFQUFnQyx3Q0FBaEMsT0FBd0NKLElBQXhDLE1BQVA7QUFKUjtBQU1IOztBQUVESyxFQUFBQSxzQkFBc0IsQ0FBQ04sV0FBRCxFQUFjO0FBQ2hDLFlBQVFBLFdBQVI7QUFDSSxXQUFLRSxxQkFBT0MsYUFBUCxDQUFxQkMsRUFBMUI7QUFDSSxlQUFPLDBDQUNGLHlCQUFHLCtCQUFILENBREUsRUFFSCx3Q0FGRyxFQUdGLHlCQUFHLDRCQUFILENBSEUsQ0FBUDs7QUFLSixXQUFLRixxQkFBT0MsYUFBUCxDQUFxQkUsRUFBMUI7QUFDSSxlQUFPLDBDQUNGLHlCQUFHLDhDQUFILENBREUsQ0FBUDtBQVJSO0FBWUg7O0FBUUR0QixFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNd0IsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBRUEsVUFBTUUsSUFBSSxHQUFHLEVBQWI7O0FBQ0EsU0FBSyxNQUFNQyxrQkFBWCxJQUFpQyxLQUFLbEMsS0FBTCxDQUFXbUMsdUJBQTVDLEVBQXFFO0FBQ2pFLFlBQU1DLGFBQWEsR0FBR2xDLGFBQUltQyxLQUFKLENBQVVILGtCQUFrQixDQUFDSSxPQUFuQixDQUEyQkMsT0FBckMsQ0FBdEI7O0FBRUEsWUFBTUMsWUFBWSxHQUFHMUIsTUFBTSxDQUFDMkIsTUFBUCxDQUFjUCxrQkFBa0IsQ0FBQ1EsUUFBakMsQ0FBckI7O0FBQ0EsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxZQUFZLENBQUNJLE1BQWpDLEVBQXlDLEVBQUVELENBQTNDLEVBQThDO0FBQzFDLGNBQU1FLE9BQU8sR0FBR0wsWUFBWSxDQUFDRyxDQUFELENBQTVCO0FBQ0EsY0FBTUcsU0FBUyxHQUFHLHVDQUFpQmhDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZOEIsT0FBWixFQUFxQjNCLE1BQXJCLENBQTZCNkIsQ0FBRCxJQUFPQSxDQUFDLEtBQUssU0FBekMsQ0FBakIsQ0FBbEI7QUFDQSxZQUFJQyxXQUFKO0FBQ0EsWUFBSUMsT0FBSjs7QUFDQSxZQUFJTixDQUFDLEtBQUssQ0FBVixFQUFhO0FBQ1RLLFVBQUFBLFdBQVcsR0FBRyxLQUFLM0IsbUJBQUwsQ0FBeUJhLGtCQUFrQixDQUFDSSxPQUFuQixDQUEyQmhCLFdBQXBELEVBQWlFYyxhQUFhLENBQUNiLElBQS9FLENBQWQ7QUFDQTBCLFVBQUFBLE9BQU8sR0FBRyxLQUFLckIsc0JBQUwsQ0FDTk0sa0JBQWtCLENBQUNJLE9BQW5CLENBQTJCaEIsV0FEckIsQ0FBVjtBQUdIOztBQUVEVyxRQUFBQSxJQUFJLENBQUNpQixJQUFMLENBQVU7QUFBSSxVQUFBLEdBQUcsRUFBRUwsT0FBTyxDQUFDQyxTQUFELENBQVAsQ0FBbUI1QztBQUE1QixXQUNOO0FBQUksVUFBQSxTQUFTLEVBQUM7QUFBZCxXQUF3QzhDLFdBQXhDLENBRE0sRUFFTjtBQUFJLFVBQUEsU0FBUyxFQUFDO0FBQWQsV0FBd0NDLE9BQXhDLENBRk0sRUFHTix5Q0FBS0osT0FBTyxDQUFDQyxTQUFELENBQVAsQ0FBbUJLLElBQXhCLE9BQThCO0FBQUcsVUFBQSxHQUFHLEVBQUMscUJBQVA7QUFBNkIsVUFBQSxNQUFNLEVBQUMsUUFBcEM7QUFBNkMsVUFBQSxJQUFJLEVBQUVOLE9BQU8sQ0FBQ0MsU0FBRCxDQUFQLENBQW1CNUM7QUFBdEUsV0FDMUI7QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixVQUQwQixDQUE5QixDQUhNLEVBTU4seUNBQUksNkJBQUMsYUFBRDtBQUNBLFVBQUEsR0FBRyxFQUFFMkMsT0FBTyxDQUFDQyxTQUFELENBQVAsQ0FBbUI1QyxHQUR4QjtBQUVBLFVBQUEsUUFBUSxFQUFFLEtBQUtrRCxzQkFGZjtBQUdBLFVBQUEsT0FBTyxFQUFFQyxPQUFPLENBQUMsS0FBS3JDLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQjRCLE9BQU8sQ0FBQ0MsU0FBRCxDQUFQLENBQW1CNUMsR0FBekMsQ0FBRDtBQUhoQixVQUFKLENBTk0sQ0FBVjtBQVlIO0FBQ0osS0FsQ0ksQ0FvQ0w7QUFDQTs7O0FBQ0EsUUFBSW9ELFlBQVksR0FBRyxLQUFuQjs7QUFDQSxTQUFLLE1BQU1wQixrQkFBWCxJQUFpQyxLQUFLbEMsS0FBTCxDQUFXbUMsdUJBQTVDLEVBQXFFO0FBQ2pFLFVBQUlvQixvQkFBb0IsR0FBRyxDQUEzQjs7QUFDQSxXQUFLLE1BQU1DLEtBQVgsSUFBb0IxQyxNQUFNLENBQUMyQixNQUFQLENBQWNQLGtCQUFrQixDQUFDUSxRQUFqQyxDQUFwQixFQUFnRTtBQUM1RCxZQUFJZSxTQUFTLEdBQUcsS0FBaEI7O0FBQ0EsYUFBSyxNQUFNQyxJQUFYLElBQW1CNUMsTUFBTSxDQUFDQyxJQUFQLENBQVl5QyxLQUFaLENBQW5CLEVBQXVDO0FBQ25DLGNBQUlFLElBQUksS0FBSyxTQUFiLEVBQXdCOztBQUN4QixjQUFJLEtBQUsxQyxLQUFMLENBQVdDLFVBQVgsQ0FBc0J1QyxLQUFLLENBQUNFLElBQUQsQ0FBTCxDQUFZeEQsR0FBbEMsQ0FBSixFQUE0QztBQUN4Q3VELFlBQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0E7QUFDSDtBQUNKOztBQUNELFlBQUlBLFNBQUosRUFBZTtBQUNYLFlBQUVGLG9CQUFGO0FBQ0g7QUFDSjs7QUFDRCxVQUFJQSxvQkFBb0IsS0FBS3pDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZbUIsa0JBQWtCLENBQUNRLFFBQS9CLEVBQXlDRSxNQUF0RSxFQUE4RTtBQUMxRVUsUUFBQUEsWUFBWSxHQUFHLElBQWY7QUFDQTtBQUNIO0FBQ0o7O0FBRUQsV0FDSSw2QkFBQyxVQUFEO0FBQ0ksTUFBQSxVQUFVLEVBQUUsS0FEaEI7QUFFSSxNQUFBLFVBQVUsRUFBRSxLQUFLSyxjQUZyQjtBQUdJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFILENBSFg7QUFJSSxNQUFBLFNBQVMsRUFBQyxtQkFKZDtBQUtJLE1BQUEsU0FBUyxFQUFFO0FBTGYsT0FPSTtBQUFLLE1BQUEsRUFBRSxFQUFDO0FBQVIsT0FDSSx3Q0FBSSx5QkFBRywyREFBSCxDQUFKLENBREosRUFHSTtBQUFPLE1BQUEsU0FBUyxFQUFDO0FBQWpCLE9BQTZDLDRDQUN6QztBQUFJLE1BQUEsU0FBUyxFQUFDO0FBQWQsT0FDSSx5Q0FBSyx5QkFBRyxTQUFILENBQUwsQ0FESixFQUVJLHlDQUFLLHlCQUFHLFNBQUgsQ0FBTCxDQUZKLEVBR0kseUNBQUsseUJBQUcsVUFBSCxDQUFMLENBSEosRUFJSSx5Q0FBSyx5QkFBRyxRQUFILENBQUwsQ0FKSixDQUR5QyxFQU94QzFCLElBUHdDLENBQTdDLENBSEosQ0FQSixFQXFCSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxhQUFhLEVBQUUseUJBQUcsTUFBSCxDQUE5QjtBQUNJLE1BQUEsU0FBUyxFQUFFLElBRGY7QUFFSSxNQUFBLFFBQVEsRUFBRSxLQUFLMEIsY0FGbkI7QUFHSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtDLFlBSC9CO0FBSUksTUFBQSxlQUFlLEVBQUUsQ0FBQ047QUFKdEIsTUFyQkosQ0FESjtBQThCSDs7QUFoS3dEOzs7OEJBQXhDM0MsVyxlQUNFO0FBQ2Y7Ozs7QUFJQXdCLEVBQUFBLHVCQUF1QixFQUFFN0IsbUJBQVV1RCxLQUFWLENBQWdCckQsVUFMMUI7O0FBT2Y7OztBQUdBUyxFQUFBQSxVQUFVLEVBQUVYLG1CQUFVd0QsT0FBVixDQUFrQnhELG1CQUFVRyxNQUE1QixDQVZHOztBQVlmOzs7OztBQUtBSSxFQUFBQSxVQUFVLEVBQUVQLG1CQUFVQyxJQUFWLENBQWVDO0FBakJaLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB1cmwgZnJvbSAndXJsJztcclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QsIHBpY2tCZXN0TGFuZ3VhZ2UgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuaW1wb3J0IE1hdHJpeCBmcm9tICdtYXRyaXgtanMtc2RrJztcclxuXHJcbmNsYXNzIFRlcm1zQ2hlY2tib3ggZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgdXJsOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgY2hlY2tlZDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZSA9IChldikgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2UodGhpcy5wcm9wcy51cmwsIGV2LnRhcmdldC5jaGVja2VkKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIlxyXG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cclxuICAgICAgICAgICAgY2hlY2tlZD17dGhpcy5wcm9wcy5jaGVja2VkfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXJtc0RpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBBcnJheSBvZiBbU2VydmljZSwgcG9saWNpZXNdIHBhaXJzLCB3aGVyZSBwb2xpY2llcyBpcyB0aGUgcmVzcG9uc2UgZnJvbSB0aGVcclxuICAgICAgICAgKiAvdGVybXMgZW5kcG9pbnQgZm9yIHRoYXQgc2VydmljZVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHBvbGljaWVzQW5kU2VydmljZVBhaXJzOiBQcm9wVHlwZXMuYXJyYXkuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogdXJscyB0aGF0IHRoZSB1c2VyIGhhcyBhbHJlYWR5IGFncmVlZCB0b1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGFncmVlZFVybHM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5zdHJpbmcpLFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBDYWxsZWQgd2l0aDpcclxuICAgICAgICAgKiAgICAgKiBzdWNjZXNzIHtib29sfSBUcnVlIGlmIHRoZSB1c2VyIGFjY2VwdGVkIGFueSBkb3VtZW50cywgZmFsc2UgaWYgY2FuY2VsbGVkXHJcbiAgICAgICAgICogICAgICogYWdyZWVkVXJscyB7c3RyaW5nW119IExpc3Qgb2YgYWdyZWVkIFVSTHNcclxuICAgICAgICAgKi9cclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICAvLyB1cmwgLT4gYm9vbGVhblxyXG4gICAgICAgICAgICBhZ3JlZWRVcmxzOiB7fSxcclxuICAgICAgICB9O1xyXG4gICAgICAgIGZvciAoY29uc3QgdXJsIG9mIHByb3BzLmFncmVlZFVybHMpIHtcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hZ3JlZWRVcmxzW3VybF0gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25DYW5jZWxDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbk5leHRDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodHJ1ZSwgT2JqZWN0LmtleXModGhpcy5zdGF0ZS5hZ3JlZWRVcmxzKS5maWx0ZXIoKHVybCkgPT4gdGhpcy5zdGF0ZS5hZ3JlZWRVcmxzW3VybF0pKTtcclxuICAgIH1cclxuXHJcbiAgICBfbmFtZUZvclNlcnZpY2VUeXBlKHNlcnZpY2VUeXBlLCBob3N0KSB7XHJcbiAgICAgICAgc3dpdGNoIChzZXJ2aWNlVHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlIE1hdHJpeC5TRVJWSUNFX1RZUEVTLklTOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXY+e190KFwiSWRlbnRpdHkgU2VydmVyXCIpfTxiciAvPih7aG9zdH0pPC9kaXY+O1xyXG4gICAgICAgICAgICBjYXNlIE1hdHJpeC5TRVJWSUNFX1RZUEVTLklNOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXY+e190KFwiSW50ZWdyYXRpb24gTWFuYWdlclwiKX08YnIgLz4oe2hvc3R9KTwvZGl2PjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3N1bW1hcnlGb3JTZXJ2aWNlVHlwZShzZXJ2aWNlVHlwZSkge1xyXG4gICAgICAgIHN3aXRjaCAoc2VydmljZVR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBNYXRyaXguU0VSVklDRV9UWVBFUy5JUzpcclxuICAgICAgICAgICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIkZpbmQgb3RoZXJzIGJ5IHBob25lIG9yIGVtYWlsXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIkJlIGZvdW5kIGJ5IHBob25lIG9yIGVtYWlsXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICBjYXNlIE1hdHJpeC5TRVJWSUNFX1RZUEVTLklNOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiVXNlIGJvdHMsIGJyaWRnZXMsIHdpZGdldHMgYW5kIHN0aWNrZXIgcGFja3NcIil9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vblRlcm1zQ2hlY2tib3hDaGFuZ2UgPSAodXJsLCBjaGVja2VkKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGFncmVlZFVybHM6IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuc3RhdGUuYWdyZWVkVXJscywgeyBbdXJsXTogY2hlY2tlZCB9KSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHJvd3MgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IHBvbGljaWVzQW5kU2VydmljZSBvZiB0aGlzLnByb3BzLnBvbGljaWVzQW5kU2VydmljZVBhaXJzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcnNlZEJhc2VVcmwgPSB1cmwucGFyc2UocG9saWNpZXNBbmRTZXJ2aWNlLnNlcnZpY2UuYmFzZVVybCk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwb2xpY3lWYWx1ZXMgPSBPYmplY3QudmFsdWVzKHBvbGljaWVzQW5kU2VydmljZS5wb2xpY2llcyk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcG9saWN5VmFsdWVzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0ZXJtRG9jID0gcG9saWN5VmFsdWVzW2ldO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGVybXNMYW5nID0gcGlja0Jlc3RMYW5ndWFnZShPYmplY3Qua2V5cyh0ZXJtRG9jKS5maWx0ZXIoKGspID0+IGsgIT09ICd2ZXJzaW9uJykpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlcnZpY2VOYW1lO1xyXG4gICAgICAgICAgICAgICAgbGV0IHN1bW1hcnk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlcnZpY2VOYW1lID0gdGhpcy5fbmFtZUZvclNlcnZpY2VUeXBlKHBvbGljaWVzQW5kU2VydmljZS5zZXJ2aWNlLnNlcnZpY2VUeXBlLCBwYXJzZWRCYXNlVXJsLmhvc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgIHN1bW1hcnkgPSB0aGlzLl9zdW1tYXJ5Rm9yU2VydmljZVR5cGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvbGljaWVzQW5kU2VydmljZS5zZXJ2aWNlLnNlcnZpY2VUeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcm93cy5wdXNoKDx0ciBrZXk9e3Rlcm1Eb2NbdGVybXNMYW5nXS51cmx9PlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZCBjbGFzc05hbWU9XCJteF9UZXJtc0RpYWxvZ19zZXJ2aWNlXCI+e3NlcnZpY2VOYW1lfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzTmFtZT1cIm14X1Rlcm1zRGlhbG9nX3N1bW1hcnlcIj57c3VtbWFyeX08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD57dGVybURvY1t0ZXJtc0xhbmddLm5hbWV9IDxhIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIiB0YXJnZXQ9XCJfYmxhbmtcIiBocmVmPXt0ZXJtRG9jW3Rlcm1zTGFuZ10udXJsfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfVGVybXNEaWFsb2dfbGlua1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9hPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxUZXJtc0NoZWNrYm94XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybD17dGVybURvY1t0ZXJtc0xhbmddLnVybH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uVGVybXNDaGVja2JveENoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17Qm9vbGVhbih0aGlzLnN0YXRlLmFncmVlZFVybHNbdGVybURvY1t0ZXJtc0xhbmddLnVybF0pfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+PC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdHI+KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaWYgYWxsIHRoZSBkb2N1bWVudHMgZm9yIGF0IGxlYXN0IG9uZSBzZXJ2aWNlIGhhdmUgYmVlbiBjaGVja2VkLCB3ZSBjYW4gZW5hYmxlXHJcbiAgICAgICAgLy8gdGhlIHN1Ym1pdCBidXR0b25cclxuICAgICAgICBsZXQgZW5hYmxlU3VibWl0ID0gZmFsc2U7XHJcbiAgICAgICAgZm9yIChjb25zdCBwb2xpY2llc0FuZFNlcnZpY2Ugb2YgdGhpcy5wcm9wcy5wb2xpY2llc0FuZFNlcnZpY2VQYWlycykge1xyXG4gICAgICAgICAgICBsZXQgZG9jc0FncmVlZEZvclNlcnZpY2UgPSAwO1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHRlcm1zIG9mIE9iamVjdC52YWx1ZXMocG9saWNpZXNBbmRTZXJ2aWNlLnBvbGljaWVzKSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGRvY0FncmVlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBsYW5nIG9mIE9iamVjdC5rZXlzKHRlcm1zKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChsYW5nID09PSAndmVyc2lvbicpIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmFncmVlZFVybHNbdGVybXNbbGFuZ10udXJsXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkb2NBZ3JlZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoZG9jQWdyZWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgKytkb2NzQWdyZWVkRm9yU2VydmljZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZG9jc0FncmVlZEZvclNlcnZpY2UgPT09IE9iamVjdC5rZXlzKHBvbGljaWVzQW5kU2VydmljZS5wb2xpY2llcykubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBlbmFibGVTdWJtaXQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nXHJcbiAgICAgICAgICAgICAgICBmaXhlZFdpZHRoPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMuX29uQ2FuY2VsQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17X3QoXCJUZXJtcyBvZiBTZXJ2aWNlXCIpfVxyXG4gICAgICAgICAgICAgICAgY29udGVudElkPSdteF9EaWFsb2dfY29udGVudCdcclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9J214X0RpYWxvZ19jb250ZW50Jz5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXCJUbyBjb250aW51ZSB5b3UgbmVlZCB0byBhY2NlcHQgdGhlIHRlcm1zIG9mIHRoaXMgc2VydmljZS5cIil9PC9wPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8dGFibGUgY2xhc3NOYW1lPVwibXhfVGVybXNEaWFsb2dfdGVybXNUYWJsZVwiPjx0Ym9keT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHRyIGNsYXNzTmFtZT1cIm14X1Rlcm1zRGlhbG9nX3Rlcm1zVGFibGVIZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD57X3QoXCJTZXJ2aWNlXCIpfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+e190KFwiU3VtbWFyeVwiKX08L3RoPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPntfdChcIkRvY3VtZW50XCIpfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+e190KFwiQWNjZXB0XCIpfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtyb3dzfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvdGJvZHk+PC90YWJsZT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e190KCdOZXh0Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLl9vbkNhbmNlbENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vbk5leHRDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5RGlzYWJsZWQ9eyFlbmFibGVTdWJtaXR9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=