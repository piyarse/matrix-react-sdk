"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _matrixJsSdk = require("matrix-js-sdk");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _groups = require("../../../groups");

/*
Copyright 2017 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * A dialog for confirming an operation on another user.
 * Takes a user ID and a verb, displays the target user prominently
 * such that it should be easy to confirm that the operation is being
 * performed on the right person, and displays the operation prominently
 * to make it obvious what is going to happen.
 * Also tweaks the style for 'dangerous' actions (albeit only with colour)
 */
var _default = (0, _createReactClass.default)({
  displayName: 'ConfirmUserActionDialog',
  propTypes: {
    // matrix-js-sdk (room) member object. Supply either this or 'groupMember'
    member: _propTypes.default.object,
    // group member object. Supply either this or 'member'
    groupMember: _groups.GroupMemberType,
    // needed if a group member is specified
    matrixClient: _propTypes.default.instanceOf(_matrixJsSdk.MatrixClient),
    action: _propTypes.default.string.isRequired,
    // eg. 'Ban'
    title: _propTypes.default.string.isRequired,
    // eg. 'Ban this user?'
    // Whether to display a text field for a reason
    // If true, the second argument to onFinished will
    // be the string entered.
    askReason: _propTypes.default.bool,
    danger: _propTypes.default.bool,
    onFinished: _propTypes.default.func.isRequired
  },
  getDefaultProps: () => ({
    danger: false,
    askReason: false
  }),
  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    this._reasonField = null;
  },
  onOk: function () {
    let reason;

    if (this._reasonField) {
      reason = this._reasonField.value;
    }

    this.props.onFinished(true, reason);
  },
  onCancel: function () {
    this.props.onFinished(false);
  },
  _collectReasonField: function (e) {
    this._reasonField = e;
  },
  render: function () {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const MemberAvatar = sdk.getComponent("views.avatars.MemberAvatar");
    const BaseAvatar = sdk.getComponent("views.avatars.BaseAvatar");
    const confirmButtonClass = this.props.danger ? 'danger' : '';
    let reasonBox;

    if (this.props.askReason) {
      reasonBox = _react.default.createElement("div", null, _react.default.createElement("form", {
        onSubmit: this.onOk
      }, _react.default.createElement("input", {
        className: "mx_ConfirmUserActionDialog_reasonField",
        ref: this._collectReasonField,
        placeholder: (0, _languageHandler._t)("Reason"),
        autoFocus: true
      })));
    }

    let avatar;
    let name;
    let userId;

    if (this.props.member) {
      avatar = _react.default.createElement(MemberAvatar, {
        member: this.props.member,
        width: 48,
        height: 48
      });
      name = this.props.member.name;
      userId = this.props.member.userId;
    } else {
      const httpAvatarUrl = this.props.groupMember.avatarUrl ? this.props.matrixClient.mxcUrlToHttp(this.props.groupMember.avatarUrl, 48, 48) : null;
      name = this.props.groupMember.displayname || this.props.groupMember.userId;
      userId = this.props.groupMember.userId;
      avatar = _react.default.createElement(BaseAvatar, {
        name: name,
        url: httpAvatarUrl,
        width: 48,
        height: 48
      });
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_ConfirmUserActionDialog",
      onFinished: this.props.onFinished,
      title: this.props.title,
      contentId: "mx_Dialog_content"
    }, _react.default.createElement("div", {
      id: "mx_Dialog_content",
      className: "mx_Dialog_content"
    }, _react.default.createElement("div", {
      className: "mx_ConfirmUserActionDialog_avatar"
    }, avatar), _react.default.createElement("div", {
      className: "mx_ConfirmUserActionDialog_name"
    }, name), _react.default.createElement("div", {
      className: "mx_ConfirmUserActionDialog_userId"
    }, userId)), reasonBox, _react.default.createElement(DialogButtons, {
      primaryButton: this.props.action,
      onPrimaryButtonClick: this.onOk,
      primaryButtonClass: confirmButtonClass,
      focus: !this.props.askReason,
      onCancel: this.onCancel
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvQ29uZmlybVVzZXJBY3Rpb25EaWFsb2cuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJtZW1iZXIiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJncm91cE1lbWJlciIsIkdyb3VwTWVtYmVyVHlwZSIsIm1hdHJpeENsaWVudCIsImluc3RhbmNlT2YiLCJNYXRyaXhDbGllbnQiLCJhY3Rpb24iLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwidGl0bGUiLCJhc2tSZWFzb24iLCJib29sIiwiZGFuZ2VyIiwib25GaW5pc2hlZCIsImZ1bmMiLCJnZXREZWZhdWx0UHJvcHMiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX3JlYXNvbkZpZWxkIiwib25PayIsInJlYXNvbiIsInZhbHVlIiwicHJvcHMiLCJvbkNhbmNlbCIsIl9jb2xsZWN0UmVhc29uRmllbGQiLCJlIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIkRpYWxvZ0J1dHRvbnMiLCJNZW1iZXJBdmF0YXIiLCJCYXNlQXZhdGFyIiwiY29uZmlybUJ1dHRvbkNsYXNzIiwicmVhc29uQm94IiwiYXZhdGFyIiwibmFtZSIsInVzZXJJZCIsImh0dHBBdmF0YXJVcmwiLCJhdmF0YXJVcmwiLCJteGNVcmxUb0h0dHAiLCJkaXNwbGF5bmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdEJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBOzs7Ozs7OztlQVFlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLHlCQURlO0FBRTVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBQyxJQUFBQSxNQUFNLEVBQUVDLG1CQUFVQyxNQUZYO0FBR1A7QUFDQUMsSUFBQUEsV0FBVyxFQUFFQyx1QkFKTjtBQUtQO0FBQ0FDLElBQUFBLFlBQVksRUFBRUosbUJBQVVLLFVBQVYsQ0FBcUJDLHlCQUFyQixDQU5QO0FBT1BDLElBQUFBLE1BQU0sRUFBRVAsbUJBQVVRLE1BQVYsQ0FBaUJDLFVBUGxCO0FBTzhCO0FBQ3JDQyxJQUFBQSxLQUFLLEVBQUVWLG1CQUFVUSxNQUFWLENBQWlCQyxVQVJqQjtBQVE2QjtBQUVwQztBQUNBO0FBQ0E7QUFDQUUsSUFBQUEsU0FBUyxFQUFFWCxtQkFBVVksSUFiZDtBQWNQQyxJQUFBQSxNQUFNLEVBQUViLG1CQUFVWSxJQWRYO0FBZVBFLElBQUFBLFVBQVUsRUFBRWQsbUJBQVVlLElBQVYsQ0FBZU47QUFmcEIsR0FGaUI7QUFvQjVCTyxFQUFBQSxlQUFlLEVBQUUsT0FBTztBQUNwQkgsSUFBQUEsTUFBTSxFQUFFLEtBRFk7QUFFcEJGLElBQUFBLFNBQVMsRUFBRTtBQUZTLEdBQVAsQ0FwQlc7QUF5QjVCO0FBQ0FNLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsU0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNILEdBNUIyQjtBQThCNUJDLEVBQUFBLElBQUksRUFBRSxZQUFXO0FBQ2IsUUFBSUMsTUFBSjs7QUFDQSxRQUFJLEtBQUtGLFlBQVQsRUFBdUI7QUFDbkJFLE1BQUFBLE1BQU0sR0FBRyxLQUFLRixZQUFMLENBQWtCRyxLQUEzQjtBQUNIOztBQUNELFNBQUtDLEtBQUwsQ0FBV1IsVUFBWCxDQUFzQixJQUF0QixFQUE0Qk0sTUFBNUI7QUFDSCxHQXBDMkI7QUFzQzVCRyxFQUFBQSxRQUFRLEVBQUUsWUFBVztBQUNqQixTQUFLRCxLQUFMLENBQVdSLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxHQXhDMkI7QUEwQzVCVSxFQUFBQSxtQkFBbUIsRUFBRSxVQUFTQyxDQUFULEVBQVk7QUFDN0IsU0FBS1AsWUFBTCxHQUFvQk8sQ0FBcEI7QUFDSCxHQTVDMkI7QUE4QzVCQyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLFVBQU1DLGFBQWEsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFVBQU1FLFlBQVksR0FBR0gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDRCQUFqQixDQUFyQjtBQUNBLFVBQU1HLFVBQVUsR0FBR0osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUVBLFVBQU1JLGtCQUFrQixHQUFHLEtBQUtYLEtBQUwsQ0FBV1QsTUFBWCxHQUFvQixRQUFwQixHQUErQixFQUExRDtBQUVBLFFBQUlxQixTQUFKOztBQUNBLFFBQUksS0FBS1osS0FBTCxDQUFXWCxTQUFmLEVBQTBCO0FBQ3RCdUIsTUFBQUEsU0FBUyxHQUNMLDBDQUNJO0FBQU0sUUFBQSxRQUFRLEVBQUUsS0FBS2Y7QUFBckIsU0FDSTtBQUFPLFFBQUEsU0FBUyxFQUFDLHdDQUFqQjtBQUNJLFFBQUEsR0FBRyxFQUFFLEtBQUtLLG1CQURkO0FBRUksUUFBQSxXQUFXLEVBQUUseUJBQUcsUUFBSCxDQUZqQjtBQUdJLFFBQUEsU0FBUyxFQUFFO0FBSGYsUUFESixDQURKLENBREo7QUFXSDs7QUFFRCxRQUFJVyxNQUFKO0FBQ0EsUUFBSUMsSUFBSjtBQUNBLFFBQUlDLE1BQUo7O0FBQ0EsUUFBSSxLQUFLZixLQUFMLENBQVd2QixNQUFmLEVBQXVCO0FBQ25Cb0MsTUFBQUEsTUFBTSxHQUFHLDZCQUFDLFlBQUQ7QUFBYyxRQUFBLE1BQU0sRUFBRSxLQUFLYixLQUFMLENBQVd2QixNQUFqQztBQUF5QyxRQUFBLEtBQUssRUFBRSxFQUFoRDtBQUFvRCxRQUFBLE1BQU0sRUFBRTtBQUE1RCxRQUFUO0FBQ0FxQyxNQUFBQSxJQUFJLEdBQUcsS0FBS2QsS0FBTCxDQUFXdkIsTUFBWCxDQUFrQnFDLElBQXpCO0FBQ0FDLE1BQUFBLE1BQU0sR0FBRyxLQUFLZixLQUFMLENBQVd2QixNQUFYLENBQWtCc0MsTUFBM0I7QUFDSCxLQUpELE1BSU87QUFDSCxZQUFNQyxhQUFhLEdBQUcsS0FBS2hCLEtBQUwsQ0FBV3BCLFdBQVgsQ0FBdUJxQyxTQUF2QixHQUNsQixLQUFLakIsS0FBTCxDQUFXbEIsWUFBWCxDQUF3Qm9DLFlBQXhCLENBQXFDLEtBQUtsQixLQUFMLENBQVdwQixXQUFYLENBQXVCcUMsU0FBNUQsRUFBdUUsRUFBdkUsRUFBMkUsRUFBM0UsQ0FEa0IsR0FDK0QsSUFEckY7QUFFQUgsTUFBQUEsSUFBSSxHQUFHLEtBQUtkLEtBQUwsQ0FBV3BCLFdBQVgsQ0FBdUJ1QyxXQUF2QixJQUFzQyxLQUFLbkIsS0FBTCxDQUFXcEIsV0FBWCxDQUF1Qm1DLE1BQXBFO0FBQ0FBLE1BQUFBLE1BQU0sR0FBRyxLQUFLZixLQUFMLENBQVdwQixXQUFYLENBQXVCbUMsTUFBaEM7QUFDQUYsTUFBQUEsTUFBTSxHQUFHLDZCQUFDLFVBQUQ7QUFBWSxRQUFBLElBQUksRUFBRUMsSUFBbEI7QUFBd0IsUUFBQSxHQUFHLEVBQUVFLGFBQTdCO0FBQTRDLFFBQUEsS0FBSyxFQUFFLEVBQW5EO0FBQXVELFFBQUEsTUFBTSxFQUFFO0FBQS9ELFFBQVQ7QUFDSDs7QUFFRCxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQyw0QkFBdEI7QUFBbUQsTUFBQSxVQUFVLEVBQUUsS0FBS2hCLEtBQUwsQ0FBV1IsVUFBMUU7QUFDSSxNQUFBLEtBQUssRUFBRSxLQUFLUSxLQUFMLENBQVdaLEtBRHRCO0FBRUksTUFBQSxTQUFTLEVBQUM7QUFGZCxPQUlJO0FBQUssTUFBQSxFQUFFLEVBQUMsbUJBQVI7QUFBNEIsTUFBQSxTQUFTLEVBQUM7QUFBdEMsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTXlCLE1BRE4sQ0FESixFQUlJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUFtREMsSUFBbkQsQ0FKSixFQUtJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUFxREMsTUFBckQsQ0FMSixDQUpKLEVBV01ILFNBWE4sRUFZSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxhQUFhLEVBQUUsS0FBS1osS0FBTCxDQUFXZixNQUF6QztBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS1ksSUFEL0I7QUFFSSxNQUFBLGtCQUFrQixFQUFFYyxrQkFGeEI7QUFHSSxNQUFBLEtBQUssRUFBRSxDQUFDLEtBQUtYLEtBQUwsQ0FBV1gsU0FIdkI7QUFJSSxNQUFBLFFBQVEsRUFBRSxLQUFLWTtBQUpuQixNQVpKLENBREo7QUFvQkg7QUF4RzJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IE1hdHJpeENsaWVudCB9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7IEdyb3VwTWVtYmVyVHlwZSB9IGZyb20gJy4uLy4uLy4uL2dyb3Vwcyc7XHJcblxyXG4vKlxyXG4gKiBBIGRpYWxvZyBmb3IgY29uZmlybWluZyBhbiBvcGVyYXRpb24gb24gYW5vdGhlciB1c2VyLlxyXG4gKiBUYWtlcyBhIHVzZXIgSUQgYW5kIGEgdmVyYiwgZGlzcGxheXMgdGhlIHRhcmdldCB1c2VyIHByb21pbmVudGx5XHJcbiAqIHN1Y2ggdGhhdCBpdCBzaG91bGQgYmUgZWFzeSB0byBjb25maXJtIHRoYXQgdGhlIG9wZXJhdGlvbiBpcyBiZWluZ1xyXG4gKiBwZXJmb3JtZWQgb24gdGhlIHJpZ2h0IHBlcnNvbiwgYW5kIGRpc3BsYXlzIHRoZSBvcGVyYXRpb24gcHJvbWluZW50bHlcclxuICogdG8gbWFrZSBpdCBvYnZpb3VzIHdoYXQgaXMgZ29pbmcgdG8gaGFwcGVuLlxyXG4gKiBBbHNvIHR3ZWFrcyB0aGUgc3R5bGUgZm9yICdkYW5nZXJvdXMnIGFjdGlvbnMgKGFsYmVpdCBvbmx5IHdpdGggY29sb3VyKVxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0NvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nJyxcclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIG1hdHJpeC1qcy1zZGsgKHJvb20pIG1lbWJlciBvYmplY3QuIFN1cHBseSBlaXRoZXIgdGhpcyBvciAnZ3JvdXBNZW1iZXInXHJcbiAgICAgICAgbWVtYmVyOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgICAgIC8vIGdyb3VwIG1lbWJlciBvYmplY3QuIFN1cHBseSBlaXRoZXIgdGhpcyBvciAnbWVtYmVyJ1xyXG4gICAgICAgIGdyb3VwTWVtYmVyOiBHcm91cE1lbWJlclR5cGUsXHJcbiAgICAgICAgLy8gbmVlZGVkIGlmIGEgZ3JvdXAgbWVtYmVyIGlzIHNwZWNpZmllZFxyXG4gICAgICAgIG1hdHJpeENsaWVudDogUHJvcFR5cGVzLmluc3RhbmNlT2YoTWF0cml4Q2xpZW50KSxcclxuICAgICAgICBhY3Rpb246IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCwgLy8gZWcuICdCYW4nXHJcbiAgICAgICAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCwgLy8gZWcuICdCYW4gdGhpcyB1c2VyPydcclxuXHJcbiAgICAgICAgLy8gV2hldGhlciB0byBkaXNwbGF5IGEgdGV4dCBmaWVsZCBmb3IgYSByZWFzb25cclxuICAgICAgICAvLyBJZiB0cnVlLCB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIG9uRmluaXNoZWQgd2lsbFxyXG4gICAgICAgIC8vIGJlIHRoZSBzdHJpbmcgZW50ZXJlZC5cclxuICAgICAgICBhc2tSZWFzb246IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIGRhbmdlcjogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiAoKSA9PiAoe1xyXG4gICAgICAgIGRhbmdlcjogZmFsc2UsXHJcbiAgICAgICAgYXNrUmVhc29uOiBmYWxzZSxcclxuICAgIH0pLFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBNb3ZlIHRoaXMgdG8gY29uc3RydWN0b3JcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3JlYXNvbkZpZWxkID0gbnVsbDtcclxuICAgIH0sXHJcblxyXG4gICAgb25PazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgbGV0IHJlYXNvbjtcclxuICAgICAgICBpZiAodGhpcy5fcmVhc29uRmllbGQpIHtcclxuICAgICAgICAgICAgcmVhc29uID0gdGhpcy5fcmVhc29uRmllbGQudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0cnVlLCByZWFzb24pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNhbmNlbDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2NvbGxlY3RSZWFzb25GaWVsZDogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHRoaXMuX3JlYXNvbkZpZWxkID0gZTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZGlhbG9ncy5CYXNlRGlhbG9nJyk7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICBjb25zdCBNZW1iZXJBdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuYXZhdGFycy5NZW1iZXJBdmF0YXJcIik7XHJcbiAgICAgICAgY29uc3QgQmFzZUF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5hdmF0YXJzLkJhc2VBdmF0YXJcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbmZpcm1CdXR0b25DbGFzcyA9IHRoaXMucHJvcHMuZGFuZ2VyID8gJ2RhbmdlcicgOiAnJztcclxuXHJcbiAgICAgICAgbGV0IHJlYXNvbkJveDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5hc2tSZWFzb24pIHtcclxuICAgICAgICAgICAgcmVhc29uQm94ID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5vbk9rfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cIm14X0NvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nX3JlYXNvbkZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fY29sbGVjdFJlYXNvbkZpZWxkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e190KFwiUmVhc29uXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGF2YXRhcjtcclxuICAgICAgICBsZXQgbmFtZTtcclxuICAgICAgICBsZXQgdXNlcklkO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm1lbWJlcikge1xyXG4gICAgICAgICAgICBhdmF0YXIgPSA8TWVtYmVyQXZhdGFyIG1lbWJlcj17dGhpcy5wcm9wcy5tZW1iZXJ9IHdpZHRoPXs0OH0gaGVpZ2h0PXs0OH0gLz47XHJcbiAgICAgICAgICAgIG5hbWUgPSB0aGlzLnByb3BzLm1lbWJlci5uYW1lO1xyXG4gICAgICAgICAgICB1c2VySWQgPSB0aGlzLnByb3BzLm1lbWJlci51c2VySWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgaHR0cEF2YXRhclVybCA9IHRoaXMucHJvcHMuZ3JvdXBNZW1iZXIuYXZhdGFyVXJsID9cclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMubWF0cml4Q2xpZW50Lm14Y1VybFRvSHR0cCh0aGlzLnByb3BzLmdyb3VwTWVtYmVyLmF2YXRhclVybCwgNDgsIDQ4KSA6IG51bGw7XHJcbiAgICAgICAgICAgIG5hbWUgPSB0aGlzLnByb3BzLmdyb3VwTWVtYmVyLmRpc3BsYXluYW1lIHx8IHRoaXMucHJvcHMuZ3JvdXBNZW1iZXIudXNlcklkO1xyXG4gICAgICAgICAgICB1c2VySWQgPSB0aGlzLnByb3BzLmdyb3VwTWVtYmVyLnVzZXJJZDtcclxuICAgICAgICAgICAgYXZhdGFyID0gPEJhc2VBdmF0YXIgbmFtZT17bmFtZX0gdXJsPXtodHRwQXZhdGFyVXJsfSB3aWR0aD17NDh9IGhlaWdodD17NDh9IC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPVwibXhfQ29uZmlybVVzZXJBY3Rpb25EaWFsb2dcIiBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGhpcy5wcm9wcy50aXRsZX1cclxuICAgICAgICAgICAgICAgIGNvbnRlbnRJZD0nbXhfRGlhbG9nX2NvbnRlbnQnXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJteF9EaWFsb2dfY29udGVudFwiIGNsYXNzTmFtZT1cIm14X0RpYWxvZ19jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Db25maXJtVXNlckFjdGlvbkRpYWxvZ19hdmF0YXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBhdmF0YXIgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ29uZmlybVVzZXJBY3Rpb25EaWFsb2dfbmFtZVwiPnsgbmFtZSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Db25maXJtVXNlckFjdGlvbkRpYWxvZ191c2VySWRcIj57IHVzZXJJZCB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIHsgcmVhc29uQm94IH1cclxuICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e3RoaXMucHJvcHMuYWN0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLm9uT2t9XHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbkNsYXNzPXtjb25maXJtQnV0dG9uQ2xhc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgZm9jdXM9eyF0aGlzLnByb3BzLmFza1JlYXNvbn1cclxuICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5vbkNhbmNlbH0gLz5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19