"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _UserAddress = require("../../../UserAddress.js");

var _GroupStore = _interopRequireDefault(require("../../../stores/GroupStore"));

var Email = _interopRequireWildcard(require("../../../email"));

var _IdentityAuthClient = _interopRequireDefault(require("../../../IdentityAuthClient"));

var _IdentityServerUtils = require("../../../utils/IdentityServerUtils");

var _UrlUtils = require("../../../utils/UrlUtils");

var _promise = require("../../../utils/promise");

var _Keyboard = require("../../../Keyboard");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018, 2019 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const TRUNCATE_QUERY_LIST = 40;
const QUERY_USER_DIRECTORY_DEBOUNCE_MS = 200;
const addressTypeName = {
  'mx-user-id': (0, _languageHandler._td)("Matrix ID"),
  'mx-room-id': (0, _languageHandler._td)("Matrix Room ID"),
  'email': (0, _languageHandler._td)("email address")
};

var _default = (0, _createReactClass.default)({
  displayName: "AddressPickerDialog",
  propTypes: {
    title: _propTypes.default.string.isRequired,
    description: _propTypes.default.node,
    // Extra node inserted after picker input, dropdown and errors
    extraNode: _propTypes.default.node,
    value: _propTypes.default.string,
    placeholder: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.func]),
    roomId: _propTypes.default.string,
    button: _propTypes.default.string,
    focus: _propTypes.default.bool,
    validAddressTypes: _propTypes.default.arrayOf(_propTypes.default.oneOf(_UserAddress.addressTypes)),
    onFinished: _propTypes.default.func.isRequired,
    groupId: _propTypes.default.string,
    // The type of entity to search for. Default: 'user'.
    pickerType: _propTypes.default.oneOf(['user', 'room']),
    // Whether the current user should be included in the addresses returned. Only
    // applicable when pickerType is `user`. Default: false.
    includeSelf: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      value: "",
      focus: true,
      validAddressTypes: _UserAddress.addressTypes,
      pickerType: 'user',
      includeSelf: false
    };
  },
  getInitialState: function () {
    let validAddressTypes = this.props.validAddressTypes; // Remove email from validAddressTypes if no IS is configured. It may be added at a later stage by the user

    if (!_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl() && validAddressTypes.includes("email")) {
      validAddressTypes = validAddressTypes.filter(type => type !== "email");
    }

    return {
      // Whether to show an error message because of an invalid address
      invalidAddressError: false,
      // List of UserAddressType objects representing
      // the list of addresses we're going to invite
      selectedList: [],
      // Whether a search is ongoing
      busy: false,
      // An error message generated during the user directory search
      searchError: null,
      // Whether the server supports the user_directory API
      serverSupportsUserDirectory: true,
      // The query being searched for
      query: "",
      // List of UserAddressType objects representing the set of
      // auto-completion results for the current search query.
      suggestedList: [],
      // List of address types initialised from props, but may change while the
      // dialog is open and represents the supported list of address types at this time.
      validAddressTypes
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._textinput = (0, _react.createRef)();
  },
  componentDidMount: function () {
    if (this.props.focus) {
      // Set the cursor at the end of the text input
      this._textinput.current.value = this.props.value;
    }
  },

  getPlaceholder() {
    const {
      placeholder
    } = this.props;

    if (typeof placeholder === "string") {
      return placeholder;
    } // Otherwise it's a function, as checked by prop types.


    return placeholder(this.state.validAddressTypes);
  },

  onButtonClick: function () {
    let selectedList = this.state.selectedList.slice(); // Check the text input field to see if user has an unconverted address
    // If there is and it's valid add it to the local selectedList

    if (this._textinput.current.value !== '') {
      selectedList = this._addAddressesToList([this._textinput.current.value]);
      if (selectedList === null) return;
    }

    this.props.onFinished(true, selectedList);
  },
  onCancel: function () {
    this.props.onFinished(false);
  },
  onKeyDown: function (e) {
    const textInput = this._textinput.current ? this._textinput.current.value : undefined;

    if (e.key === _Keyboard.Key.ESCAPE) {
      e.stopPropagation();
      e.preventDefault();
      this.props.onFinished(false);
    } else if (e.key === _Keyboard.Key.ARROW_UP) {
      e.stopPropagation();
      e.preventDefault();
      if (this.addressSelector) this.addressSelector.moveSelectionUp();
    } else if (e.key === _Keyboard.Key.ARROW_DOWN) {
      e.stopPropagation();
      e.preventDefault();
      if (this.addressSelector) this.addressSelector.moveSelectionDown();
    } else if (this.state.suggestedList.length > 0 && [_Keyboard.Key.COMMA, _Keyboard.Key.ENTER, _Keyboard.Key.TAB].includes(e.key)) {
      e.stopPropagation();
      e.preventDefault();
      if (this.addressSelector) this.addressSelector.chooseSelection();
    } else if (textInput.length === 0 && this.state.selectedList.length && e.key === _Keyboard.Key.BACKSPACE) {
      e.stopPropagation();
      e.preventDefault();
      this.onDismissed(this.state.selectedList.length - 1)();
    } else if (e.key === _Keyboard.Key.ENTER) {
      e.stopPropagation();
      e.preventDefault();

      if (textInput === '') {
        // if there's nothing in the input box, submit the form
        this.onButtonClick();
      } else {
        this._addAddressesToList([textInput]);
      }
    } else if (textInput && (e.key === _Keyboard.Key.COMMA || e.key === _Keyboard.Key.TAB)) {
      e.stopPropagation();
      e.preventDefault();

      this._addAddressesToList([textInput]);
    }
  },
  onQueryChanged: function (ev) {
    const query = ev.target.value;

    if (this.queryChangedDebouncer) {
      clearTimeout(this.queryChangedDebouncer);
    } // Only do search if there is something to search


    if (query.length > 0 && query !== '@' && query.length >= 2) {
      this.queryChangedDebouncer = setTimeout(() => {
        if (this.props.pickerType === 'user') {
          if (this.props.groupId) {
            this._doNaiveGroupSearch(query);
          } else if (this.state.serverSupportsUserDirectory) {
            this._doUserDirectorySearch(query);
          } else {
            this._doLocalSearch(query);
          }
        } else if (this.props.pickerType === 'room') {
          if (this.props.groupId) {
            this._doNaiveGroupRoomSearch(query);
          } else {
            this._doRoomSearch(query);
          }
        } else {
          console.error('Unknown pickerType', this.props.pickerType);
        }
      }, QUERY_USER_DIRECTORY_DEBOUNCE_MS);
    } else {
      this.setState({
        suggestedList: [],
        query: "",
        searchError: null
      });
    }
  },
  onDismissed: function (index) {
    return () => {
      const selectedList = this.state.selectedList.slice();
      selectedList.splice(index, 1);
      this.setState({
        selectedList,
        suggestedList: [],
        query: ""
      });
      if (this._cancelThreepidLookup) this._cancelThreepidLookup();
    };
  },
  onClick: function (index) {
    return () => {
      this.onSelected(index);
    };
  },
  onSelected: function (index) {
    const selectedList = this.state.selectedList.slice();
    selectedList.push(this._getFilteredSuggestions()[index]);
    this.setState({
      selectedList,
      suggestedList: [],
      query: ""
    });
    if (this._cancelThreepidLookup) this._cancelThreepidLookup();
  },
  _doNaiveGroupSearch: function (query) {
    const lowerCaseQuery = query.toLowerCase();
    this.setState({
      busy: true,
      query,
      searchError: null
    });

    _MatrixClientPeg.MatrixClientPeg.get().getGroupUsers(this.props.groupId).then(resp => {
      const results = [];
      resp.chunk.forEach(u => {
        const userIdMatch = u.user_id.toLowerCase().includes(lowerCaseQuery);
        const displayNameMatch = (u.displayname || '').toLowerCase().includes(lowerCaseQuery);

        if (!(userIdMatch || displayNameMatch)) {
          return;
        }

        results.push({
          user_id: u.user_id,
          avatar_url: u.avatar_url,
          display_name: u.displayname
        });
      });

      this._processResults(results, query);
    }).catch(err => {
      console.error('Error whilst searching group rooms: ', err);
      this.setState({
        searchError: err.errcode ? err.message : (0, _languageHandler._t)('Something went wrong!')
      });
    }).then(() => {
      this.setState({
        busy: false
      });
    });
  },
  _doNaiveGroupRoomSearch: function (query) {
    const lowerCaseQuery = query.toLowerCase();
    const results = [];

    _GroupStore.default.getGroupRooms(this.props.groupId).forEach(r => {
      const nameMatch = (r.name || '').toLowerCase().includes(lowerCaseQuery);
      const topicMatch = (r.topic || '').toLowerCase().includes(lowerCaseQuery);
      const aliasMatch = (r.canonical_alias || '').toLowerCase().includes(lowerCaseQuery);

      if (!(nameMatch || topicMatch || aliasMatch)) {
        return;
      }

      results.push({
        room_id: r.room_id,
        avatar_url: r.avatar_url,
        name: r.name || r.canonical_alias
      });
    });

    this._processResults(results, query);

    this.setState({
      busy: false
    });
  },
  _doRoomSearch: function (query) {
    const lowerCaseQuery = query.toLowerCase();

    const rooms = _MatrixClientPeg.MatrixClientPeg.get().getRooms();

    const results = [];
    rooms.forEach(room => {
      let rank = Infinity;
      const nameEvent = room.currentState.getStateEvents('m.room.name', '');
      const name = nameEvent ? nameEvent.getContent().name : '';
      const canonicalAlias = room.getCanonicalAlias();
      const aliasEvents = room.currentState.getStateEvents('m.room.aliases');
      const aliases = aliasEvents.map(ev => ev.getContent().aliases).reduce((a, b) => {
        return a.concat(b);
      }, []);
      const nameMatch = (name || '').toLowerCase().includes(lowerCaseQuery);
      let aliasMatch = false;
      let shortestMatchingAliasLength = Infinity;
      aliases.forEach(alias => {
        if ((alias || '').toLowerCase().includes(lowerCaseQuery)) {
          aliasMatch = true;

          if (shortestMatchingAliasLength > alias.length) {
            shortestMatchingAliasLength = alias.length;
          }
        }
      });

      if (!(nameMatch || aliasMatch)) {
        return;
      }

      if (aliasMatch) {
        // A shorter matching alias will give a better rank
        rank = shortestMatchingAliasLength;
      }

      const avatarEvent = room.currentState.getStateEvents('m.room.avatar', '');
      const avatarUrl = avatarEvent ? avatarEvent.getContent().url : undefined;
      results.push({
        rank,
        room_id: room.roomId,
        avatar_url: avatarUrl,
        name: name || canonicalAlias || aliases[0] || (0, _languageHandler._t)('Unnamed Room')
      });
    }); // Sort by rank ascending (a high rank being less relevant)

    const sortedResults = results.sort((a, b) => {
      return a.rank - b.rank;
    });

    this._processResults(sortedResults, query);

    this.setState({
      busy: false
    });
  },
  _doUserDirectorySearch: function (query) {
    this.setState({
      busy: true,
      query,
      searchError: null
    });

    _MatrixClientPeg.MatrixClientPeg.get().searchUserDirectory({
      term: query
    }).then(resp => {
      // The query might have changed since we sent the request, so ignore
      // responses for anything other than the latest query.
      if (this.state.query !== query) {
        return;
      }

      this._processResults(resp.results, query);
    }).catch(err => {
      console.error('Error whilst searching user directory: ', err);
      this.setState({
        searchError: err.errcode ? err.message : (0, _languageHandler._t)('Something went wrong!')
      });

      if (err.errcode === 'M_UNRECOGNIZED') {
        this.setState({
          serverSupportsUserDirectory: false
        }); // Do a local search immediately

        this._doLocalSearch(query);
      }
    }).then(() => {
      this.setState({
        busy: false
      });
    });
  },
  _doLocalSearch: function (query) {
    this.setState({
      query,
      searchError: null
    });
    const queryLowercase = query.toLowerCase();
    const results = [];

    _MatrixClientPeg.MatrixClientPeg.get().getUsers().forEach(user => {
      if (user.userId.toLowerCase().indexOf(queryLowercase) === -1 && user.displayName.toLowerCase().indexOf(queryLowercase) === -1) {
        return;
      } // Put results in the format of the new API


      results.push({
        user_id: user.userId,
        display_name: user.displayName,
        avatar_url: user.avatarUrl
      });
    });

    this._processResults(results, query);
  },
  _processResults: function (results, query) {
    const suggestedList = [];
    results.forEach(result => {
      if (result.room_id) {
        const client = _MatrixClientPeg.MatrixClientPeg.get();

        const room = client.getRoom(result.room_id);

        if (room) {
          const tombstone = room.currentState.getStateEvents('m.room.tombstone', '');

          if (tombstone && tombstone.getContent() && tombstone.getContent()["replacement_room"]) {
            const replacementRoom = client.getRoom(tombstone.getContent()["replacement_room"]); // Skip rooms with tombstones where we are also aware of the replacement room.

            if (replacementRoom) return;
          }
        }

        suggestedList.push({
          addressType: 'mx-room-id',
          address: result.room_id,
          displayName: result.name,
          avatarMxc: result.avatar_url,
          isKnown: true
        });
        return;
      }

      if (!this.props.includeSelf && result.user_id === _MatrixClientPeg.MatrixClientPeg.get().credentials.userId) {
        return;
      } // Return objects, structure of which is defined
      // by UserAddressType


      suggestedList.push({
        addressType: 'mx-user-id',
        address: result.user_id,
        displayName: result.display_name,
        avatarMxc: result.avatar_url,
        isKnown: true
      });
    }); // If the query is a valid address, add an entry for that
    // This is important, otherwise there's no way to invite
    // a perfectly valid address if there are close matches.

    const addrType = (0, _UserAddress.getAddressType)(query);

    if (this.state.validAddressTypes.includes(addrType)) {
      if (addrType === 'email' && !Email.looksValid(query)) {
        this.setState({
          searchError: (0, _languageHandler._t)("That doesn't look like a valid email address")
        });
        return;
      }

      suggestedList.unshift({
        addressType: addrType,
        address: query,
        isKnown: false
      });
      if (this._cancelThreepidLookup) this._cancelThreepidLookup();

      if (addrType === 'email') {
        this._lookupThreepid(addrType, query);
      }
    }

    this.setState({
      suggestedList,
      invalidAddressError: false
    }, () => {
      if (this.addressSelector) this.addressSelector.moveSelectionTop();
    });
  },
  _addAddressesToList: function (addressTexts) {
    const selectedList = this.state.selectedList.slice();
    let hasError = false;
    addressTexts.forEach(addressText => {
      addressText = addressText.trim();
      const addrType = (0, _UserAddress.getAddressType)(addressText);
      const addrObj = {
        addressType: addrType,
        address: addressText,
        isKnown: false
      };

      if (!this.state.validAddressTypes.includes(addrType)) {
        hasError = true;
      } else if (addrType === 'mx-user-id') {
        const user = _MatrixClientPeg.MatrixClientPeg.get().getUser(addrObj.address);

        if (user) {
          addrObj.displayName = user.displayName;
          addrObj.avatarMxc = user.avatarUrl;
          addrObj.isKnown = true;
        }
      } else if (addrType === 'mx-room-id') {
        const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(addrObj.address);

        if (room) {
          addrObj.displayName = room.name;
          addrObj.avatarMxc = room.avatarUrl;
          addrObj.isKnown = true;
        }
      }

      selectedList.push(addrObj);
    });
    this.setState({
      selectedList,
      suggestedList: [],
      query: "",
      invalidAddressError: hasError ? true : this.state.invalidAddressError
    });
    if (this._cancelThreepidLookup) this._cancelThreepidLookup();
    return hasError ? null : selectedList;
  },
  _lookupThreepid: async function (medium, address) {
    let cancelled = false; // Note that we can't safely remove this after we're done
    // because we don't know that it's the same one, so we just
    // leave it: it's replacing the old one each time so it's
    // not like they leak.

    this._cancelThreepidLookup = function () {
      cancelled = true;
    }; // wait a bit to let the user finish typing


    await (0, _promise.sleep)(500);
    if (cancelled) return null;

    try {
      const authClient = new _IdentityAuthClient.default();
      const identityAccessToken = await authClient.getAccessToken();
      if (cancelled) return null;
      const lookup = await _MatrixClientPeg.MatrixClientPeg.get().lookupThreePid(medium, address, undefined
      /* callback */
      , identityAccessToken);
      if (cancelled || lookup === null || !lookup.mxid) return null;
      const profile = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(lookup.mxid);
      if (cancelled || profile === null) return null;
      this.setState({
        suggestedList: [{
          // a UserAddressType
          addressType: medium,
          address: address,
          displayName: profile.displayname,
          avatarMxc: profile.avatar_url,
          isKnown: true
        }]
      });
    } catch (e) {
      console.error(e);
      this.setState({
        searchError: (0, _languageHandler._t)('Something went wrong!')
      });
    }
  },
  _getFilteredSuggestions: function () {
    // map addressType => set of addresses to avoid O(n*m) operation
    const selectedAddresses = {};
    this.state.selectedList.forEach(({
      address,
      addressType
    }) => {
      if (!selectedAddresses[addressType]) selectedAddresses[addressType] = new Set();
      selectedAddresses[addressType].add(address);
    }); // Filter out any addresses in the above already selected addresses (matching both type and address)

    return this.state.suggestedList.filter(({
      address,
      addressType
    }) => {
      return !(selectedAddresses[addressType] && selectedAddresses[addressType].has(address));
    });
  },
  _onPaste: function (e) {
    // Prevent the text being pasted into the textarea
    e.preventDefault();
    const text = e.clipboardData.getData("text"); // Process it as a list of addresses to add instead

    this._addAddressesToList(text.split(/[\s,]+/));
  },

  onUseDefaultIdentityServerClick(e) {
    e.preventDefault(); // Update the IS in account data. Actually using it may trigger terms.
    // eslint-disable-next-line react-hooks/rules-of-hooks

    (0, _IdentityServerUtils.useDefaultIdentityServer)(); // Add email as a valid address type.

    const {
      validAddressTypes
    } = this.state;
    validAddressTypes.push('email');
    this.setState({
      validAddressTypes
    });
  },

  onManageSettingsClick(e) {
    e.preventDefault();

    _dispatcher.default.dispatch({
      action: 'view_user_settings'
    });

    this.onCancel();
  },

  render: function () {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const AddressSelector = sdk.getComponent("elements.AddressSelector");
    this.scrollElement = null;
    let inputLabel;

    if (this.props.description) {
      inputLabel = _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_label"
      }, _react.default.createElement("label", {
        htmlFor: "textinput"
      }, this.props.description));
    }

    const query = []; // create the invite list

    if (this.state.selectedList.length > 0) {
      const AddressTile = sdk.getComponent("elements.AddressTile");

      for (let i = 0; i < this.state.selectedList.length; i++) {
        query.push(_react.default.createElement(AddressTile, {
          key: i,
          address: this.state.selectedList[i],
          canDismiss: true,
          onDismissed: this.onDismissed(i),
          showAddress: this.props.pickerType === 'user'
        }));
      }
    } // Add the query at the end


    query.push(_react.default.createElement("textarea", {
      key: this.state.selectedList.length,
      onPaste: this._onPaste,
      rows: "1",
      id: "textinput",
      ref: this._textinput,
      className: "mx_AddressPickerDialog_input",
      onChange: this.onQueryChanged,
      placeholder: this.getPlaceholder(),
      defaultValue: this.props.value,
      autoFocus: this.props.focus
    }));

    const filteredSuggestedList = this._getFilteredSuggestions();

    let error;
    let addressSelector;

    if (this.state.invalidAddressError) {
      const validTypeDescriptions = this.state.validAddressTypes.map(t => (0, _languageHandler._t)(addressTypeName[t]));
      error = _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_error"
      }, (0, _languageHandler._t)("You have entered an invalid address."), _react.default.createElement("br", null), (0, _languageHandler._t)("Try using one of the following valid address types: %(validTypesList)s.", {
        validTypesList: validTypeDescriptions.join(", ")
      }));
    } else if (this.state.searchError) {
      error = _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_error"
      }, this.state.searchError);
    } else if (this.state.query.length > 0 && filteredSuggestedList.length === 0 && !this.state.busy) {
      error = _react.default.createElement("div", {
        className: "mx_AddressPickerDialog_error"
      }, (0, _languageHandler._t)("No results"));
    } else {
      addressSelector = _react.default.createElement(AddressSelector, {
        ref: ref => {
          this.addressSelector = ref;
        },
        addressList: filteredSuggestedList,
        showAddress: this.props.pickerType === 'user',
        onSelected: this.onSelected,
        truncateAt: TRUNCATE_QUERY_LIST
      });
    }

    let identityServer; // If picker cannot currently accept e-mail but should be able to

    if (this.props.pickerType === 'user' && !this.state.validAddressTypes.includes('email') && this.props.validAddressTypes.includes('email')) {
      const defaultIdentityServerUrl = (0, _IdentityServerUtils.getDefaultIdentityServerUrl)();

      if (defaultIdentityServerUrl) {
        identityServer = _react.default.createElement("div", {
          className: "mx_AddressPickerDialog_identityServer"
        }, (0, _languageHandler._t)("Use an identity server to invite by email. " + "<default>Use the default (%(defaultIdentityServerName)s)</default> " + "or manage in <settings>Settings</settings>.", {
          defaultIdentityServerName: (0, _UrlUtils.abbreviateUrl)(defaultIdentityServerUrl)
        }, {
          default: sub => _react.default.createElement("a", {
            href: "#",
            onClick: this.onUseDefaultIdentityServerClick
          }, sub),
          settings: sub => _react.default.createElement("a", {
            href: "#",
            onClick: this.onManageSettingsClick
          }, sub)
        }));
      } else {
        identityServer = _react.default.createElement("div", {
          className: "mx_AddressPickerDialog_identityServer"
        }, (0, _languageHandler._t)("Use an identity server to invite by email. " + "Manage in <settings>Settings</settings>.", {}, {
          settings: sub => _react.default.createElement("a", {
            href: "#",
            onClick: this.onManageSettingsClick
          }, sub)
        }));
      }
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_AddressPickerDialog",
      onKeyDown: this.onKeyDown,
      onFinished: this.props.onFinished,
      title: this.props.title
    }, inputLabel, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, _react.default.createElement("div", {
      className: "mx_AddressPickerDialog_inputContainer"
    }, query), error, addressSelector, this.props.extraNode, identityServer), _react.default.createElement(DialogButtons, {
      primaryButton: this.props.button,
      onPrimaryButtonClick: this.onButtonClick,
      onCancel: this.onCancel
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvQWRkcmVzc1BpY2tlckRpYWxvZy5qcyJdLCJuYW1lcyI6WyJUUlVOQ0FURV9RVUVSWV9MSVNUIiwiUVVFUllfVVNFUl9ESVJFQ1RPUllfREVCT1VOQ0VfTVMiLCJhZGRyZXNzVHlwZU5hbWUiLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsInRpdGxlIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsImRlc2NyaXB0aW9uIiwibm9kZSIsImV4dHJhTm9kZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJvbmVPZlR5cGUiLCJmdW5jIiwicm9vbUlkIiwiYnV0dG9uIiwiZm9jdXMiLCJib29sIiwidmFsaWRBZGRyZXNzVHlwZXMiLCJhcnJheU9mIiwib25lT2YiLCJhZGRyZXNzVHlwZXMiLCJvbkZpbmlzaGVkIiwiZ3JvdXBJZCIsInBpY2tlclR5cGUiLCJpbmNsdWRlU2VsZiIsImdldERlZmF1bHRQcm9wcyIsImdldEluaXRpYWxTdGF0ZSIsInByb3BzIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwiLCJpbmNsdWRlcyIsImZpbHRlciIsInR5cGUiLCJpbnZhbGlkQWRkcmVzc0Vycm9yIiwic2VsZWN0ZWRMaXN0IiwiYnVzeSIsInNlYXJjaEVycm9yIiwic2VydmVyU3VwcG9ydHNVc2VyRGlyZWN0b3J5IiwicXVlcnkiLCJzdWdnZXN0ZWRMaXN0IiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIl90ZXh0aW5wdXQiLCJjb21wb25lbnREaWRNb3VudCIsImN1cnJlbnQiLCJnZXRQbGFjZWhvbGRlciIsInN0YXRlIiwib25CdXR0b25DbGljayIsInNsaWNlIiwiX2FkZEFkZHJlc3Nlc1RvTGlzdCIsIm9uQ2FuY2VsIiwib25LZXlEb3duIiwiZSIsInRleHRJbnB1dCIsInVuZGVmaW5lZCIsImtleSIsIktleSIsIkVTQ0FQRSIsInN0b3BQcm9wYWdhdGlvbiIsInByZXZlbnREZWZhdWx0IiwiQVJST1dfVVAiLCJhZGRyZXNzU2VsZWN0b3IiLCJtb3ZlU2VsZWN0aW9uVXAiLCJBUlJPV19ET1dOIiwibW92ZVNlbGVjdGlvbkRvd24iLCJsZW5ndGgiLCJDT01NQSIsIkVOVEVSIiwiVEFCIiwiY2hvb3NlU2VsZWN0aW9uIiwiQkFDS1NQQUNFIiwib25EaXNtaXNzZWQiLCJvblF1ZXJ5Q2hhbmdlZCIsImV2IiwidGFyZ2V0IiwicXVlcnlDaGFuZ2VkRGVib3VuY2VyIiwiY2xlYXJUaW1lb3V0Iiwic2V0VGltZW91dCIsIl9kb05haXZlR3JvdXBTZWFyY2giLCJfZG9Vc2VyRGlyZWN0b3J5U2VhcmNoIiwiX2RvTG9jYWxTZWFyY2giLCJfZG9OYWl2ZUdyb3VwUm9vbVNlYXJjaCIsIl9kb1Jvb21TZWFyY2giLCJjb25zb2xlIiwiZXJyb3IiLCJzZXRTdGF0ZSIsImluZGV4Iiwic3BsaWNlIiwiX2NhbmNlbFRocmVlcGlkTG9va3VwIiwib25DbGljayIsIm9uU2VsZWN0ZWQiLCJwdXNoIiwiX2dldEZpbHRlcmVkU3VnZ2VzdGlvbnMiLCJsb3dlckNhc2VRdWVyeSIsInRvTG93ZXJDYXNlIiwiZ2V0R3JvdXBVc2VycyIsInRoZW4iLCJyZXNwIiwicmVzdWx0cyIsImNodW5rIiwiZm9yRWFjaCIsInUiLCJ1c2VySWRNYXRjaCIsInVzZXJfaWQiLCJkaXNwbGF5TmFtZU1hdGNoIiwiZGlzcGxheW5hbWUiLCJhdmF0YXJfdXJsIiwiZGlzcGxheV9uYW1lIiwiX3Byb2Nlc3NSZXN1bHRzIiwiY2F0Y2giLCJlcnIiLCJlcnJjb2RlIiwibWVzc2FnZSIsIkdyb3VwU3RvcmUiLCJnZXRHcm91cFJvb21zIiwiciIsIm5hbWVNYXRjaCIsIm5hbWUiLCJ0b3BpY01hdGNoIiwidG9waWMiLCJhbGlhc01hdGNoIiwiY2Fub25pY2FsX2FsaWFzIiwicm9vbV9pZCIsInJvb21zIiwiZ2V0Um9vbXMiLCJyb29tIiwicmFuayIsIkluZmluaXR5IiwibmFtZUV2ZW50IiwiY3VycmVudFN0YXRlIiwiZ2V0U3RhdGVFdmVudHMiLCJnZXRDb250ZW50IiwiY2Fub25pY2FsQWxpYXMiLCJnZXRDYW5vbmljYWxBbGlhcyIsImFsaWFzRXZlbnRzIiwiYWxpYXNlcyIsIm1hcCIsInJlZHVjZSIsImEiLCJiIiwiY29uY2F0Iiwic2hvcnRlc3RNYXRjaGluZ0FsaWFzTGVuZ3RoIiwiYWxpYXMiLCJhdmF0YXJFdmVudCIsImF2YXRhclVybCIsInVybCIsInNvcnRlZFJlc3VsdHMiLCJzb3J0Iiwic2VhcmNoVXNlckRpcmVjdG9yeSIsInRlcm0iLCJxdWVyeUxvd2VyY2FzZSIsImdldFVzZXJzIiwidXNlciIsInVzZXJJZCIsImluZGV4T2YiLCJyZXN1bHQiLCJjbGllbnQiLCJnZXRSb29tIiwidG9tYnN0b25lIiwicmVwbGFjZW1lbnRSb29tIiwiYWRkcmVzc1R5cGUiLCJhZGRyZXNzIiwiYXZhdGFyTXhjIiwiaXNLbm93biIsImNyZWRlbnRpYWxzIiwiYWRkclR5cGUiLCJFbWFpbCIsImxvb2tzVmFsaWQiLCJ1bnNoaWZ0IiwiX2xvb2t1cFRocmVlcGlkIiwibW92ZVNlbGVjdGlvblRvcCIsImFkZHJlc3NUZXh0cyIsImhhc0Vycm9yIiwiYWRkcmVzc1RleHQiLCJ0cmltIiwiYWRkck9iaiIsImdldFVzZXIiLCJtZWRpdW0iLCJjYW5jZWxsZWQiLCJhdXRoQ2xpZW50IiwiSWRlbnRpdHlBdXRoQ2xpZW50IiwiaWRlbnRpdHlBY2Nlc3NUb2tlbiIsImdldEFjY2Vzc1Rva2VuIiwibG9va3VwIiwibG9va3VwVGhyZWVQaWQiLCJteGlkIiwicHJvZmlsZSIsImdldFByb2ZpbGVJbmZvIiwic2VsZWN0ZWRBZGRyZXNzZXMiLCJTZXQiLCJhZGQiLCJoYXMiLCJfb25QYXN0ZSIsInRleHQiLCJjbGlwYm9hcmREYXRhIiwiZ2V0RGF0YSIsInNwbGl0Iiwib25Vc2VEZWZhdWx0SWRlbnRpdHlTZXJ2ZXJDbGljayIsIm9uTWFuYWdlU2V0dGluZ3NDbGljayIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIkRpYWxvZ0J1dHRvbnMiLCJBZGRyZXNzU2VsZWN0b3IiLCJzY3JvbGxFbGVtZW50IiwiaW5wdXRMYWJlbCIsIkFkZHJlc3NUaWxlIiwiaSIsImZpbHRlcmVkU3VnZ2VzdGVkTGlzdCIsInZhbGlkVHlwZURlc2NyaXB0aW9ucyIsInQiLCJ2YWxpZFR5cGVzTGlzdCIsImpvaW4iLCJyZWYiLCJpZGVudGl0eVNlcnZlciIsImRlZmF1bHRJZGVudGl0eVNlcnZlclVybCIsImRlZmF1bHRJZGVudGl0eVNlcnZlck5hbWUiLCJkZWZhdWx0Iiwic3ViIiwic2V0dGluZ3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBbUJBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQWxDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0NBLE1BQU1BLG1CQUFtQixHQUFHLEVBQTVCO0FBQ0EsTUFBTUMsZ0NBQWdDLEdBQUcsR0FBekM7QUFFQSxNQUFNQyxlQUFlLEdBQUc7QUFDcEIsZ0JBQWMsMEJBQUksV0FBSixDQURNO0FBRXBCLGdCQUFjLDBCQUFJLGdCQUFKLENBRk07QUFHcEIsV0FBUywwQkFBSSxlQUFKO0FBSFcsQ0FBeEI7O2VBT2UsK0JBQWlCO0FBQzVCQyxFQUFBQSxXQUFXLEVBQUUscUJBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxLQUFLLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQURqQjtBQUVQQyxJQUFBQSxXQUFXLEVBQUVILG1CQUFVSSxJQUZoQjtBQUdQO0FBQ0FDLElBQUFBLFNBQVMsRUFBRUwsbUJBQVVJLElBSmQ7QUFLUEUsSUFBQUEsS0FBSyxFQUFFTixtQkFBVUMsTUFMVjtBQU1QTSxJQUFBQSxXQUFXLEVBQUVQLG1CQUFVUSxTQUFWLENBQW9CLENBQUNSLG1CQUFVQyxNQUFYLEVBQW1CRCxtQkFBVVMsSUFBN0IsQ0FBcEIsQ0FOTjtBQU9QQyxJQUFBQSxNQUFNLEVBQUVWLG1CQUFVQyxNQVBYO0FBUVBVLElBQUFBLE1BQU0sRUFBRVgsbUJBQVVDLE1BUlg7QUFTUFcsSUFBQUEsS0FBSyxFQUFFWixtQkFBVWEsSUFUVjtBQVVQQyxJQUFBQSxpQkFBaUIsRUFBRWQsbUJBQVVlLE9BQVYsQ0FBa0JmLG1CQUFVZ0IsS0FBVixDQUFnQkMseUJBQWhCLENBQWxCLENBVlo7QUFXUEMsSUFBQUEsVUFBVSxFQUFFbEIsbUJBQVVTLElBQVYsQ0FBZVAsVUFYcEI7QUFZUGlCLElBQUFBLE9BQU8sRUFBRW5CLG1CQUFVQyxNQVpaO0FBYVA7QUFDQW1CLElBQUFBLFVBQVUsRUFBRXBCLG1CQUFVZ0IsS0FBVixDQUFnQixDQUFDLE1BQUQsRUFBUyxNQUFULENBQWhCLENBZEw7QUFlUDtBQUNBO0FBQ0FLLElBQUFBLFdBQVcsRUFBRXJCLG1CQUFVYTtBQWpCaEIsR0FIaUI7QUF1QjVCUyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hoQixNQUFBQSxLQUFLLEVBQUUsRUFESjtBQUVITSxNQUFBQSxLQUFLLEVBQUUsSUFGSjtBQUdIRSxNQUFBQSxpQkFBaUIsRUFBRUcseUJBSGhCO0FBSUhHLE1BQUFBLFVBQVUsRUFBRSxNQUpUO0FBS0hDLE1BQUFBLFdBQVcsRUFBRTtBQUxWLEtBQVA7QUFPSCxHQS9CMkI7QUFpQzVCRSxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixRQUFJVCxpQkFBaUIsR0FBRyxLQUFLVSxLQUFMLENBQVdWLGlCQUFuQyxDQUR3QixDQUV4Qjs7QUFDQSxRQUFJLENBQUNXLGlDQUFnQkMsR0FBaEIsR0FBc0JDLG9CQUF0QixFQUFELElBQWlEYixpQkFBaUIsQ0FBQ2MsUUFBbEIsQ0FBMkIsT0FBM0IsQ0FBckQsRUFBMEY7QUFDdEZkLE1BQUFBLGlCQUFpQixHQUFHQSxpQkFBaUIsQ0FBQ2UsTUFBbEIsQ0FBeUJDLElBQUksSUFBSUEsSUFBSSxLQUFLLE9BQTFDLENBQXBCO0FBQ0g7O0FBRUQsV0FBTztBQUNIO0FBQ0FDLE1BQUFBLG1CQUFtQixFQUFFLEtBRmxCO0FBR0g7QUFDQTtBQUNBQyxNQUFBQSxZQUFZLEVBQUUsRUFMWDtBQU1IO0FBQ0FDLE1BQUFBLElBQUksRUFBRSxLQVBIO0FBUUg7QUFDQUMsTUFBQUEsV0FBVyxFQUFFLElBVFY7QUFVSDtBQUNBQyxNQUFBQSwyQkFBMkIsRUFBRSxJQVgxQjtBQVlIO0FBQ0FDLE1BQUFBLEtBQUssRUFBRSxFQWJKO0FBY0g7QUFDQTtBQUNBQyxNQUFBQSxhQUFhLEVBQUUsRUFoQlo7QUFpQkg7QUFDQTtBQUNBdkIsTUFBQUE7QUFuQkcsS0FBUDtBQXFCSCxHQTdEMkI7QUErRDVCO0FBQ0F3QixFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUtDLFVBQUwsR0FBa0IsdUJBQWxCO0FBQ0gsR0FsRTJCO0FBb0U1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixRQUFJLEtBQUtoQixLQUFMLENBQVdaLEtBQWYsRUFBc0I7QUFDbEI7QUFDQSxXQUFLMkIsVUFBTCxDQUFnQkUsT0FBaEIsQ0FBd0JuQyxLQUF4QixHQUFnQyxLQUFLa0IsS0FBTCxDQUFXbEIsS0FBM0M7QUFDSDtBQUNKLEdBekUyQjs7QUEyRTVCb0MsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsVUFBTTtBQUFFbkMsTUFBQUE7QUFBRixRQUFrQixLQUFLaUIsS0FBN0I7O0FBQ0EsUUFBSSxPQUFPakIsV0FBUCxLQUF1QixRQUEzQixFQUFxQztBQUNqQyxhQUFPQSxXQUFQO0FBQ0gsS0FKWSxDQUtiOzs7QUFDQSxXQUFPQSxXQUFXLENBQUMsS0FBS29DLEtBQUwsQ0FBVzdCLGlCQUFaLENBQWxCO0FBQ0gsR0FsRjJCOztBQW9GNUI4QixFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QixRQUFJWixZQUFZLEdBQUcsS0FBS1csS0FBTCxDQUFXWCxZQUFYLENBQXdCYSxLQUF4QixFQUFuQixDQURzQixDQUV0QjtBQUNBOztBQUNBLFFBQUksS0FBS04sVUFBTCxDQUFnQkUsT0FBaEIsQ0FBd0JuQyxLQUF4QixLQUFrQyxFQUF0QyxFQUEwQztBQUN0QzBCLE1BQUFBLFlBQVksR0FBRyxLQUFLYyxtQkFBTCxDQUF5QixDQUFDLEtBQUtQLFVBQUwsQ0FBZ0JFLE9BQWhCLENBQXdCbkMsS0FBekIsQ0FBekIsQ0FBZjtBQUNBLFVBQUkwQixZQUFZLEtBQUssSUFBckIsRUFBMkI7QUFDOUI7O0FBQ0QsU0FBS1IsS0FBTCxDQUFXTixVQUFYLENBQXNCLElBQXRCLEVBQTRCYyxZQUE1QjtBQUNILEdBN0YyQjtBQStGNUJlLEVBQUFBLFFBQVEsRUFBRSxZQUFXO0FBQ2pCLFNBQUt2QixLQUFMLENBQVdOLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxHQWpHMkI7QUFtRzVCOEIsRUFBQUEsU0FBUyxFQUFFLFVBQVNDLENBQVQsRUFBWTtBQUNuQixVQUFNQyxTQUFTLEdBQUcsS0FBS1gsVUFBTCxDQUFnQkUsT0FBaEIsR0FBMEIsS0FBS0YsVUFBTCxDQUFnQkUsT0FBaEIsQ0FBd0JuQyxLQUFsRCxHQUEwRDZDLFNBQTVFOztBQUVBLFFBQUlGLENBQUMsQ0FBQ0csR0FBRixLQUFVQyxjQUFJQyxNQUFsQixFQUEwQjtBQUN0QkwsTUFBQUEsQ0FBQyxDQUFDTSxlQUFGO0FBQ0FOLE1BQUFBLENBQUMsQ0FBQ08sY0FBRjtBQUNBLFdBQUtoQyxLQUFMLENBQVdOLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQUpELE1BSU8sSUFBSStCLENBQUMsQ0FBQ0csR0FBRixLQUFVQyxjQUFJSSxRQUFsQixFQUE0QjtBQUMvQlIsTUFBQUEsQ0FBQyxDQUFDTSxlQUFGO0FBQ0FOLE1BQUFBLENBQUMsQ0FBQ08sY0FBRjtBQUNBLFVBQUksS0FBS0UsZUFBVCxFQUEwQixLQUFLQSxlQUFMLENBQXFCQyxlQUFyQjtBQUM3QixLQUpNLE1BSUEsSUFBSVYsQ0FBQyxDQUFDRyxHQUFGLEtBQVVDLGNBQUlPLFVBQWxCLEVBQThCO0FBQ2pDWCxNQUFBQSxDQUFDLENBQUNNLGVBQUY7QUFDQU4sTUFBQUEsQ0FBQyxDQUFDTyxjQUFGO0FBQ0EsVUFBSSxLQUFLRSxlQUFULEVBQTBCLEtBQUtBLGVBQUwsQ0FBcUJHLGlCQUFyQjtBQUM3QixLQUpNLE1BSUEsSUFBSSxLQUFLbEIsS0FBTCxDQUFXTixhQUFYLENBQXlCeUIsTUFBekIsR0FBa0MsQ0FBbEMsSUFBdUMsQ0FBQ1QsY0FBSVUsS0FBTCxFQUFZVixjQUFJVyxLQUFoQixFQUF1QlgsY0FBSVksR0FBM0IsRUFBZ0NyQyxRQUFoQyxDQUF5Q3FCLENBQUMsQ0FBQ0csR0FBM0MsQ0FBM0MsRUFBNEY7QUFDL0ZILE1BQUFBLENBQUMsQ0FBQ00sZUFBRjtBQUNBTixNQUFBQSxDQUFDLENBQUNPLGNBQUY7QUFDQSxVQUFJLEtBQUtFLGVBQVQsRUFBMEIsS0FBS0EsZUFBTCxDQUFxQlEsZUFBckI7QUFDN0IsS0FKTSxNQUlBLElBQUloQixTQUFTLENBQUNZLE1BQVYsS0FBcUIsQ0FBckIsSUFBMEIsS0FBS25CLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QjhCLE1BQWxELElBQTREYixDQUFDLENBQUNHLEdBQUYsS0FBVUMsY0FBSWMsU0FBOUUsRUFBeUY7QUFDNUZsQixNQUFBQSxDQUFDLENBQUNNLGVBQUY7QUFDQU4sTUFBQUEsQ0FBQyxDQUFDTyxjQUFGO0FBQ0EsV0FBS1ksV0FBTCxDQUFpQixLQUFLekIsS0FBTCxDQUFXWCxZQUFYLENBQXdCOEIsTUFBeEIsR0FBaUMsQ0FBbEQ7QUFDSCxLQUpNLE1BSUEsSUFBSWIsQ0FBQyxDQUFDRyxHQUFGLEtBQVVDLGNBQUlXLEtBQWxCLEVBQXlCO0FBQzVCZixNQUFBQSxDQUFDLENBQUNNLGVBQUY7QUFDQU4sTUFBQUEsQ0FBQyxDQUFDTyxjQUFGOztBQUNBLFVBQUlOLFNBQVMsS0FBSyxFQUFsQixFQUFzQjtBQUNsQjtBQUNBLGFBQUtOLGFBQUw7QUFDSCxPQUhELE1BR087QUFDSCxhQUFLRSxtQkFBTCxDQUF5QixDQUFDSSxTQUFELENBQXpCO0FBQ0g7QUFDSixLQVRNLE1BU0EsSUFBSUEsU0FBUyxLQUFLRCxDQUFDLENBQUNHLEdBQUYsS0FBVUMsY0FBSVUsS0FBZCxJQUF1QmQsQ0FBQyxDQUFDRyxHQUFGLEtBQVVDLGNBQUlZLEdBQTFDLENBQWIsRUFBNkQ7QUFDaEVoQixNQUFBQSxDQUFDLENBQUNNLGVBQUY7QUFDQU4sTUFBQUEsQ0FBQyxDQUFDTyxjQUFGOztBQUNBLFdBQUtWLG1CQUFMLENBQXlCLENBQUNJLFNBQUQsQ0FBekI7QUFDSDtBQUNKLEdBeEkyQjtBQTBJNUJtQixFQUFBQSxjQUFjLEVBQUUsVUFBU0MsRUFBVCxFQUFhO0FBQ3pCLFVBQU1sQyxLQUFLLEdBQUdrQyxFQUFFLENBQUNDLE1BQUgsQ0FBVWpFLEtBQXhCOztBQUNBLFFBQUksS0FBS2tFLHFCQUFULEVBQWdDO0FBQzVCQyxNQUFBQSxZQUFZLENBQUMsS0FBS0QscUJBQU4sQ0FBWjtBQUNILEtBSndCLENBS3pCOzs7QUFDQSxRQUFJcEMsS0FBSyxDQUFDMEIsTUFBTixHQUFlLENBQWYsSUFBb0IxQixLQUFLLEtBQUssR0FBOUIsSUFBcUNBLEtBQUssQ0FBQzBCLE1BQU4sSUFBZ0IsQ0FBekQsRUFBNEQ7QUFDeEQsV0FBS1UscUJBQUwsR0FBNkJFLFVBQVUsQ0FBQyxNQUFNO0FBQzFDLFlBQUksS0FBS2xELEtBQUwsQ0FBV0osVUFBWCxLQUEwQixNQUE5QixFQUFzQztBQUNsQyxjQUFJLEtBQUtJLEtBQUwsQ0FBV0wsT0FBZixFQUF3QjtBQUNwQixpQkFBS3dELG1CQUFMLENBQXlCdkMsS0FBekI7QUFDSCxXQUZELE1BRU8sSUFBSSxLQUFLTyxLQUFMLENBQVdSLDJCQUFmLEVBQTRDO0FBQy9DLGlCQUFLeUMsc0JBQUwsQ0FBNEJ4QyxLQUE1QjtBQUNILFdBRk0sTUFFQTtBQUNILGlCQUFLeUMsY0FBTCxDQUFvQnpDLEtBQXBCO0FBQ0g7QUFDSixTQVJELE1BUU8sSUFBSSxLQUFLWixLQUFMLENBQVdKLFVBQVgsS0FBMEIsTUFBOUIsRUFBc0M7QUFDekMsY0FBSSxLQUFLSSxLQUFMLENBQVdMLE9BQWYsRUFBd0I7QUFDcEIsaUJBQUsyRCx1QkFBTCxDQUE2QjFDLEtBQTdCO0FBQ0gsV0FGRCxNQUVPO0FBQ0gsaUJBQUsyQyxhQUFMLENBQW1CM0MsS0FBbkI7QUFDSDtBQUNKLFNBTk0sTUFNQTtBQUNINEMsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsb0JBQWQsRUFBb0MsS0FBS3pELEtBQUwsQ0FBV0osVUFBL0M7QUFDSDtBQUNKLE9BbEJzQyxFQWtCcEN6QixnQ0FsQm9DLENBQXZDO0FBbUJILEtBcEJELE1Bb0JPO0FBQ0gsV0FBS3VGLFFBQUwsQ0FBYztBQUNWN0MsUUFBQUEsYUFBYSxFQUFFLEVBREw7QUFFVkQsUUFBQUEsS0FBSyxFQUFFLEVBRkc7QUFHVkYsUUFBQUEsV0FBVyxFQUFFO0FBSEgsT0FBZDtBQUtIO0FBQ0osR0EzSzJCO0FBNks1QmtDLEVBQUFBLFdBQVcsRUFBRSxVQUFTZSxLQUFULEVBQWdCO0FBQ3pCLFdBQU8sTUFBTTtBQUNULFlBQU1uRCxZQUFZLEdBQUcsS0FBS1csS0FBTCxDQUFXWCxZQUFYLENBQXdCYSxLQUF4QixFQUFyQjtBQUNBYixNQUFBQSxZQUFZLENBQUNvRCxNQUFiLENBQW9CRCxLQUFwQixFQUEyQixDQUEzQjtBQUNBLFdBQUtELFFBQUwsQ0FBYztBQUNWbEQsUUFBQUEsWUFEVTtBQUVWSyxRQUFBQSxhQUFhLEVBQUUsRUFGTDtBQUdWRCxRQUFBQSxLQUFLLEVBQUU7QUFIRyxPQUFkO0FBS0EsVUFBSSxLQUFLaUQscUJBQVQsRUFBZ0MsS0FBS0EscUJBQUw7QUFDbkMsS0FURDtBQVVILEdBeEwyQjtBQTBMNUJDLEVBQUFBLE9BQU8sRUFBRSxVQUFTSCxLQUFULEVBQWdCO0FBQ3JCLFdBQU8sTUFBTTtBQUNULFdBQUtJLFVBQUwsQ0FBZ0JKLEtBQWhCO0FBQ0gsS0FGRDtBQUdILEdBOUwyQjtBQWdNNUJJLEVBQUFBLFVBQVUsRUFBRSxVQUFTSixLQUFULEVBQWdCO0FBQ3hCLFVBQU1uRCxZQUFZLEdBQUcsS0FBS1csS0FBTCxDQUFXWCxZQUFYLENBQXdCYSxLQUF4QixFQUFyQjtBQUNBYixJQUFBQSxZQUFZLENBQUN3RCxJQUFiLENBQWtCLEtBQUtDLHVCQUFMLEdBQStCTixLQUEvQixDQUFsQjtBQUNBLFNBQUtELFFBQUwsQ0FBYztBQUNWbEQsTUFBQUEsWUFEVTtBQUVWSyxNQUFBQSxhQUFhLEVBQUUsRUFGTDtBQUdWRCxNQUFBQSxLQUFLLEVBQUU7QUFIRyxLQUFkO0FBS0EsUUFBSSxLQUFLaUQscUJBQVQsRUFBZ0MsS0FBS0EscUJBQUw7QUFDbkMsR0F6TTJCO0FBMk01QlYsRUFBQUEsbUJBQW1CLEVBQUUsVUFBU3ZDLEtBQVQsRUFBZ0I7QUFDakMsVUFBTXNELGNBQWMsR0FBR3RELEtBQUssQ0FBQ3VELFdBQU4sRUFBdkI7QUFDQSxTQUFLVCxRQUFMLENBQWM7QUFDVmpELE1BQUFBLElBQUksRUFBRSxJQURJO0FBRVZHLE1BQUFBLEtBRlU7QUFHVkYsTUFBQUEsV0FBVyxFQUFFO0FBSEgsS0FBZDs7QUFLQVQscUNBQWdCQyxHQUFoQixHQUFzQmtFLGFBQXRCLENBQW9DLEtBQUtwRSxLQUFMLENBQVdMLE9BQS9DLEVBQXdEMEUsSUFBeEQsQ0FBOERDLElBQUQsSUFBVTtBQUNuRSxZQUFNQyxPQUFPLEdBQUcsRUFBaEI7QUFDQUQsTUFBQUEsSUFBSSxDQUFDRSxLQUFMLENBQVdDLE9BQVgsQ0FBb0JDLENBQUQsSUFBTztBQUN0QixjQUFNQyxXQUFXLEdBQUdELENBQUMsQ0FBQ0UsT0FBRixDQUFVVCxXQUFWLEdBQXdCL0QsUUFBeEIsQ0FBaUM4RCxjQUFqQyxDQUFwQjtBQUNBLGNBQU1XLGdCQUFnQixHQUFHLENBQUNILENBQUMsQ0FBQ0ksV0FBRixJQUFpQixFQUFsQixFQUFzQlgsV0FBdEIsR0FBb0MvRCxRQUFwQyxDQUE2QzhELGNBQTdDLENBQXpCOztBQUNBLFlBQUksRUFBRVMsV0FBVyxJQUFJRSxnQkFBakIsQ0FBSixFQUF3QztBQUNwQztBQUNIOztBQUNETixRQUFBQSxPQUFPLENBQUNQLElBQVIsQ0FBYTtBQUNUWSxVQUFBQSxPQUFPLEVBQUVGLENBQUMsQ0FBQ0UsT0FERjtBQUVURyxVQUFBQSxVQUFVLEVBQUVMLENBQUMsQ0FBQ0ssVUFGTDtBQUdUQyxVQUFBQSxZQUFZLEVBQUVOLENBQUMsQ0FBQ0k7QUFIUCxTQUFiO0FBS0gsT0FYRDs7QUFZQSxXQUFLRyxlQUFMLENBQXFCVixPQUFyQixFQUE4QjNELEtBQTlCO0FBQ0gsS0FmRCxFQWVHc0UsS0FmSCxDQWVVQyxHQUFELElBQVM7QUFDZDNCLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLHNDQUFkLEVBQXNEMEIsR0FBdEQ7QUFDQSxXQUFLekIsUUFBTCxDQUFjO0FBQ1ZoRCxRQUFBQSxXQUFXLEVBQUV5RSxHQUFHLENBQUNDLE9BQUosR0FBY0QsR0FBRyxDQUFDRSxPQUFsQixHQUE0Qix5QkFBRyx1QkFBSDtBQUQvQixPQUFkO0FBR0gsS0FwQkQsRUFvQkdoQixJQXBCSCxDQW9CUSxNQUFNO0FBQ1YsV0FBS1gsUUFBTCxDQUFjO0FBQ1ZqRCxRQUFBQSxJQUFJLEVBQUU7QUFESSxPQUFkO0FBR0gsS0F4QkQ7QUF5QkgsR0EzTzJCO0FBNk81QjZDLEVBQUFBLHVCQUF1QixFQUFFLFVBQVMxQyxLQUFULEVBQWdCO0FBQ3JDLFVBQU1zRCxjQUFjLEdBQUd0RCxLQUFLLENBQUN1RCxXQUFOLEVBQXZCO0FBQ0EsVUFBTUksT0FBTyxHQUFHLEVBQWhCOztBQUNBZSx3QkFBV0MsYUFBWCxDQUF5QixLQUFLdkYsS0FBTCxDQUFXTCxPQUFwQyxFQUE2QzhFLE9BQTdDLENBQXNEZSxDQUFELElBQU87QUFDeEQsWUFBTUMsU0FBUyxHQUFHLENBQUNELENBQUMsQ0FBQ0UsSUFBRixJQUFVLEVBQVgsRUFBZXZCLFdBQWYsR0FBNkIvRCxRQUE3QixDQUFzQzhELGNBQXRDLENBQWxCO0FBQ0EsWUFBTXlCLFVBQVUsR0FBRyxDQUFDSCxDQUFDLENBQUNJLEtBQUYsSUFBVyxFQUFaLEVBQWdCekIsV0FBaEIsR0FBOEIvRCxRQUE5QixDQUF1QzhELGNBQXZDLENBQW5CO0FBQ0EsWUFBTTJCLFVBQVUsR0FBRyxDQUFDTCxDQUFDLENBQUNNLGVBQUYsSUFBcUIsRUFBdEIsRUFBMEIzQixXQUExQixHQUF3Qy9ELFFBQXhDLENBQWlEOEQsY0FBakQsQ0FBbkI7O0FBQ0EsVUFBSSxFQUFFdUIsU0FBUyxJQUFJRSxVQUFiLElBQTJCRSxVQUE3QixDQUFKLEVBQThDO0FBQzFDO0FBQ0g7O0FBQ0R0QixNQUFBQSxPQUFPLENBQUNQLElBQVIsQ0FBYTtBQUNUK0IsUUFBQUEsT0FBTyxFQUFFUCxDQUFDLENBQUNPLE9BREY7QUFFVGhCLFFBQUFBLFVBQVUsRUFBRVMsQ0FBQyxDQUFDVCxVQUZMO0FBR1RXLFFBQUFBLElBQUksRUFBRUYsQ0FBQyxDQUFDRSxJQUFGLElBQVVGLENBQUMsQ0FBQ007QUFIVCxPQUFiO0FBS0gsS0FaRDs7QUFhQSxTQUFLYixlQUFMLENBQXFCVixPQUFyQixFQUE4QjNELEtBQTlCOztBQUNBLFNBQUs4QyxRQUFMLENBQWM7QUFDVmpELE1BQUFBLElBQUksRUFBRTtBQURJLEtBQWQ7QUFHSCxHQWpRMkI7QUFtUTVCOEMsRUFBQUEsYUFBYSxFQUFFLFVBQVMzQyxLQUFULEVBQWdCO0FBQzNCLFVBQU1zRCxjQUFjLEdBQUd0RCxLQUFLLENBQUN1RCxXQUFOLEVBQXZCOztBQUNBLFVBQU02QixLQUFLLEdBQUcvRixpQ0FBZ0JDLEdBQWhCLEdBQXNCK0YsUUFBdEIsRUFBZDs7QUFDQSxVQUFNMUIsT0FBTyxHQUFHLEVBQWhCO0FBQ0F5QixJQUFBQSxLQUFLLENBQUN2QixPQUFOLENBQWV5QixJQUFELElBQVU7QUFDcEIsVUFBSUMsSUFBSSxHQUFHQyxRQUFYO0FBQ0EsWUFBTUMsU0FBUyxHQUFHSCxJQUFJLENBQUNJLFlBQUwsQ0FBa0JDLGNBQWxCLENBQWlDLGFBQWpDLEVBQWdELEVBQWhELENBQWxCO0FBQ0EsWUFBTWIsSUFBSSxHQUFHVyxTQUFTLEdBQUdBLFNBQVMsQ0FBQ0csVUFBVixHQUF1QmQsSUFBMUIsR0FBaUMsRUFBdkQ7QUFDQSxZQUFNZSxjQUFjLEdBQUdQLElBQUksQ0FBQ1EsaUJBQUwsRUFBdkI7QUFDQSxZQUFNQyxXQUFXLEdBQUdULElBQUksQ0FBQ0ksWUFBTCxDQUFrQkMsY0FBbEIsQ0FBaUMsZ0JBQWpDLENBQXBCO0FBQ0EsWUFBTUssT0FBTyxHQUFHRCxXQUFXLENBQUNFLEdBQVosQ0FBaUIvRCxFQUFELElBQVFBLEVBQUUsQ0FBQzBELFVBQUgsR0FBZ0JJLE9BQXhDLEVBQWlERSxNQUFqRCxDQUF3RCxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVTtBQUM5RSxlQUFPRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0QsQ0FBVCxDQUFQO0FBQ0gsT0FGZSxFQUViLEVBRmEsQ0FBaEI7QUFJQSxZQUFNdkIsU0FBUyxHQUFHLENBQUNDLElBQUksSUFBSSxFQUFULEVBQWF2QixXQUFiLEdBQTJCL0QsUUFBM0IsQ0FBb0M4RCxjQUFwQyxDQUFsQjtBQUNBLFVBQUkyQixVQUFVLEdBQUcsS0FBakI7QUFDQSxVQUFJcUIsMkJBQTJCLEdBQUdkLFFBQWxDO0FBQ0FRLE1BQUFBLE9BQU8sQ0FBQ25DLE9BQVIsQ0FBaUIwQyxLQUFELElBQVc7QUFDdkIsWUFBSSxDQUFDQSxLQUFLLElBQUksRUFBVixFQUFjaEQsV0FBZCxHQUE0Qi9ELFFBQTVCLENBQXFDOEQsY0FBckMsQ0FBSixFQUEwRDtBQUN0RDJCLFVBQUFBLFVBQVUsR0FBRyxJQUFiOztBQUNBLGNBQUlxQiwyQkFBMkIsR0FBR0MsS0FBSyxDQUFDN0UsTUFBeEMsRUFBZ0Q7QUFDNUM0RSxZQUFBQSwyQkFBMkIsR0FBR0MsS0FBSyxDQUFDN0UsTUFBcEM7QUFDSDtBQUNKO0FBQ0osT0FQRDs7QUFTQSxVQUFJLEVBQUVtRCxTQUFTLElBQUlJLFVBQWYsQ0FBSixFQUFnQztBQUM1QjtBQUNIOztBQUVELFVBQUlBLFVBQUosRUFBZ0I7QUFDWjtBQUNBTSxRQUFBQSxJQUFJLEdBQUdlLDJCQUFQO0FBQ0g7O0FBRUQsWUFBTUUsV0FBVyxHQUFHbEIsSUFBSSxDQUFDSSxZQUFMLENBQWtCQyxjQUFsQixDQUFpQyxlQUFqQyxFQUFrRCxFQUFsRCxDQUFwQjtBQUNBLFlBQU1jLFNBQVMsR0FBR0QsV0FBVyxHQUFHQSxXQUFXLENBQUNaLFVBQVosR0FBeUJjLEdBQTVCLEdBQWtDM0YsU0FBL0Q7QUFFQTRDLE1BQUFBLE9BQU8sQ0FBQ1AsSUFBUixDQUFhO0FBQ1RtQyxRQUFBQSxJQURTO0FBRVRKLFFBQUFBLE9BQU8sRUFBRUcsSUFBSSxDQUFDaEgsTUFGTDtBQUdUNkYsUUFBQUEsVUFBVSxFQUFFc0MsU0FISDtBQUlUM0IsUUFBQUEsSUFBSSxFQUFFQSxJQUFJLElBQUllLGNBQVIsSUFBMEJHLE9BQU8sQ0FBQyxDQUFELENBQWpDLElBQXdDLHlCQUFHLGNBQUg7QUFKckMsT0FBYjtBQU1ILEtBeENELEVBSjJCLENBOEMzQjs7QUFDQSxVQUFNVyxhQUFhLEdBQUdoRCxPQUFPLENBQUNpRCxJQUFSLENBQWEsQ0FBQ1QsQ0FBRCxFQUFJQyxDQUFKLEtBQVU7QUFDekMsYUFBT0QsQ0FBQyxDQUFDWixJQUFGLEdBQVNhLENBQUMsQ0FBQ2IsSUFBbEI7QUFDSCxLQUZxQixDQUF0Qjs7QUFJQSxTQUFLbEIsZUFBTCxDQUFxQnNDLGFBQXJCLEVBQW9DM0csS0FBcEM7O0FBQ0EsU0FBSzhDLFFBQUwsQ0FBYztBQUNWakQsTUFBQUEsSUFBSSxFQUFFO0FBREksS0FBZDtBQUdILEdBMVQyQjtBQTRUNUIyQyxFQUFBQSxzQkFBc0IsRUFBRSxVQUFTeEMsS0FBVCxFQUFnQjtBQUNwQyxTQUFLOEMsUUFBTCxDQUFjO0FBQ1ZqRCxNQUFBQSxJQUFJLEVBQUUsSUFESTtBQUVWRyxNQUFBQSxLQUZVO0FBR1ZGLE1BQUFBLFdBQVcsRUFBRTtBQUhILEtBQWQ7O0FBS0FULHFDQUFnQkMsR0FBaEIsR0FBc0J1SCxtQkFBdEIsQ0FBMEM7QUFDdENDLE1BQUFBLElBQUksRUFBRTlHO0FBRGdDLEtBQTFDLEVBRUd5RCxJQUZILENBRVNDLElBQUQsSUFBVTtBQUNkO0FBQ0E7QUFDQSxVQUFJLEtBQUtuRCxLQUFMLENBQVdQLEtBQVgsS0FBcUJBLEtBQXpCLEVBQWdDO0FBQzVCO0FBQ0g7O0FBQ0QsV0FBS3FFLGVBQUwsQ0FBcUJYLElBQUksQ0FBQ0MsT0FBMUIsRUFBbUMzRCxLQUFuQztBQUNILEtBVEQsRUFTR3NFLEtBVEgsQ0FTVUMsR0FBRCxJQUFTO0FBQ2QzQixNQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyx5Q0FBZCxFQUF5RDBCLEdBQXpEO0FBQ0EsV0FBS3pCLFFBQUwsQ0FBYztBQUNWaEQsUUFBQUEsV0FBVyxFQUFFeUUsR0FBRyxDQUFDQyxPQUFKLEdBQWNELEdBQUcsQ0FBQ0UsT0FBbEIsR0FBNEIseUJBQUcsdUJBQUg7QUFEL0IsT0FBZDs7QUFHQSxVQUFJRixHQUFHLENBQUNDLE9BQUosS0FBZ0IsZ0JBQXBCLEVBQXNDO0FBQ2xDLGFBQUsxQixRQUFMLENBQWM7QUFDVi9DLFVBQUFBLDJCQUEyQixFQUFFO0FBRG5CLFNBQWQsRUFEa0MsQ0FJbEM7O0FBQ0EsYUFBSzBDLGNBQUwsQ0FBb0J6QyxLQUFwQjtBQUNIO0FBQ0osS0FyQkQsRUFxQkd5RCxJQXJCSCxDQXFCUSxNQUFNO0FBQ1YsV0FBS1gsUUFBTCxDQUFjO0FBQ1ZqRCxRQUFBQSxJQUFJLEVBQUU7QUFESSxPQUFkO0FBR0gsS0F6QkQ7QUEwQkgsR0E1VjJCO0FBOFY1QjRDLEVBQUFBLGNBQWMsRUFBRSxVQUFTekMsS0FBVCxFQUFnQjtBQUM1QixTQUFLOEMsUUFBTCxDQUFjO0FBQ1Y5QyxNQUFBQSxLQURVO0FBRVZGLE1BQUFBLFdBQVcsRUFBRTtBQUZILEtBQWQ7QUFJQSxVQUFNaUgsY0FBYyxHQUFHL0csS0FBSyxDQUFDdUQsV0FBTixFQUF2QjtBQUNBLFVBQU1JLE9BQU8sR0FBRyxFQUFoQjs7QUFDQXRFLHFDQUFnQkMsR0FBaEIsR0FBc0IwSCxRQUF0QixHQUFpQ25ELE9BQWpDLENBQTBDb0QsSUFBRCxJQUFVO0FBQy9DLFVBQUlBLElBQUksQ0FBQ0MsTUFBTCxDQUFZM0QsV0FBWixHQUEwQjRELE9BQTFCLENBQWtDSixjQUFsQyxNQUFzRCxDQUFDLENBQXZELElBQ0FFLElBQUksQ0FBQ3hKLFdBQUwsQ0FBaUI4RixXQUFqQixHQUErQjRELE9BQS9CLENBQXVDSixjQUF2QyxNQUEyRCxDQUFDLENBRGhFLEVBRUU7QUFDRTtBQUNILE9BTDhDLENBTy9DOzs7QUFDQXBELE1BQUFBLE9BQU8sQ0FBQ1AsSUFBUixDQUFhO0FBQ1RZLFFBQUFBLE9BQU8sRUFBRWlELElBQUksQ0FBQ0MsTUFETDtBQUVUOUMsUUFBQUEsWUFBWSxFQUFFNkMsSUFBSSxDQUFDeEosV0FGVjtBQUdUMEcsUUFBQUEsVUFBVSxFQUFFOEMsSUFBSSxDQUFDUjtBQUhSLE9BQWI7QUFLSCxLQWJEOztBQWNBLFNBQUtwQyxlQUFMLENBQXFCVixPQUFyQixFQUE4QjNELEtBQTlCO0FBQ0gsR0FwWDJCO0FBc1g1QnFFLEVBQUFBLGVBQWUsRUFBRSxVQUFTVixPQUFULEVBQWtCM0QsS0FBbEIsRUFBeUI7QUFDdEMsVUFBTUMsYUFBYSxHQUFHLEVBQXRCO0FBQ0EwRCxJQUFBQSxPQUFPLENBQUNFLE9BQVIsQ0FBaUJ1RCxNQUFELElBQVk7QUFDeEIsVUFBSUEsTUFBTSxDQUFDakMsT0FBWCxFQUFvQjtBQUNoQixjQUFNa0MsTUFBTSxHQUFHaEksaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLGNBQU1nRyxJQUFJLEdBQUcrQixNQUFNLENBQUNDLE9BQVAsQ0FBZUYsTUFBTSxDQUFDakMsT0FBdEIsQ0FBYjs7QUFDQSxZQUFJRyxJQUFKLEVBQVU7QUFDTixnQkFBTWlDLFNBQVMsR0FBR2pDLElBQUksQ0FBQ0ksWUFBTCxDQUFrQkMsY0FBbEIsQ0FBaUMsa0JBQWpDLEVBQXFELEVBQXJELENBQWxCOztBQUNBLGNBQUk0QixTQUFTLElBQUlBLFNBQVMsQ0FBQzNCLFVBQVYsRUFBYixJQUF1QzJCLFNBQVMsQ0FBQzNCLFVBQVYsR0FBdUIsa0JBQXZCLENBQTNDLEVBQXVGO0FBQ25GLGtCQUFNNEIsZUFBZSxHQUFHSCxNQUFNLENBQUNDLE9BQVAsQ0FBZUMsU0FBUyxDQUFDM0IsVUFBVixHQUF1QixrQkFBdkIsQ0FBZixDQUF4QixDQURtRixDQUduRjs7QUFDQSxnQkFBSTRCLGVBQUosRUFBcUI7QUFDeEI7QUFDSjs7QUFDRHZILFFBQUFBLGFBQWEsQ0FBQ21ELElBQWQsQ0FBbUI7QUFDZnFFLFVBQUFBLFdBQVcsRUFBRSxZQURFO0FBRWZDLFVBQUFBLE9BQU8sRUFBRU4sTUFBTSxDQUFDakMsT0FGRDtBQUdmMUgsVUFBQUEsV0FBVyxFQUFFMkosTUFBTSxDQUFDdEMsSUFITDtBQUlmNkMsVUFBQUEsU0FBUyxFQUFFUCxNQUFNLENBQUNqRCxVQUpIO0FBS2Z5RCxVQUFBQSxPQUFPLEVBQUU7QUFMTSxTQUFuQjtBQU9BO0FBQ0g7O0FBQ0QsVUFBSSxDQUFDLEtBQUt4SSxLQUFMLENBQVdILFdBQVosSUFDQW1JLE1BQU0sQ0FBQ3BELE9BQVAsS0FBbUIzRSxpQ0FBZ0JDLEdBQWhCLEdBQXNCdUksV0FBdEIsQ0FBa0NYLE1BRHpELEVBRUU7QUFDRTtBQUNILE9BMUJ1QixDQTRCeEI7QUFDQTs7O0FBQ0FqSCxNQUFBQSxhQUFhLENBQUNtRCxJQUFkLENBQW1CO0FBQ2ZxRSxRQUFBQSxXQUFXLEVBQUUsWUFERTtBQUVmQyxRQUFBQSxPQUFPLEVBQUVOLE1BQU0sQ0FBQ3BELE9BRkQ7QUFHZnZHLFFBQUFBLFdBQVcsRUFBRTJKLE1BQU0sQ0FBQ2hELFlBSEw7QUFJZnVELFFBQUFBLFNBQVMsRUFBRVAsTUFBTSxDQUFDakQsVUFKSDtBQUtmeUQsUUFBQUEsT0FBTyxFQUFFO0FBTE0sT0FBbkI7QUFPSCxLQXJDRCxFQUZzQyxDQXlDdEM7QUFDQTtBQUNBOztBQUNBLFVBQU1FLFFBQVEsR0FBRyxpQ0FBZTlILEtBQWYsQ0FBakI7O0FBQ0EsUUFBSSxLQUFLTyxLQUFMLENBQVc3QixpQkFBWCxDQUE2QmMsUUFBN0IsQ0FBc0NzSSxRQUF0QyxDQUFKLEVBQXFEO0FBQ2pELFVBQUlBLFFBQVEsS0FBSyxPQUFiLElBQXdCLENBQUNDLEtBQUssQ0FBQ0MsVUFBTixDQUFpQmhJLEtBQWpCLENBQTdCLEVBQXNEO0FBQ2xELGFBQUs4QyxRQUFMLENBQWM7QUFBQ2hELFVBQUFBLFdBQVcsRUFBRSx5QkFBRyw4Q0FBSDtBQUFkLFNBQWQ7QUFDQTtBQUNIOztBQUNERyxNQUFBQSxhQUFhLENBQUNnSSxPQUFkLENBQXNCO0FBQ2xCUixRQUFBQSxXQUFXLEVBQUVLLFFBREs7QUFFbEJKLFFBQUFBLE9BQU8sRUFBRTFILEtBRlM7QUFHbEI0SCxRQUFBQSxPQUFPLEVBQUU7QUFIUyxPQUF0QjtBQUtBLFVBQUksS0FBSzNFLHFCQUFULEVBQWdDLEtBQUtBLHFCQUFMOztBQUNoQyxVQUFJNkUsUUFBUSxLQUFLLE9BQWpCLEVBQTBCO0FBQ3RCLGFBQUtJLGVBQUwsQ0FBcUJKLFFBQXJCLEVBQStCOUgsS0FBL0I7QUFDSDtBQUNKOztBQUNELFNBQUs4QyxRQUFMLENBQWM7QUFDVjdDLE1BQUFBLGFBRFU7QUFFVk4sTUFBQUEsbUJBQW1CLEVBQUU7QUFGWCxLQUFkLEVBR0csTUFBTTtBQUNMLFVBQUksS0FBSzJCLGVBQVQsRUFBMEIsS0FBS0EsZUFBTCxDQUFxQjZHLGdCQUFyQjtBQUM3QixLQUxEO0FBTUgsR0F4YjJCO0FBMGI1QnpILEVBQUFBLG1CQUFtQixFQUFFLFVBQVMwSCxZQUFULEVBQXVCO0FBQ3hDLFVBQU14SSxZQUFZLEdBQUcsS0FBS1csS0FBTCxDQUFXWCxZQUFYLENBQXdCYSxLQUF4QixFQUFyQjtBQUVBLFFBQUk0SCxRQUFRLEdBQUcsS0FBZjtBQUNBRCxJQUFBQSxZQUFZLENBQUN2RSxPQUFiLENBQXNCeUUsV0FBRCxJQUFpQjtBQUNsQ0EsTUFBQUEsV0FBVyxHQUFHQSxXQUFXLENBQUNDLElBQVosRUFBZDtBQUNBLFlBQU1ULFFBQVEsR0FBRyxpQ0FBZVEsV0FBZixDQUFqQjtBQUNBLFlBQU1FLE9BQU8sR0FBRztBQUNaZixRQUFBQSxXQUFXLEVBQUVLLFFBREQ7QUFFWkosUUFBQUEsT0FBTyxFQUFFWSxXQUZHO0FBR1pWLFFBQUFBLE9BQU8sRUFBRTtBQUhHLE9BQWhCOztBQU1BLFVBQUksQ0FBQyxLQUFLckgsS0FBTCxDQUFXN0IsaUJBQVgsQ0FBNkJjLFFBQTdCLENBQXNDc0ksUUFBdEMsQ0FBTCxFQUFzRDtBQUNsRE8sUUFBQUEsUUFBUSxHQUFHLElBQVg7QUFDSCxPQUZELE1BRU8sSUFBSVAsUUFBUSxLQUFLLFlBQWpCLEVBQStCO0FBQ2xDLGNBQU1iLElBQUksR0FBRzVILGlDQUFnQkMsR0FBaEIsR0FBc0JtSixPQUF0QixDQUE4QkQsT0FBTyxDQUFDZCxPQUF0QyxDQUFiOztBQUNBLFlBQUlULElBQUosRUFBVTtBQUNOdUIsVUFBQUEsT0FBTyxDQUFDL0ssV0FBUixHQUFzQndKLElBQUksQ0FBQ3hKLFdBQTNCO0FBQ0ErSyxVQUFBQSxPQUFPLENBQUNiLFNBQVIsR0FBb0JWLElBQUksQ0FBQ1IsU0FBekI7QUFDQStCLFVBQUFBLE9BQU8sQ0FBQ1osT0FBUixHQUFrQixJQUFsQjtBQUNIO0FBQ0osT0FQTSxNQU9BLElBQUlFLFFBQVEsS0FBSyxZQUFqQixFQUErQjtBQUNsQyxjQUFNeEMsSUFBSSxHQUFHakcsaUNBQWdCQyxHQUFoQixHQUFzQmdJLE9BQXRCLENBQThCa0IsT0FBTyxDQUFDZCxPQUF0QyxDQUFiOztBQUNBLFlBQUlwQyxJQUFKLEVBQVU7QUFDTmtELFVBQUFBLE9BQU8sQ0FBQy9LLFdBQVIsR0FBc0I2SCxJQUFJLENBQUNSLElBQTNCO0FBQ0EwRCxVQUFBQSxPQUFPLENBQUNiLFNBQVIsR0FBb0JyQyxJQUFJLENBQUNtQixTQUF6QjtBQUNBK0IsVUFBQUEsT0FBTyxDQUFDWixPQUFSLEdBQWtCLElBQWxCO0FBQ0g7QUFDSjs7QUFFRGhJLE1BQUFBLFlBQVksQ0FBQ3dELElBQWIsQ0FBa0JvRixPQUFsQjtBQUNILEtBNUJEO0FBOEJBLFNBQUsxRixRQUFMLENBQWM7QUFDVmxELE1BQUFBLFlBRFU7QUFFVkssTUFBQUEsYUFBYSxFQUFFLEVBRkw7QUFHVkQsTUFBQUEsS0FBSyxFQUFFLEVBSEc7QUFJVkwsTUFBQUEsbUJBQW1CLEVBQUUwSSxRQUFRLEdBQUcsSUFBSCxHQUFVLEtBQUs5SCxLQUFMLENBQVdaO0FBSnhDLEtBQWQ7QUFNQSxRQUFJLEtBQUtzRCxxQkFBVCxFQUFnQyxLQUFLQSxxQkFBTDtBQUNoQyxXQUFPb0YsUUFBUSxHQUFHLElBQUgsR0FBVXpJLFlBQXpCO0FBQ0gsR0FwZTJCO0FBc2U1QnNJLEVBQUFBLGVBQWUsRUFBRSxnQkFBZVEsTUFBZixFQUF1QmhCLE9BQXZCLEVBQWdDO0FBQzdDLFFBQUlpQixTQUFTLEdBQUcsS0FBaEIsQ0FENkMsQ0FFN0M7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsU0FBSzFGLHFCQUFMLEdBQTZCLFlBQVc7QUFDcEMwRixNQUFBQSxTQUFTLEdBQUcsSUFBWjtBQUNILEtBRkQsQ0FONkMsQ0FVN0M7OztBQUNBLFVBQU0sb0JBQU0sR0FBTixDQUFOO0FBQ0EsUUFBSUEsU0FBSixFQUFlLE9BQU8sSUFBUDs7QUFFZixRQUFJO0FBQ0EsWUFBTUMsVUFBVSxHQUFHLElBQUlDLDJCQUFKLEVBQW5CO0FBQ0EsWUFBTUMsbUJBQW1CLEdBQUcsTUFBTUYsVUFBVSxDQUFDRyxjQUFYLEVBQWxDO0FBQ0EsVUFBSUosU0FBSixFQUFlLE9BQU8sSUFBUDtBQUVmLFlBQU1LLE1BQU0sR0FBRyxNQUFNM0osaUNBQWdCQyxHQUFoQixHQUFzQjJKLGNBQXRCLENBQ2pCUCxNQURpQixFQUVqQmhCLE9BRmlCLEVBR2pCM0c7QUFBVTtBQUhPLFFBSWpCK0gsbUJBSmlCLENBQXJCO0FBTUEsVUFBSUgsU0FBUyxJQUFJSyxNQUFNLEtBQUssSUFBeEIsSUFBZ0MsQ0FBQ0EsTUFBTSxDQUFDRSxJQUE1QyxFQUFrRCxPQUFPLElBQVA7QUFFbEQsWUFBTUMsT0FBTyxHQUFHLE1BQU05SixpQ0FBZ0JDLEdBQWhCLEdBQXNCOEosY0FBdEIsQ0FBcUNKLE1BQU0sQ0FBQ0UsSUFBNUMsQ0FBdEI7QUFDQSxVQUFJUCxTQUFTLElBQUlRLE9BQU8sS0FBSyxJQUE3QixFQUFtQyxPQUFPLElBQVA7QUFFbkMsV0FBS3JHLFFBQUwsQ0FBYztBQUNWN0MsUUFBQUEsYUFBYSxFQUFFLENBQUM7QUFDWjtBQUNBd0gsVUFBQUEsV0FBVyxFQUFFaUIsTUFGRDtBQUdaaEIsVUFBQUEsT0FBTyxFQUFFQSxPQUhHO0FBSVpqSyxVQUFBQSxXQUFXLEVBQUUwTCxPQUFPLENBQUNqRixXQUpUO0FBS1p5RCxVQUFBQSxTQUFTLEVBQUV3QixPQUFPLENBQUNoRixVQUxQO0FBTVp5RCxVQUFBQSxPQUFPLEVBQUU7QUFORyxTQUFEO0FBREwsT0FBZDtBQVVILEtBMUJELENBMEJFLE9BQU8vRyxDQUFQLEVBQVU7QUFDUitCLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjaEMsQ0FBZDtBQUNBLFdBQUtpQyxRQUFMLENBQWM7QUFDVmhELFFBQUFBLFdBQVcsRUFBRSx5QkFBRyx1QkFBSDtBQURILE9BQWQ7QUFHSDtBQUNKLEdBcGhCMkI7QUFzaEI1QnVELEVBQUFBLHVCQUF1QixFQUFFLFlBQVc7QUFDaEM7QUFDQSxVQUFNZ0csaUJBQWlCLEdBQUcsRUFBMUI7QUFDQSxTQUFLOUksS0FBTCxDQUFXWCxZQUFYLENBQXdCaUUsT0FBeEIsQ0FBZ0MsQ0FBQztBQUFDNkQsTUFBQUEsT0FBRDtBQUFVRCxNQUFBQTtBQUFWLEtBQUQsS0FBNEI7QUFDeEQsVUFBSSxDQUFDNEIsaUJBQWlCLENBQUM1QixXQUFELENBQXRCLEVBQXFDNEIsaUJBQWlCLENBQUM1QixXQUFELENBQWpCLEdBQWlDLElBQUk2QixHQUFKLEVBQWpDO0FBQ3JDRCxNQUFBQSxpQkFBaUIsQ0FBQzVCLFdBQUQsQ0FBakIsQ0FBK0I4QixHQUEvQixDQUFtQzdCLE9BQW5DO0FBQ0gsS0FIRCxFQUhnQyxDQVFoQzs7QUFDQSxXQUFPLEtBQUtuSCxLQUFMLENBQVdOLGFBQVgsQ0FBeUJSLE1BQXpCLENBQWdDLENBQUM7QUFBQ2lJLE1BQUFBLE9BQUQ7QUFBVUQsTUFBQUE7QUFBVixLQUFELEtBQTRCO0FBQy9ELGFBQU8sRUFBRTRCLGlCQUFpQixDQUFDNUIsV0FBRCxDQUFqQixJQUFrQzRCLGlCQUFpQixDQUFDNUIsV0FBRCxDQUFqQixDQUErQitCLEdBQS9CLENBQW1DOUIsT0FBbkMsQ0FBcEMsQ0FBUDtBQUNILEtBRk0sQ0FBUDtBQUdILEdBbGlCMkI7QUFvaUI1QitCLEVBQUFBLFFBQVEsRUFBRSxVQUFTNUksQ0FBVCxFQUFZO0FBQ2xCO0FBQ0FBLElBQUFBLENBQUMsQ0FBQ08sY0FBRjtBQUNBLFVBQU1zSSxJQUFJLEdBQUc3SSxDQUFDLENBQUM4SSxhQUFGLENBQWdCQyxPQUFoQixDQUF3QixNQUF4QixDQUFiLENBSGtCLENBSWxCOztBQUNBLFNBQUtsSixtQkFBTCxDQUF5QmdKLElBQUksQ0FBQ0csS0FBTCxDQUFXLFFBQVgsQ0FBekI7QUFDSCxHQTFpQjJCOztBQTRpQjVCQyxFQUFBQSwrQkFBK0IsQ0FBQ2pKLENBQUQsRUFBSTtBQUMvQkEsSUFBQUEsQ0FBQyxDQUFDTyxjQUFGLEdBRCtCLENBRy9CO0FBQ0E7O0FBQ0EseURBTCtCLENBTy9COztBQUNBLFVBQU07QUFBRTFDLE1BQUFBO0FBQUYsUUFBd0IsS0FBSzZCLEtBQW5DO0FBQ0E3QixJQUFBQSxpQkFBaUIsQ0FBQzBFLElBQWxCLENBQXVCLE9BQXZCO0FBQ0EsU0FBS04sUUFBTCxDQUFjO0FBQUVwRSxNQUFBQTtBQUFGLEtBQWQ7QUFDSCxHQXZqQjJCOztBQXlqQjVCcUwsRUFBQUEscUJBQXFCLENBQUNsSixDQUFELEVBQUk7QUFDckJBLElBQUFBLENBQUMsQ0FBQ08sY0FBRjs7QUFDQTRJLHdCQUFJQyxRQUFKLENBQWE7QUFBRUMsTUFBQUEsTUFBTSxFQUFFO0FBQVYsS0FBYjs7QUFDQSxTQUFLdkosUUFBTDtBQUNILEdBN2pCMkI7O0FBK2pCNUJ3SixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLFVBQU1DLGFBQWEsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFVBQU1FLGVBQWUsR0FBR0gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUF4QjtBQUNBLFNBQUtHLGFBQUwsR0FBcUIsSUFBckI7QUFFQSxRQUFJQyxVQUFKOztBQUNBLFFBQUksS0FBS3RMLEtBQUwsQ0FBV3JCLFdBQWYsRUFBNEI7QUFDeEIyTSxNQUFBQSxVQUFVLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1Q7QUFBTyxRQUFBLE9BQU8sRUFBQztBQUFmLFNBQTRCLEtBQUt0TCxLQUFMLENBQVdyQixXQUF2QyxDQURTLENBQWI7QUFHSDs7QUFFRCxVQUFNaUMsS0FBSyxHQUFHLEVBQWQsQ0FiZSxDQWNmOztBQUNBLFFBQUksS0FBS08sS0FBTCxDQUFXWCxZQUFYLENBQXdCOEIsTUFBeEIsR0FBaUMsQ0FBckMsRUFBd0M7QUFDcEMsWUFBTWlKLFdBQVcsR0FBR04sR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFwQjs7QUFDQSxXQUFLLElBQUlNLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS3JLLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QjhCLE1BQTVDLEVBQW9Ea0osQ0FBQyxFQUFyRCxFQUF5RDtBQUNyRDVLLFFBQUFBLEtBQUssQ0FBQ29ELElBQU4sQ0FDSSw2QkFBQyxXQUFEO0FBQ0ksVUFBQSxHQUFHLEVBQUV3SCxDQURUO0FBRUksVUFBQSxPQUFPLEVBQUUsS0FBS3JLLEtBQUwsQ0FBV1gsWUFBWCxDQUF3QmdMLENBQXhCLENBRmI7QUFHSSxVQUFBLFVBQVUsRUFBRSxJQUhoQjtBQUlJLFVBQUEsV0FBVyxFQUFFLEtBQUs1SSxXQUFMLENBQWlCNEksQ0FBakIsQ0FKakI7QUFLSSxVQUFBLFdBQVcsRUFBRSxLQUFLeEwsS0FBTCxDQUFXSixVQUFYLEtBQTBCO0FBTDNDLFVBREo7QUFRSDtBQUNKLEtBM0JjLENBNkJmOzs7QUFDQWdCLElBQUFBLEtBQUssQ0FBQ29ELElBQU4sQ0FDSTtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUs3QyxLQUFMLENBQVdYLFlBQVgsQ0FBd0I4QixNQURqQztBQUVJLE1BQUEsT0FBTyxFQUFFLEtBQUsrSCxRQUZsQjtBQUdJLE1BQUEsSUFBSSxFQUFDLEdBSFQ7QUFJSSxNQUFBLEVBQUUsRUFBQyxXQUpQO0FBS0ksTUFBQSxHQUFHLEVBQUUsS0FBS3RKLFVBTGQ7QUFNSSxNQUFBLFNBQVMsRUFBQyw4QkFOZDtBQU9JLE1BQUEsUUFBUSxFQUFFLEtBQUs4QixjQVBuQjtBQVFJLE1BQUEsV0FBVyxFQUFFLEtBQUszQixjQUFMLEVBUmpCO0FBU0ksTUFBQSxZQUFZLEVBQUUsS0FBS2xCLEtBQUwsQ0FBV2xCLEtBVDdCO0FBVUksTUFBQSxTQUFTLEVBQUUsS0FBS2tCLEtBQUwsQ0FBV1o7QUFWMUIsTUFESjs7QUFlQSxVQUFNcU0scUJBQXFCLEdBQUcsS0FBS3hILHVCQUFMLEVBQTlCOztBQUVBLFFBQUlSLEtBQUo7QUFDQSxRQUFJdkIsZUFBSjs7QUFDQSxRQUFJLEtBQUtmLEtBQUwsQ0FBV1osbUJBQWYsRUFBb0M7QUFDaEMsWUFBTW1MLHFCQUFxQixHQUFHLEtBQUt2SyxLQUFMLENBQVc3QixpQkFBWCxDQUE2QnVILEdBQTdCLENBQWtDOEUsQ0FBRCxJQUFPLHlCQUFHdk4sZUFBZSxDQUFDdU4sQ0FBRCxDQUFsQixDQUF4QyxDQUE5QjtBQUNBbEksTUFBQUEsS0FBSyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNGLHlCQUFHLHNDQUFILENBREUsRUFFSix3Q0FGSSxFQUdGLHlCQUFHLHlFQUFILEVBQThFO0FBQzVFbUksUUFBQUEsY0FBYyxFQUFFRixxQkFBcUIsQ0FBQ0csSUFBdEIsQ0FBMkIsSUFBM0I7QUFENEQsT0FBOUUsQ0FIRSxDQUFSO0FBT0gsS0FURCxNQVNPLElBQUksS0FBSzFLLEtBQUwsQ0FBV1QsV0FBZixFQUE0QjtBQUMvQitDLE1BQUFBLEtBQUssR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBZ0QsS0FBS3RDLEtBQUwsQ0FBV1QsV0FBM0QsQ0FBUjtBQUNILEtBRk0sTUFFQSxJQUFJLEtBQUtTLEtBQUwsQ0FBV1AsS0FBWCxDQUFpQjBCLE1BQWpCLEdBQTBCLENBQTFCLElBQStCbUoscUJBQXFCLENBQUNuSixNQUF0QixLQUFpQyxDQUFoRSxJQUFxRSxDQUFDLEtBQUtuQixLQUFMLENBQVdWLElBQXJGLEVBQTJGO0FBQzlGZ0QsTUFBQUEsS0FBSyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUFnRCx5QkFBRyxZQUFILENBQWhELENBQVI7QUFDSCxLQUZNLE1BRUE7QUFDSHZCLE1BQUFBLGVBQWUsR0FDWCw2QkFBQyxlQUFEO0FBQWlCLFFBQUEsR0FBRyxFQUFHNEosR0FBRCxJQUFTO0FBQUMsZUFBSzVKLGVBQUwsR0FBdUI0SixHQUF2QjtBQUE0QixTQUE1RDtBQUNJLFFBQUEsV0FBVyxFQUFFTCxxQkFEakI7QUFFSSxRQUFBLFdBQVcsRUFBRSxLQUFLekwsS0FBTCxDQUFXSixVQUFYLEtBQTBCLE1BRjNDO0FBR0ksUUFBQSxVQUFVLEVBQUUsS0FBS21FLFVBSHJCO0FBSUksUUFBQSxVQUFVLEVBQUU3RjtBQUpoQixRQURKO0FBUUg7O0FBRUQsUUFBSTZOLGNBQUosQ0F6RWUsQ0EwRWY7O0FBQ0EsUUFBSSxLQUFLL0wsS0FBTCxDQUFXSixVQUFYLEtBQTBCLE1BQTFCLElBQW9DLENBQUMsS0FBS3VCLEtBQUwsQ0FBVzdCLGlCQUFYLENBQTZCYyxRQUE3QixDQUFzQyxPQUF0QyxDQUFyQyxJQUNHLEtBQUtKLEtBQUwsQ0FBV1YsaUJBQVgsQ0FBNkJjLFFBQTdCLENBQXNDLE9BQXRDLENBRFAsRUFDdUQ7QUFDbkQsWUFBTTRMLHdCQUF3QixHQUFHLHVEQUFqQzs7QUFDQSxVQUFJQSx3QkFBSixFQUE4QjtBQUMxQkQsUUFBQUEsY0FBYyxHQUFHO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUF3RCx5QkFDckUsZ0RBQ0EscUVBREEsR0FFQSw2Q0FIcUUsRUFJckU7QUFDSUUsVUFBQUEseUJBQXlCLEVBQUUsNkJBQWNELHdCQUFkO0FBRC9CLFNBSnFFLEVBT3JFO0FBQ0lFLFVBQUFBLE9BQU8sRUFBRUMsR0FBRyxJQUFJO0FBQUcsWUFBQSxJQUFJLEVBQUMsR0FBUjtBQUFZLFlBQUEsT0FBTyxFQUFFLEtBQUt6QjtBQUExQixhQUE0RHlCLEdBQTVELENBRHBCO0FBRUlDLFVBQUFBLFFBQVEsRUFBRUQsR0FBRyxJQUFJO0FBQUcsWUFBQSxJQUFJLEVBQUMsR0FBUjtBQUFZLFlBQUEsT0FBTyxFQUFFLEtBQUt4QjtBQUExQixhQUFrRHdCLEdBQWxEO0FBRnJCLFNBUHFFLENBQXhELENBQWpCO0FBWUgsT0FiRCxNQWFPO0FBQ0hKLFFBQUFBLGNBQWMsR0FBRztBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FBd0QseUJBQ3JFLGdEQUNBLDBDQUZxRSxFQUdyRSxFQUhxRSxFQUdqRTtBQUNBSyxVQUFBQSxRQUFRLEVBQUVELEdBQUcsSUFBSTtBQUFHLFlBQUEsSUFBSSxFQUFDLEdBQVI7QUFBWSxZQUFBLE9BQU8sRUFBRSxLQUFLeEI7QUFBMUIsYUFBa0R3QixHQUFsRDtBQURqQixTQUhpRSxDQUF4RCxDQUFqQjtBQU9IO0FBQ0o7O0FBRUQsV0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxTQUFTLEVBQUMsd0JBQXRCO0FBQStDLE1BQUEsU0FBUyxFQUFFLEtBQUszSyxTQUEvRDtBQUNJLE1BQUEsVUFBVSxFQUFFLEtBQUt4QixLQUFMLENBQVdOLFVBRDNCO0FBQ3VDLE1BQUEsS0FBSyxFQUFFLEtBQUtNLEtBQUwsQ0FBV3pCO0FBRHpELE9BRUsrTSxVQUZMLEVBR0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlEMUssS0FBekQsQ0FESixFQUVNNkMsS0FGTixFQUdNdkIsZUFITixFQUlNLEtBQUtsQyxLQUFMLENBQVduQixTQUpqQixFQUtNa04sY0FMTixDQUhKLEVBVUksNkJBQUMsYUFBRDtBQUFlLE1BQUEsYUFBYSxFQUFFLEtBQUsvTCxLQUFMLENBQVdiLE1BQXpDO0FBQ0ksTUFBQSxvQkFBb0IsRUFBRSxLQUFLaUMsYUFEL0I7QUFFSSxNQUFBLFFBQVEsRUFBRSxLQUFLRztBQUZuQixNQVZKLENBREo7QUFnQkg7QUFyckIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcblxyXG5pbXBvcnQgeyBfdCwgX3RkIH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7IGFkZHJlc3NUeXBlcywgZ2V0QWRkcmVzc1R5cGUgfSBmcm9tICcuLi8uLi8uLi9Vc2VyQWRkcmVzcy5qcyc7XHJcbmltcG9ydCBHcm91cFN0b3JlIGZyb20gJy4uLy4uLy4uL3N0b3Jlcy9Hcm91cFN0b3JlJztcclxuaW1wb3J0ICogYXMgRW1haWwgZnJvbSAnLi4vLi4vLi4vZW1haWwnO1xyXG5pbXBvcnQgSWRlbnRpdHlBdXRoQ2xpZW50IGZyb20gJy4uLy4uLy4uL0lkZW50aXR5QXV0aENsaWVudCc7XHJcbmltcG9ydCB7IGdldERlZmF1bHRJZGVudGl0eVNlcnZlclVybCwgdXNlRGVmYXVsdElkZW50aXR5U2VydmVyIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvSWRlbnRpdHlTZXJ2ZXJVdGlscyc7XHJcbmltcG9ydCB7IGFiYnJldmlhdGVVcmwgfSBmcm9tICcuLi8uLi8uLi91dGlscy9VcmxVdGlscyc7XHJcbmltcG9ydCB7c2xlZXB9IGZyb20gXCIuLi8uLi8uLi91dGlscy9wcm9taXNlXCI7XHJcbmltcG9ydCB7S2V5fSBmcm9tIFwiLi4vLi4vLi4vS2V5Ym9hcmRcIjtcclxuXHJcbmNvbnN0IFRSVU5DQVRFX1FVRVJZX0xJU1QgPSA0MDtcclxuY29uc3QgUVVFUllfVVNFUl9ESVJFQ1RPUllfREVCT1VOQ0VfTVMgPSAyMDA7XHJcblxyXG5jb25zdCBhZGRyZXNzVHlwZU5hbWUgPSB7XHJcbiAgICAnbXgtdXNlci1pZCc6IF90ZChcIk1hdHJpeCBJRFwiKSxcclxuICAgICdteC1yb29tLWlkJzogX3RkKFwiTWF0cml4IFJvb20gSURcIiksXHJcbiAgICAnZW1haWwnOiBfdGQoXCJlbWFpbCBhZGRyZXNzXCIpLFxyXG59O1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6IFwiQWRkcmVzc1BpY2tlckRpYWxvZ1wiLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IFByb3BUeXBlcy5ub2RlLFxyXG4gICAgICAgIC8vIEV4dHJhIG5vZGUgaW5zZXJ0ZWQgYWZ0ZXIgcGlja2VyIGlucHV0LCBkcm9wZG93biBhbmQgZXJyb3JzXHJcbiAgICAgICAgZXh0cmFOb2RlOiBQcm9wVHlwZXMubm9kZSxcclxuICAgICAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmZ1bmNdKSxcclxuICAgICAgICByb29tSWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgYnV0dG9uOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGZvY3VzOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICB2YWxpZEFkZHJlc3NUeXBlczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9uZU9mKGFkZHJlc3NUeXBlcykpLFxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZ3JvdXBJZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICAvLyBUaGUgdHlwZSBvZiBlbnRpdHkgdG8gc2VhcmNoIGZvci4gRGVmYXVsdDogJ3VzZXInLlxyXG4gICAgICAgIHBpY2tlclR5cGU6IFByb3BUeXBlcy5vbmVPZihbJ3VzZXInLCAncm9vbSddKSxcclxuICAgICAgICAvLyBXaGV0aGVyIHRoZSBjdXJyZW50IHVzZXIgc2hvdWxkIGJlIGluY2x1ZGVkIGluIHRoZSBhZGRyZXNzZXMgcmV0dXJuZWQuIE9ubHlcclxuICAgICAgICAvLyBhcHBsaWNhYmxlIHdoZW4gcGlja2VyVHlwZSBpcyBgdXNlcmAuIERlZmF1bHQ6IGZhbHNlLlxyXG4gICAgICAgIGluY2x1ZGVTZWxmOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB2YWx1ZTogXCJcIixcclxuICAgICAgICAgICAgZm9jdXM6IHRydWUsXHJcbiAgICAgICAgICAgIHZhbGlkQWRkcmVzc1R5cGVzOiBhZGRyZXNzVHlwZXMsXHJcbiAgICAgICAgICAgIHBpY2tlclR5cGU6ICd1c2VyJyxcclxuICAgICAgICAgICAgaW5jbHVkZVNlbGY6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgbGV0IHZhbGlkQWRkcmVzc1R5cGVzID0gdGhpcy5wcm9wcy52YWxpZEFkZHJlc3NUeXBlcztcclxuICAgICAgICAvLyBSZW1vdmUgZW1haWwgZnJvbSB2YWxpZEFkZHJlc3NUeXBlcyBpZiBubyBJUyBpcyBjb25maWd1cmVkLiBJdCBtYXkgYmUgYWRkZWQgYXQgYSBsYXRlciBzdGFnZSBieSB0aGUgdXNlclxyXG4gICAgICAgIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCkgJiYgdmFsaWRBZGRyZXNzVHlwZXMuaW5jbHVkZXMoXCJlbWFpbFwiKSkge1xyXG4gICAgICAgICAgICB2YWxpZEFkZHJlc3NUeXBlcyA9IHZhbGlkQWRkcmVzc1R5cGVzLmZpbHRlcih0eXBlID0+IHR5cGUgIT09IFwiZW1haWxcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAvLyBXaGV0aGVyIHRvIHNob3cgYW4gZXJyb3IgbWVzc2FnZSBiZWNhdXNlIG9mIGFuIGludmFsaWQgYWRkcmVzc1xyXG4gICAgICAgICAgICBpbnZhbGlkQWRkcmVzc0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgLy8gTGlzdCBvZiBVc2VyQWRkcmVzc1R5cGUgb2JqZWN0cyByZXByZXNlbnRpbmdcclxuICAgICAgICAgICAgLy8gdGhlIGxpc3Qgb2YgYWRkcmVzc2VzIHdlJ3JlIGdvaW5nIHRvIGludml0ZVxyXG4gICAgICAgICAgICBzZWxlY3RlZExpc3Q6IFtdLFxyXG4gICAgICAgICAgICAvLyBXaGV0aGVyIGEgc2VhcmNoIGlzIG9uZ29pbmdcclxuICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgIC8vIEFuIGVycm9yIG1lc3NhZ2UgZ2VuZXJhdGVkIGR1cmluZyB0aGUgdXNlciBkaXJlY3Rvcnkgc2VhcmNoXHJcbiAgICAgICAgICAgIHNlYXJjaEVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICAvLyBXaGV0aGVyIHRoZSBzZXJ2ZXIgc3VwcG9ydHMgdGhlIHVzZXJfZGlyZWN0b3J5IEFQSVxyXG4gICAgICAgICAgICBzZXJ2ZXJTdXBwb3J0c1VzZXJEaXJlY3Rvcnk6IHRydWUsXHJcbiAgICAgICAgICAgIC8vIFRoZSBxdWVyeSBiZWluZyBzZWFyY2hlZCBmb3JcclxuICAgICAgICAgICAgcXVlcnk6IFwiXCIsXHJcbiAgICAgICAgICAgIC8vIExpc3Qgb2YgVXNlckFkZHJlc3NUeXBlIG9iamVjdHMgcmVwcmVzZW50aW5nIHRoZSBzZXQgb2ZcclxuICAgICAgICAgICAgLy8gYXV0by1jb21wbGV0aW9uIHJlc3VsdHMgZm9yIHRoZSBjdXJyZW50IHNlYXJjaCBxdWVyeS5cclxuICAgICAgICAgICAgc3VnZ2VzdGVkTGlzdDogW10sXHJcbiAgICAgICAgICAgIC8vIExpc3Qgb2YgYWRkcmVzcyB0eXBlcyBpbml0aWFsaXNlZCBmcm9tIHByb3BzLCBidXQgbWF5IGNoYW5nZSB3aGlsZSB0aGVcclxuICAgICAgICAgICAgLy8gZGlhbG9nIGlzIG9wZW4gYW5kIHJlcHJlc2VudHMgdGhlIHN1cHBvcnRlZCBsaXN0IG9mIGFkZHJlc3MgdHlwZXMgYXQgdGhpcyB0aW1lLlxyXG4gICAgICAgICAgICB2YWxpZEFkZHJlc3NUeXBlcyxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3RleHRpbnB1dCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZm9jdXMpIHtcclxuICAgICAgICAgICAgLy8gU2V0IHRoZSBjdXJzb3IgYXQgdGhlIGVuZCBvZiB0aGUgdGV4dCBpbnB1dFxyXG4gICAgICAgICAgICB0aGlzLl90ZXh0aW5wdXQuY3VycmVudC52YWx1ZSA9IHRoaXMucHJvcHMudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRQbGFjZWhvbGRlcigpIHtcclxuICAgICAgICBjb25zdCB7IHBsYWNlaG9sZGVyIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGlmICh0eXBlb2YgcGxhY2Vob2xkZXIgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBsYWNlaG9sZGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBPdGhlcndpc2UgaXQncyBhIGZ1bmN0aW9uLCBhcyBjaGVja2VkIGJ5IHByb3AgdHlwZXMuXHJcbiAgICAgICAgcmV0dXJuIHBsYWNlaG9sZGVyKHRoaXMuc3RhdGUudmFsaWRBZGRyZXNzVHlwZXMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkJ1dHRvbkNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgc2VsZWN0ZWRMaXN0ID0gdGhpcy5zdGF0ZS5zZWxlY3RlZExpc3Quc2xpY2UoKTtcclxuICAgICAgICAvLyBDaGVjayB0aGUgdGV4dCBpbnB1dCBmaWVsZCB0byBzZWUgaWYgdXNlciBoYXMgYW4gdW5jb252ZXJ0ZWQgYWRkcmVzc1xyXG4gICAgICAgIC8vIElmIHRoZXJlIGlzIGFuZCBpdCdzIHZhbGlkIGFkZCBpdCB0byB0aGUgbG9jYWwgc2VsZWN0ZWRMaXN0XHJcbiAgICAgICAgaWYgKHRoaXMuX3RleHRpbnB1dC5jdXJyZW50LnZhbHVlICE9PSAnJykge1xyXG4gICAgICAgICAgICBzZWxlY3RlZExpc3QgPSB0aGlzLl9hZGRBZGRyZXNzZXNUb0xpc3QoW3RoaXMuX3RleHRpbnB1dC5jdXJyZW50LnZhbHVlXSk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZExpc3QgPT09IG51bGwpIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUsIHNlbGVjdGVkTGlzdCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQ2FuY2VsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoZmFsc2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbktleURvd246IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBjb25zdCB0ZXh0SW5wdXQgPSB0aGlzLl90ZXh0aW5wdXQuY3VycmVudCA/IHRoaXMuX3RleHRpbnB1dC5jdXJyZW50LnZhbHVlIDogdW5kZWZpbmVkO1xyXG5cclxuICAgICAgICBpZiAoZS5rZXkgPT09IEtleS5FU0NBUEUpIHtcclxuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZS5rZXkgPT09IEtleS5BUlJPV19VUCkge1xyXG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFkZHJlc3NTZWxlY3RvcikgdGhpcy5hZGRyZXNzU2VsZWN0b3IubW92ZVNlbGVjdGlvblVwKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChlLmtleSA9PT0gS2V5LkFSUk9XX0RPV04pIHtcclxuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5hZGRyZXNzU2VsZWN0b3IpIHRoaXMuYWRkcmVzc1NlbGVjdG9yLm1vdmVTZWxlY3Rpb25Eb3duKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnN1Z2dlc3RlZExpc3QubGVuZ3RoID4gMCAmJiBbS2V5LkNPTU1BLCBLZXkuRU5URVIsIEtleS5UQUJdLmluY2x1ZGVzKGUua2V5KSkge1xyXG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFkZHJlc3NTZWxlY3RvcikgdGhpcy5hZGRyZXNzU2VsZWN0b3IuY2hvb3NlU2VsZWN0aW9uKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0ZXh0SW5wdXQubGVuZ3RoID09PSAwICYmIHRoaXMuc3RhdGUuc2VsZWN0ZWRMaXN0Lmxlbmd0aCAmJiBlLmtleSA9PT0gS2V5LkJBQ0tTUEFDRSkge1xyXG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMub25EaXNtaXNzZWQodGhpcy5zdGF0ZS5zZWxlY3RlZExpc3QubGVuZ3RoIC0gMSkoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGUua2V5ID09PSBLZXkuRU5URVIpIHtcclxuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBpZiAodGV4dElucHV0ID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlcmUncyBub3RoaW5nIGluIHRoZSBpbnB1dCBib3gsIHN1Ym1pdCB0aGUgZm9ybVxyXG4gICAgICAgICAgICAgICAgdGhpcy5vbkJ1dHRvbkNsaWNrKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hZGRBZGRyZXNzZXNUb0xpc3QoW3RleHRJbnB1dF0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh0ZXh0SW5wdXQgJiYgKGUua2V5ID09PSBLZXkuQ09NTUEgfHwgZS5rZXkgPT09IEtleS5UQUIpKSB7XHJcbiAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5fYWRkQWRkcmVzc2VzVG9MaXN0KFt0ZXh0SW5wdXRdKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUXVlcnlDaGFuZ2VkOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gZXYudGFyZ2V0LnZhbHVlO1xyXG4gICAgICAgIGlmICh0aGlzLnF1ZXJ5Q2hhbmdlZERlYm91bmNlcikge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5xdWVyeUNoYW5nZWREZWJvdW5jZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBPbmx5IGRvIHNlYXJjaCBpZiB0aGVyZSBpcyBzb21ldGhpbmcgdG8gc2VhcmNoXHJcbiAgICAgICAgaWYgKHF1ZXJ5Lmxlbmd0aCA+IDAgJiYgcXVlcnkgIT09ICdAJyAmJiBxdWVyeS5sZW5ndGggPj0gMikge1xyXG4gICAgICAgICAgICB0aGlzLnF1ZXJ5Q2hhbmdlZERlYm91bmNlciA9IHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMucGlja2VyVHlwZSA9PT0gJ3VzZXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuZ3JvdXBJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9kb05haXZlR3JvdXBTZWFyY2gocXVlcnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5zZXJ2ZXJTdXBwb3J0c1VzZXJEaXJlY3RvcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZG9Vc2VyRGlyZWN0b3J5U2VhcmNoKHF1ZXJ5KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9kb0xvY2FsU2VhcmNoKHF1ZXJ5KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMucGlja2VyVHlwZSA9PT0gJ3Jvb20nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuZ3JvdXBJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9kb05haXZlR3JvdXBSb29tU2VhcmNoKHF1ZXJ5KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9kb1Jvb21TZWFyY2gocXVlcnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignVW5rbm93biBwaWNrZXJUeXBlJywgdGhpcy5wcm9wcy5waWNrZXJUeXBlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgUVVFUllfVVNFUl9ESVJFQ1RPUllfREVCT1VOQ0VfTVMpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc3VnZ2VzdGVkTGlzdDogW10sXHJcbiAgICAgICAgICAgICAgICBxdWVyeTogXCJcIixcclxuICAgICAgICAgICAgICAgIHNlYXJjaEVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRGlzbWlzc2VkOiBmdW5jdGlvbihpbmRleCkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkTGlzdCA9IHRoaXMuc3RhdGUuc2VsZWN0ZWRMaXN0LnNsaWNlKCk7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkTGlzdC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkTGlzdCxcclxuICAgICAgICAgICAgICAgIHN1Z2dlc3RlZExpc3Q6IFtdLFxyXG4gICAgICAgICAgICAgICAgcXVlcnk6IFwiXCIsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fY2FuY2VsVGhyZWVwaWRMb29rdXApIHRoaXMuX2NhbmNlbFRocmVlcGlkTG9va3VwKCk7XHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DbGljazogZnVuY3Rpb24oaW5kZXgpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0ZWQoaW5kZXgpO1xyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIG9uU2VsZWN0ZWQ6IGZ1bmN0aW9uKGluZGV4KSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRMaXN0ID0gdGhpcy5zdGF0ZS5zZWxlY3RlZExpc3Quc2xpY2UoKTtcclxuICAgICAgICBzZWxlY3RlZExpc3QucHVzaCh0aGlzLl9nZXRGaWx0ZXJlZFN1Z2dlc3Rpb25zKClbaW5kZXhdKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2VsZWN0ZWRMaXN0LFxyXG4gICAgICAgICAgICBzdWdnZXN0ZWRMaXN0OiBbXSxcclxuICAgICAgICAgICAgcXVlcnk6IFwiXCIsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NhbmNlbFRocmVlcGlkTG9va3VwKSB0aGlzLl9jYW5jZWxUaHJlZXBpZExvb2t1cCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZG9OYWl2ZUdyb3VwU2VhcmNoOiBmdW5jdGlvbihxdWVyeSkge1xyXG4gICAgICAgIGNvbnN0IGxvd2VyQ2FzZVF1ZXJ5ID0gcXVlcnkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogdHJ1ZSxcclxuICAgICAgICAgICAgcXVlcnksXHJcbiAgICAgICAgICAgIHNlYXJjaEVycm9yOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRHcm91cFVzZXJzKHRoaXMucHJvcHMuZ3JvdXBJZCkudGhlbigocmVzcCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgICAgIHJlc3AuY2h1bmsuZm9yRWFjaCgodSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXNlcklkTWF0Y2ggPSB1LnVzZXJfaWQudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhsb3dlckNhc2VRdWVyeSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaXNwbGF5TmFtZU1hdGNoID0gKHUuZGlzcGxheW5hbWUgfHwgJycpLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMobG93ZXJDYXNlUXVlcnkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCEodXNlcklkTWF0Y2ggfHwgZGlzcGxheU5hbWVNYXRjaCkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJfaWQ6IHUudXNlcl9pZCxcclxuICAgICAgICAgICAgICAgICAgICBhdmF0YXJfdXJsOiB1LmF2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheV9uYW1lOiB1LmRpc3BsYXluYW1lLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLl9wcm9jZXNzUmVzdWx0cyhyZXN1bHRzLCBxdWVyeSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsc3Qgc2VhcmNoaW5nIGdyb3VwIHJvb21zOiAnLCBlcnIpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaEVycm9yOiBlcnIuZXJyY29kZSA/IGVyci5tZXNzYWdlIDogX3QoJ1NvbWV0aGluZyB3ZW50IHdyb25nIScpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9kb05haXZlR3JvdXBSb29tU2VhcmNoOiBmdW5jdGlvbihxdWVyeSkge1xyXG4gICAgICAgIGNvbnN0IGxvd2VyQ2FzZVF1ZXJ5ID0gcXVlcnkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgR3JvdXBTdG9yZS5nZXRHcm91cFJvb21zKHRoaXMucHJvcHMuZ3JvdXBJZCkuZm9yRWFjaCgocikgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBuYW1lTWF0Y2ggPSAoci5uYW1lIHx8ICcnKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGxvd2VyQ2FzZVF1ZXJ5KTtcclxuICAgICAgICAgICAgY29uc3QgdG9waWNNYXRjaCA9IChyLnRvcGljIHx8ICcnKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGxvd2VyQ2FzZVF1ZXJ5KTtcclxuICAgICAgICAgICAgY29uc3QgYWxpYXNNYXRjaCA9IChyLmNhbm9uaWNhbF9hbGlhcyB8fCAnJykudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhsb3dlckNhc2VRdWVyeSk7XHJcbiAgICAgICAgICAgIGlmICghKG5hbWVNYXRjaCB8fCB0b3BpY01hdGNoIHx8IGFsaWFzTWF0Y2gpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHJvb21faWQ6IHIucm9vbV9pZCxcclxuICAgICAgICAgICAgICAgIGF2YXRhcl91cmw6IHIuYXZhdGFyX3VybCxcclxuICAgICAgICAgICAgICAgIG5hbWU6IHIubmFtZSB8fCByLmNhbm9uaWNhbF9hbGlhcyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fcHJvY2Vzc1Jlc3VsdHMocmVzdWx0cywgcXVlcnkpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2RvUm9vbVNlYXJjaDogZnVuY3Rpb24ocXVlcnkpIHtcclxuICAgICAgICBjb25zdCBsb3dlckNhc2VRdWVyeSA9IHF1ZXJ5LnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbXMgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbXMoKTtcclxuICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgcm9vbXMuZm9yRWFjaCgocm9vbSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgcmFuayA9IEluZmluaXR5O1xyXG4gICAgICAgICAgICBjb25zdCBuYW1lRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLm5hbWUnLCAnJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IG5hbWUgPSBuYW1lRXZlbnQgPyBuYW1lRXZlbnQuZ2V0Q29udGVudCgpLm5hbWUgOiAnJztcclxuICAgICAgICAgICAgY29uc3QgY2Fub25pY2FsQWxpYXMgPSByb29tLmdldENhbm9uaWNhbEFsaWFzKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFsaWFzRXZlbnRzID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5hbGlhc2VzJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFsaWFzZXMgPSBhbGlhc0V2ZW50cy5tYXAoKGV2KSA9PiBldi5nZXRDb250ZW50KCkuYWxpYXNlcykucmVkdWNlKChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYS5jb25jYXQoYik7XHJcbiAgICAgICAgICAgIH0sIFtdKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG5hbWVNYXRjaCA9IChuYW1lIHx8ICcnKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGxvd2VyQ2FzZVF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IGFsaWFzTWF0Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgbGV0IHNob3J0ZXN0TWF0Y2hpbmdBbGlhc0xlbmd0aCA9IEluZmluaXR5O1xyXG4gICAgICAgICAgICBhbGlhc2VzLmZvckVhY2goKGFsaWFzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoKGFsaWFzIHx8ICcnKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGxvd2VyQ2FzZVF1ZXJ5KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsaWFzTWF0Y2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzaG9ydGVzdE1hdGNoaW5nQWxpYXNMZW5ndGggPiBhbGlhcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvcnRlc3RNYXRjaGluZ0FsaWFzTGVuZ3RoID0gYWxpYXMubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIShuYW1lTWF0Y2ggfHwgYWxpYXNNYXRjaCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGFsaWFzTWF0Y2gpIHtcclxuICAgICAgICAgICAgICAgIC8vIEEgc2hvcnRlciBtYXRjaGluZyBhbGlhcyB3aWxsIGdpdmUgYSBiZXR0ZXIgcmFua1xyXG4gICAgICAgICAgICAgICAgcmFuayA9IHNob3J0ZXN0TWF0Y2hpbmdBbGlhc0xlbmd0aDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgYXZhdGFyRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLmF2YXRhcicsICcnKTtcclxuICAgICAgICAgICAgY29uc3QgYXZhdGFyVXJsID0gYXZhdGFyRXZlbnQgPyBhdmF0YXJFdmVudC5nZXRDb250ZW50KCkudXJsIDogdW5kZWZpbmVkO1xyXG5cclxuICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHJhbmssXHJcbiAgICAgICAgICAgICAgICByb29tX2lkOiByb29tLnJvb21JZCxcclxuICAgICAgICAgICAgICAgIGF2YXRhcl91cmw6IGF2YXRhclVybCxcclxuICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUgfHwgY2Fub25pY2FsQWxpYXMgfHwgYWxpYXNlc1swXSB8fCBfdCgnVW5uYW1lZCBSb29tJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBTb3J0IGJ5IHJhbmsgYXNjZW5kaW5nIChhIGhpZ2ggcmFuayBiZWluZyBsZXNzIHJlbGV2YW50KVxyXG4gICAgICAgIGNvbnN0IHNvcnRlZFJlc3VsdHMgPSByZXN1bHRzLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGEucmFuayAtIGIucmFuaztcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5fcHJvY2Vzc1Jlc3VsdHMoc29ydGVkUmVzdWx0cywgcXVlcnkpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2RvVXNlckRpcmVjdG9yeVNlYXJjaDogZnVuY3Rpb24ocXVlcnkpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogdHJ1ZSxcclxuICAgICAgICAgICAgcXVlcnksXHJcbiAgICAgICAgICAgIHNlYXJjaEVycm9yOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZWFyY2hVc2VyRGlyZWN0b3J5KHtcclxuICAgICAgICAgICAgdGVybTogcXVlcnksXHJcbiAgICAgICAgfSkudGhlbigocmVzcCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBUaGUgcXVlcnkgbWlnaHQgaGF2ZSBjaGFuZ2VkIHNpbmNlIHdlIHNlbnQgdGhlIHJlcXVlc3QsIHNvIGlnbm9yZVxyXG4gICAgICAgICAgICAvLyByZXNwb25zZXMgZm9yIGFueXRoaW5nIG90aGVyIHRoYW4gdGhlIGxhdGVzdCBxdWVyeS5cclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucXVlcnkgIT09IHF1ZXJ5KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fcHJvY2Vzc1Jlc3VsdHMocmVzcC5yZXN1bHRzLCBxdWVyeSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsc3Qgc2VhcmNoaW5nIHVzZXIgZGlyZWN0b3J5OiAnLCBlcnIpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaEVycm9yOiBlcnIuZXJyY29kZSA/IGVyci5tZXNzYWdlIDogX3QoJ1NvbWV0aGluZyB3ZW50IHdyb25nIScpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlID09PSAnTV9VTlJFQ09HTklaRUQnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBzZXJ2ZXJTdXBwb3J0c1VzZXJEaXJlY3Rvcnk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAvLyBEbyBhIGxvY2FsIHNlYXJjaCBpbW1lZGlhdGVseVxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZG9Mb2NhbFNlYXJjaChxdWVyeSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9kb0xvY2FsU2VhcmNoOiBmdW5jdGlvbihxdWVyeSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBxdWVyeSxcclxuICAgICAgICAgICAgc2VhcmNoRXJyb3I6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc3QgcXVlcnlMb3dlcmNhc2UgPSBxdWVyeS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIGNvbnN0IHJlc3VsdHMgPSBbXTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcnMoKS5mb3JFYWNoKCh1c2VyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh1c2VyLnVzZXJJZC50b0xvd2VyQ2FzZSgpLmluZGV4T2YocXVlcnlMb3dlcmNhc2UpID09PSAtMSAmJlxyXG4gICAgICAgICAgICAgICAgdXNlci5kaXNwbGF5TmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YocXVlcnlMb3dlcmNhc2UpID09PSAtMVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gUHV0IHJlc3VsdHMgaW4gdGhlIGZvcm1hdCBvZiB0aGUgbmV3IEFQSVxyXG4gICAgICAgICAgICByZXN1bHRzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgdXNlcl9pZDogdXNlci51c2VySWQsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5X25hbWU6IHVzZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICBhdmF0YXJfdXJsOiB1c2VyLmF2YXRhclVybCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fcHJvY2Vzc1Jlc3VsdHMocmVzdWx0cywgcXVlcnkpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcHJvY2Vzc1Jlc3VsdHM6IGZ1bmN0aW9uKHJlc3VsdHMsIHF1ZXJ5KSB7XHJcbiAgICAgICAgY29uc3Qgc3VnZ2VzdGVkTGlzdCA9IFtdO1xyXG4gICAgICAgIHJlc3VsdHMuZm9yRWFjaCgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQucm9vbV9pZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJlc3VsdC5yb29tX2lkKTtcclxuICAgICAgICAgICAgICAgIGlmIChyb29tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9tYnN0b25lID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS50b21ic3RvbmUnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRvbWJzdG9uZSAmJiB0b21ic3RvbmUuZ2V0Q29udGVudCgpICYmIHRvbWJzdG9uZS5nZXRDb250ZW50KClbXCJyZXBsYWNlbWVudF9yb29tXCJdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlcGxhY2VtZW50Um9vbSA9IGNsaWVudC5nZXRSb29tKHRvbWJzdG9uZS5nZXRDb250ZW50KClbXCJyZXBsYWNlbWVudF9yb29tXCJdKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFNraXAgcm9vbXMgd2l0aCB0b21ic3RvbmVzIHdoZXJlIHdlIGFyZSBhbHNvIGF3YXJlIG9mIHRoZSByZXBsYWNlbWVudCByb29tLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVwbGFjZW1lbnRSb29tKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgc3VnZ2VzdGVkTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhZGRyZXNzVHlwZTogJ214LXJvb20taWQnLFxyXG4gICAgICAgICAgICAgICAgICAgIGFkZHJlc3M6IHJlc3VsdC5yb29tX2lkLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXN1bHQubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBhdmF0YXJNeGM6IHJlc3VsdC5hdmF0YXJfdXJsLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzS25vd246IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIXRoaXMucHJvcHMuaW5jbHVkZVNlbGYgJiZcclxuICAgICAgICAgICAgICAgIHJlc3VsdC51c2VyX2lkID09PSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY3JlZGVudGlhbHMudXNlcklkXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBSZXR1cm4gb2JqZWN0cywgc3RydWN0dXJlIG9mIHdoaWNoIGlzIGRlZmluZWRcclxuICAgICAgICAgICAgLy8gYnkgVXNlckFkZHJlc3NUeXBlXHJcbiAgICAgICAgICAgIHN1Z2dlc3RlZExpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzVHlwZTogJ214LXVzZXItaWQnLFxyXG4gICAgICAgICAgICAgICAgYWRkcmVzczogcmVzdWx0LnVzZXJfaWQsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmVzdWx0LmRpc3BsYXlfbmFtZSxcclxuICAgICAgICAgICAgICAgIGF2YXRhck14YzogcmVzdWx0LmF2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICBpc0tub3duOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gSWYgdGhlIHF1ZXJ5IGlzIGEgdmFsaWQgYWRkcmVzcywgYWRkIGFuIGVudHJ5IGZvciB0aGF0XHJcbiAgICAgICAgLy8gVGhpcyBpcyBpbXBvcnRhbnQsIG90aGVyd2lzZSB0aGVyZSdzIG5vIHdheSB0byBpbnZpdGVcclxuICAgICAgICAvLyBhIHBlcmZlY3RseSB2YWxpZCBhZGRyZXNzIGlmIHRoZXJlIGFyZSBjbG9zZSBtYXRjaGVzLlxyXG4gICAgICAgIGNvbnN0IGFkZHJUeXBlID0gZ2V0QWRkcmVzc1R5cGUocXVlcnkpO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnZhbGlkQWRkcmVzc1R5cGVzLmluY2x1ZGVzKGFkZHJUeXBlKSkge1xyXG4gICAgICAgICAgICBpZiAoYWRkclR5cGUgPT09ICdlbWFpbCcgJiYgIUVtYWlsLmxvb2tzVmFsaWQocXVlcnkpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzZWFyY2hFcnJvcjogX3QoXCJUaGF0IGRvZXNuJ3QgbG9vayBsaWtlIGEgdmFsaWQgZW1haWwgYWRkcmVzc1wiKX0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHN1Z2dlc3RlZExpc3QudW5zaGlmdCh7XHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzVHlwZTogYWRkclR5cGUsXHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzOiBxdWVyeSxcclxuICAgICAgICAgICAgICAgIGlzS25vd246IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2NhbmNlbFRocmVlcGlkTG9va3VwKSB0aGlzLl9jYW5jZWxUaHJlZXBpZExvb2t1cCgpO1xyXG4gICAgICAgICAgICBpZiAoYWRkclR5cGUgPT09ICdlbWFpbCcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2xvb2t1cFRocmVlcGlkKGFkZHJUeXBlLCBxdWVyeSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHN1Z2dlc3RlZExpc3QsXHJcbiAgICAgICAgICAgIGludmFsaWRBZGRyZXNzRXJyb3I6IGZhbHNlLFxyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuYWRkcmVzc1NlbGVjdG9yKSB0aGlzLmFkZHJlc3NTZWxlY3Rvci5tb3ZlU2VsZWN0aW9uVG9wKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9hZGRBZGRyZXNzZXNUb0xpc3Q6IGZ1bmN0aW9uKGFkZHJlc3NUZXh0cykge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkTGlzdCA9IHRoaXMuc3RhdGUuc2VsZWN0ZWRMaXN0LnNsaWNlKCk7XHJcblxyXG4gICAgICAgIGxldCBoYXNFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGFkZHJlc3NUZXh0cy5mb3JFYWNoKChhZGRyZXNzVGV4dCkgPT4ge1xyXG4gICAgICAgICAgICBhZGRyZXNzVGV4dCA9IGFkZHJlc3NUZXh0LnRyaW0oKTtcclxuICAgICAgICAgICAgY29uc3QgYWRkclR5cGUgPSBnZXRBZGRyZXNzVHlwZShhZGRyZXNzVGV4dCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFkZHJPYmogPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzVHlwZTogYWRkclR5cGUsXHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzOiBhZGRyZXNzVGV4dCxcclxuICAgICAgICAgICAgICAgIGlzS25vd246IGZhbHNlLFxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnZhbGlkQWRkcmVzc1R5cGVzLmluY2x1ZGVzKGFkZHJUeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgaGFzRXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGFkZHJUeXBlID09PSAnbXgtdXNlci1pZCcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHVzZXIgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcihhZGRyT2JqLmFkZHJlc3MpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBhZGRyT2JqLmRpc3BsYXlOYW1lID0gdXNlci5kaXNwbGF5TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICBhZGRyT2JqLmF2YXRhck14YyA9IHVzZXIuYXZhdGFyVXJsO1xyXG4gICAgICAgICAgICAgICAgICAgIGFkZHJPYmouaXNLbm93biA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWRkclR5cGUgPT09ICdteC1yb29tLWlkJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKGFkZHJPYmouYWRkcmVzcyk7XHJcbiAgICAgICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkZHJPYmouZGlzcGxheU5hbWUgPSByb29tLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgYWRkck9iai5hdmF0YXJNeGMgPSByb29tLmF2YXRhclVybDtcclxuICAgICAgICAgICAgICAgICAgICBhZGRyT2JqLmlzS25vd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBzZWxlY3RlZExpc3QucHVzaChhZGRyT2JqKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkTGlzdCxcclxuICAgICAgICAgICAgc3VnZ2VzdGVkTGlzdDogW10sXHJcbiAgICAgICAgICAgIHF1ZXJ5OiBcIlwiLFxyXG4gICAgICAgICAgICBpbnZhbGlkQWRkcmVzc0Vycm9yOiBoYXNFcnJvciA/IHRydWUgOiB0aGlzLnN0YXRlLmludmFsaWRBZGRyZXNzRXJyb3IsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NhbmNlbFRocmVlcGlkTG9va3VwKSB0aGlzLl9jYW5jZWxUaHJlZXBpZExvb2t1cCgpO1xyXG4gICAgICAgIHJldHVybiBoYXNFcnJvciA/IG51bGwgOiBzZWxlY3RlZExpc3Q7XHJcbiAgICB9LFxyXG5cclxuICAgIF9sb29rdXBUaHJlZXBpZDogYXN5bmMgZnVuY3Rpb24obWVkaXVtLCBhZGRyZXNzKSB7XHJcbiAgICAgICAgbGV0IGNhbmNlbGxlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIE5vdGUgdGhhdCB3ZSBjYW4ndCBzYWZlbHkgcmVtb3ZlIHRoaXMgYWZ0ZXIgd2UncmUgZG9uZVxyXG4gICAgICAgIC8vIGJlY2F1c2Ugd2UgZG9uJ3Qga25vdyB0aGF0IGl0J3MgdGhlIHNhbWUgb25lLCBzbyB3ZSBqdXN0XHJcbiAgICAgICAgLy8gbGVhdmUgaXQ6IGl0J3MgcmVwbGFjaW5nIHRoZSBvbGQgb25lIGVhY2ggdGltZSBzbyBpdCdzXHJcbiAgICAgICAgLy8gbm90IGxpa2UgdGhleSBsZWFrLlxyXG4gICAgICAgIHRoaXMuX2NhbmNlbFRocmVlcGlkTG9va3VwID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNhbmNlbGxlZCA9IHRydWU7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gd2FpdCBhIGJpdCB0byBsZXQgdGhlIHVzZXIgZmluaXNoIHR5cGluZ1xyXG4gICAgICAgIGF3YWl0IHNsZWVwKDUwMCk7XHJcbiAgICAgICAgaWYgKGNhbmNlbGxlZCkgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGF1dGhDbGllbnQgPSBuZXcgSWRlbnRpdHlBdXRoQ2xpZW50KCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGlkZW50aXR5QWNjZXNzVG9rZW4gPSBhd2FpdCBhdXRoQ2xpZW50LmdldEFjY2Vzc1Rva2VuKCk7XHJcbiAgICAgICAgICAgIGlmIChjYW5jZWxsZWQpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbG9va3VwID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmxvb2t1cFRocmVlUGlkKFxyXG4gICAgICAgICAgICAgICAgbWVkaXVtLFxyXG4gICAgICAgICAgICAgICAgYWRkcmVzcyxcclxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZCAvKiBjYWxsYmFjayAqLyxcclxuICAgICAgICAgICAgICAgIGlkZW50aXR5QWNjZXNzVG9rZW4sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmIChjYW5jZWxsZWQgfHwgbG9va3VwID09PSBudWxsIHx8ICFsb29rdXAubXhpZCkgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwcm9maWxlID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFByb2ZpbGVJbmZvKGxvb2t1cC5teGlkKTtcclxuICAgICAgICAgICAgaWYgKGNhbmNlbGxlZCB8fCBwcm9maWxlID09PSBudWxsKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgc3VnZ2VzdGVkTGlzdDogW3tcclxuICAgICAgICAgICAgICAgICAgICAvLyBhIFVzZXJBZGRyZXNzVHlwZVxyXG4gICAgICAgICAgICAgICAgICAgIGFkZHJlc3NUeXBlOiBtZWRpdW0sXHJcbiAgICAgICAgICAgICAgICAgICAgYWRkcmVzczogYWRkcmVzcyxcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcHJvZmlsZS5kaXNwbGF5bmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBhdmF0YXJNeGM6IHByb2ZpbGUuYXZhdGFyX3VybCxcclxuICAgICAgICAgICAgICAgICAgICBpc0tub3duOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfV0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hFcnJvcjogX3QoJ1NvbWV0aGluZyB3ZW50IHdyb25nIScpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRGaWx0ZXJlZFN1Z2dlc3Rpb25zOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBtYXAgYWRkcmVzc1R5cGUgPT4gc2V0IG9mIGFkZHJlc3NlcyB0byBhdm9pZCBPKG4qbSkgb3BlcmF0aW9uXHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRBZGRyZXNzZXMgPSB7fTtcclxuICAgICAgICB0aGlzLnN0YXRlLnNlbGVjdGVkTGlzdC5mb3JFYWNoKCh7YWRkcmVzcywgYWRkcmVzc1R5cGV9KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghc2VsZWN0ZWRBZGRyZXNzZXNbYWRkcmVzc1R5cGVdKSBzZWxlY3RlZEFkZHJlc3Nlc1thZGRyZXNzVHlwZV0gPSBuZXcgU2V0KCk7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkQWRkcmVzc2VzW2FkZHJlc3NUeXBlXS5hZGQoYWRkcmVzcyk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIEZpbHRlciBvdXQgYW55IGFkZHJlc3NlcyBpbiB0aGUgYWJvdmUgYWxyZWFkeSBzZWxlY3RlZCBhZGRyZXNzZXMgKG1hdGNoaW5nIGJvdGggdHlwZSBhbmQgYWRkcmVzcylcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5zdWdnZXN0ZWRMaXN0LmZpbHRlcigoe2FkZHJlc3MsIGFkZHJlc3NUeXBlfSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gIShzZWxlY3RlZEFkZHJlc3Nlc1thZGRyZXNzVHlwZV0gJiYgc2VsZWN0ZWRBZGRyZXNzZXNbYWRkcmVzc1R5cGVdLmhhcyhhZGRyZXNzKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblBhc3RlOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgLy8gUHJldmVudCB0aGUgdGV4dCBiZWluZyBwYXN0ZWQgaW50byB0aGUgdGV4dGFyZWFcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgdGV4dCA9IGUuY2xpcGJvYXJkRGF0YS5nZXREYXRhKFwidGV4dFwiKTtcclxuICAgICAgICAvLyBQcm9jZXNzIGl0IGFzIGEgbGlzdCBvZiBhZGRyZXNzZXMgdG8gYWRkIGluc3RlYWRcclxuICAgICAgICB0aGlzLl9hZGRBZGRyZXNzZXNUb0xpc3QodGV4dC5zcGxpdCgvW1xccyxdKy8pKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Vc2VEZWZhdWx0SWRlbnRpdHlTZXJ2ZXJDbGljayhlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIElTIGluIGFjY291bnQgZGF0YS4gQWN0dWFsbHkgdXNpbmcgaXQgbWF5IHRyaWdnZXIgdGVybXMuXHJcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0LWhvb2tzL3J1bGVzLW9mLWhvb2tzXHJcbiAgICAgICAgdXNlRGVmYXVsdElkZW50aXR5U2VydmVyKCk7XHJcblxyXG4gICAgICAgIC8vIEFkZCBlbWFpbCBhcyBhIHZhbGlkIGFkZHJlc3MgdHlwZS5cclxuICAgICAgICBjb25zdCB7IHZhbGlkQWRkcmVzc1R5cGVzIH0gPSB0aGlzLnN0YXRlO1xyXG4gICAgICAgIHZhbGlkQWRkcmVzc1R5cGVzLnB1c2goJ2VtYWlsJyk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHZhbGlkQWRkcmVzc1R5cGVzIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbk1hbmFnZVNldHRpbmdzQ2xpY2soZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICd2aWV3X3VzZXJfc2V0dGluZ3MnIH0pO1xyXG4gICAgICAgIHRoaXMub25DYW5jZWwoKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZGlhbG9ncy5CYXNlRGlhbG9nJyk7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICBjb25zdCBBZGRyZXNzU2VsZWN0b3IgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuQWRkcmVzc1NlbGVjdG9yXCIpO1xyXG4gICAgICAgIHRoaXMuc2Nyb2xsRWxlbWVudCA9IG51bGw7XHJcblxyXG4gICAgICAgIGxldCBpbnB1dExhYmVsO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmRlc2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgIGlucHV0TGFiZWwgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NQaWNrZXJEaWFsb2dfbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwidGV4dGlucHV0XCI+e3RoaXMucHJvcHMuZGVzY3JpcHRpb259PC9sYWJlbD5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcXVlcnkgPSBbXTtcclxuICAgICAgICAvLyBjcmVhdGUgdGhlIGludml0ZSBsaXN0XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2VsZWN0ZWRMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgQWRkcmVzc1RpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuQWRkcmVzc1RpbGVcIik7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdGF0ZS5zZWxlY3RlZExpc3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHF1ZXJ5LnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgPEFkZHJlc3NUaWxlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17aX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWRkcmVzcz17dGhpcy5zdGF0ZS5zZWxlY3RlZExpc3RbaV19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbkRpc21pc3M9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRGlzbWlzc2VkPXt0aGlzLm9uRGlzbWlzc2VkKGkpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaG93QWRkcmVzcz17dGhpcy5wcm9wcy5waWNrZXJUeXBlID09PSAndXNlcid9IC8+LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQWRkIHRoZSBxdWVyeSBhdCB0aGUgZW5kXHJcbiAgICAgICAgcXVlcnkucHVzaChcclxuICAgICAgICAgICAgPHRleHRhcmVhXHJcbiAgICAgICAgICAgICAgICBrZXk9e3RoaXMuc3RhdGUuc2VsZWN0ZWRMaXN0Lmxlbmd0aH1cclxuICAgICAgICAgICAgICAgIG9uUGFzdGU9e3RoaXMuX29uUGFzdGV9XHJcbiAgICAgICAgICAgICAgICByb3dzPVwiMVwiXHJcbiAgICAgICAgICAgICAgICBpZD1cInRleHRpbnB1dFwiXHJcbiAgICAgICAgICAgICAgICByZWY9e3RoaXMuX3RleHRpbnB1dH1cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0FkZHJlc3NQaWNrZXJEaWFsb2dfaW5wdXRcIlxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25RdWVyeUNoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17dGhpcy5nZXRQbGFjZWhvbGRlcigpfVxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlPXt0aGlzLnByb3BzLnZhbHVlfVxyXG4gICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0aGlzLnByb3BzLmZvY3VzfT5cclxuICAgICAgICAgICAgPC90ZXh0YXJlYT4sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgY29uc3QgZmlsdGVyZWRTdWdnZXN0ZWRMaXN0ID0gdGhpcy5fZ2V0RmlsdGVyZWRTdWdnZXN0aW9ucygpO1xyXG5cclxuICAgICAgICBsZXQgZXJyb3I7XHJcbiAgICAgICAgbGV0IGFkZHJlc3NTZWxlY3RvcjtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pbnZhbGlkQWRkcmVzc0Vycm9yKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkVHlwZURlc2NyaXB0aW9ucyA9IHRoaXMuc3RhdGUudmFsaWRBZGRyZXNzVHlwZXMubWFwKCh0KSA9PiBfdChhZGRyZXNzVHlwZU5hbWVbdF0pKTtcclxuICAgICAgICAgICAgZXJyb3IgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NQaWNrZXJEaWFsb2dfZXJyb3JcIj5cclxuICAgICAgICAgICAgICAgIHsgX3QoXCJZb3UgaGF2ZSBlbnRlcmVkIGFuIGludmFsaWQgYWRkcmVzcy5cIikgfVxyXG4gICAgICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgICAgICB7IF90KFwiVHJ5IHVzaW5nIG9uZSBvZiB0aGUgZm9sbG93aW5nIHZhbGlkIGFkZHJlc3MgdHlwZXM6ICUodmFsaWRUeXBlc0xpc3Qpcy5cIiwge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkVHlwZXNMaXN0OiB2YWxpZFR5cGVEZXNjcmlwdGlvbnMuam9pbihcIiwgXCIpLFxyXG4gICAgICAgICAgICAgICAgfSkgfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnNlYXJjaEVycm9yKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gPGRpdiBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nX2Vycm9yXCI+eyB0aGlzLnN0YXRlLnNlYXJjaEVycm9yIH08L2Rpdj47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnF1ZXJ5Lmxlbmd0aCA+IDAgJiYgZmlsdGVyZWRTdWdnZXN0ZWRMaXN0Lmxlbmd0aCA9PT0gMCAmJiAhdGhpcy5zdGF0ZS5idXN5KSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gPGRpdiBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nX2Vycm9yXCI+eyBfdChcIk5vIHJlc3VsdHNcIikgfTwvZGl2PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBhZGRyZXNzU2VsZWN0b3IgPSAoXHJcbiAgICAgICAgICAgICAgICA8QWRkcmVzc1NlbGVjdG9yIHJlZj17KHJlZikgPT4ge3RoaXMuYWRkcmVzc1NlbGVjdG9yID0gcmVmO319XHJcbiAgICAgICAgICAgICAgICAgICAgYWRkcmVzc0xpc3Q9e2ZpbHRlcmVkU3VnZ2VzdGVkTGlzdH1cclxuICAgICAgICAgICAgICAgICAgICBzaG93QWRkcmVzcz17dGhpcy5wcm9wcy5waWNrZXJUeXBlID09PSAndXNlcid9XHJcbiAgICAgICAgICAgICAgICAgICAgb25TZWxlY3RlZD17dGhpcy5vblNlbGVjdGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHRydW5jYXRlQXQ9e1RSVU5DQVRFX1FVRVJZX0xJU1R9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGlkZW50aXR5U2VydmVyO1xyXG4gICAgICAgIC8vIElmIHBpY2tlciBjYW5ub3QgY3VycmVudGx5IGFjY2VwdCBlLW1haWwgYnV0IHNob3VsZCBiZSBhYmxlIHRvXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucGlja2VyVHlwZSA9PT0gJ3VzZXInICYmICF0aGlzLnN0YXRlLnZhbGlkQWRkcmVzc1R5cGVzLmluY2x1ZGVzKCdlbWFpbCcpXHJcbiAgICAgICAgICAgICYmIHRoaXMucHJvcHMudmFsaWRBZGRyZXNzVHlwZXMuaW5jbHVkZXMoJ2VtYWlsJykpIHtcclxuICAgICAgICAgICAgY29uc3QgZGVmYXVsdElkZW50aXR5U2VydmVyVXJsID0gZ2V0RGVmYXVsdElkZW50aXR5U2VydmVyVXJsKCk7XHJcbiAgICAgICAgICAgIGlmIChkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwpIHtcclxuICAgICAgICAgICAgICAgIGlkZW50aXR5U2VydmVyID0gPGRpdiBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nX2lkZW50aXR5U2VydmVyXCI+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVXNlIGFuIGlkZW50aXR5IHNlcnZlciB0byBpbnZpdGUgYnkgZW1haWwuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkZWZhdWx0PlVzZSB0aGUgZGVmYXVsdCAoJShkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lKXMpPC9kZWZhdWx0PiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvciBtYW5hZ2UgaW4gPHNldHRpbmdzPlNldHRpbmdzPC9zZXR0aW5ncz4uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lOiBhYmJyZXZpYXRlVXJsKGRlZmF1bHRJZGVudGl0eVNlcnZlclVybCksXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IHN1YiA9PiA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9e3RoaXMub25Vc2VEZWZhdWx0SWRlbnRpdHlTZXJ2ZXJDbGlja30+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczogc3ViID0+IDxhIGhyZWY9XCIjXCIgb25DbGljaz17dGhpcy5vbk1hbmFnZVNldHRpbmdzQ2xpY2t9PntzdWJ9PC9hPixcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKX08L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZGVudGl0eVNlcnZlciA9IDxkaXYgY2xhc3NOYW1lPVwibXhfQWRkcmVzc1BpY2tlckRpYWxvZ19pZGVudGl0eVNlcnZlclwiPntfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlVzZSBhbiBpZGVudGl0eSBzZXJ2ZXIgdG8gaW52aXRlIGJ5IGVtYWlsLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJNYW5hZ2UgaW4gPHNldHRpbmdzPlNldHRpbmdzPC9zZXR0aW5ncz4uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAge30sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHN1YiA9PiA8YSBocmVmPVwiI1wiIG9uQ2xpY2s9e3RoaXMub25NYW5hZ2VTZXR0aW5nc0NsaWNrfT57c3VifTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICl9PC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZURpYWxvZyBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nXCIgb25LZXlEb3duPXt0aGlzLm9uS2V5RG93bn1cclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMucHJvcHMub25GaW5pc2hlZH0gdGl0bGU9e3RoaXMucHJvcHMudGl0bGV9PlxyXG4gICAgICAgICAgICAgICAge2lucHV0TGFiZWx9XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9BZGRyZXNzUGlja2VyRGlhbG9nX2lucHV0Q29udGFpbmVyXCI+eyBxdWVyeSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBlcnJvciB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBhZGRyZXNzU2VsZWN0b3IgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5leHRyYU5vZGUgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgaWRlbnRpdHlTZXJ2ZXIgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXt0aGlzLnByb3BzLmJ1dHRvbn1cclxuICAgICAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5vbkJ1dHRvbkNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLm9uQ2FuY2VsfSAvPlxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=