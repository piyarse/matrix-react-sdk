"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _languageHandler = require("../../../../languageHandler");

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Access Secure Secret Storage by requesting the user's passphrase.
 */
class AccessSecretStorageDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onCancel", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onUseRecoveryKeyClick", () => {
      this.setState({
        forceRecoveryKey: true
      });
    });
    (0, _defineProperty2.default)(this, "_onResetRecoveryClick", () => {
      this.props.onFinished(false);
      throw new Error("Resetting secret storage unimplemented");
    });
    (0, _defineProperty2.default)(this, "_onRecoveryKeyChange", e => {
      this.setState({
        recoveryKey: e.target.value,
        recoveryKeyValid: _MatrixClientPeg.MatrixClientPeg.get().isValidRecoveryKey(e.target.value),
        keyMatches: null
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseNext", async e => {
      e.preventDefault();
      if (this.state.passPhrase.length <= 0) return;
      this.setState({
        keyMatches: null
      });
      const input = {
        passphrase: this.state.passPhrase
      };
      const keyMatches = await this.props.checkPrivateKey(input);

      if (keyMatches) {
        this.props.onFinished(input);
      } else {
        this.setState({
          keyMatches
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onRecoveryKeyNext", async e => {
      e.preventDefault();
      if (!this.state.recoveryKeyValid) return;
      this.setState({
        keyMatches: null
      });
      const input = {
        recoveryKey: this.state.recoveryKey
      };
      const keyMatches = await this.props.checkPrivateKey(input);

      if (keyMatches) {
        this.props.onFinished(input);
      } else {
        this.setState({
          keyMatches
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseChange", e => {
      this.setState({
        passPhrase: e.target.value,
        keyMatches: null
      });
    });
    this.state = {
      recoveryKey: "",
      recoveryKeyValid: false,
      forceRecoveryKey: false,
      passPhrase: '',
      keyMatches: null
    };
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const hasPassphrase = this.props.keyInfo && this.props.keyInfo.passphrase && this.props.keyInfo.passphrase.salt && this.props.keyInfo.passphrase.iterations;
    let content;
    let title;

    if (hasPassphrase && !this.state.forceRecoveryKey) {
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
      title = (0, _languageHandler._t)("Enter secret storage passphrase");
      let keyStatus;

      if (this.state.keyMatches === false) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        }, "\uD83D\uDC4E ", (0, _languageHandler._t)("Unable to access secret storage. Please verify that you " + "entered the correct passphrase."));
      } else {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        });
      }

      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("<b>Warning</b>: You should only access secret storage " + "from a trusted computer.", {}, {
        b: sub => _react.default.createElement("b", null, sub)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Access your secure message history and your cross-signing " + "identity for verifying other sessions by entering your passphrase.")), _react.default.createElement("form", {
        className: "mx_AccessSecretStorageDialog_primaryContainer",
        onSubmit: this._onPassPhraseNext
      }, _react.default.createElement("input", {
        type: "password",
        className: "mx_AccessSecretStorageDialog_passPhraseInput",
        onChange: this._onPassPhraseChange,
        value: this.state.passPhrase,
        autoFocus: true,
        autoComplete: "new-password"
      }), keyStatus, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Next'),
        onPrimaryButtonClick: this._onPassPhraseNext,
        hasCancel: true,
        onCancel: this._onCancel,
        focus: false,
        primaryDisabled: this.state.passPhrase.length === 0
      })), (0, _languageHandler._t)("If you've forgotten your passphrase you can " + "<button1>use your recovery key</button1> or " + "<button2>set up new recovery options</button2>.", {}, {
        button1: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onUseRecoveryKeyClick
        }, s),
        button2: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onResetRecoveryClick
        }, s)
      }));
    } else {
      title = (0, _languageHandler._t)("Enter secret storage recovery key");
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
      let keyStatus;

      if (this.state.recoveryKey.length === 0) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        });
      } else if (this.state.keyMatches === false) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        }, "\uD83D\uDC4E ", (0, _languageHandler._t)("Unable to access secret storage. Please verify that you " + "entered the correct recovery key."));
      } else if (this.state.recoveryKeyValid) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        }, "\uD83D\uDC4D ", (0, _languageHandler._t)("This looks like a valid recovery key!"));
      } else {
        keyStatus = _react.default.createElement("div", {
          className: "mx_AccessSecretStorageDialog_keyStatus"
        }, "\uD83D\uDC4E ", (0, _languageHandler._t)("Not a valid recovery key"));
      }

      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("<b>Warning</b>: You should only access secret storage " + "from a trusted computer.", {}, {
        b: sub => _react.default.createElement("b", null, sub)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Access your secure message history and your cross-signing " + "identity for verifying other sessions by entering your recovery key.")), _react.default.createElement("form", {
        className: "mx_AccessSecretStorageDialog_primaryContainer",
        onSubmit: this._onRecoveryKeyNext
      }, _react.default.createElement("input", {
        className: "mx_AccessSecretStorageDialog_recoveryKeyInput",
        onChange: this._onRecoveryKeyChange,
        value: this.state.recoveryKey,
        autoFocus: true
      }), keyStatus, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Next'),
        onPrimaryButtonClick: this._onRecoveryKeyNext,
        hasCancel: true,
        onCancel: this._onCancel,
        focus: false,
        primaryDisabled: !this.state.recoveryKeyValid
      })), (0, _languageHandler._t)("If you've forgotten your recovery key you can " + "<button>set up new recovery options</button>.", {}, {
        button: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onResetRecoveryClick
        }, s)
      }));
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_AccessSecretStorageDialog",
      onFinished: this.props.onFinished,
      title: title
    }, _react.default.createElement("div", null, content));
  }

}

exports.default = AccessSecretStorageDialog;
(0, _defineProperty2.default)(AccessSecretStorageDialog, "propTypes", {
  // { passphrase, pubkey }
  keyInfo: _propTypes.default.object.isRequired,
  // Function from one of { passphrase, recoveryKey } -> boolean
  checkPrivateKey: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mvc2VjcmV0c3RvcmFnZS9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nLmpzIl0sIm5hbWVzIjpbIkFjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2ciLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwib25GaW5pc2hlZCIsInNldFN0YXRlIiwiZm9yY2VSZWNvdmVyeUtleSIsIkVycm9yIiwiZSIsInJlY292ZXJ5S2V5IiwidGFyZ2V0IiwidmFsdWUiLCJyZWNvdmVyeUtleVZhbGlkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiaXNWYWxpZFJlY292ZXJ5S2V5Iiwia2V5TWF0Y2hlcyIsInByZXZlbnREZWZhdWx0Iiwic3RhdGUiLCJwYXNzUGhyYXNlIiwibGVuZ3RoIiwiaW5wdXQiLCJwYXNzcGhyYXNlIiwiY2hlY2tQcml2YXRlS2V5IiwicmVuZGVyIiwiQmFzZURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsImhhc1Bhc3NwaHJhc2UiLCJrZXlJbmZvIiwic2FsdCIsIml0ZXJhdGlvbnMiLCJjb250ZW50IiwidGl0bGUiLCJEaWFsb2dCdXR0b25zIiwiQWNjZXNzaWJsZUJ1dHRvbiIsImtleVN0YXR1cyIsImIiLCJzdWIiLCJfb25QYXNzUGhyYXNlTmV4dCIsIl9vblBhc3NQaHJhc2VDaGFuZ2UiLCJfb25DYW5jZWwiLCJidXR0b24xIiwicyIsIl9vblVzZVJlY292ZXJ5S2V5Q2xpY2siLCJidXR0b24yIiwiX29uUmVzZXRSZWNvdmVyeUNsaWNrIiwiX29uUmVjb3ZlcnlLZXlOZXh0IiwiX29uUmVjb3ZlcnlLZXlDaGFuZ2UiLCJidXR0b24iLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUF0QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBOzs7QUFHZSxNQUFNQSx5QkFBTixTQUF3Q0MsZUFBTUMsYUFBOUMsQ0FBNEQ7QUFRdkVDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLHFEQVdQLE1BQU07QUFDZCxXQUFLQSxLQUFMLENBQVdDLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQWJrQjtBQUFBLGtFQWVNLE1BQU07QUFDM0IsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLGdCQUFnQixFQUFFO0FBRFIsT0FBZDtBQUdILEtBbkJrQjtBQUFBLGlFQXFCSyxNQUFNO0FBQzFCLFdBQUtILEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixLQUF0QjtBQUNBLFlBQU0sSUFBSUcsS0FBSixDQUFVLHdDQUFWLENBQU47QUFDSCxLQXhCa0I7QUFBQSxnRUEwQktDLENBQUQsSUFBTztBQUMxQixXQUFLSCxRQUFMLENBQWM7QUFDVkksUUFBQUEsV0FBVyxFQUFFRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FEWjtBQUVWQyxRQUFBQSxnQkFBZ0IsRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsa0JBQXRCLENBQXlDUCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBbEQsQ0FGUjtBQUdWSyxRQUFBQSxVQUFVLEVBQUU7QUFIRixPQUFkO0FBS0gsS0FoQ2tCO0FBQUEsNkRBa0NDLE1BQU9SLENBQVAsSUFBYTtBQUM3QkEsTUFBQUEsQ0FBQyxDQUFDUyxjQUFGO0FBRUEsVUFBSSxLQUFLQyxLQUFMLENBQVdDLFVBQVgsQ0FBc0JDLE1BQXRCLElBQWdDLENBQXBDLEVBQXVDO0FBRXZDLFdBQUtmLFFBQUwsQ0FBYztBQUFFVyxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQUFkO0FBQ0EsWUFBTUssS0FBSyxHQUFHO0FBQUVDLFFBQUFBLFVBQVUsRUFBRSxLQUFLSixLQUFMLENBQVdDO0FBQXpCLE9BQWQ7QUFDQSxZQUFNSCxVQUFVLEdBQUcsTUFBTSxLQUFLYixLQUFMLENBQVdvQixlQUFYLENBQTJCRixLQUEzQixDQUF6Qjs7QUFDQSxVQUFJTCxVQUFKLEVBQWdCO0FBQ1osYUFBS2IsS0FBTCxDQUFXQyxVQUFYLENBQXNCaUIsS0FBdEI7QUFDSCxPQUZELE1BRU87QUFDSCxhQUFLaEIsUUFBTCxDQUFjO0FBQUVXLFVBQUFBO0FBQUYsU0FBZDtBQUNIO0FBQ0osS0EvQ2tCO0FBQUEsOERBaURFLE1BQU9SLENBQVAsSUFBYTtBQUM5QkEsTUFBQUEsQ0FBQyxDQUFDUyxjQUFGO0FBRUEsVUFBSSxDQUFDLEtBQUtDLEtBQUwsQ0FBV04sZ0JBQWhCLEVBQWtDO0FBRWxDLFdBQUtQLFFBQUwsQ0FBYztBQUFFVyxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQUFkO0FBQ0EsWUFBTUssS0FBSyxHQUFHO0FBQUVaLFFBQUFBLFdBQVcsRUFBRSxLQUFLUyxLQUFMLENBQVdUO0FBQTFCLE9BQWQ7QUFDQSxZQUFNTyxVQUFVLEdBQUcsTUFBTSxLQUFLYixLQUFMLENBQVdvQixlQUFYLENBQTJCRixLQUEzQixDQUF6Qjs7QUFDQSxVQUFJTCxVQUFKLEVBQWdCO0FBQ1osYUFBS2IsS0FBTCxDQUFXQyxVQUFYLENBQXNCaUIsS0FBdEI7QUFDSCxPQUZELE1BRU87QUFDSCxhQUFLaEIsUUFBTCxDQUFjO0FBQUVXLFVBQUFBO0FBQUYsU0FBZDtBQUNIO0FBQ0osS0E5RGtCO0FBQUEsK0RBZ0VJUixDQUFELElBQU87QUFDekIsV0FBS0gsUUFBTCxDQUFjO0FBQ1ZjLFFBQUFBLFVBQVUsRUFBRVgsQ0FBQyxDQUFDRSxNQUFGLENBQVNDLEtBRFg7QUFFVkssUUFBQUEsVUFBVSxFQUFFO0FBRkYsT0FBZDtBQUlILEtBckVrQjtBQUVmLFNBQUtFLEtBQUwsR0FBYTtBQUNUVCxNQUFBQSxXQUFXLEVBQUUsRUFESjtBQUVURyxNQUFBQSxnQkFBZ0IsRUFBRSxLQUZUO0FBR1ROLE1BQUFBLGdCQUFnQixFQUFFLEtBSFQ7QUFJVGEsTUFBQUEsVUFBVSxFQUFFLEVBSkg7QUFLVEgsTUFBQUEsVUFBVSxFQUFFO0FBTEgsS0FBYjtBQU9IOztBQThERFEsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBRUEsVUFBTUMsYUFBYSxHQUNmLEtBQUt6QixLQUFMLENBQVcwQixPQUFYLElBQ0EsS0FBSzFCLEtBQUwsQ0FBVzBCLE9BQVgsQ0FBbUJQLFVBRG5CLElBRUEsS0FBS25CLEtBQUwsQ0FBVzBCLE9BQVgsQ0FBbUJQLFVBQW5CLENBQThCUSxJQUY5QixJQUdBLEtBQUszQixLQUFMLENBQVcwQixPQUFYLENBQW1CUCxVQUFuQixDQUE4QlMsVUFKbEM7QUFPQSxRQUFJQyxPQUFKO0FBQ0EsUUFBSUMsS0FBSjs7QUFDQSxRQUFJTCxhQUFhLElBQUksQ0FBQyxLQUFLVixLQUFMLENBQVdaLGdCQUFqQyxFQUFtRDtBQUMvQyxZQUFNNEIsYUFBYSxHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsWUFBTVEsZ0JBQWdCLEdBQUdULEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQU0sTUFBQUEsS0FBSyxHQUFHLHlCQUFHLGlDQUFILENBQVI7QUFFQSxVQUFJRyxTQUFKOztBQUNBLFVBQUksS0FBS2xCLEtBQUwsQ0FBV0YsVUFBWCxLQUEwQixLQUE5QixFQUFxQztBQUNqQ29CLFFBQUFBLFNBQVMsR0FBRztBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDUCxlQURPLEVBQ1UseUJBQ2QsNkRBQ0EsaUNBRmMsQ0FEVixDQUFaO0FBTUgsT0FQRCxNQU9PO0FBQ0hBLFFBQUFBLFNBQVMsR0FBRztBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsVUFBWjtBQUNIOztBQUVESixNQUFBQSxPQUFPLEdBQUcsMENBQ04sd0NBQUkseUJBQ0EsMkRBQ0EsMEJBRkEsRUFFNEIsRUFGNUIsRUFHQTtBQUFFSyxRQUFBQSxDQUFDLEVBQUVDLEdBQUcsSUFBSSx3Q0FBSUEsR0FBSjtBQUFaLE9BSEEsQ0FBSixDQURNLEVBTU4sd0NBQUkseUJBQ0EsK0RBQ0Esb0VBRkEsQ0FBSixDQU5NLEVBV047QUFBTSxRQUFBLFNBQVMsRUFBQywrQ0FBaEI7QUFBZ0UsUUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFBL0UsU0FDSTtBQUNJLFFBQUEsSUFBSSxFQUFDLFVBRFQ7QUFFSSxRQUFBLFNBQVMsRUFBQyw4Q0FGZDtBQUdJLFFBQUEsUUFBUSxFQUFFLEtBQUtDLG1CQUhuQjtBQUlJLFFBQUEsS0FBSyxFQUFFLEtBQUt0QixLQUFMLENBQVdDLFVBSnRCO0FBS0ksUUFBQSxTQUFTLEVBQUUsSUFMZjtBQU1JLFFBQUEsWUFBWSxFQUFDO0FBTmpCLFFBREosRUFTS2lCLFNBVEwsRUFVSSw2QkFBQyxhQUFEO0FBQ0ksUUFBQSxhQUFhLEVBQUUseUJBQUcsTUFBSCxDQURuQjtBQUVJLFFBQUEsb0JBQW9CLEVBQUUsS0FBS0csaUJBRi9CO0FBR0ksUUFBQSxTQUFTLEVBQUUsSUFIZjtBQUlJLFFBQUEsUUFBUSxFQUFFLEtBQUtFLFNBSm5CO0FBS0ksUUFBQSxLQUFLLEVBQUUsS0FMWDtBQU1JLFFBQUEsZUFBZSxFQUFFLEtBQUt2QixLQUFMLENBQVdDLFVBQVgsQ0FBc0JDLE1BQXRCLEtBQWlDO0FBTnRELFFBVkosQ0FYTSxFQThCTCx5QkFDRyxpREFDQSw4Q0FEQSxHQUVBLGlEQUhILEVBSUMsRUFKRCxFQUlLO0FBQ0ZzQixRQUFBQSxPQUFPLEVBQUVDLENBQUMsSUFBSSw2QkFBQyxnQkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyxlQUE1QjtBQUNWLFVBQUEsT0FBTyxFQUFDLE1BREU7QUFFVixVQUFBLE9BQU8sRUFBRSxLQUFLQztBQUZKLFdBSVRELENBSlMsQ0FEWjtBQU9GRSxRQUFBQSxPQUFPLEVBQUVGLENBQUMsSUFBSSw2QkFBQyxnQkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyxlQUE1QjtBQUNWLFVBQUEsT0FBTyxFQUFDLE1BREU7QUFFVixVQUFBLE9BQU8sRUFBRSxLQUFLRztBQUZKLFdBSVRILENBSlM7QUFQWixPQUpMLENBOUJLLENBQVY7QUFpREgsS0FsRUQsTUFrRU87QUFDSFYsTUFBQUEsS0FBSyxHQUFHLHlCQUFHLG1DQUFILENBQVI7QUFDQSxZQUFNQyxhQUFhLEdBQUdSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFDQSxZQUFNUSxnQkFBZ0IsR0FBR1QsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFVBQUlTLFNBQUo7O0FBQ0EsVUFBSSxLQUFLbEIsS0FBTCxDQUFXVCxXQUFYLENBQXVCVyxNQUF2QixLQUFrQyxDQUF0QyxFQUF5QztBQUNyQ2dCLFFBQUFBLFNBQVMsR0FBRztBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsVUFBWjtBQUNILE9BRkQsTUFFTyxJQUFJLEtBQUtsQixLQUFMLENBQVdGLFVBQVgsS0FBMEIsS0FBOUIsRUFBcUM7QUFDeENvQixRQUFBQSxTQUFTLEdBQUc7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ1AsZUFETyxFQUNVLHlCQUNkLDZEQUNBLG1DQUZjLENBRFYsQ0FBWjtBQU1ILE9BUE0sTUFPQSxJQUFJLEtBQUtsQixLQUFMLENBQVdOLGdCQUFmLEVBQWlDO0FBQ3BDd0IsUUFBQUEsU0FBUyxHQUFHO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNQLGVBRE8sRUFDVSx5QkFBRyx1Q0FBSCxDQURWLENBQVo7QUFHSCxPQUpNLE1BSUE7QUFDSEEsUUFBQUEsU0FBUyxHQUFHO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNQLGVBRE8sRUFDVSx5QkFBRywwQkFBSCxDQURWLENBQVo7QUFHSDs7QUFFREosTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLDJEQUNBLDBCQUZBLEVBRTRCLEVBRjVCLEVBR0E7QUFBRUssUUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFBWixPQUhBLENBQUosQ0FETSxFQU1OLHdDQUFJLHlCQUNBLCtEQUNBLHNFQUZBLENBQUosQ0FOTSxFQVdOO0FBQU0sUUFBQSxTQUFTLEVBQUMsK0NBQWhCO0FBQWdFLFFBQUEsUUFBUSxFQUFFLEtBQUtTO0FBQS9FLFNBQ0k7QUFBTyxRQUFBLFNBQVMsRUFBQywrQ0FBakI7QUFDSSxRQUFBLFFBQVEsRUFBRSxLQUFLQyxvQkFEbkI7QUFFSSxRQUFBLEtBQUssRUFBRSxLQUFLOUIsS0FBTCxDQUFXVCxXQUZ0QjtBQUdJLFFBQUEsU0FBUyxFQUFFO0FBSGYsUUFESixFQU1LMkIsU0FOTCxFQU9JLDZCQUFDLGFBQUQ7QUFDSSxRQUFBLGFBQWEsRUFBRSx5QkFBRyxNQUFILENBRG5CO0FBRUksUUFBQSxvQkFBb0IsRUFBRSxLQUFLVyxrQkFGL0I7QUFHSSxRQUFBLFNBQVMsRUFBRSxJQUhmO0FBSUksUUFBQSxRQUFRLEVBQUUsS0FBS04sU0FKbkI7QUFLSSxRQUFBLEtBQUssRUFBRSxLQUxYO0FBTUksUUFBQSxlQUFlLEVBQUUsQ0FBQyxLQUFLdkIsS0FBTCxDQUFXTjtBQU5qQyxRQVBKLENBWE0sRUEyQkwseUJBQ0csbURBQ0EsK0NBRkgsRUFHQyxFQUhELEVBR0s7QUFDRnFDLFFBQUFBLE1BQU0sRUFBRU4sQ0FBQyxJQUFJLDZCQUFDLGdCQUFEO0FBQWtCLFVBQUEsU0FBUyxFQUFDLGVBQTVCO0FBQ1QsVUFBQSxPQUFPLEVBQUMsTUFEQztBQUVULFVBQUEsT0FBTyxFQUFFLEtBQUtHO0FBRkwsV0FJUkgsQ0FKUTtBQURYLE9BSEwsQ0EzQkssQ0FBVjtBQXVDSDs7QUFFRCxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQyw4QkFBdEI7QUFDSSxNQUFBLFVBQVUsRUFBRSxLQUFLeEMsS0FBTCxDQUFXQyxVQUQzQjtBQUVJLE1BQUEsS0FBSyxFQUFFNkI7QUFGWCxPQUlBLDBDQUNLRCxPQURMLENBSkEsQ0FESjtBQVVIOztBQXpPc0U7Ozs4QkFBdERqQyx5QixlQUNFO0FBQ2Y7QUFDQThCLEVBQUFBLE9BQU8sRUFBRXFCLG1CQUFVQyxNQUFWLENBQWlCQyxVQUZYO0FBR2Y7QUFDQTdCLEVBQUFBLGVBQWUsRUFBRTJCLG1CQUFVRyxJQUFWLENBQWVEO0FBSmpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCwgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSBcInByb3AtdHlwZXNcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcblxyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG4vKlxyXG4gKiBBY2Nlc3MgU2VjdXJlIFNlY3JldCBTdG9yYWdlIGJ5IHJlcXVlc3RpbmcgdGhlIHVzZXIncyBwYXNzcGhyYXNlLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWNjZXNzU2VjcmV0U3RvcmFnZURpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyB7IHBhc3NwaHJhc2UsIHB1YmtleSB9XHJcbiAgICAgICAga2V5SW5mbzogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIC8vIEZ1bmN0aW9uIGZyb20gb25lIG9mIHsgcGFzc3BocmFzZSwgcmVjb3ZlcnlLZXkgfSAtPiBib29sZWFuXHJcbiAgICAgICAgY2hlY2tQcml2YXRlS2V5OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHJlY292ZXJ5S2V5OiBcIlwiLFxyXG4gICAgICAgICAgICByZWNvdmVyeUtleVZhbGlkOiBmYWxzZSxcclxuICAgICAgICAgICAgZm9yY2VSZWNvdmVyeUtleTogZmFsc2UsXHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6ICcnLFxyXG4gICAgICAgICAgICBrZXlNYXRjaGVzOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ2FuY2VsID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZChmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uVXNlUmVjb3ZlcnlLZXlDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZm9yY2VSZWNvdmVyeUtleTogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25SZXNldFJlY292ZXJ5Q2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJSZXNldHRpbmcgc2VjcmV0IHN0b3JhZ2UgdW5pbXBsZW1lbnRlZFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25SZWNvdmVyeUtleUNoYW5nZSA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJlY292ZXJ5S2V5OiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICAgICAgcmVjb3ZlcnlLZXlWYWxpZDogTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzVmFsaWRSZWNvdmVyeUtleShlLnRhcmdldC52YWx1ZSksXHJcbiAgICAgICAgICAgIGtleU1hdGNoZXM6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUGFzc1BocmFzZU5leHQgPSBhc3luYyAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucGFzc1BocmFzZS5sZW5ndGggPD0gMCkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsga2V5TWF0Y2hlczogbnVsbCB9KTtcclxuICAgICAgICBjb25zdCBpbnB1dCA9IHsgcGFzc3BocmFzZTogdGhpcy5zdGF0ZS5wYXNzUGhyYXNlIH07XHJcbiAgICAgICAgY29uc3Qga2V5TWF0Y2hlcyA9IGF3YWl0IHRoaXMucHJvcHMuY2hlY2tQcml2YXRlS2V5KGlucHV0KTtcclxuICAgICAgICBpZiAoa2V5TWF0Y2hlcykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoaW5wdXQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBrZXlNYXRjaGVzIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25SZWNvdmVyeUtleU5leHQgPSBhc3luYyAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJlY292ZXJ5S2V5VmFsaWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGtleU1hdGNoZXM6IG51bGwgfSk7XHJcbiAgICAgICAgY29uc3QgaW5wdXQgPSB7IHJlY292ZXJ5S2V5OiB0aGlzLnN0YXRlLnJlY292ZXJ5S2V5IH07XHJcbiAgICAgICAgY29uc3Qga2V5TWF0Y2hlcyA9IGF3YWl0IHRoaXMucHJvcHMuY2hlY2tQcml2YXRlS2V5KGlucHV0KTtcclxuICAgICAgICBpZiAoa2V5TWF0Y2hlcykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoaW5wdXQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBrZXlNYXRjaGVzIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25QYXNzUGhyYXNlQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGFzc1BocmFzZTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgICAgIGtleU1hdGNoZXM6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuXHJcbiAgICAgICAgY29uc3QgaGFzUGFzc3BocmFzZSA9IChcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5rZXlJbmZvICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMua2V5SW5mby5wYXNzcGhyYXNlICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMua2V5SW5mby5wYXNzcGhyYXNlLnNhbHQgJiZcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5rZXlJbmZvLnBhc3NwaHJhc2UuaXRlcmF0aW9uc1xyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCBjb250ZW50O1xyXG4gICAgICAgIGxldCB0aXRsZTtcclxuICAgICAgICBpZiAoaGFzUGFzc3BocmFzZSAmJiAhdGhpcy5zdGF0ZS5mb3JjZVJlY292ZXJ5S2V5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgICAgIHRpdGxlID0gX3QoXCJFbnRlciBzZWNyZXQgc3RvcmFnZSBwYXNzcGhyYXNlXCIpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGtleVN0YXR1cztcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUua2V5TWF0Y2hlcyA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIGtleVN0YXR1cyA9IDxkaXYgY2xhc3NOYW1lPVwibXhfQWNjZXNzU2VjcmV0U3RvcmFnZURpYWxvZ19rZXlTdGF0dXNcIj5cclxuICAgICAgICAgICAgICAgICAgICB7XCJcXHVEODNEXFx1REM0RSBcIn17X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVW5hYmxlIHRvIGFjY2VzcyBzZWNyZXQgc3RvcmFnZS4gUGxlYXNlIHZlcmlmeSB0aGF0IHlvdSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZW50ZXJlZCB0aGUgY29ycmVjdCBwYXNzcGhyYXNlLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2dfa2V5U3RhdHVzXCIgLz47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnRlbnQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiPGI+V2FybmluZzwvYj46IFlvdSBzaG91bGQgb25seSBhY2Nlc3Mgc2VjcmV0IHN0b3JhZ2UgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZnJvbSBhIHRydXN0ZWQgY29tcHV0ZXIuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgYjogc3ViID0+IDxiPntzdWJ9PC9iPiB9LFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJBY2Nlc3MgeW91ciBzZWN1cmUgbWVzc2FnZSBoaXN0b3J5IGFuZCB5b3VyIGNyb3NzLXNpZ25pbmcgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiaWRlbnRpdHkgZm9yIHZlcmlmeWluZyBvdGhlciBzZXNzaW9ucyBieSBlbnRlcmluZyB5b3VyIHBhc3NwaHJhc2UuXCIsXHJcbiAgICAgICAgICAgICAgICApfTwvcD5cclxuXHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBjbGFzc05hbWU9XCJteF9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nX3ByaW1hcnlDb250YWluZXJcIiBvblN1Ym1pdD17dGhpcy5fb25QYXNzUGhyYXNlTmV4dH0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0FjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2dfcGFzc1BocmFzZUlucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uUGFzc1BocmFzZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc1BocmFzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJuZXctcGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAge2tleVN0YXR1c31cclxuICAgICAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdCgnTmV4dCcpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5fb25QYXNzUGhyYXNlTmV4dH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvY3VzPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpbWFyeURpc2FibGVkPXt0aGlzLnN0YXRlLnBhc3NQaHJhc2UubGVuZ3RoID09PSAwfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJJZiB5b3UndmUgZm9yZ290dGVuIHlvdXIgcGFzc3BocmFzZSB5b3UgY2FuIFwiK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGJ1dHRvbjE+dXNlIHlvdXIgcmVjb3Zlcnkga2V5PC9idXR0b24xPiBvciBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8YnV0dG9uMj5zZXQgdXAgbmV3IHJlY292ZXJ5IG9wdGlvbnM8L2J1dHRvbjI+LlwiXHJcbiAgICAgICAgICAgICAgICAsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uMTogcyA9PiA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9saW5rQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudD1cInNwYW5cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblVzZVJlY292ZXJ5S2V5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c31cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbjI6IHMgPT4gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfbGlua0J1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQ9XCJzcGFuXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25SZXNldFJlY292ZXJ5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c31cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aXRsZSA9IF90KFwiRW50ZXIgc2VjcmV0IHN0b3JhZ2UgcmVjb3Zlcnkga2V5XCIpO1xyXG4gICAgICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGtleVN0YXR1cztcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVjb3ZlcnlLZXkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2dfa2V5U3RhdHVzXCIgLz47XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5rZXlNYXRjaGVzID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAga2V5U3RhdHVzID0gPGRpdiBjbGFzc05hbWU9XCJteF9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nX2tleVN0YXR1c1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIlxcdUQ4M0RcXHVEQzRFIFwifXtfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJVbmFibGUgdG8gYWNjZXNzIHNlY3JldCBzdG9yYWdlLiBQbGVhc2UgdmVyaWZ5IHRoYXQgeW91IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJlbnRlcmVkIHRoZSBjb3JyZWN0IHJlY292ZXJ5IGtleS5cIixcclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUucmVjb3ZlcnlLZXlWYWxpZCkge1xyXG4gICAgICAgICAgICAgICAga2V5U3RhdHVzID0gPGRpdiBjbGFzc05hbWU9XCJteF9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nX2tleVN0YXR1c1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIlxcdUQ4M0RcXHVEQzREIFwifXtfdChcIlRoaXMgbG9va3MgbGlrZSBhIHZhbGlkIHJlY292ZXJ5IGtleSFcIil9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2dfa2V5U3RhdHVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge1wiXFx1RDgzRFxcdURDNEUgXCJ9e190KFwiTm90IGEgdmFsaWQgcmVjb3Zlcnkga2V5XCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb250ZW50ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICBcIjxiPldhcm5pbmc8L2I+OiBZb3Ugc2hvdWxkIG9ubHkgYWNjZXNzIHNlY3JldCBzdG9yYWdlIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImZyb20gYSB0cnVzdGVkIGNvbXB1dGVyLlwiLCB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7IGI6IHN1YiA9PiA8Yj57c3VifTwvYj4gfSxcclxuICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiQWNjZXNzIHlvdXIgc2VjdXJlIG1lc3NhZ2UgaGlzdG9yeSBhbmQgeW91ciBjcm9zcy1zaWduaW5nIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImlkZW50aXR5IGZvciB2ZXJpZnlpbmcgb3RoZXIgc2Vzc2lvbnMgYnkgZW50ZXJpbmcgeW91ciByZWNvdmVyeSBrZXkuXCIsXHJcbiAgICAgICAgICAgICAgICApfTwvcD5cclxuXHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBjbGFzc05hbWU9XCJteF9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nX3ByaW1hcnlDb250YWluZXJcIiBvblN1Ym1pdD17dGhpcy5fb25SZWNvdmVyeUtleU5leHR9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJteF9BY2Nlc3NTZWNyZXRTdG9yYWdlRGlhbG9nX3JlY292ZXJ5S2V5SW5wdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25SZWNvdmVyeUtleUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucmVjb3ZlcnlLZXl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHtrZXlTdGF0dXN9XHJcbiAgICAgICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoJ05leHQnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uUmVjb3ZlcnlLZXlOZXh0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXt0aGlzLl9vbkNhbmNlbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9jdXM9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmltYXJ5RGlzYWJsZWQ9eyF0aGlzLnN0YXRlLnJlY292ZXJ5S2V5VmFsaWR9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICBcIklmIHlvdSd2ZSBmb3Jnb3R0ZW4geW91ciByZWNvdmVyeSBrZXkgeW91IGNhbiBcIitcclxuICAgICAgICAgICAgICAgICAgICBcIjxidXR0b24+c2V0IHVwIG5ldyByZWNvdmVyeSBvcHRpb25zPC9idXR0b24+LlwiXHJcbiAgICAgICAgICAgICAgICAsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uOiBzID0+IDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X2xpbmtCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50PVwic3BhblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uUmVzZXRSZWNvdmVyeUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3N9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPixcclxuICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZURpYWxvZyBjbGFzc05hbWU9J214X0FjY2Vzc1NlY3JldFN0b3JhZ2VEaWFsb2cnXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGl0bGV9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIHtjb250ZW50fVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19