"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard3(require("../../../../index"));

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _matrixJsSdk = require("matrix-js-sdk");

var _Modal = _interopRequireDefault(require("../../../../Modal"));

var _languageHandler = require("../../../../languageHandler");

var _CrossSigningManager = require("../../../../CrossSigningManager");

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const RESTORE_TYPE_PASSPHRASE = 0;
const RESTORE_TYPE_RECOVERYKEY = 1;
const RESTORE_TYPE_SECRET_STORAGE = 2;
/*
 * Dialog for restoring e2e keys from a backup and the user's recovery key
 */

class RestoreKeyBackupDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onCancel", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onDone", () => {
      this.props.onFinished(true);
    });
    (0, _defineProperty2.default)(this, "_onUseRecoveryKeyClick", () => {
      this.setState({
        forceRecoveryKey: true
      });
    });
    (0, _defineProperty2.default)(this, "_onResetRecoveryClick", () => {
      this.props.onFinished(false);

      _Modal.default.createTrackedDialogAsync('Key Backup', 'Key Backup', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../../async-components/views/dialogs/keybackup/CreateKeyBackupDialog'))), {
        onFinished: () => {
          this._loadBackupStatus();
        }
      }, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
    (0, _defineProperty2.default)(this, "_onRecoveryKeyChange", e => {
      this.setState({
        recoveryKey: e.target.value,
        recoveryKeyValid: _MatrixClientPeg.MatrixClientPeg.get().isValidRecoveryKey(e.target.value)
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseNext", async () => {
      this.setState({
        loading: true,
        restoreError: null,
        restoreType: RESTORE_TYPE_PASSPHRASE
      });

      try {
        // We do still restore the key backup: we must ensure that the key backup key
        // is the right one and restoring it is currently the only way we can do this.
        const recoverInfo = await _MatrixClientPeg.MatrixClientPeg.get().restoreKeyBackupWithPassword(this.state.passPhrase, undefined, undefined, this.state.backupInfo);

        if (this.props.keyCallback) {
          const key = await _MatrixClientPeg.MatrixClientPeg.get().keyBackupKeyFromPassword(this.state.passPhrase, this.state.backupInfo);
          this.props.keyCallback(key);
        }

        if (!this.props.showSummary) {
          this.props.onFinished(true);
          return;
        }

        this.setState({
          loading: false,
          recoverInfo
        });
      } catch (e) {
        console.log("Error restoring backup", e);
        this.setState({
          loading: false,
          restoreError: e
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onRecoveryKeyNext", async () => {
      if (!this.state.recoveryKeyValid) return;
      this.setState({
        loading: true,
        restoreError: null,
        restoreType: RESTORE_TYPE_RECOVERYKEY
      });

      try {
        const recoverInfo = await _MatrixClientPeg.MatrixClientPeg.get().restoreKeyBackupWithRecoveryKey(this.state.recoveryKey, undefined, undefined, this.state.backupInfo);

        if (this.props.keyCallback) {
          const key = _MatrixClientPeg.MatrixClientPeg.get().keyBackupKeyFromRecoveryKey(this.state.recoveryKey);

          this.props.keyCallback(key);
        }

        if (!this.props.showSummary) {
          this.props.onFinished(true);
          return;
        }

        this.setState({
          loading: false,
          recoverInfo
        });
      } catch (e) {
        console.log("Error restoring backup", e);
        this.setState({
          loading: false,
          restoreError: e
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseChange", e => {
      this.setState({
        passPhrase: e.target.value
      });
    });
    this.state = {
      backupInfo: null,
      backupKeyStored: null,
      loading: false,
      loadError: null,
      restoreError: null,
      recoveryKey: "",
      recoverInfo: null,
      recoveryKeyValid: false,
      forceRecoveryKey: false,
      passPhrase: '',
      restoreType: null
    };
  }

  componentDidMount() {
    this._loadBackupStatus();
  }

  async _restoreWithSecretStorage() {
    this.setState({
      loading: true,
      restoreError: null,
      restoreType: RESTORE_TYPE_SECRET_STORAGE
    });

    try {
      // `accessSecretStorage` may prompt for storage access as needed.
      const recoverInfo = await (0, _CrossSigningManager.accessSecretStorage)(async () => {
        return _MatrixClientPeg.MatrixClientPeg.get().restoreKeyBackupWithSecretStorage(this.state.backupInfo);
      });
      this.setState({
        loading: false,
        recoverInfo
      });
    } catch (e) {
      console.log("Error restoring backup", e);
      this.setState({
        restoreError: e,
        loading: false
      });
    }
  }

  async _restoreWithCachedKey(backupInfo) {
    if (!backupInfo) return false;

    try {
      const recoverInfo = await _MatrixClientPeg.MatrixClientPeg.get().restoreKeyBackupWithCache(undefined,
      /* targetRoomId */
      undefined,
      /* targetSessionId */
      backupInfo);
      this.setState({
        recoverInfo
      });
      return true;
    } catch (e) {
      console.log("restoreWithCachedKey failed:", e);
      return false;
    }
  }

  async _loadBackupStatus() {
    this.setState({
      loading: true,
      loadError: null
    });

    try {
      const backupInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
      const backupKeyStored = await _MatrixClientPeg.MatrixClientPeg.get().isKeyBackupKeyStored();
      this.setState({
        backupInfo,
        backupKeyStored
      });
      const gotCache = await this._restoreWithCachedKey(backupInfo);

      if (gotCache) {
        console.log("RestoreKeyBackupDialog: found cached backup key");
        this.setState({
          loading: false
        });
        return;
      } // If the backup key is stored, we can proceed directly to restore.


      if (backupKeyStored) {
        return this._restoreWithSecretStorage();
      }

      this.setState({
        loadError: null,
        loading: false
      });
    } catch (e) {
      console.log("Error loading backup status", e);
      this.setState({
        loadError: e,
        loading: false
      });
    }
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const Spinner = sdk.getComponent("elements.Spinner");
    const backupHasPassphrase = this.state.backupInfo && this.state.backupInfo.auth_data && this.state.backupInfo.auth_data.private_key_salt && this.state.backupInfo.auth_data.private_key_iterations;
    let content;
    let title;

    if (this.state.loading) {
      title = (0, _languageHandler._t)("Loading...");
      content = _react.default.createElement(Spinner, null);
    } else if (this.state.loadError) {
      title = (0, _languageHandler._t)("Error");
      content = (0, _languageHandler._t)("Unable to load backup status");
    } else if (this.state.restoreError) {
      if (this.state.restoreError.errcode === _matrixJsSdk.MatrixClient.RESTORE_BACKUP_ERROR_BAD_KEY) {
        if (this.state.restoreType === RESTORE_TYPE_RECOVERYKEY) {
          title = (0, _languageHandler._t)("Recovery key mismatch");
          content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Backup could not be decrypted with this key: " + "please verify that you entered the correct recovery key.")));
        } else {
          title = (0, _languageHandler._t)("Incorrect recovery passphrase");
          content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Backup could not be decrypted with this passphrase: " + "please verify that you entered the correct recovery passphrase.")));
        }
      } else {
        title = (0, _languageHandler._t)("Error");
        content = (0, _languageHandler._t)("Unable to restore backup");
      }
    } else if (this.state.backupInfo === null) {
      title = (0, _languageHandler._t)("Error");
      content = (0, _languageHandler._t)("No backup found!");
    } else if (this.state.recoverInfo) {
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      title = (0, _languageHandler._t)("Backup restored");
      let failedToDecrypt;

      if (this.state.recoverInfo.total > this.state.recoverInfo.imported) {
        failedToDecrypt = _react.default.createElement("p", null, (0, _languageHandler._t)("Failed to decrypt %(failedCount)s sessions!", {
          failedCount: this.state.recoverInfo.total - this.state.recoverInfo.imported
        }));
      }

      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Restored %(sessionCount)s session keys", {
        sessionCount: this.state.recoverInfo.imported
      })), failedToDecrypt, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('OK'),
        onPrimaryButtonClick: this._onDone,
        hasCancel: false,
        focus: true
      }));
    } else if (backupHasPassphrase && !this.state.forceRecoveryKey) {
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
      title = (0, _languageHandler._t)("Enter recovery passphrase");
      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("<b>Warning</b>: you should only set up key backup " + "from a trusted computer.", {}, {
        b: sub => _react.default.createElement("b", null, sub)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Access your secure message history and set up secure " + "messaging by entering your recovery passphrase.")), _react.default.createElement("form", {
        className: "mx_RestoreKeyBackupDialog_primaryContainer"
      }, _react.default.createElement("input", {
        type: "password",
        className: "mx_RestoreKeyBackupDialog_passPhraseInput",
        onChange: this._onPassPhraseChange,
        value: this.state.passPhrase,
        autoFocus: true
      }), _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Next'),
        onPrimaryButtonClick: this._onPassPhraseNext,
        primaryIsSubmit: true,
        hasCancel: true,
        onCancel: this._onCancel,
        focus: false
      })), (0, _languageHandler._t)("If you've forgotten your recovery passphrase you can " + "<button1>use your recovery key</button1> or " + "<button2>set up new recovery options</button2>", {}, {
        button1: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onUseRecoveryKeyClick
        }, s),
        button2: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onResetRecoveryClick
        }, s)
      }));
    } else {
      title = (0, _languageHandler._t)("Enter recovery key");
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
      let keyStatus;

      if (this.state.recoveryKey.length === 0) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_RestoreKeyBackupDialog_keyStatus"
        });
      } else if (this.state.recoveryKeyValid) {
        keyStatus = _react.default.createElement("div", {
          className: "mx_RestoreKeyBackupDialog_keyStatus"
        }, "\uD83D\uDC4D ", (0, _languageHandler._t)("This looks like a valid recovery key!"));
      } else {
        keyStatus = _react.default.createElement("div", {
          className: "mx_RestoreKeyBackupDialog_keyStatus"
        }, "\uD83D\uDC4E ", (0, _languageHandler._t)("Not a valid recovery key"));
      }

      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("<b>Warning</b>: You should only set up key backup " + "from a trusted computer.", {}, {
        b: sub => _react.default.createElement("b", null, sub)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Access your secure message history and set up secure " + "messaging by entering your recovery key.")), _react.default.createElement("div", {
        className: "mx_RestoreKeyBackupDialog_primaryContainer"
      }, _react.default.createElement("input", {
        className: "mx_RestoreKeyBackupDialog_recoveryKeyInput",
        onChange: this._onRecoveryKeyChange,
        value: this.state.recoveryKey,
        autoFocus: true
      }), keyStatus, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Next'),
        onPrimaryButtonClick: this._onRecoveryKeyNext,
        hasCancel: true,
        onCancel: this._onCancel,
        focus: false,
        primaryDisabled: !this.state.recoveryKeyValid
      })), (0, _languageHandler._t)("If you've forgotten your recovery key you can " + "<button>set up new recovery options</button>", {}, {
        button: s => _react.default.createElement(AccessibleButton, {
          className: "mx_linkButton",
          element: "span",
          onClick: this._onResetRecoveryClick
        }, s)
      }));
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_RestoreKeyBackupDialog",
      onFinished: this.props.onFinished,
      title: title
    }, _react.default.createElement("div", null, content));
  }

}

exports.default = RestoreKeyBackupDialog;
(0, _defineProperty2.default)(RestoreKeyBackupDialog, "propTypes", {
  // if false, will close the dialog as soon as the restore completes succesfully
  // default: true
  showSummary: _propTypes.default.bool,
  // If specified, gather the key from the user but then call the function with the backup
  // key rather than actually (necessarily) restoring the backup.
  keyCallback: _propTypes.default.func
});
(0, _defineProperty2.default)(RestoreKeyBackupDialog, "defaultProps", {
  showSummary: true
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mva2V5YmFja3VwL1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2cuanMiXSwibmFtZXMiOlsiUkVTVE9SRV9UWVBFX1BBU1NQSFJBU0UiLCJSRVNUT1JFX1RZUEVfUkVDT1ZFUllLRVkiLCJSRVNUT1JFX1RZUEVfU0VDUkVUX1NUT1JBR0UiLCJSZXN0b3JlS2V5QmFja3VwRGlhbG9nIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJzZXRTdGF0ZSIsImZvcmNlUmVjb3ZlcnlLZXkiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2dBc3luYyIsIl9sb2FkQmFja3VwU3RhdHVzIiwiZSIsInJlY292ZXJ5S2V5IiwidGFyZ2V0IiwidmFsdWUiLCJyZWNvdmVyeUtleVZhbGlkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiaXNWYWxpZFJlY292ZXJ5S2V5IiwibG9hZGluZyIsInJlc3RvcmVFcnJvciIsInJlc3RvcmVUeXBlIiwicmVjb3ZlckluZm8iLCJyZXN0b3JlS2V5QmFja3VwV2l0aFBhc3N3b3JkIiwic3RhdGUiLCJwYXNzUGhyYXNlIiwidW5kZWZpbmVkIiwiYmFja3VwSW5mbyIsImtleUNhbGxiYWNrIiwia2V5Iiwia2V5QmFja3VwS2V5RnJvbVBhc3N3b3JkIiwic2hvd1N1bW1hcnkiLCJjb25zb2xlIiwibG9nIiwicmVzdG9yZUtleUJhY2t1cFdpdGhSZWNvdmVyeUtleSIsImtleUJhY2t1cEtleUZyb21SZWNvdmVyeUtleSIsImJhY2t1cEtleVN0b3JlZCIsImxvYWRFcnJvciIsImNvbXBvbmVudERpZE1vdW50IiwiX3Jlc3RvcmVXaXRoU2VjcmV0U3RvcmFnZSIsInJlc3RvcmVLZXlCYWNrdXBXaXRoU2VjcmV0U3RvcmFnZSIsIl9yZXN0b3JlV2l0aENhY2hlZEtleSIsInJlc3RvcmVLZXlCYWNrdXBXaXRoQ2FjaGUiLCJnZXRLZXlCYWNrdXBWZXJzaW9uIiwiaXNLZXlCYWNrdXBLZXlTdG9yZWQiLCJnb3RDYWNoZSIsInJlbmRlciIsIkJhc2VEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJTcGlubmVyIiwiYmFja3VwSGFzUGFzc3BocmFzZSIsImF1dGhfZGF0YSIsInByaXZhdGVfa2V5X3NhbHQiLCJwcml2YXRlX2tleV9pdGVyYXRpb25zIiwiY29udGVudCIsInRpdGxlIiwiZXJyY29kZSIsIk1hdHJpeENsaWVudCIsIlJFU1RPUkVfQkFDS1VQX0VSUk9SX0JBRF9LRVkiLCJEaWFsb2dCdXR0b25zIiwiZmFpbGVkVG9EZWNyeXB0IiwidG90YWwiLCJpbXBvcnRlZCIsImZhaWxlZENvdW50Iiwic2Vzc2lvbkNvdW50IiwiX29uRG9uZSIsIkFjY2Vzc2libGVCdXR0b24iLCJiIiwic3ViIiwiX29uUGFzc1BocmFzZUNoYW5nZSIsIl9vblBhc3NQaHJhc2VOZXh0IiwiX29uQ2FuY2VsIiwiYnV0dG9uMSIsInMiLCJfb25Vc2VSZWNvdmVyeUtleUNsaWNrIiwiYnV0dG9uMiIsIl9vblJlc2V0UmVjb3ZlcnlDbGljayIsImtleVN0YXR1cyIsImxlbmd0aCIsIl9vblJlY292ZXJ5S2V5Q2hhbmdlIiwiX29uUmVjb3ZlcnlLZXlOZXh0IiwiYnV0dG9uIiwiUHJvcFR5cGVzIiwiYm9vbCIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkEsTUFBTUEsdUJBQXVCLEdBQUcsQ0FBaEM7QUFDQSxNQUFNQyx3QkFBd0IsR0FBRyxDQUFqQztBQUNBLE1BQU1DLDJCQUEyQixHQUFHLENBQXBDO0FBRUE7Ozs7QUFHZSxNQUFNQyxzQkFBTixTQUFxQ0MsZUFBTUMsYUFBM0MsQ0FBeUQ7QUFjcEVDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLHFEQXFCUCxNQUFNO0FBQ2QsV0FBS0EsS0FBTCxDQUFXQyxVQUFYLENBQXNCLEtBQXRCO0FBQ0gsS0F2QmtCO0FBQUEsbURBeUJULE1BQU07QUFDWixXQUFLRCxLQUFMLENBQVdDLFVBQVgsQ0FBc0IsSUFBdEI7QUFDSCxLQTNCa0I7QUFBQSxrRUE2Qk0sTUFBTTtBQUMzQixXQUFLQyxRQUFMLENBQWM7QUFDVkMsUUFBQUEsZ0JBQWdCLEVBQUU7QUFEUixPQUFkO0FBR0gsS0FqQ2tCO0FBQUEsaUVBbUNLLE1BQU07QUFDMUIsV0FBS0gsS0FBTCxDQUFXQyxVQUFYLENBQXNCLEtBQXRCOztBQUNBRyxxQkFBTUMsd0JBQU4sQ0FBK0IsWUFBL0IsRUFBNkMsWUFBN0MsNkVBQ1csNEVBRFgsS0FFSTtBQUNJSixRQUFBQSxVQUFVLEVBQUUsTUFBTTtBQUNkLGVBQUtLLGlCQUFMO0FBQ0g7QUFITCxPQUZKLEVBTU8sSUFOUDtBQU1hO0FBQWlCLFdBTjlCO0FBTXFDO0FBQWUsVUFOcEQ7QUFRSCxLQTdDa0I7QUFBQSxnRUErQ0tDLENBQUQsSUFBTztBQUMxQixXQUFLTCxRQUFMLENBQWM7QUFDVk0sUUFBQUEsV0FBVyxFQUFFRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FEWjtBQUVWQyxRQUFBQSxnQkFBZ0IsRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsa0JBQXRCLENBQXlDUCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBbEQ7QUFGUixPQUFkO0FBSUgsS0FwRGtCO0FBQUEsNkRBc0RDLFlBQVk7QUFDNUIsV0FBS1IsUUFBTCxDQUFjO0FBQ1ZhLFFBQUFBLE9BQU8sRUFBRSxJQURDO0FBRVZDLFFBQUFBLFlBQVksRUFBRSxJQUZKO0FBR1ZDLFFBQUFBLFdBQVcsRUFBRXhCO0FBSEgsT0FBZDs7QUFLQSxVQUFJO0FBQ0E7QUFDQTtBQUNBLGNBQU15QixXQUFXLEdBQUcsTUFBTU4saUNBQWdCQyxHQUFoQixHQUFzQk0sNEJBQXRCLENBQ3RCLEtBQUtDLEtBQUwsQ0FBV0MsVUFEVyxFQUNDQyxTQURELEVBQ1lBLFNBRFosRUFDdUIsS0FBS0YsS0FBTCxDQUFXRyxVQURsQyxDQUExQjs7QUFHQSxZQUFJLEtBQUt2QixLQUFMLENBQVd3QixXQUFmLEVBQTRCO0FBQ3hCLGdCQUFNQyxHQUFHLEdBQUcsTUFBTWIsaUNBQWdCQyxHQUFoQixHQUFzQmEsd0JBQXRCLENBQ2QsS0FBS04sS0FBTCxDQUFXQyxVQURHLEVBQ1MsS0FBS0QsS0FBTCxDQUFXRyxVQURwQixDQUFsQjtBQUdBLGVBQUt2QixLQUFMLENBQVd3QixXQUFYLENBQXVCQyxHQUF2QjtBQUNIOztBQUVELFlBQUksQ0FBQyxLQUFLekIsS0FBTCxDQUFXMkIsV0FBaEIsRUFBNkI7QUFDekIsZUFBSzNCLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixJQUF0QjtBQUNBO0FBQ0g7O0FBQ0QsYUFBS0MsUUFBTCxDQUFjO0FBQ1ZhLFVBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZHLFVBQUFBO0FBRlUsU0FBZDtBQUlILE9BckJELENBcUJFLE9BQU9YLENBQVAsRUFBVTtBQUNScUIsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksd0JBQVosRUFBc0N0QixDQUF0QztBQUNBLGFBQUtMLFFBQUwsQ0FBYztBQUNWYSxVQUFBQSxPQUFPLEVBQUUsS0FEQztBQUVWQyxVQUFBQSxZQUFZLEVBQUVUO0FBRkosU0FBZDtBQUlIO0FBQ0osS0F4RmtCO0FBQUEsOERBMEZFLFlBQVk7QUFDN0IsVUFBSSxDQUFDLEtBQUthLEtBQUwsQ0FBV1QsZ0JBQWhCLEVBQWtDO0FBRWxDLFdBQUtULFFBQUwsQ0FBYztBQUNWYSxRQUFBQSxPQUFPLEVBQUUsSUFEQztBQUVWQyxRQUFBQSxZQUFZLEVBQUUsSUFGSjtBQUdWQyxRQUFBQSxXQUFXLEVBQUV2QjtBQUhILE9BQWQ7O0FBS0EsVUFBSTtBQUNBLGNBQU13QixXQUFXLEdBQUcsTUFBTU4saUNBQWdCQyxHQUFoQixHQUFzQmlCLCtCQUF0QixDQUN0QixLQUFLVixLQUFMLENBQVdaLFdBRFcsRUFDRWMsU0FERixFQUNhQSxTQURiLEVBQ3dCLEtBQUtGLEtBQUwsQ0FBV0csVUFEbkMsQ0FBMUI7O0FBR0EsWUFBSSxLQUFLdkIsS0FBTCxDQUFXd0IsV0FBZixFQUE0QjtBQUN4QixnQkFBTUMsR0FBRyxHQUFHYixpQ0FBZ0JDLEdBQWhCLEdBQXNCa0IsMkJBQXRCLENBQWtELEtBQUtYLEtBQUwsQ0FBV1osV0FBN0QsQ0FBWjs7QUFDQSxlQUFLUixLQUFMLENBQVd3QixXQUFYLENBQXVCQyxHQUF2QjtBQUNIOztBQUNELFlBQUksQ0FBQyxLQUFLekIsS0FBTCxDQUFXMkIsV0FBaEIsRUFBNkI7QUFDekIsZUFBSzNCLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixJQUF0QjtBQUNBO0FBQ0g7O0FBQ0QsYUFBS0MsUUFBTCxDQUFjO0FBQ1ZhLFVBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZHLFVBQUFBO0FBRlUsU0FBZDtBQUlILE9BaEJELENBZ0JFLE9BQU9YLENBQVAsRUFBVTtBQUNScUIsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksd0JBQVosRUFBc0N0QixDQUF0QztBQUNBLGFBQUtMLFFBQUwsQ0FBYztBQUNWYSxVQUFBQSxPQUFPLEVBQUUsS0FEQztBQUVWQyxVQUFBQSxZQUFZLEVBQUVUO0FBRkosU0FBZDtBQUlIO0FBQ0osS0F6SGtCO0FBQUEsK0RBMkhJQSxDQUFELElBQU87QUFDekIsV0FBS0wsUUFBTCxDQUFjO0FBQ1ZtQixRQUFBQSxVQUFVLEVBQUVkLENBQUMsQ0FBQ0UsTUFBRixDQUFTQztBQURYLE9BQWQ7QUFHSCxLQS9Ia0I7QUFFZixTQUFLVSxLQUFMLEdBQWE7QUFDVEcsTUFBQUEsVUFBVSxFQUFFLElBREg7QUFFVFMsTUFBQUEsZUFBZSxFQUFFLElBRlI7QUFHVGpCLE1BQUFBLE9BQU8sRUFBRSxLQUhBO0FBSVRrQixNQUFBQSxTQUFTLEVBQUUsSUFKRjtBQUtUakIsTUFBQUEsWUFBWSxFQUFFLElBTEw7QUFNVFIsTUFBQUEsV0FBVyxFQUFFLEVBTko7QUFPVFUsTUFBQUEsV0FBVyxFQUFFLElBUEo7QUFRVFAsTUFBQUEsZ0JBQWdCLEVBQUUsS0FSVDtBQVNUUixNQUFBQSxnQkFBZ0IsRUFBRSxLQVRUO0FBVVRrQixNQUFBQSxVQUFVLEVBQUUsRUFWSDtBQVdUSixNQUFBQSxXQUFXLEVBQUU7QUFYSixLQUFiO0FBYUg7O0FBRURpQixFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixTQUFLNUIsaUJBQUw7QUFDSDs7QUE4R0QsUUFBTTZCLHlCQUFOLEdBQWtDO0FBQzlCLFNBQUtqQyxRQUFMLENBQWM7QUFDVmEsTUFBQUEsT0FBTyxFQUFFLElBREM7QUFFVkMsTUFBQUEsWUFBWSxFQUFFLElBRko7QUFHVkMsTUFBQUEsV0FBVyxFQUFFdEI7QUFISCxLQUFkOztBQUtBLFFBQUk7QUFDQTtBQUNBLFlBQU11QixXQUFXLEdBQUcsTUFBTSw4Q0FBb0IsWUFBWTtBQUN0RCxlQUFPTixpQ0FBZ0JDLEdBQWhCLEdBQXNCdUIsaUNBQXRCLENBQ0gsS0FBS2hCLEtBQUwsQ0FBV0csVUFEUixDQUFQO0FBR0gsT0FKeUIsQ0FBMUI7QUFLQSxXQUFLckIsUUFBTCxDQUFjO0FBQ1ZhLFFBQUFBLE9BQU8sRUFBRSxLQURDO0FBRVZHLFFBQUFBO0FBRlUsT0FBZDtBQUlILEtBWEQsQ0FXRSxPQUFPWCxDQUFQLEVBQVU7QUFDUnFCLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHdCQUFaLEVBQXNDdEIsQ0FBdEM7QUFDQSxXQUFLTCxRQUFMLENBQWM7QUFDVmMsUUFBQUEsWUFBWSxFQUFFVCxDQURKO0FBRVZRLFFBQUFBLE9BQU8sRUFBRTtBQUZDLE9BQWQ7QUFJSDtBQUNKOztBQUVELFFBQU1zQixxQkFBTixDQUE0QmQsVUFBNUIsRUFBd0M7QUFDcEMsUUFBSSxDQUFDQSxVQUFMLEVBQWlCLE9BQU8sS0FBUDs7QUFDakIsUUFBSTtBQUNBLFlBQU1MLFdBQVcsR0FBRyxNQUFNTixpQ0FBZ0JDLEdBQWhCLEdBQXNCeUIseUJBQXRCLENBQ3RCaEIsU0FEc0I7QUFDWDtBQUNYQSxNQUFBQSxTQUZzQjtBQUVYO0FBQ1hDLE1BQUFBLFVBSHNCLENBQTFCO0FBS0EsV0FBS3JCLFFBQUwsQ0FBYztBQUNWZ0IsUUFBQUE7QUFEVSxPQUFkO0FBR0EsYUFBTyxJQUFQO0FBQ0gsS0FWRCxDQVVFLE9BQU9YLENBQVAsRUFBVTtBQUNScUIsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksOEJBQVosRUFBNEN0QixDQUE1QztBQUNBLGFBQU8sS0FBUDtBQUNIO0FBQ0o7O0FBRUQsUUFBTUQsaUJBQU4sR0FBMEI7QUFDdEIsU0FBS0osUUFBTCxDQUFjO0FBQ1ZhLE1BQUFBLE9BQU8sRUFBRSxJQURDO0FBRVZrQixNQUFBQSxTQUFTLEVBQUU7QUFGRCxLQUFkOztBQUlBLFFBQUk7QUFDQSxZQUFNVixVQUFVLEdBQUcsTUFBTVgsaUNBQWdCQyxHQUFoQixHQUFzQjBCLG1CQUF0QixFQUF6QjtBQUNBLFlBQU1QLGVBQWUsR0FBRyxNQUFNcEIsaUNBQWdCQyxHQUFoQixHQUFzQjJCLG9CQUF0QixFQUE5QjtBQUNBLFdBQUt0QyxRQUFMLENBQWM7QUFDVnFCLFFBQUFBLFVBRFU7QUFFVlMsUUFBQUE7QUFGVSxPQUFkO0FBS0EsWUFBTVMsUUFBUSxHQUFHLE1BQU0sS0FBS0oscUJBQUwsQ0FBMkJkLFVBQTNCLENBQXZCOztBQUNBLFVBQUlrQixRQUFKLEVBQWM7QUFDVmIsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksaURBQVo7QUFDQSxhQUFLM0IsUUFBTCxDQUFjO0FBQ1ZhLFVBQUFBLE9BQU8sRUFBRTtBQURDLFNBQWQ7QUFHQTtBQUNILE9BZkQsQ0FpQkE7OztBQUNBLFVBQUlpQixlQUFKLEVBQXFCO0FBQ2pCLGVBQU8sS0FBS0cseUJBQUwsRUFBUDtBQUNIOztBQUVELFdBQUtqQyxRQUFMLENBQWM7QUFDVitCLFFBQUFBLFNBQVMsRUFBRSxJQUREO0FBRVZsQixRQUFBQSxPQUFPLEVBQUU7QUFGQyxPQUFkO0FBSUgsS0ExQkQsQ0EwQkUsT0FBT1IsQ0FBUCxFQUFVO0FBQ1JxQixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSw2QkFBWixFQUEyQ3RCLENBQTNDO0FBQ0EsV0FBS0wsUUFBTCxDQUFjO0FBQ1YrQixRQUFBQSxTQUFTLEVBQUUxQixDQUREO0FBRVZRLFFBQUFBLE9BQU8sRUFBRTtBQUZDLE9BQWQ7QUFJSDtBQUNKOztBQUVEMkIsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsT0FBTyxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBRUEsVUFBTUUsbUJBQW1CLEdBQ3JCLEtBQUszQixLQUFMLENBQVdHLFVBQVgsSUFDQSxLQUFLSCxLQUFMLENBQVdHLFVBQVgsQ0FBc0J5QixTQUR0QixJQUVBLEtBQUs1QixLQUFMLENBQVdHLFVBQVgsQ0FBc0J5QixTQUF0QixDQUFnQ0MsZ0JBRmhDLElBR0EsS0FBSzdCLEtBQUwsQ0FBV0csVUFBWCxDQUFzQnlCLFNBQXRCLENBQWdDRSxzQkFKcEM7QUFPQSxRQUFJQyxPQUFKO0FBQ0EsUUFBSUMsS0FBSjs7QUFDQSxRQUFJLEtBQUtoQyxLQUFMLENBQVdMLE9BQWYsRUFBd0I7QUFDcEJxQyxNQUFBQSxLQUFLLEdBQUcseUJBQUcsWUFBSCxDQUFSO0FBQ0FELE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxPQUFELE9BQVY7QUFDSCxLQUhELE1BR08sSUFBSSxLQUFLL0IsS0FBTCxDQUFXYSxTQUFmLEVBQTBCO0FBQzdCbUIsTUFBQUEsS0FBSyxHQUFHLHlCQUFHLE9BQUgsQ0FBUjtBQUNBRCxNQUFBQSxPQUFPLEdBQUcseUJBQUcsOEJBQUgsQ0FBVjtBQUNILEtBSE0sTUFHQSxJQUFJLEtBQUsvQixLQUFMLENBQVdKLFlBQWYsRUFBNkI7QUFDaEMsVUFBSSxLQUFLSSxLQUFMLENBQVdKLFlBQVgsQ0FBd0JxQyxPQUF4QixLQUFvQ0MsMEJBQWFDLDRCQUFyRCxFQUFtRjtBQUMvRSxZQUFJLEtBQUtuQyxLQUFMLENBQVdILFdBQVgsS0FBMkJ2Qix3QkFBL0IsRUFBeUQ7QUFDckQwRCxVQUFBQSxLQUFLLEdBQUcseUJBQUcsdUJBQUgsQ0FBUjtBQUNBRCxVQUFBQSxPQUFPLEdBQUcsMENBQ04sd0NBQUkseUJBQ0Esa0RBQ0EsMERBRkEsQ0FBSixDQURNLENBQVY7QUFNSCxTQVJELE1BUU87QUFDSEMsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLCtCQUFILENBQVI7QUFDQUQsVUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLHlEQUNBLGlFQUZBLENBQUosQ0FETSxDQUFWO0FBTUg7QUFDSixPQWxCRCxNQWtCTztBQUNIQyxRQUFBQSxLQUFLLEdBQUcseUJBQUcsT0FBSCxDQUFSO0FBQ0FELFFBQUFBLE9BQU8sR0FBRyx5QkFBRywwQkFBSCxDQUFWO0FBQ0g7QUFDSixLQXZCTSxNQXVCQSxJQUFJLEtBQUsvQixLQUFMLENBQVdHLFVBQVgsS0FBMEIsSUFBOUIsRUFBb0M7QUFDdkM2QixNQUFBQSxLQUFLLEdBQUcseUJBQUcsT0FBSCxDQUFSO0FBQ0FELE1BQUFBLE9BQU8sR0FBRyx5QkFBRyxrQkFBSCxDQUFWO0FBQ0gsS0FITSxNQUdBLElBQUksS0FBSy9CLEtBQUwsQ0FBV0YsV0FBZixFQUE0QjtBQUMvQixZQUFNc0MsYUFBYSxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0FPLE1BQUFBLEtBQUssR0FBRyx5QkFBRyxpQkFBSCxDQUFSO0FBQ0EsVUFBSUssZUFBSjs7QUFDQSxVQUFJLEtBQUtyQyxLQUFMLENBQVdGLFdBQVgsQ0FBdUJ3QyxLQUF2QixHQUErQixLQUFLdEMsS0FBTCxDQUFXRixXQUFYLENBQXVCeUMsUUFBMUQsRUFBb0U7QUFDaEVGLFFBQUFBLGVBQWUsR0FBRyx3Q0FBSSx5QkFDbEIsNkNBRGtCLEVBRWxCO0FBQUNHLFVBQUFBLFdBQVcsRUFBRSxLQUFLeEMsS0FBTCxDQUFXRixXQUFYLENBQXVCd0MsS0FBdkIsR0FBK0IsS0FBS3RDLEtBQUwsQ0FBV0YsV0FBWCxDQUF1QnlDO0FBQXBFLFNBRmtCLENBQUosQ0FBbEI7QUFJSDs7QUFDRFIsTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUFHLHdDQUFILEVBQTZDO0FBQUNVLFFBQUFBLFlBQVksRUFBRSxLQUFLekMsS0FBTCxDQUFXRixXQUFYLENBQXVCeUM7QUFBdEMsT0FBN0MsQ0FBSixDQURNLEVBRUxGLGVBRkssRUFHTiw2QkFBQyxhQUFEO0FBQWUsUUFBQSxhQUFhLEVBQUUseUJBQUcsSUFBSCxDQUE5QjtBQUNJLFFBQUEsb0JBQW9CLEVBQUUsS0FBS0ssT0FEL0I7QUFFSSxRQUFBLFNBQVMsRUFBRSxLQUZmO0FBR0ksUUFBQSxLQUFLLEVBQUU7QUFIWCxRQUhNLENBQVY7QUFTSCxLQW5CTSxNQW1CQSxJQUFJZixtQkFBbUIsSUFBSSxDQUFDLEtBQUszQixLQUFMLENBQVdqQixnQkFBdkMsRUFBeUQ7QUFDNUQsWUFBTXFELGFBQWEsR0FBR1osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFlBQU1rQixnQkFBZ0IsR0FBR25CLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQU8sTUFBQUEsS0FBSyxHQUFHLHlCQUFHLDJCQUFILENBQVI7QUFDQUQsTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLHVEQUNBLDBCQUZBLEVBRTRCLEVBRjVCLEVBR0E7QUFBRWEsUUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFBWixPQUhBLENBQUosQ0FETSxFQU1OLHdDQUFJLHlCQUNBLDBEQUNBLGlEQUZBLENBQUosQ0FOTSxFQVdOO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FDSTtBQUFPLFFBQUEsSUFBSSxFQUFDLFVBQVo7QUFDSSxRQUFBLFNBQVMsRUFBQywyQ0FEZDtBQUVJLFFBQUEsUUFBUSxFQUFFLEtBQUtDLG1CQUZuQjtBQUdJLFFBQUEsS0FBSyxFQUFFLEtBQUs5QyxLQUFMLENBQVdDLFVBSHRCO0FBSUksUUFBQSxTQUFTLEVBQUU7QUFKZixRQURKLEVBT0ksNkJBQUMsYUFBRDtBQUNJLFFBQUEsYUFBYSxFQUFFLHlCQUFHLE1BQUgsQ0FEbkI7QUFFSSxRQUFBLG9CQUFvQixFQUFFLEtBQUs4QyxpQkFGL0I7QUFHSSxRQUFBLGVBQWUsRUFBRSxJQUhyQjtBQUlJLFFBQUEsU0FBUyxFQUFFLElBSmY7QUFLSSxRQUFBLFFBQVEsRUFBRSxLQUFLQyxTQUxuQjtBQU1JLFFBQUEsS0FBSyxFQUFFO0FBTlgsUUFQSixDQVhNLEVBMkJMLHlCQUNHLDBEQUNBLDhDQURBLEdBRUEsZ0RBSEgsRUFJQyxFQUpELEVBSUs7QUFDRkMsUUFBQUEsT0FBTyxFQUFFQyxDQUFDLElBQUksNkJBQUMsZ0JBQUQ7QUFBa0IsVUFBQSxTQUFTLEVBQUMsZUFBNUI7QUFDVixVQUFBLE9BQU8sRUFBQyxNQURFO0FBRVYsVUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFGSixXQUlURCxDQUpTLENBRFo7QUFPRkUsUUFBQUEsT0FBTyxFQUFFRixDQUFDLElBQUksNkJBQUMsZ0JBQUQ7QUFBa0IsVUFBQSxTQUFTLEVBQUMsZUFBNUI7QUFDVixVQUFBLE9BQU8sRUFBQyxNQURFO0FBRVYsVUFBQSxPQUFPLEVBQUUsS0FBS0c7QUFGSixXQUlUSCxDQUpTO0FBUFosT0FKTCxDQTNCSyxDQUFWO0FBOENILEtBbERNLE1Ba0RBO0FBQ0hsQixNQUFBQSxLQUFLLEdBQUcseUJBQUcsb0JBQUgsQ0FBUjtBQUNBLFlBQU1JLGFBQWEsR0FBR1osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFlBQU1rQixnQkFBZ0IsR0FBR25CLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFFQSxVQUFJNkIsU0FBSjs7QUFDQSxVQUFJLEtBQUt0RCxLQUFMLENBQVdaLFdBQVgsQ0FBdUJtRSxNQUF2QixLQUFrQyxDQUF0QyxFQUF5QztBQUNyQ0QsUUFBQUEsU0FBUyxHQUFHO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixVQUFaO0FBQ0gsT0FGRCxNQUVPLElBQUksS0FBS3RELEtBQUwsQ0FBV1QsZ0JBQWYsRUFBaUM7QUFDcEMrRCxRQUFBQSxTQUFTLEdBQUc7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ1AsZUFETyxFQUNVLHlCQUFHLHVDQUFILENBRFYsQ0FBWjtBQUdILE9BSk0sTUFJQTtBQUNIQSxRQUFBQSxTQUFTLEdBQUc7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ1AsZUFETyxFQUNVLHlCQUFHLDBCQUFILENBRFYsQ0FBWjtBQUdIOztBQUVEdkIsTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLHVEQUNBLDBCQUZBLEVBRTRCLEVBRjVCLEVBR0E7QUFBRWEsUUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFBWixPQUhBLENBQUosQ0FETSxFQU1OLHdDQUFJLHlCQUNBLDBEQUNBLDBDQUZBLENBQUosQ0FOTSxFQVdOO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQU8sUUFBQSxTQUFTLEVBQUMsNENBQWpCO0FBQ0ksUUFBQSxRQUFRLEVBQUUsS0FBS1csb0JBRG5CO0FBRUksUUFBQSxLQUFLLEVBQUUsS0FBS3hELEtBQUwsQ0FBV1osV0FGdEI7QUFHSSxRQUFBLFNBQVMsRUFBRTtBQUhmLFFBREosRUFNS2tFLFNBTkwsRUFPSSw2QkFBQyxhQUFEO0FBQWUsUUFBQSxhQUFhLEVBQUUseUJBQUcsTUFBSCxDQUE5QjtBQUNJLFFBQUEsb0JBQW9CLEVBQUUsS0FBS0csa0JBRC9CO0FBRUksUUFBQSxTQUFTLEVBQUUsSUFGZjtBQUdJLFFBQUEsUUFBUSxFQUFFLEtBQUtULFNBSG5CO0FBSUksUUFBQSxLQUFLLEVBQUUsS0FKWDtBQUtJLFFBQUEsZUFBZSxFQUFFLENBQUMsS0FBS2hELEtBQUwsQ0FBV1Q7QUFMakMsUUFQSixDQVhNLEVBMEJMLHlCQUNHLG1EQUNBLDhDQUZILEVBR0MsRUFIRCxFQUdLO0FBQ0ZtRSxRQUFBQSxNQUFNLEVBQUVSLENBQUMsSUFBSSw2QkFBQyxnQkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyxlQUE1QjtBQUNULFVBQUEsT0FBTyxFQUFDLE1BREM7QUFFVCxVQUFBLE9BQU8sRUFBRSxLQUFLRztBQUZMLFdBSVJILENBSlE7QUFEWCxPQUhMLENBMUJLLENBQVY7QUFzQ0g7O0FBRUQsV0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxTQUFTLEVBQUMsMkJBQXRCO0FBQ0ksTUFBQSxVQUFVLEVBQUUsS0FBS3RFLEtBQUwsQ0FBV0MsVUFEM0I7QUFFSSxNQUFBLEtBQUssRUFBRW1EO0FBRlgsT0FJQSwwQ0FDS0QsT0FETCxDQUpBLENBREo7QUFVSDs7QUF6Wm1FOzs7OEJBQW5EdkQsc0IsZUFDRTtBQUNmO0FBQ0E7QUFDQStCLEVBQUFBLFdBQVcsRUFBRW9ELG1CQUFVQyxJQUhSO0FBSWY7QUFDQTtBQUNBeEQsRUFBQUEsV0FBVyxFQUFFdUQsbUJBQVVFO0FBTlIsQzs4QkFERnJGLHNCLGtCQVVLO0FBQ2xCK0IsRUFBQUEsV0FBVyxFQUFFO0FBREssQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCB7IE1hdHJpeENsaWVudCB9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7IGFjY2Vzc1NlY3JldFN0b3JhZ2UgfSBmcm9tICcuLi8uLi8uLi8uLi9Dcm9zc1NpZ25pbmdNYW5hZ2VyJztcclxuXHJcbmNvbnN0IFJFU1RPUkVfVFlQRV9QQVNTUEhSQVNFID0gMDtcclxuY29uc3QgUkVTVE9SRV9UWVBFX1JFQ09WRVJZS0VZID0gMTtcclxuY29uc3QgUkVTVE9SRV9UWVBFX1NFQ1JFVF9TVE9SQUdFID0gMjtcclxuXHJcbi8qXHJcbiAqIERpYWxvZyBmb3IgcmVzdG9yaW5nIGUyZSBrZXlzIGZyb20gYSBiYWNrdXAgYW5kIHRoZSB1c2VyJ3MgcmVjb3Zlcnkga2V5XHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXN0b3JlS2V5QmFja3VwRGlhbG9nIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIGlmIGZhbHNlLCB3aWxsIGNsb3NlIHRoZSBkaWFsb2cgYXMgc29vbiBhcyB0aGUgcmVzdG9yZSBjb21wbGV0ZXMgc3VjY2VzZnVsbHlcclxuICAgICAgICAvLyBkZWZhdWx0OiB0cnVlXHJcbiAgICAgICAgc2hvd1N1bW1hcnk6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIC8vIElmIHNwZWNpZmllZCwgZ2F0aGVyIHRoZSBrZXkgZnJvbSB0aGUgdXNlciBidXQgdGhlbiBjYWxsIHRoZSBmdW5jdGlvbiB3aXRoIHRoZSBiYWNrdXBcclxuICAgICAgICAvLyBrZXkgcmF0aGVyIHRoYW4gYWN0dWFsbHkgKG5lY2Vzc2FyaWx5KSByZXN0b3JpbmcgdGhlIGJhY2t1cC5cclxuICAgICAgICBrZXlDYWxsYmFjazogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XHJcbiAgICAgICAgc2hvd1N1bW1hcnk6IHRydWUsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGJhY2t1cEluZm86IG51bGwsXHJcbiAgICAgICAgICAgIGJhY2t1cEtleVN0b3JlZDogbnVsbCxcclxuICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIGxvYWRFcnJvcjogbnVsbCxcclxuICAgICAgICAgICAgcmVzdG9yZUVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICByZWNvdmVyeUtleTogXCJcIixcclxuICAgICAgICAgICAgcmVjb3ZlckluZm86IG51bGwsXHJcbiAgICAgICAgICAgIHJlY292ZXJ5S2V5VmFsaWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBmb3JjZVJlY292ZXJ5S2V5OiBmYWxzZSxcclxuICAgICAgICAgICAgcGFzc1BocmFzZTogJycsXHJcbiAgICAgICAgICAgIHJlc3RvcmVUeXBlOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5fbG9hZEJhY2t1cFN0YXR1cygpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNhbmNlbCA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkRvbmUgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblVzZVJlY292ZXJ5S2V5Q2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGZvcmNlUmVjb3ZlcnlLZXk6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVzZXRSZWNvdmVyeUNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZChmYWxzZSk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jKCdLZXkgQmFja3VwJywgJ0tleSBCYWNrdXAnLFxyXG4gICAgICAgICAgICBpbXBvcnQoJy4uLy4uLy4uLy4uL2FzeW5jLWNvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9rZXliYWNrdXAvQ3JlYXRlS2V5QmFja3VwRGlhbG9nJyksXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9sb2FkQmFja3VwU3RhdHVzKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9LCBudWxsLCAvKiBwcmlvcml0eSA9ICovIGZhbHNlLCAvKiBzdGF0aWMgPSAqLyB0cnVlLFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVjb3ZlcnlLZXlDaGFuZ2UgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICByZWNvdmVyeUtleTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgICAgIHJlY292ZXJ5S2V5VmFsaWQ6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc1ZhbGlkUmVjb3ZlcnlLZXkoZS50YXJnZXQudmFsdWUpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblBhc3NQaHJhc2VOZXh0ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBsb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICByZXN0b3JlRXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RvcmVUeXBlOiBSRVNUT1JFX1RZUEVfUEFTU1BIUkFTRSxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAvLyBXZSBkbyBzdGlsbCByZXN0b3JlIHRoZSBrZXkgYmFja3VwOiB3ZSBtdXN0IGVuc3VyZSB0aGF0IHRoZSBrZXkgYmFja3VwIGtleVxyXG4gICAgICAgICAgICAvLyBpcyB0aGUgcmlnaHQgb25lIGFuZCByZXN0b3JpbmcgaXQgaXMgY3VycmVudGx5IHRoZSBvbmx5IHdheSB3ZSBjYW4gZG8gdGhpcy5cclxuICAgICAgICAgICAgY29uc3QgcmVjb3ZlckluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVzdG9yZUtleUJhY2t1cFdpdGhQYXNzd29yZChcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUucGFzc1BocmFzZSwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHRoaXMuc3RhdGUuYmFja3VwSW5mbyxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMua2V5Q2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGtleSA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5rZXlCYWNrdXBLZXlGcm9tUGFzc3dvcmQoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5wYXNzUGhyYXNlLCB0aGlzLnN0YXRlLmJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5rZXlDYWxsYmFjayhrZXkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoIXRoaXMucHJvcHMuc2hvd1N1bW1hcnkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcmVjb3ZlckluZm8sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciByZXN0b3JpbmcgYmFja3VwXCIsIGUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcmVzdG9yZUVycm9yOiBlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVjb3ZlcnlLZXlOZXh0ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5yZWNvdmVyeUtleVZhbGlkKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBsb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICByZXN0b3JlRXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RvcmVUeXBlOiBSRVNUT1JFX1RZUEVfUkVDT1ZFUllLRVksXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgcmVjb3ZlckluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVzdG9yZUtleUJhY2t1cFdpdGhSZWNvdmVyeUtleShcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUucmVjb3ZlcnlLZXksIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCB0aGlzLnN0YXRlLmJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmtleUNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBrZXkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkua2V5QmFja3VwS2V5RnJvbVJlY292ZXJ5S2V5KHRoaXMuc3RhdGUucmVjb3ZlcnlLZXkpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5rZXlDYWxsYmFjayhrZXkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5wcm9wcy5zaG93U3VtbWFyeSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByZWNvdmVySW5mbyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yIHJlc3RvcmluZyBiYWNrdXBcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByZXN0b3JlRXJyb3I6IGUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25QYXNzUGhyYXNlQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGFzc1BocmFzZTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3Jlc3RvcmVXaXRoU2VjcmV0U3RvcmFnZSgpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgcmVzdG9yZUVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0b3JlVHlwZTogUkVTVE9SRV9UWVBFX1NFQ1JFVF9TVE9SQUdFLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIC8vIGBhY2Nlc3NTZWNyZXRTdG9yYWdlYCBtYXkgcHJvbXB0IGZvciBzdG9yYWdlIGFjY2VzcyBhcyBuZWVkZWQuXHJcbiAgICAgICAgICAgIGNvbnN0IHJlY292ZXJJbmZvID0gYXdhaXQgYWNjZXNzU2VjcmV0U3RvcmFnZShhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlc3RvcmVLZXlCYWNrdXBXaXRoU2VjcmV0U3RvcmFnZShcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHJlY292ZXJJbmZvLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3IgcmVzdG9yaW5nIGJhY2t1cFwiLCBlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICByZXN0b3JlRXJyb3I6IGUsXHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9yZXN0b3JlV2l0aENhY2hlZEtleShiYWNrdXBJbmZvKSB7XHJcbiAgICAgICAgaWYgKCFiYWNrdXBJbmZvKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgcmVjb3ZlckluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVzdG9yZUtleUJhY2t1cFdpdGhDYWNoZShcclxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZCwgLyogdGFyZ2V0Um9vbUlkICovXHJcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQsIC8qIHRhcmdldFNlc3Npb25JZCAqL1xyXG4gICAgICAgICAgICAgICAgYmFja3VwSW5mbyxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICByZWNvdmVySW5mbyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJyZXN0b3JlV2l0aENhY2hlZEtleSBmYWlsZWQ6XCIsIGUpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9sb2FkQmFja3VwU3RhdHVzKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBsb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICBsb2FkRXJyb3I6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgYmFja3VwSW5mbyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRLZXlCYWNrdXBWZXJzaW9uKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhY2t1cEtleVN0b3JlZCA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0tleUJhY2t1cEtleVN0b3JlZCgpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgICAgICBiYWNrdXBLZXlTdG9yZWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZ290Q2FjaGUgPSBhd2FpdCB0aGlzLl9yZXN0b3JlV2l0aENhY2hlZEtleShiYWNrdXBJbmZvKTtcclxuICAgICAgICAgICAgaWYgKGdvdENhY2hlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlJlc3RvcmVLZXlCYWNrdXBEaWFsb2c6IGZvdW5kIGNhY2hlZCBiYWNrdXAga2V5XCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gSWYgdGhlIGJhY2t1cCBrZXkgaXMgc3RvcmVkLCB3ZSBjYW4gcHJvY2VlZCBkaXJlY3RseSB0byByZXN0b3JlLlxyXG4gICAgICAgICAgICBpZiAoYmFja3VwS2V5U3RvcmVkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5fcmVzdG9yZVdpdGhTZWNyZXRTdG9yYWdlKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZEVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBsb2FkaW5nIGJhY2t1cCBzdGF0dXNcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZEVycm9yOiBlLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuXHJcbiAgICAgICAgY29uc3QgYmFja3VwSGFzUGFzc3BocmFzZSA9IChcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5iYWNrdXBJbmZvICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYmFja3VwSW5mby5hdXRoX2RhdGEgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5iYWNrdXBJbmZvLmF1dGhfZGF0YS5wcml2YXRlX2tleV9zYWx0ICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYmFja3VwSW5mby5hdXRoX2RhdGEucHJpdmF0ZV9rZXlfaXRlcmF0aW9uc1xyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCBjb250ZW50O1xyXG4gICAgICAgIGxldCB0aXRsZTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5sb2FkaW5nKSB7XHJcbiAgICAgICAgICAgIHRpdGxlID0gX3QoXCJMb2FkaW5nLi4uXCIpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmxvYWRFcnJvcikge1xyXG4gICAgICAgICAgICB0aXRsZSA9IF90KFwiRXJyb3JcIik7XHJcbiAgICAgICAgICAgIGNvbnRlbnQgPSBfdChcIlVuYWJsZSB0byBsb2FkIGJhY2t1cCBzdGF0dXNcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLnJlc3RvcmVFcnJvcikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXN0b3JlRXJyb3IuZXJyY29kZSA9PT0gTWF0cml4Q2xpZW50LlJFU1RPUkVfQkFDS1VQX0VSUk9SX0JBRF9LRVkpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc3RvcmVUeXBlID09PSBSRVNUT1JFX1RZUEVfUkVDT1ZFUllLRVkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiUmVjb3Zlcnkga2V5IG1pc21hdGNoXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkJhY2t1cCBjb3VsZCBub3QgYmUgZGVjcnlwdGVkIHdpdGggdGhpcyBrZXk6IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicGxlYXNlIHZlcmlmeSB0aGF0IHlvdSBlbnRlcmVkIHRoZSBjb3JyZWN0IHJlY292ZXJ5IGtleS5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiSW5jb3JyZWN0IHJlY292ZXJ5IHBhc3NwaHJhc2VcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGNvdWxkIG5vdCBiZSBkZWNyeXB0ZWQgd2l0aCB0aGlzIHBhc3NwaHJhc2U6IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwicGxlYXNlIHZlcmlmeSB0aGF0IHlvdSBlbnRlcmVkIHRoZSBjb3JyZWN0IHJlY292ZXJ5IHBhc3NwaHJhc2UuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXCJFcnJvclwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQgPSBfdChcIlVuYWJsZSB0byByZXN0b3JlIGJhY2t1cFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5iYWNrdXBJbmZvID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHRpdGxlID0gX3QoXCJFcnJvclwiKTtcclxuICAgICAgICAgICAgY29udGVudCA9IF90KFwiTm8gYmFja3VwIGZvdW5kIVwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUucmVjb3ZlckluZm8pIHtcclxuICAgICAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICAgICAgdGl0bGUgPSBfdChcIkJhY2t1cCByZXN0b3JlZFwiKTtcclxuICAgICAgICAgICAgbGV0IGZhaWxlZFRvRGVjcnlwdDtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVjb3ZlckluZm8udG90YWwgPiB0aGlzLnN0YXRlLnJlY292ZXJJbmZvLmltcG9ydGVkKSB7XHJcbiAgICAgICAgICAgICAgICBmYWlsZWRUb0RlY3J5cHQgPSA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJGYWlsZWQgdG8gZGVjcnlwdCAlKGZhaWxlZENvdW50KXMgc2Vzc2lvbnMhXCIsXHJcbiAgICAgICAgICAgICAgICAgICAge2ZhaWxlZENvdW50OiB0aGlzLnN0YXRlLnJlY292ZXJJbmZvLnRvdGFsIC0gdGhpcy5zdGF0ZS5yZWNvdmVySW5mby5pbXBvcnRlZH0sXHJcbiAgICAgICAgICAgICAgICApfTwvcD47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29udGVudCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXCJSZXN0b3JlZCAlKHNlc3Npb25Db3VudClzIHNlc3Npb24ga2V5c1wiLCB7c2Vzc2lvbkNvdW50OiB0aGlzLnN0YXRlLnJlY292ZXJJbmZvLmltcG9ydGVkfSl9PC9wPlxyXG4gICAgICAgICAgICAgICAge2ZhaWxlZFRvRGVjcnlwdH1cclxuICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e190KCdPSycpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vbkRvbmV9XHJcbiAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICBmb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2UgaWYgKGJhY2t1cEhhc1Bhc3NwaHJhc2UgJiYgIXRoaXMuc3RhdGUuZm9yY2VSZWNvdmVyeUtleSkge1xyXG4gICAgICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgICAgICB0aXRsZSA9IF90KFwiRW50ZXIgcmVjb3ZlcnkgcGFzc3BocmFzZVwiKTtcclxuICAgICAgICAgICAgY29udGVudCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8Yj5XYXJuaW5nPC9iPjogeW91IHNob3VsZCBvbmx5IHNldCB1cCBrZXkgYmFja3VwIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImZyb20gYSB0cnVzdGVkIGNvbXB1dGVyLlwiLCB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7IGI6IHN1YiA9PiA8Yj57c3VifTwvYj4gfSxcclxuICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiQWNjZXNzIHlvdXIgc2VjdXJlIG1lc3NhZ2UgaGlzdG9yeSBhbmQgc2V0IHVwIHNlY3VyZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJtZXNzYWdpbmcgYnkgZW50ZXJpbmcgeW91ciByZWNvdmVyeSBwYXNzcGhyYXNlLlwiLFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcblxyXG4gICAgICAgICAgICAgICAgPGZvcm0gY2xhc3NOYW1lPVwibXhfUmVzdG9yZUtleUJhY2t1cERpYWxvZ19wcmltYXJ5Q29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2dfcGFzc1BocmFzZUlucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uUGFzc1BocmFzZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc1BocmFzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoJ05leHQnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uUGFzc1BocmFzZU5leHR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnlJc1N1Ym1pdD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5fb25DYW5jZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvY3VzPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAge190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiSWYgeW91J3ZlIGZvcmdvdHRlbiB5b3VyIHJlY292ZXJ5IHBhc3NwaHJhc2UgeW91IGNhbiBcIitcclxuICAgICAgICAgICAgICAgICAgICBcIjxidXR0b24xPnVzZSB5b3VyIHJlY292ZXJ5IGtleTwvYnV0dG9uMT4gb3IgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGJ1dHRvbjI+c2V0IHVwIG5ldyByZWNvdmVyeSBvcHRpb25zPC9idXR0b24yPlwiXHJcbiAgICAgICAgICAgICAgICAsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uMTogcyA9PiA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9saW5rQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudD1cInNwYW5cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblVzZVJlY292ZXJ5S2V5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c31cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbjI6IHMgPT4gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfbGlua0J1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQ9XCJzcGFuXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25SZXNldFJlY292ZXJ5Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c31cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+LFxyXG4gICAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aXRsZSA9IF90KFwiRW50ZXIgcmVjb3Zlcnkga2V5XCIpO1xyXG4gICAgICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGtleVN0YXR1cztcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVjb3ZlcnlLZXkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2dfa2V5U3RhdHVzXCI+PC9kaXY+O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUucmVjb3ZlcnlLZXlWYWxpZCkge1xyXG4gICAgICAgICAgICAgICAga2V5U3RhdHVzID0gPGRpdiBjbGFzc05hbWU9XCJteF9SZXN0b3JlS2V5QmFja3VwRGlhbG9nX2tleVN0YXR1c1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIlxcdUQ4M0RcXHVEQzREIFwifXtfdChcIlRoaXMgbG9va3MgbGlrZSBhIHZhbGlkIHJlY292ZXJ5IGtleSFcIil9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2dfa2V5U3RhdHVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge1wiXFx1RDgzRFxcdURDNEUgXCJ9e190KFwiTm90IGEgdmFsaWQgcmVjb3Zlcnkga2V5XCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb250ZW50ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICBcIjxiPldhcm5pbmc8L2I+OiBZb3Ugc2hvdWxkIG9ubHkgc2V0IHVwIGtleSBiYWNrdXAgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZnJvbSBhIHRydXN0ZWQgY29tcHV0ZXIuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgYjogc3ViID0+IDxiPntzdWJ9PC9iPiB9LFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJBY2Nlc3MgeW91ciBzZWN1cmUgbWVzc2FnZSBoaXN0b3J5IGFuZCBzZXQgdXAgc2VjdXJlIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIm1lc3NhZ2luZyBieSBlbnRlcmluZyB5b3VyIHJlY292ZXJ5IGtleS5cIixcclxuICAgICAgICAgICAgICAgICl9PC9wPlxyXG5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUmVzdG9yZUtleUJhY2t1cERpYWxvZ19wcmltYXJ5Q29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cIm14X1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2dfcmVjb3ZlcnlLZXlJbnB1dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblJlY292ZXJ5S2V5Q2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5yZWNvdmVyeUtleX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAge2tleVN0YXR1c31cclxuICAgICAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdCgnTmV4dCcpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5fb25SZWNvdmVyeUtleU5leHR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMuX29uQ2FuY2VsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb2N1cz17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnlEaXNhYmxlZD17IXRoaXMuc3RhdGUucmVjb3ZlcnlLZXlWYWxpZH1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJJZiB5b3UndmUgZm9yZ290dGVuIHlvdXIgcmVjb3Zlcnkga2V5IHlvdSBjYW4gXCIrXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8YnV0dG9uPnNldCB1cCBuZXcgcmVjb3Zlcnkgb3B0aW9uczwvYnV0dG9uPlwiXHJcbiAgICAgICAgICAgICAgICAsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uOiBzID0+IDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X2xpbmtCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50PVwic3BhblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uUmVzZXRSZWNvdmVyeUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3N9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPixcclxuICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZURpYWxvZyBjbGFzc05hbWU9J214X1Jlc3RvcmVLZXlCYWNrdXBEaWFsb2cnXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGl0bGV9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIHtjb250ZW50fVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19