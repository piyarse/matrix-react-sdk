"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _matrixJsSdk = require("matrix-js-sdk");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _Markdown = _interopRequireDefault(require("../../../Markdown"));

/*
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * A dialog for reporting an event.
 */
class ReportEventDialog extends _react.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onReasonChange", ({
      target: {
        value: reason
      }
    }) => {
      this.setState({
        reason
      });
    });
    (0, _defineProperty2.default)(this, "_onCancel", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onSubmit", async () => {
      if (!this.state.reason || !this.state.reason.trim()) {
        this.setState({
          err: (0, _languageHandler._t)("Please fill why you're reporting.")
        });
        return;
      }

      this.setState({
        busy: true,
        err: null
      });

      try {
        const ev = this.props.mxEvent;
        await _MatrixClientPeg.MatrixClientPeg.get().reportEvent(ev.getRoomId(), ev.getId(), -100, this.state.reason.trim());
        this.props.onFinished(true);
      } catch (e) {
        this.setState({
          busy: false,
          err: e.message
        });
      }
    });
    this.state = {
      reason: "",
      busy: false,
      err: null
    };
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const Loader = sdk.getComponent('elements.Spinner');
    const Field = sdk.getComponent('elements.Field');
    let error = null;

    if (this.state.err) {
      error = _react.default.createElement("div", {
        className: "error"
      }, this.state.err);
    }

    let progress = null;

    if (this.state.busy) {
      progress = _react.default.createElement("div", {
        className: "progress"
      }, _react.default.createElement(Loader, null));
    }

    const adminMessageMD = _SdkConfig.default.get().reportEvent && _SdkConfig.default.get().reportEvent.adminMessageMD;

    let adminMessage;

    if (adminMessageMD) {
      const html = new _Markdown.default(adminMessageMD).toHTML({
        externalLinks: true
      });
      adminMessage = _react.default.createElement("p", {
        dangerouslySetInnerHTML: {
          __html: html
        }
      });
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_BugReportDialog",
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)('Report Content to Your Homeserver Administrator'),
      contentId: "mx_ReportEventDialog"
    }, _react.default.createElement("div", {
      className: "mx_ReportEventDialog",
      id: "mx_ReportEventDialog"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Reporting this message will send its unique 'event ID' to the administrator of " + "your homeserver. If messages in this room are encrypted, your homeserver " + "administrator will not be able to read the message text or view any files or images.")), adminMessage, _react.default.createElement(Field, {
      className: "mx_ReportEventDialog_reason",
      element: "textarea",
      label: (0, _languageHandler._t)("Reason"),
      rows: 5,
      onChange: this._onReasonChange,
      value: this.state.reason,
      disabled: this.state.busy
    }), progress, error), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Send report"),
      onPrimaryButtonClick: this._onSubmit,
      focus: true,
      onCancel: this._onCancel,
      disabled: this.state.busy
    }));
  }

}

exports.default = ReportEventDialog;
(0, _defineProperty2.default)(ReportEventDialog, "propTypes", {
  mxEvent: _propTypes.default.instanceOf(_matrixJsSdk.MatrixEvent).isRequired,
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvUmVwb3J0RXZlbnREaWFsb2cuanMiXSwibmFtZXMiOlsiUmVwb3J0RXZlbnREaWFsb2ciLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInRhcmdldCIsInZhbHVlIiwicmVhc29uIiwic2V0U3RhdGUiLCJvbkZpbmlzaGVkIiwic3RhdGUiLCJ0cmltIiwiZXJyIiwiYnVzeSIsImV2IiwibXhFdmVudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInJlcG9ydEV2ZW50IiwiZ2V0Um9vbUlkIiwiZ2V0SWQiLCJlIiwibWVzc2FnZSIsInJlbmRlciIsIkJhc2VEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJEaWFsb2dCdXR0b25zIiwiTG9hZGVyIiwiRmllbGQiLCJlcnJvciIsInByb2dyZXNzIiwiYWRtaW5NZXNzYWdlTUQiLCJTZGtDb25maWciLCJhZG1pbk1lc3NhZ2UiLCJodG1sIiwiTWFya2Rvd24iLCJ0b0hUTUwiLCJleHRlcm5hbExpbmtzIiwiX19odG1sIiwiX29uUmVhc29uQ2hhbmdlIiwiX29uU3VibWl0IiwiX29uQ2FuY2VsIiwiUHJvcFR5cGVzIiwiaW5zdGFuY2VPZiIsIk1hdHJpeEV2ZW50IiwiaXNSZXF1aXJlZCIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBOzs7QUFHZSxNQUFNQSxpQkFBTixTQUFnQ0Msb0JBQWhDLENBQThDO0FBTXpEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSwyREFVRCxDQUFDO0FBQUNDLE1BQUFBLE1BQU0sRUFBRTtBQUFDQyxRQUFBQSxLQUFLLEVBQUVDO0FBQVI7QUFBVCxLQUFELEtBQStCO0FBQzdDLFdBQUtDLFFBQUwsQ0FBYztBQUFFRCxRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQVprQjtBQUFBLHFEQWNQLE1BQU07QUFDZCxXQUFLSCxLQUFMLENBQVdLLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQWhCa0I7QUFBQSxxREFrQlAsWUFBWTtBQUNwQixVQUFJLENBQUMsS0FBS0MsS0FBTCxDQUFXSCxNQUFaLElBQXNCLENBQUMsS0FBS0csS0FBTCxDQUFXSCxNQUFYLENBQWtCSSxJQUFsQixFQUEzQixFQUFxRDtBQUNqRCxhQUFLSCxRQUFMLENBQWM7QUFDVkksVUFBQUEsR0FBRyxFQUFFLHlCQUFHLG1DQUFIO0FBREssU0FBZDtBQUdBO0FBQ0g7O0FBRUQsV0FBS0osUUFBTCxDQUFjO0FBQ1ZLLFFBQUFBLElBQUksRUFBRSxJQURJO0FBRVZELFFBQUFBLEdBQUcsRUFBRTtBQUZLLE9BQWQ7O0FBS0EsVUFBSTtBQUNBLGNBQU1FLEVBQUUsR0FBRyxLQUFLVixLQUFMLENBQVdXLE9BQXRCO0FBQ0EsY0FBTUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsV0FBdEIsQ0FBa0NKLEVBQUUsQ0FBQ0ssU0FBSCxFQUFsQyxFQUFrREwsRUFBRSxDQUFDTSxLQUFILEVBQWxELEVBQThELENBQUMsR0FBL0QsRUFBb0UsS0FBS1YsS0FBTCxDQUFXSCxNQUFYLENBQWtCSSxJQUFsQixFQUFwRSxDQUFOO0FBQ0EsYUFBS1AsS0FBTCxDQUFXSyxVQUFYLENBQXNCLElBQXRCO0FBQ0gsT0FKRCxDQUlFLE9BQU9ZLENBQVAsRUFBVTtBQUNSLGFBQUtiLFFBQUwsQ0FBYztBQUNWSyxVQUFBQSxJQUFJLEVBQUUsS0FESTtBQUVWRCxVQUFBQSxHQUFHLEVBQUVTLENBQUMsQ0FBQ0M7QUFGRyxTQUFkO0FBSUg7QUFDSixLQXpDa0I7QUFHZixTQUFLWixLQUFMLEdBQWE7QUFDVEgsTUFBQUEsTUFBTSxFQUFFLEVBREM7QUFFVE0sTUFBQUEsSUFBSSxFQUFFLEtBRkc7QUFHVEQsTUFBQUEsR0FBRyxFQUFFO0FBSEksS0FBYjtBQUtIOztBQW1DRFcsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsVUFBTUUsTUFBTSxHQUFHSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWY7QUFDQSxVQUFNRyxLQUFLLEdBQUdKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBZDtBQUVBLFFBQUlJLEtBQUssR0FBRyxJQUFaOztBQUNBLFFBQUksS0FBS3BCLEtBQUwsQ0FBV0UsR0FBZixFQUFvQjtBQUNoQmtCLE1BQUFBLEtBQUssR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSCxLQUFLcEIsS0FBTCxDQUFXRSxHQURSLENBQVI7QUFHSDs7QUFFRCxRQUFJbUIsUUFBUSxHQUFHLElBQWY7O0FBQ0EsUUFBSSxLQUFLckIsS0FBTCxDQUFXRyxJQUFmLEVBQXFCO0FBQ2pCa0IsTUFBQUEsUUFBUSxHQUNKO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLDZCQUFDLE1BQUQsT0FESixDQURKO0FBS0g7O0FBRUQsVUFBTUMsY0FBYyxHQUNoQkMsbUJBQVVoQixHQUFWLEdBQWdCQyxXQUFoQixJQUNBZSxtQkFBVWhCLEdBQVYsR0FBZ0JDLFdBQWhCLENBQTRCYyxjQUZoQzs7QUFHQSxRQUFJRSxZQUFKOztBQUNBLFFBQUlGLGNBQUosRUFBb0I7QUFDaEIsWUFBTUcsSUFBSSxHQUFHLElBQUlDLGlCQUFKLENBQWFKLGNBQWIsRUFBNkJLLE1BQTdCLENBQW9DO0FBQUVDLFFBQUFBLGFBQWEsRUFBRTtBQUFqQixPQUFwQyxDQUFiO0FBQ0FKLE1BQUFBLFlBQVksR0FBRztBQUFHLFFBQUEsdUJBQXVCLEVBQUU7QUFBRUssVUFBQUEsTUFBTSxFQUFFSjtBQUFWO0FBQTVCLFFBQWY7QUFDSDs7QUFFRCxXQUNJLDZCQUFDLFVBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBQyxvQkFEZDtBQUVJLE1BQUEsVUFBVSxFQUFFLEtBQUsvQixLQUFMLENBQVdLLFVBRjNCO0FBR0ksTUFBQSxLQUFLLEVBQUUseUJBQUcsaURBQUgsQ0FIWDtBQUlJLE1BQUEsU0FBUyxFQUFDO0FBSmQsT0FNSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHNCQUFmO0FBQXNDLE1BQUEsRUFBRSxFQUFDO0FBQXpDLE9BQ0ksd0NBRVEseUJBQUcsb0ZBQ0MsMkVBREQsR0FFQyxzRkFGSixDQUZSLENBREosRUFRS3lCLFlBUkwsRUFTSSw2QkFBQyxLQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsNkJBRGQ7QUFFSSxNQUFBLE9BQU8sRUFBQyxVQUZaO0FBR0ksTUFBQSxLQUFLLEVBQUUseUJBQUcsUUFBSCxDQUhYO0FBSUksTUFBQSxJQUFJLEVBQUUsQ0FKVjtBQUtJLE1BQUEsUUFBUSxFQUFFLEtBQUtNLGVBTG5CO0FBTUksTUFBQSxLQUFLLEVBQUUsS0FBSzlCLEtBQUwsQ0FBV0gsTUFOdEI7QUFPSSxNQUFBLFFBQVEsRUFBRSxLQUFLRyxLQUFMLENBQVdHO0FBUHpCLE1BVEosRUFrQktrQixRQWxCTCxFQW1CS0QsS0FuQkwsQ0FOSixFQTJCSSw2QkFBQyxhQUFEO0FBQ0ksTUFBQSxhQUFhLEVBQUUseUJBQUcsYUFBSCxDQURuQjtBQUVJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS1csU0FGL0I7QUFHSSxNQUFBLEtBQUssRUFBRSxJQUhYO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS0MsU0FKbkI7QUFLSSxNQUFBLFFBQVEsRUFBRSxLQUFLaEMsS0FBTCxDQUFXRztBQUx6QixNQTNCSixDQURKO0FBcUNIOztBQXJId0Q7Ozs4QkFBeENaLGlCLGVBQ0U7QUFDZmMsRUFBQUEsT0FBTyxFQUFFNEIsbUJBQVVDLFVBQVYsQ0FBcUJDLHdCQUFyQixFQUFrQ0MsVUFENUI7QUFFZnJDLEVBQUFBLFVBQVUsRUFBRWtDLG1CQUFVSSxJQUFWLENBQWVEO0FBRlosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCwge1B1cmVDb21wb25lbnR9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XHJcbmltcG9ydCB7TWF0cml4RXZlbnR9IGZyb20gXCJtYXRyaXgtanMtc2RrXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSAnLi4vLi4vLi4vU2RrQ29uZmlnJztcclxuaW1wb3J0IE1hcmtkb3duIGZyb20gJy4uLy4uLy4uL01hcmtkb3duJztcclxuXHJcbi8qXHJcbiAqIEEgZGlhbG9nIGZvciByZXBvcnRpbmcgYW4gZXZlbnQuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXBvcnRFdmVudERpYWxvZyBleHRlbmRzIFB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMuaW5zdGFuY2VPZihNYXRyaXhFdmVudCkuaXNSZXF1aXJlZCxcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcmVhc29uOiBcIlwiLFxyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgZXJyOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVhc29uQ2hhbmdlID0gKHt0YXJnZXQ6IHt2YWx1ZTogcmVhc29ufX0pID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVhc29uIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25DYW5jZWwgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uU3VibWl0ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5yZWFzb24gfHwgIXRoaXMuc3RhdGUucmVhc29uLnRyaW0oKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGVycjogX3QoXCJQbGVhc2UgZmlsbCB3aHkgeW91J3JlIHJlcG9ydGluZy5cIiksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogdHJ1ZSxcclxuICAgICAgICAgICAgZXJyOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IHRoaXMucHJvcHMubXhFdmVudDtcclxuICAgICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlcG9ydEV2ZW50KGV2LmdldFJvb21JZCgpLCBldi5nZXRJZCgpLCAtMTAwLCB0aGlzLnN0YXRlLnJlYXNvbi50cmltKCkpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodHJ1ZSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZXJyOiBlLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlNwaW5uZXInKTtcclxuICAgICAgICBjb25zdCBGaWVsZCA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkZpZWxkJyk7XHJcblxyXG4gICAgICAgIGxldCBlcnJvciA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gPGRpdiBjbGFzc05hbWU9XCJlcnJvclwiPlxyXG4gICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuZXJyfVxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcHJvZ3Jlc3MgPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmJ1c3kpIHtcclxuICAgICAgICAgICAgcHJvZ3Jlc3MgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2dyZXNzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPExvYWRlciAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBhZG1pbk1lc3NhZ2VNRCA9XHJcbiAgICAgICAgICAgIFNka0NvbmZpZy5nZXQoKS5yZXBvcnRFdmVudCAmJlxyXG4gICAgICAgICAgICBTZGtDb25maWcuZ2V0KCkucmVwb3J0RXZlbnQuYWRtaW5NZXNzYWdlTUQ7XHJcbiAgICAgICAgbGV0IGFkbWluTWVzc2FnZTtcclxuICAgICAgICBpZiAoYWRtaW5NZXNzYWdlTUQpIHtcclxuICAgICAgICAgICAgY29uc3QgaHRtbCA9IG5ldyBNYXJrZG93bihhZG1pbk1lc3NhZ2VNRCkudG9IVE1MKHsgZXh0ZXJuYWxMaW5rczogdHJ1ZSB9KTtcclxuICAgICAgICAgICAgYWRtaW5NZXNzYWdlID0gPHAgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBodG1sIH19IC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2dcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0J1Z1JlcG9ydERpYWxvZ1wiXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17X3QoJ1JlcG9ydCBDb250ZW50IHRvIFlvdXIgSG9tZXNlcnZlciBBZG1pbmlzdHJhdG9yJyl9XHJcbiAgICAgICAgICAgICAgICBjb250ZW50SWQ9J214X1JlcG9ydEV2ZW50RGlhbG9nJ1xyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1JlcG9ydEV2ZW50RGlhbG9nXCIgaWQ9XCJteF9SZXBvcnRFdmVudERpYWxvZ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdChcIlJlcG9ydGluZyB0aGlzIG1lc3NhZ2Ugd2lsbCBzZW5kIGl0cyB1bmlxdWUgJ2V2ZW50IElEJyB0byB0aGUgYWRtaW5pc3RyYXRvciBvZiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ5b3VyIGhvbWVzZXJ2ZXIuIElmIG1lc3NhZ2VzIGluIHRoaXMgcm9vbSBhcmUgZW5jcnlwdGVkLCB5b3VyIGhvbWVzZXJ2ZXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYWRtaW5pc3RyYXRvciB3aWxsIG5vdCBiZSBhYmxlIHRvIHJlYWQgdGhlIG1lc3NhZ2UgdGV4dCBvciB2aWV3IGFueSBmaWxlcyBvciBpbWFnZXMuXCIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAge2FkbWluTWVzc2FnZX1cclxuICAgICAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUmVwb3J0RXZlbnREaWFsb2dfcmVhc29uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudD1cInRleHRhcmVhXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KFwiUmVhc29uXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3dzPXs1fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25SZWFzb25DaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnJlYXNvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUuYnVzeX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHtwcm9ncmVzc31cclxuICAgICAgICAgICAgICAgICAgICB7ZXJyb3J9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoXCJTZW5kIHJlcG9ydFwiKX1cclxuICAgICAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5fb25TdWJtaXR9XHJcbiAgICAgICAgICAgICAgICAgICAgZm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMuX29uQ2FuY2VsfVxyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmJ1c3l9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=