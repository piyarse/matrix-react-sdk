"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../Modal"));

/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const WarmFuzzy = function (props) {
  const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
  let title = (0, _languageHandler._t)('You have successfully set a password!');

  if (props.didSetEmail) {
    title = (0, _languageHandler._t)('You have successfully set a password and an email address!');
  }

  const advice = (0, _languageHandler._t)('You can now return to your account after signing out, and sign in on other devices.');
  let extraAdvice = null;

  if (!props.didSetEmail) {
    extraAdvice = (0, _languageHandler._t)('Remember, you can always set an email address in user settings if you change your mind.');
  }

  return _react.default.createElement(BaseDialog, {
    className: "mx_SetPasswordDialog",
    onFinished: props.onFinished,
    title: title
  }, _react.default.createElement("div", {
    className: "mx_Dialog_content"
  }, _react.default.createElement("p", null, advice), _react.default.createElement("p", null, extraAdvice)), _react.default.createElement("div", {
    className: "mx_Dialog_buttons"
  }, _react.default.createElement("button", {
    className: "mx_Dialog_primary",
    autoFocus: true,
    onClick: props.onFinished
  }, (0, _languageHandler._t)('Continue'))));
};
/**
 * Prompt the user to set a password
 *
 * On success, `onFinished()` when finished
 */


var _default = (0, _createReactClass.default)({
  displayName: 'SetPasswordDialog',
  propTypes: {
    onFinished: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    return {
      error: null
    };
  },
  componentDidMount: function () {
    console.info('SetPasswordDialog component did mount');
  },
  _onPasswordChanged: function (res) {
    _Modal.default.createDialog(WarmFuzzy, {
      didSetEmail: res.didSetEmail,
      onFinished: () => {
        this.props.onFinished();
      }
    });
  },
  _onPasswordChangeError: function (err) {
    let errMsg = err.error || "";

    if (err.httpStatus === 403) {
      errMsg = (0, _languageHandler._t)('Failed to change password. Is your password correct?');
    } else if (err.httpStatus) {
      errMsg += ' ' + (0, _languageHandler._t)('(HTTP status %(httpStatus)s)', {
        httpStatus: err.httpStatus
      });
    }

    this.setState({
      error: errMsg
    });
  },
  render: function () {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    const ChangePassword = sdk.getComponent('views.settings.ChangePassword');
    return _react.default.createElement(BaseDialog, {
      className: "mx_SetPasswordDialog",
      onFinished: this.props.onFinished,
      title: (0, _languageHandler._t)('Please set a password!')
    }, _react.default.createElement("div", {
      className: "mx_Dialog_content"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)('This will allow you to return to your account after signing out, and sign in on other sessions.')), _react.default.createElement(ChangePassword, {
      className: "mx_SetPasswordDialog_change_password",
      rowClassName: "",
      buttonClassNames: "mx_Dialog_primary mx_SetPasswordDialog_change_password_button",
      buttonKind: "primary",
      confirm: false,
      autoFocusNewPasswordInput: true,
      shouldAskForEmail: true,
      onError: this._onPasswordChangeError,
      onFinished: this._onPasswordChanged
    }), _react.default.createElement("div", {
      className: "error"
    }, this.state.error)));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3MvU2V0UGFzc3dvcmREaWFsb2cuanMiXSwibmFtZXMiOlsiV2FybUZ1enp5IiwicHJvcHMiLCJCYXNlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwidGl0bGUiLCJkaWRTZXRFbWFpbCIsImFkdmljZSIsImV4dHJhQWR2aWNlIiwib25GaW5pc2hlZCIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJnZXRJbml0aWFsU3RhdGUiLCJlcnJvciIsImNvbXBvbmVudERpZE1vdW50IiwiY29uc29sZSIsImluZm8iLCJfb25QYXNzd29yZENoYW5nZWQiLCJyZXMiLCJNb2RhbCIsImNyZWF0ZURpYWxvZyIsIl9vblBhc3N3b3JkQ2hhbmdlRXJyb3IiLCJlcnIiLCJlcnJNc2ciLCJodHRwU3RhdHVzIiwic2V0U3RhdGUiLCJyZW5kZXIiLCJDaGFuZ2VQYXNzd29yZCIsInN0YXRlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBLE1BQU1BLFNBQVMsR0FBRyxVQUFTQyxLQUFULEVBQWdCO0FBQzlCLFFBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLE1BQUlDLEtBQUssR0FBRyx5QkFBRyx1Q0FBSCxDQUFaOztBQUNBLE1BQUlKLEtBQUssQ0FBQ0ssV0FBVixFQUF1QjtBQUNuQkQsSUFBQUEsS0FBSyxHQUFHLHlCQUFHLDREQUFILENBQVI7QUFDSDs7QUFDRCxRQUFNRSxNQUFNLEdBQUcseUJBQUcscUZBQUgsQ0FBZjtBQUNBLE1BQUlDLFdBQVcsR0FBRyxJQUFsQjs7QUFDQSxNQUFJLENBQUNQLEtBQUssQ0FBQ0ssV0FBWCxFQUF3QjtBQUNwQkUsSUFBQUEsV0FBVyxHQUFHLHlCQUFHLHlGQUFILENBQWQ7QUFDSDs7QUFFRCxTQUFPLDZCQUFDLFVBQUQ7QUFBWSxJQUFBLFNBQVMsRUFBQyxzQkFBdEI7QUFDSCxJQUFBLFVBQVUsRUFBRVAsS0FBSyxDQUFDUSxVQURmO0FBRUgsSUFBQSxLQUFLLEVBQUdKO0FBRkwsS0FJSDtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSSx3Q0FDTUUsTUFETixDQURKLEVBSUksd0NBQ01DLFdBRE4sQ0FKSixDQUpHLEVBWUg7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0k7QUFDSSxJQUFBLFNBQVMsRUFBQyxtQkFEZDtBQUVJLElBQUEsU0FBUyxFQUFFLElBRmY7QUFHSSxJQUFBLE9BQU8sRUFBRVAsS0FBSyxDQUFDUTtBQUhuQixLQUlVLHlCQUFHLFVBQUgsQ0FKVixDQURKLENBWkcsQ0FBUDtBQXFCSCxDQWpDRDtBQW1DQTs7Ozs7OztlQUtlLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLG1CQURlO0FBRTVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEYsSUFBQUEsVUFBVSxFQUFFRyxtQkFBVUMsSUFBVixDQUFlQztBQURwQixHQUZpQjtBQU01QkMsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxLQUFLLEVBQUU7QUFESixLQUFQO0FBR0gsR0FWMkI7QUFZNUJDLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUJDLElBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLHVDQUFiO0FBQ0gsR0FkMkI7QUFnQjVCQyxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTQyxHQUFULEVBQWM7QUFDOUJDLG1CQUFNQyxZQUFOLENBQW1CdkIsU0FBbkIsRUFBOEI7QUFDMUJNLE1BQUFBLFdBQVcsRUFBRWUsR0FBRyxDQUFDZixXQURTO0FBRTFCRyxNQUFBQSxVQUFVLEVBQUUsTUFBTTtBQUNkLGFBQUtSLEtBQUwsQ0FBV1EsVUFBWDtBQUNIO0FBSnlCLEtBQTlCO0FBTUgsR0F2QjJCO0FBeUI1QmUsRUFBQUEsc0JBQXNCLEVBQUUsVUFBU0MsR0FBVCxFQUFjO0FBQ2xDLFFBQUlDLE1BQU0sR0FBR0QsR0FBRyxDQUFDVCxLQUFKLElBQWEsRUFBMUI7O0FBQ0EsUUFBSVMsR0FBRyxDQUFDRSxVQUFKLEtBQW1CLEdBQXZCLEVBQTRCO0FBQ3hCRCxNQUFBQSxNQUFNLEdBQUcseUJBQUcsc0RBQUgsQ0FBVDtBQUNILEtBRkQsTUFFTyxJQUFJRCxHQUFHLENBQUNFLFVBQVIsRUFBb0I7QUFDdkJELE1BQUFBLE1BQU0sSUFBSSxNQUFNLHlCQUNaLDhCQURZLEVBRVo7QUFBRUMsUUFBQUEsVUFBVSxFQUFFRixHQUFHLENBQUNFO0FBQWxCLE9BRlksQ0FBaEI7QUFJSDs7QUFDRCxTQUFLQyxRQUFMLENBQWM7QUFDVlosTUFBQUEsS0FBSyxFQUFFVTtBQURHLEtBQWQ7QUFHSCxHQXRDMkI7QUF3QzVCRyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU0zQixVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7QUFDQSxVQUFNMEIsY0FBYyxHQUFHM0IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUF2QjtBQUVBLFdBQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsU0FBUyxFQUFDLHNCQUF0QjtBQUNJLE1BQUEsVUFBVSxFQUFFLEtBQUtILEtBQUwsQ0FBV1EsVUFEM0I7QUFFSSxNQUFBLEtBQUssRUFBRyx5QkFBRyx3QkFBSDtBQUZaLE9BSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksd0NBQ00seUJBQUcsaUdBQUgsQ0FETixDQURKLEVBSUksNkJBQUMsY0FBRDtBQUNJLE1BQUEsU0FBUyxFQUFDLHNDQURkO0FBRUksTUFBQSxZQUFZLEVBQUMsRUFGakI7QUFHSSxNQUFBLGdCQUFnQixFQUFDLCtEQUhyQjtBQUlJLE1BQUEsVUFBVSxFQUFDLFNBSmY7QUFLSSxNQUFBLE9BQU8sRUFBRSxLQUxiO0FBTUksTUFBQSx5QkFBeUIsRUFBRSxJQU4vQjtBQU9JLE1BQUEsaUJBQWlCLEVBQUUsSUFQdkI7QUFRSSxNQUFBLE9BQU8sRUFBRSxLQUFLZSxzQkFSbEI7QUFTSSxNQUFBLFVBQVUsRUFBRSxLQUFLSjtBQVRyQixNQUpKLEVBY0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ00sS0FBS1csS0FBTCxDQUFXZixLQURqQixDQWRKLENBSkosQ0FESjtBQXlCSDtBQXJFMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuXHJcbmNvbnN0IFdhcm1GdXp6eSA9IGZ1bmN0aW9uKHByb3BzKSB7XHJcbiAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZGlhbG9ncy5CYXNlRGlhbG9nJyk7XHJcbiAgICBsZXQgdGl0bGUgPSBfdCgnWW91IGhhdmUgc3VjY2Vzc2Z1bGx5IHNldCBhIHBhc3N3b3JkIScpO1xyXG4gICAgaWYgKHByb3BzLmRpZFNldEVtYWlsKSB7XHJcbiAgICAgICAgdGl0bGUgPSBfdCgnWW91IGhhdmUgc3VjY2Vzc2Z1bGx5IHNldCBhIHBhc3N3b3JkIGFuZCBhbiBlbWFpbCBhZGRyZXNzIScpO1xyXG4gICAgfVxyXG4gICAgY29uc3QgYWR2aWNlID0gX3QoJ1lvdSBjYW4gbm93IHJldHVybiB0byB5b3VyIGFjY291bnQgYWZ0ZXIgc2lnbmluZyBvdXQsIGFuZCBzaWduIGluIG9uIG90aGVyIGRldmljZXMuJyk7XHJcbiAgICBsZXQgZXh0cmFBZHZpY2UgPSBudWxsO1xyXG4gICAgaWYgKCFwcm9wcy5kaWRTZXRFbWFpbCkge1xyXG4gICAgICAgIGV4dHJhQWR2aWNlID0gX3QoJ1JlbWVtYmVyLCB5b3UgY2FuIGFsd2F5cyBzZXQgYW4gZW1haWwgYWRkcmVzcyBpbiB1c2VyIHNldHRpbmdzIGlmIHlvdSBjaGFuZ2UgeW91ciBtaW5kLicpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiA8QmFzZURpYWxvZyBjbGFzc05hbWU9XCJteF9TZXRQYXNzd29yZERpYWxvZ1wiXHJcbiAgICAgICAgb25GaW5pc2hlZD17cHJvcHMub25GaW5pc2hlZH1cclxuICAgICAgICB0aXRsZT17IHRpdGxlIH1cclxuICAgID5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19jb250ZW50XCI+XHJcbiAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgeyBhZHZpY2UgfVxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgeyBleHRyYUFkdmljZSB9XHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19idXR0b25zXCI+XHJcbiAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0RpYWxvZ19wcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgIGF1dG9Gb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3Byb3BzLm9uRmluaXNoZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoJ0NvbnRpbnVlJykgfVxyXG4gICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvQmFzZURpYWxvZz47XHJcbn07XHJcblxyXG4vKipcclxuICogUHJvbXB0IHRoZSB1c2VyIHRvIHNldCBhIHBhc3N3b3JkXHJcbiAqXHJcbiAqIE9uIHN1Y2Nlc3MsIGBvbkZpbmlzaGVkKClgIHdoZW4gZmluaXNoZWRcclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdTZXRQYXNzd29yZERpYWxvZycsXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zb2xlLmluZm8oJ1NldFBhc3N3b3JkRGlhbG9nIGNvbXBvbmVudCBkaWQgbW91bnQnKTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uUGFzc3dvcmRDaGFuZ2VkOiBmdW5jdGlvbihyZXMpIHtcclxuICAgICAgICBNb2RhbC5jcmVhdGVEaWFsb2coV2FybUZ1enp5LCB7XHJcbiAgICAgICAgICAgIGRpZFNldEVtYWlsOiByZXMuZGlkU2V0RW1haWwsXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25QYXNzd29yZENoYW5nZUVycm9yOiBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICBsZXQgZXJyTXNnID0gZXJyLmVycm9yIHx8IFwiXCI7XHJcbiAgICAgICAgaWYgKGVyci5odHRwU3RhdHVzID09PSA0MDMpIHtcclxuICAgICAgICAgICAgZXJyTXNnID0gX3QoJ0ZhaWxlZCB0byBjaGFuZ2UgcGFzc3dvcmQuIElzIHlvdXIgcGFzc3dvcmQgY29ycmVjdD8nKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGVyci5odHRwU3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGVyck1zZyArPSAnICcgKyBfdChcclxuICAgICAgICAgICAgICAgICcoSFRUUCBzdGF0dXMgJShodHRwU3RhdHVzKXMpJyxcclxuICAgICAgICAgICAgICAgIHsgaHR0cFN0YXR1czogZXJyLmh0dHBTdGF0dXMgfSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGVycm9yOiBlcnJNc2csXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG4gICAgICAgIGNvbnN0IENoYW5nZVBhc3N3b3JkID0gc2RrLmdldENvbXBvbmVudCgndmlld3Muc2V0dGluZ3MuQ2hhbmdlUGFzc3dvcmQnKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPVwibXhfU2V0UGFzc3dvcmREaWFsb2dcIlxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5wcm9wcy5vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9eyBfdCgnUGxlYXNlIHNldCBhIHBhc3N3b3JkIScpIH1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9EaWFsb2dfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdUaGlzIHdpbGwgYWxsb3cgeW91IHRvIHJldHVybiB0byB5b3VyIGFjY291bnQgYWZ0ZXIgc2lnbmluZyBvdXQsIGFuZCBzaWduIGluIG9uIG90aGVyIHNlc3Npb25zLicpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPENoYW5nZVBhc3N3b3JkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1NldFBhc3N3b3JkRGlhbG9nX2NoYW5nZV9wYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvd0NsYXNzTmFtZT1cIlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJ1dHRvbkNsYXNzTmFtZXM9XCJteF9EaWFsb2dfcHJpbWFyeSBteF9TZXRQYXNzd29yZERpYWxvZ19jaGFuZ2VfcGFzc3dvcmRfYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnV0dG9uS2luZD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25maXJtPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzTmV3UGFzc3dvcmRJbnB1dD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvdWxkQXNrRm9yRW1haWw9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRXJyb3I9e3RoaXMuX29uUGFzc3dvcmRDaGFuZ2VFcnJvcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25GaW5pc2hlZD17dGhpcy5fb25QYXNzd29yZENoYW5nZWR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJlcnJvclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUuZXJyb3IgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==