"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _SettingsStore = _interopRequireWildcard(require("../../../settings/SettingsStore"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _notifications = require("../../../notifications");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _LabelledToggleSwitch = _interopRequireDefault(require("../elements/LabelledToggleSwitch"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

/*
Copyright 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// TODO: this "view" component still has far too much application logic in it,
// which should be factored out to other files.
// TODO: this component also does a lot of direct poking into this.state, which
// is VERY NAUGHTY.

/**
 * Rules that Vector used to set in order to override the actions of default rules.
 * These are used to port peoples existing overrides to match the current API.
 * These can be removed and forgotten once everyone has moved to the new client.
 */
const LEGACY_RULES = {
  "im.vector.rule.contains_display_name": ".m.rule.contains_display_name",
  "im.vector.rule.room_one_to_one": ".m.rule.room_one_to_one",
  "im.vector.rule.room_message": ".m.rule.message",
  "im.vector.rule.invite_for_me": ".m.rule.invite_for_me",
  "im.vector.rule.call": ".m.rule.call",
  "im.vector.rule.notices": ".m.rule.suppress_notices"
};

function portLegacyActions(actions) {
  const decoded = _notifications.NotificationUtils.decodeActions(actions);

  if (decoded !== null) {
    return _notifications.NotificationUtils.encodeActions(decoded);
  } else {
    // We don't recognise one of the actions here, so we don't try to
    // canonicalise them.
    return actions;
  }
}

var _default = (0, _createReactClass.default)({
  displayName: 'Notifications',
  phases: {
    LOADING: "LOADING",
    // The component is loading or sending data to the hs
    DISPLAY: "DISPLAY",
    // The component is ready and display data
    ERROR: "ERROR" // There was an error

  },
  getInitialState: function () {
    return {
      phase: this.phases.LOADING,
      masterPushRule: undefined,
      // The master rule ('.m.rule.master')
      vectorPushRules: [],
      // HS default push rules displayed in Vector UI
      vectorContentRules: {
        // Keyword push rules displayed in Vector UI
        vectorState: _notifications.PushRuleVectorState.ON,
        rules: []
      },
      externalPushRules: [],
      // Push rules (except content rule) that have been defined outside Vector UI
      externalContentRules: [],
      // Keyword push rules that have been defined outside Vector UI
      threepids: [] // used for email notifications

    };
  },
  componentDidMount: function () {
    this._refreshFromServer();
  },
  onEnableNotificationsChange: function (checked) {
    const self = this;
    this.setState({
      phase: this.phases.LOADING
    });

    _MatrixClientPeg.MatrixClientPeg.get().setPushRuleEnabled('global', self.state.masterPushRule.kind, self.state.masterPushRule.rule_id, !checked).then(function () {
      self._refreshFromServer();
    });
  },
  onEnableDesktopNotificationsChange: function (checked) {
    _SettingsStore.default.setValue("notificationsEnabled", null, _SettingsStore.SettingLevel.DEVICE, checked).finally(() => {
      this.forceUpdate();
    });
  },
  onEnableDesktopNotificationBodyChange: function (checked) {
    _SettingsStore.default.setValue("notificationBodyEnabled", null, _SettingsStore.SettingLevel.DEVICE, checked).finally(() => {
      this.forceUpdate();
    });
  },
  onEnableAudioNotificationsChange: function (checked) {
    _SettingsStore.default.setValue("audioNotificationsEnabled", null, _SettingsStore.SettingLevel.DEVICE, checked).finally(() => {
      this.forceUpdate();
    });
  },

  /*
   * Returns the email pusher (pusher of type 'email') for a given
   * email address. Email pushers all have the same app ID, so since
   * pushers are unique over (app ID, pushkey), there will be at most
   * one such pusher.
   */
  getEmailPusher: function (pushers, address) {
    if (pushers === undefined) {
      return undefined;
    }

    for (let i = 0; i < pushers.length; ++i) {
      if (pushers[i].kind === 'email' && pushers[i].pushkey === address) {
        return pushers[i];
      }
    }

    return undefined;
  },
  onEnableEmailNotificationsChange: function (address, checked) {
    let emailPusherPromise;

    if (checked) {
      const data = {};
      data['brand'] = _SdkConfig.default.get().brand || 'Riot';
      emailPusherPromise = _MatrixClientPeg.MatrixClientPeg.get().setPusher({
        kind: 'email',
        app_id: 'm.email',
        pushkey: address,
        app_display_name: 'Email Notifications',
        device_display_name: address,
        lang: navigator.language,
        data: data,
        append: true // We always append for email pushers since we don't want to stop other accounts notifying to the same email address

      });
    } else {
      const emailPusher = this.getEmailPusher(this.state.pushers, address);
      emailPusher.kind = null;
      emailPusherPromise = _MatrixClientPeg.MatrixClientPeg.get().setPusher(emailPusher);
    }

    emailPusherPromise.then(() => {
      this._refreshFromServer();
    }, error => {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Error saving email notification preferences', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Error saving email notification preferences'),
        description: (0, _languageHandler._t)('An error occurred whilst saving your email notification preferences.')
      });
    });
  },
  onNotifStateButtonClicked: function (event) {
    // FIXME: use .bind() rather than className metadata here surely
    const vectorRuleId = event.target.className.split("-")[0];
    const newPushRuleVectorState = event.target.className.split("-")[1];

    if ("_keywords" === vectorRuleId) {
      this._setKeywordsPushRuleVectorState(newPushRuleVectorState);
    } else {
      const rule = this.getRule(vectorRuleId);

      if (rule) {
        this._setPushRuleVectorState(rule, newPushRuleVectorState);
      }
    }
  },
  onKeywordsClicked: function (event) {
    const self = this; // Compute the keywords list to display

    let keywords = [];

    for (const i in this.state.vectorContentRules.rules) {
      const rule = this.state.vectorContentRules.rules[i];
      keywords.push(rule.pattern);
    }

    if (keywords.length) {
      // As keeping the order of per-word push rules hs side is a bit tricky to code,
      // display the keywords in alphabetical order to the user
      keywords.sort();
      keywords = keywords.join(", ");
    } else {
      keywords = "";
    }

    const TextInputDialog = sdk.getComponent("dialogs.TextInputDialog");

    _Modal.default.createTrackedDialog('Keywords Dialog', '', TextInputDialog, {
      title: (0, _languageHandler._t)('Keywords'),
      description: (0, _languageHandler._t)('Enter keywords separated by a comma:'),
      button: (0, _languageHandler._t)('OK'),
      value: keywords,
      onFinished: function onFinished(should_leave, newValue) {
        if (should_leave && newValue !== keywords) {
          let newKeywords = newValue.split(',');

          for (const i in newKeywords) {
            newKeywords[i] = newKeywords[i].trim();
          } // Remove duplicates and empty


          newKeywords = newKeywords.reduce(function (array, keyword) {
            if (keyword !== "" && array.indexOf(keyword) < 0) {
              array.push(keyword);
            }

            return array;
          }, []);

          self._setKeywords(newKeywords);
        }
      }
    });
  },
  getRule: function (vectorRuleId) {
    for (const i in this.state.vectorPushRules) {
      const rule = this.state.vectorPushRules[i];

      if (rule.vectorRuleId === vectorRuleId) {
        return rule;
      }
    }
  },
  _setPushRuleVectorState: function (rule, newPushRuleVectorState) {
    if (rule && rule.vectorState !== newPushRuleVectorState) {
      this.setState({
        phase: this.phases.LOADING
      });
      const self = this;

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const deferreds = [];
      const ruleDefinition = _notifications.VectorPushRulesDefinitions[rule.vectorRuleId];

      if (rule.rule) {
        const actions = ruleDefinition.vectorStateToActions[newPushRuleVectorState];

        if (!actions) {
          // The new state corresponds to disabling the rule.
          deferreds.push(cli.setPushRuleEnabled('global', rule.rule.kind, rule.rule.rule_id, false));
        } else {
          // The new state corresponds to enabling the rule and setting specific actions
          deferreds.push(this._updatePushRuleActions(rule.rule, actions, true));
        }
      }

      Promise.all(deferreds).then(function () {
        self._refreshFromServer();
      }, function (error) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to change settings: " + error);

        _Modal.default.createTrackedDialog('Failed to change settings', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Failed to change settings'),
          description: error && error.message ? error.message : (0, _languageHandler._t)('Operation failed'),
          onFinished: self._refreshFromServer
        });
      });
    }
  },
  _setKeywordsPushRuleVectorState: function (newPushRuleVectorState) {
    // Is there really a change?
    if (this.state.vectorContentRules.vectorState === newPushRuleVectorState || this.state.vectorContentRules.rules.length === 0) {
      return;
    }

    const self = this;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    this.setState({
      phase: this.phases.LOADING
    }); // Update all rules in self.state.vectorContentRules

    const deferreds = [];

    for (const i in this.state.vectorContentRules.rules) {
      const rule = this.state.vectorContentRules.rules[i];
      let enabled;
      let actions;

      switch (newPushRuleVectorState) {
        case _notifications.PushRuleVectorState.ON:
          if (rule.actions.length !== 1) {
            actions = _notifications.PushRuleVectorState.actionsFor(_notifications.PushRuleVectorState.ON);
          }

          if (this.state.vectorContentRules.vectorState === _notifications.PushRuleVectorState.OFF) {
            enabled = true;
          }

          break;

        case _notifications.PushRuleVectorState.LOUD:
          if (rule.actions.length !== 3) {
            actions = _notifications.PushRuleVectorState.actionsFor(_notifications.PushRuleVectorState.LOUD);
          }

          if (this.state.vectorContentRules.vectorState === _notifications.PushRuleVectorState.OFF) {
            enabled = true;
          }

          break;

        case _notifications.PushRuleVectorState.OFF:
          enabled = false;
          break;
      }

      if (actions) {
        // Note that the workaround in _updatePushRuleActions will automatically
        // enable the rule
        deferreds.push(this._updatePushRuleActions(rule, actions, enabled));
      } else if (enabled != undefined) {
        deferreds.push(cli.setPushRuleEnabled('global', rule.kind, rule.rule_id, enabled));
      }
    }

    Promise.all(deferreds).then(function (resps) {
      self._refreshFromServer();
    }, function (error) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Can't update user notification settings: " + error);

      _Modal.default.createTrackedDialog('Can\'t update user notifcation settings', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Can\'t update user notification settings'),
        description: error && error.message ? error.message : (0, _languageHandler._t)('Operation failed'),
        onFinished: self._refreshFromServer
      });
    });
  },
  _setKeywords: function (newKeywords) {
    this.setState({
      phase: this.phases.LOADING
    });
    const self = this;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const removeDeferreds = []; // Remove per-word push rules of keywords that are no more in the list

    const vectorContentRulesPatterns = [];

    for (const i in self.state.vectorContentRules.rules) {
      const rule = self.state.vectorContentRules.rules[i];
      vectorContentRulesPatterns.push(rule.pattern);

      if (newKeywords.indexOf(rule.pattern) < 0) {
        removeDeferreds.push(cli.deletePushRule('global', rule.kind, rule.rule_id));
      }
    } // If the keyword is part of `externalContentRules`, remove the rule
    // before recreating it in the right Vector path


    for (const i in self.state.externalContentRules) {
      const rule = self.state.externalContentRules[i];

      if (newKeywords.indexOf(rule.pattern) >= 0) {
        removeDeferreds.push(cli.deletePushRule('global', rule.kind, rule.rule_id));
      }
    }

    const onError = function (error) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Failed to update keywords: " + error);

      _Modal.default.createTrackedDialog('Failed to update keywords', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Failed to update keywords'),
        description: error && error.message ? error.message : (0, _languageHandler._t)('Operation failed'),
        onFinished: self._refreshFromServer
      });
    }; // Then, add the new ones


    Promise.all(removeDeferreds).then(function (resps) {
      const deferreds = [];
      let pushRuleVectorStateKind = self.state.vectorContentRules.vectorState;

      if (pushRuleVectorStateKind === _notifications.PushRuleVectorState.OFF) {
        // When the current global keywords rule is OFF, we need to look at
        // the flavor of rules in 'vectorContentRules' to apply the same actions
        // when creating the new rule.
        // Thus, this new rule will join the 'vectorContentRules' set.
        if (self.state.vectorContentRules.rules.length) {
          pushRuleVectorStateKind = _notifications.PushRuleVectorState.contentRuleVectorStateKind(self.state.vectorContentRules.rules[0]);
        } else {
          // ON is default
          pushRuleVectorStateKind = _notifications.PushRuleVectorState.ON;
        }
      }

      for (const i in newKeywords) {
        const keyword = newKeywords[i];

        if (vectorContentRulesPatterns.indexOf(keyword) < 0) {
          if (self.state.vectorContentRules.vectorState !== _notifications.PushRuleVectorState.OFF) {
            deferreds.push(cli.addPushRule('global', 'content', keyword, {
              actions: _notifications.PushRuleVectorState.actionsFor(pushRuleVectorStateKind),
              pattern: keyword
            }));
          } else {
            deferreds.push(self._addDisabledPushRule('global', 'content', keyword, {
              actions: _notifications.PushRuleVectorState.actionsFor(pushRuleVectorStateKind),
              pattern: keyword
            }));
          }
        }
      }

      Promise.all(deferreds).then(function (resps) {
        self._refreshFromServer();
      }, onError);
    }, onError);
  },
  // Create a push rule but disabled
  _addDisabledPushRule: function (scope, kind, ruleId, body) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    return cli.addPushRule(scope, kind, ruleId, body).then(() => cli.setPushRuleEnabled(scope, kind, ruleId, false));
  },
  // Check if any legacy im.vector rules need to be ported to the new API
  // for overriding the actions of default rules.
  _portRulesToNewAPI: function (rulesets) {
    const needsUpdate = [];

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    for (const kind in rulesets.global) {
      const ruleset = rulesets.global[kind];

      for (let i = 0; i < ruleset.length; ++i) {
        const rule = ruleset[i];

        if (rule.rule_id in LEGACY_RULES) {
          console.log("Porting legacy rule", rule);
          needsUpdate.push(function (kind, rule) {
            return cli.setPushRuleActions('global', kind, LEGACY_RULES[rule.rule_id], portLegacyActions(rule.actions)).then(() => cli.deletePushRule('global', kind, rule.rule_id)).catch(e => {
              console.warn("Error when porting legacy rule: ".concat(e));
            });
          }(kind, rule));
        }
      }
    }

    if (needsUpdate.length > 0) {
      // If some of the rules need to be ported then wait for the porting
      // to happen and then fetch the rules again.
      return Promise.all(needsUpdate).then(() => cli.getPushRules());
    } else {
      // Otherwise return the rules that we already have.
      return rulesets;
    }
  },
  _refreshFromServer: function () {
    const self = this;

    const pushRulesPromise = _MatrixClientPeg.MatrixClientPeg.get().getPushRules().then(self._portRulesToNewAPI).then(function (rulesets) {
      /// XXX seriously? wtf is this?
      _MatrixClientPeg.MatrixClientPeg.get().pushRules = rulesets; // Get homeserver default rules and triage them by categories

      const rule_categories = {
        // The master rule (all notifications disabling)
        '.m.rule.master': 'master',
        // The default push rules displayed by Vector UI
        '.m.rule.contains_display_name': 'vector',
        '.m.rule.contains_user_name': 'vector',
        '.m.rule.roomnotif': 'vector',
        '.m.rule.room_one_to_one': 'vector',
        '.m.rule.encrypted_room_one_to_one': 'vector',
        '.m.rule.message': 'vector',
        '.m.rule.encrypted': 'vector',
        '.m.rule.invite_for_me': 'vector',
        //'.m.rule.member_event': 'vector',
        '.m.rule.call': 'vector',
        '.m.rule.suppress_notices': 'vector',
        '.m.rule.tombstone': 'vector' // Others go to others

      }; // HS default rules

      const defaultRules = {
        master: [],
        vector: {},
        others: []
      };

      for (const kind in rulesets.global) {
        for (let i = 0; i < Object.keys(rulesets.global[kind]).length; ++i) {
          const r = rulesets.global[kind][i];
          const cat = rule_categories[r.rule_id];
          r.kind = kind;

          if (r.rule_id[0] === '.') {
            if (cat === 'vector') {
              defaultRules.vector[r.rule_id] = r;
            } else if (cat === 'master') {
              defaultRules.master.push(r);
            } else {
              defaultRules['others'].push(r);
            }
          }
        }
      } // Get the master rule if any defined by the hs


      if (defaultRules.master.length > 0) {
        self.state.masterPushRule = defaultRules.master[0];
      } // parse the keyword rules into our state


      const contentRules = _notifications.ContentRules.parseContentRules(rulesets);

      self.state.vectorContentRules = {
        vectorState: contentRules.vectorState,
        rules: contentRules.rules
      };
      self.state.externalContentRules = contentRules.externalRules; // Build the rules displayed in the Vector UI matrix table

      self.state.vectorPushRules = [];
      self.state.externalPushRules = [];
      const vectorRuleIds = ['.m.rule.contains_display_name', '.m.rule.contains_user_name', '.m.rule.roomnotif', '_keywords', '.m.rule.room_one_to_one', '.m.rule.encrypted_room_one_to_one', '.m.rule.message', '.m.rule.encrypted', '.m.rule.invite_for_me', //'im.vector.rule.member_event',
      '.m.rule.call', '.m.rule.suppress_notices', '.m.rule.tombstone'];

      for (const i in vectorRuleIds) {
        const vectorRuleId = vectorRuleIds[i];

        if (vectorRuleId === '_keywords') {
          // keywords needs a special handling
          // For Vector UI, this is a single global push rule but translated in Matrix,
          // it corresponds to all content push rules (stored in self.state.vectorContentRule)
          self.state.vectorPushRules.push({
            "vectorRuleId": "_keywords",
            "description": _react.default.createElement("span", null, (0, _languageHandler._t)('Messages containing <span>keywords</span>', {}, {
              'span': sub => _react.default.createElement("span", {
                className: "mx_UserNotifSettings_keywords",
                onClick: self.onKeywordsClicked
              }, sub)
            })),
            "vectorState": self.state.vectorContentRules.vectorState
          });
        } else {
          const ruleDefinition = _notifications.VectorPushRulesDefinitions[vectorRuleId];
          const rule = defaultRules.vector[vectorRuleId];
          const vectorState = ruleDefinition.ruleToVectorState(rule); //console.log("Refreshing vectorPushRules for " + vectorRuleId +", "+ ruleDefinition.description +", " + rule +", " + vectorState);

          self.state.vectorPushRules.push({
            "vectorRuleId": vectorRuleId,
            "description": (0, _languageHandler._t)(ruleDefinition.description),
            // Text from VectorPushRulesDefinitions.js
            "rule": rule,
            "vectorState": vectorState
          }); // if there was a rule which we couldn't parse, add it to the external list

          if (rule && !vectorState) {
            rule.description = ruleDefinition.description;
            self.state.externalPushRules.push(rule);
          }
        }
      } // Build the rules not managed by Vector UI


      const otherRulesDescriptions = {
        '.m.rule.message': (0, _languageHandler._t)('Notify for all other messages/rooms'),
        '.m.rule.fallback': (0, _languageHandler._t)('Notify me for anything else')
      };

      for (const i in defaultRules.others) {
        const rule = defaultRules.others[i];
        const ruleDescription = otherRulesDescriptions[rule.rule_id]; // Show enabled default rules that was modified by the user

        if (ruleDescription && rule.enabled && !rule.default) {
          rule.description = ruleDescription;
          self.state.externalPushRules.push(rule);
        }
      }
    });

    const pushersPromise = _MatrixClientPeg.MatrixClientPeg.get().getPushers().then(function (resp) {
      self.setState({
        pushers: resp.pushers
      });
    });

    Promise.all([pushRulesPromise, pushersPromise]).then(function () {
      self.setState({
        phase: self.phases.DISPLAY
      });
    }, function (error) {
      console.error(error);
      self.setState({
        phase: self.phases.ERROR
      });
    }).finally(() => {
      // actually explicitly update our state  having been deep-manipulating it
      self.setState({
        masterPushRule: self.state.masterPushRule,
        vectorContentRules: self.state.vectorContentRules,
        vectorPushRules: self.state.vectorPushRules,
        externalContentRules: self.state.externalContentRules,
        externalPushRules: self.state.externalPushRules
      });
    });

    _MatrixClientPeg.MatrixClientPeg.get().getThreePids().then(r => this.setState({
      threepids: r.threepids
    }));
  },
  _onClearNotifications: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.getRooms().forEach(r => {
      if (r.getUnreadNotificationCount() > 0) {
        const events = r.getLiveTimeline().getEvents();
        if (events.length) cli.sendReadReceipt(events.pop());
      }
    });
  },
  _updatePushRuleActions: function (rule, actions, enabled) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    return cli.setPushRuleActions('global', rule.kind, rule.rule_id, actions).then(function () {
      // Then, if requested, enabled or disabled the rule
      if (undefined != enabled) {
        return cli.setPushRuleEnabled('global', rule.kind, rule.rule_id, enabled);
      }
    });
  },
  renderNotifRulesTableRow: function (title, className, pushRuleVectorState) {
    return _react.default.createElement("tr", {
      key: className
    }, _react.default.createElement("th", null, title), _react.default.createElement("th", null, _react.default.createElement("input", {
      className: className + "-" + _notifications.PushRuleVectorState.OFF,
      type: "radio",
      checked: pushRuleVectorState === _notifications.PushRuleVectorState.OFF,
      onChange: this.onNotifStateButtonClicked
    })), _react.default.createElement("th", null, _react.default.createElement("input", {
      className: className + "-" + _notifications.PushRuleVectorState.ON,
      type: "radio",
      checked: pushRuleVectorState === _notifications.PushRuleVectorState.ON,
      onChange: this.onNotifStateButtonClicked
    })), _react.default.createElement("th", null, _react.default.createElement("input", {
      className: className + "-" + _notifications.PushRuleVectorState.LOUD,
      type: "radio",
      checked: pushRuleVectorState === _notifications.PushRuleVectorState.LOUD,
      onChange: this.onNotifStateButtonClicked
    })));
  },
  renderNotifRulesTableRows: function () {
    const rows = [];

    for (const i in this.state.vectorPushRules) {
      const rule = this.state.vectorPushRules[i];

      if (rule.rule === undefined && rule.vectorRuleId.startsWith(".m.")) {
        console.warn("Skipping render of rule ".concat(rule.vectorRuleId, " due to no underlying rule"));
        continue;
      } //console.log("rendering: " + rule.description + ", " + rule.vectorRuleId + ", " + rule.vectorState);


      rows.push(this.renderNotifRulesTableRow(rule.description, rule.vectorRuleId, rule.vectorState));
    }

    return rows;
  },
  hasEmailPusher: function (pushers, address) {
    if (pushers === undefined) {
      return false;
    }

    for (let i = 0; i < pushers.length; ++i) {
      if (pushers[i].kind === 'email' && pushers[i].pushkey === address) {
        return true;
      }
    }

    return false;
  },
  emailNotificationsRow: function (address, label) {
    return _react.default.createElement(_LabelledToggleSwitch.default, {
      value: this.hasEmailPusher(this.state.pushers, address),
      onChange: this.onEnableEmailNotificationsChange.bind(this, address),
      label: label,
      key: "emailNotif_".concat(label)
    });
  },
  render: function () {
    let spinner;

    if (this.state.phase === this.phases.LOADING) {
      const Loader = sdk.getComponent("elements.Spinner");
      spinner = _react.default.createElement(Loader, null);
    }

    let masterPushRuleDiv;

    if (this.state.masterPushRule) {
      masterPushRuleDiv = _react.default.createElement(_LabelledToggleSwitch.default, {
        value: !this.state.masterPushRule.enabled,
        onChange: this.onEnableNotificationsChange,
        label: (0, _languageHandler._t)('Enable notifications for this account')
      });
    }

    let clearNotificationsButton;

    if (_MatrixClientPeg.MatrixClientPeg.get().getRooms().some(r => r.getUnreadNotificationCount() > 0)) {
      clearNotificationsButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onClearNotifications,
        kind: "danger"
      }, (0, _languageHandler._t)("Clear notifications"));
    } // When enabled, the master rule inhibits all existing rules
    // So do not show all notification settings


    if (this.state.masterPushRule && this.state.masterPushRule.enabled) {
      return _react.default.createElement("div", null, masterPushRuleDiv, _react.default.createElement("div", {
        className: "mx_UserNotifSettings_notifTable"
      }, (0, _languageHandler._t)('All notifications are currently disabled for all targets.')), clearNotificationsButton);
    }

    const emailThreepids = this.state.threepids.filter(tp => tp.medium === "email");
    let emailNotificationsRows;

    if (emailThreepids.length === 0) {
      emailNotificationsRows = _react.default.createElement("div", null, (0, _languageHandler._t)('Add an email address to configure email notifications'));
    } else {
      emailNotificationsRows = emailThreepids.map(threePid => this.emailNotificationsRow(threePid.address, "".concat((0, _languageHandler._t)('Enable email notifications'), " (").concat(threePid.address, ")")));
    } // Build external push rules


    const externalRules = [];

    for (const i in this.state.externalPushRules) {
      const rule = this.state.externalPushRules[i];
      externalRules.push(_react.default.createElement("li", null, (0, _languageHandler._t)(rule.description)));
    } // Show keywords not displayed by the vector UI as a single external push rule


    let externalKeywords = [];

    for (const i in this.state.externalContentRules) {
      const rule = this.state.externalContentRules[i];
      externalKeywords.push(rule.pattern);
    }

    if (externalKeywords.length) {
      externalKeywords = externalKeywords.join(", ");
      externalRules.push(_react.default.createElement("li", null, (0, _languageHandler._t)('Notifications on the following keywords follow rules which can’t be displayed here:'), " ", externalKeywords));
    }

    let devicesSection;

    if (this.state.pushers === undefined) {
      devicesSection = _react.default.createElement("div", {
        className: "error"
      }, (0, _languageHandler._t)('Unable to fetch notification target list'));
    } else if (this.state.pushers.length === 0) {
      devicesSection = null;
    } else {
      // TODO: It would be great to be able to delete pushers from here too,
      // and this wouldn't be hard to add.
      const rows = [];

      for (let i = 0; i < this.state.pushers.length; ++i) {
        rows.push(_react.default.createElement("tr", {
          key: i
        }, _react.default.createElement("td", null, this.state.pushers[i].app_display_name), _react.default.createElement("td", null, this.state.pushers[i].device_display_name)));
      }

      devicesSection = _react.default.createElement("table", {
        className: "mx_UserNotifSettings_devicesTable"
      }, _react.default.createElement("tbody", null, rows));
    }

    if (devicesSection) {
      devicesSection = _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)('Notification targets')), devicesSection);
    }

    let advancedSettings;

    if (externalRules.length) {
      advancedSettings = _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)('Advanced notification settings')), (0, _languageHandler._t)('There are advanced notifications which are not shown here'), ".", _react.default.createElement("br", null), (0, _languageHandler._t)('You might have configured them in a client other than Riot. You cannot tune them in Riot but they still apply'), ".", _react.default.createElement("ul", null, externalRules));
    }

    return _react.default.createElement("div", null, masterPushRuleDiv, _react.default.createElement("div", {
      className: "mx_UserNotifSettings_notifTable"
    }, spinner, _react.default.createElement(_LabelledToggleSwitch.default, {
      value: _SettingsStore.default.getValue("notificationsEnabled"),
      onChange: this.onEnableDesktopNotificationsChange,
      label: (0, _languageHandler._t)('Enable desktop notifications for this session')
    }), _react.default.createElement(_LabelledToggleSwitch.default, {
      value: _SettingsStore.default.getValue("notificationBodyEnabled"),
      onChange: this.onEnableDesktopNotificationBodyChange,
      label: (0, _languageHandler._t)('Show message in desktop notification')
    }), _react.default.createElement(_LabelledToggleSwitch.default, {
      value: _SettingsStore.default.getValue("audioNotificationsEnabled"),
      onChange: this.onEnableAudioNotificationsChange,
      label: (0, _languageHandler._t)('Enable audible notifications for this session')
    }), emailNotificationsRows, _react.default.createElement("div", {
      className: "mx_UserNotifSettings_pushRulesTableWrapper"
    }, _react.default.createElement("table", {
      className: "mx_UserNotifSettings_pushRulesTable"
    }, _react.default.createElement("thead", null, _react.default.createElement("tr", null, _react.default.createElement("th", {
      width: "55%"
    }), _react.default.createElement("th", {
      width: "15%"
    }, (0, _languageHandler._t)('Off')), _react.default.createElement("th", {
      width: "15%"
    }, (0, _languageHandler._t)('On')), _react.default.createElement("th", {
      width: "15%"
    }, (0, _languageHandler._t)('Noisy')))), _react.default.createElement("tbody", null, this.renderNotifRulesTableRows()))), advancedSettings, devicesSection, clearNotificationsButton));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL05vdGlmaWNhdGlvbnMuanMiXSwibmFtZXMiOlsiTEVHQUNZX1JVTEVTIiwicG9ydExlZ2FjeUFjdGlvbnMiLCJhY3Rpb25zIiwiZGVjb2RlZCIsIk5vdGlmaWNhdGlvblV0aWxzIiwiZGVjb2RlQWN0aW9ucyIsImVuY29kZUFjdGlvbnMiLCJkaXNwbGF5TmFtZSIsInBoYXNlcyIsIkxPQURJTkciLCJESVNQTEFZIiwiRVJST1IiLCJnZXRJbml0aWFsU3RhdGUiLCJwaGFzZSIsIm1hc3RlclB1c2hSdWxlIiwidW5kZWZpbmVkIiwidmVjdG9yUHVzaFJ1bGVzIiwidmVjdG9yQ29udGVudFJ1bGVzIiwidmVjdG9yU3RhdGUiLCJQdXNoUnVsZVZlY3RvclN0YXRlIiwiT04iLCJydWxlcyIsImV4dGVybmFsUHVzaFJ1bGVzIiwiZXh0ZXJuYWxDb250ZW50UnVsZXMiLCJ0aHJlZXBpZHMiLCJjb21wb25lbnREaWRNb3VudCIsIl9yZWZyZXNoRnJvbVNlcnZlciIsIm9uRW5hYmxlTm90aWZpY2F0aW9uc0NoYW5nZSIsImNoZWNrZWQiLCJzZWxmIiwic2V0U3RhdGUiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJzZXRQdXNoUnVsZUVuYWJsZWQiLCJzdGF0ZSIsImtpbmQiLCJydWxlX2lkIiwidGhlbiIsIm9uRW5hYmxlRGVza3RvcE5vdGlmaWNhdGlvbnNDaGFuZ2UiLCJTZXR0aW5nc1N0b3JlIiwic2V0VmFsdWUiLCJTZXR0aW5nTGV2ZWwiLCJERVZJQ0UiLCJmaW5hbGx5IiwiZm9yY2VVcGRhdGUiLCJvbkVuYWJsZURlc2t0b3BOb3RpZmljYXRpb25Cb2R5Q2hhbmdlIiwib25FbmFibGVBdWRpb05vdGlmaWNhdGlvbnNDaGFuZ2UiLCJnZXRFbWFpbFB1c2hlciIsInB1c2hlcnMiLCJhZGRyZXNzIiwiaSIsImxlbmd0aCIsInB1c2hrZXkiLCJvbkVuYWJsZUVtYWlsTm90aWZpY2F0aW9uc0NoYW5nZSIsImVtYWlsUHVzaGVyUHJvbWlzZSIsImRhdGEiLCJTZGtDb25maWciLCJicmFuZCIsInNldFB1c2hlciIsImFwcF9pZCIsImFwcF9kaXNwbGF5X25hbWUiLCJkZXZpY2VfZGlzcGxheV9uYW1lIiwibGFuZyIsIm5hdmlnYXRvciIsImxhbmd1YWdlIiwiYXBwZW5kIiwiZW1haWxQdXNoZXIiLCJlcnJvciIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIm9uTm90aWZTdGF0ZUJ1dHRvbkNsaWNrZWQiLCJldmVudCIsInZlY3RvclJ1bGVJZCIsInRhcmdldCIsImNsYXNzTmFtZSIsInNwbGl0IiwibmV3UHVzaFJ1bGVWZWN0b3JTdGF0ZSIsIl9zZXRLZXl3b3Jkc1B1c2hSdWxlVmVjdG9yU3RhdGUiLCJydWxlIiwiZ2V0UnVsZSIsIl9zZXRQdXNoUnVsZVZlY3RvclN0YXRlIiwib25LZXl3b3Jkc0NsaWNrZWQiLCJrZXl3b3JkcyIsInB1c2giLCJwYXR0ZXJuIiwic29ydCIsImpvaW4iLCJUZXh0SW5wdXREaWFsb2ciLCJidXR0b24iLCJ2YWx1ZSIsIm9uRmluaXNoZWQiLCJzaG91bGRfbGVhdmUiLCJuZXdWYWx1ZSIsIm5ld0tleXdvcmRzIiwidHJpbSIsInJlZHVjZSIsImFycmF5Iiwia2V5d29yZCIsImluZGV4T2YiLCJfc2V0S2V5d29yZHMiLCJjbGkiLCJkZWZlcnJlZHMiLCJydWxlRGVmaW5pdGlvbiIsIlZlY3RvclB1c2hSdWxlc0RlZmluaXRpb25zIiwidmVjdG9yU3RhdGVUb0FjdGlvbnMiLCJfdXBkYXRlUHVzaFJ1bGVBY3Rpb25zIiwiUHJvbWlzZSIsImFsbCIsImNvbnNvbGUiLCJtZXNzYWdlIiwiZW5hYmxlZCIsImFjdGlvbnNGb3IiLCJPRkYiLCJMT1VEIiwicmVzcHMiLCJyZW1vdmVEZWZlcnJlZHMiLCJ2ZWN0b3JDb250ZW50UnVsZXNQYXR0ZXJucyIsImRlbGV0ZVB1c2hSdWxlIiwib25FcnJvciIsInB1c2hSdWxlVmVjdG9yU3RhdGVLaW5kIiwiY29udGVudFJ1bGVWZWN0b3JTdGF0ZUtpbmQiLCJhZGRQdXNoUnVsZSIsIl9hZGREaXNhYmxlZFB1c2hSdWxlIiwic2NvcGUiLCJydWxlSWQiLCJib2R5IiwiX3BvcnRSdWxlc1RvTmV3QVBJIiwicnVsZXNldHMiLCJuZWVkc1VwZGF0ZSIsImdsb2JhbCIsInJ1bGVzZXQiLCJsb2ciLCJzZXRQdXNoUnVsZUFjdGlvbnMiLCJjYXRjaCIsImUiLCJ3YXJuIiwiZ2V0UHVzaFJ1bGVzIiwicHVzaFJ1bGVzUHJvbWlzZSIsInB1c2hSdWxlcyIsInJ1bGVfY2F0ZWdvcmllcyIsImRlZmF1bHRSdWxlcyIsIm1hc3RlciIsInZlY3RvciIsIm90aGVycyIsIk9iamVjdCIsImtleXMiLCJyIiwiY2F0IiwiY29udGVudFJ1bGVzIiwiQ29udGVudFJ1bGVzIiwicGFyc2VDb250ZW50UnVsZXMiLCJleHRlcm5hbFJ1bGVzIiwidmVjdG9yUnVsZUlkcyIsInN1YiIsInJ1bGVUb1ZlY3RvclN0YXRlIiwib3RoZXJSdWxlc0Rlc2NyaXB0aW9ucyIsInJ1bGVEZXNjcmlwdGlvbiIsImRlZmF1bHQiLCJwdXNoZXJzUHJvbWlzZSIsImdldFB1c2hlcnMiLCJyZXNwIiwiZ2V0VGhyZWVQaWRzIiwiX29uQ2xlYXJOb3RpZmljYXRpb25zIiwiZ2V0Um9vbXMiLCJmb3JFYWNoIiwiZ2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQiLCJldmVudHMiLCJnZXRMaXZlVGltZWxpbmUiLCJnZXRFdmVudHMiLCJzZW5kUmVhZFJlY2VpcHQiLCJwb3AiLCJyZW5kZXJOb3RpZlJ1bGVzVGFibGVSb3ciLCJwdXNoUnVsZVZlY3RvclN0YXRlIiwicmVuZGVyTm90aWZSdWxlc1RhYmxlUm93cyIsInJvd3MiLCJzdGFydHNXaXRoIiwiaGFzRW1haWxQdXNoZXIiLCJlbWFpbE5vdGlmaWNhdGlvbnNSb3ciLCJsYWJlbCIsImJpbmQiLCJyZW5kZXIiLCJzcGlubmVyIiwiTG9hZGVyIiwibWFzdGVyUHVzaFJ1bGVEaXYiLCJjbGVhck5vdGlmaWNhdGlvbnNCdXR0b24iLCJzb21lIiwiZW1haWxUaHJlZXBpZHMiLCJmaWx0ZXIiLCJ0cCIsIm1lZGl1bSIsImVtYWlsTm90aWZpY2F0aW9uc1Jvd3MiLCJtYXAiLCJ0aHJlZVBpZCIsImV4dGVybmFsS2V5d29yZHMiLCJkZXZpY2VzU2VjdGlvbiIsImFkdmFuY2VkU2V0dGluZ3MiLCJnZXRWYWx1ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBTUE7O0FBQ0E7O0FBQ0E7O0FBL0JBOzs7Ozs7Ozs7Ozs7Ozs7QUFpQ0E7QUFDQTtBQUVBO0FBQ0E7O0FBR0E7Ozs7O0FBS0EsTUFBTUEsWUFBWSxHQUFHO0FBQ2pCLDBDQUF3QywrQkFEdkI7QUFFakIsb0NBQWtDLHlCQUZqQjtBQUdqQixpQ0FBK0IsaUJBSGQ7QUFJakIsa0NBQWdDLHVCQUpmO0FBS2pCLHlCQUF1QixjQUxOO0FBTWpCLDRCQUEwQjtBQU5ULENBQXJCOztBQVNBLFNBQVNDLGlCQUFULENBQTJCQyxPQUEzQixFQUFvQztBQUNoQyxRQUFNQyxPQUFPLEdBQUdDLGlDQUFrQkMsYUFBbEIsQ0FBZ0NILE9BQWhDLENBQWhCOztBQUNBLE1BQUlDLE9BQU8sS0FBSyxJQUFoQixFQUFzQjtBQUNsQixXQUFPQyxpQ0FBa0JFLGFBQWxCLENBQWdDSCxPQUFoQyxDQUFQO0FBQ0gsR0FGRCxNQUVPO0FBQ0g7QUFDQTtBQUNBLFdBQU9ELE9BQVA7QUFDSDtBQUNKOztlQUVjLCtCQUFpQjtBQUM1QkssRUFBQUEsV0FBVyxFQUFFLGVBRGU7QUFHNUJDLEVBQUFBLE1BQU0sRUFBRTtBQUNKQyxJQUFBQSxPQUFPLEVBQUUsU0FETDtBQUNnQjtBQUNwQkMsSUFBQUEsT0FBTyxFQUFFLFNBRkw7QUFFZ0I7QUFDcEJDLElBQUFBLEtBQUssRUFBRSxPQUhILENBR1k7O0FBSFosR0FIb0I7QUFTNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsS0FBSyxFQUFFLEtBQUtMLE1BQUwsQ0FBWUMsT0FEaEI7QUFFSEssTUFBQUEsY0FBYyxFQUFFQyxTQUZiO0FBRXdCO0FBQzNCQyxNQUFBQSxlQUFlLEVBQUUsRUFIZDtBQUdrQjtBQUNyQkMsTUFBQUEsa0JBQWtCLEVBQUU7QUFBRTtBQUNsQkMsUUFBQUEsV0FBVyxFQUFFQyxtQ0FBb0JDLEVBRGpCO0FBRWhCQyxRQUFBQSxLQUFLLEVBQUU7QUFGUyxPQUpqQjtBQVFIQyxNQUFBQSxpQkFBaUIsRUFBRSxFQVJoQjtBQVFvQjtBQUN2QkMsTUFBQUEsb0JBQW9CLEVBQUUsRUFUbkI7QUFTdUI7QUFDMUJDLE1BQUFBLFNBQVMsRUFBRSxFQVZSLENBVVk7O0FBVlosS0FBUDtBQVlILEdBdEIyQjtBQXdCNUJDLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsU0FBS0Msa0JBQUw7QUFDSCxHQTFCMkI7QUE0QjVCQyxFQUFBQSwyQkFBMkIsRUFBRSxVQUFTQyxPQUFULEVBQWtCO0FBQzNDLFVBQU1DLElBQUksR0FBRyxJQUFiO0FBQ0EsU0FBS0MsUUFBTCxDQUFjO0FBQ1ZqQixNQUFBQSxLQUFLLEVBQUUsS0FBS0wsTUFBTCxDQUFZQztBQURULEtBQWQ7O0FBSUFzQixxQ0FBZ0JDLEdBQWhCLEdBQXNCQyxrQkFBdEIsQ0FBeUMsUUFBekMsRUFBbURKLElBQUksQ0FBQ0ssS0FBTCxDQUFXcEIsY0FBWCxDQUEwQnFCLElBQTdFLEVBQW1GTixJQUFJLENBQUNLLEtBQUwsQ0FBV3BCLGNBQVgsQ0FBMEJzQixPQUE3RyxFQUFzSCxDQUFDUixPQUF2SCxFQUFnSVMsSUFBaEksQ0FBcUksWUFBVztBQUM3SVIsTUFBQUEsSUFBSSxDQUFDSCxrQkFBTDtBQUNGLEtBRkQ7QUFHSCxHQXJDMkI7QUF1QzVCWSxFQUFBQSxrQ0FBa0MsRUFBRSxVQUFTVixPQUFULEVBQWtCO0FBQ2xEVywyQkFBY0MsUUFBZCxDQUNJLHNCQURKLEVBQzRCLElBRDVCLEVBRUlDLDRCQUFhQyxNQUZqQixFQUdJZCxPQUhKLEVBSUVlLE9BSkYsQ0FJVSxNQUFNO0FBQ1osV0FBS0MsV0FBTDtBQUNILEtBTkQ7QUFPSCxHQS9DMkI7QUFpRDVCQyxFQUFBQSxxQ0FBcUMsRUFBRSxVQUFTakIsT0FBVCxFQUFrQjtBQUNyRFcsMkJBQWNDLFFBQWQsQ0FDSSx5QkFESixFQUMrQixJQUQvQixFQUVJQyw0QkFBYUMsTUFGakIsRUFHSWQsT0FISixFQUlFZSxPQUpGLENBSVUsTUFBTTtBQUNaLFdBQUtDLFdBQUw7QUFDSCxLQU5EO0FBT0gsR0F6RDJCO0FBMkQ1QkUsRUFBQUEsZ0NBQWdDLEVBQUUsVUFBU2xCLE9BQVQsRUFBa0I7QUFDaERXLDJCQUFjQyxRQUFkLENBQ0ksMkJBREosRUFDaUMsSUFEakMsRUFFSUMsNEJBQWFDLE1BRmpCLEVBR0lkLE9BSEosRUFJRWUsT0FKRixDQUlVLE1BQU07QUFDWixXQUFLQyxXQUFMO0FBQ0gsS0FORDtBQU9ILEdBbkUyQjs7QUFxRTVCOzs7Ozs7QUFNQUcsRUFBQUEsY0FBYyxFQUFFLFVBQVNDLE9BQVQsRUFBa0JDLE9BQWxCLEVBQTJCO0FBQ3ZDLFFBQUlELE9BQU8sS0FBS2pDLFNBQWhCLEVBQTJCO0FBQ3ZCLGFBQU9BLFNBQVA7QUFDSDs7QUFDRCxTQUFLLElBQUltQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixPQUFPLENBQUNHLE1BQTVCLEVBQW9DLEVBQUVELENBQXRDLEVBQXlDO0FBQ3JDLFVBQUlGLE9BQU8sQ0FBQ0UsQ0FBRCxDQUFQLENBQVdmLElBQVgsS0FBb0IsT0FBcEIsSUFBK0JhLE9BQU8sQ0FBQ0UsQ0FBRCxDQUFQLENBQVdFLE9BQVgsS0FBdUJILE9BQTFELEVBQW1FO0FBQy9ELGVBQU9ELE9BQU8sQ0FBQ0UsQ0FBRCxDQUFkO0FBQ0g7QUFDSjs7QUFDRCxXQUFPbkMsU0FBUDtBQUNILEdBckYyQjtBQXVGNUJzQyxFQUFBQSxnQ0FBZ0MsRUFBRSxVQUFTSixPQUFULEVBQWtCckIsT0FBbEIsRUFBMkI7QUFDekQsUUFBSTBCLGtCQUFKOztBQUNBLFFBQUkxQixPQUFKLEVBQWE7QUFDVCxZQUFNMkIsSUFBSSxHQUFHLEVBQWI7QUFDQUEsTUFBQUEsSUFBSSxDQUFDLE9BQUQsQ0FBSixHQUFnQkMsbUJBQVV4QixHQUFWLEdBQWdCeUIsS0FBaEIsSUFBeUIsTUFBekM7QUFDQUgsTUFBQUEsa0JBQWtCLEdBQUd2QixpQ0FBZ0JDLEdBQWhCLEdBQXNCMEIsU0FBdEIsQ0FBZ0M7QUFDakR2QixRQUFBQSxJQUFJLEVBQUUsT0FEMkM7QUFFakR3QixRQUFBQSxNQUFNLEVBQUUsU0FGeUM7QUFHakRQLFFBQUFBLE9BQU8sRUFBRUgsT0FId0M7QUFJakRXLFFBQUFBLGdCQUFnQixFQUFFLHFCQUorQjtBQUtqREMsUUFBQUEsbUJBQW1CLEVBQUVaLE9BTDRCO0FBTWpEYSxRQUFBQSxJQUFJLEVBQUVDLFNBQVMsQ0FBQ0MsUUFOaUM7QUFPakRULFFBQUFBLElBQUksRUFBRUEsSUFQMkM7QUFRakRVLFFBQUFBLE1BQU0sRUFBRSxJQVJ5QyxDQVFuQzs7QUFSbUMsT0FBaEMsQ0FBckI7QUFVSCxLQWJELE1BYU87QUFDSCxZQUFNQyxXQUFXLEdBQUcsS0FBS25CLGNBQUwsQ0FBb0IsS0FBS2IsS0FBTCxDQUFXYyxPQUEvQixFQUF3Q0MsT0FBeEMsQ0FBcEI7QUFDQWlCLE1BQUFBLFdBQVcsQ0FBQy9CLElBQVosR0FBbUIsSUFBbkI7QUFDQW1CLE1BQUFBLGtCQUFrQixHQUFHdkIsaUNBQWdCQyxHQUFoQixHQUFzQjBCLFNBQXRCLENBQWdDUSxXQUFoQyxDQUFyQjtBQUNIOztBQUNEWixJQUFBQSxrQkFBa0IsQ0FBQ2pCLElBQW5CLENBQXdCLE1BQU07QUFDMUIsV0FBS1gsa0JBQUw7QUFDSCxLQUZELEVBRUl5QyxLQUFELElBQVc7QUFDVixZQUFNQyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQiw2Q0FBMUIsRUFBeUUsRUFBekUsRUFBNkVKLFdBQTdFLEVBQTBGO0FBQ3RGSyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsNkNBQUgsQ0FEK0U7QUFFdEZDLFFBQUFBLFdBQVcsRUFBRSx5QkFBRyxzRUFBSDtBQUZ5RSxPQUExRjtBQUlILEtBUkQ7QUFTSCxHQXBIMkI7QUFzSDVCQyxFQUFBQSx5QkFBeUIsRUFBRSxVQUFTQyxLQUFULEVBQWdCO0FBQ3ZDO0FBQ0EsVUFBTUMsWUFBWSxHQUFHRCxLQUFLLENBQUNFLE1BQU4sQ0FBYUMsU0FBYixDQUF1QkMsS0FBdkIsQ0FBNkIsR0FBN0IsRUFBa0MsQ0FBbEMsQ0FBckI7QUFDQSxVQUFNQyxzQkFBc0IsR0FBR0wsS0FBSyxDQUFDRSxNQUFOLENBQWFDLFNBQWIsQ0FBdUJDLEtBQXZCLENBQTZCLEdBQTdCLEVBQWtDLENBQWxDLENBQS9COztBQUVBLFFBQUksZ0JBQWdCSCxZQUFwQixFQUFrQztBQUM5QixXQUFLSywrQkFBTCxDQUFxQ0Qsc0JBQXJDO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsWUFBTUUsSUFBSSxHQUFHLEtBQUtDLE9BQUwsQ0FBYVAsWUFBYixDQUFiOztBQUNBLFVBQUlNLElBQUosRUFBVTtBQUNOLGFBQUtFLHVCQUFMLENBQTZCRixJQUE3QixFQUFtQ0Ysc0JBQW5DO0FBQ0g7QUFDSjtBQUNKLEdBbkkyQjtBQXFJNUJLLEVBQUFBLGlCQUFpQixFQUFFLFVBQVNWLEtBQVQsRUFBZ0I7QUFDL0IsVUFBTS9DLElBQUksR0FBRyxJQUFiLENBRCtCLENBRy9COztBQUNBLFFBQUkwRCxRQUFRLEdBQUcsRUFBZjs7QUFDQSxTQUFLLE1BQU1yQyxDQUFYLElBQWdCLEtBQUtoQixLQUFMLENBQVdqQixrQkFBWCxDQUE4QkksS0FBOUMsRUFBcUQ7QUFDakQsWUFBTThELElBQUksR0FBRyxLQUFLakQsS0FBTCxDQUFXakIsa0JBQVgsQ0FBOEJJLEtBQTlCLENBQW9DNkIsQ0FBcEMsQ0FBYjtBQUNBcUMsTUFBQUEsUUFBUSxDQUFDQyxJQUFULENBQWNMLElBQUksQ0FBQ00sT0FBbkI7QUFDSDs7QUFDRCxRQUFJRixRQUFRLENBQUNwQyxNQUFiLEVBQXFCO0FBQ2pCO0FBQ0E7QUFDQW9DLE1BQUFBLFFBQVEsQ0FBQ0csSUFBVDtBQUVBSCxNQUFBQSxRQUFRLEdBQUdBLFFBQVEsQ0FBQ0ksSUFBVCxDQUFjLElBQWQsQ0FBWDtBQUNILEtBTkQsTUFNTztBQUNISixNQUFBQSxRQUFRLEdBQUcsRUFBWDtBQUNIOztBQUVELFVBQU1LLGVBQWUsR0FBR3ZCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBeEI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQixpQkFBMUIsRUFBNkMsRUFBN0MsRUFBaURvQixlQUFqRCxFQUFrRTtBQUM5RG5CLE1BQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFILENBRHVEO0FBRTlEQyxNQUFBQSxXQUFXLEVBQUUseUJBQUcsc0NBQUgsQ0FGaUQ7QUFHOURtQixNQUFBQSxNQUFNLEVBQUUseUJBQUcsSUFBSCxDQUhzRDtBQUk5REMsTUFBQUEsS0FBSyxFQUFFUCxRQUp1RDtBQUs5RFEsTUFBQUEsVUFBVSxFQUFFLFNBQVNBLFVBQVQsQ0FBb0JDLFlBQXBCLEVBQWtDQyxRQUFsQyxFQUE0QztBQUNwRCxZQUFJRCxZQUFZLElBQUlDLFFBQVEsS0FBS1YsUUFBakMsRUFBMkM7QUFDdkMsY0FBSVcsV0FBVyxHQUFHRCxRQUFRLENBQUNqQixLQUFULENBQWUsR0FBZixDQUFsQjs7QUFDQSxlQUFLLE1BQU05QixDQUFYLElBQWdCZ0QsV0FBaEIsRUFBNkI7QUFDekJBLFlBQUFBLFdBQVcsQ0FBQ2hELENBQUQsQ0FBWCxHQUFpQmdELFdBQVcsQ0FBQ2hELENBQUQsQ0FBWCxDQUFlaUQsSUFBZixFQUFqQjtBQUNILFdBSnNDLENBTXZDOzs7QUFDQUQsVUFBQUEsV0FBVyxHQUFHQSxXQUFXLENBQUNFLE1BQVosQ0FBbUIsVUFBU0MsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDdEQsZ0JBQUlBLE9BQU8sS0FBSyxFQUFaLElBQWtCRCxLQUFLLENBQUNFLE9BQU4sQ0FBY0QsT0FBZCxJQUF5QixDQUEvQyxFQUFrRDtBQUM5Q0QsY0FBQUEsS0FBSyxDQUFDYixJQUFOLENBQVdjLE9BQVg7QUFDSDs7QUFDRCxtQkFBT0QsS0FBUDtBQUNILFdBTGEsRUFLWCxFQUxXLENBQWQ7O0FBT0F4RSxVQUFBQSxJQUFJLENBQUMyRSxZQUFMLENBQWtCTixXQUFsQjtBQUNIO0FBQ0o7QUF0QjZELEtBQWxFO0FBd0JILEdBakwyQjtBQW1MNUJkLEVBQUFBLE9BQU8sRUFBRSxVQUFTUCxZQUFULEVBQXVCO0FBQzVCLFNBQUssTUFBTTNCLENBQVgsSUFBZ0IsS0FBS2hCLEtBQUwsQ0FBV2xCLGVBQTNCLEVBQTRDO0FBQ3hDLFlBQU1tRSxJQUFJLEdBQUcsS0FBS2pELEtBQUwsQ0FBV2xCLGVBQVgsQ0FBMkJrQyxDQUEzQixDQUFiOztBQUNBLFVBQUlpQyxJQUFJLENBQUNOLFlBQUwsS0FBc0JBLFlBQTFCLEVBQXdDO0FBQ3BDLGVBQU9NLElBQVA7QUFDSDtBQUNKO0FBQ0osR0ExTDJCO0FBNEw1QkUsRUFBQUEsdUJBQXVCLEVBQUUsVUFBU0YsSUFBVCxFQUFlRixzQkFBZixFQUF1QztBQUM1RCxRQUFJRSxJQUFJLElBQUlBLElBQUksQ0FBQ2pFLFdBQUwsS0FBcUIrRCxzQkFBakMsRUFBeUQ7QUFDckQsV0FBS25ELFFBQUwsQ0FBYztBQUNWakIsUUFBQUEsS0FBSyxFQUFFLEtBQUtMLE1BQUwsQ0FBWUM7QUFEVCxPQUFkO0FBSUEsWUFBTW9CLElBQUksR0FBRyxJQUFiOztBQUNBLFlBQU00RSxHQUFHLEdBQUcxRSxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsWUFBTTBFLFNBQVMsR0FBRyxFQUFsQjtBQUNBLFlBQU1DLGNBQWMsR0FBR0MsMENBQTJCekIsSUFBSSxDQUFDTixZQUFoQyxDQUF2Qjs7QUFFQSxVQUFJTSxJQUFJLENBQUNBLElBQVQsRUFBZTtBQUNYLGNBQU1qRixPQUFPLEdBQUd5RyxjQUFjLENBQUNFLG9CQUFmLENBQW9DNUIsc0JBQXBDLENBQWhCOztBQUVBLFlBQUksQ0FBQy9FLE9BQUwsRUFBYztBQUNWO0FBQ0F3RyxVQUFBQSxTQUFTLENBQUNsQixJQUFWLENBQWVpQixHQUFHLENBQUN4RSxrQkFBSixDQUF1QixRQUF2QixFQUFpQ2tELElBQUksQ0FBQ0EsSUFBTCxDQUFVaEQsSUFBM0MsRUFBaURnRCxJQUFJLENBQUNBLElBQUwsQ0FBVS9DLE9BQTNELEVBQW9FLEtBQXBFLENBQWY7QUFDSCxTQUhELE1BR087QUFDSDtBQUNBc0UsVUFBQUEsU0FBUyxDQUFDbEIsSUFBVixDQUFlLEtBQUtzQixzQkFBTCxDQUE0QjNCLElBQUksQ0FBQ0EsSUFBakMsRUFBdUNqRixPQUF2QyxFQUFnRCxJQUFoRCxDQUFmO0FBQ0g7QUFDSjs7QUFFRDZHLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZTixTQUFaLEVBQXVCckUsSUFBdkIsQ0FBNEIsWUFBVztBQUNuQ1IsUUFBQUEsSUFBSSxDQUFDSCxrQkFBTDtBQUNILE9BRkQsRUFFRyxVQUFTeUMsS0FBVCxFQUFnQjtBQUNmLGNBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBMkMsUUFBQUEsT0FBTyxDQUFDOUMsS0FBUixDQUFjLGdDQUFnQ0EsS0FBOUM7O0FBQ0FJLHVCQUFNQyxtQkFBTixDQUEwQiwyQkFBMUIsRUFBdUQsRUFBdkQsRUFBMkRKLFdBQTNELEVBQXdFO0FBQ3BFSyxVQUFBQSxLQUFLLEVBQUUseUJBQUcsMkJBQUgsQ0FENkQ7QUFFcEVDLFVBQUFBLFdBQVcsRUFBSVAsS0FBSyxJQUFJQSxLQUFLLENBQUMrQyxPQUFoQixHQUEyQi9DLEtBQUssQ0FBQytDLE9BQWpDLEdBQTJDLHlCQUFHLGtCQUFILENBRlc7QUFHcEVuQixVQUFBQSxVQUFVLEVBQUVsRSxJQUFJLENBQUNIO0FBSG1ELFNBQXhFO0FBS0gsT0FWRDtBQVdIO0FBQ0osR0EvTjJCO0FBaU81QndELEVBQUFBLCtCQUErQixFQUFFLFVBQVNELHNCQUFULEVBQWlDO0FBQzlEO0FBQ0EsUUFBSSxLQUFLL0MsS0FBTCxDQUFXakIsa0JBQVgsQ0FBOEJDLFdBQTlCLEtBQThDK0Qsc0JBQTlDLElBQ0csS0FBSy9DLEtBQUwsQ0FBV2pCLGtCQUFYLENBQThCSSxLQUE5QixDQUFvQzhCLE1BQXBDLEtBQStDLENBRHRELEVBQ3lEO0FBQ3JEO0FBQ0g7O0FBRUQsVUFBTXRCLElBQUksR0FBRyxJQUFiOztBQUNBLFVBQU00RSxHQUFHLEdBQUcxRSxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBRUEsU0FBS0YsUUFBTCxDQUFjO0FBQ1ZqQixNQUFBQSxLQUFLLEVBQUUsS0FBS0wsTUFBTCxDQUFZQztBQURULEtBQWQsRUFWOEQsQ0FjOUQ7O0FBQ0EsVUFBTWlHLFNBQVMsR0FBRyxFQUFsQjs7QUFDQSxTQUFLLE1BQU14RCxDQUFYLElBQWdCLEtBQUtoQixLQUFMLENBQVdqQixrQkFBWCxDQUE4QkksS0FBOUMsRUFBcUQ7QUFDakQsWUFBTThELElBQUksR0FBRyxLQUFLakQsS0FBTCxDQUFXakIsa0JBQVgsQ0FBOEJJLEtBQTlCLENBQW9DNkIsQ0FBcEMsQ0FBYjtBQUVBLFVBQUlpRSxPQUFKO0FBQWEsVUFBSWpILE9BQUo7O0FBQ2IsY0FBUStFLHNCQUFSO0FBQ0ksYUFBSzlELG1DQUFvQkMsRUFBekI7QUFDSSxjQUFJK0QsSUFBSSxDQUFDakYsT0FBTCxDQUFhaUQsTUFBYixLQUF3QixDQUE1QixFQUErQjtBQUMzQmpELFlBQUFBLE9BQU8sR0FBR2lCLG1DQUFvQmlHLFVBQXBCLENBQStCakcsbUNBQW9CQyxFQUFuRCxDQUFWO0FBQ0g7O0FBRUQsY0FBSSxLQUFLYyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkMsV0FBOUIsS0FBOENDLG1DQUFvQmtHLEdBQXRFLEVBQTJFO0FBQ3ZFRixZQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEOztBQUVKLGFBQUtoRyxtQ0FBb0JtRyxJQUF6QjtBQUNJLGNBQUluQyxJQUFJLENBQUNqRixPQUFMLENBQWFpRCxNQUFiLEtBQXdCLENBQTVCLEVBQStCO0FBQzNCakQsWUFBQUEsT0FBTyxHQUFHaUIsbUNBQW9CaUcsVUFBcEIsQ0FBK0JqRyxtQ0FBb0JtRyxJQUFuRCxDQUFWO0FBQ0g7O0FBRUQsY0FBSSxLQUFLcEYsS0FBTCxDQUFXakIsa0JBQVgsQ0FBOEJDLFdBQTlCLEtBQThDQyxtQ0FBb0JrRyxHQUF0RSxFQUEyRTtBQUN2RUYsWUFBQUEsT0FBTyxHQUFHLElBQVY7QUFDSDs7QUFDRDs7QUFFSixhQUFLaEcsbUNBQW9Ca0csR0FBekI7QUFDSUYsVUFBQUEsT0FBTyxHQUFHLEtBQVY7QUFDQTtBQXZCUjs7QUEwQkEsVUFBSWpILE9BQUosRUFBYTtBQUNUO0FBQ0E7QUFDQXdHLFFBQUFBLFNBQVMsQ0FBQ2xCLElBQVYsQ0FBZSxLQUFLc0Isc0JBQUwsQ0FBNEIzQixJQUE1QixFQUFrQ2pGLE9BQWxDLEVBQTJDaUgsT0FBM0MsQ0FBZjtBQUNILE9BSkQsTUFJTyxJQUFJQSxPQUFPLElBQUlwRyxTQUFmLEVBQTBCO0FBQzdCMkYsUUFBQUEsU0FBUyxDQUFDbEIsSUFBVixDQUFlaUIsR0FBRyxDQUFDeEUsa0JBQUosQ0FBdUIsUUFBdkIsRUFBaUNrRCxJQUFJLENBQUNoRCxJQUF0QyxFQUE0Q2dELElBQUksQ0FBQy9DLE9BQWpELEVBQTBEK0UsT0FBMUQsQ0FBZjtBQUNIO0FBQ0o7O0FBRURKLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZTixTQUFaLEVBQXVCckUsSUFBdkIsQ0FBNEIsVUFBU2tGLEtBQVQsRUFBZ0I7QUFDeEMxRixNQUFBQSxJQUFJLENBQUNILGtCQUFMO0FBQ0gsS0FGRCxFQUVHLFVBQVN5QyxLQUFULEVBQWdCO0FBQ2YsWUFBTUMsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EyQyxNQUFBQSxPQUFPLENBQUM5QyxLQUFSLENBQWMsOENBQThDQSxLQUE1RDs7QUFDQUkscUJBQU1DLG1CQUFOLENBQTBCLHlDQUExQixFQUFxRSxFQUFyRSxFQUF5RUosV0FBekUsRUFBc0Y7QUFDbEZLLFFBQUFBLEtBQUssRUFBRSx5QkFBRywwQ0FBSCxDQUQyRTtBQUVsRkMsUUFBQUEsV0FBVyxFQUFJUCxLQUFLLElBQUlBLEtBQUssQ0FBQytDLE9BQWhCLEdBQTJCL0MsS0FBSyxDQUFDK0MsT0FBakMsR0FBMkMseUJBQUcsa0JBQUgsQ0FGeUI7QUFHbEZuQixRQUFBQSxVQUFVLEVBQUVsRSxJQUFJLENBQUNIO0FBSGlFLE9BQXRGO0FBS0gsS0FWRDtBQVdILEdBblMyQjtBQXFTNUI4RSxFQUFBQSxZQUFZLEVBQUUsVUFBU04sV0FBVCxFQUFzQjtBQUNoQyxTQUFLcEUsUUFBTCxDQUFjO0FBQ1ZqQixNQUFBQSxLQUFLLEVBQUUsS0FBS0wsTUFBTCxDQUFZQztBQURULEtBQWQ7QUFJQSxVQUFNb0IsSUFBSSxHQUFHLElBQWI7O0FBQ0EsVUFBTTRFLEdBQUcsR0FBRzFFLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxVQUFNd0YsZUFBZSxHQUFHLEVBQXhCLENBUGdDLENBU2hDOztBQUNBLFVBQU1DLDBCQUEwQixHQUFHLEVBQW5DOztBQUNBLFNBQUssTUFBTXZFLENBQVgsSUFBZ0JyQixJQUFJLENBQUNLLEtBQUwsQ0FBV2pCLGtCQUFYLENBQThCSSxLQUE5QyxFQUFxRDtBQUNqRCxZQUFNOEQsSUFBSSxHQUFHdEQsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkksS0FBOUIsQ0FBb0M2QixDQUFwQyxDQUFiO0FBRUF1RSxNQUFBQSwwQkFBMEIsQ0FBQ2pDLElBQTNCLENBQWdDTCxJQUFJLENBQUNNLE9BQXJDOztBQUVBLFVBQUlTLFdBQVcsQ0FBQ0ssT0FBWixDQUFvQnBCLElBQUksQ0FBQ00sT0FBekIsSUFBb0MsQ0FBeEMsRUFBMkM7QUFDdkMrQixRQUFBQSxlQUFlLENBQUNoQyxJQUFoQixDQUFxQmlCLEdBQUcsQ0FBQ2lCLGNBQUosQ0FBbUIsUUFBbkIsRUFBNkJ2QyxJQUFJLENBQUNoRCxJQUFsQyxFQUF3Q2dELElBQUksQ0FBQy9DLE9BQTdDLENBQXJCO0FBQ0g7QUFDSixLQW5CK0IsQ0FxQmhDO0FBQ0E7OztBQUNBLFNBQUssTUFBTWMsQ0FBWCxJQUFnQnJCLElBQUksQ0FBQ0ssS0FBTCxDQUFXWCxvQkFBM0IsRUFBaUQ7QUFDN0MsWUFBTTRELElBQUksR0FBR3RELElBQUksQ0FBQ0ssS0FBTCxDQUFXWCxvQkFBWCxDQUFnQzJCLENBQWhDLENBQWI7O0FBRUEsVUFBSWdELFdBQVcsQ0FBQ0ssT0FBWixDQUFvQnBCLElBQUksQ0FBQ00sT0FBekIsS0FBcUMsQ0FBekMsRUFBNEM7QUFDeEMrQixRQUFBQSxlQUFlLENBQUNoQyxJQUFoQixDQUFxQmlCLEdBQUcsQ0FBQ2lCLGNBQUosQ0FBbUIsUUFBbkIsRUFBNkJ2QyxJQUFJLENBQUNoRCxJQUFsQyxFQUF3Q2dELElBQUksQ0FBQy9DLE9BQTdDLENBQXJCO0FBQ0g7QUFDSjs7QUFFRCxVQUFNdUYsT0FBTyxHQUFHLFVBQVN4RCxLQUFULEVBQWdCO0FBQzVCLFlBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBMkMsTUFBQUEsT0FBTyxDQUFDOUMsS0FBUixDQUFjLGdDQUFnQ0EsS0FBOUM7O0FBQ0FJLHFCQUFNQyxtQkFBTixDQUEwQiwyQkFBMUIsRUFBdUQsRUFBdkQsRUFBMkRKLFdBQTNELEVBQXdFO0FBQ3BFSyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsMkJBQUgsQ0FENkQ7QUFFcEVDLFFBQUFBLFdBQVcsRUFBSVAsS0FBSyxJQUFJQSxLQUFLLENBQUMrQyxPQUFoQixHQUEyQi9DLEtBQUssQ0FBQytDLE9BQWpDLEdBQTJDLHlCQUFHLGtCQUFILENBRlc7QUFHcEVuQixRQUFBQSxVQUFVLEVBQUVsRSxJQUFJLENBQUNIO0FBSG1ELE9BQXhFO0FBS0gsS0FSRCxDQS9CZ0MsQ0F5Q2hDOzs7QUFDQXFGLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZUSxlQUFaLEVBQTZCbkYsSUFBN0IsQ0FBa0MsVUFBU2tGLEtBQVQsRUFBZ0I7QUFDOUMsWUFBTWIsU0FBUyxHQUFHLEVBQWxCO0FBRUEsVUFBSWtCLHVCQUF1QixHQUFHL0YsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkMsV0FBNUQ7O0FBQ0EsVUFBSTBHLHVCQUF1QixLQUFLekcsbUNBQW9Ca0csR0FBcEQsRUFBeUQ7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFJeEYsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkksS0FBOUIsQ0FBb0M4QixNQUF4QyxFQUFnRDtBQUM1Q3lFLFVBQUFBLHVCQUF1QixHQUFHekcsbUNBQW9CMEcsMEJBQXBCLENBQStDaEcsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkksS0FBOUIsQ0FBb0MsQ0FBcEMsQ0FBL0MsQ0FBMUI7QUFDSCxTQUZELE1BRU87QUFDSDtBQUNBdUcsVUFBQUEsdUJBQXVCLEdBQUd6RyxtQ0FBb0JDLEVBQTlDO0FBQ0g7QUFDSjs7QUFFRCxXQUFLLE1BQU04QixDQUFYLElBQWdCZ0QsV0FBaEIsRUFBNkI7QUFDekIsY0FBTUksT0FBTyxHQUFHSixXQUFXLENBQUNoRCxDQUFELENBQTNCOztBQUVBLFlBQUl1RSwwQkFBMEIsQ0FBQ2xCLE9BQTNCLENBQW1DRCxPQUFuQyxJQUE4QyxDQUFsRCxFQUFxRDtBQUNqRCxjQUFJekUsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxDQUE4QkMsV0FBOUIsS0FBOENDLG1DQUFvQmtHLEdBQXRFLEVBQTJFO0FBQ3ZFWCxZQUFBQSxTQUFTLENBQUNsQixJQUFWLENBQWVpQixHQUFHLENBQUNxQixXQUFKLENBQ2QsUUFEYyxFQUNKLFNBREksRUFDT3hCLE9BRFAsRUFDZ0I7QUFDNUJwRyxjQUFBQSxPQUFPLEVBQUVpQixtQ0FBb0JpRyxVQUFwQixDQUErQlEsdUJBQS9CLENBRG1CO0FBRTVCbkMsY0FBQUEsT0FBTyxFQUFFYTtBQUZtQixhQURoQixDQUFmO0FBS0gsV0FORCxNQU1PO0FBQ0hJLFlBQUFBLFNBQVMsQ0FBQ2xCLElBQVYsQ0FBZTNELElBQUksQ0FBQ2tHLG9CQUFMLENBQTBCLFFBQTFCLEVBQW9DLFNBQXBDLEVBQStDekIsT0FBL0MsRUFBd0Q7QUFDcEVwRyxjQUFBQSxPQUFPLEVBQUVpQixtQ0FBb0JpRyxVQUFwQixDQUErQlEsdUJBQS9CLENBRDJEO0FBRXBFbkMsY0FBQUEsT0FBTyxFQUFFYTtBQUYyRCxhQUF4RCxDQUFmO0FBSUg7QUFDSjtBQUNKOztBQUVEUyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWU4sU0FBWixFQUF1QnJFLElBQXZCLENBQTRCLFVBQVNrRixLQUFULEVBQWdCO0FBQ3hDMUYsUUFBQUEsSUFBSSxDQUFDSCxrQkFBTDtBQUNILE9BRkQsRUFFR2lHLE9BRkg7QUFHSCxLQXZDRCxFQXVDR0EsT0F2Q0g7QUF3Q0gsR0F2WDJCO0FBeVg1QjtBQUNBSSxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTQyxLQUFULEVBQWdCN0YsSUFBaEIsRUFBc0I4RixNQUF0QixFQUE4QkMsSUFBOUIsRUFBb0M7QUFDdEQsVUFBTXpCLEdBQUcsR0FBRzFFLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxXQUFPeUUsR0FBRyxDQUFDcUIsV0FBSixDQUFnQkUsS0FBaEIsRUFBdUI3RixJQUF2QixFQUE2QjhGLE1BQTdCLEVBQXFDQyxJQUFyQyxFQUEyQzdGLElBQTNDLENBQWdELE1BQ25Eb0UsR0FBRyxDQUFDeEUsa0JBQUosQ0FBdUIrRixLQUF2QixFQUE4QjdGLElBQTlCLEVBQW9DOEYsTUFBcEMsRUFBNEMsS0FBNUMsQ0FERyxDQUFQO0FBR0gsR0EvWDJCO0FBaVk1QjtBQUNBO0FBQ0FFLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNDLFFBQVQsRUFBbUI7QUFDbkMsVUFBTUMsV0FBVyxHQUFHLEVBQXBCOztBQUNBLFVBQU01QixHQUFHLEdBQUcxRSxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBRUEsU0FBSyxNQUFNRyxJQUFYLElBQW1CaUcsUUFBUSxDQUFDRSxNQUE1QixFQUFvQztBQUNoQyxZQUFNQyxPQUFPLEdBQUdILFFBQVEsQ0FBQ0UsTUFBVCxDQUFnQm5HLElBQWhCLENBQWhCOztBQUNBLFdBQUssSUFBSWUsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR3FGLE9BQU8sQ0FBQ3BGLE1BQTVCLEVBQW9DLEVBQUVELENBQXRDLEVBQXlDO0FBQ3JDLGNBQU1pQyxJQUFJLEdBQUdvRCxPQUFPLENBQUNyRixDQUFELENBQXBCOztBQUNBLFlBQUlpQyxJQUFJLENBQUMvQyxPQUFMLElBQWdCcEMsWUFBcEIsRUFBa0M7QUFDOUJpSCxVQUFBQSxPQUFPLENBQUN1QixHQUFSLENBQVkscUJBQVosRUFBbUNyRCxJQUFuQztBQUNBa0QsVUFBQUEsV0FBVyxDQUFDN0MsSUFBWixDQUFrQixVQUFTckQsSUFBVCxFQUFlZ0QsSUFBZixFQUFxQjtBQUNuQyxtQkFBT3NCLEdBQUcsQ0FBQ2dDLGtCQUFKLENBQ0gsUUFERyxFQUNPdEcsSUFEUCxFQUNhbkMsWUFBWSxDQUFDbUYsSUFBSSxDQUFDL0MsT0FBTixDQUR6QixFQUN5Q25DLGlCQUFpQixDQUFDa0YsSUFBSSxDQUFDakYsT0FBTixDQUQxRCxFQUVMbUMsSUFGSyxDQUVBLE1BQ0hvRSxHQUFHLENBQUNpQixjQUFKLENBQW1CLFFBQW5CLEVBQTZCdkYsSUFBN0IsRUFBbUNnRCxJQUFJLENBQUMvQyxPQUF4QyxDQUhHLEVBSUxzRyxLQUpLLENBSUdDLENBQUQsSUFBTztBQUNaMUIsY0FBQUEsT0FBTyxDQUFDMkIsSUFBUiwyQ0FBZ0RELENBQWhEO0FBQ0gsYUFOTSxDQUFQO0FBT0gsV0FSaUIsQ0FRaEJ4RyxJQVJnQixFQVFWZ0QsSUFSVSxDQUFsQjtBQVNIO0FBQ0o7QUFDSjs7QUFFRCxRQUFJa0QsV0FBVyxDQUFDbEYsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QjtBQUNBO0FBQ0EsYUFBTzRELE9BQU8sQ0FBQ0MsR0FBUixDQUFZcUIsV0FBWixFQUF5QmhHLElBQXpCLENBQThCLE1BQ2pDb0UsR0FBRyxDQUFDb0MsWUFBSixFQURHLENBQVA7QUFHSCxLQU5ELE1BTU87QUFDSDtBQUNBLGFBQU9ULFFBQVA7QUFDSDtBQUNKLEdBcGEyQjtBQXNhNUIxRyxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCLFVBQU1HLElBQUksR0FBRyxJQUFiOztBQUNBLFVBQU1pSCxnQkFBZ0IsR0FBRy9HLGlDQUFnQkMsR0FBaEIsR0FBc0I2RyxZQUF0QixHQUFxQ3hHLElBQXJDLENBQTBDUixJQUFJLENBQUNzRyxrQkFBL0MsRUFBbUU5RixJQUFuRSxDQUF3RSxVQUFTK0YsUUFBVCxFQUFtQjtBQUNoSDtBQUNBckcsdUNBQWdCQyxHQUFoQixHQUFzQitHLFNBQXRCLEdBQWtDWCxRQUFsQyxDQUZnSCxDQUloSDs7QUFDQSxZQUFNWSxlQUFlLEdBQUc7QUFDcEI7QUFDQSwwQkFBa0IsUUFGRTtBQUlwQjtBQUNBLHlDQUFpQyxRQUxiO0FBTXBCLHNDQUE4QixRQU5WO0FBT3BCLDZCQUFxQixRQVBEO0FBUXBCLG1DQUEyQixRQVJQO0FBU3BCLDZDQUFxQyxRQVRqQjtBQVVwQiwyQkFBbUIsUUFWQztBQVdwQiw2QkFBcUIsUUFYRDtBQVlwQixpQ0FBeUIsUUFaTDtBQWFwQjtBQUNBLHdCQUFnQixRQWRJO0FBZXBCLG9DQUE0QixRQWZSO0FBZ0JwQiw2QkFBcUIsUUFoQkQsQ0FrQnBCOztBQWxCb0IsT0FBeEIsQ0FMZ0gsQ0EwQmhIOztBQUNBLFlBQU1DLFlBQVksR0FBRztBQUFDQyxRQUFBQSxNQUFNLEVBQUUsRUFBVDtBQUFhQyxRQUFBQSxNQUFNLEVBQUUsRUFBckI7QUFBeUJDLFFBQUFBLE1BQU0sRUFBRTtBQUFqQyxPQUFyQjs7QUFFQSxXQUFLLE1BQU1qSCxJQUFYLElBQW1CaUcsUUFBUSxDQUFDRSxNQUE1QixFQUFvQztBQUNoQyxhQUFLLElBQUlwRixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHbUcsTUFBTSxDQUFDQyxJQUFQLENBQVlsQixRQUFRLENBQUNFLE1BQVQsQ0FBZ0JuRyxJQUFoQixDQUFaLEVBQW1DZ0IsTUFBdkQsRUFBK0QsRUFBRUQsQ0FBakUsRUFBb0U7QUFDaEUsZ0JBQU1xRyxDQUFDLEdBQUduQixRQUFRLENBQUNFLE1BQVQsQ0FBZ0JuRyxJQUFoQixFQUFzQmUsQ0FBdEIsQ0FBVjtBQUNBLGdCQUFNc0csR0FBRyxHQUFHUixlQUFlLENBQUNPLENBQUMsQ0FBQ25ILE9BQUgsQ0FBM0I7QUFDQW1ILFVBQUFBLENBQUMsQ0FBQ3BILElBQUYsR0FBU0EsSUFBVDs7QUFFQSxjQUFJb0gsQ0FBQyxDQUFDbkgsT0FBRixDQUFVLENBQVYsTUFBaUIsR0FBckIsRUFBMEI7QUFDdEIsZ0JBQUlvSCxHQUFHLEtBQUssUUFBWixFQUFzQjtBQUNsQlAsY0FBQUEsWUFBWSxDQUFDRSxNQUFiLENBQW9CSSxDQUFDLENBQUNuSCxPQUF0QixJQUFpQ21ILENBQWpDO0FBQ0gsYUFGRCxNQUVPLElBQUlDLEdBQUcsS0FBSyxRQUFaLEVBQXNCO0FBQ3pCUCxjQUFBQSxZQUFZLENBQUNDLE1BQWIsQ0FBb0IxRCxJQUFwQixDQUF5QitELENBQXpCO0FBQ0gsYUFGTSxNQUVBO0FBQ0hOLGNBQUFBLFlBQVksQ0FBQyxRQUFELENBQVosQ0FBdUJ6RCxJQUF2QixDQUE0QitELENBQTVCO0FBQ0g7QUFDSjtBQUNKO0FBQ0osT0E3QytHLENBK0NoSDs7O0FBQ0EsVUFBSU4sWUFBWSxDQUFDQyxNQUFiLENBQW9CL0YsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDaEN0QixRQUFBQSxJQUFJLENBQUNLLEtBQUwsQ0FBV3BCLGNBQVgsR0FBNEJtSSxZQUFZLENBQUNDLE1BQWIsQ0FBb0IsQ0FBcEIsQ0FBNUI7QUFDSCxPQWxEK0csQ0FvRGhIOzs7QUFDQSxZQUFNTyxZQUFZLEdBQUdDLDRCQUFhQyxpQkFBYixDQUErQnZCLFFBQS9CLENBQXJCOztBQUNBdkcsTUFBQUEsSUFBSSxDQUFDSyxLQUFMLENBQVdqQixrQkFBWCxHQUFnQztBQUM1QkMsUUFBQUEsV0FBVyxFQUFFdUksWUFBWSxDQUFDdkksV0FERTtBQUU1QkcsUUFBQUEsS0FBSyxFQUFFb0ksWUFBWSxDQUFDcEk7QUFGUSxPQUFoQztBQUlBUSxNQUFBQSxJQUFJLENBQUNLLEtBQUwsQ0FBV1gsb0JBQVgsR0FBa0NrSSxZQUFZLENBQUNHLGFBQS9DLENBMURnSCxDQTREaEg7O0FBQ0EvSCxNQUFBQSxJQUFJLENBQUNLLEtBQUwsQ0FBV2xCLGVBQVgsR0FBNkIsRUFBN0I7QUFDQWEsTUFBQUEsSUFBSSxDQUFDSyxLQUFMLENBQVdaLGlCQUFYLEdBQStCLEVBQS9CO0FBRUEsWUFBTXVJLGFBQWEsR0FBRyxDQUNsQiwrQkFEa0IsRUFFbEIsNEJBRmtCLEVBR2xCLG1CQUhrQixFQUlsQixXQUprQixFQUtsQix5QkFMa0IsRUFNbEIsbUNBTmtCLEVBT2xCLGlCQVBrQixFQVFsQixtQkFSa0IsRUFTbEIsdUJBVGtCLEVBVWxCO0FBQ0Esb0JBWGtCLEVBWWxCLDBCQVprQixFQWFsQixtQkFia0IsQ0FBdEI7O0FBZUEsV0FBSyxNQUFNM0csQ0FBWCxJQUFnQjJHLGFBQWhCLEVBQStCO0FBQzNCLGNBQU1oRixZQUFZLEdBQUdnRixhQUFhLENBQUMzRyxDQUFELENBQWxDOztBQUVBLFlBQUkyQixZQUFZLEtBQUssV0FBckIsRUFBa0M7QUFDOUI7QUFDQTtBQUNBO0FBQ0FoRCxVQUFBQSxJQUFJLENBQUNLLEtBQUwsQ0FBV2xCLGVBQVgsQ0FBMkJ3RSxJQUEzQixDQUFnQztBQUM1Qiw0QkFBZ0IsV0FEWTtBQUU1QiwyQkFDSSwyQ0FDRSx5QkFBRywyQ0FBSCxFQUNFLEVBREYsRUFFRTtBQUFFLHNCQUFTc0UsR0FBRCxJQUNOO0FBQU0sZ0JBQUEsU0FBUyxFQUFDLCtCQUFoQjtBQUFnRCxnQkFBQSxPQUFPLEVBQUdqSSxJQUFJLENBQUN5RDtBQUEvRCxpQkFBb0Z3RSxHQUFwRjtBQURKLGFBRkYsQ0FERixDQUh3QjtBQVk1QiwyQkFBZWpJLElBQUksQ0FBQ0ssS0FBTCxDQUFXakIsa0JBQVgsQ0FBOEJDO0FBWmpCLFdBQWhDO0FBY0gsU0FsQkQsTUFrQk87QUFDSCxnQkFBTXlGLGNBQWMsR0FBR0MsMENBQTJCL0IsWUFBM0IsQ0FBdkI7QUFDQSxnQkFBTU0sSUFBSSxHQUFHOEQsWUFBWSxDQUFDRSxNQUFiLENBQW9CdEUsWUFBcEIsQ0FBYjtBQUVBLGdCQUFNM0QsV0FBVyxHQUFHeUYsY0FBYyxDQUFDb0QsaUJBQWYsQ0FBaUM1RSxJQUFqQyxDQUFwQixDQUpHLENBTUg7O0FBRUF0RCxVQUFBQSxJQUFJLENBQUNLLEtBQUwsQ0FBV2xCLGVBQVgsQ0FBMkJ3RSxJQUEzQixDQUFnQztBQUM1Qiw0QkFBZ0JYLFlBRFk7QUFFNUIsMkJBQWUseUJBQUc4QixjQUFjLENBQUNqQyxXQUFsQixDQUZhO0FBRW1CO0FBQy9DLG9CQUFRUyxJQUhvQjtBQUk1QiwyQkFBZWpFO0FBSmEsV0FBaEMsRUFSRyxDQWVIOztBQUNBLGNBQUlpRSxJQUFJLElBQUksQ0FBQ2pFLFdBQWIsRUFBMEI7QUFDdEJpRSxZQUFBQSxJQUFJLENBQUNULFdBQUwsR0FBbUJpQyxjQUFjLENBQUNqQyxXQUFsQztBQUNBN0MsWUFBQUEsSUFBSSxDQUFDSyxLQUFMLENBQVdaLGlCQUFYLENBQTZCa0UsSUFBN0IsQ0FBa0NMLElBQWxDO0FBQ0g7QUFDSjtBQUNKLE9BekgrRyxDQTJIaEg7OztBQUNBLFlBQU02RSxzQkFBc0IsR0FBRztBQUMzQiwyQkFBbUIseUJBQUcscUNBQUgsQ0FEUTtBQUUzQiw0QkFBb0IseUJBQUcsNkJBQUg7QUFGTyxPQUEvQjs7QUFLQSxXQUFLLE1BQU05RyxDQUFYLElBQWdCK0YsWUFBWSxDQUFDRyxNQUE3QixFQUFxQztBQUNqQyxjQUFNakUsSUFBSSxHQUFHOEQsWUFBWSxDQUFDRyxNQUFiLENBQW9CbEcsQ0FBcEIsQ0FBYjtBQUNBLGNBQU0rRyxlQUFlLEdBQUdELHNCQUFzQixDQUFDN0UsSUFBSSxDQUFDL0MsT0FBTixDQUE5QyxDQUZpQyxDQUlqQzs7QUFDQSxZQUFJNkgsZUFBZSxJQUFJOUUsSUFBSSxDQUFDZ0MsT0FBeEIsSUFBbUMsQ0FBQ2hDLElBQUksQ0FBQytFLE9BQTdDLEVBQXNEO0FBQ2xEL0UsVUFBQUEsSUFBSSxDQUFDVCxXQUFMLEdBQW1CdUYsZUFBbkI7QUFDQXBJLFVBQUFBLElBQUksQ0FBQ0ssS0FBTCxDQUFXWixpQkFBWCxDQUE2QmtFLElBQTdCLENBQWtDTCxJQUFsQztBQUNIO0FBQ0o7QUFDSixLQTNJd0IsQ0FBekI7O0FBNklBLFVBQU1nRixjQUFjLEdBQUdwSSxpQ0FBZ0JDLEdBQWhCLEdBQXNCb0ksVUFBdEIsR0FBbUMvSCxJQUFuQyxDQUF3QyxVQUFTZ0ksSUFBVCxFQUFlO0FBQzFFeEksTUFBQUEsSUFBSSxDQUFDQyxRQUFMLENBQWM7QUFBQ2tCLFFBQUFBLE9BQU8sRUFBRXFILElBQUksQ0FBQ3JIO0FBQWYsT0FBZDtBQUNILEtBRnNCLENBQXZCOztBQUlBK0QsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksQ0FBQzhCLGdCQUFELEVBQW1CcUIsY0FBbkIsQ0FBWixFQUFnRDlILElBQWhELENBQXFELFlBQVc7QUFDNURSLE1BQUFBLElBQUksQ0FBQ0MsUUFBTCxDQUFjO0FBQ1ZqQixRQUFBQSxLQUFLLEVBQUVnQixJQUFJLENBQUNyQixNQUFMLENBQVlFO0FBRFQsT0FBZDtBQUdILEtBSkQsRUFJRyxVQUFTeUQsS0FBVCxFQUFnQjtBQUNmOEMsTUFBQUEsT0FBTyxDQUFDOUMsS0FBUixDQUFjQSxLQUFkO0FBQ0F0QyxNQUFBQSxJQUFJLENBQUNDLFFBQUwsQ0FBYztBQUNWakIsUUFBQUEsS0FBSyxFQUFFZ0IsSUFBSSxDQUFDckIsTUFBTCxDQUFZRztBQURULE9BQWQ7QUFHSCxLQVRELEVBU0dnQyxPQVRILENBU1csTUFBTTtBQUNiO0FBQ0FkLE1BQUFBLElBQUksQ0FBQ0MsUUFBTCxDQUFjO0FBQ1ZoQixRQUFBQSxjQUFjLEVBQUVlLElBQUksQ0FBQ0ssS0FBTCxDQUFXcEIsY0FEakI7QUFFVkcsUUFBQUEsa0JBQWtCLEVBQUVZLElBQUksQ0FBQ0ssS0FBTCxDQUFXakIsa0JBRnJCO0FBR1ZELFFBQUFBLGVBQWUsRUFBRWEsSUFBSSxDQUFDSyxLQUFMLENBQVdsQixlQUhsQjtBQUlWTyxRQUFBQSxvQkFBb0IsRUFBRU0sSUFBSSxDQUFDSyxLQUFMLENBQVdYLG9CQUp2QjtBQUtWRCxRQUFBQSxpQkFBaUIsRUFBRU8sSUFBSSxDQUFDSyxLQUFMLENBQVdaO0FBTHBCLE9BQWQ7QUFPSCxLQWxCRDs7QUFvQkFTLHFDQUFnQkMsR0FBaEIsR0FBc0JzSSxZQUF0QixHQUFxQ2pJLElBQXJDLENBQTJDa0gsQ0FBRCxJQUFPLEtBQUt6SCxRQUFMLENBQWM7QUFBQ04sTUFBQUEsU0FBUyxFQUFFK0gsQ0FBQyxDQUFDL0g7QUFBZCxLQUFkLENBQWpEO0FBQ0gsR0E5a0IyQjtBQWdsQjVCK0ksRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixVQUFNOUQsR0FBRyxHQUFHMUUsaUNBQWdCQyxHQUFoQixFQUFaOztBQUVBeUUsSUFBQUEsR0FBRyxDQUFDK0QsUUFBSixHQUFlQyxPQUFmLENBQXVCbEIsQ0FBQyxJQUFJO0FBQ3hCLFVBQUlBLENBQUMsQ0FBQ21CLDBCQUFGLEtBQWlDLENBQXJDLEVBQXdDO0FBQ3BDLGNBQU1DLE1BQU0sR0FBR3BCLENBQUMsQ0FBQ3FCLGVBQUYsR0FBb0JDLFNBQXBCLEVBQWY7QUFDQSxZQUFJRixNQUFNLENBQUN4SCxNQUFYLEVBQW1Cc0QsR0FBRyxDQUFDcUUsZUFBSixDQUFvQkgsTUFBTSxDQUFDSSxHQUFQLEVBQXBCO0FBQ3RCO0FBQ0osS0FMRDtBQU1ILEdBemxCMkI7QUEybEI1QmpFLEVBQUFBLHNCQUFzQixFQUFFLFVBQVMzQixJQUFULEVBQWVqRixPQUFmLEVBQXdCaUgsT0FBeEIsRUFBaUM7QUFDckQsVUFBTVYsR0FBRyxHQUFHMUUsaUNBQWdCQyxHQUFoQixFQUFaOztBQUVBLFdBQU95RSxHQUFHLENBQUNnQyxrQkFBSixDQUNILFFBREcsRUFDT3RELElBQUksQ0FBQ2hELElBRFosRUFDa0JnRCxJQUFJLENBQUMvQyxPQUR2QixFQUNnQ2xDLE9BRGhDLEVBRUxtQyxJQUZLLENBRUMsWUFBVztBQUNmO0FBQ0EsVUFBSXRCLFNBQVMsSUFBSW9HLE9BQWpCLEVBQTBCO0FBQ3RCLGVBQU9WLEdBQUcsQ0FBQ3hFLGtCQUFKLENBQ0gsUUFERyxFQUNPa0QsSUFBSSxDQUFDaEQsSUFEWixFQUNrQmdELElBQUksQ0FBQy9DLE9BRHZCLEVBQ2dDK0UsT0FEaEMsQ0FBUDtBQUdIO0FBQ0osS0FUTSxDQUFQO0FBVUgsR0F4bUIyQjtBQTBtQjVCNkQsRUFBQUEsd0JBQXdCLEVBQUUsVUFBU3ZHLEtBQVQsRUFBZ0JNLFNBQWhCLEVBQTJCa0csbUJBQTNCLEVBQWdEO0FBQ3RFLFdBQ0k7QUFBSSxNQUFBLEdBQUcsRUFBR2xHO0FBQVYsT0FDSSx5Q0FDTU4sS0FETixDQURKLEVBS0kseUNBQ0k7QUFBTyxNQUFBLFNBQVMsRUFBR00sU0FBUyxHQUFHLEdBQVosR0FBa0I1RCxtQ0FBb0JrRyxHQUF6RDtBQUNJLE1BQUEsSUFBSSxFQUFDLE9BRFQ7QUFFSSxNQUFBLE9BQU8sRUFBRzRELG1CQUFtQixLQUFLOUosbUNBQW9Ca0csR0FGMUQ7QUFHSSxNQUFBLFFBQVEsRUFBRyxLQUFLMUM7QUFIcEIsTUFESixDQUxKLEVBWUkseUNBQ0k7QUFBTyxNQUFBLFNBQVMsRUFBR0ksU0FBUyxHQUFHLEdBQVosR0FBa0I1RCxtQ0FBb0JDLEVBQXpEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsT0FEVDtBQUVJLE1BQUEsT0FBTyxFQUFHNkosbUJBQW1CLEtBQUs5SixtQ0FBb0JDLEVBRjFEO0FBR0ksTUFBQSxRQUFRLEVBQUcsS0FBS3VEO0FBSHBCLE1BREosQ0FaSixFQW1CSSx5Q0FDSTtBQUFPLE1BQUEsU0FBUyxFQUFHSSxTQUFTLEdBQUcsR0FBWixHQUFrQjVELG1DQUFvQm1HLElBQXpEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsT0FEVDtBQUVJLE1BQUEsT0FBTyxFQUFHMkQsbUJBQW1CLEtBQUs5SixtQ0FBb0JtRyxJQUYxRDtBQUdJLE1BQUEsUUFBUSxFQUFHLEtBQUszQztBQUhwQixNQURKLENBbkJKLENBREo7QUE0QkgsR0F2b0IyQjtBQXlvQjVCdUcsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxVQUFNQyxJQUFJLEdBQUcsRUFBYjs7QUFDQSxTQUFLLE1BQU1qSSxDQUFYLElBQWdCLEtBQUtoQixLQUFMLENBQVdsQixlQUEzQixFQUE0QztBQUN4QyxZQUFNbUUsSUFBSSxHQUFHLEtBQUtqRCxLQUFMLENBQVdsQixlQUFYLENBQTJCa0MsQ0FBM0IsQ0FBYjs7QUFDQSxVQUFJaUMsSUFBSSxDQUFDQSxJQUFMLEtBQWNwRSxTQUFkLElBQTJCb0UsSUFBSSxDQUFDTixZQUFMLENBQWtCdUcsVUFBbEIsQ0FBNkIsS0FBN0IsQ0FBL0IsRUFBb0U7QUFDaEVuRSxRQUFBQSxPQUFPLENBQUMyQixJQUFSLG1DQUF3Q3pELElBQUksQ0FBQ04sWUFBN0M7QUFDQTtBQUNILE9BTHVDLENBTXhDOzs7QUFDQXNHLE1BQUFBLElBQUksQ0FBQzNGLElBQUwsQ0FBVSxLQUFLd0Ysd0JBQUwsQ0FBOEI3RixJQUFJLENBQUNULFdBQW5DLEVBQWdEUyxJQUFJLENBQUNOLFlBQXJELEVBQW1FTSxJQUFJLENBQUNqRSxXQUF4RSxDQUFWO0FBQ0g7O0FBQ0QsV0FBT2lLLElBQVA7QUFDSCxHQXJwQjJCO0FBdXBCNUJFLEVBQUFBLGNBQWMsRUFBRSxVQUFTckksT0FBVCxFQUFrQkMsT0FBbEIsRUFBMkI7QUFDdkMsUUFBSUQsT0FBTyxLQUFLakMsU0FBaEIsRUFBMkI7QUFDdkIsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsU0FBSyxJQUFJbUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsT0FBTyxDQUFDRyxNQUE1QixFQUFvQyxFQUFFRCxDQUF0QyxFQUF5QztBQUNyQyxVQUFJRixPQUFPLENBQUNFLENBQUQsQ0FBUCxDQUFXZixJQUFYLEtBQW9CLE9BQXBCLElBQStCYSxPQUFPLENBQUNFLENBQUQsQ0FBUCxDQUFXRSxPQUFYLEtBQXVCSCxPQUExRCxFQUFtRTtBQUMvRCxlQUFPLElBQVA7QUFDSDtBQUNKOztBQUNELFdBQU8sS0FBUDtBQUNILEdBanFCMkI7QUFtcUI1QnFJLEVBQUFBLHFCQUFxQixFQUFFLFVBQVNySSxPQUFULEVBQWtCc0ksS0FBbEIsRUFBeUI7QUFDNUMsV0FBTyw2QkFBQyw2QkFBRDtBQUFzQixNQUFBLEtBQUssRUFBRSxLQUFLRixjQUFMLENBQW9CLEtBQUtuSixLQUFMLENBQVdjLE9BQS9CLEVBQXdDQyxPQUF4QyxDQUE3QjtBQUNzQixNQUFBLFFBQVEsRUFBRSxLQUFLSSxnQ0FBTCxDQUFzQ21JLElBQXRDLENBQTJDLElBQTNDLEVBQWlEdkksT0FBakQsQ0FEaEM7QUFFc0IsTUFBQSxLQUFLLEVBQUVzSSxLQUY3QjtBQUVvQyxNQUFBLEdBQUcsdUJBQWdCQSxLQUFoQjtBQUZ2QyxNQUFQO0FBR0gsR0F2cUIyQjtBQXlxQjVCRSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFFBQUlDLE9BQUo7O0FBQ0EsUUFBSSxLQUFLeEosS0FBTCxDQUFXckIsS0FBWCxLQUFxQixLQUFLTCxNQUFMLENBQVlDLE9BQXJDLEVBQThDO0FBQzFDLFlBQU1rTCxNQUFNLEdBQUd0SCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWY7QUFDQW9ILE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxNQUFELE9BQVY7QUFDSDs7QUFFRCxRQUFJRSxpQkFBSjs7QUFDQSxRQUFJLEtBQUsxSixLQUFMLENBQVdwQixjQUFmLEVBQStCO0FBQzNCOEssTUFBQUEsaUJBQWlCLEdBQUcsNkJBQUMsNkJBQUQ7QUFBc0IsUUFBQSxLQUFLLEVBQUUsQ0FBQyxLQUFLMUosS0FBTCxDQUFXcEIsY0FBWCxDQUEwQnFHLE9BQXhEO0FBQ3NCLFFBQUEsUUFBUSxFQUFFLEtBQUt4RiwyQkFEckM7QUFFc0IsUUFBQSxLQUFLLEVBQUUseUJBQUcsdUNBQUg7QUFGN0IsUUFBcEI7QUFHSDs7QUFFRCxRQUFJa0ssd0JBQUo7O0FBQ0EsUUFBSTlKLGlDQUFnQkMsR0FBaEIsR0FBc0J3SSxRQUF0QixHQUFpQ3NCLElBQWpDLENBQXNDdkMsQ0FBQyxJQUFJQSxDQUFDLENBQUNtQiwwQkFBRixLQUFpQyxDQUE1RSxDQUFKLEVBQW9GO0FBQ2hGbUIsTUFBQUEsd0JBQXdCLEdBQUcsNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUUsS0FBS3RCLHFCQUFoQztBQUF1RCxRQUFBLElBQUksRUFBQztBQUE1RCxTQUN0Qix5QkFBRyxxQkFBSCxDQURzQixDQUEzQjtBQUdILEtBbkJjLENBcUJmO0FBQ0E7OztBQUNBLFFBQUksS0FBS3JJLEtBQUwsQ0FBV3BCLGNBQVgsSUFBNkIsS0FBS29CLEtBQUwsQ0FBV3BCLGNBQVgsQ0FBMEJxRyxPQUEzRCxFQUFvRTtBQUNoRSxhQUNJLDBDQUNLeUUsaUJBREwsRUFHSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDTSx5QkFBRywyREFBSCxDQUROLENBSEosRUFPS0Msd0JBUEwsQ0FESjtBQVdIOztBQUVELFVBQU1FLGNBQWMsR0FBRyxLQUFLN0osS0FBTCxDQUFXVixTQUFYLENBQXFCd0ssTUFBckIsQ0FBNkJDLEVBQUQsSUFBUUEsRUFBRSxDQUFDQyxNQUFILEtBQWMsT0FBbEQsQ0FBdkI7QUFDQSxRQUFJQyxzQkFBSjs7QUFDQSxRQUFJSixjQUFjLENBQUM1SSxNQUFmLEtBQTBCLENBQTlCLEVBQWlDO0FBQzdCZ0osTUFBQUEsc0JBQXNCLEdBQUcsMENBQ25CLHlCQUFHLHVEQUFILENBRG1CLENBQXpCO0FBR0gsS0FKRCxNQUlPO0FBQ0hBLE1BQUFBLHNCQUFzQixHQUFHSixjQUFjLENBQUNLLEdBQWYsQ0FBb0JDLFFBQUQsSUFBYyxLQUFLZixxQkFBTCxDQUN0RGUsUUFBUSxDQUFDcEosT0FENkMsWUFDakMseUJBQUcsNEJBQUgsQ0FEaUMsZUFDSW9KLFFBQVEsQ0FBQ3BKLE9BRGIsT0FBakMsQ0FBekI7QUFHSCxLQS9DYyxDQWlEZjs7O0FBQ0EsVUFBTTJHLGFBQWEsR0FBRyxFQUF0Qjs7QUFDQSxTQUFLLE1BQU0xRyxDQUFYLElBQWdCLEtBQUtoQixLQUFMLENBQVdaLGlCQUEzQixFQUE4QztBQUMxQyxZQUFNNkQsSUFBSSxHQUFHLEtBQUtqRCxLQUFMLENBQVdaLGlCQUFYLENBQTZCNEIsQ0FBN0IsQ0FBYjtBQUNBMEcsTUFBQUEsYUFBYSxDQUFDcEUsSUFBZCxDQUFtQix5Q0FBTSx5QkFBR0wsSUFBSSxDQUFDVCxXQUFSLENBQU4sQ0FBbkI7QUFDSCxLQXREYyxDQXdEZjs7O0FBQ0EsUUFBSTRILGdCQUFnQixHQUFHLEVBQXZCOztBQUNBLFNBQUssTUFBTXBKLENBQVgsSUFBZ0IsS0FBS2hCLEtBQUwsQ0FBV1gsb0JBQTNCLEVBQWlEO0FBQzdDLFlBQU00RCxJQUFJLEdBQUcsS0FBS2pELEtBQUwsQ0FBV1gsb0JBQVgsQ0FBZ0MyQixDQUFoQyxDQUFiO0FBQ0FvSixNQUFBQSxnQkFBZ0IsQ0FBQzlHLElBQWpCLENBQXNCTCxJQUFJLENBQUNNLE9BQTNCO0FBQ0g7O0FBQ0QsUUFBSTZHLGdCQUFnQixDQUFDbkosTUFBckIsRUFBNkI7QUFDekJtSixNQUFBQSxnQkFBZ0IsR0FBR0EsZ0JBQWdCLENBQUMzRyxJQUFqQixDQUFzQixJQUF0QixDQUFuQjtBQUNBaUUsTUFBQUEsYUFBYSxDQUFDcEUsSUFBZCxDQUFtQix5Q0FBTSx5QkFBRyxxRkFBSCxDQUFOLE9BQW9HOEcsZ0JBQXBHLENBQW5CO0FBQ0g7O0FBRUQsUUFBSUMsY0FBSjs7QUFDQSxRQUFJLEtBQUtySyxLQUFMLENBQVdjLE9BQVgsS0FBdUJqQyxTQUEzQixFQUFzQztBQUNsQ3dMLE1BQUFBLGNBQWMsR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBeUIseUJBQUcsMENBQUgsQ0FBekIsQ0FBakI7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLckssS0FBTCxDQUFXYyxPQUFYLENBQW1CRyxNQUFuQixLQUE4QixDQUFsQyxFQUFxQztBQUN4Q29KLE1BQUFBLGNBQWMsR0FBRyxJQUFqQjtBQUNILEtBRk0sTUFFQTtBQUNIO0FBQ0E7QUFDQSxZQUFNcEIsSUFBSSxHQUFHLEVBQWI7O0FBQ0EsV0FBSyxJQUFJakksQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxLQUFLaEIsS0FBTCxDQUFXYyxPQUFYLENBQW1CRyxNQUF2QyxFQUErQyxFQUFFRCxDQUFqRCxFQUFvRDtBQUNoRGlJLFFBQUFBLElBQUksQ0FBQzNGLElBQUwsQ0FBVTtBQUFJLFVBQUEsR0FBRyxFQUFHdEM7QUFBVixXQUNOLHlDQUFLLEtBQUtoQixLQUFMLENBQVdjLE9BQVgsQ0FBbUJFLENBQW5CLEVBQXNCVSxnQkFBM0IsQ0FETSxFQUVOLHlDQUFLLEtBQUsxQixLQUFMLENBQVdjLE9BQVgsQ0FBbUJFLENBQW5CLEVBQXNCVyxtQkFBM0IsQ0FGTSxDQUFWO0FBSUg7O0FBQ0QwSSxNQUFBQSxjQUFjLEdBQUk7QUFBTyxRQUFBLFNBQVMsRUFBQztBQUFqQixTQUNkLDRDQUNLcEIsSUFETCxDQURjLENBQWxCO0FBS0g7O0FBQ0QsUUFBSW9CLGNBQUosRUFBb0I7QUFDaEJBLE1BQUFBLGNBQWMsR0FBSSwwQ0FDZCx5Q0FBTSx5QkFBRyxzQkFBSCxDQUFOLENBRGMsRUFFWkEsY0FGWSxDQUFsQjtBQUlIOztBQUVELFFBQUlDLGdCQUFKOztBQUNBLFFBQUk1QyxhQUFhLENBQUN6RyxNQUFsQixFQUEwQjtBQUN0QnFKLE1BQUFBLGdCQUFnQixHQUNaLDBDQUNJLHlDQUFNLHlCQUFHLGdDQUFILENBQU4sQ0FESixFQUVNLHlCQUFHLDJEQUFILENBRk4sT0FFd0Usd0NBRnhFLEVBR00seUJBQUcsK0dBQUgsQ0FITixPQUlJLHlDQUNNNUMsYUFETixDQUpKLENBREo7QUFVSDs7QUFFRCxXQUNJLDBDQUVLZ0MsaUJBRkwsRUFJSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FFTUYsT0FGTixFQUlJLDZCQUFDLDZCQUFEO0FBQXNCLE1BQUEsS0FBSyxFQUFFbkosdUJBQWNrSyxRQUFkLENBQXVCLHNCQUF2QixDQUE3QjtBQUNzQixNQUFBLFFBQVEsRUFBRSxLQUFLbkssa0NBRHJDO0FBRXNCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLCtDQUFIO0FBRjdCLE1BSkosRUFRSSw2QkFBQyw2QkFBRDtBQUFzQixNQUFBLEtBQUssRUFBRUMsdUJBQWNrSyxRQUFkLENBQXVCLHlCQUF2QixDQUE3QjtBQUNzQixNQUFBLFFBQVEsRUFBRSxLQUFLNUoscUNBRHJDO0FBRXNCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLHNDQUFIO0FBRjdCLE1BUkosRUFZSSw2QkFBQyw2QkFBRDtBQUFzQixNQUFBLEtBQUssRUFBRU4sdUJBQWNrSyxRQUFkLENBQXVCLDJCQUF2QixDQUE3QjtBQUNzQixNQUFBLFFBQVEsRUFBRSxLQUFLM0osZ0NBRHJDO0FBRXNCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLCtDQUFIO0FBRjdCLE1BWkosRUFnQk1xSixzQkFoQk4sRUFrQkk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTyxNQUFBLFNBQVMsRUFBQztBQUFqQixPQUNJLDRDQUNJLHlDQUNJO0FBQUksTUFBQSxLQUFLLEVBQUM7QUFBVixNQURKLEVBRUk7QUFBSSxNQUFBLEtBQUssRUFBQztBQUFWLE9BQWtCLHlCQUFHLEtBQUgsQ0FBbEIsQ0FGSixFQUdJO0FBQUksTUFBQSxLQUFLLEVBQUM7QUFBVixPQUFrQix5QkFBRyxJQUFILENBQWxCLENBSEosRUFJSTtBQUFJLE1BQUEsS0FBSyxFQUFDO0FBQVYsT0FBa0IseUJBQUcsT0FBSCxDQUFsQixDQUpKLENBREosQ0FESixFQVNJLDRDQUVNLEtBQUtqQix5QkFBTCxFQUZOLENBVEosQ0FESixDQWxCSixFQW9DTXNCLGdCQXBDTixFQXNDTUQsY0F0Q04sRUF3Q01WLHdCQXhDTixDQUpKLENBREo7QUFrREg7QUF4MEIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSAnLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZSc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCB7XHJcbiAgICBOb3RpZmljYXRpb25VdGlscyxcclxuICAgIFZlY3RvclB1c2hSdWxlc0RlZmluaXRpb25zLFxyXG4gICAgUHVzaFJ1bGVWZWN0b3JTdGF0ZSxcclxuICAgIENvbnRlbnRSdWxlcyxcclxufSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb25zJztcclxuaW1wb3J0IFNka0NvbmZpZyBmcm9tIFwiLi4vLi4vLi4vU2RrQ29uZmlnXCI7XHJcbmltcG9ydCBMYWJlbGxlZFRvZ2dsZVN3aXRjaCBmcm9tIFwiLi4vZWxlbWVudHMvTGFiZWxsZWRUb2dnbGVTd2l0Y2hcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuXHJcbi8vIFRPRE86IHRoaXMgXCJ2aWV3XCIgY29tcG9uZW50IHN0aWxsIGhhcyBmYXIgdG9vIG11Y2ggYXBwbGljYXRpb24gbG9naWMgaW4gaXQsXHJcbi8vIHdoaWNoIHNob3VsZCBiZSBmYWN0b3JlZCBvdXQgdG8gb3RoZXIgZmlsZXMuXHJcblxyXG4vLyBUT0RPOiB0aGlzIGNvbXBvbmVudCBhbHNvIGRvZXMgYSBsb3Qgb2YgZGlyZWN0IHBva2luZyBpbnRvIHRoaXMuc3RhdGUsIHdoaWNoXHJcbi8vIGlzIFZFUlkgTkFVR0hUWS5cclxuXHJcblxyXG4vKipcclxuICogUnVsZXMgdGhhdCBWZWN0b3IgdXNlZCB0byBzZXQgaW4gb3JkZXIgdG8gb3ZlcnJpZGUgdGhlIGFjdGlvbnMgb2YgZGVmYXVsdCBydWxlcy5cclxuICogVGhlc2UgYXJlIHVzZWQgdG8gcG9ydCBwZW9wbGVzIGV4aXN0aW5nIG92ZXJyaWRlcyB0byBtYXRjaCB0aGUgY3VycmVudCBBUEkuXHJcbiAqIFRoZXNlIGNhbiBiZSByZW1vdmVkIGFuZCBmb3Jnb3R0ZW4gb25jZSBldmVyeW9uZSBoYXMgbW92ZWQgdG8gdGhlIG5ldyBjbGllbnQuXHJcbiAqL1xyXG5jb25zdCBMRUdBQ1lfUlVMRVMgPSB7XHJcbiAgICBcImltLnZlY3Rvci5ydWxlLmNvbnRhaW5zX2Rpc3BsYXlfbmFtZVwiOiBcIi5tLnJ1bGUuY29udGFpbnNfZGlzcGxheV9uYW1lXCIsXHJcbiAgICBcImltLnZlY3Rvci5ydWxlLnJvb21fb25lX3RvX29uZVwiOiBcIi5tLnJ1bGUucm9vbV9vbmVfdG9fb25lXCIsXHJcbiAgICBcImltLnZlY3Rvci5ydWxlLnJvb21fbWVzc2FnZVwiOiBcIi5tLnJ1bGUubWVzc2FnZVwiLFxyXG4gICAgXCJpbS52ZWN0b3IucnVsZS5pbnZpdGVfZm9yX21lXCI6IFwiLm0ucnVsZS5pbnZpdGVfZm9yX21lXCIsXHJcbiAgICBcImltLnZlY3Rvci5ydWxlLmNhbGxcIjogXCIubS5ydWxlLmNhbGxcIixcclxuICAgIFwiaW0udmVjdG9yLnJ1bGUubm90aWNlc1wiOiBcIi5tLnJ1bGUuc3VwcHJlc3Nfbm90aWNlc1wiLFxyXG59O1xyXG5cclxuZnVuY3Rpb24gcG9ydExlZ2FjeUFjdGlvbnMoYWN0aW9ucykge1xyXG4gICAgY29uc3QgZGVjb2RlZCA9IE5vdGlmaWNhdGlvblV0aWxzLmRlY29kZUFjdGlvbnMoYWN0aW9ucyk7XHJcbiAgICBpZiAoZGVjb2RlZCAhPT0gbnVsbCkge1xyXG4gICAgICAgIHJldHVybiBOb3RpZmljYXRpb25VdGlscy5lbmNvZGVBY3Rpb25zKGRlY29kZWQpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBXZSBkb24ndCByZWNvZ25pc2Ugb25lIG9mIHRoZSBhY3Rpb25zIGhlcmUsIHNvIHdlIGRvbid0IHRyeSB0b1xyXG4gICAgICAgIC8vIGNhbm9uaWNhbGlzZSB0aGVtLlxyXG4gICAgICAgIHJldHVybiBhY3Rpb25zO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnTm90aWZpY2F0aW9ucycsXHJcblxyXG4gICAgcGhhc2VzOiB7XHJcbiAgICAgICAgTE9BRElORzogXCJMT0FESU5HXCIsIC8vIFRoZSBjb21wb25lbnQgaXMgbG9hZGluZyBvciBzZW5kaW5nIGRhdGEgdG8gdGhlIGhzXHJcbiAgICAgICAgRElTUExBWTogXCJESVNQTEFZXCIsIC8vIFRoZSBjb21wb25lbnQgaXMgcmVhZHkgYW5kIGRpc3BsYXkgZGF0YVxyXG4gICAgICAgIEVSUk9SOiBcIkVSUk9SXCIsIC8vIFRoZXJlIHdhcyBhbiBlcnJvclxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHBoYXNlOiB0aGlzLnBoYXNlcy5MT0FESU5HLFxyXG4gICAgICAgICAgICBtYXN0ZXJQdXNoUnVsZTogdW5kZWZpbmVkLCAvLyBUaGUgbWFzdGVyIHJ1bGUgKCcubS5ydWxlLm1hc3RlcicpXHJcbiAgICAgICAgICAgIHZlY3RvclB1c2hSdWxlczogW10sIC8vIEhTIGRlZmF1bHQgcHVzaCBydWxlcyBkaXNwbGF5ZWQgaW4gVmVjdG9yIFVJXHJcbiAgICAgICAgICAgIHZlY3RvckNvbnRlbnRSdWxlczogeyAvLyBLZXl3b3JkIHB1c2ggcnVsZXMgZGlzcGxheWVkIGluIFZlY3RvciBVSVxyXG4gICAgICAgICAgICAgICAgdmVjdG9yU3RhdGU6IFB1c2hSdWxlVmVjdG9yU3RhdGUuT04sXHJcbiAgICAgICAgICAgICAgICBydWxlczogW10sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGV4dGVybmFsUHVzaFJ1bGVzOiBbXSwgLy8gUHVzaCBydWxlcyAoZXhjZXB0IGNvbnRlbnQgcnVsZSkgdGhhdCBoYXZlIGJlZW4gZGVmaW5lZCBvdXRzaWRlIFZlY3RvciBVSVxyXG4gICAgICAgICAgICBleHRlcm5hbENvbnRlbnRSdWxlczogW10sIC8vIEtleXdvcmQgcHVzaCBydWxlcyB0aGF0IGhhdmUgYmVlbiBkZWZpbmVkIG91dHNpZGUgVmVjdG9yIFVJXHJcbiAgICAgICAgICAgIHRocmVlcGlkczogW10sIC8vIHVzZWQgZm9yIGVtYWlsIG5vdGlmaWNhdGlvbnNcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fcmVmcmVzaEZyb21TZXJ2ZXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FbmFibGVOb3RpZmljYXRpb25zQ2hhbmdlOiBmdW5jdGlvbihjaGVja2VkKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBoYXNlOiB0aGlzLnBoYXNlcy5MT0FESU5HLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0UHVzaFJ1bGVFbmFibGVkKCdnbG9iYWwnLCBzZWxmLnN0YXRlLm1hc3RlclB1c2hSdWxlLmtpbmQsIHNlbGYuc3RhdGUubWFzdGVyUHVzaFJ1bGUucnVsZV9pZCwgIWNoZWNrZWQpLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgc2VsZi5fcmVmcmVzaEZyb21TZXJ2ZXIoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FbmFibGVEZXNrdG9wTm90aWZpY2F0aW9uc0NoYW5nZTogZnVuY3Rpb24oY2hlY2tlZCkge1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXHJcbiAgICAgICAgICAgIFwibm90aWZpY2F0aW9uc0VuYWJsZWRcIiwgbnVsbCxcclxuICAgICAgICAgICAgU2V0dGluZ0xldmVsLkRFVklDRSxcclxuICAgICAgICAgICAgY2hlY2tlZCxcclxuICAgICAgICApLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRW5hYmxlRGVza3RvcE5vdGlmaWNhdGlvbkJvZHlDaGFuZ2U6IGZ1bmN0aW9uKGNoZWNrZWQpIHtcclxuICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFxyXG4gICAgICAgICAgICBcIm5vdGlmaWNhdGlvbkJvZHlFbmFibGVkXCIsIG51bGwsXHJcbiAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsXHJcbiAgICAgICAgICAgIGNoZWNrZWQsXHJcbiAgICAgICAgKS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkVuYWJsZUF1ZGlvTm90aWZpY2F0aW9uc0NoYW5nZTogZnVuY3Rpb24oY2hlY2tlZCkge1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXHJcbiAgICAgICAgICAgIFwiYXVkaW9Ob3RpZmljYXRpb25zRW5hYmxlZFwiLCBudWxsLFxyXG4gICAgICAgICAgICBTZXR0aW5nTGV2ZWwuREVWSUNFLFxyXG4gICAgICAgICAgICBjaGVja2VkLFxyXG4gICAgICAgICkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLypcclxuICAgICAqIFJldHVybnMgdGhlIGVtYWlsIHB1c2hlciAocHVzaGVyIG9mIHR5cGUgJ2VtYWlsJykgZm9yIGEgZ2l2ZW5cclxuICAgICAqIGVtYWlsIGFkZHJlc3MuIEVtYWlsIHB1c2hlcnMgYWxsIGhhdmUgdGhlIHNhbWUgYXBwIElELCBzbyBzaW5jZVxyXG4gICAgICogcHVzaGVycyBhcmUgdW5pcXVlIG92ZXIgKGFwcCBJRCwgcHVzaGtleSksIHRoZXJlIHdpbGwgYmUgYXQgbW9zdFxyXG4gICAgICogb25lIHN1Y2ggcHVzaGVyLlxyXG4gICAgICovXHJcbiAgICBnZXRFbWFpbFB1c2hlcjogZnVuY3Rpb24ocHVzaGVycywgYWRkcmVzcykge1xyXG4gICAgICAgIGlmIChwdXNoZXJzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwdXNoZXJzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGlmIChwdXNoZXJzW2ldLmtpbmQgPT09ICdlbWFpbCcgJiYgcHVzaGVyc1tpXS5wdXNoa2V5ID09PSBhZGRyZXNzKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHVzaGVyc1tpXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkVuYWJsZUVtYWlsTm90aWZpY2F0aW9uc0NoYW5nZTogZnVuY3Rpb24oYWRkcmVzcywgY2hlY2tlZCkge1xyXG4gICAgICAgIGxldCBlbWFpbFB1c2hlclByb21pc2U7XHJcbiAgICAgICAgaWYgKGNoZWNrZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0YSA9IHt9O1xyXG4gICAgICAgICAgICBkYXRhWydicmFuZCddID0gU2RrQ29uZmlnLmdldCgpLmJyYW5kIHx8ICdSaW90JztcclxuICAgICAgICAgICAgZW1haWxQdXNoZXJQcm9taXNlID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldFB1c2hlcih7XHJcbiAgICAgICAgICAgICAgICBraW5kOiAnZW1haWwnLFxyXG4gICAgICAgICAgICAgICAgYXBwX2lkOiAnbS5lbWFpbCcsXHJcbiAgICAgICAgICAgICAgICBwdXNoa2V5OiBhZGRyZXNzLFxyXG4gICAgICAgICAgICAgICAgYXBwX2Rpc3BsYXlfbmFtZTogJ0VtYWlsIE5vdGlmaWNhdGlvbnMnLFxyXG4gICAgICAgICAgICAgICAgZGV2aWNlX2Rpc3BsYXlfbmFtZTogYWRkcmVzcyxcclxuICAgICAgICAgICAgICAgIGxhbmc6IG5hdmlnYXRvci5sYW5ndWFnZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICBhcHBlbmQ6IHRydWUsIC8vIFdlIGFsd2F5cyBhcHBlbmQgZm9yIGVtYWlsIHB1c2hlcnMgc2luY2Ugd2UgZG9uJ3Qgd2FudCB0byBzdG9wIG90aGVyIGFjY291bnRzIG5vdGlmeWluZyB0byB0aGUgc2FtZSBlbWFpbCBhZGRyZXNzXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGVtYWlsUHVzaGVyID0gdGhpcy5nZXRFbWFpbFB1c2hlcih0aGlzLnN0YXRlLnB1c2hlcnMsIGFkZHJlc3MpO1xyXG4gICAgICAgICAgICBlbWFpbFB1c2hlci5raW5kID0gbnVsbDtcclxuICAgICAgICAgICAgZW1haWxQdXNoZXJQcm9taXNlID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldFB1c2hlcihlbWFpbFB1c2hlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVtYWlsUHVzaGVyUHJvbWlzZS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcmVmcmVzaEZyb21TZXJ2ZXIoKTtcclxuICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRXJyb3Igc2F2aW5nIGVtYWlsIG5vdGlmaWNhdGlvbiBwcmVmZXJlbmNlcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvciBzYXZpbmcgZW1haWwgbm90aWZpY2F0aW9uIHByZWZlcmVuY2VzJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxzdCBzYXZpbmcgeW91ciBlbWFpbCBub3RpZmljYXRpb24gcHJlZmVyZW5jZXMuJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbk5vdGlmU3RhdGVCdXR0b25DbGlja2VkOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIC8vIEZJWE1FOiB1c2UgLmJpbmQoKSByYXRoZXIgdGhhbiBjbGFzc05hbWUgbWV0YWRhdGEgaGVyZSBzdXJlbHlcclxuICAgICAgICBjb25zdCB2ZWN0b3JSdWxlSWQgPSBldmVudC50YXJnZXQuY2xhc3NOYW1lLnNwbGl0KFwiLVwiKVswXTtcclxuICAgICAgICBjb25zdCBuZXdQdXNoUnVsZVZlY3RvclN0YXRlID0gZXZlbnQudGFyZ2V0LmNsYXNzTmFtZS5zcGxpdChcIi1cIilbMV07XHJcblxyXG4gICAgICAgIGlmIChcIl9rZXl3b3Jkc1wiID09PSB2ZWN0b3JSdWxlSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2V0S2V5d29yZHNQdXNoUnVsZVZlY3RvclN0YXRlKG5ld1B1c2hSdWxlVmVjdG9yU3RhdGUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLmdldFJ1bGUodmVjdG9yUnVsZUlkKTtcclxuICAgICAgICAgICAgaWYgKHJ1bGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFB1c2hSdWxlVmVjdG9yU3RhdGUocnVsZSwgbmV3UHVzaFJ1bGVWZWN0b3JTdGF0ZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uS2V5d29yZHNDbGlja2VkOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAvLyBDb21wdXRlIHRoZSBrZXl3b3JkcyBsaXN0IHRvIGRpc3BsYXlcclxuICAgICAgICBsZXQga2V5d29yZHMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gdGhpcy5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZXMucnVsZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgcnVsZSA9IHRoaXMuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLnJ1bGVzW2ldO1xyXG4gICAgICAgICAgICBrZXl3b3Jkcy5wdXNoKHJ1bGUucGF0dGVybik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChrZXl3b3Jkcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgLy8gQXMga2VlcGluZyB0aGUgb3JkZXIgb2YgcGVyLXdvcmQgcHVzaCBydWxlcyBocyBzaWRlIGlzIGEgYml0IHRyaWNreSB0byBjb2RlLFxyXG4gICAgICAgICAgICAvLyBkaXNwbGF5IHRoZSBrZXl3b3JkcyBpbiBhbHBoYWJldGljYWwgb3JkZXIgdG8gdGhlIHVzZXJcclxuICAgICAgICAgICAga2V5d29yZHMuc29ydCgpO1xyXG5cclxuICAgICAgICAgICAga2V5d29yZHMgPSBrZXl3b3Jkcy5qb2luKFwiLCBcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAga2V5d29yZHMgPSBcIlwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgVGV4dElucHV0RGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuVGV4dElucHV0RGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0tleXdvcmRzIERpYWxvZycsICcnLCBUZXh0SW5wdXREaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KCdLZXl3b3JkcycpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ0VudGVyIGtleXdvcmRzIHNlcGFyYXRlZCBieSBhIGNvbW1hOicpLFxyXG4gICAgICAgICAgICBidXR0b246IF90KCdPSycpLFxyXG4gICAgICAgICAgICB2YWx1ZToga2V5d29yZHMsXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IGZ1bmN0aW9uIG9uRmluaXNoZWQoc2hvdWxkX2xlYXZlLCBuZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNob3VsZF9sZWF2ZSAmJiBuZXdWYWx1ZSAhPT0ga2V5d29yZHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbmV3S2V5d29yZHMgPSBuZXdWYWx1ZS5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgaSBpbiBuZXdLZXl3b3Jkcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdLZXl3b3Jkc1tpXSA9IG5ld0tleXdvcmRzW2ldLnRyaW0oKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFJlbW92ZSBkdXBsaWNhdGVzIGFuZCBlbXB0eVxyXG4gICAgICAgICAgICAgICAgICAgIG5ld0tleXdvcmRzID0gbmV3S2V5d29yZHMucmVkdWNlKGZ1bmN0aW9uKGFycmF5LCBrZXl3b3JkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChrZXl3b3JkICE9PSBcIlwiICYmIGFycmF5LmluZGV4T2Yoa2V5d29yZCkgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnJheS5wdXNoKGtleXdvcmQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcnJheTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBbXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuX3NldEtleXdvcmRzKG5ld0tleXdvcmRzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0UnVsZTogZnVuY3Rpb24odmVjdG9yUnVsZUlkKSB7XHJcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHRoaXMuc3RhdGUudmVjdG9yUHVzaFJ1bGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLnN0YXRlLnZlY3RvclB1c2hSdWxlc1tpXTtcclxuICAgICAgICAgICAgaWYgKHJ1bGUudmVjdG9yUnVsZUlkID09PSB2ZWN0b3JSdWxlSWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBydWxlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UHVzaFJ1bGVWZWN0b3JTdGF0ZTogZnVuY3Rpb24ocnVsZSwgbmV3UHVzaFJ1bGVWZWN0b3JTdGF0ZSkge1xyXG4gICAgICAgIGlmIChydWxlICYmIHJ1bGUudmVjdG9yU3RhdGUgIT09IG5ld1B1c2hSdWxlVmVjdG9yU3RhdGUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBwaGFzZTogdGhpcy5waGFzZXMuTE9BRElORyxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICBjb25zdCBkZWZlcnJlZHMgPSBbXTtcclxuICAgICAgICAgICAgY29uc3QgcnVsZURlZmluaXRpb24gPSBWZWN0b3JQdXNoUnVsZXNEZWZpbml0aW9uc1tydWxlLnZlY3RvclJ1bGVJZF07XHJcblxyXG4gICAgICAgICAgICBpZiAocnVsZS5ydWxlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhY3Rpb25zID0gcnVsZURlZmluaXRpb24udmVjdG9yU3RhdGVUb0FjdGlvbnNbbmV3UHVzaFJ1bGVWZWN0b3JTdGF0ZV07XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFhY3Rpb25zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVGhlIG5ldyBzdGF0ZSBjb3JyZXNwb25kcyB0byBkaXNhYmxpbmcgdGhlIHJ1bGUuXHJcbiAgICAgICAgICAgICAgICAgICAgZGVmZXJyZWRzLnB1c2goY2xpLnNldFB1c2hSdWxlRW5hYmxlZCgnZ2xvYmFsJywgcnVsZS5ydWxlLmtpbmQsIHJ1bGUucnVsZS5ydWxlX2lkLCBmYWxzZSkpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBUaGUgbmV3IHN0YXRlIGNvcnJlc3BvbmRzIHRvIGVuYWJsaW5nIHRoZSBydWxlIGFuZCBzZXR0aW5nIHNwZWNpZmljIGFjdGlvbnNcclxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZHMucHVzaCh0aGlzLl91cGRhdGVQdXNoUnVsZUFjdGlvbnMocnVsZS5ydWxlLCBhY3Rpb25zLCB0cnVlKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIFByb21pc2UuYWxsKGRlZmVycmVkcykudGhlbihmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHNlbGYuX3JlZnJlc2hGcm9tU2VydmVyKCk7XHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBjaGFuZ2Ugc2V0dGluZ3M6IFwiICsgZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGNoYW5nZSBzZXR0aW5ncycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRmFpbGVkIHRvIGNoYW5nZSBzZXR0aW5ncycpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVycm9yICYmIGVycm9yLm1lc3NhZ2UpID8gZXJyb3IubWVzc2FnZSA6IF90KCdPcGVyYXRpb24gZmFpbGVkJykpLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IHNlbGYuX3JlZnJlc2hGcm9tU2VydmVyLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3NldEtleXdvcmRzUHVzaFJ1bGVWZWN0b3JTdGF0ZTogZnVuY3Rpb24obmV3UHVzaFJ1bGVWZWN0b3JTdGF0ZSkge1xyXG4gICAgICAgIC8vIElzIHRoZXJlIHJlYWxseSBhIGNoYW5nZT9cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZXMudmVjdG9yU3RhdGUgPT09IG5ld1B1c2hSdWxlVmVjdG9yU3RhdGVcclxuICAgICAgICAgICAgfHwgdGhpcy5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZXMucnVsZXMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBoYXNlOiB0aGlzLnBoYXNlcy5MT0FESU5HLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgYWxsIHJ1bGVzIGluIHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzXHJcbiAgICAgICAgY29uc3QgZGVmZXJyZWRzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHRoaXMuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLnJ1bGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLnN0YXRlLnZlY3RvckNvbnRlbnRSdWxlcy5ydWxlc1tpXTtcclxuXHJcbiAgICAgICAgICAgIGxldCBlbmFibGVkOyBsZXQgYWN0aW9ucztcclxuICAgICAgICAgICAgc3dpdGNoIChuZXdQdXNoUnVsZVZlY3RvclN0YXRlKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFB1c2hSdWxlVmVjdG9yU3RhdGUuT046XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJ1bGUuYWN0aW9ucy5sZW5ndGggIT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9ucyA9IFB1c2hSdWxlVmVjdG9yU3RhdGUuYWN0aW9uc0ZvcihQdXNoUnVsZVZlY3RvclN0YXRlLk9OKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnZlY3RvckNvbnRlbnRSdWxlcy52ZWN0b3JTdGF0ZSA9PT0gUHVzaFJ1bGVWZWN0b3JTdGF0ZS5PRkYpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGNhc2UgUHVzaFJ1bGVWZWN0b3JTdGF0ZS5MT1VEOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChydWxlLmFjdGlvbnMubGVuZ3RoICE9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbnMgPSBQdXNoUnVsZVZlY3RvclN0YXRlLmFjdGlvbnNGb3IoUHVzaFJ1bGVWZWN0b3JTdGF0ZS5MT1VEKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnZlY3RvckNvbnRlbnRSdWxlcy52ZWN0b3JTdGF0ZSA9PT0gUHVzaFJ1bGVWZWN0b3JTdGF0ZS5PRkYpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGNhc2UgUHVzaFJ1bGVWZWN0b3JTdGF0ZS5PRkY6XHJcbiAgICAgICAgICAgICAgICAgICAgZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoYWN0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgLy8gTm90ZSB0aGF0IHRoZSB3b3JrYXJvdW5kIGluIF91cGRhdGVQdXNoUnVsZUFjdGlvbnMgd2lsbCBhdXRvbWF0aWNhbGx5XHJcbiAgICAgICAgICAgICAgICAvLyBlbmFibGUgdGhlIHJ1bGVcclxuICAgICAgICAgICAgICAgIGRlZmVycmVkcy5wdXNoKHRoaXMuX3VwZGF0ZVB1c2hSdWxlQWN0aW9ucyhydWxlLCBhY3Rpb25zLCBlbmFibGVkKSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZW5hYmxlZCAhPSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIGRlZmVycmVkcy5wdXNoKGNsaS5zZXRQdXNoUnVsZUVuYWJsZWQoJ2dsb2JhbCcsIHJ1bGUua2luZCwgcnVsZS5ydWxlX2lkLCBlbmFibGVkKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFByb21pc2UuYWxsKGRlZmVycmVkcykudGhlbihmdW5jdGlvbihyZXNwcykge1xyXG4gICAgICAgICAgICBzZWxmLl9yZWZyZXNoRnJvbVNlcnZlcigpO1xyXG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJDYW4ndCB1cGRhdGUgdXNlciBub3RpZmljYXRpb24gc2V0dGluZ3M6IFwiICsgZXJyb3IpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYW5cXCd0IHVwZGF0ZSB1c2VyIG5vdGlmY2F0aW9uIHNldHRpbmdzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0NhblxcJ3QgdXBkYXRlIHVzZXIgbm90aWZpY2F0aW9uIHNldHRpbmdzJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnJvciAmJiBlcnJvci5tZXNzYWdlKSA/IGVycm9yLm1lc3NhZ2UgOiBfdCgnT3BlcmF0aW9uIGZhaWxlZCcpKSxcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IHNlbGYuX3JlZnJlc2hGcm9tU2VydmVyLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3NldEtleXdvcmRzOiBmdW5jdGlvbihuZXdLZXl3b3Jkcykge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwaGFzZTogdGhpcy5waGFzZXMuTE9BRElORyxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHJlbW92ZURlZmVycmVkcyA9IFtdO1xyXG5cclxuICAgICAgICAvLyBSZW1vdmUgcGVyLXdvcmQgcHVzaCBydWxlcyBvZiBrZXl3b3JkcyB0aGF0IGFyZSBubyBtb3JlIGluIHRoZSBsaXN0XHJcbiAgICAgICAgY29uc3QgdmVjdG9yQ29udGVudFJ1bGVzUGF0dGVybnMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gc2VsZi5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZXMucnVsZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgcnVsZSA9IHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLnJ1bGVzW2ldO1xyXG5cclxuICAgICAgICAgICAgdmVjdG9yQ29udGVudFJ1bGVzUGF0dGVybnMucHVzaChydWxlLnBhdHRlcm4pO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5ld0tleXdvcmRzLmluZGV4T2YocnVsZS5wYXR0ZXJuKSA8IDApIHtcclxuICAgICAgICAgICAgICAgIHJlbW92ZURlZmVycmVkcy5wdXNoKGNsaS5kZWxldGVQdXNoUnVsZSgnZ2xvYmFsJywgcnVsZS5raW5kLCBydWxlLnJ1bGVfaWQpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgdGhlIGtleXdvcmQgaXMgcGFydCBvZiBgZXh0ZXJuYWxDb250ZW50UnVsZXNgLCByZW1vdmUgdGhlIHJ1bGVcclxuICAgICAgICAvLyBiZWZvcmUgcmVjcmVhdGluZyBpdCBpbiB0aGUgcmlnaHQgVmVjdG9yIHBhdGhcclxuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gc2VsZi5zdGF0ZS5leHRlcm5hbENvbnRlbnRSdWxlcykge1xyXG4gICAgICAgICAgICBjb25zdCBydWxlID0gc2VsZi5zdGF0ZS5leHRlcm5hbENvbnRlbnRSdWxlc1tpXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdLZXl3b3Jkcy5pbmRleE9mKHJ1bGUucGF0dGVybikgPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmVtb3ZlRGVmZXJyZWRzLnB1c2goY2xpLmRlbGV0ZVB1c2hSdWxlKCdnbG9iYWwnLCBydWxlLmtpbmQsIHJ1bGUucnVsZV9pZCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBvbkVycm9yID0gZnVuY3Rpb24oZXJyb3IpIHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byB1cGRhdGUga2V5d29yZHM6IFwiICsgZXJyb3IpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gdXBkYXRlIGtleXdvcmRzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0ZhaWxlZCB0byB1cGRhdGUga2V5d29yZHMnKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVycm9yICYmIGVycm9yLm1lc3NhZ2UpID8gZXJyb3IubWVzc2FnZSA6IF90KCdPcGVyYXRpb24gZmFpbGVkJykpLFxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZDogc2VsZi5fcmVmcmVzaEZyb21TZXJ2ZXIsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIFRoZW4sIGFkZCB0aGUgbmV3IG9uZXNcclxuICAgICAgICBQcm9taXNlLmFsbChyZW1vdmVEZWZlcnJlZHMpLnRoZW4oZnVuY3Rpb24ocmVzcHMpIHtcclxuICAgICAgICAgICAgY29uc3QgZGVmZXJyZWRzID0gW107XHJcblxyXG4gICAgICAgICAgICBsZXQgcHVzaFJ1bGVWZWN0b3JTdGF0ZUtpbmQgPSBzZWxmLnN0YXRlLnZlY3RvckNvbnRlbnRSdWxlcy52ZWN0b3JTdGF0ZTtcclxuICAgICAgICAgICAgaWYgKHB1c2hSdWxlVmVjdG9yU3RhdGVLaW5kID09PSBQdXNoUnVsZVZlY3RvclN0YXRlLk9GRikge1xyXG4gICAgICAgICAgICAgICAgLy8gV2hlbiB0aGUgY3VycmVudCBnbG9iYWwga2V5d29yZHMgcnVsZSBpcyBPRkYsIHdlIG5lZWQgdG8gbG9vayBhdFxyXG4gICAgICAgICAgICAgICAgLy8gdGhlIGZsYXZvciBvZiBydWxlcyBpbiAndmVjdG9yQ29udGVudFJ1bGVzJyB0byBhcHBseSB0aGUgc2FtZSBhY3Rpb25zXHJcbiAgICAgICAgICAgICAgICAvLyB3aGVuIGNyZWF0aW5nIHRoZSBuZXcgcnVsZS5cclxuICAgICAgICAgICAgICAgIC8vIFRodXMsIHRoaXMgbmV3IHJ1bGUgd2lsbCBqb2luIHRoZSAndmVjdG9yQ29udGVudFJ1bGVzJyBzZXQuXHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZXMucnVsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHVzaFJ1bGVWZWN0b3JTdGF0ZUtpbmQgPSBQdXNoUnVsZVZlY3RvclN0YXRlLmNvbnRlbnRSdWxlVmVjdG9yU3RhdGVLaW5kKHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLnJ1bGVzWzBdKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gT04gaXMgZGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIHB1c2hSdWxlVmVjdG9yU3RhdGVLaW5kID0gUHVzaFJ1bGVWZWN0b3JTdGF0ZS5PTjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCBpIGluIG5ld0tleXdvcmRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBrZXl3b3JkID0gbmV3S2V5d29yZHNbaV07XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHZlY3RvckNvbnRlbnRSdWxlc1BhdHRlcm5zLmluZGV4T2Yoa2V5d29yZCkgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLnZlY3RvclN0YXRlICE9PSBQdXNoUnVsZVZlY3RvclN0YXRlLk9GRikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZHMucHVzaChjbGkuYWRkUHVzaFJ1bGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgKCdnbG9iYWwnLCAnY29udGVudCcsIGtleXdvcmQsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uczogUHVzaFJ1bGVWZWN0b3JTdGF0ZS5hY3Rpb25zRm9yKHB1c2hSdWxlVmVjdG9yU3RhdGVLaW5kKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgcGF0dGVybjoga2V5d29yZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkcy5wdXNoKHNlbGYuX2FkZERpc2FibGVkUHVzaFJ1bGUoJ2dsb2JhbCcsICdjb250ZW50Jywga2V5d29yZCwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb25zOiBQdXNoUnVsZVZlY3RvclN0YXRlLmFjdGlvbnNGb3IocHVzaFJ1bGVWZWN0b3JTdGF0ZUtpbmQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBwYXR0ZXJuOiBrZXl3b3JkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBQcm9taXNlLmFsbChkZWZlcnJlZHMpLnRoZW4oZnVuY3Rpb24ocmVzcHMpIHtcclxuICAgICAgICAgICAgICAgIHNlbGYuX3JlZnJlc2hGcm9tU2VydmVyKCk7XHJcbiAgICAgICAgICAgIH0sIG9uRXJyb3IpO1xyXG4gICAgICAgIH0sIG9uRXJyb3IpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBDcmVhdGUgYSBwdXNoIHJ1bGUgYnV0IGRpc2FibGVkXHJcbiAgICBfYWRkRGlzYWJsZWRQdXNoUnVsZTogZnVuY3Rpb24oc2NvcGUsIGtpbmQsIHJ1bGVJZCwgYm9keSkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICByZXR1cm4gY2xpLmFkZFB1c2hSdWxlKHNjb3BlLCBraW5kLCBydWxlSWQsIGJvZHkpLnRoZW4oKCkgPT5cclxuICAgICAgICAgICAgY2xpLnNldFB1c2hSdWxlRW5hYmxlZChzY29wZSwga2luZCwgcnVsZUlkLCBmYWxzZSksXHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gQ2hlY2sgaWYgYW55IGxlZ2FjeSBpbS52ZWN0b3IgcnVsZXMgbmVlZCB0byBiZSBwb3J0ZWQgdG8gdGhlIG5ldyBBUElcclxuICAgIC8vIGZvciBvdmVycmlkaW5nIHRoZSBhY3Rpb25zIG9mIGRlZmF1bHQgcnVsZXMuXHJcbiAgICBfcG9ydFJ1bGVzVG9OZXdBUEk6IGZ1bmN0aW9uKHJ1bGVzZXRzKSB7XHJcbiAgICAgICAgY29uc3QgbmVlZHNVcGRhdGUgPSBbXTtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIGZvciAoY29uc3Qga2luZCBpbiBydWxlc2V0cy5nbG9iYWwpIHtcclxuICAgICAgICAgICAgY29uc3QgcnVsZXNldCA9IHJ1bGVzZXRzLmdsb2JhbFtraW5kXTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBydWxlc2V0Lmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBydWxlID0gcnVsZXNldFtpXTtcclxuICAgICAgICAgICAgICAgIGlmIChydWxlLnJ1bGVfaWQgaW4gTEVHQUNZX1JVTEVTKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJQb3J0aW5nIGxlZ2FjeSBydWxlXCIsIHJ1bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG5lZWRzVXBkYXRlLnB1c2goIGZ1bmN0aW9uKGtpbmQsIHJ1bGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNsaS5zZXRQdXNoUnVsZUFjdGlvbnMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZ2xvYmFsJywga2luZCwgTEVHQUNZX1JVTEVTW3J1bGUucnVsZV9pZF0sIHBvcnRMZWdhY3lBY3Rpb25zKHJ1bGUuYWN0aW9ucyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICkudGhlbigoKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpLmRlbGV0ZVB1c2hSdWxlKCdnbG9iYWwnLCBraW5kLCBydWxlLnJ1bGVfaWQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApLmNhdGNoKCAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKGBFcnJvciB3aGVuIHBvcnRpbmcgbGVnYWN5IHJ1bGU6ICR7ZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfShraW5kLCBydWxlKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChuZWVkc1VwZGF0ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIC8vIElmIHNvbWUgb2YgdGhlIHJ1bGVzIG5lZWQgdG8gYmUgcG9ydGVkIHRoZW4gd2FpdCBmb3IgdGhlIHBvcnRpbmdcclxuICAgICAgICAgICAgLy8gdG8gaGFwcGVuIGFuZCB0aGVuIGZldGNoIHRoZSBydWxlcyBhZ2Fpbi5cclxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UuYWxsKG5lZWRzVXBkYXRlKS50aGVuKCgpID0+XHJcbiAgICAgICAgICAgICAgICBjbGkuZ2V0UHVzaFJ1bGVzKCksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gT3RoZXJ3aXNlIHJldHVybiB0aGUgcnVsZXMgdGhhdCB3ZSBhbHJlYWR5IGhhdmUuXHJcbiAgICAgICAgICAgIHJldHVybiBydWxlc2V0cztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9yZWZyZXNoRnJvbVNlcnZlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgY29uc3QgcHVzaFJ1bGVzUHJvbWlzZSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRQdXNoUnVsZXMoKS50aGVuKHNlbGYuX3BvcnRSdWxlc1RvTmV3QVBJKS50aGVuKGZ1bmN0aW9uKHJ1bGVzZXRzKSB7XHJcbiAgICAgICAgICAgIC8vLyBYWFggc2VyaW91c2x5PyB3dGYgaXMgdGhpcz9cclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnB1c2hSdWxlcyA9IHJ1bGVzZXRzO1xyXG5cclxuICAgICAgICAgICAgLy8gR2V0IGhvbWVzZXJ2ZXIgZGVmYXVsdCBydWxlcyBhbmQgdHJpYWdlIHRoZW0gYnkgY2F0ZWdvcmllc1xyXG4gICAgICAgICAgICBjb25zdCBydWxlX2NhdGVnb3JpZXMgPSB7XHJcbiAgICAgICAgICAgICAgICAvLyBUaGUgbWFzdGVyIHJ1bGUgKGFsbCBub3RpZmljYXRpb25zIGRpc2FibGluZylcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLm1hc3Rlcic6ICdtYXN0ZXInLFxyXG5cclxuICAgICAgICAgICAgICAgIC8vIFRoZSBkZWZhdWx0IHB1c2ggcnVsZXMgZGlzcGxheWVkIGJ5IFZlY3RvciBVSVxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUuY29udGFpbnNfZGlzcGxheV9uYW1lJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5jb250YWluc191c2VyX25hbWUnOiAndmVjdG9yJyxcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLnJvb21ub3RpZic6ICd2ZWN0b3InLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUucm9vbV9vbmVfdG9fb25lJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5lbmNyeXB0ZWRfcm9vbV9vbmVfdG9fb25lJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5tZXNzYWdlJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5lbmNyeXB0ZWQnOiAndmVjdG9yJyxcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLmludml0ZV9mb3JfbWUnOiAndmVjdG9yJyxcclxuICAgICAgICAgICAgICAgIC8vJy5tLnJ1bGUubWVtYmVyX2V2ZW50JzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5jYWxsJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5zdXBwcmVzc19ub3RpY2VzJzogJ3ZlY3RvcicsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS50b21ic3RvbmUnOiAndmVjdG9yJyxcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBPdGhlcnMgZ28gdG8gb3RoZXJzXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAvLyBIUyBkZWZhdWx0IHJ1bGVzXHJcbiAgICAgICAgICAgIGNvbnN0IGRlZmF1bHRSdWxlcyA9IHttYXN0ZXI6IFtdLCB2ZWN0b3I6IHt9LCBvdGhlcnM6IFtdfTtcclxuXHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qga2luZCBpbiBydWxlc2V0cy5nbG9iYWwpIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgT2JqZWN0LmtleXMocnVsZXNldHMuZ2xvYmFsW2tpbmRdKS5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHIgPSBydWxlc2V0cy5nbG9iYWxba2luZF1baV07XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2F0ID0gcnVsZV9jYXRlZ29yaWVzW3IucnVsZV9pZF07XHJcbiAgICAgICAgICAgICAgICAgICAgci5raW5kID0ga2luZDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHIucnVsZV9pZFswXSA9PT0gJy4nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjYXQgPT09ICd2ZWN0b3InKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UnVsZXMudmVjdG9yW3IucnVsZV9pZF0gPSByO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGNhdCA9PT0gJ21hc3RlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRSdWxlcy5tYXN0ZXIucHVzaChyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRSdWxlc1snb3RoZXJzJ10ucHVzaChyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gR2V0IHRoZSBtYXN0ZXIgcnVsZSBpZiBhbnkgZGVmaW5lZCBieSB0aGUgaHNcclxuICAgICAgICAgICAgaWYgKGRlZmF1bHRSdWxlcy5tYXN0ZXIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgc2VsZi5zdGF0ZS5tYXN0ZXJQdXNoUnVsZSA9IGRlZmF1bHRSdWxlcy5tYXN0ZXJbMF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHBhcnNlIHRoZSBrZXl3b3JkIHJ1bGVzIGludG8gb3VyIHN0YXRlXHJcbiAgICAgICAgICAgIGNvbnN0IGNvbnRlbnRSdWxlcyA9IENvbnRlbnRSdWxlcy5wYXJzZUNvbnRlbnRSdWxlcyhydWxlc2V0cyk7XHJcbiAgICAgICAgICAgIHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzID0ge1xyXG4gICAgICAgICAgICAgICAgdmVjdG9yU3RhdGU6IGNvbnRlbnRSdWxlcy52ZWN0b3JTdGF0ZSxcclxuICAgICAgICAgICAgICAgIHJ1bGVzOiBjb250ZW50UnVsZXMucnVsZXMsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHNlbGYuc3RhdGUuZXh0ZXJuYWxDb250ZW50UnVsZXMgPSBjb250ZW50UnVsZXMuZXh0ZXJuYWxSdWxlcztcclxuXHJcbiAgICAgICAgICAgIC8vIEJ1aWxkIHRoZSBydWxlcyBkaXNwbGF5ZWQgaW4gdGhlIFZlY3RvciBVSSBtYXRyaXggdGFibGVcclxuICAgICAgICAgICAgc2VsZi5zdGF0ZS52ZWN0b3JQdXNoUnVsZXMgPSBbXTtcclxuICAgICAgICAgICAgc2VsZi5zdGF0ZS5leHRlcm5hbFB1c2hSdWxlcyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmVjdG9yUnVsZUlkcyA9IFtcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLmNvbnRhaW5zX2Rpc3BsYXlfbmFtZScsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5jb250YWluc191c2VyX25hbWUnLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUucm9vbW5vdGlmJyxcclxuICAgICAgICAgICAgICAgICdfa2V5d29yZHMnLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUucm9vbV9vbmVfdG9fb25lJyxcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLmVuY3J5cHRlZF9yb29tX29uZV90b19vbmUnLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUubWVzc2FnZScsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5lbmNyeXB0ZWQnLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUuaW52aXRlX2Zvcl9tZScsXHJcbiAgICAgICAgICAgICAgICAvLydpbS52ZWN0b3IucnVsZS5tZW1iZXJfZXZlbnQnLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUuY2FsbCcsXHJcbiAgICAgICAgICAgICAgICAnLm0ucnVsZS5zdXBwcmVzc19ub3RpY2VzJyxcclxuICAgICAgICAgICAgICAgICcubS5ydWxlLnRvbWJzdG9uZScsXHJcbiAgICAgICAgICAgIF07XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgaSBpbiB2ZWN0b3JSdWxlSWRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2ZWN0b3JSdWxlSWQgPSB2ZWN0b3JSdWxlSWRzW2ldO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh2ZWN0b3JSdWxlSWQgPT09ICdfa2V5d29yZHMnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8ga2V5d29yZHMgbmVlZHMgYSBzcGVjaWFsIGhhbmRsaW5nXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRm9yIFZlY3RvciBVSSwgdGhpcyBpcyBhIHNpbmdsZSBnbG9iYWwgcHVzaCBydWxlIGJ1dCB0cmFuc2xhdGVkIGluIE1hdHJpeCxcclxuICAgICAgICAgICAgICAgICAgICAvLyBpdCBjb3JyZXNwb25kcyB0byBhbGwgY29udGVudCBwdXNoIHJ1bGVzIChzdG9yZWQgaW4gc2VsZi5zdGF0ZS52ZWN0b3JDb250ZW50UnVsZSlcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnN0YXRlLnZlY3RvclB1c2hSdWxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ2ZWN0b3JSdWxlSWRcIjogXCJfa2V5d29yZHNcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJkZXNjcmlwdGlvblwiOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ01lc3NhZ2VzIGNvbnRhaW5pbmcgPHNwYW4+a2V5d29yZHM8L3NwYW4+JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ICdzcGFuJzogKHN1YikgPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfVXNlck5vdGlmU2V0dGluZ3Nfa2V5d29yZHNcIiBvbkNsaWNrPXsgc2VsZi5vbktleXdvcmRzQ2xpY2tlZCB9PntzdWJ9PC9zcGFuPixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ2ZWN0b3JTdGF0ZVwiOiBzZWxmLnN0YXRlLnZlY3RvckNvbnRlbnRSdWxlcy52ZWN0b3JTdGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcnVsZURlZmluaXRpb24gPSBWZWN0b3JQdXNoUnVsZXNEZWZpbml0aW9uc1t2ZWN0b3JSdWxlSWRdO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSBkZWZhdWx0UnVsZXMudmVjdG9yW3ZlY3RvclJ1bGVJZF07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHZlY3RvclN0YXRlID0gcnVsZURlZmluaXRpb24ucnVsZVRvVmVjdG9yU3RhdGUocnVsZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJSZWZyZXNoaW5nIHZlY3RvclB1c2hSdWxlcyBmb3IgXCIgKyB2ZWN0b3JSdWxlSWQgK1wiLCBcIisgcnVsZURlZmluaXRpb24uZGVzY3JpcHRpb24gK1wiLCBcIiArIHJ1bGUgK1wiLCBcIiArIHZlY3RvclN0YXRlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5zdGF0ZS52ZWN0b3JQdXNoUnVsZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidmVjdG9yUnVsZUlkXCI6IHZlY3RvclJ1bGVJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJkZXNjcmlwdGlvblwiOiBfdChydWxlRGVmaW5pdGlvbi5kZXNjcmlwdGlvbiksIC8vIFRleHQgZnJvbSBWZWN0b3JQdXNoUnVsZXNEZWZpbml0aW9ucy5qc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInJ1bGVcIjogcnVsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ2ZWN0b3JTdGF0ZVwiOiB2ZWN0b3JTdGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlcmUgd2FzIGEgcnVsZSB3aGljaCB3ZSBjb3VsZG4ndCBwYXJzZSwgYWRkIGl0IHRvIHRoZSBleHRlcm5hbCBsaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJ1bGUgJiYgIXZlY3RvclN0YXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGUuZGVzY3JpcHRpb24gPSBydWxlRGVmaW5pdGlvbi5kZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5zdGF0ZS5leHRlcm5hbFB1c2hSdWxlcy5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gQnVpbGQgdGhlIHJ1bGVzIG5vdCBtYW5hZ2VkIGJ5IFZlY3RvciBVSVxyXG4gICAgICAgICAgICBjb25zdCBvdGhlclJ1bGVzRGVzY3JpcHRpb25zID0ge1xyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUubWVzc2FnZSc6IF90KCdOb3RpZnkgZm9yIGFsbCBvdGhlciBtZXNzYWdlcy9yb29tcycpLFxyXG4gICAgICAgICAgICAgICAgJy5tLnJ1bGUuZmFsbGJhY2snOiBfdCgnTm90aWZ5IG1lIGZvciBhbnl0aGluZyBlbHNlJyksXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGkgaW4gZGVmYXVsdFJ1bGVzLm90aGVycykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcnVsZSA9IGRlZmF1bHRSdWxlcy5vdGhlcnNbaV07XHJcbiAgICAgICAgICAgICAgICBjb25zdCBydWxlRGVzY3JpcHRpb24gPSBvdGhlclJ1bGVzRGVzY3JpcHRpb25zW3J1bGUucnVsZV9pZF07XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gU2hvdyBlbmFibGVkIGRlZmF1bHQgcnVsZXMgdGhhdCB3YXMgbW9kaWZpZWQgYnkgdGhlIHVzZXJcclxuICAgICAgICAgICAgICAgIGlmIChydWxlRGVzY3JpcHRpb24gJiYgcnVsZS5lbmFibGVkICYmICFydWxlLmRlZmF1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICBydWxlLmRlc2NyaXB0aW9uID0gcnVsZURlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuc3RhdGUuZXh0ZXJuYWxQdXNoUnVsZXMucHVzaChydWxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBwdXNoZXJzUHJvbWlzZSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRQdXNoZXJzKCkudGhlbihmdW5jdGlvbihyZXNwKSB7XHJcbiAgICAgICAgICAgIHNlbGYuc2V0U3RhdGUoe3B1c2hlcnM6IHJlc3AucHVzaGVyc30pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBQcm9taXNlLmFsbChbcHVzaFJ1bGVzUHJvbWlzZSwgcHVzaGVyc1Byb21pc2VdKS50aGVuKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHBoYXNlOiBzZWxmLnBoYXNlcy5ESVNQTEFZLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnJvcikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgc2VsZi5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBwaGFzZTogc2VsZi5waGFzZXMuRVJST1IsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBhY3R1YWxseSBleHBsaWNpdGx5IHVwZGF0ZSBvdXIgc3RhdGUgIGhhdmluZyBiZWVuIGRlZXAtbWFuaXB1bGF0aW5nIGl0XHJcbiAgICAgICAgICAgIHNlbGYuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbWFzdGVyUHVzaFJ1bGU6IHNlbGYuc3RhdGUubWFzdGVyUHVzaFJ1bGUsXHJcbiAgICAgICAgICAgICAgICB2ZWN0b3JDb250ZW50UnVsZXM6IHNlbGYuc3RhdGUudmVjdG9yQ29udGVudFJ1bGVzLFxyXG4gICAgICAgICAgICAgICAgdmVjdG9yUHVzaFJ1bGVzOiBzZWxmLnN0YXRlLnZlY3RvclB1c2hSdWxlcyxcclxuICAgICAgICAgICAgICAgIGV4dGVybmFsQ29udGVudFJ1bGVzOiBzZWxmLnN0YXRlLmV4dGVybmFsQ29udGVudFJ1bGVzLFxyXG4gICAgICAgICAgICAgICAgZXh0ZXJuYWxQdXNoUnVsZXM6IHNlbGYuc3RhdGUuZXh0ZXJuYWxQdXNoUnVsZXMsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VGhyZWVQaWRzKCkudGhlbigocikgPT4gdGhpcy5zZXRTdGF0ZSh7dGhyZWVwaWRzOiByLnRocmVlcGlkc30pKTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uQ2xlYXJOb3RpZmljYXRpb25zOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIGNsaS5nZXRSb29tcygpLmZvckVhY2gociA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KCkgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudHMgPSByLmdldExpdmVUaW1lbGluZSgpLmdldEV2ZW50cygpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50cy5sZW5ndGgpIGNsaS5zZW5kUmVhZFJlY2VpcHQoZXZlbnRzLnBvcCgpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdXBkYXRlUHVzaFJ1bGVBY3Rpb25zOiBmdW5jdGlvbihydWxlLCBhY3Rpb25zLCBlbmFibGVkKSB7XHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICByZXR1cm4gY2xpLnNldFB1c2hSdWxlQWN0aW9ucyhcclxuICAgICAgICAgICAgJ2dsb2JhbCcsIHJ1bGUua2luZCwgcnVsZS5ydWxlX2lkLCBhY3Rpb25zLFxyXG4gICAgICAgICkudGhlbiggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIC8vIFRoZW4sIGlmIHJlcXVlc3RlZCwgZW5hYmxlZCBvciBkaXNhYmxlZCB0aGUgcnVsZVxyXG4gICAgICAgICAgICBpZiAodW5kZWZpbmVkICE9IGVuYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjbGkuc2V0UHVzaFJ1bGVFbmFibGVkKFxyXG4gICAgICAgICAgICAgICAgICAgICdnbG9iYWwnLCBydWxlLmtpbmQsIHJ1bGUucnVsZV9pZCwgZW5hYmxlZCxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyTm90aWZSdWxlc1RhYmxlUm93OiBmdW5jdGlvbih0aXRsZSwgY2xhc3NOYW1lLCBwdXNoUnVsZVZlY3RvclN0YXRlKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPHRyIGtleT17IGNsYXNzTmFtZSB9PlxyXG4gICAgICAgICAgICAgICAgPHRoPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgPC90aD5cclxuXHJcbiAgICAgICAgICAgICAgICA8dGg+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT0ge2NsYXNzTmFtZSArIFwiLVwiICsgUHVzaFJ1bGVWZWN0b3JTdGF0ZS5PRkZ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJyYWRpb1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9eyBwdXNoUnVsZVZlY3RvclN0YXRlID09PSBQdXNoUnVsZVZlY3RvclN0YXRlLk9GRiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdGhpcy5vbk5vdGlmU3RhdGVCdXR0b25DbGlja2VkIH0gLz5cclxuICAgICAgICAgICAgICAgIDwvdGg+XHJcblxyXG4gICAgICAgICAgICAgICAgPHRoPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9IHtjbGFzc05hbWUgKyBcIi1cIiArIFB1c2hSdWxlVmVjdG9yU3RhdGUuT059XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJyYWRpb1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9eyBwdXNoUnVsZVZlY3RvclN0YXRlID09PSBQdXNoUnVsZVZlY3RvclN0YXRlLk9OIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB0aGlzLm9uTm90aWZTdGF0ZUJ1dHRvbkNsaWNrZWQgfSAvPlxyXG4gICAgICAgICAgICAgICAgPC90aD5cclxuXHJcbiAgICAgICAgICAgICAgICA8dGg+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT0ge2NsYXNzTmFtZSArIFwiLVwiICsgUHVzaFJ1bGVWZWN0b3JTdGF0ZS5MT1VEfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwicmFkaW9cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXsgcHVzaFJ1bGVWZWN0b3JTdGF0ZSA9PT0gUHVzaFJ1bGVWZWN0b3JTdGF0ZS5MT1VEIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB0aGlzLm9uTm90aWZTdGF0ZUJ1dHRvbkNsaWNrZWQgfSAvPlxyXG4gICAgICAgICAgICAgICAgPC90aD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJOb3RpZlJ1bGVzVGFibGVSb3dzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCByb3dzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHRoaXMuc3RhdGUudmVjdG9yUHVzaFJ1bGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLnN0YXRlLnZlY3RvclB1c2hSdWxlc1tpXTtcclxuICAgICAgICAgICAgaWYgKHJ1bGUucnVsZSA9PT0gdW5kZWZpbmVkICYmIHJ1bGUudmVjdG9yUnVsZUlkLnN0YXJ0c1dpdGgoXCIubS5cIikpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgU2tpcHBpbmcgcmVuZGVyIG9mIHJ1bGUgJHtydWxlLnZlY3RvclJ1bGVJZH0gZHVlIHRvIG5vIHVuZGVybHlpbmcgcnVsZWApO1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcInJlbmRlcmluZzogXCIgKyBydWxlLmRlc2NyaXB0aW9uICsgXCIsIFwiICsgcnVsZS52ZWN0b3JSdWxlSWQgKyBcIiwgXCIgKyBydWxlLnZlY3RvclN0YXRlKTtcclxuICAgICAgICAgICAgcm93cy5wdXNoKHRoaXMucmVuZGVyTm90aWZSdWxlc1RhYmxlUm93KHJ1bGUuZGVzY3JpcHRpb24sIHJ1bGUudmVjdG9yUnVsZUlkLCBydWxlLnZlY3RvclN0YXRlKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByb3dzO1xyXG4gICAgfSxcclxuXHJcbiAgICBoYXNFbWFpbFB1c2hlcjogZnVuY3Rpb24ocHVzaGVycywgYWRkcmVzcykge1xyXG4gICAgICAgIGlmIChwdXNoZXJzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHB1c2hlcnMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgaWYgKHB1c2hlcnNbaV0ua2luZCA9PT0gJ2VtYWlsJyAmJiBwdXNoZXJzW2ldLnB1c2hrZXkgPT09IGFkZHJlc3MpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgZW1haWxOb3RpZmljYXRpb25zUm93OiBmdW5jdGlvbihhZGRyZXNzLCBsYWJlbCkge1xyXG4gICAgICAgIHJldHVybiA8TGFiZWxsZWRUb2dnbGVTd2l0Y2ggdmFsdWU9e3RoaXMuaGFzRW1haWxQdXNoZXIodGhpcy5zdGF0ZS5wdXNoZXJzLCBhZGRyZXNzKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uRW5hYmxlRW1haWxOb3RpZmljYXRpb25zQ2hhbmdlLmJpbmQodGhpcywgYWRkcmVzcyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17bGFiZWx9IGtleT17YGVtYWlsTm90aWZfJHtsYWJlbH1gfSAvPjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgc3Bpbm5lcjtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5waGFzZSA9PT0gdGhpcy5waGFzZXMuTE9BRElORykge1xyXG4gICAgICAgICAgICBjb25zdCBMb2FkZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuICAgICAgICAgICAgc3Bpbm5lciA9IDxMb2FkZXIgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgbWFzdGVyUHVzaFJ1bGVEaXY7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUubWFzdGVyUHVzaFJ1bGUpIHtcclxuICAgICAgICAgICAgbWFzdGVyUHVzaFJ1bGVEaXYgPSA8TGFiZWxsZWRUb2dnbGVTd2l0Y2ggdmFsdWU9eyF0aGlzLnN0YXRlLm1hc3RlclB1c2hSdWxlLmVuYWJsZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uRW5hYmxlTm90aWZpY2F0aW9uc0NoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdFbmFibGUgbm90aWZpY2F0aW9ucyBmb3IgdGhpcyBhY2NvdW50Jyl9Lz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY2xlYXJOb3RpZmljYXRpb25zQnV0dG9uO1xyXG4gICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbXMoKS5zb21lKHIgPT4gci5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCgpID4gMCkpIHtcclxuICAgICAgICAgICAgY2xlYXJOb3RpZmljYXRpb25zQnV0dG9uID0gPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25DbGVhck5vdGlmaWNhdGlvbnN9IGtpbmQ9J2Rhbmdlcic+XHJcbiAgICAgICAgICAgICAgICB7X3QoXCJDbGVhciBub3RpZmljYXRpb25zXCIpfVxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gV2hlbiBlbmFibGVkLCB0aGUgbWFzdGVyIHJ1bGUgaW5oaWJpdHMgYWxsIGV4aXN0aW5nIHJ1bGVzXHJcbiAgICAgICAgLy8gU28gZG8gbm90IHNob3cgYWxsIG5vdGlmaWNhdGlvbiBzZXR0aW5nc1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1hc3RlclB1c2hSdWxlICYmIHRoaXMuc3RhdGUubWFzdGVyUHVzaFJ1bGUuZW5hYmxlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7bWFzdGVyUHVzaFJ1bGVEaXZ9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlck5vdGlmU2V0dGluZ3Nfbm90aWZUYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdBbGwgbm90aWZpY2F0aW9ucyBhcmUgY3VycmVudGx5IGRpc2FibGVkIGZvciBhbGwgdGFyZ2V0cy4nKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHtjbGVhck5vdGlmaWNhdGlvbnNCdXR0b259XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGVtYWlsVGhyZWVwaWRzID0gdGhpcy5zdGF0ZS50aHJlZXBpZHMuZmlsdGVyKCh0cCkgPT4gdHAubWVkaXVtID09PSBcImVtYWlsXCIpO1xyXG4gICAgICAgIGxldCBlbWFpbE5vdGlmaWNhdGlvbnNSb3dzO1xyXG4gICAgICAgIGlmIChlbWFpbFRocmVlcGlkcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgZW1haWxOb3RpZmljYXRpb25zUm93cyA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7IF90KCdBZGQgYW4gZW1haWwgYWRkcmVzcyB0byBjb25maWd1cmUgZW1haWwgbm90aWZpY2F0aW9ucycpIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGVtYWlsTm90aWZpY2F0aW9uc1Jvd3MgPSBlbWFpbFRocmVlcGlkcy5tYXAoKHRocmVlUGlkKSA9PiB0aGlzLmVtYWlsTm90aWZpY2F0aW9uc1JvdyhcclxuICAgICAgICAgICAgICAgIHRocmVlUGlkLmFkZHJlc3MsIGAke190KCdFbmFibGUgZW1haWwgbm90aWZpY2F0aW9ucycpfSAoJHt0aHJlZVBpZC5hZGRyZXNzfSlgLFxyXG4gICAgICAgICAgICApKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEJ1aWxkIGV4dGVybmFsIHB1c2ggcnVsZXNcclxuICAgICAgICBjb25zdCBleHRlcm5hbFJ1bGVzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHRoaXMuc3RhdGUuZXh0ZXJuYWxQdXNoUnVsZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgcnVsZSA9IHRoaXMuc3RhdGUuZXh0ZXJuYWxQdXNoUnVsZXNbaV07XHJcbiAgICAgICAgICAgIGV4dGVybmFsUnVsZXMucHVzaCg8bGk+eyBfdChydWxlLmRlc2NyaXB0aW9uKSB9PC9saT4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gU2hvdyBrZXl3b3JkcyBub3QgZGlzcGxheWVkIGJ5IHRoZSB2ZWN0b3IgVUkgYXMgYSBzaW5nbGUgZXh0ZXJuYWwgcHVzaCBydWxlXHJcbiAgICAgICAgbGV0IGV4dGVybmFsS2V5d29yZHMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gdGhpcy5zdGF0ZS5leHRlcm5hbENvbnRlbnRSdWxlcykge1xyXG4gICAgICAgICAgICBjb25zdCBydWxlID0gdGhpcy5zdGF0ZS5leHRlcm5hbENvbnRlbnRSdWxlc1tpXTtcclxuICAgICAgICAgICAgZXh0ZXJuYWxLZXl3b3Jkcy5wdXNoKHJ1bGUucGF0dGVybik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChleHRlcm5hbEtleXdvcmRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBleHRlcm5hbEtleXdvcmRzID0gZXh0ZXJuYWxLZXl3b3Jkcy5qb2luKFwiLCBcIik7XHJcbiAgICAgICAgICAgIGV4dGVybmFsUnVsZXMucHVzaCg8bGk+eyBfdCgnTm90aWZpY2F0aW9ucyBvbiB0aGUgZm9sbG93aW5nIGtleXdvcmRzIGZvbGxvdyBydWxlcyB3aGljaCBjYW7igJl0IGJlIGRpc3BsYXllZCBoZXJlOicpIH0geyBleHRlcm5hbEtleXdvcmRzIH08L2xpPik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZGV2aWNlc1NlY3Rpb247XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucHVzaGVycyA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIGRldmljZXNTZWN0aW9uID0gPGRpdiBjbGFzc05hbWU9XCJlcnJvclwiPnsgX3QoJ1VuYWJsZSB0byBmZXRjaCBub3RpZmljYXRpb24gdGFyZ2V0IGxpc3QnKSB9PC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5wdXNoZXJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICBkZXZpY2VzU2VjdGlvbiA9IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gVE9ETzogSXQgd291bGQgYmUgZ3JlYXQgdG8gYmUgYWJsZSB0byBkZWxldGUgcHVzaGVycyBmcm9tIGhlcmUgdG9vLFxyXG4gICAgICAgICAgICAvLyBhbmQgdGhpcyB3b3VsZG4ndCBiZSBoYXJkIHRvIGFkZC5cclxuICAgICAgICAgICAgY29uc3Qgcm93cyA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3RhdGUucHVzaGVycy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICAgICAgcm93cy5wdXNoKDx0ciBrZXk9eyBpIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPnt0aGlzLnN0YXRlLnB1c2hlcnNbaV0uYXBwX2Rpc3BsYXlfbmFtZX08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD57dGhpcy5zdGF0ZS5wdXNoZXJzW2ldLmRldmljZV9kaXNwbGF5X25hbWV9PC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdHI+KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkZXZpY2VzU2VjdGlvbiA9ICg8dGFibGUgY2xhc3NOYW1lPVwibXhfVXNlck5vdGlmU2V0dGluZ3NfZGV2aWNlc1RhYmxlXCI+XHJcbiAgICAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgICAgICAge3Jvd3N9XHJcbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChkZXZpY2VzU2VjdGlvbikge1xyXG4gICAgICAgICAgICBkZXZpY2VzU2VjdGlvbiA9ICg8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGgzPnsgX3QoJ05vdGlmaWNhdGlvbiB0YXJnZXRzJykgfTwvaDM+XHJcbiAgICAgICAgICAgICAgICB7IGRldmljZXNTZWN0aW9uIH1cclxuICAgICAgICAgICAgPC9kaXY+KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBhZHZhbmNlZFNldHRpbmdzO1xyXG4gICAgICAgIGlmIChleHRlcm5hbFJ1bGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBhZHZhbmNlZFNldHRpbmdzID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8aDM+eyBfdCgnQWR2YW5jZWQgbm90aWZpY2F0aW9uIHNldHRpbmdzJykgfTwvaDM+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnVGhlcmUgYXJlIGFkdmFuY2VkIG5vdGlmaWNhdGlvbnMgd2hpY2ggYXJlIG5vdCBzaG93biBoZXJlJykgfS48YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdZb3UgbWlnaHQgaGF2ZSBjb25maWd1cmVkIHRoZW0gaW4gYSBjbGllbnQgb3RoZXIgdGhhbiBSaW90LiBZb3UgY2Fubm90IHR1bmUgdGhlbSBpbiBSaW90IGJ1dCB0aGV5IHN0aWxsIGFwcGx5JykgfS5cclxuICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgZXh0ZXJuYWxSdWxlcyB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuXHJcbiAgICAgICAgICAgICAgICB7bWFzdGVyUHVzaFJ1bGVEaXZ9XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Vc2VyTm90aWZTZXR0aW5nc19ub3RpZlRhYmxlXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHsgc3Bpbm5lciB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbGxlZFRvZ2dsZVN3aXRjaCB2YWx1ZT17U2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcIm5vdGlmaWNhdGlvbnNFbmFibGVkXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkVuYWJsZURlc2t0b3BOb3RpZmljYXRpb25zQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoJ0VuYWJsZSBkZXNrdG9wIG5vdGlmaWNhdGlvbnMgZm9yIHRoaXMgc2Vzc2lvbicpfSAvPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8TGFiZWxsZWRUb2dnbGVTd2l0Y2ggdmFsdWU9e1NldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJub3RpZmljYXRpb25Cb2R5RW5hYmxlZFwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25FbmFibGVEZXNrdG9wTm90aWZpY2F0aW9uQm9keUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdTaG93IG1lc3NhZ2UgaW4gZGVza3RvcCBub3RpZmljYXRpb24nKX0gLz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsbGVkVG9nZ2xlU3dpdGNoIHZhbHVlPXtTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiYXVkaW9Ob3RpZmljYXRpb25zRW5hYmxlZFwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25FbmFibGVBdWRpb05vdGlmaWNhdGlvbnNDaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnRW5hYmxlIGF1ZGlibGUgbm90aWZpY2F0aW9ucyBmb3IgdGhpcyBzZXNzaW9uJyl9IC8+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHsgZW1haWxOb3RpZmljYXRpb25zUm93cyB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlck5vdGlmU2V0dGluZ3NfcHVzaFJ1bGVzVGFibGVXcmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzc05hbWU9XCJteF9Vc2VyTm90aWZTZXR0aW5nc19wdXNoUnVsZXNUYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIHdpZHRoPVwiNTUlXCI+PC90aD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIHdpZHRoPVwiMTUlXCI+eyBfdCgnT2ZmJykgfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aCB3aWR0aD1cIjE1JVwiPnsgX3QoJ09uJykgfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aCB3aWR0aD1cIjE1JVwiPnsgX3QoJ05vaXN5JykgfTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGJvZHk+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhpcy5yZW5kZXJOb3RpZlJ1bGVzVGFibGVSb3dzKCkgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdGFibGU+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHsgYWR2YW5jZWRTZXR0aW5ncyB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHsgZGV2aWNlc1NlY3Rpb24gfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7IGNsZWFyTm90aWZpY2F0aW9uc0J1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19