"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LabsSettingToggle = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../../../languageHandler");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _SettingsStore = _interopRequireWildcard(require("../../../../../settings/SettingsStore"));

var _LabelledToggleSwitch = _interopRequireDefault(require("../../../elements/LabelledToggleSwitch"));

var sdk = _interopRequireWildcard(require("../../../../../index"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class LabsSettingToggle extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onChange", async checked => {
      await _SettingsStore.default.setFeatureEnabled(this.props.featureId, checked);
      this.forceUpdate();
    });
  }

  render() {
    const label = _SettingsStore.default.getDisplayName(this.props.featureId);

    const value = _SettingsStore.default.isFeatureEnabled(this.props.featureId);

    return _react.default.createElement(_LabelledToggleSwitch.default, {
      value: value,
      label: label,
      onChange: this._onChange
    });
  }

}

exports.LabsSettingToggle = LabsSettingToggle;
(0, _defineProperty2.default)(LabsSettingToggle, "propTypes", {
  featureId: _propTypes.default.string.isRequired
});

class LabsUserSettingsTab extends _react.default.Component {
  constructor() {
    super();
  }

  render() {
    const SettingsFlag = sdk.getComponent("views.elements.SettingsFlag");

    const flags = _SettingsStore.default.getLabsFeatures().map(f => _react.default.createElement(LabsSettingToggle, {
      featureId: f,
      key: f
    }));

    return _react.default.createElement("div", {
      className: "mx_SettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Labs")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)('Customise your experience with experimental labs features. ' + '<a>Learn more</a>.', {}, {
      'a': sub => {
        return _react.default.createElement("a", {
          href: "https://github.com/vector-im/riot-web/blob/develop/docs/labs.md",
          rel: "noreferrer noopener",
          target: "_blank"
        }, sub);
      }
    })), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, flags, _react.default.createElement(SettingsFlag, {
      name: "enableWidgetScreenshots",
      level: _SettingsStore.SettingLevel.ACCOUNT
    }), _react.default.createElement(SettingsFlag, {
      name: "showHiddenEventsInTimeline",
      level: _SettingsStore.SettingLevel.DEVICE
    }), _react.default.createElement(SettingsFlag, {
      name: "lowBandwidth",
      level: _SettingsStore.SettingLevel.DEVICE
    }), _react.default.createElement(SettingsFlag, {
      name: "sendReadReceipts",
      level: _SettingsStore.SettingLevel.ACCOUNT
    }), _react.default.createElement(SettingsFlag, {
      name: "keepSecretStoragePassphraseForSession",
      level: _SettingsStore.SettingLevel.DEVICE
    })));
  }

}

exports.default = LabsUserSettingsTab;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9MYWJzVXNlclNldHRpbmdzVGFiLmpzIl0sIm5hbWVzIjpbIkxhYnNTZXR0aW5nVG9nZ2xlIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjaGVja2VkIiwiU2V0dGluZ3NTdG9yZSIsInNldEZlYXR1cmVFbmFibGVkIiwicHJvcHMiLCJmZWF0dXJlSWQiLCJmb3JjZVVwZGF0ZSIsInJlbmRlciIsImxhYmVsIiwiZ2V0RGlzcGxheU5hbWUiLCJ2YWx1ZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJfb25DaGFuZ2UiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiTGFic1VzZXJTZXR0aW5nc1RhYiIsImNvbnN0cnVjdG9yIiwiU2V0dGluZ3NGbGFnIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZmxhZ3MiLCJnZXRMYWJzRmVhdHVyZXMiLCJtYXAiLCJmIiwic3ViIiwiU2V0dGluZ0xldmVsIiwiQUNDT1VOVCIsIkRFVklDRSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQkE7Ozs7Ozs7Ozs7Ozs7OztBQXVCTyxNQUFNQSxpQkFBTixTQUFnQ0MsZUFBTUMsU0FBdEMsQ0FBZ0Q7QUFBQTtBQUFBO0FBQUEscURBS3ZDLE1BQU9DLE9BQVAsSUFBbUI7QUFDM0IsWUFBTUMsdUJBQWNDLGlCQUFkLENBQWdDLEtBQUtDLEtBQUwsQ0FBV0MsU0FBM0MsRUFBc0RKLE9BQXRELENBQU47QUFDQSxXQUFLSyxXQUFMO0FBQ0gsS0FSa0Q7QUFBQTs7QUFVbkRDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLEtBQUssR0FBR04sdUJBQWNPLGNBQWQsQ0FBNkIsS0FBS0wsS0FBTCxDQUFXQyxTQUF4QyxDQUFkOztBQUNBLFVBQU1LLEtBQUssR0FBR1IsdUJBQWNTLGdCQUFkLENBQStCLEtBQUtQLEtBQUwsQ0FBV0MsU0FBMUMsQ0FBZDs7QUFDQSxXQUFPLDZCQUFDLDZCQUFEO0FBQXNCLE1BQUEsS0FBSyxFQUFFSyxLQUE3QjtBQUFvQyxNQUFBLEtBQUssRUFBRUYsS0FBM0M7QUFBa0QsTUFBQSxRQUFRLEVBQUUsS0FBS0k7QUFBakUsTUFBUDtBQUNIOztBQWRrRDs7OzhCQUExQ2QsaUIsZUFDVTtBQUNmTyxFQUFBQSxTQUFTLEVBQUVRLG1CQUFVQyxNQUFWLENBQWlCQztBQURiLEM7O0FBZ0JSLE1BQU1DLG1CQUFOLFNBQWtDakIsZUFBTUMsU0FBeEMsQ0FBa0Q7QUFDN0RpQixFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQUNIOztBQUVEVixFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNVyxZQUFZLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBckI7O0FBQ0EsVUFBTUMsS0FBSyxHQUFHbkIsdUJBQWNvQixlQUFkLEdBQWdDQyxHQUFoQyxDQUFvQ0MsQ0FBQyxJQUFJLDZCQUFDLGlCQUFEO0FBQW1CLE1BQUEsU0FBUyxFQUFFQSxDQUE5QjtBQUFpQyxNQUFBLEdBQUcsRUFBRUE7QUFBdEMsTUFBekMsQ0FBZDs7QUFDQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF5Qyx5QkFBRyxNQUFILENBQXpDLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FFUSx5QkFBRyxnRUFDQyxvQkFESixFQUMwQixFQUQxQixFQUM4QjtBQUMxQixXQUFNQyxHQUFELElBQVM7QUFDVixlQUFPO0FBQUcsVUFBQSxJQUFJLEVBQUMsaUVBQVI7QUFDSCxVQUFBLEdBQUcsRUFBQyxxQkFERDtBQUN1QixVQUFBLE1BQU0sRUFBQztBQUQ5QixXQUN3Q0EsR0FEeEMsQ0FBUDtBQUVIO0FBSnlCLEtBRDlCLENBRlIsQ0FGSixFQWFJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLSixLQURMLEVBRUksNkJBQUMsWUFBRDtBQUFjLE1BQUEsSUFBSSxFQUFFLHlCQUFwQjtBQUErQyxNQUFBLEtBQUssRUFBRUssNEJBQWFDO0FBQW5FLE1BRkosRUFHSSw2QkFBQyxZQUFEO0FBQWMsTUFBQSxJQUFJLEVBQUUsNEJBQXBCO0FBQWtELE1BQUEsS0FBSyxFQUFFRCw0QkFBYUU7QUFBdEUsTUFISixFQUlJLDZCQUFDLFlBQUQ7QUFBYyxNQUFBLElBQUksRUFBRSxjQUFwQjtBQUFvQyxNQUFBLEtBQUssRUFBRUYsNEJBQWFFO0FBQXhELE1BSkosRUFLSSw2QkFBQyxZQUFEO0FBQWMsTUFBQSxJQUFJLEVBQUUsa0JBQXBCO0FBQXdDLE1BQUEsS0FBSyxFQUFFRiw0QkFBYUM7QUFBNUQsTUFMSixFQU1JLDZCQUFDLFlBQUQ7QUFBYyxNQUFBLElBQUksRUFBRSx1Q0FBcEI7QUFBNkQsTUFBQSxLQUFLLEVBQUVELDRCQUFhRTtBQUFqRixNQU5KLENBYkosQ0FESjtBQXdCSDs7QUFoQzREIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSBcInByb3AtdHlwZXNcIjtcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUsIHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBMYWJlbGxlZFRvZ2dsZVN3aXRjaCBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvTGFiZWxsZWRUb2dnbGVTd2l0Y2hcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9pbmRleFwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIExhYnNTZXR0aW5nVG9nZ2xlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgZmVhdHVyZUlkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkNoYW5nZSA9IGFzeW5jIChjaGVja2VkKSA9PiB7XHJcbiAgICAgICAgYXdhaXQgU2V0dGluZ3NTdG9yZS5zZXRGZWF0dXJlRW5hYmxlZCh0aGlzLnByb3BzLmZlYXR1cmVJZCwgY2hlY2tlZCk7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgbGFiZWwgPSBTZXR0aW5nc1N0b3JlLmdldERpc3BsYXlOYW1lKHRoaXMucHJvcHMuZmVhdHVyZUlkKTtcclxuICAgICAgICBjb25zdCB2YWx1ZSA9IFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZCh0aGlzLnByb3BzLmZlYXR1cmVJZCk7XHJcbiAgICAgICAgcmV0dXJuIDxMYWJlbGxlZFRvZ2dsZVN3aXRjaCB2YWx1ZT17dmFsdWV9IGxhYmVsPXtsYWJlbH0gb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfSAvPjtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGFic1VzZXJTZXR0aW5nc1RhYiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBTZXR0aW5nc0ZsYWcgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZWxlbWVudHMuU2V0dGluZ3NGbGFnXCIpO1xyXG4gICAgICAgIGNvbnN0IGZsYWdzID0gU2V0dGluZ3NTdG9yZS5nZXRMYWJzRmVhdHVyZXMoKS5tYXAoZiA9PiA8TGFic1NldHRpbmdUb2dnbGUgZmVhdHVyZUlkPXtmfSBrZXk9e2Z9IC8+KTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX2hlYWRpbmdcIj57X3QoXCJMYWJzXCIpfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90KCdDdXN0b21pc2UgeW91ciBleHBlcmllbmNlIHdpdGggZXhwZXJpbWVudGFsIGxhYnMgZmVhdHVyZXMuICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxhPkxlYXJuIG1vcmU8L2E+LicsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYSc6IChzdWIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvYmxvYi9kZXZlbG9wL2RvY3MvbGFicy5tZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbD0nbm9yZWZlcnJlciBub29wZW5lcicgdGFyZ2V0PSdfYmxhbmsnPntzdWJ9PC9hPjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3NlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICB7ZmxhZ3N9XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZyBuYW1lPXtcImVuYWJsZVdpZGdldFNjcmVlbnNob3RzXCJ9IGxldmVsPXtTZXR0aW5nTGV2ZWwuQUNDT1VOVH0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8U2V0dGluZ3NGbGFnIG5hbWU9e1wic2hvd0hpZGRlbkV2ZW50c0luVGltZWxpbmVcIn0gbGV2ZWw9e1NldHRpbmdMZXZlbC5ERVZJQ0V9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZyBuYW1lPXtcImxvd0JhbmR3aWR0aFwifSBsZXZlbD17U2V0dGluZ0xldmVsLkRFVklDRX0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8U2V0dGluZ3NGbGFnIG5hbWU9e1wic2VuZFJlYWRSZWNlaXB0c1wifSBsZXZlbD17U2V0dGluZ0xldmVsLkFDQ09VTlR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZyBuYW1lPXtcImtlZXBTZWNyZXRTdG9yYWdlUGFzc3BocmFzZUZvclNlc3Npb25cIn0gbGV2ZWw9e1NldHRpbmdMZXZlbC5ERVZJQ0V9IC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=