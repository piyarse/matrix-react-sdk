"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../../../languageHandler");

var _CallMediaHandler = _interopRequireDefault(require("../../../../../CallMediaHandler"));

var _Field = _interopRequireDefault(require("../../../elements/Field"));

var _AccessibleButton = _interopRequireDefault(require("../../../elements/AccessibleButton"));

var _SettingsStore = require("../../../../../settings/SettingsStore");

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../../index"));

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class VoiceUserSettingsTab extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_refreshMediaDevices", async stream => {
      this.setState({
        mediaDevices: await _CallMediaHandler.default.getDevices(),
        activeAudioOutput: _CallMediaHandler.default.getAudioOutput(),
        activeAudioInput: _CallMediaHandler.default.getAudioInput(),
        activeVideoInput: _CallMediaHandler.default.getVideoInput()
      });

      if (stream) {
        // kill stream (after we've enumerated the devices, otherwise we'd get empty labels again)
        // so that we don't leave it lingering around with webcam enabled etc
        // as here we called gUM to ask user for permission to their device names only
        stream.getTracks().forEach(track => track.stop());
      }
    });
    (0, _defineProperty2.default)(this, "_requestMediaPermissions", async () => {
      let constraints;
      let stream;
      let error;

      try {
        constraints = {
          video: true,
          audio: true
        };
        stream = await navigator.mediaDevices.getUserMedia(constraints);
      } catch (err) {
        // user likely doesn't have a webcam,
        // we should still allow to select a microphone
        if (err.name === "NotFoundError") {
          constraints = {
            audio: true
          };

          try {
            stream = await navigator.mediaDevices.getUserMedia(constraints);
          } catch (err) {
            error = err;
          }
        } else {
          error = err;
        }
      }

      if (error) {
        const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

        _Modal.default.createTrackedDialog('No media permissions', '', ErrorDialog, {
          title: (0, _languageHandler._t)('No media permissions'),
          description: (0, _languageHandler._t)('You may need to manually permit Riot to access your microphone/webcam')
        });
      } else {
        this._refreshMediaDevices(stream);
      }
    });
    (0, _defineProperty2.default)(this, "_setAudioOutput", e => {
      _CallMediaHandler.default.setAudioOutput(e.target.value);

      this.setState({
        activeAudioOutput: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_setAudioInput", e => {
      _CallMediaHandler.default.setAudioInput(e.target.value);

      this.setState({
        activeAudioInput: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_setVideoInput", e => {
      _CallMediaHandler.default.setVideoInput(e.target.value);

      this.setState({
        activeVideoInput: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_changeWebRtcMethod", p2p => {
      _MatrixClientPeg.MatrixClientPeg.get().setForceTURN(!p2p);
    });
    (0, _defineProperty2.default)(this, "_changeFallbackICEServerAllowed", allow => {
      _MatrixClientPeg.MatrixClientPeg.get().setFallbackICEServerAllowed(allow);
    });
    this.state = {
      mediaDevices: false,
      activeAudioOutput: null,
      activeAudioInput: null,
      activeVideoInput: null
    };
  }

  async componentDidMount() {
    const canSeeDeviceLabels = await _CallMediaHandler.default.hasAnyLabeledDevices();

    if (canSeeDeviceLabels) {
      this._refreshMediaDevices();
    }
  }

  _renderDeviceOptions(devices, category) {
    return devices.map(d => {
      return _react.default.createElement("option", {
        key: "".concat(category, "-").concat(d.deviceId),
        value: d.deviceId
      }, d.label);
    });
  }

  render() {
    const SettingsFlag = sdk.getComponent("views.elements.SettingsFlag");
    let requestButton = null;
    let speakerDropdown = null;
    let microphoneDropdown = null;
    let webcamDropdown = null;

    if (this.state.mediaDevices === false) {
      requestButton = _react.default.createElement("div", {
        className: "mx_VoiceUserSettingsTab_missingMediaPermissions"
      }, _react.default.createElement("p", null, (0, _languageHandler._t)("Missing media permissions, click the button below to request.")), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._requestMediaPermissions,
        kind: "primary"
      }, (0, _languageHandler._t)("Request media permissions")));
    } else if (this.state.mediaDevices) {
      speakerDropdown = _react.default.createElement("p", null, (0, _languageHandler._t)('No Audio Outputs detected'));
      microphoneDropdown = _react.default.createElement("p", null, (0, _languageHandler._t)('No Microphones detected'));
      webcamDropdown = _react.default.createElement("p", null, (0, _languageHandler._t)('No Webcams detected'));
      const defaultOption = {
        deviceId: '',
        label: (0, _languageHandler._t)('Default Device')
      };

      const getDefaultDevice = devices => {
        if (!devices.some(i => i.deviceId === 'default')) {
          devices.unshift(defaultOption);
          return '';
        } else {
          return 'default';
        }
      };

      const audioOutputs = this.state.mediaDevices.audiooutput.slice(0);

      if (audioOutputs.length > 0) {
        const defaultDevice = getDefaultDevice(audioOutputs);
        speakerDropdown = _react.default.createElement(_Field.default, {
          element: "select",
          label: (0, _languageHandler._t)("Audio Output"),
          value: this.state.activeAudioOutput || defaultDevice,
          onChange: this._setAudioOutput
        }, this._renderDeviceOptions(audioOutputs, 'audioOutput'));
      }

      const audioInputs = this.state.mediaDevices.audioinput.slice(0);

      if (audioInputs.length > 0) {
        const defaultDevice = getDefaultDevice(audioInputs);
        microphoneDropdown = _react.default.createElement(_Field.default, {
          element: "select",
          label: (0, _languageHandler._t)("Microphone"),
          value: this.state.activeAudioInput || defaultDevice,
          onChange: this._setAudioInput
        }, this._renderDeviceOptions(audioInputs, 'audioInput'));
      }

      const videoInputs = this.state.mediaDevices.videoinput.slice(0);

      if (videoInputs.length > 0) {
        const defaultDevice = getDefaultDevice(videoInputs);
        webcamDropdown = _react.default.createElement(_Field.default, {
          element: "select",
          label: (0, _languageHandler._t)("Camera"),
          value: this.state.activeVideoInput || defaultDevice,
          onChange: this._setVideoInput
        }, this._renderDeviceOptions(videoInputs, 'videoInput'));
      }
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_VoiceUserSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Voice & Video")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, requestButton, speakerDropdown, microphoneDropdown, webcamDropdown, _react.default.createElement(SettingsFlag, {
      name: "VideoView.flipVideoHorizontally",
      level: _SettingsStore.SettingLevel.ACCOUNT
    }), _react.default.createElement(SettingsFlag, {
      name: "webRtcAllowPeerToPeer",
      level: _SettingsStore.SettingLevel.DEVICE,
      onChange: this._changeWebRtcMethod
    }), _react.default.createElement(SettingsFlag, {
      name: "fallbackICEServerAllowed",
      level: _SettingsStore.SettingLevel.DEVICE,
      onChange: this._changeFallbackICEServerAllowed
    })));
  }

}

exports.default = VoiceUserSettingsTab;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9Wb2ljZVVzZXJTZXR0aW5nc1RhYi5qcyJdLCJuYW1lcyI6WyJWb2ljZVVzZXJTZXR0aW5nc1RhYiIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJzdHJlYW0iLCJzZXRTdGF0ZSIsIm1lZGlhRGV2aWNlcyIsIkNhbGxNZWRpYUhhbmRsZXIiLCJnZXREZXZpY2VzIiwiYWN0aXZlQXVkaW9PdXRwdXQiLCJnZXRBdWRpb091dHB1dCIsImFjdGl2ZUF1ZGlvSW5wdXQiLCJnZXRBdWRpb0lucHV0IiwiYWN0aXZlVmlkZW9JbnB1dCIsImdldFZpZGVvSW5wdXQiLCJnZXRUcmFja3MiLCJmb3JFYWNoIiwidHJhY2siLCJzdG9wIiwiY29uc3RyYWludHMiLCJlcnJvciIsInZpZGVvIiwiYXVkaW8iLCJuYXZpZ2F0b3IiLCJnZXRVc2VyTWVkaWEiLCJlcnIiLCJuYW1lIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiX3JlZnJlc2hNZWRpYURldmljZXMiLCJlIiwic2V0QXVkaW9PdXRwdXQiLCJ0YXJnZXQiLCJ2YWx1ZSIsInNldEF1ZGlvSW5wdXQiLCJzZXRWaWRlb0lucHV0IiwicDJwIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwic2V0Rm9yY2VUVVJOIiwiYWxsb3ciLCJzZXRGYWxsYmFja0lDRVNlcnZlckFsbG93ZWQiLCJzdGF0ZSIsImNvbXBvbmVudERpZE1vdW50IiwiY2FuU2VlRGV2aWNlTGFiZWxzIiwiaGFzQW55TGFiZWxlZERldmljZXMiLCJfcmVuZGVyRGV2aWNlT3B0aW9ucyIsImRldmljZXMiLCJjYXRlZ29yeSIsIm1hcCIsImQiLCJkZXZpY2VJZCIsImxhYmVsIiwicmVuZGVyIiwiU2V0dGluZ3NGbGFnIiwicmVxdWVzdEJ1dHRvbiIsInNwZWFrZXJEcm9wZG93biIsIm1pY3JvcGhvbmVEcm9wZG93biIsIndlYmNhbURyb3Bkb3duIiwiX3JlcXVlc3RNZWRpYVBlcm1pc3Npb25zIiwiZGVmYXVsdE9wdGlvbiIsImdldERlZmF1bHREZXZpY2UiLCJzb21lIiwiaSIsInVuc2hpZnQiLCJhdWRpb091dHB1dHMiLCJhdWRpb291dHB1dCIsInNsaWNlIiwibGVuZ3RoIiwiZGVmYXVsdERldmljZSIsIl9zZXRBdWRpb091dHB1dCIsImF1ZGlvSW5wdXRzIiwiYXVkaW9pbnB1dCIsIl9zZXRBdWRpb0lucHV0IiwidmlkZW9JbnB1dHMiLCJ2aWRlb2lucHV0IiwiX3NldFZpZGVvSW5wdXQiLCJTZXR0aW5nTGV2ZWwiLCJBQ0NPVU5UIiwiREVWSUNFIiwiX2NoYW5nZVdlYlJ0Y01ldGhvZCIsIl9jaGFuZ2VGYWxsYmFja0lDRVNlcnZlckFsbG93ZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBeEJBOzs7Ozs7Ozs7Ozs7Ozs7QUEwQmUsTUFBTUEsb0JBQU4sU0FBbUNDLGVBQU1DLFNBQXpDLENBQW1EO0FBQzlEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLGdFQWtCUyxNQUFPQyxNQUFQLElBQWtCO0FBQ3JDLFdBQUtDLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSxZQUFZLEVBQUUsTUFBTUMsMEJBQWlCQyxVQUFqQixFQURWO0FBRVZDLFFBQUFBLGlCQUFpQixFQUFFRiwwQkFBaUJHLGNBQWpCLEVBRlQ7QUFHVkMsUUFBQUEsZ0JBQWdCLEVBQUVKLDBCQUFpQkssYUFBakIsRUFIUjtBQUlWQyxRQUFBQSxnQkFBZ0IsRUFBRU4sMEJBQWlCTyxhQUFqQjtBQUpSLE9BQWQ7O0FBTUEsVUFBSVYsTUFBSixFQUFZO0FBQ1I7QUFDQTtBQUNBO0FBQ0FBLFFBQUFBLE1BQU0sQ0FBQ1csU0FBUCxHQUFtQkMsT0FBbkIsQ0FBNEJDLEtBQUQsSUFBV0EsS0FBSyxDQUFDQyxJQUFOLEVBQXRDO0FBQ0g7QUFDSixLQS9CYTtBQUFBLG9FQWlDYSxZQUFZO0FBQ25DLFVBQUlDLFdBQUo7QUFDQSxVQUFJZixNQUFKO0FBQ0EsVUFBSWdCLEtBQUo7O0FBQ0EsVUFBSTtBQUNBRCxRQUFBQSxXQUFXLEdBQUc7QUFBQ0UsVUFBQUEsS0FBSyxFQUFFLElBQVI7QUFBY0MsVUFBQUEsS0FBSyxFQUFFO0FBQXJCLFNBQWQ7QUFDQWxCLFFBQUFBLE1BQU0sR0FBRyxNQUFNbUIsU0FBUyxDQUFDakIsWUFBVixDQUF1QmtCLFlBQXZCLENBQW9DTCxXQUFwQyxDQUFmO0FBQ0gsT0FIRCxDQUdFLE9BQU9NLEdBQVAsRUFBWTtBQUNWO0FBQ0E7QUFDQSxZQUFJQSxHQUFHLENBQUNDLElBQUosS0FBYSxlQUFqQixFQUFrQztBQUM5QlAsVUFBQUEsV0FBVyxHQUFHO0FBQUVHLFlBQUFBLEtBQUssRUFBRTtBQUFULFdBQWQ7O0FBQ0EsY0FBSTtBQUNBbEIsWUFBQUEsTUFBTSxHQUFHLE1BQU1tQixTQUFTLENBQUNqQixZQUFWLENBQXVCa0IsWUFBdkIsQ0FBb0NMLFdBQXBDLENBQWY7QUFDSCxXQUZELENBRUUsT0FBT00sR0FBUCxFQUFZO0FBQ1ZMLFlBQUFBLEtBQUssR0FBR0ssR0FBUjtBQUNIO0FBQ0osU0FQRCxNQU9PO0FBQ0hMLFVBQUFBLEtBQUssR0FBR0ssR0FBUjtBQUNIO0FBQ0o7O0FBQ0QsVUFBSUwsS0FBSixFQUFXO0FBQ1AsY0FBTU8sV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx1QkFBTUMsbUJBQU4sQ0FBMEIsc0JBQTFCLEVBQWtELEVBQWxELEVBQXNESixXQUF0RCxFQUFtRTtBQUMvREssVUFBQUEsS0FBSyxFQUFFLHlCQUFHLHNCQUFILENBRHdEO0FBRS9EQyxVQUFBQSxXQUFXLEVBQUUseUJBQUcsdUVBQUg7QUFGa0QsU0FBbkU7QUFJSCxPQU5ELE1BTU87QUFDSCxhQUFLQyxvQkFBTCxDQUEwQjlCLE1BQTFCO0FBQ0g7QUFDSixLQS9EYTtBQUFBLDJEQWlFSytCLENBQUQsSUFBTztBQUNyQjVCLGdDQUFpQjZCLGNBQWpCLENBQWdDRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBekM7O0FBQ0EsV0FBS2pDLFFBQUwsQ0FBYztBQUNWSSxRQUFBQSxpQkFBaUIsRUFBRTBCLENBQUMsQ0FBQ0UsTUFBRixDQUFTQztBQURsQixPQUFkO0FBR0gsS0F0RWE7QUFBQSwwREF3RUlILENBQUQsSUFBTztBQUNwQjVCLGdDQUFpQmdDLGFBQWpCLENBQStCSixDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBeEM7O0FBQ0EsV0FBS2pDLFFBQUwsQ0FBYztBQUNWTSxRQUFBQSxnQkFBZ0IsRUFBRXdCLENBQUMsQ0FBQ0UsTUFBRixDQUFTQztBQURqQixPQUFkO0FBR0gsS0E3RWE7QUFBQSwwREErRUlILENBQUQsSUFBTztBQUNwQjVCLGdDQUFpQmlDLGFBQWpCLENBQStCTCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBeEM7O0FBQ0EsV0FBS2pDLFFBQUwsQ0FBYztBQUNWUSxRQUFBQSxnQkFBZ0IsRUFBRXNCLENBQUMsQ0FBQ0UsTUFBRixDQUFTQztBQURqQixPQUFkO0FBR0gsS0FwRmE7QUFBQSwrREFzRlNHLEdBQUQsSUFBUztBQUMzQkMsdUNBQWdCQyxHQUFoQixHQUFzQkMsWUFBdEIsQ0FBbUMsQ0FBQ0gsR0FBcEM7QUFDSCxLQXhGYTtBQUFBLDJFQTBGcUJJLEtBQUQsSUFBVztBQUN6Q0gsdUNBQWdCQyxHQUFoQixHQUFzQkcsMkJBQXRCLENBQWtERCxLQUFsRDtBQUNILEtBNUZhO0FBR1YsU0FBS0UsS0FBTCxHQUFhO0FBQ1R6QyxNQUFBQSxZQUFZLEVBQUUsS0FETDtBQUVURyxNQUFBQSxpQkFBaUIsRUFBRSxJQUZWO0FBR1RFLE1BQUFBLGdCQUFnQixFQUFFLElBSFQ7QUFJVEUsTUFBQUEsZ0JBQWdCLEVBQUU7QUFKVCxLQUFiO0FBTUg7O0FBRUQsUUFBTW1DLGlCQUFOLEdBQTBCO0FBQ3RCLFVBQU1DLGtCQUFrQixHQUFHLE1BQU0xQywwQkFBaUIyQyxvQkFBakIsRUFBakM7O0FBQ0EsUUFBSUQsa0JBQUosRUFBd0I7QUFDcEIsV0FBS2Ysb0JBQUw7QUFDSDtBQUNKOztBQThFRGlCLEVBQUFBLG9CQUFvQixDQUFDQyxPQUFELEVBQVVDLFFBQVYsRUFBb0I7QUFDcEMsV0FBT0QsT0FBTyxDQUFDRSxHQUFSLENBQWFDLENBQUQsSUFBTztBQUN0QixhQUFRO0FBQVEsUUFBQSxHQUFHLFlBQUtGLFFBQUwsY0FBaUJFLENBQUMsQ0FBQ0MsUUFBbkIsQ0FBWDtBQUEwQyxRQUFBLEtBQUssRUFBRUQsQ0FBQyxDQUFDQztBQUFuRCxTQUE4REQsQ0FBQyxDQUFDRSxLQUFoRSxDQUFSO0FBQ0gsS0FGTSxDQUFQO0FBR0g7O0FBRURDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLFlBQVksR0FBRy9CLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBckI7QUFFQSxRQUFJK0IsYUFBYSxHQUFHLElBQXBCO0FBQ0EsUUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsUUFBSUMsa0JBQWtCLEdBQUcsSUFBekI7QUFDQSxRQUFJQyxjQUFjLEdBQUcsSUFBckI7O0FBQ0EsUUFBSSxLQUFLaEIsS0FBTCxDQUFXekMsWUFBWCxLQUE0QixLQUFoQyxFQUF1QztBQUNuQ3NELE1BQUFBLGFBQWEsR0FDVDtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSx3Q0FBSSx5QkFBRywrREFBSCxDQUFKLENBREosRUFFSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLSSx3QkFBaEM7QUFBMEQsUUFBQSxJQUFJLEVBQUM7QUFBL0QsU0FDSyx5QkFBRywyQkFBSCxDQURMLENBRkosQ0FESjtBQVFILEtBVEQsTUFTTyxJQUFJLEtBQUtqQixLQUFMLENBQVd6QyxZQUFmLEVBQTZCO0FBQ2hDdUQsTUFBQUEsZUFBZSxHQUFHLHdDQUFLLHlCQUFHLDJCQUFILENBQUwsQ0FBbEI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsd0NBQUsseUJBQUcseUJBQUgsQ0FBTCxDQUFyQjtBQUNBQyxNQUFBQSxjQUFjLEdBQUcsd0NBQUsseUJBQUcscUJBQUgsQ0FBTCxDQUFqQjtBQUVBLFlBQU1FLGFBQWEsR0FBRztBQUNsQlQsUUFBQUEsUUFBUSxFQUFFLEVBRFE7QUFFbEJDLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxnQkFBSDtBQUZXLE9BQXRCOztBQUlBLFlBQU1TLGdCQUFnQixHQUFJZCxPQUFELElBQWE7QUFDbEMsWUFBSSxDQUFDQSxPQUFPLENBQUNlLElBQVIsQ0FBY0MsQ0FBRCxJQUFPQSxDQUFDLENBQUNaLFFBQUYsS0FBZSxTQUFuQyxDQUFMLEVBQW9EO0FBQ2hESixVQUFBQSxPQUFPLENBQUNpQixPQUFSLENBQWdCSixhQUFoQjtBQUNBLGlCQUFPLEVBQVA7QUFDSCxTQUhELE1BR087QUFDSCxpQkFBTyxTQUFQO0FBQ0g7QUFDSixPQVBEOztBQVNBLFlBQU1LLFlBQVksR0FBRyxLQUFLdkIsS0FBTCxDQUFXekMsWUFBWCxDQUF3QmlFLFdBQXhCLENBQW9DQyxLQUFwQyxDQUEwQyxDQUExQyxDQUFyQjs7QUFDQSxVQUFJRixZQUFZLENBQUNHLE1BQWIsR0FBc0IsQ0FBMUIsRUFBNkI7QUFDekIsY0FBTUMsYUFBYSxHQUFHUixnQkFBZ0IsQ0FBQ0ksWUFBRCxDQUF0QztBQUNBVCxRQUFBQSxlQUFlLEdBQ1gsNkJBQUMsY0FBRDtBQUFPLFVBQUEsT0FBTyxFQUFDLFFBQWY7QUFBd0IsVUFBQSxLQUFLLEVBQUUseUJBQUcsY0FBSCxDQUEvQjtBQUNPLFVBQUEsS0FBSyxFQUFFLEtBQUtkLEtBQUwsQ0FBV3RDLGlCQUFYLElBQWdDaUUsYUFEOUM7QUFFTyxVQUFBLFFBQVEsRUFBRSxLQUFLQztBQUZ0QixXQUdLLEtBQUt4QixvQkFBTCxDQUEwQm1CLFlBQTFCLEVBQXdDLGFBQXhDLENBSEwsQ0FESjtBQU9IOztBQUVELFlBQU1NLFdBQVcsR0FBRyxLQUFLN0IsS0FBTCxDQUFXekMsWUFBWCxDQUF3QnVFLFVBQXhCLENBQW1DTCxLQUFuQyxDQUF5QyxDQUF6QyxDQUFwQjs7QUFDQSxVQUFJSSxXQUFXLENBQUNILE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDeEIsY0FBTUMsYUFBYSxHQUFHUixnQkFBZ0IsQ0FBQ1UsV0FBRCxDQUF0QztBQUNBZCxRQUFBQSxrQkFBa0IsR0FDZCw2QkFBQyxjQUFEO0FBQU8sVUFBQSxPQUFPLEVBQUMsUUFBZjtBQUF3QixVQUFBLEtBQUssRUFBRSx5QkFBRyxZQUFILENBQS9CO0FBQ08sVUFBQSxLQUFLLEVBQUUsS0FBS2YsS0FBTCxDQUFXcEMsZ0JBQVgsSUFBK0IrRCxhQUQ3QztBQUVPLFVBQUEsUUFBUSxFQUFFLEtBQUtJO0FBRnRCLFdBR0ssS0FBSzNCLG9CQUFMLENBQTBCeUIsV0FBMUIsRUFBdUMsWUFBdkMsQ0FITCxDQURKO0FBT0g7O0FBRUQsWUFBTUcsV0FBVyxHQUFHLEtBQUtoQyxLQUFMLENBQVd6QyxZQUFYLENBQXdCMEUsVUFBeEIsQ0FBbUNSLEtBQW5DLENBQXlDLENBQXpDLENBQXBCOztBQUNBLFVBQUlPLFdBQVcsQ0FBQ04sTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QixjQUFNQyxhQUFhLEdBQUdSLGdCQUFnQixDQUFDYSxXQUFELENBQXRDO0FBQ0FoQixRQUFBQSxjQUFjLEdBQ1YsNkJBQUMsY0FBRDtBQUFPLFVBQUEsT0FBTyxFQUFDLFFBQWY7QUFBd0IsVUFBQSxLQUFLLEVBQUUseUJBQUcsUUFBSCxDQUEvQjtBQUNPLFVBQUEsS0FBSyxFQUFFLEtBQUtoQixLQUFMLENBQVdsQyxnQkFBWCxJQUErQjZELGFBRDdDO0FBRU8sVUFBQSxRQUFRLEVBQUUsS0FBS087QUFGdEIsV0FHSyxLQUFLOUIsb0JBQUwsQ0FBMEI0QixXQUExQixFQUF1QyxZQUF2QyxDQUhMLENBREo7QUFPSDtBQUNKOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlDLHlCQUFHLGVBQUgsQ0FBekMsQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLbkIsYUFETCxFQUVLQyxlQUZMLEVBR0tDLGtCQUhMLEVBSUtDLGNBSkwsRUFLSSw2QkFBQyxZQUFEO0FBQWMsTUFBQSxJQUFJLEVBQUMsaUNBQW5CO0FBQXFELE1BQUEsS0FBSyxFQUFFbUIsNEJBQWFDO0FBQXpFLE1BTEosRUFNSSw2QkFBQyxZQUFEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsdUJBRFQ7QUFFSSxNQUFBLEtBQUssRUFBRUQsNEJBQWFFLE1BRnhCO0FBR0ksTUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFIbkIsTUFOSixFQVdJLDZCQUFDLFlBQUQ7QUFDSSxNQUFBLElBQUksRUFBQywwQkFEVDtBQUVJLE1BQUEsS0FBSyxFQUFFSCw0QkFBYUUsTUFGeEI7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLRTtBQUhuQixNQVhKLENBRkosQ0FESjtBQXNCSDs7QUFsTTZEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCBDYWxsTWVkaWFIYW5kbGVyIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9DYWxsTWVkaWFIYW5kbGVyXCI7XHJcbmltcG9ydCBGaWVsZCBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvRmllbGRcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4uLy4uLy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuaW1wb3J0IHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9Nb2RhbFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVm9pY2VVc2VyU2V0dGluZ3NUYWIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgbWVkaWFEZXZpY2VzOiBmYWxzZSxcclxuICAgICAgICAgICAgYWN0aXZlQXVkaW9PdXRwdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGFjdGl2ZUF1ZGlvSW5wdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGFjdGl2ZVZpZGVvSW5wdXQ6IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBjb25zdCBjYW5TZWVEZXZpY2VMYWJlbHMgPSBhd2FpdCBDYWxsTWVkaWFIYW5kbGVyLmhhc0FueUxhYmVsZWREZXZpY2VzKCk7XHJcbiAgICAgICAgaWYgKGNhblNlZURldmljZUxhYmVscykge1xyXG4gICAgICAgICAgICB0aGlzLl9yZWZyZXNoTWVkaWFEZXZpY2VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9yZWZyZXNoTWVkaWFEZXZpY2VzID0gYXN5bmMgKHN0cmVhbSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBtZWRpYURldmljZXM6IGF3YWl0IENhbGxNZWRpYUhhbmRsZXIuZ2V0RGV2aWNlcygpLFxyXG4gICAgICAgICAgICBhY3RpdmVBdWRpb091dHB1dDogQ2FsbE1lZGlhSGFuZGxlci5nZXRBdWRpb091dHB1dCgpLFxyXG4gICAgICAgICAgICBhY3RpdmVBdWRpb0lucHV0OiBDYWxsTWVkaWFIYW5kbGVyLmdldEF1ZGlvSW5wdXQoKSxcclxuICAgICAgICAgICAgYWN0aXZlVmlkZW9JbnB1dDogQ2FsbE1lZGlhSGFuZGxlci5nZXRWaWRlb0lucHV0KCksXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHN0cmVhbSkge1xyXG4gICAgICAgICAgICAvLyBraWxsIHN0cmVhbSAoYWZ0ZXIgd2UndmUgZW51bWVyYXRlZCB0aGUgZGV2aWNlcywgb3RoZXJ3aXNlIHdlJ2QgZ2V0IGVtcHR5IGxhYmVscyBhZ2FpbilcclxuICAgICAgICAgICAgLy8gc28gdGhhdCB3ZSBkb24ndCBsZWF2ZSBpdCBsaW5nZXJpbmcgYXJvdW5kIHdpdGggd2ViY2FtIGVuYWJsZWQgZXRjXHJcbiAgICAgICAgICAgIC8vIGFzIGhlcmUgd2UgY2FsbGVkIGdVTSB0byBhc2sgdXNlciBmb3IgcGVybWlzc2lvbiB0byB0aGVpciBkZXZpY2UgbmFtZXMgb25seVxyXG4gICAgICAgICAgICBzdHJlYW0uZ2V0VHJhY2tzKCkuZm9yRWFjaCgodHJhY2spID0+IHRyYWNrLnN0b3AoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcmVxdWVzdE1lZGlhUGVybWlzc2lvbnMgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgbGV0IGNvbnN0cmFpbnRzO1xyXG4gICAgICAgIGxldCBzdHJlYW07XHJcbiAgICAgICAgbGV0IGVycm9yO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0cmFpbnRzID0ge3ZpZGVvOiB0cnVlLCBhdWRpbzogdHJ1ZX07XHJcbiAgICAgICAgICAgIHN0cmVhbSA9IGF3YWl0IG5hdmlnYXRvci5tZWRpYURldmljZXMuZ2V0VXNlck1lZGlhKGNvbnN0cmFpbnRzKTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgLy8gdXNlciBsaWtlbHkgZG9lc24ndCBoYXZlIGEgd2ViY2FtLFxyXG4gICAgICAgICAgICAvLyB3ZSBzaG91bGQgc3RpbGwgYWxsb3cgdG8gc2VsZWN0IGEgbWljcm9waG9uZVxyXG4gICAgICAgICAgICBpZiAoZXJyLm5hbWUgPT09IFwiTm90Rm91bmRFcnJvclwiKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdHJhaW50cyA9IHsgYXVkaW86IHRydWUgfTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RyZWFtID0gYXdhaXQgbmF2aWdhdG9yLm1lZGlhRGV2aWNlcy5nZXRVc2VyTWVkaWEoY29uc3RyYWludHMpO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IgPSBlcnI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBlcnJvciA9IGVycjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXJyb3IpIHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLkVycm9yRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ05vIG1lZGlhIHBlcm1pc3Npb25zJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ05vIG1lZGlhIHBlcm1pc3Npb25zJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ1lvdSBtYXkgbmVlZCB0byBtYW51YWxseSBwZXJtaXQgUmlvdCB0byBhY2Nlc3MgeW91ciBtaWNyb3Bob25lL3dlYmNhbScpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZWZyZXNoTWVkaWFEZXZpY2VzKHN0cmVhbSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfc2V0QXVkaW9PdXRwdXQgPSAoZSkgPT4ge1xyXG4gICAgICAgIENhbGxNZWRpYUhhbmRsZXIuc2V0QXVkaW9PdXRwdXQoZS50YXJnZXQudmFsdWUpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBhY3RpdmVBdWRpb091dHB1dDogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9zZXRBdWRpb0lucHV0ID0gKGUpID0+IHtcclxuICAgICAgICBDYWxsTWVkaWFIYW5kbGVyLnNldEF1ZGlvSW5wdXQoZS50YXJnZXQudmFsdWUpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBhY3RpdmVBdWRpb0lucHV0OiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3NldFZpZGVvSW5wdXQgPSAoZSkgPT4ge1xyXG4gICAgICAgIENhbGxNZWRpYUhhbmRsZXIuc2V0VmlkZW9JbnB1dChlLnRhcmdldC52YWx1ZSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGFjdGl2ZVZpZGVvSW5wdXQ6IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY2hhbmdlV2ViUnRjTWV0aG9kID0gKHAycCkgPT4ge1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZXRGb3JjZVRVUk4oIXAycCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jaGFuZ2VGYWxsYmFja0lDRVNlcnZlckFsbG93ZWQgPSAoYWxsb3cpID0+IHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0RmFsbGJhY2tJQ0VTZXJ2ZXJBbGxvd2VkKGFsbG93KTtcclxuICAgIH07XHJcblxyXG4gICAgX3JlbmRlckRldmljZU9wdGlvbnMoZGV2aWNlcywgY2F0ZWdvcnkpIHtcclxuICAgICAgICByZXR1cm4gZGV2aWNlcy5tYXAoKGQpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuICg8b3B0aW9uIGtleT17YCR7Y2F0ZWdvcnl9LSR7ZC5kZXZpY2VJZH1gfSB2YWx1ZT17ZC5kZXZpY2VJZH0+e2QubGFiZWx9PC9vcHRpb24+KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgU2V0dGluZ3NGbGFnID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLlNldHRpbmdzRmxhZ1wiKTtcclxuXHJcbiAgICAgICAgbGV0IHJlcXVlc3RCdXR0b24gPSBudWxsO1xyXG4gICAgICAgIGxldCBzcGVha2VyRHJvcGRvd24gPSBudWxsO1xyXG4gICAgICAgIGxldCBtaWNyb3Bob25lRHJvcGRvd24gPSBudWxsO1xyXG4gICAgICAgIGxldCB3ZWJjYW1Ecm9wZG93biA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUubWVkaWFEZXZpY2VzID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICByZXF1ZXN0QnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1ZvaWNlVXNlclNldHRpbmdzVGFiX21pc3NpbmdNZWRpYVBlcm1pc3Npb25zJz5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXCJNaXNzaW5nIG1lZGlhIHBlcm1pc3Npb25zLCBjbGljayB0aGUgYnV0dG9uIGJlbG93IHRvIHJlcXVlc3QuXCIpfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9yZXF1ZXN0TWVkaWFQZXJtaXNzaW9uc30ga2luZD1cInByaW1hcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiUmVxdWVzdCBtZWRpYSBwZXJtaXNzaW9uc1wiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUubWVkaWFEZXZpY2VzKSB7XHJcbiAgICAgICAgICAgIHNwZWFrZXJEcm9wZG93biA9IDxwPnsgX3QoJ05vIEF1ZGlvIE91dHB1dHMgZGV0ZWN0ZWQnKSB9PC9wPjtcclxuICAgICAgICAgICAgbWljcm9waG9uZURyb3Bkb3duID0gPHA+eyBfdCgnTm8gTWljcm9waG9uZXMgZGV0ZWN0ZWQnKSB9PC9wPjtcclxuICAgICAgICAgICAgd2ViY2FtRHJvcGRvd24gPSA8cD57IF90KCdObyBXZWJjYW1zIGRldGVjdGVkJykgfTwvcD47XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkZWZhdWx0T3B0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgZGV2aWNlSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgbGFiZWw6IF90KCdEZWZhdWx0IERldmljZScpLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBjb25zdCBnZXREZWZhdWx0RGV2aWNlID0gKGRldmljZXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghZGV2aWNlcy5zb21lKChpKSA9PiBpLmRldmljZUlkID09PSAnZGVmYXVsdCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGV2aWNlcy51bnNoaWZ0KGRlZmF1bHRPcHRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdkZWZhdWx0JztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGF1ZGlvT3V0cHV0cyA9IHRoaXMuc3RhdGUubWVkaWFEZXZpY2VzLmF1ZGlvb3V0cHV0LnNsaWNlKDApO1xyXG4gICAgICAgICAgICBpZiAoYXVkaW9PdXRwdXRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlZmF1bHREZXZpY2UgPSBnZXREZWZhdWx0RGV2aWNlKGF1ZGlvT3V0cHV0cyk7XHJcbiAgICAgICAgICAgICAgICBzcGVha2VyRHJvcGRvd24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEZpZWxkIGVsZW1lbnQ9XCJzZWxlY3RcIiBsYWJlbD17X3QoXCJBdWRpbyBPdXRwdXRcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLmFjdGl2ZUF1ZGlvT3V0cHV0IHx8IGRlZmF1bHREZXZpY2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9zZXRBdWRpb091dHB1dH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJEZXZpY2VPcHRpb25zKGF1ZGlvT3V0cHV0cywgJ2F1ZGlvT3V0cHV0Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9GaWVsZD5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGF1ZGlvSW5wdXRzID0gdGhpcy5zdGF0ZS5tZWRpYURldmljZXMuYXVkaW9pbnB1dC5zbGljZSgwKTtcclxuICAgICAgICAgICAgaWYgKGF1ZGlvSW5wdXRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlZmF1bHREZXZpY2UgPSBnZXREZWZhdWx0RGV2aWNlKGF1ZGlvSW5wdXRzKTtcclxuICAgICAgICAgICAgICAgIG1pY3JvcGhvbmVEcm9wZG93biA9IChcclxuICAgICAgICAgICAgICAgICAgICA8RmllbGQgZWxlbWVudD1cInNlbGVjdFwiIGxhYmVsPXtfdChcIk1pY3JvcGhvbmVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLmFjdGl2ZUF1ZGlvSW5wdXQgfHwgZGVmYXVsdERldmljZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX3NldEF1ZGlvSW5wdXR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyRGV2aWNlT3B0aW9ucyhhdWRpb0lucHV0cywgJ2F1ZGlvSW5wdXQnKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0ZpZWxkPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgdmlkZW9JbnB1dHMgPSB0aGlzLnN0YXRlLm1lZGlhRGV2aWNlcy52aWRlb2lucHV0LnNsaWNlKDApO1xyXG4gICAgICAgICAgICBpZiAodmlkZW9JbnB1dHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGVmYXVsdERldmljZSA9IGdldERlZmF1bHREZXZpY2UodmlkZW9JbnB1dHMpO1xyXG4gICAgICAgICAgICAgICAgd2ViY2FtRHJvcGRvd24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEZpZWxkIGVsZW1lbnQ9XCJzZWxlY3RcIiBsYWJlbD17X3QoXCJDYW1lcmFcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLmFjdGl2ZVZpZGVvSW5wdXQgfHwgZGVmYXVsdERldmljZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX3NldFZpZGVvSW5wdXR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyRGV2aWNlT3B0aW9ucyh2aWRlb0lucHV0cywgJ3ZpZGVvSW5wdXQnKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0ZpZWxkPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYiBteF9Wb2ljZVVzZXJTZXR0aW5nc1RhYlwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9oZWFkaW5nXCI+e190KFwiVm9pY2UgJiBWaWRlb1wiKX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtyZXF1ZXN0QnV0dG9ufVxyXG4gICAgICAgICAgICAgICAgICAgIHtzcGVha2VyRHJvcGRvd259XHJcbiAgICAgICAgICAgICAgICAgICAge21pY3JvcGhvbmVEcm9wZG93bn1cclxuICAgICAgICAgICAgICAgICAgICB7d2ViY2FtRHJvcGRvd259XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZyBuYW1lPSdWaWRlb1ZpZXcuZmxpcFZpZGVvSG9yaXpvbnRhbGx5JyBsZXZlbD17U2V0dGluZ0xldmVsLkFDQ09VTlR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPSd3ZWJSdGNBbGxvd1BlZXJUb1BlZXInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsPXtTZXR0aW5nTGV2ZWwuREVWSUNFfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fY2hhbmdlV2ViUnRjTWV0aG9kfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPSdmYWxsYmFja0lDRVNlcnZlckFsbG93ZWQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsPXtTZXR0aW5nTGV2ZWwuREVWSUNFfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fY2hhbmdlRmFsbGJhY2tJQ0VTZXJ2ZXJBbGxvd2VkfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=