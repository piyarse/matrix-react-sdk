"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var languageHandler = _interopRequireWildcard(require("../../../../../languageHandler"));

var _ProfileSettings = _interopRequireDefault(require("../../ProfileSettings"));

var _Field = _interopRequireDefault(require("../../../elements/Field"));

var _SettingsStore = _interopRequireWildcard(require("../../../../../settings/SettingsStore"));

var _LanguageDropdown = _interopRequireDefault(require("../../../elements/LanguageDropdown"));

var _AccessibleButton = _interopRequireDefault(require("../../../elements/AccessibleButton"));

var _DeactivateAccountDialog = _interopRequireDefault(require("../../../dialogs/DeactivateAccountDialog"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _theme = require("../../../../../theme");

var _PlatformPeg = _interopRequireDefault(require("../../../../../PlatformPeg"));

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../.."));

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

var _dispatcher = _interopRequireDefault(require("../../../../../dispatcher"));

var _Terms = require("../../../../../Terms");

var _matrixJsSdk = require("matrix-js-sdk");

var _IdentityAuthClient = _interopRequireDefault(require("../../../../../IdentityAuthClient"));

var _UrlUtils = require("../../../../../utils/UrlUtils");

var _boundThreepids = require("../../../../../boundThreepids");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

class GeneralUserSettingsTab extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onAction", payload => {
      if (payload.action === 'id_server_changed') {
        this.setState({
          haveIdServer: Boolean(_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl())
        });

        this._getThreepidState();
      }
    });
    (0, _defineProperty2.default)(this, "_onEmailsChange", emails => {
      this.setState({
        emails
      });
    });
    (0, _defineProperty2.default)(this, "_onMsisdnsChange", msisdns => {
      this.setState({
        msisdns
      });
    });
    (0, _defineProperty2.default)(this, "_onLanguageChange", newLanguage => {
      if (this.state.language === newLanguage) return;

      _SettingsStore.default.setValue("language", null, _SettingsStore.SettingLevel.DEVICE, newLanguage);

      this.setState({
        language: newLanguage
      });

      _PlatformPeg.default.get().reload();
    });
    (0, _defineProperty2.default)(this, "_onThemeChange", e => {
      const newTheme = e.target.value;
      if (this.state.theme === newTheme) return; // doing getValue in the .catch will still return the value we failed to set,
      // so remember what the value was before we tried to set it so we can revert

      const oldTheme = _SettingsStore.default.getValue('theme');

      _SettingsStore.default.setValue("theme", null, _SettingsStore.SettingLevel.ACCOUNT, newTheme).catch(() => {
        _dispatcher.default.dispatch({
          action: 'recheck_theme'
        });

        this.setState({
          theme: oldTheme
        });
      });

      this.setState({
        theme: newTheme
      }); // The settings watcher doesn't fire until the echo comes back from the
      // server, so to make the theme change immediately we need to manually
      // do the dispatch now
      // XXX: The local echoed value appears to be unreliable, in particular
      // when settings custom themes(!) so adding forceTheme to override
      // the value from settings.

      _dispatcher.default.dispatch({
        action: 'recheck_theme',
        forceTheme: newTheme
      });
    });
    (0, _defineProperty2.default)(this, "_onUseSystemThemeChanged", checked => {
      this.setState({
        useSystemTheme: checked
      });

      _SettingsStore.default.setValue("use_system_theme", null, _SettingsStore.SettingLevel.DEVICE, checked);

      _dispatcher.default.dispatch({
        action: 'recheck_theme'
      });
    });
    (0, _defineProperty2.default)(this, "_onPasswordChangeError", err => {
      // TODO: Figure out a design that doesn't involve replacing the current dialog
      let errMsg = err.error || "";

      if (err.httpStatus === 403) {
        errMsg = (0, languageHandler._t)("Failed to change password. Is your password correct?");
      } else if (err.httpStatus) {
        errMsg += " (HTTP status ".concat(err.httpStatus, ")");
      }

      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Failed to change password: " + errMsg);

      _Modal.default.createTrackedDialog('Failed to change password', '', ErrorDialog, {
        title: (0, languageHandler._t)("Error"),
        description: errMsg
      });
    });
    (0, _defineProperty2.default)(this, "_onPasswordChanged", () => {
      // TODO: Figure out a design that doesn't involve replacing the current dialog
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Password changed', '', ErrorDialog, {
        title: (0, languageHandler._t)("Success"),
        description: (0, languageHandler._t)("Your password was successfully changed. You will not receive " + "push notifications on other sessions until you log back in to them") + "."
      });
    });
    (0, _defineProperty2.default)(this, "_onDeactivateClicked", () => {
      _Modal.default.createTrackedDialog('Deactivate Account', '', _DeactivateAccountDialog.default, {
        onFinished: success => {
          if (success) this.props.closeSettingsFn();
        }
      });
    });
    (0, _defineProperty2.default)(this, "_onAddCustomTheme", async () => {
      let currentThemes = _SettingsStore.default.getValue("custom_themes");

      if (!currentThemes) currentThemes = [];
      currentThemes = currentThemes.map(c => c); // cheap clone

      if (this._themeTimer) {
        clearTimeout(this._themeTimer);
      }

      try {
        const r = await fetch(this.state.customThemeUrl);
        const themeInfo = await r.json();

        if (!themeInfo || typeof themeInfo['name'] !== 'string' || typeof themeInfo['colors'] !== 'object') {
          this.setState({
            customThemeMessage: {
              text: (0, languageHandler._t)("Invalid theme schema."),
              isError: true
            }
          });
          return;
        }

        currentThemes.push(themeInfo);
      } catch (e) {
        console.error(e);
        this.setState({
          customThemeMessage: {
            text: (0, languageHandler._t)("Error downloading theme information."),
            isError: true
          }
        });
        return; // Don't continue on error
      }

      await _SettingsStore.default.setValue("custom_themes", null, _SettingsStore.SettingLevel.ACCOUNT, currentThemes);
      this.setState({
        customThemeUrl: "",
        customThemeMessage: {
          text: (0, languageHandler._t)("Theme added!"),
          isError: false
        }
      });
      this._themeTimer = setTimeout(() => {
        this.setState({
          customThemeMessage: {
            text: "",
            isError: false
          }
        });
      }, 3000);
    });
    (0, _defineProperty2.default)(this, "_onCustomThemeChange", e => {
      this.setState({
        customThemeUrl: e.target.value
      });
    });
    this.state = _objectSpread({
      language: languageHandler.getCurrentLanguage(),
      haveIdServer: Boolean(_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()),
      serverSupportsSeparateAddAndBind: null,
      idServerHasUnsignedTerms: false,
      requiredPolicyInfo: {
        // This object is passed along to a component for handling
        hasTerms: false // policiesAndServices, // From the startTermsFlow callback
        // agreedUrls,          // From the startTermsFlow callback
        // resolve,             // Promise resolve function for startTermsFlow callback

      },
      emails: [],
      msisdns: []
    }, this._calculateThemeState(), {
      customThemeUrl: "",
      customThemeMessage: {
        isError: false,
        text: ""
      }
    });
    this.dispatcherRef = _dispatcher.default.register(this._onAction);
  } // TODO: [REACT-WARNING] Move this to constructor


  async UNSAFE_componentWillMount() {
    // eslint-disable-line camelcase
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const serverSupportsSeparateAddAndBind = await cli.doesServerSupportSeparateAddAndBind();
    const capabilities = await cli.getCapabilities(); // this is cached

    const changePasswordCap = capabilities['m.change_password']; // You can change your password so long as the capability isn't explicitly disabled. The implicit
    // behaviour is you can change your password when the capability is missing or has not-false as
    // the enabled flag value.

    const canChangePassword = !changePasswordCap || changePasswordCap['enabled'] !== false;
    this.setState({
      serverSupportsSeparateAddAndBind,
      canChangePassword
    });

    this._getThreepidState();
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this.dispatcherRef);
  }

  _calculateThemeState() {
    // We have to mirror the logic from ThemeWatcher.getEffectiveTheme so we
    // show the right values for things.
    const themeChoice = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.ACCOUNT, "theme");

    const systemThemeExplicit = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, "use_system_theme", null, false, true);

    const themeExplicit = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, "theme", null, false, true); // If the user has enabled system theme matching, use that.


    if (systemThemeExplicit) {
      return {
        theme: themeChoice,
        useSystemTheme: true
      };
    } // If the user has set a theme explicitly, use that (no system theme matching)


    if (themeExplicit) {
      return {
        theme: themeChoice,
        useSystemTheme: false
      };
    } // Otherwise assume the defaults for the settings


    return {
      theme: themeChoice,
      useSystemTheme: _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, "use_system_theme")
    };
  }

  async _getThreepidState() {
    const cli = _MatrixClientPeg.MatrixClientPeg.get(); // Check to see if terms need accepting


    this._checkTerms(); // Need to get 3PIDs generally for Account section and possibly also for
    // Discovery (assuming we have an IS and terms are agreed).


    let threepids = [];

    try {
      threepids = await (0, _boundThreepids.getThreepidsWithBindStatus)(cli);
    } catch (e) {
      const idServerUrl = _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl();

      console.warn("Unable to reach identity server at ".concat(idServerUrl, " to check ") + "for 3PIDs bindings in Settings");
      console.warn(e);
    }

    this.setState({
      emails: threepids.filter(a => a.medium === 'email')
    });
    this.setState({
      msisdns: threepids.filter(a => a.medium === 'msisdn')
    });
  }

  async _checkTerms() {
    if (!this.state.haveIdServer) {
      this.setState({
        idServerHasUnsignedTerms: false
      });
      return;
    } // By starting the terms flow we get the logic for checking which terms the user has signed
    // for free. So we might as well use that for our own purposes.


    const idServerUrl = _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl();

    const authClient = new _IdentityAuthClient.default();

    try {
      const idAccessToken = await authClient.getAccessToken({
        check: false
      });
      await (0, _Terms.startTermsFlow)([new _Terms.Service(_matrixJsSdk.SERVICE_TYPES.IS, idServerUrl, idAccessToken)], (policiesAndServices, agreedUrls, extraClassNames) => {
        return new Promise((resolve, reject) => {
          this.setState({
            idServerName: (0, _UrlUtils.abbreviateUrl)(idServerUrl),
            requiredPolicyInfo: {
              hasTerms: true,
              policiesAndServices,
              agreedUrls,
              resolve
            }
          });
        });
      }); // User accepted all terms

      this.setState({
        requiredPolicyInfo: {
          hasTerms: false
        }
      });
    } catch (e) {
      console.warn("Unable to reach identity server at ".concat(idServerUrl, " to check ") + "for terms in Settings");
      console.warn(e);
    }
  }

  _renderProfileSection() {
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Profile")), _react.default.createElement(_ProfileSettings.default, null));
  }

  _renderAccountSection() {
    const ChangePassword = sdk.getComponent("views.settings.ChangePassword");
    const EmailAddresses = sdk.getComponent("views.settings.account.EmailAddresses");
    const PhoneNumbers = sdk.getComponent("views.settings.account.PhoneNumbers");
    const Spinner = sdk.getComponent("views.elements.Spinner");

    let passwordChangeForm = _react.default.createElement(ChangePassword, {
      className: "mx_GeneralUserSettingsTab_changePassword",
      rowClassName: "",
      buttonKind: "primary",
      onError: this._onPasswordChangeError,
      onFinished: this._onPasswordChanged
    });

    let threepidSection = null; // For older homeservers without separate 3PID add and bind methods (MSC2290),
    // we use a combo add with bind option API which requires an identity server to
    // validate 3PID ownership even if we're just adding to the homeserver only.
    // For newer homeservers with separate 3PID add and bind methods (MSC2290),
    // there is no such concern, so we can always show the HS account 3PIDs.

    if (this.state.haveIdServer || this.state.serverSupportsSeparateAddAndBind === true) {
      threepidSection = _react.default.createElement("div", null, _react.default.createElement("span", {
        className: "mx_SettingsTab_subheading"
      }, (0, languageHandler._t)("Email addresses")), _react.default.createElement(EmailAddresses, {
        emails: this.state.emails,
        onEmailsChange: this._onEmailsChange
      }), _react.default.createElement("span", {
        className: "mx_SettingsTab_subheading"
      }, (0, languageHandler._t)("Phone numbers")), _react.default.createElement(PhoneNumbers, {
        msisdns: this.state.msisdns,
        onMsisdnsChange: this._onMsisdnsChange
      }));
    } else if (this.state.serverSupportsSeparateAddAndBind === null) {
      threepidSection = _react.default.createElement(Spinner, null);
    }

    let passwordChangeText = (0, languageHandler._t)("Set a new account password...");

    if (!this.state.canChangePassword) {
      // Just don't show anything if you can't do anything.
      passwordChangeText = null;
      passwordChangeForm = null;
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_GeneralUserSettingsTab_accountSection"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Account")), _react.default.createElement("p", {
      className: "mx_SettingsTab_subsectionText"
    }, passwordChangeText), passwordChangeForm, threepidSection);
  }

  _renderLanguageSection() {
    // TODO: Convert to new-styled Field
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Language and region")), _react.default.createElement(_LanguageDropdown.default, {
      className: "mx_GeneralUserSettingsTab_languageInput",
      onOptionChange: this._onLanguageChange,
      value: this.state.language
    }));
  }

  _renderThemeSection() {
    const SettingsFlag = sdk.getComponent("views.elements.SettingsFlag");
    const LabelledToggleSwitch = sdk.getComponent("views.elements.LabelledToggleSwitch");
    const themeWatcher = new _theme.ThemeWatcher();
    let systemThemeSection;

    if (themeWatcher.isSystemThemeSupported()) {
      systemThemeSection = _react.default.createElement("div", null, _react.default.createElement(LabelledToggleSwitch, {
        value: this.state.useSystemTheme,
        label: _SettingsStore.default.getDisplayName("use_system_theme"),
        onChange: this._onUseSystemThemeChanged
      }));
    }

    let customThemeForm;

    if (_SettingsStore.default.isFeatureEnabled("feature_custom_themes")) {
      let messageElement = null;

      if (this.state.customThemeMessage.text) {
        if (this.state.customThemeMessage.isError) {
          messageElement = _react.default.createElement("div", {
            className: "text-error"
          }, this.state.customThemeMessage.text);
        } else {
          messageElement = _react.default.createElement("div", {
            className: "text-success"
          }, this.state.customThemeMessage.text);
        }
      }

      customThemeForm = _react.default.createElement("div", {
        className: "mx_SettingsTab_section"
      }, _react.default.createElement("form", {
        onSubmit: this._onAddCustomTheme
      }, _react.default.createElement(_Field.default, {
        label: (0, languageHandler._t)("Custom theme URL"),
        type: "text",
        autoComplete: "off",
        onChange: this._onCustomThemeChange,
        value: this.state.customThemeUrl
      }), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onAddCustomTheme,
        type: "submit",
        kind: "primary_sm",
        disabled: !this.state.customThemeUrl.trim()
      }, (0, languageHandler._t)("Add theme")), messageElement));
    }

    const themes = Object.entries((0, _theme.enumerateThemes)()).map(p => ({
      id: p[0],
      name: p[1]
    })); // convert pairs to objects for code readability

    const builtInThemes = themes.filter(p => !p.id.startsWith("custom-"));
    const customThemes = themes.filter(p => !builtInThemes.includes(p)).sort((a, b) => a.name.localeCompare(b.name));
    const orderedThemes = [...builtInThemes, ...customThemes];
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_GeneralUserSettingsTab_themeSection"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Theme")), systemThemeSection, _react.default.createElement(_Field.default, {
      label: (0, languageHandler._t)("Theme"),
      element: "select",
      value: this.state.theme,
      onChange: this._onThemeChange,
      disabled: this.state.useSystemTheme
    }, orderedThemes.map(theme => {
      return _react.default.createElement("option", {
        key: theme.id,
        value: theme.id
      }, theme.name);
    })), customThemeForm, _react.default.createElement(SettingsFlag, {
      name: "useCompactLayout",
      level: _SettingsStore.SettingLevel.ACCOUNT
    }));
  }

  _renderDiscoverySection() {
    const SetIdServer = sdk.getComponent("views.settings.SetIdServer");

    if (this.state.requiredPolicyInfo.hasTerms) {
      const InlineTermsAgreement = sdk.getComponent("views.terms.InlineTermsAgreement");

      const intro = _react.default.createElement("span", {
        className: "mx_SettingsTab_subsectionText"
      }, (0, languageHandler._t)("Agree to the identity server (%(serverName)s) Terms of Service to " + "allow yourself to be discoverable by email address or phone number.", {
        serverName: this.state.idServerName
      }));

      return _react.default.createElement("div", null, _react.default.createElement(InlineTermsAgreement, {
        policiesAndServicePairs: this.state.requiredPolicyInfo.policiesAndServices,
        agreedUrls: this.state.requiredPolicyInfo.agreedUrls,
        onFinished: this.state.requiredPolicyInfo.resolve,
        introElement: intro
      }), _react.default.createElement(SetIdServer, {
        missingTerms: true
      }));
    }

    const EmailAddresses = sdk.getComponent("views.settings.discovery.EmailAddresses");
    const PhoneNumbers = sdk.getComponent("views.settings.discovery.PhoneNumbers");
    const threepidSection = this.state.haveIdServer ? _react.default.createElement("div", {
      className: "mx_GeneralUserSettingsTab_discovery"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Email addresses")), _react.default.createElement(EmailAddresses, {
      emails: this.state.emails
    }), _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Phone numbers")), _react.default.createElement(PhoneNumbers, {
      msisdns: this.state.msisdns
    })) : null;
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, threepidSection, _react.default.createElement(SetIdServer, null));
  }

  _renderManagementSection() {
    // TODO: Improve warning text for account deactivation
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, languageHandler._t)("Account management")), _react.default.createElement("span", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, languageHandler._t)("Deactivating your account is a permanent action - be careful!")), _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onDeactivateClicked,
      kind: "danger"
    }, (0, languageHandler._t)("Deactivate Account")));
  }

  _renderIntegrationManagerSection() {
    const SetIntegrationManager = sdk.getComponent("views.settings.SetIntegrationManager");
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement(SetIntegrationManager, null));
  }

  render() {
    const discoWarning = this.state.requiredPolicyInfo.hasTerms ? _react.default.createElement("img", {
      className: "mx_GeneralUserSettingsTab_warningIcon",
      src: require("../../../../../../res/img/feather-customised/warning-triangle.svg"),
      width: "18",
      height: "18",
      alt: (0, languageHandler._t)("Warning")
    }) : null;
    return _react.default.createElement("div", {
      className: "mx_SettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, languageHandler._t)("General")), this._renderProfileSection(), this._renderAccountSection(), this._renderLanguageSection(), this._renderThemeSection(), _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, discoWarning, " ", (0, languageHandler._t)("Discovery")), this._renderDiscoverySection(), this._renderIntegrationManagerSection()
    /* Has its own title */
    , _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, languageHandler._t)("Deactivate account")), this._renderManagementSection());
  }

}

exports.default = GeneralUserSettingsTab;
(0, _defineProperty2.default)(GeneralUserSettingsTab, "propTypes", {
  closeSettingsFn: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9HZW5lcmFsVXNlclNldHRpbmdzVGFiLmpzIl0sIm5hbWVzIjpbIkdlbmVyYWxVc2VyU2V0dGluZ3NUYWIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicGF5bG9hZCIsImFjdGlvbiIsInNldFN0YXRlIiwiaGF2ZUlkU2VydmVyIiwiQm9vbGVhbiIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldElkZW50aXR5U2VydmVyVXJsIiwiX2dldFRocmVlcGlkU3RhdGUiLCJlbWFpbHMiLCJtc2lzZG5zIiwibmV3TGFuZ3VhZ2UiLCJzdGF0ZSIsImxhbmd1YWdlIiwiU2V0dGluZ3NTdG9yZSIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIiwiUGxhdGZvcm1QZWciLCJyZWxvYWQiLCJlIiwibmV3VGhlbWUiLCJ0YXJnZXQiLCJ2YWx1ZSIsInRoZW1lIiwib2xkVGhlbWUiLCJnZXRWYWx1ZSIsIkFDQ09VTlQiLCJjYXRjaCIsImRpcyIsImRpc3BhdGNoIiwiZm9yY2VUaGVtZSIsImNoZWNrZWQiLCJ1c2VTeXN0ZW1UaGVtZSIsImVyciIsImVyck1zZyIsImVycm9yIiwiaHR0cFN0YXR1cyIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY29uc29sZSIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJEZWFjdGl2YXRlQWNjb3VudERpYWxvZyIsIm9uRmluaXNoZWQiLCJzdWNjZXNzIiwicHJvcHMiLCJjbG9zZVNldHRpbmdzRm4iLCJjdXJyZW50VGhlbWVzIiwibWFwIiwiYyIsIl90aGVtZVRpbWVyIiwiY2xlYXJUaW1lb3V0IiwiciIsImZldGNoIiwiY3VzdG9tVGhlbWVVcmwiLCJ0aGVtZUluZm8iLCJqc29uIiwiY3VzdG9tVGhlbWVNZXNzYWdlIiwidGV4dCIsImlzRXJyb3IiLCJwdXNoIiwic2V0VGltZW91dCIsImxhbmd1YWdlSGFuZGxlciIsImdldEN1cnJlbnRMYW5ndWFnZSIsInNlcnZlclN1cHBvcnRzU2VwYXJhdGVBZGRBbmRCaW5kIiwiaWRTZXJ2ZXJIYXNVbnNpZ25lZFRlcm1zIiwicmVxdWlyZWRQb2xpY3lJbmZvIiwiaGFzVGVybXMiLCJfY2FsY3VsYXRlVGhlbWVTdGF0ZSIsImRpc3BhdGNoZXJSZWYiLCJyZWdpc3RlciIsIl9vbkFjdGlvbiIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJjbGkiLCJkb2VzU2VydmVyU3VwcG9ydFNlcGFyYXRlQWRkQW5kQmluZCIsImNhcGFiaWxpdGllcyIsImdldENhcGFiaWxpdGllcyIsImNoYW5nZVBhc3N3b3JkQ2FwIiwiY2FuQ2hhbmdlUGFzc3dvcmQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJ0aGVtZUNob2ljZSIsImdldFZhbHVlQXQiLCJzeXN0ZW1UaGVtZUV4cGxpY2l0IiwidGhlbWVFeHBsaWNpdCIsIl9jaGVja1Rlcm1zIiwidGhyZWVwaWRzIiwiaWRTZXJ2ZXJVcmwiLCJ3YXJuIiwiZmlsdGVyIiwiYSIsIm1lZGl1bSIsImF1dGhDbGllbnQiLCJJZGVudGl0eUF1dGhDbGllbnQiLCJpZEFjY2Vzc1Rva2VuIiwiZ2V0QWNjZXNzVG9rZW4iLCJjaGVjayIsIlNlcnZpY2UiLCJTRVJWSUNFX1RZUEVTIiwiSVMiLCJwb2xpY2llc0FuZFNlcnZpY2VzIiwiYWdyZWVkVXJscyIsImV4dHJhQ2xhc3NOYW1lcyIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiaWRTZXJ2ZXJOYW1lIiwiX3JlbmRlclByb2ZpbGVTZWN0aW9uIiwiX3JlbmRlckFjY291bnRTZWN0aW9uIiwiQ2hhbmdlUGFzc3dvcmQiLCJFbWFpbEFkZHJlc3NlcyIsIlBob25lTnVtYmVycyIsIlNwaW5uZXIiLCJwYXNzd29yZENoYW5nZUZvcm0iLCJfb25QYXNzd29yZENoYW5nZUVycm9yIiwiX29uUGFzc3dvcmRDaGFuZ2VkIiwidGhyZWVwaWRTZWN0aW9uIiwiX29uRW1haWxzQ2hhbmdlIiwiX29uTXNpc2Ruc0NoYW5nZSIsInBhc3N3b3JkQ2hhbmdlVGV4dCIsIl9yZW5kZXJMYW5ndWFnZVNlY3Rpb24iLCJfb25MYW5ndWFnZUNoYW5nZSIsIl9yZW5kZXJUaGVtZVNlY3Rpb24iLCJTZXR0aW5nc0ZsYWciLCJMYWJlbGxlZFRvZ2dsZVN3aXRjaCIsInRoZW1lV2F0Y2hlciIsIlRoZW1lV2F0Y2hlciIsInN5c3RlbVRoZW1lU2VjdGlvbiIsImlzU3lzdGVtVGhlbWVTdXBwb3J0ZWQiLCJnZXREaXNwbGF5TmFtZSIsIl9vblVzZVN5c3RlbVRoZW1lQ2hhbmdlZCIsImN1c3RvbVRoZW1lRm9ybSIsImlzRmVhdHVyZUVuYWJsZWQiLCJtZXNzYWdlRWxlbWVudCIsIl9vbkFkZEN1c3RvbVRoZW1lIiwiX29uQ3VzdG9tVGhlbWVDaGFuZ2UiLCJ0cmltIiwidGhlbWVzIiwiT2JqZWN0IiwiZW50cmllcyIsInAiLCJpZCIsIm5hbWUiLCJidWlsdEluVGhlbWVzIiwic3RhcnRzV2l0aCIsImN1c3RvbVRoZW1lcyIsImluY2x1ZGVzIiwic29ydCIsImIiLCJsb2NhbGVDb21wYXJlIiwib3JkZXJlZFRoZW1lcyIsIl9vblRoZW1lQ2hhbmdlIiwiX3JlbmRlckRpc2NvdmVyeVNlY3Rpb24iLCJTZXRJZFNlcnZlciIsIklubGluZVRlcm1zQWdyZWVtZW50IiwiaW50cm8iLCJzZXJ2ZXJOYW1lIiwiX3JlbmRlck1hbmFnZW1lbnRTZWN0aW9uIiwiX29uRGVhY3RpdmF0ZUNsaWNrZWQiLCJfcmVuZGVySW50ZWdyYXRpb25NYW5hZ2VyU2VjdGlvbiIsIlNldEludGVncmF0aW9uTWFuYWdlciIsInJlbmRlciIsImRpc2NvV2FybmluZyIsInJlcXVpcmUiLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7O0FBRWUsTUFBTUEsc0JBQU4sU0FBcUNDLGVBQU1DLFNBQTNDLENBQXFEO0FBS2hFQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLHFEQWdGREMsT0FBRCxJQUFhO0FBQ3JCLFVBQUlBLE9BQU8sQ0FBQ0MsTUFBUixLQUFtQixtQkFBdkIsRUFBNEM7QUFDeEMsYUFBS0MsUUFBTCxDQUFjO0FBQUNDLFVBQUFBLFlBQVksRUFBRUMsT0FBTyxDQUFDQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxvQkFBdEIsRUFBRDtBQUF0QixTQUFkOztBQUNBLGFBQUtDLGlCQUFMO0FBQ0g7QUFDSixLQXJGYTtBQUFBLDJEQXVGS0MsTUFBRCxJQUFZO0FBQzFCLFdBQUtQLFFBQUwsQ0FBYztBQUFFTyxRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQXpGYTtBQUFBLDREQTJGTUMsT0FBRCxJQUFhO0FBQzVCLFdBQUtSLFFBQUwsQ0FBYztBQUFFUSxRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQTdGYTtBQUFBLDZEQWtLT0MsV0FBRCxJQUFpQjtBQUNqQyxVQUFJLEtBQUtDLEtBQUwsQ0FBV0MsUUFBWCxLQUF3QkYsV0FBNUIsRUFBeUM7O0FBRXpDRyw2QkFBY0MsUUFBZCxDQUF1QixVQUF2QixFQUFtQyxJQUFuQyxFQUF5Q0MsNEJBQWFDLE1BQXRELEVBQThETixXQUE5RDs7QUFDQSxXQUFLVCxRQUFMLENBQWM7QUFBQ1csUUFBQUEsUUFBUSxFQUFFRjtBQUFYLE9BQWQ7O0FBQ0FPLDJCQUFZWixHQUFaLEdBQWtCYSxNQUFsQjtBQUNILEtBeEthO0FBQUEsMERBMEtJQyxDQUFELElBQU87QUFDcEIsWUFBTUMsUUFBUSxHQUFHRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0MsS0FBMUI7QUFDQSxVQUFJLEtBQUtYLEtBQUwsQ0FBV1ksS0FBWCxLQUFxQkgsUUFBekIsRUFBbUMsT0FGZixDQUlwQjtBQUNBOztBQUNBLFlBQU1JLFFBQVEsR0FBR1gsdUJBQWNZLFFBQWQsQ0FBdUIsT0FBdkIsQ0FBakI7O0FBQ0FaLDZCQUFjQyxRQUFkLENBQXVCLE9BQXZCLEVBQWdDLElBQWhDLEVBQXNDQyw0QkFBYVcsT0FBbkQsRUFBNEROLFFBQTVELEVBQXNFTyxLQUF0RSxDQUE0RSxNQUFNO0FBQzlFQyw0QkFBSUMsUUFBSixDQUFhO0FBQUM3QixVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiOztBQUNBLGFBQUtDLFFBQUwsQ0FBYztBQUFDc0IsVUFBQUEsS0FBSyxFQUFFQztBQUFSLFNBQWQ7QUFDSCxPQUhEOztBQUlBLFdBQUt2QixRQUFMLENBQWM7QUFBQ3NCLFFBQUFBLEtBQUssRUFBRUg7QUFBUixPQUFkLEVBWG9CLENBWXBCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQVEsMEJBQUlDLFFBQUosQ0FBYTtBQUFDN0IsUUFBQUEsTUFBTSxFQUFFLGVBQVQ7QUFBMEI4QixRQUFBQSxVQUFVLEVBQUVWO0FBQXRDLE9BQWI7QUFDSCxLQTdMYTtBQUFBLG9FQStMY1csT0FBRCxJQUFhO0FBQ3BDLFdBQUs5QixRQUFMLENBQWM7QUFBQytCLFFBQUFBLGNBQWMsRUFBRUQ7QUFBakIsT0FBZDs7QUFDQWxCLDZCQUFjQyxRQUFkLENBQXVCLGtCQUF2QixFQUEyQyxJQUEzQyxFQUFpREMsNEJBQWFDLE1BQTlELEVBQXNFZSxPQUF0RTs7QUFDQUgsMEJBQUlDLFFBQUosQ0FBYTtBQUFDN0IsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjtBQUNILEtBbk1hO0FBQUEsa0VBcU1ZaUMsR0FBRCxJQUFTO0FBQzlCO0FBQ0EsVUFBSUMsTUFBTSxHQUFHRCxHQUFHLENBQUNFLEtBQUosSUFBYSxFQUExQjs7QUFDQSxVQUFJRixHQUFHLENBQUNHLFVBQUosS0FBbUIsR0FBdkIsRUFBNEI7QUFDeEJGLFFBQUFBLE1BQU0sR0FBRyx3QkFBRyxzREFBSCxDQUFUO0FBQ0gsT0FGRCxNQUVPLElBQUlELEdBQUcsQ0FBQ0csVUFBUixFQUFvQjtBQUN2QkYsUUFBQUEsTUFBTSw0QkFBcUJELEdBQUcsQ0FBQ0csVUFBekIsTUFBTjtBQUNIOztBQUNELFlBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBQyxNQUFBQSxPQUFPLENBQUNMLEtBQVIsQ0FBYyxnQ0FBZ0NELE1BQTlDOztBQUNBTyxxQkFBTUMsbUJBQU4sQ0FBMEIsMkJBQTFCLEVBQXVELEVBQXZELEVBQTJETCxXQUEzRCxFQUF3RTtBQUNwRU0sUUFBQUEsS0FBSyxFQUFFLHdCQUFHLE9BQUgsQ0FENkQ7QUFFcEVDLFFBQUFBLFdBQVcsRUFBRVY7QUFGdUQsT0FBeEU7QUFJSCxLQW5OYTtBQUFBLDhEQXFOTyxNQUFNO0FBQ3ZCO0FBQ0EsWUFBTUcsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBRSxxQkFBTUMsbUJBQU4sQ0FBMEIsa0JBQTFCLEVBQThDLEVBQTlDLEVBQWtETCxXQUFsRCxFQUErRDtBQUMzRE0sUUFBQUEsS0FBSyxFQUFFLHdCQUFHLFNBQUgsQ0FEb0Q7QUFFM0RDLFFBQUFBLFdBQVcsRUFBRSx3QkFDVCxrRUFDQSxvRUFGUyxJQUdUO0FBTHVELE9BQS9EO0FBT0gsS0EvTmE7QUFBQSxnRUFpT1MsTUFBTTtBQUN6QkgscUJBQU1DLG1CQUFOLENBQTBCLG9CQUExQixFQUFnRCxFQUFoRCxFQUFvREcsZ0NBQXBELEVBQTZFO0FBQ3pFQyxRQUFBQSxVQUFVLEVBQUdDLE9BQUQsSUFBYTtBQUNyQixjQUFJQSxPQUFKLEVBQWEsS0FBS0MsS0FBTCxDQUFXQyxlQUFYO0FBQ2hCO0FBSHdFLE9BQTdFO0FBS0gsS0F2T2E7QUFBQSw2REF5T00sWUFBWTtBQUM1QixVQUFJQyxhQUFhLEdBQUdyQyx1QkFBY1ksUUFBZCxDQUF1QixlQUF2QixDQUFwQjs7QUFDQSxVQUFJLENBQUN5QixhQUFMLEVBQW9CQSxhQUFhLEdBQUcsRUFBaEI7QUFDcEJBLE1BQUFBLGFBQWEsR0FBR0EsYUFBYSxDQUFDQyxHQUFkLENBQWtCQyxDQUFDLElBQUlBLENBQXZCLENBQWhCLENBSDRCLENBR2U7O0FBRTNDLFVBQUksS0FBS0MsV0FBVCxFQUFzQjtBQUNsQkMsUUFBQUEsWUFBWSxDQUFDLEtBQUtELFdBQU4sQ0FBWjtBQUNIOztBQUVELFVBQUk7QUFDQSxjQUFNRSxDQUFDLEdBQUcsTUFBTUMsS0FBSyxDQUFDLEtBQUs3QyxLQUFMLENBQVc4QyxjQUFaLENBQXJCO0FBQ0EsY0FBTUMsU0FBUyxHQUFHLE1BQU1ILENBQUMsQ0FBQ0ksSUFBRixFQUF4Qjs7QUFDQSxZQUFJLENBQUNELFNBQUQsSUFBYyxPQUFPQSxTQUFTLENBQUMsTUFBRCxDQUFoQixLQUE4QixRQUE1QyxJQUF3RCxPQUFPQSxTQUFTLENBQUMsUUFBRCxDQUFoQixLQUFnQyxRQUE1RixFQUFzRztBQUNsRyxlQUFLekQsUUFBTCxDQUFjO0FBQUMyRCxZQUFBQSxrQkFBa0IsRUFBRTtBQUFDQyxjQUFBQSxJQUFJLEVBQUUsd0JBQUcsdUJBQUgsQ0FBUDtBQUFvQ0MsY0FBQUEsT0FBTyxFQUFFO0FBQTdDO0FBQXJCLFdBQWQ7QUFDQTtBQUNIOztBQUNEWixRQUFBQSxhQUFhLENBQUNhLElBQWQsQ0FBbUJMLFNBQW5CO0FBQ0gsT0FSRCxDQVFFLE9BQU92QyxDQUFQLEVBQVU7QUFDUnFCLFFBQUFBLE9BQU8sQ0FBQ0wsS0FBUixDQUFjaEIsQ0FBZDtBQUNBLGFBQUtsQixRQUFMLENBQWM7QUFBQzJELFVBQUFBLGtCQUFrQixFQUFFO0FBQUNDLFlBQUFBLElBQUksRUFBRSx3QkFBRyxzQ0FBSCxDQUFQO0FBQW1EQyxZQUFBQSxPQUFPLEVBQUU7QUFBNUQ7QUFBckIsU0FBZDtBQUNBLGVBSFEsQ0FHQTtBQUNYOztBQUVELFlBQU1qRCx1QkFBY0MsUUFBZCxDQUF1QixlQUF2QixFQUF3QyxJQUF4QyxFQUE4Q0MsNEJBQWFXLE9BQTNELEVBQW9Fd0IsYUFBcEUsQ0FBTjtBQUNBLFdBQUtqRCxRQUFMLENBQWM7QUFBQ3dELFFBQUFBLGNBQWMsRUFBRSxFQUFqQjtBQUFxQkcsUUFBQUEsa0JBQWtCLEVBQUU7QUFBQ0MsVUFBQUEsSUFBSSxFQUFFLHdCQUFHLGNBQUgsQ0FBUDtBQUEyQkMsVUFBQUEsT0FBTyxFQUFFO0FBQXBDO0FBQXpDLE9BQWQ7QUFFQSxXQUFLVCxXQUFMLEdBQW1CVyxVQUFVLENBQUMsTUFBTTtBQUNoQyxhQUFLL0QsUUFBTCxDQUFjO0FBQUMyRCxVQUFBQSxrQkFBa0IsRUFBRTtBQUFDQyxZQUFBQSxJQUFJLEVBQUUsRUFBUDtBQUFXQyxZQUFBQSxPQUFPLEVBQUU7QUFBcEI7QUFBckIsU0FBZDtBQUNILE9BRjRCLEVBRTFCLElBRjBCLENBQTdCO0FBR0gsS0F0UWE7QUFBQSxnRUF3UVUzQyxDQUFELElBQU87QUFDMUIsV0FBS2xCLFFBQUwsQ0FBYztBQUFDd0QsUUFBQUEsY0FBYyxFQUFFdEMsQ0FBQyxDQUFDRSxNQUFGLENBQVNDO0FBQTFCLE9BQWQ7QUFDSCxLQTFRYTtBQUdWLFNBQUtYLEtBQUw7QUFDSUMsTUFBQUEsUUFBUSxFQUFFcUQsZUFBZSxDQUFDQyxrQkFBaEIsRUFEZDtBQUVJaEUsTUFBQUEsWUFBWSxFQUFFQyxPQUFPLENBQUNDLGlDQUFnQkMsR0FBaEIsR0FBc0JDLG9CQUF0QixFQUFELENBRnpCO0FBR0k2RCxNQUFBQSxnQ0FBZ0MsRUFBRSxJQUh0QztBQUlJQyxNQUFBQSx3QkFBd0IsRUFBRSxLQUo5QjtBQUtJQyxNQUFBQSxrQkFBa0IsRUFBRTtBQUFRO0FBQ3hCQyxRQUFBQSxRQUFRLEVBQUUsS0FETSxDQUVoQjtBQUNBO0FBQ0E7O0FBSmdCLE9BTHhCO0FBV0k5RCxNQUFBQSxNQUFNLEVBQUUsRUFYWjtBQVlJQyxNQUFBQSxPQUFPLEVBQUU7QUFaYixPQWFPLEtBQUs4RCxvQkFBTCxFQWJQO0FBY0lkLE1BQUFBLGNBQWMsRUFBRSxFQWRwQjtBQWVJRyxNQUFBQSxrQkFBa0IsRUFBRTtBQUFDRSxRQUFBQSxPQUFPLEVBQUUsS0FBVjtBQUFpQkQsUUFBQUEsSUFBSSxFQUFFO0FBQXZCO0FBZnhCO0FBa0JBLFNBQUtXLGFBQUwsR0FBcUI1QyxvQkFBSTZDLFFBQUosQ0FBYSxLQUFLQyxTQUFsQixDQUFyQjtBQUNILEdBM0IrRCxDQTZCaEU7OztBQUNBLFFBQU1DLHlCQUFOLEdBQWtDO0FBQUU7QUFDaEMsVUFBTUMsR0FBRyxHQUFHeEUsaUNBQWdCQyxHQUFoQixFQUFaOztBQUVBLFVBQU04RCxnQ0FBZ0MsR0FBRyxNQUFNUyxHQUFHLENBQUNDLG1DQUFKLEVBQS9DO0FBRUEsVUFBTUMsWUFBWSxHQUFHLE1BQU1GLEdBQUcsQ0FBQ0csZUFBSixFQUEzQixDQUw4QixDQUtvQjs7QUFDbEQsVUFBTUMsaUJBQWlCLEdBQUdGLFlBQVksQ0FBQyxtQkFBRCxDQUF0QyxDQU44QixDQVE5QjtBQUNBO0FBQ0E7O0FBQ0EsVUFBTUcsaUJBQWlCLEdBQUcsQ0FBQ0QsaUJBQUQsSUFBc0JBLGlCQUFpQixDQUFDLFNBQUQsQ0FBakIsS0FBaUMsS0FBakY7QUFFQSxTQUFLL0UsUUFBTCxDQUFjO0FBQUNrRSxNQUFBQSxnQ0FBRDtBQUFtQ2MsTUFBQUE7QUFBbkMsS0FBZDs7QUFFQSxTQUFLMUUsaUJBQUw7QUFDSDs7QUFFRDJFLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CdEQsd0JBQUl1RCxVQUFKLENBQWUsS0FBS1gsYUFBcEI7QUFDSDs7QUFFREQsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkI7QUFDQTtBQUVBLFVBQU1hLFdBQVcsR0FBR3ZFLHVCQUFjd0UsVUFBZCxDQUF5QnRFLDRCQUFhVyxPQUF0QyxFQUErQyxPQUEvQyxDQUFwQjs7QUFDQSxVQUFNNEQsbUJBQW1CLEdBQUd6RSx1QkFBY3dFLFVBQWQsQ0FDeEJ0RSw0QkFBYUMsTUFEVyxFQUNILGtCQURHLEVBQ2lCLElBRGpCLEVBQ3VCLEtBRHZCLEVBQzhCLElBRDlCLENBQTVCOztBQUVBLFVBQU11RSxhQUFhLEdBQUcxRSx1QkFBY3dFLFVBQWQsQ0FDbEJ0RSw0QkFBYUMsTUFESyxFQUNHLE9BREgsRUFDWSxJQURaLEVBQ2tCLEtBRGxCLEVBQ3lCLElBRHpCLENBQXRCLENBUG1CLENBVW5COzs7QUFDQSxRQUFJc0UsbUJBQUosRUFBeUI7QUFDckIsYUFBTztBQUNIL0QsUUFBQUEsS0FBSyxFQUFFNkQsV0FESjtBQUVIcEQsUUFBQUEsY0FBYyxFQUFFO0FBRmIsT0FBUDtBQUlILEtBaEJrQixDQWtCbkI7OztBQUNBLFFBQUl1RCxhQUFKLEVBQW1CO0FBQ2YsYUFBTztBQUNIaEUsUUFBQUEsS0FBSyxFQUFFNkQsV0FESjtBQUVIcEQsUUFBQUEsY0FBYyxFQUFFO0FBRmIsT0FBUDtBQUlILEtBeEJrQixDQTBCbkI7OztBQUNBLFdBQU87QUFDSFQsTUFBQUEsS0FBSyxFQUFFNkQsV0FESjtBQUVIcEQsTUFBQUEsY0FBYyxFQUFFbkIsdUJBQWN3RSxVQUFkLENBQXlCdEUsNEJBQWFDLE1BQXRDLEVBQThDLGtCQUE5QztBQUZiLEtBQVA7QUFJSDs7QUFpQkQsUUFBTVQsaUJBQU4sR0FBMEI7QUFDdEIsVUFBTXFFLEdBQUcsR0FBR3hFLGlDQUFnQkMsR0FBaEIsRUFBWixDQURzQixDQUd0Qjs7O0FBQ0EsU0FBS21GLFdBQUwsR0FKc0IsQ0FNdEI7QUFDQTs7O0FBQ0EsUUFBSUMsU0FBUyxHQUFHLEVBQWhCOztBQUNBLFFBQUk7QUFDQUEsTUFBQUEsU0FBUyxHQUFHLE1BQU0sZ0RBQTJCYixHQUEzQixDQUFsQjtBQUNILEtBRkQsQ0FFRSxPQUFPekQsQ0FBUCxFQUFVO0FBQ1IsWUFBTXVFLFdBQVcsR0FBR3RGLGlDQUFnQkMsR0FBaEIsR0FBc0JDLG9CQUF0QixFQUFwQjs7QUFDQWtDLE1BQUFBLE9BQU8sQ0FBQ21ELElBQVIsQ0FDSSw2Q0FBc0NELFdBQXRDLGtEQURKO0FBSUFsRCxNQUFBQSxPQUFPLENBQUNtRCxJQUFSLENBQWF4RSxDQUFiO0FBQ0g7O0FBQ0QsU0FBS2xCLFFBQUwsQ0FBYztBQUFFTyxNQUFBQSxNQUFNLEVBQUVpRixTQUFTLENBQUNHLE1BQVYsQ0FBa0JDLENBQUQsSUFBT0EsQ0FBQyxDQUFDQyxNQUFGLEtBQWEsT0FBckM7QUFBVixLQUFkO0FBQ0EsU0FBSzdGLFFBQUwsQ0FBYztBQUFFUSxNQUFBQSxPQUFPLEVBQUVnRixTQUFTLENBQUNHLE1BQVYsQ0FBa0JDLENBQUQsSUFBT0EsQ0FBQyxDQUFDQyxNQUFGLEtBQWEsUUFBckM7QUFBWCxLQUFkO0FBQ0g7O0FBRUQsUUFBTU4sV0FBTixHQUFvQjtBQUNoQixRQUFJLENBQUMsS0FBSzdFLEtBQUwsQ0FBV1QsWUFBaEIsRUFBOEI7QUFDMUIsV0FBS0QsUUFBTCxDQUFjO0FBQUNtRSxRQUFBQSx3QkFBd0IsRUFBRTtBQUEzQixPQUFkO0FBQ0E7QUFDSCxLQUplLENBTWhCO0FBQ0E7OztBQUNBLFVBQU1zQixXQUFXLEdBQUd0RixpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxvQkFBdEIsRUFBcEI7O0FBQ0EsVUFBTXlGLFVBQVUsR0FBRyxJQUFJQywyQkFBSixFQUFuQjs7QUFDQSxRQUFJO0FBQ0EsWUFBTUMsYUFBYSxHQUFHLE1BQU1GLFVBQVUsQ0FBQ0csY0FBWCxDQUEwQjtBQUFFQyxRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQUExQixDQUE1QjtBQUNBLFlBQU0sMkJBQWUsQ0FBQyxJQUFJQyxjQUFKLENBQ2xCQywyQkFBY0MsRUFESSxFQUVsQlosV0FGa0IsRUFHbEJPLGFBSGtCLENBQUQsQ0FBZixFQUlGLENBQUNNLG1CQUFELEVBQXNCQyxVQUF0QixFQUFrQ0MsZUFBbEMsS0FBc0Q7QUFDdEQsZUFBTyxJQUFJQyxPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQ3BDLGVBQUszRyxRQUFMLENBQWM7QUFDVjRHLFlBQUFBLFlBQVksRUFBRSw2QkFBY25CLFdBQWQsQ0FESjtBQUVWckIsWUFBQUEsa0JBQWtCLEVBQUU7QUFDaEJDLGNBQUFBLFFBQVEsRUFBRSxJQURNO0FBRWhCaUMsY0FBQUEsbUJBRmdCO0FBR2hCQyxjQUFBQSxVQUhnQjtBQUloQkcsY0FBQUE7QUFKZ0I7QUFGVixXQUFkO0FBU0gsU0FWTSxDQUFQO0FBV0gsT0FoQkssQ0FBTixDQUZBLENBbUJBOztBQUNBLFdBQUsxRyxRQUFMLENBQWM7QUFDVm9FLFFBQUFBLGtCQUFrQixFQUFFO0FBQ2hCQyxVQUFBQSxRQUFRLEVBQUU7QUFETTtBQURWLE9BQWQ7QUFLSCxLQXpCRCxDQXlCRSxPQUFPbkQsQ0FBUCxFQUFVO0FBQ1JxQixNQUFBQSxPQUFPLENBQUNtRCxJQUFSLENBQ0ksNkNBQXNDRCxXQUF0Qyx5Q0FESjtBQUlBbEQsTUFBQUEsT0FBTyxDQUFDbUQsSUFBUixDQUFheEUsQ0FBYjtBQUNIO0FBQ0o7O0FBNEdEMkYsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHdCQUFHLFNBQUgsQ0FBN0MsQ0FESixFQUVJLDZCQUFDLHdCQUFELE9BRkosQ0FESjtBQU1IOztBQUVEQyxFQUFBQSxxQkFBcUIsR0FBRztBQUNwQixVQUFNQyxjQUFjLEdBQUcxRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsK0JBQWpCLENBQXZCO0FBQ0EsVUFBTTBFLGNBQWMsR0FBRzNFLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix1Q0FBakIsQ0FBdkI7QUFDQSxVQUFNMkUsWUFBWSxHQUFHNUUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFDQUFqQixDQUFyQjtBQUNBLFVBQU00RSxPQUFPLEdBQUc3RSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQWhCOztBQUVBLFFBQUk2RSxrQkFBa0IsR0FDbEIsNkJBQUMsY0FBRDtBQUNJLE1BQUEsU0FBUyxFQUFDLDBDQURkO0FBRUksTUFBQSxZQUFZLEVBQUMsRUFGakI7QUFHSSxNQUFBLFVBQVUsRUFBQyxTQUhmO0FBSUksTUFBQSxPQUFPLEVBQUUsS0FBS0Msc0JBSmxCO0FBS0ksTUFBQSxVQUFVLEVBQUUsS0FBS0M7QUFMckIsTUFESjs7QUFTQSxRQUFJQyxlQUFlLEdBQUcsSUFBdEIsQ0Fmb0IsQ0FpQnBCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLNUcsS0FBTCxDQUFXVCxZQUFYLElBQTJCLEtBQUtTLEtBQUwsQ0FBV3dELGdDQUFYLEtBQWdELElBQS9FLEVBQXFGO0FBQ2pGb0QsTUFBQUEsZUFBZSxHQUFHLDBDQUNkO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBNkMsd0JBQUcsaUJBQUgsQ0FBN0MsQ0FEYyxFQUVkLDZCQUFDLGNBQUQ7QUFDSSxRQUFBLE1BQU0sRUFBRSxLQUFLNUcsS0FBTCxDQUFXSCxNQUR2QjtBQUVJLFFBQUEsY0FBYyxFQUFFLEtBQUtnSDtBQUZ6QixRQUZjLEVBT2Q7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUE2Qyx3QkFBRyxlQUFILENBQTdDLENBUGMsRUFRZCw2QkFBQyxZQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUUsS0FBSzdHLEtBQUwsQ0FBV0YsT0FEeEI7QUFFSSxRQUFBLGVBQWUsRUFBRSxLQUFLZ0g7QUFGMUIsUUFSYyxDQUFsQjtBQWFILEtBZEQsTUFjTyxJQUFJLEtBQUs5RyxLQUFMLENBQVd3RCxnQ0FBWCxLQUFnRCxJQUFwRCxFQUEwRDtBQUM3RG9ELE1BQUFBLGVBQWUsR0FBRyw2QkFBQyxPQUFELE9BQWxCO0FBQ0g7O0FBRUQsUUFBSUcsa0JBQWtCLEdBQUcsd0JBQUcsK0JBQUgsQ0FBekI7O0FBQ0EsUUFBSSxDQUFDLEtBQUsvRyxLQUFMLENBQVdzRSxpQkFBaEIsRUFBbUM7QUFDL0I7QUFDQXlDLE1BQUFBLGtCQUFrQixHQUFHLElBQXJCO0FBQ0FOLE1BQUFBLGtCQUFrQixHQUFHLElBQXJCO0FBQ0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHdCQUFHLFNBQUgsQ0FBN0MsQ0FESixFQUVJO0FBQUcsTUFBQSxTQUFTLEVBQUM7QUFBYixPQUNLTSxrQkFETCxDQUZKLEVBS0tOLGtCQUxMLEVBTUtHLGVBTkwsQ0FESjtBQVVIOztBQUVESSxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQjtBQUNBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx3QkFBRyxxQkFBSCxDQUE3QyxDQURKLEVBRUksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMseUNBQTVCO0FBQ2tCLE1BQUEsY0FBYyxFQUFFLEtBQUtDLGlCQUR2QztBQUMwRCxNQUFBLEtBQUssRUFBRSxLQUFLakgsS0FBTCxDQUFXQztBQUQ1RSxNQUZKLENBREo7QUFPSDs7QUFFRGlILEVBQUFBLG1CQUFtQixHQUFHO0FBQ2xCLFVBQU1DLFlBQVksR0FBR3hGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBckI7QUFDQSxVQUFNd0Ysb0JBQW9CLEdBQUd6RixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUNBQWpCLENBQTdCO0FBRUEsVUFBTXlGLFlBQVksR0FBRyxJQUFJQyxtQkFBSixFQUFyQjtBQUNBLFFBQUlDLGtCQUFKOztBQUNBLFFBQUlGLFlBQVksQ0FBQ0csc0JBQWIsRUFBSixFQUEyQztBQUN2Q0QsTUFBQUEsa0JBQWtCLEdBQUcsMENBQ2pCLDZCQUFDLG9CQUFEO0FBQ0ksUUFBQSxLQUFLLEVBQUUsS0FBS3ZILEtBQUwsQ0FBV3FCLGNBRHRCO0FBRUksUUFBQSxLQUFLLEVBQUVuQix1QkFBY3VILGNBQWQsQ0FBNkIsa0JBQTdCLENBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxLQUFLQztBQUhuQixRQURpQixDQUFyQjtBQU9IOztBQUVELFFBQUlDLGVBQUo7O0FBQ0EsUUFBSXpILHVCQUFjMEgsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQUosRUFBNkQ7QUFDekQsVUFBSUMsY0FBYyxHQUFHLElBQXJCOztBQUNBLFVBQUksS0FBSzdILEtBQUwsQ0FBV2lELGtCQUFYLENBQThCQyxJQUFsQyxFQUF3QztBQUNwQyxZQUFJLEtBQUtsRCxLQUFMLENBQVdpRCxrQkFBWCxDQUE4QkUsT0FBbEMsRUFBMkM7QUFDdkMwRSxVQUFBQSxjQUFjLEdBQUc7QUFBSyxZQUFBLFNBQVMsRUFBQztBQUFmLGFBQTZCLEtBQUs3SCxLQUFMLENBQVdpRCxrQkFBWCxDQUE4QkMsSUFBM0QsQ0FBakI7QUFDSCxTQUZELE1BRU87QUFDSDJFLFVBQUFBLGNBQWMsR0FBRztBQUFLLFlBQUEsU0FBUyxFQUFDO0FBQWYsYUFBK0IsS0FBSzdILEtBQUwsQ0FBV2lELGtCQUFYLENBQThCQyxJQUE3RCxDQUFqQjtBQUNIO0FBQ0o7O0FBQ0R5RSxNQUFBQSxlQUFlLEdBQ1g7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBTSxRQUFBLFFBQVEsRUFBRSxLQUFLRztBQUFyQixTQUNJLDZCQUFDLGNBQUQ7QUFDSSxRQUFBLEtBQUssRUFBRSx3QkFBRyxrQkFBSCxDQURYO0FBRUksUUFBQSxJQUFJLEVBQUMsTUFGVDtBQUdJLFFBQUEsWUFBWSxFQUFDLEtBSGpCO0FBSUksUUFBQSxRQUFRLEVBQUUsS0FBS0Msb0JBSm5CO0FBS0ksUUFBQSxLQUFLLEVBQUUsS0FBSy9ILEtBQUwsQ0FBVzhDO0FBTHRCLFFBREosRUFRSSw2QkFBQyx5QkFBRDtBQUNJLFFBQUEsT0FBTyxFQUFFLEtBQUtnRixpQkFEbEI7QUFFSSxRQUFBLElBQUksRUFBQyxRQUZUO0FBRWtCLFFBQUEsSUFBSSxFQUFDLFlBRnZCO0FBR0ksUUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLOUgsS0FBTCxDQUFXOEMsY0FBWCxDQUEwQmtGLElBQTFCO0FBSGYsU0FJRSx3QkFBRyxXQUFILENBSkYsQ0FSSixFQWFLSCxjQWJMLENBREosQ0FESjtBQW1CSDs7QUFFRCxVQUFNSSxNQUFNLEdBQUdDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlLDZCQUFmLEVBQ1YzRixHQURVLENBQ040RixDQUFDLEtBQUs7QUFBQ0MsTUFBQUEsRUFBRSxFQUFFRCxDQUFDLENBQUMsQ0FBRCxDQUFOO0FBQVdFLE1BQUFBLElBQUksRUFBRUYsQ0FBQyxDQUFDLENBQUQ7QUFBbEIsS0FBTCxDQURLLENBQWYsQ0EvQ2tCLENBZ0R1Qjs7QUFDekMsVUFBTUcsYUFBYSxHQUFHTixNQUFNLENBQUNoRCxNQUFQLENBQWNtRCxDQUFDLElBQUksQ0FBQ0EsQ0FBQyxDQUFDQyxFQUFGLENBQUtHLFVBQUwsQ0FBZ0IsU0FBaEIsQ0FBcEIsQ0FBdEI7QUFDQSxVQUFNQyxZQUFZLEdBQUdSLE1BQU0sQ0FBQ2hELE1BQVAsQ0FBY21ELENBQUMsSUFBSSxDQUFDRyxhQUFhLENBQUNHLFFBQWQsQ0FBdUJOLENBQXZCLENBQXBCLEVBQ2hCTyxJQURnQixDQUNYLENBQUN6RCxDQUFELEVBQUkwRCxDQUFKLEtBQVUxRCxDQUFDLENBQUNvRCxJQUFGLENBQU9PLGFBQVAsQ0FBcUJELENBQUMsQ0FBQ04sSUFBdkIsQ0FEQyxDQUFyQjtBQUVBLFVBQU1RLGFBQWEsR0FBRyxDQUFDLEdBQUdQLGFBQUosRUFBbUIsR0FBR0UsWUFBdEIsQ0FBdEI7QUFDQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMsd0JBQUcsT0FBSCxDQUE3QyxDQURKLEVBRUtsQixrQkFGTCxFQUdJLDZCQUFDLGNBQUQ7QUFBTyxNQUFBLEtBQUssRUFBRSx3QkFBRyxPQUFILENBQWQ7QUFBMkIsTUFBQSxPQUFPLEVBQUMsUUFBbkM7QUFDTyxNQUFBLEtBQUssRUFBRSxLQUFLdkgsS0FBTCxDQUFXWSxLQUR6QjtBQUNnQyxNQUFBLFFBQVEsRUFBRSxLQUFLbUksY0FEL0M7QUFFTyxNQUFBLFFBQVEsRUFBRSxLQUFLL0ksS0FBTCxDQUFXcUI7QUFGNUIsT0FJS3lILGFBQWEsQ0FBQ3RHLEdBQWQsQ0FBa0I1QixLQUFLLElBQUk7QUFDeEIsYUFBTztBQUFRLFFBQUEsR0FBRyxFQUFFQSxLQUFLLENBQUN5SCxFQUFuQjtBQUF1QixRQUFBLEtBQUssRUFBRXpILEtBQUssQ0FBQ3lIO0FBQXBDLFNBQXlDekgsS0FBSyxDQUFDMEgsSUFBL0MsQ0FBUDtBQUNILEtBRkEsQ0FKTCxDQUhKLEVBV0tYLGVBWEwsRUFZSSw2QkFBQyxZQUFEO0FBQWMsTUFBQSxJQUFJLEVBQUMsa0JBQW5CO0FBQXNDLE1BQUEsS0FBSyxFQUFFdkgsNEJBQWFXO0FBQTFELE1BWkosQ0FESjtBQWdCSDs7QUFFRGlJLEVBQUFBLHVCQUF1QixHQUFHO0FBQ3RCLFVBQU1DLFdBQVcsR0FBR3RILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw0QkFBakIsQ0FBcEI7O0FBRUEsUUFBSSxLQUFLNUIsS0FBTCxDQUFXMEQsa0JBQVgsQ0FBOEJDLFFBQWxDLEVBQTRDO0FBQ3hDLFlBQU11RixvQkFBb0IsR0FBR3ZILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQ0FBakIsQ0FBN0I7O0FBQ0EsWUFBTXVILEtBQUssR0FBRztBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ1Qsd0JBQ0csdUVBQ0EscUVBRkgsRUFHRztBQUFDQyxRQUFBQSxVQUFVLEVBQUUsS0FBS3BKLEtBQUwsQ0FBV2tHO0FBQXhCLE9BSEgsQ0FEUyxDQUFkOztBQU9BLGFBQ0ksMENBQ0ksNkJBQUMsb0JBQUQ7QUFDSSxRQUFBLHVCQUF1QixFQUFFLEtBQUtsRyxLQUFMLENBQVcwRCxrQkFBWCxDQUE4QmtDLG1CQUQzRDtBQUVJLFFBQUEsVUFBVSxFQUFFLEtBQUs1RixLQUFMLENBQVcwRCxrQkFBWCxDQUE4Qm1DLFVBRjlDO0FBR0ksUUFBQSxVQUFVLEVBQUUsS0FBSzdGLEtBQUwsQ0FBVzBELGtCQUFYLENBQThCc0MsT0FIOUM7QUFJSSxRQUFBLFlBQVksRUFBRW1EO0FBSmxCLFFBREosRUFRSSw2QkFBQyxXQUFEO0FBQWEsUUFBQSxZQUFZLEVBQUU7QUFBM0IsUUFSSixDQURKO0FBWUg7O0FBRUQsVUFBTTdDLGNBQWMsR0FBRzNFLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5Q0FBakIsQ0FBdkI7QUFDQSxVQUFNMkUsWUFBWSxHQUFHNUUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVDQUFqQixDQUFyQjtBQUVBLFVBQU1nRixlQUFlLEdBQUcsS0FBSzVHLEtBQUwsQ0FBV1QsWUFBWCxHQUEwQjtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDOUM7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx3QkFBRyxpQkFBSCxDQUE3QyxDQUQ4QyxFQUU5Qyw2QkFBQyxjQUFEO0FBQWdCLE1BQUEsTUFBTSxFQUFFLEtBQUtTLEtBQUwsQ0FBV0g7QUFBbkMsTUFGOEMsRUFJOUM7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx3QkFBRyxlQUFILENBQTdDLENBSjhDLEVBSzlDLDZCQUFDLFlBQUQ7QUFBYyxNQUFBLE9BQU8sRUFBRSxLQUFLRyxLQUFMLENBQVdGO0FBQWxDLE1BTDhDLENBQTFCLEdBTWYsSUFOVDtBQVFBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0s4RyxlQURMLEVBR0ksNkJBQUMsV0FBRCxPQUhKLENBREo7QUFPSDs7QUFFRHlDLEVBQUFBLHdCQUF3QixHQUFHO0FBQ3ZCO0FBQ0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHdCQUFHLG9CQUFILENBQTdDLENBREosRUFFSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ0ssd0JBQUcsK0RBQUgsQ0FETCxDQUZKLEVBS0ksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxPQUFPLEVBQUUsS0FBS0Msb0JBQWhDO0FBQXNELE1BQUEsSUFBSSxFQUFDO0FBQTNELE9BQ0ssd0JBQUcsb0JBQUgsQ0FETCxDQUxKLENBREo7QUFXSDs7QUFFREMsRUFBQUEsZ0NBQWdDLEdBQUc7QUFDL0IsVUFBTUMscUJBQXFCLEdBQUc3SCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0NBQWpCLENBQTlCO0FBRUEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FFSSw2QkFBQyxxQkFBRCxPQUZKLENBREo7QUFNSDs7QUFFRDZILEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLFlBQVksR0FBRyxLQUFLMUosS0FBTCxDQUFXMEQsa0JBQVgsQ0FBOEJDLFFBQTlCLEdBQ2Y7QUFBSyxNQUFBLFNBQVMsRUFBQyx1Q0FBZjtBQUNFLE1BQUEsR0FBRyxFQUFFZ0csT0FBTyxDQUFDLG1FQUFELENBRGQ7QUFFRSxNQUFBLEtBQUssRUFBQyxJQUZSO0FBRWEsTUFBQSxNQUFNLEVBQUMsSUFGcEI7QUFFeUIsTUFBQSxHQUFHLEVBQUUsd0JBQUcsU0FBSDtBQUY5QixNQURlLEdBSWYsSUFKTjtBQU1BLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlDLHdCQUFHLFNBQUgsQ0FBekMsQ0FESixFQUVLLEtBQUt4RCxxQkFBTCxFQUZMLEVBR0ssS0FBS0MscUJBQUwsRUFITCxFQUlLLEtBQUtZLHNCQUFMLEVBSkwsRUFLSyxLQUFLRSxtQkFBTCxFQUxMLEVBTUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlDd0MsWUFBekMsT0FBd0Qsd0JBQUcsV0FBSCxDQUF4RCxDQU5KLEVBT0ssS0FBS1YsdUJBQUwsRUFQTCxFQVFLLEtBQUtPLGdDQUFMO0FBQXdDO0FBUjdDLE1BU0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlDLHdCQUFHLG9CQUFILENBQXpDLENBVEosRUFVSyxLQUFLRix3QkFBTCxFQVZMLENBREo7QUFjSDs7QUFwZ0IrRDs7OzhCQUEvQ3JLLHNCLGVBQ0U7QUFDZnNELEVBQUFBLGVBQWUsRUFBRXNILG1CQUFVQyxJQUFWLENBQWVDO0FBRGpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCBQcm9maWxlU2V0dGluZ3MgZnJvbSBcIi4uLy4uL1Byb2ZpbGVTZXR0aW5nc1wiO1xyXG5pbXBvcnQgRmllbGQgZnJvbSBcIi4uLy4uLy4uL2VsZW1lbnRzL0ZpZWxkXCI7XHJcbmltcG9ydCAqIGFzIGxhbmd1YWdlSGFuZGxlciBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCB7U2V0dGluZ0xldmVsfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgTGFuZ3VhZ2VEcm9wZG93biBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvTGFuZ3VhZ2VEcm9wZG93blwiO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvblwiO1xyXG5pbXBvcnQgRGVhY3RpdmF0ZUFjY291bnREaWFsb2cgZnJvbSBcIi4uLy4uLy4uL2RpYWxvZ3MvRGVhY3RpdmF0ZUFjY291bnREaWFsb2dcIjtcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xyXG5pbXBvcnQge2VudW1lcmF0ZVRoZW1lcywgVGhlbWVXYXRjaGVyfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vdGhlbWVcIjtcclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9QbGF0Zm9ybVBlZ1wiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uLy4uLy4uXCI7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTW9kYWxcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQge1NlcnZpY2UsIHN0YXJ0VGVybXNGbG93fSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vVGVybXNcIjtcclxuaW1wb3J0IHtTRVJWSUNFX1RZUEVTfSBmcm9tIFwibWF0cml4LWpzLXNka1wiO1xyXG5pbXBvcnQgSWRlbnRpdHlBdXRoQ2xpZW50IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9JZGVudGl0eUF1dGhDbGllbnRcIjtcclxuaW1wb3J0IHthYmJyZXZpYXRlVXJsfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vdXRpbHMvVXJsVXRpbHNcIjtcclxuaW1wb3J0IHsgZ2V0VGhyZWVwaWRzV2l0aEJpbmRTdGF0dXMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9ib3VuZFRocmVlcGlkcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHZW5lcmFsVXNlclNldHRpbmdzVGFiIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgY2xvc2VTZXR0aW5nc0ZuOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBsYW5ndWFnZTogbGFuZ3VhZ2VIYW5kbGVyLmdldEN1cnJlbnRMYW5ndWFnZSgpLFxyXG4gICAgICAgICAgICBoYXZlSWRTZXJ2ZXI6IEJvb2xlYW4oTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCkpLFxyXG4gICAgICAgICAgICBzZXJ2ZXJTdXBwb3J0c1NlcGFyYXRlQWRkQW5kQmluZDogbnVsbCxcclxuICAgICAgICAgICAgaWRTZXJ2ZXJIYXNVbnNpZ25lZFRlcm1zOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVxdWlyZWRQb2xpY3lJbmZvOiB7ICAgICAgIC8vIFRoaXMgb2JqZWN0IGlzIHBhc3NlZCBhbG9uZyB0byBhIGNvbXBvbmVudCBmb3IgaGFuZGxpbmdcclxuICAgICAgICAgICAgICAgIGhhc1Rlcm1zOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIC8vIHBvbGljaWVzQW5kU2VydmljZXMsIC8vIEZyb20gdGhlIHN0YXJ0VGVybXNGbG93IGNhbGxiYWNrXHJcbiAgICAgICAgICAgICAgICAvLyBhZ3JlZWRVcmxzLCAgICAgICAgICAvLyBGcm9tIHRoZSBzdGFydFRlcm1zRmxvdyBjYWxsYmFja1xyXG4gICAgICAgICAgICAgICAgLy8gcmVzb2x2ZSwgICAgICAgICAgICAgLy8gUHJvbWlzZSByZXNvbHZlIGZ1bmN0aW9uIGZvciBzdGFydFRlcm1zRmxvdyBjYWxsYmFja1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlbWFpbHM6IFtdLFxyXG4gICAgICAgICAgICBtc2lzZG5zOiBbXSxcclxuICAgICAgICAgICAgLi4udGhpcy5fY2FsY3VsYXRlVGhlbWVTdGF0ZSgpLFxyXG4gICAgICAgICAgICBjdXN0b21UaGVtZVVybDogXCJcIixcclxuICAgICAgICAgICAgY3VzdG9tVGhlbWVNZXNzYWdlOiB7aXNFcnJvcjogZmFsc2UsIHRleHQ6IFwiXCJ9LFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLl9vbkFjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIE1vdmUgdGhpcyB0byBjb25zdHJ1Y3RvclxyXG4gICAgYXN5bmMgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCgpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjYW1lbGNhc2VcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHNlcnZlclN1cHBvcnRzU2VwYXJhdGVBZGRBbmRCaW5kID0gYXdhaXQgY2xpLmRvZXNTZXJ2ZXJTdXBwb3J0U2VwYXJhdGVBZGRBbmRCaW5kKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGNhcGFiaWxpdGllcyA9IGF3YWl0IGNsaS5nZXRDYXBhYmlsaXRpZXMoKTsgLy8gdGhpcyBpcyBjYWNoZWRcclxuICAgICAgICBjb25zdCBjaGFuZ2VQYXNzd29yZENhcCA9IGNhcGFiaWxpdGllc1snbS5jaGFuZ2VfcGFzc3dvcmQnXTtcclxuXHJcbiAgICAgICAgLy8gWW91IGNhbiBjaGFuZ2UgeW91ciBwYXNzd29yZCBzbyBsb25nIGFzIHRoZSBjYXBhYmlsaXR5IGlzbid0IGV4cGxpY2l0bHkgZGlzYWJsZWQuIFRoZSBpbXBsaWNpdFxyXG4gICAgICAgIC8vIGJlaGF2aW91ciBpcyB5b3UgY2FuIGNoYW5nZSB5b3VyIHBhc3N3b3JkIHdoZW4gdGhlIGNhcGFiaWxpdHkgaXMgbWlzc2luZyBvciBoYXMgbm90LWZhbHNlIGFzXHJcbiAgICAgICAgLy8gdGhlIGVuYWJsZWQgZmxhZyB2YWx1ZS5cclxuICAgICAgICBjb25zdCBjYW5DaGFuZ2VQYXNzd29yZCA9ICFjaGFuZ2VQYXNzd29yZENhcCB8fCBjaGFuZ2VQYXNzd29yZENhcFsnZW5hYmxlZCddICE9PSBmYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2VydmVyU3VwcG9ydHNTZXBhcmF0ZUFkZEFuZEJpbmQsIGNhbkNoYW5nZVBhc3N3b3JkfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2dldFRocmVlcGlkU3RhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLmRpc3BhdGNoZXJSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIF9jYWxjdWxhdGVUaGVtZVN0YXRlKCkge1xyXG4gICAgICAgIC8vIFdlIGhhdmUgdG8gbWlycm9yIHRoZSBsb2dpYyBmcm9tIFRoZW1lV2F0Y2hlci5nZXRFZmZlY3RpdmVUaGVtZSBzbyB3ZVxyXG4gICAgICAgIC8vIHNob3cgdGhlIHJpZ2h0IHZhbHVlcyBmb3IgdGhpbmdzLlxyXG5cclxuICAgICAgICBjb25zdCB0aGVtZUNob2ljZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWVBdChTZXR0aW5nTGV2ZWwuQUNDT1VOVCwgXCJ0aGVtZVwiKTtcclxuICAgICAgICBjb25zdCBzeXN0ZW1UaGVtZUV4cGxpY2l0ID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZUF0KFxyXG4gICAgICAgICAgICBTZXR0aW5nTGV2ZWwuREVWSUNFLCBcInVzZV9zeXN0ZW1fdGhlbWVcIiwgbnVsbCwgZmFsc2UsIHRydWUpO1xyXG4gICAgICAgIGNvbnN0IHRoZW1lRXhwbGljaXQgPSBTZXR0aW5nc1N0b3JlLmdldFZhbHVlQXQoXHJcbiAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsIFwidGhlbWVcIiwgbnVsbCwgZmFsc2UsIHRydWUpO1xyXG5cclxuICAgICAgICAvLyBJZiB0aGUgdXNlciBoYXMgZW5hYmxlZCBzeXN0ZW0gdGhlbWUgbWF0Y2hpbmcsIHVzZSB0aGF0LlxyXG4gICAgICAgIGlmIChzeXN0ZW1UaGVtZUV4cGxpY2l0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICB0aGVtZTogdGhlbWVDaG9pY2UsXHJcbiAgICAgICAgICAgICAgICB1c2VTeXN0ZW1UaGVtZTogdHJ1ZSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIElmIHRoZSB1c2VyIGhhcyBzZXQgYSB0aGVtZSBleHBsaWNpdGx5LCB1c2UgdGhhdCAobm8gc3lzdGVtIHRoZW1lIG1hdGNoaW5nKVxyXG4gICAgICAgIGlmICh0aGVtZUV4cGxpY2l0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICB0aGVtZTogdGhlbWVDaG9pY2UsXHJcbiAgICAgICAgICAgICAgICB1c2VTeXN0ZW1UaGVtZTogZmFsc2UsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBPdGhlcndpc2UgYXNzdW1lIHRoZSBkZWZhdWx0cyBmb3IgdGhlIHNldHRpbmdzXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdGhlbWU6IHRoZW1lQ2hvaWNlLFxyXG4gICAgICAgICAgICB1c2VTeXN0ZW1UaGVtZTogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZUF0KFNldHRpbmdMZXZlbC5ERVZJQ0UsIFwidXNlX3N5c3RlbV90aGVtZVwiKSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkFjdGlvbiA9IChwYXlsb2FkKSA9PiB7XHJcbiAgICAgICAgaWYgKHBheWxvYWQuYWN0aW9uID09PSAnaWRfc2VydmVyX2NoYW5nZWQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2hhdmVJZFNlcnZlcjogQm9vbGVhbihNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwoKSl9KTtcclxuICAgICAgICAgICAgdGhpcy5fZ2V0VGhyZWVwaWRTdGF0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX29uRW1haWxzQ2hhbmdlID0gKGVtYWlscykgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBlbWFpbHMgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbk1zaXNkbnNDaGFuZ2UgPSAobXNpc2RucykgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtc2lzZG5zIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBhc3luYyBfZ2V0VGhyZWVwaWRTdGF0ZSgpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIC8vIENoZWNrIHRvIHNlZSBpZiB0ZXJtcyBuZWVkIGFjY2VwdGluZ1xyXG4gICAgICAgIHRoaXMuX2NoZWNrVGVybXMoKTtcclxuXHJcbiAgICAgICAgLy8gTmVlZCB0byBnZXQgM1BJRHMgZ2VuZXJhbGx5IGZvciBBY2NvdW50IHNlY3Rpb24gYW5kIHBvc3NpYmx5IGFsc28gZm9yXHJcbiAgICAgICAgLy8gRGlzY292ZXJ5IChhc3N1bWluZyB3ZSBoYXZlIGFuIElTIGFuZCB0ZXJtcyBhcmUgYWdyZWVkKS5cclxuICAgICAgICBsZXQgdGhyZWVwaWRzID0gW107XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgdGhyZWVwaWRzID0gYXdhaXQgZ2V0VGhyZWVwaWRzV2l0aEJpbmRTdGF0dXMoY2xpKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlkU2VydmVyVXJsID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcclxuICAgICAgICAgICAgICAgIGBVbmFibGUgdG8gcmVhY2ggaWRlbnRpdHkgc2VydmVyIGF0ICR7aWRTZXJ2ZXJVcmx9IHRvIGNoZWNrIGAgK1xyXG4gICAgICAgICAgICAgICAgYGZvciAzUElEcyBiaW5kaW5ncyBpbiBTZXR0aW5nc2AsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGVtYWlsczogdGhyZWVwaWRzLmZpbHRlcigoYSkgPT4gYS5tZWRpdW0gPT09ICdlbWFpbCcpIH0pO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtc2lzZG5zOiB0aHJlZXBpZHMuZmlsdGVyKChhKSA9PiBhLm1lZGl1bSA9PT0gJ21zaXNkbicpIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9jaGVja1Rlcm1zKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5oYXZlSWRTZXJ2ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aWRTZXJ2ZXJIYXNVbnNpZ25lZFRlcm1zOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBCeSBzdGFydGluZyB0aGUgdGVybXMgZmxvdyB3ZSBnZXQgdGhlIGxvZ2ljIGZvciBjaGVja2luZyB3aGljaCB0ZXJtcyB0aGUgdXNlciBoYXMgc2lnbmVkXHJcbiAgICAgICAgLy8gZm9yIGZyZWUuIFNvIHdlIG1pZ2h0IGFzIHdlbGwgdXNlIHRoYXQgZm9yIG91ciBvd24gcHVycG9zZXMuXHJcbiAgICAgICAgY29uc3QgaWRTZXJ2ZXJVcmwgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwoKTtcclxuICAgICAgICBjb25zdCBhdXRoQ2xpZW50ID0gbmV3IElkZW50aXR5QXV0aENsaWVudCgpO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlkQWNjZXNzVG9rZW4gPSBhd2FpdCBhdXRoQ2xpZW50LmdldEFjY2Vzc1Rva2VuKHsgY2hlY2s6IGZhbHNlIH0pO1xyXG4gICAgICAgICAgICBhd2FpdCBzdGFydFRlcm1zRmxvdyhbbmV3IFNlcnZpY2UoXHJcbiAgICAgICAgICAgICAgICBTRVJWSUNFX1RZUEVTLklTLFxyXG4gICAgICAgICAgICAgICAgaWRTZXJ2ZXJVcmwsXHJcbiAgICAgICAgICAgICAgICBpZEFjY2Vzc1Rva2VuLFxyXG4gICAgICAgICAgICApXSwgKHBvbGljaWVzQW5kU2VydmljZXMsIGFncmVlZFVybHMsIGV4dHJhQ2xhc3NOYW1lcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWRTZXJ2ZXJOYW1lOiBhYmJyZXZpYXRlVXJsKGlkU2VydmVyVXJsKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWRQb2xpY3lJbmZvOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXNUZXJtczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvbGljaWVzQW5kU2VydmljZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZ3JlZWRVcmxzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLy8gVXNlciBhY2NlcHRlZCBhbGwgdGVybXNcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZFBvbGljeUluZm86IHtcclxuICAgICAgICAgICAgICAgICAgICBoYXNUZXJtczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcclxuICAgICAgICAgICAgICAgIGBVbmFibGUgdG8gcmVhY2ggaWRlbnRpdHkgc2VydmVyIGF0ICR7aWRTZXJ2ZXJVcmx9IHRvIGNoZWNrIGAgK1xyXG4gICAgICAgICAgICAgICAgYGZvciB0ZXJtcyBpbiBTZXR0aW5nc2AsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uTGFuZ3VhZ2VDaGFuZ2UgPSAobmV3TGFuZ3VhZ2UpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5sYW5ndWFnZSA9PT0gbmV3TGFuZ3VhZ2UpIHJldHVybjtcclxuXHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS5zZXRWYWx1ZShcImxhbmd1YWdlXCIsIG51bGwsIFNldHRpbmdMZXZlbC5ERVZJQ0UsIG5ld0xhbmd1YWdlKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtsYW5ndWFnZTogbmV3TGFuZ3VhZ2V9KTtcclxuICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5yZWxvYWQoKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uVGhlbWVDaGFuZ2UgPSAoZSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IG5ld1RoZW1lID0gZS50YXJnZXQudmFsdWU7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudGhlbWUgPT09IG5ld1RoZW1lKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIGRvaW5nIGdldFZhbHVlIGluIHRoZSAuY2F0Y2ggd2lsbCBzdGlsbCByZXR1cm4gdGhlIHZhbHVlIHdlIGZhaWxlZCB0byBzZXQsXHJcbiAgICAgICAgLy8gc28gcmVtZW1iZXIgd2hhdCB0aGUgdmFsdWUgd2FzIGJlZm9yZSB3ZSB0cmllZCB0byBzZXQgaXQgc28gd2UgY2FuIHJldmVydFxyXG4gICAgICAgIGNvbnN0IG9sZFRoZW1lID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZSgndGhlbWUnKTtcclxuICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwidGhlbWVcIiwgbnVsbCwgU2V0dGluZ0xldmVsLkFDQ09VTlQsIG5ld1RoZW1lKS5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAncmVjaGVja190aGVtZSd9KTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dGhlbWU6IG9sZFRoZW1lfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dGhlbWU6IG5ld1RoZW1lfSk7XHJcbiAgICAgICAgLy8gVGhlIHNldHRpbmdzIHdhdGNoZXIgZG9lc24ndCBmaXJlIHVudGlsIHRoZSBlY2hvIGNvbWVzIGJhY2sgZnJvbSB0aGVcclxuICAgICAgICAvLyBzZXJ2ZXIsIHNvIHRvIG1ha2UgdGhlIHRoZW1lIGNoYW5nZSBpbW1lZGlhdGVseSB3ZSBuZWVkIHRvIG1hbnVhbGx5XHJcbiAgICAgICAgLy8gZG8gdGhlIGRpc3BhdGNoIG5vd1xyXG4gICAgICAgIC8vIFhYWDogVGhlIGxvY2FsIGVjaG9lZCB2YWx1ZSBhcHBlYXJzIHRvIGJlIHVucmVsaWFibGUsIGluIHBhcnRpY3VsYXJcclxuICAgICAgICAvLyB3aGVuIHNldHRpbmdzIGN1c3RvbSB0aGVtZXMoISkgc28gYWRkaW5nIGZvcmNlVGhlbWUgdG8gb3ZlcnJpZGVcclxuICAgICAgICAvLyB0aGUgdmFsdWUgZnJvbSBzZXR0aW5ncy5cclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlY2hlY2tfdGhlbWUnLCBmb3JjZVRoZW1lOiBuZXdUaGVtZX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25Vc2VTeXN0ZW1UaGVtZUNoYW5nZWQgPSAoY2hlY2tlZCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3VzZVN5c3RlbVRoZW1lOiBjaGVja2VkfSk7XHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS5zZXRWYWx1ZShcInVzZV9zeXN0ZW1fdGhlbWVcIiwgbnVsbCwgU2V0dGluZ0xldmVsLkRFVklDRSwgY2hlY2tlZCk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdyZWNoZWNrX3RoZW1lJ30pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25QYXNzd29yZENoYW5nZUVycm9yID0gKGVycikgPT4ge1xyXG4gICAgICAgIC8vIFRPRE86IEZpZ3VyZSBvdXQgYSBkZXNpZ24gdGhhdCBkb2Vzbid0IGludm9sdmUgcmVwbGFjaW5nIHRoZSBjdXJyZW50IGRpYWxvZ1xyXG4gICAgICAgIGxldCBlcnJNc2cgPSBlcnIuZXJyb3IgfHwgXCJcIjtcclxuICAgICAgICBpZiAoZXJyLmh0dHBTdGF0dXMgPT09IDQwMykge1xyXG4gICAgICAgICAgICBlcnJNc2cgPSBfdChcIkZhaWxlZCB0byBjaGFuZ2UgcGFzc3dvcmQuIElzIHlvdXIgcGFzc3dvcmQgY29ycmVjdD9cIik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChlcnIuaHR0cFN0YXR1cykge1xyXG4gICAgICAgICAgICBlcnJNc2cgKz0gYCAoSFRUUCBzdGF0dXMgJHtlcnIuaHR0cFN0YXR1c30pYDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIGNoYW5nZSBwYXNzd29yZDogXCIgKyBlcnJNc2cpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBjaGFuZ2UgcGFzc3dvcmQnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBlcnJNc2csXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblBhc3N3b3JkQ2hhbmdlZCA9ICgpID0+IHtcclxuICAgICAgICAvLyBUT0RPOiBGaWd1cmUgb3V0IGEgZGVzaWduIHRoYXQgZG9lc24ndCBpbnZvbHZlIHJlcGxhY2luZyB0aGUgY3VycmVudCBkaWFsb2dcclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1Bhc3N3b3JkIGNoYW5nZWQnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KFwiU3VjY2Vzc1wiKSxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgXCJZb3VyIHBhc3N3b3JkIHdhcyBzdWNjZXNzZnVsbHkgY2hhbmdlZC4gWW91IHdpbGwgbm90IHJlY2VpdmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJwdXNoIG5vdGlmaWNhdGlvbnMgb24gb3RoZXIgc2Vzc2lvbnMgdW50aWwgeW91IGxvZyBiYWNrIGluIHRvIHRoZW1cIixcclxuICAgICAgICAgICAgKSArIFwiLlwiLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25EZWFjdGl2YXRlQ2xpY2tlZCA9ICgpID0+IHtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdEZWFjdGl2YXRlIEFjY291bnQnLCAnJywgRGVhY3RpdmF0ZUFjY291bnREaWFsb2csIHtcclxuICAgICAgICAgICAgb25GaW5pc2hlZDogKHN1Y2Nlc3MpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChzdWNjZXNzKSB0aGlzLnByb3BzLmNsb3NlU2V0dGluZ3NGbigpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25BZGRDdXN0b21UaGVtZSA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICBsZXQgY3VycmVudFRoZW1lcyA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJjdXN0b21fdGhlbWVzXCIpO1xyXG4gICAgICAgIGlmICghY3VycmVudFRoZW1lcykgY3VycmVudFRoZW1lcyA9IFtdO1xyXG4gICAgICAgIGN1cnJlbnRUaGVtZXMgPSBjdXJyZW50VGhlbWVzLm1hcChjID0+IGMpOyAvLyBjaGVhcCBjbG9uZVxyXG5cclxuICAgICAgICBpZiAodGhpcy5fdGhlbWVUaW1lcikge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fdGhlbWVUaW1lcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCByID0gYXdhaXQgZmV0Y2godGhpcy5zdGF0ZS5jdXN0b21UaGVtZVVybCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRoZW1lSW5mbyA9IGF3YWl0IHIuanNvbigpO1xyXG4gICAgICAgICAgICBpZiAoIXRoZW1lSW5mbyB8fCB0eXBlb2YodGhlbWVJbmZvWyduYW1lJ10pICE9PSAnc3RyaW5nJyB8fCB0eXBlb2YodGhlbWVJbmZvWydjb2xvcnMnXSkgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXN0b21UaGVtZU1lc3NhZ2U6IHt0ZXh0OiBfdChcIkludmFsaWQgdGhlbWUgc2NoZW1hLlwiKSwgaXNFcnJvcjogdHJ1ZX19KTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjdXJyZW50VGhlbWVzLnB1c2godGhlbWVJbmZvKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1c3RvbVRoZW1lTWVzc2FnZToge3RleHQ6IF90KFwiRXJyb3IgZG93bmxvYWRpbmcgdGhlbWUgaW5mb3JtYXRpb24uXCIpLCBpc0Vycm9yOiB0cnVlfX0pO1xyXG4gICAgICAgICAgICByZXR1cm47IC8vIERvbid0IGNvbnRpbnVlIG9uIGVycm9yXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhd2FpdCBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiY3VzdG9tX3RoZW1lc1wiLCBudWxsLCBTZXR0aW5nTGV2ZWwuQUNDT1VOVCwgY3VycmVudFRoZW1lcyk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VzdG9tVGhlbWVVcmw6IFwiXCIsIGN1c3RvbVRoZW1lTWVzc2FnZToge3RleHQ6IF90KFwiVGhlbWUgYWRkZWQhXCIpLCBpc0Vycm9yOiBmYWxzZX19KTtcclxuXHJcbiAgICAgICAgdGhpcy5fdGhlbWVUaW1lciA9IHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXN0b21UaGVtZU1lc3NhZ2U6IHt0ZXh0OiBcIlwiLCBpc0Vycm9yOiBmYWxzZX19KTtcclxuICAgICAgICB9LCAzMDAwKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQ3VzdG9tVGhlbWVDaGFuZ2UgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1c3RvbVRoZW1lVXJsOiBlLnRhcmdldC52YWx1ZX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcmVuZGVyUHJvZmlsZVNlY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiUHJvZmlsZVwiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8UHJvZmlsZVNldHRpbmdzIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlckFjY291bnRTZWN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IENoYW5nZVBhc3N3b3JkID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLnNldHRpbmdzLkNoYW5nZVBhc3N3b3JkXCIpO1xyXG4gICAgICAgIGNvbnN0IEVtYWlsQWRkcmVzc2VzID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLnNldHRpbmdzLmFjY291bnQuRW1haWxBZGRyZXNzZXNcIik7XHJcbiAgICAgICAgY29uc3QgUGhvbmVOdW1iZXJzID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLnNldHRpbmdzLmFjY291bnQuUGhvbmVOdW1iZXJzXCIpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuXHJcbiAgICAgICAgbGV0IHBhc3N3b3JkQ2hhbmdlRm9ybSA9IChcclxuICAgICAgICAgICAgPENoYW5nZVBhc3N3b3JkXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9HZW5lcmFsVXNlclNldHRpbmdzVGFiX2NoYW5nZVBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgIHJvd0NsYXNzTmFtZT1cIlwiXHJcbiAgICAgICAgICAgICAgICBidXR0b25LaW5kPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICBvbkVycm9yPXt0aGlzLl9vblBhc3N3b3JkQ2hhbmdlRXJyb3J9XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLl9vblBhc3N3b3JkQ2hhbmdlZH0gLz5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBsZXQgdGhyZWVwaWRTZWN0aW9uID0gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gRm9yIG9sZGVyIGhvbWVzZXJ2ZXJzIHdpdGhvdXQgc2VwYXJhdGUgM1BJRCBhZGQgYW5kIGJpbmQgbWV0aG9kcyAoTVNDMjI5MCksXHJcbiAgICAgICAgLy8gd2UgdXNlIGEgY29tYm8gYWRkIHdpdGggYmluZCBvcHRpb24gQVBJIHdoaWNoIHJlcXVpcmVzIGFuIGlkZW50aXR5IHNlcnZlciB0b1xyXG4gICAgICAgIC8vIHZhbGlkYXRlIDNQSUQgb3duZXJzaGlwIGV2ZW4gaWYgd2UncmUganVzdCBhZGRpbmcgdG8gdGhlIGhvbWVzZXJ2ZXIgb25seS5cclxuICAgICAgICAvLyBGb3IgbmV3ZXIgaG9tZXNlcnZlcnMgd2l0aCBzZXBhcmF0ZSAzUElEIGFkZCBhbmQgYmluZCBtZXRob2RzIChNU0MyMjkwKSxcclxuICAgICAgICAvLyB0aGVyZSBpcyBubyBzdWNoIGNvbmNlcm4sIHNvIHdlIGNhbiBhbHdheXMgc2hvdyB0aGUgSFMgYWNjb3VudCAzUElEcy5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5oYXZlSWRTZXJ2ZXIgfHwgdGhpcy5zdGF0ZS5zZXJ2ZXJTdXBwb3J0c1NlcGFyYXRlQWRkQW5kQmluZCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICB0aHJlZXBpZFNlY3Rpb24gPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZ1wiPntfdChcIkVtYWlsIGFkZHJlc3Nlc1wiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8RW1haWxBZGRyZXNzZXNcclxuICAgICAgICAgICAgICAgICAgICBlbWFpbHM9e3RoaXMuc3RhdGUuZW1haWxzfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uRW1haWxzQ2hhbmdlPXt0aGlzLl9vbkVtYWlsc0NoYW5nZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZ1wiPntfdChcIlBob25lIG51bWJlcnNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPFBob25lTnVtYmVyc1xyXG4gICAgICAgICAgICAgICAgICAgIG1zaXNkbnM9e3RoaXMuc3RhdGUubXNpc2Ruc31cclxuICAgICAgICAgICAgICAgICAgICBvbk1zaXNkbnNDaGFuZ2U9e3RoaXMuX29uTXNpc2Ruc0NoYW5nZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuc2VydmVyU3VwcG9ydHNTZXBhcmF0ZUFkZEFuZEJpbmQgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhyZWVwaWRTZWN0aW9uID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcGFzc3dvcmRDaGFuZ2VUZXh0ID0gX3QoXCJTZXQgYSBuZXcgYWNjb3VudCBwYXNzd29yZC4uLlwiKTtcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY2FuQ2hhbmdlUGFzc3dvcmQpIHtcclxuICAgICAgICAgICAgLy8gSnVzdCBkb24ndCBzaG93IGFueXRoaW5nIGlmIHlvdSBjYW4ndCBkbyBhbnl0aGluZy5cclxuICAgICAgICAgICAgcGFzc3dvcmRDaGFuZ2VUZXh0ID0gbnVsbDtcclxuICAgICAgICAgICAgcGFzc3dvcmRDaGFuZ2VGb3JtID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvbiBteF9HZW5lcmFsVXNlclNldHRpbmdzVGFiX2FjY291bnRTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiQWNjb3VudFwiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtwYXNzd29yZENoYW5nZVRleHR9XHJcbiAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICB7cGFzc3dvcmRDaGFuZ2VGb3JtfVxyXG4gICAgICAgICAgICAgICAge3RocmVlcGlkU2VjdGlvbn1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyTGFuZ3VhZ2VTZWN0aW9uKCkge1xyXG4gICAgICAgIC8vIFRPRE86IENvbnZlcnQgdG8gbmV3LXN0eWxlZCBGaWVsZFxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZ1wiPntfdChcIkxhbmd1YWdlIGFuZCByZWdpb25cIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPExhbmd1YWdlRHJvcGRvd24gY2xhc3NOYW1lPVwibXhfR2VuZXJhbFVzZXJTZXR0aW5nc1RhYl9sYW5ndWFnZUlucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uT3B0aW9uQ2hhbmdlPXt0aGlzLl9vbkxhbmd1YWdlQ2hhbmdlfSB2YWx1ZT17dGhpcy5zdGF0ZS5sYW5ndWFnZX0gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyVGhlbWVTZWN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFNldHRpbmdzRmxhZyA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5lbGVtZW50cy5TZXR0aW5nc0ZsYWdcIik7XHJcbiAgICAgICAgY29uc3QgTGFiZWxsZWRUb2dnbGVTd2l0Y2ggPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZWxlbWVudHMuTGFiZWxsZWRUb2dnbGVTd2l0Y2hcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IHRoZW1lV2F0Y2hlciA9IG5ldyBUaGVtZVdhdGNoZXIoKTtcclxuICAgICAgICBsZXQgc3lzdGVtVGhlbWVTZWN0aW9uO1xyXG4gICAgICAgIGlmICh0aGVtZVdhdGNoZXIuaXNTeXN0ZW1UaGVtZVN1cHBvcnRlZCgpKSB7XHJcbiAgICAgICAgICAgIHN5c3RlbVRoZW1lU2VjdGlvbiA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8TGFiZWxsZWRUb2dnbGVTd2l0Y2hcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS51c2VTeXN0ZW1UaGVtZX1cclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17U2V0dGluZ3NTdG9yZS5nZXREaXNwbGF5TmFtZShcInVzZV9zeXN0ZW1fdGhlbWVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uVXNlU3lzdGVtVGhlbWVDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGN1c3RvbVRoZW1lRm9ybTtcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jdXN0b21fdGhlbWVzXCIpKSB7XHJcbiAgICAgICAgICAgIGxldCBtZXNzYWdlRWxlbWVudCA9IG51bGw7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmN1c3RvbVRoZW1lTWVzc2FnZS50ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXN0b21UaGVtZU1lc3NhZ2UuaXNFcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VFbGVtZW50ID0gPGRpdiBjbGFzc05hbWU9J3RleHQtZXJyb3InPnt0aGlzLnN0YXRlLmN1c3RvbVRoZW1lTWVzc2FnZS50ZXh0fTwvZGl2PjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZUVsZW1lbnQgPSA8ZGl2IGNsYXNzTmFtZT0ndGV4dC1zdWNjZXNzJz57dGhpcy5zdGF0ZS5jdXN0b21UaGVtZU1lc3NhZ2UudGV4dH08L2Rpdj47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY3VzdG9tVGhlbWVGb3JtID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24nPlxyXG4gICAgICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLl9vbkFkZEN1c3RvbVRoZW1lfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJDdXN0b20gdGhlbWUgVVJMXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT0ndGV4dCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Db21wbGV0ZT1cIm9mZlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25DdXN0b21UaGVtZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLmN1c3RvbVRoZW1lVXJsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25BZGRDdXN0b21UaGVtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIiBraW5kPVwicHJpbWFyeV9zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IXRoaXMuc3RhdGUuY3VzdG9tVGhlbWVVcmwudHJpbSgpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+e190KFwiQWRkIHRoZW1lXCIpfTwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge21lc3NhZ2VFbGVtZW50fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdGhlbWVzID0gT2JqZWN0LmVudHJpZXMoZW51bWVyYXRlVGhlbWVzKCkpXHJcbiAgICAgICAgICAgIC5tYXAocCA9PiAoe2lkOiBwWzBdLCBuYW1lOiBwWzFdfSkpOyAvLyBjb252ZXJ0IHBhaXJzIHRvIG9iamVjdHMgZm9yIGNvZGUgcmVhZGFiaWxpdHlcclxuICAgICAgICBjb25zdCBidWlsdEluVGhlbWVzID0gdGhlbWVzLmZpbHRlcihwID0+ICFwLmlkLnN0YXJ0c1dpdGgoXCJjdXN0b20tXCIpKTtcclxuICAgICAgICBjb25zdCBjdXN0b21UaGVtZXMgPSB0aGVtZXMuZmlsdGVyKHAgPT4gIWJ1aWx0SW5UaGVtZXMuaW5jbHVkZXMocCkpXHJcbiAgICAgICAgICAgIC5zb3J0KChhLCBiKSA9PiBhLm5hbWUubG9jYWxlQ29tcGFyZShiLm5hbWUpKTtcclxuICAgICAgICBjb25zdCBvcmRlcmVkVGhlbWVzID0gWy4uLmJ1aWx0SW5UaGVtZXMsIC4uLmN1c3RvbVRoZW1lc107XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uIG14X0dlbmVyYWxVc2VyU2V0dGluZ3NUYWJfdGhlbWVTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiVGhlbWVcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAge3N5c3RlbVRoZW1lU2VjdGlvbn1cclxuICAgICAgICAgICAgICAgIDxGaWVsZCBsYWJlbD17X3QoXCJUaGVtZVwiKX0gZWxlbWVudD1cInNlbGVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUudGhlbWV9IG9uQ2hhbmdlPXt0aGlzLl9vblRoZW1lQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLnVzZVN5c3RlbVRoZW1lfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHtvcmRlcmVkVGhlbWVzLm1hcCh0aGVtZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8b3B0aW9uIGtleT17dGhlbWUuaWR9IHZhbHVlPXt0aGVtZS5pZH0+e3RoZW1lLm5hbWV9PC9vcHRpb24+O1xyXG4gICAgICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICAgICAgPC9GaWVsZD5cclxuICAgICAgICAgICAgICAgIHtjdXN0b21UaGVtZUZvcm19XHJcbiAgICAgICAgICAgICAgICA8U2V0dGluZ3NGbGFnIG5hbWU9XCJ1c2VDb21wYWN0TGF5b3V0XCIgbGV2ZWw9e1NldHRpbmdMZXZlbC5BQ0NPVU5UfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJEaXNjb3ZlcnlTZWN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFNldElkU2VydmVyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLnNldHRpbmdzLlNldElkU2VydmVyXCIpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXF1aXJlZFBvbGljeUluZm8uaGFzVGVybXMpIHtcclxuICAgICAgICAgICAgY29uc3QgSW5saW5lVGVybXNBZ3JlZW1lbnQgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MudGVybXMuSW5saW5lVGVybXNBZ3JlZW1lbnRcIik7XHJcbiAgICAgICAgICAgIGNvbnN0IGludHJvID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHRcIj5cclxuICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkFncmVlIHRvIHRoZSBpZGVudGl0eSBzZXJ2ZXIgKCUoc2VydmVyTmFtZSlzKSBUZXJtcyBvZiBTZXJ2aWNlIHRvIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImFsbG93IHlvdXJzZWxmIHRvIGJlIGRpc2NvdmVyYWJsZSBieSBlbWFpbCBhZGRyZXNzIG9yIHBob25lIG51bWJlci5cIixcclxuICAgICAgICAgICAgICAgICAgICB7c2VydmVyTmFtZTogdGhpcy5zdGF0ZS5pZFNlcnZlck5hbWV9LFxyXG4gICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgPC9zcGFuPjtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPElubGluZVRlcm1zQWdyZWVtZW50XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvbGljaWVzQW5kU2VydmljZVBhaXJzPXt0aGlzLnN0YXRlLnJlcXVpcmVkUG9saWN5SW5mby5wb2xpY2llc0FuZFNlcnZpY2VzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhZ3JlZWRVcmxzPXt0aGlzLnN0YXRlLnJlcXVpcmVkUG9saWN5SW5mby5hZ3JlZWRVcmxzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnN0YXRlLnJlcXVpcmVkUG9saWN5SW5mby5yZXNvbHZlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbnRyb0VsZW1lbnQ9e2ludHJvfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyAvKiBoYXMgaXRzIG93biBoZWFkaW5nIGFzIGl0IGluY2x1ZGVzIHRoZSBjdXJyZW50IElEIHNlcnZlciAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgPFNldElkU2VydmVyIG1pc3NpbmdUZXJtcz17dHJ1ZX0gLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgRW1haWxBZGRyZXNzZXMgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3Muc2V0dGluZ3MuZGlzY292ZXJ5LkVtYWlsQWRkcmVzc2VzXCIpO1xyXG4gICAgICAgIGNvbnN0IFBob25lTnVtYmVycyA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5zZXR0aW5ncy5kaXNjb3ZlcnkuUGhvbmVOdW1iZXJzXCIpO1xyXG5cclxuICAgICAgICBjb25zdCB0aHJlZXBpZFNlY3Rpb24gPSB0aGlzLnN0YXRlLmhhdmVJZFNlcnZlciA/IDxkaXYgY2xhc3NOYW1lPSdteF9HZW5lcmFsVXNlclNldHRpbmdzVGFiX2Rpc2NvdmVyeSc+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJFbWFpbCBhZGRyZXNzZXNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICA8RW1haWxBZGRyZXNzZXMgZW1haWxzPXt0aGlzLnN0YXRlLmVtYWlsc30gLz5cclxuXHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJQaG9uZSBudW1iZXJzXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgPFBob25lTnVtYmVycyBtc2lzZG5zPXt0aGlzLnN0YXRlLm1zaXNkbnN9IC8+XHJcbiAgICAgICAgPC9kaXY+IDogbnVsbDtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICB7dGhyZWVwaWRTZWN0aW9ufVxyXG4gICAgICAgICAgICAgICAgeyAvKiBoYXMgaXRzIG93biBoZWFkaW5nIGFzIGl0IGluY2x1ZGVzIHRoZSBjdXJyZW50IElEIHNlcnZlciAqLyB9XHJcbiAgICAgICAgICAgICAgICA8U2V0SWRTZXJ2ZXIgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyTWFuYWdlbWVudFNlY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gVE9ETzogSW1wcm92ZSB3YXJuaW5nIHRleHQgZm9yIGFjY291bnQgZGVhY3RpdmF0aW9uXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiQWNjb3VudCBtYW5hZ2VtZW50XCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiRGVhY3RpdmF0aW5nIHlvdXIgYWNjb3VudCBpcyBhIHBlcm1hbmVudCBhY3Rpb24gLSBiZSBjYXJlZnVsIVwiKX1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uRGVhY3RpdmF0ZUNsaWNrZWR9IGtpbmQ9XCJkYW5nZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJEZWFjdGl2YXRlIEFjY291bnRcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlckludGVncmF0aW9uTWFuYWdlclNlY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgU2V0SW50ZWdyYXRpb25NYW5hZ2VyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLnNldHRpbmdzLlNldEludGVncmF0aW9uTWFuYWdlclwiKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICB7IC8qIGhhcyBpdHMgb3duIGhlYWRpbmcgYXMgaXQgaW5jbHVkZXMgdGhlIGN1cnJlbnQgaW50ZWdyYXRpb24gbWFuYWdlciAqLyB9XHJcbiAgICAgICAgICAgICAgICA8U2V0SW50ZWdyYXRpb25NYW5hZ2VyIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGRpc2NvV2FybmluZyA9IHRoaXMuc3RhdGUucmVxdWlyZWRQb2xpY3lJbmZvLmhhc1Rlcm1zXHJcbiAgICAgICAgICAgID8gPGltZyBjbGFzc05hbWU9J214X0dlbmVyYWxVc2VyU2V0dGluZ3NUYWJfd2FybmluZ0ljb24nXHJcbiAgICAgICAgICAgICAgICBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9yZXMvaW1nL2ZlYXRoZXItY3VzdG9taXNlZC93YXJuaW5nLXRyaWFuZ2xlLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgIHdpZHRoPVwiMThcIiBoZWlnaHQ9XCIxOFwiIGFsdD17X3QoXCJXYXJuaW5nXCIpfSAvPlxyXG4gICAgICAgICAgICA6IG51bGw7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfaGVhZGluZ1wiPntfdChcIkdlbmVyYWxcIil9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyUHJvZmlsZVNlY3Rpb24oKX1cclxuICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJBY2NvdW50U2VjdGlvbigpfVxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlckxhbmd1YWdlU2VjdGlvbigpfVxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlclRoZW1lU2VjdGlvbigpfVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9oZWFkaW5nXCI+e2Rpc2NvV2FybmluZ30ge190KFwiRGlzY292ZXJ5XCIpfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlckRpc2NvdmVyeVNlY3Rpb24oKX1cclxuICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJJbnRlZ3JhdGlvbk1hbmFnZXJTZWN0aW9uKCkgLyogSGFzIGl0cyBvd24gdGl0bGUgKi99XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX2hlYWRpbmdcIj57X3QoXCJEZWFjdGl2YXRlIGFjY291bnRcIil9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyTWFuYWdlbWVudFNlY3Rpb24oKX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=