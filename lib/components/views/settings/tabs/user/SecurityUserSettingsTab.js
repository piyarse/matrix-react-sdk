"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.IgnoredUser = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../../languageHandler");

var _SettingsStore = _interopRequireWildcard3(require("../../../../../settings/SettingsStore"));

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var FormattingUtils = _interopRequireWildcard3(require("../../../../../utils/FormattingUtils"));

var _AccessibleButton = _interopRequireDefault(require("../../../elements/AccessibleButton"));

var _Analytics = _interopRequireDefault(require("../../../../../Analytics"));

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

var sdk = _interopRequireWildcard3(require("../../../../.."));

var _promise = require("../../../../../utils/promise");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class IgnoredUser extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onUnignoreClicked", e => {
      this.props.onUnignored(this.props.userId);
    });
  }

  render() {
    const id = "mx_SecurityUserSettingsTab_ignoredUser_".concat(this.props.userId);
    return _react.default.createElement("div", {
      className: "mx_SecurityUserSettingsTab_ignoredUser"
    }, _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onUnignoreClicked,
      kind: "primary_sm",
      "aria-describedby": id
    }, (0, _languageHandler._t)('Unignore')), _react.default.createElement("span", {
      id: id
    }, this.props.userId));
  }

}

exports.IgnoredUser = IgnoredUser;
(0, _defineProperty2.default)(IgnoredUser, "propTypes", {
  userId: _propTypes.default.string.isRequired,
  onUnignored: _propTypes.default.func.isRequired
});

class SecurityUserSettingsTab extends _react.default.Component {
  constructor() {
    super(); // Get number of rooms we're invited to

    (0, _defineProperty2.default)(this, "_updateBlacklistDevicesFlag", checked => {
      _MatrixClientPeg.MatrixClientPeg.get().setGlobalBlacklistUnverifiedDevices(checked);
    });
    (0, _defineProperty2.default)(this, "_updateAnalytics", checked => {
      checked ? _Analytics.default.enable() : _Analytics.default.disable();
    });
    (0, _defineProperty2.default)(this, "_onExportE2eKeysClicked", () => {
      _Modal.default.createTrackedDialogAsync('Export E2E Keys', '', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../../../async-components/views/dialogs/ExportE2eKeysDialog'))), {
        matrixClient: _MatrixClientPeg.MatrixClientPeg.get()
      });
    });
    (0, _defineProperty2.default)(this, "_onImportE2eKeysClicked", () => {
      _Modal.default.createTrackedDialogAsync('Import E2E Keys', '', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../../../async-components/views/dialogs/ImportE2eKeysDialog'))), {
        matrixClient: _MatrixClientPeg.MatrixClientPeg.get()
      });
    });
    (0, _defineProperty2.default)(this, "_onUserUnignored", async userId => {
      // Don't use this.state to get the ignored user list as it might be
      // ever so slightly outdated. Instead, prefer to get a fresh list and
      // update that.
      const ignoredUsers = _MatrixClientPeg.MatrixClientPeg.get().getIgnoredUsers();

      const index = ignoredUsers.indexOf(userId);

      if (index !== -1) {
        ignoredUsers.splice(index, 1);

        _MatrixClientPeg.MatrixClientPeg.get().setIgnoredUsers(ignoredUsers);
      }

      this.setState({
        ignoredUsers
      });
    });
    (0, _defineProperty2.default)(this, "_getInvitedRooms", () => {
      return _MatrixClientPeg.MatrixClientPeg.get().getRooms().filter(r => {
        return r.hasMembershipState(_MatrixClientPeg.MatrixClientPeg.get().getUserId(), "invite");
      });
    });
    (0, _defineProperty2.default)(this, "_manageInvites", async accept => {
      this.setState({
        managingInvites: true
      }); // Compile array of invitation room ids

      const invitedRoomIds = this._getInvitedRooms().map(room => {
        return room.roomId;
      }); // Execute all acceptances/rejections sequentially


      const self = this;

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const action = accept ? cli.joinRoom.bind(cli) : cli.leave.bind(cli);

      for (let i = 0; i < invitedRoomIds.length; i++) {
        const roomId = invitedRoomIds[i]; // Accept/reject invite

        await action(roomId).then(() => {
          // No error, update invited rooms button
          this.setState({
            invitedRoomAmt: self.state.invitedRoomAmt - 1
          });
        }, async e => {
          // Action failure
          if (e.errcode === "M_LIMIT_EXCEEDED") {
            // Add a delay between each invite change in order to avoid rate
            // limiting by the server.
            await (0, _promise.sleep)(e.retry_after_ms || 2500); // Redo last action

            i--;
          } else {
            // Print out error with joining/leaving room
            console.warn(e);
          }
        });
      }

      this.setState({
        managingInvites: false
      });
    });
    (0, _defineProperty2.default)(this, "_onAcceptAllInvitesClicked", ev => {
      this._manageInvites(true);
    });
    (0, _defineProperty2.default)(this, "_onRejectAllInvitesClicked", ev => {
      this._manageInvites(false);
    });

    const invitedRooms = this._getInvitedRooms();

    this.state = {
      ignoredUserIds: _MatrixClientPeg.MatrixClientPeg.get().getIgnoredUsers(),
      managingInvites: false,
      invitedRoomAmt: invitedRooms.length
    };
  }

  _renderCurrentDeviceInfo() {
    const SettingsFlag = sdk.getComponent('views.elements.SettingsFlag');

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const deviceId = client.deviceId;
    let identityKey = client.getDeviceEd25519Key();

    if (!identityKey) {
      identityKey = (0, _languageHandler._t)("<not supported>");
    } else {
      identityKey = FormattingUtils.formatCryptoKey(identityKey);
    }

    let importExportButtons = null;

    if (client.isCryptoEnabled()) {
      importExportButtons = _react.default.createElement("div", {
        className: "mx_SecurityUserSettingsTab_importExportButtons"
      }, _react.default.createElement(_AccessibleButton.default, {
        kind: "primary",
        onClick: this._onExportE2eKeysClicked
      }, (0, _languageHandler._t)("Export E2E room keys")), _react.default.createElement(_AccessibleButton.default, {
        kind: "primary",
        onClick: this._onImportE2eKeysClicked
      }, (0, _languageHandler._t)("Import E2E room keys")));
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Cryptography")), _react.default.createElement("ul", {
      className: "mx_SettingsTab_subsectionText mx_SecurityUserSettingsTab_deviceInfo"
    }, _react.default.createElement("li", null, _react.default.createElement("label", null, (0, _languageHandler._t)("Session ID:")), _react.default.createElement("span", null, _react.default.createElement("code", null, deviceId))), _react.default.createElement("li", null, _react.default.createElement("label", null, (0, _languageHandler._t)("Session key:")), _react.default.createElement("span", null, _react.default.createElement("code", null, _react.default.createElement("b", null, identityKey))))), importExportButtons, _react.default.createElement(SettingsFlag, {
      name: "blacklistUnverifiedDevices",
      level: _SettingsStore.SettingLevel.DEVICE,
      onChange: this._updateBlacklistDevicesFlag
    }));
  }

  _renderIgnoredUsers() {
    if (!this.state.ignoredUserIds || this.state.ignoredUserIds.length === 0) return null;
    const userIds = this.state.ignoredUserIds.map(u => _react.default.createElement(IgnoredUser, {
      userId: u,
      onUnignored: this._onUserUnignored,
      key: u
    }));
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)('Ignored users')), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, userIds));
  }

  _renderManageInvites() {
    if (this.state.invitedRoomAmt === 0) {
      return null;
    }

    const invitedRooms = this._getInvitedRooms();

    const InlineSpinner = sdk.getComponent('elements.InlineSpinner');

    const onClickAccept = this._onAcceptAllInvitesClicked.bind(this, invitedRooms);

    const onClickReject = this._onRejectAllInvitesClicked.bind(this, invitedRooms);

    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_SecurityUserSettingsTab_bulkOptions"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)('Bulk options')), _react.default.createElement(_AccessibleButton.default, {
      onClick: onClickAccept,
      kind: "primary",
      disabled: this.state.managingInvites
    }, (0, _languageHandler._t)("Accept all %(invitedRooms)s invites", {
      invitedRooms: this.state.invitedRoomAmt
    })), _react.default.createElement(_AccessibleButton.default, {
      onClick: onClickReject,
      kind: "danger",
      disabled: this.state.managingInvites
    }, (0, _languageHandler._t)("Reject all %(invitedRooms)s invites", {
      invitedRooms: this.state.invitedRoomAmt
    })), this.state.managingInvites ? _react.default.createElement(InlineSpinner, null) : _react.default.createElement("div", null));
  }

  render() {
    const DevicesPanel = sdk.getComponent('views.settings.DevicesPanel');
    const SettingsFlag = sdk.getComponent('views.elements.SettingsFlag');
    const EventIndexPanel = sdk.getComponent('views.settings.EventIndexPanel');
    const KeyBackupPanel = sdk.getComponent('views.settings.KeyBackupPanel');

    const keyBackup = _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Key backup")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, _react.default.createElement(KeyBackupPanel, null)));

    let eventIndex;

    if (_SettingsStore.default.isFeatureEnabled("feature_event_indexing")) {
      eventIndex = _react.default.createElement("div", {
        className: "mx_SettingsTab_section"
      }, _react.default.createElement("span", {
        className: "mx_SettingsTab_subheading"
      }, (0, _languageHandler._t)("Message search")), _react.default.createElement(EventIndexPanel, null));
    } // XXX: There's no such panel in the current cross-signing designs, but
    // it's useful to have for testing the feature. If there's no interest
    // in having advanced details here once all flows are implemented, we
    // can remove this.


    const CrossSigningPanel = sdk.getComponent('views.settings.CrossSigningPanel');
    let crossSigning;

    if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      crossSigning = _react.default.createElement("div", {
        className: "mx_SettingsTab_section"
      }, _react.default.createElement("span", {
        className: "mx_SettingsTab_subheading"
      }, (0, _languageHandler._t)("Cross-signing")), _react.default.createElement("div", {
        className: "mx_SettingsTab_subsectionText"
      }, _react.default.createElement(CrossSigningPanel, null)));
    }

    const E2eAdvancedPanel = sdk.getComponent('views.settings.E2eAdvancedPanel');
    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_SecurityUserSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Security & Privacy")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Sessions")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("A session's public name is visible to people you communicate with"), _react.default.createElement(DevicesPanel, null))), keyBackup, eventIndex, crossSigning, this._renderCurrentDeviceInfo(), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Analytics")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("Riot collects anonymous analytics to allow us to improve the application."), "\xA0", (0, _languageHandler._t)("Privacy is important to us, so we don't collect any personal or " + "identifiable data for our analytics."), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_SettingsTab_linkBtn",
      onClick: _Analytics.default.showDetailsModal
    }, (0, _languageHandler._t)("Learn more about how we use analytics."))), _react.default.createElement(SettingsFlag, {
      name: "analyticsOptIn",
      level: _SettingsStore.SettingLevel.DEVICE,
      onChange: this._updateAnalytics
    })), this._renderIgnoredUsers(), this._renderManageInvites(), _react.default.createElement(E2eAdvancedPanel, null));
  }

}

exports.default = SecurityUserSettingsTab;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9TZWN1cml0eVVzZXJTZXR0aW5nc1RhYi5qcyJdLCJuYW1lcyI6WyJJZ25vcmVkVXNlciIsIlJlYWN0IiwiQ29tcG9uZW50IiwiZSIsInByb3BzIiwib25Vbmlnbm9yZWQiLCJ1c2VySWQiLCJyZW5kZXIiLCJpZCIsIl9vblVuaWdub3JlQ2xpY2tlZCIsIlByb3BUeXBlcyIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJmdW5jIiwiU2VjdXJpdHlVc2VyU2V0dGluZ3NUYWIiLCJjb25zdHJ1Y3RvciIsImNoZWNrZWQiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJzZXRHbG9iYWxCbGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlcyIsIkFuYWx5dGljcyIsImVuYWJsZSIsImRpc2FibGUiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2dBc3luYyIsIm1hdHJpeENsaWVudCIsImlnbm9yZWRVc2VycyIsImdldElnbm9yZWRVc2VycyIsImluZGV4IiwiaW5kZXhPZiIsInNwbGljZSIsInNldElnbm9yZWRVc2VycyIsInNldFN0YXRlIiwiZ2V0Um9vbXMiLCJmaWx0ZXIiLCJyIiwiaGFzTWVtYmVyc2hpcFN0YXRlIiwiZ2V0VXNlcklkIiwiYWNjZXB0IiwibWFuYWdpbmdJbnZpdGVzIiwiaW52aXRlZFJvb21JZHMiLCJfZ2V0SW52aXRlZFJvb21zIiwibWFwIiwicm9vbSIsInJvb21JZCIsInNlbGYiLCJjbGkiLCJhY3Rpb24iLCJqb2luUm9vbSIsImJpbmQiLCJsZWF2ZSIsImkiLCJsZW5ndGgiLCJ0aGVuIiwiaW52aXRlZFJvb21BbXQiLCJzdGF0ZSIsImVycmNvZGUiLCJyZXRyeV9hZnRlcl9tcyIsImNvbnNvbGUiLCJ3YXJuIiwiZXYiLCJfbWFuYWdlSW52aXRlcyIsImludml0ZWRSb29tcyIsImlnbm9yZWRVc2VySWRzIiwiX3JlbmRlckN1cnJlbnREZXZpY2VJbmZvIiwiU2V0dGluZ3NGbGFnIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY2xpZW50IiwiZGV2aWNlSWQiLCJpZGVudGl0eUtleSIsImdldERldmljZUVkMjU1MTlLZXkiLCJGb3JtYXR0aW5nVXRpbHMiLCJmb3JtYXRDcnlwdG9LZXkiLCJpbXBvcnRFeHBvcnRCdXR0b25zIiwiaXNDcnlwdG9FbmFibGVkIiwiX29uRXhwb3J0RTJlS2V5c0NsaWNrZWQiLCJfb25JbXBvcnRFMmVLZXlzQ2xpY2tlZCIsIlNldHRpbmdMZXZlbCIsIkRFVklDRSIsIl91cGRhdGVCbGFja2xpc3REZXZpY2VzRmxhZyIsIl9yZW5kZXJJZ25vcmVkVXNlcnMiLCJ1c2VySWRzIiwidSIsIl9vblVzZXJVbmlnbm9yZWQiLCJfcmVuZGVyTWFuYWdlSW52aXRlcyIsIklubGluZVNwaW5uZXIiLCJvbkNsaWNrQWNjZXB0IiwiX29uQWNjZXB0QWxsSW52aXRlc0NsaWNrZWQiLCJvbkNsaWNrUmVqZWN0IiwiX29uUmVqZWN0QWxsSW52aXRlc0NsaWNrZWQiLCJEZXZpY2VzUGFuZWwiLCJFdmVudEluZGV4UGFuZWwiLCJLZXlCYWNrdXBQYW5lbCIsImtleUJhY2t1cCIsImV2ZW50SW5kZXgiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsIkNyb3NzU2lnbmluZ1BhbmVsIiwiY3Jvc3NTaWduaW5nIiwiRTJlQWR2YW5jZWRQYW5lbCIsInNob3dEZXRhaWxzTW9kYWwiLCJfdXBkYXRlQW5hbHl0aWNzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7QUE0Qk8sTUFBTUEsV0FBTixTQUEwQkMsZUFBTUMsU0FBaEMsQ0FBMEM7QUFBQTtBQUFBO0FBQUEsOERBTXZCQyxDQUFELElBQU87QUFDeEIsV0FBS0MsS0FBTCxDQUFXQyxXQUFYLENBQXVCLEtBQUtELEtBQUwsQ0FBV0UsTUFBbEM7QUFDSCxLQVI0QztBQUFBOztBQVU3Q0MsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsRUFBRSxvREFBNkMsS0FBS0osS0FBTCxDQUFXRSxNQUF4RCxDQUFSO0FBQ0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRSxLQUFLRyxrQkFBaEM7QUFBb0QsTUFBQSxJQUFJLEVBQUMsWUFBekQ7QUFBc0UsMEJBQWtCRDtBQUF4RixPQUNNLHlCQUFHLFVBQUgsQ0FETixDQURKLEVBSUk7QUFBTSxNQUFBLEVBQUUsRUFBRUE7QUFBVixPQUFnQixLQUFLSixLQUFMLENBQVdFLE1BQTNCLENBSkosQ0FESjtBQVFIOztBQXBCNEM7Ozs4QkFBcENOLFcsZUFDVTtBQUNmTSxFQUFBQSxNQUFNLEVBQUVJLG1CQUFVQyxNQUFWLENBQWlCQyxVQURWO0FBRWZQLEVBQUFBLFdBQVcsRUFBRUssbUJBQVVHLElBQVYsQ0FBZUQ7QUFGYixDOztBQXNCUixNQUFNRSx1QkFBTixTQUFzQ2IsZUFBTUMsU0FBNUMsQ0FBc0Q7QUFDakVhLEVBQUFBLFdBQVcsR0FBRztBQUNWLFlBRFUsQ0FHVjs7QUFIVSx1RUFhaUJDLE9BQUQsSUFBYTtBQUN2Q0MsdUNBQWdCQyxHQUFoQixHQUFzQkMsbUNBQXRCLENBQTBESCxPQUExRDtBQUNILEtBZmE7QUFBQSw0REFpQk1BLE9BQUQsSUFBYTtBQUM1QkEsTUFBQUEsT0FBTyxHQUFHSSxtQkFBVUMsTUFBVixFQUFILEdBQXdCRCxtQkFBVUUsT0FBVixFQUEvQjtBQUNILEtBbkJhO0FBQUEsbUVBcUJZLE1BQU07QUFDNUJDLHFCQUFNQyx3QkFBTixDQUErQixpQkFBL0IsRUFBa0QsRUFBbEQsNkVBQ1csbUVBRFgsS0FFSTtBQUFDQyxRQUFBQSxZQUFZLEVBQUVSLGlDQUFnQkMsR0FBaEI7QUFBZixPQUZKO0FBSUgsS0ExQmE7QUFBQSxtRUE0QlksTUFBTTtBQUM1QksscUJBQU1DLHdCQUFOLENBQStCLGlCQUEvQixFQUFrRCxFQUFsRCw2RUFDVyxtRUFEWCxLQUVJO0FBQUNDLFFBQUFBLFlBQVksRUFBRVIsaUNBQWdCQyxHQUFoQjtBQUFmLE9BRko7QUFJSCxLQWpDYTtBQUFBLDREQW1DSyxNQUFPWixNQUFQLElBQWtCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLFlBQU1vQixZQUFZLEdBQUdULGlDQUFnQkMsR0FBaEIsR0FBc0JTLGVBQXRCLEVBQXJCOztBQUNBLFlBQU1DLEtBQUssR0FBR0YsWUFBWSxDQUFDRyxPQUFiLENBQXFCdkIsTUFBckIsQ0FBZDs7QUFDQSxVQUFJc0IsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQjtBQUNkRixRQUFBQSxZQUFZLENBQUNJLE1BQWIsQ0FBb0JGLEtBQXBCLEVBQTJCLENBQTNCOztBQUNBWCx5Q0FBZ0JDLEdBQWhCLEdBQXNCYSxlQUF0QixDQUFzQ0wsWUFBdEM7QUFDSDs7QUFDRCxXQUFLTSxRQUFMLENBQWM7QUFBQ04sUUFBQUE7QUFBRCxPQUFkO0FBQ0gsS0E5Q2E7QUFBQSw0REFnREssTUFBTTtBQUNyQixhQUFPVCxpQ0FBZ0JDLEdBQWhCLEdBQXNCZSxRQUF0QixHQUFpQ0MsTUFBakMsQ0FBeUNDLENBQUQsSUFBTztBQUNsRCxlQUFPQSxDQUFDLENBQUNDLGtCQUFGLENBQXFCbkIsaUNBQWdCQyxHQUFoQixHQUFzQm1CLFNBQXRCLEVBQXJCLEVBQXdELFFBQXhELENBQVA7QUFDSCxPQUZNLENBQVA7QUFHSCxLQXBEYTtBQUFBLDBEQXNERyxNQUFPQyxNQUFQLElBQWtCO0FBQy9CLFdBQUtOLFFBQUwsQ0FBYztBQUNWTyxRQUFBQSxlQUFlLEVBQUU7QUFEUCxPQUFkLEVBRCtCLENBSy9COztBQUNBLFlBQU1DLGNBQWMsR0FBRyxLQUFLQyxnQkFBTCxHQUF3QkMsR0FBeEIsQ0FBNkJDLElBQUQsSUFBVTtBQUN6RCxlQUFPQSxJQUFJLENBQUNDLE1BQVo7QUFDSCxPQUZzQixDQUF2QixDQU4rQixDQVUvQjs7O0FBQ0EsWUFBTUMsSUFBSSxHQUFHLElBQWI7O0FBQ0EsWUFBTUMsR0FBRyxHQUFHN0IsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFlBQU02QixNQUFNLEdBQUdULE1BQU0sR0FBR1EsR0FBRyxDQUFDRSxRQUFKLENBQWFDLElBQWIsQ0FBa0JILEdBQWxCLENBQUgsR0FBNEJBLEdBQUcsQ0FBQ0ksS0FBSixDQUFVRCxJQUFWLENBQWVILEdBQWYsQ0FBakQ7O0FBQ0EsV0FBSyxJQUFJSyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWCxjQUFjLENBQUNZLE1BQW5DLEVBQTJDRCxDQUFDLEVBQTVDLEVBQWdEO0FBQzVDLGNBQU1QLE1BQU0sR0FBR0osY0FBYyxDQUFDVyxDQUFELENBQTdCLENBRDRDLENBRzVDOztBQUNBLGNBQU1KLE1BQU0sQ0FBQ0gsTUFBRCxDQUFOLENBQWVTLElBQWYsQ0FBb0IsTUFBTTtBQUM1QjtBQUNBLGVBQUtyQixRQUFMLENBQWM7QUFBQ3NCLFlBQUFBLGNBQWMsRUFBRVQsSUFBSSxDQUFDVSxLQUFMLENBQVdELGNBQVgsR0FBNEI7QUFBN0MsV0FBZDtBQUNILFNBSEssRUFHSCxNQUFPbkQsQ0FBUCxJQUFhO0FBQ1o7QUFDQSxjQUFJQSxDQUFDLENBQUNxRCxPQUFGLEtBQWMsa0JBQWxCLEVBQXNDO0FBQ2xDO0FBQ0E7QUFDQSxrQkFBTSxvQkFBTXJELENBQUMsQ0FBQ3NELGNBQUYsSUFBb0IsSUFBMUIsQ0FBTixDQUhrQyxDQUtsQzs7QUFDQU4sWUFBQUEsQ0FBQztBQUNKLFdBUEQsTUFPTztBQUNIO0FBQ0FPLFlBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFheEQsQ0FBYjtBQUNIO0FBQ0osU0FoQkssQ0FBTjtBQWlCSDs7QUFFRCxXQUFLNkIsUUFBTCxDQUFjO0FBQ1ZPLFFBQUFBLGVBQWUsRUFBRTtBQURQLE9BQWQ7QUFHSCxLQTlGYTtBQUFBLHNFQWdHZ0JxQixFQUFELElBQVE7QUFDakMsV0FBS0MsY0FBTCxDQUFvQixJQUFwQjtBQUNILEtBbEdhO0FBQUEsc0VBb0dnQkQsRUFBRCxJQUFRO0FBQ2pDLFdBQUtDLGNBQUwsQ0FBb0IsS0FBcEI7QUFDSCxLQXRHYTs7QUFJVixVQUFNQyxZQUFZLEdBQUcsS0FBS3JCLGdCQUFMLEVBQXJCOztBQUVBLFNBQUtjLEtBQUwsR0FBYTtBQUNUUSxNQUFBQSxjQUFjLEVBQUU5QyxpQ0FBZ0JDLEdBQWhCLEdBQXNCUyxlQUF0QixFQURQO0FBRVRZLE1BQUFBLGVBQWUsRUFBRSxLQUZSO0FBR1RlLE1BQUFBLGNBQWMsRUFBRVEsWUFBWSxDQUFDVjtBQUhwQixLQUFiO0FBS0g7O0FBNkZEWSxFQUFBQSx3QkFBd0IsR0FBRztBQUN2QixVQUFNQyxZQUFZLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBckI7O0FBRUEsVUFBTUMsTUFBTSxHQUFHbkQsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFVBQU1tRCxRQUFRLEdBQUdELE1BQU0sQ0FBQ0MsUUFBeEI7QUFDQSxRQUFJQyxXQUFXLEdBQUdGLE1BQU0sQ0FBQ0csbUJBQVAsRUFBbEI7O0FBQ0EsUUFBSSxDQUFDRCxXQUFMLEVBQWtCO0FBQ2RBLE1BQUFBLFdBQVcsR0FBRyx5QkFBRyxpQkFBSCxDQUFkO0FBQ0gsS0FGRCxNQUVPO0FBQ0hBLE1BQUFBLFdBQVcsR0FBR0UsZUFBZSxDQUFDQyxlQUFoQixDQUFnQ0gsV0FBaEMsQ0FBZDtBQUNIOztBQUVELFFBQUlJLG1CQUFtQixHQUFHLElBQTFCOztBQUNBLFFBQUlOLE1BQU0sQ0FBQ08sZUFBUCxFQUFKLEVBQThCO0FBQzFCRCxNQUFBQSxtQkFBbUIsR0FDZjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxRQUFBLE9BQU8sRUFBRSxLQUFLRTtBQUEvQyxTQUNLLHlCQUFHLHNCQUFILENBREwsQ0FESixFQUlJLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsSUFBSSxFQUFDLFNBQXZCO0FBQWlDLFFBQUEsT0FBTyxFQUFFLEtBQUtDO0FBQS9DLFNBQ0sseUJBQUcsc0JBQUgsQ0FETCxDQUpKLENBREo7QUFVSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsY0FBSCxDQUE3QyxDQURKLEVBRUk7QUFBSSxNQUFBLFNBQVMsRUFBQztBQUFkLE9BQ0kseUNBQ0ksNENBQVEseUJBQUcsYUFBSCxDQUFSLENBREosRUFFSSwyQ0FBTSwyQ0FBT1IsUUFBUCxDQUFOLENBRkosQ0FESixFQUtJLHlDQUNJLDRDQUFRLHlCQUFHLGNBQUgsQ0FBUixDQURKLEVBRUksMkNBQU0sMkNBQU0sd0NBQUlDLFdBQUosQ0FBTixDQUFOLENBRkosQ0FMSixDQUZKLEVBWUtJLG1CQVpMLEVBYUksNkJBQUMsWUFBRDtBQUFjLE1BQUEsSUFBSSxFQUFDLDRCQUFuQjtBQUFnRCxNQUFBLEtBQUssRUFBRUksNEJBQWFDLE1BQXBFO0FBQ2MsTUFBQSxRQUFRLEVBQUUsS0FBS0M7QUFEN0IsTUFiSixDQURKO0FBa0JIOztBQUVEQyxFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixRQUFJLENBQUMsS0FBSzFCLEtBQUwsQ0FBV1EsY0FBWixJQUE4QixLQUFLUixLQUFMLENBQVdRLGNBQVgsQ0FBMEJYLE1BQTFCLEtBQXFDLENBQXZFLEVBQTBFLE9BQU8sSUFBUDtBQUUxRSxVQUFNOEIsT0FBTyxHQUFHLEtBQUszQixLQUFMLENBQVdRLGNBQVgsQ0FDWHJCLEdBRFcsQ0FDTnlDLENBQUQsSUFBTyw2QkFBQyxXQUFEO0FBQWEsTUFBQSxNQUFNLEVBQUVBLENBQXJCO0FBQXdCLE1BQUEsV0FBVyxFQUFFLEtBQUtDLGdCQUExQztBQUE0RCxNQUFBLEdBQUcsRUFBRUQ7QUFBakUsTUFEQSxDQUFoQjtBQUdBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxlQUFILENBQTdDLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS0QsT0FETCxDQUZKLENBREo7QUFRSDs7QUFFREcsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsUUFBSSxLQUFLOUIsS0FBTCxDQUFXRCxjQUFYLEtBQThCLENBQWxDLEVBQXFDO0FBQ2pDLGFBQU8sSUFBUDtBQUNIOztBQUVELFVBQU1RLFlBQVksR0FBRyxLQUFLckIsZ0JBQUwsRUFBckI7O0FBQ0EsVUFBTTZDLGFBQWEsR0FBR3BCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7O0FBQ0EsVUFBTW9CLGFBQWEsR0FBRyxLQUFLQywwQkFBTCxDQUFnQ3ZDLElBQWhDLENBQXFDLElBQXJDLEVBQTJDYSxZQUEzQyxDQUF0Qjs7QUFDQSxVQUFNMkIsYUFBYSxHQUFHLEtBQUtDLDBCQUFMLENBQWdDekMsSUFBaEMsQ0FBcUMsSUFBckMsRUFBMkNhLFlBQTNDLENBQXRCOztBQUNBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxjQUFILENBQTdDLENBREosRUFFSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRXlCLGFBQTNCO0FBQTBDLE1BQUEsSUFBSSxFQUFDLFNBQS9DO0FBQXlELE1BQUEsUUFBUSxFQUFFLEtBQUtoQyxLQUFMLENBQVdoQjtBQUE5RSxPQUNLLHlCQUFHLHFDQUFILEVBQTBDO0FBQUN1QixNQUFBQSxZQUFZLEVBQUUsS0FBS1AsS0FBTCxDQUFXRDtBQUExQixLQUExQyxDQURMLENBRkosRUFLSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRW1DLGFBQTNCO0FBQTBDLE1BQUEsSUFBSSxFQUFDLFFBQS9DO0FBQXdELE1BQUEsUUFBUSxFQUFFLEtBQUtsQyxLQUFMLENBQVdoQjtBQUE3RSxPQUNLLHlCQUFHLHFDQUFILEVBQTBDO0FBQUN1QixNQUFBQSxZQUFZLEVBQUUsS0FBS1AsS0FBTCxDQUFXRDtBQUExQixLQUExQyxDQURMLENBTEosRUFRSyxLQUFLQyxLQUFMLENBQVdoQixlQUFYLEdBQTZCLDZCQUFDLGFBQUQsT0FBN0IsR0FBaUQseUNBUnRELENBREo7QUFZSDs7QUFFRGhDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1vRixZQUFZLEdBQUd6QixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQXJCO0FBQ0EsVUFBTUYsWUFBWSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQXJCO0FBQ0EsVUFBTXlCLGVBQWUsR0FBRzFCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQ0FBakIsQ0FBeEI7QUFFQSxVQUFNMEIsY0FBYyxHQUFHM0IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUF2Qjs7QUFDQSxVQUFNMkIsU0FBUyxHQUNYO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsWUFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsY0FBRCxPQURKLENBRkosQ0FESjs7QUFTQSxRQUFJQyxVQUFKOztBQUNBLFFBQUlDLHVCQUFjQyxnQkFBZCxDQUErQix3QkFBL0IsQ0FBSixFQUE4RDtBQUMxREYsTUFBQUEsVUFBVSxHQUNOO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBNkMseUJBQUcsZ0JBQUgsQ0FBN0MsQ0FESixFQUVJLDZCQUFDLGVBQUQsT0FGSixDQURKO0FBTUgsS0F2QkksQ0F5Qkw7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFVBQU1HLGlCQUFpQixHQUFHaEMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtDQUFqQixDQUExQjtBQUNBLFFBQUlnQyxZQUFKOztBQUNBLFFBQUlILHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6REUsTUFBQUEsWUFBWSxHQUNSO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBNkMseUJBQUcsZUFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsaUJBQUQsT0FESixDQUZKLENBREo7QUFRSDs7QUFFRCxVQUFNQyxnQkFBZ0IsR0FBR2xDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQ0FBakIsQ0FBekI7QUFFQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF5Qyx5QkFBRyxvQkFBSCxDQUF6QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxVQUFILENBQTdDLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyx5QkFBRyxtRUFBSCxDQURMLEVBRUksNkJBQUMsWUFBRCxPQUZKLENBRkosQ0FGSixFQVNLMkIsU0FUTCxFQVVLQyxVQVZMLEVBV0tJLFlBWEwsRUFZSyxLQUFLbkMsd0JBQUwsRUFaTCxFQWFJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsV0FBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0sseUJBQUcsMkVBQUgsQ0FETCxVQUdLLHlCQUFHLHFFQUNBLHNDQURILENBSEwsRUFLSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBQyx3QkFBNUI7QUFBcUQsTUFBQSxPQUFPLEVBQUU1QyxtQkFBVWlGO0FBQXhFLE9BQ0sseUJBQUcsd0NBQUgsQ0FETCxDQUxKLENBRkosRUFXSSw2QkFBQyxZQUFEO0FBQWMsTUFBQSxJQUFJLEVBQUMsZ0JBQW5CO0FBQW9DLE1BQUEsS0FBSyxFQUFFdkIsNEJBQWFDLE1BQXhEO0FBQ2MsTUFBQSxRQUFRLEVBQUUsS0FBS3VCO0FBRDdCLE1BWEosQ0FiSixFQTJCSyxLQUFLckIsbUJBQUwsRUEzQkwsRUE0QkssS0FBS0ksb0JBQUwsRUE1QkwsRUE2QkksNkJBQUMsZ0JBQUQsT0E3QkosQ0FESjtBQWlDSDs7QUEzUWdFIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlLCB7U2V0dGluZ0xldmVsfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgKiBhcyBGb3JtYXR0aW5nVXRpbHMgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL3V0aWxzL0Zvcm1hdHRpbmdVdGlsc1wiO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvblwiO1xyXG5pbXBvcnQgQW5hbHl0aWNzIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9BbmFseXRpY3NcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9Nb2RhbFwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uLy4uLy4uXCI7XHJcbmltcG9ydCB7c2xlZXB9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi91dGlscy9wcm9taXNlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgSWdub3JlZFVzZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICB1c2VySWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBvblVuaWdub3JlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgX29uVW5pZ25vcmVDbGlja2VkID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uVW5pZ25vcmVkKHRoaXMucHJvcHMudXNlcklkKTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGlkID0gYG14X1NlY3VyaXR5VXNlclNldHRpbmdzVGFiX2lnbm9yZWRVc2VyXyR7dGhpcy5wcm9wcy51c2VySWR9YDtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2VjdXJpdHlVc2VyU2V0dGluZ3NUYWJfaWdub3JlZFVzZXInPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25Vbmlnbm9yZUNsaWNrZWR9IGtpbmQ9J3ByaW1hcnlfc20nIGFyaWEtZGVzY3JpYmVkYnk9e2lkfT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdVbmlnbm9yZScpIH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGlkPXtpZH0+eyB0aGlzLnByb3BzLnVzZXJJZCB9PC9zcGFuPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZWN1cml0eVVzZXJTZXR0aW5nc1RhYiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICAvLyBHZXQgbnVtYmVyIG9mIHJvb21zIHdlJ3JlIGludml0ZWQgdG9cclxuICAgICAgICBjb25zdCBpbnZpdGVkUm9vbXMgPSB0aGlzLl9nZXRJbnZpdGVkUm9vbXMoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaWdub3JlZFVzZXJJZHM6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRJZ25vcmVkVXNlcnMoKSxcclxuICAgICAgICAgICAgbWFuYWdpbmdJbnZpdGVzOiBmYWxzZSxcclxuICAgICAgICAgICAgaW52aXRlZFJvb21BbXQ6IGludml0ZWRSb29tcy5sZW5ndGgsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBfdXBkYXRlQmxhY2tsaXN0RGV2aWNlc0ZsYWcgPSAoY2hlY2tlZCkgPT4ge1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZXRHbG9iYWxCbGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlcyhjaGVja2VkKTtcclxuICAgIH07XHJcblxyXG4gICAgX3VwZGF0ZUFuYWx5dGljcyA9IChjaGVja2VkKSA9PiB7XHJcbiAgICAgICAgY2hlY2tlZCA/IEFuYWx5dGljcy5lbmFibGUoKSA6IEFuYWx5dGljcy5kaXNhYmxlKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkV4cG9ydEUyZUtleXNDbGlja2VkID0gKCkgPT4ge1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2dBc3luYygnRXhwb3J0IEUyRSBLZXlzJywgJycsXHJcbiAgICAgICAgICAgIGltcG9ydCgnLi4vLi4vLi4vLi4vLi4vYXN5bmMtY29tcG9uZW50cy92aWV3cy9kaWFsb2dzL0V4cG9ydEUyZUtleXNEaWFsb2cnKSxcclxuICAgICAgICAgICAge21hdHJpeENsaWVudDogTWF0cml4Q2xpZW50UGVnLmdldCgpfSxcclxuICAgICAgICApO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25JbXBvcnRFMmVLZXlzQ2xpY2tlZCA9ICgpID0+IHtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMoJ0ltcG9ydCBFMkUgS2V5cycsICcnLFxyXG4gICAgICAgICAgICBpbXBvcnQoJy4uLy4uLy4uLy4uLy4uL2FzeW5jLWNvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9JbXBvcnRFMmVLZXlzRGlhbG9nJyksXHJcbiAgICAgICAgICAgIHttYXRyaXhDbGllbnQ6IE1hdHJpeENsaWVudFBlZy5nZXQoKX0sXHJcbiAgICAgICAgKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uVXNlclVuaWdub3JlZCA9IGFzeW5jICh1c2VySWQpID0+IHtcclxuICAgICAgICAvLyBEb24ndCB1c2UgdGhpcy5zdGF0ZSB0byBnZXQgdGhlIGlnbm9yZWQgdXNlciBsaXN0IGFzIGl0IG1pZ2h0IGJlXHJcbiAgICAgICAgLy8gZXZlciBzbyBzbGlnaHRseSBvdXRkYXRlZC4gSW5zdGVhZCwgcHJlZmVyIHRvIGdldCBhIGZyZXNoIGxpc3QgYW5kXHJcbiAgICAgICAgLy8gdXBkYXRlIHRoYXQuXHJcbiAgICAgICAgY29uc3QgaWdub3JlZFVzZXJzID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElnbm9yZWRVc2VycygpO1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gaWdub3JlZFVzZXJzLmluZGV4T2YodXNlcklkKTtcclxuICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgIGlnbm9yZWRVc2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0SWdub3JlZFVzZXJzKGlnbm9yZWRVc2Vycyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2lnbm9yZWRVc2Vyc30pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfZ2V0SW52aXRlZFJvb21zID0gKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbXMoKS5maWx0ZXIoKHIpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHIuaGFzTWVtYmVyc2hpcFN0YXRlKE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSwgXCJpbnZpdGVcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9tYW5hZ2VJbnZpdGVzID0gYXN5bmMgKGFjY2VwdCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBtYW5hZ2luZ0ludml0ZXM6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIENvbXBpbGUgYXJyYXkgb2YgaW52aXRhdGlvbiByb29tIGlkc1xyXG4gICAgICAgIGNvbnN0IGludml0ZWRSb29tSWRzID0gdGhpcy5fZ2V0SW52aXRlZFJvb21zKCkubWFwKChyb29tKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiByb29tLnJvb21JZDtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gRXhlY3V0ZSBhbGwgYWNjZXB0YW5jZXMvcmVqZWN0aW9ucyBzZXF1ZW50aWFsbHlcclxuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3QgYWN0aW9uID0gYWNjZXB0ID8gY2xpLmpvaW5Sb29tLmJpbmQoY2xpKSA6IGNsaS5sZWF2ZS5iaW5kKGNsaSk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpbnZpdGVkUm9vbUlkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCByb29tSWQgPSBpbnZpdGVkUm9vbUlkc1tpXTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjY2VwdC9yZWplY3QgaW52aXRlXHJcbiAgICAgICAgICAgIGF3YWl0IGFjdGlvbihyb29tSWQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gTm8gZXJyb3IsIHVwZGF0ZSBpbnZpdGVkIHJvb21zIGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aW52aXRlZFJvb21BbXQ6IHNlbGYuc3RhdGUuaW52aXRlZFJvb21BbXQgLSAxfSk7XHJcbiAgICAgICAgICAgIH0sIGFzeW5jIChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBBY3Rpb24gZmFpbHVyZVxyXG4gICAgICAgICAgICAgICAgaWYgKGUuZXJyY29kZSA9PT0gXCJNX0xJTUlUX0VYQ0VFREVEXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBBZGQgYSBkZWxheSBiZXR3ZWVuIGVhY2ggaW52aXRlIGNoYW5nZSBpbiBvcmRlciB0byBhdm9pZCByYXRlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbGltaXRpbmcgYnkgdGhlIHNlcnZlci5cclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCBzbGVlcChlLnJldHJ5X2FmdGVyX21zIHx8IDI1MDApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBSZWRvIGxhc3QgYWN0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgaS0tO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBQcmludCBvdXQgZXJyb3Igd2l0aCBqb2luaW5nL2xlYXZpbmcgcm9vbVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbWFuYWdpbmdJbnZpdGVzOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQWNjZXB0QWxsSW52aXRlc0NsaWNrZWQgPSAoZXYpID0+IHtcclxuICAgICAgICB0aGlzLl9tYW5hZ2VJbnZpdGVzKHRydWUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25SZWplY3RBbGxJbnZpdGVzQ2xpY2tlZCA9IChldikgPT4ge1xyXG4gICAgICAgIHRoaXMuX21hbmFnZUludml0ZXMoZmFsc2UpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcmVuZGVyQ3VycmVudERldmljZUluZm8oKSB7XHJcbiAgICAgICAgY29uc3QgU2V0dGluZ3NGbGFnID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuU2V0dGluZ3NGbGFnJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCBkZXZpY2VJZCA9IGNsaWVudC5kZXZpY2VJZDtcclxuICAgICAgICBsZXQgaWRlbnRpdHlLZXkgPSBjbGllbnQuZ2V0RGV2aWNlRWQyNTUxOUtleSgpO1xyXG4gICAgICAgIGlmICghaWRlbnRpdHlLZXkpIHtcclxuICAgICAgICAgICAgaWRlbnRpdHlLZXkgPSBfdChcIjxub3Qgc3VwcG9ydGVkPlwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZGVudGl0eUtleSA9IEZvcm1hdHRpbmdVdGlscy5mb3JtYXRDcnlwdG9LZXkoaWRlbnRpdHlLZXkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGltcG9ydEV4cG9ydEJ1dHRvbnMgPSBudWxsO1xyXG4gICAgICAgIGlmIChjbGllbnQuaXNDcnlwdG9FbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgaW1wb3J0RXhwb3J0QnV0dG9ucyA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZWN1cml0eVVzZXJTZXR0aW5nc1RhYl9pbXBvcnRFeHBvcnRCdXR0b25zJz5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBraW5kPSdwcmltYXJ5JyBvbkNsaWNrPXt0aGlzLl9vbkV4cG9ydEUyZUtleXNDbGlja2VkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiRXhwb3J0IEUyRSByb29tIGtleXNcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9J3ByaW1hcnknIG9uQ2xpY2s9e3RoaXMuX29uSW1wb3J0RTJlS2V5c0NsaWNrZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJJbXBvcnQgRTJFIHJvb20ga2V5c1wiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uJz5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZyc+e190KFwiQ3J5cHRvZ3JhcGh5XCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0IG14X1NlY3VyaXR5VXNlclNldHRpbmdzVGFiX2RldmljZUluZm8nPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPntfdChcIlNlc3Npb24gSUQ6XCIpfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPjxjb2RlPntkZXZpY2VJZH08L2NvZGU+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+e190KFwiU2Vzc2lvbiBrZXk6XCIpfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPjxjb2RlPjxiPntpZGVudGl0eUtleX08L2I+PC9jb2RlPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIHtpbXBvcnRFeHBvcnRCdXR0b25zfVxyXG4gICAgICAgICAgICAgICAgPFNldHRpbmdzRmxhZyBuYW1lPSdibGFja2xpc3RVbnZlcmlmaWVkRGV2aWNlcycgbGV2ZWw9e1NldHRpbmdMZXZlbC5ERVZJQ0V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl91cGRhdGVCbGFja2xpc3REZXZpY2VzRmxhZ30gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVySWdub3JlZFVzZXJzKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5pZ25vcmVkVXNlcklkcyB8fCB0aGlzLnN0YXRlLmlnbm9yZWRVc2VySWRzLmxlbmd0aCA9PT0gMCkgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIGNvbnN0IHVzZXJJZHMgPSB0aGlzLnN0YXRlLmlnbm9yZWRVc2VySWRzXHJcbiAgICAgICAgICAgIC5tYXAoKHUpID0+IDxJZ25vcmVkVXNlciB1c2VySWQ9e3V9IG9uVW5pZ25vcmVkPXt0aGlzLl9vblVzZXJVbmlnbm9yZWR9IGtleT17dX0gLz4pO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc2VjdGlvbic+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdCgnSWdub3JlZCB1c2VycycpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAge3VzZXJJZHN9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyTWFuYWdlSW52aXRlcygpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pbnZpdGVkUm9vbUFtdCA9PT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGludml0ZWRSb29tcyA9IHRoaXMuX2dldEludml0ZWRSb29tcygpO1xyXG4gICAgICAgIGNvbnN0IElubGluZVNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5JbmxpbmVTcGlubmVyJyk7XHJcbiAgICAgICAgY29uc3Qgb25DbGlja0FjY2VwdCA9IHRoaXMuX29uQWNjZXB0QWxsSW52aXRlc0NsaWNrZWQuYmluZCh0aGlzLCBpbnZpdGVkUm9vbXMpO1xyXG4gICAgICAgIGNvbnN0IG9uQ2xpY2tSZWplY3QgPSB0aGlzLl9vblJlamVjdEFsbEludml0ZXNDbGlja2VkLmJpbmQodGhpcywgaW52aXRlZFJvb21zKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc2VjdGlvbiBteF9TZWN1cml0eVVzZXJTZXR0aW5nc1RhYl9idWxrT3B0aW9ucyc+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdCgnQnVsayBvcHRpb25zJyl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17b25DbGlja0FjY2VwdH0ga2luZD0ncHJpbWFyeScgZGlzYWJsZWQ9e3RoaXMuc3RhdGUubWFuYWdpbmdJbnZpdGVzfT5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJBY2NlcHQgYWxsICUoaW52aXRlZFJvb21zKXMgaW52aXRlc1wiLCB7aW52aXRlZFJvb21zOiB0aGlzLnN0YXRlLmludml0ZWRSb29tQW10fSl9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtvbkNsaWNrUmVqZWN0fSBraW5kPSdkYW5nZXInIGRpc2FibGVkPXt0aGlzLnN0YXRlLm1hbmFnaW5nSW52aXRlc30+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiUmVqZWN0IGFsbCAlKGludml0ZWRSb29tcylzIGludml0ZXNcIiwge2ludml0ZWRSb29tczogdGhpcy5zdGF0ZS5pbnZpdGVkUm9vbUFtdH0pfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAge3RoaXMuc3RhdGUubWFuYWdpbmdJbnZpdGVzID8gPElubGluZVNwaW5uZXIgLz4gOiA8ZGl2IC8+fVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBEZXZpY2VzUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5zZXR0aW5ncy5EZXZpY2VzUGFuZWwnKTtcclxuICAgICAgICBjb25zdCBTZXR0aW5nc0ZsYWcgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5TZXR0aW5nc0ZsYWcnKTtcclxuICAgICAgICBjb25zdCBFdmVudEluZGV4UGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5zZXR0aW5ncy5FdmVudEluZGV4UGFuZWwnKTtcclxuXHJcbiAgICAgICAgY29uc3QgS2V5QmFja3VwUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5zZXR0aW5ncy5LZXlCYWNrdXBQYW5lbCcpO1xyXG4gICAgICAgIGNvbnN0IGtleUJhY2t1cCA9IChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24nPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZ1wiPntfdChcIktleSBiYWNrdXBcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICA8S2V5QmFja3VwUGFuZWwgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBsZXQgZXZlbnRJbmRleDtcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9ldmVudF9pbmRleGluZ1wiKSkge1xyXG4gICAgICAgICAgICBldmVudEluZGV4ID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZ1wiPntfdChcIk1lc3NhZ2Ugc2VhcmNoXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8RXZlbnRJbmRleFBhbmVsIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFhYWDogVGhlcmUncyBubyBzdWNoIHBhbmVsIGluIHRoZSBjdXJyZW50IGNyb3NzLXNpZ25pbmcgZGVzaWducywgYnV0XHJcbiAgICAgICAgLy8gaXQncyB1c2VmdWwgdG8gaGF2ZSBmb3IgdGVzdGluZyB0aGUgZmVhdHVyZS4gSWYgdGhlcmUncyBubyBpbnRlcmVzdFxyXG4gICAgICAgIC8vIGluIGhhdmluZyBhZHZhbmNlZCBkZXRhaWxzIGhlcmUgb25jZSBhbGwgZmxvd3MgYXJlIGltcGxlbWVudGVkLCB3ZVxyXG4gICAgICAgIC8vIGNhbiByZW1vdmUgdGhpcy5cclxuICAgICAgICBjb25zdCBDcm9zc1NpZ25pbmdQYW5lbCA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLnNldHRpbmdzLkNyb3NzU2lnbmluZ1BhbmVsJyk7XHJcbiAgICAgICAgbGV0IGNyb3NzU2lnbmluZztcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgICAgIGNyb3NzU2lnbmluZyA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uJz5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiQ3Jvc3Mtc2lnbmluZ1wiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPENyb3NzU2lnbmluZ1BhbmVsIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IEUyZUFkdmFuY2VkUGFuZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5zZXR0aW5ncy5FMmVBZHZhbmNlZFBhbmVsJyk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWIgbXhfU2VjdXJpdHlVc2VyU2V0dGluZ3NUYWJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfaGVhZGluZ1wiPntfdChcIlNlY3VyaXR5ICYgUHJpdmFjeVwiKX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJTZXNzaW9uc1wiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQSBzZXNzaW9uJ3MgcHVibGljIG5hbWUgaXMgdmlzaWJsZSB0byBwZW9wbGUgeW91IGNvbW11bmljYXRlIHdpdGhcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxEZXZpY2VzUGFuZWwgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAge2tleUJhY2t1cH1cclxuICAgICAgICAgICAgICAgIHtldmVudEluZGV4fVxyXG4gICAgICAgICAgICAgICAge2Nyb3NzU2lnbmluZ31cclxuICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJDdXJyZW50RGV2aWNlSW5mbygpfVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24nPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJBbmFseXRpY3NcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlJpb3QgY29sbGVjdHMgYW5vbnltb3VzIGFuYWx5dGljcyB0byBhbGxvdyB1cyB0byBpbXByb3ZlIHRoZSBhcHBsaWNhdGlvbi5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJQcml2YWN5IGlzIGltcG9ydGFudCB0byB1cywgc28gd2UgZG9uJ3QgY29sbGVjdCBhbnkgcGVyc29uYWwgb3IgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZGVudGlmaWFibGUgZGF0YSBmb3Igb3VyIGFuYWx5dGljcy5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX2xpbmtCdG5cIiBvbkNsaWNrPXtBbmFseXRpY3Muc2hvd0RldGFpbHNNb2RhbH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJMZWFybiBtb3JlIGFib3V0IGhvdyB3ZSB1c2UgYW5hbHl0aWNzLlwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxTZXR0aW5nc0ZsYWcgbmFtZT0nYW5hbHl0aWNzT3B0SW4nIGxldmVsPXtTZXR0aW5nTGV2ZWwuREVWSUNFfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX3VwZGF0ZUFuYWx5dGljc30gLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlcklnbm9yZWRVc2VycygpfVxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlck1hbmFnZUludml0ZXMoKX1cclxuICAgICAgICAgICAgICAgIDxFMmVBZHZhbmNlZFBhbmVsIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19