"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../../languageHandler");

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var _AccessibleButton = _interopRequireDefault(require("../../../elements/AccessibleButton"));

var _SdkConfig = _interopRequireDefault(require("../../../../../SdkConfig"));

var _createRoom = _interopRequireDefault(require("../../../../../createRoom"));

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

var sdk = _interopRequireWildcard(require("../../../../../"));

var _PlatformPeg = _interopRequireDefault(require("../../../../../PlatformPeg"));

var KeyboardShortcuts = _interopRequireWildcard(require("../../../../../accessibility/KeyboardShortcuts"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class HelpUserSettingsTab extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onClearCacheAndReload", e => {
      if (!_PlatformPeg.default.get()) return; // Dev note: please keep this log line, it's useful when troubleshooting a MatrixClient suddenly
      // stopping in the middle of the logs.

      console.log("Clear cache & reload clicked");

      _MatrixClientPeg.MatrixClientPeg.get().stopClient();

      _MatrixClientPeg.MatrixClientPeg.get().store.deleteAllData().then(() => {
        _PlatformPeg.default.get().reload();
      });
    });
    (0, _defineProperty2.default)(this, "_onBugReport", e => {
      const BugReportDialog = sdk.getComponent("dialogs.BugReportDialog");

      if (!BugReportDialog) {
        return;
      }

      _Modal.default.createTrackedDialog('Bug Report Dialog', '', BugReportDialog, {});
    });
    (0, _defineProperty2.default)(this, "_onStartBotChat", e => {
      this.props.closeSettingsFn();
      (0, _createRoom.default)({
        dmUserId: _SdkConfig.default.get().welcomeUserId,
        andView: true
      });
    });
    (0, _defineProperty2.default)(this, "_showSpoiler", event => {
      const target = event.target;
      target.innerHTML = target.getAttribute('data-spoiler');
      const range = document.createRange();
      range.selectNodeContents(target);
      const selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
    });
    this.state = {
      vectorVersion: null,
      canUpdate: false
    };
  }

  componentDidMount()
  /*: void*/
  {
    _PlatformPeg.default.get().getAppVersion().then(ver => this.setState({
      vectorVersion: ver
    })).catch(e => {
      console.error("Error getting vector version: ", e);
    });

    _PlatformPeg.default.get().canSelfUpdate().then(v => this.setState({
      canUpdate: v
    })).catch(e => {
      console.error("Error getting self updatability: ", e);
    });
  }

  _renderLegal() {
    const tocLinks = _SdkConfig.default.get().terms_and_conditions_links;

    if (!tocLinks) return null;
    const legalLinks = [];

    for (const tocEntry of _SdkConfig.default.get().terms_and_conditions_links) {
      legalLinks.push(_react.default.createElement("div", {
        key: tocEntry.url
      }, _react.default.createElement("a", {
        href: tocEntry.url,
        rel: "noreferrer noopener",
        target: "_blank"
      }, tocEntry.text)));
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_HelpUserSettingsTab_versions"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Legal")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, legalLinks));
  }

  _renderCredits() {
    // Note: This is not translated because it is legal text.
    // Also, &nbsp; is ugly but necessary.
    return _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Credits")), _react.default.createElement("ul", null, _react.default.createElement("li", null, "The ", _react.default.createElement("a", {
      href: "themes/riot/img/backgrounds/valley.jpg",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "default cover photo"), " is \xA9\xA0", _react.default.createElement("a", {
      href: "https://www.flickr.com/golan",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "Jes\xFAs Roncero"), ' ', "used under the terms of\xA0", _react.default.createElement("a", {
      href: "https://creativecommons.org/licenses/by-sa/4.0/",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "CC-BY-SA 4.0"), "."), _react.default.createElement("li", null, "The ", _react.default.createElement("a", {
      href: "https://github.com/matrix-org/twemoji-colr",
      rel: "noreferrer noopener",
      target: "_blank"
    }, " twemoji-colr"), " font is \xA9\xA0", _react.default.createElement("a", {
      href: "https://mozilla.org",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "Mozilla Foundation"), ' ', "used under the terms of\xA0", _react.default.createElement("a", {
      href: "http://www.apache.org/licenses/LICENSE-2.0",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "Apache 2.0"), "."), _react.default.createElement("li", null, "The ", _react.default.createElement("a", {
      href: "https://twemoji.twitter.com/",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "Twemoji"), " emoji art is \xA9\xA0", _react.default.createElement("a", {
      href: "https://twemoji.twitter.com/",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "Twitter, Inc and other contributors"), " used under the terms of\xA0", _react.default.createElement("a", {
      href: "https://creativecommons.org/licenses/by/4.0/",
      rel: "noreferrer noopener",
      target: "_blank"
    }, "CC-BY 4.0"), ".")));
  }

  render() {
    let faqText = (0, _languageHandler._t)('For help with using Riot, click <a>here</a>.', {}, {
      'a': sub => _react.default.createElement("a", {
        href: "https://about.riot.im/need-help/",
        rel: "noreferrer noopener",
        target: "_blank"
      }, sub)
    });

    if (_SdkConfig.default.get().welcomeUserId && (0, _languageHandler.getCurrentLanguage)().startsWith('en')) {
      faqText = _react.default.createElement("div", null, (0, _languageHandler._t)('For help with using Riot, click <a>here</a> or start a chat with our ' + 'bot using the button below.', {}, {
        'a': sub => _react.default.createElement("a", {
          href: "https://about.riot.im/need-help/",
          rel: "noreferrer noopener",
          target: "_blank"
        }, sub)
      }), _react.default.createElement("div", null, _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onStartBotChat,
        kind: "primary"
      }, (0, _languageHandler._t)("Chat with Riot Bot"))));
    }

    const vectorVersion = this.state.vectorVersion || 'unknown';

    let olmVersion = _MatrixClientPeg.MatrixClientPeg.get().olmVersion;

    olmVersion = olmVersion ? "".concat(olmVersion[0], ".").concat(olmVersion[1], ".").concat(olmVersion[2]) : '<not-enabled>';
    let updateButton = null;

    if (this.state.canUpdate) {
      const platform = _PlatformPeg.default.get();

      updateButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: platform.startUpdateCheck,
        kind: "primary"
      }, (0, _languageHandler._t)('Check for update'));
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_HelpUserSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Help & About")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)('Bug reporting')), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("If you've submitted a bug via GitHub, debug logs can help " + "us track down the problem. Debug logs contain application " + "usage data including your username, the IDs or aliases of " + "the rooms or groups you have visited and the usernames of " + "other users. They do not contain messages."), _react.default.createElement("div", {
      className: "mx_HelpUserSettingsTab_debugButton"
    }, _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onBugReport,
      kind: "primary"
    }, (0, _languageHandler._t)("Submit debug logs"))), _react.default.createElement("div", {
      className: "mx_HelpUserSettingsTab_debugButton"
    }, _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onClearCacheAndReload,
      kind: "danger"
    }, (0, _languageHandler._t)("Clear cache and reload"))), (0, _languageHandler._t)("To report a Matrix-related security issue, please read the Matrix.org " + "<a>Security Disclosure Policy</a>.", {}, {
      'a': sub => _react.default.createElement("a", {
        href: "https://matrix.org/security-disclosure-policy/",
        rel: "noreferrer noopener",
        target: "_blank"
      }, sub)
    }))), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("FAQ")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, faqText), _react.default.createElement(_AccessibleButton.default, {
      kind: "primary",
      onClick: KeyboardShortcuts.toggleDialog
    }, (0, _languageHandler._t)("Keyboard Shortcuts"))), _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_HelpUserSettingsTab_versions"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Versions")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("riot-web version:"), " ", vectorVersion, _react.default.createElement("br", null), (0, _languageHandler._t)("olm version:"), " ", olmVersion, _react.default.createElement("br", null), updateButton)), this._renderLegal(), this._renderCredits(), _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_HelpUserSettingsTab_versions"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Advanced")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("Homeserver is"), " ", _react.default.createElement("code", null, _MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl()), _react.default.createElement("br", null), (0, _languageHandler._t)("Identity Server is"), " ", _react.default.createElement("code", null, _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()), _react.default.createElement("br", null), (0, _languageHandler._t)("Access Token:") + ' ', _react.default.createElement(_AccessibleButton.default, {
      element: "span",
      onClick: this._showSpoiler,
      "data-spoiler": _MatrixClientPeg.MatrixClientPeg.get().getAccessToken()
    }, "<", (0, _languageHandler._t)("click to reveal"), ">"))));
  }

}

exports.default = HelpUserSettingsTab;
(0, _defineProperty2.default)(HelpUserSettingsTab, "propTypes", {
  closeSettingsFn: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9IZWxwVXNlclNldHRpbmdzVGFiLmpzIl0sIm5hbWVzIjpbIkhlbHBVc2VyU2V0dGluZ3NUYWIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZSIsIlBsYXRmb3JtUGVnIiwiZ2V0IiwiY29uc29sZSIsImxvZyIsIk1hdHJpeENsaWVudFBlZyIsInN0b3BDbGllbnQiLCJzdG9yZSIsImRlbGV0ZUFsbERhdGEiLCJ0aGVuIiwicmVsb2FkIiwiQnVnUmVwb3J0RGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwicHJvcHMiLCJjbG9zZVNldHRpbmdzRm4iLCJkbVVzZXJJZCIsIlNka0NvbmZpZyIsIndlbGNvbWVVc2VySWQiLCJhbmRWaWV3IiwiZXZlbnQiLCJ0YXJnZXQiLCJpbm5lckhUTUwiLCJnZXRBdHRyaWJ1dGUiLCJyYW5nZSIsImRvY3VtZW50IiwiY3JlYXRlUmFuZ2UiLCJzZWxlY3ROb2RlQ29udGVudHMiLCJzZWxlY3Rpb24iLCJ3aW5kb3ciLCJnZXRTZWxlY3Rpb24iLCJyZW1vdmVBbGxSYW5nZXMiLCJhZGRSYW5nZSIsInN0YXRlIiwidmVjdG9yVmVyc2lvbiIsImNhblVwZGF0ZSIsImNvbXBvbmVudERpZE1vdW50IiwiZ2V0QXBwVmVyc2lvbiIsInZlciIsInNldFN0YXRlIiwiY2F0Y2giLCJlcnJvciIsImNhblNlbGZVcGRhdGUiLCJ2IiwiX3JlbmRlckxlZ2FsIiwidG9jTGlua3MiLCJ0ZXJtc19hbmRfY29uZGl0aW9uc19saW5rcyIsImxlZ2FsTGlua3MiLCJ0b2NFbnRyeSIsInB1c2giLCJ1cmwiLCJ0ZXh0IiwiX3JlbmRlckNyZWRpdHMiLCJyZW5kZXIiLCJmYXFUZXh0Iiwic3ViIiwic3RhcnRzV2l0aCIsIl9vblN0YXJ0Qm90Q2hhdCIsIm9sbVZlcnNpb24iLCJ1cGRhdGVCdXR0b24iLCJwbGF0Zm9ybSIsInN0YXJ0VXBkYXRlQ2hlY2siLCJfb25CdWdSZXBvcnQiLCJfb25DbGVhckNhY2hlQW5kUmVsb2FkIiwiS2V5Ym9hcmRTaG9ydGN1dHMiLCJ0b2dnbGVEaWFsb2ciLCJnZXRIb21lc2VydmVyVXJsIiwiZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwiLCJfc2hvd1Nwb2lsZXIiLCJnZXRBY2Nlc3NUb2tlbiIsIlByb3BUeXBlcyIsImZ1bmMiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTFCQTs7Ozs7Ozs7Ozs7Ozs7O0FBNEJlLE1BQU1BLG1CQUFOLFNBQWtDQyxlQUFNQyxTQUF4QyxDQUFrRDtBQUs3REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSxrRUFrQllDLENBQUQsSUFBTztBQUM1QixVQUFJLENBQUNDLHFCQUFZQyxHQUFaLEVBQUwsRUFBd0IsT0FESSxDQUc1QjtBQUNBOztBQUNBQyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSw4QkFBWjs7QUFDQUMsdUNBQWdCSCxHQUFoQixHQUFzQkksVUFBdEI7O0FBQ0FELHVDQUFnQkgsR0FBaEIsR0FBc0JLLEtBQXRCLENBQTRCQyxhQUE1QixHQUE0Q0MsSUFBNUMsQ0FBaUQsTUFBTTtBQUNuRFIsNkJBQVlDLEdBQVosR0FBa0JRLE1BQWxCO0FBQ0gsT0FGRDtBQUdILEtBNUJhO0FBQUEsd0RBOEJFVixDQUFELElBQU87QUFDbEIsWUFBTVcsZUFBZSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIseUJBQWpCLENBQXhCOztBQUNBLFVBQUksQ0FBQ0YsZUFBTCxFQUFzQjtBQUNsQjtBQUNIOztBQUNERyxxQkFBTUMsbUJBQU4sQ0FBMEIsbUJBQTFCLEVBQStDLEVBQS9DLEVBQW1ESixlQUFuRCxFQUFvRSxFQUFwRTtBQUNILEtBcENhO0FBQUEsMkRBc0NLWCxDQUFELElBQU87QUFDckIsV0FBS2dCLEtBQUwsQ0FBV0MsZUFBWDtBQUNBLCtCQUFXO0FBQ1BDLFFBQUFBLFFBQVEsRUFBRUMsbUJBQVVqQixHQUFWLEdBQWdCa0IsYUFEbkI7QUFFUEMsUUFBQUEsT0FBTyxFQUFFO0FBRkYsT0FBWDtBQUlILEtBNUNhO0FBQUEsd0RBOENFQyxLQUFELElBQVc7QUFDdEIsWUFBTUMsTUFBTSxHQUFHRCxLQUFLLENBQUNDLE1BQXJCO0FBQ0FBLE1BQUFBLE1BQU0sQ0FBQ0MsU0FBUCxHQUFtQkQsTUFBTSxDQUFDRSxZQUFQLENBQW9CLGNBQXBCLENBQW5CO0FBRUEsWUFBTUMsS0FBSyxHQUFHQyxRQUFRLENBQUNDLFdBQVQsRUFBZDtBQUNBRixNQUFBQSxLQUFLLENBQUNHLGtCQUFOLENBQXlCTixNQUF6QjtBQUVBLFlBQU1PLFNBQVMsR0FBR0MsTUFBTSxDQUFDQyxZQUFQLEVBQWxCO0FBQ0FGLE1BQUFBLFNBQVMsQ0FBQ0csZUFBVjtBQUNBSCxNQUFBQSxTQUFTLENBQUNJLFFBQVYsQ0FBbUJSLEtBQW5CO0FBQ0gsS0F4RGE7QUFHVixTQUFLUyxLQUFMLEdBQWE7QUFDVEMsTUFBQUEsYUFBYSxFQUFFLElBRE47QUFFVEMsTUFBQUEsU0FBUyxFQUFFO0FBRkYsS0FBYjtBQUlIOztBQUVEQyxFQUFBQSxpQkFBaUI7QUFBQTtBQUFTO0FBQ3RCckMseUJBQVlDLEdBQVosR0FBa0JxQyxhQUFsQixHQUFrQzlCLElBQWxDLENBQXdDK0IsR0FBRCxJQUFTLEtBQUtDLFFBQUwsQ0FBYztBQUFDTCxNQUFBQSxhQUFhLEVBQUVJO0FBQWhCLEtBQWQsQ0FBaEQsRUFBcUZFLEtBQXJGLENBQTRGMUMsQ0FBRCxJQUFPO0FBQzlGRyxNQUFBQSxPQUFPLENBQUN3QyxLQUFSLENBQWMsZ0NBQWQsRUFBZ0QzQyxDQUFoRDtBQUNILEtBRkQ7O0FBR0FDLHlCQUFZQyxHQUFaLEdBQWtCMEMsYUFBbEIsR0FBa0NuQyxJQUFsQyxDQUF3Q29DLENBQUQsSUFBTyxLQUFLSixRQUFMLENBQWM7QUFBQ0osTUFBQUEsU0FBUyxFQUFFUTtBQUFaLEtBQWQsQ0FBOUMsRUFBNkVILEtBQTdFLENBQW9GMUMsQ0FBRCxJQUFPO0FBQ3RGRyxNQUFBQSxPQUFPLENBQUN3QyxLQUFSLENBQWMsbUNBQWQsRUFBbUQzQyxDQUFuRDtBQUNILEtBRkQ7QUFHSDs7QUEwQ0Q4QyxFQUFBQSxZQUFZLEdBQUc7QUFDWCxVQUFNQyxRQUFRLEdBQUc1QixtQkFBVWpCLEdBQVYsR0FBZ0I4QywwQkFBakM7O0FBQ0EsUUFBSSxDQUFDRCxRQUFMLEVBQWUsT0FBTyxJQUFQO0FBRWYsVUFBTUUsVUFBVSxHQUFHLEVBQW5COztBQUNBLFNBQUssTUFBTUMsUUFBWCxJQUF1Qi9CLG1CQUFVakIsR0FBVixHQUFnQjhDLDBCQUF2QyxFQUFtRTtBQUMvREMsTUFBQUEsVUFBVSxDQUFDRSxJQUFYLENBQWdCO0FBQUssUUFBQSxHQUFHLEVBQUVELFFBQVEsQ0FBQ0U7QUFBbkIsU0FDWjtBQUFHLFFBQUEsSUFBSSxFQUFFRixRQUFRLENBQUNFLEdBQWxCO0FBQXVCLFFBQUEsR0FBRyxFQUFDLHFCQUEzQjtBQUFpRCxRQUFBLE1BQU0sRUFBQztBQUF4RCxTQUFrRUYsUUFBUSxDQUFDRyxJQUEzRSxDQURZLENBQWhCO0FBR0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHlCQUFHLE9BQUgsQ0FBN0MsQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLSixVQURMLENBRkosQ0FESjtBQVFIOztBQUVESyxFQUFBQSxjQUFjLEdBQUc7QUFDYjtBQUNBO0FBQ0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHlCQUFHLFNBQUgsQ0FBN0MsQ0FESixFQUVJLHlDQUNJLGlEQUNRO0FBQUcsTUFBQSxJQUFJLEVBQUMsd0NBQVI7QUFBaUQsTUFBQSxHQUFHLEVBQUMscUJBQXJEO0FBQTJFLE1BQUEsTUFBTSxFQUFDO0FBQWxGLDZCQURSLGtCQUdJO0FBQUcsTUFBQSxJQUFJLEVBQUMsOEJBQVI7QUFBdUMsTUFBQSxHQUFHLEVBQUMscUJBQTNDO0FBQWlFLE1BQUEsTUFBTSxFQUFDO0FBQXhFLDBCQUhKLEVBR3VHLEdBSHZHLGlDQUtJO0FBQUcsTUFBQSxJQUFJLEVBQUMsaURBQVI7QUFBMEQsTUFBQSxHQUFHLEVBQUMscUJBQTlEO0FBQW9GLE1BQUEsTUFBTSxFQUFDO0FBQTNGLHNCQUxKLE1BREosRUFTSSxpREFDUTtBQUFHLE1BQUEsSUFBSSxFQUFDLDRDQUFSO0FBQXFELE1BQUEsR0FBRyxFQUFDLHFCQUF6RDtBQUNHLE1BQUEsTUFBTSxFQUFDO0FBRFYsdUJBRFIsdUJBR0k7QUFBRyxNQUFBLElBQUksRUFBQyxxQkFBUjtBQUE4QixNQUFBLEdBQUcsRUFBQyxxQkFBbEM7QUFBd0QsTUFBQSxNQUFNLEVBQUM7QUFBL0QsNEJBSEosRUFHbUcsR0FIbkcsaUNBS0k7QUFBRyxNQUFBLElBQUksRUFBQyw0Q0FBUjtBQUFxRCxNQUFBLEdBQUcsRUFBQyxxQkFBekQ7QUFBK0UsTUFBQSxNQUFNLEVBQUM7QUFBdEYsb0JBTEosTUFUSixFQWlCSSxpREFDUTtBQUFHLE1BQUEsSUFBSSxFQUFDLDhCQUFSO0FBQXVDLE1BQUEsR0FBRyxFQUFDLHFCQUEzQztBQUFpRSxNQUFBLE1BQU0sRUFBQztBQUF4RSxpQkFEUiw0QkFHSTtBQUFHLE1BQUEsSUFBSSxFQUFDLDhCQUFSO0FBQXVDLE1BQUEsR0FBRyxFQUFDLHFCQUEzQztBQUFpRSxNQUFBLE1BQU0sRUFBQztBQUF4RSw2Q0FISixrQ0FLSTtBQUFHLE1BQUEsSUFBSSxFQUFDLDhDQUFSO0FBQXVELE1BQUEsR0FBRyxFQUFDLHFCQUEzRDtBQUFpRixNQUFBLE1BQU0sRUFBQztBQUF4RixtQkFMSixNQWpCSixDQUZKLENBREo7QUErQkg7O0FBRURDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUlDLE9BQU8sR0FBRyx5QkFBRyw4Q0FBSCxFQUFtRCxFQUFuRCxFQUF1RDtBQUNqRSxXQUFNQyxHQUFELElBQ0Q7QUFBRyxRQUFBLElBQUksRUFBQyxrQ0FBUjtBQUEyQyxRQUFBLEdBQUcsRUFBQyxxQkFBL0M7QUFBcUUsUUFBQSxNQUFNLEVBQUM7QUFBNUUsU0FBc0ZBLEdBQXRGO0FBRjZELEtBQXZELENBQWQ7O0FBSUEsUUFBSXRDLG1CQUFVakIsR0FBVixHQUFnQmtCLGFBQWhCLElBQWlDLDJDQUFxQnNDLFVBQXJCLENBQWdDLElBQWhDLENBQXJDLEVBQTRFO0FBQ3hFRixNQUFBQSxPQUFPLEdBQ0gsMENBRVEseUJBQUcsMEVBQ0MsNkJBREosRUFDbUMsRUFEbkMsRUFDdUM7QUFDbkMsYUFBTUMsR0FBRCxJQUFTO0FBQUcsVUFBQSxJQUFJLEVBQUMsa0NBQVI7QUFBMkMsVUFBQSxHQUFHLEVBQUMscUJBQS9DO0FBQ0csVUFBQSxNQUFNLEVBQUM7QUFEVixXQUNvQkEsR0FEcEI7QUFEcUIsT0FEdkMsQ0FGUixFQVFJLDBDQUNJLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUtFLGVBQWhDO0FBQWlELFFBQUEsSUFBSSxFQUFDO0FBQXRELFNBQ0sseUJBQUcsb0JBQUgsQ0FETCxDQURKLENBUkosQ0FESjtBQWdCSDs7QUFFRCxVQUFNdkIsYUFBYSxHQUFHLEtBQUtELEtBQUwsQ0FBV0MsYUFBWCxJQUE0QixTQUFsRDs7QUFFQSxRQUFJd0IsVUFBVSxHQUFHdkQsaUNBQWdCSCxHQUFoQixHQUFzQjBELFVBQXZDOztBQUNBQSxJQUFBQSxVQUFVLEdBQUdBLFVBQVUsYUFBTUEsVUFBVSxDQUFDLENBQUQsQ0FBaEIsY0FBdUJBLFVBQVUsQ0FBQyxDQUFELENBQWpDLGNBQXdDQSxVQUFVLENBQUMsQ0FBRCxDQUFsRCxJQUEwRCxlQUFqRjtBQUVBLFFBQUlDLFlBQVksR0FBRyxJQUFuQjs7QUFDQSxRQUFJLEtBQUsxQixLQUFMLENBQVdFLFNBQWYsRUFBMEI7QUFDdEIsWUFBTXlCLFFBQVEsR0FBRzdELHFCQUFZQyxHQUFaLEVBQWpCOztBQUNBMkQsTUFBQUEsWUFBWSxHQUNSLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFQyxRQUFRLENBQUNDLGdCQUFwQztBQUFzRCxRQUFBLElBQUksRUFBQztBQUEzRCxTQUNLLHlCQUFHLGtCQUFILENBREwsQ0FESjtBQUtIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXlDLHlCQUFHLGNBQUgsQ0FBekMsQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsZUFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BRVEseUJBQUksK0RBQ0EsNERBREEsR0FFQSw0REFGQSxHQUdBLDREQUhBLEdBSUEsNENBSkosQ0FGUixFQVNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFLEtBQUtDLFlBQWhDO0FBQThDLE1BQUEsSUFBSSxFQUFDO0FBQW5ELE9BQ0sseUJBQUcsbUJBQUgsQ0FETCxDQURKLENBVEosRUFjSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRSxLQUFLQyxzQkFBaEM7QUFBd0QsTUFBQSxJQUFJLEVBQUM7QUFBN0QsT0FDSyx5QkFBRyx3QkFBSCxDQURMLENBREosQ0FkSixFQW9CUSx5QkFBSSwyRUFDQSxvQ0FESixFQUMwQyxFQUQxQyxFQUVJO0FBQ0ksV0FBTVIsR0FBRCxJQUNEO0FBQUcsUUFBQSxJQUFJLEVBQUMsZ0RBQVI7QUFDQSxRQUFBLEdBQUcsRUFBQyxxQkFESjtBQUMwQixRQUFBLE1BQU0sRUFBQztBQURqQyxTQUMyQ0EsR0FEM0M7QUFGUixLQUZKLENBcEJSLENBRkosQ0FGSixFQWtDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHlCQUFHLEtBQUgsQ0FBN0MsQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLRCxPQURMLENBRkosRUFLSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxNQUFBLE9BQU8sRUFBRVUsaUJBQWlCLENBQUNDO0FBQTVELE9BQ00seUJBQUcsb0JBQUgsQ0FETixDQUxKLENBbENKLEVBMkNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsVUFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0sseUJBQUcsbUJBQUgsQ0FETCxPQUMrQi9CLGFBRC9CLEVBQzZDLHdDQUQ3QyxFQUVLLHlCQUFHLGNBQUgsQ0FGTCxPQUUwQndCLFVBRjFCLEVBRXFDLHdDQUZyQyxFQUdLQyxZQUhMLENBRkosQ0EzQ0osRUFtREssS0FBS2YsWUFBTCxFQW5ETCxFQW9ESyxLQUFLUSxjQUFMLEVBcERMLEVBcURJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBNkMseUJBQUcsVUFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0sseUJBQUcsZUFBSCxDQURMLE9BQzBCLDJDQUFPakQsaUNBQWdCSCxHQUFoQixHQUFzQmtFLGdCQUF0QixFQUFQLENBRDFCLEVBQ2lGLHdDQURqRixFQUVLLHlCQUFHLG9CQUFILENBRkwsT0FFK0IsMkNBQU8vRCxpQ0FBZ0JILEdBQWhCLEdBQXNCbUUsb0JBQXRCLEVBQVAsQ0FGL0IsRUFFMEYsd0NBRjFGLEVBR0sseUJBQUcsZUFBSCxJQUFzQixHQUgzQixFQUlJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFDLE1BQTFCO0FBQWlDLE1BQUEsT0FBTyxFQUFFLEtBQUtDLFlBQS9DO0FBQ2tCLHNCQUFjakUsaUNBQWdCSCxHQUFoQixHQUFzQnFFLGNBQXRCO0FBRGhDLFlBRVUseUJBQUcsaUJBQUgsQ0FGVixNQUpKLENBRkosQ0FyREosQ0FESjtBQW9FSDs7QUFuTzREOzs7OEJBQTVDM0UsbUIsZUFDRTtBQUNmcUIsRUFBQUEsZUFBZSxFQUFFdUQsbUJBQVVDLElBQVYsQ0FBZUM7QUFEakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdCwgZ2V0Q3VycmVudExhbmd1YWdlfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi8uLi8uLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL1Nka0NvbmZpZ1wiO1xyXG5pbXBvcnQgY3JlYXRlUm9vbSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vY3JlYXRlUm9vbVwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vXCI7XHJcbmltcG9ydCBQbGF0Zm9ybVBlZyBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vUGxhdGZvcm1QZWdcIjtcclxuaW1wb3J0ICogYXMgS2V5Ym9hcmRTaG9ydGN1dHMgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL2FjY2Vzc2liaWxpdHkvS2V5Ym9hcmRTaG9ydGN1dHNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEhlbHBVc2VyU2V0dGluZ3NUYWIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBjbG9zZVNldHRpbmdzRm46IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHZlY3RvclZlcnNpb246IG51bGwsXHJcbiAgICAgICAgICAgIGNhblVwZGF0ZTogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpOiB2b2lkIHtcclxuICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRBcHBWZXJzaW9uKCkudGhlbigodmVyKSA9PiB0aGlzLnNldFN0YXRlKHt2ZWN0b3JWZXJzaW9uOiB2ZXJ9KSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIGdldHRpbmcgdmVjdG9yIHZlcnNpb246IFwiLCBlKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5jYW5TZWxmVXBkYXRlKCkudGhlbigodikgPT4gdGhpcy5zZXRTdGF0ZSh7Y2FuVXBkYXRlOiB2fSkpLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJFcnJvciBnZXR0aW5nIHNlbGYgdXBkYXRhYmlsaXR5OiBcIiwgZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ2xlYXJDYWNoZUFuZFJlbG9hZCA9IChlKSA9PiB7XHJcbiAgICAgICAgaWYgKCFQbGF0Zm9ybVBlZy5nZXQoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBEZXYgbm90ZTogcGxlYXNlIGtlZXAgdGhpcyBsb2cgbGluZSwgaXQncyB1c2VmdWwgd2hlbiB0cm91Ymxlc2hvb3RpbmcgYSBNYXRyaXhDbGllbnQgc3VkZGVubHlcclxuICAgICAgICAvLyBzdG9wcGluZyBpbiB0aGUgbWlkZGxlIG9mIHRoZSBsb2dzLlxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiQ2xlYXIgY2FjaGUgJiByZWxvYWQgY2xpY2tlZFwiKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc3RvcENsaWVudCgpO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zdG9yZS5kZWxldGVBbGxEYXRhKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIFBsYXRmb3JtUGVnLmdldCgpLnJlbG9hZCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25CdWdSZXBvcnQgPSAoZSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IEJ1Z1JlcG9ydERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkJ1Z1JlcG9ydERpYWxvZ1wiKTtcclxuICAgICAgICBpZiAoIUJ1Z1JlcG9ydERpYWxvZykge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0J1ZyBSZXBvcnQgRGlhbG9nJywgJycsIEJ1Z1JlcG9ydERpYWxvZywge30pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25TdGFydEJvdENoYXQgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMuY2xvc2VTZXR0aW5nc0ZuKCk7XHJcbiAgICAgICAgY3JlYXRlUm9vbSh7XHJcbiAgICAgICAgICAgIGRtVXNlcklkOiBTZGtDb25maWcuZ2V0KCkud2VsY29tZVVzZXJJZCxcclxuICAgICAgICAgICAgYW5kVmlldzogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Nob3dTcG9pbGVyID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0ID0gZXZlbnQudGFyZ2V0O1xyXG4gICAgICAgIHRhcmdldC5pbm5lckhUTUwgPSB0YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXNwb2lsZXInKTtcclxuXHJcbiAgICAgICAgY29uc3QgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xyXG4gICAgICAgIHJhbmdlLnNlbGVjdE5vZGVDb250ZW50cyh0YXJnZXQpO1xyXG5cclxuICAgICAgICBjb25zdCBzZWxlY3Rpb24gPSB3aW5kb3cuZ2V0U2VsZWN0aW9uKCk7XHJcbiAgICAgICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xyXG4gICAgICAgIHNlbGVjdGlvbi5hZGRSYW5nZShyYW5nZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9yZW5kZXJMZWdhbCgpIHtcclxuICAgICAgICBjb25zdCB0b2NMaW5rcyA9IFNka0NvbmZpZy5nZXQoKS50ZXJtc19hbmRfY29uZGl0aW9uc19saW5rcztcclxuICAgICAgICBpZiAoIXRvY0xpbmtzKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgbGVnYWxMaW5rcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgdG9jRW50cnkgb2YgU2RrQ29uZmlnLmdldCgpLnRlcm1zX2FuZF9jb25kaXRpb25zX2xpbmtzKSB7XHJcbiAgICAgICAgICAgIGxlZ2FsTGlua3MucHVzaCg8ZGl2IGtleT17dG9jRW50cnkudXJsfT5cclxuICAgICAgICAgICAgICAgIDxhIGhyZWY9e3RvY0VudHJ5LnVybH0gcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPnt0b2NFbnRyeS50ZXh0fTwvYT5cclxuICAgICAgICAgICAgPC9kaXY+KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uIG14X0hlbHBVc2VyU2V0dGluZ3NUYWJfdmVyc2lvbnMnPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57X3QoXCJMZWdhbFwiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgIHtsZWdhbExpbmtzfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlckNyZWRpdHMoKSB7XHJcbiAgICAgICAgLy8gTm90ZTogVGhpcyBpcyBub3QgdHJhbnNsYXRlZCBiZWNhdXNlIGl0IGlzIGxlZ2FsIHRleHQuXHJcbiAgICAgICAgLy8gQWxzbywgJm5ic3A7IGlzIHVnbHkgYnV0IG5lY2Vzc2FyeS5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc2VjdGlvbic+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdChcIkNyZWRpdHNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgVGhlIDxhIGhyZWY9XCJ0aGVtZXMvcmlvdC9pbWcvYmFja2dyb3VuZHMvdmFsbGV5LmpwZ1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIiB0YXJnZXQ9XCJfYmxhbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdCBjb3ZlciBwaG90bzwvYT4gaXMgwqkmbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vd3d3LmZsaWNrci5jb20vZ29sYW5cIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgdGFyZ2V0PVwiX2JsYW5rXCI+SmVzw7pzIFJvbmNlcm88L2E+eycgJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlZCB1bmRlciB0aGUgdGVybXMgb2YmbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9ieS1zYS80LjAvXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDQy1CWS1TQSA0LjA8L2E+LlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBUaGUgPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9tYXRyaXgtb3JnL3R3ZW1vamktY29sclwiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCI+IHR3ZW1vamktY29scjwvYT4gZm9udCBpcyDCqSZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9tb3ppbGxhLm9yZ1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIiB0YXJnZXQ9XCJfYmxhbmtcIj5Nb3ppbGxhIEZvdW5kYXRpb248L2E+eycgJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlZCB1bmRlciB0aGUgdGVybXMgb2YmbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFwiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIiB0YXJnZXQ9XCJfYmxhbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgQXBhY2hlIDIuMDwvYT4uXHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFRoZSA8YSBocmVmPVwiaHR0cHM6Ly90d2Vtb2ppLnR3aXR0ZXIuY29tL1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIiB0YXJnZXQ9XCJfYmxhbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgVHdlbW9qaTwvYT4gZW1vamkgYXJ0IGlzIMKpJm5ic3A7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJodHRwczovL3R3ZW1vamkudHdpdHRlci5jb20vXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPlR3aXR0ZXIsIEluYyBhbmQgb3RoZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udHJpYnV0b3JzPC9hPiB1c2VkIHVuZGVyIHRoZSB0ZXJtcyBvZiZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LzQuMC9cIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgdGFyZ2V0PVwiX2JsYW5rXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENDLUJZIDQuMDwvYT4uXHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBmYXFUZXh0ID0gX3QoJ0ZvciBoZWxwIHdpdGggdXNpbmcgUmlvdCwgY2xpY2sgPGE+aGVyZTwvYT4uJywge30sIHtcclxuICAgICAgICAgICAgJ2EnOiAoc3ViKSA9PlxyXG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vYWJvdXQucmlvdC5pbS9uZWVkLWhlbHAvXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHRhcmdldD1cIl9ibGFua1wiPntzdWJ9PC9hPixcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoU2RrQ29uZmlnLmdldCgpLndlbGNvbWVVc2VySWQgJiYgZ2V0Q3VycmVudExhbmd1YWdlKCkuc3RhcnRzV2l0aCgnZW4nKSkge1xyXG4gICAgICAgICAgICBmYXFUZXh0ID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90KCdGb3IgaGVscCB3aXRoIHVzaW5nIFJpb3QsIGNsaWNrIDxhPmhlcmU8L2E+IG9yIHN0YXJ0IGEgY2hhdCB3aXRoIG91ciAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib3QgdXNpbmcgdGhlIGJ1dHRvbiBiZWxvdy4nLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2EnOiAoc3ViKSA9PiA8YSBocmVmPVwiaHR0cHM6Ly9hYm91dC5yaW90LmltL25lZWQtaGVscC9cIiByZWw9J25vcmVmZXJyZXIgbm9vcGVuZXInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldD0nX2JsYW5rJz57c3VifTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uU3RhcnRCb3RDaGF0fSBraW5kPSdwcmltYXJ5Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkNoYXQgd2l0aCBSaW90IEJvdFwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB2ZWN0b3JWZXJzaW9uID0gdGhpcy5zdGF0ZS52ZWN0b3JWZXJzaW9uIHx8ICd1bmtub3duJztcclxuXHJcbiAgICAgICAgbGV0IG9sbVZlcnNpb24gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub2xtVmVyc2lvbjtcclxuICAgICAgICBvbG1WZXJzaW9uID0gb2xtVmVyc2lvbiA/IGAke29sbVZlcnNpb25bMF19LiR7b2xtVmVyc2lvblsxXX0uJHtvbG1WZXJzaW9uWzJdfWAgOiAnPG5vdC1lbmFibGVkPic7XHJcblxyXG4gICAgICAgIGxldCB1cGRhdGVCdXR0b24gPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNhblVwZGF0ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBwbGF0Zm9ybSA9IFBsYXRmb3JtUGVnLmdldCgpO1xyXG4gICAgICAgICAgICB1cGRhdGVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtwbGF0Zm9ybS5zdGFydFVwZGF0ZUNoZWNrfSBraW5kPSdwcmltYXJ5Jz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoJ0NoZWNrIGZvciB1cGRhdGUnKX1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWIgbXhfSGVscFVzZXJTZXR0aW5nc1RhYlwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9oZWFkaW5nXCI+e190KFwiSGVscCAmIEFib3V0XCIpfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57X3QoJ0J1ZyByZXBvcnRpbmcnKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3QoIFwiSWYgeW91J3ZlIHN1Ym1pdHRlZCBhIGJ1ZyB2aWEgR2l0SHViLCBkZWJ1ZyBsb2dzIGNhbiBoZWxwIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInVzIHRyYWNrIGRvd24gdGhlIHByb2JsZW0uIERlYnVnIGxvZ3MgY29udGFpbiBhcHBsaWNhdGlvbiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ1c2FnZSBkYXRhIGluY2x1ZGluZyB5b3VyIHVzZXJuYW1lLCB0aGUgSURzIG9yIGFsaWFzZXMgb2YgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGhlIHJvb21zIG9yIGdyb3VwcyB5b3UgaGF2ZSB2aXNpdGVkIGFuZCB0aGUgdXNlcm5hbWVzIG9mIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm90aGVyIHVzZXJzLiBUaGV5IGRvIG5vdCBjb250YWluIG1lc3NhZ2VzLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9IZWxwVXNlclNldHRpbmdzVGFiX2RlYnVnQnV0dG9uJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uQnVnUmVwb3J0fSBraW5kPSdwcmltYXJ5Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJTdWJtaXQgZGVidWcgbG9nc1wiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9IZWxwVXNlclNldHRpbmdzVGFiX2RlYnVnQnV0dG9uJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uQ2xlYXJDYWNoZUFuZFJlbG9hZH0ga2luZD0nZGFuZ2VyJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJDbGVhciBjYWNoZSBhbmQgcmVsb2FkXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3QoIFwiVG8gcmVwb3J0IGEgTWF0cml4LXJlbGF0ZWQgc2VjdXJpdHkgaXNzdWUsIHBsZWFzZSByZWFkIHRoZSBNYXRyaXgub3JnIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxhPlNlY3VyaXR5IERpc2Nsb3N1cmUgUG9saWN5PC9hPi5cIiwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYSc6IChzdWIpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9tYXRyaXgub3JnL3NlY3VyaXR5LWRpc2Nsb3N1cmUtcG9saWN5L1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgdGFyZ2V0PVwiX2JsYW5rXCI+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24nPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZyc+e190KFwiRkFRXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7ZmFxVGV4dH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBraW5kPVwicHJpbWFyeVwiIG9uQ2xpY2s9e0tleWJvYXJkU2hvcnRjdXRzLnRvZ2dsZURpYWxvZ30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJLZXlib2FyZCBTaG9ydGN1dHNcIikgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24gbXhfSGVscFVzZXJTZXR0aW5nc1RhYl92ZXJzaW9ucyc+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57X3QoXCJWZXJzaW9uc1wiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwicmlvdC13ZWIgdmVyc2lvbjpcIil9IHt2ZWN0b3JWZXJzaW9ufTxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJvbG0gdmVyc2lvbjpcIil9IHtvbG1WZXJzaW9ufTxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dXBkYXRlQnV0dG9ufVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyTGVnYWwoKX1cclxuICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJDcmVkaXRzKCl9XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc2VjdGlvbiBteF9IZWxwVXNlclNldHRpbmdzVGFiX3ZlcnNpb25zJz5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdChcIkFkdmFuY2VkXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJIb21lc2VydmVyIGlzXCIpfSA8Y29kZT57TWF0cml4Q2xpZW50UGVnLmdldCgpLmdldEhvbWVzZXJ2ZXJVcmwoKX08L2NvZGU+PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIklkZW50aXR5IFNlcnZlciBpc1wiKX0gPGNvZGU+e01hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRJZGVudGl0eVNlcnZlclVybCgpfTwvY29kZT48YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQWNjZXNzIFRva2VuOlwiKSArICcgJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gZWxlbWVudD1cInNwYW5cIiBvbkNsaWNrPXt0aGlzLl9zaG93U3BvaWxlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcG9pbGVyPXtNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0QWNjZXNzVG9rZW4oKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7eyBfdChcImNsaWNrIHRvIHJldmVhbFwiKSB9Jmd0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=