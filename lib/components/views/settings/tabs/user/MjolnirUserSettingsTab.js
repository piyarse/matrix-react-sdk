"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../../../languageHandler");

var _Mjolnir = require("../../../../../mjolnir/Mjolnir");

var _ListRule = require("../../../../../mjolnir/ListRule");

var _BanList = require("../../../../../mjolnir/BanList");

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../../index"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MjolnirUserSettingsTab extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onPersonalRuleChanged", e => {
      this.setState({
        newPersonalRule: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onNewListChanged", e => {
      this.setState({
        newList: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onAddPersonalRule", async e => {
      e.preventDefault();
      e.stopPropagation();
      let kind = _BanList.RULE_SERVER;

      if (this.state.newPersonalRule.startsWith("@")) {
        kind = _BanList.RULE_USER;
      }

      this.setState({
        busy: true
      });

      try {
        const list = await _Mjolnir.Mjolnir.sharedInstance().getOrCreatePersonalList();
        await list.banEntity(kind, this.state.newPersonalRule, (0, _languageHandler._t)("Ignored/Blocked"));
        this.setState({
          newPersonalRule: ""
        }); // this will also cause the new rule to be rendered
      } catch (e) {
        console.error(e);
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Failed to add Mjolnir rule', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error adding ignored user/server'),
          description: (0, _languageHandler._t)('Something went wrong. Please try again or view your console for hints.')
        });
      } finally {
        this.setState({
          busy: false
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onSubscribeList", async e => {
      e.preventDefault();
      e.stopPropagation();
      this.setState({
        busy: true
      });

      try {
        const room = await _MatrixClientPeg.MatrixClientPeg.get().joinRoom(this.state.newList);
        await _Mjolnir.Mjolnir.sharedInstance().subscribeToList(room.roomId);
        this.setState({
          newList: ""
        }); // this will also cause the new rule to be rendered
      } catch (e) {
        console.error(e);
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Failed to subscribe to Mjolnir list', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error subscribing to list'),
          description: (0, _languageHandler._t)('Please verify the room ID or alias and try again.')
        });
      } finally {
        this.setState({
          busy: false
        });
      }
    });
    this.state = {
      busy: false,
      newPersonalRule: "",
      newList: ""
    };
  }

  async _removePersonalRule(rule
  /*: ListRule*/
  ) {
    this.setState({
      busy: true
    });

    try {
      const list = _Mjolnir.Mjolnir.sharedInstance().getPersonalList();

      await list.unbanEntity(rule.kind, rule.entity);
    } catch (e) {
      console.error(e);
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to remove Mjolnir rule', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Error removing ignored user/server'),
        description: (0, _languageHandler._t)('Something went wrong. Please try again or view your console for hints.')
      });
    } finally {
      this.setState({
        busy: false
      });
    }
  }

  async _unsubscribeFromList(list
  /*: BanList*/
  ) {
    this.setState({
      busy: true
    });

    try {
      await _Mjolnir.Mjolnir.sharedInstance().unsubscribeFromList(list.roomId);
      await _MatrixClientPeg.MatrixClientPeg.get().leave(list.roomId);
    } catch (e) {
      console.error(e);
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to unsubscribe from Mjolnir list', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Error unsubscribing from list'),
        description: (0, _languageHandler._t)('Please try again or view your console for hints.')
      });
    } finally {
      this.setState({
        busy: false
      });
    }
  }

  _viewListRules(list
  /*: BanList*/
  ) {
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(list.roomId);

    const name = room ? room.name : list.roomId;

    const renderRules = (rules
    /*: ListRule[]*/
    ) => {
      if (rules.length === 0) return _react.default.createElement("i", null, (0, _languageHandler._t)("None"));
      const tiles = [];

      for (const rule of rules) {
        tiles.push(_react.default.createElement("li", {
          key: rule.kind + rule.entity
        }, _react.default.createElement("code", null, rule.entity)));
      }

      return _react.default.createElement("ul", null, tiles);
    };

    _Modal.default.createTrackedDialog('View Mjolnir list rules', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Ban list rules - %(roomName)s", {
        roomName: name
      }),
      description: _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)("Server rules")), renderRules(list.serverRules), _react.default.createElement("h3", null, (0, _languageHandler._t)("User rules")), renderRules(list.userRules)),
      button: (0, _languageHandler._t)("Close"),
      hasCancelButton: false
    });
  }

  _renderPersonalBanListRules() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');

    const list = _Mjolnir.Mjolnir.sharedInstance().getPersonalList();

    const rules = list ? [...list.userRules, ...list.serverRules] : [];
    if (!list || rules.length <= 0) return _react.default.createElement("i", null, (0, _languageHandler._t)("You have not ignored anyone."));
    const tiles = [];

    for (const rule of rules) {
      tiles.push(_react.default.createElement("li", {
        key: rule.entity,
        className: "mx_MjolnirUserSettingsTab_listItem"
      }, _react.default.createElement(AccessibleButton, {
        kind: "danger_sm",
        onClick: () => this._removePersonalRule(rule),
        disabled: this.state.busy
      }, (0, _languageHandler._t)("Remove")), "\xA0", _react.default.createElement("code", null, rule.entity)));
    }

    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You are currently ignoring:")), _react.default.createElement("ul", null, tiles));
  }

  _renderSubscribedBanLists() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');

    const personalList = _Mjolnir.Mjolnir.sharedInstance().getPersonalList();

    const lists = _Mjolnir.Mjolnir.sharedInstance().lists.filter(b => {
      return personalList ? personalList.roomId !== b.roomId : true;
    });

    if (!lists || lists.length <= 0) return _react.default.createElement("i", null, (0, _languageHandler._t)("You are not subscribed to any lists"));
    const tiles = [];

    for (const list of lists) {
      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(list.roomId);

      const name = room ? _react.default.createElement("span", null, room.name, " (", _react.default.createElement("code", null, list.roomId), ")") : _react.default.createElement("code", null, "list.roomId");
      tiles.push(_react.default.createElement("li", {
        key: list.roomId,
        className: "mx_MjolnirUserSettingsTab_listItem"
      }, _react.default.createElement(AccessibleButton, {
        kind: "danger_sm",
        onClick: () => this._unsubscribeFromList(list),
        disabled: this.state.busy
      }, (0, _languageHandler._t)("Unsubscribe")), "\xA0", _react.default.createElement(AccessibleButton, {
        kind: "primary_sm",
        onClick: () => this._viewListRules(list),
        disabled: this.state.busy
      }, (0, _languageHandler._t)("View rules")), "\xA0", name));
    }

    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You are currently subscribed to:")), _react.default.createElement("ul", null, tiles));
  }

  render() {
    const Field = sdk.getComponent('elements.Field');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_MjolnirUserSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Ignored users")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, _react.default.createElement("span", {
      className: "warning"
    }, (0, _languageHandler._t)("⚠ These settings are meant for advanced users.")), _react.default.createElement("br", null), _react.default.createElement("br", null), (0, _languageHandler._t)("Add users and servers you want to ignore here. Use asterisks " + "to have Riot match any characters. For example, <code>@bot:*</code> " + "would ignore all users that have the name 'bot' on any server.", {}, {
      code: s => _react.default.createElement("code", null, s)
    }), _react.default.createElement("br", null), _react.default.createElement("br", null), (0, _languageHandler._t)("Ignoring people is done through ban lists which contain rules for " + "who to ban. Subscribing to a ban list means the users/servers blocked by " + "that list will be hidden from you."))), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Personal ban list")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)("Your personal ban list holds all the users/servers you personally don't " + "want to see messages from. After ignoring your first user/server, a new room " + "will show up in your room list named 'My Ban List' - stay in this room to keep " + "the ban list in effect.")), _react.default.createElement("div", null, this._renderPersonalBanListRules()), _react.default.createElement("div", null, _react.default.createElement("form", {
      onSubmit: this._onAddPersonalRule,
      autoComplete: "off"
    }, _react.default.createElement(Field, {
      type: "text",
      label: (0, _languageHandler._t)("Server or user ID to ignore"),
      placeholder: (0, _languageHandler._t)("eg: @bot:* or example.org"),
      value: this.state.newPersonalRule,
      onChange: this._onPersonalRuleChanged
    }), _react.default.createElement(AccessibleButton, {
      type: "submit",
      kind: "primary",
      onClick: this._onAddPersonalRule,
      disabled: this.state.busy
    }, (0, _languageHandler._t)("Ignore"))))), _react.default.createElement("div", {
      className: "mx_SettingsTab_section"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Subscribed lists")), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, _react.default.createElement("span", {
      className: "warning"
    }, (0, _languageHandler._t)("Subscribing to a ban list will cause you to join it!")), "\xA0", _react.default.createElement("span", null, (0, _languageHandler._t)("If this isn't what you want, please use a different tool to ignore users."))), _react.default.createElement("div", null, this._renderSubscribedBanLists()), _react.default.createElement("div", null, _react.default.createElement("form", {
      onSubmit: this._onSubscribeList,
      autoComplete: "off"
    }, _react.default.createElement(Field, {
      type: "text",
      label: (0, _languageHandler._t)("Room ID or alias of ban list"),
      value: this.state.newList,
      onChange: this._onNewListChanged
    }), _react.default.createElement(AccessibleButton, {
      type: "submit",
      kind: "primary",
      onClick: this._onSubscribeList,
      disabled: this.state.busy
    }, (0, _languageHandler._t)("Subscribe"))))));
  }

}

exports.default = MjolnirUserSettingsTab;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvdXNlci9Nam9sbmlyVXNlclNldHRpbmdzVGFiLmpzIl0sIm5hbWVzIjpbIk1qb2xuaXJVc2VyU2V0dGluZ3NUYWIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZSIsInNldFN0YXRlIiwibmV3UGVyc29uYWxSdWxlIiwidGFyZ2V0IiwidmFsdWUiLCJuZXdMaXN0IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJraW5kIiwiUlVMRV9TRVJWRVIiLCJzdGF0ZSIsInN0YXJ0c1dpdGgiLCJSVUxFX1VTRVIiLCJidXN5IiwibGlzdCIsIk1qb2xuaXIiLCJzaGFyZWRJbnN0YW5jZSIsImdldE9yQ3JlYXRlUGVyc29uYWxMaXN0IiwiYmFuRW50aXR5IiwiY29uc29sZSIsImVycm9yIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwicm9vbSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImpvaW5Sb29tIiwic3Vic2NyaWJlVG9MaXN0Iiwicm9vbUlkIiwiX3JlbW92ZVBlcnNvbmFsUnVsZSIsInJ1bGUiLCJnZXRQZXJzb25hbExpc3QiLCJ1bmJhbkVudGl0eSIsImVudGl0eSIsIl91bnN1YnNjcmliZUZyb21MaXN0IiwidW5zdWJzY3JpYmVGcm9tTGlzdCIsImxlYXZlIiwiX3ZpZXdMaXN0UnVsZXMiLCJRdWVzdGlvbkRpYWxvZyIsImdldFJvb20iLCJuYW1lIiwicmVuZGVyUnVsZXMiLCJydWxlcyIsImxlbmd0aCIsInRpbGVzIiwicHVzaCIsInJvb21OYW1lIiwic2VydmVyUnVsZXMiLCJ1c2VyUnVsZXMiLCJidXR0b24iLCJoYXNDYW5jZWxCdXR0b24iLCJfcmVuZGVyUGVyc29uYWxCYW5MaXN0UnVsZXMiLCJBY2Nlc3NpYmxlQnV0dG9uIiwiX3JlbmRlclN1YnNjcmliZWRCYW5MaXN0cyIsInBlcnNvbmFsTGlzdCIsImxpc3RzIiwiZmlsdGVyIiwiYiIsInJlbmRlciIsIkZpZWxkIiwiY29kZSIsInMiLCJfb25BZGRQZXJzb25hbFJ1bGUiLCJfb25QZXJzb25hbFJ1bGVDaGFuZ2VkIiwiX29uU3Vic2NyaWJlTGlzdCIsIl9vbk5ld0xpc3RDaGFuZ2VkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7O0FBeUJlLE1BQU1BLHNCQUFOLFNBQXFDQyxlQUFNQyxTQUEzQyxDQUFxRDtBQUNoRUMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSxrRUFVWUMsQ0FBRCxJQUFPO0FBQzVCLFdBQUtDLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxlQUFlLEVBQUVGLENBQUMsQ0FBQ0csTUFBRixDQUFTQztBQUEzQixPQUFkO0FBQ0gsS0FaYTtBQUFBLDZEQWNPSixDQUFELElBQU87QUFDdkIsV0FBS0MsUUFBTCxDQUFjO0FBQUNJLFFBQUFBLE9BQU8sRUFBRUwsQ0FBQyxDQUFDRyxNQUFGLENBQVNDO0FBQW5CLE9BQWQ7QUFDSCxLQWhCYTtBQUFBLDhEQWtCTyxNQUFPSixDQUFQLElBQWE7QUFDOUJBLE1BQUFBLENBQUMsQ0FBQ00sY0FBRjtBQUNBTixNQUFBQSxDQUFDLENBQUNPLGVBQUY7QUFFQSxVQUFJQyxJQUFJLEdBQUdDLG9CQUFYOztBQUNBLFVBQUksS0FBS0MsS0FBTCxDQUFXUixlQUFYLENBQTJCUyxVQUEzQixDQUFzQyxHQUF0QyxDQUFKLEVBQWdEO0FBQzVDSCxRQUFBQSxJQUFJLEdBQUdJLGtCQUFQO0FBQ0g7O0FBRUQsV0FBS1gsUUFBTCxDQUFjO0FBQUNZLFFBQUFBLElBQUksRUFBRTtBQUFQLE9BQWQ7O0FBQ0EsVUFBSTtBQUNBLGNBQU1DLElBQUksR0FBRyxNQUFNQyxpQkFBUUMsY0FBUixHQUF5QkMsdUJBQXpCLEVBQW5CO0FBQ0EsY0FBTUgsSUFBSSxDQUFDSSxTQUFMLENBQWVWLElBQWYsRUFBcUIsS0FBS0UsS0FBTCxDQUFXUixlQUFoQyxFQUFpRCx5QkFBRyxpQkFBSCxDQUFqRCxDQUFOO0FBQ0EsYUFBS0QsUUFBTCxDQUFjO0FBQUNDLFVBQUFBLGVBQWUsRUFBRTtBQUFsQixTQUFkLEVBSEEsQ0FHc0M7QUFDekMsT0FKRCxDQUlFLE9BQU9GLENBQVAsRUFBVTtBQUNSbUIsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNwQixDQUFkO0FBRUEsY0FBTXFCLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsdUJBQU1DLG1CQUFOLENBQTBCLDRCQUExQixFQUF3RCxFQUF4RCxFQUE0REosV0FBNUQsRUFBeUU7QUFDckVLLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxrQ0FBSCxDQUQ4RDtBQUVyRUMsVUFBQUEsV0FBVyxFQUFFLHlCQUFHLHdFQUFIO0FBRndELFNBQXpFO0FBSUgsT0FaRCxTQVlVO0FBQ04sYUFBSzFCLFFBQUwsQ0FBYztBQUFDWSxVQUFBQSxJQUFJLEVBQUU7QUFBUCxTQUFkO0FBQ0g7QUFDSixLQTNDYTtBQUFBLDREQTZDSyxNQUFPYixDQUFQLElBQWE7QUFDNUJBLE1BQUFBLENBQUMsQ0FBQ00sY0FBRjtBQUNBTixNQUFBQSxDQUFDLENBQUNPLGVBQUY7QUFFQSxXQUFLTixRQUFMLENBQWM7QUFBQ1ksUUFBQUEsSUFBSSxFQUFFO0FBQVAsT0FBZDs7QUFDQSxVQUFJO0FBQ0EsY0FBTWUsSUFBSSxHQUFHLE1BQU1DLGlDQUFnQkMsR0FBaEIsR0FBc0JDLFFBQXRCLENBQStCLEtBQUtyQixLQUFMLENBQVdMLE9BQTFDLENBQW5CO0FBQ0EsY0FBTVUsaUJBQVFDLGNBQVIsR0FBeUJnQixlQUF6QixDQUF5Q0osSUFBSSxDQUFDSyxNQUE5QyxDQUFOO0FBQ0EsYUFBS2hDLFFBQUwsQ0FBYztBQUFDSSxVQUFBQSxPQUFPLEVBQUU7QUFBVixTQUFkLEVBSEEsQ0FHOEI7QUFDakMsT0FKRCxDQUlFLE9BQU9MLENBQVAsRUFBVTtBQUNSbUIsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNwQixDQUFkO0FBRUEsY0FBTXFCLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsdUJBQU1DLG1CQUFOLENBQTBCLHFDQUExQixFQUFpRSxFQUFqRSxFQUFxRUosV0FBckUsRUFBa0Y7QUFDOUVLLFVBQUFBLEtBQUssRUFBRSx5QkFBRywyQkFBSCxDQUR1RTtBQUU5RUMsVUFBQUEsV0FBVyxFQUFFLHlCQUFHLG1EQUFIO0FBRmlFLFNBQWxGO0FBSUgsT0FaRCxTQVlVO0FBQ04sYUFBSzFCLFFBQUwsQ0FBYztBQUFDWSxVQUFBQSxJQUFJLEVBQUU7QUFBUCxTQUFkO0FBQ0g7QUFDSixLQWpFYTtBQUdWLFNBQUtILEtBQUwsR0FBYTtBQUNURyxNQUFBQSxJQUFJLEVBQUUsS0FERztBQUVUWCxNQUFBQSxlQUFlLEVBQUUsRUFGUjtBQUdURyxNQUFBQSxPQUFPLEVBQUU7QUFIQSxLQUFiO0FBS0g7O0FBMkRELFFBQU02QixtQkFBTixDQUEwQkM7QUFBMUI7QUFBQSxJQUEwQztBQUN0QyxTQUFLbEMsUUFBTCxDQUFjO0FBQUNZLE1BQUFBLElBQUksRUFBRTtBQUFQLEtBQWQ7O0FBQ0EsUUFBSTtBQUNBLFlBQU1DLElBQUksR0FBR0MsaUJBQVFDLGNBQVIsR0FBeUJvQixlQUF6QixFQUFiOztBQUNBLFlBQU10QixJQUFJLENBQUN1QixXQUFMLENBQWlCRixJQUFJLENBQUMzQixJQUF0QixFQUE0QjJCLElBQUksQ0FBQ0csTUFBakMsQ0FBTjtBQUNILEtBSEQsQ0FHRSxPQUFPdEMsQ0FBUCxFQUFVO0FBQ1JtQixNQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3BCLENBQWQ7QUFFQSxZQUFNcUIsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsK0JBQTFCLEVBQTJELEVBQTNELEVBQStESixXQUEvRCxFQUE0RTtBQUN4RUssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLG9DQUFILENBRGlFO0FBRXhFQyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsd0VBQUg7QUFGMkQsT0FBNUU7QUFJSCxLQVhELFNBV1U7QUFDTixXQUFLMUIsUUFBTCxDQUFjO0FBQUNZLFFBQUFBLElBQUksRUFBRTtBQUFQLE9BQWQ7QUFDSDtBQUNKOztBQUVELFFBQU0wQixvQkFBTixDQUEyQnpCO0FBQTNCO0FBQUEsSUFBMEM7QUFDdEMsU0FBS2IsUUFBTCxDQUFjO0FBQUNZLE1BQUFBLElBQUksRUFBRTtBQUFQLEtBQWQ7O0FBQ0EsUUFBSTtBQUNBLFlBQU1FLGlCQUFRQyxjQUFSLEdBQXlCd0IsbUJBQXpCLENBQTZDMUIsSUFBSSxDQUFDbUIsTUFBbEQsQ0FBTjtBQUNBLFlBQU1KLGlDQUFnQkMsR0FBaEIsR0FBc0JXLEtBQXRCLENBQTRCM0IsSUFBSSxDQUFDbUIsTUFBakMsQ0FBTjtBQUNILEtBSEQsQ0FHRSxPQUFPakMsQ0FBUCxFQUFVO0FBQ1JtQixNQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3BCLENBQWQ7QUFFQSxZQUFNcUIsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIseUNBQTFCLEVBQXFFLEVBQXJFLEVBQXlFSixXQUF6RSxFQUFzRjtBQUNsRkssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLCtCQUFILENBRDJFO0FBRWxGQyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsa0RBQUg7QUFGcUUsT0FBdEY7QUFJSCxLQVhELFNBV1U7QUFDTixXQUFLMUIsUUFBTCxDQUFjO0FBQUNZLFFBQUFBLElBQUksRUFBRTtBQUFQLE9BQWQ7QUFDSDtBQUNKOztBQUVENkIsRUFBQUEsY0FBYyxDQUFDNUI7QUFBRDtBQUFBLElBQWdCO0FBQzFCLFVBQU02QixjQUFjLEdBQUdyQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCOztBQUVBLFVBQU1LLElBQUksR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQmMsT0FBdEIsQ0FBOEI5QixJQUFJLENBQUNtQixNQUFuQyxDQUFiOztBQUNBLFVBQU1ZLElBQUksR0FBR2pCLElBQUksR0FBR0EsSUFBSSxDQUFDaUIsSUFBUixHQUFlL0IsSUFBSSxDQUFDbUIsTUFBckM7O0FBRUEsVUFBTWEsV0FBVyxHQUFHLENBQUNDO0FBQUQ7QUFBQSxTQUF1QjtBQUN2QyxVQUFJQSxLQUFLLENBQUNDLE1BQU4sS0FBaUIsQ0FBckIsRUFBd0IsT0FBTyx3Q0FBSSx5QkFBRyxNQUFILENBQUosQ0FBUDtBQUV4QixZQUFNQyxLQUFLLEdBQUcsRUFBZDs7QUFDQSxXQUFLLE1BQU1kLElBQVgsSUFBbUJZLEtBQW5CLEVBQTBCO0FBQ3RCRSxRQUFBQSxLQUFLLENBQUNDLElBQU4sQ0FBVztBQUFJLFVBQUEsR0FBRyxFQUFFZixJQUFJLENBQUMzQixJQUFMLEdBQVkyQixJQUFJLENBQUNHO0FBQTFCLFdBQWtDLDJDQUFPSCxJQUFJLENBQUNHLE1BQVosQ0FBbEMsQ0FBWDtBQUNIOztBQUNELGFBQU8seUNBQUtXLEtBQUwsQ0FBUDtBQUNILEtBUkQ7O0FBVUF6QixtQkFBTUMsbUJBQU4sQ0FBMEIseUJBQTFCLEVBQXFELEVBQXJELEVBQXlEa0IsY0FBekQsRUFBeUU7QUFDckVqQixNQUFBQSxLQUFLLEVBQUUseUJBQUcsK0JBQUgsRUFBb0M7QUFBQ3lCLFFBQUFBLFFBQVEsRUFBRU47QUFBWCxPQUFwQyxDQUQ4RDtBQUVyRWxCLE1BQUFBLFdBQVcsRUFDUCwwQ0FDSSx5Q0FBSyx5QkFBRyxjQUFILENBQUwsQ0FESixFQUVLbUIsV0FBVyxDQUFDaEMsSUFBSSxDQUFDc0MsV0FBTixDQUZoQixFQUdJLHlDQUFLLHlCQUFHLFlBQUgsQ0FBTCxDQUhKLEVBSUtOLFdBQVcsQ0FBQ2hDLElBQUksQ0FBQ3VDLFNBQU4sQ0FKaEIsQ0FIaUU7QUFVckVDLE1BQUFBLE1BQU0sRUFBRSx5QkFBRyxPQUFILENBVjZEO0FBV3JFQyxNQUFBQSxlQUFlLEVBQUU7QUFYb0QsS0FBekU7QUFhSDs7QUFFREMsRUFBQUEsMkJBQTJCLEdBQUc7QUFDMUIsVUFBTUMsZ0JBQWdCLEdBQUduQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCOztBQUVBLFVBQU1ULElBQUksR0FBR0MsaUJBQVFDLGNBQVIsR0FBeUJvQixlQUF6QixFQUFiOztBQUNBLFVBQU1XLEtBQUssR0FBR2pDLElBQUksR0FBRyxDQUFDLEdBQUdBLElBQUksQ0FBQ3VDLFNBQVQsRUFBb0IsR0FBR3ZDLElBQUksQ0FBQ3NDLFdBQTVCLENBQUgsR0FBOEMsRUFBaEU7QUFDQSxRQUFJLENBQUN0QyxJQUFELElBQVNpQyxLQUFLLENBQUNDLE1BQU4sSUFBZ0IsQ0FBN0IsRUFBZ0MsT0FBTyx3Q0FBSSx5QkFBRyw4QkFBSCxDQUFKLENBQVA7QUFFaEMsVUFBTUMsS0FBSyxHQUFHLEVBQWQ7O0FBQ0EsU0FBSyxNQUFNZCxJQUFYLElBQW1CWSxLQUFuQixFQUEwQjtBQUN0QkUsTUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQ0k7QUFBSSxRQUFBLEdBQUcsRUFBRWYsSUFBSSxDQUFDRyxNQUFkO0FBQXNCLFFBQUEsU0FBUyxFQUFDO0FBQWhDLFNBQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxRQUFBLElBQUksRUFBQyxXQURUO0FBRUksUUFBQSxPQUFPLEVBQUUsTUFBTSxLQUFLSixtQkFBTCxDQUF5QkMsSUFBekIsQ0FGbkI7QUFHSSxRQUFBLFFBQVEsRUFBRSxLQUFLekIsS0FBTCxDQUFXRztBQUh6QixTQUtLLHlCQUFHLFFBQUgsQ0FMTCxDQURKLFVBUUksMkNBQU9zQixJQUFJLENBQUNHLE1BQVosQ0FSSixDQURKO0FBWUg7O0FBRUQsV0FDSSwwQ0FDSSx3Q0FBSSx5QkFBRyw2QkFBSCxDQUFKLENBREosRUFFSSx5Q0FBS1csS0FBTCxDQUZKLENBREo7QUFNSDs7QUFFRFMsRUFBQUEseUJBQXlCLEdBQUc7QUFDeEIsVUFBTUQsZ0JBQWdCLEdBQUduQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCOztBQUVBLFVBQU1vQyxZQUFZLEdBQUc1QyxpQkFBUUMsY0FBUixHQUF5Qm9CLGVBQXpCLEVBQXJCOztBQUNBLFVBQU13QixLQUFLLEdBQUc3QyxpQkFBUUMsY0FBUixHQUF5QjRDLEtBQXpCLENBQStCQyxNQUEvQixDQUFzQ0MsQ0FBQyxJQUFJO0FBQ3JELGFBQU9ILFlBQVksR0FBRUEsWUFBWSxDQUFDMUIsTUFBYixLQUF3QjZCLENBQUMsQ0FBQzdCLE1BQTVCLEdBQXFDLElBQXhEO0FBQ0gsS0FGYSxDQUFkOztBQUdBLFFBQUksQ0FBQzJCLEtBQUQsSUFBVUEsS0FBSyxDQUFDWixNQUFOLElBQWdCLENBQTlCLEVBQWlDLE9BQU8sd0NBQUkseUJBQUcscUNBQUgsQ0FBSixDQUFQO0FBRWpDLFVBQU1DLEtBQUssR0FBRyxFQUFkOztBQUNBLFNBQUssTUFBTW5DLElBQVgsSUFBbUI4QyxLQUFuQixFQUEwQjtBQUN0QixZQUFNaEMsSUFBSSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCYyxPQUF0QixDQUE4QjlCLElBQUksQ0FBQ21CLE1BQW5DLENBQWI7O0FBQ0EsWUFBTVksSUFBSSxHQUFHakIsSUFBSSxHQUFHLDJDQUFPQSxJQUFJLENBQUNpQixJQUFaLFFBQW1CLDJDQUFPL0IsSUFBSSxDQUFDbUIsTUFBWixDQUFuQixNQUFILEdBQTJELHlEQUE1RTtBQUNBZ0IsTUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQ0k7QUFBSSxRQUFBLEdBQUcsRUFBRXBDLElBQUksQ0FBQ21CLE1BQWQ7QUFBc0IsUUFBQSxTQUFTLEVBQUM7QUFBaEMsU0FDSSw2QkFBQyxnQkFBRDtBQUNJLFFBQUEsSUFBSSxFQUFDLFdBRFQ7QUFFSSxRQUFBLE9BQU8sRUFBRSxNQUFNLEtBQUtNLG9CQUFMLENBQTBCekIsSUFBMUIsQ0FGbkI7QUFHSSxRQUFBLFFBQVEsRUFBRSxLQUFLSixLQUFMLENBQVdHO0FBSHpCLFNBS0sseUJBQUcsYUFBSCxDQUxMLENBREosVUFRSSw2QkFBQyxnQkFBRDtBQUNJLFFBQUEsSUFBSSxFQUFDLFlBRFQ7QUFFSSxRQUFBLE9BQU8sRUFBRSxNQUFNLEtBQUs2QixjQUFMLENBQW9CNUIsSUFBcEIsQ0FGbkI7QUFHSSxRQUFBLFFBQVEsRUFBRSxLQUFLSixLQUFMLENBQVdHO0FBSHpCLFNBS0sseUJBQUcsWUFBSCxDQUxMLENBUkosVUFlS2dDLElBZkwsQ0FESjtBQW1CSDs7QUFFRCxXQUNJLDBDQUNJLHdDQUFJLHlCQUFHLGtDQUFILENBQUosQ0FESixFQUVJLHlDQUFLSSxLQUFMLENBRkosQ0FESjtBQU1IOztBQUVEYyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxLQUFLLEdBQUcxQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsZ0JBQWpCLENBQWQ7QUFDQSxVQUFNa0MsZ0JBQWdCLEdBQUduQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBRUEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBeUMseUJBQUcsZUFBSCxDQUF6QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUEyQix5QkFBRyxnREFBSCxDQUEzQixDQURKLEVBQzJGLHdDQUQzRixFQUVJLHdDQUZKLEVBR0sseUJBQ0csa0VBQ0Esc0VBREEsR0FFQSxnRUFISCxFQUlHLEVBSkgsRUFJTztBQUFDMEMsTUFBQUEsSUFBSSxFQUFHQyxDQUFELElBQU8sMkNBQU9BLENBQVA7QUFBZCxLQUpQLENBSEwsRUFRTSx3Q0FSTixFQVNJLHdDQVRKLEVBVUsseUJBQ0csdUVBQ0EsMkVBREEsR0FFQSxvQ0FISCxDQVZMLENBREosQ0FGSixFQW9CSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHlCQUFHLG1CQUFILENBQTdDLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyx5QkFDRyw2RUFDQSwrRUFEQSxHQUVBLGlGQUZBLEdBR0EseUJBSkgsQ0FETCxDQUZKLEVBVUksMENBQ0ssS0FBS1YsMkJBQUwsRUFETCxDQVZKLEVBYUksMENBQ0k7QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLVyxrQkFBckI7QUFBeUMsTUFBQSxZQUFZLEVBQUM7QUFBdEQsT0FDSSw2QkFBQyxLQUFEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsTUFEVDtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLDZCQUFILENBRlg7QUFHSSxNQUFBLFdBQVcsRUFBRSx5QkFBRywyQkFBSCxDQUhqQjtBQUlJLE1BQUEsS0FBSyxFQUFFLEtBQUt6RCxLQUFMLENBQVdSLGVBSnRCO0FBS0ksTUFBQSxRQUFRLEVBQUUsS0FBS2tFO0FBTG5CLE1BREosRUFRSSw2QkFBQyxnQkFBRDtBQUNJLE1BQUEsSUFBSSxFQUFDLFFBRFQ7QUFFSSxNQUFBLElBQUksRUFBQyxTQUZUO0FBR0ksTUFBQSxPQUFPLEVBQUUsS0FBS0Qsa0JBSGxCO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS3pELEtBQUwsQ0FBV0c7QUFKekIsT0FNSyx5QkFBRyxRQUFILENBTkwsQ0FSSixDQURKLENBYkosQ0FwQkosRUFxREk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxrQkFBSCxDQUE3QyxDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUEyQix5QkFBRyxzREFBSCxDQUEzQixDQURKLFVBR0ksMkNBQU8seUJBQ0gsMkVBREcsQ0FBUCxDQUhKLENBRkosRUFTSSwwQ0FDSyxLQUFLNkMseUJBQUwsRUFETCxDQVRKLEVBWUksMENBQ0k7QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLVyxnQkFBckI7QUFBdUMsTUFBQSxZQUFZLEVBQUM7QUFBcEQsT0FDSSw2QkFBQyxLQUFEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsTUFEVDtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLDhCQUFILENBRlg7QUFHSSxNQUFBLEtBQUssRUFBRSxLQUFLM0QsS0FBTCxDQUFXTCxPQUh0QjtBQUlJLE1BQUEsUUFBUSxFQUFFLEtBQUtpRTtBQUpuQixNQURKLEVBT0ksNkJBQUMsZ0JBQUQ7QUFDSSxNQUFBLElBQUksRUFBQyxRQURUO0FBRUksTUFBQSxJQUFJLEVBQUMsU0FGVDtBQUdJLE1BQUEsT0FBTyxFQUFFLEtBQUtELGdCQUhsQjtBQUlJLE1BQUEsUUFBUSxFQUFFLEtBQUszRCxLQUFMLENBQVdHO0FBSnpCLE9BTUsseUJBQUcsV0FBSCxDQU5MLENBUEosQ0FESixDQVpKLENBckRKLENBREo7QUF1Rkg7O0FBM1MrRCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge01qb2xuaXJ9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9tam9sbmlyL01qb2xuaXJcIjtcclxuaW1wb3J0IHtMaXN0UnVsZX0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL21qb2xuaXIvTGlzdFJ1bGVcIjtcclxuaW1wb3J0IHtCYW5MaXN0LCBSVUxFX1NFUlZFUiwgUlVMRV9VU0VSfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbWpvbG5pci9CYW5MaXN0XCI7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTW9kYWxcIjtcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9pbmRleFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWpvbG5pclVzZXJTZXR0aW5nc1RhYiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgbmV3UGVyc29uYWxSdWxlOiBcIlwiLFxyXG4gICAgICAgICAgICBuZXdMaXN0OiBcIlwiLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX29uUGVyc29uYWxSdWxlQ2hhbmdlZCA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bmV3UGVyc29uYWxSdWxlOiBlLnRhcmdldC52YWx1ZX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25OZXdMaXN0Q2hhbmdlZCA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bmV3TGlzdDogZS50YXJnZXQudmFsdWV9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQWRkUGVyc29uYWxSdWxlID0gYXN5bmMgKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgbGV0IGtpbmQgPSBSVUxFX1NFUlZFUjtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5uZXdQZXJzb25hbFJ1bGUuc3RhcnRzV2l0aChcIkBcIikpIHtcclxuICAgICAgICAgICAga2luZCA9IFJVTEVfVVNFUjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IHRydWV9KTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBsaXN0ID0gYXdhaXQgTWpvbG5pci5zaGFyZWRJbnN0YW5jZSgpLmdldE9yQ3JlYXRlUGVyc29uYWxMaXN0KCk7XHJcbiAgICAgICAgICAgIGF3YWl0IGxpc3QuYmFuRW50aXR5KGtpbmQsIHRoaXMuc3RhdGUubmV3UGVyc29uYWxSdWxlLCBfdChcIklnbm9yZWQvQmxvY2tlZFwiKSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe25ld1BlcnNvbmFsUnVsZTogXCJcIn0pOyAvLyB0aGlzIHdpbGwgYWxzbyBjYXVzZSB0aGUgbmV3IHJ1bGUgdG8gYmUgcmVuZGVyZWRcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gYWRkIE1qb2xuaXIgcnVsZScsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvciBhZGRpbmcgaWdub3JlZCB1c2VyL3NlcnZlcicpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdTb21ldGhpbmcgd2VudCB3cm9uZy4gUGxlYXNlIHRyeSBhZ2FpbiBvciB2aWV3IHlvdXIgY29uc29sZSBmb3IgaGludHMuJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IGZhbHNlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfb25TdWJzY3JpYmVMaXN0ID0gYXN5bmMgKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogdHJ1ZX0pO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb20gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuam9pblJvb20odGhpcy5zdGF0ZS5uZXdMaXN0KTtcclxuICAgICAgICAgICAgYXdhaXQgTWpvbG5pci5zaGFyZWRJbnN0YW5jZSgpLnN1YnNjcmliZVRvTGlzdChyb29tLnJvb21JZCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe25ld0xpc3Q6IFwiXCJ9KTsgLy8gdGhpcyB3aWxsIGFsc28gY2F1c2UgdGhlIG5ldyBydWxlIHRvIGJlIHJlbmRlcmVkXHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIHN1YnNjcmliZSB0byBNam9sbmlyIGxpc3QnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXJyb3Igc3Vic2NyaWJpbmcgdG8gbGlzdCcpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdQbGVhc2UgdmVyaWZ5IHRoZSByb29tIElEIG9yIGFsaWFzIGFuZCB0cnkgYWdhaW4uJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IGZhbHNlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBhc3luYyBfcmVtb3ZlUGVyc29uYWxSdWxlKHJ1bGU6IExpc3RSdWxlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogdHJ1ZX0pO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxpc3QgPSBNam9sbmlyLnNoYXJlZEluc3RhbmNlKCkuZ2V0UGVyc29uYWxMaXN0KCk7XHJcbiAgICAgICAgICAgIGF3YWl0IGxpc3QudW5iYW5FbnRpdHkocnVsZS5raW5kLCBydWxlLmVudGl0eSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIHJlbW92ZSBNam9sbmlyIHJ1bGUnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXJyb3IgcmVtb3ZpbmcgaWdub3JlZCB1c2VyL3NlcnZlcicpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdTb21ldGhpbmcgd2VudCB3cm9uZy4gUGxlYXNlIHRyeSBhZ2FpbiBvciB2aWV3IHlvdXIgY29uc29sZSBmb3IgaGludHMuJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IGZhbHNlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF91bnN1YnNjcmliZUZyb21MaXN0KGxpc3Q6IEJhbkxpc3QpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtidXN5OiB0cnVlfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgTWpvbG5pci5zaGFyZWRJbnN0YW5jZSgpLnVuc3Vic2NyaWJlRnJvbUxpc3QobGlzdC5yb29tSWQpO1xyXG4gICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubGVhdmUobGlzdC5yb29tSWQpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byB1bnN1YnNjcmliZSBmcm9tIE1qb2xuaXIgbGlzdCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvciB1bnN1YnNjcmliaW5nIGZyb20gbGlzdCcpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdQbGVhc2UgdHJ5IGFnYWluIG9yIHZpZXcgeW91ciBjb25zb2xlIGZvciBoaW50cy4nKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBmaW5hbGx5IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3ZpZXdMaXN0UnVsZXMobGlzdDogQmFuTGlzdCkge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShsaXN0LnJvb21JZCk7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9IHJvb20gPyByb29tLm5hbWUgOiBsaXN0LnJvb21JZDtcclxuXHJcbiAgICAgICAgY29uc3QgcmVuZGVyUnVsZXMgPSAocnVsZXM6IExpc3RSdWxlW10pID0+IHtcclxuICAgICAgICAgICAgaWYgKHJ1bGVzLmxlbmd0aCA9PT0gMCkgcmV0dXJuIDxpPntfdChcIk5vbmVcIil9PC9pPjtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHRpbGVzID0gW107XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcnVsZSBvZiBydWxlcykge1xyXG4gICAgICAgICAgICAgICAgdGlsZXMucHVzaCg8bGkga2V5PXtydWxlLmtpbmQgKyBydWxlLmVudGl0eX0+PGNvZGU+e3J1bGUuZW50aXR5fTwvY29kZT48L2xpPik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIDx1bD57dGlsZXN9PC91bD47XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVmlldyBNam9sbmlyIGxpc3QgcnVsZXMnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KFwiQmFuIGxpc3QgcnVsZXMgLSAlKHJvb21OYW1lKXNcIiwge3Jvb21OYW1lOiBuYW1lfSksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxoMz57X3QoXCJTZXJ2ZXIgcnVsZXNcIil9PC9oMz5cclxuICAgICAgICAgICAgICAgICAgICB7cmVuZGVyUnVsZXMobGlzdC5zZXJ2ZXJSdWxlcyl9XHJcbiAgICAgICAgICAgICAgICAgICAgPGgzPntfdChcIlVzZXIgcnVsZXNcIil9PC9oMz5cclxuICAgICAgICAgICAgICAgICAgICB7cmVuZGVyUnVsZXMobGlzdC51c2VyUnVsZXMpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJDbG9zZVwiKSxcclxuICAgICAgICAgICAgaGFzQ2FuY2VsQnV0dG9uOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyUGVyc29uYWxCYW5MaXN0UnVsZXMoKSB7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgY29uc3QgbGlzdCA9IE1qb2xuaXIuc2hhcmVkSW5zdGFuY2UoKS5nZXRQZXJzb25hbExpc3QoKTtcclxuICAgICAgICBjb25zdCBydWxlcyA9IGxpc3QgPyBbLi4ubGlzdC51c2VyUnVsZXMsIC4uLmxpc3Quc2VydmVyUnVsZXNdIDogW107XHJcbiAgICAgICAgaWYgKCFsaXN0IHx8IHJ1bGVzLmxlbmd0aCA8PSAwKSByZXR1cm4gPGk+e190KFwiWW91IGhhdmUgbm90IGlnbm9yZWQgYW55b25lLlwiKX08L2k+O1xyXG5cclxuICAgICAgICBjb25zdCB0aWxlcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgcnVsZSBvZiBydWxlcykge1xyXG4gICAgICAgICAgICB0aWxlcy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgPGxpIGtleT17cnVsZS5lbnRpdHl9IGNsYXNzTmFtZT1cIm14X01qb2xuaXJVc2VyU2V0dGluZ3NUYWJfbGlzdEl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBraW5kPVwiZGFuZ2VyX3NtXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5fcmVtb3ZlUGVyc29uYWxSdWxlKHJ1bGUpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5idXN5fVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiUmVtb3ZlXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj4mbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICA8Y29kZT57cnVsZS5lbnRpdHl9PC9jb2RlPlxyXG4gICAgICAgICAgICAgICAgPC9saT4sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFwiWW91IGFyZSBjdXJyZW50bHkgaWdub3Jpbmc6XCIpfTwvcD5cclxuICAgICAgICAgICAgICAgIDx1bD57dGlsZXN9PC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyU3Vic2NyaWJlZEJhbkxpc3RzKCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHBlcnNvbmFsTGlzdCA9IE1qb2xuaXIuc2hhcmVkSW5zdGFuY2UoKS5nZXRQZXJzb25hbExpc3QoKTtcclxuICAgICAgICBjb25zdCBsaXN0cyA9IE1qb2xuaXIuc2hhcmVkSW5zdGFuY2UoKS5saXN0cy5maWx0ZXIoYiA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBwZXJzb25hbExpc3Q/IHBlcnNvbmFsTGlzdC5yb29tSWQgIT09IGIucm9vbUlkIDogdHJ1ZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoIWxpc3RzIHx8IGxpc3RzLmxlbmd0aCA8PSAwKSByZXR1cm4gPGk+e190KFwiWW91IGFyZSBub3Qgc3Vic2NyaWJlZCB0byBhbnkgbGlzdHNcIil9PC9pPjtcclxuXHJcbiAgICAgICAgY29uc3QgdGlsZXMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGxpc3Qgb2YgbGlzdHMpIHtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKGxpc3Qucm9vbUlkKTtcclxuICAgICAgICAgICAgY29uc3QgbmFtZSA9IHJvb20gPyA8c3Bhbj57cm9vbS5uYW1lfSAoPGNvZGU+e2xpc3Qucm9vbUlkfTwvY29kZT4pPC9zcGFuPiA6IDxjb2RlPmxpc3Qucm9vbUlkPC9jb2RlPjtcclxuICAgICAgICAgICAgdGlsZXMucHVzaChcclxuICAgICAgICAgICAgICAgIDxsaSBrZXk9e2xpc3Qucm9vbUlkfSBjbGFzc05hbWU9XCJteF9Nam9sbmlyVXNlclNldHRpbmdzVGFiX2xpc3RJdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAga2luZD1cImRhbmdlcl9zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuX3Vuc3Vic2NyaWJlRnJvbUxpc3QobGlzdCl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmJ1c3l9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJVbnN1YnNjcmliZVwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+Jm5ic3A7XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAga2luZD1cInByaW1hcnlfc21cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLl92aWV3TGlzdFJ1bGVzKGxpc3QpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5idXN5fVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiVmlldyBydWxlc1wiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+Jm5ic3A7XHJcbiAgICAgICAgICAgICAgICAgICAge25hbWV9XHJcbiAgICAgICAgICAgICAgICA8L2xpPixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXCJZb3UgYXJlIGN1cnJlbnRseSBzdWJzY3JpYmVkIHRvOlwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8dWw+e3RpbGVzfTwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiIG14X01qb2xuaXJVc2VyU2V0dGluZ3NUYWJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfaGVhZGluZ1wiPntfdChcIklnbm9yZWQgdXNlcnNcIil9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3NlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J3dhcm5pbmcnPntfdChcIuKaoCBUaGVzZSBzZXR0aW5ncyBhcmUgbWVhbnQgZm9yIGFkdmFuY2VkIHVzZXJzLlwiKX08L3NwYW4+PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkFkZCB1c2VycyBhbmQgc2VydmVycyB5b3Ugd2FudCB0byBpZ25vcmUgaGVyZS4gVXNlIGFzdGVyaXNrcyBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRvIGhhdmUgUmlvdCBtYXRjaCBhbnkgY2hhcmFjdGVycy4gRm9yIGV4YW1wbGUsIDxjb2RlPkBib3Q6KjwvY29kZT4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3b3VsZCBpZ25vcmUgYWxsIHVzZXJzIHRoYXQgaGF2ZSB0aGUgbmFtZSAnYm90JyBvbiBhbnkgc2VydmVyLlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge30sIHtjb2RlOiAocykgPT4gPGNvZGU+e3N9PC9jb2RlPn0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICl9PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIklnbm9yaW5nIHBlb3BsZSBpcyBkb25lIHRocm91Z2ggYmFuIGxpc3RzIHdoaWNoIGNvbnRhaW4gcnVsZXMgZm9yIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwid2hvIHRvIGJhbi4gU3Vic2NyaWJpbmcgdG8gYSBiYW4gbGlzdCBtZWFucyB0aGUgdXNlcnMvc2VydmVycyBibG9ja2VkIGJ5IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGhhdCBsaXN0IHdpbGwgYmUgaGlkZGVuIGZyb20geW91LlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3NlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nXCI+e190KFwiUGVyc29uYWwgYmFuIGxpc3RcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiWW91ciBwZXJzb25hbCBiYW4gbGlzdCBob2xkcyBhbGwgdGhlIHVzZXJzL3NlcnZlcnMgeW91IHBlcnNvbmFsbHkgZG9uJ3QgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3YW50IHRvIHNlZSBtZXNzYWdlcyBmcm9tLiBBZnRlciBpZ25vcmluZyB5b3VyIGZpcnN0IHVzZXIvc2VydmVyLCBhIG5ldyByb29tIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwid2lsbCBzaG93IHVwIGluIHlvdXIgcm9vbSBsaXN0IG5hbWVkICdNeSBCYW4gTGlzdCcgLSBzdGF5IGluIHRoaXMgcm9vbSB0byBrZWVwIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGhlIGJhbiBsaXN0IGluIGVmZmVjdC5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyUGVyc29uYWxCYW5MaXN0UnVsZXMoKX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25BZGRQZXJzb25hbFJ1bGV9IGF1dG9Db21wbGV0ZT1cIm9mZlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdChcIlNlcnZlciBvciB1c2VyIElEIHRvIGlnbm9yZVwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17X3QoXCJlZzogQGJvdDoqIG9yIGV4YW1wbGUub3JnXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm5ld1BlcnNvbmFsUnVsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25QZXJzb25hbFJ1bGVDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2luZD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQWRkUGVyc29uYWxSdWxlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmJ1c3l9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge190KFwiSWdub3JlXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJTdWJzY3JpYmVkIGxpc3RzXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J3dhcm5pbmcnPntfdChcIlN1YnNjcmliaW5nIHRvIGEgYmFuIGxpc3Qgd2lsbCBjYXVzZSB5b3UgdG8gam9pbiBpdCFcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmbmJzcDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJJZiB0aGlzIGlzbid0IHdoYXQgeW91IHdhbnQsIHBsZWFzZSB1c2UgYSBkaWZmZXJlbnQgdG9vbCB0byBpZ25vcmUgdXNlcnMuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJTdWJzY3JpYmVkQmFuTGlzdHMoKX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25TdWJzY3JpYmVMaXN0fSBhdXRvQ29tcGxldGU9XCJvZmZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJSb29tIElEIG9yIGFsaWFzIG9mIGJhbiBsaXN0XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm5ld0xpc3R9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uTmV3TGlzdENoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBraW5kPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25TdWJzY3JpYmVMaXN0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmJ1c3l9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge190KFwiU3Vic2NyaWJlXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=