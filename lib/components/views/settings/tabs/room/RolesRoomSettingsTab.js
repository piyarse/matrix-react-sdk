"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.BannedUser = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../../languageHandler");

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../.."));

var _AccessibleButton = _interopRequireDefault(require("../../../elements/AccessibleButton"));

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const plEventsToLabels = {
  // These will be translated for us later.
  "m.room.avatar": (0, _languageHandler._td)("Change room avatar"),
  "m.room.name": (0, _languageHandler._td)("Change room name"),
  "m.room.canonical_alias": (0, _languageHandler._td)("Change main address for the room"),
  "m.room.history_visibility": (0, _languageHandler._td)("Change history visibility"),
  "m.room.power_levels": (0, _languageHandler._td)("Change permissions"),
  "m.room.topic": (0, _languageHandler._td)("Change topic"),
  "m.room.tombstone": (0, _languageHandler._td)("Upgrade the room"),
  "m.room.encryption": (0, _languageHandler._td)("Enable room encryption"),
  "im.vector.modular.widgets": (0, _languageHandler._td)("Modify widgets")
};
const plEventsToShow = {
  // If an event is listed here, it will be shown in the PL settings. Defaults will be calculated.
  "m.room.avatar": {
    isState: true
  },
  "m.room.name": {
    isState: true
  },
  "m.room.canonical_alias": {
    isState: true
  },
  "m.room.history_visibility": {
    isState: true
  },
  "m.room.power_levels": {
    isState: true
  },
  "m.room.topic": {
    isState: true
  },
  "m.room.tombstone": {
    isState: true
  },
  "m.room.encryption": {
    isState: true
  },
  "im.vector.modular.widgets": {
    isState: true
  }
}; // parse a string as an integer; if the input is undefined, or cannot be parsed
// as an integer, return a default.

function parseIntWithDefault(val, def) {
  const res = parseInt(val);
  return isNaN(res) ? def : res;
}

class BannedUser extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onUnbanClick", e => {
      _MatrixClientPeg.MatrixClientPeg.get().unban(this.props.member.roomId, this.props.member.userId).catch(err => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to unban: " + err);

        _Modal.default.createTrackedDialog('Failed to unban', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error'),
          description: (0, _languageHandler._t)('Failed to unban')
        });
      });
    });
  }

  render() {
    let unbanButton;

    if (this.props.canUnban) {
      unbanButton = _react.default.createElement(_AccessibleButton.default, {
        kind: "danger_sm",
        onClick: this._onUnbanClick,
        className: "mx_RolesRoomSettingsTab_unbanBtn"
      }, (0, _languageHandler._t)('Unban'));
    }

    const userId = this.props.member.name === this.props.member.userId ? null : this.props.member.userId;
    return _react.default.createElement("li", null, unbanButton, _react.default.createElement("span", {
      title: (0, _languageHandler._t)("Banned by %(displayName)s", {
        displayName: this.props.by
      })
    }, _react.default.createElement("strong", null, this.props.member.name), " ", userId, this.props.reason ? " " + (0, _languageHandler._t)('Reason') + ": " + this.props.reason : ""));
  }

}

exports.BannedUser = BannedUser;
(0, _defineProperty2.default)(BannedUser, "propTypes", {
  canUnban: _propTypes.default.bool,
  member: _propTypes.default.object.isRequired,
  // js-sdk RoomMember
  by: _propTypes.default.string.isRequired,
  reason: _propTypes.default.string
});

class RolesRoomSettingsTab extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onRoomMembership", (event, state, member) => {
      if (state.roomId !== this.props.roomId) return;
      this.forceUpdate();
    });
    (0, _defineProperty2.default)(this, "_onPowerLevelsChanged", (value, powerLevelKey) => {
      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const room = client.getRoom(this.props.roomId);
      const plEvent = room.currentState.getStateEvents('m.room.power_levels', '');
      let plContent = plEvent ? plEvent.getContent() || {} : {}; // Clone the power levels just in case

      plContent = Object.assign({}, plContent);
      const eventsLevelPrefix = "event_levels_";
      value = parseInt(value);

      if (powerLevelKey.startsWith(eventsLevelPrefix)) {
        // deep copy "events" object, Object.assign itself won't deep copy
        plContent["events"] = Object.assign({}, plContent["events"] || {});
        plContent["events"][powerLevelKey.slice(eventsLevelPrefix.length)] = value;
      } else {
        const keyPath = powerLevelKey.split('.');
        let parentObj;
        let currentObj = plContent;

        for (const key of keyPath) {
          if (!currentObj[key]) {
            currentObj[key] = {};
          }

          parentObj = currentObj;
          currentObj = currentObj[key];
        }

        parentObj[keyPath[keyPath.length - 1]] = value;
      }

      client.sendStateEvent(this.props.roomId, "m.room.power_levels", plContent).catch(e => {
        console.error(e);
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Power level requirement change failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error changing power level requirement'),
          description: (0, _languageHandler._t)("An error occurred changing the room's power level requirements. Ensure you have sufficient " + "permissions and try again.")
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onUserPowerLevelChanged", (value, powerLevelKey) => {
      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const room = client.getRoom(this.props.roomId);
      const plEvent = room.currentState.getStateEvents('m.room.power_levels', '');
      let plContent = plEvent ? plEvent.getContent() || {} : {}; // Clone the power levels just in case

      plContent = Object.assign({}, plContent); // powerLevelKey should be a user ID

      if (!plContent['users']) plContent['users'] = {};
      plContent['users'][powerLevelKey] = value;
      client.sendStateEvent(this.props.roomId, "m.room.power_levels", plContent).catch(e => {
        console.error(e);
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Power level change failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error changing power level'),
          description: (0, _languageHandler._t)("An error occurred changing the user's power level. Ensure you have sufficient " + "permissions and try again.")
        });
      });
    });
  }

  componentDidMount()
  /*: void*/
  {
    _MatrixClientPeg.MatrixClientPeg.get().on("RoomState.members", this._onRoomMembership);
  }

  componentWillUnmount()
  /*: void*/
  {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client) {
      client.removeListener("RoomState.members", this._onRoomMembership);
    }
  }

  _populateDefaultPlEvents(eventsSection, stateLevel, eventsLevel) {
    for (const desiredEvent of Object.keys(plEventsToShow)) {
      if (!(desiredEvent in eventsSection)) {
        eventsSection[desiredEvent] = plEventsToShow[desiredEvent].isState ? stateLevel : eventsLevel;
      }
    }
  }

  render() {
    const PowerSelector = sdk.getComponent('elements.PowerSelector');

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const room = client.getRoom(this.props.roomId);
    const plEvent = room.currentState.getStateEvents('m.room.power_levels', '');
    const plContent = plEvent ? plEvent.getContent() || {} : {};
    const canChangeLevels = room.currentState.mayClientSendStateEvent('m.room.power_levels', client);
    const powerLevelDescriptors = {
      "users_default": {
        desc: (0, _languageHandler._t)('Default role'),
        defaultValue: 0
      },
      "events_default": {
        desc: (0, _languageHandler._t)('Send messages'),
        defaultValue: 0
      },
      "invite": {
        desc: (0, _languageHandler._t)('Invite users'),
        defaultValue: 50
      },
      "state_default": {
        desc: (0, _languageHandler._t)('Change settings'),
        defaultValue: 50
      },
      "kick": {
        desc: (0, _languageHandler._t)('Kick users'),
        defaultValue: 50
      },
      "ban": {
        desc: (0, _languageHandler._t)('Ban users'),
        defaultValue: 50
      },
      "redact": {
        desc: (0, _languageHandler._t)('Remove messages'),
        defaultValue: 50
      },
      "notifications.room": {
        desc: (0, _languageHandler._t)('Notify everyone'),
        defaultValue: 50
      }
    };
    const eventsLevels = plContent.events || {};
    const userLevels = plContent.users || {};
    const banLevel = parseIntWithDefault(plContent.ban, powerLevelDescriptors.ban.defaultValue);
    const defaultUserLevel = parseIntWithDefault(plContent.users_default, powerLevelDescriptors.users_default.defaultValue);
    let currentUserLevel = userLevels[client.getUserId()];

    if (currentUserLevel === undefined) {
      currentUserLevel = defaultUserLevel;
    }

    this._populateDefaultPlEvents(eventsLevels, parseIntWithDefault(plContent.state_default, powerLevelDescriptors.state_default.defaultValue), parseIntWithDefault(plContent.events_default, powerLevelDescriptors.events_default.defaultValue));

    let privilegedUsersSection = _react.default.createElement("div", null, (0, _languageHandler._t)('No users have specific privileges in this room'));

    let mutedUsersSection;

    if (Object.keys(userLevels).length) {
      const privilegedUsers = [];
      const mutedUsers = [];
      Object.keys(userLevels).forEach(user => {
        const canChange = userLevels[user] < currentUserLevel && canChangeLevels;

        if (userLevels[user] > defaultUserLevel) {
          // privileged
          privilegedUsers.push(_react.default.createElement(PowerSelector, {
            value: userLevels[user],
            disabled: !canChange,
            label: user,
            key: user,
            powerLevelKey: user // Will be sent as the second parameter to `onChange`
            ,
            onChange: this._onUserPowerLevelChanged
          }));
        } else if (userLevels[user] < defaultUserLevel) {
          // muted
          mutedUsers.push(_react.default.createElement(PowerSelector, {
            value: userLevels[user],
            disabled: !canChange,
            label: user,
            key: user,
            powerLevelKey: user // Will be sent as the second parameter to `onChange`
            ,
            onChange: this._onUserPowerLevelChanged
          }));
        }
      }); // comparator for sorting PL users lexicographically on PL descending, MXID ascending. (case-insensitive)

      const comparator = (a, b) => {
        const plDiff = userLevels[b.key] - userLevels[a.key];
        return plDiff !== 0 ? plDiff : a.key.toLocaleLowerCase().localeCompare(b.key.toLocaleLowerCase());
      };

      privilegedUsers.sort(comparator);
      mutedUsers.sort(comparator);

      if (privilegedUsers.length) {
        privilegedUsersSection = _react.default.createElement("div", {
          className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
        }, _react.default.createElement("div", {
          className: "mx_SettingsTab_subheading"
        }, (0, _languageHandler._t)('Privileged Users')), privilegedUsers);
      }

      if (mutedUsers.length) {
        mutedUsersSection = _react.default.createElement("div", {
          className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
        }, _react.default.createElement("div", {
          className: "mx_SettingsTab_subheading"
        }, (0, _languageHandler._t)('Muted Users')), mutedUsers);
      }
    }

    const banned = room.getMembersWithMembership("ban");
    let bannedUsersSection;

    if (banned.length) {
      const canBanUsers = currentUserLevel >= banLevel;
      bannedUsersSection = _react.default.createElement("div", {
        className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
      }, _react.default.createElement("div", {
        className: "mx_SettingsTab_subheading"
      }, (0, _languageHandler._t)('Banned users')), _react.default.createElement("ul", null, banned.map(member => {
        const banEvent = member.events.member.getContent();
        const sender = room.getMember(member.events.member.getSender());
        let bannedBy = member.events.member.getSender(); // start by falling back to mxid

        if (sender) bannedBy = sender.name;
        return _react.default.createElement(BannedUser, {
          key: member.userId,
          canUnban: canBanUsers,
          member: member,
          reason: banEvent.reason,
          by: bannedBy
        });
      })));
    }

    const powerSelectors = Object.keys(powerLevelDescriptors).map((key, index) => {
      const descriptor = powerLevelDescriptors[key];
      const keyPath = key.split('.');
      let currentObj = plContent;

      for (const prop of keyPath) {
        if (currentObj === undefined) {
          break;
        }

        currentObj = currentObj[prop];
      }

      const value = parseIntWithDefault(currentObj, descriptor.defaultValue);
      return _react.default.createElement("div", {
        key: index,
        className: ""
      }, _react.default.createElement(PowerSelector, {
        label: descriptor.desc,
        value: value,
        usersDefault: defaultUserLevel,
        disabled: !canChangeLevels || currentUserLevel < value,
        powerLevelKey: key // Will be sent as the second parameter to `onChange`
        ,
        onChange: this._onPowerLevelsChanged
      }));
    }); // hide the power level selector for enabling E2EE if it the room is already encrypted

    if (client.isRoomEncrypted(this.props.roomId)) {
      delete eventsLevels["m.room.encryption"];
    }

    const eventPowerSelectors = Object.keys(eventsLevels).map((eventType, i) => {
      let label = plEventsToLabels[eventType];

      if (label) {
        label = (0, _languageHandler._t)(label);
      } else {
        label = (0, _languageHandler._t)("Send %(eventType)s events", {
          eventType
        });
      }

      return _react.default.createElement("div", {
        className: "",
        key: eventType
      }, _react.default.createElement(PowerSelector, {
        label: label,
        value: eventsLevels[eventType],
        usersDefault: defaultUserLevel,
        disabled: !canChangeLevels || currentUserLevel < eventsLevels[eventType],
        powerLevelKey: "event_levels_" + eventType,
        onChange: this._onPowerLevelsChanged
      }));
    });
    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_RolesRoomSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Roles & Permissions")), privilegedUsersSection, mutedUsersSection, bannedUsersSection, _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Permissions")), _react.default.createElement("p", null, (0, _languageHandler._t)('Select the roles required to change various parts of the room')), powerSelectors, eventPowerSelectors));
  }

}

exports.default = RolesRoomSettingsTab;
(0, _defineProperty2.default)(RolesRoomSettingsTab, "propTypes", {
  roomId: _propTypes.default.string.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvcm9vbS9Sb2xlc1Jvb21TZXR0aW5nc1RhYi5qcyJdLCJuYW1lcyI6WyJwbEV2ZW50c1RvTGFiZWxzIiwicGxFdmVudHNUb1Nob3ciLCJpc1N0YXRlIiwicGFyc2VJbnRXaXRoRGVmYXVsdCIsInZhbCIsImRlZiIsInJlcyIsInBhcnNlSW50IiwiaXNOYU4iLCJCYW5uZWRVc2VyIiwiUmVhY3QiLCJDb21wb25lbnQiLCJlIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwidW5iYW4iLCJwcm9wcyIsIm1lbWJlciIsInJvb21JZCIsInVzZXJJZCIsImNhdGNoIiwiZXJyIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjb25zb2xlIiwiZXJyb3IiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwicmVuZGVyIiwidW5iYW5CdXR0b24iLCJjYW5VbmJhbiIsIl9vblVuYmFuQ2xpY2siLCJuYW1lIiwiZGlzcGxheU5hbWUiLCJieSIsInJlYXNvbiIsIlByb3BUeXBlcyIsImJvb2wiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiUm9sZXNSb29tU2V0dGluZ3NUYWIiLCJldmVudCIsInN0YXRlIiwiZm9yY2VVcGRhdGUiLCJ2YWx1ZSIsInBvd2VyTGV2ZWxLZXkiLCJjbGllbnQiLCJyb29tIiwiZ2V0Um9vbSIsInBsRXZlbnQiLCJjdXJyZW50U3RhdGUiLCJnZXRTdGF0ZUV2ZW50cyIsInBsQ29udGVudCIsImdldENvbnRlbnQiLCJPYmplY3QiLCJhc3NpZ24iLCJldmVudHNMZXZlbFByZWZpeCIsInN0YXJ0c1dpdGgiLCJzbGljZSIsImxlbmd0aCIsImtleVBhdGgiLCJzcGxpdCIsInBhcmVudE9iaiIsImN1cnJlbnRPYmoiLCJrZXkiLCJzZW5kU3RhdGVFdmVudCIsImNvbXBvbmVudERpZE1vdW50Iiwib24iLCJfb25Sb29tTWVtYmVyc2hpcCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJfcG9wdWxhdGVEZWZhdWx0UGxFdmVudHMiLCJldmVudHNTZWN0aW9uIiwic3RhdGVMZXZlbCIsImV2ZW50c0xldmVsIiwiZGVzaXJlZEV2ZW50Iiwia2V5cyIsIlBvd2VyU2VsZWN0b3IiLCJjYW5DaGFuZ2VMZXZlbHMiLCJtYXlDbGllbnRTZW5kU3RhdGVFdmVudCIsInBvd2VyTGV2ZWxEZXNjcmlwdG9ycyIsImRlc2MiLCJkZWZhdWx0VmFsdWUiLCJldmVudHNMZXZlbHMiLCJldmVudHMiLCJ1c2VyTGV2ZWxzIiwidXNlcnMiLCJiYW5MZXZlbCIsImJhbiIsImRlZmF1bHRVc2VyTGV2ZWwiLCJ1c2Vyc19kZWZhdWx0IiwiY3VycmVudFVzZXJMZXZlbCIsImdldFVzZXJJZCIsInVuZGVmaW5lZCIsInN0YXRlX2RlZmF1bHQiLCJldmVudHNfZGVmYXVsdCIsInByaXZpbGVnZWRVc2Vyc1NlY3Rpb24iLCJtdXRlZFVzZXJzU2VjdGlvbiIsInByaXZpbGVnZWRVc2VycyIsIm11dGVkVXNlcnMiLCJmb3JFYWNoIiwidXNlciIsImNhbkNoYW5nZSIsInB1c2giLCJfb25Vc2VyUG93ZXJMZXZlbENoYW5nZWQiLCJjb21wYXJhdG9yIiwiYSIsImIiLCJwbERpZmYiLCJ0b0xvY2FsZUxvd2VyQ2FzZSIsImxvY2FsZUNvbXBhcmUiLCJzb3J0IiwiYmFubmVkIiwiZ2V0TWVtYmVyc1dpdGhNZW1iZXJzaGlwIiwiYmFubmVkVXNlcnNTZWN0aW9uIiwiY2FuQmFuVXNlcnMiLCJtYXAiLCJiYW5FdmVudCIsInNlbmRlciIsImdldE1lbWJlciIsImdldFNlbmRlciIsImJhbm5lZEJ5IiwicG93ZXJTZWxlY3RvcnMiLCJpbmRleCIsImRlc2NyaXB0b3IiLCJwcm9wIiwiX29uUG93ZXJMZXZlbHNDaGFuZ2VkIiwiaXNSb29tRW5jcnlwdGVkIiwiZXZlbnRQb3dlclNlbGVjdG9ycyIsImV2ZW50VHlwZSIsImkiLCJsYWJlbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF0QkE7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxNQUFNQSxnQkFBZ0IsR0FBRztBQUNyQjtBQUNBLG1CQUFpQiwwQkFBSSxvQkFBSixDQUZJO0FBR3JCLGlCQUFlLDBCQUFJLGtCQUFKLENBSE07QUFJckIsNEJBQTBCLDBCQUFJLGtDQUFKLENBSkw7QUFLckIsK0JBQTZCLDBCQUFJLDJCQUFKLENBTFI7QUFNckIseUJBQXVCLDBCQUFJLG9CQUFKLENBTkY7QUFPckIsa0JBQWdCLDBCQUFJLGNBQUosQ0FQSztBQVFyQixzQkFBb0IsMEJBQUksa0JBQUosQ0FSQztBQVNyQix1QkFBcUIsMEJBQUksd0JBQUosQ0FUQTtBQVdyQiwrQkFBNkIsMEJBQUksZ0JBQUo7QUFYUixDQUF6QjtBQWNBLE1BQU1DLGNBQWMsR0FBRztBQUNuQjtBQUNBLG1CQUFpQjtBQUFDQyxJQUFBQSxPQUFPLEVBQUU7QUFBVixHQUZFO0FBR25CLGlCQUFlO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBSEk7QUFJbkIsNEJBQTBCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBSlA7QUFLbkIsK0JBQTZCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBTFY7QUFNbkIseUJBQXVCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBTko7QUFPbkIsa0JBQWdCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBUEc7QUFRbkIsc0JBQW9CO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBUkQ7QUFTbkIsdUJBQXFCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWLEdBVEY7QUFXbkIsK0JBQTZCO0FBQUNBLElBQUFBLE9BQU8sRUFBRTtBQUFWO0FBWFYsQ0FBdkIsQyxDQWNBO0FBQ0E7O0FBQ0EsU0FBU0MsbUJBQVQsQ0FBNkJDLEdBQTdCLEVBQWtDQyxHQUFsQyxFQUF1QztBQUNuQyxRQUFNQyxHQUFHLEdBQUdDLFFBQVEsQ0FBQ0gsR0FBRCxDQUFwQjtBQUNBLFNBQU9JLEtBQUssQ0FBQ0YsR0FBRCxDQUFMLEdBQWFELEdBQWIsR0FBbUJDLEdBQTFCO0FBQ0g7O0FBRU0sTUFBTUcsVUFBTixTQUF5QkMsZUFBTUMsU0FBL0IsQ0FBeUM7QUFBQTtBQUFBO0FBQUEseURBUTNCQyxDQUFELElBQU87QUFDbkJDLHVDQUFnQkMsR0FBaEIsR0FBc0JDLEtBQXRCLENBQTRCLEtBQUtDLEtBQUwsQ0FBV0MsTUFBWCxDQUFrQkMsTUFBOUMsRUFBc0QsS0FBS0YsS0FBTCxDQUFXQyxNQUFYLENBQWtCRSxNQUF4RSxFQUFnRkMsS0FBaEYsQ0FBdUZDLEdBQUQsSUFBUztBQUMzRixjQUFNQyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQUMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsc0JBQXNCTCxHQUFwQzs7QUFDQU0sdUJBQU1DLG1CQUFOLENBQTBCLGlCQUExQixFQUE2QyxFQUE3QyxFQUFpRE4sV0FBakQsRUFBOEQ7QUFDMURPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRG1EO0FBRTFEQyxVQUFBQSxXQUFXLEVBQUUseUJBQUcsaUJBQUg7QUFGNkMsU0FBOUQ7QUFJSCxPQVBEO0FBUUgsS0FqQjJDO0FBQUE7O0FBbUI1Q0MsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSUMsV0FBSjs7QUFFQSxRQUFJLEtBQUtoQixLQUFMLENBQVdpQixRQUFmLEVBQXlCO0FBQ3JCRCxNQUFBQSxXQUFXLEdBQ1AsNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsV0FBdkI7QUFBbUMsUUFBQSxPQUFPLEVBQUUsS0FBS0UsYUFBakQ7QUFDa0IsUUFBQSxTQUFTLEVBQUM7QUFENUIsU0FFTSx5QkFBRyxPQUFILENBRk4sQ0FESjtBQU1IOztBQUVELFVBQU1mLE1BQU0sR0FBRyxLQUFLSCxLQUFMLENBQVdDLE1BQVgsQ0FBa0JrQixJQUFsQixLQUEyQixLQUFLbkIsS0FBTCxDQUFXQyxNQUFYLENBQWtCRSxNQUE3QyxHQUFzRCxJQUF0RCxHQUE2RCxLQUFLSCxLQUFMLENBQVdDLE1BQVgsQ0FBa0JFLE1BQTlGO0FBQ0EsV0FDSSx5Q0FDS2EsV0FETCxFQUVJO0FBQU0sTUFBQSxLQUFLLEVBQUUseUJBQUcsMkJBQUgsRUFBZ0M7QUFBQ0ksUUFBQUEsV0FBVyxFQUFFLEtBQUtwQixLQUFMLENBQVdxQjtBQUF6QixPQUFoQztBQUFiLE9BQ0ksNkNBQVUsS0FBS3JCLEtBQUwsQ0FBV0MsTUFBWCxDQUFrQmtCLElBQTVCLENBREosT0FDaURoQixNQURqRCxFQUVLLEtBQUtILEtBQUwsQ0FBV3NCLE1BQVgsR0FBb0IsTUFBTSx5QkFBRyxRQUFILENBQU4sR0FBcUIsSUFBckIsR0FBNEIsS0FBS3RCLEtBQUwsQ0FBV3NCLE1BQTNELEdBQW9FLEVBRnpFLENBRkosQ0FESjtBQVNIOztBQXpDMkM7Ozs4QkFBbkM3QixVLGVBQ1U7QUFDZndCLEVBQUFBLFFBQVEsRUFBRU0sbUJBQVVDLElBREw7QUFFZnZCLEVBQUFBLE1BQU0sRUFBRXNCLG1CQUFVRSxNQUFWLENBQWlCQyxVQUZWO0FBRXNCO0FBQ3JDTCxFQUFBQSxFQUFFLEVBQUVFLG1CQUFVSSxNQUFWLENBQWlCRCxVQUhOO0FBSWZKLEVBQUFBLE1BQU0sRUFBRUMsbUJBQVVJO0FBSkgsQzs7QUEyQ1IsTUFBTUMsb0JBQU4sU0FBbUNsQyxlQUFNQyxTQUF6QyxDQUFtRDtBQUFBO0FBQUE7QUFBQSw2REFnQjFDLENBQUNrQyxLQUFELEVBQVFDLEtBQVIsRUFBZTdCLE1BQWYsS0FBMEI7QUFDMUMsVUFBSTZCLEtBQUssQ0FBQzVCLE1BQU4sS0FBaUIsS0FBS0YsS0FBTCxDQUFXRSxNQUFoQyxFQUF3QztBQUN4QyxXQUFLNkIsV0FBTDtBQUNILEtBbkI2RDtBQUFBLGlFQTZCdEMsQ0FBQ0MsS0FBRCxFQUFRQyxhQUFSLEtBQTBCO0FBQzlDLFlBQU1DLE1BQU0sR0FBR3JDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxZQUFNcUMsSUFBSSxHQUFHRCxNQUFNLENBQUNFLE9BQVAsQ0FBZSxLQUFLcEMsS0FBTCxDQUFXRSxNQUExQixDQUFiO0FBQ0EsWUFBTW1DLE9BQU8sR0FBR0YsSUFBSSxDQUFDRyxZQUFMLENBQWtCQyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBaEI7QUFDQSxVQUFJQyxTQUFTLEdBQUdILE9BQU8sR0FBSUEsT0FBTyxDQUFDSSxVQUFSLE1BQXdCLEVBQTVCLEdBQWtDLEVBQXpELENBSjhDLENBTTlDOztBQUNBRCxNQUFBQSxTQUFTLEdBQUdFLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JILFNBQWxCLENBQVo7QUFFQSxZQUFNSSxpQkFBaUIsR0FBRyxlQUExQjtBQUVBWixNQUFBQSxLQUFLLEdBQUd6QyxRQUFRLENBQUN5QyxLQUFELENBQWhCOztBQUVBLFVBQUlDLGFBQWEsQ0FBQ1ksVUFBZCxDQUF5QkQsaUJBQXpCLENBQUosRUFBaUQ7QUFDN0M7QUFDQUosUUFBQUEsU0FBUyxDQUFDLFFBQUQsQ0FBVCxHQUFzQkUsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQkgsU0FBUyxDQUFDLFFBQUQsQ0FBVCxJQUF1QixFQUF6QyxDQUF0QjtBQUNBQSxRQUFBQSxTQUFTLENBQUMsUUFBRCxDQUFULENBQW9CUCxhQUFhLENBQUNhLEtBQWQsQ0FBb0JGLGlCQUFpQixDQUFDRyxNQUF0QyxDQUFwQixJQUFxRWYsS0FBckU7QUFDSCxPQUpELE1BSU87QUFDSCxjQUFNZ0IsT0FBTyxHQUFHZixhQUFhLENBQUNnQixLQUFkLENBQW9CLEdBQXBCLENBQWhCO0FBQ0EsWUFBSUMsU0FBSjtBQUNBLFlBQUlDLFVBQVUsR0FBR1gsU0FBakI7O0FBQ0EsYUFBSyxNQUFNWSxHQUFYLElBQWtCSixPQUFsQixFQUEyQjtBQUN2QixjQUFJLENBQUNHLFVBQVUsQ0FBQ0MsR0FBRCxDQUFmLEVBQXNCO0FBQ2xCRCxZQUFBQSxVQUFVLENBQUNDLEdBQUQsQ0FBVixHQUFrQixFQUFsQjtBQUNIOztBQUNERixVQUFBQSxTQUFTLEdBQUdDLFVBQVo7QUFDQUEsVUFBQUEsVUFBVSxHQUFHQSxVQUFVLENBQUNDLEdBQUQsQ0FBdkI7QUFDSDs7QUFDREYsUUFBQUEsU0FBUyxDQUFDRixPQUFPLENBQUNBLE9BQU8sQ0FBQ0QsTUFBUixHQUFpQixDQUFsQixDQUFSLENBQVQsR0FBeUNmLEtBQXpDO0FBQ0g7O0FBRURFLE1BQUFBLE1BQU0sQ0FBQ21CLGNBQVAsQ0FBc0IsS0FBS3JELEtBQUwsQ0FBV0UsTUFBakMsRUFBeUMscUJBQXpDLEVBQWdFc0MsU0FBaEUsRUFBMkVwQyxLQUEzRSxDQUFpRlIsQ0FBQyxJQUFJO0FBQ2xGYSxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY2QsQ0FBZDtBQUVBLGNBQU1VLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUcsdUJBQU1DLG1CQUFOLENBQTBCLHVDQUExQixFQUFtRSxFQUFuRSxFQUF1RU4sV0FBdkUsRUFBb0Y7QUFDaEZPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyx3Q0FBSCxDQUR5RTtBQUVoRkMsVUFBQUEsV0FBVyxFQUFFLHlCQUNULGdHQUNBLDRCQUZTO0FBRm1FLFNBQXBGO0FBT0gsT0FYRDtBQVlILEtBeEU2RDtBQUFBLG9FQTBFbkMsQ0FBQ2tCLEtBQUQsRUFBUUMsYUFBUixLQUEwQjtBQUNqRCxZQUFNQyxNQUFNLEdBQUdyQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsWUFBTXFDLElBQUksR0FBR0QsTUFBTSxDQUFDRSxPQUFQLENBQWUsS0FBS3BDLEtBQUwsQ0FBV0UsTUFBMUIsQ0FBYjtBQUNBLFlBQU1tQyxPQUFPLEdBQUdGLElBQUksQ0FBQ0csWUFBTCxDQUFrQkMsY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQWhCO0FBQ0EsVUFBSUMsU0FBUyxHQUFHSCxPQUFPLEdBQUlBLE9BQU8sQ0FBQ0ksVUFBUixNQUF3QixFQUE1QixHQUFrQyxFQUF6RCxDQUppRCxDQU1qRDs7QUFDQUQsTUFBQUEsU0FBUyxHQUFHRSxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCSCxTQUFsQixDQUFaLENBUGlELENBU2pEOztBQUNBLFVBQUksQ0FBQ0EsU0FBUyxDQUFDLE9BQUQsQ0FBZCxFQUF5QkEsU0FBUyxDQUFDLE9BQUQsQ0FBVCxHQUFxQixFQUFyQjtBQUN6QkEsTUFBQUEsU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQlAsYUFBbkIsSUFBb0NELEtBQXBDO0FBRUFFLE1BQUFBLE1BQU0sQ0FBQ21CLGNBQVAsQ0FBc0IsS0FBS3JELEtBQUwsQ0FBV0UsTUFBakMsRUFBeUMscUJBQXpDLEVBQWdFc0MsU0FBaEUsRUFBMkVwQyxLQUEzRSxDQUFpRlIsQ0FBQyxJQUFJO0FBQ2xGYSxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY2QsQ0FBZDtBQUVBLGNBQU1VLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUcsdUJBQU1DLG1CQUFOLENBQTBCLDJCQUExQixFQUF1RCxFQUF2RCxFQUEyRE4sV0FBM0QsRUFBd0U7QUFDcEVPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyw0QkFBSCxDQUQ2RDtBQUVwRUMsVUFBQUEsV0FBVyxFQUFFLHlCQUNULG1GQUNBLDRCQUZTO0FBRnVELFNBQXhFO0FBT0gsT0FYRDtBQVlILEtBbkc2RDtBQUFBOztBQUs5RHdDLEVBQUFBLGlCQUFpQjtBQUFBO0FBQVM7QUFDdEJ6RCxxQ0FBZ0JDLEdBQWhCLEdBQXNCeUQsRUFBdEIsQ0FBeUIsbUJBQXpCLEVBQThDLEtBQUtDLGlCQUFuRDtBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0I7QUFBQTtBQUFTO0FBQ3pCLFVBQU12QixNQUFNLEdBQUdyQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsUUFBSW9DLE1BQUosRUFBWTtBQUNSQSxNQUFBQSxNQUFNLENBQUN3QixjQUFQLENBQXNCLG1CQUF0QixFQUEyQyxLQUFLRixpQkFBaEQ7QUFDSDtBQUNKOztBQU9ERyxFQUFBQSx3QkFBd0IsQ0FBQ0MsYUFBRCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzdELFNBQUssTUFBTUMsWUFBWCxJQUEyQnJCLE1BQU0sQ0FBQ3NCLElBQVAsQ0FBWS9FLGNBQVosQ0FBM0IsRUFBd0Q7QUFDcEQsVUFBSSxFQUFFOEUsWUFBWSxJQUFJSCxhQUFsQixDQUFKLEVBQXNDO0FBQ2xDQSxRQUFBQSxhQUFhLENBQUNHLFlBQUQsQ0FBYixHQUErQjlFLGNBQWMsQ0FBQzhFLFlBQUQsQ0FBZCxDQUE2QjdFLE9BQTdCLEdBQXVDMkUsVUFBdkMsR0FBb0RDLFdBQW5GO0FBQ0g7QUFDSjtBQUNKOztBQTBFRC9DLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1rRCxhQUFhLEdBQUcxRCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCOztBQUVBLFVBQU0wQixNQUFNLEdBQUdyQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTXFDLElBQUksR0FBR0QsTUFBTSxDQUFDRSxPQUFQLENBQWUsS0FBS3BDLEtBQUwsQ0FBV0UsTUFBMUIsQ0FBYjtBQUNBLFVBQU1tQyxPQUFPLEdBQUdGLElBQUksQ0FBQ0csWUFBTCxDQUFrQkMsY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQWhCO0FBQ0EsVUFBTUMsU0FBUyxHQUFHSCxPQUFPLEdBQUlBLE9BQU8sQ0FBQ0ksVUFBUixNQUF3QixFQUE1QixHQUFrQyxFQUEzRDtBQUNBLFVBQU15QixlQUFlLEdBQUcvQixJQUFJLENBQUNHLFlBQUwsQ0FBa0I2Qix1QkFBbEIsQ0FBMEMscUJBQTFDLEVBQWlFakMsTUFBakUsQ0FBeEI7QUFFQSxVQUFNa0MscUJBQXFCLEdBQUc7QUFDMUIsdUJBQWlCO0FBQ2JDLFFBQUFBLElBQUksRUFBRSx5QkFBRyxjQUFILENBRE87QUFFYkMsUUFBQUEsWUFBWSxFQUFFO0FBRkQsT0FEUztBQUsxQix3QkFBa0I7QUFDZEQsUUFBQUEsSUFBSSxFQUFFLHlCQUFHLGVBQUgsQ0FEUTtBQUVkQyxRQUFBQSxZQUFZLEVBQUU7QUFGQSxPQUxRO0FBUzFCLGdCQUFVO0FBQ05ELFFBQUFBLElBQUksRUFBRSx5QkFBRyxjQUFILENBREE7QUFFTkMsUUFBQUEsWUFBWSxFQUFFO0FBRlIsT0FUZ0I7QUFhMUIsdUJBQWlCO0FBQ2JELFFBQUFBLElBQUksRUFBRSx5QkFBRyxpQkFBSCxDQURPO0FBRWJDLFFBQUFBLFlBQVksRUFBRTtBQUZELE9BYlM7QUFpQjFCLGNBQVE7QUFDSkQsUUFBQUEsSUFBSSxFQUFFLHlCQUFHLFlBQUgsQ0FERjtBQUVKQyxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQWpCa0I7QUFxQjFCLGFBQU87QUFDSEQsUUFBQUEsSUFBSSxFQUFFLHlCQUFHLFdBQUgsQ0FESDtBQUVIQyxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQXJCbUI7QUF5QjFCLGdCQUFVO0FBQ05ELFFBQUFBLElBQUksRUFBRSx5QkFBRyxpQkFBSCxDQURBO0FBRU5DLFFBQUFBLFlBQVksRUFBRTtBQUZSLE9BekJnQjtBQTZCMUIsNEJBQXNCO0FBQ2xCRCxRQUFBQSxJQUFJLEVBQUUseUJBQUcsaUJBQUgsQ0FEWTtBQUVsQkMsUUFBQUEsWUFBWSxFQUFFO0FBRkk7QUE3QkksS0FBOUI7QUFtQ0EsVUFBTUMsWUFBWSxHQUFHL0IsU0FBUyxDQUFDZ0MsTUFBVixJQUFvQixFQUF6QztBQUNBLFVBQU1DLFVBQVUsR0FBR2pDLFNBQVMsQ0FBQ2tDLEtBQVYsSUFBbUIsRUFBdEM7QUFDQSxVQUFNQyxRQUFRLEdBQUd4RixtQkFBbUIsQ0FBQ3FELFNBQVMsQ0FBQ29DLEdBQVgsRUFBZ0JSLHFCQUFxQixDQUFDUSxHQUF0QixDQUEwQk4sWUFBMUMsQ0FBcEM7QUFDQSxVQUFNTyxnQkFBZ0IsR0FBRzFGLG1CQUFtQixDQUN4Q3FELFNBQVMsQ0FBQ3NDLGFBRDhCLEVBRXhDVixxQkFBcUIsQ0FBQ1UsYUFBdEIsQ0FBb0NSLFlBRkksQ0FBNUM7QUFLQSxRQUFJUyxnQkFBZ0IsR0FBR04sVUFBVSxDQUFDdkMsTUFBTSxDQUFDOEMsU0FBUCxFQUFELENBQWpDOztBQUNBLFFBQUlELGdCQUFnQixLQUFLRSxTQUF6QixFQUFvQztBQUNoQ0YsTUFBQUEsZ0JBQWdCLEdBQUdGLGdCQUFuQjtBQUNIOztBQUVELFNBQUtsQix3QkFBTCxDQUNJWSxZQURKLEVBRUlwRixtQkFBbUIsQ0FBQ3FELFNBQVMsQ0FBQzBDLGFBQVgsRUFBMEJkLHFCQUFxQixDQUFDYyxhQUF0QixDQUFvQ1osWUFBOUQsQ0FGdkIsRUFHSW5GLG1CQUFtQixDQUFDcUQsU0FBUyxDQUFDMkMsY0FBWCxFQUEyQmYscUJBQXFCLENBQUNlLGNBQXRCLENBQXFDYixZQUFoRSxDQUh2Qjs7QUFNQSxRQUFJYyxzQkFBc0IsR0FBRywwQ0FBTSx5QkFBRyxnREFBSCxDQUFOLENBQTdCOztBQUNBLFFBQUlDLGlCQUFKOztBQUNBLFFBQUkzQyxNQUFNLENBQUNzQixJQUFQLENBQVlTLFVBQVosRUFBd0IxQixNQUE1QixFQUFvQztBQUNoQyxZQUFNdUMsZUFBZSxHQUFHLEVBQXhCO0FBQ0EsWUFBTUMsVUFBVSxHQUFHLEVBQW5CO0FBRUE3QyxNQUFBQSxNQUFNLENBQUNzQixJQUFQLENBQVlTLFVBQVosRUFBd0JlLE9BQXhCLENBQWlDQyxJQUFELElBQVU7QUFDdEMsY0FBTUMsU0FBUyxHQUFHakIsVUFBVSxDQUFDZ0IsSUFBRCxDQUFWLEdBQW1CVixnQkFBbkIsSUFBdUNiLGVBQXpEOztBQUNBLFlBQUlPLFVBQVUsQ0FBQ2dCLElBQUQsQ0FBVixHQUFtQlosZ0JBQXZCLEVBQXlDO0FBQUU7QUFDdkNTLFVBQUFBLGVBQWUsQ0FBQ0ssSUFBaEIsQ0FDSSw2QkFBQyxhQUFEO0FBQ0ksWUFBQSxLQUFLLEVBQUVsQixVQUFVLENBQUNnQixJQUFELENBRHJCO0FBRUksWUFBQSxRQUFRLEVBQUUsQ0FBQ0MsU0FGZjtBQUdJLFlBQUEsS0FBSyxFQUFFRCxJQUhYO0FBSUksWUFBQSxHQUFHLEVBQUVBLElBSlQ7QUFLSSxZQUFBLGFBQWEsRUFBRUEsSUFMbkIsQ0FLeUI7QUFMekI7QUFNSSxZQUFBLFFBQVEsRUFBRSxLQUFLRztBQU5uQixZQURKO0FBVUgsU0FYRCxNQVdPLElBQUluQixVQUFVLENBQUNnQixJQUFELENBQVYsR0FBbUJaLGdCQUF2QixFQUF5QztBQUFFO0FBQzlDVSxVQUFBQSxVQUFVLENBQUNJLElBQVgsQ0FDSSw2QkFBQyxhQUFEO0FBQ0ksWUFBQSxLQUFLLEVBQUVsQixVQUFVLENBQUNnQixJQUFELENBRHJCO0FBRUksWUFBQSxRQUFRLEVBQUUsQ0FBQ0MsU0FGZjtBQUdJLFlBQUEsS0FBSyxFQUFFRCxJQUhYO0FBSUksWUFBQSxHQUFHLEVBQUVBLElBSlQ7QUFLSSxZQUFBLGFBQWEsRUFBRUEsSUFMbkIsQ0FLeUI7QUFMekI7QUFNSSxZQUFBLFFBQVEsRUFBRSxLQUFLRztBQU5uQixZQURKO0FBVUg7QUFDSixPQXpCRCxFQUpnQyxDQStCaEM7O0FBQ0EsWUFBTUMsVUFBVSxHQUFHLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVO0FBQ3pCLGNBQU1DLE1BQU0sR0FBR3ZCLFVBQVUsQ0FBQ3NCLENBQUMsQ0FBQzNDLEdBQUgsQ0FBVixHQUFvQnFCLFVBQVUsQ0FBQ3FCLENBQUMsQ0FBQzFDLEdBQUgsQ0FBN0M7QUFDQSxlQUFPNEMsTUFBTSxLQUFLLENBQVgsR0FBZUEsTUFBZixHQUF3QkYsQ0FBQyxDQUFDMUMsR0FBRixDQUFNNkMsaUJBQU4sR0FBMEJDLGFBQTFCLENBQXdDSCxDQUFDLENBQUMzQyxHQUFGLENBQU02QyxpQkFBTixFQUF4QyxDQUEvQjtBQUNILE9BSEQ7O0FBS0FYLE1BQUFBLGVBQWUsQ0FBQ2EsSUFBaEIsQ0FBcUJOLFVBQXJCO0FBQ0FOLE1BQUFBLFVBQVUsQ0FBQ1ksSUFBWCxDQUFnQk4sVUFBaEI7O0FBRUEsVUFBSVAsZUFBZSxDQUFDdkMsTUFBcEIsRUFBNEI7QUFDeEJxQyxRQUFBQSxzQkFBc0IsR0FDbEI7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQTZDLHlCQUFHLGtCQUFILENBQTdDLENBREosRUFFS0UsZUFGTCxDQURKO0FBS0g7O0FBQ0QsVUFBSUMsVUFBVSxDQUFDeEMsTUFBZixFQUF1QjtBQUNuQnNDLFFBQUFBLGlCQUFpQixHQUNiO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUE2Qyx5QkFBRyxhQUFILENBQTdDLENBREosRUFFS0UsVUFGTCxDQURKO0FBS0g7QUFDSjs7QUFFRCxVQUFNYSxNQUFNLEdBQUdqRSxJQUFJLENBQUNrRSx3QkFBTCxDQUE4QixLQUE5QixDQUFmO0FBQ0EsUUFBSUMsa0JBQUo7O0FBQ0EsUUFBSUYsTUFBTSxDQUFDckQsTUFBWCxFQUFtQjtBQUNmLFlBQU13RCxXQUFXLEdBQUd4QixnQkFBZ0IsSUFBSUosUUFBeEM7QUFDQTJCLE1BQUFBLGtCQUFrQixHQUNkO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUE2Qyx5QkFBRyxjQUFILENBQTdDLENBREosRUFFSSx5Q0FDS0YsTUFBTSxDQUFDSSxHQUFQLENBQVl2RyxNQUFELElBQVk7QUFDcEIsY0FBTXdHLFFBQVEsR0FBR3hHLE1BQU0sQ0FBQ3VFLE1BQVAsQ0FBY3ZFLE1BQWQsQ0FBcUJ3QyxVQUFyQixFQUFqQjtBQUNBLGNBQU1pRSxNQUFNLEdBQUd2RSxJQUFJLENBQUN3RSxTQUFMLENBQWUxRyxNQUFNLENBQUN1RSxNQUFQLENBQWN2RSxNQUFkLENBQXFCMkcsU0FBckIsRUFBZixDQUFmO0FBQ0EsWUFBSUMsUUFBUSxHQUFHNUcsTUFBTSxDQUFDdUUsTUFBUCxDQUFjdkUsTUFBZCxDQUFxQjJHLFNBQXJCLEVBQWYsQ0FIb0IsQ0FHNkI7O0FBQ2pELFlBQUlGLE1BQUosRUFBWUcsUUFBUSxHQUFHSCxNQUFNLENBQUN2RixJQUFsQjtBQUNaLGVBQ0ksNkJBQUMsVUFBRDtBQUFZLFVBQUEsR0FBRyxFQUFFbEIsTUFBTSxDQUFDRSxNQUF4QjtBQUFnQyxVQUFBLFFBQVEsRUFBRW9HLFdBQTFDO0FBQ1ksVUFBQSxNQUFNLEVBQUV0RyxNQURwQjtBQUM0QixVQUFBLE1BQU0sRUFBRXdHLFFBQVEsQ0FBQ25GLE1BRDdDO0FBRVksVUFBQSxFQUFFLEVBQUV1RjtBQUZoQixVQURKO0FBS0gsT0FWQSxDQURMLENBRkosQ0FESjtBQWlCSDs7QUFFRCxVQUFNQyxjQUFjLEdBQUdwRSxNQUFNLENBQUNzQixJQUFQLENBQVlJLHFCQUFaLEVBQW1Db0MsR0FBbkMsQ0FBdUMsQ0FBQ3BELEdBQUQsRUFBTTJELEtBQU4sS0FBZ0I7QUFDMUUsWUFBTUMsVUFBVSxHQUFHNUMscUJBQXFCLENBQUNoQixHQUFELENBQXhDO0FBRUEsWUFBTUosT0FBTyxHQUFHSSxHQUFHLENBQUNILEtBQUosQ0FBVSxHQUFWLENBQWhCO0FBQ0EsVUFBSUUsVUFBVSxHQUFHWCxTQUFqQjs7QUFDQSxXQUFLLE1BQU15RSxJQUFYLElBQW1CakUsT0FBbkIsRUFBNEI7QUFDeEIsWUFBSUcsVUFBVSxLQUFLOEIsU0FBbkIsRUFBOEI7QUFDMUI7QUFDSDs7QUFDRDlCLFFBQUFBLFVBQVUsR0FBR0EsVUFBVSxDQUFDOEQsSUFBRCxDQUF2QjtBQUNIOztBQUVELFlBQU1qRixLQUFLLEdBQUc3QyxtQkFBbUIsQ0FBQ2dFLFVBQUQsRUFBYTZELFVBQVUsQ0FBQzFDLFlBQXhCLENBQWpDO0FBQ0EsYUFBTztBQUFLLFFBQUEsR0FBRyxFQUFFeUMsS0FBVjtBQUFpQixRQUFBLFNBQVMsRUFBQztBQUEzQixTQUNILDZCQUFDLGFBQUQ7QUFDSSxRQUFBLEtBQUssRUFBRUMsVUFBVSxDQUFDM0MsSUFEdEI7QUFFSSxRQUFBLEtBQUssRUFBRXJDLEtBRlg7QUFHSSxRQUFBLFlBQVksRUFBRTZDLGdCQUhsQjtBQUlJLFFBQUEsUUFBUSxFQUFFLENBQUNYLGVBQUQsSUFBb0JhLGdCQUFnQixHQUFHL0MsS0FKckQ7QUFLSSxRQUFBLGFBQWEsRUFBRW9CLEdBTG5CLENBS3dCO0FBTHhCO0FBTUksUUFBQSxRQUFRLEVBQUUsS0FBSzhEO0FBTm5CLFFBREcsQ0FBUDtBQVVILEtBdkJzQixDQUF2QixDQWhKSyxDQXlLTDs7QUFDQSxRQUFJaEYsTUFBTSxDQUFDaUYsZUFBUCxDQUF1QixLQUFLbkgsS0FBTCxDQUFXRSxNQUFsQyxDQUFKLEVBQStDO0FBQzNDLGFBQU9xRSxZQUFZLENBQUMsbUJBQUQsQ0FBbkI7QUFDSDs7QUFFRCxVQUFNNkMsbUJBQW1CLEdBQUcxRSxNQUFNLENBQUNzQixJQUFQLENBQVlPLFlBQVosRUFBMEJpQyxHQUExQixDQUE4QixDQUFDYSxTQUFELEVBQVlDLENBQVosS0FBa0I7QUFDeEUsVUFBSUMsS0FBSyxHQUFHdkksZ0JBQWdCLENBQUNxSSxTQUFELENBQTVCOztBQUNBLFVBQUlFLEtBQUosRUFBVztBQUNQQSxRQUFBQSxLQUFLLEdBQUcseUJBQUdBLEtBQUgsQ0FBUjtBQUNILE9BRkQsTUFFTztBQUNIQSxRQUFBQSxLQUFLLEdBQUcseUJBQUcsMkJBQUgsRUFBZ0M7QUFBQ0YsVUFBQUE7QUFBRCxTQUFoQyxDQUFSO0FBQ0g7O0FBQ0QsYUFDSTtBQUFLLFFBQUEsU0FBUyxFQUFDLEVBQWY7QUFBa0IsUUFBQSxHQUFHLEVBQUVBO0FBQXZCLFNBQ0ksNkJBQUMsYUFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFRSxLQURYO0FBRUksUUFBQSxLQUFLLEVBQUVoRCxZQUFZLENBQUM4QyxTQUFELENBRnZCO0FBR0ksUUFBQSxZQUFZLEVBQUV4QyxnQkFIbEI7QUFJSSxRQUFBLFFBQVEsRUFBRSxDQUFDWCxlQUFELElBQW9CYSxnQkFBZ0IsR0FBR1IsWUFBWSxDQUFDOEMsU0FBRCxDQUpqRTtBQUtJLFFBQUEsYUFBYSxFQUFFLGtCQUFrQkEsU0FMckM7QUFNSSxRQUFBLFFBQVEsRUFBRSxLQUFLSDtBQU5uQixRQURKLENBREo7QUFZSCxLQW5CMkIsQ0FBNUI7QUFxQkEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBeUMseUJBQUcscUJBQUgsQ0FBekMsQ0FESixFQUVLOUIsc0JBRkwsRUFHS0MsaUJBSEwsRUFJS2lCLGtCQUpMLEVBS0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxhQUFILENBQTdDLENBREosRUFFSSx3Q0FBSSx5QkFBRywrREFBSCxDQUFKLENBRkosRUFHS1EsY0FITCxFQUlLTSxtQkFKTCxDQUxKLENBREo7QUFjSDs7QUF0VDZEOzs7OEJBQTdDeEYsb0IsZUFDRTtBQUNmMUIsRUFBQUEsTUFBTSxFQUFFcUIsbUJBQVVJLE1BQVYsQ0FBaUJEO0FBRFYsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdCwgX3RkfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vLi4vLi5cIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4uLy4uLy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9Nb2RhbFwiO1xyXG5cclxuY29uc3QgcGxFdmVudHNUb0xhYmVscyA9IHtcclxuICAgIC8vIFRoZXNlIHdpbGwgYmUgdHJhbnNsYXRlZCBmb3IgdXMgbGF0ZXIuXHJcbiAgICBcIm0ucm9vbS5hdmF0YXJcIjogX3RkKFwiQ2hhbmdlIHJvb20gYXZhdGFyXCIpLFxyXG4gICAgXCJtLnJvb20ubmFtZVwiOiBfdGQoXCJDaGFuZ2Ugcm9vbSBuYW1lXCIpLFxyXG4gICAgXCJtLnJvb20uY2Fub25pY2FsX2FsaWFzXCI6IF90ZChcIkNoYW5nZSBtYWluIGFkZHJlc3MgZm9yIHRoZSByb29tXCIpLFxyXG4gICAgXCJtLnJvb20uaGlzdG9yeV92aXNpYmlsaXR5XCI6IF90ZChcIkNoYW5nZSBoaXN0b3J5IHZpc2liaWxpdHlcIiksXHJcbiAgICBcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIjogX3RkKFwiQ2hhbmdlIHBlcm1pc3Npb25zXCIpLFxyXG4gICAgXCJtLnJvb20udG9waWNcIjogX3RkKFwiQ2hhbmdlIHRvcGljXCIpLFxyXG4gICAgXCJtLnJvb20udG9tYnN0b25lXCI6IF90ZChcIlVwZ3JhZGUgdGhlIHJvb21cIiksXHJcbiAgICBcIm0ucm9vbS5lbmNyeXB0aW9uXCI6IF90ZChcIkVuYWJsZSByb29tIGVuY3J5cHRpb25cIiksXHJcblxyXG4gICAgXCJpbS52ZWN0b3IubW9kdWxhci53aWRnZXRzXCI6IF90ZChcIk1vZGlmeSB3aWRnZXRzXCIpLFxyXG59O1xyXG5cclxuY29uc3QgcGxFdmVudHNUb1Nob3cgPSB7XHJcbiAgICAvLyBJZiBhbiBldmVudCBpcyBsaXN0ZWQgaGVyZSwgaXQgd2lsbCBiZSBzaG93biBpbiB0aGUgUEwgc2V0dGluZ3MuIERlZmF1bHRzIHdpbGwgYmUgY2FsY3VsYXRlZC5cclxuICAgIFwibS5yb29tLmF2YXRhclwiOiB7aXNTdGF0ZTogdHJ1ZX0sXHJcbiAgICBcIm0ucm9vbS5uYW1lXCI6IHtpc1N0YXRlOiB0cnVlfSxcclxuICAgIFwibS5yb29tLmNhbm9uaWNhbF9hbGlhc1wiOiB7aXNTdGF0ZTogdHJ1ZX0sXHJcbiAgICBcIm0ucm9vbS5oaXN0b3J5X3Zpc2liaWxpdHlcIjoge2lzU3RhdGU6IHRydWV9LFxyXG4gICAgXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCI6IHtpc1N0YXRlOiB0cnVlfSxcclxuICAgIFwibS5yb29tLnRvcGljXCI6IHtpc1N0YXRlOiB0cnVlfSxcclxuICAgIFwibS5yb29tLnRvbWJzdG9uZVwiOiB7aXNTdGF0ZTogdHJ1ZX0sXHJcbiAgICBcIm0ucm9vbS5lbmNyeXB0aW9uXCI6IHtpc1N0YXRlOiB0cnVlfSxcclxuXHJcbiAgICBcImltLnZlY3Rvci5tb2R1bGFyLndpZGdldHNcIjoge2lzU3RhdGU6IHRydWV9LFxyXG59O1xyXG5cclxuLy8gcGFyc2UgYSBzdHJpbmcgYXMgYW4gaW50ZWdlcjsgaWYgdGhlIGlucHV0IGlzIHVuZGVmaW5lZCwgb3IgY2Fubm90IGJlIHBhcnNlZFxyXG4vLyBhcyBhbiBpbnRlZ2VyLCByZXR1cm4gYSBkZWZhdWx0LlxyXG5mdW5jdGlvbiBwYXJzZUludFdpdGhEZWZhdWx0KHZhbCwgZGVmKSB7XHJcbiAgICBjb25zdCByZXMgPSBwYXJzZUludCh2YWwpO1xyXG4gICAgcmV0dXJuIGlzTmFOKHJlcykgPyBkZWYgOiByZXM7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBCYW5uZWRVc2VyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgY2FuVW5iYW46IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIG1lbWJlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLCAvLyBqcy1zZGsgUm9vbU1lbWJlclxyXG4gICAgICAgIGJ5OiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgcmVhc29uOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgfTtcclxuXHJcbiAgICBfb25VbmJhbkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkudW5iYW4odGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkLCB0aGlzLnByb3BzLm1lbWJlci51c2VySWQpLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byB1bmJhbjogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gdW5iYW4nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXJyb3InKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdCgnRmFpbGVkIHRvIHVuYmFuJyksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgbGV0IHVuYmFuQnV0dG9uO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jYW5VbmJhbikge1xyXG4gICAgICAgICAgICB1bmJhbkJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9J2Rhbmdlcl9zbScgb25DbGljaz17dGhpcy5fb25VbmJhbkNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdteF9Sb2xlc1Jvb21TZXR0aW5nc1RhYl91bmJhbkJ0bic+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnVW5iYW4nKSB9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnByb3BzLm1lbWJlci5uYW1lID09PSB0aGlzLnByb3BzLm1lbWJlci51c2VySWQgPyBudWxsIDogdGhpcy5wcm9wcy5tZW1iZXIudXNlcklkO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgIHt1bmJhbkJ1dHRvbn1cclxuICAgICAgICAgICAgICAgIDxzcGFuIHRpdGxlPXtfdChcIkJhbm5lZCBieSAlKGRpc3BsYXlOYW1lKXNcIiwge2Rpc3BsYXlOYW1lOiB0aGlzLnByb3BzLmJ5fSl9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+eyB0aGlzLnByb3BzLm1lbWJlci5uYW1lIH08L3N0cm9uZz4ge3VzZXJJZH1cclxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5yZWFzb24gPyBcIiBcIiArIF90KCdSZWFzb24nKSArIFwiOiBcIiArIHRoaXMucHJvcHMucmVhc29uIDogXCJcIn1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb2xlc1Jvb21TZXR0aW5nc1RhYiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpOiB2b2lkIHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLl9vblJvb21NZW1iZXJzaGlwKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaWVudCkge1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLl9vblJvb21NZW1iZXJzaGlwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uUm9vbU1lbWJlcnNoaXAgPSAoZXZlbnQsIHN0YXRlLCBtZW1iZXIpID0+IHtcclxuICAgICAgICBpZiAoc3RhdGUucm9vbUlkICE9PSB0aGlzLnByb3BzLnJvb21JZCkgcmV0dXJuO1xyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgX3BvcHVsYXRlRGVmYXVsdFBsRXZlbnRzKGV2ZW50c1NlY3Rpb24sIHN0YXRlTGV2ZWwsIGV2ZW50c0xldmVsKSB7XHJcbiAgICAgICAgZm9yIChjb25zdCBkZXNpcmVkRXZlbnQgb2YgT2JqZWN0LmtleXMocGxFdmVudHNUb1Nob3cpKSB7XHJcbiAgICAgICAgICAgIGlmICghKGRlc2lyZWRFdmVudCBpbiBldmVudHNTZWN0aW9uKSkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnRzU2VjdGlvbltkZXNpcmVkRXZlbnRdID0gKHBsRXZlbnRzVG9TaG93W2Rlc2lyZWRFdmVudF0uaXNTdGF0ZSA/IHN0YXRlTGV2ZWwgOiBldmVudHNMZXZlbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uUG93ZXJMZXZlbHNDaGFuZ2VkID0gKHZhbHVlLCBwb3dlckxldmVsS2V5KSA9PiB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSBjbGllbnQuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgY29uc3QgcGxFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKCdtLnJvb20ucG93ZXJfbGV2ZWxzJywgJycpO1xyXG4gICAgICAgIGxldCBwbENvbnRlbnQgPSBwbEV2ZW50ID8gKHBsRXZlbnQuZ2V0Q29udGVudCgpIHx8IHt9KSA6IHt9O1xyXG5cclxuICAgICAgICAvLyBDbG9uZSB0aGUgcG93ZXIgbGV2ZWxzIGp1c3QgaW4gY2FzZVxyXG4gICAgICAgIHBsQ29udGVudCA9IE9iamVjdC5hc3NpZ24oe30sIHBsQ29udGVudCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50c0xldmVsUHJlZml4ID0gXCJldmVudF9sZXZlbHNfXCI7XHJcblxyXG4gICAgICAgIHZhbHVlID0gcGFyc2VJbnQodmFsdWUpO1xyXG5cclxuICAgICAgICBpZiAocG93ZXJMZXZlbEtleS5zdGFydHNXaXRoKGV2ZW50c0xldmVsUHJlZml4KSkge1xyXG4gICAgICAgICAgICAvLyBkZWVwIGNvcHkgXCJldmVudHNcIiBvYmplY3QsIE9iamVjdC5hc3NpZ24gaXRzZWxmIHdvbid0IGRlZXAgY29weVxyXG4gICAgICAgICAgICBwbENvbnRlbnRbXCJldmVudHNcIl0gPSBPYmplY3QuYXNzaWduKHt9LCBwbENvbnRlbnRbXCJldmVudHNcIl0gfHwge30pO1xyXG4gICAgICAgICAgICBwbENvbnRlbnRbXCJldmVudHNcIl1bcG93ZXJMZXZlbEtleS5zbGljZShldmVudHNMZXZlbFByZWZpeC5sZW5ndGgpXSA9IHZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGtleVBhdGggPSBwb3dlckxldmVsS2V5LnNwbGl0KCcuJyk7XHJcbiAgICAgICAgICAgIGxldCBwYXJlbnRPYmo7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50T2JqID0gcGxDb250ZW50O1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBvZiBrZXlQYXRoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWN1cnJlbnRPYmpba2V5XSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRPYmpba2V5XSA9IHt9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcGFyZW50T2JqID0gY3VycmVudE9iajtcclxuICAgICAgICAgICAgICAgIGN1cnJlbnRPYmogPSBjdXJyZW50T2JqW2tleV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcGFyZW50T2JqW2tleVBhdGhba2V5UGF0aC5sZW5ndGggLSAxXV0gPSB2YWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIHBsQ29udGVudCkuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdQb3dlciBsZXZlbCByZXF1aXJlbWVudCBjaGFuZ2UgZmFpbGVkJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0Vycm9yIGNoYW5naW5nIHBvd2VyIGxldmVsIHJlcXVpcmVtZW50JyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJBbiBlcnJvciBvY2N1cnJlZCBjaGFuZ2luZyB0aGUgcm9vbSdzIHBvd2VyIGxldmVsIHJlcXVpcmVtZW50cy4gRW5zdXJlIHlvdSBoYXZlIHN1ZmZpY2llbnQgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwicGVybWlzc2lvbnMgYW5kIHRyeSBhZ2Fpbi5cIixcclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25Vc2VyUG93ZXJMZXZlbENoYW5nZWQgPSAodmFsdWUsIHBvd2VyTGV2ZWxLZXkpID0+IHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHRoaXMucHJvcHMucm9vbUlkKTtcclxuICAgICAgICBjb25zdCBwbEV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5wb3dlcl9sZXZlbHMnLCAnJyk7XHJcbiAgICAgICAgbGV0IHBsQ29udGVudCA9IHBsRXZlbnQgPyAocGxFdmVudC5nZXRDb250ZW50KCkgfHwge30pIDoge307XHJcblxyXG4gICAgICAgIC8vIENsb25lIHRoZSBwb3dlciBsZXZlbHMganVzdCBpbiBjYXNlXHJcbiAgICAgICAgcGxDb250ZW50ID0gT2JqZWN0LmFzc2lnbih7fSwgcGxDb250ZW50KTtcclxuXHJcbiAgICAgICAgLy8gcG93ZXJMZXZlbEtleSBzaG91bGQgYmUgYSB1c2VyIElEXHJcbiAgICAgICAgaWYgKCFwbENvbnRlbnRbJ3VzZXJzJ10pIHBsQ29udGVudFsndXNlcnMnXSA9IHt9O1xyXG4gICAgICAgIHBsQ29udGVudFsndXNlcnMnXVtwb3dlckxldmVsS2V5XSA9IHZhbHVlO1xyXG5cclxuICAgICAgICBjbGllbnQuc2VuZFN0YXRlRXZlbnQodGhpcy5wcm9wcy5yb29tSWQsIFwibS5yb29tLnBvd2VyX2xldmVsc1wiLCBwbENvbnRlbnQpLmNhdGNoKGUgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnUG93ZXIgbGV2ZWwgY2hhbmdlIGZhaWxlZCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvciBjaGFuZ2luZyBwb3dlciBsZXZlbCcpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiQW4gZXJyb3Igb2NjdXJyZWQgY2hhbmdpbmcgdGhlIHVzZXIncyBwb3dlciBsZXZlbC4gRW5zdXJlIHlvdSBoYXZlIHN1ZmZpY2llbnQgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwicGVybWlzc2lvbnMgYW5kIHRyeSBhZ2Fpbi5cIixcclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgUG93ZXJTZWxlY3RvciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlBvd2VyU2VsZWN0b3InKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSBjbGllbnQuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgY29uc3QgcGxFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKCdtLnJvb20ucG93ZXJfbGV2ZWxzJywgJycpO1xyXG4gICAgICAgIGNvbnN0IHBsQ29udGVudCA9IHBsRXZlbnQgPyAocGxFdmVudC5nZXRDb250ZW50KCkgfHwge30pIDoge307XHJcbiAgICAgICAgY29uc3QgY2FuQ2hhbmdlTGV2ZWxzID0gcm9vbS5jdXJyZW50U3RhdGUubWF5Q2xpZW50U2VuZFN0YXRlRXZlbnQoJ20ucm9vbS5wb3dlcl9sZXZlbHMnLCBjbGllbnQpO1xyXG5cclxuICAgICAgICBjb25zdCBwb3dlckxldmVsRGVzY3JpcHRvcnMgPSB7XHJcbiAgICAgICAgICAgIFwidXNlcnNfZGVmYXVsdFwiOiB7XHJcbiAgICAgICAgICAgICAgICBkZXNjOiBfdCgnRGVmYXVsdCByb2xlJyksXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU6IDAsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFwiZXZlbnRzX2RlZmF1bHRcIjoge1xyXG4gICAgICAgICAgICAgICAgZGVzYzogX3QoJ1NlbmQgbWVzc2FnZXMnKSxcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZTogMCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJpbnZpdGVcIjoge1xyXG4gICAgICAgICAgICAgICAgZGVzYzogX3QoJ0ludml0ZSB1c2VycycpLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiA1MCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJzdGF0ZV9kZWZhdWx0XCI6IHtcclxuICAgICAgICAgICAgICAgIGRlc2M6IF90KCdDaGFuZ2Ugc2V0dGluZ3MnKSxcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZTogNTAsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFwia2lja1wiOiB7XHJcbiAgICAgICAgICAgICAgICBkZXNjOiBfdCgnS2ljayB1c2VycycpLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiA1MCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJiYW5cIjoge1xyXG4gICAgICAgICAgICAgICAgZGVzYzogX3QoJ0JhbiB1c2VycycpLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiA1MCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJyZWRhY3RcIjoge1xyXG4gICAgICAgICAgICAgICAgZGVzYzogX3QoJ1JlbW92ZSBtZXNzYWdlcycpLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiA1MCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJub3RpZmljYXRpb25zLnJvb21cIjoge1xyXG4gICAgICAgICAgICAgICAgZGVzYzogX3QoJ05vdGlmeSBldmVyeW9uZScpLFxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiA1MCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBldmVudHNMZXZlbHMgPSBwbENvbnRlbnQuZXZlbnRzIHx8IHt9O1xyXG4gICAgICAgIGNvbnN0IHVzZXJMZXZlbHMgPSBwbENvbnRlbnQudXNlcnMgfHwge307XHJcbiAgICAgICAgY29uc3QgYmFuTGV2ZWwgPSBwYXJzZUludFdpdGhEZWZhdWx0KHBsQ29udGVudC5iYW4sIHBvd2VyTGV2ZWxEZXNjcmlwdG9ycy5iYW4uZGVmYXVsdFZhbHVlKTtcclxuICAgICAgICBjb25zdCBkZWZhdWx0VXNlckxldmVsID0gcGFyc2VJbnRXaXRoRGVmYXVsdChcclxuICAgICAgICAgICAgcGxDb250ZW50LnVzZXJzX2RlZmF1bHQsXHJcbiAgICAgICAgICAgIHBvd2VyTGV2ZWxEZXNjcmlwdG9ycy51c2Vyc19kZWZhdWx0LmRlZmF1bHRWYWx1ZSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBsZXQgY3VycmVudFVzZXJMZXZlbCA9IHVzZXJMZXZlbHNbY2xpZW50LmdldFVzZXJJZCgpXTtcclxuICAgICAgICBpZiAoY3VycmVudFVzZXJMZXZlbCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnRVc2VyTGV2ZWwgPSBkZWZhdWx0VXNlckxldmVsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fcG9wdWxhdGVEZWZhdWx0UGxFdmVudHMoXHJcbiAgICAgICAgICAgIGV2ZW50c0xldmVscyxcclxuICAgICAgICAgICAgcGFyc2VJbnRXaXRoRGVmYXVsdChwbENvbnRlbnQuc3RhdGVfZGVmYXVsdCwgcG93ZXJMZXZlbERlc2NyaXB0b3JzLnN0YXRlX2RlZmF1bHQuZGVmYXVsdFZhbHVlKSxcclxuICAgICAgICAgICAgcGFyc2VJbnRXaXRoRGVmYXVsdChwbENvbnRlbnQuZXZlbnRzX2RlZmF1bHQsIHBvd2VyTGV2ZWxEZXNjcmlwdG9ycy5ldmVudHNfZGVmYXVsdC5kZWZhdWx0VmFsdWUpLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCBwcml2aWxlZ2VkVXNlcnNTZWN0aW9uID0gPGRpdj57X3QoJ05vIHVzZXJzIGhhdmUgc3BlY2lmaWMgcHJpdmlsZWdlcyBpbiB0aGlzIHJvb20nKX08L2Rpdj47XHJcbiAgICAgICAgbGV0IG11dGVkVXNlcnNTZWN0aW9uO1xyXG4gICAgICAgIGlmIChPYmplY3Qua2V5cyh1c2VyTGV2ZWxzKS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3QgcHJpdmlsZWdlZFVzZXJzID0gW107XHJcbiAgICAgICAgICAgIGNvbnN0IG11dGVkVXNlcnMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHVzZXJMZXZlbHMpLmZvckVhY2goKHVzZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhbkNoYW5nZSA9IHVzZXJMZXZlbHNbdXNlcl0gPCBjdXJyZW50VXNlckxldmVsICYmIGNhbkNoYW5nZUxldmVscztcclxuICAgICAgICAgICAgICAgIGlmICh1c2VyTGV2ZWxzW3VzZXJdID4gZGVmYXVsdFVzZXJMZXZlbCkgeyAvLyBwcml2aWxlZ2VkXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpdmlsZWdlZFVzZXJzLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxQb3dlclNlbGVjdG9yXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dXNlckxldmVsc1t1c2VyXX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshY2FuQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e3VzZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e3VzZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3dlckxldmVsS2V5PXt1c2VyfSAvLyBXaWxsIGJlIHNlbnQgYXMgdGhlIHNlY29uZCBwYXJhbWV0ZXIgdG8gYG9uQ2hhbmdlYFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uVXNlclBvd2VyTGV2ZWxDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPixcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh1c2VyTGV2ZWxzW3VzZXJdIDwgZGVmYXVsdFVzZXJMZXZlbCkgeyAvLyBtdXRlZFxyXG4gICAgICAgICAgICAgICAgICAgIG11dGVkVXNlcnMucHVzaChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPFBvd2VyU2VsZWN0b3JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt1c2VyTGV2ZWxzW3VzZXJdfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFjYW5DaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dXNlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17dXNlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvd2VyTGV2ZWxLZXk9e3VzZXJ9IC8vIFdpbGwgYmUgc2VudCBhcyB0aGUgc2Vjb25kIHBhcmFtZXRlciB0byBgb25DaGFuZ2VgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25Vc2VyUG93ZXJMZXZlbENoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+LFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gY29tcGFyYXRvciBmb3Igc29ydGluZyBQTCB1c2VycyBsZXhpY29ncmFwaGljYWxseSBvbiBQTCBkZXNjZW5kaW5nLCBNWElEIGFzY2VuZGluZy4gKGNhc2UtaW5zZW5zaXRpdmUpXHJcbiAgICAgICAgICAgIGNvbnN0IGNvbXBhcmF0b3IgPSAoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGxEaWZmID0gdXNlckxldmVsc1tiLmtleV0gLSB1c2VyTGV2ZWxzW2Eua2V5XTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwbERpZmYgIT09IDAgPyBwbERpZmYgOiBhLmtleS50b0xvY2FsZUxvd2VyQ2FzZSgpLmxvY2FsZUNvbXBhcmUoYi5rZXkudG9Mb2NhbGVMb3dlckNhc2UoKSk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBwcml2aWxlZ2VkVXNlcnMuc29ydChjb21wYXJhdG9yKTtcclxuICAgICAgICAgICAgbXV0ZWRVc2Vycy5zb3J0KGNvbXBhcmF0b3IpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHByaXZpbGVnZWRVc2Vycy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHByaXZpbGVnZWRVc2Vyc1NlY3Rpb24gPVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uIG14X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPnsgX3QoJ1ByaXZpbGVnZWQgVXNlcnMnKSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtwcml2aWxlZ2VkVXNlcnN9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChtdXRlZFVzZXJzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgbXV0ZWRVc2Vyc1NlY3Rpb24gPVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uIG14X1NldHRpbmdzVGFiX3N1YnNlY3Rpb25UZXh0Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPnsgX3QoJ011dGVkIFVzZXJzJykgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7bXV0ZWRVc2Vyc31cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGJhbm5lZCA9IHJvb20uZ2V0TWVtYmVyc1dpdGhNZW1iZXJzaGlwKFwiYmFuXCIpO1xyXG4gICAgICAgIGxldCBiYW5uZWRVc2Vyc1NlY3Rpb247XHJcbiAgICAgICAgaWYgKGJhbm5lZC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3QgY2FuQmFuVXNlcnMgPSBjdXJyZW50VXNlckxldmVsID49IGJhbkxldmVsO1xyXG4gICAgICAgICAgICBiYW5uZWRVc2Vyc1NlY3Rpb24gPVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24gbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57IF90KCdCYW5uZWQgdXNlcnMnKSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7YmFubmVkLm1hcCgobWVtYmVyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBiYW5FdmVudCA9IG1lbWJlci5ldmVudHMubWVtYmVyLmdldENvbnRlbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbmRlciA9IHJvb20uZ2V0TWVtYmVyKG1lbWJlci5ldmVudHMubWVtYmVyLmdldFNlbmRlcigpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBiYW5uZWRCeSA9IG1lbWJlci5ldmVudHMubWVtYmVyLmdldFNlbmRlcigpOyAvLyBzdGFydCBieSBmYWxsaW5nIGJhY2sgdG8gbXhpZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbmRlcikgYmFubmVkQnkgPSBzZW5kZXIubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJhbm5lZFVzZXIga2V5PXttZW1iZXIudXNlcklkfSBjYW5VbmJhbj17Y2FuQmFuVXNlcnN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyPXttZW1iZXJ9IHJlYXNvbj17YmFuRXZlbnQucmVhc29ufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJ5PXtiYW5uZWRCeX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBwb3dlclNlbGVjdG9ycyA9IE9iamVjdC5rZXlzKHBvd2VyTGV2ZWxEZXNjcmlwdG9ycykubWFwKChrZXksIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRlc2NyaXB0b3IgPSBwb3dlckxldmVsRGVzY3JpcHRvcnNba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGtleVBhdGggPSBrZXkuc3BsaXQoJy4nKTtcclxuICAgICAgICAgICAgbGV0IGN1cnJlbnRPYmogPSBwbENvbnRlbnQ7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcHJvcCBvZiBrZXlQYXRoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudE9iaiA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50T2JqID0gY3VycmVudE9ialtwcm9wXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBwYXJzZUludFdpdGhEZWZhdWx0KGN1cnJlbnRPYmosIGRlc2NyaXB0b3IuZGVmYXVsdFZhbHVlKTtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYga2V5PXtpbmRleH0gY2xhc3NOYW1lPVwiXCI+XHJcbiAgICAgICAgICAgICAgICA8UG93ZXJTZWxlY3RvclxyXG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXtkZXNjcmlwdG9yLmRlc2N9XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJzRGVmYXVsdD17ZGVmYXVsdFVzZXJMZXZlbH1cclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IWNhbkNoYW5nZUxldmVscyB8fCBjdXJyZW50VXNlckxldmVsIDwgdmFsdWV9XHJcbiAgICAgICAgICAgICAgICAgICAgcG93ZXJMZXZlbEtleT17a2V5fSAvLyBXaWxsIGJlIHNlbnQgYXMgdGhlIHNlY29uZCBwYXJhbWV0ZXIgdG8gYG9uQ2hhbmdlYFxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblBvd2VyTGV2ZWxzQ2hhbmdlZH1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gaGlkZSB0aGUgcG93ZXIgbGV2ZWwgc2VsZWN0b3IgZm9yIGVuYWJsaW5nIEUyRUUgaWYgaXQgdGhlIHJvb20gaXMgYWxyZWFkeSBlbmNyeXB0ZWRcclxuICAgICAgICBpZiAoY2xpZW50LmlzUm9vbUVuY3J5cHRlZCh0aGlzLnByb3BzLnJvb21JZCkpIHtcclxuICAgICAgICAgICAgZGVsZXRlIGV2ZW50c0xldmVsc1tcIm0ucm9vbS5lbmNyeXB0aW9uXCJdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZXZlbnRQb3dlclNlbGVjdG9ycyA9IE9iamVjdC5rZXlzKGV2ZW50c0xldmVscykubWFwKChldmVudFR5cGUsIGkpID0+IHtcclxuICAgICAgICAgICAgbGV0IGxhYmVsID0gcGxFdmVudHNUb0xhYmVsc1tldmVudFR5cGVdO1xyXG4gICAgICAgICAgICBpZiAobGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGxhYmVsID0gX3QobGFiZWwpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGFiZWwgPSBfdChcIlNlbmQgJShldmVudFR5cGUpcyBldmVudHNcIiwge2V2ZW50VHlwZX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIlwiIGtleT17ZXZlbnRUeXBlfT5cclxuICAgICAgICAgICAgICAgICAgICA8UG93ZXJTZWxlY3RvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17bGFiZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtldmVudHNMZXZlbHNbZXZlbnRUeXBlXX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcnNEZWZhdWx0PXtkZWZhdWx0VXNlckxldmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IWNhbkNoYW5nZUxldmVscyB8fCBjdXJyZW50VXNlckxldmVsIDwgZXZlbnRzTGV2ZWxzW2V2ZW50VHlwZV19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvd2VyTGV2ZWxLZXk9e1wiZXZlbnRfbGV2ZWxzX1wiICsgZXZlbnRUeXBlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25Qb3dlckxldmVsc0NoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiIG14X1JvbGVzUm9vbVNldHRpbmdzVGFiXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX2hlYWRpbmdcIj57X3QoXCJSb2xlcyAmIFBlcm1pc3Npb25zXCIpfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAge3ByaXZpbGVnZWRVc2Vyc1NlY3Rpb259XHJcbiAgICAgICAgICAgICAgICB7bXV0ZWRVc2Vyc1NlY3Rpb259XHJcbiAgICAgICAgICAgICAgICB7YmFubmVkVXNlcnNTZWN0aW9ufVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24gbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3ViaGVhZGluZyc+e190KFwiUGVybWlzc2lvbnNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdCgnU2VsZWN0IHRoZSByb2xlcyByZXF1aXJlZCB0byBjaGFuZ2UgdmFyaW91cyBwYXJ0cyBvZiB0aGUgcm9vbScpfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICB7cG93ZXJTZWxlY3RvcnN9XHJcbiAgICAgICAgICAgICAgICAgICAge2V2ZW50UG93ZXJTZWxlY3RvcnN9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=