"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../../languageHandler");

var _MatrixClientPeg = require("../../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../.."));

var _LabelledToggleSwitch = _interopRequireDefault(require("../../../elements/LabelledToggleSwitch"));

var _SettingsStore = require("../../../../../settings/SettingsStore");

var _Modal = _interopRequireDefault(require("../../../../../Modal"));

var _QuestionDialog = _interopRequireDefault(require("../../../dialogs/QuestionDialog"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class SecurityRoomSettingsTab extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onStateEvent", e => {
      const refreshWhenTypes = ['m.room.join_rules', 'm.room.guest_access', 'm.room.history_visibility', 'm.room.encryption'];
      if (refreshWhenTypes.includes(e.getType())) this.forceUpdate();
    });
    (0, _defineProperty2.default)(this, "_onEncryptionChange", e => {
      _Modal.default.createTrackedDialog('Enable encryption', '', _QuestionDialog.default, {
        title: (0, _languageHandler._t)('Enable encryption?'),
        description: (0, _languageHandler._t)("Once enabled, encryption for a room cannot be disabled. Messages sent in an encrypted " + "room cannot be seen by the server, only by the participants of the room. Enabling encryption " + "may prevent many bots and bridges from working correctly. <a>Learn more about encryption.</a>", {}, {
          'a': sub => {
            return _react.default.createElement("a", {
              rel: "noreferrer noopener",
              target: "_blank",
              href: "https://about.riot.im/help#end-to-end-encryption"
            }, sub);
          }
        }),
        onFinished: confirm => {
          if (!confirm) {
            this.setState({
              encrypted: false
            });
            return;
          }

          const beforeEncrypted = this.state.encrypted;
          this.setState({
            encrypted: true
          });

          _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(this.props.roomId, "m.room.encryption", {
            algorithm: "m.megolm.v1.aes-sha2"
          }).catch(e => {
            console.error(e);
            this.setState({
              encrypted: beforeEncrypted
            });
          });
        }
      });
    });
    (0, _defineProperty2.default)(this, "_fixGuestAccess", e => {
      e.preventDefault();
      e.stopPropagation();
      const joinRule = "invite";
      const guestAccess = "can_join";
      const beforeJoinRule = this.state.joinRule;
      const beforeGuestAccess = this.state.guestAccess;
      this.setState({
        joinRule,
        guestAccess
      });

      const client = _MatrixClientPeg.MatrixClientPeg.get();

      client.sendStateEvent(this.props.roomId, "m.room.join_rules", {
        join_rule: joinRule
      }, "").catch(e => {
        console.error(e);
        this.setState({
          joinRule: beforeJoinRule
        });
      });
      client.sendStateEvent(this.props.roomId, "m.room.guest_access", {
        guest_access: guestAccess
      }, "").catch(e => {
        console.error(e);
        this.setState({
          guestAccess: beforeGuestAccess
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onRoomAccessRadioToggle", ev => {
      //                         join_rule
      //                      INVITE  |  PUBLIC
      //        ----------------------+----------------
      // guest  CAN_JOIN   | inv_only | pub_with_guest
      // access ----------------------+----------------
      //        FORBIDDEN  | inv_only | pub_no_guest
      //        ----------------------+----------------
      // we always set guests can_join here as it makes no sense to have
      // an invite-only room that guests can't join.  If you explicitly
      // invite them, you clearly want them to join, whether they're a
      // guest or not.  In practice, guest_access should probably have
      // been implemented as part of the join_rules enum.
      let joinRule = "invite";
      let guestAccess = "can_join";

      switch (ev.target.value) {
        case "invite_only":
          // no change - use defaults above
          break;

        case "public_no_guests":
          joinRule = "public";
          guestAccess = "forbidden";
          break;

        case "public_with_guests":
          joinRule = "public";
          guestAccess = "can_join";
          break;
      }

      const beforeJoinRule = this.state.joinRule;
      const beforeGuestAccess = this.state.guestAccess;
      this.setState({
        joinRule,
        guestAccess
      });

      const client = _MatrixClientPeg.MatrixClientPeg.get();

      client.sendStateEvent(this.props.roomId, "m.room.join_rules", {
        join_rule: joinRule
      }, "").catch(e => {
        console.error(e);
        this.setState({
          joinRule: beforeJoinRule
        });
      });
      client.sendStateEvent(this.props.roomId, "m.room.guest_access", {
        guest_access: guestAccess
      }, "").catch(e => {
        console.error(e);
        this.setState({
          guestAccess: beforeGuestAccess
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onHistoryRadioToggle", ev => {
      const beforeHistory = this.state.history;
      this.setState({
        history: ev.target.value
      });

      _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(this.props.roomId, "m.room.history_visibility", {
        history_visibility: ev.target.value
      }, "").catch(e => {
        console.error(e);
        this.setState({
          history: beforeHistory
        });
      });
    });
    (0, _defineProperty2.default)(this, "_updateBlacklistDevicesFlag", checked => {
      _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.roomId).setBlacklistUnverifiedDevices(checked);
    });
    this.state = {
      joinRule: "invite",
      guestAccess: "can_join",
      history: "shared",
      hasAliases: false,
      encrypted: false
    };
  } // TODO: [REACT-WARNING] Move this to constructor


  async UNSAFE_componentWillMount()
  /*: void*/
  {
    // eslint-disable-line camelcase
    _MatrixClientPeg.MatrixClientPeg.get().on("RoomState.events", this._onStateEvent);

    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.roomId);

    const state = room.currentState;

    const joinRule = this._pullContentPropertyFromEvent(state.getStateEvents("m.room.join_rules", ""), 'join_rule', 'invite');

    const guestAccess = this._pullContentPropertyFromEvent(state.getStateEvents("m.room.guest_access", ""), 'guest_access', 'forbidden');

    const history = this._pullContentPropertyFromEvent(state.getStateEvents("m.room.history_visibility", ""), 'history_visibility', 'shared');

    const encrypted = _MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(this.props.roomId);

    this.setState({
      joinRule,
      guestAccess,
      history,
      encrypted
    });
    const hasAliases = await this._hasAliases();
    this.setState({
      hasAliases
    });
  }

  _pullContentPropertyFromEvent(event, key, defaultValue) {
    if (!event || !event.getContent()) return defaultValue;
    return event.getContent()[key] || defaultValue;
  }

  componentWillUnmount()
  /*: void*/
  {
    _MatrixClientPeg.MatrixClientPeg.get().removeListener("RoomState.events", this._onStateEvent);
  }

  async _hasAliases() {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (await cli.doesServerSupportUnstableFeature("org.matrix.msc2432")) {
      const response = await cli.unstableGetLocalAliases(this.props.roomId);
      const localAliases = response.aliases;
      return Array.isArray(localAliases) && localAliases.length !== 0;
    } else {
      const room = cli.getRoom(this.props.roomId);
      const aliasEvents = room.currentState.getStateEvents("m.room.aliases") || [];
      const hasAliases = !!aliasEvents.find(ev => (ev.getContent().aliases || []).length > 0);
      return hasAliases;
    }
  }

  _renderRoomAccess() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const room = client.getRoom(this.props.roomId);
    const joinRule = this.state.joinRule;
    const guestAccess = this.state.guestAccess;
    const canChangeAccess = room.currentState.mayClientSendStateEvent("m.room.join_rules", client) && room.currentState.mayClientSendStateEvent("m.room.guest_access", client);
    let guestWarning = null;

    if (joinRule !== 'public' && guestAccess === 'forbidden') {
      guestWarning = _react.default.createElement("div", {
        className: "mx_SecurityRoomSettingsTab_warning"
      }, _react.default.createElement("img", {
        src: require("../../../../../../res/img/warning.svg"),
        width: 15,
        height: 15
      }), _react.default.createElement("span", null, (0, _languageHandler._t)("Guests cannot join this room even if explicitly invited."), "\xA0", _react.default.createElement("a", {
        href: "",
        onClick: this._fixGuestAccess
      }, (0, _languageHandler._t)("Click here to fix"))));
    }

    let aliasWarning = null;

    if (joinRule === 'public' && !this.state.hasAliases) {
      aliasWarning = _react.default.createElement("div", {
        className: "mx_SecurityRoomSettingsTab_warning"
      }, _react.default.createElement("img", {
        src: require("../../../../../../res/img/warning.svg"),
        width: 15,
        height: 15
      }), _react.default.createElement("span", null, (0, _languageHandler._t)("To link to this room, please add an alias.")));
    }

    return _react.default.createElement("div", null, guestWarning, aliasWarning, _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "roomVis",
      value: "invite_only",
      disabled: !canChangeAccess,
      onChange: this._onRoomAccessRadioToggle,
      checked: joinRule !== "public"
    }), (0, _languageHandler._t)('Only people who have been invited')), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "roomVis",
      value: "public_no_guests",
      disabled: !canChangeAccess,
      onChange: this._onRoomAccessRadioToggle,
      checked: joinRule === "public" && guestAccess !== "can_join"
    }), (0, _languageHandler._t)('Anyone who knows the room\'s link, apart from guests')), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "roomVis",
      value: "public_with_guests",
      disabled: !canChangeAccess,
      onChange: this._onRoomAccessRadioToggle,
      checked: joinRule === "public" && guestAccess === "can_join"
    }), (0, _languageHandler._t)("Anyone who knows the room's link, including guests")));
  }

  _renderHistory() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const history = this.state.history;
    const state = client.getRoom(this.props.roomId).currentState;
    const canChangeHistory = state.mayClientSendStateEvent('m.room.history_visibility', client);
    return _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)('Changes to who can read history will only apply to future messages in this room. ' + 'The visibility of existing history will be unchanged.')), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "historyVis",
      value: "world_readable",
      disabled: !canChangeHistory,
      checked: history === "world_readable",
      onChange: this._onHistoryRadioToggle
    }), (0, _languageHandler._t)("Anyone")), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "historyVis",
      value: "shared",
      disabled: !canChangeHistory,
      checked: history === "shared",
      onChange: this._onHistoryRadioToggle
    }), (0, _languageHandler._t)('Members only (since the point in time of selecting this option)')), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "historyVis",
      value: "invited",
      disabled: !canChangeHistory,
      checked: history === "invited",
      onChange: this._onHistoryRadioToggle
    }), (0, _languageHandler._t)('Members only (since they were invited)')), _react.default.createElement("label", null, _react.default.createElement("input", {
      type: "radio",
      name: "historyVis",
      value: "joined",
      disabled: !canChangeHistory,
      checked: history === "joined",
      onChange: this._onHistoryRadioToggle
    }), (0, _languageHandler._t)('Members only (since they joined)')));
  }

  render() {
    const SettingsFlag = sdk.getComponent("elements.SettingsFlag");

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const room = client.getRoom(this.props.roomId);
    const isEncrypted = this.state.encrypted;
    const hasEncryptionPermission = room.currentState.mayClientSendStateEvent("m.room.encryption", client);
    const canEnableEncryption = !isEncrypted && hasEncryptionPermission;
    let encryptionSettings = null;

    if (isEncrypted) {
      encryptionSettings = _react.default.createElement(SettingsFlag, {
        name: "blacklistUnverifiedDevices",
        level: _SettingsStore.SettingLevel.ROOM_DEVICE,
        onChange: this._updateBlacklistDevicesFlag,
        roomId: this.props.roomId
      });
    }

    return _react.default.createElement("div", {
      className: "mx_SettingsTab mx_SecurityRoomSettingsTab"
    }, _react.default.createElement("div", {
      className: "mx_SettingsTab_heading"
    }, (0, _languageHandler._t)("Security & Privacy")), _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Encryption")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_SecurityRoomSettingsTab_encryptionSection"
    }, _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, _react.default.createElement("span", null, (0, _languageHandler._t)("Once enabled, encryption cannot be disabled."))), _react.default.createElement(_LabelledToggleSwitch.default, {
      value: isEncrypted,
      onChange: this._onEncryptionChange,
      label: (0, _languageHandler._t)("Encrypted"),
      disabled: !canEnableEncryption
    })), encryptionSettings), _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Who can access this room?")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
    }, this._renderRoomAccess()), _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Who can read history?")), _react.default.createElement("div", {
      className: "mx_SettingsTab_section mx_SettingsTab_subsectionText"
    }, this._renderHistory()));
  }

}

exports.default = SecurityRoomSettingsTab;
(0, _defineProperty2.default)(SecurityRoomSettingsTab, "propTypes", {
  roomId: _propTypes.default.string.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL3RhYnMvcm9vbS9TZWN1cml0eVJvb21TZXR0aW5nc1RhYi5qcyJdLCJuYW1lcyI6WyJTZWN1cml0eVJvb21TZXR0aW5nc1RhYiIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJlIiwicmVmcmVzaFdoZW5UeXBlcyIsImluY2x1ZGVzIiwiZ2V0VHlwZSIsImZvcmNlVXBkYXRlIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwiUXVlc3Rpb25EaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwic3ViIiwib25GaW5pc2hlZCIsImNvbmZpcm0iLCJzZXRTdGF0ZSIsImVuY3J5cHRlZCIsImJlZm9yZUVuY3J5cHRlZCIsInN0YXRlIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwic2VuZFN0YXRlRXZlbnQiLCJwcm9wcyIsInJvb21JZCIsImFsZ29yaXRobSIsImNhdGNoIiwiY29uc29sZSIsImVycm9yIiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJqb2luUnVsZSIsImd1ZXN0QWNjZXNzIiwiYmVmb3JlSm9pblJ1bGUiLCJiZWZvcmVHdWVzdEFjY2VzcyIsImNsaWVudCIsImpvaW5fcnVsZSIsImd1ZXN0X2FjY2VzcyIsImV2IiwidGFyZ2V0IiwidmFsdWUiLCJiZWZvcmVIaXN0b3J5IiwiaGlzdG9yeSIsImhpc3RvcnlfdmlzaWJpbGl0eSIsImNoZWNrZWQiLCJnZXRSb29tIiwic2V0QmxhY2tsaXN0VW52ZXJpZmllZERldmljZXMiLCJoYXNBbGlhc2VzIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIm9uIiwiX29uU3RhdGVFdmVudCIsInJvb20iLCJjdXJyZW50U3RhdGUiLCJfcHVsbENvbnRlbnRQcm9wZXJ0eUZyb21FdmVudCIsImdldFN0YXRlRXZlbnRzIiwiaXNSb29tRW5jcnlwdGVkIiwiX2hhc0FsaWFzZXMiLCJldmVudCIsImtleSIsImRlZmF1bHRWYWx1ZSIsImdldENvbnRlbnQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUxpc3RlbmVyIiwiY2xpIiwiZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUiLCJyZXNwb25zZSIsInVuc3RhYmxlR2V0TG9jYWxBbGlhc2VzIiwibG9jYWxBbGlhc2VzIiwiYWxpYXNlcyIsIkFycmF5IiwiaXNBcnJheSIsImxlbmd0aCIsImFsaWFzRXZlbnRzIiwiZmluZCIsIl9yZW5kZXJSb29tQWNjZXNzIiwiY2FuQ2hhbmdlQWNjZXNzIiwibWF5Q2xpZW50U2VuZFN0YXRlRXZlbnQiLCJndWVzdFdhcm5pbmciLCJyZXF1aXJlIiwiX2ZpeEd1ZXN0QWNjZXNzIiwiYWxpYXNXYXJuaW5nIiwiX29uUm9vbUFjY2Vzc1JhZGlvVG9nZ2xlIiwiX3JlbmRlckhpc3RvcnkiLCJjYW5DaGFuZ2VIaXN0b3J5IiwiX29uSGlzdG9yeVJhZGlvVG9nZ2xlIiwicmVuZGVyIiwiU2V0dGluZ3NGbGFnIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiaXNFbmNyeXB0ZWQiLCJoYXNFbmNyeXB0aW9uUGVybWlzc2lvbiIsImNhbkVuYWJsZUVuY3J5cHRpb24iLCJlbmNyeXB0aW9uU2V0dGluZ3MiLCJTZXR0aW5nTGV2ZWwiLCJST09NX0RFVklDRSIsIl91cGRhdGVCbGFja2xpc3REZXZpY2VzRmxhZyIsIl9vbkVuY3J5cHRpb25DaGFuZ2UiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXhCQTs7Ozs7Ozs7Ozs7Ozs7O0FBMEJlLE1BQU1BLHVCQUFOLFNBQXNDQyxlQUFNQyxTQUE1QyxDQUFzRDtBQUtqRUMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSx5REFpREdDLENBQUQsSUFBTztBQUNuQixZQUFNQyxnQkFBZ0IsR0FBRyxDQUNyQixtQkFEcUIsRUFFckIscUJBRnFCLEVBR3JCLDJCQUhxQixFQUlyQixtQkFKcUIsQ0FBekI7QUFNQSxVQUFJQSxnQkFBZ0IsQ0FBQ0MsUUFBakIsQ0FBMEJGLENBQUMsQ0FBQ0csT0FBRixFQUExQixDQUFKLEVBQTRDLEtBQUtDLFdBQUw7QUFDL0MsS0F6RGE7QUFBQSwrREEyRFNKLENBQUQsSUFBTztBQUN6QksscUJBQU1DLG1CQUFOLENBQTBCLG1CQUExQixFQUErQyxFQUEvQyxFQUFtREMsdUJBQW5ELEVBQW1FO0FBQy9EQyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsb0JBQUgsQ0FEd0Q7QUFFL0RDLFFBQUFBLFdBQVcsRUFBRSx5QkFDVCwyRkFDQSwrRkFEQSxHQUVBLCtGQUhTLEVBSVQsRUFKUyxFQUtUO0FBQ0ksZUFBTUMsR0FBRCxJQUFTO0FBQ1YsbUJBQU87QUFBRyxjQUFBLEdBQUcsRUFBQyxxQkFBUDtBQUE2QixjQUFBLE1BQU0sRUFBQyxRQUFwQztBQUNHLGNBQUEsSUFBSSxFQUFDO0FBRFIsZUFDNERBLEdBRDVELENBQVA7QUFFSDtBQUpMLFNBTFMsQ0FGa0Q7QUFjL0RDLFFBQUFBLFVBQVUsRUFBR0MsT0FBRCxJQUFhO0FBQ3JCLGNBQUksQ0FBQ0EsT0FBTCxFQUFjO0FBQ1YsaUJBQUtDLFFBQUwsQ0FBYztBQUFDQyxjQUFBQSxTQUFTLEVBQUU7QUFBWixhQUFkO0FBQ0E7QUFDSDs7QUFFRCxnQkFBTUMsZUFBZSxHQUFHLEtBQUtDLEtBQUwsQ0FBV0YsU0FBbkM7QUFDQSxlQUFLRCxRQUFMLENBQWM7QUFBQ0MsWUFBQUEsU0FBUyxFQUFFO0FBQVosV0FBZDs7QUFDQUcsMkNBQWdCQyxHQUFoQixHQUFzQkMsY0FBdEIsQ0FDSSxLQUFLQyxLQUFMLENBQVdDLE1BRGYsRUFDdUIsbUJBRHZCLEVBRUk7QUFBRUMsWUFBQUEsU0FBUyxFQUFFO0FBQWIsV0FGSixFQUdFQyxLQUhGLENBR1N2QixDQUFELElBQU87QUFDWHdCLFlBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjekIsQ0FBZDtBQUNBLGlCQUFLYSxRQUFMLENBQWM7QUFBQ0MsY0FBQUEsU0FBUyxFQUFFQztBQUFaLGFBQWQ7QUFDSCxXQU5EO0FBT0g7QUE3QjhELE9BQW5FO0FBK0JILEtBM0ZhO0FBQUEsMkRBNkZLZixDQUFELElBQU87QUFDckJBLE1BQUFBLENBQUMsQ0FBQzBCLGNBQUY7QUFDQTFCLE1BQUFBLENBQUMsQ0FBQzJCLGVBQUY7QUFFQSxZQUFNQyxRQUFRLEdBQUcsUUFBakI7QUFDQSxZQUFNQyxXQUFXLEdBQUcsVUFBcEI7QUFFQSxZQUFNQyxjQUFjLEdBQUcsS0FBS2QsS0FBTCxDQUFXWSxRQUFsQztBQUNBLFlBQU1HLGlCQUFpQixHQUFHLEtBQUtmLEtBQUwsQ0FBV2EsV0FBckM7QUFDQSxXQUFLaEIsUUFBTCxDQUFjO0FBQUNlLFFBQUFBLFFBQUQ7QUFBV0MsUUFBQUE7QUFBWCxPQUFkOztBQUVBLFlBQU1HLE1BQU0sR0FBR2YsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBYyxNQUFBQSxNQUFNLENBQUNiLGNBQVAsQ0FBc0IsS0FBS0MsS0FBTCxDQUFXQyxNQUFqQyxFQUF5QyxtQkFBekMsRUFBOEQ7QUFBQ1ksUUFBQUEsU0FBUyxFQUFFTDtBQUFaLE9BQTlELEVBQXFGLEVBQXJGLEVBQXlGTCxLQUF6RixDQUFnR3ZCLENBQUQsSUFBTztBQUNsR3dCLFFBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjekIsQ0FBZDtBQUNBLGFBQUthLFFBQUwsQ0FBYztBQUFDZSxVQUFBQSxRQUFRLEVBQUVFO0FBQVgsU0FBZDtBQUNILE9BSEQ7QUFJQUUsTUFBQUEsTUFBTSxDQUFDYixjQUFQLENBQXNCLEtBQUtDLEtBQUwsQ0FBV0MsTUFBakMsRUFBeUMscUJBQXpDLEVBQWdFO0FBQUNhLFFBQUFBLFlBQVksRUFBRUw7QUFBZixPQUFoRSxFQUE2RixFQUE3RixFQUFpR04sS0FBakcsQ0FBd0d2QixDQUFELElBQU87QUFDMUd3QixRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3pCLENBQWQ7QUFDQSxhQUFLYSxRQUFMLENBQWM7QUFBQ2dCLFVBQUFBLFdBQVcsRUFBRUU7QUFBZCxTQUFkO0FBQ0gsT0FIRDtBQUlILEtBakhhO0FBQUEsb0VBbUhjSSxFQUFELElBQVE7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBSVAsUUFBUSxHQUFHLFFBQWY7QUFDQSxVQUFJQyxXQUFXLEdBQUcsVUFBbEI7O0FBRUEsY0FBUU0sRUFBRSxDQUFDQyxNQUFILENBQVVDLEtBQWxCO0FBQ0ksYUFBSyxhQUFMO0FBQ0k7QUFDQTs7QUFDSixhQUFLLGtCQUFMO0FBQ0lULFVBQUFBLFFBQVEsR0FBRyxRQUFYO0FBQ0FDLFVBQUFBLFdBQVcsR0FBRyxXQUFkO0FBQ0E7O0FBQ0osYUFBSyxvQkFBTDtBQUNJRCxVQUFBQSxRQUFRLEdBQUcsUUFBWDtBQUNBQyxVQUFBQSxXQUFXLEdBQUcsVUFBZDtBQUNBO0FBWFI7O0FBY0EsWUFBTUMsY0FBYyxHQUFHLEtBQUtkLEtBQUwsQ0FBV1ksUUFBbEM7QUFDQSxZQUFNRyxpQkFBaUIsR0FBRyxLQUFLZixLQUFMLENBQVdhLFdBQXJDO0FBQ0EsV0FBS2hCLFFBQUwsQ0FBYztBQUFDZSxRQUFBQSxRQUFEO0FBQVdDLFFBQUFBO0FBQVgsT0FBZDs7QUFFQSxZQUFNRyxNQUFNLEdBQUdmLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQWMsTUFBQUEsTUFBTSxDQUFDYixjQUFQLENBQXNCLEtBQUtDLEtBQUwsQ0FBV0MsTUFBakMsRUFBeUMsbUJBQXpDLEVBQThEO0FBQUNZLFFBQUFBLFNBQVMsRUFBRUw7QUFBWixPQUE5RCxFQUFxRixFQUFyRixFQUF5RkwsS0FBekYsQ0FBZ0d2QixDQUFELElBQU87QUFDbEd3QixRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3pCLENBQWQ7QUFDQSxhQUFLYSxRQUFMLENBQWM7QUFBQ2UsVUFBQUEsUUFBUSxFQUFFRTtBQUFYLFNBQWQ7QUFDSCxPQUhEO0FBSUFFLE1BQUFBLE1BQU0sQ0FBQ2IsY0FBUCxDQUFzQixLQUFLQyxLQUFMLENBQVdDLE1BQWpDLEVBQXlDLHFCQUF6QyxFQUFnRTtBQUFDYSxRQUFBQSxZQUFZLEVBQUVMO0FBQWYsT0FBaEUsRUFBNkYsRUFBN0YsRUFBaUdOLEtBQWpHLENBQXdHdkIsQ0FBRCxJQUFPO0FBQzFHd0IsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWN6QixDQUFkO0FBQ0EsYUFBS2EsUUFBTCxDQUFjO0FBQUNnQixVQUFBQSxXQUFXLEVBQUVFO0FBQWQsU0FBZDtBQUNILE9BSEQ7QUFJSCxLQS9KYTtBQUFBLGlFQWlLV0ksRUFBRCxJQUFRO0FBQzVCLFlBQU1HLGFBQWEsR0FBRyxLQUFLdEIsS0FBTCxDQUFXdUIsT0FBakM7QUFDQSxXQUFLMUIsUUFBTCxDQUFjO0FBQUMwQixRQUFBQSxPQUFPLEVBQUVKLEVBQUUsQ0FBQ0MsTUFBSCxDQUFVQztBQUFwQixPQUFkOztBQUNBcEIsdUNBQWdCQyxHQUFoQixHQUFzQkMsY0FBdEIsQ0FBcUMsS0FBS0MsS0FBTCxDQUFXQyxNQUFoRCxFQUF3RCwyQkFBeEQsRUFBcUY7QUFDakZtQixRQUFBQSxrQkFBa0IsRUFBRUwsRUFBRSxDQUFDQyxNQUFILENBQVVDO0FBRG1ELE9BQXJGLEVBRUcsRUFGSCxFQUVPZCxLQUZQLENBRWN2QixDQUFELElBQU87QUFDaEJ3QixRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY3pCLENBQWQ7QUFDQSxhQUFLYSxRQUFMLENBQWM7QUFBQzBCLFVBQUFBLE9BQU8sRUFBRUQ7QUFBVixTQUFkO0FBQ0gsT0FMRDtBQU1ILEtBMUthO0FBQUEsdUVBNEtpQkcsT0FBRCxJQUFhO0FBQ3ZDeEIsdUNBQWdCQyxHQUFoQixHQUFzQndCLE9BQXRCLENBQThCLEtBQUt0QixLQUFMLENBQVdDLE1BQXpDLEVBQWlEc0IsNkJBQWpELENBQStFRixPQUEvRTtBQUNILEtBOUthO0FBR1YsU0FBS3pCLEtBQUwsR0FBYTtBQUNUWSxNQUFBQSxRQUFRLEVBQUUsUUFERDtBQUVUQyxNQUFBQSxXQUFXLEVBQUUsVUFGSjtBQUdUVSxNQUFBQSxPQUFPLEVBQUUsUUFIQTtBQUlUSyxNQUFBQSxVQUFVLEVBQUUsS0FKSDtBQUtUOUIsTUFBQUEsU0FBUyxFQUFFO0FBTEYsS0FBYjtBQU9ILEdBZmdFLENBaUJqRTs7O0FBQ0EsUUFBTStCLHlCQUFOO0FBQUE7QUFBd0M7QUFBRTtBQUN0QzVCLHFDQUFnQkMsR0FBaEIsR0FBc0I0QixFQUF0QixDQUF5QixrQkFBekIsRUFBNkMsS0FBS0MsYUFBbEQ7O0FBRUEsVUFBTUMsSUFBSSxHQUFHL0IsaUNBQWdCQyxHQUFoQixHQUFzQndCLE9BQXRCLENBQThCLEtBQUt0QixLQUFMLENBQVdDLE1BQXpDLENBQWI7O0FBQ0EsVUFBTUwsS0FBSyxHQUFHZ0MsSUFBSSxDQUFDQyxZQUFuQjs7QUFFQSxVQUFNckIsUUFBUSxHQUFHLEtBQUtzQiw2QkFBTCxDQUNibEMsS0FBSyxDQUFDbUMsY0FBTixDQUFxQixtQkFBckIsRUFBMEMsRUFBMUMsQ0FEYSxFQUViLFdBRmEsRUFHYixRQUhhLENBQWpCOztBQUtBLFVBQU10QixXQUFXLEdBQUcsS0FBS3FCLDZCQUFMLENBQ2hCbEMsS0FBSyxDQUFDbUMsY0FBTixDQUFxQixxQkFBckIsRUFBNEMsRUFBNUMsQ0FEZ0IsRUFFaEIsY0FGZ0IsRUFHaEIsV0FIZ0IsQ0FBcEI7O0FBS0EsVUFBTVosT0FBTyxHQUFHLEtBQUtXLDZCQUFMLENBQ1psQyxLQUFLLENBQUNtQyxjQUFOLENBQXFCLDJCQUFyQixFQUFrRCxFQUFsRCxDQURZLEVBRVosb0JBRlksRUFHWixRQUhZLENBQWhCOztBQUtBLFVBQU1yQyxTQUFTLEdBQUdHLGlDQUFnQkMsR0FBaEIsR0FBc0JrQyxlQUF0QixDQUFzQyxLQUFLaEMsS0FBTCxDQUFXQyxNQUFqRCxDQUFsQjs7QUFDQSxTQUFLUixRQUFMLENBQWM7QUFBQ2UsTUFBQUEsUUFBRDtBQUFXQyxNQUFBQSxXQUFYO0FBQXdCVSxNQUFBQSxPQUF4QjtBQUFpQ3pCLE1BQUFBO0FBQWpDLEtBQWQ7QUFDQSxVQUFNOEIsVUFBVSxHQUFHLE1BQU0sS0FBS1MsV0FBTCxFQUF6QjtBQUNBLFNBQUt4QyxRQUFMLENBQWM7QUFBQytCLE1BQUFBO0FBQUQsS0FBZDtBQUNIOztBQUVETSxFQUFBQSw2QkFBNkIsQ0FBQ0ksS0FBRCxFQUFRQyxHQUFSLEVBQWFDLFlBQWIsRUFBMkI7QUFDcEQsUUFBSSxDQUFDRixLQUFELElBQVUsQ0FBQ0EsS0FBSyxDQUFDRyxVQUFOLEVBQWYsRUFBbUMsT0FBT0QsWUFBUDtBQUNuQyxXQUFPRixLQUFLLENBQUNHLFVBQU4sR0FBbUJGLEdBQW5CLEtBQTJCQyxZQUFsQztBQUNIOztBQUVERSxFQUFBQSxvQkFBb0I7QUFBQTtBQUFTO0FBQ3pCekMscUNBQWdCQyxHQUFoQixHQUFzQnlDLGNBQXRCLENBQXFDLGtCQUFyQyxFQUF5RCxLQUFLWixhQUE5RDtBQUNIOztBQWlJRCxRQUFNTSxXQUFOLEdBQW9CO0FBQ2hCLFVBQU1PLEdBQUcsR0FBRzNDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxRQUFJLE1BQU0wQyxHQUFHLENBQUNDLGdDQUFKLENBQXFDLG9CQUFyQyxDQUFWLEVBQXNFO0FBQ2xFLFlBQU1DLFFBQVEsR0FBRyxNQUFNRixHQUFHLENBQUNHLHVCQUFKLENBQTRCLEtBQUszQyxLQUFMLENBQVdDLE1BQXZDLENBQXZCO0FBQ0EsWUFBTTJDLFlBQVksR0FBR0YsUUFBUSxDQUFDRyxPQUE5QjtBQUNBLGFBQU9DLEtBQUssQ0FBQ0MsT0FBTixDQUFjSCxZQUFkLEtBQStCQSxZQUFZLENBQUNJLE1BQWIsS0FBd0IsQ0FBOUQ7QUFDSCxLQUpELE1BSU87QUFDSCxZQUFNcEIsSUFBSSxHQUFHWSxHQUFHLENBQUNsQixPQUFKLENBQVksS0FBS3RCLEtBQUwsQ0FBV0MsTUFBdkIsQ0FBYjtBQUNBLFlBQU1nRCxXQUFXLEdBQUdyQixJQUFJLENBQUNDLFlBQUwsQ0FBa0JFLGNBQWxCLENBQWlDLGdCQUFqQyxLQUFzRCxFQUExRTtBQUNBLFlBQU1QLFVBQVUsR0FBRyxDQUFDLENBQUN5QixXQUFXLENBQUNDLElBQVosQ0FBa0JuQyxFQUFELElBQVEsQ0FBQ0EsRUFBRSxDQUFDc0IsVUFBSCxHQUFnQlEsT0FBaEIsSUFBMkIsRUFBNUIsRUFBZ0NHLE1BQWhDLEdBQXlDLENBQWxFLENBQXJCO0FBQ0EsYUFBT3hCLFVBQVA7QUFDSDtBQUNKOztBQUVEMkIsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsVUFBTXZDLE1BQU0sR0FBR2YsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFVBQU04QixJQUFJLEdBQUdoQixNQUFNLENBQUNVLE9BQVAsQ0FBZSxLQUFLdEIsS0FBTCxDQUFXQyxNQUExQixDQUFiO0FBQ0EsVUFBTU8sUUFBUSxHQUFHLEtBQUtaLEtBQUwsQ0FBV1ksUUFBNUI7QUFDQSxVQUFNQyxXQUFXLEdBQUcsS0FBS2IsS0FBTCxDQUFXYSxXQUEvQjtBQUVBLFVBQU0yQyxlQUFlLEdBQUd4QixJQUFJLENBQUNDLFlBQUwsQ0FBa0J3Qix1QkFBbEIsQ0FBMEMsbUJBQTFDLEVBQStEekMsTUFBL0QsS0FDakJnQixJQUFJLENBQUNDLFlBQUwsQ0FBa0J3Qix1QkFBbEIsQ0FBMEMscUJBQTFDLEVBQWlFekMsTUFBakUsQ0FEUDtBQUdBLFFBQUkwQyxZQUFZLEdBQUcsSUFBbkI7O0FBQ0EsUUFBSTlDLFFBQVEsS0FBSyxRQUFiLElBQXlCQyxXQUFXLEtBQUssV0FBN0MsRUFBMEQ7QUFDdEQ2QyxNQUFBQSxZQUFZLEdBQ1I7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLHVDQUFELENBQWpCO0FBQTRELFFBQUEsS0FBSyxFQUFFLEVBQW5FO0FBQXVFLFFBQUEsTUFBTSxFQUFFO0FBQS9FLFFBREosRUFFSSwyQ0FDSyx5QkFBRywwREFBSCxDQURMLFVBRUk7QUFBRyxRQUFBLElBQUksRUFBQyxFQUFSO0FBQVcsUUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBekIsU0FBMkMseUJBQUcsbUJBQUgsQ0FBM0MsQ0FGSixDQUZKLENBREo7QUFTSDs7QUFFRCxRQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBQ0EsUUFBSWpELFFBQVEsS0FBSyxRQUFiLElBQXlCLENBQUMsS0FBS1osS0FBTCxDQUFXNEIsVUFBekMsRUFBcUQ7QUFDakRpQyxNQUFBQSxZQUFZLEdBQ1I7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRUYsT0FBTyxDQUFDLHVDQUFELENBQWpCO0FBQTRELFFBQUEsS0FBSyxFQUFFLEVBQW5FO0FBQXVFLFFBQUEsTUFBTSxFQUFFO0FBQS9FLFFBREosRUFFSSwyQ0FDSyx5QkFBRyw0Q0FBSCxDQURMLENBRkosQ0FESjtBQVFIOztBQUVELFdBQ0ksMENBQ0tELFlBREwsRUFFS0csWUFGTCxFQUdJLDRDQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsT0FBWjtBQUFvQixNQUFBLElBQUksRUFBQyxTQUF6QjtBQUFtQyxNQUFBLEtBQUssRUFBQyxhQUF6QztBQUNPLE1BQUEsUUFBUSxFQUFFLENBQUNMLGVBRGxCO0FBRU8sTUFBQSxRQUFRLEVBQUUsS0FBS00sd0JBRnRCO0FBR08sTUFBQSxPQUFPLEVBQUVsRCxRQUFRLEtBQUs7QUFIN0IsTUFESixFQUtLLHlCQUFHLG1DQUFILENBTEwsQ0FISixFQVVJLDRDQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsT0FBWjtBQUFvQixNQUFBLElBQUksRUFBQyxTQUF6QjtBQUFtQyxNQUFBLEtBQUssRUFBQyxrQkFBekM7QUFDTyxNQUFBLFFBQVEsRUFBRSxDQUFDNEMsZUFEbEI7QUFFTyxNQUFBLFFBQVEsRUFBRSxLQUFLTSx3QkFGdEI7QUFHTyxNQUFBLE9BQU8sRUFBRWxELFFBQVEsS0FBSyxRQUFiLElBQXlCQyxXQUFXLEtBQUs7QUFIekQsTUFESixFQUtLLHlCQUFHLHNEQUFILENBTEwsQ0FWSixFQWlCSSw0Q0FDSTtBQUFPLE1BQUEsSUFBSSxFQUFDLE9BQVo7QUFBb0IsTUFBQSxJQUFJLEVBQUMsU0FBekI7QUFBbUMsTUFBQSxLQUFLLEVBQUMsb0JBQXpDO0FBQ08sTUFBQSxRQUFRLEVBQUUsQ0FBQzJDLGVBRGxCO0FBRU8sTUFBQSxRQUFRLEVBQUUsS0FBS00sd0JBRnRCO0FBR08sTUFBQSxPQUFPLEVBQUVsRCxRQUFRLEtBQUssUUFBYixJQUF5QkMsV0FBVyxLQUFLO0FBSHpELE1BREosRUFLSyx5QkFBRyxvREFBSCxDQUxMLENBakJKLENBREo7QUEyQkg7O0FBRURrRCxFQUFBQSxjQUFjLEdBQUc7QUFDYixVQUFNL0MsTUFBTSxHQUFHZixpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTXFCLE9BQU8sR0FBRyxLQUFLdkIsS0FBTCxDQUFXdUIsT0FBM0I7QUFDQSxVQUFNdkIsS0FBSyxHQUFHZ0IsTUFBTSxDQUFDVSxPQUFQLENBQWUsS0FBS3RCLEtBQUwsQ0FBV0MsTUFBMUIsRUFBa0M0QixZQUFoRDtBQUNBLFVBQU0rQixnQkFBZ0IsR0FBR2hFLEtBQUssQ0FBQ3lELHVCQUFOLENBQThCLDJCQUE5QixFQUEyRHpDLE1BQTNELENBQXpCO0FBRUEsV0FDSSwwQ0FDSSwwQ0FDSyx5QkFBRyxzRkFDQSx1REFESCxDQURMLENBREosRUFLSSw0Q0FDSTtBQUFPLE1BQUEsSUFBSSxFQUFDLE9BQVo7QUFBb0IsTUFBQSxJQUFJLEVBQUMsWUFBekI7QUFBc0MsTUFBQSxLQUFLLEVBQUMsZ0JBQTVDO0FBQ08sTUFBQSxRQUFRLEVBQUUsQ0FBQ2dELGdCQURsQjtBQUVPLE1BQUEsT0FBTyxFQUFFekMsT0FBTyxLQUFLLGdCQUY1QjtBQUdPLE1BQUEsUUFBUSxFQUFFLEtBQUswQztBQUh0QixNQURKLEVBS0sseUJBQUcsUUFBSCxDQUxMLENBTEosRUFZSSw0Q0FDSTtBQUFPLE1BQUEsSUFBSSxFQUFDLE9BQVo7QUFBb0IsTUFBQSxJQUFJLEVBQUMsWUFBekI7QUFBc0MsTUFBQSxLQUFLLEVBQUMsUUFBNUM7QUFDTyxNQUFBLFFBQVEsRUFBRSxDQUFDRCxnQkFEbEI7QUFFTyxNQUFBLE9BQU8sRUFBRXpDLE9BQU8sS0FBSyxRQUY1QjtBQUdPLE1BQUEsUUFBUSxFQUFFLEtBQUswQztBQUh0QixNQURKLEVBS0sseUJBQUcsaUVBQUgsQ0FMTCxDQVpKLEVBbUJJLDRDQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsT0FBWjtBQUFvQixNQUFBLElBQUksRUFBQyxZQUF6QjtBQUFzQyxNQUFBLEtBQUssRUFBQyxTQUE1QztBQUNPLE1BQUEsUUFBUSxFQUFFLENBQUNELGdCQURsQjtBQUVPLE1BQUEsT0FBTyxFQUFFekMsT0FBTyxLQUFLLFNBRjVCO0FBR08sTUFBQSxRQUFRLEVBQUUsS0FBSzBDO0FBSHRCLE1BREosRUFLSyx5QkFBRyx3Q0FBSCxDQUxMLENBbkJKLEVBMEJJLDRDQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsT0FBWjtBQUFvQixNQUFBLElBQUksRUFBQyxZQUF6QjtBQUFzQyxNQUFBLEtBQUssRUFBQyxRQUE1QztBQUNPLE1BQUEsUUFBUSxFQUFFLENBQUNELGdCQURsQjtBQUVPLE1BQUEsT0FBTyxFQUFFekMsT0FBTyxLQUFLLFFBRjVCO0FBR08sTUFBQSxRQUFRLEVBQUUsS0FBSzBDO0FBSHRCLE1BREosRUFLSyx5QkFBRyxrQ0FBSCxDQUxMLENBMUJKLENBREo7QUFvQ0g7O0FBRURDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLFlBQVksR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFyQjs7QUFFQSxVQUFNckQsTUFBTSxHQUFHZixpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTThCLElBQUksR0FBR2hCLE1BQU0sQ0FBQ1UsT0FBUCxDQUFlLEtBQUt0QixLQUFMLENBQVdDLE1BQTFCLENBQWI7QUFDQSxVQUFNaUUsV0FBVyxHQUFHLEtBQUt0RSxLQUFMLENBQVdGLFNBQS9CO0FBQ0EsVUFBTXlFLHVCQUF1QixHQUFHdkMsSUFBSSxDQUFDQyxZQUFMLENBQWtCd0IsdUJBQWxCLENBQTBDLG1CQUExQyxFQUErRHpDLE1BQS9ELENBQWhDO0FBQ0EsVUFBTXdELG1CQUFtQixHQUFHLENBQUNGLFdBQUQsSUFBZ0JDLHVCQUE1QztBQUVBLFFBQUlFLGtCQUFrQixHQUFHLElBQXpCOztBQUNBLFFBQUlILFdBQUosRUFBaUI7QUFDYkcsTUFBQUEsa0JBQWtCLEdBQUcsNkJBQUMsWUFBRDtBQUFjLFFBQUEsSUFBSSxFQUFDLDRCQUFuQjtBQUFnRCxRQUFBLEtBQUssRUFBRUMsNEJBQWFDLFdBQXBFO0FBQ2MsUUFBQSxRQUFRLEVBQUUsS0FBS0MsMkJBRDdCO0FBRWMsUUFBQSxNQUFNLEVBQUUsS0FBS3hFLEtBQUwsQ0FBV0M7QUFGakMsUUFBckI7QUFHSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF5Qyx5QkFBRyxvQkFBSCxDQUF6QyxDQURKLEVBR0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxZQUFILENBQTdDLENBSEosRUFJSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSwwQ0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSwyQ0FBTyx5QkFBRyw4Q0FBSCxDQUFQLENBREosQ0FESixFQUlJLDZCQUFDLDZCQUFEO0FBQXNCLE1BQUEsS0FBSyxFQUFFaUUsV0FBN0I7QUFBMEMsTUFBQSxRQUFRLEVBQUUsS0FBS08sbUJBQXpEO0FBQ3NCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLFdBQUgsQ0FEN0I7QUFDOEMsTUFBQSxRQUFRLEVBQUUsQ0FBQ0w7QUFEekQsTUFKSixDQURKLEVBUUtDLGtCQVJMLENBSkosRUFlSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQTZDLHlCQUFHLDJCQUFILENBQTdDLENBZkosRUFnQkk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ssS0FBS2xCLGlCQUFMLEVBREwsQ0FoQkosRUFvQkk7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyx1QkFBSCxDQUE3QyxDQXBCSixFQXFCSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLUSxjQUFMLEVBREwsQ0FyQkosQ0FESjtBQTJCSDs7QUF6VmdFOzs7OEJBQWhEbkYsdUIsZUFDRTtBQUNmeUIsRUFBQUEsTUFBTSxFQUFFeUUsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRFYsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uLy4uLy4uXCI7XHJcbmltcG9ydCBMYWJlbGxlZFRvZ2dsZVN3aXRjaCBmcm9tIFwiLi4vLi4vLi4vZWxlbWVudHMvTGFiZWxsZWRUb2dnbGVTd2l0Y2hcIjtcclxuaW1wb3J0IHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vTW9kYWxcIjtcclxuaW1wb3J0IFF1ZXN0aW9uRGlhbG9nIGZyb20gXCIuLi8uLi8uLi9kaWFsb2dzL1F1ZXN0aW9uRGlhbG9nXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZWN1cml0eVJvb21TZXR0aW5nc1RhYiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBqb2luUnVsZTogXCJpbnZpdGVcIixcclxuICAgICAgICAgICAgZ3Vlc3RBY2Nlc3M6IFwiY2FuX2pvaW5cIixcclxuICAgICAgICAgICAgaGlzdG9yeTogXCJzaGFyZWRcIixcclxuICAgICAgICAgICAgaGFzQWxpYXNlczogZmFsc2UsXHJcbiAgICAgICAgICAgIGVuY3J5cHRlZDogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gTW92ZSB0aGlzIHRvIGNvbnN0cnVjdG9yXHJcbiAgICBhc3luYyBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50KCk6IHZvaWQgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbihcIlJvb21TdGF0ZS5ldmVudHNcIiwgdGhpcy5fb25TdGF0ZUV2ZW50KTtcclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHRoaXMucHJvcHMucm9vbUlkKTtcclxuICAgICAgICBjb25zdCBzdGF0ZSA9IHJvb20uY3VycmVudFN0YXRlO1xyXG5cclxuICAgICAgICBjb25zdCBqb2luUnVsZSA9IHRoaXMuX3B1bGxDb250ZW50UHJvcGVydHlGcm9tRXZlbnQoXHJcbiAgICAgICAgICAgIHN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLmpvaW5fcnVsZXNcIiwgXCJcIiksXHJcbiAgICAgICAgICAgICdqb2luX3J1bGUnLFxyXG4gICAgICAgICAgICAnaW52aXRlJyxcclxuICAgICAgICApO1xyXG4gICAgICAgIGNvbnN0IGd1ZXN0QWNjZXNzID0gdGhpcy5fcHVsbENvbnRlbnRQcm9wZXJ0eUZyb21FdmVudChcclxuICAgICAgICAgICAgc3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20uZ3Vlc3RfYWNjZXNzXCIsIFwiXCIpLFxyXG4gICAgICAgICAgICAnZ3Vlc3RfYWNjZXNzJyxcclxuICAgICAgICAgICAgJ2ZvcmJpZGRlbicsXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zdCBoaXN0b3J5ID0gdGhpcy5fcHVsbENvbnRlbnRQcm9wZXJ0eUZyb21FdmVudChcclxuICAgICAgICAgICAgc3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20uaGlzdG9yeV92aXNpYmlsaXR5XCIsIFwiXCIpLFxyXG4gICAgICAgICAgICAnaGlzdG9yeV92aXNpYmlsaXR5JyxcclxuICAgICAgICAgICAgJ3NoYXJlZCcsXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zdCBlbmNyeXB0ZWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNSb29tRW5jcnlwdGVkKHRoaXMucHJvcHMucm9vbUlkKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtqb2luUnVsZSwgZ3Vlc3RBY2Nlc3MsIGhpc3RvcnksIGVuY3J5cHRlZH0pO1xyXG4gICAgICAgIGNvbnN0IGhhc0FsaWFzZXMgPSBhd2FpdCB0aGlzLl9oYXNBbGlhc2VzKCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aGFzQWxpYXNlc30pO1xyXG4gICAgfVxyXG5cclxuICAgIF9wdWxsQ29udGVudFByb3BlcnR5RnJvbUV2ZW50KGV2ZW50LCBrZXksIGRlZmF1bHRWYWx1ZSkge1xyXG4gICAgICAgIGlmICghZXZlbnQgfHwgIWV2ZW50LmdldENvbnRlbnQoKSkgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcclxuICAgICAgICByZXR1cm4gZXZlbnQuZ2V0Q29udGVudCgpW2tleV0gfHwgZGVmYXVsdFZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCk6IHZvaWQge1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcIlJvb21TdGF0ZS5ldmVudHNcIiwgdGhpcy5fb25TdGF0ZUV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25TdGF0ZUV2ZW50ID0gKGUpID0+IHtcclxuICAgICAgICBjb25zdCByZWZyZXNoV2hlblR5cGVzID0gW1xyXG4gICAgICAgICAgICAnbS5yb29tLmpvaW5fcnVsZXMnLFxyXG4gICAgICAgICAgICAnbS5yb29tLmd1ZXN0X2FjY2VzcycsXHJcbiAgICAgICAgICAgICdtLnJvb20uaGlzdG9yeV92aXNpYmlsaXR5JyxcclxuICAgICAgICAgICAgJ20ucm9vbS5lbmNyeXB0aW9uJyxcclxuICAgICAgICBdO1xyXG4gICAgICAgIGlmIChyZWZyZXNoV2hlblR5cGVzLmluY2x1ZGVzKGUuZ2V0VHlwZSgpKSkgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25FbmNyeXB0aW9uQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdFbmFibGUgZW5jcnlwdGlvbicsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoJ0VuYWJsZSBlbmNyeXB0aW9uPycpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXHJcbiAgICAgICAgICAgICAgICBcIk9uY2UgZW5hYmxlZCwgZW5jcnlwdGlvbiBmb3IgYSByb29tIGNhbm5vdCBiZSBkaXNhYmxlZC4gTWVzc2FnZXMgc2VudCBpbiBhbiBlbmNyeXB0ZWQgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJyb29tIGNhbm5vdCBiZSBzZWVuIGJ5IHRoZSBzZXJ2ZXIsIG9ubHkgYnkgdGhlIHBhcnRpY2lwYW50cyBvZiB0aGUgcm9vbS4gRW5hYmxpbmcgZW5jcnlwdGlvbiBcIiArXHJcbiAgICAgICAgICAgICAgICBcIm1heSBwcmV2ZW50IG1hbnkgYm90cyBhbmQgYnJpZGdlcyBmcm9tIHdvcmtpbmcgY29ycmVjdGx5LiA8YT5MZWFybiBtb3JlIGFib3V0IGVuY3J5cHRpb24uPC9hPlwiLFxyXG4gICAgICAgICAgICAgICAge30sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ2EnOiAoc3ViKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8YSByZWw9J25vcmVmZXJyZXIgbm9vcGVuZXInIHRhcmdldD0nX2JsYW5rJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZj0naHR0cHM6Ly9hYm91dC5yaW90LmltL2hlbHAjZW5kLXRvLWVuZC1lbmNyeXB0aW9uJz57c3VifTwvYT47XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IChjb25maXJtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWNvbmZpcm0pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtlbmNyeXB0ZWQ6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGJlZm9yZUVuY3J5cHRlZCA9IHRoaXMuc3RhdGUuZW5jcnlwdGVkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZW5jcnlwdGVkOiB0cnVlfSk7XHJcbiAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2VuZFN0YXRlRXZlbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5yb29tSWQsIFwibS5yb29tLmVuY3J5cHRpb25cIixcclxuICAgICAgICAgICAgICAgICAgICB7IGFsZ29yaXRobTogXCJtLm1lZ29sbS52MS5hZXMtc2hhMlwiIH0sXHJcbiAgICAgICAgICAgICAgICApLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtlbmNyeXB0ZWQ6IGJlZm9yZUVuY3J5cHRlZH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9maXhHdWVzdEFjY2VzcyA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGpvaW5SdWxlID0gXCJpbnZpdGVcIjtcclxuICAgICAgICBjb25zdCBndWVzdEFjY2VzcyA9IFwiY2FuX2pvaW5cIjtcclxuXHJcbiAgICAgICAgY29uc3QgYmVmb3JlSm9pblJ1bGUgPSB0aGlzLnN0YXRlLmpvaW5SdWxlO1xyXG4gICAgICAgIGNvbnN0IGJlZm9yZUd1ZXN0QWNjZXNzID0gdGhpcy5zdGF0ZS5ndWVzdEFjY2VzcztcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtqb2luUnVsZSwgZ3Vlc3RBY2Nlc3N9KTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uam9pbl9ydWxlc1wiLCB7am9pbl9ydWxlOiBqb2luUnVsZX0sIFwiXCIpLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2pvaW5SdWxlOiBiZWZvcmVKb2luUnVsZX0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uZ3Vlc3RfYWNjZXNzXCIsIHtndWVzdF9hY2Nlc3M6IGd1ZXN0QWNjZXNzfSwgXCJcIikuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Z3Vlc3RBY2Nlc3M6IGJlZm9yZUd1ZXN0QWNjZXNzfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblJvb21BY2Nlc3NSYWRpb1RvZ2dsZSA9IChldikgPT4ge1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGpvaW5fcnVsZVxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgIElOVklURSAgfCAgUFVCTElDXHJcbiAgICAgICAgLy8gICAgICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIC8vIGd1ZXN0ICBDQU5fSk9JTiAgIHwgaW52X29ubHkgfCBwdWJfd2l0aF9ndWVzdFxyXG4gICAgICAgIC8vIGFjY2VzcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAgICAvLyAgICAgICAgRk9SQklEREVOICB8IGludl9vbmx5IHwgcHViX25vX2d1ZXN0XHJcbiAgICAgICAgLy8gICAgICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgICAgICAvLyB3ZSBhbHdheXMgc2V0IGd1ZXN0cyBjYW5fam9pbiBoZXJlIGFzIGl0IG1ha2VzIG5vIHNlbnNlIHRvIGhhdmVcclxuICAgICAgICAvLyBhbiBpbnZpdGUtb25seSByb29tIHRoYXQgZ3Vlc3RzIGNhbid0IGpvaW4uICBJZiB5b3UgZXhwbGljaXRseVxyXG4gICAgICAgIC8vIGludml0ZSB0aGVtLCB5b3UgY2xlYXJseSB3YW50IHRoZW0gdG8gam9pbiwgd2hldGhlciB0aGV5J3JlIGFcclxuICAgICAgICAvLyBndWVzdCBvciBub3QuICBJbiBwcmFjdGljZSwgZ3Vlc3RfYWNjZXNzIHNob3VsZCBwcm9iYWJseSBoYXZlXHJcbiAgICAgICAgLy8gYmVlbiBpbXBsZW1lbnRlZCBhcyBwYXJ0IG9mIHRoZSBqb2luX3J1bGVzIGVudW0uXHJcbiAgICAgICAgbGV0IGpvaW5SdWxlID0gXCJpbnZpdGVcIjtcclxuICAgICAgICBsZXQgZ3Vlc3RBY2Nlc3MgPSBcImNhbl9qb2luXCI7XHJcblxyXG4gICAgICAgIHN3aXRjaCAoZXYudGFyZ2V0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJpbnZpdGVfb25seVwiOlxyXG4gICAgICAgICAgICAgICAgLy8gbm8gY2hhbmdlIC0gdXNlIGRlZmF1bHRzIGFib3ZlXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcInB1YmxpY19ub19ndWVzdHNcIjpcclxuICAgICAgICAgICAgICAgIGpvaW5SdWxlID0gXCJwdWJsaWNcIjtcclxuICAgICAgICAgICAgICAgIGd1ZXN0QWNjZXNzID0gXCJmb3JiaWRkZW5cIjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwicHVibGljX3dpdGhfZ3Vlc3RzXCI6XHJcbiAgICAgICAgICAgICAgICBqb2luUnVsZSA9IFwicHVibGljXCI7XHJcbiAgICAgICAgICAgICAgICBndWVzdEFjY2VzcyA9IFwiY2FuX2pvaW5cIjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgYmVmb3JlSm9pblJ1bGUgPSB0aGlzLnN0YXRlLmpvaW5SdWxlO1xyXG4gICAgICAgIGNvbnN0IGJlZm9yZUd1ZXN0QWNjZXNzID0gdGhpcy5zdGF0ZS5ndWVzdEFjY2VzcztcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtqb2luUnVsZSwgZ3Vlc3RBY2Nlc3N9KTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uam9pbl9ydWxlc1wiLCB7am9pbl9ydWxlOiBqb2luUnVsZX0sIFwiXCIpLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2pvaW5SdWxlOiBiZWZvcmVKb2luUnVsZX0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uZ3Vlc3RfYWNjZXNzXCIsIHtndWVzdF9hY2Nlc3M6IGd1ZXN0QWNjZXNzfSwgXCJcIikuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Z3Vlc3RBY2Nlc3M6IGJlZm9yZUd1ZXN0QWNjZXNzfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkhpc3RvcnlSYWRpb1RvZ2dsZSA9IChldikgPT4ge1xyXG4gICAgICAgIGNvbnN0IGJlZm9yZUhpc3RvcnkgPSB0aGlzLnN0YXRlLmhpc3Rvcnk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aGlzdG9yeTogZXYudGFyZ2V0LnZhbHVlfSk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRTdGF0ZUV2ZW50KHRoaXMucHJvcHMucm9vbUlkLCBcIm0ucm9vbS5oaXN0b3J5X3Zpc2liaWxpdHlcIiwge1xyXG4gICAgICAgICAgICBoaXN0b3J5X3Zpc2liaWxpdHk6IGV2LnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9LCBcIlwiKS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtoaXN0b3J5OiBiZWZvcmVIaXN0b3J5fSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF91cGRhdGVCbGFja2xpc3REZXZpY2VzRmxhZyA9IChjaGVja2VkKSA9PiB7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20odGhpcy5wcm9wcy5yb29tSWQpLnNldEJsYWNrbGlzdFVudmVyaWZpZWREZXZpY2VzKGNoZWNrZWQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBhc3luYyBfaGFzQWxpYXNlcygpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGF3YWl0IGNsaS5kb2VzU2VydmVyU3VwcG9ydFVuc3RhYmxlRmVhdHVyZShcIm9yZy5tYXRyaXgubXNjMjQzMlwiKSkge1xyXG4gICAgICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGNsaS51bnN0YWJsZUdldExvY2FsQWxpYXNlcyh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGxvY2FsQWxpYXNlcyA9IHJlc3BvbnNlLmFsaWFzZXM7XHJcbiAgICAgICAgICAgIHJldHVybiBBcnJheS5pc0FycmF5KGxvY2FsQWxpYXNlcykgJiYgbG9jYWxBbGlhc2VzLmxlbmd0aCAhPT0gMDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20odGhpcy5wcm9wcy5yb29tSWQpO1xyXG4gICAgICAgICAgICBjb25zdCBhbGlhc0V2ZW50cyA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLmFsaWFzZXNcIikgfHwgW107XHJcbiAgICAgICAgICAgIGNvbnN0IGhhc0FsaWFzZXMgPSAhIWFsaWFzRXZlbnRzLmZpbmQoKGV2KSA9PiAoZXYuZ2V0Q29udGVudCgpLmFsaWFzZXMgfHwgW10pLmxlbmd0aCA+IDApO1xyXG4gICAgICAgICAgICByZXR1cm4gaGFzQWxpYXNlcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclJvb21BY2Nlc3MoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSBjbGllbnQuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgY29uc3Qgam9pblJ1bGUgPSB0aGlzLnN0YXRlLmpvaW5SdWxlO1xyXG4gICAgICAgIGNvbnN0IGd1ZXN0QWNjZXNzID0gdGhpcy5zdGF0ZS5ndWVzdEFjY2VzcztcclxuXHJcbiAgICAgICAgY29uc3QgY2FuQ2hhbmdlQWNjZXNzID0gcm9vbS5jdXJyZW50U3RhdGUubWF5Q2xpZW50U2VuZFN0YXRlRXZlbnQoXCJtLnJvb20uam9pbl9ydWxlc1wiLCBjbGllbnQpXHJcbiAgICAgICAgICAgICYmIHJvb20uY3VycmVudFN0YXRlLm1heUNsaWVudFNlbmRTdGF0ZUV2ZW50KFwibS5yb29tLmd1ZXN0X2FjY2Vzc1wiLCBjbGllbnQpO1xyXG5cclxuICAgICAgICBsZXQgZ3Vlc3RXYXJuaW5nID0gbnVsbDtcclxuICAgICAgICBpZiAoam9pblJ1bGUgIT09ICdwdWJsaWMnICYmIGd1ZXN0QWNjZXNzID09PSAnZm9yYmlkZGVuJykge1xyXG4gICAgICAgICAgICBndWVzdFdhcm5pbmcgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2VjdXJpdHlSb29tU2V0dGluZ3NUYWJfd2FybmluZyc+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9yZXMvaW1nL3dhcm5pbmcuc3ZnXCIpfSB3aWR0aD17MTV9IGhlaWdodD17MTV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkd1ZXN0cyBjYW5ub3Qgam9pbiB0aGlzIHJvb20gZXZlbiBpZiBleHBsaWNpdGx5IGludml0ZWQuXCIpfSZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiXCIgb25DbGljaz17dGhpcy5fZml4R3Vlc3RBY2Nlc3N9PntfdChcIkNsaWNrIGhlcmUgdG8gZml4XCIpfTwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBhbGlhc1dhcm5pbmcgPSBudWxsO1xyXG4gICAgICAgIGlmIChqb2luUnVsZSA9PT0gJ3B1YmxpYycgJiYgIXRoaXMuc3RhdGUuaGFzQWxpYXNlcykge1xyXG4gICAgICAgICAgICBhbGlhc1dhcm5pbmcgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2VjdXJpdHlSb29tU2V0dGluZ3NUYWJfd2FybmluZyc+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9yZXMvaW1nL3dhcm5pbmcuc3ZnXCIpfSB3aWR0aD17MTV9IGhlaWdodD17MTV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlRvIGxpbmsgdG8gdGhpcyByb29tLCBwbGVhc2UgYWRkIGFuIGFsaWFzLlwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7Z3Vlc3RXYXJuaW5nfVxyXG4gICAgICAgICAgICAgICAge2FsaWFzV2FybmluZ31cclxuICAgICAgICAgICAgICAgIDxsYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgbmFtZT1cInJvb21WaXNcIiB2YWx1ZT1cImludml0ZV9vbmx5XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFjYW5DaGFuZ2VBY2Nlc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblJvb21BY2Nlc3NSYWRpb1RvZ2dsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17am9pblJ1bGUgIT09IFwicHVibGljXCJ9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAge190KCdPbmx5IHBlb3BsZSB3aG8gaGF2ZSBiZWVuIGludml0ZWQnKX1cclxuICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIG5hbWU9XCJyb29tVmlzXCIgdmFsdWU9XCJwdWJsaWNfbm9fZ3Vlc3RzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFjYW5DaGFuZ2VBY2Nlc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblJvb21BY2Nlc3NSYWRpb1RvZ2dsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17am9pblJ1bGUgPT09IFwicHVibGljXCIgJiYgZ3Vlc3RBY2Nlc3MgIT09IFwiY2FuX2pvaW5cIn0gLz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoJ0FueW9uZSB3aG8ga25vd3MgdGhlIHJvb21cXCdzIGxpbmssIGFwYXJ0IGZyb20gZ3Vlc3RzJyl9XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBuYW1lPVwicm9vbVZpc1wiIHZhbHVlPVwicHVibGljX3dpdGhfZ3Vlc3RzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFjYW5DaGFuZ2VBY2Nlc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblJvb21BY2Nlc3NSYWRpb1RvZ2dsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17am9pblJ1bGUgPT09IFwicHVibGljXCIgJiYgZ3Vlc3RBY2Nlc3MgPT09IFwiY2FuX2pvaW5cIn0gLz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJBbnlvbmUgd2hvIGtub3dzIHRoZSByb29tJ3MgbGluaywgaW5jbHVkaW5nIGd1ZXN0c1wiKX1cclxuICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlckhpc3RvcnkoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IGhpc3RvcnkgPSB0aGlzLnN0YXRlLmhpc3Rvcnk7XHJcbiAgICAgICAgY29uc3Qgc3RhdGUgPSBjbGllbnQuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCkuY3VycmVudFN0YXRlO1xyXG4gICAgICAgIGNvbnN0IGNhbkNoYW5nZUhpc3RvcnkgPSBzdGF0ZS5tYXlDbGllbnRTZW5kU3RhdGVFdmVudCgnbS5yb29tLmhpc3RvcnlfdmlzaWJpbGl0eScsIGNsaWVudCk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdCgnQ2hhbmdlcyB0byB3aG8gY2FuIHJlYWQgaGlzdG9yeSB3aWxsIG9ubHkgYXBwbHkgdG8gZnV0dXJlIG1lc3NhZ2VzIGluIHRoaXMgcm9vbS4gJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGUgdmlzaWJpbGl0eSBvZiBleGlzdGluZyBoaXN0b3J5IHdpbGwgYmUgdW5jaGFuZ2VkLicpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIG5hbWU9XCJoaXN0b3J5VmlzXCIgdmFsdWU9XCJ3b3JsZF9yZWFkYWJsZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshY2FuQ2hhbmdlSGlzdG9yeX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGlzdG9yeSA9PT0gXCJ3b3JsZF9yZWFkYWJsZVwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25IaXN0b3J5UmFkaW9Ub2dnbGV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiQW55b25lXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxsYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgbmFtZT1cImhpc3RvcnlWaXNcIiB2YWx1ZT1cInNoYXJlZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshY2FuQ2hhbmdlSGlzdG9yeX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGlzdG9yeSA9PT0gXCJzaGFyZWRcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uSGlzdG9yeVJhZGlvVG9nZ2xlfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdCgnTWVtYmVycyBvbmx5IChzaW5jZSB0aGUgcG9pbnQgaW4gdGltZSBvZiBzZWxlY3RpbmcgdGhpcyBvcHRpb24pJyl9XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBuYW1lPVwiaGlzdG9yeVZpc1wiIHZhbHVlPVwiaW52aXRlZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshY2FuQ2hhbmdlSGlzdG9yeX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGlzdG9yeSA9PT0gXCJpbnZpdGVkXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkhpc3RvcnlSYWRpb1RvZ2dsZX0gLz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoJ01lbWJlcnMgb25seSAoc2luY2UgdGhleSB3ZXJlIGludml0ZWQpJyl9XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsID5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgbmFtZT1cImhpc3RvcnlWaXNcIiB2YWx1ZT1cImpvaW5lZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshY2FuQ2hhbmdlSGlzdG9yeX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGlzdG9yeSA9PT0gXCJqb2luZWRcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uSGlzdG9yeVJhZGlvVG9nZ2xlfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdCgnTWVtYmVycyBvbmx5IChzaW5jZSB0aGV5IGpvaW5lZCknKX1cclxuICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IFNldHRpbmdzRmxhZyA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TZXR0aW5nc0ZsYWdcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCByb29tID0gY2xpZW50LmdldFJvb20odGhpcy5wcm9wcy5yb29tSWQpO1xyXG4gICAgICAgIGNvbnN0IGlzRW5jcnlwdGVkID0gdGhpcy5zdGF0ZS5lbmNyeXB0ZWQ7XHJcbiAgICAgICAgY29uc3QgaGFzRW5jcnlwdGlvblBlcm1pc3Npb24gPSByb29tLmN1cnJlbnRTdGF0ZS5tYXlDbGllbnRTZW5kU3RhdGVFdmVudChcIm0ucm9vbS5lbmNyeXB0aW9uXCIsIGNsaWVudCk7XHJcbiAgICAgICAgY29uc3QgY2FuRW5hYmxlRW5jcnlwdGlvbiA9ICFpc0VuY3J5cHRlZCAmJiBoYXNFbmNyeXB0aW9uUGVybWlzc2lvbjtcclxuXHJcbiAgICAgICAgbGV0IGVuY3J5cHRpb25TZXR0aW5ncyA9IG51bGw7XHJcbiAgICAgICAgaWYgKGlzRW5jcnlwdGVkKSB7XHJcbiAgICAgICAgICAgIGVuY3J5cHRpb25TZXR0aW5ncyA9IDxTZXR0aW5nc0ZsYWcgbmFtZT1cImJsYWNrbGlzdFVudmVyaWZpZWREZXZpY2VzXCIgbGV2ZWw9e1NldHRpbmdMZXZlbC5ST09NX0RFVklDRX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fdXBkYXRlQmxhY2tsaXN0RGV2aWNlc0ZsYWd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9vbUlkPXt0aGlzLnByb3BzLnJvb21JZH0gLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiIG14X1NlY3VyaXR5Um9vbVNldHRpbmdzVGFiXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX2hlYWRpbmdcIj57X3QoXCJTZWN1cml0eSAmIFByaXZhY3lcIil9PC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57X3QoXCJFbmNyeXB0aW9uXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zZWN0aW9uIG14X1NlY3VyaXR5Um9vbVNldHRpbmdzVGFiX2VuY3J5cHRpb25TZWN0aW9uJz5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e190KFwiT25jZSBlbmFibGVkLCBlbmNyeXB0aW9uIGNhbm5vdCBiZSBkaXNhYmxlZC5cIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsbGVkVG9nZ2xlU3dpdGNoIHZhbHVlPXtpc0VuY3J5cHRlZH0gb25DaGFuZ2U9e3RoaXMuX29uRW5jcnlwdGlvbkNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdChcIkVuY3J5cHRlZFwiKX0gZGlzYWJsZWQ9eyFjYW5FbmFibGVFbmNyeXB0aW9ufSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtlbmNyeXB0aW9uU2V0dGluZ3N9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdChcIldobyBjYW4gYWNjZXNzIHRoaXMgcm9vbT9cIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3NlY3Rpb24gbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJSb29tQWNjZXNzKCl9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcnPntfdChcIldobyBjYW4gcmVhZCBoaXN0b3J5P1wiKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc2VjdGlvbiBteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlckhpc3RvcnkoKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==