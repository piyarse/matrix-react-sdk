"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _SettingsStore = require("../../../settings/SettingsStore");

/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const SETTING_MANUALLY_VERIFY_ALL_SESSIONS = "e2ee.manuallyVerifyAllSessions";

const E2eAdvancedPanel = props => {
  const SettingsFlag = sdk.getComponent('views.elements.SettingsFlag');
  return _react.default.createElement("div", {
    className: "mx_SettingsTab_section"
  }, _react.default.createElement("span", {
    className: "mx_SettingsTab_subheading"
  }, (0, _languageHandler._t)("Advanced")), _react.default.createElement(SettingsFlag, {
    name: SETTING_MANUALLY_VERIFY_ALL_SESSIONS,
    level: _SettingsStore.SettingLevel.DEVICE
  }), _react.default.createElement("div", {
    className: "mx_E2eAdvancedPanel_settingLongDescription"
  }, (0, _languageHandler._t)("Individually verify each session used by a user to mark it as trusted, not trusting cross-signed devices.")));
};

var _default = E2eAdvancedPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL0UyZUFkdmFuY2VkUGFuZWwuanMiXSwibmFtZXMiOlsiU0VUVElOR19NQU5VQUxMWV9WRVJJRllfQUxMX1NFU1NJT05TIiwiRTJlQWR2YW5jZWRQYW5lbCIsInByb3BzIiwiU2V0dGluZ3NGbGFnIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFwQkE7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSxNQUFNQSxvQ0FBb0MsR0FBRyxnQ0FBN0M7O0FBRUEsTUFBTUMsZ0JBQWdCLEdBQUdDLEtBQUssSUFBSTtBQUM5QixRQUFNQyxZQUFZLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw2QkFBakIsQ0FBckI7QUFDQSxTQUFPO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNIO0FBQU0sSUFBQSxTQUFTLEVBQUM7QUFBaEIsS0FBNkMseUJBQUcsVUFBSCxDQUE3QyxDQURHLEVBR0gsNkJBQUMsWUFBRDtBQUFjLElBQUEsSUFBSSxFQUFFTCxvQ0FBcEI7QUFDSSxJQUFBLEtBQUssRUFBRU0sNEJBQWFDO0FBRHhCLElBSEcsRUFNSDtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FBNkQseUJBQ3pELDJHQUR5RCxDQUE3RCxDQU5HLENBQVA7QUFVSCxDQVpEOztlQWNlTixnQiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcblxyXG5jb25zdCBTRVRUSU5HX01BTlVBTExZX1ZFUklGWV9BTExfU0VTU0lPTlMgPSBcImUyZWUubWFudWFsbHlWZXJpZnlBbGxTZXNzaW9uc1wiO1xyXG5cclxuY29uc3QgRTJlQWR2YW5jZWRQYW5lbCA9IHByb3BzID0+IHtcclxuICAgIGNvbnN0IFNldHRpbmdzRmxhZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLlNldHRpbmdzRmxhZycpO1xyXG4gICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvblwiPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj57X3QoXCJBZHZhbmNlZFwiKX08L3NwYW4+XHJcblxyXG4gICAgICAgIDxTZXR0aW5nc0ZsYWcgbmFtZT17U0VUVElOR19NQU5VQUxMWV9WRVJJRllfQUxMX1NFU1NJT05TfVxyXG4gICAgICAgICAgICBsZXZlbD17U2V0dGluZ0xldmVsLkRFVklDRX1cclxuICAgICAgICAvPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRTJlQWR2YW5jZWRQYW5lbF9zZXR0aW5nTG9uZ0Rlc2NyaXB0aW9uXCI+e190KFxyXG4gICAgICAgICAgICBcIkluZGl2aWR1YWxseSB2ZXJpZnkgZWFjaCBzZXNzaW9uIHVzZWQgYnkgYSB1c2VyIHRvIG1hcmsgaXQgYXMgdHJ1c3RlZCwgbm90IHRydXN0aW5nIGNyb3NzLXNpZ25lZCBkZXZpY2VzLlwiLFxyXG4gICAgICAgICl9PC9kaXY+XHJcbiAgICA8L2Rpdj47XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBFMmVBZHZhbmNlZFBhbmVsO1xyXG4iXX0=