"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _contentRepo = require("matrix-js-sdk/src/content-repo");

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _Pill = _interopRequireDefault(require("../elements/Pill"));

var _Permalinks = require("../../../utils/permalinks/Permalinks");

var _BaseAvatar = _interopRequireDefault(require("../avatars/BaseAvatar"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _replaceableComponent = require("../../../utils/replaceableComponent");

var _dec, _class, _class2, _temp;

let BridgeTile = (_dec = (0, _replaceableComponent.replaceableComponent)("views.settings.BridgeTile"), _dec(_class = (_temp = _class2 = class BridgeTile extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "state", {
      visible: false
    });
  }

  _toggleVisible() {
    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    const content = this.props.ev.getContent();
    const {
      channel,
      network,
      protocol
    } = content;
    const protocolName = protocol.displayname || protocol.id;
    const channelName = channel.displayname || channel.id;
    const networkName = network ? network.displayname || network.id : protocolName;
    let creator = null;

    if (content.creator) {
      creator = (0, _languageHandler._t)("This bridge was provisioned by <user />.", {}, {
        user: _react.default.createElement(_Pill.default, {
          type: _Pill.default.TYPE_USER_MENTION,
          room: this.props.room,
          url: (0, _Permalinks.makeUserPermalink)(content.creator),
          shouldShowPillAvatar: true
        })
      });
    }

    const bot = (0, _languageHandler._t)("This bridge is managed by <user />.", {}, {
      user: _react.default.createElement(_Pill.default, {
        type: _Pill.default.TYPE_USER_MENTION,
        room: this.props.room,
        url: (0, _Permalinks.makeUserPermalink)(this.props.ev.getSender()),
        shouldShowPillAvatar: true
      })
    });
    let networkIcon;

    if (protocol.avatar) {
      const avatarUrl = (0, _contentRepo.getHttpUriForMxc)(_MatrixClientPeg.MatrixClientPeg.get().getHomeserverUrl(), protocol.avatar, 64, 64, "crop");
      networkIcon = _react.default.createElement(_BaseAvatar.default, {
        className: "protocol-icon",
        width: 48,
        height: 48,
        resizeMethod: "crop",
        name: protocolName,
        idName: protocolName,
        url: avatarUrl
      });
    } else {
      networkIcon = _react.default.createElement("div", {
        class: "noProtocolIcon"
      });
    }

    const id = this.props.ev.getId();
    const metadataClassname = "metadata" + (this.state.visible ? " visible" : "");
    return _react.default.createElement("li", {
      key: id
    }, _react.default.createElement("div", {
      className: "column-icon"
    }, networkIcon), _react.default.createElement("div", {
      className: "column-data"
    }, _react.default.createElement("h3", null, protocolName), _react.default.createElement("p", {
      className: "workspace-channel-details"
    }, _react.default.createElement("span", null, (0, _languageHandler._t)("Workspace: %(networkName)s", {
      networkName
    })), _react.default.createElement("span", {
      className: "channel"
    }, (0, _languageHandler._t)("Channel: %(channelName)s", {
      channelName
    }))), _react.default.createElement("p", {
      className: metadataClassname
    }, creator, " ", bot), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_showMore",
      kind: "secondary",
      onClick: this._toggleVisible.bind(this)
    }, this.state.visible ? (0, _languageHandler._t)("Show less") : (0, _languageHandler._t)("Show more"))));
  }

}, (0, _defineProperty2.default)(_class2, "propTypes", {
  ev: _propTypes.default.object.isRequired,
  room: _propTypes.default.object.isRequired
}), _temp)) || _class);
exports.default = BridgeTile;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL0JyaWRnZVRpbGUuanMiXSwibmFtZXMiOlsiQnJpZGdlVGlsZSIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsInZpc2libGUiLCJfdG9nZ2xlVmlzaWJsZSIsInNldFN0YXRlIiwic3RhdGUiLCJyZW5kZXIiLCJjb250ZW50IiwicHJvcHMiLCJldiIsImdldENvbnRlbnQiLCJjaGFubmVsIiwibmV0d29yayIsInByb3RvY29sIiwicHJvdG9jb2xOYW1lIiwiZGlzcGxheW5hbWUiLCJpZCIsImNoYW5uZWxOYW1lIiwibmV0d29ya05hbWUiLCJjcmVhdG9yIiwidXNlciIsIlBpbGwiLCJUWVBFX1VTRVJfTUVOVElPTiIsInJvb20iLCJib3QiLCJnZXRTZW5kZXIiLCJuZXR3b3JrSWNvbiIsImF2YXRhciIsImF2YXRhclVybCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldEhvbWVzZXJ2ZXJVcmwiLCJnZXRJZCIsIm1ldGFkYXRhQ2xhc3NuYW1lIiwiYmluZCIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7O0lBR3FCQSxVLFdBRHBCLGdEQUFxQiwyQkFBckIsQyxtQ0FBRCxNQUNxQkEsVUFEckIsU0FDd0NDLGVBQU1DLGFBRDlDLENBQzREO0FBQUE7QUFBQTtBQUFBLGlEQU1oRDtBQUNKQyxNQUFBQSxPQUFPLEVBQUU7QUFETCxLQU5nRDtBQUFBOztBQVV4REMsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsU0FBS0MsUUFBTCxDQUFjO0FBQ1ZGLE1BQUFBLE9BQU8sRUFBRSxDQUFDLEtBQUtHLEtBQUwsQ0FBV0g7QUFEWCxLQUFkO0FBR0g7O0FBRURJLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE9BQU8sR0FBRyxLQUFLQyxLQUFMLENBQVdDLEVBQVgsQ0FBY0MsVUFBZCxFQUFoQjtBQUNBLFVBQU07QUFBRUMsTUFBQUEsT0FBRjtBQUFXQyxNQUFBQSxPQUFYO0FBQW9CQyxNQUFBQTtBQUFwQixRQUFpQ04sT0FBdkM7QUFDQSxVQUFNTyxZQUFZLEdBQUdELFFBQVEsQ0FBQ0UsV0FBVCxJQUF3QkYsUUFBUSxDQUFDRyxFQUF0RDtBQUNBLFVBQU1DLFdBQVcsR0FBR04sT0FBTyxDQUFDSSxXQUFSLElBQXVCSixPQUFPLENBQUNLLEVBQW5EO0FBQ0EsVUFBTUUsV0FBVyxHQUFHTixPQUFPLEdBQUdBLE9BQU8sQ0FBQ0csV0FBUixJQUF1QkgsT0FBTyxDQUFDSSxFQUFsQyxHQUF1Q0YsWUFBbEU7QUFFQSxRQUFJSyxPQUFPLEdBQUcsSUFBZDs7QUFDQSxRQUFJWixPQUFPLENBQUNZLE9BQVosRUFBcUI7QUFDakJBLE1BQUFBLE9BQU8sR0FBRyx5QkFBRywwQ0FBSCxFQUErQyxFQUEvQyxFQUFtRDtBQUNyREMsUUFBQUEsSUFBSSxFQUFFLDZCQUFDLGFBQUQ7QUFDRixVQUFBLElBQUksRUFBRUMsY0FBS0MsaUJBRFQ7QUFFRixVQUFBLElBQUksRUFBRSxLQUFLZCxLQUFMLENBQVdlLElBRmY7QUFHRixVQUFBLEdBQUcsRUFBRSxtQ0FBa0JoQixPQUFPLENBQUNZLE9BQTFCLENBSEg7QUFJRixVQUFBLG9CQUFvQixFQUFFO0FBSnBCO0FBRCtDLE9BQW5ELENBQVY7QUFRSDs7QUFFRCxVQUFNSyxHQUFHLEdBQUcseUJBQUcscUNBQUgsRUFBMEMsRUFBMUMsRUFBOEM7QUFDdERKLE1BQUFBLElBQUksRUFBRSw2QkFBQyxhQUFEO0FBQ0YsUUFBQSxJQUFJLEVBQUVDLGNBQUtDLGlCQURUO0FBRUYsUUFBQSxJQUFJLEVBQUUsS0FBS2QsS0FBTCxDQUFXZSxJQUZmO0FBR0YsUUFBQSxHQUFHLEVBQUUsbUNBQWtCLEtBQUtmLEtBQUwsQ0FBV0MsRUFBWCxDQUFjZ0IsU0FBZCxFQUFsQixDQUhIO0FBSUYsUUFBQSxvQkFBb0IsRUFBRTtBQUpwQjtBQURnRCxLQUE5QyxDQUFaO0FBU0EsUUFBSUMsV0FBSjs7QUFFQSxRQUFJYixRQUFRLENBQUNjLE1BQWIsRUFBcUI7QUFDakIsWUFBTUMsU0FBUyxHQUFHLG1DQUNkQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxnQkFBdEIsRUFEYyxFQUVkbEIsUUFBUSxDQUFDYyxNQUZLLEVBRUcsRUFGSCxFQUVPLEVBRlAsRUFFVyxNQUZYLENBQWxCO0FBS0FELE1BQUFBLFdBQVcsR0FBRyw2QkFBQyxtQkFBRDtBQUFZLFFBQUEsU0FBUyxFQUFDLGVBQXRCO0FBQ1YsUUFBQSxLQUFLLEVBQUUsRUFERztBQUVWLFFBQUEsTUFBTSxFQUFFLEVBRkU7QUFHVixRQUFBLFlBQVksRUFBQyxNQUhIO0FBSVYsUUFBQSxJQUFJLEVBQUdaLFlBSkc7QUFLVixRQUFBLE1BQU0sRUFBR0EsWUFMQztBQU1WLFFBQUEsR0FBRyxFQUFHYztBQU5JLFFBQWQ7QUFRSCxLQWRELE1BY087QUFDSEYsTUFBQUEsV0FBVyxHQUFHO0FBQUssUUFBQSxLQUFLLEVBQUM7QUFBWCxRQUFkO0FBQ0g7O0FBRUQsVUFBTVYsRUFBRSxHQUFHLEtBQUtSLEtBQUwsQ0FBV0MsRUFBWCxDQUFjdUIsS0FBZCxFQUFYO0FBQ0EsVUFBTUMsaUJBQWlCLEdBQUcsY0FBYyxLQUFLNUIsS0FBTCxDQUFXSCxPQUFYLEdBQXFCLFVBQXJCLEdBQWtDLEVBQWhELENBQTFCO0FBQ0EsV0FBUTtBQUFJLE1BQUEsR0FBRyxFQUFFYztBQUFULE9BQ0o7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0tVLFdBREwsQ0FESSxFQUlKO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLHlDQUFLWixZQUFMLENBREosRUFFSTtBQUFHLE1BQUEsU0FBUyxFQUFDO0FBQWIsT0FDSSwyQ0FBTyx5QkFBRyw0QkFBSCxFQUFpQztBQUFDSSxNQUFBQTtBQUFELEtBQWpDLENBQVAsQ0FESixFQUVJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBMkIseUJBQUcsMEJBQUgsRUFBK0I7QUFBQ0QsTUFBQUE7QUFBRCxLQUEvQixDQUEzQixDQUZKLENBRkosRUFNSTtBQUFHLE1BQUEsU0FBUyxFQUFFZ0I7QUFBZCxPQUNLZCxPQURMLE9BQ2VLLEdBRGYsQ0FOSixFQVNJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLGFBQTVCO0FBQTBDLE1BQUEsSUFBSSxFQUFDLFdBQS9DO0FBQTJELE1BQUEsT0FBTyxFQUFFLEtBQUtyQixjQUFMLENBQW9CK0IsSUFBcEIsQ0FBeUIsSUFBekI7QUFBcEUsT0FDTSxLQUFLN0IsS0FBTCxDQUFXSCxPQUFYLEdBQXFCLHlCQUFHLFdBQUgsQ0FBckIsR0FBdUMseUJBQUcsV0FBSCxDQUQ3QyxDQVRKLENBSkksQ0FBUjtBQWtCSDs7QUFwRnVELEMsc0RBQ3JDO0FBQ2ZPLEVBQUFBLEVBQUUsRUFBRTBCLG1CQUFVQyxNQUFWLENBQWlCQyxVQUROO0FBRWZkLEVBQUFBLElBQUksRUFBRVksbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRlIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtnZXRIdHRwVXJpRm9yTXhjfSBmcm9tIFwibWF0cml4LWpzLXNkay9zcmMvY29udGVudC1yZXBvXCI7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0IFBpbGwgZnJvbSBcIi4uL2VsZW1lbnRzL1BpbGxcIjtcclxuaW1wb3J0IHttYWtlVXNlclBlcm1hbGlua30gZnJvbSBcIi4uLy4uLy4uL3V0aWxzL3Blcm1hbGlua3MvUGVybWFsaW5rc1wiO1xyXG5pbXBvcnQgQmFzZUF2YXRhciBmcm9tIFwiLi4vYXZhdGFycy9CYXNlQXZhdGFyXCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCB7cmVwbGFjZWFibGVDb21wb25lbnR9IGZyb20gXCIuLi8uLi8uLi91dGlscy9yZXBsYWNlYWJsZUNvbXBvbmVudFwiO1xyXG5cclxuQHJlcGxhY2VhYmxlQ29tcG9uZW50KFwidmlld3Muc2V0dGluZ3MuQnJpZGdlVGlsZVwiKVxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCcmlkZ2VUaWxlIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIGV2OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRlID0ge1xyXG4gICAgICAgIHZpc2libGU6IGZhbHNlLFxyXG4gICAgfVxyXG5cclxuICAgIF90b2dnbGVWaXNpYmxlKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB2aXNpYmxlOiAhdGhpcy5zdGF0ZS52aXNpYmxlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5ldi5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgY29uc3QgeyBjaGFubmVsLCBuZXR3b3JrLCBwcm90b2NvbCB9ID0gY29udGVudDtcclxuICAgICAgICBjb25zdCBwcm90b2NvbE5hbWUgPSBwcm90b2NvbC5kaXNwbGF5bmFtZSB8fCBwcm90b2NvbC5pZDtcclxuICAgICAgICBjb25zdCBjaGFubmVsTmFtZSA9IGNoYW5uZWwuZGlzcGxheW5hbWUgfHwgY2hhbm5lbC5pZDtcclxuICAgICAgICBjb25zdCBuZXR3b3JrTmFtZSA9IG5ldHdvcmsgPyBuZXR3b3JrLmRpc3BsYXluYW1lIHx8IG5ldHdvcmsuaWQgOiBwcm90b2NvbE5hbWU7XHJcblxyXG4gICAgICAgIGxldCBjcmVhdG9yID0gbnVsbDtcclxuICAgICAgICBpZiAoY29udGVudC5jcmVhdG9yKSB7XHJcbiAgICAgICAgICAgIGNyZWF0b3IgPSBfdChcIlRoaXMgYnJpZGdlIHdhcyBwcm92aXNpb25lZCBieSA8dXNlciAvPi5cIiwge30sIHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VyOiA8UGlsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPXtQaWxsLlRZUEVfVVNFUl9NRU5USU9OfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybD17bWFrZVVzZXJQZXJtYWxpbmsoY29udGVudC5jcmVhdG9yKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvdWxkU2hvd1BpbGxBdmF0YXI9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgLz4sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgYm90ID0gX3QoXCJUaGlzIGJyaWRnZSBpcyBtYW5hZ2VkIGJ5IDx1c2VyIC8+LlwiLCB7fSwge1xyXG4gICAgICAgICAgICB1c2VyOiA8UGlsbFxyXG4gICAgICAgICAgICAgICAgdHlwZT17UGlsbC5UWVBFX1VTRVJfTUVOVElPTn1cclxuICAgICAgICAgICAgICAgIHJvb209e3RoaXMucHJvcHMucm9vbX1cclxuICAgICAgICAgICAgICAgIHVybD17bWFrZVVzZXJQZXJtYWxpbmsodGhpcy5wcm9wcy5ldi5nZXRTZW5kZXIoKSl9XHJcbiAgICAgICAgICAgICAgICBzaG91bGRTaG93UGlsbEF2YXRhcj17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIC8+LFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgbmV0d29ya0ljb247XHJcblxyXG4gICAgICAgIGlmIChwcm90b2NvbC5hdmF0YXIpIHtcclxuICAgICAgICAgICAgY29uc3QgYXZhdGFyVXJsID0gZ2V0SHR0cFVyaUZvck14YyhcclxuICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRIb21lc2VydmVyVXJsKCksXHJcbiAgICAgICAgICAgICAgICBwcm90b2NvbC5hdmF0YXIsIDY0LCA2NCwgXCJjcm9wXCIsXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBuZXR3b3JrSWNvbiA9IDxCYXNlQXZhdGFyIGNsYXNzTmFtZT1cInByb3RvY29sLWljb25cIlxyXG4gICAgICAgICAgICAgICAgd2lkdGg9ezQ4fVxyXG4gICAgICAgICAgICAgICAgaGVpZ2h0PXs0OH1cclxuICAgICAgICAgICAgICAgIHJlc2l6ZU1ldGhvZD0nY3JvcCdcclxuICAgICAgICAgICAgICAgIG5hbWU9eyBwcm90b2NvbE5hbWUgfVxyXG4gICAgICAgICAgICAgICAgaWROYW1lPXsgcHJvdG9jb2xOYW1lIH1cclxuICAgICAgICAgICAgICAgIHVybD17IGF2YXRhclVybCB9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5ldHdvcmtJY29uID0gPGRpdiBjbGFzcz1cIm5vUHJvdG9jb2xJY29uXCI+PC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaWQgPSB0aGlzLnByb3BzLmV2LmdldElkKCk7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGFDbGFzc25hbWUgPSBcIm1ldGFkYXRhXCIgKyAodGhpcy5zdGF0ZS52aXNpYmxlID8gXCIgdmlzaWJsZVwiIDogXCJcIik7XHJcbiAgICAgICAgcmV0dXJuICg8bGkga2V5PXtpZH0+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sdW1uLWljb25cIj5cclxuICAgICAgICAgICAgICAgIHtuZXR3b3JrSWNvbn1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sdW1uLWRhdGFcIj5cclxuICAgICAgICAgICAgICAgIDxoMz57cHJvdG9jb2xOYW1lfTwvaDM+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ3b3Jrc3BhY2UtY2hhbm5lbC1kZXRhaWxzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+e190KFwiV29ya3NwYWNlOiAlKG5ldHdvcmtOYW1lKXNcIiwge25ldHdvcmtOYW1lfSl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNoYW5uZWxcIj57X3QoXCJDaGFubmVsOiAlKGNoYW5uZWxOYW1lKXNcIiwge2NoYW5uZWxOYW1lfSl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPXttZXRhZGF0YUNsYXNzbmFtZX0+XHJcbiAgICAgICAgICAgICAgICAgICAge2NyZWF0b3J9IHtib3R9XHJcbiAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9zaG93TW9yZVwiIGtpbmQ9XCJzZWNvbmRhcnlcIiBvbkNsaWNrPXt0aGlzLl90b2dnbGVWaXNpYmxlLmJpbmQodGhpcyl9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5zdGF0ZS52aXNpYmxlID8gX3QoXCJTaG93IGxlc3NcIikgOiBfdChcIlNob3cgbW9yZVwiKSB9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbGk+KTtcclxuICAgIH1cclxufVxyXG4iXX0=