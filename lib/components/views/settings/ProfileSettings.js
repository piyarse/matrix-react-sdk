"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _Field = _interopRequireDefault(require("../elements/Field"));

var _matrixJsSdk = require("matrix-js-sdk");

var _HostingLink = require("../../../utils/HostingLink");

var sdk = _interopRequireWildcard(require("../../../index"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class ProfileSettings extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_uploadAvatar", () => {
      this._avatarUpload.current.click();
    });
    (0, _defineProperty2.default)(this, "_removeAvatar", () => {
      // clear file upload field so same file can be selected
      this._avatarUpload.current.value = "";
      this.setState({
        avatarUrl: undefined,
        avatarFile: undefined,
        enableProfileSave: true
      });
    });
    (0, _defineProperty2.default)(this, "_saveProfile", async e => {
      e.stopPropagation();
      e.preventDefault();
      if (!this.state.enableProfileSave) return;
      this.setState({
        enableProfileSave: false
      });

      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const newState = {}; // TODO: What do we do about errors?

      if (this.state.originalDisplayName !== this.state.displayName) {
        await client.setDisplayName(this.state.displayName);
        newState.originalDisplayName = this.state.displayName;
      }

      if (this.state.avatarFile) {
        const uri = await client.uploadContent(this.state.avatarFile);
        await client.setAvatarUrl(uri);
        newState.avatarUrl = client.mxcUrlToHttp(uri, 96, 96, 'crop', false);
        newState.originalAvatarUrl = newState.avatarUrl;
        newState.avatarFile = null;
      } else if (this.state.originalAvatarUrl !== this.state.avatarUrl) {
        await client.setAvatarUrl(""); // use empty string as Synapse 500s on undefined
      }

      this.setState(newState);
    });
    (0, _defineProperty2.default)(this, "_onDisplayNameChanged", e => {
      this.setState({
        displayName: e.target.value,
        enableProfileSave: true
      });
    });
    (0, _defineProperty2.default)(this, "_onAvatarChanged", e => {
      if (!e.target.files || !e.target.files.length) {
        this.setState({
          avatarUrl: this.state.originalAvatarUrl,
          avatarFile: null,
          enableProfileSave: false
        });
        return;
      }

      const file = e.target.files[0];
      const reader = new FileReader();

      reader.onload = ev => {
        this.setState({
          avatarUrl: ev.target.result,
          avatarFile: file,
          enableProfileSave: true
        });
      };

      reader.readAsDataURL(file);
    });

    const _client = _MatrixClientPeg.MatrixClientPeg.get();

    let user = _client.getUser(_client.getUserId());

    if (!user) {
      // XXX: We shouldn't have to do this.
      // There seems to be a condition where the User object won't exist until a room
      // exists on the account. To work around this, we'll just create a temporary User
      // and use that.
      console.warn("User object not found - creating one for ProfileSettings");
      user = new _matrixJsSdk.User(_client.getUserId());
    }

    let avatarUrl = user.avatarUrl;
    if (avatarUrl) avatarUrl = _client.mxcUrlToHttp(avatarUrl, 96, 96, 'crop', false);
    this.state = {
      userId: user.userId,
      originalDisplayName: user.displayName,
      displayName: user.displayName,
      originalAvatarUrl: avatarUrl,
      avatarUrl: avatarUrl,
      avatarFile: null,
      enableProfileSave: false
    };
    this._avatarUpload = (0, _react.createRef)();
  }

  render() {
    const hostingSignupLink = (0, _HostingLink.getHostingLink)('user-settings');
    let hostingSignup = null;

    if (hostingSignupLink) {
      hostingSignup = _react.default.createElement("span", {
        className: "mx_ProfileSettings_hostingSignup"
      }, (0, _languageHandler._t)("<a>Upgrade</a> to your own domain", {}, {
        a: sub => _react.default.createElement("a", {
          href: hostingSignupLink,
          target: "_blank",
          rel: "noreferrer noopener"
        }, sub)
      }), _react.default.createElement("a", {
        href: hostingSignupLink,
        target: "_blank",
        rel: "noreferrer noopener"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/external-link.svg"),
        width: "11",
        height: "10",
        alt: ""
      })));
    }

    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const AvatarSetting = sdk.getComponent('settings.AvatarSetting');
    return _react.default.createElement("form", {
      onSubmit: this._saveProfile,
      autoComplete: "off",
      noValidate: true
    }, _react.default.createElement("input", {
      type: "file",
      ref: this._avatarUpload,
      className: "mx_ProfileSettings_avatarUpload",
      onChange: this._onAvatarChanged,
      accept: "image/*"
    }), _react.default.createElement("div", {
      className: "mx_ProfileSettings_profile"
    }, _react.default.createElement("div", {
      className: "mx_ProfileSettings_controls"
    }, _react.default.createElement("p", null, this.state.userId, hostingSignup), _react.default.createElement(_Field.default, {
      label: (0, _languageHandler._t)("Display Name"),
      type: "text",
      value: this.state.displayName,
      autoComplete: "off",
      onChange: this._onDisplayNameChanged
    })), _react.default.createElement(AvatarSetting, {
      avatarUrl: this.state.avatarUrl,
      avatarName: this.state.displayName || this.state.userId,
      avatarAltText: (0, _languageHandler._t)("Profile picture"),
      uploadAvatar: this._uploadAvatar,
      removeAvatar: this._removeAvatar
    })), _react.default.createElement(AccessibleButton, {
      onClick: this._saveProfile,
      kind: "primary",
      disabled: !this.state.enableProfileSave
    }, (0, _languageHandler._t)("Save")));
  }

}

exports.default = ProfileSettings;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL1Byb2ZpbGVTZXR0aW5ncy5qcyJdLCJuYW1lcyI6WyJQcm9maWxlU2V0dGluZ3MiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiX2F2YXRhclVwbG9hZCIsImN1cnJlbnQiLCJjbGljayIsInZhbHVlIiwic2V0U3RhdGUiLCJhdmF0YXJVcmwiLCJ1bmRlZmluZWQiLCJhdmF0YXJGaWxlIiwiZW5hYmxlUHJvZmlsZVNhdmUiLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJzdGF0ZSIsImNsaWVudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm5ld1N0YXRlIiwib3JpZ2luYWxEaXNwbGF5TmFtZSIsImRpc3BsYXlOYW1lIiwic2V0RGlzcGxheU5hbWUiLCJ1cmkiLCJ1cGxvYWRDb250ZW50Iiwic2V0QXZhdGFyVXJsIiwibXhjVXJsVG9IdHRwIiwib3JpZ2luYWxBdmF0YXJVcmwiLCJ0YXJnZXQiLCJmaWxlcyIsImxlbmd0aCIsImZpbGUiLCJyZWFkZXIiLCJGaWxlUmVhZGVyIiwib25sb2FkIiwiZXYiLCJyZXN1bHQiLCJyZWFkQXNEYXRhVVJMIiwidXNlciIsImdldFVzZXIiLCJnZXRVc2VySWQiLCJjb25zb2xlIiwid2FybiIsIlVzZXIiLCJ1c2VySWQiLCJyZW5kZXIiLCJob3N0aW5nU2lnbnVwTGluayIsImhvc3RpbmdTaWdudXAiLCJhIiwic3ViIiwicmVxdWlyZSIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJBdmF0YXJTZXR0aW5nIiwiX3NhdmVQcm9maWxlIiwiX29uQXZhdGFyQ2hhbmdlZCIsIl9vbkRpc3BsYXlOYW1lQ2hhbmdlZCIsIl91cGxvYWRBdmF0YXIiLCJfcmVtb3ZlQXZhdGFyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7O0FBd0JlLE1BQU1BLGVBQU4sU0FBOEJDLGVBQU1DLFNBQXBDLENBQThDO0FBQ3pEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLHlEQTRCRSxNQUFNO0FBQ2xCLFdBQUtDLGFBQUwsQ0FBbUJDLE9BQW5CLENBQTJCQyxLQUEzQjtBQUNILEtBOUJhO0FBQUEseURBZ0NFLE1BQU07QUFDbEI7QUFDQSxXQUFLRixhQUFMLENBQW1CQyxPQUFuQixDQUEyQkUsS0FBM0IsR0FBbUMsRUFBbkM7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFDVkMsUUFBQUEsU0FBUyxFQUFFQyxTQUREO0FBRVZDLFFBQUFBLFVBQVUsRUFBRUQsU0FGRjtBQUdWRSxRQUFBQSxpQkFBaUIsRUFBRTtBQUhULE9BQWQ7QUFLSCxLQXhDYTtBQUFBLHdEQTBDQyxNQUFPQyxDQUFQLElBQWE7QUFDeEJBLE1BQUFBLENBQUMsQ0FBQ0MsZUFBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFFQSxVQUFJLENBQUMsS0FBS0MsS0FBTCxDQUFXSixpQkFBaEIsRUFBbUM7QUFDbkMsV0FBS0osUUFBTCxDQUFjO0FBQUNJLFFBQUFBLGlCQUFpQixFQUFFO0FBQXBCLE9BQWQ7O0FBRUEsWUFBTUssTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsWUFBTUMsUUFBUSxHQUFHLEVBQWpCLENBUndCLENBVXhCOztBQUVBLFVBQUksS0FBS0osS0FBTCxDQUFXSyxtQkFBWCxLQUFtQyxLQUFLTCxLQUFMLENBQVdNLFdBQWxELEVBQStEO0FBQzNELGNBQU1MLE1BQU0sQ0FBQ00sY0FBUCxDQUFzQixLQUFLUCxLQUFMLENBQVdNLFdBQWpDLENBQU47QUFDQUYsUUFBQUEsUUFBUSxDQUFDQyxtQkFBVCxHQUErQixLQUFLTCxLQUFMLENBQVdNLFdBQTFDO0FBQ0g7O0FBRUQsVUFBSSxLQUFLTixLQUFMLENBQVdMLFVBQWYsRUFBMkI7QUFDdkIsY0FBTWEsR0FBRyxHQUFHLE1BQU1QLE1BQU0sQ0FBQ1EsYUFBUCxDQUFxQixLQUFLVCxLQUFMLENBQVdMLFVBQWhDLENBQWxCO0FBQ0EsY0FBTU0sTUFBTSxDQUFDUyxZQUFQLENBQW9CRixHQUFwQixDQUFOO0FBQ0FKLFFBQUFBLFFBQVEsQ0FBQ1gsU0FBVCxHQUFxQlEsTUFBTSxDQUFDVSxZQUFQLENBQW9CSCxHQUFwQixFQUF5QixFQUF6QixFQUE2QixFQUE3QixFQUFpQyxNQUFqQyxFQUF5QyxLQUF6QyxDQUFyQjtBQUNBSixRQUFBQSxRQUFRLENBQUNRLGlCQUFULEdBQTZCUixRQUFRLENBQUNYLFNBQXRDO0FBQ0FXLFFBQUFBLFFBQVEsQ0FBQ1QsVUFBVCxHQUFzQixJQUF0QjtBQUNILE9BTkQsTUFNTyxJQUFJLEtBQUtLLEtBQUwsQ0FBV1ksaUJBQVgsS0FBaUMsS0FBS1osS0FBTCxDQUFXUCxTQUFoRCxFQUEyRDtBQUM5RCxjQUFNUSxNQUFNLENBQUNTLFlBQVAsQ0FBb0IsRUFBcEIsQ0FBTixDQUQ4RCxDQUMvQjtBQUNsQzs7QUFFRCxXQUFLbEIsUUFBTCxDQUFjWSxRQUFkO0FBQ0gsS0F0RWE7QUFBQSxpRUF3RVdQLENBQUQsSUFBTztBQUMzQixXQUFLTCxRQUFMLENBQWM7QUFDVmMsUUFBQUEsV0FBVyxFQUFFVCxDQUFDLENBQUNnQixNQUFGLENBQVN0QixLQURaO0FBRVZLLFFBQUFBLGlCQUFpQixFQUFFO0FBRlQsT0FBZDtBQUlILEtBN0VhO0FBQUEsNERBK0VNQyxDQUFELElBQU87QUFDdEIsVUFBSSxDQUFDQSxDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVYsSUFBbUIsQ0FBQ2pCLENBQUMsQ0FBQ2dCLE1BQUYsQ0FBU0MsS0FBVCxDQUFlQyxNQUF2QyxFQUErQztBQUMzQyxhQUFLdkIsUUFBTCxDQUFjO0FBQ1ZDLFVBQUFBLFNBQVMsRUFBRSxLQUFLTyxLQUFMLENBQVdZLGlCQURaO0FBRVZqQixVQUFBQSxVQUFVLEVBQUUsSUFGRjtBQUdWQyxVQUFBQSxpQkFBaUIsRUFBRTtBQUhULFNBQWQ7QUFLQTtBQUNIOztBQUVELFlBQU1vQixJQUFJLEdBQUduQixDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVQsQ0FBZSxDQUFmLENBQWI7QUFDQSxZQUFNRyxNQUFNLEdBQUcsSUFBSUMsVUFBSixFQUFmOztBQUNBRCxNQUFBQSxNQUFNLENBQUNFLE1BQVAsR0FBaUJDLEVBQUQsSUFBUTtBQUNwQixhQUFLNUIsUUFBTCxDQUFjO0FBQ1ZDLFVBQUFBLFNBQVMsRUFBRTJCLEVBQUUsQ0FBQ1AsTUFBSCxDQUFVUSxNQURYO0FBRVYxQixVQUFBQSxVQUFVLEVBQUVxQixJQUZGO0FBR1ZwQixVQUFBQSxpQkFBaUIsRUFBRTtBQUhULFNBQWQ7QUFLSCxPQU5EOztBQU9BcUIsTUFBQUEsTUFBTSxDQUFDSyxhQUFQLENBQXFCTixJQUFyQjtBQUNILEtBbkdhOztBQUdWLFVBQU1mLE9BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFFBQUlvQixJQUFJLEdBQUd0QixPQUFNLENBQUN1QixPQUFQLENBQWV2QixPQUFNLENBQUN3QixTQUFQLEVBQWYsQ0FBWDs7QUFDQSxRQUFJLENBQUNGLElBQUwsRUFBVztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0FHLE1BQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLDBEQUFiO0FBQ0FKLE1BQUFBLElBQUksR0FBRyxJQUFJSyxpQkFBSixDQUFTM0IsT0FBTSxDQUFDd0IsU0FBUCxFQUFULENBQVA7QUFDSDs7QUFDRCxRQUFJaEMsU0FBUyxHQUFHOEIsSUFBSSxDQUFDOUIsU0FBckI7QUFDQSxRQUFJQSxTQUFKLEVBQWVBLFNBQVMsR0FBR1EsT0FBTSxDQUFDVSxZQUFQLENBQW9CbEIsU0FBcEIsRUFBK0IsRUFBL0IsRUFBbUMsRUFBbkMsRUFBdUMsTUFBdkMsRUFBK0MsS0FBL0MsQ0FBWjtBQUNmLFNBQUtPLEtBQUwsR0FBYTtBQUNUNkIsTUFBQUEsTUFBTSxFQUFFTixJQUFJLENBQUNNLE1BREo7QUFFVHhCLE1BQUFBLG1CQUFtQixFQUFFa0IsSUFBSSxDQUFDakIsV0FGakI7QUFHVEEsTUFBQUEsV0FBVyxFQUFFaUIsSUFBSSxDQUFDakIsV0FIVDtBQUlUTSxNQUFBQSxpQkFBaUIsRUFBRW5CLFNBSlY7QUFLVEEsTUFBQUEsU0FBUyxFQUFFQSxTQUxGO0FBTVRFLE1BQUFBLFVBQVUsRUFBRSxJQU5IO0FBT1RDLE1BQUFBLGlCQUFpQixFQUFFO0FBUFYsS0FBYjtBQVVBLFNBQUtSLGFBQUwsR0FBcUIsdUJBQXJCO0FBQ0g7O0FBMkVEMEMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsaUJBQWlCLEdBQUcsaUNBQWUsZUFBZixDQUExQjtBQUNBLFFBQUlDLGFBQWEsR0FBRyxJQUFwQjs7QUFDQSxRQUFJRCxpQkFBSixFQUF1QjtBQUNuQkMsTUFBQUEsYUFBYSxHQUFHO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FDWCx5QkFDRyxtQ0FESCxFQUN3QyxFQUR4QyxFQUVHO0FBQ0lDLFFBQUFBLENBQUMsRUFBRUMsR0FBRyxJQUFJO0FBQUcsVUFBQSxJQUFJLEVBQUVILGlCQUFUO0FBQTRCLFVBQUEsTUFBTSxFQUFDLFFBQW5DO0FBQTRDLFVBQUEsR0FBRyxFQUFDO0FBQWhELFdBQXVFRyxHQUF2RTtBQURkLE9BRkgsQ0FEVyxFQU9aO0FBQUcsUUFBQSxJQUFJLEVBQUVILGlCQUFUO0FBQTRCLFFBQUEsTUFBTSxFQUFDLFFBQW5DO0FBQTRDLFFBQUEsR0FBRyxFQUFDO0FBQWhELFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRUksT0FBTyxDQUFDLHVDQUFELENBQWpCO0FBQTRELFFBQUEsS0FBSyxFQUFDLElBQWxFO0FBQXVFLFFBQUEsTUFBTSxFQUFDLElBQTlFO0FBQW1GLFFBQUEsR0FBRyxFQUFDO0FBQXZGLFFBREosQ0FQWSxDQUFoQjtBQVdIOztBQUVELFVBQU1DLGdCQUFnQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTUMsYUFBYSxHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBQ0EsV0FDSTtBQUFNLE1BQUEsUUFBUSxFQUFFLEtBQUtFLFlBQXJCO0FBQW1DLE1BQUEsWUFBWSxFQUFDLEtBQWhEO0FBQXNELE1BQUEsVUFBVSxFQUFFO0FBQWxFLE9BQ0k7QUFBTyxNQUFBLElBQUksRUFBQyxNQUFaO0FBQW1CLE1BQUEsR0FBRyxFQUFFLEtBQUtwRCxhQUE3QjtBQUE0QyxNQUFBLFNBQVMsRUFBQyxpQ0FBdEQ7QUFDTyxNQUFBLFFBQVEsRUFBRSxLQUFLcUQsZ0JBRHRCO0FBQ3dDLE1BQUEsTUFBTSxFQUFDO0FBRC9DLE1BREosRUFHSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx3Q0FDSyxLQUFLekMsS0FBTCxDQUFXNkIsTUFEaEIsRUFFS0csYUFGTCxDQURKLEVBS0ksNkJBQUMsY0FBRDtBQUFPLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGNBQUgsQ0FBZDtBQUNPLE1BQUEsSUFBSSxFQUFDLE1BRFo7QUFDbUIsTUFBQSxLQUFLLEVBQUUsS0FBS2hDLEtBQUwsQ0FBV00sV0FEckM7QUFDa0QsTUFBQSxZQUFZLEVBQUMsS0FEL0Q7QUFFTyxNQUFBLFFBQVEsRUFBRSxLQUFLb0M7QUFGdEIsTUFMSixDQURKLEVBVUksNkJBQUMsYUFBRDtBQUNJLE1BQUEsU0FBUyxFQUFFLEtBQUsxQyxLQUFMLENBQVdQLFNBRDFCO0FBRUksTUFBQSxVQUFVLEVBQUUsS0FBS08sS0FBTCxDQUFXTSxXQUFYLElBQTBCLEtBQUtOLEtBQUwsQ0FBVzZCLE1BRnJEO0FBR0ksTUFBQSxhQUFhLEVBQUUseUJBQUcsaUJBQUgsQ0FIbkI7QUFJSSxNQUFBLFlBQVksRUFBRSxLQUFLYyxhQUp2QjtBQUtJLE1BQUEsWUFBWSxFQUFFLEtBQUtDO0FBTHZCLE1BVkosQ0FISixFQW9CSSw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRSxLQUFLSixZQUFoQztBQUE4QyxNQUFBLElBQUksRUFBQyxTQUFuRDtBQUNrQixNQUFBLFFBQVEsRUFBRSxDQUFDLEtBQUt4QyxLQUFMLENBQVdKO0FBRHhDLE9BRUsseUJBQUcsTUFBSCxDQUZMLENBcEJKLENBREo7QUEyQkg7O0FBcEp3RCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0IEZpZWxkIGZyb20gXCIuLi9lbGVtZW50cy9GaWVsZFwiO1xyXG5pbXBvcnQge1VzZXJ9IGZyb20gXCJtYXRyaXgtanMtc2RrXCI7XHJcbmltcG9ydCB7IGdldEhvc3RpbmdMaW5rIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvSG9zdGluZ0xpbmsnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uL2luZGV4XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQcm9maWxlU2V0dGluZ3MgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGxldCB1c2VyID0gY2xpZW50LmdldFVzZXIoY2xpZW50LmdldFVzZXJJZCgpKTtcclxuICAgICAgICBpZiAoIXVzZXIpIHtcclxuICAgICAgICAgICAgLy8gWFhYOiBXZSBzaG91bGRuJ3QgaGF2ZSB0byBkbyB0aGlzLlxyXG4gICAgICAgICAgICAvLyBUaGVyZSBzZWVtcyB0byBiZSBhIGNvbmRpdGlvbiB3aGVyZSB0aGUgVXNlciBvYmplY3Qgd29uJ3QgZXhpc3QgdW50aWwgYSByb29tXHJcbiAgICAgICAgICAgIC8vIGV4aXN0cyBvbiB0aGUgYWNjb3VudC4gVG8gd29yayBhcm91bmQgdGhpcywgd2UnbGwganVzdCBjcmVhdGUgYSB0ZW1wb3JhcnkgVXNlclxyXG4gICAgICAgICAgICAvLyBhbmQgdXNlIHRoYXQuXHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIlVzZXIgb2JqZWN0IG5vdCBmb3VuZCAtIGNyZWF0aW5nIG9uZSBmb3IgUHJvZmlsZVNldHRpbmdzXCIpO1xyXG4gICAgICAgICAgICB1c2VyID0gbmV3IFVzZXIoY2xpZW50LmdldFVzZXJJZCgpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGF2YXRhclVybCA9IHVzZXIuYXZhdGFyVXJsO1xyXG4gICAgICAgIGlmIChhdmF0YXJVcmwpIGF2YXRhclVybCA9IGNsaWVudC5teGNVcmxUb0h0dHAoYXZhdGFyVXJsLCA5NiwgOTYsICdjcm9wJywgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHVzZXJJZDogdXNlci51c2VySWQsXHJcbiAgICAgICAgICAgIG9yaWdpbmFsRGlzcGxheU5hbWU6IHVzZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICBvcmlnaW5hbEF2YXRhclVybDogYXZhdGFyVXJsLFxyXG4gICAgICAgICAgICBhdmF0YXJVcmw6IGF2YXRhclVybCxcclxuICAgICAgICAgICAgYXZhdGFyRmlsZTogbnVsbCxcclxuICAgICAgICAgICAgZW5hYmxlUHJvZmlsZVNhdmU6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuX2F2YXRhclVwbG9hZCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfVxyXG5cclxuICAgIF91cGxvYWRBdmF0YXIgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fYXZhdGFyVXBsb2FkLmN1cnJlbnQuY2xpY2soKTtcclxuICAgIH07XHJcblxyXG4gICAgX3JlbW92ZUF2YXRhciA9ICgpID0+IHtcclxuICAgICAgICAvLyBjbGVhciBmaWxlIHVwbG9hZCBmaWVsZCBzbyBzYW1lIGZpbGUgY2FuIGJlIHNlbGVjdGVkXHJcbiAgICAgICAgdGhpcy5fYXZhdGFyVXBsb2FkLmN1cnJlbnQudmFsdWUgPSBcIlwiO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBhdmF0YXJVcmw6IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgYXZhdGFyRmlsZTogdW5kZWZpbmVkLFxyXG4gICAgICAgICAgICBlbmFibGVQcm9maWxlU2F2ZTogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3NhdmVQcm9maWxlID0gYXN5bmMgKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmVuYWJsZVByb2ZpbGVTYXZlKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZW5hYmxlUHJvZmlsZVNhdmU6IGZhbHNlfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHt9O1xyXG5cclxuICAgICAgICAvLyBUT0RPOiBXaGF0IGRvIHdlIGRvIGFib3V0IGVycm9ycz9cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUub3JpZ2luYWxEaXNwbGF5TmFtZSAhPT0gdGhpcy5zdGF0ZS5kaXNwbGF5TmFtZSkge1xyXG4gICAgICAgICAgICBhd2FpdCBjbGllbnQuc2V0RGlzcGxheU5hbWUodGhpcy5zdGF0ZS5kaXNwbGF5TmFtZSk7XHJcbiAgICAgICAgICAgIG5ld1N0YXRlLm9yaWdpbmFsRGlzcGxheU5hbWUgPSB0aGlzLnN0YXRlLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuYXZhdGFyRmlsZSkge1xyXG4gICAgICAgICAgICBjb25zdCB1cmkgPSBhd2FpdCBjbGllbnQudXBsb2FkQ29udGVudCh0aGlzLnN0YXRlLmF2YXRhckZpbGUpO1xyXG4gICAgICAgICAgICBhd2FpdCBjbGllbnQuc2V0QXZhdGFyVXJsKHVyaSk7XHJcbiAgICAgICAgICAgIG5ld1N0YXRlLmF2YXRhclVybCA9IGNsaWVudC5teGNVcmxUb0h0dHAodXJpLCA5NiwgOTYsICdjcm9wJywgZmFsc2UpO1xyXG4gICAgICAgICAgICBuZXdTdGF0ZS5vcmlnaW5hbEF2YXRhclVybCA9IG5ld1N0YXRlLmF2YXRhclVybDtcclxuICAgICAgICAgICAgbmV3U3RhdGUuYXZhdGFyRmlsZSA9IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLm9yaWdpbmFsQXZhdGFyVXJsICE9PSB0aGlzLnN0YXRlLmF2YXRhclVybCkge1xyXG4gICAgICAgICAgICBhd2FpdCBjbGllbnQuc2V0QXZhdGFyVXJsKFwiXCIpOyAvLyB1c2UgZW1wdHkgc3RyaW5nIGFzIFN5bmFwc2UgNTAwcyBvbiB1bmRlZmluZWRcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25EaXNwbGF5TmFtZUNoYW5nZWQgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBkaXNwbGF5TmFtZTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgICAgIGVuYWJsZVByb2ZpbGVTYXZlOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25BdmF0YXJDaGFuZ2VkID0gKGUpID0+IHtcclxuICAgICAgICBpZiAoIWUudGFyZ2V0LmZpbGVzIHx8ICFlLnRhcmdldC5maWxlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBhdmF0YXJVcmw6IHRoaXMuc3RhdGUub3JpZ2luYWxBdmF0YXJVcmwsXHJcbiAgICAgICAgICAgICAgICBhdmF0YXJGaWxlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgZW5hYmxlUHJvZmlsZVNhdmU6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xyXG4gICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9IChldikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGF2YXRhclVybDogZXYudGFyZ2V0LnJlc3VsdCxcclxuICAgICAgICAgICAgICAgIGF2YXRhckZpbGU6IGZpbGUsXHJcbiAgICAgICAgICAgICAgICBlbmFibGVQcm9maWxlU2F2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGhvc3RpbmdTaWdudXBMaW5rID0gZ2V0SG9zdGluZ0xpbmsoJ3VzZXItc2V0dGluZ3MnKTtcclxuICAgICAgICBsZXQgaG9zdGluZ1NpZ251cCA9IG51bGw7XHJcbiAgICAgICAgaWYgKGhvc3RpbmdTaWdudXBMaW5rKSB7XHJcbiAgICAgICAgICAgIGhvc3RpbmdTaWdudXAgPSA8c3BhbiBjbGFzc05hbWU9XCJteF9Qcm9maWxlU2V0dGluZ3NfaG9zdGluZ1NpZ251cFwiPlxyXG4gICAgICAgICAgICAgICAge190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiPGE+VXBncmFkZTwvYT4gdG8geW91ciBvd24gZG9tYWluXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYTogc3ViID0+IDxhIGhyZWY9e2hvc3RpbmdTaWdudXBMaW5rfSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCI+e3N1Yn08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPGEgaHJlZj17aG9zdGluZ1NpZ251cExpbmt9IHRhcmdldD1cIl9ibGFua1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZXh0ZXJuYWwtbGluay5zdmdcIil9IHdpZHRoPVwiMTFcIiBoZWlnaHQ9XCIxMFwiIGFsdD0nJyAvPlxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8L3NwYW4+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgICAgICBjb25zdCBBdmF0YXJTZXR0aW5nID0gc2RrLmdldENvbXBvbmVudCgnc2V0dGluZ3MuQXZhdGFyU2V0dGluZycpO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLl9zYXZlUHJvZmlsZX0gYXV0b0NvbXBsZXRlPVwib2ZmXCIgbm9WYWxpZGF0ZT17dHJ1ZX0+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImZpbGVcIiByZWY9e3RoaXMuX2F2YXRhclVwbG9hZH0gY2xhc3NOYW1lPVwibXhfUHJvZmlsZVNldHRpbmdzX2F2YXRhclVwbG9hZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uQXZhdGFyQ2hhbmdlZH0gYWNjZXB0PVwiaW1hZ2UvKlwiIC8+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Byb2ZpbGVTZXR0aW5nc19wcm9maWxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Qcm9maWxlU2V0dGluZ3NfY29udHJvbHNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS51c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7aG9zdGluZ1NpZ251cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8RmllbGQgbGFiZWw9e190KFwiRGlzcGxheSBOYW1lXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiB2YWx1ZT17dGhpcy5zdGF0ZS5kaXNwbGF5TmFtZX0gYXV0b0NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkRpc3BsYXlOYW1lQ2hhbmdlZH0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8QXZhdGFyU2V0dGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJVcmw9e3RoaXMuc3RhdGUuYXZhdGFyVXJsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJOYW1lPXt0aGlzLnN0YXRlLmRpc3BsYXlOYW1lIHx8IHRoaXMuc3RhdGUudXNlcklkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJBbHRUZXh0PXtfdChcIlByb2ZpbGUgcGljdHVyZVwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdXBsb2FkQXZhdGFyPXt0aGlzLl91cGxvYWRBdmF0YXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZUF2YXRhcj17dGhpcy5fcmVtb3ZlQXZhdGFyfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9zYXZlUHJvZmlsZX0ga2luZD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyF0aGlzLnN0YXRlLmVuYWJsZVByb2ZpbGVTYXZlfT5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJTYXZlXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=