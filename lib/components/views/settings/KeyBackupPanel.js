"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard3(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _languageHandler = require("../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

/*
Copyright 2018 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class KeyBackupPanel extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onKeyBackupSessionsRemaining", sessionsRemaining => {
      this.setState({
        sessionsRemaining
      });
    });
    (0, _defineProperty2.default)(this, "_onKeyBackupStatus", () => {
      // This just loads the current backup status rather than forcing
      // a re-check otherwise we risk causing infinite loops
      this._loadBackupStatus();
    });
    (0, _defineProperty2.default)(this, "_startNewBackup", () => {
      _Modal.default.createTrackedDialogAsync('Key Backup', 'Key Backup', Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../async-components/views/dialogs/keybackup/CreateKeyBackupDialog'))), {
        onFinished: () => {
          this._loadBackupStatus();
        }
      }, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
    (0, _defineProperty2.default)(this, "_deleteBackup", () => {
      const QuestionDialog = sdk.getComponent('dialogs.QuestionDialog');

      _Modal.default.createTrackedDialog('Delete Backup', '', QuestionDialog, {
        title: (0, _languageHandler._t)('Delete Backup'),
        description: (0, _languageHandler._t)("Are you sure? You will lose your encrypted messages if your " + "keys are not backed up properly."),
        button: (0, _languageHandler._t)('Delete Backup'),
        danger: true,
        onFinished: proceed => {
          if (!proceed) return;
          this.setState({
            loading: true
          });

          _MatrixClientPeg.MatrixClientPeg.get().deleteKeyBackupVersion(this.state.backupInfo.version).then(() => {
            this._loadBackupStatus();
          });
        }
      });
    });
    (0, _defineProperty2.default)(this, "_restoreBackup", async () => {
      const RestoreKeyBackupDialog = sdk.getComponent('dialogs.keybackup.RestoreKeyBackupDialog');

      _Modal.default.createTrackedDialog('Restore Backup', '', RestoreKeyBackupDialog, null, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
    this._unmounted = false;
    this.state = {
      loading: true,
      error: null,
      backupInfo: null,
      backupSigStatus: null,
      backupKeyStored: null,
      sessionsRemaining: 0
    };
  }

  componentDidMount() {
    this._checkKeyBackupStatus();

    _MatrixClientPeg.MatrixClientPeg.get().on('crypto.keyBackupStatus', this._onKeyBackupStatus);

    _MatrixClientPeg.MatrixClientPeg.get().on('crypto.keyBackupSessionsRemaining', this._onKeyBackupSessionsRemaining);
  }

  componentWillUnmount() {
    this._unmounted = true;

    if (_MatrixClientPeg.MatrixClientPeg.get()) {
      _MatrixClientPeg.MatrixClientPeg.get().removeListener('crypto.keyBackupStatus', this._onKeyBackupStatus);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener('crypto.keyBackupSessionsRemaining', this._onKeyBackupSessionsRemaining);
    }
  }

  async _checkKeyBackupStatus() {
    try {
      const {
        backupInfo,
        trustInfo
      } = await _MatrixClientPeg.MatrixClientPeg.get().checkKeyBackup();
      const backupKeyStored = await _MatrixClientPeg.MatrixClientPeg.get().isKeyBackupKeyStored();
      this.setState({
        backupInfo,
        backupSigStatus: trustInfo,
        backupKeyStored,
        error: null,
        loading: false
      });
    } catch (e) {
      console.log("Unable to fetch check backup status", e);
      if (this._unmounted) return;
      this.setState({
        error: e,
        backupInfo: null,
        backupSigStatus: null,
        backupKeyStored: null,
        loading: false
      });
    }
  }

  async _loadBackupStatus() {
    this.setState({
      loading: true
    });

    try {
      const backupInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
      const backupSigStatus = await _MatrixClientPeg.MatrixClientPeg.get().isKeyBackupTrusted(backupInfo);
      const backupKeyStored = await _MatrixClientPeg.MatrixClientPeg.get().isKeyBackupKeyStored();
      if (this._unmounted) return;
      this.setState({
        error: null,
        backupInfo,
        backupSigStatus,
        backupKeyStored,
        loading: false
      });
    } catch (e) {
      console.log("Unable to fetch key backup status", e);
      if (this._unmounted) return;
      this.setState({
        error: e,
        backupInfo: null,
        backupSigStatus: null,
        backupKeyStored: null,
        loading: false
      });
    }
  }

  render() {
    const Spinner = sdk.getComponent("elements.Spinner");
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    const encryptedMessageAreEncrypted = (0, _languageHandler._t)("Encrypted messages are secured with end-to-end encryption. " + "Only you and the recipient(s) have the keys to read these messages.");

    if (this.state.error) {
      return _react.default.createElement("div", {
        className: "error"
      }, (0, _languageHandler._t)("Unable to load key backup status"));
    } else if (this.state.loading) {
      return _react.default.createElement(Spinner, null);
    } else if (this.state.backupInfo) {
      let clientBackupStatus;
      let restoreButtonCaption = (0, _languageHandler._t)("Restore from Backup");

      if (_MatrixClientPeg.MatrixClientPeg.get().getKeyBackupEnabled()) {
        clientBackupStatus = _react.default.createElement("div", null, _react.default.createElement("p", null, encryptedMessageAreEncrypted), _react.default.createElement("p", null, "\u2705 ", (0, _languageHandler._t)("This session is backing up your keys. ")));
      } else {
        clientBackupStatus = _react.default.createElement("div", null, _react.default.createElement("p", null, encryptedMessageAreEncrypted), _react.default.createElement("p", null, (0, _languageHandler._t)("This session is <b>not backing up your keys</b>, " + "but you do have an existing backup you can restore from " + "and add to going forward.", {}, {
          b: sub => _react.default.createElement("b", null, sub)
        })), _react.default.createElement("p", null, (0, _languageHandler._t)("Connect this session to key backup before signing out to avoid " + "losing any keys that may only be on this session.")));
        restoreButtonCaption = (0, _languageHandler._t)("Connect this session to Key Backup");
      }

      let keyStatus;

      if (this.state.backupKeyStored === true) {
        keyStatus = (0, _languageHandler._t)("in secret storage");
      } else {
        keyStatus = (0, _languageHandler._t)("not stored");
      }

      let uploadStatus;
      const {
        sessionsRemaining
      } = this.state;

      if (!_MatrixClientPeg.MatrixClientPeg.get().getKeyBackupEnabled()) {
        // No upload status to show when backup disabled.
        uploadStatus = "";
      } else if (sessionsRemaining > 0) {
        uploadStatus = _react.default.createElement("div", null, (0, _languageHandler._t)("Backing up %(sessionsRemaining)s keys...", {
          sessionsRemaining
        }), " ", _react.default.createElement("br", null));
      } else {
        uploadStatus = _react.default.createElement("div", null, (0, _languageHandler._t)("All keys backed up"), " ", _react.default.createElement("br", null));
      }

      let backupSigStatuses = this.state.backupSigStatus.sigs.map((sig, i) => {
        const deviceName = sig.device ? sig.device.getDisplayName() || sig.device.deviceId : null;

        const validity = sub => _react.default.createElement("span", {
          className: sig.valid ? 'mx_KeyBackupPanel_sigValid' : 'mx_KeyBackupPanel_sigInvalid'
        }, sub);

        const verify = sub => _react.default.createElement("span", {
          className: sig.device && sig.deviceTrust.isVerified() ? 'mx_KeyBackupPanel_deviceVerified' : 'mx_KeyBackupPanel_deviceNotVerified'
        }, sub);

        const device = sub => _react.default.createElement("span", {
          className: "mx_KeyBackupPanel_deviceName"
        }, deviceName);

        const fromThisDevice = sig.device && sig.device.getFingerprint() === _MatrixClientPeg.MatrixClientPeg.get().getDeviceEd25519Key();

        const fromThisUser = sig.crossSigningId && sig.deviceId === _MatrixClientPeg.MatrixClientPeg.get().getCrossSigningId();

        let sigStatus;

        if (sig.valid && fromThisUser) {
          sigStatus = (0, _languageHandler._t)("Backup has a <validity>valid</validity> signature from this user", {}, {
            validity
          });
        } else if (!sig.valid && fromThisUser) {
          sigStatus = (0, _languageHandler._t)("Backup has a <validity>invalid</validity> signature from this user", {}, {
            validity
          });
        } else if (sig.crossSigningId) {
          sigStatus = (0, _languageHandler._t)("Backup has a signature from <verify>unknown</verify> user with ID %(deviceId)s", {
            deviceId: sig.deviceId
          }, {
            verify
          });
        } else if (!sig.device) {
          sigStatus = (0, _languageHandler._t)("Backup has a signature from <verify>unknown</verify> session with ID %(deviceId)s", {
            deviceId: sig.deviceId
          }, {
            verify
          });
        } else if (sig.valid && fromThisDevice) {
          sigStatus = (0, _languageHandler._t)("Backup has a <validity>valid</validity> signature from this session", {}, {
            validity
          });
        } else if (!sig.valid && fromThisDevice) {
          // it can happen...
          sigStatus = (0, _languageHandler._t)("Backup has an <validity>invalid</validity> signature from this session", {}, {
            validity
          });
        } else if (sig.valid && sig.deviceTrust.isVerified()) {
          sigStatus = (0, _languageHandler._t)("Backup has a <validity>valid</validity> signature from " + "<verify>verified</verify> session <device></device>", {}, {
            validity,
            verify,
            device
          });
        } else if (sig.valid && !sig.deviceTrust.isVerified()) {
          sigStatus = (0, _languageHandler._t)("Backup has a <validity>valid</validity> signature from " + "<verify>unverified</verify> session <device></device>", {}, {
            validity,
            verify,
            device
          });
        } else if (!sig.valid && sig.deviceTrust.isVerified()) {
          sigStatus = (0, _languageHandler._t)("Backup has an <validity>invalid</validity> signature from " + "<verify>verified</verify> session <device></device>", {}, {
            validity,
            verify,
            device
          });
        } else if (!sig.valid && !sig.deviceTrust.isVerified()) {
          sigStatus = (0, _languageHandler._t)("Backup has an <validity>invalid</validity> signature from " + "<verify>unverified</verify> session <device></device>", {}, {
            validity,
            verify,
            device
          });
        }

        return _react.default.createElement("div", {
          key: i
        }, sigStatus);
      });

      if (this.state.backupSigStatus.sigs.length === 0) {
        backupSigStatuses = (0, _languageHandler._t)("Backup is not signed by any of your sessions");
      }

      let trustedLocally;

      if (this.state.backupSigStatus.trusted_locally) {
        trustedLocally = (0, _languageHandler._t)("This backup is trusted because it has been restored on this session");
      }

      let buttonRow = _react.default.createElement("div", {
        className: "mx_KeyBackupPanel_buttonRow"
      }, _react.default.createElement(AccessibleButton, {
        kind: "primary",
        onClick: this._restoreBackup
      }, restoreButtonCaption), "\xA0\xA0\xA0", _react.default.createElement(AccessibleButton, {
        kind: "danger",
        onClick: this._deleteBackup
      }, (0, _languageHandler._t)("Delete Backup")));

      if (this.state.backupKeyStored && !_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
        buttonRow = _react.default.createElement("p", null, "\u26A0\uFE0F ", (0, _languageHandler._t)("Backup key stored in secret storage, but this feature is not " + "enabled on this session. Please enable cross-signing in Labs to " + "modify key backup state."));
      }

      return _react.default.createElement("div", null, _react.default.createElement("div", null, clientBackupStatus), _react.default.createElement("details", null, _react.default.createElement("summary", null, (0, _languageHandler._t)("Advanced")), _react.default.createElement("div", null, (0, _languageHandler._t)("Backup version: "), this.state.backupInfo.version), _react.default.createElement("div", null, (0, _languageHandler._t)("Algorithm: "), this.state.backupInfo.algorithm), _react.default.createElement("div", null, (0, _languageHandler._t)("Backup key stored: "), keyStatus), uploadStatus, _react.default.createElement("div", null, backupSigStatuses), _react.default.createElement("div", null, trustedLocally)), buttonRow);
    } else {
      return _react.default.createElement("div", null, _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your keys are <b>not being backed up from this session</b>.", {}, {
        b: sub => _react.default.createElement("b", null, sub)
      })), _react.default.createElement("p", null, encryptedMessageAreEncrypted), _react.default.createElement("p", null, (0, _languageHandler._t)("Back up your keys before signing out to avoid losing them."))), _react.default.createElement("div", {
        className: "mx_KeyBackupPanel_buttonRow"
      }, _react.default.createElement(AccessibleButton, {
        kind: "primary",
        onClick: this._startNewBackup
      }, (0, _languageHandler._t)("Start using Key Backup"))));
    }
  }

}

exports.default = KeyBackupPanel;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL0tleUJhY2t1cFBhbmVsLmpzIl0sIm5hbWVzIjpbIktleUJhY2t1cFBhbmVsIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInNlc3Npb25zUmVtYWluaW5nIiwic2V0U3RhdGUiLCJfbG9hZEJhY2t1cFN0YXR1cyIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jIiwib25GaW5pc2hlZCIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJidXR0b24iLCJkYW5nZXIiLCJwcm9jZWVkIiwibG9hZGluZyIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImRlbGV0ZUtleUJhY2t1cFZlcnNpb24iLCJzdGF0ZSIsImJhY2t1cEluZm8iLCJ2ZXJzaW9uIiwidGhlbiIsIlJlc3RvcmVLZXlCYWNrdXBEaWFsb2ciLCJfdW5tb3VudGVkIiwiZXJyb3IiLCJiYWNrdXBTaWdTdGF0dXMiLCJiYWNrdXBLZXlTdG9yZWQiLCJjb21wb25lbnREaWRNb3VudCIsIl9jaGVja0tleUJhY2t1cFN0YXR1cyIsIm9uIiwiX29uS2V5QmFja3VwU3RhdHVzIiwiX29uS2V5QmFja3VwU2Vzc2lvbnNSZW1haW5pbmciLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUxpc3RlbmVyIiwidHJ1c3RJbmZvIiwiY2hlY2tLZXlCYWNrdXAiLCJpc0tleUJhY2t1cEtleVN0b3JlZCIsImUiLCJjb25zb2xlIiwibG9nIiwiZ2V0S2V5QmFja3VwVmVyc2lvbiIsImlzS2V5QmFja3VwVHJ1c3RlZCIsInJlbmRlciIsIlNwaW5uZXIiLCJBY2Nlc3NpYmxlQnV0dG9uIiwiZW5jcnlwdGVkTWVzc2FnZUFyZUVuY3J5cHRlZCIsImNsaWVudEJhY2t1cFN0YXR1cyIsInJlc3RvcmVCdXR0b25DYXB0aW9uIiwiZ2V0S2V5QmFja3VwRW5hYmxlZCIsImIiLCJzdWIiLCJrZXlTdGF0dXMiLCJ1cGxvYWRTdGF0dXMiLCJiYWNrdXBTaWdTdGF0dXNlcyIsInNpZ3MiLCJtYXAiLCJzaWciLCJpIiwiZGV2aWNlTmFtZSIsImRldmljZSIsImdldERpc3BsYXlOYW1lIiwiZGV2aWNlSWQiLCJ2YWxpZGl0eSIsInZhbGlkIiwidmVyaWZ5IiwiZGV2aWNlVHJ1c3QiLCJpc1ZlcmlmaWVkIiwiZnJvbVRoaXNEZXZpY2UiLCJnZXRGaW5nZXJwcmludCIsImdldERldmljZUVkMjU1MTlLZXkiLCJmcm9tVGhpc1VzZXIiLCJjcm9zc1NpZ25pbmdJZCIsImdldENyb3NzU2lnbmluZ0lkIiwic2lnU3RhdHVzIiwibGVuZ3RoIiwidHJ1c3RlZExvY2FsbHkiLCJ0cnVzdGVkX2xvY2FsbHkiLCJidXR0b25Sb3ciLCJfcmVzdG9yZUJhY2t1cCIsIl9kZWxldGVCYWNrdXAiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsImFsZ29yaXRobSIsIl9zdGFydE5ld0JhY2t1cCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXlCZSxNQUFNQSxjQUFOLFNBQTZCQyxlQUFNQyxhQUFuQyxDQUFpRDtBQUM1REMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUseUVBb0NjQyxpQkFBRCxJQUF1QjtBQUNuRCxXQUFLQyxRQUFMLENBQWM7QUFDVkQsUUFBQUE7QUFEVSxPQUFkO0FBR0gsS0F4Q2tCO0FBQUEsOERBMENFLE1BQU07QUFDdkI7QUFDQTtBQUNBLFdBQUtFLGlCQUFMO0FBQ0gsS0E5Q2tCO0FBQUEsMkRBbUdELE1BQU07QUFDcEJDLHFCQUFNQyx3QkFBTixDQUErQixZQUEvQixFQUE2QyxZQUE3Qyw2RUFDVyx5RUFEWCxLQUVJO0FBQ0lDLFFBQUFBLFVBQVUsRUFBRSxNQUFNO0FBQ2QsZUFBS0gsaUJBQUw7QUFDSDtBQUhMLE9BRkosRUFNTyxJQU5QO0FBTWE7QUFBaUIsV0FOOUI7QUFNcUM7QUFBZSxVQU5wRDtBQVFILEtBNUdrQjtBQUFBLHlEQThHSCxNQUFNO0FBQ2xCLFlBQU1JLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQUwscUJBQU1NLG1CQUFOLENBQTBCLGVBQTFCLEVBQTJDLEVBQTNDLEVBQStDSCxjQUEvQyxFQUErRDtBQUMzREksUUFBQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FEb0Q7QUFFM0RDLFFBQUFBLFdBQVcsRUFBRSx5QkFDVCxpRUFDQSxrQ0FGUyxDQUY4QztBQU0zREMsUUFBQUEsTUFBTSxFQUFFLHlCQUFHLGVBQUgsQ0FObUQ7QUFPM0RDLFFBQUFBLE1BQU0sRUFBRSxJQVBtRDtBQVEzRFIsUUFBQUEsVUFBVSxFQUFHUyxPQUFELElBQWE7QUFDckIsY0FBSSxDQUFDQSxPQUFMLEVBQWM7QUFDZCxlQUFLYixRQUFMLENBQWM7QUFBQ2MsWUFBQUEsT0FBTyxFQUFFO0FBQVYsV0FBZDs7QUFDQUMsMkNBQWdCQyxHQUFoQixHQUFzQkMsc0JBQXRCLENBQTZDLEtBQUtDLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQkMsT0FBbkUsRUFBNEVDLElBQTVFLENBQWlGLE1BQU07QUFDbkYsaUJBQUtwQixpQkFBTDtBQUNILFdBRkQ7QUFHSDtBQWQwRCxPQUEvRDtBQWdCSCxLQWhJa0I7QUFBQSwwREFrSUYsWUFBWTtBQUN6QixZQUFNcUIsc0JBQXNCLEdBQUdoQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMENBQWpCLENBQS9COztBQUNBTCxxQkFBTU0sbUJBQU4sQ0FDSSxnQkFESixFQUNzQixFQUR0QixFQUMwQmMsc0JBRDFCLEVBQ2tELElBRGxELEVBQ3dELElBRHhEO0FBRUk7QUFBaUIsV0FGckI7QUFFNEI7QUFBZSxVQUYzQztBQUlILEtBeElrQjtBQUdmLFNBQUtDLFVBQUwsR0FBa0IsS0FBbEI7QUFDQSxTQUFLTCxLQUFMLEdBQWE7QUFDVEosTUFBQUEsT0FBTyxFQUFFLElBREE7QUFFVFUsTUFBQUEsS0FBSyxFQUFFLElBRkU7QUFHVEwsTUFBQUEsVUFBVSxFQUFFLElBSEg7QUFJVE0sTUFBQUEsZUFBZSxFQUFFLElBSlI7QUFLVEMsTUFBQUEsZUFBZSxFQUFFLElBTFI7QUFNVDNCLE1BQUFBLGlCQUFpQixFQUFFO0FBTlYsS0FBYjtBQVFIOztBQUVENEIsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsU0FBS0MscUJBQUw7O0FBRUFiLHFDQUFnQkMsR0FBaEIsR0FBc0JhLEVBQXRCLENBQXlCLHdCQUF6QixFQUFtRCxLQUFLQyxrQkFBeEQ7O0FBQ0FmLHFDQUFnQkMsR0FBaEIsR0FBc0JhLEVBQXRCLENBQ0ksbUNBREosRUFFSSxLQUFLRSw2QkFGVDtBQUlIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixTQUFLVCxVQUFMLEdBQWtCLElBQWxCOztBQUVBLFFBQUlSLGlDQUFnQkMsR0FBaEIsRUFBSixFQUEyQjtBQUN2QkQsdUNBQWdCQyxHQUFoQixHQUFzQmlCLGNBQXRCLENBQXFDLHdCQUFyQyxFQUErRCxLQUFLSCxrQkFBcEU7O0FBQ0FmLHVDQUFnQkMsR0FBaEIsR0FBc0JpQixjQUF0QixDQUNJLG1DQURKLEVBRUksS0FBS0YsNkJBRlQ7QUFJSDtBQUNKOztBQWNELFFBQU1ILHFCQUFOLEdBQThCO0FBQzFCLFFBQUk7QUFDQSxZQUFNO0FBQUNULFFBQUFBLFVBQUQ7QUFBYWUsUUFBQUE7QUFBYixVQUEwQixNQUFNbkIsaUNBQWdCQyxHQUFoQixHQUFzQm1CLGNBQXRCLEVBQXRDO0FBQ0EsWUFBTVQsZUFBZSxHQUFHLE1BQU1YLGlDQUFnQkMsR0FBaEIsR0FBc0JvQixvQkFBdEIsRUFBOUI7QUFDQSxXQUFLcEMsUUFBTCxDQUFjO0FBQ1ZtQixRQUFBQSxVQURVO0FBRVZNLFFBQUFBLGVBQWUsRUFBRVMsU0FGUDtBQUdWUixRQUFBQSxlQUhVO0FBSVZGLFFBQUFBLEtBQUssRUFBRSxJQUpHO0FBS1ZWLFFBQUFBLE9BQU8sRUFBRTtBQUxDLE9BQWQ7QUFPSCxLQVZELENBVUUsT0FBT3VCLENBQVAsRUFBVTtBQUNSQyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxxQ0FBWixFQUFtREYsQ0FBbkQ7QUFDQSxVQUFJLEtBQUtkLFVBQVQsRUFBcUI7QUFDckIsV0FBS3ZCLFFBQUwsQ0FBYztBQUNWd0IsUUFBQUEsS0FBSyxFQUFFYSxDQURHO0FBRVZsQixRQUFBQSxVQUFVLEVBQUUsSUFGRjtBQUdWTSxRQUFBQSxlQUFlLEVBQUUsSUFIUDtBQUlWQyxRQUFBQSxlQUFlLEVBQUUsSUFKUDtBQUtWWixRQUFBQSxPQUFPLEVBQUU7QUFMQyxPQUFkO0FBT0g7QUFDSjs7QUFFRCxRQUFNYixpQkFBTixHQUEwQjtBQUN0QixTQUFLRCxRQUFMLENBQWM7QUFBQ2MsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBZDs7QUFDQSxRQUFJO0FBQ0EsWUFBTUssVUFBVSxHQUFHLE1BQU1KLGlDQUFnQkMsR0FBaEIsR0FBc0J3QixtQkFBdEIsRUFBekI7QUFDQSxZQUFNZixlQUFlLEdBQUcsTUFBTVYsaUNBQWdCQyxHQUFoQixHQUFzQnlCLGtCQUF0QixDQUF5Q3RCLFVBQXpDLENBQTlCO0FBQ0EsWUFBTU8sZUFBZSxHQUFHLE1BQU1YLGlDQUFnQkMsR0FBaEIsR0FBc0JvQixvQkFBdEIsRUFBOUI7QUFDQSxVQUFJLEtBQUtiLFVBQVQsRUFBcUI7QUFDckIsV0FBS3ZCLFFBQUwsQ0FBYztBQUNWd0IsUUFBQUEsS0FBSyxFQUFFLElBREc7QUFFVkwsUUFBQUEsVUFGVTtBQUdWTSxRQUFBQSxlQUhVO0FBSVZDLFFBQUFBLGVBSlU7QUFLVlosUUFBQUEsT0FBTyxFQUFFO0FBTEMsT0FBZDtBQU9ILEtBWkQsQ0FZRSxPQUFPdUIsQ0FBUCxFQUFVO0FBQ1JDLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG1DQUFaLEVBQWlERixDQUFqRDtBQUNBLFVBQUksS0FBS2QsVUFBVCxFQUFxQjtBQUNyQixXQUFLdkIsUUFBTCxDQUFjO0FBQ1Z3QixRQUFBQSxLQUFLLEVBQUVhLENBREc7QUFFVmxCLFFBQUFBLFVBQVUsRUFBRSxJQUZGO0FBR1ZNLFFBQUFBLGVBQWUsRUFBRSxJQUhQO0FBSVZDLFFBQUFBLGVBQWUsRUFBRSxJQUpQO0FBS1ZaLFFBQUFBLE9BQU8sRUFBRTtBQUxDLE9BQWQ7QUFPSDtBQUNKOztBQXlDRDRCLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE9BQU8sR0FBR3JDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxVQUFNcUMsZ0JBQWdCLEdBQUd0QyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTXNDLDRCQUE0QixHQUFHLHlCQUNqQyxnRUFDQSxxRUFGaUMsQ0FBckM7O0FBS0EsUUFBSSxLQUFLM0IsS0FBTCxDQUFXTSxLQUFmLEVBQXNCO0FBQ2xCLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0sseUJBQUcsa0NBQUgsQ0FETCxDQURKO0FBS0gsS0FORCxNQU1PLElBQUksS0FBS04sS0FBTCxDQUFXSixPQUFmLEVBQXdCO0FBQzNCLGFBQU8sNkJBQUMsT0FBRCxPQUFQO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBS0ksS0FBTCxDQUFXQyxVQUFmLEVBQTJCO0FBQzlCLFVBQUkyQixrQkFBSjtBQUNBLFVBQUlDLG9CQUFvQixHQUFHLHlCQUFHLHFCQUFILENBQTNCOztBQUVBLFVBQUloQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCZ0MsbUJBQXRCLEVBQUosRUFBaUQ7QUFDN0NGLFFBQUFBLGtCQUFrQixHQUFHLDBDQUNqQix3Q0FBSUQsNEJBQUosQ0FEaUIsRUFFakIsbURBQU0seUJBQUcsd0NBQUgsQ0FBTixDQUZpQixDQUFyQjtBQUlILE9BTEQsTUFLTztBQUNIQyxRQUFBQSxrQkFBa0IsR0FBRywwQ0FDakIsd0NBQUlELDRCQUFKLENBRGlCLEVBRWpCLHdDQUFJLHlCQUNBLHNEQUNBLDBEQURBLEdBRUEsMkJBSEEsRUFHNkIsRUFIN0IsRUFJQTtBQUFDSSxVQUFBQSxDQUFDLEVBQUVDLEdBQUcsSUFBSSx3Q0FBSUEsR0FBSjtBQUFYLFNBSkEsQ0FBSixDQUZpQixFQVFqQix3Q0FBSSx5QkFDQSxvRUFDQSxtREFGQSxDQUFKLENBUmlCLENBQXJCO0FBYUFILFFBQUFBLG9CQUFvQixHQUFHLHlCQUFHLG9DQUFILENBQXZCO0FBQ0g7O0FBRUQsVUFBSUksU0FBSjs7QUFDQSxVQUFJLEtBQUtqQyxLQUFMLENBQVdRLGVBQVgsS0FBK0IsSUFBbkMsRUFBeUM7QUFDckN5QixRQUFBQSxTQUFTLEdBQUcseUJBQUcsbUJBQUgsQ0FBWjtBQUNILE9BRkQsTUFFTztBQUNIQSxRQUFBQSxTQUFTLEdBQUcseUJBQUcsWUFBSCxDQUFaO0FBQ0g7O0FBRUQsVUFBSUMsWUFBSjtBQUNBLFlBQU07QUFBRXJELFFBQUFBO0FBQUYsVUFBd0IsS0FBS21CLEtBQW5DOztBQUNBLFVBQUksQ0FBQ0gsaUNBQWdCQyxHQUFoQixHQUFzQmdDLG1CQUF0QixFQUFMLEVBQWtEO0FBQzlDO0FBQ0FJLFFBQUFBLFlBQVksR0FBRyxFQUFmO0FBQ0gsT0FIRCxNQUdPLElBQUlyRCxpQkFBaUIsR0FBRyxDQUF4QixFQUEyQjtBQUM5QnFELFFBQUFBLFlBQVksR0FBRywwQ0FDVix5QkFBRywwQ0FBSCxFQUErQztBQUFFckQsVUFBQUE7QUFBRixTQUEvQyxDQURVLE9BQzZELHdDQUQ3RCxDQUFmO0FBR0gsT0FKTSxNQUlBO0FBQ0hxRCxRQUFBQSxZQUFZLEdBQUcsMENBQ1YseUJBQUcsb0JBQUgsQ0FEVSxPQUNnQix3Q0FEaEIsQ0FBZjtBQUdIOztBQUVELFVBQUlDLGlCQUFpQixHQUFHLEtBQUtuQyxLQUFMLENBQVdPLGVBQVgsQ0FBMkI2QixJQUEzQixDQUFnQ0MsR0FBaEMsQ0FBb0MsQ0FBQ0MsR0FBRCxFQUFNQyxDQUFOLEtBQVk7QUFDcEUsY0FBTUMsVUFBVSxHQUFHRixHQUFHLENBQUNHLE1BQUosR0FBY0gsR0FBRyxDQUFDRyxNQUFKLENBQVdDLGNBQVgsTUFBK0JKLEdBQUcsQ0FBQ0csTUFBSixDQUFXRSxRQUF4RCxHQUFvRSxJQUF2Rjs7QUFDQSxjQUFNQyxRQUFRLEdBQUdaLEdBQUcsSUFDaEI7QUFBTSxVQUFBLFNBQVMsRUFBRU0sR0FBRyxDQUFDTyxLQUFKLEdBQVksNEJBQVosR0FBMkM7QUFBNUQsV0FDS2IsR0FETCxDQURKOztBQUlBLGNBQU1jLE1BQU0sR0FBR2QsR0FBRyxJQUNkO0FBQU0sVUFBQSxTQUFTLEVBQUVNLEdBQUcsQ0FBQ0csTUFBSixJQUFjSCxHQUFHLENBQUNTLFdBQUosQ0FBZ0JDLFVBQWhCLEVBQWQsR0FBNkMsa0NBQTdDLEdBQWtGO0FBQW5HLFdBQ0toQixHQURMLENBREo7O0FBSUEsY0FBTVMsTUFBTSxHQUFHVCxHQUFHLElBQUk7QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixXQUFnRFEsVUFBaEQsQ0FBdEI7O0FBQ0EsY0FBTVMsY0FBYyxHQUNoQlgsR0FBRyxDQUFDRyxNQUFKLElBQ0FILEdBQUcsQ0FBQ0csTUFBSixDQUFXUyxjQUFYLE9BQWdDckQsaUNBQWdCQyxHQUFoQixHQUFzQnFELG1CQUF0QixFQUZwQzs7QUFJQSxjQUFNQyxZQUFZLEdBQ2RkLEdBQUcsQ0FBQ2UsY0FBSixJQUNBZixHQUFHLENBQUNLLFFBQUosS0FBaUI5QyxpQ0FBZ0JDLEdBQWhCLEdBQXNCd0QsaUJBQXRCLEVBRnJCOztBQUlBLFlBQUlDLFNBQUo7O0FBQ0EsWUFBSWpCLEdBQUcsQ0FBQ08sS0FBSixJQUFhTyxZQUFqQixFQUErQjtBQUMzQkcsVUFBQUEsU0FBUyxHQUFHLHlCQUNSLGtFQURRLEVBRVIsRUFGUSxFQUVKO0FBQUVYLFlBQUFBO0FBQUYsV0FGSSxDQUFaO0FBSUgsU0FMRCxNQUtPLElBQUksQ0FBQ04sR0FBRyxDQUFDTyxLQUFMLElBQWNPLFlBQWxCLEVBQWdDO0FBQ25DRyxVQUFBQSxTQUFTLEdBQUcseUJBQ1Isb0VBRFEsRUFFUixFQUZRLEVBRUo7QUFBRVgsWUFBQUE7QUFBRixXQUZJLENBQVo7QUFJSCxTQUxNLE1BS0EsSUFBSU4sR0FBRyxDQUFDZSxjQUFSLEVBQXdCO0FBQzNCRSxVQUFBQSxTQUFTLEdBQUcseUJBQ1IsZ0ZBRFEsRUFFUjtBQUFFWixZQUFBQSxRQUFRLEVBQUVMLEdBQUcsQ0FBQ0s7QUFBaEIsV0FGUSxFQUVvQjtBQUFFRyxZQUFBQTtBQUFGLFdBRnBCLENBQVo7QUFJSCxTQUxNLE1BS0EsSUFBSSxDQUFDUixHQUFHLENBQUNHLE1BQVQsRUFBaUI7QUFDcEJjLFVBQUFBLFNBQVMsR0FBRyx5QkFDUixtRkFEUSxFQUVSO0FBQUVaLFlBQUFBLFFBQVEsRUFBRUwsR0FBRyxDQUFDSztBQUFoQixXQUZRLEVBRW9CO0FBQUVHLFlBQUFBO0FBQUYsV0FGcEIsQ0FBWjtBQUlILFNBTE0sTUFLQSxJQUFJUixHQUFHLENBQUNPLEtBQUosSUFBYUksY0FBakIsRUFBaUM7QUFDcENNLFVBQUFBLFNBQVMsR0FBRyx5QkFDUixxRUFEUSxFQUVSLEVBRlEsRUFFSjtBQUFFWCxZQUFBQTtBQUFGLFdBRkksQ0FBWjtBQUlILFNBTE0sTUFLQSxJQUFJLENBQUNOLEdBQUcsQ0FBQ08sS0FBTCxJQUFjSSxjQUFsQixFQUFrQztBQUNyQztBQUNBTSxVQUFBQSxTQUFTLEdBQUcseUJBQ1Isd0VBRFEsRUFFUixFQUZRLEVBRUo7QUFBRVgsWUFBQUE7QUFBRixXQUZJLENBQVo7QUFJSCxTQU5NLE1BTUEsSUFBSU4sR0FBRyxDQUFDTyxLQUFKLElBQWFQLEdBQUcsQ0FBQ1MsV0FBSixDQUFnQkMsVUFBaEIsRUFBakIsRUFBK0M7QUFDbERPLFVBQUFBLFNBQVMsR0FBRyx5QkFDUiw0REFDQSxxREFGUSxFQUdSLEVBSFEsRUFHSjtBQUFFWCxZQUFBQSxRQUFGO0FBQVlFLFlBQUFBLE1BQVo7QUFBb0JMLFlBQUFBO0FBQXBCLFdBSEksQ0FBWjtBQUtILFNBTk0sTUFNQSxJQUFJSCxHQUFHLENBQUNPLEtBQUosSUFBYSxDQUFDUCxHQUFHLENBQUNTLFdBQUosQ0FBZ0JDLFVBQWhCLEVBQWxCLEVBQWdEO0FBQ25ETyxVQUFBQSxTQUFTLEdBQUcseUJBQ1IsNERBQ0EsdURBRlEsRUFHUixFQUhRLEVBR0o7QUFBRVgsWUFBQUEsUUFBRjtBQUFZRSxZQUFBQSxNQUFaO0FBQW9CTCxZQUFBQTtBQUFwQixXQUhJLENBQVo7QUFLSCxTQU5NLE1BTUEsSUFBSSxDQUFDSCxHQUFHLENBQUNPLEtBQUwsSUFBY1AsR0FBRyxDQUFDUyxXQUFKLENBQWdCQyxVQUFoQixFQUFsQixFQUFnRDtBQUNuRE8sVUFBQUEsU0FBUyxHQUFHLHlCQUNSLCtEQUNBLHFEQUZRLEVBR1IsRUFIUSxFQUdKO0FBQUVYLFlBQUFBLFFBQUY7QUFBWUUsWUFBQUEsTUFBWjtBQUFvQkwsWUFBQUE7QUFBcEIsV0FISSxDQUFaO0FBS0gsU0FOTSxNQU1BLElBQUksQ0FBQ0gsR0FBRyxDQUFDTyxLQUFMLElBQWMsQ0FBQ1AsR0FBRyxDQUFDUyxXQUFKLENBQWdCQyxVQUFoQixFQUFuQixFQUFpRDtBQUNwRE8sVUFBQUEsU0FBUyxHQUFHLHlCQUNSLCtEQUNBLHVEQUZRLEVBR1IsRUFIUSxFQUdKO0FBQUVYLFlBQUFBLFFBQUY7QUFBWUUsWUFBQUEsTUFBWjtBQUFvQkwsWUFBQUE7QUFBcEIsV0FISSxDQUFaO0FBS0g7O0FBRUQsZUFBTztBQUFLLFVBQUEsR0FBRyxFQUFFRjtBQUFWLFdBQ0ZnQixTQURFLENBQVA7QUFHSCxPQWhGdUIsQ0FBeEI7O0FBaUZBLFVBQUksS0FBS3ZELEtBQUwsQ0FBV08sZUFBWCxDQUEyQjZCLElBQTNCLENBQWdDb0IsTUFBaEMsS0FBMkMsQ0FBL0MsRUFBa0Q7QUFDOUNyQixRQUFBQSxpQkFBaUIsR0FBRyx5QkFBRyw4Q0FBSCxDQUFwQjtBQUNIOztBQUVELFVBQUlzQixjQUFKOztBQUNBLFVBQUksS0FBS3pELEtBQUwsQ0FBV08sZUFBWCxDQUEyQm1ELGVBQS9CLEVBQWdEO0FBQzVDRCxRQUFBQSxjQUFjLEdBQUcseUJBQUcscUVBQUgsQ0FBakI7QUFDSDs7QUFFRCxVQUFJRSxTQUFTLEdBQ1Q7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsUUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBL0MsU0FDSy9CLG9CQURMLENBREosa0JBSUksNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsUUFBdkI7QUFBZ0MsUUFBQSxPQUFPLEVBQUUsS0FBS2dDO0FBQTlDLFNBQ0sseUJBQUcsZUFBSCxDQURMLENBSkosQ0FESjs7QUFVQSxVQUFJLEtBQUs3RCxLQUFMLENBQVdRLGVBQVgsSUFBOEIsQ0FBQ3NELHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBbkMsRUFBNEY7QUFDeEZKLFFBQUFBLFNBQVMsR0FBRyx5REFBTyx5QkFDZixrRUFDQSxrRUFEQSxHQUVBLDBCQUhlLENBQVAsQ0FBWjtBQUtIOztBQUVELGFBQU8sMENBQ0gsMENBQU0vQixrQkFBTixDQURHLEVBRUgsOENBQ0ksOENBQVUseUJBQUcsVUFBSCxDQUFWLENBREosRUFFSSwwQ0FBTSx5QkFBRyxrQkFBSCxDQUFOLEVBQThCLEtBQUs1QixLQUFMLENBQVdDLFVBQVgsQ0FBc0JDLE9BQXBELENBRkosRUFHSSwwQ0FBTSx5QkFBRyxhQUFILENBQU4sRUFBeUIsS0FBS0YsS0FBTCxDQUFXQyxVQUFYLENBQXNCK0QsU0FBL0MsQ0FISixFQUlJLDBDQUFNLHlCQUFHLHFCQUFILENBQU4sRUFBaUMvQixTQUFqQyxDQUpKLEVBS0tDLFlBTEwsRUFNSSwwQ0FBTUMsaUJBQU4sQ0FOSixFQU9JLDBDQUFNc0IsY0FBTixDQVBKLENBRkcsRUFXRkUsU0FYRSxDQUFQO0FBYUgsS0F6S00sTUF5S0E7QUFDSCxhQUFPLDBDQUNILDBDQUNJLHdDQUFJLHlCQUNBLDZEQURBLEVBQytELEVBRC9ELEVBRUE7QUFBQzVCLFFBQUFBLENBQUMsRUFBRUMsR0FBRyxJQUFJLHdDQUFJQSxHQUFKO0FBQVgsT0FGQSxDQUFKLENBREosRUFLSSx3Q0FBSUwsNEJBQUosQ0FMSixFQU1JLHdDQUFJLHlCQUFHLDREQUFILENBQUosQ0FOSixDQURHLEVBU0g7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsUUFBQSxPQUFPLEVBQUUsS0FBS3NDO0FBQS9DLFNBQ0sseUJBQUcsd0JBQUgsQ0FETCxDQURKLENBVEcsQ0FBUDtBQWVIO0FBQ0o7O0FBclYyRCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gJy4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmUnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgS2V5QmFja3VwUGFuZWwgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBsb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICAgICAgYmFja3VwSW5mbzogbnVsbCxcclxuICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzOiBudWxsLFxyXG4gICAgICAgICAgICBiYWNrdXBLZXlTdG9yZWQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNlc3Npb25zUmVtYWluaW5nOiAwLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5fY2hlY2tLZXlCYWNrdXBTdGF0dXMoKTtcclxuXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKCdjcnlwdG8ua2V5QmFja3VwU3RhdHVzJywgdGhpcy5fb25LZXlCYWNrdXBTdGF0dXMpO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbihcclxuICAgICAgICAgICAgJ2NyeXB0by5rZXlCYWNrdXBTZXNzaW9uc1JlbWFpbmluZycsXHJcbiAgICAgICAgICAgIHRoaXMuX29uS2V5QmFja3VwU2Vzc2lvbnNSZW1haW5pbmcsXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICB0aGlzLl91bm1vdW50ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpKSB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcignY3J5cHRvLmtleUJhY2t1cFN0YXR1cycsIHRoaXMuX29uS2V5QmFja3VwU3RhdHVzKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKFxyXG4gICAgICAgICAgICAgICAgJ2NyeXB0by5rZXlCYWNrdXBTZXNzaW9uc1JlbWFpbmluZycsXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9vbktleUJhY2t1cFNlc3Npb25zUmVtYWluaW5nLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25LZXlCYWNrdXBTZXNzaW9uc1JlbWFpbmluZyA9IChzZXNzaW9uc1JlbWFpbmluZykgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzZXNzaW9uc1JlbWFpbmluZyxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25LZXlCYWNrdXBTdGF0dXMgPSAoKSA9PiB7XHJcbiAgICAgICAgLy8gVGhpcyBqdXN0IGxvYWRzIHRoZSBjdXJyZW50IGJhY2t1cCBzdGF0dXMgcmF0aGVyIHRoYW4gZm9yY2luZ1xyXG4gICAgICAgIC8vIGEgcmUtY2hlY2sgb3RoZXJ3aXNlIHdlIHJpc2sgY2F1c2luZyBpbmZpbml0ZSBsb29wc1xyXG4gICAgICAgIHRoaXMuX2xvYWRCYWNrdXBTdGF0dXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfY2hlY2tLZXlCYWNrdXBTdGF0dXMoKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3Qge2JhY2t1cEluZm8sIHRydXN0SW5mb30gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY2hlY2tLZXlCYWNrdXAoKTtcclxuICAgICAgICAgICAgY29uc3QgYmFja3VwS2V5U3RvcmVkID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzS2V5QmFja3VwS2V5U3RvcmVkKCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYmFja3VwSW5mbyxcclxuICAgICAgICAgICAgICAgIGJhY2t1cFNpZ1N0YXR1czogdHJ1c3RJbmZvLFxyXG4gICAgICAgICAgICAgICAgYmFja3VwS2V5U3RvcmVkLFxyXG4gICAgICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlVuYWJsZSB0byBmZXRjaCBjaGVjayBiYWNrdXAgc3RhdHVzXCIsIGUpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3I6IGUsXHJcbiAgICAgICAgICAgICAgICBiYWNrdXBJbmZvOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgYmFja3VwS2V5U3RvcmVkOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfbG9hZEJhY2t1cFN0YXR1cygpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtsb2FkaW5nOiB0cnVlfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgYmFja3VwSW5mbyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRLZXlCYWNrdXBWZXJzaW9uKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhY2t1cFNpZ1N0YXR1cyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0tleUJhY2t1cFRydXN0ZWQoYmFja3VwSW5mbyk7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhY2t1cEtleVN0b3JlZCA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0tleUJhY2t1cEtleVN0b3JlZCgpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBiYWNrdXBJbmZvLFxyXG4gICAgICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgYmFja3VwS2V5U3RvcmVkLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZmV0Y2gga2V5IGJhY2t1cCBzdGF0dXNcIiwgZSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl91bm1vdW50ZWQpIHJldHVybjtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZSxcclxuICAgICAgICAgICAgICAgIGJhY2t1cEluZm86IG51bGwsXHJcbiAgICAgICAgICAgICAgICBiYWNrdXBTaWdTdGF0dXM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBiYWNrdXBLZXlTdG9yZWQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9zdGFydE5ld0JhY2t1cCA9ICgpID0+IHtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMoJ0tleSBCYWNrdXAnLCAnS2V5IEJhY2t1cCcsXHJcbiAgICAgICAgICAgIGltcG9ydCgnLi4vLi4vLi4vYXN5bmMtY29tcG9uZW50cy92aWV3cy9kaWFsb2dzL2tleWJhY2t1cC9DcmVhdGVLZXlCYWNrdXBEaWFsb2cnKSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZDogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2xvYWRCYWNrdXBTdGF0dXMoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUsXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfZGVsZXRlQmFja3VwID0gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5RdWVzdGlvbkRpYWxvZycpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0RlbGV0ZSBCYWNrdXAnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgdGl0bGU6IF90KCdEZWxldGUgQmFja3VwJyksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcclxuICAgICAgICAgICAgICAgIFwiQXJlIHlvdSBzdXJlPyBZb3Ugd2lsbCBsb3NlIHlvdXIgZW5jcnlwdGVkIG1lc3NhZ2VzIGlmIHlvdXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJrZXlzIGFyZSBub3QgYmFja2VkIHVwIHByb3Blcmx5LlwiLFxyXG4gICAgICAgICAgICApLFxyXG4gICAgICAgICAgICBidXR0b246IF90KCdEZWxldGUgQmFja3VwJyksXHJcbiAgICAgICAgICAgIGRhbmdlcjogdHJ1ZSxcclxuICAgICAgICAgICAgb25GaW5pc2hlZDogKHByb2NlZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghcHJvY2VlZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bG9hZGluZzogdHJ1ZX0pO1xyXG4gICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRlbGV0ZUtleUJhY2t1cFZlcnNpb24odGhpcy5zdGF0ZS5iYWNrdXBJbmZvLnZlcnNpb24pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2xvYWRCYWNrdXBTdGF0dXMoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZXN0b3JlQmFja3VwID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IFJlc3RvcmVLZXlCYWNrdXBEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLmtleWJhY2t1cC5SZXN0b3JlS2V5QmFja3VwRGlhbG9nJyk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgJ1Jlc3RvcmUgQmFja3VwJywgJycsIFJlc3RvcmVLZXlCYWNrdXBEaWFsb2csIG51bGwsIG51bGwsXHJcbiAgICAgICAgICAgIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUsXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvblwiKTtcclxuICAgICAgICBjb25zdCBlbmNyeXB0ZWRNZXNzYWdlQXJlRW5jcnlwdGVkID0gX3QoXHJcbiAgICAgICAgICAgIFwiRW5jcnlwdGVkIG1lc3NhZ2VzIGFyZSBzZWN1cmVkIHdpdGggZW5kLXRvLWVuZCBlbmNyeXB0aW9uLiBcIiArXHJcbiAgICAgICAgICAgIFwiT25seSB5b3UgYW5kIHRoZSByZWNpcGllbnQocykgaGF2ZSB0aGUga2V5cyB0byByZWFkIHRoZXNlIG1lc3NhZ2VzLlwiLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmVycm9yKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImVycm9yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiVW5hYmxlIHRvIGxvYWQga2V5IGJhY2t1cCBzdGF0dXNcIil9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUubG9hZGluZykge1xyXG4gICAgICAgICAgICByZXR1cm4gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmJhY2t1cEluZm8pIHtcclxuICAgICAgICAgICAgbGV0IGNsaWVudEJhY2t1cFN0YXR1cztcclxuICAgICAgICAgICAgbGV0IHJlc3RvcmVCdXR0b25DYXB0aW9uID0gX3QoXCJSZXN0b3JlIGZyb20gQmFja3VwXCIpO1xyXG5cclxuICAgICAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRLZXlCYWNrdXBFbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgICAgIGNsaWVudEJhY2t1cFN0YXR1cyA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+e2VuY3J5cHRlZE1lc3NhZ2VBcmVFbmNyeXB0ZWR9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPuKchSB7X3QoXCJUaGlzIHNlc3Npb24gaXMgYmFja2luZyB1cCB5b3VyIGtleXMuIFwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjbGllbnRCYWNrdXBTdGF0dXMgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntlbmNyeXB0ZWRNZXNzYWdlQXJlRW5jcnlwdGVkfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhpcyBzZXNzaW9uIGlzIDxiPm5vdCBiYWNraW5nIHVwIHlvdXIga2V5czwvYj4sIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJidXQgeW91IGRvIGhhdmUgYW4gZXhpc3RpbmcgYmFja3VwIHlvdSBjYW4gcmVzdG9yZSBmcm9tIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJhbmQgYWRkIHRvIGdvaW5nIGZvcndhcmQuXCIsIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Yjogc3ViID0+IDxiPntzdWJ9PC9iPn0sXHJcbiAgICAgICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIkNvbm5lY3QgdGhpcyBzZXNzaW9uIHRvIGtleSBiYWNrdXAgYmVmb3JlIHNpZ25pbmcgb3V0IHRvIGF2b2lkIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJsb3NpbmcgYW55IGtleXMgdGhhdCBtYXkgb25seSBiZSBvbiB0aGlzIHNlc3Npb24uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgICAgICByZXN0b3JlQnV0dG9uQ2FwdGlvbiA9IF90KFwiQ29ubmVjdCB0aGlzIHNlc3Npb24gdG8gS2V5IEJhY2t1cFwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IGtleVN0YXR1cztcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuYmFja3VwS2V5U3RvcmVkID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICBrZXlTdGF0dXMgPSBfdChcImluIHNlY3JldCBzdG9yYWdlXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAga2V5U3RhdHVzID0gX3QoXCJub3Qgc3RvcmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgdXBsb2FkU3RhdHVzO1xyXG4gICAgICAgICAgICBjb25zdCB7IHNlc3Npb25zUmVtYWluaW5nIH0gPSB0aGlzLnN0YXRlO1xyXG4gICAgICAgICAgICBpZiAoIU1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRLZXlCYWNrdXBFbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgICAgIC8vIE5vIHVwbG9hZCBzdGF0dXMgdG8gc2hvdyB3aGVuIGJhY2t1cCBkaXNhYmxlZC5cclxuICAgICAgICAgICAgICAgIHVwbG9hZFN0YXR1cyA9IFwiXCI7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoc2Vzc2lvbnNSZW1haW5pbmcgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWRTdGF0dXMgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIkJhY2tpbmcgdXAgJShzZXNzaW9uc1JlbWFpbmluZylzIGtleXMuLi5cIiwgeyBzZXNzaW9uc1JlbWFpbmluZyB9KX0gPGJyIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWRTdGF0dXMgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIkFsbCBrZXlzIGJhY2tlZCB1cFwiKX0gPGJyIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBiYWNrdXBTaWdTdGF0dXNlcyA9IHRoaXMuc3RhdGUuYmFja3VwU2lnU3RhdHVzLnNpZ3MubWFwKChzaWcsIGkpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRldmljZU5hbWUgPSBzaWcuZGV2aWNlID8gKHNpZy5kZXZpY2UuZ2V0RGlzcGxheU5hbWUoKSB8fCBzaWcuZGV2aWNlLmRldmljZUlkKSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2YWxpZGl0eSA9IHN1YiA9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17c2lnLnZhbGlkID8gJ214X0tleUJhY2t1cFBhbmVsX3NpZ1ZhbGlkJyA6ICdteF9LZXlCYWNrdXBQYW5lbF9zaWdJbnZhbGlkJ30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtzdWJ9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPjtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHZlcmlmeSA9IHN1YiA9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17c2lnLmRldmljZSAmJiBzaWcuZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpID8gJ214X0tleUJhY2t1cFBhbmVsX2RldmljZVZlcmlmaWVkJyA6ICdteF9LZXlCYWNrdXBQYW5lbF9kZXZpY2VOb3RWZXJpZmllZCd9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c3VifVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj47XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkZXZpY2UgPSBzdWIgPT4gPHNwYW4gY2xhc3NOYW1lPVwibXhfS2V5QmFja3VwUGFuZWxfZGV2aWNlTmFtZVwiPntkZXZpY2VOYW1lfTwvc3Bhbj47XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmcm9tVGhpc0RldmljZSA9IChcclxuICAgICAgICAgICAgICAgICAgICBzaWcuZGV2aWNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgc2lnLmRldmljZS5nZXRGaW5nZXJwcmludCgpID09PSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0RGV2aWNlRWQyNTUxOUtleSgpXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZnJvbVRoaXNVc2VyID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIHNpZy5jcm9zc1NpZ25pbmdJZCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHNpZy5kZXZpY2VJZCA9PT0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldENyb3NzU2lnbmluZ0lkKClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBsZXQgc2lnU3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNpZy52YWxpZCAmJiBmcm9tVGhpc1VzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBzaWdTdGF0dXMgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJCYWNrdXAgaGFzIGEgPHZhbGlkaXR5PnZhbGlkPC92YWxpZGl0eT4gc2lnbmF0dXJlIGZyb20gdGhpcyB1c2VyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt9LCB7IHZhbGlkaXR5IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXNpZy52YWxpZCAmJiBmcm9tVGhpc1VzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBzaWdTdGF0dXMgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJCYWNrdXAgaGFzIGEgPHZhbGlkaXR5PmludmFsaWQ8L3ZhbGlkaXR5PiBzaWduYXR1cmUgZnJvbSB0aGlzIHVzZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAge30sIHsgdmFsaWRpdHkgfSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzaWcuY3Jvc3NTaWduaW5nSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBzaWdTdGF0dXMgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJCYWNrdXAgaGFzIGEgc2lnbmF0dXJlIGZyb20gPHZlcmlmeT51bmtub3duPC92ZXJpZnk+IHVzZXIgd2l0aCBJRCAlKGRldmljZUlkKXNcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBkZXZpY2VJZDogc2lnLmRldmljZUlkIH0sIHsgdmVyaWZ5IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXNpZy5kZXZpY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBzaWdTdGF0dXMgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJCYWNrdXAgaGFzIGEgc2lnbmF0dXJlIGZyb20gPHZlcmlmeT51bmtub3duPC92ZXJpZnk+IHNlc3Npb24gd2l0aCBJRCAlKGRldmljZUlkKXNcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBkZXZpY2VJZDogc2lnLmRldmljZUlkIH0sIHsgdmVyaWZ5IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc2lnLnZhbGlkICYmIGZyb21UaGlzRGV2aWNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2lnU3RhdHVzID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGhhcyBhIDx2YWxpZGl0eT52YWxpZDwvdmFsaWRpdHk+IHNpZ25hdHVyZSBmcm9tIHRoaXMgc2Vzc2lvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7fSwgeyB2YWxpZGl0eSB9LFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCFzaWcudmFsaWQgJiYgZnJvbVRoaXNEZXZpY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBpdCBjYW4gaGFwcGVuLi4uXHJcbiAgICAgICAgICAgICAgICAgICAgc2lnU3RhdHVzID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGhhcyBhbiA8dmFsaWRpdHk+aW52YWxpZDwvdmFsaWRpdHk+IHNpZ25hdHVyZSBmcm9tIHRoaXMgc2Vzc2lvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7fSwgeyB2YWxpZGl0eSB9LFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHNpZy52YWxpZCAmJiBzaWcuZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2lnU3RhdHVzID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGhhcyBhIDx2YWxpZGl0eT52YWxpZDwvdmFsaWRpdHk+IHNpZ25hdHVyZSBmcm9tIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8dmVyaWZ5PnZlcmlmaWVkPC92ZXJpZnk+IHNlc3Npb24gPGRldmljZT48L2RldmljZT5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAge30sIHsgdmFsaWRpdHksIHZlcmlmeSwgZGV2aWNlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc2lnLnZhbGlkICYmICFzaWcuZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2lnU3RhdHVzID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGhhcyBhIDx2YWxpZGl0eT52YWxpZDwvdmFsaWRpdHk+IHNpZ25hdHVyZSBmcm9tIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8dmVyaWZ5PnVudmVyaWZpZWQ8L3ZlcmlmeT4gc2Vzc2lvbiA8ZGV2aWNlPjwvZGV2aWNlPlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7fSwgeyB2YWxpZGl0eSwgdmVyaWZ5LCBkZXZpY2UgfSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghc2lnLnZhbGlkICYmIHNpZy5kZXZpY2VUcnVzdC5pc1ZlcmlmaWVkKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBzaWdTdGF0dXMgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJCYWNrdXAgaGFzIGFuIDx2YWxpZGl0eT5pbnZhbGlkPC92YWxpZGl0eT4gc2lnbmF0dXJlIGZyb20gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjx2ZXJpZnk+dmVyaWZpZWQ8L3ZlcmlmeT4gc2Vzc2lvbiA8ZGV2aWNlPjwvZGV2aWNlPlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7fSwgeyB2YWxpZGl0eSwgdmVyaWZ5LCBkZXZpY2UgfSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghc2lnLnZhbGlkICYmICFzaWcuZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2lnU3RhdHVzID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQmFja3VwIGhhcyBhbiA8dmFsaWRpdHk+aW52YWxpZDwvdmFsaWRpdHk+IHNpZ25hdHVyZSBmcm9tIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8dmVyaWZ5PnVudmVyaWZpZWQ8L3ZlcmlmeT4gc2Vzc2lvbiA8ZGV2aWNlPjwvZGV2aWNlPlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7fSwgeyB2YWxpZGl0eSwgdmVyaWZ5LCBkZXZpY2UgfSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiA8ZGl2IGtleT17aX0+XHJcbiAgICAgICAgICAgICAgICAgICAge3NpZ1N0YXR1c31cclxuICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmJhY2t1cFNpZ1N0YXR1cy5zaWdzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzZXMgPSBfdChcIkJhY2t1cCBpcyBub3Qgc2lnbmVkIGJ5IGFueSBvZiB5b3VyIHNlc3Npb25zXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgdHJ1c3RlZExvY2FsbHk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmJhY2t1cFNpZ1N0YXR1cy50cnVzdGVkX2xvY2FsbHkpIHtcclxuICAgICAgICAgICAgICAgIHRydXN0ZWRMb2NhbGx5ID0gX3QoXCJUaGlzIGJhY2t1cCBpcyB0cnVzdGVkIGJlY2F1c2UgaXQgaGFzIGJlZW4gcmVzdG9yZWQgb24gdGhpcyBzZXNzaW9uXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgYnV0dG9uUm93ID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9LZXlCYWNrdXBQYW5lbF9idXR0b25Sb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBraW5kPVwicHJpbWFyeVwiIG9uQ2xpY2s9e3RoaXMuX3Jlc3RvcmVCYWNrdXB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7cmVzdG9yZUJ1dHRvbkNhcHRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPiZuYnNwOyZuYnNwOyZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJkYW5nZXJcIiBvbkNsaWNrPXt0aGlzLl9kZWxldGVCYWNrdXB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJEZWxldGUgQmFja3VwXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5iYWNrdXBLZXlTdG9yZWQgJiYgIVNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSkge1xyXG4gICAgICAgICAgICAgICAgYnV0dG9uUm93ID0gPHA+4pqg77iPIHtfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkJhY2t1cCBrZXkgc3RvcmVkIGluIHNlY3JldCBzdG9yYWdlLCBidXQgdGhpcyBmZWF0dXJlIGlzIG5vdCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbmFibGVkIG9uIHRoaXMgc2Vzc2lvbi4gUGxlYXNlIGVuYWJsZSBjcm9zcy1zaWduaW5nIGluIExhYnMgdG8gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwibW9kaWZ5IGtleSBiYWNrdXAgc3RhdGUuXCIsXHJcbiAgICAgICAgICAgICAgICApfTwvcD47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdj57Y2xpZW50QmFja3VwU3RhdHVzfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRldGFpbHM+XHJcbiAgICAgICAgICAgICAgICAgICAgPHN1bW1hcnk+e190KFwiQWR2YW5jZWRcIil9PC9zdW1tYXJ5PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+e190KFwiQmFja3VwIHZlcnNpb246IFwiKX17dGhpcy5zdGF0ZS5iYWNrdXBJbmZvLnZlcnNpb259PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj57X3QoXCJBbGdvcml0aG06IFwiKX17dGhpcy5zdGF0ZS5iYWNrdXBJbmZvLmFsZ29yaXRobX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PntfdChcIkJhY2t1cCBrZXkgc3RvcmVkOiBcIil9e2tleVN0YXR1c308L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICB7dXBsb2FkU3RhdHVzfVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+e2JhY2t1cFNpZ1N0YXR1c2VzfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+e3RydXN0ZWRMb2NhbGx5fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kZXRhaWxzPlxyXG4gICAgICAgICAgICAgICAge2J1dHRvblJvd31cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiWW91ciBrZXlzIGFyZSA8Yj5ub3QgYmVpbmcgYmFja2VkIHVwIGZyb20gdGhpcyBzZXNzaW9uPC9iPi5cIiwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtiOiBzdWIgPT4gPGI+e3N1Yn08L2I+fSxcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD57ZW5jcnlwdGVkTWVzc2FnZUFyZUVuY3J5cHRlZH08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+e190KFwiQmFjayB1cCB5b3VyIGtleXMgYmVmb3JlIHNpZ25pbmcgb3V0IHRvIGF2b2lkIGxvc2luZyB0aGVtLlwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfS2V5QmFja3VwUGFuZWxfYnV0dG9uUm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD1cInByaW1hcnlcIiBvbkNsaWNrPXt0aGlzLl9zdGFydE5ld0JhY2t1cH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlN0YXJ0IHVzaW5nIEtleSBCYWNrdXBcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19