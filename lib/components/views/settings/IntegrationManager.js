"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _Keyboard = require("../../../Keyboard");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class IntegrationManager extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onKeyDown", ev => {
      if (ev.key === _Keyboard.Key.ESCAPE) {
        ev.stopPropagation();
        ev.preventDefault();
        this.props.onFinished();
      }
    });
    (0, _defineProperty2.default)(this, "onAction", payload => {
      if (payload.action === 'close_scalar') {
        this.props.onFinished();
      }
    });
  }

  componentDidMount() {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    document.addEventListener("keydown", this.onKeyDown);
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this.dispatcherRef);

    document.removeEventListener("keydown", this.onKeyDown);
  }

  render() {
    if (this.props.loading) {
      const Spinner = sdk.getComponent("elements.Spinner");
      return _react.default.createElement("div", {
        className: "mx_IntegrationManager_loading"
      }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Connecting to integration manager...")), _react.default.createElement(Spinner, null));
    }

    if (!this.props.connected) {
      return _react.default.createElement("div", {
        className: "mx_IntegrationManager_error"
      }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Cannot connect to integration manager")), _react.default.createElement("p", null, (0, _languageHandler._t)("The integration manager is offline or it cannot reach your homeserver.")));
    }

    return _react.default.createElement("iframe", {
      src: this.props.url
    });
  }

}

exports.default = IntegrationManager;
(0, _defineProperty2.default)(IntegrationManager, "propTypes", {
  // false to display an error saying that we couldn't connect to the integration manager
  connected: _propTypes.default.bool.isRequired,
  // true to display a loading spinner
  loading: _propTypes.default.bool.isRequired,
  // The source URL to load
  url: _propTypes.default.string,
  // callback when the manager is dismissed
  onFinished: _propTypes.default.func.isRequired
});
(0, _defineProperty2.default)(IntegrationManager, "defaultProps", {
  connected: true,
  loading: false
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL0ludGVncmF0aW9uTWFuYWdlci5qcyJdLCJuYW1lcyI6WyJJbnRlZ3JhdGlvbk1hbmFnZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImV2Iiwia2V5IiwiS2V5IiwiRVNDQVBFIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJwYXlsb2FkIiwiYWN0aW9uIiwiY29tcG9uZW50RGlkTW91bnQiLCJkaXNwYXRjaGVyUmVmIiwiZGlzIiwicmVnaXN0ZXIiLCJvbkFjdGlvbiIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIm9uS2V5RG93biIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwidW5yZWdpc3RlciIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJyZW5kZXIiLCJsb2FkaW5nIiwiU3Bpbm5lciIsInNkayIsImdldENvbXBvbmVudCIsImNvbm5lY3RlZCIsInVybCIsIlByb3BUeXBlcyIsImJvb2wiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF0QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QmUsTUFBTUEsa0JBQU4sU0FBaUNDLGVBQU1DLFNBQXZDLENBQWlEO0FBQUE7QUFBQTtBQUFBLHFEQThCL0NDLEVBQUQsSUFBUTtBQUNoQixVQUFJQSxFQUFFLENBQUNDLEdBQUgsS0FBV0MsY0FBSUMsTUFBbkIsRUFBMkI7QUFDdkJILFFBQUFBLEVBQUUsQ0FBQ0ksZUFBSDtBQUNBSixRQUFBQSxFQUFFLENBQUNLLGNBQUg7QUFDQSxhQUFLQyxLQUFMLENBQVdDLFVBQVg7QUFDSDtBQUNKLEtBcEMyRDtBQUFBLG9EQXNDaERDLE9BQUQsSUFBYTtBQUNwQixVQUFJQSxPQUFPLENBQUNDLE1BQVIsS0FBbUIsY0FBdkIsRUFBdUM7QUFDbkMsYUFBS0gsS0FBTCxDQUFXQyxVQUFYO0FBQ0g7QUFDSixLQTFDMkQ7QUFBQTs7QUFvQjVERyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixTQUFLQyxhQUFMLEdBQXFCQyxvQkFBSUMsUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0FDLElBQUFBLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBcUMsS0FBS0MsU0FBMUM7QUFDSDs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkJOLHdCQUFJTyxVQUFKLENBQWUsS0FBS1IsYUFBcEI7O0FBQ0FJLElBQUFBLFFBQVEsQ0FBQ0ssbUJBQVQsQ0FBNkIsU0FBN0IsRUFBd0MsS0FBS0gsU0FBN0M7QUFDSDs7QUFnQkRJLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUksS0FBS2YsS0FBTCxDQUFXZ0IsT0FBZixFQUF3QjtBQUNwQixZQUFNQyxPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxhQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLHlDQUFLLHlCQUFHLHNDQUFILENBQUwsQ0FESixFQUVJLDZCQUFDLE9BQUQsT0FGSixDQURKO0FBTUg7O0FBRUQsUUFBSSxDQUFDLEtBQUtuQixLQUFMLENBQVdvQixTQUFoQixFQUEyQjtBQUN2QixhQUNJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLHlDQUFLLHlCQUFHLHVDQUFILENBQUwsQ0FESixFQUVJLHdDQUFJLHlCQUFHLHdFQUFILENBQUosQ0FGSixDQURKO0FBTUg7O0FBRUQsV0FBTztBQUFRLE1BQUEsR0FBRyxFQUFFLEtBQUtwQixLQUFMLENBQVdxQjtBQUF4QixNQUFQO0FBQ0g7O0FBakUyRDs7OzhCQUEzQzlCLGtCLGVBQ0U7QUFDZjtBQUNBNkIsRUFBQUEsU0FBUyxFQUFFRSxtQkFBVUMsSUFBVixDQUFlQyxVQUZYO0FBSWY7QUFDQVIsRUFBQUEsT0FBTyxFQUFFTSxtQkFBVUMsSUFBVixDQUFlQyxVQUxUO0FBT2Y7QUFDQUgsRUFBQUEsR0FBRyxFQUFFQyxtQkFBVUcsTUFSQTtBQVVmO0FBQ0F4QixFQUFBQSxVQUFVLEVBQUVxQixtQkFBVUksSUFBVixDQUFlRjtBQVhaLEM7OEJBREZqQyxrQixrQkFlSztBQUNsQjZCLEVBQUFBLFNBQVMsRUFBRSxJQURPO0FBRWxCSixFQUFBQSxPQUFPLEVBQUU7QUFGUyxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7S2V5fSBmcm9tIFwiLi4vLi4vLi4vS2V5Ym9hcmRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEludGVncmF0aW9uTWFuYWdlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIGZhbHNlIHRvIGRpc3BsYXkgYW4gZXJyb3Igc2F5aW5nIHRoYXQgd2UgY291bGRuJ3QgY29ubmVjdCB0byB0aGUgaW50ZWdyYXRpb24gbWFuYWdlclxyXG4gICAgICAgIGNvbm5lY3RlZDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLy8gdHJ1ZSB0byBkaXNwbGF5IGEgbG9hZGluZyBzcGlubmVyXHJcbiAgICAgICAgbG9hZGluZzogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLy8gVGhlIHNvdXJjZSBVUkwgdG8gbG9hZFxyXG4gICAgICAgIHVybDogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2sgd2hlbiB0aGUgbWFuYWdlciBpcyBkaXNtaXNzZWRcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xyXG4gICAgICAgIGNvbm5lY3RlZDogdHJ1ZSxcclxuICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgIH07XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIHRoaXMub25LZXlEb3duKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLmRpc3BhdGNoZXJSZWYpO1xyXG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIHRoaXMub25LZXlEb3duKTtcclxuICAgIH1cclxuXHJcbiAgICBvbktleURvd24gPSAoZXYpID0+IHtcclxuICAgICAgICBpZiAoZXYua2V5ID09PSBLZXkuRVNDQVBFKSB7XHJcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9uQWN0aW9uID0gKHBheWxvYWQpID0+IHtcclxuICAgICAgICBpZiAocGF5bG9hZC5hY3Rpb24gPT09ICdjbG9zZV9zY2FsYXInKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmxvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludGVncmF0aW9uTWFuYWdlcl9sb2FkaW5nJz5cclxuICAgICAgICAgICAgICAgICAgICA8aDM+e190KFwiQ29ubmVjdGluZyB0byBpbnRlZ3JhdGlvbiBtYW5hZ2VyLi4uXCIpfTwvaDM+XHJcbiAgICAgICAgICAgICAgICAgICAgPFNwaW5uZXIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLmNvbm5lY3RlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0ludGVncmF0aW9uTWFuYWdlcl9lcnJvcic+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgzPntfdChcIkNhbm5vdCBjb25uZWN0IHRvIGludGVncmF0aW9uIG1hbmFnZXJcIil9PC9oMz5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXCJUaGUgaW50ZWdyYXRpb24gbWFuYWdlciBpcyBvZmZsaW5lIG9yIGl0IGNhbm5vdCByZWFjaCB5b3VyIGhvbWVzZXJ2ZXIuXCIpfTwvcD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxpZnJhbWUgc3JjPXt0aGlzLnByb3BzLnVybH0+PC9pZnJhbWU+O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==