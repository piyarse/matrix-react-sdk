"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.EmailAddress = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../languageHandler");

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../index"));

var _Modal = _interopRequireDefault(require("../../../../Modal"));

var _AddThreepid = _interopRequireDefault(require("../../../../AddThreepid"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
TODO: Improve the UX for everything in here.
It's very much placeholder, but it gets the job done. The old way of handling
email addresses in user settings was to use dialogs to communicate state, however
due to our dialog system overriding dialogs (causing unmounts) this creates problems
for a sane UX. For instance, the user could easily end up entering an email address
and receive a dialog to verify the address, which then causes the component here
to forget what it was doing and ultimately fail. Dialogs are still used in some
places to communicate errors - these should be replaced with inline validation when
that is available.
*/

/*
TODO: Reduce all the copying between account vs. discovery components.
*/
class EmailAddress extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onRevokeClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.changeBinding({
        bind: false,
        label: "revoke",
        errorTitle: (0, _languageHandler._t)("Unable to revoke sharing for email address")
      });
    });
    (0, _defineProperty2.default)(this, "onShareClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.changeBinding({
        bind: true,
        label: "share",
        errorTitle: (0, _languageHandler._t)("Unable to share email address")
      });
    });
    (0, _defineProperty2.default)(this, "onContinueClick", async e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        continueDisabled: true
      });

      try {
        await this.state.addTask.checkEmailLinkClicked();
        this.setState({
          addTask: null,
          continueDisabled: false,
          verifying: false
        });
      } catch (err) {
        this.setState({
          continueDisabled: false
        });
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        if (err.errcode === 'M_THREEPID_AUTH_FAILED') {
          _Modal.default.createTrackedDialog("E-mail hasn't been verified yet", "", ErrorDialog, {
            title: (0, _languageHandler._t)("Your email address hasn't been verified yet"),
            description: (0, _languageHandler._t)("Click the link in the email you received to verify " + "and then click continue again.")
          });
        } else {
          console.error("Unable to verify email address: " + err);

          _Modal.default.createTrackedDialog('Unable to verify email address', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Unable to verify email address."),
            description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
          });
        }
      }
    });
    const {
      bound
    } = props.email;
    this.state = {
      verifying: false,
      addTask: null,
      continueDisabled: false,
      bound
    };
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(nextProps) {
    // eslint-disable-line camelcase
    const {
      bound
    } = nextProps.email;
    this.setState({
      bound
    });
  }

  async changeBinding({
    bind,
    label,
    errorTitle
  }) {
    if (!(await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind())) {
      return this.changeBindingTangledAddBind({
        bind,
        label,
        errorTitle
      });
    }

    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const {
      medium,
      address
    } = this.props.email;

    try {
      if (bind) {
        const task = new _AddThreepid.default();
        this.setState({
          verifying: true,
          continueDisabled: true,
          addTask: task
        });
        await task.bindEmailAddress(address);
        this.setState({
          continueDisabled: false
        });
      } else {
        await _MatrixClientPeg.MatrixClientPeg.get().unbindThreePid(medium, address);
      }

      this.setState({
        bound: bind
      });
    } catch (err) {
      console.error("Unable to ".concat(label, " email address ").concat(address, " ").concat(err));
      this.setState({
        verifying: false,
        continueDisabled: false,
        addTask: null
      });

      _Modal.default.createTrackedDialog("Unable to ".concat(label, " email address"), '', ErrorDialog, {
        title: errorTitle,
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });
    }
  }

  async changeBindingTangledAddBind({
    bind,
    label,
    errorTitle
  }) {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const {
      medium,
      address
    } = this.props.email;
    const task = new _AddThreepid.default();
    this.setState({
      verifying: true,
      continueDisabled: true,
      addTask: task
    });

    try {
      await _MatrixClientPeg.MatrixClientPeg.get().deleteThreePid(medium, address);

      if (bind) {
        await task.bindEmailAddress(address);
      } else {
        await task.addEmailAddress(address);
      }

      this.setState({
        continueDisabled: false,
        bound: bind
      });
    } catch (err) {
      console.error("Unable to ".concat(label, " email address ").concat(address, " ").concat(err));
      this.setState({
        verifying: false,
        continueDisabled: false,
        addTask: null
      });

      _Modal.default.createTrackedDialog("Unable to ".concat(label, " email address"), '', ErrorDialog, {
        title: errorTitle,
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });
    }
  }

  render() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const {
      address
    } = this.props.email;
    const {
      verifying,
      bound
    } = this.state;
    let status;

    if (verifying) {
      status = _react.default.createElement("span", null, (0, _languageHandler._t)("Verify the link in your inbox"), _react.default.createElement(AccessibleButton, {
        className: "mx_ExistingEmailAddress_confirmBtn",
        kind: "primary_sm",
        onClick: this.onContinueClick
      }, (0, _languageHandler._t)("Complete")));
    } else if (bound) {
      status = _react.default.createElement(AccessibleButton, {
        className: "mx_ExistingEmailAddress_confirmBtn",
        kind: "danger_sm",
        onClick: this.onRevokeClick
      }, (0, _languageHandler._t)("Revoke"));
    } else {
      status = _react.default.createElement(AccessibleButton, {
        className: "mx_ExistingEmailAddress_confirmBtn",
        kind: "primary_sm",
        onClick: this.onShareClick
      }, (0, _languageHandler._t)("Share"));
    }

    return _react.default.createElement("div", {
      className: "mx_ExistingEmailAddress"
    }, _react.default.createElement("span", {
      className: "mx_ExistingEmailAddress_email"
    }, address), status);
  }

}

exports.EmailAddress = EmailAddress;
(0, _defineProperty2.default)(EmailAddress, "propTypes", {
  email: _propTypes.default.object.isRequired
});

class EmailAddresses extends _react.default.Component {
  render() {
    let content;

    if (this.props.emails.length > 0) {
      content = this.props.emails.map(e => {
        return _react.default.createElement(EmailAddress, {
          email: e,
          key: e.address
        });
      });
    } else {
      content = _react.default.createElement("span", {
        className: "mx_SettingsTab_subsectionText"
      }, (0, _languageHandler._t)("Discovery options will appear once you have added an email above."));
    }

    return _react.default.createElement("div", {
      className: "mx_EmailAddresses"
    }, content);
  }

}

exports.default = EmailAddresses;
(0, _defineProperty2.default)(EmailAddresses, "propTypes", {
  emails: _propTypes.default.array.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL2Rpc2NvdmVyeS9FbWFpbEFkZHJlc3Nlcy5qcyJdLCJuYW1lcyI6WyJFbWFpbEFkZHJlc3MiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJjaGFuZ2VCaW5kaW5nIiwiYmluZCIsImxhYmVsIiwiZXJyb3JUaXRsZSIsInNldFN0YXRlIiwiY29udGludWVEaXNhYmxlZCIsInN0YXRlIiwiYWRkVGFzayIsImNoZWNrRW1haWxMaW5rQ2xpY2tlZCIsInZlcmlmeWluZyIsImVyciIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZXJyY29kZSIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJjb25zb2xlIiwiZXJyb3IiLCJtZXNzYWdlIiwiYm91bmQiLCJlbWFpbCIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV4dFByb3BzIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZG9lc1NlcnZlclN1cHBvcnRTZXBhcmF0ZUFkZEFuZEJpbmQiLCJjaGFuZ2VCaW5kaW5nVGFuZ2xlZEFkZEJpbmQiLCJtZWRpdW0iLCJhZGRyZXNzIiwidGFzayIsIkFkZFRocmVlcGlkIiwiYmluZEVtYWlsQWRkcmVzcyIsInVuYmluZFRocmVlUGlkIiwiZGVsZXRlVGhyZWVQaWQiLCJhZGRFbWFpbEFkZHJlc3MiLCJyZW5kZXIiLCJBY2Nlc3NpYmxlQnV0dG9uIiwic3RhdHVzIiwib25Db250aW51ZUNsaWNrIiwib25SZXZva2VDbGljayIsIm9uU2hhcmVDbGljayIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJFbWFpbEFkZHJlc3NlcyIsImNvbnRlbnQiLCJlbWFpbHMiLCJsZW5ndGgiLCJtYXAiLCJhcnJheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBOzs7Ozs7Ozs7Ozs7QUFZQTs7O0FBSU8sTUFBTUEsWUFBTixTQUEyQkMsZUFBTUMsU0FBakMsQ0FBMkM7QUFLOUNDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLHlEQTZGRkMsQ0FBRCxJQUFPO0FBQ25CQSxNQUFBQSxDQUFDLENBQUNDLGVBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxjQUFGO0FBQ0EsV0FBS0MsYUFBTCxDQUFtQjtBQUNmQyxRQUFBQSxJQUFJLEVBQUUsS0FEUztBQUVmQyxRQUFBQSxLQUFLLEVBQUUsUUFGUTtBQUdmQyxRQUFBQSxVQUFVLEVBQUUseUJBQUcsNENBQUg7QUFIRyxPQUFuQjtBQUtILEtBckdrQjtBQUFBLHdEQXVHSE4sQ0FBRCxJQUFPO0FBQ2xCQSxNQUFBQSxDQUFDLENBQUNDLGVBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxjQUFGO0FBQ0EsV0FBS0MsYUFBTCxDQUFtQjtBQUNmQyxRQUFBQSxJQUFJLEVBQUUsSUFEUztBQUVmQyxRQUFBQSxLQUFLLEVBQUUsT0FGUTtBQUdmQyxRQUFBQSxVQUFVLEVBQUUseUJBQUcsK0JBQUg7QUFIRyxPQUFuQjtBQUtILEtBL0drQjtBQUFBLDJEQWlIRCxNQUFPTixDQUFQLElBQWE7QUFDM0JBLE1BQUFBLENBQUMsQ0FBQ0MsZUFBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFFQSxXQUFLSyxRQUFMLENBQWM7QUFBRUMsUUFBQUEsZ0JBQWdCLEVBQUU7QUFBcEIsT0FBZDs7QUFDQSxVQUFJO0FBQ0EsY0FBTSxLQUFLQyxLQUFMLENBQVdDLE9BQVgsQ0FBbUJDLHFCQUFuQixFQUFOO0FBQ0EsYUFBS0osUUFBTCxDQUFjO0FBQ1ZHLFVBQUFBLE9BQU8sRUFBRSxJQURDO0FBRVZGLFVBQUFBLGdCQUFnQixFQUFFLEtBRlI7QUFHVkksVUFBQUEsU0FBUyxFQUFFO0FBSEQsU0FBZDtBQUtILE9BUEQsQ0FPRSxPQUFPQyxHQUFQLEVBQVk7QUFDVixhQUFLTixRQUFMLENBQWM7QUFBRUMsVUFBQUEsZ0JBQWdCLEVBQUU7QUFBcEIsU0FBZDtBQUNBLGNBQU1NLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQSxZQUFJSCxHQUFHLENBQUNJLE9BQUosS0FBZ0Isd0JBQXBCLEVBQThDO0FBQzFDQyx5QkFBTUMsbUJBQU4sQ0FBMEIsaUNBQTFCLEVBQTZELEVBQTdELEVBQWlFTCxXQUFqRSxFQUE4RTtBQUMxRU0sWUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZDQUFILENBRG1FO0FBRTFFQyxZQUFBQSxXQUFXLEVBQUUseUJBQUcsd0RBQ1osZ0NBRFM7QUFGNkQsV0FBOUU7QUFLSCxTQU5ELE1BTU87QUFDSEMsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMscUNBQXFDVixHQUFuRDs7QUFDQUsseUJBQU1DLG1CQUFOLENBQTBCLGdDQUExQixFQUE0RCxFQUE1RCxFQUFnRUwsV0FBaEUsRUFBNkU7QUFDekVNLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxpQ0FBSCxDQURrRTtBQUV6RUMsWUFBQUEsV0FBVyxFQUFJUixHQUFHLElBQUlBLEdBQUcsQ0FBQ1csT0FBWixHQUF1QlgsR0FBRyxDQUFDVyxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZzQixXQUE3RTtBQUlIO0FBQ0o7QUFDSixLQTlJa0I7QUFHZixVQUFNO0FBQUVDLE1BQUFBO0FBQUYsUUFBWTFCLEtBQUssQ0FBQzJCLEtBQXhCO0FBRUEsU0FBS2pCLEtBQUwsR0FBYTtBQUNURyxNQUFBQSxTQUFTLEVBQUUsS0FERjtBQUVURixNQUFBQSxPQUFPLEVBQUUsSUFGQTtBQUdURixNQUFBQSxnQkFBZ0IsRUFBRSxLQUhUO0FBSVRpQixNQUFBQTtBQUpTLEtBQWI7QUFNSCxHQWhCNkMsQ0FrQjlDOzs7QUFDQUUsRUFBQUEsZ0NBQWdDLENBQUNDLFNBQUQsRUFBWTtBQUFFO0FBQzFDLFVBQU07QUFBRUgsTUFBQUE7QUFBRixRQUFZRyxTQUFTLENBQUNGLEtBQTVCO0FBQ0EsU0FBS25CLFFBQUwsQ0FBYztBQUFFa0IsTUFBQUE7QUFBRixLQUFkO0FBQ0g7O0FBRUQsUUFBTXRCLGFBQU4sQ0FBb0I7QUFBRUMsSUFBQUEsSUFBRjtBQUFRQyxJQUFBQSxLQUFSO0FBQWVDLElBQUFBO0FBQWYsR0FBcEIsRUFBaUQ7QUFDN0MsUUFBSSxFQUFDLE1BQU11QixpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxtQ0FBdEIsRUFBUCxDQUFKLEVBQXdFO0FBQ3BFLGFBQU8sS0FBS0MsMkJBQUwsQ0FBaUM7QUFBRTVCLFFBQUFBLElBQUY7QUFBUUMsUUFBQUEsS0FBUjtBQUFlQyxRQUFBQTtBQUFmLE9BQWpDLENBQVA7QUFDSDs7QUFFRCxVQUFNUSxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQSxVQUFNO0FBQUVpQixNQUFBQSxNQUFGO0FBQVVDLE1BQUFBO0FBQVYsUUFBc0IsS0FBS25DLEtBQUwsQ0FBVzJCLEtBQXZDOztBQUVBLFFBQUk7QUFDQSxVQUFJdEIsSUFBSixFQUFVO0FBQ04sY0FBTStCLElBQUksR0FBRyxJQUFJQyxvQkFBSixFQUFiO0FBQ0EsYUFBSzdCLFFBQUwsQ0FBYztBQUNWSyxVQUFBQSxTQUFTLEVBQUUsSUFERDtBQUVWSixVQUFBQSxnQkFBZ0IsRUFBRSxJQUZSO0FBR1ZFLFVBQUFBLE9BQU8sRUFBRXlCO0FBSEMsU0FBZDtBQUtBLGNBQU1BLElBQUksQ0FBQ0UsZ0JBQUwsQ0FBc0JILE9BQXRCLENBQU47QUFDQSxhQUFLM0IsUUFBTCxDQUFjO0FBQ1ZDLFVBQUFBLGdCQUFnQixFQUFFO0FBRFIsU0FBZDtBQUdILE9BWEQsTUFXTztBQUNILGNBQU1xQixpQ0FBZ0JDLEdBQWhCLEdBQXNCUSxjQUF0QixDQUFxQ0wsTUFBckMsRUFBNkNDLE9BQTdDLENBQU47QUFDSDs7QUFDRCxXQUFLM0IsUUFBTCxDQUFjO0FBQUVrQixRQUFBQSxLQUFLLEVBQUVyQjtBQUFULE9BQWQ7QUFDSCxLQWhCRCxDQWdCRSxPQUFPUyxHQUFQLEVBQVk7QUFDVlMsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLHFCQUEyQmxCLEtBQTNCLDRCQUFrRDZCLE9BQWxELGNBQTZEckIsR0FBN0Q7QUFDQSxXQUFLTixRQUFMLENBQWM7QUFDVkssUUFBQUEsU0FBUyxFQUFFLEtBREQ7QUFFVkosUUFBQUEsZ0JBQWdCLEVBQUUsS0FGUjtBQUdWRSxRQUFBQSxPQUFPLEVBQUU7QUFIQyxPQUFkOztBQUtBUSxxQkFBTUMsbUJBQU4scUJBQXVDZCxLQUF2QyxxQkFBOEQsRUFBOUQsRUFBa0VTLFdBQWxFLEVBQStFO0FBQzNFTSxRQUFBQSxLQUFLLEVBQUVkLFVBRG9FO0FBRTNFZSxRQUFBQSxXQUFXLEVBQUlSLEdBQUcsSUFBSUEsR0FBRyxDQUFDVyxPQUFaLEdBQXVCWCxHQUFHLENBQUNXLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRndCLE9BQS9FO0FBSUg7QUFDSjs7QUFFRCxRQUFNUSwyQkFBTixDQUFrQztBQUFFNUIsSUFBQUEsSUFBRjtBQUFRQyxJQUFBQSxLQUFSO0FBQWVDLElBQUFBO0FBQWYsR0FBbEMsRUFBK0Q7QUFDM0QsVUFBTVEsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EsVUFBTTtBQUFFaUIsTUFBQUEsTUFBRjtBQUFVQyxNQUFBQTtBQUFWLFFBQXNCLEtBQUtuQyxLQUFMLENBQVcyQixLQUF2QztBQUVBLFVBQU1TLElBQUksR0FBRyxJQUFJQyxvQkFBSixFQUFiO0FBQ0EsU0FBSzdCLFFBQUwsQ0FBYztBQUNWSyxNQUFBQSxTQUFTLEVBQUUsSUFERDtBQUVWSixNQUFBQSxnQkFBZ0IsRUFBRSxJQUZSO0FBR1ZFLE1BQUFBLE9BQU8sRUFBRXlCO0FBSEMsS0FBZDs7QUFNQSxRQUFJO0FBQ0EsWUFBTU4saUNBQWdCQyxHQUFoQixHQUFzQlMsY0FBdEIsQ0FBcUNOLE1BQXJDLEVBQTZDQyxPQUE3QyxDQUFOOztBQUNBLFVBQUk5QixJQUFKLEVBQVU7QUFDTixjQUFNK0IsSUFBSSxDQUFDRSxnQkFBTCxDQUFzQkgsT0FBdEIsQ0FBTjtBQUNILE9BRkQsTUFFTztBQUNILGNBQU1DLElBQUksQ0FBQ0ssZUFBTCxDQUFxQk4sT0FBckIsQ0FBTjtBQUNIOztBQUNELFdBQUszQixRQUFMLENBQWM7QUFDVkMsUUFBQUEsZ0JBQWdCLEVBQUUsS0FEUjtBQUVWaUIsUUFBQUEsS0FBSyxFQUFFckI7QUFGRyxPQUFkO0FBSUgsS0FYRCxDQVdFLE9BQU9TLEdBQVAsRUFBWTtBQUNWUyxNQUFBQSxPQUFPLENBQUNDLEtBQVIscUJBQTJCbEIsS0FBM0IsNEJBQWtENkIsT0FBbEQsY0FBNkRyQixHQUE3RDtBQUNBLFdBQUtOLFFBQUwsQ0FBYztBQUNWSyxRQUFBQSxTQUFTLEVBQUUsS0FERDtBQUVWSixRQUFBQSxnQkFBZ0IsRUFBRSxLQUZSO0FBR1ZFLFFBQUFBLE9BQU8sRUFBRTtBQUhDLE9BQWQ7O0FBS0FRLHFCQUFNQyxtQkFBTixxQkFBdUNkLEtBQXZDLHFCQUE4RCxFQUE5RCxFQUFrRVMsV0FBbEUsRUFBK0U7QUFDM0VNLFFBQUFBLEtBQUssRUFBRWQsVUFEb0U7QUFFM0VlLFFBQUFBLFdBQVcsRUFBSVIsR0FBRyxJQUFJQSxHQUFHLENBQUNXLE9BQVosR0FBdUJYLEdBQUcsQ0FBQ1csT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGd0IsT0FBL0U7QUFJSDtBQUNKOztBQXFERGlCLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLGdCQUFnQixHQUFHM0IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU07QUFBRWtCLE1BQUFBO0FBQUYsUUFBYyxLQUFLbkMsS0FBTCxDQUFXMkIsS0FBL0I7QUFDQSxVQUFNO0FBQUVkLE1BQUFBLFNBQUY7QUFBYWEsTUFBQUE7QUFBYixRQUF1QixLQUFLaEIsS0FBbEM7QUFFQSxRQUFJa0MsTUFBSjs7QUFDQSxRQUFJL0IsU0FBSixFQUFlO0FBQ1grQixNQUFBQSxNQUFNLEdBQUcsMkNBQ0oseUJBQUcsK0JBQUgsQ0FESSxFQUVMLDZCQUFDLGdCQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUMsb0NBRGQ7QUFFSSxRQUFBLElBQUksRUFBQyxZQUZUO0FBR0ksUUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFIbEIsU0FLSyx5QkFBRyxVQUFILENBTEwsQ0FGSyxDQUFUO0FBVUgsS0FYRCxNQVdPLElBQUluQixLQUFKLEVBQVc7QUFDZGtCLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxnQkFBRDtBQUNMLFFBQUEsU0FBUyxFQUFDLG9DQURMO0FBRUwsUUFBQSxJQUFJLEVBQUMsV0FGQTtBQUdMLFFBQUEsT0FBTyxFQUFFLEtBQUtFO0FBSFQsU0FLSix5QkFBRyxRQUFILENBTEksQ0FBVDtBQU9ILEtBUk0sTUFRQTtBQUNIRixNQUFBQSxNQUFNLEdBQUcsNkJBQUMsZ0JBQUQ7QUFDTCxRQUFBLFNBQVMsRUFBQyxvQ0FETDtBQUVMLFFBQUEsSUFBSSxFQUFDLFlBRkE7QUFHTCxRQUFBLE9BQU8sRUFBRSxLQUFLRztBQUhULFNBS0oseUJBQUcsT0FBSCxDQUxJLENBQVQ7QUFPSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBaURaLE9BQWpELENBREosRUFFS1MsTUFGTCxDQURKO0FBTUg7O0FBOUw2Qzs7OzhCQUFyQ2hELFksZUFDVTtBQUNmK0IsRUFBQUEsS0FBSyxFQUFFcUIsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRFQsQzs7QUFnTVIsTUFBTUMsY0FBTixTQUE2QnRELGVBQU1DLFNBQW5DLENBQTZDO0FBS3hENEMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSVUsT0FBSjs7QUFDQSxRQUFJLEtBQUtwRCxLQUFMLENBQVdxRCxNQUFYLENBQWtCQyxNQUFsQixHQUEyQixDQUEvQixFQUFrQztBQUM5QkYsTUFBQUEsT0FBTyxHQUFHLEtBQUtwRCxLQUFMLENBQVdxRCxNQUFYLENBQWtCRSxHQUFsQixDQUF1QnRELENBQUQsSUFBTztBQUNuQyxlQUFPLDZCQUFDLFlBQUQ7QUFBYyxVQUFBLEtBQUssRUFBRUEsQ0FBckI7QUFBd0IsVUFBQSxHQUFHLEVBQUVBLENBQUMsQ0FBQ2tDO0FBQS9CLFVBQVA7QUFDSCxPQUZTLENBQVY7QUFHSCxLQUpELE1BSU87QUFDSGlCLE1BQUFBLE9BQU8sR0FBRztBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0wseUJBQUcsbUVBQUgsQ0FESyxDQUFWO0FBR0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS0EsT0FETCxDQURKO0FBS0g7O0FBdEJ1RDs7OzhCQUF2Q0QsYyxlQUNFO0FBQ2ZFLEVBQUFBLE1BQU0sRUFBRUwsbUJBQVVRLEtBQVYsQ0FBZ0JOO0FBRFQsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuXHJcbmltcG9ydCB7IF90IH0gZnJvbSBcIi4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgQWRkVGhyZWVwaWQgZnJvbSAnLi4vLi4vLi4vLi4vQWRkVGhyZWVwaWQnO1xyXG5cclxuLypcclxuVE9ETzogSW1wcm92ZSB0aGUgVVggZm9yIGV2ZXJ5dGhpbmcgaW4gaGVyZS5cclxuSXQncyB2ZXJ5IG11Y2ggcGxhY2Vob2xkZXIsIGJ1dCBpdCBnZXRzIHRoZSBqb2IgZG9uZS4gVGhlIG9sZCB3YXkgb2YgaGFuZGxpbmdcclxuZW1haWwgYWRkcmVzc2VzIGluIHVzZXIgc2V0dGluZ3Mgd2FzIHRvIHVzZSBkaWFsb2dzIHRvIGNvbW11bmljYXRlIHN0YXRlLCBob3dldmVyXHJcbmR1ZSB0byBvdXIgZGlhbG9nIHN5c3RlbSBvdmVycmlkaW5nIGRpYWxvZ3MgKGNhdXNpbmcgdW5tb3VudHMpIHRoaXMgY3JlYXRlcyBwcm9ibGVtc1xyXG5mb3IgYSBzYW5lIFVYLiBGb3IgaW5zdGFuY2UsIHRoZSB1c2VyIGNvdWxkIGVhc2lseSBlbmQgdXAgZW50ZXJpbmcgYW4gZW1haWwgYWRkcmVzc1xyXG5hbmQgcmVjZWl2ZSBhIGRpYWxvZyB0byB2ZXJpZnkgdGhlIGFkZHJlc3MsIHdoaWNoIHRoZW4gY2F1c2VzIHRoZSBjb21wb25lbnQgaGVyZVxyXG50byBmb3JnZXQgd2hhdCBpdCB3YXMgZG9pbmcgYW5kIHVsdGltYXRlbHkgZmFpbC4gRGlhbG9ncyBhcmUgc3RpbGwgdXNlZCBpbiBzb21lXHJcbnBsYWNlcyB0byBjb21tdW5pY2F0ZSBlcnJvcnMgLSB0aGVzZSBzaG91bGQgYmUgcmVwbGFjZWQgd2l0aCBpbmxpbmUgdmFsaWRhdGlvbiB3aGVuXHJcbnRoYXQgaXMgYXZhaWxhYmxlLlxyXG4qL1xyXG5cclxuLypcclxuVE9ETzogUmVkdWNlIGFsbCB0aGUgY29weWluZyBiZXR3ZWVuIGFjY291bnQgdnMuIGRpc2NvdmVyeSBjb21wb25lbnRzLlxyXG4qL1xyXG5cclxuZXhwb3J0IGNsYXNzIEVtYWlsQWRkcmVzcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIGVtYWlsOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICBjb25zdCB7IGJvdW5kIH0gPSBwcm9wcy5lbWFpbDtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgdmVyaWZ5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgYWRkVGFzazogbnVsbCxcclxuICAgICAgICAgICAgY29udGludWVEaXNhYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGJvdW5kLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2Ugd2l0aCBhcHByb3ByaWF0ZSBsaWZlY3ljbGUgZXZlbnRcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxyXG4gICAgICAgIGNvbnN0IHsgYm91bmQgfSA9IG5leHRQcm9wcy5lbWFpbDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgYm91bmQgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgY2hhbmdlQmluZGluZyh7IGJpbmQsIGxhYmVsLCBlcnJvclRpdGxlIH0pIHtcclxuICAgICAgICBpZiAoIWF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5kb2VzU2VydmVyU3VwcG9ydFNlcGFyYXRlQWRkQW5kQmluZCgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNoYW5nZUJpbmRpbmdUYW5nbGVkQWRkQmluZCh7IGJpbmQsIGxhYmVsLCBlcnJvclRpdGxlIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCB7IG1lZGl1bSwgYWRkcmVzcyB9ID0gdGhpcy5wcm9wcy5lbWFpbDtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKGJpbmQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2sgPSBuZXcgQWRkVGhyZWVwaWQoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHZlcmlmeWluZzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGFkZFRhc2s6IHRhc2ssXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGF3YWl0IHRhc2suYmluZEVtYWlsQWRkcmVzcyhhZGRyZXNzKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkudW5iaW5kVGhyZWVQaWQobWVkaXVtLCBhZGRyZXNzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgYm91bmQ6IGJpbmQgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoYFVuYWJsZSB0byAke2xhYmVsfSBlbWFpbCBhZGRyZXNzICR7YWRkcmVzc30gJHtlcnJ9YCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgdmVyaWZ5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYWRkVGFzazogbnVsbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coYFVuYWJsZSB0byAke2xhYmVsfSBlbWFpbCBhZGRyZXNzYCwgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogZXJyb3JUaXRsZSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KFwiT3BlcmF0aW9uIGZhaWxlZFwiKSksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjaGFuZ2VCaW5kaW5nVGFuZ2xlZEFkZEJpbmQoeyBiaW5kLCBsYWJlbCwgZXJyb3JUaXRsZSB9KSB7XHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCB7IG1lZGl1bSwgYWRkcmVzcyB9ID0gdGhpcy5wcm9wcy5lbWFpbDtcclxuXHJcbiAgICAgICAgY29uc3QgdGFzayA9IG5ldyBBZGRUaHJlZXBpZCgpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB2ZXJpZnlpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIGFkZFRhc2s6IHRhc2ssXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5kZWxldGVUaHJlZVBpZChtZWRpdW0sIGFkZHJlc3MpO1xyXG4gICAgICAgICAgICBpZiAoYmluZCkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGFzay5iaW5kRW1haWxBZGRyZXNzKGFkZHJlc3MpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGFzay5hZGRFbWFpbEFkZHJlc3MoYWRkcmVzcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGJvdW5kOiBiaW5kLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgVW5hYmxlIHRvICR7bGFiZWx9IGVtYWlsIGFkZHJlc3MgJHthZGRyZXNzfSAke2Vycn1gKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICB2ZXJpZnlpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY29udGludWVEaXNhYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBhZGRUYXNrOiBudWxsLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhgVW5hYmxlIHRvICR7bGFiZWx9IGVtYWlsIGFkZHJlc3NgLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBlcnJvclRpdGxlLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogX3QoXCJPcGVyYXRpb24gZmFpbGVkXCIpKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmV2b2tlQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlQmluZGluZyh7XHJcbiAgICAgICAgICAgIGJpbmQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBsYWJlbDogXCJyZXZva2VcIixcclxuICAgICAgICAgICAgZXJyb3JUaXRsZTogX3QoXCJVbmFibGUgdG8gcmV2b2tlIHNoYXJpbmcgZm9yIGVtYWlsIGFkZHJlc3NcIiksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TaGFyZUNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmNoYW5nZUJpbmRpbmcoe1xyXG4gICAgICAgICAgICBiaW5kOiB0cnVlLFxyXG4gICAgICAgICAgICBsYWJlbDogXCJzaGFyZVwiLFxyXG4gICAgICAgICAgICBlcnJvclRpdGxlOiBfdChcIlVuYWJsZSB0byBzaGFyZSBlbWFpbCBhZGRyZXNzXCIpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ29udGludWVDbGljayA9IGFzeW5jIChlKSA9PiB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjb250aW51ZURpc2FibGVkOiB0cnVlIH0pO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuc3RhdGUuYWRkVGFzay5jaGVja0VtYWlsTGlua0NsaWNrZWQoKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBhZGRUYXNrOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY29udGludWVEaXNhYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB2ZXJpZnlpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBpZiAoZXJyLmVycmNvZGUgPT09ICdNX1RIUkVFUElEX0FVVEhfRkFJTEVEJykge1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcIkUtbWFpbCBoYXNuJ3QgYmVlbiB2ZXJpZmllZCB5ZXRcIiwgXCJcIiwgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJZb3VyIGVtYWlsIGFkZHJlc3MgaGFzbid0IGJlZW4gdmVyaWZpZWQgeWV0XCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIkNsaWNrIHRoZSBsaW5rIGluIHRoZSBlbWFpbCB5b3UgcmVjZWl2ZWQgdG8gdmVyaWZ5IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJhbmQgdGhlbiBjbGljayBjb250aW51ZSBhZ2Fpbi5cIiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJVbmFibGUgdG8gdmVyaWZ5IGVtYWlsIGFkZHJlc3M6IFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1VuYWJsZSB0byB2ZXJpZnkgZW1haWwgYWRkcmVzcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlVuYWJsZSB0byB2ZXJpZnkgZW1haWwgYWRkcmVzcy5cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogX3QoXCJPcGVyYXRpb24gZmFpbGVkXCIpKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIGNvbnN0IHsgYWRkcmVzcyB9ID0gdGhpcy5wcm9wcy5lbWFpbDtcclxuICAgICAgICBjb25zdCB7IHZlcmlmeWluZywgYm91bmQgfSA9IHRoaXMuc3RhdGU7XHJcblxyXG4gICAgICAgIGxldCBzdGF0dXM7XHJcbiAgICAgICAgaWYgKHZlcmlmeWluZykge1xyXG4gICAgICAgICAgICBzdGF0dXMgPSA8c3Bhbj5cclxuICAgICAgICAgICAgICAgIHtfdChcIlZlcmlmeSB0aGUgbGluayBpbiB5b3VyIGluYm94XCIpfVxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9FeGlzdGluZ0VtYWlsQWRkcmVzc19jb25maXJtQnRuXCJcclxuICAgICAgICAgICAgICAgICAgICBraW5kPVwicHJpbWFyeV9zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkNvbnRpbnVlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiQ29tcGxldGVcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvc3Bhbj47XHJcbiAgICAgICAgfSBlbHNlIGlmIChib3VuZCkge1xyXG4gICAgICAgICAgICBzdGF0dXMgPSA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdFbWFpbEFkZHJlc3NfY29uZmlybUJ0blwiXHJcbiAgICAgICAgICAgICAgICBraW5kPVwiZGFuZ2VyX3NtXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25SZXZva2VDbGlja31cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAge190KFwiUmV2b2tlXCIpfVxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHN0YXR1cyA9IDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9FeGlzdGluZ0VtYWlsQWRkcmVzc19jb25maXJtQnRuXCJcclxuICAgICAgICAgICAgICAgIGtpbmQ9XCJwcmltYXJ5X3NtXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25TaGFyZUNsaWNrfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7X3QoXCJTaGFyZVwiKX1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdFbWFpbEFkZHJlc3NcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X0V4aXN0aW5nRW1haWxBZGRyZXNzX2VtYWlsXCI+e2FkZHJlc3N9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAge3N0YXR1c31cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRW1haWxBZGRyZXNzZXMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBlbWFpbHM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgY29udGVudDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5lbWFpbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gdGhpcy5wcm9wcy5lbWFpbHMubWFwKChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gPEVtYWlsQWRkcmVzcyBlbWFpbD17ZX0ga2V5PXtlLmFkZHJlc3N9IC8+O1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHRcIj5cclxuICAgICAgICAgICAgICAgIHtfdChcIkRpc2NvdmVyeSBvcHRpb25zIHdpbGwgYXBwZWFyIG9uY2UgeW91IGhhdmUgYWRkZWQgYW4gZW1haWwgYWJvdmUuXCIpfVxyXG4gICAgICAgICAgICA8L3NwYW4+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbWFpbEFkZHJlc3Nlc1wiPlxyXG4gICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19