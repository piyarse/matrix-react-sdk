"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PhoneNumber = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../languageHandler");

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../../index"));

var _Modal = _interopRequireDefault(require("../../../../Modal"));

var _AddThreepid = _interopRequireDefault(require("../../../../AddThreepid"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
TODO: Improve the UX for everything in here.
This is a copy/paste of EmailAddresses, mostly.
 */
// TODO: Combine EmailAddresses and PhoneNumbers to be 3pid agnostic
class PhoneNumber extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onRevokeClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.changeBinding({
        bind: false,
        label: "revoke",
        errorTitle: (0, _languageHandler._t)("Unable to revoke sharing for phone number")
      });
    });
    (0, _defineProperty2.default)(this, "onShareClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.changeBinding({
        bind: true,
        label: "share",
        errorTitle: (0, _languageHandler._t)("Unable to share phone number")
      });
    });
    (0, _defineProperty2.default)(this, "onVerificationCodeChange", e => {
      this.setState({
        verificationCode: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "onContinueClick", async e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        continueDisabled: true
      });
      const token = this.state.verificationCode;

      try {
        await this.state.addTask.haveMsisdnToken(token);
        this.setState({
          addTask: null,
          continueDisabled: false,
          verifying: false,
          verifyError: null,
          verificationCode: ""
        });
      } catch (err) {
        this.setState({
          continueDisabled: false
        });

        if (err.errcode !== 'M_THREEPID_AUTH_FAILED') {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
          console.error("Unable to verify phone number: " + err);

          _Modal.default.createTrackedDialog('Unable to verify phone number', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Unable to verify phone number."),
            description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
          });
        } else {
          this.setState({
            verifyError: (0, _languageHandler._t)("Incorrect verification code")
          });
        }
      }
    });
    const {
      bound
    } = props.msisdn;
    this.state = {
      verifying: false,
      verificationCode: "",
      addTask: null,
      continueDisabled: false,
      bound
    };
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(nextProps) {
    // eslint-disable-line camelcase
    const {
      bound
    } = nextProps.msisdn;
    this.setState({
      bound
    });
  }

  async changeBinding({
    bind,
    label,
    errorTitle
  }) {
    if (!(await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind())) {
      return this.changeBindingTangledAddBind({
        bind,
        label,
        errorTitle
      });
    }

    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const {
      medium,
      address
    } = this.props.msisdn;

    try {
      if (bind) {
        const task = new _AddThreepid.default();
        this.setState({
          verifying: true,
          continueDisabled: true,
          addTask: task
        }); // XXX: Sydent will accept a number without country code if you add
        // a leading plus sign to a number in E.164 format (which the 3PID
        // address is), but this goes against the spec.
        // See https://github.com/matrix-org/matrix-doc/issues/2222

        await task.bindMsisdn(null, "+".concat(address));
        this.setState({
          continueDisabled: false
        });
      } else {
        await _MatrixClientPeg.MatrixClientPeg.get().unbindThreePid(medium, address);
      }

      this.setState({
        bound: bind
      });
    } catch (err) {
      console.error("Unable to ".concat(label, " phone number ").concat(address, " ").concat(err));
      this.setState({
        verifying: false,
        continueDisabled: false,
        addTask: null
      });

      _Modal.default.createTrackedDialog("Unable to ".concat(label, " phone number"), '', ErrorDialog, {
        title: errorTitle,
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });
    }
  }

  async changeBindingTangledAddBind({
    bind,
    label,
    errorTitle
  }) {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const {
      medium,
      address
    } = this.props.msisdn;
    const task = new _AddThreepid.default();
    this.setState({
      verifying: true,
      continueDisabled: true,
      addTask: task
    });

    try {
      await _MatrixClientPeg.MatrixClientPeg.get().deleteThreePid(medium, address); // XXX: Sydent will accept a number without country code if you add
      // a leading plus sign to a number in E.164 format (which the 3PID
      // address is), but this goes against the spec.
      // See https://github.com/matrix-org/matrix-doc/issues/2222

      if (bind) {
        await task.bindMsisdn(null, "+".concat(address));
      } else {
        await task.addMsisdn(null, "+".concat(address));
      }

      this.setState({
        continueDisabled: false,
        bound: bind
      });
    } catch (err) {
      console.error("Unable to ".concat(label, " phone number ").concat(address, " ").concat(err));
      this.setState({
        verifying: false,
        continueDisabled: false,
        addTask: null
      });

      _Modal.default.createTrackedDialog("Unable to ".concat(label, " phone number"), '', ErrorDialog, {
        title: errorTitle,
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });
    }
  }

  render() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const Field = sdk.getComponent('elements.Field');
    const {
      address
    } = this.props.msisdn;
    const {
      verifying,
      bound
    } = this.state;
    let status;

    if (verifying) {
      status = _react.default.createElement("span", {
        className: "mx_ExistingPhoneNumber_verification"
      }, _react.default.createElement("span", null, (0, _languageHandler._t)("Please enter verification code sent via text."), _react.default.createElement("br", null), this.state.verifyError), _react.default.createElement("form", {
        onSubmit: this.onContinueClick,
        autoComplete: "off",
        noValidate: true
      }, _react.default.createElement(Field, {
        type: "text",
        label: (0, _languageHandler._t)("Verification code"),
        autoComplete: "off",
        disabled: this.state.continueDisabled,
        value: this.state.verificationCode,
        onChange: this.onVerificationCodeChange
      })));
    } else if (bound) {
      status = _react.default.createElement(AccessibleButton, {
        className: "mx_ExistingPhoneNumber_confirmBtn",
        kind: "danger_sm",
        onClick: this.onRevokeClick
      }, (0, _languageHandler._t)("Revoke"));
    } else {
      status = _react.default.createElement(AccessibleButton, {
        className: "mx_ExistingPhoneNumber_confirmBtn",
        kind: "primary_sm",
        onClick: this.onShareClick
      }, (0, _languageHandler._t)("Share"));
    }

    return _react.default.createElement("div", {
      className: "mx_ExistingPhoneNumber"
    }, _react.default.createElement("span", {
      className: "mx_ExistingPhoneNumber_address"
    }, "+", address), status);
  }

}

exports.PhoneNumber = PhoneNumber;
(0, _defineProperty2.default)(PhoneNumber, "propTypes", {
  msisdn: _propTypes.default.object.isRequired
});

class PhoneNumbers extends _react.default.Component {
  render() {
    let content;

    if (this.props.msisdns.length > 0) {
      content = this.props.msisdns.map(e => {
        return _react.default.createElement(PhoneNumber, {
          msisdn: e,
          key: e.address
        });
      });
    } else {
      content = _react.default.createElement("span", {
        className: "mx_SettingsTab_subsectionText"
      }, (0, _languageHandler._t)("Discovery options will appear once you have added a phone number above."));
    }

    return _react.default.createElement("div", {
      className: "mx_PhoneNumbers"
    }, content);
  }

}

exports.default = PhoneNumbers;
(0, _defineProperty2.default)(PhoneNumbers, "propTypes", {
  msisdns: _propTypes.default.array.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL2Rpc2NvdmVyeS9QaG9uZU51bWJlcnMuanMiXSwibmFtZXMiOlsiUGhvbmVOdW1iZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJjaGFuZ2VCaW5kaW5nIiwiYmluZCIsImxhYmVsIiwiZXJyb3JUaXRsZSIsInNldFN0YXRlIiwidmVyaWZpY2F0aW9uQ29kZSIsInRhcmdldCIsInZhbHVlIiwiY29udGludWVEaXNhYmxlZCIsInRva2VuIiwic3RhdGUiLCJhZGRUYXNrIiwiaGF2ZU1zaXNkblRva2VuIiwidmVyaWZ5aW5nIiwidmVyaWZ5RXJyb3IiLCJlcnIiLCJlcnJjb2RlIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjb25zb2xlIiwiZXJyb3IiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwibWVzc2FnZSIsImJvdW5kIiwibXNpc2RuIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMiLCJuZXh0UHJvcHMiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJkb2VzU2VydmVyU3VwcG9ydFNlcGFyYXRlQWRkQW5kQmluZCIsImNoYW5nZUJpbmRpbmdUYW5nbGVkQWRkQmluZCIsIm1lZGl1bSIsImFkZHJlc3MiLCJ0YXNrIiwiQWRkVGhyZWVwaWQiLCJiaW5kTXNpc2RuIiwidW5iaW5kVGhyZWVQaWQiLCJkZWxldGVUaHJlZVBpZCIsImFkZE1zaXNkbiIsInJlbmRlciIsIkFjY2Vzc2libGVCdXR0b24iLCJGaWVsZCIsInN0YXR1cyIsIm9uQ29udGludWVDbGljayIsIm9uVmVyaWZpY2F0aW9uQ29kZUNoYW5nZSIsIm9uUmV2b2tlQ2xpY2siLCJvblNoYXJlQ2xpY2siLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiUGhvbmVOdW1iZXJzIiwiY29udGVudCIsIm1zaXNkbnMiLCJsZW5ndGgiLCJtYXAiLCJhcnJheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBOzs7O0FBS0E7QUFFTyxNQUFNQSxXQUFOLFNBQTBCQyxlQUFNQyxTQUFoQyxDQUEwQztBQUs3Q0MsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUseURBc0dGQyxDQUFELElBQU87QUFDbkJBLE1BQUFBLENBQUMsQ0FBQ0MsZUFBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFDQSxXQUFLQyxhQUFMLENBQW1CO0FBQ2ZDLFFBQUFBLElBQUksRUFBRSxLQURTO0FBRWZDLFFBQUFBLEtBQUssRUFBRSxRQUZRO0FBR2ZDLFFBQUFBLFVBQVUsRUFBRSx5QkFBRywyQ0FBSDtBQUhHLE9BQW5CO0FBS0gsS0E5R2tCO0FBQUEsd0RBZ0hITixDQUFELElBQU87QUFDbEJBLE1BQUFBLENBQUMsQ0FBQ0MsZUFBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFDQSxXQUFLQyxhQUFMLENBQW1CO0FBQ2ZDLFFBQUFBLElBQUksRUFBRSxJQURTO0FBRWZDLFFBQUFBLEtBQUssRUFBRSxPQUZRO0FBR2ZDLFFBQUFBLFVBQVUsRUFBRSx5QkFBRyw4QkFBSDtBQUhHLE9BQW5CO0FBS0gsS0F4SGtCO0FBQUEsb0VBMEhTTixDQUFELElBQU87QUFDOUIsV0FBS08sUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLGdCQUFnQixFQUFFUixDQUFDLENBQUNTLE1BQUYsQ0FBU0M7QUFEakIsT0FBZDtBQUdILEtBOUhrQjtBQUFBLDJEQWdJRCxNQUFPVixDQUFQLElBQWE7QUFDM0JBLE1BQUFBLENBQUMsQ0FBQ0MsZUFBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFFQSxXQUFLSyxRQUFMLENBQWM7QUFBRUksUUFBQUEsZ0JBQWdCLEVBQUU7QUFBcEIsT0FBZDtBQUNBLFlBQU1DLEtBQUssR0FBRyxLQUFLQyxLQUFMLENBQVdMLGdCQUF6Qjs7QUFDQSxVQUFJO0FBQ0EsY0FBTSxLQUFLSyxLQUFMLENBQVdDLE9BQVgsQ0FBbUJDLGVBQW5CLENBQW1DSCxLQUFuQyxDQUFOO0FBQ0EsYUFBS0wsUUFBTCxDQUFjO0FBQ1ZPLFVBQUFBLE9BQU8sRUFBRSxJQURDO0FBRVZILFVBQUFBLGdCQUFnQixFQUFFLEtBRlI7QUFHVkssVUFBQUEsU0FBUyxFQUFFLEtBSEQ7QUFJVkMsVUFBQUEsV0FBVyxFQUFFLElBSkg7QUFLVlQsVUFBQUEsZ0JBQWdCLEVBQUU7QUFMUixTQUFkO0FBT0gsT0FURCxDQVNFLE9BQU9VLEdBQVAsRUFBWTtBQUNWLGFBQUtYLFFBQUwsQ0FBYztBQUFFSSxVQUFBQSxnQkFBZ0IsRUFBRTtBQUFwQixTQUFkOztBQUNBLFlBQUlPLEdBQUcsQ0FBQ0MsT0FBSixLQUFnQix3QkFBcEIsRUFBOEM7QUFDMUMsZ0JBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBQyxVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxvQ0FBb0NOLEdBQWxEOztBQUNBTyx5QkFBTUMsbUJBQU4sQ0FBMEIsK0JBQTFCLEVBQTJELEVBQTNELEVBQStETixXQUEvRCxFQUE0RTtBQUN4RU8sWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGdDQUFILENBRGlFO0FBRXhFQyxZQUFBQSxXQUFXLEVBQUlWLEdBQUcsSUFBSUEsR0FBRyxDQUFDVyxPQUFaLEdBQXVCWCxHQUFHLENBQUNXLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRnFCLFdBQTVFO0FBSUgsU0FQRCxNQU9PO0FBQ0gsZUFBS3RCLFFBQUwsQ0FBYztBQUFDVSxZQUFBQSxXQUFXLEVBQUUseUJBQUcsNkJBQUg7QUFBZCxXQUFkO0FBQ0g7QUFDSjtBQUNKLEtBNUprQjtBQUdmLFVBQU07QUFBRWEsTUFBQUE7QUFBRixRQUFZL0IsS0FBSyxDQUFDZ0MsTUFBeEI7QUFFQSxTQUFLbEIsS0FBTCxHQUFhO0FBQ1RHLE1BQUFBLFNBQVMsRUFBRSxLQURGO0FBRVRSLE1BQUFBLGdCQUFnQixFQUFFLEVBRlQ7QUFHVE0sTUFBQUEsT0FBTyxFQUFFLElBSEE7QUFJVEgsTUFBQUEsZ0JBQWdCLEVBQUUsS0FKVDtBQUtUbUIsTUFBQUE7QUFMUyxLQUFiO0FBT0gsR0FqQjRDLENBbUI3Qzs7O0FBQ0FFLEVBQUFBLGdDQUFnQyxDQUFDQyxTQUFELEVBQVk7QUFBRTtBQUMxQyxVQUFNO0FBQUVILE1BQUFBO0FBQUYsUUFBWUcsU0FBUyxDQUFDRixNQUE1QjtBQUNBLFNBQUt4QixRQUFMLENBQWM7QUFBRXVCLE1BQUFBO0FBQUYsS0FBZDtBQUNIOztBQUVELFFBQU0zQixhQUFOLENBQW9CO0FBQUVDLElBQUFBLElBQUY7QUFBUUMsSUFBQUEsS0FBUjtBQUFlQyxJQUFBQTtBQUFmLEdBQXBCLEVBQWlEO0FBQzdDLFFBQUksRUFBQyxNQUFNNEIsaUNBQWdCQyxHQUFoQixHQUFzQkMsbUNBQXRCLEVBQVAsQ0FBSixFQUF3RTtBQUNwRSxhQUFPLEtBQUtDLDJCQUFMLENBQWlDO0FBQUVqQyxRQUFBQSxJQUFGO0FBQVFDLFFBQUFBLEtBQVI7QUFBZUMsUUFBQUE7QUFBZixPQUFqQyxDQUFQO0FBQ0g7O0FBRUQsVUFBTWMsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EsVUFBTTtBQUFFZ0IsTUFBQUEsTUFBRjtBQUFVQyxNQUFBQTtBQUFWLFFBQXNCLEtBQUt4QyxLQUFMLENBQVdnQyxNQUF2Qzs7QUFFQSxRQUFJO0FBQ0EsVUFBSTNCLElBQUosRUFBVTtBQUNOLGNBQU1vQyxJQUFJLEdBQUcsSUFBSUMsb0JBQUosRUFBYjtBQUNBLGFBQUtsQyxRQUFMLENBQWM7QUFDVlMsVUFBQUEsU0FBUyxFQUFFLElBREQ7QUFFVkwsVUFBQUEsZ0JBQWdCLEVBQUUsSUFGUjtBQUdWRyxVQUFBQSxPQUFPLEVBQUUwQjtBQUhDLFNBQWQsRUFGTSxDQU9OO0FBQ0E7QUFDQTtBQUNBOztBQUNBLGNBQU1BLElBQUksQ0FBQ0UsVUFBTCxDQUFnQixJQUFoQixhQUEwQkgsT0FBMUIsRUFBTjtBQUNBLGFBQUtoQyxRQUFMLENBQWM7QUFDVkksVUFBQUEsZ0JBQWdCLEVBQUU7QUFEUixTQUFkO0FBR0gsT0FmRCxNQWVPO0FBQ0gsY0FBTXVCLGlDQUFnQkMsR0FBaEIsR0FBc0JRLGNBQXRCLENBQXFDTCxNQUFyQyxFQUE2Q0MsT0FBN0MsQ0FBTjtBQUNIOztBQUNELFdBQUtoQyxRQUFMLENBQWM7QUFBRXVCLFFBQUFBLEtBQUssRUFBRTFCO0FBQVQsT0FBZDtBQUNILEtBcEJELENBb0JFLE9BQU9jLEdBQVAsRUFBWTtBQUNWSyxNQUFBQSxPQUFPLENBQUNDLEtBQVIscUJBQTJCbkIsS0FBM0IsMkJBQWlEa0MsT0FBakQsY0FBNERyQixHQUE1RDtBQUNBLFdBQUtYLFFBQUwsQ0FBYztBQUNWUyxRQUFBQSxTQUFTLEVBQUUsS0FERDtBQUVWTCxRQUFBQSxnQkFBZ0IsRUFBRSxLQUZSO0FBR1ZHLFFBQUFBLE9BQU8sRUFBRTtBQUhDLE9BQWQ7O0FBS0FXLHFCQUFNQyxtQkFBTixxQkFBdUNyQixLQUF2QyxvQkFBNkQsRUFBN0QsRUFBaUVlLFdBQWpFLEVBQThFO0FBQzFFTyxRQUFBQSxLQUFLLEVBQUVyQixVQURtRTtBQUUxRXNCLFFBQUFBLFdBQVcsRUFBSVYsR0FBRyxJQUFJQSxHQUFHLENBQUNXLE9BQVosR0FBdUJYLEdBQUcsQ0FBQ1csT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGdUIsT0FBOUU7QUFJSDtBQUNKOztBQUVELFFBQU1RLDJCQUFOLENBQWtDO0FBQUVqQyxJQUFBQSxJQUFGO0FBQVFDLElBQUFBLEtBQVI7QUFBZUMsSUFBQUE7QUFBZixHQUFsQyxFQUErRDtBQUMzRCxVQUFNYyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQSxVQUFNO0FBQUVnQixNQUFBQSxNQUFGO0FBQVVDLE1BQUFBO0FBQVYsUUFBc0IsS0FBS3hDLEtBQUwsQ0FBV2dDLE1BQXZDO0FBRUEsVUFBTVMsSUFBSSxHQUFHLElBQUlDLG9CQUFKLEVBQWI7QUFDQSxTQUFLbEMsUUFBTCxDQUFjO0FBQ1ZTLE1BQUFBLFNBQVMsRUFBRSxJQUREO0FBRVZMLE1BQUFBLGdCQUFnQixFQUFFLElBRlI7QUFHVkcsTUFBQUEsT0FBTyxFQUFFMEI7QUFIQyxLQUFkOztBQU1BLFFBQUk7QUFDQSxZQUFNTixpQ0FBZ0JDLEdBQWhCLEdBQXNCUyxjQUF0QixDQUFxQ04sTUFBckMsRUFBNkNDLE9BQTdDLENBQU4sQ0FEQSxDQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFVBQUluQyxJQUFKLEVBQVU7QUFDTixjQUFNb0MsSUFBSSxDQUFDRSxVQUFMLENBQWdCLElBQWhCLGFBQTBCSCxPQUExQixFQUFOO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsY0FBTUMsSUFBSSxDQUFDSyxTQUFMLENBQWUsSUFBZixhQUF5Qk4sT0FBekIsRUFBTjtBQUNIOztBQUNELFdBQUtoQyxRQUFMLENBQWM7QUFDVkksUUFBQUEsZ0JBQWdCLEVBQUUsS0FEUjtBQUVWbUIsUUFBQUEsS0FBSyxFQUFFMUI7QUFGRyxPQUFkO0FBSUgsS0FmRCxDQWVFLE9BQU9jLEdBQVAsRUFBWTtBQUNWSyxNQUFBQSxPQUFPLENBQUNDLEtBQVIscUJBQTJCbkIsS0FBM0IsMkJBQWlEa0MsT0FBakQsY0FBNERyQixHQUE1RDtBQUNBLFdBQUtYLFFBQUwsQ0FBYztBQUNWUyxRQUFBQSxTQUFTLEVBQUUsS0FERDtBQUVWTCxRQUFBQSxnQkFBZ0IsRUFBRSxLQUZSO0FBR1ZHLFFBQUFBLE9BQU8sRUFBRTtBQUhDLE9BQWQ7O0FBS0FXLHFCQUFNQyxtQkFBTixxQkFBdUNyQixLQUF2QyxvQkFBNkQsRUFBN0QsRUFBaUVlLFdBQWpFLEVBQThFO0FBQzFFTyxRQUFBQSxLQUFLLEVBQUVyQixVQURtRTtBQUUxRXNCLFFBQUFBLFdBQVcsRUFBSVYsR0FBRyxJQUFJQSxHQUFHLENBQUNXLE9BQVosR0FBdUJYLEdBQUcsQ0FBQ1csT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGdUIsT0FBOUU7QUFJSDtBQUNKOztBQTBERGlCLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLGdCQUFnQixHQUFHMUIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU0wQixLQUFLLEdBQUczQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsZ0JBQWpCLENBQWQ7QUFDQSxVQUFNO0FBQUVpQixNQUFBQTtBQUFGLFFBQWMsS0FBS3hDLEtBQUwsQ0FBV2dDLE1BQS9CO0FBQ0EsVUFBTTtBQUFFZixNQUFBQSxTQUFGO0FBQWFjLE1BQUFBO0FBQWIsUUFBdUIsS0FBS2pCLEtBQWxDO0FBRUEsUUFBSW9DLE1BQUo7O0FBQ0EsUUFBSWpDLFNBQUosRUFBZTtBQUNYaUMsTUFBQUEsTUFBTSxHQUFHO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FDTCwyQ0FDSyx5QkFBRywrQ0FBSCxDQURMLEVBRUksd0NBRkosRUFHSyxLQUFLcEMsS0FBTCxDQUFXSSxXQUhoQixDQURLLEVBTUw7QUFBTSxRQUFBLFFBQVEsRUFBRSxLQUFLaUMsZUFBckI7QUFBc0MsUUFBQSxZQUFZLEVBQUMsS0FBbkQ7QUFBeUQsUUFBQSxVQUFVLEVBQUU7QUFBckUsU0FDSSw2QkFBQyxLQUFEO0FBQ0ksUUFBQSxJQUFJLEVBQUMsTUFEVDtBQUVJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLG1CQUFILENBRlg7QUFHSSxRQUFBLFlBQVksRUFBQyxLQUhqQjtBQUlJLFFBQUEsUUFBUSxFQUFFLEtBQUtyQyxLQUFMLENBQVdGLGdCQUp6QjtBQUtJLFFBQUEsS0FBSyxFQUFFLEtBQUtFLEtBQUwsQ0FBV0wsZ0JBTHRCO0FBTUksUUFBQSxRQUFRLEVBQUUsS0FBSzJDO0FBTm5CLFFBREosQ0FOSyxDQUFUO0FBaUJILEtBbEJELE1Ba0JPLElBQUlyQixLQUFKLEVBQVc7QUFDZG1CLE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxnQkFBRDtBQUNMLFFBQUEsU0FBUyxFQUFDLG1DQURMO0FBRUwsUUFBQSxJQUFJLEVBQUMsV0FGQTtBQUdMLFFBQUEsT0FBTyxFQUFFLEtBQUtHO0FBSFQsU0FLSix5QkFBRyxRQUFILENBTEksQ0FBVDtBQU9ILEtBUk0sTUFRQTtBQUNISCxNQUFBQSxNQUFNLEdBQUcsNkJBQUMsZ0JBQUQ7QUFDTCxRQUFBLFNBQVMsRUFBQyxtQ0FETDtBQUVMLFFBQUEsSUFBSSxFQUFDLFlBRkE7QUFHTCxRQUFBLE9BQU8sRUFBRSxLQUFLSTtBQUhULFNBS0oseUJBQUcsT0FBSCxDQUxJLENBQVQ7QUFPSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsWUFBbURkLE9BQW5ELENBREosRUFFS1UsTUFGTCxDQURKO0FBTUg7O0FBcE40Qzs7OzhCQUFwQ3RELFcsZUFDVTtBQUNmb0MsRUFBQUEsTUFBTSxFQUFFdUIsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRFYsQzs7QUFzTlIsTUFBTUMsWUFBTixTQUEyQjdELGVBQU1DLFNBQWpDLENBQTJDO0FBS3REaUQsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSVksT0FBSjs7QUFDQSxRQUFJLEtBQUszRCxLQUFMLENBQVc0RCxPQUFYLENBQW1CQyxNQUFuQixHQUE0QixDQUFoQyxFQUFtQztBQUMvQkYsTUFBQUEsT0FBTyxHQUFHLEtBQUszRCxLQUFMLENBQVc0RCxPQUFYLENBQW1CRSxHQUFuQixDQUF3QjdELENBQUQsSUFBTztBQUNwQyxlQUFPLDZCQUFDLFdBQUQ7QUFBYSxVQUFBLE1BQU0sRUFBRUEsQ0FBckI7QUFBd0IsVUFBQSxHQUFHLEVBQUVBLENBQUMsQ0FBQ3VDO0FBQS9CLFVBQVA7QUFDSCxPQUZTLENBQVY7QUFHSCxLQUpELE1BSU87QUFDSG1CLE1BQUFBLE9BQU8sR0FBRztBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0wseUJBQUcseUVBQUgsQ0FESyxDQUFWO0FBR0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS0EsT0FETCxDQURKO0FBS0g7O0FBdEJxRDs7OzhCQUFyQ0QsWSxlQUNFO0FBQ2ZFLEVBQUFBLE9BQU8sRUFBRUwsbUJBQVVRLEtBQVYsQ0FBZ0JOO0FBRFYsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuXHJcbmltcG9ydCB7IF90IH0gZnJvbSBcIi4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgQWRkVGhyZWVwaWQgZnJvbSAnLi4vLi4vLi4vLi4vQWRkVGhyZWVwaWQnO1xyXG5cclxuLypcclxuVE9ETzogSW1wcm92ZSB0aGUgVVggZm9yIGV2ZXJ5dGhpbmcgaW4gaGVyZS5cclxuVGhpcyBpcyBhIGNvcHkvcGFzdGUgb2YgRW1haWxBZGRyZXNzZXMsIG1vc3RseS5cclxuICovXHJcblxyXG4vLyBUT0RPOiBDb21iaW5lIEVtYWlsQWRkcmVzc2VzIGFuZCBQaG9uZU51bWJlcnMgdG8gYmUgM3BpZCBhZ25vc3RpY1xyXG5cclxuZXhwb3J0IGNsYXNzIFBob25lTnVtYmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgbXNpc2RuOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICBjb25zdCB7IGJvdW5kIH0gPSBwcm9wcy5tc2lzZG47XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHZlcmlmeWluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIHZlcmlmaWNhdGlvbkNvZGU6IFwiXCIsXHJcbiAgICAgICAgICAgIGFkZFRhc2s6IG51bGwsXHJcbiAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBib3VuZCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjYW1lbGNhc2VcclxuICAgICAgICBjb25zdCB7IGJvdW5kIH0gPSBuZXh0UHJvcHMubXNpc2RuO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBib3VuZCB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjaGFuZ2VCaW5kaW5nKHsgYmluZCwgbGFiZWwsIGVycm9yVGl0bGUgfSkge1xyXG4gICAgICAgIGlmICghYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRvZXNTZXJ2ZXJTdXBwb3J0U2VwYXJhdGVBZGRBbmRCaW5kKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2hhbmdlQmluZGluZ1RhbmdsZWRBZGRCaW5kKHsgYmluZCwgbGFiZWwsIGVycm9yVGl0bGUgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgIGNvbnN0IHsgbWVkaXVtLCBhZGRyZXNzIH0gPSB0aGlzLnByb3BzLm1zaXNkbjtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKGJpbmQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2sgPSBuZXcgQWRkVGhyZWVwaWQoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHZlcmlmeWluZzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGFkZFRhc2s6IHRhc2ssXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIC8vIFhYWDogU3lkZW50IHdpbGwgYWNjZXB0IGEgbnVtYmVyIHdpdGhvdXQgY291bnRyeSBjb2RlIGlmIHlvdSBhZGRcclxuICAgICAgICAgICAgICAgIC8vIGEgbGVhZGluZyBwbHVzIHNpZ24gdG8gYSBudW1iZXIgaW4gRS4xNjQgZm9ybWF0ICh3aGljaCB0aGUgM1BJRFxyXG4gICAgICAgICAgICAgICAgLy8gYWRkcmVzcyBpcyksIGJ1dCB0aGlzIGdvZXMgYWdhaW5zdCB0aGUgc3BlYy5cclxuICAgICAgICAgICAgICAgIC8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vbWF0cml4LW9yZy9tYXRyaXgtZG9jL2lzc3Vlcy8yMjIyXHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0YXNrLmJpbmRNc2lzZG4obnVsbCwgYCske2FkZHJlc3N9YCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLnVuYmluZFRocmVlUGlkKG1lZGl1bSwgYWRkcmVzcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGJvdW5kOiBiaW5kIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGBVbmFibGUgdG8gJHtsYWJlbH0gcGhvbmUgbnVtYmVyICR7YWRkcmVzc30gJHtlcnJ9YCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgdmVyaWZ5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYWRkVGFzazogbnVsbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coYFVuYWJsZSB0byAke2xhYmVsfSBwaG9uZSBudW1iZXJgLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBlcnJvclRpdGxlLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogX3QoXCJPcGVyYXRpb24gZmFpbGVkXCIpKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGNoYW5nZUJpbmRpbmdUYW5nbGVkQWRkQmluZCh7IGJpbmQsIGxhYmVsLCBlcnJvclRpdGxlIH0pIHtcclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgIGNvbnN0IHsgbWVkaXVtLCBhZGRyZXNzIH0gPSB0aGlzLnByb3BzLm1zaXNkbjtcclxuXHJcbiAgICAgICAgY29uc3QgdGFzayA9IG5ldyBBZGRUaHJlZXBpZCgpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB2ZXJpZnlpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIGFkZFRhc2s6IHRhc2ssXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5kZWxldGVUaHJlZVBpZChtZWRpdW0sIGFkZHJlc3MpO1xyXG4gICAgICAgICAgICAvLyBYWFg6IFN5ZGVudCB3aWxsIGFjY2VwdCBhIG51bWJlciB3aXRob3V0IGNvdW50cnkgY29kZSBpZiB5b3UgYWRkXHJcbiAgICAgICAgICAgIC8vIGEgbGVhZGluZyBwbHVzIHNpZ24gdG8gYSBudW1iZXIgaW4gRS4xNjQgZm9ybWF0ICh3aGljaCB0aGUgM1BJRFxyXG4gICAgICAgICAgICAvLyBhZGRyZXNzIGlzKSwgYnV0IHRoaXMgZ29lcyBhZ2FpbnN0IHRoZSBzcGVjLlxyXG4gICAgICAgICAgICAvLyBTZWUgaHR0cHM6Ly9naXRodWIuY29tL21hdHJpeC1vcmcvbWF0cml4LWRvYy9pc3N1ZXMvMjIyMlxyXG4gICAgICAgICAgICBpZiAoYmluZCkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGFzay5iaW5kTXNpc2RuKG51bGwsIGArJHthZGRyZXNzfWApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGFzay5hZGRNc2lzZG4obnVsbCwgYCske2FkZHJlc3N9YCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGJvdW5kOiBiaW5kLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgVW5hYmxlIHRvICR7bGFiZWx9IHBob25lIG51bWJlciAke2FkZHJlc3N9ICR7ZXJyfWApO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHZlcmlmeWluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGFkZFRhc2s6IG51bGwsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKGBVbmFibGUgdG8gJHtsYWJlbH0gcGhvbmUgbnVtYmVyYCwgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogZXJyb3JUaXRsZSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KFwiT3BlcmF0aW9uIGZhaWxlZFwiKSksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblJldm9rZUNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmNoYW5nZUJpbmRpbmcoe1xyXG4gICAgICAgICAgICBiaW5kOiBmYWxzZSxcclxuICAgICAgICAgICAgbGFiZWw6IFwicmV2b2tlXCIsXHJcbiAgICAgICAgICAgIGVycm9yVGl0bGU6IF90KFwiVW5hYmxlIHRvIHJldm9rZSBzaGFyaW5nIGZvciBwaG9uZSBudW1iZXJcIiksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TaGFyZUNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmNoYW5nZUJpbmRpbmcoe1xyXG4gICAgICAgICAgICBiaW5kOiB0cnVlLFxyXG4gICAgICAgICAgICBsYWJlbDogXCJzaGFyZVwiLFxyXG4gICAgICAgICAgICBlcnJvclRpdGxlOiBfdChcIlVuYWJsZSB0byBzaGFyZSBwaG9uZSBudW1iZXJcIiksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25WZXJpZmljYXRpb25Db2RlQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdmVyaWZpY2F0aW9uQ29kZTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Db250aW51ZUNsaWNrID0gYXN5bmMgKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGNvbnRpbnVlRGlzYWJsZWQ6IHRydWUgfSk7XHJcbiAgICAgICAgY29uc3QgdG9rZW4gPSB0aGlzLnN0YXRlLnZlcmlmaWNhdGlvbkNvZGU7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5zdGF0ZS5hZGRUYXNrLmhhdmVNc2lzZG5Ub2tlbih0b2tlbik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYWRkVGFzazogbnVsbCxcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdmVyaWZ5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZlcmlmeUVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgdmVyaWZpY2F0aW9uQ29kZTogXCJcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjb250aW51ZURpc2FibGVkOiBmYWxzZSB9KTtcclxuICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlICE9PSAnTV9USFJFRVBJRF9BVVRIX0ZBSUxFRCcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiVW5hYmxlIHRvIHZlcmlmeSBwaG9uZSBudW1iZXI6IFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1VuYWJsZSB0byB2ZXJpZnkgcGhvbmUgbnVtYmVyJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiVW5hYmxlIHRvIHZlcmlmeSBwaG9uZSBudW1iZXIuXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KFwiT3BlcmF0aW9uIGZhaWxlZFwiKSksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3ZlcmlmeUVycm9yOiBfdChcIkluY29ycmVjdCB2ZXJpZmljYXRpb24gY29kZVwiKX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCB7IGFkZHJlc3MgfSA9IHRoaXMucHJvcHMubXNpc2RuO1xyXG4gICAgICAgIGNvbnN0IHsgdmVyaWZ5aW5nLCBib3VuZCB9ID0gdGhpcy5zdGF0ZTtcclxuXHJcbiAgICAgICAgbGV0IHN0YXR1cztcclxuICAgICAgICBpZiAodmVyaWZ5aW5nKSB7XHJcbiAgICAgICAgICAgIHN0YXR1cyA9IDxzcGFuIGNsYXNzTmFtZT1cIm14X0V4aXN0aW5nUGhvbmVOdW1iZXJfdmVyaWZpY2F0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJQbGVhc2UgZW50ZXIgdmVyaWZpY2F0aW9uIGNvZGUgc2VudCB2aWEgdGV4dC5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUudmVyaWZ5RXJyb3J9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5vbkNvbnRpbnVlQ2xpY2t9IGF1dG9Db21wbGV0ZT1cIm9mZlwiIG5vVmFsaWRhdGU9e3RydWV9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdChcIlZlcmlmaWNhdGlvbiBjb2RlXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5jb250aW51ZURpc2FibGVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS52ZXJpZmljYXRpb25Db2RlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vblZlcmlmaWNhdGlvbkNvZGVDaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgPC9zcGFuPjtcclxuICAgICAgICB9IGVsc2UgaWYgKGJvdW5kKSB7XHJcbiAgICAgICAgICAgIHN0YXR1cyA9IDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9FeGlzdGluZ1Bob25lTnVtYmVyX2NvbmZpcm1CdG5cIlxyXG4gICAgICAgICAgICAgICAga2luZD1cImRhbmdlcl9zbVwiXHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uUmV2b2tlQ2xpY2t9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtfdChcIlJldm9rZVwiKX1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzdGF0dXMgPSA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlcl9jb25maXJtQnRuXCJcclxuICAgICAgICAgICAgICAgIGtpbmQ9XCJwcmltYXJ5X3NtXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25TaGFyZUNsaWNrfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7X3QoXCJTaGFyZVwiKX1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlclwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlcl9hZGRyZXNzXCI+K3thZGRyZXNzfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIHtzdGF0dXN9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBob25lTnVtYmVycyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG1zaXNkbnM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgY29udGVudDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5tc2lzZG5zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29udGVudCA9IHRoaXMucHJvcHMubXNpc2Rucy5tYXAoKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiA8UGhvbmVOdW1iZXIgbXNpc2RuPXtlfSBrZXk9e2UuYWRkcmVzc30gLz47XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQgPSA8c3BhbiBjbGFzc05hbWU9XCJteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dFwiPlxyXG4gICAgICAgICAgICAgICAge190KFwiRGlzY292ZXJ5IG9wdGlvbnMgd2lsbCBhcHBlYXIgb25jZSB5b3UgaGF2ZSBhZGRlZCBhIHBob25lIG51bWJlciBhYm92ZS5cIil9XHJcbiAgICAgICAgICAgIDwvc3Bhbj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Bob25lTnVtYmVyc1wiPlxyXG4gICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19