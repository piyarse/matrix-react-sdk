"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _url = _interopRequireDefault(require("url"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _boundThreepids = require("../../../boundThreepids");

var _IdentityAuthClient = _interopRequireDefault(require("../../../IdentityAuthClient"));

var _UrlUtils = require("../../../utils/UrlUtils");

var _IdentityServerUtils = require("../../../utils/IdentityServerUtils");

var _promise = require("../../../utils/promise");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// We'll wait up to this long when checking for 3PID bindings on the IS.
const REACHABILITY_TIMEOUT = 10000; // ms

/**
 * Check an IS URL is valid, including liveness check
 *
 * @param {string} u The url to check
 * @returns {string} null if url passes all checks, otherwise i18ned error string
 */

async function checkIdentityServerUrl(u) {
  const parsedUrl = _url.default.parse(u);

  if (parsedUrl.protocol !== 'https:') return (0, _languageHandler._t)("Identity Server URL must be HTTPS"); // XXX: duplicated logic from js-sdk but it's quite tied up in the validation logic in the
  // js-sdk so probably as easy to duplicate it than to separate it out so we can reuse it

  try {
    const response = await fetch(u + '/_matrix/identity/api/v1');

    if (response.ok) {
      return null;
    } else if (response.status < 200 || response.status >= 300) {
      return (0, _languageHandler._t)("Not a valid Identity Server (status code %(code)s)", {
        code: response.status
      });
    } else {
      return (0, _languageHandler._t)("Could not connect to Identity Server");
    }
  } catch (e) {
    return (0, _languageHandler._t)("Could not connect to Identity Server");
  }
}

class SetIdServer extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onAction", payload => {
      // We react to changes in the ID server in the event the user is staring at this form
      // when changing their identity server on another device.
      if (payload.action !== "id_server_changed") return;
      this.setState({
        currentClientIdServer: _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()
      });
    });
    (0, _defineProperty2.default)(this, "_onIdentityServerChanged", ev => {
      const u = ev.target.value;
      this.setState({
        idServer: u
      });
    });
    (0, _defineProperty2.default)(this, "_getTooltip", () => {
      if (this.state.checking) {
        const InlineSpinner = sdk.getComponent('views.elements.InlineSpinner');
        return _react.default.createElement("div", null, _react.default.createElement(InlineSpinner, null), (0, _languageHandler._t)("Checking server"));
      } else if (this.state.error) {
        return _react.default.createElement("span", {
          className: "warning"
        }, this.state.error);
      } else {
        return null;
      }
    });
    (0, _defineProperty2.default)(this, "_idServerChangeEnabled", () => {
      return !!this.state.idServer && !this.state.busy;
    });
    (0, _defineProperty2.default)(this, "_saveIdServer", fullUrl => {
      // Account data change will update localstorage, client, etc through dispatcher
      _MatrixClientPeg.MatrixClientPeg.get().setAccountData("m.identity_server", {
        base_url: fullUrl
      });

      this.setState({
        busy: false,
        error: null,
        currentClientIdServer: fullUrl,
        idServer: ''
      });
    });
    (0, _defineProperty2.default)(this, "_checkIdServer", async e => {
      e.preventDefault();
      const {
        idServer,
        currentClientIdServer
      } = this.state;
      this.setState({
        busy: true,
        checking: true,
        error: null
      });
      const fullUrl = (0, _UrlUtils.unabbreviateUrl)(idServer);
      let errStr = await checkIdentityServerUrl(fullUrl);

      if (!errStr) {
        try {
          this.setState({
            checking: false
          }); // clear tooltip
          // Test the identity server by trying to register with it. This
          // may result in a terms of service prompt.

          const authClient = new _IdentityAuthClient.default(fullUrl);
          await authClient.getAccessToken();
          let save = true; // Double check that the identity server even has terms of service.

          const hasTerms = await (0, _IdentityServerUtils.doesIdentityServerHaveTerms)(fullUrl);

          if (!hasTerms) {
            const [confirmed] = await this._showNoTermsWarning(fullUrl);
            save = confirmed;
          } // Show a general warning, possibly with details about any bound
          // 3PIDs that would be left behind.


          if (save && currentClientIdServer && fullUrl !== currentClientIdServer) {
            const [confirmed] = await this._showServerChangeWarning({
              title: (0, _languageHandler._t)("Change identity server"),
              unboundMessage: (0, _languageHandler._t)("Disconnect from the identity server <current /> and " + "connect to <new /> instead?", {}, {
                current: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(currentClientIdServer)),
                new: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(idServer))
              }),
              button: (0, _languageHandler._t)("Continue")
            });
            save = confirmed;
          }

          if (save) {
            this._saveIdServer(fullUrl);
          }
        } catch (e) {
          console.error(e);
          errStr = (0, _languageHandler._t)("Terms of service not accepted or the identity server is invalid.");
        }
      }

      this.setState({
        busy: false,
        checking: false,
        error: errStr,
        currentClientIdServer: _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()
      });
    });
    (0, _defineProperty2.default)(this, "_onDisconnectClicked", async () => {
      this.setState({
        disconnectBusy: true
      });

      try {
        const [confirmed] = await this._showServerChangeWarning({
          title: (0, _languageHandler._t)("Disconnect identity server"),
          unboundMessage: (0, _languageHandler._t)("Disconnect from the identity server <idserver />?", {}, {
            idserver: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(this.state.currentClientIdServer))
          }),
          button: (0, _languageHandler._t)("Disconnect")
        });

        if (confirmed) {
          this._disconnectIdServer();
        }
      } finally {
        this.setState({
          disconnectBusy: false
        });
      }
    });
    (0, _defineProperty2.default)(this, "_disconnectIdServer", () => {
      // Account data change will update localstorage, client, etc through dispatcher
      _MatrixClientPeg.MatrixClientPeg.get().setAccountData("m.identity_server", {
        base_url: null // clear

      });

      let newFieldVal = '';

      if ((0, _IdentityServerUtils.getDefaultIdentityServerUrl)()) {
        // Prepopulate the client's default so the user at least has some idea of
        // a valid value they might enter
        newFieldVal = (0, _UrlUtils.abbreviateUrl)((0, _IdentityServerUtils.getDefaultIdentityServerUrl)());
      }

      this.setState({
        busy: false,
        error: null,
        currentClientIdServer: _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl(),
        idServer: newFieldVal
      });
    });
    let defaultIdServer = '';

    if (!_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl() && (0, _IdentityServerUtils.getDefaultIdentityServerUrl)()) {
      // If no ID server is configured but there's one in the config, prepopulate
      // the field to help the user.
      defaultIdServer = (0, _UrlUtils.abbreviateUrl)((0, _IdentityServerUtils.getDefaultIdentityServerUrl)());
    }

    this.state = {
      defaultIdServer,
      currentClientIdServer: _MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl(),
      idServer: "",
      error: null,
      busy: false,
      disconnectBusy: false,
      checking: false
    };
  }

  componentDidMount()
  /*: void*/
  {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
  }

  componentWillUnmount()
  /*: void*/
  {
    _dispatcher.default.unregister(this.dispatcherRef);
  }

  _showNoTermsWarning(fullUrl) {
    const QuestionDialog = sdk.getComponent("views.dialogs.QuestionDialog");

    const {
      finished
    } = _Modal.default.createTrackedDialog('No Terms Warning', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Identity server has no terms of service"),
      description: _react.default.createElement("div", null, _react.default.createElement("span", {
        className: "warning"
      }, (0, _languageHandler._t)("The identity server you have chosen does not have any terms of service.")), _react.default.createElement("span", null, "\xA0", (0, _languageHandler._t)("Only continue if you trust the owner of the server."))),
      button: (0, _languageHandler._t)("Continue")
    });

    return finished;
  }

  async _showServerChangeWarning({
    title,
    unboundMessage,
    button
  }) {
    const {
      currentClientIdServer
    } = this.state;
    let threepids = [];
    let currentServerReachable = true;

    try {
      threepids = await (0, _promise.timeout)((0, _boundThreepids.getThreepidsWithBindStatus)(_MatrixClientPeg.MatrixClientPeg.get()), Promise.reject(new Error("Timeout attempting to reach identity server")), REACHABILITY_TIMEOUT);
    } catch (e) {
      currentServerReachable = false;
      console.warn("Unable to reach identity server at ".concat(currentClientIdServer, " to check ") + "for 3PIDs during IS change flow");
      console.warn(e);
    }

    const boundThreepids = threepids.filter(tp => tp.bound);
    let message;
    let danger = false;
    const messageElements = {
      idserver: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(currentClientIdServer)),
      b: sub => _react.default.createElement("b", null, sub)
    };

    if (!currentServerReachable) {
      message = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You should <b>remove your personal data</b> from identity server " + "<idserver /> before disconnecting. Unfortunately, identity server " + "<idserver /> is currently offline or cannot be reached.", {}, messageElements)), _react.default.createElement("p", null, (0, _languageHandler._t)("You should:")), _react.default.createElement("ul", null, _react.default.createElement("li", null, (0, _languageHandler._t)("check your browser plugins for anything that might block " + "the identity server (such as Privacy Badger)")), _react.default.createElement("li", null, (0, _languageHandler._t)("contact the administrators of identity server <idserver />", {}, {
        idserver: messageElements.idserver
      })), _react.default.createElement("li", null, (0, _languageHandler._t)("wait and try again later"))));
      danger = true;
      button = (0, _languageHandler._t)("Disconnect anyway");
    } else if (boundThreepids.length) {
      message = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You are still <b>sharing your personal data</b> on the identity " + "server <idserver />.", {}, messageElements)), _react.default.createElement("p", null, (0, _languageHandler._t)("We recommend that you remove your email addresses and phone numbers " + "from the identity server before disconnecting.")));
      danger = true;
      button = (0, _languageHandler._t)("Disconnect anyway");
    } else {
      message = unboundMessage;
    }

    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

    const {
      finished
    } = _Modal.default.createTrackedDialog('Identity Server Bound Warning', '', QuestionDialog, {
      title,
      description: message,
      button,
      cancelButton: (0, _languageHandler._t)("Go back"),
      danger
    });

    return finished;
  }

  render() {
    const AccessibleButton = sdk.getComponent('views.elements.AccessibleButton');
    const Field = sdk.getComponent('elements.Field');
    const idServerUrl = this.state.currentClientIdServer;
    let sectionTitle;
    let bodyText;

    if (idServerUrl) {
      sectionTitle = (0, _languageHandler._t)("Identity Server (%(server)s)", {
        server: (0, _UrlUtils.abbreviateUrl)(idServerUrl)
      });
      bodyText = (0, _languageHandler._t)("You are currently using <server></server> to discover and be discoverable by " + "existing contacts you know. You can change your identity server below.", {}, {
        server: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(idServerUrl))
      });

      if (this.props.missingTerms) {
        bodyText = (0, _languageHandler._t)("If you don't want to use <server /> to discover and be discoverable by existing " + "contacts you know, enter another identity server below.", {}, {
          server: sub => _react.default.createElement("b", null, (0, _UrlUtils.abbreviateUrl)(idServerUrl))
        });
      }
    } else {
      sectionTitle = (0, _languageHandler._t)("Identity Server");
      bodyText = (0, _languageHandler._t)("You are not currently using an identity server. " + "To discover and be discoverable by existing contacts you know, " + "add one below.");
    }

    let discoSection;

    if (idServerUrl) {
      let discoButtonContent = (0, _languageHandler._t)("Disconnect");
      let discoBodyText = (0, _languageHandler._t)("Disconnecting from your identity server will mean you " + "won't be discoverable by other users and you won't be " + "able to invite others by email or phone.");

      if (this.props.missingTerms) {
        discoBodyText = (0, _languageHandler._t)("Using an identity server is optional. If you choose not to " + "use an identity server, you won't be discoverable by other users " + "and you won't be able to invite others by email or phone.");
        discoButtonContent = (0, _languageHandler._t)("Do not use an identity server");
      }

      if (this.state.disconnectBusy) {
        const InlineSpinner = sdk.getComponent('views.elements.InlineSpinner');
        discoButtonContent = _react.default.createElement(InlineSpinner, null);
      }

      discoSection = _react.default.createElement("div", null, _react.default.createElement("span", {
        className: "mx_SettingsTab_subsectionText"
      }, discoBodyText), _react.default.createElement(AccessibleButton, {
        onClick: this._onDisconnectClicked,
        kind: "danger_sm"
      }, discoButtonContent));
    }

    return _react.default.createElement("form", {
      className: "mx_SettingsTab_section mx_SetIdServer",
      onSubmit: this._checkIdServer
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, sectionTitle), _react.default.createElement("span", {
      className: "mx_SettingsTab_subsectionText"
    }, bodyText), _react.default.createElement(Field, {
      label: (0, _languageHandler._t)("Enter a new identity server"),
      type: "text",
      autoComplete: "off",
      placeholder: this.state.defaultIdServer,
      value: this.state.idServer,
      onChange: this._onIdentityServerChanged,
      tooltipContent: this._getTooltip(),
      tooltipClassName: "mx_SetIdServer_tooltip",
      disabled: this.state.busy,
      flagInvalid: !!this.state.error
    }), _react.default.createElement(AccessibleButton, {
      type: "submit",
      kind: "primary_sm",
      onClick: this._checkIdServer,
      disabled: !this._idServerChangeEnabled()
    }, (0, _languageHandler._t)("Change")), discoSection);
  }

}

exports.default = SetIdServer;
(0, _defineProperty2.default)(SetIdServer, "propTypes", {
  // Whether or not the ID server is missing terms. This affects the text
  // shown to the user.
  missingTerms: _propTypes.default.bool
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL1NldElkU2VydmVyLmpzIl0sIm5hbWVzIjpbIlJFQUNIQUJJTElUWV9USU1FT1VUIiwiY2hlY2tJZGVudGl0eVNlcnZlclVybCIsInUiLCJwYXJzZWRVcmwiLCJ1cmwiLCJwYXJzZSIsInByb3RvY29sIiwicmVzcG9uc2UiLCJmZXRjaCIsIm9rIiwic3RhdHVzIiwiY29kZSIsImUiLCJTZXRJZFNlcnZlciIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwYXlsb2FkIiwiYWN0aW9uIiwic2V0U3RhdGUiLCJjdXJyZW50Q2xpZW50SWRTZXJ2ZXIiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJnZXRJZGVudGl0eVNlcnZlclVybCIsImV2IiwidGFyZ2V0IiwidmFsdWUiLCJpZFNlcnZlciIsInN0YXRlIiwiY2hlY2tpbmciLCJJbmxpbmVTcGlubmVyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZXJyb3IiLCJidXN5IiwiZnVsbFVybCIsInNldEFjY291bnREYXRhIiwiYmFzZV91cmwiLCJwcmV2ZW50RGVmYXVsdCIsImVyclN0ciIsImF1dGhDbGllbnQiLCJJZGVudGl0eUF1dGhDbGllbnQiLCJnZXRBY2Nlc3NUb2tlbiIsInNhdmUiLCJoYXNUZXJtcyIsImNvbmZpcm1lZCIsIl9zaG93Tm9UZXJtc1dhcm5pbmciLCJfc2hvd1NlcnZlckNoYW5nZVdhcm5pbmciLCJ0aXRsZSIsInVuYm91bmRNZXNzYWdlIiwiY3VycmVudCIsInN1YiIsIm5ldyIsImJ1dHRvbiIsIl9zYXZlSWRTZXJ2ZXIiLCJjb25zb2xlIiwiZGlzY29ubmVjdEJ1c3kiLCJpZHNlcnZlciIsIl9kaXNjb25uZWN0SWRTZXJ2ZXIiLCJuZXdGaWVsZFZhbCIsImRlZmF1bHRJZFNlcnZlciIsImNvbXBvbmVudERpZE1vdW50IiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJRdWVzdGlvbkRpYWxvZyIsImZpbmlzaGVkIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwiZGVzY3JpcHRpb24iLCJ0aHJlZXBpZHMiLCJjdXJyZW50U2VydmVyUmVhY2hhYmxlIiwiUHJvbWlzZSIsInJlamVjdCIsIkVycm9yIiwid2FybiIsImJvdW5kVGhyZWVwaWRzIiwiZmlsdGVyIiwidHAiLCJib3VuZCIsIm1lc3NhZ2UiLCJkYW5nZXIiLCJtZXNzYWdlRWxlbWVudHMiLCJiIiwibGVuZ3RoIiwiY2FuY2VsQnV0dG9uIiwicmVuZGVyIiwiQWNjZXNzaWJsZUJ1dHRvbiIsIkZpZWxkIiwiaWRTZXJ2ZXJVcmwiLCJzZWN0aW9uVGl0bGUiLCJib2R5VGV4dCIsInNlcnZlciIsInByb3BzIiwibWlzc2luZ1Rlcm1zIiwiZGlzY29TZWN0aW9uIiwiZGlzY29CdXR0b25Db250ZW50IiwiZGlzY29Cb2R5VGV4dCIsIl9vbkRpc2Nvbm5lY3RDbGlja2VkIiwiX2NoZWNrSWRTZXJ2ZXIiLCJfb25JZGVudGl0eVNlcnZlckNoYW5nZWQiLCJfZ2V0VG9vbHRpcCIsIl9pZFNlcnZlckNoYW5nZUVuYWJsZWQiLCJQcm9wVHlwZXMiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTVCQTs7Ozs7Ozs7Ozs7Ozs7O0FBOEJBO0FBQ0EsTUFBTUEsb0JBQW9CLEdBQUcsS0FBN0IsQyxDQUFvQzs7QUFFcEM7Ozs7Ozs7QUFNQSxlQUFlQyxzQkFBZixDQUFzQ0MsQ0FBdEMsRUFBeUM7QUFDckMsUUFBTUMsU0FBUyxHQUFHQyxhQUFJQyxLQUFKLENBQVVILENBQVYsQ0FBbEI7O0FBRUEsTUFBSUMsU0FBUyxDQUFDRyxRQUFWLEtBQXVCLFFBQTNCLEVBQXFDLE9BQU8seUJBQUcsbUNBQUgsQ0FBUCxDQUhBLENBS3JDO0FBQ0E7O0FBQ0EsTUFBSTtBQUNBLFVBQU1DLFFBQVEsR0FBRyxNQUFNQyxLQUFLLENBQUNOLENBQUMsR0FBRywwQkFBTCxDQUE1Qjs7QUFDQSxRQUFJSyxRQUFRLENBQUNFLEVBQWIsRUFBaUI7QUFDYixhQUFPLElBQVA7QUFDSCxLQUZELE1BRU8sSUFBSUYsUUFBUSxDQUFDRyxNQUFULEdBQWtCLEdBQWxCLElBQXlCSCxRQUFRLENBQUNHLE1BQVQsSUFBbUIsR0FBaEQsRUFBcUQ7QUFDeEQsYUFBTyx5QkFBRyxvREFBSCxFQUF5RDtBQUFDQyxRQUFBQSxJQUFJLEVBQUVKLFFBQVEsQ0FBQ0c7QUFBaEIsT0FBekQsQ0FBUDtBQUNILEtBRk0sTUFFQTtBQUNILGFBQU8seUJBQUcsc0NBQUgsQ0FBUDtBQUNIO0FBQ0osR0FURCxDQVNFLE9BQU9FLENBQVAsRUFBVTtBQUNSLFdBQU8seUJBQUcsc0NBQUgsQ0FBUDtBQUNIO0FBQ0o7O0FBRWMsTUFBTUMsV0FBTixTQUEwQkMsZUFBTUMsU0FBaEMsQ0FBMEM7QUFPckRDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUsb0RBNkJGQyxPQUFELElBQWE7QUFDcEI7QUFDQTtBQUNBLFVBQUlBLE9BQU8sQ0FBQ0MsTUFBUixLQUFtQixtQkFBdkIsRUFBNEM7QUFFNUMsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLHFCQUFxQixFQUFFQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxvQkFBdEI7QUFEYixPQUFkO0FBR0gsS0FyQ2E7QUFBQSxvRUF1Q2NDLEVBQUQsSUFBUTtBQUMvQixZQUFNdEIsQ0FBQyxHQUFHc0IsRUFBRSxDQUFDQyxNQUFILENBQVVDLEtBQXBCO0FBRUEsV0FBS1AsUUFBTCxDQUFjO0FBQUNRLFFBQUFBLFFBQVEsRUFBRXpCO0FBQVgsT0FBZDtBQUNILEtBM0NhO0FBQUEsdURBNkNBLE1BQU07QUFDaEIsVUFBSSxLQUFLMEIsS0FBTCxDQUFXQyxRQUFmLEVBQXlCO0FBQ3JCLGNBQU1DLGFBQWEsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLGVBQU8sMENBQ0gsNkJBQUMsYUFBRCxPQURHLEVBRUQseUJBQUcsaUJBQUgsQ0FGQyxDQUFQO0FBSUgsT0FORCxNQU1PLElBQUksS0FBS0osS0FBTCxDQUFXSyxLQUFmLEVBQXNCO0FBQ3pCLGVBQU87QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixXQUEyQixLQUFLTCxLQUFMLENBQVdLLEtBQXRDLENBQVA7QUFDSCxPQUZNLE1BRUE7QUFDSCxlQUFPLElBQVA7QUFDSDtBQUNKLEtBekRhO0FBQUEsa0VBMkRXLE1BQU07QUFDM0IsYUFBTyxDQUFDLENBQUMsS0FBS0wsS0FBTCxDQUFXRCxRQUFiLElBQXlCLENBQUMsS0FBS0MsS0FBTCxDQUFXTSxJQUE1QztBQUNILEtBN0RhO0FBQUEseURBK0RHQyxPQUFELElBQWE7QUFDekI7QUFDQWQsdUNBQWdCQyxHQUFoQixHQUFzQmMsY0FBdEIsQ0FBcUMsbUJBQXJDLEVBQTBEO0FBQ3REQyxRQUFBQSxRQUFRLEVBQUVGO0FBRDRDLE9BQTFEOztBQUdBLFdBQUtoQixRQUFMLENBQWM7QUFDVmUsUUFBQUEsSUFBSSxFQUFFLEtBREk7QUFFVkQsUUFBQUEsS0FBSyxFQUFFLElBRkc7QUFHVmIsUUFBQUEscUJBQXFCLEVBQUVlLE9BSGI7QUFJVlIsUUFBQUEsUUFBUSxFQUFFO0FBSkEsT0FBZDtBQU1ILEtBMUVhO0FBQUEsMERBNEVHLE1BQU9mLENBQVAsSUFBYTtBQUMxQkEsTUFBQUEsQ0FBQyxDQUFDMEIsY0FBRjtBQUNBLFlBQU07QUFBRVgsUUFBQUEsUUFBRjtBQUFZUCxRQUFBQTtBQUFaLFVBQXNDLEtBQUtRLEtBQWpEO0FBRUEsV0FBS1QsUUFBTCxDQUFjO0FBQUNlLFFBQUFBLElBQUksRUFBRSxJQUFQO0FBQWFMLFFBQUFBLFFBQVEsRUFBRSxJQUF2QjtBQUE2QkksUUFBQUEsS0FBSyxFQUFFO0FBQXBDLE9BQWQ7QUFFQSxZQUFNRSxPQUFPLEdBQUcsK0JBQWdCUixRQUFoQixDQUFoQjtBQUVBLFVBQUlZLE1BQU0sR0FBRyxNQUFNdEMsc0JBQXNCLENBQUNrQyxPQUFELENBQXpDOztBQUNBLFVBQUksQ0FBQ0ksTUFBTCxFQUFhO0FBQ1QsWUFBSTtBQUNBLGVBQUtwQixRQUFMLENBQWM7QUFBQ1UsWUFBQUEsUUFBUSxFQUFFO0FBQVgsV0FBZCxFQURBLENBQ2tDO0FBRWxDO0FBQ0E7O0FBQ0EsZ0JBQU1XLFVBQVUsR0FBRyxJQUFJQywyQkFBSixDQUF1Qk4sT0FBdkIsQ0FBbkI7QUFDQSxnQkFBTUssVUFBVSxDQUFDRSxjQUFYLEVBQU47QUFFQSxjQUFJQyxJQUFJLEdBQUcsSUFBWCxDQVJBLENBVUE7O0FBQ0EsZ0JBQU1DLFFBQVEsR0FBRyxNQUFNLHNEQUE0QlQsT0FBNUIsQ0FBdkI7O0FBQ0EsY0FBSSxDQUFDUyxRQUFMLEVBQWU7QUFDWCxrQkFBTSxDQUFDQyxTQUFELElBQWMsTUFBTSxLQUFLQyxtQkFBTCxDQUF5QlgsT0FBekIsQ0FBMUI7QUFDQVEsWUFBQUEsSUFBSSxHQUFHRSxTQUFQO0FBQ0gsV0FmRCxDQWlCQTtBQUNBOzs7QUFDQSxjQUFJRixJQUFJLElBQUl2QixxQkFBUixJQUFpQ2UsT0FBTyxLQUFLZixxQkFBakQsRUFBd0U7QUFDcEUsa0JBQU0sQ0FBQ3lCLFNBQUQsSUFBYyxNQUFNLEtBQUtFLHdCQUFMLENBQThCO0FBQ3BEQyxjQUFBQSxLQUFLLEVBQUUseUJBQUcsd0JBQUgsQ0FENkM7QUFFcERDLGNBQUFBLGNBQWMsRUFBRSx5QkFDWix5REFDQSw2QkFGWSxFQUVtQixFQUZuQixFQUdaO0FBQ0lDLGdCQUFBQSxPQUFPLEVBQUVDLEdBQUcsSUFBSSx3Q0FBSSw2QkFBYy9CLHFCQUFkLENBQUosQ0FEcEI7QUFFSWdDLGdCQUFBQSxHQUFHLEVBQUVELEdBQUcsSUFBSSx3Q0FBSSw2QkFBY3hCLFFBQWQsQ0FBSjtBQUZoQixlQUhZLENBRm9DO0FBVXBEMEIsY0FBQUEsTUFBTSxFQUFFLHlCQUFHLFVBQUg7QUFWNEMsYUFBOUIsQ0FBMUI7QUFZQVYsWUFBQUEsSUFBSSxHQUFHRSxTQUFQO0FBQ0g7O0FBRUQsY0FBSUYsSUFBSixFQUFVO0FBQ04saUJBQUtXLGFBQUwsQ0FBbUJuQixPQUFuQjtBQUNIO0FBQ0osU0F0Q0QsQ0FzQ0UsT0FBT3ZCLENBQVAsRUFBVTtBQUNSMkMsVUFBQUEsT0FBTyxDQUFDdEIsS0FBUixDQUFjckIsQ0FBZDtBQUNBMkIsVUFBQUEsTUFBTSxHQUFHLHlCQUFHLGtFQUFILENBQVQ7QUFDSDtBQUNKOztBQUNELFdBQUtwQixRQUFMLENBQWM7QUFDVmUsUUFBQUEsSUFBSSxFQUFFLEtBREk7QUFFVkwsUUFBQUEsUUFBUSxFQUFFLEtBRkE7QUFHVkksUUFBQUEsS0FBSyxFQUFFTSxNQUhHO0FBSVZuQixRQUFBQSxxQkFBcUIsRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsb0JBQXRCO0FBSmIsT0FBZDtBQU1ILEtBdklhO0FBQUEsZ0VBNEpTLFlBQVk7QUFDL0IsV0FBS0osUUFBTCxDQUFjO0FBQUNxQyxRQUFBQSxjQUFjLEVBQUU7QUFBakIsT0FBZDs7QUFDQSxVQUFJO0FBQ0EsY0FBTSxDQUFDWCxTQUFELElBQWMsTUFBTSxLQUFLRSx3QkFBTCxDQUE4QjtBQUNwREMsVUFBQUEsS0FBSyxFQUFFLHlCQUFHLDRCQUFILENBRDZDO0FBRXBEQyxVQUFBQSxjQUFjLEVBQUUseUJBQ1osbURBRFksRUFDeUMsRUFEekMsRUFFWjtBQUFDUSxZQUFBQSxRQUFRLEVBQUVOLEdBQUcsSUFBSSx3Q0FBSSw2QkFBYyxLQUFLdkIsS0FBTCxDQUFXUixxQkFBekIsQ0FBSjtBQUFsQixXQUZZLENBRm9DO0FBTXBEaUMsVUFBQUEsTUFBTSxFQUFFLHlCQUFHLFlBQUg7QUFONEMsU0FBOUIsQ0FBMUI7O0FBUUEsWUFBSVIsU0FBSixFQUFlO0FBQ1gsZUFBS2EsbUJBQUw7QUFDSDtBQUNKLE9BWkQsU0FZVTtBQUNOLGFBQUt2QyxRQUFMLENBQWM7QUFBQ3FDLFVBQUFBLGNBQWMsRUFBRTtBQUFqQixTQUFkO0FBQ0g7QUFDSixLQTdLYTtBQUFBLCtEQTJQUSxNQUFNO0FBQ3hCO0FBQ0FuQyx1Q0FBZ0JDLEdBQWhCLEdBQXNCYyxjQUF0QixDQUFxQyxtQkFBckMsRUFBMEQ7QUFDdERDLFFBQUFBLFFBQVEsRUFBRSxJQUQ0QyxDQUN0Qzs7QUFEc0MsT0FBMUQ7O0FBSUEsVUFBSXNCLFdBQVcsR0FBRyxFQUFsQjs7QUFDQSxVQUFJLHVEQUFKLEVBQW1DO0FBQy9CO0FBQ0E7QUFDQUEsUUFBQUEsV0FBVyxHQUFHLDZCQUFjLHVEQUFkLENBQWQ7QUFDSDs7QUFFRCxXQUFLeEMsUUFBTCxDQUFjO0FBQ1ZlLFFBQUFBLElBQUksRUFBRSxLQURJO0FBRVZELFFBQUFBLEtBQUssRUFBRSxJQUZHO0FBR1ZiLFFBQUFBLHFCQUFxQixFQUFFQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxvQkFBdEIsRUFIYjtBQUlWSSxRQUFBQSxRQUFRLEVBQUVnQztBQUpBLE9BQWQ7QUFNSCxLQTlRYTtBQUdWLFFBQUlDLGVBQWUsR0FBRyxFQUF0Qjs7QUFDQSxRQUFJLENBQUN2QyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxvQkFBdEIsRUFBRCxJQUFpRCx1REFBckQsRUFBb0Y7QUFDaEY7QUFDQTtBQUNBcUMsTUFBQUEsZUFBZSxHQUFHLDZCQUFjLHVEQUFkLENBQWxCO0FBQ0g7O0FBRUQsU0FBS2hDLEtBQUwsR0FBYTtBQUNUZ0MsTUFBQUEsZUFEUztBQUVUeEMsTUFBQUEscUJBQXFCLEVBQUVDLGlDQUFnQkMsR0FBaEIsR0FBc0JDLG9CQUF0QixFQUZkO0FBR1RJLE1BQUFBLFFBQVEsRUFBRSxFQUhEO0FBSVRNLE1BQUFBLEtBQUssRUFBRSxJQUpFO0FBS1RDLE1BQUFBLElBQUksRUFBRSxLQUxHO0FBTVRzQixNQUFBQSxjQUFjLEVBQUUsS0FOUDtBQU9UM0IsTUFBQUEsUUFBUSxFQUFFO0FBUEQsS0FBYjtBQVNIOztBQUVEZ0MsRUFBQUEsaUJBQWlCO0FBQUE7QUFBUztBQUN0QixTQUFLQyxhQUFMLEdBQXFCQyxvQkFBSUMsUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQjtBQUFBO0FBQVM7QUFDekJILHdCQUFJSSxVQUFKLENBQWUsS0FBS0wsYUFBcEI7QUFDSDs7QUE4R0RoQixFQUFBQSxtQkFBbUIsQ0FBQ1gsT0FBRCxFQUFVO0FBQ3pCLFVBQU1pQyxjQUFjLEdBQUdyQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXZCOztBQUNBLFVBQU07QUFBRXFDLE1BQUFBO0FBQUYsUUFBZUMsZUFBTUMsbUJBQU4sQ0FBMEIsa0JBQTFCLEVBQThDLEVBQTlDLEVBQWtESCxjQUFsRCxFQUFrRTtBQUNuRnBCLE1BQUFBLEtBQUssRUFBRSx5QkFBRyx5Q0FBSCxDQUQ0RTtBQUVuRndCLE1BQUFBLFdBQVcsRUFDUCwwQ0FDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0sseUJBQUcseUVBQUgsQ0FETCxDQURKLEVBSUksbURBQ1cseUJBQUcscURBQUgsQ0FEWCxDQUpKLENBSCtFO0FBWW5GbkIsTUFBQUEsTUFBTSxFQUFFLHlCQUFHLFVBQUg7QUFaMkUsS0FBbEUsQ0FBckI7O0FBY0EsV0FBT2dCLFFBQVA7QUFDSDs7QUFxQkQsUUFBTXRCLHdCQUFOLENBQStCO0FBQUVDLElBQUFBLEtBQUY7QUFBU0MsSUFBQUEsY0FBVDtBQUF5QkksSUFBQUE7QUFBekIsR0FBL0IsRUFBa0U7QUFDOUQsVUFBTTtBQUFFakMsTUFBQUE7QUFBRixRQUE0QixLQUFLUSxLQUF2QztBQUVBLFFBQUk2QyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxRQUFJQyxzQkFBc0IsR0FBRyxJQUE3Qjs7QUFDQSxRQUFJO0FBQ0FELE1BQUFBLFNBQVMsR0FBRyxNQUFNLHNCQUNkLGdEQUEyQnBELGlDQUFnQkMsR0FBaEIsRUFBM0IsQ0FEYyxFQUVkcUQsT0FBTyxDQUFDQyxNQUFSLENBQWUsSUFBSUMsS0FBSixDQUFVLDZDQUFWLENBQWYsQ0FGYyxFQUdkN0Usb0JBSGMsQ0FBbEI7QUFLSCxLQU5ELENBTUUsT0FBT1ksQ0FBUCxFQUFVO0FBQ1I4RCxNQUFBQSxzQkFBc0IsR0FBRyxLQUF6QjtBQUNBbkIsTUFBQUEsT0FBTyxDQUFDdUIsSUFBUixDQUNJLDZDQUFzQzFELHFCQUF0QyxtREFESjtBQUlBbUMsTUFBQUEsT0FBTyxDQUFDdUIsSUFBUixDQUFhbEUsQ0FBYjtBQUNIOztBQUNELFVBQU1tRSxjQUFjLEdBQUdOLFNBQVMsQ0FBQ08sTUFBVixDQUFpQkMsRUFBRSxJQUFJQSxFQUFFLENBQUNDLEtBQTFCLENBQXZCO0FBQ0EsUUFBSUMsT0FBSjtBQUNBLFFBQUlDLE1BQU0sR0FBRyxLQUFiO0FBQ0EsVUFBTUMsZUFBZSxHQUFHO0FBQ3BCNUIsTUFBQUEsUUFBUSxFQUFFTixHQUFHLElBQUksd0NBQUksNkJBQWMvQixxQkFBZCxDQUFKLENBREc7QUFFcEJrRSxNQUFBQSxDQUFDLEVBQUVuQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFGVSxLQUF4Qjs7QUFJQSxRQUFJLENBQUN1QixzQkFBTCxFQUE2QjtBQUN6QlMsTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLHNFQUNBLG9FQURBLEdBRUEseURBSEEsRUFJQSxFQUpBLEVBSUlFLGVBSkosQ0FBSixDQURNLEVBT04sd0NBQUkseUJBQUcsYUFBSCxDQUFKLENBUE0sRUFRTix5Q0FDSSx5Q0FBSyx5QkFDRCw4REFDQSw4Q0FGQyxDQUFMLENBREosRUFLSSx5Q0FBSyx5QkFBRyw0REFBSCxFQUFpRSxFQUFqRSxFQUFxRTtBQUN0RTVCLFFBQUFBLFFBQVEsRUFBRTRCLGVBQWUsQ0FBQzVCO0FBRDRDLE9BQXJFLENBQUwsQ0FMSixFQVFJLHlDQUFLLHlCQUFHLDBCQUFILENBQUwsQ0FSSixDQVJNLENBQVY7QUFtQkEyQixNQUFBQSxNQUFNLEdBQUcsSUFBVDtBQUNBL0IsTUFBQUEsTUFBTSxHQUFHLHlCQUFHLG1CQUFILENBQVQ7QUFDSCxLQXRCRCxNQXNCTyxJQUFJMEIsY0FBYyxDQUFDUSxNQUFuQixFQUEyQjtBQUM5QkosTUFBQUEsT0FBTyxHQUFHLDBDQUNOLHdDQUFJLHlCQUNBLHFFQUNBLHNCQUZBLEVBRXdCLEVBRnhCLEVBRTRCRSxlQUY1QixDQUFKLENBRE0sRUFLTix3Q0FBSSx5QkFDQSx5RUFDQSxnREFGQSxDQUFKLENBTE0sQ0FBVjtBQVVBRCxNQUFBQSxNQUFNLEdBQUcsSUFBVDtBQUNBL0IsTUFBQUEsTUFBTSxHQUFHLHlCQUFHLG1CQUFILENBQVQ7QUFDSCxLQWJNLE1BYUE7QUFDSDhCLE1BQUFBLE9BQU8sR0FBR2xDLGNBQVY7QUFDSDs7QUFFRCxVQUFNbUIsY0FBYyxHQUFHckMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQSxVQUFNO0FBQUVxQyxNQUFBQTtBQUFGLFFBQWVDLGVBQU1DLG1CQUFOLENBQTBCLCtCQUExQixFQUEyRCxFQUEzRCxFQUErREgsY0FBL0QsRUFBK0U7QUFDaEdwQixNQUFBQSxLQURnRztBQUVoR3dCLE1BQUFBLFdBQVcsRUFBRVcsT0FGbUY7QUFHaEc5QixNQUFBQSxNQUhnRztBQUloR21DLE1BQUFBLFlBQVksRUFBRSx5QkFBRyxTQUFILENBSmtGO0FBS2hHSixNQUFBQTtBQUxnRyxLQUEvRSxDQUFyQjs7QUFPQSxXQUFPZixRQUFQO0FBQ0g7O0FBdUJEb0IsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsZ0JBQWdCLEdBQUczRCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQXpCO0FBQ0EsVUFBTTJELEtBQUssR0FBRzVELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBZDtBQUNBLFVBQU00RCxXQUFXLEdBQUcsS0FBS2hFLEtBQUwsQ0FBV1IscUJBQS9CO0FBQ0EsUUFBSXlFLFlBQUo7QUFDQSxRQUFJQyxRQUFKOztBQUNBLFFBQUlGLFdBQUosRUFBaUI7QUFDYkMsTUFBQUEsWUFBWSxHQUFHLHlCQUFHLDhCQUFILEVBQW1DO0FBQUVFLFFBQUFBLE1BQU0sRUFBRSw2QkFBY0gsV0FBZDtBQUFWLE9BQW5DLENBQWY7QUFDQUUsTUFBQUEsUUFBUSxHQUFHLHlCQUNQLGtGQUNBLHdFQUZPLEVBR1AsRUFITyxFQUlQO0FBQUVDLFFBQUFBLE1BQU0sRUFBRTVDLEdBQUcsSUFBSSx3Q0FBSSw2QkFBY3lDLFdBQWQsQ0FBSjtBQUFqQixPQUpPLENBQVg7O0FBTUEsVUFBSSxLQUFLSSxLQUFMLENBQVdDLFlBQWYsRUFBNkI7QUFDekJILFFBQUFBLFFBQVEsR0FBRyx5QkFDUCxxRkFDQSx5REFGTyxFQUdQLEVBSE8sRUFHSDtBQUFDQyxVQUFBQSxNQUFNLEVBQUU1QyxHQUFHLElBQUksd0NBQUksNkJBQWN5QyxXQUFkLENBQUo7QUFBaEIsU0FIRyxDQUFYO0FBS0g7QUFDSixLQWZELE1BZU87QUFDSEMsTUFBQUEsWUFBWSxHQUFHLHlCQUFHLGlCQUFILENBQWY7QUFDQUMsTUFBQUEsUUFBUSxHQUFHLHlCQUNQLHFEQUNBLGlFQURBLEdBRUEsZ0JBSE8sQ0FBWDtBQUtIOztBQUVELFFBQUlJLFlBQUo7O0FBQ0EsUUFBSU4sV0FBSixFQUFpQjtBQUNiLFVBQUlPLGtCQUFrQixHQUFHLHlCQUFHLFlBQUgsQ0FBekI7QUFDQSxVQUFJQyxhQUFhLEdBQUcseUJBQ2hCLDJEQUNBLHdEQURBLEdBRUEsMENBSGdCLENBQXBCOztBQUtBLFVBQUksS0FBS0osS0FBTCxDQUFXQyxZQUFmLEVBQTZCO0FBQ3pCRyxRQUFBQSxhQUFhLEdBQUcseUJBQ1osZ0VBQ0EsbUVBREEsR0FFQSwyREFIWSxDQUFoQjtBQUtBRCxRQUFBQSxrQkFBa0IsR0FBRyx5QkFBRywrQkFBSCxDQUFyQjtBQUNIOztBQUNELFVBQUksS0FBS3ZFLEtBQUwsQ0FBVzRCLGNBQWYsRUFBK0I7QUFDM0IsY0FBTTFCLGFBQWEsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBbUUsUUFBQUEsa0JBQWtCLEdBQUcsNkJBQUMsYUFBRCxPQUFyQjtBQUNIOztBQUNERCxNQUFBQSxZQUFZLEdBQUcsMENBQ1g7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUFpREUsYUFBakQsQ0FEVyxFQUVYLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUtDLG9CQUFoQztBQUFzRCxRQUFBLElBQUksRUFBQztBQUEzRCxTQUNLRixrQkFETCxDQUZXLENBQWY7QUFNSDs7QUFFRCxXQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUMsdUNBQWhCO0FBQXdELE1BQUEsUUFBUSxFQUFFLEtBQUtHO0FBQXZFLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNLVCxZQURMLENBREosRUFJSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ0tDLFFBREwsQ0FKSixFQU9JLDZCQUFDLEtBQUQ7QUFDSSxNQUFBLEtBQUssRUFBRSx5QkFBRyw2QkFBSCxDQURYO0FBRUksTUFBQSxJQUFJLEVBQUMsTUFGVDtBQUdJLE1BQUEsWUFBWSxFQUFDLEtBSGpCO0FBSUksTUFBQSxXQUFXLEVBQUUsS0FBS2xFLEtBQUwsQ0FBV2dDLGVBSjVCO0FBS0ksTUFBQSxLQUFLLEVBQUUsS0FBS2hDLEtBQUwsQ0FBV0QsUUFMdEI7QUFNSSxNQUFBLFFBQVEsRUFBRSxLQUFLNEUsd0JBTm5CO0FBT0ksTUFBQSxjQUFjLEVBQUUsS0FBS0MsV0FBTCxFQVBwQjtBQVFJLE1BQUEsZ0JBQWdCLEVBQUMsd0JBUnJCO0FBU0ksTUFBQSxRQUFRLEVBQUUsS0FBSzVFLEtBQUwsQ0FBV00sSUFUekI7QUFVSSxNQUFBLFdBQVcsRUFBRSxDQUFDLENBQUMsS0FBS04sS0FBTCxDQUFXSztBQVY5QixNQVBKLEVBbUJJLDZCQUFDLGdCQUFEO0FBQWtCLE1BQUEsSUFBSSxFQUFDLFFBQXZCO0FBQWdDLE1BQUEsSUFBSSxFQUFDLFlBQXJDO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBS3FFLGNBRGxCO0FBRUksTUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLRyxzQkFBTDtBQUZmLE9BR0UseUJBQUcsUUFBSCxDQUhGLENBbkJKLEVBdUJLUCxZQXZCTCxDQURKO0FBMkJIOztBQTVXb0Q7Ozs4QkFBcENyRixXLGVBQ0U7QUFDZjtBQUNBO0FBQ0FvRixFQUFBQSxZQUFZLEVBQUVTLG1CQUFVQztBQUhULEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB1cmwgZnJvbSAndXJsJztcclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCB7IGdldFRocmVlcGlkc1dpdGhCaW5kU3RhdHVzIH0gZnJvbSAnLi4vLi4vLi4vYm91bmRUaHJlZXBpZHMnO1xyXG5pbXBvcnQgSWRlbnRpdHlBdXRoQ2xpZW50IGZyb20gXCIuLi8uLi8uLi9JZGVudGl0eUF1dGhDbGllbnRcIjtcclxuaW1wb3J0IHthYmJyZXZpYXRlVXJsLCB1bmFiYnJldmlhdGVVcmx9IGZyb20gXCIuLi8uLi8uLi91dGlscy9VcmxVdGlsc1wiO1xyXG5pbXBvcnQgeyBnZXREZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwsIGRvZXNJZGVudGl0eVNlcnZlckhhdmVUZXJtcyB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL0lkZW50aXR5U2VydmVyVXRpbHMnO1xyXG5pbXBvcnQge3RpbWVvdXR9IGZyb20gXCIuLi8uLi8uLi91dGlscy9wcm9taXNlXCI7XHJcblxyXG4vLyBXZSdsbCB3YWl0IHVwIHRvIHRoaXMgbG9uZyB3aGVuIGNoZWNraW5nIGZvciAzUElEIGJpbmRpbmdzIG9uIHRoZSBJUy5cclxuY29uc3QgUkVBQ0hBQklMSVRZX1RJTUVPVVQgPSAxMDAwMDsgLy8gbXNcclxuXHJcbi8qKlxyXG4gKiBDaGVjayBhbiBJUyBVUkwgaXMgdmFsaWQsIGluY2x1ZGluZyBsaXZlbmVzcyBjaGVja1xyXG4gKlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gdSBUaGUgdXJsIHRvIGNoZWNrXHJcbiAqIEByZXR1cm5zIHtzdHJpbmd9IG51bGwgaWYgdXJsIHBhc3NlcyBhbGwgY2hlY2tzLCBvdGhlcndpc2UgaTE4bmVkIGVycm9yIHN0cmluZ1xyXG4gKi9cclxuYXN5bmMgZnVuY3Rpb24gY2hlY2tJZGVudGl0eVNlcnZlclVybCh1KSB7XHJcbiAgICBjb25zdCBwYXJzZWRVcmwgPSB1cmwucGFyc2UodSk7XHJcblxyXG4gICAgaWYgKHBhcnNlZFVybC5wcm90b2NvbCAhPT0gJ2h0dHBzOicpIHJldHVybiBfdChcIklkZW50aXR5IFNlcnZlciBVUkwgbXVzdCBiZSBIVFRQU1wiKTtcclxuXHJcbiAgICAvLyBYWFg6IGR1cGxpY2F0ZWQgbG9naWMgZnJvbSBqcy1zZGsgYnV0IGl0J3MgcXVpdGUgdGllZCB1cCBpbiB0aGUgdmFsaWRhdGlvbiBsb2dpYyBpbiB0aGVcclxuICAgIC8vIGpzLXNkayBzbyBwcm9iYWJseSBhcyBlYXN5IHRvIGR1cGxpY2F0ZSBpdCB0aGFuIHRvIHNlcGFyYXRlIGl0IG91dCBzbyB3ZSBjYW4gcmV1c2UgaXRcclxuICAgIHRyeSB7XHJcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaCh1ICsgJy9fbWF0cml4L2lkZW50aXR5L2FwaS92MScpO1xyXG4gICAgICAgIGlmIChyZXNwb25zZS5vaykge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyA8IDIwMCB8fCByZXNwb25zZS5zdGF0dXMgPj0gMzAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdChcIk5vdCBhIHZhbGlkIElkZW50aXR5IFNlcnZlciAoc3RhdHVzIGNvZGUgJShjb2RlKXMpXCIsIHtjb2RlOiByZXNwb25zZS5zdGF0dXN9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gX3QoXCJDb3VsZCBub3QgY29ubmVjdCB0byBJZGVudGl0eSBTZXJ2ZXJcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgIHJldHVybiBfdChcIkNvdWxkIG5vdCBjb25uZWN0IHRvIElkZW50aXR5IFNlcnZlclwiKTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2V0SWRTZXJ2ZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyBXaGV0aGVyIG9yIG5vdCB0aGUgSUQgc2VydmVyIGlzIG1pc3NpbmcgdGVybXMuIFRoaXMgYWZmZWN0cyB0aGUgdGV4dFxyXG4gICAgICAgIC8vIHNob3duIHRvIHRoZSB1c2VyLlxyXG4gICAgICAgIG1pc3NpbmdUZXJtczogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIGxldCBkZWZhdWx0SWRTZXJ2ZXIgPSAnJztcclxuICAgICAgICBpZiAoIU1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRJZGVudGl0eVNlcnZlclVybCgpICYmIGdldERlZmF1bHRJZGVudGl0eVNlcnZlclVybCgpKSB7XHJcbiAgICAgICAgICAgIC8vIElmIG5vIElEIHNlcnZlciBpcyBjb25maWd1cmVkIGJ1dCB0aGVyZSdzIG9uZSBpbiB0aGUgY29uZmlnLCBwcmVwb3B1bGF0ZVxyXG4gICAgICAgICAgICAvLyB0aGUgZmllbGQgdG8gaGVscCB0aGUgdXNlci5cclxuICAgICAgICAgICAgZGVmYXVsdElkU2VydmVyID0gYWJicmV2aWF0ZVVybChnZXREZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBkZWZhdWx0SWRTZXJ2ZXIsXHJcbiAgICAgICAgICAgIGN1cnJlbnRDbGllbnRJZFNlcnZlcjogTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCksXHJcbiAgICAgICAgICAgIGlkU2VydmVyOiBcIlwiLFxyXG4gICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgIGRpc2Nvbm5lY3RCdXN5OiBmYWxzZSxcclxuICAgICAgICAgICAgY2hlY2tpbmc6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCk6IHZvaWQge1xyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgb25BY3Rpb24gPSAocGF5bG9hZCkgPT4ge1xyXG4gICAgICAgIC8vIFdlIHJlYWN0IHRvIGNoYW5nZXMgaW4gdGhlIElEIHNlcnZlciBpbiB0aGUgZXZlbnQgdGhlIHVzZXIgaXMgc3RhcmluZyBhdCB0aGlzIGZvcm1cclxuICAgICAgICAvLyB3aGVuIGNoYW5naW5nIHRoZWlyIGlkZW50aXR5IHNlcnZlciBvbiBhbm90aGVyIGRldmljZS5cclxuICAgICAgICBpZiAocGF5bG9hZC5hY3Rpb24gIT09IFwiaWRfc2VydmVyX2NoYW5nZWRcIikgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgY3VycmVudENsaWVudElkU2VydmVyOiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwoKSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uSWRlbnRpdHlTZXJ2ZXJDaGFuZ2VkID0gKGV2KSA9PiB7XHJcbiAgICAgICAgY29uc3QgdSA9IGV2LnRhcmdldC52YWx1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aWRTZXJ2ZXI6IHV9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2dldFRvb2x0aXAgPSAoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY2hlY2tpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgSW5saW5lU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLklubGluZVNwaW5uZXInKTtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8SW5saW5lU3Bpbm5lciAvPlxyXG4gICAgICAgICAgICAgICAgeyBfdChcIkNoZWNraW5nIHNlcnZlclwiKSB9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuZXJyb3IpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxzcGFuIGNsYXNzTmFtZT0nd2FybmluZyc+e3RoaXMuc3RhdGUuZXJyb3J9PC9zcGFuPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9pZFNlcnZlckNoYW5nZUVuYWJsZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICEhdGhpcy5zdGF0ZS5pZFNlcnZlciAmJiAhdGhpcy5zdGF0ZS5idXN5O1xyXG4gICAgfTtcclxuXHJcbiAgICBfc2F2ZUlkU2VydmVyID0gKGZ1bGxVcmwpID0+IHtcclxuICAgICAgICAvLyBBY2NvdW50IGRhdGEgY2hhbmdlIHdpbGwgdXBkYXRlIGxvY2Fsc3RvcmFnZSwgY2xpZW50LCBldGMgdGhyb3VnaCBkaXNwYXRjaGVyXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldEFjY291bnREYXRhKFwibS5pZGVudGl0eV9zZXJ2ZXJcIiwge1xyXG4gICAgICAgICAgICBiYXNlX3VybDogZnVsbFVybCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBjdXJyZW50Q2xpZW50SWRTZXJ2ZXI6IGZ1bGxVcmwsXHJcbiAgICAgICAgICAgIGlkU2VydmVyOiAnJyxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NoZWNrSWRTZXJ2ZXIgPSBhc3luYyAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCB7IGlkU2VydmVyLCBjdXJyZW50Q2xpZW50SWRTZXJ2ZXIgfSA9IHRoaXMuc3RhdGU7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IHRydWUsIGNoZWNraW5nOiB0cnVlLCBlcnJvcjogbnVsbH0pO1xyXG5cclxuICAgICAgICBjb25zdCBmdWxsVXJsID0gdW5hYmJyZXZpYXRlVXJsKGlkU2VydmVyKTtcclxuXHJcbiAgICAgICAgbGV0IGVyclN0ciA9IGF3YWl0IGNoZWNrSWRlbnRpdHlTZXJ2ZXJVcmwoZnVsbFVybCk7XHJcbiAgICAgICAgaWYgKCFlcnJTdHIpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2NoZWNraW5nOiBmYWxzZX0pOyAvLyBjbGVhciB0b29sdGlwXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gVGVzdCB0aGUgaWRlbnRpdHkgc2VydmVyIGJ5IHRyeWluZyB0byByZWdpc3RlciB3aXRoIGl0LiBUaGlzXHJcbiAgICAgICAgICAgICAgICAvLyBtYXkgcmVzdWx0IGluIGEgdGVybXMgb2Ygc2VydmljZSBwcm9tcHQuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBhdXRoQ2xpZW50ID0gbmV3IElkZW50aXR5QXV0aENsaWVudChmdWxsVXJsKTtcclxuICAgICAgICAgICAgICAgIGF3YWl0IGF1dGhDbGllbnQuZ2V0QWNjZXNzVG9rZW4oKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgc2F2ZSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gRG91YmxlIGNoZWNrIHRoYXQgdGhlIGlkZW50aXR5IHNlcnZlciBldmVuIGhhcyB0ZXJtcyBvZiBzZXJ2aWNlLlxyXG4gICAgICAgICAgICAgICAgY29uc3QgaGFzVGVybXMgPSBhd2FpdCBkb2VzSWRlbnRpdHlTZXJ2ZXJIYXZlVGVybXMoZnVsbFVybCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWhhc1Rlcm1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgW2NvbmZpcm1lZF0gPSBhd2FpdCB0aGlzLl9zaG93Tm9UZXJtc1dhcm5pbmcoZnVsbFVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2F2ZSA9IGNvbmZpcm1lZDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBTaG93IGEgZ2VuZXJhbCB3YXJuaW5nLCBwb3NzaWJseSB3aXRoIGRldGFpbHMgYWJvdXQgYW55IGJvdW5kXHJcbiAgICAgICAgICAgICAgICAvLyAzUElEcyB0aGF0IHdvdWxkIGJlIGxlZnQgYmVoaW5kLlxyXG4gICAgICAgICAgICAgICAgaWYgKHNhdmUgJiYgY3VycmVudENsaWVudElkU2VydmVyICYmIGZ1bGxVcmwgIT09IGN1cnJlbnRDbGllbnRJZFNlcnZlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IFtjb25maXJtZWRdID0gYXdhaXQgdGhpcy5fc2hvd1NlcnZlckNoYW5nZVdhcm5pbmcoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJDaGFuZ2UgaWRlbnRpdHkgc2VydmVyXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1bmJvdW5kTWVzc2FnZTogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkRpc2Nvbm5lY3QgZnJvbSB0aGUgaWRlbnRpdHkgc2VydmVyIDxjdXJyZW50IC8+IGFuZCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbm5lY3QgdG8gPG5ldyAvPiBpbnN0ZWFkP1wiLCB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50OiBzdWIgPT4gPGI+e2FiYnJldmlhdGVVcmwoY3VycmVudENsaWVudElkU2VydmVyKX08L2I+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ldzogc3ViID0+IDxiPnthYmJyZXZpYXRlVXJsKGlkU2VydmVyKX08L2I+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnV0dG9uOiBfdChcIkNvbnRpbnVlXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHNhdmUgPSBjb25maXJtZWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHNhdmUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zYXZlSWRTZXJ2ZXIoZnVsbFVybCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgICAgICBlcnJTdHIgPSBfdChcIlRlcm1zIG9mIHNlcnZpY2Ugbm90IGFjY2VwdGVkIG9yIHRoZSBpZGVudGl0eSBzZXJ2ZXIgaXMgaW52YWxpZC5cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgICAgICBjaGVja2luZzogZmFsc2UsXHJcbiAgICAgICAgICAgIGVycm9yOiBlcnJTdHIsXHJcbiAgICAgICAgICAgIGN1cnJlbnRDbGllbnRJZFNlcnZlcjogTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9zaG93Tm9UZXJtc1dhcm5pbmcoZnVsbFVybCkge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3QgeyBmaW5pc2hlZCB9ID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTm8gVGVybXMgV2FybmluZycsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoXCJJZGVudGl0eSBzZXJ2ZXIgaGFzIG5vIHRlcm1zIG9mIHNlcnZpY2VcIiksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIndhcm5pbmdcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiVGhlIGlkZW50aXR5IHNlcnZlciB5b3UgaGF2ZSBjaG9zZW4gZG9lcyBub3QgaGF2ZSBhbnkgdGVybXMgb2Ygc2VydmljZS5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmbmJzcDt7X3QoXCJPbmx5IGNvbnRpbnVlIGlmIHlvdSB0cnVzdCB0aGUgb3duZXIgb2YgdGhlIHNlcnZlci5cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJDb250aW51ZVwiKSxcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZmluaXNoZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgX29uRGlzY29ubmVjdENsaWNrZWQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGlzY29ubmVjdEJ1c3k6IHRydWV9KTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBbY29uZmlybWVkXSA9IGF3YWl0IHRoaXMuX3Nob3dTZXJ2ZXJDaGFuZ2VXYXJuaW5nKHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkRpc2Nvbm5lY3QgaWRlbnRpdHkgc2VydmVyXCIpLFxyXG4gICAgICAgICAgICAgICAgdW5ib3VuZE1lc3NhZ2U6IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiRGlzY29ubmVjdCBmcm9tIHRoZSBpZGVudGl0eSBzZXJ2ZXIgPGlkc2VydmVyIC8+P1wiLCB7fSxcclxuICAgICAgICAgICAgICAgICAgICB7aWRzZXJ2ZXI6IHN1YiA9PiA8Yj57YWJicmV2aWF0ZVVybCh0aGlzLnN0YXRlLmN1cnJlbnRDbGllbnRJZFNlcnZlcil9PC9iPn0sXHJcbiAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgYnV0dG9uOiBfdChcIkRpc2Nvbm5lY3RcIiksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaXNjb25uZWN0SWRTZXJ2ZXIoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2Rpc2Nvbm5lY3RCdXN5OiBmYWxzZX0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgYXN5bmMgX3Nob3dTZXJ2ZXJDaGFuZ2VXYXJuaW5nKHsgdGl0bGUsIHVuYm91bmRNZXNzYWdlLCBidXR0b24gfSkge1xyXG4gICAgICAgIGNvbnN0IHsgY3VycmVudENsaWVudElkU2VydmVyIH0gPSB0aGlzLnN0YXRlO1xyXG5cclxuICAgICAgICBsZXQgdGhyZWVwaWRzID0gW107XHJcbiAgICAgICAgbGV0IGN1cnJlbnRTZXJ2ZXJSZWFjaGFibGUgPSB0cnVlO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHRocmVlcGlkcyA9IGF3YWl0IHRpbWVvdXQoXHJcbiAgICAgICAgICAgICAgICBnZXRUaHJlZXBpZHNXaXRoQmluZFN0YXR1cyhNYXRyaXhDbGllbnRQZWcuZ2V0KCkpLFxyXG4gICAgICAgICAgICAgICAgUHJvbWlzZS5yZWplY3QobmV3IEVycm9yKFwiVGltZW91dCBhdHRlbXB0aW5nIHRvIHJlYWNoIGlkZW50aXR5IHNlcnZlclwiKSksXHJcbiAgICAgICAgICAgICAgICBSRUFDSEFCSUxJVFlfVElNRU9VVCxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnRTZXJ2ZXJSZWFjaGFibGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFxyXG4gICAgICAgICAgICAgICAgYFVuYWJsZSB0byByZWFjaCBpZGVudGl0eSBzZXJ2ZXIgYXQgJHtjdXJyZW50Q2xpZW50SWRTZXJ2ZXJ9IHRvIGNoZWNrIGAgK1xyXG4gICAgICAgICAgICAgICAgYGZvciAzUElEcyBkdXJpbmcgSVMgY2hhbmdlIGZsb3dgLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGJvdW5kVGhyZWVwaWRzID0gdGhyZWVwaWRzLmZpbHRlcih0cCA9PiB0cC5ib3VuZCk7XHJcbiAgICAgICAgbGV0IG1lc3NhZ2U7XHJcbiAgICAgICAgbGV0IGRhbmdlciA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IG1lc3NhZ2VFbGVtZW50cyA9IHtcclxuICAgICAgICAgICAgaWRzZXJ2ZXI6IHN1YiA9PiA8Yj57YWJicmV2aWF0ZVVybChjdXJyZW50Q2xpZW50SWRTZXJ2ZXIpfTwvYj4sXHJcbiAgICAgICAgICAgIGI6IHN1YiA9PiA8Yj57c3VifTwvYj4sXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAoIWN1cnJlbnRTZXJ2ZXJSZWFjaGFibGUpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJZb3Ugc2hvdWxkIDxiPnJlbW92ZSB5b3VyIHBlcnNvbmFsIGRhdGE8L2I+IGZyb20gaWRlbnRpdHkgc2VydmVyIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxpZHNlcnZlciAvPiBiZWZvcmUgZGlzY29ubmVjdGluZy4gVW5mb3J0dW5hdGVseSwgaWRlbnRpdHkgc2VydmVyIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxpZHNlcnZlciAvPiBpcyBjdXJyZW50bHkgb2ZmbGluZSBvciBjYW5ub3QgYmUgcmVhY2hlZC5cIixcclxuICAgICAgICAgICAgICAgICAgICB7fSwgbWVzc2FnZUVsZW1lbnRzLFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXCJZb3Ugc2hvdWxkOlwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJjaGVjayB5b3VyIGJyb3dzZXIgcGx1Z2lucyBmb3IgYW55dGhpbmcgdGhhdCBtaWdodCBibG9jayBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGhlIGlkZW50aXR5IHNlcnZlciAoc3VjaCBhcyBQcml2YWN5IEJhZGdlcilcIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPntfdChcImNvbnRhY3QgdGhlIGFkbWluaXN0cmF0b3JzIG9mIGlkZW50aXR5IHNlcnZlciA8aWRzZXJ2ZXIgLz5cIiwge30sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWRzZXJ2ZXI6IG1lc3NhZ2VFbGVtZW50cy5pZHNlcnZlcixcclxuICAgICAgICAgICAgICAgICAgICB9KX08L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT57X3QoXCJ3YWl0IGFuZCB0cnkgYWdhaW4gbGF0ZXJcIil9PC9saT5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgZGFuZ2VyID0gdHJ1ZTtcclxuICAgICAgICAgICAgYnV0dG9uID0gX3QoXCJEaXNjb25uZWN0IGFueXdheVwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGJvdW5kVGhyZWVwaWRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICBcIllvdSBhcmUgc3RpbGwgPGI+c2hhcmluZyB5b3VyIHBlcnNvbmFsIGRhdGE8L2I+IG9uIHRoZSBpZGVudGl0eSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzZXJ2ZXIgPGlkc2VydmVyIC8+LlwiLCB7fSwgbWVzc2FnZUVsZW1lbnRzLFxyXG4gICAgICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJXZSByZWNvbW1lbmQgdGhhdCB5b3UgcmVtb3ZlIHlvdXIgZW1haWwgYWRkcmVzc2VzIGFuZCBwaG9uZSBudW1iZXJzIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImZyb20gdGhlIGlkZW50aXR5IHNlcnZlciBiZWZvcmUgZGlzY29ubmVjdGluZy5cIixcclxuICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIGRhbmdlciA9IHRydWU7XHJcbiAgICAgICAgICAgIGJ1dHRvbiA9IF90KFwiRGlzY29ubmVjdCBhbnl3YXlcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IHVuYm91bmRNZXNzYWdlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCB7IGZpbmlzaGVkIH0gPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdJZGVudGl0eSBTZXJ2ZXIgQm91bmQgV2FybmluZycsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZSxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIGJ1dHRvbixcclxuICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiBfdChcIkdvIGJhY2tcIiksXHJcbiAgICAgICAgICAgIGRhbmdlcixcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZmluaXNoZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgX2Rpc2Nvbm5lY3RJZFNlcnZlciA9ICgpID0+IHtcclxuICAgICAgICAvLyBBY2NvdW50IGRhdGEgY2hhbmdlIHdpbGwgdXBkYXRlIGxvY2Fsc3RvcmFnZSwgY2xpZW50LCBldGMgdGhyb3VnaCBkaXNwYXRjaGVyXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldEFjY291bnREYXRhKFwibS5pZGVudGl0eV9zZXJ2ZXJcIiwge1xyXG4gICAgICAgICAgICBiYXNlX3VybDogbnVsbCwgLy8gY2xlYXJcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IG5ld0ZpZWxkVmFsID0gJyc7XHJcbiAgICAgICAgaWYgKGdldERlZmF1bHRJZGVudGl0eVNlcnZlclVybCgpKSB7XHJcbiAgICAgICAgICAgIC8vIFByZXBvcHVsYXRlIHRoZSBjbGllbnQncyBkZWZhdWx0IHNvIHRoZSB1c2VyIGF0IGxlYXN0IGhhcyBzb21lIGlkZWEgb2ZcclxuICAgICAgICAgICAgLy8gYSB2YWxpZCB2YWx1ZSB0aGV5IG1pZ2h0IGVudGVyXHJcbiAgICAgICAgICAgIG5ld0ZpZWxkVmFsID0gYWJicmV2aWF0ZVVybChnZXREZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgYnVzeTogZmFsc2UsXHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBjdXJyZW50Q2xpZW50SWRTZXJ2ZXI6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRJZGVudGl0eVNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICBpZFNlcnZlcjogbmV3RmllbGRWYWwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCBpZFNlcnZlclVybCA9IHRoaXMuc3RhdGUuY3VycmVudENsaWVudElkU2VydmVyO1xyXG4gICAgICAgIGxldCBzZWN0aW9uVGl0bGU7XHJcbiAgICAgICAgbGV0IGJvZHlUZXh0O1xyXG4gICAgICAgIGlmIChpZFNlcnZlclVybCkge1xyXG4gICAgICAgICAgICBzZWN0aW9uVGl0bGUgPSBfdChcIklkZW50aXR5IFNlcnZlciAoJShzZXJ2ZXIpcylcIiwgeyBzZXJ2ZXI6IGFiYnJldmlhdGVVcmwoaWRTZXJ2ZXJVcmwpIH0pO1xyXG4gICAgICAgICAgICBib2R5VGV4dCA9IF90KFxyXG4gICAgICAgICAgICAgICAgXCJZb3UgYXJlIGN1cnJlbnRseSB1c2luZyA8c2VydmVyPjwvc2VydmVyPiB0byBkaXNjb3ZlciBhbmQgYmUgZGlzY292ZXJhYmxlIGJ5IFwiICtcclxuICAgICAgICAgICAgICAgIFwiZXhpc3RpbmcgY29udGFjdHMgeW91IGtub3cuIFlvdSBjYW4gY2hhbmdlIHlvdXIgaWRlbnRpdHkgc2VydmVyIGJlbG93LlwiLFxyXG4gICAgICAgICAgICAgICAge30sXHJcbiAgICAgICAgICAgICAgICB7IHNlcnZlcjogc3ViID0+IDxiPnthYmJyZXZpYXRlVXJsKGlkU2VydmVyVXJsKX08L2I+IH0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm1pc3NpbmdUZXJtcykge1xyXG4gICAgICAgICAgICAgICAgYm9keVRleHQgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIklmIHlvdSBkb24ndCB3YW50IHRvIHVzZSA8c2VydmVyIC8+IHRvIGRpc2NvdmVyIGFuZCBiZSBkaXNjb3ZlcmFibGUgYnkgZXhpc3RpbmcgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiY29udGFjdHMgeW91IGtub3csIGVudGVyIGFub3RoZXIgaWRlbnRpdHkgc2VydmVyIGJlbG93LlwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHt9LCB7c2VydmVyOiBzdWIgPT4gPGI+e2FiYnJldmlhdGVVcmwoaWRTZXJ2ZXJVcmwpfTwvYj59LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNlY3Rpb25UaXRsZSA9IF90KFwiSWRlbnRpdHkgU2VydmVyXCIpO1xyXG4gICAgICAgICAgICBib2R5VGV4dCA9IF90KFxyXG4gICAgICAgICAgICAgICAgXCJZb3UgYXJlIG5vdCBjdXJyZW50bHkgdXNpbmcgYW4gaWRlbnRpdHkgc2VydmVyLiBcIiArXHJcbiAgICAgICAgICAgICAgICBcIlRvIGRpc2NvdmVyIGFuZCBiZSBkaXNjb3ZlcmFibGUgYnkgZXhpc3RpbmcgY29udGFjdHMgeW91IGtub3csIFwiICtcclxuICAgICAgICAgICAgICAgIFwiYWRkIG9uZSBiZWxvdy5cIixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBkaXNjb1NlY3Rpb247XHJcbiAgICAgICAgaWYgKGlkU2VydmVyVXJsKSB7XHJcbiAgICAgICAgICAgIGxldCBkaXNjb0J1dHRvbkNvbnRlbnQgPSBfdChcIkRpc2Nvbm5lY3RcIik7XHJcbiAgICAgICAgICAgIGxldCBkaXNjb0JvZHlUZXh0ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIkRpc2Nvbm5lY3RpbmcgZnJvbSB5b3VyIGlkZW50aXR5IHNlcnZlciB3aWxsIG1lYW4geW91IFwiICtcclxuICAgICAgICAgICAgICAgIFwid29uJ3QgYmUgZGlzY292ZXJhYmxlIGJ5IG90aGVyIHVzZXJzIGFuZCB5b3Ugd29uJ3QgYmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJhYmxlIHRvIGludml0ZSBvdGhlcnMgYnkgZW1haWwgb3IgcGhvbmUuXCIsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm1pc3NpbmdUZXJtcykge1xyXG4gICAgICAgICAgICAgICAgZGlzY29Cb2R5VGV4dCA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVXNpbmcgYW4gaWRlbnRpdHkgc2VydmVyIGlzIG9wdGlvbmFsLiBJZiB5b3UgY2hvb3NlIG5vdCB0byBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ1c2UgYW4gaWRlbnRpdHkgc2VydmVyLCB5b3Ugd29uJ3QgYmUgZGlzY292ZXJhYmxlIGJ5IG90aGVyIHVzZXJzIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImFuZCB5b3Ugd29uJ3QgYmUgYWJsZSB0byBpbnZpdGUgb3RoZXJzIGJ5IGVtYWlsIG9yIHBob25lLlwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIGRpc2NvQnV0dG9uQ29udGVudCA9IF90KFwiRG8gbm90IHVzZSBhbiBpZGVudGl0eSBzZXJ2ZXJcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGlzY29ubmVjdEJ1c3kpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IElubGluZVNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5JbmxpbmVTcGlubmVyJyk7XHJcbiAgICAgICAgICAgICAgICBkaXNjb0J1dHRvbkNvbnRlbnQgPSA8SW5saW5lU3Bpbm5lciAvPjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkaXNjb1NlY3Rpb24gPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHRcIj57ZGlzY29Cb2R5VGV4dH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkRpc2Nvbm5lY3RDbGlja2VkfSBraW5kPVwiZGFuZ2VyX3NtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge2Rpc2NvQnV0dG9uQ29udGVudH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGZvcm0gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc2VjdGlvbiBteF9TZXRJZFNlcnZlclwiIG9uU3VibWl0PXt0aGlzLl9jaGVja0lkU2VydmVyfT5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1NldHRpbmdzVGFiX3N1YmhlYWRpbmdcIj5cclxuICAgICAgICAgICAgICAgICAgICB7c2VjdGlvblRpdGxlfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7Ym9keVRleHR9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJFbnRlciBhIG5ldyBpZGVudGl0eSBzZXJ2ZXJcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGF1dG9Db21wbGV0ZT1cIm9mZlwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3RoaXMuc3RhdGUuZGVmYXVsdElkU2VydmVyfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLmlkU2VydmVyfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbklkZW50aXR5U2VydmVyQ2hhbmdlZH1cclxuICAgICAgICAgICAgICAgICAgICB0b29sdGlwQ29udGVudD17dGhpcy5fZ2V0VG9vbHRpcCgpfVxyXG4gICAgICAgICAgICAgICAgICAgIHRvb2x0aXBDbGFzc05hbWU9XCJteF9TZXRJZFNlcnZlcl90b29sdGlwXCJcclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5idXN5fVxyXG4gICAgICAgICAgICAgICAgICAgIGZsYWdJbnZhbGlkPXshIXRoaXMuc3RhdGUuZXJyb3J9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gdHlwZT1cInN1Ym1pdFwiIGtpbmQ9XCJwcmltYXJ5X3NtXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9jaGVja0lkU2VydmVyfVxyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshdGhpcy5faWRTZXJ2ZXJDaGFuZ2VFbmFibGVkKCl9XHJcbiAgICAgICAgICAgICAgICA+e190KFwiQ2hhbmdlXCIpfTwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIHtkaXNjb1NlY3Rpb259XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==