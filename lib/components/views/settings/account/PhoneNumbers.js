"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ExistingPhoneNumber = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../languageHandler");

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _Field = _interopRequireDefault(require("../../elements/Field"));

var _AccessibleButton = _interopRequireDefault(require("../../elements/AccessibleButton"));

var _AddThreepid = _interopRequireDefault(require("../../../../AddThreepid"));

var _CountryDropdown = _interopRequireDefault(require("../../auth/CountryDropdown"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _Modal = _interopRequireDefault(require("../../../../Modal"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
TODO: Improve the UX for everything in here.
This is a copy/paste of EmailAddresses, mostly.
 */
// TODO: Combine EmailAddresses and PhoneNumbers to be 3pid agnostic
class ExistingPhoneNumber extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onRemove", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        verifyRemove: true
      });
    });
    (0, _defineProperty2.default)(this, "_onDontRemove", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        verifyRemove: false
      });
    });
    (0, _defineProperty2.default)(this, "_onActuallyRemove", e => {
      e.stopPropagation();
      e.preventDefault();

      _MatrixClientPeg.MatrixClientPeg.get().deleteThreePid(this.props.msisdn.medium, this.props.msisdn.address).then(() => {
        return this.props.onRemoved(this.props.msisdn);
      }).catch(err => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Unable to remove contact information: " + err);

        _Modal.default.createTrackedDialog('Remove 3pid failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Unable to remove contact information"),
          description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
        });
      });
    });
    this.state = {
      verifyRemove: false
    };
  }

  render() {
    if (this.state.verifyRemove) {
      return _react.default.createElement("div", {
        className: "mx_ExistingPhoneNumber"
      }, _react.default.createElement("span", {
        className: "mx_ExistingPhoneNumber_promptText"
      }, (0, _languageHandler._t)("Remove %(phone)s?", {
        phone: this.props.msisdn.address
      })), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onActuallyRemove,
        kind: "danger_sm",
        className: "mx_ExistingPhoneNumber_confirmBtn"
      }, (0, _languageHandler._t)("Remove")), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onDontRemove,
        kind: "link_sm",
        className: "mx_ExistingPhoneNumber_confirmBtn"
      }, (0, _languageHandler._t)("Cancel")));
    }

    return _react.default.createElement("div", {
      className: "mx_ExistingPhoneNumber"
    }, _react.default.createElement("span", {
      className: "mx_ExistingPhoneNumber_address"
    }, "+", this.props.msisdn.address), _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onRemove,
      kind: "danger_sm"
    }, (0, _languageHandler._t)("Remove")));
  }

}

exports.ExistingPhoneNumber = ExistingPhoneNumber;
(0, _defineProperty2.default)(ExistingPhoneNumber, "propTypes", {
  msisdn: _propTypes.default.object.isRequired,
  onRemoved: _propTypes.default.func.isRequired
});

class PhoneNumbers extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onRemoved", address => {
      const msisdns = this.props.msisdns.filter(e => e !== address);
      this.props.onMsisdnsChange(msisdns);
    });
    (0, _defineProperty2.default)(this, "_onChangeNewPhoneNumber", e => {
      this.setState({
        newPhoneNumber: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onChangeNewPhoneNumberCode", e => {
      this.setState({
        newPhoneNumberCode: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onAddClick", e => {
      e.stopPropagation();
      e.preventDefault();
      if (!this.state.newPhoneNumber) return;
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      const phoneNumber = this.state.newPhoneNumber;
      const phoneCountry = this.state.phoneCountry;
      const task = new _AddThreepid.default();
      this.setState({
        verifying: true,
        continueDisabled: true,
        addTask: task
      });
      task.addMsisdn(phoneCountry, phoneNumber).then(response => {
        this.setState({
          continueDisabled: false,
          verifyMsisdn: response.msisdn
        });
      }).catch(err => {
        console.error("Unable to add phone number " + phoneNumber + " " + err);
        this.setState({
          verifying: false,
          continueDisabled: false,
          addTask: null
        });

        _Modal.default.createTrackedDialog('Add Phone Number Error', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Error"),
          description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onContinueClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        continueDisabled: true
      });
      const token = this.state.newPhoneNumberCode;
      const address = this.state.verifyMsisdn;
      this.state.addTask.haveMsisdnToken(token).then(() => {
        this.setState({
          addTask: null,
          continueDisabled: false,
          verifying: false,
          verifyMsisdn: "",
          verifyError: null,
          newPhoneNumber: "",
          newPhoneNumberCode: ""
        });
        const msisdns = [...this.props.msisdns, {
          address,
          medium: "msisdn"
        }];
        this.props.onMsisdnsChange(msisdns);
      }).catch(err => {
        this.setState({
          continueDisabled: false
        });

        if (err.errcode !== 'M_THREEPID_AUTH_FAILED') {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
          console.error("Unable to verify phone number: " + err);

          _Modal.default.createTrackedDialog('Unable to verify phone number', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Unable to verify phone number."),
            description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
          });
        } else {
          this.setState({
            verifyError: (0, _languageHandler._t)("Incorrect verification code")
          });
        }
      });
    });
    (0, _defineProperty2.default)(this, "_onCountryChanged", e => {
      this.setState({
        phoneCountry: e.iso2
      });
    });
    this.state = {
      verifying: false,
      verifyError: false,
      verifyMsisdn: "",
      addTask: null,
      continueDisabled: false,
      phoneCountry: "",
      newPhoneNumber: "",
      newPhoneNumberCode: ""
    };
  }

  render() {
    const existingPhoneElements = this.props.msisdns.map(p => {
      return _react.default.createElement(ExistingPhoneNumber, {
        msisdn: p,
        onRemoved: this._onRemoved,
        key: p.address
      });
    });

    let addVerifySection = _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onAddClick,
      kind: "primary"
    }, (0, _languageHandler._t)("Add"));

    if (this.state.verifying) {
      const msisdn = this.state.verifyMsisdn;
      addVerifySection = _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)("A text message has been sent to +%(msisdn)s. " + "Please enter the verification code it contains.", {
        msisdn: msisdn
      }), _react.default.createElement("br", null), this.state.verifyError), _react.default.createElement("form", {
        onSubmit: this._onContinueClick,
        autoComplete: "off",
        noValidate: true
      }, _react.default.createElement(_Field.default, {
        type: "text",
        label: (0, _languageHandler._t)("Verification code"),
        autoComplete: "off",
        disabled: this.state.continueDisabled,
        value: this.state.newPhoneNumberCode,
        onChange: this._onChangeNewPhoneNumberCode
      }), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onContinueClick,
        kind: "primary",
        disabled: this.state.continueDisabled
      }, (0, _languageHandler._t)("Continue"))));
    }

    const phoneCountry = _react.default.createElement(_CountryDropdown.default, {
      onOptionChange: this._onCountryChanged,
      className: "mx_PhoneNumbers_country",
      value: this.state.phoneCountry,
      disabled: this.state.verifying,
      isSmall: true,
      showPrefix: true
    });

    return _react.default.createElement("div", {
      className: "mx_PhoneNumbers"
    }, existingPhoneElements, _react.default.createElement("form", {
      onSubmit: this._onAddClick,
      autoComplete: "off",
      noValidate: true,
      className: "mx_PhoneNumbers_new"
    }, _react.default.createElement("div", {
      className: "mx_PhoneNumbers_input"
    }, _react.default.createElement(_Field.default, {
      type: "text",
      label: (0, _languageHandler._t)("Phone Number"),
      autoComplete: "off",
      disabled: this.state.verifying,
      prefix: phoneCountry,
      value: this.state.newPhoneNumber,
      onChange: this._onChangeNewPhoneNumber
    }))), addVerifySection);
  }

}

exports.default = PhoneNumbers;
(0, _defineProperty2.default)(PhoneNumbers, "propTypes", {
  msisdns: _propTypes.default.array.isRequired,
  onMsisdnsChange: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL2FjY291bnQvUGhvbmVOdW1iZXJzLmpzIl0sIm5hbWVzIjpbIkV4aXN0aW5nUGhvbmVOdW1iZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZSIsInN0b3BQcm9wYWdhdGlvbiIsInByZXZlbnREZWZhdWx0Iiwic2V0U3RhdGUiLCJ2ZXJpZnlSZW1vdmUiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJkZWxldGVUaHJlZVBpZCIsInByb3BzIiwibXNpc2RuIiwibWVkaXVtIiwiYWRkcmVzcyIsInRoZW4iLCJvblJlbW92ZWQiLCJjYXRjaCIsImVyciIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY29uc29sZSIsImVycm9yIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIm1lc3NhZ2UiLCJzdGF0ZSIsInJlbmRlciIsInBob25lIiwiX29uQWN0dWFsbHlSZW1vdmUiLCJfb25Eb250UmVtb3ZlIiwiX29uUmVtb3ZlIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJQaG9uZU51bWJlcnMiLCJtc2lzZG5zIiwiZmlsdGVyIiwib25Nc2lzZG5zQ2hhbmdlIiwibmV3UGhvbmVOdW1iZXIiLCJ0YXJnZXQiLCJ2YWx1ZSIsIm5ld1Bob25lTnVtYmVyQ29kZSIsInBob25lTnVtYmVyIiwicGhvbmVDb3VudHJ5IiwidGFzayIsIkFkZFRocmVlcGlkIiwidmVyaWZ5aW5nIiwiY29udGludWVEaXNhYmxlZCIsImFkZFRhc2siLCJhZGRNc2lzZG4iLCJyZXNwb25zZSIsInZlcmlmeU1zaXNkbiIsInRva2VuIiwiaGF2ZU1zaXNkblRva2VuIiwidmVyaWZ5RXJyb3IiLCJlcnJjb2RlIiwiaXNvMiIsImV4aXN0aW5nUGhvbmVFbGVtZW50cyIsIm1hcCIsInAiLCJfb25SZW1vdmVkIiwiYWRkVmVyaWZ5U2VjdGlvbiIsIl9vbkFkZENsaWNrIiwiX29uQ29udGludWVDbGljayIsIl9vbkNoYW5nZU5ld1Bob25lTnVtYmVyQ29kZSIsIl9vbkNvdW50cnlDaGFuZ2VkIiwiX29uQ2hhbmdlTmV3UGhvbmVOdW1iZXIiLCJhcnJheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBOzs7O0FBS0E7QUFFTyxNQUFNQSxtQkFBTixTQUFrQ0MsZUFBTUMsU0FBeEMsQ0FBa0Q7QUFNckRDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUscURBUURDLENBQUQsSUFBTztBQUNmQSxNQUFBQSxDQUFDLENBQUNDLGVBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxjQUFGO0FBRUEsV0FBS0MsUUFBTCxDQUFjO0FBQUNDLFFBQUFBLFlBQVksRUFBRTtBQUFmLE9BQWQ7QUFDSCxLQWJhO0FBQUEseURBZUdKLENBQUQsSUFBTztBQUNuQkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjtBQUVBLFdBQUtDLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxZQUFZLEVBQUU7QUFBZixPQUFkO0FBQ0gsS0FwQmE7QUFBQSw2REFzQk9KLENBQUQsSUFBTztBQUN2QkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjs7QUFFQUcsdUNBQWdCQyxHQUFoQixHQUFzQkMsY0FBdEIsQ0FBcUMsS0FBS0MsS0FBTCxDQUFXQyxNQUFYLENBQWtCQyxNQUF2RCxFQUErRCxLQUFLRixLQUFMLENBQVdDLE1BQVgsQ0FBa0JFLE9BQWpGLEVBQTBGQyxJQUExRixDQUErRixNQUFNO0FBQ2pHLGVBQU8sS0FBS0osS0FBTCxDQUFXSyxTQUFYLENBQXFCLEtBQUtMLEtBQUwsQ0FBV0MsTUFBaEMsQ0FBUDtBQUNILE9BRkQsRUFFR0ssS0FGSCxDQUVVQyxHQUFELElBQVM7QUFDZCxjQUFNQyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQUMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsMkNBQTJDTCxHQUF6RDs7QUFDQU0sdUJBQU1DLG1CQUFOLENBQTBCLG9CQUExQixFQUFnRCxFQUFoRCxFQUFvRE4sV0FBcEQsRUFBaUU7QUFDN0RPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxzQ0FBSCxDQURzRDtBQUU3REMsVUFBQUEsV0FBVyxFQUFJVCxHQUFHLElBQUlBLEdBQUcsQ0FBQ1UsT0FBWixHQUF1QlYsR0FBRyxDQUFDVSxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZVLFNBQWpFO0FBSUgsT0FURDtBQVVILEtBcENhO0FBR1YsU0FBS0MsS0FBTCxHQUFhO0FBQ1R0QixNQUFBQSxZQUFZLEVBQUU7QUFETCxLQUFiO0FBR0g7O0FBZ0NEdUIsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLRCxLQUFMLENBQVd0QixZQUFmLEVBQTZCO0FBQ3pCLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUNLLHlCQUFHLG1CQUFILEVBQXdCO0FBQUN3QixRQUFBQSxLQUFLLEVBQUUsS0FBS3BCLEtBQUwsQ0FBV0MsTUFBWCxDQUFrQkU7QUFBMUIsT0FBeEIsQ0FETCxDQURKLEVBSUksNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUUsS0FBS2tCLGlCQUFoQztBQUFtRCxRQUFBLElBQUksRUFBQyxXQUF4RDtBQUNrQixRQUFBLFNBQVMsRUFBQztBQUQ1QixTQUVLLHlCQUFHLFFBQUgsQ0FGTCxDQUpKLEVBUUksNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUUsS0FBS0MsYUFBaEM7QUFBK0MsUUFBQSxJQUFJLEVBQUMsU0FBcEQ7QUFDa0IsUUFBQSxTQUFTLEVBQUM7QUFENUIsU0FFSyx5QkFBRyxRQUFILENBRkwsQ0FSSixDQURKO0FBZUg7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLFlBQW1ELEtBQUt0QixLQUFMLENBQVdDLE1BQVgsQ0FBa0JFLE9BQXJFLENBREosRUFFSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRSxLQUFLb0IsU0FBaEM7QUFBMkMsTUFBQSxJQUFJLEVBQUM7QUFBaEQsT0FDSyx5QkFBRyxRQUFILENBREwsQ0FGSixDQURKO0FBUUg7O0FBdkVvRDs7OzhCQUE1Q25DLG1CLGVBQ1U7QUFDZmEsRUFBQUEsTUFBTSxFQUFFdUIsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRFY7QUFFZnJCLEVBQUFBLFNBQVMsRUFBRW1CLG1CQUFVRyxJQUFWLENBQWVEO0FBRlgsQzs7QUF5RVIsTUFBTUUsWUFBTixTQUEyQnZDLGVBQU1DLFNBQWpDLENBQTJDO0FBTXREQyxFQUFBQSxXQUFXLENBQUNTLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSxzREFlTEcsT0FBRCxJQUFhO0FBQ3RCLFlBQU0wQixPQUFPLEdBQUcsS0FBSzdCLEtBQUwsQ0FBVzZCLE9BQVgsQ0FBbUJDLE1BQW5CLENBQTJCdEMsQ0FBRCxJQUFPQSxDQUFDLEtBQUtXLE9BQXZDLENBQWhCO0FBQ0EsV0FBS0gsS0FBTCxDQUFXK0IsZUFBWCxDQUEyQkYsT0FBM0I7QUFDSCxLQWxCa0I7QUFBQSxtRUFvQlFyQyxDQUFELElBQU87QUFDN0IsV0FBS0csUUFBTCxDQUFjO0FBQ1ZxQyxRQUFBQSxjQUFjLEVBQUV4QyxDQUFDLENBQUN5QyxNQUFGLENBQVNDO0FBRGYsT0FBZDtBQUdILEtBeEJrQjtBQUFBLHVFQTBCWTFDLENBQUQsSUFBTztBQUNqQyxXQUFLRyxRQUFMLENBQWM7QUFDVndDLFFBQUFBLGtCQUFrQixFQUFFM0MsQ0FBQyxDQUFDeUMsTUFBRixDQUFTQztBQURuQixPQUFkO0FBR0gsS0E5QmtCO0FBQUEsdURBZ0NKMUMsQ0FBRCxJQUFPO0FBQ2pCQSxNQUFBQSxDQUFDLENBQUNDLGVBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxjQUFGO0FBRUEsVUFBSSxDQUFDLEtBQUt3QixLQUFMLENBQVdjLGNBQWhCLEVBQWdDO0FBRWhDLFlBQU14QixXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQSxZQUFNMEIsV0FBVyxHQUFHLEtBQUtsQixLQUFMLENBQVdjLGNBQS9CO0FBQ0EsWUFBTUssWUFBWSxHQUFHLEtBQUtuQixLQUFMLENBQVdtQixZQUFoQztBQUVBLFlBQU1DLElBQUksR0FBRyxJQUFJQyxvQkFBSixFQUFiO0FBQ0EsV0FBSzVDLFFBQUwsQ0FBYztBQUFDNkMsUUFBQUEsU0FBUyxFQUFFLElBQVo7QUFBa0JDLFFBQUFBLGdCQUFnQixFQUFFLElBQXBDO0FBQTBDQyxRQUFBQSxPQUFPLEVBQUVKO0FBQW5ELE9BQWQ7QUFFQUEsTUFBQUEsSUFBSSxDQUFDSyxTQUFMLENBQWVOLFlBQWYsRUFBNkJELFdBQTdCLEVBQTBDaEMsSUFBMUMsQ0FBZ0R3QyxRQUFELElBQWM7QUFDekQsYUFBS2pELFFBQUwsQ0FBYztBQUFDOEMsVUFBQUEsZ0JBQWdCLEVBQUUsS0FBbkI7QUFBMEJJLFVBQUFBLFlBQVksRUFBRUQsUUFBUSxDQUFDM0M7QUFBakQsU0FBZDtBQUNILE9BRkQsRUFFR0ssS0FGSCxDQUVVQyxHQUFELElBQVM7QUFDZEksUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsZ0NBQWdDd0IsV0FBaEMsR0FBOEMsR0FBOUMsR0FBb0Q3QixHQUFsRTtBQUNBLGFBQUtaLFFBQUwsQ0FBYztBQUFDNkMsVUFBQUEsU0FBUyxFQUFFLEtBQVo7QUFBbUJDLFVBQUFBLGdCQUFnQixFQUFFLEtBQXJDO0FBQTRDQyxVQUFBQSxPQUFPLEVBQUU7QUFBckQsU0FBZDs7QUFDQTdCLHVCQUFNQyxtQkFBTixDQUEwQix3QkFBMUIsRUFBb0QsRUFBcEQsRUFBd0ROLFdBQXhELEVBQXFFO0FBQ2pFTyxVQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUQwRDtBQUVqRUMsVUFBQUEsV0FBVyxFQUFJVCxHQUFHLElBQUlBLEdBQUcsQ0FBQ1UsT0FBWixHQUF1QlYsR0FBRyxDQUFDVSxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZjLFNBQXJFO0FBSUgsT0FURDtBQVVILEtBdkRrQjtBQUFBLDREQXlEQ3pCLENBQUQsSUFBTztBQUN0QkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjtBQUVBLFdBQUtDLFFBQUwsQ0FBYztBQUFDOEMsUUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsT0FBZDtBQUNBLFlBQU1LLEtBQUssR0FBRyxLQUFLNUIsS0FBTCxDQUFXaUIsa0JBQXpCO0FBQ0EsWUFBTWhDLE9BQU8sR0FBRyxLQUFLZSxLQUFMLENBQVcyQixZQUEzQjtBQUNBLFdBQUszQixLQUFMLENBQVd3QixPQUFYLENBQW1CSyxlQUFuQixDQUFtQ0QsS0FBbkMsRUFBMEMxQyxJQUExQyxDQUErQyxNQUFNO0FBQ2pELGFBQUtULFFBQUwsQ0FBYztBQUNWK0MsVUFBQUEsT0FBTyxFQUFFLElBREM7QUFFVkQsVUFBQUEsZ0JBQWdCLEVBQUUsS0FGUjtBQUdWRCxVQUFBQSxTQUFTLEVBQUUsS0FIRDtBQUlWSyxVQUFBQSxZQUFZLEVBQUUsRUFKSjtBQUtWRyxVQUFBQSxXQUFXLEVBQUUsSUFMSDtBQU1WaEIsVUFBQUEsY0FBYyxFQUFFLEVBTk47QUFPVkcsVUFBQUEsa0JBQWtCLEVBQUU7QUFQVixTQUFkO0FBU0EsY0FBTU4sT0FBTyxHQUFHLENBQ1osR0FBRyxLQUFLN0IsS0FBTCxDQUFXNkIsT0FERixFQUVaO0FBQUUxQixVQUFBQSxPQUFGO0FBQVdELFVBQUFBLE1BQU0sRUFBRTtBQUFuQixTQUZZLENBQWhCO0FBSUEsYUFBS0YsS0FBTCxDQUFXK0IsZUFBWCxDQUEyQkYsT0FBM0I7QUFDSCxPQWZELEVBZUd2QixLQWZILENBZVVDLEdBQUQsSUFBUztBQUNkLGFBQUtaLFFBQUwsQ0FBYztBQUFDOEMsVUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsU0FBZDs7QUFDQSxZQUFJbEMsR0FBRyxDQUFDMEMsT0FBSixLQUFnQix3QkFBcEIsRUFBOEM7QUFDMUMsZ0JBQU16QyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQUMsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsb0NBQW9DTCxHQUFsRDs7QUFDQU0seUJBQU1DLG1CQUFOLENBQTBCLCtCQUExQixFQUEyRCxFQUEzRCxFQUErRE4sV0FBL0QsRUFBNEU7QUFDeEVPLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxnQ0FBSCxDQURpRTtBQUV4RUMsWUFBQUEsV0FBVyxFQUFJVCxHQUFHLElBQUlBLEdBQUcsQ0FBQ1UsT0FBWixHQUF1QlYsR0FBRyxDQUFDVSxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZxQixXQUE1RTtBQUlILFNBUEQsTUFPTztBQUNILGVBQUt0QixRQUFMLENBQWM7QUFBQ3FELFlBQUFBLFdBQVcsRUFBRSx5QkFBRyw2QkFBSDtBQUFkLFdBQWQ7QUFDSDtBQUNKLE9BM0JEO0FBNEJILEtBNUZrQjtBQUFBLDZEQThGRXhELENBQUQsSUFBTztBQUN2QixXQUFLRyxRQUFMLENBQWM7QUFBQzBDLFFBQUFBLFlBQVksRUFBRTdDLENBQUMsQ0FBQzBEO0FBQWpCLE9BQWQ7QUFDSCxLQWhHa0I7QUFHZixTQUFLaEMsS0FBTCxHQUFhO0FBQ1RzQixNQUFBQSxTQUFTLEVBQUUsS0FERjtBQUVUUSxNQUFBQSxXQUFXLEVBQUUsS0FGSjtBQUdUSCxNQUFBQSxZQUFZLEVBQUUsRUFITDtBQUlUSCxNQUFBQSxPQUFPLEVBQUUsSUFKQTtBQUtURCxNQUFBQSxnQkFBZ0IsRUFBRSxLQUxUO0FBTVRKLE1BQUFBLFlBQVksRUFBRSxFQU5MO0FBT1RMLE1BQUFBLGNBQWMsRUFBRSxFQVBQO0FBUVRHLE1BQUFBLGtCQUFrQixFQUFFO0FBUlgsS0FBYjtBQVVIOztBQXFGRGhCLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1nQyxxQkFBcUIsR0FBRyxLQUFLbkQsS0FBTCxDQUFXNkIsT0FBWCxDQUFtQnVCLEdBQW5CLENBQXdCQyxDQUFELElBQU87QUFDeEQsYUFBTyw2QkFBQyxtQkFBRDtBQUFxQixRQUFBLE1BQU0sRUFBRUEsQ0FBN0I7QUFBZ0MsUUFBQSxTQUFTLEVBQUUsS0FBS0MsVUFBaEQ7QUFBNEQsUUFBQSxHQUFHLEVBQUVELENBQUMsQ0FBQ2xEO0FBQW5FLFFBQVA7QUFDSCxLQUY2QixDQUE5Qjs7QUFJQSxRQUFJb0QsZ0JBQWdCLEdBQ2hCLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFLEtBQUtDLFdBQWhDO0FBQTZDLE1BQUEsSUFBSSxFQUFDO0FBQWxELE9BQ0sseUJBQUcsS0FBSCxDQURMLENBREo7O0FBS0EsUUFBSSxLQUFLdEMsS0FBTCxDQUFXc0IsU0FBZixFQUEwQjtBQUN0QixZQUFNdkMsTUFBTSxHQUFHLEtBQUtpQixLQUFMLENBQVcyQixZQUExQjtBQUNBVSxNQUFBQSxnQkFBZ0IsR0FDWiwwQ0FDSSwwQ0FDSyx5QkFBRyxrREFDQSxpREFESCxFQUNzRDtBQUFFdEQsUUFBQUEsTUFBTSxFQUFFQTtBQUFWLE9BRHRELENBREwsRUFHSSx3Q0FISixFQUlLLEtBQUtpQixLQUFMLENBQVc4QixXQUpoQixDQURKLEVBT0k7QUFBTSxRQUFBLFFBQVEsRUFBRSxLQUFLUyxnQkFBckI7QUFBdUMsUUFBQSxZQUFZLEVBQUMsS0FBcEQ7QUFBMEQsUUFBQSxVQUFVLEVBQUU7QUFBdEUsU0FDSSw2QkFBQyxjQUFEO0FBQ0ksUUFBQSxJQUFJLEVBQUMsTUFEVDtBQUVJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLG1CQUFILENBRlg7QUFHSSxRQUFBLFlBQVksRUFBQyxLQUhqQjtBQUlJLFFBQUEsUUFBUSxFQUFFLEtBQUt2QyxLQUFMLENBQVd1QixnQkFKekI7QUFLSSxRQUFBLEtBQUssRUFBRSxLQUFLdkIsS0FBTCxDQUFXaUIsa0JBTHRCO0FBTUksUUFBQSxRQUFRLEVBQUUsS0FBS3VCO0FBTm5CLFFBREosRUFTSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLRCxnQkFBaEM7QUFBa0QsUUFBQSxJQUFJLEVBQUMsU0FBdkQ7QUFDa0IsUUFBQSxRQUFRLEVBQUUsS0FBS3ZDLEtBQUwsQ0FBV3VCO0FBRHZDLFNBRUsseUJBQUcsVUFBSCxDQUZMLENBVEosQ0FQSixDQURKO0FBd0JIOztBQUVELFVBQU1KLFlBQVksR0FBRyw2QkFBQyx3QkFBRDtBQUFpQixNQUFBLGNBQWMsRUFBRSxLQUFLc0IsaUJBQXRDO0FBQ2pCLE1BQUEsU0FBUyxFQUFDLHlCQURPO0FBRWpCLE1BQUEsS0FBSyxFQUFFLEtBQUt6QyxLQUFMLENBQVdtQixZQUZEO0FBR2pCLE1BQUEsUUFBUSxFQUFFLEtBQUtuQixLQUFMLENBQVdzQixTQUhKO0FBSWpCLE1BQUEsT0FBTyxFQUFFLElBSlE7QUFLakIsTUFBQSxVQUFVLEVBQUU7QUFMSyxNQUFyQjs7QUFRQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLVyxxQkFETCxFQUVJO0FBQU0sTUFBQSxRQUFRLEVBQUUsS0FBS0ssV0FBckI7QUFBa0MsTUFBQSxZQUFZLEVBQUMsS0FBL0M7QUFBcUQsTUFBQSxVQUFVLEVBQUUsSUFBakU7QUFBdUUsTUFBQSxTQUFTLEVBQUM7QUFBakYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSw2QkFBQyxjQUFEO0FBQ0ksTUFBQSxJQUFJLEVBQUMsTUFEVDtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGNBQUgsQ0FGWDtBQUdJLE1BQUEsWUFBWSxFQUFDLEtBSGpCO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS3RDLEtBQUwsQ0FBV3NCLFNBSnpCO0FBS0ksTUFBQSxNQUFNLEVBQUVILFlBTFo7QUFNSSxNQUFBLEtBQUssRUFBRSxLQUFLbkIsS0FBTCxDQUFXYyxjQU50QjtBQU9JLE1BQUEsUUFBUSxFQUFFLEtBQUs0QjtBQVBuQixNQURKLENBREosQ0FGSixFQWVLTCxnQkFmTCxDQURKO0FBbUJIOztBQXpLcUQ7Ozs4QkFBckMzQixZLGVBQ0U7QUFDZkMsRUFBQUEsT0FBTyxFQUFFTCxtQkFBVXFDLEtBQVYsQ0FBZ0JuQyxVQURWO0FBRWZLLEVBQUFBLGVBQWUsRUFBRVAsbUJBQVVHLElBQVYsQ0FBZUQ7QUFGakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgRmllbGQgZnJvbSBcIi4uLy4uL2VsZW1lbnRzL0ZpZWxkXCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi8uLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCBBZGRUaHJlZXBpZCBmcm9tIFwiLi4vLi4vLi4vLi4vQWRkVGhyZWVwaWRcIjtcclxuaW1wb3J0IENvdW50cnlEcm9wZG93biBmcm9tIFwiLi4vLi4vYXV0aC9Db3VudHJ5RHJvcGRvd25cIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uLy4uL01vZGFsJztcclxuXHJcbi8qXHJcblRPRE86IEltcHJvdmUgdGhlIFVYIGZvciBldmVyeXRoaW5nIGluIGhlcmUuXHJcblRoaXMgaXMgYSBjb3B5L3Bhc3RlIG9mIEVtYWlsQWRkcmVzc2VzLCBtb3N0bHkuXHJcbiAqL1xyXG5cclxuLy8gVE9ETzogQ29tYmluZSBFbWFpbEFkZHJlc3NlcyBhbmQgUGhvbmVOdW1iZXJzIHRvIGJlIDNwaWQgYWdub3N0aWNcclxuXHJcbmV4cG9ydCBjbGFzcyBFeGlzdGluZ1Bob25lTnVtYmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgbXNpc2RuOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25SZW1vdmVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICB2ZXJpZnlSZW1vdmU6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVtb3ZlID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmVyaWZ5UmVtb3ZlOiB0cnVlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkRvbnRSZW1vdmUgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHt2ZXJpZnlSZW1vdmU6IGZhbHNlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkFjdHVhbGx5UmVtb3ZlID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRlbGV0ZVRocmVlUGlkKHRoaXMucHJvcHMubXNpc2RuLm1lZGl1bSwgdGhpcy5wcm9wcy5tc2lzZG4uYWRkcmVzcykudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BzLm9uUmVtb3ZlZCh0aGlzLnByb3BzLm1zaXNkbik7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiVW5hYmxlIHRvIHJlbW92ZSBjb250YWN0IGluZm9ybWF0aW9uOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlbW92ZSAzcGlkIGZhaWxlZCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiVW5hYmxlIHRvIHJlbW92ZSBjb250YWN0IGluZm9ybWF0aW9uXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogX3QoXCJPcGVyYXRpb24gZmFpbGVkXCIpKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS52ZXJpZnlSZW1vdmUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X0V4aXN0aW5nUGhvbmVOdW1iZXJfcHJvbXB0VGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJSZW1vdmUgJShwaG9uZSlzP1wiLCB7cGhvbmU6IHRoaXMucHJvcHMubXNpc2RuLmFkZHJlc3N9KX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25BY3R1YWxseVJlbW92ZX0ga2luZD1cImRhbmdlcl9zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlcl9jb25maXJtQnRuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlJlbW92ZVwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25Eb250UmVtb3ZlfSBraW5kPVwibGlua19zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlcl9jb25maXJtQnRuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkNhbmNlbFwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlclwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdQaG9uZU51bWJlcl9hZGRyZXNzXCI+K3t0aGlzLnByb3BzLm1zaXNkbi5hZGRyZXNzfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uUmVtb3ZlfSBraW5kPVwiZGFuZ2VyX3NtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiUmVtb3ZlXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQaG9uZU51bWJlcnMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBtc2lzZG5zOiBQcm9wVHlwZXMuYXJyYXkuaXNSZXF1aXJlZCxcclxuICAgICAgICBvbk1zaXNkbnNDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHZlcmlmeWluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIHZlcmlmeUVycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgdmVyaWZ5TXNpc2RuOiBcIlwiLFxyXG4gICAgICAgICAgICBhZGRUYXNrOiBudWxsLFxyXG4gICAgICAgICAgICBjb250aW51ZURpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcGhvbmVDb3VudHJ5OiBcIlwiLFxyXG4gICAgICAgICAgICBuZXdQaG9uZU51bWJlcjogXCJcIixcclxuICAgICAgICAgICAgbmV3UGhvbmVOdW1iZXJDb2RlOiBcIlwiLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmVtb3ZlZCA9IChhZGRyZXNzKSA9PiB7XHJcbiAgICAgICAgY29uc3QgbXNpc2RucyA9IHRoaXMucHJvcHMubXNpc2Rucy5maWx0ZXIoKGUpID0+IGUgIT09IGFkZHJlc3MpO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25Nc2lzZG5zQ2hhbmdlKG1zaXNkbnMpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25DaGFuZ2VOZXdQaG9uZU51bWJlciA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIG5ld1Bob25lTnVtYmVyOiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQ2hhbmdlTmV3UGhvbmVOdW1iZXJDb2RlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbmV3UGhvbmVOdW1iZXJDb2RlOiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQWRkQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUubmV3UGhvbmVOdW1iZXIpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCBwaG9uZU51bWJlciA9IHRoaXMuc3RhdGUubmV3UGhvbmVOdW1iZXI7XHJcbiAgICAgICAgY29uc3QgcGhvbmVDb3VudHJ5ID0gdGhpcy5zdGF0ZS5waG9uZUNvdW50cnk7XHJcblxyXG4gICAgICAgIGNvbnN0IHRhc2sgPSBuZXcgQWRkVGhyZWVwaWQoKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHt2ZXJpZnlpbmc6IHRydWUsIGNvbnRpbnVlRGlzYWJsZWQ6IHRydWUsIGFkZFRhc2s6IHRhc2t9KTtcclxuXHJcbiAgICAgICAgdGFzay5hZGRNc2lzZG4ocGhvbmVDb3VudHJ5LCBwaG9uZU51bWJlcikudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y29udGludWVEaXNhYmxlZDogZmFsc2UsIHZlcmlmeU1zaXNkbjogcmVzcG9uc2UubXNpc2RufSk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiVW5hYmxlIHRvIGFkZCBwaG9uZSBudW1iZXIgXCIgKyBwaG9uZU51bWJlciArIFwiIFwiICsgZXJyKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmVyaWZ5aW5nOiBmYWxzZSwgY29udGludWVEaXNhYmxlZDogZmFsc2UsIGFkZFRhc2s6IG51bGx9KTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnQWRkIFBob25lIE51bWJlciBFcnJvcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQ29udGludWVDbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2NvbnRpbnVlRGlzYWJsZWQ6IHRydWV9KTtcclxuICAgICAgICBjb25zdCB0b2tlbiA9IHRoaXMuc3RhdGUubmV3UGhvbmVOdW1iZXJDb2RlO1xyXG4gICAgICAgIGNvbnN0IGFkZHJlc3MgPSB0aGlzLnN0YXRlLnZlcmlmeU1zaXNkbjtcclxuICAgICAgICB0aGlzLnN0YXRlLmFkZFRhc2suaGF2ZU1zaXNkblRva2VuKHRva2VuKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBhZGRUYXNrOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY29udGludWVEaXNhYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB2ZXJpZnlpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdmVyaWZ5TXNpc2RuOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgdmVyaWZ5RXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBuZXdQaG9uZU51bWJlcjogXCJcIixcclxuICAgICAgICAgICAgICAgIG5ld1Bob25lTnVtYmVyQ29kZTogXCJcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IG1zaXNkbnMgPSBbXHJcbiAgICAgICAgICAgICAgICAuLi50aGlzLnByb3BzLm1zaXNkbnMsXHJcbiAgICAgICAgICAgICAgICB7IGFkZHJlc3MsIG1lZGl1bTogXCJtc2lzZG5cIiB9LFxyXG4gICAgICAgICAgICBdO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uTXNpc2Ruc0NoYW5nZShtc2lzZG5zKTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2NvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgIGlmIChlcnIuZXJyY29kZSAhPT0gJ01fVEhSRUVQSURfQVVUSF9GQUlMRUQnKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIlVuYWJsZSB0byB2ZXJpZnkgcGhvbmUgbnVtYmVyOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdVbmFibGUgdG8gdmVyaWZ5IHBob25lIG51bWJlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlVuYWJsZSB0byB2ZXJpZnkgcGhvbmUgbnVtYmVyLlwiKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt2ZXJpZnlFcnJvcjogX3QoXCJJbmNvcnJlY3QgdmVyaWZpY2F0aW9uIGNvZGVcIil9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25Db3VudHJ5Q2hhbmdlZCA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cGhvbmVDb3VudHJ5OiBlLmlzbzJ9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGV4aXN0aW5nUGhvbmVFbGVtZW50cyA9IHRoaXMucHJvcHMubXNpc2Rucy5tYXAoKHApID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIDxFeGlzdGluZ1Bob25lTnVtYmVyIG1zaXNkbj17cH0gb25SZW1vdmVkPXt0aGlzLl9vblJlbW92ZWR9IGtleT17cC5hZGRyZXNzfSAvPjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGFkZFZlcmlmeVNlY3Rpb24gPSAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uQWRkQ2xpY2t9IGtpbmQ9XCJwcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICB7X3QoXCJBZGRcIil9XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICApO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnZlcmlmeWluZykge1xyXG4gICAgICAgICAgICBjb25zdCBtc2lzZG4gPSB0aGlzLnN0YXRlLnZlcmlmeU1zaXNkbjtcclxuICAgICAgICAgICAgYWRkVmVyaWZ5U2VjdGlvbiA9IChcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQSB0ZXh0IG1lc3NhZ2UgaGFzIGJlZW4gc2VudCB0byArJShtc2lzZG4pcy4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJQbGVhc2UgZW50ZXIgdGhlIHZlcmlmaWNhdGlvbiBjb2RlIGl0IGNvbnRhaW5zLlwiLCB7IG1zaXNkbjogbXNpc2RuIH0pfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUudmVyaWZ5RXJyb3J9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9e3RoaXMuX29uQ29udGludWVDbGlja30gYXV0b0NvbXBsZXRlPVwib2ZmXCIgbm9WYWxpZGF0ZT17dHJ1ZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KFwiVmVyaWZpY2F0aW9uIGNvZGVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUuY29udGludWVEaXNhYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm5ld1Bob25lTnVtYmVyQ29kZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkNoYW5nZU5ld1Bob25lTnVtYmVyQ29kZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25Db250aW51ZUNsaWNrfSBraW5kPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLmNvbnRpbnVlRGlzYWJsZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQ29udGludWVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBob25lQ291bnRyeSA9IDxDb3VudHJ5RHJvcGRvd24gb25PcHRpb25DaGFuZ2U9e3RoaXMuX29uQ291bnRyeUNoYW5nZWR9XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Bob25lTnVtYmVyc19jb3VudHJ5XCJcclxuICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGhvbmVDb3VudHJ5fVxyXG4gICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS52ZXJpZnlpbmd9XHJcbiAgICAgICAgICAgIGlzU21hbGw9e3RydWV9XHJcbiAgICAgICAgICAgIHNob3dQcmVmaXg9e3RydWV9XHJcbiAgICAgICAgLz47XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUGhvbmVOdW1iZXJzXCI+XHJcbiAgICAgICAgICAgICAgICB7ZXhpc3RpbmdQaG9uZUVsZW1lbnRzfVxyXG4gICAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9e3RoaXMuX29uQWRkQ2xpY2t9IGF1dG9Db21wbGV0ZT1cIm9mZlwiIG5vVmFsaWRhdGU9e3RydWV9IGNsYXNzTmFtZT1cIm14X1Bob25lTnVtYmVyc19uZXdcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Bob25lTnVtYmVyc19pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdChcIlBob25lIE51bWJlclwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Db21wbGV0ZT1cIm9mZlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS52ZXJpZnlpbmd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXg9e3Bob25lQ291bnRyeX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm5ld1Bob25lTnVtYmVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlTmV3UGhvbmVOdW1iZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICB7YWRkVmVyaWZ5U2VjdGlvbn1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=