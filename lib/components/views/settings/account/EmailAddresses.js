"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ExistingEmailAddress = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../../languageHandler");

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _Field = _interopRequireDefault(require("../../elements/Field"));

var _AccessibleButton = _interopRequireDefault(require("../../elements/AccessibleButton"));

var Email = _interopRequireWildcard(require("../../../../email"));

var _AddThreepid = _interopRequireDefault(require("../../../../AddThreepid"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _Modal = _interopRequireDefault(require("../../../../Modal"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
TODO: Improve the UX for everything in here.
It's very much placeholder, but it gets the job done. The old way of handling
email addresses in user settings was to use dialogs to communicate state, however
due to our dialog system overriding dialogs (causing unmounts) this creates problems
for a sane UX. For instance, the user could easily end up entering an email address
and receive a dialog to verify the address, which then causes the component here
to forget what it was doing and ultimately fail. Dialogs are still used in some
places to communicate errors - these should be replaced with inline validation when
that is available.
 */
class ExistingEmailAddress extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onRemove", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        verifyRemove: true
      });
    });
    (0, _defineProperty2.default)(this, "_onDontRemove", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        verifyRemove: false
      });
    });
    (0, _defineProperty2.default)(this, "_onActuallyRemove", e => {
      e.stopPropagation();
      e.preventDefault();

      _MatrixClientPeg.MatrixClientPeg.get().deleteThreePid(this.props.email.medium, this.props.email.address).then(() => {
        return this.props.onRemoved(this.props.email);
      }).catch(err => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Unable to remove contact information: " + err);

        _Modal.default.createTrackedDialog('Remove 3pid failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Unable to remove contact information"),
          description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
        });
      });
    });
    this.state = {
      verifyRemove: false
    };
  }

  render() {
    if (this.state.verifyRemove) {
      return _react.default.createElement("div", {
        className: "mx_ExistingEmailAddress"
      }, _react.default.createElement("span", {
        className: "mx_ExistingEmailAddress_promptText"
      }, (0, _languageHandler._t)("Remove %(email)s?", {
        email: this.props.email.address
      })), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onActuallyRemove,
        kind: "danger_sm",
        className: "mx_ExistingEmailAddress_confirmBtn"
      }, (0, _languageHandler._t)("Remove")), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onDontRemove,
        kind: "link_sm",
        className: "mx_ExistingEmailAddress_confirmBtn"
      }, (0, _languageHandler._t)("Cancel")));
    }

    return _react.default.createElement("div", {
      className: "mx_ExistingEmailAddress"
    }, _react.default.createElement("span", {
      className: "mx_ExistingEmailAddress_email"
    }, this.props.email.address), _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onRemove,
      kind: "danger_sm"
    }, (0, _languageHandler._t)("Remove")));
  }

}

exports.ExistingEmailAddress = ExistingEmailAddress;
(0, _defineProperty2.default)(ExistingEmailAddress, "propTypes", {
  email: _propTypes.default.object.isRequired,
  onRemoved: _propTypes.default.func.isRequired
});

class EmailAddresses extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onRemoved", address => {
      const emails = this.props.emails.filter(e => e !== address);
      this.props.onEmailsChange(emails);
    });
    (0, _defineProperty2.default)(this, "_onChangeNewEmailAddress", e => {
      this.setState({
        newEmailAddress: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onAddClick", e => {
      e.stopPropagation();
      e.preventDefault();
      if (!this.state.newEmailAddress) return;
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      const email = this.state.newEmailAddress; // TODO: Inline field validation

      if (!Email.looksValid(email)) {
        _Modal.default.createTrackedDialog('Invalid email address', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Invalid Email Address"),
          description: (0, _languageHandler._t)("This doesn't appear to be a valid email address")
        });

        return;
      }

      const task = new _AddThreepid.default();
      this.setState({
        verifying: true,
        continueDisabled: true,
        addTask: task
      });
      task.addEmailAddress(email).then(() => {
        this.setState({
          continueDisabled: false
        });
      }).catch(err => {
        console.error("Unable to add email address " + email + " " + err);
        this.setState({
          verifying: false,
          continueDisabled: false,
          addTask: null
        });

        _Modal.default.createTrackedDialog('Unable to add email address', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Unable to add email address"),
          description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
        });
      });
    });
    (0, _defineProperty2.default)(this, "_onContinueClick", e => {
      e.stopPropagation();
      e.preventDefault();
      this.setState({
        continueDisabled: true
      });
      this.state.addTask.checkEmailLinkClicked().then(() => {
        const email = this.state.newEmailAddress;
        this.setState({
          addTask: null,
          continueDisabled: false,
          verifying: false,
          newEmailAddress: ""
        });
        const emails = [...this.props.emails, {
          address: email,
          medium: "email"
        }];
        this.props.onEmailsChange(emails);
      }).catch(err => {
        this.setState({
          continueDisabled: false
        });
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        if (err.errcode === 'M_THREEPID_AUTH_FAILED') {
          _Modal.default.createTrackedDialog("Email hasn't been verified yet", "", ErrorDialog, {
            title: (0, _languageHandler._t)("Your email address hasn't been verified yet"),
            description: (0, _languageHandler._t)("Click the link in the email you received to verify " + "and then click continue again.")
          });
        } else {
          console.error("Unable to verify email address: ", err);

          _Modal.default.createTrackedDialog('Unable to verify email address', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Unable to verify email address."),
            description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
          });
        }
      });
    });
    this.state = {
      verifying: false,
      addTask: null,
      continueDisabled: false,
      newEmailAddress: ""
    };
  }

  render() {
    const existingEmailElements = this.props.emails.map(e => {
      return _react.default.createElement(ExistingEmailAddress, {
        email: e,
        onRemoved: this._onRemoved,
        key: e.address
      });
    });

    let addButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onAddClick,
      kind: "primary"
    }, (0, _languageHandler._t)("Add"));

    if (this.state.verifying) {
      addButton = _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)("We've sent you an email to verify your address. Please follow the instructions there and then click the button below.")), _react.default.createElement(_AccessibleButton.default, {
        onClick: this._onContinueClick,
        kind: "primary",
        disabled: this.state.continueDisabled
      }, (0, _languageHandler._t)("Continue")));
    }

    return _react.default.createElement("div", {
      className: "mx_EmailAddresses"
    }, existingEmailElements, _react.default.createElement("form", {
      onSubmit: this._onAddClick,
      autoComplete: "off",
      noValidate: true,
      className: "mx_EmailAddresses_new"
    }, _react.default.createElement(_Field.default, {
      type: "text",
      label: (0, _languageHandler._t)("Email Address"),
      autoComplete: "off",
      disabled: this.state.verifying,
      value: this.state.newEmailAddress,
      onChange: this._onChangeNewEmailAddress
    }), addButton));
  }

}

exports.default = EmailAddresses;
(0, _defineProperty2.default)(EmailAddresses, "propTypes", {
  emails: _propTypes.default.array.isRequired,
  onEmailsChange: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3NldHRpbmdzL2FjY291bnQvRW1haWxBZGRyZXNzZXMuanMiXSwibmFtZXMiOlsiRXhpc3RpbmdFbWFpbEFkZHJlc3MiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZSIsInN0b3BQcm9wYWdhdGlvbiIsInByZXZlbnREZWZhdWx0Iiwic2V0U3RhdGUiLCJ2ZXJpZnlSZW1vdmUiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJkZWxldGVUaHJlZVBpZCIsInByb3BzIiwiZW1haWwiLCJtZWRpdW0iLCJhZGRyZXNzIiwidGhlbiIsIm9uUmVtb3ZlZCIsImNhdGNoIiwiZXJyIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjb25zb2xlIiwiZXJyb3IiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwibWVzc2FnZSIsInN0YXRlIiwicmVuZGVyIiwiX29uQWN0dWFsbHlSZW1vdmUiLCJfb25Eb250UmVtb3ZlIiwiX29uUmVtb3ZlIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJFbWFpbEFkZHJlc3NlcyIsImVtYWlscyIsImZpbHRlciIsIm9uRW1haWxzQ2hhbmdlIiwibmV3RW1haWxBZGRyZXNzIiwidGFyZ2V0IiwidmFsdWUiLCJFbWFpbCIsImxvb2tzVmFsaWQiLCJ0YXNrIiwiQWRkVGhyZWVwaWQiLCJ2ZXJpZnlpbmciLCJjb250aW51ZURpc2FibGVkIiwiYWRkVGFzayIsImFkZEVtYWlsQWRkcmVzcyIsImNoZWNrRW1haWxMaW5rQ2xpY2tlZCIsImVycmNvZGUiLCJleGlzdGluZ0VtYWlsRWxlbWVudHMiLCJtYXAiLCJfb25SZW1vdmVkIiwiYWRkQnV0dG9uIiwiX29uQWRkQ2xpY2siLCJfb25Db250aW51ZUNsaWNrIiwiX29uQ2hhbmdlTmV3RW1haWxBZGRyZXNzIiwiYXJyYXkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQTs7Ozs7Ozs7Ozs7QUFZTyxNQUFNQSxvQkFBTixTQUFtQ0MsZUFBTUMsU0FBekMsQ0FBbUQ7QUFNdERDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUscURBUURDLENBQUQsSUFBTztBQUNmQSxNQUFBQSxDQUFDLENBQUNDLGVBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxjQUFGO0FBRUEsV0FBS0MsUUFBTCxDQUFjO0FBQUNDLFFBQUFBLFlBQVksRUFBRTtBQUFmLE9BQWQ7QUFDSCxLQWJhO0FBQUEseURBZUdKLENBQUQsSUFBTztBQUNuQkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjtBQUVBLFdBQUtDLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxZQUFZLEVBQUU7QUFBZixPQUFkO0FBQ0gsS0FwQmE7QUFBQSw2REFzQk9KLENBQUQsSUFBTztBQUN2QkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjs7QUFFQUcsdUNBQWdCQyxHQUFoQixHQUFzQkMsY0FBdEIsQ0FBcUMsS0FBS0MsS0FBTCxDQUFXQyxLQUFYLENBQWlCQyxNQUF0RCxFQUE4RCxLQUFLRixLQUFMLENBQVdDLEtBQVgsQ0FBaUJFLE9BQS9FLEVBQXdGQyxJQUF4RixDQUE2RixNQUFNO0FBQy9GLGVBQU8sS0FBS0osS0FBTCxDQUFXSyxTQUFYLENBQXFCLEtBQUtMLEtBQUwsQ0FBV0MsS0FBaEMsQ0FBUDtBQUNILE9BRkQsRUFFR0ssS0FGSCxDQUVVQyxHQUFELElBQVM7QUFDZCxjQUFNQyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQUMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsMkNBQTJDTCxHQUF6RDs7QUFDQU0sdUJBQU1DLG1CQUFOLENBQTBCLG9CQUExQixFQUFnRCxFQUFoRCxFQUFvRE4sV0FBcEQsRUFBaUU7QUFDN0RPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxzQ0FBSCxDQURzRDtBQUU3REMsVUFBQUEsV0FBVyxFQUFJVCxHQUFHLElBQUlBLEdBQUcsQ0FBQ1UsT0FBWixHQUF1QlYsR0FBRyxDQUFDVSxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZVLFNBQWpFO0FBSUgsT0FURDtBQVVILEtBcENhO0FBR1YsU0FBS0MsS0FBTCxHQUFhO0FBQ1R0QixNQUFBQSxZQUFZLEVBQUU7QUFETCxLQUFiO0FBR0g7O0FBZ0NEdUIsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLRCxLQUFMLENBQVd0QixZQUFmLEVBQTZCO0FBQ3pCLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUNLLHlCQUFHLG1CQUFILEVBQXdCO0FBQUNLLFFBQUFBLEtBQUssRUFBRSxLQUFLRCxLQUFMLENBQVdDLEtBQVgsQ0FBaUJFO0FBQXpCLE9BQXhCLENBREwsQ0FESixFQUlJLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUtpQixpQkFBaEM7QUFBbUQsUUFBQSxJQUFJLEVBQUMsV0FBeEQ7QUFDa0IsUUFBQSxTQUFTLEVBQUM7QUFENUIsU0FFSyx5QkFBRyxRQUFILENBRkwsQ0FKSixFQVFJLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUtDLGFBQWhDO0FBQStDLFFBQUEsSUFBSSxFQUFDLFNBQXBEO0FBQ2tCLFFBQUEsU0FBUyxFQUFDO0FBRDVCLFNBRUsseUJBQUcsUUFBSCxDQUZMLENBUkosQ0FESjtBQWVIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUFpRCxLQUFLckIsS0FBTCxDQUFXQyxLQUFYLENBQWlCRSxPQUFsRSxDQURKLEVBRUksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxPQUFPLEVBQUUsS0FBS21CLFNBQWhDO0FBQTJDLE1BQUEsSUFBSSxFQUFDO0FBQWhELE9BQ0sseUJBQUcsUUFBSCxDQURMLENBRkosQ0FESjtBQVFIOztBQXZFcUQ7Ozs4QkFBN0NsQyxvQixlQUNVO0FBQ2ZhLEVBQUFBLEtBQUssRUFBRXNCLG1CQUFVQyxNQUFWLENBQWlCQyxVQURUO0FBRWZwQixFQUFBQSxTQUFTLEVBQUVrQixtQkFBVUcsSUFBVixDQUFlRDtBQUZYLEM7O0FBeUVSLE1BQU1FLGNBQU4sU0FBNkJ0QyxlQUFNQyxTQUFuQyxDQUE2QztBQU14REMsRUFBQUEsV0FBVyxDQUFDUyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsc0RBV0xHLE9BQUQsSUFBYTtBQUN0QixZQUFNeUIsTUFBTSxHQUFHLEtBQUs1QixLQUFMLENBQVc0QixNQUFYLENBQWtCQyxNQUFsQixDQUEwQnJDLENBQUQsSUFBT0EsQ0FBQyxLQUFLVyxPQUF0QyxDQUFmO0FBQ0EsV0FBS0gsS0FBTCxDQUFXOEIsY0FBWCxDQUEwQkYsTUFBMUI7QUFDSCxLQWRrQjtBQUFBLG9FQWdCU3BDLENBQUQsSUFBTztBQUM5QixXQUFLRyxRQUFMLENBQWM7QUFDVm9DLFFBQUFBLGVBQWUsRUFBRXZDLENBQUMsQ0FBQ3dDLE1BQUYsQ0FBU0M7QUFEaEIsT0FBZDtBQUdILEtBcEJrQjtBQUFBLHVEQXNCSnpDLENBQUQsSUFBTztBQUNqQkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjtBQUVBLFVBQUksQ0FBQyxLQUFLd0IsS0FBTCxDQUFXYSxlQUFoQixFQUFpQztBQUVqQyxZQUFNdkIsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EsWUFBTVQsS0FBSyxHQUFHLEtBQUtpQixLQUFMLENBQVdhLGVBQXpCLENBUGlCLENBU2pCOztBQUNBLFVBQUksQ0FBQ0csS0FBSyxDQUFDQyxVQUFOLENBQWlCbEMsS0FBakIsQ0FBTCxFQUE4QjtBQUMxQlksdUJBQU1DLG1CQUFOLENBQTBCLHVCQUExQixFQUFtRCxFQUFuRCxFQUF1RE4sV0FBdkQsRUFBb0U7QUFDaEVPLFVBQUFBLEtBQUssRUFBRSx5QkFBRyx1QkFBSCxDQUR5RDtBQUVoRUMsVUFBQUEsV0FBVyxFQUFFLHlCQUFHLGlEQUFIO0FBRm1ELFNBQXBFOztBQUlBO0FBQ0g7O0FBRUQsWUFBTW9CLElBQUksR0FBRyxJQUFJQyxvQkFBSixFQUFiO0FBQ0EsV0FBSzFDLFFBQUwsQ0FBYztBQUFDMkMsUUFBQUEsU0FBUyxFQUFFLElBQVo7QUFBa0JDLFFBQUFBLGdCQUFnQixFQUFFLElBQXBDO0FBQTBDQyxRQUFBQSxPQUFPLEVBQUVKO0FBQW5ELE9BQWQ7QUFFQUEsTUFBQUEsSUFBSSxDQUFDSyxlQUFMLENBQXFCeEMsS0FBckIsRUFBNEJHLElBQTVCLENBQWlDLE1BQU07QUFDbkMsYUFBS1QsUUFBTCxDQUFjO0FBQUM0QyxVQUFBQSxnQkFBZ0IsRUFBRTtBQUFuQixTQUFkO0FBQ0gsT0FGRCxFQUVHakMsS0FGSCxDQUVVQyxHQUFELElBQVM7QUFDZEksUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsaUNBQWlDWCxLQUFqQyxHQUF5QyxHQUF6QyxHQUErQ00sR0FBN0Q7QUFDQSxhQUFLWixRQUFMLENBQWM7QUFBQzJDLFVBQUFBLFNBQVMsRUFBRSxLQUFaO0FBQW1CQyxVQUFBQSxnQkFBZ0IsRUFBRSxLQUFyQztBQUE0Q0MsVUFBQUEsT0FBTyxFQUFFO0FBQXJELFNBQWQ7O0FBQ0EzQix1QkFBTUMsbUJBQU4sQ0FBMEIsNkJBQTFCLEVBQXlELEVBQXpELEVBQTZETixXQUE3RCxFQUEwRTtBQUN0RU8sVUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZCQUFILENBRCtEO0FBRXRFQyxVQUFBQSxXQUFXLEVBQUlULEdBQUcsSUFBSUEsR0FBRyxDQUFDVSxPQUFaLEdBQXVCVixHQUFHLENBQUNVLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRm1CLFNBQTFFO0FBSUgsT0FURDtBQVVILEtBckRrQjtBQUFBLDREQXVEQ3pCLENBQUQsSUFBTztBQUN0QkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELE1BQUFBLENBQUMsQ0FBQ0UsY0FBRjtBQUVBLFdBQUtDLFFBQUwsQ0FBYztBQUFDNEMsUUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsT0FBZDtBQUNBLFdBQUtyQixLQUFMLENBQVdzQixPQUFYLENBQW1CRSxxQkFBbkIsR0FBMkN0QyxJQUEzQyxDQUFnRCxNQUFNO0FBQ2xELGNBQU1ILEtBQUssR0FBRyxLQUFLaUIsS0FBTCxDQUFXYSxlQUF6QjtBQUNBLGFBQUtwQyxRQUFMLENBQWM7QUFDVjZDLFVBQUFBLE9BQU8sRUFBRSxJQURDO0FBRVZELFVBQUFBLGdCQUFnQixFQUFFLEtBRlI7QUFHVkQsVUFBQUEsU0FBUyxFQUFFLEtBSEQ7QUFJVlAsVUFBQUEsZUFBZSxFQUFFO0FBSlAsU0FBZDtBQU1BLGNBQU1ILE1BQU0sR0FBRyxDQUNYLEdBQUcsS0FBSzVCLEtBQUwsQ0FBVzRCLE1BREgsRUFFWDtBQUFFekIsVUFBQUEsT0FBTyxFQUFFRixLQUFYO0FBQWtCQyxVQUFBQSxNQUFNLEVBQUU7QUFBMUIsU0FGVyxDQUFmO0FBSUEsYUFBS0YsS0FBTCxDQUFXOEIsY0FBWCxDQUEwQkYsTUFBMUI7QUFDSCxPQWJELEVBYUd0QixLQWJILENBYVVDLEdBQUQsSUFBUztBQUNkLGFBQUtaLFFBQUwsQ0FBYztBQUFDNEMsVUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsU0FBZDtBQUNBLGNBQU0vQixXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0EsWUFBSUgsR0FBRyxDQUFDb0MsT0FBSixLQUFnQix3QkFBcEIsRUFBOEM7QUFDMUM5Qix5QkFBTUMsbUJBQU4sQ0FBMEIsZ0NBQTFCLEVBQTRELEVBQTVELEVBQWdFTixXQUFoRSxFQUE2RTtBQUN6RU8sWUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZDQUFILENBRGtFO0FBRXpFQyxZQUFBQSxXQUFXLEVBQUUseUJBQUcsd0RBQ1osZ0NBRFM7QUFGNEQsV0FBN0U7QUFLSCxTQU5ELE1BTU87QUFDSEwsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsa0NBQWQsRUFBa0RMLEdBQWxEOztBQUNBTSx5QkFBTUMsbUJBQU4sQ0FBMEIsZ0NBQTFCLEVBQTRELEVBQTVELEVBQWdFTixXQUFoRSxFQUE2RTtBQUN6RU8sWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGlDQUFILENBRGtFO0FBRXpFQyxZQUFBQSxXQUFXLEVBQUlULEdBQUcsSUFBSUEsR0FBRyxDQUFDVSxPQUFaLEdBQXVCVixHQUFHLENBQUNVLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRnNCLFdBQTdFO0FBSUg7QUFDSixPQTdCRDtBQThCSCxLQTFGa0I7QUFHZixTQUFLQyxLQUFMLEdBQWE7QUFDVG9CLE1BQUFBLFNBQVMsRUFBRSxLQURGO0FBRVRFLE1BQUFBLE9BQU8sRUFBRSxJQUZBO0FBR1RELE1BQUFBLGdCQUFnQixFQUFFLEtBSFQ7QUFJVFIsTUFBQUEsZUFBZSxFQUFFO0FBSlIsS0FBYjtBQU1IOztBQW1GRFosRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTXlCLHFCQUFxQixHQUFHLEtBQUs1QyxLQUFMLENBQVc0QixNQUFYLENBQWtCaUIsR0FBbEIsQ0FBdUJyRCxDQUFELElBQU87QUFDdkQsYUFBTyw2QkFBQyxvQkFBRDtBQUFzQixRQUFBLEtBQUssRUFBRUEsQ0FBN0I7QUFBZ0MsUUFBQSxTQUFTLEVBQUUsS0FBS3NELFVBQWhEO0FBQTRELFFBQUEsR0FBRyxFQUFFdEQsQ0FBQyxDQUFDVztBQUFuRSxRQUFQO0FBQ0gsS0FGNkIsQ0FBOUI7O0FBSUEsUUFBSTRDLFNBQVMsR0FDVCw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRSxLQUFLQyxXQUFoQztBQUE2QyxNQUFBLElBQUksRUFBQztBQUFsRCxPQUNLLHlCQUFHLEtBQUgsQ0FETCxDQURKOztBQUtBLFFBQUksS0FBSzlCLEtBQUwsQ0FBV29CLFNBQWYsRUFBMEI7QUFDdEJTLE1BQUFBLFNBQVMsR0FDUCwwQ0FDSSwwQ0FBTSx5QkFBRyx1SEFBSCxDQUFOLENBREosRUFFSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLRSxnQkFBaEM7QUFBa0QsUUFBQSxJQUFJLEVBQUMsU0FBdkQ7QUFDa0IsUUFBQSxRQUFRLEVBQUUsS0FBSy9CLEtBQUwsQ0FBV3FCO0FBRHZDLFNBRUsseUJBQUcsVUFBSCxDQUZMLENBRkosQ0FERjtBQVNIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0tLLHFCQURMLEVBRUk7QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLSSxXQUFyQjtBQUFrQyxNQUFBLFlBQVksRUFBQyxLQUEvQztBQUNNLE1BQUEsVUFBVSxFQUFFLElBRGxCO0FBQ3dCLE1BQUEsU0FBUyxFQUFDO0FBRGxDLE9BRUksNkJBQUMsY0FBRDtBQUNJLE1BQUEsSUFBSSxFQUFDLE1BRFQ7QUFFSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxlQUFILENBRlg7QUFHSSxNQUFBLFlBQVksRUFBQyxLQUhqQjtBQUlJLE1BQUEsUUFBUSxFQUFFLEtBQUs5QixLQUFMLENBQVdvQixTQUp6QjtBQUtJLE1BQUEsS0FBSyxFQUFFLEtBQUtwQixLQUFMLENBQVdhLGVBTHRCO0FBTUksTUFBQSxRQUFRLEVBQUUsS0FBS21CO0FBTm5CLE1BRkosRUFVS0gsU0FWTCxDQUZKLENBREo7QUFpQkg7O0FBekl1RDs7OzhCQUF2Q3BCLGMsZUFDRTtBQUNmQyxFQUFBQSxNQUFNLEVBQUVMLG1CQUFVNEIsS0FBVixDQUFnQjFCLFVBRFQ7QUFFZkssRUFBQUEsY0FBYyxFQUFFUCxtQkFBVUcsSUFBVixDQUFlRDtBQUZoQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBGaWVsZCBmcm9tIFwiLi4vLi4vZWxlbWVudHMvRmllbGRcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4uLy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuaW1wb3J0ICogYXMgRW1haWwgZnJvbSBcIi4uLy4uLy4uLy4uL2VtYWlsXCI7XHJcbmltcG9ydCBBZGRUaHJlZXBpZCBmcm9tIFwiLi4vLi4vLi4vLi4vQWRkVGhyZWVwaWRcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uLy4uL01vZGFsJztcclxuXHJcbi8qXHJcblRPRE86IEltcHJvdmUgdGhlIFVYIGZvciBldmVyeXRoaW5nIGluIGhlcmUuXHJcbkl0J3MgdmVyeSBtdWNoIHBsYWNlaG9sZGVyLCBidXQgaXQgZ2V0cyB0aGUgam9iIGRvbmUuIFRoZSBvbGQgd2F5IG9mIGhhbmRsaW5nXHJcbmVtYWlsIGFkZHJlc3NlcyBpbiB1c2VyIHNldHRpbmdzIHdhcyB0byB1c2UgZGlhbG9ncyB0byBjb21tdW5pY2F0ZSBzdGF0ZSwgaG93ZXZlclxyXG5kdWUgdG8gb3VyIGRpYWxvZyBzeXN0ZW0gb3ZlcnJpZGluZyBkaWFsb2dzIChjYXVzaW5nIHVubW91bnRzKSB0aGlzIGNyZWF0ZXMgcHJvYmxlbXNcclxuZm9yIGEgc2FuZSBVWC4gRm9yIGluc3RhbmNlLCB0aGUgdXNlciBjb3VsZCBlYXNpbHkgZW5kIHVwIGVudGVyaW5nIGFuIGVtYWlsIGFkZHJlc3NcclxuYW5kIHJlY2VpdmUgYSBkaWFsb2cgdG8gdmVyaWZ5IHRoZSBhZGRyZXNzLCB3aGljaCB0aGVuIGNhdXNlcyB0aGUgY29tcG9uZW50IGhlcmVcclxudG8gZm9yZ2V0IHdoYXQgaXQgd2FzIGRvaW5nIGFuZCB1bHRpbWF0ZWx5IGZhaWwuIERpYWxvZ3MgYXJlIHN0aWxsIHVzZWQgaW4gc29tZVxyXG5wbGFjZXMgdG8gY29tbXVuaWNhdGUgZXJyb3JzIC0gdGhlc2Ugc2hvdWxkIGJlIHJlcGxhY2VkIHdpdGggaW5saW5lIHZhbGlkYXRpb24gd2hlblxyXG50aGF0IGlzIGF2YWlsYWJsZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgRXhpc3RpbmdFbWFpbEFkZHJlc3MgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBlbWFpbDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uUmVtb3ZlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgdmVyaWZ5UmVtb3ZlOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIF9vblJlbW92ZSA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3ZlcmlmeVJlbW92ZTogdHJ1ZX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25Eb250UmVtb3ZlID0gKGUpID0+IHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmVyaWZ5UmVtb3ZlOiBmYWxzZX0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25BY3R1YWxseVJlbW92ZSA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5kZWxldGVUaHJlZVBpZCh0aGlzLnByb3BzLmVtYWlsLm1lZGl1bSwgdGhpcy5wcm9wcy5lbWFpbC5hZGRyZXNzKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucHJvcHMub25SZW1vdmVkKHRoaXMucHJvcHMuZW1haWwpO1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIlVuYWJsZSB0byByZW1vdmUgY29udGFjdCBpbmZvcm1hdGlvbjogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdSZW1vdmUgM3BpZCBmYWlsZWQnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlVuYWJsZSB0byByZW1vdmUgY29udGFjdCBpbmZvcm1hdGlvblwiKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KFwiT3BlcmF0aW9uIGZhaWxlZFwiKSksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudmVyaWZ5UmVtb3ZlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V4aXN0aW5nRW1haWxBZGRyZXNzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdFbWFpbEFkZHJlc3NfcHJvbXB0VGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJSZW1vdmUgJShlbWFpbClzP1wiLCB7ZW1haWw6IHRoaXMucHJvcHMuZW1haWwuYWRkcmVzc30gKX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25BY3R1YWxseVJlbW92ZX0ga2luZD1cImRhbmdlcl9zbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdFbWFpbEFkZHJlc3NfY29uZmlybUJ0blwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJSZW1vdmVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uRG9udFJlbW92ZX0ga2luZD1cImxpbmtfc21cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0V4aXN0aW5nRW1haWxBZGRyZXNzX2NvbmZpcm1CdG5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQ2FuY2VsXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FeGlzdGluZ0VtYWlsQWRkcmVzc1wiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRXhpc3RpbmdFbWFpbEFkZHJlc3NfZW1haWxcIj57dGhpcy5wcm9wcy5lbWFpbC5hZGRyZXNzfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uUmVtb3ZlfSBraW5kPVwiZGFuZ2VyX3NtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiUmVtb3ZlXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFbWFpbEFkZHJlc3NlcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIGVtYWlsczogUHJvcFR5cGVzLmFycmF5LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25FbWFpbHNDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHZlcmlmeWluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIGFkZFRhc2s6IG51bGwsXHJcbiAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBuZXdFbWFpbEFkZHJlc3M6IFwiXCIsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBfb25SZW1vdmVkID0gKGFkZHJlc3MpID0+IHtcclxuICAgICAgICBjb25zdCBlbWFpbHMgPSB0aGlzLnByb3BzLmVtYWlscy5maWx0ZXIoKGUpID0+IGUgIT09IGFkZHJlc3MpO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25FbWFpbHNDaGFuZ2UoZW1haWxzKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQ2hhbmdlTmV3RW1haWxBZGRyZXNzID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbmV3RW1haWxBZGRyZXNzOiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQWRkQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUubmV3RW1haWxBZGRyZXNzKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3QgZW1haWwgPSB0aGlzLnN0YXRlLm5ld0VtYWlsQWRkcmVzcztcclxuXHJcbiAgICAgICAgLy8gVE9ETzogSW5saW5lIGZpZWxkIHZhbGlkYXRpb25cclxuICAgICAgICBpZiAoIUVtYWlsLmxvb2tzVmFsaWQoZW1haWwpKSB7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ludmFsaWQgZW1haWwgYWRkcmVzcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiSW52YWxpZCBFbWFpbCBBZGRyZXNzXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFwiVGhpcyBkb2Vzbid0IGFwcGVhciB0byBiZSBhIHZhbGlkIGVtYWlsIGFkZHJlc3NcIiksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0YXNrID0gbmV3IEFkZFRocmVlcGlkKCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmVyaWZ5aW5nOiB0cnVlLCBjb250aW51ZURpc2FibGVkOiB0cnVlLCBhZGRUYXNrOiB0YXNrfSk7XHJcblxyXG4gICAgICAgIHRhc2suYWRkRW1haWxBZGRyZXNzKGVtYWlsKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y29udGludWVEaXNhYmxlZDogZmFsc2V9KTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJVbmFibGUgdG8gYWRkIGVtYWlsIGFkZHJlc3MgXCIgKyBlbWFpbCArIFwiIFwiICsgZXJyKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmVyaWZ5aW5nOiBmYWxzZSwgY29udGludWVEaXNhYmxlZDogZmFsc2UsIGFkZFRhc2s6IG51bGx9KTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVW5hYmxlIHRvIGFkZCBlbWFpbCBhZGRyZXNzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJVbmFibGUgdG8gYWRkIGVtYWlsIGFkZHJlc3NcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uQ29udGludWVDbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2NvbnRpbnVlRGlzYWJsZWQ6IHRydWV9KTtcclxuICAgICAgICB0aGlzLnN0YXRlLmFkZFRhc2suY2hlY2tFbWFpbExpbmtDbGlja2VkKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGVtYWlsID0gdGhpcy5zdGF0ZS5uZXdFbWFpbEFkZHJlc3M7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgYWRkVGFzazogbnVsbCxcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdmVyaWZ5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIG5ld0VtYWlsQWRkcmVzczogXCJcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGVtYWlscyA9IFtcclxuICAgICAgICAgICAgICAgIC4uLnRoaXMucHJvcHMuZW1haWxzLFxyXG4gICAgICAgICAgICAgICAgeyBhZGRyZXNzOiBlbWFpbCwgbWVkaXVtOiBcImVtYWlsXCIgfSxcclxuICAgICAgICAgICAgXTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkVtYWlsc0NoYW5nZShlbWFpbHMpO1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y29udGludWVEaXNhYmxlZDogZmFsc2V9KTtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlID09PSAnTV9USFJFRVBJRF9BVVRIX0ZBSUxFRCcpIHtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXCJFbWFpbCBoYXNuJ3QgYmVlbiB2ZXJpZmllZCB5ZXRcIiwgXCJcIiwgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJZb3VyIGVtYWlsIGFkZHJlc3MgaGFzbid0IGJlZW4gdmVyaWZpZWQgeWV0XCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIkNsaWNrIHRoZSBsaW5rIGluIHRoZSBlbWFpbCB5b3UgcmVjZWl2ZWQgdG8gdmVyaWZ5IFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJhbmQgdGhlbiBjbGljayBjb250aW51ZSBhZ2Fpbi5cIiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJVbmFibGUgdG8gdmVyaWZ5IGVtYWlsIGFkZHJlc3M6IFwiLCBlcnIpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVW5hYmxlIHRvIHZlcmlmeSBlbWFpbCBhZGRyZXNzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiVW5hYmxlIHRvIHZlcmlmeSBlbWFpbCBhZGRyZXNzLlwiKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGV4aXN0aW5nRW1haWxFbGVtZW50cyA9IHRoaXMucHJvcHMuZW1haWxzLm1hcCgoZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gPEV4aXN0aW5nRW1haWxBZGRyZXNzIGVtYWlsPXtlfSBvblJlbW92ZWQ9e3RoaXMuX29uUmVtb3ZlZH0ga2V5PXtlLmFkZHJlc3N9IC8+O1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgYWRkQnV0dG9uID0gKFxyXG4gICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkFkZENsaWNrfSBraW5kPVwicHJpbWFyeVwiPlxyXG4gICAgICAgICAgICAgICAge190KFwiQWRkXCIpfVxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgKTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS52ZXJpZnlpbmcpIHtcclxuICAgICAgICAgICAgYWRkQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXY+e190KFwiV2UndmUgc2VudCB5b3UgYW4gZW1haWwgdG8gdmVyaWZ5IHlvdXIgYWRkcmVzcy4gUGxlYXNlIGZvbGxvdyB0aGUgaW5zdHJ1Y3Rpb25zIHRoZXJlIGFuZCB0aGVuIGNsaWNrIHRoZSBidXR0b24gYmVsb3cuXCIpfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkNvbnRpbnVlQ2xpY2t9IGtpbmQ9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUuY29udGludWVEaXNhYmxlZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJDb250aW51ZVwiKX1cclxuICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbWFpbEFkZHJlc3Nlc1wiPlxyXG4gICAgICAgICAgICAgICAge2V4aXN0aW5nRW1haWxFbGVtZW50c31cclxuICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLl9vbkFkZENsaWNrfSBhdXRvQ29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgbm9WYWxpZGF0ZT17dHJ1ZX0gY2xhc3NOYW1lPVwibXhfRW1haWxBZGRyZXNzZXNfbmV3XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KFwiRW1haWwgQWRkcmVzc1wiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUudmVyaWZ5aW5nfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5uZXdFbWFpbEFkZHJlc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vbkNoYW5nZU5ld0VtYWlsQWRkcmVzc31cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIHthZGRCdXR0b259XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19