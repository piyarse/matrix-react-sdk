"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _EditableItemList = _interopRequireDefault(require("../elements/EditableItemList"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _Field = _interopRequireDefault(require("../elements/Field"));

var _ErrorDialog = _interopRequireDefault(require("../dialogs/ErrorDialog"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _RoomPublishSetting = _interopRequireDefault(require("./RoomPublishSetting"));

/*
Copyright 2016 OpenMarket Ltd
Copyright 2018, 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class EditableAliasesList extends _EditableItemList.default {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onAliasAdded", async () => {
      await this._aliasField.current.validate({
        allowEmpty: false
      });

      if (this._aliasField.current.isValid) {
        if (this.props.onItemAdded) this.props.onItemAdded(this.props.newItem);
        return;
      }

      this._aliasField.current.focus();

      this._aliasField.current.validate({
        allowEmpty: false,
        focused: true
      });
    });
    this._aliasField = (0, _react.createRef)();
  }

  _renderNewItemField() {
    // if we don't need the RoomAliasField,
    // we don't need to overriden version of _renderNewItemField
    if (!this.props.domain) {
      return super._renderNewItemField();
    }

    const RoomAliasField = sdk.getComponent('views.elements.RoomAliasField');

    const onChange = alias => this._onNewItemChanged({
      target: {
        value: alias
      }
    });

    return _react.default.createElement("form", {
      onSubmit: this._onAliasAdded,
      autoComplete: "off",
      noValidate: true,
      className: "mx_EditableItemList_newItem"
    }, _react.default.createElement(RoomAliasField, {
      ref: this._aliasField,
      onChange: onChange,
      value: this.props.newItem || "",
      domain: this.props.domain
    }), _react.default.createElement(_AccessibleButton.default, {
      onClick: this._onAliasAdded,
      kind: "primary"
    }, (0, _languageHandler._t)("Add")));
  }

}

class AliasSettings extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onNewAliasChanged", value => {
      this.setState({
        newAlias: value
      });
    });
    (0, _defineProperty2.default)(this, "onLocalAliasAdded", alias => {
      if (!alias || alias.length === 0) return; // ignore attempts to create blank aliases

      const localDomain = _MatrixClientPeg.MatrixClientPeg.get().getDomain();

      if (!alias.includes(':')) alias += ':' + localDomain;

      _MatrixClientPeg.MatrixClientPeg.get().createAlias(alias, this.props.roomId).then(() => {
        this.setState({
          localAliases: this.state.localAliases.concat(alias),
          newAlias: null
        });

        if (!this.state.canonicalAlias) {
          this.changeCanonicalAlias(alias);
        }
      }).catch(err => {
        console.error(err);

        _Modal.default.createTrackedDialog('Error creating alias', '', _ErrorDialog.default, {
          title: (0, _languageHandler._t)("Error creating alias"),
          description: (0, _languageHandler._t)("There was an error creating that alias. It may not be allowed by the server " + "or a temporary failure occurred.")
        });
      });
    });
    (0, _defineProperty2.default)(this, "onLocalAliasDeleted", index => {
      const alias = this.state.localAliases[index]; // TODO: In future, we should probably be making sure that the alias actually belongs
      // to this room. See https://github.com/vector-im/riot-web/issues/7353

      _MatrixClientPeg.MatrixClientPeg.get().deleteAlias(alias).then(() => {
        const localAliases = this.state.localAliases.filter(a => a !== alias);
        this.setState({
          localAliases
        });

        if (this.state.canonicalAlias === alias) {
          this.changeCanonicalAlias(null);
        }
      }).catch(err => {
        console.error(err);
        let description;

        if (err.errcode === "M_FORBIDDEN") {
          description = (0, _languageHandler._t)("You don't have permission to delete the alias.");
        } else {
          description = (0, _languageHandler._t)("There was an error removing that alias. It may no longer exist or a temporary " + "error occurred.");
        }

        _Modal.default.createTrackedDialog('Error removing alias', '', _ErrorDialog.default, {
          title: (0, _languageHandler._t)("Error removing alias"),
          description
        });
      });
    });
    (0, _defineProperty2.default)(this, "onLocalAliasesToggled", event => {
      // expanded
      if (event.target.open) {
        // if local aliases haven't been preloaded yet at component mount
        if (!this.props.canSetCanonicalAlias && this.state.localAliases.length === 0) {
          this.loadLocalAliases();
        }
      }

      this.setState({
        detailsOpen: event.target.open
      });
    });
    (0, _defineProperty2.default)(this, "onCanonicalAliasChange", event => {
      this.changeCanonicalAlias(event.target.value);
    });
    (0, _defineProperty2.default)(this, "onNewAltAliasChanged", value => {
      this.setState({
        newAltAlias: value
      });
    });
    (0, _defineProperty2.default)(this, "onAltAliasAdded", alias => {
      const altAliases = this.state.altAliases.slice();

      if (!altAliases.some(a => a.trim() === alias.trim())) {
        altAliases.push(alias.trim());
        this.changeAltAliases(altAliases);
        this.setState({
          newAltAlias: ""
        });
      }
    });
    (0, _defineProperty2.default)(this, "onAltAliasDeleted", index => {
      const altAliases = this.state.altAliases.slice();
      altAliases.splice(index, 1);
      this.changeAltAliases(altAliases);
    });
    const state = {
      altAliases: [],
      // [ #alias:domain.tld, ... ]
      localAliases: [],
      // [ #alias:my-hs.tld, ... ]
      canonicalAlias: null,
      // #canonical:domain.tld
      updatingCanonicalAlias: false,
      localAliasesLoading: false,
      detailsOpen: false
    };

    if (props.canonicalAliasEvent) {
      const content = props.canonicalAliasEvent.getContent();
      const altAliases = content.alt_aliases;

      if (Array.isArray(altAliases)) {
        state.altAliases = altAliases.slice();
      }

      state.canonicalAlias = content.alias;
    }

    this.state = state;
  }

  componentDidMount() {
    if (this.props.canSetCanonicalAlias) {
      // load local aliases for providing recommendations
      // for the canonical alias and alt_aliases
      this.loadLocalAliases();
    }
  }

  async loadLocalAliases() {
    this.setState({
      localAliasesLoading: true
    });

    try {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      let localAliases = [];

      if (await cli.doesServerSupportUnstableFeature("org.matrix.msc2432")) {
        const response = await cli.unstableGetLocalAliases(this.props.roomId);

        if (Array.isArray(response.aliases)) {
          localAliases = response.aliases;
        }
      }

      this.setState({
        localAliases
      });
    } finally {
      this.setState({
        localAliasesLoading: false
      });
    }
  }

  changeCanonicalAlias(alias) {
    if (!this.props.canSetCanonicalAlias) return;
    const oldAlias = this.state.canonicalAlias;
    this.setState({
      canonicalAlias: alias,
      updatingCanonicalAlias: true
    });
    const eventContent = {
      alt_aliases: this.state.altAliases
    };
    if (alias) eventContent["alias"] = alias;

    _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(this.props.roomId, "m.room.canonical_alias", eventContent, "").catch(err => {
      console.error(err);

      _Modal.default.createTrackedDialog('Error updating main address', '', _ErrorDialog.default, {
        title: (0, _languageHandler._t)("Error updating main address"),
        description: (0, _languageHandler._t)("There was an error updating the room's main address. It may not be allowed by the server " + "or a temporary failure occurred.")
      });

      this.setState({
        canonicalAlias: oldAlias
      });
    }).finally(() => {
      this.setState({
        updatingCanonicalAlias: false
      });
    });
  }

  changeAltAliases(altAliases) {
    if (!this.props.canSetCanonicalAlias) return;
    this.setState({
      updatingCanonicalAlias: true,
      altAliases
    });
    const eventContent = {};

    if (this.state.canonicalAlias) {
      eventContent.alias = this.state.canonicalAlias;
    }

    if (altAliases) {
      eventContent["alt_aliases"] = altAliases;
    }

    _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(this.props.roomId, "m.room.canonical_alias", eventContent, "").catch(err => {
      console.error(err);

      _Modal.default.createTrackedDialog('Error updating alternative addresses', '', _ErrorDialog.default, {
        title: (0, _languageHandler._t)("Error updating main address"),
        description: (0, _languageHandler._t)("There was an error updating the room's alternative addresses. " + "It may not be allowed by the server or a temporary failure occurred.")
      });
    }).finally(() => {
      this.setState({
        updatingCanonicalAlias: false
      });
    });
  }

  _getAliases() {
    return this.state.altAliases.concat(this._getLocalNonAltAliases());
  }

  _getLocalNonAltAliases() {
    const {
      altAliases
    } = this.state;
    return this.state.localAliases.filter(alias => !altAliases.includes(alias));
  }

  render() {
    const localDomain = _MatrixClientPeg.MatrixClientPeg.get().getDomain();

    let found = false;
    const canonicalValue = this.state.canonicalAlias || "";

    const canonicalAliasSection = _react.default.createElement(_Field.default, {
      onChange: this.onCanonicalAliasChange,
      value: canonicalValue,
      disabled: this.state.updatingCanonicalAlias || !this.props.canSetCanonicalAlias,
      element: "select",
      id: "canonicalAlias",
      label: (0, _languageHandler._t)('Main address')
    }, _react.default.createElement("option", {
      value: "",
      key: "unset"
    }, (0, _languageHandler._t)('not specified')), this._getAliases().map((alias, i) => {
      if (alias === this.state.canonicalAlias) found = true;
      return _react.default.createElement("option", {
        value: alias,
        key: i
      }, alias);
    }), found || !this.state.canonicalAlias ? '' : _react.default.createElement("option", {
      value: this.state.canonicalAlias,
      key: "arbitrary"
    }, this.state.canonicalAlias));

    let localAliasesList;

    if (this.state.localAliasesLoading) {
      const Spinner = sdk.getComponent("elements.Spinner");
      localAliasesList = _react.default.createElement(Spinner, null);
    } else {
      localAliasesList = _react.default.createElement(EditableAliasesList, {
        id: "roomAliases",
        className: "mx_RoomSettings_localAliases",
        items: this.state.localAliases,
        newItem: this.state.newAlias,
        onNewItemChanged: this.onNewAliasChanged,
        canRemove: this.props.canSetAliases,
        canEdit: this.props.canSetAliases,
        onItemAdded: this.onLocalAliasAdded,
        onItemRemoved: this.onLocalAliasDeleted,
        noItemsLabel: (0, _languageHandler._t)('This room has no local addresses'),
        placeholder: (0, _languageHandler._t)('Local address'),
        domain: localDomain
      });
    }

    return _react.default.createElement("div", {
      className: "mx_AliasSettings"
    }, _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading"
    }, (0, _languageHandler._t)("Published Addresses")), _react.default.createElement("p", null, (0, _languageHandler._t)("Published addresses can be used by anyone on any server to join your room. " + "To publish an address, it needs to be set as a local address first.")), canonicalAliasSection, _react.default.createElement(_RoomPublishSetting.default, {
      roomId: this.props.roomId,
      canSetCanonicalAlias: this.props.canSetCanonicalAlias
    }), _react.default.createElement("datalist", {
      id: "mx_AliasSettings_altRecommendations"
    }, this._getLocalNonAltAliases().map(alias => {
      return _react.default.createElement("option", {
        value: alias,
        key: alias
      });
    }), ";"), _react.default.createElement(EditableAliasesList, {
      id: "roomAltAliases",
      className: "mx_RoomSettings_altAliases",
      items: this.state.altAliases,
      newItem: this.state.newAltAlias,
      onNewItemChanged: this.onNewAltAliasChanged,
      canRemove: this.props.canSetCanonicalAlias,
      canEdit: this.props.canSetCanonicalAlias,
      onItemAdded: this.onAltAliasAdded,
      onItemRemoved: this.onAltAliasDeleted,
      suggestionsListId: "mx_AliasSettings_altRecommendations",
      itemsLabel: (0, _languageHandler._t)('Other published addresses:'),
      noItemsLabel: (0, _languageHandler._t)('No other published addresses yet, add one below'),
      placeholder: (0, _languageHandler._t)('New published address (e.g. #alias:server)')
    }), _react.default.createElement("span", {
      className: "mx_SettingsTab_subheading mx_AliasSettings_localAliasHeader"
    }, (0, _languageHandler._t)("Local Addresses")), _react.default.createElement("p", null, (0, _languageHandler._t)("Set addresses for this room so users can find this room through your homeserver (%(localDomain)s)", {
      localDomain
    })), _react.default.createElement("details", {
      onToggle: this.onLocalAliasesToggled
    }, _react.default.createElement("summary", null, this.state.detailsOpen ? (0, _languageHandler._t)('Show less') : (0, _languageHandler._t)("Show more")), localAliasesList));
  }

}

exports.default = AliasSettings;
(0, _defineProperty2.default)(AliasSettings, "propTypes", {
  roomId: _propTypes.default.string.isRequired,
  canSetCanonicalAlias: _propTypes.default.bool.isRequired,
  canSetAliases: _propTypes.default.bool.isRequired,
  canonicalAliasEvent: _propTypes.default.object // MatrixEvent

});
(0, _defineProperty2.default)(AliasSettings, "defaultProps", {
  canSetAliases: false,
  canSetCanonicalAlias: false,
  aliasEvents: []
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21fc2V0dGluZ3MvQWxpYXNTZXR0aW5ncy5qcyJdLCJuYW1lcyI6WyJFZGl0YWJsZUFsaWFzZXNMaXN0IiwiRWRpdGFibGVJdGVtTGlzdCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJfYWxpYXNGaWVsZCIsImN1cnJlbnQiLCJ2YWxpZGF0ZSIsImFsbG93RW1wdHkiLCJpc1ZhbGlkIiwib25JdGVtQWRkZWQiLCJuZXdJdGVtIiwiZm9jdXMiLCJmb2N1c2VkIiwiX3JlbmRlck5ld0l0ZW1GaWVsZCIsImRvbWFpbiIsIlJvb21BbGlhc0ZpZWxkIiwic2RrIiwiZ2V0Q29tcG9uZW50Iiwib25DaGFuZ2UiLCJhbGlhcyIsIl9vbk5ld0l0ZW1DaGFuZ2VkIiwidGFyZ2V0IiwidmFsdWUiLCJfb25BbGlhc0FkZGVkIiwiQWxpYXNTZXR0aW5ncyIsIlJlYWN0IiwiQ29tcG9uZW50Iiwic2V0U3RhdGUiLCJuZXdBbGlhcyIsImxlbmd0aCIsImxvY2FsRG9tYWluIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0RG9tYWluIiwiaW5jbHVkZXMiLCJjcmVhdGVBbGlhcyIsInJvb21JZCIsInRoZW4iLCJsb2NhbEFsaWFzZXMiLCJzdGF0ZSIsImNvbmNhdCIsImNhbm9uaWNhbEFsaWFzIiwiY2hhbmdlQ2Fub25pY2FsQWxpYXMiLCJjYXRjaCIsImVyciIsImNvbnNvbGUiLCJlcnJvciIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsIkVycm9yRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImluZGV4IiwiZGVsZXRlQWxpYXMiLCJmaWx0ZXIiLCJhIiwiZXJyY29kZSIsImV2ZW50Iiwib3BlbiIsImNhblNldENhbm9uaWNhbEFsaWFzIiwibG9hZExvY2FsQWxpYXNlcyIsImRldGFpbHNPcGVuIiwibmV3QWx0QWxpYXMiLCJhbHRBbGlhc2VzIiwic2xpY2UiLCJzb21lIiwidHJpbSIsInB1c2giLCJjaGFuZ2VBbHRBbGlhc2VzIiwic3BsaWNlIiwidXBkYXRpbmdDYW5vbmljYWxBbGlhcyIsImxvY2FsQWxpYXNlc0xvYWRpbmciLCJjYW5vbmljYWxBbGlhc0V2ZW50IiwiY29udGVudCIsImdldENvbnRlbnQiLCJhbHRfYWxpYXNlcyIsIkFycmF5IiwiaXNBcnJheSIsImNvbXBvbmVudERpZE1vdW50IiwiY2xpIiwiZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUiLCJyZXNwb25zZSIsInVuc3RhYmxlR2V0TG9jYWxBbGlhc2VzIiwiYWxpYXNlcyIsIm9sZEFsaWFzIiwiZXZlbnRDb250ZW50Iiwic2VuZFN0YXRlRXZlbnQiLCJmaW5hbGx5IiwiX2dldEFsaWFzZXMiLCJfZ2V0TG9jYWxOb25BbHRBbGlhc2VzIiwicmVuZGVyIiwiZm91bmQiLCJjYW5vbmljYWxWYWx1ZSIsImNhbm9uaWNhbEFsaWFzU2VjdGlvbiIsIm9uQ2Fub25pY2FsQWxpYXNDaGFuZ2UiLCJtYXAiLCJpIiwibG9jYWxBbGlhc2VzTGlzdCIsIlNwaW5uZXIiLCJvbk5ld0FsaWFzQ2hhbmdlZCIsImNhblNldEFsaWFzZXMiLCJvbkxvY2FsQWxpYXNBZGRlZCIsIm9uTG9jYWxBbGlhc0RlbGV0ZWQiLCJvbk5ld0FsdEFsaWFzQ2hhbmdlZCIsIm9uQWx0QWxpYXNBZGRlZCIsIm9uQWx0QWxpYXNEZWxldGVkIiwib25Mb2NhbEFsaWFzZXNUb2dnbGVkIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsImJvb2wiLCJvYmplY3QiLCJhbGlhc0V2ZW50cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUEzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2QkEsTUFBTUEsbUJBQU4sU0FBa0NDLHlCQUFsQyxDQUFtRDtBQUMvQ0MsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUseURBTUgsWUFBWTtBQUN4QixZQUFNLEtBQUtDLFdBQUwsQ0FBaUJDLE9BQWpCLENBQXlCQyxRQUF6QixDQUFrQztBQUFFQyxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQUFsQyxDQUFOOztBQUVBLFVBQUksS0FBS0gsV0FBTCxDQUFpQkMsT0FBakIsQ0FBeUJHLE9BQTdCLEVBQXNDO0FBQ2xDLFlBQUksS0FBS0wsS0FBTCxDQUFXTSxXQUFmLEVBQTRCLEtBQUtOLEtBQUwsQ0FBV00sV0FBWCxDQUF1QixLQUFLTixLQUFMLENBQVdPLE9BQWxDO0FBQzVCO0FBQ0g7O0FBRUQsV0FBS04sV0FBTCxDQUFpQkMsT0FBakIsQ0FBeUJNLEtBQXpCOztBQUNBLFdBQUtQLFdBQUwsQ0FBaUJDLE9BQWpCLENBQXlCQyxRQUF6QixDQUFrQztBQUFFQyxRQUFBQSxVQUFVLEVBQUUsS0FBZDtBQUFxQkssUUFBQUEsT0FBTyxFQUFFO0FBQTlCLE9BQWxDO0FBQ0gsS0FoQmtCO0FBR2YsU0FBS1IsV0FBTCxHQUFtQix1QkFBbkI7QUFDSDs7QUFjRFMsRUFBQUEsbUJBQW1CLEdBQUc7QUFDbEI7QUFDQTtBQUNBLFFBQUksQ0FBQyxLQUFLVixLQUFMLENBQVdXLE1BQWhCLEVBQXdCO0FBQ3BCLGFBQU8sTUFBTUQsbUJBQU4sRUFBUDtBQUNIOztBQUNELFVBQU1FLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUF2Qjs7QUFDQSxVQUFNQyxRQUFRLEdBQUlDLEtBQUQsSUFBVyxLQUFLQyxpQkFBTCxDQUF1QjtBQUFDQyxNQUFBQSxNQUFNLEVBQUU7QUFBQ0MsUUFBQUEsS0FBSyxFQUFFSDtBQUFSO0FBQVQsS0FBdkIsQ0FBNUI7O0FBQ0EsV0FDSTtBQUNJLE1BQUEsUUFBUSxFQUFFLEtBQUtJLGFBRG5CO0FBRUksTUFBQSxZQUFZLEVBQUMsS0FGakI7QUFHSSxNQUFBLFVBQVUsRUFBRSxJQUhoQjtBQUlJLE1BQUEsU0FBUyxFQUFDO0FBSmQsT0FNSSw2QkFBQyxjQUFEO0FBQ0ksTUFBQSxHQUFHLEVBQUUsS0FBS25CLFdBRGQ7QUFFSSxNQUFBLFFBQVEsRUFBRWMsUUFGZDtBQUdJLE1BQUEsS0FBSyxFQUFFLEtBQUtmLEtBQUwsQ0FBV08sT0FBWCxJQUFzQixFQUhqQztBQUlJLE1BQUEsTUFBTSxFQUFFLEtBQUtQLEtBQUwsQ0FBV1c7QUFKdkIsTUFOSixFQVdJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFLEtBQUtTLGFBQWhDO0FBQStDLE1BQUEsSUFBSSxFQUFDO0FBQXBELE9BQ00seUJBQUcsS0FBSCxDQUROLENBWEosQ0FESjtBQWlCSDs7QUE1QzhDOztBQStDcEMsTUFBTUMsYUFBTixTQUE0QkMsZUFBTUMsU0FBbEMsQ0FBNEM7QUFjdkR4QixFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSw2REFnSEVtQixLQUFELElBQVc7QUFDM0IsV0FBS0ssUUFBTCxDQUFjO0FBQUNDLFFBQUFBLFFBQVEsRUFBRU47QUFBWCxPQUFkO0FBQ0gsS0FsSGtCO0FBQUEsNkRBb0hFSCxLQUFELElBQVc7QUFDM0IsVUFBSSxDQUFDQSxLQUFELElBQVVBLEtBQUssQ0FBQ1UsTUFBTixLQUFpQixDQUEvQixFQUFrQyxPQURQLENBQ2U7O0FBRTFDLFlBQU1DLFdBQVcsR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsU0FBdEIsRUFBcEI7O0FBQ0EsVUFBSSxDQUFDZCxLQUFLLENBQUNlLFFBQU4sQ0FBZSxHQUFmLENBQUwsRUFBMEJmLEtBQUssSUFBSSxNQUFNVyxXQUFmOztBQUUxQkMsdUNBQWdCQyxHQUFoQixHQUFzQkcsV0FBdEIsQ0FBa0NoQixLQUFsQyxFQUF5QyxLQUFLaEIsS0FBTCxDQUFXaUMsTUFBcEQsRUFBNERDLElBQTVELENBQWlFLE1BQU07QUFDbkUsYUFBS1YsUUFBTCxDQUFjO0FBQ1ZXLFVBQUFBLFlBQVksRUFBRSxLQUFLQyxLQUFMLENBQVdELFlBQVgsQ0FBd0JFLE1BQXhCLENBQStCckIsS0FBL0IsQ0FESjtBQUVWUyxVQUFBQSxRQUFRLEVBQUU7QUFGQSxTQUFkOztBQUlBLFlBQUksQ0FBQyxLQUFLVyxLQUFMLENBQVdFLGNBQWhCLEVBQWdDO0FBQzVCLGVBQUtDLG9CQUFMLENBQTBCdkIsS0FBMUI7QUFDSDtBQUNKLE9BUkQsRUFRR3dCLEtBUkgsQ0FRVUMsR0FBRCxJQUFTO0FBQ2RDLFFBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjRixHQUFkOztBQUNBRyx1QkFBTUMsbUJBQU4sQ0FBMEIsc0JBQTFCLEVBQWtELEVBQWxELEVBQXNEQyxvQkFBdEQsRUFBbUU7QUFDL0RDLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxzQkFBSCxDQUR3RDtBQUUvREMsVUFBQUEsV0FBVyxFQUFFLHlCQUNULGlGQUNBLGtDQUZTO0FBRmtELFNBQW5FO0FBT0gsT0FqQkQ7QUFrQkgsS0E1SWtCO0FBQUEsK0RBOElJQyxLQUFELElBQVc7QUFDN0IsWUFBTWpDLEtBQUssR0FBRyxLQUFLb0IsS0FBTCxDQUFXRCxZQUFYLENBQXdCYyxLQUF4QixDQUFkLENBRDZCLENBRTdCO0FBQ0E7O0FBQ0FyQix1Q0FBZ0JDLEdBQWhCLEdBQXNCcUIsV0FBdEIsQ0FBa0NsQyxLQUFsQyxFQUF5Q2tCLElBQXpDLENBQThDLE1BQU07QUFDaEQsY0FBTUMsWUFBWSxHQUFHLEtBQUtDLEtBQUwsQ0FBV0QsWUFBWCxDQUF3QmdCLE1BQXhCLENBQStCQyxDQUFDLElBQUlBLENBQUMsS0FBS3BDLEtBQTFDLENBQXJCO0FBQ0EsYUFBS1EsUUFBTCxDQUFjO0FBQUNXLFVBQUFBO0FBQUQsU0FBZDs7QUFFQSxZQUFJLEtBQUtDLEtBQUwsQ0FBV0UsY0FBWCxLQUE4QnRCLEtBQWxDLEVBQXlDO0FBQ3JDLGVBQUt1QixvQkFBTCxDQUEwQixJQUExQjtBQUNIO0FBQ0osT0FQRCxFQU9HQyxLQVBILENBT1VDLEdBQUQsSUFBUztBQUNkQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBZDtBQUNBLFlBQUlPLFdBQUo7O0FBQ0EsWUFBSVAsR0FBRyxDQUFDWSxPQUFKLEtBQWdCLGFBQXBCLEVBQW1DO0FBQy9CTCxVQUFBQSxXQUFXLEdBQUcseUJBQUcsZ0RBQUgsQ0FBZDtBQUNILFNBRkQsTUFFTztBQUNIQSxVQUFBQSxXQUFXLEdBQUcseUJBQ1YsbUZBQ0EsaUJBRlUsQ0FBZDtBQUlIOztBQUNESix1QkFBTUMsbUJBQU4sQ0FBMEIsc0JBQTFCLEVBQWtELEVBQWxELEVBQXNEQyxvQkFBdEQsRUFBbUU7QUFDL0RDLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxzQkFBSCxDQUR3RDtBQUUvREMsVUFBQUE7QUFGK0QsU0FBbkU7QUFJSCxPQXRCRDtBQXVCSCxLQXpLa0I7QUFBQSxpRUEyS01NLEtBQUQsSUFBVztBQUMvQjtBQUNBLFVBQUlBLEtBQUssQ0FBQ3BDLE1BQU4sQ0FBYXFDLElBQWpCLEVBQXVCO0FBQ25CO0FBQ0EsWUFBSSxDQUFDLEtBQUt2RCxLQUFMLENBQVd3RCxvQkFBWixJQUFvQyxLQUFLcEIsS0FBTCxDQUFXRCxZQUFYLENBQXdCVCxNQUF4QixLQUFtQyxDQUEzRSxFQUE4RTtBQUMxRSxlQUFLK0IsZ0JBQUw7QUFDSDtBQUNKOztBQUNELFdBQUtqQyxRQUFMLENBQWM7QUFBQ2tDLFFBQUFBLFdBQVcsRUFBRUosS0FBSyxDQUFDcEMsTUFBTixDQUFhcUM7QUFBM0IsT0FBZDtBQUNILEtBcExrQjtBQUFBLGtFQXNMT0QsS0FBRCxJQUFXO0FBQ2hDLFdBQUtmLG9CQUFMLENBQTBCZSxLQUFLLENBQUNwQyxNQUFOLENBQWFDLEtBQXZDO0FBQ0gsS0F4TGtCO0FBQUEsZ0VBMExLQSxLQUFELElBQVc7QUFDOUIsV0FBS0ssUUFBTCxDQUFjO0FBQUNtQyxRQUFBQSxXQUFXLEVBQUV4QztBQUFkLE9BQWQ7QUFDSCxLQTVMa0I7QUFBQSwyREE4TEFILEtBQUQsSUFBVztBQUN6QixZQUFNNEMsVUFBVSxHQUFHLEtBQUt4QixLQUFMLENBQVd3QixVQUFYLENBQXNCQyxLQUF0QixFQUFuQjs7QUFDQSxVQUFJLENBQUNELFVBQVUsQ0FBQ0UsSUFBWCxDQUFnQlYsQ0FBQyxJQUFJQSxDQUFDLENBQUNXLElBQUYsT0FBYS9DLEtBQUssQ0FBQytDLElBQU4sRUFBbEMsQ0FBTCxFQUFzRDtBQUNsREgsUUFBQUEsVUFBVSxDQUFDSSxJQUFYLENBQWdCaEQsS0FBSyxDQUFDK0MsSUFBTixFQUFoQjtBQUNBLGFBQUtFLGdCQUFMLENBQXNCTCxVQUF0QjtBQUNBLGFBQUtwQyxRQUFMLENBQWM7QUFBQ21DLFVBQUFBLFdBQVcsRUFBRTtBQUFkLFNBQWQ7QUFDSDtBQUNKLEtBck1rQjtBQUFBLDZEQXVNRVYsS0FBRCxJQUFXO0FBQzNCLFlBQU1XLFVBQVUsR0FBRyxLQUFLeEIsS0FBTCxDQUFXd0IsVUFBWCxDQUFzQkMsS0FBdEIsRUFBbkI7QUFDQUQsTUFBQUEsVUFBVSxDQUFDTSxNQUFYLENBQWtCakIsS0FBbEIsRUFBeUIsQ0FBekI7QUFDQSxXQUFLZ0IsZ0JBQUwsQ0FBc0JMLFVBQXRCO0FBQ0gsS0EzTWtCO0FBR2YsVUFBTXhCLEtBQUssR0FBRztBQUNWd0IsTUFBQUEsVUFBVSxFQUFFLEVBREY7QUFDTTtBQUNoQnpCLE1BQUFBLFlBQVksRUFBRSxFQUZKO0FBRVE7QUFDbEJHLE1BQUFBLGNBQWMsRUFBRSxJQUhOO0FBR1k7QUFDdEI2QixNQUFBQSxzQkFBc0IsRUFBRSxLQUpkO0FBS1ZDLE1BQUFBLG1CQUFtQixFQUFFLEtBTFg7QUFNVlYsTUFBQUEsV0FBVyxFQUFFO0FBTkgsS0FBZDs7QUFTQSxRQUFJMUQsS0FBSyxDQUFDcUUsbUJBQVYsRUFBK0I7QUFDM0IsWUFBTUMsT0FBTyxHQUFHdEUsS0FBSyxDQUFDcUUsbUJBQU4sQ0FBMEJFLFVBQTFCLEVBQWhCO0FBQ0EsWUFBTVgsVUFBVSxHQUFHVSxPQUFPLENBQUNFLFdBQTNCOztBQUNBLFVBQUlDLEtBQUssQ0FBQ0MsT0FBTixDQUFjZCxVQUFkLENBQUosRUFBK0I7QUFDM0J4QixRQUFBQSxLQUFLLENBQUN3QixVQUFOLEdBQW1CQSxVQUFVLENBQUNDLEtBQVgsRUFBbkI7QUFDSDs7QUFDRHpCLE1BQUFBLEtBQUssQ0FBQ0UsY0FBTixHQUF1QmdDLE9BQU8sQ0FBQ3RELEtBQS9CO0FBQ0g7O0FBRUQsU0FBS29CLEtBQUwsR0FBYUEsS0FBYjtBQUNIOztBQUVEdUMsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsUUFBSSxLQUFLM0UsS0FBTCxDQUFXd0Qsb0JBQWYsRUFBcUM7QUFDakM7QUFDQTtBQUNBLFdBQUtDLGdCQUFMO0FBQ0g7QUFDSjs7QUFFRCxRQUFNQSxnQkFBTixHQUF5QjtBQUNyQixTQUFLakMsUUFBTCxDQUFjO0FBQUU0QyxNQUFBQSxtQkFBbUIsRUFBRTtBQUF2QixLQUFkOztBQUNBLFFBQUk7QUFDQSxZQUFNUSxHQUFHLEdBQUdoRCxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsVUFBSU0sWUFBWSxHQUFHLEVBQW5COztBQUNBLFVBQUksTUFBTXlDLEdBQUcsQ0FBQ0MsZ0NBQUosQ0FBcUMsb0JBQXJDLENBQVYsRUFBc0U7QUFDbEUsY0FBTUMsUUFBUSxHQUFHLE1BQU1GLEdBQUcsQ0FBQ0csdUJBQUosQ0FBNEIsS0FBSy9FLEtBQUwsQ0FBV2lDLE1BQXZDLENBQXZCOztBQUNBLFlBQUl3QyxLQUFLLENBQUNDLE9BQU4sQ0FBY0ksUUFBUSxDQUFDRSxPQUF2QixDQUFKLEVBQXFDO0FBQ2pDN0MsVUFBQUEsWUFBWSxHQUFHMkMsUUFBUSxDQUFDRSxPQUF4QjtBQUNIO0FBQ0o7O0FBQ0QsV0FBS3hELFFBQUwsQ0FBYztBQUFFVyxRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQVZELFNBVVU7QUFDTixXQUFLWCxRQUFMLENBQWM7QUFBRTRDLFFBQUFBLG1CQUFtQixFQUFFO0FBQXZCLE9BQWQ7QUFDSDtBQUNKOztBQUVEN0IsRUFBQUEsb0JBQW9CLENBQUN2QixLQUFELEVBQVE7QUFDeEIsUUFBSSxDQUFDLEtBQUtoQixLQUFMLENBQVd3RCxvQkFBaEIsRUFBc0M7QUFFdEMsVUFBTXlCLFFBQVEsR0FBRyxLQUFLN0MsS0FBTCxDQUFXRSxjQUE1QjtBQUNBLFNBQUtkLFFBQUwsQ0FBYztBQUNWYyxNQUFBQSxjQUFjLEVBQUV0QixLQUROO0FBRVZtRCxNQUFBQSxzQkFBc0IsRUFBRTtBQUZkLEtBQWQ7QUFLQSxVQUFNZSxZQUFZLEdBQUc7QUFDakJWLE1BQUFBLFdBQVcsRUFBRSxLQUFLcEMsS0FBTCxDQUFXd0I7QUFEUCxLQUFyQjtBQUlBLFFBQUk1QyxLQUFKLEVBQVdrRSxZQUFZLENBQUMsT0FBRCxDQUFaLEdBQXdCbEUsS0FBeEI7O0FBRVhZLHFDQUFnQkMsR0FBaEIsR0FBc0JzRCxjQUF0QixDQUFxQyxLQUFLbkYsS0FBTCxDQUFXaUMsTUFBaEQsRUFBd0Qsd0JBQXhELEVBQ0lpRCxZQURKLEVBQ2tCLEVBRGxCLEVBQ3NCMUMsS0FEdEIsQ0FDNkJDLEdBQUQsSUFBUztBQUNqQ0MsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQWQ7O0FBQ0FHLHFCQUFNQyxtQkFBTixDQUEwQiw2QkFBMUIsRUFBeUQsRUFBekQsRUFBNkRDLG9CQUE3RCxFQUEwRTtBQUN0RUMsUUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZCQUFILENBRCtEO0FBRXRFQyxRQUFBQSxXQUFXLEVBQUUseUJBQ1QsOEZBQ0Esa0NBRlM7QUFGeUQsT0FBMUU7O0FBT0EsV0FBS3hCLFFBQUwsQ0FBYztBQUFDYyxRQUFBQSxjQUFjLEVBQUUyQztBQUFqQixPQUFkO0FBQ0gsS0FYRCxFQVdHRyxPQVhILENBV1csTUFBTTtBQUNiLFdBQUs1RCxRQUFMLENBQWM7QUFBQzJDLFFBQUFBLHNCQUFzQixFQUFFO0FBQXpCLE9BQWQ7QUFDSCxLQWJEO0FBY0g7O0FBRURGLEVBQUFBLGdCQUFnQixDQUFDTCxVQUFELEVBQWE7QUFDekIsUUFBSSxDQUFDLEtBQUs1RCxLQUFMLENBQVd3RCxvQkFBaEIsRUFBc0M7QUFFdEMsU0FBS2hDLFFBQUwsQ0FBYztBQUNWMkMsTUFBQUEsc0JBQXNCLEVBQUUsSUFEZDtBQUVWUCxNQUFBQTtBQUZVLEtBQWQ7QUFLQSxVQUFNc0IsWUFBWSxHQUFHLEVBQXJCOztBQUVBLFFBQUksS0FBSzlDLEtBQUwsQ0FBV0UsY0FBZixFQUErQjtBQUMzQjRDLE1BQUFBLFlBQVksQ0FBQ2xFLEtBQWIsR0FBcUIsS0FBS29CLEtBQUwsQ0FBV0UsY0FBaEM7QUFDSDs7QUFDRCxRQUFJc0IsVUFBSixFQUFnQjtBQUNac0IsTUFBQUEsWUFBWSxDQUFDLGFBQUQsQ0FBWixHQUE4QnRCLFVBQTlCO0FBQ0g7O0FBRURoQyxxQ0FBZ0JDLEdBQWhCLEdBQXNCc0QsY0FBdEIsQ0FBcUMsS0FBS25GLEtBQUwsQ0FBV2lDLE1BQWhELEVBQXdELHdCQUF4RCxFQUNJaUQsWUFESixFQUNrQixFQURsQixFQUNzQjFDLEtBRHRCLENBQzZCQyxHQUFELElBQVM7QUFDakNDLE1BQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjRixHQUFkOztBQUNBRyxxQkFBTUMsbUJBQU4sQ0FBMEIsc0NBQTFCLEVBQWtFLEVBQWxFLEVBQXNFQyxvQkFBdEUsRUFBbUY7QUFDL0VDLFFBQUFBLEtBQUssRUFBRSx5QkFBRyw2QkFBSCxDQUR3RTtBQUUvRUMsUUFBQUEsV0FBVyxFQUFFLHlCQUNULG1FQUNBLHNFQUZTO0FBRmtFLE9BQW5GO0FBT0gsS0FWRCxFQVVHb0MsT0FWSCxDQVVXLE1BQU07QUFDYixXQUFLNUQsUUFBTCxDQUFjO0FBQUMyQyxRQUFBQSxzQkFBc0IsRUFBRTtBQUF6QixPQUFkO0FBQ0gsS0FaRDtBQWFIOztBQStGRGtCLEVBQUFBLFdBQVcsR0FBRztBQUNWLFdBQU8sS0FBS2pELEtBQUwsQ0FBV3dCLFVBQVgsQ0FBc0J2QixNQUF0QixDQUE2QixLQUFLaUQsc0JBQUwsRUFBN0IsQ0FBUDtBQUNIOztBQUVEQSxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQixVQUFNO0FBQUMxQixNQUFBQTtBQUFELFFBQWUsS0FBS3hCLEtBQTFCO0FBQ0EsV0FBTyxLQUFLQSxLQUFMLENBQVdELFlBQVgsQ0FBd0JnQixNQUF4QixDQUErQm5DLEtBQUssSUFBSSxDQUFDNEMsVUFBVSxDQUFDN0IsUUFBWCxDQUFvQmYsS0FBcEIsQ0FBekMsQ0FBUDtBQUNIOztBQUVEdUUsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTTVELFdBQVcsR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsU0FBdEIsRUFBcEI7O0FBRUEsUUFBSTBELEtBQUssR0FBRyxLQUFaO0FBQ0EsVUFBTUMsY0FBYyxHQUFHLEtBQUtyRCxLQUFMLENBQVdFLGNBQVgsSUFBNkIsRUFBcEQ7O0FBQ0EsVUFBTW9ELHFCQUFxQixHQUN2Qiw2QkFBQyxjQUFEO0FBQU8sTUFBQSxRQUFRLEVBQUUsS0FBS0Msc0JBQXRCO0FBQThDLE1BQUEsS0FBSyxFQUFFRixjQUFyRDtBQUNPLE1BQUEsUUFBUSxFQUFFLEtBQUtyRCxLQUFMLENBQVcrQixzQkFBWCxJQUFxQyxDQUFDLEtBQUtuRSxLQUFMLENBQVd3RCxvQkFEbEU7QUFFTyxNQUFBLE9BQU8sRUFBQyxRQUZmO0FBRXdCLE1BQUEsRUFBRSxFQUFDLGdCQUYzQjtBQUU0QyxNQUFBLEtBQUssRUFBRSx5QkFBRyxjQUFIO0FBRm5ELE9BR0k7QUFBUSxNQUFBLEtBQUssRUFBQyxFQUFkO0FBQWlCLE1BQUEsR0FBRyxFQUFDO0FBQXJCLE9BQStCLHlCQUFHLGVBQUgsQ0FBL0IsQ0FISixFQUtRLEtBQUs2QixXQUFMLEdBQW1CTyxHQUFuQixDQUF1QixDQUFDNUUsS0FBRCxFQUFRNkUsQ0FBUixLQUFjO0FBQ2pDLFVBQUk3RSxLQUFLLEtBQUssS0FBS29CLEtBQUwsQ0FBV0UsY0FBekIsRUFBeUNrRCxLQUFLLEdBQUcsSUFBUjtBQUN6QyxhQUNJO0FBQVEsUUFBQSxLQUFLLEVBQUV4RSxLQUFmO0FBQXNCLFFBQUEsR0FBRyxFQUFFNkU7QUFBM0IsU0FDTTdFLEtBRE4sQ0FESjtBQUtILEtBUEQsQ0FMUixFQWVRd0UsS0FBSyxJQUFJLENBQUMsS0FBS3BELEtBQUwsQ0FBV0UsY0FBckIsR0FBc0MsRUFBdEMsR0FDQTtBQUFRLE1BQUEsS0FBSyxFQUFHLEtBQUtGLEtBQUwsQ0FBV0UsY0FBM0I7QUFBNEMsTUFBQSxHQUFHLEVBQUM7QUFBaEQsT0FDTSxLQUFLRixLQUFMLENBQVdFLGNBRGpCLENBaEJSLENBREo7O0FBd0JBLFFBQUl3RCxnQkFBSjs7QUFDQSxRQUFJLEtBQUsxRCxLQUFMLENBQVdnQyxtQkFBZixFQUFvQztBQUNoQyxZQUFNMkIsT0FBTyxHQUFHbEYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBZ0YsTUFBQUEsZ0JBQWdCLEdBQUcsNkJBQUMsT0FBRCxPQUFuQjtBQUNILEtBSEQsTUFHTztBQUNIQSxNQUFBQSxnQkFBZ0IsR0FBSSw2QkFBQyxtQkFBRDtBQUNoQixRQUFBLEVBQUUsRUFBQyxhQURhO0FBRWhCLFFBQUEsU0FBUyxFQUFFLDhCQUZLO0FBR2hCLFFBQUEsS0FBSyxFQUFFLEtBQUsxRCxLQUFMLENBQVdELFlBSEY7QUFJaEIsUUFBQSxPQUFPLEVBQUUsS0FBS0MsS0FBTCxDQUFXWCxRQUpKO0FBS2hCLFFBQUEsZ0JBQWdCLEVBQUUsS0FBS3VFLGlCQUxQO0FBTWhCLFFBQUEsU0FBUyxFQUFFLEtBQUtoRyxLQUFMLENBQVdpRyxhQU5OO0FBT2hCLFFBQUEsT0FBTyxFQUFFLEtBQUtqRyxLQUFMLENBQVdpRyxhQVBKO0FBUWhCLFFBQUEsV0FBVyxFQUFFLEtBQUtDLGlCQVJGO0FBU2hCLFFBQUEsYUFBYSxFQUFFLEtBQUtDLG1CQVRKO0FBVWhCLFFBQUEsWUFBWSxFQUFFLHlCQUFHLGtDQUFILENBVkU7QUFXaEIsUUFBQSxXQUFXLEVBQUUseUJBQUcsZUFBSCxDQVhHO0FBWWhCLFFBQUEsTUFBTSxFQUFFeEU7QUFaUSxRQUFwQjtBQWNIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUE2Qyx5QkFBRyxxQkFBSCxDQUE3QyxDQURKLEVBRUksd0NBQUkseUJBQUcsZ0ZBQ0gscUVBREEsQ0FBSixDQUZKLEVBSUsrRCxxQkFKTCxFQUtJLDZCQUFDLDJCQUFEO0FBQW9CLE1BQUEsTUFBTSxFQUFFLEtBQUsxRixLQUFMLENBQVdpQyxNQUF2QztBQUErQyxNQUFBLG9CQUFvQixFQUFFLEtBQUtqQyxLQUFMLENBQVd3RDtBQUFoRixNQUxKLEVBTUk7QUFBVSxNQUFBLEVBQUUsRUFBQztBQUFiLE9BQ0ssS0FBSzhCLHNCQUFMLEdBQThCTSxHQUE5QixDQUFrQzVFLEtBQUssSUFBSTtBQUN4QyxhQUFPO0FBQVEsUUFBQSxLQUFLLEVBQUVBLEtBQWY7QUFBc0IsUUFBQSxHQUFHLEVBQUVBO0FBQTNCLFFBQVA7QUFDSCxLQUZBLENBREwsTUFOSixFQVdJLDZCQUFDLG1CQUFEO0FBQ0ksTUFBQSxFQUFFLEVBQUMsZ0JBRFA7QUFFSSxNQUFBLFNBQVMsRUFBRSw0QkFGZjtBQUdJLE1BQUEsS0FBSyxFQUFFLEtBQUtvQixLQUFMLENBQVd3QixVQUh0QjtBQUlJLE1BQUEsT0FBTyxFQUFFLEtBQUt4QixLQUFMLENBQVd1QixXQUp4QjtBQUtJLE1BQUEsZ0JBQWdCLEVBQUUsS0FBS3lDLG9CQUwzQjtBQU1JLE1BQUEsU0FBUyxFQUFFLEtBQUtwRyxLQUFMLENBQVd3RCxvQkFOMUI7QUFPSSxNQUFBLE9BQU8sRUFBRSxLQUFLeEQsS0FBTCxDQUFXd0Qsb0JBUHhCO0FBUUksTUFBQSxXQUFXLEVBQUUsS0FBSzZDLGVBUnRCO0FBU0ksTUFBQSxhQUFhLEVBQUUsS0FBS0MsaUJBVHhCO0FBVUksTUFBQSxpQkFBaUIsRUFBQyxxQ0FWdEI7QUFXSSxNQUFBLFVBQVUsRUFBRSx5QkFBRyw0QkFBSCxDQVhoQjtBQVlJLE1BQUEsWUFBWSxFQUFFLHlCQUFHLGlEQUFILENBWmxCO0FBYUksTUFBQSxXQUFXLEVBQUUseUJBQUcsNENBQUg7QUFiakIsTUFYSixFQTBCSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQStFLHlCQUFHLGlCQUFILENBQS9FLENBMUJKLEVBMkJJLHdDQUFJLHlCQUFHLG1HQUFILEVBQXdHO0FBQUMzRSxNQUFBQTtBQUFELEtBQXhHLENBQUosQ0EzQkosRUE0Qkk7QUFBUyxNQUFBLFFBQVEsRUFBRSxLQUFLNEU7QUFBeEIsT0FDSSw4Q0FBVyxLQUFLbkUsS0FBTCxDQUFXc0IsV0FBWCxHQUF5Qix5QkFBRyxXQUFILENBQXpCLEdBQTJDLHlCQUFHLFdBQUgsQ0FBdEQsQ0FESixFQUVLb0MsZ0JBRkwsQ0E1QkosQ0FESjtBQW1DSDs7QUF6VHNEOzs7OEJBQXRDekUsYSxlQUNFO0FBQ2ZZLEVBQUFBLE1BQU0sRUFBRXVFLG1CQUFVQyxNQUFWLENBQWlCQyxVQURWO0FBRWZsRCxFQUFBQSxvQkFBb0IsRUFBRWdELG1CQUFVRyxJQUFWLENBQWVELFVBRnRCO0FBR2ZULEVBQUFBLGFBQWEsRUFBRU8sbUJBQVVHLElBQVYsQ0FBZUQsVUFIZjtBQUlmckMsRUFBQUEsbUJBQW1CLEVBQUVtQyxtQkFBVUksTUFKaEIsQ0FJd0I7O0FBSnhCLEM7OEJBREZ2RixhLGtCQVFLO0FBQ2xCNEUsRUFBQUEsYUFBYSxFQUFFLEtBREc7QUFFbEJ6QyxFQUFBQSxvQkFBb0IsRUFBRSxLQUZKO0FBR2xCcUQsRUFBQUEsV0FBVyxFQUFFO0FBSEssQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IEVkaXRhYmxlSXRlbUxpc3QgZnJvbSBcIi4uL2VsZW1lbnRzL0VkaXRhYmxlSXRlbUxpc3RcIjtcclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgRmllbGQgZnJvbSBcIi4uL2VsZW1lbnRzL0ZpZWxkXCI7XHJcbmltcG9ydCBFcnJvckRpYWxvZyBmcm9tIFwiLi4vZGlhbG9ncy9FcnJvckRpYWxvZ1wiO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tIFwiLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvblwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCBSb29tUHVibGlzaFNldHRpbmcgZnJvbSBcIi4vUm9vbVB1Ymxpc2hTZXR0aW5nXCI7XHJcblxyXG5jbGFzcyBFZGl0YWJsZUFsaWFzZXNMaXN0IGV4dGVuZHMgRWRpdGFibGVJdGVtTGlzdCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fYWxpYXNGaWVsZCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkFsaWFzQWRkZWQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5fYWxpYXNGaWVsZC5jdXJyZW50LnZhbGlkYXRlKHsgYWxsb3dFbXB0eTogZmFsc2UgfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9hbGlhc0ZpZWxkLmN1cnJlbnQuaXNWYWxpZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkl0ZW1BZGRlZCkgdGhpcy5wcm9wcy5vbkl0ZW1BZGRlZCh0aGlzLnByb3BzLm5ld0l0ZW0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9hbGlhc0ZpZWxkLmN1cnJlbnQuZm9jdXMoKTtcclxuICAgICAgICB0aGlzLl9hbGlhc0ZpZWxkLmN1cnJlbnQudmFsaWRhdGUoeyBhbGxvd0VtcHR5OiBmYWxzZSwgZm9jdXNlZDogdHJ1ZSB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3JlbmRlck5ld0l0ZW1GaWVsZCgpIHtcclxuICAgICAgICAvLyBpZiB3ZSBkb24ndCBuZWVkIHRoZSBSb29tQWxpYXNGaWVsZCxcclxuICAgICAgICAvLyB3ZSBkb24ndCBuZWVkIHRvIG92ZXJyaWRlbiB2ZXJzaW9uIG9mIF9yZW5kZXJOZXdJdGVtRmllbGRcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuZG9tYWluKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzdXBlci5fcmVuZGVyTmV3SXRlbUZpZWxkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IFJvb21BbGlhc0ZpZWxkID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuUm9vbUFsaWFzRmllbGQnKTtcclxuICAgICAgICBjb25zdCBvbkNoYW5nZSA9IChhbGlhcykgPT4gdGhpcy5fb25OZXdJdGVtQ2hhbmdlZCh7dGFyZ2V0OiB7dmFsdWU6IGFsaWFzfX0pO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxmb3JtXHJcbiAgICAgICAgICAgICAgICBvblN1Ym1pdD17dGhpcy5fb25BbGlhc0FkZGVkfVxyXG4gICAgICAgICAgICAgICAgYXV0b0NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICAgICAgIG5vVmFsaWRhdGU9e3RydWV9XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9FZGl0YWJsZUl0ZW1MaXN0X25ld0l0ZW1cIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8Um9vbUFsaWFzRmllbGRcclxuICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX2FsaWFzRmllbGR9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnByb3BzLm5ld0l0ZW0gfHwgXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICBkb21haW49e3RoaXMucHJvcHMuZG9tYWlufSAvPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb25BbGlhc0FkZGVkfSBraW5kPVwicHJpbWFyeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJBZGRcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWxpYXNTZXR0aW5ncyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGNhblNldENhbm9uaWNhbEFsaWFzOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGNhblNldEFsaWFzZXM6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgY2Fub25pY2FsQWxpYXNFdmVudDogUHJvcFR5cGVzLm9iamVjdCwgLy8gTWF0cml4RXZlbnRcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICBjYW5TZXRBbGlhc2VzOiBmYWxzZSxcclxuICAgICAgICBjYW5TZXRDYW5vbmljYWxBbGlhczogZmFsc2UsXHJcbiAgICAgICAgYWxpYXNFdmVudHM6IFtdLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGFsdEFsaWFzZXM6IFtdLCAvLyBbICNhbGlhczpkb21haW4udGxkLCAuLi4gXVxyXG4gICAgICAgICAgICBsb2NhbEFsaWFzZXM6IFtdLCAvLyBbICNhbGlhczpteS1ocy50bGQsIC4uLiBdXHJcbiAgICAgICAgICAgIGNhbm9uaWNhbEFsaWFzOiBudWxsLCAvLyAjY2Fub25pY2FsOmRvbWFpbi50bGRcclxuICAgICAgICAgICAgdXBkYXRpbmdDYW5vbmljYWxBbGlhczogZmFsc2UsXHJcbiAgICAgICAgICAgIGxvY2FsQWxpYXNlc0xvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZXRhaWxzT3BlbjogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKHByb3BzLmNhbm9uaWNhbEFsaWFzRXZlbnQpIHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudCA9IHByb3BzLmNhbm9uaWNhbEFsaWFzRXZlbnQuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgICAgICBjb25zdCBhbHRBbGlhc2VzID0gY29udGVudC5hbHRfYWxpYXNlcztcclxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoYWx0QWxpYXNlcykpIHtcclxuICAgICAgICAgICAgICAgIHN0YXRlLmFsdEFsaWFzZXMgPSBhbHRBbGlhc2VzLnNsaWNlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc3RhdGUuY2Fub25pY2FsQWxpYXMgPSBjb250ZW50LmFsaWFzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHN0YXRlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmNhblNldENhbm9uaWNhbEFsaWFzKSB7XHJcbiAgICAgICAgICAgIC8vIGxvYWQgbG9jYWwgYWxpYXNlcyBmb3IgcHJvdmlkaW5nIHJlY29tbWVuZGF0aW9uc1xyXG4gICAgICAgICAgICAvLyBmb3IgdGhlIGNhbm9uaWNhbCBhbGlhcyBhbmQgYWx0X2FsaWFzZXNcclxuICAgICAgICAgICAgdGhpcy5sb2FkTG9jYWxBbGlhc2VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGxvYWRMb2NhbEFsaWFzZXMoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGxvY2FsQWxpYXNlc0xvYWRpbmc6IHRydWUgfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICBsZXQgbG9jYWxBbGlhc2VzID0gW107XHJcbiAgICAgICAgICAgIGlmIChhd2FpdCBjbGkuZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUoXCJvcmcubWF0cml4Lm1zYzI0MzJcIikpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgY2xpLnVuc3RhYmxlR2V0TG9jYWxBbGlhc2VzKHRoaXMucHJvcHMucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHJlc3BvbnNlLmFsaWFzZXMpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbG9jYWxBbGlhc2VzID0gcmVzcG9uc2UuYWxpYXNlcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgbG9jYWxBbGlhc2VzIH0pO1xyXG4gICAgICAgIH0gZmluYWxseSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBsb2NhbEFsaWFzZXNMb2FkaW5nOiBmYWxzZSB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlQ2Fub25pY2FsQWxpYXMoYWxpYXMpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuY2FuU2V0Q2Fub25pY2FsQWxpYXMpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3Qgb2xkQWxpYXMgPSB0aGlzLnN0YXRlLmNhbm9uaWNhbEFsaWFzO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjYW5vbmljYWxBbGlhczogYWxpYXMsXHJcbiAgICAgICAgICAgIHVwZGF0aW5nQ2Fub25pY2FsQWxpYXM6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50Q29udGVudCA9IHtcclxuICAgICAgICAgICAgYWx0X2FsaWFzZXM6IHRoaXMuc3RhdGUuYWx0QWxpYXNlcyxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoYWxpYXMpIGV2ZW50Q29udGVudFtcImFsaWFzXCJdID0gYWxpYXM7XHJcblxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uY2Fub25pY2FsX2FsaWFzXCIsXHJcbiAgICAgICAgICAgIGV2ZW50Q29udGVudCwgXCJcIikuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0Vycm9yIHVwZGF0aW5nIG1haW4gYWRkcmVzcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3IgdXBkYXRpbmcgbWFpbiBhZGRyZXNzXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVGhlcmUgd2FzIGFuIGVycm9yIHVwZGF0aW5nIHRoZSByb29tJ3MgbWFpbiBhZGRyZXNzLiBJdCBtYXkgbm90IGJlIGFsbG93ZWQgYnkgdGhlIHNlcnZlciBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvciBhIHRlbXBvcmFyeSBmYWlsdXJlIG9jY3VycmVkLlwiLFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2Nhbm9uaWNhbEFsaWFzOiBvbGRBbGlhc30pO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt1cGRhdGluZ0Nhbm9uaWNhbEFsaWFzOiBmYWxzZX0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZUFsdEFsaWFzZXMoYWx0QWxpYXNlcykge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5jYW5TZXRDYW5vbmljYWxBbGlhcykgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdXBkYXRpbmdDYW5vbmljYWxBbGlhczogdHJ1ZSxcclxuICAgICAgICAgICAgYWx0QWxpYXNlcyxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgZXZlbnRDb250ZW50ID0ge307XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNhbm9uaWNhbEFsaWFzKSB7XHJcbiAgICAgICAgICAgIGV2ZW50Q29udGVudC5hbGlhcyA9IHRoaXMuc3RhdGUuY2Fub25pY2FsQWxpYXM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChhbHRBbGlhc2VzKSB7XHJcbiAgICAgICAgICAgIGV2ZW50Q29udGVudFtcImFsdF9hbGlhc2VzXCJdID0gYWx0QWxpYXNlcztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLnJvb21JZCwgXCJtLnJvb20uY2Fub25pY2FsX2FsaWFzXCIsXHJcbiAgICAgICAgICAgIGV2ZW50Q29udGVudCwgXCJcIikuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0Vycm9yIHVwZGF0aW5nIGFsdGVybmF0aXZlIGFkZHJlc3NlcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3IgdXBkYXRpbmcgbWFpbiBhZGRyZXNzXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVGhlcmUgd2FzIGFuIGVycm9yIHVwZGF0aW5nIHRoZSByb29tJ3MgYWx0ZXJuYXRpdmUgYWRkcmVzc2VzLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJJdCBtYXkgbm90IGJlIGFsbG93ZWQgYnkgdGhlIHNlcnZlciBvciBhIHRlbXBvcmFyeSBmYWlsdXJlIG9jY3VycmVkLlwiLFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3VwZGF0aW5nQ2Fub25pY2FsQWxpYXM6IGZhbHNlfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25OZXdBbGlhc0NoYW5nZWQgPSAodmFsdWUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtuZXdBbGlhczogdmFsdWV9KTtcclxuICAgIH07XHJcblxyXG4gICAgb25Mb2NhbEFsaWFzQWRkZWQgPSAoYWxpYXMpID0+IHtcclxuICAgICAgICBpZiAoIWFsaWFzIHx8IGFsaWFzLmxlbmd0aCA9PT0gMCkgcmV0dXJuOyAvLyBpZ25vcmUgYXR0ZW1wdHMgdG8gY3JlYXRlIGJsYW5rIGFsaWFzZXNcclxuXHJcbiAgICAgICAgY29uc3QgbG9jYWxEb21haW4gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0RG9tYWluKCk7XHJcbiAgICAgICAgaWYgKCFhbGlhcy5pbmNsdWRlcygnOicpKSBhbGlhcyArPSAnOicgKyBsb2NhbERvbWFpbjtcclxuXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWF0ZUFsaWFzKGFsaWFzLCB0aGlzLnByb3BzLnJvb21JZCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9jYWxBbGlhc2VzOiB0aGlzLnN0YXRlLmxvY2FsQWxpYXNlcy5jb25jYXQoYWxpYXMpLFxyXG4gICAgICAgICAgICAgICAgbmV3QWxpYXM6IG51bGwsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY2Fub25pY2FsQWxpYXMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hhbmdlQ2Fub25pY2FsQWxpYXMoYWxpYXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0Vycm9yIGNyZWF0aW5nIGFsaWFzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJFcnJvciBjcmVhdGluZyBhbGlhc1wiKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlRoZXJlIHdhcyBhbiBlcnJvciBjcmVhdGluZyB0aGF0IGFsaWFzLiBJdCBtYXkgbm90IGJlIGFsbG93ZWQgYnkgdGhlIHNlcnZlciBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvciBhIHRlbXBvcmFyeSBmYWlsdXJlIG9jY3VycmVkLlwiLFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uTG9jYWxBbGlhc0RlbGV0ZWQgPSAoaW5kZXgpID0+IHtcclxuICAgICAgICBjb25zdCBhbGlhcyA9IHRoaXMuc3RhdGUubG9jYWxBbGlhc2VzW2luZGV4XTtcclxuICAgICAgICAvLyBUT0RPOiBJbiBmdXR1cmUsIHdlIHNob3VsZCBwcm9iYWJseSBiZSBtYWtpbmcgc3VyZSB0aGF0IHRoZSBhbGlhcyBhY3R1YWxseSBiZWxvbmdzXHJcbiAgICAgICAgLy8gdG8gdGhpcyByb29tLiBTZWUgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvNzM1M1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5kZWxldGVBbGlhcyhhbGlhcykudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxvY2FsQWxpYXNlcyA9IHRoaXMuc3RhdGUubG9jYWxBbGlhc2VzLmZpbHRlcihhID0+IGEgIT09IGFsaWFzKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bG9jYWxBbGlhc2VzfSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5jYW5vbmljYWxBbGlhcyA9PT0gYWxpYXMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hhbmdlQ2Fub25pY2FsQWxpYXMobnVsbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgbGV0IGRlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICBpZiAoZXJyLmVycmNvZGUgPT09IFwiTV9GT1JCSURERU5cIikge1xyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb24gPSBfdChcIllvdSBkb24ndCBoYXZlIHBlcm1pc3Npb24gdG8gZGVsZXRlIHRoZSBhbGlhcy5cIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbiA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVGhlcmUgd2FzIGFuIGVycm9yIHJlbW92aW5nIHRoYXQgYWxpYXMuIEl0IG1heSBubyBsb25nZXIgZXhpc3Qgb3IgYSB0ZW1wb3JhcnkgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZXJyb3Igb2NjdXJyZWQuXCIsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0Vycm9yIHJlbW92aW5nIGFsaWFzJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJFcnJvciByZW1vdmluZyBhbGlhc1wiKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgb25Mb2NhbEFsaWFzZXNUb2dnbGVkID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgLy8gZXhwYW5kZWRcclxuICAgICAgICBpZiAoZXZlbnQudGFyZ2V0Lm9wZW4pIHtcclxuICAgICAgICAgICAgLy8gaWYgbG9jYWwgYWxpYXNlcyBoYXZlbid0IGJlZW4gcHJlbG9hZGVkIHlldCBhdCBjb21wb25lbnQgbW91bnRcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnByb3BzLmNhblNldENhbm9uaWNhbEFsaWFzICYmIHRoaXMuc3RhdGUubG9jYWxBbGlhc2VzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkTG9jYWxBbGlhc2VzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGV0YWlsc09wZW46IGV2ZW50LnRhcmdldC5vcGVufSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uQ2Fub25pY2FsQWxpYXNDaGFuZ2UgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICB0aGlzLmNoYW5nZUNhbm9uaWNhbEFsaWFzKGV2ZW50LnRhcmdldC52YWx1ZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uTmV3QWx0QWxpYXNDaGFuZ2VkID0gKHZhbHVlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bmV3QWx0QWxpYXM6IHZhbHVlfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25BbHRBbGlhc0FkZGVkID0gKGFsaWFzKSA9PiB7XHJcbiAgICAgICAgY29uc3QgYWx0QWxpYXNlcyA9IHRoaXMuc3RhdGUuYWx0QWxpYXNlcy5zbGljZSgpO1xyXG4gICAgICAgIGlmICghYWx0QWxpYXNlcy5zb21lKGEgPT4gYS50cmltKCkgPT09IGFsaWFzLnRyaW0oKSkpIHtcclxuICAgICAgICAgICAgYWx0QWxpYXNlcy5wdXNoKGFsaWFzLnRyaW0oKSk7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlQWx0QWxpYXNlcyhhbHRBbGlhc2VzKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bmV3QWx0QWxpYXM6IFwiXCJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25BbHRBbGlhc0RlbGV0ZWQgPSAoaW5kZXgpID0+IHtcclxuICAgICAgICBjb25zdCBhbHRBbGlhc2VzID0gdGhpcy5zdGF0ZS5hbHRBbGlhc2VzLnNsaWNlKCk7XHJcbiAgICAgICAgYWx0QWxpYXNlcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlQWx0QWxpYXNlcyhhbHRBbGlhc2VzKTtcclxuICAgIH1cclxuXHJcbiAgICBfZ2V0QWxpYXNlcygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5hbHRBbGlhc2VzLmNvbmNhdCh0aGlzLl9nZXRMb2NhbE5vbkFsdEFsaWFzZXMoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldExvY2FsTm9uQWx0QWxpYXNlcygpIHtcclxuICAgICAgICBjb25zdCB7YWx0QWxpYXNlc30gPSB0aGlzLnN0YXRlO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN0YXRlLmxvY2FsQWxpYXNlcy5maWx0ZXIoYWxpYXMgPT4gIWFsdEFsaWFzZXMuaW5jbHVkZXMoYWxpYXMpKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgbG9jYWxEb21haW4gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0RG9tYWluKCk7XHJcblxyXG4gICAgICAgIGxldCBmb3VuZCA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IGNhbm9uaWNhbFZhbHVlID0gdGhpcy5zdGF0ZS5jYW5vbmljYWxBbGlhcyB8fCBcIlwiO1xyXG4gICAgICAgIGNvbnN0IGNhbm9uaWNhbEFsaWFzU2VjdGlvbiA9IChcclxuICAgICAgICAgICAgPEZpZWxkIG9uQ2hhbmdlPXt0aGlzLm9uQ2Fub25pY2FsQWxpYXNDaGFuZ2V9IHZhbHVlPXtjYW5vbmljYWxWYWx1ZX1cclxuICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0aGlzLnN0YXRlLnVwZGF0aW5nQ2Fub25pY2FsQWxpYXMgfHwgIXRoaXMucHJvcHMuY2FuU2V0Q2Fub25pY2FsQWxpYXN9XHJcbiAgICAgICAgICAgICAgICAgICBlbGVtZW50PSdzZWxlY3QnIGlkPSdjYW5vbmljYWxBbGlhcycgbGFiZWw9e190KCdNYWluIGFkZHJlc3MnKX0+XHJcbiAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiXCIga2V5PVwidW5zZXRcIj57IF90KCdub3Qgc3BlY2lmaWVkJykgfTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2dldEFsaWFzZXMoKS5tYXAoKGFsaWFzLCBpKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbGlhcyA9PT0gdGhpcy5zdGF0ZS5jYW5vbmljYWxBbGlhcykgZm91bmQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT17YWxpYXN9IGtleT17aX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBhbGlhcyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kIHx8ICF0aGlzLnN0YXRlLmNhbm9uaWNhbEFsaWFzID8gJycgOlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9eyB0aGlzLnN0YXRlLmNhbm9uaWNhbEFsaWFzIH0ga2V5PSdhcmJpdHJhcnknPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUuY2Fub25pY2FsQWxpYXMgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L0ZpZWxkPlxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCBsb2NhbEFsaWFzZXNMaXN0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmxvY2FsQWxpYXNlc0xvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICBsb2NhbEFsaWFzZXNMaXN0ID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbG9jYWxBbGlhc2VzTGlzdCA9ICg8RWRpdGFibGVBbGlhc2VzTGlzdFxyXG4gICAgICAgICAgICAgICAgaWQ9XCJyb29tQWxpYXNlc1wiXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1wibXhfUm9vbVNldHRpbmdzX2xvY2FsQWxpYXNlc1wifVxyXG4gICAgICAgICAgICAgICAgaXRlbXM9e3RoaXMuc3RhdGUubG9jYWxBbGlhc2VzfVxyXG4gICAgICAgICAgICAgICAgbmV3SXRlbT17dGhpcy5zdGF0ZS5uZXdBbGlhc31cclxuICAgICAgICAgICAgICAgIG9uTmV3SXRlbUNoYW5nZWQ9e3RoaXMub25OZXdBbGlhc0NoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICBjYW5SZW1vdmU9e3RoaXMucHJvcHMuY2FuU2V0QWxpYXNlc31cclxuICAgICAgICAgICAgICAgIGNhbkVkaXQ9e3RoaXMucHJvcHMuY2FuU2V0QWxpYXNlc31cclxuICAgICAgICAgICAgICAgIG9uSXRlbUFkZGVkPXt0aGlzLm9uTG9jYWxBbGlhc0FkZGVkfVxyXG4gICAgICAgICAgICAgICAgb25JdGVtUmVtb3ZlZD17dGhpcy5vbkxvY2FsQWxpYXNEZWxldGVkfVxyXG4gICAgICAgICAgICAgICAgbm9JdGVtc0xhYmVsPXtfdCgnVGhpcyByb29tIGhhcyBubyBsb2NhbCBhZGRyZXNzZXMnKX1cclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtfdCgnTG9jYWwgYWRkcmVzcycpfVxyXG4gICAgICAgICAgICAgICAgZG9tYWluPXtsb2NhbERvbWFpbn1cclxuICAgICAgICAgICAgLz4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0FsaWFzU2V0dGluZ3MnPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJoZWFkaW5nJz57X3QoXCJQdWJsaXNoZWQgQWRkcmVzc2VzXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcIlB1Ymxpc2hlZCBhZGRyZXNzZXMgY2FuIGJlIHVzZWQgYnkgYW55b25lIG9uIGFueSBzZXJ2ZXIgdG8gam9pbiB5b3VyIHJvb20uIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIlRvIHB1Ymxpc2ggYW4gYWRkcmVzcywgaXQgbmVlZHMgdG8gYmUgc2V0IGFzIGEgbG9jYWwgYWRkcmVzcyBmaXJzdC5cIil9PC9wPlxyXG4gICAgICAgICAgICAgICAge2Nhbm9uaWNhbEFsaWFzU2VjdGlvbn1cclxuICAgICAgICAgICAgICAgIDxSb29tUHVibGlzaFNldHRpbmcgcm9vbUlkPXt0aGlzLnByb3BzLnJvb21JZH0gY2FuU2V0Q2Fub25pY2FsQWxpYXM9e3RoaXMucHJvcHMuY2FuU2V0Q2Fub25pY2FsQWxpYXN9IC8+XHJcbiAgICAgICAgICAgICAgICA8ZGF0YWxpc3QgaWQ9XCJteF9BbGlhc1NldHRpbmdzX2FsdFJlY29tbWVuZGF0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLl9nZXRMb2NhbE5vbkFsdEFsaWFzZXMoKS5tYXAoYWxpYXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gPG9wdGlvbiB2YWx1ZT17YWxpYXN9IGtleT17YWxpYXN9IC8+O1xyXG4gICAgICAgICAgICAgICAgICAgIH0pfTtcclxuICAgICAgICAgICAgICAgIDwvZGF0YWxpc3Q+XHJcbiAgICAgICAgICAgICAgICA8RWRpdGFibGVBbGlhc2VzTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgIGlkPVwicm9vbUFsdEFsaWFzZXNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XCJteF9Sb29tU2V0dGluZ3NfYWx0QWxpYXNlc1wifVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXt0aGlzLnN0YXRlLmFsdEFsaWFzZXN9XHJcbiAgICAgICAgICAgICAgICAgICAgbmV3SXRlbT17dGhpcy5zdGF0ZS5uZXdBbHRBbGlhc31cclxuICAgICAgICAgICAgICAgICAgICBvbk5ld0l0ZW1DaGFuZ2VkPXt0aGlzLm9uTmV3QWx0QWxpYXNDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICAgICAgIGNhblJlbW92ZT17dGhpcy5wcm9wcy5jYW5TZXRDYW5vbmljYWxBbGlhc31cclxuICAgICAgICAgICAgICAgICAgICBjYW5FZGl0PXt0aGlzLnByb3BzLmNhblNldENhbm9uaWNhbEFsaWFzfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uSXRlbUFkZGVkPXt0aGlzLm9uQWx0QWxpYXNBZGRlZH1cclxuICAgICAgICAgICAgICAgICAgICBvbkl0ZW1SZW1vdmVkPXt0aGlzLm9uQWx0QWxpYXNEZWxldGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHN1Z2dlc3Rpb25zTGlzdElkPVwibXhfQWxpYXNTZXR0aW5nc19hbHRSZWNvbW1lbmRhdGlvbnNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zTGFiZWw9e190KCdPdGhlciBwdWJsaXNoZWQgYWRkcmVzc2VzOicpfVxyXG4gICAgICAgICAgICAgICAgICAgIG5vSXRlbXNMYWJlbD17X3QoJ05vIG90aGVyIHB1Ymxpc2hlZCBhZGRyZXNzZXMgeWV0LCBhZGQgb25lIGJlbG93Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e190KCdOZXcgcHVibGlzaGVkIGFkZHJlc3MgKGUuZy4gI2FsaWFzOnNlcnZlciknKX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1NldHRpbmdzVGFiX3N1YmhlYWRpbmcgbXhfQWxpYXNTZXR0aW5nc19sb2NhbEFsaWFzSGVhZGVyJz57X3QoXCJMb2NhbCBBZGRyZXNzZXNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFwiU2V0IGFkZHJlc3NlcyBmb3IgdGhpcyByb29tIHNvIHVzZXJzIGNhbiBmaW5kIHRoaXMgcm9vbSB0aHJvdWdoIHlvdXIgaG9tZXNlcnZlciAoJShsb2NhbERvbWFpbilzKVwiLCB7bG9jYWxEb21haW59KX08L3A+XHJcbiAgICAgICAgICAgICAgICA8ZGV0YWlscyBvblRvZ2dsZT17dGhpcy5vbkxvY2FsQWxpYXNlc1RvZ2dsZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzdW1tYXJ5PnsgdGhpcy5zdGF0ZS5kZXRhaWxzT3BlbiA/IF90KCdTaG93IGxlc3MnKSA6IF90KFwiU2hvdyBtb3JlXCIpfTwvc3VtbWFyeT5cclxuICAgICAgICAgICAgICAgICAgICB7bG9jYWxBbGlhc2VzTGlzdH1cclxuICAgICAgICAgICAgICAgIDwvZGV0YWlscz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=