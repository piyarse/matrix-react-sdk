"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _SettingsStore = _interopRequireWildcard(require("../../../settings/SettingsStore"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Travis Ralston
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'UrlPreviewSettings',
  propTypes: {
    room: _propTypes.default.object
  },
  _onClickUserSettings: e => {
    e.preventDefault();
    e.stopPropagation();

    _dispatcher.default.dispatch({
      action: 'view_user_settings'
    });
  },
  render: function () {
    const SettingsFlag = sdk.getComponent("elements.SettingsFlag");
    const roomId = this.props.room.roomId;

    const isEncrypted = _MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(roomId);

    let previewsForAccount = null;
    let previewsForRoom = null;

    if (!isEncrypted) {
      // Only show account setting state and room state setting state in non-e2ee rooms where they apply
      const accountEnabled = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.ACCOUNT, "urlPreviewsEnabled");

      if (accountEnabled) {
        previewsForAccount = (0, _languageHandler._t)("You have <a>enabled</a> URL previews by default.", {}, {
          'a': sub => _react.default.createElement("a", {
            onClick: this._onClickUserSettings,
            href: ""
          }, sub)
        });
      } else if (accountEnabled) {
        previewsForAccount = (0, _languageHandler._t)("You have <a>disabled</a> URL previews by default.", {}, {
          'a': sub => _react.default.createElement("a", {
            onClick: this._onClickUserSettings,
            href: ""
          }, sub)
        });
      }

      if (_SettingsStore.default.canSetValue("urlPreviewsEnabled", roomId, "room")) {
        previewsForRoom = _react.default.createElement("label", null, _react.default.createElement(SettingsFlag, {
          name: "urlPreviewsEnabled",
          level: _SettingsStore.SettingLevel.ROOM,
          roomId: roomId,
          isExplicit: true
        }));
      } else {
        let str = (0, _languageHandler._td)("URL previews are enabled by default for participants in this room.");

        if (!_SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.ROOM, "urlPreviewsEnabled", roomId,
        /*explicit=*/
        true)) {
          str = (0, _languageHandler._td)("URL previews are disabled by default for participants in this room.");
        }

        previewsForRoom = _react.default.createElement("label", null, (0, _languageHandler._t)(str));
      }
    } else {
      previewsForAccount = (0, _languageHandler._t)("In encrypted rooms, like this one, URL previews are disabled by default to ensure that your " + "homeserver (where the previews are generated) cannot gather information about links you see in " + "this room.");
    }

    const previewsForRoomAccount = // in an e2ee room we use a special key to enforce per-room opt-in
    _react.default.createElement(SettingsFlag, {
      name: isEncrypted ? 'urlPreviewsEnabled_e2ee' : 'urlPreviewsEnabled',
      level: _SettingsStore.SettingLevel.ROOM_ACCOUNT,
      roomId: roomId
    });

    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, (0, _languageHandler._t)('When someone puts a URL in their message, a URL preview can be shown to give more ' + 'information about that link such as the title, description, and an image from the website.')), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, previewsForAccount), previewsForRoom, _react.default.createElement("label", null, previewsForRoomAccount));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21fc2V0dGluZ3MvVXJsUHJldmlld1NldHRpbmdzLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwicm9vbSIsIlByb3BUeXBlcyIsIm9iamVjdCIsIl9vbkNsaWNrVXNlclNldHRpbmdzIiwiZSIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJyZW5kZXIiLCJTZXR0aW5nc0ZsYWciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJyb29tSWQiLCJwcm9wcyIsImlzRW5jcnlwdGVkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiaXNSb29tRW5jcnlwdGVkIiwicHJldmlld3NGb3JBY2NvdW50IiwicHJldmlld3NGb3JSb29tIiwiYWNjb3VudEVuYWJsZWQiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWVBdCIsIlNldHRpbmdMZXZlbCIsIkFDQ09VTlQiLCJzdWIiLCJjYW5TZXRWYWx1ZSIsIlJPT00iLCJzdHIiLCJwcmV2aWV3c0ZvclJvb21BY2NvdW50IiwiUk9PTV9BQ0NPVU5UIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztlQTZCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxvQkFEZTtBQUc1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLElBQUksRUFBRUMsbUJBQVVDO0FBRFQsR0FIaUI7QUFPNUJDLEVBQUFBLG9CQUFvQixFQUFHQyxDQUFELElBQU87QUFDekJBLElBQUFBLENBQUMsQ0FBQ0MsY0FBRjtBQUNBRCxJQUFBQSxDQUFDLENBQUNFLGVBQUY7O0FBQ0FDLHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjtBQUNILEdBWDJCO0FBYTVCQyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFlBQVksR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFyQjtBQUNBLFVBQU1DLE1BQU0sR0FBRyxLQUFLQyxLQUFMLENBQVdmLElBQVgsQ0FBZ0JjLE1BQS9COztBQUNBLFVBQU1FLFdBQVcsR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsZUFBdEIsQ0FBc0NMLE1BQXRDLENBQXBCOztBQUVBLFFBQUlNLGtCQUFrQixHQUFHLElBQXpCO0FBQ0EsUUFBSUMsZUFBZSxHQUFHLElBQXRCOztBQUVBLFFBQUksQ0FBQ0wsV0FBTCxFQUFrQjtBQUNkO0FBQ0EsWUFBTU0sY0FBYyxHQUFHQyx1QkFBY0MsVUFBZCxDQUF5QkMsNEJBQWFDLE9BQXRDLEVBQStDLG9CQUEvQyxDQUF2Qjs7QUFDQSxVQUFJSixjQUFKLEVBQW9CO0FBQ2hCRixRQUFBQSxrQkFBa0IsR0FDZCx5QkFBRyxrREFBSCxFQUF1RCxFQUF2RCxFQUEyRDtBQUN2RCxlQUFNTyxHQUFELElBQU87QUFBRyxZQUFBLE9BQU8sRUFBRSxLQUFLeEIsb0JBQWpCO0FBQXVDLFlBQUEsSUFBSSxFQUFDO0FBQTVDLGFBQWlEd0IsR0FBakQ7QUFEMkMsU0FBM0QsQ0FESjtBQUtILE9BTkQsTUFNTyxJQUFJTCxjQUFKLEVBQW9CO0FBQ3ZCRixRQUFBQSxrQkFBa0IsR0FDZCx5QkFBRyxtREFBSCxFQUF3RCxFQUF4RCxFQUE0RDtBQUN4RCxlQUFNTyxHQUFELElBQU87QUFBRyxZQUFBLE9BQU8sRUFBRSxLQUFLeEIsb0JBQWpCO0FBQXVDLFlBQUEsSUFBSSxFQUFDO0FBQTVDLGFBQWlEd0IsR0FBakQ7QUFENEMsU0FBNUQsQ0FESjtBQUtIOztBQUVELFVBQUlKLHVCQUFjSyxXQUFkLENBQTBCLG9CQUExQixFQUFnRGQsTUFBaEQsRUFBd0QsTUFBeEQsQ0FBSixFQUFxRTtBQUNqRU8sUUFBQUEsZUFBZSxHQUNYLDRDQUNJLDZCQUFDLFlBQUQ7QUFBYyxVQUFBLElBQUksRUFBQyxvQkFBbkI7QUFDYyxVQUFBLEtBQUssRUFBRUksNEJBQWFJLElBRGxDO0FBRWMsVUFBQSxNQUFNLEVBQUVmLE1BRnRCO0FBR2MsVUFBQSxVQUFVLEVBQUU7QUFIMUIsVUFESixDQURKO0FBUUgsT0FURCxNQVNPO0FBQ0gsWUFBSWdCLEdBQUcsR0FBRywwQkFBSSxvRUFBSixDQUFWOztBQUNBLFlBQUksQ0FBQ1AsdUJBQWNDLFVBQWQsQ0FBeUJDLDRCQUFhSSxJQUF0QyxFQUE0QyxvQkFBNUMsRUFBa0VmLE1BQWxFO0FBQTBFO0FBQWEsWUFBdkYsQ0FBTCxFQUFtRztBQUMvRmdCLFVBQUFBLEdBQUcsR0FBRywwQkFBSSxxRUFBSixDQUFOO0FBQ0g7O0FBQ0RULFFBQUFBLGVBQWUsR0FBSSw0Q0FBUyx5QkFBR1MsR0FBSCxDQUFULENBQW5CO0FBQ0g7QUFDSixLQWpDRCxNQWlDTztBQUNIVixNQUFBQSxrQkFBa0IsR0FDZCx5QkFBRyxpR0FDQyxpR0FERCxHQUVDLFlBRkosQ0FESjtBQUtIOztBQUVELFVBQU1XLHNCQUFzQixHQUFLO0FBQzdCLGlDQUFDLFlBQUQ7QUFBYyxNQUFBLElBQUksRUFBRWYsV0FBVyxHQUFHLHlCQUFILEdBQStCLG9CQUE5RDtBQUNjLE1BQUEsS0FBSyxFQUFFUyw0QkFBYU8sWUFEbEM7QUFFYyxNQUFBLE1BQU0sRUFBRWxCO0FBRnRCLE1BREo7O0FBTUEsV0FDSSwwQ0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTSx5QkFBRyx1RkFDRCw0RkFERixDQUROLENBREosRUFLSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTU0sa0JBRE4sQ0FMSixFQVFNQyxlQVJOLEVBU0ksNENBQVNVLHNCQUFULENBVEosQ0FESjtBQWFIO0FBakYyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVHJhdmlzIFJhbHN0b25cclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCB7IF90LCBfdGQgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdVcmxQcmV2aWV3U2V0dGluZ3MnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHJvb206IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrVXNlclNldHRpbmdzOiAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld191c2VyX3NldHRpbmdzJ30pO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFNldHRpbmdzRmxhZyA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TZXR0aW5nc0ZsYWdcIik7XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gdGhpcy5wcm9wcy5yb29tLnJvb21JZDtcclxuICAgICAgICBjb25zdCBpc0VuY3J5cHRlZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc1Jvb21FbmNyeXB0ZWQocm9vbUlkKTtcclxuXHJcbiAgICAgICAgbGV0IHByZXZpZXdzRm9yQWNjb3VudCA9IG51bGw7XHJcbiAgICAgICAgbGV0IHByZXZpZXdzRm9yUm9vbSA9IG51bGw7XHJcblxyXG4gICAgICAgIGlmICghaXNFbmNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy8gT25seSBzaG93IGFjY291bnQgc2V0dGluZyBzdGF0ZSBhbmQgcm9vbSBzdGF0ZSBzZXR0aW5nIHN0YXRlIGluIG5vbi1lMmVlIHJvb21zIHdoZXJlIHRoZXkgYXBwbHlcclxuICAgICAgICAgICAgY29uc3QgYWNjb3VudEVuYWJsZWQgPSBTZXR0aW5nc1N0b3JlLmdldFZhbHVlQXQoU2V0dGluZ0xldmVsLkFDQ09VTlQsIFwidXJsUHJldmlld3NFbmFibGVkXCIpO1xyXG4gICAgICAgICAgICBpZiAoYWNjb3VudEVuYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIHByZXZpZXdzRm9yQWNjb3VudCA9IChcclxuICAgICAgICAgICAgICAgICAgICBfdChcIllvdSBoYXZlIDxhPmVuYWJsZWQ8L2E+IFVSTCBwcmV2aWV3cyBieSBkZWZhdWx0LlwiLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnYSc6IChzdWIpPT48YSBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrVXNlclNldHRpbmdzfSBocmVmPScnPnsgc3ViIH08L2E+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGFjY291bnRFbmFibGVkKSB7XHJcbiAgICAgICAgICAgICAgICBwcmV2aWV3c0ZvckFjY291bnQgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgX3QoXCJZb3UgaGF2ZSA8YT5kaXNhYmxlZDwvYT4gVVJMIHByZXZpZXdzIGJ5IGRlZmF1bHQuXCIsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdhJzogKHN1Yik9PjxhIG9uQ2xpY2s9e3RoaXMuX29uQ2xpY2tVc2VyU2V0dGluZ3N9IGhyZWY9Jyc+eyBzdWIgfTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmNhblNldFZhbHVlKFwidXJsUHJldmlld3NFbmFibGVkXCIsIHJvb21JZCwgXCJyb29tXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBwcmV2aWV3c0ZvclJvb20gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8U2V0dGluZ3NGbGFnIG5hbWU9XCJ1cmxQcmV2aWV3c0VuYWJsZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsPXtTZXR0aW5nTGV2ZWwuUk9PTX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tSWQ9e3Jvb21JZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0V4cGxpY2l0PXt0cnVlfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHN0ciA9IF90ZChcIlVSTCBwcmV2aWV3cyBhcmUgZW5hYmxlZCBieSBkZWZhdWx0IGZvciBwYXJ0aWNpcGFudHMgaW4gdGhpcyByb29tLlwiKTtcclxuICAgICAgICAgICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZUF0KFNldHRpbmdMZXZlbC5ST09NLCBcInVybFByZXZpZXdzRW5hYmxlZFwiLCByb29tSWQsIC8qZXhwbGljaXQ9Ki90cnVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0ciA9IF90ZChcIlVSTCBwcmV2aWV3cyBhcmUgZGlzYWJsZWQgYnkgZGVmYXVsdCBmb3IgcGFydGljaXBhbnRzIGluIHRoaXMgcm9vbS5cIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBwcmV2aWV3c0ZvclJvb20gPSAoPGxhYmVsPnsgX3Qoc3RyKSB9PC9sYWJlbD4pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJldmlld3NGb3JBY2NvdW50ID0gKFxyXG4gICAgICAgICAgICAgICAgX3QoXCJJbiBlbmNyeXB0ZWQgcm9vbXMsIGxpa2UgdGhpcyBvbmUsIFVSTCBwcmV2aWV3cyBhcmUgZGlzYWJsZWQgYnkgZGVmYXVsdCB0byBlbnN1cmUgdGhhdCB5b3VyIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImhvbWVzZXJ2ZXIgKHdoZXJlIHRoZSBwcmV2aWV3cyBhcmUgZ2VuZXJhdGVkKSBjYW5ub3QgZ2F0aGVyIGluZm9ybWF0aW9uIGFib3V0IGxpbmtzIHlvdSBzZWUgaW4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwidGhpcyByb29tLlwiKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcHJldmlld3NGb3JSb29tQWNjb3VudCA9ICggLy8gaW4gYW4gZTJlZSByb29tIHdlIHVzZSBhIHNwZWNpYWwga2V5IHRvIGVuZm9yY2UgcGVyLXJvb20gb3B0LWluXHJcbiAgICAgICAgICAgIDxTZXR0aW5nc0ZsYWcgbmFtZT17aXNFbmNyeXB0ZWQgPyAndXJsUHJldmlld3NFbmFibGVkX2UyZWUnIDogJ3VybFByZXZpZXdzRW5hYmxlZCd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbGV2ZWw9e1NldHRpbmdMZXZlbC5ST09NX0FDQ09VTlR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcm9vbUlkPXtyb29tSWR9IC8+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnV2hlbiBzb21lb25lIHB1dHMgYSBVUkwgaW4gdGhlaXIgbWVzc2FnZSwgYSBVUkwgcHJldmlldyBjYW4gYmUgc2hvd24gdG8gZ2l2ZSBtb3JlICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnaW5mb3JtYXRpb24gYWJvdXQgdGhhdCBsaW5rIHN1Y2ggYXMgdGhlIHRpdGxlLCBkZXNjcmlwdGlvbiwgYW5kIGFuIGltYWdlIGZyb20gdGhlIHdlYnNpdGUuJykgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHQnPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgcHJldmlld3NGb3JBY2NvdW50IH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgeyBwcmV2aWV3c0ZvclJvb20gfVxyXG4gICAgICAgICAgICAgICAgPGxhYmVsPnsgcHJldmlld3NGb3JSb29tQWNjb3VudCB9PC9sYWJlbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=