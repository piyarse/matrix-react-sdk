"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../.."));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class InlineTermsAgreement extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_togglePolicy", index => {
      const policies = JSON.parse(JSON.stringify(this.state.policies)); // deep & cheap clone

      policies[index].checked = !policies[index].checked;
      this.setState({
        policies
      });
    });
    (0, _defineProperty2.default)(this, "_onContinue", () => {
      const hasUnchecked = !!this.state.policies.some(p => !p.checked);
      if (hasUnchecked) return;
      this.setState({
        busy: true
      });
      this.props.onFinished(this.state.policies.map(p => p.url));
    });
    this.state = {
      policies: [],
      busy: false
    };
  }

  componentDidMount() {
    // Build all the terms the user needs to accept
    const policies = []; // { checked, url, name }

    for (const servicePolicies of this.props.policiesAndServicePairs) {
      const availablePolicies = Object.values(servicePolicies.policies);

      for (const policy of availablePolicies) {
        const language = (0, _languageHandler.pickBestLanguage)(Object.keys(policy).filter(p => p !== 'version'));
        const renderablePolicy = {
          checked: false,
          url: policy[language].url,
          name: policy[language].name
        };
        policies.push(renderablePolicy);
      }
    }

    this.setState({
      policies
    });
  }

  _renderCheckboxes() {
    const rendered = [];

    for (let i = 0; i < this.state.policies.length; i++) {
      const policy = this.state.policies[i];
      const introText = (0, _languageHandler._t)("Accept <policyLink /> to continue:", {}, {
        policyLink: () => {
          return _react.default.createElement("a", {
            href: policy.url,
            rel: "noreferrer noopener",
            target: "_blank"
          }, policy.name, _react.default.createElement("span", {
            className: "mx_InlineTermsAgreement_link"
          }));
        }
      });
      rendered.push(_react.default.createElement("div", {
        key: i,
        className: "mx_InlineTermsAgreement_cbContainer"
      }, _react.default.createElement("div", null, introText), _react.default.createElement("div", {
        className: "mx_InlineTermsAgreement_checkbox"
      }, _react.default.createElement("input", {
        type: "checkbox",
        onChange: () => this._togglePolicy(i),
        checked: policy.checked
      }), (0, _languageHandler._t)("Accept"))));
    }

    return rendered;
  }

  render() {
    const AccessibleButton = sdk.getComponent("views.elements.AccessibleButton");
    const hasUnchecked = !!this.state.policies.some(p => !p.checked);
    return _react.default.createElement("div", null, this.props.introElement, this._renderCheckboxes(), _react.default.createElement(AccessibleButton, {
      onClick: this._onContinue,
      disabled: hasUnchecked || this.state.busy,
      kind: "primary_sm"
    }, (0, _languageHandler._t)("Continue")));
  }

}

exports.default = InlineTermsAgreement;
(0, _defineProperty2.default)(InlineTermsAgreement, "propTypes", {
  policiesAndServicePairs: _propTypes.default.array.isRequired,
  // array of service/policy pairs
  agreedUrls: _propTypes.default.array.isRequired,
  // array of URLs the user has accepted
  onFinished: _propTypes.default.func.isRequired,
  // takes an argument of accepted URLs
  introElement: _propTypes.default.node
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Rlcm1zL0lubGluZVRlcm1zQWdyZWVtZW50LmpzIl0sIm5hbWVzIjpbIklubGluZVRlcm1zQWdyZWVtZW50IiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsImluZGV4IiwicG9saWNpZXMiLCJKU09OIiwicGFyc2UiLCJzdHJpbmdpZnkiLCJzdGF0ZSIsImNoZWNrZWQiLCJzZXRTdGF0ZSIsImhhc1VuY2hlY2tlZCIsInNvbWUiLCJwIiwiYnVzeSIsInByb3BzIiwib25GaW5pc2hlZCIsIm1hcCIsInVybCIsImNvbXBvbmVudERpZE1vdW50Iiwic2VydmljZVBvbGljaWVzIiwicG9saWNpZXNBbmRTZXJ2aWNlUGFpcnMiLCJhdmFpbGFibGVQb2xpY2llcyIsIk9iamVjdCIsInZhbHVlcyIsInBvbGljeSIsImxhbmd1YWdlIiwia2V5cyIsImZpbHRlciIsInJlbmRlcmFibGVQb2xpY3kiLCJuYW1lIiwicHVzaCIsIl9yZW5kZXJDaGVja2JveGVzIiwicmVuZGVyZWQiLCJpIiwibGVuZ3RoIiwiaW50cm9UZXh0IiwicG9saWN5TGluayIsIl90b2dnbGVQb2xpY3kiLCJyZW5kZXIiLCJBY2Nlc3NpYmxlQnV0dG9uIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiaW50cm9FbGVtZW50IiwiX29uQ29udGludWUiLCJQcm9wVHlwZXMiLCJhcnJheSIsImlzUmVxdWlyZWQiLCJhZ3JlZWRVcmxzIiwiZnVuYyIsIm5vZGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBbkJBOzs7Ozs7Ozs7Ozs7Ozs7QUFxQmUsTUFBTUEsb0JBQU4sU0FBbUNDLGVBQU1DLFNBQXpDLENBQW1EO0FBUTlEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLHlEQTRCR0MsS0FBRCxJQUFXO0FBQ3ZCLFlBQU1DLFFBQVEsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsU0FBTCxDQUFlLEtBQUtDLEtBQUwsQ0FBV0osUUFBMUIsQ0FBWCxDQUFqQixDQUR1QixDQUMyQzs7QUFDbEVBLE1BQUFBLFFBQVEsQ0FBQ0QsS0FBRCxDQUFSLENBQWdCTSxPQUFoQixHQUEwQixDQUFDTCxRQUFRLENBQUNELEtBQUQsQ0FBUixDQUFnQk0sT0FBM0M7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFBQ04sUUFBQUE7QUFBRCxPQUFkO0FBQ0gsS0FoQ2E7QUFBQSx1REFrQ0EsTUFBTTtBQUNoQixZQUFNTyxZQUFZLEdBQUcsQ0FBQyxDQUFDLEtBQUtILEtBQUwsQ0FBV0osUUFBWCxDQUFvQlEsSUFBcEIsQ0FBeUJDLENBQUMsSUFBSSxDQUFDQSxDQUFDLENBQUNKLE9BQWpDLENBQXZCO0FBQ0EsVUFBSUUsWUFBSixFQUFrQjtBQUVsQixXQUFLRCxRQUFMLENBQWM7QUFBQ0ksUUFBQUEsSUFBSSxFQUFFO0FBQVAsT0FBZDtBQUNBLFdBQUtDLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixLQUFLUixLQUFMLENBQVdKLFFBQVgsQ0FBb0JhLEdBQXBCLENBQXdCSixDQUFDLElBQUlBLENBQUMsQ0FBQ0ssR0FBL0IsQ0FBdEI7QUFDSCxLQXhDYTtBQUdWLFNBQUtWLEtBQUwsR0FBYTtBQUNUSixNQUFBQSxRQUFRLEVBQUUsRUFERDtBQUVUVSxNQUFBQSxJQUFJLEVBQUU7QUFGRyxLQUFiO0FBSUg7O0FBRURLLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCO0FBQ0EsVUFBTWYsUUFBUSxHQUFHLEVBQWpCLENBRmdCLENBRUs7O0FBQ3JCLFNBQUssTUFBTWdCLGVBQVgsSUFBOEIsS0FBS0wsS0FBTCxDQUFXTSx1QkFBekMsRUFBa0U7QUFDOUQsWUFBTUMsaUJBQWlCLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjSixlQUFlLENBQUNoQixRQUE5QixDQUExQjs7QUFDQSxXQUFLLE1BQU1xQixNQUFYLElBQXFCSCxpQkFBckIsRUFBd0M7QUFDcEMsY0FBTUksUUFBUSxHQUFHLHVDQUFpQkgsTUFBTSxDQUFDSSxJQUFQLENBQVlGLE1BQVosRUFBb0JHLE1BQXBCLENBQTJCZixDQUFDLElBQUlBLENBQUMsS0FBSyxTQUF0QyxDQUFqQixDQUFqQjtBQUNBLGNBQU1nQixnQkFBZ0IsR0FBRztBQUNyQnBCLFVBQUFBLE9BQU8sRUFBRSxLQURZO0FBRXJCUyxVQUFBQSxHQUFHLEVBQUVPLE1BQU0sQ0FBQ0MsUUFBRCxDQUFOLENBQWlCUixHQUZEO0FBR3JCWSxVQUFBQSxJQUFJLEVBQUVMLE1BQU0sQ0FBQ0MsUUFBRCxDQUFOLENBQWlCSTtBQUhGLFNBQXpCO0FBS0ExQixRQUFBQSxRQUFRLENBQUMyQixJQUFULENBQWNGLGdCQUFkO0FBQ0g7QUFDSjs7QUFFRCxTQUFLbkIsUUFBTCxDQUFjO0FBQUNOLE1BQUFBO0FBQUQsS0FBZDtBQUNIOztBQWdCRDRCLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFVBQU1DLFFBQVEsR0FBRyxFQUFqQjs7QUFDQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBSzFCLEtBQUwsQ0FBV0osUUFBWCxDQUFvQitCLE1BQXhDLEVBQWdERCxDQUFDLEVBQWpELEVBQXFEO0FBQ2pELFlBQU1ULE1BQU0sR0FBRyxLQUFLakIsS0FBTCxDQUFXSixRQUFYLENBQW9COEIsQ0FBcEIsQ0FBZjtBQUNBLFlBQU1FLFNBQVMsR0FBRyx5QkFDZCxvQ0FEYyxFQUN3QixFQUR4QixFQUM0QjtBQUN0Q0MsUUFBQUEsVUFBVSxFQUFFLE1BQU07QUFDZCxpQkFDSTtBQUFHLFlBQUEsSUFBSSxFQUFFWixNQUFNLENBQUNQLEdBQWhCO0FBQXFCLFlBQUEsR0FBRyxFQUFDLHFCQUF6QjtBQUErQyxZQUFBLE1BQU0sRUFBQztBQUF0RCxhQUNLTyxNQUFNLENBQUNLLElBRFosRUFFSTtBQUFNLFlBQUEsU0FBUyxFQUFDO0FBQWhCLFlBRkosQ0FESjtBQU1IO0FBUnFDLE9BRDVCLENBQWxCO0FBWUFHLE1BQUFBLFFBQVEsQ0FBQ0YsSUFBVCxDQUNJO0FBQUssUUFBQSxHQUFHLEVBQUVHLENBQVY7QUFBYSxRQUFBLFNBQVMsRUFBQztBQUF2QixTQUNJLDBDQUFNRSxTQUFOLENBREosRUFFSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFPLFFBQUEsSUFBSSxFQUFDLFVBQVo7QUFBdUIsUUFBQSxRQUFRLEVBQUUsTUFBTSxLQUFLRSxhQUFMLENBQW1CSixDQUFuQixDQUF2QztBQUE4RCxRQUFBLE9BQU8sRUFBRVQsTUFBTSxDQUFDaEI7QUFBOUUsUUFESixFQUVLLHlCQUFHLFFBQUgsQ0FGTCxDQUZKLENBREo7QUFTSDs7QUFDRCxXQUFPd0IsUUFBUDtBQUNIOztBQUVETSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUF6QjtBQUNBLFVBQU0vQixZQUFZLEdBQUcsQ0FBQyxDQUFDLEtBQUtILEtBQUwsQ0FBV0osUUFBWCxDQUFvQlEsSUFBcEIsQ0FBeUJDLENBQUMsSUFBSSxDQUFDQSxDQUFDLENBQUNKLE9BQWpDLENBQXZCO0FBRUEsV0FDSSwwQ0FDSyxLQUFLTSxLQUFMLENBQVc0QixZQURoQixFQUVLLEtBQUtYLGlCQUFMLEVBRkwsRUFHSSw2QkFBQyxnQkFBRDtBQUNJLE1BQUEsT0FBTyxFQUFFLEtBQUtZLFdBRGxCO0FBRUksTUFBQSxRQUFRLEVBQUVqQyxZQUFZLElBQUksS0FBS0gsS0FBTCxDQUFXTSxJQUZ6QztBQUdJLE1BQUEsSUFBSSxFQUFDO0FBSFQsT0FLSyx5QkFBRyxVQUFILENBTEwsQ0FISixDQURKO0FBYUg7O0FBaEc2RDs7OzhCQUE3Q2Ysb0IsZUFDRTtBQUNmc0IsRUFBQUEsdUJBQXVCLEVBQUV3QixtQkFBVUMsS0FBVixDQUFnQkMsVUFEMUI7QUFDc0M7QUFDckRDLEVBQUFBLFVBQVUsRUFBRUgsbUJBQVVDLEtBQVYsQ0FBZ0JDLFVBRmI7QUFFeUI7QUFDeEMvQixFQUFBQSxVQUFVLEVBQUU2QixtQkFBVUksSUFBVixDQUFlRixVQUhaO0FBR3dCO0FBQ3ZDSixFQUFBQSxZQUFZLEVBQUVFLG1CQUFVSztBQUpULEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xyXG5pbXBvcnQge190LCBwaWNrQmVzdExhbmd1YWdlfSBmcm9tIFwiLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIElubGluZVRlcm1zQWdyZWVtZW50IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgcG9saWNpZXNBbmRTZXJ2aWNlUGFpcnM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLCAvLyBhcnJheSBvZiBzZXJ2aWNlL3BvbGljeSBwYWlyc1xyXG4gICAgICAgIGFncmVlZFVybHM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLCAvLyBhcnJheSBvZiBVUkxzIHRoZSB1c2VyIGhhcyBhY2NlcHRlZFxyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsIC8vIHRha2VzIGFuIGFyZ3VtZW50IG9mIGFjY2VwdGVkIFVSTHNcclxuICAgICAgICBpbnRyb0VsZW1lbnQ6IFByb3BUeXBlcy5ub2RlLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwb2xpY2llczogW10sXHJcbiAgICAgICAgICAgIGJ1c3k6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgLy8gQnVpbGQgYWxsIHRoZSB0ZXJtcyB0aGUgdXNlciBuZWVkcyB0byBhY2NlcHRcclxuICAgICAgICBjb25zdCBwb2xpY2llcyA9IFtdOyAvLyB7IGNoZWNrZWQsIHVybCwgbmFtZSB9XHJcbiAgICAgICAgZm9yIChjb25zdCBzZXJ2aWNlUG9saWNpZXMgb2YgdGhpcy5wcm9wcy5wb2xpY2llc0FuZFNlcnZpY2VQYWlycykge1xyXG4gICAgICAgICAgICBjb25zdCBhdmFpbGFibGVQb2xpY2llcyA9IE9iamVjdC52YWx1ZXMoc2VydmljZVBvbGljaWVzLnBvbGljaWVzKTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBwb2xpY3kgb2YgYXZhaWxhYmxlUG9saWNpZXMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGxhbmd1YWdlID0gcGlja0Jlc3RMYW5ndWFnZShPYmplY3Qua2V5cyhwb2xpY3kpLmZpbHRlcihwID0+IHAgIT09ICd2ZXJzaW9uJykpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVuZGVyYWJsZVBvbGljeSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBjaGVja2VkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IHBvbGljeVtsYW5ndWFnZV0udXJsLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHBvbGljeVtsYW5ndWFnZV0ubmFtZSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBwb2xpY2llcy5wdXNoKHJlbmRlcmFibGVQb2xpY3kpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtwb2xpY2llc30pO1xyXG4gICAgfVxyXG5cclxuICAgIF90b2dnbGVQb2xpY3kgPSAoaW5kZXgpID0+IHtcclxuICAgICAgICBjb25zdCBwb2xpY2llcyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5zdGF0ZS5wb2xpY2llcykpOyAvLyBkZWVwICYgY2hlYXAgY2xvbmVcclxuICAgICAgICBwb2xpY2llc1tpbmRleF0uY2hlY2tlZCA9ICFwb2xpY2llc1tpbmRleF0uY2hlY2tlZDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtwb2xpY2llc30pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25Db250aW51ZSA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBoYXNVbmNoZWNrZWQgPSAhIXRoaXMuc3RhdGUucG9saWNpZXMuc29tZShwID0+ICFwLmNoZWNrZWQpO1xyXG4gICAgICAgIGlmIChoYXNVbmNoZWNrZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogdHJ1ZX0pO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0aGlzLnN0YXRlLnBvbGljaWVzLm1hcChwID0+IHAudXJsKSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9yZW5kZXJDaGVja2JveGVzKCkge1xyXG4gICAgICAgIGNvbnN0IHJlbmRlcmVkID0gW107XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnN0YXRlLnBvbGljaWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBvbGljeSA9IHRoaXMuc3RhdGUucG9saWNpZXNbaV07XHJcbiAgICAgICAgICAgIGNvbnN0IGludHJvVGV4dCA9IF90KFxyXG4gICAgICAgICAgICAgICAgXCJBY2NlcHQgPHBvbGljeUxpbmsgLz4gdG8gY29udGludWU6XCIsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9saWN5TGluazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj17cG9saWN5LnVybH0gcmVsPSdub3JlZmVycmVyIG5vb3BlbmVyJyB0YXJnZXQ9J19ibGFuayc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3BvbGljeS5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfSW5saW5lVGVybXNBZ3JlZW1lbnRfbGluaycgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmVuZGVyZWQucHVzaChcclxuICAgICAgICAgICAgICAgIDxkaXYga2V5PXtpfSBjbGFzc05hbWU9J214X0lubGluZVRlcm1zQWdyZWVtZW50X2NiQ29udGFpbmVyJz5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PntpbnRyb1RleHR9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X0lubGluZVRlcm1zQWdyZWVtZW50X2NoZWNrYm94Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9J2NoZWNrYm94JyBvbkNoYW5nZT17KCkgPT4gdGhpcy5fdG9nZ2xlUG9saWN5KGkpfSBjaGVja2VkPXtwb2xpY3kuY2hlY2tlZH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiQWNjZXB0XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVuZGVyZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvblwiKTtcclxuICAgICAgICBjb25zdCBoYXNVbmNoZWNrZWQgPSAhIXRoaXMuc3RhdGUucG9saWNpZXMuc29tZShwID0+ICFwLmNoZWNrZWQpO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAge3RoaXMucHJvcHMuaW50cm9FbGVtZW50fVxyXG4gICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlckNoZWNrYm94ZXMoKX1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25Db250aW51ZX1cclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17aGFzVW5jaGVja2VkIHx8IHRoaXMuc3RhdGUuYnVzeX1cclxuICAgICAgICAgICAgICAgICAgICBraW5kPVwicHJpbWFyeV9zbVwiXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiQ29udGludWVcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19