"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _HtmlUtils = require("../../../HtmlUtils");

var _languageHandler = require("../../../languageHandler");

var _FormattingUtils = require("../../../utils/FormattingUtils");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class ReactionsRowButtonTooltip extends _react.default.PureComponent {
  render() {
    const Tooltip = sdk.getComponent('elements.Tooltip');
    const {
      content,
      reactionEvents,
      mxEvent,
      visible
    } = this.props;

    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(mxEvent.getRoomId());

    let tooltipLabel;

    if (room) {
      const senders = [];

      for (const reactionEvent of reactionEvents) {
        const member = room.getMember(reactionEvent.getSender());
        const name = member ? member.name : reactionEvent.getSender();
        senders.push(name);
      }

      const shortName = (0, _HtmlUtils.unicodeToShortcode)(content);
      tooltipLabel = _react.default.createElement("div", null, (0, _languageHandler._t)("<reactors/><reactedWith>reacted with %(shortName)s</reactedWith>", {
        shortName
      }, {
        reactors: () => {
          return _react.default.createElement("div", {
            className: "mx_ReactionsRowButtonTooltip_senders"
          }, (0, _FormattingUtils.formatCommaSeparatedList)(senders, 6));
        },
        reactedWith: sub => {
          if (!shortName) {
            return null;
          }

          return _react.default.createElement("div", {
            className: "mx_ReactionsRowButtonTooltip_reactedWith"
          }, sub);
        }
      }));
    }

    let tooltip;

    if (tooltipLabel) {
      tooltip = _react.default.createElement(Tooltip, {
        tooltipClassName: "mx_Tooltip_timeline",
        visible: visible,
        label: tooltipLabel
      });
    }

    return tooltip;
  }

}

exports.default = ReactionsRowButtonTooltip;
(0, _defineProperty2.default)(ReactionsRowButtonTooltip, "propTypes", {
  // The event we're displaying reactions for
  mxEvent: _propTypes.default.object.isRequired,
  // The reaction content / key / emoji
  content: _propTypes.default.string.isRequired,
  // A Set of Martix reaction events for this key
  reactionEvents: _propTypes.default.object.isRequired,
  visible: _propTypes.default.bool.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL1JlYWN0aW9uc1Jvd0J1dHRvblRvb2x0aXAuanMiXSwibmFtZXMiOlsiUmVhY3Rpb25zUm93QnV0dG9uVG9vbHRpcCIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsInJlbmRlciIsIlRvb2x0aXAiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjb250ZW50IiwicmVhY3Rpb25FdmVudHMiLCJteEV2ZW50IiwidmlzaWJsZSIsInByb3BzIiwicm9vbSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFJvb20iLCJnZXRSb29tSWQiLCJ0b29sdGlwTGFiZWwiLCJzZW5kZXJzIiwicmVhY3Rpb25FdmVudCIsIm1lbWJlciIsImdldE1lbWJlciIsImdldFNlbmRlciIsIm5hbWUiLCJwdXNoIiwic2hvcnROYW1lIiwicmVhY3RvcnMiLCJyZWFjdGVkV2l0aCIsInN1YiIsInRvb2x0aXAiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7OztBQXlCZSxNQUFNQSx5QkFBTixTQUF3Q0MsZUFBTUMsYUFBOUMsQ0FBNEQ7QUFXdkVDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFVBQU07QUFBRUMsTUFBQUEsT0FBRjtBQUFXQyxNQUFBQSxjQUFYO0FBQTJCQyxNQUFBQSxPQUEzQjtBQUFvQ0MsTUFBQUE7QUFBcEMsUUFBZ0QsS0FBS0MsS0FBM0Q7O0FBRUEsVUFBTUMsSUFBSSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxPQUF0QixDQUE4Qk4sT0FBTyxDQUFDTyxTQUFSLEVBQTlCLENBQWI7O0FBQ0EsUUFBSUMsWUFBSjs7QUFDQSxRQUFJTCxJQUFKLEVBQVU7QUFDTixZQUFNTSxPQUFPLEdBQUcsRUFBaEI7O0FBQ0EsV0FBSyxNQUFNQyxhQUFYLElBQTRCWCxjQUE1QixFQUE0QztBQUN4QyxjQUFNWSxNQUFNLEdBQUdSLElBQUksQ0FBQ1MsU0FBTCxDQUFlRixhQUFhLENBQUNHLFNBQWQsRUFBZixDQUFmO0FBQ0EsY0FBTUMsSUFBSSxHQUFHSCxNQUFNLEdBQUdBLE1BQU0sQ0FBQ0csSUFBVixHQUFpQkosYUFBYSxDQUFDRyxTQUFkLEVBQXBDO0FBQ0FKLFFBQUFBLE9BQU8sQ0FBQ00sSUFBUixDQUFhRCxJQUFiO0FBQ0g7O0FBQ0QsWUFBTUUsU0FBUyxHQUFHLG1DQUFtQmxCLE9BQW5CLENBQWxCO0FBQ0FVLE1BQUFBLFlBQVksR0FBRywwQ0FBTSx5QkFDakIsa0VBRGlCLEVBRWpCO0FBQ0lRLFFBQUFBO0FBREosT0FGaUIsRUFLakI7QUFDSUMsUUFBQUEsUUFBUSxFQUFFLE1BQU07QUFDWixpQkFBTztBQUFLLFlBQUEsU0FBUyxFQUFDO0FBQWYsYUFDRiwrQ0FBeUJSLE9BQXpCLEVBQWtDLENBQWxDLENBREUsQ0FBUDtBQUdILFNBTEw7QUFNSVMsUUFBQUEsV0FBVyxFQUFHQyxHQUFELElBQVM7QUFDbEIsY0FBSSxDQUFDSCxTQUFMLEVBQWdCO0FBQ1osbUJBQU8sSUFBUDtBQUNIOztBQUNELGlCQUFPO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNGRyxHQURFLENBQVA7QUFHSDtBQWJMLE9BTGlCLENBQU4sQ0FBZjtBQXFCSDs7QUFFRCxRQUFJQyxPQUFKOztBQUNBLFFBQUlaLFlBQUosRUFBa0I7QUFDZFksTUFBQUEsT0FBTyxHQUFHLDZCQUFDLE9BQUQ7QUFDTixRQUFBLGdCQUFnQixFQUFDLHFCQURYO0FBRU4sUUFBQSxPQUFPLEVBQUVuQixPQUZIO0FBR04sUUFBQSxLQUFLLEVBQUVPO0FBSEQsUUFBVjtBQUtIOztBQUVELFdBQU9ZLE9BQVA7QUFDSDs7QUExRHNFOzs7OEJBQXREN0IseUIsZUFDRTtBQUNmO0FBQ0FTLEVBQUFBLE9BQU8sRUFBRXFCLG1CQUFVQyxNQUFWLENBQWlCQyxVQUZYO0FBR2Y7QUFDQXpCLEVBQUFBLE9BQU8sRUFBRXVCLG1CQUFVRyxNQUFWLENBQWlCRCxVQUpYO0FBS2Y7QUFDQXhCLEVBQUFBLGNBQWMsRUFBRXNCLG1CQUFVQyxNQUFWLENBQWlCQyxVQU5sQjtBQU9mdEIsRUFBQUEsT0FBTyxFQUFFb0IsbUJBQVVJLElBQVYsQ0FBZUY7QUFQVCxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5cclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IHVuaWNvZGVUb1Nob3J0Y29kZSB9IGZyb20gJy4uLy4uLy4uL0h0bWxVdGlscyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHsgZm9ybWF0Q29tbWFTZXBhcmF0ZWRMaXN0IH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvRm9ybWF0dGluZ1V0aWxzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJlYWN0aW9uc1Jvd0J1dHRvblRvb2x0aXAgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLy8gVGhlIGV2ZW50IHdlJ3JlIGRpc3BsYXlpbmcgcmVhY3Rpb25zIGZvclxyXG4gICAgICAgIG14RXZlbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBUaGUgcmVhY3Rpb24gY29udGVudCAvIGtleSAvIGVtb2ppXHJcbiAgICAgICAgY29udGVudDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIC8vIEEgU2V0IG9mIE1hcnRpeCByZWFjdGlvbiBldmVudHMgZm9yIHRoaXMga2V5XHJcbiAgICAgICAgcmVhY3Rpb25FdmVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICB2aXNpYmxlOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBUb29sdGlwID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuVG9vbHRpcCcpO1xyXG4gICAgICAgIGNvbnN0IHsgY29udGVudCwgcmVhY3Rpb25FdmVudHMsIG14RXZlbnQsIHZpc2libGUgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShteEV2ZW50LmdldFJvb21JZCgpKTtcclxuICAgICAgICBsZXQgdG9vbHRpcExhYmVsO1xyXG4gICAgICAgIGlmIChyb29tKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbmRlcnMgPSBbXTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCByZWFjdGlvbkV2ZW50IG9mIHJlYWN0aW9uRXZlbnRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtZW1iZXIgPSByb29tLmdldE1lbWJlcihyZWFjdGlvbkV2ZW50LmdldFNlbmRlcigpKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG5hbWUgPSBtZW1iZXIgPyBtZW1iZXIubmFtZSA6IHJlYWN0aW9uRXZlbnQuZ2V0U2VuZGVyKCk7XHJcbiAgICAgICAgICAgICAgICBzZW5kZXJzLnB1c2gobmFtZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3Qgc2hvcnROYW1lID0gdW5pY29kZVRvU2hvcnRjb2RlKGNvbnRlbnQpO1xyXG4gICAgICAgICAgICB0b29sdGlwTGFiZWwgPSA8ZGl2PntfdChcclxuICAgICAgICAgICAgICAgIFwiPHJlYWN0b3JzLz48cmVhY3RlZFdpdGg+cmVhY3RlZCB3aXRoICUoc2hvcnROYW1lKXM8L3JlYWN0ZWRXaXRoPlwiLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3J0TmFtZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhY3RvcnM6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfUmVhY3Rpb25zUm93QnV0dG9uVG9vbHRpcF9zZW5kZXJzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Zm9ybWF0Q29tbWFTZXBhcmF0ZWRMaXN0KHNlbmRlcnMsIDYpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICByZWFjdGVkV2l0aDogKHN1YikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXNob3J0TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfUmVhY3Rpb25zUm93QnV0dG9uVG9vbHRpcF9yZWFjdGVkV2l0aFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3N1Yn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApfTwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB0b29sdGlwO1xyXG4gICAgICAgIGlmICh0b29sdGlwTGFiZWwpIHtcclxuICAgICAgICAgICAgdG9vbHRpcCA9IDxUb29sdGlwXHJcbiAgICAgICAgICAgICAgICB0b29sdGlwQ2xhc3NOYW1lPVwibXhfVG9vbHRpcF90aW1lbGluZVwiXHJcbiAgICAgICAgICAgICAgICB2aXNpYmxlPXt2aXNpYmxlfVxyXG4gICAgICAgICAgICAgICAgbGFiZWw9e3Rvb2x0aXBMYWJlbH1cclxuICAgICAgICAgICAgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdG9vbHRpcDtcclxuICAgIH1cclxufVxyXG4iXX0=