"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _DateUtils = require("../../../DateUtils");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function getdaysArray() {
  return [(0, _languageHandler._t)('Sunday'), (0, _languageHandler._t)('Monday'), (0, _languageHandler._t)('Tuesday'), (0, _languageHandler._t)('Wednesday'), (0, _languageHandler._t)('Thursday'), (0, _languageHandler._t)('Friday'), (0, _languageHandler._t)('Saturday')];
}

class DateSeparator extends _react.default.Component {
  getLabel() {
    const date = new Date(this.props.ts);
    const today = new Date();
    const yesterday = new Date();
    const days = getdaysArray();
    yesterday.setDate(today.getDate() - 1);

    if (date.toDateString() === today.toDateString()) {
      return (0, _languageHandler._t)('Today');
    } else if (date.toDateString() === yesterday.toDateString()) {
      return (0, _languageHandler._t)('Yesterday');
    } else if (today.getTime() - date.getTime() < 6 * 24 * 60 * 60 * 1000) {
      return days[date.getDay()];
    } else {
      return (0, _DateUtils.formatFullDateNoTime)(date);
    }
  }

  render() {
    // ARIA treats <hr/>s as separators, here we abuse them slightly so manually treat this entire thing as one
    // tab-index=-1 to allow it to be focusable but do not add tab stop for it, primarily for screen readers
    return _react.default.createElement("h2", {
      className: "mx_DateSeparator",
      role: "separator",
      tabIndex: -1
    }, _react.default.createElement("hr", {
      role: "none"
    }), _react.default.createElement("div", null, this.getLabel()), _react.default.createElement("hr", {
      role: "none"
    }));
  }

}

exports.default = DateSeparator;
(0, _defineProperty2.default)(DateSeparator, "propTypes", {
  ts: _propTypes.default.number.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL0RhdGVTZXBhcmF0b3IuanMiXSwibmFtZXMiOlsiZ2V0ZGF5c0FycmF5IiwiRGF0ZVNlcGFyYXRvciIsIlJlYWN0IiwiQ29tcG9uZW50IiwiZ2V0TGFiZWwiLCJkYXRlIiwiRGF0ZSIsInByb3BzIiwidHMiLCJ0b2RheSIsInllc3RlcmRheSIsImRheXMiLCJzZXREYXRlIiwiZ2V0RGF0ZSIsInRvRGF0ZVN0cmluZyIsImdldFRpbWUiLCJnZXREYXkiLCJyZW5kZXIiLCJQcm9wVHlwZXMiLCJudW1iZXIiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFwQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkEsU0FBU0EsWUFBVCxHQUF3QjtBQUN2QixTQUFPLENBQ0EseUJBQUcsUUFBSCxDQURBLEVBRUEseUJBQUcsUUFBSCxDQUZBLEVBR0EseUJBQUcsU0FBSCxDQUhBLEVBSUEseUJBQUcsV0FBSCxDQUpBLEVBS0EseUJBQUcsVUFBSCxDQUxBLEVBTUEseUJBQUcsUUFBSCxDQU5BLEVBT0EseUJBQUcsVUFBSCxDQVBBLENBQVA7QUFTQTs7QUFFYyxNQUFNQyxhQUFOLFNBQTRCQyxlQUFNQyxTQUFsQyxDQUE0QztBQUt2REMsRUFBQUEsUUFBUSxHQUFHO0FBQ1AsVUFBTUMsSUFBSSxHQUFHLElBQUlDLElBQUosQ0FBUyxLQUFLQyxLQUFMLENBQVdDLEVBQXBCLENBQWI7QUFDQSxVQUFNQyxLQUFLLEdBQUcsSUFBSUgsSUFBSixFQUFkO0FBQ0EsVUFBTUksU0FBUyxHQUFHLElBQUlKLElBQUosRUFBbEI7QUFDQSxVQUFNSyxJQUFJLEdBQUdYLFlBQVksRUFBekI7QUFDQVUsSUFBQUEsU0FBUyxDQUFDRSxPQUFWLENBQWtCSCxLQUFLLENBQUNJLE9BQU4sS0FBa0IsQ0FBcEM7O0FBRUEsUUFBSVIsSUFBSSxDQUFDUyxZQUFMLE9BQXdCTCxLQUFLLENBQUNLLFlBQU4sRUFBNUIsRUFBa0Q7QUFDOUMsYUFBTyx5QkFBRyxPQUFILENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSVQsSUFBSSxDQUFDUyxZQUFMLE9BQXdCSixTQUFTLENBQUNJLFlBQVYsRUFBNUIsRUFBc0Q7QUFDekQsYUFBTyx5QkFBRyxXQUFILENBQVA7QUFDSCxLQUZNLE1BRUEsSUFBSUwsS0FBSyxDQUFDTSxPQUFOLEtBQWtCVixJQUFJLENBQUNVLE9BQUwsRUFBbEIsR0FBbUMsSUFBSSxFQUFKLEdBQVMsRUFBVCxHQUFjLEVBQWQsR0FBbUIsSUFBMUQsRUFBZ0U7QUFDbkUsYUFBT0osSUFBSSxDQUFDTixJQUFJLENBQUNXLE1BQUwsRUFBRCxDQUFYO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsYUFBTyxxQ0FBcUJYLElBQXJCLENBQVA7QUFDSDtBQUNKOztBQUVEWSxFQUFBQSxNQUFNLEdBQUc7QUFDTDtBQUNBO0FBQ0EsV0FBTztBQUFJLE1BQUEsU0FBUyxFQUFDLGtCQUFkO0FBQWlDLE1BQUEsSUFBSSxFQUFDLFdBQXRDO0FBQWtELE1BQUEsUUFBUSxFQUFFLENBQUM7QUFBN0QsT0FDSDtBQUFJLE1BQUEsSUFBSSxFQUFDO0FBQVQsTUFERyxFQUVILDBDQUFPLEtBQUtiLFFBQUwsRUFBUCxDQUZHLEVBR0g7QUFBSSxNQUFBLElBQUksRUFBQztBQUFULE1BSEcsQ0FBUDtBQUtIOztBQS9Cc0Q7Ozs4QkFBdENILGEsZUFDRTtBQUNmTyxFQUFBQSxFQUFFLEVBQUVVLG1CQUFVQyxNQUFWLENBQWlCQztBQUROLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7Zm9ybWF0RnVsbERhdGVOb1RpbWV9IGZyb20gJy4uLy4uLy4uL0RhdGVVdGlscyc7XHJcblxyXG5mdW5jdGlvbiBnZXRkYXlzQXJyYXkoKSB7XHJcblx0cmV0dXJuIFtcclxuICAgICAgICBfdCgnU3VuZGF5JyksXHJcbiAgICAgICAgX3QoJ01vbmRheScpLFxyXG4gICAgICAgIF90KCdUdWVzZGF5JyksXHJcbiAgICAgICAgX3QoJ1dlZG5lc2RheScpLFxyXG4gICAgICAgIF90KCdUaHVyc2RheScpLFxyXG4gICAgICAgIF90KCdGcmlkYXknKSxcclxuICAgICAgICBfdCgnU2F0dXJkYXknKSxcclxuICAgIF07XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERhdGVTZXBhcmF0b3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICB0czogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBnZXRMYWJlbCgpIHtcclxuICAgICAgICBjb25zdCBkYXRlID0gbmV3IERhdGUodGhpcy5wcm9wcy50cyk7XHJcbiAgICAgICAgY29uc3QgdG9kYXkgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGNvbnN0IHllc3RlcmRheSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgY29uc3QgZGF5cyA9IGdldGRheXNBcnJheSgpO1xyXG4gICAgICAgIHllc3RlcmRheS5zZXREYXRlKHRvZGF5LmdldERhdGUoKSAtIDEpO1xyXG5cclxuICAgICAgICBpZiAoZGF0ZS50b0RhdGVTdHJpbmcoKSA9PT0gdG9kYXkudG9EYXRlU3RyaW5nKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90KCdUb2RheScpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZGF0ZS50b0RhdGVTdHJpbmcoKSA9PT0geWVzdGVyZGF5LnRvRGF0ZVN0cmluZygpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdCgnWWVzdGVyZGF5Jyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0b2RheS5nZXRUaW1lKCkgLSBkYXRlLmdldFRpbWUoKSA8IDYgKiAyNCAqIDYwICogNjAgKiAxMDAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXlzW2RhdGUuZ2V0RGF5KCldO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmb3JtYXRGdWxsRGF0ZU5vVGltZShkYXRlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIC8vIEFSSUEgdHJlYXRzIDxoci8+cyBhcyBzZXBhcmF0b3JzLCBoZXJlIHdlIGFidXNlIHRoZW0gc2xpZ2h0bHkgc28gbWFudWFsbHkgdHJlYXQgdGhpcyBlbnRpcmUgdGhpbmcgYXMgb25lXHJcbiAgICAgICAgLy8gdGFiLWluZGV4PS0xIHRvIGFsbG93IGl0IHRvIGJlIGZvY3VzYWJsZSBidXQgZG8gbm90IGFkZCB0YWIgc3RvcCBmb3IgaXQsIHByaW1hcmlseSBmb3Igc2NyZWVuIHJlYWRlcnNcclxuICAgICAgICByZXR1cm4gPGgyIGNsYXNzTmFtZT1cIm14X0RhdGVTZXBhcmF0b3JcIiByb2xlPVwic2VwYXJhdG9yXCIgdGFiSW5kZXg9ey0xfT5cclxuICAgICAgICAgICAgPGhyIHJvbGU9XCJub25lXCIgLz5cclxuICAgICAgICAgICAgPGRpdj57IHRoaXMuZ2V0TGFiZWwoKSB9PC9kaXY+XHJcbiAgICAgICAgICAgIDxociByb2xlPVwibm9uZVwiIC8+XHJcbiAgICAgICAgPC9oMj47XHJcbiAgICB9XHJcbn1cclxuIl19