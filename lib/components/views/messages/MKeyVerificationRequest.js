"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _KeyVerificationStateObserver = require("../../../utils/KeyVerificationStateObserver");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _RightPanelStorePhases = require("../../../stores/RightPanelStorePhases");

/*
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MKeyVerificationRequest extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_openRequest", () => {
      const {
        verificationRequest
      } = this.props.mxEvent;

      const member = _MatrixClientPeg.MatrixClientPeg.get().getUser(verificationRequest.otherUserId);

      _dispatcher.default.dispatch({
        action: "set_right_panel_phase",
        phase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel,
        refireParams: {
          verificationRequest,
          member
        }
      });
    });
    (0, _defineProperty2.default)(this, "_onRequestChanged", () => {
      this.forceUpdate();
    });
    (0, _defineProperty2.default)(this, "_onAcceptClicked", async () => {
      const request = this.props.mxEvent.verificationRequest;

      if (request) {
        try {
          this._openRequest();

          await request.accept();
        } catch (err) {
          console.error(err.message);
        }
      }
    });
    (0, _defineProperty2.default)(this, "_onRejectClicked", async () => {
      const request = this.props.mxEvent.verificationRequest;

      if (request) {
        try {
          await request.cancel();
        } catch (err) {
          console.error(err.message);
        }
      }
    });
    this.state = {};
  }

  componentDidMount() {
    const request = this.props.mxEvent.verificationRequest;

    if (request) {
      request.on("change", this._onRequestChanged);
    }
  }

  componentWillUnmount() {
    const request = this.props.mxEvent.verificationRequest;

    if (request) {
      request.off("change", this._onRequestChanged);
    }
  }

  _acceptedLabel(userId) {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const myUserId = client.getUserId();

    if (userId === myUserId) {
      return (0, _languageHandler._t)("You accepted");
    } else {
      return (0, _languageHandler._t)("%(name)s accepted", {
        name: (0, _KeyVerificationStateObserver.getNameForEventRoom)(userId, this.props.mxEvent.getRoomId())
      });
    }
  }

  _cancelledLabel(userId) {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const myUserId = client.getUserId();
    const {
      cancellationCode
    } = this.props.mxEvent.verificationRequest;
    const declined = cancellationCode === "m.user";

    if (userId === myUserId) {
      if (declined) {
        return (0, _languageHandler._t)("You declined");
      } else {
        return (0, _languageHandler._t)("You cancelled");
      }
    } else {
      if (declined) {
        return (0, _languageHandler._t)("%(name)s declined", {
          name: (0, _KeyVerificationStateObserver.getNameForEventRoom)(userId, this.props.mxEvent.getRoomId())
        });
      } else {
        return (0, _languageHandler._t)("%(name)s cancelled", {
          name: (0, _KeyVerificationStateObserver.getNameForEventRoom)(userId, this.props.mxEvent.getRoomId())
        });
      }
    }
  }

  render() {
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    const FormButton = sdk.getComponent("elements.FormButton");
    const {
      mxEvent
    } = this.props;
    const request = mxEvent.verificationRequest;

    if (!request || request.invalid) {
      return null;
    }

    let title;
    let subtitle;
    let stateNode;

    if (!request.canAccept) {
      let stateLabel;
      const accepted = request.ready || request.started || request.done;

      if (accepted) {
        stateLabel = _react.default.createElement(AccessibleButton, {
          onClick: this._openRequest
        }, this._acceptedLabel(request.receivingUserId));
      } else if (request.cancelled) {
        stateLabel = this._cancelledLabel(request.cancellingUserId);
      } else if (request.accepting) {
        stateLabel = (0, _languageHandler._t)("Accepting …");
      } else if (request.declining) {
        stateLabel = (0, _languageHandler._t)("Declining …");
      }

      stateNode = _react.default.createElement("div", {
        className: "mx_cryptoEvent_state"
      }, stateLabel);
    }

    if (!request.initiatedByMe) {
      const name = (0, _KeyVerificationStateObserver.getNameForEventRoom)(request.requestingUserId, mxEvent.getRoomId());
      title = _react.default.createElement("div", {
        className: "mx_cryptoEvent_title"
      }, (0, _languageHandler._t)("%(name)s wants to verify", {
        name
      }));
      subtitle = _react.default.createElement("div", {
        className: "mx_cryptoEvent_subtitle"
      }, (0, _KeyVerificationStateObserver.userLabelForEventRoom)(request.requestingUserId, mxEvent.getRoomId()));

      if (request.canAccept) {
        stateNode = _react.default.createElement("div", {
          className: "mx_cryptoEvent_buttons"
        }, _react.default.createElement(FormButton, {
          kind: "danger",
          onClick: this._onRejectClicked,
          label: (0, _languageHandler._t)("Decline")
        }), _react.default.createElement(FormButton, {
          onClick: this._onAcceptClicked,
          label: (0, _languageHandler._t)("Accept")
        }));
      }
    } else {
      // request sent by us
      title = _react.default.createElement("div", {
        className: "mx_cryptoEvent_title"
      }, (0, _languageHandler._t)("You sent a verification request"));
      subtitle = _react.default.createElement("div", {
        className: "mx_cryptoEvent_subtitle"
      }, (0, _KeyVerificationStateObserver.userLabelForEventRoom)(request.receivingUserId, mxEvent.getRoomId()));
    }

    if (title) {
      return _react.default.createElement("div", {
        className: "mx_EventTile_bubble mx_cryptoEvent mx_cryptoEvent_icon"
      }, title, subtitle, stateNode);
    }

    return null;
  }

}

exports.default = MKeyVerificationRequest;
MKeyVerificationRequest.propTypes = {
  /* the MatrixEvent to show */
  mxEvent: _propTypes.default.object.isRequired
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01LZXlWZXJpZmljYXRpb25SZXF1ZXN0LmpzIl0sIm5hbWVzIjpbIk1LZXlWZXJpZmljYXRpb25SZXF1ZXN0IiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwidmVyaWZpY2F0aW9uUmVxdWVzdCIsIm14RXZlbnQiLCJtZW1iZXIiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJnZXRVc2VyIiwib3RoZXJVc2VySWQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInBoYXNlIiwiUklHSFRfUEFORUxfUEhBU0VTIiwiRW5jcnlwdGlvblBhbmVsIiwicmVmaXJlUGFyYW1zIiwiZm9yY2VVcGRhdGUiLCJyZXF1ZXN0IiwiX29wZW5SZXF1ZXN0IiwiYWNjZXB0IiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwibWVzc2FnZSIsImNhbmNlbCIsInN0YXRlIiwiY29tcG9uZW50RGlkTW91bnQiLCJvbiIsIl9vblJlcXVlc3RDaGFuZ2VkIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJvZmYiLCJfYWNjZXB0ZWRMYWJlbCIsInVzZXJJZCIsImNsaWVudCIsIm15VXNlcklkIiwiZ2V0VXNlcklkIiwibmFtZSIsImdldFJvb21JZCIsIl9jYW5jZWxsZWRMYWJlbCIsImNhbmNlbGxhdGlvbkNvZGUiLCJkZWNsaW5lZCIsInJlbmRlciIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJGb3JtQnV0dG9uIiwiaW52YWxpZCIsInRpdGxlIiwic3VidGl0bGUiLCJzdGF0ZU5vZGUiLCJjYW5BY2NlcHQiLCJzdGF0ZUxhYmVsIiwiYWNjZXB0ZWQiLCJyZWFkeSIsInN0YXJ0ZWQiLCJkb25lIiwicmVjZWl2aW5nVXNlcklkIiwiY2FuY2VsbGVkIiwiY2FuY2VsbGluZ1VzZXJJZCIsImFjY2VwdGluZyIsImRlY2xpbmluZyIsImluaXRpYXRlZEJ5TWUiLCJyZXF1ZXN0aW5nVXNlcklkIiwiX29uUmVqZWN0Q2xpY2tlZCIsIl9vbkFjY2VwdENsaWNrZWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQXhCQTs7Ozs7Ozs7Ozs7Ozs7O0FBMEJlLE1BQU1BLHVCQUFOLFNBQXNDQyxlQUFNQyxTQUE1QyxDQUFzRDtBQUNqRUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsd0RBbUJKLE1BQU07QUFDakIsWUFBTTtBQUFDQyxRQUFBQTtBQUFELFVBQXdCLEtBQUtELEtBQUwsQ0FBV0UsT0FBekM7O0FBQ0EsWUFBTUMsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxPQUF0QixDQUE4QkwsbUJBQW1CLENBQUNNLFdBQWxELENBQWY7O0FBQ0FDLDBCQUFJQyxRQUFKLENBQWE7QUFDVEMsUUFBQUEsTUFBTSxFQUFFLHVCQURDO0FBRVRDLFFBQUFBLEtBQUssRUFBRUMsMENBQW1CQyxlQUZqQjtBQUdUQyxRQUFBQSxZQUFZLEVBQUU7QUFBQ2IsVUFBQUEsbUJBQUQ7QUFBc0JFLFVBQUFBO0FBQXRCO0FBSEwsT0FBYjtBQUtILEtBM0JrQjtBQUFBLDZEQTZCQyxNQUFNO0FBQ3RCLFdBQUtZLFdBQUw7QUFDSCxLQS9Ca0I7QUFBQSw0REFpQ0EsWUFBWTtBQUMzQixZQUFNQyxPQUFPLEdBQUcsS0FBS2hCLEtBQUwsQ0FBV0UsT0FBWCxDQUFtQkQsbUJBQW5DOztBQUNBLFVBQUllLE9BQUosRUFBYTtBQUNULFlBQUk7QUFDQSxlQUFLQyxZQUFMOztBQUNBLGdCQUFNRCxPQUFPLENBQUNFLE1BQVIsRUFBTjtBQUNILFNBSEQsQ0FHRSxPQUFPQyxHQUFQLEVBQVk7QUFDVkMsVUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQUcsQ0FBQ0csT0FBbEI7QUFDSDtBQUNKO0FBQ0osS0EzQ2tCO0FBQUEsNERBNkNBLFlBQVk7QUFDM0IsWUFBTU4sT0FBTyxHQUFHLEtBQUtoQixLQUFMLENBQVdFLE9BQVgsQ0FBbUJELG1CQUFuQzs7QUFDQSxVQUFJZSxPQUFKLEVBQWE7QUFDVCxZQUFJO0FBQ0EsZ0JBQU1BLE9BQU8sQ0FBQ08sTUFBUixFQUFOO0FBQ0gsU0FGRCxDQUVFLE9BQU9KLEdBQVAsRUFBWTtBQUNWQyxVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBRyxDQUFDRyxPQUFsQjtBQUNIO0FBQ0o7QUFDSixLQXREa0I7QUFFZixTQUFLRSxLQUFMLEdBQWEsRUFBYjtBQUNIOztBQUVEQyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixVQUFNVCxPQUFPLEdBQUcsS0FBS2hCLEtBQUwsQ0FBV0UsT0FBWCxDQUFtQkQsbUJBQW5DOztBQUNBLFFBQUllLE9BQUosRUFBYTtBQUNUQSxNQUFBQSxPQUFPLENBQUNVLEVBQVIsQ0FBVyxRQUFYLEVBQXFCLEtBQUtDLGlCQUExQjtBQUNIO0FBQ0o7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFVBQU1aLE9BQU8sR0FBRyxLQUFLaEIsS0FBTCxDQUFXRSxPQUFYLENBQW1CRCxtQkFBbkM7O0FBQ0EsUUFBSWUsT0FBSixFQUFhO0FBQ1RBLE1BQUFBLE9BQU8sQ0FBQ2EsR0FBUixDQUFZLFFBQVosRUFBc0IsS0FBS0YsaUJBQTNCO0FBQ0g7QUFDSjs7QUF1Q0RHLEVBQUFBLGNBQWMsQ0FBQ0MsTUFBRCxFQUFTO0FBQ25CLFVBQU1DLE1BQU0sR0FBRzVCLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxVQUFNNEIsUUFBUSxHQUFHRCxNQUFNLENBQUNFLFNBQVAsRUFBakI7O0FBQ0EsUUFBSUgsTUFBTSxLQUFLRSxRQUFmLEVBQXlCO0FBQ3JCLGFBQU8seUJBQUcsY0FBSCxDQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsYUFBTyx5QkFBRyxtQkFBSCxFQUF3QjtBQUFDRSxRQUFBQSxJQUFJLEVBQUUsdURBQW9CSixNQUFwQixFQUE0QixLQUFLL0IsS0FBTCxDQUFXRSxPQUFYLENBQW1Ca0MsU0FBbkIsRUFBNUI7QUFBUCxPQUF4QixDQUFQO0FBQ0g7QUFDSjs7QUFFREMsRUFBQUEsZUFBZSxDQUFDTixNQUFELEVBQVM7QUFDcEIsVUFBTUMsTUFBTSxHQUFHNUIsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFVBQU00QixRQUFRLEdBQUdELE1BQU0sQ0FBQ0UsU0FBUCxFQUFqQjtBQUNBLFVBQU07QUFBQ0ksTUFBQUE7QUFBRCxRQUFxQixLQUFLdEMsS0FBTCxDQUFXRSxPQUFYLENBQW1CRCxtQkFBOUM7QUFDQSxVQUFNc0MsUUFBUSxHQUFHRCxnQkFBZ0IsS0FBSyxRQUF0Qzs7QUFDQSxRQUFJUCxNQUFNLEtBQUtFLFFBQWYsRUFBeUI7QUFDckIsVUFBSU0sUUFBSixFQUFjO0FBQ1YsZUFBTyx5QkFBRyxjQUFILENBQVA7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLHlCQUFHLGVBQUgsQ0FBUDtBQUNIO0FBQ0osS0FORCxNQU1PO0FBQ0gsVUFBSUEsUUFBSixFQUFjO0FBQ1YsZUFBTyx5QkFBRyxtQkFBSCxFQUF3QjtBQUFDSixVQUFBQSxJQUFJLEVBQUUsdURBQW9CSixNQUFwQixFQUE0QixLQUFLL0IsS0FBTCxDQUFXRSxPQUFYLENBQW1Ca0MsU0FBbkIsRUFBNUI7QUFBUCxTQUF4QixDQUFQO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsZUFBTyx5QkFBRyxvQkFBSCxFQUF5QjtBQUFDRCxVQUFBQSxJQUFJLEVBQUUsdURBQW9CSixNQUFwQixFQUE0QixLQUFLL0IsS0FBTCxDQUFXRSxPQUFYLENBQW1Ca0MsU0FBbkIsRUFBNUI7QUFBUCxTQUF6QixDQUFQO0FBQ0g7QUFDSjtBQUNKOztBQUVESSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU1DLFVBQVUsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFuQjtBQUVBLFVBQU07QUFBQ3pDLE1BQUFBO0FBQUQsUUFBWSxLQUFLRixLQUF2QjtBQUNBLFVBQU1nQixPQUFPLEdBQUdkLE9BQU8sQ0FBQ0QsbUJBQXhCOztBQUVBLFFBQUksQ0FBQ2UsT0FBRCxJQUFZQSxPQUFPLENBQUM2QixPQUF4QixFQUFpQztBQUM3QixhQUFPLElBQVA7QUFDSDs7QUFFRCxRQUFJQyxLQUFKO0FBQ0EsUUFBSUMsUUFBSjtBQUNBLFFBQUlDLFNBQUo7O0FBRUEsUUFBSSxDQUFDaEMsT0FBTyxDQUFDaUMsU0FBYixFQUF3QjtBQUNwQixVQUFJQyxVQUFKO0FBQ0EsWUFBTUMsUUFBUSxHQUFHbkMsT0FBTyxDQUFDb0MsS0FBUixJQUFpQnBDLE9BQU8sQ0FBQ3FDLE9BQXpCLElBQW9DckMsT0FBTyxDQUFDc0MsSUFBN0Q7O0FBQ0EsVUFBSUgsUUFBSixFQUFjO0FBQ1ZELFFBQUFBLFVBQVUsR0FBSSw2QkFBQyxnQkFBRDtBQUFrQixVQUFBLE9BQU8sRUFBRSxLQUFLakM7QUFBaEMsV0FDVCxLQUFLYSxjQUFMLENBQW9CZCxPQUFPLENBQUN1QyxlQUE1QixDQURTLENBQWQ7QUFHSCxPQUpELE1BSU8sSUFBSXZDLE9BQU8sQ0FBQ3dDLFNBQVosRUFBdUI7QUFDMUJOLFFBQUFBLFVBQVUsR0FBRyxLQUFLYixlQUFMLENBQXFCckIsT0FBTyxDQUFDeUMsZ0JBQTdCLENBQWI7QUFDSCxPQUZNLE1BRUEsSUFBSXpDLE9BQU8sQ0FBQzBDLFNBQVosRUFBdUI7QUFDMUJSLFFBQUFBLFVBQVUsR0FBRyx5QkFBRyxhQUFILENBQWI7QUFDSCxPQUZNLE1BRUEsSUFBSWxDLE9BQU8sQ0FBQzJDLFNBQVosRUFBdUI7QUFDMUJULFFBQUFBLFVBQVUsR0FBRyx5QkFBRyxhQUFILENBQWI7QUFDSDs7QUFDREYsTUFBQUEsU0FBUyxHQUFJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUF1Q0UsVUFBdkMsQ0FBYjtBQUNIOztBQUVELFFBQUksQ0FBQ2xDLE9BQU8sQ0FBQzRDLGFBQWIsRUFBNEI7QUFDeEIsWUFBTXpCLElBQUksR0FBRyx1REFBb0JuQixPQUFPLENBQUM2QyxnQkFBNUIsRUFBOEMzRCxPQUFPLENBQUNrQyxTQUFSLEVBQTlDLENBQWI7QUFDQVUsTUFBQUEsS0FBSyxHQUFJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNMLHlCQUFHLDBCQUFILEVBQStCO0FBQUNYLFFBQUFBO0FBQUQsT0FBL0IsQ0FESyxDQUFUO0FBRUFZLE1BQUFBLFFBQVEsR0FBSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDUix5REFBc0IvQixPQUFPLENBQUM2QyxnQkFBOUIsRUFBZ0QzRCxPQUFPLENBQUNrQyxTQUFSLEVBQWhELENBRFEsQ0FBWjs7QUFFQSxVQUFJcEIsT0FBTyxDQUFDaUMsU0FBWixFQUF1QjtBQUNuQkQsUUFBQUEsU0FBUyxHQUFJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNULDZCQUFDLFVBQUQ7QUFBWSxVQUFBLElBQUksRUFBQyxRQUFqQjtBQUEwQixVQUFBLE9BQU8sRUFBRSxLQUFLYyxnQkFBeEM7QUFBMEQsVUFBQSxLQUFLLEVBQUUseUJBQUcsU0FBSDtBQUFqRSxVQURTLEVBRVQsNkJBQUMsVUFBRDtBQUFZLFVBQUEsT0FBTyxFQUFFLEtBQUtDLGdCQUExQjtBQUE0QyxVQUFBLEtBQUssRUFBRSx5QkFBRyxRQUFIO0FBQW5ELFVBRlMsQ0FBYjtBQUlIO0FBQ0osS0FaRCxNQVlPO0FBQUU7QUFDTGpCLE1BQUFBLEtBQUssR0FBSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDTCx5QkFBRyxpQ0FBSCxDQURLLENBQVQ7QUFFQUMsTUFBQUEsUUFBUSxHQUFJO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNSLHlEQUFzQi9CLE9BQU8sQ0FBQ3VDLGVBQTlCLEVBQStDckQsT0FBTyxDQUFDa0MsU0FBUixFQUEvQyxDQURRLENBQVo7QUFFSDs7QUFFRCxRQUFJVSxLQUFKLEVBQVc7QUFDUCxhQUFRO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNIQSxLQURHLEVBRUhDLFFBRkcsRUFHSEMsU0FIRyxDQUFSO0FBS0g7O0FBQ0QsV0FBTyxJQUFQO0FBQ0g7O0FBbEpnRTs7O0FBcUpyRXBELHVCQUF1QixDQUFDb0UsU0FBeEIsR0FBb0M7QUFDaEM7QUFDQTlELEVBQUFBLE9BQU8sRUFBRStELG1CQUFVQyxNQUFWLENBQWlCQztBQUZNLENBQXBDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTksIDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge2dldE5hbWVGb3JFdmVudFJvb20sIHVzZXJMYWJlbEZvckV2ZW50Um9vbX1cclxuICAgIGZyb20gJy4uLy4uLy4uL3V0aWxzL0tleVZlcmlmaWNhdGlvblN0YXRlT2JzZXJ2ZXInO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCB7UklHSFRfUEFORUxfUEhBU0VTfSBmcm9tIFwiLi4vLi4vLi4vc3RvcmVzL1JpZ2h0UGFuZWxTdG9yZVBoYXNlc1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTUtleVZlcmlmaWNhdGlvblJlcXVlc3QgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHt9O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSB0aGlzLnByb3BzLm14RXZlbnQudmVyaWZpY2F0aW9uUmVxdWVzdDtcclxuICAgICAgICBpZiAocmVxdWVzdCkge1xyXG4gICAgICAgICAgICByZXF1ZXN0Lm9uKFwiY2hhbmdlXCIsIHRoaXMuX29uUmVxdWVzdENoYW5nZWQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBjb25zdCByZXF1ZXN0ID0gdGhpcy5wcm9wcy5teEV2ZW50LnZlcmlmaWNhdGlvblJlcXVlc3Q7XHJcbiAgICAgICAgaWYgKHJlcXVlc3QpIHtcclxuICAgICAgICAgICAgcmVxdWVzdC5vZmYoXCJjaGFuZ2VcIiwgdGhpcy5fb25SZXF1ZXN0Q2hhbmdlZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vcGVuUmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCB7dmVyaWZpY2F0aW9uUmVxdWVzdH0gPSB0aGlzLnByb3BzLm14RXZlbnQ7XHJcbiAgICAgICAgY29uc3QgbWVtYmVyID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXIodmVyaWZpY2F0aW9uUmVxdWVzdC5vdGhlclVzZXJJZCk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiBcInNldF9yaWdodF9wYW5lbF9waGFzZVwiLFxyXG4gICAgICAgICAgICBwaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLkVuY3J5cHRpb25QYW5lbCxcclxuICAgICAgICAgICAgcmVmaXJlUGFyYW1zOiB7dmVyaWZpY2F0aW9uUmVxdWVzdCwgbWVtYmVyfSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX29uUmVxdWVzdENoYW5nZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25BY2NlcHRDbGlja2VkID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSB0aGlzLnByb3BzLm14RXZlbnQudmVyaWZpY2F0aW9uUmVxdWVzdDtcclxuICAgICAgICBpZiAocmVxdWVzdCkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fb3BlblJlcXVlc3QoKTtcclxuICAgICAgICAgICAgICAgIGF3YWl0IHJlcXVlc3QuYWNjZXB0KCk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIubWVzc2FnZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblJlamVjdENsaWNrZWQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IHRoaXMucHJvcHMubXhFdmVudC52ZXJpZmljYXRpb25SZXF1ZXN0O1xyXG4gICAgICAgIGlmIChyZXF1ZXN0KSB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCByZXF1ZXN0LmNhbmNlbCgpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyLm1lc3NhZ2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfYWNjZXB0ZWRMYWJlbCh1c2VySWQpIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3QgbXlVc2VySWQgPSBjbGllbnQuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgaWYgKHVzZXJJZCA9PT0gbXlVc2VySWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90KFwiWW91IGFjY2VwdGVkXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdChcIiUobmFtZSlzIGFjY2VwdGVkXCIsIHtuYW1lOiBnZXROYW1lRm9yRXZlbnRSb29tKHVzZXJJZCwgdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpKX0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfY2FuY2VsbGVkTGFiZWwodXNlcklkKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IG15VXNlcklkID0gY2xpZW50LmdldFVzZXJJZCgpO1xyXG4gICAgICAgIGNvbnN0IHtjYW5jZWxsYXRpb25Db2RlfSA9IHRoaXMucHJvcHMubXhFdmVudC52ZXJpZmljYXRpb25SZXF1ZXN0O1xyXG4gICAgICAgIGNvbnN0IGRlY2xpbmVkID0gY2FuY2VsbGF0aW9uQ29kZSA9PT0gXCJtLnVzZXJcIjtcclxuICAgICAgICBpZiAodXNlcklkID09PSBteVVzZXJJZCkge1xyXG4gICAgICAgICAgICBpZiAoZGVjbGluZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdChcIllvdSBkZWNsaW5lZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdChcIllvdSBjYW5jZWxsZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoZGVjbGluZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdChcIiUobmFtZSlzIGRlY2xpbmVkXCIsIHtuYW1lOiBnZXROYW1lRm9yRXZlbnRSb29tKHVzZXJJZCwgdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpKX0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KFwiJShuYW1lKXMgY2FuY2VsbGVkXCIsIHtuYW1lOiBnZXROYW1lRm9yRXZlbnRSb29tKHVzZXJJZCwgdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpKX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b25cIik7XHJcbiAgICAgICAgY29uc3QgRm9ybUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5Gb3JtQnV0dG9uXCIpO1xyXG5cclxuICAgICAgICBjb25zdCB7bXhFdmVudH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBteEV2ZW50LnZlcmlmaWNhdGlvblJlcXVlc3Q7XHJcblxyXG4gICAgICAgIGlmICghcmVxdWVzdCB8fCByZXF1ZXN0LmludmFsaWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdGl0bGU7XHJcbiAgICAgICAgbGV0IHN1YnRpdGxlO1xyXG4gICAgICAgIGxldCBzdGF0ZU5vZGU7XHJcblxyXG4gICAgICAgIGlmICghcmVxdWVzdC5jYW5BY2NlcHQpIHtcclxuICAgICAgICAgICAgbGV0IHN0YXRlTGFiZWw7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjY2VwdGVkID0gcmVxdWVzdC5yZWFkeSB8fCByZXF1ZXN0LnN0YXJ0ZWQgfHwgcmVxdWVzdC5kb25lO1xyXG4gICAgICAgICAgICBpZiAoYWNjZXB0ZWQpIHtcclxuICAgICAgICAgICAgICAgIHN0YXRlTGFiZWwgPSAoPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17dGhpcy5fb3BlblJlcXVlc3R9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLl9hY2NlcHRlZExhYmVsKHJlcXVlc3QucmVjZWl2aW5nVXNlcklkKX1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj4pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3QuY2FuY2VsbGVkKSB7XHJcbiAgICAgICAgICAgICAgICBzdGF0ZUxhYmVsID0gdGhpcy5fY2FuY2VsbGVkTGFiZWwocmVxdWVzdC5jYW5jZWxsaW5nVXNlcklkKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0LmFjY2VwdGluZykge1xyXG4gICAgICAgICAgICAgICAgc3RhdGVMYWJlbCA9IF90KFwiQWNjZXB0aW5nIOKAplwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0LmRlY2xpbmluZykge1xyXG4gICAgICAgICAgICAgICAgc3RhdGVMYWJlbCA9IF90KFwiRGVjbGluaW5nIOKAplwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBzdGF0ZU5vZGUgPSAoPGRpdiBjbGFzc05hbWU9XCJteF9jcnlwdG9FdmVudF9zdGF0ZVwiPntzdGF0ZUxhYmVsfTwvZGl2Pik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXJlcXVlc3QuaW5pdGlhdGVkQnlNZSkge1xyXG4gICAgICAgICAgICBjb25zdCBuYW1lID0gZ2V0TmFtZUZvckV2ZW50Um9vbShyZXF1ZXN0LnJlcXVlc3RpbmdVc2VySWQsIG14RXZlbnQuZ2V0Um9vbUlkKCkpO1xyXG4gICAgICAgICAgICB0aXRsZSA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X2NyeXB0b0V2ZW50X3RpdGxlXCI+e1xyXG4gICAgICAgICAgICAgICAgX3QoXCIlKG5hbWUpcyB3YW50cyB0byB2ZXJpZnlcIiwge25hbWV9KX08L2Rpdj4pO1xyXG4gICAgICAgICAgICBzdWJ0aXRsZSA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X2NyeXB0b0V2ZW50X3N1YnRpdGxlXCI+e1xyXG4gICAgICAgICAgICAgICAgdXNlckxhYmVsRm9yRXZlbnRSb29tKHJlcXVlc3QucmVxdWVzdGluZ1VzZXJJZCwgbXhFdmVudC5nZXRSb29tSWQoKSl9PC9kaXY+KTtcclxuICAgICAgICAgICAgaWYgKHJlcXVlc3QuY2FuQWNjZXB0KSB7XHJcbiAgICAgICAgICAgICAgICBzdGF0ZU5vZGUgPSAoPGRpdiBjbGFzc05hbWU9XCJteF9jcnlwdG9FdmVudF9idXR0b25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm1CdXR0b24ga2luZD1cImRhbmdlclwiIG9uQ2xpY2s9e3RoaXMuX29uUmVqZWN0Q2xpY2tlZH0gbGFiZWw9e190KFwiRGVjbGluZVwiKX0gLz5cclxuICAgICAgICAgICAgICAgICAgICA8Rm9ybUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkFjY2VwdENsaWNrZWR9IGxhYmVsPXtfdChcIkFjY2VwdFwiKX0gLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2Pik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgeyAvLyByZXF1ZXN0IHNlbnQgYnkgdXNcclxuICAgICAgICAgICAgdGl0bGUgPSAoPGRpdiBjbGFzc05hbWU9XCJteF9jcnlwdG9FdmVudF90aXRsZVwiPntcclxuICAgICAgICAgICAgICAgIF90KFwiWW91IHNlbnQgYSB2ZXJpZmljYXRpb24gcmVxdWVzdFwiKX08L2Rpdj4pO1xyXG4gICAgICAgICAgICBzdWJ0aXRsZSA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X2NyeXB0b0V2ZW50X3N1YnRpdGxlXCI+e1xyXG4gICAgICAgICAgICAgICAgdXNlckxhYmVsRm9yRXZlbnRSb29tKHJlcXVlc3QucmVjZWl2aW5nVXNlcklkLCBteEV2ZW50LmdldFJvb21JZCgpKX08L2Rpdj4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRpdGxlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfYnViYmxlIG14X2NyeXB0b0V2ZW50IG14X2NyeXB0b0V2ZW50X2ljb25cIj5cclxuICAgICAgICAgICAgICAgIHt0aXRsZX1cclxuICAgICAgICAgICAgICAgIHtzdWJ0aXRsZX1cclxuICAgICAgICAgICAgICAgIHtzdGF0ZU5vZGV9XHJcbiAgICAgICAgICAgIDwvZGl2Pik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG59XHJcblxyXG5NS2V5VmVyaWZpY2F0aW9uUmVxdWVzdC5wcm9wVHlwZXMgPSB7XHJcbiAgICAvKiB0aGUgTWF0cml4RXZlbnQgdG8gc2hvdyAqL1xyXG4gICAgbXhFdmVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG59O1xyXG4iXX0=