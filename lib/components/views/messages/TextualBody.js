"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _highlight = _interopRequireDefault(require("highlight.js"));

var HtmlUtils = _interopRequireWildcard(require("../../../HtmlUtils"));

var _DateUtils = require("../../../DateUtils");

var sdk = _interopRequireWildcard(require("../../../index"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _languageHandler = require("../../../languageHandler");

var ContextMenu = _interopRequireWildcard(require("../../structures/ContextMenu"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _ReplyThread = _interopRequireDefault(require("../elements/ReplyThread"));

var _pillify = require("../../../utils/pillify");

var _IntegrationManagers = require("../../../integrations/IntegrationManagers");

var _Permalinks = require("../../../utils/permalinks/Permalinks");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = (0, _createReactClass.default)({
  displayName: 'TextualBody',
  propTypes: {
    /* the MatrixEvent to show */
    mxEvent: _propTypes.default.object.isRequired,

    /* a list of words to highlight */
    highlights: _propTypes.default.array,

    /* link URL for the highlights */
    highlightLink: _propTypes.default.string,

    /* should show URL previews for this event */
    showUrlPreview: _propTypes.default.bool,

    /* callback for when our widget has loaded */
    onHeightChanged: _propTypes.default.func,

    /* the shape of the tile, used */
    tileShape: _propTypes.default.string
  },
  getInitialState: function () {
    return {
      // the URLs (if any) to be previewed with a LinkPreviewWidget
      // inside this TextualBody.
      links: [],
      // track whether the preview widget is hidden
      widgetHidden: false
    };
  },
  copyToClipboard: function (text) {
    const textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    let successful = false;

    try {
      successful = document.execCommand('copy');
    } catch (err) {
      console.log('Unable to copy');
    }

    document.body.removeChild(textArea);
    return successful;
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._content = (0, _react.createRef)();
  },
  componentDidMount: function () {
    this._unmounted = false;
    this._pills = [];

    if (!this.props.editState) {
      this._applyFormatting();
    }
  },

  _applyFormatting() {
    this.activateSpoilers([this._content.current]); // pillifyLinks BEFORE linkifyElement because plain room/user URLs in the composer
    // are still sent as plaintext URLs. If these are ever pillified in the composer,
    // we should be pillify them here by doing the linkifying BEFORE the pillifying.

    (0, _pillify.pillifyLinks)([this._content.current], this.props.mxEvent, this._pills);
    HtmlUtils.linkifyElement(this._content.current);
    this.calculateUrlPreview();

    if (this.props.mxEvent.getContent().format === "org.matrix.custom.html") {
      const blocks = _reactDom.default.findDOMNode(this).getElementsByTagName("code");

      if (blocks.length > 0) {
        // Do this asynchronously: parsing code takes time and we don't
        // need to block the DOM update on it.
        setTimeout(() => {
          if (this._unmounted) return;

          for (let i = 0; i < blocks.length; i++) {
            if (_SettingsStore.default.getValue("enableSyntaxHighlightLanguageDetection")) {
              _highlight.default.highlightBlock(blocks[i]);
            } else {
              // Only syntax highlight if there's a class starting with language-
              const classes = blocks[i].className.split(/\s+/).filter(function (cl) {
                return cl.startsWith('language-');
              });

              if (classes.length != 0) {
                _highlight.default.highlightBlock(blocks[i]);
              }
            }
          }
        }, 10);
      }

      this._addCodeCopyButton();
    }
  },

  componentDidUpdate: function (prevProps) {
    if (!this.props.editState) {
      const stoppedEditing = prevProps.editState && !this.props.editState;
      const messageWasEdited = prevProps.replacingEventId !== this.props.replacingEventId;

      if (messageWasEdited || stoppedEditing) {
        this._applyFormatting();
      }
    }
  },
  componentWillUnmount: function () {
    this._unmounted = true;
    (0, _pillify.unmountPills)(this._pills);
  },
  shouldComponentUpdate: function (nextProps, nextState) {
    //console.info("shouldComponentUpdate: ShowUrlPreview for %s is %s", this.props.mxEvent.getId(), this.props.showUrlPreview);
    // exploit that events are immutable :)
    return nextProps.mxEvent.getId() !== this.props.mxEvent.getId() || nextProps.highlights !== this.props.highlights || nextProps.replacingEventId !== this.props.replacingEventId || nextProps.highlightLink !== this.props.highlightLink || nextProps.showUrlPreview !== this.props.showUrlPreview || nextProps.editState !== this.props.editState || nextState.links !== this.state.links || nextState.editedMarkerHovered !== this.state.editedMarkerHovered || nextState.widgetHidden !== this.state.widgetHidden;
  },
  calculateUrlPreview: function () {
    //console.info("calculateUrlPreview: ShowUrlPreview for %s is %s", this.props.mxEvent.getId(), this.props.showUrlPreview);
    if (this.props.showUrlPreview) {
      // pass only the first child which is the event tile otherwise this recurses on edited events
      let links = this.findLinks([this._content.current]);

      if (links.length) {
        // de-dup the links (but preserve ordering)
        const seen = new Set();
        links = links.filter(link => {
          if (seen.has(link)) return false;
          seen.add(link);
          return true;
        });
        this.setState({
          links: links
        }); // lazy-load the hidden state of the preview widget from localstorage

        if (global.localStorage) {
          const hidden = global.localStorage.getItem("hide_preview_" + this.props.mxEvent.getId());
          this.setState({
            widgetHidden: hidden
          });
        }
      }
    }
  },
  activateSpoilers: function (nodes) {
    let node = nodes[0];

    while (node) {
      if (node.tagName === "SPAN" && typeof node.getAttribute("data-mx-spoiler") === "string") {
        const spoilerContainer = document.createElement('span');
        const reason = node.getAttribute("data-mx-spoiler");
        const Spoiler = sdk.getComponent('elements.Spoiler');
        node.removeAttribute("data-mx-spoiler"); // we don't want to recurse

        const spoiler = _react.default.createElement(Spoiler, {
          reason: reason,
          contentHtml: node.outerHTML
        });

        _reactDom.default.render(spoiler, spoilerContainer);

        node.parentNode.replaceChild(spoilerContainer, node);
        node = spoilerContainer;
      }

      if (node.childNodes && node.childNodes.length) {
        this.activateSpoilers(node.childNodes);
      }

      node = node.nextSibling;
    }
  },
  findLinks: function (nodes) {
    let links = [];

    for (let i = 0; i < nodes.length; i++) {
      const node = nodes[i];

      if (node.tagName === "A" && node.getAttribute("href")) {
        if (this.isLinkPreviewable(node)) {
          links.push(node.getAttribute("href"));
        }
      } else if (node.tagName === "PRE" || node.tagName === "CODE" || node.tagName === "BLOCKQUOTE") {
        continue;
      } else if (node.children && node.children.length) {
        links = links.concat(this.findLinks(node.children));
      }
    }

    return links;
  },
  isLinkPreviewable: function (node) {
    // don't try to preview relative links
    if (!node.getAttribute("href").startsWith("http://") && !node.getAttribute("href").startsWith("https://")) {
      return false;
    } // as a random heuristic to avoid highlighting things like "foo.pl"
    // we require the linked text to either include a / (either from http://
    // or from a full foo.bar/baz style schemeless URL) - or be a markdown-style
    // link, in which case we check the target text differs from the link value.
    // TODO: make this configurable?


    if (node.textContent.indexOf("/") > -1) {
      return true;
    } else {
      const url = node.getAttribute("href");
      const host = url.match(/^https?:\/\/(.*?)(\/|$)/)[1]; // never preview permalinks (if anything we should give a smart
      // preview of the room/user they point to: nobody needs to be reminded
      // what the matrix.to site looks like).

      if ((0, _Permalinks.isPermalinkHost)(host)) return false;

      if (node.textContent.toLowerCase().trim().startsWith(host.toLowerCase())) {
        // it's a "foo.pl" style link
        return false;
      } else {
        // it's a [foo bar](http://foo.com) style link
        return true;
      }
    }
  },

  _addCodeCopyButton() {
    // Add 'copy' buttons to pre blocks
    Array.from(_reactDom.default.findDOMNode(this).querySelectorAll('.mx_EventTile_body pre')).forEach(p => {
      const button = document.createElement("span");
      button.className = "mx_EventTile_copyButton";

      button.onclick = e => {
        const copyCode = button.parentNode.getElementsByTagName("pre")[0];
        const successful = this.copyToClipboard(copyCode.textContent);
        const buttonRect = e.target.getBoundingClientRect();
        const GenericTextContextMenu = sdk.getComponent('context_menus.GenericTextContextMenu');
        const {
          close
        } = ContextMenu.createMenu(GenericTextContextMenu, _objectSpread({}, (0, ContextMenu.toRightOf)(buttonRect, 2), {
          message: successful ? (0, _languageHandler._t)('Copied!') : (0, _languageHandler._t)('Failed to copy')
        }));
        e.target.onmouseleave = close;
      }; // Wrap a div around <pre> so that the copy button can be correctly positioned
      // when the <pre> overflows and is scrolled horizontally.


      const div = document.createElement("div");
      div.className = "mx_EventTile_pre_container"; // Insert containing div in place of <pre> block

      p.parentNode.replaceChild(div, p); // Append <pre> block and copy button to container

      div.appendChild(p);
      div.appendChild(button);
    });
  },

  onCancelClick: function (event) {
    this.setState({
      widgetHidden: true
    }); // FIXME: persist this somewhere smarter than local storage

    if (global.localStorage) {
      global.localStorage.setItem("hide_preview_" + this.props.mxEvent.getId(), "1");
    }

    this.forceUpdate();
  },
  onEmoteSenderClick: function (event) {
    const mxEvent = this.props.mxEvent;

    _dispatcher.default.dispatch({
      action: 'insert_mention',
      user_id: mxEvent.getSender()
    });
  },
  getEventTileOps: function () {
    return {
      isWidgetHidden: () => {
        return this.state.widgetHidden;
      },
      unhideWidget: () => {
        this.setState({
          widgetHidden: false
        });

        if (global.localStorage) {
          global.localStorage.removeItem("hide_preview_" + this.props.mxEvent.getId());
        }
      }
    };
  },
  onStarterLinkClick: function (starterLink, ev) {
    ev.preventDefault(); // We need to add on our scalar token to the starter link, but we may not have one!
    // In addition, we can't fetch one on click and then go to it immediately as that
    // is then treated as a popup!
    // We can get around this by fetching one now and showing a "confirmation dialog" (hurr hurr)
    // which requires the user to click through and THEN we can open the link in a new tab because
    // the window.open command occurs in the same stack frame as the onClick callback.

    const managers = _IntegrationManagers.IntegrationManagers.sharedInstance();

    if (!managers.hasManager()) {
      managers.openNoManagerDialog();
      return;
    } // Go fetch a scalar token


    const integrationManager = managers.getPrimaryManager();
    const scalarClient = integrationManager.getScalarClient();
    scalarClient.connect().then(() => {
      const completeUrl = scalarClient.getStarterLink(starterLink);
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
      const integrationsUrl = integrationManager.uiUrl;

      _Modal.default.createTrackedDialog('Add an integration', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Add an Integration"),
        description: _react.default.createElement("div", null, (0, _languageHandler._t)("You are about to be taken to a third-party site so you can " + "authenticate your account for use with %(integrationsUrl)s. " + "Do you wish to continue?", {
          integrationsUrl: integrationsUrl
        })),
        button: (0, _languageHandler._t)("Continue"),
        onFinished: function (confirmed) {
          if (!confirmed) {
            return;
          }

          const width = window.screen.width > 1024 ? 1024 : window.screen.width;
          const height = window.screen.height > 800 ? 800 : window.screen.height;
          const left = (window.screen.width - width) / 2;
          const top = (window.screen.height - height) / 2;
          const features = "height=".concat(height, ", width=").concat(width, ", top=").concat(top, ", left=").concat(left, ",");
          const wnd = window.open(completeUrl, '_blank', features);
          wnd.opener = null;
        }
      });
    });
  },
  _onMouseEnterEditedMarker: function () {
    this.setState({
      editedMarkerHovered: true
    });
  },
  _onMouseLeaveEditedMarker: function () {
    this.setState({
      editedMarkerHovered: false
    });
  },
  _openHistoryDialog: async function () {
    const MessageEditHistoryDialog = sdk.getComponent("views.dialogs.MessageEditHistoryDialog");

    _Modal.default.createDialog(MessageEditHistoryDialog, {
      mxEvent: this.props.mxEvent
    });
  },
  _renderEditedMarker: function () {
    let editedTooltip;

    if (this.state.editedMarkerHovered) {
      const Tooltip = sdk.getComponent('elements.Tooltip');
      const date = this.props.mxEvent.replacingEventDate();
      const dateString = date && (0, _DateUtils.formatDate)(date);
      editedTooltip = _react.default.createElement(Tooltip, {
        tooltipClassName: "mx_Tooltip_timeline",
        label: (0, _languageHandler._t)("Edited at %(date)s. Click to view edits.", {
          date: dateString
        })
      });
    }

    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement(AccessibleButton, {
      key: "editedMarker",
      className: "mx_EventTile_edited",
      onClick: this._openHistoryDialog,
      onMouseEnter: this._onMouseEnterEditedMarker,
      onMouseLeave: this._onMouseLeaveEditedMarker
    }, editedTooltip, _react.default.createElement("span", null, "(".concat((0, _languageHandler._t)("edited"), ")")));
  },
  render: function () {
    if (this.props.editState) {
      const EditMessageComposer = sdk.getComponent('rooms.EditMessageComposer');
      return _react.default.createElement(EditMessageComposer, {
        editState: this.props.editState,
        className: "mx_EventTile_content"
      });
    }

    const mxEvent = this.props.mxEvent;
    const content = mxEvent.getContent();

    const stripReply = _ReplyThread.default.getParentEventId(mxEvent);

    let body = HtmlUtils.bodyToHtml(content, this.props.highlights, {
      disableBigEmoji: content.msgtype === "m.emote" || !_SettingsStore.default.getValue('TextualBody.enableBigEmoji'),
      // Part of Replies fallback support
      stripReplyFallback: stripReply,
      ref: this._content
    });

    if (this.props.replacingEventId) {
      body = [body, this._renderEditedMarker()];
    }

    if (this.props.highlightLink) {
      body = _react.default.createElement("a", {
        href: this.props.highlightLink
      }, body);
    } else if (content.data && typeof content.data["org.matrix.neb.starter_link"] === "string") {
      body = _react.default.createElement("a", {
        href: "#",
        onClick: this.onStarterLinkClick.bind(this, content.data["org.matrix.neb.starter_link"])
      }, body);
    }

    let widgets;

    if (this.state.links.length && !this.state.widgetHidden && this.props.showUrlPreview) {
      const LinkPreviewWidget = sdk.getComponent('rooms.LinkPreviewWidget');
      widgets = this.state.links.map(link => {
        return _react.default.createElement(LinkPreviewWidget, {
          key: link,
          link: link,
          mxEvent: this.props.mxEvent,
          onCancelClick: this.onCancelClick,
          onHeightChanged: this.props.onHeightChanged
        });
      });
    }

    switch (content.msgtype) {
      case "m.emote":
        return _react.default.createElement("span", {
          className: "mx_MEmoteBody mx_EventTile_content"
        }, "*\xA0", _react.default.createElement("span", {
          className: "mx_MEmoteBody_sender",
          onClick: this.onEmoteSenderClick
        }, mxEvent.sender ? mxEvent.sender.name : mxEvent.getSender()), "\xA0", body, widgets);

      case "m.notice":
        return _react.default.createElement("span", {
          className: "mx_MNoticeBody mx_EventTile_content"
        }, body, widgets);

      default:
        // including "m.text"
        return _react.default.createElement("span", {
          className: "mx_MTextBody mx_EventTile_content"
        }, body, widgets);
    }
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL1RleHR1YWxCb2R5LmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwibXhFdmVudCIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJoaWdobGlnaHRzIiwiYXJyYXkiLCJoaWdobGlnaHRMaW5rIiwic3RyaW5nIiwic2hvd1VybFByZXZpZXciLCJib29sIiwib25IZWlnaHRDaGFuZ2VkIiwiZnVuYyIsInRpbGVTaGFwZSIsImdldEluaXRpYWxTdGF0ZSIsImxpbmtzIiwid2lkZ2V0SGlkZGVuIiwiY29weVRvQ2xpcGJvYXJkIiwidGV4dCIsInRleHRBcmVhIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwidmFsdWUiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJzZWxlY3QiLCJzdWNjZXNzZnVsIiwiZXhlY0NvbW1hbmQiLCJlcnIiLCJjb25zb2xlIiwibG9nIiwicmVtb3ZlQ2hpbGQiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX2NvbnRlbnQiLCJjb21wb25lbnREaWRNb3VudCIsIl91bm1vdW50ZWQiLCJfcGlsbHMiLCJwcm9wcyIsImVkaXRTdGF0ZSIsIl9hcHBseUZvcm1hdHRpbmciLCJhY3RpdmF0ZVNwb2lsZXJzIiwiY3VycmVudCIsIkh0bWxVdGlscyIsImxpbmtpZnlFbGVtZW50IiwiY2FsY3VsYXRlVXJsUHJldmlldyIsImdldENvbnRlbnQiLCJmb3JtYXQiLCJibG9ja3MiLCJSZWFjdERPTSIsImZpbmRET01Ob2RlIiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJsZW5ndGgiLCJzZXRUaW1lb3V0IiwiaSIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsImhpZ2hsaWdodCIsImhpZ2hsaWdodEJsb2NrIiwiY2xhc3NlcyIsImNsYXNzTmFtZSIsInNwbGl0IiwiZmlsdGVyIiwiY2wiLCJzdGFydHNXaXRoIiwiX2FkZENvZGVDb3B5QnV0dG9uIiwiY29tcG9uZW50RGlkVXBkYXRlIiwicHJldlByb3BzIiwic3RvcHBlZEVkaXRpbmciLCJtZXNzYWdlV2FzRWRpdGVkIiwicmVwbGFjaW5nRXZlbnRJZCIsImNvbXBvbmVudFdpbGxVbm1vdW50Iiwic2hvdWxkQ29tcG9uZW50VXBkYXRlIiwibmV4dFByb3BzIiwibmV4dFN0YXRlIiwiZ2V0SWQiLCJzdGF0ZSIsImVkaXRlZE1hcmtlckhvdmVyZWQiLCJmaW5kTGlua3MiLCJzZWVuIiwiU2V0IiwibGluayIsImhhcyIsImFkZCIsInNldFN0YXRlIiwiZ2xvYmFsIiwibG9jYWxTdG9yYWdlIiwiaGlkZGVuIiwiZ2V0SXRlbSIsIm5vZGVzIiwibm9kZSIsInRhZ05hbWUiLCJnZXRBdHRyaWJ1dGUiLCJzcG9pbGVyQ29udGFpbmVyIiwicmVhc29uIiwiU3BvaWxlciIsInNkayIsImdldENvbXBvbmVudCIsInJlbW92ZUF0dHJpYnV0ZSIsInNwb2lsZXIiLCJvdXRlckhUTUwiLCJyZW5kZXIiLCJwYXJlbnROb2RlIiwicmVwbGFjZUNoaWxkIiwiY2hpbGROb2RlcyIsIm5leHRTaWJsaW5nIiwiaXNMaW5rUHJldmlld2FibGUiLCJwdXNoIiwiY2hpbGRyZW4iLCJjb25jYXQiLCJ0ZXh0Q29udGVudCIsImluZGV4T2YiLCJ1cmwiLCJob3N0IiwibWF0Y2giLCJ0b0xvd2VyQ2FzZSIsInRyaW0iLCJBcnJheSIsImZyb20iLCJxdWVyeVNlbGVjdG9yQWxsIiwiZm9yRWFjaCIsInAiLCJidXR0b24iLCJvbmNsaWNrIiwiZSIsImNvcHlDb2RlIiwiYnV0dG9uUmVjdCIsInRhcmdldCIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsIkdlbmVyaWNUZXh0Q29udGV4dE1lbnUiLCJjbG9zZSIsIkNvbnRleHRNZW51IiwiY3JlYXRlTWVudSIsIm1lc3NhZ2UiLCJvbm1vdXNlbGVhdmUiLCJkaXYiLCJvbkNhbmNlbENsaWNrIiwiZXZlbnQiLCJzZXRJdGVtIiwiZm9yY2VVcGRhdGUiLCJvbkVtb3RlU2VuZGVyQ2xpY2siLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInVzZXJfaWQiLCJnZXRTZW5kZXIiLCJnZXRFdmVudFRpbGVPcHMiLCJpc1dpZGdldEhpZGRlbiIsInVuaGlkZVdpZGdldCIsInJlbW92ZUl0ZW0iLCJvblN0YXJ0ZXJMaW5rQ2xpY2siLCJzdGFydGVyTGluayIsImV2IiwicHJldmVudERlZmF1bHQiLCJtYW5hZ2VycyIsIkludGVncmF0aW9uTWFuYWdlcnMiLCJzaGFyZWRJbnN0YW5jZSIsImhhc01hbmFnZXIiLCJvcGVuTm9NYW5hZ2VyRGlhbG9nIiwiaW50ZWdyYXRpb25NYW5hZ2VyIiwiZ2V0UHJpbWFyeU1hbmFnZXIiLCJzY2FsYXJDbGllbnQiLCJnZXRTY2FsYXJDbGllbnQiLCJjb25uZWN0IiwidGhlbiIsImNvbXBsZXRlVXJsIiwiZ2V0U3RhcnRlckxpbmsiLCJRdWVzdGlvbkRpYWxvZyIsImludGVncmF0aW9uc1VybCIsInVpVXJsIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIm9uRmluaXNoZWQiLCJjb25maXJtZWQiLCJ3aWR0aCIsIndpbmRvdyIsInNjcmVlbiIsImhlaWdodCIsImxlZnQiLCJ0b3AiLCJmZWF0dXJlcyIsInduZCIsIm9wZW4iLCJvcGVuZXIiLCJfb25Nb3VzZUVudGVyRWRpdGVkTWFya2VyIiwiX29uTW91c2VMZWF2ZUVkaXRlZE1hcmtlciIsIl9vcGVuSGlzdG9yeURpYWxvZyIsIk1lc3NhZ2VFZGl0SGlzdG9yeURpYWxvZyIsImNyZWF0ZURpYWxvZyIsIl9yZW5kZXJFZGl0ZWRNYXJrZXIiLCJlZGl0ZWRUb29sdGlwIiwiVG9vbHRpcCIsImRhdGUiLCJyZXBsYWNpbmdFdmVudERhdGUiLCJkYXRlU3RyaW5nIiwiQWNjZXNzaWJsZUJ1dHRvbiIsIkVkaXRNZXNzYWdlQ29tcG9zZXIiLCJjb250ZW50Iiwic3RyaXBSZXBseSIsIlJlcGx5VGhyZWFkIiwiZ2V0UGFyZW50RXZlbnRJZCIsImJvZHlUb0h0bWwiLCJkaXNhYmxlQmlnRW1vamkiLCJtc2d0eXBlIiwic3RyaXBSZXBseUZhbGxiYWNrIiwicmVmIiwiZGF0YSIsImJpbmQiLCJ3aWRnZXRzIiwiTGlua1ByZXZpZXdXaWRnZXQiLCJtYXAiLCJzZW5kZXIiLCJuYW1lIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7ZUFHZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxhQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBQyxJQUFBQSxPQUFPLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQUZuQjs7QUFJUDtBQUNBQyxJQUFBQSxVQUFVLEVBQUVILG1CQUFVSSxLQUxmOztBQU9QO0FBQ0FDLElBQUFBLGFBQWEsRUFBRUwsbUJBQVVNLE1BUmxCOztBQVVQO0FBQ0FDLElBQUFBLGNBQWMsRUFBRVAsbUJBQVVRLElBWG5COztBQWFQO0FBQ0FDLElBQUFBLGVBQWUsRUFBRVQsbUJBQVVVLElBZHBCOztBQWdCUDtBQUNBQyxJQUFBQSxTQUFTLEVBQUVYLG1CQUFVTTtBQWpCZCxHQUhpQjtBQXVCNUJNLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSDtBQUNBO0FBQ0FDLE1BQUFBLEtBQUssRUFBRSxFQUhKO0FBS0g7QUFDQUMsTUFBQUEsWUFBWSxFQUFFO0FBTlgsS0FBUDtBQVFILEdBaEMyQjtBQWtDNUJDLEVBQUFBLGVBQWUsRUFBRSxVQUFTQyxJQUFULEVBQWU7QUFDNUIsVUFBTUMsUUFBUSxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsVUFBdkIsQ0FBakI7QUFDQUYsSUFBQUEsUUFBUSxDQUFDRyxLQUFULEdBQWlCSixJQUFqQjtBQUNBRSxJQUFBQSxRQUFRLENBQUNHLElBQVQsQ0FBY0MsV0FBZCxDQUEwQkwsUUFBMUI7QUFDQUEsSUFBQUEsUUFBUSxDQUFDTSxNQUFUO0FBRUEsUUFBSUMsVUFBVSxHQUFHLEtBQWpCOztBQUNBLFFBQUk7QUFDQUEsTUFBQUEsVUFBVSxHQUFHTixRQUFRLENBQUNPLFdBQVQsQ0FBcUIsTUFBckIsQ0FBYjtBQUNILEtBRkQsQ0FFRSxPQUFPQyxHQUFQLEVBQVk7QUFDVkMsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksZ0JBQVo7QUFDSDs7QUFFRFYsSUFBQUEsUUFBUSxDQUFDRyxJQUFULENBQWNRLFdBQWQsQ0FBMEJaLFFBQTFCO0FBQ0EsV0FBT08sVUFBUDtBQUNILEdBakQyQjtBQW1ENUI7QUFDQU0sRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLQyxRQUFMLEdBQWdCLHVCQUFoQjtBQUNILEdBdEQyQjtBQXdENUJDLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsU0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxFQUFkOztBQUNBLFFBQUksQ0FBQyxLQUFLQyxLQUFMLENBQVdDLFNBQWhCLEVBQTJCO0FBQ3ZCLFdBQUtDLGdCQUFMO0FBQ0g7QUFDSixHQTlEMkI7O0FBZ0U1QkEsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixTQUFLQyxnQkFBTCxDQUFzQixDQUFDLEtBQUtQLFFBQUwsQ0FBY1EsT0FBZixDQUF0QixFQURlLENBR2Y7QUFDQTtBQUNBOztBQUNBLCtCQUFhLENBQUMsS0FBS1IsUUFBTCxDQUFjUSxPQUFmLENBQWIsRUFBc0MsS0FBS0osS0FBTCxDQUFXcEMsT0FBakQsRUFBMEQsS0FBS21DLE1BQS9EO0FBQ0FNLElBQUFBLFNBQVMsQ0FBQ0MsY0FBVixDQUF5QixLQUFLVixRQUFMLENBQWNRLE9BQXZDO0FBQ0EsU0FBS0csbUJBQUw7O0FBRUEsUUFBSSxLQUFLUCxLQUFMLENBQVdwQyxPQUFYLENBQW1CNEMsVUFBbkIsR0FBZ0NDLE1BQWhDLEtBQTJDLHdCQUEvQyxFQUF5RTtBQUNyRSxZQUFNQyxNQUFNLEdBQUdDLGtCQUFTQyxXQUFULENBQXFCLElBQXJCLEVBQTJCQyxvQkFBM0IsQ0FBZ0QsTUFBaEQsQ0FBZjs7QUFDQSxVQUFJSCxNQUFNLENBQUNJLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkI7QUFDQTtBQUNBQyxRQUFBQSxVQUFVLENBQUMsTUFBTTtBQUNiLGNBQUksS0FBS2pCLFVBQVQsRUFBcUI7O0FBQ3JCLGVBQUssSUFBSWtCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdOLE1BQU0sQ0FBQ0ksTUFBM0IsRUFBbUNFLENBQUMsRUFBcEMsRUFBd0M7QUFDcEMsZ0JBQUlDLHVCQUFjQyxRQUFkLENBQXVCLHdDQUF2QixDQUFKLEVBQXNFO0FBQ2xFQyxpQ0FBVUMsY0FBVixDQUF5QlYsTUFBTSxDQUFDTSxDQUFELENBQS9CO0FBQ0gsYUFGRCxNQUVPO0FBQ0g7QUFDQSxvQkFBTUssT0FBTyxHQUFHWCxNQUFNLENBQUNNLENBQUQsQ0FBTixDQUFVTSxTQUFWLENBQW9CQyxLQUFwQixDQUEwQixLQUExQixFQUFpQ0MsTUFBakMsQ0FBd0MsVUFBU0MsRUFBVCxFQUFhO0FBQ2pFLHVCQUFPQSxFQUFFLENBQUNDLFVBQUgsQ0FBYyxXQUFkLENBQVA7QUFDSCxlQUZlLENBQWhCOztBQUlBLGtCQUFJTCxPQUFPLENBQUNQLE1BQVIsSUFBa0IsQ0FBdEIsRUFBeUI7QUFDckJLLG1DQUFVQyxjQUFWLENBQXlCVixNQUFNLENBQUNNLENBQUQsQ0FBL0I7QUFDSDtBQUNKO0FBQ0o7QUFDSixTQWhCUyxFQWdCUCxFQWhCTyxDQUFWO0FBaUJIOztBQUNELFdBQUtXLGtCQUFMO0FBQ0g7QUFDSixHQW5HMkI7O0FBcUc1QkMsRUFBQUEsa0JBQWtCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQjtBQUNwQyxRQUFJLENBQUMsS0FBSzdCLEtBQUwsQ0FBV0MsU0FBaEIsRUFBMkI7QUFDdkIsWUFBTTZCLGNBQWMsR0FBR0QsU0FBUyxDQUFDNUIsU0FBVixJQUF1QixDQUFDLEtBQUtELEtBQUwsQ0FBV0MsU0FBMUQ7QUFDQSxZQUFNOEIsZ0JBQWdCLEdBQUdGLFNBQVMsQ0FBQ0csZ0JBQVYsS0FBK0IsS0FBS2hDLEtBQUwsQ0FBV2dDLGdCQUFuRTs7QUFDQSxVQUFJRCxnQkFBZ0IsSUFBSUQsY0FBeEIsRUFBd0M7QUFDcEMsYUFBSzVCLGdCQUFMO0FBQ0g7QUFDSjtBQUNKLEdBN0cyQjtBQStHNUIrQixFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFNBQUtuQyxVQUFMLEdBQWtCLElBQWxCO0FBQ0EsK0JBQWEsS0FBS0MsTUFBbEI7QUFDSCxHQWxIMkI7QUFvSDVCbUMsRUFBQUEscUJBQXFCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQkMsU0FBcEIsRUFBK0I7QUFDbEQ7QUFFQTtBQUNBLFdBQVFELFNBQVMsQ0FBQ3ZFLE9BQVYsQ0FBa0J5RSxLQUFsQixPQUE4QixLQUFLckMsS0FBTCxDQUFXcEMsT0FBWCxDQUFtQnlFLEtBQW5CLEVBQTlCLElBQ0FGLFNBQVMsQ0FBQ25FLFVBQVYsS0FBeUIsS0FBS2dDLEtBQUwsQ0FBV2hDLFVBRHBDLElBRUFtRSxTQUFTLENBQUNILGdCQUFWLEtBQStCLEtBQUtoQyxLQUFMLENBQVdnQyxnQkFGMUMsSUFHQUcsU0FBUyxDQUFDakUsYUFBVixLQUE0QixLQUFLOEIsS0FBTCxDQUFXOUIsYUFIdkMsSUFJQWlFLFNBQVMsQ0FBQy9ELGNBQVYsS0FBNkIsS0FBSzRCLEtBQUwsQ0FBVzVCLGNBSnhDLElBS0ErRCxTQUFTLENBQUNsQyxTQUFWLEtBQXdCLEtBQUtELEtBQUwsQ0FBV0MsU0FMbkMsSUFNQW1DLFNBQVMsQ0FBQzFELEtBQVYsS0FBb0IsS0FBSzRELEtBQUwsQ0FBVzVELEtBTi9CLElBT0EwRCxTQUFTLENBQUNHLG1CQUFWLEtBQWtDLEtBQUtELEtBQUwsQ0FBV0MsbUJBUDdDLElBUUFILFNBQVMsQ0FBQ3pELFlBQVYsS0FBMkIsS0FBSzJELEtBQUwsQ0FBVzNELFlBUjlDO0FBU0gsR0FqSTJCO0FBbUk1QjRCLEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUI7QUFFQSxRQUFJLEtBQUtQLEtBQUwsQ0FBVzVCLGNBQWYsRUFBK0I7QUFDM0I7QUFDQSxVQUFJTSxLQUFLLEdBQUcsS0FBSzhELFNBQUwsQ0FBZSxDQUFDLEtBQUs1QyxRQUFMLENBQWNRLE9BQWYsQ0FBZixDQUFaOztBQUNBLFVBQUkxQixLQUFLLENBQUNvQyxNQUFWLEVBQWtCO0FBQ2Q7QUFDQSxjQUFNMkIsSUFBSSxHQUFHLElBQUlDLEdBQUosRUFBYjtBQUNBaEUsUUFBQUEsS0FBSyxHQUFHQSxLQUFLLENBQUM4QyxNQUFOLENBQWNtQixJQUFELElBQVU7QUFDM0IsY0FBSUYsSUFBSSxDQUFDRyxHQUFMLENBQVNELElBQVQsQ0FBSixFQUFvQixPQUFPLEtBQVA7QUFDcEJGLFVBQUFBLElBQUksQ0FBQ0ksR0FBTCxDQUFTRixJQUFUO0FBQ0EsaUJBQU8sSUFBUDtBQUNILFNBSk8sQ0FBUjtBQU1BLGFBQUtHLFFBQUwsQ0FBYztBQUFFcEUsVUFBQUEsS0FBSyxFQUFFQTtBQUFULFNBQWQsRUFUYyxDQVdkOztBQUNBLFlBQUlxRSxNQUFNLENBQUNDLFlBQVgsRUFBeUI7QUFDckIsZ0JBQU1DLE1BQU0sR0FBR0YsTUFBTSxDQUFDQyxZQUFQLENBQW9CRSxPQUFwQixDQUE0QixrQkFBa0IsS0FBS2xELEtBQUwsQ0FBV3BDLE9BQVgsQ0FBbUJ5RSxLQUFuQixFQUE5QyxDQUFmO0FBQ0EsZUFBS1MsUUFBTCxDQUFjO0FBQUVuRSxZQUFBQSxZQUFZLEVBQUVzRTtBQUFoQixXQUFkO0FBQ0g7QUFDSjtBQUNKO0FBQ0osR0EzSjJCO0FBNko1QjlDLEVBQUFBLGdCQUFnQixFQUFFLFVBQVNnRCxLQUFULEVBQWdCO0FBQzlCLFFBQUlDLElBQUksR0FBR0QsS0FBSyxDQUFDLENBQUQsQ0FBaEI7O0FBQ0EsV0FBT0MsSUFBUCxFQUFhO0FBQ1QsVUFBSUEsSUFBSSxDQUFDQyxPQUFMLEtBQWlCLE1BQWpCLElBQTJCLE9BQU9ELElBQUksQ0FBQ0UsWUFBTCxDQUFrQixpQkFBbEIsQ0FBUCxLQUFnRCxRQUEvRSxFQUF5RjtBQUNyRixjQUFNQyxnQkFBZ0IsR0FBR3hFLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixNQUF2QixDQUF6QjtBQUVBLGNBQU13RSxNQUFNLEdBQUdKLElBQUksQ0FBQ0UsWUFBTCxDQUFrQixpQkFBbEIsQ0FBZjtBQUNBLGNBQU1HLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBUCxRQUFBQSxJQUFJLENBQUNRLGVBQUwsQ0FBcUIsaUJBQXJCLEVBTHFGLENBSzVDOztBQUN6QyxjQUFNQyxPQUFPLEdBQUcsNkJBQUMsT0FBRDtBQUNaLFVBQUEsTUFBTSxFQUFFTCxNQURJO0FBRVosVUFBQSxXQUFXLEVBQUVKLElBQUksQ0FBQ1U7QUFGTixVQUFoQjs7QUFLQW5ELDBCQUFTb0QsTUFBVCxDQUFnQkYsT0FBaEIsRUFBeUJOLGdCQUF6Qjs7QUFDQUgsUUFBQUEsSUFBSSxDQUFDWSxVQUFMLENBQWdCQyxZQUFoQixDQUE2QlYsZ0JBQTdCLEVBQStDSCxJQUEvQztBQUVBQSxRQUFBQSxJQUFJLEdBQUdHLGdCQUFQO0FBQ0g7O0FBRUQsVUFBSUgsSUFBSSxDQUFDYyxVQUFMLElBQW1CZCxJQUFJLENBQUNjLFVBQUwsQ0FBZ0JwRCxNQUF2QyxFQUErQztBQUMzQyxhQUFLWCxnQkFBTCxDQUFzQmlELElBQUksQ0FBQ2MsVUFBM0I7QUFDSDs7QUFFRGQsTUFBQUEsSUFBSSxHQUFHQSxJQUFJLENBQUNlLFdBQVo7QUFDSDtBQUNKLEdBdkwyQjtBQXlMNUIzQixFQUFBQSxTQUFTLEVBQUUsVUFBU1csS0FBVCxFQUFnQjtBQUN2QixRQUFJekUsS0FBSyxHQUFHLEVBQVo7O0FBRUEsU0FBSyxJQUFJc0MsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR21DLEtBQUssQ0FBQ3JDLE1BQTFCLEVBQWtDRSxDQUFDLEVBQW5DLEVBQXVDO0FBQ25DLFlBQU1vQyxJQUFJLEdBQUdELEtBQUssQ0FBQ25DLENBQUQsQ0FBbEI7O0FBQ0EsVUFBSW9DLElBQUksQ0FBQ0MsT0FBTCxLQUFpQixHQUFqQixJQUF3QkQsSUFBSSxDQUFDRSxZQUFMLENBQWtCLE1BQWxCLENBQTVCLEVBQXVEO0FBQ25ELFlBQUksS0FBS2MsaUJBQUwsQ0FBdUJoQixJQUF2QixDQUFKLEVBQWtDO0FBQzlCMUUsVUFBQUEsS0FBSyxDQUFDMkYsSUFBTixDQUFXakIsSUFBSSxDQUFDRSxZQUFMLENBQWtCLE1BQWxCLENBQVg7QUFDSDtBQUNKLE9BSkQsTUFJTyxJQUFJRixJQUFJLENBQUNDLE9BQUwsS0FBaUIsS0FBakIsSUFBMEJELElBQUksQ0FBQ0MsT0FBTCxLQUFpQixNQUEzQyxJQUNIRCxJQUFJLENBQUNDLE9BQUwsS0FBaUIsWUFEbEIsRUFDZ0M7QUFDbkM7QUFDSCxPQUhNLE1BR0EsSUFBSUQsSUFBSSxDQUFDa0IsUUFBTCxJQUFpQmxCLElBQUksQ0FBQ2tCLFFBQUwsQ0FBY3hELE1BQW5DLEVBQTJDO0FBQzlDcEMsUUFBQUEsS0FBSyxHQUFHQSxLQUFLLENBQUM2RixNQUFOLENBQWEsS0FBSy9CLFNBQUwsQ0FBZVksSUFBSSxDQUFDa0IsUUFBcEIsQ0FBYixDQUFSO0FBQ0g7QUFDSjs7QUFDRCxXQUFPNUYsS0FBUDtBQUNILEdBMU0yQjtBQTRNNUIwRixFQUFBQSxpQkFBaUIsRUFBRSxVQUFTaEIsSUFBVCxFQUFlO0FBQzlCO0FBQ0EsUUFBSSxDQUFDQSxJQUFJLENBQUNFLFlBQUwsQ0FBa0IsTUFBbEIsRUFBMEI1QixVQUExQixDQUFxQyxTQUFyQyxDQUFELElBQ0EsQ0FBQzBCLElBQUksQ0FBQ0UsWUFBTCxDQUFrQixNQUFsQixFQUEwQjVCLFVBQTFCLENBQXFDLFVBQXJDLENBREwsRUFDdUQ7QUFDbkQsYUFBTyxLQUFQO0FBQ0gsS0FMNkIsQ0FPOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSTBCLElBQUksQ0FBQ29CLFdBQUwsQ0FBaUJDLE9BQWpCLENBQXlCLEdBQXpCLElBQWdDLENBQUMsQ0FBckMsRUFBd0M7QUFDcEMsYUFBTyxJQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsWUFBTUMsR0FBRyxHQUFHdEIsSUFBSSxDQUFDRSxZQUFMLENBQWtCLE1BQWxCLENBQVo7QUFDQSxZQUFNcUIsSUFBSSxHQUFHRCxHQUFHLENBQUNFLEtBQUosQ0FBVSx5QkFBVixFQUFxQyxDQUFyQyxDQUFiLENBRkcsQ0FJSDtBQUNBO0FBQ0E7O0FBQ0EsVUFBSSxpQ0FBZ0JELElBQWhCLENBQUosRUFBMkIsT0FBTyxLQUFQOztBQUUzQixVQUFJdkIsSUFBSSxDQUFDb0IsV0FBTCxDQUFpQkssV0FBakIsR0FBK0JDLElBQS9CLEdBQXNDcEQsVUFBdEMsQ0FBaURpRCxJQUFJLENBQUNFLFdBQUwsRUFBakQsQ0FBSixFQUEwRTtBQUN0RTtBQUNBLGVBQU8sS0FBUDtBQUNILE9BSEQsTUFHTztBQUNIO0FBQ0EsZUFBTyxJQUFQO0FBQ0g7QUFDSjtBQUNKLEdBM08yQjs7QUE2TzVCbEQsRUFBQUEsa0JBQWtCLEdBQUc7QUFDakI7QUFDQW9ELElBQUFBLEtBQUssQ0FBQ0MsSUFBTixDQUFXckUsa0JBQVNDLFdBQVQsQ0FBcUIsSUFBckIsRUFBMkJxRSxnQkFBM0IsQ0FBNEMsd0JBQTVDLENBQVgsRUFBa0ZDLE9BQWxGLENBQTJGQyxDQUFELElBQU87QUFDN0YsWUFBTUMsTUFBTSxHQUFHckcsUUFBUSxDQUFDQyxhQUFULENBQXVCLE1BQXZCLENBQWY7QUFDQW9HLE1BQUFBLE1BQU0sQ0FBQzlELFNBQVAsR0FBbUIseUJBQW5COztBQUNBOEQsTUFBQUEsTUFBTSxDQUFDQyxPQUFQLEdBQWtCQyxDQUFELElBQU87QUFDcEIsY0FBTUMsUUFBUSxHQUFHSCxNQUFNLENBQUNwQixVQUFQLENBQWtCbkQsb0JBQWxCLENBQXVDLEtBQXZDLEVBQThDLENBQTlDLENBQWpCO0FBQ0EsY0FBTXhCLFVBQVUsR0FBRyxLQUFLVCxlQUFMLENBQXFCMkcsUUFBUSxDQUFDZixXQUE5QixDQUFuQjtBQUVBLGNBQU1nQixVQUFVLEdBQUdGLENBQUMsQ0FBQ0csTUFBRixDQUFTQyxxQkFBVCxFQUFuQjtBQUNBLGNBQU1DLHNCQUFzQixHQUFHakMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNDQUFqQixDQUEvQjtBQUNBLGNBQU07QUFBQ2lDLFVBQUFBO0FBQUQsWUFBVUMsV0FBVyxDQUFDQyxVQUFaLENBQXVCSCxzQkFBdkIsb0JBQ1QsMkJBQVVILFVBQVYsRUFBc0IsQ0FBdEIsQ0FEUztBQUVaTyxVQUFBQSxPQUFPLEVBQUUxRyxVQUFVLEdBQUcseUJBQUcsU0FBSCxDQUFILEdBQW1CLHlCQUFHLGdCQUFIO0FBRjFCLFdBQWhCO0FBSUFpRyxRQUFBQSxDQUFDLENBQUNHLE1BQUYsQ0FBU08sWUFBVCxHQUF3QkosS0FBeEI7QUFDSCxPQVhELENBSDZGLENBZ0I3RjtBQUNBOzs7QUFDQSxZQUFNSyxHQUFHLEdBQUdsSCxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWjtBQUNBaUgsTUFBQUEsR0FBRyxDQUFDM0UsU0FBSixHQUFnQiw0QkFBaEIsQ0FuQjZGLENBcUI3Rjs7QUFDQTZELE1BQUFBLENBQUMsQ0FBQ25CLFVBQUYsQ0FBYUMsWUFBYixDQUEwQmdDLEdBQTFCLEVBQStCZCxDQUEvQixFQXRCNkYsQ0F3QjdGOztBQUNBYyxNQUFBQSxHQUFHLENBQUM5RyxXQUFKLENBQWdCZ0csQ0FBaEI7QUFDQWMsTUFBQUEsR0FBRyxDQUFDOUcsV0FBSixDQUFnQmlHLE1BQWhCO0FBQ0gsS0EzQkQ7QUE0QkgsR0EzUTJCOztBQTZRNUJjLEVBQUFBLGFBQWEsRUFBRSxVQUFTQyxLQUFULEVBQWdCO0FBQzNCLFNBQUtyRCxRQUFMLENBQWM7QUFBRW5FLE1BQUFBLFlBQVksRUFBRTtBQUFoQixLQUFkLEVBRDJCLENBRTNCOztBQUNBLFFBQUlvRSxNQUFNLENBQUNDLFlBQVgsRUFBeUI7QUFDckJELE1BQUFBLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQm9ELE9BQXBCLENBQTRCLGtCQUFrQixLQUFLcEcsS0FBTCxDQUFXcEMsT0FBWCxDQUFtQnlFLEtBQW5CLEVBQTlDLEVBQTBFLEdBQTFFO0FBQ0g7O0FBQ0QsU0FBS2dFLFdBQUw7QUFDSCxHQXBSMkI7QUFzUjVCQyxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTSCxLQUFULEVBQWdCO0FBQ2hDLFVBQU12SSxPQUFPLEdBQUcsS0FBS29DLEtBQUwsQ0FBV3BDLE9BQTNCOztBQUNBMkksd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsZ0JBREM7QUFFVEMsTUFBQUEsT0FBTyxFQUFFOUksT0FBTyxDQUFDK0ksU0FBUjtBQUZBLEtBQWI7QUFJSCxHQTVSMkI7QUE4UjVCQyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLGNBQWMsRUFBRSxNQUFNO0FBQ2xCLGVBQU8sS0FBS3ZFLEtBQUwsQ0FBVzNELFlBQWxCO0FBQ0gsT0FIRTtBQUtIbUksTUFBQUEsWUFBWSxFQUFFLE1BQU07QUFDaEIsYUFBS2hFLFFBQUwsQ0FBYztBQUFFbkUsVUFBQUEsWUFBWSxFQUFFO0FBQWhCLFNBQWQ7O0FBQ0EsWUFBSW9FLE1BQU0sQ0FBQ0MsWUFBWCxFQUF5QjtBQUNyQkQsVUFBQUEsTUFBTSxDQUFDQyxZQUFQLENBQW9CK0QsVUFBcEIsQ0FBK0Isa0JBQWtCLEtBQUsvRyxLQUFMLENBQVdwQyxPQUFYLENBQW1CeUUsS0FBbkIsRUFBakQ7QUFDSDtBQUNKO0FBVkUsS0FBUDtBQVlILEdBM1MyQjtBQTZTNUIyRSxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTQyxXQUFULEVBQXNCQyxFQUF0QixFQUEwQjtBQUMxQ0EsSUFBQUEsRUFBRSxDQUFDQyxjQUFILEdBRDBDLENBRTFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxVQUFNQyxRQUFRLEdBQUdDLHlDQUFvQkMsY0FBcEIsRUFBakI7O0FBQ0EsUUFBSSxDQUFDRixRQUFRLENBQUNHLFVBQVQsRUFBTCxFQUE0QjtBQUN4QkgsTUFBQUEsUUFBUSxDQUFDSSxtQkFBVDtBQUNBO0FBQ0gsS0FieUMsQ0FlMUM7OztBQUNBLFVBQU1DLGtCQUFrQixHQUFHTCxRQUFRLENBQUNNLGlCQUFULEVBQTNCO0FBQ0EsVUFBTUMsWUFBWSxHQUFHRixrQkFBa0IsQ0FBQ0csZUFBbkIsRUFBckI7QUFDQUQsSUFBQUEsWUFBWSxDQUFDRSxPQUFiLEdBQXVCQyxJQUF2QixDQUE0QixNQUFNO0FBQzlCLFlBQU1DLFdBQVcsR0FBR0osWUFBWSxDQUFDSyxjQUFiLENBQTRCZixXQUE1QixDQUFwQjtBQUNBLFlBQU1nQixjQUFjLEdBQUd2RSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCO0FBQ0EsWUFBTXVFLGVBQWUsR0FBR1Qsa0JBQWtCLENBQUNVLEtBQTNDOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsb0JBQTFCLEVBQWdELEVBQWhELEVBQW9ESixjQUFwRCxFQUFvRTtBQUNoRUssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLG9CQUFILENBRHlEO0FBRWhFQyxRQUFBQSxXQUFXLEVBQ1AsMENBQ00seUJBQUcsZ0VBQ0QsOERBREMsR0FFRCwwQkFGRixFQUU4QjtBQUFFTCxVQUFBQSxlQUFlLEVBQUVBO0FBQW5CLFNBRjlCLENBRE4sQ0FINEQ7QUFRaEU5QyxRQUFBQSxNQUFNLEVBQUUseUJBQUcsVUFBSCxDQVJ3RDtBQVNoRW9ELFFBQUFBLFVBQVUsRUFBRSxVQUFTQyxTQUFULEVBQW9CO0FBQzVCLGNBQUksQ0FBQ0EsU0FBTCxFQUFnQjtBQUNaO0FBQ0g7O0FBQ0QsZ0JBQU1DLEtBQUssR0FBR0MsTUFBTSxDQUFDQyxNQUFQLENBQWNGLEtBQWQsR0FBc0IsSUFBdEIsR0FBNkIsSUFBN0IsR0FBb0NDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjRixLQUFoRTtBQUNBLGdCQUFNRyxNQUFNLEdBQUdGLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjQyxNQUFkLEdBQXVCLEdBQXZCLEdBQTZCLEdBQTdCLEdBQW1DRixNQUFNLENBQUNDLE1BQVAsQ0FBY0MsTUFBaEU7QUFDQSxnQkFBTUMsSUFBSSxHQUFHLENBQUNILE1BQU0sQ0FBQ0MsTUFBUCxDQUFjRixLQUFkLEdBQXNCQSxLQUF2QixJQUFnQyxDQUE3QztBQUNBLGdCQUFNSyxHQUFHLEdBQUcsQ0FBQ0osTUFBTSxDQUFDQyxNQUFQLENBQWNDLE1BQWQsR0FBdUJBLE1BQXhCLElBQWtDLENBQTlDO0FBQ0EsZ0JBQU1HLFFBQVEsb0JBQWFILE1BQWIscUJBQThCSCxLQUE5QixtQkFBNENLLEdBQTVDLG9CQUF5REQsSUFBekQsTUFBZDtBQUNBLGdCQUFNRyxHQUFHLEdBQUdOLE1BQU0sQ0FBQ08sSUFBUCxDQUFZbkIsV0FBWixFQUF5QixRQUF6QixFQUFtQ2lCLFFBQW5DLENBQVo7QUFDQUMsVUFBQUEsR0FBRyxDQUFDRSxNQUFKLEdBQWEsSUFBYjtBQUNIO0FBcEIrRCxPQUFwRTtBQXNCSCxLQTFCRDtBQTJCSCxHQTFWMkI7QUE0VjVCQyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUt0RyxRQUFMLENBQWM7QUFBQ1AsTUFBQUEsbUJBQW1CLEVBQUU7QUFBdEIsS0FBZDtBQUNILEdBOVYyQjtBQWdXNUI4RyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUt2RyxRQUFMLENBQWM7QUFBQ1AsTUFBQUEsbUJBQW1CLEVBQUU7QUFBdEIsS0FBZDtBQUNILEdBbFcyQjtBQW9XNUIrRyxFQUFBQSxrQkFBa0IsRUFBRSxrQkFBaUI7QUFDakMsVUFBTUMsd0JBQXdCLEdBQUc3RixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0NBQWpCLENBQWpDOztBQUNBeUUsbUJBQU1vQixZQUFOLENBQW1CRCx3QkFBbkIsRUFBNkM7QUFBQzNMLE1BQUFBLE9BQU8sRUFBRSxLQUFLb0MsS0FBTCxDQUFXcEM7QUFBckIsS0FBN0M7QUFDSCxHQXZXMkI7QUF5VzVCNkwsRUFBQUEsbUJBQW1CLEVBQUUsWUFBVztBQUM1QixRQUFJQyxhQUFKOztBQUNBLFFBQUksS0FBS3BILEtBQUwsQ0FBV0MsbUJBQWYsRUFBb0M7QUFDaEMsWUFBTW9ILE9BQU8sR0FBR2pHLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxZQUFNaUcsSUFBSSxHQUFHLEtBQUs1SixLQUFMLENBQVdwQyxPQUFYLENBQW1CaU0sa0JBQW5CLEVBQWI7QUFDQSxZQUFNQyxVQUFVLEdBQUdGLElBQUksSUFBSSwyQkFBV0EsSUFBWCxDQUEzQjtBQUNBRixNQUFBQSxhQUFhLEdBQUcsNkJBQUMsT0FBRDtBQUNaLFFBQUEsZ0JBQWdCLEVBQUMscUJBREw7QUFFWixRQUFBLEtBQUssRUFBRSx5QkFBRywwQ0FBSCxFQUErQztBQUFDRSxVQUFBQSxJQUFJLEVBQUVFO0FBQVAsU0FBL0M7QUFGSyxRQUFoQjtBQUlIOztBQUVELFVBQU1DLGdCQUFnQixHQUFHckcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFdBQ0ksNkJBQUMsZ0JBQUQ7QUFDSSxNQUFBLEdBQUcsRUFBQyxjQURSO0FBRUksTUFBQSxTQUFTLEVBQUMscUJBRmQ7QUFHSSxNQUFBLE9BQU8sRUFBRSxLQUFLMkYsa0JBSGxCO0FBSUksTUFBQSxZQUFZLEVBQUUsS0FBS0YseUJBSnZCO0FBS0ksTUFBQSxZQUFZLEVBQUUsS0FBS0M7QUFMdkIsT0FPTUssYUFQTixFQU9xQixzREFBVyx5QkFBRyxRQUFILENBQVgsT0FQckIsQ0FESjtBQVdILEdBalkyQjtBQW1ZNUIzRixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFFBQUksS0FBSy9ELEtBQUwsQ0FBV0MsU0FBZixFQUEwQjtBQUN0QixZQUFNK0osbUJBQW1CLEdBQUd0RyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQTVCO0FBQ0EsYUFBTyw2QkFBQyxtQkFBRDtBQUFxQixRQUFBLFNBQVMsRUFBRSxLQUFLM0QsS0FBTCxDQUFXQyxTQUEzQztBQUFzRCxRQUFBLFNBQVMsRUFBQztBQUFoRSxRQUFQO0FBQ0g7O0FBQ0QsVUFBTXJDLE9BQU8sR0FBRyxLQUFLb0MsS0FBTCxDQUFXcEMsT0FBM0I7QUFDQSxVQUFNcU0sT0FBTyxHQUFHck0sT0FBTyxDQUFDNEMsVUFBUixFQUFoQjs7QUFFQSxVQUFNMEosVUFBVSxHQUFHQyxxQkFBWUMsZ0JBQVosQ0FBNkJ4TSxPQUE3QixDQUFuQjs7QUFDQSxRQUFJc0IsSUFBSSxHQUFHbUIsU0FBUyxDQUFDZ0ssVUFBVixDQUFxQkosT0FBckIsRUFBOEIsS0FBS2pLLEtBQUwsQ0FBV2hDLFVBQXpDLEVBQXFEO0FBQzVEc00sTUFBQUEsZUFBZSxFQUFFTCxPQUFPLENBQUNNLE9BQVIsS0FBb0IsU0FBcEIsSUFBaUMsQ0FBQ3RKLHVCQUFjQyxRQUFkLENBQXVCLDRCQUF2QixDQURTO0FBRTVEO0FBQ0FzSixNQUFBQSxrQkFBa0IsRUFBRU4sVUFId0M7QUFJNURPLE1BQUFBLEdBQUcsRUFBRSxLQUFLN0s7QUFKa0QsS0FBckQsQ0FBWDs7QUFNQSxRQUFJLEtBQUtJLEtBQUwsQ0FBV2dDLGdCQUFmLEVBQWlDO0FBQzdCOUMsTUFBQUEsSUFBSSxHQUFHLENBQUNBLElBQUQsRUFBTyxLQUFLdUssbUJBQUwsRUFBUCxDQUFQO0FBQ0g7O0FBRUQsUUFBSSxLQUFLekosS0FBTCxDQUFXOUIsYUFBZixFQUE4QjtBQUMxQmdCLE1BQUFBLElBQUksR0FBRztBQUFHLFFBQUEsSUFBSSxFQUFFLEtBQUtjLEtBQUwsQ0FBVzlCO0FBQXBCLFNBQXFDZ0IsSUFBckMsQ0FBUDtBQUNILEtBRkQsTUFFTyxJQUFJK0ssT0FBTyxDQUFDUyxJQUFSLElBQWdCLE9BQU9ULE9BQU8sQ0FBQ1MsSUFBUixDQUFhLDZCQUFiLENBQVAsS0FBdUQsUUFBM0UsRUFBcUY7QUFDeEZ4TCxNQUFBQSxJQUFJLEdBQUc7QUFBRyxRQUFBLElBQUksRUFBQyxHQUFSO0FBQVksUUFBQSxPQUFPLEVBQUUsS0FBSzhILGtCQUFMLENBQXdCMkQsSUFBeEIsQ0FBNkIsSUFBN0IsRUFBbUNWLE9BQU8sQ0FBQ1MsSUFBUixDQUFhLDZCQUFiLENBQW5DO0FBQXJCLFNBQXdHeEwsSUFBeEcsQ0FBUDtBQUNIOztBQUVELFFBQUkwTCxPQUFKOztBQUNBLFFBQUksS0FBS3RJLEtBQUwsQ0FBVzVELEtBQVgsQ0FBaUJvQyxNQUFqQixJQUEyQixDQUFDLEtBQUt3QixLQUFMLENBQVczRCxZQUF2QyxJQUF1RCxLQUFLcUIsS0FBTCxDQUFXNUIsY0FBdEUsRUFBc0Y7QUFDbEYsWUFBTXlNLGlCQUFpQixHQUFHbkgsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlCQUFqQixDQUExQjtBQUNBaUgsTUFBQUEsT0FBTyxHQUFHLEtBQUt0SSxLQUFMLENBQVc1RCxLQUFYLENBQWlCb00sR0FBakIsQ0FBc0JuSSxJQUFELElBQVE7QUFDbkMsZUFBTyw2QkFBQyxpQkFBRDtBQUNLLFVBQUEsR0FBRyxFQUFFQSxJQURWO0FBRUssVUFBQSxJQUFJLEVBQUVBLElBRlg7QUFHSyxVQUFBLE9BQU8sRUFBRSxLQUFLM0MsS0FBTCxDQUFXcEMsT0FIekI7QUFJSyxVQUFBLGFBQWEsRUFBRSxLQUFLc0ksYUFKekI7QUFLSyxVQUFBLGVBQWUsRUFBRSxLQUFLbEcsS0FBTCxDQUFXMUI7QUFMakMsVUFBUDtBQU1ILE9BUFMsQ0FBVjtBQVFIOztBQUVELFlBQVEyTCxPQUFPLENBQUNNLE9BQWhCO0FBQ0ksV0FBSyxTQUFMO0FBQ0ksZUFDSTtBQUFNLFVBQUEsU0FBUyxFQUFDO0FBQWhCLG9CQUVJO0FBQ0ksVUFBQSxTQUFTLEVBQUMsc0JBRGQ7QUFFSSxVQUFBLE9BQU8sRUFBRSxLQUFLakU7QUFGbEIsV0FJTTFJLE9BQU8sQ0FBQ21OLE1BQVIsR0FBaUJuTixPQUFPLENBQUNtTixNQUFSLENBQWVDLElBQWhDLEdBQXVDcE4sT0FBTyxDQUFDK0ksU0FBUixFQUo3QyxDQUZKLFVBU016SCxJQVROLEVBVU0wTCxPQVZOLENBREo7O0FBY0osV0FBSyxVQUFMO0FBQ0ksZUFDSTtBQUFNLFVBQUEsU0FBUyxFQUFDO0FBQWhCLFdBQ00xTCxJQUROLEVBRU0wTCxPQUZOLENBREo7O0FBTUo7QUFBUztBQUNMLGVBQ0k7QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixXQUNNMUwsSUFETixFQUVNMEwsT0FGTixDQURKO0FBeEJSO0FBK0JIO0FBeGMyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBoaWdobGlnaHQgZnJvbSAnaGlnaGxpZ2h0LmpzJztcclxuaW1wb3J0ICogYXMgSHRtbFV0aWxzIGZyb20gJy4uLy4uLy4uL0h0bWxVdGlscyc7XHJcbmltcG9ydCB7Zm9ybWF0RGF0ZX0gZnJvbSAnLi4vLi4vLi4vRGF0ZVV0aWxzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgKiBhcyBDb250ZXh0TWVudSBmcm9tICcuLi8uLi9zdHJ1Y3R1cmVzL0NvbnRleHRNZW51JztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IFJlcGx5VGhyZWFkIGZyb20gXCIuLi9lbGVtZW50cy9SZXBseVRocmVhZFwiO1xyXG5pbXBvcnQge3BpbGxpZnlMaW5rcywgdW5tb3VudFBpbGxzfSBmcm9tICcuLi8uLi8uLi91dGlscy9waWxsaWZ5JztcclxuaW1wb3J0IHtJbnRlZ3JhdGlvbk1hbmFnZXJzfSBmcm9tIFwiLi4vLi4vLi4vaW50ZWdyYXRpb25zL0ludGVncmF0aW9uTWFuYWdlcnNcIjtcclxuaW1wb3J0IHtpc1Blcm1hbGlua0hvc3R9IGZyb20gXCIuLi8uLi8uLi91dGlscy9wZXJtYWxpbmtzL1Blcm1hbGlua3NcIjtcclxuaW1wb3J0IHt0b1JpZ2h0T2Z9IGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL0NvbnRleHRNZW51XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnVGV4dHVhbEJvZHknLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8qIHRoZSBNYXRyaXhFdmVudCB0byBzaG93ICovXHJcbiAgICAgICAgbXhFdmVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICAvKiBhIGxpc3Qgb2Ygd29yZHMgdG8gaGlnaGxpZ2h0ICovXHJcbiAgICAgICAgaGlnaGxpZ2h0czogUHJvcFR5cGVzLmFycmF5LFxyXG5cclxuICAgICAgICAvKiBsaW5rIFVSTCBmb3IgdGhlIGhpZ2hsaWdodHMgKi9cclxuICAgICAgICBoaWdobGlnaHRMaW5rOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvKiBzaG91bGQgc2hvdyBVUkwgcHJldmlld3MgZm9yIHRoaXMgZXZlbnQgKi9cclxuICAgICAgICBzaG93VXJsUHJldmlldzogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIGNhbGxiYWNrIGZvciB3aGVuIG91ciB3aWRnZXQgaGFzIGxvYWRlZCAqL1xyXG4gICAgICAgIG9uSGVpZ2h0Q2hhbmdlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8qIHRoZSBzaGFwZSBvZiB0aGUgdGlsZSwgdXNlZCAqL1xyXG4gICAgICAgIHRpbGVTaGFwZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAvLyB0aGUgVVJMcyAoaWYgYW55KSB0byBiZSBwcmV2aWV3ZWQgd2l0aCBhIExpbmtQcmV2aWV3V2lkZ2V0XHJcbiAgICAgICAgICAgIC8vIGluc2lkZSB0aGlzIFRleHR1YWxCb2R5LlxyXG4gICAgICAgICAgICBsaW5rczogW10sXHJcblxyXG4gICAgICAgICAgICAvLyB0cmFjayB3aGV0aGVyIHRoZSBwcmV2aWV3IHdpZGdldCBpcyBoaWRkZW5cclxuICAgICAgICAgICAgd2lkZ2V0SGlkZGVuOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb3B5VG9DbGlwYm9hcmQ6IGZ1bmN0aW9uKHRleHQpIHtcclxuICAgICAgICBjb25zdCB0ZXh0QXJlYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJ0ZXh0YXJlYVwiKTtcclxuICAgICAgICB0ZXh0QXJlYS52YWx1ZSA9IHRleHQ7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0ZXh0QXJlYSk7XHJcbiAgICAgICAgdGV4dEFyZWEuc2VsZWN0KCk7XHJcblxyXG4gICAgICAgIGxldCBzdWNjZXNzZnVsID0gZmFsc2U7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgc3VjY2Vzc2Z1bCA9IGRvY3VtZW50LmV4ZWNDb21tYW5kKCdjb3B5Jyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdVbmFibGUgdG8gY29weScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZCh0ZXh0QXJlYSk7XHJcbiAgICAgICAgcmV0dXJuIHN1Y2Nlc3NmdWw7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIGNvbXBvbmVudCB3aXRoIHJlYWwgY2xhc3MsIHVzZSBjb25zdHJ1Y3RvciBmb3IgcmVmc1xyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fY29udGVudCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fcGlsbHMgPSBbXTtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuZWRpdFN0YXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5Rm9ybWF0dGluZygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2FwcGx5Rm9ybWF0dGluZygpIHtcclxuICAgICAgICB0aGlzLmFjdGl2YXRlU3BvaWxlcnMoW3RoaXMuX2NvbnRlbnQuY3VycmVudF0pO1xyXG5cclxuICAgICAgICAvLyBwaWxsaWZ5TGlua3MgQkVGT1JFIGxpbmtpZnlFbGVtZW50IGJlY2F1c2UgcGxhaW4gcm9vbS91c2VyIFVSTHMgaW4gdGhlIGNvbXBvc2VyXHJcbiAgICAgICAgLy8gYXJlIHN0aWxsIHNlbnQgYXMgcGxhaW50ZXh0IFVSTHMuIElmIHRoZXNlIGFyZSBldmVyIHBpbGxpZmllZCBpbiB0aGUgY29tcG9zZXIsXHJcbiAgICAgICAgLy8gd2Ugc2hvdWxkIGJlIHBpbGxpZnkgdGhlbSBoZXJlIGJ5IGRvaW5nIHRoZSBsaW5raWZ5aW5nIEJFRk9SRSB0aGUgcGlsbGlmeWluZy5cclxuICAgICAgICBwaWxsaWZ5TGlua3MoW3RoaXMuX2NvbnRlbnQuY3VycmVudF0sIHRoaXMucHJvcHMubXhFdmVudCwgdGhpcy5fcGlsbHMpO1xyXG4gICAgICAgIEh0bWxVdGlscy5saW5raWZ5RWxlbWVudCh0aGlzLl9jb250ZW50LmN1cnJlbnQpO1xyXG4gICAgICAgIHRoaXMuY2FsY3VsYXRlVXJsUHJldmlldygpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKS5mb3JtYXQgPT09IFwib3JnLm1hdHJpeC5jdXN0b20uaHRtbFwiKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJsb2NrcyA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMpLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiY29kZVwiKTtcclxuICAgICAgICAgICAgaWYgKGJsb2Nrcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBEbyB0aGlzIGFzeW5jaHJvbm91c2x5OiBwYXJzaW5nIGNvZGUgdGFrZXMgdGltZSBhbmQgd2UgZG9uJ3RcclxuICAgICAgICAgICAgICAgIC8vIG5lZWQgdG8gYmxvY2sgdGhlIERPTSB1cGRhdGUgb24gaXQuXHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBibG9ja3MubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJlbmFibGVTeW50YXhIaWdobGlnaHRMYW5ndWFnZURldGVjdGlvblwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0LmhpZ2hsaWdodEJsb2NrKGJsb2Nrc1tpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBPbmx5IHN5bnRheCBoaWdobGlnaHQgaWYgdGhlcmUncyBhIGNsYXNzIHN0YXJ0aW5nIHdpdGggbGFuZ3VhZ2UtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjbGFzc2VzID0gYmxvY2tzW2ldLmNsYXNzTmFtZS5zcGxpdCgvXFxzKy8pLmZpbHRlcihmdW5jdGlvbihjbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBjbC5zdGFydHNXaXRoKCdsYW5ndWFnZS0nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjbGFzc2VzLmxlbmd0aCAhPSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0LmhpZ2hsaWdodEJsb2NrKGJsb2Nrc1tpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCAxMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fYWRkQ29kZUNvcHlCdXR0b24oKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24ocHJldlByb3BzKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLmVkaXRTdGF0ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBzdG9wcGVkRWRpdGluZyA9IHByZXZQcm9wcy5lZGl0U3RhdGUgJiYgIXRoaXMucHJvcHMuZWRpdFN0YXRlO1xyXG4gICAgICAgICAgICBjb25zdCBtZXNzYWdlV2FzRWRpdGVkID0gcHJldlByb3BzLnJlcGxhY2luZ0V2ZW50SWQgIT09IHRoaXMucHJvcHMucmVwbGFjaW5nRXZlbnRJZDtcclxuICAgICAgICAgICAgaWYgKG1lc3NhZ2VXYXNFZGl0ZWQgfHwgc3RvcHBlZEVkaXRpbmcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2FwcGx5Rm9ybWF0dGluZygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gdHJ1ZTtcclxuICAgICAgICB1bm1vdW50UGlsbHModGhpcy5fcGlsbHMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IGZ1bmN0aW9uKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oXCJzaG91bGRDb21wb25lbnRVcGRhdGU6IFNob3dVcmxQcmV2aWV3IGZvciAlcyBpcyAlc1wiLCB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSwgdGhpcy5wcm9wcy5zaG93VXJsUHJldmlldyk7XHJcblxyXG4gICAgICAgIC8vIGV4cGxvaXQgdGhhdCBldmVudHMgYXJlIGltbXV0YWJsZSA6KVxyXG4gICAgICAgIHJldHVybiAobmV4dFByb3BzLm14RXZlbnQuZ2V0SWQoKSAhPT0gdGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCkgfHxcclxuICAgICAgICAgICAgICAgIG5leHRQcm9wcy5oaWdobGlnaHRzICE9PSB0aGlzLnByb3BzLmhpZ2hsaWdodHMgfHxcclxuICAgICAgICAgICAgICAgIG5leHRQcm9wcy5yZXBsYWNpbmdFdmVudElkICE9PSB0aGlzLnByb3BzLnJlcGxhY2luZ0V2ZW50SWQgfHxcclxuICAgICAgICAgICAgICAgIG5leHRQcm9wcy5oaWdobGlnaHRMaW5rICE9PSB0aGlzLnByb3BzLmhpZ2hsaWdodExpbmsgfHxcclxuICAgICAgICAgICAgICAgIG5leHRQcm9wcy5zaG93VXJsUHJldmlldyAhPT0gdGhpcy5wcm9wcy5zaG93VXJsUHJldmlldyB8fFxyXG4gICAgICAgICAgICAgICAgbmV4dFByb3BzLmVkaXRTdGF0ZSAhPT0gdGhpcy5wcm9wcy5lZGl0U3RhdGUgfHxcclxuICAgICAgICAgICAgICAgIG5leHRTdGF0ZS5saW5rcyAhPT0gdGhpcy5zdGF0ZS5saW5rcyB8fFxyXG4gICAgICAgICAgICAgICAgbmV4dFN0YXRlLmVkaXRlZE1hcmtlckhvdmVyZWQgIT09IHRoaXMuc3RhdGUuZWRpdGVkTWFya2VySG92ZXJlZCB8fFxyXG4gICAgICAgICAgICAgICAgbmV4dFN0YXRlLndpZGdldEhpZGRlbiAhPT0gdGhpcy5zdGF0ZS53aWRnZXRIaWRkZW4pO1xyXG4gICAgfSxcclxuXHJcbiAgICBjYWxjdWxhdGVVcmxQcmV2aWV3OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyhcImNhbGN1bGF0ZVVybFByZXZpZXc6IFNob3dVcmxQcmV2aWV3IGZvciAlcyBpcyAlc1wiLCB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSwgdGhpcy5wcm9wcy5zaG93VXJsUHJldmlldyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNob3dVcmxQcmV2aWV3KSB7XHJcbiAgICAgICAgICAgIC8vIHBhc3Mgb25seSB0aGUgZmlyc3QgY2hpbGQgd2hpY2ggaXMgdGhlIGV2ZW50IHRpbGUgb3RoZXJ3aXNlIHRoaXMgcmVjdXJzZXMgb24gZWRpdGVkIGV2ZW50c1xyXG4gICAgICAgICAgICBsZXQgbGlua3MgPSB0aGlzLmZpbmRMaW5rcyhbdGhpcy5fY29udGVudC5jdXJyZW50XSk7XHJcbiAgICAgICAgICAgIGlmIChsaW5rcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIC8vIGRlLWR1cCB0aGUgbGlua3MgKGJ1dCBwcmVzZXJ2ZSBvcmRlcmluZylcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlZW4gPSBuZXcgU2V0KCk7XHJcbiAgICAgICAgICAgICAgICBsaW5rcyA9IGxpbmtzLmZpbHRlcigobGluaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWVuLmhhcyhsaW5rKSkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlZW4uYWRkKGxpbmspO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGxpbmtzOiBsaW5rcyB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBsYXp5LWxvYWQgdGhlIGhpZGRlbiBzdGF0ZSBvZiB0aGUgcHJldmlldyB3aWRnZXQgZnJvbSBsb2NhbHN0b3JhZ2VcclxuICAgICAgICAgICAgICAgIGlmIChnbG9iYWwubG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaGlkZGVuID0gZ2xvYmFsLmxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiaGlkZV9wcmV2aWV3X1wiICsgdGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB3aWRnZXRIaWRkZW46IGhpZGRlbiB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgYWN0aXZhdGVTcG9pbGVyczogZnVuY3Rpb24obm9kZXMpIHtcclxuICAgICAgICBsZXQgbm9kZSA9IG5vZGVzWzBdO1xyXG4gICAgICAgIHdoaWxlIChub2RlKSB7XHJcbiAgICAgICAgICAgIGlmIChub2RlLnRhZ05hbWUgPT09IFwiU1BBTlwiICYmIHR5cGVvZiBub2RlLmdldEF0dHJpYnV0ZShcImRhdGEtbXgtc3BvaWxlclwiKSA9PT0gXCJzdHJpbmdcIikge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3BvaWxlckNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWFzb24gPSBub2RlLmdldEF0dHJpYnV0ZShcImRhdGEtbXgtc3BvaWxlclwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IFNwb2lsZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5TcG9pbGVyJyk7XHJcbiAgICAgICAgICAgICAgICBub2RlLnJlbW92ZUF0dHJpYnV0ZShcImRhdGEtbXgtc3BvaWxlclwiKTsgLy8gd2UgZG9uJ3Qgd2FudCB0byByZWN1cnNlXHJcbiAgICAgICAgICAgICAgICBjb25zdCBzcG9pbGVyID0gPFNwb2lsZXJcclxuICAgICAgICAgICAgICAgICAgICByZWFzb249e3JlYXNvbn1cclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50SHRtbD17bm9kZS5vdXRlckhUTUx9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuXHJcbiAgICAgICAgICAgICAgICBSZWFjdERPTS5yZW5kZXIoc3BvaWxlciwgc3BvaWxlckNvbnRhaW5lcik7XHJcbiAgICAgICAgICAgICAgICBub2RlLnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKHNwb2lsZXJDb250YWluZXIsIG5vZGUpO1xyXG5cclxuICAgICAgICAgICAgICAgIG5vZGUgPSBzcG9pbGVyQ29udGFpbmVyO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobm9kZS5jaGlsZE5vZGVzICYmIG5vZGUuY2hpbGROb2Rlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZhdGVTcG9pbGVycyhub2RlLmNoaWxkTm9kZXMpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBub2RlID0gbm9kZS5uZXh0U2libGluZztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGZpbmRMaW5rczogZnVuY3Rpb24obm9kZXMpIHtcclxuICAgICAgICBsZXQgbGlua3MgPSBbXTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBub2Rlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBub2RlID0gbm9kZXNbaV07XHJcbiAgICAgICAgICAgIGlmIChub2RlLnRhZ05hbWUgPT09IFwiQVwiICYmIG5vZGUuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNMaW5rUHJldmlld2FibGUobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBsaW5rcy5wdXNoKG5vZGUuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobm9kZS50YWdOYW1lID09PSBcIlBSRVwiIHx8IG5vZGUudGFnTmFtZSA9PT0gXCJDT0RFXCIgfHxcclxuICAgICAgICAgICAgICAgICAgICBub2RlLnRhZ05hbWUgPT09IFwiQkxPQ0tRVU9URVwiKSB7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChub2RlLmNoaWxkcmVuICYmIG5vZGUuY2hpbGRyZW4ubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBsaW5rcyA9IGxpbmtzLmNvbmNhdCh0aGlzLmZpbmRMaW5rcyhub2RlLmNoaWxkcmVuKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxpbmtzO1xyXG4gICAgfSxcclxuXHJcbiAgICBpc0xpbmtQcmV2aWV3YWJsZTogZnVuY3Rpb24obm9kZSkge1xyXG4gICAgICAgIC8vIGRvbid0IHRyeSB0byBwcmV2aWV3IHJlbGF0aXZlIGxpbmtzXHJcbiAgICAgICAgaWYgKCFub2RlLmdldEF0dHJpYnV0ZShcImhyZWZcIikuc3RhcnRzV2l0aChcImh0dHA6Ly9cIikgJiZcclxuICAgICAgICAgICAgIW5vZGUuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKS5zdGFydHNXaXRoKFwiaHR0cHM6Ly9cIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gYXMgYSByYW5kb20gaGV1cmlzdGljIHRvIGF2b2lkIGhpZ2hsaWdodGluZyB0aGluZ3MgbGlrZSBcImZvby5wbFwiXHJcbiAgICAgICAgLy8gd2UgcmVxdWlyZSB0aGUgbGlua2VkIHRleHQgdG8gZWl0aGVyIGluY2x1ZGUgYSAvIChlaXRoZXIgZnJvbSBodHRwOi8vXHJcbiAgICAgICAgLy8gb3IgZnJvbSBhIGZ1bGwgZm9vLmJhci9iYXogc3R5bGUgc2NoZW1lbGVzcyBVUkwpIC0gb3IgYmUgYSBtYXJrZG93bi1zdHlsZVxyXG4gICAgICAgIC8vIGxpbmssIGluIHdoaWNoIGNhc2Ugd2UgY2hlY2sgdGhlIHRhcmdldCB0ZXh0IGRpZmZlcnMgZnJvbSB0aGUgbGluayB2YWx1ZS5cclxuICAgICAgICAvLyBUT0RPOiBtYWtlIHRoaXMgY29uZmlndXJhYmxlP1xyXG4gICAgICAgIGlmIChub2RlLnRleHRDb250ZW50LmluZGV4T2YoXCIvXCIpID4gLTEpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gbm9kZS5nZXRBdHRyaWJ1dGUoXCJocmVmXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBob3N0ID0gdXJsLm1hdGNoKC9eaHR0cHM/OlxcL1xcLyguKj8pKFxcL3wkKS8pWzFdO1xyXG5cclxuICAgICAgICAgICAgLy8gbmV2ZXIgcHJldmlldyBwZXJtYWxpbmtzIChpZiBhbnl0aGluZyB3ZSBzaG91bGQgZ2l2ZSBhIHNtYXJ0XHJcbiAgICAgICAgICAgIC8vIHByZXZpZXcgb2YgdGhlIHJvb20vdXNlciB0aGV5IHBvaW50IHRvOiBub2JvZHkgbmVlZHMgdG8gYmUgcmVtaW5kZWRcclxuICAgICAgICAgICAgLy8gd2hhdCB0aGUgbWF0cml4LnRvIHNpdGUgbG9va3MgbGlrZSkuXHJcbiAgICAgICAgICAgIGlmIChpc1Blcm1hbGlua0hvc3QoaG9zdCkpIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIGlmIChub2RlLnRleHRDb250ZW50LnRvTG93ZXJDYXNlKCkudHJpbSgpLnN0YXJ0c1dpdGgoaG9zdC50b0xvd2VyQ2FzZSgpKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gaXQncyBhIFwiZm9vLnBsXCIgc3R5bGUgbGlua1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gaXQncyBhIFtmb28gYmFyXShodHRwOi8vZm9vLmNvbSkgc3R5bGUgbGlua1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9hZGRDb2RlQ29weUJ1dHRvbigpIHtcclxuICAgICAgICAvLyBBZGQgJ2NvcHknIGJ1dHRvbnMgdG8gcHJlIGJsb2Nrc1xyXG4gICAgICAgIEFycmF5LmZyb20oUmVhY3RET00uZmluZERPTU5vZGUodGhpcykucXVlcnlTZWxlY3RvckFsbCgnLm14X0V2ZW50VGlsZV9ib2R5IHByZScpKS5mb3JFYWNoKChwKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpO1xyXG4gICAgICAgICAgICBidXR0b24uY2xhc3NOYW1lID0gXCJteF9FdmVudFRpbGVfY29weUJ1dHRvblwiO1xyXG4gICAgICAgICAgICBidXR0b24ub25jbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjb3B5Q29kZSA9IGJ1dHRvbi5wYXJlbnROb2RlLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwicHJlXCIpWzBdO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3VjY2Vzc2Z1bCA9IHRoaXMuY29weVRvQ2xpcGJvYXJkKGNvcHlDb2RlLnRleHRDb250ZW50KTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBidXR0b25SZWN0ID0gZS50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBHZW5lcmljVGV4dENvbnRleHRNZW51ID0gc2RrLmdldENvbXBvbmVudCgnY29udGV4dF9tZW51cy5HZW5lcmljVGV4dENvbnRleHRNZW51Jyk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB7Y2xvc2V9ID0gQ29udGV4dE1lbnUuY3JlYXRlTWVudShHZW5lcmljVGV4dENvbnRleHRNZW51LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4udG9SaWdodE9mKGJ1dHRvblJlY3QsIDIpLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHN1Y2Nlc3NmdWwgPyBfdCgnQ29waWVkIScpIDogX3QoJ0ZhaWxlZCB0byBjb3B5JyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGUudGFyZ2V0Lm9ubW91c2VsZWF2ZSA9IGNsb3NlO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgLy8gV3JhcCBhIGRpdiBhcm91bmQgPHByZT4gc28gdGhhdCB0aGUgY29weSBidXR0b24gY2FuIGJlIGNvcnJlY3RseSBwb3NpdGlvbmVkXHJcbiAgICAgICAgICAgIC8vIHdoZW4gdGhlIDxwcmU+IG92ZXJmbG93cyBhbmQgaXMgc2Nyb2xsZWQgaG9yaXpvbnRhbGx5LlxyXG4gICAgICAgICAgICBjb25zdCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xyXG4gICAgICAgICAgICBkaXYuY2xhc3NOYW1lID0gXCJteF9FdmVudFRpbGVfcHJlX2NvbnRhaW5lclwiO1xyXG5cclxuICAgICAgICAgICAgLy8gSW5zZXJ0IGNvbnRhaW5pbmcgZGl2IGluIHBsYWNlIG9mIDxwcmU+IGJsb2NrXHJcbiAgICAgICAgICAgIHAucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQoZGl2LCBwKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFwcGVuZCA8cHJlPiBibG9jayBhbmQgY29weSBidXR0b24gdG8gY29udGFpbmVyXHJcbiAgICAgICAgICAgIGRpdi5hcHBlbmRDaGlsZChwKTtcclxuICAgICAgICAgICAgZGl2LmFwcGVuZENoaWxkKGJ1dHRvbik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQ2FuY2VsQ2xpY2s6IGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHdpZGdldEhpZGRlbjogdHJ1ZSB9KTtcclxuICAgICAgICAvLyBGSVhNRTogcGVyc2lzdCB0aGlzIHNvbWV3aGVyZSBzbWFydGVyIHRoYW4gbG9jYWwgc3RvcmFnZVxyXG4gICAgICAgIGlmIChnbG9iYWwubG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgIGdsb2JhbC5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImhpZGVfcHJldmlld19cIiArIHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpLCBcIjFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FbW90ZVNlbmRlckNsaWNrOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGNvbnN0IG14RXZlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQ7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnaW5zZXJ0X21lbnRpb24nLFxyXG4gICAgICAgICAgICB1c2VyX2lkOiBteEV2ZW50LmdldFNlbmRlcigpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRFdmVudFRpbGVPcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzV2lkZ2V0SGlkZGVuOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS53aWRnZXRIaWRkZW47XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICB1bmhpZGVXaWRnZXQ6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB3aWRnZXRIaWRkZW46IGZhbHNlIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKGdsb2JhbC5sb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBnbG9iYWwubG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJoaWRlX3ByZXZpZXdfXCIgKyB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TdGFydGVyTGlua0NsaWNrOiBmdW5jdGlvbihzdGFydGVyTGluaywgZXYpIHtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIC8vIFdlIG5lZWQgdG8gYWRkIG9uIG91ciBzY2FsYXIgdG9rZW4gdG8gdGhlIHN0YXJ0ZXIgbGluaywgYnV0IHdlIG1heSBub3QgaGF2ZSBvbmUhXHJcbiAgICAgICAgLy8gSW4gYWRkaXRpb24sIHdlIGNhbid0IGZldGNoIG9uZSBvbiBjbGljayBhbmQgdGhlbiBnbyB0byBpdCBpbW1lZGlhdGVseSBhcyB0aGF0XHJcbiAgICAgICAgLy8gaXMgdGhlbiB0cmVhdGVkIGFzIGEgcG9wdXAhXHJcbiAgICAgICAgLy8gV2UgY2FuIGdldCBhcm91bmQgdGhpcyBieSBmZXRjaGluZyBvbmUgbm93IGFuZCBzaG93aW5nIGEgXCJjb25maXJtYXRpb24gZGlhbG9nXCIgKGh1cnIgaHVycilcclxuICAgICAgICAvLyB3aGljaCByZXF1aXJlcyB0aGUgdXNlciB0byBjbGljayB0aHJvdWdoIGFuZCBUSEVOIHdlIGNhbiBvcGVuIHRoZSBsaW5rIGluIGEgbmV3IHRhYiBiZWNhdXNlXHJcbiAgICAgICAgLy8gdGhlIHdpbmRvdy5vcGVuIGNvbW1hbmQgb2NjdXJzIGluIHRoZSBzYW1lIHN0YWNrIGZyYW1lIGFzIHRoZSBvbkNsaWNrIGNhbGxiYWNrLlxyXG5cclxuICAgICAgICBjb25zdCBtYW5hZ2VycyA9IEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKTtcclxuICAgICAgICBpZiAoIW1hbmFnZXJzLmhhc01hbmFnZXIoKSkge1xyXG4gICAgICAgICAgICBtYW5hZ2Vycy5vcGVuTm9NYW5hZ2VyRGlhbG9nKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEdvIGZldGNoIGEgc2NhbGFyIHRva2VuXHJcbiAgICAgICAgY29uc3QgaW50ZWdyYXRpb25NYW5hZ2VyID0gbWFuYWdlcnMuZ2V0UHJpbWFyeU1hbmFnZXIoKTtcclxuICAgICAgICBjb25zdCBzY2FsYXJDbGllbnQgPSBpbnRlZ3JhdGlvbk1hbmFnZXIuZ2V0U2NhbGFyQ2xpZW50KCk7XHJcbiAgICAgICAgc2NhbGFyQ2xpZW50LmNvbm5lY3QoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY29tcGxldGVVcmwgPSBzY2FsYXJDbGllbnQuZ2V0U3RhcnRlckxpbmsoc3RhcnRlckxpbmspO1xyXG4gICAgICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBpbnRlZ3JhdGlvbnNVcmwgPSBpbnRlZ3JhdGlvbk1hbmFnZXIudWlVcmw7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0FkZCBhbiBpbnRlZ3JhdGlvbicsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiQWRkIGFuIEludGVncmF0aW9uXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIllvdSBhcmUgYWJvdXQgdG8gYmUgdGFrZW4gdG8gYSB0aGlyZC1wYXJ0eSBzaXRlIHNvIHlvdSBjYW4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhdXRoZW50aWNhdGUgeW91ciBhY2NvdW50IGZvciB1c2Ugd2l0aCAlKGludGVncmF0aW9uc1VybClzLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkRvIHlvdSB3aXNoIHRvIGNvbnRpbnVlP1wiLCB7IGludGVncmF0aW9uc1VybDogaW50ZWdyYXRpb25zVXJsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICBidXR0b246IF90KFwiQ29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiBmdW5jdGlvbihjb25maXJtZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWNvbmZpcm1lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gd2luZG93LnNjcmVlbi53aWR0aCA+IDEwMjQgPyAxMDI0IDogd2luZG93LnNjcmVlbi53aWR0aDtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBoZWlnaHQgPSB3aW5kb3cuc2NyZWVuLmhlaWdodCA+IDgwMCA/IDgwMCA6IHdpbmRvdy5zY3JlZW4uaGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxlZnQgPSAod2luZG93LnNjcmVlbi53aWR0aCAtIHdpZHRoKSAvIDI7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9wID0gKHdpbmRvdy5zY3JlZW4uaGVpZ2h0IC0gaGVpZ2h0KSAvIDI7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmVhdHVyZXMgPSBgaGVpZ2h0PSR7aGVpZ2h0fSwgd2lkdGg9JHt3aWR0aH0sIHRvcD0ke3RvcH0sIGxlZnQ9JHtsZWZ0fSxgO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHduZCA9IHdpbmRvdy5vcGVuKGNvbXBsZXRlVXJsLCAnX2JsYW5rJywgZmVhdHVyZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHduZC5vcGVuZXIgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbk1vdXNlRW50ZXJFZGl0ZWRNYXJrZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2VkaXRlZE1hcmtlckhvdmVyZWQ6IHRydWV9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uTW91c2VMZWF2ZUVkaXRlZE1hcmtlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZWRpdGVkTWFya2VySG92ZXJlZDogZmFsc2V9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29wZW5IaXN0b3J5RGlhbG9nOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBNZXNzYWdlRWRpdEhpc3RvcnlEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZGlhbG9ncy5NZXNzYWdlRWRpdEhpc3RvcnlEaWFsb2dcIik7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlRGlhbG9nKE1lc3NhZ2VFZGl0SGlzdG9yeURpYWxvZywge214RXZlbnQ6IHRoaXMucHJvcHMubXhFdmVudH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVuZGVyRWRpdGVkTWFya2VyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgZWRpdGVkVG9vbHRpcDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lZGl0ZWRNYXJrZXJIb3ZlcmVkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5Ub29sdGlwJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGUgPSB0aGlzLnByb3BzLm14RXZlbnQucmVwbGFjaW5nRXZlbnREYXRlKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGVTdHJpbmcgPSBkYXRlICYmIGZvcm1hdERhdGUoZGF0ZSk7XHJcbiAgICAgICAgICAgIGVkaXRlZFRvb2x0aXAgPSA8VG9vbHRpcFxyXG4gICAgICAgICAgICAgICAgdG9vbHRpcENsYXNzTmFtZT1cIm14X1Rvb2x0aXBfdGltZWxpbmVcIlxyXG4gICAgICAgICAgICAgICAgbGFiZWw9e190KFwiRWRpdGVkIGF0ICUoZGF0ZSlzLiBDbGljayB0byB2aWV3IGVkaXRzLlwiLCB7ZGF0ZTogZGF0ZVN0cmluZ30pfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgIGtleT1cImVkaXRlZE1hcmtlclwiXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfZWRpdGVkXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29wZW5IaXN0b3J5RGlhbG9nfVxyXG4gICAgICAgICAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLl9vbk1vdXNlRW50ZXJFZGl0ZWRNYXJrZXJ9XHJcbiAgICAgICAgICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMuX29uTW91c2VMZWF2ZUVkaXRlZE1hcmtlcn1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgeyBlZGl0ZWRUb29sdGlwIH08c3Bhbj57YCgke190KFwiZWRpdGVkXCIpfSlgfTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZWRpdFN0YXRlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEVkaXRNZXNzYWdlQ29tcG9zZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5FZGl0TWVzc2FnZUNvbXBvc2VyJyk7XHJcbiAgICAgICAgICAgIHJldHVybiA8RWRpdE1lc3NhZ2VDb21wb3NlciBlZGl0U3RhdGU9e3RoaXMucHJvcHMuZWRpdFN0YXRlfSBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfY29udGVudFwiIC8+O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBteEV2ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50O1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSBteEV2ZW50LmdldENvbnRlbnQoKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc3RyaXBSZXBseSA9IFJlcGx5VGhyZWFkLmdldFBhcmVudEV2ZW50SWQobXhFdmVudCk7XHJcbiAgICAgICAgbGV0IGJvZHkgPSBIdG1sVXRpbHMuYm9keVRvSHRtbChjb250ZW50LCB0aGlzLnByb3BzLmhpZ2hsaWdodHMsIHtcclxuICAgICAgICAgICAgZGlzYWJsZUJpZ0Vtb2ppOiBjb250ZW50Lm1zZ3R5cGUgPT09IFwibS5lbW90ZVwiIHx8ICFTZXR0aW5nc1N0b3JlLmdldFZhbHVlKCdUZXh0dWFsQm9keS5lbmFibGVCaWdFbW9qaScpLFxyXG4gICAgICAgICAgICAvLyBQYXJ0IG9mIFJlcGxpZXMgZmFsbGJhY2sgc3VwcG9ydFxyXG4gICAgICAgICAgICBzdHJpcFJlcGx5RmFsbGJhY2s6IHN0cmlwUmVwbHksXHJcbiAgICAgICAgICAgIHJlZjogdGhpcy5fY29udGVudCxcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5yZXBsYWNpbmdFdmVudElkKSB7XHJcbiAgICAgICAgICAgIGJvZHkgPSBbYm9keSwgdGhpcy5fcmVuZGVyRWRpdGVkTWFya2VyKCldO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuaGlnaGxpZ2h0TGluaykge1xyXG4gICAgICAgICAgICBib2R5ID0gPGEgaHJlZj17dGhpcy5wcm9wcy5oaWdobGlnaHRMaW5rfT57IGJvZHkgfTwvYT47XHJcbiAgICAgICAgfSBlbHNlIGlmIChjb250ZW50LmRhdGEgJiYgdHlwZW9mIGNvbnRlbnQuZGF0YVtcIm9yZy5tYXRyaXgubmViLnN0YXJ0ZXJfbGlua1wiXSA9PT0gXCJzdHJpbmdcIikge1xyXG4gICAgICAgICAgICBib2R5ID0gPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXt0aGlzLm9uU3RhcnRlckxpbmtDbGljay5iaW5kKHRoaXMsIGNvbnRlbnQuZGF0YVtcIm9yZy5tYXRyaXgubmViLnN0YXJ0ZXJfbGlua1wiXSl9PnsgYm9keSB9PC9hPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB3aWRnZXRzO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmxpbmtzLmxlbmd0aCAmJiAhdGhpcy5zdGF0ZS53aWRnZXRIaWRkZW4gJiYgdGhpcy5wcm9wcy5zaG93VXJsUHJldmlldykge1xyXG4gICAgICAgICAgICBjb25zdCBMaW5rUHJldmlld1dpZGdldCA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLkxpbmtQcmV2aWV3V2lkZ2V0Jyk7XHJcbiAgICAgICAgICAgIHdpZGdldHMgPSB0aGlzLnN0YXRlLmxpbmtzLm1hcCgobGluayk9PntcclxuICAgICAgICAgICAgICAgIHJldHVybiA8TGlua1ByZXZpZXdXaWRnZXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17bGlua31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbms9e2xpbmt9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBteEV2ZW50PXt0aGlzLnByb3BzLm14RXZlbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbENsaWNrPXt0aGlzLm9uQ2FuY2VsQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9e3RoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkfSAvPjtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzd2l0Y2ggKGNvbnRlbnQubXNndHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlIFwibS5lbW90ZVwiOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9NRW1vdGVCb2R5IG14X0V2ZW50VGlsZV9jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICombmJzcDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X01FbW90ZUJvZHlfc2VuZGVyXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25FbW90ZVNlbmRlckNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IG14RXZlbnQuc2VuZGVyID8gbXhFdmVudC5zZW5kZXIubmFtZSA6IG14RXZlbnQuZ2V0U2VuZGVyKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICZuYnNwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGJvZHkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHdpZGdldHMgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNhc2UgXCJtLm5vdGljZVwiOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9NTm90aWNlQm9keSBteF9FdmVudFRpbGVfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGJvZHkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHdpZGdldHMgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IC8vIGluY2x1ZGluZyBcIm0udGV4dFwiXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01UZXh0Qm9keSBteF9FdmVudFRpbGVfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGJvZHkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHdpZGdldHMgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==