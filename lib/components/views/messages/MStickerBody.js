"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _MImageBody = _interopRequireDefault(require("./MImageBody"));

var sdk = _interopRequireWildcard(require("../../../index"));

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MStickerBody extends _MImageBody.default {
  // Mostly empty to prevent default behaviour of MImageBody
  onClick(ev) {
    ev.preventDefault();

    if (!this.state.showImage) {
      this.showImage();
    }
  } // MStickerBody doesn't need a wrapping `<a href=...>`, but it does need extra padding
  // which is added by mx_MStickerBody_wrapper


  wrapImage(contentUrl, children) {
    let onClick = null;

    if (!this.state.showImage) {
      onClick = this.onClick;
    }

    return _react.default.createElement("div", {
      className: "mx_MStickerBody_wrapper",
      onClick: onClick
    }, " ", children, " ");
  } // Placeholder to show in place of the sticker image if
  // img onLoad hasn't fired yet.


  getPlaceholder() {
    const TintableSVG = sdk.getComponent('elements.TintableSvg');
    return _react.default.createElement(TintableSVG, {
      src: require("../../../../res/img/icons-show-stickers.svg"),
      width: "75",
      height: "75"
    });
  } // Tooltip to show on mouse over


  getTooltip() {
    const content = this.props.mxEvent && this.props.mxEvent.getContent();
    if (!content || !content.body || !content.info || !content.info.w) return null;
    const Tooltip = sdk.getComponent('elements.Tooltip');
    return _react.default.createElement("div", {
      style: {
        left: content.info.w + 'px'
      },
      className: "mx_MStickerBody_tooltip"
    }, _react.default.createElement(Tooltip, {
      label: content.body
    }));
  } // Don't show "Download this_file.png ..."


  getFileBody() {
    return null;
  }

}

exports.default = MStickerBody;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01TdGlja2VyQm9keS5qcyJdLCJuYW1lcyI6WyJNU3RpY2tlckJvZHkiLCJNSW1hZ2VCb2R5Iiwib25DbGljayIsImV2IiwicHJldmVudERlZmF1bHQiLCJzdGF0ZSIsInNob3dJbWFnZSIsIndyYXBJbWFnZSIsImNvbnRlbnRVcmwiLCJjaGlsZHJlbiIsImdldFBsYWNlaG9sZGVyIiwiVGludGFibGVTVkciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJyZXF1aXJlIiwiZ2V0VG9vbHRpcCIsImNvbnRlbnQiLCJwcm9wcyIsIm14RXZlbnQiLCJnZXRDb250ZW50IiwiYm9keSIsImluZm8iLCJ3IiwiVG9vbHRpcCIsImxlZnQiLCJnZXRGaWxlQm9keSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBbEJBOzs7Ozs7Ozs7Ozs7Ozs7QUFvQmUsTUFBTUEsWUFBTixTQUEyQkMsbUJBQTNCLENBQXNDO0FBQ2pEO0FBQ0FDLEVBQUFBLE9BQU8sQ0FBQ0MsRUFBRCxFQUFLO0FBQ1JBLElBQUFBLEVBQUUsQ0FBQ0MsY0FBSDs7QUFDQSxRQUFJLENBQUMsS0FBS0MsS0FBTCxDQUFXQyxTQUFoQixFQUEyQjtBQUN2QixXQUFLQSxTQUFMO0FBQ0g7QUFDSixHQVBnRCxDQVNqRDtBQUNBOzs7QUFDQUMsRUFBQUEsU0FBUyxDQUFDQyxVQUFELEVBQWFDLFFBQWIsRUFBdUI7QUFDNUIsUUFBSVAsT0FBTyxHQUFHLElBQWQ7O0FBQ0EsUUFBSSxDQUFDLEtBQUtHLEtBQUwsQ0FBV0MsU0FBaEIsRUFBMkI7QUFDdkJKLE1BQUFBLE9BQU8sR0FBRyxLQUFLQSxPQUFmO0FBQ0g7O0FBQ0QsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFDLHlCQUFmO0FBQXlDLE1BQUEsT0FBTyxFQUFFQTtBQUFsRCxZQUE4RE8sUUFBOUQsTUFBUDtBQUNILEdBakJnRCxDQW1CakQ7QUFDQTs7O0FBQ0FDLEVBQUFBLGNBQWMsR0FBRztBQUNiLFVBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFwQjtBQUNBLFdBQU8sNkJBQUMsV0FBRDtBQUFhLE1BQUEsR0FBRyxFQUFFQyxPQUFPLENBQUMsNkNBQUQsQ0FBekI7QUFBMEUsTUFBQSxLQUFLLEVBQUMsSUFBaEY7QUFBcUYsTUFBQSxNQUFNLEVBQUM7QUFBNUYsTUFBUDtBQUNILEdBeEJnRCxDQTBCakQ7OztBQUNBQyxFQUFBQSxVQUFVLEdBQUc7QUFDVCxVQUFNQyxPQUFPLEdBQUcsS0FBS0MsS0FBTCxDQUFXQyxPQUFYLElBQXNCLEtBQUtELEtBQUwsQ0FBV0MsT0FBWCxDQUFtQkMsVUFBbkIsRUFBdEM7QUFFQSxRQUFJLENBQUNILE9BQUQsSUFBWSxDQUFDQSxPQUFPLENBQUNJLElBQXJCLElBQTZCLENBQUNKLE9BQU8sQ0FBQ0ssSUFBdEMsSUFBOEMsQ0FBQ0wsT0FBTyxDQUFDSyxJQUFSLENBQWFDLENBQWhFLEVBQW1FLE9BQU8sSUFBUDtBQUVuRSxVQUFNQyxPQUFPLEdBQUdYLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxXQUFPO0FBQUssTUFBQSxLQUFLLEVBQUU7QUFBQ1csUUFBQUEsSUFBSSxFQUFFUixPQUFPLENBQUNLLElBQVIsQ0FBYUMsQ0FBYixHQUFpQjtBQUF4QixPQUFaO0FBQTJDLE1BQUEsU0FBUyxFQUFDO0FBQXJELE9BQ0gsNkJBQUMsT0FBRDtBQUFTLE1BQUEsS0FBSyxFQUFFTixPQUFPLENBQUNJO0FBQXhCLE1BREcsQ0FBUDtBQUdILEdBcENnRCxDQXNDakQ7OztBQUNBSyxFQUFBQSxXQUFXLEdBQUc7QUFDVixXQUFPLElBQVA7QUFDSDs7QUF6Q2dEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgTUltYWdlQm9keSBmcm9tICcuL01JbWFnZUJvZHknO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTVN0aWNrZXJCb2R5IGV4dGVuZHMgTUltYWdlQm9keSB7XHJcbiAgICAvLyBNb3N0bHkgZW1wdHkgdG8gcHJldmVudCBkZWZhdWx0IGJlaGF2aW91ciBvZiBNSW1hZ2VCb2R5XHJcbiAgICBvbkNsaWNrKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2hvd0ltYWdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd0ltYWdlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIE1TdGlja2VyQm9keSBkb2Vzbid0IG5lZWQgYSB3cmFwcGluZyBgPGEgaHJlZj0uLi4+YCwgYnV0IGl0IGRvZXMgbmVlZCBleHRyYSBwYWRkaW5nXHJcbiAgICAvLyB3aGljaCBpcyBhZGRlZCBieSBteF9NU3RpY2tlckJvZHlfd3JhcHBlclxyXG4gICAgd3JhcEltYWdlKGNvbnRlbnRVcmwsIGNoaWxkcmVuKSB7XHJcbiAgICAgICAgbGV0IG9uQ2xpY2sgPSBudWxsO1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5zaG93SW1hZ2UpIHtcclxuICAgICAgICAgICAgb25DbGljayA9IHRoaXMub25DbGljaztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfTVN0aWNrZXJCb2R5X3dyYXBwZXJcIiBvbkNsaWNrPXtvbkNsaWNrfT4geyBjaGlsZHJlbiB9IDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQbGFjZWhvbGRlciB0byBzaG93IGluIHBsYWNlIG9mIHRoZSBzdGlja2VyIGltYWdlIGlmXHJcbiAgICAvLyBpbWcgb25Mb2FkIGhhc24ndCBmaXJlZCB5ZXQuXHJcbiAgICBnZXRQbGFjZWhvbGRlcigpIHtcclxuICAgICAgICBjb25zdCBUaW50YWJsZVNWRyA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlRpbnRhYmxlU3ZnJyk7XHJcbiAgICAgICAgcmV0dXJuIDxUaW50YWJsZVNWRyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ljb25zLXNob3ctc3RpY2tlcnMuc3ZnXCIpfSB3aWR0aD1cIjc1XCIgaGVpZ2h0PVwiNzVcIiAvPjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUb29sdGlwIHRvIHNob3cgb24gbW91c2Ugb3ZlclxyXG4gICAgZ2V0VG9vbHRpcCgpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50ICYmIHRoaXMucHJvcHMubXhFdmVudC5nZXRDb250ZW50KCk7XHJcblxyXG4gICAgICAgIGlmICghY29udGVudCB8fCAhY29udGVudC5ib2R5IHx8ICFjb250ZW50LmluZm8gfHwgIWNvbnRlbnQuaW5mby53KSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgVG9vbHRpcCA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlRvb2x0aXAnKTtcclxuICAgICAgICByZXR1cm4gPGRpdiBzdHlsZT17e2xlZnQ6IGNvbnRlbnQuaW5mby53ICsgJ3B4J319IGNsYXNzTmFtZT1cIm14X01TdGlja2VyQm9keV90b29sdGlwXCI+XHJcbiAgICAgICAgICAgIDxUb29sdGlwIGxhYmVsPXtjb250ZW50LmJvZHl9IC8+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIERvbid0IHNob3cgXCJEb3dubG9hZCB0aGlzX2ZpbGUucG5nIC4uLlwiXHJcbiAgICBnZXRGaWxlQm9keSgpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxufVxyXG4iXX0=