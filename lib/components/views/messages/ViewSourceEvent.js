"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class ViewSourceEvent extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onToggle", ev => {
      ev.preventDefault();
      const {
        expanded
      } = this.state;
      this.setState({
        expanded: !expanded
      });
    });
    this.state = {
      expanded: false
    };
  }

  componentDidMount() {
    const {
      mxEvent
    } = this.props;

    if (mxEvent.isBeingDecrypted()) {
      mxEvent.once("Event.decrypted", () => this.forceUpdate());
    }
  }

  render() {
    const {
      mxEvent
    } = this.props;
    const {
      expanded
    } = this.state;
    let content;

    if (expanded) {
      content = _react.default.createElement("pre", null, JSON.stringify(mxEvent, null, 4));
    } else {
      content = _react.default.createElement("code", null, "{ \"type\": ".concat(mxEvent.getType(), " }"));
    }

    const classes = (0, _classnames.default)("mx_ViewSourceEvent mx_EventTile_content", {
      mx_ViewSourceEvent_expanded: expanded
    });
    return _react.default.createElement("span", {
      className: classes
    }, content, _react.default.createElement("a", {
      className: "mx_ViewSourceEvent_toggle",
      href: "#",
      onClick: this.onToggle
    }));
  }

}

exports.default = ViewSourceEvent;
(0, _defineProperty2.default)(ViewSourceEvent, "propTypes", {
  /* the MatrixEvent to show */
  mxEvent: _propTypes.default.object.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL1ZpZXdTb3VyY2VFdmVudC5qcyJdLCJuYW1lcyI6WyJWaWV3U291cmNlRXZlbnQiLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiZXYiLCJwcmV2ZW50RGVmYXVsdCIsImV4cGFuZGVkIiwic3RhdGUiLCJzZXRTdGF0ZSIsImNvbXBvbmVudERpZE1vdW50IiwibXhFdmVudCIsImlzQmVpbmdEZWNyeXB0ZWQiLCJvbmNlIiwiZm9yY2VVcGRhdGUiLCJyZW5kZXIiLCJjb250ZW50IiwiSlNPTiIsInN0cmluZ2lmeSIsImdldFR5cGUiLCJjbGFzc2VzIiwibXhfVmlld1NvdXJjZUV2ZW50X2V4cGFuZGVkIiwib25Ub2dnbGUiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFsQkE7Ozs7Ozs7Ozs7Ozs7OztBQW9CZSxNQUFNQSxlQUFOLFNBQThCQyxlQUFNQyxhQUFwQyxDQUFrRDtBQU03REMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsb0RBZVBDLEVBQUQsSUFBUTtBQUNmQSxNQUFBQSxFQUFFLENBQUNDLGNBQUg7QUFDQSxZQUFNO0FBQUVDLFFBQUFBO0FBQUYsVUFBZSxLQUFLQyxLQUExQjtBQUNBLFdBQUtDLFFBQUwsQ0FBYztBQUNWRixRQUFBQSxRQUFRLEVBQUUsQ0FBQ0E7QUFERCxPQUFkO0FBR0gsS0FyQmtCO0FBR2YsU0FBS0MsS0FBTCxHQUFhO0FBQ1RELE1BQUFBLFFBQVEsRUFBRTtBQURELEtBQWI7QUFHSDs7QUFFREcsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsVUFBTTtBQUFDQyxNQUFBQTtBQUFELFFBQVksS0FBS1AsS0FBdkI7O0FBQ0EsUUFBSU8sT0FBTyxDQUFDQyxnQkFBUixFQUFKLEVBQWdDO0FBQzVCRCxNQUFBQSxPQUFPLENBQUNFLElBQVIsQ0FBYSxpQkFBYixFQUFnQyxNQUFNLEtBQUtDLFdBQUwsRUFBdEM7QUFDSDtBQUNKOztBQVVEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNO0FBQUVKLE1BQUFBO0FBQUYsUUFBYyxLQUFLUCxLQUF6QjtBQUNBLFVBQU07QUFBRUcsTUFBQUE7QUFBRixRQUFlLEtBQUtDLEtBQTFCO0FBRUEsUUFBSVEsT0FBSjs7QUFDQSxRQUFJVCxRQUFKLEVBQWM7QUFDVlMsTUFBQUEsT0FBTyxHQUFHLDBDQUFNQyxJQUFJLENBQUNDLFNBQUwsQ0FBZVAsT0FBZixFQUF3QixJQUF4QixFQUE4QixDQUE5QixDQUFOLENBQVY7QUFDSCxLQUZELE1BRU87QUFDSEssTUFBQUEsT0FBTyxHQUFHLGlFQUFvQkwsT0FBTyxDQUFDUSxPQUFSLEVBQXBCLFFBQVY7QUFDSDs7QUFFRCxVQUFNQyxPQUFPLEdBQUcseUJBQVcseUNBQVgsRUFBc0Q7QUFDbEVDLE1BQUFBLDJCQUEyQixFQUFFZDtBQURxQyxLQUF0RCxDQUFoQjtBQUlBLFdBQU87QUFBTSxNQUFBLFNBQVMsRUFBRWE7QUFBakIsT0FDRkosT0FERSxFQUVIO0FBQ0ksTUFBQSxTQUFTLEVBQUMsMkJBRGQ7QUFFSSxNQUFBLElBQUksRUFBQyxHQUZUO0FBR0ksTUFBQSxPQUFPLEVBQUUsS0FBS007QUFIbEIsTUFGRyxDQUFQO0FBUUg7O0FBcEQ0RDs7OzhCQUE1Q3RCLGUsZUFDRTtBQUNmO0FBQ0FXLEVBQUFBLE9BQU8sRUFBRVksbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRlgsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWaWV3U291cmNlRXZlbnQgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLyogdGhlIE1hdHJpeEV2ZW50IHRvIHNob3cgKi9cclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBleHBhbmRlZDogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBjb25zdCB7bXhFdmVudH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGlmIChteEV2ZW50LmlzQmVpbmdEZWNyeXB0ZWQoKSkge1xyXG4gICAgICAgICAgICBteEV2ZW50Lm9uY2UoXCJFdmVudC5kZWNyeXB0ZWRcIiwgKCkgPT4gdGhpcy5mb3JjZVVwZGF0ZSgpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Ub2dnbGUgPSAoZXYpID0+IHtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGNvbnN0IHsgZXhwYW5kZWQgfSA9IHRoaXMuc3RhdGU7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGV4cGFuZGVkOiAhZXhwYW5kZWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IHsgbXhFdmVudCB9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBjb25zdCB7IGV4cGFuZGVkIH0gPSB0aGlzLnN0YXRlO1xyXG5cclxuICAgICAgICBsZXQgY29udGVudDtcclxuICAgICAgICBpZiAoZXhwYW5kZWQpIHtcclxuICAgICAgICAgICAgY29udGVudCA9IDxwcmU+e0pTT04uc3RyaW5naWZ5KG14RXZlbnQsIG51bGwsIDQpfTwvcHJlPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPGNvZGU+e2B7IFwidHlwZVwiOiAke214RXZlbnQuZ2V0VHlwZSgpfSB9YH08L2NvZGU+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9WaWV3U291cmNlRXZlbnQgbXhfRXZlbnRUaWxlX2NvbnRlbnRcIiwge1xyXG4gICAgICAgICAgICBteF9WaWV3U291cmNlRXZlbnRfZXhwYW5kZWQ6IGV4cGFuZGVkLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gPHNwYW4gY2xhc3NOYW1lPXtjbGFzc2VzfT5cclxuICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9WaWV3U291cmNlRXZlbnRfdG9nZ2xlXCJcclxuICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25Ub2dnbGV9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9zcGFuPjtcclxuICAgIH1cclxufVxyXG4iXX0=