"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _filesize = _interopRequireDefault(require("filesize"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _DecryptFile = require("../../../utils/DecryptFile");

var _Tinter = _interopRequireDefault(require("../../../Tinter"));

var _browserRequest = _interopRequireDefault(require("browser-request"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// A cached tinted copy of require("../../../../res/img/download.svg")
let tintedDownloadImageURL; // Track a list of mounted MFileBody instances so that we can update
// the require("../../../../res/img/download.svg") when the tint changes.

let nextMountId = 0;
const mounts = {};
/**
 * Updates the tinted copy of require("../../../../res/img/download.svg") when the tint changes.
 */

function updateTintedDownloadImage() {
  // Download the svg as an XML document.
  // We could cache the XML response here, but since the tint rarely changes
  // it's probably not worth it.
  // Also note that we can't use fetch here because fetch doesn't support
  // file URLs, which the download image will be if we're running from
  // the filesystem (like in an Electron wrapper).
  (0, _browserRequest.default)({
    uri: require("../../../../res/img/download.svg")
  }, (err, response, body) => {
    if (err) return;
    const svg = new DOMParser().parseFromString(body, "image/svg+xml"); // Apply the fixups to the XML.

    const fixups = _Tinter.default.calcSvgFixups([{
      contentDocument: svg
    }]);

    _Tinter.default.applySvgFixups(fixups); // Encoded the fixed up SVG as a data URL.


    const svgString = new XMLSerializer().serializeToString(svg);
    tintedDownloadImageURL = "data:image/svg+xml;base64," + window.btoa(svgString); // Notify each mounted MFileBody that the URL has changed.

    Object.keys(mounts).forEach(function (id) {
      mounts[id].tint();
    });
  });
}

_Tinter.default.registerTintable(updateTintedDownloadImage); // User supplied content can contain scripts, we have to be careful that
// we don't accidentally run those script within the same origin as the
// client. Otherwise those scripts written by remote users can read
// the access token and end-to-end keys that are in local storage.
//
// For attachments downloaded directly from the homeserver we can use
// Content-Security-Policy headers to disable script execution.
//
// But attachments with end-to-end encryption are more difficult to handle.
// We need to decrypt the attachment on the client and then display it.
// To display the attachment we need to turn the decrypted bytes into a URL.
//
// There are two ways to turn bytes into URLs, data URL and blob URLs.
// Data URLs aren't suitable for downloading a file because Chrome has a
// 2MB limit on the size of URLs that can be viewed in the browser or
// downloaded. This limit does not seem to apply when the url is used as
// the source attribute of an image tag.
//
// Blob URLs are generated using window.URL.createObjectURL and unfortunately
// for our purposes they inherit the origin of the page that created them.
// This means that any scripts that run when the URL is viewed will be able
// to access local storage.
//
// The easiest solution is to host the code that generates the blob URL on
// a different domain to the client.
// Another possibility is to generate the blob URL within a sandboxed iframe.
// The downside of using a second domain is that it complicates hosting,
// the downside of using a sandboxed iframe is that the browers are overly
// restrictive in what you are allowed to do with the generated URL.

/**
 * Get the current CSS style for a DOMElement.
 * @param {HTMLElement} element The element to get the current style of.
 * @return {string} The CSS style encoded as a string.
 */


function computedStyle(element) {
  if (!element) {
    return "";
  }

  const style = window.getComputedStyle(element, null);
  let cssText = style.cssText;

  if (cssText == "") {
    // Firefox doesn't implement ".cssText" for computed styles.
    // https://bugzilla.mozilla.org/show_bug.cgi?id=137687
    for (let i = 0; i < style.length; i++) {
      cssText += style[i] + ":";
      cssText += style.getPropertyValue(style[i]) + ";";
    }
  }

  return cssText;
}

var _default = (0, _createReactClass.default)({
  displayName: 'MFileBody',
  getInitialState: function () {
    return {
      decryptedBlob: this.props.decryptedBlob ? this.props.decryptedBlob : null
    };
  },
  propTypes: {
    /* the MatrixEvent to show */
    mxEvent: _propTypes.default.object.isRequired,

    /* already decrypted blob */
    decryptedBlob: _propTypes.default.object,

    /* called when the download link iframe is shown */
    onHeightChanged: _propTypes.default.func,

    /* the shape of the tile, used */
    tileShape: _propTypes.default.string
  },

  /**
   * Extracts a human readable label for the file attachment to use as
   * link text.
   *
   * @params {Object} content The "content" key of the matrix event.
   * @return {string} the human readable link text for the attachment.
   */
  presentableTextForFile: function (content) {
    let linkText = (0, _languageHandler._t)("Attachment");

    if (content.body && content.body.length > 0) {
      // The content body should be the name of the file including a
      // file extension.
      linkText = content.body;
    }

    if (content.info && content.info.size) {
      // If we know the size of the file then add it as human readable
      // string to the end of the link text so that the user knows how
      // big a file they are downloading.
      // The content.info also contains a MIME-type but we don't display
      // it since it is "ugly", users generally aren't aware what it
      // means and the type of the attachment can usually be inferrered
      // from the file extension.
      linkText += ' (' + (0, _filesize.default)(content.info.size) + ')';
    }

    return linkText;
  },
  _getContentUrl: function () {
    const content = this.props.mxEvent.getContent();
    return _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(content.url);
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._iframe = (0, _react.createRef)();
    this._dummyLink = (0, _react.createRef)();
    this._downloadImage = (0, _react.createRef)();
  },
  componentDidMount: function () {
    // Add this to the list of mounted components to receive notifications
    // when the tint changes.
    this.id = nextMountId++;
    mounts[this.id] = this;
    this.tint();
  },
  componentDidUpdate: function (prevProps, prevState) {
    if (this.props.onHeightChanged && !prevState.decryptedBlob && this.state.decryptedBlob) {
      this.props.onHeightChanged();
    }
  },
  componentWillUnmount: function () {
    // Remove this from the list of mounted components
    delete mounts[this.id];
  },
  tint: function () {
    // Update our tinted copy of require("../../../../res/img/download.svg")
    if (this._downloadImage.current) {
      this._downloadImage.current.src = tintedDownloadImageURL;
    }

    if (this._iframe.current) {
      // If the attachment is encrypted then the download image
      // will be inside the iframe so we wont be able to update
      // it directly.
      this._iframe.current.contentWindow.postMessage({
        imgSrc: tintedDownloadImageURL,
        style: computedStyle(this._dummyLink.current)
      }, "*");
    }
  },
  render: function () {
    const content = this.props.mxEvent.getContent();
    const text = this.presentableTextForFile(content);
    const isEncrypted = content.file !== undefined;
    const fileName = content.body && content.body.length > 0 ? content.body : (0, _languageHandler._t)("Attachment");

    const contentUrl = this._getContentUrl();

    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const fileSize = content.info ? content.info.size : null;
    const fileType = content.info ? content.info.mimetype : "application/octet-stream";

    if (isEncrypted) {
      if (this.state.decryptedBlob === null) {
        // Need to decrypt the attachment
        // Wait for the user to click on the link before downloading
        // and decrypting the attachment.
        let decrypting = false;

        const decrypt = e => {
          if (decrypting) {
            return false;
          }

          decrypting = true;
          (0, _DecryptFile.decryptFile)(content.file).then(blob => {
            this.setState({
              decryptedBlob: blob
            });
          }).catch(err => {
            console.warn("Unable to decrypt attachment: ", err);

            _Modal.default.createTrackedDialog('Error decrypting attachment', '', ErrorDialog, {
              title: (0, _languageHandler._t)("Error"),
              description: (0, _languageHandler._t)("Error decrypting attachment")
            });
          }).finally(() => {
            decrypting = false;
          });
        }; // This button should actually Download because usercontent/ will try to click itself
        // but it is not guaranteed between various browsers' settings.


        return _react.default.createElement("span", {
          className: "mx_MFileBody"
        }, _react.default.createElement("div", {
          className: "mx_MFileBody_download"
        }, _react.default.createElement(_AccessibleButton.default, {
          onClick: decrypt
        }, (0, _languageHandler._t)("Decrypt %(text)s", {
          text: text
        }))));
      } // When the iframe loads we tell it to render a download link


      const onIframeLoad = ev => {
        ev.target.contentWindow.postMessage({
          imgSrc: tintedDownloadImageURL,
          style: computedStyle(this._dummyLink.current),
          blob: this.state.decryptedBlob,
          // Set a download attribute for encrypted files so that the file
          // will have the correct name when the user tries to download it.
          // We can't provide a Content-Disposition header like we would for HTTP.
          download: fileName,
          textContent: (0, _languageHandler._t)("Download %(text)s", {
            text: text
          }),
          // only auto-download if a user triggered this iframe explicitly
          auto: !this.props.decryptedBlob
        }, "*");
      };

      const url = "usercontent/"; // XXX: this path should probably be passed from the skin
      // If the attachment is encrypted then put the link inside an iframe.

      return _react.default.createElement("span", {
        className: "mx_MFileBody"
      }, _react.default.createElement("div", {
        className: "mx_MFileBody_download"
      }, _react.default.createElement("div", {
        style: {
          display: "none"
        }
      }, _react.default.createElement("a", {
        ref: this._dummyLink
      })), _react.default.createElement("iframe", {
        src: "".concat(url, "?origin=").concat(encodeURIComponent(window.location.origin)),
        onLoad: onIframeLoad,
        ref: this._iframe,
        sandbox: "allow-scripts allow-downloads allow-downloads-without-user-activation"
      })));
    } else if (contentUrl) {
      const downloadProps = {
        target: "_blank",
        rel: "noreferrer noopener",
        // We set the href regardless of whether or not we intercept the download
        // because we don't really want to convert the file to a blob eagerly, and
        // still want "open in new tab" and "save link as" to work.
        href: contentUrl
      }; // Blobs can only have up to 500mb, so if the file reports as being too large then
      // we won't try and convert it. Likewise, if the file size is unknown then we'll assume
      // it is too big. There is the risk of the reported file size and the actual file size
      // being different, however the user shouldn't normally run into this problem.

      const fileTooBig = typeof fileSize === 'number' ? fileSize > 524288000 : true;

      if (["application/pdf"].includes(fileType) && !fileTooBig) {
        // We want to force a download on this type, so use an onClick handler.
        downloadProps["onClick"] = e => {
          console.log("Downloading ".concat(fileType, " as blob (unencrypted)")); // Avoid letting the <a> do its thing

          e.preventDefault();
          e.stopPropagation(); // Start a fetch for the download
          // Based upon https://stackoverflow.com/a/49500465

          fetch(contentUrl).then(response => response.blob()).then(blob => {
            const blobUrl = URL.createObjectURL(blob); // We have to create an anchor to download the file

            const tempAnchor = document.createElement('a');
            tempAnchor.download = fileName;
            tempAnchor.href = blobUrl;
            document.body.appendChild(tempAnchor); // for firefox: https://stackoverflow.com/a/32226068

            tempAnchor.click();
            tempAnchor.remove();
          });
        };
      } else {
        // Else we are hoping the browser will do the right thing
        downloadProps["download"] = fileName;
      } // If the attachment is not encrypted then we check whether we
      // are being displayed in the room timeline or in a list of
      // files in the right hand side of the screen.


      if (this.props.tileShape === "file_grid") {
        return _react.default.createElement("span", {
          className: "mx_MFileBody"
        }, _react.default.createElement("div", {
          className: "mx_MFileBody_download"
        }, _react.default.createElement("a", (0, _extends2.default)({
          className: "mx_MFileBody_downloadLink"
        }, downloadProps), fileName), _react.default.createElement("div", {
          className: "mx_MImageBody_size"
        }, content.info && content.info.size ? (0, _filesize.default)(content.info.size) : "")));
      } else {
        return _react.default.createElement("span", {
          className: "mx_MFileBody"
        }, _react.default.createElement("div", {
          className: "mx_MFileBody_download"
        }, _react.default.createElement("a", downloadProps, _react.default.createElement("img", {
          src: tintedDownloadImageURL,
          width: "12",
          height: "14",
          ref: this._downloadImage
        }), (0, _languageHandler._t)("Download %(text)s", {
          text: text
        }))));
      }
    } else {
      const extra = text ? ': ' + text : '';
      return _react.default.createElement("span", {
        className: "mx_MFileBody"
      }, (0, _languageHandler._t)("Invalid file%(extra)s", {
        extra: extra
      }));
    }
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01GaWxlQm9keS5qcyJdLCJuYW1lcyI6WyJ0aW50ZWREb3dubG9hZEltYWdlVVJMIiwibmV4dE1vdW50SWQiLCJtb3VudHMiLCJ1cGRhdGVUaW50ZWREb3dubG9hZEltYWdlIiwidXJpIiwicmVxdWlyZSIsImVyciIsInJlc3BvbnNlIiwiYm9keSIsInN2ZyIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsImZpeHVwcyIsIlRpbnRlciIsImNhbGNTdmdGaXh1cHMiLCJjb250ZW50RG9jdW1lbnQiLCJhcHBseVN2Z0ZpeHVwcyIsInN2Z1N0cmluZyIsIlhNTFNlcmlhbGl6ZXIiLCJzZXJpYWxpemVUb1N0cmluZyIsIndpbmRvdyIsImJ0b2EiLCJPYmplY3QiLCJrZXlzIiwiZm9yRWFjaCIsImlkIiwidGludCIsInJlZ2lzdGVyVGludGFibGUiLCJjb21wdXRlZFN0eWxlIiwiZWxlbWVudCIsInN0eWxlIiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImNzc1RleHQiLCJpIiwibGVuZ3RoIiwiZ2V0UHJvcGVydHlWYWx1ZSIsImRpc3BsYXlOYW1lIiwiZ2V0SW5pdGlhbFN0YXRlIiwiZGVjcnlwdGVkQmxvYiIsInByb3BzIiwicHJvcFR5cGVzIiwibXhFdmVudCIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJvbkhlaWdodENoYW5nZWQiLCJmdW5jIiwidGlsZVNoYXBlIiwic3RyaW5nIiwicHJlc2VudGFibGVUZXh0Rm9yRmlsZSIsImNvbnRlbnQiLCJsaW5rVGV4dCIsImluZm8iLCJzaXplIiwiX2dldENvbnRlbnRVcmwiLCJnZXRDb250ZW50IiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwibXhjVXJsVG9IdHRwIiwidXJsIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCIsIl9pZnJhbWUiLCJfZHVtbXlMaW5rIiwiX2Rvd25sb2FkSW1hZ2UiLCJjb21wb25lbnREaWRNb3VudCIsImNvbXBvbmVudERpZFVwZGF0ZSIsInByZXZQcm9wcyIsInByZXZTdGF0ZSIsInN0YXRlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJjdXJyZW50Iiwic3JjIiwiY29udGVudFdpbmRvdyIsInBvc3RNZXNzYWdlIiwiaW1nU3JjIiwicmVuZGVyIiwidGV4dCIsImlzRW5jcnlwdGVkIiwiZmlsZSIsInVuZGVmaW5lZCIsImZpbGVOYW1lIiwiY29udGVudFVybCIsIkVycm9yRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZmlsZVNpemUiLCJmaWxlVHlwZSIsIm1pbWV0eXBlIiwiZGVjcnlwdGluZyIsImRlY3J5cHQiLCJlIiwidGhlbiIsImJsb2IiLCJzZXRTdGF0ZSIsImNhdGNoIiwiY29uc29sZSIsIndhcm4iLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiZmluYWxseSIsIm9uSWZyYW1lTG9hZCIsImV2IiwidGFyZ2V0IiwiZG93bmxvYWQiLCJ0ZXh0Q29udGVudCIsImF1dG8iLCJkaXNwbGF5IiwiZW5jb2RlVVJJQ29tcG9uZW50IiwibG9jYXRpb24iLCJvcmlnaW4iLCJkb3dubG9hZFByb3BzIiwicmVsIiwiaHJlZiIsImZpbGVUb29CaWciLCJpbmNsdWRlcyIsImxvZyIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiZmV0Y2giLCJibG9iVXJsIiwiVVJMIiwiY3JlYXRlT2JqZWN0VVJMIiwidGVtcEFuY2hvciIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImFwcGVuZENoaWxkIiwiY2xpY2siLCJyZW1vdmUiLCJleHRyYSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQkE7QUFDQSxJQUFJQSxzQkFBSixDLENBQ0E7QUFDQTs7QUFDQSxJQUFJQyxXQUFXLEdBQUcsQ0FBbEI7QUFDQSxNQUFNQyxNQUFNLEdBQUcsRUFBZjtBQUVBOzs7O0FBR0EsU0FBU0MseUJBQVQsR0FBcUM7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQVE7QUFBQ0MsSUFBQUEsR0FBRyxFQUFFQyxPQUFPLENBQUMsa0NBQUQ7QUFBYixHQUFSLEVBQTRELENBQUNDLEdBQUQsRUFBTUMsUUFBTixFQUFnQkMsSUFBaEIsS0FBeUI7QUFDakYsUUFBSUYsR0FBSixFQUFTO0FBRVQsVUFBTUcsR0FBRyxHQUFHLElBQUlDLFNBQUosR0FBZ0JDLGVBQWhCLENBQWdDSCxJQUFoQyxFQUFzQyxlQUF0QyxDQUFaLENBSGlGLENBSWpGOztBQUNBLFVBQU1JLE1BQU0sR0FBR0MsZ0JBQU9DLGFBQVAsQ0FBcUIsQ0FBQztBQUFDQyxNQUFBQSxlQUFlLEVBQUVOO0FBQWxCLEtBQUQsQ0FBckIsQ0FBZjs7QUFDQUksb0JBQU9HLGNBQVAsQ0FBc0JKLE1BQXRCLEVBTmlGLENBT2pGOzs7QUFDQSxVQUFNSyxTQUFTLEdBQUcsSUFBSUMsYUFBSixHQUFvQkMsaUJBQXBCLENBQXNDVixHQUF0QyxDQUFsQjtBQUNBVCxJQUFBQSxzQkFBc0IsR0FBRywrQkFBK0JvQixNQUFNLENBQUNDLElBQVAsQ0FBWUosU0FBWixDQUF4RCxDQVRpRixDQVVqRjs7QUFDQUssSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlyQixNQUFaLEVBQW9Cc0IsT0FBcEIsQ0FBNEIsVUFBU0MsRUFBVCxFQUFhO0FBQ3JDdkIsTUFBQUEsTUFBTSxDQUFDdUIsRUFBRCxDQUFOLENBQVdDLElBQVg7QUFDSCxLQUZEO0FBR0gsR0FkRDtBQWVIOztBQUVEYixnQkFBT2MsZ0JBQVAsQ0FBd0J4Qix5QkFBeEIsRSxDQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFLQSxTQUFTeUIsYUFBVCxDQUF1QkMsT0FBdkIsRUFBZ0M7QUFDNUIsTUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDVixXQUFPLEVBQVA7QUFDSDs7QUFDRCxRQUFNQyxLQUFLLEdBQUdWLE1BQU0sQ0FBQ1csZ0JBQVAsQ0FBd0JGLE9BQXhCLEVBQWlDLElBQWpDLENBQWQ7QUFDQSxNQUFJRyxPQUFPLEdBQUdGLEtBQUssQ0FBQ0UsT0FBcEI7O0FBQ0EsTUFBSUEsT0FBTyxJQUFJLEVBQWYsRUFBbUI7QUFDZjtBQUNBO0FBQ0EsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxLQUFLLENBQUNJLE1BQTFCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXVDO0FBQ25DRCxNQUFBQSxPQUFPLElBQUlGLEtBQUssQ0FBQ0csQ0FBRCxDQUFMLEdBQVcsR0FBdEI7QUFDQUQsTUFBQUEsT0FBTyxJQUFJRixLQUFLLENBQUNLLGdCQUFOLENBQXVCTCxLQUFLLENBQUNHLENBQUQsQ0FBNUIsSUFBbUMsR0FBOUM7QUFDSDtBQUNKOztBQUNELFNBQU9ELE9BQVA7QUFDSDs7ZUFFYywrQkFBaUI7QUFDNUJJLEVBQUFBLFdBQVcsRUFBRSxXQURlO0FBRzVCQyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLGFBQWEsRUFBRyxLQUFLQyxLQUFMLENBQVdELGFBQVgsR0FBMkIsS0FBS0MsS0FBTCxDQUFXRCxhQUF0QyxHQUFzRDtBQURuRSxLQUFQO0FBR0gsR0FQMkI7QUFTNUJFLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0FDLElBQUFBLE9BQU8sRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRm5COztBQUdQO0FBQ0FOLElBQUFBLGFBQWEsRUFBRUksbUJBQVVDLE1BSmxCOztBQUtQO0FBQ0FFLElBQUFBLGVBQWUsRUFBRUgsbUJBQVVJLElBTnBCOztBQU9QO0FBQ0FDLElBQUFBLFNBQVMsRUFBRUwsbUJBQVVNO0FBUmQsR0FUaUI7O0FBb0I1Qjs7Ozs7OztBQU9BQyxFQUFBQSxzQkFBc0IsRUFBRSxVQUFTQyxPQUFULEVBQWtCO0FBQ3RDLFFBQUlDLFFBQVEsR0FBRyx5QkFBRyxZQUFILENBQWY7O0FBQ0EsUUFBSUQsT0FBTyxDQUFDMUMsSUFBUixJQUFnQjBDLE9BQU8sQ0FBQzFDLElBQVIsQ0FBYTBCLE1BQWIsR0FBc0IsQ0FBMUMsRUFBNkM7QUFDekM7QUFDQTtBQUNBaUIsTUFBQUEsUUFBUSxHQUFHRCxPQUFPLENBQUMxQyxJQUFuQjtBQUNIOztBQUVELFFBQUkwQyxPQUFPLENBQUNFLElBQVIsSUFBZ0JGLE9BQU8sQ0FBQ0UsSUFBUixDQUFhQyxJQUFqQyxFQUF1QztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBRixNQUFBQSxRQUFRLElBQUksT0FBTyx1QkFBU0QsT0FBTyxDQUFDRSxJQUFSLENBQWFDLElBQXRCLENBQVAsR0FBcUMsR0FBakQ7QUFDSDs7QUFDRCxXQUFPRixRQUFQO0FBQ0gsR0E5QzJCO0FBZ0Q1QkcsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIsVUFBTUosT0FBTyxHQUFHLEtBQUtYLEtBQUwsQ0FBV0UsT0FBWCxDQUFtQmMsVUFBbkIsRUFBaEI7QUFDQSxXQUFPQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxZQUF0QixDQUFtQ1IsT0FBTyxDQUFDUyxHQUEzQyxDQUFQO0FBQ0gsR0FuRDJCO0FBcUQ1QjtBQUNBQyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUtDLE9BQUwsR0FBZSx1QkFBZjtBQUNBLFNBQUtDLFVBQUwsR0FBa0IsdUJBQWxCO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQix1QkFBdEI7QUFDSCxHQTFEMkI7QUE0RDVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCO0FBQ0E7QUFDQSxTQUFLdkMsRUFBTCxHQUFVeEIsV0FBVyxFQUFyQjtBQUNBQyxJQUFBQSxNQUFNLENBQUMsS0FBS3VCLEVBQU4sQ0FBTixHQUFrQixJQUFsQjtBQUNBLFNBQUtDLElBQUw7QUFDSCxHQWxFMkI7QUFvRTVCdUMsRUFBQUEsa0JBQWtCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQkMsU0FBcEIsRUFBK0I7QUFDL0MsUUFBSSxLQUFLNUIsS0FBTCxDQUFXTSxlQUFYLElBQThCLENBQUNzQixTQUFTLENBQUM3QixhQUF6QyxJQUEwRCxLQUFLOEIsS0FBTCxDQUFXOUIsYUFBekUsRUFBd0Y7QUFDcEYsV0FBS0MsS0FBTCxDQUFXTSxlQUFYO0FBQ0g7QUFDSixHQXhFMkI7QUEwRTVCd0IsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QjtBQUNBLFdBQU9uRSxNQUFNLENBQUMsS0FBS3VCLEVBQU4sQ0FBYjtBQUNILEdBN0UyQjtBQStFNUJDLEVBQUFBLElBQUksRUFBRSxZQUFXO0FBQ2I7QUFDQSxRQUFJLEtBQUtxQyxjQUFMLENBQW9CTyxPQUF4QixFQUFpQztBQUM3QixXQUFLUCxjQUFMLENBQW9CTyxPQUFwQixDQUE0QkMsR0FBNUIsR0FBa0N2RSxzQkFBbEM7QUFDSDs7QUFDRCxRQUFJLEtBQUs2RCxPQUFMLENBQWFTLE9BQWpCLEVBQTBCO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBLFdBQUtULE9BQUwsQ0FBYVMsT0FBYixDQUFxQkUsYUFBckIsQ0FBbUNDLFdBQW5DLENBQStDO0FBQzNDQyxRQUFBQSxNQUFNLEVBQUUxRSxzQkFEbUM7QUFFM0M4QixRQUFBQSxLQUFLLEVBQUVGLGFBQWEsQ0FBQyxLQUFLa0MsVUFBTCxDQUFnQlEsT0FBakI7QUFGdUIsT0FBL0MsRUFHRyxHQUhIO0FBSUg7QUFDSixHQTdGMkI7QUErRjVCSyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU16QixPQUFPLEdBQUcsS0FBS1gsS0FBTCxDQUFXRSxPQUFYLENBQW1CYyxVQUFuQixFQUFoQjtBQUNBLFVBQU1xQixJQUFJLEdBQUcsS0FBSzNCLHNCQUFMLENBQTRCQyxPQUE1QixDQUFiO0FBQ0EsVUFBTTJCLFdBQVcsR0FBRzNCLE9BQU8sQ0FBQzRCLElBQVIsS0FBaUJDLFNBQXJDO0FBQ0EsVUFBTUMsUUFBUSxHQUFHOUIsT0FBTyxDQUFDMUMsSUFBUixJQUFnQjBDLE9BQU8sQ0FBQzFDLElBQVIsQ0FBYTBCLE1BQWIsR0FBc0IsQ0FBdEMsR0FBMENnQixPQUFPLENBQUMxQyxJQUFsRCxHQUF5RCx5QkFBRyxZQUFILENBQTFFOztBQUNBLFVBQU15RSxVQUFVLEdBQUcsS0FBSzNCLGNBQUwsRUFBbkI7O0FBQ0EsVUFBTTRCLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBLFVBQU1DLFFBQVEsR0FBR25DLE9BQU8sQ0FBQ0UsSUFBUixHQUFlRixPQUFPLENBQUNFLElBQVIsQ0FBYUMsSUFBNUIsR0FBbUMsSUFBcEQ7QUFDQSxVQUFNaUMsUUFBUSxHQUFHcEMsT0FBTyxDQUFDRSxJQUFSLEdBQWVGLE9BQU8sQ0FBQ0UsSUFBUixDQUFhbUMsUUFBNUIsR0FBdUMsMEJBQXhEOztBQUVBLFFBQUlWLFdBQUosRUFBaUI7QUFDYixVQUFJLEtBQUtULEtBQUwsQ0FBVzlCLGFBQVgsS0FBNkIsSUFBakMsRUFBdUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0EsWUFBSWtELFVBQVUsR0FBRyxLQUFqQjs7QUFDQSxjQUFNQyxPQUFPLEdBQUlDLENBQUQsSUFBTztBQUNuQixjQUFJRixVQUFKLEVBQWdCO0FBQ1osbUJBQU8sS0FBUDtBQUNIOztBQUNEQSxVQUFBQSxVQUFVLEdBQUcsSUFBYjtBQUNBLHdDQUFZdEMsT0FBTyxDQUFDNEIsSUFBcEIsRUFBMEJhLElBQTFCLENBQWdDQyxJQUFELElBQVU7QUFDckMsaUJBQUtDLFFBQUwsQ0FBYztBQUNWdkQsY0FBQUEsYUFBYSxFQUFFc0Q7QUFETCxhQUFkO0FBR0gsV0FKRCxFQUlHRSxLQUpILENBSVV4RixHQUFELElBQVM7QUFDZHlGLFlBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLGdDQUFiLEVBQStDMUYsR0FBL0M7O0FBQ0EyRiwyQkFBTUMsbUJBQU4sQ0FBMEIsNkJBQTFCLEVBQXlELEVBQXpELEVBQTZEaEIsV0FBN0QsRUFBMEU7QUFDdEVpQixjQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUQrRDtBQUV0RUMsY0FBQUEsV0FBVyxFQUFFLHlCQUFHLDZCQUFIO0FBRnlELGFBQTFFO0FBSUgsV0FWRCxFQVVHQyxPQVZILENBVVcsTUFBTTtBQUNiYixZQUFBQSxVQUFVLEdBQUcsS0FBYjtBQUNILFdBWkQ7QUFhSCxTQWxCRCxDQUxtQyxDQXlCbkM7QUFDQTs7O0FBQ0EsZUFDSTtBQUFNLFVBQUEsU0FBUyxFQUFDO0FBQWhCLFdBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0ksNkJBQUMseUJBQUQ7QUFBa0IsVUFBQSxPQUFPLEVBQUVDO0FBQTNCLFdBQ00seUJBQUcsa0JBQUgsRUFBdUI7QUFBRWIsVUFBQUEsSUFBSSxFQUFFQTtBQUFSLFNBQXZCLENBRE4sQ0FESixDQURKLENBREo7QUFTSCxPQXJDWSxDQXVDYjs7O0FBQ0EsWUFBTTBCLFlBQVksR0FBSUMsRUFBRCxJQUFRO0FBQ3pCQSxRQUFBQSxFQUFFLENBQUNDLE1BQUgsQ0FBVWhDLGFBQVYsQ0FBd0JDLFdBQXhCLENBQW9DO0FBQ2hDQyxVQUFBQSxNQUFNLEVBQUUxRSxzQkFEd0I7QUFFaEM4QixVQUFBQSxLQUFLLEVBQUVGLGFBQWEsQ0FBQyxLQUFLa0MsVUFBTCxDQUFnQlEsT0FBakIsQ0FGWTtBQUdoQ3NCLFVBQUFBLElBQUksRUFBRSxLQUFLeEIsS0FBTCxDQUFXOUIsYUFIZTtBQUloQztBQUNBO0FBQ0E7QUFDQW1FLFVBQUFBLFFBQVEsRUFBRXpCLFFBUHNCO0FBUWhDMEIsVUFBQUEsV0FBVyxFQUFFLHlCQUFHLG1CQUFILEVBQXdCO0FBQUU5QixZQUFBQSxJQUFJLEVBQUVBO0FBQVIsV0FBeEIsQ0FSbUI7QUFTaEM7QUFDQStCLFVBQUFBLElBQUksRUFBRSxDQUFDLEtBQUtwRSxLQUFMLENBQVdEO0FBVmMsU0FBcEMsRUFXRyxHQVhIO0FBWUgsT0FiRDs7QUFlQSxZQUFNcUIsR0FBRyxHQUFHLGNBQVosQ0F2RGEsQ0F1RGU7QUFFNUI7O0FBQ0EsYUFDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLEtBQUssRUFBRTtBQUFDaUQsVUFBQUEsT0FBTyxFQUFFO0FBQVY7QUFBWixTQU1JO0FBQUcsUUFBQSxHQUFHLEVBQUUsS0FBSzlDO0FBQWIsUUFOSixDQURKLEVBU0k7QUFDSSxRQUFBLEdBQUcsWUFBS0gsR0FBTCxxQkFBbUJrRCxrQkFBa0IsQ0FBQ3pGLE1BQU0sQ0FBQzBGLFFBQVAsQ0FBZ0JDLE1BQWpCLENBQXJDLENBRFA7QUFFSSxRQUFBLE1BQU0sRUFBRVQsWUFGWjtBQUdJLFFBQUEsR0FBRyxFQUFFLEtBQUt6QyxPQUhkO0FBSUksUUFBQSxPQUFPLEVBQUM7QUFKWixRQVRKLENBREosQ0FESjtBQW1CSCxLQTdFRCxNQTZFTyxJQUFJb0IsVUFBSixFQUFnQjtBQUNuQixZQUFNK0IsYUFBYSxHQUFHO0FBQ2xCUixRQUFBQSxNQUFNLEVBQUUsUUFEVTtBQUVsQlMsUUFBQUEsR0FBRyxFQUFFLHFCQUZhO0FBSWxCO0FBQ0E7QUFDQTtBQUNBQyxRQUFBQSxJQUFJLEVBQUVqQztBQVBZLE9BQXRCLENBRG1CLENBV25CO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFlBQU1rQyxVQUFVLEdBQUcsT0FBTzlCLFFBQVAsS0FBcUIsUUFBckIsR0FBZ0NBLFFBQVEsR0FBRyxTQUEzQyxHQUF1RCxJQUExRTs7QUFFQSxVQUFJLENBQUMsaUJBQUQsRUFBb0IrQixRQUFwQixDQUE2QjlCLFFBQTdCLEtBQTBDLENBQUM2QixVQUEvQyxFQUEyRDtBQUN2RDtBQUNBSCxRQUFBQSxhQUFhLENBQUMsU0FBRCxDQUFiLEdBQTRCdEIsQ0FBRCxJQUFPO0FBQzlCSyxVQUFBQSxPQUFPLENBQUNzQixHQUFSLHVCQUEyQi9CLFFBQTNCLDZCQUQ4QixDQUc5Qjs7QUFDQUksVUFBQUEsQ0FBQyxDQUFDNEIsY0FBRjtBQUNBNUIsVUFBQUEsQ0FBQyxDQUFDNkIsZUFBRixHQUw4QixDQU85QjtBQUNBOztBQUNBQyxVQUFBQSxLQUFLLENBQUN2QyxVQUFELENBQUwsQ0FBa0JVLElBQWxCLENBQXdCcEYsUUFBRCxJQUFjQSxRQUFRLENBQUNxRixJQUFULEVBQXJDLEVBQXNERCxJQUF0RCxDQUE0REMsSUFBRCxJQUFVO0FBQ2pFLGtCQUFNNkIsT0FBTyxHQUFHQyxHQUFHLENBQUNDLGVBQUosQ0FBb0IvQixJQUFwQixDQUFoQixDQURpRSxDQUdqRTs7QUFDQSxrQkFBTWdDLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxhQUFULENBQXVCLEdBQXZCLENBQW5CO0FBQ0FGLFlBQUFBLFVBQVUsQ0FBQ25CLFFBQVgsR0FBc0J6QixRQUF0QjtBQUNBNEMsWUFBQUEsVUFBVSxDQUFDVixJQUFYLEdBQWtCTyxPQUFsQjtBQUNBSSxZQUFBQSxRQUFRLENBQUNySCxJQUFULENBQWN1SCxXQUFkLENBQTBCSCxVQUExQixFQVBpRSxDQU8xQjs7QUFDdkNBLFlBQUFBLFVBQVUsQ0FBQ0ksS0FBWDtBQUNBSixZQUFBQSxVQUFVLENBQUNLLE1BQVg7QUFDSCxXQVZEO0FBV0gsU0FwQkQ7QUFxQkgsT0F2QkQsTUF1Qk87QUFDSDtBQUNBakIsUUFBQUEsYUFBYSxDQUFDLFVBQUQsQ0FBYixHQUE0QmhDLFFBQTVCO0FBQ0gsT0EzQ2tCLENBNkNuQjtBQUNBO0FBQ0E7OztBQUNBLFVBQUksS0FBS3pDLEtBQUwsQ0FBV1EsU0FBWCxLQUF5QixXQUE3QixFQUEwQztBQUN0QyxlQUNJO0FBQU0sVUFBQSxTQUFTLEVBQUM7QUFBaEIsV0FDSTtBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDSTtBQUFHLFVBQUEsU0FBUyxFQUFDO0FBQWIsV0FBNkNpRSxhQUE3QyxHQUNNaEMsUUFETixDQURKLEVBSUk7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ005QixPQUFPLENBQUNFLElBQVIsSUFBZ0JGLE9BQU8sQ0FBQ0UsSUFBUixDQUFhQyxJQUE3QixHQUFvQyx1QkFBU0gsT0FBTyxDQUFDRSxJQUFSLENBQWFDLElBQXRCLENBQXBDLEdBQWtFLEVBRHhFLENBSkosQ0FESixDQURKO0FBWUgsT0FiRCxNQWFPO0FBQ0gsZUFDSTtBQUFNLFVBQUEsU0FBUyxFQUFDO0FBQWhCLFdBQ0k7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0ksa0NBQU8yRCxhQUFQLEVBQ0k7QUFBSyxVQUFBLEdBQUcsRUFBRWhILHNCQUFWO0FBQWtDLFVBQUEsS0FBSyxFQUFDLElBQXhDO0FBQTZDLFVBQUEsTUFBTSxFQUFDLElBQXBEO0FBQXlELFVBQUEsR0FBRyxFQUFFLEtBQUsrRDtBQUFuRSxVQURKLEVBRU0seUJBQUcsbUJBQUgsRUFBd0I7QUFBRWEsVUFBQUEsSUFBSSxFQUFFQTtBQUFSLFNBQXhCLENBRk4sQ0FESixDQURKLENBREo7QUFVSDtBQUNKLEtBekVNLE1BeUVBO0FBQ0gsWUFBTXNELEtBQUssR0FBR3RELElBQUksR0FBSSxPQUFPQSxJQUFYLEdBQW1CLEVBQXJDO0FBQ0EsYUFBTztBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0QseUJBQUcsdUJBQUgsRUFBNEI7QUFBRXNELFFBQUFBLEtBQUssRUFBRUE7QUFBVCxPQUE1QixDQURDLENBQVA7QUFHSDtBQUNKO0FBclEyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IGZpbGVzaXplIGZyb20gJ2ZpbGVzaXplJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtkZWNyeXB0RmlsZX0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvRGVjcnlwdEZpbGUnO1xyXG5pbXBvcnQgVGludGVyIGZyb20gJy4uLy4uLy4uL1RpbnRlcic7XHJcbmltcG9ydCByZXF1ZXN0IGZyb20gJ2Jyb3dzZXItcmVxdWVzdCc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcblxyXG5cclxuLy8gQSBjYWNoZWQgdGludGVkIGNvcHkgb2YgcmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZG93bmxvYWQuc3ZnXCIpXHJcbmxldCB0aW50ZWREb3dubG9hZEltYWdlVVJMO1xyXG4vLyBUcmFjayBhIGxpc3Qgb2YgbW91bnRlZCBNRmlsZUJvZHkgaW5zdGFuY2VzIHNvIHRoYXQgd2UgY2FuIHVwZGF0ZVxyXG4vLyB0aGUgcmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZG93bmxvYWQuc3ZnXCIpIHdoZW4gdGhlIHRpbnQgY2hhbmdlcy5cclxubGV0IG5leHRNb3VudElkID0gMDtcclxuY29uc3QgbW91bnRzID0ge307XHJcblxyXG4vKipcclxuICogVXBkYXRlcyB0aGUgdGludGVkIGNvcHkgb2YgcmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZG93bmxvYWQuc3ZnXCIpIHdoZW4gdGhlIHRpbnQgY2hhbmdlcy5cclxuICovXHJcbmZ1bmN0aW9uIHVwZGF0ZVRpbnRlZERvd25sb2FkSW1hZ2UoKSB7XHJcbiAgICAvLyBEb3dubG9hZCB0aGUgc3ZnIGFzIGFuIFhNTCBkb2N1bWVudC5cclxuICAgIC8vIFdlIGNvdWxkIGNhY2hlIHRoZSBYTUwgcmVzcG9uc2UgaGVyZSwgYnV0IHNpbmNlIHRoZSB0aW50IHJhcmVseSBjaGFuZ2VzXHJcbiAgICAvLyBpdCdzIHByb2JhYmx5IG5vdCB3b3J0aCBpdC5cclxuICAgIC8vIEFsc28gbm90ZSB0aGF0IHdlIGNhbid0IHVzZSBmZXRjaCBoZXJlIGJlY2F1c2UgZmV0Y2ggZG9lc24ndCBzdXBwb3J0XHJcbiAgICAvLyBmaWxlIFVSTHMsIHdoaWNoIHRoZSBkb3dubG9hZCBpbWFnZSB3aWxsIGJlIGlmIHdlJ3JlIHJ1bm5pbmcgZnJvbVxyXG4gICAgLy8gdGhlIGZpbGVzeXN0ZW0gKGxpa2UgaW4gYW4gRWxlY3Ryb24gd3JhcHBlcikuXHJcbiAgICByZXF1ZXN0KHt1cmk6IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2Rvd25sb2FkLnN2Z1wiKX0sIChlcnIsIHJlc3BvbnNlLCBib2R5KSA9PiB7XHJcbiAgICAgICAgaWYgKGVycikgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBzdmcgPSBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKGJvZHksIFwiaW1hZ2Uvc3ZnK3htbFwiKTtcclxuICAgICAgICAvLyBBcHBseSB0aGUgZml4dXBzIHRvIHRoZSBYTUwuXHJcbiAgICAgICAgY29uc3QgZml4dXBzID0gVGludGVyLmNhbGNTdmdGaXh1cHMoW3tjb250ZW50RG9jdW1lbnQ6IHN2Z31dKTtcclxuICAgICAgICBUaW50ZXIuYXBwbHlTdmdGaXh1cHMoZml4dXBzKTtcclxuICAgICAgICAvLyBFbmNvZGVkIHRoZSBmaXhlZCB1cCBTVkcgYXMgYSBkYXRhIFVSTC5cclxuICAgICAgICBjb25zdCBzdmdTdHJpbmcgPSBuZXcgWE1MU2VyaWFsaXplcigpLnNlcmlhbGl6ZVRvU3RyaW5nKHN2Zyk7XHJcbiAgICAgICAgdGludGVkRG93bmxvYWRJbWFnZVVSTCA9IFwiZGF0YTppbWFnZS9zdmcreG1sO2Jhc2U2NCxcIiArIHdpbmRvdy5idG9hKHN2Z1N0cmluZyk7XHJcbiAgICAgICAgLy8gTm90aWZ5IGVhY2ggbW91bnRlZCBNRmlsZUJvZHkgdGhhdCB0aGUgVVJMIGhhcyBjaGFuZ2VkLlxyXG4gICAgICAgIE9iamVjdC5rZXlzKG1vdW50cykuZm9yRWFjaChmdW5jdGlvbihpZCkge1xyXG4gICAgICAgICAgICBtb3VudHNbaWRdLnRpbnQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5UaW50ZXIucmVnaXN0ZXJUaW50YWJsZSh1cGRhdGVUaW50ZWREb3dubG9hZEltYWdlKTtcclxuXHJcbi8vIFVzZXIgc3VwcGxpZWQgY29udGVudCBjYW4gY29udGFpbiBzY3JpcHRzLCB3ZSBoYXZlIHRvIGJlIGNhcmVmdWwgdGhhdFxyXG4vLyB3ZSBkb24ndCBhY2NpZGVudGFsbHkgcnVuIHRob3NlIHNjcmlwdCB3aXRoaW4gdGhlIHNhbWUgb3JpZ2luIGFzIHRoZVxyXG4vLyBjbGllbnQuIE90aGVyd2lzZSB0aG9zZSBzY3JpcHRzIHdyaXR0ZW4gYnkgcmVtb3RlIHVzZXJzIGNhbiByZWFkXHJcbi8vIHRoZSBhY2Nlc3MgdG9rZW4gYW5kIGVuZC10by1lbmQga2V5cyB0aGF0IGFyZSBpbiBsb2NhbCBzdG9yYWdlLlxyXG4vL1xyXG4vLyBGb3IgYXR0YWNobWVudHMgZG93bmxvYWRlZCBkaXJlY3RseSBmcm9tIHRoZSBob21lc2VydmVyIHdlIGNhbiB1c2VcclxuLy8gQ29udGVudC1TZWN1cml0eS1Qb2xpY3kgaGVhZGVycyB0byBkaXNhYmxlIHNjcmlwdCBleGVjdXRpb24uXHJcbi8vXHJcbi8vIEJ1dCBhdHRhY2htZW50cyB3aXRoIGVuZC10by1lbmQgZW5jcnlwdGlvbiBhcmUgbW9yZSBkaWZmaWN1bHQgdG8gaGFuZGxlLlxyXG4vLyBXZSBuZWVkIHRvIGRlY3J5cHQgdGhlIGF0dGFjaG1lbnQgb24gdGhlIGNsaWVudCBhbmQgdGhlbiBkaXNwbGF5IGl0LlxyXG4vLyBUbyBkaXNwbGF5IHRoZSBhdHRhY2htZW50IHdlIG5lZWQgdG8gdHVybiB0aGUgZGVjcnlwdGVkIGJ5dGVzIGludG8gYSBVUkwuXHJcbi8vXHJcbi8vIFRoZXJlIGFyZSB0d28gd2F5cyB0byB0dXJuIGJ5dGVzIGludG8gVVJMcywgZGF0YSBVUkwgYW5kIGJsb2IgVVJMcy5cclxuLy8gRGF0YSBVUkxzIGFyZW4ndCBzdWl0YWJsZSBmb3IgZG93bmxvYWRpbmcgYSBmaWxlIGJlY2F1c2UgQ2hyb21lIGhhcyBhXHJcbi8vIDJNQiBsaW1pdCBvbiB0aGUgc2l6ZSBvZiBVUkxzIHRoYXQgY2FuIGJlIHZpZXdlZCBpbiB0aGUgYnJvd3NlciBvclxyXG4vLyBkb3dubG9hZGVkLiBUaGlzIGxpbWl0IGRvZXMgbm90IHNlZW0gdG8gYXBwbHkgd2hlbiB0aGUgdXJsIGlzIHVzZWQgYXNcclxuLy8gdGhlIHNvdXJjZSBhdHRyaWJ1dGUgb2YgYW4gaW1hZ2UgdGFnLlxyXG4vL1xyXG4vLyBCbG9iIFVSTHMgYXJlIGdlbmVyYXRlZCB1c2luZyB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTCBhbmQgdW5mb3J0dW5hdGVseVxyXG4vLyBmb3Igb3VyIHB1cnBvc2VzIHRoZXkgaW5oZXJpdCB0aGUgb3JpZ2luIG9mIHRoZSBwYWdlIHRoYXQgY3JlYXRlZCB0aGVtLlxyXG4vLyBUaGlzIG1lYW5zIHRoYXQgYW55IHNjcmlwdHMgdGhhdCBydW4gd2hlbiB0aGUgVVJMIGlzIHZpZXdlZCB3aWxsIGJlIGFibGVcclxuLy8gdG8gYWNjZXNzIGxvY2FsIHN0b3JhZ2UuXHJcbi8vXHJcbi8vIFRoZSBlYXNpZXN0IHNvbHV0aW9uIGlzIHRvIGhvc3QgdGhlIGNvZGUgdGhhdCBnZW5lcmF0ZXMgdGhlIGJsb2IgVVJMIG9uXHJcbi8vIGEgZGlmZmVyZW50IGRvbWFpbiB0byB0aGUgY2xpZW50LlxyXG4vLyBBbm90aGVyIHBvc3NpYmlsaXR5IGlzIHRvIGdlbmVyYXRlIHRoZSBibG9iIFVSTCB3aXRoaW4gYSBzYW5kYm94ZWQgaWZyYW1lLlxyXG4vLyBUaGUgZG93bnNpZGUgb2YgdXNpbmcgYSBzZWNvbmQgZG9tYWluIGlzIHRoYXQgaXQgY29tcGxpY2F0ZXMgaG9zdGluZyxcclxuLy8gdGhlIGRvd25zaWRlIG9mIHVzaW5nIGEgc2FuZGJveGVkIGlmcmFtZSBpcyB0aGF0IHRoZSBicm93ZXJzIGFyZSBvdmVybHlcclxuLy8gcmVzdHJpY3RpdmUgaW4gd2hhdCB5b3UgYXJlIGFsbG93ZWQgdG8gZG8gd2l0aCB0aGUgZ2VuZXJhdGVkIFVSTC5cclxuXHJcbi8qKlxyXG4gKiBHZXQgdGhlIGN1cnJlbnQgQ1NTIHN0eWxlIGZvciBhIERPTUVsZW1lbnQuXHJcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGVsZW1lbnQgVGhlIGVsZW1lbnQgdG8gZ2V0IHRoZSBjdXJyZW50IHN0eWxlIG9mLlxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9IFRoZSBDU1Mgc3R5bGUgZW5jb2RlZCBhcyBhIHN0cmluZy5cclxuICovXHJcbmZ1bmN0aW9uIGNvbXB1dGVkU3R5bGUoZWxlbWVudCkge1xyXG4gICAgaWYgKCFlbGVtZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIFwiXCI7XHJcbiAgICB9XHJcbiAgICBjb25zdCBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQsIG51bGwpO1xyXG4gICAgbGV0IGNzc1RleHQgPSBzdHlsZS5jc3NUZXh0O1xyXG4gICAgaWYgKGNzc1RleHQgPT0gXCJcIikge1xyXG4gICAgICAgIC8vIEZpcmVmb3ggZG9lc24ndCBpbXBsZW1lbnQgXCIuY3NzVGV4dFwiIGZvciBjb21wdXRlZCBzdHlsZXMuXHJcbiAgICAgICAgLy8gaHR0cHM6Ly9idWd6aWxsYS5tb3ppbGxhLm9yZy9zaG93X2J1Zy5jZ2k/aWQ9MTM3Njg3XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdHlsZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjc3NUZXh0ICs9IHN0eWxlW2ldICsgXCI6XCI7XHJcbiAgICAgICAgICAgIGNzc1RleHQgKz0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShzdHlsZVtpXSkgKyBcIjtcIjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gY3NzVGV4dDtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ01GaWxlQm9keScsXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBkZWNyeXB0ZWRCbG9iOiAodGhpcy5wcm9wcy5kZWNyeXB0ZWRCbG9iID8gdGhpcy5wcm9wcy5kZWNyeXB0ZWRCbG9iIDogbnVsbCksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLyogdGhlIE1hdHJpeEV2ZW50IHRvIHNob3cgKi9cclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLyogYWxyZWFkeSBkZWNyeXB0ZWQgYmxvYiAqL1xyXG4gICAgICAgIGRlY3J5cHRlZEJsb2I6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICAgICAgLyogY2FsbGVkIHdoZW4gdGhlIGRvd25sb2FkIGxpbmsgaWZyYW1lIGlzIHNob3duICovXHJcbiAgICAgICAgb25IZWlnaHRDaGFuZ2VkOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICAvKiB0aGUgc2hhcGUgb2YgdGhlIHRpbGUsIHVzZWQgKi9cclxuICAgICAgICB0aWxlU2hhcGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRXh0cmFjdHMgYSBodW1hbiByZWFkYWJsZSBsYWJlbCBmb3IgdGhlIGZpbGUgYXR0YWNobWVudCB0byB1c2UgYXNcclxuICAgICAqIGxpbmsgdGV4dC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW1zIHtPYmplY3R9IGNvbnRlbnQgVGhlIFwiY29udGVudFwiIGtleSBvZiB0aGUgbWF0cml4IGV2ZW50LlxyXG4gICAgICogQHJldHVybiB7c3RyaW5nfSB0aGUgaHVtYW4gcmVhZGFibGUgbGluayB0ZXh0IGZvciB0aGUgYXR0YWNobWVudC5cclxuICAgICAqL1xyXG4gICAgcHJlc2VudGFibGVUZXh0Rm9yRmlsZTogZnVuY3Rpb24oY29udGVudCkge1xyXG4gICAgICAgIGxldCBsaW5rVGV4dCA9IF90KFwiQXR0YWNobWVudFwiKTtcclxuICAgICAgICBpZiAoY29udGVudC5ib2R5ICYmIGNvbnRlbnQuYm9keS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIC8vIFRoZSBjb250ZW50IGJvZHkgc2hvdWxkIGJlIHRoZSBuYW1lIG9mIHRoZSBmaWxlIGluY2x1ZGluZyBhXHJcbiAgICAgICAgICAgIC8vIGZpbGUgZXh0ZW5zaW9uLlxyXG4gICAgICAgICAgICBsaW5rVGV4dCA9IGNvbnRlbnQuYm9keTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjb250ZW50LmluZm8gJiYgY29udGVudC5pbmZvLnNpemUpIHtcclxuICAgICAgICAgICAgLy8gSWYgd2Uga25vdyB0aGUgc2l6ZSBvZiB0aGUgZmlsZSB0aGVuIGFkZCBpdCBhcyBodW1hbiByZWFkYWJsZVxyXG4gICAgICAgICAgICAvLyBzdHJpbmcgdG8gdGhlIGVuZCBvZiB0aGUgbGluayB0ZXh0IHNvIHRoYXQgdGhlIHVzZXIga25vd3MgaG93XHJcbiAgICAgICAgICAgIC8vIGJpZyBhIGZpbGUgdGhleSBhcmUgZG93bmxvYWRpbmcuXHJcbiAgICAgICAgICAgIC8vIFRoZSBjb250ZW50LmluZm8gYWxzbyBjb250YWlucyBhIE1JTUUtdHlwZSBidXQgd2UgZG9uJ3QgZGlzcGxheVxyXG4gICAgICAgICAgICAvLyBpdCBzaW5jZSBpdCBpcyBcInVnbHlcIiwgdXNlcnMgZ2VuZXJhbGx5IGFyZW4ndCBhd2FyZSB3aGF0IGl0XHJcbiAgICAgICAgICAgIC8vIG1lYW5zIGFuZCB0aGUgdHlwZSBvZiB0aGUgYXR0YWNobWVudCBjYW4gdXN1YWxseSBiZSBpbmZlcnJlcmVkXHJcbiAgICAgICAgICAgIC8vIGZyb20gdGhlIGZpbGUgZXh0ZW5zaW9uLlxyXG4gICAgICAgICAgICBsaW5rVGV4dCArPSAnICgnICsgZmlsZXNpemUoY29udGVudC5pbmZvLnNpemUpICsgJyknO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbGlua1RleHQ7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRDb250ZW50VXJsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLm14Y1VybFRvSHR0cChjb250ZW50LnVybCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIGNvbXBvbmVudCB3aXRoIHJlYWwgY2xhc3MsIHVzZSBjb25zdHJ1Y3RvciBmb3IgcmVmc1xyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5faWZyYW1lID0gY3JlYXRlUmVmKCk7XHJcbiAgICAgICAgdGhpcy5fZHVtbXlMaW5rID0gY3JlYXRlUmVmKCk7XHJcbiAgICAgICAgdGhpcy5fZG93bmxvYWRJbWFnZSA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gQWRkIHRoaXMgdG8gdGhlIGxpc3Qgb2YgbW91bnRlZCBjb21wb25lbnRzIHRvIHJlY2VpdmUgbm90aWZpY2F0aW9uc1xyXG4gICAgICAgIC8vIHdoZW4gdGhlIHRpbnQgY2hhbmdlcy5cclxuICAgICAgICB0aGlzLmlkID0gbmV4dE1vdW50SWQrKztcclxuICAgICAgICBtb3VudHNbdGhpcy5pZF0gPSB0aGlzO1xyXG4gICAgICAgIHRoaXMudGludCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRVcGRhdGU6IGZ1bmN0aW9uKHByZXZQcm9wcywgcHJldlN0YXRlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkICYmICFwcmV2U3RhdGUuZGVjcnlwdGVkQmxvYiAmJiB0aGlzLnN0YXRlLmRlY3J5cHRlZEJsb2IpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBSZW1vdmUgdGhpcyBmcm9tIHRoZSBsaXN0IG9mIG1vdW50ZWQgY29tcG9uZW50c1xyXG4gICAgICAgIGRlbGV0ZSBtb3VudHNbdGhpcy5pZF07XHJcbiAgICB9LFxyXG5cclxuICAgIHRpbnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIFVwZGF0ZSBvdXIgdGludGVkIGNvcHkgb2YgcmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZG93bmxvYWQuc3ZnXCIpXHJcbiAgICAgICAgaWYgKHRoaXMuX2Rvd25sb2FkSW1hZ2UuY3VycmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLl9kb3dubG9hZEltYWdlLmN1cnJlbnQuc3JjID0gdGludGVkRG93bmxvYWRJbWFnZVVSTDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX2lmcmFtZS5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIC8vIElmIHRoZSBhdHRhY2htZW50IGlzIGVuY3J5cHRlZCB0aGVuIHRoZSBkb3dubG9hZCBpbWFnZVxyXG4gICAgICAgICAgICAvLyB3aWxsIGJlIGluc2lkZSB0aGUgaWZyYW1lIHNvIHdlIHdvbnQgYmUgYWJsZSB0byB1cGRhdGVcclxuICAgICAgICAgICAgLy8gaXQgZGlyZWN0bHkuXHJcbiAgICAgICAgICAgIHRoaXMuX2lmcmFtZS5jdXJyZW50LmNvbnRlbnRXaW5kb3cucG9zdE1lc3NhZ2Uoe1xyXG4gICAgICAgICAgICAgICAgaW1nU3JjOiB0aW50ZWREb3dubG9hZEltYWdlVVJMLFxyXG4gICAgICAgICAgICAgICAgc3R5bGU6IGNvbXB1dGVkU3R5bGUodGhpcy5fZHVtbXlMaW5rLmN1cnJlbnQpLFxyXG4gICAgICAgICAgICB9LCBcIipcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgIGNvbnN0IHRleHQgPSB0aGlzLnByZXNlbnRhYmxlVGV4dEZvckZpbGUoY29udGVudCk7XHJcbiAgICAgICAgY29uc3QgaXNFbmNyeXB0ZWQgPSBjb250ZW50LmZpbGUgIT09IHVuZGVmaW5lZDtcclxuICAgICAgICBjb25zdCBmaWxlTmFtZSA9IGNvbnRlbnQuYm9keSAmJiBjb250ZW50LmJvZHkubGVuZ3RoID4gMCA/IGNvbnRlbnQuYm9keSA6IF90KFwiQXR0YWNobWVudFwiKTtcclxuICAgICAgICBjb25zdCBjb250ZW50VXJsID0gdGhpcy5fZ2V0Q29udGVudFVybCgpO1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3QgZmlsZVNpemUgPSBjb250ZW50LmluZm8gPyBjb250ZW50LmluZm8uc2l6ZSA6IG51bGw7XHJcbiAgICAgICAgY29uc3QgZmlsZVR5cGUgPSBjb250ZW50LmluZm8gPyBjb250ZW50LmluZm8ubWltZXR5cGUgOiBcImFwcGxpY2F0aW9uL29jdGV0LXN0cmVhbVwiO1xyXG5cclxuICAgICAgICBpZiAoaXNFbmNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGVjcnlwdGVkQmxvYiA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTmVlZCB0byBkZWNyeXB0IHRoZSBhdHRhY2htZW50XHJcbiAgICAgICAgICAgICAgICAvLyBXYWl0IGZvciB0aGUgdXNlciB0byBjbGljayBvbiB0aGUgbGluayBiZWZvcmUgZG93bmxvYWRpbmdcclxuICAgICAgICAgICAgICAgIC8vIGFuZCBkZWNyeXB0aW5nIHRoZSBhdHRhY2htZW50LlxyXG4gICAgICAgICAgICAgICAgbGV0IGRlY3J5cHRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlY3J5cHQgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZWNyeXB0aW5nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgZGVjcnlwdGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVjcnlwdEZpbGUoY29udGVudC5maWxlKS50aGVuKChibG9iKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVjcnlwdGVkQmxvYjogYmxvYixcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXCJVbmFibGUgdG8gZGVjcnlwdCBhdHRhY2htZW50OiBcIiwgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRXJyb3IgZGVjcnlwdGluZyBhdHRhY2htZW50JywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJFcnJvclwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIkVycm9yIGRlY3J5cHRpbmcgYXR0YWNobWVudFwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY3J5cHRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBidXR0b24gc2hvdWxkIGFjdHVhbGx5IERvd25sb2FkIGJlY2F1c2UgdXNlcmNvbnRlbnQvIHdpbGwgdHJ5IHRvIGNsaWNrIGl0c2VsZlxyXG4gICAgICAgICAgICAgICAgLy8gYnV0IGl0IGlzIG5vdCBndWFyYW50ZWVkIGJldHdlZW4gdmFyaW91cyBicm93c2Vycycgc2V0dGluZ3MuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01GaWxlQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01GaWxlQm9keV9kb3dubG9hZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17ZGVjcnlwdH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIkRlY3J5cHQgJSh0ZXh0KXNcIiwgeyB0ZXh0OiB0ZXh0IH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gV2hlbiB0aGUgaWZyYW1lIGxvYWRzIHdlIHRlbGwgaXQgdG8gcmVuZGVyIGEgZG93bmxvYWQgbGlua1xyXG4gICAgICAgICAgICBjb25zdCBvbklmcmFtZUxvYWQgPSAoZXYpID0+IHtcclxuICAgICAgICAgICAgICAgIGV2LnRhcmdldC5jb250ZW50V2luZG93LnBvc3RNZXNzYWdlKHtcclxuICAgICAgICAgICAgICAgICAgICBpbWdTcmM6IHRpbnRlZERvd25sb2FkSW1hZ2VVUkwsXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU6IGNvbXB1dGVkU3R5bGUodGhpcy5fZHVtbXlMaW5rLmN1cnJlbnQpLFxyXG4gICAgICAgICAgICAgICAgICAgIGJsb2I6IHRoaXMuc3RhdGUuZGVjcnlwdGVkQmxvYixcclxuICAgICAgICAgICAgICAgICAgICAvLyBTZXQgYSBkb3dubG9hZCBhdHRyaWJ1dGUgZm9yIGVuY3J5cHRlZCBmaWxlcyBzbyB0aGF0IHRoZSBmaWxlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2lsbCBoYXZlIHRoZSBjb3JyZWN0IG5hbWUgd2hlbiB0aGUgdXNlciB0cmllcyB0byBkb3dubG9hZCBpdC5cclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSBjYW4ndCBwcm92aWRlIGEgQ29udGVudC1EaXNwb3NpdGlvbiBoZWFkZXIgbGlrZSB3ZSB3b3VsZCBmb3IgSFRUUC5cclxuICAgICAgICAgICAgICAgICAgICBkb3dubG9hZDogZmlsZU5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dENvbnRlbnQ6IF90KFwiRG93bmxvYWQgJSh0ZXh0KXNcIiwgeyB0ZXh0OiB0ZXh0IH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgYXV0by1kb3dubG9hZCBpZiBhIHVzZXIgdHJpZ2dlcmVkIHRoaXMgaWZyYW1lIGV4cGxpY2l0bHlcclxuICAgICAgICAgICAgICAgICAgICBhdXRvOiAhdGhpcy5wcm9wcy5kZWNyeXB0ZWRCbG9iLFxyXG4gICAgICAgICAgICAgICAgfSwgXCIqXCIpO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJ1c2VyY29udGVudC9cIjsgLy8gWFhYOiB0aGlzIHBhdGggc2hvdWxkIHByb2JhYmx5IGJlIHBhc3NlZCBmcm9tIHRoZSBza2luXHJcblxyXG4gICAgICAgICAgICAvLyBJZiB0aGUgYXR0YWNobWVudCBpcyBlbmNyeXB0ZWQgdGhlbiBwdXQgdGhlIGxpbmsgaW5zaWRlIGFuIGlmcmFtZS5cclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01GaWxlQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTUZpbGVCb2R5X2Rvd25sb2FkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3tkaXNwbGF5OiBcIm5vbmVcIn19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyAvKlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqIEFkZCBkdW1teSBjb3B5IG9mIHRoZSBcImFcIiB0YWdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKiBXZSdsbCB1c2UgaXQgdG8gbGVhcm4gaG93IHRoZSBkb3dubG9hZCBsaW5rXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICogd291bGQgaGF2ZSBiZWVuIHN0eWxlZCBpZiBpdCB3YXMgcmVuZGVyZWQgaW5saW5lLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSByZWY9e3RoaXMuX2R1bW15TGlua30gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpZnJhbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYz17YCR7dXJsfT9vcmlnaW49JHtlbmNvZGVVUklDb21wb25lbnQod2luZG93LmxvY2F0aW9uLm9yaWdpbil9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uTG9hZD17b25JZnJhbWVMb2FkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9pZnJhbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYW5kYm94PVwiYWxsb3ctc2NyaXB0cyBhbGxvdy1kb3dubG9hZHMgYWxsb3ctZG93bmxvYWRzLXdpdGhvdXQtdXNlci1hY3RpdmF0aW9uXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRlbnRVcmwpIHtcclxuICAgICAgICAgICAgY29uc3QgZG93bmxvYWRQcm9wcyA9IHtcclxuICAgICAgICAgICAgICAgIHRhcmdldDogXCJfYmxhbmtcIixcclxuICAgICAgICAgICAgICAgIHJlbDogXCJub3JlZmVycmVyIG5vb3BlbmVyXCIsXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2Ugc2V0IHRoZSBocmVmIHJlZ2FyZGxlc3Mgb2Ygd2hldGhlciBvciBub3Qgd2UgaW50ZXJjZXB0IHRoZSBkb3dubG9hZFxyXG4gICAgICAgICAgICAgICAgLy8gYmVjYXVzZSB3ZSBkb24ndCByZWFsbHkgd2FudCB0byBjb252ZXJ0IHRoZSBmaWxlIHRvIGEgYmxvYiBlYWdlcmx5LCBhbmRcclxuICAgICAgICAgICAgICAgIC8vIHN0aWxsIHdhbnQgXCJvcGVuIGluIG5ldyB0YWJcIiBhbmQgXCJzYXZlIGxpbmsgYXNcIiB0byB3b3JrLlxyXG4gICAgICAgICAgICAgICAgaHJlZjogY29udGVudFVybCxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIC8vIEJsb2JzIGNhbiBvbmx5IGhhdmUgdXAgdG8gNTAwbWIsIHNvIGlmIHRoZSBmaWxlIHJlcG9ydHMgYXMgYmVpbmcgdG9vIGxhcmdlIHRoZW5cclxuICAgICAgICAgICAgLy8gd2Ugd29uJ3QgdHJ5IGFuZCBjb252ZXJ0IGl0LiBMaWtld2lzZSwgaWYgdGhlIGZpbGUgc2l6ZSBpcyB1bmtub3duIHRoZW4gd2UnbGwgYXNzdW1lXHJcbiAgICAgICAgICAgIC8vIGl0IGlzIHRvbyBiaWcuIFRoZXJlIGlzIHRoZSByaXNrIG9mIHRoZSByZXBvcnRlZCBmaWxlIHNpemUgYW5kIHRoZSBhY3R1YWwgZmlsZSBzaXplXHJcbiAgICAgICAgICAgIC8vIGJlaW5nIGRpZmZlcmVudCwgaG93ZXZlciB0aGUgdXNlciBzaG91bGRuJ3Qgbm9ybWFsbHkgcnVuIGludG8gdGhpcyBwcm9ibGVtLlxyXG4gICAgICAgICAgICBjb25zdCBmaWxlVG9vQmlnID0gdHlwZW9mKGZpbGVTaXplKSA9PT0gJ251bWJlcicgPyBmaWxlU2l6ZSA+IDUyNDI4ODAwMCA6IHRydWU7XHJcblxyXG4gICAgICAgICAgICBpZiAoW1wiYXBwbGljYXRpb24vcGRmXCJdLmluY2x1ZGVzKGZpbGVUeXBlKSAmJiAhZmlsZVRvb0JpZykge1xyXG4gICAgICAgICAgICAgICAgLy8gV2Ugd2FudCB0byBmb3JjZSBhIGRvd25sb2FkIG9uIHRoaXMgdHlwZSwgc28gdXNlIGFuIG9uQ2xpY2sgaGFuZGxlci5cclxuICAgICAgICAgICAgICAgIGRvd25sb2FkUHJvcHNbXCJvbkNsaWNrXCJdID0gKGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgRG93bmxvYWRpbmcgJHtmaWxlVHlwZX0gYXMgYmxvYiAodW5lbmNyeXB0ZWQpYCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIEF2b2lkIGxldHRpbmcgdGhlIDxhPiBkbyBpdHMgdGhpbmdcclxuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gU3RhcnQgYSBmZXRjaCBmb3IgdGhlIGRvd25sb2FkXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQmFzZWQgdXBvbiBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvNDk1MDA0NjVcclxuICAgICAgICAgICAgICAgICAgICBmZXRjaChjb250ZW50VXJsKS50aGVuKChyZXNwb25zZSkgPT4gcmVzcG9uc2UuYmxvYigpKS50aGVuKChibG9iKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGJsb2JVcmwgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGJsb2IpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2UgaGF2ZSB0byBjcmVhdGUgYW4gYW5jaG9yIHRvIGRvd25sb2FkIHRoZSBmaWxlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBBbmNob3IgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBBbmNob3IuZG93bmxvYWQgPSBmaWxlTmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcEFuY2hvci5ocmVmID0gYmxvYlVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0ZW1wQW5jaG9yKTsgLy8gZm9yIGZpcmVmb3g6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zMjIyNjA2OFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQW5jaG9yLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBBbmNob3IucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gRWxzZSB3ZSBhcmUgaG9waW5nIHRoZSBicm93c2VyIHdpbGwgZG8gdGhlIHJpZ2h0IHRoaW5nXHJcbiAgICAgICAgICAgICAgICBkb3dubG9hZFByb3BzW1wiZG93bmxvYWRcIl0gPSBmaWxlTmFtZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gSWYgdGhlIGF0dGFjaG1lbnQgaXMgbm90IGVuY3J5cHRlZCB0aGVuIHdlIGNoZWNrIHdoZXRoZXIgd2VcclxuICAgICAgICAgICAgLy8gYXJlIGJlaW5nIGRpc3BsYXllZCBpbiB0aGUgcm9vbSB0aW1lbGluZSBvciBpbiBhIGxpc3Qgb2ZcclxuICAgICAgICAgICAgLy8gZmlsZXMgaW4gdGhlIHJpZ2h0IGhhbmQgc2lkZSBvZiB0aGUgc2NyZWVuLlxyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy50aWxlU2hhcGUgPT09IFwiZmlsZV9ncmlkXCIpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfTUZpbGVCb2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTUZpbGVCb2R5X2Rvd25sb2FkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJteF9NRmlsZUJvZHlfZG93bmxvYWRMaW5rXCIgey4uLmRvd25sb2FkUHJvcHN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgZmlsZU5hbWUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NSW1hZ2VCb2R5X3NpemVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGNvbnRlbnQuaW5mbyAmJiBjb250ZW50LmluZm8uc2l6ZSA/IGZpbGVzaXplKGNvbnRlbnQuaW5mby5zaXplKSA6IFwiXCIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01GaWxlQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01GaWxlQm9keV9kb3dubG9hZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgey4uLmRvd25sb2FkUHJvcHN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXt0aW50ZWREb3dubG9hZEltYWdlVVJMfSB3aWR0aD1cIjEyXCIgaGVpZ2h0PVwiMTRcIiByZWY9e3RoaXMuX2Rvd25sb2FkSW1hZ2V9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIkRvd25sb2FkICUodGV4dClzXCIsIHsgdGV4dDogdGV4dCB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBleHRyYSA9IHRleHQgPyAoJzogJyArIHRleHQpIDogJyc7XHJcbiAgICAgICAgICAgIHJldHVybiA8c3BhbiBjbGFzc05hbWU9XCJteF9NRmlsZUJvZHlcIj5cclxuICAgICAgICAgICAgICAgIHsgX3QoXCJJbnZhbGlkIGZpbGUlKGV4dHJhKXNcIiwgeyBleHRyYTogZXh0cmEgfSkgfVxyXG4gICAgICAgICAgICA8L3NwYW4+O1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=