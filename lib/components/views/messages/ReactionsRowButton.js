"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _FormattingUtils = require("../../../utils/FormattingUtils");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class ReactionsRowButton extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onClick", ev => {
      const {
        mxEvent,
        myReactionEvent,
        content
      } = this.props;

      if (myReactionEvent) {
        _MatrixClientPeg.MatrixClientPeg.get().redactEvent(mxEvent.getRoomId(), myReactionEvent.getId());
      } else {
        _MatrixClientPeg.MatrixClientPeg.get().sendEvent(mxEvent.getRoomId(), "m.reaction", {
          "m.relates_to": {
            "rel_type": "m.annotation",
            "event_id": mxEvent.getId(),
            "key": content
          }
        });
      }
    });
    (0, _defineProperty2.default)(this, "onMouseOver", () => {
      this.setState({
        // To avoid littering the DOM with a tooltip for every reaction,
        // only render it on first use.
        tooltipRendered: true,
        tooltipVisible: true
      });
    });
    (0, _defineProperty2.default)(this, "onMouseOut", () => {
      this.setState({
        tooltipVisible: false
      });
    });
    this.state = {
      tooltipVisible: false
    };
  }

  render() {
    const ReactionsRowButtonTooltip = sdk.getComponent('messages.ReactionsRowButtonTooltip');
    const {
      mxEvent,
      content,
      count,
      reactionEvents,
      myReactionEvent
    } = this.props;
    const classes = (0, _classnames.default)({
      mx_ReactionsRowButton: true,
      mx_ReactionsRowButton_selected: !!myReactionEvent
    });
    let tooltip;

    if (this.state.tooltipRendered) {
      tooltip = _react.default.createElement(ReactionsRowButtonTooltip, {
        mxEvent: this.props.mxEvent,
        content: content,
        reactionEvents: reactionEvents,
        visible: this.state.tooltipVisible
      });
    }

    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(mxEvent.getRoomId());

    let label;

    if (room) {
      const senders = [];

      for (const reactionEvent of reactionEvents) {
        const member = room.getMember(reactionEvent.getSender());
        const name = member ? member.name : reactionEvent.getSender();
        senders.push(name);
      }

      label = (0, _languageHandler._t)("<reactors/><reactedWith> reacted with %(content)s</reactedWith>", {
        content
      }, {
        reactors: () => {
          return (0, _FormattingUtils.formatCommaSeparatedList)(senders, 6);
        },
        reactedWith: sub => {
          if (!content) {
            return null;
          }

          return sub;
        }
      });
    }

    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement(AccessibleButton, {
      className: classes,
      "aria-label": label,
      onClick: this.onClick,
      onMouseOver: this.onMouseOver,
      onMouseOut: this.onMouseOut
    }, _react.default.createElement("span", {
      className: "mx_ReactionsRowButton_content",
      "aria-hidden": "true"
    }, content), _react.default.createElement("span", {
      className: "mx_ReactionsRowButton_count",
      "aria-hidden": "true"
    }, count), tooltip);
  }

}

exports.default = ReactionsRowButton;
(0, _defineProperty2.default)(ReactionsRowButton, "propTypes", {
  // The event we're displaying reactions for
  mxEvent: _propTypes.default.object.isRequired,
  // The reaction content / key / emoji
  content: _propTypes.default.string.isRequired,
  // The count of votes for this key
  count: _propTypes.default.number.isRequired,
  // A Set of Martix reaction events for this key
  reactionEvents: _propTypes.default.object.isRequired,
  // A possible Matrix event if the current user has voted for this type
  myReactionEvent: _propTypes.default.object
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL1JlYWN0aW9uc1Jvd0J1dHRvbi5qcyJdLCJuYW1lcyI6WyJSZWFjdGlvbnNSb3dCdXR0b24iLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiZXYiLCJteEV2ZW50IiwibXlSZWFjdGlvbkV2ZW50IiwiY29udGVudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInJlZGFjdEV2ZW50IiwiZ2V0Um9vbUlkIiwiZ2V0SWQiLCJzZW5kRXZlbnQiLCJzZXRTdGF0ZSIsInRvb2x0aXBSZW5kZXJlZCIsInRvb2x0aXBWaXNpYmxlIiwic3RhdGUiLCJyZW5kZXIiLCJSZWFjdGlvbnNSb3dCdXR0b25Ub29sdGlwIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY291bnQiLCJyZWFjdGlvbkV2ZW50cyIsImNsYXNzZXMiLCJteF9SZWFjdGlvbnNSb3dCdXR0b24iLCJteF9SZWFjdGlvbnNSb3dCdXR0b25fc2VsZWN0ZWQiLCJ0b29sdGlwIiwicm9vbSIsImdldFJvb20iLCJsYWJlbCIsInNlbmRlcnMiLCJyZWFjdGlvbkV2ZW50IiwibWVtYmVyIiwiZ2V0TWVtYmVyIiwiZ2V0U2VuZGVyIiwibmFtZSIsInB1c2giLCJyZWFjdG9ycyIsInJlYWN0ZWRXaXRoIiwic3ViIiwiQWNjZXNzaWJsZUJ1dHRvbiIsIm9uQ2xpY2siLCJvbk1vdXNlT3ZlciIsIm9uTW91c2VPdXQiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7O0FBeUJlLE1BQU1BLGtCQUFOLFNBQWlDQyxlQUFNQyxhQUF2QyxDQUFxRDtBQWNoRUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsbURBUVJDLEVBQUQsSUFBUTtBQUNkLFlBQU07QUFBRUMsUUFBQUEsT0FBRjtBQUFXQyxRQUFBQSxlQUFYO0FBQTRCQyxRQUFBQTtBQUE1QixVQUF3QyxLQUFLSixLQUFuRDs7QUFDQSxVQUFJRyxlQUFKLEVBQXFCO0FBQ2pCRSx5Q0FBZ0JDLEdBQWhCLEdBQXNCQyxXQUF0QixDQUNJTCxPQUFPLENBQUNNLFNBQVIsRUFESixFQUVJTCxlQUFlLENBQUNNLEtBQWhCLEVBRko7QUFJSCxPQUxELE1BS087QUFDSEoseUNBQWdCQyxHQUFoQixHQUFzQkksU0FBdEIsQ0FBZ0NSLE9BQU8sQ0FBQ00sU0FBUixFQUFoQyxFQUFxRCxZQUFyRCxFQUFtRTtBQUMvRCwwQkFBZ0I7QUFDWix3QkFBWSxjQURBO0FBRVosd0JBQVlOLE9BQU8sQ0FBQ08sS0FBUixFQUZBO0FBR1osbUJBQU9MO0FBSEs7QUFEK0MsU0FBbkU7QUFPSDtBQUNKLEtBeEJrQjtBQUFBLHVEQTBCTCxNQUFNO0FBQ2hCLFdBQUtPLFFBQUwsQ0FBYztBQUNWO0FBQ0E7QUFDQUMsUUFBQUEsZUFBZSxFQUFFLElBSFA7QUFJVkMsUUFBQUEsY0FBYyxFQUFFO0FBSk4sT0FBZDtBQU1ILEtBakNrQjtBQUFBLHNEQW1DTixNQUFNO0FBQ2YsV0FBS0YsUUFBTCxDQUFjO0FBQ1ZFLFFBQUFBLGNBQWMsRUFBRTtBQUROLE9BQWQ7QUFHSCxLQXZDa0I7QUFHZixTQUFLQyxLQUFMLEdBQWE7QUFDVEQsTUFBQUEsY0FBYyxFQUFFO0FBRFAsS0FBYjtBQUdIOztBQW1DREUsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMseUJBQXlCLEdBQzNCQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsb0NBQWpCLENBREo7QUFFQSxVQUFNO0FBQUVoQixNQUFBQSxPQUFGO0FBQVdFLE1BQUFBLE9BQVg7QUFBb0JlLE1BQUFBLEtBQXBCO0FBQTJCQyxNQUFBQSxjQUEzQjtBQUEyQ2pCLE1BQUFBO0FBQTNDLFFBQStELEtBQUtILEtBQTFFO0FBRUEsVUFBTXFCLE9BQU8sR0FBRyx5QkFBVztBQUN2QkMsTUFBQUEscUJBQXFCLEVBQUUsSUFEQTtBQUV2QkMsTUFBQUEsOEJBQThCLEVBQUUsQ0FBQyxDQUFDcEI7QUFGWCxLQUFYLENBQWhCO0FBS0EsUUFBSXFCLE9BQUo7O0FBQ0EsUUFBSSxLQUFLVixLQUFMLENBQVdGLGVBQWYsRUFBZ0M7QUFDNUJZLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyx5QkFBRDtBQUNOLFFBQUEsT0FBTyxFQUFFLEtBQUt4QixLQUFMLENBQVdFLE9BRGQ7QUFFTixRQUFBLE9BQU8sRUFBRUUsT0FGSDtBQUdOLFFBQUEsY0FBYyxFQUFFZ0IsY0FIVjtBQUlOLFFBQUEsT0FBTyxFQUFFLEtBQUtOLEtBQUwsQ0FBV0Q7QUFKZCxRQUFWO0FBTUg7O0FBRUQsVUFBTVksSUFBSSxHQUFHcEIsaUNBQWdCQyxHQUFoQixHQUFzQm9CLE9BQXRCLENBQThCeEIsT0FBTyxDQUFDTSxTQUFSLEVBQTlCLENBQWI7O0FBQ0EsUUFBSW1CLEtBQUo7O0FBQ0EsUUFBSUYsSUFBSixFQUFVO0FBQ04sWUFBTUcsT0FBTyxHQUFHLEVBQWhCOztBQUNBLFdBQUssTUFBTUMsYUFBWCxJQUE0QlQsY0FBNUIsRUFBNEM7QUFDeEMsY0FBTVUsTUFBTSxHQUFHTCxJQUFJLENBQUNNLFNBQUwsQ0FBZUYsYUFBYSxDQUFDRyxTQUFkLEVBQWYsQ0FBZjtBQUNBLGNBQU1DLElBQUksR0FBR0gsTUFBTSxHQUFHQSxNQUFNLENBQUNHLElBQVYsR0FBaUJKLGFBQWEsQ0FBQ0csU0FBZCxFQUFwQztBQUNBSixRQUFBQSxPQUFPLENBQUNNLElBQVIsQ0FBYUQsSUFBYjtBQUNIOztBQUNETixNQUFBQSxLQUFLLEdBQUcseUJBQ0osaUVBREksRUFFSjtBQUNJdkIsUUFBQUE7QUFESixPQUZJLEVBS0o7QUFDSStCLFFBQUFBLFFBQVEsRUFBRSxNQUFNO0FBQ1osaUJBQU8sK0NBQXlCUCxPQUF6QixFQUFrQyxDQUFsQyxDQUFQO0FBQ0gsU0FITDtBQUlJUSxRQUFBQSxXQUFXLEVBQUdDLEdBQUQsSUFBUztBQUNsQixjQUFJLENBQUNqQyxPQUFMLEVBQWM7QUFDVixtQkFBTyxJQUFQO0FBQ0g7O0FBQ0QsaUJBQU9pQyxHQUFQO0FBQ0g7QUFUTCxPQUxJLENBQVI7QUFpQkg7O0FBRUQsVUFBTUMsZ0JBQWdCLEdBQUdyQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsV0FBTyw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBRUcsT0FBN0I7QUFDSCxvQkFBWU0sS0FEVDtBQUVILE1BQUEsT0FBTyxFQUFFLEtBQUtZLE9BRlg7QUFHSCxNQUFBLFdBQVcsRUFBRSxLQUFLQyxXQUhmO0FBSUgsTUFBQSxVQUFVLEVBQUUsS0FBS0M7QUFKZCxPQU1IO0FBQU0sTUFBQSxTQUFTLEVBQUMsK0JBQWhCO0FBQWdELHFCQUFZO0FBQTVELE9BQ0tyQyxPQURMLENBTkcsRUFTSDtBQUFNLE1BQUEsU0FBUyxFQUFDLDZCQUFoQjtBQUE4QyxxQkFBWTtBQUExRCxPQUNLZSxLQURMLENBVEcsRUFZRkssT0FaRSxDQUFQO0FBY0g7O0FBdEgrRDs7OzhCQUEvQzVCLGtCLGVBQ0U7QUFDZjtBQUNBTSxFQUFBQSxPQUFPLEVBQUV3QyxtQkFBVUMsTUFBVixDQUFpQkMsVUFGWDtBQUdmO0FBQ0F4QyxFQUFBQSxPQUFPLEVBQUVzQyxtQkFBVUcsTUFBVixDQUFpQkQsVUFKWDtBQUtmO0FBQ0F6QixFQUFBQSxLQUFLLEVBQUV1QixtQkFBVUksTUFBVixDQUFpQkYsVUFOVDtBQU9mO0FBQ0F4QixFQUFBQSxjQUFjLEVBQUVzQixtQkFBVUMsTUFBVixDQUFpQkMsVUFSbEI7QUFTZjtBQUNBekMsRUFBQUEsZUFBZSxFQUFFdUMsbUJBQVVDO0FBVlosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgeyBmb3JtYXRDb21tYVNlcGFyYXRlZExpc3QgfSBmcm9tICcuLi8uLi8uLi91dGlscy9Gb3JtYXR0aW5nVXRpbHMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmVhY3Rpb25zUm93QnV0dG9uIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIFRoZSBldmVudCB3ZSdyZSBkaXNwbGF5aW5nIHJlYWN0aW9ucyBmb3JcclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLy8gVGhlIHJlYWN0aW9uIGNvbnRlbnQgLyBrZXkgLyBlbW9qaVxyXG4gICAgICAgIGNvbnRlbnQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBUaGUgY291bnQgb2Ygdm90ZXMgZm9yIHRoaXMga2V5XHJcbiAgICAgICAgY291bnQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBBIFNldCBvZiBNYXJ0aXggcmVhY3Rpb24gZXZlbnRzIGZvciB0aGlzIGtleVxyXG4gICAgICAgIHJlYWN0aW9uRXZlbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLy8gQSBwb3NzaWJsZSBNYXRyaXggZXZlbnQgaWYgdGhlIGN1cnJlbnQgdXNlciBoYXMgdm90ZWQgZm9yIHRoaXMgdHlwZVxyXG4gICAgICAgIG15UmVhY3Rpb25FdmVudDogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgdG9vbHRpcFZpc2libGU6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGljayA9IChldikgPT4ge1xyXG4gICAgICAgIGNvbnN0IHsgbXhFdmVudCwgbXlSZWFjdGlvbkV2ZW50LCBjb250ZW50IH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGlmIChteVJlYWN0aW9uRXZlbnQpIHtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlZGFjdEV2ZW50KFxyXG4gICAgICAgICAgICAgICAgbXhFdmVudC5nZXRSb29tSWQoKSxcclxuICAgICAgICAgICAgICAgIG15UmVhY3Rpb25FdmVudC5nZXRJZCgpLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZW5kRXZlbnQobXhFdmVudC5nZXRSb29tSWQoKSwgXCJtLnJlYWN0aW9uXCIsIHtcclxuICAgICAgICAgICAgICAgIFwibS5yZWxhdGVzX3RvXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICBcInJlbF90eXBlXCI6IFwibS5hbm5vdGF0aW9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJldmVudF9pZFwiOiBteEV2ZW50LmdldElkKCksXHJcbiAgICAgICAgICAgICAgICAgICAgXCJrZXlcIjogY29udGVudCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25Nb3VzZU92ZXIgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIC8vIFRvIGF2b2lkIGxpdHRlcmluZyB0aGUgRE9NIHdpdGggYSB0b29sdGlwIGZvciBldmVyeSByZWFjdGlvbixcclxuICAgICAgICAgICAgLy8gb25seSByZW5kZXIgaXQgb24gZmlyc3QgdXNlLlxyXG4gICAgICAgICAgICB0b29sdGlwUmVuZGVyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHRvb2x0aXBWaXNpYmxlOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VPdXQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHRvb2x0aXBWaXNpYmxlOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgUmVhY3Rpb25zUm93QnV0dG9uVG9vbHRpcCA9XHJcbiAgICAgICAgICAgIHNkay5nZXRDb21wb25lbnQoJ21lc3NhZ2VzLlJlYWN0aW9uc1Jvd0J1dHRvblRvb2x0aXAnKTtcclxuICAgICAgICBjb25zdCB7IG14RXZlbnQsIGNvbnRlbnQsIGNvdW50LCByZWFjdGlvbkV2ZW50cywgbXlSZWFjdGlvbkV2ZW50IH0gPSB0aGlzLnByb3BzO1xyXG5cclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgIG14X1JlYWN0aW9uc1Jvd0J1dHRvbjogdHJ1ZSxcclxuICAgICAgICAgICAgbXhfUmVhY3Rpb25zUm93QnV0dG9uX3NlbGVjdGVkOiAhIW15UmVhY3Rpb25FdmVudCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHRvb2x0aXA7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudG9vbHRpcFJlbmRlcmVkKSB7XHJcbiAgICAgICAgICAgIHRvb2x0aXAgPSA8UmVhY3Rpb25zUm93QnV0dG9uVG9vbHRpcFxyXG4gICAgICAgICAgICAgICAgbXhFdmVudD17dGhpcy5wcm9wcy5teEV2ZW50fVxyXG4gICAgICAgICAgICAgICAgY29udGVudD17Y29udGVudH1cclxuICAgICAgICAgICAgICAgIHJlYWN0aW9uRXZlbnRzPXtyZWFjdGlvbkV2ZW50c31cclxuICAgICAgICAgICAgICAgIHZpc2libGU9e3RoaXMuc3RhdGUudG9vbHRpcFZpc2libGV9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKG14RXZlbnQuZ2V0Um9vbUlkKCkpO1xyXG4gICAgICAgIGxldCBsYWJlbDtcclxuICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICBjb25zdCBzZW5kZXJzID0gW107XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcmVhY3Rpb25FdmVudCBvZiByZWFjdGlvbkV2ZW50cykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWVtYmVyID0gcm9vbS5nZXRNZW1iZXIocmVhY3Rpb25FdmVudC5nZXRTZW5kZXIoKSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBuYW1lID0gbWVtYmVyID8gbWVtYmVyLm5hbWUgOiByZWFjdGlvbkV2ZW50LmdldFNlbmRlcigpO1xyXG4gICAgICAgICAgICAgICAgc2VuZGVycy5wdXNoKG5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxhYmVsID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIjxyZWFjdG9ycy8+PHJlYWN0ZWRXaXRoPiByZWFjdGVkIHdpdGggJShjb250ZW50KXM8L3JlYWN0ZWRXaXRoPlwiLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWN0b3JzOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmb3JtYXRDb21tYVNlcGFyYXRlZExpc3Qoc2VuZGVycywgNik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICByZWFjdGVkV2l0aDogKHN1YikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWNvbnRlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWI7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIHJldHVybiA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9e2NsYXNzZXN9XHJcbiAgICAgICAgICAgIGFyaWEtbGFiZWw9e2xhYmVsfVxyXG4gICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9XHJcbiAgICAgICAgICAgIG9uTW91c2VPdmVyPXt0aGlzLm9uTW91c2VPdmVyfVxyXG4gICAgICAgICAgICBvbk1vdXNlT3V0PXt0aGlzLm9uTW91c2VPdXR9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9SZWFjdGlvbnNSb3dCdXR0b25fY29udGVudFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPlxyXG4gICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfUmVhY3Rpb25zUm93QnV0dG9uX2NvdW50XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+XHJcbiAgICAgICAgICAgICAgICB7Y291bnR9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAge3Rvb2x0aXB9XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgIH1cclxufVxyXG4iXX0=