"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MjolnirBody extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "_onAllowClick", e => {
      e.preventDefault();
      e.stopPropagation();
      const key = "mx_mjolnir_render_".concat(this.props.mxEvent.getRoomId(), "__").concat(this.props.mxEvent.getId());
      localStorage.setItem(key, "true");
      this.props.onMessageAllowed();
    });
  }

  render() {
    return _react.default.createElement("div", {
      className: "mx_MjolnirBody"
    }, _react.default.createElement("i", null, (0, _languageHandler._t)("You have ignored this user, so their message is hidden. <a>Show anyways.</a>", {}, {
      a: sub => _react.default.createElement("a", {
        href: "#",
        onClick: this._onAllowClick
      }, sub)
    })));
  }

}

exports.default = MjolnirBody;
(0, _defineProperty2.default)(MjolnirBody, "propTypes", {
  mxEvent: _propTypes.default.object.isRequired,
  onMessageAllowed: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01qb2xuaXJCb2R5LmpzIl0sIm5hbWVzIjpbIk1qb2xuaXJCb2R5IiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsImtleSIsInByb3BzIiwibXhFdmVudCIsImdldFJvb21JZCIsImdldElkIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsIm9uTWVzc2FnZUFsbG93ZWQiLCJyZW5kZXIiLCJhIiwic3ViIiwiX29uQWxsb3dDbGljayIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFsQkE7Ozs7Ozs7Ozs7Ozs7OztBQW9CZSxNQUFNQSxXQUFOLFNBQTBCQyxlQUFNQyxTQUFoQyxDQUEwQztBQU1yREMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSx5REFJR0MsQ0FBRCxJQUFPO0FBQ25CQSxNQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsTUFBQUEsQ0FBQyxDQUFDRSxlQUFGO0FBRUEsWUFBTUMsR0FBRywrQkFBd0IsS0FBS0MsS0FBTCxDQUFXQyxPQUFYLENBQW1CQyxTQUFuQixFQUF4QixlQUEyRCxLQUFLRixLQUFMLENBQVdDLE9BQVgsQ0FBbUJFLEtBQW5CLEVBQTNELENBQVQ7QUFDQUMsTUFBQUEsWUFBWSxDQUFDQyxPQUFiLENBQXFCTixHQUFyQixFQUEwQixNQUExQjtBQUNBLFdBQUtDLEtBQUwsQ0FBV00sZ0JBQVg7QUFDSCxLQVhhO0FBRWI7O0FBV0RDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQWdDLHdDQUFJLHlCQUNoQyw4RUFEZ0MsRUFFaEMsRUFGZ0MsRUFFNUI7QUFBQ0MsTUFBQUEsQ0FBQyxFQUFHQyxHQUFELElBQVM7QUFBRyxRQUFBLElBQUksRUFBQyxHQUFSO0FBQVksUUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBMUIsU0FBMENELEdBQTFDO0FBQWIsS0FGNEIsQ0FBSixDQUFoQyxDQURKO0FBTUg7O0FBMUJvRDs7OzhCQUFwQ2pCLFcsZUFDRTtBQUNmUyxFQUFBQSxPQUFPLEVBQUVVLG1CQUFVQyxNQUFWLENBQWlCQyxVQURYO0FBRWZQLEVBQUFBLGdCQUFnQixFQUFFSyxtQkFBVUcsSUFBVixDQUFlRDtBQUZsQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQge190fSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWpvbG5pckJvZHkgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25NZXNzYWdlQWxsb3dlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25BbGxvd0NsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgY29uc3Qga2V5ID0gYG14X21qb2xuaXJfcmVuZGVyXyR7dGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpfV9fJHt0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKX1gO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKGtleSwgXCJ0cnVlXCIpO1xyXG4gICAgICAgIHRoaXMucHJvcHMub25NZXNzYWdlQWxsb3dlZCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J214X01qb2xuaXJCb2R5Jz48aT57X3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdSBoYXZlIGlnbm9yZWQgdGhpcyB1c2VyLCBzbyB0aGVpciBtZXNzYWdlIGlzIGhpZGRlbi4gPGE+U2hvdyBhbnl3YXlzLjwvYT5cIixcclxuICAgICAgICAgICAgICAgIHt9LCB7YTogKHN1YikgPT4gPGEgaHJlZj1cIiNcIiBvbkNsaWNrPXt0aGlzLl9vbkFsbG93Q2xpY2t9PntzdWJ9PC9hPn0sXHJcbiAgICAgICAgICAgICl9PC9pPjwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19