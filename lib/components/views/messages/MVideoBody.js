"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _MFileBody = _interopRequireDefault(require("./MFileBody"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _DecryptFile = require("../../../utils/DecryptFile");

var _languageHandler = require("../../../languageHandler");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'MVideoBody',
  propTypes: {
    /* the MatrixEvent to show */
    mxEvent: _propTypes.default.object.isRequired,

    /* called when the video has loaded */
    onHeightChanged: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    return {
      decryptedUrl: null,
      decryptedThumbnailUrl: null,
      decryptedBlob: null,
      error: null
    };
  },
  thumbScale: function (fullWidth, fullHeight, thumbWidth, thumbHeight) {
    if (!fullWidth || !fullHeight) {
      // Cannot calculate thumbnail height for image: missing w/h in metadata. We can't even
      // log this because it's spammy
      return undefined;
    }

    if (fullWidth < thumbWidth && fullHeight < thumbHeight) {
      // no scaling needs to be applied
      return 1;
    }

    const widthMulti = thumbWidth / fullWidth;
    const heightMulti = thumbHeight / fullHeight;

    if (widthMulti < heightMulti) {
      // width is the dominant dimension so scaling will be fixed on that
      return widthMulti;
    } else {
      // height is the dominant dimension so scaling will be fixed on that
      return heightMulti;
    }
  },
  _getContentUrl: function () {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined) {
      return this.state.decryptedUrl;
    } else {
      return _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(content.url);
    }
  },
  _getThumbUrl: function () {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined) {
      return this.state.decryptedThumbnailUrl;
    } else if (content.info && content.info.thumbnail_url) {
      return _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(content.info.thumbnail_url);
    } else {
      return null;
    }
  },
  componentDidMount: function () {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      let thumbnailPromise = Promise.resolve(null);

      if (content.info && content.info.thumbnail_file) {
        thumbnailPromise = (0, _DecryptFile.decryptFile)(content.info.thumbnail_file).then(function (blob) {
          return URL.createObjectURL(blob);
        });
      }

      let decryptedBlob;
      thumbnailPromise.then(thumbnailUrl => {
        return (0, _DecryptFile.decryptFile)(content.file).then(function (blob) {
          decryptedBlob = blob;
          return URL.createObjectURL(blob);
        }).then(contentUrl => {
          this.setState({
            decryptedUrl: contentUrl,
            decryptedThumbnailUrl: thumbnailUrl,
            decryptedBlob: decryptedBlob
          });
          this.props.onHeightChanged();
        });
      }).catch(err => {
        console.warn("Unable to decrypt attachment: ", err); // Set a placeholder image when we can't decrypt the image.

        this.setState({
          error: err
        });
      });
    }
  },
  componentWillUnmount: function () {
    if (this.state.decryptedUrl) {
      URL.revokeObjectURL(this.state.decryptedUrl);
    }

    if (this.state.decryptedThumbnailUrl) {
      URL.revokeObjectURL(this.state.decryptedThumbnailUrl);
    }
  },
  render: function () {
    const content = this.props.mxEvent.getContent();

    if (this.state.error !== null) {
      return _react.default.createElement("span", {
        className: "mx_MVideoBody"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/warning.svg"),
        width: "16",
        height: "16"
      }), (0, _languageHandler._t)("Error decrypting video"));
    }

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      // Need to decrypt the attachment
      // The attachment is decrypted in componentDidMount.
      // For now add an img tag with a spinner.
      return _react.default.createElement("span", {
        className: "mx_MVideoBody"
      }, _react.default.createElement("div", {
        className: "mx_MImageBody_thumbnail mx_MImageBody_thumbnail_spinner"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/spinner.gif"),
        alt: content.body,
        width: "16",
        height: "16"
      })));
    }

    const contentUrl = this._getContentUrl();

    const thumbUrl = this._getThumbUrl();

    const autoplay = _SettingsStore.default.getValue("autoplayGifsAndVideos");

    let height = null;
    let width = null;
    let poster = null;
    let preload = "metadata";

    if (content.info) {
      const scale = this.thumbScale(content.info.w, content.info.h, 480, 360);

      if (scale) {
        width = Math.floor(content.info.w * scale);
        height = Math.floor(content.info.h * scale);
      }

      if (thumbUrl) {
        poster = thumbUrl;
        preload = "none";
      }
    }

    return _react.default.createElement("span", {
      className: "mx_MVideoBody"
    }, _react.default.createElement("video", {
      className: "mx_MVideoBody",
      src: contentUrl,
      alt: content.body,
      controls: true,
      preload: preload,
      muted: autoplay,
      autoPlay: autoplay,
      height: height,
      width: width,
      poster: poster
    }), _react.default.createElement(_MFileBody.default, (0, _extends2.default)({}, this.props, {
      decryptedBlob: this.state.decryptedBlob
    })));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01WaWRlb0JvZHkuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJteEV2ZW50IiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm9uSGVpZ2h0Q2hhbmdlZCIsImZ1bmMiLCJnZXRJbml0aWFsU3RhdGUiLCJkZWNyeXB0ZWRVcmwiLCJkZWNyeXB0ZWRUaHVtYm5haWxVcmwiLCJkZWNyeXB0ZWRCbG9iIiwiZXJyb3IiLCJ0aHVtYlNjYWxlIiwiZnVsbFdpZHRoIiwiZnVsbEhlaWdodCIsInRodW1iV2lkdGgiLCJ0aHVtYkhlaWdodCIsInVuZGVmaW5lZCIsIndpZHRoTXVsdGkiLCJoZWlnaHRNdWx0aSIsIl9nZXRDb250ZW50VXJsIiwiY29udGVudCIsInByb3BzIiwiZ2V0Q29udGVudCIsImZpbGUiLCJzdGF0ZSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm14Y1VybFRvSHR0cCIsInVybCIsIl9nZXRUaHVtYlVybCIsImluZm8iLCJ0aHVtYm5haWxfdXJsIiwiY29tcG9uZW50RGlkTW91bnQiLCJ0aHVtYm5haWxQcm9taXNlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJ0aHVtYm5haWxfZmlsZSIsInRoZW4iLCJibG9iIiwiVVJMIiwiY3JlYXRlT2JqZWN0VVJMIiwidGh1bWJuYWlsVXJsIiwiY29udGVudFVybCIsInNldFN0YXRlIiwiY2F0Y2giLCJlcnIiLCJjb25zb2xlIiwid2FybiIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmV2b2tlT2JqZWN0VVJMIiwicmVuZGVyIiwicmVxdWlyZSIsImJvZHkiLCJ0aHVtYlVybCIsImF1dG9wbGF5IiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwiaGVpZ2h0Iiwid2lkdGgiLCJwb3N0ZXIiLCJwcmVsb2FkIiwic2NhbGUiLCJ3IiwiaCIsIk1hdGgiLCJmbG9vciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBeEJBOzs7Ozs7Ozs7Ozs7Ozs7O2VBMEJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLFlBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0FDLElBQUFBLE9BQU8sRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRm5COztBQUlQO0FBQ0FDLElBQUFBLGVBQWUsRUFBRUgsbUJBQVVJLElBQVYsQ0FBZUY7QUFMekIsR0FIaUI7QUFXNUJHLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsWUFBWSxFQUFFLElBRFg7QUFFSEMsTUFBQUEscUJBQXFCLEVBQUUsSUFGcEI7QUFHSEMsTUFBQUEsYUFBYSxFQUFFLElBSFo7QUFJSEMsTUFBQUEsS0FBSyxFQUFFO0FBSkosS0FBUDtBQU1ILEdBbEIyQjtBQW9CNUJDLEVBQUFBLFVBQVUsRUFBRSxVQUFTQyxTQUFULEVBQW9CQyxVQUFwQixFQUFnQ0MsVUFBaEMsRUFBNENDLFdBQTVDLEVBQXlEO0FBQ2pFLFFBQUksQ0FBQ0gsU0FBRCxJQUFjLENBQUNDLFVBQW5CLEVBQStCO0FBQzNCO0FBQ0E7QUFDQSxhQUFPRyxTQUFQO0FBQ0g7O0FBQ0QsUUFBSUosU0FBUyxHQUFHRSxVQUFaLElBQTBCRCxVQUFVLEdBQUdFLFdBQTNDLEVBQXdEO0FBQ3BEO0FBQ0EsYUFBTyxDQUFQO0FBQ0g7O0FBQ0QsVUFBTUUsVUFBVSxHQUFHSCxVQUFVLEdBQUdGLFNBQWhDO0FBQ0EsVUFBTU0sV0FBVyxHQUFHSCxXQUFXLEdBQUdGLFVBQWxDOztBQUNBLFFBQUlJLFVBQVUsR0FBR0MsV0FBakIsRUFBOEI7QUFDMUI7QUFDQSxhQUFPRCxVQUFQO0FBQ0gsS0FIRCxNQUdPO0FBQ0g7QUFDQSxhQUFPQyxXQUFQO0FBQ0g7QUFDSixHQXZDMkI7QUF5QzVCQyxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QixVQUFNQyxPQUFPLEdBQUcsS0FBS0MsS0FBTCxDQUFXckIsT0FBWCxDQUFtQnNCLFVBQW5CLEVBQWhCOztBQUNBLFFBQUlGLE9BQU8sQ0FBQ0csSUFBUixLQUFpQlAsU0FBckIsRUFBZ0M7QUFDNUIsYUFBTyxLQUFLUSxLQUFMLENBQVdqQixZQUFsQjtBQUNILEtBRkQsTUFFTztBQUNILGFBQU9rQixpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxZQUF0QixDQUFtQ1AsT0FBTyxDQUFDUSxHQUEzQyxDQUFQO0FBQ0g7QUFDSixHQWhEMkI7QUFrRDVCQyxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQixVQUFNVCxPQUFPLEdBQUcsS0FBS0MsS0FBTCxDQUFXckIsT0FBWCxDQUFtQnNCLFVBQW5CLEVBQWhCOztBQUNBLFFBQUlGLE9BQU8sQ0FBQ0csSUFBUixLQUFpQlAsU0FBckIsRUFBZ0M7QUFDNUIsYUFBTyxLQUFLUSxLQUFMLENBQVdoQixxQkFBbEI7QUFDSCxLQUZELE1BRU8sSUFBSVksT0FBTyxDQUFDVSxJQUFSLElBQWdCVixPQUFPLENBQUNVLElBQVIsQ0FBYUMsYUFBakMsRUFBZ0Q7QUFDbkQsYUFBT04saUNBQWdCQyxHQUFoQixHQUFzQkMsWUFBdEIsQ0FBbUNQLE9BQU8sQ0FBQ1UsSUFBUixDQUFhQyxhQUFoRCxDQUFQO0FBQ0gsS0FGTSxNQUVBO0FBQ0gsYUFBTyxJQUFQO0FBQ0g7QUFDSixHQTNEMkI7QUE2RDVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFVBQU1aLE9BQU8sR0FBRyxLQUFLQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cc0IsVUFBbkIsRUFBaEI7O0FBQ0EsUUFBSUYsT0FBTyxDQUFDRyxJQUFSLEtBQWlCUCxTQUFqQixJQUE4QixLQUFLUSxLQUFMLENBQVdqQixZQUFYLEtBQTRCLElBQTlELEVBQW9FO0FBQ2hFLFVBQUkwQixnQkFBZ0IsR0FBR0MsT0FBTyxDQUFDQyxPQUFSLENBQWdCLElBQWhCLENBQXZCOztBQUNBLFVBQUlmLE9BQU8sQ0FBQ1UsSUFBUixJQUFnQlYsT0FBTyxDQUFDVSxJQUFSLENBQWFNLGNBQWpDLEVBQWlEO0FBQzdDSCxRQUFBQSxnQkFBZ0IsR0FBRyw4QkFDZmIsT0FBTyxDQUFDVSxJQUFSLENBQWFNLGNBREUsRUFFakJDLElBRmlCLENBRVosVUFBU0MsSUFBVCxFQUFlO0FBQ2xCLGlCQUFPQyxHQUFHLENBQUNDLGVBQUosQ0FBb0JGLElBQXBCLENBQVA7QUFDSCxTQUprQixDQUFuQjtBQUtIOztBQUNELFVBQUk3QixhQUFKO0FBQ0F3QixNQUFBQSxnQkFBZ0IsQ0FBQ0ksSUFBakIsQ0FBdUJJLFlBQUQsSUFBa0I7QUFDcEMsZUFBTyw4QkFBWXJCLE9BQU8sQ0FBQ0csSUFBcEIsRUFBMEJjLElBQTFCLENBQStCLFVBQVNDLElBQVQsRUFBZTtBQUNqRDdCLFVBQUFBLGFBQWEsR0FBRzZCLElBQWhCO0FBQ0EsaUJBQU9DLEdBQUcsQ0FBQ0MsZUFBSixDQUFvQkYsSUFBcEIsQ0FBUDtBQUNILFNBSE0sRUFHSkQsSUFISSxDQUdFSyxVQUFELElBQWdCO0FBQ3BCLGVBQUtDLFFBQUwsQ0FBYztBQUNWcEMsWUFBQUEsWUFBWSxFQUFFbUMsVUFESjtBQUVWbEMsWUFBQUEscUJBQXFCLEVBQUVpQyxZQUZiO0FBR1ZoQyxZQUFBQSxhQUFhLEVBQUVBO0FBSEwsV0FBZDtBQUtBLGVBQUtZLEtBQUwsQ0FBV2pCLGVBQVg7QUFDSCxTQVZNLENBQVA7QUFXSCxPQVpELEVBWUd3QyxLQVpILENBWVVDLEdBQUQsSUFBUztBQUNkQyxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxnQ0FBYixFQUErQ0YsR0FBL0MsRUFEYyxDQUVkOztBQUNBLGFBQUtGLFFBQUwsQ0FBYztBQUNWakMsVUFBQUEsS0FBSyxFQUFFbUM7QUFERyxTQUFkO0FBR0gsT0FsQkQ7QUFtQkg7QUFDSixHQTdGMkI7QUErRjVCRyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFFBQUksS0FBS3hCLEtBQUwsQ0FBV2pCLFlBQWYsRUFBNkI7QUFDekJnQyxNQUFBQSxHQUFHLENBQUNVLGVBQUosQ0FBb0IsS0FBS3pCLEtBQUwsQ0FBV2pCLFlBQS9CO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLaUIsS0FBTCxDQUFXaEIscUJBQWYsRUFBc0M7QUFDbEMrQixNQUFBQSxHQUFHLENBQUNVLGVBQUosQ0FBb0IsS0FBS3pCLEtBQUwsQ0FBV2hCLHFCQUEvQjtBQUNIO0FBQ0osR0F0RzJCO0FBd0c1QjBDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTTlCLE9BQU8sR0FBRyxLQUFLQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cc0IsVUFBbkIsRUFBaEI7O0FBRUEsUUFBSSxLQUFLRSxLQUFMLENBQVdkLEtBQVgsS0FBcUIsSUFBekIsRUFBK0I7QUFDM0IsYUFDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRXlDLE9BQU8sQ0FBQyxpQ0FBRCxDQUFqQjtBQUFzRCxRQUFBLEtBQUssRUFBQyxJQUE1RDtBQUFpRSxRQUFBLE1BQU0sRUFBQztBQUF4RSxRQURKLEVBRU0seUJBQUcsd0JBQUgsQ0FGTixDQURKO0FBTUg7O0FBRUQsUUFBSS9CLE9BQU8sQ0FBQ0csSUFBUixLQUFpQlAsU0FBakIsSUFBOEIsS0FBS1EsS0FBTCxDQUFXakIsWUFBWCxLQUE0QixJQUE5RCxFQUFvRTtBQUNoRTtBQUNBO0FBQ0E7QUFDQSxhQUNJO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFLLFFBQUEsR0FBRyxFQUFFNEMsT0FBTyxDQUFDLGlDQUFELENBQWpCO0FBQXNELFFBQUEsR0FBRyxFQUFFL0IsT0FBTyxDQUFDZ0MsSUFBbkU7QUFBeUUsUUFBQSxLQUFLLEVBQUMsSUFBL0U7QUFBb0YsUUFBQSxNQUFNLEVBQUM7QUFBM0YsUUFESixDQURKLENBREo7QUFPSDs7QUFFRCxVQUFNVixVQUFVLEdBQUcsS0FBS3ZCLGNBQUwsRUFBbkI7O0FBQ0EsVUFBTWtDLFFBQVEsR0FBRyxLQUFLeEIsWUFBTCxFQUFqQjs7QUFDQSxVQUFNeUIsUUFBUSxHQUFHQyx1QkFBY0MsUUFBZCxDQUF1Qix1QkFBdkIsQ0FBakI7O0FBQ0EsUUFBSUMsTUFBTSxHQUFHLElBQWI7QUFDQSxRQUFJQyxLQUFLLEdBQUcsSUFBWjtBQUNBLFFBQUlDLE1BQU0sR0FBRyxJQUFiO0FBQ0EsUUFBSUMsT0FBTyxHQUFHLFVBQWQ7O0FBQ0EsUUFBSXhDLE9BQU8sQ0FBQ1UsSUFBWixFQUFrQjtBQUNkLFlBQU0rQixLQUFLLEdBQUcsS0FBS2xELFVBQUwsQ0FBZ0JTLE9BQU8sQ0FBQ1UsSUFBUixDQUFhZ0MsQ0FBN0IsRUFBZ0MxQyxPQUFPLENBQUNVLElBQVIsQ0FBYWlDLENBQTdDLEVBQWdELEdBQWhELEVBQXFELEdBQXJELENBQWQ7O0FBQ0EsVUFBSUYsS0FBSixFQUFXO0FBQ1BILFFBQUFBLEtBQUssR0FBR00sSUFBSSxDQUFDQyxLQUFMLENBQVc3QyxPQUFPLENBQUNVLElBQVIsQ0FBYWdDLENBQWIsR0FBaUJELEtBQTVCLENBQVI7QUFDQUosUUFBQUEsTUFBTSxHQUFHTyxJQUFJLENBQUNDLEtBQUwsQ0FBVzdDLE9BQU8sQ0FBQ1UsSUFBUixDQUFhaUMsQ0FBYixHQUFpQkYsS0FBNUIsQ0FBVDtBQUNIOztBQUVELFVBQUlSLFFBQUosRUFBYztBQUNWTSxRQUFBQSxNQUFNLEdBQUdOLFFBQVQ7QUFDQU8sUUFBQUEsT0FBTyxHQUFHLE1BQVY7QUFDSDtBQUNKOztBQUNELFdBQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNJO0FBQU8sTUFBQSxTQUFTLEVBQUMsZUFBakI7QUFBaUMsTUFBQSxHQUFHLEVBQUVsQixVQUF0QztBQUFrRCxNQUFBLEdBQUcsRUFBRXRCLE9BQU8sQ0FBQ2dDLElBQS9EO0FBQ0ksTUFBQSxRQUFRLE1BRFo7QUFDYSxNQUFBLE9BQU8sRUFBRVEsT0FEdEI7QUFDK0IsTUFBQSxLQUFLLEVBQUVOLFFBRHRDO0FBQ2dELE1BQUEsUUFBUSxFQUFFQSxRQUQxRDtBQUVJLE1BQUEsTUFBTSxFQUFFRyxNQUZaO0FBRW9CLE1BQUEsS0FBSyxFQUFFQyxLQUYzQjtBQUVrQyxNQUFBLE1BQU0sRUFBRUM7QUFGMUMsTUFESixFQUtJLDZCQUFDLGtCQUFELDZCQUFlLEtBQUt0QyxLQUFwQjtBQUEyQixNQUFBLGFBQWEsRUFBRSxLQUFLRyxLQUFMLENBQVdmO0FBQXJELE9BTEosQ0FESjtBQVNIO0FBN0oyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgTUZpbGVCb2R5IGZyb20gJy4vTUZpbGVCb2R5JztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCB7IGRlY3J5cHRGaWxlIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvRGVjcnlwdEZpbGUnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnTVZpZGVvQm9keScsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLyogdGhlIE1hdHJpeEV2ZW50IHRvIHNob3cgKi9cclxuICAgICAgICBteEV2ZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIC8qIGNhbGxlZCB3aGVuIHRoZSB2aWRlbyBoYXMgbG9hZGVkICovXHJcbiAgICAgICAgb25IZWlnaHRDaGFuZ2VkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGRlY3J5cHRlZFVybDogbnVsbCxcclxuICAgICAgICAgICAgZGVjcnlwdGVkVGh1bWJuYWlsVXJsOiBudWxsLFxyXG4gICAgICAgICAgICBkZWNyeXB0ZWRCbG9iOiBudWxsLFxyXG4gICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICB0aHVtYlNjYWxlOiBmdW5jdGlvbihmdWxsV2lkdGgsIGZ1bGxIZWlnaHQsIHRodW1iV2lkdGgsIHRodW1iSGVpZ2h0KSB7XHJcbiAgICAgICAgaWYgKCFmdWxsV2lkdGggfHwgIWZ1bGxIZWlnaHQpIHtcclxuICAgICAgICAgICAgLy8gQ2Fubm90IGNhbGN1bGF0ZSB0aHVtYm5haWwgaGVpZ2h0IGZvciBpbWFnZTogbWlzc2luZyB3L2ggaW4gbWV0YWRhdGEuIFdlIGNhbid0IGV2ZW5cclxuICAgICAgICAgICAgLy8gbG9nIHRoaXMgYmVjYXVzZSBpdCdzIHNwYW1teVxyXG4gICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZnVsbFdpZHRoIDwgdGh1bWJXaWR0aCAmJiBmdWxsSGVpZ2h0IDwgdGh1bWJIZWlnaHQpIHtcclxuICAgICAgICAgICAgLy8gbm8gc2NhbGluZyBuZWVkcyB0byBiZSBhcHBsaWVkXHJcbiAgICAgICAgICAgIHJldHVybiAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB3aWR0aE11bHRpID0gdGh1bWJXaWR0aCAvIGZ1bGxXaWR0aDtcclxuICAgICAgICBjb25zdCBoZWlnaHRNdWx0aSA9IHRodW1iSGVpZ2h0IC8gZnVsbEhlaWdodDtcclxuICAgICAgICBpZiAod2lkdGhNdWx0aSA8IGhlaWdodE11bHRpKSB7XHJcbiAgICAgICAgICAgIC8vIHdpZHRoIGlzIHRoZSBkb21pbmFudCBkaW1lbnNpb24gc28gc2NhbGluZyB3aWxsIGJlIGZpeGVkIG9uIHRoYXRcclxuICAgICAgICAgICAgcmV0dXJuIHdpZHRoTXVsdGk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gaGVpZ2h0IGlzIHRoZSBkb21pbmFudCBkaW1lbnNpb24gc28gc2NhbGluZyB3aWxsIGJlIGZpeGVkIG9uIHRoYXRcclxuICAgICAgICAgICAgcmV0dXJuIGhlaWdodE11bHRpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2dldENvbnRlbnRVcmw6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgIGlmIChjb250ZW50LmZpbGUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5teGNVcmxUb0h0dHAoY29udGVudC51cmwpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2dldFRodW1iVXJsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoY29udGVudC5maWxlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuZGVjcnlwdGVkVGh1bWJuYWlsVXJsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoY29udGVudC5pbmZvICYmIGNvbnRlbnQuaW5mby50aHVtYm5haWxfdXJsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubXhjVXJsVG9IdHRwKGNvbnRlbnQuaW5mby50aHVtYm5haWxfdXJsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoY29udGVudC5maWxlICE9PSB1bmRlZmluZWQgJiYgdGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgbGV0IHRodW1ibmFpbFByb21pc2UgPSBQcm9taXNlLnJlc29sdmUobnVsbCk7XHJcbiAgICAgICAgICAgIGlmIChjb250ZW50LmluZm8gJiYgY29udGVudC5pbmZvLnRodW1ibmFpbF9maWxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aHVtYm5haWxQcm9taXNlID0gZGVjcnlwdEZpbGUoXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudC5pbmZvLnRodW1ibmFpbF9maWxlLFxyXG4gICAgICAgICAgICAgICAgKS50aGVuKGZ1bmN0aW9uKGJsb2IpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCBkZWNyeXB0ZWRCbG9iO1xyXG4gICAgICAgICAgICB0aHVtYm5haWxQcm9taXNlLnRoZW4oKHRodW1ibmFpbFVybCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRlY3J5cHRGaWxlKGNvbnRlbnQuZmlsZSkudGhlbihmdW5jdGlvbihibG9iKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVjcnlwdGVkQmxvYiA9IGJsb2I7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKChjb250ZW50VXJsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZFVybDogY29udGVudFVybCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVjcnlwdGVkVGh1bWJuYWlsVXJsOiB0aHVtYm5haWxVcmwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZEJsb2I6IGRlY3J5cHRlZEJsb2IsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWQoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXCJVbmFibGUgdG8gZGVjcnlwdCBhdHRhY2htZW50OiBcIiwgZXJyKTtcclxuICAgICAgICAgICAgICAgIC8vIFNldCBhIHBsYWNlaG9sZGVyIGltYWdlIHdoZW4gd2UgY2FuJ3QgZGVjcnlwdCB0aGUgaW1hZ2UuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcjogZXJyLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmRlY3J5cHRlZFVybCkge1xyXG4gICAgICAgICAgICBVUkwucmV2b2tlT2JqZWN0VVJMKHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGVjcnlwdGVkVGh1bWJuYWlsVXJsKSB7XHJcbiAgICAgICAgICAgIFVSTC5yZXZva2VPYmplY3RVUkwodGhpcy5zdGF0ZS5kZWNyeXB0ZWRUaHVtYm5haWxVcmwpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3IgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01WaWRlb0JvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvd2FybmluZy5zdmdcIil9IHdpZHRoPVwiMTZcIiBoZWlnaHQ9XCIxNlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIkVycm9yIGRlY3J5cHRpbmcgdmlkZW9cIikgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbnRlbnQuZmlsZSAhPT0gdW5kZWZpbmVkICYmIHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIC8vIE5lZWQgdG8gZGVjcnlwdCB0aGUgYXR0YWNobWVudFxyXG4gICAgICAgICAgICAvLyBUaGUgYXR0YWNobWVudCBpcyBkZWNyeXB0ZWQgaW4gY29tcG9uZW50RGlkTW91bnQuXHJcbiAgICAgICAgICAgIC8vIEZvciBub3cgYWRkIGFuIGltZyB0YWcgd2l0aCBhIHNwaW5uZXIuXHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9NVmlkZW9Cb2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NSW1hZ2VCb2R5X3RodW1ibmFpbCBteF9NSW1hZ2VCb2R5X3RodW1ibmFpbF9zcGlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9zcGlubmVyLmdpZlwiKX0gYWx0PXtjb250ZW50LmJvZHl9IHdpZHRoPVwiMTZcIiBoZWlnaHQ9XCIxNlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50VXJsID0gdGhpcy5fZ2V0Q29udGVudFVybCgpO1xyXG4gICAgICAgIGNvbnN0IHRodW1iVXJsID0gdGhpcy5fZ2V0VGh1bWJVcmwoKTtcclxuICAgICAgICBjb25zdCBhdXRvcGxheSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJhdXRvcGxheUdpZnNBbmRWaWRlb3NcIik7XHJcbiAgICAgICAgbGV0IGhlaWdodCA9IG51bGw7XHJcbiAgICAgICAgbGV0IHdpZHRoID0gbnVsbDtcclxuICAgICAgICBsZXQgcG9zdGVyID0gbnVsbDtcclxuICAgICAgICBsZXQgcHJlbG9hZCA9IFwibWV0YWRhdGFcIjtcclxuICAgICAgICBpZiAoY29udGVudC5pbmZvKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNjYWxlID0gdGhpcy50aHVtYlNjYWxlKGNvbnRlbnQuaW5mby53LCBjb250ZW50LmluZm8uaCwgNDgwLCAzNjApO1xyXG4gICAgICAgICAgICBpZiAoc2NhbGUpIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoID0gTWF0aC5mbG9vcihjb250ZW50LmluZm8udyAqIHNjYWxlKTtcclxuICAgICAgICAgICAgICAgIGhlaWdodCA9IE1hdGguZmxvb3IoY29udGVudC5pbmZvLmggKiBzY2FsZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aHVtYlVybCkge1xyXG4gICAgICAgICAgICAgICAgcG9zdGVyID0gdGh1bWJVcmw7XHJcbiAgICAgICAgICAgICAgICBwcmVsb2FkID0gXCJub25lXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfTVZpZGVvQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgPHZpZGVvIGNsYXNzTmFtZT1cIm14X01WaWRlb0JvZHlcIiBzcmM9e2NvbnRlbnRVcmx9IGFsdD17Y29udGVudC5ib2R5fVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xzIHByZWxvYWQ9e3ByZWxvYWR9IG11dGVkPXthdXRvcGxheX0gYXV0b1BsYXk9e2F1dG9wbGF5fVxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodD17aGVpZ2h0fSB3aWR0aD17d2lkdGh9IHBvc3Rlcj17cG9zdGVyfT5cclxuICAgICAgICAgICAgICAgIDwvdmlkZW8+XHJcbiAgICAgICAgICAgICAgICA8TUZpbGVCb2R5IHsuLi50aGlzLnByb3BzfSBkZWNyeXB0ZWRCbG9iPXt0aGlzLnN0YXRlLmRlY3J5cHRlZEJsb2J9IC8+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==