"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HiddenImagePlaceholder = exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MFileBody = _interopRequireDefault(require("./MFileBody"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _DecryptFile = require("../../../utils/DecryptFile");

var _languageHandler = require("../../../languageHandler");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2018, 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MImageBody extends _react.default.Component {
  constructor(props) {
    super(props);
    this.onImageError = this.onImageError.bind(this);
    this.onImageLoad = this.onImageLoad.bind(this);
    this.onImageEnter = this.onImageEnter.bind(this);
    this.onImageLeave = this.onImageLeave.bind(this);
    this.onClientSync = this.onClientSync.bind(this);
    this.onClick = this.onClick.bind(this);
    this._isGif = this._isGif.bind(this);
    this.state = {
      decryptedUrl: null,
      decryptedThumbnailUrl: null,
      decryptedBlob: null,
      error: null,
      imgError: false,
      imgLoaded: false,
      loadedImageDimensions: null,
      hover: false,
      showImage: _SettingsStore.default.getValue("showImages")
    };
    this._image = (0, _react.createRef)();
  } // FIXME: factor this out and aplpy it to MVideoBody and MAudioBody too!


  onClientSync(syncState, prevState) {
    if (this.unmounted) return; // Consider the client reconnected if there is no error with syncing.
    // This means the state could be RECONNECTING, SYNCING, PREPARED or CATCHUP.

    const reconnected = syncState !== "ERROR" && prevState !== syncState;

    if (reconnected && this.state.imgError) {
      // Load the image again
      this.setState({
        imgError: false
      });
    }
  }

  showImage() {
    localStorage.setItem("mx_ShowImage_" + this.props.mxEvent.getId(), "true");
    this.setState({
      showImage: true
    });
  }

  onClick(ev) {
    if (ev.button === 0 && !ev.metaKey) {
      ev.preventDefault();

      if (!this.state.showImage) {
        this.showImage();
        return;
      }

      const content = this.props.mxEvent.getContent();

      const httpUrl = this._getContentUrl();

      const ImageView = sdk.getComponent("elements.ImageView");
      const params = {
        src: httpUrl,
        name: content.body && content.body.length > 0 ? content.body : (0, _languageHandler._t)('Attachment'),
        mxEvent: this.props.mxEvent
      };

      if (content.info) {
        params.width = content.info.w;
        params.height = content.info.h;
        params.fileSize = content.info.size;
      }

      _Modal.default.createDialog(ImageView, params, "mx_Dialog_lightbox");
    }
  }

  _isGif() {
    const content = this.props.mxEvent.getContent();
    return content && content.info && content.info.mimetype === "image/gif";
  }

  onImageEnter(e) {
    this.setState({
      hover: true
    });

    if (!this.state.showImage || !this._isGif() || _SettingsStore.default.getValue("autoplayGifsAndVideos")) {
      return;
    }

    const imgElement = e.target;
    imgElement.src = this._getContentUrl();
  }

  onImageLeave(e) {
    this.setState({
      hover: false
    });

    if (!this.state.showImage || !this._isGif() || _SettingsStore.default.getValue("autoplayGifsAndVideos")) {
      return;
    }

    const imgElement = e.target;
    imgElement.src = this._getThumbUrl();
  }

  onImageError() {
    this.setState({
      imgError: true
    });
  }

  onImageLoad() {
    this.props.onHeightChanged();
    let loadedImageDimensions;

    if (this._image.current) {
      const {
        naturalWidth,
        naturalHeight
      } = this._image.current; // this is only used as a fallback in case content.info.w/h is missing

      loadedImageDimensions = {
        naturalWidth,
        naturalHeight
      };
    }

    this.setState({
      imgLoaded: true,
      loadedImageDimensions
    });
  }

  _getContentUrl() {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined) {
      return this.state.decryptedUrl;
    } else {
      return this.context.mxcUrlToHttp(content.url);
    }
  }

  _getThumbUrl() {
    // FIXME: the dharma skin lets images grow as wide as you like, rather than capped to 800x600.
    // So either we need to support custom timeline widths here, or reimpose the cap, otherwise the
    // thumbnail resolution will be unnecessarily reduced.
    // custom timeline widths seems preferable.
    const pixelRatio = window.devicePixelRatio;
    const thumbWidth = Math.round(800 * pixelRatio);
    const thumbHeight = Math.round(600 * pixelRatio);
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined) {
      // Don't use the thumbnail for clients wishing to autoplay gifs.
      if (this.state.decryptedThumbnailUrl) {
        return this.state.decryptedThumbnailUrl;
      }

      return this.state.decryptedUrl;
    } else if (content.info && content.info.mimetype === "image/svg+xml" && content.info.thumbnail_url) {
      // special case to return clientside sender-generated thumbnails for SVGs, if any,
      // given we deliberately don't thumbnail them serverside to prevent
      // billion lol attacks and similar
      return this.context.mxcUrlToHttp(content.info.thumbnail_url, thumbWidth, thumbHeight);
    } else {
      // we try to download the correct resolution
      // for hi-res images (like retina screenshots).
      // synapse only supports 800x600 thumbnails for now though,
      // so we'll need to download the original image for this to work
      // well for now. First, let's try a few cases that let us avoid
      // downloading the original, including:
      //   - When displaying a GIF, we always want to thumbnail so that we can
      //     properly respect the user's GIF autoplay setting (which relies on
      //     thumbnailing to produce the static preview image)
      //   - On a low DPI device, always thumbnail to save bandwidth
      //   - If there's no sizing info in the event, default to thumbnail
      const info = content.info;

      if (this._isGif() || pixelRatio === 1.0 || !info || !info.w || !info.h || !info.size) {
        return this.context.mxcUrlToHttp(content.url, thumbWidth, thumbHeight);
      } else {
        // we should only request thumbnails if the image is bigger than 800x600
        // (or 1600x1200 on retina) otherwise the image in the timeline will just
        // end up resampled and de-retina'd for no good reason.
        // Ideally the server would pregen 1600x1200 thumbnails in order to provide retina
        // thumbnails, but we don't do this currently in synapse for fear of disk space.
        // As a compromise, let's switch to non-retina thumbnails only if the original
        // image is both physically too large and going to be massive to load in the
        // timeline (e.g. >1MB).
        const isLargerThanThumbnail = info.w > thumbWidth || info.h > thumbHeight;
        const isLargeFileSize = info.size > 1 * 1024 * 1024;

        if (isLargeFileSize && isLargerThanThumbnail) {
          // image is too large physically and bytewise to clutter our timeline so
          // we ask for a thumbnail, despite knowing that it will be max 800x600
          // despite us being retina (as synapse doesn't do 1600x1200 thumbs yet).
          return this.context.mxcUrlToHttp(content.url, thumbWidth, thumbHeight);
        } else {
          // download the original image otherwise, so we can scale it client side
          // to take pixelRatio into account.
          // ( no width/height means we want the original image)
          return this.context.mxcUrlToHttp(content.url);
        }
      }
    }
  }

  componentDidMount() {
    this.unmounted = false;
    this.context.on('sync', this.onClientSync);
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      let thumbnailPromise = Promise.resolve(null);

      if (content.info && content.info.thumbnail_file) {
        thumbnailPromise = (0, _DecryptFile.decryptFile)(content.info.thumbnail_file).then(function (blob) {
          return URL.createObjectURL(blob);
        });
      }

      let decryptedBlob;
      thumbnailPromise.then(thumbnailUrl => {
        return (0, _DecryptFile.decryptFile)(content.file).then(function (blob) {
          decryptedBlob = blob;
          return URL.createObjectURL(blob);
        }).then(contentUrl => {
          if (this.unmounted) return;
          this.setState({
            decryptedUrl: contentUrl,
            decryptedThumbnailUrl: thumbnailUrl,
            decryptedBlob: decryptedBlob
          });
        });
      }).catch(err => {
        if (this.unmounted) return;
        console.warn("Unable to decrypt attachment: ", err); // Set a placeholder image when we can't decrypt the image.

        this.setState({
          error: err
        });
      });
    } // Remember that the user wanted to show this particular image


    if (!this.state.showImage && localStorage.getItem("mx_ShowImage_" + this.props.mxEvent.getId()) === "true") {
      this.setState({
        showImage: true
      });
    }

    this._afterComponentDidMount();
  } // To be overridden by subclasses (e.g. MStickerBody) for further
  // initialisation after componentDidMount


  _afterComponentDidMount() {}

  componentWillUnmount() {
    this.unmounted = true;
    this.context.removeListener('sync', this.onClientSync);

    this._afterComponentWillUnmount();

    if (this.state.decryptedUrl) {
      URL.revokeObjectURL(this.state.decryptedUrl);
    }

    if (this.state.decryptedThumbnailUrl) {
      URL.revokeObjectURL(this.state.decryptedThumbnailUrl);
    }
  } // To be overridden by subclasses (e.g. MStickerBody) for further
  // cleanup after componentWillUnmount


  _afterComponentWillUnmount() {}

  _messageContent(contentUrl, thumbUrl, content) {
    let infoWidth;
    let infoHeight;

    if (content && content.info && content.info.w && content.info.h) {
      infoWidth = content.info.w;
      infoHeight = content.info.h;
    } else {
      // Whilst the image loads, display nothing.
      //
      // Once loaded, use the loaded image dimensions stored in `loadedImageDimensions`.
      //
      // By doing this, the image "pops" into the timeline, but is still restricted
      // by the same width and height logic below.
      if (!this.state.loadedImageDimensions) {
        let imageElement;

        if (!this.state.showImage) {
          imageElement = _react.default.createElement(HiddenImagePlaceholder, null);
        } else {
          imageElement = _react.default.createElement("img", {
            style: {
              display: 'none'
            },
            src: thumbUrl,
            ref: this._image,
            alt: content.body,
            onError: this.onImageError,
            onLoad: this.onImageLoad
          });
        }

        return this.wrapImage(contentUrl, imageElement);
      }

      infoWidth = this.state.loadedImageDimensions.naturalWidth;
      infoHeight = this.state.loadedImageDimensions.naturalHeight;
    } // The maximum height of the thumbnail as it is rendered as an <img>


    const maxHeight = Math.min(this.props.maxImageHeight || 600, infoHeight); // The maximum width of the thumbnail, as dictated by its natural
    // maximum height.

    const maxWidth = infoWidth * maxHeight / infoHeight;
    let img = null;
    let placeholder = null;
    let gifLabel = null; // e2e image hasn't been decrypted yet

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      placeholder = _react.default.createElement("img", {
        src: require("../../../../res/img/spinner.gif"),
        alt: content.body,
        width: "32",
        height: "32"
      });
    } else if (!this.state.imgLoaded) {
      // Deliberately, getSpinner is left unimplemented here, MStickerBody overides
      placeholder = this.getPlaceholder();
    }

    let showPlaceholder = Boolean(placeholder);

    if (thumbUrl && !this.state.imgError) {
      // Restrict the width of the thumbnail here, otherwise it will fill the container
      // which has the same width as the timeline
      // mx_MImageBody_thumbnail resizes img to exactly container size
      img = _react.default.createElement("img", {
        className: "mx_MImageBody_thumbnail",
        src: thumbUrl,
        ref: this._image,
        style: {
          maxWidth: maxWidth + "px"
        },
        alt: content.body,
        onError: this.onImageError,
        onLoad: this.onImageLoad,
        onMouseEnter: this.onImageEnter,
        onMouseLeave: this.onImageLeave
      });
    }

    if (!this.state.showImage) {
      img = _react.default.createElement(HiddenImagePlaceholder, {
        style: {
          maxWidth: maxWidth + "px"
        }
      });
      showPlaceholder = false; // because we're hiding the image, so don't show the sticker icon.
    }

    if (this._isGif() && !_SettingsStore.default.getValue("autoplayGifsAndVideos") && !this.state.hover) {
      gifLabel = _react.default.createElement("p", {
        className: "mx_MImageBody_gifLabel"
      }, "GIF");
    }

    const thumbnail = _react.default.createElement("div", {
      className: "mx_MImageBody_thumbnail_container",
      style: {
        maxHeight: maxHeight + "px"
      }
    }, _react.default.createElement("div", {
      style: {
        paddingBottom: 100 * infoHeight / infoWidth + '%'
      }
    }), showPlaceholder && _react.default.createElement("div", {
      className: "mx_MImageBody_thumbnail",
      style: {
        // Constrain width here so that spinner appears central to the loaded thumbnail
        maxWidth: infoWidth + "px"
      }
    }, _react.default.createElement("div", {
      className: "mx_MImageBody_thumbnail_spinner"
    }, placeholder)), _react.default.createElement("div", {
      style: {
        display: !showPlaceholder ? undefined : 'none'
      }
    }, img, gifLabel), this.state.hover && this.getTooltip());

    return this.wrapImage(contentUrl, thumbnail);
  } // Overidden by MStickerBody


  wrapImage(contentUrl, children) {
    return _react.default.createElement("a", {
      href: contentUrl,
      onClick: this.onClick
    }, children);
  } // Overidden by MStickerBody


  getPlaceholder() {
    // MImageBody doesn't show a placeholder whilst the image loads, (but it could do)
    return null;
  } // Overidden by MStickerBody


  getTooltip() {
    return null;
  } // Overidden by MStickerBody


  getFileBody() {
    return _react.default.createElement(_MFileBody.default, (0, _extends2.default)({}, this.props, {
      decryptedBlob: this.state.decryptedBlob
    }));
  }

  render() {
    const content = this.props.mxEvent.getContent();

    if (this.state.error !== null) {
      return _react.default.createElement("span", {
        className: "mx_MImageBody"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/warning.svg"),
        width: "16",
        height: "16"
      }), (0, _languageHandler._t)("Error decrypting image"));
    }

    const contentUrl = this._getContentUrl();

    let thumbUrl;

    if (this._isGif() && _SettingsStore.default.getValue("autoplayGifsAndVideos")) {
      thumbUrl = contentUrl;
    } else {
      thumbUrl = this._getThumbUrl();
    }

    const thumbnail = this._messageContent(contentUrl, thumbUrl, content);

    const fileBody = this.getFileBody();
    return _react.default.createElement("span", {
      className: "mx_MImageBody"
    }, thumbnail, fileBody);
  }

}

exports.default = MImageBody;
(0, _defineProperty2.default)(MImageBody, "propTypes", {
  /* the MatrixEvent to show */
  mxEvent: _propTypes.default.object.isRequired,

  /* called when the image has loaded */
  onHeightChanged: _propTypes.default.func.isRequired,

  /* the maximum image height to use */
  maxImageHeight: _propTypes.default.number
});
(0, _defineProperty2.default)(MImageBody, "contextType", _MatrixClientContext.default);

class HiddenImagePlaceholder extends _react.default.PureComponent {
  render() {
    let className = 'mx_HiddenImagePlaceholder';
    if (this.props.hover) className += ' mx_HiddenImagePlaceholder_hover';
    return _react.default.createElement("div", {
      className: className
    }, _react.default.createElement("div", {
      className: "mx_HiddenImagePlaceholder_button"
    }, _react.default.createElement("span", {
      className: "mx_HiddenImagePlaceholder_eye"
    }), _react.default.createElement("span", null, (0, _languageHandler._t)("Show image"))));
  }

}

exports.HiddenImagePlaceholder = HiddenImagePlaceholder;
(0, _defineProperty2.default)(HiddenImagePlaceholder, "propTypes", {
  hover: _propTypes.default.bool
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01JbWFnZUJvZHkuanMiXSwibmFtZXMiOlsiTUltYWdlQm9keSIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsIm9uSW1hZ2VFcnJvciIsImJpbmQiLCJvbkltYWdlTG9hZCIsIm9uSW1hZ2VFbnRlciIsIm9uSW1hZ2VMZWF2ZSIsIm9uQ2xpZW50U3luYyIsIm9uQ2xpY2siLCJfaXNHaWYiLCJzdGF0ZSIsImRlY3J5cHRlZFVybCIsImRlY3J5cHRlZFRodW1ibmFpbFVybCIsImRlY3J5cHRlZEJsb2IiLCJlcnJvciIsImltZ0Vycm9yIiwiaW1nTG9hZGVkIiwibG9hZGVkSW1hZ2VEaW1lbnNpb25zIiwiaG92ZXIiLCJzaG93SW1hZ2UiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJfaW1hZ2UiLCJzeW5jU3RhdGUiLCJwcmV2U3RhdGUiLCJ1bm1vdW50ZWQiLCJyZWNvbm5lY3RlZCIsInNldFN0YXRlIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsIm14RXZlbnQiLCJnZXRJZCIsImV2IiwiYnV0dG9uIiwibWV0YUtleSIsInByZXZlbnREZWZhdWx0IiwiY29udGVudCIsImdldENvbnRlbnQiLCJodHRwVXJsIiwiX2dldENvbnRlbnRVcmwiLCJJbWFnZVZpZXciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJwYXJhbXMiLCJzcmMiLCJuYW1lIiwiYm9keSIsImxlbmd0aCIsImluZm8iLCJ3aWR0aCIsInciLCJoZWlnaHQiLCJoIiwiZmlsZVNpemUiLCJzaXplIiwiTW9kYWwiLCJjcmVhdGVEaWFsb2ciLCJtaW1ldHlwZSIsImUiLCJpbWdFbGVtZW50IiwidGFyZ2V0IiwiX2dldFRodW1iVXJsIiwib25IZWlnaHRDaGFuZ2VkIiwiY3VycmVudCIsIm5hdHVyYWxXaWR0aCIsIm5hdHVyYWxIZWlnaHQiLCJmaWxlIiwidW5kZWZpbmVkIiwiY29udGV4dCIsIm14Y1VybFRvSHR0cCIsInVybCIsInBpeGVsUmF0aW8iLCJ3aW5kb3ciLCJkZXZpY2VQaXhlbFJhdGlvIiwidGh1bWJXaWR0aCIsIk1hdGgiLCJyb3VuZCIsInRodW1iSGVpZ2h0IiwidGh1bWJuYWlsX3VybCIsImlzTGFyZ2VyVGhhblRodW1ibmFpbCIsImlzTGFyZ2VGaWxlU2l6ZSIsImNvbXBvbmVudERpZE1vdW50Iiwib24iLCJ0aHVtYm5haWxQcm9taXNlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJ0aHVtYm5haWxfZmlsZSIsInRoZW4iLCJibG9iIiwiVVJMIiwiY3JlYXRlT2JqZWN0VVJMIiwidGh1bWJuYWlsVXJsIiwiY29udGVudFVybCIsImNhdGNoIiwiZXJyIiwiY29uc29sZSIsIndhcm4iLCJnZXRJdGVtIiwiX2FmdGVyQ29tcG9uZW50RGlkTW91bnQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUxpc3RlbmVyIiwiX2FmdGVyQ29tcG9uZW50V2lsbFVubW91bnQiLCJyZXZva2VPYmplY3RVUkwiLCJfbWVzc2FnZUNvbnRlbnQiLCJ0aHVtYlVybCIsImluZm9XaWR0aCIsImluZm9IZWlnaHQiLCJpbWFnZUVsZW1lbnQiLCJkaXNwbGF5Iiwid3JhcEltYWdlIiwibWF4SGVpZ2h0IiwibWluIiwibWF4SW1hZ2VIZWlnaHQiLCJtYXhXaWR0aCIsImltZyIsInBsYWNlaG9sZGVyIiwiZ2lmTGFiZWwiLCJyZXF1aXJlIiwiZ2V0UGxhY2Vob2xkZXIiLCJzaG93UGxhY2Vob2xkZXIiLCJCb29sZWFuIiwidGh1bWJuYWlsIiwicGFkZGluZ0JvdHRvbSIsImdldFRvb2x0aXAiLCJjaGlsZHJlbiIsImdldEZpbGVCb2R5IiwicmVuZGVyIiwiZmlsZUJvZHkiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZnVuYyIsIm51bWJlciIsIk1hdHJpeENsaWVudENvbnRleHQiLCJIaWRkZW5JbWFnZVBsYWNlaG9sZGVyIiwiUHVyZUNvbXBvbmVudCIsImNsYXNzTmFtZSIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUEzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNkJlLE1BQU1BLFVBQU4sU0FBeUJDLGVBQU1DLFNBQS9CLENBQXlDO0FBY3BEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFFQSxTQUFLQyxZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixLQUFLQSxXQUFMLENBQWlCRCxJQUFqQixDQUFzQixJQUF0QixDQUFuQjtBQUNBLFNBQUtFLFlBQUwsR0FBb0IsS0FBS0EsWUFBTCxDQUFrQkYsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEI7QUFDQSxTQUFLRyxZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JILElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0EsU0FBS0ksWUFBTCxHQUFvQixLQUFLQSxZQUFMLENBQWtCSixJQUFsQixDQUF1QixJQUF2QixDQUFwQjtBQUNBLFNBQUtLLE9BQUwsR0FBZSxLQUFLQSxPQUFMLENBQWFMLElBQWIsQ0FBa0IsSUFBbEIsQ0FBZjtBQUNBLFNBQUtNLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlOLElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUVBLFNBQUtPLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxZQUFZLEVBQUUsSUFETDtBQUVUQyxNQUFBQSxxQkFBcUIsRUFBRSxJQUZkO0FBR1RDLE1BQUFBLGFBQWEsRUFBRSxJQUhOO0FBSVRDLE1BQUFBLEtBQUssRUFBRSxJQUpFO0FBS1RDLE1BQUFBLFFBQVEsRUFBRSxLQUxEO0FBTVRDLE1BQUFBLFNBQVMsRUFBRSxLQU5GO0FBT1RDLE1BQUFBLHFCQUFxQixFQUFFLElBUGQ7QUFRVEMsTUFBQUEsS0FBSyxFQUFFLEtBUkU7QUFTVEMsTUFBQUEsU0FBUyxFQUFFQyx1QkFBY0MsUUFBZCxDQUF1QixZQUF2QjtBQVRGLEtBQWI7QUFZQSxTQUFLQyxNQUFMLEdBQWMsdUJBQWQ7QUFDSCxHQXRDbUQsQ0F3Q3BEOzs7QUFDQWYsRUFBQUEsWUFBWSxDQUFDZ0IsU0FBRCxFQUFZQyxTQUFaLEVBQXVCO0FBQy9CLFFBQUksS0FBS0MsU0FBVCxFQUFvQixPQURXLENBRS9CO0FBQ0E7O0FBQ0EsVUFBTUMsV0FBVyxHQUFHSCxTQUFTLEtBQUssT0FBZCxJQUF5QkMsU0FBUyxLQUFLRCxTQUEzRDs7QUFDQSxRQUFJRyxXQUFXLElBQUksS0FBS2hCLEtBQUwsQ0FBV0ssUUFBOUIsRUFBd0M7QUFDcEM7QUFDQSxXQUFLWSxRQUFMLENBQWM7QUFDVlosUUFBQUEsUUFBUSxFQUFFO0FBREEsT0FBZDtBQUdIO0FBQ0o7O0FBRURJLEVBQUFBLFNBQVMsR0FBRztBQUNSUyxJQUFBQSxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsa0JBQWtCLEtBQUs1QixLQUFMLENBQVc2QixPQUFYLENBQW1CQyxLQUFuQixFQUF2QyxFQUFtRSxNQUFuRTtBQUNBLFNBQUtKLFFBQUwsQ0FBYztBQUFDUixNQUFBQSxTQUFTLEVBQUU7QUFBWixLQUFkO0FBQ0g7O0FBRURYLEVBQUFBLE9BQU8sQ0FBQ3dCLEVBQUQsRUFBSztBQUNSLFFBQUlBLEVBQUUsQ0FBQ0MsTUFBSCxLQUFjLENBQWQsSUFBbUIsQ0FBQ0QsRUFBRSxDQUFDRSxPQUEzQixFQUFvQztBQUNoQ0YsTUFBQUEsRUFBRSxDQUFDRyxjQUFIOztBQUNBLFVBQUksQ0FBQyxLQUFLekIsS0FBTCxDQUFXUyxTQUFoQixFQUEyQjtBQUN2QixhQUFLQSxTQUFMO0FBQ0E7QUFDSDs7QUFFRCxZQUFNaUIsT0FBTyxHQUFHLEtBQUtuQyxLQUFMLENBQVc2QixPQUFYLENBQW1CTyxVQUFuQixFQUFoQjs7QUFDQSxZQUFNQyxPQUFPLEdBQUcsS0FBS0MsY0FBTCxFQUFoQjs7QUFDQSxZQUFNQyxTQUFTLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbEI7QUFDQSxZQUFNQyxNQUFNLEdBQUc7QUFDWEMsUUFBQUEsR0FBRyxFQUFFTixPQURNO0FBRVhPLFFBQUFBLElBQUksRUFBRVQsT0FBTyxDQUFDVSxJQUFSLElBQWdCVixPQUFPLENBQUNVLElBQVIsQ0FBYUMsTUFBYixHQUFzQixDQUF0QyxHQUEwQ1gsT0FBTyxDQUFDVSxJQUFsRCxHQUF5RCx5QkFBRyxZQUFILENBRnBEO0FBR1hoQixRQUFBQSxPQUFPLEVBQUUsS0FBSzdCLEtBQUwsQ0FBVzZCO0FBSFQsT0FBZjs7QUFNQSxVQUFJTSxPQUFPLENBQUNZLElBQVosRUFBa0I7QUFDZEwsUUFBQUEsTUFBTSxDQUFDTSxLQUFQLEdBQWViLE9BQU8sQ0FBQ1ksSUFBUixDQUFhRSxDQUE1QjtBQUNBUCxRQUFBQSxNQUFNLENBQUNRLE1BQVAsR0FBZ0JmLE9BQU8sQ0FBQ1ksSUFBUixDQUFhSSxDQUE3QjtBQUNBVCxRQUFBQSxNQUFNLENBQUNVLFFBQVAsR0FBa0JqQixPQUFPLENBQUNZLElBQVIsQ0FBYU0sSUFBL0I7QUFDSDs7QUFFREMscUJBQU1DLFlBQU4sQ0FBbUJoQixTQUFuQixFQUE4QkcsTUFBOUIsRUFBc0Msb0JBQXRDO0FBQ0g7QUFDSjs7QUFFRGxDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU0yQixPQUFPLEdBQUcsS0FBS25DLEtBQUwsQ0FBVzZCLE9BQVgsQ0FBbUJPLFVBQW5CLEVBQWhCO0FBQ0EsV0FDRUQsT0FBTyxJQUNQQSxPQUFPLENBQUNZLElBRFIsSUFFQVosT0FBTyxDQUFDWSxJQUFSLENBQWFTLFFBQWIsS0FBMEIsV0FINUI7QUFLSDs7QUFFRHBELEVBQUFBLFlBQVksQ0FBQ3FELENBQUQsRUFBSTtBQUNaLFNBQUsvQixRQUFMLENBQWM7QUFBRVQsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBZDs7QUFFQSxRQUFJLENBQUMsS0FBS1IsS0FBTCxDQUFXUyxTQUFaLElBQXlCLENBQUMsS0FBS1YsTUFBTCxFQUExQixJQUEyQ1csdUJBQWNDLFFBQWQsQ0FBdUIsdUJBQXZCLENBQS9DLEVBQWdHO0FBQzVGO0FBQ0g7O0FBQ0QsVUFBTXNDLFVBQVUsR0FBR0QsQ0FBQyxDQUFDRSxNQUFyQjtBQUNBRCxJQUFBQSxVQUFVLENBQUNmLEdBQVgsR0FBaUIsS0FBS0wsY0FBTCxFQUFqQjtBQUNIOztBQUVEakMsRUFBQUEsWUFBWSxDQUFDb0QsQ0FBRCxFQUFJO0FBQ1osU0FBSy9CLFFBQUwsQ0FBYztBQUFFVCxNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQUFkOztBQUVBLFFBQUksQ0FBQyxLQUFLUixLQUFMLENBQVdTLFNBQVosSUFBeUIsQ0FBQyxLQUFLVixNQUFMLEVBQTFCLElBQTJDVyx1QkFBY0MsUUFBZCxDQUF1Qix1QkFBdkIsQ0FBL0MsRUFBZ0c7QUFDNUY7QUFDSDs7QUFDRCxVQUFNc0MsVUFBVSxHQUFHRCxDQUFDLENBQUNFLE1BQXJCO0FBQ0FELElBQUFBLFVBQVUsQ0FBQ2YsR0FBWCxHQUFpQixLQUFLaUIsWUFBTCxFQUFqQjtBQUNIOztBQUVEM0QsRUFBQUEsWUFBWSxHQUFHO0FBQ1gsU0FBS3lCLFFBQUwsQ0FBYztBQUNWWixNQUFBQSxRQUFRLEVBQUU7QUFEQSxLQUFkO0FBR0g7O0FBRURYLEVBQUFBLFdBQVcsR0FBRztBQUNWLFNBQUtILEtBQUwsQ0FBVzZELGVBQVg7QUFFQSxRQUFJN0MscUJBQUo7O0FBRUEsUUFBSSxLQUFLSyxNQUFMLENBQVl5QyxPQUFoQixFQUF5QjtBQUNyQixZQUFNO0FBQUVDLFFBQUFBLFlBQUY7QUFBZ0JDLFFBQUFBO0FBQWhCLFVBQWtDLEtBQUszQyxNQUFMLENBQVl5QyxPQUFwRCxDQURxQixDQUVyQjs7QUFDQTlDLE1BQUFBLHFCQUFxQixHQUFHO0FBQUUrQyxRQUFBQSxZQUFGO0FBQWdCQyxRQUFBQTtBQUFoQixPQUF4QjtBQUNIOztBQUVELFNBQUt0QyxRQUFMLENBQWM7QUFBRVgsTUFBQUEsU0FBUyxFQUFFLElBQWI7QUFBbUJDLE1BQUFBO0FBQW5CLEtBQWQ7QUFDSDs7QUFFRHNCLEVBQUFBLGNBQWMsR0FBRztBQUNiLFVBQU1ILE9BQU8sR0FBRyxLQUFLbkMsS0FBTCxDQUFXNkIsT0FBWCxDQUFtQk8sVUFBbkIsRUFBaEI7O0FBQ0EsUUFBSUQsT0FBTyxDQUFDOEIsSUFBUixLQUFpQkMsU0FBckIsRUFBZ0M7QUFDNUIsYUFBTyxLQUFLekQsS0FBTCxDQUFXQyxZQUFsQjtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sS0FBS3lELE9BQUwsQ0FBYUMsWUFBYixDQUEwQmpDLE9BQU8sQ0FBQ2tDLEdBQWxDLENBQVA7QUFDSDtBQUNKOztBQUVEVCxFQUFBQSxZQUFZLEdBQUc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQU1VLFVBQVUsR0FBR0MsTUFBTSxDQUFDQyxnQkFBMUI7QUFDQSxVQUFNQyxVQUFVLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXLE1BQU1MLFVBQWpCLENBQW5CO0FBQ0EsVUFBTU0sV0FBVyxHQUFHRixJQUFJLENBQUNDLEtBQUwsQ0FBVyxNQUFNTCxVQUFqQixDQUFwQjtBQUVBLFVBQU1uQyxPQUFPLEdBQUcsS0FBS25DLEtBQUwsQ0FBVzZCLE9BQVgsQ0FBbUJPLFVBQW5CLEVBQWhCOztBQUNBLFFBQUlELE9BQU8sQ0FBQzhCLElBQVIsS0FBaUJDLFNBQXJCLEVBQWdDO0FBQzVCO0FBQ0EsVUFBSSxLQUFLekQsS0FBTCxDQUFXRSxxQkFBZixFQUFzQztBQUNsQyxlQUFPLEtBQUtGLEtBQUwsQ0FBV0UscUJBQWxCO0FBQ0g7O0FBQ0QsYUFBTyxLQUFLRixLQUFMLENBQVdDLFlBQWxCO0FBQ0gsS0FORCxNQU1PLElBQUl5QixPQUFPLENBQUNZLElBQVIsSUFBZ0JaLE9BQU8sQ0FBQ1ksSUFBUixDQUFhUyxRQUFiLEtBQTBCLGVBQTFDLElBQTZEckIsT0FBTyxDQUFDWSxJQUFSLENBQWE4QixhQUE5RSxFQUE2RjtBQUNoRztBQUNBO0FBQ0E7QUFDQSxhQUFPLEtBQUtWLE9BQUwsQ0FBYUMsWUFBYixDQUNIakMsT0FBTyxDQUFDWSxJQUFSLENBQWE4QixhQURWLEVBRUhKLFVBRkcsRUFHSEcsV0FIRyxDQUFQO0FBS0gsS0FUTSxNQVNBO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQU03QixJQUFJLEdBQUdaLE9BQU8sQ0FBQ1ksSUFBckI7O0FBQ0EsVUFDSSxLQUFLdkMsTUFBTCxNQUNBOEQsVUFBVSxLQUFLLEdBRGYsSUFFQyxDQUFDdkIsSUFBRCxJQUFTLENBQUNBLElBQUksQ0FBQ0UsQ0FBZixJQUFvQixDQUFDRixJQUFJLENBQUNJLENBQTFCLElBQStCLENBQUNKLElBQUksQ0FBQ00sSUFIMUMsRUFJRTtBQUNFLGVBQU8sS0FBS2MsT0FBTCxDQUFhQyxZQUFiLENBQTBCakMsT0FBTyxDQUFDa0MsR0FBbEMsRUFBdUNJLFVBQXZDLEVBQW1ERyxXQUFuRCxDQUFQO0FBQ0gsT0FORCxNQU1PO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLGNBQU1FLHFCQUFxQixHQUN2Qi9CLElBQUksQ0FBQ0UsQ0FBTCxHQUFTd0IsVUFBVCxJQUNBMUIsSUFBSSxDQUFDSSxDQUFMLEdBQVN5QixXQUZiO0FBSUEsY0FBTUcsZUFBZSxHQUFHaEMsSUFBSSxDQUFDTSxJQUFMLEdBQVksSUFBRSxJQUFGLEdBQU8sSUFBM0M7O0FBRUEsWUFBSTBCLGVBQWUsSUFBSUQscUJBQXZCLEVBQThDO0FBQzFDO0FBQ0E7QUFDQTtBQUNBLGlCQUFPLEtBQUtYLE9BQUwsQ0FBYUMsWUFBYixDQUNIakMsT0FBTyxDQUFDa0MsR0FETCxFQUVISSxVQUZHLEVBR0hHLFdBSEcsQ0FBUDtBQUtILFNBVEQsTUFTTztBQUNIO0FBQ0E7QUFDQTtBQUNBLGlCQUFPLEtBQUtULE9BQUwsQ0FBYUMsWUFBYixDQUNIakMsT0FBTyxDQUFDa0MsR0FETCxDQUFQO0FBR0g7QUFDSjtBQUNKO0FBQ0o7O0FBRURXLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFNBQUt4RCxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsU0FBSzJDLE9BQUwsQ0FBYWMsRUFBYixDQUFnQixNQUFoQixFQUF3QixLQUFLM0UsWUFBN0I7QUFFQSxVQUFNNkIsT0FBTyxHQUFHLEtBQUtuQyxLQUFMLENBQVc2QixPQUFYLENBQW1CTyxVQUFuQixFQUFoQjs7QUFDQSxRQUFJRCxPQUFPLENBQUM4QixJQUFSLEtBQWlCQyxTQUFqQixJQUE4QixLQUFLekQsS0FBTCxDQUFXQyxZQUFYLEtBQTRCLElBQTlELEVBQW9FO0FBQ2hFLFVBQUl3RSxnQkFBZ0IsR0FBR0MsT0FBTyxDQUFDQyxPQUFSLENBQWdCLElBQWhCLENBQXZCOztBQUNBLFVBQUlqRCxPQUFPLENBQUNZLElBQVIsSUFBZ0JaLE9BQU8sQ0FBQ1ksSUFBUixDQUFhc0MsY0FBakMsRUFBaUQ7QUFDN0NILFFBQUFBLGdCQUFnQixHQUFHLDhCQUNmL0MsT0FBTyxDQUFDWSxJQUFSLENBQWFzQyxjQURFLEVBRWpCQyxJQUZpQixDQUVaLFVBQVNDLElBQVQsRUFBZTtBQUNsQixpQkFBT0MsR0FBRyxDQUFDQyxlQUFKLENBQW9CRixJQUFwQixDQUFQO0FBQ0gsU0FKa0IsQ0FBbkI7QUFLSDs7QUFDRCxVQUFJM0UsYUFBSjtBQUNBc0UsTUFBQUEsZ0JBQWdCLENBQUNJLElBQWpCLENBQXVCSSxZQUFELElBQWtCO0FBQ3BDLGVBQU8sOEJBQVl2RCxPQUFPLENBQUM4QixJQUFwQixFQUEwQnFCLElBQTFCLENBQStCLFVBQVNDLElBQVQsRUFBZTtBQUNqRDNFLFVBQUFBLGFBQWEsR0FBRzJFLElBQWhCO0FBQ0EsaUJBQU9DLEdBQUcsQ0FBQ0MsZUFBSixDQUFvQkYsSUFBcEIsQ0FBUDtBQUNILFNBSE0sRUFHSkQsSUFISSxDQUdFSyxVQUFELElBQWdCO0FBQ3BCLGNBQUksS0FBS25FLFNBQVQsRUFBb0I7QUFDcEIsZUFBS0UsUUFBTCxDQUFjO0FBQ1ZoQixZQUFBQSxZQUFZLEVBQUVpRixVQURKO0FBRVZoRixZQUFBQSxxQkFBcUIsRUFBRStFLFlBRmI7QUFHVjlFLFlBQUFBLGFBQWEsRUFBRUE7QUFITCxXQUFkO0FBS0gsU0FWTSxDQUFQO0FBV0gsT0FaRCxFQVlHZ0YsS0FaSCxDQVlVQyxHQUFELElBQVM7QUFDZCxZQUFJLEtBQUtyRSxTQUFULEVBQW9CO0FBQ3BCc0UsUUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsZ0NBQWIsRUFBK0NGLEdBQS9DLEVBRmMsQ0FHZDs7QUFDQSxhQUFLbkUsUUFBTCxDQUFjO0FBQ1ZiLFVBQUFBLEtBQUssRUFBRWdGO0FBREcsU0FBZDtBQUdILE9BbkJEO0FBb0JILEtBbkNlLENBcUNoQjs7O0FBQ0EsUUFBSSxDQUFDLEtBQUtwRixLQUFMLENBQVdTLFNBQVosSUFBeUJTLFlBQVksQ0FBQ3FFLE9BQWIsQ0FBcUIsa0JBQWtCLEtBQUtoRyxLQUFMLENBQVc2QixPQUFYLENBQW1CQyxLQUFuQixFQUF2QyxNQUF1RSxNQUFwRyxFQUE0RztBQUN4RyxXQUFLSixRQUFMLENBQWM7QUFBQ1IsUUFBQUEsU0FBUyxFQUFFO0FBQVosT0FBZDtBQUNIOztBQUVELFNBQUsrRSx1QkFBTDtBQUNILEdBNVFtRCxDQThRcEQ7QUFDQTs7O0FBQ0FBLEVBQUFBLHVCQUF1QixHQUFHLENBQ3pCOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixTQUFLMUUsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFNBQUsyQyxPQUFMLENBQWFnQyxjQUFiLENBQTRCLE1BQTVCLEVBQW9DLEtBQUs3RixZQUF6Qzs7QUFDQSxTQUFLOEYsMEJBQUw7O0FBRUEsUUFBSSxLQUFLM0YsS0FBTCxDQUFXQyxZQUFmLEVBQTZCO0FBQ3pCOEUsTUFBQUEsR0FBRyxDQUFDYSxlQUFKLENBQW9CLEtBQUs1RixLQUFMLENBQVdDLFlBQS9CO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLRCxLQUFMLENBQVdFLHFCQUFmLEVBQXNDO0FBQ2xDNkUsTUFBQUEsR0FBRyxDQUFDYSxlQUFKLENBQW9CLEtBQUs1RixLQUFMLENBQVdFLHFCQUEvQjtBQUNIO0FBQ0osR0E5Um1ELENBZ1NwRDtBQUNBOzs7QUFDQXlGLEVBQUFBLDBCQUEwQixHQUFHLENBQzVCOztBQUVERSxFQUFBQSxlQUFlLENBQUNYLFVBQUQsRUFBYVksUUFBYixFQUF1QnBFLE9BQXZCLEVBQWdDO0FBQzNDLFFBQUlxRSxTQUFKO0FBQ0EsUUFBSUMsVUFBSjs7QUFFQSxRQUFJdEUsT0FBTyxJQUFJQSxPQUFPLENBQUNZLElBQW5CLElBQTJCWixPQUFPLENBQUNZLElBQVIsQ0FBYUUsQ0FBeEMsSUFBNkNkLE9BQU8sQ0FBQ1ksSUFBUixDQUFhSSxDQUE5RCxFQUFpRTtBQUM3RHFELE1BQUFBLFNBQVMsR0FBR3JFLE9BQU8sQ0FBQ1ksSUFBUixDQUFhRSxDQUF6QjtBQUNBd0QsTUFBQUEsVUFBVSxHQUFHdEUsT0FBTyxDQUFDWSxJQUFSLENBQWFJLENBQTFCO0FBQ0gsS0FIRCxNQUdPO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBSSxDQUFDLEtBQUsxQyxLQUFMLENBQVdPLHFCQUFoQixFQUF1QztBQUNuQyxZQUFJMEYsWUFBSjs7QUFDQSxZQUFJLENBQUMsS0FBS2pHLEtBQUwsQ0FBV1MsU0FBaEIsRUFBMkI7QUFDdkJ3RixVQUFBQSxZQUFZLEdBQUcsNkJBQUMsc0JBQUQsT0FBZjtBQUNILFNBRkQsTUFFTztBQUNIQSxVQUFBQSxZQUFZLEdBQ1I7QUFBSyxZQUFBLEtBQUssRUFBRTtBQUFDQyxjQUFBQSxPQUFPLEVBQUU7QUFBVixhQUFaO0FBQStCLFlBQUEsR0FBRyxFQUFFSixRQUFwQztBQUE4QyxZQUFBLEdBQUcsRUFBRSxLQUFLbEYsTUFBeEQ7QUFDSyxZQUFBLEdBQUcsRUFBRWMsT0FBTyxDQUFDVSxJQURsQjtBQUVLLFlBQUEsT0FBTyxFQUFFLEtBQUs1QyxZQUZuQjtBQUdLLFlBQUEsTUFBTSxFQUFFLEtBQUtFO0FBSGxCLFlBREo7QUFPSDs7QUFDRCxlQUFPLEtBQUt5RyxTQUFMLENBQWVqQixVQUFmLEVBQTJCZSxZQUEzQixDQUFQO0FBQ0g7O0FBQ0RGLE1BQUFBLFNBQVMsR0FBRyxLQUFLL0YsS0FBTCxDQUFXTyxxQkFBWCxDQUFpQytDLFlBQTdDO0FBQ0EwQyxNQUFBQSxVQUFVLEdBQUcsS0FBS2hHLEtBQUwsQ0FBV08scUJBQVgsQ0FBaUNnRCxhQUE5QztBQUNILEtBL0IwQyxDQWlDM0M7OztBQUNBLFVBQU02QyxTQUFTLEdBQUduQyxJQUFJLENBQUNvQyxHQUFMLENBQVMsS0FBSzlHLEtBQUwsQ0FBVytHLGNBQVgsSUFBNkIsR0FBdEMsRUFBMkNOLFVBQTNDLENBQWxCLENBbEMyQyxDQW1DM0M7QUFDQTs7QUFDQSxVQUFNTyxRQUFRLEdBQUdSLFNBQVMsR0FBR0ssU0FBWixHQUF3QkosVUFBekM7QUFFQSxRQUFJUSxHQUFHLEdBQUcsSUFBVjtBQUNBLFFBQUlDLFdBQVcsR0FBRyxJQUFsQjtBQUNBLFFBQUlDLFFBQVEsR0FBRyxJQUFmLENBekMyQyxDQTJDM0M7O0FBQ0EsUUFBSWhGLE9BQU8sQ0FBQzhCLElBQVIsS0FBaUJDLFNBQWpCLElBQThCLEtBQUt6RCxLQUFMLENBQVdDLFlBQVgsS0FBNEIsSUFBOUQsRUFBb0U7QUFDaEV3RyxNQUFBQSxXQUFXLEdBQUc7QUFDVixRQUFBLEdBQUcsRUFBRUUsT0FBTyxDQUFDLGlDQUFELENBREY7QUFFVixRQUFBLEdBQUcsRUFBRWpGLE9BQU8sQ0FBQ1UsSUFGSDtBQUdWLFFBQUEsS0FBSyxFQUFDLElBSEk7QUFJVixRQUFBLE1BQU0sRUFBQztBQUpHLFFBQWQ7QUFNSCxLQVBELE1BT08sSUFBSSxDQUFDLEtBQUtwQyxLQUFMLENBQVdNLFNBQWhCLEVBQTJCO0FBQzlCO0FBQ0FtRyxNQUFBQSxXQUFXLEdBQUcsS0FBS0csY0FBTCxFQUFkO0FBQ0g7O0FBRUQsUUFBSUMsZUFBZSxHQUFHQyxPQUFPLENBQUNMLFdBQUQsQ0FBN0I7O0FBRUEsUUFBSVgsUUFBUSxJQUFJLENBQUMsS0FBSzlGLEtBQUwsQ0FBV0ssUUFBNUIsRUFBc0M7QUFDbEM7QUFDQTtBQUNBO0FBQ0FtRyxNQUFBQSxHQUFHLEdBQ0M7QUFBSyxRQUFBLFNBQVMsRUFBQyx5QkFBZjtBQUF5QyxRQUFBLEdBQUcsRUFBRVYsUUFBOUM7QUFBd0QsUUFBQSxHQUFHLEVBQUUsS0FBS2xGLE1BQWxFO0FBQ0ssUUFBQSxLQUFLLEVBQUU7QUFBRTJGLFVBQUFBLFFBQVEsRUFBRUEsUUFBUSxHQUFHO0FBQXZCLFNBRFo7QUFFSyxRQUFBLEdBQUcsRUFBRTdFLE9BQU8sQ0FBQ1UsSUFGbEI7QUFHSyxRQUFBLE9BQU8sRUFBRSxLQUFLNUMsWUFIbkI7QUFJSyxRQUFBLE1BQU0sRUFBRSxLQUFLRSxXQUpsQjtBQUtLLFFBQUEsWUFBWSxFQUFFLEtBQUtDLFlBTHhCO0FBTUssUUFBQSxZQUFZLEVBQUUsS0FBS0M7QUFOeEIsUUFESjtBQVNIOztBQUVELFFBQUksQ0FBQyxLQUFLSSxLQUFMLENBQVdTLFNBQWhCLEVBQTJCO0FBQ3ZCK0YsTUFBQUEsR0FBRyxHQUFHLDZCQUFDLHNCQUFEO0FBQXdCLFFBQUEsS0FBSyxFQUFFO0FBQUVELFVBQUFBLFFBQVEsRUFBRUEsUUFBUSxHQUFHO0FBQXZCO0FBQS9CLFFBQU47QUFDQU0sTUFBQUEsZUFBZSxHQUFHLEtBQWxCLENBRnVCLENBRUU7QUFDNUI7O0FBRUQsUUFBSSxLQUFLOUcsTUFBTCxNQUFpQixDQUFDVyx1QkFBY0MsUUFBZCxDQUF1Qix1QkFBdkIsQ0FBbEIsSUFBcUUsQ0FBQyxLQUFLWCxLQUFMLENBQVdRLEtBQXJGLEVBQTRGO0FBQ3hGa0csTUFBQUEsUUFBUSxHQUFHO0FBQUcsUUFBQSxTQUFTLEVBQUM7QUFBYixlQUFYO0FBQ0g7O0FBRUQsVUFBTUssU0FBUyxHQUNYO0FBQUssTUFBQSxTQUFTLEVBQUMsbUNBQWY7QUFBbUQsTUFBQSxLQUFLLEVBQUU7QUFBRVgsUUFBQUEsU0FBUyxFQUFFQSxTQUFTLEdBQUc7QUFBekI7QUFBMUQsT0FFSTtBQUFLLE1BQUEsS0FBSyxFQUFFO0FBQUVZLFFBQUFBLGFBQWEsRUFBRyxNQUFNaEIsVUFBTixHQUFtQkQsU0FBcEIsR0FBaUM7QUFBbEQ7QUFBWixNQUZKLEVBR01jLGVBQWUsSUFDYjtBQUFLLE1BQUEsU0FBUyxFQUFDLHlCQUFmO0FBQXlDLE1BQUEsS0FBSyxFQUFFO0FBQzVDO0FBQ0FOLFFBQUFBLFFBQVEsRUFBRVIsU0FBUyxHQUFHO0FBRnNCO0FBQWhELE9BSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ01VLFdBRE4sQ0FKSixDQUpSLEVBY0k7QUFBSyxNQUFBLEtBQUssRUFBRTtBQUFDUCxRQUFBQSxPQUFPLEVBQUUsQ0FBQ1csZUFBRCxHQUFtQnBELFNBQW5CLEdBQStCO0FBQXpDO0FBQVosT0FDTStDLEdBRE4sRUFFTUUsUUFGTixDQWRKLEVBbUJNLEtBQUsxRyxLQUFMLENBQVdRLEtBQVgsSUFBb0IsS0FBS3lHLFVBQUwsRUFuQjFCLENBREo7O0FBd0JBLFdBQU8sS0FBS2QsU0FBTCxDQUFlakIsVUFBZixFQUEyQjZCLFNBQTNCLENBQVA7QUFDSCxHQWhabUQsQ0FrWnBEOzs7QUFDQVosRUFBQUEsU0FBUyxDQUFDakIsVUFBRCxFQUFhZ0MsUUFBYixFQUF1QjtBQUM1QixXQUFPO0FBQUcsTUFBQSxJQUFJLEVBQUVoQyxVQUFUO0FBQXFCLE1BQUEsT0FBTyxFQUFFLEtBQUtwRjtBQUFuQyxPQUNGb0gsUUFERSxDQUFQO0FBR0gsR0F2Wm1ELENBeVpwRDs7O0FBQ0FOLEVBQUFBLGNBQWMsR0FBRztBQUNiO0FBQ0EsV0FBTyxJQUFQO0FBQ0gsR0E3Wm1ELENBK1pwRDs7O0FBQ0FLLEVBQUFBLFVBQVUsR0FBRztBQUNULFdBQU8sSUFBUDtBQUNILEdBbGFtRCxDQW9hcEQ7OztBQUNBRSxFQUFBQSxXQUFXLEdBQUc7QUFDVixXQUFPLDZCQUFDLGtCQUFELDZCQUFlLEtBQUs1SCxLQUFwQjtBQUEyQixNQUFBLGFBQWEsRUFBRSxLQUFLUyxLQUFMLENBQVdHO0FBQXJELE9BQVA7QUFDSDs7QUFFRGlILEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU0xRixPQUFPLEdBQUcsS0FBS25DLEtBQUwsQ0FBVzZCLE9BQVgsQ0FBbUJPLFVBQW5CLEVBQWhCOztBQUVBLFFBQUksS0FBSzNCLEtBQUwsQ0FBV0ksS0FBWCxLQUFxQixJQUF6QixFQUErQjtBQUMzQixhQUNJO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FDSTtBQUFLLFFBQUEsR0FBRyxFQUFFdUcsT0FBTyxDQUFDLGlDQUFELENBQWpCO0FBQXNELFFBQUEsS0FBSyxFQUFDLElBQTVEO0FBQWlFLFFBQUEsTUFBTSxFQUFDO0FBQXhFLFFBREosRUFFTSx5QkFBRyx3QkFBSCxDQUZOLENBREo7QUFNSDs7QUFFRCxVQUFNekIsVUFBVSxHQUFHLEtBQUtyRCxjQUFMLEVBQW5COztBQUNBLFFBQUlpRSxRQUFKOztBQUNBLFFBQUksS0FBSy9GLE1BQUwsTUFBaUJXLHVCQUFjQyxRQUFkLENBQXVCLHVCQUF2QixDQUFyQixFQUFzRTtBQUNwRW1GLE1BQUFBLFFBQVEsR0FBR1osVUFBWDtBQUNELEtBRkQsTUFFTztBQUNMWSxNQUFBQSxRQUFRLEdBQUcsS0FBSzNDLFlBQUwsRUFBWDtBQUNEOztBQUVELFVBQU00RCxTQUFTLEdBQUcsS0FBS2xCLGVBQUwsQ0FBcUJYLFVBQXJCLEVBQWlDWSxRQUFqQyxFQUEyQ3BFLE9BQTNDLENBQWxCOztBQUNBLFVBQU0yRixRQUFRLEdBQUcsS0FBS0YsV0FBTCxFQUFqQjtBQUVBLFdBQU87QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNESixTQURDLEVBRURNLFFBRkMsQ0FBUDtBQUlIOztBQXBjbUQ7Ozs4QkFBbkNsSSxVLGVBQ0U7QUFDZjtBQUNBaUMsRUFBQUEsT0FBTyxFQUFFa0csbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRlg7O0FBSWY7QUFDQXBFLEVBQUFBLGVBQWUsRUFBRWtFLG1CQUFVRyxJQUFWLENBQWVELFVBTGpCOztBQU9mO0FBQ0FsQixFQUFBQSxjQUFjLEVBQUVnQixtQkFBVUk7QUFSWCxDOzhCQURGdkksVSxpQkFZSXdJLDRCOztBQTJibEIsTUFBTUMsc0JBQU4sU0FBcUN4SSxlQUFNeUksYUFBM0MsQ0FBeUQ7QUFLNURULEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUlVLFNBQVMsR0FBRywyQkFBaEI7QUFDQSxRQUFJLEtBQUt2SSxLQUFMLENBQVdpQixLQUFmLEVBQXNCc0gsU0FBUyxJQUFJLGtDQUFiO0FBQ3RCLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBRUE7QUFBaEIsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE1BREosRUFFSSwyQ0FBTyx5QkFBRyxZQUFILENBQVAsQ0FGSixDQURKLENBREo7QUFRSDs7QUFoQjJEOzs7OEJBQW5ERixzQixlQUNVO0FBQ2ZwSCxFQUFBQSxLQUFLLEVBQUU4RyxtQkFBVVM7QUFERixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcblxyXG5pbXBvcnQgTUZpbGVCb2R5IGZyb20gJy4vTUZpbGVCb2R5JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgZGVjcnlwdEZpbGUgfSBmcm9tICcuLi8uLi8uLi91dGlscy9EZWNyeXB0RmlsZSc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1JbWFnZUJvZHkgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvKiB0aGUgTWF0cml4RXZlbnQgdG8gc2hvdyAqL1xyXG4gICAgICAgIG14RXZlbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLyogY2FsbGVkIHdoZW4gdGhlIGltYWdlIGhhcyBsb2FkZWQgKi9cclxuICAgICAgICBvbkhlaWdodENoYW5nZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcblxyXG4gICAgICAgIC8qIHRoZSBtYXhpbXVtIGltYWdlIGhlaWdodCB0byB1c2UgKi9cclxuICAgICAgICBtYXhJbWFnZUhlaWdodDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGNvbnRleHRUeXBlID0gTWF0cml4Q2xpZW50Q29udGV4dDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuXHJcbiAgICAgICAgdGhpcy5vbkltYWdlRXJyb3IgPSB0aGlzLm9uSW1hZ2VFcnJvci5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25JbWFnZUxvYWQgPSB0aGlzLm9uSW1hZ2VMb2FkLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vbkltYWdlRW50ZXIgPSB0aGlzLm9uSW1hZ2VFbnRlci5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25JbWFnZUxlYXZlID0gdGhpcy5vbkltYWdlTGVhdmUuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLm9uQ2xpZW50U3luYyA9IHRoaXMub25DbGllbnRTeW5jLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vbkNsaWNrID0gdGhpcy5vbkNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5faXNHaWYgPSB0aGlzLl9pc0dpZi5iaW5kKHRoaXMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBkZWNyeXB0ZWRVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIGRlY3J5cHRlZFRodW1ibmFpbFVybDogbnVsbCxcclxuICAgICAgICAgICAgZGVjcnlwdGVkQmxvYjogbnVsbCxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIGltZ0Vycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgaW1nTG9hZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgbG9hZGVkSW1hZ2VEaW1lbnNpb25zOiBudWxsLFxyXG4gICAgICAgICAgICBob3ZlcjogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dJbWFnZTogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dJbWFnZXNcIiksXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5faW1hZ2UgPSBjcmVhdGVSZWYoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBGSVhNRTogZmFjdG9yIHRoaXMgb3V0IGFuZCBhcGxweSBpdCB0byBNVmlkZW9Cb2R5IGFuZCBNQXVkaW9Cb2R5IHRvbyFcclxuICAgIG9uQ2xpZW50U3luYyhzeW5jU3RhdGUsIHByZXZTdGF0ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgIC8vIENvbnNpZGVyIHRoZSBjbGllbnQgcmVjb25uZWN0ZWQgaWYgdGhlcmUgaXMgbm8gZXJyb3Igd2l0aCBzeW5jaW5nLlxyXG4gICAgICAgIC8vIFRoaXMgbWVhbnMgdGhlIHN0YXRlIGNvdWxkIGJlIFJFQ09OTkVDVElORywgU1lOQ0lORywgUFJFUEFSRUQgb3IgQ0FUQ0hVUC5cclxuICAgICAgICBjb25zdCByZWNvbm5lY3RlZCA9IHN5bmNTdGF0ZSAhPT0gXCJFUlJPUlwiICYmIHByZXZTdGF0ZSAhPT0gc3luY1N0YXRlO1xyXG4gICAgICAgIGlmIChyZWNvbm5lY3RlZCAmJiB0aGlzLnN0YXRlLmltZ0Vycm9yKSB7XHJcbiAgICAgICAgICAgIC8vIExvYWQgdGhlIGltYWdlIGFnYWluXHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgaW1nRXJyb3I6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0ltYWdlKCkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibXhfU2hvd0ltYWdlX1wiICsgdGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCksIFwidHJ1ZVwiKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtzaG93SW1hZ2U6IHRydWV9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrKGV2KSB7XHJcbiAgICAgICAgaWYgKGV2LmJ1dHRvbiA9PT0gMCAmJiAhZXYubWV0YUtleSkge1xyXG4gICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2hvd0ltYWdlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dJbWFnZSgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICAgICAgY29uc3QgaHR0cFVybCA9IHRoaXMuX2dldENvbnRlbnRVcmwoKTtcclxuICAgICAgICAgICAgY29uc3QgSW1hZ2VWaWV3ID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkltYWdlVmlld1wiKTtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgc3JjOiBodHRwVXJsLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogY29udGVudC5ib2R5ICYmIGNvbnRlbnQuYm9keS5sZW5ndGggPiAwID8gY29udGVudC5ib2R5IDogX3QoJ0F0dGFjaG1lbnQnKSxcclxuICAgICAgICAgICAgICAgIG14RXZlbnQ6IHRoaXMucHJvcHMubXhFdmVudCxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChjb250ZW50LmluZm8pIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtcy53aWR0aCA9IGNvbnRlbnQuaW5mby53O1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zLmhlaWdodCA9IGNvbnRlbnQuaW5mby5oO1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zLmZpbGVTaXplID0gY29udGVudC5pbmZvLnNpemU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZURpYWxvZyhJbWFnZVZpZXcsIHBhcmFtcywgXCJteF9EaWFsb2dfbGlnaHRib3hcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9pc0dpZigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgY29udGVudCAmJlxyXG4gICAgICAgICAgY29udGVudC5pbmZvICYmXHJcbiAgICAgICAgICBjb250ZW50LmluZm8ubWltZXR5cGUgPT09IFwiaW1hZ2UvZ2lmXCJcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIG9uSW1hZ2VFbnRlcihlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2hvd0ltYWdlIHx8ICF0aGlzLl9pc0dpZigpIHx8IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJhdXRvcGxheUdpZnNBbmRWaWRlb3NcIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpbWdFbGVtZW50ID0gZS50YXJnZXQ7XHJcbiAgICAgICAgaW1nRWxlbWVudC5zcmMgPSB0aGlzLl9nZXRDb250ZW50VXJsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25JbWFnZUxlYXZlKGUpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuc2hvd0ltYWdlIHx8ICF0aGlzLl9pc0dpZigpIHx8IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJhdXRvcGxheUdpZnNBbmRWaWRlb3NcIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpbWdFbGVtZW50ID0gZS50YXJnZXQ7XHJcbiAgICAgICAgaW1nRWxlbWVudC5zcmMgPSB0aGlzLl9nZXRUaHVtYlVybCgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uSW1hZ2VFcnJvcigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgaW1nRXJyb3I6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25JbWFnZUxvYWQoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWQoKTtcclxuXHJcbiAgICAgICAgbGV0IGxvYWRlZEltYWdlRGltZW5zaW9ucztcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX2ltYWdlLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgY29uc3QgeyBuYXR1cmFsV2lkdGgsIG5hdHVyYWxIZWlnaHQgfSA9IHRoaXMuX2ltYWdlLmN1cnJlbnQ7XHJcbiAgICAgICAgICAgIC8vIHRoaXMgaXMgb25seSB1c2VkIGFzIGEgZmFsbGJhY2sgaW4gY2FzZSBjb250ZW50LmluZm8udy9oIGlzIG1pc3NpbmdcclxuICAgICAgICAgICAgbG9hZGVkSW1hZ2VEaW1lbnNpb25zID0geyBuYXR1cmFsV2lkdGgsIG5hdHVyYWxIZWlnaHQgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBpbWdMb2FkZWQ6IHRydWUsIGxvYWRlZEltYWdlRGltZW5zaW9ucyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfZ2V0Q29udGVudFVybCgpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoY29udGVudC5maWxlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnRleHQubXhjVXJsVG9IdHRwKGNvbnRlbnQudXJsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX2dldFRodW1iVXJsKCkge1xyXG4gICAgICAgIC8vIEZJWE1FOiB0aGUgZGhhcm1hIHNraW4gbGV0cyBpbWFnZXMgZ3JvdyBhcyB3aWRlIGFzIHlvdSBsaWtlLCByYXRoZXIgdGhhbiBjYXBwZWQgdG8gODAweDYwMC5cclxuICAgICAgICAvLyBTbyBlaXRoZXIgd2UgbmVlZCB0byBzdXBwb3J0IGN1c3RvbSB0aW1lbGluZSB3aWR0aHMgaGVyZSwgb3IgcmVpbXBvc2UgdGhlIGNhcCwgb3RoZXJ3aXNlIHRoZVxyXG4gICAgICAgIC8vIHRodW1ibmFpbCByZXNvbHV0aW9uIHdpbGwgYmUgdW5uZWNlc3NhcmlseSByZWR1Y2VkLlxyXG4gICAgICAgIC8vIGN1c3RvbSB0aW1lbGluZSB3aWR0aHMgc2VlbXMgcHJlZmVyYWJsZS5cclxuICAgICAgICBjb25zdCBwaXhlbFJhdGlvID0gd2luZG93LmRldmljZVBpeGVsUmF0aW87XHJcbiAgICAgICAgY29uc3QgdGh1bWJXaWR0aCA9IE1hdGgucm91bmQoODAwICogcGl4ZWxSYXRpbyk7XHJcbiAgICAgICAgY29uc3QgdGh1bWJIZWlnaHQgPSBNYXRoLnJvdW5kKDYwMCAqIHBpeGVsUmF0aW8pO1xyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoY29udGVudC5maWxlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgLy8gRG9uJ3QgdXNlIHRoZSB0aHVtYm5haWwgZm9yIGNsaWVudHMgd2lzaGluZyB0byBhdXRvcGxheSBnaWZzLlxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5kZWNyeXB0ZWRUaHVtYm5haWxVcmwpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnN0YXRlLmRlY3J5cHRlZFRodW1ibmFpbFVybDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChjb250ZW50LmluZm8gJiYgY29udGVudC5pbmZvLm1pbWV0eXBlID09PSBcImltYWdlL3N2Zyt4bWxcIiAmJiBjb250ZW50LmluZm8udGh1bWJuYWlsX3VybCkge1xyXG4gICAgICAgICAgICAvLyBzcGVjaWFsIGNhc2UgdG8gcmV0dXJuIGNsaWVudHNpZGUgc2VuZGVyLWdlbmVyYXRlZCB0aHVtYm5haWxzIGZvciBTVkdzLCBpZiBhbnksXHJcbiAgICAgICAgICAgIC8vIGdpdmVuIHdlIGRlbGliZXJhdGVseSBkb24ndCB0aHVtYm5haWwgdGhlbSBzZXJ2ZXJzaWRlIHRvIHByZXZlbnRcclxuICAgICAgICAgICAgLy8gYmlsbGlvbiBsb2wgYXR0YWNrcyBhbmQgc2ltaWxhclxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb250ZXh0Lm14Y1VybFRvSHR0cChcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQuaW5mby50aHVtYm5haWxfdXJsLFxyXG4gICAgICAgICAgICAgICAgdGh1bWJXaWR0aCxcclxuICAgICAgICAgICAgICAgIHRodW1iSGVpZ2h0LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHdlIHRyeSB0byBkb3dubG9hZCB0aGUgY29ycmVjdCByZXNvbHV0aW9uXHJcbiAgICAgICAgICAgIC8vIGZvciBoaS1yZXMgaW1hZ2VzIChsaWtlIHJldGluYSBzY3JlZW5zaG90cykuXHJcbiAgICAgICAgICAgIC8vIHN5bmFwc2Ugb25seSBzdXBwb3J0cyA4MDB4NjAwIHRodW1ibmFpbHMgZm9yIG5vdyB0aG91Z2gsXHJcbiAgICAgICAgICAgIC8vIHNvIHdlJ2xsIG5lZWQgdG8gZG93bmxvYWQgdGhlIG9yaWdpbmFsIGltYWdlIGZvciB0aGlzIHRvIHdvcmtcclxuICAgICAgICAgICAgLy8gd2VsbCBmb3Igbm93LiBGaXJzdCwgbGV0J3MgdHJ5IGEgZmV3IGNhc2VzIHRoYXQgbGV0IHVzIGF2b2lkXHJcbiAgICAgICAgICAgIC8vIGRvd25sb2FkaW5nIHRoZSBvcmlnaW5hbCwgaW5jbHVkaW5nOlxyXG4gICAgICAgICAgICAvLyAgIC0gV2hlbiBkaXNwbGF5aW5nIGEgR0lGLCB3ZSBhbHdheXMgd2FudCB0byB0aHVtYm5haWwgc28gdGhhdCB3ZSBjYW5cclxuICAgICAgICAgICAgLy8gICAgIHByb3Blcmx5IHJlc3BlY3QgdGhlIHVzZXIncyBHSUYgYXV0b3BsYXkgc2V0dGluZyAod2hpY2ggcmVsaWVzIG9uXHJcbiAgICAgICAgICAgIC8vICAgICB0aHVtYm5haWxpbmcgdG8gcHJvZHVjZSB0aGUgc3RhdGljIHByZXZpZXcgaW1hZ2UpXHJcbiAgICAgICAgICAgIC8vICAgLSBPbiBhIGxvdyBEUEkgZGV2aWNlLCBhbHdheXMgdGh1bWJuYWlsIHRvIHNhdmUgYmFuZHdpZHRoXHJcbiAgICAgICAgICAgIC8vICAgLSBJZiB0aGVyZSdzIG5vIHNpemluZyBpbmZvIGluIHRoZSBldmVudCwgZGVmYXVsdCB0byB0aHVtYm5haWxcclxuICAgICAgICAgICAgY29uc3QgaW5mbyA9IGNvbnRlbnQuaW5mbztcclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5faXNHaWYoKSB8fFxyXG4gICAgICAgICAgICAgICAgcGl4ZWxSYXRpbyA9PT0gMS4wIHx8XHJcbiAgICAgICAgICAgICAgICAoIWluZm8gfHwgIWluZm8udyB8fCAhaW5mby5oIHx8ICFpbmZvLnNpemUpXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29udGV4dC5teGNVcmxUb0h0dHAoY29udGVudC51cmwsIHRodW1iV2lkdGgsIHRodW1iSGVpZ2h0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIHdlIHNob3VsZCBvbmx5IHJlcXVlc3QgdGh1bWJuYWlscyBpZiB0aGUgaW1hZ2UgaXMgYmlnZ2VyIHRoYW4gODAweDYwMFxyXG4gICAgICAgICAgICAgICAgLy8gKG9yIDE2MDB4MTIwMCBvbiByZXRpbmEpIG90aGVyd2lzZSB0aGUgaW1hZ2UgaW4gdGhlIHRpbWVsaW5lIHdpbGwganVzdFxyXG4gICAgICAgICAgICAgICAgLy8gZW5kIHVwIHJlc2FtcGxlZCBhbmQgZGUtcmV0aW5hJ2QgZm9yIG5vIGdvb2QgcmVhc29uLlxyXG4gICAgICAgICAgICAgICAgLy8gSWRlYWxseSB0aGUgc2VydmVyIHdvdWxkIHByZWdlbiAxNjAweDEyMDAgdGh1bWJuYWlscyBpbiBvcmRlciB0byBwcm92aWRlIHJldGluYVxyXG4gICAgICAgICAgICAgICAgLy8gdGh1bWJuYWlscywgYnV0IHdlIGRvbid0IGRvIHRoaXMgY3VycmVudGx5IGluIHN5bmFwc2UgZm9yIGZlYXIgb2YgZGlzayBzcGFjZS5cclxuICAgICAgICAgICAgICAgIC8vIEFzIGEgY29tcHJvbWlzZSwgbGV0J3Mgc3dpdGNoIHRvIG5vbi1yZXRpbmEgdGh1bWJuYWlscyBvbmx5IGlmIHRoZSBvcmlnaW5hbFxyXG4gICAgICAgICAgICAgICAgLy8gaW1hZ2UgaXMgYm90aCBwaHlzaWNhbGx5IHRvbyBsYXJnZSBhbmQgZ29pbmcgdG8gYmUgbWFzc2l2ZSB0byBsb2FkIGluIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gdGltZWxpbmUgKGUuZy4gPjFNQikuXHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgaXNMYXJnZXJUaGFuVGh1bWJuYWlsID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIGluZm8udyA+IHRodW1iV2lkdGggfHxcclxuICAgICAgICAgICAgICAgICAgICBpbmZvLmggPiB0aHVtYkhlaWdodFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlzTGFyZ2VGaWxlU2l6ZSA9IGluZm8uc2l6ZSA+IDEqMTAyNCoxMDI0O1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpc0xhcmdlRmlsZVNpemUgJiYgaXNMYXJnZXJUaGFuVGh1bWJuYWlsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaW1hZ2UgaXMgdG9vIGxhcmdlIHBoeXNpY2FsbHkgYW5kIGJ5dGV3aXNlIHRvIGNsdXR0ZXIgb3VyIHRpbWVsaW5lIHNvXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgYXNrIGZvciBhIHRodW1ibmFpbCwgZGVzcGl0ZSBrbm93aW5nIHRoYXQgaXQgd2lsbCBiZSBtYXggODAweDYwMFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGRlc3BpdGUgdXMgYmVpbmcgcmV0aW5hIChhcyBzeW5hcHNlIGRvZXNuJ3QgZG8gMTYwMHgxMjAwIHRodW1icyB5ZXQpLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnRleHQubXhjVXJsVG9IdHRwKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50LnVybCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGh1bWJXaWR0aCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGh1bWJIZWlnaHQsXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZG93bmxvYWQgdGhlIG9yaWdpbmFsIGltYWdlIG90aGVyd2lzZSwgc28gd2UgY2FuIHNjYWxlIGl0IGNsaWVudCBzaWRlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdG8gdGFrZSBwaXhlbFJhdGlvIGludG8gYWNjb3VudC5cclxuICAgICAgICAgICAgICAgICAgICAvLyAoIG5vIHdpZHRoL2hlaWdodCBtZWFucyB3ZSB3YW50IHRoZSBvcmlnaW5hbCBpbWFnZSlcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5jb250ZXh0Lm14Y1VybFRvSHR0cChcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudC51cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICB0aGlzLnVubW91bnRlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5vbignc3luYycsIHRoaXMub25DbGllbnRTeW5jKTtcclxuXHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IHRoaXMucHJvcHMubXhFdmVudC5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgaWYgKGNvbnRlbnQuZmlsZSAhPT0gdW5kZWZpbmVkICYmIHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGxldCB0aHVtYm5haWxQcm9taXNlID0gUHJvbWlzZS5yZXNvbHZlKG51bGwpO1xyXG4gICAgICAgICAgICBpZiAoY29udGVudC5pbmZvICYmIGNvbnRlbnQuaW5mby50aHVtYm5haWxfZmlsZSkge1xyXG4gICAgICAgICAgICAgICAgdGh1bWJuYWlsUHJvbWlzZSA9IGRlY3J5cHRGaWxlKFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQuaW5mby50aHVtYm5haWxfZmlsZSxcclxuICAgICAgICAgICAgICAgICkudGhlbihmdW5jdGlvbihibG9iKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsZXQgZGVjcnlwdGVkQmxvYjtcclxuICAgICAgICAgICAgdGh1bWJuYWlsUHJvbWlzZS50aGVuKCh0aHVtYm5haWxVcmwpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWNyeXB0RmlsZShjb250ZW50LmZpbGUpLnRoZW4oZnVuY3Rpb24oYmxvYikge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZEJsb2IgPSBibG9iO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBVUkwuY3JlYXRlT2JqZWN0VVJMKGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgfSkudGhlbigoY29udGVudFVybCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWNyeXB0ZWRVcmw6IGNvbnRlbnRVcmwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZFRodW1ibmFpbFVybDogdGh1bWJuYWlsVXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWNyeXB0ZWRCbG9iOiBkZWNyeXB0ZWRCbG9iLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS53YXJuKFwiVW5hYmxlIHRvIGRlY3J5cHQgYXR0YWNobWVudDogXCIsIGVycik7XHJcbiAgICAgICAgICAgICAgICAvLyBTZXQgYSBwbGFjZWhvbGRlciBpbWFnZSB3aGVuIHdlIGNhbid0IGRlY3J5cHQgdGhlIGltYWdlLlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGVycixcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFJlbWVtYmVyIHRoYXQgdGhlIHVzZXIgd2FudGVkIHRvIHNob3cgdGhpcyBwYXJ0aWN1bGFyIGltYWdlXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNob3dJbWFnZSAmJiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIm14X1Nob3dJbWFnZV9cIiArIHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpKSA9PT0gXCJ0cnVlXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2hvd0ltYWdlOiB0cnVlfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9hZnRlckNvbXBvbmVudERpZE1vdW50KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVG8gYmUgb3ZlcnJpZGRlbiBieSBzdWJjbGFzc2VzIChlLmcuIE1TdGlja2VyQm9keSkgZm9yIGZ1cnRoZXJcclxuICAgIC8vIGluaXRpYWxpc2F0aW9uIGFmdGVyIGNvbXBvbmVudERpZE1vdW50XHJcbiAgICBfYWZ0ZXJDb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICB0aGlzLnVubW91bnRlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0LnJlbW92ZUxpc3RlbmVyKCdzeW5jJywgdGhpcy5vbkNsaWVudFN5bmMpO1xyXG4gICAgICAgIHRoaXMuX2FmdGVyQ29tcG9uZW50V2lsbFVubW91bnQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsKSB7XHJcbiAgICAgICAgICAgIFVSTC5yZXZva2VPYmplY3RVUkwodGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5kZWNyeXB0ZWRUaHVtYm5haWxVcmwpIHtcclxuICAgICAgICAgICAgVVJMLnJldm9rZU9iamVjdFVSTCh0aGlzLnN0YXRlLmRlY3J5cHRlZFRodW1ibmFpbFVybCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFRvIGJlIG92ZXJyaWRkZW4gYnkgc3ViY2xhc3NlcyAoZS5nLiBNU3RpY2tlckJvZHkpIGZvciBmdXJ0aGVyXHJcbiAgICAvLyBjbGVhbnVwIGFmdGVyIGNvbXBvbmVudFdpbGxVbm1vdW50XHJcbiAgICBfYWZ0ZXJDb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgIH1cclxuXHJcbiAgICBfbWVzc2FnZUNvbnRlbnQoY29udGVudFVybCwgdGh1bWJVcmwsIGNvbnRlbnQpIHtcclxuICAgICAgICBsZXQgaW5mb1dpZHRoO1xyXG4gICAgICAgIGxldCBpbmZvSGVpZ2h0O1xyXG5cclxuICAgICAgICBpZiAoY29udGVudCAmJiBjb250ZW50LmluZm8gJiYgY29udGVudC5pbmZvLncgJiYgY29udGVudC5pbmZvLmgpIHtcclxuICAgICAgICAgICAgaW5mb1dpZHRoID0gY29udGVudC5pbmZvLnc7XHJcbiAgICAgICAgICAgIGluZm9IZWlnaHQgPSBjb250ZW50LmluZm8uaDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBXaGlsc3QgdGhlIGltYWdlIGxvYWRzLCBkaXNwbGF5IG5vdGhpbmcuXHJcbiAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgIC8vIE9uY2UgbG9hZGVkLCB1c2UgdGhlIGxvYWRlZCBpbWFnZSBkaW1lbnNpb25zIHN0b3JlZCBpbiBgbG9hZGVkSW1hZ2VEaW1lbnNpb25zYC5cclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gQnkgZG9pbmcgdGhpcywgdGhlIGltYWdlIFwicG9wc1wiIGludG8gdGhlIHRpbWVsaW5lLCBidXQgaXMgc3RpbGwgcmVzdHJpY3RlZFxyXG4gICAgICAgICAgICAvLyBieSB0aGUgc2FtZSB3aWR0aCBhbmQgaGVpZ2h0IGxvZ2ljIGJlbG93LlxyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUubG9hZGVkSW1hZ2VEaW1lbnNpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW1hZ2VFbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNob3dJbWFnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlRWxlbWVudCA9IDxIaWRkZW5JbWFnZVBsYWNlaG9sZGVyIC8+O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpbWFnZUVsZW1lbnQgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3R5bGU9e3tkaXNwbGF5OiAnbm9uZSd9fSBzcmM9e3RodW1iVXJsfSByZWY9e3RoaXMuX2ltYWdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17Y29udGVudC5ib2R5fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRXJyb3I9e3RoaXMub25JbWFnZUVycm9yfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uTG9hZD17dGhpcy5vbkltYWdlTG9hZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMud3JhcEltYWdlKGNvbnRlbnRVcmwsIGltYWdlRWxlbWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaW5mb1dpZHRoID0gdGhpcy5zdGF0ZS5sb2FkZWRJbWFnZURpbWVuc2lvbnMubmF0dXJhbFdpZHRoO1xyXG4gICAgICAgICAgICBpbmZvSGVpZ2h0ID0gdGhpcy5zdGF0ZS5sb2FkZWRJbWFnZURpbWVuc2lvbnMubmF0dXJhbEhlaWdodDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRoZSBtYXhpbXVtIGhlaWdodCBvZiB0aGUgdGh1bWJuYWlsIGFzIGl0IGlzIHJlbmRlcmVkIGFzIGFuIDxpbWc+XHJcbiAgICAgICAgY29uc3QgbWF4SGVpZ2h0ID0gTWF0aC5taW4odGhpcy5wcm9wcy5tYXhJbWFnZUhlaWdodCB8fCA2MDAsIGluZm9IZWlnaHQpO1xyXG4gICAgICAgIC8vIFRoZSBtYXhpbXVtIHdpZHRoIG9mIHRoZSB0aHVtYm5haWwsIGFzIGRpY3RhdGVkIGJ5IGl0cyBuYXR1cmFsXHJcbiAgICAgICAgLy8gbWF4aW11bSBoZWlnaHQuXHJcbiAgICAgICAgY29uc3QgbWF4V2lkdGggPSBpbmZvV2lkdGggKiBtYXhIZWlnaHQgLyBpbmZvSGVpZ2h0O1xyXG5cclxuICAgICAgICBsZXQgaW1nID0gbnVsbDtcclxuICAgICAgICBsZXQgcGxhY2Vob2xkZXIgPSBudWxsO1xyXG4gICAgICAgIGxldCBnaWZMYWJlbCA9IG51bGw7XHJcblxyXG4gICAgICAgIC8vIGUyZSBpbWFnZSBoYXNuJ3QgYmVlbiBkZWNyeXB0ZWQgeWV0XHJcbiAgICAgICAgaWYgKGNvbnRlbnQuZmlsZSAhPT0gdW5kZWZpbmVkICYmIHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gPGltZ1xyXG4gICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9zcGlubmVyLmdpZlwiKX1cclxuICAgICAgICAgICAgICAgIGFsdD17Y29udGVudC5ib2R5fVxyXG4gICAgICAgICAgICAgICAgd2lkdGg9XCIzMlwiXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ9XCIzMlwiXHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuc3RhdGUuaW1nTG9hZGVkKSB7XHJcbiAgICAgICAgICAgIC8vIERlbGliZXJhdGVseSwgZ2V0U3Bpbm5lciBpcyBsZWZ0IHVuaW1wbGVtZW50ZWQgaGVyZSwgTVN0aWNrZXJCb2R5IG92ZXJpZGVzXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gdGhpcy5nZXRQbGFjZWhvbGRlcigpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHNob3dQbGFjZWhvbGRlciA9IEJvb2xlYW4ocGxhY2Vob2xkZXIpO1xyXG5cclxuICAgICAgICBpZiAodGh1bWJVcmwgJiYgIXRoaXMuc3RhdGUuaW1nRXJyb3IpIHtcclxuICAgICAgICAgICAgLy8gUmVzdHJpY3QgdGhlIHdpZHRoIG9mIHRoZSB0aHVtYm5haWwgaGVyZSwgb3RoZXJ3aXNlIGl0IHdpbGwgZmlsbCB0aGUgY29udGFpbmVyXHJcbiAgICAgICAgICAgIC8vIHdoaWNoIGhhcyB0aGUgc2FtZSB3aWR0aCBhcyB0aGUgdGltZWxpbmVcclxuICAgICAgICAgICAgLy8gbXhfTUltYWdlQm9keV90aHVtYm5haWwgcmVzaXplcyBpbWcgdG8gZXhhY3RseSBjb250YWluZXIgc2l6ZVxyXG4gICAgICAgICAgICBpbWcgPSAoXHJcbiAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X01JbWFnZUJvZHlfdGh1bWJuYWlsXCIgc3JjPXt0aHVtYlVybH0gcmVmPXt0aGlzLl9pbWFnZX1cclxuICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgbWF4V2lkdGg6IG1heFdpZHRoICsgXCJweFwiIH19XHJcbiAgICAgICAgICAgICAgICAgICAgIGFsdD17Y29udGVudC5ib2R5fVxyXG4gICAgICAgICAgICAgICAgICAgICBvbkVycm9yPXt0aGlzLm9uSW1hZ2VFcnJvcn1cclxuICAgICAgICAgICAgICAgICAgICAgb25Mb2FkPXt0aGlzLm9uSW1hZ2VMb2FkfVxyXG4gICAgICAgICAgICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25JbWFnZUVudGVyfVxyXG4gICAgICAgICAgICAgICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25JbWFnZUxlYXZlfSAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNob3dJbWFnZSkge1xyXG4gICAgICAgICAgICBpbWcgPSA8SGlkZGVuSW1hZ2VQbGFjZWhvbGRlciBzdHlsZT17eyBtYXhXaWR0aDogbWF4V2lkdGggKyBcInB4XCIgfX0gLz47XHJcbiAgICAgICAgICAgIHNob3dQbGFjZWhvbGRlciA9IGZhbHNlOyAvLyBiZWNhdXNlIHdlJ3JlIGhpZGluZyB0aGUgaW1hZ2UsIHNvIGRvbid0IHNob3cgdGhlIHN0aWNrZXIgaWNvbi5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9pc0dpZigpICYmICFTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiYXV0b3BsYXlHaWZzQW5kVmlkZW9zXCIpICYmICF0aGlzLnN0YXRlLmhvdmVyKSB7XHJcbiAgICAgICAgICAgIGdpZkxhYmVsID0gPHAgY2xhc3NOYW1lPVwibXhfTUltYWdlQm9keV9naWZMYWJlbFwiPkdJRjwvcD47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0aHVtYm5haWwgPSAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTUltYWdlQm9keV90aHVtYm5haWxfY29udGFpbmVyXCIgc3R5bGU9e3sgbWF4SGVpZ2h0OiBtYXhIZWlnaHQgKyBcInB4XCIgfX0gPlxyXG4gICAgICAgICAgICAgICAgeyAvKiBDYWxjdWxhdGUgYXNwZWN0IHJhdGlvLCB1c2luZyAlcGFkZGluZyB3aWxsIHNpemUgX2NvbnRhaW5lciBjb3JyZWN0bHkgKi8gfVxyXG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBwYWRkaW5nQm90dG9tOiAoMTAwICogaW5mb0hlaWdodCAvIGluZm9XaWR0aCkgKyAnJScgfX0gLz5cclxuICAgICAgICAgICAgICAgIHsgc2hvd1BsYWNlaG9sZGVyICYmXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NSW1hZ2VCb2R5X3RodW1ibmFpbFwiIHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIENvbnN0cmFpbiB3aWR0aCBoZXJlIHNvIHRoYXQgc3Bpbm5lciBhcHBlYXJzIGNlbnRyYWwgdG8gdGhlIGxvYWRlZCB0aHVtYm5haWxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4V2lkdGg6IGluZm9XaWR0aCArIFwicHhcIixcclxuICAgICAgICAgICAgICAgICAgICB9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NSW1hZ2VCb2R5X3RodW1ibmFpbF9zcGlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHBsYWNlaG9sZGVyIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e2Rpc3BsYXk6ICFzaG93UGxhY2Vob2xkZXIgPyB1bmRlZmluZWQgOiAnbm9uZSd9fT5cclxuICAgICAgICAgICAgICAgICAgICB7IGltZyB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBnaWZMYWJlbCB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUuaG92ZXIgJiYgdGhpcy5nZXRUb29sdGlwKCkgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSW1hZ2UoY29udGVudFVybCwgdGh1bWJuYWlsKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBPdmVyaWRkZW4gYnkgTVN0aWNrZXJCb2R5XHJcbiAgICB3cmFwSW1hZ2UoY29udGVudFVybCwgY2hpbGRyZW4pIHtcclxuICAgICAgICByZXR1cm4gPGEgaHJlZj17Y29udGVudFVybH0gb25DbGljaz17dGhpcy5vbkNsaWNrfT5cclxuICAgICAgICAgICAge2NoaWxkcmVufVxyXG4gICAgICAgIDwvYT47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gT3ZlcmlkZGVuIGJ5IE1TdGlja2VyQm9keVxyXG4gICAgZ2V0UGxhY2Vob2xkZXIoKSB7XHJcbiAgICAgICAgLy8gTUltYWdlQm9keSBkb2Vzbid0IHNob3cgYSBwbGFjZWhvbGRlciB3aGlsc3QgdGhlIGltYWdlIGxvYWRzLCAoYnV0IGl0IGNvdWxkIGRvKVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE92ZXJpZGRlbiBieSBNU3RpY2tlckJvZHlcclxuICAgIGdldFRvb2x0aXAoKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gT3ZlcmlkZGVuIGJ5IE1TdGlja2VyQm9keVxyXG4gICAgZ2V0RmlsZUJvZHkoKSB7XHJcbiAgICAgICAgcmV0dXJuIDxNRmlsZUJvZHkgey4uLnRoaXMucHJvcHN9IGRlY3J5cHRlZEJsb2I9e3RoaXMuc3RhdGUuZGVjcnlwdGVkQmxvYn0gLz47XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0Q29udGVudCgpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvciAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfTUltYWdlQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy93YXJuaW5nLnN2Z1wiKX0gd2lkdGg9XCIxNlwiIGhlaWdodD1cIjE2XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KFwiRXJyb3IgZGVjcnlwdGluZyBpbWFnZVwiKSB9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50VXJsID0gdGhpcy5fZ2V0Q29udGVudFVybCgpO1xyXG4gICAgICAgIGxldCB0aHVtYlVybDtcclxuICAgICAgICBpZiAodGhpcy5faXNHaWYoKSAmJiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiYXV0b3BsYXlHaWZzQW5kVmlkZW9zXCIpKSB7XHJcbiAgICAgICAgICB0aHVtYlVybCA9IGNvbnRlbnRVcmw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRodW1iVXJsID0gdGhpcy5fZ2V0VGh1bWJVcmwoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHRodW1ibmFpbCA9IHRoaXMuX21lc3NhZ2VDb250ZW50KGNvbnRlbnRVcmwsIHRodW1iVXJsLCBjb250ZW50KTtcclxuICAgICAgICBjb25zdCBmaWxlQm9keSA9IHRoaXMuZ2V0RmlsZUJvZHkoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxzcGFuIGNsYXNzTmFtZT1cIm14X01JbWFnZUJvZHlcIj5cclxuICAgICAgICAgICAgeyB0aHVtYm5haWwgfVxyXG4gICAgICAgICAgICB7IGZpbGVCb2R5IH1cclxuICAgICAgICA8L3NwYW4+O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgSGlkZGVuSW1hZ2VQbGFjZWhvbGRlciBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBob3ZlcjogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgY2xhc3NOYW1lID0gJ214X0hpZGRlbkltYWdlUGxhY2Vob2xkZXInO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmhvdmVyKSBjbGFzc05hbWUgKz0gJyBteF9IaWRkZW5JbWFnZVBsYWNlaG9sZGVyX2hvdmVyJztcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lfT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9IaWRkZW5JbWFnZVBsYWNlaG9sZGVyX2J1dHRvbic+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdteF9IaWRkZW5JbWFnZVBsYWNlaG9sZGVyX2V5ZScgLz5cclxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57X3QoXCJTaG93IGltYWdlXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==