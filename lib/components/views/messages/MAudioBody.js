/*
 Copyright 2016 OpenMarket Ltd

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireDefault(require("react"));

var _MFileBody = _interopRequireDefault(require("./MFileBody"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _DecryptFile = require("../../../utils/DecryptFile");

var _languageHandler = require("../../../languageHandler");

class MAudioBody extends _react.default.Component {
  constructor(props) {
    super(props);
    this.state = {
      playing: false,
      decryptedUrl: null,
      decryptedBlob: null,
      error: null
    };
  }

  onPlayToggle() {
    this.setState({
      playing: !this.state.playing
    });
  }

  _getContentUrl() {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined) {
      return this.state.decryptedUrl;
    } else {
      return _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(content.url);
    }
  }

  componentDidMount() {
    const content = this.props.mxEvent.getContent();

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      let decryptedBlob;
      (0, _DecryptFile.decryptFile)(content.file).then(function (blob) {
        decryptedBlob = blob;
        return URL.createObjectURL(decryptedBlob);
      }).then(url => {
        this.setState({
          decryptedUrl: url,
          decryptedBlob: decryptedBlob
        });
      }, err => {
        console.warn("Unable to decrypt attachment: ", err);
        this.setState({
          error: err
        });
      });
    }
  }

  componentWillUnmount() {
    if (this.state.decryptedUrl) {
      URL.revokeObjectURL(this.state.decryptedUrl);
    }
  }

  render() {
    const content = this.props.mxEvent.getContent();

    if (this.state.error !== null) {
      return _react.default.createElement("span", {
        className: "mx_MAudioBody"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/warning.svg"),
        width: "16",
        height: "16"
      }), (0, _languageHandler._t)("Error decrypting audio"));
    }

    if (content.file !== undefined && this.state.decryptedUrl === null) {
      // Need to decrypt the attachment
      // The attachment is decrypted in componentDidMount.
      // For now add an img tag with a 16x16 spinner.
      // Not sure how tall the audio player is so not sure how tall it should actually be.
      return _react.default.createElement("span", {
        className: "mx_MAudioBody"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/spinner.gif"),
        alt: content.body,
        width: "16",
        height: "16"
      }));
    }

    const contentUrl = this._getContentUrl();

    return _react.default.createElement("span", {
      className: "mx_MAudioBody"
    }, _react.default.createElement("audio", {
      src: contentUrl,
      controls: true
    }), _react.default.createElement(_MFileBody.default, (0, _extends2.default)({}, this.props, {
      decryptedBlob: this.state.decryptedBlob
    })));
  }

}

exports.default = MAudioBody;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL21lc3NhZ2VzL01BdWRpb0JvZHkuanMiXSwibmFtZXMiOlsiTUF1ZGlvQm9keSIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInN0YXRlIiwicGxheWluZyIsImRlY3J5cHRlZFVybCIsImRlY3J5cHRlZEJsb2IiLCJlcnJvciIsIm9uUGxheVRvZ2dsZSIsInNldFN0YXRlIiwiX2dldENvbnRlbnRVcmwiLCJjb250ZW50IiwibXhFdmVudCIsImdldENvbnRlbnQiLCJmaWxlIiwidW5kZWZpbmVkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwibXhjVXJsVG9IdHRwIiwidXJsIiwiY29tcG9uZW50RGlkTW91bnQiLCJ0aGVuIiwiYmxvYiIsIlVSTCIsImNyZWF0ZU9iamVjdFVSTCIsImVyciIsImNvbnNvbGUiLCJ3YXJuIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZXZva2VPYmplY3RVUkwiLCJyZW5kZXIiLCJyZXF1aXJlIiwiYm9keSIsImNvbnRlbnRVcmwiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7Ozs7Ozs7Ozs7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBRWUsTUFBTUEsVUFBTixTQUF5QkMsZUFBTUMsU0FBL0IsQ0FBeUM7QUFDcERDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUtDLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxPQUFPLEVBQUUsS0FEQTtBQUVUQyxNQUFBQSxZQUFZLEVBQUUsSUFGTDtBQUdUQyxNQUFBQSxhQUFhLEVBQUUsSUFITjtBQUlUQyxNQUFBQSxLQUFLLEVBQUU7QUFKRSxLQUFiO0FBTUg7O0FBQ0RDLEVBQUFBLFlBQVksR0FBRztBQUNYLFNBQUtDLFFBQUwsQ0FBYztBQUNWTCxNQUFBQSxPQUFPLEVBQUUsQ0FBQyxLQUFLRCxLQUFMLENBQVdDO0FBRFgsS0FBZDtBQUdIOztBQUVETSxFQUFBQSxjQUFjLEdBQUc7QUFDYixVQUFNQyxPQUFPLEdBQUcsS0FBS1QsS0FBTCxDQUFXVSxPQUFYLENBQW1CQyxVQUFuQixFQUFoQjs7QUFDQSxRQUFJRixPQUFPLENBQUNHLElBQVIsS0FBaUJDLFNBQXJCLEVBQWdDO0FBQzVCLGFBQU8sS0FBS1osS0FBTCxDQUFXRSxZQUFsQjtBQUNILEtBRkQsTUFFTztBQUNILGFBQU9XLGlDQUFnQkMsR0FBaEIsR0FBc0JDLFlBQXRCLENBQW1DUCxPQUFPLENBQUNRLEdBQTNDLENBQVA7QUFDSDtBQUNKOztBQUVEQyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixVQUFNVCxPQUFPLEdBQUcsS0FBS1QsS0FBTCxDQUFXVSxPQUFYLENBQW1CQyxVQUFuQixFQUFoQjs7QUFDQSxRQUFJRixPQUFPLENBQUNHLElBQVIsS0FBaUJDLFNBQWpCLElBQThCLEtBQUtaLEtBQUwsQ0FBV0UsWUFBWCxLQUE0QixJQUE5RCxFQUFvRTtBQUNoRSxVQUFJQyxhQUFKO0FBQ0Esb0NBQVlLLE9BQU8sQ0FBQ0csSUFBcEIsRUFBMEJPLElBQTFCLENBQStCLFVBQVNDLElBQVQsRUFBZTtBQUMxQ2hCLFFBQUFBLGFBQWEsR0FBR2dCLElBQWhCO0FBQ0EsZUFBT0MsR0FBRyxDQUFDQyxlQUFKLENBQW9CbEIsYUFBcEIsQ0FBUDtBQUNILE9BSEQsRUFHR2UsSUFISCxDQUdTRixHQUFELElBQVM7QUFDYixhQUFLVixRQUFMLENBQWM7QUFDVkosVUFBQUEsWUFBWSxFQUFFYyxHQURKO0FBRVZiLFVBQUFBLGFBQWEsRUFBRUE7QUFGTCxTQUFkO0FBSUgsT0FSRCxFQVFJbUIsR0FBRCxJQUFTO0FBQ1JDLFFBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLGdDQUFiLEVBQStDRixHQUEvQztBQUNBLGFBQUtoQixRQUFMLENBQWM7QUFDVkYsVUFBQUEsS0FBSyxFQUFFa0I7QUFERyxTQUFkO0FBR0gsT0FiRDtBQWNIO0FBQ0o7O0FBRURHLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFFBQUksS0FBS3pCLEtBQUwsQ0FBV0UsWUFBZixFQUE2QjtBQUN6QmtCLE1BQUFBLEdBQUcsQ0FBQ00sZUFBSixDQUFvQixLQUFLMUIsS0FBTCxDQUFXRSxZQUEvQjtBQUNIO0FBQ0o7O0FBRUR5QixFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNbkIsT0FBTyxHQUFHLEtBQUtULEtBQUwsQ0FBV1UsT0FBWCxDQUFtQkMsVUFBbkIsRUFBaEI7O0FBRUEsUUFBSSxLQUFLVixLQUFMLENBQVdJLEtBQVgsS0FBcUIsSUFBekIsRUFBK0I7QUFDM0IsYUFDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRXdCLE9BQU8sQ0FBQyxpQ0FBRCxDQUFqQjtBQUFzRCxRQUFBLEtBQUssRUFBQyxJQUE1RDtBQUFpRSxRQUFBLE1BQU0sRUFBQztBQUF4RSxRQURKLEVBRU0seUJBQUcsd0JBQUgsQ0FGTixDQURKO0FBTUg7O0FBRUQsUUFBSXBCLE9BQU8sQ0FBQ0csSUFBUixLQUFpQkMsU0FBakIsSUFBOEIsS0FBS1osS0FBTCxDQUFXRSxZQUFYLEtBQTRCLElBQTlELEVBQW9FO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFDSTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFNBQ0k7QUFBSyxRQUFBLEdBQUcsRUFBRTBCLE9BQU8sQ0FBQyxpQ0FBRCxDQUFqQjtBQUFzRCxRQUFBLEdBQUcsRUFBRXBCLE9BQU8sQ0FBQ3FCLElBQW5FO0FBQXlFLFFBQUEsS0FBSyxFQUFDLElBQS9FO0FBQW9GLFFBQUEsTUFBTSxFQUFDO0FBQTNGLFFBREosQ0FESjtBQUtIOztBQUVELFVBQU1DLFVBQVUsR0FBRyxLQUFLdkIsY0FBTCxFQUFuQjs7QUFFQSxXQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDSTtBQUFPLE1BQUEsR0FBRyxFQUFFdUIsVUFBWjtBQUF3QixNQUFBLFFBQVE7QUFBaEMsTUFESixFQUVJLDZCQUFDLGtCQUFELDZCQUFlLEtBQUsvQixLQUFwQjtBQUEyQixNQUFBLGFBQWEsRUFBRSxLQUFLQyxLQUFMLENBQVdHO0FBQXJELE9BRkosQ0FESjtBQU1IOztBQXBGbUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4gQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuXHJcbiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4geW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG4gVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgTUZpbGVCb2R5IGZyb20gJy4vTUZpbGVCb2R5JztcclxuXHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgeyBkZWNyeXB0RmlsZSB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL0RlY3J5cHRGaWxlJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTUF1ZGlvQm9keSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwbGF5aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVjcnlwdGVkVXJsOiBudWxsLFxyXG4gICAgICAgICAgICBkZWNyeXB0ZWRCbG9iOiBudWxsLFxyXG4gICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgb25QbGF5VG9nZ2xlKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwbGF5aW5nOiAhdGhpcy5zdGF0ZS5wbGF5aW5nLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9nZXRDb250ZW50VXJsKCkge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgIGlmIChjb250ZW50LmZpbGUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5teGNVcmxUb0h0dHAoY29udGVudC51cmwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoY29udGVudC5maWxlICE9PSB1bmRlZmluZWQgJiYgdGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgbGV0IGRlY3J5cHRlZEJsb2I7XHJcbiAgICAgICAgICAgIGRlY3J5cHRGaWxlKGNvbnRlbnQuZmlsZSkudGhlbihmdW5jdGlvbihibG9iKSB7XHJcbiAgICAgICAgICAgICAgICBkZWNyeXB0ZWRCbG9iID0gYmxvYjtcclxuICAgICAgICAgICAgICAgIHJldHVybiBVUkwuY3JlYXRlT2JqZWN0VVJMKGRlY3J5cHRlZEJsb2IpO1xyXG4gICAgICAgICAgICB9KS50aGVuKCh1cmwpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZFVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlY3J5cHRlZEJsb2I6IGRlY3J5cHRlZEJsb2IsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS53YXJuKFwiVW5hYmxlIHRvIGRlY3J5cHQgYXR0YWNobWVudDogXCIsIGVycik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcjogZXJyLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5kZWNyeXB0ZWRVcmwpIHtcclxuICAgICAgICAgICAgVVJMLnJldm9rZU9iamVjdFVSTCh0aGlzLnN0YXRlLmRlY3J5cHRlZFVybCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldENvbnRlbnQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3IgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X01BdWRpb0JvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvd2FybmluZy5zdmdcIil9IHdpZHRoPVwiMTZcIiBoZWlnaHQ9XCIxNlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIkVycm9yIGRlY3J5cHRpbmcgYXVkaW9cIikgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbnRlbnQuZmlsZSAhPT0gdW5kZWZpbmVkICYmIHRoaXMuc3RhdGUuZGVjcnlwdGVkVXJsID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIC8vIE5lZWQgdG8gZGVjcnlwdCB0aGUgYXR0YWNobWVudFxyXG4gICAgICAgICAgICAvLyBUaGUgYXR0YWNobWVudCBpcyBkZWNyeXB0ZWQgaW4gY29tcG9uZW50RGlkTW91bnQuXHJcbiAgICAgICAgICAgIC8vIEZvciBub3cgYWRkIGFuIGltZyB0YWcgd2l0aCBhIDE2eDE2IHNwaW5uZXIuXHJcbiAgICAgICAgICAgIC8vIE5vdCBzdXJlIGhvdyB0YWxsIHRoZSBhdWRpbyBwbGF5ZXIgaXMgc28gbm90IHN1cmUgaG93IHRhbGwgaXQgc2hvdWxkIGFjdHVhbGx5IGJlLlxyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfTUF1ZGlvQm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9zcGlubmVyLmdpZlwiKX0gYWx0PXtjb250ZW50LmJvZHl9IHdpZHRoPVwiMTZcIiBoZWlnaHQ9XCIxNlwiIC8+XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50VXJsID0gdGhpcy5fZ2V0Q29udGVudFVybCgpO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9NQXVkaW9Cb2R5XCI+XHJcbiAgICAgICAgICAgICAgICA8YXVkaW8gc3JjPXtjb250ZW50VXJsfSBjb250cm9scyAvPlxyXG4gICAgICAgICAgICAgICAgPE1GaWxlQm9keSB7Li4udGhpcy5wcm9wc30gZGVjcnlwdGVkQmxvYj17dGhpcy5zdGF0ZS5kZWNyeXB0ZWRCbG9ifSAvPlxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=