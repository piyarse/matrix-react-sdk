"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _DeviceListener = _interopRequireDefault(require("../../../DeviceListener"));

var _SetupEncryptionDialog = _interopRequireDefault(require("../dialogs/SetupEncryptionDialog"));

var _CrossSigningManager = require("../../../CrossSigningManager");

/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class SetupEncryptionToast extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onLaterClick", () => {
      _DeviceListener.default.sharedInstance().dismissEncryptionSetup();
    });
    (0, _defineProperty2.default)(this, "_onSetupClick", async () => {
      if (this.props.kind === "verify_this_session") {
        _Modal.default.createTrackedDialog('Verify session', 'Verify session', _SetupEncryptionDialog.default, {}, null,
        /* priority = */
        false,
        /* static = */
        true);
      } else {
        const Spinner = sdk.getComponent("elements.Spinner");

        const modal = _Modal.default.createDialog(Spinner, null, 'mx_Dialog_spinner',
        /* priority */
        false,
        /* static */
        true);

        try {
          await (0, _CrossSigningManager.accessSecretStorage)();
          await this._waitForCompletion();
        } finally {
          modal.close();
        }
      }
    });
  }

  async _waitForCompletion() {
    if (this.props.kind === 'upgrade_ssss') {
      return new Promise(resolve => {
        const recheck = async () => {
          const needsUpgrade = await _MatrixClientPeg.MatrixClientPeg.get().secretStorageKeyNeedsUpgrade();

          if (!needsUpgrade) {
            _MatrixClientPeg.MatrixClientPeg.get().removeListener('accountData', recheck);

            resolve();
          }
        };

        _MatrixClientPeg.MatrixClientPeg.get().on('accountData', recheck);

        recheck();
      });
    } else {
      return;
    }
  }

  getDescription() {
    switch (this.props.kind) {
      case 'set_up_encryption':
      case 'upgrade_encryption':
        return (0, _languageHandler._t)('Verify yourself & others to keep your chats safe');

      case 'verify_this_session':
        return (0, _languageHandler._t)('Other users may not trust it');

      case 'upgrade_ssss':
        return (0, _languageHandler._t)('Update your secure storage');
    }
  }

  getSetupCaption() {
    switch (this.props.kind) {
      case 'set_up_encryption':
      case 'upgrade_encryption':
      case 'upgrade_ssss':
        return (0, _languageHandler._t)('Upgrade');

      case 'verify_this_session':
        return (0, _languageHandler._t)('Verify');
    }
  }

  render() {
    const FormButton = sdk.getComponent("elements.FormButton");
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Toast_description"
    }, this.getDescription()), _react.default.createElement("div", {
      className: "mx_Toast_buttons",
      "aria-live": "off"
    }, _react.default.createElement(FormButton, {
      label: (0, _languageHandler._t)("Later"),
      kind: "danger",
      onClick: this._onLaterClick
    }), _react.default.createElement(FormButton, {
      label: this.getSetupCaption(),
      onClick: this._onSetupClick
    })));
  }

}

exports.default = SetupEncryptionToast;
(0, _defineProperty2.default)(SetupEncryptionToast, "propTypes", {
  toastKey: _propTypes.default.string.isRequired,
  kind: _propTypes.default.oneOf(['set_up_encryption', 'verify_this_session', 'upgrade_encryption', 'upgrade_ssss']).isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3RvYXN0cy9TZXR1cEVuY3J5cHRpb25Ub2FzdC5qcyJdLCJuYW1lcyI6WyJTZXR1cEVuY3J5cHRpb25Ub2FzdCIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsIkRldmljZUxpc3RlbmVyIiwic2hhcmVkSW5zdGFuY2UiLCJkaXNtaXNzRW5jcnlwdGlvblNldHVwIiwicHJvcHMiLCJraW5kIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwiU2V0dXBFbmNyeXB0aW9uRGlhbG9nIiwiU3Bpbm5lciIsInNkayIsImdldENvbXBvbmVudCIsIm1vZGFsIiwiY3JlYXRlRGlhbG9nIiwiX3dhaXRGb3JDb21wbGV0aW9uIiwiY2xvc2UiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlY2hlY2siLCJuZWVkc1VwZ3JhZGUiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJzZWNyZXRTdG9yYWdlS2V5TmVlZHNVcGdyYWRlIiwicmVtb3ZlTGlzdGVuZXIiLCJvbiIsImdldERlc2NyaXB0aW9uIiwiZ2V0U2V0dXBDYXB0aW9uIiwicmVuZGVyIiwiRm9ybUJ1dHRvbiIsIl9vbkxhdGVyQ2xpY2siLCJfb25TZXR1cENsaWNrIiwidG9hc3RLZXkiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwib25lT2YiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBeEJBOzs7Ozs7Ozs7Ozs7Ozs7QUEwQmUsTUFBTUEsb0JBQU4sU0FBbUNDLGVBQU1DLGFBQXpDLENBQXVEO0FBQUE7QUFBQTtBQUFBLHlEQVdsRCxNQUFNO0FBQ2xCQyw4QkFBZUMsY0FBZixHQUFnQ0Msc0JBQWhDO0FBQ0gsS0FiaUU7QUFBQSx5REFpQ2xELFlBQVk7QUFDeEIsVUFBSSxLQUFLQyxLQUFMLENBQVdDLElBQVgsS0FBb0IscUJBQXhCLEVBQStDO0FBQzNDQyx1QkFBTUMsbUJBQU4sQ0FBMEIsZ0JBQTFCLEVBQTRDLGdCQUE1QyxFQUE4REMsOEJBQTlELEVBQ0ksRUFESixFQUNRLElBRFI7QUFDYztBQUFpQixhQUQvQjtBQUNzQztBQUFlLFlBRHJEO0FBRUgsT0FIRCxNQUdPO0FBQ0gsY0FBTUMsT0FBTyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCOztBQUNBLGNBQU1DLEtBQUssR0FBR04sZUFBTU8sWUFBTixDQUNWSixPQURVLEVBQ0QsSUFEQyxFQUNLLG1CQURMO0FBQzBCO0FBQWUsYUFEekM7QUFDZ0Q7QUFBYSxZQUQ3RCxDQUFkOztBQUdBLFlBQUk7QUFDQSxnQkFBTSwrQ0FBTjtBQUNBLGdCQUFNLEtBQUtLLGtCQUFMLEVBQU47QUFDSCxTQUhELFNBR1U7QUFDTkYsVUFBQUEsS0FBSyxDQUFDRyxLQUFOO0FBQ0g7QUFDSjtBQUNKLEtBakRpRTtBQUFBOztBQWVsRSxRQUFNRCxrQkFBTixHQUEyQjtBQUN2QixRQUFJLEtBQUtWLEtBQUwsQ0FBV0MsSUFBWCxLQUFvQixjQUF4QixFQUF3QztBQUNwQyxhQUFPLElBQUlXLE9BQUosQ0FBWUMsT0FBTyxJQUFJO0FBQzFCLGNBQU1DLE9BQU8sR0FBRyxZQUFZO0FBQ3hCLGdCQUFNQyxZQUFZLEdBQUcsTUFBTUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsNEJBQXRCLEVBQTNCOztBQUNBLGNBQUksQ0FBQ0gsWUFBTCxFQUFtQjtBQUNmQyw2Q0FBZ0JDLEdBQWhCLEdBQXNCRSxjQUF0QixDQUFxQyxhQUFyQyxFQUFvREwsT0FBcEQ7O0FBQ0FELFlBQUFBLE9BQU87QUFDVjtBQUNKLFNBTkQ7O0FBT0FHLHlDQUFnQkMsR0FBaEIsR0FBc0JHLEVBQXRCLENBQXlCLGFBQXpCLEVBQXdDTixPQUF4Qzs7QUFDQUEsUUFBQUEsT0FBTztBQUNWLE9BVk0sQ0FBUDtBQVdILEtBWkQsTUFZTztBQUNIO0FBQ0g7QUFDSjs7QUFvQkRPLEVBQUFBLGNBQWMsR0FBRztBQUNiLFlBQVEsS0FBS3JCLEtBQUwsQ0FBV0MsSUFBbkI7QUFDSSxXQUFLLG1CQUFMO0FBQ0EsV0FBSyxvQkFBTDtBQUNJLGVBQU8seUJBQUcsa0RBQUgsQ0FBUDs7QUFDSixXQUFLLHFCQUFMO0FBQ0ksZUFBTyx5QkFBRyw4QkFBSCxDQUFQOztBQUNKLFdBQUssY0FBTDtBQUNJLGVBQU8seUJBQUcsNEJBQUgsQ0FBUDtBQVBSO0FBU0g7O0FBRURxQixFQUFBQSxlQUFlLEdBQUc7QUFDZCxZQUFRLEtBQUt0QixLQUFMLENBQVdDLElBQW5CO0FBQ0ksV0FBSyxtQkFBTDtBQUNBLFdBQUssb0JBQUw7QUFDQSxXQUFLLGNBQUw7QUFDSSxlQUFPLHlCQUFHLFNBQUgsQ0FBUDs7QUFDSixXQUFLLHFCQUFMO0FBQ0ksZUFBTyx5QkFBRyxRQUFILENBQVA7QUFOUjtBQVFIOztBQUVEc0IsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHbEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFuQjtBQUNBLFdBQVEsMENBQ0o7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXVDLEtBQUtjLGNBQUwsRUFBdkMsQ0FESSxFQUVKO0FBQUssTUFBQSxTQUFTLEVBQUMsa0JBQWY7QUFBa0MsbUJBQVU7QUFBNUMsT0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQUFuQjtBQUFnQyxNQUFBLElBQUksRUFBQyxRQUFyQztBQUE4QyxNQUFBLE9BQU8sRUFBRSxLQUFLSTtBQUE1RCxNQURKLEVBRUksNkJBQUMsVUFBRDtBQUFZLE1BQUEsS0FBSyxFQUFFLEtBQUtILGVBQUwsRUFBbkI7QUFBMkMsTUFBQSxPQUFPLEVBQUUsS0FBS0k7QUFBekQsTUFGSixDQUZJLENBQVI7QUFPSDs7QUFuRmlFOzs7OEJBQWpEaEMsb0IsZUFDRTtBQUNmaUMsRUFBQUEsUUFBUSxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEWjtBQUVmN0IsRUFBQUEsSUFBSSxFQUFFMkIsbUJBQVVHLEtBQVYsQ0FBZ0IsQ0FDbEIsbUJBRGtCLEVBRWxCLHFCQUZrQixFQUdsQixvQkFIa0IsRUFJbEIsY0FKa0IsQ0FBaEIsRUFLSEQ7QUFQWSxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG5odHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCB7IE1hdHJpeENsaWVudFBlZyB9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgRGV2aWNlTGlzdGVuZXIgZnJvbSAnLi4vLi4vLi4vRGV2aWNlTGlzdGVuZXInO1xyXG5pbXBvcnQgU2V0dXBFbmNyeXB0aW9uRGlhbG9nIGZyb20gXCIuLi9kaWFsb2dzL1NldHVwRW5jcnlwdGlvbkRpYWxvZ1wiO1xyXG5pbXBvcnQgeyBhY2Nlc3NTZWNyZXRTdG9yYWdlIH0gZnJvbSAnLi4vLi4vLi4vQ3Jvc3NTaWduaW5nTWFuYWdlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZXR1cEVuY3J5cHRpb25Ub2FzdCBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICB0b2FzdEtleTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGtpbmQ6IFByb3BUeXBlcy5vbmVPZihbXHJcbiAgICAgICAgICAgICdzZXRfdXBfZW5jcnlwdGlvbicsXHJcbiAgICAgICAgICAgICd2ZXJpZnlfdGhpc19zZXNzaW9uJyxcclxuICAgICAgICAgICAgJ3VwZ3JhZGVfZW5jcnlwdGlvbicsXHJcbiAgICAgICAgICAgICd1cGdyYWRlX3Nzc3MnLFxyXG4gICAgICAgIF0pLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIF9vbkxhdGVyQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgRGV2aWNlTGlzdGVuZXIuc2hhcmVkSW5zdGFuY2UoKS5kaXNtaXNzRW5jcnlwdGlvblNldHVwKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIGFzeW5jIF93YWl0Rm9yQ29tcGxldGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5raW5kID09PSAndXBncmFkZV9zc3NzJykge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWNoZWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG5lZWRzVXBncmFkZSA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZWNyZXRTdG9yYWdlS2V5TmVlZHNVcGdyYWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFuZWVkc1VwZ3JhZGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKCdhY2NvdW50RGF0YScsIHJlY2hlY2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbignYWNjb3VudERhdGEnLCByZWNoZWNrKTtcclxuICAgICAgICAgICAgICAgIHJlY2hlY2soKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25TZXR1cENsaWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmtpbmQgPT09IFwidmVyaWZ5X3RoaXNfc2Vzc2lvblwiKSB7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1ZlcmlmeSBzZXNzaW9uJywgJ1ZlcmlmeSBzZXNzaW9uJywgU2V0dXBFbmNyeXB0aW9uRGlhbG9nLFxyXG4gICAgICAgICAgICAgICAge30sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuICAgICAgICAgICAgY29uc3QgbW9kYWwgPSBNb2RhbC5jcmVhdGVEaWFsb2coXHJcbiAgICAgICAgICAgICAgICBTcGlubmVyLCBudWxsLCAnbXhfRGlhbG9nX3NwaW5uZXInLCAvKiBwcmlvcml0eSAqLyBmYWxzZSwgLyogc3RhdGljICovIHRydWUsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBhY2Nlc3NTZWNyZXRTdG9yYWdlKCk7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl93YWl0Rm9yQ29tcGxldGlvbigpO1xyXG4gICAgICAgICAgICB9IGZpbmFsbHkge1xyXG4gICAgICAgICAgICAgICAgbW9kYWwuY2xvc2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgZ2V0RGVzY3JpcHRpb24oKSB7XHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnByb3BzLmtpbmQpIHtcclxuICAgICAgICAgICAgY2FzZSAnc2V0X3VwX2VuY3J5cHRpb24nOlxyXG4gICAgICAgICAgICBjYXNlICd1cGdyYWRlX2VuY3J5cHRpb24nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdWZXJpZnkgeW91cnNlbGYgJiBvdGhlcnMgdG8ga2VlcCB5b3VyIGNoYXRzIHNhZmUnKTtcclxuICAgICAgICAgICAgY2FzZSAndmVyaWZ5X3RoaXNfc2Vzc2lvbic6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ090aGVyIHVzZXJzIG1heSBub3QgdHJ1c3QgaXQnKTtcclxuICAgICAgICAgICAgY2FzZSAndXBncmFkZV9zc3NzJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnVXBkYXRlIHlvdXIgc2VjdXJlIHN0b3JhZ2UnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2V0dXBDYXB0aW9uKCkge1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5wcm9wcy5raW5kKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3NldF91cF9lbmNyeXB0aW9uJzpcclxuICAgICAgICAgICAgY2FzZSAndXBncmFkZV9lbmNyeXB0aW9uJzpcclxuICAgICAgICAgICAgY2FzZSAndXBncmFkZV9zc3NzJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnVXBncmFkZScpO1xyXG4gICAgICAgICAgICBjYXNlICd2ZXJpZnlfdGhpc19zZXNzaW9uJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnVmVyaWZ5Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBGb3JtQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkZvcm1CdXR0b25cIik7XHJcbiAgICAgICAgcmV0dXJuICg8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvYXN0X2Rlc2NyaXB0aW9uXCI+e3RoaXMuZ2V0RGVzY3JpcHRpb24oKX08L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Ub2FzdF9idXR0b25zXCIgYXJpYS1saXZlPVwib2ZmXCI+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybUJ1dHRvbiBsYWJlbD17X3QoXCJMYXRlclwiKX0ga2luZD1cImRhbmdlclwiIG9uQ2xpY2s9e3RoaXMuX29uTGF0ZXJDbGlja30gLz5cclxuICAgICAgICAgICAgICAgIDxGb3JtQnV0dG9uIGxhYmVsPXt0aGlzLmdldFNldHVwQ2FwdGlvbigpfSBvbkNsaWNrPXt0aGlzLl9vblNldHVwQ2xpY2t9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2Pik7XHJcbiAgICB9XHJcbn1cclxuIl19