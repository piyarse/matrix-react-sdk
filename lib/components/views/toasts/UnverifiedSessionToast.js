"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _DeviceListener = _interopRequireDefault(require("../../../DeviceListener"));

var _NewSessionReviewDialog = _interopRequireDefault(require("../dialogs/NewSessionReviewDialog"));

var _FormButton = _interopRequireDefault(require("../elements/FormButton"));

var _replaceableComponent = require("../../../utils/replaceableComponent");

var _dec, _class, _class2, _temp;

let UnverifiedSessionToast = (_dec = (0, _replaceableComponent.replaceableComponent)("views.toasts.UnverifiedSessionToast"), _dec(_class = (_temp = _class2 = class UnverifiedSessionToast extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_onLaterClick", () => {
      const {
        device
      } = this.props;

      _DeviceListener.default.sharedInstance().dismissVerification(device.deviceId);
    });
    (0, _defineProperty2.default)(this, "_onReviewClick", async () => {
      const {
        device
      } = this.props;

      _Modal.default.createTrackedDialog('New Session Review', 'Starting dialog', _NewSessionReviewDialog.default, {
        userId: _MatrixClientPeg.MatrixClientPeg.get().getUserId(),
        device,
        onFinished: r => {
          if (!r) {
            /* This'll come back false if the user clicks "this wasn't me" and saw a warning dialog */
            this._onLaterClick();
          }
        }
      }, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
  }

  render() {
    const {
      device
    } = this.props;
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Toast_description"
    }, _react.default.createElement("span", {
      className: "mx_Toast_deviceName"
    }, device.getDisplayName()), " ", _react.default.createElement("span", {
      className: "mx_Toast_deviceID"
    }, "(", device.deviceId, ")")), _react.default.createElement("div", {
      className: "mx_Toast_buttons",
      "aria-live": "off"
    }, _react.default.createElement(_FormButton.default, {
      label: (0, _languageHandler._t)("Later"),
      kind: "danger",
      onClick: this._onLaterClick
    }), _react.default.createElement(_FormButton.default, {
      label: (0, _languageHandler._t)("Review"),
      onClick: this._onReviewClick
    })));
  }

}, (0, _defineProperty2.default)(_class2, "propTypes", {
  toastKey: _propTypes.default.string.isRequired,
  device: _propTypes.default.object.isRequired
}), _temp)) || _class);
exports.default = UnverifiedSessionToast;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3RvYXN0cy9VbnZlcmlmaWVkU2Vzc2lvblRvYXN0LmpzIl0sIm5hbWVzIjpbIlVudmVyaWZpZWRTZXNzaW9uVG9hc3QiLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJkZXZpY2UiLCJwcm9wcyIsIkRldmljZUxpc3RlbmVyIiwic2hhcmVkSW5zdGFuY2UiLCJkaXNtaXNzVmVyaWZpY2F0aW9uIiwiZGV2aWNlSWQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJOZXdTZXNzaW9uUmV2aWV3RGlhbG9nIiwidXNlcklkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0VXNlcklkIiwib25GaW5pc2hlZCIsInIiLCJfb25MYXRlckNsaWNrIiwicmVuZGVyIiwiZ2V0RGlzcGxheU5hbWUiLCJfb25SZXZpZXdDbGljayIsInRvYXN0S2V5IiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7SUFHcUJBLHNCLFdBRHBCLGdEQUFxQixxQ0FBckIsQyxtQ0FBRCxNQUNxQkEsc0JBRHJCLFNBQ29EQyxlQUFNQyxhQUQxRCxDQUN3RTtBQUFBO0FBQUE7QUFBQSx5REFNcEQsTUFBTTtBQUNsQixZQUFNO0FBQUVDLFFBQUFBO0FBQUYsVUFBYSxLQUFLQyxLQUF4Qjs7QUFDQUMsOEJBQWVDLGNBQWYsR0FBZ0NDLG1CQUFoQyxDQUFvREosTUFBTSxDQUFDSyxRQUEzRDtBQUNILEtBVG1FO0FBQUEsMERBV25ELFlBQVk7QUFDekIsWUFBTTtBQUFFTCxRQUFBQTtBQUFGLFVBQWEsS0FBS0MsS0FBeEI7O0FBRUFLLHFCQUFNQyxtQkFBTixDQUEwQixvQkFBMUIsRUFBZ0QsaUJBQWhELEVBQW1FQywrQkFBbkUsRUFBMkY7QUFDdkZDLFFBQUFBLE1BQU0sRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsU0FBdEIsRUFEK0U7QUFFdkZaLFFBQUFBLE1BRnVGO0FBR3ZGYSxRQUFBQSxVQUFVLEVBQUdDLENBQUQsSUFBTztBQUNmLGNBQUksQ0FBQ0EsQ0FBTCxFQUFRO0FBQ0o7QUFDQSxpQkFBS0MsYUFBTDtBQUNIO0FBQ0o7QUFSc0YsT0FBM0YsRUFTRyxJQVRIO0FBU1M7QUFBaUIsV0FUMUI7QUFTaUM7QUFBZSxVQVRoRDtBQVVILEtBeEJtRTtBQUFBOztBQTBCcEVDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU07QUFBRWhCLE1BQUFBO0FBQUYsUUFBYSxLQUFLQyxLQUF4QjtBQUVBLFdBQVEsMENBQ0o7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNLRCxNQUFNLENBQUNpQixjQUFQLEVBREwsQ0FESixPQUdZO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsWUFDRmpCLE1BQU0sQ0FBQ0ssUUFETCxNQUhaLENBREksRUFRSjtBQUFLLE1BQUEsU0FBUyxFQUFDLGtCQUFmO0FBQWtDLG1CQUFVO0FBQTVDLE9BQ0ksNkJBQUMsbUJBQUQ7QUFBWSxNQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBQW5CO0FBQWdDLE1BQUEsSUFBSSxFQUFDLFFBQXJDO0FBQThDLE1BQUEsT0FBTyxFQUFFLEtBQUtVO0FBQTVELE1BREosRUFFSSw2QkFBQyxtQkFBRDtBQUFZLE1BQUEsS0FBSyxFQUFFLHlCQUFHLFFBQUgsQ0FBbkI7QUFBaUMsTUFBQSxPQUFPLEVBQUUsS0FBS0c7QUFBL0MsTUFGSixDQVJJLENBQVI7QUFhSDs7QUExQ21FLEMsc0RBQ2pEO0FBQ2ZDLEVBQUFBLFFBQVEsRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRFo7QUFFZnRCLEVBQUFBLE1BQU0sRUFBRW9CLG1CQUFVRyxNQUFWLENBQWlCRDtBQUZWLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbmh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCB7IE1hdHJpeENsaWVudFBlZyB9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBEZXZpY2VMaXN0ZW5lciBmcm9tICcuLi8uLi8uLi9EZXZpY2VMaXN0ZW5lcic7XHJcbmltcG9ydCBOZXdTZXNzaW9uUmV2aWV3RGlhbG9nIGZyb20gJy4uL2RpYWxvZ3MvTmV3U2Vzc2lvblJldmlld0RpYWxvZyc7XHJcbmltcG9ydCBGb3JtQnV0dG9uIGZyb20gJy4uL2VsZW1lbnRzL0Zvcm1CdXR0b24nO1xyXG5pbXBvcnQgeyByZXBsYWNlYWJsZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL3JlcGxhY2VhYmxlQ29tcG9uZW50JztcclxuXHJcbkByZXBsYWNlYWJsZUNvbXBvbmVudChcInZpZXdzLnRvYXN0cy5VbnZlcmlmaWVkU2Vzc2lvblRvYXN0XCIpXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVudmVyaWZpZWRTZXNzaW9uVG9hc3QgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgdG9hc3RLZXk6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBkZXZpY2U6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgX29uTGF0ZXJDbGljayA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCB7IGRldmljZSB9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBEZXZpY2VMaXN0ZW5lci5zaGFyZWRJbnN0YW5jZSgpLmRpc21pc3NWZXJpZmljYXRpb24oZGV2aWNlLmRldmljZUlkKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uUmV2aWV3Q2xpY2sgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgeyBkZXZpY2UgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ05ldyBTZXNzaW9uIFJldmlldycsICdTdGFydGluZyBkaWFsb2cnLCBOZXdTZXNzaW9uUmV2aWV3RGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHVzZXJJZDogTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXJJZCgpLFxyXG4gICAgICAgICAgICBkZXZpY2UsXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IChyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAvKiBUaGlzJ2xsIGNvbWUgYmFjayBmYWxzZSBpZiB0aGUgdXNlciBjbGlja3MgXCJ0aGlzIHdhc24ndCBtZVwiIGFuZCBzYXcgYSB3YXJuaW5nIGRpYWxvZyAqL1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX29uTGF0ZXJDbGljaygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgeyBkZXZpY2UgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIHJldHVybiAoPGRpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Ub2FzdF9kZXNjcmlwdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfVG9hc3RfZGV2aWNlTmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtkZXZpY2UuZ2V0RGlzcGxheU5hbWUoKX1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj4gPHNwYW4gY2xhc3NOYW1lPVwibXhfVG9hc3RfZGV2aWNlSURcIj5cclxuICAgICAgICAgICAgICAgICAgICAoe2RldmljZS5kZXZpY2VJZH0pXHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvYXN0X2J1dHRvbnNcIiBhcmlhLWxpdmU9XCJvZmZcIj5cclxuICAgICAgICAgICAgICAgIDxGb3JtQnV0dG9uIGxhYmVsPXtfdChcIkxhdGVyXCIpfSBraW5kPVwiZGFuZ2VyXCIgb25DbGljaz17dGhpcy5fb25MYXRlckNsaWNrfSAvPlxyXG4gICAgICAgICAgICAgICAgPEZvcm1CdXR0b24gbGFiZWw9e190KFwiUmV2aWV3XCIpfSBvbkNsaWNrPXt0aGlzLl9vblJldmlld0NsaWNrfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj4pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==