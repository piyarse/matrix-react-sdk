"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _RightPanelStorePhases = require("../../../stores/RightPanelStorePhases");

var _KeyVerificationStateObserver = require("../../../utils/KeyVerificationStateObserver");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _ToastStore = _interopRequireDefault(require("../../../stores/ToastStore"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class VerificationRequestToast extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_checkRequestIsPending", () => {
      const {
        request
      } = this.props;

      if (!request.canAccept) {
        _ToastStore.default.sharedInstance().dismissToast(this.props.toastKey);
      }
    });
    (0, _defineProperty2.default)(this, "cancel", () => {
      _ToastStore.default.sharedInstance().dismissToast(this.props.toastKey);

      try {
        this.props.request.cancel();
      } catch (err) {
        console.error("Error while cancelling verification request", err);
      }
    });
    (0, _defineProperty2.default)(this, "accept", async () => {
      _ToastStore.default.sharedInstance().dismissToast(this.props.toastKey);

      const {
        request
      } = this.props; // no room id for to_device requests

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      try {
        if (request.channel.roomId) {
          _dispatcher.default.dispatch({
            action: 'view_room',
            room_id: request.channel.roomId,
            should_peek: false
          });

          _dispatcher.default.dispatch({
            action: "set_right_panel_phase",
            phase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel,
            refireParams: {
              verificationRequest: request,
              member: cli.getUser(request.otherUserId)
            }
          });
        } else {
          const VerificationRequestDialog = sdk.getComponent("views.dialogs.VerificationRequestDialog");

          _Modal.default.createTrackedDialog('Incoming Verification', '', VerificationRequestDialog, {
            verificationRequest: request
          }, null,
          /* priority = */
          false,
          /* static = */
          true);
        }

        await request.accept();
      } catch (err) {
        console.error(err.message);
      }
    });
    this.state = {
      counter: Math.ceil(props.request.timeout / 1000)
    };
  }

  async componentDidMount() {
    const {
      request
    } = this.props;

    if (request.timeout && request.timeout > 0) {
      this._intervalHandle = setInterval(() => {
        let {
          counter
        } = this.state;
        counter = Math.max(0, counter - 1);
        this.setState({
          counter
        });
      }, 1000);
    }

    request.on("change", this._checkRequestIsPending); // We should probably have a separate class managing the active verification toasts,
    // rather than monitoring this in the toast component itself, since we'll get problems
    // like the toasdt not going away when the verification is cancelled unless it's the
    // one on the top (ie. the one that's mounted).
    // As a quick & dirty fix, check the toast is still relevant when it mounts (this prevents
    // a toast hanging around after logging in if you did a verification as part of login).

    this._checkRequestIsPending();

    if (request.isSelfVerification) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      this.setState({
        device: await cli.getStoredDevice(cli.getUserId(), request.channel.deviceId)
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this._intervalHandle);
    const {
      request
    } = this.props;
    request.off("change", this._checkRequestIsPending);
  }

  render() {
    const FormButton = sdk.getComponent("elements.FormButton");
    const {
      request
    } = this.props;
    let nameLabel;

    if (request.isSelfVerification) {
      if (this.state.device) {
        nameLabel = (0, _languageHandler._t)("From %(deviceName)s (%(deviceId)s)", {
          deviceName: this.state.device.getDisplayName(),
          deviceId: this.state.device.deviceId
        });
      }
    } else {
      const userId = request.otherUserId;
      const roomId = request.channel.roomId;
      nameLabel = roomId ? (0, _KeyVerificationStateObserver.userLabelForEventRoom)(userId, roomId) : userId; // for legacy to_device verification requests

      if (nameLabel === userId) {
        const client = _MatrixClientPeg.MatrixClientPeg.get();

        const user = client.getUser(userId);

        if (user && user.displayName) {
          nameLabel = (0, _languageHandler._t)("%(name)s (%(userId)s)", {
            name: user.displayName,
            userId
          });
        }
      }
    }

    const declineLabel = this.state.counter == 0 ? (0, _languageHandler._t)("Decline") : (0, _languageHandler._t)("Decline (%(counter)s)", {
      counter: this.state.counter
    });
    return _react.default.createElement("div", null, _react.default.createElement("div", {
      className: "mx_Toast_description"
    }, nameLabel), _react.default.createElement("div", {
      className: "mx_Toast_buttons",
      "aria-live": "off"
    }, _react.default.createElement(FormButton, {
      label: declineLabel,
      kind: "danger",
      onClick: this.cancel
    }), _react.default.createElement(FormButton, {
      label: (0, _languageHandler._t)("Accept"),
      onClick: this.accept
    })));
  }

}

exports.default = VerificationRequestToast;
VerificationRequestToast.propTypes = {
  request: _propTypes.default.object.isRequired,
  toastKey: _propTypes.default.string.isRequired
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3RvYXN0cy9WZXJpZmljYXRpb25SZXF1ZXN0VG9hc3QuanMiXSwibmFtZXMiOlsiVmVyaWZpY2F0aW9uUmVxdWVzdFRvYXN0IiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInJlcXVlc3QiLCJjYW5BY2NlcHQiLCJUb2FzdFN0b3JlIiwic2hhcmVkSW5zdGFuY2UiLCJkaXNtaXNzVG9hc3QiLCJ0b2FzdEtleSIsImNhbmNlbCIsImVyciIsImNvbnNvbGUiLCJlcnJvciIsImNsaSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImNoYW5uZWwiLCJyb29tSWQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJvb21faWQiLCJzaG91bGRfcGVlayIsInBoYXNlIiwiUklHSFRfUEFORUxfUEhBU0VTIiwiRW5jcnlwdGlvblBhbmVsIiwicmVmaXJlUGFyYW1zIiwidmVyaWZpY2F0aW9uUmVxdWVzdCIsIm1lbWJlciIsImdldFVzZXIiLCJvdGhlclVzZXJJZCIsIlZlcmlmaWNhdGlvblJlcXVlc3REaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJhY2NlcHQiLCJtZXNzYWdlIiwic3RhdGUiLCJjb3VudGVyIiwiTWF0aCIsImNlaWwiLCJ0aW1lb3V0IiwiY29tcG9uZW50RGlkTW91bnQiLCJfaW50ZXJ2YWxIYW5kbGUiLCJzZXRJbnRlcnZhbCIsIm1heCIsInNldFN0YXRlIiwib24iLCJfY2hlY2tSZXF1ZXN0SXNQZW5kaW5nIiwiaXNTZWxmVmVyaWZpY2F0aW9uIiwiZGV2aWNlIiwiZ2V0U3RvcmVkRGV2aWNlIiwiZ2V0VXNlcklkIiwiZGV2aWNlSWQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNsZWFySW50ZXJ2YWwiLCJvZmYiLCJyZW5kZXIiLCJGb3JtQnV0dG9uIiwibmFtZUxhYmVsIiwiZGV2aWNlTmFtZSIsImdldERpc3BsYXlOYW1lIiwidXNlcklkIiwiY2xpZW50IiwidXNlciIsImRpc3BsYXlOYW1lIiwibmFtZSIsImRlY2xpbmVMYWJlbCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7QUEyQmUsTUFBTUEsd0JBQU4sU0FBdUNDLGVBQU1DLGFBQTdDLENBQTJEO0FBQ3RFQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSxrRUFtQ00sTUFBTTtBQUMzQixZQUFNO0FBQUNDLFFBQUFBO0FBQUQsVUFBWSxLQUFLRCxLQUF2Qjs7QUFDQSxVQUFJLENBQUNDLE9BQU8sQ0FBQ0MsU0FBYixFQUF3QjtBQUNwQkMsNEJBQVdDLGNBQVgsR0FBNEJDLFlBQTVCLENBQXlDLEtBQUtMLEtBQUwsQ0FBV00sUUFBcEQ7QUFDSDtBQUNKLEtBeENrQjtBQUFBLGtEQTBDVixNQUFNO0FBQ1hILDBCQUFXQyxjQUFYLEdBQTRCQyxZQUE1QixDQUF5QyxLQUFLTCxLQUFMLENBQVdNLFFBQXBEOztBQUNBLFVBQUk7QUFDQSxhQUFLTixLQUFMLENBQVdDLE9BQVgsQ0FBbUJNLE1BQW5CO0FBQ0gsT0FGRCxDQUVFLE9BQU9DLEdBQVAsRUFBWTtBQUNWQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyw2Q0FBZCxFQUE2REYsR0FBN0Q7QUFDSDtBQUNKLEtBakRrQjtBQUFBLGtEQW1EVixZQUFZO0FBQ2pCTCwwQkFBV0MsY0FBWCxHQUE0QkMsWUFBNUIsQ0FBeUMsS0FBS0wsS0FBTCxDQUFXTSxRQUFwRDs7QUFDQSxZQUFNO0FBQUNMLFFBQUFBO0FBQUQsVUFBWSxLQUFLRCxLQUF2QixDQUZpQixDQUdqQjs7QUFDQSxZQUFNVyxHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxVQUFJO0FBQ0EsWUFBSVosT0FBTyxDQUFDYSxPQUFSLENBQWdCQyxNQUFwQixFQUE0QjtBQUN4QkMsOEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxZQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxZQUFBQSxPQUFPLEVBQUVsQixPQUFPLENBQUNhLE9BQVIsQ0FBZ0JDLE1BRmhCO0FBR1RLLFlBQUFBLFdBQVcsRUFBRTtBQUhKLFdBQWI7O0FBS0FKLDhCQUFJQyxRQUFKLENBQWE7QUFDVEMsWUFBQUEsTUFBTSxFQUFFLHVCQURDO0FBRVRHLFlBQUFBLEtBQUssRUFBRUMsMENBQW1CQyxlQUZqQjtBQUdUQyxZQUFBQSxZQUFZLEVBQUU7QUFDVkMsY0FBQUEsbUJBQW1CLEVBQUV4QixPQURYO0FBRVZ5QixjQUFBQSxNQUFNLEVBQUVmLEdBQUcsQ0FBQ2dCLE9BQUosQ0FBWTFCLE9BQU8sQ0FBQzJCLFdBQXBCO0FBRkU7QUFITCxXQUFiO0FBUUgsU0FkRCxNQWNPO0FBQ0gsZ0JBQU1DLHlCQUF5QixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIseUNBQWpCLENBQWxDOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVESix5QkFBdkQsRUFBa0Y7QUFDOUVKLFlBQUFBLG1CQUFtQixFQUFFeEI7QUFEeUQsV0FBbEYsRUFFRyxJQUZIO0FBRVM7QUFBaUIsZUFGMUI7QUFFaUM7QUFBZSxjQUZoRDtBQUdIOztBQUNELGNBQU1BLE9BQU8sQ0FBQ2lDLE1BQVIsRUFBTjtBQUNILE9BdEJELENBc0JFLE9BQU8xQixHQUFQLEVBQVk7QUFDVkMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQUcsQ0FBQzJCLE9BQWxCO0FBQ0g7QUFDSixLQWpGa0I7QUFFZixTQUFLQyxLQUFMLEdBQWE7QUFBQ0MsTUFBQUEsT0FBTyxFQUFFQyxJQUFJLENBQUNDLElBQUwsQ0FBVXZDLEtBQUssQ0FBQ0MsT0FBTixDQUFjdUMsT0FBZCxHQUF3QixJQUFsQztBQUFWLEtBQWI7QUFDSDs7QUFFRCxRQUFNQyxpQkFBTixHQUEwQjtBQUN0QixVQUFNO0FBQUN4QyxNQUFBQTtBQUFELFFBQVksS0FBS0QsS0FBdkI7O0FBQ0EsUUFBSUMsT0FBTyxDQUFDdUMsT0FBUixJQUFtQnZDLE9BQU8sQ0FBQ3VDLE9BQVIsR0FBa0IsQ0FBekMsRUFBNEM7QUFDeEMsV0FBS0UsZUFBTCxHQUF1QkMsV0FBVyxDQUFDLE1BQU07QUFDckMsWUFBSTtBQUFDTixVQUFBQTtBQUFELFlBQVksS0FBS0QsS0FBckI7QUFDQUMsUUFBQUEsT0FBTyxHQUFHQyxJQUFJLENBQUNNLEdBQUwsQ0FBUyxDQUFULEVBQVlQLE9BQU8sR0FBRyxDQUF0QixDQUFWO0FBQ0EsYUFBS1EsUUFBTCxDQUFjO0FBQUNSLFVBQUFBO0FBQUQsU0FBZDtBQUNILE9BSmlDLEVBSS9CLElBSitCLENBQWxDO0FBS0g7O0FBQ0RwQyxJQUFBQSxPQUFPLENBQUM2QyxFQUFSLENBQVcsUUFBWCxFQUFxQixLQUFLQyxzQkFBMUIsRUFUc0IsQ0FVdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFNBQUtBLHNCQUFMOztBQUVBLFFBQUk5QyxPQUFPLENBQUMrQyxrQkFBWixFQUFnQztBQUM1QixZQUFNckMsR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsV0FBS2dDLFFBQUwsQ0FBYztBQUFDSSxRQUFBQSxNQUFNLEVBQUUsTUFBTXRDLEdBQUcsQ0FBQ3VDLGVBQUosQ0FBb0J2QyxHQUFHLENBQUN3QyxTQUFKLEVBQXBCLEVBQXFDbEQsT0FBTyxDQUFDYSxPQUFSLENBQWdCc0MsUUFBckQ7QUFBZixPQUFkO0FBQ0g7QUFDSjs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkJDLElBQUFBLGFBQWEsQ0FBQyxLQUFLWixlQUFOLENBQWI7QUFDQSxVQUFNO0FBQUN6QyxNQUFBQTtBQUFELFFBQVksS0FBS0QsS0FBdkI7QUFDQUMsSUFBQUEsT0FBTyxDQUFDc0QsR0FBUixDQUFZLFFBQVosRUFBc0IsS0FBS1Isc0JBQTNCO0FBQ0g7O0FBa0REUyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxVQUFVLEdBQUczQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQW5CO0FBQ0EsVUFBTTtBQUFDOUIsTUFBQUE7QUFBRCxRQUFZLEtBQUtELEtBQXZCO0FBQ0EsUUFBSTBELFNBQUo7O0FBQ0EsUUFBSXpELE9BQU8sQ0FBQytDLGtCQUFaLEVBQWdDO0FBQzVCLFVBQUksS0FBS1osS0FBTCxDQUFXYSxNQUFmLEVBQXVCO0FBQ25CUyxRQUFBQSxTQUFTLEdBQUcseUJBQUcsb0NBQUgsRUFBeUM7QUFDakRDLFVBQUFBLFVBQVUsRUFBRSxLQUFLdkIsS0FBTCxDQUFXYSxNQUFYLENBQWtCVyxjQUFsQixFQURxQztBQUVqRFIsVUFBQUEsUUFBUSxFQUFFLEtBQUtoQixLQUFMLENBQVdhLE1BQVgsQ0FBa0JHO0FBRnFCLFNBQXpDLENBQVo7QUFJSDtBQUNKLEtBUEQsTUFPTztBQUNILFlBQU1TLE1BQU0sR0FBRzVELE9BQU8sQ0FBQzJCLFdBQXZCO0FBQ0EsWUFBTWIsTUFBTSxHQUFHZCxPQUFPLENBQUNhLE9BQVIsQ0FBZ0JDLE1BQS9CO0FBQ0EyQyxNQUFBQSxTQUFTLEdBQUczQyxNQUFNLEdBQUcseURBQXNCOEMsTUFBdEIsRUFBOEI5QyxNQUE5QixDQUFILEdBQTJDOEMsTUFBN0QsQ0FIRyxDQUlIOztBQUNBLFVBQUlILFNBQVMsS0FBS0csTUFBbEIsRUFBMEI7QUFDdEIsY0FBTUMsTUFBTSxHQUFHbEQsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLGNBQU1rRCxJQUFJLEdBQUdELE1BQU0sQ0FBQ25DLE9BQVAsQ0FBZWtDLE1BQWYsQ0FBYjs7QUFDQSxZQUFJRSxJQUFJLElBQUlBLElBQUksQ0FBQ0MsV0FBakIsRUFBOEI7QUFDMUJOLFVBQUFBLFNBQVMsR0FBRyx5QkFBRyx1QkFBSCxFQUE0QjtBQUFDTyxZQUFBQSxJQUFJLEVBQUVGLElBQUksQ0FBQ0MsV0FBWjtBQUF5QkgsWUFBQUE7QUFBekIsV0FBNUIsQ0FBWjtBQUNIO0FBQ0o7QUFDSjs7QUFDRCxVQUFNSyxZQUFZLEdBQUcsS0FBSzlCLEtBQUwsQ0FBV0MsT0FBWCxJQUFzQixDQUF0QixHQUNqQix5QkFBRyxTQUFILENBRGlCLEdBRWpCLHlCQUFHLHVCQUFILEVBQTRCO0FBQUNBLE1BQUFBLE9BQU8sRUFBRSxLQUFLRCxLQUFMLENBQVdDO0FBQXJCLEtBQTVCLENBRko7QUFHQSxXQUFRLDBDQUNKO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUF1Q3FCLFNBQXZDLENBREksRUFFSjtBQUFLLE1BQUEsU0FBUyxFQUFDLGtCQUFmO0FBQWtDLG1CQUFVO0FBQTVDLE9BQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsS0FBSyxFQUFFUSxZQUFuQjtBQUFpQyxNQUFBLElBQUksRUFBQyxRQUF0QztBQUErQyxNQUFBLE9BQU8sRUFBRSxLQUFLM0Q7QUFBN0QsTUFESixFQUVJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLEtBQUssRUFBRSx5QkFBRyxRQUFILENBQW5CO0FBQWlDLE1BQUEsT0FBTyxFQUFFLEtBQUsyQjtBQUEvQyxNQUZKLENBRkksQ0FBUjtBQU9IOztBQXRIcUU7OztBQXlIMUV0Qyx3QkFBd0IsQ0FBQ3VFLFNBQXpCLEdBQXFDO0FBQ2pDbEUsRUFBQUEsT0FBTyxFQUFFbUUsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRE87QUFFakNoRSxFQUFBQSxRQUFRLEVBQUU4RCxtQkFBVUcsTUFBVixDQUFpQkQ7QUFGTSxDQUFyQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCB7UklHSFRfUEFORUxfUEhBU0VTfSBmcm9tIFwiLi4vLi4vLi4vc3RvcmVzL1JpZ2h0UGFuZWxTdG9yZVBoYXNlc1wiO1xyXG5pbXBvcnQge3VzZXJMYWJlbEZvckV2ZW50Um9vbX0gZnJvbSBcIi4uLy4uLy4uL3V0aWxzL0tleVZlcmlmaWNhdGlvblN0YXRlT2JzZXJ2ZXJcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQgVG9hc3RTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc3RvcmVzL1RvYXN0U3RvcmVcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi9Nb2RhbFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVmVyaWZpY2F0aW9uUmVxdWVzdFRvYXN0IGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge2NvdW50ZXI6IE1hdGguY2VpbChwcm9wcy5yZXF1ZXN0LnRpbWVvdXQgLyAxMDAwKX07XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgY29uc3Qge3JlcXVlc3R9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAocmVxdWVzdC50aW1lb3V0ICYmIHJlcXVlc3QudGltZW91dCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5faW50ZXJ2YWxIYW5kbGUgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQge2NvdW50ZXJ9ID0gdGhpcy5zdGF0ZTtcclxuICAgICAgICAgICAgICAgIGNvdW50ZXIgPSBNYXRoLm1heCgwLCBjb3VudGVyIC0gMSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjb3VudGVyfSk7XHJcbiAgICAgICAgICAgIH0sIDEwMDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXF1ZXN0Lm9uKFwiY2hhbmdlXCIsIHRoaXMuX2NoZWNrUmVxdWVzdElzUGVuZGluZyk7XHJcbiAgICAgICAgLy8gV2Ugc2hvdWxkIHByb2JhYmx5IGhhdmUgYSBzZXBhcmF0ZSBjbGFzcyBtYW5hZ2luZyB0aGUgYWN0aXZlIHZlcmlmaWNhdGlvbiB0b2FzdHMsXHJcbiAgICAgICAgLy8gcmF0aGVyIHRoYW4gbW9uaXRvcmluZyB0aGlzIGluIHRoZSB0b2FzdCBjb21wb25lbnQgaXRzZWxmLCBzaW5jZSB3ZSdsbCBnZXQgcHJvYmxlbXNcclxuICAgICAgICAvLyBsaWtlIHRoZSB0b2FzZHQgbm90IGdvaW5nIGF3YXkgd2hlbiB0aGUgdmVyaWZpY2F0aW9uIGlzIGNhbmNlbGxlZCB1bmxlc3MgaXQncyB0aGVcclxuICAgICAgICAvLyBvbmUgb24gdGhlIHRvcCAoaWUuIHRoZSBvbmUgdGhhdCdzIG1vdW50ZWQpLlxyXG4gICAgICAgIC8vIEFzIGEgcXVpY2sgJiBkaXJ0eSBmaXgsIGNoZWNrIHRoZSB0b2FzdCBpcyBzdGlsbCByZWxldmFudCB3aGVuIGl0IG1vdW50cyAodGhpcyBwcmV2ZW50c1xyXG4gICAgICAgIC8vIGEgdG9hc3QgaGFuZ2luZyBhcm91bmQgYWZ0ZXIgbG9nZ2luZyBpbiBpZiB5b3UgZGlkIGEgdmVyaWZpY2F0aW9uIGFzIHBhcnQgb2YgbG9naW4pLlxyXG4gICAgICAgIHRoaXMuX2NoZWNrUmVxdWVzdElzUGVuZGluZygpO1xyXG5cclxuICAgICAgICBpZiAocmVxdWVzdC5pc1NlbGZWZXJpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZXZpY2U6IGF3YWl0IGNsaS5nZXRTdG9yZWREZXZpY2UoY2xpLmdldFVzZXJJZCgpLCByZXF1ZXN0LmNoYW5uZWwuZGV2aWNlSWQpfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5faW50ZXJ2YWxIYW5kbGUpO1xyXG4gICAgICAgIGNvbnN0IHtyZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgcmVxdWVzdC5vZmYoXCJjaGFuZ2VcIiwgdGhpcy5fY2hlY2tSZXF1ZXN0SXNQZW5kaW5nKTtcclxuICAgIH1cclxuXHJcbiAgICBfY2hlY2tSZXF1ZXN0SXNQZW5kaW5nID0gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHtyZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgaWYgKCFyZXF1ZXN0LmNhbkFjY2VwdCkge1xyXG4gICAgICAgICAgICBUb2FzdFN0b3JlLnNoYXJlZEluc3RhbmNlKCkuZGlzbWlzc1RvYXN0KHRoaXMucHJvcHMudG9hc3RLZXkpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgY2FuY2VsID0gKCkgPT4ge1xyXG4gICAgICAgIFRvYXN0U3RvcmUuc2hhcmVkSW5zdGFuY2UoKS5kaXNtaXNzVG9hc3QodGhpcy5wcm9wcy50b2FzdEtleSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5yZXF1ZXN0LmNhbmNlbCgpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igd2hpbGUgY2FuY2VsbGluZyB2ZXJpZmljYXRpb24gcmVxdWVzdFwiLCBlcnIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhY2NlcHQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgVG9hc3RTdG9yZS5zaGFyZWRJbnN0YW5jZSgpLmRpc21pc3NUb2FzdCh0aGlzLnByb3BzLnRvYXN0S2V5KTtcclxuICAgICAgICBjb25zdCB7cmVxdWVzdH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIC8vIG5vIHJvb20gaWQgZm9yIHRvX2RldmljZSByZXF1ZXN0c1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdC5jaGFubmVsLnJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21faWQ6IHJlcXVlc3QuY2hhbm5lbC5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgc2hvdWxkX3BlZWs6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogXCJzZXRfcmlnaHRfcGFuZWxfcGhhc2VcIixcclxuICAgICAgICAgICAgICAgICAgICBwaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLkVuY3J5cHRpb25QYW5lbCxcclxuICAgICAgICAgICAgICAgICAgICByZWZpcmVQYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVyaWZpY2F0aW9uUmVxdWVzdDogcmVxdWVzdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyOiBjbGkuZ2V0VXNlcihyZXF1ZXN0Lm90aGVyVXNlcklkKSxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBWZXJpZmljYXRpb25SZXF1ZXN0RGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmRpYWxvZ3MuVmVyaWZpY2F0aW9uUmVxdWVzdERpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0luY29taW5nIFZlcmlmaWNhdGlvbicsICcnLCBWZXJpZmljYXRpb25SZXF1ZXN0RGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmVyaWZpY2F0aW9uUmVxdWVzdDogcmVxdWVzdCxcclxuICAgICAgICAgICAgICAgIH0sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF3YWl0IHJlcXVlc3QuYWNjZXB0KCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyLm1lc3NhZ2UpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEZvcm1CdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuRm9ybUJ1dHRvblwiKTtcclxuICAgICAgICBjb25zdCB7cmVxdWVzdH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGxldCBuYW1lTGFiZWw7XHJcbiAgICAgICAgaWYgKHJlcXVlc3QuaXNTZWxmVmVyaWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmRldmljZSkge1xyXG4gICAgICAgICAgICAgICAgbmFtZUxhYmVsID0gX3QoXCJGcm9tICUoZGV2aWNlTmFtZSlzICglKGRldmljZUlkKXMpXCIsIHtcclxuICAgICAgICAgICAgICAgICAgICBkZXZpY2VOYW1lOiB0aGlzLnN0YXRlLmRldmljZS5nZXREaXNwbGF5TmFtZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRldmljZUlkOiB0aGlzLnN0YXRlLmRldmljZS5kZXZpY2VJZCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgdXNlcklkID0gcmVxdWVzdC5vdGhlclVzZXJJZDtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbUlkID0gcmVxdWVzdC5jaGFubmVsLnJvb21JZDtcclxuICAgICAgICAgICAgbmFtZUxhYmVsID0gcm9vbUlkID8gdXNlckxhYmVsRm9yRXZlbnRSb29tKHVzZXJJZCwgcm9vbUlkKSA6IHVzZXJJZDtcclxuICAgICAgICAgICAgLy8gZm9yIGxlZ2FjeSB0b19kZXZpY2UgdmVyaWZpY2F0aW9uIHJlcXVlc3RzXHJcbiAgICAgICAgICAgIGlmIChuYW1lTGFiZWwgPT09IHVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXNlciA9IGNsaWVudC5nZXRVc2VyKHVzZXJJZCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlciAmJiB1c2VyLmRpc3BsYXlOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZUxhYmVsID0gX3QoXCIlKG5hbWUpcyAoJSh1c2VySWQpcylcIiwge25hbWU6IHVzZXIuZGlzcGxheU5hbWUsIHVzZXJJZH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGRlY2xpbmVMYWJlbCA9IHRoaXMuc3RhdGUuY291bnRlciA9PSAwID9cclxuICAgICAgICAgICAgX3QoXCJEZWNsaW5lXCIpIDpcclxuICAgICAgICAgICAgX3QoXCJEZWNsaW5lICglKGNvdW50ZXIpcylcIiwge2NvdW50ZXI6IHRoaXMuc3RhdGUuY291bnRlcn0pO1xyXG4gICAgICAgIHJldHVybiAoPGRpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Ub2FzdF9kZXNjcmlwdGlvblwiPntuYW1lTGFiZWx9PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVG9hc3RfYnV0dG9uc1wiIGFyaWEtbGl2ZT1cIm9mZlwiPlxyXG4gICAgICAgICAgICAgICAgPEZvcm1CdXR0b24gbGFiZWw9e2RlY2xpbmVMYWJlbH0ga2luZD1cImRhbmdlclwiIG9uQ2xpY2s9e3RoaXMuY2FuY2VsfSAvPlxyXG4gICAgICAgICAgICAgICAgPEZvcm1CdXR0b24gbGFiZWw9e190KFwiQWNjZXB0XCIpfSBvbkNsaWNrPXt0aGlzLmFjY2VwdH0gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+KTtcclxuICAgIH1cclxufVxyXG5cclxuVmVyaWZpY2F0aW9uUmVxdWVzdFRvYXN0LnByb3BUeXBlcyA9IHtcclxuICAgIHJlcXVlc3Q6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgIHRvYXN0S2V5OiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbn07XHJcbiJdfQ==