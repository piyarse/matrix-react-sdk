"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.useDevices = exports.getE2EStatus = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _matrixJsSdk = require("matrix-js-sdk");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _createRoom = _interopRequireDefault(require("../../../createRoom"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _AutoHideScrollbar = _interopRequireDefault(require("../../structures/AutoHideScrollbar"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _MultiInviter = _interopRequireDefault(require("../../../utils/MultiInviter"));

var _GroupStore = _interopRequireDefault(require("../../../stores/GroupStore"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _E2EIcon = _interopRequireDefault(require("../rooms/E2EIcon"));

var _useEventEmitter = require("../../../hooks/useEventEmitter");

var _Roles = require("../../../Roles");

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

var _RightPanelStorePhases = require("../../../stores/RightPanelStorePhases");

var _EncryptionPanel = _interopRequireDefault(require("./EncryptionPanel"));

var _useAsyncMemo = require("../../../hooks/useAsyncMemo");

var _verification = require("../../../verification");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 Vector Creations Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const _disambiguateDevices = devices => {
  const names = Object.create(null);

  for (let i = 0; i < devices.length; i++) {
    const name = devices[i].getDisplayName();
    const indexList = names[name] || [];
    indexList.push(i);
    names[name] = indexList;
  }

  for (const name in names) {
    if (names[name].length > 1) {
      names[name].forEach(j => {
        devices[j].ambiguous = true;
      });
    }
  }
};

const getE2EStatus = (cli, userId, devices) => {
  if (!_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
    const hasUnverifiedDevice = devices.some(device => device.isUnverified());
    return hasUnverifiedDevice ? "warning" : "verified";
  }

  const isMe = userId === cli.getUserId();
  const userTrust = cli.checkUserTrust(userId);

  if (!userTrust.isCrossSigningVerified()) {
    return userTrust.wasCrossSigningVerified() ? "warning" : "normal";
  }

  const anyDeviceUnverified = devices.some(device => {
    const {
      deviceId
    } = device; // For your own devices, we use the stricter check of cross-signing
    // verification to encourage everyone to trust their own devices via
    // cross-signing so that other users can then safely trust you.
    // For other people's devices, the more general verified check that
    // includes locally verified devices can be used.

    const deviceTrust = cli.checkDeviceTrust(userId, deviceId);
    return isMe ? !deviceTrust.isCrossSigningVerified() : !deviceTrust.isVerified();
  });
  return anyDeviceUnverified ? "warning" : "verified";
};

exports.getE2EStatus = getE2EStatus;

async function openDMForUser(matrixClient, userId) {
  const dmRooms = _DMRoomMap.default.shared().getDMRoomsForUserId(userId);

  const lastActiveRoom = dmRooms.reduce((lastActiveRoom, roomId) => {
    const room = matrixClient.getRoom(roomId);

    if (!room || room.getMyMembership() === "leave") {
      return lastActiveRoom;
    }

    if (!lastActiveRoom || lastActiveRoom.getLastActiveTimestamp() < room.getLastActiveTimestamp()) {
      return room;
    }

    return lastActiveRoom;
  }, null);

  if (lastActiveRoom) {
    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: lastActiveRoom.roomId
    });

    return;
  }

  const createRoomOptions = {
    dmUserId: userId
  };

  if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
    // Check whether all users have uploaded device keys before.
    // If so, enable encryption in the new room.
    const usersToDevicesMap = await matrixClient.downloadKeys([userId]);
    const allHaveDeviceKeys = Object.values(usersToDevicesMap).every(devices => {
      // `devices` is an object of the form { deviceId: deviceInfo, ... }.
      return Object.keys(devices).length > 0;
    });

    if (allHaveDeviceKeys) {
      createRoomOptions.encryption = true;
    }
  }

  (0, _createRoom.default)(createRoomOptions);
}

function useIsEncrypted(cli, room) {
  const [isEncrypted, setIsEncrypted] = (0, _react.useState)(room ? cli.isRoomEncrypted(room.roomId) : undefined);
  const update = (0, _react.useCallback)(event => {
    if (event.getType() === "m.room.encryption") {
      setIsEncrypted(cli.isRoomEncrypted(room.roomId));
    }
  }, [cli, room]);
  (0, _useEventEmitter.useEventEmitter)(room ? room.currentState : undefined, "RoomState.events", update);
  return isEncrypted;
}

function useHasCrossSigningKeys(cli, member, canVerify, setUpdating) {
  return (0, _useAsyncMemo.useAsyncMemo)(async () => {
    if (!canVerify) {
      return false;
    }

    setUpdating(true);

    try {
      await cli.downloadKeys([member.userId]);
      const xsi = cli.getStoredCrossSigningForUser(member.userId);
      const key = xsi && xsi.getId();
      return !!key;
    } finally {
      setUpdating(false);
    }
  }, [cli, member, canVerify], false);
}

function DeviceItem({
  userId,
  device
}) {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  const isMe = userId === cli.getUserId();
  const deviceTrust = cli.checkDeviceTrust(userId, device.deviceId);
  const userTrust = cli.checkUserTrust(userId); // For your own devices, we use the stricter check of cross-signing
  // verification to encourage everyone to trust their own devices via
  // cross-signing so that other users can then safely trust you.
  // For other people's devices, the more general verified check that
  // includes locally verified devices can be used.

  const isVerified = isMe && _SettingsStore.default.isFeatureEnabled("feature_cross_signing") ? deviceTrust.isCrossSigningVerified() : deviceTrust.isVerified();
  const classes = (0, _classnames.default)("mx_UserInfo_device", {
    mx_UserInfo_device_verified: isVerified,
    mx_UserInfo_device_unverified: !isVerified
  });
  const iconClasses = (0, _classnames.default)("mx_E2EIcon", {
    mx_E2EIcon_normal: !userTrust.isVerified(),
    mx_E2EIcon_verified: isVerified,
    mx_E2EIcon_warning: userTrust.isVerified() && !isVerified
  });

  const onDeviceClick = () => {
    if (!isVerified) {
      (0, _verification.verifyDevice)(cli.getUser(userId), device);
    }
  };

  const deviceName = device.ambiguous ? (device.getDisplayName() ? device.getDisplayName() : "") + " (" + device.deviceId + ")" : device.getDisplayName();
  let trustedLabel = null;
  if (userTrust.isVerified()) trustedLabel = isVerified ? (0, _languageHandler._t)("Trusted") : (0, _languageHandler._t)("Not trusted");
  return _react.default.createElement(_AccessibleButton.default, {
    className: classes,
    title: device.deviceId,
    onClick: onDeviceClick
  }, _react.default.createElement("div", {
    className: iconClasses
  }), _react.default.createElement("div", {
    className: "mx_UserInfo_device_name"
  }, deviceName), _react.default.createElement("div", {
    className: "mx_UserInfo_device_trusted"
  }, trustedLabel));
}

function DevicesSection({
  devices,
  userId,
  loading
}) {
  const Spinner = sdk.getComponent("elements.Spinner");
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  const userTrust = cli.checkUserTrust(userId);
  const [isExpanded, setExpanded] = (0, _react.useState)(false);

  if (loading) {
    // still loading
    return _react.default.createElement(Spinner, null);
  }

  if (devices === null) {
    return (0, _languageHandler._t)("Unable to load session list");
  }

  const isMe = userId === cli.getUserId();
  const deviceTrusts = devices.map(d => cli.checkDeviceTrust(userId, d.deviceId));
  let expandSectionDevices = [];
  const unverifiedDevices = [];
  let expandCountCaption;
  let expandHideCaption;
  let expandIconClasses = "mx_E2EIcon";

  if (userTrust.isVerified()) {
    for (let i = 0; i < devices.length; ++i) {
      const device = devices[i];
      const deviceTrust = deviceTrusts[i]; // For your own devices, we use the stricter check of cross-signing
      // verification to encourage everyone to trust their own devices via
      // cross-signing so that other users can then safely trust you.
      // For other people's devices, the more general verified check that
      // includes locally verified devices can be used.

      const isVerified = isMe && _SettingsStore.default.isFeatureEnabled("feature_cross_signing") ? deviceTrust.isCrossSigningVerified() : deviceTrust.isVerified();

      if (isVerified) {
        expandSectionDevices.push(device);
      } else {
        unverifiedDevices.push(device);
      }
    }

    expandCountCaption = (0, _languageHandler._t)("%(count)s verified sessions", {
      count: expandSectionDevices.length
    });
    expandHideCaption = (0, _languageHandler._t)("Hide verified sessions");
    expandIconClasses += " mx_E2EIcon_verified";
  } else {
    expandSectionDevices = devices;
    expandCountCaption = (0, _languageHandler._t)("%(count)s sessions", {
      count: devices.length
    });
    expandHideCaption = (0, _languageHandler._t)("Hide sessions");
    expandIconClasses += " mx_E2EIcon_normal";
  }

  let expandButton;

  if (expandSectionDevices.length) {
    if (isExpanded) {
      expandButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_UserInfo_expand mx_linkButton",
        onClick: () => setExpanded(false)
      }, _react.default.createElement("div", null, expandHideCaption));
    } else {
      expandButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_UserInfo_expand mx_linkButton",
        onClick: () => setExpanded(true)
      }, _react.default.createElement("div", {
        className: expandIconClasses
      }), _react.default.createElement("div", null, expandCountCaption));
    }
  }

  let deviceList = unverifiedDevices.map((device, i) => {
    return _react.default.createElement(DeviceItem, {
      key: i,
      userId: userId,
      device: device
    });
  });

  if (isExpanded) {
    const keyStart = unverifiedDevices.length;
    deviceList = deviceList.concat(expandSectionDevices.map((device, i) => {
      return _react.default.createElement(DeviceItem, {
        key: i + keyStart,
        userId: userId,
        device: device
      });
    }));
  }

  return _react.default.createElement("div", {
    className: "mx_UserInfo_devices"
  }, _react.default.createElement("div", null, deviceList), _react.default.createElement("div", null, expandButton));
}

const UserOptionsSection = ({
  member,
  isIgnored,
  canInvite,
  devices
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  let ignoreButton = null;
  let insertPillButton = null;
  let inviteUserButton = null;
  let readReceiptButton = null;
  const isMe = member.userId === cli.getUserId();

  const onShareUserClick = () => {
    const ShareDialog = sdk.getComponent("dialogs.ShareDialog");

    _Modal.default.createTrackedDialog('share room member dialog', '', ShareDialog, {
      target: member
    });
  }; // Only allow the user to ignore the user if its not ourselves
  // same goes for jumping to read receipt


  if (!isMe) {
    const onIgnoreToggle = () => {
      const ignoredUsers = cli.getIgnoredUsers();

      if (isIgnored) {
        const index = ignoredUsers.indexOf(member.userId);
        if (index !== -1) ignoredUsers.splice(index, 1);
      } else {
        ignoredUsers.push(member.userId);
      }

      cli.setIgnoredUsers(ignoredUsers);
    };

    ignoreButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: onIgnoreToggle,
      className: (0, _classnames.default)("mx_UserInfo_field", {
        mx_UserInfo_destructive: !isIgnored
      })
    }, isIgnored ? (0, _languageHandler._t)("Unignore") : (0, _languageHandler._t)("Ignore"));

    if (member.roomId) {
      const onReadReceiptButton = function () {
        const room = cli.getRoom(member.roomId);

        _dispatcher.default.dispatch({
          action: 'view_room',
          highlighted: true,
          event_id: room.getEventReadUpTo(member.userId),
          room_id: member.roomId
        });
      };

      const onInsertPillButton = function () {
        _dispatcher.default.dispatch({
          action: 'insert_mention',
          user_id: member.userId
        });
      };

      readReceiptButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: onReadReceiptButton,
        className: "mx_UserInfo_field"
      }, (0, _languageHandler._t)('Jump to read receipt'));
      insertPillButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: onInsertPillButton,
        className: "mx_UserInfo_field"
      }, (0, _languageHandler._t)('Mention'));
    }

    if (canInvite && (!member || !member.membership || member.membership === 'leave')) {
      const roomId = member && member.roomId ? member.roomId : _RoomViewStore.default.getRoomId();

      const onInviteUserButton = async () => {
        try {
          // We use a MultiInviter to re-use the invite logic, even though
          // we're only inviting one user.
          const inviter = new _MultiInviter.default(roomId);
          await inviter.invite([member.userId]).then(() => {
            if (inviter.getCompletionState(member.userId) !== "invited") {
              throw new Error(inviter.getErrorText(member.userId));
            }
          });
        } catch (err) {
          const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

          _Modal.default.createTrackedDialog('Failed to invite', '', ErrorDialog, {
            title: (0, _languageHandler._t)('Failed to invite'),
            description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
          });
        }
      };

      inviteUserButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: onInviteUserButton,
        className: "mx_UserInfo_field"
      }, (0, _languageHandler._t)('Invite'));
    }
  }

  const shareUserButton = _react.default.createElement(_AccessibleButton.default, {
    onClick: onShareUserClick,
    className: "mx_UserInfo_field"
  }, (0, _languageHandler._t)('Share Link to User'));

  let directMessageButton;

  if (!isMe) {
    directMessageButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: () => openDMForUser(cli, member.userId),
      className: "mx_UserInfo_field"
    }, (0, _languageHandler._t)('Direct message'));
  }

  return _react.default.createElement("div", {
    className: "mx_UserInfo_container"
  }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Options")), _react.default.createElement("div", null, directMessageButton, readReceiptButton, shareUserButton, insertPillButton, inviteUserButton, ignoreButton));
};

const _warnSelfDemote = async () => {
  const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

  const {
    finished
  } = _Modal.default.createTrackedDialog('Demoting Self', '', QuestionDialog, {
    title: (0, _languageHandler._t)("Demote yourself?"),
    description: _react.default.createElement("div", null, (0, _languageHandler._t)("You will not be able to undo this change as you are demoting yourself, " + "if you are the last privileged user in the room it will be impossible " + "to regain privileges.")),
    button: (0, _languageHandler._t)("Demote")
  });

  const [confirmed] = await finished;
  return confirmed;
};

const GenericAdminToolsContainer = ({
  children
}) => {
  return _react.default.createElement("div", {
    className: "mx_UserInfo_container"
  }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Admin Tools")), _react.default.createElement("div", {
    className: "mx_UserInfo_buttons"
  }, children));
};

const _isMuted = (member, powerLevelContent) => {
  if (!powerLevelContent || !member) return false;
  const levelToSend = (powerLevelContent.events ? powerLevelContent.events["m.room.message"] : null) || powerLevelContent.events_default;
  return member.powerLevel < levelToSend;
};

const useRoomPowerLevels = (cli, room) => {
  const [powerLevels, setPowerLevels] = (0, _react.useState)({});
  const update = (0, _react.useCallback)(() => {
    if (!room) {
      return;
    }

    const event = room.currentState.getStateEvents("m.room.power_levels", "");

    if (event) {
      setPowerLevels(event.getContent());
    } else {
      setPowerLevels({});
    }

    return () => {
      setPowerLevels({});
    };
  }, [room]);
  (0, _useEventEmitter.useEventEmitter)(cli, "RoomState.members", update);
  (0, _react.useEffect)(() => {
    update();
    return () => {
      setPowerLevels({});
    };
  }, [update]);
  return powerLevels;
};

const RoomKickButton = ({
  member,
  startUpdating,
  stopUpdating
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default); // check if user can be kicked/disinvited

  if (member.membership !== "invite" && member.membership !== "join") return null;

  const onKick = async () => {
    const ConfirmUserActionDialog = sdk.getComponent("dialogs.ConfirmUserActionDialog");

    const {
      finished
    } = _Modal.default.createTrackedDialog('Confirm User Action Dialog', 'onKick', ConfirmUserActionDialog, {
      member,
      action: member.membership === "invite" ? (0, _languageHandler._t)("Disinvite") : (0, _languageHandler._t)("Kick"),
      title: member.membership === "invite" ? (0, _languageHandler._t)("Disinvite this user?") : (0, _languageHandler._t)("Kick this user?"),
      askReason: member.membership === "join",
      danger: true
    });

    const [proceed, reason] = await finished;
    if (!proceed) return;
    startUpdating();
    cli.kick(member.roomId, member.userId, reason || undefined).then(() => {
      // NO-OP; rely on the m.room.member event coming down else we could
      // get out of sync if we force setState here!
      console.log("Kick success");
    }, function (err) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Kick error: " + err);

      _Modal.default.createTrackedDialog('Failed to kick', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to kick"),
        description: err && err.message ? err.message : "Operation failed"
      });
    }).finally(() => {
      stopUpdating();
    });
  };

  const kickLabel = member.membership === "invite" ? (0, _languageHandler._t)("Disinvite") : (0, _languageHandler._t)("Kick");
  return _react.default.createElement(_AccessibleButton.default, {
    className: "mx_UserInfo_field mx_UserInfo_destructive",
    onClick: onKick
  }, kickLabel);
};

const RedactMessagesButton = ({
  member
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);

  const onRedactAllMessages = async () => {
    const {
      roomId,
      userId
    } = member;
    const room = cli.getRoom(roomId);

    if (!room) {
      return;
    }

    let timeline = room.getLiveTimeline();
    let eventsToRedact = [];

    while (timeline) {
      eventsToRedact = timeline.getEvents().reduce((events, event) => {
        if (event.getSender() === userId && !event.isRedacted()) {
          return events.concat(event);
        } else {
          return events;
        }
      }, eventsToRedact);
      timeline = timeline.getNeighbouringTimeline(_matrixJsSdk.EventTimeline.BACKWARDS);
    }

    const count = eventsToRedact.length;
    const user = member.name;

    if (count === 0) {
      const InfoDialog = sdk.getComponent("dialogs.InfoDialog");

      _Modal.default.createTrackedDialog('No user messages found to remove', '', InfoDialog, {
        title: (0, _languageHandler._t)("No recent messages by %(user)s found", {
          user
        }),
        description: _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Try scrolling up in the timeline to see if there are any earlier ones.")))
      });
    } else {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      const {
        finished
      } = _Modal.default.createTrackedDialog('Remove recent messages by user', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Remove recent messages by %(user)s", {
          user
        }),
        description: _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You are about to remove %(count)s messages by %(user)s. This cannot be undone. Do you wish to continue?", {
          count,
          user
        })), _react.default.createElement("p", null, (0, _languageHandler._t)("For a large amount of messages, this might take some time. Please don't refresh your client in the meantime."))),
        button: (0, _languageHandler._t)("Remove %(count)s messages", {
          count
        })
      });

      const [confirmed] = await finished;

      if (!confirmed) {
        return;
      } // Submitting a large number of redactions freezes the UI,
      // so first yield to allow to rerender after closing the dialog.


      await Promise.resolve();
      console.info("Started redacting recent ".concat(count, " messages for ").concat(user, " in ").concat(roomId));
      await Promise.all(eventsToRedact.map(async event => {
        try {
          await cli.redactEvent(roomId, event.getId());
        } catch (err) {
          // log and swallow errors
          console.error("Could not redact", event.getId());
          console.error(err);
        }
      }));
      console.info("Finished redacting recent ".concat(count, " messages for ").concat(user, " in ").concat(roomId));
    }
  };

  return _react.default.createElement(_AccessibleButton.default, {
    className: "mx_UserInfo_field mx_UserInfo_destructive",
    onClick: onRedactAllMessages
  }, (0, _languageHandler._t)("Remove recent messages"));
};

const BanToggleButton = ({
  member,
  startUpdating,
  stopUpdating
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);

  const onBanOrUnban = async () => {
    const ConfirmUserActionDialog = sdk.getComponent("dialogs.ConfirmUserActionDialog");

    const {
      finished
    } = _Modal.default.createTrackedDialog('Confirm User Action Dialog', 'onBanOrUnban', ConfirmUserActionDialog, {
      member,
      action: member.membership === 'ban' ? (0, _languageHandler._t)("Unban") : (0, _languageHandler._t)("Ban"),
      title: member.membership === 'ban' ? (0, _languageHandler._t)("Unban this user?") : (0, _languageHandler._t)("Ban this user?"),
      askReason: member.membership !== 'ban',
      danger: member.membership !== 'ban'
    });

    const [proceed, reason] = await finished;
    if (!proceed) return;
    startUpdating();
    let promise;

    if (member.membership === 'ban') {
      promise = cli.unban(member.roomId, member.userId);
    } else {
      promise = cli.ban(member.roomId, member.userId, reason || undefined);
    }

    promise.then(() => {
      // NO-OP; rely on the m.room.member event coming down else we could
      // get out of sync if we force setState here!
      console.log("Ban success");
    }, function (err) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Ban error: " + err);

      _Modal.default.createTrackedDialog('Failed to ban user', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Failed to ban user")
      });
    }).finally(() => {
      stopUpdating();
    });
  };

  let label = (0, _languageHandler._t)("Ban");

  if (member.membership === 'ban') {
    label = (0, _languageHandler._t)("Unban");
  }

  const classes = (0, _classnames.default)("mx_UserInfo_field", {
    mx_UserInfo_destructive: member.membership !== 'ban'
  });
  return _react.default.createElement(_AccessibleButton.default, {
    className: classes,
    onClick: onBanOrUnban
  }, label);
};

const MuteToggleButton = ({
  member,
  room,
  powerLevels,
  startUpdating,
  stopUpdating
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default); // Don't show the mute/unmute option if the user is not in the room

  if (member.membership !== "join") return null;

  const isMuted = _isMuted(member, powerLevels);

  const onMuteToggle = async () => {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const roomId = member.roomId;
    const target = member.userId; // if muting self, warn as it may be irreversible

    if (target === cli.getUserId()) {
      try {
        if (!(await _warnSelfDemote())) return;
      } catch (e) {
        console.error("Failed to warn about self demotion: ", e);
        return;
      }
    }

    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevelEvent) return;
    const powerLevels = powerLevelEvent.getContent();
    const levelToSend = (powerLevels.events ? powerLevels.events["m.room.message"] : null) || powerLevels.events_default;
    let level;

    if (isMuted) {
      // unmute
      level = levelToSend;
    } else {
      // mute
      level = levelToSend - 1;
    }

    level = parseInt(level);

    if (!isNaN(level)) {
      startUpdating();
      cli.setPowerLevel(roomId, target, level, powerLevelEvent).then(() => {
        // NO-OP; rely on the m.room.member event coming down else we could
        // get out of sync if we force setState here!
        console.log("Mute toggle success");
      }, function (err) {
        console.error("Mute error: " + err);

        _Modal.default.createTrackedDialog('Failed to mute user', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Error"),
          description: (0, _languageHandler._t)("Failed to mute user")
        });
      }).finally(() => {
        stopUpdating();
      });
    }
  };

  const classes = (0, _classnames.default)("mx_UserInfo_field", {
    mx_UserInfo_destructive: !isMuted
  });
  const muteLabel = isMuted ? (0, _languageHandler._t)("Unmute") : (0, _languageHandler._t)("Mute");
  return _react.default.createElement(_AccessibleButton.default, {
    className: classes,
    onClick: onMuteToggle
  }, muteLabel);
};

const RoomAdminToolsContainer = ({
  room,
  children,
  member,
  startUpdating,
  stopUpdating,
  powerLevels
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  let kickButton;
  let banButton;
  let muteButton;
  let redactButton;
  const editPowerLevel = (powerLevels.events ? powerLevels.events["m.room.power_levels"] : null) || powerLevels.state_default;
  const me = room.getMember(cli.getUserId());
  const isMe = me.userId === member.userId;
  const canAffectUser = member.powerLevel < me.powerLevel || isMe;

  if (canAffectUser && me.powerLevel >= powerLevels.kick) {
    kickButton = _react.default.createElement(RoomKickButton, {
      member: member,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    });
  }

  if (me.powerLevel >= powerLevels.redact) {
    redactButton = _react.default.createElement(RedactMessagesButton, {
      member: member,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    });
  }

  if (canAffectUser && me.powerLevel >= powerLevels.ban) {
    banButton = _react.default.createElement(BanToggleButton, {
      member: member,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    });
  }

  if (canAffectUser && me.powerLevel >= editPowerLevel) {
    muteButton = _react.default.createElement(MuteToggleButton, {
      member: member,
      room: room,
      powerLevels: powerLevels,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    });
  }

  if (kickButton || banButton || muteButton || redactButton || children) {
    return _react.default.createElement(GenericAdminToolsContainer, null, muteButton, kickButton, banButton, redactButton, children);
  }

  return _react.default.createElement("div", null);
};

const GroupAdminToolsSection = ({
  children,
  groupId,
  groupMember,
  startUpdating,
  stopUpdating
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  const [isPrivileged, setIsPrivileged] = (0, _react.useState)(false);
  const [isInvited, setIsInvited] = (0, _react.useState)(false); // Listen to group store changes

  (0, _react.useEffect)(() => {
    let unmounted = false;

    const onGroupStoreUpdated = () => {
      if (unmounted) return;
      setIsPrivileged(_GroupStore.default.isUserPrivileged(groupId));
      setIsInvited(_GroupStore.default.getGroupInvitedMembers(groupId).some(m => m.userId === groupMember.userId));
    };

    _GroupStore.default.registerListener(groupId, onGroupStoreUpdated);

    onGroupStoreUpdated(); // Handle unmount

    return () => {
      unmounted = true;

      _GroupStore.default.unregisterListener(onGroupStoreUpdated);
    };
  }, [groupId, groupMember.userId]);

  if (isPrivileged) {
    const _onKick = async () => {
      const ConfirmUserActionDialog = sdk.getComponent("dialogs.ConfirmUserActionDialog");

      const {
        finished
      } = _Modal.default.createDialog(ConfirmUserActionDialog, {
        matrixClient: cli,
        groupMember,
        action: isInvited ? (0, _languageHandler._t)('Disinvite') : (0, _languageHandler._t)('Remove from community'),
        title: isInvited ? (0, _languageHandler._t)('Disinvite this user from community?') : (0, _languageHandler._t)('Remove this user from community?'),
        danger: true
      });

      const [proceed] = await finished;
      if (!proceed) return;
      startUpdating();
      cli.removeUserFromGroup(groupId, groupMember.userId).then(() => {
        // return to the user list
        _dispatcher.default.dispatch({
          action: "view_user",
          member: null
        });
      }).catch(e => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Failed to remove user from group', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Error'),
          description: isInvited ? (0, _languageHandler._t)('Failed to withdraw invitation') : (0, _languageHandler._t)('Failed to remove user from community')
        });

        console.log(e);
      }).finally(() => {
        stopUpdating();
      });
    };

    const kickButton = _react.default.createElement(_AccessibleButton.default, {
      className: "mx_UserInfo_field mx_UserInfo_destructive",
      onClick: _onKick
    }, isInvited ? (0, _languageHandler._t)('Disinvite') : (0, _languageHandler._t)('Remove from community')); // No make/revoke admin API yet

    /*const opLabel = this.state.isTargetMod ? _t("Revoke Moderator") : _t("Make Moderator");
    giveModButton = <AccessibleButton className="mx_UserInfo_field" onClick={this.onModToggle}>
        {giveOpLabel}
    </AccessibleButton>;*/


    return _react.default.createElement(GenericAdminToolsContainer, null, kickButton, children);
  }

  return _react.default.createElement("div", null);
};

const GroupMember = _propTypes.default.shape({
  userId: _propTypes.default.string.isRequired,
  displayname: _propTypes.default.string,
  // XXX: GroupMember objects are inconsistent :((
  avatarUrl: _propTypes.default.string
});

const useIsSynapseAdmin = cli => {
  const [isAdmin, setIsAdmin] = (0, _react.useState)(false);
  (0, _react.useEffect)(() => {
    cli.isSynapseAdministrator().then(isAdmin => {
      setIsAdmin(isAdmin);
    }, () => {
      setIsAdmin(false);
    });
  }, [cli]);
  return isAdmin;
};

const useHomeserverSupportsCrossSigning = cli => {
  return (0, _useAsyncMemo.useAsyncMemo)(async () => {
    return cli.doesServerSupportUnstableFeature("org.matrix.e2e_cross_signing");
  }, [cli], false);
};

function useRoomPermissions(cli, room, user) {
  const [roomPermissions, setRoomPermissions] = (0, _react.useState)({
    // modifyLevelMax is the max PL we can set this user to, typically min(their PL, our PL) && canSetPL
    modifyLevelMax: -1,
    canEdit: false,
    canInvite: false
  });
  const updateRoomPermissions = (0, _react.useCallback)(() => {
    if (!room) {
      return;
    }

    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevelEvent) return;
    const powerLevels = powerLevelEvent.getContent();
    if (!powerLevels) return;
    const me = room.getMember(cli.getUserId());
    if (!me) return;
    const them = user;
    const isMe = me.userId === them.userId;
    const canAffectUser = them.powerLevel < me.powerLevel || isMe;
    let modifyLevelMax = -1;

    if (canAffectUser) {
      const editPowerLevel = (powerLevels.events ? powerLevels.events["m.room.power_levels"] : null) || powerLevels.state_default;

      if (me.powerLevel >= editPowerLevel && (isMe || me.powerLevel > them.powerLevel)) {
        modifyLevelMax = me.powerLevel;
      }
    }

    setRoomPermissions({
      canInvite: me.powerLevel >= powerLevels.invite,
      canEdit: modifyLevelMax >= 0,
      modifyLevelMax
    });
  }, [cli, user, room]);
  (0, _useEventEmitter.useEventEmitter)(cli, "RoomState.members", updateRoomPermissions);
  (0, _react.useEffect)(() => {
    updateRoomPermissions();
    return () => {
      setRoomPermissions({
        maximalPowerLevel: -1,
        canEdit: false,
        canInvite: false
      });
    };
  }, [updateRoomPermissions]);
  return roomPermissions;
}

const PowerLevelSection = ({
  user,
  room,
  roomPermissions,
  powerLevels
}) => {
  const [isEditing, setEditing] = (0, _react.useState)(false);

  if (room && user.roomId) {
    // is in room
    if (isEditing) {
      return _react.default.createElement(PowerLevelEditor, {
        user: user,
        room: room,
        roomPermissions: roomPermissions,
        onFinished: () => setEditing(false)
      });
    } else {
      const IconButton = sdk.getComponent('elements.IconButton');
      const powerLevelUsersDefault = powerLevels.users_default || 0;
      const powerLevel = parseInt(user.powerLevel, 10);
      const modifyButton = roomPermissions.canEdit ? _react.default.createElement(IconButton, {
        icon: "edit",
        onClick: () => setEditing(true)
      }) : null;
      const role = (0, _Roles.textualPowerLevel)(powerLevel, powerLevelUsersDefault);
      const label = (0, _languageHandler._t)("<strong>%(role)s</strong> in %(roomName)s", {
        role,
        roomName: room.name
      }, {
        strong: label => _react.default.createElement("strong", null, label)
      });
      return _react.default.createElement("div", {
        className: "mx_UserInfo_profileField"
      }, _react.default.createElement("div", {
        className: "mx_UserInfo_roleDescription"
      }, label, modifyButton));
    }
  } else {
    return null;
  }
};

const PowerLevelEditor = ({
  user,
  room,
  roomPermissions,
  onFinished
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  const [isUpdating, setIsUpdating] = (0, _react.useState)(false);
  const [selectedPowerLevel, setSelectedPowerLevel] = (0, _react.useState)(parseInt(user.powerLevel, 10));
  const [isDirty, setIsDirty] = (0, _react.useState)(false);
  const onPowerChange = (0, _react.useCallback)(powerLevel => {
    setIsDirty(true);
    setSelectedPowerLevel(parseInt(powerLevel, 10));
  }, [setSelectedPowerLevel, setIsDirty]);
  const changePowerLevel = (0, _react.useCallback)(async () => {
    const _applyPowerChange = (roomId, target, powerLevel, powerLevelEvent) => {
      return cli.setPowerLevel(roomId, target, parseInt(powerLevel), powerLevelEvent).then(function () {
        // NO-OP; rely on the m.room.member event coming down else we could
        // get out of sync if we force setState here!
        console.log("Power change success");
      }, function (err) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to change power level " + err);

        _Modal.default.createTrackedDialog('Failed to change power level', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Error"),
          description: (0, _languageHandler._t)("Failed to change power level")
        });
      });
    };

    try {
      if (!isDirty) {
        return;
      }

      setIsUpdating(true);
      const powerLevel = selectedPowerLevel;
      const roomId = user.roomId;
      const target = user.userId;
      const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
      if (!powerLevelEvent) return;

      if (!powerLevelEvent.getContent().users) {
        _applyPowerChange(roomId, target, powerLevel, powerLevelEvent);

        return;
      }

      const myUserId = cli.getUserId();
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog"); // If we are changing our own PL it can only ever be decreasing, which we cannot reverse.

      if (myUserId === target) {
        try {
          if (!(await _warnSelfDemote())) return;
        } catch (e) {
          console.error("Failed to warn about self demotion: ", e);
        }

        await _applyPowerChange(roomId, target, powerLevel, powerLevelEvent);
        return;
      }

      const myPower = powerLevelEvent.getContent().users[myUserId];

      if (parseInt(myPower) === parseInt(powerLevel)) {
        const {
          finished
        } = _Modal.default.createTrackedDialog('Promote to PL100 Warning', '', QuestionDialog, {
          title: (0, _languageHandler._t)("Warning!"),
          description: _react.default.createElement("div", null, (0, _languageHandler._t)("You will not be able to undo this change as you are promoting the user " + "to have the same power level as yourself."), _react.default.createElement("br", null), (0, _languageHandler._t)("Are you sure?")),
          button: (0, _languageHandler._t)("Continue")
        });

        const [confirmed] = await finished;
        if (!confirmed) return;
      }

      await _applyPowerChange(roomId, target, powerLevel, powerLevelEvent);
    } finally {
      onFinished();
    }
  }, [user.roomId, user.userId, cli, selectedPowerLevel, isDirty, setIsUpdating, onFinished, room]);
  const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
  const powerLevelUsersDefault = powerLevelEvent ? powerLevelEvent.getContent().users_default : 0;
  const IconButton = sdk.getComponent('elements.IconButton');
  const Spinner = sdk.getComponent("elements.Spinner");
  const buttonOrSpinner = isUpdating ? _react.default.createElement(Spinner, {
    w: 16,
    h: 16
  }) : _react.default.createElement(IconButton, {
    icon: "check",
    onClick: changePowerLevel
  });
  const PowerSelector = sdk.getComponent('elements.PowerSelector');
  return _react.default.createElement("div", {
    className: "mx_UserInfo_profileField"
  }, _react.default.createElement(PowerSelector, {
    label: null,
    value: selectedPowerLevel,
    maxValue: roomPermissions.modifyLevelMax,
    usersDefault: powerLevelUsersDefault,
    onChange: onPowerChange,
    disabled: isUpdating
  }), buttonOrSpinner);
};

const useDevices = userId => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default); // undefined means yet to be loaded, null means failed to load, otherwise list of devices

  const [devices, setDevices] = (0, _react.useState)(undefined); // Download device lists

  (0, _react.useEffect)(() => {
    setDevices(undefined);
    let cancelled = false;

    async function _downloadDeviceList() {
      try {
        await cli.downloadKeys([userId], true);
        const devices = await cli.getStoredDevicesForUser(userId);

        if (cancelled) {
          // we got cancelled - presumably a different user now
          return;
        }

        _disambiguateDevices(devices);

        setDevices(devices);
      } catch (err) {
        setDevices(null);
      }
    }

    _downloadDeviceList(); // Handle being unmounted


    return () => {
      cancelled = true;
    };
  }, [cli, userId]); // Listen to changes

  (0, _react.useEffect)(() => {
    let cancel = false;

    const updateDevices = async () => {
      const newDevices = await cli.getStoredDevicesForUser(userId);
      if (cancel) return;
      setDevices(newDevices);
    };

    const onDevicesUpdated = users => {
      if (!users.includes(userId)) return;
      updateDevices();
    };

    const onDeviceVerificationChanged = (_userId, device) => {
      if (_userId !== userId) return;
      updateDevices();
    };

    const onUserTrustStatusChanged = (_userId, trustStatus) => {
      if (_userId !== userId) return;
      updateDevices();
    };

    cli.on("crypto.devicesUpdated", onDevicesUpdated);
    cli.on("deviceVerificationChanged", onDeviceVerificationChanged);
    cli.on("userTrustStatusChanged", onUserTrustStatusChanged); // Handle being unmounted

    return () => {
      cancel = true;
      cli.removeListener("crypto.devicesUpdated", onDevicesUpdated);
      cli.removeListener("deviceVerificationChanged", onDeviceVerificationChanged);
      cli.removeListener("userTrustStatusChanged", onUserTrustStatusChanged);
    };
  }, [cli, userId]);
  return devices;
};

exports.useDevices = useDevices;

const BasicUserInfo = ({
  room,
  member,
  groupId,
  devices,
  isRoomEncrypted
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  const powerLevels = useRoomPowerLevels(cli, room); // Load whether or not we are a Synapse Admin

  const isSynapseAdmin = useIsSynapseAdmin(cli); // Check whether the user is ignored

  const [isIgnored, setIsIgnored] = (0, _react.useState)(cli.isUserIgnored(member.userId)); // Recheck if the user or client changes

  (0, _react.useEffect)(() => {
    setIsIgnored(cli.isUserIgnored(member.userId));
  }, [cli, member.userId]); // Recheck also if we receive new accountData m.ignored_user_list

  const accountDataHandler = (0, _react.useCallback)(ev => {
    if (ev.getType() === "m.ignored_user_list") {
      setIsIgnored(cli.isUserIgnored(member.userId));
    }
  }, [cli, member.userId]);
  (0, _useEventEmitter.useEventEmitter)(cli, "accountData", accountDataHandler); // Count of how many operations are currently in progress, if > 0 then show a Spinner

  const [pendingUpdateCount, setPendingUpdateCount] = (0, _react.useState)(0);
  const startUpdating = (0, _react.useCallback)(() => {
    setPendingUpdateCount(pendingUpdateCount + 1);
  }, [pendingUpdateCount]);
  const stopUpdating = (0, _react.useCallback)(() => {
    setPendingUpdateCount(pendingUpdateCount - 1);
  }, [pendingUpdateCount]);
  const roomPermissions = useRoomPermissions(cli, room, member);
  const onSynapseDeactivate = (0, _react.useCallback)(async () => {
    const QuestionDialog = sdk.getComponent('views.dialogs.QuestionDialog');

    const {
      finished
    } = _Modal.default.createTrackedDialog('Synapse User Deactivation', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Deactivate user?"),
      description: _react.default.createElement("div", null, (0, _languageHandler._t)("Deactivating this user will log them out and prevent them from logging back in. Additionally, " + "they will leave all the rooms they are in. This action cannot be reversed. Are you sure you " + "want to deactivate this user?")),
      button: (0, _languageHandler._t)("Deactivate user"),
      danger: true
    });

    const [accepted] = await finished;
    if (!accepted) return;

    try {
      await cli.deactivateSynapseUser(member.userId);
    } catch (err) {
      console.error("Failed to deactivate user");
      console.error(err);
      const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

      _Modal.default.createTrackedDialog('Failed to deactivate Synapse user', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Failed to deactivate user'),
        description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
      });
    }
  }, [cli, member.userId]);
  let synapseDeactivateButton;
  let spinner; // We don't need a perfect check here, just something to pass as "probably not our homeserver". If
  // someone does figure out how to bypass this check the worst that happens is an error.
  // FIXME this should be using cli instead of MatrixClientPeg.matrixClient

  if (isSynapseAdmin && member.userId.endsWith(":".concat(_MatrixClientPeg.MatrixClientPeg.getHomeserverName()))) {
    synapseDeactivateButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: onSynapseDeactivate,
      className: "mx_UserInfo_field mx_UserInfo_destructive"
    }, (0, _languageHandler._t)("Deactivate user"));
  }

  let adminToolsContainer;

  if (room && member.roomId) {
    adminToolsContainer = _react.default.createElement(RoomAdminToolsContainer, {
      powerLevels: powerLevels,
      member: member,
      room: room,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    }, synapseDeactivateButton);
  } else if (groupId) {
    adminToolsContainer = _react.default.createElement(GroupAdminToolsSection, {
      groupId: groupId,
      groupMember: member,
      startUpdating: startUpdating,
      stopUpdating: stopUpdating
    }, synapseDeactivateButton);
  } else if (synapseDeactivateButton) {
    adminToolsContainer = _react.default.createElement(GenericAdminToolsContainer, null, synapseDeactivateButton);
  }

  if (pendingUpdateCount > 0) {
    const Loader = sdk.getComponent("elements.Spinner");
    spinner = _react.default.createElement(Loader, {
      imgClassName: "mx_ContextualMenu_spinner"
    });
  }

  const memberDetails = _react.default.createElement(PowerLevelSection, {
    powerLevels: powerLevels,
    user: member,
    room: room,
    roomPermissions: roomPermissions
  }); // only display the devices list if our client supports E2E


  const _enableDevices = cli.isCryptoEnabled();

  let text;

  if (!isRoomEncrypted) {
    if (!_enableDevices) {
      text = (0, _languageHandler._t)("This client does not support end-to-end encryption.");
    } else if (room) {
      text = (0, _languageHandler._t)("Messages in this room are not end-to-end encrypted.");
    } else {// TODO what to render for GroupMember
    }
  } else {
    text = (0, _languageHandler._t)("Messages in this room are end-to-end encrypted.");
  }

  let verifyButton;
  const homeserverSupportsCrossSigning = useHomeserverSupportsCrossSigning(cli);
  const userTrust = cli.checkUserTrust(member.userId);
  const userVerified = userTrust.isCrossSigningVerified();
  const isMe = member.userId === cli.getUserId();
  const canVerify = _SettingsStore.default.isFeatureEnabled("feature_cross_signing") && homeserverSupportsCrossSigning && !userVerified && !isMe;

  const setUpdating = updating => {
    setPendingUpdateCount(count => count + (updating ? 1 : -1));
  };

  const hasCrossSigningKeys = useHasCrossSigningKeys(cli, member, canVerify, setUpdating);

  if (canVerify) {
    verifyButton = _react.default.createElement(_AccessibleButton.default, {
      className: "mx_UserInfo_field",
      onClick: () => {
        if (hasCrossSigningKeys) {
          (0, _verification.verifyUser)(member);
        } else {
          (0, _verification.legacyVerifyUser)(member);
        }
      }
    }, (0, _languageHandler._t)("Verify"));
  }

  const securitySection = _react.default.createElement("div", {
    className: "mx_UserInfo_container"
  }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Security")), _react.default.createElement("p", null, text), verifyButton, _react.default.createElement(DevicesSection, {
    loading: devices === undefined,
    devices: devices,
    userId: member.userId
  }));

  return _react.default.createElement(_react.default.Fragment, null, memberDetails && _react.default.createElement("div", {
    className: "mx_UserInfo_container mx_UserInfo_separator mx_UserInfo_memberDetailsContainer"
  }, _react.default.createElement("div", {
    className: "mx_UserInfo_memberDetails"
  }, memberDetails)), securitySection, _react.default.createElement(UserOptionsSection, {
    devices: devices,
    canInvite: roomPermissions.canInvite,
    isIgnored: isIgnored,
    member: member
  }), adminToolsContainer, spinner);
};

const UserInfoHeader = ({
  onClose,
  member,
  e2eStatus
}) => {
  const cli = (0, _react.useContext)(_MatrixClientContext.default);
  let closeButton;

  if (onClose) {
    closeButton = _react.default.createElement(_AccessibleButton.default, {
      className: "mx_UserInfo_cancel",
      onClick: onClose,
      title: (0, _languageHandler._t)('Close')
    }, _react.default.createElement("div", null));
  }

  const onMemberAvatarClick = (0, _react.useCallback)(() => {
    const avatarUrl = member.getMxcAvatarUrl ? member.getMxcAvatarUrl() : member.avatarUrl;
    if (!avatarUrl) return;
    const httpUrl = cli.mxcUrlToHttp(avatarUrl);
    const ImageView = sdk.getComponent("elements.ImageView");
    const params = {
      src: httpUrl,
      name: member.name
    };

    _Modal.default.createDialog(ImageView, params, "mx_Dialog_lightbox");
  }, [cli, member]);
  const MemberAvatar = sdk.getComponent('avatars.MemberAvatar');

  const avatarElement = _react.default.createElement("div", {
    className: "mx_UserInfo_avatar"
  }, _react.default.createElement("div", null, _react.default.createElement("div", null, _react.default.createElement(MemberAvatar, {
    key: member.userId // to instantly blank the avatar when UserInfo changes members
    ,
    member: member,
    width: 2 * 0.3 * window.innerHeight // 2x@30vh
    ,
    height: 2 * 0.3 * window.innerHeight // 2x@30vh
    ,
    resizeMethod: "scale",
    fallbackUserId: member.userId,
    onClick: onMemberAvatarClick,
    urls: member.avatarUrl ? [member.avatarUrl] : undefined
  }))));

  let presenceState;
  let presenceLastActiveAgo;
  let presenceCurrentlyActive;
  let statusMessage;

  if (member instanceof _matrixJsSdk.RoomMember && member.user) {
    presenceState = member.user.presence;
    presenceLastActiveAgo = member.user.lastActiveAgo;
    presenceCurrentlyActive = member.user.currentlyActive;

    if (_SettingsStore.default.isFeatureEnabled("feature_custom_status")) {
      statusMessage = member.user._unstable_statusMessage;
    }
  }

  const enablePresenceByHsUrl = _SdkConfig.default.get()["enable_presence_by_hs_url"];

  let showPresence = true;

  if (enablePresenceByHsUrl && enablePresenceByHsUrl[cli.baseUrl] !== undefined) {
    showPresence = enablePresenceByHsUrl[cli.baseUrl];
  }

  let presenceLabel = null;

  if (showPresence) {
    const PresenceLabel = sdk.getComponent('rooms.PresenceLabel');
    presenceLabel = _react.default.createElement(PresenceLabel, {
      activeAgo: presenceLastActiveAgo,
      currentlyActive: presenceCurrentlyActive,
      presenceState: presenceState
    });
  }

  let statusLabel = null;

  if (statusMessage) {
    statusLabel = _react.default.createElement("span", {
      className: "mx_UserInfo_statusMessage"
    }, statusMessage);
  }

  let e2eIcon;

  if (e2eStatus) {
    e2eIcon = _react.default.createElement(_E2EIcon.default, {
      size: 18,
      status: e2eStatus,
      isUser: true
    });
  }

  const displayName = member.name || member.displayname;
  return _react.default.createElement(_react.default.Fragment, null, closeButton, avatarElement, _react.default.createElement("div", {
    className: "mx_UserInfo_container mx_UserInfo_separator"
  }, _react.default.createElement("div", {
    className: "mx_UserInfo_profile"
  }, _react.default.createElement("div", null, _react.default.createElement("h2", null, e2eIcon, _react.default.createElement("span", {
    title: displayName,
    "aria-label": displayName
  }, displayName))), _react.default.createElement("div", null, member.userId), _react.default.createElement("div", {
    className: "mx_UserInfo_profileStatus"
  }, presenceLabel, statusLabel))));
};

const UserInfo = (_ref) => {
  let {
    user,
    groupId,
    roomId,
    onClose,
    phase = _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo
  } = _ref,
      props = (0, _objectWithoutProperties2.default)(_ref, ["user", "groupId", "roomId", "onClose", "phase"]);
  const cli = (0, _react.useContext)(_MatrixClientContext.default); // Load room if we are given a room id and memoize it

  const room = (0, _react.useMemo)(() => roomId ? cli.getRoom(roomId) : null, [cli, roomId]); // fetch latest room member if we have a room, so we don't show historical information, falling back to user

  const member = (0, _react.useMemo)(() => room ? room.getMember(user.userId) || user : user, [room, user]);
  const isRoomEncrypted = useIsEncrypted(cli, room);
  const devices = useDevices(user.userId);
  let e2eStatus;

  if (isRoomEncrypted && devices) {
    e2eStatus = getE2EStatus(cli, user.userId, devices);
  }

  const classes = ["mx_UserInfo"];
  let content;

  switch (phase) {
    case _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo:
    case _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberInfo:
      content = _react.default.createElement(BasicUserInfo, {
        room: room,
        member: member,
        groupId: groupId,
        devices: devices,
        isRoomEncrypted: isRoomEncrypted
      });
      break;

    case _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel:
      classes.push("mx_UserInfo_smallAvatar");
      content = _react.default.createElement(_EncryptionPanel.default, (0, _extends2.default)({}, props, {
        member: member,
        onClose: onClose,
        isRoomEncrypted: isRoomEncrypted
      }));
      break;
  }

  return _react.default.createElement("div", {
    className: classes.join(" "),
    role: "tabpanel"
  }, _react.default.createElement(_AutoHideScrollbar.default, {
    className: "mx_UserInfo_scrollContainer"
  }, _react.default.createElement(UserInfoHeader, {
    member: member,
    e2eStatus: e2eStatus,
    onClose: onClose
  }), content));
};

UserInfo.propTypes = {
  user: _propTypes.default.oneOfType([_propTypes.default.instanceOf(_matrixJsSdk.User), _propTypes.default.instanceOf(_matrixJsSdk.RoomMember), GroupMember]).isRequired,
  group: _propTypes.default.instanceOf(_matrixJsSdk.Group),
  groupId: _propTypes.default.string,
  roomId: _propTypes.default.string,
  onClose: _propTypes.default.func
};
var _default = UserInfo;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3JpZ2h0X3BhbmVsL1VzZXJJbmZvLmpzIl0sIm5hbWVzIjpbIl9kaXNhbWJpZ3VhdGVEZXZpY2VzIiwiZGV2aWNlcyIsIm5hbWVzIiwiT2JqZWN0IiwiY3JlYXRlIiwiaSIsImxlbmd0aCIsIm5hbWUiLCJnZXREaXNwbGF5TmFtZSIsImluZGV4TGlzdCIsInB1c2giLCJmb3JFYWNoIiwiaiIsImFtYmlndW91cyIsImdldEUyRVN0YXR1cyIsImNsaSIsInVzZXJJZCIsIlNldHRpbmdzU3RvcmUiLCJpc0ZlYXR1cmVFbmFibGVkIiwiaGFzVW52ZXJpZmllZERldmljZSIsInNvbWUiLCJkZXZpY2UiLCJpc1VudmVyaWZpZWQiLCJpc01lIiwiZ2V0VXNlcklkIiwidXNlclRydXN0IiwiY2hlY2tVc2VyVHJ1c3QiLCJpc0Nyb3NzU2lnbmluZ1ZlcmlmaWVkIiwid2FzQ3Jvc3NTaWduaW5nVmVyaWZpZWQiLCJhbnlEZXZpY2VVbnZlcmlmaWVkIiwiZGV2aWNlSWQiLCJkZXZpY2VUcnVzdCIsImNoZWNrRGV2aWNlVHJ1c3QiLCJpc1ZlcmlmaWVkIiwib3BlbkRNRm9yVXNlciIsIm1hdHJpeENsaWVudCIsImRtUm9vbXMiLCJETVJvb21NYXAiLCJzaGFyZWQiLCJnZXRETVJvb21zRm9yVXNlcklkIiwibGFzdEFjdGl2ZVJvb20iLCJyZWR1Y2UiLCJyb29tSWQiLCJyb29tIiwiZ2V0Um9vbSIsImdldE15TWVtYmVyc2hpcCIsImdldExhc3RBY3RpdmVUaW1lc3RhbXAiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJvb21faWQiLCJjcmVhdGVSb29tT3B0aW9ucyIsImRtVXNlcklkIiwidXNlcnNUb0RldmljZXNNYXAiLCJkb3dubG9hZEtleXMiLCJhbGxIYXZlRGV2aWNlS2V5cyIsInZhbHVlcyIsImV2ZXJ5Iiwia2V5cyIsImVuY3J5cHRpb24iLCJ1c2VJc0VuY3J5cHRlZCIsImlzRW5jcnlwdGVkIiwic2V0SXNFbmNyeXB0ZWQiLCJpc1Jvb21FbmNyeXB0ZWQiLCJ1bmRlZmluZWQiLCJ1cGRhdGUiLCJldmVudCIsImdldFR5cGUiLCJjdXJyZW50U3RhdGUiLCJ1c2VIYXNDcm9zc1NpZ25pbmdLZXlzIiwibWVtYmVyIiwiY2FuVmVyaWZ5Iiwic2V0VXBkYXRpbmciLCJ4c2kiLCJnZXRTdG9yZWRDcm9zc1NpZ25pbmdGb3JVc2VyIiwia2V5IiwiZ2V0SWQiLCJEZXZpY2VJdGVtIiwiTWF0cml4Q2xpZW50Q29udGV4dCIsImNsYXNzZXMiLCJteF9Vc2VySW5mb19kZXZpY2VfdmVyaWZpZWQiLCJteF9Vc2VySW5mb19kZXZpY2VfdW52ZXJpZmllZCIsImljb25DbGFzc2VzIiwibXhfRTJFSWNvbl9ub3JtYWwiLCJteF9FMkVJY29uX3ZlcmlmaWVkIiwibXhfRTJFSWNvbl93YXJuaW5nIiwib25EZXZpY2VDbGljayIsImdldFVzZXIiLCJkZXZpY2VOYW1lIiwidHJ1c3RlZExhYmVsIiwiRGV2aWNlc1NlY3Rpb24iLCJsb2FkaW5nIiwiU3Bpbm5lciIsInNkayIsImdldENvbXBvbmVudCIsImlzRXhwYW5kZWQiLCJzZXRFeHBhbmRlZCIsImRldmljZVRydXN0cyIsIm1hcCIsImQiLCJleHBhbmRTZWN0aW9uRGV2aWNlcyIsInVudmVyaWZpZWREZXZpY2VzIiwiZXhwYW5kQ291bnRDYXB0aW9uIiwiZXhwYW5kSGlkZUNhcHRpb24iLCJleHBhbmRJY29uQ2xhc3NlcyIsImNvdW50IiwiZXhwYW5kQnV0dG9uIiwiZGV2aWNlTGlzdCIsImtleVN0YXJ0IiwiY29uY2F0IiwiVXNlck9wdGlvbnNTZWN0aW9uIiwiaXNJZ25vcmVkIiwiY2FuSW52aXRlIiwiaWdub3JlQnV0dG9uIiwiaW5zZXJ0UGlsbEJ1dHRvbiIsImludml0ZVVzZXJCdXR0b24iLCJyZWFkUmVjZWlwdEJ1dHRvbiIsIm9uU2hhcmVVc2VyQ2xpY2siLCJTaGFyZURpYWxvZyIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRhcmdldCIsIm9uSWdub3JlVG9nZ2xlIiwiaWdub3JlZFVzZXJzIiwiZ2V0SWdub3JlZFVzZXJzIiwiaW5kZXgiLCJpbmRleE9mIiwic3BsaWNlIiwic2V0SWdub3JlZFVzZXJzIiwibXhfVXNlckluZm9fZGVzdHJ1Y3RpdmUiLCJvblJlYWRSZWNlaXB0QnV0dG9uIiwiaGlnaGxpZ2h0ZWQiLCJldmVudF9pZCIsImdldEV2ZW50UmVhZFVwVG8iLCJvbkluc2VydFBpbGxCdXR0b24iLCJ1c2VyX2lkIiwibWVtYmVyc2hpcCIsIlJvb21WaWV3U3RvcmUiLCJnZXRSb29tSWQiLCJvbkludml0ZVVzZXJCdXR0b24iLCJpbnZpdGVyIiwiTXVsdGlJbnZpdGVyIiwiaW52aXRlIiwidGhlbiIsImdldENvbXBsZXRpb25TdGF0ZSIsIkVycm9yIiwiZ2V0RXJyb3JUZXh0IiwiZXJyIiwiRXJyb3JEaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwibWVzc2FnZSIsInNoYXJlVXNlckJ1dHRvbiIsImRpcmVjdE1lc3NhZ2VCdXR0b24iLCJfd2FyblNlbGZEZW1vdGUiLCJRdWVzdGlvbkRpYWxvZyIsImZpbmlzaGVkIiwiYnV0dG9uIiwiY29uZmlybWVkIiwiR2VuZXJpY0FkbWluVG9vbHNDb250YWluZXIiLCJjaGlsZHJlbiIsIl9pc011dGVkIiwicG93ZXJMZXZlbENvbnRlbnQiLCJsZXZlbFRvU2VuZCIsImV2ZW50cyIsImV2ZW50c19kZWZhdWx0IiwicG93ZXJMZXZlbCIsInVzZVJvb21Qb3dlckxldmVscyIsInBvd2VyTGV2ZWxzIiwic2V0UG93ZXJMZXZlbHMiLCJnZXRTdGF0ZUV2ZW50cyIsImdldENvbnRlbnQiLCJSb29tS2lja0J1dHRvbiIsInN0YXJ0VXBkYXRpbmciLCJzdG9wVXBkYXRpbmciLCJvbktpY2siLCJDb25maXJtVXNlckFjdGlvbkRpYWxvZyIsImFza1JlYXNvbiIsImRhbmdlciIsInByb2NlZWQiLCJyZWFzb24iLCJraWNrIiwiY29uc29sZSIsImxvZyIsImVycm9yIiwiZmluYWxseSIsImtpY2tMYWJlbCIsIlJlZGFjdE1lc3NhZ2VzQnV0dG9uIiwib25SZWRhY3RBbGxNZXNzYWdlcyIsInRpbWVsaW5lIiwiZ2V0TGl2ZVRpbWVsaW5lIiwiZXZlbnRzVG9SZWRhY3QiLCJnZXRFdmVudHMiLCJnZXRTZW5kZXIiLCJpc1JlZGFjdGVkIiwiZ2V0TmVpZ2hib3VyaW5nVGltZWxpbmUiLCJFdmVudFRpbWVsaW5lIiwiQkFDS1dBUkRTIiwidXNlciIsIkluZm9EaWFsb2ciLCJQcm9taXNlIiwicmVzb2x2ZSIsImluZm8iLCJhbGwiLCJyZWRhY3RFdmVudCIsIkJhblRvZ2dsZUJ1dHRvbiIsIm9uQmFuT3JVbmJhbiIsInByb21pc2UiLCJ1bmJhbiIsImJhbiIsImxhYmVsIiwiTXV0ZVRvZ2dsZUJ1dHRvbiIsImlzTXV0ZWQiLCJvbk11dGVUb2dnbGUiLCJlIiwicG93ZXJMZXZlbEV2ZW50IiwibGV2ZWwiLCJwYXJzZUludCIsImlzTmFOIiwic2V0UG93ZXJMZXZlbCIsIm11dGVMYWJlbCIsIlJvb21BZG1pblRvb2xzQ29udGFpbmVyIiwia2lja0J1dHRvbiIsImJhbkJ1dHRvbiIsIm11dGVCdXR0b24iLCJyZWRhY3RCdXR0b24iLCJlZGl0UG93ZXJMZXZlbCIsInN0YXRlX2RlZmF1bHQiLCJtZSIsImdldE1lbWJlciIsImNhbkFmZmVjdFVzZXIiLCJyZWRhY3QiLCJHcm91cEFkbWluVG9vbHNTZWN0aW9uIiwiZ3JvdXBJZCIsImdyb3VwTWVtYmVyIiwiaXNQcml2aWxlZ2VkIiwic2V0SXNQcml2aWxlZ2VkIiwiaXNJbnZpdGVkIiwic2V0SXNJbnZpdGVkIiwidW5tb3VudGVkIiwib25Hcm91cFN0b3JlVXBkYXRlZCIsIkdyb3VwU3RvcmUiLCJpc1VzZXJQcml2aWxlZ2VkIiwiZ2V0R3JvdXBJbnZpdGVkTWVtYmVycyIsIm0iLCJyZWdpc3Rlckxpc3RlbmVyIiwidW5yZWdpc3Rlckxpc3RlbmVyIiwiX29uS2ljayIsImNyZWF0ZURpYWxvZyIsInJlbW92ZVVzZXJGcm9tR3JvdXAiLCJjYXRjaCIsIkdyb3VwTWVtYmVyIiwiUHJvcFR5cGVzIiwic2hhcGUiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiZGlzcGxheW5hbWUiLCJhdmF0YXJVcmwiLCJ1c2VJc1N5bmFwc2VBZG1pbiIsImlzQWRtaW4iLCJzZXRJc0FkbWluIiwiaXNTeW5hcHNlQWRtaW5pc3RyYXRvciIsInVzZUhvbWVzZXJ2ZXJTdXBwb3J0c0Nyb3NzU2lnbmluZyIsImRvZXNTZXJ2ZXJTdXBwb3J0VW5zdGFibGVGZWF0dXJlIiwidXNlUm9vbVBlcm1pc3Npb25zIiwicm9vbVBlcm1pc3Npb25zIiwic2V0Um9vbVBlcm1pc3Npb25zIiwibW9kaWZ5TGV2ZWxNYXgiLCJjYW5FZGl0IiwidXBkYXRlUm9vbVBlcm1pc3Npb25zIiwidGhlbSIsIm1heGltYWxQb3dlckxldmVsIiwiUG93ZXJMZXZlbFNlY3Rpb24iLCJpc0VkaXRpbmciLCJzZXRFZGl0aW5nIiwiSWNvbkJ1dHRvbiIsInBvd2VyTGV2ZWxVc2Vyc0RlZmF1bHQiLCJ1c2Vyc19kZWZhdWx0IiwibW9kaWZ5QnV0dG9uIiwicm9sZSIsInJvb21OYW1lIiwic3Ryb25nIiwiUG93ZXJMZXZlbEVkaXRvciIsIm9uRmluaXNoZWQiLCJpc1VwZGF0aW5nIiwic2V0SXNVcGRhdGluZyIsInNlbGVjdGVkUG93ZXJMZXZlbCIsInNldFNlbGVjdGVkUG93ZXJMZXZlbCIsImlzRGlydHkiLCJzZXRJc0RpcnR5Iiwib25Qb3dlckNoYW5nZSIsImNoYW5nZVBvd2VyTGV2ZWwiLCJfYXBwbHlQb3dlckNoYW5nZSIsInVzZXJzIiwibXlVc2VySWQiLCJteVBvd2VyIiwiYnV0dG9uT3JTcGlubmVyIiwiUG93ZXJTZWxlY3RvciIsInVzZURldmljZXMiLCJzZXREZXZpY2VzIiwiY2FuY2VsbGVkIiwiX2Rvd25sb2FkRGV2aWNlTGlzdCIsImdldFN0b3JlZERldmljZXNGb3JVc2VyIiwiY2FuY2VsIiwidXBkYXRlRGV2aWNlcyIsIm5ld0RldmljZXMiLCJvbkRldmljZXNVcGRhdGVkIiwiaW5jbHVkZXMiLCJvbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQiLCJfdXNlcklkIiwib25Vc2VyVHJ1c3RTdGF0dXNDaGFuZ2VkIiwidHJ1c3RTdGF0dXMiLCJvbiIsInJlbW92ZUxpc3RlbmVyIiwiQmFzaWNVc2VySW5mbyIsImlzU3luYXBzZUFkbWluIiwic2V0SXNJZ25vcmVkIiwiaXNVc2VySWdub3JlZCIsImFjY291bnREYXRhSGFuZGxlciIsImV2IiwicGVuZGluZ1VwZGF0ZUNvdW50Iiwic2V0UGVuZGluZ1VwZGF0ZUNvdW50Iiwib25TeW5hcHNlRGVhY3RpdmF0ZSIsImFjY2VwdGVkIiwiZGVhY3RpdmF0ZVN5bmFwc2VVc2VyIiwic3luYXBzZURlYWN0aXZhdGVCdXR0b24iLCJzcGlubmVyIiwiZW5kc1dpdGgiLCJNYXRyaXhDbGllbnRQZWciLCJnZXRIb21lc2VydmVyTmFtZSIsImFkbWluVG9vbHNDb250YWluZXIiLCJMb2FkZXIiLCJtZW1iZXJEZXRhaWxzIiwiX2VuYWJsZURldmljZXMiLCJpc0NyeXB0b0VuYWJsZWQiLCJ0ZXh0IiwidmVyaWZ5QnV0dG9uIiwiaG9tZXNlcnZlclN1cHBvcnRzQ3Jvc3NTaWduaW5nIiwidXNlclZlcmlmaWVkIiwidXBkYXRpbmciLCJoYXNDcm9zc1NpZ25pbmdLZXlzIiwic2VjdXJpdHlTZWN0aW9uIiwiVXNlckluZm9IZWFkZXIiLCJvbkNsb3NlIiwiZTJlU3RhdHVzIiwiY2xvc2VCdXR0b24iLCJvbk1lbWJlckF2YXRhckNsaWNrIiwiZ2V0TXhjQXZhdGFyVXJsIiwiaHR0cFVybCIsIm14Y1VybFRvSHR0cCIsIkltYWdlVmlldyIsInBhcmFtcyIsInNyYyIsIk1lbWJlckF2YXRhciIsImF2YXRhckVsZW1lbnQiLCJ3aW5kb3ciLCJpbm5lckhlaWdodCIsInByZXNlbmNlU3RhdGUiLCJwcmVzZW5jZUxhc3RBY3RpdmVBZ28iLCJwcmVzZW5jZUN1cnJlbnRseUFjdGl2ZSIsInN0YXR1c01lc3NhZ2UiLCJSb29tTWVtYmVyIiwicHJlc2VuY2UiLCJsYXN0QWN0aXZlQWdvIiwiY3VycmVudGx5QWN0aXZlIiwiX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2UiLCJlbmFibGVQcmVzZW5jZUJ5SHNVcmwiLCJTZGtDb25maWciLCJnZXQiLCJzaG93UHJlc2VuY2UiLCJiYXNlVXJsIiwicHJlc2VuY2VMYWJlbCIsIlByZXNlbmNlTGFiZWwiLCJzdGF0dXNMYWJlbCIsImUyZUljb24iLCJkaXNwbGF5TmFtZSIsIlVzZXJJbmZvIiwicGhhc2UiLCJSSUdIVF9QQU5FTF9QSEFTRVMiLCJSb29tTWVtYmVySW5mbyIsInByb3BzIiwiY29udGVudCIsIkdyb3VwTWVtYmVySW5mbyIsIkVuY3J5cHRpb25QYW5lbCIsImpvaW4iLCJwcm9wVHlwZXMiLCJvbmVPZlR5cGUiLCJpbnN0YW5jZU9mIiwiVXNlciIsImdyb3VwIiwiR3JvdXAiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBN0NBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQ0EsTUFBTUEsb0JBQW9CLEdBQUlDLE9BQUQsSUFBYTtBQUN0QyxRQUFNQyxLQUFLLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLElBQWQsQ0FBZDs7QUFDQSxPQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdKLE9BQU8sQ0FBQ0ssTUFBNUIsRUFBb0NELENBQUMsRUFBckMsRUFBeUM7QUFDckMsVUFBTUUsSUFBSSxHQUFHTixPQUFPLENBQUNJLENBQUQsQ0FBUCxDQUFXRyxjQUFYLEVBQWI7QUFDQSxVQUFNQyxTQUFTLEdBQUdQLEtBQUssQ0FBQ0ssSUFBRCxDQUFMLElBQWUsRUFBakM7QUFDQUUsSUFBQUEsU0FBUyxDQUFDQyxJQUFWLENBQWVMLENBQWY7QUFDQUgsSUFBQUEsS0FBSyxDQUFDSyxJQUFELENBQUwsR0FBY0UsU0FBZDtBQUNIOztBQUNELE9BQUssTUFBTUYsSUFBWCxJQUFtQkwsS0FBbkIsRUFBMEI7QUFDdEIsUUFBSUEsS0FBSyxDQUFDSyxJQUFELENBQUwsQ0FBWUQsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QkosTUFBQUEsS0FBSyxDQUFDSyxJQUFELENBQUwsQ0FBWUksT0FBWixDQUFxQkMsQ0FBRCxJQUFLO0FBQ3JCWCxRQUFBQSxPQUFPLENBQUNXLENBQUQsQ0FBUCxDQUFXQyxTQUFYLEdBQXVCLElBQXZCO0FBQ0gsT0FGRDtBQUdIO0FBQ0o7QUFDSixDQWZEOztBQWlCTyxNQUFNQyxZQUFZLEdBQUcsQ0FBQ0MsR0FBRCxFQUFNQyxNQUFOLEVBQWNmLE9BQWQsS0FBMEI7QUFDbEQsTUFBSSxDQUFDZ0IsdUJBQWNDLGdCQUFkLENBQStCLHVCQUEvQixDQUFMLEVBQThEO0FBQzFELFVBQU1DLG1CQUFtQixHQUFHbEIsT0FBTyxDQUFDbUIsSUFBUixDQUFjQyxNQUFELElBQVlBLE1BQU0sQ0FBQ0MsWUFBUCxFQUF6QixDQUE1QjtBQUNBLFdBQU9ILG1CQUFtQixHQUFHLFNBQUgsR0FBZSxVQUF6QztBQUNIOztBQUNELFFBQU1JLElBQUksR0FBR1AsTUFBTSxLQUFLRCxHQUFHLENBQUNTLFNBQUosRUFBeEI7QUFDQSxRQUFNQyxTQUFTLEdBQUdWLEdBQUcsQ0FBQ1csY0FBSixDQUFtQlYsTUFBbkIsQ0FBbEI7O0FBQ0EsTUFBSSxDQUFDUyxTQUFTLENBQUNFLHNCQUFWLEVBQUwsRUFBeUM7QUFDckMsV0FBT0YsU0FBUyxDQUFDRyx1QkFBVixLQUFzQyxTQUF0QyxHQUFrRCxRQUF6RDtBQUNIOztBQUVELFFBQU1DLG1CQUFtQixHQUFHNUIsT0FBTyxDQUFDbUIsSUFBUixDQUFhQyxNQUFNLElBQUk7QUFDL0MsVUFBTTtBQUFFUyxNQUFBQTtBQUFGLFFBQWVULE1BQXJCLENBRCtDLENBRS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsVUFBTVUsV0FBVyxHQUFHaEIsR0FBRyxDQUFDaUIsZ0JBQUosQ0FBcUJoQixNQUFyQixFQUE2QmMsUUFBN0IsQ0FBcEI7QUFDQSxXQUFPUCxJQUFJLEdBQUcsQ0FBQ1EsV0FBVyxDQUFDSixzQkFBWixFQUFKLEdBQTJDLENBQUNJLFdBQVcsQ0FBQ0UsVUFBWixFQUF2RDtBQUNILEdBVDJCLENBQTVCO0FBVUEsU0FBT0osbUJBQW1CLEdBQUcsU0FBSCxHQUFlLFVBQXpDO0FBQ0gsQ0F0Qk07Ozs7QUF3QlAsZUFBZUssYUFBZixDQUE2QkMsWUFBN0IsRUFBMkNuQixNQUEzQyxFQUFtRDtBQUMvQyxRQUFNb0IsT0FBTyxHQUFHQyxtQkFBVUMsTUFBVixHQUFtQkMsbUJBQW5CLENBQXVDdkIsTUFBdkMsQ0FBaEI7O0FBQ0EsUUFBTXdCLGNBQWMsR0FBR0osT0FBTyxDQUFDSyxNQUFSLENBQWUsQ0FBQ0QsY0FBRCxFQUFpQkUsTUFBakIsS0FBNEI7QUFDOUQsVUFBTUMsSUFBSSxHQUFHUixZQUFZLENBQUNTLE9BQWIsQ0FBcUJGLE1BQXJCLENBQWI7O0FBQ0EsUUFBSSxDQUFDQyxJQUFELElBQVNBLElBQUksQ0FBQ0UsZUFBTCxPQUEyQixPQUF4QyxFQUFpRDtBQUM3QyxhQUFPTCxjQUFQO0FBQ0g7O0FBQ0QsUUFBSSxDQUFDQSxjQUFELElBQW1CQSxjQUFjLENBQUNNLHNCQUFmLEtBQTBDSCxJQUFJLENBQUNHLHNCQUFMLEVBQWpFLEVBQWdHO0FBQzVGLGFBQU9ILElBQVA7QUFDSDs7QUFDRCxXQUFPSCxjQUFQO0FBQ0gsR0FUc0IsRUFTcEIsSUFUb0IsQ0FBdkI7O0FBV0EsTUFBSUEsY0FBSixFQUFvQjtBQUNoQk8sd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxNQUFBQSxPQUFPLEVBQUVWLGNBQWMsQ0FBQ0U7QUFGZixLQUFiOztBQUlBO0FBQ0g7O0FBRUQsUUFBTVMsaUJBQWlCLEdBQUc7QUFDdEJDLElBQUFBLFFBQVEsRUFBRXBDO0FBRFksR0FBMUI7O0FBSUEsTUFBSUMsdUJBQWNDLGdCQUFkLENBQStCLHVCQUEvQixDQUFKLEVBQTZEO0FBQ3pEO0FBQ0E7QUFDQSxVQUFNbUMsaUJBQWlCLEdBQUcsTUFBTWxCLFlBQVksQ0FBQ21CLFlBQWIsQ0FBMEIsQ0FBQ3RDLE1BQUQsQ0FBMUIsQ0FBaEM7QUFDQSxVQUFNdUMsaUJBQWlCLEdBQUdwRCxNQUFNLENBQUNxRCxNQUFQLENBQWNILGlCQUFkLEVBQWlDSSxLQUFqQyxDQUF1Q3hELE9BQU8sSUFBSTtBQUN4RTtBQUNBLGFBQU9FLE1BQU0sQ0FBQ3VELElBQVAsQ0FBWXpELE9BQVosRUFBcUJLLE1BQXJCLEdBQThCLENBQXJDO0FBQ0gsS0FIeUIsQ0FBMUI7O0FBSUEsUUFBSWlELGlCQUFKLEVBQXVCO0FBQ25CSixNQUFBQSxpQkFBaUIsQ0FBQ1EsVUFBbEIsR0FBK0IsSUFBL0I7QUFDSDtBQUNKOztBQUVELDJCQUFXUixpQkFBWDtBQUNIOztBQUVELFNBQVNTLGNBQVQsQ0FBd0I3QyxHQUF4QixFQUE2QjRCLElBQTdCLEVBQW1DO0FBQy9CLFFBQU0sQ0FBQ2tCLFdBQUQsRUFBY0MsY0FBZCxJQUFnQyxxQkFBU25CLElBQUksR0FBRzVCLEdBQUcsQ0FBQ2dELGVBQUosQ0FBb0JwQixJQUFJLENBQUNELE1BQXpCLENBQUgsR0FBc0NzQixTQUFuRCxDQUF0QztBQUVBLFFBQU1DLE1BQU0sR0FBRyx3QkFBYUMsS0FBRCxJQUFXO0FBQ2xDLFFBQUlBLEtBQUssQ0FBQ0MsT0FBTixPQUFvQixtQkFBeEIsRUFBNkM7QUFDekNMLE1BQUFBLGNBQWMsQ0FBQy9DLEdBQUcsQ0FBQ2dELGVBQUosQ0FBb0JwQixJQUFJLENBQUNELE1BQXpCLENBQUQsQ0FBZDtBQUNIO0FBQ0osR0FKYyxFQUlaLENBQUMzQixHQUFELEVBQU00QixJQUFOLENBSlksQ0FBZjtBQUtBLHdDQUFnQkEsSUFBSSxHQUFHQSxJQUFJLENBQUN5QixZQUFSLEdBQXVCSixTQUEzQyxFQUFzRCxrQkFBdEQsRUFBMEVDLE1BQTFFO0FBQ0EsU0FBT0osV0FBUDtBQUNIOztBQUVELFNBQVNRLHNCQUFULENBQWdDdEQsR0FBaEMsRUFBcUN1RCxNQUFyQyxFQUE2Q0MsU0FBN0MsRUFBd0RDLFdBQXhELEVBQXFFO0FBQ2pFLFNBQU8sZ0NBQWEsWUFBWTtBQUM1QixRQUFJLENBQUNELFNBQUwsRUFBZ0I7QUFDWixhQUFPLEtBQVA7QUFDSDs7QUFDREMsSUFBQUEsV0FBVyxDQUFDLElBQUQsQ0FBWDs7QUFDQSxRQUFJO0FBQ0EsWUFBTXpELEdBQUcsQ0FBQ3VDLFlBQUosQ0FBaUIsQ0FBQ2dCLE1BQU0sQ0FBQ3RELE1BQVIsQ0FBakIsQ0FBTjtBQUNBLFlBQU15RCxHQUFHLEdBQUcxRCxHQUFHLENBQUMyRCw0QkFBSixDQUFpQ0osTUFBTSxDQUFDdEQsTUFBeEMsQ0FBWjtBQUNBLFlBQU0yRCxHQUFHLEdBQUdGLEdBQUcsSUFBSUEsR0FBRyxDQUFDRyxLQUFKLEVBQW5CO0FBQ0EsYUFBTyxDQUFDLENBQUNELEdBQVQ7QUFDSCxLQUxELFNBS1U7QUFDTkgsTUFBQUEsV0FBVyxDQUFDLEtBQUQsQ0FBWDtBQUNIO0FBQ0osR0FiTSxFQWFKLENBQUN6RCxHQUFELEVBQU11RCxNQUFOLEVBQWNDLFNBQWQsQ0FiSSxFQWFzQixLQWJ0QixDQUFQO0FBY0g7O0FBRUQsU0FBU00sVUFBVCxDQUFvQjtBQUFDN0QsRUFBQUEsTUFBRDtBQUFTSyxFQUFBQTtBQUFULENBQXBCLEVBQXNDO0FBQ2xDLFFBQU1OLEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVo7QUFDQSxRQUFNdkQsSUFBSSxHQUFHUCxNQUFNLEtBQUtELEdBQUcsQ0FBQ1MsU0FBSixFQUF4QjtBQUNBLFFBQU1PLFdBQVcsR0FBR2hCLEdBQUcsQ0FBQ2lCLGdCQUFKLENBQXFCaEIsTUFBckIsRUFBNkJLLE1BQU0sQ0FBQ1MsUUFBcEMsQ0FBcEI7QUFDQSxRQUFNTCxTQUFTLEdBQUdWLEdBQUcsQ0FBQ1csY0FBSixDQUFtQlYsTUFBbkIsQ0FBbEIsQ0FKa0MsQ0FLbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFNaUIsVUFBVSxHQUFJVixJQUFJLElBQUlOLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBVCxHQUNmYSxXQUFXLENBQUNKLHNCQUFaLEVBRGUsR0FFZkksV0FBVyxDQUFDRSxVQUFaLEVBRko7QUFJQSxRQUFNOEMsT0FBTyxHQUFHLHlCQUFXLG9CQUFYLEVBQWlDO0FBQzdDQyxJQUFBQSwyQkFBMkIsRUFBRS9DLFVBRGdCO0FBRTdDZ0QsSUFBQUEsNkJBQTZCLEVBQUUsQ0FBQ2hEO0FBRmEsR0FBakMsQ0FBaEI7QUFJQSxRQUFNaUQsV0FBVyxHQUFHLHlCQUFXLFlBQVgsRUFBeUI7QUFDekNDLElBQUFBLGlCQUFpQixFQUFFLENBQUMxRCxTQUFTLENBQUNRLFVBQVYsRUFEcUI7QUFFekNtRCxJQUFBQSxtQkFBbUIsRUFBRW5ELFVBRm9CO0FBR3pDb0QsSUFBQUEsa0JBQWtCLEVBQUU1RCxTQUFTLENBQUNRLFVBQVYsTUFBMEIsQ0FBQ0E7QUFITixHQUF6QixDQUFwQjs7QUFNQSxRQUFNcUQsYUFBYSxHQUFHLE1BQU07QUFDeEIsUUFBSSxDQUFDckQsVUFBTCxFQUFpQjtBQUNiLHNDQUFhbEIsR0FBRyxDQUFDd0UsT0FBSixDQUFZdkUsTUFBWixDQUFiLEVBQWtDSyxNQUFsQztBQUNIO0FBQ0osR0FKRDs7QUFNQSxRQUFNbUUsVUFBVSxHQUFHbkUsTUFBTSxDQUFDUixTQUFQLEdBQ1gsQ0FBQ1EsTUFBTSxDQUFDYixjQUFQLEtBQTBCYSxNQUFNLENBQUNiLGNBQVAsRUFBMUIsR0FBb0QsRUFBckQsSUFBMkQsSUFBM0QsR0FBa0VhLE1BQU0sQ0FBQ1MsUUFBekUsR0FBb0YsR0FEekUsR0FFWFQsTUFBTSxDQUFDYixjQUFQLEVBRlI7QUFHQSxNQUFJaUYsWUFBWSxHQUFHLElBQW5CO0FBQ0EsTUFBSWhFLFNBQVMsQ0FBQ1EsVUFBVixFQUFKLEVBQTRCd0QsWUFBWSxHQUFHeEQsVUFBVSxHQUFHLHlCQUFHLFNBQUgsQ0FBSCxHQUFtQix5QkFBRyxhQUFILENBQTVDO0FBQzVCLFNBQ0ksNkJBQUMseUJBQUQ7QUFDSSxJQUFBLFNBQVMsRUFBRThDLE9BRGY7QUFFSSxJQUFBLEtBQUssRUFBRTFELE1BQU0sQ0FBQ1MsUUFGbEI7QUFHSSxJQUFBLE9BQU8sRUFBRXdEO0FBSGIsS0FLSTtBQUFLLElBQUEsU0FBUyxFQUFFSjtBQUFoQixJQUxKLEVBTUk7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQTBDTSxVQUExQyxDQU5KLEVBT0k7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQTZDQyxZQUE3QyxDQVBKLENBREo7QUFXSDs7QUFFRCxTQUFTQyxjQUFULENBQXdCO0FBQUN6RixFQUFBQSxPQUFEO0FBQVVlLEVBQUFBLE1BQVY7QUFBa0IyRSxFQUFBQTtBQUFsQixDQUF4QixFQUFvRDtBQUNoRCxRQUFNQyxPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxRQUFNL0UsR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWjtBQUNBLFFBQU1yRCxTQUFTLEdBQUdWLEdBQUcsQ0FBQ1csY0FBSixDQUFtQlYsTUFBbkIsQ0FBbEI7QUFFQSxRQUFNLENBQUMrRSxVQUFELEVBQWFDLFdBQWIsSUFBNEIscUJBQVMsS0FBVCxDQUFsQzs7QUFFQSxNQUFJTCxPQUFKLEVBQWE7QUFDVDtBQUNBLFdBQU8sNkJBQUMsT0FBRCxPQUFQO0FBQ0g7O0FBQ0QsTUFBSTFGLE9BQU8sS0FBSyxJQUFoQixFQUFzQjtBQUNsQixXQUFPLHlCQUFHLDZCQUFILENBQVA7QUFDSDs7QUFDRCxRQUFNc0IsSUFBSSxHQUFHUCxNQUFNLEtBQUtELEdBQUcsQ0FBQ1MsU0FBSixFQUF4QjtBQUNBLFFBQU15RSxZQUFZLEdBQUdoRyxPQUFPLENBQUNpRyxHQUFSLENBQVlDLENBQUMsSUFBSXBGLEdBQUcsQ0FBQ2lCLGdCQUFKLENBQXFCaEIsTUFBckIsRUFBNkJtRixDQUFDLENBQUNyRSxRQUEvQixDQUFqQixDQUFyQjtBQUVBLE1BQUlzRSxvQkFBb0IsR0FBRyxFQUEzQjtBQUNBLFFBQU1DLGlCQUFpQixHQUFHLEVBQTFCO0FBRUEsTUFBSUMsa0JBQUo7QUFDQSxNQUFJQyxpQkFBSjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLFlBQXhCOztBQUVBLE1BQUkvRSxTQUFTLENBQUNRLFVBQVYsRUFBSixFQUE0QjtBQUN4QixTQUFLLElBQUk1QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSixPQUFPLENBQUNLLE1BQTVCLEVBQW9DLEVBQUVELENBQXRDLEVBQXlDO0FBQ3JDLFlBQU1nQixNQUFNLEdBQUdwQixPQUFPLENBQUNJLENBQUQsQ0FBdEI7QUFDQSxZQUFNMEIsV0FBVyxHQUFHa0UsWUFBWSxDQUFDNUYsQ0FBRCxDQUFoQyxDQUZxQyxDQUdyQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFlBQU00QixVQUFVLEdBQUlWLElBQUksSUFBSU4sdUJBQWNDLGdCQUFkLENBQStCLHVCQUEvQixDQUFULEdBQ2ZhLFdBQVcsQ0FBQ0osc0JBQVosRUFEZSxHQUVmSSxXQUFXLENBQUNFLFVBQVosRUFGSjs7QUFJQSxVQUFJQSxVQUFKLEVBQWdCO0FBQ1ptRSxRQUFBQSxvQkFBb0IsQ0FBQzFGLElBQXJCLENBQTBCVyxNQUExQjtBQUNILE9BRkQsTUFFTztBQUNIZ0YsUUFBQUEsaUJBQWlCLENBQUMzRixJQUFsQixDQUF1QlcsTUFBdkI7QUFDSDtBQUNKOztBQUNEaUYsSUFBQUEsa0JBQWtCLEdBQUcseUJBQUcsNkJBQUgsRUFBa0M7QUFBQ0csTUFBQUEsS0FBSyxFQUFFTCxvQkFBb0IsQ0FBQzlGO0FBQTdCLEtBQWxDLENBQXJCO0FBQ0FpRyxJQUFBQSxpQkFBaUIsR0FBRyx5QkFBRyx3QkFBSCxDQUFwQjtBQUNBQyxJQUFBQSxpQkFBaUIsSUFBSSxzQkFBckI7QUFDSCxHQXRCRCxNQXNCTztBQUNISixJQUFBQSxvQkFBb0IsR0FBR25HLE9BQXZCO0FBQ0FxRyxJQUFBQSxrQkFBa0IsR0FBRyx5QkFBRyxvQkFBSCxFQUF5QjtBQUFDRyxNQUFBQSxLQUFLLEVBQUV4RyxPQUFPLENBQUNLO0FBQWhCLEtBQXpCLENBQXJCO0FBQ0FpRyxJQUFBQSxpQkFBaUIsR0FBRyx5QkFBRyxlQUFILENBQXBCO0FBQ0FDLElBQUFBLGlCQUFpQixJQUFJLG9CQUFyQjtBQUNIOztBQUVELE1BQUlFLFlBQUo7O0FBQ0EsTUFBSU4sb0JBQW9CLENBQUM5RixNQUF6QixFQUFpQztBQUM3QixRQUFJeUYsVUFBSixFQUFnQjtBQUNaVyxNQUFBQSxZQUFZLEdBQUksNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMsa0NBQTVCO0FBQ1osUUFBQSxPQUFPLEVBQUUsTUFBTVYsV0FBVyxDQUFDLEtBQUQ7QUFEZCxTQUdaLDBDQUFNTyxpQkFBTixDQUhZLENBQWhCO0FBS0gsS0FORCxNQU1PO0FBQ0hHLE1BQUFBLFlBQVksR0FBSSw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxrQ0FBNUI7QUFDWixRQUFBLE9BQU8sRUFBRSxNQUFNVixXQUFXLENBQUMsSUFBRDtBQURkLFNBR1o7QUFBSyxRQUFBLFNBQVMsRUFBRVE7QUFBaEIsUUFIWSxFQUlaLDBDQUFNRixrQkFBTixDQUpZLENBQWhCO0FBTUg7QUFDSjs7QUFFRCxNQUFJSyxVQUFVLEdBQUdOLGlCQUFpQixDQUFDSCxHQUFsQixDQUFzQixDQUFDN0UsTUFBRCxFQUFTaEIsQ0FBVCxLQUFlO0FBQ2xELFdBQVEsNkJBQUMsVUFBRDtBQUFZLE1BQUEsR0FBRyxFQUFFQSxDQUFqQjtBQUFvQixNQUFBLE1BQU0sRUFBRVcsTUFBNUI7QUFBb0MsTUFBQSxNQUFNLEVBQUVLO0FBQTVDLE1BQVI7QUFDSCxHQUZnQixDQUFqQjs7QUFHQSxNQUFJMEUsVUFBSixFQUFnQjtBQUNaLFVBQU1hLFFBQVEsR0FBR1AsaUJBQWlCLENBQUMvRixNQUFuQztBQUNBcUcsSUFBQUEsVUFBVSxHQUFHQSxVQUFVLENBQUNFLE1BQVgsQ0FBa0JULG9CQUFvQixDQUFDRixHQUFyQixDQUF5QixDQUFDN0UsTUFBRCxFQUFTaEIsQ0FBVCxLQUFlO0FBQ25FLGFBQVEsNkJBQUMsVUFBRDtBQUFZLFFBQUEsR0FBRyxFQUFFQSxDQUFDLEdBQUd1RyxRQUFyQjtBQUErQixRQUFBLE1BQU0sRUFBRTVGLE1BQXZDO0FBQStDLFFBQUEsTUFBTSxFQUFFSztBQUF2RCxRQUFSO0FBQ0gsS0FGOEIsQ0FBbEIsQ0FBYjtBQUdIOztBQUVELFNBQ0k7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0ksMENBQU1zRixVQUFOLENBREosRUFFSSwwQ0FBTUQsWUFBTixDQUZKLENBREo7QUFNSDs7QUFFRCxNQUFNSSxrQkFBa0IsR0FBRyxDQUFDO0FBQUN4QyxFQUFBQSxNQUFEO0FBQVN5QyxFQUFBQSxTQUFUO0FBQW9CQyxFQUFBQSxTQUFwQjtBQUErQi9HLEVBQUFBO0FBQS9CLENBQUQsS0FBNkM7QUFDcEUsUUFBTWMsR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWjtBQUVBLE1BQUltQyxZQUFZLEdBQUcsSUFBbkI7QUFDQSxNQUFJQyxnQkFBZ0IsR0FBRyxJQUF2QjtBQUNBLE1BQUlDLGdCQUFnQixHQUFHLElBQXZCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsSUFBeEI7QUFFQSxRQUFNN0YsSUFBSSxHQUFHK0MsTUFBTSxDQUFDdEQsTUFBUCxLQUFrQkQsR0FBRyxDQUFDUyxTQUFKLEVBQS9COztBQUVBLFFBQU02RixnQkFBZ0IsR0FBRyxNQUFNO0FBQzNCLFVBQU1DLFdBQVcsR0FBR3pCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0F5QixtQkFBTUMsbUJBQU4sQ0FBMEIsMEJBQTFCLEVBQXNELEVBQXRELEVBQTBERixXQUExRCxFQUF1RTtBQUNuRUcsTUFBQUEsTUFBTSxFQUFFbkQ7QUFEMkQsS0FBdkU7QUFHSCxHQUxELENBVm9FLENBaUJwRTtBQUNBOzs7QUFDQSxNQUFJLENBQUMvQyxJQUFMLEVBQVc7QUFDUCxVQUFNbUcsY0FBYyxHQUFHLE1BQU07QUFDekIsWUFBTUMsWUFBWSxHQUFHNUcsR0FBRyxDQUFDNkcsZUFBSixFQUFyQjs7QUFDQSxVQUFJYixTQUFKLEVBQWU7QUFDWCxjQUFNYyxLQUFLLEdBQUdGLFlBQVksQ0FBQ0csT0FBYixDQUFxQnhELE1BQU0sQ0FBQ3RELE1BQTVCLENBQWQ7QUFDQSxZQUFJNkcsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQkYsWUFBWSxDQUFDSSxNQUFiLENBQW9CRixLQUFwQixFQUEyQixDQUEzQjtBQUNyQixPQUhELE1BR087QUFDSEYsUUFBQUEsWUFBWSxDQUFDakgsSUFBYixDQUFrQjRELE1BQU0sQ0FBQ3RELE1BQXpCO0FBQ0g7O0FBRURELE1BQUFBLEdBQUcsQ0FBQ2lILGVBQUosQ0FBb0JMLFlBQXBCO0FBQ0gsS0FWRDs7QUFZQVYsSUFBQUEsWUFBWSxHQUNSLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFUyxjQUEzQjtBQUEyQyxNQUFBLFNBQVMsRUFBRSx5QkFBVyxtQkFBWCxFQUFnQztBQUFDTyxRQUFBQSx1QkFBdUIsRUFBRSxDQUFDbEI7QUFBM0IsT0FBaEM7QUFBdEQsT0FDTUEsU0FBUyxHQUFHLHlCQUFHLFVBQUgsQ0FBSCxHQUFvQix5QkFBRyxRQUFILENBRG5DLENBREo7O0FBTUEsUUFBSXpDLE1BQU0sQ0FBQzVCLE1BQVgsRUFBbUI7QUFDZixZQUFNd0YsbUJBQW1CLEdBQUcsWUFBVztBQUNuQyxjQUFNdkYsSUFBSSxHQUFHNUIsR0FBRyxDQUFDNkIsT0FBSixDQUFZMEIsTUFBTSxDQUFDNUIsTUFBbkIsQ0FBYjs7QUFDQUssNEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxVQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUa0YsVUFBQUEsV0FBVyxFQUFFLElBRko7QUFHVEMsVUFBQUEsUUFBUSxFQUFFekYsSUFBSSxDQUFDMEYsZ0JBQUwsQ0FBc0IvRCxNQUFNLENBQUN0RCxNQUE3QixDQUhEO0FBSVRrQyxVQUFBQSxPQUFPLEVBQUVvQixNQUFNLENBQUM1QjtBQUpQLFNBQWI7QUFNSCxPQVJEOztBQVVBLFlBQU00RixrQkFBa0IsR0FBRyxZQUFXO0FBQ2xDdkYsNEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxVQUFBQSxNQUFNLEVBQUUsZ0JBREM7QUFFVHNGLFVBQUFBLE9BQU8sRUFBRWpFLE1BQU0sQ0FBQ3REO0FBRlAsU0FBYjtBQUlILE9BTEQ7O0FBT0FvRyxNQUFBQSxpQkFBaUIsR0FDYiw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRWMsbUJBQTNCO0FBQWdELFFBQUEsU0FBUyxFQUFDO0FBQTFELFNBQ00seUJBQUcsc0JBQUgsQ0FETixDQURKO0FBTUFoQixNQUFBQSxnQkFBZ0IsR0FDWiw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRW9CLGtCQUEzQjtBQUErQyxRQUFBLFNBQVMsRUFBRTtBQUExRCxTQUNNLHlCQUFHLFNBQUgsQ0FETixDQURKO0FBS0g7O0FBRUQsUUFBSXRCLFNBQVMsS0FBSyxDQUFDMUMsTUFBRCxJQUFXLENBQUNBLE1BQU0sQ0FBQ2tFLFVBQW5CLElBQWlDbEUsTUFBTSxDQUFDa0UsVUFBUCxLQUFzQixPQUE1RCxDQUFiLEVBQW1GO0FBQy9FLFlBQU05RixNQUFNLEdBQUc0QixNQUFNLElBQUlBLE1BQU0sQ0FBQzVCLE1BQWpCLEdBQTBCNEIsTUFBTSxDQUFDNUIsTUFBakMsR0FBMEMrRix1QkFBY0MsU0FBZCxFQUF6RDs7QUFDQSxZQUFNQyxrQkFBa0IsR0FBRyxZQUFZO0FBQ25DLFlBQUk7QUFDQTtBQUNBO0FBQ0EsZ0JBQU1DLE9BQU8sR0FBRyxJQUFJQyxxQkFBSixDQUFpQm5HLE1BQWpCLENBQWhCO0FBQ0EsZ0JBQU1rRyxPQUFPLENBQUNFLE1BQVIsQ0FBZSxDQUFDeEUsTUFBTSxDQUFDdEQsTUFBUixDQUFmLEVBQWdDK0gsSUFBaEMsQ0FBcUMsTUFBTTtBQUM3QyxnQkFBSUgsT0FBTyxDQUFDSSxrQkFBUixDQUEyQjFFLE1BQU0sQ0FBQ3RELE1BQWxDLE1BQThDLFNBQWxELEVBQTZEO0FBQ3pELG9CQUFNLElBQUlpSSxLQUFKLENBQVVMLE9BQU8sQ0FBQ00sWUFBUixDQUFxQjVFLE1BQU0sQ0FBQ3RELE1BQTVCLENBQVYsQ0FBTjtBQUNIO0FBQ0osV0FKSyxDQUFOO0FBS0gsU0FURCxDQVNFLE9BQU9tSSxHQUFQLEVBQVk7QUFDVixnQkFBTUMsV0FBVyxHQUFHdkQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQXlCLHlCQUFNQyxtQkFBTixDQUEwQixrQkFBMUIsRUFBOEMsRUFBOUMsRUFBa0Q0QixXQUFsRCxFQUErRDtBQUMzREMsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFILENBRG9EO0FBRTNEQyxZQUFBQSxXQUFXLEVBQUlILEdBQUcsSUFBSUEsR0FBRyxDQUFDSSxPQUFaLEdBQXVCSixHQUFHLENBQUNJLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRlEsV0FBL0Q7QUFJSDtBQUNKLE9BakJEOztBQW1CQXBDLE1BQUFBLGdCQUFnQixHQUNaLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFd0Isa0JBQTNCO0FBQStDLFFBQUEsU0FBUyxFQUFDO0FBQXpELFNBQ00seUJBQUcsUUFBSCxDQUROLENBREo7QUFLSDtBQUNKOztBQUVELFFBQU1hLGVBQWUsR0FDakIsNkJBQUMseUJBQUQ7QUFBa0IsSUFBQSxPQUFPLEVBQUVuQyxnQkFBM0I7QUFBNkMsSUFBQSxTQUFTLEVBQUM7QUFBdkQsS0FDTSx5QkFBRyxvQkFBSCxDQUROLENBREo7O0FBTUEsTUFBSW9DLG1CQUFKOztBQUNBLE1BQUksQ0FBQ2xJLElBQUwsRUFBVztBQUNQa0ksSUFBQUEsbUJBQW1CLEdBQ2YsNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxPQUFPLEVBQUUsTUFBTXZILGFBQWEsQ0FBQ25CLEdBQUQsRUFBTXVELE1BQU0sQ0FBQ3RELE1BQWIsQ0FBOUM7QUFBb0UsTUFBQSxTQUFTLEVBQUM7QUFBOUUsT0FDTSx5QkFBRyxnQkFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxTQUNJO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNJLHlDQUFNLHlCQUFHLFNBQUgsQ0FBTixDQURKLEVBRUksMENBQ015SSxtQkFETixFQUVNckMsaUJBRk4sRUFHTW9DLGVBSE4sRUFJTXRDLGdCQUpOLEVBS01DLGdCQUxOLEVBTU1GLFlBTk4sQ0FGSixDQURKO0FBYUgsQ0E5SEQ7O0FBZ0lBLE1BQU15QyxlQUFlLEdBQUcsWUFBWTtBQUNoQyxRQUFNQyxjQUFjLEdBQUc5RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCOztBQUNBLFFBQU07QUFBQzhELElBQUFBO0FBQUQsTUFBYXJDLGVBQU1DLG1CQUFOLENBQTBCLGVBQTFCLEVBQTJDLEVBQTNDLEVBQStDbUMsY0FBL0MsRUFBK0Q7QUFDOUVOLElBQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSCxDQUR1RTtBQUU5RUMsSUFBQUEsV0FBVyxFQUNQLDBDQUNNLHlCQUFHLDRFQUNELHdFQURDLEdBRUQsdUJBRkYsQ0FETixDQUgwRTtBQVE5RU8sSUFBQUEsTUFBTSxFQUFFLHlCQUFHLFFBQUg7QUFSc0UsR0FBL0QsQ0FBbkI7O0FBV0EsUUFBTSxDQUFDQyxTQUFELElBQWMsTUFBTUYsUUFBMUI7QUFDQSxTQUFPRSxTQUFQO0FBQ0gsQ0FmRDs7QUFpQkEsTUFBTUMsMEJBQTBCLEdBQUcsQ0FBQztBQUFDQyxFQUFBQTtBQUFELENBQUQsS0FBZ0I7QUFDL0MsU0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSSx5Q0FBTSx5QkFBRyxhQUFILENBQU4sQ0FESixFQUVJO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNNQSxRQUROLENBRkosQ0FESjtBQVFILENBVEQ7O0FBV0EsTUFBTUMsUUFBUSxHQUFHLENBQUMzRixNQUFELEVBQVM0RixpQkFBVCxLQUErQjtBQUM1QyxNQUFJLENBQUNBLGlCQUFELElBQXNCLENBQUM1RixNQUEzQixFQUFtQyxPQUFPLEtBQVA7QUFFbkMsUUFBTTZGLFdBQVcsR0FDYixDQUFDRCxpQkFBaUIsQ0FBQ0UsTUFBbEIsR0FBMkJGLGlCQUFpQixDQUFDRSxNQUFsQixDQUF5QixnQkFBekIsQ0FBM0IsR0FBd0UsSUFBekUsS0FDQUYsaUJBQWlCLENBQUNHLGNBRnRCO0FBSUEsU0FBTy9GLE1BQU0sQ0FBQ2dHLFVBQVAsR0FBb0JILFdBQTNCO0FBQ0gsQ0FSRDs7QUFVQSxNQUFNSSxrQkFBa0IsR0FBRyxDQUFDeEosR0FBRCxFQUFNNEIsSUFBTixLQUFlO0FBQ3RDLFFBQU0sQ0FBQzZILFdBQUQsRUFBY0MsY0FBZCxJQUFnQyxxQkFBUyxFQUFULENBQXRDO0FBRUEsUUFBTXhHLE1BQU0sR0FBRyx3QkFBWSxNQUFNO0FBQzdCLFFBQUksQ0FBQ3RCLElBQUwsRUFBVztBQUNQO0FBQ0g7O0FBQ0QsVUFBTXVCLEtBQUssR0FBR3ZCLElBQUksQ0FBQ3lCLFlBQUwsQ0FBa0JzRyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBZDs7QUFDQSxRQUFJeEcsS0FBSixFQUFXO0FBQ1B1RyxNQUFBQSxjQUFjLENBQUN2RyxLQUFLLENBQUN5RyxVQUFOLEVBQUQsQ0FBZDtBQUNILEtBRkQsTUFFTztBQUNIRixNQUFBQSxjQUFjLENBQUMsRUFBRCxDQUFkO0FBQ0g7O0FBQ0QsV0FBTyxNQUFNO0FBQ1RBLE1BQUFBLGNBQWMsQ0FBQyxFQUFELENBQWQ7QUFDSCxLQUZEO0FBR0gsR0FiYyxFQWFaLENBQUM5SCxJQUFELENBYlksQ0FBZjtBQWVBLHdDQUFnQjVCLEdBQWhCLEVBQXFCLG1CQUFyQixFQUEwQ2tELE1BQTFDO0FBQ0Esd0JBQVUsTUFBTTtBQUNaQSxJQUFBQSxNQUFNO0FBQ04sV0FBTyxNQUFNO0FBQ1R3RyxNQUFBQSxjQUFjLENBQUMsRUFBRCxDQUFkO0FBQ0gsS0FGRDtBQUdILEdBTEQsRUFLRyxDQUFDeEcsTUFBRCxDQUxIO0FBTUEsU0FBT3VHLFdBQVA7QUFDSCxDQTFCRDs7QUE0QkEsTUFBTUksY0FBYyxHQUFHLENBQUM7QUFBQ3RHLEVBQUFBLE1BQUQ7QUFBU3VHLEVBQUFBLGFBQVQ7QUFBd0JDLEVBQUFBO0FBQXhCLENBQUQsS0FBMkM7QUFDOUQsUUFBTS9KLEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVosQ0FEOEQsQ0FHOUQ7O0FBQ0EsTUFBSVIsTUFBTSxDQUFDa0UsVUFBUCxLQUFzQixRQUF0QixJQUFrQ2xFLE1BQU0sQ0FBQ2tFLFVBQVAsS0FBc0IsTUFBNUQsRUFBb0UsT0FBTyxJQUFQOztBQUVwRSxRQUFNdUMsTUFBTSxHQUFHLFlBQVk7QUFDdkIsVUFBTUMsdUJBQXVCLEdBQUduRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQWhDOztBQUNBLFVBQU07QUFBQzhELE1BQUFBO0FBQUQsUUFBYXJDLGVBQU1DLG1CQUFOLENBQ2YsNEJBRGUsRUFFZixRQUZlLEVBR2Z3RCx1QkFIZSxFQUlmO0FBQ0kxRyxNQUFBQSxNQURKO0FBRUlyQixNQUFBQSxNQUFNLEVBQUVxQixNQUFNLENBQUNrRSxVQUFQLEtBQXNCLFFBQXRCLEdBQWlDLHlCQUFHLFdBQUgsQ0FBakMsR0FBbUQseUJBQUcsTUFBSCxDQUYvRDtBQUdJYSxNQUFBQSxLQUFLLEVBQUUvRSxNQUFNLENBQUNrRSxVQUFQLEtBQXNCLFFBQXRCLEdBQWlDLHlCQUFHLHNCQUFILENBQWpDLEdBQThELHlCQUFHLGlCQUFILENBSHpFO0FBSUl5QyxNQUFBQSxTQUFTLEVBQUUzRyxNQUFNLENBQUNrRSxVQUFQLEtBQXNCLE1BSnJDO0FBS0kwQyxNQUFBQSxNQUFNLEVBQUU7QUFMWixLQUplLENBQW5COztBQWFBLFVBQU0sQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLElBQW9CLE1BQU14QixRQUFoQztBQUNBLFFBQUksQ0FBQ3VCLE9BQUwsRUFBYztBQUVkTixJQUFBQSxhQUFhO0FBQ2I5SixJQUFBQSxHQUFHLENBQUNzSyxJQUFKLENBQVMvRyxNQUFNLENBQUM1QixNQUFoQixFQUF3QjRCLE1BQU0sQ0FBQ3RELE1BQS9CLEVBQXVDb0ssTUFBTSxJQUFJcEgsU0FBakQsRUFBNEQrRSxJQUE1RCxDQUFpRSxNQUFNO0FBQ25FO0FBQ0E7QUFDQXVDLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGNBQVo7QUFDSCxLQUpELEVBSUcsVUFBU3BDLEdBQVQsRUFBYztBQUNiLFlBQU1DLFdBQVcsR0FBR3ZELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQXdGLE1BQUFBLE9BQU8sQ0FBQ0UsS0FBUixDQUFjLGlCQUFpQnJDLEdBQS9COztBQUNBNUIscUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxFQUE1QyxFQUFnRDRCLFdBQWhELEVBQTZEO0FBQ3pEQyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsZ0JBQUgsQ0FEa0Q7QUFFekRDLFFBQUFBLFdBQVcsRUFBSUgsR0FBRyxJQUFJQSxHQUFHLENBQUNJLE9BQVosR0FBdUJKLEdBQUcsQ0FBQ0ksT0FBM0IsR0FBcUM7QUFGTSxPQUE3RDtBQUlILEtBWEQsRUFXR2tDLE9BWEgsQ0FXVyxNQUFNO0FBQ2JYLE1BQUFBLFlBQVk7QUFDZixLQWJEO0FBY0gsR0FqQ0Q7O0FBbUNBLFFBQU1ZLFNBQVMsR0FBR3BILE1BQU0sQ0FBQ2tFLFVBQVAsS0FBc0IsUUFBdEIsR0FBaUMseUJBQUcsV0FBSCxDQUFqQyxHQUFtRCx5QkFBRyxNQUFILENBQXJFO0FBQ0EsU0FBTyw2QkFBQyx5QkFBRDtBQUFrQixJQUFBLFNBQVMsRUFBQywyQ0FBNUI7QUFBd0UsSUFBQSxPQUFPLEVBQUV1QztBQUFqRixLQUNEVyxTQURDLENBQVA7QUFHSCxDQTdDRDs7QUErQ0EsTUFBTUMsb0JBQW9CLEdBQUcsQ0FBQztBQUFDckgsRUFBQUE7QUFBRCxDQUFELEtBQWM7QUFDdkMsUUFBTXZELEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVo7O0FBRUEsUUFBTThHLG1CQUFtQixHQUFHLFlBQVk7QUFDcEMsVUFBTTtBQUFDbEosTUFBQUEsTUFBRDtBQUFTMUIsTUFBQUE7QUFBVCxRQUFtQnNELE1BQXpCO0FBQ0EsVUFBTTNCLElBQUksR0FBRzVCLEdBQUcsQ0FBQzZCLE9BQUosQ0FBWUYsTUFBWixDQUFiOztBQUNBLFFBQUksQ0FBQ0MsSUFBTCxFQUFXO0FBQ1A7QUFDSDs7QUFDRCxRQUFJa0osUUFBUSxHQUFHbEosSUFBSSxDQUFDbUosZUFBTCxFQUFmO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLEVBQXJCOztBQUNBLFdBQU9GLFFBQVAsRUFBaUI7QUFDYkUsTUFBQUEsY0FBYyxHQUFHRixRQUFRLENBQUNHLFNBQVQsR0FBcUJ2SixNQUFyQixDQUE0QixDQUFDMkgsTUFBRCxFQUFTbEcsS0FBVCxLQUFtQjtBQUM1RCxZQUFJQSxLQUFLLENBQUMrSCxTQUFOLE9BQXNCakwsTUFBdEIsSUFBZ0MsQ0FBQ2tELEtBQUssQ0FBQ2dJLFVBQU4sRUFBckMsRUFBeUQ7QUFDckQsaUJBQU85QixNQUFNLENBQUN2RCxNQUFQLENBQWMzQyxLQUFkLENBQVA7QUFDSCxTQUZELE1BRU87QUFDSCxpQkFBT2tHLE1BQVA7QUFDSDtBQUNKLE9BTmdCLEVBTWQyQixjQU5jLENBQWpCO0FBT0FGLE1BQUFBLFFBQVEsR0FBR0EsUUFBUSxDQUFDTSx1QkFBVCxDQUFpQ0MsMkJBQWNDLFNBQS9DLENBQVg7QUFDSDs7QUFFRCxVQUFNNUYsS0FBSyxHQUFHc0YsY0FBYyxDQUFDekwsTUFBN0I7QUFDQSxVQUFNZ00sSUFBSSxHQUFHaEksTUFBTSxDQUFDL0QsSUFBcEI7O0FBRUEsUUFBSWtHLEtBQUssS0FBSyxDQUFkLEVBQWlCO0FBQ2IsWUFBTThGLFVBQVUsR0FBRzFHLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBQ0F5QixxQkFBTUMsbUJBQU4sQ0FBMEIsa0NBQTFCLEVBQThELEVBQTlELEVBQWtFK0UsVUFBbEUsRUFBOEU7QUFDMUVsRCxRQUFBQSxLQUFLLEVBQUUseUJBQUcsc0NBQUgsRUFBMkM7QUFBQ2lELFVBQUFBO0FBQUQsU0FBM0MsQ0FEbUU7QUFFMUVoRCxRQUFBQSxXQUFXLEVBQ1AsMENBQ0ksd0NBQUsseUJBQUcsd0VBQUgsQ0FBTCxDQURKO0FBSHNFLE9BQTlFO0FBT0gsS0FURCxNQVNPO0FBQ0gsWUFBTUssY0FBYyxHQUFHOUQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFFQSxZQUFNO0FBQUM4RCxRQUFBQTtBQUFELFVBQWFyQyxlQUFNQyxtQkFBTixDQUEwQixnQ0FBMUIsRUFBNEQsRUFBNUQsRUFBZ0VtQyxjQUFoRSxFQUFnRjtBQUMvRk4sUUFBQUEsS0FBSyxFQUFFLHlCQUFHLG9DQUFILEVBQXlDO0FBQUNpRCxVQUFBQTtBQUFELFNBQXpDLENBRHdGO0FBRS9GaEQsUUFBQUEsV0FBVyxFQUNQLDBDQUNJLHdDQUFLLHlCQUFHLHlHQUFILEVBQThHO0FBQUM3QyxVQUFBQSxLQUFEO0FBQVE2RixVQUFBQTtBQUFSLFNBQTlHLENBQUwsQ0FESixFQUVJLHdDQUFLLHlCQUFHLDhHQUFILENBQUwsQ0FGSixDQUgyRjtBQU8vRnpDLFFBQUFBLE1BQU0sRUFBRSx5QkFBRywyQkFBSCxFQUFnQztBQUFDcEQsVUFBQUE7QUFBRCxTQUFoQztBQVB1RixPQUFoRixDQUFuQjs7QUFVQSxZQUFNLENBQUNxRCxTQUFELElBQWMsTUFBTUYsUUFBMUI7O0FBQ0EsVUFBSSxDQUFDRSxTQUFMLEVBQWdCO0FBQ1o7QUFDSCxPQWhCRSxDQWtCSDtBQUNBOzs7QUFDQSxZQUFNMEMsT0FBTyxDQUFDQyxPQUFSLEVBQU47QUFFQW5CLE1BQUFBLE9BQU8sQ0FBQ29CLElBQVIsb0NBQXlDakcsS0FBekMsMkJBQStENkYsSUFBL0QsaUJBQTBFNUosTUFBMUU7QUFDQSxZQUFNOEosT0FBTyxDQUFDRyxHQUFSLENBQVlaLGNBQWMsQ0FBQzdGLEdBQWYsQ0FBbUIsTUFBTWhDLEtBQU4sSUFBZTtBQUNoRCxZQUFJO0FBQ0EsZ0JBQU1uRCxHQUFHLENBQUM2TCxXQUFKLENBQWdCbEssTUFBaEIsRUFBd0J3QixLQUFLLENBQUNVLEtBQU4sRUFBeEIsQ0FBTjtBQUNILFNBRkQsQ0FFRSxPQUFPdUUsR0FBUCxFQUFZO0FBQ1Y7QUFDQW1DLFVBQUFBLE9BQU8sQ0FBQ0UsS0FBUixDQUFjLGtCQUFkLEVBQWtDdEgsS0FBSyxDQUFDVSxLQUFOLEVBQWxDO0FBQ0EwRyxVQUFBQSxPQUFPLENBQUNFLEtBQVIsQ0FBY3JDLEdBQWQ7QUFDSDtBQUNKLE9BUmlCLENBQVosQ0FBTjtBQVNBbUMsTUFBQUEsT0FBTyxDQUFDb0IsSUFBUixxQ0FBMENqRyxLQUExQywyQkFBZ0U2RixJQUFoRSxpQkFBMkU1SixNQUEzRTtBQUNIO0FBQ0osR0FqRUQ7O0FBbUVBLFNBQU8sNkJBQUMseUJBQUQ7QUFBa0IsSUFBQSxTQUFTLEVBQUMsMkNBQTVCO0FBQXdFLElBQUEsT0FBTyxFQUFFa0o7QUFBakYsS0FDRCx5QkFBRyx3QkFBSCxDQURDLENBQVA7QUFHSCxDQXpFRDs7QUEyRUEsTUFBTWlCLGVBQWUsR0FBRyxDQUFDO0FBQUN2SSxFQUFBQSxNQUFEO0FBQVN1RyxFQUFBQSxhQUFUO0FBQXdCQyxFQUFBQTtBQUF4QixDQUFELEtBQTJDO0FBQy9ELFFBQU0vSixHQUFHLEdBQUcsdUJBQVcrRCw0QkFBWCxDQUFaOztBQUVBLFFBQU1nSSxZQUFZLEdBQUcsWUFBWTtBQUM3QixVQUFNOUIsdUJBQXVCLEdBQUduRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQWhDOztBQUNBLFVBQU07QUFBQzhELE1BQUFBO0FBQUQsUUFBYXJDLGVBQU1DLG1CQUFOLENBQ2YsNEJBRGUsRUFFZixjQUZlLEVBR2Z3RCx1QkFIZSxFQUlmO0FBQ0kxRyxNQUFBQSxNQURKO0FBRUlyQixNQUFBQSxNQUFNLEVBQUVxQixNQUFNLENBQUNrRSxVQUFQLEtBQXNCLEtBQXRCLEdBQThCLHlCQUFHLE9BQUgsQ0FBOUIsR0FBNEMseUJBQUcsS0FBSCxDQUZ4RDtBQUdJYSxNQUFBQSxLQUFLLEVBQUUvRSxNQUFNLENBQUNrRSxVQUFQLEtBQXNCLEtBQXRCLEdBQThCLHlCQUFHLGtCQUFILENBQTlCLEdBQXVELHlCQUFHLGdCQUFILENBSGxFO0FBSUl5QyxNQUFBQSxTQUFTLEVBQUUzRyxNQUFNLENBQUNrRSxVQUFQLEtBQXNCLEtBSnJDO0FBS0kwQyxNQUFBQSxNQUFNLEVBQUU1RyxNQUFNLENBQUNrRSxVQUFQLEtBQXNCO0FBTGxDLEtBSmUsQ0FBbkI7O0FBYUEsVUFBTSxDQUFDMkMsT0FBRCxFQUFVQyxNQUFWLElBQW9CLE1BQU14QixRQUFoQztBQUNBLFFBQUksQ0FBQ3VCLE9BQUwsRUFBYztBQUVkTixJQUFBQSxhQUFhO0FBQ2IsUUFBSWtDLE9BQUo7O0FBQ0EsUUFBSXpJLE1BQU0sQ0FBQ2tFLFVBQVAsS0FBc0IsS0FBMUIsRUFBaUM7QUFDN0J1RSxNQUFBQSxPQUFPLEdBQUdoTSxHQUFHLENBQUNpTSxLQUFKLENBQVUxSSxNQUFNLENBQUM1QixNQUFqQixFQUF5QjRCLE1BQU0sQ0FBQ3RELE1BQWhDLENBQVY7QUFDSCxLQUZELE1BRU87QUFDSCtMLE1BQUFBLE9BQU8sR0FBR2hNLEdBQUcsQ0FBQ2tNLEdBQUosQ0FBUTNJLE1BQU0sQ0FBQzVCLE1BQWYsRUFBdUI0QixNQUFNLENBQUN0RCxNQUE5QixFQUFzQ29LLE1BQU0sSUFBSXBILFNBQWhELENBQVY7QUFDSDs7QUFDRCtJLElBQUFBLE9BQU8sQ0FBQ2hFLElBQVIsQ0FBYSxNQUFNO0FBQ2Y7QUFDQTtBQUNBdUMsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWjtBQUNILEtBSkQsRUFJRyxVQUFTcEMsR0FBVCxFQUFjO0FBQ2IsWUFBTUMsV0FBVyxHQUFHdkQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBd0YsTUFBQUEsT0FBTyxDQUFDRSxLQUFSLENBQWMsZ0JBQWdCckMsR0FBOUI7O0FBQ0E1QixxQkFBTUMsbUJBQU4sQ0FBMEIsb0JBQTFCLEVBQWdELEVBQWhELEVBQW9ENEIsV0FBcEQsRUFBaUU7QUFDN0RDLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRHNEO0FBRTdEQyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsb0JBQUg7QUFGZ0QsT0FBakU7QUFJSCxLQVhELEVBV0dtQyxPQVhILENBV1csTUFBTTtBQUNiWCxNQUFBQSxZQUFZO0FBQ2YsS0FiRDtBQWNILEdBdkNEOztBQXlDQSxNQUFJb0MsS0FBSyxHQUFHLHlCQUFHLEtBQUgsQ0FBWjs7QUFDQSxNQUFJNUksTUFBTSxDQUFDa0UsVUFBUCxLQUFzQixLQUExQixFQUFpQztBQUM3QjBFLElBQUFBLEtBQUssR0FBRyx5QkFBRyxPQUFILENBQVI7QUFDSDs7QUFFRCxRQUFNbkksT0FBTyxHQUFHLHlCQUFXLG1CQUFYLEVBQWdDO0FBQzVDa0QsSUFBQUEsdUJBQXVCLEVBQUUzRCxNQUFNLENBQUNrRSxVQUFQLEtBQXNCO0FBREgsR0FBaEMsQ0FBaEI7QUFJQSxTQUFPLDZCQUFDLHlCQUFEO0FBQWtCLElBQUEsU0FBUyxFQUFFekQsT0FBN0I7QUFBc0MsSUFBQSxPQUFPLEVBQUUrSDtBQUEvQyxLQUNESSxLQURDLENBQVA7QUFHSCxDQXhERDs7QUEwREEsTUFBTUMsZ0JBQWdCLEdBQUcsQ0FBQztBQUFDN0ksRUFBQUEsTUFBRDtBQUFTM0IsRUFBQUEsSUFBVDtBQUFlNkgsRUFBQUEsV0FBZjtBQUE0QkssRUFBQUEsYUFBNUI7QUFBMkNDLEVBQUFBO0FBQTNDLENBQUQsS0FBOEQ7QUFDbkYsUUFBTS9KLEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVosQ0FEbUYsQ0FHbkY7O0FBQ0EsTUFBSVIsTUFBTSxDQUFDa0UsVUFBUCxLQUFzQixNQUExQixFQUFrQyxPQUFPLElBQVA7O0FBRWxDLFFBQU00RSxPQUFPLEdBQUduRCxRQUFRLENBQUMzRixNQUFELEVBQVNrRyxXQUFULENBQXhCOztBQUNBLFFBQU02QyxZQUFZLEdBQUcsWUFBWTtBQUM3QixVQUFNakUsV0FBVyxHQUFHdkQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBLFVBQU1wRCxNQUFNLEdBQUc0QixNQUFNLENBQUM1QixNQUF0QjtBQUNBLFVBQU0rRSxNQUFNLEdBQUduRCxNQUFNLENBQUN0RCxNQUF0QixDQUg2QixDQUs3Qjs7QUFDQSxRQUFJeUcsTUFBTSxLQUFLMUcsR0FBRyxDQUFDUyxTQUFKLEVBQWYsRUFBZ0M7QUFDNUIsVUFBSTtBQUNBLFlBQUksRUFBRSxNQUFNa0ksZUFBZSxFQUF2QixDQUFKLEVBQWdDO0FBQ25DLE9BRkQsQ0FFRSxPQUFPNEQsQ0FBUCxFQUFVO0FBQ1JoQyxRQUFBQSxPQUFPLENBQUNFLEtBQVIsQ0FBYyxzQ0FBZCxFQUFzRDhCLENBQXREO0FBQ0E7QUFDSDtBQUNKOztBQUVELFVBQU1DLGVBQWUsR0FBRzVLLElBQUksQ0FBQ3lCLFlBQUwsQ0FBa0JzRyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBeEI7QUFDQSxRQUFJLENBQUM2QyxlQUFMLEVBQXNCO0FBRXRCLFVBQU0vQyxXQUFXLEdBQUcrQyxlQUFlLENBQUM1QyxVQUFoQixFQUFwQjtBQUNBLFVBQU1SLFdBQVcsR0FDYixDQUFDSyxXQUFXLENBQUNKLE1BQVosR0FBcUJJLFdBQVcsQ0FBQ0osTUFBWixDQUFtQixnQkFBbkIsQ0FBckIsR0FBNEQsSUFBN0QsS0FDQUksV0FBVyxDQUFDSCxjQUZoQjtBQUlBLFFBQUltRCxLQUFKOztBQUNBLFFBQUlKLE9BQUosRUFBYTtBQUFFO0FBQ1hJLE1BQUFBLEtBQUssR0FBR3JELFdBQVI7QUFDSCxLQUZELE1BRU87QUFBRTtBQUNMcUQsTUFBQUEsS0FBSyxHQUFHckQsV0FBVyxHQUFHLENBQXRCO0FBQ0g7O0FBQ0RxRCxJQUFBQSxLQUFLLEdBQUdDLFFBQVEsQ0FBQ0QsS0FBRCxDQUFoQjs7QUFFQSxRQUFJLENBQUNFLEtBQUssQ0FBQ0YsS0FBRCxDQUFWLEVBQW1CO0FBQ2YzQyxNQUFBQSxhQUFhO0FBQ2I5SixNQUFBQSxHQUFHLENBQUM0TSxhQUFKLENBQWtCakwsTUFBbEIsRUFBMEIrRSxNQUExQixFQUFrQytGLEtBQWxDLEVBQXlDRCxlQUF6QyxFQUEwRHhFLElBQTFELENBQStELE1BQU07QUFDakU7QUFDQTtBQUNBdUMsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkscUJBQVo7QUFDSCxPQUpELEVBSUcsVUFBU3BDLEdBQVQsRUFBYztBQUNibUMsUUFBQUEsT0FBTyxDQUFDRSxLQUFSLENBQWMsaUJBQWlCckMsR0FBL0I7O0FBQ0E1Qix1QkFBTUMsbUJBQU4sQ0FBMEIscUJBQTFCLEVBQWlELEVBQWpELEVBQXFENEIsV0FBckQsRUFBa0U7QUFDOURDLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRHVEO0FBRTlEQyxVQUFBQSxXQUFXLEVBQUUseUJBQUcscUJBQUg7QUFGaUQsU0FBbEU7QUFJSCxPQVZELEVBVUdtQyxPQVZILENBVVcsTUFBTTtBQUNiWCxRQUFBQSxZQUFZO0FBQ2YsT0FaRDtBQWFIO0FBQ0osR0EvQ0Q7O0FBaURBLFFBQU0vRixPQUFPLEdBQUcseUJBQVcsbUJBQVgsRUFBZ0M7QUFDNUNrRCxJQUFBQSx1QkFBdUIsRUFBRSxDQUFDbUY7QUFEa0IsR0FBaEMsQ0FBaEI7QUFJQSxRQUFNUSxTQUFTLEdBQUdSLE9BQU8sR0FBRyx5QkFBRyxRQUFILENBQUgsR0FBa0IseUJBQUcsTUFBSCxDQUEzQztBQUNBLFNBQU8sNkJBQUMseUJBQUQ7QUFBa0IsSUFBQSxTQUFTLEVBQUVySSxPQUE3QjtBQUFzQyxJQUFBLE9BQU8sRUFBRXNJO0FBQS9DLEtBQ0RPLFNBREMsQ0FBUDtBQUdILENBaEVEOztBQWtFQSxNQUFNQyx1QkFBdUIsR0FBRyxDQUFDO0FBQUNsTCxFQUFBQSxJQUFEO0FBQU9xSCxFQUFBQSxRQUFQO0FBQWlCMUYsRUFBQUEsTUFBakI7QUFBeUJ1RyxFQUFBQSxhQUF6QjtBQUF3Q0MsRUFBQUEsWUFBeEM7QUFBc0ROLEVBQUFBO0FBQXRELENBQUQsS0FBd0U7QUFDcEcsUUFBTXpKLEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVo7QUFDQSxNQUFJZ0osVUFBSjtBQUNBLE1BQUlDLFNBQUo7QUFDQSxNQUFJQyxVQUFKO0FBQ0EsTUFBSUMsWUFBSjtBQUVBLFFBQU1DLGNBQWMsR0FDaEIsQ0FBQzFELFdBQVcsQ0FBQ0osTUFBWixHQUFxQkksV0FBVyxDQUFDSixNQUFaLENBQW1CLHFCQUFuQixDQUFyQixHQUFpRSxJQUFsRSxLQUNBSSxXQUFXLENBQUMyRCxhQUZoQjtBQUtBLFFBQU1DLEVBQUUsR0FBR3pMLElBQUksQ0FBQzBMLFNBQUwsQ0FBZXROLEdBQUcsQ0FBQ1MsU0FBSixFQUFmLENBQVg7QUFDQSxRQUFNRCxJQUFJLEdBQUc2TSxFQUFFLENBQUNwTixNQUFILEtBQWNzRCxNQUFNLENBQUN0RCxNQUFsQztBQUNBLFFBQU1zTixhQUFhLEdBQUdoSyxNQUFNLENBQUNnRyxVQUFQLEdBQW9COEQsRUFBRSxDQUFDOUQsVUFBdkIsSUFBcUMvSSxJQUEzRDs7QUFFQSxNQUFJK00sYUFBYSxJQUFJRixFQUFFLENBQUM5RCxVQUFILElBQWlCRSxXQUFXLENBQUNhLElBQWxELEVBQXdEO0FBQ3BEeUMsSUFBQUEsVUFBVSxHQUFHLDZCQUFDLGNBQUQ7QUFBZ0IsTUFBQSxNQUFNLEVBQUV4SixNQUF4QjtBQUFnQyxNQUFBLGFBQWEsRUFBRXVHLGFBQS9DO0FBQThELE1BQUEsWUFBWSxFQUFFQztBQUE1RSxNQUFiO0FBQ0g7O0FBQ0QsTUFBSXNELEVBQUUsQ0FBQzlELFVBQUgsSUFBaUJFLFdBQVcsQ0FBQytELE1BQWpDLEVBQXlDO0FBQ3JDTixJQUFBQSxZQUFZLEdBQ1IsNkJBQUMsb0JBQUQ7QUFBc0IsTUFBQSxNQUFNLEVBQUUzSixNQUE5QjtBQUFzQyxNQUFBLGFBQWEsRUFBRXVHLGFBQXJEO0FBQW9FLE1BQUEsWUFBWSxFQUFFQztBQUFsRixNQURKO0FBR0g7O0FBQ0QsTUFBSXdELGFBQWEsSUFBSUYsRUFBRSxDQUFDOUQsVUFBSCxJQUFpQkUsV0FBVyxDQUFDeUMsR0FBbEQsRUFBdUQ7QUFDbkRjLElBQUFBLFNBQVMsR0FBRyw2QkFBQyxlQUFEO0FBQWlCLE1BQUEsTUFBTSxFQUFFekosTUFBekI7QUFBaUMsTUFBQSxhQUFhLEVBQUV1RyxhQUFoRDtBQUErRCxNQUFBLFlBQVksRUFBRUM7QUFBN0UsTUFBWjtBQUNIOztBQUNELE1BQUl3RCxhQUFhLElBQUlGLEVBQUUsQ0FBQzlELFVBQUgsSUFBaUI0RCxjQUF0QyxFQUFzRDtBQUNsREYsSUFBQUEsVUFBVSxHQUNOLDZCQUFDLGdCQUFEO0FBQ0ksTUFBQSxNQUFNLEVBQUUxSixNQURaO0FBRUksTUFBQSxJQUFJLEVBQUUzQixJQUZWO0FBR0ksTUFBQSxXQUFXLEVBQUU2SCxXQUhqQjtBQUlJLE1BQUEsYUFBYSxFQUFFSyxhQUpuQjtBQUtJLE1BQUEsWUFBWSxFQUFFQztBQUxsQixNQURKO0FBU0g7O0FBRUQsTUFBSWdELFVBQVUsSUFBSUMsU0FBZCxJQUEyQkMsVUFBM0IsSUFBeUNDLFlBQXpDLElBQXlEakUsUUFBN0QsRUFBdUU7QUFDbkUsV0FBTyw2QkFBQywwQkFBRCxRQUNEZ0UsVUFEQyxFQUVERixVQUZDLEVBR0RDLFNBSEMsRUFJREUsWUFKQyxFQUtEakUsUUFMQyxDQUFQO0FBT0g7O0FBRUQsU0FBTyx5Q0FBUDtBQUNILENBbEREOztBQW9EQSxNQUFNd0Usc0JBQXNCLEdBQUcsQ0FBQztBQUFDeEUsRUFBQUEsUUFBRDtBQUFXeUUsRUFBQUEsT0FBWDtBQUFvQkMsRUFBQUEsV0FBcEI7QUFBaUM3RCxFQUFBQSxhQUFqQztBQUFnREMsRUFBQUE7QUFBaEQsQ0FBRCxLQUFtRTtBQUM5RixRQUFNL0osR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWjtBQUVBLFFBQU0sQ0FBQzZKLFlBQUQsRUFBZUMsZUFBZixJQUFrQyxxQkFBUyxLQUFULENBQXhDO0FBQ0EsUUFBTSxDQUFDQyxTQUFELEVBQVlDLFlBQVosSUFBNEIscUJBQVMsS0FBVCxDQUFsQyxDQUo4RixDQU05Rjs7QUFDQSx3QkFBVSxNQUFNO0FBQ1osUUFBSUMsU0FBUyxHQUFHLEtBQWhCOztBQUVBLFVBQU1DLG1CQUFtQixHQUFHLE1BQU07QUFDOUIsVUFBSUQsU0FBSixFQUFlO0FBQ2ZILE1BQUFBLGVBQWUsQ0FBQ0ssb0JBQVdDLGdCQUFYLENBQTRCVCxPQUE1QixDQUFELENBQWY7QUFDQUssTUFBQUEsWUFBWSxDQUFDRyxvQkFBV0Usc0JBQVgsQ0FBa0NWLE9BQWxDLEVBQTJDck4sSUFBM0MsQ0FDUmdPLENBQUQsSUFBT0EsQ0FBQyxDQUFDcE8sTUFBRixLQUFhME4sV0FBVyxDQUFDMU4sTUFEdkIsQ0FBRCxDQUFaO0FBR0gsS0FORDs7QUFRQWlPLHdCQUFXSSxnQkFBWCxDQUE0QlosT0FBNUIsRUFBcUNPLG1CQUFyQzs7QUFDQUEsSUFBQUEsbUJBQW1CLEdBWlAsQ0FhWjs7QUFDQSxXQUFPLE1BQU07QUFDVEQsTUFBQUEsU0FBUyxHQUFHLElBQVo7O0FBQ0FFLDBCQUFXSyxrQkFBWCxDQUE4Qk4sbUJBQTlCO0FBQ0gsS0FIRDtBQUlILEdBbEJELEVBa0JHLENBQUNQLE9BQUQsRUFBVUMsV0FBVyxDQUFDMU4sTUFBdEIsQ0FsQkg7O0FBb0JBLE1BQUkyTixZQUFKLEVBQWtCO0FBQ2QsVUFBTVksT0FBTyxHQUFHLFlBQVk7QUFDeEIsWUFBTXZFLHVCQUF1QixHQUFHbkYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUFoQzs7QUFDQSxZQUFNO0FBQUM4RCxRQUFBQTtBQUFELFVBQWFyQyxlQUFNaUksWUFBTixDQUFtQnhFLHVCQUFuQixFQUE0QztBQUMzRDdJLFFBQUFBLFlBQVksRUFBRXBCLEdBRDZDO0FBRTNEMk4sUUFBQUEsV0FGMkQ7QUFHM0R6TCxRQUFBQSxNQUFNLEVBQUU0TCxTQUFTLEdBQUcseUJBQUcsV0FBSCxDQUFILEdBQXFCLHlCQUFHLHVCQUFILENBSHFCO0FBSTNEeEYsUUFBQUEsS0FBSyxFQUFFd0YsU0FBUyxHQUFHLHlCQUFHLHFDQUFILENBQUgsR0FDVix5QkFBRyxrQ0FBSCxDQUxxRDtBQU0zRDNELFFBQUFBLE1BQU0sRUFBRTtBQU5tRCxPQUE1QyxDQUFuQjs7QUFTQSxZQUFNLENBQUNDLE9BQUQsSUFBWSxNQUFNdkIsUUFBeEI7QUFDQSxVQUFJLENBQUN1QixPQUFMLEVBQWM7QUFFZE4sTUFBQUEsYUFBYTtBQUNiOUosTUFBQUEsR0FBRyxDQUFDME8sbUJBQUosQ0FBd0JoQixPQUF4QixFQUFpQ0MsV0FBVyxDQUFDMU4sTUFBN0MsRUFBcUQrSCxJQUFyRCxDQUEwRCxNQUFNO0FBQzVEO0FBQ0FoRyw0QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxXQURDO0FBRVRxQixVQUFBQSxNQUFNLEVBQUU7QUFGQyxTQUFiO0FBSUgsT0FORCxFQU1Hb0wsS0FOSCxDQU1VcEMsQ0FBRCxJQUFPO0FBQ1osY0FBTWxFLFdBQVcsR0FBR3ZELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0F5Qix1QkFBTUMsbUJBQU4sQ0FBMEIsa0NBQTFCLEVBQThELEVBQTlELEVBQWtFNEIsV0FBbEUsRUFBK0U7QUFDM0VDLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRG9FO0FBRTNFQyxVQUFBQSxXQUFXLEVBQUV1RixTQUFTLEdBQ2xCLHlCQUFHLCtCQUFILENBRGtCLEdBRWxCLHlCQUFHLHNDQUFIO0FBSnVFLFNBQS9FOztBQU1BdkQsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkrQixDQUFaO0FBQ0gsT0FmRCxFQWVHN0IsT0FmSCxDQWVXLE1BQU07QUFDYlgsUUFBQUEsWUFBWTtBQUNmLE9BakJEO0FBa0JILEtBakNEOztBQW1DQSxVQUFNZ0QsVUFBVSxHQUNaLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLDJDQUE1QjtBQUF3RSxNQUFBLE9BQU8sRUFBRXlCO0FBQWpGLE9BQ01WLFNBQVMsR0FBRyx5QkFBRyxXQUFILENBQUgsR0FBcUIseUJBQUcsdUJBQUgsQ0FEcEMsQ0FESixDQXBDYyxDQTBDZDs7QUFDQTs7Ozs7O0FBS0EsV0FBTyw2QkFBQywwQkFBRCxRQUNEZixVQURDLEVBRUQ5RCxRQUZDLENBQVA7QUFJSDs7QUFFRCxTQUFPLHlDQUFQO0FBQ0gsQ0FsRkQ7O0FBb0ZBLE1BQU0yRixXQUFXLEdBQUdDLG1CQUFVQyxLQUFWLENBQWdCO0FBQ2hDN08sRUFBQUEsTUFBTSxFQUFFNE8sbUJBQVVFLE1BQVYsQ0FBaUJDLFVBRE87QUFFaENDLEVBQUFBLFdBQVcsRUFBRUosbUJBQVVFLE1BRlM7QUFFRDtBQUMvQkcsRUFBQUEsU0FBUyxFQUFFTCxtQkFBVUU7QUFIVyxDQUFoQixDQUFwQjs7QUFNQSxNQUFNSSxpQkFBaUIsR0FBSW5QLEdBQUQsSUFBUztBQUMvQixRQUFNLENBQUNvUCxPQUFELEVBQVVDLFVBQVYsSUFBd0IscUJBQVMsS0FBVCxDQUE5QjtBQUNBLHdCQUFVLE1BQU07QUFDWnJQLElBQUFBLEdBQUcsQ0FBQ3NQLHNCQUFKLEdBQTZCdEgsSUFBN0IsQ0FBbUNvSCxPQUFELElBQWE7QUFDM0NDLE1BQUFBLFVBQVUsQ0FBQ0QsT0FBRCxDQUFWO0FBQ0gsS0FGRCxFQUVHLE1BQU07QUFDTEMsTUFBQUEsVUFBVSxDQUFDLEtBQUQsQ0FBVjtBQUNILEtBSkQ7QUFLSCxHQU5ELEVBTUcsQ0FBQ3JQLEdBQUQsQ0FOSDtBQU9BLFNBQU9vUCxPQUFQO0FBQ0gsQ0FWRDs7QUFZQSxNQUFNRyxpQ0FBaUMsR0FBSXZQLEdBQUQsSUFBUztBQUMvQyxTQUFPLGdDQUFhLFlBQVk7QUFDNUIsV0FBT0EsR0FBRyxDQUFDd1AsZ0NBQUosQ0FBcUMsOEJBQXJDLENBQVA7QUFDSCxHQUZNLEVBRUosQ0FBQ3hQLEdBQUQsQ0FGSSxFQUVHLEtBRkgsQ0FBUDtBQUdILENBSkQ7O0FBTUEsU0FBU3lQLGtCQUFULENBQTRCelAsR0FBNUIsRUFBaUM0QixJQUFqQyxFQUF1QzJKLElBQXZDLEVBQTZDO0FBQ3pDLFFBQU0sQ0FBQ21FLGVBQUQsRUFBa0JDLGtCQUFsQixJQUF3QyxxQkFBUztBQUNuRDtBQUNBQyxJQUFBQSxjQUFjLEVBQUUsQ0FBQyxDQUZrQztBQUduREMsSUFBQUEsT0FBTyxFQUFFLEtBSDBDO0FBSW5ENUosSUFBQUEsU0FBUyxFQUFFO0FBSndDLEdBQVQsQ0FBOUM7QUFNQSxRQUFNNkoscUJBQXFCLEdBQUcsd0JBQVksTUFBTTtBQUM1QyxRQUFJLENBQUNsTyxJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUVELFVBQU00SyxlQUFlLEdBQUc1SyxJQUFJLENBQUN5QixZQUFMLENBQWtCc0csY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQXhCO0FBQ0EsUUFBSSxDQUFDNkMsZUFBTCxFQUFzQjtBQUN0QixVQUFNL0MsV0FBVyxHQUFHK0MsZUFBZSxDQUFDNUMsVUFBaEIsRUFBcEI7QUFDQSxRQUFJLENBQUNILFdBQUwsRUFBa0I7QUFFbEIsVUFBTTRELEVBQUUsR0FBR3pMLElBQUksQ0FBQzBMLFNBQUwsQ0FBZXROLEdBQUcsQ0FBQ1MsU0FBSixFQUFmLENBQVg7QUFDQSxRQUFJLENBQUM0TSxFQUFMLEVBQVM7QUFFVCxVQUFNMEMsSUFBSSxHQUFHeEUsSUFBYjtBQUNBLFVBQU0vSyxJQUFJLEdBQUc2TSxFQUFFLENBQUNwTixNQUFILEtBQWM4UCxJQUFJLENBQUM5UCxNQUFoQztBQUNBLFVBQU1zTixhQUFhLEdBQUd3QyxJQUFJLENBQUN4RyxVQUFMLEdBQWtCOEQsRUFBRSxDQUFDOUQsVUFBckIsSUFBbUMvSSxJQUF6RDtBQUVBLFFBQUlvUCxjQUFjLEdBQUcsQ0FBQyxDQUF0Qjs7QUFDQSxRQUFJckMsYUFBSixFQUFtQjtBQUNmLFlBQU1KLGNBQWMsR0FDaEIsQ0FBQzFELFdBQVcsQ0FBQ0osTUFBWixHQUFxQkksV0FBVyxDQUFDSixNQUFaLENBQW1CLHFCQUFuQixDQUFyQixHQUFpRSxJQUFsRSxLQUNBSSxXQUFXLENBQUMyRCxhQUZoQjs7QUFJQSxVQUFJQyxFQUFFLENBQUM5RCxVQUFILElBQWlCNEQsY0FBakIsS0FBb0MzTSxJQUFJLElBQUk2TSxFQUFFLENBQUM5RCxVQUFILEdBQWdCd0csSUFBSSxDQUFDeEcsVUFBakUsQ0FBSixFQUFrRjtBQUM5RXFHLFFBQUFBLGNBQWMsR0FBR3ZDLEVBQUUsQ0FBQzlELFVBQXBCO0FBQ0g7QUFDSjs7QUFFRG9HLElBQUFBLGtCQUFrQixDQUFDO0FBQ2YxSixNQUFBQSxTQUFTLEVBQUVvSCxFQUFFLENBQUM5RCxVQUFILElBQWlCRSxXQUFXLENBQUMxQixNQUR6QjtBQUVmOEgsTUFBQUEsT0FBTyxFQUFFRCxjQUFjLElBQUksQ0FGWjtBQUdmQSxNQUFBQTtBQUhlLEtBQUQsQ0FBbEI7QUFLSCxHQWpDNkIsRUFpQzNCLENBQUM1UCxHQUFELEVBQU11TCxJQUFOLEVBQVkzSixJQUFaLENBakMyQixDQUE5QjtBQWtDQSx3Q0FBZ0I1QixHQUFoQixFQUFxQixtQkFBckIsRUFBMEM4UCxxQkFBMUM7QUFDQSx3QkFBVSxNQUFNO0FBQ1pBLElBQUFBLHFCQUFxQjtBQUNyQixXQUFPLE1BQU07QUFDVEgsTUFBQUEsa0JBQWtCLENBQUM7QUFDZkssUUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxDQURMO0FBRWZILFFBQUFBLE9BQU8sRUFBRSxLQUZNO0FBR2Y1SixRQUFBQSxTQUFTLEVBQUU7QUFISSxPQUFELENBQWxCO0FBS0gsS0FORDtBQU9ILEdBVEQsRUFTRyxDQUFDNkoscUJBQUQsQ0FUSDtBQVdBLFNBQU9KLGVBQVA7QUFDSDs7QUFFRCxNQUFNTyxpQkFBaUIsR0FBRyxDQUFDO0FBQUMxRSxFQUFBQSxJQUFEO0FBQU8zSixFQUFBQSxJQUFQO0FBQWE4TixFQUFBQSxlQUFiO0FBQThCakcsRUFBQUE7QUFBOUIsQ0FBRCxLQUFnRDtBQUN0RSxRQUFNLENBQUN5RyxTQUFELEVBQVlDLFVBQVosSUFBMEIscUJBQVMsS0FBVCxDQUFoQzs7QUFDQSxNQUFJdk8sSUFBSSxJQUFJMkosSUFBSSxDQUFDNUosTUFBakIsRUFBeUI7QUFBRTtBQUN2QixRQUFJdU8sU0FBSixFQUFlO0FBQ1gsYUFBUSw2QkFBQyxnQkFBRDtBQUNKLFFBQUEsSUFBSSxFQUFFM0UsSUFERjtBQUNRLFFBQUEsSUFBSSxFQUFFM0osSUFEZDtBQUNvQixRQUFBLGVBQWUsRUFBRThOLGVBRHJDO0FBRUosUUFBQSxVQUFVLEVBQUUsTUFBTVMsVUFBVSxDQUFDLEtBQUQ7QUFGeEIsUUFBUjtBQUdILEtBSkQsTUFJTztBQUNILFlBQU1DLFVBQVUsR0FBR3RMLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBbkI7QUFDQSxZQUFNc0wsc0JBQXNCLEdBQUc1RyxXQUFXLENBQUM2RyxhQUFaLElBQTZCLENBQTVEO0FBQ0EsWUFBTS9HLFVBQVUsR0FBR21ELFFBQVEsQ0FBQ25CLElBQUksQ0FBQ2hDLFVBQU4sRUFBa0IsRUFBbEIsQ0FBM0I7QUFDQSxZQUFNZ0gsWUFBWSxHQUFHYixlQUFlLENBQUNHLE9BQWhCLEdBQ2hCLDZCQUFDLFVBQUQ7QUFBWSxRQUFBLElBQUksRUFBQyxNQUFqQjtBQUF3QixRQUFBLE9BQU8sRUFBRSxNQUFNTSxVQUFVLENBQUMsSUFBRDtBQUFqRCxRQURnQixHQUMrQyxJQURwRTtBQUVBLFlBQU1LLElBQUksR0FBRyw4QkFBa0JqSCxVQUFsQixFQUE4QjhHLHNCQUE5QixDQUFiO0FBQ0EsWUFBTWxFLEtBQUssR0FBRyx5QkFBRywyQ0FBSCxFQUNWO0FBQUNxRSxRQUFBQSxJQUFEO0FBQU9DLFFBQUFBLFFBQVEsRUFBRTdPLElBQUksQ0FBQ3BDO0FBQXRCLE9BRFUsRUFFVjtBQUFDa1IsUUFBQUEsTUFBTSxFQUFFdkUsS0FBSyxJQUFJLDZDQUFTQSxLQUFUO0FBQWxCLE9BRlUsQ0FBZDtBQUlBLGFBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQThDQSxLQUE5QyxFQUFxRG9FLFlBQXJELENBREosQ0FESjtBQUtIO0FBQ0osR0F0QkQsTUFzQk87QUFDSCxXQUFPLElBQVA7QUFDSDtBQUNKLENBM0JEOztBQTZCQSxNQUFNSSxnQkFBZ0IsR0FBRyxDQUFDO0FBQUNwRixFQUFBQSxJQUFEO0FBQU8zSixFQUFBQSxJQUFQO0FBQWE4TixFQUFBQSxlQUFiO0FBQThCa0IsRUFBQUE7QUFBOUIsQ0FBRCxLQUErQztBQUNwRSxRQUFNNVEsR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWjtBQUVBLFFBQU0sQ0FBQzhNLFVBQUQsRUFBYUMsYUFBYixJQUE4QixxQkFBUyxLQUFULENBQXBDO0FBQ0EsUUFBTSxDQUFDQyxrQkFBRCxFQUFxQkMscUJBQXJCLElBQThDLHFCQUFTdEUsUUFBUSxDQUFDbkIsSUFBSSxDQUFDaEMsVUFBTixFQUFrQixFQUFsQixDQUFqQixDQUFwRDtBQUNBLFFBQU0sQ0FBQzBILE9BQUQsRUFBVUMsVUFBVixJQUF3QixxQkFBUyxLQUFULENBQTlCO0FBQ0EsUUFBTUMsYUFBYSxHQUFHLHdCQUFhNUgsVUFBRCxJQUFnQjtBQUM5QzJILElBQUFBLFVBQVUsQ0FBQyxJQUFELENBQVY7QUFDQUYsSUFBQUEscUJBQXFCLENBQUN0RSxRQUFRLENBQUNuRCxVQUFELEVBQWEsRUFBYixDQUFULENBQXJCO0FBQ0gsR0FIcUIsRUFHbkIsQ0FBQ3lILHFCQUFELEVBQXdCRSxVQUF4QixDQUhtQixDQUF0QjtBQUtBLFFBQU1FLGdCQUFnQixHQUFHLHdCQUFZLFlBQVk7QUFDN0MsVUFBTUMsaUJBQWlCLEdBQUcsQ0FBQzFQLE1BQUQsRUFBUytFLE1BQVQsRUFBaUI2QyxVQUFqQixFQUE2QmlELGVBQTdCLEtBQWlEO0FBQ3ZFLGFBQU94TSxHQUFHLENBQUM0TSxhQUFKLENBQWtCakwsTUFBbEIsRUFBMEIrRSxNQUExQixFQUFrQ2dHLFFBQVEsQ0FBQ25ELFVBQUQsQ0FBMUMsRUFBd0RpRCxlQUF4RCxFQUF5RXhFLElBQXpFLENBQ0gsWUFBVztBQUNQO0FBQ0E7QUFDQXVDLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHNCQUFaO0FBQ0gsT0FMRSxFQUtBLFVBQVNwQyxHQUFULEVBQWM7QUFDYixjQUFNQyxXQUFXLEdBQUd2RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0F3RixRQUFBQSxPQUFPLENBQUNFLEtBQVIsQ0FBYyxrQ0FBa0NyQyxHQUFoRDs7QUFDQTVCLHVCQUFNQyxtQkFBTixDQUEwQiw4QkFBMUIsRUFBMEQsRUFBMUQsRUFBOEQ0QixXQUE5RCxFQUEyRTtBQUN2RUMsVUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FEZ0U7QUFFdkVDLFVBQUFBLFdBQVcsRUFBRSx5QkFBRyw4QkFBSDtBQUYwRCxTQUEzRTtBQUlILE9BWkUsQ0FBUDtBQWNILEtBZkQ7O0FBaUJBLFFBQUk7QUFDQSxVQUFJLENBQUMwSSxPQUFMLEVBQWM7QUFDVjtBQUNIOztBQUVESCxNQUFBQSxhQUFhLENBQUMsSUFBRCxDQUFiO0FBRUEsWUFBTXZILFVBQVUsR0FBR3dILGtCQUFuQjtBQUVBLFlBQU1wUCxNQUFNLEdBQUc0SixJQUFJLENBQUM1SixNQUFwQjtBQUNBLFlBQU0rRSxNQUFNLEdBQUc2RSxJQUFJLENBQUN0TCxNQUFwQjtBQUVBLFlBQU11TSxlQUFlLEdBQUc1SyxJQUFJLENBQUN5QixZQUFMLENBQWtCc0csY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQXhCO0FBQ0EsVUFBSSxDQUFDNkMsZUFBTCxFQUFzQjs7QUFFdEIsVUFBSSxDQUFDQSxlQUFlLENBQUM1QyxVQUFoQixHQUE2QjBILEtBQWxDLEVBQXlDO0FBQ3JDRCxRQUFBQSxpQkFBaUIsQ0FBQzFQLE1BQUQsRUFBUytFLE1BQVQsRUFBaUI2QyxVQUFqQixFQUE2QmlELGVBQTdCLENBQWpCOztBQUNBO0FBQ0g7O0FBRUQsWUFBTStFLFFBQVEsR0FBR3ZSLEdBQUcsQ0FBQ1MsU0FBSixFQUFqQjtBQUNBLFlBQU1tSSxjQUFjLEdBQUc5RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCLENBckJBLENBdUJBOztBQUNBLFVBQUl3TSxRQUFRLEtBQUs3SyxNQUFqQixFQUF5QjtBQUNyQixZQUFJO0FBQ0EsY0FBSSxFQUFFLE1BQU1pQyxlQUFlLEVBQXZCLENBQUosRUFBZ0M7QUFDbkMsU0FGRCxDQUVFLE9BQU80RCxDQUFQLEVBQVU7QUFDUmhDLFVBQUFBLE9BQU8sQ0FBQ0UsS0FBUixDQUFjLHNDQUFkLEVBQXNEOEIsQ0FBdEQ7QUFDSDs7QUFDRCxjQUFNOEUsaUJBQWlCLENBQUMxUCxNQUFELEVBQVMrRSxNQUFULEVBQWlCNkMsVUFBakIsRUFBNkJpRCxlQUE3QixDQUF2QjtBQUNBO0FBQ0g7O0FBRUQsWUFBTWdGLE9BQU8sR0FBR2hGLGVBQWUsQ0FBQzVDLFVBQWhCLEdBQTZCMEgsS0FBN0IsQ0FBbUNDLFFBQW5DLENBQWhCOztBQUNBLFVBQUk3RSxRQUFRLENBQUM4RSxPQUFELENBQVIsS0FBc0I5RSxRQUFRLENBQUNuRCxVQUFELENBQWxDLEVBQWdEO0FBQzVDLGNBQU07QUFBQ1YsVUFBQUE7QUFBRCxZQUFhckMsZUFBTUMsbUJBQU4sQ0FBMEIsMEJBQTFCLEVBQXNELEVBQXRELEVBQTBEbUMsY0FBMUQsRUFBMEU7QUFDekZOLFVBQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFILENBRGtGO0FBRXpGQyxVQUFBQSxXQUFXLEVBQ1AsMENBQ00seUJBQUcsNEVBQ0QsMkNBREYsQ0FETixFQUVzRCx3Q0FGdEQsRUFHTSx5QkFBRyxlQUFILENBSE4sQ0FIcUY7QUFRekZPLFVBQUFBLE1BQU0sRUFBRSx5QkFBRyxVQUFIO0FBUmlGLFNBQTFFLENBQW5COztBQVdBLGNBQU0sQ0FBQ0MsU0FBRCxJQUFjLE1BQU1GLFFBQTFCO0FBQ0EsWUFBSSxDQUFDRSxTQUFMLEVBQWdCO0FBQ25COztBQUNELFlBQU1zSSxpQkFBaUIsQ0FBQzFQLE1BQUQsRUFBUytFLE1BQVQsRUFBaUI2QyxVQUFqQixFQUE2QmlELGVBQTdCLENBQXZCO0FBQ0gsS0FuREQsU0FtRFU7QUFDTm9FLE1BQUFBLFVBQVU7QUFDYjtBQUNKLEdBeEV3QixFQXdFdEIsQ0FBQ3JGLElBQUksQ0FBQzVKLE1BQU4sRUFBYzRKLElBQUksQ0FBQ3RMLE1BQW5CLEVBQTJCRCxHQUEzQixFQUFnQytRLGtCQUFoQyxFQUFvREUsT0FBcEQsRUFBNkRILGFBQTdELEVBQTRFRixVQUE1RSxFQUF3RmhQLElBQXhGLENBeEVzQixDQUF6QjtBQTBFQSxRQUFNNEssZUFBZSxHQUFHNUssSUFBSSxDQUFDeUIsWUFBTCxDQUFrQnNHLGNBQWxCLENBQWlDLHFCQUFqQyxFQUF3RCxFQUF4RCxDQUF4QjtBQUNBLFFBQU0wRyxzQkFBc0IsR0FBRzdELGVBQWUsR0FBR0EsZUFBZSxDQUFDNUMsVUFBaEIsR0FBNkIwRyxhQUFoQyxHQUFnRCxDQUE5RjtBQUNBLFFBQU1GLFVBQVUsR0FBR3RMLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBbkI7QUFDQSxRQUFNRixPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxRQUFNME0sZUFBZSxHQUFHWixVQUFVLEdBQUcsNkJBQUMsT0FBRDtBQUFTLElBQUEsQ0FBQyxFQUFFLEVBQVo7QUFBZ0IsSUFBQSxDQUFDLEVBQUU7QUFBbkIsSUFBSCxHQUM5Qiw2QkFBQyxVQUFEO0FBQVksSUFBQSxJQUFJLEVBQUMsT0FBakI7QUFBeUIsSUFBQSxPQUFPLEVBQUVPO0FBQWxDLElBREo7QUFHQSxRQUFNTSxhQUFhLEdBQUc1TSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBQ0EsU0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSSw2QkFBQyxhQUFEO0FBQ0ksSUFBQSxLQUFLLEVBQUUsSUFEWDtBQUVJLElBQUEsS0FBSyxFQUFFZ00sa0JBRlg7QUFHSSxJQUFBLFFBQVEsRUFBRXJCLGVBQWUsQ0FBQ0UsY0FIOUI7QUFJSSxJQUFBLFlBQVksRUFBRVMsc0JBSmxCO0FBS0ksSUFBQSxRQUFRLEVBQUVjLGFBTGQ7QUFNSSxJQUFBLFFBQVEsRUFBRU47QUFOZCxJQURKLEVBU0tZLGVBVEwsQ0FESjtBQWFILENBMUdEOztBQTRHTyxNQUFNRSxVQUFVLEdBQUkxUixNQUFELElBQVk7QUFDbEMsUUFBTUQsR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWixDQURrQyxDQUdsQzs7QUFDQSxRQUFNLENBQUM3RSxPQUFELEVBQVUwUyxVQUFWLElBQXdCLHFCQUFTM08sU0FBVCxDQUE5QixDQUprQyxDQUtsQzs7QUFDQSx3QkFBVSxNQUFNO0FBQ1oyTyxJQUFBQSxVQUFVLENBQUMzTyxTQUFELENBQVY7QUFFQSxRQUFJNE8sU0FBUyxHQUFHLEtBQWhCOztBQUVBLG1CQUFlQyxtQkFBZixHQUFxQztBQUNqQyxVQUFJO0FBQ0EsY0FBTTlSLEdBQUcsQ0FBQ3VDLFlBQUosQ0FBaUIsQ0FBQ3RDLE1BQUQsQ0FBakIsRUFBMkIsSUFBM0IsQ0FBTjtBQUNBLGNBQU1mLE9BQU8sR0FBRyxNQUFNYyxHQUFHLENBQUMrUix1QkFBSixDQUE0QjlSLE1BQTVCLENBQXRCOztBQUVBLFlBQUk0UixTQUFKLEVBQWU7QUFDWDtBQUNBO0FBQ0g7O0FBRUQ1UyxRQUFBQSxvQkFBb0IsQ0FBQ0MsT0FBRCxDQUFwQjs7QUFDQTBTLFFBQUFBLFVBQVUsQ0FBQzFTLE9BQUQsQ0FBVjtBQUNILE9BWEQsQ0FXRSxPQUFPa0osR0FBUCxFQUFZO0FBQ1Z3SixRQUFBQSxVQUFVLENBQUMsSUFBRCxDQUFWO0FBQ0g7QUFDSjs7QUFDREUsSUFBQUEsbUJBQW1CLEdBckJQLENBdUJaOzs7QUFDQSxXQUFPLE1BQU07QUFDVEQsTUFBQUEsU0FBUyxHQUFHLElBQVo7QUFDSCxLQUZEO0FBR0gsR0EzQkQsRUEyQkcsQ0FBQzdSLEdBQUQsRUFBTUMsTUFBTixDQTNCSCxFQU5rQyxDQW1DbEM7O0FBQ0Esd0JBQVUsTUFBTTtBQUNaLFFBQUkrUixNQUFNLEdBQUcsS0FBYjs7QUFDQSxVQUFNQyxhQUFhLEdBQUcsWUFBWTtBQUM5QixZQUFNQyxVQUFVLEdBQUcsTUFBTWxTLEdBQUcsQ0FBQytSLHVCQUFKLENBQTRCOVIsTUFBNUIsQ0FBekI7QUFDQSxVQUFJK1IsTUFBSixFQUFZO0FBQ1pKLE1BQUFBLFVBQVUsQ0FBQ00sVUFBRCxDQUFWO0FBQ0gsS0FKRDs7QUFLQSxVQUFNQyxnQkFBZ0IsR0FBSWIsS0FBRCxJQUFXO0FBQ2hDLFVBQUksQ0FBQ0EsS0FBSyxDQUFDYyxRQUFOLENBQWVuUyxNQUFmLENBQUwsRUFBNkI7QUFDN0JnUyxNQUFBQSxhQUFhO0FBQ2hCLEtBSEQ7O0FBSUEsVUFBTUksMkJBQTJCLEdBQUcsQ0FBQ0MsT0FBRCxFQUFVaFMsTUFBVixLQUFxQjtBQUNyRCxVQUFJZ1MsT0FBTyxLQUFLclMsTUFBaEIsRUFBd0I7QUFDeEJnUyxNQUFBQSxhQUFhO0FBQ2hCLEtBSEQ7O0FBSUEsVUFBTU0sd0JBQXdCLEdBQUcsQ0FBQ0QsT0FBRCxFQUFVRSxXQUFWLEtBQTBCO0FBQ3ZELFVBQUlGLE9BQU8sS0FBS3JTLE1BQWhCLEVBQXdCO0FBQ3hCZ1MsTUFBQUEsYUFBYTtBQUNoQixLQUhEOztBQUlBalMsSUFBQUEsR0FBRyxDQUFDeVMsRUFBSixDQUFPLHVCQUFQLEVBQWdDTixnQkFBaEM7QUFDQW5TLElBQUFBLEdBQUcsQ0FBQ3lTLEVBQUosQ0FBTywyQkFBUCxFQUFvQ0osMkJBQXBDO0FBQ0FyUyxJQUFBQSxHQUFHLENBQUN5UyxFQUFKLENBQU8sd0JBQVAsRUFBaUNGLHdCQUFqQyxFQXJCWSxDQXNCWjs7QUFDQSxXQUFPLE1BQU07QUFDVFAsTUFBQUEsTUFBTSxHQUFHLElBQVQ7QUFDQWhTLE1BQUFBLEdBQUcsQ0FBQzBTLGNBQUosQ0FBbUIsdUJBQW5CLEVBQTRDUCxnQkFBNUM7QUFDQW5TLE1BQUFBLEdBQUcsQ0FBQzBTLGNBQUosQ0FBbUIsMkJBQW5CLEVBQWdETCwyQkFBaEQ7QUFDQXJTLE1BQUFBLEdBQUcsQ0FBQzBTLGNBQUosQ0FBbUIsd0JBQW5CLEVBQTZDSCx3QkFBN0M7QUFDSCxLQUxEO0FBTUgsR0E3QkQsRUE2QkcsQ0FBQ3ZTLEdBQUQsRUFBTUMsTUFBTixDQTdCSDtBQStCQSxTQUFPZixPQUFQO0FBQ0gsQ0FwRU07Ozs7QUFzRVAsTUFBTXlULGFBQWEsR0FBRyxDQUFDO0FBQUMvUSxFQUFBQSxJQUFEO0FBQU8yQixFQUFBQSxNQUFQO0FBQWVtSyxFQUFBQSxPQUFmO0FBQXdCeE8sRUFBQUEsT0FBeEI7QUFBaUM4RCxFQUFBQTtBQUFqQyxDQUFELEtBQXVEO0FBQ3pFLFFBQU1oRCxHQUFHLEdBQUcsdUJBQVcrRCw0QkFBWCxDQUFaO0FBRUEsUUFBTTBGLFdBQVcsR0FBR0Qsa0JBQWtCLENBQUN4SixHQUFELEVBQU00QixJQUFOLENBQXRDLENBSHlFLENBSXpFOztBQUNBLFFBQU1nUixjQUFjLEdBQUd6RCxpQkFBaUIsQ0FBQ25QLEdBQUQsQ0FBeEMsQ0FMeUUsQ0FPekU7O0FBQ0EsUUFBTSxDQUFDZ0csU0FBRCxFQUFZNk0sWUFBWixJQUE0QixxQkFBUzdTLEdBQUcsQ0FBQzhTLGFBQUosQ0FBa0J2UCxNQUFNLENBQUN0RCxNQUF6QixDQUFULENBQWxDLENBUnlFLENBU3pFOztBQUNBLHdCQUFVLE1BQU07QUFDWjRTLElBQUFBLFlBQVksQ0FBQzdTLEdBQUcsQ0FBQzhTLGFBQUosQ0FBa0J2UCxNQUFNLENBQUN0RCxNQUF6QixDQUFELENBQVo7QUFDSCxHQUZELEVBRUcsQ0FBQ0QsR0FBRCxFQUFNdUQsTUFBTSxDQUFDdEQsTUFBYixDQUZILEVBVnlFLENBYXpFOztBQUNBLFFBQU04UyxrQkFBa0IsR0FBRyx3QkFBYUMsRUFBRCxJQUFRO0FBQzNDLFFBQUlBLEVBQUUsQ0FBQzVQLE9BQUgsT0FBaUIscUJBQXJCLEVBQTRDO0FBQ3hDeVAsTUFBQUEsWUFBWSxDQUFDN1MsR0FBRyxDQUFDOFMsYUFBSixDQUFrQnZQLE1BQU0sQ0FBQ3RELE1BQXpCLENBQUQsQ0FBWjtBQUNIO0FBQ0osR0FKMEIsRUFJeEIsQ0FBQ0QsR0FBRCxFQUFNdUQsTUFBTSxDQUFDdEQsTUFBYixDQUp3QixDQUEzQjtBQUtBLHdDQUFnQkQsR0FBaEIsRUFBcUIsYUFBckIsRUFBb0MrUyxrQkFBcEMsRUFuQnlFLENBcUJ6RTs7QUFDQSxRQUFNLENBQUNFLGtCQUFELEVBQXFCQyxxQkFBckIsSUFBOEMscUJBQVMsQ0FBVCxDQUFwRDtBQUNBLFFBQU1wSixhQUFhLEdBQUcsd0JBQVksTUFBTTtBQUNwQ29KLElBQUFBLHFCQUFxQixDQUFDRCxrQkFBa0IsR0FBRyxDQUF0QixDQUFyQjtBQUNILEdBRnFCLEVBRW5CLENBQUNBLGtCQUFELENBRm1CLENBQXRCO0FBR0EsUUFBTWxKLFlBQVksR0FBRyx3QkFBWSxNQUFNO0FBQ25DbUosSUFBQUEscUJBQXFCLENBQUNELGtCQUFrQixHQUFHLENBQXRCLENBQXJCO0FBQ0gsR0FGb0IsRUFFbEIsQ0FBQ0Esa0JBQUQsQ0FGa0IsQ0FBckI7QUFJQSxRQUFNdkQsZUFBZSxHQUFHRCxrQkFBa0IsQ0FBQ3pQLEdBQUQsRUFBTTRCLElBQU4sRUFBWTJCLE1BQVosQ0FBMUM7QUFFQSxRQUFNNFAsbUJBQW1CLEdBQUcsd0JBQVksWUFBWTtBQUNoRCxVQUFNdkssY0FBYyxHQUFHOUQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF2Qjs7QUFDQSxVQUFNO0FBQUM4RCxNQUFBQTtBQUFELFFBQWFyQyxlQUFNQyxtQkFBTixDQUEwQiwyQkFBMUIsRUFBdUQsRUFBdkQsRUFBMkRtQyxjQUEzRCxFQUEyRTtBQUMxRk4sTUFBQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFILENBRG1GO0FBRTFGQyxNQUFBQSxXQUFXLEVBQ1AsMENBQU8seUJBQ0gsbUdBQ0EsOEZBREEsR0FFQSwrQkFIRyxDQUFQLENBSHNGO0FBUTFGTyxNQUFBQSxNQUFNLEVBQUUseUJBQUcsaUJBQUgsQ0FSa0Y7QUFTMUZxQixNQUFBQSxNQUFNLEVBQUU7QUFUa0YsS0FBM0UsQ0FBbkI7O0FBWUEsVUFBTSxDQUFDaUosUUFBRCxJQUFhLE1BQU12SyxRQUF6QjtBQUNBLFFBQUksQ0FBQ3VLLFFBQUwsRUFBZTs7QUFDZixRQUFJO0FBQ0EsWUFBTXBULEdBQUcsQ0FBQ3FULHFCQUFKLENBQTBCOVAsTUFBTSxDQUFDdEQsTUFBakMsQ0FBTjtBQUNILEtBRkQsQ0FFRSxPQUFPbUksR0FBUCxFQUFZO0FBQ1ZtQyxNQUFBQSxPQUFPLENBQUNFLEtBQVIsQ0FBYywyQkFBZDtBQUNBRixNQUFBQSxPQUFPLENBQUNFLEtBQVIsQ0FBY3JDLEdBQWQ7QUFFQSxZQUFNQyxXQUFXLEdBQUd2RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBeUIscUJBQU1DLG1CQUFOLENBQTBCLG1DQUExQixFQUErRCxFQUEvRCxFQUFtRTRCLFdBQW5FLEVBQWdGO0FBQzVFQyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsMkJBQUgsQ0FEcUU7QUFFNUVDLFFBQUFBLFdBQVcsRUFBSUgsR0FBRyxJQUFJQSxHQUFHLENBQUNJLE9BQVosR0FBdUJKLEdBQUcsQ0FBQ0ksT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGeUIsT0FBaEY7QUFJSDtBQUNKLEdBNUIyQixFQTRCekIsQ0FBQ3hJLEdBQUQsRUFBTXVELE1BQU0sQ0FBQ3RELE1BQWIsQ0E1QnlCLENBQTVCO0FBOEJBLE1BQUlxVCx1QkFBSjtBQUNBLE1BQUlDLE9BQUosQ0EvRHlFLENBaUV6RTtBQUNBO0FBQ0E7O0FBQ0EsTUFBSVgsY0FBYyxJQUFJclAsTUFBTSxDQUFDdEQsTUFBUCxDQUFjdVQsUUFBZCxZQUEyQkMsaUNBQWdCQyxpQkFBaEIsRUFBM0IsRUFBdEIsRUFBeUY7QUFDckZKLElBQUFBLHVCQUF1QixHQUNuQiw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLE9BQU8sRUFBRUgsbUJBQTNCO0FBQWdELE1BQUEsU0FBUyxFQUFDO0FBQTFELE9BQ0sseUJBQUcsaUJBQUgsQ0FETCxDQURKO0FBS0g7O0FBRUQsTUFBSVEsbUJBQUo7O0FBQ0EsTUFBSS9SLElBQUksSUFBSTJCLE1BQU0sQ0FBQzVCLE1BQW5CLEVBQTJCO0FBQ3ZCZ1MsSUFBQUEsbUJBQW1CLEdBQ2YsNkJBQUMsdUJBQUQ7QUFDSSxNQUFBLFdBQVcsRUFBRWxLLFdBRGpCO0FBRUksTUFBQSxNQUFNLEVBQUVsRyxNQUZaO0FBR0ksTUFBQSxJQUFJLEVBQUUzQixJQUhWO0FBSUksTUFBQSxhQUFhLEVBQUVrSSxhQUpuQjtBQUtJLE1BQUEsWUFBWSxFQUFFQztBQUxsQixPQU1NdUosdUJBTk4sQ0FESjtBQVVILEdBWEQsTUFXTyxJQUFJNUYsT0FBSixFQUFhO0FBQ2hCaUcsSUFBQUEsbUJBQW1CLEdBQ2YsNkJBQUMsc0JBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBRWpHLE9BRGI7QUFFSSxNQUFBLFdBQVcsRUFBRW5LLE1BRmpCO0FBR0ksTUFBQSxhQUFhLEVBQUV1RyxhQUhuQjtBQUlJLE1BQUEsWUFBWSxFQUFFQztBQUpsQixPQUtNdUosdUJBTE4sQ0FESjtBQVNILEdBVk0sTUFVQSxJQUFJQSx1QkFBSixFQUE2QjtBQUNoQ0ssSUFBQUEsbUJBQW1CLEdBQ2YsNkJBQUMsMEJBQUQsUUFDTUwsdUJBRE4sQ0FESjtBQUtIOztBQUVELE1BQUlMLGtCQUFrQixHQUFHLENBQXpCLEVBQTRCO0FBQ3hCLFVBQU1XLE1BQU0sR0FBRzlPLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjtBQUNBd08sSUFBQUEsT0FBTyxHQUFHLDZCQUFDLE1BQUQ7QUFBUSxNQUFBLFlBQVksRUFBQztBQUFyQixNQUFWO0FBQ0g7O0FBRUQsUUFBTU0sYUFBYSxHQUNmLDZCQUFDLGlCQUFEO0FBQ0ksSUFBQSxXQUFXLEVBQUVwSyxXQURqQjtBQUVJLElBQUEsSUFBSSxFQUFFbEcsTUFGVjtBQUdJLElBQUEsSUFBSSxFQUFFM0IsSUFIVjtBQUlJLElBQUEsZUFBZSxFQUFFOE47QUFKckIsSUFESixDQS9HeUUsQ0F3SHpFOzs7QUFDQSxRQUFNb0UsY0FBYyxHQUFHOVQsR0FBRyxDQUFDK1QsZUFBSixFQUF2Qjs7QUFFQSxNQUFJQyxJQUFKOztBQUNBLE1BQUksQ0FBQ2hSLGVBQUwsRUFBc0I7QUFDbEIsUUFBSSxDQUFDOFEsY0FBTCxFQUFxQjtBQUNqQkUsTUFBQUEsSUFBSSxHQUFHLHlCQUFHLHFEQUFILENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSXBTLElBQUosRUFBVTtBQUNib1MsTUFBQUEsSUFBSSxHQUFHLHlCQUFHLHFEQUFILENBQVA7QUFDSCxLQUZNLE1BRUEsQ0FDSDtBQUNIO0FBQ0osR0FSRCxNQVFPO0FBQ0hBLElBQUFBLElBQUksR0FBRyx5QkFBRyxpREFBSCxDQUFQO0FBQ0g7O0FBRUQsTUFBSUMsWUFBSjtBQUNBLFFBQU1DLDhCQUE4QixHQUFHM0UsaUNBQWlDLENBQUN2UCxHQUFELENBQXhFO0FBRUEsUUFBTVUsU0FBUyxHQUFHVixHQUFHLENBQUNXLGNBQUosQ0FBbUI0QyxNQUFNLENBQUN0RCxNQUExQixDQUFsQjtBQUNBLFFBQU1rVSxZQUFZLEdBQUd6VCxTQUFTLENBQUNFLHNCQUFWLEVBQXJCO0FBQ0EsUUFBTUosSUFBSSxHQUFHK0MsTUFBTSxDQUFDdEQsTUFBUCxLQUFrQkQsR0FBRyxDQUFDUyxTQUFKLEVBQS9CO0FBQ0EsUUFBTStDLFNBQVMsR0FBR3RELHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsS0FDRStULDhCQURGLElBQ29DLENBQUNDLFlBRHJDLElBQ3FELENBQUMzVCxJQUR4RTs7QUFHQSxRQUFNaUQsV0FBVyxHQUFJMlEsUUFBRCxJQUFjO0FBQzlCbEIsSUFBQUEscUJBQXFCLENBQUN4TixLQUFLLElBQUlBLEtBQUssSUFBSTBPLFFBQVEsR0FBRyxDQUFILEdBQU8sQ0FBQyxDQUFwQixDQUFmLENBQXJCO0FBQ0gsR0FGRDs7QUFHQSxRQUFNQyxtQkFBbUIsR0FDckIvUSxzQkFBc0IsQ0FBQ3RELEdBQUQsRUFBTXVELE1BQU4sRUFBY0MsU0FBZCxFQUF5QkMsV0FBekIsQ0FEMUI7O0FBR0EsTUFBSUQsU0FBSixFQUFlO0FBQ1h5USxJQUFBQSxZQUFZLEdBQ1IsNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsbUJBQTVCO0FBQWdELE1BQUEsT0FBTyxFQUFFLE1BQU07QUFDM0QsWUFBSUksbUJBQUosRUFBeUI7QUFDckIsd0NBQVc5USxNQUFYO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsOENBQWlCQSxNQUFqQjtBQUNIO0FBQ0o7QUFORCxPQU9LLHlCQUFHLFFBQUgsQ0FQTCxDQURKO0FBV0g7O0FBRUQsUUFBTStRLGVBQWUsR0FDakI7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0kseUNBQU0seUJBQUcsVUFBSCxDQUFOLENBREosRUFFSSx3Q0FBS04sSUFBTCxDQUZKLEVBR01DLFlBSE4sRUFJSSw2QkFBQyxjQUFEO0FBQ0ksSUFBQSxPQUFPLEVBQUUvVSxPQUFPLEtBQUsrRCxTQUR6QjtBQUVJLElBQUEsT0FBTyxFQUFFL0QsT0FGYjtBQUdJLElBQUEsTUFBTSxFQUFFcUUsTUFBTSxDQUFDdEQ7QUFIbkIsSUFKSixDQURKOztBQVlBLFNBQU8sNkJBQUMsY0FBRCxDQUFPLFFBQVAsUUFDRDRULGFBQWEsSUFDZjtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDTUEsYUFETixDQURKLENBRkcsRUFRRFMsZUFSQyxFQVNILDZCQUFDLGtCQUFEO0FBQ0ksSUFBQSxPQUFPLEVBQUVwVixPQURiO0FBRUksSUFBQSxTQUFTLEVBQUV3USxlQUFlLENBQUN6SixTQUYvQjtBQUdJLElBQUEsU0FBUyxFQUFFRCxTQUhmO0FBSUksSUFBQSxNQUFNLEVBQUV6QztBQUpaLElBVEcsRUFlRG9RLG1CQWZDLEVBaUJESixPQWpCQyxDQUFQO0FBbUJILENBcE1EOztBQXNNQSxNQUFNZ0IsY0FBYyxHQUFHLENBQUM7QUFBQ0MsRUFBQUEsT0FBRDtBQUFValIsRUFBQUEsTUFBVjtBQUFrQmtSLEVBQUFBO0FBQWxCLENBQUQsS0FBa0M7QUFDckQsUUFBTXpVLEdBQUcsR0FBRyx1QkFBVytELDRCQUFYLENBQVo7QUFFQSxNQUFJMlEsV0FBSjs7QUFDQSxNQUFJRixPQUFKLEVBQWE7QUFDVEUsSUFBQUEsV0FBVyxHQUFHLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLG9CQUE1QjtBQUFpRCxNQUFBLE9BQU8sRUFBRUYsT0FBMUQ7QUFBbUUsTUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSDtBQUExRSxPQUNWLHlDQURVLENBQWQ7QUFHSDs7QUFFRCxRQUFNRyxtQkFBbUIsR0FBRyx3QkFBWSxNQUFNO0FBQzFDLFVBQU16RixTQUFTLEdBQUczTCxNQUFNLENBQUNxUixlQUFQLEdBQXlCclIsTUFBTSxDQUFDcVIsZUFBUCxFQUF6QixHQUFvRHJSLE1BQU0sQ0FBQzJMLFNBQTdFO0FBQ0EsUUFBSSxDQUFDQSxTQUFMLEVBQWdCO0FBRWhCLFVBQU0yRixPQUFPLEdBQUc3VSxHQUFHLENBQUM4VSxZQUFKLENBQWlCNUYsU0FBakIsQ0FBaEI7QUFDQSxVQUFNNkYsU0FBUyxHQUFHalEsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFsQjtBQUNBLFVBQU1pUSxNQUFNLEdBQUc7QUFDWEMsTUFBQUEsR0FBRyxFQUFFSixPQURNO0FBRVhyVixNQUFBQSxJQUFJLEVBQUUrRCxNQUFNLENBQUMvRDtBQUZGLEtBQWY7O0FBS0FnSCxtQkFBTWlJLFlBQU4sQ0FBbUJzRyxTQUFuQixFQUE4QkMsTUFBOUIsRUFBc0Msb0JBQXRDO0FBQ0gsR0FaMkIsRUFZekIsQ0FBQ2hWLEdBQUQsRUFBTXVELE1BQU4sQ0FaeUIsQ0FBNUI7QUFjQSxRQUFNMlIsWUFBWSxHQUFHcFEsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFyQjs7QUFDQSxRQUFNb1EsYUFBYSxHQUNmO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNJLDBDQUNJLDBDQUNJLDZCQUFDLFlBQUQ7QUFDSSxJQUFBLEdBQUcsRUFBRTVSLE1BQU0sQ0FBQ3RELE1BRGhCLENBQ3dCO0FBRHhCO0FBRUksSUFBQSxNQUFNLEVBQUVzRCxNQUZaO0FBR0ksSUFBQSxLQUFLLEVBQUUsSUFBSSxHQUFKLEdBQVU2UixNQUFNLENBQUNDLFdBSDVCLENBR3lDO0FBSHpDO0FBSUksSUFBQSxNQUFNLEVBQUUsSUFBSSxHQUFKLEdBQVVELE1BQU0sQ0FBQ0MsV0FKN0IsQ0FJMEM7QUFKMUM7QUFLSSxJQUFBLFlBQVksRUFBQyxPQUxqQjtBQU1JLElBQUEsY0FBYyxFQUFFOVIsTUFBTSxDQUFDdEQsTUFOM0I7QUFPSSxJQUFBLE9BQU8sRUFBRTBVLG1CQVBiO0FBUUksSUFBQSxJQUFJLEVBQUVwUixNQUFNLENBQUMyTCxTQUFQLEdBQW1CLENBQUMzTCxNQUFNLENBQUMyTCxTQUFSLENBQW5CLEdBQXdDak07QUFSbEQsSUFESixDQURKLENBREosQ0FESjs7QUFrQkEsTUFBSXFTLGFBQUo7QUFDQSxNQUFJQyxxQkFBSjtBQUNBLE1BQUlDLHVCQUFKO0FBQ0EsTUFBSUMsYUFBSjs7QUFFQSxNQUFJbFMsTUFBTSxZQUFZbVMsdUJBQWxCLElBQWdDblMsTUFBTSxDQUFDZ0ksSUFBM0MsRUFBaUQ7QUFDN0MrSixJQUFBQSxhQUFhLEdBQUcvUixNQUFNLENBQUNnSSxJQUFQLENBQVlvSyxRQUE1QjtBQUNBSixJQUFBQSxxQkFBcUIsR0FBR2hTLE1BQU0sQ0FBQ2dJLElBQVAsQ0FBWXFLLGFBQXBDO0FBQ0FKLElBQUFBLHVCQUF1QixHQUFHalMsTUFBTSxDQUFDZ0ksSUFBUCxDQUFZc0ssZUFBdEM7O0FBRUEsUUFBSTNWLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6RHNWLE1BQUFBLGFBQWEsR0FBR2xTLE1BQU0sQ0FBQ2dJLElBQVAsQ0FBWXVLLHVCQUE1QjtBQUNIO0FBQ0o7O0FBRUQsUUFBTUMscUJBQXFCLEdBQUdDLG1CQUFVQyxHQUFWLEdBQWdCLDJCQUFoQixDQUE5Qjs7QUFDQSxNQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBQ0EsTUFBSUgscUJBQXFCLElBQUlBLHFCQUFxQixDQUFDL1YsR0FBRyxDQUFDbVcsT0FBTCxDQUFyQixLQUF1Q2xULFNBQXBFLEVBQStFO0FBQzNFaVQsSUFBQUEsWUFBWSxHQUFHSCxxQkFBcUIsQ0FBQy9WLEdBQUcsQ0FBQ21XLE9BQUwsQ0FBcEM7QUFDSDs7QUFFRCxNQUFJQyxhQUFhLEdBQUcsSUFBcEI7O0FBQ0EsTUFBSUYsWUFBSixFQUFrQjtBQUNkLFVBQU1HLGFBQWEsR0FBR3ZSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBdEI7QUFDQXFSLElBQUFBLGFBQWEsR0FBRyw2QkFBQyxhQUFEO0FBQWUsTUFBQSxTQUFTLEVBQUViLHFCQUExQjtBQUNlLE1BQUEsZUFBZSxFQUFFQyx1QkFEaEM7QUFFZSxNQUFBLGFBQWEsRUFBRUY7QUFGOUIsTUFBaEI7QUFHSDs7QUFFRCxNQUFJZ0IsV0FBVyxHQUFHLElBQWxCOztBQUNBLE1BQUliLGFBQUosRUFBbUI7QUFDZmEsSUFBQUEsV0FBVyxHQUFHO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBOENiLGFBQTlDLENBQWQ7QUFDSDs7QUFFRCxNQUFJYyxPQUFKOztBQUNBLE1BQUk5QixTQUFKLEVBQWU7QUFDWDhCLElBQUFBLE9BQU8sR0FBRyw2QkFBQyxnQkFBRDtBQUFTLE1BQUEsSUFBSSxFQUFFLEVBQWY7QUFBbUIsTUFBQSxNQUFNLEVBQUU5QixTQUEzQjtBQUFzQyxNQUFBLE1BQU0sRUFBRTtBQUE5QyxNQUFWO0FBQ0g7O0FBRUQsUUFBTStCLFdBQVcsR0FBR2pULE1BQU0sQ0FBQy9ELElBQVAsSUFBZStELE1BQU0sQ0FBQzBMLFdBQTFDO0FBQ0EsU0FBTyw2QkFBQyxjQUFELENBQU8sUUFBUCxRQUNEeUYsV0FEQyxFQUVEUyxhQUZDLEVBSUg7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0k7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0ksMENBQ0kseUNBQ01vQixPQUROLEVBRUk7QUFBTSxJQUFBLEtBQUssRUFBRUMsV0FBYjtBQUEwQixrQkFBWUE7QUFBdEMsS0FDTUEsV0FETixDQUZKLENBREosQ0FESixFQVNJLDBDQUFPalQsTUFBTSxDQUFDdEQsTUFBZCxDQVRKLEVBVUk7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0ttVyxhQURMLEVBRUtFLFdBRkwsQ0FWSixDQURKLENBSkcsQ0FBUDtBQXNCSCxDQXpHRDs7QUEyR0EsTUFBTUcsUUFBUSxHQUFHLFVBQXlGO0FBQUEsTUFBeEY7QUFBQ2xMLElBQUFBLElBQUQ7QUFBT21DLElBQUFBLE9BQVA7QUFBZ0IvTCxJQUFBQSxNQUFoQjtBQUF3QjZTLElBQUFBLE9BQXhCO0FBQWlDa0MsSUFBQUEsS0FBSyxHQUFDQywwQ0FBbUJDO0FBQTFELEdBQXdGO0FBQUEsTUFBWEMsS0FBVztBQUN0RyxRQUFNN1csR0FBRyxHQUFHLHVCQUFXK0QsNEJBQVgsQ0FBWixDQURzRyxDQUd0Rzs7QUFDQSxRQUFNbkMsSUFBSSxHQUFHLG9CQUFRLE1BQU1ELE1BQU0sR0FBRzNCLEdBQUcsQ0FBQzZCLE9BQUosQ0FBWUYsTUFBWixDQUFILEdBQXlCLElBQTdDLEVBQW1ELENBQUMzQixHQUFELEVBQU0yQixNQUFOLENBQW5ELENBQWIsQ0FKc0csQ0FLdEc7O0FBQ0EsUUFBTTRCLE1BQU0sR0FBRyxvQkFBUSxNQUFNM0IsSUFBSSxHQUFJQSxJQUFJLENBQUMwTCxTQUFMLENBQWUvQixJQUFJLENBQUN0TCxNQUFwQixLQUErQnNMLElBQW5DLEdBQTJDQSxJQUE3RCxFQUFtRSxDQUFDM0osSUFBRCxFQUFPMkosSUFBUCxDQUFuRSxDQUFmO0FBRUEsUUFBTXZJLGVBQWUsR0FBR0gsY0FBYyxDQUFDN0MsR0FBRCxFQUFNNEIsSUFBTixDQUF0QztBQUNBLFFBQU0xQyxPQUFPLEdBQUd5UyxVQUFVLENBQUNwRyxJQUFJLENBQUN0TCxNQUFOLENBQTFCO0FBRUEsTUFBSXdVLFNBQUo7O0FBQ0EsTUFBSXpSLGVBQWUsSUFBSTlELE9BQXZCLEVBQWdDO0FBQzVCdVYsSUFBQUEsU0FBUyxHQUFHMVUsWUFBWSxDQUFDQyxHQUFELEVBQU11TCxJQUFJLENBQUN0TCxNQUFYLEVBQW1CZixPQUFuQixDQUF4QjtBQUNIOztBQUVELFFBQU04RSxPQUFPLEdBQUcsQ0FBQyxhQUFELENBQWhCO0FBRUEsTUFBSThTLE9BQUo7O0FBQ0EsVUFBUUosS0FBUjtBQUNJLFNBQUtDLDBDQUFtQkMsY0FBeEI7QUFDQSxTQUFLRCwwQ0FBbUJJLGVBQXhCO0FBQ0lELE1BQUFBLE9BQU8sR0FDSCw2QkFBQyxhQUFEO0FBQ0ksUUFBQSxJQUFJLEVBQUVsVixJQURWO0FBRUksUUFBQSxNQUFNLEVBQUUyQixNQUZaO0FBR0ksUUFBQSxPQUFPLEVBQUVtSyxPQUhiO0FBSUksUUFBQSxPQUFPLEVBQUV4TyxPQUpiO0FBS0ksUUFBQSxlQUFlLEVBQUU4RDtBQUxyQixRQURKO0FBUUE7O0FBQ0osU0FBSzJULDBDQUFtQkssZUFBeEI7QUFDSWhULE1BQUFBLE9BQU8sQ0FBQ3JFLElBQVIsQ0FBYSx5QkFBYjtBQUNBbVgsTUFBQUEsT0FBTyxHQUNILDZCQUFDLHdCQUFELDZCQUFxQkQsS0FBckI7QUFBNEIsUUFBQSxNQUFNLEVBQUV0VCxNQUFwQztBQUE0QyxRQUFBLE9BQU8sRUFBRWlSLE9BQXJEO0FBQThELFFBQUEsZUFBZSxFQUFFeFI7QUFBL0UsU0FESjtBQUdBO0FBakJSOztBQW9CQSxTQUNJO0FBQUssSUFBQSxTQUFTLEVBQUVnQixPQUFPLENBQUNpVCxJQUFSLENBQWEsR0FBYixDQUFoQjtBQUFtQyxJQUFBLElBQUksRUFBQztBQUF4QyxLQUNJLDZCQUFDLDBCQUFEO0FBQW1CLElBQUEsU0FBUyxFQUFDO0FBQTdCLEtBQ0ksNkJBQUMsY0FBRDtBQUFnQixJQUFBLE1BQU0sRUFBRTFULE1BQXhCO0FBQWdDLElBQUEsU0FBUyxFQUFFa1IsU0FBM0M7QUFBc0QsSUFBQSxPQUFPLEVBQUVEO0FBQS9ELElBREosRUFHTXNDLE9BSE4sQ0FESixDQURKO0FBU0gsQ0FoREQ7O0FBa0RBTCxRQUFRLENBQUNTLFNBQVQsR0FBcUI7QUFDakIzTCxFQUFBQSxJQUFJLEVBQUVzRCxtQkFBVXNJLFNBQVYsQ0FBb0IsQ0FDdEJ0SSxtQkFBVXVJLFVBQVYsQ0FBcUJDLGlCQUFyQixDQURzQixFQUV0QnhJLG1CQUFVdUksVUFBVixDQUFxQjFCLHVCQUFyQixDQUZzQixFQUd0QjlHLFdBSHNCLENBQXBCLEVBSUhJLFVBTGM7QUFNakJzSSxFQUFBQSxLQUFLLEVBQUV6SSxtQkFBVXVJLFVBQVYsQ0FBcUJHLGtCQUFyQixDQU5VO0FBT2pCN0osRUFBQUEsT0FBTyxFQUFFbUIsbUJBQVVFLE1BUEY7QUFRakJwTixFQUFBQSxNQUFNLEVBQUVrTixtQkFBVUUsTUFSRDtBQVVqQnlGLEVBQUFBLE9BQU8sRUFBRTNGLG1CQUFVMkk7QUFWRixDQUFyQjtlQWFlZixRIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7dXNlQ2FsbGJhY2ssIHVzZU1lbW8sIHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZUNvbnRleHR9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCB7R3JvdXAsIFJvb21NZW1iZXIsIFVzZXJ9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBjcmVhdGVSb29tIGZyb20gJy4uLy4uLy4uL2NyZWF0ZVJvb20nO1xyXG5pbXBvcnQgRE1Sb29tTWFwIGZyb20gJy4uLy4uLy4uL3V0aWxzL0RNUm9vbU1hcCc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b24nO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gJy4uLy4uLy4uL1Nka0NvbmZpZyc7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCB7RXZlbnRUaW1lbGluZX0gZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0IEF1dG9IaWRlU2Nyb2xsYmFyIGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL0F1dG9IaWRlU2Nyb2xsYmFyXCI7XHJcbmltcG9ydCBSb29tVmlld1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zdG9yZXMvUm9vbVZpZXdTdG9yZVwiO1xyXG5pbXBvcnQgTXVsdGlJbnZpdGVyIGZyb20gXCIuLi8uLi8uLi91dGlscy9NdWx0aUludml0ZXJcIjtcclxuaW1wb3J0IEdyb3VwU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3N0b3Jlcy9Hcm91cFN0b3JlXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBFMkVJY29uIGZyb20gXCIuLi9yb29tcy9FMkVJY29uXCI7XHJcbmltcG9ydCB7dXNlRXZlbnRFbWl0dGVyfSBmcm9tIFwiLi4vLi4vLi4vaG9va3MvdXNlRXZlbnRFbWl0dGVyXCI7XHJcbmltcG9ydCB7dGV4dHVhbFBvd2VyTGV2ZWx9IGZyb20gJy4uLy4uLy4uL1JvbGVzJztcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuaW1wb3J0IHtSSUdIVF9QQU5FTF9QSEFTRVN9IGZyb20gXCIuLi8uLi8uLi9zdG9yZXMvUmlnaHRQYW5lbFN0b3JlUGhhc2VzXCI7XHJcbmltcG9ydCBFbmNyeXB0aW9uUGFuZWwgZnJvbSBcIi4vRW5jcnlwdGlvblBhbmVsXCI7XHJcbmltcG9ydCB7IHVzZUFzeW5jTWVtbyB9IGZyb20gJy4uLy4uLy4uL2hvb2tzL3VzZUFzeW5jTWVtbyc7XHJcbmltcG9ydCB7IHZlcmlmeVVzZXIsIGxlZ2FjeVZlcmlmeVVzZXIsIHZlcmlmeURldmljZSB9IGZyb20gJy4uLy4uLy4uL3ZlcmlmaWNhdGlvbic7XHJcblxyXG5jb25zdCBfZGlzYW1iaWd1YXRlRGV2aWNlcyA9IChkZXZpY2VzKSA9PiB7XHJcbiAgICBjb25zdCBuYW1lcyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRldmljZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBjb25zdCBuYW1lID0gZGV2aWNlc1tpXS5nZXREaXNwbGF5TmFtZSgpO1xyXG4gICAgICAgIGNvbnN0IGluZGV4TGlzdCA9IG5hbWVzW25hbWVdIHx8IFtdO1xyXG4gICAgICAgIGluZGV4TGlzdC5wdXNoKGkpO1xyXG4gICAgICAgIG5hbWVzW25hbWVdID0gaW5kZXhMaXN0O1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBuYW1lIGluIG5hbWVzKSB7XHJcbiAgICAgICAgaWYgKG5hbWVzW25hbWVdLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgbmFtZXNbbmFtZV0uZm9yRWFjaCgoaik9PntcclxuICAgICAgICAgICAgICAgIGRldmljZXNbal0uYW1iaWd1b3VzID0gdHJ1ZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEUyRVN0YXR1cyA9IChjbGksIHVzZXJJZCwgZGV2aWNlcykgPT4ge1xyXG4gICAgaWYgKCFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICBjb25zdCBoYXNVbnZlcmlmaWVkRGV2aWNlID0gZGV2aWNlcy5zb21lKChkZXZpY2UpID0+IGRldmljZS5pc1VudmVyaWZpZWQoKSk7XHJcbiAgICAgICAgcmV0dXJuIGhhc1VudmVyaWZpZWREZXZpY2UgPyBcIndhcm5pbmdcIiA6IFwidmVyaWZpZWRcIjtcclxuICAgIH1cclxuICAgIGNvbnN0IGlzTWUgPSB1c2VySWQgPT09IGNsaS5nZXRVc2VySWQoKTtcclxuICAgIGNvbnN0IHVzZXJUcnVzdCA9IGNsaS5jaGVja1VzZXJUcnVzdCh1c2VySWQpO1xyXG4gICAgaWYgKCF1c2VyVHJ1c3QuaXNDcm9zc1NpZ25pbmdWZXJpZmllZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuIHVzZXJUcnVzdC53YXNDcm9zc1NpZ25pbmdWZXJpZmllZCgpID8gXCJ3YXJuaW5nXCIgOiBcIm5vcm1hbFwiO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGFueURldmljZVVudmVyaWZpZWQgPSBkZXZpY2VzLnNvbWUoZGV2aWNlID0+IHtcclxuICAgICAgICBjb25zdCB7IGRldmljZUlkIH0gPSBkZXZpY2U7XHJcbiAgICAgICAgLy8gRm9yIHlvdXIgb3duIGRldmljZXMsIHdlIHVzZSB0aGUgc3RyaWN0ZXIgY2hlY2sgb2YgY3Jvc3Mtc2lnbmluZ1xyXG4gICAgICAgIC8vIHZlcmlmaWNhdGlvbiB0byBlbmNvdXJhZ2UgZXZlcnlvbmUgdG8gdHJ1c3QgdGhlaXIgb3duIGRldmljZXMgdmlhXHJcbiAgICAgICAgLy8gY3Jvc3Mtc2lnbmluZyBzbyB0aGF0IG90aGVyIHVzZXJzIGNhbiB0aGVuIHNhZmVseSB0cnVzdCB5b3UuXHJcbiAgICAgICAgLy8gRm9yIG90aGVyIHBlb3BsZSdzIGRldmljZXMsIHRoZSBtb3JlIGdlbmVyYWwgdmVyaWZpZWQgY2hlY2sgdGhhdFxyXG4gICAgICAgIC8vIGluY2x1ZGVzIGxvY2FsbHkgdmVyaWZpZWQgZGV2aWNlcyBjYW4gYmUgdXNlZC5cclxuICAgICAgICBjb25zdCBkZXZpY2VUcnVzdCA9IGNsaS5jaGVja0RldmljZVRydXN0KHVzZXJJZCwgZGV2aWNlSWQpO1xyXG4gICAgICAgIHJldHVybiBpc01lID8gIWRldmljZVRydXN0LmlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQoKSA6ICFkZXZpY2VUcnVzdC5pc1ZlcmlmaWVkKCk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBhbnlEZXZpY2VVbnZlcmlmaWVkID8gXCJ3YXJuaW5nXCIgOiBcInZlcmlmaWVkXCI7XHJcbn07XHJcblxyXG5hc3luYyBmdW5jdGlvbiBvcGVuRE1Gb3JVc2VyKG1hdHJpeENsaWVudCwgdXNlcklkKSB7XHJcbiAgICBjb25zdCBkbVJvb21zID0gRE1Sb29tTWFwLnNoYXJlZCgpLmdldERNUm9vbXNGb3JVc2VySWQodXNlcklkKTtcclxuICAgIGNvbnN0IGxhc3RBY3RpdmVSb29tID0gZG1Sb29tcy5yZWR1Y2UoKGxhc3RBY3RpdmVSb29tLCByb29tSWQpID0+IHtcclxuICAgICAgICBjb25zdCByb29tID0gbWF0cml4Q2xpZW50LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20gfHwgcm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gXCJsZWF2ZVwiKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBsYXN0QWN0aXZlUm9vbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFsYXN0QWN0aXZlUm9vbSB8fCBsYXN0QWN0aXZlUm9vbS5nZXRMYXN0QWN0aXZlVGltZXN0YW1wKCkgPCByb29tLmdldExhc3RBY3RpdmVUaW1lc3RhbXAoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gcm9vbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxhc3RBY3RpdmVSb29tO1xyXG4gICAgfSwgbnVsbCk7XHJcblxyXG4gICAgaWYgKGxhc3RBY3RpdmVSb29tKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogbGFzdEFjdGl2ZVJvb20ucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBjcmVhdGVSb29tT3B0aW9ucyA9IHtcclxuICAgICAgICBkbVVzZXJJZDogdXNlcklkLFxyXG4gICAgfTtcclxuXHJcbiAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgLy8gQ2hlY2sgd2hldGhlciBhbGwgdXNlcnMgaGF2ZSB1cGxvYWRlZCBkZXZpY2Uga2V5cyBiZWZvcmUuXHJcbiAgICAgICAgLy8gSWYgc28sIGVuYWJsZSBlbmNyeXB0aW9uIGluIHRoZSBuZXcgcm9vbS5cclxuICAgICAgICBjb25zdCB1c2Vyc1RvRGV2aWNlc01hcCA9IGF3YWl0IG1hdHJpeENsaWVudC5kb3dubG9hZEtleXMoW3VzZXJJZF0pO1xyXG4gICAgICAgIGNvbnN0IGFsbEhhdmVEZXZpY2VLZXlzID0gT2JqZWN0LnZhbHVlcyh1c2Vyc1RvRGV2aWNlc01hcCkuZXZlcnkoZGV2aWNlcyA9PiB7XHJcbiAgICAgICAgICAgIC8vIGBkZXZpY2VzYCBpcyBhbiBvYmplY3Qgb2YgdGhlIGZvcm0geyBkZXZpY2VJZDogZGV2aWNlSW5mbywgLi4uIH0uXHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3Qua2V5cyhkZXZpY2VzKS5sZW5ndGggPiAwO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChhbGxIYXZlRGV2aWNlS2V5cykge1xyXG4gICAgICAgICAgICBjcmVhdGVSb29tT3B0aW9ucy5lbmNyeXB0aW9uID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlUm9vbShjcmVhdGVSb29tT3B0aW9ucyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHVzZUlzRW5jcnlwdGVkKGNsaSwgcm9vbSkge1xyXG4gICAgY29uc3QgW2lzRW5jcnlwdGVkLCBzZXRJc0VuY3J5cHRlZF0gPSB1c2VTdGF0ZShyb29tID8gY2xpLmlzUm9vbUVuY3J5cHRlZChyb29tLnJvb21JZCkgOiB1bmRlZmluZWQpO1xyXG5cclxuICAgIGNvbnN0IHVwZGF0ZSA9IHVzZUNhbGxiYWNrKChldmVudCkgPT4ge1xyXG4gICAgICAgIGlmIChldmVudC5nZXRUeXBlKCkgPT09IFwibS5yb29tLmVuY3J5cHRpb25cIikge1xyXG4gICAgICAgICAgICBzZXRJc0VuY3J5cHRlZChjbGkuaXNSb29tRW5jcnlwdGVkKHJvb20ucm9vbUlkKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgW2NsaSwgcm9vbV0pO1xyXG4gICAgdXNlRXZlbnRFbWl0dGVyKHJvb20gPyByb29tLmN1cnJlbnRTdGF0ZSA6IHVuZGVmaW5lZCwgXCJSb29tU3RhdGUuZXZlbnRzXCIsIHVwZGF0ZSk7XHJcbiAgICByZXR1cm4gaXNFbmNyeXB0ZWQ7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHVzZUhhc0Nyb3NzU2lnbmluZ0tleXMoY2xpLCBtZW1iZXIsIGNhblZlcmlmeSwgc2V0VXBkYXRpbmcpIHtcclxuICAgIHJldHVybiB1c2VBc3luY01lbW8oYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGlmICghY2FuVmVyaWZ5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2V0VXBkYXRpbmcodHJ1ZSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgY2xpLmRvd25sb2FkS2V5cyhbbWVtYmVyLnVzZXJJZF0pO1xyXG4gICAgICAgICAgICBjb25zdCB4c2kgPSBjbGkuZ2V0U3RvcmVkQ3Jvc3NTaWduaW5nRm9yVXNlcihtZW1iZXIudXNlcklkKTtcclxuICAgICAgICAgICAgY29uc3Qga2V5ID0geHNpICYmIHhzaS5nZXRJZCgpO1xyXG4gICAgICAgICAgICByZXR1cm4gISFrZXk7XHJcbiAgICAgICAgfSBmaW5hbGx5IHtcclxuICAgICAgICAgICAgc2V0VXBkYXRpbmcoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH0sIFtjbGksIG1lbWJlciwgY2FuVmVyaWZ5XSwgZmFsc2UpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBEZXZpY2VJdGVtKHt1c2VySWQsIGRldmljZX0pIHtcclxuICAgIGNvbnN0IGNsaSA9IHVzZUNvbnRleHQoTWF0cml4Q2xpZW50Q29udGV4dCk7XHJcbiAgICBjb25zdCBpc01lID0gdXNlcklkID09PSBjbGkuZ2V0VXNlcklkKCk7XHJcbiAgICBjb25zdCBkZXZpY2VUcnVzdCA9IGNsaS5jaGVja0RldmljZVRydXN0KHVzZXJJZCwgZGV2aWNlLmRldmljZUlkKTtcclxuICAgIGNvbnN0IHVzZXJUcnVzdCA9IGNsaS5jaGVja1VzZXJUcnVzdCh1c2VySWQpO1xyXG4gICAgLy8gRm9yIHlvdXIgb3duIGRldmljZXMsIHdlIHVzZSB0aGUgc3RyaWN0ZXIgY2hlY2sgb2YgY3Jvc3Mtc2lnbmluZ1xyXG4gICAgLy8gdmVyaWZpY2F0aW9uIHRvIGVuY291cmFnZSBldmVyeW9uZSB0byB0cnVzdCB0aGVpciBvd24gZGV2aWNlcyB2aWFcclxuICAgIC8vIGNyb3NzLXNpZ25pbmcgc28gdGhhdCBvdGhlciB1c2VycyBjYW4gdGhlbiBzYWZlbHkgdHJ1c3QgeW91LlxyXG4gICAgLy8gRm9yIG90aGVyIHBlb3BsZSdzIGRldmljZXMsIHRoZSBtb3JlIGdlbmVyYWwgdmVyaWZpZWQgY2hlY2sgdGhhdFxyXG4gICAgLy8gaW5jbHVkZXMgbG9jYWxseSB2ZXJpZmllZCBkZXZpY2VzIGNhbiBiZSB1c2VkLlxyXG4gICAgY29uc3QgaXNWZXJpZmllZCA9IChpc01lICYmIFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSkgP1xyXG4gICAgICAgIGRldmljZVRydXN0LmlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQoKSA6XHJcbiAgICAgICAgZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpO1xyXG5cclxuICAgIGNvbnN0IGNsYXNzZXMgPSBjbGFzc05hbWVzKFwibXhfVXNlckluZm9fZGV2aWNlXCIsIHtcclxuICAgICAgICBteF9Vc2VySW5mb19kZXZpY2VfdmVyaWZpZWQ6IGlzVmVyaWZpZWQsXHJcbiAgICAgICAgbXhfVXNlckluZm9fZGV2aWNlX3VudmVyaWZpZWQ6ICFpc1ZlcmlmaWVkLFxyXG4gICAgfSk7XHJcbiAgICBjb25zdCBpY29uQ2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9FMkVJY29uXCIsIHtcclxuICAgICAgICBteF9FMkVJY29uX25vcm1hbDogIXVzZXJUcnVzdC5pc1ZlcmlmaWVkKCksXHJcbiAgICAgICAgbXhfRTJFSWNvbl92ZXJpZmllZDogaXNWZXJpZmllZCxcclxuICAgICAgICBteF9FMkVJY29uX3dhcm5pbmc6IHVzZXJUcnVzdC5pc1ZlcmlmaWVkKCkgJiYgIWlzVmVyaWZpZWQsXHJcbiAgICB9KTtcclxuXHJcbiAgICBjb25zdCBvbkRldmljZUNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIGlmICghaXNWZXJpZmllZCkge1xyXG4gICAgICAgICAgICB2ZXJpZnlEZXZpY2UoY2xpLmdldFVzZXIodXNlcklkKSwgZGV2aWNlKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0IGRldmljZU5hbWUgPSBkZXZpY2UuYW1iaWd1b3VzID9cclxuICAgICAgICAgICAgKGRldmljZS5nZXREaXNwbGF5TmFtZSgpID8gZGV2aWNlLmdldERpc3BsYXlOYW1lKCkgOiBcIlwiKSArIFwiIChcIiArIGRldmljZS5kZXZpY2VJZCArIFwiKVwiIDpcclxuICAgICAgICAgICAgZGV2aWNlLmdldERpc3BsYXlOYW1lKCk7XHJcbiAgICBsZXQgdHJ1c3RlZExhYmVsID0gbnVsbDtcclxuICAgIGlmICh1c2VyVHJ1c3QuaXNWZXJpZmllZCgpKSB0cnVzdGVkTGFiZWwgPSBpc1ZlcmlmaWVkID8gX3QoXCJUcnVzdGVkXCIpIDogX3QoXCJOb3QgdHJ1c3RlZFwiKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzfVxyXG4gICAgICAgICAgICB0aXRsZT17ZGV2aWNlLmRldmljZUlkfVxyXG4gICAgICAgICAgICBvbkNsaWNrPXtvbkRldmljZUNsaWNrfVxyXG4gICAgICAgID5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2ljb25DbGFzc2VzfSAvPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2RldmljZV9uYW1lXCI+e2RldmljZU5hbWV9PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZGV2aWNlX3RydXN0ZWRcIj57dHJ1c3RlZExhYmVsfTwvZGl2PlxyXG4gICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIERldmljZXNTZWN0aW9uKHtkZXZpY2VzLCB1c2VySWQsIGxvYWRpbmd9KSB7XHJcbiAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICBjb25zdCBjbGkgPSB1c2VDb250ZXh0KE1hdHJpeENsaWVudENvbnRleHQpO1xyXG4gICAgY29uc3QgdXNlclRydXN0ID0gY2xpLmNoZWNrVXNlclRydXN0KHVzZXJJZCk7XHJcblxyXG4gICAgY29uc3QgW2lzRXhwYW5kZWQsIHNldEV4cGFuZGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuXHJcbiAgICBpZiAobG9hZGluZykge1xyXG4gICAgICAgIC8vIHN0aWxsIGxvYWRpbmdcclxuICAgICAgICByZXR1cm4gPFNwaW5uZXIgLz47XHJcbiAgICB9XHJcbiAgICBpZiAoZGV2aWNlcyA9PT0gbnVsbCkge1xyXG4gICAgICAgIHJldHVybiBfdChcIlVuYWJsZSB0byBsb2FkIHNlc3Npb24gbGlzdFwiKTtcclxuICAgIH1cclxuICAgIGNvbnN0IGlzTWUgPSB1c2VySWQgPT09IGNsaS5nZXRVc2VySWQoKTtcclxuICAgIGNvbnN0IGRldmljZVRydXN0cyA9IGRldmljZXMubWFwKGQgPT4gY2xpLmNoZWNrRGV2aWNlVHJ1c3QodXNlcklkLCBkLmRldmljZUlkKSk7XHJcblxyXG4gICAgbGV0IGV4cGFuZFNlY3Rpb25EZXZpY2VzID0gW107XHJcbiAgICBjb25zdCB1bnZlcmlmaWVkRGV2aWNlcyA9IFtdO1xyXG5cclxuICAgIGxldCBleHBhbmRDb3VudENhcHRpb247XHJcbiAgICBsZXQgZXhwYW5kSGlkZUNhcHRpb247XHJcbiAgICBsZXQgZXhwYW5kSWNvbkNsYXNzZXMgPSBcIm14X0UyRUljb25cIjtcclxuXHJcbiAgICBpZiAodXNlclRydXN0LmlzVmVyaWZpZWQoKSkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGV2aWNlcy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBjb25zdCBkZXZpY2UgPSBkZXZpY2VzW2ldO1xyXG4gICAgICAgICAgICBjb25zdCBkZXZpY2VUcnVzdCA9IGRldmljZVRydXN0c1tpXTtcclxuICAgICAgICAgICAgLy8gRm9yIHlvdXIgb3duIGRldmljZXMsIHdlIHVzZSB0aGUgc3RyaWN0ZXIgY2hlY2sgb2YgY3Jvc3Mtc2lnbmluZ1xyXG4gICAgICAgICAgICAvLyB2ZXJpZmljYXRpb24gdG8gZW5jb3VyYWdlIGV2ZXJ5b25lIHRvIHRydXN0IHRoZWlyIG93biBkZXZpY2VzIHZpYVxyXG4gICAgICAgICAgICAvLyBjcm9zcy1zaWduaW5nIHNvIHRoYXQgb3RoZXIgdXNlcnMgY2FuIHRoZW4gc2FmZWx5IHRydXN0IHlvdS5cclxuICAgICAgICAgICAgLy8gRm9yIG90aGVyIHBlb3BsZSdzIGRldmljZXMsIHRoZSBtb3JlIGdlbmVyYWwgdmVyaWZpZWQgY2hlY2sgdGhhdFxyXG4gICAgICAgICAgICAvLyBpbmNsdWRlcyBsb2NhbGx5IHZlcmlmaWVkIGRldmljZXMgY2FuIGJlIHVzZWQuXHJcbiAgICAgICAgICAgIGNvbnN0IGlzVmVyaWZpZWQgPSAoaXNNZSAmJiBTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpID9cclxuICAgICAgICAgICAgICAgIGRldmljZVRydXN0LmlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQoKSA6XHJcbiAgICAgICAgICAgICAgICBkZXZpY2VUcnVzdC5pc1ZlcmlmaWVkKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoaXNWZXJpZmllZCkge1xyXG4gICAgICAgICAgICAgICAgZXhwYW5kU2VjdGlvbkRldmljZXMucHVzaChkZXZpY2UpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdW52ZXJpZmllZERldmljZXMucHVzaChkZXZpY2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGV4cGFuZENvdW50Q2FwdGlvbiA9IF90KFwiJShjb3VudClzIHZlcmlmaWVkIHNlc3Npb25zXCIsIHtjb3VudDogZXhwYW5kU2VjdGlvbkRldmljZXMubGVuZ3RofSk7XHJcbiAgICAgICAgZXhwYW5kSGlkZUNhcHRpb24gPSBfdChcIkhpZGUgdmVyaWZpZWQgc2Vzc2lvbnNcIik7XHJcbiAgICAgICAgZXhwYW5kSWNvbkNsYXNzZXMgKz0gXCIgbXhfRTJFSWNvbl92ZXJpZmllZFwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBleHBhbmRTZWN0aW9uRGV2aWNlcyA9IGRldmljZXM7XHJcbiAgICAgICAgZXhwYW5kQ291bnRDYXB0aW9uID0gX3QoXCIlKGNvdW50KXMgc2Vzc2lvbnNcIiwge2NvdW50OiBkZXZpY2VzLmxlbmd0aH0pO1xyXG4gICAgICAgIGV4cGFuZEhpZGVDYXB0aW9uID0gX3QoXCJIaWRlIHNlc3Npb25zXCIpO1xyXG4gICAgICAgIGV4cGFuZEljb25DbGFzc2VzICs9IFwiIG14X0UyRUljb25fbm9ybWFsXCI7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGV4cGFuZEJ1dHRvbjtcclxuICAgIGlmIChleHBhbmRTZWN0aW9uRGV2aWNlcy5sZW5ndGgpIHtcclxuICAgICAgICBpZiAoaXNFeHBhbmRlZCkge1xyXG4gICAgICAgICAgICBleHBhbmRCdXR0b24gPSAoPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZXhwYW5kIG14X2xpbmtCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gc2V0RXhwYW5kZWQoZmFsc2UpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PntleHBhbmRIaWRlQ2FwdGlvbn08L2Rpdj5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZXhwYW5kQnV0dG9uID0gKDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2V4cGFuZCBteF9saW5rQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHNldEV4cGFuZGVkKHRydWUpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17ZXhwYW5kSWNvbkNsYXNzZXN9IC8+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PntleHBhbmRDb3VudENhcHRpb259PC9kaXY+XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj4pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBsZXQgZGV2aWNlTGlzdCA9IHVudmVyaWZpZWREZXZpY2VzLm1hcCgoZGV2aWNlLCBpKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICg8RGV2aWNlSXRlbSBrZXk9e2l9IHVzZXJJZD17dXNlcklkfSBkZXZpY2U9e2RldmljZX0gLz4pO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoaXNFeHBhbmRlZCkge1xyXG4gICAgICAgIGNvbnN0IGtleVN0YXJ0ID0gdW52ZXJpZmllZERldmljZXMubGVuZ3RoO1xyXG4gICAgICAgIGRldmljZUxpc3QgPSBkZXZpY2VMaXN0LmNvbmNhdChleHBhbmRTZWN0aW9uRGV2aWNlcy5tYXAoKGRldmljZSwgaSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gKDxEZXZpY2VJdGVtIGtleT17aSArIGtleVN0YXJ0fSB1c2VySWQ9e3VzZXJJZH0gZGV2aWNlPXtkZXZpY2V9IC8+KTtcclxuICAgICAgICB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2RldmljZXNcIj5cclxuICAgICAgICAgICAgPGRpdj57ZGV2aWNlTGlzdH08L2Rpdj5cclxuICAgICAgICAgICAgPGRpdj57ZXhwYW5kQnV0dG9ufTwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxufVxyXG5cclxuY29uc3QgVXNlck9wdGlvbnNTZWN0aW9uID0gKHttZW1iZXIsIGlzSWdub3JlZCwgY2FuSW52aXRlLCBkZXZpY2VzfSkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICBsZXQgaWdub3JlQnV0dG9uID0gbnVsbDtcclxuICAgIGxldCBpbnNlcnRQaWxsQnV0dG9uID0gbnVsbDtcclxuICAgIGxldCBpbnZpdGVVc2VyQnV0dG9uID0gbnVsbDtcclxuICAgIGxldCByZWFkUmVjZWlwdEJ1dHRvbiA9IG51bGw7XHJcblxyXG4gICAgY29uc3QgaXNNZSA9IG1lbWJlci51c2VySWQgPT09IGNsaS5nZXRVc2VySWQoKTtcclxuXHJcbiAgICBjb25zdCBvblNoYXJlVXNlckNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IFNoYXJlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuU2hhcmVEaWFsb2dcIik7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnc2hhcmUgcm9vbSBtZW1iZXIgZGlhbG9nJywgJycsIFNoYXJlRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRhcmdldDogbWVtYmVyLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICAvLyBPbmx5IGFsbG93IHRoZSB1c2VyIHRvIGlnbm9yZSB0aGUgdXNlciBpZiBpdHMgbm90IG91cnNlbHZlc1xyXG4gICAgLy8gc2FtZSBnb2VzIGZvciBqdW1waW5nIHRvIHJlYWQgcmVjZWlwdFxyXG4gICAgaWYgKCFpc01lKSB7XHJcbiAgICAgICAgY29uc3Qgb25JZ25vcmVUb2dnbGUgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlnbm9yZWRVc2VycyA9IGNsaS5nZXRJZ25vcmVkVXNlcnMoKTtcclxuICAgICAgICAgICAgaWYgKGlzSWdub3JlZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaW5kZXggPSBpZ25vcmVkVXNlcnMuaW5kZXhPZihtZW1iZXIudXNlcklkKTtcclxuICAgICAgICAgICAgICAgIGlmIChpbmRleCAhPT0gLTEpIGlnbm9yZWRVc2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWdub3JlZFVzZXJzLnB1c2gobWVtYmVyLnVzZXJJZCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNsaS5zZXRJZ25vcmVkVXNlcnMoaWdub3JlZFVzZXJzKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZ25vcmVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e29uSWdub3JlVG9nZ2xlfSBjbGFzc05hbWU9e2NsYXNzTmFtZXMoXCJteF9Vc2VySW5mb19maWVsZFwiLCB7bXhfVXNlckluZm9fZGVzdHJ1Y3RpdmU6ICFpc0lnbm9yZWR9KX0+XHJcbiAgICAgICAgICAgICAgICB7IGlzSWdub3JlZCA/IF90KFwiVW5pZ25vcmVcIikgOiBfdChcIklnbm9yZVwiKSB9XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBpZiAobWVtYmVyLnJvb21JZCkge1xyXG4gICAgICAgICAgICBjb25zdCBvblJlYWRSZWNlaXB0QnV0dG9uID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20obWVtYmVyLnJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRfaWQ6IHJvb20uZ2V0RXZlbnRSZWFkVXBUbyhtZW1iZXIudXNlcklkKSxcclxuICAgICAgICAgICAgICAgICAgICByb29tX2lkOiBtZW1iZXIucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluc2VydFBpbGxCdXR0b24gPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnaW5zZXJ0X21lbnRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJfaWQ6IG1lbWJlci51c2VySWQsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHJlYWRSZWNlaXB0QnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17b25SZWFkUmVjZWlwdEJ1dHRvbn0gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdKdW1wIHRvIHJlYWQgcmVjZWlwdCcpIH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgIGluc2VydFBpbGxCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtvbkluc2VydFBpbGxCdXR0b259IGNsYXNzTmFtZT17XCJteF9Vc2VySW5mb19maWVsZFwifT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdNZW50aW9uJykgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNhbkludml0ZSAmJiAoIW1lbWJlciB8fCAhbWVtYmVyLm1lbWJlcnNoaXAgfHwgbWVtYmVyLm1lbWJlcnNoaXAgPT09ICdsZWF2ZScpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21JZCA9IG1lbWJlciAmJiBtZW1iZXIucm9vbUlkID8gbWVtYmVyLnJvb21JZCA6IFJvb21WaWV3U3RvcmUuZ2V0Um9vbUlkKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW52aXRlVXNlckJ1dHRvbiA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gV2UgdXNlIGEgTXVsdGlJbnZpdGVyIHRvIHJlLXVzZSB0aGUgaW52aXRlIGxvZ2ljLCBldmVuIHRob3VnaFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdlJ3JlIG9ubHkgaW52aXRpbmcgb25lIHVzZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaW52aXRlciA9IG5ldyBNdWx0aUludml0ZXIocm9vbUlkKTtcclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCBpbnZpdGVyLmludml0ZShbbWVtYmVyLnVzZXJJZF0pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW52aXRlci5nZXRDb21wbGV0aW9uU3RhdGUobWVtYmVyLnVzZXJJZCkgIT09IFwiaW52aXRlZFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoaW52aXRlci5nZXRFcnJvclRleHQobWVtYmVyLnVzZXJJZCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuRXJyb3JEaWFsb2cnKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gaW52aXRlJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRmFpbGVkIHRvIGludml0ZScpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgaW52aXRlVXNlckJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e29uSW52aXRlVXNlckJ1dHRvbn0gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdJbnZpdGUnKSB9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHNoYXJlVXNlckJ1dHRvbiA9IChcclxuICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtvblNoYXJlVXNlckNsaWNrfSBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19maWVsZFwiPlxyXG4gICAgICAgICAgICB7IF90KCdTaGFyZSBMaW5rIHRvIFVzZXInKSB9XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgKTtcclxuXHJcbiAgICBsZXQgZGlyZWN0TWVzc2FnZUJ1dHRvbjtcclxuICAgIGlmICghaXNNZSkge1xyXG4gICAgICAgIGRpcmVjdE1lc3NhZ2VCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9eygpID0+IG9wZW5ETUZvclVzZXIoY2xpLCBtZW1iZXIudXNlcklkKX0gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGRcIj5cclxuICAgICAgICAgICAgICAgIHsgX3QoJ0RpcmVjdCBtZXNzYWdlJykgfVxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgIDxoMz57IF90KFwiT3B0aW9uc1wiKSB9PC9oMz5cclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIHsgZGlyZWN0TWVzc2FnZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IHJlYWRSZWNlaXB0QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgc2hhcmVVc2VyQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgaW5zZXJ0UGlsbEJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IGludml0ZVVzZXJCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyBpZ25vcmVCdXR0b24gfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICk7XHJcbn07XHJcblxyXG5jb25zdCBfd2FyblNlbGZEZW1vdGUgPSBhc3luYyAoKSA9PiB7XHJcbiAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgY29uc3Qge2ZpbmlzaGVkfSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0RlbW90aW5nIFNlbGYnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICB0aXRsZTogX3QoXCJEZW1vdGUgeW91cnNlbGY/XCIpLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgeyBfdChcIllvdSB3aWxsIG5vdCBiZSBhYmxlIHRvIHVuZG8gdGhpcyBjaGFuZ2UgYXMgeW91IGFyZSBkZW1vdGluZyB5b3Vyc2VsZiwgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiaWYgeW91IGFyZSB0aGUgbGFzdCBwcml2aWxlZ2VkIHVzZXIgaW4gdGhlIHJvb20gaXQgd2lsbCBiZSBpbXBvc3NpYmxlIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcInRvIHJlZ2FpbiBwcml2aWxlZ2VzLlwiKSB9XHJcbiAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICBidXR0b246IF90KFwiRGVtb3RlXCIpLFxyXG4gICAgfSk7XHJcblxyXG4gICAgY29uc3QgW2NvbmZpcm1lZF0gPSBhd2FpdCBmaW5pc2hlZDtcclxuICAgIHJldHVybiBjb25maXJtZWQ7XHJcbn07XHJcblxyXG5jb25zdCBHZW5lcmljQWRtaW5Ub29sc0NvbnRhaW5lciA9ICh7Y2hpbGRyZW59KSA9PiB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgIDxoMz57IF90KFwiQWRtaW4gVG9vbHNcIikgfTwvaDM+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxufTtcclxuXHJcbmNvbnN0IF9pc011dGVkID0gKG1lbWJlciwgcG93ZXJMZXZlbENvbnRlbnQpID0+IHtcclxuICAgIGlmICghcG93ZXJMZXZlbENvbnRlbnQgfHwgIW1lbWJlcikgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIGNvbnN0IGxldmVsVG9TZW5kID0gKFxyXG4gICAgICAgIChwb3dlckxldmVsQ29udGVudC5ldmVudHMgPyBwb3dlckxldmVsQ29udGVudC5ldmVudHNbXCJtLnJvb20ubWVzc2FnZVwiXSA6IG51bGwpIHx8XHJcbiAgICAgICAgcG93ZXJMZXZlbENvbnRlbnQuZXZlbnRzX2RlZmF1bHRcclxuICAgICk7XHJcbiAgICByZXR1cm4gbWVtYmVyLnBvd2VyTGV2ZWwgPCBsZXZlbFRvU2VuZDtcclxufTtcclxuXHJcbmNvbnN0IHVzZVJvb21Qb3dlckxldmVscyA9IChjbGksIHJvb20pID0+IHtcclxuICAgIGNvbnN0IFtwb3dlckxldmVscywgc2V0UG93ZXJMZXZlbHNdID0gdXNlU3RhdGUoe30pO1xyXG5cclxuICAgIGNvbnN0IHVwZGF0ZSA9IHVzZUNhbGxiYWNrKCgpID0+IHtcclxuICAgICAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBldmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLnBvd2VyX2xldmVsc1wiLCBcIlwiKTtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgc2V0UG93ZXJMZXZlbHMoZXZlbnQuZ2V0Q29udGVudCgpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZXRQb3dlckxldmVscyh7fSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIHNldFBvd2VyTGV2ZWxzKHt9KTtcclxuICAgICAgICB9O1xyXG4gICAgfSwgW3Jvb21dKTtcclxuXHJcbiAgICB1c2VFdmVudEVtaXR0ZXIoY2xpLCBcIlJvb21TdGF0ZS5tZW1iZXJzXCIsIHVwZGF0ZSk7XHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIHVwZGF0ZSgpO1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIHNldFBvd2VyTGV2ZWxzKHt9KTtcclxuICAgICAgICB9O1xyXG4gICAgfSwgW3VwZGF0ZV0pO1xyXG4gICAgcmV0dXJuIHBvd2VyTGV2ZWxzO1xyXG59O1xyXG5cclxuY29uc3QgUm9vbUtpY2tCdXR0b24gPSAoe21lbWJlciwgc3RhcnRVcGRhdGluZywgc3RvcFVwZGF0aW5nfSkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICAvLyBjaGVjayBpZiB1c2VyIGNhbiBiZSBraWNrZWQvZGlzaW52aXRlZFxyXG4gICAgaWYgKG1lbWJlci5tZW1iZXJzaGlwICE9PSBcImludml0ZVwiICYmIG1lbWJlci5tZW1iZXJzaGlwICE9PSBcImpvaW5cIikgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgY29uc3Qgb25LaWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IENvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuQ29uZmlybVVzZXJBY3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3Qge2ZpbmlzaGVkfSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICdDb25maXJtIFVzZXIgQWN0aW9uIERpYWxvZycsXHJcbiAgICAgICAgICAgICdvbktpY2snLFxyXG4gICAgICAgICAgICBDb25maXJtVXNlckFjdGlvbkRpYWxvZyxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbWVtYmVyLFxyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBtZW1iZXIubWVtYmVyc2hpcCA9PT0gXCJpbnZpdGVcIiA/IF90KFwiRGlzaW52aXRlXCIpIDogX3QoXCJLaWNrXCIpLFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6IG1lbWJlci5tZW1iZXJzaGlwID09PSBcImludml0ZVwiID8gX3QoXCJEaXNpbnZpdGUgdGhpcyB1c2VyP1wiKSA6IF90KFwiS2ljayB0aGlzIHVzZXI/XCIpLFxyXG4gICAgICAgICAgICAgICAgYXNrUmVhc29uOiBtZW1iZXIubWVtYmVyc2hpcCA9PT0gXCJqb2luXCIsXHJcbiAgICAgICAgICAgICAgICBkYW5nZXI6IHRydWUsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgY29uc3QgW3Byb2NlZWQsIHJlYXNvbl0gPSBhd2FpdCBmaW5pc2hlZDtcclxuICAgICAgICBpZiAoIXByb2NlZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgc3RhcnRVcGRhdGluZygpO1xyXG4gICAgICAgIGNsaS5raWNrKG1lbWJlci5yb29tSWQsIG1lbWJlci51c2VySWQsIHJlYXNvbiB8fCB1bmRlZmluZWQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBOTy1PUDsgcmVseSBvbiB0aGUgbS5yb29tLm1lbWJlciBldmVudCBjb21pbmcgZG93biBlbHNlIHdlIGNvdWxkXHJcbiAgICAgICAgICAgIC8vIGdldCBvdXQgb2Ygc3luYyBpZiB3ZSBmb3JjZSBzZXRTdGF0ZSBoZXJlIVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIktpY2sgc3VjY2Vzc1wiKTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIktpY2sgZXJyb3I6IFwiICsgZXJyKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGtpY2snLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkZhaWxlZCB0byBraWNrXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogXCJPcGVyYXRpb24gZmFpbGVkXCIpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgc3RvcFVwZGF0aW5nKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0IGtpY2tMYWJlbCA9IG1lbWJlci5tZW1iZXJzaGlwID09PSBcImludml0ZVwiID8gX3QoXCJEaXNpbnZpdGVcIikgOiBfdChcIktpY2tcIik7XHJcbiAgICByZXR1cm4gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGQgbXhfVXNlckluZm9fZGVzdHJ1Y3RpdmVcIiBvbkNsaWNrPXtvbktpY2t9PlxyXG4gICAgICAgIHsga2lja0xhYmVsIH1cclxuICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbn07XHJcblxyXG5jb25zdCBSZWRhY3RNZXNzYWdlc0J1dHRvbiA9ICh7bWVtYmVyfSkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICBjb25zdCBvblJlZGFjdEFsbE1lc3NhZ2VzID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHtyb29tSWQsIHVzZXJJZH0gPSBtZW1iZXI7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IGNsaS5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHRpbWVsaW5lID0gcm9vbS5nZXRMaXZlVGltZWxpbmUoKTtcclxuICAgICAgICBsZXQgZXZlbnRzVG9SZWRhY3QgPSBbXTtcclxuICAgICAgICB3aGlsZSAodGltZWxpbmUpIHtcclxuICAgICAgICAgICAgZXZlbnRzVG9SZWRhY3QgPSB0aW1lbGluZS5nZXRFdmVudHMoKS5yZWR1Y2UoKGV2ZW50cywgZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChldmVudC5nZXRTZW5kZXIoKSA9PT0gdXNlcklkICYmICFldmVudC5pc1JlZGFjdGVkKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXZlbnRzLmNvbmNhdChldmVudCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudHM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGV2ZW50c1RvUmVkYWN0KTtcclxuICAgICAgICAgICAgdGltZWxpbmUgPSB0aW1lbGluZS5nZXROZWlnaGJvdXJpbmdUaW1lbGluZShFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb3VudCA9IGV2ZW50c1RvUmVkYWN0Lmxlbmd0aDtcclxuICAgICAgICBjb25zdCB1c2VyID0gbWVtYmVyLm5hbWU7XHJcblxyXG4gICAgICAgIGlmIChjb3VudCA9PT0gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBJbmZvRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuSW5mb0RpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTm8gdXNlciBtZXNzYWdlcyBmb3VuZCB0byByZW1vdmUnLCAnJywgSW5mb0RpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiTm8gcmVjZW50IG1lc3NhZ2VzIGJ5ICUodXNlcilzIGZvdW5kXCIsIHt1c2VyfSksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD57IF90KFwiVHJ5IHNjcm9sbGluZyB1cCBpbiB0aGUgdGltZWxpbmUgdG8gc2VlIGlmIHRoZXJlIGFyZSBhbnkgZWFybGllciBvbmVzLlwiKSB9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHtmaW5pc2hlZH0gPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdSZW1vdmUgcmVjZW50IG1lc3NhZ2VzIGJ5IHVzZXInLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlJlbW92ZSByZWNlbnQgbWVzc2FnZXMgYnkgJSh1c2VyKXNcIiwge3VzZXJ9KSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwPnsgX3QoXCJZb3UgYXJlIGFib3V0IHRvIHJlbW92ZSAlKGNvdW50KXMgbWVzc2FnZXMgYnkgJSh1c2VyKXMuIFRoaXMgY2Fubm90IGJlIHVuZG9uZS4gRG8geW91IHdpc2ggdG8gY29udGludWU/XCIsIHtjb3VudCwgdXNlcn0pIH08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwPnsgX3QoXCJGb3IgYSBsYXJnZSBhbW91bnQgb2YgbWVzc2FnZXMsIHRoaXMgbWlnaHQgdGFrZSBzb21lIHRpbWUuIFBsZWFzZSBkb24ndCByZWZyZXNoIHlvdXIgY2xpZW50IGluIHRoZSBtZWFudGltZS5cIikgfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICBidXR0b246IF90KFwiUmVtb3ZlICUoY291bnQpcyBtZXNzYWdlc1wiLCB7Y291bnR9KSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBbY29uZmlybWVkXSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgICAgICBpZiAoIWNvbmZpcm1lZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBTdWJtaXR0aW5nIGEgbGFyZ2UgbnVtYmVyIG9mIHJlZGFjdGlvbnMgZnJlZXplcyB0aGUgVUksXHJcbiAgICAgICAgICAgIC8vIHNvIGZpcnN0IHlpZWxkIHRvIGFsbG93IHRvIHJlcmVuZGVyIGFmdGVyIGNsb3NpbmcgdGhlIGRpYWxvZy5cclxuICAgICAgICAgICAgYXdhaXQgUHJvbWlzZS5yZXNvbHZlKCk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oYFN0YXJ0ZWQgcmVkYWN0aW5nIHJlY2VudCAke2NvdW50fSBtZXNzYWdlcyBmb3IgJHt1c2VyfSBpbiAke3Jvb21JZH1gKTtcclxuICAgICAgICAgICAgYXdhaXQgUHJvbWlzZS5hbGwoZXZlbnRzVG9SZWRhY3QubWFwKGFzeW5jIGV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgY2xpLnJlZGFjdEV2ZW50KHJvb21JZCwgZXZlbnQuZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgYW5kIHN3YWxsb3cgZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkNvdWxkIG5vdCByZWRhY3RcIiwgZXZlbnQuZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhgRmluaXNoZWQgcmVkYWN0aW5nIHJlY2VudCAke2NvdW50fSBtZXNzYWdlcyBmb3IgJHt1c2VyfSBpbiAke3Jvb21JZH1gKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19maWVsZCBteF9Vc2VySW5mb19kZXN0cnVjdGl2ZVwiIG9uQ2xpY2s9e29uUmVkYWN0QWxsTWVzc2FnZXN9PlxyXG4gICAgICAgIHsgX3QoXCJSZW1vdmUgcmVjZW50IG1lc3NhZ2VzXCIpIH1cclxuICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbn07XHJcblxyXG5jb25zdCBCYW5Ub2dnbGVCdXR0b24gPSAoe21lbWJlciwgc3RhcnRVcGRhdGluZywgc3RvcFVwZGF0aW5nfSkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICBjb25zdCBvbkJhbk9yVW5iYW4gPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgQ29uZmlybVVzZXJBY3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5Db25maXJtVXNlckFjdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCB7ZmluaXNoZWR9ID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgJ0NvbmZpcm0gVXNlciBBY3Rpb24gRGlhbG9nJyxcclxuICAgICAgICAgICAgJ29uQmFuT3JVbmJhbicsXHJcbiAgICAgICAgICAgIENvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBtZW1iZXIsXHJcbiAgICAgICAgICAgICAgICBhY3Rpb246IG1lbWJlci5tZW1iZXJzaGlwID09PSAnYmFuJyA/IF90KFwiVW5iYW5cIikgOiBfdChcIkJhblwiKSxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBtZW1iZXIubWVtYmVyc2hpcCA9PT0gJ2JhbicgPyBfdChcIlVuYmFuIHRoaXMgdXNlcj9cIikgOiBfdChcIkJhbiB0aGlzIHVzZXI/XCIpLFxyXG4gICAgICAgICAgICAgICAgYXNrUmVhc29uOiBtZW1iZXIubWVtYmVyc2hpcCAhPT0gJ2JhbicsXHJcbiAgICAgICAgICAgICAgICBkYW5nZXI6IG1lbWJlci5tZW1iZXJzaGlwICE9PSAnYmFuJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBbcHJvY2VlZCwgcmVhc29uXSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgIGlmICghcHJvY2VlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBzdGFydFVwZGF0aW5nKCk7XHJcbiAgICAgICAgbGV0IHByb21pc2U7XHJcbiAgICAgICAgaWYgKG1lbWJlci5tZW1iZXJzaGlwID09PSAnYmFuJykge1xyXG4gICAgICAgICAgICBwcm9taXNlID0gY2xpLnVuYmFuKG1lbWJlci5yb29tSWQsIG1lbWJlci51c2VySWQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHByb21pc2UgPSBjbGkuYmFuKG1lbWJlci5yb29tSWQsIG1lbWJlci51c2VySWQsIHJlYXNvbiB8fCB1bmRlZmluZWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwcm9taXNlLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBOTy1PUDsgcmVseSBvbiB0aGUgbS5yb29tLm1lbWJlciBldmVudCBjb21pbmcgZG93biBlbHNlIHdlIGNvdWxkXHJcbiAgICAgICAgICAgIC8vIGdldCBvdXQgb2Ygc3luYyBpZiB3ZSBmb3JjZSBzZXRTdGF0ZSBoZXJlIVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkJhbiBzdWNjZXNzXCIpO1xyXG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQmFuIGVycm9yOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBiYW4gdXNlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJGYWlsZWQgdG8gYmFuIHVzZXJcIiksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICBzdG9wVXBkYXRpbmcoKTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgbGV0IGxhYmVsID0gX3QoXCJCYW5cIik7XHJcbiAgICBpZiAobWVtYmVyLm1lbWJlcnNoaXAgPT09ICdiYW4nKSB7XHJcbiAgICAgICAgbGFiZWwgPSBfdChcIlVuYmFuXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGNsYXNzZXMgPSBjbGFzc05hbWVzKFwibXhfVXNlckluZm9fZmllbGRcIiwge1xyXG4gICAgICAgIG14X1VzZXJJbmZvX2Rlc3RydWN0aXZlOiBtZW1iZXIubWVtYmVyc2hpcCAhPT0gJ2JhbicsXHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPXtjbGFzc2VzfSBvbkNsaWNrPXtvbkJhbk9yVW5iYW59PlxyXG4gICAgICAgIHsgbGFiZWwgfVxyXG4gICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxufTtcclxuXHJcbmNvbnN0IE11dGVUb2dnbGVCdXR0b24gPSAoe21lbWJlciwgcm9vbSwgcG93ZXJMZXZlbHMsIHN0YXJ0VXBkYXRpbmcsIHN0b3BVcGRhdGluZ30pID0+IHtcclxuICAgIGNvbnN0IGNsaSA9IHVzZUNvbnRleHQoTWF0cml4Q2xpZW50Q29udGV4dCk7XHJcblxyXG4gICAgLy8gRG9uJ3Qgc2hvdyB0aGUgbXV0ZS91bm11dGUgb3B0aW9uIGlmIHRoZSB1c2VyIGlzIG5vdCBpbiB0aGUgcm9vbVxyXG4gICAgaWYgKG1lbWJlci5tZW1iZXJzaGlwICE9PSBcImpvaW5cIikgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgY29uc3QgaXNNdXRlZCA9IF9pc011dGVkKG1lbWJlciwgcG93ZXJMZXZlbHMpO1xyXG4gICAgY29uc3Qgb25NdXRlVG9nZ2xlID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gbWVtYmVyLnJvb21JZDtcclxuICAgICAgICBjb25zdCB0YXJnZXQgPSBtZW1iZXIudXNlcklkO1xyXG5cclxuICAgICAgICAvLyBpZiBtdXRpbmcgc2VsZiwgd2FybiBhcyBpdCBtYXkgYmUgaXJyZXZlcnNpYmxlXHJcbiAgICAgICAgaWYgKHRhcmdldCA9PT0gY2xpLmdldFVzZXJJZCgpKSB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIShhd2FpdCBfd2FyblNlbGZEZW1vdGUoKSkpIHJldHVybjtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byB3YXJuIGFib3V0IHNlbGYgZGVtb3Rpb246IFwiLCBlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbEV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIFwiXCIpO1xyXG4gICAgICAgIGlmICghcG93ZXJMZXZlbEV2ZW50KSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxzID0gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBjb25zdCBsZXZlbFRvU2VuZCA9IChcclxuICAgICAgICAgICAgKHBvd2VyTGV2ZWxzLmV2ZW50cyA/IHBvd2VyTGV2ZWxzLmV2ZW50c1tcIm0ucm9vbS5tZXNzYWdlXCJdIDogbnVsbCkgfHxcclxuICAgICAgICAgICAgcG93ZXJMZXZlbHMuZXZlbnRzX2RlZmF1bHRcclxuICAgICAgICApO1xyXG4gICAgICAgIGxldCBsZXZlbDtcclxuICAgICAgICBpZiAoaXNNdXRlZCkgeyAvLyB1bm11dGVcclxuICAgICAgICAgICAgbGV2ZWwgPSBsZXZlbFRvU2VuZDtcclxuICAgICAgICB9IGVsc2UgeyAvLyBtdXRlXHJcbiAgICAgICAgICAgIGxldmVsID0gbGV2ZWxUb1NlbmQgLSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXZlbCA9IHBhcnNlSW50KGxldmVsKTtcclxuXHJcbiAgICAgICAgaWYgKCFpc05hTihsZXZlbCkpIHtcclxuICAgICAgICAgICAgc3RhcnRVcGRhdGluZygpO1xyXG4gICAgICAgICAgICBjbGkuc2V0UG93ZXJMZXZlbChyb29tSWQsIHRhcmdldCwgbGV2ZWwsIHBvd2VyTGV2ZWxFdmVudCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBOTy1PUDsgcmVseSBvbiB0aGUgbS5yb29tLm1lbWJlciBldmVudCBjb21pbmcgZG93biBlbHNlIHdlIGNvdWxkXHJcbiAgICAgICAgICAgICAgICAvLyBnZXQgb3V0IG9mIHN5bmMgaWYgd2UgZm9yY2Ugc2V0U3RhdGUgaGVyZSFcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTXV0ZSB0b2dnbGUgc3VjY2Vzc1wiKTtcclxuICAgICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiTXV0ZSBlcnJvcjogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIG11dGUgdXNlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIkZhaWxlZCB0byBtdXRlIHVzZXJcIiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzdG9wVXBkYXRpbmcoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyhcIm14X1VzZXJJbmZvX2ZpZWxkXCIsIHtcclxuICAgICAgICBteF9Vc2VySW5mb19kZXN0cnVjdGl2ZTogIWlzTXV0ZWQsXHJcbiAgICB9KTtcclxuXHJcbiAgICBjb25zdCBtdXRlTGFiZWwgPSBpc011dGVkID8gX3QoXCJVbm11dGVcIikgOiBfdChcIk11dGVcIik7XHJcbiAgICByZXR1cm4gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPXtjbGFzc2VzfSBvbkNsaWNrPXtvbk11dGVUb2dnbGV9PlxyXG4gICAgICAgIHsgbXV0ZUxhYmVsIH1cclxuICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbn07XHJcblxyXG5jb25zdCBSb29tQWRtaW5Ub29sc0NvbnRhaW5lciA9ICh7cm9vbSwgY2hpbGRyZW4sIG1lbWJlciwgc3RhcnRVcGRhdGluZywgc3RvcFVwZGF0aW5nLCBwb3dlckxldmVsc30pID0+IHtcclxuICAgIGNvbnN0IGNsaSA9IHVzZUNvbnRleHQoTWF0cml4Q2xpZW50Q29udGV4dCk7XHJcbiAgICBsZXQga2lja0J1dHRvbjtcclxuICAgIGxldCBiYW5CdXR0b247XHJcbiAgICBsZXQgbXV0ZUJ1dHRvbjtcclxuICAgIGxldCByZWRhY3RCdXR0b247XHJcblxyXG4gICAgY29uc3QgZWRpdFBvd2VyTGV2ZWwgPSAoXHJcbiAgICAgICAgKHBvd2VyTGV2ZWxzLmV2ZW50cyA/IHBvd2VyTGV2ZWxzLmV2ZW50c1tcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIl0gOiBudWxsKSB8fFxyXG4gICAgICAgIHBvd2VyTGV2ZWxzLnN0YXRlX2RlZmF1bHRcclxuICAgICk7XHJcblxyXG4gICAgY29uc3QgbWUgPSByb29tLmdldE1lbWJlcihjbGkuZ2V0VXNlcklkKCkpO1xyXG4gICAgY29uc3QgaXNNZSA9IG1lLnVzZXJJZCA9PT0gbWVtYmVyLnVzZXJJZDtcclxuICAgIGNvbnN0IGNhbkFmZmVjdFVzZXIgPSBtZW1iZXIucG93ZXJMZXZlbCA8IG1lLnBvd2VyTGV2ZWwgfHwgaXNNZTtcclxuXHJcbiAgICBpZiAoY2FuQWZmZWN0VXNlciAmJiBtZS5wb3dlckxldmVsID49IHBvd2VyTGV2ZWxzLmtpY2spIHtcclxuICAgICAgICBraWNrQnV0dG9uID0gPFJvb21LaWNrQnV0dG9uIG1lbWJlcj17bWVtYmVyfSBzdGFydFVwZGF0aW5nPXtzdGFydFVwZGF0aW5nfSBzdG9wVXBkYXRpbmc9e3N0b3BVcGRhdGluZ30gLz47XHJcbiAgICB9XHJcbiAgICBpZiAobWUucG93ZXJMZXZlbCA+PSBwb3dlckxldmVscy5yZWRhY3QpIHtcclxuICAgICAgICByZWRhY3RCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxSZWRhY3RNZXNzYWdlc0J1dHRvbiBtZW1iZXI9e21lbWJlcn0gc3RhcnRVcGRhdGluZz17c3RhcnRVcGRhdGluZ30gc3RvcFVwZGF0aW5nPXtzdG9wVXBkYXRpbmd9IC8+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICAgIGlmIChjYW5BZmZlY3RVc2VyICYmIG1lLnBvd2VyTGV2ZWwgPj0gcG93ZXJMZXZlbHMuYmFuKSB7XHJcbiAgICAgICAgYmFuQnV0dG9uID0gPEJhblRvZ2dsZUJ1dHRvbiBtZW1iZXI9e21lbWJlcn0gc3RhcnRVcGRhdGluZz17c3RhcnRVcGRhdGluZ30gc3RvcFVwZGF0aW5nPXtzdG9wVXBkYXRpbmd9IC8+O1xyXG4gICAgfVxyXG4gICAgaWYgKGNhbkFmZmVjdFVzZXIgJiYgbWUucG93ZXJMZXZlbCA+PSBlZGl0UG93ZXJMZXZlbCkge1xyXG4gICAgICAgIG11dGVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxNdXRlVG9nZ2xlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICBtZW1iZXI9e21lbWJlcn1cclxuICAgICAgICAgICAgICAgIHJvb209e3Jvb219XHJcbiAgICAgICAgICAgICAgICBwb3dlckxldmVscz17cG93ZXJMZXZlbHN9XHJcbiAgICAgICAgICAgICAgICBzdGFydFVwZGF0aW5nPXtzdGFydFVwZGF0aW5nfVxyXG4gICAgICAgICAgICAgICAgc3RvcFVwZGF0aW5nPXtzdG9wVXBkYXRpbmd9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoa2lja0J1dHRvbiB8fCBiYW5CdXR0b24gfHwgbXV0ZUJ1dHRvbiB8fCByZWRhY3RCdXR0b24gfHwgY2hpbGRyZW4pIHtcclxuICAgICAgICByZXR1cm4gPEdlbmVyaWNBZG1pblRvb2xzQ29udGFpbmVyPlxyXG4gICAgICAgICAgICB7IG11dGVCdXR0b24gfVxyXG4gICAgICAgICAgICB7IGtpY2tCdXR0b24gfVxyXG4gICAgICAgICAgICB7IGJhbkJ1dHRvbiB9XHJcbiAgICAgICAgICAgIHsgcmVkYWN0QnV0dG9uIH1cclxuICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgPC9HZW5lcmljQWRtaW5Ub29sc0NvbnRhaW5lcj47XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIDxkaXYgLz47XHJcbn07XHJcblxyXG5jb25zdCBHcm91cEFkbWluVG9vbHNTZWN0aW9uID0gKHtjaGlsZHJlbiwgZ3JvdXBJZCwgZ3JvdXBNZW1iZXIsIHN0YXJ0VXBkYXRpbmcsIHN0b3BVcGRhdGluZ30pID0+IHtcclxuICAgIGNvbnN0IGNsaSA9IHVzZUNvbnRleHQoTWF0cml4Q2xpZW50Q29udGV4dCk7XHJcblxyXG4gICAgY29uc3QgW2lzUHJpdmlsZWdlZCwgc2V0SXNQcml2aWxlZ2VkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICAgIGNvbnN0IFtpc0ludml0ZWQsIHNldElzSW52aXRlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gICAgLy8gTGlzdGVuIHRvIGdyb3VwIHN0b3JlIGNoYW5nZXNcclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgICAgbGV0IHVubW91bnRlZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICBjb25zdCBvbkdyb3VwU3RvcmVVcGRhdGVkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgIHNldElzUHJpdmlsZWdlZChHcm91cFN0b3JlLmlzVXNlclByaXZpbGVnZWQoZ3JvdXBJZCkpO1xyXG4gICAgICAgICAgICBzZXRJc0ludml0ZWQoR3JvdXBTdG9yZS5nZXRHcm91cEludml0ZWRNZW1iZXJzKGdyb3VwSWQpLnNvbWUoXHJcbiAgICAgICAgICAgICAgICAobSkgPT4gbS51c2VySWQgPT09IGdyb3VwTWVtYmVyLnVzZXJJZCxcclxuICAgICAgICAgICAgKSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgR3JvdXBTdG9yZS5yZWdpc3Rlckxpc3RlbmVyKGdyb3VwSWQsIG9uR3JvdXBTdG9yZVVwZGF0ZWQpO1xyXG4gICAgICAgIG9uR3JvdXBTdG9yZVVwZGF0ZWQoKTtcclxuICAgICAgICAvLyBIYW5kbGUgdW5tb3VudFxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIHVubW91bnRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIEdyb3VwU3RvcmUudW5yZWdpc3Rlckxpc3RlbmVyKG9uR3JvdXBTdG9yZVVwZGF0ZWQpO1xyXG4gICAgICAgIH07XHJcbiAgICB9LCBbZ3JvdXBJZCwgZ3JvdXBNZW1iZXIudXNlcklkXSk7XHJcblxyXG4gICAgaWYgKGlzUHJpdmlsZWdlZCkge1xyXG4gICAgICAgIGNvbnN0IF9vbktpY2sgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IENvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuQ29uZmlybVVzZXJBY3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgICAgIGNvbnN0IHtmaW5pc2hlZH0gPSBNb2RhbC5jcmVhdGVEaWFsb2coQ29uZmlybVVzZXJBY3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIG1hdHJpeENsaWVudDogY2xpLFxyXG4gICAgICAgICAgICAgICAgZ3JvdXBNZW1iZXIsXHJcbiAgICAgICAgICAgICAgICBhY3Rpb246IGlzSW52aXRlZCA/IF90KCdEaXNpbnZpdGUnKSA6IF90KCdSZW1vdmUgZnJvbSBjb21tdW5pdHknKSxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBpc0ludml0ZWQgPyBfdCgnRGlzaW52aXRlIHRoaXMgdXNlciBmcm9tIGNvbW11bml0eT8nKVxyXG4gICAgICAgICAgICAgICAgICAgIDogX3QoJ1JlbW92ZSB0aGlzIHVzZXIgZnJvbSBjb21tdW5pdHk/JyksXHJcbiAgICAgICAgICAgICAgICBkYW5nZXI6IHRydWUsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgW3Byb2NlZWRdID0gYXdhaXQgZmluaXNoZWQ7XHJcbiAgICAgICAgICAgIGlmICghcHJvY2VlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgc3RhcnRVcGRhdGluZygpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlVXNlckZyb21Hcm91cChncm91cElkLCBncm91cE1lbWJlci51c2VySWQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gcmV0dXJuIHRvIHRoZSB1c2VyIGxpc3RcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiBcInZpZXdfdXNlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lbWJlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byByZW1vdmUgdXNlciBmcm9tIGdyb3VwJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvcicpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBpc0ludml0ZWQgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfdCgnRmFpbGVkIHRvIHdpdGhkcmF3IGludml0YXRpb24nKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF90KCdGYWlsZWQgdG8gcmVtb3ZlIHVzZXIgZnJvbSBjb21tdW5pdHknKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XHJcbiAgICAgICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc3RvcFVwZGF0aW5nKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGtpY2tCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2ZpZWxkIG14X1VzZXJJbmZvX2Rlc3RydWN0aXZlXCIgb25DbGljaz17X29uS2lja30+XHJcbiAgICAgICAgICAgICAgICB7IGlzSW52aXRlZCA/IF90KCdEaXNpbnZpdGUnKSA6IF90KCdSZW1vdmUgZnJvbSBjb21tdW5pdHknKSB9XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvLyBObyBtYWtlL3Jldm9rZSBhZG1pbiBBUEkgeWV0XHJcbiAgICAgICAgLypjb25zdCBvcExhYmVsID0gdGhpcy5zdGF0ZS5pc1RhcmdldE1vZCA/IF90KFwiUmV2b2tlIE1vZGVyYXRvclwiKSA6IF90KFwiTWFrZSBNb2RlcmF0b3JcIik7XHJcbiAgICAgICAgZ2l2ZU1vZEJ1dHRvbiA9IDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2ZpZWxkXCIgb25DbGljaz17dGhpcy5vbk1vZFRvZ2dsZX0+XHJcbiAgICAgICAgICAgIHtnaXZlT3BMYWJlbH1cclxuICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+OyovXHJcblxyXG4gICAgICAgIHJldHVybiA8R2VuZXJpY0FkbWluVG9vbHNDb250YWluZXI+XHJcbiAgICAgICAgICAgIHsga2lja0J1dHRvbiB9XHJcbiAgICAgICAgICAgIHsgY2hpbGRyZW4gfVxyXG4gICAgICAgIDwvR2VuZXJpY0FkbWluVG9vbHNDb250YWluZXI+O1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiA8ZGl2IC8+O1xyXG59O1xyXG5cclxuY29uc3QgR3JvdXBNZW1iZXIgPSBQcm9wVHlwZXMuc2hhcGUoe1xyXG4gICAgdXNlcklkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICBkaXNwbGF5bmFtZTogUHJvcFR5cGVzLnN0cmluZywgLy8gWFhYOiBHcm91cE1lbWJlciBvYmplY3RzIGFyZSBpbmNvbnNpc3RlbnQgOigoXHJcbiAgICBhdmF0YXJVcmw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbn0pO1xyXG5cclxuY29uc3QgdXNlSXNTeW5hcHNlQWRtaW4gPSAoY2xpKSA9PiB7XHJcbiAgICBjb25zdCBbaXNBZG1pbiwgc2V0SXNBZG1pbl0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIGNsaS5pc1N5bmFwc2VBZG1pbmlzdHJhdG9yKCkudGhlbigoaXNBZG1pbikgPT4ge1xyXG4gICAgICAgICAgICBzZXRJc0FkbWluKGlzQWRtaW4pO1xyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgc2V0SXNBZG1pbihmYWxzZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LCBbY2xpXSk7XHJcbiAgICByZXR1cm4gaXNBZG1pbjtcclxufTtcclxuXHJcbmNvbnN0IHVzZUhvbWVzZXJ2ZXJTdXBwb3J0c0Nyb3NzU2lnbmluZyA9IChjbGkpID0+IHtcclxuICAgIHJldHVybiB1c2VBc3luY01lbW8oYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBjbGkuZG9lc1NlcnZlclN1cHBvcnRVbnN0YWJsZUZlYXR1cmUoXCJvcmcubWF0cml4LmUyZV9jcm9zc19zaWduaW5nXCIpO1xyXG4gICAgfSwgW2NsaV0sIGZhbHNlKTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIHVzZVJvb21QZXJtaXNzaW9ucyhjbGksIHJvb20sIHVzZXIpIHtcclxuICAgIGNvbnN0IFtyb29tUGVybWlzc2lvbnMsIHNldFJvb21QZXJtaXNzaW9uc10gPSB1c2VTdGF0ZSh7XHJcbiAgICAgICAgLy8gbW9kaWZ5TGV2ZWxNYXggaXMgdGhlIG1heCBQTCB3ZSBjYW4gc2V0IHRoaXMgdXNlciB0bywgdHlwaWNhbGx5IG1pbih0aGVpciBQTCwgb3VyIFBMKSAmJiBjYW5TZXRQTFxyXG4gICAgICAgIG1vZGlmeUxldmVsTWF4OiAtMSxcclxuICAgICAgICBjYW5FZGl0OiBmYWxzZSxcclxuICAgICAgICBjYW5JbnZpdGU6IGZhbHNlLFxyXG4gICAgfSk7XHJcbiAgICBjb25zdCB1cGRhdGVSb29tUGVybWlzc2lvbnMgPSB1c2VDYWxsYmFjaygoKSA9PiB7XHJcbiAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLnBvd2VyX2xldmVsc1wiLCBcIlwiKTtcclxuICAgICAgICBpZiAoIXBvd2VyTGV2ZWxFdmVudCkgcmV0dXJuO1xyXG4gICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxzID0gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAoIXBvd2VyTGV2ZWxzKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IG1lID0gcm9vbS5nZXRNZW1iZXIoY2xpLmdldFVzZXJJZCgpKTtcclxuICAgICAgICBpZiAoIW1lKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHRoZW0gPSB1c2VyO1xyXG4gICAgICAgIGNvbnN0IGlzTWUgPSBtZS51c2VySWQgPT09IHRoZW0udXNlcklkO1xyXG4gICAgICAgIGNvbnN0IGNhbkFmZmVjdFVzZXIgPSB0aGVtLnBvd2VyTGV2ZWwgPCBtZS5wb3dlckxldmVsIHx8IGlzTWU7XHJcblxyXG4gICAgICAgIGxldCBtb2RpZnlMZXZlbE1heCA9IC0xO1xyXG4gICAgICAgIGlmIChjYW5BZmZlY3RVc2VyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGVkaXRQb3dlckxldmVsID0gKFxyXG4gICAgICAgICAgICAgICAgKHBvd2VyTGV2ZWxzLmV2ZW50cyA/IHBvd2VyTGV2ZWxzLmV2ZW50c1tcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIl0gOiBudWxsKSB8fFxyXG4gICAgICAgICAgICAgICAgcG93ZXJMZXZlbHMuc3RhdGVfZGVmYXVsdFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBpZiAobWUucG93ZXJMZXZlbCA+PSBlZGl0UG93ZXJMZXZlbCAmJiAoaXNNZSB8fCBtZS5wb3dlckxldmVsID4gdGhlbS5wb3dlckxldmVsKSkge1xyXG4gICAgICAgICAgICAgICAgbW9kaWZ5TGV2ZWxNYXggPSBtZS5wb3dlckxldmVsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXRSb29tUGVybWlzc2lvbnMoe1xyXG4gICAgICAgICAgICBjYW5JbnZpdGU6IG1lLnBvd2VyTGV2ZWwgPj0gcG93ZXJMZXZlbHMuaW52aXRlLFxyXG4gICAgICAgICAgICBjYW5FZGl0OiBtb2RpZnlMZXZlbE1heCA+PSAwLFxyXG4gICAgICAgICAgICBtb2RpZnlMZXZlbE1heCxcclxuICAgICAgICB9KTtcclxuICAgIH0sIFtjbGksIHVzZXIsIHJvb21dKTtcclxuICAgIHVzZUV2ZW50RW1pdHRlcihjbGksIFwiUm9vbVN0YXRlLm1lbWJlcnNcIiwgdXBkYXRlUm9vbVBlcm1pc3Npb25zKTtcclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgICAgdXBkYXRlUm9vbVBlcm1pc3Npb25zKCk7XHJcbiAgICAgICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgICAgICAgc2V0Um9vbVBlcm1pc3Npb25zKHtcclxuICAgICAgICAgICAgICAgIG1heGltYWxQb3dlckxldmVsOiAtMSxcclxuICAgICAgICAgICAgICAgIGNhbkVkaXQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY2FuSW52aXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH0sIFt1cGRhdGVSb29tUGVybWlzc2lvbnNdKTtcclxuXHJcbiAgICByZXR1cm4gcm9vbVBlcm1pc3Npb25zO1xyXG59XHJcblxyXG5jb25zdCBQb3dlckxldmVsU2VjdGlvbiA9ICh7dXNlciwgcm9vbSwgcm9vbVBlcm1pc3Npb25zLCBwb3dlckxldmVsc30pID0+IHtcclxuICAgIGNvbnN0IFtpc0VkaXRpbmcsIHNldEVkaXRpbmddID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gICAgaWYgKHJvb20gJiYgdXNlci5yb29tSWQpIHsgLy8gaXMgaW4gcm9vbVxyXG4gICAgICAgIGlmIChpc0VkaXRpbmcpIHtcclxuICAgICAgICAgICAgcmV0dXJuICg8UG93ZXJMZXZlbEVkaXRvclxyXG4gICAgICAgICAgICAgICAgdXNlcj17dXNlcn0gcm9vbT17cm9vbX0gcm9vbVBlcm1pc3Npb25zPXtyb29tUGVybWlzc2lvbnN9XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXsoKSA9PiBzZXRFZGl0aW5nKGZhbHNlKX0gLz4pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEljb25CdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5JY29uQnV0dG9uJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxVc2Vyc0RlZmF1bHQgPSBwb3dlckxldmVscy51c2Vyc19kZWZhdWx0IHx8IDA7XHJcbiAgICAgICAgICAgIGNvbnN0IHBvd2VyTGV2ZWwgPSBwYXJzZUludCh1c2VyLnBvd2VyTGV2ZWwsIDEwKTtcclxuICAgICAgICAgICAgY29uc3QgbW9kaWZ5QnV0dG9uID0gcm9vbVBlcm1pc3Npb25zLmNhbkVkaXQgP1xyXG4gICAgICAgICAgICAgICAgKDxJY29uQnV0dG9uIGljb249XCJlZGl0XCIgb25DbGljaz17KCkgPT4gc2V0RWRpdGluZyh0cnVlKX0gLz4pIDogbnVsbDtcclxuICAgICAgICAgICAgY29uc3Qgcm9sZSA9IHRleHR1YWxQb3dlckxldmVsKHBvd2VyTGV2ZWwsIHBvd2VyTGV2ZWxVc2Vyc0RlZmF1bHQpO1xyXG4gICAgICAgICAgICBjb25zdCBsYWJlbCA9IF90KFwiPHN0cm9uZz4lKHJvbGUpczwvc3Ryb25nPiBpbiAlKHJvb21OYW1lKXNcIixcclxuICAgICAgICAgICAgICAgIHtyb2xlLCByb29tTmFtZTogcm9vbS5uYW1lfSxcclxuICAgICAgICAgICAgICAgIHtzdHJvbmc6IGxhYmVsID0+IDxzdHJvbmc+e2xhYmVsfTwvc3Ryb25nPn0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX3Byb2ZpbGVGaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fcm9sZURlc2NyaXB0aW9uXCI+e2xhYmVsfXttb2RpZnlCdXR0b259PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG59O1xyXG5cclxuY29uc3QgUG93ZXJMZXZlbEVkaXRvciA9ICh7dXNlciwgcm9vbSwgcm9vbVBlcm1pc3Npb25zLCBvbkZpbmlzaGVkfSkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICBjb25zdCBbaXNVcGRhdGluZywgc2V0SXNVcGRhdGluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgICBjb25zdCBbc2VsZWN0ZWRQb3dlckxldmVsLCBzZXRTZWxlY3RlZFBvd2VyTGV2ZWxdID0gdXNlU3RhdGUocGFyc2VJbnQodXNlci5wb3dlckxldmVsLCAxMCkpO1xyXG4gICAgY29uc3QgW2lzRGlydHksIHNldElzRGlydHldID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gICAgY29uc3Qgb25Qb3dlckNoYW5nZSA9IHVzZUNhbGxiYWNrKChwb3dlckxldmVsKSA9PiB7XHJcbiAgICAgICAgc2V0SXNEaXJ0eSh0cnVlKTtcclxuICAgICAgICBzZXRTZWxlY3RlZFBvd2VyTGV2ZWwocGFyc2VJbnQocG93ZXJMZXZlbCwgMTApKTtcclxuICAgIH0sIFtzZXRTZWxlY3RlZFBvd2VyTGV2ZWwsIHNldElzRGlydHldKTtcclxuXHJcbiAgICBjb25zdCBjaGFuZ2VQb3dlckxldmVsID0gdXNlQ2FsbGJhY2soYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IF9hcHBseVBvd2VyQ2hhbmdlID0gKHJvb21JZCwgdGFyZ2V0LCBwb3dlckxldmVsLCBwb3dlckxldmVsRXZlbnQpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGNsaS5zZXRQb3dlckxldmVsKHJvb21JZCwgdGFyZ2V0LCBwYXJzZUludChwb3dlckxldmVsKSwgcG93ZXJMZXZlbEV2ZW50KS50aGVuKFxyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gTk8tT1A7IHJlbHkgb24gdGhlIG0ucm9vbS5tZW1iZXIgZXZlbnQgY29taW5nIGRvd24gZWxzZSB3ZSBjb3VsZFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGdldCBvdXQgb2Ygc3luYyBpZiB3ZSBmb3JjZSBzZXRTdGF0ZSBoZXJlIVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUG93ZXIgY2hhbmdlIHN1Y2Nlc3NcIik7XHJcbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gY2hhbmdlIHBvd2VyIGxldmVsIFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gY2hhbmdlIHBvd2VyIGxldmVsJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJGYWlsZWQgdG8gY2hhbmdlIHBvd2VyIGxldmVsXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAoIWlzRGlydHkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2V0SXNVcGRhdGluZyh0cnVlKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHBvd2VyTGV2ZWwgPSBzZWxlY3RlZFBvd2VyTGV2ZWw7XHJcblxyXG4gICAgICAgICAgICBjb25zdCByb29tSWQgPSB1c2VyLnJvb21JZDtcclxuICAgICAgICAgICAgY29uc3QgdGFyZ2V0ID0gdXNlci51c2VySWQ7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwb3dlckxldmVsRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIiwgXCJcIik7XHJcbiAgICAgICAgICAgIGlmICghcG93ZXJMZXZlbEV2ZW50KSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICBpZiAoIXBvd2VyTGV2ZWxFdmVudC5nZXRDb250ZW50KCkudXNlcnMpIHtcclxuICAgICAgICAgICAgICAgIF9hcHBseVBvd2VyQ2hhbmdlKHJvb21JZCwgdGFyZ2V0LCBwb3dlckxldmVsLCBwb3dlckxldmVsRXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBteVVzZXJJZCA9IGNsaS5nZXRVc2VySWQoKTtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuXHJcbiAgICAgICAgICAgIC8vIElmIHdlIGFyZSBjaGFuZ2luZyBvdXIgb3duIFBMIGl0IGNhbiBvbmx5IGV2ZXIgYmUgZGVjcmVhc2luZywgd2hpY2ggd2UgY2Fubm90IHJldmVyc2UuXHJcbiAgICAgICAgICAgIGlmIChteVVzZXJJZCA9PT0gdGFyZ2V0KSB7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKGF3YWl0IF93YXJuU2VsZkRlbW90ZSgpKSkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gd2FybiBhYm91dCBzZWxmIGRlbW90aW9uOiBcIiwgZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBfYXBwbHlQb3dlckNoYW5nZShyb29tSWQsIHRhcmdldCwgcG93ZXJMZXZlbCwgcG93ZXJMZXZlbEV2ZW50KTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgbXlQb3dlciA9IHBvd2VyTGV2ZWxFdmVudC5nZXRDb250ZW50KCkudXNlcnNbbXlVc2VySWRdO1xyXG4gICAgICAgICAgICBpZiAocGFyc2VJbnQobXlQb3dlcikgPT09IHBhcnNlSW50KHBvd2VyTGV2ZWwpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB7ZmluaXNoZWR9ID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnUHJvbW90ZSB0byBQTDEwMCBXYXJuaW5nJywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiV2FybmluZyFcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiWW91IHdpbGwgbm90IGJlIGFibGUgdG8gdW5kbyB0aGlzIGNoYW5nZSBhcyB5b3UgYXJlIHByb21vdGluZyB0aGUgdXNlciBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0byBoYXZlIHRoZSBzYW1lIHBvd2VyIGxldmVsIGFzIHlvdXJzZWxmLlwiKSB9PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiQXJlIHlvdSBzdXJlP1wiKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICAgICAgICAgICAgICBidXR0b246IF90KFwiQ29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBbY29uZmlybWVkXSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFjb25maXJtZWQpIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhd2FpdCBfYXBwbHlQb3dlckNoYW5nZShyb29tSWQsIHRhcmdldCwgcG93ZXJMZXZlbCwgcG93ZXJMZXZlbEV2ZW50KTtcclxuICAgICAgICB9IGZpbmFsbHkge1xyXG4gICAgICAgICAgICBvbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgW3VzZXIucm9vbUlkLCB1c2VyLnVzZXJJZCwgY2xpLCBzZWxlY3RlZFBvd2VyTGV2ZWwsIGlzRGlydHksIHNldElzVXBkYXRpbmcsIG9uRmluaXNoZWQsIHJvb21dKTtcclxuXHJcbiAgICBjb25zdCBwb3dlckxldmVsRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIiwgXCJcIik7XHJcbiAgICBjb25zdCBwb3dlckxldmVsVXNlcnNEZWZhdWx0ID0gcG93ZXJMZXZlbEV2ZW50ID8gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKS51c2Vyc19kZWZhdWx0IDogMDtcclxuICAgIGNvbnN0IEljb25CdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5JY29uQnV0dG9uJyk7XHJcbiAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICBjb25zdCBidXR0b25PclNwaW5uZXIgPSBpc1VwZGF0aW5nID8gPFNwaW5uZXIgdz17MTZ9IGg9ezE2fSAvPiA6XHJcbiAgICAgICAgPEljb25CdXR0b24gaWNvbj1cImNoZWNrXCIgb25DbGljaz17Y2hhbmdlUG93ZXJMZXZlbH0gLz47XHJcblxyXG4gICAgY29uc3QgUG93ZXJTZWxlY3RvciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlBvd2VyU2VsZWN0b3InKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19wcm9maWxlRmllbGRcIj5cclxuICAgICAgICAgICAgPFBvd2VyU2VsZWN0b3JcclxuICAgICAgICAgICAgICAgIGxhYmVsPXtudWxsfVxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e3NlbGVjdGVkUG93ZXJMZXZlbH1cclxuICAgICAgICAgICAgICAgIG1heFZhbHVlPXtyb29tUGVybWlzc2lvbnMubW9kaWZ5TGV2ZWxNYXh9XHJcbiAgICAgICAgICAgICAgICB1c2Vyc0RlZmF1bHQ9e3Bvd2VyTGV2ZWxVc2Vyc0RlZmF1bHR9XHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25Qb3dlckNoYW5nZX1cclxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXtpc1VwZGF0aW5nfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICB7YnV0dG9uT3JTcGlubmVyfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCB1c2VEZXZpY2VzID0gKHVzZXJJZCkgPT4ge1xyXG4gICAgY29uc3QgY2xpID0gdXNlQ29udGV4dChNYXRyaXhDbGllbnRDb250ZXh0KTtcclxuXHJcbiAgICAvLyB1bmRlZmluZWQgbWVhbnMgeWV0IHRvIGJlIGxvYWRlZCwgbnVsbCBtZWFucyBmYWlsZWQgdG8gbG9hZCwgb3RoZXJ3aXNlIGxpc3Qgb2YgZGV2aWNlc1xyXG4gICAgY29uc3QgW2RldmljZXMsIHNldERldmljZXNdID0gdXNlU3RhdGUodW5kZWZpbmVkKTtcclxuICAgIC8vIERvd25sb2FkIGRldmljZSBsaXN0c1xyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBzZXREZXZpY2VzKHVuZGVmaW5lZCk7XHJcblxyXG4gICAgICAgIGxldCBjYW5jZWxsZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgYXN5bmMgZnVuY3Rpb24gX2Rvd25sb2FkRGV2aWNlTGlzdCgpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGF3YWl0IGNsaS5kb3dubG9hZEtleXMoW3VzZXJJZF0sIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGV2aWNlcyA9IGF3YWl0IGNsaS5nZXRTdG9yZWREZXZpY2VzRm9yVXNlcih1c2VySWQpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChjYW5jZWxsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyB3ZSBnb3QgY2FuY2VsbGVkIC0gcHJlc3VtYWJseSBhIGRpZmZlcmVudCB1c2VyIG5vd1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBfZGlzYW1iaWd1YXRlRGV2aWNlcyhkZXZpY2VzKTtcclxuICAgICAgICAgICAgICAgIHNldERldmljZXMoZGV2aWNlcyk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgc2V0RGV2aWNlcyhudWxsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBfZG93bmxvYWREZXZpY2VMaXN0KCk7XHJcblxyXG4gICAgICAgIC8vIEhhbmRsZSBiZWluZyB1bm1vdW50ZWRcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICBjYW5jZWxsZWQgPSB0cnVlO1xyXG4gICAgICAgIH07XHJcbiAgICB9LCBbY2xpLCB1c2VySWRdKTtcclxuXHJcbiAgICAvLyBMaXN0ZW4gdG8gY2hhbmdlc1xyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBsZXQgY2FuY2VsID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgdXBkYXRlRGV2aWNlcyA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbmV3RGV2aWNlcyA9IGF3YWl0IGNsaS5nZXRTdG9yZWREZXZpY2VzRm9yVXNlcih1c2VySWQpO1xyXG4gICAgICAgICAgICBpZiAoY2FuY2VsKSByZXR1cm47XHJcbiAgICAgICAgICAgIHNldERldmljZXMobmV3RGV2aWNlcyk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBvbkRldmljZXNVcGRhdGVkID0gKHVzZXJzKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdXNlcnMuaW5jbHVkZXModXNlcklkKSkgcmV0dXJuO1xyXG4gICAgICAgICAgICB1cGRhdGVEZXZpY2VzKCk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBvbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQgPSAoX3VzZXJJZCwgZGV2aWNlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChfdXNlcklkICE9PSB1c2VySWQpIHJldHVybjtcclxuICAgICAgICAgICAgdXBkYXRlRGV2aWNlcygpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3Qgb25Vc2VyVHJ1c3RTdGF0dXNDaGFuZ2VkID0gKF91c2VySWQsIHRydXN0U3RhdHVzKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChfdXNlcklkICE9PSB1c2VySWQpIHJldHVybjtcclxuICAgICAgICAgICAgdXBkYXRlRGV2aWNlcygpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgY2xpLm9uKFwiY3J5cHRvLmRldmljZXNVcGRhdGVkXCIsIG9uRGV2aWNlc1VwZGF0ZWQpO1xyXG4gICAgICAgIGNsaS5vbihcImRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWRcIiwgb25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkKTtcclxuICAgICAgICBjbGkub24oXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIG9uVXNlclRydXN0U3RhdHVzQ2hhbmdlZCk7XHJcbiAgICAgICAgLy8gSGFuZGxlIGJlaW5nIHVubW91bnRlZFxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIGNsaS5yZW1vdmVMaXN0ZW5lcihcImNyeXB0by5kZXZpY2VzVXBkYXRlZFwiLCBvbkRldmljZXNVcGRhdGVkKTtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiZGV2aWNlVmVyaWZpY2F0aW9uQ2hhbmdlZFwiLCBvbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIG9uVXNlclRydXN0U3RhdHVzQ2hhbmdlZCk7XHJcbiAgICAgICAgfTtcclxuICAgIH0sIFtjbGksIHVzZXJJZF0pO1xyXG5cclxuICAgIHJldHVybiBkZXZpY2VzO1xyXG59O1xyXG5cclxuY29uc3QgQmFzaWNVc2VySW5mbyA9ICh7cm9vbSwgbWVtYmVyLCBncm91cElkLCBkZXZpY2VzLCBpc1Jvb21FbmNyeXB0ZWR9KSA9PiB7XHJcbiAgICBjb25zdCBjbGkgPSB1c2VDb250ZXh0KE1hdHJpeENsaWVudENvbnRleHQpO1xyXG5cclxuICAgIGNvbnN0IHBvd2VyTGV2ZWxzID0gdXNlUm9vbVBvd2VyTGV2ZWxzKGNsaSwgcm9vbSk7XHJcbiAgICAvLyBMb2FkIHdoZXRoZXIgb3Igbm90IHdlIGFyZSBhIFN5bmFwc2UgQWRtaW5cclxuICAgIGNvbnN0IGlzU3luYXBzZUFkbWluID0gdXNlSXNTeW5hcHNlQWRtaW4oY2xpKTtcclxuXHJcbiAgICAvLyBDaGVjayB3aGV0aGVyIHRoZSB1c2VyIGlzIGlnbm9yZWRcclxuICAgIGNvbnN0IFtpc0lnbm9yZWQsIHNldElzSWdub3JlZF0gPSB1c2VTdGF0ZShjbGkuaXNVc2VySWdub3JlZChtZW1iZXIudXNlcklkKSk7XHJcbiAgICAvLyBSZWNoZWNrIGlmIHRoZSB1c2VyIG9yIGNsaWVudCBjaGFuZ2VzXHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIHNldElzSWdub3JlZChjbGkuaXNVc2VySWdub3JlZChtZW1iZXIudXNlcklkKSk7XHJcbiAgICB9LCBbY2xpLCBtZW1iZXIudXNlcklkXSk7XHJcbiAgICAvLyBSZWNoZWNrIGFsc28gaWYgd2UgcmVjZWl2ZSBuZXcgYWNjb3VudERhdGEgbS5pZ25vcmVkX3VzZXJfbGlzdFxyXG4gICAgY29uc3QgYWNjb3VudERhdGFIYW5kbGVyID0gdXNlQ2FsbGJhY2soKGV2KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2LmdldFR5cGUoKSA9PT0gXCJtLmlnbm9yZWRfdXNlcl9saXN0XCIpIHtcclxuICAgICAgICAgICAgc2V0SXNJZ25vcmVkKGNsaS5pc1VzZXJJZ25vcmVkKG1lbWJlci51c2VySWQpKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbY2xpLCBtZW1iZXIudXNlcklkXSk7XHJcbiAgICB1c2VFdmVudEVtaXR0ZXIoY2xpLCBcImFjY291bnREYXRhXCIsIGFjY291bnREYXRhSGFuZGxlcik7XHJcblxyXG4gICAgLy8gQ291bnQgb2YgaG93IG1hbnkgb3BlcmF0aW9ucyBhcmUgY3VycmVudGx5IGluIHByb2dyZXNzLCBpZiA+IDAgdGhlbiBzaG93IGEgU3Bpbm5lclxyXG4gICAgY29uc3QgW3BlbmRpbmdVcGRhdGVDb3VudCwgc2V0UGVuZGluZ1VwZGF0ZUNvdW50XSA9IHVzZVN0YXRlKDApO1xyXG4gICAgY29uc3Qgc3RhcnRVcGRhdGluZyA9IHVzZUNhbGxiYWNrKCgpID0+IHtcclxuICAgICAgICBzZXRQZW5kaW5nVXBkYXRlQ291bnQocGVuZGluZ1VwZGF0ZUNvdW50ICsgMSk7XHJcbiAgICB9LCBbcGVuZGluZ1VwZGF0ZUNvdW50XSk7XHJcbiAgICBjb25zdCBzdG9wVXBkYXRpbmcgPSB1c2VDYWxsYmFjaygoKSA9PiB7XHJcbiAgICAgICAgc2V0UGVuZGluZ1VwZGF0ZUNvdW50KHBlbmRpbmdVcGRhdGVDb3VudCAtIDEpO1xyXG4gICAgfSwgW3BlbmRpbmdVcGRhdGVDb3VudF0pO1xyXG5cclxuICAgIGNvbnN0IHJvb21QZXJtaXNzaW9ucyA9IHVzZVJvb21QZXJtaXNzaW9ucyhjbGksIHJvb20sIG1lbWJlcik7XHJcblxyXG4gICAgY29uc3Qgb25TeW5hcHNlRGVhY3RpdmF0ZSA9IHVzZUNhbGxiYWNrKGFzeW5jICgpID0+IHtcclxuICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuUXVlc3Rpb25EaWFsb2cnKTtcclxuICAgICAgICBjb25zdCB7ZmluaXNoZWR9ID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU3luYXBzZSBVc2VyIERlYWN0aXZhdGlvbicsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoXCJEZWFjdGl2YXRlIHVzZXI/XCIpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgIDxkaXY+eyBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkRlYWN0aXZhdGluZyB0aGlzIHVzZXIgd2lsbCBsb2cgdGhlbSBvdXQgYW5kIHByZXZlbnQgdGhlbSBmcm9tIGxvZ2dpbmcgYmFjayBpbi4gQWRkaXRpb25hbGx5LCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0aGV5IHdpbGwgbGVhdmUgYWxsIHRoZSByb29tcyB0aGV5IGFyZSBpbi4gVGhpcyBhY3Rpb24gY2Fubm90IGJlIHJldmVyc2VkLiBBcmUgeW91IHN1cmUgeW91IFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIndhbnQgdG8gZGVhY3RpdmF0ZSB0aGlzIHVzZXI/XCIsXHJcbiAgICAgICAgICAgICAgICApIH08L2Rpdj4sXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJEZWFjdGl2YXRlIHVzZXJcIiksXHJcbiAgICAgICAgICAgIGRhbmdlcjogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgW2FjY2VwdGVkXSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgIGlmICghYWNjZXB0ZWQpIHJldHVybjtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBhd2FpdCBjbGkuZGVhY3RpdmF0ZVN5bmFwc2VVc2VyKG1lbWJlci51c2VySWQpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIGRlYWN0aXZhdGUgdXNlclwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLkVycm9yRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBkZWFjdGl2YXRlIFN5bmFwc2UgdXNlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gZGVhY3RpdmF0ZSB1c2VyJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbY2xpLCBtZW1iZXIudXNlcklkXSk7XHJcblxyXG4gICAgbGV0IHN5bmFwc2VEZWFjdGl2YXRlQnV0dG9uO1xyXG4gICAgbGV0IHNwaW5uZXI7XHJcblxyXG4gICAgLy8gV2UgZG9uJ3QgbmVlZCBhIHBlcmZlY3QgY2hlY2sgaGVyZSwganVzdCBzb21ldGhpbmcgdG8gcGFzcyBhcyBcInByb2JhYmx5IG5vdCBvdXIgaG9tZXNlcnZlclwiLiBJZlxyXG4gICAgLy8gc29tZW9uZSBkb2VzIGZpZ3VyZSBvdXQgaG93IHRvIGJ5cGFzcyB0aGlzIGNoZWNrIHRoZSB3b3JzdCB0aGF0IGhhcHBlbnMgaXMgYW4gZXJyb3IuXHJcbiAgICAvLyBGSVhNRSB0aGlzIHNob3VsZCBiZSB1c2luZyBjbGkgaW5zdGVhZCBvZiBNYXRyaXhDbGllbnRQZWcubWF0cml4Q2xpZW50XHJcbiAgICBpZiAoaXNTeW5hcHNlQWRtaW4gJiYgbWVtYmVyLnVzZXJJZC5lbmRzV2l0aChgOiR7TWF0cml4Q2xpZW50UGVnLmdldEhvbWVzZXJ2ZXJOYW1lKCl9YCkpIHtcclxuICAgICAgICBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17b25TeW5hcHNlRGVhY3RpdmF0ZX0gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGQgbXhfVXNlckluZm9fZGVzdHJ1Y3RpdmVcIj5cclxuICAgICAgICAgICAgICAgIHtfdChcIkRlYWN0aXZhdGUgdXNlclwiKX1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGFkbWluVG9vbHNDb250YWluZXI7XHJcbiAgICBpZiAocm9vbSAmJiBtZW1iZXIucm9vbUlkKSB7XHJcbiAgICAgICAgYWRtaW5Ub29sc0NvbnRhaW5lciA9IChcclxuICAgICAgICAgICAgPFJvb21BZG1pblRvb2xzQ29udGFpbmVyXHJcbiAgICAgICAgICAgICAgICBwb3dlckxldmVscz17cG93ZXJMZXZlbHN9XHJcbiAgICAgICAgICAgICAgICBtZW1iZXI9e21lbWJlcn1cclxuICAgICAgICAgICAgICAgIHJvb209e3Jvb219XHJcbiAgICAgICAgICAgICAgICBzdGFydFVwZGF0aW5nPXtzdGFydFVwZGF0aW5nfVxyXG4gICAgICAgICAgICAgICAgc3RvcFVwZGF0aW5nPXtzdG9wVXBkYXRpbmd9PlxyXG4gICAgICAgICAgICAgICAgeyBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgIDwvUm9vbUFkbWluVG9vbHNDb250YWluZXI+XHJcbiAgICAgICAgKTtcclxuICAgIH0gZWxzZSBpZiAoZ3JvdXBJZCkge1xyXG4gICAgICAgIGFkbWluVG9vbHNDb250YWluZXIgPSAoXHJcbiAgICAgICAgICAgIDxHcm91cEFkbWluVG9vbHNTZWN0aW9uXHJcbiAgICAgICAgICAgICAgICBncm91cElkPXtncm91cElkfVxyXG4gICAgICAgICAgICAgICAgZ3JvdXBNZW1iZXI9e21lbWJlcn1cclxuICAgICAgICAgICAgICAgIHN0YXJ0VXBkYXRpbmc9e3N0YXJ0VXBkYXRpbmd9XHJcbiAgICAgICAgICAgICAgICBzdG9wVXBkYXRpbmc9e3N0b3BVcGRhdGluZ30+XHJcbiAgICAgICAgICAgICAgICB7IHN5bmFwc2VEZWFjdGl2YXRlQnV0dG9uIH1cclxuICAgICAgICAgICAgPC9Hcm91cEFkbWluVG9vbHNTZWN0aW9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9IGVsc2UgaWYgKHN5bmFwc2VEZWFjdGl2YXRlQnV0dG9uKSB7XHJcbiAgICAgICAgYWRtaW5Ub29sc0NvbnRhaW5lciA9IChcclxuICAgICAgICAgICAgPEdlbmVyaWNBZG1pblRvb2xzQ29udGFpbmVyPlxyXG4gICAgICAgICAgICAgICAgeyBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgIDwvR2VuZXJpY0FkbWluVG9vbHNDb250YWluZXI+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAocGVuZGluZ1VwZGF0ZUNvdW50ID4gMCkge1xyXG4gICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgIHNwaW5uZXIgPSA8TG9hZGVyIGltZ0NsYXNzTmFtZT1cIm14X0NvbnRleHR1YWxNZW51X3NwaW5uZXJcIiAvPjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBtZW1iZXJEZXRhaWxzID0gKFxyXG4gICAgICAgIDxQb3dlckxldmVsU2VjdGlvblxyXG4gICAgICAgICAgICBwb3dlckxldmVscz17cG93ZXJMZXZlbHN9XHJcbiAgICAgICAgICAgIHVzZXI9e21lbWJlcn1cclxuICAgICAgICAgICAgcm9vbT17cm9vbX1cclxuICAgICAgICAgICAgcm9vbVBlcm1pc3Npb25zPXtyb29tUGVybWlzc2lvbnN9XHJcbiAgICAgICAgLz5cclxuICAgICk7XHJcblxyXG4gICAgLy8gb25seSBkaXNwbGF5IHRoZSBkZXZpY2VzIGxpc3QgaWYgb3VyIGNsaWVudCBzdXBwb3J0cyBFMkVcclxuICAgIGNvbnN0IF9lbmFibGVEZXZpY2VzID0gY2xpLmlzQ3J5cHRvRW5hYmxlZCgpO1xyXG5cclxuICAgIGxldCB0ZXh0O1xyXG4gICAgaWYgKCFpc1Jvb21FbmNyeXB0ZWQpIHtcclxuICAgICAgICBpZiAoIV9lbmFibGVEZXZpY2VzKSB7XHJcbiAgICAgICAgICAgIHRleHQgPSBfdChcIlRoaXMgY2xpZW50IGRvZXMgbm90IHN1cHBvcnQgZW5kLXRvLWVuZCBlbmNyeXB0aW9uLlwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHJvb20pIHtcclxuICAgICAgICAgICAgdGV4dCA9IF90KFwiTWVzc2FnZXMgaW4gdGhpcyByb29tIGFyZSBub3QgZW5kLXRvLWVuZCBlbmNyeXB0ZWQuXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIFRPRE8gd2hhdCB0byByZW5kZXIgZm9yIEdyb3VwTWVtYmVyXHJcbiAgICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0ZXh0ID0gX3QoXCJNZXNzYWdlcyBpbiB0aGlzIHJvb20gYXJlIGVuZC10by1lbmQgZW5jcnlwdGVkLlwiKTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgdmVyaWZ5QnV0dG9uO1xyXG4gICAgY29uc3QgaG9tZXNlcnZlclN1cHBvcnRzQ3Jvc3NTaWduaW5nID0gdXNlSG9tZXNlcnZlclN1cHBvcnRzQ3Jvc3NTaWduaW5nKGNsaSk7XHJcblxyXG4gICAgY29uc3QgdXNlclRydXN0ID0gY2xpLmNoZWNrVXNlclRydXN0KG1lbWJlci51c2VySWQpO1xyXG4gICAgY29uc3QgdXNlclZlcmlmaWVkID0gdXNlclRydXN0LmlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQoKTtcclxuICAgIGNvbnN0IGlzTWUgPSBtZW1iZXIudXNlcklkID09PSBjbGkuZ2V0VXNlcklkKCk7XHJcbiAgICBjb25zdCBjYW5WZXJpZnkgPSBTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgaG9tZXNlcnZlclN1cHBvcnRzQ3Jvc3NTaWduaW5nICYmICF1c2VyVmVyaWZpZWQgJiYgIWlzTWU7XHJcblxyXG4gICAgY29uc3Qgc2V0VXBkYXRpbmcgPSAodXBkYXRpbmcpID0+IHtcclxuICAgICAgICBzZXRQZW5kaW5nVXBkYXRlQ291bnQoY291bnQgPT4gY291bnQgKyAodXBkYXRpbmcgPyAxIDogLTEpKTtcclxuICAgIH07XHJcbiAgICBjb25zdCBoYXNDcm9zc1NpZ25pbmdLZXlzID1cclxuICAgICAgICB1c2VIYXNDcm9zc1NpZ25pbmdLZXlzKGNsaSwgbWVtYmVyLCBjYW5WZXJpZnksIHNldFVwZGF0aW5nICk7XHJcblxyXG4gICAgaWYgKGNhblZlcmlmeSkge1xyXG4gICAgICAgIHZlcmlmeUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfVXNlckluZm9fZmllbGRcIiBvbkNsaWNrPXsoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaGFzQ3Jvc3NTaWduaW5nS2V5cykge1xyXG4gICAgICAgICAgICAgICAgICAgIHZlcmlmeVVzZXIobWVtYmVyKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVnYWN5VmVyaWZ5VXNlcihtZW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9fT5cclxuICAgICAgICAgICAgICAgIHtfdChcIlZlcmlmeVwiKX1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3Qgc2VjdXJpdHlTZWN0aW9uID0gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgIDxoMz57IF90KFwiU2VjdXJpdHlcIikgfTwvaDM+XHJcbiAgICAgICAgICAgIDxwPnsgdGV4dCB9PC9wPlxyXG4gICAgICAgICAgICB7IHZlcmlmeUJ1dHRvbiB9XHJcbiAgICAgICAgICAgIDxEZXZpY2VzU2VjdGlvblxyXG4gICAgICAgICAgICAgICAgbG9hZGluZz17ZGV2aWNlcyA9PT0gdW5kZWZpbmVkfVxyXG4gICAgICAgICAgICAgICAgZGV2aWNlcz17ZGV2aWNlc31cclxuICAgICAgICAgICAgICAgIHVzZXJJZD17bWVtYmVyLnVzZXJJZH0gLz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICk7XHJcblxyXG4gICAgcmV0dXJuIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgICB7IG1lbWJlckRldGFpbHMgJiZcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2NvbnRhaW5lciBteF9Vc2VySW5mb19zZXBhcmF0b3IgbXhfVXNlckluZm9fbWVtYmVyRGV0YWlsc0NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX21lbWJlckRldGFpbHNcIj5cclxuICAgICAgICAgICAgICAgIHsgbWVtYmVyRGV0YWlscyB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PiB9XHJcblxyXG4gICAgICAgIHsgc2VjdXJpdHlTZWN0aW9uIH1cclxuICAgICAgICA8VXNlck9wdGlvbnNTZWN0aW9uXHJcbiAgICAgICAgICAgIGRldmljZXM9e2RldmljZXN9XHJcbiAgICAgICAgICAgIGNhbkludml0ZT17cm9vbVBlcm1pc3Npb25zLmNhbkludml0ZX1cclxuICAgICAgICAgICAgaXNJZ25vcmVkPXtpc0lnbm9yZWR9XHJcbiAgICAgICAgICAgIG1lbWJlcj17bWVtYmVyfSAvPlxyXG5cclxuICAgICAgICB7IGFkbWluVG9vbHNDb250YWluZXIgfVxyXG5cclxuICAgICAgICB7IHNwaW5uZXIgfVxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD47XHJcbn07XHJcblxyXG5jb25zdCBVc2VySW5mb0hlYWRlciA9ICh7b25DbG9zZSwgbWVtYmVyLCBlMmVTdGF0dXN9KSA9PiB7XHJcbiAgICBjb25zdCBjbGkgPSB1c2VDb250ZXh0KE1hdHJpeENsaWVudENvbnRleHQpO1xyXG5cclxuICAgIGxldCBjbG9zZUJ1dHRvbjtcclxuICAgIGlmIChvbkNsb3NlKSB7XHJcbiAgICAgICAgY2xvc2VCdXR0b24gPSA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19jYW5jZWxcIiBvbkNsaWNrPXtvbkNsb3NlfSB0aXRsZT17X3QoJ0Nsb3NlJyl9PlxyXG4gICAgICAgICAgICA8ZGl2IC8+XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBvbk1lbWJlckF2YXRhckNsaWNrID0gdXNlQ2FsbGJhY2soKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGF2YXRhclVybCA9IG1lbWJlci5nZXRNeGNBdmF0YXJVcmwgPyBtZW1iZXIuZ2V0TXhjQXZhdGFyVXJsKCkgOiBtZW1iZXIuYXZhdGFyVXJsO1xyXG4gICAgICAgIGlmICghYXZhdGFyVXJsKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IGh0dHBVcmwgPSBjbGkubXhjVXJsVG9IdHRwKGF2YXRhclVybCk7XHJcbiAgICAgICAgY29uc3QgSW1hZ2VWaWV3ID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkltYWdlVmlld1wiKTtcclxuICAgICAgICBjb25zdCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgIHNyYzogaHR0cFVybCxcclxuICAgICAgICAgICAgbmFtZTogbWVtYmVyLm5hbWUsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlRGlhbG9nKEltYWdlVmlldywgcGFyYW1zLCBcIm14X0RpYWxvZ19saWdodGJveFwiKTtcclxuICAgIH0sIFtjbGksIG1lbWJlcl0pO1xyXG5cclxuICAgIGNvbnN0IE1lbWJlckF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoJ2F2YXRhcnMuTWVtYmVyQXZhdGFyJyk7XHJcbiAgICBjb25zdCBhdmF0YXJFbGVtZW50ID0gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fYXZhdGFyXCI+XHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxNZW1iZXJBdmF0YXJcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PXttZW1iZXIudXNlcklkfSAvLyB0byBpbnN0YW50bHkgYmxhbmsgdGhlIGF2YXRhciB3aGVuIFVzZXJJbmZvIGNoYW5nZXMgbWVtYmVyc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZW1iZXI9e21lbWJlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezIgKiAwLjMgKiB3aW5kb3cuaW5uZXJIZWlnaHR9IC8vIDJ4QDMwdmhcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PXsyICogMC4zICogd2luZG93LmlubmVySGVpZ2h0fSAvLyAyeEAzMHZoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc2l6ZU1ldGhvZD1cInNjYWxlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmFsbGJhY2tVc2VySWQ9e21lbWJlci51c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e29uTWVtYmVyQXZhdGFyQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybHM9e21lbWJlci5hdmF0YXJVcmwgPyBbbWVtYmVyLmF2YXRhclVybF0gOiB1bmRlZmluZWR9IC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG5cclxuICAgIGxldCBwcmVzZW5jZVN0YXRlO1xyXG4gICAgbGV0IHByZXNlbmNlTGFzdEFjdGl2ZUFnbztcclxuICAgIGxldCBwcmVzZW5jZUN1cnJlbnRseUFjdGl2ZTtcclxuICAgIGxldCBzdGF0dXNNZXNzYWdlO1xyXG5cclxuICAgIGlmIChtZW1iZXIgaW5zdGFuY2VvZiBSb29tTWVtYmVyICYmIG1lbWJlci51c2VyKSB7XHJcbiAgICAgICAgcHJlc2VuY2VTdGF0ZSA9IG1lbWJlci51c2VyLnByZXNlbmNlO1xyXG4gICAgICAgIHByZXNlbmNlTGFzdEFjdGl2ZUFnbyA9IG1lbWJlci51c2VyLmxhc3RBY3RpdmVBZ287XHJcbiAgICAgICAgcHJlc2VuY2VDdXJyZW50bHlBY3RpdmUgPSBtZW1iZXIudXNlci5jdXJyZW50bHlBY3RpdmU7XHJcblxyXG4gICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2N1c3RvbV9zdGF0dXNcIikpIHtcclxuICAgICAgICAgICAgc3RhdHVzTWVzc2FnZSA9IG1lbWJlci51c2VyLl91bnN0YWJsZV9zdGF0dXNNZXNzYWdlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBlbmFibGVQcmVzZW5jZUJ5SHNVcmwgPSBTZGtDb25maWcuZ2V0KClbXCJlbmFibGVfcHJlc2VuY2VfYnlfaHNfdXJsXCJdO1xyXG4gICAgbGV0IHNob3dQcmVzZW5jZSA9IHRydWU7XHJcbiAgICBpZiAoZW5hYmxlUHJlc2VuY2VCeUhzVXJsICYmIGVuYWJsZVByZXNlbmNlQnlIc1VybFtjbGkuYmFzZVVybF0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHNob3dQcmVzZW5jZSA9IGVuYWJsZVByZXNlbmNlQnlIc1VybFtjbGkuYmFzZVVybF07XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHByZXNlbmNlTGFiZWwgPSBudWxsO1xyXG4gICAgaWYgKHNob3dQcmVzZW5jZSkge1xyXG4gICAgICAgIGNvbnN0IFByZXNlbmNlTGFiZWwgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5QcmVzZW5jZUxhYmVsJyk7XHJcbiAgICAgICAgcHJlc2VuY2VMYWJlbCA9IDxQcmVzZW5jZUxhYmVsIGFjdGl2ZUFnbz17cHJlc2VuY2VMYXN0QWN0aXZlQWdvfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50bHlBY3RpdmU9e3ByZXNlbmNlQ3VycmVudGx5QWN0aXZlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVzZW5jZVN0YXRlPXtwcmVzZW5jZVN0YXRlfSAvPjtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgc3RhdHVzTGFiZWwgPSBudWxsO1xyXG4gICAgaWYgKHN0YXR1c01lc3NhZ2UpIHtcclxuICAgICAgICBzdGF0dXNMYWJlbCA9IDxzcGFuIGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX3N0YXR1c01lc3NhZ2VcIj57IHN0YXR1c01lc3NhZ2UgfTwvc3Bhbj47XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGUyZUljb247XHJcbiAgICBpZiAoZTJlU3RhdHVzKSB7XHJcbiAgICAgICAgZTJlSWNvbiA9IDxFMkVJY29uIHNpemU9ezE4fSBzdGF0dXM9e2UyZVN0YXR1c30gaXNVc2VyPXt0cnVlfSAvPjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBkaXNwbGF5TmFtZSA9IG1lbWJlci5uYW1lIHx8IG1lbWJlci5kaXNwbGF5bmFtZTtcclxuICAgIHJldHVybiA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgeyBjbG9zZUJ1dHRvbiB9XHJcbiAgICAgICAgeyBhdmF0YXJFbGVtZW50IH1cclxuXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19jb250YWluZXIgbXhfVXNlckluZm9fc2VwYXJhdG9yXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fcHJvZmlsZVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgZTJlSWNvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHRpdGxlPXtkaXNwbGF5TmFtZX0gYXJpYS1sYWJlbD17ZGlzcGxheU5hbWV9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBkaXNwbGF5TmFtZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PnsgbWVtYmVyLnVzZXJJZCB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX3Byb2ZpbGVTdGF0dXNcIj5cclxuICAgICAgICAgICAgICAgICAgICB7cHJlc2VuY2VMYWJlbH1cclxuICAgICAgICAgICAgICAgICAgICB7c3RhdHVzTGFiZWx9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxufTtcclxuXHJcbmNvbnN0IFVzZXJJbmZvID0gKHt1c2VyLCBncm91cElkLCByb29tSWQsIG9uQ2xvc2UsIHBoYXNlPVJJR0hUX1BBTkVMX1BIQVNFUy5Sb29tTWVtYmVySW5mbywgLi4ucHJvcHN9KSA9PiB7XHJcbiAgICBjb25zdCBjbGkgPSB1c2VDb250ZXh0KE1hdHJpeENsaWVudENvbnRleHQpO1xyXG5cclxuICAgIC8vIExvYWQgcm9vbSBpZiB3ZSBhcmUgZ2l2ZW4gYSByb29tIGlkIGFuZCBtZW1vaXplIGl0XHJcbiAgICBjb25zdCByb29tID0gdXNlTWVtbygoKSA9PiByb29tSWQgPyBjbGkuZ2V0Um9vbShyb29tSWQpIDogbnVsbCwgW2NsaSwgcm9vbUlkXSk7XHJcbiAgICAvLyBmZXRjaCBsYXRlc3Qgcm9vbSBtZW1iZXIgaWYgd2UgaGF2ZSBhIHJvb20sIHNvIHdlIGRvbid0IHNob3cgaGlzdG9yaWNhbCBpbmZvcm1hdGlvbiwgZmFsbGluZyBiYWNrIHRvIHVzZXJcclxuICAgIGNvbnN0IG1lbWJlciA9IHVzZU1lbW8oKCkgPT4gcm9vbSA/IChyb29tLmdldE1lbWJlcih1c2VyLnVzZXJJZCkgfHwgdXNlcikgOiB1c2VyLCBbcm9vbSwgdXNlcl0pO1xyXG5cclxuICAgIGNvbnN0IGlzUm9vbUVuY3J5cHRlZCA9IHVzZUlzRW5jcnlwdGVkKGNsaSwgcm9vbSk7XHJcbiAgICBjb25zdCBkZXZpY2VzID0gdXNlRGV2aWNlcyh1c2VyLnVzZXJJZCk7XHJcblxyXG4gICAgbGV0IGUyZVN0YXR1cztcclxuICAgIGlmIChpc1Jvb21FbmNyeXB0ZWQgJiYgZGV2aWNlcykge1xyXG4gICAgICAgIGUyZVN0YXR1cyA9IGdldEUyRVN0YXR1cyhjbGksIHVzZXIudXNlcklkLCBkZXZpY2VzKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBjbGFzc2VzID0gW1wibXhfVXNlckluZm9cIl07XHJcblxyXG4gICAgbGV0IGNvbnRlbnQ7XHJcbiAgICBzd2l0Y2ggKHBoYXNlKSB7XHJcbiAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuUm9vbU1lbWJlckluZm86XHJcbiAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuR3JvdXBNZW1iZXJJbmZvOlxyXG4gICAgICAgICAgICBjb250ZW50ID0gKFxyXG4gICAgICAgICAgICAgICAgPEJhc2ljVXNlckluZm9cclxuICAgICAgICAgICAgICAgICAgICByb29tPXtyb29tfVxyXG4gICAgICAgICAgICAgICAgICAgIG1lbWJlcj17bWVtYmVyfVxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwSWQ9e2dyb3VwSWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGV2aWNlcz17ZGV2aWNlc31cclxuICAgICAgICAgICAgICAgICAgICBpc1Jvb21FbmNyeXB0ZWQ9e2lzUm9vbUVuY3J5cHRlZH0gLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBSSUdIVF9QQU5FTF9QSEFTRVMuRW5jcnlwdGlvblBhbmVsOlxyXG4gICAgICAgICAgICBjbGFzc2VzLnB1c2goXCJteF9Vc2VySW5mb19zbWFsbEF2YXRhclwiKTtcclxuICAgICAgICAgICAgY29udGVudCA9IChcclxuICAgICAgICAgICAgICAgIDxFbmNyeXB0aW9uUGFuZWwgey4uLnByb3BzfSBtZW1iZXI9e21lbWJlcn0gb25DbG9zZT17b25DbG9zZX0gaXNSb29tRW5jcnlwdGVkPXtpc1Jvb21FbmNyeXB0ZWR9IC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuam9pbihcIiBcIil9IHJvbGU9XCJ0YWJwYW5lbFwiPlxyXG4gICAgICAgICAgICA8QXV0b0hpZGVTY3JvbGxiYXIgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fc2Nyb2xsQ29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8VXNlckluZm9IZWFkZXIgbWVtYmVyPXttZW1iZXJ9IGUyZVN0YXR1cz17ZTJlU3RhdHVzfSBvbkNsb3NlPXtvbkNsb3NlfSAvPlxyXG5cclxuICAgICAgICAgICAgICAgIHsgY29udGVudCB9XHJcbiAgICAgICAgICAgIDwvQXV0b0hpZGVTY3JvbGxiYXI+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG59O1xyXG5cclxuVXNlckluZm8ucHJvcFR5cGVzID0ge1xyXG4gICAgdXNlcjogUHJvcFR5cGVzLm9uZU9mVHlwZShbXHJcbiAgICAgICAgUHJvcFR5cGVzLmluc3RhbmNlT2YoVXNlciksXHJcbiAgICAgICAgUHJvcFR5cGVzLmluc3RhbmNlT2YoUm9vbU1lbWJlciksXHJcbiAgICAgICAgR3JvdXBNZW1iZXIsXHJcbiAgICBdKS5pc1JlcXVpcmVkLFxyXG4gICAgZ3JvdXA6IFByb3BUeXBlcy5pbnN0YW5jZU9mKEdyb3VwKSxcclxuICAgIGdyb3VwSWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICByb29tSWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgb25DbG9zZTogUHJvcFR5cGVzLmZ1bmMsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBVc2VySW5mbztcclxuIl19