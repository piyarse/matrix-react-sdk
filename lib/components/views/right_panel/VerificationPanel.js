"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _crypto = require("matrix-js-sdk/src/crypto");

var _QRCode = require("matrix-js-sdk/src/crypto/verification/QRCode");

var _VerificationQRCode = _interopRequireDefault(require("../elements/crypto/VerificationQRCode"));

var _languageHandler = require("../../../languageHandler");

var _E2EIcon = _interopRequireDefault(require("../rooms/E2EIcon"));

var _VerificationRequest = require("matrix-js-sdk/src/crypto/verification/request/VerificationRequest");

var _Spinner = _interopRequireDefault(require("../elements/Spinner"));

/*
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class VerificationPanel extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onReciprocateYesClick", () => {
      this.setState({
        reciprocateButtonClicked: true
      });
      this.state.reciprocateQREvent.confirm();
    });
    (0, _defineProperty2.default)(this, "_onReciprocateNoClick", () => {
      this.setState({
        reciprocateButtonClicked: true
      });
      this.state.reciprocateQREvent.cancel();
    });
    (0, _defineProperty2.default)(this, "_startSAS", async () => {
      this.setState({
        emojiButtonClicked: true
      });
      const verifier = this.props.request.beginKeyVerification(_crypto.verificationMethods.SAS);

      try {
        await verifier.verify();
      } catch (err) {
        console.error(err);
      }
    });
    (0, _defineProperty2.default)(this, "_onSasMatchesClick", () => {
      this.state.sasEvent.confirm();
    });
    (0, _defineProperty2.default)(this, "_onSasMismatchesClick", () => {
      this.state.sasEvent.mismatch();
    });
    (0, _defineProperty2.default)(this, "_updateVerifierState", () => {
      const {
        request
      } = this.props;
      const {
        sasEvent,
        reciprocateQREvent
      } = request.verifier;
      request.verifier.off('show_sas', this._updateVerifierState);
      request.verifier.off('show_reciprocate_qr', this._updateVerifierState);
      this.setState({
        sasEvent,
        reciprocateQREvent
      });
    });
    (0, _defineProperty2.default)(this, "_onRequestChange", async () => {
      const {
        request
      } = this.props;
      const hadVerifier = this._hasVerifier;
      this._hasVerifier = !!request.verifier;

      if (!hadVerifier && this._hasVerifier) {
        request.verifier.on('show_sas', this._updateVerifierState);
        request.verifier.on('show_reciprocate_qr', this._updateVerifierState);

        try {
          // on the requester side, this is also awaited in _startSAS,
          // but that's ok as verify should return the same promise.
          await request.verifier.verify();
        } catch (err) {
          console.error("error verify", err);
        }
      }
    });
    this.state = {};
    this._hasVerifier = false;
  }

  renderQRPhase() {
    const {
      member,
      request
    } = this.props;
    const showSAS = request.otherPartySupportsMethod(_crypto.verificationMethods.SAS);
    const showQR = request.otherPartySupportsMethod(_QRCode.SCAN_QR_CODE_METHOD);
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const noCommonMethodError = !showSAS && !showQR ? _react.default.createElement("p", null, (0, _languageHandler._t)("The session you are trying to verify doesn't support scanning a QR code or emoji verification, which is what Riot supports. Try with a different client.")) : null;

    if (this.props.layout === 'dialog') {
      // HACK: This is a terrible idea.
      let qrBlock;
      let sasBlock;

      if (showQR) {
        qrBlock = _react.default.createElement("div", {
          className: "mx_VerificationPanel_QRPhase_startOption"
        }, _react.default.createElement("p", null, (0, _languageHandler._t)("Scan this unique code")), _react.default.createElement(_VerificationQRCode.default, {
          qrCodeData: request.qrCodeData
        }));
      }

      if (showSAS) {
        sasBlock = _react.default.createElement("div", {
          className: "mx_VerificationPanel_QRPhase_startOption"
        }, _react.default.createElement("p", null, (0, _languageHandler._t)("Compare unique emoji")), _react.default.createElement("span", {
          className: "mx_VerificationPanel_QRPhase_helpText"
        }, (0, _languageHandler._t)("Compare a unique set of emoji if you don't have a camera on either device")), _react.default.createElement(AccessibleButton, {
          disabled: this.state.emojiButtonClicked,
          onClick: this._startSAS,
          kind: "primary"
        }, (0, _languageHandler._t)("Start")));
      }

      const or = qrBlock && sasBlock ? _react.default.createElement("div", {
        className: "mx_VerificationPanel_QRPhase_betweenText"
      }, (0, _languageHandler._t)("or")) : null;
      return _react.default.createElement("div", null, (0, _languageHandler._t)("Verify this session by completing one of the following:"), _react.default.createElement("div", {
        className: "mx_VerificationPanel_QRPhase_startOptions"
      }, qrBlock, or, sasBlock, noCommonMethodError));
    }

    let qrBlock;

    if (showQR) {
      qrBlock = _react.default.createElement("div", {
        className: "mx_UserInfo_container"
      }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Verify by scanning")), _react.default.createElement("p", null, (0, _languageHandler._t)("Ask %(displayName)s to scan your code:", {
        displayName: member.displayName || member.name || member.userId
      })), _react.default.createElement("div", {
        className: "mx_VerificationPanel_qrCode"
      }, _react.default.createElement(_VerificationQRCode.default, {
        qrCodeData: request.qrCodeData
      })));
    }

    let sasBlock;

    if (showSAS) {
      const disabled = this.state.emojiButtonClicked;
      const sasLabel = showQR ? (0, _languageHandler._t)("If you can't scan the code above, verify by comparing unique emoji.") : (0, _languageHandler._t)("Verify by comparing unique emoji.");
      sasBlock = _react.default.createElement("div", {
        className: "mx_UserInfo_container"
      }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Verify by emoji")), _react.default.createElement("p", null, sasLabel), _react.default.createElement(AccessibleButton, {
        disabled: disabled,
        kind: "primary",
        className: "mx_UserInfo_wideButton",
        onClick: this._startSAS
      }, (0, _languageHandler._t)("Verify by emoji")));
    }

    const noCommonMethodBlock = noCommonMethodError ? _react.default.createElement("div", {
      className: "mx_UserInfo_container"
    }, noCommonMethodError) : null; // TODO: add way to open camera to scan a QR code

    return _react.default.createElement(_react.default.Fragment, null, qrBlock, sasBlock, noCommonMethodBlock);
  }

  renderQRReciprocatePhase() {
    const {
      member,
      request
    } = this.props;
    let Button; // a bit of a hack, but the FormButton should only be used in the right panel
    // they should probably just be the same component with a css class applied to it?

    if (this.props.inDialog) {
      Button = sdk.getComponent("elements.AccessibleButton");
    } else {
      Button = sdk.getComponent("elements.FormButton");
    }

    const description = request.isSelfVerification ? (0, _languageHandler._t)("Almost there! Is your other session showing the same shield?") : (0, _languageHandler._t)("Almost there! Is %(displayName)s showing the same shield?", {
      displayName: member.displayName || member.name || member.userId
    });
    let body;

    if (this.state.reciprocateQREvent) {
      // riot web doesn't support scanning yet, so assume here we're the client being scanned.
      //
      // we're passing both a label and a child string to Button as
      // FormButton and AccessibleButton expect this differently
      body = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("p", null, description), _react.default.createElement(_E2EIcon.default, {
        isUser: true,
        status: "verified",
        size: 128,
        hideTooltip: true
      }), _react.default.createElement("div", {
        className: "mx_VerificationPanel_reciprocateButtons"
      }, _react.default.createElement(Button, {
        label: (0, _languageHandler._t)("No"),
        kind: "danger",
        disabled: this.state.reciprocateButtonClicked,
        onClick: this._onReciprocateNoClick
      }, (0, _languageHandler._t)("No")), _react.default.createElement(Button, {
        label: (0, _languageHandler._t)("Yes"),
        kind: "primary",
        disabled: this.state.reciprocateButtonClicked,
        onClick: this._onReciprocateYesClick
      }, (0, _languageHandler._t)("Yes"))));
    } else {
      body = _react.default.createElement("p", null, _react.default.createElement(_Spinner.default, null));
    }

    return _react.default.createElement("div", {
      className: "mx_UserInfo_container mx_VerificationPanel_reciprocate_section"
    }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Verify by scanning")), body);
  }

  renderVerifiedPhase() {
    const {
      member,
      request
    } = this.props;
    let text;

    if (!request.isSelfVerification) {
      if (this.props.isRoomEncrypted) {
        text = (0, _languageHandler._t)("Verify all users in a room to ensure it's secure.");
      } else {
        text = (0, _languageHandler._t)("In encrypted rooms, verify all users to ensure it’s secure.");
      }
    }

    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const description = request.isSelfVerification ? (0, _languageHandler._t)("You've successfully verified %(deviceName)s (%(deviceId)s)!", {
      deviceName: this.props.device.getDisplayName(),
      deviceId: this.props.device.deviceId
    }) : (0, _languageHandler._t)("You've successfully verified %(displayName)s!", {
      displayName: member.displayName || member.name || member.userId
    });
    return _react.default.createElement("div", {
      className: "mx_UserInfo_container mx_VerificationPanel_verified_section"
    }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Verified")), _react.default.createElement("p", null, description), _react.default.createElement(_E2EIcon.default, {
      isUser: true,
      status: "verified",
      size: 128,
      hideTooltip: true
    }), text ? _react.default.createElement("p", null, text) : null, _react.default.createElement(AccessibleButton, {
      kind: "primary",
      className: "mx_UserInfo_wideButton",
      onClick: this.props.onClose
    }, (0, _languageHandler._t)("Got it")));
  }

  renderCancelledPhase() {
    const {
      member,
      request
    } = this.props;
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let startAgainInstruction;

    if (request.isSelfVerification) {
      startAgainInstruction = (0, _languageHandler._t)("Start verification again from the notification.");
    } else {
      startAgainInstruction = (0, _languageHandler._t)("Start verification again from their profile.");
    }

    let text;

    if (request.cancellationCode === "m.timeout") {
      text = (0, _languageHandler._t)("Verification timed out.") + " ".concat(startAgainInstruction);
    } else if (request.cancellingUserId === request.otherUserId) {
      if (request.isSelfVerification) {
        text = (0, _languageHandler._t)("You cancelled verification on your other session.");
      } else {
        text = (0, _languageHandler._t)("%(displayName)s cancelled verification.", {
          displayName: member.displayName || member.name || member.userId
        });
      }

      text = "".concat(text, " ").concat(startAgainInstruction);
    } else {
      text = (0, _languageHandler._t)("You cancelled verification.") + " ".concat(startAgainInstruction);
    }

    return _react.default.createElement("div", {
      className: "mx_UserInfo_container"
    }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Verification cancelled")), _react.default.createElement("p", null, text), _react.default.createElement(AccessibleButton, {
      kind: "primary",
      className: "mx_UserInfo_wideButton",
      onClick: this.props.onClose
    }, (0, _languageHandler._t)("Got it")));
  }

  render() {
    const {
      member,
      phase,
      request
    } = this.props;
    const displayName = member.displayName || member.name || member.userId;

    switch (phase) {
      case _VerificationRequest.PHASE_READY:
        return this.renderQRPhase();

      case _VerificationRequest.PHASE_STARTED:
        switch (request.chosenMethod) {
          case _crypto.verificationMethods.RECIPROCATE_QR_CODE:
            return this.renderQRReciprocatePhase();

          case _crypto.verificationMethods.SAS:
            {
              const VerificationShowSas = sdk.getComponent('views.verification.VerificationShowSas');
              const emojis = this.state.sasEvent ? _react.default.createElement(VerificationShowSas, {
                displayName: displayName,
                sas: this.state.sasEvent.sas,
                onCancel: this._onSasMismatchesClick,
                onDone: this._onSasMatchesClick,
                inDialog: this.props.inDialog,
                isSelf: request.isSelfVerification,
                device: this.props.device
              }) : _react.default.createElement(_Spinner.default, null);
              return _react.default.createElement("div", {
                className: "mx_UserInfo_container"
              }, _react.default.createElement("h3", null, (0, _languageHandler._t)("Compare emoji")), emojis);
            }

          default:
            return null;
        }

      case _VerificationRequest.PHASE_DONE:
        return this.renderVerifiedPhase();

      case _VerificationRequest.PHASE_CANCELLED:
        return this.renderCancelledPhase();
    }

    console.error("VerificationPanel unhandled phase:", phase);
    return null;
  }

  componentDidMount() {
    const {
      request
    } = this.props;
    request.on("change", this._onRequestChange);

    if (request.verifier) {
      const {
        request
      } = this.props;
      const {
        sasEvent,
        reciprocateQREvent
      } = request.verifier;
      this.setState({
        sasEvent,
        reciprocateQREvent
      });
    }

    this._onRequestChange();
  }

  componentWillUnmount() {
    const {
      request
    } = this.props;

    if (request.verifier) {
      request.verifier.off('show_sas', this._updateVerifierState);
      request.verifier.off('show_reciprocate_qr', this._updateVerifierState);
    }

    request.off("change", this._onRequestChange);
  }

}

exports.default = VerificationPanel;
(0, _defineProperty2.default)(VerificationPanel, "propTypes", {
  layout: _propTypes.default.string,
  request: _propTypes.default.object.isRequired,
  member: _propTypes.default.object.isRequired,
  phase: _propTypes.default.oneOf([_VerificationRequest.PHASE_UNSENT, _VerificationRequest.PHASE_REQUESTED, _VerificationRequest.PHASE_READY, _VerificationRequest.PHASE_STARTED, _VerificationRequest.PHASE_CANCELLED, _VerificationRequest.PHASE_DONE]).isRequired,
  onClose: _propTypes.default.func.isRequired,
  isRoomEncrypted: _propTypes.default.bool
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3JpZ2h0X3BhbmVsL1ZlcmlmaWNhdGlvblBhbmVsLmpzIl0sIm5hbWVzIjpbIlZlcmlmaWNhdGlvblBhbmVsIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInNldFN0YXRlIiwicmVjaXByb2NhdGVCdXR0b25DbGlja2VkIiwic3RhdGUiLCJyZWNpcHJvY2F0ZVFSRXZlbnQiLCJjb25maXJtIiwiY2FuY2VsIiwiZW1vamlCdXR0b25DbGlja2VkIiwidmVyaWZpZXIiLCJyZXF1ZXN0IiwiYmVnaW5LZXlWZXJpZmljYXRpb24iLCJ2ZXJpZmljYXRpb25NZXRob2RzIiwiU0FTIiwidmVyaWZ5IiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwic2FzRXZlbnQiLCJtaXNtYXRjaCIsIm9mZiIsIl91cGRhdGVWZXJpZmllclN0YXRlIiwiaGFkVmVyaWZpZXIiLCJfaGFzVmVyaWZpZXIiLCJvbiIsInJlbmRlclFSUGhhc2UiLCJtZW1iZXIiLCJzaG93U0FTIiwib3RoZXJQYXJ0eVN1cHBvcnRzTWV0aG9kIiwic2hvd1FSIiwiU0NBTl9RUl9DT0RFX01FVEhPRCIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJub0NvbW1vbk1ldGhvZEVycm9yIiwibGF5b3V0IiwicXJCbG9jayIsInNhc0Jsb2NrIiwicXJDb2RlRGF0YSIsIl9zdGFydFNBUyIsIm9yIiwiZGlzcGxheU5hbWUiLCJuYW1lIiwidXNlcklkIiwiZGlzYWJsZWQiLCJzYXNMYWJlbCIsIm5vQ29tbW9uTWV0aG9kQmxvY2siLCJyZW5kZXJRUlJlY2lwcm9jYXRlUGhhc2UiLCJCdXR0b24iLCJpbkRpYWxvZyIsImRlc2NyaXB0aW9uIiwiaXNTZWxmVmVyaWZpY2F0aW9uIiwiYm9keSIsIl9vblJlY2lwcm9jYXRlTm9DbGljayIsIl9vblJlY2lwcm9jYXRlWWVzQ2xpY2siLCJyZW5kZXJWZXJpZmllZFBoYXNlIiwidGV4dCIsImlzUm9vbUVuY3J5cHRlZCIsImRldmljZU5hbWUiLCJkZXZpY2UiLCJnZXREaXNwbGF5TmFtZSIsImRldmljZUlkIiwib25DbG9zZSIsInJlbmRlckNhbmNlbGxlZFBoYXNlIiwic3RhcnRBZ2Fpbkluc3RydWN0aW9uIiwiY2FuY2VsbGF0aW9uQ29kZSIsImNhbmNlbGxpbmdVc2VySWQiLCJvdGhlclVzZXJJZCIsInJlbmRlciIsInBoYXNlIiwiUEhBU0VfUkVBRFkiLCJQSEFTRV9TVEFSVEVEIiwiY2hvc2VuTWV0aG9kIiwiUkVDSVBST0NBVEVfUVJfQ09ERSIsIlZlcmlmaWNhdGlvblNob3dTYXMiLCJlbW9qaXMiLCJzYXMiLCJfb25TYXNNaXNtYXRjaGVzQ2xpY2siLCJfb25TYXNNYXRjaGVzQ2xpY2siLCJQSEFTRV9ET05FIiwiUEhBU0VfQ0FOQ0VMTEVEIiwiY29tcG9uZW50RGlkTW91bnQiLCJfb25SZXF1ZXN0Q2hhbmdlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwib25lT2YiLCJQSEFTRV9VTlNFTlQiLCJQSEFTRV9SRVFVRVNURUQiLCJmdW5jIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFRQTs7QUFsQ0E7Ozs7Ozs7Ozs7Ozs7OztBQW9DZSxNQUFNQSxpQkFBTixTQUFnQ0MsZUFBTUMsYUFBdEMsQ0FBb0Q7QUFpQi9EQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSxrRUE2Rk0sTUFBTTtBQUMzQixXQUFLQyxRQUFMLENBQWM7QUFBQ0MsUUFBQUEsd0JBQXdCLEVBQUU7QUFBM0IsT0FBZDtBQUNBLFdBQUtDLEtBQUwsQ0FBV0Msa0JBQVgsQ0FBOEJDLE9BQTlCO0FBQ0gsS0FoR2tCO0FBQUEsaUVBa0dLLE1BQU07QUFDMUIsV0FBS0osUUFBTCxDQUFjO0FBQUNDLFFBQUFBLHdCQUF3QixFQUFFO0FBQTNCLE9BQWQ7QUFDQSxXQUFLQyxLQUFMLENBQVdDLGtCQUFYLENBQThCRSxNQUE5QjtBQUNILEtBckdrQjtBQUFBLHFEQXVRUCxZQUFZO0FBQ3BCLFdBQUtMLFFBQUwsQ0FBYztBQUFDTSxRQUFBQSxrQkFBa0IsRUFBRTtBQUFyQixPQUFkO0FBQ0EsWUFBTUMsUUFBUSxHQUFHLEtBQUtSLEtBQUwsQ0FBV1MsT0FBWCxDQUFtQkMsb0JBQW5CLENBQXdDQyw0QkFBb0JDLEdBQTVELENBQWpCOztBQUNBLFVBQUk7QUFDQSxjQUFNSixRQUFRLENBQUNLLE1BQVQsRUFBTjtBQUNILE9BRkQsQ0FFRSxPQUFPQyxHQUFQLEVBQVk7QUFDVkMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQWQ7QUFDSDtBQUNKLEtBL1FrQjtBQUFBLDhEQWlSRSxNQUFNO0FBQ3ZCLFdBQUtYLEtBQUwsQ0FBV2MsUUFBWCxDQUFvQlosT0FBcEI7QUFDSCxLQW5Sa0I7QUFBQSxpRUFxUkssTUFBTTtBQUMxQixXQUFLRixLQUFMLENBQVdjLFFBQVgsQ0FBb0JDLFFBQXBCO0FBQ0gsS0F2UmtCO0FBQUEsZ0VBeVJJLE1BQU07QUFDekIsWUFBTTtBQUFDVCxRQUFBQTtBQUFELFVBQVksS0FBS1QsS0FBdkI7QUFDQSxZQUFNO0FBQUNpQixRQUFBQSxRQUFEO0FBQVdiLFFBQUFBO0FBQVgsVUFBaUNLLE9BQU8sQ0FBQ0QsUUFBL0M7QUFDQUMsTUFBQUEsT0FBTyxDQUFDRCxRQUFSLENBQWlCVyxHQUFqQixDQUFxQixVQUFyQixFQUFpQyxLQUFLQyxvQkFBdEM7QUFDQVgsTUFBQUEsT0FBTyxDQUFDRCxRQUFSLENBQWlCVyxHQUFqQixDQUFxQixxQkFBckIsRUFBNEMsS0FBS0Msb0JBQWpEO0FBQ0EsV0FBS25CLFFBQUwsQ0FBYztBQUFDZ0IsUUFBQUEsUUFBRDtBQUFXYixRQUFBQTtBQUFYLE9BQWQ7QUFDSCxLQS9Sa0I7QUFBQSw0REFpU0EsWUFBWTtBQUMzQixZQUFNO0FBQUNLLFFBQUFBO0FBQUQsVUFBWSxLQUFLVCxLQUF2QjtBQUNBLFlBQU1xQixXQUFXLEdBQUcsS0FBS0MsWUFBekI7QUFDQSxXQUFLQSxZQUFMLEdBQW9CLENBQUMsQ0FBQ2IsT0FBTyxDQUFDRCxRQUE5Qjs7QUFDQSxVQUFJLENBQUNhLFdBQUQsSUFBZ0IsS0FBS0MsWUFBekIsRUFBdUM7QUFDbkNiLFFBQUFBLE9BQU8sQ0FBQ0QsUUFBUixDQUFpQmUsRUFBakIsQ0FBb0IsVUFBcEIsRUFBZ0MsS0FBS0gsb0JBQXJDO0FBQ0FYLFFBQUFBLE9BQU8sQ0FBQ0QsUUFBUixDQUFpQmUsRUFBakIsQ0FBb0IscUJBQXBCLEVBQTJDLEtBQUtILG9CQUFoRDs7QUFDQSxZQUFJO0FBQ0E7QUFDQTtBQUNBLGdCQUFNWCxPQUFPLENBQUNELFFBQVIsQ0FBaUJLLE1BQWpCLEVBQU47QUFDSCxTQUpELENBSUUsT0FBT0MsR0FBUCxFQUFZO0FBQ1ZDLFVBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLGNBQWQsRUFBOEJGLEdBQTlCO0FBQ0g7QUFDSjtBQUNKLEtBaFRrQjtBQUVmLFNBQUtYLEtBQUwsR0FBYSxFQUFiO0FBQ0EsU0FBS21CLFlBQUwsR0FBb0IsS0FBcEI7QUFDSDs7QUFFREUsRUFBQUEsYUFBYSxHQUFHO0FBQ1osVUFBTTtBQUFDQyxNQUFBQSxNQUFEO0FBQVNoQixNQUFBQTtBQUFULFFBQW9CLEtBQUtULEtBQS9CO0FBQ0EsVUFBTTBCLE9BQU8sR0FBR2pCLE9BQU8sQ0FBQ2tCLHdCQUFSLENBQWlDaEIsNEJBQW9CQyxHQUFyRCxDQUFoQjtBQUNBLFVBQU1nQixNQUFNLEdBQUduQixPQUFPLENBQUNrQix3QkFBUixDQUFpQ0UsMkJBQWpDLENBQWY7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFVBQU1DLG1CQUFtQixHQUFHLENBQUNQLE9BQUQsSUFBWSxDQUFDRSxNQUFiLEdBQ3hCLHdDQUFJLHlCQUFHLDBKQUFILENBQUosQ0FEd0IsR0FFeEIsSUFGSjs7QUFJQSxRQUFJLEtBQUs1QixLQUFMLENBQVdrQyxNQUFYLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ2hDO0FBQ0EsVUFBSUMsT0FBSjtBQUNBLFVBQUlDLFFBQUo7O0FBQ0EsVUFBSVIsTUFBSixFQUFZO0FBQ1JPLFFBQUFBLE9BQU8sR0FDSDtBQUFLLFVBQUEsU0FBUyxFQUFDO0FBQWYsV0FDSSx3Q0FBSSx5QkFBRyx1QkFBSCxDQUFKLENBREosRUFFSSw2QkFBQywyQkFBRDtBQUFvQixVQUFBLFVBQVUsRUFBRTFCLE9BQU8sQ0FBQzRCO0FBQXhDLFVBRkosQ0FESjtBQUtIOztBQUNELFVBQUlYLE9BQUosRUFBYTtBQUNUVSxRQUFBQSxRQUFRLEdBQ0o7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFdBQ0ksd0NBQUkseUJBQUcsc0JBQUgsQ0FBSixDQURKLEVBRUk7QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixXQUF5RCx5QkFBRywyRUFBSCxDQUF6RCxDQUZKLEVBR0ksNkJBQUMsZ0JBQUQ7QUFBa0IsVUFBQSxRQUFRLEVBQUUsS0FBS2pDLEtBQUwsQ0FBV0ksa0JBQXZDO0FBQTJELFVBQUEsT0FBTyxFQUFFLEtBQUsrQixTQUF6RTtBQUFvRixVQUFBLElBQUksRUFBQztBQUF6RixXQUNLLHlCQUFHLE9BQUgsQ0FETCxDQUhKLENBREo7QUFRSDs7QUFDRCxZQUFNQyxFQUFFLEdBQUdKLE9BQU8sSUFBSUMsUUFBWCxHQUNQO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUEyRCx5QkFBRyxJQUFILENBQTNELENBRE8sR0FDc0UsSUFEakY7QUFFQSxhQUNJLDBDQUNLLHlCQUFHLHlEQUFILENBREwsRUFFSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDS0QsT0FETCxFQUVLSSxFQUZMLEVBR0tILFFBSEwsRUFJS0gsbUJBSkwsQ0FGSixDQURKO0FBV0g7O0FBRUQsUUFBSUUsT0FBSjs7QUFDQSxRQUFJUCxNQUFKLEVBQVk7QUFDUk8sTUFBQUEsT0FBTyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNOLHlDQUFLLHlCQUFHLG9CQUFILENBQUwsQ0FETSxFQUVOLHdDQUFJLHlCQUFHLHdDQUFILEVBQTZDO0FBQzdDSyxRQUFBQSxXQUFXLEVBQUVmLE1BQU0sQ0FBQ2UsV0FBUCxJQUFzQmYsTUFBTSxDQUFDZ0IsSUFBN0IsSUFBcUNoQixNQUFNLENBQUNpQjtBQURaLE9BQTdDLENBQUosQ0FGTSxFQU1OO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLDZCQUFDLDJCQUFEO0FBQW9CLFFBQUEsVUFBVSxFQUFFakMsT0FBTyxDQUFDNEI7QUFBeEMsUUFESixDQU5NLENBQVY7QUFVSDs7QUFFRCxRQUFJRCxRQUFKOztBQUNBLFFBQUlWLE9BQUosRUFBYTtBQUNULFlBQU1pQixRQUFRLEdBQUcsS0FBS3hDLEtBQUwsQ0FBV0ksa0JBQTVCO0FBQ0EsWUFBTXFDLFFBQVEsR0FBR2hCLE1BQU0sR0FDbkIseUJBQUcscUVBQUgsQ0FEbUIsR0FFbkIseUJBQUcsbUNBQUgsQ0FGSjtBQUdBUSxNQUFBQSxRQUFRLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1AseUNBQUsseUJBQUcsaUJBQUgsQ0FBTCxDQURPLEVBRVAsd0NBQUlRLFFBQUosQ0FGTyxFQUdQLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsUUFBUSxFQUFFRCxRQUE1QjtBQUFzQyxRQUFBLElBQUksRUFBQyxTQUEzQztBQUFxRCxRQUFBLFNBQVMsRUFBQyx3QkFBL0Q7QUFBd0YsUUFBQSxPQUFPLEVBQUUsS0FBS0w7QUFBdEcsU0FDSyx5QkFBRyxpQkFBSCxDQURMLENBSE8sQ0FBWDtBQU9IOztBQUVELFVBQU1PLG1CQUFtQixHQUFHWixtQkFBbUIsR0FDMUM7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXdDQSxtQkFBeEMsQ0FEMEMsR0FFMUMsSUFGTCxDQTNFWSxDQStFWjs7QUFDQSxXQUFPLDZCQUFDLGNBQUQsQ0FBTyxRQUFQLFFBQ0ZFLE9BREUsRUFFRkMsUUFGRSxFQUdGUyxtQkFIRSxDQUFQO0FBS0g7O0FBWURDLEVBQUFBLHdCQUF3QixHQUFHO0FBQ3ZCLFVBQU07QUFBQ3JCLE1BQUFBLE1BQUQ7QUFBU2hCLE1BQUFBO0FBQVQsUUFBb0IsS0FBS1QsS0FBL0I7QUFDQSxRQUFJK0MsTUFBSixDQUZ1QixDQUd2QjtBQUNBOztBQUNBLFFBQUksS0FBSy9DLEtBQUwsQ0FBV2dELFFBQWYsRUFBeUI7QUFDckJELE1BQUFBLE1BQU0sR0FBR2hCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBVDtBQUNILEtBRkQsTUFFTztBQUNIZSxNQUFBQSxNQUFNLEdBQUdoQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQVQ7QUFDSDs7QUFDRCxVQUFNaUIsV0FBVyxHQUFHeEMsT0FBTyxDQUFDeUMsa0JBQVIsR0FDaEIseUJBQUcsOERBQUgsQ0FEZ0IsR0FFaEIseUJBQUcsMkRBQUgsRUFBZ0U7QUFDNURWLE1BQUFBLFdBQVcsRUFBRWYsTUFBTSxDQUFDZSxXQUFQLElBQXNCZixNQUFNLENBQUNnQixJQUE3QixJQUFxQ2hCLE1BQU0sQ0FBQ2lCO0FBREcsS0FBaEUsQ0FGSjtBQUtBLFFBQUlTLElBQUo7O0FBQ0EsUUFBSSxLQUFLaEQsS0FBTCxDQUFXQyxrQkFBZixFQUFtQztBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBK0MsTUFBQUEsSUFBSSxHQUFHLDZCQUFDLGNBQUQsQ0FBTyxRQUFQLFFBQ0gsd0NBQUlGLFdBQUosQ0FERyxFQUVILDZCQUFDLGdCQUFEO0FBQVMsUUFBQSxNQUFNLEVBQUUsSUFBakI7QUFBdUIsUUFBQSxNQUFNLEVBQUMsVUFBOUI7QUFBeUMsUUFBQSxJQUFJLEVBQUUsR0FBL0M7QUFBb0QsUUFBQSxXQUFXLEVBQUU7QUFBakUsUUFGRyxFQUdIO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLDZCQUFDLE1BQUQ7QUFDSSxRQUFBLEtBQUssRUFBRSx5QkFBRyxJQUFILENBRFg7QUFDcUIsUUFBQSxJQUFJLEVBQUMsUUFEMUI7QUFFSSxRQUFBLFFBQVEsRUFBRSxLQUFLOUMsS0FBTCxDQUFXRCx3QkFGekI7QUFHSSxRQUFBLE9BQU8sRUFBRSxLQUFLa0Q7QUFIbEIsU0FHMEMseUJBQUcsSUFBSCxDQUgxQyxDQURKLEVBS0ksNkJBQUMsTUFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLEtBQUgsQ0FEWDtBQUNzQixRQUFBLElBQUksRUFBQyxTQUQzQjtBQUVJLFFBQUEsUUFBUSxFQUFFLEtBQUtqRCxLQUFMLENBQVdELHdCQUZ6QjtBQUdJLFFBQUEsT0FBTyxFQUFFLEtBQUttRDtBQUhsQixTQUcyQyx5QkFBRyxLQUFILENBSDNDLENBTEosQ0FIRyxDQUFQO0FBY0gsS0FuQkQsTUFtQk87QUFDSEYsTUFBQUEsSUFBSSxHQUFHLHdDQUFHLDZCQUFDLGdCQUFELE9BQUgsQ0FBUDtBQUNIOztBQUNELFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0gseUNBQUsseUJBQUcsb0JBQUgsQ0FBTCxDQURHLEVBRURBLElBRkMsQ0FBUDtBQUlIOztBQUVERyxFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixVQUFNO0FBQUM3QixNQUFBQSxNQUFEO0FBQVNoQixNQUFBQTtBQUFULFFBQW9CLEtBQUtULEtBQS9CO0FBRUEsUUFBSXVELElBQUo7O0FBQ0EsUUFBSSxDQUFDOUMsT0FBTyxDQUFDeUMsa0JBQWIsRUFBaUM7QUFDN0IsVUFBSSxLQUFLbEQsS0FBTCxDQUFXd0QsZUFBZixFQUFnQztBQUM1QkQsUUFBQUEsSUFBSSxHQUFHLHlCQUFHLG1EQUFILENBQVA7QUFDSCxPQUZELE1BRU87QUFDSEEsUUFBQUEsSUFBSSxHQUFHLHlCQUFHLDZEQUFILENBQVA7QUFDSDtBQUNKOztBQUVELFVBQU16QixnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU1pQixXQUFXLEdBQUd4QyxPQUFPLENBQUN5QyxrQkFBUixHQUNoQix5QkFBRyw2REFBSCxFQUFrRTtBQUM5RE8sTUFBQUEsVUFBVSxFQUFFLEtBQUt6RCxLQUFMLENBQVcwRCxNQUFYLENBQWtCQyxjQUFsQixFQURrRDtBQUU5REMsTUFBQUEsUUFBUSxFQUFFLEtBQUs1RCxLQUFMLENBQVcwRCxNQUFYLENBQWtCRTtBQUZrQyxLQUFsRSxDQURnQixHQUtoQix5QkFBRywrQ0FBSCxFQUFvRDtBQUNoRHBCLE1BQUFBLFdBQVcsRUFBRWYsTUFBTSxDQUFDZSxXQUFQLElBQXNCZixNQUFNLENBQUNnQixJQUE3QixJQUFxQ2hCLE1BQU0sQ0FBQ2lCO0FBRFQsS0FBcEQsQ0FMSjtBQVNBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0kseUNBQUsseUJBQUcsVUFBSCxDQUFMLENBREosRUFFSSx3Q0FBSU8sV0FBSixDQUZKLEVBR0ksNkJBQUMsZ0JBQUQ7QUFBUyxNQUFBLE1BQU0sRUFBRSxJQUFqQjtBQUF1QixNQUFBLE1BQU0sRUFBQyxVQUE5QjtBQUF5QyxNQUFBLElBQUksRUFBRSxHQUEvQztBQUFvRCxNQUFBLFdBQVcsRUFBRTtBQUFqRSxNQUhKLEVBSU1NLElBQUksR0FBRyx3Q0FBS0EsSUFBTCxDQUFILEdBQXFCLElBSi9CLEVBS0ksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsTUFBQSxTQUFTLEVBQUMsd0JBQTNDO0FBQW9FLE1BQUEsT0FBTyxFQUFFLEtBQUt2RCxLQUFMLENBQVc2RDtBQUF4RixPQUNLLHlCQUFHLFFBQUgsQ0FETCxDQUxKLENBREo7QUFXSDs7QUFFREMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsVUFBTTtBQUFDckMsTUFBQUEsTUFBRDtBQUFTaEIsTUFBQUE7QUFBVCxRQUFvQixLQUFLVCxLQUEvQjtBQUVBLFVBQU04QixnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFFBQUkrQixxQkFBSjs7QUFDQSxRQUFJdEQsT0FBTyxDQUFDeUMsa0JBQVosRUFBZ0M7QUFDNUJhLE1BQUFBLHFCQUFxQixHQUFHLHlCQUFHLGlEQUFILENBQXhCO0FBQ0gsS0FGRCxNQUVPO0FBQ0hBLE1BQUFBLHFCQUFxQixHQUFHLHlCQUFHLDhDQUFILENBQXhCO0FBQ0g7O0FBRUQsUUFBSVIsSUFBSjs7QUFDQSxRQUFJOUMsT0FBTyxDQUFDdUQsZ0JBQVIsS0FBNkIsV0FBakMsRUFBOEM7QUFDMUNULE1BQUFBLElBQUksR0FBRyx5QkFBRyx5QkFBSCxlQUFvQ1EscUJBQXBDLENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSXRELE9BQU8sQ0FBQ3dELGdCQUFSLEtBQTZCeEQsT0FBTyxDQUFDeUQsV0FBekMsRUFBc0Q7QUFDekQsVUFBSXpELE9BQU8sQ0FBQ3lDLGtCQUFaLEVBQWdDO0FBQzVCSyxRQUFBQSxJQUFJLEdBQUcseUJBQUcsbURBQUgsQ0FBUDtBQUNILE9BRkQsTUFFTztBQUNIQSxRQUFBQSxJQUFJLEdBQUcseUJBQUcseUNBQUgsRUFBOEM7QUFDakRmLFVBQUFBLFdBQVcsRUFBRWYsTUFBTSxDQUFDZSxXQUFQLElBQXNCZixNQUFNLENBQUNnQixJQUE3QixJQUFxQ2hCLE1BQU0sQ0FBQ2lCO0FBRFIsU0FBOUMsQ0FBUDtBQUdIOztBQUNEYSxNQUFBQSxJQUFJLGFBQU1BLElBQU4sY0FBY1EscUJBQWQsQ0FBSjtBQUNILEtBVE0sTUFTQTtBQUNIUixNQUFBQSxJQUFJLEdBQUcseUJBQUcsNkJBQUgsZUFBd0NRLHFCQUF4QyxDQUFQO0FBQ0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx5Q0FBSyx5QkFBRyx3QkFBSCxDQUFMLENBREosRUFFSSx3Q0FBS1IsSUFBTCxDQUZKLEVBSUksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsTUFBQSxTQUFTLEVBQUMsd0JBQTNDO0FBQW9FLE1BQUEsT0FBTyxFQUFFLEtBQUt2RCxLQUFMLENBQVc2RDtBQUF4RixPQUNLLHlCQUFHLFFBQUgsQ0FETCxDQUpKLENBREo7QUFVSDs7QUFFRE0sRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTTtBQUFDMUMsTUFBQUEsTUFBRDtBQUFTMkMsTUFBQUEsS0FBVDtBQUFnQjNELE1BQUFBO0FBQWhCLFFBQTJCLEtBQUtULEtBQXRDO0FBRUEsVUFBTXdDLFdBQVcsR0FBR2YsTUFBTSxDQUFDZSxXQUFQLElBQXNCZixNQUFNLENBQUNnQixJQUE3QixJQUFxQ2hCLE1BQU0sQ0FBQ2lCLE1BQWhFOztBQUVBLFlBQVEwQixLQUFSO0FBQ0ksV0FBS0MsZ0NBQUw7QUFDSSxlQUFPLEtBQUs3QyxhQUFMLEVBQVA7O0FBQ0osV0FBSzhDLGtDQUFMO0FBQ0ksZ0JBQVE3RCxPQUFPLENBQUM4RCxZQUFoQjtBQUNJLGVBQUs1RCw0QkFBb0I2RCxtQkFBekI7QUFDSSxtQkFBTyxLQUFLMUIsd0JBQUwsRUFBUDs7QUFDSixlQUFLbkMsNEJBQW9CQyxHQUF6QjtBQUE4QjtBQUMxQixvQkFBTTZELG1CQUFtQixHQUFHMUMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdDQUFqQixDQUE1QjtBQUNBLG9CQUFNMEMsTUFBTSxHQUFHLEtBQUt2RSxLQUFMLENBQVdjLFFBQVgsR0FDWCw2QkFBQyxtQkFBRDtBQUNJLGdCQUFBLFdBQVcsRUFBRXVCLFdBRGpCO0FBRUksZ0JBQUEsR0FBRyxFQUFFLEtBQUtyQyxLQUFMLENBQVdjLFFBQVgsQ0FBb0IwRCxHQUY3QjtBQUdJLGdCQUFBLFFBQVEsRUFBRSxLQUFLQyxxQkFIbkI7QUFJSSxnQkFBQSxNQUFNLEVBQUUsS0FBS0Msa0JBSmpCO0FBS0ksZ0JBQUEsUUFBUSxFQUFFLEtBQUs3RSxLQUFMLENBQVdnRCxRQUx6QjtBQU1JLGdCQUFBLE1BQU0sRUFBRXZDLE9BQU8sQ0FBQ3lDLGtCQU5wQjtBQU9JLGdCQUFBLE1BQU0sRUFBRSxLQUFLbEQsS0FBTCxDQUFXMEQ7QUFQdkIsZ0JBRFcsR0FTTiw2QkFBQyxnQkFBRCxPQVRUO0FBVUEscUJBQU87QUFBSyxnQkFBQSxTQUFTLEVBQUM7QUFBZixpQkFDSCx5Q0FBSyx5QkFBRyxlQUFILENBQUwsQ0FERyxFQUVEZ0IsTUFGQyxDQUFQO0FBSUg7O0FBQ0Q7QUFDSSxtQkFBTyxJQUFQO0FBckJSOztBQXVCSixXQUFLSSwrQkFBTDtBQUNJLGVBQU8sS0FBS3hCLG1CQUFMLEVBQVA7O0FBQ0osV0FBS3lCLG9DQUFMO0FBQ0ksZUFBTyxLQUFLakIsb0JBQUwsRUFBUDtBQTlCUjs7QUFnQ0EvQyxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxvQ0FBZCxFQUFvRG9ELEtBQXBEO0FBQ0EsV0FBTyxJQUFQO0FBQ0g7O0FBNkNEWSxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixVQUFNO0FBQUN2RSxNQUFBQTtBQUFELFFBQVksS0FBS1QsS0FBdkI7QUFDQVMsSUFBQUEsT0FBTyxDQUFDYyxFQUFSLENBQVcsUUFBWCxFQUFxQixLQUFLMEQsZ0JBQTFCOztBQUNBLFFBQUl4RSxPQUFPLENBQUNELFFBQVosRUFBc0I7QUFDbEIsWUFBTTtBQUFDQyxRQUFBQTtBQUFELFVBQVksS0FBS1QsS0FBdkI7QUFDQSxZQUFNO0FBQUNpQixRQUFBQSxRQUFEO0FBQVdiLFFBQUFBO0FBQVgsVUFBaUNLLE9BQU8sQ0FBQ0QsUUFBL0M7QUFDQSxXQUFLUCxRQUFMLENBQWM7QUFBQ2dCLFFBQUFBLFFBQUQ7QUFBV2IsUUFBQUE7QUFBWCxPQUFkO0FBQ0g7O0FBQ0QsU0FBSzZFLGdCQUFMO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFVBQU07QUFBQ3pFLE1BQUFBO0FBQUQsUUFBWSxLQUFLVCxLQUF2Qjs7QUFDQSxRQUFJUyxPQUFPLENBQUNELFFBQVosRUFBc0I7QUFDbEJDLE1BQUFBLE9BQU8sQ0FBQ0QsUUFBUixDQUFpQlcsR0FBakIsQ0FBcUIsVUFBckIsRUFBaUMsS0FBS0Msb0JBQXRDO0FBQ0FYLE1BQUFBLE9BQU8sQ0FBQ0QsUUFBUixDQUFpQlcsR0FBakIsQ0FBcUIscUJBQXJCLEVBQTRDLEtBQUtDLG9CQUFqRDtBQUNIOztBQUNEWCxJQUFBQSxPQUFPLENBQUNVLEdBQVIsQ0FBWSxRQUFaLEVBQXNCLEtBQUs4RCxnQkFBM0I7QUFDSDs7QUFyVjhEOzs7OEJBQTlDckYsaUIsZUFDRTtBQUNmc0MsRUFBQUEsTUFBTSxFQUFFaUQsbUJBQVVDLE1BREg7QUFFZjNFLEVBQUFBLE9BQU8sRUFBRTBFLG1CQUFVRSxNQUFWLENBQWlCQyxVQUZYO0FBR2Y3RCxFQUFBQSxNQUFNLEVBQUUwRCxtQkFBVUUsTUFBVixDQUFpQkMsVUFIVjtBQUlmbEIsRUFBQUEsS0FBSyxFQUFFZSxtQkFBVUksS0FBVixDQUFnQixDQUNuQkMsaUNBRG1CLEVBRW5CQyxvQ0FGbUIsRUFHbkJwQixnQ0FIbUIsRUFJbkJDLGtDQUptQixFQUtuQlMsb0NBTG1CLEVBTW5CRCwrQkFObUIsQ0FBaEIsRUFPSlEsVUFYWTtBQVlmekIsRUFBQUEsT0FBTyxFQUFFc0IsbUJBQVVPLElBQVYsQ0FBZUosVUFaVDtBQWFmOUIsRUFBQUEsZUFBZSxFQUFFMkIsbUJBQVVRO0FBYlosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XHJcblxyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge3ZlcmlmaWNhdGlvbk1ldGhvZHN9IGZyb20gJ21hdHJpeC1qcy1zZGsvc3JjL2NyeXB0byc7XHJcbmltcG9ydCB7U0NBTl9RUl9DT0RFX01FVEhPRH0gZnJvbSBcIm1hdHJpeC1qcy1zZGsvc3JjL2NyeXB0by92ZXJpZmljYXRpb24vUVJDb2RlXCI7XHJcblxyXG5pbXBvcnQgVmVyaWZpY2F0aW9uUVJDb2RlIGZyb20gXCIuLi9lbGVtZW50cy9jcnlwdG8vVmVyaWZpY2F0aW9uUVJDb2RlXCI7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IEUyRUljb24gZnJvbSBcIi4uL3Jvb21zL0UyRUljb25cIjtcclxuaW1wb3J0IHtcclxuICAgIFBIQVNFX1VOU0VOVCxcclxuICAgIFBIQVNFX1JFUVVFU1RFRCxcclxuICAgIFBIQVNFX1JFQURZLFxyXG4gICAgUEhBU0VfRE9ORSxcclxuICAgIFBIQVNFX1NUQVJURUQsXHJcbiAgICBQSEFTRV9DQU5DRUxMRUQsXHJcbn0gZnJvbSBcIm1hdHJpeC1qcy1zZGsvc3JjL2NyeXB0by92ZXJpZmljYXRpb24vcmVxdWVzdC9WZXJpZmljYXRpb25SZXF1ZXN0XCI7XHJcbmltcG9ydCBTcGlubmVyIGZyb20gXCIuLi9lbGVtZW50cy9TcGlubmVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWZXJpZmljYXRpb25QYW5lbCBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBsYXlvdXQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgcmVxdWVzdDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG1lbWJlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHBoYXNlOiBQcm9wVHlwZXMub25lT2YoW1xyXG4gICAgICAgICAgICBQSEFTRV9VTlNFTlQsXHJcbiAgICAgICAgICAgIFBIQVNFX1JFUVVFU1RFRCxcclxuICAgICAgICAgICAgUEhBU0VfUkVBRFksXHJcbiAgICAgICAgICAgIFBIQVNFX1NUQVJURUQsXHJcbiAgICAgICAgICAgIFBIQVNFX0NBTkNFTExFRCxcclxuICAgICAgICAgICAgUEhBU0VfRE9ORSxcclxuICAgICAgICBdKS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uQ2xvc2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgaXNSb29tRW5jcnlwdGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHt9O1xyXG4gICAgICAgIHRoaXMuX2hhc1ZlcmlmaWVyID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyUVJQaGFzZSgpIHtcclxuICAgICAgICBjb25zdCB7bWVtYmVyLCByZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgY29uc3Qgc2hvd1NBUyA9IHJlcXVlc3Qub3RoZXJQYXJ0eVN1cHBvcnRzTWV0aG9kKHZlcmlmaWNhdGlvbk1ldGhvZHMuU0FTKTtcclxuICAgICAgICBjb25zdCBzaG93UVIgPSByZXF1ZXN0Lm90aGVyUGFydHlTdXBwb3J0c01ldGhvZChTQ0FOX1FSX0NPREVfTUVUSE9EKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG5cclxuICAgICAgICBjb25zdCBub0NvbW1vbk1ldGhvZEVycm9yID0gIXNob3dTQVMgJiYgIXNob3dRUiA/XHJcbiAgICAgICAgICAgIDxwPntfdChcIlRoZSBzZXNzaW9uIHlvdSBhcmUgdHJ5aW5nIHRvIHZlcmlmeSBkb2Vzbid0IHN1cHBvcnQgc2Nhbm5pbmcgYSBRUiBjb2RlIG9yIGVtb2ppIHZlcmlmaWNhdGlvbiwgd2hpY2ggaXMgd2hhdCBSaW90IHN1cHBvcnRzLiBUcnkgd2l0aCBhIGRpZmZlcmVudCBjbGllbnQuXCIpfTwvcD4gOlxyXG4gICAgICAgICAgICBudWxsO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5sYXlvdXQgPT09ICdkaWFsb2cnKSB7XHJcbiAgICAgICAgICAgIC8vIEhBQ0s6IFRoaXMgaXMgYSB0ZXJyaWJsZSBpZGVhLlxyXG4gICAgICAgICAgICBsZXQgcXJCbG9jaztcclxuICAgICAgICAgICAgbGV0IHNhc0Jsb2NrO1xyXG4gICAgICAgICAgICBpZiAoc2hvd1FSKSB7XHJcbiAgICAgICAgICAgICAgICBxckJsb2NrID1cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfVmVyaWZpY2F0aW9uUGFuZWxfUVJQaGFzZV9zdGFydE9wdGlvbic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwPntfdChcIlNjYW4gdGhpcyB1bmlxdWUgY29kZVwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxWZXJpZmljYXRpb25RUkNvZGUgcXJDb2RlRGF0YT17cmVxdWVzdC5xckNvZGVEYXRhfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc2hvd1NBUykge1xyXG4gICAgICAgICAgICAgICAgc2FzQmxvY2sgPVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9WZXJpZmljYXRpb25QYW5lbF9RUlBoYXNlX3N0YXJ0T3B0aW9uJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHA+e190KFwiQ29tcGFyZSB1bmlxdWUgZW1vamlcIil9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J214X1ZlcmlmaWNhdGlvblBhbmVsX1FSUGhhc2VfaGVscFRleHQnPntfdChcIkNvbXBhcmUgYSB1bmlxdWUgc2V0IG9mIGVtb2ppIGlmIHlvdSBkb24ndCBoYXZlIGEgY2FtZXJhIG9uIGVpdGhlciBkZXZpY2VcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBkaXNhYmxlZD17dGhpcy5zdGF0ZS5lbW9qaUJ1dHRvbkNsaWNrZWR9IG9uQ2xpY2s9e3RoaXMuX3N0YXJ0U0FTfSBraW5kPSdwcmltYXJ5Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlN0YXJ0XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IG9yID0gcXJCbG9jayAmJiBzYXNCbG9jayA/XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfVmVyaWZpY2F0aW9uUGFuZWxfUVJQaGFzZV9iZXR3ZWVuVGV4dCc+e190KFwib3JcIil9PC9kaXY+IDogbnVsbDtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiVmVyaWZ5IHRoaXMgc2Vzc2lvbiBieSBjb21wbGV0aW5nIG9uZSBvZiB0aGUgZm9sbG93aW5nOlwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfVmVyaWZpY2F0aW9uUGFuZWxfUVJQaGFzZV9zdGFydE9wdGlvbnMnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7cXJCbG9ja31cclxuICAgICAgICAgICAgICAgICAgICAgICAge29yfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c2FzQmxvY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtub0NvbW1vbk1ldGhvZEVycm9yfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcXJCbG9jaztcclxuICAgICAgICBpZiAoc2hvd1FSKSB7XHJcbiAgICAgICAgICAgIHFyQmxvY2sgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgPGgzPntfdChcIlZlcmlmeSBieSBzY2FubmluZ1wiKX08L2gzPlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFwiQXNrICUoZGlzcGxheU5hbWUpcyB0byBzY2FuIHlvdXIgY29kZTpcIiwge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiBtZW1iZXIuZGlzcGxheU5hbWUgfHwgbWVtYmVyLm5hbWUgfHwgbWVtYmVyLnVzZXJJZCxcclxuICAgICAgICAgICAgICAgIH0pfTwvcD5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZlcmlmaWNhdGlvblBhbmVsX3FyQ29kZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxWZXJpZmljYXRpb25RUkNvZGUgcXJDb2RlRGF0YT17cmVxdWVzdC5xckNvZGVEYXRhfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzYXNCbG9jaztcclxuICAgICAgICBpZiAoc2hvd1NBUykge1xyXG4gICAgICAgICAgICBjb25zdCBkaXNhYmxlZCA9IHRoaXMuc3RhdGUuZW1vamlCdXR0b25DbGlja2VkO1xyXG4gICAgICAgICAgICBjb25zdCBzYXNMYWJlbCA9IHNob3dRUiA/XHJcbiAgICAgICAgICAgICAgICBfdChcIklmIHlvdSBjYW4ndCBzY2FuIHRoZSBjb2RlIGFib3ZlLCB2ZXJpZnkgYnkgY29tcGFyaW5nIHVuaXF1ZSBlbW9qaS5cIikgOlxyXG4gICAgICAgICAgICAgICAgX3QoXCJWZXJpZnkgYnkgY29tcGFyaW5nIHVuaXF1ZSBlbW9qaS5cIik7XHJcbiAgICAgICAgICAgIHNhc0Jsb2NrID0gPGRpdiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxoMz57X3QoXCJWZXJpZnkgYnkgZW1vamlcIil9PC9oMz5cclxuICAgICAgICAgICAgICAgIDxwPntzYXNMYWJlbH08L3A+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBkaXNhYmxlZD17ZGlzYWJsZWR9IGtpbmQ9XCJwcmltYXJ5XCIgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fd2lkZUJ1dHRvblwiIG9uQ2xpY2s9e3RoaXMuX3N0YXJ0U0FTfT5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJWZXJpZnkgYnkgZW1vamlcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG5vQ29tbW9uTWV0aG9kQmxvY2sgPSBub0NvbW1vbk1ldGhvZEVycm9yID9cclxuICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+e25vQ29tbW9uTWV0aG9kRXJyb3J9PC9kaXY+IDpcclxuICAgICAgICAgICAgIG51bGw7XHJcblxyXG4gICAgICAgIC8vIFRPRE86IGFkZCB3YXkgdG8gb3BlbiBjYW1lcmEgdG8gc2NhbiBhIFFSIGNvZGVcclxuICAgICAgICByZXR1cm4gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICB7cXJCbG9ja31cclxuICAgICAgICAgICAge3Nhc0Jsb2NrfVxyXG4gICAgICAgICAgICB7bm9Db21tb25NZXRob2RCbG9ja31cclxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxuICAgIH1cclxuXHJcbiAgICBfb25SZWNpcHJvY2F0ZVllc0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3JlY2lwcm9jYXRlQnV0dG9uQ2xpY2tlZDogdHJ1ZX0pO1xyXG4gICAgICAgIHRoaXMuc3RhdGUucmVjaXByb2NhdGVRUkV2ZW50LmNvbmZpcm0oKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uUmVjaXByb2NhdGVOb0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3JlY2lwcm9jYXRlQnV0dG9uQ2xpY2tlZDogdHJ1ZX0pO1xyXG4gICAgICAgIHRoaXMuc3RhdGUucmVjaXByb2NhdGVRUkV2ZW50LmNhbmNlbCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXJRUlJlY2lwcm9jYXRlUGhhc2UoKSB7XHJcbiAgICAgICAgY29uc3Qge21lbWJlciwgcmVxdWVzdH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGxldCBCdXR0b247XHJcbiAgICAgICAgLy8gYSBiaXQgb2YgYSBoYWNrLCBidXQgdGhlIEZvcm1CdXR0b24gc2hvdWxkIG9ubHkgYmUgdXNlZCBpbiB0aGUgcmlnaHQgcGFuZWxcclxuICAgICAgICAvLyB0aGV5IHNob3VsZCBwcm9iYWJseSBqdXN0IGJlIHRoZSBzYW1lIGNvbXBvbmVudCB3aXRoIGEgY3NzIGNsYXNzIGFwcGxpZWQgdG8gaXQ/XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuaW5EaWFsb2cpIHtcclxuICAgICAgICAgICAgQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b25cIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkZvcm1CdXR0b25cIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGRlc2NyaXB0aW9uID0gcmVxdWVzdC5pc1NlbGZWZXJpZmljYXRpb24gP1xyXG4gICAgICAgICAgICBfdChcIkFsbW9zdCB0aGVyZSEgSXMgeW91ciBvdGhlciBzZXNzaW9uIHNob3dpbmcgdGhlIHNhbWUgc2hpZWxkP1wiKSA6XHJcbiAgICAgICAgICAgIF90KFwiQWxtb3N0IHRoZXJlISBJcyAlKGRpc3BsYXlOYW1lKXMgc2hvd2luZyB0aGUgc2FtZSBzaGllbGQ/XCIsIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiBtZW1iZXIuZGlzcGxheU5hbWUgfHwgbWVtYmVyLm5hbWUgfHwgbWVtYmVyLnVzZXJJZCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IGJvZHk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVjaXByb2NhdGVRUkV2ZW50KSB7XHJcbiAgICAgICAgICAgIC8vIHJpb3Qgd2ViIGRvZXNuJ3Qgc3VwcG9ydCBzY2FubmluZyB5ZXQsIHNvIGFzc3VtZSBoZXJlIHdlJ3JlIHRoZSBjbGllbnQgYmVpbmcgc2Nhbm5lZC5cclxuICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgLy8gd2UncmUgcGFzc2luZyBib3RoIGEgbGFiZWwgYW5kIGEgY2hpbGQgc3RyaW5nIHRvIEJ1dHRvbiBhc1xyXG4gICAgICAgICAgICAvLyBGb3JtQnV0dG9uIGFuZCBBY2Nlc3NpYmxlQnV0dG9uIGV4cGVjdCB0aGlzIGRpZmZlcmVudGx5XHJcbiAgICAgICAgICAgIGJvZHkgPSA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICAgICAgICA8cD57ZGVzY3JpcHRpb259PC9wPlxyXG4gICAgICAgICAgICAgICAgPEUyRUljb24gaXNVc2VyPXt0cnVlfSBzdGF0dXM9XCJ2ZXJpZmllZFwiIHNpemU9ezEyOH0gaGlkZVRvb2x0aXA9e3RydWV9IC8+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZlcmlmaWNhdGlvblBhbmVsX3JlY2lwcm9jYXRlQnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KFwiTm9cIil9IGtpbmQ9XCJkYW5nZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5yZWNpcHJvY2F0ZUJ1dHRvbkNsaWNrZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uUmVjaXByb2NhdGVOb0NsaWNrfT57X3QoXCJOb1wiKX08L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdChcIlllc1wiKX0ga2luZD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5yZWNpcHJvY2F0ZUJ1dHRvbkNsaWNrZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uUmVjaXByb2NhdGVZZXNDbGlja30+e190KFwiWWVzXCIpfTwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGJvZHkgPSA8cD48U3Bpbm5lciAvPjwvcD47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X1VzZXJJbmZvX2NvbnRhaW5lciBteF9WZXJpZmljYXRpb25QYW5lbF9yZWNpcHJvY2F0ZV9zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgIDxoMz57X3QoXCJWZXJpZnkgYnkgc2Nhbm5pbmdcIil9PC9oMz5cclxuICAgICAgICAgICAgeyBib2R5IH1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyVmVyaWZpZWRQaGFzZSgpIHtcclxuICAgICAgICBjb25zdCB7bWVtYmVyLCByZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGxldCB0ZXh0O1xyXG4gICAgICAgIGlmICghcmVxdWVzdC5pc1NlbGZWZXJpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuaXNSb29tRW5jcnlwdGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0ID0gX3QoXCJWZXJpZnkgYWxsIHVzZXJzIGluIGEgcm9vbSB0byBlbnN1cmUgaXQncyBzZWN1cmUuXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGV4dCA9IF90KFwiSW4gZW5jcnlwdGVkIHJvb21zLCB2ZXJpZnkgYWxsIHVzZXJzIHRvIGVuc3VyZSBpdOKAmXMgc2VjdXJlLlwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgICAgICBjb25zdCBkZXNjcmlwdGlvbiA9IHJlcXVlc3QuaXNTZWxmVmVyaWZpY2F0aW9uID9cclxuICAgICAgICAgICAgX3QoXCJZb3UndmUgc3VjY2Vzc2Z1bGx5IHZlcmlmaWVkICUoZGV2aWNlTmFtZSlzICglKGRldmljZUlkKXMpIVwiLCB7XHJcbiAgICAgICAgICAgICAgICBkZXZpY2VOYW1lOiB0aGlzLnByb3BzLmRldmljZS5nZXREaXNwbGF5TmFtZSgpLFxyXG4gICAgICAgICAgICAgICAgZGV2aWNlSWQ6IHRoaXMucHJvcHMuZGV2aWNlLmRldmljZUlkLFxyXG4gICAgICAgICAgICB9KTpcclxuICAgICAgICAgICAgX3QoXCJZb3UndmUgc3VjY2Vzc2Z1bGx5IHZlcmlmaWVkICUoZGlzcGxheU5hbWUpcyFcIiwge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IG1lbWJlci5kaXNwbGF5TmFtZSB8fCBtZW1iZXIubmFtZSB8fCBtZW1iZXIudXNlcklkLFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb19jb250YWluZXIgbXhfVmVyaWZpY2F0aW9uUGFuZWxfdmVyaWZpZWRfc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgPGgzPntfdChcIlZlcmlmaWVkXCIpfTwvaDM+XHJcbiAgICAgICAgICAgICAgICA8cD57ZGVzY3JpcHRpb259PC9wPlxyXG4gICAgICAgICAgICAgICAgPEUyRUljb24gaXNVc2VyPXt0cnVlfSBzdGF0dXM9XCJ2ZXJpZmllZFwiIHNpemU9ezEyOH0gaGlkZVRvb2x0aXA9e3RydWV9IC8+XHJcbiAgICAgICAgICAgICAgICB7IHRleHQgPyA8cD57IHRleHQgfTwvcD4gOiBudWxsIH1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJwcmltYXJ5XCIgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fd2lkZUJ1dHRvblwiIG9uQ2xpY2s9e3RoaXMucHJvcHMub25DbG9zZX0+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiR290IGl0XCIpfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlckNhbmNlbGxlZFBoYXNlKCkge1xyXG4gICAgICAgIGNvbnN0IHttZW1iZXIsIHJlcXVlc3R9ID0gdGhpcy5wcm9wcztcclxuXHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgbGV0IHN0YXJ0QWdhaW5JbnN0cnVjdGlvbjtcclxuICAgICAgICBpZiAocmVxdWVzdC5pc1NlbGZWZXJpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgc3RhcnRBZ2Fpbkluc3RydWN0aW9uID0gX3QoXCJTdGFydCB2ZXJpZmljYXRpb24gYWdhaW4gZnJvbSB0aGUgbm90aWZpY2F0aW9uLlwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzdGFydEFnYWluSW5zdHJ1Y3Rpb24gPSBfdChcIlN0YXJ0IHZlcmlmaWNhdGlvbiBhZ2FpbiBmcm9tIHRoZWlyIHByb2ZpbGUuXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHRleHQ7XHJcbiAgICAgICAgaWYgKHJlcXVlc3QuY2FuY2VsbGF0aW9uQ29kZSA9PT0gXCJtLnRpbWVvdXRcIikge1xyXG4gICAgICAgICAgICB0ZXh0ID0gX3QoXCJWZXJpZmljYXRpb24gdGltZWQgb3V0LlwiKSArIGAgJHtzdGFydEFnYWluSW5zdHJ1Y3Rpb259YDtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3QuY2FuY2VsbGluZ1VzZXJJZCA9PT0gcmVxdWVzdC5vdGhlclVzZXJJZCkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdC5pc1NlbGZWZXJpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgICAgIHRleHQgPSBfdChcIllvdSBjYW5jZWxsZWQgdmVyaWZpY2F0aW9uIG9uIHlvdXIgb3RoZXIgc2Vzc2lvbi5cIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0ID0gX3QoXCIlKGRpc3BsYXlOYW1lKXMgY2FuY2VsbGVkIHZlcmlmaWNhdGlvbi5cIiwge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiBtZW1iZXIuZGlzcGxheU5hbWUgfHwgbWVtYmVyLm5hbWUgfHwgbWVtYmVyLnVzZXJJZCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRleHQgPSBgJHt0ZXh0fSAke3N0YXJ0QWdhaW5JbnN0cnVjdGlvbn1gO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRleHQgPSBfdChcIllvdSBjYW5jZWxsZWQgdmVyaWZpY2F0aW9uLlwiKSArIGAgJHtzdGFydEFnYWluSW5zdHJ1Y3Rpb259YDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8aDM+e190KFwiVmVyaWZpY2F0aW9uIGNhbmNlbGxlZFwiKX08L2gzPlxyXG4gICAgICAgICAgICAgICAgPHA+eyB0ZXh0IH08L3A+XHJcblxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD1cInByaW1hcnlcIiBjbGFzc05hbWU9XCJteF9Vc2VySW5mb193aWRlQnV0dG9uXCIgb25DbGljaz17dGhpcy5wcm9wcy5vbkNsb3NlfT5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJHb3QgaXRcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IHttZW1iZXIsIHBoYXNlLCByZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlOYW1lID0gbWVtYmVyLmRpc3BsYXlOYW1lIHx8IG1lbWJlci5uYW1lIHx8IG1lbWJlci51c2VySWQ7XHJcblxyXG4gICAgICAgIHN3aXRjaCAocGhhc2UpIHtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9SRUFEWTpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnJlbmRlclFSUGhhc2UoKTtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9TVEFSVEVEOlxyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChyZXF1ZXN0LmNob3Nlbk1ldGhvZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgdmVyaWZpY2F0aW9uTWV0aG9kcy5SRUNJUFJPQ0FURV9RUl9DT0RFOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJRUlJlY2lwcm9jYXRlUGhhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIHZlcmlmaWNhdGlvbk1ldGhvZHMuU0FTOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IFZlcmlmaWNhdGlvblNob3dTYXMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy52ZXJpZmljYXRpb24uVmVyaWZpY2F0aW9uU2hvd1NhcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBlbW9qaXMgPSB0aGlzLnN0YXRlLnNhc0V2ZW50ID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxWZXJpZmljYXRpb25TaG93U2FzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU9e2Rpc3BsYXlOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNhcz17dGhpcy5zdGF0ZS5zYXNFdmVudC5zYXN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMuX29uU2FzTWlzbWF0Y2hlc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRG9uZT17dGhpcy5fb25TYXNNYXRjaGVzQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5EaWFsb2c9e3RoaXMucHJvcHMuaW5EaWFsb2d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNTZWxmPXtyZXF1ZXN0LmlzU2VsZlZlcmlmaWNhdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXZpY2U9e3RoaXMucHJvcHMuZGV2aWNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz4gOiA8U3Bpbm5lciAvPjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfVXNlckluZm9fY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDM+e190KFwiQ29tcGFyZSBlbW9qaVwiKX08L2gzPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBlbW9qaXMgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX0RPTkU6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJWZXJpZmllZFBoYXNlKCk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfQ0FOQ0VMTEVEOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVuZGVyQ2FuY2VsbGVkUGhhc2UoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihcIlZlcmlmaWNhdGlvblBhbmVsIHVuaGFuZGxlZCBwaGFzZTpcIiwgcGhhc2UpO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIF9zdGFydFNBUyA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtlbW9qaUJ1dHRvbkNsaWNrZWQ6IHRydWV9KTtcclxuICAgICAgICBjb25zdCB2ZXJpZmllciA9IHRoaXMucHJvcHMucmVxdWVzdC5iZWdpbktleVZlcmlmaWNhdGlvbih2ZXJpZmljYXRpb25NZXRob2RzLlNBUyk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgdmVyaWZpZXIudmVyaWZ5KCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblNhc01hdGNoZXNDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnN0YXRlLnNhc0V2ZW50LmNvbmZpcm0oKTtcclxuICAgIH07XHJcblxyXG4gICAgX29uU2FzTWlzbWF0Y2hlc0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc3RhdGUuc2FzRXZlbnQubWlzbWF0Y2goKTtcclxuICAgIH07XHJcblxyXG4gICAgX3VwZGF0ZVZlcmlmaWVyU3RhdGUgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3Qge3JlcXVlc3R9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBjb25zdCB7c2FzRXZlbnQsIHJlY2lwcm9jYXRlUVJFdmVudH0gPSByZXF1ZXN0LnZlcmlmaWVyO1xyXG4gICAgICAgIHJlcXVlc3QudmVyaWZpZXIub2ZmKCdzaG93X3NhcycsIHRoaXMuX3VwZGF0ZVZlcmlmaWVyU3RhdGUpO1xyXG4gICAgICAgIHJlcXVlc3QudmVyaWZpZXIub2ZmKCdzaG93X3JlY2lwcm9jYXRlX3FyJywgdGhpcy5fdXBkYXRlVmVyaWZpZXJTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2FzRXZlbnQsIHJlY2lwcm9jYXRlUVJFdmVudH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25SZXF1ZXN0Q2hhbmdlID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHtyZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgY29uc3QgaGFkVmVyaWZpZXIgPSB0aGlzLl9oYXNWZXJpZmllcjtcclxuICAgICAgICB0aGlzLl9oYXNWZXJpZmllciA9ICEhcmVxdWVzdC52ZXJpZmllcjtcclxuICAgICAgICBpZiAoIWhhZFZlcmlmaWVyICYmIHRoaXMuX2hhc1ZlcmlmaWVyKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3QudmVyaWZpZXIub24oJ3Nob3dfc2FzJywgdGhpcy5fdXBkYXRlVmVyaWZpZXJTdGF0ZSk7XHJcbiAgICAgICAgICAgIHJlcXVlc3QudmVyaWZpZXIub24oJ3Nob3dfcmVjaXByb2NhdGVfcXInLCB0aGlzLl91cGRhdGVWZXJpZmllclN0YXRlKTtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIC8vIG9uIHRoZSByZXF1ZXN0ZXIgc2lkZSwgdGhpcyBpcyBhbHNvIGF3YWl0ZWQgaW4gX3N0YXJ0U0FTLFxyXG4gICAgICAgICAgICAgICAgLy8gYnV0IHRoYXQncyBvayBhcyB2ZXJpZnkgc2hvdWxkIHJldHVybiB0aGUgc2FtZSBwcm9taXNlLlxyXG4gICAgICAgICAgICAgICAgYXdhaXQgcmVxdWVzdC52ZXJpZmllci52ZXJpZnkoKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiZXJyb3IgdmVyaWZ5XCIsIGVycik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IHtyZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgcmVxdWVzdC5vbihcImNoYW5nZVwiLCB0aGlzLl9vblJlcXVlc3RDaGFuZ2UpO1xyXG4gICAgICAgIGlmIChyZXF1ZXN0LnZlcmlmaWVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtyZXF1ZXN0fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgICAgIGNvbnN0IHtzYXNFdmVudCwgcmVjaXByb2NhdGVRUkV2ZW50fSA9IHJlcXVlc3QudmVyaWZpZXI7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nhc0V2ZW50LCByZWNpcHJvY2F0ZVFSRXZlbnR9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fb25SZXF1ZXN0Q2hhbmdlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgY29uc3Qge3JlcXVlc3R9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAocmVxdWVzdC52ZXJpZmllcikge1xyXG4gICAgICAgICAgICByZXF1ZXN0LnZlcmlmaWVyLm9mZignc2hvd19zYXMnLCB0aGlzLl91cGRhdGVWZXJpZmllclN0YXRlKTtcclxuICAgICAgICAgICAgcmVxdWVzdC52ZXJpZmllci5vZmYoJ3Nob3dfcmVjaXByb2NhdGVfcXInLCB0aGlzLl91cGRhdGVWZXJpZmllclN0YXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmVxdWVzdC5vZmYoXCJjaGFuZ2VcIiwgdGhpcy5fb25SZXF1ZXN0Q2hhbmdlKTtcclxuICAgIH1cclxufVxyXG4iXX0=