"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

var sdk = _interopRequireWildcard(require("../../../index"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

class AccessibleTooltipButton extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "state", {
      hover: false
    });
    (0, _defineProperty2.default)(this, "onMouseOver", () => {
      this.setState({
        hover: true
      });
    });
    (0, _defineProperty2.default)(this, "onMouseOut", () => {
      this.setState({
        hover: false
      });
    });
  }

  render() {
    const Tooltip = sdk.getComponent("elements.Tooltip");
    const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
    const _this$props = this.props,
          {
      title,
      children
    } = _this$props,
          props = (0, _objectWithoutProperties2.default)(_this$props, ["title", "children"]);
    const tip = this.state.hover ? _react.default.createElement(Tooltip, {
      className: "mx_AccessibleTooltipButton_container",
      tooltipClassName: "mx_AccessibleTooltipButton_tooltip",
      label: title
    }) : _react.default.createElement("div", null);
    return _react.default.createElement(AccessibleButton, (0, _extends2.default)({}, props, {
      onMouseOver: this.onMouseOver,
      onMouseOut: this.onMouseOut,
      "aria-label": title
    }), children, tip);
  }

}

exports.default = AccessibleTooltipButton;
(0, _defineProperty2.default)(AccessibleTooltipButton, "propTypes", _objectSpread({}, _AccessibleButton.default.propTypes, {
  // The tooltip to render on hover
  title: _propTypes.default.string.isRequired
}));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0FjY2Vzc2libGVUb29sdGlwQnV0dG9uLmpzIl0sIm5hbWVzIjpbIkFjY2Vzc2libGVUb29sdGlwQnV0dG9uIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiaG92ZXIiLCJzZXRTdGF0ZSIsInJlbmRlciIsIlRvb2x0aXAiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJBY2Nlc3NpYmxlQnV0dG9uIiwicHJvcHMiLCJ0aXRsZSIsImNoaWxkcmVuIiwidGlwIiwic3RhdGUiLCJvbk1vdXNlT3ZlciIsIm9uTW91c2VPdXQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFFQTs7QUFDQTs7Ozs7O0FBRWUsTUFBTUEsdUJBQU4sU0FBc0NDLGVBQU1DLGFBQTVDLENBQTBEO0FBQUE7QUFBQTtBQUFBLGlEQU83RDtBQUNKQyxNQUFBQSxLQUFLLEVBQUU7QUFESCxLQVA2RDtBQUFBLHVEQVd2RCxNQUFNO0FBQ2hCLFdBQUtDLFFBQUwsQ0FBYztBQUNWRCxRQUFBQSxLQUFLLEVBQUU7QUFERyxPQUFkO0FBR0gsS0Fmb0U7QUFBQSxzREFpQnhELE1BQU07QUFDZixXQUFLQyxRQUFMLENBQWM7QUFDVkQsUUFBQUEsS0FBSyxFQUFFO0FBREcsT0FBZDtBQUdILEtBckJvRTtBQUFBOztBQXVCckVFLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFVBQU1DLGdCQUFnQixHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBRUEsd0JBQW9DLEtBQUtFLEtBQXpDO0FBQUEsVUFBTTtBQUFDQyxNQUFBQSxLQUFEO0FBQVFDLE1BQUFBO0FBQVIsS0FBTjtBQUFBLFVBQTJCRixLQUEzQjtBQUVBLFVBQU1HLEdBQUcsR0FBRyxLQUFLQyxLQUFMLENBQVdYLEtBQVgsR0FBbUIsNkJBQUMsT0FBRDtBQUMzQixNQUFBLFNBQVMsRUFBQyxzQ0FEaUI7QUFFM0IsTUFBQSxnQkFBZ0IsRUFBQyxvQ0FGVTtBQUczQixNQUFBLEtBQUssRUFBRVE7QUFIb0IsTUFBbkIsR0FJUCx5Q0FKTDtBQUtBLFdBQ0ksNkJBQUMsZ0JBQUQsNkJBQXNCRCxLQUF0QjtBQUE2QixNQUFBLFdBQVcsRUFBRSxLQUFLSyxXQUEvQztBQUE0RCxNQUFBLFVBQVUsRUFBRSxLQUFLQyxVQUE3RTtBQUF5RixvQkFBWUw7QUFBckcsUUFDTUMsUUFETixFQUVNQyxHQUZOLENBREo7QUFNSDs7QUF4Q29FOzs7OEJBQXBEYix1QixpQ0FFVlMsMEJBQWlCUSxTO0FBQ3BCO0FBQ0FOLEVBQUFBLEtBQUssRUFBRU8sbUJBQVVDLE1BQVYsQ0FBaUJDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuXHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuL0FjY2Vzc2libGVCdXR0b25cIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWNjZXNzaWJsZVRvb2x0aXBCdXR0b24gZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLi4uQWNjZXNzaWJsZUJ1dHRvbi5wcm9wVHlwZXMsXHJcbiAgICAgICAgLy8gVGhlIHRvb2x0aXAgdG8gcmVuZGVyIG9uIGhvdmVyXHJcbiAgICAgICAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgaG92ZXI6IGZhbHNlLFxyXG4gICAgfTtcclxuXHJcbiAgICBvbk1vdXNlT3ZlciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgaG92ZXI6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uTW91c2VPdXQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGhvdmVyOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVG9vbHRpcFwiKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b25cIik7XHJcblxyXG4gICAgICAgIGNvbnN0IHt0aXRsZSwgY2hpbGRyZW4sIC4uLnByb3BzfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpcCA9IHRoaXMuc3RhdGUuaG92ZXIgPyA8VG9vbHRpcFxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BY2Nlc3NpYmxlVG9vbHRpcEJ1dHRvbl9jb250YWluZXJcIlxyXG4gICAgICAgICAgICB0b29sdGlwQ2xhc3NOYW1lPVwibXhfQWNjZXNzaWJsZVRvb2x0aXBCdXR0b25fdG9vbHRpcFwiXHJcbiAgICAgICAgICAgIGxhYmVsPXt0aXRsZX1cclxuICAgICAgICAvPiA6IDxkaXYgLz47XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gey4uLnByb3BzfSBvbk1vdXNlT3Zlcj17dGhpcy5vbk1vdXNlT3Zlcn0gb25Nb3VzZU91dD17dGhpcy5vbk1vdXNlT3V0fSBhcmlhLWxhYmVsPXt0aXRsZX0+XHJcbiAgICAgICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgIHsgdGlwIH1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19