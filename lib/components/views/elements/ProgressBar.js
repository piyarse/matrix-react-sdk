"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'ProgressBar',
  propTypes: {
    value: _propTypes.default.number,
    max: _propTypes.default.number
  },
  render: function () {
    // Would use an HTML5 progress tag but if that doesn't animate if you
    // use the HTML attributes rather than styles
    const progressStyle = {
      width: this.props.value / this.props.max * 100 + "%"
    };
    return _react.default.createElement("div", {
      className: "mx_ProgressBar"
    }, _react.default.createElement("div", {
      className: "mx_ProgressBar_fill",
      style: progressStyle
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1Byb2dyZXNzQmFyLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwidmFsdWUiLCJQcm9wVHlwZXMiLCJudW1iZXIiLCJtYXgiLCJyZW5kZXIiLCJwcm9ncmVzc1N0eWxlIiwid2lkdGgiLCJwcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQW5CQTs7Ozs7Ozs7Ozs7Ozs7OztlQXFCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxhQURlO0FBRTVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsS0FBSyxFQUFFQyxtQkFBVUMsTUFEVjtBQUVQQyxJQUFBQSxHQUFHLEVBQUVGLG1CQUFVQztBQUZSLEdBRmlCO0FBTzVCRSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmO0FBQ0E7QUFDQSxVQUFNQyxhQUFhLEdBQUc7QUFDbEJDLE1BQUFBLEtBQUssRUFBSSxLQUFLQyxLQUFMLENBQVdQLEtBQVgsR0FBbUIsS0FBS08sS0FBTCxDQUFXSixHQUEvQixHQUFzQyxHQUF2QyxHQUE0QztBQURqQyxLQUF0QjtBQUdBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQWdDO0FBQUssTUFBQSxTQUFTLEVBQUMscUJBQWY7QUFBcUMsTUFBQSxLQUFLLEVBQUVFO0FBQTVDLE1BQWhDLENBREo7QUFHSDtBQWhCMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1Byb2dyZXNzQmFyJyxcclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHZhbHVlOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIG1heDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBXb3VsZCB1c2UgYW4gSFRNTDUgcHJvZ3Jlc3MgdGFnIGJ1dCBpZiB0aGF0IGRvZXNuJ3QgYW5pbWF0ZSBpZiB5b3VcclxuICAgICAgICAvLyB1c2UgdGhlIEhUTUwgYXR0cmlidXRlcyByYXRoZXIgdGhhbiBzdHlsZXNcclxuICAgICAgICBjb25zdCBwcm9ncmVzc1N0eWxlID0ge1xyXG4gICAgICAgICAgICB3aWR0aDogKCh0aGlzLnByb3BzLnZhbHVlIC8gdGhpcy5wcm9wcy5tYXgpICogMTAwKStcIiVcIixcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUHJvZ3Jlc3NCYXJcIj48ZGl2IGNsYXNzTmFtZT1cIm14X1Byb2dyZXNzQmFyX2ZpbGxcIiBzdHlsZT17cHJvZ3Jlc3NTdHlsZX0+PC9kaXY+PC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=