"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

/*
Copyright 2017 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'MessageSpinner',
  render: function () {
    const w = this.props.w || 32;
    const h = this.props.h || 32;
    const imgClass = this.props.imgClassName || "";
    const msg = this.props.msg || "Loading...";
    return _react.default.createElement("div", {
      className: "mx_Spinner"
    }, _react.default.createElement("div", {
      className: "mx_Spinner_Msg"
    }, msg), "\xA0", _react.default.createElement("img", {
      src: require("../../../../res/img/spinner.gif"),
      width: w,
      height: h,
      className: imgClass
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL01lc3NhZ2VTcGlubmVyLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicmVuZGVyIiwidyIsInByb3BzIiwiaCIsImltZ0NsYXNzIiwiaW1nQ2xhc3NOYW1lIiwibXNnIiwicmVxdWlyZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQWpCQTs7Ozs7Ozs7Ozs7Ozs7O2VBbUJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLGdCQURlO0FBRzVCQyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLENBQUMsR0FBRyxLQUFLQyxLQUFMLENBQVdELENBQVgsSUFBZ0IsRUFBMUI7QUFDQSxVQUFNRSxDQUFDLEdBQUcsS0FBS0QsS0FBTCxDQUFXQyxDQUFYLElBQWdCLEVBQTFCO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLEtBQUtGLEtBQUwsQ0FBV0csWUFBWCxJQUEyQixFQUE1QztBQUNBLFVBQU1DLEdBQUcsR0FBRyxLQUFLSixLQUFMLENBQVdJLEdBQVgsSUFBa0IsWUFBOUI7QUFDQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUFrQ0EsR0FBbEMsQ0FESixVQUVJO0FBQUssTUFBQSxHQUFHLEVBQUVDLE9BQU8sQ0FBQyxpQ0FBRCxDQUFqQjtBQUFzRCxNQUFBLEtBQUssRUFBRU4sQ0FBN0Q7QUFBZ0UsTUFBQSxNQUFNLEVBQUVFLENBQXhFO0FBQTJFLE1BQUEsU0FBUyxFQUFFQztBQUF0RixNQUZKLENBREo7QUFNSDtBQWQyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ01lc3NhZ2VTcGlubmVyJyxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHcgPSB0aGlzLnByb3BzLncgfHwgMzI7XHJcbiAgICAgICAgY29uc3QgaCA9IHRoaXMucHJvcHMuaCB8fCAzMjtcclxuICAgICAgICBjb25zdCBpbWdDbGFzcyA9IHRoaXMucHJvcHMuaW1nQ2xhc3NOYW1lIHx8IFwiXCI7XHJcbiAgICAgICAgY29uc3QgbXNnID0gdGhpcy5wcm9wcy5tc2cgfHwgXCJMb2FkaW5nLi4uXCI7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TcGlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NwaW5uZXJfTXNnXCI+eyBtc2cgfTwvZGl2PiZuYnNwO1xyXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL3NwaW5uZXIuZ2lmXCIpfSB3aWR0aD17d30gaGVpZ2h0PXtofSBjbGFzc05hbWU9e2ltZ0NsYXNzfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==