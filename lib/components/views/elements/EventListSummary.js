"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MemberAvatar = _interopRequireDefault(require("../avatars/MemberAvatar"));

var _languageHandler = require("../../../languageHandler");

var _matrixJsSdk = require("matrix-js-sdk");

var _useStateToggle = require("../../../hooks/useStateToggle");

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const EventListSummary = ({
  events,
  children,
  threshold = 3,
  onToggle,
  startExpanded,
  summaryMembers = [],
  summaryText
}) => {
  const [expanded, toggleExpanded] = (0, _useStateToggle.useStateToggle)(startExpanded); // Whenever expanded changes call onToggle

  (0, _react.useEffect)(() => {
    if (onToggle) {
      onToggle();
    }
  }, [expanded]); // eslint-disable-line react-hooks/exhaustive-deps

  const eventIds = events.map(e => e.getId()).join(','); // If we are only given few events then just pass them through

  if (events.length < threshold) {
    return _react.default.createElement("div", {
      className: "mx_EventListSummary",
      "data-scroll-tokens": eventIds
    }, children);
  }

  let body;

  if (expanded) {
    body = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
      className: "mx_EventListSummary_line"
    }, "\xA0"), children);
  } else {
    const avatars = summaryMembers.map(m => _react.default.createElement(_MemberAvatar.default, {
      key: m.userId,
      member: m,
      width: 14,
      height: 14
    }));
    body = _react.default.createElement("div", {
      className: "mx_EventTile_line"
    }, _react.default.createElement("div", {
      className: "mx_EventTile_info"
    }, _react.default.createElement("span", {
      className: "mx_EventListSummary_avatars",
      onClick: toggleExpanded
    }, avatars), _react.default.createElement("span", {
      className: "mx_TextualEvent mx_EventListSummary_summary"
    }, summaryText)));
  }

  return _react.default.createElement("div", {
    className: "mx_EventListSummary",
    "data-scroll-tokens": eventIds
  }, _react.default.createElement(_AccessibleButton.default, {
    className: "mx_EventListSummary_toggle",
    onClick: toggleExpanded,
    "aria-expanded": expanded
  }, expanded ? (0, _languageHandler._t)('collapse') : (0, _languageHandler._t)('expand')), body);
};

EventListSummary.propTypes = {
  // An array of member events to summarise
  events: _propTypes.default.arrayOf(_propTypes.default.instanceOf(_matrixJsSdk.MatrixEvent)).isRequired,
  // An array of EventTiles to render when expanded
  children: _propTypes.default.arrayOf(_propTypes.default.element).isRequired,
  // The minimum number of events needed to trigger summarisation
  threshold: _propTypes.default.number,
  // Called when the event list expansion is toggled
  onToggle: _propTypes.default.func,
  // Whether or not to begin with state.expanded=true
  startExpanded: _propTypes.default.bool,
  // The list of room members for which to show avatars next to the summary
  summaryMembers: _propTypes.default.arrayOf(_propTypes.default.instanceOf(_matrixJsSdk.RoomMember)),
  // The text to show as the summary of this event list
  summaryText: _propTypes.default.string.isRequired
};
var _default = EventListSummary;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0V2ZW50TGlzdFN1bW1hcnkuanMiXSwibmFtZXMiOlsiRXZlbnRMaXN0U3VtbWFyeSIsImV2ZW50cyIsImNoaWxkcmVuIiwidGhyZXNob2xkIiwib25Ub2dnbGUiLCJzdGFydEV4cGFuZGVkIiwic3VtbWFyeU1lbWJlcnMiLCJzdW1tYXJ5VGV4dCIsImV4cGFuZGVkIiwidG9nZ2xlRXhwYW5kZWQiLCJldmVudElkcyIsIm1hcCIsImUiLCJnZXRJZCIsImpvaW4iLCJsZW5ndGgiLCJib2R5IiwiYXZhdGFycyIsIm0iLCJ1c2VySWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheU9mIiwiaW5zdGFuY2VPZiIsIk1hdHJpeEV2ZW50IiwiaXNSZXF1aXJlZCIsImVsZW1lbnQiLCJudW1iZXIiLCJmdW5jIiwiYm9vbCIsIlJvb21NZW1iZXIiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBLE1BQU1BLGdCQUFnQixHQUFHLENBQUM7QUFBQ0MsRUFBQUEsTUFBRDtBQUFTQyxFQUFBQSxRQUFUO0FBQW1CQyxFQUFBQSxTQUFTLEdBQUMsQ0FBN0I7QUFBZ0NDLEVBQUFBLFFBQWhDO0FBQTBDQyxFQUFBQSxhQUExQztBQUF5REMsRUFBQUEsY0FBYyxHQUFDLEVBQXhFO0FBQTRFQyxFQUFBQTtBQUE1RSxDQUFELEtBQThGO0FBQ25ILFFBQU0sQ0FBQ0MsUUFBRCxFQUFXQyxjQUFYLElBQTZCLG9DQUFlSixhQUFmLENBQW5DLENBRG1ILENBR25IOztBQUNBLHdCQUFVLE1BQU07QUFDWixRQUFJRCxRQUFKLEVBQWM7QUFDVkEsTUFBQUEsUUFBUTtBQUNYO0FBQ0osR0FKRCxFQUlHLENBQUNJLFFBQUQsQ0FKSCxFQUptSCxDQVFuRzs7QUFFaEIsUUFBTUUsUUFBUSxHQUFHVCxNQUFNLENBQUNVLEdBQVAsQ0FBWUMsQ0FBRCxJQUFPQSxDQUFDLENBQUNDLEtBQUYsRUFBbEIsRUFBNkJDLElBQTdCLENBQWtDLEdBQWxDLENBQWpCLENBVm1ILENBWW5IOztBQUNBLE1BQUliLE1BQU0sQ0FBQ2MsTUFBUCxHQUFnQlosU0FBcEIsRUFBK0I7QUFDM0IsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHFCQUFmO0FBQXFDLDRCQUFvQk87QUFBekQsT0FDTVIsUUFETixDQURKO0FBS0g7O0FBRUQsTUFBSWMsSUFBSjs7QUFDQSxNQUFJUixRQUFKLEVBQWM7QUFDVlEsSUFBQUEsSUFBSSxHQUFHLDZCQUFDLGNBQUQsQ0FBTyxRQUFQLFFBQ0g7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLGNBREcsRUFFRGQsUUFGQyxDQUFQO0FBSUgsR0FMRCxNQUtPO0FBQ0gsVUFBTWUsT0FBTyxHQUFHWCxjQUFjLENBQUNLLEdBQWYsQ0FBb0JPLENBQUQsSUFBTyw2QkFBQyxxQkFBRDtBQUFjLE1BQUEsR0FBRyxFQUFFQSxDQUFDLENBQUNDLE1BQXJCO0FBQTZCLE1BQUEsTUFBTSxFQUFFRCxDQUFyQztBQUF3QyxNQUFBLEtBQUssRUFBRSxFQUEvQztBQUFtRCxNQUFBLE1BQU0sRUFBRTtBQUEzRCxNQUExQixDQUFoQjtBQUNBRixJQUFBQSxJQUFJLEdBQ0E7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQyw2QkFBaEI7QUFBOEMsTUFBQSxPQUFPLEVBQUVQO0FBQXZELE9BQ01RLE9BRE4sQ0FESixFQUlJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDTVYsV0FETixDQUpKLENBREosQ0FESjtBQVlIOztBQUVELFNBQ0k7QUFBSyxJQUFBLFNBQVMsRUFBQyxxQkFBZjtBQUFxQywwQkFBb0JHO0FBQXpELEtBQ0ksNkJBQUMseUJBQUQ7QUFBa0IsSUFBQSxTQUFTLEVBQUMsNEJBQTVCO0FBQXlELElBQUEsT0FBTyxFQUFFRCxjQUFsRTtBQUFrRixxQkFBZUQ7QUFBakcsS0FDTUEsUUFBUSxHQUFHLHlCQUFHLFVBQUgsQ0FBSCxHQUFvQix5QkFBRyxRQUFILENBRGxDLENBREosRUFJTVEsSUFKTixDQURKO0FBUUgsQ0FuREQ7O0FBcURBaEIsZ0JBQWdCLENBQUNvQixTQUFqQixHQUE2QjtBQUN6QjtBQUNBbkIsRUFBQUEsTUFBTSxFQUFFb0IsbUJBQVVDLE9BQVYsQ0FBa0JELG1CQUFVRSxVQUFWLENBQXFCQyx3QkFBckIsQ0FBbEIsRUFBcURDLFVBRnBDO0FBR3pCO0FBQ0F2QixFQUFBQSxRQUFRLEVBQUVtQixtQkFBVUMsT0FBVixDQUFrQkQsbUJBQVVLLE9BQTVCLEVBQXFDRCxVQUp0QjtBQUt6QjtBQUNBdEIsRUFBQUEsU0FBUyxFQUFFa0IsbUJBQVVNLE1BTkk7QUFPekI7QUFDQXZCLEVBQUFBLFFBQVEsRUFBRWlCLG1CQUFVTyxJQVJLO0FBU3pCO0FBQ0F2QixFQUFBQSxhQUFhLEVBQUVnQixtQkFBVVEsSUFWQTtBQVl6QjtBQUNBdkIsRUFBQUEsY0FBYyxFQUFFZSxtQkFBVUMsT0FBVixDQUFrQkQsbUJBQVVFLFVBQVYsQ0FBcUJPLHVCQUFyQixDQUFsQixDQWJTO0FBY3pCO0FBQ0F2QixFQUFBQSxXQUFXLEVBQUVjLG1CQUFVVSxNQUFWLENBQWlCTjtBQWZMLENBQTdCO2VBa0JlekIsZ0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCwge3VzZUVmZmVjdH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgTWVtYmVyQXZhdGFyIGZyb20gJy4uL2F2YXRhcnMvTWVtYmVyQXZhdGFyJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge01hdHJpeEV2ZW50LCBSb29tTWVtYmVyfSBmcm9tIFwibWF0cml4LWpzLXNka1wiO1xyXG5pbXBvcnQge3VzZVN0YXRlVG9nZ2xlfSBmcm9tIFwiLi4vLi4vLi4vaG9va3MvdXNlU3RhdGVUb2dnbGVcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4vQWNjZXNzaWJsZUJ1dHRvblwiO1xyXG5cclxuY29uc3QgRXZlbnRMaXN0U3VtbWFyeSA9ICh7ZXZlbnRzLCBjaGlsZHJlbiwgdGhyZXNob2xkPTMsIG9uVG9nZ2xlLCBzdGFydEV4cGFuZGVkLCBzdW1tYXJ5TWVtYmVycz1bXSwgc3VtbWFyeVRleHR9KSA9PiB7XHJcbiAgICBjb25zdCBbZXhwYW5kZWQsIHRvZ2dsZUV4cGFuZGVkXSA9IHVzZVN0YXRlVG9nZ2xlKHN0YXJ0RXhwYW5kZWQpO1xyXG5cclxuICAgIC8vIFdoZW5ldmVyIGV4cGFuZGVkIGNoYW5nZXMgY2FsbCBvblRvZ2dsZVxyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBpZiAob25Ub2dnbGUpIHtcclxuICAgICAgICAgICAgb25Ub2dnbGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbZXhwYW5kZWRdKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSByZWFjdC1ob29rcy9leGhhdXN0aXZlLWRlcHNcclxuXHJcbiAgICBjb25zdCBldmVudElkcyA9IGV2ZW50cy5tYXAoKGUpID0+IGUuZ2V0SWQoKSkuam9pbignLCcpO1xyXG5cclxuICAgIC8vIElmIHdlIGFyZSBvbmx5IGdpdmVuIGZldyBldmVudHMgdGhlbiBqdXN0IHBhc3MgdGhlbSB0aHJvdWdoXHJcbiAgICBpZiAoZXZlbnRzLmxlbmd0aCA8IHRocmVzaG9sZCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRMaXN0U3VtbWFyeVwiIGRhdGEtc2Nyb2xsLXRva2Vucz17ZXZlbnRJZHN9PlxyXG4gICAgICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGJvZHk7XHJcbiAgICBpZiAoZXhwYW5kZWQpIHtcclxuICAgICAgICBib2R5ID0gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V2ZW50TGlzdFN1bW1hcnlfbGluZVwiPiZuYnNwOzwvZGl2PlxyXG4gICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgYXZhdGFycyA9IHN1bW1hcnlNZW1iZXJzLm1hcCgobSkgPT4gPE1lbWJlckF2YXRhciBrZXk9e20udXNlcklkfSBtZW1iZXI9e219IHdpZHRoPXsxNH0gaGVpZ2h0PXsxNH0gLz4pO1xyXG4gICAgICAgIGJvZHkgPSAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2xpbmVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2luZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9FdmVudExpc3RTdW1tYXJ5X2F2YXRhcnNcIiBvbkNsaWNrPXt0b2dnbGVFeHBhbmRlZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgYXZhdGFycyB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1RleHR1YWxFdmVudCBteF9FdmVudExpc3RTdW1tYXJ5X3N1bW1hcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBzdW1tYXJ5VGV4dCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V2ZW50TGlzdFN1bW1hcnlcIiBkYXRhLXNjcm9sbC10b2tlbnM9e2V2ZW50SWRzfT5cclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfRXZlbnRMaXN0U3VtbWFyeV90b2dnbGVcIiBvbkNsaWNrPXt0b2dnbGVFeHBhbmRlZH0gYXJpYS1leHBhbmRlZD17ZXhwYW5kZWR9PlxyXG4gICAgICAgICAgICAgICAgeyBleHBhbmRlZCA/IF90KCdjb2xsYXBzZScpIDogX3QoJ2V4cGFuZCcpIH1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICB7IGJvZHkgfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxufTtcclxuXHJcbkV2ZW50TGlzdFN1bW1hcnkucHJvcFR5cGVzID0ge1xyXG4gICAgLy8gQW4gYXJyYXkgb2YgbWVtYmVyIGV2ZW50cyB0byBzdW1tYXJpc2VcclxuICAgIGV2ZW50czogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLmluc3RhbmNlT2YoTWF0cml4RXZlbnQpKS5pc1JlcXVpcmVkLFxyXG4gICAgLy8gQW4gYXJyYXkgb2YgRXZlbnRUaWxlcyB0byByZW5kZXIgd2hlbiBleHBhbmRlZFxyXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5lbGVtZW50KS5pc1JlcXVpcmVkLFxyXG4gICAgLy8gVGhlIG1pbmltdW0gbnVtYmVyIG9mIGV2ZW50cyBuZWVkZWQgdG8gdHJpZ2dlciBzdW1tYXJpc2F0aW9uXHJcbiAgICB0aHJlc2hvbGQ6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgZXZlbnQgbGlzdCBleHBhbnNpb24gaXMgdG9nZ2xlZFxyXG4gICAgb25Ub2dnbGU6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgLy8gV2hldGhlciBvciBub3QgdG8gYmVnaW4gd2l0aCBzdGF0ZS5leHBhbmRlZD10cnVlXHJcbiAgICBzdGFydEV4cGFuZGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuXHJcbiAgICAvLyBUaGUgbGlzdCBvZiByb29tIG1lbWJlcnMgZm9yIHdoaWNoIHRvIHNob3cgYXZhdGFycyBuZXh0IHRvIHRoZSBzdW1tYXJ5XHJcbiAgICBzdW1tYXJ5TWVtYmVyczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLmluc3RhbmNlT2YoUm9vbU1lbWJlcikpLFxyXG4gICAgLy8gVGhlIHRleHQgdG8gc2hvdyBhcyB0aGUgc3VtbWFyeSBvZiB0aGlzIGV2ZW50IGxpc3RcclxuICAgIHN1bW1hcnlUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBFdmVudExpc3RTdW1tYXJ5O1xyXG4iXX0=