"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

var _languageHandler = require("../../../languageHandler");

var _Keyboard = require("../../../Keyboard");

/*
Copyright 2017 Vector Creations Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class MenuOption extends _react.default.Component {
  constructor(props) {
    super(props);
    this._onMouseEnter = this._onMouseEnter.bind(this);
    this._onClick = this._onClick.bind(this);
  }

  _onMouseEnter() {
    this.props.onMouseEnter(this.props.dropdownKey);
  }

  _onClick(e) {
    e.preventDefault();
    e.stopPropagation();
    this.props.onClick(this.props.dropdownKey);
  }

  render() {
    const optClasses = (0, _classnames.default)({
      mx_Dropdown_option: true,
      mx_Dropdown_option_highlight: this.props.highlighted
    });
    return _react.default.createElement("div", {
      id: this.props.id,
      className: optClasses,
      onClick: this._onClick,
      onMouseEnter: this._onMouseEnter,
      role: "option",
      "aria-selected": this.props.highlighted,
      ref: this.props.inputRef
    }, this.props.children);
  }

}

(0, _defineProperty2.default)(MenuOption, "defaultProps", {
  disabled: false
});
MenuOption.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]),
  highlighted: _propTypes.default.bool,
  dropdownKey: _propTypes.default.string,
  onClick: _propTypes.default.func.isRequired,
  onMouseEnter: _propTypes.default.func.isRequired,
  inputRef: _propTypes.default.any
};
/*
 * Reusable dropdown select control, akin to react-select,
 * but somewhat simpler as react-select is 79KB of minified
 * javascript.
 *
 * TODO: Port NetworkDropdown to use this.
 */

class Dropdown extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onInputKeyDown", e => {
      let handled = true; // These keys don't generate keypress events and so needs to be on keyup

      switch (e.key) {
        case _Keyboard.Key.ENTER:
          this.props.onOptionChange(this.state.highlightedOption);
        // fallthrough

        case _Keyboard.Key.ESCAPE:
          this._close();

          break;

        case _Keyboard.Key.ARROW_DOWN:
          this.setState({
            highlightedOption: this._nextOption(this.state.highlightedOption)
          });
          break;

        case _Keyboard.Key.ARROW_UP:
          this.setState({
            highlightedOption: this._prevOption(this.state.highlightedOption)
          });
          break;

        default:
          handled = false;
      }

      if (handled) {
        e.preventDefault();
        e.stopPropagation();
      }
    });
    this.dropdownRootElement = null;
    this.ignoreEvent = null;
    this._onInputClick = this._onInputClick.bind(this);
    this._onRootClick = this._onRootClick.bind(this);
    this._onDocumentClick = this._onDocumentClick.bind(this);
    this._onMenuOptionClick = this._onMenuOptionClick.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._collectRoot = this._collectRoot.bind(this);
    this._collectInputTextBox = this._collectInputTextBox.bind(this);
    this._setHighlightedOption = this._setHighlightedOption.bind(this);
    this.inputTextBox = null;

    this._reindexChildren(this.props.children);

    const firstChild = _react.default.Children.toArray(props.children)[0];

    this.state = {
      // True if the menu is dropped-down
      expanded: false,
      // The key of the highlighted option
      // (the option that would become selected if you pressed enter)
      highlightedOption: firstChild ? firstChild.key : null,
      // the current search query
      searchQuery: ''
    };
  } // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs


  UNSAFE_componentWillMount() {
    // eslint-disable-line camelcase
    this._button = (0, _react.createRef)(); // Listen for all clicks on the document so we can close the
    // menu when the user clicks somewhere else

    document.addEventListener('click', this._onDocumentClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this._onDocumentClick, false);
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(nextProps) {
    // eslint-disable-line camelcase
    if (!nextProps.children || nextProps.children.length === 0) {
      return;
    }

    this._reindexChildren(nextProps.children);

    const firstChild = nextProps.children[0];
    this.setState({
      highlightedOption: firstChild ? firstChild.key : null
    });
  }

  _reindexChildren(children) {
    this.childrenByKey = {};

    _react.default.Children.forEach(children, child => {
      this.childrenByKey[child.key] = child;
    });
  }

  _onDocumentClick(ev) {
    // Close the dropdown if the user clicks anywhere that isn't
    // within our root element
    if (ev !== this.ignoreEvent) {
      this.setState({
        expanded: false
      });
    }
  }

  _onRootClick(ev) {
    // This captures any clicks that happen within our elements,
    // such that we can then ignore them when they're seen by the
    // click listener on the document handler, ie. not close the
    // dropdown immediately after opening it.
    // NB. We can't just stopPropagation() because then the event
    // doesn't reach the React onClick().
    this.ignoreEvent = ev;
  }

  _onInputClick(ev) {
    if (this.props.disabled) return;

    if (!this.state.expanded) {
      this.setState({
        expanded: true
      });
      ev.preventDefault();
    }
  }

  _close() {
    this.setState({
      expanded: false
    }); // their focus was on the input, its getting unmounted, move it to the button

    if (this._button.current) {
      this._button.current.focus();
    }
  }

  _onMenuOptionClick(dropdownKey) {
    this._close();

    this.props.onOptionChange(dropdownKey);
  }

  _onInputChange(e) {
    this.setState({
      searchQuery: e.target.value
    });

    if (this.props.onSearchChange) {
      this.props.onSearchChange(e.target.value);
    }
  }

  _collectRoot(e) {
    if (this.dropdownRootElement) {
      this.dropdownRootElement.removeEventListener('click', this._onRootClick, false);
    }

    if (e) {
      e.addEventListener('click', this._onRootClick, false);
    }

    this.dropdownRootElement = e;
  }

  _collectInputTextBox(e) {
    this.inputTextBox = e;
    if (e) e.focus();
  }

  _setHighlightedOption(optionKey) {
    this.setState({
      highlightedOption: optionKey
    });
  }

  _nextOption(optionKey) {
    const keys = Object.keys(this.childrenByKey);
    const index = keys.indexOf(optionKey);
    return keys[(index + 1) % keys.length];
  }

  _prevOption(optionKey) {
    const keys = Object.keys(this.childrenByKey);
    const index = keys.indexOf(optionKey);
    return keys[(index - 1) % keys.length];
  }

  _scrollIntoView(node) {
    if (node) {
      node.scrollIntoView({
        block: "nearest",
        behavior: "auto"
      });
    }
  }

  _getMenuOptions() {
    const options = _react.default.Children.map(this.props.children, child => {
      const highlighted = this.state.highlightedOption === child.key;
      return _react.default.createElement(MenuOption, {
        id: "".concat(this.props.id, "__").concat(child.key),
        key: child.key,
        dropdownKey: child.key,
        highlighted: highlighted,
        onMouseEnter: this._setHighlightedOption,
        onClick: this._onMenuOptionClick,
        inputRef: highlighted ? this._scrollIntoView : undefined
      }, child);
    });

    if (options.length === 0) {
      return [_react.default.createElement("div", {
        key: "0",
        className: "mx_Dropdown_option",
        role: "option"
      }, (0, _languageHandler._t)("No results"))];
    }

    return options;
  }

  render() {
    let currentValue;
    const menuStyle = {};
    if (this.props.menuWidth) menuStyle.width = this.props.menuWidth;
    let menu;

    if (this.state.expanded) {
      if (this.props.searchEnabled) {
        currentValue = _react.default.createElement("input", {
          type: "text",
          className: "mx_Dropdown_option",
          ref: this._collectInputTextBox,
          onKeyDown: this._onInputKeyDown,
          onChange: this._onInputChange,
          value: this.state.searchQuery,
          role: "combobox",
          "aria-autocomplete": "list",
          "aria-activedescendant": "".concat(this.props.id, "__").concat(this.state.highlightedOption),
          "aria-owns": "".concat(this.props.id, "_listbox"),
          "aria-disabled": this.props.disabled,
          "aria-label": this.props.label
        });
      }

      menu = _react.default.createElement("div", {
        className: "mx_Dropdown_menu",
        style: menuStyle,
        role: "listbox",
        id: "".concat(this.props.id, "_listbox")
      }, this._getMenuOptions());
    }

    if (!currentValue) {
      const selectedChild = this.props.getShortOption ? this.props.getShortOption(this.props.value) : this.childrenByKey[this.props.value];
      currentValue = _react.default.createElement("div", {
        className: "mx_Dropdown_option",
        id: "".concat(this.props.id, "_value")
      }, selectedChild);
    }

    const dropdownClasses = {
      mx_Dropdown: true,
      mx_Dropdown_disabled: this.props.disabled
    };

    if (this.props.className) {
      dropdownClasses[this.props.className] = true;
    } // Note the menu sits inside the AccessibleButton div so it's anchored
    // to the input, but overflows below it. The root contains both.


    return _react.default.createElement("div", {
      className: (0, _classnames.default)(dropdownClasses),
      ref: this._collectRoot
    }, _react.default.createElement(_AccessibleButton.default, {
      className: "mx_Dropdown_input mx_no_textinput",
      onClick: this._onInputClick,
      "aria-haspopup": "listbox",
      "aria-expanded": this.state.expanded,
      disabled: this.props.disabled,
      inputRef: this._button,
      "aria-label": this.props.label,
      "aria-describedby": "".concat(this.props.id, "_value")
    }, currentValue, _react.default.createElement("span", {
      className: "mx_Dropdown_arrow"
    }), menu));
  }

}

exports.default = Dropdown;
Dropdown.propTypes = {
  id: _propTypes.default.string.isRequired,
  // The width that the dropdown should be. If specified,
  // the dropped-down part of the menu will be set to this
  // width.
  menuWidth: _propTypes.default.number,
  // Called when the selected option changes
  onOptionChange: _propTypes.default.func.isRequired,
  // Called when the value of the search field changes
  onSearchChange: _propTypes.default.func,
  searchEnabled: _propTypes.default.bool,
  // Function that, given the key of an option, returns
  // a node representing that option to be displayed in the
  // box itself as the currently-selected option (ie. as
  // opposed to in the actual dropped-down part). If
  // unspecified, the appropriate child element is used as
  // in the dropped-down menu.
  getShortOption: _propTypes.default.func,
  value: _propTypes.default.string,
  // negative for consistency with HTML
  disabled: _propTypes.default.bool,
  // ARIA label
  label: _propTypes.default.string.isRequired
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0Ryb3Bkb3duLmpzIl0sIm5hbWVzIjpbIk1lbnVPcHRpb24iLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJfb25Nb3VzZUVudGVyIiwiYmluZCIsIl9vbkNsaWNrIiwib25Nb3VzZUVudGVyIiwiZHJvcGRvd25LZXkiLCJlIiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJvbkNsaWNrIiwicmVuZGVyIiwib3B0Q2xhc3NlcyIsIm14X0Ryb3Bkb3duX29wdGlvbiIsIm14X0Ryb3Bkb3duX29wdGlvbl9oaWdobGlnaHQiLCJoaWdobGlnaHRlZCIsImlkIiwiaW5wdXRSZWYiLCJjaGlsZHJlbiIsImRpc2FibGVkIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib25lT2ZUeXBlIiwiYXJyYXlPZiIsIm5vZGUiLCJib29sIiwic3RyaW5nIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJhbnkiLCJEcm9wZG93biIsImhhbmRsZWQiLCJrZXkiLCJLZXkiLCJFTlRFUiIsIm9uT3B0aW9uQ2hhbmdlIiwic3RhdGUiLCJoaWdobGlnaHRlZE9wdGlvbiIsIkVTQ0FQRSIsIl9jbG9zZSIsIkFSUk9XX0RPV04iLCJzZXRTdGF0ZSIsIl9uZXh0T3B0aW9uIiwiQVJST1dfVVAiLCJfcHJldk9wdGlvbiIsImRyb3Bkb3duUm9vdEVsZW1lbnQiLCJpZ25vcmVFdmVudCIsIl9vbklucHV0Q2xpY2siLCJfb25Sb290Q2xpY2siLCJfb25Eb2N1bWVudENsaWNrIiwiX29uTWVudU9wdGlvbkNsaWNrIiwiX29uSW5wdXRDaGFuZ2UiLCJfY29sbGVjdFJvb3QiLCJfY29sbGVjdElucHV0VGV4dEJveCIsIl9zZXRIaWdobGlnaHRlZE9wdGlvbiIsImlucHV0VGV4dEJveCIsIl9yZWluZGV4Q2hpbGRyZW4iLCJmaXJzdENoaWxkIiwiQ2hpbGRyZW4iLCJ0b0FycmF5IiwiZXhwYW5kZWQiLCJzZWFyY2hRdWVyeSIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJfYnV0dG9uIiwiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMiLCJuZXh0UHJvcHMiLCJsZW5ndGgiLCJjaGlsZHJlbkJ5S2V5IiwiZm9yRWFjaCIsImNoaWxkIiwiZXYiLCJjdXJyZW50IiwiZm9jdXMiLCJ0YXJnZXQiLCJ2YWx1ZSIsIm9uU2VhcmNoQ2hhbmdlIiwib3B0aW9uS2V5Iiwia2V5cyIsIk9iamVjdCIsImluZGV4IiwiaW5kZXhPZiIsIl9zY3JvbGxJbnRvVmlldyIsInNjcm9sbEludG9WaWV3IiwiYmxvY2siLCJiZWhhdmlvciIsIl9nZXRNZW51T3B0aW9ucyIsIm9wdGlvbnMiLCJtYXAiLCJ1bmRlZmluZWQiLCJjdXJyZW50VmFsdWUiLCJtZW51U3R5bGUiLCJtZW51V2lkdGgiLCJ3aWR0aCIsIm1lbnUiLCJzZWFyY2hFbmFibGVkIiwiX29uSW5wdXRLZXlEb3duIiwibGFiZWwiLCJzZWxlY3RlZENoaWxkIiwiZ2V0U2hvcnRPcHRpb24iLCJkcm9wZG93bkNsYXNzZXMiLCJteF9Ecm9wZG93biIsIm14X0Ryb3Bkb3duX2Rpc2FibGVkIiwiY2xhc3NOYW1lIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsTUFBTUEsVUFBTixTQUF5QkMsZUFBTUMsU0FBL0IsQ0FBeUM7QUFDckNDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUtDLGFBQUwsR0FBcUIsS0FBS0EsYUFBTCxDQUFtQkMsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBckI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLEtBQUtBLFFBQUwsQ0FBY0QsSUFBZCxDQUFtQixJQUFuQixDQUFoQjtBQUNIOztBQU1ERCxFQUFBQSxhQUFhLEdBQUc7QUFDWixTQUFLRCxLQUFMLENBQVdJLFlBQVgsQ0FBd0IsS0FBS0osS0FBTCxDQUFXSyxXQUFuQztBQUNIOztBQUVERixFQUFBQSxRQUFRLENBQUNHLENBQUQsRUFBSTtBQUNSQSxJQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsSUFBQUEsQ0FBQyxDQUFDRSxlQUFGO0FBQ0EsU0FBS1IsS0FBTCxDQUFXUyxPQUFYLENBQW1CLEtBQUtULEtBQUwsQ0FBV0ssV0FBOUI7QUFDSDs7QUFFREssRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHLHlCQUFXO0FBQzFCQyxNQUFBQSxrQkFBa0IsRUFBRSxJQURNO0FBRTFCQyxNQUFBQSw0QkFBNEIsRUFBRSxLQUFLYixLQUFMLENBQVdjO0FBRmYsS0FBWCxDQUFuQjtBQUtBLFdBQU87QUFDSCxNQUFBLEVBQUUsRUFBRSxLQUFLZCxLQUFMLENBQVdlLEVBRFo7QUFFSCxNQUFBLFNBQVMsRUFBRUosVUFGUjtBQUdILE1BQUEsT0FBTyxFQUFFLEtBQUtSLFFBSFg7QUFJSCxNQUFBLFlBQVksRUFBRSxLQUFLRixhQUpoQjtBQUtILE1BQUEsSUFBSSxFQUFDLFFBTEY7QUFNSCx1QkFBZSxLQUFLRCxLQUFMLENBQVdjLFdBTnZCO0FBT0gsTUFBQSxHQUFHLEVBQUUsS0FBS2QsS0FBTCxDQUFXZ0I7QUFQYixPQVNELEtBQUtoQixLQUFMLENBQVdpQixRQVRWLENBQVA7QUFXSDs7QUF0Q29DOzs4QkFBbkNyQixVLGtCQU9vQjtBQUNsQnNCLEVBQUFBLFFBQVEsRUFBRTtBQURRLEM7QUFrQzFCdEIsVUFBVSxDQUFDdUIsU0FBWCxHQUF1QjtBQUNuQkYsRUFBQUEsUUFBUSxFQUFFRyxtQkFBVUMsU0FBVixDQUFvQixDQUM1QkQsbUJBQVVFLE9BQVYsQ0FBa0JGLG1CQUFVRyxJQUE1QixDQUQ0QixFQUU1QkgsbUJBQVVHLElBRmtCLENBQXBCLENBRFM7QUFLbkJULEVBQUFBLFdBQVcsRUFBRU0sbUJBQVVJLElBTEo7QUFNbkJuQixFQUFBQSxXQUFXLEVBQUVlLG1CQUFVSyxNQU5KO0FBT25CaEIsRUFBQUEsT0FBTyxFQUFFVyxtQkFBVU0sSUFBVixDQUFlQyxVQVBMO0FBUW5CdkIsRUFBQUEsWUFBWSxFQUFFZ0IsbUJBQVVNLElBQVYsQ0FBZUMsVUFSVjtBQVNuQlgsRUFBQUEsUUFBUSxFQUFFSSxtQkFBVVE7QUFURCxDQUF2QjtBQVlBOzs7Ozs7OztBQU9lLE1BQU1DLFFBQU4sU0FBdUJoQyxlQUFNQyxTQUE3QixDQUF1QztBQUNsREMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsMkRBNkdBTSxDQUFELElBQU87QUFDckIsVUFBSXdCLE9BQU8sR0FBRyxJQUFkLENBRHFCLENBR3JCOztBQUNBLGNBQVF4QixDQUFDLENBQUN5QixHQUFWO0FBQ0ksYUFBS0MsY0FBSUMsS0FBVDtBQUNJLGVBQUtqQyxLQUFMLENBQVdrQyxjQUFYLENBQTBCLEtBQUtDLEtBQUwsQ0FBV0MsaUJBQXJDO0FBQ0E7O0FBQ0osYUFBS0osY0FBSUssTUFBVDtBQUNJLGVBQUtDLE1BQUw7O0FBQ0E7O0FBQ0osYUFBS04sY0FBSU8sVUFBVDtBQUNJLGVBQUtDLFFBQUwsQ0FBYztBQUNWSixZQUFBQSxpQkFBaUIsRUFBRSxLQUFLSyxXQUFMLENBQWlCLEtBQUtOLEtBQUwsQ0FBV0MsaUJBQTVCO0FBRFQsV0FBZDtBQUdBOztBQUNKLGFBQUtKLGNBQUlVLFFBQVQ7QUFDSSxlQUFLRixRQUFMLENBQWM7QUFDVkosWUFBQUEsaUJBQWlCLEVBQUUsS0FBS08sV0FBTCxDQUFpQixLQUFLUixLQUFMLENBQVdDLGlCQUE1QjtBQURULFdBQWQ7QUFHQTs7QUFDSjtBQUNJTixVQUFBQSxPQUFPLEdBQUcsS0FBVjtBQWxCUjs7QUFxQkEsVUFBSUEsT0FBSixFQUFhO0FBQ1R4QixRQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsUUFBQUEsQ0FBQyxDQUFDRSxlQUFGO0FBQ0g7QUFDSixLQTFJa0I7QUFHZixTQUFLb0MsbUJBQUwsR0FBMkIsSUFBM0I7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBRUEsU0FBS0MsYUFBTCxHQUFxQixLQUFLQSxhQUFMLENBQW1CNUMsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBckI7QUFDQSxTQUFLNkMsWUFBTCxHQUFvQixLQUFLQSxZQUFMLENBQWtCN0MsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEI7QUFDQSxTQUFLOEMsZ0JBQUwsR0FBd0IsS0FBS0EsZ0JBQUwsQ0FBc0I5QyxJQUF0QixDQUEyQixJQUEzQixDQUF4QjtBQUNBLFNBQUsrQyxrQkFBTCxHQUEwQixLQUFLQSxrQkFBTCxDQUF3Qi9DLElBQXhCLENBQTZCLElBQTdCLENBQTFCO0FBQ0EsU0FBS2dELGNBQUwsR0FBc0IsS0FBS0EsY0FBTCxDQUFvQmhELElBQXBCLENBQXlCLElBQXpCLENBQXRCO0FBQ0EsU0FBS2lELFlBQUwsR0FBb0IsS0FBS0EsWUFBTCxDQUFrQmpELElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0EsU0FBS2tELG9CQUFMLEdBQTRCLEtBQUtBLG9CQUFMLENBQTBCbEQsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFDQSxTQUFLbUQscUJBQUwsR0FBNkIsS0FBS0EscUJBQUwsQ0FBMkJuRCxJQUEzQixDQUFnQyxJQUFoQyxDQUE3QjtBQUVBLFNBQUtvRCxZQUFMLEdBQW9CLElBQXBCOztBQUVBLFNBQUtDLGdCQUFMLENBQXNCLEtBQUt2RCxLQUFMLENBQVdpQixRQUFqQzs7QUFFQSxVQUFNdUMsVUFBVSxHQUFHM0QsZUFBTTRELFFBQU4sQ0FBZUMsT0FBZixDQUF1QjFELEtBQUssQ0FBQ2lCLFFBQTdCLEVBQXVDLENBQXZDLENBQW5COztBQUVBLFNBQUtrQixLQUFMLEdBQWE7QUFDVDtBQUNBd0IsTUFBQUEsUUFBUSxFQUFFLEtBRkQ7QUFHVDtBQUNBO0FBQ0F2QixNQUFBQSxpQkFBaUIsRUFBRW9CLFVBQVUsR0FBR0EsVUFBVSxDQUFDekIsR0FBZCxHQUFvQixJQUx4QztBQU1UO0FBQ0E2QixNQUFBQSxXQUFXLEVBQUU7QUFQSixLQUFiO0FBU0gsR0EvQmlELENBaUNsRDs7O0FBQ0FDLEVBQUFBLHlCQUF5QixHQUFHO0FBQUU7QUFDMUIsU0FBS0MsT0FBTCxHQUFlLHVCQUFmLENBRHdCLENBRXhCO0FBQ0E7O0FBQ0FDLElBQUFBLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsS0FBS2hCLGdCQUF4QyxFQUEwRCxLQUExRDtBQUNIOztBQUVEaUIsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkJGLElBQUFBLFFBQVEsQ0FBQ0csbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsS0FBS2xCLGdCQUEzQyxFQUE2RCxLQUE3RDtBQUNILEdBM0NpRCxDQTZDbEQ7OztBQUNBbUIsRUFBQUEsZ0NBQWdDLENBQUNDLFNBQUQsRUFBWTtBQUFFO0FBQzFDLFFBQUksQ0FBQ0EsU0FBUyxDQUFDbkQsUUFBWCxJQUF1Qm1ELFNBQVMsQ0FBQ25ELFFBQVYsQ0FBbUJvRCxNQUFuQixLQUE4QixDQUF6RCxFQUE0RDtBQUN4RDtBQUNIOztBQUNELFNBQUtkLGdCQUFMLENBQXNCYSxTQUFTLENBQUNuRCxRQUFoQzs7QUFDQSxVQUFNdUMsVUFBVSxHQUFHWSxTQUFTLENBQUNuRCxRQUFWLENBQW1CLENBQW5CLENBQW5CO0FBQ0EsU0FBS3VCLFFBQUwsQ0FBYztBQUNWSixNQUFBQSxpQkFBaUIsRUFBRW9CLFVBQVUsR0FBR0EsVUFBVSxDQUFDekIsR0FBZCxHQUFvQjtBQUR2QyxLQUFkO0FBR0g7O0FBRUR3QixFQUFBQSxnQkFBZ0IsQ0FBQ3RDLFFBQUQsRUFBVztBQUN2QixTQUFLcUQsYUFBTCxHQUFxQixFQUFyQjs7QUFDQXpFLG1CQUFNNEQsUUFBTixDQUFlYyxPQUFmLENBQXVCdEQsUUFBdkIsRUFBa0N1RCxLQUFELElBQVc7QUFDeEMsV0FBS0YsYUFBTCxDQUFtQkUsS0FBSyxDQUFDekMsR0FBekIsSUFBZ0N5QyxLQUFoQztBQUNILEtBRkQ7QUFHSDs7QUFFRHhCLEVBQUFBLGdCQUFnQixDQUFDeUIsRUFBRCxFQUFLO0FBQ2pCO0FBQ0E7QUFDQSxRQUFJQSxFQUFFLEtBQUssS0FBSzVCLFdBQWhCLEVBQTZCO0FBQ3pCLFdBQUtMLFFBQUwsQ0FBYztBQUNWbUIsUUFBQUEsUUFBUSxFQUFFO0FBREEsT0FBZDtBQUdIO0FBQ0o7O0FBRURaLEVBQUFBLFlBQVksQ0FBQzBCLEVBQUQsRUFBSztBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQUs1QixXQUFMLEdBQW1CNEIsRUFBbkI7QUFDSDs7QUFFRDNCLEVBQUFBLGFBQWEsQ0FBQzJCLEVBQUQsRUFBSztBQUNkLFFBQUksS0FBS3pFLEtBQUwsQ0FBV2tCLFFBQWYsRUFBeUI7O0FBRXpCLFFBQUksQ0FBQyxLQUFLaUIsS0FBTCxDQUFXd0IsUUFBaEIsRUFBMEI7QUFDdEIsV0FBS25CLFFBQUwsQ0FBYztBQUNWbUIsUUFBQUEsUUFBUSxFQUFFO0FBREEsT0FBZDtBQUdBYyxNQUFBQSxFQUFFLENBQUNsRSxjQUFIO0FBQ0g7QUFDSjs7QUFFRCtCLEVBQUFBLE1BQU0sR0FBRztBQUNMLFNBQUtFLFFBQUwsQ0FBYztBQUNWbUIsTUFBQUEsUUFBUSxFQUFFO0FBREEsS0FBZCxFQURLLENBSUw7O0FBQ0EsUUFBSSxLQUFLRyxPQUFMLENBQWFZLE9BQWpCLEVBQTBCO0FBQ3RCLFdBQUtaLE9BQUwsQ0FBYVksT0FBYixDQUFxQkMsS0FBckI7QUFDSDtBQUNKOztBQUVEMUIsRUFBQUEsa0JBQWtCLENBQUM1QyxXQUFELEVBQWM7QUFDNUIsU0FBS2lDLE1BQUw7O0FBQ0EsU0FBS3RDLEtBQUwsQ0FBV2tDLGNBQVgsQ0FBMEI3QixXQUExQjtBQUNIOztBQWlDRDZDLEVBQUFBLGNBQWMsQ0FBQzVDLENBQUQsRUFBSTtBQUNkLFNBQUtrQyxRQUFMLENBQWM7QUFDVm9CLE1BQUFBLFdBQVcsRUFBRXRELENBQUMsQ0FBQ3NFLE1BQUYsQ0FBU0M7QUFEWixLQUFkOztBQUdBLFFBQUksS0FBSzdFLEtBQUwsQ0FBVzhFLGNBQWYsRUFBK0I7QUFDM0IsV0FBSzlFLEtBQUwsQ0FBVzhFLGNBQVgsQ0FBMEJ4RSxDQUFDLENBQUNzRSxNQUFGLENBQVNDLEtBQW5DO0FBQ0g7QUFDSjs7QUFFRDFCLEVBQUFBLFlBQVksQ0FBQzdDLENBQUQsRUFBSTtBQUNaLFFBQUksS0FBS3NDLG1CQUFULEVBQThCO0FBQzFCLFdBQUtBLG1CQUFMLENBQXlCc0IsbUJBQXpCLENBQ0ksT0FESixFQUNhLEtBQUtuQixZQURsQixFQUNnQyxLQURoQztBQUdIOztBQUNELFFBQUl6QyxDQUFKLEVBQU87QUFDSEEsTUFBQUEsQ0FBQyxDQUFDMEQsZ0JBQUYsQ0FBbUIsT0FBbkIsRUFBNEIsS0FBS2pCLFlBQWpDLEVBQStDLEtBQS9DO0FBQ0g7O0FBQ0QsU0FBS0gsbUJBQUwsR0FBMkJ0QyxDQUEzQjtBQUNIOztBQUVEOEMsRUFBQUEsb0JBQW9CLENBQUM5QyxDQUFELEVBQUk7QUFDcEIsU0FBS2dELFlBQUwsR0FBb0JoRCxDQUFwQjtBQUNBLFFBQUlBLENBQUosRUFBT0EsQ0FBQyxDQUFDcUUsS0FBRjtBQUNWOztBQUVEdEIsRUFBQUEscUJBQXFCLENBQUMwQixTQUFELEVBQVk7QUFDN0IsU0FBS3ZDLFFBQUwsQ0FBYztBQUNWSixNQUFBQSxpQkFBaUIsRUFBRTJDO0FBRFQsS0FBZDtBQUdIOztBQUVEdEMsRUFBQUEsV0FBVyxDQUFDc0MsU0FBRCxFQUFZO0FBQ25CLFVBQU1DLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVksS0FBS1YsYUFBakIsQ0FBYjtBQUNBLFVBQU1ZLEtBQUssR0FBR0YsSUFBSSxDQUFDRyxPQUFMLENBQWFKLFNBQWIsQ0FBZDtBQUNBLFdBQU9DLElBQUksQ0FBQyxDQUFDRSxLQUFLLEdBQUcsQ0FBVCxJQUFjRixJQUFJLENBQUNYLE1BQXBCLENBQVg7QUFDSDs7QUFFRDFCLEVBQUFBLFdBQVcsQ0FBQ29DLFNBQUQsRUFBWTtBQUNuQixVQUFNQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZLEtBQUtWLGFBQWpCLENBQWI7QUFDQSxVQUFNWSxLQUFLLEdBQUdGLElBQUksQ0FBQ0csT0FBTCxDQUFhSixTQUFiLENBQWQ7QUFDQSxXQUFPQyxJQUFJLENBQUMsQ0FBQ0UsS0FBSyxHQUFHLENBQVQsSUFBY0YsSUFBSSxDQUFDWCxNQUFwQixDQUFYO0FBQ0g7O0FBRURlLEVBQUFBLGVBQWUsQ0FBQzdELElBQUQsRUFBTztBQUNsQixRQUFJQSxJQUFKLEVBQVU7QUFDTkEsTUFBQUEsSUFBSSxDQUFDOEQsY0FBTCxDQUFvQjtBQUNoQkMsUUFBQUEsS0FBSyxFQUFFLFNBRFM7QUFFaEJDLFFBQUFBLFFBQVEsRUFBRTtBQUZNLE9BQXBCO0FBSUg7QUFDSjs7QUFFREMsRUFBQUEsZUFBZSxHQUFHO0FBQ2QsVUFBTUMsT0FBTyxHQUFHNUYsZUFBTTRELFFBQU4sQ0FBZWlDLEdBQWYsQ0FBbUIsS0FBSzFGLEtBQUwsQ0FBV2lCLFFBQTlCLEVBQXlDdUQsS0FBRCxJQUFXO0FBQy9ELFlBQU0xRCxXQUFXLEdBQUcsS0FBS3FCLEtBQUwsQ0FBV0MsaUJBQVgsS0FBaUNvQyxLQUFLLENBQUN6QyxHQUEzRDtBQUNBLGFBQ0ksNkJBQUMsVUFBRDtBQUNJLFFBQUEsRUFBRSxZQUFLLEtBQUsvQixLQUFMLENBQVdlLEVBQWhCLGVBQXVCeUQsS0FBSyxDQUFDekMsR0FBN0IsQ0FETjtBQUVJLFFBQUEsR0FBRyxFQUFFeUMsS0FBSyxDQUFDekMsR0FGZjtBQUdJLFFBQUEsV0FBVyxFQUFFeUMsS0FBSyxDQUFDekMsR0FIdkI7QUFJSSxRQUFBLFdBQVcsRUFBRWpCLFdBSmpCO0FBS0ksUUFBQSxZQUFZLEVBQUUsS0FBS3VDLHFCQUx2QjtBQU1JLFFBQUEsT0FBTyxFQUFFLEtBQUtKLGtCQU5sQjtBQU9JLFFBQUEsUUFBUSxFQUFFbkMsV0FBVyxHQUFHLEtBQUtzRSxlQUFSLEdBQTBCTztBQVBuRCxTQVNNbkIsS0FUTixDQURKO0FBYUgsS0FmZSxDQUFoQjs7QUFnQkEsUUFBSWlCLE9BQU8sQ0FBQ3BCLE1BQVIsS0FBbUIsQ0FBdkIsRUFBMEI7QUFDdEIsYUFBTyxDQUFDO0FBQUssUUFBQSxHQUFHLEVBQUMsR0FBVDtBQUFhLFFBQUEsU0FBUyxFQUFDLG9CQUF2QjtBQUE0QyxRQUFBLElBQUksRUFBQztBQUFqRCxTQUNGLHlCQUFHLFlBQUgsQ0FERSxDQUFELENBQVA7QUFHSDs7QUFDRCxXQUFPb0IsT0FBUDtBQUNIOztBQUVEL0UsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSWtGLFlBQUo7QUFFQSxVQUFNQyxTQUFTLEdBQUcsRUFBbEI7QUFDQSxRQUFJLEtBQUs3RixLQUFMLENBQVc4RixTQUFmLEVBQTBCRCxTQUFTLENBQUNFLEtBQVYsR0FBa0IsS0FBSy9GLEtBQUwsQ0FBVzhGLFNBQTdCO0FBRTFCLFFBQUlFLElBQUo7O0FBQ0EsUUFBSSxLQUFLN0QsS0FBTCxDQUFXd0IsUUFBZixFQUF5QjtBQUNyQixVQUFJLEtBQUszRCxLQUFMLENBQVdpRyxhQUFmLEVBQThCO0FBQzFCTCxRQUFBQSxZQUFZLEdBQ1I7QUFDSSxVQUFBLElBQUksRUFBQyxNQURUO0FBRUksVUFBQSxTQUFTLEVBQUMsb0JBRmQ7QUFHSSxVQUFBLEdBQUcsRUFBRSxLQUFLeEMsb0JBSGQ7QUFJSSxVQUFBLFNBQVMsRUFBRSxLQUFLOEMsZUFKcEI7QUFLSSxVQUFBLFFBQVEsRUFBRSxLQUFLaEQsY0FMbkI7QUFNSSxVQUFBLEtBQUssRUFBRSxLQUFLZixLQUFMLENBQVd5QixXQU50QjtBQU9JLFVBQUEsSUFBSSxFQUFDLFVBUFQ7QUFRSSwrQkFBa0IsTUFSdEI7QUFTSSw2Q0FBMEIsS0FBSzVELEtBQUwsQ0FBV2UsRUFBckMsZUFBNEMsS0FBS29CLEtBQUwsQ0FBV0MsaUJBQXZELENBVEo7QUFVSSxpQ0FBYyxLQUFLcEMsS0FBTCxDQUFXZSxFQUF6QixhQVZKO0FBV0ksMkJBQWUsS0FBS2YsS0FBTCxDQUFXa0IsUUFYOUI7QUFZSSx3QkFBWSxLQUFLbEIsS0FBTCxDQUFXbUc7QUFaM0IsVUFESjtBQWdCSDs7QUFDREgsTUFBQUEsSUFBSSxHQUNBO0FBQUssUUFBQSxTQUFTLEVBQUMsa0JBQWY7QUFBa0MsUUFBQSxLQUFLLEVBQUVILFNBQXpDO0FBQW9ELFFBQUEsSUFBSSxFQUFDLFNBQXpEO0FBQW1FLFFBQUEsRUFBRSxZQUFLLEtBQUs3RixLQUFMLENBQVdlLEVBQWhCO0FBQXJFLFNBQ00sS0FBS3lFLGVBQUwsRUFETixDQURKO0FBS0g7O0FBRUQsUUFBSSxDQUFDSSxZQUFMLEVBQW1CO0FBQ2YsWUFBTVEsYUFBYSxHQUFHLEtBQUtwRyxLQUFMLENBQVdxRyxjQUFYLEdBQ2xCLEtBQUtyRyxLQUFMLENBQVdxRyxjQUFYLENBQTBCLEtBQUtyRyxLQUFMLENBQVc2RSxLQUFyQyxDQURrQixHQUVsQixLQUFLUCxhQUFMLENBQW1CLEtBQUt0RSxLQUFMLENBQVc2RSxLQUE5QixDQUZKO0FBR0FlLE1BQUFBLFlBQVksR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDLG9CQUFmO0FBQW9DLFFBQUEsRUFBRSxZQUFLLEtBQUs1RixLQUFMLENBQVdlLEVBQWhCO0FBQXRDLFNBQ1RxRixhQURTLENBQWY7QUFHSDs7QUFFRCxVQUFNRSxlQUFlLEdBQUc7QUFDcEJDLE1BQUFBLFdBQVcsRUFBRSxJQURPO0FBRXBCQyxNQUFBQSxvQkFBb0IsRUFBRSxLQUFLeEcsS0FBTCxDQUFXa0I7QUFGYixLQUF4Qjs7QUFJQSxRQUFJLEtBQUtsQixLQUFMLENBQVd5RyxTQUFmLEVBQTBCO0FBQ3RCSCxNQUFBQSxlQUFlLENBQUMsS0FBS3RHLEtBQUwsQ0FBV3lHLFNBQVosQ0FBZixHQUF3QyxJQUF4QztBQUNILEtBaERJLENBa0RMO0FBQ0E7OztBQUNBLFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBRSx5QkFBV0gsZUFBWCxDQUFoQjtBQUE2QyxNQUFBLEdBQUcsRUFBRSxLQUFLbkQ7QUFBdkQsT0FDSCw2QkFBQyx5QkFBRDtBQUNJLE1BQUEsU0FBUyxFQUFDLG1DQURkO0FBRUksTUFBQSxPQUFPLEVBQUUsS0FBS0wsYUFGbEI7QUFHSSx1QkFBYyxTQUhsQjtBQUlJLHVCQUFlLEtBQUtYLEtBQUwsQ0FBV3dCLFFBSjlCO0FBS0ksTUFBQSxRQUFRLEVBQUUsS0FBSzNELEtBQUwsQ0FBV2tCLFFBTHpCO0FBTUksTUFBQSxRQUFRLEVBQUUsS0FBSzRDLE9BTm5CO0FBT0ksb0JBQVksS0FBSzlELEtBQUwsQ0FBV21HLEtBUDNCO0FBUUksb0NBQXFCLEtBQUtuRyxLQUFMLENBQVdlLEVBQWhDO0FBUkosT0FVTTZFLFlBVk4sRUFXSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE1BWEosRUFZTUksSUFaTixDQURHLENBQVA7QUFnQkg7O0FBL1JpRDs7O0FBa1N0RG5FLFFBQVEsQ0FBQ1YsU0FBVCxHQUFxQjtBQUNqQkosRUFBQUEsRUFBRSxFQUFFSyxtQkFBVUssTUFBVixDQUFpQkUsVUFESjtBQUVqQjtBQUNBO0FBQ0E7QUFDQW1FLEVBQUFBLFNBQVMsRUFBRTFFLG1CQUFVc0YsTUFMSjtBQU1qQjtBQUNBeEUsRUFBQUEsY0FBYyxFQUFFZCxtQkFBVU0sSUFBVixDQUFlQyxVQVBkO0FBUWpCO0FBQ0FtRCxFQUFBQSxjQUFjLEVBQUUxRCxtQkFBVU0sSUFUVDtBQVVqQnVFLEVBQUFBLGFBQWEsRUFBRTdFLG1CQUFVSSxJQVZSO0FBV2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBNkUsRUFBQUEsY0FBYyxFQUFFakYsbUJBQVVNLElBakJUO0FBa0JqQm1ELEVBQUFBLEtBQUssRUFBRXpELG1CQUFVSyxNQWxCQTtBQW1CakI7QUFDQVAsRUFBQUEsUUFBUSxFQUFFRSxtQkFBVUksSUFwQkg7QUFxQmpCO0FBQ0EyRSxFQUFBQSxLQUFLLEVBQUUvRSxtQkFBVUssTUFBVixDQUFpQkU7QUF0QlAsQ0FBckIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4vQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtLZXl9IGZyb20gXCIuLi8uLi8uLi9LZXlib2FyZFwiO1xyXG5cclxuY2xhc3MgTWVudU9wdGlvbiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLl9vbk1vdXNlRW50ZXIgPSB0aGlzLl9vbk1vdXNlRW50ZXIuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbkNsaWNrID0gdGhpcy5fb25DbGljay5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XHJcbiAgICAgICAgZGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgfTtcclxuXHJcbiAgICBfb25Nb3VzZUVudGVyKCkge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25Nb3VzZUVudGVyKHRoaXMucHJvcHMuZHJvcGRvd25LZXkpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNsaWNrKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLnByb3BzLm9uQ2xpY2sodGhpcy5wcm9wcy5kcm9wZG93bktleSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IG9wdENsYXNzZXMgPSBjbGFzc25hbWVzKHtcclxuICAgICAgICAgICAgbXhfRHJvcGRvd25fb3B0aW9uOiB0cnVlLFxyXG4gICAgICAgICAgICBteF9Ecm9wZG93bl9vcHRpb25faGlnaGxpZ2h0OiB0aGlzLnByb3BzLmhpZ2hsaWdodGVkLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gPGRpdlxyXG4gICAgICAgICAgICBpZD17dGhpcy5wcm9wcy5pZH1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtvcHRDbGFzc2VzfVxyXG4gICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrfVxyXG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMuX29uTW91c2VFbnRlcn1cclxuICAgICAgICAgICAgcm9sZT1cIm9wdGlvblwiXHJcbiAgICAgICAgICAgIGFyaWEtc2VsZWN0ZWQ9e3RoaXMucHJvcHMuaGlnaGxpZ2h0ZWR9XHJcbiAgICAgICAgICAgIHJlZj17dGhpcy5wcm9wcy5pbnB1dFJlZn1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5jaGlsZHJlbiB9XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG59XHJcblxyXG5NZW51T3B0aW9uLnByb3BUeXBlcyA9IHtcclxuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtcclxuICAgICAgUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm5vZGUpLFxyXG4gICAgICBQcm9wVHlwZXMubm9kZSxcclxuICAgIF0pLFxyXG4gICAgaGlnaGxpZ2h0ZWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgZHJvcGRvd25LZXk6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICBvbkNsaWNrOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgb25Nb3VzZUVudGVyOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgaW5wdXRSZWY6IFByb3BUeXBlcy5hbnksXHJcbn07XHJcblxyXG4vKlxyXG4gKiBSZXVzYWJsZSBkcm9wZG93biBzZWxlY3QgY29udHJvbCwgYWtpbiB0byByZWFjdC1zZWxlY3QsXHJcbiAqIGJ1dCBzb21ld2hhdCBzaW1wbGVyIGFzIHJlYWN0LXNlbGVjdCBpcyA3OUtCIG9mIG1pbmlmaWVkXHJcbiAqIGphdmFzY3JpcHQuXHJcbiAqXHJcbiAqIFRPRE86IFBvcnQgTmV0d29ya0Ryb3Bkb3duIHRvIHVzZSB0aGlzLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRHJvcGRvd24gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuZHJvcGRvd25Sb290RWxlbWVudCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5pZ25vcmVFdmVudCA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuX29uSW5wdXRDbGljayA9IHRoaXMuX29uSW5wdXRDbGljay5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uUm9vdENsaWNrID0gdGhpcy5fb25Sb290Q2xpY2suYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbkRvY3VtZW50Q2xpY2sgPSB0aGlzLl9vbkRvY3VtZW50Q2xpY2suYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbk1lbnVPcHRpb25DbGljayA9IHRoaXMuX29uTWVudU9wdGlvbkNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25JbnB1dENoYW5nZSA9IHRoaXMuX29uSW5wdXRDaGFuZ2UuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9jb2xsZWN0Um9vdCA9IHRoaXMuX2NvbGxlY3RSb290LmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fY29sbGVjdElucHV0VGV4dEJveCA9IHRoaXMuX2NvbGxlY3RJbnB1dFRleHRCb3guYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9zZXRIaWdobGlnaHRlZE9wdGlvbiA9IHRoaXMuX3NldEhpZ2hsaWdodGVkT3B0aW9uLmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIHRoaXMuaW5wdXRUZXh0Qm94ID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy5fcmVpbmRleENoaWxkcmVuKHRoaXMucHJvcHMuY2hpbGRyZW4pO1xyXG5cclxuICAgICAgICBjb25zdCBmaXJzdENoaWxkID0gUmVhY3QuQ2hpbGRyZW4udG9BcnJheShwcm9wcy5jaGlsZHJlbilbMF07XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIC8vIFRydWUgaWYgdGhlIG1lbnUgaXMgZHJvcHBlZC1kb3duXHJcbiAgICAgICAgICAgIGV4cGFuZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgLy8gVGhlIGtleSBvZiB0aGUgaGlnaGxpZ2h0ZWQgb3B0aW9uXHJcbiAgICAgICAgICAgIC8vICh0aGUgb3B0aW9uIHRoYXQgd291bGQgYmVjb21lIHNlbGVjdGVkIGlmIHlvdSBwcmVzc2VkIGVudGVyKVxyXG4gICAgICAgICAgICBoaWdobGlnaHRlZE9wdGlvbjogZmlyc3RDaGlsZCA/IGZpcnN0Q2hpbGQua2V5IDogbnVsbCxcclxuICAgICAgICAgICAgLy8gdGhlIGN1cnJlbnQgc2VhcmNoIHF1ZXJ5XHJcbiAgICAgICAgICAgIHNlYXJjaFF1ZXJ5OiAnJyxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIGNvbXBvbmVudCB3aXRoIHJlYWwgY2xhc3MsIHVzZSBjb25zdHJ1Y3RvciBmb3IgcmVmc1xyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudCgpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjYW1lbGNhc2VcclxuICAgICAgICB0aGlzLl9idXR0b24gPSBjcmVhdGVSZWYoKTtcclxuICAgICAgICAvLyBMaXN0ZW4gZm9yIGFsbCBjbGlja3Mgb24gdGhlIGRvY3VtZW50IHNvIHdlIGNhbiBjbG9zZSB0aGVcclxuICAgICAgICAvLyBtZW51IHdoZW4gdGhlIHVzZXIgY2xpY2tzIHNvbWV3aGVyZSBlbHNlXHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLl9vbkRvY3VtZW50Q2xpY2ssIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuX29uRG9jdW1lbnRDbGljaywgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBjYW1lbGNhc2VcclxuICAgICAgICBpZiAoIW5leHRQcm9wcy5jaGlsZHJlbiB8fCBuZXh0UHJvcHMuY2hpbGRyZW4ubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fcmVpbmRleENoaWxkcmVuKG5leHRQcm9wcy5jaGlsZHJlbik7XHJcbiAgICAgICAgY29uc3QgZmlyc3RDaGlsZCA9IG5leHRQcm9wcy5jaGlsZHJlblswXTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgaGlnaGxpZ2h0ZWRPcHRpb246IGZpcnN0Q2hpbGQgPyBmaXJzdENoaWxkLmtleSA6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlaW5kZXhDaGlsZHJlbihjaGlsZHJlbikge1xyXG4gICAgICAgIHRoaXMuY2hpbGRyZW5CeUtleSA9IHt9O1xyXG4gICAgICAgIFJlYWN0LkNoaWxkcmVuLmZvckVhY2goY2hpbGRyZW4sIChjaGlsZCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNoaWxkcmVuQnlLZXlbY2hpbGQua2V5XSA9IGNoaWxkO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkRvY3VtZW50Q2xpY2soZXYpIHtcclxuICAgICAgICAvLyBDbG9zZSB0aGUgZHJvcGRvd24gaWYgdGhlIHVzZXIgY2xpY2tzIGFueXdoZXJlIHRoYXQgaXNuJ3RcclxuICAgICAgICAvLyB3aXRoaW4gb3VyIHJvb3QgZWxlbWVudFxyXG4gICAgICAgIGlmIChldiAhPT0gdGhpcy5pZ25vcmVFdmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGV4cGFuZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vblJvb3RDbGljayhldikge1xyXG4gICAgICAgIC8vIFRoaXMgY2FwdHVyZXMgYW55IGNsaWNrcyB0aGF0IGhhcHBlbiB3aXRoaW4gb3VyIGVsZW1lbnRzLFxyXG4gICAgICAgIC8vIHN1Y2ggdGhhdCB3ZSBjYW4gdGhlbiBpZ25vcmUgdGhlbSB3aGVuIHRoZXkncmUgc2VlbiBieSB0aGVcclxuICAgICAgICAvLyBjbGljayBsaXN0ZW5lciBvbiB0aGUgZG9jdW1lbnQgaGFuZGxlciwgaWUuIG5vdCBjbG9zZSB0aGVcclxuICAgICAgICAvLyBkcm9wZG93biBpbW1lZGlhdGVseSBhZnRlciBvcGVuaW5nIGl0LlxyXG4gICAgICAgIC8vIE5CLiBXZSBjYW4ndCBqdXN0IHN0b3BQcm9wYWdhdGlvbigpIGJlY2F1c2UgdGhlbiB0aGUgZXZlbnRcclxuICAgICAgICAvLyBkb2Vzbid0IHJlYWNoIHRoZSBSZWFjdCBvbkNsaWNrKCkuXHJcbiAgICAgICAgdGhpcy5pZ25vcmVFdmVudCA9IGV2O1xyXG4gICAgfVxyXG5cclxuICAgIF9vbklucHV0Q2xpY2soZXYpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5kaXNhYmxlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuZXhwYW5kZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBleHBhbmRlZDogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9jbG9zZSgpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZXhwYW5kZWQ6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vIHRoZWlyIGZvY3VzIHdhcyBvbiB0aGUgaW5wdXQsIGl0cyBnZXR0aW5nIHVubW91bnRlZCwgbW92ZSBpdCB0byB0aGUgYnV0dG9uXHJcbiAgICAgICAgaWYgKHRoaXMuX2J1dHRvbi5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2J1dHRvbi5jdXJyZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbk1lbnVPcHRpb25DbGljayhkcm9wZG93bktleSkge1xyXG4gICAgICAgIHRoaXMuX2Nsb3NlKCk7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbk9wdGlvbkNoYW5nZShkcm9wZG93bktleSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uSW5wdXRLZXlEb3duID0gKGUpID0+IHtcclxuICAgICAgICBsZXQgaGFuZGxlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIC8vIFRoZXNlIGtleXMgZG9uJ3QgZ2VuZXJhdGUga2V5cHJlc3MgZXZlbnRzIGFuZCBzbyBuZWVkcyB0byBiZSBvbiBrZXl1cFxyXG4gICAgICAgIHN3aXRjaCAoZS5rZXkpIHtcclxuICAgICAgICAgICAgY2FzZSBLZXkuRU5URVI6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uT3B0aW9uQ2hhbmdlKHRoaXMuc3RhdGUuaGlnaGxpZ2h0ZWRPcHRpb24pO1xyXG4gICAgICAgICAgICAgICAgLy8gZmFsbHRocm91Z2hcclxuICAgICAgICAgICAgY2FzZSBLZXkuRVNDQVBFOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19ET1dOOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWRPcHRpb246IHRoaXMuX25leHRPcHRpb24odGhpcy5zdGF0ZS5oaWdobGlnaHRlZE9wdGlvbiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEtleS5BUlJPV19VUDpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodGVkT3B0aW9uOiB0aGlzLl9wcmV2T3B0aW9uKHRoaXMuc3RhdGUuaGlnaGxpZ2h0ZWRPcHRpb24pLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGhhbmRsZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChoYW5kbGVkKSB7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uSW5wdXRDaGFuZ2UoZSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzZWFyY2hRdWVyeTogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25TZWFyY2hDaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblNlYXJjaENoYW5nZShlLnRhcmdldC52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9jb2xsZWN0Um9vdChlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZHJvcGRvd25Sb290RWxlbWVudCkge1xyXG4gICAgICAgICAgICB0aGlzLmRyb3Bkb3duUm9vdEVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcclxuICAgICAgICAgICAgICAgICdjbGljaycsIHRoaXMuX29uUm9vdENsaWNrLCBmYWxzZSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGUpIHtcclxuICAgICAgICAgICAgZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuX29uUm9vdENsaWNrLCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZHJvcGRvd25Sb290RWxlbWVudCA9IGU7XHJcbiAgICB9XHJcblxyXG4gICAgX2NvbGxlY3RJbnB1dFRleHRCb3goZSkge1xyXG4gICAgICAgIHRoaXMuaW5wdXRUZXh0Qm94ID0gZTtcclxuICAgICAgICBpZiAoZSkgZS5mb2N1cygpO1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXRIaWdobGlnaHRlZE9wdGlvbihvcHRpb25LZXkpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgaGlnaGxpZ2h0ZWRPcHRpb246IG9wdGlvbktleSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfbmV4dE9wdGlvbihvcHRpb25LZXkpIHtcclxuICAgICAgICBjb25zdCBrZXlzID0gT2JqZWN0LmtleXModGhpcy5jaGlsZHJlbkJ5S2V5KTtcclxuICAgICAgICBjb25zdCBpbmRleCA9IGtleXMuaW5kZXhPZihvcHRpb25LZXkpO1xyXG4gICAgICAgIHJldHVybiBrZXlzWyhpbmRleCArIDEpICUga2V5cy5sZW5ndGhdO1xyXG4gICAgfVxyXG5cclxuICAgIF9wcmV2T3B0aW9uKG9wdGlvbktleSkge1xyXG4gICAgICAgIGNvbnN0IGtleXMgPSBPYmplY3Qua2V5cyh0aGlzLmNoaWxkcmVuQnlLZXkpO1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0ga2V5cy5pbmRleE9mKG9wdGlvbktleSk7XHJcbiAgICAgICAgcmV0dXJuIGtleXNbKGluZGV4IC0gMSkgJSBrZXlzLmxlbmd0aF07XHJcbiAgICB9XHJcblxyXG4gICAgX3Njcm9sbEludG9WaWV3KG5vZGUpIHtcclxuICAgICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgICAgICBub2RlLnNjcm9sbEludG9WaWV3KHtcclxuICAgICAgICAgICAgICAgIGJsb2NrOiBcIm5lYXJlc3RcIixcclxuICAgICAgICAgICAgICAgIGJlaGF2aW9yOiBcImF1dG9cIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9nZXRNZW51T3B0aW9ucygpIHtcclxuICAgICAgICBjb25zdCBvcHRpb25zID0gUmVhY3QuQ2hpbGRyZW4ubWFwKHRoaXMucHJvcHMuY2hpbGRyZW4sIChjaGlsZCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBoaWdobGlnaHRlZCA9IHRoaXMuc3RhdGUuaGlnaGxpZ2h0ZWRPcHRpb24gPT09IGNoaWxkLmtleTtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxNZW51T3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgaWQ9e2Ake3RoaXMucHJvcHMuaWR9X18ke2NoaWxkLmtleX1gfVxyXG4gICAgICAgICAgICAgICAgICAgIGtleT17Y2hpbGQua2V5fVxyXG4gICAgICAgICAgICAgICAgICAgIGRyb3Bkb3duS2V5PXtjaGlsZC5rZXl9XHJcbiAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ9e2hpZ2hsaWdodGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5fc2V0SGlnaGxpZ2h0ZWRPcHRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25NZW51T3B0aW9uQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgaW5wdXRSZWY9e2hpZ2hsaWdodGVkID8gdGhpcy5fc2Nyb2xsSW50b1ZpZXcgOiB1bmRlZmluZWR9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBjaGlsZCB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVPcHRpb24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKG9wdGlvbnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbPGRpdiBrZXk9XCIwXCIgY2xhc3NOYW1lPVwibXhfRHJvcGRvd25fb3B0aW9uXCIgcm9sZT1cIm9wdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgeyBfdChcIk5vIHJlc3VsdHNcIikgfVxyXG4gICAgICAgICAgICA8L2Rpdj5dO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb3B0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRWYWx1ZTtcclxuXHJcbiAgICAgICAgY29uc3QgbWVudVN0eWxlID0ge307XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubWVudVdpZHRoKSBtZW51U3R5bGUud2lkdGggPSB0aGlzLnByb3BzLm1lbnVXaWR0aDtcclxuXHJcbiAgICAgICAgbGV0IG1lbnU7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXhwYW5kZWQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuc2VhcmNoRW5hYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgY3VycmVudFZhbHVlID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0Ryb3Bkb3duX29wdGlvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5fY29sbGVjdElucHV0VGV4dEJveH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25LZXlEb3duPXt0aGlzLl9vbklucHV0S2V5RG93bn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uSW5wdXRDaGFuZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnNlYXJjaFF1ZXJ5fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwiY29tYm9ib3hcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWF1dG9jb21wbGV0ZT1cImxpc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWFjdGl2ZWRlc2NlbmRhbnQ9e2Ake3RoaXMucHJvcHMuaWR9X18ke3RoaXMuc3RhdGUuaGlnaGxpZ2h0ZWRPcHRpb259YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1vd25zPXtgJHt0aGlzLnByb3BzLmlkfV9saXN0Ym94YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1kaXNhYmxlZD17dGhpcy5wcm9wcy5kaXNhYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17dGhpcy5wcm9wcy5sYWJlbH1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBtZW51ID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Ecm9wZG93bl9tZW51XCIgc3R5bGU9e21lbnVTdHlsZX0gcm9sZT1cImxpc3Rib3hcIiBpZD17YCR7dGhpcy5wcm9wcy5pZH1fbGlzdGJveGB9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5fZ2V0TWVudU9wdGlvbnMoKSB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQ2hpbGQgPSB0aGlzLnByb3BzLmdldFNob3J0T3B0aW9uID9cclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZ2V0U2hvcnRPcHRpb24odGhpcy5wcm9wcy52YWx1ZSkgOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGlsZHJlbkJ5S2V5W3RoaXMucHJvcHMudmFsdWVdO1xyXG4gICAgICAgICAgICBjdXJyZW50VmFsdWUgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0Ryb3Bkb3duX29wdGlvblwiIGlkPXtgJHt0aGlzLnByb3BzLmlkfV92YWx1ZWB9PlxyXG4gICAgICAgICAgICAgICAgeyBzZWxlY3RlZENoaWxkIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZHJvcGRvd25DbGFzc2VzID0ge1xyXG4gICAgICAgICAgICBteF9Ecm9wZG93bjogdHJ1ZSxcclxuICAgICAgICAgICAgbXhfRHJvcGRvd25fZGlzYWJsZWQ6IHRoaXMucHJvcHMuZGlzYWJsZWQsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jbGFzc05hbWUpIHtcclxuICAgICAgICAgICAgZHJvcGRvd25DbGFzc2VzW3RoaXMucHJvcHMuY2xhc3NOYW1lXSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBOb3RlIHRoZSBtZW51IHNpdHMgaW5zaWRlIHRoZSBBY2Nlc3NpYmxlQnV0dG9uIGRpdiBzbyBpdCdzIGFuY2hvcmVkXHJcbiAgICAgICAgLy8gdG8gdGhlIGlucHV0LCBidXQgb3ZlcmZsb3dzIGJlbG93IGl0LiBUaGUgcm9vdCBjb250YWlucyBib3RoLlxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhkcm9wZG93bkNsYXNzZXMpfSByZWY9e3RoaXMuX2NvbGxlY3RSb290fT5cclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0Ryb3Bkb3duX2lucHV0IG14X25vX3RleHRpbnB1dFwiXHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbklucHV0Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICBhcmlhLWhhc3BvcHVwPVwibGlzdGJveFwiXHJcbiAgICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXt0aGlzLnN0YXRlLmV4cGFuZGVkfVxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMucHJvcHMuZGlzYWJsZWR9XHJcbiAgICAgICAgICAgICAgICBpbnB1dFJlZj17dGhpcy5fYnV0dG9ufVxyXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17dGhpcy5wcm9wcy5sYWJlbH1cclxuICAgICAgICAgICAgICAgIGFyaWEtZGVzY3JpYmVkYnk9e2Ake3RoaXMucHJvcHMuaWR9X3ZhbHVlYH1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgeyBjdXJyZW50VmFsdWUgfVxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRHJvcGRvd25fYXJyb3dcIiAvPlxyXG4gICAgICAgICAgICAgICAgeyBtZW51IH1cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxufVxyXG5cclxuRHJvcGRvd24ucHJvcFR5cGVzID0ge1xyXG4gICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgIC8vIFRoZSB3aWR0aCB0aGF0IHRoZSBkcm9wZG93biBzaG91bGQgYmUuIElmIHNwZWNpZmllZCxcclxuICAgIC8vIHRoZSBkcm9wcGVkLWRvd24gcGFydCBvZiB0aGUgbWVudSB3aWxsIGJlIHNldCB0byB0aGlzXHJcbiAgICAvLyB3aWR0aC5cclxuICAgIG1lbnVXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBzZWxlY3RlZCBvcHRpb24gY2hhbmdlc1xyXG4gICAgb25PcHRpb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgdmFsdWUgb2YgdGhlIHNlYXJjaCBmaWVsZCBjaGFuZ2VzXHJcbiAgICBvblNlYXJjaENoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZWFyY2hFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIEZ1bmN0aW9uIHRoYXQsIGdpdmVuIHRoZSBrZXkgb2YgYW4gb3B0aW9uLCByZXR1cm5zXHJcbiAgICAvLyBhIG5vZGUgcmVwcmVzZW50aW5nIHRoYXQgb3B0aW9uIHRvIGJlIGRpc3BsYXllZCBpbiB0aGVcclxuICAgIC8vIGJveCBpdHNlbGYgYXMgdGhlIGN1cnJlbnRseS1zZWxlY3RlZCBvcHRpb24gKGllLiBhc1xyXG4gICAgLy8gb3Bwb3NlZCB0byBpbiB0aGUgYWN0dWFsIGRyb3BwZWQtZG93biBwYXJ0KS4gSWZcclxuICAgIC8vIHVuc3BlY2lmaWVkLCB0aGUgYXBwcm9wcmlhdGUgY2hpbGQgZWxlbWVudCBpcyB1c2VkIGFzXHJcbiAgICAvLyBpbiB0aGUgZHJvcHBlZC1kb3duIG1lbnUuXHJcbiAgICBnZXRTaG9ydE9wdGlvbjogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIC8vIG5lZ2F0aXZlIGZvciBjb25zaXN0ZW5jeSB3aXRoIEhUTUxcclxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIEFSSUEgbGFiZWxcclxuICAgIGxhYmVsOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbn07XHJcbiJdfQ==