"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _url = _interopRequireDefault(require("url"));

var _qs = _interopRequireDefault(require("qs"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _WidgetMessaging = _interopRequireDefault(require("../../../WidgetMessaging"));

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _AppPermission = _interopRequireDefault(require("./AppPermission"));

var _AppWarning = _interopRequireDefault(require("./AppWarning"));

var _MessageSpinner = _interopRequireDefault(require("./MessageSpinner"));

var _WidgetUtils = _interopRequireDefault(require("../../../utils/WidgetUtils"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _ActiveWidgetStore = _interopRequireDefault(require("../../../stores/ActiveWidgetStore"));

var _classnames = _interopRequireDefault(require("classnames"));

var _IntegrationManagers = require("../../../integrations/IntegrationManagers");

var _SettingsStore = _interopRequireWildcard(require("../../../settings/SettingsStore"));

var _ContextMenu = require("../../structures/ContextMenu");

var _PersistedElement = _interopRequireDefault(require("./PersistedElement"));

/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const ALLOWED_APP_URL_SCHEMES = ['https:', 'http:'];
const ENABLE_REACT_PERF = false;
/**
 * Does template substitution on a URL (or any string). Variables will be
 * passed through encodeURIComponent.
 * @param {string} uriTemplate The path with template variables e.g. '/foo/$bar'.
 * @param {Object} variables The key/value pairs to replace the template
 * variables with. E.g. { '$bar': 'baz' }.
 * @return {string} The result of replacing all template variables e.g. '/foo/baz'.
 */

function uriFromTemplate(uriTemplate, variables) {
  let out = uriTemplate;

  for (const [key, val] of Object.entries(variables)) {
    out = out.replace('$' + key, encodeURIComponent(val));
  }

  return out;
}

class AppTile extends _react.default.Component {
  constructor(props) {
    super(props); // The key used for PersistedElement

    (0, _defineProperty2.default)(this, "_onContextMenuClick", () => {
      this.setState({
        menuDisplayed: true
      });
    });
    (0, _defineProperty2.default)(this, "_closeContextMenu", () => {
      this.setState({
        menuDisplayed: false
      });
    });
    this._persistKey = 'widget_' + this.props.app.id;
    this.state = this._getNewState(props);
    this._onAction = this._onAction.bind(this);
    this._onLoaded = this._onLoaded.bind(this);
    this._onEditClick = this._onEditClick.bind(this);
    this._onDeleteClick = this._onDeleteClick.bind(this);
    this._onRevokeClicked = this._onRevokeClicked.bind(this);
    this._onSnapshotClick = this._onSnapshotClick.bind(this);
    this.onClickMenuBar = this.onClickMenuBar.bind(this);
    this._onMinimiseClick = this._onMinimiseClick.bind(this);
    this._grantWidgetPermission = this._grantWidgetPermission.bind(this);
    this._revokeWidgetPermission = this._revokeWidgetPermission.bind(this);
    this._onPopoutWidgetClick = this._onPopoutWidgetClick.bind(this);
    this._onReloadWidgetClick = this._onReloadWidgetClick.bind(this);
    this._contextMenuButton = (0, _react.createRef)();
    this._appFrame = (0, _react.createRef)();
    this._menu_bar = (0, _react.createRef)();
  }
  /**
   * Set initial component state when the App wUrl (widget URL) is being updated.
   * Component props *must* be passed (rather than relying on this.props).
   * @param  {Object} newProps The new properties of the component
   * @return {Object} Updated component state to be set with setState
   */


  _getNewState(newProps) {
    // This is a function to make the impact of calling SettingsStore slightly less
    const hasPermissionToLoad = () => {
      const currentlyAllowedWidgets = _SettingsStore.default.getValue("allowedWidgets", newProps.room.roomId);

      return !!currentlyAllowedWidgets[newProps.app.eventId];
    };

    const PersistedElement = sdk.getComponent("elements.PersistedElement");
    return {
      initialising: true,
      // True while we are mangling the widget URL
      // True while the iframe content is loading
      loading: this.props.waitForIframeLoad && !PersistedElement.isMounted(this._persistKey),
      widgetUrl: this._addWurlParams(newProps.app.url),
      // Assume that widget has permission to load if we are the user who
      // added it to the room, or if explicitly granted by the user
      hasPermissionToLoad: newProps.userId === newProps.creatorUserId || hasPermissionToLoad(),
      error: null,
      deleting: false,
      widgetPageTitle: newProps.widgetPageTitle,
      menuDisplayed: false
    };
  }
  /**
   * Does the widget support a given capability
   * @param  {string}  capability Capability to check for
   * @return {Boolean}            True if capability supported
   */


  _hasCapability(capability) {
    return _ActiveWidgetStore.default.widgetHasCapability(this.props.app.id, capability);
  }
  /**
   * Add widget instance specific parameters to pass in wUrl
   * Properties passed to widget instance:
   *  - widgetId
   *  - origin / parent URL
   * @param {string} urlString Url string to modify
   * @return {string}
   * Url string with parameters appended.
   * If url can not be parsed, it is returned unmodified.
   */


  _addWurlParams(urlString) {
    const u = _url.default.parse(urlString);

    if (!u) {
      console.error("_addWurlParams", "Invalid URL", urlString);
      return _url.default;
    }

    const params = _qs.default.parse(u.query); // Append widget ID to query parameters


    params.widgetId = this.props.app.id; // Append current / parent URL, minus the hash because that will change when
    // we view a different room (ie. may change for persistent widgets)

    params.parentUrl = window.location.href.split('#', 2)[0];
    u.search = undefined;
    u.query = params;
    return u.format();
  }

  isMixedContent() {
    const parentContentProtocol = window.location.protocol;

    const u = _url.default.parse(this.props.app.url);

    const childContentProtocol = u.protocol;

    if (parentContentProtocol === 'https:' && childContentProtocol !== 'https:') {
      console.warn("Refusing to load mixed-content app:", parentContentProtocol, childContentProtocol, window.location, this.props.app.url);
      return true;
    }

    return false;
  }

  componentDidMount() {
    // Only fetch IM token on mount if we're showing and have permission to load
    if (this.props.show && this.state.hasPermissionToLoad) {
      this.setScalarToken();
    } // Widget action listeners


    this.dispatcherRef = _dispatcher.default.register(this._onAction);
  }

  componentWillUnmount() {
    // Widget action listeners
    if (this.dispatcherRef) _dispatcher.default.unregister(this.dispatcherRef); // if it's not remaining on screen, get rid of the PersistedElement container

    if (!_ActiveWidgetStore.default.getWidgetPersistence(this.props.app.id)) {
      _ActiveWidgetStore.default.destroyPersistentWidget(this.props.app.id);

      const PersistedElement = sdk.getComponent("elements.PersistedElement");
      PersistedElement.destroyElement(this._persistKey);
    }
  }
  /**
   * Adds a scalar token to the widget URL, if required
   * Component initialisation is only complete when this function has resolved
   */


  setScalarToken() {
    if (!_WidgetUtils.default.isScalarUrl(this.props.app.url)) {
      console.warn('Non-scalar widget, not setting scalar token!', _url.default);
      this.setState({
        error: null,
        widgetUrl: this._addWurlParams(this.props.app.url),
        initialising: false
      });
      return;
    }

    const managers = _IntegrationManagers.IntegrationManagers.sharedInstance();

    if (!managers.hasManager()) {
      console.warn("No integration manager - not setting scalar token", _url.default);
      this.setState({
        error: null,
        widgetUrl: this._addWurlParams(this.props.app.url),
        initialising: false
      });
      return;
    } // TODO: Pick the right manager for the widget


    const defaultManager = managers.getPrimaryManager();

    if (!_WidgetUtils.default.isScalarUrl(defaultManager.apiUrl)) {
      console.warn('Non-scalar manager, not setting scalar token!', _url.default);
      this.setState({
        error: null,
        widgetUrl: this._addWurlParams(this.props.app.url),
        initialising: false
      });
      return;
    } // Fetch the token before loading the iframe as we need it to mangle the URL


    if (!this._scalarClient) {
      this._scalarClient = defaultManager.getScalarClient();
    }

    this._scalarClient.getScalarToken().then(token => {
      // Append scalar_token as a query param if not already present
      this._scalarClient.scalarToken = token;

      const u = _url.default.parse(this._addWurlParams(this.props.app.url));

      const params = _qs.default.parse(u.query);

      if (!params.scalar_token) {
        params.scalar_token = encodeURIComponent(token); // u.search must be set to undefined, so that u.format() uses query parameters - https://nodejs.org/docs/latest/api/url.html#url_url_format_url_options

        u.search = undefined;
        u.query = params;
      }

      this.setState({
        error: null,
        widgetUrl: u.format(),
        initialising: false
      }); // Fetch page title from remote content if not already set

      if (!this.state.widgetPageTitle && params.url) {
        this._fetchWidgetTitle(params.url);
      }
    }, err => {
      console.error("Failed to get scalar_token", err);
      this.setState({
        error: err.message,
        initialising: false
      });
    });
  } // TODO: [REACT-WARNING] Replace with appropriate lifecycle event


  UNSAFE_componentWillReceiveProps(nextProps) {
    // eslint-disable-line camelcase
    if (nextProps.app.url !== this.props.app.url) {
      this._getNewState(nextProps); // Fetch IM token for new URL if we're showing and have permission to load


      if (this.props.show && this.state.hasPermissionToLoad) {
        this.setScalarToken();
      }
    } else if (nextProps.show && !this.props.show) {
      // We assume that persisted widgets are loaded and don't need a spinner.
      if (this.props.waitForIframeLoad && !_PersistedElement.default.isMounted(this._persistKey)) {
        this.setState({
          loading: true
        });
      } // Fetch IM token now that we're showing if we already have permission to load


      if (this.state.hasPermissionToLoad) {
        this.setScalarToken();
      }
    } else if (nextProps.widgetPageTitle !== this.props.widgetPageTitle) {
      this.setState({
        widgetPageTitle: nextProps.widgetPageTitle
      });
    }
  }

  _canUserModify() {
    // User widgets should always be modifiable by their creator
    if (this.props.userWidget && _MatrixClientPeg.MatrixClientPeg.get().credentials.userId === this.props.creatorUserId) {
      return true;
    } // Check if the current user can modify widgets in the current room


    return _WidgetUtils.default.canUserModifyWidgets(this.props.room.roomId);
  }

  _onEditClick() {
    console.log("Edit widget ID ", this.props.app.id);

    if (this.props.onEditClick) {
      this.props.onEditClick();
    } else {
      // TODO: Open the right manager for the widget
      if (_SettingsStore.default.isFeatureEnabled("feature_many_integration_managers")) {
        _IntegrationManagers.IntegrationManagers.sharedInstance().openAll(this.props.room, 'type_' + this.props.type, this.props.app.id);
      } else {
        _IntegrationManagers.IntegrationManagers.sharedInstance().getPrimaryManager().open(this.props.room, 'type_' + this.props.type, this.props.app.id);
      }
    }
  }

  _onSnapshotClick() {
    console.log("Requesting widget snapshot");

    _ActiveWidgetStore.default.getWidgetMessaging(this.props.app.id).getScreenshot().catch(err => {
      console.error("Failed to get screenshot", err);
    }).then(screenshot => {
      _dispatcher.default.dispatch({
        action: 'picture_snapshot',
        file: screenshot
      }, true);
    });
  }
  /* If user has permission to modify widgets, delete the widget,
   * otherwise revoke access for the widget to load in the user's browser
  */


  _onDeleteClick() {
    if (this.props.onDeleteClick) {
      this.props.onDeleteClick();
    } else if (this._canUserModify()) {
      // Show delete confirmation dialog
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      _Modal.default.createTrackedDialog('Delete Widget', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Delete Widget"),
        description: (0, _languageHandler._t)("Deleting a widget removes it for all users in this room." + " Are you sure you want to delete this widget?"),
        button: (0, _languageHandler._t)("Delete widget"),
        onFinished: confirmed => {
          if (!confirmed) {
            return;
          }

          this.setState({
            deleting: true
          }); // HACK: This is a really dirty way to ensure that Jitsi cleans up
          // its hold on the webcam. Without this, the widget holds a media
          // stream open, even after death. See https://github.com/vector-im/riot-web/issues/7351

          if (this._appFrame.current) {
            // In practice we could just do `+= ''` to trick the browser
            // into thinking the URL changed, however I can foresee this
            // being optimized out by a browser. Instead, we'll just point
            // the iframe at a page that is reasonably safe to use in the
            // event the iframe doesn't wink away.
            // This is relative to where the Riot instance is located.
            this._appFrame.current.src = 'about:blank';
          }

          _WidgetUtils.default.setRoomWidget(this.props.room.roomId, this.props.app.id).catch(e => {
            console.error('Failed to delete widget', e);
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

            _Modal.default.createTrackedDialog('Failed to remove widget', '', ErrorDialog, {
              title: (0, _languageHandler._t)('Failed to remove widget'),
              description: (0, _languageHandler._t)('An error ocurred whilst trying to remove the widget from the room')
            });
          }).finally(() => {
            this.setState({
              deleting: false
            });
          });
        }
      });
    }
  }

  _onRevokeClicked() {
    console.info("Revoke widget permissions - %s", this.props.app.id);

    this._revokeWidgetPermission();
  }
  /**
   * Called when widget iframe has finished loading
   */


  _onLoaded() {
    // Destroy the old widget messaging before starting it back up again. Some widgets
    // have startup routines that run when they are loaded, so we just need to reinitialize
    // the messaging for them.
    _ActiveWidgetStore.default.delWidgetMessaging(this.props.app.id);

    this._setupWidgetMessaging();

    _ActiveWidgetStore.default.setRoomId(this.props.app.id, this.props.room.roomId);

    this.setState({
      loading: false
    });
  }

  _setupWidgetMessaging() {
    // FIXME: There's probably no reason to do this here: it should probably be done entirely
    // in ActiveWidgetStore.
    const widgetMessaging = new _WidgetMessaging.default(this.props.app.id, this._getRenderedUrl(), this.props.userWidget, this._appFrame.current.contentWindow);

    _ActiveWidgetStore.default.setWidgetMessaging(this.props.app.id, widgetMessaging);

    widgetMessaging.getCapabilities().then(requestedCapabilities => {
      console.log("Widget ".concat(this.props.app.id, " requested capabilities: ") + requestedCapabilities);
      requestedCapabilities = requestedCapabilities || []; // Allow whitelisted capabilities

      let requestedWhitelistCapabilies = [];

      if (this.props.whitelistCapabilities && this.props.whitelistCapabilities.length > 0) {
        requestedWhitelistCapabilies = requestedCapabilities.filter(function (e) {
          return this.indexOf(e) >= 0;
        }, this.props.whitelistCapabilities);

        if (requestedWhitelistCapabilies.length > 0) {
          console.log("Widget ".concat(this.props.app.id, " allowing requested, whitelisted properties: ") + requestedWhitelistCapabilies);
        }
      } // TODO -- Add UI to warn about and optionally allow requested capabilities


      _ActiveWidgetStore.default.setWidgetCapabilities(this.props.app.id, requestedWhitelistCapabilies);

      if (this.props.onCapabilityRequest) {
        this.props.onCapabilityRequest(requestedCapabilities);
      } // We only tell Jitsi widgets that we're ready because they're realistically the only ones
      // using this custom extension to the widget API.


      if (this.props.app.type === 'jitsi') {
        widgetMessaging.flagReadyToContinue();
      }
    }).catch(err => {
      console.log("Failed to get capabilities for widget type ".concat(this.props.app.type), this.props.app.id, err);
    });
  }

  _onAction(payload) {
    if (payload.widgetId === this.props.app.id) {
      switch (payload.action) {
        case 'm.sticker':
          if (this._hasCapability('m.sticker')) {
            _dispatcher.default.dispatch({
              action: 'post_sticker_message',
              data: payload.data
            });
          } else {
            console.warn('Ignoring sticker message. Invalid capability');
          }

          break;
      }
    }
  }
  /**
   * Set remote content title on AppTile
   * @param {string} url Url to check for title
   */


  _fetchWidgetTitle(url) {
    this._scalarClient.getScalarPageTitle(url).then(widgetPageTitle => {
      if (widgetPageTitle) {
        this.setState({
          widgetPageTitle: widgetPageTitle
        });
      }
    }, err => {
      console.error("Failed to get page title", err);
    });
  }

  _grantWidgetPermission() {
    const roomId = this.props.room.roomId;
    console.info("Granting permission for widget to load: " + this.props.app.eventId);

    const current = _SettingsStore.default.getValue("allowedWidgets", roomId);

    current[this.props.app.eventId] = true;

    _SettingsStore.default.setValue("allowedWidgets", roomId, _SettingsStore.SettingLevel.ROOM_ACCOUNT, current).then(() => {
      this.setState({
        hasPermissionToLoad: true
      }); // Fetch a token for the integration manager, now that we're allowed to

      this.setScalarToken();
    }).catch(err => {
      console.error(err); // We don't really need to do anything about this - the user will just hit the button again.
    });
  }

  _revokeWidgetPermission() {
    const roomId = this.props.room.roomId;
    console.info("Revoking permission for widget to load: " + this.props.app.eventId);

    const current = _SettingsStore.default.getValue("allowedWidgets", roomId);

    current[this.props.app.eventId] = false;

    _SettingsStore.default.setValue("allowedWidgets", roomId, _SettingsStore.SettingLevel.ROOM_ACCOUNT, current).then(() => {
      this.setState({
        hasPermissionToLoad: false
      }); // Force the widget to be non-persistent (able to be deleted/forgotten)

      _ActiveWidgetStore.default.destroyPersistentWidget(this.props.app.id);

      const PersistedElement = sdk.getComponent("elements.PersistedElement");
      PersistedElement.destroyElement(this._persistKey);
    }).catch(err => {
      console.error(err); // We don't really need to do anything about this - the user will just hit the button again.
    });
  }

  formatAppTileName() {
    let appTileName = "No name";

    if (this.props.app.name && this.props.app.name.trim()) {
      appTileName = this.props.app.name.trim();
    }

    return appTileName;
  }

  onClickMenuBar(ev) {
    ev.preventDefault(); // Ignore clicks on menu bar children

    if (ev.target !== this._menu_bar.current) {
      return;
    } // Toggle the view state of the apps drawer


    if (this.props.userWidget) {
      this._onMinimiseClick();
    } else {
      _dispatcher.default.dispatch({
        action: 'appsDrawer',
        show: !this.props.show
      });
    }
  }
  /**
   * Replace the widget template variables in a url with their values
   *
   * @param {string} u The URL with template variables
   *
   * @returns {string} url with temlate variables replaced
   */


  _templatedUrl(u) {
    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().credentials.userId;

    const myUser = _MatrixClientPeg.MatrixClientPeg.get().getUser(myUserId);

    const vars = Object.assign({
      domain: "jitsi.riot.im" // v1 widgets have this hardcoded

    }, this.props.app.data, {
      'matrix_user_id': myUserId,
      'matrix_room_id': this.props.room.roomId,
      'matrix_display_name': myUser ? myUser.displayName : myUserId,
      'matrix_avatar_url': myUser ? _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(myUser.avatarUrl) : '',
      // TODO: Namespace themes through some standard
      'theme': _SettingsStore.default.getValue("theme")
    });

    if (vars.conferenceId === undefined) {
      // we'll need to parse the conference ID out of the URL for v1 Jitsi widgets
      const parsedUrl = new URL(this.props.app.url);
      vars.conferenceId = parsedUrl.searchParams.get("confId");
    }

    return uriFromTemplate(u, vars);
  }
  /**
   * Get the URL used in the iframe
   * In cases where we supply our own UI for a widget, this is an internal
   * URL different to the one used if the widget is popped out to a separate
   * tab / browser
   *
   * @returns {string} url
   */


  _getRenderedUrl() {
    let url;

    if (this.props.app.type === 'jitsi') {
      console.log("Replacing Jitsi widget URL with local wrapper");
      url = _WidgetUtils.default.getLocalJitsiWrapperUrl({
        forLocalRender: true
      });
      url = this._addWurlParams(url);
    } else {
      url = this._getSafeUrl(this.state.widgetUrl);
    }

    return this._templatedUrl(url);
  }

  _getPopoutUrl() {
    if (this.props.app.type === 'jitsi') {
      return this._templatedUrl(_WidgetUtils.default.getLocalJitsiWrapperUrl({
        forLocalRender: false
      }));
    } else {
      // use app.url, not state.widgetUrl, because we want the one without
      // the wURL params for the popped-out version.
      return this._templatedUrl(this._getSafeUrl(this.props.app.url));
    }
  }

  _getSafeUrl(u) {
    const parsedWidgetUrl = _url.default.parse(u, true);

    if (ENABLE_REACT_PERF) {
      parsedWidgetUrl.search = null;
      parsedWidgetUrl.query.react_perf = true;
    }

    let safeWidgetUrl = '';

    if (ALLOWED_APP_URL_SCHEMES.includes(parsedWidgetUrl.protocol)) {
      safeWidgetUrl = _url.default.format(parsedWidgetUrl);
    }

    return safeWidgetUrl;
  }

  _getTileTitle() {
    const name = this.formatAppTileName();

    const titleSpacer = _react.default.createElement("span", null, "\xA0-\xA0");

    let title = '';

    if (this.state.widgetPageTitle && this.state.widgetPageTitle != this.formatAppTileName()) {
      title = this.state.widgetPageTitle;
    }

    return _react.default.createElement("span", null, _react.default.createElement("b", null, name), _react.default.createElement("span", null, title ? titleSpacer : '', title));
  }

  _onMinimiseClick(e) {
    if (this.props.onMinimiseClick) {
      this.props.onMinimiseClick();
    }
  }

  _onPopoutWidgetClick() {
    // Using Object.assign workaround as the following opens in a new window instead of a new tab.
    // window.open(this._getPopoutUrl(), '_blank', 'noopener=yes');
    Object.assign(document.createElement('a'), {
      target: '_blank',
      href: this._getPopoutUrl(),
      rel: 'noreferrer noopener'
    }).click();
  }

  _onReloadWidgetClick() {
    // Reload iframe in this way to avoid cross-origin restrictions
    this._appFrame.current.src = this._appFrame.current.src;
  }

  render() {
    let appTileBody; // Don't render widget if it is in the process of being deleted

    if (this.state.deleting) {
      return _react.default.createElement("div", null);
    } // Note that there is advice saying allow-scripts shouldn't be used with allow-same-origin
    // because that would allow the iframe to programmatically remove the sandbox attribute, but
    // this would only be for content hosted on the same origin as the riot client: anything
    // hosted on the same origin as the client will get the same access as if you clicked
    // a link to it.


    const sandboxFlags = "allow-forms allow-popups allow-popups-to-escape-sandbox " + "allow-same-origin allow-scripts allow-presentation"; // Additional iframe feature pemissions
    // (see - https://sites.google.com/a/chromium.org/dev/Home/chromium-security/deprecating-permissions-in-cross-origin-iframes and https://wicg.github.io/feature-policy/)

    const iframeFeatures = "microphone; camera; encrypted-media; autoplay;";
    const appTileBodyClass = 'mx_AppTileBody' + (this.props.miniMode ? '_mini  ' : ' ');

    if (this.props.show) {
      const loadingElement = _react.default.createElement("div", {
        className: "mx_AppLoading_spinner_fadeIn"
      }, _react.default.createElement(_MessageSpinner.default, {
        msg: "Loading..."
      }));

      if (!this.state.hasPermissionToLoad) {
        const isEncrypted = _MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(this.props.room.roomId);

        appTileBody = _react.default.createElement("div", {
          className: appTileBodyClass
        }, _react.default.createElement(_AppPermission.default, {
          roomId: this.props.room.roomId,
          creatorUserId: this.props.creatorUserId,
          url: this.state.widgetUrl,
          isRoomEncrypted: isEncrypted,
          onPermissionGranted: this._grantWidgetPermission
        }));
      } else if (this.state.initialising) {
        appTileBody = _react.default.createElement("div", {
          className: appTileBodyClass + (this.state.loading ? 'mx_AppLoading' : '')
        }, loadingElement);
      } else {
        if (this.isMixedContent()) {
          appTileBody = _react.default.createElement("div", {
            className: appTileBodyClass
          }, _react.default.createElement(_AppWarning.default, {
            errorMsg: "Error - Mixed content"
          }));
        } else {
          appTileBody = _react.default.createElement("div", {
            className: appTileBodyClass + (this.state.loading ? 'mx_AppLoading' : '')
          }, this.state.loading && loadingElement, _react.default.createElement("iframe", {
            allow: iframeFeatures,
            ref: this._appFrame,
            src: this._getRenderedUrl(),
            allowFullScreen: true,
            sandbox: sandboxFlags,
            onLoad: this._onLoaded
          })); // if the widget would be allowed to remain on screen, we must put it in
          // a PersistedElement from the get-go, otherwise the iframe will be
          // re-mounted later when we do.

          if (this.props.whitelistCapabilities.includes('m.always_on_screen')) {
            const PersistedElement = sdk.getComponent("elements.PersistedElement"); // Also wrap the PersistedElement in a div to fix the height, otherwise
            // AppTile's border is in the wrong place

            appTileBody = _react.default.createElement("div", {
              className: "mx_AppTile_persistedWrapper"
            }, _react.default.createElement(PersistedElement, {
              persistKey: this._persistKey
            }, appTileBody));
          }
        }
      }
    }

    const showMinimiseButton = this.props.showMinimise && this.props.show;
    const showMaximiseButton = this.props.showMinimise && !this.props.show;
    let appTileClass;

    if (this.props.miniMode) {
      appTileClass = 'mx_AppTile_mini';
    } else if (this.props.fullWidth) {
      appTileClass = 'mx_AppTileFullWidth';
    } else {
      appTileClass = 'mx_AppTile';
    }

    const menuBarClasses = (0, _classnames.default)({
      mx_AppTileMenuBar: true,
      mx_AppTileMenuBar_expanded: this.props.show
    });
    let contextMenu;

    if (this.state.menuDisplayed) {
      const elementRect = this._contextMenuButton.current.getBoundingClientRect();

      const canUserModify = this._canUserModify();

      const showEditButton = Boolean(this._scalarClient && canUserModify);
      const showDeleteButton = (this.props.showDelete === undefined || this.props.showDelete) && canUserModify;
      const showPictureSnapshotButton = this._hasCapability('m.capability.screenshot') && this.props.show;
      const WidgetContextMenu = sdk.getComponent('views.context_menus.WidgetContextMenu');
      contextMenu = _react.default.createElement(_ContextMenu.ContextMenu, (0, _extends2.default)({}, (0, _ContextMenu.aboveLeftOf)(elementRect, null), {
        onFinished: this._closeContextMenu
      }), _react.default.createElement(WidgetContextMenu, {
        onRevokeClicked: this._onRevokeClicked,
        onEditClicked: showEditButton ? this._onEditClick : undefined,
        onDeleteClicked: showDeleteButton ? this._onDeleteClick : undefined,
        onSnapshotClicked: showPictureSnapshotButton ? this._onSnapshotClick : undefined,
        onReloadClicked: this.props.showReload ? this._onReloadWidgetClick : undefined,
        onFinished: this._closeContextMenu
      }));
    }

    return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
      className: appTileClass,
      id: this.props.app.id
    }, this.props.showMenubar && _react.default.createElement("div", {
      ref: this._menu_bar,
      className: menuBarClasses,
      onClick: this.onClickMenuBar
    }, _react.default.createElement("span", {
      className: "mx_AppTileMenuBarTitle",
      style: {
        pointerEvents: this.props.handleMinimisePointerEvents ? 'all' : false
      }
    }, showMinimiseButton && _react.default.createElement(_AccessibleButton.default, {
      className: "mx_AppTileMenuBar_iconButton mx_AppTileMenuBar_iconButton_minimise",
      title: (0, _languageHandler._t)('Minimize apps'),
      onClick: this._onMinimiseClick
    }), showMaximiseButton && _react.default.createElement(_AccessibleButton.default, {
      className: "mx_AppTileMenuBar_iconButton mx_AppTileMenuBar_iconButton_maximise",
      title: (0, _languageHandler._t)('Maximize apps'),
      onClick: this._onMinimiseClick
    }), this.props.showTitle && this._getTileTitle()), _react.default.createElement("span", {
      className: "mx_AppTileMenuBarWidgets"
    }, this.props.showPopout && _react.default.createElement(_AccessibleButton.default, {
      className: "mx_AppTileMenuBar_iconButton mx_AppTileMenuBar_iconButton_popout",
      title: (0, _languageHandler._t)('Popout widget'),
      onClick: this._onPopoutWidgetClick
    }), _react.default.createElement(_ContextMenu.ContextMenuButton, {
      className: "mx_AppTileMenuBar_iconButton mx_AppTileMenuBar_iconButton_menu",
      label: (0, _languageHandler._t)('More options'),
      isExpanded: this.state.menuDisplayed,
      inputRef: this._contextMenuButton,
      onClick: this._onContextMenuClick
    }))), appTileBody), contextMenu);
  }

}

exports.default = AppTile;
AppTile.displayName = 'AppTile';
AppTile.propTypes = {
  app: _propTypes.default.object.isRequired,
  room: _propTypes.default.object.isRequired,
  // Specifying 'fullWidth' as true will render the app tile to fill the width of the app drawer continer.
  // This should be set to true when there is only one widget in the app drawer, otherwise it should be false.
  fullWidth: _propTypes.default.bool,
  // Optional. If set, renders a smaller view of the widget
  miniMode: _propTypes.default.bool,
  // UserId of the current user
  userId: _propTypes.default.string.isRequired,
  // UserId of the entity that added / modified the widget
  creatorUserId: _propTypes.default.string,
  waitForIframeLoad: _propTypes.default.bool,
  showMenubar: _propTypes.default.bool,
  // Should the AppTile render itself
  show: _propTypes.default.bool,
  // Optional onEditClickHandler (overrides default behaviour)
  onEditClick: _propTypes.default.func,
  // Optional onDeleteClickHandler (overrides default behaviour)
  onDeleteClick: _propTypes.default.func,
  // Optional onMinimiseClickHandler
  onMinimiseClick: _propTypes.default.func,
  // Optionally hide the tile title
  showTitle: _propTypes.default.bool,
  // Optionally hide the tile minimise icon
  showMinimise: _propTypes.default.bool,
  // Optionally handle minimise button pointer events (default false)
  handleMinimisePointerEvents: _propTypes.default.bool,
  // Optionally hide the delete icon
  showDelete: _propTypes.default.bool,
  // Optionally hide the popout widget icon
  showPopout: _propTypes.default.bool,
  // Optionally show the reload widget icon
  // This is not currently intended for use with production widgets. However
  // it can be useful when developing persistent widgets in order to avoid
  // having to reload all of riot to get new widget content.
  showReload: _propTypes.default.bool,
  // Widget capabilities to allow by default (without user confirmation)
  // NOTE -- Use with caution. This is intended to aid better integration / UX
  // basic widget capabilities, e.g. injecting sticker message events.
  whitelistCapabilities: _propTypes.default.array,
  // Optional function to be called on widget capability request
  // Called with an array of the requested capabilities
  onCapabilityRequest: _propTypes.default.func,
  // Is this an instance of a user widget
  userWidget: _propTypes.default.bool
};
AppTile.defaultProps = {
  waitForIframeLoad: true,
  showMenubar: true,
  showTitle: true,
  showMinimise: true,
  showDelete: true,
  showPopout: true,
  showReload: false,
  handleMinimisePointerEvents: false,
  whitelistCapabilities: [],
  userWidget: false,
  miniMode: false
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0FwcFRpbGUuanMiXSwibmFtZXMiOlsiQUxMT1dFRF9BUFBfVVJMX1NDSEVNRVMiLCJFTkFCTEVfUkVBQ1RfUEVSRiIsInVyaUZyb21UZW1wbGF0ZSIsInVyaVRlbXBsYXRlIiwidmFyaWFibGVzIiwib3V0Iiwia2V5IiwidmFsIiwiT2JqZWN0IiwiZW50cmllcyIsInJlcGxhY2UiLCJlbmNvZGVVUklDb21wb25lbnQiLCJBcHBUaWxlIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwic2V0U3RhdGUiLCJtZW51RGlzcGxheWVkIiwiX3BlcnNpc3RLZXkiLCJhcHAiLCJpZCIsInN0YXRlIiwiX2dldE5ld1N0YXRlIiwiX29uQWN0aW9uIiwiYmluZCIsIl9vbkxvYWRlZCIsIl9vbkVkaXRDbGljayIsIl9vbkRlbGV0ZUNsaWNrIiwiX29uUmV2b2tlQ2xpY2tlZCIsIl9vblNuYXBzaG90Q2xpY2siLCJvbkNsaWNrTWVudUJhciIsIl9vbk1pbmltaXNlQ2xpY2siLCJfZ3JhbnRXaWRnZXRQZXJtaXNzaW9uIiwiX3Jldm9rZVdpZGdldFBlcm1pc3Npb24iLCJfb25Qb3BvdXRXaWRnZXRDbGljayIsIl9vblJlbG9hZFdpZGdldENsaWNrIiwiX2NvbnRleHRNZW51QnV0dG9uIiwiX2FwcEZyYW1lIiwiX21lbnVfYmFyIiwibmV3UHJvcHMiLCJoYXNQZXJtaXNzaW9uVG9Mb2FkIiwiY3VycmVudGx5QWxsb3dlZFdpZGdldHMiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJyb29tIiwicm9vbUlkIiwiZXZlbnRJZCIsIlBlcnNpc3RlZEVsZW1lbnQiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJpbml0aWFsaXNpbmciLCJsb2FkaW5nIiwid2FpdEZvcklmcmFtZUxvYWQiLCJpc01vdW50ZWQiLCJ3aWRnZXRVcmwiLCJfYWRkV3VybFBhcmFtcyIsInVybCIsInVzZXJJZCIsImNyZWF0b3JVc2VySWQiLCJlcnJvciIsImRlbGV0aW5nIiwid2lkZ2V0UGFnZVRpdGxlIiwiX2hhc0NhcGFiaWxpdHkiLCJjYXBhYmlsaXR5IiwiQWN0aXZlV2lkZ2V0U3RvcmUiLCJ3aWRnZXRIYXNDYXBhYmlsaXR5IiwidXJsU3RyaW5nIiwidSIsInBhcnNlIiwiY29uc29sZSIsInBhcmFtcyIsInFzIiwicXVlcnkiLCJ3aWRnZXRJZCIsInBhcmVudFVybCIsIndpbmRvdyIsImxvY2F0aW9uIiwiaHJlZiIsInNwbGl0Iiwic2VhcmNoIiwidW5kZWZpbmVkIiwiZm9ybWF0IiwiaXNNaXhlZENvbnRlbnQiLCJwYXJlbnRDb250ZW50UHJvdG9jb2wiLCJwcm90b2NvbCIsImNoaWxkQ29udGVudFByb3RvY29sIiwid2FybiIsImNvbXBvbmVudERpZE1vdW50Iiwic2hvdyIsInNldFNjYWxhclRva2VuIiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bnJlZ2lzdGVyIiwiZ2V0V2lkZ2V0UGVyc2lzdGVuY2UiLCJkZXN0cm95UGVyc2lzdGVudFdpZGdldCIsImRlc3Ryb3lFbGVtZW50IiwiV2lkZ2V0VXRpbHMiLCJpc1NjYWxhclVybCIsIm1hbmFnZXJzIiwiSW50ZWdyYXRpb25NYW5hZ2VycyIsInNoYXJlZEluc3RhbmNlIiwiaGFzTWFuYWdlciIsImRlZmF1bHRNYW5hZ2VyIiwiZ2V0UHJpbWFyeU1hbmFnZXIiLCJhcGlVcmwiLCJfc2NhbGFyQ2xpZW50IiwiZ2V0U2NhbGFyQ2xpZW50IiwiZ2V0U2NhbGFyVG9rZW4iLCJ0aGVuIiwidG9rZW4iLCJzY2FsYXJUb2tlbiIsInNjYWxhcl90b2tlbiIsIl9mZXRjaFdpZGdldFRpdGxlIiwiZXJyIiwibWVzc2FnZSIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV4dFByb3BzIiwiX2NhblVzZXJNb2RpZnkiLCJ1c2VyV2lkZ2V0IiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiY3JlZGVudGlhbHMiLCJjYW5Vc2VyTW9kaWZ5V2lkZ2V0cyIsImxvZyIsIm9uRWRpdENsaWNrIiwiaXNGZWF0dXJlRW5hYmxlZCIsIm9wZW5BbGwiLCJ0eXBlIiwib3BlbiIsImdldFdpZGdldE1lc3NhZ2luZyIsImdldFNjcmVlbnNob3QiLCJjYXRjaCIsInNjcmVlbnNob3QiLCJkaXNwYXRjaCIsImFjdGlvbiIsImZpbGUiLCJvbkRlbGV0ZUNsaWNrIiwiUXVlc3Rpb25EaWFsb2ciLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiYnV0dG9uIiwib25GaW5pc2hlZCIsImNvbmZpcm1lZCIsImN1cnJlbnQiLCJzcmMiLCJzZXRSb29tV2lkZ2V0IiwiZSIsIkVycm9yRGlhbG9nIiwiZmluYWxseSIsImluZm8iLCJkZWxXaWRnZXRNZXNzYWdpbmciLCJfc2V0dXBXaWRnZXRNZXNzYWdpbmciLCJzZXRSb29tSWQiLCJ3aWRnZXRNZXNzYWdpbmciLCJXaWRnZXRNZXNzYWdpbmciLCJfZ2V0UmVuZGVyZWRVcmwiLCJjb250ZW50V2luZG93Iiwic2V0V2lkZ2V0TWVzc2FnaW5nIiwiZ2V0Q2FwYWJpbGl0aWVzIiwicmVxdWVzdGVkQ2FwYWJpbGl0aWVzIiwicmVxdWVzdGVkV2hpdGVsaXN0Q2FwYWJpbGllcyIsIndoaXRlbGlzdENhcGFiaWxpdGllcyIsImxlbmd0aCIsImZpbHRlciIsImluZGV4T2YiLCJzZXRXaWRnZXRDYXBhYmlsaXRpZXMiLCJvbkNhcGFiaWxpdHlSZXF1ZXN0IiwiZmxhZ1JlYWR5VG9Db250aW51ZSIsInBheWxvYWQiLCJkYXRhIiwiZ2V0U2NhbGFyUGFnZVRpdGxlIiwic2V0VmFsdWUiLCJTZXR0aW5nTGV2ZWwiLCJST09NX0FDQ09VTlQiLCJmb3JtYXRBcHBUaWxlTmFtZSIsImFwcFRpbGVOYW1lIiwibmFtZSIsInRyaW0iLCJldiIsInByZXZlbnREZWZhdWx0IiwidGFyZ2V0IiwiX3RlbXBsYXRlZFVybCIsIm15VXNlcklkIiwibXlVc2VyIiwiZ2V0VXNlciIsInZhcnMiLCJhc3NpZ24iLCJkb21haW4iLCJkaXNwbGF5TmFtZSIsIm14Y1VybFRvSHR0cCIsImF2YXRhclVybCIsImNvbmZlcmVuY2VJZCIsInBhcnNlZFVybCIsIlVSTCIsInNlYXJjaFBhcmFtcyIsImdldExvY2FsSml0c2lXcmFwcGVyVXJsIiwiZm9yTG9jYWxSZW5kZXIiLCJfZ2V0U2FmZVVybCIsIl9nZXRQb3BvdXRVcmwiLCJwYXJzZWRXaWRnZXRVcmwiLCJyZWFjdF9wZXJmIiwic2FmZVdpZGdldFVybCIsImluY2x1ZGVzIiwiX2dldFRpbGVUaXRsZSIsInRpdGxlU3BhY2VyIiwib25NaW5pbWlzZUNsaWNrIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwicmVsIiwiY2xpY2siLCJyZW5kZXIiLCJhcHBUaWxlQm9keSIsInNhbmRib3hGbGFncyIsImlmcmFtZUZlYXR1cmVzIiwiYXBwVGlsZUJvZHlDbGFzcyIsIm1pbmlNb2RlIiwibG9hZGluZ0VsZW1lbnQiLCJpc0VuY3J5cHRlZCIsImlzUm9vbUVuY3J5cHRlZCIsInNob3dNaW5pbWlzZUJ1dHRvbiIsInNob3dNaW5pbWlzZSIsInNob3dNYXhpbWlzZUJ1dHRvbiIsImFwcFRpbGVDbGFzcyIsImZ1bGxXaWR0aCIsIm1lbnVCYXJDbGFzc2VzIiwibXhfQXBwVGlsZU1lbnVCYXIiLCJteF9BcHBUaWxlTWVudUJhcl9leHBhbmRlZCIsImNvbnRleHRNZW51IiwiZWxlbWVudFJlY3QiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJjYW5Vc2VyTW9kaWZ5Iiwic2hvd0VkaXRCdXR0b24iLCJCb29sZWFuIiwic2hvd0RlbGV0ZUJ1dHRvbiIsInNob3dEZWxldGUiLCJzaG93UGljdHVyZVNuYXBzaG90QnV0dG9uIiwiV2lkZ2V0Q29udGV4dE1lbnUiLCJfY2xvc2VDb250ZXh0TWVudSIsInNob3dSZWxvYWQiLCJzaG93TWVudWJhciIsInBvaW50ZXJFdmVudHMiLCJoYW5kbGVNaW5pbWlzZVBvaW50ZXJFdmVudHMiLCJzaG93VGl0bGUiLCJzaG93UG9wb3V0IiwiX29uQ29udGV4dE1lbnVDbGljayIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJib29sIiwic3RyaW5nIiwiZnVuYyIsImFycmF5IiwiZGVmYXVsdFByb3BzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdkNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5Q0EsTUFBTUEsdUJBQXVCLEdBQUcsQ0FBQyxRQUFELEVBQVcsT0FBWCxDQUFoQztBQUNBLE1BQU1DLGlCQUFpQixHQUFHLEtBQTFCO0FBRUE7Ozs7Ozs7OztBQVFBLFNBQVNDLGVBQVQsQ0FBeUJDLFdBQXpCLEVBQXNDQyxTQUF0QyxFQUFpRDtBQUM3QyxNQUFJQyxHQUFHLEdBQUdGLFdBQVY7O0FBQ0EsT0FBSyxNQUFNLENBQUNHLEdBQUQsRUFBTUMsR0FBTixDQUFYLElBQXlCQyxNQUFNLENBQUNDLE9BQVAsQ0FBZUwsU0FBZixDQUF6QixFQUFvRDtBQUNoREMsSUFBQUEsR0FBRyxHQUFHQSxHQUFHLENBQUNLLE9BQUosQ0FDRixNQUFNSixHQURKLEVBQ1NLLGtCQUFrQixDQUFDSixHQUFELENBRDNCLENBQU47QUFHSDs7QUFDRCxTQUFPRixHQUFQO0FBQ0g7O0FBRWMsTUFBTU8sT0FBTixTQUFzQkMsZUFBTUMsU0FBNUIsQ0FBc0M7QUFDakRDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTixFQURlLENBR2Y7O0FBSGUsK0RBMmtCRyxNQUFNO0FBQ3hCLFdBQUtDLFFBQUwsQ0FBYztBQUFFQyxRQUFBQSxhQUFhLEVBQUU7QUFBakIsT0FBZDtBQUNILEtBN2tCa0I7QUFBQSw2REEra0JDLE1BQU07QUFDdEIsV0FBS0QsUUFBTCxDQUFjO0FBQUVDLFFBQUFBLGFBQWEsRUFBRTtBQUFqQixPQUFkO0FBQ0gsS0FqbEJrQjtBQUlmLFNBQUtDLFdBQUwsR0FBbUIsWUFBWSxLQUFLSCxLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFBOUM7QUFFQSxTQUFLQyxLQUFMLEdBQWEsS0FBS0MsWUFBTCxDQUFrQlAsS0FBbEIsQ0FBYjtBQUVBLFNBQUtRLFNBQUwsR0FBaUIsS0FBS0EsU0FBTCxDQUFlQyxJQUFmLENBQW9CLElBQXBCLENBQWpCO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixLQUFLQSxTQUFMLENBQWVELElBQWYsQ0FBb0IsSUFBcEIsQ0FBakI7QUFDQSxTQUFLRSxZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JGLElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0EsU0FBS0csY0FBTCxHQUFzQixLQUFLQSxjQUFMLENBQW9CSCxJQUFwQixDQUF5QixJQUF6QixDQUF0QjtBQUNBLFNBQUtJLGdCQUFMLEdBQXdCLEtBQUtBLGdCQUFMLENBQXNCSixJQUF0QixDQUEyQixJQUEzQixDQUF4QjtBQUNBLFNBQUtLLGdCQUFMLEdBQXdCLEtBQUtBLGdCQUFMLENBQXNCTCxJQUF0QixDQUEyQixJQUEzQixDQUF4QjtBQUNBLFNBQUtNLGNBQUwsR0FBc0IsS0FBS0EsY0FBTCxDQUFvQk4sSUFBcEIsQ0FBeUIsSUFBekIsQ0FBdEI7QUFDQSxTQUFLTyxnQkFBTCxHQUF3QixLQUFLQSxnQkFBTCxDQUFzQlAsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBeEI7QUFDQSxTQUFLUSxzQkFBTCxHQUE4QixLQUFLQSxzQkFBTCxDQUE0QlIsSUFBNUIsQ0FBaUMsSUFBakMsQ0FBOUI7QUFDQSxTQUFLUyx1QkFBTCxHQUErQixLQUFLQSx1QkFBTCxDQUE2QlQsSUFBN0IsQ0FBa0MsSUFBbEMsQ0FBL0I7QUFDQSxTQUFLVSxvQkFBTCxHQUE0QixLQUFLQSxvQkFBTCxDQUEwQlYsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFDQSxTQUFLVyxvQkFBTCxHQUE0QixLQUFLQSxvQkFBTCxDQUEwQlgsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFFQSxTQUFLWSxrQkFBTCxHQUEwQix1QkFBMUI7QUFDQSxTQUFLQyxTQUFMLEdBQWlCLHVCQUFqQjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsdUJBQWpCO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQWhCLEVBQUFBLFlBQVksQ0FBQ2lCLFFBQUQsRUFBVztBQUNuQjtBQUNBLFVBQU1DLG1CQUFtQixHQUFHLE1BQU07QUFDOUIsWUFBTUMsdUJBQXVCLEdBQUdDLHVCQUFjQyxRQUFkLENBQXVCLGdCQUF2QixFQUF5Q0osUUFBUSxDQUFDSyxJQUFULENBQWNDLE1BQXZELENBQWhDOztBQUNBLGFBQU8sQ0FBQyxDQUFDSix1QkFBdUIsQ0FBQ0YsUUFBUSxDQUFDcEIsR0FBVCxDQUFhMkIsT0FBZCxDQUFoQztBQUNILEtBSEQ7O0FBS0EsVUFBTUMsZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxXQUFPO0FBQ0hDLE1BQUFBLFlBQVksRUFBRSxJQURYO0FBQ2lCO0FBQ3BCO0FBQ0FDLE1BQUFBLE9BQU8sRUFBRSxLQUFLcEMsS0FBTCxDQUFXcUMsaUJBQVgsSUFBZ0MsQ0FBQ0wsZ0JBQWdCLENBQUNNLFNBQWpCLENBQTJCLEtBQUtuQyxXQUFoQyxDQUh2QztBQUlIb0MsTUFBQUEsU0FBUyxFQUFFLEtBQUtDLGNBQUwsQ0FBb0JoQixRQUFRLENBQUNwQixHQUFULENBQWFxQyxHQUFqQyxDQUpSO0FBS0g7QUFDQTtBQUNBaEIsTUFBQUEsbUJBQW1CLEVBQUVELFFBQVEsQ0FBQ2tCLE1BQVQsS0FBb0JsQixRQUFRLENBQUNtQixhQUE3QixJQUE4Q2xCLG1CQUFtQixFQVBuRjtBQVFIbUIsTUFBQUEsS0FBSyxFQUFFLElBUko7QUFTSEMsTUFBQUEsUUFBUSxFQUFFLEtBVFA7QUFVSEMsTUFBQUEsZUFBZSxFQUFFdEIsUUFBUSxDQUFDc0IsZUFWdkI7QUFXSDVDLE1BQUFBLGFBQWEsRUFBRTtBQVhaLEtBQVA7QUFhSDtBQUVEOzs7Ozs7O0FBS0E2QyxFQUFBQSxjQUFjLENBQUNDLFVBQUQsRUFBYTtBQUN2QixXQUFPQywyQkFBa0JDLG1CQUFsQixDQUFzQyxLQUFLbEQsS0FBTCxDQUFXSSxHQUFYLENBQWVDLEVBQXJELEVBQXlEMkMsVUFBekQsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVBUixFQUFBQSxjQUFjLENBQUNXLFNBQUQsRUFBWTtBQUN0QixVQUFNQyxDQUFDLEdBQUdYLGFBQUlZLEtBQUosQ0FBVUYsU0FBVixDQUFWOztBQUNBLFFBQUksQ0FBQ0MsQ0FBTCxFQUFRO0FBQ0pFLE1BQUFBLE9BQU8sQ0FBQ1YsS0FBUixDQUFjLGdCQUFkLEVBQWdDLGFBQWhDLEVBQStDTyxTQUEvQztBQUNBLGFBQU9WLFlBQVA7QUFDSDs7QUFFRCxVQUFNYyxNQUFNLEdBQUdDLFlBQUdILEtBQUgsQ0FBU0QsQ0FBQyxDQUFDSyxLQUFYLENBQWYsQ0FQc0IsQ0FRdEI7OztBQUNBRixJQUFBQSxNQUFNLENBQUNHLFFBQVAsR0FBa0IsS0FBSzFELEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUFqQyxDQVRzQixDQVV0QjtBQUNBOztBQUNBa0QsSUFBQUEsTUFBTSxDQUFDSSxTQUFQLEdBQW1CQyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQWhCLENBQXFCQyxLQUFyQixDQUEyQixHQUEzQixFQUFnQyxDQUFoQyxFQUFtQyxDQUFuQyxDQUFuQjtBQUNBWCxJQUFBQSxDQUFDLENBQUNZLE1BQUYsR0FBV0MsU0FBWDtBQUNBYixJQUFBQSxDQUFDLENBQUNLLEtBQUYsR0FBVUYsTUFBVjtBQUVBLFdBQU9ILENBQUMsQ0FBQ2MsTUFBRixFQUFQO0FBQ0g7O0FBRURDLEVBQUFBLGNBQWMsR0FBRztBQUNiLFVBQU1DLHFCQUFxQixHQUFHUixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JRLFFBQTlDOztBQUNBLFVBQU1qQixDQUFDLEdBQUdYLGFBQUlZLEtBQUosQ0FBVSxLQUFLckQsS0FBTCxDQUFXSSxHQUFYLENBQWVxQyxHQUF6QixDQUFWOztBQUNBLFVBQU02QixvQkFBb0IsR0FBR2xCLENBQUMsQ0FBQ2lCLFFBQS9COztBQUNBLFFBQUlELHFCQUFxQixLQUFLLFFBQTFCLElBQXNDRSxvQkFBb0IsS0FBSyxRQUFuRSxFQUE2RTtBQUN6RWhCLE1BQUFBLE9BQU8sQ0FBQ2lCLElBQVIsQ0FBYSxxQ0FBYixFQUNBSCxxQkFEQSxFQUN1QkUsb0JBRHZCLEVBQzZDVixNQUFNLENBQUNDLFFBRHBELEVBQzhELEtBQUs3RCxLQUFMLENBQVdJLEdBQVgsQ0FBZXFDLEdBRDdFO0FBRUEsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsV0FBTyxLQUFQO0FBQ0g7O0FBRUQrQixFQUFBQSxpQkFBaUIsR0FBRztBQUNoQjtBQUNBLFFBQUksS0FBS3hFLEtBQUwsQ0FBV3lFLElBQVgsSUFBbUIsS0FBS25FLEtBQUwsQ0FBV21CLG1CQUFsQyxFQUF1RDtBQUNuRCxXQUFLaUQsY0FBTDtBQUNILEtBSmUsQ0FNaEI7OztBQUNBLFNBQUtDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS3JFLFNBQWxCLENBQXJCO0FBQ0g7O0FBRURzRSxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQjtBQUNBLFFBQUksS0FBS0gsYUFBVCxFQUF3QkMsb0JBQUlHLFVBQUosQ0FBZSxLQUFLSixhQUFwQixFQUZMLENBSW5COztBQUNBLFFBQUksQ0FBQzFCLDJCQUFrQitCLG9CQUFsQixDQUF1QyxLQUFLaEYsS0FBTCxDQUFXSSxHQUFYLENBQWVDLEVBQXRELENBQUwsRUFBZ0U7QUFDNUQ0QyxpQ0FBa0JnQyx1QkFBbEIsQ0FBMEMsS0FBS2pGLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUF6RDs7QUFDQSxZQUFNMkIsZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQUYsTUFBQUEsZ0JBQWdCLENBQUNrRCxjQUFqQixDQUFnQyxLQUFLL0UsV0FBckM7QUFDSDtBQUNKO0FBRUQ7Ozs7OztBQUlBdUUsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsUUFBSSxDQUFDUyxxQkFBWUMsV0FBWixDQUF3QixLQUFLcEYsS0FBTCxDQUFXSSxHQUFYLENBQWVxQyxHQUF2QyxDQUFMLEVBQWtEO0FBQzlDYSxNQUFBQSxPQUFPLENBQUNpQixJQUFSLENBQWEsOENBQWIsRUFBNkQ5QixZQUE3RDtBQUNBLFdBQUt4QyxRQUFMLENBQWM7QUFDVjJDLFFBQUFBLEtBQUssRUFBRSxJQURHO0FBRVZMLFFBQUFBLFNBQVMsRUFBRSxLQUFLQyxjQUFMLENBQW9CLEtBQUt4QyxLQUFMLENBQVdJLEdBQVgsQ0FBZXFDLEdBQW5DLENBRkQ7QUFHVk4sUUFBQUEsWUFBWSxFQUFFO0FBSEosT0FBZDtBQUtBO0FBQ0g7O0FBRUQsVUFBTWtELFFBQVEsR0FBR0MseUNBQW9CQyxjQUFwQixFQUFqQjs7QUFDQSxRQUFJLENBQUNGLFFBQVEsQ0FBQ0csVUFBVCxFQUFMLEVBQTRCO0FBQ3hCbEMsTUFBQUEsT0FBTyxDQUFDaUIsSUFBUixDQUFhLG1EQUFiLEVBQWtFOUIsWUFBbEU7QUFDQSxXQUFLeEMsUUFBTCxDQUFjO0FBQ1YyQyxRQUFBQSxLQUFLLEVBQUUsSUFERztBQUVWTCxRQUFBQSxTQUFTLEVBQUUsS0FBS0MsY0FBTCxDQUFvQixLQUFLeEMsS0FBTCxDQUFXSSxHQUFYLENBQWVxQyxHQUFuQyxDQUZEO0FBR1ZOLFFBQUFBLFlBQVksRUFBRTtBQUhKLE9BQWQ7QUFLQTtBQUNILEtBcEJZLENBc0JiOzs7QUFFQSxVQUFNc0QsY0FBYyxHQUFHSixRQUFRLENBQUNLLGlCQUFULEVBQXZCOztBQUNBLFFBQUksQ0FBQ1AscUJBQVlDLFdBQVosQ0FBd0JLLGNBQWMsQ0FBQ0UsTUFBdkMsQ0FBTCxFQUFxRDtBQUNqRHJDLE1BQUFBLE9BQU8sQ0FBQ2lCLElBQVIsQ0FBYSwrQ0FBYixFQUE4RDlCLFlBQTlEO0FBQ0EsV0FBS3hDLFFBQUwsQ0FBYztBQUNWMkMsUUFBQUEsS0FBSyxFQUFFLElBREc7QUFFVkwsUUFBQUEsU0FBUyxFQUFFLEtBQUtDLGNBQUwsQ0FBb0IsS0FBS3hDLEtBQUwsQ0FBV0ksR0FBWCxDQUFlcUMsR0FBbkMsQ0FGRDtBQUdWTixRQUFBQSxZQUFZLEVBQUU7QUFISixPQUFkO0FBS0E7QUFDSCxLQWpDWSxDQW1DYjs7O0FBQ0EsUUFBSSxDQUFDLEtBQUt5RCxhQUFWLEVBQXlCO0FBQ3JCLFdBQUtBLGFBQUwsR0FBcUJILGNBQWMsQ0FBQ0ksZUFBZixFQUFyQjtBQUNIOztBQUNELFNBQUtELGFBQUwsQ0FBbUJFLGNBQW5CLEdBQW9DQyxJQUFwQyxDQUEwQ0MsS0FBRCxJQUFXO0FBQ2hEO0FBQ0EsV0FBS0osYUFBTCxDQUFtQkssV0FBbkIsR0FBaUNELEtBQWpDOztBQUNBLFlBQU01QyxDQUFDLEdBQUdYLGFBQUlZLEtBQUosQ0FBVSxLQUFLYixjQUFMLENBQW9CLEtBQUt4QyxLQUFMLENBQVdJLEdBQVgsQ0FBZXFDLEdBQW5DLENBQVYsQ0FBVjs7QUFDQSxZQUFNYyxNQUFNLEdBQUdDLFlBQUdILEtBQUgsQ0FBU0QsQ0FBQyxDQUFDSyxLQUFYLENBQWY7O0FBQ0EsVUFBSSxDQUFDRixNQUFNLENBQUMyQyxZQUFaLEVBQTBCO0FBQ3RCM0MsUUFBQUEsTUFBTSxDQUFDMkMsWUFBUCxHQUFzQnZHLGtCQUFrQixDQUFDcUcsS0FBRCxDQUF4QyxDQURzQixDQUV0Qjs7QUFDQTVDLFFBQUFBLENBQUMsQ0FBQ1ksTUFBRixHQUFXQyxTQUFYO0FBQ0FiLFFBQUFBLENBQUMsQ0FBQ0ssS0FBRixHQUFVRixNQUFWO0FBQ0g7O0FBRUQsV0FBS3RELFFBQUwsQ0FBYztBQUNWMkMsUUFBQUEsS0FBSyxFQUFFLElBREc7QUFFVkwsUUFBQUEsU0FBUyxFQUFFYSxDQUFDLENBQUNjLE1BQUYsRUFGRDtBQUdWL0IsUUFBQUEsWUFBWSxFQUFFO0FBSEosT0FBZCxFQVpnRCxDQWtCaEQ7O0FBQ0EsVUFBSSxDQUFDLEtBQUs3QixLQUFMLENBQVd3QyxlQUFaLElBQStCUyxNQUFNLENBQUNkLEdBQTFDLEVBQStDO0FBQzNDLGFBQUswRCxpQkFBTCxDQUF1QjVDLE1BQU0sQ0FBQ2QsR0FBOUI7QUFDSDtBQUNKLEtBdEJELEVBc0JJMkQsR0FBRCxJQUFTO0FBQ1I5QyxNQUFBQSxPQUFPLENBQUNWLEtBQVIsQ0FBYyw0QkFBZCxFQUE0Q3dELEdBQTVDO0FBQ0EsV0FBS25HLFFBQUwsQ0FBYztBQUNWMkMsUUFBQUEsS0FBSyxFQUFFd0QsR0FBRyxDQUFDQyxPQUREO0FBRVZsRSxRQUFBQSxZQUFZLEVBQUU7QUFGSixPQUFkO0FBSUgsS0E1QkQ7QUE2QkgsR0F4TWdELENBME1qRDs7O0FBQ0FtRSxFQUFBQSxnQ0FBZ0MsQ0FBQ0MsU0FBRCxFQUFZO0FBQUU7QUFDMUMsUUFBSUEsU0FBUyxDQUFDbkcsR0FBVixDQUFjcUMsR0FBZCxLQUFzQixLQUFLekMsS0FBTCxDQUFXSSxHQUFYLENBQWVxQyxHQUF6QyxFQUE4QztBQUMxQyxXQUFLbEMsWUFBTCxDQUFrQmdHLFNBQWxCLEVBRDBDLENBRTFDOzs7QUFDQSxVQUFJLEtBQUt2RyxLQUFMLENBQVd5RSxJQUFYLElBQW1CLEtBQUtuRSxLQUFMLENBQVdtQixtQkFBbEMsRUFBdUQ7QUFDbkQsYUFBS2lELGNBQUw7QUFDSDtBQUNKLEtBTkQsTUFNTyxJQUFJNkIsU0FBUyxDQUFDOUIsSUFBVixJQUFrQixDQUFDLEtBQUt6RSxLQUFMLENBQVd5RSxJQUFsQyxFQUF3QztBQUMzQztBQUNBLFVBQUksS0FBS3pFLEtBQUwsQ0FBV3FDLGlCQUFYLElBQWdDLENBQUNMLDBCQUFpQk0sU0FBakIsQ0FBMkIsS0FBS25DLFdBQWhDLENBQXJDLEVBQW1GO0FBQy9FLGFBQUtGLFFBQUwsQ0FBYztBQUNWbUMsVUFBQUEsT0FBTyxFQUFFO0FBREMsU0FBZDtBQUdILE9BTjBDLENBTzNDOzs7QUFDQSxVQUFJLEtBQUs5QixLQUFMLENBQVdtQixtQkFBZixFQUFvQztBQUNoQyxhQUFLaUQsY0FBTDtBQUNIO0FBQ0osS0FYTSxNQVdBLElBQUk2QixTQUFTLENBQUN6RCxlQUFWLEtBQThCLEtBQUs5QyxLQUFMLENBQVc4QyxlQUE3QyxFQUE4RDtBQUNqRSxXQUFLN0MsUUFBTCxDQUFjO0FBQ1Y2QyxRQUFBQSxlQUFlLEVBQUV5RCxTQUFTLENBQUN6RDtBQURqQixPQUFkO0FBR0g7QUFDSjs7QUFFRDBELEVBQUFBLGNBQWMsR0FBRztBQUNiO0FBQ0EsUUFBSSxLQUFLeEcsS0FBTCxDQUFXeUcsVUFBWCxJQUF5QkMsaUNBQWdCQyxHQUFoQixHQUFzQkMsV0FBdEIsQ0FBa0NsRSxNQUFsQyxLQUE2QyxLQUFLMUMsS0FBTCxDQUFXMkMsYUFBckYsRUFBb0c7QUFDaEcsYUFBTyxJQUFQO0FBQ0gsS0FKWSxDQUtiOzs7QUFDQSxXQUFPd0MscUJBQVkwQixvQkFBWixDQUFpQyxLQUFLN0csS0FBTCxDQUFXNkIsSUFBWCxDQUFnQkMsTUFBakQsQ0FBUDtBQUNIOztBQUVEbkIsRUFBQUEsWUFBWSxHQUFHO0FBQ1gyQyxJQUFBQSxPQUFPLENBQUN3RCxHQUFSLENBQVksaUJBQVosRUFBK0IsS0FBSzlHLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUE5Qzs7QUFDQSxRQUFJLEtBQUtMLEtBQUwsQ0FBVytHLFdBQWYsRUFBNEI7QUFDeEIsV0FBSy9HLEtBQUwsQ0FBVytHLFdBQVg7QUFDSCxLQUZELE1BRU87QUFDSDtBQUNBLFVBQUlwRix1QkFBY3FGLGdCQUFkLENBQStCLG1DQUEvQixDQUFKLEVBQXlFO0FBQ3JFMUIsaURBQW9CQyxjQUFwQixHQUFxQzBCLE9BQXJDLENBQ0ksS0FBS2pILEtBQUwsQ0FBVzZCLElBRGYsRUFFSSxVQUFVLEtBQUs3QixLQUFMLENBQVdrSCxJQUZ6QixFQUdJLEtBQUtsSCxLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFIbkI7QUFLSCxPQU5ELE1BTU87QUFDSGlGLGlEQUFvQkMsY0FBcEIsR0FBcUNHLGlCQUFyQyxHQUF5RHlCLElBQXpELENBQ0ksS0FBS25ILEtBQUwsQ0FBVzZCLElBRGYsRUFFSSxVQUFVLEtBQUs3QixLQUFMLENBQVdrSCxJQUZ6QixFQUdJLEtBQUtsSCxLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFIbkI7QUFLSDtBQUNKO0FBQ0o7O0FBRURTLEVBQUFBLGdCQUFnQixHQUFHO0FBQ2Z3QyxJQUFBQSxPQUFPLENBQUN3RCxHQUFSLENBQVksNEJBQVo7O0FBQ0E3RCwrQkFBa0JtRSxrQkFBbEIsQ0FBcUMsS0FBS3BILEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUFwRCxFQUF3RGdILGFBQXhELEdBQ0tDLEtBREwsQ0FDWWxCLEdBQUQsSUFBUztBQUNaOUMsTUFBQUEsT0FBTyxDQUFDVixLQUFSLENBQWMsMEJBQWQsRUFBMEN3RCxHQUExQztBQUNILEtBSEwsRUFJS0wsSUFKTCxDQUlXd0IsVUFBRCxJQUFnQjtBQUNsQjNDLDBCQUFJNEMsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRSxrQkFEQztBQUVUQyxRQUFBQSxJQUFJLEVBQUVIO0FBRkcsT0FBYixFQUdHLElBSEg7QUFJSCxLQVRMO0FBVUg7QUFFRDs7Ozs7QUFHQTNHLEVBQUFBLGNBQWMsR0FBRztBQUNiLFFBQUksS0FBS1osS0FBTCxDQUFXMkgsYUFBZixFQUE4QjtBQUMxQixXQUFLM0gsS0FBTCxDQUFXMkgsYUFBWDtBQUNILEtBRkQsTUFFTyxJQUFJLEtBQUtuQixjQUFMLEVBQUosRUFBMkI7QUFDOUI7QUFDQSxZQUFNb0IsY0FBYyxHQUFHM0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQTJGLHFCQUFNQyxtQkFBTixDQUEwQixlQUExQixFQUEyQyxFQUEzQyxFQUErQ0YsY0FBL0MsRUFBK0Q7QUFDM0RHLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxlQUFILENBRG9EO0FBRTNEQyxRQUFBQSxXQUFXLEVBQUUseUJBQ1QsNkRBQ0EsK0NBRlMsQ0FGOEM7QUFLM0RDLFFBQUFBLE1BQU0sRUFBRSx5QkFBRyxlQUFILENBTG1EO0FBTTNEQyxRQUFBQSxVQUFVLEVBQUdDLFNBQUQsSUFBZTtBQUN2QixjQUFJLENBQUNBLFNBQUwsRUFBZ0I7QUFDWjtBQUNIOztBQUNELGVBQUtsSSxRQUFMLENBQWM7QUFBQzRDLFlBQUFBLFFBQVEsRUFBRTtBQUFYLFdBQWQsRUFKdUIsQ0FNdkI7QUFDQTtBQUNBOztBQUNBLGNBQUksS0FBS3ZCLFNBQUwsQ0FBZThHLE9BQW5CLEVBQTRCO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFLOUcsU0FBTCxDQUFlOEcsT0FBZixDQUF1QkMsR0FBdkIsR0FBNkIsYUFBN0I7QUFDSDs7QUFFRGxELCtCQUFZbUQsYUFBWixDQUNJLEtBQUt0SSxLQUFMLENBQVc2QixJQUFYLENBQWdCQyxNQURwQixFQUVJLEtBQUs5QixLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFGbkIsRUFHRWlILEtBSEYsQ0FHU2lCLENBQUQsSUFBTztBQUNYakYsWUFBQUEsT0FBTyxDQUFDVixLQUFSLENBQWMseUJBQWQsRUFBeUMyRixDQUF6QztBQUNBLGtCQUFNQyxXQUFXLEdBQUd2RyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUVBMkYsMkJBQU1DLG1CQUFOLENBQTBCLHlCQUExQixFQUFxRCxFQUFyRCxFQUF5RFUsV0FBekQsRUFBc0U7QUFDbEVULGNBQUFBLEtBQUssRUFBRSx5QkFBRyx5QkFBSCxDQUQyRDtBQUVsRUMsY0FBQUEsV0FBVyxFQUFFLHlCQUFHLG1FQUFIO0FBRnFELGFBQXRFO0FBSUgsV0FYRCxFQVdHUyxPQVhILENBV1csTUFBTTtBQUNiLGlCQUFLeEksUUFBTCxDQUFjO0FBQUM0QyxjQUFBQSxRQUFRLEVBQUU7QUFBWCxhQUFkO0FBQ0gsV0FiRDtBQWNIO0FBdkMwRCxPQUEvRDtBQXlDSDtBQUNKOztBQUVEaEMsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZnlDLElBQUFBLE9BQU8sQ0FBQ29GLElBQVIsQ0FBYSxnQ0FBYixFQUErQyxLQUFLMUksS0FBTCxDQUFXSSxHQUFYLENBQWVDLEVBQTlEOztBQUNBLFNBQUthLHVCQUFMO0FBQ0g7QUFFRDs7Ozs7QUFHQVIsRUFBQUEsU0FBUyxHQUFHO0FBQ1I7QUFDQTtBQUNBO0FBQ0F1QywrQkFBa0IwRixrQkFBbEIsQ0FBcUMsS0FBSzNJLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUFwRDs7QUFDQSxTQUFLdUkscUJBQUw7O0FBRUEzRiwrQkFBa0I0RixTQUFsQixDQUE0QixLQUFLN0ksS0FBTCxDQUFXSSxHQUFYLENBQWVDLEVBQTNDLEVBQStDLEtBQUtMLEtBQUwsQ0FBVzZCLElBQVgsQ0FBZ0JDLE1BQS9EOztBQUNBLFNBQUs3QixRQUFMLENBQWM7QUFBQ21DLE1BQUFBLE9BQU8sRUFBRTtBQUFWLEtBQWQ7QUFDSDs7QUFFRHdHLEVBQUFBLHFCQUFxQixHQUFHO0FBQ3BCO0FBQ0E7QUFDQSxVQUFNRSxlQUFlLEdBQUcsSUFBSUMsd0JBQUosQ0FDcEIsS0FBSy9JLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQURLLEVBQ0QsS0FBSzJJLGVBQUwsRUFEQyxFQUN1QixLQUFLaEosS0FBTCxDQUFXeUcsVUFEbEMsRUFDOEMsS0FBS25GLFNBQUwsQ0FBZThHLE9BQWYsQ0FBdUJhLGFBRHJFLENBQXhCOztBQUVBaEcsK0JBQWtCaUcsa0JBQWxCLENBQXFDLEtBQUtsSixLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFBcEQsRUFBd0R5SSxlQUF4RDs7QUFDQUEsSUFBQUEsZUFBZSxDQUFDSyxlQUFoQixHQUFrQ3BELElBQWxDLENBQXdDcUQscUJBQUQsSUFBMkI7QUFDOUQ5RixNQUFBQSxPQUFPLENBQUN3RCxHQUFSLENBQVksaUJBQVUsS0FBSzlHLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUF6QixpQ0FBeUQrSSxxQkFBckU7QUFDQUEsTUFBQUEscUJBQXFCLEdBQUdBLHFCQUFxQixJQUFJLEVBQWpELENBRjhELENBSTlEOztBQUNBLFVBQUlDLDRCQUE0QixHQUFHLEVBQW5DOztBQUVBLFVBQUksS0FBS3JKLEtBQUwsQ0FBV3NKLHFCQUFYLElBQW9DLEtBQUt0SixLQUFMLENBQVdzSixxQkFBWCxDQUFpQ0MsTUFBakMsR0FBMEMsQ0FBbEYsRUFBcUY7QUFDakZGLFFBQUFBLDRCQUE0QixHQUFHRCxxQkFBcUIsQ0FBQ0ksTUFBdEIsQ0FBNkIsVUFBU2pCLENBQVQsRUFBWTtBQUNwRSxpQkFBTyxLQUFLa0IsT0FBTCxDQUFhbEIsQ0FBYixLQUFpQixDQUF4QjtBQUNILFNBRjhCLEVBRTVCLEtBQUt2SSxLQUFMLENBQVdzSixxQkFGaUIsQ0FBL0I7O0FBSUEsWUFBSUQsNEJBQTRCLENBQUNFLE1BQTdCLEdBQXNDLENBQTFDLEVBQThDO0FBQzFDakcsVUFBQUEsT0FBTyxDQUFDd0QsR0FBUixDQUFZLGlCQUFVLEtBQUs5RyxLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFBekIscURBQ1JnSiw0QkFESjtBQUdIO0FBQ0osT0FqQjZELENBbUI5RDs7O0FBRUFwRyxpQ0FBa0J5RyxxQkFBbEIsQ0FBd0MsS0FBSzFKLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQyxFQUF2RCxFQUEyRGdKLDRCQUEzRDs7QUFFQSxVQUFJLEtBQUtySixLQUFMLENBQVcySixtQkFBZixFQUFvQztBQUNoQyxhQUFLM0osS0FBTCxDQUFXMkosbUJBQVgsQ0FBK0JQLHFCQUEvQjtBQUNILE9BekI2RCxDQTJCOUQ7QUFDQTs7O0FBQ0EsVUFBSSxLQUFLcEosS0FBTCxDQUFXSSxHQUFYLENBQWU4RyxJQUFmLEtBQXdCLE9BQTVCLEVBQXFDO0FBQ2pDNEIsUUFBQUEsZUFBZSxDQUFDYyxtQkFBaEI7QUFDSDtBQUNKLEtBaENELEVBZ0NHdEMsS0FoQ0gsQ0FnQ1VsQixHQUFELElBQVM7QUFDZDlDLE1BQUFBLE9BQU8sQ0FBQ3dELEdBQVIsc0RBQTBELEtBQUs5RyxLQUFMLENBQVdJLEdBQVgsQ0FBZThHLElBQXpFLEdBQWlGLEtBQUtsSCxLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFBaEcsRUFBb0crRixHQUFwRztBQUNILEtBbENEO0FBbUNIOztBQUVENUYsRUFBQUEsU0FBUyxDQUFDcUosT0FBRCxFQUFVO0FBQ2YsUUFBSUEsT0FBTyxDQUFDbkcsUUFBUixLQUFxQixLQUFLMUQsS0FBTCxDQUFXSSxHQUFYLENBQWVDLEVBQXhDLEVBQTRDO0FBQ3hDLGNBQVF3SixPQUFPLENBQUNwQyxNQUFoQjtBQUNJLGFBQUssV0FBTDtBQUNBLGNBQUksS0FBSzFFLGNBQUwsQ0FBb0IsV0FBcEIsQ0FBSixFQUFzQztBQUNsQzZCLGdDQUFJNEMsUUFBSixDQUFhO0FBQUNDLGNBQUFBLE1BQU0sRUFBRSxzQkFBVDtBQUFpQ3FDLGNBQUFBLElBQUksRUFBRUQsT0FBTyxDQUFDQztBQUEvQyxhQUFiO0FBQ0gsV0FGRCxNQUVPO0FBQ0h4RyxZQUFBQSxPQUFPLENBQUNpQixJQUFSLENBQWEsOENBQWI7QUFDSDs7QUFDRDtBQVBKO0FBU0g7QUFDSjtBQUVEOzs7Ozs7QUFJQTRCLEVBQUFBLGlCQUFpQixDQUFDMUQsR0FBRCxFQUFNO0FBQ25CLFNBQUttRCxhQUFMLENBQW1CbUUsa0JBQW5CLENBQXNDdEgsR0FBdEMsRUFBMkNzRCxJQUEzQyxDQUFpRGpELGVBQUQsSUFBcUI7QUFDakUsVUFBSUEsZUFBSixFQUFxQjtBQUNqQixhQUFLN0MsUUFBTCxDQUFjO0FBQUM2QyxVQUFBQSxlQUFlLEVBQUVBO0FBQWxCLFNBQWQ7QUFDSDtBQUNKLEtBSkQsRUFJSXNELEdBQUQsSUFBUTtBQUNQOUMsTUFBQUEsT0FBTyxDQUFDVixLQUFSLENBQWMsMEJBQWQsRUFBMEN3RCxHQUExQztBQUNILEtBTkQ7QUFPSDs7QUFFRG5GLEVBQUFBLHNCQUFzQixHQUFHO0FBQ3JCLFVBQU1hLE1BQU0sR0FBRyxLQUFLOUIsS0FBTCxDQUFXNkIsSUFBWCxDQUFnQkMsTUFBL0I7QUFDQXdCLElBQUFBLE9BQU8sQ0FBQ29GLElBQVIsQ0FBYSw2Q0FBNkMsS0FBSzFJLEtBQUwsQ0FBV0ksR0FBWCxDQUFlMkIsT0FBekU7O0FBQ0EsVUFBTXFHLE9BQU8sR0FBR3pHLHVCQUFjQyxRQUFkLENBQXVCLGdCQUF2QixFQUF5Q0UsTUFBekMsQ0FBaEI7O0FBQ0FzRyxJQUFBQSxPQUFPLENBQUMsS0FBS3BJLEtBQUwsQ0FBV0ksR0FBWCxDQUFlMkIsT0FBaEIsQ0FBUCxHQUFrQyxJQUFsQzs7QUFDQUosMkJBQWNxSSxRQUFkLENBQXVCLGdCQUF2QixFQUF5Q2xJLE1BQXpDLEVBQWlEbUksNEJBQWFDLFlBQTlELEVBQTRFOUIsT0FBNUUsRUFBcUZyQyxJQUFyRixDQUEwRixNQUFNO0FBQzVGLFdBQUs5RixRQUFMLENBQWM7QUFBQ3dCLFFBQUFBLG1CQUFtQixFQUFFO0FBQXRCLE9BQWQsRUFENEYsQ0FHNUY7O0FBQ0EsV0FBS2lELGNBQUw7QUFDSCxLQUxELEVBS0c0QyxLQUxILENBS1NsQixHQUFHLElBQUk7QUFDWjlDLE1BQUFBLE9BQU8sQ0FBQ1YsS0FBUixDQUFjd0QsR0FBZCxFQURZLENBRVo7QUFDSCxLQVJEO0FBU0g7O0FBRURsRixFQUFBQSx1QkFBdUIsR0FBRztBQUN0QixVQUFNWSxNQUFNLEdBQUcsS0FBSzlCLEtBQUwsQ0FBVzZCLElBQVgsQ0FBZ0JDLE1BQS9CO0FBQ0F3QixJQUFBQSxPQUFPLENBQUNvRixJQUFSLENBQWEsNkNBQTZDLEtBQUsxSSxLQUFMLENBQVdJLEdBQVgsQ0FBZTJCLE9BQXpFOztBQUNBLFVBQU1xRyxPQUFPLEdBQUd6Ryx1QkFBY0MsUUFBZCxDQUF1QixnQkFBdkIsRUFBeUNFLE1BQXpDLENBQWhCOztBQUNBc0csSUFBQUEsT0FBTyxDQUFDLEtBQUtwSSxLQUFMLENBQVdJLEdBQVgsQ0FBZTJCLE9BQWhCLENBQVAsR0FBa0MsS0FBbEM7O0FBQ0FKLDJCQUFjcUksUUFBZCxDQUF1QixnQkFBdkIsRUFBeUNsSSxNQUF6QyxFQUFpRG1JLDRCQUFhQyxZQUE5RCxFQUE0RTlCLE9BQTVFLEVBQXFGckMsSUFBckYsQ0FBMEYsTUFBTTtBQUM1RixXQUFLOUYsUUFBTCxDQUFjO0FBQUN3QixRQUFBQSxtQkFBbUIsRUFBRTtBQUF0QixPQUFkLEVBRDRGLENBRzVGOztBQUNBd0IsaUNBQWtCZ0MsdUJBQWxCLENBQTBDLEtBQUtqRixLQUFMLENBQVdJLEdBQVgsQ0FBZUMsRUFBekQ7O0FBQ0EsWUFBTTJCLGdCQUFnQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0FGLE1BQUFBLGdCQUFnQixDQUFDa0QsY0FBakIsQ0FBZ0MsS0FBSy9FLFdBQXJDO0FBQ0gsS0FQRCxFQU9HbUgsS0FQSCxDQU9TbEIsR0FBRyxJQUFJO0FBQ1o5QyxNQUFBQSxPQUFPLENBQUNWLEtBQVIsQ0FBY3dELEdBQWQsRUFEWSxDQUVaO0FBQ0gsS0FWRDtBQVdIOztBQUVEK0QsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsUUFBSUMsV0FBVyxHQUFHLFNBQWxCOztBQUNBLFFBQUksS0FBS3BLLEtBQUwsQ0FBV0ksR0FBWCxDQUFlaUssSUFBZixJQUF1QixLQUFLckssS0FBTCxDQUFXSSxHQUFYLENBQWVpSyxJQUFmLENBQW9CQyxJQUFwQixFQUEzQixFQUF1RDtBQUNuREYsTUFBQUEsV0FBVyxHQUFHLEtBQUtwSyxLQUFMLENBQVdJLEdBQVgsQ0FBZWlLLElBQWYsQ0FBb0JDLElBQXBCLEVBQWQ7QUFDSDs7QUFDRCxXQUFPRixXQUFQO0FBQ0g7O0FBRURySixFQUFBQSxjQUFjLENBQUN3SixFQUFELEVBQUs7QUFDZkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFILEdBRGUsQ0FHZjs7QUFDQSxRQUFJRCxFQUFFLENBQUNFLE1BQUgsS0FBYyxLQUFLbEosU0FBTCxDQUFlNkcsT0FBakMsRUFBMEM7QUFDdEM7QUFDSCxLQU5jLENBUWY7OztBQUNBLFFBQUksS0FBS3BJLEtBQUwsQ0FBV3lHLFVBQWYsRUFBMkI7QUFDdkIsV0FBS3pGLGdCQUFMO0FBQ0gsS0FGRCxNQUVPO0FBQ0g0RCwwQkFBSTRDLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUaEQsUUFBQUEsSUFBSSxFQUFFLENBQUMsS0FBS3pFLEtBQUwsQ0FBV3lFO0FBRlQsT0FBYjtBQUlIO0FBQ0o7QUFFRDs7Ozs7Ozs7O0FBT0FpRyxFQUFBQSxhQUFhLENBQUN0SCxDQUFELEVBQUk7QUFDYixVQUFNdUgsUUFBUSxHQUFHakUsaUNBQWdCQyxHQUFoQixHQUFzQkMsV0FBdEIsQ0FBa0NsRSxNQUFuRDs7QUFDQSxVQUFNa0ksTUFBTSxHQUFHbEUsaUNBQWdCQyxHQUFoQixHQUFzQmtFLE9BQXRCLENBQThCRixRQUE5QixDQUFmOztBQUNBLFVBQU1HLElBQUksR0FBR3RMLE1BQU0sQ0FBQ3VMLE1BQVAsQ0FBYztBQUN2QkMsTUFBQUEsTUFBTSxFQUFFLGVBRGUsQ0FDRTs7QUFERixLQUFkLEVBRVYsS0FBS2hMLEtBQUwsQ0FBV0ksR0FBWCxDQUFlMEosSUFGTCxFQUVXO0FBQ3BCLHdCQUFrQmEsUUFERTtBQUVwQix3QkFBa0IsS0FBSzNLLEtBQUwsQ0FBVzZCLElBQVgsQ0FBZ0JDLE1BRmQ7QUFHcEIsNkJBQXVCOEksTUFBTSxHQUFHQSxNQUFNLENBQUNLLFdBQVYsR0FBd0JOLFFBSGpDO0FBSXBCLDJCQUFxQkMsTUFBTSxHQUFHbEUsaUNBQWdCQyxHQUFoQixHQUFzQnVFLFlBQXRCLENBQW1DTixNQUFNLENBQUNPLFNBQTFDLENBQUgsR0FBMEQsRUFKakU7QUFNcEI7QUFDQSxlQUFTeEosdUJBQWNDLFFBQWQsQ0FBdUIsT0FBdkI7QUFQVyxLQUZYLENBQWI7O0FBWUEsUUFBSWtKLElBQUksQ0FBQ00sWUFBTCxLQUFzQm5ILFNBQTFCLEVBQXFDO0FBQ2pDO0FBQ0EsWUFBTW9ILFNBQVMsR0FBRyxJQUFJQyxHQUFKLENBQVEsS0FBS3RMLEtBQUwsQ0FBV0ksR0FBWCxDQUFlcUMsR0FBdkIsQ0FBbEI7QUFDQXFJLE1BQUFBLElBQUksQ0FBQ00sWUFBTCxHQUFvQkMsU0FBUyxDQUFDRSxZQUFWLENBQXVCNUUsR0FBdkIsQ0FBMkIsUUFBM0IsQ0FBcEI7QUFDSDs7QUFFRCxXQUFPekgsZUFBZSxDQUFDa0UsQ0FBRCxFQUFJMEgsSUFBSixDQUF0QjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7QUFRQTlCLEVBQUFBLGVBQWUsR0FBRztBQUNkLFFBQUl2RyxHQUFKOztBQUVBLFFBQUksS0FBS3pDLEtBQUwsQ0FBV0ksR0FBWCxDQUFlOEcsSUFBZixLQUF3QixPQUE1QixFQUFxQztBQUNqQzVELE1BQUFBLE9BQU8sQ0FBQ3dELEdBQVIsQ0FBWSwrQ0FBWjtBQUNBckUsTUFBQUEsR0FBRyxHQUFHMEMscUJBQVlxRyx1QkFBWixDQUFvQztBQUFDQyxRQUFBQSxjQUFjLEVBQUU7QUFBakIsT0FBcEMsQ0FBTjtBQUNBaEosTUFBQUEsR0FBRyxHQUFHLEtBQUtELGNBQUwsQ0FBb0JDLEdBQXBCLENBQU47QUFDSCxLQUpELE1BSU87QUFDSEEsTUFBQUEsR0FBRyxHQUFHLEtBQUtpSixXQUFMLENBQWlCLEtBQUtwTCxLQUFMLENBQVdpQyxTQUE1QixDQUFOO0FBQ0g7O0FBQ0QsV0FBTyxLQUFLbUksYUFBTCxDQUFtQmpJLEdBQW5CLENBQVA7QUFDSDs7QUFFRGtKLEVBQUFBLGFBQWEsR0FBRztBQUNaLFFBQUksS0FBSzNMLEtBQUwsQ0FBV0ksR0FBWCxDQUFlOEcsSUFBZixLQUF3QixPQUE1QixFQUFxQztBQUNqQyxhQUFPLEtBQUt3RCxhQUFMLENBQ0h2RixxQkFBWXFHLHVCQUFaLENBQW9DO0FBQUNDLFFBQUFBLGNBQWMsRUFBRTtBQUFqQixPQUFwQyxDQURHLENBQVA7QUFHSCxLQUpELE1BSU87QUFDSDtBQUNBO0FBQ0EsYUFBTyxLQUFLZixhQUFMLENBQW1CLEtBQUtnQixXQUFMLENBQWlCLEtBQUsxTCxLQUFMLENBQVdJLEdBQVgsQ0FBZXFDLEdBQWhDLENBQW5CLENBQVA7QUFDSDtBQUNKOztBQUVEaUosRUFBQUEsV0FBVyxDQUFDdEksQ0FBRCxFQUFJO0FBQ1gsVUFBTXdJLGVBQWUsR0FBR25KLGFBQUlZLEtBQUosQ0FBVUQsQ0FBVixFQUFhLElBQWIsQ0FBeEI7O0FBQ0EsUUFBSW5FLGlCQUFKLEVBQXVCO0FBQ25CMk0sTUFBQUEsZUFBZSxDQUFDNUgsTUFBaEIsR0FBeUIsSUFBekI7QUFDQTRILE1BQUFBLGVBQWUsQ0FBQ25JLEtBQWhCLENBQXNCb0ksVUFBdEIsR0FBbUMsSUFBbkM7QUFDSDs7QUFDRCxRQUFJQyxhQUFhLEdBQUcsRUFBcEI7O0FBQ0EsUUFBSTlNLHVCQUF1QixDQUFDK00sUUFBeEIsQ0FBaUNILGVBQWUsQ0FBQ3ZILFFBQWpELENBQUosRUFBZ0U7QUFDNUR5SCxNQUFBQSxhQUFhLEdBQUdySixhQUFJeUIsTUFBSixDQUFXMEgsZUFBWCxDQUFoQjtBQUNIOztBQUNELFdBQU9FLGFBQVA7QUFDSDs7QUFFREUsRUFBQUEsYUFBYSxHQUFHO0FBQ1osVUFBTTNCLElBQUksR0FBRyxLQUFLRixpQkFBTCxFQUFiOztBQUNBLFVBQU04QixXQUFXLEdBQUcsdURBQXBCOztBQUNBLFFBQUlsRSxLQUFLLEdBQUcsRUFBWjs7QUFDQSxRQUFJLEtBQUt6SCxLQUFMLENBQVd3QyxlQUFYLElBQThCLEtBQUt4QyxLQUFMLENBQVd3QyxlQUFYLElBQThCLEtBQUtxSCxpQkFBTCxFQUFoRSxFQUEwRjtBQUN0RnBDLE1BQUFBLEtBQUssR0FBRyxLQUFLekgsS0FBTCxDQUFXd0MsZUFBbkI7QUFDSDs7QUFFRCxXQUNJLDJDQUNJLHdDQUFLdUgsSUFBTCxDQURKLEVBRUksMkNBQVF0QyxLQUFLLEdBQUdrRSxXQUFILEdBQWlCLEVBQTlCLEVBQW9DbEUsS0FBcEMsQ0FGSixDQURKO0FBTUg7O0FBRUQvRyxFQUFBQSxnQkFBZ0IsQ0FBQ3VILENBQUQsRUFBSTtBQUNoQixRQUFJLEtBQUt2SSxLQUFMLENBQVdrTSxlQUFmLEVBQWdDO0FBQzVCLFdBQUtsTSxLQUFMLENBQVdrTSxlQUFYO0FBQ0g7QUFDSjs7QUFFRC9LLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CO0FBQ0E7QUFDQTNCLElBQUFBLE1BQU0sQ0FBQ3VMLE1BQVAsQ0FBY29CLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixHQUF2QixDQUFkLEVBQ0k7QUFBRTNCLE1BQUFBLE1BQU0sRUFBRSxRQUFWO0FBQW9CM0csTUFBQUEsSUFBSSxFQUFFLEtBQUs2SCxhQUFMLEVBQTFCO0FBQWdEVSxNQUFBQSxHQUFHLEVBQUU7QUFBckQsS0FESixFQUNpRkMsS0FEakY7QUFFSDs7QUFFRGxMLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CO0FBQ0EsU0FBS0UsU0FBTCxDQUFlOEcsT0FBZixDQUF1QkMsR0FBdkIsR0FBNkIsS0FBSy9HLFNBQUwsQ0FBZThHLE9BQWYsQ0FBdUJDLEdBQXBEO0FBQ0g7O0FBVURrRSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJQyxXQUFKLENBREssQ0FHTDs7QUFDQSxRQUFJLEtBQUtsTSxLQUFMLENBQVd1QyxRQUFmLEVBQXlCO0FBQ3JCLGFBQU8seUNBQVA7QUFDSCxLQU5JLENBUUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBTTRKLFlBQVksR0FBRyw2REFDakIsb0RBREosQ0FiSyxDQWdCTDtBQUNBOztBQUNBLFVBQU1DLGNBQWMsR0FBRyxnREFBdkI7QUFFQSxVQUFNQyxnQkFBZ0IsR0FBRyxvQkFBb0IsS0FBSzNNLEtBQUwsQ0FBVzRNLFFBQVgsR0FBc0IsU0FBdEIsR0FBa0MsR0FBdEQsQ0FBekI7O0FBRUEsUUFBSSxLQUFLNU0sS0FBTCxDQUFXeUUsSUFBZixFQUFxQjtBQUNqQixZQUFNb0ksY0FBYyxHQUNoQjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyx1QkFBRDtBQUFnQixRQUFBLEdBQUcsRUFBQztBQUFwQixRQURKLENBREo7O0FBS0EsVUFBSSxDQUFDLEtBQUt2TSxLQUFMLENBQVdtQixtQkFBaEIsRUFBcUM7QUFDakMsY0FBTXFMLFdBQVcsR0FBR3BHLGlDQUFnQkMsR0FBaEIsR0FBc0JvRyxlQUF0QixDQUFzQyxLQUFLL00sS0FBTCxDQUFXNkIsSUFBWCxDQUFnQkMsTUFBdEQsQ0FBcEI7O0FBQ0EwSyxRQUFBQSxXQUFXLEdBQ1A7QUFBSyxVQUFBLFNBQVMsRUFBRUc7QUFBaEIsV0FDSSw2QkFBQyxzQkFBRDtBQUNJLFVBQUEsTUFBTSxFQUFFLEtBQUszTSxLQUFMLENBQVc2QixJQUFYLENBQWdCQyxNQUQ1QjtBQUVJLFVBQUEsYUFBYSxFQUFFLEtBQUs5QixLQUFMLENBQVcyQyxhQUY5QjtBQUdJLFVBQUEsR0FBRyxFQUFFLEtBQUtyQyxLQUFMLENBQVdpQyxTQUhwQjtBQUlJLFVBQUEsZUFBZSxFQUFFdUssV0FKckI7QUFLSSxVQUFBLG1CQUFtQixFQUFFLEtBQUs3TDtBQUw5QixVQURKLENBREo7QUFXSCxPQWJELE1BYU8sSUFBSSxLQUFLWCxLQUFMLENBQVc2QixZQUFmLEVBQTZCO0FBQ2hDcUssUUFBQUEsV0FBVyxHQUNQO0FBQUssVUFBQSxTQUFTLEVBQUVHLGdCQUFnQixJQUFJLEtBQUtyTSxLQUFMLENBQVc4QixPQUFYLEdBQXFCLGVBQXJCLEdBQXVDLEVBQTNDO0FBQWhDLFdBQ015SyxjQUROLENBREo7QUFLSCxPQU5NLE1BTUE7QUFDSCxZQUFJLEtBQUsxSSxjQUFMLEVBQUosRUFBMkI7QUFDdkJxSSxVQUFBQSxXQUFXLEdBQ1A7QUFBSyxZQUFBLFNBQVMsRUFBRUc7QUFBaEIsYUFDSSw2QkFBQyxtQkFBRDtBQUFZLFlBQUEsUUFBUSxFQUFDO0FBQXJCLFlBREosQ0FESjtBQUtILFNBTkQsTUFNTztBQUNISCxVQUFBQSxXQUFXLEdBQ1A7QUFBSyxZQUFBLFNBQVMsRUFBRUcsZ0JBQWdCLElBQUksS0FBS3JNLEtBQUwsQ0FBVzhCLE9BQVgsR0FBcUIsZUFBckIsR0FBdUMsRUFBM0M7QUFBaEMsYUFDTSxLQUFLOUIsS0FBTCxDQUFXOEIsT0FBWCxJQUFzQnlLLGNBRDVCLEVBRUk7QUFDSSxZQUFBLEtBQUssRUFBRUgsY0FEWDtBQUVJLFlBQUEsR0FBRyxFQUFFLEtBQUtwTCxTQUZkO0FBR0ksWUFBQSxHQUFHLEVBQUUsS0FBSzBILGVBQUwsRUFIVDtBQUlJLFlBQUEsZUFBZSxFQUFFLElBSnJCO0FBS0ksWUFBQSxPQUFPLEVBQUV5RCxZQUxiO0FBTUksWUFBQSxNQUFNLEVBQUUsS0FBSy9MO0FBTmpCLFlBRkosQ0FESixDQURHLENBYUg7QUFDQTtBQUNBOztBQUNBLGNBQUksS0FBS1YsS0FBTCxDQUFXc0oscUJBQVgsQ0FBaUN5QyxRQUFqQyxDQUEwQyxvQkFBMUMsQ0FBSixFQUFxRTtBQUNqRSxrQkFBTS9KLGdCQUFnQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCLENBRGlFLENBRWpFO0FBQ0E7O0FBQ0FzSyxZQUFBQSxXQUFXLEdBQUc7QUFBSyxjQUFBLFNBQVMsRUFBQztBQUFmLGVBQ1YsNkJBQUMsZ0JBQUQ7QUFBa0IsY0FBQSxVQUFVLEVBQUUsS0FBS3JNO0FBQW5DLGVBQ0txTSxXQURMLENBRFUsQ0FBZDtBQUtIO0FBQ0o7QUFDSjtBQUNKOztBQUVELFVBQU1RLGtCQUFrQixHQUFHLEtBQUtoTixLQUFMLENBQVdpTixZQUFYLElBQTJCLEtBQUtqTixLQUFMLENBQVd5RSxJQUFqRTtBQUNBLFVBQU15SSxrQkFBa0IsR0FBRyxLQUFLbE4sS0FBTCxDQUFXaU4sWUFBWCxJQUEyQixDQUFDLEtBQUtqTixLQUFMLENBQVd5RSxJQUFsRTtBQUVBLFFBQUkwSSxZQUFKOztBQUNBLFFBQUksS0FBS25OLEtBQUwsQ0FBVzRNLFFBQWYsRUFBeUI7QUFDckJPLE1BQUFBLFlBQVksR0FBRyxpQkFBZjtBQUNILEtBRkQsTUFFTyxJQUFJLEtBQUtuTixLQUFMLENBQVdvTixTQUFmLEVBQTBCO0FBQzdCRCxNQUFBQSxZQUFZLEdBQUcscUJBQWY7QUFDSCxLQUZNLE1BRUE7QUFDSEEsTUFBQUEsWUFBWSxHQUFHLFlBQWY7QUFDSDs7QUFFRCxVQUFNRSxjQUFjLEdBQUcseUJBQVc7QUFDOUJDLE1BQUFBLGlCQUFpQixFQUFFLElBRFc7QUFFOUJDLE1BQUFBLDBCQUEwQixFQUFFLEtBQUt2TixLQUFMLENBQVd5RTtBQUZULEtBQVgsQ0FBdkI7QUFLQSxRQUFJK0ksV0FBSjs7QUFDQSxRQUFJLEtBQUtsTixLQUFMLENBQVdKLGFBQWYsRUFBOEI7QUFDMUIsWUFBTXVOLFdBQVcsR0FBRyxLQUFLcE0sa0JBQUwsQ0FBd0IrRyxPQUF4QixDQUFnQ3NGLHFCQUFoQyxFQUFwQjs7QUFFQSxZQUFNQyxhQUFhLEdBQUcsS0FBS25ILGNBQUwsRUFBdEI7O0FBQ0EsWUFBTW9ILGNBQWMsR0FBR0MsT0FBTyxDQUFDLEtBQUtqSSxhQUFMLElBQXNCK0gsYUFBdkIsQ0FBOUI7QUFDQSxZQUFNRyxnQkFBZ0IsR0FBRyxDQUFDLEtBQUs5TixLQUFMLENBQVcrTixVQUFYLEtBQTBCOUosU0FBMUIsSUFBdUMsS0FBS2pFLEtBQUwsQ0FBVytOLFVBQW5ELEtBQWtFSixhQUEzRjtBQUNBLFlBQU1LLHlCQUF5QixHQUFHLEtBQUtqTCxjQUFMLENBQW9CLHlCQUFwQixLQUFrRCxLQUFLL0MsS0FBTCxDQUFXeUUsSUFBL0Y7QUFFQSxZQUFNd0osaUJBQWlCLEdBQUdoTSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUNBQWpCLENBQTFCO0FBQ0FzTCxNQUFBQSxXQUFXLEdBQ1AsNkJBQUMsd0JBQUQsNkJBQWlCLDhCQUFZQyxXQUFaLEVBQXlCLElBQXpCLENBQWpCO0FBQWlELFFBQUEsVUFBVSxFQUFFLEtBQUtTO0FBQWxFLFVBQ0ksNkJBQUMsaUJBQUQ7QUFDSSxRQUFBLGVBQWUsRUFBRSxLQUFLck4sZ0JBRDFCO0FBRUksUUFBQSxhQUFhLEVBQUUrTSxjQUFjLEdBQUcsS0FBS2pOLFlBQVIsR0FBdUJzRCxTQUZ4RDtBQUdJLFFBQUEsZUFBZSxFQUFFNkosZ0JBQWdCLEdBQUcsS0FBS2xOLGNBQVIsR0FBeUJxRCxTQUg5RDtBQUlJLFFBQUEsaUJBQWlCLEVBQUUrSix5QkFBeUIsR0FBRyxLQUFLbE4sZ0JBQVIsR0FBMkJtRCxTQUozRTtBQUtJLFFBQUEsZUFBZSxFQUFFLEtBQUtqRSxLQUFMLENBQVdtTyxVQUFYLEdBQXdCLEtBQUsvTSxvQkFBN0IsR0FBb0Q2QyxTQUx6RTtBQU1JLFFBQUEsVUFBVSxFQUFFLEtBQUtpSztBQU5yQixRQURKLENBREo7QUFZSDs7QUFFRCxXQUFPLDZCQUFDLGNBQUQsQ0FBTyxRQUFQLFFBQ0g7QUFBSyxNQUFBLFNBQVMsRUFBRWYsWUFBaEI7QUFBOEIsTUFBQSxFQUFFLEVBQUUsS0FBS25OLEtBQUwsQ0FBV0ksR0FBWCxDQUFlQztBQUFqRCxPQUNNLEtBQUtMLEtBQUwsQ0FBV29PLFdBQVgsSUFDRjtBQUFLLE1BQUEsR0FBRyxFQUFFLEtBQUs3TSxTQUFmO0FBQTBCLE1BQUEsU0FBUyxFQUFFOEwsY0FBckM7QUFBcUQsTUFBQSxPQUFPLEVBQUUsS0FBS3RNO0FBQW5FLE9BQ0k7QUFBTSxNQUFBLFNBQVMsRUFBQyx3QkFBaEI7QUFBeUMsTUFBQSxLQUFLLEVBQUU7QUFBQ3NOLFFBQUFBLGFBQWEsRUFBRyxLQUFLck8sS0FBTCxDQUFXc08sMkJBQVgsR0FBeUMsS0FBekMsR0FBaUQ7QUFBbEU7QUFBaEQsT0FFTXRCLGtCQUFrQixJQUFJLDZCQUFDLHlCQUFEO0FBQ3BCLE1BQUEsU0FBUyxFQUFDLG9FQURVO0FBRXBCLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FGYTtBQUdwQixNQUFBLE9BQU8sRUFBRSxLQUFLaE07QUFITSxNQUY1QixFQVFNa00sa0JBQWtCLElBQUksNkJBQUMseUJBQUQ7QUFDcEIsTUFBQSxTQUFTLEVBQUMsb0VBRFU7QUFFcEIsTUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSCxDQUZhO0FBR3BCLE1BQUEsT0FBTyxFQUFFLEtBQUtsTTtBQUhNLE1BUjVCLEVBY00sS0FBS2hCLEtBQUwsQ0FBV3VPLFNBQVgsSUFBd0IsS0FBS3ZDLGFBQUwsRUFkOUIsQ0FESixFQWlCSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BRU0sS0FBS2hNLEtBQUwsQ0FBV3dPLFVBQVgsSUFBeUIsNkJBQUMseUJBQUQ7QUFDdkIsTUFBQSxTQUFTLEVBQUMsa0VBRGE7QUFFdkIsTUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSCxDQUZnQjtBQUd2QixNQUFBLE9BQU8sRUFBRSxLQUFLck47QUFIUyxNQUYvQixFQVFNLDZCQUFDLDhCQUFEO0FBQ0UsTUFBQSxTQUFTLEVBQUMsZ0VBRFo7QUFFRSxNQUFBLEtBQUssRUFBRSx5QkFBRyxjQUFILENBRlQ7QUFHRSxNQUFBLFVBQVUsRUFBRSxLQUFLYixLQUFMLENBQVdKLGFBSHpCO0FBSUUsTUFBQSxRQUFRLEVBQUUsS0FBS21CLGtCQUpqQjtBQUtFLE1BQUEsT0FBTyxFQUFFLEtBQUtvTjtBQUxoQixNQVJOLENBakJKLENBRkosRUFvQ01qQyxXQXBDTixDQURHLEVBd0NEZ0IsV0F4Q0MsQ0FBUDtBQTBDSDs7QUEzdkJnRDs7O0FBOHZCckQ1TixPQUFPLENBQUNxTCxXQUFSLEdBQXNCLFNBQXRCO0FBRUFyTCxPQUFPLENBQUM4TyxTQUFSLEdBQW9CO0FBQ2hCdE8sRUFBQUEsR0FBRyxFQUFFdU8sbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRE47QUFFaEJoTixFQUFBQSxJQUFJLEVBQUU4TSxtQkFBVUMsTUFBVixDQUFpQkMsVUFGUDtBQUdoQjtBQUNBO0FBQ0F6QixFQUFBQSxTQUFTLEVBQUV1QixtQkFBVUcsSUFMTDtBQU1oQjtBQUNBbEMsRUFBQUEsUUFBUSxFQUFFK0IsbUJBQVVHLElBUEo7QUFRaEI7QUFDQXBNLEVBQUFBLE1BQU0sRUFBRWlNLG1CQUFVSSxNQUFWLENBQWlCRixVQVRUO0FBVWhCO0FBQ0FsTSxFQUFBQSxhQUFhLEVBQUVnTSxtQkFBVUksTUFYVDtBQVloQjFNLEVBQUFBLGlCQUFpQixFQUFFc00sbUJBQVVHLElBWmI7QUFhaEJWLEVBQUFBLFdBQVcsRUFBRU8sbUJBQVVHLElBYlA7QUFjaEI7QUFDQXJLLEVBQUFBLElBQUksRUFBRWtLLG1CQUFVRyxJQWZBO0FBZ0JoQjtBQUNBL0gsRUFBQUEsV0FBVyxFQUFFNEgsbUJBQVVLLElBakJQO0FBa0JoQjtBQUNBckgsRUFBQUEsYUFBYSxFQUFFZ0gsbUJBQVVLLElBbkJUO0FBb0JoQjtBQUNBOUMsRUFBQUEsZUFBZSxFQUFFeUMsbUJBQVVLLElBckJYO0FBc0JoQjtBQUNBVCxFQUFBQSxTQUFTLEVBQUVJLG1CQUFVRyxJQXZCTDtBQXdCaEI7QUFDQTdCLEVBQUFBLFlBQVksRUFBRTBCLG1CQUFVRyxJQXpCUjtBQTBCaEI7QUFDQVIsRUFBQUEsMkJBQTJCLEVBQUVLLG1CQUFVRyxJQTNCdkI7QUE0QmhCO0FBQ0FmLEVBQUFBLFVBQVUsRUFBRVksbUJBQVVHLElBN0JOO0FBOEJoQjtBQUNBTixFQUFBQSxVQUFVLEVBQUVHLG1CQUFVRyxJQS9CTjtBQWdDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQVgsRUFBQUEsVUFBVSxFQUFFUSxtQkFBVUcsSUFwQ047QUFxQ2hCO0FBQ0E7QUFDQTtBQUNBeEYsRUFBQUEscUJBQXFCLEVBQUVxRixtQkFBVU0sS0F4Q2pCO0FBeUNoQjtBQUNBO0FBQ0F0RixFQUFBQSxtQkFBbUIsRUFBRWdGLG1CQUFVSyxJQTNDZjtBQTRDaEI7QUFDQXZJLEVBQUFBLFVBQVUsRUFBRWtJLG1CQUFVRztBQTdDTixDQUFwQjtBQWdEQWxQLE9BQU8sQ0FBQ3NQLFlBQVIsR0FBdUI7QUFDbkI3TSxFQUFBQSxpQkFBaUIsRUFBRSxJQURBO0FBRW5CK0wsRUFBQUEsV0FBVyxFQUFFLElBRk07QUFHbkJHLEVBQUFBLFNBQVMsRUFBRSxJQUhRO0FBSW5CdEIsRUFBQUEsWUFBWSxFQUFFLElBSks7QUFLbkJjLEVBQUFBLFVBQVUsRUFBRSxJQUxPO0FBTW5CUyxFQUFBQSxVQUFVLEVBQUUsSUFOTztBQU9uQkwsRUFBQUEsVUFBVSxFQUFFLEtBUE87QUFRbkJHLEVBQUFBLDJCQUEyQixFQUFFLEtBUlY7QUFTbkJoRixFQUFBQSxxQkFBcUIsRUFBRSxFQVRKO0FBVW5CN0MsRUFBQUEsVUFBVSxFQUFFLEtBVk87QUFXbkJtRyxFQUFBQSxRQUFRLEVBQUU7QUFYUyxDQUF2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB1cmwgZnJvbSAndXJsJztcclxuaW1wb3J0IHFzIGZyb20gJ3FzJztcclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgV2lkZ2V0TWVzc2FnaW5nIGZyb20gJy4uLy4uLy4uL1dpZGdldE1lc3NhZ2luZyc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4vQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IEFwcFBlcm1pc3Npb24gZnJvbSAnLi9BcHBQZXJtaXNzaW9uJztcclxuaW1wb3J0IEFwcFdhcm5pbmcgZnJvbSAnLi9BcHBXYXJuaW5nJztcclxuaW1wb3J0IE1lc3NhZ2VTcGlubmVyIGZyb20gJy4vTWVzc2FnZVNwaW5uZXInO1xyXG5pbXBvcnQgV2lkZ2V0VXRpbHMgZnJvbSAnLi4vLi4vLi4vdXRpbHMvV2lkZ2V0VXRpbHMnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgQWN0aXZlV2lkZ2V0U3RvcmUgZnJvbSAnLi4vLi4vLi4vc3RvcmVzL0FjdGl2ZVdpZGdldFN0b3JlJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCB7SW50ZWdyYXRpb25NYW5hZ2Vyc30gZnJvbSBcIi4uLy4uLy4uL2ludGVncmF0aW9ucy9JbnRlZ3JhdGlvbk1hbmFnZXJzXCI7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlLCB7U2V0dGluZ0xldmVsfSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQge2Fib3ZlTGVmdE9mLCBDb250ZXh0TWVudSwgQ29udGV4dE1lbnVCdXR0b259IGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL0NvbnRleHRNZW51XCI7XHJcbmltcG9ydCBQZXJzaXN0ZWRFbGVtZW50IGZyb20gXCIuL1BlcnNpc3RlZEVsZW1lbnRcIjtcclxuXHJcbmNvbnN0IEFMTE9XRURfQVBQX1VSTF9TQ0hFTUVTID0gWydodHRwczonLCAnaHR0cDonXTtcclxuY29uc3QgRU5BQkxFX1JFQUNUX1BFUkYgPSBmYWxzZTtcclxuXHJcbi8qKlxyXG4gKiBEb2VzIHRlbXBsYXRlIHN1YnN0aXR1dGlvbiBvbiBhIFVSTCAob3IgYW55IHN0cmluZykuIFZhcmlhYmxlcyB3aWxsIGJlXHJcbiAqIHBhc3NlZCB0aHJvdWdoIGVuY29kZVVSSUNvbXBvbmVudC5cclxuICogQHBhcmFtIHtzdHJpbmd9IHVyaVRlbXBsYXRlIFRoZSBwYXRoIHdpdGggdGVtcGxhdGUgdmFyaWFibGVzIGUuZy4gJy9mb28vJGJhcicuXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YXJpYWJsZXMgVGhlIGtleS92YWx1ZSBwYWlycyB0byByZXBsYWNlIHRoZSB0ZW1wbGF0ZVxyXG4gKiB2YXJpYWJsZXMgd2l0aC4gRS5nLiB7ICckYmFyJzogJ2JheicgfS5cclxuICogQHJldHVybiB7c3RyaW5nfSBUaGUgcmVzdWx0IG9mIHJlcGxhY2luZyBhbGwgdGVtcGxhdGUgdmFyaWFibGVzIGUuZy4gJy9mb28vYmF6Jy5cclxuICovXHJcbmZ1bmN0aW9uIHVyaUZyb21UZW1wbGF0ZSh1cmlUZW1wbGF0ZSwgdmFyaWFibGVzKSB7XHJcbiAgICBsZXQgb3V0ID0gdXJpVGVtcGxhdGU7XHJcbiAgICBmb3IgKGNvbnN0IFtrZXksIHZhbF0gb2YgT2JqZWN0LmVudHJpZXModmFyaWFibGVzKSkge1xyXG4gICAgICAgIG91dCA9IG91dC5yZXBsYWNlKFxyXG4gICAgICAgICAgICAnJCcgKyBrZXksIGVuY29kZVVSSUNvbXBvbmVudCh2YWwpLFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gb3V0O1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBcHBUaWxlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICAvLyBUaGUga2V5IHVzZWQgZm9yIFBlcnNpc3RlZEVsZW1lbnRcclxuICAgICAgICB0aGlzLl9wZXJzaXN0S2V5ID0gJ3dpZGdldF8nICsgdGhpcy5wcm9wcy5hcHAuaWQ7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB0aGlzLl9nZXROZXdTdGF0ZShwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuX29uQWN0aW9uID0gdGhpcy5fb25BY3Rpb24uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbkxvYWRlZCA9IHRoaXMuX29uTG9hZGVkLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25FZGl0Q2xpY2sgPSB0aGlzLl9vbkVkaXRDbGljay5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uRGVsZXRlQ2xpY2sgPSB0aGlzLl9vbkRlbGV0ZUNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25SZXZva2VDbGlja2VkID0gdGhpcy5fb25SZXZva2VDbGlja2VkLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25TbmFwc2hvdENsaWNrID0gdGhpcy5fb25TbmFwc2hvdENsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vbkNsaWNrTWVudUJhciA9IHRoaXMub25DbGlja01lbnVCYXIuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vbk1pbmltaXNlQ2xpY2sgPSB0aGlzLl9vbk1pbmltaXNlQ2xpY2suYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9ncmFudFdpZGdldFBlcm1pc3Npb24gPSB0aGlzLl9ncmFudFdpZGdldFBlcm1pc3Npb24uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9yZXZva2VXaWRnZXRQZXJtaXNzaW9uID0gdGhpcy5fcmV2b2tlV2lkZ2V0UGVybWlzc2lvbi5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uUG9wb3V0V2lkZ2V0Q2xpY2sgPSB0aGlzLl9vblBvcG91dFdpZGdldENsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25SZWxvYWRXaWRnZXRDbGljayA9IHRoaXMuX29uUmVsb2FkV2lkZ2V0Q2xpY2suYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fY29udGV4dE1lbnVCdXR0b24gPSBjcmVhdGVSZWYoKTtcclxuICAgICAgICB0aGlzLl9hcHBGcmFtZSA9IGNyZWF0ZVJlZigpO1xyXG4gICAgICAgIHRoaXMuX21lbnVfYmFyID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgaW5pdGlhbCBjb21wb25lbnQgc3RhdGUgd2hlbiB0aGUgQXBwIHdVcmwgKHdpZGdldCBVUkwpIGlzIGJlaW5nIHVwZGF0ZWQuXHJcbiAgICAgKiBDb21wb25lbnQgcHJvcHMgKm11c3QqIGJlIHBhc3NlZCAocmF0aGVyIHRoYW4gcmVseWluZyBvbiB0aGlzLnByb3BzKS5cclxuICAgICAqIEBwYXJhbSAge09iamVjdH0gbmV3UHJvcHMgVGhlIG5ldyBwcm9wZXJ0aWVzIG9mIHRoZSBjb21wb25lbnRcclxuICAgICAqIEByZXR1cm4ge09iamVjdH0gVXBkYXRlZCBjb21wb25lbnQgc3RhdGUgdG8gYmUgc2V0IHdpdGggc2V0U3RhdGVcclxuICAgICAqL1xyXG4gICAgX2dldE5ld1N0YXRlKG5ld1Byb3BzKSB7XHJcbiAgICAgICAgLy8gVGhpcyBpcyBhIGZ1bmN0aW9uIHRvIG1ha2UgdGhlIGltcGFjdCBvZiBjYWxsaW5nIFNldHRpbmdzU3RvcmUgc2xpZ2h0bHkgbGVzc1xyXG4gICAgICAgIGNvbnN0IGhhc1Blcm1pc3Npb25Ub0xvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRseUFsbG93ZWRXaWRnZXRzID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImFsbG93ZWRXaWRnZXRzXCIsIG5ld1Byb3BzLnJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgcmV0dXJuICEhY3VycmVudGx5QWxsb3dlZFdpZGdldHNbbmV3UHJvcHMuYXBwLmV2ZW50SWRdO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IFBlcnNpc3RlZEVsZW1lbnQgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuUGVyc2lzdGVkRWxlbWVudFwiKTtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBpbml0aWFsaXNpbmc6IHRydWUsIC8vIFRydWUgd2hpbGUgd2UgYXJlIG1hbmdsaW5nIHRoZSB3aWRnZXQgVVJMXHJcbiAgICAgICAgICAgIC8vIFRydWUgd2hpbGUgdGhlIGlmcmFtZSBjb250ZW50IGlzIGxvYWRpbmdcclxuICAgICAgICAgICAgbG9hZGluZzogdGhpcy5wcm9wcy53YWl0Rm9ySWZyYW1lTG9hZCAmJiAhUGVyc2lzdGVkRWxlbWVudC5pc01vdW50ZWQodGhpcy5fcGVyc2lzdEtleSksXHJcbiAgICAgICAgICAgIHdpZGdldFVybDogdGhpcy5fYWRkV3VybFBhcmFtcyhuZXdQcm9wcy5hcHAudXJsKSxcclxuICAgICAgICAgICAgLy8gQXNzdW1lIHRoYXQgd2lkZ2V0IGhhcyBwZXJtaXNzaW9uIHRvIGxvYWQgaWYgd2UgYXJlIHRoZSB1c2VyIHdob1xyXG4gICAgICAgICAgICAvLyBhZGRlZCBpdCB0byB0aGUgcm9vbSwgb3IgaWYgZXhwbGljaXRseSBncmFudGVkIGJ5IHRoZSB1c2VyXHJcbiAgICAgICAgICAgIGhhc1Blcm1pc3Npb25Ub0xvYWQ6IG5ld1Byb3BzLnVzZXJJZCA9PT0gbmV3UHJvcHMuY3JlYXRvclVzZXJJZCB8fCBoYXNQZXJtaXNzaW9uVG9Mb2FkKCksXHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBkZWxldGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIHdpZGdldFBhZ2VUaXRsZTogbmV3UHJvcHMud2lkZ2V0UGFnZVRpdGxlLFxyXG4gICAgICAgICAgICBtZW51RGlzcGxheWVkOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRG9lcyB0aGUgd2lkZ2V0IHN1cHBvcnQgYSBnaXZlbiBjYXBhYmlsaXR5XHJcbiAgICAgKiBAcGFyYW0gIHtzdHJpbmd9ICBjYXBhYmlsaXR5IENhcGFiaWxpdHkgdG8gY2hlY2sgZm9yXHJcbiAgICAgKiBAcmV0dXJuIHtCb29sZWFufSAgICAgICAgICAgIFRydWUgaWYgY2FwYWJpbGl0eSBzdXBwb3J0ZWRcclxuICAgICAqL1xyXG4gICAgX2hhc0NhcGFiaWxpdHkoY2FwYWJpbGl0eSkge1xyXG4gICAgICAgIHJldHVybiBBY3RpdmVXaWRnZXRTdG9yZS53aWRnZXRIYXNDYXBhYmlsaXR5KHRoaXMucHJvcHMuYXBwLmlkLCBjYXBhYmlsaXR5KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZCB3aWRnZXQgaW5zdGFuY2Ugc3BlY2lmaWMgcGFyYW1ldGVycyB0byBwYXNzIGluIHdVcmxcclxuICAgICAqIFByb3BlcnRpZXMgcGFzc2VkIHRvIHdpZGdldCBpbnN0YW5jZTpcclxuICAgICAqICAtIHdpZGdldElkXHJcbiAgICAgKiAgLSBvcmlnaW4gLyBwYXJlbnQgVVJMXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsU3RyaW5nIFVybCBzdHJpbmcgdG8gbW9kaWZ5XHJcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAgICAgKiBVcmwgc3RyaW5nIHdpdGggcGFyYW1ldGVycyBhcHBlbmRlZC5cclxuICAgICAqIElmIHVybCBjYW4gbm90IGJlIHBhcnNlZCwgaXQgaXMgcmV0dXJuZWQgdW5tb2RpZmllZC5cclxuICAgICAqL1xyXG4gICAgX2FkZFd1cmxQYXJhbXModXJsU3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgdSA9IHVybC5wYXJzZSh1cmxTdHJpbmcpO1xyXG4gICAgICAgIGlmICghdSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiX2FkZFd1cmxQYXJhbXNcIiwgXCJJbnZhbGlkIFVSTFwiLCB1cmxTdHJpbmcpO1xyXG4gICAgICAgICAgICByZXR1cm4gdXJsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW1zID0gcXMucGFyc2UodS5xdWVyeSk7XHJcbiAgICAgICAgLy8gQXBwZW5kIHdpZGdldCBJRCB0byBxdWVyeSBwYXJhbWV0ZXJzXHJcbiAgICAgICAgcGFyYW1zLndpZGdldElkID0gdGhpcy5wcm9wcy5hcHAuaWQ7XHJcbiAgICAgICAgLy8gQXBwZW5kIGN1cnJlbnQgLyBwYXJlbnQgVVJMLCBtaW51cyB0aGUgaGFzaCBiZWNhdXNlIHRoYXQgd2lsbCBjaGFuZ2Ugd2hlblxyXG4gICAgICAgIC8vIHdlIHZpZXcgYSBkaWZmZXJlbnQgcm9vbSAoaWUuIG1heSBjaGFuZ2UgZm9yIHBlcnNpc3RlbnQgd2lkZ2V0cylcclxuICAgICAgICBwYXJhbXMucGFyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWYuc3BsaXQoJyMnLCAyKVswXTtcclxuICAgICAgICB1LnNlYXJjaCA9IHVuZGVmaW5lZDtcclxuICAgICAgICB1LnF1ZXJ5ID0gcGFyYW1zO1xyXG5cclxuICAgICAgICByZXR1cm4gdS5mb3JtYXQoKTtcclxuICAgIH1cclxuXHJcbiAgICBpc01peGVkQ29udGVudCgpIHtcclxuICAgICAgICBjb25zdCBwYXJlbnRDb250ZW50UHJvdG9jb2wgPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2w7XHJcbiAgICAgICAgY29uc3QgdSA9IHVybC5wYXJzZSh0aGlzLnByb3BzLmFwcC51cmwpO1xyXG4gICAgICAgIGNvbnN0IGNoaWxkQ29udGVudFByb3RvY29sID0gdS5wcm90b2NvbDtcclxuICAgICAgICBpZiAocGFyZW50Q29udGVudFByb3RvY29sID09PSAnaHR0cHM6JyAmJiBjaGlsZENvbnRlbnRQcm90b2NvbCAhPT0gJ2h0dHBzOicpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiUmVmdXNpbmcgdG8gbG9hZCBtaXhlZC1jb250ZW50IGFwcDpcIixcclxuICAgICAgICAgICAgcGFyZW50Q29udGVudFByb3RvY29sLCBjaGlsZENvbnRlbnRQcm90b2NvbCwgd2luZG93LmxvY2F0aW9uLCB0aGlzLnByb3BzLmFwcC51cmwpO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIC8vIE9ubHkgZmV0Y2ggSU0gdG9rZW4gb24gbW91bnQgaWYgd2UncmUgc2hvd2luZyBhbmQgaGF2ZSBwZXJtaXNzaW9uIHRvIGxvYWRcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93ICYmIHRoaXMuc3RhdGUuaGFzUGVybWlzc2lvblRvTG9hZCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFNjYWxhclRva2VuKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBXaWRnZXQgYWN0aW9uIGxpc3RlbmVyc1xyXG4gICAgICAgIHRoaXMuZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLl9vbkFjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgLy8gV2lkZ2V0IGFjdGlvbiBsaXN0ZW5lcnNcclxuICAgICAgICBpZiAodGhpcy5kaXNwYXRjaGVyUmVmKSBkaXMudW5yZWdpc3Rlcih0aGlzLmRpc3BhdGNoZXJSZWYpO1xyXG5cclxuICAgICAgICAvLyBpZiBpdCdzIG5vdCByZW1haW5pbmcgb24gc2NyZWVuLCBnZXQgcmlkIG9mIHRoZSBQZXJzaXN0ZWRFbGVtZW50IGNvbnRhaW5lclxyXG4gICAgICAgIGlmICghQWN0aXZlV2lkZ2V0U3RvcmUuZ2V0V2lkZ2V0UGVyc2lzdGVuY2UodGhpcy5wcm9wcy5hcHAuaWQpKSB7XHJcbiAgICAgICAgICAgIEFjdGl2ZVdpZGdldFN0b3JlLmRlc3Ryb3lQZXJzaXN0ZW50V2lkZ2V0KHRoaXMucHJvcHMuYXBwLmlkKTtcclxuICAgICAgICAgICAgY29uc3QgUGVyc2lzdGVkRWxlbWVudCA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5QZXJzaXN0ZWRFbGVtZW50XCIpO1xyXG4gICAgICAgICAgICBQZXJzaXN0ZWRFbGVtZW50LmRlc3Ryb3lFbGVtZW50KHRoaXMuX3BlcnNpc3RLZXkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgYSBzY2FsYXIgdG9rZW4gdG8gdGhlIHdpZGdldCBVUkwsIGlmIHJlcXVpcmVkXHJcbiAgICAgKiBDb21wb25lbnQgaW5pdGlhbGlzYXRpb24gaXMgb25seSBjb21wbGV0ZSB3aGVuIHRoaXMgZnVuY3Rpb24gaGFzIHJlc29sdmVkXHJcbiAgICAgKi9cclxuICAgIHNldFNjYWxhclRva2VuKCkge1xyXG4gICAgICAgIGlmICghV2lkZ2V0VXRpbHMuaXNTY2FsYXJVcmwodGhpcy5wcm9wcy5hcHAudXJsKSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ05vbi1zY2FsYXIgd2lkZ2V0LCBub3Qgc2V0dGluZyBzY2FsYXIgdG9rZW4hJywgdXJsKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICAgICAgICAgIHdpZGdldFVybDogdGhpcy5fYWRkV3VybFBhcmFtcyh0aGlzLnByb3BzLmFwcC51cmwpLFxyXG4gICAgICAgICAgICAgICAgaW5pdGlhbGlzaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG1hbmFnZXJzID0gSW50ZWdyYXRpb25NYW5hZ2Vycy5zaGFyZWRJbnN0YW5jZSgpO1xyXG4gICAgICAgIGlmICghbWFuYWdlcnMuaGFzTWFuYWdlcigpKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIk5vIGludGVncmF0aW9uIG1hbmFnZXIgLSBub3Qgc2V0dGluZyBzY2FsYXIgdG9rZW5cIiwgdXJsKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICAgICAgICAgIHdpZGdldFVybDogdGhpcy5fYWRkV3VybFBhcmFtcyh0aGlzLnByb3BzLmFwcC51cmwpLFxyXG4gICAgICAgICAgICAgICAgaW5pdGlhbGlzaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRPRE86IFBpY2sgdGhlIHJpZ2h0IG1hbmFnZXIgZm9yIHRoZSB3aWRnZXRcclxuXHJcbiAgICAgICAgY29uc3QgZGVmYXVsdE1hbmFnZXIgPSBtYW5hZ2Vycy5nZXRQcmltYXJ5TWFuYWdlcigpO1xyXG4gICAgICAgIGlmICghV2lkZ2V0VXRpbHMuaXNTY2FsYXJVcmwoZGVmYXVsdE1hbmFnZXIuYXBpVXJsKSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ05vbi1zY2FsYXIgbWFuYWdlciwgbm90IHNldHRpbmcgc2NhbGFyIHRva2VuIScsIHVybCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB3aWRnZXRVcmw6IHRoaXMuX2FkZFd1cmxQYXJhbXModGhpcy5wcm9wcy5hcHAudXJsKSxcclxuICAgICAgICAgICAgICAgIGluaXRpYWxpc2luZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBGZXRjaCB0aGUgdG9rZW4gYmVmb3JlIGxvYWRpbmcgdGhlIGlmcmFtZSBhcyB3ZSBuZWVkIGl0IHRvIG1hbmdsZSB0aGUgVVJMXHJcbiAgICAgICAgaWYgKCF0aGlzLl9zY2FsYXJDbGllbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2NhbGFyQ2xpZW50ID0gZGVmYXVsdE1hbmFnZXIuZ2V0U2NhbGFyQ2xpZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3NjYWxhckNsaWVudC5nZXRTY2FsYXJUb2tlbigpLnRoZW4oKHRva2VuKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFwcGVuZCBzY2FsYXJfdG9rZW4gYXMgYSBxdWVyeSBwYXJhbSBpZiBub3QgYWxyZWFkeSBwcmVzZW50XHJcbiAgICAgICAgICAgIHRoaXMuX3NjYWxhckNsaWVudC5zY2FsYXJUb2tlbiA9IHRva2VuO1xyXG4gICAgICAgICAgICBjb25zdCB1ID0gdXJsLnBhcnNlKHRoaXMuX2FkZFd1cmxQYXJhbXModGhpcy5wcm9wcy5hcHAudXJsKSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHFzLnBhcnNlKHUucXVlcnkpO1xyXG4gICAgICAgICAgICBpZiAoIXBhcmFtcy5zY2FsYXJfdG9rZW4pIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtcy5zY2FsYXJfdG9rZW4gPSBlbmNvZGVVUklDb21wb25lbnQodG9rZW4pO1xyXG4gICAgICAgICAgICAgICAgLy8gdS5zZWFyY2ggbXVzdCBiZSBzZXQgdG8gdW5kZWZpbmVkLCBzbyB0aGF0IHUuZm9ybWF0KCkgdXNlcyBxdWVyeSBwYXJhbWV0ZXJzIC0gaHR0cHM6Ly9ub2RlanMub3JnL2RvY3MvbGF0ZXN0L2FwaS91cmwuaHRtbCN1cmxfdXJsX2Zvcm1hdF91cmxfb3B0aW9uc1xyXG4gICAgICAgICAgICAgICAgdS5zZWFyY2ggPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICB1LnF1ZXJ5ID0gcGFyYW1zO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgd2lkZ2V0VXJsOiB1LmZvcm1hdCgpLFxyXG4gICAgICAgICAgICAgICAgaW5pdGlhbGlzaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBGZXRjaCBwYWdlIHRpdGxlIGZyb20gcmVtb3RlIGNvbnRlbnQgaWYgbm90IGFscmVhZHkgc2V0XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZS53aWRnZXRQYWdlVGl0bGUgJiYgcGFyYW1zLnVybCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZmV0Y2hXaWRnZXRUaXRsZShwYXJhbXMudXJsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBnZXQgc2NhbGFyX3Rva2VuXCIsIGVycik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZXJyb3I6IGVyci5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgaW5pdGlhbGlzaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2Ugd2l0aCBhcHByb3ByaWF0ZSBsaWZlY3ljbGUgZXZlbnRcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxyXG4gICAgICAgIGlmIChuZXh0UHJvcHMuYXBwLnVybCAhPT0gdGhpcy5wcm9wcy5hcHAudXJsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2dldE5ld1N0YXRlKG5leHRQcm9wcyk7XHJcbiAgICAgICAgICAgIC8vIEZldGNoIElNIHRva2VuIGZvciBuZXcgVVJMIGlmIHdlJ3JlIHNob3dpbmcgYW5kIGhhdmUgcGVybWlzc2lvbiB0byBsb2FkXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLnNob3cgJiYgdGhpcy5zdGF0ZS5oYXNQZXJtaXNzaW9uVG9Mb2FkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFNjYWxhclRva2VuKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKG5leHRQcm9wcy5zaG93ICYmICF0aGlzLnByb3BzLnNob3cpIHtcclxuICAgICAgICAgICAgLy8gV2UgYXNzdW1lIHRoYXQgcGVyc2lzdGVkIHdpZGdldHMgYXJlIGxvYWRlZCBhbmQgZG9uJ3QgbmVlZCBhIHNwaW5uZXIuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLndhaXRGb3JJZnJhbWVMb2FkICYmICFQZXJzaXN0ZWRFbGVtZW50LmlzTW91bnRlZCh0aGlzLl9wZXJzaXN0S2V5KSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgbG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEZldGNoIElNIHRva2VuIG5vdyB0aGF0IHdlJ3JlIHNob3dpbmcgaWYgd2UgYWxyZWFkeSBoYXZlIHBlcm1pc3Npb24gdG8gbG9hZFxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5oYXNQZXJtaXNzaW9uVG9Mb2FkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFNjYWxhclRva2VuKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKG5leHRQcm9wcy53aWRnZXRQYWdlVGl0bGUgIT09IHRoaXMucHJvcHMud2lkZ2V0UGFnZVRpdGxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgd2lkZ2V0UGFnZVRpdGxlOiBuZXh0UHJvcHMud2lkZ2V0UGFnZVRpdGxlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX2NhblVzZXJNb2RpZnkoKSB7XHJcbiAgICAgICAgLy8gVXNlciB3aWRnZXRzIHNob3VsZCBhbHdheXMgYmUgbW9kaWZpYWJsZSBieSB0aGVpciBjcmVhdG9yXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMudXNlcldpZGdldCAmJiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY3JlZGVudGlhbHMudXNlcklkID09PSB0aGlzLnByb3BzLmNyZWF0b3JVc2VySWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIENoZWNrIGlmIHRoZSBjdXJyZW50IHVzZXIgY2FuIG1vZGlmeSB3aWRnZXRzIGluIHRoZSBjdXJyZW50IHJvb21cclxuICAgICAgICByZXR1cm4gV2lkZ2V0VXRpbHMuY2FuVXNlck1vZGlmeVdpZGdldHModGhpcy5wcm9wcy5yb29tLnJvb21JZCk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uRWRpdENsaWNrKCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiRWRpdCB3aWRnZXQgSUQgXCIsIHRoaXMucHJvcHMuYXBwLmlkKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkVkaXRDbGljaykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRWRpdENsaWNrKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gVE9ETzogT3BlbiB0aGUgcmlnaHQgbWFuYWdlciBmb3IgdGhlIHdpZGdldFxyXG4gICAgICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9tYW55X2ludGVncmF0aW9uX21hbmFnZXJzXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBJbnRlZ3JhdGlvbk1hbmFnZXJzLnNoYXJlZEluc3RhbmNlKCkub3BlbkFsbChcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnJvb20sXHJcbiAgICAgICAgICAgICAgICAgICAgJ3R5cGVfJyArIHRoaXMucHJvcHMudHlwZSxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmFwcC5pZCxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBJbnRlZ3JhdGlvbk1hbmFnZXJzLnNoYXJlZEluc3RhbmNlKCkuZ2V0UHJpbWFyeU1hbmFnZXIoKS5vcGVuKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMucm9vbSxcclxuICAgICAgICAgICAgICAgICAgICAndHlwZV8nICsgdGhpcy5wcm9wcy50eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuYXBwLmlkLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25TbmFwc2hvdENsaWNrKCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiUmVxdWVzdGluZyB3aWRnZXQgc25hcHNob3RcIik7XHJcbiAgICAgICAgQWN0aXZlV2lkZ2V0U3RvcmUuZ2V0V2lkZ2V0TWVzc2FnaW5nKHRoaXMucHJvcHMuYXBwLmlkKS5nZXRTY3JlZW5zaG90KClcclxuICAgICAgICAgICAgLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gZ2V0IHNjcmVlbnNob3RcIiwgZXJyKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHNjcmVlbnNob3QpID0+IHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAncGljdHVyZV9zbmFwc2hvdCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZTogc2NyZWVuc2hvdCxcclxuICAgICAgICAgICAgICAgIH0sIHRydWUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBJZiB1c2VyIGhhcyBwZXJtaXNzaW9uIHRvIG1vZGlmeSB3aWRnZXRzLCBkZWxldGUgdGhlIHdpZGdldCxcclxuICAgICAqIG90aGVyd2lzZSByZXZva2UgYWNjZXNzIGZvciB0aGUgd2lkZ2V0IHRvIGxvYWQgaW4gdGhlIHVzZXIncyBicm93c2VyXHJcbiAgICAqL1xyXG4gICAgX29uRGVsZXRlQ2xpY2soKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25EZWxldGVDbGljaykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRGVsZXRlQ2xpY2soKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuX2NhblVzZXJNb2RpZnkoKSkge1xyXG4gICAgICAgICAgICAvLyBTaG93IGRlbGV0ZSBjb25maXJtYXRpb24gZGlhbG9nXHJcbiAgICAgICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0RlbGV0ZSBXaWRnZXQnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkRlbGV0ZSBXaWRnZXRcIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJEZWxldGluZyBhIHdpZGdldCByZW1vdmVzIGl0IGZvciBhbGwgdXNlcnMgaW4gdGhpcyByb29tLlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIiBBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgd2lkZ2V0P1wiKSxcclxuICAgICAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJEZWxldGUgd2lkZ2V0XCIpLFxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZDogKGNvbmZpcm1lZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRpbmc6IHRydWV9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSEFDSzogVGhpcyBpcyBhIHJlYWxseSBkaXJ0eSB3YXkgdG8gZW5zdXJlIHRoYXQgSml0c2kgY2xlYW5zIHVwXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaXRzIGhvbGQgb24gdGhlIHdlYmNhbS4gV2l0aG91dCB0aGlzLCB0aGUgd2lkZ2V0IGhvbGRzIGEgbWVkaWFcclxuICAgICAgICAgICAgICAgICAgICAvLyBzdHJlYW0gb3BlbiwgZXZlbiBhZnRlciBkZWF0aC4gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzczNTFcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5fYXBwRnJhbWUuY3VycmVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJbiBwcmFjdGljZSB3ZSBjb3VsZCBqdXN0IGRvIGArPSAnJ2AgdG8gdHJpY2sgdGhlIGJyb3dzZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaW50byB0aGlua2luZyB0aGUgVVJMIGNoYW5nZWQsIGhvd2V2ZXIgSSBjYW4gZm9yZXNlZSB0aGlzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJlaW5nIG9wdGltaXplZCBvdXQgYnkgYSBicm93c2VyLiBJbnN0ZWFkLCB3ZSdsbCBqdXN0IHBvaW50XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSBpZnJhbWUgYXQgYSBwYWdlIHRoYXQgaXMgcmVhc29uYWJseSBzYWZlIHRvIHVzZSBpbiB0aGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZXZlbnQgdGhlIGlmcmFtZSBkb2Vzbid0IHdpbmsgYXdheS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVGhpcyBpcyByZWxhdGl2ZSB0byB3aGVyZSB0aGUgUmlvdCBpbnN0YW5jZSBpcyBsb2NhdGVkLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9hcHBGcmFtZS5jdXJyZW50LnNyYyA9ICdhYm91dDpibGFuayc7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBXaWRnZXRVdGlscy5zZXRSb29tV2lkZ2V0KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnJvb20ucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmFwcC5pZCxcclxuICAgICAgICAgICAgICAgICAgICApLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ZhaWxlZCB0byBkZWxldGUgd2lkZ2V0JywgZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gcmVtb3ZlIHdpZGdldCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gcmVtb3ZlIHdpZGdldCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdBbiBlcnJvciBvY3VycmVkIHdoaWxzdCB0cnlpbmcgdG8gcmVtb3ZlIHRoZSB3aWRnZXQgZnJvbSB0aGUgcm9vbScpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRpbmc6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uUmV2b2tlQ2xpY2tlZCgpIHtcclxuICAgICAgICBjb25zb2xlLmluZm8oXCJSZXZva2Ugd2lkZ2V0IHBlcm1pc3Npb25zIC0gJXNcIiwgdGhpcy5wcm9wcy5hcHAuaWQpO1xyXG4gICAgICAgIHRoaXMuX3Jldm9rZVdpZGdldFBlcm1pc3Npb24oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGxlZCB3aGVuIHdpZGdldCBpZnJhbWUgaGFzIGZpbmlzaGVkIGxvYWRpbmdcclxuICAgICAqL1xyXG4gICAgX29uTG9hZGVkKCkge1xyXG4gICAgICAgIC8vIERlc3Ryb3kgdGhlIG9sZCB3aWRnZXQgbWVzc2FnaW5nIGJlZm9yZSBzdGFydGluZyBpdCBiYWNrIHVwIGFnYWluLiBTb21lIHdpZGdldHNcclxuICAgICAgICAvLyBoYXZlIHN0YXJ0dXAgcm91dGluZXMgdGhhdCBydW4gd2hlbiB0aGV5IGFyZSBsb2FkZWQsIHNvIHdlIGp1c3QgbmVlZCB0byByZWluaXRpYWxpemVcclxuICAgICAgICAvLyB0aGUgbWVzc2FnaW5nIGZvciB0aGVtLlxyXG4gICAgICAgIEFjdGl2ZVdpZGdldFN0b3JlLmRlbFdpZGdldE1lc3NhZ2luZyh0aGlzLnByb3BzLmFwcC5pZCk7XHJcbiAgICAgICAgdGhpcy5fc2V0dXBXaWRnZXRNZXNzYWdpbmcoKTtcclxuXHJcbiAgICAgICAgQWN0aXZlV2lkZ2V0U3RvcmUuc2V0Um9vbUlkKHRoaXMucHJvcHMuYXBwLmlkLCB0aGlzLnByb3BzLnJvb20ucm9vbUlkKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtsb2FkaW5nOiBmYWxzZX0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXR1cFdpZGdldE1lc3NhZ2luZygpIHtcclxuICAgICAgICAvLyBGSVhNRTogVGhlcmUncyBwcm9iYWJseSBubyByZWFzb24gdG8gZG8gdGhpcyBoZXJlOiBpdCBzaG91bGQgcHJvYmFibHkgYmUgZG9uZSBlbnRpcmVseVxyXG4gICAgICAgIC8vIGluIEFjdGl2ZVdpZGdldFN0b3JlLlxyXG4gICAgICAgIGNvbnN0IHdpZGdldE1lc3NhZ2luZyA9IG5ldyBXaWRnZXRNZXNzYWdpbmcoXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuYXBwLmlkLCB0aGlzLl9nZXRSZW5kZXJlZFVybCgpLCB0aGlzLnByb3BzLnVzZXJXaWRnZXQsIHRoaXMuX2FwcEZyYW1lLmN1cnJlbnQuY29udGVudFdpbmRvdyk7XHJcbiAgICAgICAgQWN0aXZlV2lkZ2V0U3RvcmUuc2V0V2lkZ2V0TWVzc2FnaW5nKHRoaXMucHJvcHMuYXBwLmlkLCB3aWRnZXRNZXNzYWdpbmcpO1xyXG4gICAgICAgIHdpZGdldE1lc3NhZ2luZy5nZXRDYXBhYmlsaXRpZXMoKS50aGVuKChyZXF1ZXN0ZWRDYXBhYmlsaXRpZXMpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYFdpZGdldCAke3RoaXMucHJvcHMuYXBwLmlkfSByZXF1ZXN0ZWQgY2FwYWJpbGl0aWVzOiBgICsgcmVxdWVzdGVkQ2FwYWJpbGl0aWVzKTtcclxuICAgICAgICAgICAgcmVxdWVzdGVkQ2FwYWJpbGl0aWVzID0gcmVxdWVzdGVkQ2FwYWJpbGl0aWVzIHx8IFtdO1xyXG5cclxuICAgICAgICAgICAgLy8gQWxsb3cgd2hpdGVsaXN0ZWQgY2FwYWJpbGl0aWVzXHJcbiAgICAgICAgICAgIGxldCByZXF1ZXN0ZWRXaGl0ZWxpc3RDYXBhYmlsaWVzID0gW107XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy53aGl0ZWxpc3RDYXBhYmlsaXRpZXMgJiYgdGhpcy5wcm9wcy53aGl0ZWxpc3RDYXBhYmlsaXRpZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdGVkV2hpdGVsaXN0Q2FwYWJpbGllcyA9IHJlcXVlc3RlZENhcGFiaWxpdGllcy5maWx0ZXIoZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmluZGV4T2YoZSk+PTA7XHJcbiAgICAgICAgICAgICAgICB9LCB0aGlzLnByb3BzLndoaXRlbGlzdENhcGFiaWxpdGllcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJlcXVlc3RlZFdoaXRlbGlzdENhcGFiaWxpZXMubGVuZ3RoID4gMCApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgV2lkZ2V0ICR7dGhpcy5wcm9wcy5hcHAuaWR9IGFsbG93aW5nIHJlcXVlc3RlZCwgd2hpdGVsaXN0ZWQgcHJvcGVydGllczogYCArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RlZFdoaXRlbGlzdENhcGFiaWxpZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gVE9ETyAtLSBBZGQgVUkgdG8gd2FybiBhYm91dCBhbmQgb3B0aW9uYWxseSBhbGxvdyByZXF1ZXN0ZWQgY2FwYWJpbGl0aWVzXHJcblxyXG4gICAgICAgICAgICBBY3RpdmVXaWRnZXRTdG9yZS5zZXRXaWRnZXRDYXBhYmlsaXRpZXModGhpcy5wcm9wcy5hcHAuaWQsIHJlcXVlc3RlZFdoaXRlbGlzdENhcGFiaWxpZXMpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMub25DYXBhYmlsaXR5UmVxdWVzdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkNhcGFiaWxpdHlSZXF1ZXN0KHJlcXVlc3RlZENhcGFiaWxpdGllcyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIFdlIG9ubHkgdGVsbCBKaXRzaSB3aWRnZXRzIHRoYXQgd2UncmUgcmVhZHkgYmVjYXVzZSB0aGV5J3JlIHJlYWxpc3RpY2FsbHkgdGhlIG9ubHkgb25lc1xyXG4gICAgICAgICAgICAvLyB1c2luZyB0aGlzIGN1c3RvbSBleHRlbnNpb24gdG8gdGhlIHdpZGdldCBBUEkuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmFwcC50eXBlID09PSAnaml0c2knKSB7XHJcbiAgICAgICAgICAgICAgICB3aWRnZXRNZXNzYWdpbmcuZmxhZ1JlYWR5VG9Db250aW51ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgRmFpbGVkIHRvIGdldCBjYXBhYmlsaXRpZXMgZm9yIHdpZGdldCB0eXBlICR7dGhpcy5wcm9wcy5hcHAudHlwZX1gLCB0aGlzLnByb3BzLmFwcC5pZCwgZXJyKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25BY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIGlmIChwYXlsb2FkLndpZGdldElkID09PSB0aGlzLnByb3BzLmFwcC5pZCkge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdtLnN0aWNrZXInOlxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX2hhc0NhcGFiaWxpdHkoJ20uc3RpY2tlcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdwb3N0X3N0aWNrZXJfbWVzc2FnZScsIGRhdGE6IHBheWxvYWQuZGF0YX0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ0lnbm9yaW5nIHN0aWNrZXIgbWVzc2FnZS4gSW52YWxpZCBjYXBhYmlsaXR5Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCByZW1vdGUgY29udGVudCB0aXRsZSBvbiBBcHBUaWxlXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIFVybCB0byBjaGVjayBmb3IgdGl0bGVcclxuICAgICAqL1xyXG4gICAgX2ZldGNoV2lkZ2V0VGl0bGUodXJsKSB7XHJcbiAgICAgICAgdGhpcy5fc2NhbGFyQ2xpZW50LmdldFNjYWxhclBhZ2VUaXRsZSh1cmwpLnRoZW4oKHdpZGdldFBhZ2VUaXRsZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAod2lkZ2V0UGFnZVRpdGxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt3aWRnZXRQYWdlVGl0bGU6IHdpZGdldFBhZ2VUaXRsZX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgKGVycikgPT57XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gZ2V0IHBhZ2UgdGl0bGVcIiwgZXJyKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfZ3JhbnRXaWRnZXRQZXJtaXNzaW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb21JZCA9IHRoaXMucHJvcHMucm9vbS5yb29tSWQ7XHJcbiAgICAgICAgY29uc29sZS5pbmZvKFwiR3JhbnRpbmcgcGVybWlzc2lvbiBmb3Igd2lkZ2V0IHRvIGxvYWQ6IFwiICsgdGhpcy5wcm9wcy5hcHAuZXZlbnRJZCk7XHJcbiAgICAgICAgY29uc3QgY3VycmVudCA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJhbGxvd2VkV2lkZ2V0c1wiLCByb29tSWQpO1xyXG4gICAgICAgIGN1cnJlbnRbdGhpcy5wcm9wcy5hcHAuZXZlbnRJZF0gPSB0cnVlO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXCJhbGxvd2VkV2lkZ2V0c1wiLCByb29tSWQsIFNldHRpbmdMZXZlbC5ST09NX0FDQ09VTlQsIGN1cnJlbnQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtoYXNQZXJtaXNzaW9uVG9Mb2FkOiB0cnVlfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBGZXRjaCBhIHRva2VuIGZvciB0aGUgaW50ZWdyYXRpb24gbWFuYWdlciwgbm93IHRoYXQgd2UncmUgYWxsb3dlZCB0b1xyXG4gICAgICAgICAgICB0aGlzLnNldFNjYWxhclRva2VuKCk7XHJcbiAgICAgICAgfSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAvLyBXZSBkb24ndCByZWFsbHkgbmVlZCB0byBkbyBhbnl0aGluZyBhYm91dCB0aGlzIC0gdGhlIHVzZXIgd2lsbCBqdXN0IGhpdCB0aGUgYnV0dG9uIGFnYWluLlxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZXZva2VXaWRnZXRQZXJtaXNzaW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb21JZCA9IHRoaXMucHJvcHMucm9vbS5yb29tSWQ7XHJcbiAgICAgICAgY29uc29sZS5pbmZvKFwiUmV2b2tpbmcgcGVybWlzc2lvbiBmb3Igd2lkZ2V0IHRvIGxvYWQ6IFwiICsgdGhpcy5wcm9wcy5hcHAuZXZlbnRJZCk7XHJcbiAgICAgICAgY29uc3QgY3VycmVudCA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJhbGxvd2VkV2lkZ2V0c1wiLCByb29tSWQpO1xyXG4gICAgICAgIGN1cnJlbnRbdGhpcy5wcm9wcy5hcHAuZXZlbnRJZF0gPSBmYWxzZTtcclxuICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiYWxsb3dlZFdpZGdldHNcIiwgcm9vbUlkLCBTZXR0aW5nTGV2ZWwuUk9PTV9BQ0NPVU5ULCBjdXJyZW50KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aGFzUGVybWlzc2lvblRvTG9hZDogZmFsc2V9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEZvcmNlIHRoZSB3aWRnZXQgdG8gYmUgbm9uLXBlcnNpc3RlbnQgKGFibGUgdG8gYmUgZGVsZXRlZC9mb3Jnb3R0ZW4pXHJcbiAgICAgICAgICAgIEFjdGl2ZVdpZGdldFN0b3JlLmRlc3Ryb3lQZXJzaXN0ZW50V2lkZ2V0KHRoaXMucHJvcHMuYXBwLmlkKTtcclxuICAgICAgICAgICAgY29uc3QgUGVyc2lzdGVkRWxlbWVudCA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5QZXJzaXN0ZWRFbGVtZW50XCIpO1xyXG4gICAgICAgICAgICBQZXJzaXN0ZWRFbGVtZW50LmRlc3Ryb3lFbGVtZW50KHRoaXMuX3BlcnNpc3RLZXkpO1xyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgLy8gV2UgZG9uJ3QgcmVhbGx5IG5lZWQgdG8gZG8gYW55dGhpbmcgYWJvdXQgdGhpcyAtIHRoZSB1c2VyIHdpbGwganVzdCBoaXQgdGhlIGJ1dHRvbiBhZ2Fpbi5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtYXRBcHBUaWxlTmFtZSgpIHtcclxuICAgICAgICBsZXQgYXBwVGlsZU5hbWUgPSBcIk5vIG5hbWVcIjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5hcHAubmFtZSAmJiB0aGlzLnByb3BzLmFwcC5uYW1lLnRyaW0oKSkge1xyXG4gICAgICAgICAgICBhcHBUaWxlTmFtZSA9IHRoaXMucHJvcHMuYXBwLm5hbWUudHJpbSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gYXBwVGlsZU5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGlja01lbnVCYXIoZXYpIHtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAvLyBJZ25vcmUgY2xpY2tzIG9uIG1lbnUgYmFyIGNoaWxkcmVuXHJcbiAgICAgICAgaWYgKGV2LnRhcmdldCAhPT0gdGhpcy5fbWVudV9iYXIuY3VycmVudCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBUb2dnbGUgdGhlIHZpZXcgc3RhdGUgb2YgdGhlIGFwcHMgZHJhd2VyXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMudXNlcldpZGdldCkge1xyXG4gICAgICAgICAgICB0aGlzLl9vbk1pbmltaXNlQ2xpY2soKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnYXBwc0RyYXdlcicsXHJcbiAgICAgICAgICAgICAgICBzaG93OiAhdGhpcy5wcm9wcy5zaG93LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXBsYWNlIHRoZSB3aWRnZXQgdGVtcGxhdGUgdmFyaWFibGVzIGluIGEgdXJsIHdpdGggdGhlaXIgdmFsdWVzXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHUgVGhlIFVSTCB3aXRoIHRlbXBsYXRlIHZhcmlhYmxlc1xyXG4gICAgICpcclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IHVybCB3aXRoIHRlbWxhdGUgdmFyaWFibGVzIHJlcGxhY2VkXHJcbiAgICAgKi9cclxuICAgIF90ZW1wbGF0ZWRVcmwodSkge1xyXG4gICAgICAgIGNvbnN0IG15VXNlcklkID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZDtcclxuICAgICAgICBjb25zdCBteVVzZXIgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcihteVVzZXJJZCk7XHJcbiAgICAgICAgY29uc3QgdmFycyA9IE9iamVjdC5hc3NpZ24oe1xyXG4gICAgICAgICAgICBkb21haW46IFwiaml0c2kucmlvdC5pbVwiLCAvLyB2MSB3aWRnZXRzIGhhdmUgdGhpcyBoYXJkY29kZWRcclxuICAgICAgICB9LCB0aGlzLnByb3BzLmFwcC5kYXRhLCB7XHJcbiAgICAgICAgICAgICdtYXRyaXhfdXNlcl9pZCc6IG15VXNlcklkLFxyXG4gICAgICAgICAgICAnbWF0cml4X3Jvb21faWQnOiB0aGlzLnByb3BzLnJvb20ucm9vbUlkLFxyXG4gICAgICAgICAgICAnbWF0cml4X2Rpc3BsYXlfbmFtZSc6IG15VXNlciA/IG15VXNlci5kaXNwbGF5TmFtZSA6IG15VXNlcklkLFxyXG4gICAgICAgICAgICAnbWF0cml4X2F2YXRhcl91cmwnOiBteVVzZXIgPyBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubXhjVXJsVG9IdHRwKG15VXNlci5hdmF0YXJVcmwpIDogJycsXHJcblxyXG4gICAgICAgICAgICAvLyBUT0RPOiBOYW1lc3BhY2UgdGhlbWVzIHRocm91Z2ggc29tZSBzdGFuZGFyZFxyXG4gICAgICAgICAgICAndGhlbWUnOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwidGhlbWVcIiksXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh2YXJzLmNvbmZlcmVuY2VJZCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIC8vIHdlJ2xsIG5lZWQgdG8gcGFyc2UgdGhlIGNvbmZlcmVuY2UgSUQgb3V0IG9mIHRoZSBVUkwgZm9yIHYxIEppdHNpIHdpZGdldHNcclxuICAgICAgICAgICAgY29uc3QgcGFyc2VkVXJsID0gbmV3IFVSTCh0aGlzLnByb3BzLmFwcC51cmwpO1xyXG4gICAgICAgICAgICB2YXJzLmNvbmZlcmVuY2VJZCA9IHBhcnNlZFVybC5zZWFyY2hQYXJhbXMuZ2V0KFwiY29uZklkXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHVyaUZyb21UZW1wbGF0ZSh1LCB2YXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgVVJMIHVzZWQgaW4gdGhlIGlmcmFtZVxyXG4gICAgICogSW4gY2FzZXMgd2hlcmUgd2Ugc3VwcGx5IG91ciBvd24gVUkgZm9yIGEgd2lkZ2V0LCB0aGlzIGlzIGFuIGludGVybmFsXHJcbiAgICAgKiBVUkwgZGlmZmVyZW50IHRvIHRoZSBvbmUgdXNlZCBpZiB0aGUgd2lkZ2V0IGlzIHBvcHBlZCBvdXQgdG8gYSBzZXBhcmF0ZVxyXG4gICAgICogdGFiIC8gYnJvd3NlclxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IHVybFxyXG4gICAgICovXHJcbiAgICBfZ2V0UmVuZGVyZWRVcmwoKSB7XHJcbiAgICAgICAgbGV0IHVybDtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuYXBwLnR5cGUgPT09ICdqaXRzaScpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJSZXBsYWNpbmcgSml0c2kgd2lkZ2V0IFVSTCB3aXRoIGxvY2FsIHdyYXBwZXJcIik7XHJcbiAgICAgICAgICAgIHVybCA9IFdpZGdldFV0aWxzLmdldExvY2FsSml0c2lXcmFwcGVyVXJsKHtmb3JMb2NhbFJlbmRlcjogdHJ1ZX0pO1xyXG4gICAgICAgICAgICB1cmwgPSB0aGlzLl9hZGRXdXJsUGFyYW1zKHVybCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdXJsID0gdGhpcy5fZ2V0U2FmZVVybCh0aGlzLnN0YXRlLndpZGdldFVybCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl90ZW1wbGF0ZWRVcmwodXJsKTtcclxuICAgIH1cclxuXHJcbiAgICBfZ2V0UG9wb3V0VXJsKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmFwcC50eXBlID09PSAnaml0c2knKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl90ZW1wbGF0ZWRVcmwoXHJcbiAgICAgICAgICAgICAgICBXaWRnZXRVdGlscy5nZXRMb2NhbEppdHNpV3JhcHBlclVybCh7Zm9yTG9jYWxSZW5kZXI6IGZhbHNlfSksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gdXNlIGFwcC51cmwsIG5vdCBzdGF0ZS53aWRnZXRVcmwsIGJlY2F1c2Ugd2Ugd2FudCB0aGUgb25lIHdpdGhvdXRcclxuICAgICAgICAgICAgLy8gdGhlIHdVUkwgcGFyYW1zIGZvciB0aGUgcG9wcGVkLW91dCB2ZXJzaW9uLlxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fdGVtcGxhdGVkVXJsKHRoaXMuX2dldFNhZmVVcmwodGhpcy5wcm9wcy5hcHAudXJsKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9nZXRTYWZlVXJsKHUpIHtcclxuICAgICAgICBjb25zdCBwYXJzZWRXaWRnZXRVcmwgPSB1cmwucGFyc2UodSwgdHJ1ZSk7XHJcbiAgICAgICAgaWYgKEVOQUJMRV9SRUFDVF9QRVJGKSB7XHJcbiAgICAgICAgICAgIHBhcnNlZFdpZGdldFVybC5zZWFyY2ggPSBudWxsO1xyXG4gICAgICAgICAgICBwYXJzZWRXaWRnZXRVcmwucXVlcnkucmVhY3RfcGVyZiA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBzYWZlV2lkZ2V0VXJsID0gJyc7XHJcbiAgICAgICAgaWYgKEFMTE9XRURfQVBQX1VSTF9TQ0hFTUVTLmluY2x1ZGVzKHBhcnNlZFdpZGdldFVybC5wcm90b2NvbCkpIHtcclxuICAgICAgICAgICAgc2FmZVdpZGdldFVybCA9IHVybC5mb3JtYXQocGFyc2VkV2lkZ2V0VXJsKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNhZmVXaWRnZXRVcmw7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldFRpbGVUaXRsZSgpIHtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5mb3JtYXRBcHBUaWxlTmFtZSgpO1xyXG4gICAgICAgIGNvbnN0IHRpdGxlU3BhY2VyID0gPHNwYW4+Jm5ic3A7LSZuYnNwOzwvc3Bhbj47XHJcbiAgICAgICAgbGV0IHRpdGxlID0gJyc7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUud2lkZ2V0UGFnZVRpdGxlICYmIHRoaXMuc3RhdGUud2lkZ2V0UGFnZVRpdGxlICE9IHRoaXMuZm9ybWF0QXBwVGlsZU5hbWUoKSkge1xyXG4gICAgICAgICAgICB0aXRsZSA9IHRoaXMuc3RhdGUud2lkZ2V0UGFnZVRpdGxlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICA8Yj57IG5hbWUgfTwvYj5cclxuICAgICAgICAgICAgICAgIDxzcGFuPnsgdGl0bGUgPyB0aXRsZVNwYWNlciA6ICcnIH17IHRpdGxlIH08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbk1pbmltaXNlQ2xpY2soZSkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uTWluaW1pc2VDbGljaykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uTWluaW1pc2VDbGljaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25Qb3BvdXRXaWRnZXRDbGljaygpIHtcclxuICAgICAgICAvLyBVc2luZyBPYmplY3QuYXNzaWduIHdvcmthcm91bmQgYXMgdGhlIGZvbGxvd2luZyBvcGVucyBpbiBhIG5ldyB3aW5kb3cgaW5zdGVhZCBvZiBhIG5ldyB0YWIuXHJcbiAgICAgICAgLy8gd2luZG93Lm9wZW4odGhpcy5fZ2V0UG9wb3V0VXJsKCksICdfYmxhbmsnLCAnbm9vcGVuZXI9eWVzJyk7XHJcbiAgICAgICAgT2JqZWN0LmFzc2lnbihkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyksXHJcbiAgICAgICAgICAgIHsgdGFyZ2V0OiAnX2JsYW5rJywgaHJlZjogdGhpcy5fZ2V0UG9wb3V0VXJsKCksIHJlbDogJ25vcmVmZXJyZXIgbm9vcGVuZXInfSkuY2xpY2soKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25SZWxvYWRXaWRnZXRDbGljaygpIHtcclxuICAgICAgICAvLyBSZWxvYWQgaWZyYW1lIGluIHRoaXMgd2F5IHRvIGF2b2lkIGNyb3NzLW9yaWdpbiByZXN0cmljdGlvbnNcclxuICAgICAgICB0aGlzLl9hcHBGcmFtZS5jdXJyZW50LnNyYyA9IHRoaXMuX2FwcEZyYW1lLmN1cnJlbnQuc3JjO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNvbnRleHRNZW51Q2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lbnVEaXNwbGF5ZWQ6IHRydWUgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jbG9zZUNvbnRleHRNZW51ID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZW51RGlzcGxheWVkOiBmYWxzZSB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBhcHBUaWxlQm9keTtcclxuXHJcbiAgICAgICAgLy8gRG9uJ3QgcmVuZGVyIHdpZGdldCBpZiBpdCBpcyBpbiB0aGUgcHJvY2VzcyBvZiBiZWluZyBkZWxldGVkXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGVsZXRpbmcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBOb3RlIHRoYXQgdGhlcmUgaXMgYWR2aWNlIHNheWluZyBhbGxvdy1zY3JpcHRzIHNob3VsZG4ndCBiZSB1c2VkIHdpdGggYWxsb3ctc2FtZS1vcmlnaW5cclxuICAgICAgICAvLyBiZWNhdXNlIHRoYXQgd291bGQgYWxsb3cgdGhlIGlmcmFtZSB0byBwcm9ncmFtbWF0aWNhbGx5IHJlbW92ZSB0aGUgc2FuZGJveCBhdHRyaWJ1dGUsIGJ1dFxyXG4gICAgICAgIC8vIHRoaXMgd291bGQgb25seSBiZSBmb3IgY29udGVudCBob3N0ZWQgb24gdGhlIHNhbWUgb3JpZ2luIGFzIHRoZSByaW90IGNsaWVudDogYW55dGhpbmdcclxuICAgICAgICAvLyBob3N0ZWQgb24gdGhlIHNhbWUgb3JpZ2luIGFzIHRoZSBjbGllbnQgd2lsbCBnZXQgdGhlIHNhbWUgYWNjZXNzIGFzIGlmIHlvdSBjbGlja2VkXHJcbiAgICAgICAgLy8gYSBsaW5rIHRvIGl0LlxyXG4gICAgICAgIGNvbnN0IHNhbmRib3hGbGFncyA9IFwiYWxsb3ctZm9ybXMgYWxsb3ctcG9wdXBzIGFsbG93LXBvcHVwcy10by1lc2NhcGUtc2FuZGJveCBcIitcclxuICAgICAgICAgICAgXCJhbGxvdy1zYW1lLW9yaWdpbiBhbGxvdy1zY3JpcHRzIGFsbG93LXByZXNlbnRhdGlvblwiO1xyXG5cclxuICAgICAgICAvLyBBZGRpdGlvbmFsIGlmcmFtZSBmZWF0dXJlIHBlbWlzc2lvbnNcclxuICAgICAgICAvLyAoc2VlIC0gaHR0cHM6Ly9zaXRlcy5nb29nbGUuY29tL2EvY2hyb21pdW0ub3JnL2Rldi9Ib21lL2Nocm9taXVtLXNlY3VyaXR5L2RlcHJlY2F0aW5nLXBlcm1pc3Npb25zLWluLWNyb3NzLW9yaWdpbi1pZnJhbWVzIGFuZCBodHRwczovL3dpY2cuZ2l0aHViLmlvL2ZlYXR1cmUtcG9saWN5LylcclxuICAgICAgICBjb25zdCBpZnJhbWVGZWF0dXJlcyA9IFwibWljcm9waG9uZTsgY2FtZXJhOyBlbmNyeXB0ZWQtbWVkaWE7IGF1dG9wbGF5O1wiO1xyXG5cclxuICAgICAgICBjb25zdCBhcHBUaWxlQm9keUNsYXNzID0gJ214X0FwcFRpbGVCb2R5JyArICh0aGlzLnByb3BzLm1pbmlNb2RlID8gJ19taW5pICAnIDogJyAnKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2hvdykge1xyXG4gICAgICAgICAgICBjb25zdCBsb2FkaW5nRWxlbWVudCA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQXBwTG9hZGluZ19zcGlubmVyX2ZhZGVJblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxNZXNzYWdlU3Bpbm5lciBtc2c9J0xvYWRpbmcuLi4nIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmhhc1Blcm1pc3Npb25Ub0xvYWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlzRW5jcnlwdGVkID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzUm9vbUVuY3J5cHRlZCh0aGlzLnByb3BzLnJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGFwcFRpbGVCb2R5ID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXthcHBUaWxlQm9keUNsYXNzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFwcFBlcm1pc3Npb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvb21JZD17dGhpcy5wcm9wcy5yb29tLnJvb21JZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0b3JVc2VySWQ9e3RoaXMucHJvcHMuY3JlYXRvclVzZXJJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybD17dGhpcy5zdGF0ZS53aWRnZXRVcmx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1Jvb21FbmNyeXB0ZWQ9e2lzRW5jcnlwdGVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25QZXJtaXNzaW9uR3JhbnRlZD17dGhpcy5fZ3JhbnRXaWRnZXRQZXJtaXNzaW9ufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmluaXRpYWxpc2luZykge1xyXG4gICAgICAgICAgICAgICAgYXBwVGlsZUJvZHkgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2FwcFRpbGVCb2R5Q2xhc3MgKyAodGhpcy5zdGF0ZS5sb2FkaW5nID8gJ214X0FwcExvYWRpbmcnIDogJycpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBsb2FkaW5nRWxlbWVudCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNNaXhlZENvbnRlbnQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcFRpbGVCb2R5ID0gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YXBwVGlsZUJvZHlDbGFzc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QXBwV2FybmluZyBlcnJvck1zZz1cIkVycm9yIC0gTWl4ZWQgY29udGVudFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcFRpbGVCb2R5ID0gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YXBwVGlsZUJvZHlDbGFzcyArICh0aGlzLnN0YXRlLmxvYWRpbmcgPyAnbXhfQXBwTG9hZGluZycgOiAnJyl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnN0YXRlLmxvYWRpbmcgJiYgbG9hZGluZ0VsZW1lbnQgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlmcmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsbG93PXtpZnJhbWVGZWF0dXJlc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX2FwcEZyYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYz17dGhpcy5fZ2V0UmVuZGVyZWRVcmwoKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGxvd0Z1bGxTY3JlZW49e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2FuZGJveD17c2FuZGJveEZsYWdzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uTG9hZD17dGhpcy5fb25Mb2FkZWR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIHdpZGdldCB3b3VsZCBiZSBhbGxvd2VkIHRvIHJlbWFpbiBvbiBzY3JlZW4sIHdlIG11c3QgcHV0IGl0IGluXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gYSBQZXJzaXN0ZWRFbGVtZW50IGZyb20gdGhlIGdldC1nbywgb3RoZXJ3aXNlIHRoZSBpZnJhbWUgd2lsbCBiZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlLW1vdW50ZWQgbGF0ZXIgd2hlbiB3ZSBkby5cclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy53aGl0ZWxpc3RDYXBhYmlsaXRpZXMuaW5jbHVkZXMoJ20uYWx3YXlzX29uX3NjcmVlbicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IFBlcnNpc3RlZEVsZW1lbnQgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuUGVyc2lzdGVkRWxlbWVudFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQWxzbyB3cmFwIHRoZSBQZXJzaXN0ZWRFbGVtZW50IGluIGEgZGl2IHRvIGZpeCB0aGUgaGVpZ2h0LCBvdGhlcndpc2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQXBwVGlsZSdzIGJvcmRlciBpcyBpbiB0aGUgd3JvbmcgcGxhY2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXBwVGlsZUJvZHkgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0FwcFRpbGVfcGVyc2lzdGVkV3JhcHBlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFBlcnNpc3RlZEVsZW1lbnQgcGVyc2lzdEtleT17dGhpcy5fcGVyc2lzdEtleX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2FwcFRpbGVCb2R5fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9QZXJzaXN0ZWRFbGVtZW50PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzaG93TWluaW1pc2VCdXR0b24gPSB0aGlzLnByb3BzLnNob3dNaW5pbWlzZSAmJiB0aGlzLnByb3BzLnNob3c7XHJcbiAgICAgICAgY29uc3Qgc2hvd01heGltaXNlQnV0dG9uID0gdGhpcy5wcm9wcy5zaG93TWluaW1pc2UgJiYgIXRoaXMucHJvcHMuc2hvdztcclxuXHJcbiAgICAgICAgbGV0IGFwcFRpbGVDbGFzcztcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5taW5pTW9kZSkge1xyXG4gICAgICAgICAgICBhcHBUaWxlQ2xhc3MgPSAnbXhfQXBwVGlsZV9taW5pJztcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuZnVsbFdpZHRoKSB7XHJcbiAgICAgICAgICAgIGFwcFRpbGVDbGFzcyA9ICdteF9BcHBUaWxlRnVsbFdpZHRoJztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBhcHBUaWxlQ2xhc3MgPSAnbXhfQXBwVGlsZSc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBtZW51QmFyQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICBteF9BcHBUaWxlTWVudUJhcjogdHJ1ZSxcclxuICAgICAgICAgICAgbXhfQXBwVGlsZU1lbnVCYXJfZXhwYW5kZWQ6IHRoaXMucHJvcHMuc2hvdyxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGNvbnRleHRNZW51O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLm1lbnVEaXNwbGF5ZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgZWxlbWVudFJlY3QgPSB0aGlzLl9jb250ZXh0TWVudUJ1dHRvbi5jdXJyZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgY2FuVXNlck1vZGlmeSA9IHRoaXMuX2NhblVzZXJNb2RpZnkoKTtcclxuICAgICAgICAgICAgY29uc3Qgc2hvd0VkaXRCdXR0b24gPSBCb29sZWFuKHRoaXMuX3NjYWxhckNsaWVudCAmJiBjYW5Vc2VyTW9kaWZ5KTtcclxuICAgICAgICAgICAgY29uc3Qgc2hvd0RlbGV0ZUJ1dHRvbiA9ICh0aGlzLnByb3BzLnNob3dEZWxldGUgPT09IHVuZGVmaW5lZCB8fCB0aGlzLnByb3BzLnNob3dEZWxldGUpICYmIGNhblVzZXJNb2RpZnk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNob3dQaWN0dXJlU25hcHNob3RCdXR0b24gPSB0aGlzLl9oYXNDYXBhYmlsaXR5KCdtLmNhcGFiaWxpdHkuc2NyZWVuc2hvdCcpICYmIHRoaXMucHJvcHMuc2hvdztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IFdpZGdldENvbnRleHRNZW51ID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuY29udGV4dF9tZW51cy5XaWRnZXRDb250ZXh0TWVudScpO1xyXG4gICAgICAgICAgICBjb250ZXh0TWVudSA9IChcclxuICAgICAgICAgICAgICAgIDxDb250ZXh0TWVudSB7Li4uYWJvdmVMZWZ0T2YoZWxlbWVudFJlY3QsIG51bGwpfSBvbkZpbmlzaGVkPXt0aGlzLl9jbG9zZUNvbnRleHRNZW51fT5cclxuICAgICAgICAgICAgICAgICAgICA8V2lkZ2V0Q29udGV4dE1lbnVcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25SZXZva2VDbGlja2VkPXt0aGlzLl9vblJldm9rZUNsaWNrZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRWRpdENsaWNrZWQ9e3Nob3dFZGl0QnV0dG9uID8gdGhpcy5fb25FZGl0Q2xpY2sgOiB1bmRlZmluZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRGVsZXRlQ2xpY2tlZD17c2hvd0RlbGV0ZUJ1dHRvbiA/IHRoaXMuX29uRGVsZXRlQ2xpY2sgOiB1bmRlZmluZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uU25hcHNob3RDbGlja2VkPXtzaG93UGljdHVyZVNuYXBzaG90QnV0dG9uID8gdGhpcy5fb25TbmFwc2hvdENsaWNrIDogdW5kZWZpbmVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvblJlbG9hZENsaWNrZWQ9e3RoaXMucHJvcHMuc2hvd1JlbG9hZCA/IHRoaXMuX29uUmVsb2FkV2lkZ2V0Q2xpY2sgOiB1bmRlZmluZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMuX2Nsb3NlQ29udGV4dE1lbnV9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ29udGV4dE1lbnU+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YXBwVGlsZUNsYXNzfSBpZD17dGhpcy5wcm9wcy5hcHAuaWR9PlxyXG4gICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLnNob3dNZW51YmFyICYmXHJcbiAgICAgICAgICAgICAgICA8ZGl2IHJlZj17dGhpcy5fbWVudV9iYXJ9IGNsYXNzTmFtZT17bWVudUJhckNsYXNzZXN9IG9uQ2xpY2s9e3RoaXMub25DbGlja01lbnVCYXJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X0FwcFRpbGVNZW51QmFyVGl0bGVcIiBzdHlsZT17e3BvaW50ZXJFdmVudHM6ICh0aGlzLnByb3BzLmhhbmRsZU1pbmltaXNlUG9pbnRlckV2ZW50cyA/ICdhbGwnIDogZmFsc2UpfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgLyogTWluaW1pc2Ugd2lkZ2V0ICovIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBzaG93TWluaW1pc2VCdXR0b24gJiYgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0FwcFRpbGVNZW51QmFyX2ljb25CdXR0b24gbXhfQXBwVGlsZU1lbnVCYXJfaWNvbkJ1dHRvbl9taW5pbWlzZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17X3QoJ01pbmltaXplIGFwcHMnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uTWluaW1pc2VDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz4gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IC8qIE1heGltaXNlIHdpZGdldCAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgc2hvd01heGltaXNlQnV0dG9uICYmIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BcHBUaWxlTWVudUJhcl9pY29uQnV0dG9uIG14X0FwcFRpbGVNZW51QmFyX2ljb25CdXR0b25fbWF4aW1pc2VcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e190KCdNYXhpbWl6ZSBhcHBzJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbk1pbmltaXNlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyAvKiBUaXRsZSAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5zaG93VGl0bGUgJiYgdGhpcy5fZ2V0VGlsZVRpdGxlKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9BcHBUaWxlTWVudUJhcldpZGdldHNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyAvKiBQb3BvdXQgd2lkZ2V0ICovIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLnNob3dQb3BvdXQgJiYgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0FwcFRpbGVNZW51QmFyX2ljb25CdXR0b24gbXhfQXBwVGlsZU1lbnVCYXJfaWNvbkJ1dHRvbl9wb3BvdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e190KCdQb3BvdXQgd2lkZ2V0Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblBvcG91dFdpZGdldENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgLyogQ29udGV4dCBtZW51ICovIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyA8Q29udGV4dE1lbnVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0FwcFRpbGVNZW51QmFyX2ljb25CdXR0b24gbXhfQXBwVGlsZU1lbnVCYXJfaWNvbkJ1dHRvbl9tZW51XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnTW9yZSBvcHRpb25zJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0V4cGFuZGVkPXt0aGlzLnN0YXRlLm1lbnVEaXNwbGF5ZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dFJlZj17dGhpcy5fY29udGV4dE1lbnVCdXR0b259XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkNvbnRleHRNZW51Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+IH1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj4gfVxyXG4gICAgICAgICAgICAgICAgeyBhcHBUaWxlQm9keSB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgeyBjb250ZXh0TWVudSB9XHJcbiAgICAgICAgPC9SZWFjdC5GcmFnbWVudD47XHJcbiAgICB9XHJcbn1cclxuXHJcbkFwcFRpbGUuZGlzcGxheU5hbWUgPSAnQXBwVGlsZSc7XHJcblxyXG5BcHBUaWxlLnByb3BUeXBlcyA9IHtcclxuICAgIGFwcDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgLy8gU3BlY2lmeWluZyAnZnVsbFdpZHRoJyBhcyB0cnVlIHdpbGwgcmVuZGVyIHRoZSBhcHAgdGlsZSB0byBmaWxsIHRoZSB3aWR0aCBvZiB0aGUgYXBwIGRyYXdlciBjb250aW5lci5cclxuICAgIC8vIFRoaXMgc2hvdWxkIGJlIHNldCB0byB0cnVlIHdoZW4gdGhlcmUgaXMgb25seSBvbmUgd2lkZ2V0IGluIHRoZSBhcHAgZHJhd2VyLCBvdGhlcndpc2UgaXQgc2hvdWxkIGJlIGZhbHNlLlxyXG4gICAgZnVsbFdpZHRoOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIE9wdGlvbmFsLiBJZiBzZXQsIHJlbmRlcnMgYSBzbWFsbGVyIHZpZXcgb2YgdGhlIHdpZGdldFxyXG4gICAgbWluaU1vZGU6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgLy8gVXNlcklkIG9mIHRoZSBjdXJyZW50IHVzZXJcclxuICAgIHVzZXJJZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgLy8gVXNlcklkIG9mIHRoZSBlbnRpdHkgdGhhdCBhZGRlZCAvIG1vZGlmaWVkIHRoZSB3aWRnZXRcclxuICAgIGNyZWF0b3JVc2VySWQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICB3YWl0Rm9ySWZyYW1lTG9hZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBzaG93TWVudWJhcjogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAvLyBTaG91bGQgdGhlIEFwcFRpbGUgcmVuZGVyIGl0c2VsZlxyXG4gICAgc2hvdzogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAvLyBPcHRpb25hbCBvbkVkaXRDbGlja0hhbmRsZXIgKG92ZXJyaWRlcyBkZWZhdWx0IGJlaGF2aW91cilcclxuICAgIG9uRWRpdENsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIC8vIE9wdGlvbmFsIG9uRGVsZXRlQ2xpY2tIYW5kbGVyIChvdmVycmlkZXMgZGVmYXVsdCBiZWhhdmlvdXIpXHJcbiAgICBvbkRlbGV0ZUNsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIC8vIE9wdGlvbmFsIG9uTWluaW1pc2VDbGlja0hhbmRsZXJcclxuICAgIG9uTWluaW1pc2VDbGljazogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAvLyBPcHRpb25hbGx5IGhpZGUgdGhlIHRpbGUgdGl0bGVcclxuICAgIHNob3dUaXRsZTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAvLyBPcHRpb25hbGx5IGhpZGUgdGhlIHRpbGUgbWluaW1pc2UgaWNvblxyXG4gICAgc2hvd01pbmltaXNlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIE9wdGlvbmFsbHkgaGFuZGxlIG1pbmltaXNlIGJ1dHRvbiBwb2ludGVyIGV2ZW50cyAoZGVmYXVsdCBmYWxzZSlcclxuICAgIGhhbmRsZU1pbmltaXNlUG9pbnRlckV2ZW50czogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAvLyBPcHRpb25hbGx5IGhpZGUgdGhlIGRlbGV0ZSBpY29uXHJcbiAgICBzaG93RGVsZXRlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIE9wdGlvbmFsbHkgaGlkZSB0aGUgcG9wb3V0IHdpZGdldCBpY29uXHJcbiAgICBzaG93UG9wb3V0OiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIE9wdGlvbmFsbHkgc2hvdyB0aGUgcmVsb2FkIHdpZGdldCBpY29uXHJcbiAgICAvLyBUaGlzIGlzIG5vdCBjdXJyZW50bHkgaW50ZW5kZWQgZm9yIHVzZSB3aXRoIHByb2R1Y3Rpb24gd2lkZ2V0cy4gSG93ZXZlclxyXG4gICAgLy8gaXQgY2FuIGJlIHVzZWZ1bCB3aGVuIGRldmVsb3BpbmcgcGVyc2lzdGVudCB3aWRnZXRzIGluIG9yZGVyIHRvIGF2b2lkXHJcbiAgICAvLyBoYXZpbmcgdG8gcmVsb2FkIGFsbCBvZiByaW90IHRvIGdldCBuZXcgd2lkZ2V0IGNvbnRlbnQuXHJcbiAgICBzaG93UmVsb2FkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIC8vIFdpZGdldCBjYXBhYmlsaXRpZXMgdG8gYWxsb3cgYnkgZGVmYXVsdCAod2l0aG91dCB1c2VyIGNvbmZpcm1hdGlvbilcclxuICAgIC8vIE5PVEUgLS0gVXNlIHdpdGggY2F1dGlvbi4gVGhpcyBpcyBpbnRlbmRlZCB0byBhaWQgYmV0dGVyIGludGVncmF0aW9uIC8gVVhcclxuICAgIC8vIGJhc2ljIHdpZGdldCBjYXBhYmlsaXRpZXMsIGUuZy4gaW5qZWN0aW5nIHN0aWNrZXIgbWVzc2FnZSBldmVudHMuXHJcbiAgICB3aGl0ZWxpc3RDYXBhYmlsaXRpZXM6IFByb3BUeXBlcy5hcnJheSxcclxuICAgIC8vIE9wdGlvbmFsIGZ1bmN0aW9uIHRvIGJlIGNhbGxlZCBvbiB3aWRnZXQgY2FwYWJpbGl0eSByZXF1ZXN0XHJcbiAgICAvLyBDYWxsZWQgd2l0aCBhbiBhcnJheSBvZiB0aGUgcmVxdWVzdGVkIGNhcGFiaWxpdGllc1xyXG4gICAgb25DYXBhYmlsaXR5UmVxdWVzdDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAvLyBJcyB0aGlzIGFuIGluc3RhbmNlIG9mIGEgdXNlciB3aWRnZXRcclxuICAgIHVzZXJXaWRnZXQ6IFByb3BUeXBlcy5ib29sLFxyXG59O1xyXG5cclxuQXBwVGlsZS5kZWZhdWx0UHJvcHMgPSB7XHJcbiAgICB3YWl0Rm9ySWZyYW1lTG9hZDogdHJ1ZSxcclxuICAgIHNob3dNZW51YmFyOiB0cnVlLFxyXG4gICAgc2hvd1RpdGxlOiB0cnVlLFxyXG4gICAgc2hvd01pbmltaXNlOiB0cnVlLFxyXG4gICAgc2hvd0RlbGV0ZTogdHJ1ZSxcclxuICAgIHNob3dQb3BvdXQ6IHRydWUsXHJcbiAgICBzaG93UmVsb2FkOiBmYWxzZSxcclxuICAgIGhhbmRsZU1pbmltaXNlUG9pbnRlckV2ZW50czogZmFsc2UsXHJcbiAgICB3aGl0ZWxpc3RDYXBhYmlsaXRpZXM6IFtdLFxyXG4gICAgdXNlcldpZGdldDogZmFsc2UsXHJcbiAgICBtaW5pTW9kZTogZmFsc2UsXHJcbn07XHJcbiJdfQ==