"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

/*
Copyright 2017 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'InlineSpinner',
  render: function () {
    const w = this.props.w || 16;
    const h = this.props.h || 16;
    const imgClass = this.props.imgClassName || "";
    return _react.default.createElement("div", {
      className: "mx_InlineSpinner"
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/spinner.gif"),
      width: w,
      height: h,
      className: imgClass
    }));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0lubGluZVNwaW5uZXIuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJyZW5kZXIiLCJ3IiwicHJvcHMiLCJoIiwiaW1nQ2xhc3MiLCJpbWdDbGFzc05hbWUiLCJyZXF1aXJlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBakJBOzs7Ozs7Ozs7Ozs7Ozs7ZUFtQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsZUFEZTtBQUc1QkMsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxDQUFDLEdBQUcsS0FBS0MsS0FBTCxDQUFXRCxDQUFYLElBQWdCLEVBQTFCO0FBQ0EsVUFBTUUsQ0FBQyxHQUFHLEtBQUtELEtBQUwsQ0FBV0MsQ0FBWCxJQUFnQixFQUExQjtBQUNBLFVBQU1DLFFBQVEsR0FBRyxLQUFLRixLQUFMLENBQVdHLFlBQVgsSUFBMkIsRUFBNUM7QUFFQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxHQUFHLEVBQUVDLE9BQU8sQ0FBQyxpQ0FBRCxDQUFqQjtBQUFzRCxNQUFBLEtBQUssRUFBRUwsQ0FBN0Q7QUFBZ0UsTUFBQSxNQUFNLEVBQUVFLENBQXhFO0FBQTJFLE1BQUEsU0FBUyxFQUFFQztBQUF0RixNQURKLENBREo7QUFLSDtBQWIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGQuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0lubGluZVNwaW5uZXInLFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgdyA9IHRoaXMucHJvcHMudyB8fCAxNjtcclxuICAgICAgICBjb25zdCBoID0gdGhpcy5wcm9wcy5oIHx8IDE2O1xyXG4gICAgICAgIGNvbnN0IGltZ0NsYXNzID0gdGhpcy5wcm9wcy5pbWdDbGFzc05hbWUgfHwgXCJcIjtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbmxpbmVTcGlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvc3Bpbm5lci5naWZcIil9IHdpZHRoPXt3fSBoZWlnaHQ9e2h9IGNsYXNzTmFtZT17aW1nQ2xhc3N9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19