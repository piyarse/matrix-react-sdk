"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _PlatformPeg = _interopRequireDefault(require("../../../PlatformPeg"));

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const SSOButton = (_ref) => {
  let {
    matrixClient,
    loginType
  } = _ref,
      props = (0, _objectWithoutProperties2.default)(_ref, ["matrixClient", "loginType"]);

  const onClick = () => {
    _PlatformPeg.default.get().startSingleSignOn(matrixClient, loginType);
  };

  return _react.default.createElement(_AccessibleButton.default, (0, _extends2.default)({}, props, {
    kind: "primary",
    onClick: onClick
  }), (0, _languageHandler._t)("Sign in with single sign-on"));
};

SSOButton.propTypes = {
  matrixClient: _propTypes.default.object.isRequired,
  // does not use context as may use a temporary client
  loginType: _propTypes.default.oneOf(["sso", "cas"]) // defaults to "sso" in base-apis

};
var _default = SSOButton;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1NTT0J1dHRvbi5qcyJdLCJuYW1lcyI6WyJTU09CdXR0b24iLCJtYXRyaXhDbGllbnQiLCJsb2dpblR5cGUiLCJwcm9wcyIsIm9uQ2xpY2siLCJQbGF0Zm9ybVBlZyIsImdldCIsInN0YXJ0U2luZ2xlU2lnbk9uIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm9uZU9mIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBLE1BQU1BLFNBQVMsR0FBRyxVQUF5QztBQUFBLE1BQXhDO0FBQUNDLElBQUFBLFlBQUQ7QUFBZUMsSUFBQUE7QUFBZixHQUF3QztBQUFBLE1BQVhDLEtBQVc7O0FBQ3ZELFFBQU1DLE9BQU8sR0FBRyxNQUFNO0FBQ2xCQyx5QkFBWUMsR0FBWixHQUFrQkMsaUJBQWxCLENBQW9DTixZQUFwQyxFQUFrREMsU0FBbEQ7QUFDSCxHQUZEOztBQUlBLFNBQ0ksNkJBQUMseUJBQUQsNkJBQXNCQyxLQUF0QjtBQUE2QixJQUFBLElBQUksRUFBQyxTQUFsQztBQUE0QyxJQUFBLE9BQU8sRUFBRUM7QUFBckQsTUFDSyx5QkFBRyw2QkFBSCxDQURMLENBREo7QUFLSCxDQVZEOztBQVlBSixTQUFTLENBQUNRLFNBQVYsR0FBc0I7QUFDbEJQLEVBQUFBLFlBQVksRUFBRVEsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRGI7QUFDeUI7QUFDM0NULEVBQUFBLFNBQVMsRUFBRU8sbUJBQVVHLEtBQVYsQ0FBZ0IsQ0FBQyxLQUFELEVBQVEsS0FBUixDQUFoQixDQUZPLENBRTBCOztBQUYxQixDQUF0QjtlQUtlWixTIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5cclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gXCIuLi8uLi8uLi9QbGF0Zm9ybVBlZ1wiO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tIFwiLi9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCB7X3R9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuXHJcbmNvbnN0IFNTT0J1dHRvbiA9ICh7bWF0cml4Q2xpZW50LCBsb2dpblR5cGUsIC4uLnByb3BzfSkgPT4ge1xyXG4gICAgY29uc3Qgb25DbGljayA9ICgpID0+IHtcclxuICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5zdGFydFNpbmdsZVNpZ25PbihtYXRyaXhDbGllbnQsIGxvZ2luVHlwZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gey4uLnByb3BzfSBraW5kPVwicHJpbWFyeVwiIG9uQ2xpY2s9e29uQ2xpY2t9PlxyXG4gICAgICAgICAgICB7X3QoXCJTaWduIGluIHdpdGggc2luZ2xlIHNpZ24tb25cIil9XHJcbiAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgKTtcclxufTtcclxuXHJcblNTT0J1dHRvbi5wcm9wVHlwZXMgPSB7XHJcbiAgICBtYXRyaXhDbGllbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCwgLy8gZG9lcyBub3QgdXNlIGNvbnRleHQgYXMgbWF5IHVzZSBhIHRlbXBvcmFyeSBjbGllbnRcclxuICAgIGxvZ2luVHlwZTogUHJvcFR5cGVzLm9uZU9mKFtcInNzb1wiLCBcImNhc1wiXSksIC8vIGRlZmF1bHRzIHRvIFwic3NvXCIgaW4gYmFzZS1hcGlzXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTU09CdXR0b247XHJcbiJdfQ==