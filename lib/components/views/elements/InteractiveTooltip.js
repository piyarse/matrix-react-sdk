"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

const InteractiveTooltipContainerId = "mx_InteractiveTooltip_Container"; // If the distance from tooltip to window edge is below this value, the tooltip
// will flip around to the other side of the target.

const MIN_SAFE_DISTANCE_TO_WINDOW_EDGE = 20;

function getOrCreateContainer() {
  let container = document.getElementById(InteractiveTooltipContainerId);

  if (!container) {
    container = document.createElement("div");
    container.id = InteractiveTooltipContainerId;
    document.body.appendChild(container);
  }

  return container;
}

function isInRect(x, y, rect) {
  const {
    top,
    right,
    bottom,
    left
  } = rect;
  return x >= left && x <= right && y >= top && y <= bottom;
}
/**
 * Returns the positive slope of the diagonal of the rect.
 *
 * @param {DOMRect} rect
 * @return {integer}
 */


function getDiagonalSlope(rect) {
  const {
    top,
    right,
    bottom,
    left
  } = rect;
  return (bottom - top) / (right - left);
}

function isInUpperLeftHalf(x, y, rect) {
  const {
    bottom,
    left
  } = rect; // Negative slope because Y values grow downwards and for this case, the
  // diagonal goes from larger to smaller Y values.

  const diagonalSlope = getDiagonalSlope(rect) * -1;
  return isInRect(x, y, rect) && y <= bottom + diagonalSlope * (x - left);
}

function isInLowerRightHalf(x, y, rect) {
  const {
    bottom,
    left
  } = rect; // Negative slope because Y values grow downwards and for this case, the
  // diagonal goes from larger to smaller Y values.

  const diagonalSlope = getDiagonalSlope(rect) * -1;
  return isInRect(x, y, rect) && y >= bottom + diagonalSlope * (x - left);
}

function isInUpperRightHalf(x, y, rect) {
  const {
    top,
    left
  } = rect; // Positive slope because Y values grow downwards and for this case, the
  // diagonal goes from smaller to larger Y values.

  const diagonalSlope = getDiagonalSlope(rect) * 1;
  return isInRect(x, y, rect) && y <= top + diagonalSlope * (x - left);
}

function isInLowerLeftHalf(x, y, rect) {
  const {
    top,
    left
  } = rect; // Positive slope because Y values grow downwards and for this case, the
  // diagonal goes from smaller to larger Y values.

  const diagonalSlope = getDiagonalSlope(rect) * 1;
  return isInRect(x, y, rect) && y >= top + diagonalSlope * (x - left);
}
/*
 * This style of tooltip takes a "target" element as its child and centers the
 * tooltip along one edge of the target.
 */


class InteractiveTooltip extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "collectContentRect", element => {
      // We don't need to clean up when unmounting, so ignore
      if (!element) return;
      this.setState({
        contentRect: element.getBoundingClientRect()
      });
    });
    (0, _defineProperty2.default)(this, "collectTarget", element => {
      this.target = element;
    });
    (0, _defineProperty2.default)(this, "onMouseMove", ev => {
      const {
        clientX: x,
        clientY: y
      } = ev;
      const {
        contentRect
      } = this.state;
      const targetRect = this.target.getBoundingClientRect(); // When moving the mouse from the target to the tooltip, we create a
      // safe area that includes the tooltip, the target, and the trapezoid
      // ABCD between them:
      //                            ┌───────────┐
      //                            │           │
      //                            │           │
      //                          A └───E───F───┘ B
      //                                  V
      //                                 ┌─┐
      //                                 │ │
      //                                C└─┘D
      //
      // As long as the mouse remains inside the safe area, the tooltip will
      // stay open.

      const buffer = 50;

      if (isInRect(x, y, targetRect)) {
        return;
      }

      if (this.canTooltipFitAboveTarget()) {
        const contentRectWithBuffer = {
          top: contentRect.top - buffer,
          right: contentRect.right + buffer,
          bottom: contentRect.bottom,
          left: contentRect.left - buffer
        };
        const trapezoidLeft = {
          top: contentRect.bottom,
          right: targetRect.left,
          bottom: targetRect.bottom,
          left: contentRect.left - buffer
        };
        const trapezoidCenter = {
          top: contentRect.bottom,
          right: targetRect.right,
          bottom: targetRect.bottom,
          left: targetRect.left
        };
        const trapezoidRight = {
          top: contentRect.bottom,
          right: contentRect.right + buffer,
          bottom: targetRect.bottom,
          left: targetRect.right
        };

        if (isInRect(x, y, contentRectWithBuffer) || isInUpperRightHalf(x, y, trapezoidLeft) || isInRect(x, y, trapezoidCenter) || isInUpperLeftHalf(x, y, trapezoidRight)) {
          return;
        }
      } else {
        const contentRectWithBuffer = {
          top: contentRect.top,
          right: contentRect.right + buffer,
          bottom: contentRect.bottom + buffer,
          left: contentRect.left - buffer
        };
        const trapezoidLeft = {
          top: targetRect.top,
          right: targetRect.left,
          bottom: contentRect.top,
          left: contentRect.left - buffer
        };
        const trapezoidCenter = {
          top: targetRect.top,
          right: targetRect.right,
          bottom: contentRect.top,
          left: targetRect.left
        };
        const trapezoidRight = {
          top: targetRect.top,
          right: contentRect.right + buffer,
          bottom: contentRect.top,
          left: targetRect.right
        };

        if (isInRect(x, y, contentRectWithBuffer) || isInLowerRightHalf(x, y, trapezoidLeft) || isInRect(x, y, trapezoidCenter) || isInLowerLeftHalf(x, y, trapezoidRight)) {
          return;
        }
      }

      this.hideTooltip();
    });
    (0, _defineProperty2.default)(this, "onTargetMouseOver", ev => {
      this.showTooltip();
    });
    this.state = {
      contentRect: null,
      visible: false
    };
  }

  componentDidUpdate() {
    // Whenever this passthrough component updates, also render the tooltip
    // in a separate DOM tree. This allows the tooltip content to participate
    // the normal React rendering cycle: when this component re-renders, the
    // tooltip content re-renders.
    // Once we upgrade to React 16, this could be done a bit more naturally
    // using the portals feature instead.
    this.renderTooltip();
  }

  componentWillUnmount() {
    document.removeEventListener("mousemove", this.onMouseMove);
  }

  canTooltipFitAboveTarget() {
    const {
      contentRect
    } = this.state;
    const targetRect = this.target.getBoundingClientRect();
    const targetTop = targetRect.top + window.pageYOffset;
    return !contentRect || targetTop - contentRect.height > MIN_SAFE_DISTANCE_TO_WINDOW_EDGE;
  }

  showTooltip() {
    // Don't enter visible state if we haven't collected the target yet
    if (!this.target) {
      return;
    }

    this.setState({
      visible: true
    });

    if (this.props.onVisibilityChange) {
      this.props.onVisibilityChange(true);
    }

    document.addEventListener("mousemove", this.onMouseMove);
  }

  hideTooltip() {
    this.setState({
      visible: false
    });

    if (this.props.onVisibilityChange) {
      this.props.onVisibilityChange(false);
    }

    document.removeEventListener("mousemove", this.onMouseMove);
  }

  renderTooltip() {
    const {
      contentRect,
      visible
    } = this.state;

    if (this.props.forceHidden === true || !visible) {
      _reactDom.default.render(null, getOrCreateContainer());

      return null;
    }

    const targetRect = this.target.getBoundingClientRect(); // The window X and Y offsets are to adjust position when zoomed in to page

    const targetLeft = targetRect.left + window.pageXOffset;
    const targetBottom = targetRect.bottom + window.pageYOffset;
    const targetTop = targetRect.top + window.pageYOffset; // Place the tooltip above the target by default. If we find that the
    // tooltip content would extend past the safe area towards the window
    // edge, flip around to below the target.

    const position = {};
    let chevronFace = null;

    if (this.canTooltipFitAboveTarget()) {
      position.bottom = window.innerHeight - targetTop;
      chevronFace = "bottom";
    } else {
      position.top = targetBottom;
      chevronFace = "top";
    } // Center the tooltip horizontally with the target's center.


    position.left = targetLeft + targetRect.width / 2;

    const chevron = _react.default.createElement("div", {
      className: "mx_InteractiveTooltip_chevron_" + chevronFace
    });

    const menuClasses = (0, _classnames.default)({
      'mx_InteractiveTooltip': true,
      'mx_InteractiveTooltip_withChevron_top': chevronFace === 'top',
      'mx_InteractiveTooltip_withChevron_bottom': chevronFace === 'bottom'
    });
    const menuStyle = {};

    if (contentRect) {
      menuStyle.left = "-".concat(contentRect.width / 2, "px");
    }

    const tooltip = _react.default.createElement("div", {
      className: "mx_InteractiveTooltip_wrapper",
      style: _objectSpread({}, position)
    }, _react.default.createElement("div", {
      className: menuClasses,
      style: menuStyle,
      ref: this.collectContentRect
    }, chevron, this.props.content));

    _reactDom.default.render(tooltip, getOrCreateContainer());
  }

  render() {
    // We use `cloneElement` here to append some props to the child content
    // without using a wrapper element which could disrupt layout.
    return _react.default.cloneElement(this.props.children, {
      ref: this.collectTarget,
      onMouseOver: this.onTargetMouseOver
    });
  }

}

exports.default = InteractiveTooltip;
(0, _defineProperty2.default)(InteractiveTooltip, "propTypes", {
  // Content to show in the tooltip
  content: _propTypes.default.node.isRequired,
  // Function to call when visibility of the tooltip changes
  onVisibilityChange: _propTypes.default.func,
  // flag to forcefully hide this tooltip
  forceHidden: _propTypes.default.bool
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0ludGVyYWN0aXZlVG9vbHRpcC5qcyJdLCJuYW1lcyI6WyJJbnRlcmFjdGl2ZVRvb2x0aXBDb250YWluZXJJZCIsIk1JTl9TQUZFX0RJU1RBTkNFX1RPX1dJTkRPV19FREdFIiwiZ2V0T3JDcmVhdGVDb250YWluZXIiLCJjb250YWluZXIiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwiY3JlYXRlRWxlbWVudCIsImlkIiwiYm9keSIsImFwcGVuZENoaWxkIiwiaXNJblJlY3QiLCJ4IiwieSIsInJlY3QiLCJ0b3AiLCJyaWdodCIsImJvdHRvbSIsImxlZnQiLCJnZXREaWFnb25hbFNsb3BlIiwiaXNJblVwcGVyTGVmdEhhbGYiLCJkaWFnb25hbFNsb3BlIiwiaXNJbkxvd2VyUmlnaHRIYWxmIiwiaXNJblVwcGVyUmlnaHRIYWxmIiwiaXNJbkxvd2VyTGVmdEhhbGYiLCJJbnRlcmFjdGl2ZVRvb2x0aXAiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiZWxlbWVudCIsInNldFN0YXRlIiwiY29udGVudFJlY3QiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJ0YXJnZXQiLCJldiIsImNsaWVudFgiLCJjbGllbnRZIiwic3RhdGUiLCJ0YXJnZXRSZWN0IiwiYnVmZmVyIiwiY2FuVG9vbHRpcEZpdEFib3ZlVGFyZ2V0IiwiY29udGVudFJlY3RXaXRoQnVmZmVyIiwidHJhcGV6b2lkTGVmdCIsInRyYXBlem9pZENlbnRlciIsInRyYXBlem9pZFJpZ2h0IiwiaGlkZVRvb2x0aXAiLCJzaG93VG9vbHRpcCIsInZpc2libGUiLCJjb21wb25lbnREaWRVcGRhdGUiLCJyZW5kZXJUb29sdGlwIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwib25Nb3VzZU1vdmUiLCJ0YXJnZXRUb3AiLCJ3aW5kb3ciLCJwYWdlWU9mZnNldCIsImhlaWdodCIsInByb3BzIiwib25WaXNpYmlsaXR5Q2hhbmdlIiwiYWRkRXZlbnRMaXN0ZW5lciIsImZvcmNlSGlkZGVuIiwiUmVhY3RET00iLCJyZW5kZXIiLCJ0YXJnZXRMZWZ0IiwicGFnZVhPZmZzZXQiLCJ0YXJnZXRCb3R0b20iLCJwb3NpdGlvbiIsImNoZXZyb25GYWNlIiwiaW5uZXJIZWlnaHQiLCJ3aWR0aCIsImNoZXZyb24iLCJtZW51Q2xhc3NlcyIsIm1lbnVTdHlsZSIsInRvb2x0aXAiLCJjb2xsZWN0Q29udGVudFJlY3QiLCJjb250ZW50IiwiY2xvbmVFbGVtZW50IiwiY2hpbGRyZW4iLCJyZWYiLCJjb2xsZWN0VGFyZ2V0Iiwib25Nb3VzZU92ZXIiLCJvblRhcmdldE1vdXNlT3ZlciIsIlByb3BUeXBlcyIsIm5vZGUiLCJpc1JlcXVpcmVkIiwiZnVuYyIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxNQUFNQSw2QkFBNkIsR0FBRyxpQ0FBdEMsQyxDQUVBO0FBQ0E7O0FBQ0EsTUFBTUMsZ0NBQWdDLEdBQUcsRUFBekM7O0FBRUEsU0FBU0Msb0JBQVQsR0FBZ0M7QUFDNUIsTUFBSUMsU0FBUyxHQUFHQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0JMLDZCQUF4QixDQUFoQjs7QUFFQSxNQUFJLENBQUNHLFNBQUwsRUFBZ0I7QUFDWkEsSUFBQUEsU0FBUyxHQUFHQyxRQUFRLENBQUNFLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWjtBQUNBSCxJQUFBQSxTQUFTLENBQUNJLEVBQVYsR0FBZVAsNkJBQWY7QUFDQUksSUFBQUEsUUFBUSxDQUFDSSxJQUFULENBQWNDLFdBQWQsQ0FBMEJOLFNBQTFCO0FBQ0g7O0FBRUQsU0FBT0EsU0FBUDtBQUNIOztBQUVELFNBQVNPLFFBQVQsQ0FBa0JDLENBQWxCLEVBQXFCQyxDQUFyQixFQUF3QkMsSUFBeEIsRUFBOEI7QUFDMUIsUUFBTTtBQUFFQyxJQUFBQSxHQUFGO0FBQU9DLElBQUFBLEtBQVA7QUFBY0MsSUFBQUEsTUFBZDtBQUFzQkMsSUFBQUE7QUFBdEIsTUFBK0JKLElBQXJDO0FBQ0EsU0FBT0YsQ0FBQyxJQUFJTSxJQUFMLElBQWFOLENBQUMsSUFBSUksS0FBbEIsSUFBMkJILENBQUMsSUFBSUUsR0FBaEMsSUFBdUNGLENBQUMsSUFBSUksTUFBbkQ7QUFDSDtBQUVEOzs7Ozs7OztBQU1BLFNBQVNFLGdCQUFULENBQTBCTCxJQUExQixFQUFnQztBQUM1QixRQUFNO0FBQUVDLElBQUFBLEdBQUY7QUFBT0MsSUFBQUEsS0FBUDtBQUFjQyxJQUFBQSxNQUFkO0FBQXNCQyxJQUFBQTtBQUF0QixNQUErQkosSUFBckM7QUFDQSxTQUFPLENBQUNHLE1BQU0sR0FBR0YsR0FBVixLQUFrQkMsS0FBSyxHQUFHRSxJQUExQixDQUFQO0FBQ0g7O0FBRUQsU0FBU0UsaUJBQVQsQ0FBMkJSLENBQTNCLEVBQThCQyxDQUE5QixFQUFpQ0MsSUFBakMsRUFBdUM7QUFDbkMsUUFBTTtBQUFFRyxJQUFBQSxNQUFGO0FBQVVDLElBQUFBO0FBQVYsTUFBbUJKLElBQXpCLENBRG1DLENBRW5DO0FBQ0E7O0FBQ0EsUUFBTU8sYUFBYSxHQUFHRixnQkFBZ0IsQ0FBQ0wsSUFBRCxDQUFoQixHQUF5QixDQUFDLENBQWhEO0FBQ0EsU0FBT0gsUUFBUSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBT0MsSUFBUCxDQUFSLElBQXlCRCxDQUFDLElBQUlJLE1BQU0sR0FBR0ksYUFBYSxJQUFJVCxDQUFDLEdBQUdNLElBQVIsQ0FBM0Q7QUFDSDs7QUFFRCxTQUFTSSxrQkFBVCxDQUE0QlYsQ0FBNUIsRUFBK0JDLENBQS9CLEVBQWtDQyxJQUFsQyxFQUF3QztBQUNwQyxRQUFNO0FBQUVHLElBQUFBLE1BQUY7QUFBVUMsSUFBQUE7QUFBVixNQUFtQkosSUFBekIsQ0FEb0MsQ0FFcEM7QUFDQTs7QUFDQSxRQUFNTyxhQUFhLEdBQUdGLGdCQUFnQixDQUFDTCxJQUFELENBQWhCLEdBQXlCLENBQUMsQ0FBaEQ7QUFDQSxTQUFPSCxRQUFRLENBQUNDLENBQUQsRUFBSUMsQ0FBSixFQUFPQyxJQUFQLENBQVIsSUFBeUJELENBQUMsSUFBSUksTUFBTSxHQUFHSSxhQUFhLElBQUlULENBQUMsR0FBR00sSUFBUixDQUEzRDtBQUNIOztBQUVELFNBQVNLLGtCQUFULENBQTRCWCxDQUE1QixFQUErQkMsQ0FBL0IsRUFBa0NDLElBQWxDLEVBQXdDO0FBQ3BDLFFBQU07QUFBRUMsSUFBQUEsR0FBRjtBQUFPRyxJQUFBQTtBQUFQLE1BQWdCSixJQUF0QixDQURvQyxDQUVwQztBQUNBOztBQUNBLFFBQU1PLGFBQWEsR0FBR0YsZ0JBQWdCLENBQUNMLElBQUQsQ0FBaEIsR0FBeUIsQ0FBL0M7QUFDQSxTQUFPSCxRQUFRLENBQUNDLENBQUQsRUFBSUMsQ0FBSixFQUFPQyxJQUFQLENBQVIsSUFBeUJELENBQUMsSUFBSUUsR0FBRyxHQUFHTSxhQUFhLElBQUlULENBQUMsR0FBR00sSUFBUixDQUF4RDtBQUNIOztBQUVELFNBQVNNLGlCQUFULENBQTJCWixDQUEzQixFQUE4QkMsQ0FBOUIsRUFBaUNDLElBQWpDLEVBQXVDO0FBQ25DLFFBQU07QUFBRUMsSUFBQUEsR0FBRjtBQUFPRyxJQUFBQTtBQUFQLE1BQWdCSixJQUF0QixDQURtQyxDQUVuQztBQUNBOztBQUNBLFFBQU1PLGFBQWEsR0FBR0YsZ0JBQWdCLENBQUNMLElBQUQsQ0FBaEIsR0FBeUIsQ0FBL0M7QUFDQSxTQUFPSCxRQUFRLENBQUNDLENBQUQsRUFBSUMsQ0FBSixFQUFPQyxJQUFQLENBQVIsSUFBeUJELENBQUMsSUFBSUUsR0FBRyxHQUFHTSxhQUFhLElBQUlULENBQUMsR0FBR00sSUFBUixDQUF4RDtBQUNIO0FBRUQ7Ozs7OztBQUllLE1BQU1PLGtCQUFOLFNBQWlDQyxlQUFNQyxTQUF2QyxDQUFpRDtBQVU1REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSw4REF1QlFDLE9BQUQsSUFBYTtBQUM5QjtBQUNBLFVBQUksQ0FBQ0EsT0FBTCxFQUFjO0FBRWQsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLFdBQVcsRUFBRUYsT0FBTyxDQUFDRyxxQkFBUjtBQURILE9BQWQ7QUFHSCxLQTlCYTtBQUFBLHlEQWdDR0gsT0FBRCxJQUFhO0FBQ3pCLFdBQUtJLE1BQUwsR0FBY0osT0FBZDtBQUNILEtBbENhO0FBQUEsdURBOENDSyxFQUFELElBQVE7QUFDbEIsWUFBTTtBQUFFQyxRQUFBQSxPQUFPLEVBQUV2QixDQUFYO0FBQWN3QixRQUFBQSxPQUFPLEVBQUV2QjtBQUF2QixVQUE2QnFCLEVBQW5DO0FBQ0EsWUFBTTtBQUFFSCxRQUFBQTtBQUFGLFVBQWtCLEtBQUtNLEtBQTdCO0FBQ0EsWUFBTUMsVUFBVSxHQUFHLEtBQUtMLE1BQUwsQ0FBWUQscUJBQVosRUFBbkIsQ0FIa0IsQ0FLbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxZQUFNTyxNQUFNLEdBQUcsRUFBZjs7QUFDQSxVQUFJNUIsUUFBUSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBT3lCLFVBQVAsQ0FBWixFQUFnQztBQUM1QjtBQUNIOztBQUNELFVBQUksS0FBS0Usd0JBQUwsRUFBSixFQUFxQztBQUNqQyxjQUFNQyxxQkFBcUIsR0FBRztBQUMxQjFCLFVBQUFBLEdBQUcsRUFBRWdCLFdBQVcsQ0FBQ2hCLEdBQVosR0FBa0J3QixNQURHO0FBRTFCdkIsVUFBQUEsS0FBSyxFQUFFZSxXQUFXLENBQUNmLEtBQVosR0FBb0J1QixNQUZEO0FBRzFCdEIsVUFBQUEsTUFBTSxFQUFFYyxXQUFXLENBQUNkLE1BSE07QUFJMUJDLFVBQUFBLElBQUksRUFBRWEsV0FBVyxDQUFDYixJQUFaLEdBQW1CcUI7QUFKQyxTQUE5QjtBQU1BLGNBQU1HLGFBQWEsR0FBRztBQUNsQjNCLFVBQUFBLEdBQUcsRUFBRWdCLFdBQVcsQ0FBQ2QsTUFEQztBQUVsQkQsVUFBQUEsS0FBSyxFQUFFc0IsVUFBVSxDQUFDcEIsSUFGQTtBQUdsQkQsVUFBQUEsTUFBTSxFQUFFcUIsVUFBVSxDQUFDckIsTUFIRDtBQUlsQkMsVUFBQUEsSUFBSSxFQUFFYSxXQUFXLENBQUNiLElBQVosR0FBbUJxQjtBQUpQLFNBQXRCO0FBTUEsY0FBTUksZUFBZSxHQUFHO0FBQ3BCNUIsVUFBQUEsR0FBRyxFQUFFZ0IsV0FBVyxDQUFDZCxNQURHO0FBRXBCRCxVQUFBQSxLQUFLLEVBQUVzQixVQUFVLENBQUN0QixLQUZFO0FBR3BCQyxVQUFBQSxNQUFNLEVBQUVxQixVQUFVLENBQUNyQixNQUhDO0FBSXBCQyxVQUFBQSxJQUFJLEVBQUVvQixVQUFVLENBQUNwQjtBQUpHLFNBQXhCO0FBTUEsY0FBTTBCLGNBQWMsR0FBRztBQUNuQjdCLFVBQUFBLEdBQUcsRUFBRWdCLFdBQVcsQ0FBQ2QsTUFERTtBQUVuQkQsVUFBQUEsS0FBSyxFQUFFZSxXQUFXLENBQUNmLEtBQVosR0FBb0J1QixNQUZSO0FBR25CdEIsVUFBQUEsTUFBTSxFQUFFcUIsVUFBVSxDQUFDckIsTUFIQTtBQUluQkMsVUFBQUEsSUFBSSxFQUFFb0IsVUFBVSxDQUFDdEI7QUFKRSxTQUF2Qjs7QUFPQSxZQUNJTCxRQUFRLENBQUNDLENBQUQsRUFBSUMsQ0FBSixFQUFPNEIscUJBQVAsQ0FBUixJQUNBbEIsa0JBQWtCLENBQUNYLENBQUQsRUFBSUMsQ0FBSixFQUFPNkIsYUFBUCxDQURsQixJQUVBL0IsUUFBUSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBTzhCLGVBQVAsQ0FGUixJQUdBdkIsaUJBQWlCLENBQUNSLENBQUQsRUFBSUMsQ0FBSixFQUFPK0IsY0FBUCxDQUpyQixFQUtFO0FBQ0U7QUFDSDtBQUNKLE9BbENELE1Ba0NPO0FBQ0gsY0FBTUgscUJBQXFCLEdBQUc7QUFDMUIxQixVQUFBQSxHQUFHLEVBQUVnQixXQUFXLENBQUNoQixHQURTO0FBRTFCQyxVQUFBQSxLQUFLLEVBQUVlLFdBQVcsQ0FBQ2YsS0FBWixHQUFvQnVCLE1BRkQ7QUFHMUJ0QixVQUFBQSxNQUFNLEVBQUVjLFdBQVcsQ0FBQ2QsTUFBWixHQUFxQnNCLE1BSEg7QUFJMUJyQixVQUFBQSxJQUFJLEVBQUVhLFdBQVcsQ0FBQ2IsSUFBWixHQUFtQnFCO0FBSkMsU0FBOUI7QUFNQSxjQUFNRyxhQUFhLEdBQUc7QUFDbEIzQixVQUFBQSxHQUFHLEVBQUV1QixVQUFVLENBQUN2QixHQURFO0FBRWxCQyxVQUFBQSxLQUFLLEVBQUVzQixVQUFVLENBQUNwQixJQUZBO0FBR2xCRCxVQUFBQSxNQUFNLEVBQUVjLFdBQVcsQ0FBQ2hCLEdBSEY7QUFJbEJHLFVBQUFBLElBQUksRUFBRWEsV0FBVyxDQUFDYixJQUFaLEdBQW1CcUI7QUFKUCxTQUF0QjtBQU1BLGNBQU1JLGVBQWUsR0FBRztBQUNwQjVCLFVBQUFBLEdBQUcsRUFBRXVCLFVBQVUsQ0FBQ3ZCLEdBREk7QUFFcEJDLFVBQUFBLEtBQUssRUFBRXNCLFVBQVUsQ0FBQ3RCLEtBRkU7QUFHcEJDLFVBQUFBLE1BQU0sRUFBRWMsV0FBVyxDQUFDaEIsR0FIQTtBQUlwQkcsVUFBQUEsSUFBSSxFQUFFb0IsVUFBVSxDQUFDcEI7QUFKRyxTQUF4QjtBQU1BLGNBQU0wQixjQUFjLEdBQUc7QUFDbkI3QixVQUFBQSxHQUFHLEVBQUV1QixVQUFVLENBQUN2QixHQURHO0FBRW5CQyxVQUFBQSxLQUFLLEVBQUVlLFdBQVcsQ0FBQ2YsS0FBWixHQUFvQnVCLE1BRlI7QUFHbkJ0QixVQUFBQSxNQUFNLEVBQUVjLFdBQVcsQ0FBQ2hCLEdBSEQ7QUFJbkJHLFVBQUFBLElBQUksRUFBRW9CLFVBQVUsQ0FBQ3RCO0FBSkUsU0FBdkI7O0FBT0EsWUFDSUwsUUFBUSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBTzRCLHFCQUFQLENBQVIsSUFDQW5CLGtCQUFrQixDQUFDVixDQUFELEVBQUlDLENBQUosRUFBTzZCLGFBQVAsQ0FEbEIsSUFFQS9CLFFBQVEsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEVBQU84QixlQUFQLENBRlIsSUFHQW5CLGlCQUFpQixDQUFDWixDQUFELEVBQUlDLENBQUosRUFBTytCLGNBQVAsQ0FKckIsRUFLRTtBQUNFO0FBQ0g7QUFDSjs7QUFFRCxXQUFLQyxXQUFMO0FBQ0gsS0E1SWE7QUFBQSw2REE4SU9YLEVBQUQsSUFBUTtBQUN4QixXQUFLWSxXQUFMO0FBQ0gsS0FoSmE7QUFHVixTQUFLVCxLQUFMLEdBQWE7QUFDVE4sTUFBQUEsV0FBVyxFQUFFLElBREo7QUFFVGdCLE1BQUFBLE9BQU8sRUFBRTtBQUZBLEtBQWI7QUFJSDs7QUFFREMsRUFBQUEsa0JBQWtCLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBS0MsYUFBTDtBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQjdDLElBQUFBLFFBQVEsQ0FBQzhDLG1CQUFULENBQTZCLFdBQTdCLEVBQTBDLEtBQUtDLFdBQS9DO0FBQ0g7O0FBZURaLEVBQUFBLHdCQUF3QixHQUFHO0FBQ3ZCLFVBQU07QUFBRVQsTUFBQUE7QUFBRixRQUFrQixLQUFLTSxLQUE3QjtBQUNBLFVBQU1DLFVBQVUsR0FBRyxLQUFLTCxNQUFMLENBQVlELHFCQUFaLEVBQW5CO0FBQ0EsVUFBTXFCLFNBQVMsR0FBR2YsVUFBVSxDQUFDdkIsR0FBWCxHQUFpQnVDLE1BQU0sQ0FBQ0MsV0FBMUM7QUFDQSxXQUNJLENBQUN4QixXQUFELElBQ0NzQixTQUFTLEdBQUd0QixXQUFXLENBQUN5QixNQUF4QixHQUFpQ3RELGdDQUZ0QztBQUlIOztBQXNHRDRDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBQ0EsUUFBSSxDQUFDLEtBQUtiLE1BQVYsRUFBa0I7QUFDZDtBQUNIOztBQUNELFNBQUtILFFBQUwsQ0FBYztBQUNWaUIsTUFBQUEsT0FBTyxFQUFFO0FBREMsS0FBZDs7QUFHQSxRQUFJLEtBQUtVLEtBQUwsQ0FBV0Msa0JBQWYsRUFBbUM7QUFDL0IsV0FBS0QsS0FBTCxDQUFXQyxrQkFBWCxDQUE4QixJQUE5QjtBQUNIOztBQUNEckQsSUFBQUEsUUFBUSxDQUFDc0QsZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsS0FBS1AsV0FBNUM7QUFDSDs7QUFFRFAsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsU0FBS2YsUUFBTCxDQUFjO0FBQ1ZpQixNQUFBQSxPQUFPLEVBQUU7QUFEQyxLQUFkOztBQUdBLFFBQUksS0FBS1UsS0FBTCxDQUFXQyxrQkFBZixFQUFtQztBQUMvQixXQUFLRCxLQUFMLENBQVdDLGtCQUFYLENBQThCLEtBQTlCO0FBQ0g7O0FBQ0RyRCxJQUFBQSxRQUFRLENBQUM4QyxtQkFBVCxDQUE2QixXQUE3QixFQUEwQyxLQUFLQyxXQUEvQztBQUNIOztBQUVESCxFQUFBQSxhQUFhLEdBQUc7QUFDWixVQUFNO0FBQUVsQixNQUFBQSxXQUFGO0FBQWVnQixNQUFBQTtBQUFmLFFBQTJCLEtBQUtWLEtBQXRDOztBQUNBLFFBQUksS0FBS29CLEtBQUwsQ0FBV0csV0FBWCxLQUEyQixJQUEzQixJQUFtQyxDQUFDYixPQUF4QyxFQUFpRDtBQUM3Q2Msd0JBQVNDLE1BQVQsQ0FBZ0IsSUFBaEIsRUFBc0IzRCxvQkFBb0IsRUFBMUM7O0FBQ0EsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTW1DLFVBQVUsR0FBRyxLQUFLTCxNQUFMLENBQVlELHFCQUFaLEVBQW5CLENBUFksQ0FTWjs7QUFDQSxVQUFNK0IsVUFBVSxHQUFHekIsVUFBVSxDQUFDcEIsSUFBWCxHQUFrQm9DLE1BQU0sQ0FBQ1UsV0FBNUM7QUFDQSxVQUFNQyxZQUFZLEdBQUczQixVQUFVLENBQUNyQixNQUFYLEdBQW9CcUMsTUFBTSxDQUFDQyxXQUFoRDtBQUNBLFVBQU1GLFNBQVMsR0FBR2YsVUFBVSxDQUFDdkIsR0FBWCxHQUFpQnVDLE1BQU0sQ0FBQ0MsV0FBMUMsQ0FaWSxDQWNaO0FBQ0E7QUFDQTs7QUFDQSxVQUFNVyxRQUFRLEdBQUcsRUFBakI7QUFDQSxRQUFJQyxXQUFXLEdBQUcsSUFBbEI7O0FBQ0EsUUFBSSxLQUFLM0Isd0JBQUwsRUFBSixFQUFxQztBQUNqQzBCLE1BQUFBLFFBQVEsQ0FBQ2pELE1BQVQsR0FBa0JxQyxNQUFNLENBQUNjLFdBQVAsR0FBcUJmLFNBQXZDO0FBQ0FjLE1BQUFBLFdBQVcsR0FBRyxRQUFkO0FBQ0gsS0FIRCxNQUdPO0FBQ0hELE1BQUFBLFFBQVEsQ0FBQ25ELEdBQVQsR0FBZWtELFlBQWY7QUFDQUUsTUFBQUEsV0FBVyxHQUFHLEtBQWQ7QUFDSCxLQXpCVyxDQTJCWjs7O0FBQ0FELElBQUFBLFFBQVEsQ0FBQ2hELElBQVQsR0FBZ0I2QyxVQUFVLEdBQUd6QixVQUFVLENBQUMrQixLQUFYLEdBQW1CLENBQWhEOztBQUVBLFVBQU1DLE9BQU8sR0FBRztBQUFLLE1BQUEsU0FBUyxFQUFFLG1DQUFtQ0g7QUFBbkQsTUFBaEI7O0FBRUEsVUFBTUksV0FBVyxHQUFHLHlCQUFXO0FBQzNCLCtCQUF5QixJQURFO0FBRTNCLCtDQUF5Q0osV0FBVyxLQUFLLEtBRjlCO0FBRzNCLGtEQUE0Q0EsV0FBVyxLQUFLO0FBSGpDLEtBQVgsQ0FBcEI7QUFNQSxVQUFNSyxTQUFTLEdBQUcsRUFBbEI7O0FBQ0EsUUFBSXpDLFdBQUosRUFBaUI7QUFDYnlDLE1BQUFBLFNBQVMsQ0FBQ3RELElBQVYsY0FBcUJhLFdBQVcsQ0FBQ3NDLEtBQVosR0FBb0IsQ0FBekM7QUFDSDs7QUFFRCxVQUFNSSxPQUFPLEdBQUc7QUFBSyxNQUFBLFNBQVMsRUFBQywrQkFBZjtBQUErQyxNQUFBLEtBQUssb0JBQU1QLFFBQU47QUFBcEQsT0FDWjtBQUFLLE1BQUEsU0FBUyxFQUFFSyxXQUFoQjtBQUNJLE1BQUEsS0FBSyxFQUFFQyxTQURYO0FBRUksTUFBQSxHQUFHLEVBQUUsS0FBS0U7QUFGZCxPQUlLSixPQUpMLEVBS0ssS0FBS2IsS0FBTCxDQUFXa0IsT0FMaEIsQ0FEWSxDQUFoQjs7QUFVQWQsc0JBQVNDLE1BQVQsQ0FBZ0JXLE9BQWhCLEVBQXlCdEUsb0JBQW9CLEVBQTdDO0FBQ0g7O0FBRUQyRCxFQUFBQSxNQUFNLEdBQUc7QUFDTDtBQUNBO0FBQ0EsV0FBT3BDLGVBQU1rRCxZQUFOLENBQW1CLEtBQUtuQixLQUFMLENBQVdvQixRQUE5QixFQUF3QztBQUMzQ0MsTUFBQUEsR0FBRyxFQUFFLEtBQUtDLGFBRGlDO0FBRTNDQyxNQUFBQSxXQUFXLEVBQUUsS0FBS0M7QUFGeUIsS0FBeEMsQ0FBUDtBQUlIOztBQW5QMkQ7Ozs4QkFBM0N4RCxrQixlQUNFO0FBQ2Y7QUFDQWtELEVBQUFBLE9BQU8sRUFBRU8sbUJBQVVDLElBQVYsQ0FBZUMsVUFGVDtBQUdmO0FBQ0ExQixFQUFBQSxrQkFBa0IsRUFBRXdCLG1CQUFVRyxJQUpmO0FBS2Y7QUFDQXpCLEVBQUFBLFdBQVcsRUFBRXNCLG1CQUFVSTtBQU5SLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuXHJcbmNvbnN0IEludGVyYWN0aXZlVG9vbHRpcENvbnRhaW5lcklkID0gXCJteF9JbnRlcmFjdGl2ZVRvb2x0aXBfQ29udGFpbmVyXCI7XHJcblxyXG4vLyBJZiB0aGUgZGlzdGFuY2UgZnJvbSB0b29sdGlwIHRvIHdpbmRvdyBlZGdlIGlzIGJlbG93IHRoaXMgdmFsdWUsIHRoZSB0b29sdGlwXHJcbi8vIHdpbGwgZmxpcCBhcm91bmQgdG8gdGhlIG90aGVyIHNpZGUgb2YgdGhlIHRhcmdldC5cclxuY29uc3QgTUlOX1NBRkVfRElTVEFOQ0VfVE9fV0lORE9XX0VER0UgPSAyMDtcclxuXHJcbmZ1bmN0aW9uIGdldE9yQ3JlYXRlQ29udGFpbmVyKCkge1xyXG4gICAgbGV0IGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKEludGVyYWN0aXZlVG9vbHRpcENvbnRhaW5lcklkKTtcclxuXHJcbiAgICBpZiAoIWNvbnRhaW5lcikge1xyXG4gICAgICAgIGNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgY29udGFpbmVyLmlkID0gSW50ZXJhY3RpdmVUb29sdGlwQ29udGFpbmVySWQ7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChjb250YWluZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBjb250YWluZXI7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGlzSW5SZWN0KHgsIHksIHJlY3QpIHtcclxuICAgIGNvbnN0IHsgdG9wLCByaWdodCwgYm90dG9tLCBsZWZ0IH0gPSByZWN0O1xyXG4gICAgcmV0dXJuIHggPj0gbGVmdCAmJiB4IDw9IHJpZ2h0ICYmIHkgPj0gdG9wICYmIHkgPD0gYm90dG9tO1xyXG59XHJcblxyXG4vKipcclxuICogUmV0dXJucyB0aGUgcG9zaXRpdmUgc2xvcGUgb2YgdGhlIGRpYWdvbmFsIG9mIHRoZSByZWN0LlxyXG4gKlxyXG4gKiBAcGFyYW0ge0RPTVJlY3R9IHJlY3RcclxuICogQHJldHVybiB7aW50ZWdlcn1cclxuICovXHJcbmZ1bmN0aW9uIGdldERpYWdvbmFsU2xvcGUocmVjdCkge1xyXG4gICAgY29uc3QgeyB0b3AsIHJpZ2h0LCBib3R0b20sIGxlZnQgfSA9IHJlY3Q7XHJcbiAgICByZXR1cm4gKGJvdHRvbSAtIHRvcCkgLyAocmlnaHQgLSBsZWZ0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gaXNJblVwcGVyTGVmdEhhbGYoeCwgeSwgcmVjdCkge1xyXG4gICAgY29uc3QgeyBib3R0b20sIGxlZnQgfSA9IHJlY3Q7XHJcbiAgICAvLyBOZWdhdGl2ZSBzbG9wZSBiZWNhdXNlIFkgdmFsdWVzIGdyb3cgZG93bndhcmRzIGFuZCBmb3IgdGhpcyBjYXNlLCB0aGVcclxuICAgIC8vIGRpYWdvbmFsIGdvZXMgZnJvbSBsYXJnZXIgdG8gc21hbGxlciBZIHZhbHVlcy5cclxuICAgIGNvbnN0IGRpYWdvbmFsU2xvcGUgPSBnZXREaWFnb25hbFNsb3BlKHJlY3QpICogLTE7XHJcbiAgICByZXR1cm4gaXNJblJlY3QoeCwgeSwgcmVjdCkgJiYgKHkgPD0gYm90dG9tICsgZGlhZ29uYWxTbG9wZSAqICh4IC0gbGVmdCkpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBpc0luTG93ZXJSaWdodEhhbGYoeCwgeSwgcmVjdCkge1xyXG4gICAgY29uc3QgeyBib3R0b20sIGxlZnQgfSA9IHJlY3Q7XHJcbiAgICAvLyBOZWdhdGl2ZSBzbG9wZSBiZWNhdXNlIFkgdmFsdWVzIGdyb3cgZG93bndhcmRzIGFuZCBmb3IgdGhpcyBjYXNlLCB0aGVcclxuICAgIC8vIGRpYWdvbmFsIGdvZXMgZnJvbSBsYXJnZXIgdG8gc21hbGxlciBZIHZhbHVlcy5cclxuICAgIGNvbnN0IGRpYWdvbmFsU2xvcGUgPSBnZXREaWFnb25hbFNsb3BlKHJlY3QpICogLTE7XHJcbiAgICByZXR1cm4gaXNJblJlY3QoeCwgeSwgcmVjdCkgJiYgKHkgPj0gYm90dG9tICsgZGlhZ29uYWxTbG9wZSAqICh4IC0gbGVmdCkpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBpc0luVXBwZXJSaWdodEhhbGYoeCwgeSwgcmVjdCkge1xyXG4gICAgY29uc3QgeyB0b3AsIGxlZnQgfSA9IHJlY3Q7XHJcbiAgICAvLyBQb3NpdGl2ZSBzbG9wZSBiZWNhdXNlIFkgdmFsdWVzIGdyb3cgZG93bndhcmRzIGFuZCBmb3IgdGhpcyBjYXNlLCB0aGVcclxuICAgIC8vIGRpYWdvbmFsIGdvZXMgZnJvbSBzbWFsbGVyIHRvIGxhcmdlciBZIHZhbHVlcy5cclxuICAgIGNvbnN0IGRpYWdvbmFsU2xvcGUgPSBnZXREaWFnb25hbFNsb3BlKHJlY3QpICogMTtcclxuICAgIHJldHVybiBpc0luUmVjdCh4LCB5LCByZWN0KSAmJiAoeSA8PSB0b3AgKyBkaWFnb25hbFNsb3BlICogKHggLSBsZWZ0KSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGlzSW5Mb3dlckxlZnRIYWxmKHgsIHksIHJlY3QpIHtcclxuICAgIGNvbnN0IHsgdG9wLCBsZWZ0IH0gPSByZWN0O1xyXG4gICAgLy8gUG9zaXRpdmUgc2xvcGUgYmVjYXVzZSBZIHZhbHVlcyBncm93IGRvd253YXJkcyBhbmQgZm9yIHRoaXMgY2FzZSwgdGhlXHJcbiAgICAvLyBkaWFnb25hbCBnb2VzIGZyb20gc21hbGxlciB0byBsYXJnZXIgWSB2YWx1ZXMuXHJcbiAgICBjb25zdCBkaWFnb25hbFNsb3BlID0gZ2V0RGlhZ29uYWxTbG9wZShyZWN0KSAqIDE7XHJcbiAgICByZXR1cm4gaXNJblJlY3QoeCwgeSwgcmVjdCkgJiYgKHkgPj0gdG9wICsgZGlhZ29uYWxTbG9wZSAqICh4IC0gbGVmdCkpO1xyXG59XHJcblxyXG4vKlxyXG4gKiBUaGlzIHN0eWxlIG9mIHRvb2x0aXAgdGFrZXMgYSBcInRhcmdldFwiIGVsZW1lbnQgYXMgaXRzIGNoaWxkIGFuZCBjZW50ZXJzIHRoZVxyXG4gKiB0b29sdGlwIGFsb25nIG9uZSBlZGdlIG9mIHRoZSB0YXJnZXQuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbnRlcmFjdGl2ZVRvb2x0aXAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyBDb250ZW50IHRvIHNob3cgaW4gdGhlIHRvb2x0aXBcclxuICAgICAgICBjb250ZW50OiBQcm9wVHlwZXMubm9kZS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIC8vIEZ1bmN0aW9uIHRvIGNhbGwgd2hlbiB2aXNpYmlsaXR5IG9mIHRoZSB0b29sdGlwIGNoYW5nZXNcclxuICAgICAgICBvblZpc2liaWxpdHlDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIC8vIGZsYWcgdG8gZm9yY2VmdWxseSBoaWRlIHRoaXMgdG9vbHRpcFxyXG4gICAgICAgIGZvcmNlSGlkZGVuOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgY29udGVudFJlY3Q6IG51bGwsXHJcbiAgICAgICAgICAgIHZpc2libGU6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlKCkge1xyXG4gICAgICAgIC8vIFdoZW5ldmVyIHRoaXMgcGFzc3Rocm91Z2ggY29tcG9uZW50IHVwZGF0ZXMsIGFsc28gcmVuZGVyIHRoZSB0b29sdGlwXHJcbiAgICAgICAgLy8gaW4gYSBzZXBhcmF0ZSBET00gdHJlZS4gVGhpcyBhbGxvd3MgdGhlIHRvb2x0aXAgY29udGVudCB0byBwYXJ0aWNpcGF0ZVxyXG4gICAgICAgIC8vIHRoZSBub3JtYWwgUmVhY3QgcmVuZGVyaW5nIGN5Y2xlOiB3aGVuIHRoaXMgY29tcG9uZW50IHJlLXJlbmRlcnMsIHRoZVxyXG4gICAgICAgIC8vIHRvb2x0aXAgY29udGVudCByZS1yZW5kZXJzLlxyXG4gICAgICAgIC8vIE9uY2Ugd2UgdXBncmFkZSB0byBSZWFjdCAxNiwgdGhpcyBjb3VsZCBiZSBkb25lIGEgYml0IG1vcmUgbmF0dXJhbGx5XHJcbiAgICAgICAgLy8gdXNpbmcgdGhlIHBvcnRhbHMgZmVhdHVyZSBpbnN0ZWFkLlxyXG4gICAgICAgIHRoaXMucmVuZGVyVG9vbHRpcCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdGhpcy5vbk1vdXNlTW92ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29sbGVjdENvbnRlbnRSZWN0ID0gKGVsZW1lbnQpID0+IHtcclxuICAgICAgICAvLyBXZSBkb24ndCBuZWVkIHRvIGNsZWFuIHVwIHdoZW4gdW5tb3VudGluZywgc28gaWdub3JlXHJcbiAgICAgICAgaWYgKCFlbGVtZW50KSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjb250ZW50UmVjdDogZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb2xsZWN0VGFyZ2V0ID0gKGVsZW1lbnQpID0+IHtcclxuICAgICAgICB0aGlzLnRhcmdldCA9IGVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuVG9vbHRpcEZpdEFib3ZlVGFyZ2V0KCkge1xyXG4gICAgICAgIGNvbnN0IHsgY29udGVudFJlY3QgfSA9IHRoaXMuc3RhdGU7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0UmVjdCA9IHRoaXMudGFyZ2V0LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldFRvcCA9IHRhcmdldFJlY3QudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICFjb250ZW50UmVjdCB8fFxyXG4gICAgICAgICAgICAodGFyZ2V0VG9wIC0gY29udGVudFJlY3QuaGVpZ2h0ID4gTUlOX1NBRkVfRElTVEFOQ0VfVE9fV0lORE9XX0VER0UpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlTW92ZSA9IChldikgPT4ge1xyXG4gICAgICAgIGNvbnN0IHsgY2xpZW50WDogeCwgY2xpZW50WTogeSB9ID0gZXY7XHJcbiAgICAgICAgY29uc3QgeyBjb250ZW50UmVjdCB9ID0gdGhpcy5zdGF0ZTtcclxuICAgICAgICBjb25zdCB0YXJnZXRSZWN0ID0gdGhpcy50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gbW92aW5nIHRoZSBtb3VzZSBmcm9tIHRoZSB0YXJnZXQgdG8gdGhlIHRvb2x0aXAsIHdlIGNyZWF0ZSBhXHJcbiAgICAgICAgLy8gc2FmZSBhcmVhIHRoYXQgaW5jbHVkZXMgdGhlIHRvb2x0aXAsIHRoZSB0YXJnZXQsIGFuZCB0aGUgdHJhcGV6b2lkXHJcbiAgICAgICAgLy8gQUJDRCBiZXR3ZWVuIHRoZW06XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAg4pSM4pSA4pSA4pSA4pSA4pSA4pSA4pSA4pSA4pSA4pSA4pSA4pSQXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAg4pSCICAgICAgICAgICDilIJcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICDilIIgICAgICAgICAgIOKUglxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICBBIOKUlOKUgOKUgOKUgEXilIDilIDilIBG4pSA4pSA4pSA4pSYIEJcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBWXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDilIzilIDilJBcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIOKUgiDilIJcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ+KUlOKUgOKUmERcclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIEFzIGxvbmcgYXMgdGhlIG1vdXNlIHJlbWFpbnMgaW5zaWRlIHRoZSBzYWZlIGFyZWEsIHRoZSB0b29sdGlwIHdpbGxcclxuICAgICAgICAvLyBzdGF5IG9wZW4uXHJcbiAgICAgICAgY29uc3QgYnVmZmVyID0gNTA7XHJcbiAgICAgICAgaWYgKGlzSW5SZWN0KHgsIHksIHRhcmdldFJlY3QpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY2FuVG9vbHRpcEZpdEFib3ZlVGFyZ2V0KCkpIHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudFJlY3RXaXRoQnVmZmVyID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiBjb250ZW50UmVjdC50b3AgLSBidWZmZXIsXHJcbiAgICAgICAgICAgICAgICByaWdodDogY29udGVudFJlY3QucmlnaHQgKyBidWZmZXIsXHJcbiAgICAgICAgICAgICAgICBib3R0b206IGNvbnRlbnRSZWN0LmJvdHRvbSxcclxuICAgICAgICAgICAgICAgIGxlZnQ6IGNvbnRlbnRSZWN0LmxlZnQgLSBidWZmZXIsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYXBlem9pZExlZnQgPSB7XHJcbiAgICAgICAgICAgICAgICB0b3A6IGNvbnRlbnRSZWN0LmJvdHRvbSxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiB0YXJnZXRSZWN0LmxlZnQsXHJcbiAgICAgICAgICAgICAgICBib3R0b206IHRhcmdldFJlY3QuYm90dG9tLFxyXG4gICAgICAgICAgICAgICAgbGVmdDogY29udGVudFJlY3QubGVmdCAtIGJ1ZmZlcixcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgY29uc3QgdHJhcGV6b2lkQ2VudGVyID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiBjb250ZW50UmVjdC5ib3R0b20sXHJcbiAgICAgICAgICAgICAgICByaWdodDogdGFyZ2V0UmVjdC5yaWdodCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogdGFyZ2V0UmVjdC5ib3R0b20sXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiB0YXJnZXRSZWN0LmxlZnQsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYXBlem9pZFJpZ2h0ID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiBjb250ZW50UmVjdC5ib3R0b20sXHJcbiAgICAgICAgICAgICAgICByaWdodDogY29udGVudFJlY3QucmlnaHQgKyBidWZmZXIsXHJcbiAgICAgICAgICAgICAgICBib3R0b206IHRhcmdldFJlY3QuYm90dG9tLFxyXG4gICAgICAgICAgICAgICAgbGVmdDogdGFyZ2V0UmVjdC5yaWdodCxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIGlzSW5SZWN0KHgsIHksIGNvbnRlbnRSZWN0V2l0aEJ1ZmZlcikgfHxcclxuICAgICAgICAgICAgICAgIGlzSW5VcHBlclJpZ2h0SGFsZih4LCB5LCB0cmFwZXpvaWRMZWZ0KSB8fFxyXG4gICAgICAgICAgICAgICAgaXNJblJlY3QoeCwgeSwgdHJhcGV6b2lkQ2VudGVyKSB8fFxyXG4gICAgICAgICAgICAgICAgaXNJblVwcGVyTGVmdEhhbGYoeCwgeSwgdHJhcGV6b2lkUmlnaHQpXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudFJlY3RXaXRoQnVmZmVyID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiBjb250ZW50UmVjdC50b3AsXHJcbiAgICAgICAgICAgICAgICByaWdodDogY29udGVudFJlY3QucmlnaHQgKyBidWZmZXIsXHJcbiAgICAgICAgICAgICAgICBib3R0b206IGNvbnRlbnRSZWN0LmJvdHRvbSArIGJ1ZmZlcixcclxuICAgICAgICAgICAgICAgIGxlZnQ6IGNvbnRlbnRSZWN0LmxlZnQgLSBidWZmZXIsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYXBlem9pZExlZnQgPSB7XHJcbiAgICAgICAgICAgICAgICB0b3A6IHRhcmdldFJlY3QudG9wLFxyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IHRhcmdldFJlY3QubGVmdCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogY29udGVudFJlY3QudG9wLFxyXG4gICAgICAgICAgICAgICAgbGVmdDogY29udGVudFJlY3QubGVmdCAtIGJ1ZmZlcixcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgY29uc3QgdHJhcGV6b2lkQ2VudGVyID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiB0YXJnZXRSZWN0LnRvcCxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiB0YXJnZXRSZWN0LnJpZ2h0LFxyXG4gICAgICAgICAgICAgICAgYm90dG9tOiBjb250ZW50UmVjdC50b3AsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiB0YXJnZXRSZWN0LmxlZnQsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYXBlem9pZFJpZ2h0ID0ge1xyXG4gICAgICAgICAgICAgICAgdG9wOiB0YXJnZXRSZWN0LnRvcCxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiBjb250ZW50UmVjdC5yaWdodCArIGJ1ZmZlcixcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogY29udGVudFJlY3QudG9wLFxyXG4gICAgICAgICAgICAgICAgbGVmdDogdGFyZ2V0UmVjdC5yaWdodCxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIGlzSW5SZWN0KHgsIHksIGNvbnRlbnRSZWN0V2l0aEJ1ZmZlcikgfHxcclxuICAgICAgICAgICAgICAgIGlzSW5Mb3dlclJpZ2h0SGFsZih4LCB5LCB0cmFwZXpvaWRMZWZ0KSB8fFxyXG4gICAgICAgICAgICAgICAgaXNJblJlY3QoeCwgeSwgdHJhcGV6b2lkQ2VudGVyKSB8fFxyXG4gICAgICAgICAgICAgICAgaXNJbkxvd2VyTGVmdEhhbGYoeCwgeSwgdHJhcGV6b2lkUmlnaHQpXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmhpZGVUb29sdGlwKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25UYXJnZXRNb3VzZU92ZXIgPSAoZXYpID0+IHtcclxuICAgICAgICB0aGlzLnNob3dUb29sdGlwKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1Rvb2x0aXAoKSB7XHJcbiAgICAgICAgLy8gRG9uJ3QgZW50ZXIgdmlzaWJsZSBzdGF0ZSBpZiB3ZSBoYXZlbid0IGNvbGxlY3RlZCB0aGUgdGFyZ2V0IHlldFxyXG4gICAgICAgIGlmICghdGhpcy50YXJnZXQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vblZpc2liaWxpdHlDaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblZpc2liaWxpdHlDaGFuZ2UodHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdGhpcy5vbk1vdXNlTW92ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGlkZVRvb2x0aXAoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHZpc2libGU6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uVmlzaWJpbGl0eUNoYW5nZSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uVmlzaWJpbGl0eUNoYW5nZShmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdGhpcy5vbk1vdXNlTW92ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyVG9vbHRpcCgpIHtcclxuICAgICAgICBjb25zdCB7IGNvbnRlbnRSZWN0LCB2aXNpYmxlIH0gPSB0aGlzLnN0YXRlO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmZvcmNlSGlkZGVuID09PSB0cnVlIHx8ICF2aXNpYmxlKSB7XHJcbiAgICAgICAgICAgIFJlYWN0RE9NLnJlbmRlcihudWxsLCBnZXRPckNyZWF0ZUNvbnRhaW5lcigpKTtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0YXJnZXRSZWN0ID0gdGhpcy50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblxyXG4gICAgICAgIC8vIFRoZSB3aW5kb3cgWCBhbmQgWSBvZmZzZXRzIGFyZSB0byBhZGp1c3QgcG9zaXRpb24gd2hlbiB6b29tZWQgaW4gdG8gcGFnZVxyXG4gICAgICAgIGNvbnN0IHRhcmdldExlZnQgPSB0YXJnZXRSZWN0LmxlZnQgKyB3aW5kb3cucGFnZVhPZmZzZXQ7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0Qm90dG9tID0gdGFyZ2V0UmVjdC5ib3R0b20gKyB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0VG9wID0gdGFyZ2V0UmVjdC50b3AgKyB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblxyXG4gICAgICAgIC8vIFBsYWNlIHRoZSB0b29sdGlwIGFib3ZlIHRoZSB0YXJnZXQgYnkgZGVmYXVsdC4gSWYgd2UgZmluZCB0aGF0IHRoZVxyXG4gICAgICAgIC8vIHRvb2x0aXAgY29udGVudCB3b3VsZCBleHRlbmQgcGFzdCB0aGUgc2FmZSBhcmVhIHRvd2FyZHMgdGhlIHdpbmRvd1xyXG4gICAgICAgIC8vIGVkZ2UsIGZsaXAgYXJvdW5kIHRvIGJlbG93IHRoZSB0YXJnZXQuXHJcbiAgICAgICAgY29uc3QgcG9zaXRpb24gPSB7fTtcclxuICAgICAgICBsZXQgY2hldnJvbkZhY2UgPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLmNhblRvb2x0aXBGaXRBYm92ZVRhcmdldCgpKSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uLmJvdHRvbSA9IHdpbmRvdy5pbm5lckhlaWdodCAtIHRhcmdldFRvcDtcclxuICAgICAgICAgICAgY2hldnJvbkZhY2UgPSBcImJvdHRvbVwiO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uLnRvcCA9IHRhcmdldEJvdHRvbTtcclxuICAgICAgICAgICAgY2hldnJvbkZhY2UgPSBcInRvcFwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ2VudGVyIHRoZSB0b29sdGlwIGhvcml6b250YWxseSB3aXRoIHRoZSB0YXJnZXQncyBjZW50ZXIuXHJcbiAgICAgICAgcG9zaXRpb24ubGVmdCA9IHRhcmdldExlZnQgKyB0YXJnZXRSZWN0LndpZHRoIC8gMjtcclxuXHJcbiAgICAgICAgY29uc3QgY2hldnJvbiA9IDxkaXYgY2xhc3NOYW1lPXtcIm14X0ludGVyYWN0aXZlVG9vbHRpcF9jaGV2cm9uX1wiICsgY2hldnJvbkZhY2V9IC8+O1xyXG5cclxuICAgICAgICBjb25zdCBtZW51Q2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAnbXhfSW50ZXJhY3RpdmVUb29sdGlwJzogdHJ1ZSxcclxuICAgICAgICAgICAgJ214X0ludGVyYWN0aXZlVG9vbHRpcF93aXRoQ2hldnJvbl90b3AnOiBjaGV2cm9uRmFjZSA9PT0gJ3RvcCcsXHJcbiAgICAgICAgICAgICdteF9JbnRlcmFjdGl2ZVRvb2x0aXBfd2l0aENoZXZyb25fYm90dG9tJzogY2hldnJvbkZhY2UgPT09ICdib3R0b20nLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBtZW51U3R5bGUgPSB7fTtcclxuICAgICAgICBpZiAoY29udGVudFJlY3QpIHtcclxuICAgICAgICAgICAgbWVudVN0eWxlLmxlZnQgPSBgLSR7Y29udGVudFJlY3Qud2lkdGggLyAyfXB4YDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHRvb2x0aXAgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X0ludGVyYWN0aXZlVG9vbHRpcF93cmFwcGVyXCIgc3R5bGU9e3suLi5wb3NpdGlvbn19PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17bWVudUNsYXNzZXN9XHJcbiAgICAgICAgICAgICAgICBzdHlsZT17bWVudVN0eWxlfVxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLmNvbGxlY3RDb250ZW50UmVjdH1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAge2NoZXZyb259XHJcbiAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jb250ZW50fVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj47XHJcblxyXG4gICAgICAgIFJlYWN0RE9NLnJlbmRlcih0b29sdGlwLCBnZXRPckNyZWF0ZUNvbnRhaW5lcigpKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgLy8gV2UgdXNlIGBjbG9uZUVsZW1lbnRgIGhlcmUgdG8gYXBwZW5kIHNvbWUgcHJvcHMgdG8gdGhlIGNoaWxkIGNvbnRlbnRcclxuICAgICAgICAvLyB3aXRob3V0IHVzaW5nIGEgd3JhcHBlciBlbGVtZW50IHdoaWNoIGNvdWxkIGRpc3J1cHQgbGF5b3V0LlxyXG4gICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQodGhpcy5wcm9wcy5jaGlsZHJlbiwge1xyXG4gICAgICAgICAgICByZWY6IHRoaXMuY29sbGVjdFRhcmdldCxcclxuICAgICAgICAgICAgb25Nb3VzZU92ZXI6IHRoaXMub25UYXJnZXRNb3VzZU92ZXIsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19