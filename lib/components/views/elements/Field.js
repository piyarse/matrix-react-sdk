"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _lodash = require("lodash");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Invoke validation from user input (when typing, etc.) at most once every N ms.
const VALIDATION_THROTTLE_MS = 200;
const BASE_ID = "mx_Field";
let count = 1;

function getId() {
  return "".concat(BASE_ID, "_").concat(count++);
}

class Field extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onFocus", ev => {
      this.setState({
        focused: true
      });
      this.validate({
        focused: true
      }); // Parent component may have supplied its own `onFocus` as well

      if (this.props.onFocus) {
        this.props.onFocus(ev);
      }
    });
    (0, _defineProperty2.default)(this, "onChange", ev => {
      this.validateOnChange(); // Parent component may have supplied its own `onChange` as well

      if (this.props.onChange) {
        this.props.onChange(ev);
      }
    });
    (0, _defineProperty2.default)(this, "onBlur", ev => {
      this.setState({
        focused: false
      });
      this.validate({
        focused: false
      }); // Parent component may have supplied its own `onBlur` as well

      if (this.props.onBlur) {
        this.props.onBlur(ev);
      }
    });
    (0, _defineProperty2.default)(this, "validateOnChange", (0, _lodash.debounce)(() => {
      this.validate({
        focused: true
      });
    }, VALIDATION_THROTTLE_MS));
    this.state = {
      valid: undefined,
      feedback: undefined,
      focused: false
    };
    this.id = this.props.id || getId();
  }

  focus() {
    this.input.focus();
  }

  async validate({
    focused,
    allowEmpty = true
  }) {
    if (!this.props.onValidate) {
      return;
    }

    const value = this.input ? this.input.value : null;
    const {
      valid,
      feedback
    } = await this.props.onValidate({
      value,
      focused,
      allowEmpty
    }); // this method is async and so we may have been blurred since the method was called
    // if we have then hide the feedback as withValidation does

    if (this.state.focused && feedback) {
      this.setState({
        valid,
        feedback,
        feedbackVisible: true
      });
    } else {
      // When we receive null `feedback`, we want to hide the tooltip.
      // We leave the previous `feedback` content in state without updating it,
      // so that we can hide the tooltip containing the most recent feedback
      // via CSS animation.
      this.setState({
        valid,
        feedbackVisible: false
      });
    }
  }
  /*
   * This was changed from throttle to debounce: this is more traditional for
   * form validation since it means that the validation doesn't happen at all
   * until the user stops typing for a bit (debounce defaults to not running on
   * the leading edge). If we're doing an HTTP hit on each validation, we have more
   * incentive to prevent validating input that's very unlikely to be valid.
   * We may find that we actually want different behaviour for registration
   * fields, in which case we can add some options to control it.
   */


  render() {
    const _this$props = this.props,
          {
      element,
      prefix,
      postfix,
      className,
      onValidate,
      children,
      tooltipContent,
      flagInvalid,
      tooltipClassName,
      list
    } = _this$props,
          inputProps = (0, _objectWithoutProperties2.default)(_this$props, ["element", "prefix", "postfix", "className", "onValidate", "children", "tooltipContent", "flagInvalid", "tooltipClassName", "list"]);
    const inputElement = element || "input"; // Set some defaults for the <input> element

    inputProps.type = inputProps.type || "text";

    inputProps.ref = input => this.input = input;

    inputProps.placeholder = inputProps.placeholder || inputProps.label;
    inputProps.id = this.id; // this overwrites the id from props

    inputProps.onFocus = this.onFocus;
    inputProps.onChange = this.onChange;
    inputProps.onBlur = this.onBlur;
    inputProps.list = list;

    const fieldInput = _react.default.createElement(inputElement, inputProps, children);

    let prefixContainer = null;

    if (prefix) {
      prefixContainer = _react.default.createElement("span", {
        className: "mx_Field_prefix"
      }, prefix);
    }

    let postfixContainer = null;

    if (postfix) {
      postfixContainer = _react.default.createElement("span", {
        className: "mx_Field_postfix"
      }, postfix);
    }

    const hasValidationFlag = flagInvalid !== null && flagInvalid !== undefined;
    const fieldClasses = (0, _classnames.default)("mx_Field", "mx_Field_".concat(inputElement), className, {
      // If we have a prefix element, leave the label always at the top left and
      // don't animate it, as it looks a bit clunky and would add complexity to do
      // properly.
      mx_Field_labelAlwaysTopLeft: prefix,
      mx_Field_valid: onValidate && this.state.valid === true,
      mx_Field_invalid: hasValidationFlag ? flagInvalid : onValidate && this.state.valid === false
    }); // Handle displaying feedback on validity

    const Tooltip = sdk.getComponent("elements.Tooltip");
    let fieldTooltip;

    if (tooltipContent || this.state.feedback) {
      const addlClassName = tooltipClassName ? tooltipClassName : '';
      fieldTooltip = _react.default.createElement(Tooltip, {
        tooltipClassName: "mx_Field_tooltip ".concat(addlClassName),
        visible: this.state.feedbackVisible,
        label: tooltipContent || this.state.feedback
      });
    }

    return _react.default.createElement("div", {
      className: fieldClasses
    }, prefixContainer, fieldInput, _react.default.createElement("label", {
      htmlFor: this.id
    }, this.props.label), postfixContainer, fieldTooltip);
  }

}

exports.default = Field;
(0, _defineProperty2.default)(Field, "propTypes", {
  // The field's ID, which binds the input and label together. Immutable.
  id: _propTypes.default.string,
  // The element to create. Defaults to "input".
  // To define options for a select, use <Field><option ... /></Field>
  element: _propTypes.default.oneOf(["input", "select", "textarea"]),
  // The field's type (when used as an <input>). Defaults to "text".
  type: _propTypes.default.string,
  // id of a <datalist> element for suggestions
  list: _propTypes.default.string,
  // The field's label string.
  label: _propTypes.default.string,
  // The field's placeholder string. Defaults to the label.
  placeholder: _propTypes.default.string,
  // The field's value.
  // This is a controlled component, so the value is required.
  value: _propTypes.default.string.isRequired,
  // Optional component to include inside the field before the input.
  prefix: _propTypes.default.node,
  // Optional component to include inside the field after the input.
  postfix: _propTypes.default.node,
  // The callback called whenever the contents of the field
  // changes.  Returns an object with `valid` boolean field
  // and a `feedback` react component field to provide feedback
  // to the user.
  onValidate: _propTypes.default.func,
  // If specified, overrides the value returned by onValidate.
  flagInvalid: _propTypes.default.bool,
  // If specified, contents will appear as a tooltip on the element and
  // validation feedback tooltips will be suppressed.
  tooltipContent: _propTypes.default.node,
  // If specified alongside tooltipContent, the class name to apply to the
  // tooltip itself.
  tooltipClassName: _propTypes.default.string,
  // If specified, an additional class name to apply to the field container
  className: _propTypes.default.string // All other props pass through to the <input>.

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0ZpZWxkLmpzIl0sIm5hbWVzIjpbIlZBTElEQVRJT05fVEhST1RUTEVfTVMiLCJCQVNFX0lEIiwiY291bnQiLCJnZXRJZCIsIkZpZWxkIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsImV2Iiwic2V0U3RhdGUiLCJmb2N1c2VkIiwidmFsaWRhdGUiLCJvbkZvY3VzIiwidmFsaWRhdGVPbkNoYW5nZSIsIm9uQ2hhbmdlIiwib25CbHVyIiwic3RhdGUiLCJ2YWxpZCIsInVuZGVmaW5lZCIsImZlZWRiYWNrIiwiaWQiLCJmb2N1cyIsImlucHV0IiwiYWxsb3dFbXB0eSIsIm9uVmFsaWRhdGUiLCJ2YWx1ZSIsImZlZWRiYWNrVmlzaWJsZSIsInJlbmRlciIsImVsZW1lbnQiLCJwcmVmaXgiLCJwb3N0Zml4IiwiY2xhc3NOYW1lIiwiY2hpbGRyZW4iLCJ0b29sdGlwQ29udGVudCIsImZsYWdJbnZhbGlkIiwidG9vbHRpcENsYXNzTmFtZSIsImxpc3QiLCJpbnB1dFByb3BzIiwiaW5wdXRFbGVtZW50IiwidHlwZSIsInJlZiIsInBsYWNlaG9sZGVyIiwibGFiZWwiLCJmaWVsZElucHV0IiwiY3JlYXRlRWxlbWVudCIsInByZWZpeENvbnRhaW5lciIsInBvc3RmaXhDb250YWluZXIiLCJoYXNWYWxpZGF0aW9uRmxhZyIsImZpZWxkQ2xhc3NlcyIsIm14X0ZpZWxkX2xhYmVsQWx3YXlzVG9wTGVmdCIsIm14X0ZpZWxkX3ZhbGlkIiwibXhfRmllbGRfaW52YWxpZCIsIlRvb2x0aXAiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJmaWVsZFRvb2x0aXAiLCJhZGRsQ2xhc3NOYW1lIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwib25lT2YiLCJpc1JlcXVpcmVkIiwibm9kZSIsImZ1bmMiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBcEJBOzs7Ozs7Ozs7Ozs7Ozs7QUFzQkE7QUFDQSxNQUFNQSxzQkFBc0IsR0FBRyxHQUEvQjtBQUVBLE1BQU1DLE9BQU8sR0FBRyxVQUFoQjtBQUNBLElBQUlDLEtBQUssR0FBRyxDQUFaOztBQUNBLFNBQVNDLEtBQVQsR0FBaUI7QUFDYixtQkFBVUYsT0FBVixjQUFxQkMsS0FBSyxFQUExQjtBQUNIOztBQUVjLE1BQU1FLEtBQU4sU0FBb0JDLGVBQU1DLGFBQTFCLENBQXdDO0FBd0NuREMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsbURBV1JDLEVBQUQsSUFBUTtBQUNkLFdBQUtDLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSxPQUFPLEVBQUU7QUFEQyxPQUFkO0FBR0EsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZELFFBQUFBLE9BQU8sRUFBRTtBQURDLE9BQWQsRUFKYyxDQU9kOztBQUNBLFVBQUksS0FBS0gsS0FBTCxDQUFXSyxPQUFmLEVBQXdCO0FBQ3BCLGFBQUtMLEtBQUwsQ0FBV0ssT0FBWCxDQUFtQkosRUFBbkI7QUFDSDtBQUNKLEtBdEJrQjtBQUFBLG9EQXdCUEEsRUFBRCxJQUFRO0FBQ2YsV0FBS0ssZ0JBQUwsR0FEZSxDQUVmOztBQUNBLFVBQUksS0FBS04sS0FBTCxDQUFXTyxRQUFmLEVBQXlCO0FBQ3JCLGFBQUtQLEtBQUwsQ0FBV08sUUFBWCxDQUFvQk4sRUFBcEI7QUFDSDtBQUNKLEtBOUJrQjtBQUFBLGtEQWdDVEEsRUFBRCxJQUFRO0FBQ2IsV0FBS0MsUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLE9BQU8sRUFBRTtBQURDLE9BQWQ7QUFHQSxXQUFLQyxRQUFMLENBQWM7QUFDVkQsUUFBQUEsT0FBTyxFQUFFO0FBREMsT0FBZCxFQUphLENBT2I7O0FBQ0EsVUFBSSxLQUFLSCxLQUFMLENBQVdRLE1BQWYsRUFBdUI7QUFDbkIsYUFBS1IsS0FBTCxDQUFXUSxNQUFYLENBQWtCUCxFQUFsQjtBQUNIO0FBQ0osS0EzQ2tCO0FBQUEsNERBeUZBLHNCQUFTLE1BQU07QUFDOUIsV0FBS0csUUFBTCxDQUFjO0FBQ1ZELFFBQUFBLE9BQU8sRUFBRTtBQURDLE9BQWQ7QUFHSCxLQUprQixFQUloQlgsc0JBSmdCLENBekZBO0FBRWYsU0FBS2lCLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxLQUFLLEVBQUVDLFNBREU7QUFFVEMsTUFBQUEsUUFBUSxFQUFFRCxTQUZEO0FBR1RSLE1BQUFBLE9BQU8sRUFBRTtBQUhBLEtBQWI7QUFNQSxTQUFLVSxFQUFMLEdBQVUsS0FBS2IsS0FBTCxDQUFXYSxFQUFYLElBQWlCbEIsS0FBSyxFQUFoQztBQUNIOztBQW9DRG1CLEVBQUFBLEtBQUssR0FBRztBQUNKLFNBQUtDLEtBQUwsQ0FBV0QsS0FBWDtBQUNIOztBQUVELFFBQU1WLFFBQU4sQ0FBZTtBQUFFRCxJQUFBQSxPQUFGO0FBQVdhLElBQUFBLFVBQVUsR0FBRztBQUF4QixHQUFmLEVBQStDO0FBQzNDLFFBQUksQ0FBQyxLQUFLaEIsS0FBTCxDQUFXaUIsVUFBaEIsRUFBNEI7QUFDeEI7QUFDSDs7QUFDRCxVQUFNQyxLQUFLLEdBQUcsS0FBS0gsS0FBTCxHQUFhLEtBQUtBLEtBQUwsQ0FBV0csS0FBeEIsR0FBZ0MsSUFBOUM7QUFDQSxVQUFNO0FBQUVSLE1BQUFBLEtBQUY7QUFBU0UsTUFBQUE7QUFBVCxRQUFzQixNQUFNLEtBQUtaLEtBQUwsQ0FBV2lCLFVBQVgsQ0FBc0I7QUFDcERDLE1BQUFBLEtBRG9EO0FBRXBEZixNQUFBQSxPQUZvRDtBQUdwRGEsTUFBQUE7QUFIb0QsS0FBdEIsQ0FBbEMsQ0FMMkMsQ0FXM0M7QUFDQTs7QUFDQSxRQUFJLEtBQUtQLEtBQUwsQ0FBV04sT0FBWCxJQUFzQlMsUUFBMUIsRUFBb0M7QUFDaEMsV0FBS1YsUUFBTCxDQUFjO0FBQ1ZRLFFBQUFBLEtBRFU7QUFFVkUsUUFBQUEsUUFGVTtBQUdWTyxRQUFBQSxlQUFlLEVBQUU7QUFIUCxPQUFkO0FBS0gsS0FORCxNQU1PO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFLakIsUUFBTCxDQUFjO0FBQ1ZRLFFBQUFBLEtBRFU7QUFFVlMsUUFBQUEsZUFBZSxFQUFFO0FBRlAsT0FBZDtBQUlIO0FBQ0o7QUFFRDs7Ozs7Ozs7Ozs7QUFlQUMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsd0JBRTBFLEtBQUtwQixLQUYvRTtBQUFBLFVBQU07QUFDRnFCLE1BQUFBLE9BREU7QUFDT0MsTUFBQUEsTUFEUDtBQUNlQyxNQUFBQSxPQURmO0FBQ3dCQyxNQUFBQSxTQUR4QjtBQUNtQ1AsTUFBQUEsVUFEbkM7QUFDK0NRLE1BQUFBLFFBRC9DO0FBRUZDLE1BQUFBLGNBRkU7QUFFY0MsTUFBQUEsV0FGZDtBQUUyQkMsTUFBQUEsZ0JBRjNCO0FBRTZDQyxNQUFBQTtBQUY3QyxLQUFOO0FBQUEsVUFFNERDLFVBRjVEO0FBSUEsVUFBTUMsWUFBWSxHQUFHVixPQUFPLElBQUksT0FBaEMsQ0FMSyxDQU9MOztBQUNBUyxJQUFBQSxVQUFVLENBQUNFLElBQVgsR0FBa0JGLFVBQVUsQ0FBQ0UsSUFBWCxJQUFtQixNQUFyQzs7QUFDQUYsSUFBQUEsVUFBVSxDQUFDRyxHQUFYLEdBQWlCbEIsS0FBSyxJQUFJLEtBQUtBLEtBQUwsR0FBYUEsS0FBdkM7O0FBQ0FlLElBQUFBLFVBQVUsQ0FBQ0ksV0FBWCxHQUF5QkosVUFBVSxDQUFDSSxXQUFYLElBQTBCSixVQUFVLENBQUNLLEtBQTlEO0FBQ0FMLElBQUFBLFVBQVUsQ0FBQ2pCLEVBQVgsR0FBZ0IsS0FBS0EsRUFBckIsQ0FYSyxDQVdvQjs7QUFFekJpQixJQUFBQSxVQUFVLENBQUN6QixPQUFYLEdBQXFCLEtBQUtBLE9BQTFCO0FBQ0F5QixJQUFBQSxVQUFVLENBQUN2QixRQUFYLEdBQXNCLEtBQUtBLFFBQTNCO0FBQ0F1QixJQUFBQSxVQUFVLENBQUN0QixNQUFYLEdBQW9CLEtBQUtBLE1BQXpCO0FBQ0FzQixJQUFBQSxVQUFVLENBQUNELElBQVgsR0FBa0JBLElBQWxCOztBQUVBLFVBQU1PLFVBQVUsR0FBR3ZDLGVBQU13QyxhQUFOLENBQW9CTixZQUFwQixFQUFrQ0QsVUFBbEMsRUFBOENMLFFBQTlDLENBQW5COztBQUVBLFFBQUlhLGVBQWUsR0FBRyxJQUF0Qjs7QUFDQSxRQUFJaEIsTUFBSixFQUFZO0FBQ1JnQixNQUFBQSxlQUFlLEdBQUc7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUFtQ2hCLE1BQW5DLENBQWxCO0FBQ0g7O0FBQ0QsUUFBSWlCLGdCQUFnQixHQUFHLElBQXZCOztBQUNBLFFBQUloQixPQUFKLEVBQWE7QUFDVGdCLE1BQUFBLGdCQUFnQixHQUFHO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBb0NoQixPQUFwQyxDQUFuQjtBQUNIOztBQUVELFVBQU1pQixpQkFBaUIsR0FBR2IsV0FBVyxLQUFLLElBQWhCLElBQXdCQSxXQUFXLEtBQUtoQixTQUFsRTtBQUNBLFVBQU04QixZQUFZLEdBQUcseUJBQVcsVUFBWCxxQkFBbUNWLFlBQW5DLEdBQW1EUCxTQUFuRCxFQUE4RDtBQUMvRTtBQUNBO0FBQ0E7QUFDQWtCLE1BQUFBLDJCQUEyQixFQUFFcEIsTUFKa0Q7QUFLL0VxQixNQUFBQSxjQUFjLEVBQUUxQixVQUFVLElBQUksS0FBS1IsS0FBTCxDQUFXQyxLQUFYLEtBQXFCLElBTDRCO0FBTS9Fa0MsTUFBQUEsZ0JBQWdCLEVBQUVKLGlCQUFpQixHQUM3QmIsV0FENkIsR0FFN0JWLFVBQVUsSUFBSSxLQUFLUixLQUFMLENBQVdDLEtBQVgsS0FBcUI7QUFSc0MsS0FBOUQsQ0FBckIsQ0E5QkssQ0F5Q0w7O0FBQ0EsVUFBTW1DLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFFBQUlDLFlBQUo7O0FBQ0EsUUFBSXRCLGNBQWMsSUFBSSxLQUFLakIsS0FBTCxDQUFXRyxRQUFqQyxFQUEyQztBQUN2QyxZQUFNcUMsYUFBYSxHQUFHckIsZ0JBQWdCLEdBQUdBLGdCQUFILEdBQXNCLEVBQTVEO0FBQ0FvQixNQUFBQSxZQUFZLEdBQUcsNkJBQUMsT0FBRDtBQUNYLFFBQUEsZ0JBQWdCLDZCQUFzQkMsYUFBdEIsQ0FETDtBQUVYLFFBQUEsT0FBTyxFQUFFLEtBQUt4QyxLQUFMLENBQVdVLGVBRlQ7QUFHWCxRQUFBLEtBQUssRUFBRU8sY0FBYyxJQUFJLEtBQUtqQixLQUFMLENBQVdHO0FBSHpCLFFBQWY7QUFLSDs7QUFFRCxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUU2QjtBQUFoQixPQUNGSCxlQURFLEVBRUZGLFVBRkUsRUFHSDtBQUFPLE1BQUEsT0FBTyxFQUFFLEtBQUt2QjtBQUFyQixPQUEwQixLQUFLYixLQUFMLENBQVdtQyxLQUFyQyxDQUhHLEVBSUZJLGdCQUpFLEVBS0ZTLFlBTEUsQ0FBUDtBQU9IOztBQW5Na0Q7Ozs4QkFBbENwRCxLLGVBQ0U7QUFDZjtBQUNBaUIsRUFBQUEsRUFBRSxFQUFFcUMsbUJBQVVDLE1BRkM7QUFHZjtBQUNBO0FBQ0E5QixFQUFBQSxPQUFPLEVBQUU2QixtQkFBVUUsS0FBVixDQUFnQixDQUFDLE9BQUQsRUFBVSxRQUFWLEVBQW9CLFVBQXBCLENBQWhCLENBTE07QUFNZjtBQUNBcEIsRUFBQUEsSUFBSSxFQUFFa0IsbUJBQVVDLE1BUEQ7QUFRZjtBQUNBdEIsRUFBQUEsSUFBSSxFQUFFcUIsbUJBQVVDLE1BVEQ7QUFVZjtBQUNBaEIsRUFBQUEsS0FBSyxFQUFFZSxtQkFBVUMsTUFYRjtBQVlmO0FBQ0FqQixFQUFBQSxXQUFXLEVBQUVnQixtQkFBVUMsTUFiUjtBQWNmO0FBQ0E7QUFDQWpDLEVBQUFBLEtBQUssRUFBRWdDLG1CQUFVQyxNQUFWLENBQWlCRSxVQWhCVDtBQWlCZjtBQUNBL0IsRUFBQUEsTUFBTSxFQUFFNEIsbUJBQVVJLElBbEJIO0FBbUJmO0FBQ0EvQixFQUFBQSxPQUFPLEVBQUUyQixtQkFBVUksSUFwQko7QUFxQmY7QUFDQTtBQUNBO0FBQ0E7QUFDQXJDLEVBQUFBLFVBQVUsRUFBRWlDLG1CQUFVSyxJQXpCUDtBQTBCZjtBQUNBNUIsRUFBQUEsV0FBVyxFQUFFdUIsbUJBQVVNLElBM0JSO0FBNEJmO0FBQ0E7QUFDQTlCLEVBQUFBLGNBQWMsRUFBRXdCLG1CQUFVSSxJQTlCWDtBQStCZjtBQUNBO0FBQ0ExQixFQUFBQSxnQkFBZ0IsRUFBRXNCLG1CQUFVQyxNQWpDYjtBQWtDZjtBQUNBM0IsRUFBQUEsU0FBUyxFQUFFMEIsbUJBQVVDLE1BbkNOLENBb0NmOztBQXBDZSxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgZGVib3VuY2UgfSBmcm9tICdsb2Rhc2gnO1xyXG5cclxuLy8gSW52b2tlIHZhbGlkYXRpb24gZnJvbSB1c2VyIGlucHV0ICh3aGVuIHR5cGluZywgZXRjLikgYXQgbW9zdCBvbmNlIGV2ZXJ5IE4gbXMuXHJcbmNvbnN0IFZBTElEQVRJT05fVEhST1RUTEVfTVMgPSAyMDA7XHJcblxyXG5jb25zdCBCQVNFX0lEID0gXCJteF9GaWVsZFwiO1xyXG5sZXQgY291bnQgPSAxO1xyXG5mdW5jdGlvbiBnZXRJZCgpIHtcclxuICAgIHJldHVybiBgJHtCQVNFX0lEfV8ke2NvdW50Kyt9YDtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRmllbGQgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLy8gVGhlIGZpZWxkJ3MgSUQsIHdoaWNoIGJpbmRzIHRoZSBpbnB1dCBhbmQgbGFiZWwgdG9nZXRoZXIuIEltbXV0YWJsZS5cclxuICAgICAgICBpZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICAvLyBUaGUgZWxlbWVudCB0byBjcmVhdGUuIERlZmF1bHRzIHRvIFwiaW5wdXRcIi5cclxuICAgICAgICAvLyBUbyBkZWZpbmUgb3B0aW9ucyBmb3IgYSBzZWxlY3QsIHVzZSA8RmllbGQ+PG9wdGlvbiAuLi4gLz48L0ZpZWxkPlxyXG4gICAgICAgIGVsZW1lbnQ6IFByb3BUeXBlcy5vbmVPZihbXCJpbnB1dFwiLCBcInNlbGVjdFwiLCBcInRleHRhcmVhXCJdKSxcclxuICAgICAgICAvLyBUaGUgZmllbGQncyB0eXBlICh3aGVuIHVzZWQgYXMgYW4gPGlucHV0PikuIERlZmF1bHRzIHRvIFwidGV4dFwiLlxyXG4gICAgICAgIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gaWQgb2YgYSA8ZGF0YWxpc3Q+IGVsZW1lbnQgZm9yIHN1Z2dlc3Rpb25zXHJcbiAgICAgICAgbGlzdDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICAvLyBUaGUgZmllbGQncyBsYWJlbCBzdHJpbmcuXHJcbiAgICAgICAgbGFiZWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gVGhlIGZpZWxkJ3MgcGxhY2Vob2xkZXIgc3RyaW5nLiBEZWZhdWx0cyB0byB0aGUgbGFiZWwuXHJcbiAgICAgICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gVGhlIGZpZWxkJ3MgdmFsdWUuXHJcbiAgICAgICAgLy8gVGhpcyBpcyBhIGNvbnRyb2xsZWQgY29tcG9uZW50LCBzbyB0aGUgdmFsdWUgaXMgcmVxdWlyZWQuXHJcbiAgICAgICAgdmFsdWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBPcHRpb25hbCBjb21wb25lbnQgdG8gaW5jbHVkZSBpbnNpZGUgdGhlIGZpZWxkIGJlZm9yZSB0aGUgaW5wdXQuXHJcbiAgICAgICAgcHJlZml4OiBQcm9wVHlwZXMubm9kZSxcclxuICAgICAgICAvLyBPcHRpb25hbCBjb21wb25lbnQgdG8gaW5jbHVkZSBpbnNpZGUgdGhlIGZpZWxkIGFmdGVyIHRoZSBpbnB1dC5cclxuICAgICAgICBwb3N0Zml4OiBQcm9wVHlwZXMubm9kZSxcclxuICAgICAgICAvLyBUaGUgY2FsbGJhY2sgY2FsbGVkIHdoZW5ldmVyIHRoZSBjb250ZW50cyBvZiB0aGUgZmllbGRcclxuICAgICAgICAvLyBjaGFuZ2VzLiAgUmV0dXJucyBhbiBvYmplY3Qgd2l0aCBgdmFsaWRgIGJvb2xlYW4gZmllbGRcclxuICAgICAgICAvLyBhbmQgYSBgZmVlZGJhY2tgIHJlYWN0IGNvbXBvbmVudCBmaWVsZCB0byBwcm92aWRlIGZlZWRiYWNrXHJcbiAgICAgICAgLy8gdG8gdGhlIHVzZXIuXHJcbiAgICAgICAgb25WYWxpZGF0ZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAgICAgLy8gSWYgc3BlY2lmaWVkLCBvdmVycmlkZXMgdGhlIHZhbHVlIHJldHVybmVkIGJ5IG9uVmFsaWRhdGUuXHJcbiAgICAgICAgZmxhZ0ludmFsaWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIC8vIElmIHNwZWNpZmllZCwgY29udGVudHMgd2lsbCBhcHBlYXIgYXMgYSB0b29sdGlwIG9uIHRoZSBlbGVtZW50IGFuZFxyXG4gICAgICAgIC8vIHZhbGlkYXRpb24gZmVlZGJhY2sgdG9vbHRpcHMgd2lsbCBiZSBzdXBwcmVzc2VkLlxyXG4gICAgICAgIHRvb2x0aXBDb250ZW50OiBQcm9wVHlwZXMubm9kZSxcclxuICAgICAgICAvLyBJZiBzcGVjaWZpZWQgYWxvbmdzaWRlIHRvb2x0aXBDb250ZW50LCB0aGUgY2xhc3MgbmFtZSB0byBhcHBseSB0byB0aGVcclxuICAgICAgICAvLyB0b29sdGlwIGl0c2VsZi5cclxuICAgICAgICB0b29sdGlwQ2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIC8vIElmIHNwZWNpZmllZCwgYW4gYWRkaXRpb25hbCBjbGFzcyBuYW1lIHRvIGFwcGx5IHRvIHRoZSBmaWVsZCBjb250YWluZXJcclxuICAgICAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgLy8gQWxsIG90aGVyIHByb3BzIHBhc3MgdGhyb3VnaCB0byB0aGUgPGlucHV0Pi5cclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgdmFsaWQ6IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgZmVlZGJhY2s6IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgZm9jdXNlZDogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5pZCA9IHRoaXMucHJvcHMuaWQgfHwgZ2V0SWQoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkZvY3VzID0gKGV2KSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGZvY3VzZWQ6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0ZSh7XHJcbiAgICAgICAgICAgIGZvY3VzZWQ6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gUGFyZW50IGNvbXBvbmVudCBtYXkgaGF2ZSBzdXBwbGllZCBpdHMgb3duIGBvbkZvY3VzYCBhcyB3ZWxsXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25Gb2N1cykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRm9jdXMoZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25DaGFuZ2UgPSAoZXYpID0+IHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRlT25DaGFuZ2UoKTtcclxuICAgICAgICAvLyBQYXJlbnQgY29tcG9uZW50IG1heSBoYXZlIHN1cHBsaWVkIGl0cyBvd24gYG9uQ2hhbmdlYCBhcyB3ZWxsXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25DaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZShldik7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvbkJsdXIgPSAoZXYpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZm9jdXNlZDogZmFsc2UsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0ZSh7XHJcbiAgICAgICAgICAgIGZvY3VzZWQ6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vIFBhcmVudCBjb21wb25lbnQgbWF5IGhhdmUgc3VwcGxpZWQgaXRzIG93biBgb25CbHVyYCBhcyB3ZWxsXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25CbHVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25CbHVyKGV2KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZvY3VzKCkge1xyXG4gICAgICAgIHRoaXMuaW5wdXQuZm9jdXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyB2YWxpZGF0ZSh7IGZvY3VzZWQsIGFsbG93RW1wdHkgPSB0cnVlIH0pIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMub25WYWxpZGF0ZSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5pbnB1dCA/IHRoaXMuaW5wdXQudmFsdWUgOiBudWxsO1xyXG4gICAgICAgIGNvbnN0IHsgdmFsaWQsIGZlZWRiYWNrIH0gPSBhd2FpdCB0aGlzLnByb3BzLm9uVmFsaWRhdGUoe1xyXG4gICAgICAgICAgICB2YWx1ZSxcclxuICAgICAgICAgICAgZm9jdXNlZCxcclxuICAgICAgICAgICAgYWxsb3dFbXB0eSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gdGhpcyBtZXRob2QgaXMgYXN5bmMgYW5kIHNvIHdlIG1heSBoYXZlIGJlZW4gYmx1cnJlZCBzaW5jZSB0aGUgbWV0aG9kIHdhcyBjYWxsZWRcclxuICAgICAgICAvLyBpZiB3ZSBoYXZlIHRoZW4gaGlkZSB0aGUgZmVlZGJhY2sgYXMgd2l0aFZhbGlkYXRpb24gZG9lc1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmZvY3VzZWQgJiYgZmVlZGJhY2spIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICB2YWxpZCxcclxuICAgICAgICAgICAgICAgIGZlZWRiYWNrLFxyXG4gICAgICAgICAgICAgICAgZmVlZGJhY2tWaXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBXaGVuIHdlIHJlY2VpdmUgbnVsbCBgZmVlZGJhY2tgLCB3ZSB3YW50IHRvIGhpZGUgdGhlIHRvb2x0aXAuXHJcbiAgICAgICAgICAgIC8vIFdlIGxlYXZlIHRoZSBwcmV2aW91cyBgZmVlZGJhY2tgIGNvbnRlbnQgaW4gc3RhdGUgd2l0aG91dCB1cGRhdGluZyBpdCxcclxuICAgICAgICAgICAgLy8gc28gdGhhdCB3ZSBjYW4gaGlkZSB0aGUgdG9vbHRpcCBjb250YWluaW5nIHRoZSBtb3N0IHJlY2VudCBmZWVkYmFja1xyXG4gICAgICAgICAgICAvLyB2aWEgQ1NTIGFuaW1hdGlvbi5cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICB2YWxpZCxcclxuICAgICAgICAgICAgICAgIGZlZWRiYWNrVmlzaWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVGhpcyB3YXMgY2hhbmdlZCBmcm9tIHRocm90dGxlIHRvIGRlYm91bmNlOiB0aGlzIGlzIG1vcmUgdHJhZGl0aW9uYWwgZm9yXHJcbiAgICAgKiBmb3JtIHZhbGlkYXRpb24gc2luY2UgaXQgbWVhbnMgdGhhdCB0aGUgdmFsaWRhdGlvbiBkb2Vzbid0IGhhcHBlbiBhdCBhbGxcclxuICAgICAqIHVudGlsIHRoZSB1c2VyIHN0b3BzIHR5cGluZyBmb3IgYSBiaXQgKGRlYm91bmNlIGRlZmF1bHRzIHRvIG5vdCBydW5uaW5nIG9uXHJcbiAgICAgKiB0aGUgbGVhZGluZyBlZGdlKS4gSWYgd2UncmUgZG9pbmcgYW4gSFRUUCBoaXQgb24gZWFjaCB2YWxpZGF0aW9uLCB3ZSBoYXZlIG1vcmVcclxuICAgICAqIGluY2VudGl2ZSB0byBwcmV2ZW50IHZhbGlkYXRpbmcgaW5wdXQgdGhhdCdzIHZlcnkgdW5saWtlbHkgdG8gYmUgdmFsaWQuXHJcbiAgICAgKiBXZSBtYXkgZmluZCB0aGF0IHdlIGFjdHVhbGx5IHdhbnQgZGlmZmVyZW50IGJlaGF2aW91ciBmb3IgcmVnaXN0cmF0aW9uXHJcbiAgICAgKiBmaWVsZHMsIGluIHdoaWNoIGNhc2Ugd2UgY2FuIGFkZCBzb21lIG9wdGlvbnMgdG8gY29udHJvbCBpdC5cclxuICAgICAqL1xyXG4gICAgdmFsaWRhdGVPbkNoYW5nZSA9IGRlYm91bmNlKCgpID0+IHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRlKHtcclxuICAgICAgICAgICAgZm9jdXNlZDogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sIFZBTElEQVRJT05fVEhST1RUTEVfTVMpO1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCB7XHJcbiAgICAgICAgICAgIGVsZW1lbnQsIHByZWZpeCwgcG9zdGZpeCwgY2xhc3NOYW1lLCBvblZhbGlkYXRlLCBjaGlsZHJlbixcclxuICAgICAgICAgICAgdG9vbHRpcENvbnRlbnQsIGZsYWdJbnZhbGlkLCB0b29sdGlwQ2xhc3NOYW1lLCBsaXN0LCAuLi5pbnB1dFByb3BzfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGNvbnN0IGlucHV0RWxlbWVudCA9IGVsZW1lbnQgfHwgXCJpbnB1dFwiO1xyXG5cclxuICAgICAgICAvLyBTZXQgc29tZSBkZWZhdWx0cyBmb3IgdGhlIDxpbnB1dD4gZWxlbWVudFxyXG4gICAgICAgIGlucHV0UHJvcHMudHlwZSA9IGlucHV0UHJvcHMudHlwZSB8fCBcInRleHRcIjtcclxuICAgICAgICBpbnB1dFByb3BzLnJlZiA9IGlucHV0ID0+IHRoaXMuaW5wdXQgPSBpbnB1dDtcclxuICAgICAgICBpbnB1dFByb3BzLnBsYWNlaG9sZGVyID0gaW5wdXRQcm9wcy5wbGFjZWhvbGRlciB8fCBpbnB1dFByb3BzLmxhYmVsO1xyXG4gICAgICAgIGlucHV0UHJvcHMuaWQgPSB0aGlzLmlkOyAvLyB0aGlzIG92ZXJ3cml0ZXMgdGhlIGlkIGZyb20gcHJvcHNcclxuXHJcbiAgICAgICAgaW5wdXRQcm9wcy5vbkZvY3VzID0gdGhpcy5vbkZvY3VzO1xyXG4gICAgICAgIGlucHV0UHJvcHMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlO1xyXG4gICAgICAgIGlucHV0UHJvcHMub25CbHVyID0gdGhpcy5vbkJsdXI7XHJcbiAgICAgICAgaW5wdXRQcm9wcy5saXN0ID0gbGlzdDtcclxuXHJcbiAgICAgICAgY29uc3QgZmllbGRJbnB1dCA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoaW5wdXRFbGVtZW50LCBpbnB1dFByb3BzLCBjaGlsZHJlbik7XHJcblxyXG4gICAgICAgIGxldCBwcmVmaXhDb250YWluZXIgPSBudWxsO1xyXG4gICAgICAgIGlmIChwcmVmaXgpIHtcclxuICAgICAgICAgICAgcHJlZml4Q29udGFpbmVyID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfRmllbGRfcHJlZml4XCI+e3ByZWZpeH08L3NwYW4+O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgcG9zdGZpeENvbnRhaW5lciA9IG51bGw7XHJcbiAgICAgICAgaWYgKHBvc3RmaXgpIHtcclxuICAgICAgICAgICAgcG9zdGZpeENvbnRhaW5lciA9IDxzcGFuIGNsYXNzTmFtZT1cIm14X0ZpZWxkX3Bvc3RmaXhcIj57cG9zdGZpeH08L3NwYW4+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaGFzVmFsaWRhdGlvbkZsYWcgPSBmbGFnSW52YWxpZCAhPT0gbnVsbCAmJiBmbGFnSW52YWxpZCAhPT0gdW5kZWZpbmVkO1xyXG4gICAgICAgIGNvbnN0IGZpZWxkQ2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9GaWVsZFwiLCBgbXhfRmllbGRfJHtpbnB1dEVsZW1lbnR9YCwgY2xhc3NOYW1lLCB7XHJcbiAgICAgICAgICAgIC8vIElmIHdlIGhhdmUgYSBwcmVmaXggZWxlbWVudCwgbGVhdmUgdGhlIGxhYmVsIGFsd2F5cyBhdCB0aGUgdG9wIGxlZnQgYW5kXHJcbiAgICAgICAgICAgIC8vIGRvbid0IGFuaW1hdGUgaXQsIGFzIGl0IGxvb2tzIGEgYml0IGNsdW5reSBhbmQgd291bGQgYWRkIGNvbXBsZXhpdHkgdG8gZG9cclxuICAgICAgICAgICAgLy8gcHJvcGVybHkuXHJcbiAgICAgICAgICAgIG14X0ZpZWxkX2xhYmVsQWx3YXlzVG9wTGVmdDogcHJlZml4LFxyXG4gICAgICAgICAgICBteF9GaWVsZF92YWxpZDogb25WYWxpZGF0ZSAmJiB0aGlzLnN0YXRlLnZhbGlkID09PSB0cnVlLFxyXG4gICAgICAgICAgICBteF9GaWVsZF9pbnZhbGlkOiBoYXNWYWxpZGF0aW9uRmxhZ1xyXG4gICAgICAgICAgICAgICAgPyBmbGFnSW52YWxpZFxyXG4gICAgICAgICAgICAgICAgOiBvblZhbGlkYXRlICYmIHRoaXMuc3RhdGUudmFsaWQgPT09IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBIYW5kbGUgZGlzcGxheWluZyBmZWVkYmFjayBvbiB2YWxpZGl0eVxyXG4gICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVG9vbHRpcFwiKTtcclxuICAgICAgICBsZXQgZmllbGRUb29sdGlwO1xyXG4gICAgICAgIGlmICh0b29sdGlwQ29udGVudCB8fCB0aGlzLnN0YXRlLmZlZWRiYWNrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFkZGxDbGFzc05hbWUgPSB0b29sdGlwQ2xhc3NOYW1lID8gdG9vbHRpcENsYXNzTmFtZSA6ICcnO1xyXG4gICAgICAgICAgICBmaWVsZFRvb2x0aXAgPSA8VG9vbHRpcFxyXG4gICAgICAgICAgICAgICAgdG9vbHRpcENsYXNzTmFtZT17YG14X0ZpZWxkX3Rvb2x0aXAgJHthZGRsQ2xhc3NOYW1lfWB9XHJcbiAgICAgICAgICAgICAgICB2aXNpYmxlPXt0aGlzLnN0YXRlLmZlZWRiYWNrVmlzaWJsZX1cclxuICAgICAgICAgICAgICAgIGxhYmVsPXt0b29sdGlwQ29udGVudCB8fCB0aGlzLnN0YXRlLmZlZWRiYWNrfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17ZmllbGRDbGFzc2VzfT5cclxuICAgICAgICAgICAge3ByZWZpeENvbnRhaW5lcn1cclxuICAgICAgICAgICAge2ZpZWxkSW5wdXR9XHJcbiAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPXt0aGlzLmlkfT57dGhpcy5wcm9wcy5sYWJlbH08L2xhYmVsPlxyXG4gICAgICAgICAgICB7cG9zdGZpeENvbnRhaW5lcn1cclxuICAgICAgICAgICAge2ZpZWxkVG9vbHRpcH1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuIl19