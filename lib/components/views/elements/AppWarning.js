"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

// eslint-disable-line no-unused-vars
const AppWarning = props => {
  return _react.default.createElement("div", {
    className: "mx_AppPermissionWarning"
  }, _react.default.createElement("div", {
    className: "mx_AppPermissionWarningImage"
  }, _react.default.createElement("img", {
    src: require("../../../../res/img/warning.svg"),
    alt: ""
  })), _react.default.createElement("div", {
    className: "mx_AppPermissionWarningText"
  }, _react.default.createElement("span", {
    className: "mx_AppPermissionWarningTextLabel"
  }, props.errorMsg)));
};

AppWarning.propTypes = {
  errorMsg: _propTypes.default.string
};
AppWarning.defaultProps = {
  errorMsg: 'Error'
};
var _default = AppWarning;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0FwcFdhcm5pbmcuanMiXSwibmFtZXMiOlsiQXBwV2FybmluZyIsInByb3BzIiwicmVxdWlyZSIsImVycm9yTXNnIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiZGVmYXVsdFByb3BzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFEMkI7QUFHM0IsTUFBTUEsVUFBVSxHQUFJQyxLQUFELElBQVc7QUFDMUIsU0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FDSTtBQUFLLElBQUEsR0FBRyxFQUFFQyxPQUFPLENBQUMsaUNBQUQsQ0FBakI7QUFBc0QsSUFBQSxHQUFHLEVBQUM7QUFBMUQsSUFESixDQURKLEVBSUk7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQ0k7QUFBTSxJQUFBLFNBQVMsRUFBQztBQUFoQixLQUFxREQsS0FBSyxDQUFDRSxRQUEzRCxDQURKLENBSkosQ0FESjtBQVVILENBWEQ7O0FBYUFILFVBQVUsQ0FBQ0ksU0FBWCxHQUF1QjtBQUNuQkQsRUFBQUEsUUFBUSxFQUFFRSxtQkFBVUM7QUFERCxDQUF2QjtBQUdBTixVQUFVLENBQUNPLFlBQVgsR0FBMEI7QUFDdEJKLEVBQUFBLFFBQVEsRUFBRTtBQURZLENBQTFCO2VBSWVILFUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVudXNlZC12YXJzXHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcblxyXG5jb25zdCBBcHBXYXJuaW5nID0gKHByb3BzKSA9PiB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9BcHBQZXJtaXNzaW9uV2FybmluZyc+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9BcHBQZXJtaXNzaW9uV2FybmluZ0ltYWdlJz5cclxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy93YXJuaW5nLnN2Z1wiKX0gYWx0PScnIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nbXhfQXBwUGVybWlzc2lvbldhcm5pbmdUZXh0Jz5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0nbXhfQXBwUGVybWlzc2lvbldhcm5pbmdUZXh0TGFiZWwnPnsgcHJvcHMuZXJyb3JNc2cgfTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG59O1xyXG5cclxuQXBwV2FybmluZy5wcm9wVHlwZXMgPSB7XHJcbiAgICBlcnJvck1zZzogUHJvcFR5cGVzLnN0cmluZyxcclxufTtcclxuQXBwV2FybmluZy5kZWZhdWx0UHJvcHMgPSB7XHJcbiAgICBlcnJvck1zZzogJ0Vycm9yJyxcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEFwcFdhcm5pbmc7XHJcbiJdfQ==