"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _languageHandler = require("../../../languageHandler");

var _UserAddress = require("../../../UserAddress.js");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'AddressTile',
  propTypes: {
    address: _UserAddress.UserAddressType.isRequired,
    canDismiss: _propTypes.default.bool,
    onDismissed: _propTypes.default.func,
    justified: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      canDismiss: false,
      onDismissed: function () {},
      // NOP
      justified: false
    };
  },
  render: function () {
    const address = this.props.address;
    const name = address.displayName || address.address;
    const imgUrls = [];
    const isMatrixAddress = ['mx-user-id', 'mx-room-id'].includes(address.addressType);

    if (isMatrixAddress && address.avatarMxc) {
      imgUrls.push(_MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(address.avatarMxc, 25, 25, 'crop'));
    } else if (address.addressType === 'email') {
      imgUrls.push(require("../../../../res/img/icon-email-user.svg"));
    } // Removing networks for now as they're not really supported

    /*
    var network;
    if (this.props.networkUrl !== "") {
        network = (
            <div className="mx_AddressTile_network">
                <BaseAvatar width={25} height={25} name={this.props.networkName} title="Riot" url={this.props.networkUrl} />
            </div>
        );
    }
    */


    const BaseAvatar = sdk.getComponent('avatars.BaseAvatar');
    const TintableSvg = sdk.getComponent("elements.TintableSvg");
    const nameClasses = (0, _classnames.default)({
      "mx_AddressTile_name": true,
      "mx_AddressTile_justified": this.props.justified
    });
    let info;
    let error = false;

    if (isMatrixAddress && address.isKnown) {
      const idClasses = (0, _classnames.default)({
        "mx_AddressTile_id": true,
        "mx_AddressTile_justified": this.props.justified
      });
      info = _react.default.createElement("div", {
        className: "mx_AddressTile_mx"
      }, _react.default.createElement("div", {
        className: nameClasses
      }, name), this.props.showAddress ? _react.default.createElement("div", {
        className: idClasses
      }, address.address) : _react.default.createElement("div", null));
    } else if (isMatrixAddress) {
      const unknownMxClasses = (0, _classnames.default)({
        "mx_AddressTile_unknownMx": true,
        "mx_AddressTile_justified": this.props.justified
      });
      info = _react.default.createElement("div", {
        className: unknownMxClasses
      }, this.props.address.address);
    } else if (address.addressType === "email") {
      const emailClasses = (0, _classnames.default)({
        "mx_AddressTile_email": true,
        "mx_AddressTile_justified": this.props.justified
      });
      let nameNode = null;

      if (address.displayName) {
        nameNode = _react.default.createElement("div", {
          className: nameClasses
        }, address.displayName);
      }

      info = _react.default.createElement("div", {
        className: "mx_AddressTile_mx"
      }, _react.default.createElement("div", {
        className: emailClasses
      }, address.address), nameNode);
    } else {
      error = true;
      const unknownClasses = (0, _classnames.default)({
        "mx_AddressTile_unknown": true,
        "mx_AddressTile_justified": this.props.justified
      });
      info = _react.default.createElement("div", {
        className: unknownClasses
      }, (0, _languageHandler._t)("Unknown Address"));
    }

    const classes = (0, _classnames.default)({
      "mx_AddressTile": true,
      "mx_AddressTile_error": error
    });
    let dismiss;

    if (this.props.canDismiss) {
      dismiss = _react.default.createElement("div", {
        className: "mx_AddressTile_dismiss",
        onClick: this.props.onDismissed
      }, _react.default.createElement(TintableSvg, {
        src: require("../../../../res/img/icon-address-delete.svg"),
        width: "9",
        height: "9"
      }));
    }

    return _react.default.createElement("div", {
      className: classes
    }, _react.default.createElement("div", {
      className: "mx_AddressTile_avatar"
    }, _react.default.createElement(BaseAvatar, {
      defaultToInitialLetter: true,
      width: 25,
      height: 25,
      name: name,
      title: name,
      urls: imgUrls
    })), info, dismiss);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0FkZHJlc3NUaWxlLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwiYWRkcmVzcyIsIlVzZXJBZGRyZXNzVHlwZSIsImlzUmVxdWlyZWQiLCJjYW5EaXNtaXNzIiwiUHJvcFR5cGVzIiwiYm9vbCIsIm9uRGlzbWlzc2VkIiwiZnVuYyIsImp1c3RpZmllZCIsImdldERlZmF1bHRQcm9wcyIsInJlbmRlciIsInByb3BzIiwibmFtZSIsImltZ1VybHMiLCJpc01hdHJpeEFkZHJlc3MiLCJpbmNsdWRlcyIsImFkZHJlc3NUeXBlIiwiYXZhdGFyTXhjIiwicHVzaCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm14Y1VybFRvSHR0cCIsInJlcXVpcmUiLCJCYXNlQXZhdGFyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiVGludGFibGVTdmciLCJuYW1lQ2xhc3NlcyIsImluZm8iLCJlcnJvciIsImlzS25vd24iLCJpZENsYXNzZXMiLCJzaG93QWRkcmVzcyIsInVua25vd25NeENsYXNzZXMiLCJlbWFpbENsYXNzZXMiLCJuYW1lTm9kZSIsInVua25vd25DbGFzc2VzIiwiY2xhc3NlcyIsImRpc21pc3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXhCQTs7Ozs7Ozs7Ozs7Ozs7OztlQTJCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxhQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsT0FBTyxFQUFFQyw2QkFBZ0JDLFVBRGxCO0FBRVBDLElBQUFBLFVBQVUsRUFBRUMsbUJBQVVDLElBRmY7QUFHUEMsSUFBQUEsV0FBVyxFQUFFRixtQkFBVUcsSUFIaEI7QUFJUEMsSUFBQUEsU0FBUyxFQUFFSixtQkFBVUM7QUFKZCxHQUhpQjtBQVU1QkksRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNITixNQUFBQSxVQUFVLEVBQUUsS0FEVDtBQUVIRyxNQUFBQSxXQUFXLEVBQUUsWUFBVyxDQUFFLENBRnZCO0FBRXlCO0FBQzVCRSxNQUFBQSxTQUFTLEVBQUU7QUFIUixLQUFQO0FBS0gsR0FoQjJCO0FBa0I1QkUsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNVixPQUFPLEdBQUcsS0FBS1csS0FBTCxDQUFXWCxPQUEzQjtBQUNBLFVBQU1ZLElBQUksR0FBR1osT0FBTyxDQUFDRixXQUFSLElBQXVCRSxPQUFPLENBQUNBLE9BQTVDO0FBRUEsVUFBTWEsT0FBTyxHQUFHLEVBQWhCO0FBQ0EsVUFBTUMsZUFBZSxHQUFHLENBQUMsWUFBRCxFQUFlLFlBQWYsRUFBNkJDLFFBQTdCLENBQXNDZixPQUFPLENBQUNnQixXQUE5QyxDQUF4Qjs7QUFFQSxRQUFJRixlQUFlLElBQUlkLE9BQU8sQ0FBQ2lCLFNBQS9CLEVBQTBDO0FBQ3RDSixNQUFBQSxPQUFPLENBQUNLLElBQVIsQ0FBYUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsWUFBdEIsQ0FDVHJCLE9BQU8sQ0FBQ2lCLFNBREMsRUFDVSxFQURWLEVBQ2MsRUFEZCxFQUNrQixNQURsQixDQUFiO0FBR0gsS0FKRCxNQUlPLElBQUlqQixPQUFPLENBQUNnQixXQUFSLEtBQXdCLE9BQTVCLEVBQXFDO0FBQ3hDSCxNQUFBQSxPQUFPLENBQUNLLElBQVIsQ0FBYUksT0FBTyxDQUFDLHlDQUFELENBQXBCO0FBQ0gsS0FiYyxDQWVmOztBQUNBOzs7Ozs7Ozs7Ozs7QUFXQSxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7QUFDQSxVQUFNQyxXQUFXLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBcEI7QUFFQSxVQUFNRSxXQUFXLEdBQUcseUJBQVc7QUFDM0IsNkJBQXVCLElBREk7QUFFM0Isa0NBQTRCLEtBQUtoQixLQUFMLENBQVdIO0FBRlosS0FBWCxDQUFwQjtBQUtBLFFBQUlvQixJQUFKO0FBQ0EsUUFBSUMsS0FBSyxHQUFHLEtBQVo7O0FBQ0EsUUFBSWYsZUFBZSxJQUFJZCxPQUFPLENBQUM4QixPQUEvQixFQUF3QztBQUNwQyxZQUFNQyxTQUFTLEdBQUcseUJBQVc7QUFDekIsNkJBQXFCLElBREk7QUFFekIsb0NBQTRCLEtBQUtwQixLQUFMLENBQVdIO0FBRmQsT0FBWCxDQUFsQjtBQUtBb0IsTUFBQUEsSUFBSSxHQUNBO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQUssUUFBQSxTQUFTLEVBQUVEO0FBQWhCLFNBQStCZixJQUEvQixDQURKLEVBRU0sS0FBS0QsS0FBTCxDQUFXcUIsV0FBWCxHQUNFO0FBQUssUUFBQSxTQUFTLEVBQUVEO0FBQWhCLFNBQTZCL0IsT0FBTyxDQUFDQSxPQUFyQyxDQURGLEdBRUUseUNBSlIsQ0FESjtBQVNILEtBZkQsTUFlTyxJQUFJYyxlQUFKLEVBQXFCO0FBQ3hCLFlBQU1tQixnQkFBZ0IsR0FBRyx5QkFBVztBQUNoQyxvQ0FBNEIsSUFESTtBQUVoQyxvQ0FBNEIsS0FBS3RCLEtBQUwsQ0FBV0g7QUFGUCxPQUFYLENBQXpCO0FBS0FvQixNQUFBQSxJQUFJLEdBQ0E7QUFBSyxRQUFBLFNBQVMsRUFBRUs7QUFBaEIsU0FBb0MsS0FBS3RCLEtBQUwsQ0FBV1gsT0FBWCxDQUFtQkEsT0FBdkQsQ0FESjtBQUdILEtBVE0sTUFTQSxJQUFJQSxPQUFPLENBQUNnQixXQUFSLEtBQXdCLE9BQTVCLEVBQXFDO0FBQ3hDLFlBQU1rQixZQUFZLEdBQUcseUJBQVc7QUFDNUIsZ0NBQXdCLElBREk7QUFFNUIsb0NBQTRCLEtBQUt2QixLQUFMLENBQVdIO0FBRlgsT0FBWCxDQUFyQjtBQUtBLFVBQUkyQixRQUFRLEdBQUcsSUFBZjs7QUFDQSxVQUFJbkMsT0FBTyxDQUFDRixXQUFaLEVBQXlCO0FBQ3JCcUMsUUFBQUEsUUFBUSxHQUFHO0FBQUssVUFBQSxTQUFTLEVBQUVSO0FBQWhCLFdBQStCM0IsT0FBTyxDQUFDRixXQUF2QyxDQUFYO0FBQ0g7O0FBRUQ4QixNQUFBQSxJQUFJLEdBQ0E7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBRU07QUFBaEIsU0FBZ0NsQyxPQUFPLENBQUNBLE9BQXhDLENBREosRUFFTW1DLFFBRk4sQ0FESjtBQU1ILEtBakJNLE1BaUJBO0FBQ0hOLE1BQUFBLEtBQUssR0FBRyxJQUFSO0FBQ0EsWUFBTU8sY0FBYyxHQUFHLHlCQUFXO0FBQzlCLGtDQUEwQixJQURJO0FBRTlCLG9DQUE0QixLQUFLekIsS0FBTCxDQUFXSDtBQUZULE9BQVgsQ0FBdkI7QUFLQW9CLE1BQUFBLElBQUksR0FDQTtBQUFLLFFBQUEsU0FBUyxFQUFFUTtBQUFoQixTQUFrQyx5QkFBRyxpQkFBSCxDQUFsQyxDQURKO0FBR0g7O0FBRUQsVUFBTUMsT0FBTyxHQUFHLHlCQUFXO0FBQ3ZCLHdCQUFrQixJQURLO0FBRXZCLDhCQUF3QlI7QUFGRCxLQUFYLENBQWhCO0FBS0EsUUFBSVMsT0FBSjs7QUFDQSxRQUFJLEtBQUszQixLQUFMLENBQVdSLFVBQWYsRUFBMkI7QUFDdkJtQyxNQUFBQSxPQUFPLEdBQ0g7QUFBSyxRQUFBLFNBQVMsRUFBQyx3QkFBZjtBQUF3QyxRQUFBLE9BQU8sRUFBRSxLQUFLM0IsS0FBTCxDQUFXTDtBQUE1RCxTQUNJLDZCQUFDLFdBQUQ7QUFBYSxRQUFBLEdBQUcsRUFBRWdCLE9BQU8sQ0FBQyw2Q0FBRCxDQUF6QjtBQUEwRSxRQUFBLEtBQUssRUFBQyxHQUFoRjtBQUFvRixRQUFBLE1BQU0sRUFBQztBQUEzRixRQURKLENBREo7QUFLSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUVlO0FBQWhCLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsc0JBQXNCLEVBQUUsSUFBcEM7QUFBMEMsTUFBQSxLQUFLLEVBQUUsRUFBakQ7QUFBcUQsTUFBQSxNQUFNLEVBQUUsRUFBN0Q7QUFBaUUsTUFBQSxJQUFJLEVBQUV6QixJQUF2RTtBQUE2RSxNQUFBLEtBQUssRUFBRUEsSUFBcEY7QUFBMEYsTUFBQSxJQUFJLEVBQUVDO0FBQWhHLE1BREosQ0FESixFQUlNZSxJQUpOLEVBS01VLE9BTE4sQ0FESjtBQVNIO0FBbkkyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7IFVzZXJBZGRyZXNzVHlwZSB9IGZyb20gJy4uLy4uLy4uL1VzZXJBZGRyZXNzLmpzJztcclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnQWRkcmVzc1RpbGUnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIGFkZHJlc3M6IFVzZXJBZGRyZXNzVHlwZS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGNhbkRpc21pc3M6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIG9uRGlzbWlzc2VkOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBqdXN0aWZpZWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNhbkRpc21pc3M6IGZhbHNlLFxyXG4gICAgICAgICAgICBvbkRpc21pc3NlZDogZnVuY3Rpb24oKSB7fSwgLy8gTk9QXHJcbiAgICAgICAgICAgIGp1c3RpZmllZDogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBhZGRyZXNzID0gdGhpcy5wcm9wcy5hZGRyZXNzO1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSBhZGRyZXNzLmRpc3BsYXlOYW1lIHx8IGFkZHJlc3MuYWRkcmVzcztcclxuXHJcbiAgICAgICAgY29uc3QgaW1nVXJscyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGlzTWF0cml4QWRkcmVzcyA9IFsnbXgtdXNlci1pZCcsICdteC1yb29tLWlkJ10uaW5jbHVkZXMoYWRkcmVzcy5hZGRyZXNzVHlwZSk7XHJcblxyXG4gICAgICAgIGlmIChpc01hdHJpeEFkZHJlc3MgJiYgYWRkcmVzcy5hdmF0YXJNeGMpIHtcclxuICAgICAgICAgICAgaW1nVXJscy5wdXNoKE1hdHJpeENsaWVudFBlZy5nZXQoKS5teGNVcmxUb0h0dHAoXHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzLmF2YXRhck14YywgMjUsIDI1LCAnY3JvcCcsXHJcbiAgICAgICAgICAgICkpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoYWRkcmVzcy5hZGRyZXNzVHlwZSA9PT0gJ2VtYWlsJykge1xyXG4gICAgICAgICAgICBpbWdVcmxzLnB1c2gocmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbi1lbWFpbC11c2VyLnN2Z1wiKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZW1vdmluZyBuZXR3b3JrcyBmb3Igbm93IGFzIHRoZXkncmUgbm90IHJlYWxseSBzdXBwb3J0ZWRcclxuICAgICAgICAvKlxyXG4gICAgICAgIHZhciBuZXR3b3JrO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm5ldHdvcmtVcmwgIT09IFwiXCIpIHtcclxuICAgICAgICAgICAgbmV0d29yayA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQWRkcmVzc1RpbGVfbmV0d29ya1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxCYXNlQXZhdGFyIHdpZHRoPXsyNX0gaGVpZ2h0PXsyNX0gbmFtZT17dGhpcy5wcm9wcy5uZXR3b3JrTmFtZX0gdGl0bGU9XCJSaW90XCIgdXJsPXt0aGlzLnByb3BzLm5ldHdvcmtVcmx9IC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgKi9cclxuXHJcbiAgICAgICAgY29uc3QgQmFzZUF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoJ2F2YXRhcnMuQmFzZUF2YXRhcicpO1xyXG4gICAgICAgIGNvbnN0IFRpbnRhYmxlU3ZnID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlRpbnRhYmxlU3ZnXCIpO1xyXG5cclxuICAgICAgICBjb25zdCBuYW1lQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICBcIm14X0FkZHJlc3NUaWxlX25hbWVcIjogdHJ1ZSxcclxuICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZV9qdXN0aWZpZWRcIjogdGhpcy5wcm9wcy5qdXN0aWZpZWQsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGxldCBpbmZvO1xyXG4gICAgICAgIGxldCBlcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGlmIChpc01hdHJpeEFkZHJlc3MgJiYgYWRkcmVzcy5pc0tub3duKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlkQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZV9pZFwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZV9qdXN0aWZpZWRcIjogdGhpcy5wcm9wcy5qdXN0aWZpZWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaW5mbyA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQWRkcmVzc1RpbGVfbXhcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17bmFtZUNsYXNzZXN9PnsgbmFtZSB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLnNob3dBZGRyZXNzID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2lkQ2xhc3Nlc30+eyBhZGRyZXNzLmFkZHJlc3MgfTwvZGl2PiA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgLz5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGlzTWF0cml4QWRkcmVzcykge1xyXG4gICAgICAgICAgICBjb25zdCB1bmtub3duTXhDbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICAgICBcIm14X0FkZHJlc3NUaWxlX3Vua25vd25NeFwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZV9qdXN0aWZpZWRcIjogdGhpcy5wcm9wcy5qdXN0aWZpZWQsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaW5mbyA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXt1bmtub3duTXhDbGFzc2VzfT57IHRoaXMucHJvcHMuYWRkcmVzcy5hZGRyZXNzIH08L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGFkZHJlc3MuYWRkcmVzc1R5cGUgPT09IFwiZW1haWxcIikge1xyXG4gICAgICAgICAgICBjb25zdCBlbWFpbENsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgICAgIFwibXhfQWRkcmVzc1RpbGVfZW1haWxcIjogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIFwibXhfQWRkcmVzc1RpbGVfanVzdGlmaWVkXCI6IHRoaXMucHJvcHMuanVzdGlmaWVkLFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGxldCBuYW1lTm9kZSA9IG51bGw7XHJcbiAgICAgICAgICAgIGlmIChhZGRyZXNzLmRpc3BsYXlOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBuYW1lTm9kZSA9IDxkaXYgY2xhc3NOYW1lPXtuYW1lQ2xhc3Nlc30+eyBhZGRyZXNzLmRpc3BsYXlOYW1lIH08L2Rpdj47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGluZm8gPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NUaWxlX214XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2VtYWlsQ2xhc3Nlc30+eyBhZGRyZXNzLmFkZHJlc3MgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgbmFtZU5vZGUgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICBjb25zdCB1bmtub3duQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZV91bmtub3duXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBcIm14X0FkZHJlc3NUaWxlX2p1c3RpZmllZFwiOiB0aGlzLnByb3BzLmp1c3RpZmllZCxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBpbmZvID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Vua25vd25DbGFzc2VzfT57IF90KFwiVW5rbm93biBBZGRyZXNzXCIpIH08L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGNsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgXCJteF9BZGRyZXNzVGlsZVwiOiB0cnVlLFxyXG4gICAgICAgICAgICBcIm14X0FkZHJlc3NUaWxlX2Vycm9yXCI6IGVycm9yLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgZGlzbWlzcztcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jYW5EaXNtaXNzKSB7XHJcbiAgICAgICAgICAgIGRpc21pc3MgPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NUaWxlX2Rpc21pc3NcIiBvbkNsaWNrPXt0aGlzLnByb3BzLm9uRGlzbWlzc2VkfSA+XHJcbiAgICAgICAgICAgICAgICAgICAgPFRpbnRhYmxlU3ZnIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbi1hZGRyZXNzLWRlbGV0ZS5zdmdcIil9IHdpZHRoPVwiOVwiIGhlaWdodD1cIjlcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0FkZHJlc3NUaWxlX2F2YXRhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxCYXNlQXZhdGFyIGRlZmF1bHRUb0luaXRpYWxMZXR0ZXI9e3RydWV9IHdpZHRoPXsyNX0gaGVpZ2h0PXsyNX0gbmFtZT17bmFtZX0gdGl0bGU9e25hbWV9IHVybHM9e2ltZ1VybHN9IC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIHsgaW5mbyB9XHJcbiAgICAgICAgICAgICAgICB7IGRpc21pc3MgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==