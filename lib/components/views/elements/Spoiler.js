"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

/*
 Copyright 2019 Sorunome

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
class Spoiler extends _react.default.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  toggleVisible(e) {
    if (!this.state.visible) {
      // we are un-blurring, we don't want this click to propagate to potential child pills
      e.preventDefault();
      e.stopPropagation();
    }

    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    const reason = this.props.reason ? _react.default.createElement("span", {
      className: "mx_EventTile_spoiler_reason"
    }, "(" + this.props.reason + ")") : null; // react doesn't allow appending a DOM node as child.
    // as such, we pass the this.props.contentHtml instead and then set the raw
    // HTML content. This is secure as the contents have already been parsed previously

    return _react.default.createElement("span", {
      className: "mx_EventTile_spoiler" + (this.state.visible ? " visible" : ""),
      onClick: this.toggleVisible.bind(this)
    }, reason, "\xA0", _react.default.createElement("span", {
      className: "mx_EventTile_spoiler_content",
      dangerouslySetInnerHTML: {
        __html: this.props.contentHtml
      }
    }));
  }

}

exports.default = Spoiler;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1Nwb2lsZXIuanMiXSwibmFtZXMiOlsiU3BvaWxlciIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInN0YXRlIiwidmlzaWJsZSIsInRvZ2dsZVZpc2libGUiLCJlIiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJzZXRTdGF0ZSIsInJlbmRlciIsInJlYXNvbiIsImJpbmQiLCJfX2h0bWwiLCJjb250ZW50SHRtbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBZ0JBOztBQWhCQTs7Ozs7Ozs7Ozs7Ozs7O0FBa0JlLE1BQU1BLE9BQU4sU0FBc0JDLGVBQU1DLFNBQTVCLENBQXNDO0FBQ2pEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFDQSxTQUFLQyxLQUFMLEdBQWE7QUFDVEMsTUFBQUEsT0FBTyxFQUFFO0FBREEsS0FBYjtBQUdIOztBQUVEQyxFQUFBQSxhQUFhLENBQUNDLENBQUQsRUFBSTtBQUNiLFFBQUksQ0FBQyxLQUFLSCxLQUFMLENBQVdDLE9BQWhCLEVBQXlCO0FBQ3JCO0FBQ0FFLE1BQUFBLENBQUMsQ0FBQ0MsY0FBRjtBQUNBRCxNQUFBQSxDQUFDLENBQUNFLGVBQUY7QUFDSDs7QUFDRCxTQUFLQyxRQUFMLENBQWM7QUFBRUwsTUFBQUEsT0FBTyxFQUFFLENBQUMsS0FBS0QsS0FBTCxDQUFXQztBQUF2QixLQUFkO0FBQ0g7O0FBRURNLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE1BQU0sR0FBRyxLQUFLVCxLQUFMLENBQVdTLE1BQVgsR0FDWDtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQStDLE1BQU0sS0FBS1QsS0FBTCxDQUFXUyxNQUFqQixHQUEwQixHQUF6RSxDQURXLEdBRVgsSUFGSixDQURLLENBSUw7QUFDQTtBQUNBOztBQUNBLFdBQ0k7QUFBTSxNQUFBLFNBQVMsRUFBRSwwQkFBMEIsS0FBS1IsS0FBTCxDQUFXQyxPQUFYLEdBQXFCLFVBQXJCLEdBQWtDLEVBQTVELENBQWpCO0FBQWtGLE1BQUEsT0FBTyxFQUFFLEtBQUtDLGFBQUwsQ0FBbUJPLElBQW5CLENBQXdCLElBQXhCO0FBQTNGLE9BQ01ELE1BRE4sVUFHSTtBQUFNLE1BQUEsU0FBUyxFQUFDLDhCQUFoQjtBQUErQyxNQUFBLHVCQUF1QixFQUFFO0FBQUVFLFFBQUFBLE1BQU0sRUFBRSxLQUFLWCxLQUFMLENBQVdZO0FBQXJCO0FBQXhFLE1BSEosQ0FESjtBQU9IOztBQS9CZ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4gQ29weXJpZ2h0IDIwMTkgU29ydW5vbWVcclxuXHJcbiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4geW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG4gVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3BvaWxlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICB2aXNpYmxlOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZVZpc2libGUoZSkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS52aXNpYmxlKSB7XHJcbiAgICAgICAgICAgIC8vIHdlIGFyZSB1bi1ibHVycmluZywgd2UgZG9uJ3Qgd2FudCB0aGlzIGNsaWNrIHRvIHByb3BhZ2F0ZSB0byBwb3RlbnRpYWwgY2hpbGQgcGlsbHNcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgdmlzaWJsZTogIXRoaXMuc3RhdGUudmlzaWJsZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgcmVhc29uID0gdGhpcy5wcm9wcy5yZWFzb24gPyAoXHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9zcG9pbGVyX3JlYXNvblwiPntcIihcIiArIHRoaXMucHJvcHMucmVhc29uICsgXCIpXCJ9PC9zcGFuPlxyXG4gICAgICAgICkgOiBudWxsO1xyXG4gICAgICAgIC8vIHJlYWN0IGRvZXNuJ3QgYWxsb3cgYXBwZW5kaW5nIGEgRE9NIG5vZGUgYXMgY2hpbGQuXHJcbiAgICAgICAgLy8gYXMgc3VjaCwgd2UgcGFzcyB0aGUgdGhpcy5wcm9wcy5jb250ZW50SHRtbCBpbnN0ZWFkIGFuZCB0aGVuIHNldCB0aGUgcmF3XHJcbiAgICAgICAgLy8gSFRNTCBjb250ZW50LiBUaGlzIGlzIHNlY3VyZSBhcyB0aGUgY29udGVudHMgaGF2ZSBhbHJlYWR5IGJlZW4gcGFyc2VkIHByZXZpb3VzbHlcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e1wibXhfRXZlbnRUaWxlX3Nwb2lsZXJcIiArICh0aGlzLnN0YXRlLnZpc2libGUgPyBcIiB2aXNpYmxlXCIgOiBcIlwiKX0gb25DbGljaz17dGhpcy50b2dnbGVWaXNpYmxlLmJpbmQodGhpcyl9PlxyXG4gICAgICAgICAgICAgICAgeyByZWFzb24gfVxyXG4gICAgICAgICAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfc3BvaWxlcl9jb250ZW50XCIgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiB0aGlzLnByb3BzLmNvbnRlbnRIdG1sIH19IC8+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==