"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _languageHandler = require("../../../languageHandler");

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Validation = _interopRequireDefault(require("./Validation"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Controlled form component wrapping Field for inputting a room alias scoped to a given domain
class RoomAliasField extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onChange", ev => {
      if (this.props.onChange) {
        this.props.onChange(this._asFullAlias(ev.target.value));
      }
    });
    (0, _defineProperty2.default)(this, "_onValidate", async fieldState => {
      const result = await this._validationRules(fieldState);
      this.setState({
        isValid: result.valid
      });
      return result;
    });
    (0, _defineProperty2.default)(this, "_validationRules", (0, _Validation.default)({
      rules: [{
        key: "safeLocalpart",
        test: async ({
          value
        }) => {
          if (!value) {
            return true;
          }

          const fullAlias = this._asFullAlias(value); // XXX: FIXME https://github.com/matrix-org/matrix-doc/issues/668


          return !value.includes("#") && !value.includes(":") && !value.includes(",") && encodeURI(fullAlias) === fullAlias;
        },
        invalid: () => (0, _languageHandler._t)("Some characters not allowed")
      }, {
        key: "required",
        test: async ({
          value,
          allowEmpty
        }) => allowEmpty || !!value,
        invalid: () => (0, _languageHandler._t)("Please provide a room alias")
      }, {
        key: "taken",
        final: true,
        test: async ({
          value
        }) => {
          if (!value) {
            return true;
          }

          const client = _MatrixClientPeg.MatrixClientPeg.get();

          try {
            await client.getRoomIdForAlias(this._asFullAlias(value)); // we got a room id, so the alias is taken

            return false;
          } catch (err) {
            // any server error code will do,
            // either it M_NOT_FOUND or the alias is invalid somehow,
            // in which case we don't want to show the invalid message
            return !!err.errcode;
          }
        },
        valid: () => (0, _languageHandler._t)("This alias is available to use"),
        invalid: () => (0, _languageHandler._t)("This alias is already in use")
      }]
    }));
    this.state = {
      isValid: true
    };
  }

  _asFullAlias(localpart) {
    return "#".concat(localpart, ":").concat(this.props.domain);
  }

  render() {
    const Field = sdk.getComponent('views.elements.Field');

    const poundSign = _react.default.createElement("span", null, "#");

    const aliasPostfix = ":" + this.props.domain;

    const domain = _react.default.createElement("span", {
      title: aliasPostfix
    }, aliasPostfix);

    const maxlength = 255 - this.props.domain.length - 2; // 2 for # and :

    return _react.default.createElement(Field, {
      label: (0, _languageHandler._t)("Room alias"),
      className: "mx_RoomAliasField",
      prefix: poundSign,
      postfix: domain,
      ref: ref => this._fieldRef = ref,
      onValidate: this._onValidate,
      placeholder: (0, _languageHandler._t)("e.g. my-room"),
      onChange: this._onChange,
      value: this.props.value.substring(1, this.props.value.length - this.props.domain.length - 1),
      maxLength: maxlength
    });
  }

  get isValid() {
    return this.state.isValid;
  }

  validate(options) {
    return this._fieldRef.validate(options);
  }

  focus() {
    this._fieldRef.focus();
  }

}

exports.default = RoomAliasField;
(0, _defineProperty2.default)(RoomAliasField, "propTypes", {
  domain: _propTypes.default.string.isRequired,
  onChange: _propTypes.default.func,
  value: _propTypes.default.string.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1Jvb21BbGlhc0ZpZWxkLmpzIl0sIm5hbWVzIjpbIlJvb21BbGlhc0ZpZWxkIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsImV2Iiwib25DaGFuZ2UiLCJfYXNGdWxsQWxpYXMiLCJ0YXJnZXQiLCJ2YWx1ZSIsImZpZWxkU3RhdGUiLCJyZXN1bHQiLCJfdmFsaWRhdGlvblJ1bGVzIiwic2V0U3RhdGUiLCJpc1ZhbGlkIiwidmFsaWQiLCJydWxlcyIsImtleSIsInRlc3QiLCJmdWxsQWxpYXMiLCJpbmNsdWRlcyIsImVuY29kZVVSSSIsImludmFsaWQiLCJhbGxvd0VtcHR5IiwiZmluYWwiLCJjbGllbnQiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJnZXRSb29tSWRGb3JBbGlhcyIsImVyciIsImVycmNvZGUiLCJzdGF0ZSIsImxvY2FscGFydCIsImRvbWFpbiIsInJlbmRlciIsIkZpZWxkIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwicG91bmRTaWduIiwiYWxpYXNQb3N0Zml4IiwibWF4bGVuZ3RoIiwibGVuZ3RoIiwicmVmIiwiX2ZpZWxkUmVmIiwiX29uVmFsaWRhdGUiLCJfb25DaGFuZ2UiLCJzdWJzdHJpbmciLCJ2YWxpZGF0ZSIsIm9wdGlvbnMiLCJmb2N1cyIsIlByb3BUeXBlcyIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBcEJBOzs7Ozs7Ozs7Ozs7Ozs7QUFzQkE7QUFDZSxNQUFNQSxjQUFOLFNBQTZCQyxlQUFNQyxhQUFuQyxDQUFpRDtBQU81REMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUscURBOEJOQyxFQUFELElBQVE7QUFDaEIsVUFBSSxLQUFLRCxLQUFMLENBQVdFLFFBQWYsRUFBeUI7QUFDckIsYUFBS0YsS0FBTCxDQUFXRSxRQUFYLENBQW9CLEtBQUtDLFlBQUwsQ0FBa0JGLEVBQUUsQ0FBQ0csTUFBSCxDQUFVQyxLQUE1QixDQUFwQjtBQUNIO0FBQ0osS0FsQ2tCO0FBQUEsdURBb0NMLE1BQU9DLFVBQVAsSUFBc0I7QUFDaEMsWUFBTUMsTUFBTSxHQUFHLE1BQU0sS0FBS0MsZ0JBQUwsQ0FBc0JGLFVBQXRCLENBQXJCO0FBQ0EsV0FBS0csUUFBTCxDQUFjO0FBQUNDLFFBQUFBLE9BQU8sRUFBRUgsTUFBTSxDQUFDSTtBQUFqQixPQUFkO0FBQ0EsYUFBT0osTUFBUDtBQUNILEtBeENrQjtBQUFBLDREQTBDQSx5QkFBZTtBQUM5QkssTUFBQUEsS0FBSyxFQUFFLENBQ0g7QUFDSUMsUUFBQUEsR0FBRyxFQUFFLGVBRFQ7QUFFSUMsUUFBQUEsSUFBSSxFQUFFLE9BQU87QUFBRVQsVUFBQUE7QUFBRixTQUFQLEtBQXFCO0FBQ3ZCLGNBQUksQ0FBQ0EsS0FBTCxFQUFZO0FBQ1IsbUJBQU8sSUFBUDtBQUNIOztBQUNELGdCQUFNVSxTQUFTLEdBQUcsS0FBS1osWUFBTCxDQUFrQkUsS0FBbEIsQ0FBbEIsQ0FKdUIsQ0FLdkI7OztBQUNBLGlCQUFPLENBQUNBLEtBQUssQ0FBQ1csUUFBTixDQUFlLEdBQWYsQ0FBRCxJQUF3QixDQUFDWCxLQUFLLENBQUNXLFFBQU4sQ0FBZSxHQUFmLENBQXpCLElBQWdELENBQUNYLEtBQUssQ0FBQ1csUUFBTixDQUFlLEdBQWYsQ0FBakQsSUFDSEMsU0FBUyxDQUFDRixTQUFELENBQVQsS0FBeUJBLFNBRDdCO0FBRUgsU0FWTDtBQVdJRyxRQUFBQSxPQUFPLEVBQUUsTUFBTSx5QkFBRyw2QkFBSDtBQVhuQixPQURHLEVBYUE7QUFDQ0wsUUFBQUEsR0FBRyxFQUFFLFVBRE47QUFFQ0MsUUFBQUEsSUFBSSxFQUFFLE9BQU87QUFBRVQsVUFBQUEsS0FBRjtBQUFTYyxVQUFBQTtBQUFULFNBQVAsS0FBaUNBLFVBQVUsSUFBSSxDQUFDLENBQUNkLEtBRnhEO0FBR0NhLFFBQUFBLE9BQU8sRUFBRSxNQUFNLHlCQUFHLDZCQUFIO0FBSGhCLE9BYkEsRUFpQkE7QUFDQ0wsUUFBQUEsR0FBRyxFQUFFLE9BRE47QUFFQ08sUUFBQUEsS0FBSyxFQUFFLElBRlI7QUFHQ04sUUFBQUEsSUFBSSxFQUFFLE9BQU87QUFBQ1QsVUFBQUE7QUFBRCxTQUFQLEtBQW1CO0FBQ3JCLGNBQUksQ0FBQ0EsS0FBTCxFQUFZO0FBQ1IsbUJBQU8sSUFBUDtBQUNIOztBQUNELGdCQUFNZ0IsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsY0FBSTtBQUNBLGtCQUFNRixNQUFNLENBQUNHLGlCQUFQLENBQXlCLEtBQUtyQixZQUFMLENBQWtCRSxLQUFsQixDQUF6QixDQUFOLENBREEsQ0FFQTs7QUFDQSxtQkFBTyxLQUFQO0FBQ0gsV0FKRCxDQUlFLE9BQU9vQixHQUFQLEVBQVk7QUFDVjtBQUNBO0FBQ0E7QUFDQSxtQkFBTyxDQUFDLENBQUNBLEdBQUcsQ0FBQ0MsT0FBYjtBQUNIO0FBQ0osU0FsQkY7QUFtQkNmLFFBQUFBLEtBQUssRUFBRSxNQUFNLHlCQUFHLGdDQUFILENBbkJkO0FBb0JDTyxRQUFBQSxPQUFPLEVBQUUsTUFBTSx5QkFBRyw4QkFBSDtBQXBCaEIsT0FqQkE7QUFEdUIsS0FBZixDQTFDQTtBQUVmLFNBQUtTLEtBQUwsR0FBYTtBQUFDakIsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBYjtBQUNIOztBQUVEUCxFQUFBQSxZQUFZLENBQUN5QixTQUFELEVBQVk7QUFDcEIsc0JBQVdBLFNBQVgsY0FBd0IsS0FBSzVCLEtBQUwsQ0FBVzZCLE1BQW5DO0FBQ0g7O0FBRURDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLEtBQUssR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFkOztBQUNBLFVBQU1DLFNBQVMsR0FBSSwrQ0FBbkI7O0FBQ0EsVUFBTUMsWUFBWSxHQUFHLE1BQU0sS0FBS25DLEtBQUwsQ0FBVzZCLE1BQXRDOztBQUNBLFVBQU1BLE1BQU0sR0FBSTtBQUFNLE1BQUEsS0FBSyxFQUFFTTtBQUFiLE9BQTRCQSxZQUE1QixDQUFoQjs7QUFDQSxVQUFNQyxTQUFTLEdBQUcsTUFBTSxLQUFLcEMsS0FBTCxDQUFXNkIsTUFBWCxDQUFrQlEsTUFBeEIsR0FBaUMsQ0FBbkQsQ0FMSyxDQUttRDs7QUFDeEQsV0FDUSw2QkFBQyxLQUFEO0FBQ0ksTUFBQSxLQUFLLEVBQUUseUJBQUcsWUFBSCxDQURYO0FBRUksTUFBQSxTQUFTLEVBQUMsbUJBRmQ7QUFHSSxNQUFBLE1BQU0sRUFBRUgsU0FIWjtBQUlJLE1BQUEsT0FBTyxFQUFFTCxNQUpiO0FBS0ksTUFBQSxHQUFHLEVBQUVTLEdBQUcsSUFBSSxLQUFLQyxTQUFMLEdBQWlCRCxHQUxqQztBQU1JLE1BQUEsVUFBVSxFQUFFLEtBQUtFLFdBTnJCO0FBT0ksTUFBQSxXQUFXLEVBQUUseUJBQUcsY0FBSCxDQVBqQjtBQVFJLE1BQUEsUUFBUSxFQUFFLEtBQUtDLFNBUm5CO0FBU0ksTUFBQSxLQUFLLEVBQUUsS0FBS3pDLEtBQUwsQ0FBV0ssS0FBWCxDQUFpQnFDLFNBQWpCLENBQTJCLENBQTNCLEVBQThCLEtBQUsxQyxLQUFMLENBQVdLLEtBQVgsQ0FBaUJnQyxNQUFqQixHQUEwQixLQUFLckMsS0FBTCxDQUFXNkIsTUFBWCxDQUFrQlEsTUFBNUMsR0FBcUQsQ0FBbkYsQ0FUWDtBQVVJLE1BQUEsU0FBUyxFQUFFRDtBQVZmLE1BRFI7QUFhSDs7QUF5REQsTUFBSTFCLE9BQUosR0FBYztBQUNWLFdBQU8sS0FBS2lCLEtBQUwsQ0FBV2pCLE9BQWxCO0FBQ0g7O0FBRURpQyxFQUFBQSxRQUFRLENBQUNDLE9BQUQsRUFBVTtBQUNkLFdBQU8sS0FBS0wsU0FBTCxDQUFlSSxRQUFmLENBQXdCQyxPQUF4QixDQUFQO0FBQ0g7O0FBRURDLEVBQUFBLEtBQUssR0FBRztBQUNKLFNBQUtOLFNBQUwsQ0FBZU0sS0FBZjtBQUNIOztBQXRHMkQ7Ozs4QkFBM0NqRCxjLGVBQ0U7QUFDZmlDLEVBQUFBLE1BQU0sRUFBRWlCLG1CQUFVQyxNQUFWLENBQWlCQyxVQURWO0FBRWY5QyxFQUFBQSxRQUFRLEVBQUU0QyxtQkFBVUcsSUFGTDtBQUdmNUMsRUFBQUEsS0FBSyxFQUFFeUMsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBSFQsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB3aXRoVmFsaWRhdGlvbiBmcm9tICcuL1ZhbGlkYXRpb24nO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuXHJcbi8vIENvbnRyb2xsZWQgZm9ybSBjb21wb25lbnQgd3JhcHBpbmcgRmllbGQgZm9yIGlucHV0dGluZyBhIHJvb20gYWxpYXMgc2NvcGVkIHRvIGEgZ2l2ZW4gZG9tYWluXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJvb21BbGlhc0ZpZWxkIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIGRvbWFpbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnN0YXRlID0ge2lzVmFsaWQ6IHRydWV9O1xyXG4gICAgfVxyXG5cclxuICAgIF9hc0Z1bGxBbGlhcyhsb2NhbHBhcnQpIHtcclxuICAgICAgICByZXR1cm4gYCMke2xvY2FscGFydH06JHt0aGlzLnByb3BzLmRvbWFpbn1gO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBGaWVsZCA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkZpZWxkJyk7XHJcbiAgICAgICAgY29uc3QgcG91bmRTaWduID0gKDxzcGFuPiM8L3NwYW4+KTtcclxuICAgICAgICBjb25zdCBhbGlhc1Bvc3RmaXggPSBcIjpcIiArIHRoaXMucHJvcHMuZG9tYWluO1xyXG4gICAgICAgIGNvbnN0IGRvbWFpbiA9ICg8c3BhbiB0aXRsZT17YWxpYXNQb3N0Zml4fT57YWxpYXNQb3N0Zml4fTwvc3Bhbj4pO1xyXG4gICAgICAgIGNvbnN0IG1heGxlbmd0aCA9IDI1NSAtIHRoaXMucHJvcHMuZG9tYWluLmxlbmd0aCAtIDI7ICAgLy8gMiBmb3IgIyBhbmQgOlxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJSb29tIGFsaWFzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Jvb21BbGlhc0ZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICBwcmVmaXg9e3BvdW5kU2lnbn1cclxuICAgICAgICAgICAgICAgICAgICBwb3N0Zml4PXtkb21haW59XHJcbiAgICAgICAgICAgICAgICAgICAgcmVmPXtyZWYgPT4gdGhpcy5fZmllbGRSZWYgPSByZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgb25WYWxpZGF0ZT17dGhpcy5fb25WYWxpZGF0ZX1cclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17X3QoXCJlLmcuIG15LXJvb21cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnByb3BzLnZhbHVlLnN1YnN0cmluZygxLCB0aGlzLnByb3BzLnZhbHVlLmxlbmd0aCAtIHRoaXMucHJvcHMuZG9tYWluLmxlbmd0aCAtIDEpfVxyXG4gICAgICAgICAgICAgICAgICAgIG1heExlbmd0aD17bWF4bGVuZ3RofSAvPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ2hhbmdlID0gKGV2KSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25DaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZSh0aGlzLl9hc0Z1bGxBbGlhcyhldi50YXJnZXQudmFsdWUpKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblZhbGlkYXRlID0gYXN5bmMgKGZpZWxkU3RhdGUpID0+IHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLl92YWxpZGF0aW9uUnVsZXMoZmllbGRTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNWYWxpZDogcmVzdWx0LnZhbGlkfSk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH07XHJcblxyXG4gICAgX3ZhbGlkYXRpb25SdWxlcyA9IHdpdGhWYWxpZGF0aW9uKHtcclxuICAgICAgICBydWxlczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwic2FmZUxvY2FscGFydFwiLFxyXG4gICAgICAgICAgICAgICAgdGVzdDogYXN5bmMgKHsgdmFsdWUgfSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGZ1bGxBbGlhcyA9IHRoaXMuX2FzRnVsbEFsaWFzKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBYWFg6IEZJWE1FIGh0dHBzOi8vZ2l0aHViLmNvbS9tYXRyaXgtb3JnL21hdHJpeC1kb2MvaXNzdWVzLzY2OFxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAhdmFsdWUuaW5jbHVkZXMoXCIjXCIpICYmICF2YWx1ZS5pbmNsdWRlcyhcIjpcIikgJiYgIXZhbHVlLmluY2x1ZGVzKFwiLFwiKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmNvZGVVUkkoZnVsbEFsaWFzKSA9PT0gZnVsbEFsaWFzO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICgpID0+IF90KFwiU29tZSBjaGFyYWN0ZXJzIG5vdCBhbGxvd2VkXCIpLFxyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwicmVxdWlyZWRcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6IGFzeW5jICh7IHZhbHVlLCBhbGxvd0VtcHR5IH0pID0+IGFsbG93RW1wdHkgfHwgISF2YWx1ZSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICgpID0+IF90KFwiUGxlYXNlIHByb3ZpZGUgYSByb29tIGFsaWFzXCIpLFxyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwidGFrZW5cIixcclxuICAgICAgICAgICAgICAgIGZpbmFsOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgdGVzdDogYXN5bmMgKHt2YWx1ZX0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXdhaXQgY2xpZW50LmdldFJvb21JZEZvckFsaWFzKHRoaXMuX2FzRnVsbEFsaWFzKHZhbHVlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdlIGdvdCBhIHJvb20gaWQsIHNvIHRoZSBhbGlhcyBpcyB0YWtlblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFueSBzZXJ2ZXIgZXJyb3IgY29kZSB3aWxsIGRvLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlaXRoZXIgaXQgTV9OT1RfRk9VTkQgb3IgdGhlIGFsaWFzIGlzIGludmFsaWQgc29tZWhvdyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaW4gd2hpY2ggY2FzZSB3ZSBkb24ndCB3YW50IHRvIHNob3cgdGhlIGludmFsaWQgbWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gISFlcnIuZXJyY29kZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdmFsaWQ6ICgpID0+IF90KFwiVGhpcyBhbGlhcyBpcyBhdmFpbGFibGUgdG8gdXNlXCIpLFxyXG4gICAgICAgICAgICAgICAgaW52YWxpZDogKCkgPT4gX3QoXCJUaGlzIGFsaWFzIGlzIGFscmVhZHkgaW4gdXNlXCIpLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICB9KTtcclxuXHJcbiAgICBnZXQgaXNWYWxpZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5pc1ZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKG9wdGlvbnMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZmllbGRSZWYudmFsaWRhdGUob3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9jdXMoKSB7XHJcbiAgICAgICAgdGhpcy5fZmllbGRSZWYuZm9jdXMoKTtcclxuICAgIH1cclxufVxyXG4iXX0=