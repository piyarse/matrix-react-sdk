/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _DateUtils = require("../../../DateUtils");

var _languageHandler = require("../../../languageHandler");

var _filesize = _interopRequireDefault(require("filesize"));

var _AccessibleButton = _interopRequireDefault(require("./AccessibleButton"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Keyboard = require("../../../Keyboard");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

class ImageView extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onKeyDown", ev => {
      if (ev.key === _Keyboard.Key.ESCAPE) {
        ev.stopPropagation();
        ev.preventDefault();
        this.props.onFinished();
      }
    });
    (0, _defineProperty2.default)(this, "onRedactClick", () => {
      const ConfirmRedactDialog = sdk.getComponent("dialogs.ConfirmRedactDialog");

      _Modal.default.createTrackedDialog('Confirm Redact Dialog', 'Image View', ConfirmRedactDialog, {
        onFinished: proceed => {
          if (!proceed) return;

          _MatrixClientPeg.MatrixClientPeg.get().redactEvent(this.props.mxEvent.getRoomId(), this.props.mxEvent.getId()).catch(function (e) {
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog"); // display error message stating you couldn't delete this.

            const code = e.errcode || e.statusCode;

            _Modal.default.createTrackedDialog('You cannot delete this image.', '', ErrorDialog, {
              title: (0, _languageHandler._t)('Error'),
              description: (0, _languageHandler._t)('You cannot delete this image. (%(code)s)', {
                code: code
              })
            });
          });
        }
      });
    });
    (0, _defineProperty2.default)(this, "rotateCounterClockwise", () => {
      const cur = this.state.rotationDegrees;
      const rotationDegrees = (cur - 90) % 360;
      this.setState({
        rotationDegrees
      });
    });
    (0, _defineProperty2.default)(this, "rotateClockwise", () => {
      const cur = this.state.rotationDegrees;
      const rotationDegrees = (cur + 90) % 360;
      this.setState({
        rotationDegrees
      });
    });
    this.state = {
      rotationDegrees: 0
    };
  } // XXX: keyboard shortcuts for managing dialogs should be done by the modal
  // dialog base class somehow, surely...


  componentDidMount() {
    document.addEventListener("keydown", this.onKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.onKeyDown);
  }

  getName() {
    let name = this.props.name;

    if (name && this.props.link) {
      name = _react.default.createElement("a", {
        href: this.props.link,
        target: "_blank",
        rel: "noreferrer noopener"
      }, name);
    }

    return name;
  }

  render() {
    /*
            // In theory max-width: 80%, max-height: 80% on the CSS should work
            // but in practice, it doesn't, so do it manually:
    
            var width = this.props.width || 500;
            var height = this.props.height || 500;
    
            var maxWidth = document.documentElement.clientWidth * 0.8;
            var maxHeight = document.documentElement.clientHeight * 0.8;
    
            var widthFrac = width / maxWidth;
            var heightFrac = height / maxHeight;
    
            var displayWidth;
            var displayHeight;
            if (widthFrac > heightFrac) {
                displayWidth = Math.min(width, maxWidth);
                displayHeight = (displayWidth / width) * height;
            } else {
                displayHeight = Math.min(height, maxHeight);
                displayWidth = (displayHeight / height) * width;
            }
    
            var style = {
                width: displayWidth,
                height: displayHeight
            };
    */
    let style = {};
    let res;

    if (this.props.width && this.props.height) {
      style = {
        width: this.props.width,
        height: this.props.height
      };
      res = style.width + "x" + style.height + "px";
    }

    let size;

    if (this.props.fileSize) {
      size = (0, _filesize.default)(this.props.fileSize);
    }

    let sizeRes;

    if (size && res) {
      sizeRes = size + ", " + res;
    } else {
      sizeRes = size || res;
    }

    let mayRedact = false;
    const showEventMeta = !!this.props.mxEvent;
    let eventMeta;

    if (showEventMeta) {
      // Figure out the sender, defaulting to mxid
      let sender = this.props.mxEvent.getSender();

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const room = cli.getRoom(this.props.mxEvent.getRoomId());

      if (room) {
        mayRedact = room.currentState.maySendRedactionForEvent(this.props.mxEvent, cli.credentials.userId);
        const member = room.getMember(sender);
        if (member) sender = member.name;
      }

      eventMeta = _react.default.createElement("div", {
        className: "mx_ImageView_metadata"
      }, (0, _languageHandler._t)('Uploaded on %(date)s by %(user)s', {
        date: (0, _DateUtils.formatDate)(new Date(this.props.mxEvent.getTs())),
        user: sender
      }));
    }

    let eventRedact;

    if (mayRedact) {
      eventRedact = _react.default.createElement("div", {
        className: "mx_ImageView_button",
        onClick: this.onRedactClick
      }, (0, _languageHandler._t)('Remove'));
    }

    const rotationDegrees = this.state.rotationDegrees;

    const effectiveStyle = _objectSpread({
      transform: "rotate(".concat(rotationDegrees, "deg)")
    }, style);

    return _react.default.createElement("div", {
      className: "mx_ImageView"
    }, _react.default.createElement("div", {
      className: "mx_ImageView_lhs"
    }), _react.default.createElement("div", {
      className: "mx_ImageView_content"
    }, _react.default.createElement("img", {
      src: this.props.src,
      title: this.props.name,
      style: effectiveStyle,
      className: "mainImage"
    }), _react.default.createElement("div", {
      className: "mx_ImageView_labelWrapper"
    }, _react.default.createElement("div", {
      className: "mx_ImageView_label"
    }, _react.default.createElement(_AccessibleButton.default, {
      className: "mx_ImageView_rotateCounterClockwise",
      title: (0, _languageHandler._t)("Rotate Left"),
      onClick: this.rotateCounterClockwise
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/rotate-ccw.svg"),
      alt: (0, _languageHandler._t)('Rotate counter-clockwise'),
      width: "18",
      height: "18"
    })), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_ImageView_rotateClockwise",
      title: (0, _languageHandler._t)("Rotate Right"),
      onClick: this.rotateClockwise
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/rotate-cw.svg"),
      alt: (0, _languageHandler._t)('Rotate clockwise'),
      width: "18",
      height: "18"
    })), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_ImageView_cancel",
      title: (0, _languageHandler._t)("Close"),
      onClick: this.props.onFinished
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/cancel-white.svg"),
      width: "18",
      height: "18",
      alt: (0, _languageHandler._t)('Close')
    })), _react.default.createElement("div", {
      className: "mx_ImageView_shim"
    }), _react.default.createElement("div", {
      className: "mx_ImageView_name"
    }, this.getName()), eventMeta, _react.default.createElement("a", {
      className: "mx_ImageView_link",
      href: this.props.src,
      download: this.props.name,
      target: "_blank",
      rel: "noopener"
    }, _react.default.createElement("div", {
      className: "mx_ImageView_download"
    }, (0, _languageHandler._t)('Download this file'), _react.default.createElement("br", null), _react.default.createElement("span", {
      className: "mx_ImageView_size"
    }, sizeRes))), eventRedact, _react.default.createElement("div", {
      className: "mx_ImageView_shim"
    })))), _react.default.createElement("div", {
      className: "mx_ImageView_rhs"
    }));
  }

}

exports.default = ImageView;
(0, _defineProperty2.default)(ImageView, "propTypes", {
  src: _propTypes.default.string.isRequired,
  // the source of the image being displayed
  name: _propTypes.default.string,
  // the main title ('name') for the image
  link: _propTypes.default.string,
  // the link (if any) applied to the name of the image
  width: _propTypes.default.number,
  // width of the image src in pixels
  height: _propTypes.default.number,
  // height of the image src in pixels
  fileSize: _propTypes.default.number,
  // size of the image src in bytes
  onFinished: _propTypes.default.func.isRequired,
  // callback when the lightbox is dismissed
  // the event (if any) that the Image is displaying. Used for event-specific stuff like
  // redactions, senders, timestamps etc.  Other descriptors are taken from the explicit
  // properties above, which let us use lightboxes to display images which aren't associated
  // with events.
  mxEvent: _propTypes.default.object
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0ltYWdlVmlldy5qcyJdLCJuYW1lcyI6WyJJbWFnZVZpZXciLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJldiIsImtleSIsIktleSIsIkVTQ0FQRSIsInN0b3BQcm9wYWdhdGlvbiIsInByZXZlbnREZWZhdWx0Iiwib25GaW5pc2hlZCIsIkNvbmZpcm1SZWRhY3REaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJwcm9jZWVkIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwicmVkYWN0RXZlbnQiLCJteEV2ZW50IiwiZ2V0Um9vbUlkIiwiZ2V0SWQiLCJjYXRjaCIsImUiLCJFcnJvckRpYWxvZyIsImNvZGUiLCJlcnJjb2RlIiwic3RhdHVzQ29kZSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJjdXIiLCJzdGF0ZSIsInJvdGF0aW9uRGVncmVlcyIsInNldFN0YXRlIiwiY29tcG9uZW50RGlkTW91bnQiLCJkb2N1bWVudCIsImFkZEV2ZW50TGlzdGVuZXIiLCJvbktleURvd24iLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJnZXROYW1lIiwibmFtZSIsImxpbmsiLCJyZW5kZXIiLCJzdHlsZSIsInJlcyIsIndpZHRoIiwiaGVpZ2h0Iiwic2l6ZSIsImZpbGVTaXplIiwic2l6ZVJlcyIsIm1heVJlZGFjdCIsInNob3dFdmVudE1ldGEiLCJldmVudE1ldGEiLCJzZW5kZXIiLCJnZXRTZW5kZXIiLCJjbGkiLCJyb29tIiwiZ2V0Um9vbSIsImN1cnJlbnRTdGF0ZSIsIm1heVNlbmRSZWRhY3Rpb25Gb3JFdmVudCIsImNyZWRlbnRpYWxzIiwidXNlcklkIiwibWVtYmVyIiwiZ2V0TWVtYmVyIiwiZGF0ZSIsIkRhdGUiLCJnZXRUcyIsInVzZXIiLCJldmVudFJlZGFjdCIsIm9uUmVkYWN0Q2xpY2siLCJlZmZlY3RpdmVTdHlsZSIsInRyYW5zZm9ybSIsInNyYyIsInJvdGF0ZUNvdW50ZXJDbG9ja3dpc2UiLCJyZXF1aXJlIiwicm90YXRlQ2xvY2t3aXNlIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm51bWJlciIsImZ1bmMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOzs7Ozs7Ozs7Ozs7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7OztBQUVlLE1BQU1BLFNBQU4sU0FBd0JDLGVBQU1DLFNBQTlCLENBQXdDO0FBaUJuREMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUscURBZU5DLEVBQUQsSUFBUTtBQUNoQixVQUFJQSxFQUFFLENBQUNDLEdBQUgsS0FBV0MsY0FBSUMsTUFBbkIsRUFBMkI7QUFDdkJILFFBQUFBLEVBQUUsQ0FBQ0ksZUFBSDtBQUNBSixRQUFBQSxFQUFFLENBQUNLLGNBQUg7QUFDQSxhQUFLTixLQUFMLENBQVdPLFVBQVg7QUFDSDtBQUNKLEtBckJrQjtBQUFBLHlEQXVCSCxNQUFNO0FBQ2xCLFlBQU1DLG1CQUFtQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTVCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELFlBQW5ELEVBQWlFSixtQkFBakUsRUFBc0Y7QUFDbEZELFFBQUFBLFVBQVUsRUFBR00sT0FBRCxJQUFhO0FBQ3JCLGNBQUksQ0FBQ0EsT0FBTCxFQUFjOztBQUNkQywyQ0FBZ0JDLEdBQWhCLEdBQXNCQyxXQUF0QixDQUNJLEtBQUtoQixLQUFMLENBQVdpQixPQUFYLENBQW1CQyxTQUFuQixFQURKLEVBQ29DLEtBQUtsQixLQUFMLENBQVdpQixPQUFYLENBQW1CRSxLQUFuQixFQURwQyxFQUVFQyxLQUZGLENBRVEsVUFBU0MsQ0FBVCxFQUFZO0FBQ2hCLGtCQUFNQyxXQUFXLEdBQUdiLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEIsQ0FEZ0IsQ0FFaEI7O0FBQ0Esa0JBQU1hLElBQUksR0FBR0YsQ0FBQyxDQUFDRyxPQUFGLElBQWFILENBQUMsQ0FBQ0ksVUFBNUI7O0FBQ0FkLDJCQUFNQyxtQkFBTixDQUEwQiwrQkFBMUIsRUFBMkQsRUFBM0QsRUFBK0RVLFdBQS9ELEVBQTRFO0FBQ3hFSSxjQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQURpRTtBQUV4RUMsY0FBQUEsV0FBVyxFQUFFLHlCQUFHLDBDQUFILEVBQStDO0FBQUNKLGdCQUFBQSxJQUFJLEVBQUVBO0FBQVAsZUFBL0M7QUFGMkQsYUFBNUU7QUFJSCxXQVZEO0FBV0g7QUFkaUYsT0FBdEY7QUFnQkgsS0F6Q2tCO0FBQUEsa0VBbURNLE1BQU07QUFDM0IsWUFBTUssR0FBRyxHQUFHLEtBQUtDLEtBQUwsQ0FBV0MsZUFBdkI7QUFDQSxZQUFNQSxlQUFlLEdBQUcsQ0FBQ0YsR0FBRyxHQUFHLEVBQVAsSUFBYSxHQUFyQztBQUNBLFdBQUtHLFFBQUwsQ0FBYztBQUFFRCxRQUFBQTtBQUFGLE9BQWQ7QUFDSCxLQXZEa0I7QUFBQSwyREF5REQsTUFBTTtBQUNwQixZQUFNRixHQUFHLEdBQUcsS0FBS0MsS0FBTCxDQUFXQyxlQUF2QjtBQUNBLFlBQU1BLGVBQWUsR0FBRyxDQUFDRixHQUFHLEdBQUcsRUFBUCxJQUFhLEdBQXJDO0FBQ0EsV0FBS0csUUFBTCxDQUFjO0FBQUVELFFBQUFBO0FBQUYsT0FBZDtBQUNILEtBN0RrQjtBQUVmLFNBQUtELEtBQUwsR0FBYTtBQUFFQyxNQUFBQSxlQUFlLEVBQUU7QUFBbkIsS0FBYjtBQUNILEdBcEJrRCxDQXNCbkQ7QUFDQTs7O0FBQ0FFLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCQyxJQUFBQSxRQUFRLENBQUNDLGdCQUFULENBQTBCLFNBQTFCLEVBQXFDLEtBQUtDLFNBQTFDO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CSCxJQUFBQSxRQUFRLENBQUNJLG1CQUFULENBQTZCLFNBQTdCLEVBQXdDLEtBQUtGLFNBQTdDO0FBQ0g7O0FBOEJERyxFQUFBQSxPQUFPLEdBQUc7QUFDTixRQUFJQyxJQUFJLEdBQUcsS0FBS3ZDLEtBQUwsQ0FBV3VDLElBQXRCOztBQUNBLFFBQUlBLElBQUksSUFBSSxLQUFLdkMsS0FBTCxDQUFXd0MsSUFBdkIsRUFBNkI7QUFDekJELE1BQUFBLElBQUksR0FBRztBQUFHLFFBQUEsSUFBSSxFQUFHLEtBQUt2QyxLQUFMLENBQVd3QyxJQUFyQjtBQUE0QixRQUFBLE1BQU0sRUFBQyxRQUFuQztBQUE0QyxRQUFBLEdBQUcsRUFBQztBQUFoRCxTQUF3RUQsSUFBeEUsQ0FBUDtBQUNIOztBQUNELFdBQU9BLElBQVA7QUFDSDs7QUFjREUsRUFBQUEsTUFBTSxHQUFHO0FBQ2I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0QlEsUUFBSUMsS0FBSyxHQUFHLEVBQVo7QUFDQSxRQUFJQyxHQUFKOztBQUVBLFFBQUksS0FBSzNDLEtBQUwsQ0FBVzRDLEtBQVgsSUFBb0IsS0FBSzVDLEtBQUwsQ0FBVzZDLE1BQW5DLEVBQTJDO0FBQ3ZDSCxNQUFBQSxLQUFLLEdBQUc7QUFDSkUsUUFBQUEsS0FBSyxFQUFFLEtBQUs1QyxLQUFMLENBQVc0QyxLQURkO0FBRUpDLFFBQUFBLE1BQU0sRUFBRSxLQUFLN0MsS0FBTCxDQUFXNkM7QUFGZixPQUFSO0FBSUFGLE1BQUFBLEdBQUcsR0FBR0QsS0FBSyxDQUFDRSxLQUFOLEdBQWMsR0FBZCxHQUFvQkYsS0FBSyxDQUFDRyxNQUExQixHQUFtQyxJQUF6QztBQUNIOztBQUVELFFBQUlDLElBQUo7O0FBQ0EsUUFBSSxLQUFLOUMsS0FBTCxDQUFXK0MsUUFBZixFQUF5QjtBQUNyQkQsTUFBQUEsSUFBSSxHQUFHLHVCQUFTLEtBQUs5QyxLQUFMLENBQVcrQyxRQUFwQixDQUFQO0FBQ0g7O0FBRUQsUUFBSUMsT0FBSjs7QUFDQSxRQUFJRixJQUFJLElBQUlILEdBQVosRUFBaUI7QUFDYkssTUFBQUEsT0FBTyxHQUFHRixJQUFJLEdBQUcsSUFBUCxHQUFjSCxHQUF4QjtBQUNILEtBRkQsTUFFTztBQUNISyxNQUFBQSxPQUFPLEdBQUdGLElBQUksSUFBSUgsR0FBbEI7QUFDSDs7QUFFRCxRQUFJTSxTQUFTLEdBQUcsS0FBaEI7QUFDQSxVQUFNQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEtBQUtsRCxLQUFMLENBQVdpQixPQUFuQztBQUVBLFFBQUlrQyxTQUFKOztBQUNBLFFBQUlELGFBQUosRUFBbUI7QUFDZjtBQUNBLFVBQUlFLE1BQU0sR0FBRyxLQUFLcEQsS0FBTCxDQUFXaUIsT0FBWCxDQUFtQm9DLFNBQW5CLEVBQWI7O0FBQ0EsWUFBTUMsR0FBRyxHQUFHeEMsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFlBQU13QyxJQUFJLEdBQUdELEdBQUcsQ0FBQ0UsT0FBSixDQUFZLEtBQUt4RCxLQUFMLENBQVdpQixPQUFYLENBQW1CQyxTQUFuQixFQUFaLENBQWI7O0FBQ0EsVUFBSXFDLElBQUosRUFBVTtBQUNOTixRQUFBQSxTQUFTLEdBQUdNLElBQUksQ0FBQ0UsWUFBTCxDQUFrQkMsd0JBQWxCLENBQTJDLEtBQUsxRCxLQUFMLENBQVdpQixPQUF0RCxFQUErRHFDLEdBQUcsQ0FBQ0ssV0FBSixDQUFnQkMsTUFBL0UsQ0FBWjtBQUNBLGNBQU1DLE1BQU0sR0FBR04sSUFBSSxDQUFDTyxTQUFMLENBQWVWLE1BQWYsQ0FBZjtBQUNBLFlBQUlTLE1BQUosRUFBWVQsTUFBTSxHQUFHUyxNQUFNLENBQUN0QixJQUFoQjtBQUNmOztBQUVEWSxNQUFBQSxTQUFTLEdBQUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1AseUJBQUcsa0NBQUgsRUFBdUM7QUFDckNZLFFBQUFBLElBQUksRUFBRSwyQkFBVyxJQUFJQyxJQUFKLENBQVMsS0FBS2hFLEtBQUwsQ0FBV2lCLE9BQVgsQ0FBbUJnRCxLQUFuQixFQUFULENBQVgsQ0FEK0I7QUFFckNDLFFBQUFBLElBQUksRUFBRWQ7QUFGK0IsT0FBdkMsQ0FETyxDQUFiO0FBTUg7O0FBRUQsUUFBSWUsV0FBSjs7QUFDQSxRQUFJbEIsU0FBSixFQUFlO0FBQ1hrQixNQUFBQSxXQUFXLEdBQUk7QUFBSyxRQUFBLFNBQVMsRUFBQyxxQkFBZjtBQUFxQyxRQUFBLE9BQU8sRUFBRSxLQUFLQztBQUFuRCxTQUNULHlCQUFHLFFBQUgsQ0FEUyxDQUFmO0FBR0g7O0FBRUQsVUFBTXRDLGVBQWUsR0FBRyxLQUFLRCxLQUFMLENBQVdDLGVBQW5DOztBQUNBLFVBQU11QyxjQUFjO0FBQUlDLE1BQUFBLFNBQVMsbUJBQVl4QyxlQUFaO0FBQWIsT0FBbURZLEtBQW5ELENBQXBCOztBQUVBLFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE1BREosRUFHSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsR0FBRyxFQUFFLEtBQUsxQyxLQUFMLENBQVd1RSxHQUFyQjtBQUEwQixNQUFBLEtBQUssRUFBRSxLQUFLdkUsS0FBTCxDQUFXdUMsSUFBNUM7QUFBa0QsTUFBQSxLQUFLLEVBQUU4QixjQUF6RDtBQUF5RSxNQUFBLFNBQVMsRUFBQztBQUFuRixNQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMscUNBQTVCO0FBQWtFLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGFBQUgsQ0FBekU7QUFBNEYsTUFBQSxPQUFPLEVBQUcsS0FBS0c7QUFBM0csT0FDSTtBQUFLLE1BQUEsR0FBRyxFQUFFQyxPQUFPLENBQUMsb0NBQUQsQ0FBakI7QUFBeUQsTUFBQSxHQUFHLEVBQUcseUJBQUcsMEJBQUgsQ0FBL0Q7QUFBZ0csTUFBQSxLQUFLLEVBQUMsSUFBdEc7QUFBMkcsTUFBQSxNQUFNLEVBQUM7QUFBbEgsTUFESixDQURKLEVBSUksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsOEJBQTVCO0FBQTJELE1BQUEsS0FBSyxFQUFFLHlCQUFHLGNBQUgsQ0FBbEU7QUFBc0YsTUFBQSxPQUFPLEVBQUcsS0FBS0M7QUFBckcsT0FDSTtBQUFLLE1BQUEsR0FBRyxFQUFFRCxPQUFPLENBQUMsbUNBQUQsQ0FBakI7QUFBd0QsTUFBQSxHQUFHLEVBQUcseUJBQUcsa0JBQUgsQ0FBOUQ7QUFBdUYsTUFBQSxLQUFLLEVBQUMsSUFBN0Y7QUFBa0csTUFBQSxNQUFNLEVBQUM7QUFBekcsTUFESixDQUpKLEVBT0ksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMscUJBQTVCO0FBQWtELE1BQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FBekQ7QUFBc0UsTUFBQSxPQUFPLEVBQUcsS0FBS3pFLEtBQUwsQ0FBV087QUFBM0YsT0FDRTtBQUFLLE1BQUEsR0FBRyxFQUFFa0UsT0FBTyxDQUFDLHNDQUFELENBQWpCO0FBQTJELE1BQUEsS0FBSyxFQUFDLElBQWpFO0FBQXNFLE1BQUEsTUFBTSxFQUFDLElBQTdFO0FBQWtGLE1BQUEsR0FBRyxFQUFHLHlCQUFHLE9BQUg7QUFBeEYsTUFERixDQVBKLEVBVUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE1BVkosRUFZSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTSxLQUFLbkMsT0FBTCxFQUROLENBWkosRUFlTWEsU0FmTixFQWdCSTtBQUFHLE1BQUEsU0FBUyxFQUFDLG1CQUFiO0FBQWlDLE1BQUEsSUFBSSxFQUFHLEtBQUtuRCxLQUFMLENBQVd1RSxHQUFuRDtBQUF5RCxNQUFBLFFBQVEsRUFBRyxLQUFLdkUsS0FBTCxDQUFXdUMsSUFBL0U7QUFBc0YsTUFBQSxNQUFNLEVBQUMsUUFBN0Y7QUFBc0csTUFBQSxHQUFHLEVBQUM7QUFBMUcsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDVSx5QkFBRyxvQkFBSCxDQURWLEVBQ29DLHdDQURwQyxFQUVTO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FBc0NTLE9BQXRDLENBRlQsQ0FESixDQWhCSixFQXNCTW1CLFdBdEJOLEVBdUJJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixNQXZCSixDQURKLENBRkosQ0FISixFQWtDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsTUFsQ0osQ0FESjtBQXVDSDs7QUE1TWtEOzs7OEJBQWxDdkUsUyxlQUNFO0FBQ2YyRSxFQUFBQSxHQUFHLEVBQUVJLG1CQUFVQyxNQUFWLENBQWlCQyxVQURQO0FBQ21CO0FBQ2xDdEMsRUFBQUEsSUFBSSxFQUFFb0MsbUJBQVVDLE1BRkQ7QUFFUztBQUN4QnBDLEVBQUFBLElBQUksRUFBRW1DLG1CQUFVQyxNQUhEO0FBR1M7QUFDeEJoQyxFQUFBQSxLQUFLLEVBQUUrQixtQkFBVUcsTUFKRjtBQUlVO0FBQ3pCakMsRUFBQUEsTUFBTSxFQUFFOEIsbUJBQVVHLE1BTEg7QUFLVztBQUMxQi9CLEVBQUFBLFFBQVEsRUFBRTRCLG1CQUFVRyxNQU5MO0FBTWE7QUFDNUJ2RSxFQUFBQSxVQUFVLEVBQUVvRSxtQkFBVUksSUFBVixDQUFlRixVQVBaO0FBT3dCO0FBRXZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E1RCxFQUFBQSxPQUFPLEVBQUUwRCxtQkFBVUs7QUFiSixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0IHtmb3JtYXREYXRlfSBmcm9tICcuLi8uLi8uLi9EYXRlVXRpbHMnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBmaWxlc2l6ZSBmcm9tIFwiZmlsZXNpemVcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSBcIi4vQWNjZXNzaWJsZUJ1dHRvblwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uL01vZGFsXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IHtLZXl9IGZyb20gXCIuLi8uLi8uLi9LZXlib2FyZFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSW1hZ2VWaWV3IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgc3JjOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsIC8vIHRoZSBzb3VyY2Ugb2YgdGhlIGltYWdlIGJlaW5nIGRpc3BsYXllZFxyXG4gICAgICAgIG5hbWU6IFByb3BUeXBlcy5zdHJpbmcsIC8vIHRoZSBtYWluIHRpdGxlICgnbmFtZScpIGZvciB0aGUgaW1hZ2VcclxuICAgICAgICBsaW5rOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyB0aGUgbGluayAoaWYgYW55KSBhcHBsaWVkIHRvIHRoZSBuYW1lIG9mIHRoZSBpbWFnZVxyXG4gICAgICAgIHdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLCAvLyB3aWR0aCBvZiB0aGUgaW1hZ2Ugc3JjIGluIHBpeGVsc1xyXG4gICAgICAgIGhlaWdodDogUHJvcFR5cGVzLm51bWJlciwgLy8gaGVpZ2h0IG9mIHRoZSBpbWFnZSBzcmMgaW4gcGl4ZWxzXHJcbiAgICAgICAgZmlsZVNpemU6IFByb3BUeXBlcy5udW1iZXIsIC8vIHNpemUgb2YgdGhlIGltYWdlIHNyYyBpbiBieXRlc1xyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsIC8vIGNhbGxiYWNrIHdoZW4gdGhlIGxpZ2h0Ym94IGlzIGRpc21pc3NlZFxyXG5cclxuICAgICAgICAvLyB0aGUgZXZlbnQgKGlmIGFueSkgdGhhdCB0aGUgSW1hZ2UgaXMgZGlzcGxheWluZy4gVXNlZCBmb3IgZXZlbnQtc3BlY2lmaWMgc3R1ZmYgbGlrZVxyXG4gICAgICAgIC8vIHJlZGFjdGlvbnMsIHNlbmRlcnMsIHRpbWVzdGFtcHMgZXRjLiAgT3RoZXIgZGVzY3JpcHRvcnMgYXJlIHRha2VuIGZyb20gdGhlIGV4cGxpY2l0XHJcbiAgICAgICAgLy8gcHJvcGVydGllcyBhYm92ZSwgd2hpY2ggbGV0IHVzIHVzZSBsaWdodGJveGVzIHRvIGRpc3BsYXkgaW1hZ2VzIHdoaWNoIGFyZW4ndCBhc3NvY2lhdGVkXHJcbiAgICAgICAgLy8gd2l0aCBldmVudHMuXHJcbiAgICAgICAgbXhFdmVudDogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHsgcm90YXRpb25EZWdyZWVzOiAwIH07XHJcbiAgICB9XHJcblxyXG4gICAgLy8gWFhYOiBrZXlib2FyZCBzaG9ydGN1dHMgZm9yIG1hbmFnaW5nIGRpYWxvZ3Mgc2hvdWxkIGJlIGRvbmUgYnkgdGhlIG1vZGFsXHJcbiAgICAvLyBkaWFsb2cgYmFzZSBjbGFzcyBzb21laG93LCBzdXJlbHkuLi5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIHRoaXMub25LZXlEb3duKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLm9uS2V5RG93bik7XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlEb3duID0gKGV2KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2LmtleSA9PT0gS2V5LkVTQ0FQRSkge1xyXG4gICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvblJlZGFjdENsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IENvbmZpcm1SZWRhY3REaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5Db25maXJtUmVkYWN0RGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NvbmZpcm0gUmVkYWN0IERpYWxvZycsICdJbWFnZSBWaWV3JywgQ29uZmlybVJlZGFjdERpYWxvZywge1xyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAocHJvY2VlZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFwcm9jZWVkKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVkYWN0RXZlbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpLCB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgICAgICkuY2F0Y2goZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZGlzcGxheSBlcnJvciBtZXNzYWdlIHN0YXRpbmcgeW91IGNvdWxkbid0IGRlbGV0ZSB0aGlzLlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvZGUgPSBlLmVycmNvZGUgfHwgZS5zdGF0dXNDb2RlO1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1lvdSBjYW5ub3QgZGVsZXRlIHRoaXMgaW1hZ2UuJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXJyb3InKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdZb3UgY2Fubm90IGRlbGV0ZSB0aGlzIGltYWdlLiAoJShjb2RlKXMpJywge2NvZGU6IGNvZGV9KSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBnZXROYW1lKCkge1xyXG4gICAgICAgIGxldCBuYW1lID0gdGhpcy5wcm9wcy5uYW1lO1xyXG4gICAgICAgIGlmIChuYW1lICYmIHRoaXMucHJvcHMubGluaykge1xyXG4gICAgICAgICAgICBuYW1lID0gPGEgaHJlZj17IHRoaXMucHJvcHMubGluayB9IHRhcmdldD1cIl9ibGFua1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIj57IG5hbWUgfTwvYT47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHJvdGF0ZUNvdW50ZXJDbG9ja3dpc2UgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgY3VyID0gdGhpcy5zdGF0ZS5yb3RhdGlvbkRlZ3JlZXM7XHJcbiAgICAgICAgY29uc3Qgcm90YXRpb25EZWdyZWVzID0gKGN1ciAtIDkwKSAlIDM2MDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcm90YXRpb25EZWdyZWVzIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByb3RhdGVDbG9ja3dpc2UgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgY3VyID0gdGhpcy5zdGF0ZS5yb3RhdGlvbkRlZ3JlZXM7XHJcbiAgICAgICAgY29uc3Qgcm90YXRpb25EZWdyZWVzID0gKGN1ciArIDkwKSAlIDM2MDtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcm90YXRpb25EZWdyZWVzIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbi8qXHJcbiAgICAgICAgLy8gSW4gdGhlb3J5IG1heC13aWR0aDogODAlLCBtYXgtaGVpZ2h0OiA4MCUgb24gdGhlIENTUyBzaG91bGQgd29ya1xyXG4gICAgICAgIC8vIGJ1dCBpbiBwcmFjdGljZSwgaXQgZG9lc24ndCwgc28gZG8gaXQgbWFudWFsbHk6XHJcblxyXG4gICAgICAgIHZhciB3aWR0aCA9IHRoaXMucHJvcHMud2lkdGggfHwgNTAwO1xyXG4gICAgICAgIHZhciBoZWlnaHQgPSB0aGlzLnByb3BzLmhlaWdodCB8fCA1MDA7XHJcblxyXG4gICAgICAgIHZhciBtYXhXaWR0aCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCAqIDAuODtcclxuICAgICAgICB2YXIgbWF4SGVpZ2h0ID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCAqIDAuODtcclxuXHJcbiAgICAgICAgdmFyIHdpZHRoRnJhYyA9IHdpZHRoIC8gbWF4V2lkdGg7XHJcbiAgICAgICAgdmFyIGhlaWdodEZyYWMgPSBoZWlnaHQgLyBtYXhIZWlnaHQ7XHJcblxyXG4gICAgICAgIHZhciBkaXNwbGF5V2lkdGg7XHJcbiAgICAgICAgdmFyIGRpc3BsYXlIZWlnaHQ7XHJcbiAgICAgICAgaWYgKHdpZHRoRnJhYyA+IGhlaWdodEZyYWMpIHtcclxuICAgICAgICAgICAgZGlzcGxheVdpZHRoID0gTWF0aC5taW4od2lkdGgsIG1heFdpZHRoKTtcclxuICAgICAgICAgICAgZGlzcGxheUhlaWdodCA9IChkaXNwbGF5V2lkdGggLyB3aWR0aCkgKiBoZWlnaHQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGlzcGxheUhlaWdodCA9IE1hdGgubWluKGhlaWdodCwgbWF4SGVpZ2h0KTtcclxuICAgICAgICAgICAgZGlzcGxheVdpZHRoID0gKGRpc3BsYXlIZWlnaHQgLyBoZWlnaHQpICogd2lkdGg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgc3R5bGUgPSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBkaXNwbGF5V2lkdGgsXHJcbiAgICAgICAgICAgIGhlaWdodDogZGlzcGxheUhlaWdodFxyXG4gICAgICAgIH07XHJcbiovXHJcbiAgICAgICAgbGV0IHN0eWxlID0ge307XHJcbiAgICAgICAgbGV0IHJlcztcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMud2lkdGggJiYgdGhpcy5wcm9wcy5oZWlnaHQpIHtcclxuICAgICAgICAgICAgc3R5bGUgPSB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogdGhpcy5wcm9wcy53aWR0aCxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdGhpcy5wcm9wcy5oZWlnaHQsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHJlcyA9IHN0eWxlLndpZHRoICsgXCJ4XCIgKyBzdHlsZS5oZWlnaHQgKyBcInB4XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2l6ZTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5maWxlU2l6ZSkge1xyXG4gICAgICAgICAgICBzaXplID0gZmlsZXNpemUodGhpcy5wcm9wcy5maWxlU2l6ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2l6ZVJlcztcclxuICAgICAgICBpZiAoc2l6ZSAmJiByZXMpIHtcclxuICAgICAgICAgICAgc2l6ZVJlcyA9IHNpemUgKyBcIiwgXCIgKyByZXM7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2l6ZVJlcyA9IHNpemUgfHwgcmVzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG1heVJlZGFjdCA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IHNob3dFdmVudE1ldGEgPSAhIXRoaXMucHJvcHMubXhFdmVudDtcclxuXHJcbiAgICAgICAgbGV0IGV2ZW50TWV0YTtcclxuICAgICAgICBpZiAoc2hvd0V2ZW50TWV0YSkge1xyXG4gICAgICAgICAgICAvLyBGaWd1cmUgb3V0IHRoZSBzZW5kZXIsIGRlZmF1bHRpbmcgdG8gbXhpZFxyXG4gICAgICAgICAgICBsZXQgc2VuZGVyID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldFNlbmRlcigpO1xyXG4gICAgICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbSh0aGlzLnByb3BzLm14RXZlbnQuZ2V0Um9vbUlkKCkpO1xyXG4gICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgbWF5UmVkYWN0ID0gcm9vbS5jdXJyZW50U3RhdGUubWF5U2VuZFJlZGFjdGlvbkZvckV2ZW50KHRoaXMucHJvcHMubXhFdmVudCwgY2xpLmNyZWRlbnRpYWxzLnVzZXJJZCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtZW1iZXIgPSByb29tLmdldE1lbWJlcihzZW5kZXIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1lbWJlcikgc2VuZGVyID0gbWVtYmVyLm5hbWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGV2ZW50TWV0YSA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X0ltYWdlVmlld19tZXRhZGF0YVwiPlxyXG4gICAgICAgICAgICAgICAgeyBfdCgnVXBsb2FkZWQgb24gJShkYXRlKXMgYnkgJSh1c2VyKXMnLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZTogZm9ybWF0RGF0ZShuZXcgRGF0ZSh0aGlzLnByb3BzLm14RXZlbnQuZ2V0VHMoKSkpLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXI6IHNlbmRlcixcclxuICAgICAgICAgICAgICAgIH0pIH1cclxuICAgICAgICAgICAgPC9kaXY+KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBldmVudFJlZGFjdDtcclxuICAgICAgICBpZiAobWF5UmVkYWN0KSB7XHJcbiAgICAgICAgICAgIGV2ZW50UmVkYWN0ID0gKDxkaXYgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X2J1dHRvblwiIG9uQ2xpY2s9e3RoaXMub25SZWRhY3RDbGlja30+XHJcbiAgICAgICAgICAgICAgICB7IF90KCdSZW1vdmUnKSB9XHJcbiAgICAgICAgICAgIDwvZGl2Pik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByb3RhdGlvbkRlZ3JlZXMgPSB0aGlzLnN0YXRlLnJvdGF0aW9uRGVncmVlcztcclxuICAgICAgICBjb25zdCBlZmZlY3RpdmVTdHlsZSA9IHt0cmFuc2Zvcm06IGByb3RhdGUoJHtyb3RhdGlvbkRlZ3JlZXN9ZGVnKWAsIC4uLnN0eWxlfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbWFnZVZpZXdcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X2xoc1wiPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0ltYWdlVmlld19jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3RoaXMucHJvcHMuc3JjfSB0aXRsZT17dGhpcy5wcm9wcy5uYW1lfSBzdHlsZT17ZWZmZWN0aXZlU3R5bGV9IGNsYXNzTmFtZT1cIm1haW5JbWFnZVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbWFnZVZpZXdfbGFiZWxXcmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X2xhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9JbWFnZVZpZXdfcm90YXRlQ291bnRlckNsb2Nrd2lzZVwiIHRpdGxlPXtfdChcIlJvdGF0ZSBMZWZ0XCIpfSBvbkNsaWNrPXsgdGhpcy5yb3RhdGVDb3VudGVyQ2xvY2t3aXNlIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL3JvdGF0ZS1jY3cuc3ZnXCIpfSBhbHQ9eyBfdCgnUm90YXRlIGNvdW50ZXItY2xvY2t3aXNlJykgfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X3JvdGF0ZUNsb2Nrd2lzZVwiIHRpdGxlPXtfdChcIlJvdGF0ZSBSaWdodFwiKX0gb25DbGljaz17IHRoaXMucm90YXRlQ2xvY2t3aXNlIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL3JvdGF0ZS1jdy5zdmdcIil9IGFsdD17IF90KCdSb3RhdGUgY2xvY2t3aXNlJykgfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X2NhbmNlbFwiIHRpdGxlPXtfdChcIkNsb3NlXCIpfSBvbkNsaWNrPXsgdGhpcy5wcm9wcy5vbkZpbmlzaGVkIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwtd2hpdGUuc3ZnXCIpfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIiBhbHQ9eyBfdCgnQ2xvc2UnKSB9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0ltYWdlVmlld19zaGltXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X25hbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuZ2V0TmFtZSgpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBldmVudE1ldGEgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X2xpbmtcIiBocmVmPXsgdGhpcy5wcm9wcy5zcmMgfSBkb3dubG9hZD17IHRoaXMucHJvcHMubmFtZSB9IHRhcmdldD1cIl9ibGFua1wiIHJlbD1cIm5vb3BlbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbWFnZVZpZXdfZG93bmxvYWRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ0Rvd25sb2FkIHRoaXMgZmlsZScpIH08YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9JbWFnZVZpZXdfc2l6ZVwiPnsgc2l6ZVJlcyB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBldmVudFJlZGFjdCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0ltYWdlVmlld19zaGltXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfSW1hZ2VWaWV3X3Joc1wiPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19