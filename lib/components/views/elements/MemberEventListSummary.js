"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var _FormattingUtils = require("../../../utils/FormattingUtils");

var sdk = _interopRequireWildcard(require("../../../index"));

var _matrixJsSdk = require("matrix-js-sdk");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'MemberEventListSummary',
  propTypes: {
    // An array of member events to summarise
    events: _propTypes.default.arrayOf(_propTypes.default.instanceOf(_matrixJsSdk.MatrixEvent)).isRequired,
    // An array of EventTiles to render when expanded
    children: _propTypes.default.array.isRequired,
    // The maximum number of names to show in either each summary e.g. 2 would result "A, B and 234 others left"
    summaryLength: _propTypes.default.number,
    // The maximum number of avatars to display in the summary
    avatarsMaxLength: _propTypes.default.number,
    // The minimum number of events needed to trigger summarisation
    threshold: _propTypes.default.number,
    // Called when the MELS expansion is toggled
    onToggle: _propTypes.default.func,
    // Whether or not to begin with state.expanded=true
    startExpanded: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      summaryLength: 1,
      threshold: 3,
      avatarsMaxLength: 5
    };
  },
  shouldComponentUpdate: function (nextProps) {
    // Update if
    //  - The number of summarised events has changed
    //  - or if the summary is about to toggle to become collapsed
    //  - or if there are fewEvents, meaning the child eventTiles are shown as-is
    return nextProps.events.length !== this.props.events.length || nextProps.events.length < this.props.threshold;
  },

  /**
   * Generate the text for users aggregated by their transition sequences (`eventAggregates`) where
   * the sequences are ordered by `orderedTransitionSequences`.
   * @param {object[]} eventAggregates a map of transition sequence to array of user display names
   * or user IDs.
   * @param {string[]} orderedTransitionSequences an array which is some ordering of
   * `Object.keys(eventAggregates)`.
   * @returns {string} the textual summary of the aggregated events that occurred.
   */
  _generateSummary: function (eventAggregates, orderedTransitionSequences) {
    const summaries = orderedTransitionSequences.map(transitions => {
      const userNames = eventAggregates[transitions];

      const nameList = this._renderNameList(userNames);

      const splitTransitions = transitions.split(','); // Some neighbouring transitions are common, so canonicalise some into "pair"
      // transitions

      const canonicalTransitions = this._getCanonicalTransitions(splitTransitions); // Transform into consecutive repetitions of the same transition (like 5
      // consecutive 'joined_and_left's)


      const coalescedTransitions = this._coalesceRepeatedTransitions(canonicalTransitions);

      const descs = coalescedTransitions.map(t => {
        return this._getDescriptionForTransition(t.transitionType, userNames.length, t.repeats);
      });
      const desc = (0, _FormattingUtils.formatCommaSeparatedList)(descs);
      return (0, _languageHandler._t)('%(nameList)s %(transitionList)s', {
        nameList: nameList,
        transitionList: desc
      });
    });

    if (!summaries) {
      return null;
    }

    return summaries.join(", ");
  },

  /**
   * @param {string[]} users an array of user display names or user IDs.
   * @returns {string} a comma-separated list that ends with "and [n] others" if there are
   * more items in `users` than `this.props.summaryLength`, which is the number of names
   * included before "and [n] others".
   */
  _renderNameList: function (users) {
    return (0, _FormattingUtils.formatCommaSeparatedList)(users, this.props.summaryLength);
  },

  /**
   * Canonicalise an array of transitions such that some pairs of transitions become
   * single transitions. For example an input ['joined','left'] would result in an output
   * ['joined_and_left'].
   * @param {string[]} transitions an array of transitions.
   * @returns {string[]} an array of transitions.
   */
  _getCanonicalTransitions: function (transitions) {
    const modMap = {
      'joined': {
        'after': 'left',
        'newTransition': 'joined_and_left'
      },
      'left': {
        'after': 'joined',
        'newTransition': 'left_and_joined'
      } // $currentTransition : {
      //     'after' : $nextTransition,
      //     'newTransition' : 'new_transition_type',
      // },

    };
    const res = [];

    for (let i = 0; i < transitions.length; i++) {
      const t = transitions[i];
      const t2 = transitions[i + 1];
      let transition = t;

      if (i < transitions.length - 1 && modMap[t] && modMap[t].after === t2) {
        transition = modMap[t].newTransition;
        i++;
      }

      res.push(transition);
    }

    return res;
  },

  /**
   * Transform an array of transitions into an array of transitions and how many times
   * they are repeated consecutively.
   *
   * An array of 123 "joined_and_left" transitions, would result in:
   * ```
   * [{
   *   transitionType: "joined_and_left"
   *   repeats: 123
   * }]
   * ```
   * @param {string[]} transitions the array of transitions to transform.
   * @returns {object[]} an array of coalesced transitions.
   */
  _coalesceRepeatedTransitions: function (transitions) {
    const res = [];

    for (let i = 0; i < transitions.length; i++) {
      if (res.length > 0 && res[res.length - 1].transitionType === transitions[i]) {
        res[res.length - 1].repeats += 1;
      } else {
        res.push({
          transitionType: transitions[i],
          repeats: 1
        });
      }
    }

    return res;
  },

  /**
   * For a certain transition, t, describe what happened to the users that
   * underwent the transition.
   * @param {string} t the transition type.
   * @param {number} userCount number of usernames
   * @param {number} repeats the number of times the transition was repeated in a row.
   * @returns {string} the written Human Readable equivalent of the transition.
   */
  _getDescriptionForTransition(t, userCount, repeats) {
    // The empty interpolations 'severalUsers' and 'oneUser'
    // are there only to show translators to non-English languages
    // that the verb is conjugated to plural or singular Subject.
    let res = null;

    switch (t) {
      case "joined":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)sjoined %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)sjoined %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "left":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)sleft %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)sleft %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "joined_and_left":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)sjoined and left %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)sjoined and left %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "left_and_joined":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)sleft and rejoined %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)sleft and rejoined %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "invite_reject":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)srejected their invitations %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)srejected their invitation %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "invite_withdrawal":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)shad their invitations withdrawn %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)shad their invitation withdrawn %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "invited":
        res = userCount > 1 ? (0, _languageHandler._t)("were invited %(count)s times", {
          count: repeats
        }) : (0, _languageHandler._t)("was invited %(count)s times", {
          count: repeats
        });
        break;

      case "banned":
        res = userCount > 1 ? (0, _languageHandler._t)("were banned %(count)s times", {
          count: repeats
        }) : (0, _languageHandler._t)("was banned %(count)s times", {
          count: repeats
        });
        break;

      case "unbanned":
        res = userCount > 1 ? (0, _languageHandler._t)("were unbanned %(count)s times", {
          count: repeats
        }) : (0, _languageHandler._t)("was unbanned %(count)s times", {
          count: repeats
        });
        break;

      case "kicked":
        res = userCount > 1 ? (0, _languageHandler._t)("were kicked %(count)s times", {
          count: repeats
        }) : (0, _languageHandler._t)("was kicked %(count)s times", {
          count: repeats
        });
        break;

      case "changed_name":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)schanged their name %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)schanged their name %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "changed_avatar":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)schanged their avatar %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)schanged their avatar %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;

      case "no_change":
        res = userCount > 1 ? (0, _languageHandler._t)("%(severalUsers)smade no changes %(count)s times", {
          severalUsers: "",
          count: repeats
        }) : (0, _languageHandler._t)("%(oneUser)smade no changes %(count)s times", {
          oneUser: "",
          count: repeats
        });
        break;
    }

    return res;
  },

  _getTransitionSequence: function (events) {
    return events.map(this._getTransition);
  },

  /**
   * Label a given membership event, `e`, where `getContent().membership` has
   * changed for each transition allowed by the Matrix protocol. This attempts to
   * label the membership changes that occur in `../../../TextForEvent.js`.
   * @param {MatrixEvent} e the membership change event to label.
   * @returns {string?} the transition type given to this event. This defaults to `null`
   * if a transition is not recognised.
   */
  _getTransition: function (e) {
    if (e.mxEvent.getType() === 'm.room.third_party_invite') {
      // Handle 3pid invites the same as invites so they get bundled together
      return 'invited';
    }

    switch (e.mxEvent.getContent().membership) {
      case 'invite':
        return 'invited';

      case 'ban':
        return 'banned';

      case 'join':
        if (e.mxEvent.getPrevContent().membership === 'join') {
          if (e.mxEvent.getContent().displayname !== e.mxEvent.getPrevContent().displayname) {
            return 'changed_name';
          } else if (e.mxEvent.getContent().avatar_url !== e.mxEvent.getPrevContent().avatar_url) {
            return 'changed_avatar';
          } // console.log("MELS ignoring duplicate membership join event");


          return 'no_change';
        } else {
          return 'joined';
        }

      case 'leave':
        if (e.mxEvent.getSender() === e.mxEvent.getStateKey()) {
          switch (e.mxEvent.getPrevContent().membership) {
            case 'invite':
              return 'invite_reject';

            default:
              return 'left';
          }
        }

        switch (e.mxEvent.getPrevContent().membership) {
          case 'invite':
            return 'invite_withdrawal';

          case 'ban':
            return 'unbanned';
          // sender is not target and made the target leave, if not from invite/ban then this is a kick

          default:
            return 'kicked';
        }

      default:
        return null;
    }
  },
  _getAggregate: function (userEvents) {
    // A map of aggregate type to arrays of display names. Each aggregate type
    // is a comma-delimited string of transitions, e.g. "joined,left,kicked".
    // The array of display names is the array of users who went through that
    // sequence during eventsToRender.
    const aggregate = {// $aggregateType : []:string
    }; // A map of aggregate types to the indices that order them (the index of
    // the first event for a given transition sequence)

    const aggregateIndices = {// $aggregateType : int
    };
    const users = Object.keys(userEvents);
    users.forEach(userId => {
      const firstEvent = userEvents[userId][0];
      const displayName = firstEvent.displayName;

      const seq = this._getTransitionSequence(userEvents[userId]);

      if (!aggregate[seq]) {
        aggregate[seq] = [];
        aggregateIndices[seq] = -1;
      }

      aggregate[seq].push(displayName);

      if (aggregateIndices[seq] === -1 || firstEvent.index < aggregateIndices[seq]) {
        aggregateIndices[seq] = firstEvent.index;
      }
    });
    return {
      names: aggregate,
      indices: aggregateIndices
    };
  },
  render: function () {
    const eventsToRender = this.props.events; // Map user IDs to an array of objects:

    const userEvents = {// $userId : [{
      //     // The original event
      //     mxEvent: e,
      //     // The display name of the user (if not, then user ID)
      //     displayName: e.target.name || userId,
      //     // The original index of the event in this.props.events
      //     index: index,
      // }]
    };
    const avatarMembers = [];
    eventsToRender.forEach((e, index) => {
      const userId = e.getStateKey(); // Initialise a user's events

      if (!userEvents[userId]) {
        userEvents[userId] = [];
        if (e.target) avatarMembers.push(e.target);
      }

      let displayName = userId;

      if (e.getType() === 'm.room.third_party_invite') {
        displayName = e.getContent().display_name;
      } else if (e.target) {
        displayName = e.target.name;
      }

      userEvents[userId].push({
        mxEvent: e,
        displayName,
        index: index
      });
    });

    const aggregate = this._getAggregate(userEvents); // Sort types by order of lowest event index within sequence


    const orderedTransitionSequences = Object.keys(aggregate.names).sort((seq1, seq2) => aggregate.indices[seq1] > aggregate.indices[seq2]);
    const EventListSummary = sdk.getComponent("views.elements.EventListSummary");
    return _react.default.createElement(EventListSummary, {
      events: this.props.events,
      threshold: this.props.threshold,
      onToggle: this.props.onToggle,
      startExpanded: this.props.startExpanded,
      children: this.props.children,
      summaryMembers: avatarMembers,
      summaryText: this._generateSummary(aggregate.names, orderedTransitionSequences)
    });
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL01lbWJlckV2ZW50TGlzdFN1bW1hcnkuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJldmVudHMiLCJQcm9wVHlwZXMiLCJhcnJheU9mIiwiaW5zdGFuY2VPZiIsIk1hdHJpeEV2ZW50IiwiaXNSZXF1aXJlZCIsImNoaWxkcmVuIiwiYXJyYXkiLCJzdW1tYXJ5TGVuZ3RoIiwibnVtYmVyIiwiYXZhdGFyc01heExlbmd0aCIsInRocmVzaG9sZCIsIm9uVG9nZ2xlIiwiZnVuYyIsInN0YXJ0RXhwYW5kZWQiLCJib29sIiwiZ2V0RGVmYXVsdFByb3BzIiwic2hvdWxkQ29tcG9uZW50VXBkYXRlIiwibmV4dFByb3BzIiwibGVuZ3RoIiwicHJvcHMiLCJfZ2VuZXJhdGVTdW1tYXJ5IiwiZXZlbnRBZ2dyZWdhdGVzIiwib3JkZXJlZFRyYW5zaXRpb25TZXF1ZW5jZXMiLCJzdW1tYXJpZXMiLCJtYXAiLCJ0cmFuc2l0aW9ucyIsInVzZXJOYW1lcyIsIm5hbWVMaXN0IiwiX3JlbmRlck5hbWVMaXN0Iiwic3BsaXRUcmFuc2l0aW9ucyIsInNwbGl0IiwiY2Fub25pY2FsVHJhbnNpdGlvbnMiLCJfZ2V0Q2Fub25pY2FsVHJhbnNpdGlvbnMiLCJjb2FsZXNjZWRUcmFuc2l0aW9ucyIsIl9jb2FsZXNjZVJlcGVhdGVkVHJhbnNpdGlvbnMiLCJkZXNjcyIsInQiLCJfZ2V0RGVzY3JpcHRpb25Gb3JUcmFuc2l0aW9uIiwidHJhbnNpdGlvblR5cGUiLCJyZXBlYXRzIiwiZGVzYyIsInRyYW5zaXRpb25MaXN0Iiwiam9pbiIsInVzZXJzIiwibW9kTWFwIiwicmVzIiwiaSIsInQyIiwidHJhbnNpdGlvbiIsImFmdGVyIiwibmV3VHJhbnNpdGlvbiIsInB1c2giLCJ1c2VyQ291bnQiLCJzZXZlcmFsVXNlcnMiLCJjb3VudCIsIm9uZVVzZXIiLCJfZ2V0VHJhbnNpdGlvblNlcXVlbmNlIiwiX2dldFRyYW5zaXRpb24iLCJlIiwibXhFdmVudCIsImdldFR5cGUiLCJnZXRDb250ZW50IiwibWVtYmVyc2hpcCIsImdldFByZXZDb250ZW50IiwiZGlzcGxheW5hbWUiLCJhdmF0YXJfdXJsIiwiZ2V0U2VuZGVyIiwiZ2V0U3RhdGVLZXkiLCJfZ2V0QWdncmVnYXRlIiwidXNlckV2ZW50cyIsImFnZ3JlZ2F0ZSIsImFnZ3JlZ2F0ZUluZGljZXMiLCJPYmplY3QiLCJrZXlzIiwiZm9yRWFjaCIsInVzZXJJZCIsImZpcnN0RXZlbnQiLCJzZXEiLCJpbmRleCIsIm5hbWVzIiwiaW5kaWNlcyIsInJlbmRlciIsImV2ZW50c1RvUmVuZGVyIiwiYXZhdGFyTWVtYmVycyIsInRhcmdldCIsImRpc3BsYXlfbmFtZSIsIm5hbWUiLCJzb3J0Iiwic2VxMSIsInNlcTIiLCJFdmVudExpc3RTdW1tYXJ5Iiwic2RrIiwiZ2V0Q29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBMEJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLHdCQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBQyxJQUFBQSxNQUFNLEVBQUVDLG1CQUFVQyxPQUFWLENBQWtCRCxtQkFBVUUsVUFBVixDQUFxQkMsd0JBQXJCLENBQWxCLEVBQXFEQyxVQUZ0RDtBQUdQO0FBQ0FDLElBQUFBLFFBQVEsRUFBRUwsbUJBQVVNLEtBQVYsQ0FBZ0JGLFVBSm5CO0FBS1A7QUFDQUcsSUFBQUEsYUFBYSxFQUFFUCxtQkFBVVEsTUFObEI7QUFPUDtBQUNBQyxJQUFBQSxnQkFBZ0IsRUFBRVQsbUJBQVVRLE1BUnJCO0FBU1A7QUFDQUUsSUFBQUEsU0FBUyxFQUFFVixtQkFBVVEsTUFWZDtBQVdQO0FBQ0FHLElBQUFBLFFBQVEsRUFBRVgsbUJBQVVZLElBWmI7QUFhUDtBQUNBQyxJQUFBQSxhQUFhLEVBQUViLG1CQUFVYztBQWRsQixHQUhpQjtBQW9CNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSFIsTUFBQUEsYUFBYSxFQUFFLENBRFo7QUFFSEcsTUFBQUEsU0FBUyxFQUFFLENBRlI7QUFHSEQsTUFBQUEsZ0JBQWdCLEVBQUU7QUFIZixLQUFQO0FBS0gsR0ExQjJCO0FBNEI1Qk8sRUFBQUEscUJBQXFCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQ0lBLFNBQVMsQ0FBQ2xCLE1BQVYsQ0FBaUJtQixNQUFqQixLQUE0QixLQUFLQyxLQUFMLENBQVdwQixNQUFYLENBQWtCbUIsTUFBOUMsSUFDQUQsU0FBUyxDQUFDbEIsTUFBVixDQUFpQm1CLE1BQWpCLEdBQTBCLEtBQUtDLEtBQUwsQ0FBV1QsU0FGekM7QUFJSCxHQXJDMkI7O0FBdUM1Qjs7Ozs7Ozs7O0FBU0FVLEVBQUFBLGdCQUFnQixFQUFFLFVBQVNDLGVBQVQsRUFBMEJDLDBCQUExQixFQUFzRDtBQUNwRSxVQUFNQyxTQUFTLEdBQUdELDBCQUEwQixDQUFDRSxHQUEzQixDQUFnQ0MsV0FBRCxJQUFpQjtBQUM5RCxZQUFNQyxTQUFTLEdBQUdMLGVBQWUsQ0FBQ0ksV0FBRCxDQUFqQzs7QUFDQSxZQUFNRSxRQUFRLEdBQUcsS0FBS0MsZUFBTCxDQUFxQkYsU0FBckIsQ0FBakI7O0FBRUEsWUFBTUcsZ0JBQWdCLEdBQUdKLFdBQVcsQ0FBQ0ssS0FBWixDQUFrQixHQUFsQixDQUF6QixDQUo4RCxDQU05RDtBQUNBOztBQUNBLFlBQU1DLG9CQUFvQixHQUFHLEtBQUtDLHdCQUFMLENBQThCSCxnQkFBOUIsQ0FBN0IsQ0FSOEQsQ0FTOUQ7QUFDQTs7O0FBQ0EsWUFBTUksb0JBQW9CLEdBQUcsS0FBS0MsNEJBQUwsQ0FDekJILG9CQUR5QixDQUE3Qjs7QUFJQSxZQUFNSSxLQUFLLEdBQUdGLG9CQUFvQixDQUFDVCxHQUFyQixDQUEwQlksQ0FBRCxJQUFPO0FBQzFDLGVBQU8sS0FBS0MsNEJBQUwsQ0FDSEQsQ0FBQyxDQUFDRSxjQURDLEVBQ2VaLFNBQVMsQ0FBQ1IsTUFEekIsRUFDaUNrQixDQUFDLENBQUNHLE9BRG5DLENBQVA7QUFHSCxPQUphLENBQWQ7QUFNQSxZQUFNQyxJQUFJLEdBQUcsK0NBQXlCTCxLQUF6QixDQUFiO0FBRUEsYUFBTyx5QkFBRyxpQ0FBSCxFQUFzQztBQUFFUixRQUFBQSxRQUFRLEVBQUVBLFFBQVo7QUFBc0JjLFFBQUFBLGNBQWMsRUFBRUQ7QUFBdEMsT0FBdEMsQ0FBUDtBQUNILEtBeEJpQixDQUFsQjs7QUEwQkEsUUFBSSxDQUFDakIsU0FBTCxFQUFnQjtBQUNaLGFBQU8sSUFBUDtBQUNIOztBQUVELFdBQU9BLFNBQVMsQ0FBQ21CLElBQVYsQ0FBZSxJQUFmLENBQVA7QUFDSCxHQWhGMkI7O0FBa0Y1Qjs7Ozs7O0FBTUFkLEVBQUFBLGVBQWUsRUFBRSxVQUFTZSxLQUFULEVBQWdCO0FBQzdCLFdBQU8sK0NBQXlCQSxLQUF6QixFQUFnQyxLQUFLeEIsS0FBTCxDQUFXWixhQUEzQyxDQUFQO0FBQ0gsR0ExRjJCOztBQTRGNUI7Ozs7Ozs7QUFPQXlCLEVBQUFBLHdCQUF3QixFQUFFLFVBQVNQLFdBQVQsRUFBc0I7QUFDNUMsVUFBTW1CLE1BQU0sR0FBRztBQUNYLGdCQUFVO0FBQ04saUJBQVMsTUFESDtBQUVOLHlCQUFpQjtBQUZYLE9BREM7QUFLWCxjQUFRO0FBQ0osaUJBQVMsUUFETDtBQUVKLHlCQUFpQjtBQUZiLE9BTEcsQ0FTWDtBQUNBO0FBQ0E7QUFDQTs7QUFaVyxLQUFmO0FBY0EsVUFBTUMsR0FBRyxHQUFHLEVBQVo7O0FBRUEsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHckIsV0FBVyxDQUFDUCxNQUFoQyxFQUF3QzRCLENBQUMsRUFBekMsRUFBNkM7QUFDekMsWUFBTVYsQ0FBQyxHQUFHWCxXQUFXLENBQUNxQixDQUFELENBQXJCO0FBQ0EsWUFBTUMsRUFBRSxHQUFHdEIsV0FBVyxDQUFDcUIsQ0FBQyxHQUFHLENBQUwsQ0FBdEI7QUFFQSxVQUFJRSxVQUFVLEdBQUdaLENBQWpCOztBQUVBLFVBQUlVLENBQUMsR0FBR3JCLFdBQVcsQ0FBQ1AsTUFBWixHQUFxQixDQUF6QixJQUE4QjBCLE1BQU0sQ0FBQ1IsQ0FBRCxDQUFwQyxJQUEyQ1EsTUFBTSxDQUFDUixDQUFELENBQU4sQ0FBVWEsS0FBVixLQUFvQkYsRUFBbkUsRUFBdUU7QUFDbkVDLFFBQUFBLFVBQVUsR0FBR0osTUFBTSxDQUFDUixDQUFELENBQU4sQ0FBVWMsYUFBdkI7QUFDQUosUUFBQUEsQ0FBQztBQUNKOztBQUVERCxNQUFBQSxHQUFHLENBQUNNLElBQUosQ0FBU0gsVUFBVDtBQUNIOztBQUNELFdBQU9ILEdBQVA7QUFDSCxHQWxJMkI7O0FBb0k1Qjs7Ozs7Ozs7Ozs7Ozs7QUFjQVgsRUFBQUEsNEJBQTRCLEVBQUUsVUFBU1QsV0FBVCxFQUFzQjtBQUNoRCxVQUFNb0IsR0FBRyxHQUFHLEVBQVo7O0FBQ0EsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHckIsV0FBVyxDQUFDUCxNQUFoQyxFQUF3QzRCLENBQUMsRUFBekMsRUFBNkM7QUFDekMsVUFBSUQsR0FBRyxDQUFDM0IsTUFBSixHQUFhLENBQWIsSUFBa0IyQixHQUFHLENBQUNBLEdBQUcsQ0FBQzNCLE1BQUosR0FBYSxDQUFkLENBQUgsQ0FBb0JvQixjQUFwQixLQUF1Q2IsV0FBVyxDQUFDcUIsQ0FBRCxDQUF4RSxFQUE2RTtBQUN6RUQsUUFBQUEsR0FBRyxDQUFDQSxHQUFHLENBQUMzQixNQUFKLEdBQWEsQ0FBZCxDQUFILENBQW9CcUIsT0FBcEIsSUFBK0IsQ0FBL0I7QUFDSCxPQUZELE1BRU87QUFDSE0sUUFBQUEsR0FBRyxDQUFDTSxJQUFKLENBQVM7QUFDTGIsVUFBQUEsY0FBYyxFQUFFYixXQUFXLENBQUNxQixDQUFELENBRHRCO0FBRUxQLFVBQUFBLE9BQU8sRUFBRTtBQUZKLFNBQVQ7QUFJSDtBQUNKOztBQUNELFdBQU9NLEdBQVA7QUFDSCxHQS9KMkI7O0FBaUs1Qjs7Ozs7Ozs7QUFRQVIsRUFBQUEsNEJBQTRCLENBQUNELENBQUQsRUFBSWdCLFNBQUosRUFBZWIsT0FBZixFQUF3QjtBQUNoRDtBQUNBO0FBQ0E7QUFDQSxRQUFJTSxHQUFHLEdBQUcsSUFBVjs7QUFDQSxZQUFRVCxDQUFSO0FBQ0ksV0FBSyxRQUFMO0FBQ0lTLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyx3Q0FBSCxFQUE2QztBQUFFQyxVQUFBQSxZQUFZLEVBQUUsRUFBaEI7QUFBb0JDLFVBQUFBLEtBQUssRUFBRWY7QUFBM0IsU0FBN0MsQ0FEQSxHQUVBLHlCQUFHLG1DQUFILEVBQXdDO0FBQUVnQixVQUFBQSxPQUFPLEVBQUUsRUFBWDtBQUFlRCxVQUFBQSxLQUFLLEVBQUVmO0FBQXRCLFNBQXhDLENBRk47QUFHQTs7QUFDSixXQUFLLE1BQUw7QUFDSU0sUUFBQUEsR0FBRyxHQUFJTyxTQUFTLEdBQUcsQ0FBYixHQUNBLHlCQUFHLHNDQUFILEVBQTJDO0FBQUVDLFVBQUFBLFlBQVksRUFBRSxFQUFoQjtBQUFvQkMsVUFBQUEsS0FBSyxFQUFFZjtBQUEzQixTQUEzQyxDQURBLEdBRUEseUJBQUcsaUNBQUgsRUFBc0M7QUFBRWdCLFVBQUFBLE9BQU8sRUFBRSxFQUFYO0FBQWVELFVBQUFBLEtBQUssRUFBRWY7QUFBdEIsU0FBdEMsQ0FGTjtBQUdBOztBQUNKLFdBQUssaUJBQUw7QUFDSU0sUUFBQUEsR0FBRyxHQUFJTyxTQUFTLEdBQUcsQ0FBYixHQUNBLHlCQUFHLGlEQUFILEVBQXNEO0FBQUVDLFVBQUFBLFlBQVksRUFBRSxFQUFoQjtBQUFvQkMsVUFBQUEsS0FBSyxFQUFFZjtBQUEzQixTQUF0RCxDQURBLEdBRUEseUJBQUcsNENBQUgsRUFBaUQ7QUFBRWdCLFVBQUFBLE9BQU8sRUFBRSxFQUFYO0FBQWVELFVBQUFBLEtBQUssRUFBRWY7QUFBdEIsU0FBakQsQ0FGTjtBQUdBOztBQUNKLFdBQUssaUJBQUw7QUFDSU0sUUFBQUEsR0FBRyxHQUFJTyxTQUFTLEdBQUcsQ0FBYixHQUNBLHlCQUFHLG1EQUFILEVBQXdEO0FBQUVDLFVBQUFBLFlBQVksRUFBRSxFQUFoQjtBQUFvQkMsVUFBQUEsS0FBSyxFQUFFZjtBQUEzQixTQUF4RCxDQURBLEdBRUEseUJBQUcsOENBQUgsRUFBbUQ7QUFBRWdCLFVBQUFBLE9BQU8sRUFBRSxFQUFYO0FBQWVELFVBQUFBLEtBQUssRUFBRWY7QUFBdEIsU0FBbkQsQ0FGTjtBQUdBOztBQUNKLFdBQUssZUFBTDtBQUNJTSxRQUFBQSxHQUFHLEdBQUlPLFNBQVMsR0FBRyxDQUFiLEdBQ0EseUJBQUcsNERBQUgsRUFBaUU7QUFBRUMsVUFBQUEsWUFBWSxFQUFFLEVBQWhCO0FBQW9CQyxVQUFBQSxLQUFLLEVBQUVmO0FBQTNCLFNBQWpFLENBREEsR0FFQSx5QkFBRyxzREFBSCxFQUEyRDtBQUFFZ0IsVUFBQUEsT0FBTyxFQUFFLEVBQVg7QUFBZUQsVUFBQUEsS0FBSyxFQUFFZjtBQUF0QixTQUEzRCxDQUZOO0FBR0E7O0FBQ0osV0FBSyxtQkFBTDtBQUNJTSxRQUFBQSxHQUFHLEdBQUlPLFNBQVMsR0FBRyxDQUFiLEdBQ0EseUJBQUcsaUVBQUgsRUFBc0U7QUFBRUMsVUFBQUEsWUFBWSxFQUFFLEVBQWhCO0FBQW9CQyxVQUFBQSxLQUFLLEVBQUVmO0FBQTNCLFNBQXRFLENBREEsR0FFQSx5QkFBRywyREFBSCxFQUFnRTtBQUFFZ0IsVUFBQUEsT0FBTyxFQUFFLEVBQVg7QUFBZUQsVUFBQUEsS0FBSyxFQUFFZjtBQUF0QixTQUFoRSxDQUZOO0FBR0E7O0FBQ0osV0FBSyxTQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyw4QkFBSCxFQUFtQztBQUFFRSxVQUFBQSxLQUFLLEVBQUVmO0FBQVQsU0FBbkMsQ0FEQSxHQUVBLHlCQUFHLDZCQUFILEVBQWtDO0FBQUVlLFVBQUFBLEtBQUssRUFBRWY7QUFBVCxTQUFsQyxDQUZOO0FBR0E7O0FBQ0osV0FBSyxRQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyw2QkFBSCxFQUFrQztBQUFFRSxVQUFBQSxLQUFLLEVBQUVmO0FBQVQsU0FBbEMsQ0FEQSxHQUVBLHlCQUFHLDRCQUFILEVBQWlDO0FBQUVlLFVBQUFBLEtBQUssRUFBRWY7QUFBVCxTQUFqQyxDQUZOO0FBR0E7O0FBQ0osV0FBSyxVQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRywrQkFBSCxFQUFvQztBQUFFRSxVQUFBQSxLQUFLLEVBQUVmO0FBQVQsU0FBcEMsQ0FEQSxHQUVBLHlCQUFHLDhCQUFILEVBQW1DO0FBQUVlLFVBQUFBLEtBQUssRUFBRWY7QUFBVCxTQUFuQyxDQUZOO0FBR0E7O0FBQ0osV0FBSyxRQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyw2QkFBSCxFQUFrQztBQUFFRSxVQUFBQSxLQUFLLEVBQUVmO0FBQVQsU0FBbEMsQ0FEQSxHQUVBLHlCQUFHLDRCQUFILEVBQWlDO0FBQUVlLFVBQUFBLEtBQUssRUFBRWY7QUFBVCxTQUFqQyxDQUZOO0FBR0E7O0FBQ0osV0FBSyxjQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyxvREFBSCxFQUF5RDtBQUFFQyxVQUFBQSxZQUFZLEVBQUUsRUFBaEI7QUFBb0JDLFVBQUFBLEtBQUssRUFBRWY7QUFBM0IsU0FBekQsQ0FEQSxHQUVBLHlCQUFHLCtDQUFILEVBQW9EO0FBQUVnQixVQUFBQSxPQUFPLEVBQUUsRUFBWDtBQUFlRCxVQUFBQSxLQUFLLEVBQUVmO0FBQXRCLFNBQXBELENBRk47QUFHQTs7QUFDSixXQUFLLGdCQUFMO0FBQ0lNLFFBQUFBLEdBQUcsR0FBSU8sU0FBUyxHQUFHLENBQWIsR0FDQSx5QkFBRyxzREFBSCxFQUEyRDtBQUFFQyxVQUFBQSxZQUFZLEVBQUUsRUFBaEI7QUFBb0JDLFVBQUFBLEtBQUssRUFBRWY7QUFBM0IsU0FBM0QsQ0FEQSxHQUVBLHlCQUFHLGlEQUFILEVBQXNEO0FBQUVnQixVQUFBQSxPQUFPLEVBQUUsRUFBWDtBQUFlRCxVQUFBQSxLQUFLLEVBQUVmO0FBQXRCLFNBQXRELENBRk47QUFHQTs7QUFDSixXQUFLLFdBQUw7QUFDSU0sUUFBQUEsR0FBRyxHQUFJTyxTQUFTLEdBQUcsQ0FBYixHQUNBLHlCQUFHLGlEQUFILEVBQXNEO0FBQUVDLFVBQUFBLFlBQVksRUFBRSxFQUFoQjtBQUFvQkMsVUFBQUEsS0FBSyxFQUFFZjtBQUEzQixTQUF0RCxDQURBLEdBRUEseUJBQUcsNENBQUgsRUFBaUQ7QUFBRWdCLFVBQUFBLE9BQU8sRUFBRSxFQUFYO0FBQWVELFVBQUFBLEtBQUssRUFBRWY7QUFBdEIsU0FBakQsQ0FGTjtBQUdBO0FBakVSOztBQW9FQSxXQUFPTSxHQUFQO0FBQ0gsR0FuUDJCOztBQXFQNUJXLEVBQUFBLHNCQUFzQixFQUFFLFVBQVN6RCxNQUFULEVBQWlCO0FBQ3JDLFdBQU9BLE1BQU0sQ0FBQ3lCLEdBQVAsQ0FBVyxLQUFLaUMsY0FBaEIsQ0FBUDtBQUNILEdBdlAyQjs7QUF5UDVCOzs7Ozs7OztBQVFBQSxFQUFBQSxjQUFjLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQ3hCLFFBQUlBLENBQUMsQ0FBQ0MsT0FBRixDQUFVQyxPQUFWLE9BQXdCLDJCQUE1QixFQUF5RDtBQUNyRDtBQUNBLGFBQU8sU0FBUDtBQUNIOztBQUVELFlBQVFGLENBQUMsQ0FBQ0MsT0FBRixDQUFVRSxVQUFWLEdBQXVCQyxVQUEvQjtBQUNJLFdBQUssUUFBTDtBQUFlLGVBQU8sU0FBUDs7QUFDZixXQUFLLEtBQUw7QUFBWSxlQUFPLFFBQVA7O0FBQ1osV0FBSyxNQUFMO0FBQ0ksWUFBSUosQ0FBQyxDQUFDQyxPQUFGLENBQVVJLGNBQVYsR0FBMkJELFVBQTNCLEtBQTBDLE1BQTlDLEVBQXNEO0FBQ2xELGNBQUlKLENBQUMsQ0FBQ0MsT0FBRixDQUFVRSxVQUFWLEdBQXVCRyxXQUF2QixLQUNBTixDQUFDLENBQUNDLE9BQUYsQ0FBVUksY0FBVixHQUEyQkMsV0FEL0IsRUFDNEM7QUFDeEMsbUJBQU8sY0FBUDtBQUNILFdBSEQsTUFHTyxJQUFJTixDQUFDLENBQUNDLE9BQUYsQ0FBVUUsVUFBVixHQUF1QkksVUFBdkIsS0FDUFAsQ0FBQyxDQUFDQyxPQUFGLENBQVVJLGNBQVYsR0FBMkJFLFVBRHhCLEVBQ29DO0FBQ3ZDLG1CQUFPLGdCQUFQO0FBQ0gsV0FQaUQsQ0FRbEQ7OztBQUNBLGlCQUFPLFdBQVA7QUFDSCxTQVZELE1BVU87QUFDSCxpQkFBTyxRQUFQO0FBQ0g7O0FBQ0wsV0FBSyxPQUFMO0FBQ0ksWUFBSVAsQ0FBQyxDQUFDQyxPQUFGLENBQVVPLFNBQVYsT0FBMEJSLENBQUMsQ0FBQ0MsT0FBRixDQUFVUSxXQUFWLEVBQTlCLEVBQXVEO0FBQ25ELGtCQUFRVCxDQUFDLENBQUNDLE9BQUYsQ0FBVUksY0FBVixHQUEyQkQsVUFBbkM7QUFDSSxpQkFBSyxRQUFMO0FBQWUscUJBQU8sZUFBUDs7QUFDZjtBQUFTLHFCQUFPLE1BQVA7QUFGYjtBQUlIOztBQUNELGdCQUFRSixDQUFDLENBQUNDLE9BQUYsQ0FBVUksY0FBVixHQUEyQkQsVUFBbkM7QUFDSSxlQUFLLFFBQUw7QUFBZSxtQkFBTyxtQkFBUDs7QUFDZixlQUFLLEtBQUw7QUFBWSxtQkFBTyxVQUFQO0FBQ1o7O0FBQ0E7QUFBUyxtQkFBTyxRQUFQO0FBSmI7O0FBTUo7QUFBUyxlQUFPLElBQVA7QUE5QmI7QUFnQ0gsR0F2UzJCO0FBeVM1Qk0sRUFBQUEsYUFBYSxFQUFFLFVBQVNDLFVBQVQsRUFBcUI7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFNQyxTQUFTLEdBQUcsQ0FDZDtBQURjLEtBQWxCLENBTGdDLENBUWhDO0FBQ0E7O0FBQ0EsVUFBTUMsZ0JBQWdCLEdBQUcsQ0FDckI7QUFEcUIsS0FBekI7QUFJQSxVQUFNNUIsS0FBSyxHQUFHNkIsTUFBTSxDQUFDQyxJQUFQLENBQVlKLFVBQVosQ0FBZDtBQUNBMUIsSUFBQUEsS0FBSyxDQUFDK0IsT0FBTixDQUNLQyxNQUFELElBQVk7QUFDUixZQUFNQyxVQUFVLEdBQUdQLFVBQVUsQ0FBQ00sTUFBRCxDQUFWLENBQW1CLENBQW5CLENBQW5CO0FBQ0EsWUFBTTlFLFdBQVcsR0FBRytFLFVBQVUsQ0FBQy9FLFdBQS9COztBQUVBLFlBQU1nRixHQUFHLEdBQUcsS0FBS3JCLHNCQUFMLENBQTRCYSxVQUFVLENBQUNNLE1BQUQsQ0FBdEMsQ0FBWjs7QUFDQSxVQUFJLENBQUNMLFNBQVMsQ0FBQ08sR0FBRCxDQUFkLEVBQXFCO0FBQ2pCUCxRQUFBQSxTQUFTLENBQUNPLEdBQUQsQ0FBVCxHQUFpQixFQUFqQjtBQUNBTixRQUFBQSxnQkFBZ0IsQ0FBQ00sR0FBRCxDQUFoQixHQUF3QixDQUFDLENBQXpCO0FBQ0g7O0FBRURQLE1BQUFBLFNBQVMsQ0FBQ08sR0FBRCxDQUFULENBQWUxQixJQUFmLENBQW9CdEQsV0FBcEI7O0FBRUEsVUFBSTBFLGdCQUFnQixDQUFDTSxHQUFELENBQWhCLEtBQTBCLENBQUMsQ0FBM0IsSUFDQUQsVUFBVSxDQUFDRSxLQUFYLEdBQW1CUCxnQkFBZ0IsQ0FBQ00sR0FBRCxDQUR2QyxFQUM4QztBQUN0Q04sUUFBQUEsZ0JBQWdCLENBQUNNLEdBQUQsQ0FBaEIsR0FBd0JELFVBQVUsQ0FBQ0UsS0FBbkM7QUFDUDtBQUNKLEtBakJMO0FBb0JBLFdBQU87QUFDSEMsTUFBQUEsS0FBSyxFQUFFVCxTQURKO0FBRUhVLE1BQUFBLE9BQU8sRUFBRVQ7QUFGTixLQUFQO0FBSUgsR0FoVjJCO0FBa1Y1QlUsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxjQUFjLEdBQUcsS0FBSy9ELEtBQUwsQ0FBV3BCLE1BQWxDLENBRGUsQ0FHZjs7QUFDQSxVQUFNc0UsVUFBVSxHQUFHLENBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJlLEtBQW5CO0FBV0EsVUFBTWMsYUFBYSxHQUFHLEVBQXRCO0FBQ0FELElBQUFBLGNBQWMsQ0FBQ1IsT0FBZixDQUF1QixDQUFDaEIsQ0FBRCxFQUFJb0IsS0FBSixLQUFjO0FBQ2pDLFlBQU1ILE1BQU0sR0FBR2pCLENBQUMsQ0FBQ1MsV0FBRixFQUFmLENBRGlDLENBRWpDOztBQUNBLFVBQUksQ0FBQ0UsVUFBVSxDQUFDTSxNQUFELENBQWYsRUFBeUI7QUFDckJOLFFBQUFBLFVBQVUsQ0FBQ00sTUFBRCxDQUFWLEdBQXFCLEVBQXJCO0FBQ0EsWUFBSWpCLENBQUMsQ0FBQzBCLE1BQU4sRUFBY0QsYUFBYSxDQUFDaEMsSUFBZCxDQUFtQk8sQ0FBQyxDQUFDMEIsTUFBckI7QUFDakI7O0FBRUQsVUFBSXZGLFdBQVcsR0FBRzhFLE1BQWxCOztBQUNBLFVBQUlqQixDQUFDLENBQUNFLE9BQUYsT0FBZ0IsMkJBQXBCLEVBQWlEO0FBQzdDL0QsUUFBQUEsV0FBVyxHQUFHNkQsQ0FBQyxDQUFDRyxVQUFGLEdBQWV3QixZQUE3QjtBQUNILE9BRkQsTUFFTyxJQUFJM0IsQ0FBQyxDQUFDMEIsTUFBTixFQUFjO0FBQ2pCdkYsUUFBQUEsV0FBVyxHQUFHNkQsQ0FBQyxDQUFDMEIsTUFBRixDQUFTRSxJQUF2QjtBQUNIOztBQUVEakIsTUFBQUEsVUFBVSxDQUFDTSxNQUFELENBQVYsQ0FBbUJ4QixJQUFuQixDQUF3QjtBQUNwQlEsUUFBQUEsT0FBTyxFQUFFRCxDQURXO0FBRXBCN0QsUUFBQUEsV0FGb0I7QUFHcEJpRixRQUFBQSxLQUFLLEVBQUVBO0FBSGEsT0FBeEI7QUFLSCxLQXBCRDs7QUFzQkEsVUFBTVIsU0FBUyxHQUFHLEtBQUtGLGFBQUwsQ0FBbUJDLFVBQW5CLENBQWxCLENBdENlLENBd0NmOzs7QUFDQSxVQUFNL0MsMEJBQTBCLEdBQUdrRCxNQUFNLENBQUNDLElBQVAsQ0FBWUgsU0FBUyxDQUFDUyxLQUF0QixFQUE2QlEsSUFBN0IsQ0FDL0IsQ0FBQ0MsSUFBRCxFQUFPQyxJQUFQLEtBQWdCbkIsU0FBUyxDQUFDVSxPQUFWLENBQWtCUSxJQUFsQixJQUEwQmxCLFNBQVMsQ0FBQ1UsT0FBVixDQUFrQlMsSUFBbEIsQ0FEWCxDQUFuQztBQUlBLFVBQU1DLGdCQUFnQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQXpCO0FBQ0EsV0FBTyw2QkFBQyxnQkFBRDtBQUNILE1BQUEsTUFBTSxFQUFFLEtBQUt6RSxLQUFMLENBQVdwQixNQURoQjtBQUVILE1BQUEsU0FBUyxFQUFFLEtBQUtvQixLQUFMLENBQVdULFNBRm5CO0FBR0gsTUFBQSxRQUFRLEVBQUUsS0FBS1MsS0FBTCxDQUFXUixRQUhsQjtBQUlILE1BQUEsYUFBYSxFQUFFLEtBQUtRLEtBQUwsQ0FBV04sYUFKdkI7QUFLSCxNQUFBLFFBQVEsRUFBRSxLQUFLTSxLQUFMLENBQVdkLFFBTGxCO0FBTUgsTUFBQSxjQUFjLEVBQUU4RSxhQU5iO0FBT0gsTUFBQSxXQUFXLEVBQUUsS0FBSy9ELGdCQUFMLENBQXNCa0QsU0FBUyxDQUFDUyxLQUFoQyxFQUF1Q3pELDBCQUF2QztBQVBWLE1BQVA7QUFRSDtBQXhZMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHsgZm9ybWF0Q29tbWFTZXBhcmF0ZWRMaXN0IH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvRm9ybWF0dGluZ1V0aWxzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQge01hdHJpeEV2ZW50fSBmcm9tIFwibWF0cml4LWpzLXNka1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ01lbWJlckV2ZW50TGlzdFN1bW1hcnknLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIEFuIGFycmF5IG9mIG1lbWJlciBldmVudHMgdG8gc3VtbWFyaXNlXHJcbiAgICAgICAgZXZlbnRzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMuaW5zdGFuY2VPZihNYXRyaXhFdmVudCkpLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLy8gQW4gYXJyYXkgb2YgRXZlbnRUaWxlcyB0byByZW5kZXIgd2hlbiBleHBhbmRlZFxyXG4gICAgICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMuYXJyYXkuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBUaGUgbWF4aW11bSBudW1iZXIgb2YgbmFtZXMgdG8gc2hvdyBpbiBlaXRoZXIgZWFjaCBzdW1tYXJ5IGUuZy4gMiB3b3VsZCByZXN1bHQgXCJBLCBCIGFuZCAyMzQgb3RoZXJzIGxlZnRcIlxyXG4gICAgICAgIHN1bW1hcnlMZW5ndGg6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAgICAgLy8gVGhlIG1heGltdW0gbnVtYmVyIG9mIGF2YXRhcnMgdG8gZGlzcGxheSBpbiB0aGUgc3VtbWFyeVxyXG4gICAgICAgIGF2YXRhcnNNYXhMZW5ndGg6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAgICAgLy8gVGhlIG1pbmltdW0gbnVtYmVyIG9mIGV2ZW50cyBuZWVkZWQgdG8gdHJpZ2dlciBzdW1tYXJpc2F0aW9uXHJcbiAgICAgICAgdGhyZXNob2xkOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIC8vIENhbGxlZCB3aGVuIHRoZSBNRUxTIGV4cGFuc2lvbiBpcyB0b2dnbGVkXHJcbiAgICAgICAgb25Ub2dnbGU6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIC8vIFdoZXRoZXIgb3Igbm90IHRvIGJlZ2luIHdpdGggc3RhdGUuZXhwYW5kZWQ9dHJ1ZVxyXG4gICAgICAgIHN0YXJ0RXhwYW5kZWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHN1bW1hcnlMZW5ndGg6IDEsXHJcbiAgICAgICAgICAgIHRocmVzaG9sZDogMyxcclxuICAgICAgICAgICAgYXZhdGFyc01heExlbmd0aDogNSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IGZ1bmN0aW9uKG5leHRQcm9wcykge1xyXG4gICAgICAgIC8vIFVwZGF0ZSBpZlxyXG4gICAgICAgIC8vICAtIFRoZSBudW1iZXIgb2Ygc3VtbWFyaXNlZCBldmVudHMgaGFzIGNoYW5nZWRcclxuICAgICAgICAvLyAgLSBvciBpZiB0aGUgc3VtbWFyeSBpcyBhYm91dCB0byB0b2dnbGUgdG8gYmVjb21lIGNvbGxhcHNlZFxyXG4gICAgICAgIC8vICAtIG9yIGlmIHRoZXJlIGFyZSBmZXdFdmVudHMsIG1lYW5pbmcgdGhlIGNoaWxkIGV2ZW50VGlsZXMgYXJlIHNob3duIGFzLWlzXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgbmV4dFByb3BzLmV2ZW50cy5sZW5ndGggIT09IHRoaXMucHJvcHMuZXZlbnRzLmxlbmd0aCB8fFxyXG4gICAgICAgICAgICBuZXh0UHJvcHMuZXZlbnRzLmxlbmd0aCA8IHRoaXMucHJvcHMudGhyZXNob2xkXHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZW5lcmF0ZSB0aGUgdGV4dCBmb3IgdXNlcnMgYWdncmVnYXRlZCBieSB0aGVpciB0cmFuc2l0aW9uIHNlcXVlbmNlcyAoYGV2ZW50QWdncmVnYXRlc2ApIHdoZXJlXHJcbiAgICAgKiB0aGUgc2VxdWVuY2VzIGFyZSBvcmRlcmVkIGJ5IGBvcmRlcmVkVHJhbnNpdGlvblNlcXVlbmNlc2AuXHJcbiAgICAgKiBAcGFyYW0ge29iamVjdFtdfSBldmVudEFnZ3JlZ2F0ZXMgYSBtYXAgb2YgdHJhbnNpdGlvbiBzZXF1ZW5jZSB0byBhcnJheSBvZiB1c2VyIGRpc3BsYXkgbmFtZXNcclxuICAgICAqIG9yIHVzZXIgSURzLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmdbXX0gb3JkZXJlZFRyYW5zaXRpb25TZXF1ZW5jZXMgYW4gYXJyYXkgd2hpY2ggaXMgc29tZSBvcmRlcmluZyBvZlxyXG4gICAgICogYE9iamVjdC5rZXlzKGV2ZW50QWdncmVnYXRlcylgLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ30gdGhlIHRleHR1YWwgc3VtbWFyeSBvZiB0aGUgYWdncmVnYXRlZCBldmVudHMgdGhhdCBvY2N1cnJlZC5cclxuICAgICAqL1xyXG4gICAgX2dlbmVyYXRlU3VtbWFyeTogZnVuY3Rpb24oZXZlbnRBZ2dyZWdhdGVzLCBvcmRlcmVkVHJhbnNpdGlvblNlcXVlbmNlcykge1xyXG4gICAgICAgIGNvbnN0IHN1bW1hcmllcyA9IG9yZGVyZWRUcmFuc2l0aW9uU2VxdWVuY2VzLm1hcCgodHJhbnNpdGlvbnMpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXNlck5hbWVzID0gZXZlbnRBZ2dyZWdhdGVzW3RyYW5zaXRpb25zXTtcclxuICAgICAgICAgICAgY29uc3QgbmFtZUxpc3QgPSB0aGlzLl9yZW5kZXJOYW1lTGlzdCh1c2VyTmFtZXMpO1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgc3BsaXRUcmFuc2l0aW9ucyA9IHRyYW5zaXRpb25zLnNwbGl0KCcsJyk7XHJcblxyXG4gICAgICAgICAgICAvLyBTb21lIG5laWdoYm91cmluZyB0cmFuc2l0aW9ucyBhcmUgY29tbW9uLCBzbyBjYW5vbmljYWxpc2Ugc29tZSBpbnRvIFwicGFpclwiXHJcbiAgICAgICAgICAgIC8vIHRyYW5zaXRpb25zXHJcbiAgICAgICAgICAgIGNvbnN0IGNhbm9uaWNhbFRyYW5zaXRpb25zID0gdGhpcy5fZ2V0Q2Fub25pY2FsVHJhbnNpdGlvbnMoc3BsaXRUcmFuc2l0aW9ucyk7XHJcbiAgICAgICAgICAgIC8vIFRyYW5zZm9ybSBpbnRvIGNvbnNlY3V0aXZlIHJlcGV0aXRpb25zIG9mIHRoZSBzYW1lIHRyYW5zaXRpb24gKGxpa2UgNVxyXG4gICAgICAgICAgICAvLyBjb25zZWN1dGl2ZSAnam9pbmVkX2FuZF9sZWZ0J3MpXHJcbiAgICAgICAgICAgIGNvbnN0IGNvYWxlc2NlZFRyYW5zaXRpb25zID0gdGhpcy5fY29hbGVzY2VSZXBlYXRlZFRyYW5zaXRpb25zKFxyXG4gICAgICAgICAgICAgICAgY2Fub25pY2FsVHJhbnNpdGlvbnMsXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkZXNjcyA9IGNvYWxlc2NlZFRyYW5zaXRpb25zLm1hcCgodCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2dldERlc2NyaXB0aW9uRm9yVHJhbnNpdGlvbihcclxuICAgICAgICAgICAgICAgICAgICB0LnRyYW5zaXRpb25UeXBlLCB1c2VyTmFtZXMubGVuZ3RoLCB0LnJlcGVhdHMsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGRlc2MgPSBmb3JtYXRDb21tYVNlcGFyYXRlZExpc3QoZGVzY3MpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIF90KCclKG5hbWVMaXN0KXMgJSh0cmFuc2l0aW9uTGlzdClzJywgeyBuYW1lTGlzdDogbmFtZUxpc3QsIHRyYW5zaXRpb25MaXN0OiBkZXNjIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoIXN1bW1hcmllcykge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBzdW1tYXJpZXMuam9pbihcIiwgXCIpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nW119IHVzZXJzIGFuIGFycmF5IG9mIHVzZXIgZGlzcGxheSBuYW1lcyBvciB1c2VyIElEcy5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IGEgY29tbWEtc2VwYXJhdGVkIGxpc3QgdGhhdCBlbmRzIHdpdGggXCJhbmQgW25dIG90aGVyc1wiIGlmIHRoZXJlIGFyZVxyXG4gICAgICogbW9yZSBpdGVtcyBpbiBgdXNlcnNgIHRoYW4gYHRoaXMucHJvcHMuc3VtbWFyeUxlbmd0aGAsIHdoaWNoIGlzIHRoZSBudW1iZXIgb2YgbmFtZXNcclxuICAgICAqIGluY2x1ZGVkIGJlZm9yZSBcImFuZCBbbl0gb3RoZXJzXCIuXHJcbiAgICAgKi9cclxuICAgIF9yZW5kZXJOYW1lTGlzdDogZnVuY3Rpb24odXNlcnMpIHtcclxuICAgICAgICByZXR1cm4gZm9ybWF0Q29tbWFTZXBhcmF0ZWRMaXN0KHVzZXJzLCB0aGlzLnByb3BzLnN1bW1hcnlMZW5ndGgpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbm9uaWNhbGlzZSBhbiBhcnJheSBvZiB0cmFuc2l0aW9ucyBzdWNoIHRoYXQgc29tZSBwYWlycyBvZiB0cmFuc2l0aW9ucyBiZWNvbWVcclxuICAgICAqIHNpbmdsZSB0cmFuc2l0aW9ucy4gRm9yIGV4YW1wbGUgYW4gaW5wdXQgWydqb2luZWQnLCdsZWZ0J10gd291bGQgcmVzdWx0IGluIGFuIG91dHB1dFxyXG4gICAgICogWydqb2luZWRfYW5kX2xlZnQnXS5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nW119IHRyYW5zaXRpb25zIGFuIGFycmF5IG9mIHRyYW5zaXRpb25zLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ1tdfSBhbiBhcnJheSBvZiB0cmFuc2l0aW9ucy5cclxuICAgICAqL1xyXG4gICAgX2dldENhbm9uaWNhbFRyYW5zaXRpb25zOiBmdW5jdGlvbih0cmFuc2l0aW9ucykge1xyXG4gICAgICAgIGNvbnN0IG1vZE1hcCA9IHtcclxuICAgICAgICAgICAgJ2pvaW5lZCc6IHtcclxuICAgICAgICAgICAgICAgICdhZnRlcic6ICdsZWZ0JyxcclxuICAgICAgICAgICAgICAgICduZXdUcmFuc2l0aW9uJzogJ2pvaW5lZF9hbmRfbGVmdCcsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICdsZWZ0Jzoge1xyXG4gICAgICAgICAgICAgICAgJ2FmdGVyJzogJ2pvaW5lZCcsXHJcbiAgICAgICAgICAgICAgICAnbmV3VHJhbnNpdGlvbic6ICdsZWZ0X2FuZF9qb2luZWQnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAvLyAkY3VycmVudFRyYW5zaXRpb24gOiB7XHJcbiAgICAgICAgICAgIC8vICAgICAnYWZ0ZXInIDogJG5leHRUcmFuc2l0aW9uLFxyXG4gICAgICAgICAgICAvLyAgICAgJ25ld1RyYW5zaXRpb24nIDogJ25ld190cmFuc2l0aW9uX3R5cGUnLFxyXG4gICAgICAgICAgICAvLyB9LFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgcmVzID0gW107XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdHJhbnNpdGlvbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3QgdCA9IHRyYW5zaXRpb25zW2ldO1xyXG4gICAgICAgICAgICBjb25zdCB0MiA9IHRyYW5zaXRpb25zW2kgKyAxXTtcclxuXHJcbiAgICAgICAgICAgIGxldCB0cmFuc2l0aW9uID0gdDtcclxuXHJcbiAgICAgICAgICAgIGlmIChpIDwgdHJhbnNpdGlvbnMubGVuZ3RoIC0gMSAmJiBtb2RNYXBbdF0gJiYgbW9kTWFwW3RdLmFmdGVyID09PSB0Mikge1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbiA9IG1vZE1hcFt0XS5uZXdUcmFuc2l0aW9uO1xyXG4gICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXMucHVzaCh0cmFuc2l0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUcmFuc2Zvcm0gYW4gYXJyYXkgb2YgdHJhbnNpdGlvbnMgaW50byBhbiBhcnJheSBvZiB0cmFuc2l0aW9ucyBhbmQgaG93IG1hbnkgdGltZXNcclxuICAgICAqIHRoZXkgYXJlIHJlcGVhdGVkIGNvbnNlY3V0aXZlbHkuXHJcbiAgICAgKlxyXG4gICAgICogQW4gYXJyYXkgb2YgMTIzIFwiam9pbmVkX2FuZF9sZWZ0XCIgdHJhbnNpdGlvbnMsIHdvdWxkIHJlc3VsdCBpbjpcclxuICAgICAqIGBgYFxyXG4gICAgICogW3tcclxuICAgICAqICAgdHJhbnNpdGlvblR5cGU6IFwiam9pbmVkX2FuZF9sZWZ0XCJcclxuICAgICAqICAgcmVwZWF0czogMTIzXHJcbiAgICAgKiB9XVxyXG4gICAgICogYGBgXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ1tdfSB0cmFuc2l0aW9ucyB0aGUgYXJyYXkgb2YgdHJhbnNpdGlvbnMgdG8gdHJhbnNmb3JtLlxyXG4gICAgICogQHJldHVybnMge29iamVjdFtdfSBhbiBhcnJheSBvZiBjb2FsZXNjZWQgdHJhbnNpdGlvbnMuXHJcbiAgICAgKi9cclxuICAgIF9jb2FsZXNjZVJlcGVhdGVkVHJhbnNpdGlvbnM6IGZ1bmN0aW9uKHRyYW5zaXRpb25zKSB7XHJcbiAgICAgICAgY29uc3QgcmVzID0gW107XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0cmFuc2l0aW9ucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAocmVzLmxlbmd0aCA+IDAgJiYgcmVzW3Jlcy5sZW5ndGggLSAxXS50cmFuc2l0aW9uVHlwZSA9PT0gdHJhbnNpdGlvbnNbaV0pIHtcclxuICAgICAgICAgICAgICAgIHJlc1tyZXMubGVuZ3RoIC0gMV0ucmVwZWF0cyArPSAxO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb25UeXBlOiB0cmFuc2l0aW9uc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICByZXBlYXRzOiAxLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGb3IgYSBjZXJ0YWluIHRyYW5zaXRpb24sIHQsIGRlc2NyaWJlIHdoYXQgaGFwcGVuZWQgdG8gdGhlIHVzZXJzIHRoYXRcclxuICAgICAqIHVuZGVyd2VudCB0aGUgdHJhbnNpdGlvbi5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0IHRoZSB0cmFuc2l0aW9uIHR5cGUuXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gdXNlckNvdW50IG51bWJlciBvZiB1c2VybmFtZXNcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSByZXBlYXRzIHRoZSBudW1iZXIgb2YgdGltZXMgdGhlIHRyYW5zaXRpb24gd2FzIHJlcGVhdGVkIGluIGEgcm93LlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ30gdGhlIHdyaXR0ZW4gSHVtYW4gUmVhZGFibGUgZXF1aXZhbGVudCBvZiB0aGUgdHJhbnNpdGlvbi5cclxuICAgICAqL1xyXG4gICAgX2dldERlc2NyaXB0aW9uRm9yVHJhbnNpdGlvbih0LCB1c2VyQ291bnQsIHJlcGVhdHMpIHtcclxuICAgICAgICAvLyBUaGUgZW1wdHkgaW50ZXJwb2xhdGlvbnMgJ3NldmVyYWxVc2VycycgYW5kICdvbmVVc2VyJ1xyXG4gICAgICAgIC8vIGFyZSB0aGVyZSBvbmx5IHRvIHNob3cgdHJhbnNsYXRvcnMgdG8gbm9uLUVuZ2xpc2ggbGFuZ3VhZ2VzXHJcbiAgICAgICAgLy8gdGhhdCB0aGUgdmVyYiBpcyBjb25qdWdhdGVkIHRvIHBsdXJhbCBvciBzaW5ndWxhciBTdWJqZWN0LlxyXG4gICAgICAgIGxldCByZXMgPSBudWxsO1xyXG4gICAgICAgIHN3aXRjaCAodCkge1xyXG4gICAgICAgICAgICBjYXNlIFwiam9pbmVkXCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwiJShzZXZlcmFsVXNlcnMpc2pvaW5lZCAlKGNvdW50KXMgdGltZXNcIiwgeyBzZXZlcmFsVXNlcnM6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBfdChcIiUob25lVXNlcilzam9pbmVkICUoY291bnQpcyB0aW1lc1wiLCB7IG9uZVVzZXI6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJsZWZ0XCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwiJShzZXZlcmFsVXNlcnMpc2xlZnQgJShjb3VudClzIHRpbWVzXCIsIHsgc2V2ZXJhbFVzZXJzOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KVxyXG4gICAgICAgICAgICAgICAgICAgIDogX3QoXCIlKG9uZVVzZXIpc2xlZnQgJShjb3VudClzIHRpbWVzXCIsIHsgb25lVXNlcjogXCJcIiwgY291bnQ6IHJlcGVhdHMgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImpvaW5lZF9hbmRfbGVmdFwiOlxyXG4gICAgICAgICAgICAgICAgcmVzID0gKHVzZXJDb3VudCA+IDEpXHJcbiAgICAgICAgICAgICAgICAgICAgPyBfdChcIiUoc2V2ZXJhbFVzZXJzKXNqb2luZWQgYW5kIGxlZnQgJShjb3VudClzIHRpbWVzXCIsIHsgc2V2ZXJhbFVzZXJzOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KVxyXG4gICAgICAgICAgICAgICAgICAgIDogX3QoXCIlKG9uZVVzZXIpc2pvaW5lZCBhbmQgbGVmdCAlKGNvdW50KXMgdGltZXNcIiwgeyBvbmVVc2VyOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwibGVmdF9hbmRfam9pbmVkXCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwiJShzZXZlcmFsVXNlcnMpc2xlZnQgYW5kIHJlam9pbmVkICUoY291bnQpcyB0aW1lc1wiLCB7IHNldmVyYWxVc2VyczogXCJcIiwgY291bnQ6IHJlcGVhdHMgfSlcclxuICAgICAgICAgICAgICAgICAgICA6IF90KFwiJShvbmVVc2VyKXNsZWZ0IGFuZCByZWpvaW5lZCAlKGNvdW50KXMgdGltZXNcIiwgeyBvbmVVc2VyOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwiaW52aXRlX3JlamVjdFwiOlxyXG4gICAgICAgICAgICAgICAgcmVzID0gKHVzZXJDb3VudCA+IDEpXHJcbiAgICAgICAgICAgICAgICAgICAgPyBfdChcIiUoc2V2ZXJhbFVzZXJzKXNyZWplY3RlZCB0aGVpciBpbnZpdGF0aW9ucyAlKGNvdW50KXMgdGltZXNcIiwgeyBzZXZlcmFsVXNlcnM6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBfdChcIiUob25lVXNlcilzcmVqZWN0ZWQgdGhlaXIgaW52aXRhdGlvbiAlKGNvdW50KXMgdGltZXNcIiwgeyBvbmVVc2VyOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwiaW52aXRlX3dpdGhkcmF3YWxcIjpcclxuICAgICAgICAgICAgICAgIHJlcyA9ICh1c2VyQ291bnQgPiAxKVxyXG4gICAgICAgICAgICAgICAgICAgID8gX3QoXCIlKHNldmVyYWxVc2VycylzaGFkIHRoZWlyIGludml0YXRpb25zIHdpdGhkcmF3biAlKGNvdW50KXMgdGltZXNcIiwgeyBzZXZlcmFsVXNlcnM6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBfdChcIiUob25lVXNlcilzaGFkIHRoZWlyIGludml0YXRpb24gd2l0aGRyYXduICUoY291bnQpcyB0aW1lc1wiLCB7IG9uZVVzZXI6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJpbnZpdGVkXCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwid2VyZSBpbnZpdGVkICUoY291bnQpcyB0aW1lc1wiLCB7IGNvdW50OiByZXBlYXRzIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBfdChcIndhcyBpbnZpdGVkICUoY291bnQpcyB0aW1lc1wiLCB7IGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJiYW5uZWRcIjpcclxuICAgICAgICAgICAgICAgIHJlcyA9ICh1c2VyQ291bnQgPiAxKVxyXG4gICAgICAgICAgICAgICAgICAgID8gX3QoXCJ3ZXJlIGJhbm5lZCAlKGNvdW50KXMgdGltZXNcIiwgeyBjb3VudDogcmVwZWF0cyB9KVxyXG4gICAgICAgICAgICAgICAgICAgIDogX3QoXCJ3YXMgYmFubmVkICUoY291bnQpcyB0aW1lc1wiLCB7IGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJ1bmJhbm5lZFwiOlxyXG4gICAgICAgICAgICAgICAgcmVzID0gKHVzZXJDb3VudCA+IDEpXHJcbiAgICAgICAgICAgICAgICAgICAgPyBfdChcIndlcmUgdW5iYW5uZWQgJShjb3VudClzIHRpbWVzXCIsIHsgY291bnQ6IHJlcGVhdHMgfSlcclxuICAgICAgICAgICAgICAgICAgICA6IF90KFwid2FzIHVuYmFubmVkICUoY291bnQpcyB0aW1lc1wiLCB7IGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJraWNrZWRcIjpcclxuICAgICAgICAgICAgICAgIHJlcyA9ICh1c2VyQ291bnQgPiAxKVxyXG4gICAgICAgICAgICAgICAgICAgID8gX3QoXCJ3ZXJlIGtpY2tlZCAlKGNvdW50KXMgdGltZXNcIiwgeyBjb3VudDogcmVwZWF0cyB9KVxyXG4gICAgICAgICAgICAgICAgICAgIDogX3QoXCJ3YXMga2lja2VkICUoY291bnQpcyB0aW1lc1wiLCB7IGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJjaGFuZ2VkX25hbWVcIjpcclxuICAgICAgICAgICAgICAgIHJlcyA9ICh1c2VyQ291bnQgPiAxKVxyXG4gICAgICAgICAgICAgICAgICAgID8gX3QoXCIlKHNldmVyYWxVc2VycylzY2hhbmdlZCB0aGVpciBuYW1lICUoY291bnQpcyB0aW1lc1wiLCB7IHNldmVyYWxVc2VyczogXCJcIiwgY291bnQ6IHJlcGVhdHMgfSlcclxuICAgICAgICAgICAgICAgICAgICA6IF90KFwiJShvbmVVc2VyKXNjaGFuZ2VkIHRoZWlyIG5hbWUgJShjb3VudClzIHRpbWVzXCIsIHsgb25lVXNlcjogXCJcIiwgY291bnQ6IHJlcGVhdHMgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImNoYW5nZWRfYXZhdGFyXCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwiJShzZXZlcmFsVXNlcnMpc2NoYW5nZWQgdGhlaXIgYXZhdGFyICUoY291bnQpcyB0aW1lc1wiLCB7IHNldmVyYWxVc2VyczogXCJcIiwgY291bnQ6IHJlcGVhdHMgfSlcclxuICAgICAgICAgICAgICAgICAgICA6IF90KFwiJShvbmVVc2VyKXNjaGFuZ2VkIHRoZWlyIGF2YXRhciAlKGNvdW50KXMgdGltZXNcIiwgeyBvbmVVc2VyOiBcIlwiLCBjb3VudDogcmVwZWF0cyB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwibm9fY2hhbmdlXCI6XHJcbiAgICAgICAgICAgICAgICByZXMgPSAodXNlckNvdW50ID4gMSlcclxuICAgICAgICAgICAgICAgICAgICA/IF90KFwiJShzZXZlcmFsVXNlcnMpc21hZGUgbm8gY2hhbmdlcyAlKGNvdW50KXMgdGltZXNcIiwgeyBzZXZlcmFsVXNlcnM6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBfdChcIiUob25lVXNlcilzbWFkZSBubyBjaGFuZ2VzICUoY291bnQpcyB0aW1lc1wiLCB7IG9uZVVzZXI6IFwiXCIsIGNvdW50OiByZXBlYXRzIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0VHJhbnNpdGlvblNlcXVlbmNlOiBmdW5jdGlvbihldmVudHMpIHtcclxuICAgICAgICByZXR1cm4gZXZlbnRzLm1hcCh0aGlzLl9nZXRUcmFuc2l0aW9uKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMYWJlbCBhIGdpdmVuIG1lbWJlcnNoaXAgZXZlbnQsIGBlYCwgd2hlcmUgYGdldENvbnRlbnQoKS5tZW1iZXJzaGlwYCBoYXNcclxuICAgICAqIGNoYW5nZWQgZm9yIGVhY2ggdHJhbnNpdGlvbiBhbGxvd2VkIGJ5IHRoZSBNYXRyaXggcHJvdG9jb2wuIFRoaXMgYXR0ZW1wdHMgdG9cclxuICAgICAqIGxhYmVsIHRoZSBtZW1iZXJzaGlwIGNoYW5nZXMgdGhhdCBvY2N1ciBpbiBgLi4vLi4vLi4vVGV4dEZvckV2ZW50LmpzYC5cclxuICAgICAqIEBwYXJhbSB7TWF0cml4RXZlbnR9IGUgdGhlIG1lbWJlcnNoaXAgY2hhbmdlIGV2ZW50IHRvIGxhYmVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZz99IHRoZSB0cmFuc2l0aW9uIHR5cGUgZ2l2ZW4gdG8gdGhpcyBldmVudC4gVGhpcyBkZWZhdWx0cyB0byBgbnVsbGBcclxuICAgICAqIGlmIGEgdHJhbnNpdGlvbiBpcyBub3QgcmVjb2duaXNlZC5cclxuICAgICAqL1xyXG4gICAgX2dldFRyYW5zaXRpb246IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBpZiAoZS5teEV2ZW50LmdldFR5cGUoKSA9PT0gJ20ucm9vbS50aGlyZF9wYXJ0eV9pbnZpdGUnKSB7XHJcbiAgICAgICAgICAgIC8vIEhhbmRsZSAzcGlkIGludml0ZXMgdGhlIHNhbWUgYXMgaW52aXRlcyBzbyB0aGV5IGdldCBidW5kbGVkIHRvZ2V0aGVyXHJcbiAgICAgICAgICAgIHJldHVybiAnaW52aXRlZCc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzd2l0Y2ggKGUubXhFdmVudC5nZXRDb250ZW50KCkubWVtYmVyc2hpcCkge1xyXG4gICAgICAgICAgICBjYXNlICdpbnZpdGUnOiByZXR1cm4gJ2ludml0ZWQnO1xyXG4gICAgICAgICAgICBjYXNlICdiYW4nOiByZXR1cm4gJ2Jhbm5lZCc7XHJcbiAgICAgICAgICAgIGNhc2UgJ2pvaW4nOlxyXG4gICAgICAgICAgICAgICAgaWYgKGUubXhFdmVudC5nZXRQcmV2Q29udGVudCgpLm1lbWJlcnNoaXAgPT09ICdqb2luJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlLm14RXZlbnQuZ2V0Q29udGVudCgpLmRpc3BsYXluYW1lICE9PVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlLm14RXZlbnQuZ2V0UHJldkNvbnRlbnQoKS5kaXNwbGF5bmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ2NoYW5nZWRfbmFtZSc7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChlLm14RXZlbnQuZ2V0Q29udGVudCgpLmF2YXRhcl91cmwgIT09XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGUubXhFdmVudC5nZXRQcmV2Q29udGVudCgpLmF2YXRhcl91cmwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdjaGFuZ2VkX2F2YXRhcic7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiTUVMUyBpZ25vcmluZyBkdXBsaWNhdGUgbWVtYmVyc2hpcCBqb2luIGV2ZW50XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnbm9fY2hhbmdlJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdqb2luZWQnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlICdsZWF2ZSc6XHJcbiAgICAgICAgICAgICAgICBpZiAoZS5teEV2ZW50LmdldFNlbmRlcigpID09PSBlLm14RXZlbnQuZ2V0U3RhdGVLZXkoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoZS5teEV2ZW50LmdldFByZXZDb250ZW50KCkubWVtYmVyc2hpcCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdpbnZpdGUnOiByZXR1cm4gJ2ludml0ZV9yZWplY3QnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gJ2xlZnQnO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoZS5teEV2ZW50LmdldFByZXZDb250ZW50KCkubWVtYmVyc2hpcCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2ludml0ZSc6IHJldHVybiAnaW52aXRlX3dpdGhkcmF3YWwnO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Jhbic6IHJldHVybiAndW5iYW5uZWQnO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlbmRlciBpcyBub3QgdGFyZ2V0IGFuZCBtYWRlIHRoZSB0YXJnZXQgbGVhdmUsIGlmIG5vdCBmcm9tIGludml0ZS9iYW4gdGhlbiB0aGlzIGlzIGEga2lja1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IHJldHVybiAna2lja2VkJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZGVmYXVsdDogcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0QWdncmVnYXRlOiBmdW5jdGlvbih1c2VyRXZlbnRzKSB7XHJcbiAgICAgICAgLy8gQSBtYXAgb2YgYWdncmVnYXRlIHR5cGUgdG8gYXJyYXlzIG9mIGRpc3BsYXkgbmFtZXMuIEVhY2ggYWdncmVnYXRlIHR5cGVcclxuICAgICAgICAvLyBpcyBhIGNvbW1hLWRlbGltaXRlZCBzdHJpbmcgb2YgdHJhbnNpdGlvbnMsIGUuZy4gXCJqb2luZWQsbGVmdCxraWNrZWRcIi5cclxuICAgICAgICAvLyBUaGUgYXJyYXkgb2YgZGlzcGxheSBuYW1lcyBpcyB0aGUgYXJyYXkgb2YgdXNlcnMgd2hvIHdlbnQgdGhyb3VnaCB0aGF0XHJcbiAgICAgICAgLy8gc2VxdWVuY2UgZHVyaW5nIGV2ZW50c1RvUmVuZGVyLlxyXG4gICAgICAgIGNvbnN0IGFnZ3JlZ2F0ZSA9IHtcclxuICAgICAgICAgICAgLy8gJGFnZ3JlZ2F0ZVR5cGUgOiBbXTpzdHJpbmdcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIEEgbWFwIG9mIGFnZ3JlZ2F0ZSB0eXBlcyB0byB0aGUgaW5kaWNlcyB0aGF0IG9yZGVyIHRoZW0gKHRoZSBpbmRleCBvZlxyXG4gICAgICAgIC8vIHRoZSBmaXJzdCBldmVudCBmb3IgYSBnaXZlbiB0cmFuc2l0aW9uIHNlcXVlbmNlKVxyXG4gICAgICAgIGNvbnN0IGFnZ3JlZ2F0ZUluZGljZXMgPSB7XHJcbiAgICAgICAgICAgIC8vICRhZ2dyZWdhdGVUeXBlIDogaW50XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgdXNlcnMgPSBPYmplY3Qua2V5cyh1c2VyRXZlbnRzKTtcclxuICAgICAgICB1c2Vycy5mb3JFYWNoKFxyXG4gICAgICAgICAgICAodXNlcklkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaXJzdEV2ZW50ID0gdXNlckV2ZW50c1t1c2VySWRdWzBdO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGlzcGxheU5hbWUgPSBmaXJzdEV2ZW50LmRpc3BsYXlOYW1lO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlcSA9IHRoaXMuX2dldFRyYW5zaXRpb25TZXF1ZW5jZSh1c2VyRXZlbnRzW3VzZXJJZF0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFhZ2dyZWdhdGVbc2VxXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFnZ3JlZ2F0ZVtzZXFdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgYWdncmVnYXRlSW5kaWNlc1tzZXFdID0gLTE7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgYWdncmVnYXRlW3NlcV0ucHVzaChkaXNwbGF5TmFtZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGFnZ3JlZ2F0ZUluZGljZXNbc2VxXSA9PT0gLTEgfHxcclxuICAgICAgICAgICAgICAgICAgICBmaXJzdEV2ZW50LmluZGV4IDwgYWdncmVnYXRlSW5kaWNlc1tzZXFdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFnZ3JlZ2F0ZUluZGljZXNbc2VxXSA9IGZpcnN0RXZlbnQuaW5kZXg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmFtZXM6IGFnZ3JlZ2F0ZSxcclxuICAgICAgICAgICAgaW5kaWNlczogYWdncmVnYXRlSW5kaWNlcyxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50c1RvUmVuZGVyID0gdGhpcy5wcm9wcy5ldmVudHM7XHJcblxyXG4gICAgICAgIC8vIE1hcCB1c2VyIElEcyB0byBhbiBhcnJheSBvZiBvYmplY3RzOlxyXG4gICAgICAgIGNvbnN0IHVzZXJFdmVudHMgPSB7XHJcbiAgICAgICAgICAgIC8vICR1c2VySWQgOiBbe1xyXG4gICAgICAgICAgICAvLyAgICAgLy8gVGhlIG9yaWdpbmFsIGV2ZW50XHJcbiAgICAgICAgICAgIC8vICAgICBteEV2ZW50OiBlLFxyXG4gICAgICAgICAgICAvLyAgICAgLy8gVGhlIGRpc3BsYXkgbmFtZSBvZiB0aGUgdXNlciAoaWYgbm90LCB0aGVuIHVzZXIgSUQpXHJcbiAgICAgICAgICAgIC8vICAgICBkaXNwbGF5TmFtZTogZS50YXJnZXQubmFtZSB8fCB1c2VySWQsXHJcbiAgICAgICAgICAgIC8vICAgICAvLyBUaGUgb3JpZ2luYWwgaW5kZXggb2YgdGhlIGV2ZW50IGluIHRoaXMucHJvcHMuZXZlbnRzXHJcbiAgICAgICAgICAgIC8vICAgICBpbmRleDogaW5kZXgsXHJcbiAgICAgICAgICAgIC8vIH1dXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgYXZhdGFyTWVtYmVycyA9IFtdO1xyXG4gICAgICAgIGV2ZW50c1RvUmVuZGVyLmZvckVhY2goKGUsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IGUuZ2V0U3RhdGVLZXkoKTtcclxuICAgICAgICAgICAgLy8gSW5pdGlhbGlzZSBhIHVzZXIncyBldmVudHNcclxuICAgICAgICAgICAgaWYgKCF1c2VyRXZlbnRzW3VzZXJJZF0pIHtcclxuICAgICAgICAgICAgICAgIHVzZXJFdmVudHNbdXNlcklkXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgaWYgKGUudGFyZ2V0KSBhdmF0YXJNZW1iZXJzLnB1c2goZS50YXJnZXQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgZGlzcGxheU5hbWUgPSB1c2VySWQ7XHJcbiAgICAgICAgICAgIGlmIChlLmdldFR5cGUoKSA9PT0gJ20ucm9vbS50aGlyZF9wYXJ0eV9pbnZpdGUnKSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZSA9IGUuZ2V0Q29udGVudCgpLmRpc3BsYXlfbmFtZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlLnRhcmdldCkge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWUgPSBlLnRhcmdldC5uYW1lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB1c2VyRXZlbnRzW3VzZXJJZF0ucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBteEV2ZW50OiBlLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICBpbmRleDogaW5kZXgsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBhZ2dyZWdhdGUgPSB0aGlzLl9nZXRBZ2dyZWdhdGUodXNlckV2ZW50cyk7XHJcblxyXG4gICAgICAgIC8vIFNvcnQgdHlwZXMgYnkgb3JkZXIgb2YgbG93ZXN0IGV2ZW50IGluZGV4IHdpdGhpbiBzZXF1ZW5jZVxyXG4gICAgICAgIGNvbnN0IG9yZGVyZWRUcmFuc2l0aW9uU2VxdWVuY2VzID0gT2JqZWN0LmtleXMoYWdncmVnYXRlLm5hbWVzKS5zb3J0KFxyXG4gICAgICAgICAgICAoc2VxMSwgc2VxMikgPT4gYWdncmVnYXRlLmluZGljZXNbc2VxMV0gPiBhZ2dyZWdhdGUuaW5kaWNlc1tzZXEyXSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBFdmVudExpc3RTdW1tYXJ5ID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLkV2ZW50TGlzdFN1bW1hcnlcIik7XHJcbiAgICAgICAgcmV0dXJuIDxFdmVudExpc3RTdW1tYXJ5XHJcbiAgICAgICAgICAgIGV2ZW50cz17dGhpcy5wcm9wcy5ldmVudHN9XHJcbiAgICAgICAgICAgIHRocmVzaG9sZD17dGhpcy5wcm9wcy50aHJlc2hvbGR9XHJcbiAgICAgICAgICAgIG9uVG9nZ2xlPXt0aGlzLnByb3BzLm9uVG9nZ2xlfVxyXG4gICAgICAgICAgICBzdGFydEV4cGFuZGVkPXt0aGlzLnByb3BzLnN0YXJ0RXhwYW5kZWR9XHJcbiAgICAgICAgICAgIGNoaWxkcmVuPXt0aGlzLnByb3BzLmNoaWxkcmVufVxyXG4gICAgICAgICAgICBzdW1tYXJ5TWVtYmVycz17YXZhdGFyTWVtYmVyc31cclxuICAgICAgICAgICAgc3VtbWFyeVRleHQ9e3RoaXMuX2dlbmVyYXRlU3VtbWFyeShhZ2dyZWdhdGUubmFtZXMsIG9yZGVyZWRUcmFuc2l0aW9uU2VxdWVuY2VzKX0gLz47XHJcbiAgICB9LFxyXG59KTtcclxuIl19