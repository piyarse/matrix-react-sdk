"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _PlatformPeg = _interopRequireDefault(require("../../../PlatformPeg"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This error boundary component can be used to wrap large content areas and
 * catch exceptions during rendering in the component tree below them.
 */
class ErrorBoundary extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onClearCacheAndReload", () => {
      if (!_PlatformPeg.default.get()) return;

      _MatrixClientPeg.MatrixClientPeg.get().stopClient();

      _MatrixClientPeg.MatrixClientPeg.get().store.deleteAllData().then(() => {
        _PlatformPeg.default.get().reload();
      });
    });
    (0, _defineProperty2.default)(this, "_onBugReport", () => {
      const BugReportDialog = sdk.getComponent("dialogs.BugReportDialog");

      if (!BugReportDialog) {
        return;
      }

      _Modal.default.createTrackedDialog('Bug Report Dialog', '', BugReportDialog, {
        label: 'react-soft-crash'
      });
    });
    this.state = {
      error: null
    };
  }

  static getDerivedStateFromError(error) {
    // Side effects are not permitted here, so we only update the state so
    // that the next render shows an error message.
    return {
      error
    };
  }

  componentDidCatch(error, {
    componentStack
  }) {
    // Browser consoles are better at formatting output when native errors are passed
    // in their own `console.error` invocation.
    console.error(error);
    console.error("The above error occured while React was rendering the following components:", componentStack);
  }

  render() {
    if (this.state.error) {
      const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
      const newIssueUrl = "https://github.com/vector-im/riot-web/issues/new";
      return _react.default.createElement("div", {
        className: "mx_ErrorBoundary"
      }, _react.default.createElement("div", {
        className: "mx_ErrorBoundary_body"
      }, _react.default.createElement("h1", null, (0, _languageHandler._t)("Something went wrong!")), _react.default.createElement("p", null, (0, _languageHandler._t)("Please <newIssueLink>create a new issue</newIssueLink> " + "on GitHub so that we can investigate this bug.", {}, {
        newIssueLink: sub => {
          return _react.default.createElement("a", {
            target: "_blank",
            rel: "noreferrer noopener",
            href: newIssueUrl
          }, sub);
        }
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("If you've submitted a bug via GitHub, debug logs can help " + "us track down the problem. Debug logs contain application " + "usage data including your username, the IDs or aliases of " + "the rooms or groups you have visited and the usernames of " + "other users. They do not contain messages.")), _react.default.createElement(AccessibleButton, {
        onClick: this._onBugReport,
        kind: "primary"
      }, (0, _languageHandler._t)("Submit debug logs")), _react.default.createElement(AccessibleButton, {
        onClick: this._onClearCacheAndReload,
        kind: "danger"
      }, (0, _languageHandler._t)("Clear cache and reload"))));
    }

    return this.props.children;
  }

}

exports.default = ErrorBoundary;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0Vycm9yQm91bmRhcnkuanMiXSwibmFtZXMiOlsiRXJyb3JCb3VuZGFyeSIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJQbGF0Zm9ybVBlZyIsImdldCIsIk1hdHJpeENsaWVudFBlZyIsInN0b3BDbGllbnQiLCJzdG9yZSIsImRlbGV0ZUFsbERhdGEiLCJ0aGVuIiwicmVsb2FkIiwiQnVnUmVwb3J0RGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwibGFiZWwiLCJzdGF0ZSIsImVycm9yIiwiZ2V0RGVyaXZlZFN0YXRlRnJvbUVycm9yIiwiY29tcG9uZW50RGlkQ2F0Y2giLCJjb21wb25lbnRTdGFjayIsImNvbnNvbGUiLCJyZW5kZXIiLCJBY2Nlc3NpYmxlQnV0dG9uIiwibmV3SXNzdWVVcmwiLCJuZXdJc3N1ZUxpbmsiLCJzdWIiLCJfb25CdWdSZXBvcnQiLCJfb25DbGVhckNhY2hlQW5kUmVsb2FkIiwiY2hpbGRyZW4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBOzs7O0FBSWUsTUFBTUEsYUFBTixTQUE0QkMsZUFBTUMsYUFBbEMsQ0FBZ0Q7QUFDM0RDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLGtFQXdCTSxNQUFNO0FBQzNCLFVBQUksQ0FBQ0MscUJBQVlDLEdBQVosRUFBTCxFQUF3Qjs7QUFFeEJDLHVDQUFnQkQsR0FBaEIsR0FBc0JFLFVBQXRCOztBQUNBRCx1Q0FBZ0JELEdBQWhCLEdBQXNCRyxLQUF0QixDQUE0QkMsYUFBNUIsR0FBNENDLElBQTVDLENBQWlELE1BQU07QUFDbkROLDZCQUFZQyxHQUFaLEdBQWtCTSxNQUFsQjtBQUNILE9BRkQ7QUFHSCxLQS9Ca0I7QUFBQSx3REFpQ0osTUFBTTtBQUNqQixZQUFNQyxlQUFlLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBeEI7O0FBQ0EsVUFBSSxDQUFDRixlQUFMLEVBQXNCO0FBQ2xCO0FBQ0g7O0FBQ0RHLHFCQUFNQyxtQkFBTixDQUEwQixtQkFBMUIsRUFBK0MsRUFBL0MsRUFBbURKLGVBQW5ELEVBQW9FO0FBQ2hFSyxRQUFBQSxLQUFLLEVBQUU7QUFEeUQsT0FBcEU7QUFHSCxLQXpDa0I7QUFHZixTQUFLQyxLQUFMLEdBQWE7QUFDVEMsTUFBQUEsS0FBSyxFQUFFO0FBREUsS0FBYjtBQUdIOztBQUVELFNBQU9DLHdCQUFQLENBQWdDRCxLQUFoQyxFQUF1QztBQUNuQztBQUNBO0FBQ0EsV0FBTztBQUFFQSxNQUFBQTtBQUFGLEtBQVA7QUFDSDs7QUFFREUsRUFBQUEsaUJBQWlCLENBQUNGLEtBQUQsRUFBUTtBQUFFRyxJQUFBQTtBQUFGLEdBQVIsRUFBNEI7QUFDekM7QUFDQTtBQUNBQyxJQUFBQSxPQUFPLENBQUNKLEtBQVIsQ0FBY0EsS0FBZDtBQUNBSSxJQUFBQSxPQUFPLENBQUNKLEtBQVIsQ0FDSSw2RUFESixFQUVJRyxjQUZKO0FBSUg7O0FBcUJERSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtOLEtBQUwsQ0FBV0MsS0FBZixFQUFzQjtBQUNsQixZQUFNTSxnQkFBZ0IsR0FBR1osR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFlBQU1ZLFdBQVcsR0FBRyxrREFBcEI7QUFDQSxhQUFPO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNIO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLHlDQUFLLHlCQUFHLHVCQUFILENBQUwsQ0FESixFQUVJLHdDQUFJLHlCQUNBLDREQUNBLGdEQUZBLEVBRWtELEVBRmxELEVBRXNEO0FBQ2xEQyxRQUFBQSxZQUFZLEVBQUdDLEdBQUQsSUFBUztBQUNuQixpQkFBTztBQUFHLFlBQUEsTUFBTSxFQUFDLFFBQVY7QUFBbUIsWUFBQSxHQUFHLEVBQUMscUJBQXZCO0FBQTZDLFlBQUEsSUFBSSxFQUFFRjtBQUFuRCxhQUFrRUUsR0FBbEUsQ0FBUDtBQUNIO0FBSGlELE9BRnRELENBQUosQ0FGSixFQVVJLHdDQUFJLHlCQUNBLCtEQUNBLDREQURBLEdBRUEsNERBRkEsR0FHQSw0REFIQSxHQUlBLDRDQUxBLENBQUosQ0FWSixFQWlCSSw2QkFBQyxnQkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLQyxZQUFoQztBQUE4QyxRQUFBLElBQUksRUFBQztBQUFuRCxTQUNLLHlCQUFHLG1CQUFILENBREwsQ0FqQkosRUFvQkksNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUUsS0FBS0Msc0JBQWhDO0FBQXdELFFBQUEsSUFBSSxFQUFDO0FBQTdELFNBQ0sseUJBQUcsd0JBQUgsQ0FETCxDQXBCSixDQURHLENBQVA7QUEwQkg7O0FBRUQsV0FBTyxLQUFLM0IsS0FBTCxDQUFXNEIsUUFBbEI7QUFDSDs7QUE3RTBEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSAnLi4vLi4vLi4vUGxhdGZvcm1QZWcnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5cclxuLyoqXHJcbiAqIFRoaXMgZXJyb3IgYm91bmRhcnkgY29tcG9uZW50IGNhbiBiZSB1c2VkIHRvIHdyYXAgbGFyZ2UgY29udGVudCBhcmVhcyBhbmRcclxuICogY2F0Y2ggZXhjZXB0aW9ucyBkdXJpbmcgcmVuZGVyaW5nIGluIHRoZSBjb21wb25lbnQgdHJlZSBiZWxvdyB0aGVtLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXJyb3JCb3VuZGFyeSBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21FcnJvcihlcnJvcikge1xyXG4gICAgICAgIC8vIFNpZGUgZWZmZWN0cyBhcmUgbm90IHBlcm1pdHRlZCBoZXJlLCBzbyB3ZSBvbmx5IHVwZGF0ZSB0aGUgc3RhdGUgc29cclxuICAgICAgICAvLyB0aGF0IHRoZSBuZXh0IHJlbmRlciBzaG93cyBhbiBlcnJvciBtZXNzYWdlLlxyXG4gICAgICAgIHJldHVybiB7IGVycm9yIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkQ2F0Y2goZXJyb3IsIHsgY29tcG9uZW50U3RhY2sgfSkge1xyXG4gICAgICAgIC8vIEJyb3dzZXIgY29uc29sZXMgYXJlIGJldHRlciBhdCBmb3JtYXR0aW5nIG91dHB1dCB3aGVuIG5hdGl2ZSBlcnJvcnMgYXJlIHBhc3NlZFxyXG4gICAgICAgIC8vIGluIHRoZWlyIG93biBgY29uc29sZS5lcnJvcmAgaW52b2NhdGlvbi5cclxuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICBjb25zb2xlLmVycm9yKFxyXG4gICAgICAgICAgICBcIlRoZSBhYm92ZSBlcnJvciBvY2N1cmVkIHdoaWxlIFJlYWN0IHdhcyByZW5kZXJpbmcgdGhlIGZvbGxvd2luZyBjb21wb25lbnRzOlwiLFxyXG4gICAgICAgICAgICBjb21wb25lbnRTdGFjayxcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNsZWFyQ2FjaGVBbmRSZWxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgaWYgKCFQbGF0Zm9ybVBlZy5nZXQoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc3RvcENsaWVudCgpO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zdG9yZS5kZWxldGVBbGxEYXRhKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIFBsYXRmb3JtUGVnLmdldCgpLnJlbG9hZCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25CdWdSZXBvcnQgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgQnVnUmVwb3J0RGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuQnVnUmVwb3J0RGlhbG9nXCIpO1xyXG4gICAgICAgIGlmICghQnVnUmVwb3J0RGlhbG9nKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnQnVnIFJlcG9ydCBEaWFsb2cnLCAnJywgQnVnUmVwb3J0RGlhbG9nLCB7XHJcbiAgICAgICAgICAgIGxhYmVsOiAncmVhY3Qtc29mdC1jcmFzaCcsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvcikge1xyXG4gICAgICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgICAgICBjb25zdCBuZXdJc3N1ZVVybCA9IFwiaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvbmV3XCI7XHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0Vycm9yQm91bmRhcnlcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXJyb3JCb3VuZGFyeV9ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgxPntfdChcIlNvbWV0aGluZyB3ZW50IHdyb25nIVwiKX08L2gxPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJQbGVhc2UgPG5ld0lzc3VlTGluaz5jcmVhdGUgYSBuZXcgaXNzdWU8L25ld0lzc3VlTGluaz4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIm9uIEdpdEh1YiBzbyB0aGF0IHdlIGNhbiBpbnZlc3RpZ2F0ZSB0aGlzIGJ1Zy5cIiwge30sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ld0lzc3VlTGluazogKHN1YikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8YSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgaHJlZj17bmV3SXNzdWVVcmx9Pnsgc3ViIH08L2E+O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiSWYgeW91J3ZlIHN1Ym1pdHRlZCBhIGJ1ZyB2aWEgR2l0SHViLCBkZWJ1ZyBsb2dzIGNhbiBoZWxwIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ1cyB0cmFjayBkb3duIHRoZSBwcm9ibGVtLiBEZWJ1ZyBsb2dzIGNvbnRhaW4gYXBwbGljYXRpb24gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInVzYWdlIGRhdGEgaW5jbHVkaW5nIHlvdXIgdXNlcm5hbWUsIHRoZSBJRHMgb3IgYWxpYXNlcyBvZiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGhlIHJvb21zIG9yIGdyb3VwcyB5b3UgaGF2ZSB2aXNpdGVkIGFuZCB0aGUgdXNlcm5hbWVzIG9mIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJvdGhlciB1c2Vycy4gVGhleSBkbyBub3QgY29udGFpbiBtZXNzYWdlcy5cIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkJ1Z1JlcG9ydH0ga2luZD0ncHJpbWFyeSc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIlN1Ym1pdCBkZWJ1ZyBsb2dzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbkNsZWFyQ2FjaGVBbmRSZWxvYWR9IGtpbmQ9J2Rhbmdlcic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkNsZWFyIGNhY2hlIGFuZCByZWxvYWRcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLmNoaWxkcmVuO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==