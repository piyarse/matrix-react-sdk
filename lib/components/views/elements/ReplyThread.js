"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _DateUtils = require("../../../DateUtils");

var _matrixJsSdk = require("matrix-js-sdk");

var _Permalinks = require("../../../utils/permalinks/Permalinks");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _escapeHtml = _interopRequireDefault(require("escape-html"));

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

/*
Copyright 2017 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// This component does no cycle detection, simply because the only way to make such a cycle would be to
// craft event_id's, using a homeserver that generates predictable event IDs; even then the impact would
// be low as each event being loaded (after the first) is triggered by an explicit user action.
class ReplyThread extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onRoomRedaction", (ev, room) => {
      if (this.unmounted) return; // If one of the events we are rendering gets redacted, force a re-render

      if (this.state.events.some(event => event.getId() === ev.getId())) {
        this.forceUpdate();
      }
    });
    this.state = {
      // The loaded events to be rendered as linear-replies
      events: [],
      // The latest loaded event which has not yet been shown
      loadedEv: null,
      // Whether the component is still loading more events
      loading: true,
      // Whether as error was encountered fetching a replied to event.
      err: false
    };
    this.onQuoteClick = this.onQuoteClick.bind(this);
    this.canCollapse = this.canCollapse.bind(this);
    this.collapse = this.collapse.bind(this);
  }

  static getParentEventId(ev) {
    if (!ev || ev.isRedacted()) return; // XXX: For newer relations (annotations, replacements, etc.), we now
    // have a `getRelation` helper on the event, and you might assume it
    // could be used here for replies as well... However, the helper
    // currently assumes the relation has a `rel_type`, which older replies
    // do not, so this block is left as-is for now.

    const mRelatesTo = ev.getWireContent()['m.relates_to'];

    if (mRelatesTo && mRelatesTo['m.in_reply_to']) {
      const mInReplyTo = mRelatesTo['m.in_reply_to'];
      if (mInReplyTo && mInReplyTo['event_id']) return mInReplyTo['event_id'];
    }
  } // Part of Replies fallback support


  static stripPlainReply(body) {
    // Removes lines beginning with `> ` until you reach one that doesn't.
    const lines = body.split('\n');

    while (lines.length && lines[0].startsWith('> ')) lines.shift(); // Reply fallback has a blank line after it, so remove it to prevent leading newline


    if (lines[0] === '') lines.shift();
    return lines.join('\n');
  } // Part of Replies fallback support


  static stripHTMLReply(html) {
    return html.replace(/^<mx-reply>[\s\S]+?<\/mx-reply>/, '');
  } // Part of Replies fallback support


  static getNestedReplyText(ev, permalinkCreator) {
    if (!ev) return null;
    let {
      body,
      formatted_body: html
    } = ev.getContent();

    if (this.getParentEventId(ev)) {
      if (body) body = this.stripPlainReply(body);
      if (html) html = this.stripHTMLReply(html);
    }

    if (!body) body = ""; // Always ensure we have a body, for reasons.
    // Escape the body to use as HTML below.
    // We also run a nl2br over the result to fix the fallback representation. We do this
    // after converting the text to safe HTML to avoid user-provided BR's from being converted.

    if (!html) html = (0, _escapeHtml.default)(body).replace(/\n/g, '<br/>'); // dev note: do not rely on `body` being safe for HTML usage below.

    const evLink = permalinkCreator.forEvent(ev.getId());
    const userLink = (0, _Permalinks.makeUserPermalink)(ev.getSender());
    const mxid = ev.getSender(); // This fallback contains text that is explicitly EN.

    switch (ev.getContent().msgtype) {
      case 'm.text':
      case 'm.notice':
        {
          html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> <a href=\"").concat(userLink, "\">").concat(mxid, "</a>") + "<br>".concat(html, "</blockquote></mx-reply>");
          const lines = body.trim().split('\n');

          if (lines.length > 0) {
            lines[0] = "<".concat(mxid, "> ").concat(lines[0]);
            body = lines.map(line => "> ".concat(line)).join('\n') + '\n\n';
          }

          break;
        }

      case 'm.image':
        html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> <a href=\"").concat(userLink, "\">").concat(mxid, "</a>") + "<br>sent an image.</blockquote></mx-reply>";
        body = "> <".concat(mxid, "> sent an image.\n\n");
        break;

      case 'm.video':
        html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> <a href=\"").concat(userLink, "\">").concat(mxid, "</a>") + "<br>sent a video.</blockquote></mx-reply>";
        body = "> <".concat(mxid, "> sent a video.\n\n");
        break;

      case 'm.audio':
        html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> <a href=\"").concat(userLink, "\">").concat(mxid, "</a>") + "<br>sent an audio file.</blockquote></mx-reply>";
        body = "> <".concat(mxid, "> sent an audio file.\n\n");
        break;

      case 'm.file':
        html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> <a href=\"").concat(userLink, "\">").concat(mxid, "</a>") + "<br>sent a file.</blockquote></mx-reply>";
        body = "> <".concat(mxid, "> sent a file.\n\n");
        break;

      case 'm.emote':
        {
          html = "<mx-reply><blockquote><a href=\"".concat(evLink, "\">In reply to</a> * ") + "<a href=\"".concat(userLink, "\">").concat(mxid, "</a><br>").concat(html, "</blockquote></mx-reply>");
          const lines = body.trim().split('\n');

          if (lines.length > 0) {
            lines[0] = "* <".concat(mxid, "> ").concat(lines[0]);
            body = lines.map(line => "> ".concat(line)).join('\n') + '\n\n';
          }

          break;
        }

      default:
        return null;
    }

    return {
      body,
      html
    };
  }

  static makeReplyMixIn(ev) {
    if (!ev) return {};
    return {
      'm.relates_to': {
        'm.in_reply_to': {
          'event_id': ev.getId()
        }
      }
    };
  }

  static makeThread(parentEv, onHeightChanged, permalinkCreator, ref) {
    if (!ReplyThread.getParentEventId(parentEv)) {
      return _react.default.createElement("div", null);
    }

    return _react.default.createElement(ReplyThread, {
      parentEv: parentEv,
      onHeightChanged: onHeightChanged,
      ref: ref,
      permalinkCreator: permalinkCreator
    });
  }

  componentDidMount() {
    this.unmounted = false;
    this.room = this.context.getRoom(this.props.parentEv.getRoomId());
    this.room.on("Room.redaction", this.onRoomRedaction); // same event handler as Room.redaction as for both we just do forceUpdate

    this.room.on("Room.redactionCancelled", this.onRoomRedaction);
    this.initialize();
  }

  componentDidUpdate() {
    this.props.onHeightChanged();
  }

  componentWillUnmount() {
    this.unmounted = true;

    if (this.room) {
      this.room.removeListener("Room.redaction", this.onRoomRedaction);
      this.room.removeListener("Room.redactionCancelled", this.onRoomRedaction);
    }
  }

  async initialize() {
    const {
      parentEv
    } = this.props; // at time of making this component we checked that props.parentEv has a parentEventId

    const ev = await this.getEvent(ReplyThread.getParentEventId(parentEv));
    if (this.unmounted) return;

    if (ev) {
      this.setState({
        events: [ev]
      }, this.loadNextEvent);
    } else {
      this.setState({
        err: true
      });
    }
  }

  async loadNextEvent() {
    if (this.unmounted) return;
    const ev = this.state.events[0];
    const inReplyToEventId = ReplyThread.getParentEventId(ev);

    if (!inReplyToEventId) {
      this.setState({
        loading: false
      });
      return;
    }

    const loadedEv = await this.getEvent(inReplyToEventId);
    if (this.unmounted) return;

    if (loadedEv) {
      this.setState({
        loadedEv
      });
    } else {
      this.setState({
        err: true
      });
    }
  }

  async getEvent(eventId) {
    const event = this.room.findEventById(eventId);
    if (event) return event;

    try {
      // ask the client to fetch the event we want using the context API, only interface to do so is to ask
      // for a timeline with that event, but once it is loaded we can use findEventById to look up the ev map
      await this.context.getEventTimeline(this.room.getUnfilteredTimelineSet(), eventId);
    } catch (e) {
      // if it fails catch the error and return early, there's no point trying to find the event in this case.
      // Return null as it is falsey and thus should be treated as an error (as the event cannot be resolved).
      return null;
    }

    return this.room.findEventById(eventId);
  }

  canCollapse() {
    return this.state.events.length > 1;
  }

  collapse() {
    this.initialize();
  }

  onQuoteClick() {
    const events = [this.state.loadedEv, ...this.state.events];
    this.setState({
      loadedEv: null,
      events
    }, this.loadNextEvent);

    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });
  }

  render() {
    let header = null;

    if (this.state.err) {
      header = _react.default.createElement("blockquote", {
        className: "mx_ReplyThread mx_ReplyThread_error"
      }, (0, _languageHandler._t)('Unable to load event that was replied to, ' + 'it either does not exist or you do not have permission to view it.'));
    } else if (this.state.loadedEv) {
      const ev = this.state.loadedEv;
      const Pill = sdk.getComponent('elements.Pill');
      const room = this.context.getRoom(ev.getRoomId());
      header = _react.default.createElement("blockquote", {
        className: "mx_ReplyThread"
      }, (0, _languageHandler._t)('<a>In reply to</a> <pill>', {}, {
        'a': sub => _react.default.createElement("a", {
          onClick: this.onQuoteClick,
          className: "mx_ReplyThread_show"
        }, sub),
        'pill': _react.default.createElement(Pill, {
          type: Pill.TYPE_USER_MENTION,
          room: room,
          url: (0, _Permalinks.makeUserPermalink)(ev.getSender()),
          shouldShowPillAvatar: true
        })
      }));
    } else if (this.state.loading) {
      const Spinner = sdk.getComponent("elements.Spinner");
      header = _react.default.createElement(Spinner, {
        w: 16,
        h: 16
      });
    }

    const EventTile = sdk.getComponent('views.rooms.EventTile');
    const DateSeparator = sdk.getComponent('messages.DateSeparator');
    const evTiles = this.state.events.map(ev => {
      let dateSep = null;

      if ((0, _DateUtils.wantsDateSeparator)(this.props.parentEv.getDate(), ev.getDate())) {
        dateSep = _react.default.createElement("a", {
          href: this.props.url
        }, _react.default.createElement(DateSeparator, {
          ts: ev.getTs()
        }));
      }

      return _react.default.createElement("blockquote", {
        className: "mx_ReplyThread",
        key: ev.getId()
      }, dateSep, _react.default.createElement(EventTile, {
        mxEvent: ev,
        tileShape: "reply",
        onHeightChanged: this.props.onHeightChanged,
        permalinkCreator: this.props.permalinkCreator,
        isRedacted: ev.isRedacted(),
        isTwelveHour: _SettingsStore.default.getValue("showTwelveHourTimestamps")
      }));
    });
    return _react.default.createElement("div", null, _react.default.createElement("div", null, header), _react.default.createElement("div", null, evTiles));
  }

}

exports.default = ReplyThread;
(0, _defineProperty2.default)(ReplyThread, "propTypes", {
  // the latest event in this chain of replies
  parentEv: _propTypes.default.instanceOf(_matrixJsSdk.MatrixEvent),
  // called when the ReplyThread contents has changed, including EventTiles thereof
  onHeightChanged: _propTypes.default.func.isRequired,
  permalinkCreator: _propTypes.default.instanceOf(_Permalinks.RoomPermalinkCreator).isRequired
});
(0, _defineProperty2.default)(ReplyThread, "contextType", _MatrixClientContext.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1JlcGx5VGhyZWFkLmpzIl0sIm5hbWVzIjpbIlJlcGx5VGhyZWFkIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiZXYiLCJyb29tIiwidW5tb3VudGVkIiwic3RhdGUiLCJldmVudHMiLCJzb21lIiwiZXZlbnQiLCJnZXRJZCIsImZvcmNlVXBkYXRlIiwibG9hZGVkRXYiLCJsb2FkaW5nIiwiZXJyIiwib25RdW90ZUNsaWNrIiwiYmluZCIsImNhbkNvbGxhcHNlIiwiY29sbGFwc2UiLCJnZXRQYXJlbnRFdmVudElkIiwiaXNSZWRhY3RlZCIsIm1SZWxhdGVzVG8iLCJnZXRXaXJlQ29udGVudCIsIm1JblJlcGx5VG8iLCJzdHJpcFBsYWluUmVwbHkiLCJib2R5IiwibGluZXMiLCJzcGxpdCIsImxlbmd0aCIsInN0YXJ0c1dpdGgiLCJzaGlmdCIsImpvaW4iLCJzdHJpcEhUTUxSZXBseSIsImh0bWwiLCJyZXBsYWNlIiwiZ2V0TmVzdGVkUmVwbHlUZXh0IiwicGVybWFsaW5rQ3JlYXRvciIsImZvcm1hdHRlZF9ib2R5IiwiZ2V0Q29udGVudCIsImV2TGluayIsImZvckV2ZW50IiwidXNlckxpbmsiLCJnZXRTZW5kZXIiLCJteGlkIiwibXNndHlwZSIsInRyaW0iLCJtYXAiLCJsaW5lIiwibWFrZVJlcGx5TWl4SW4iLCJtYWtlVGhyZWFkIiwicGFyZW50RXYiLCJvbkhlaWdodENoYW5nZWQiLCJyZWYiLCJjb21wb25lbnREaWRNb3VudCIsImNvbnRleHQiLCJnZXRSb29tIiwiZ2V0Um9vbUlkIiwib24iLCJvblJvb21SZWRhY3Rpb24iLCJpbml0aWFsaXplIiwiY29tcG9uZW50RGlkVXBkYXRlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZW1vdmVMaXN0ZW5lciIsImdldEV2ZW50Iiwic2V0U3RhdGUiLCJsb2FkTmV4dEV2ZW50IiwiaW5SZXBseVRvRXZlbnRJZCIsImV2ZW50SWQiLCJmaW5kRXZlbnRCeUlkIiwiZ2V0RXZlbnRUaW1lbGluZSIsImdldFVuZmlsdGVyZWRUaW1lbGluZVNldCIsImUiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJlbmRlciIsImhlYWRlciIsIlBpbGwiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJzdWIiLCJUWVBFX1VTRVJfTUVOVElPTiIsIlNwaW5uZXIiLCJFdmVudFRpbGUiLCJEYXRlU2VwYXJhdG9yIiwiZXZUaWxlcyIsImRhdGVTZXAiLCJnZXREYXRlIiwidXJsIiwiZ2V0VHMiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJQcm9wVHlwZXMiLCJpbnN0YW5jZU9mIiwiTWF0cml4RXZlbnQiLCJmdW5jIiwiaXNSZXF1aXJlZCIsIlJvb21QZXJtYWxpbmtDcmVhdG9yIiwiTWF0cml4Q2xpZW50Q29udGV4dCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUEzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBO0FBQ0E7QUFDQTtBQUNlLE1BQU1BLFdBQU4sU0FBMEJDLGVBQU1DLFNBQWhDLENBQTBDO0FBV3JEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSwyREFvS0QsQ0FBQ0MsRUFBRCxFQUFLQyxJQUFMLEtBQWM7QUFDNUIsVUFBSSxLQUFLQyxTQUFULEVBQW9CLE9BRFEsQ0FHNUI7O0FBQ0EsVUFBSSxLQUFLQyxLQUFMLENBQVdDLE1BQVgsQ0FBa0JDLElBQWxCLENBQXVCQyxLQUFLLElBQUlBLEtBQUssQ0FBQ0MsS0FBTixPQUFrQlAsRUFBRSxDQUFDTyxLQUFILEVBQWxELENBQUosRUFBbUU7QUFDL0QsYUFBS0MsV0FBTDtBQUNIO0FBQ0osS0EzS2tCO0FBR2YsU0FBS0wsS0FBTCxHQUFhO0FBQ1Q7QUFDQUMsTUFBQUEsTUFBTSxFQUFFLEVBRkM7QUFJVDtBQUNBSyxNQUFBQSxRQUFRLEVBQUUsSUFMRDtBQU1UO0FBQ0FDLE1BQUFBLE9BQU8sRUFBRSxJQVBBO0FBU1Q7QUFDQUMsTUFBQUEsR0FBRyxFQUFFO0FBVkksS0FBYjtBQWFBLFNBQUtDLFlBQUwsR0FBb0IsS0FBS0EsWUFBTCxDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEI7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLEtBQUtBLFdBQUwsQ0FBaUJELElBQWpCLENBQXNCLElBQXRCLENBQW5CO0FBQ0EsU0FBS0UsUUFBTCxHQUFnQixLQUFLQSxRQUFMLENBQWNGLElBQWQsQ0FBbUIsSUFBbkIsQ0FBaEI7QUFDSDs7QUFFRCxTQUFPRyxnQkFBUCxDQUF3QmhCLEVBQXhCLEVBQTRCO0FBQ3hCLFFBQUksQ0FBQ0EsRUFBRCxJQUFPQSxFQUFFLENBQUNpQixVQUFILEVBQVgsRUFBNEIsT0FESixDQUd4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFVBQU1DLFVBQVUsR0FBR2xCLEVBQUUsQ0FBQ21CLGNBQUgsR0FBb0IsY0FBcEIsQ0FBbkI7O0FBQ0EsUUFBSUQsVUFBVSxJQUFJQSxVQUFVLENBQUMsZUFBRCxDQUE1QixFQUErQztBQUMzQyxZQUFNRSxVQUFVLEdBQUdGLFVBQVUsQ0FBQyxlQUFELENBQTdCO0FBQ0EsVUFBSUUsVUFBVSxJQUFJQSxVQUFVLENBQUMsVUFBRCxDQUE1QixFQUEwQyxPQUFPQSxVQUFVLENBQUMsVUFBRCxDQUFqQjtBQUM3QztBQUNKLEdBN0NvRCxDQStDckQ7OztBQUNBLFNBQU9DLGVBQVAsQ0FBdUJDLElBQXZCLEVBQTZCO0FBQ3pCO0FBQ0EsVUFBTUMsS0FBSyxHQUFHRCxJQUFJLENBQUNFLEtBQUwsQ0FBVyxJQUFYLENBQWQ7O0FBQ0EsV0FBT0QsS0FBSyxDQUFDRSxNQUFOLElBQWdCRixLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNHLFVBQVQsQ0FBb0IsSUFBcEIsQ0FBdkIsRUFBa0RILEtBQUssQ0FBQ0ksS0FBTixHQUh6QixDQUl6Qjs7O0FBQ0EsUUFBSUosS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFhLEVBQWpCLEVBQXFCQSxLQUFLLENBQUNJLEtBQU47QUFDckIsV0FBT0osS0FBSyxDQUFDSyxJQUFOLENBQVcsSUFBWCxDQUFQO0FBQ0gsR0F2RG9ELENBeURyRDs7O0FBQ0EsU0FBT0MsY0FBUCxDQUFzQkMsSUFBdEIsRUFBNEI7QUFDeEIsV0FBT0EsSUFBSSxDQUFDQyxPQUFMLENBQWEsaUNBQWIsRUFBZ0QsRUFBaEQsQ0FBUDtBQUNILEdBNURvRCxDQThEckQ7OztBQUNBLFNBQU9DLGtCQUFQLENBQTBCaEMsRUFBMUIsRUFBOEJpQyxnQkFBOUIsRUFBZ0Q7QUFDNUMsUUFBSSxDQUFDakMsRUFBTCxFQUFTLE9BQU8sSUFBUDtBQUVULFFBQUk7QUFBQ3NCLE1BQUFBLElBQUQ7QUFBT1ksTUFBQUEsY0FBYyxFQUFFSjtBQUF2QixRQUErQjlCLEVBQUUsQ0FBQ21DLFVBQUgsRUFBbkM7O0FBQ0EsUUFBSSxLQUFLbkIsZ0JBQUwsQ0FBc0JoQixFQUF0QixDQUFKLEVBQStCO0FBQzNCLFVBQUlzQixJQUFKLEVBQVVBLElBQUksR0FBRyxLQUFLRCxlQUFMLENBQXFCQyxJQUFyQixDQUFQO0FBQ1YsVUFBSVEsSUFBSixFQUFVQSxJQUFJLEdBQUcsS0FBS0QsY0FBTCxDQUFvQkMsSUFBcEIsQ0FBUDtBQUNiOztBQUVELFFBQUksQ0FBQ1IsSUFBTCxFQUFXQSxJQUFJLEdBQUcsRUFBUCxDQVRpQyxDQVN0QjtBQUV0QjtBQUNBO0FBQ0E7O0FBQ0EsUUFBSSxDQUFDUSxJQUFMLEVBQVdBLElBQUksR0FBRyx5QkFBV1IsSUFBWCxFQUFpQlMsT0FBakIsQ0FBeUIsS0FBekIsRUFBZ0MsT0FBaEMsQ0FBUCxDQWRpQyxDQWdCNUM7O0FBRUEsVUFBTUssTUFBTSxHQUFHSCxnQkFBZ0IsQ0FBQ0ksUUFBakIsQ0FBMEJyQyxFQUFFLENBQUNPLEtBQUgsRUFBMUIsQ0FBZjtBQUNBLFVBQU0rQixRQUFRLEdBQUcsbUNBQWtCdEMsRUFBRSxDQUFDdUMsU0FBSCxFQUFsQixDQUFqQjtBQUNBLFVBQU1DLElBQUksR0FBR3hDLEVBQUUsQ0FBQ3VDLFNBQUgsRUFBYixDQXBCNEMsQ0FzQjVDOztBQUNBLFlBQVF2QyxFQUFFLENBQUNtQyxVQUFILEdBQWdCTSxPQUF4QjtBQUNJLFdBQUssUUFBTDtBQUNBLFdBQUssVUFBTDtBQUFpQjtBQUNiWCxVQUFBQSxJQUFJLEdBQUcsMENBQWtDTSxNQUFsQywwQ0FBc0VFLFFBQXRFLGdCQUFtRkUsSUFBbkYsMEJBQ01WLElBRE4sNkJBQVA7QUFFQSxnQkFBTVAsS0FBSyxHQUFHRCxJQUFJLENBQUNvQixJQUFMLEdBQVlsQixLQUFaLENBQWtCLElBQWxCLENBQWQ7O0FBQ0EsY0FBSUQsS0FBSyxDQUFDRSxNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEJGLFlBQUFBLEtBQUssQ0FBQyxDQUFELENBQUwsY0FBZWlCLElBQWYsZUFBd0JqQixLQUFLLENBQUMsQ0FBRCxDQUE3QjtBQUNBRCxZQUFBQSxJQUFJLEdBQUdDLEtBQUssQ0FBQ29CLEdBQU4sQ0FBV0MsSUFBRCxnQkFBZUEsSUFBZixDQUFWLEVBQWlDaEIsSUFBakMsQ0FBc0MsSUFBdEMsSUFBOEMsTUFBckQ7QUFDSDs7QUFDRDtBQUNIOztBQUNELFdBQUssU0FBTDtBQUNJRSxRQUFBQSxJQUFJLEdBQUcsMENBQWtDTSxNQUFsQywwQ0FBc0VFLFFBQXRFLGdCQUFtRkUsSUFBbkYsd0RBQVA7QUFFQWxCLFFBQUFBLElBQUksZ0JBQVNrQixJQUFULHlCQUFKO0FBQ0E7O0FBQ0osV0FBSyxTQUFMO0FBQ0lWLFFBQUFBLElBQUksR0FBRywwQ0FBa0NNLE1BQWxDLDBDQUFzRUUsUUFBdEUsZ0JBQW1GRSxJQUFuRix1REFBUDtBQUVBbEIsUUFBQUEsSUFBSSxnQkFBU2tCLElBQVQsd0JBQUo7QUFDQTs7QUFDSixXQUFLLFNBQUw7QUFDSVYsUUFBQUEsSUFBSSxHQUFHLDBDQUFrQ00sTUFBbEMsMENBQXNFRSxRQUF0RSxnQkFBbUZFLElBQW5GLDZEQUFQO0FBRUFsQixRQUFBQSxJQUFJLGdCQUFTa0IsSUFBVCw4QkFBSjtBQUNBOztBQUNKLFdBQUssUUFBTDtBQUNJVixRQUFBQSxJQUFJLEdBQUcsMENBQWtDTSxNQUFsQywwQ0FBc0VFLFFBQXRFLGdCQUFtRkUsSUFBbkYsc0RBQVA7QUFFQWxCLFFBQUFBLElBQUksZ0JBQVNrQixJQUFULHVCQUFKO0FBQ0E7O0FBQ0osV0FBSyxTQUFMO0FBQWdCO0FBQ1pWLFVBQUFBLElBQUksR0FBRywwQ0FBa0NNLE1BQWxDLGlEQUNXRSxRQURYLGdCQUN3QkUsSUFEeEIscUJBQ3VDVixJQUR2Qyw2QkFBUDtBQUVBLGdCQUFNUCxLQUFLLEdBQUdELElBQUksQ0FBQ29CLElBQUwsR0FBWWxCLEtBQVosQ0FBa0IsSUFBbEIsQ0FBZDs7QUFDQSxjQUFJRCxLQUFLLENBQUNFLE1BQU4sR0FBZSxDQUFuQixFQUFzQjtBQUNsQkYsWUFBQUEsS0FBSyxDQUFDLENBQUQsQ0FBTCxnQkFBaUJpQixJQUFqQixlQUEwQmpCLEtBQUssQ0FBQyxDQUFELENBQS9CO0FBQ0FELFlBQUFBLElBQUksR0FBR0MsS0FBSyxDQUFDb0IsR0FBTixDQUFXQyxJQUFELGdCQUFlQSxJQUFmLENBQVYsRUFBaUNoQixJQUFqQyxDQUFzQyxJQUF0QyxJQUE4QyxNQUFyRDtBQUNIOztBQUNEO0FBQ0g7O0FBQ0Q7QUFDSSxlQUFPLElBQVA7QUEzQ1I7O0FBOENBLFdBQU87QUFBQ04sTUFBQUEsSUFBRDtBQUFPUSxNQUFBQTtBQUFQLEtBQVA7QUFDSDs7QUFFRCxTQUFPZSxjQUFQLENBQXNCN0MsRUFBdEIsRUFBMEI7QUFDdEIsUUFBSSxDQUFDQSxFQUFMLEVBQVMsT0FBTyxFQUFQO0FBQ1QsV0FBTztBQUNILHNCQUFnQjtBQUNaLHlCQUFpQjtBQUNiLHNCQUFZQSxFQUFFLENBQUNPLEtBQUg7QUFEQztBQURMO0FBRGIsS0FBUDtBQU9IOztBQUVELFNBQU91QyxVQUFQLENBQWtCQyxRQUFsQixFQUE0QkMsZUFBNUIsRUFBNkNmLGdCQUE3QyxFQUErRGdCLEdBQS9ELEVBQW9FO0FBQ2hFLFFBQUksQ0FBQ3RELFdBQVcsQ0FBQ3FCLGdCQUFaLENBQTZCK0IsUUFBN0IsQ0FBTCxFQUE2QztBQUN6QyxhQUFPLHlDQUFQO0FBQ0g7O0FBQ0QsV0FBTyw2QkFBQyxXQUFEO0FBQWEsTUFBQSxRQUFRLEVBQUVBLFFBQXZCO0FBQWlDLE1BQUEsZUFBZSxFQUFFQyxlQUFsRDtBQUNILE1BQUEsR0FBRyxFQUFFQyxHQURGO0FBQ08sTUFBQSxnQkFBZ0IsRUFBRWhCO0FBRHpCLE1BQVA7QUFFSDs7QUFFRGlCLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFNBQUtoRCxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsU0FBS0QsSUFBTCxHQUFZLEtBQUtrRCxPQUFMLENBQWFDLE9BQWIsQ0FBcUIsS0FBS3JELEtBQUwsQ0FBV2dELFFBQVgsQ0FBb0JNLFNBQXBCLEVBQXJCLENBQVo7QUFDQSxTQUFLcEQsSUFBTCxDQUFVcUQsRUFBVixDQUFhLGdCQUFiLEVBQStCLEtBQUtDLGVBQXBDLEVBSGdCLENBSWhCOztBQUNBLFNBQUt0RCxJQUFMLENBQVVxRCxFQUFWLENBQWEseUJBQWIsRUFBd0MsS0FBS0MsZUFBN0M7QUFDQSxTQUFLQyxVQUFMO0FBQ0g7O0FBRURDLEVBQUFBLGtCQUFrQixHQUFHO0FBQ2pCLFNBQUsxRCxLQUFMLENBQVdpRCxlQUFYO0FBQ0g7O0FBRURVLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFNBQUt4RCxTQUFMLEdBQWlCLElBQWpCOztBQUNBLFFBQUksS0FBS0QsSUFBVCxFQUFlO0FBQ1gsV0FBS0EsSUFBTCxDQUFVMEQsY0FBVixDQUF5QixnQkFBekIsRUFBMkMsS0FBS0osZUFBaEQ7QUFDQSxXQUFLdEQsSUFBTCxDQUFVMEQsY0FBVixDQUF5Qix5QkFBekIsRUFBb0QsS0FBS0osZUFBekQ7QUFDSDtBQUNKOztBQVdELFFBQU1DLFVBQU4sR0FBbUI7QUFDZixVQUFNO0FBQUNULE1BQUFBO0FBQUQsUUFBYSxLQUFLaEQsS0FBeEIsQ0FEZSxDQUVmOztBQUNBLFVBQU1DLEVBQUUsR0FBRyxNQUFNLEtBQUs0RCxRQUFMLENBQWNqRSxXQUFXLENBQUNxQixnQkFBWixDQUE2QitCLFFBQTdCLENBQWQsQ0FBakI7QUFDQSxRQUFJLEtBQUs3QyxTQUFULEVBQW9COztBQUVwQixRQUFJRixFQUFKLEVBQVE7QUFDSixXQUFLNkQsUUFBTCxDQUFjO0FBQ1Z6RCxRQUFBQSxNQUFNLEVBQUUsQ0FBQ0osRUFBRDtBQURFLE9BQWQsRUFFRyxLQUFLOEQsYUFGUjtBQUdILEtBSkQsTUFJTztBQUNILFdBQUtELFFBQUwsQ0FBYztBQUFDbEQsUUFBQUEsR0FBRyxFQUFFO0FBQU4sT0FBZDtBQUNIO0FBQ0o7O0FBRUQsUUFBTW1ELGFBQU4sR0FBc0I7QUFDbEIsUUFBSSxLQUFLNUQsU0FBVCxFQUFvQjtBQUNwQixVQUFNRixFQUFFLEdBQUcsS0FBS0csS0FBTCxDQUFXQyxNQUFYLENBQWtCLENBQWxCLENBQVg7QUFDQSxVQUFNMkQsZ0JBQWdCLEdBQUdwRSxXQUFXLENBQUNxQixnQkFBWixDQUE2QmhCLEVBQTdCLENBQXpCOztBQUVBLFFBQUksQ0FBQytELGdCQUFMLEVBQXVCO0FBQ25CLFdBQUtGLFFBQUwsQ0FBYztBQUNWbkQsUUFBQUEsT0FBTyxFQUFFO0FBREMsT0FBZDtBQUdBO0FBQ0g7O0FBRUQsVUFBTUQsUUFBUSxHQUFHLE1BQU0sS0FBS21ELFFBQUwsQ0FBY0csZ0JBQWQsQ0FBdkI7QUFDQSxRQUFJLEtBQUs3RCxTQUFULEVBQW9COztBQUVwQixRQUFJTyxRQUFKLEVBQWM7QUFDVixXQUFLb0QsUUFBTCxDQUFjO0FBQUNwRCxRQUFBQTtBQUFELE9BQWQ7QUFDSCxLQUZELE1BRU87QUFDSCxXQUFLb0QsUUFBTCxDQUFjO0FBQUNsRCxRQUFBQSxHQUFHLEVBQUU7QUFBTixPQUFkO0FBQ0g7QUFDSjs7QUFFRCxRQUFNaUQsUUFBTixDQUFlSSxPQUFmLEVBQXdCO0FBQ3BCLFVBQU0xRCxLQUFLLEdBQUcsS0FBS0wsSUFBTCxDQUFVZ0UsYUFBVixDQUF3QkQsT0FBeEIsQ0FBZDtBQUNBLFFBQUkxRCxLQUFKLEVBQVcsT0FBT0EsS0FBUDs7QUFFWCxRQUFJO0FBQ0E7QUFDQTtBQUNBLFlBQU0sS0FBSzZDLE9BQUwsQ0FBYWUsZ0JBQWIsQ0FBOEIsS0FBS2pFLElBQUwsQ0FBVWtFLHdCQUFWLEVBQTlCLEVBQW9FSCxPQUFwRSxDQUFOO0FBQ0gsS0FKRCxDQUlFLE9BQU9JLENBQVAsRUFBVTtBQUNSO0FBQ0E7QUFDQSxhQUFPLElBQVA7QUFDSDs7QUFDRCxXQUFPLEtBQUtuRSxJQUFMLENBQVVnRSxhQUFWLENBQXdCRCxPQUF4QixDQUFQO0FBQ0g7O0FBRURsRCxFQUFBQSxXQUFXLEdBQUc7QUFDVixXQUFPLEtBQUtYLEtBQUwsQ0FBV0MsTUFBWCxDQUFrQnFCLE1BQWxCLEdBQTJCLENBQWxDO0FBQ0g7O0FBRURWLEVBQUFBLFFBQVEsR0FBRztBQUNQLFNBQUt5QyxVQUFMO0FBQ0g7O0FBRUQ1QyxFQUFBQSxZQUFZLEdBQUc7QUFDWCxVQUFNUixNQUFNLEdBQUcsQ0FBQyxLQUFLRCxLQUFMLENBQVdNLFFBQVosRUFBc0IsR0FBRyxLQUFLTixLQUFMLENBQVdDLE1BQXBDLENBQWY7QUFFQSxTQUFLeUQsUUFBTCxDQUFjO0FBQ1ZwRCxNQUFBQSxRQUFRLEVBQUUsSUFEQTtBQUVWTCxNQUFBQTtBQUZVLEtBQWQsRUFHRyxLQUFLMEQsYUFIUjs7QUFLQU8sd0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxNQUFBQSxNQUFNLEVBQUU7QUFBVCxLQUFiO0FBQ0g7O0FBRURDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUlDLE1BQU0sR0FBRyxJQUFiOztBQUVBLFFBQUksS0FBS3RFLEtBQUwsQ0FBV1EsR0FBZixFQUFvQjtBQUNoQjhELE1BQUFBLE1BQU0sR0FBRztBQUFZLFFBQUEsU0FBUyxFQUFDO0FBQXRCLFNBRUQseUJBQUcsK0NBQ0Msb0VBREosQ0FGQyxDQUFUO0FBTUgsS0FQRCxNQU9PLElBQUksS0FBS3RFLEtBQUwsQ0FBV00sUUFBZixFQUF5QjtBQUM1QixZQUFNVCxFQUFFLEdBQUcsS0FBS0csS0FBTCxDQUFXTSxRQUF0QjtBQUNBLFlBQU1pRSxJQUFJLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixlQUFqQixDQUFiO0FBQ0EsWUFBTTNFLElBQUksR0FBRyxLQUFLa0QsT0FBTCxDQUFhQyxPQUFiLENBQXFCcEQsRUFBRSxDQUFDcUQsU0FBSCxFQUFyQixDQUFiO0FBQ0FvQixNQUFBQSxNQUFNLEdBQUc7QUFBWSxRQUFBLFNBQVMsRUFBQztBQUF0QixTQUVELHlCQUFHLDJCQUFILEVBQWdDLEVBQWhDLEVBQW9DO0FBQ2hDLGFBQU1JLEdBQUQsSUFBUztBQUFHLFVBQUEsT0FBTyxFQUFFLEtBQUtqRSxZQUFqQjtBQUErQixVQUFBLFNBQVMsRUFBQztBQUF6QyxXQUFpRWlFLEdBQWpFLENBRGtCO0FBRWhDLGdCQUFRLDZCQUFDLElBQUQ7QUFBTSxVQUFBLElBQUksRUFBRUgsSUFBSSxDQUFDSSxpQkFBakI7QUFBb0MsVUFBQSxJQUFJLEVBQUU3RSxJQUExQztBQUNNLFVBQUEsR0FBRyxFQUFFLG1DQUFrQkQsRUFBRSxDQUFDdUMsU0FBSCxFQUFsQixDQURYO0FBQzhDLFVBQUEsb0JBQW9CLEVBQUU7QUFEcEU7QUFGd0IsT0FBcEMsQ0FGQyxDQUFUO0FBU0gsS0FiTSxNQWFBLElBQUksS0FBS3BDLEtBQUwsQ0FBV08sT0FBZixFQUF3QjtBQUMzQixZQUFNcUUsT0FBTyxHQUFHSixHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBQ0FILE1BQUFBLE1BQU0sR0FBRyw2QkFBQyxPQUFEO0FBQVMsUUFBQSxDQUFDLEVBQUUsRUFBWjtBQUFnQixRQUFBLENBQUMsRUFBRTtBQUFuQixRQUFUO0FBQ0g7O0FBRUQsVUFBTU8sU0FBUyxHQUFHTCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUJBQWpCLENBQWxCO0FBQ0EsVUFBTUssYUFBYSxHQUFHTixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBQ0EsVUFBTU0sT0FBTyxHQUFHLEtBQUsvRSxLQUFMLENBQVdDLE1BQVgsQ0FBa0J1QyxHQUFsQixDQUF1QjNDLEVBQUQsSUFBUTtBQUMxQyxVQUFJbUYsT0FBTyxHQUFHLElBQWQ7O0FBRUEsVUFBSSxtQ0FBbUIsS0FBS3BGLEtBQUwsQ0FBV2dELFFBQVgsQ0FBb0JxQyxPQUFwQixFQUFuQixFQUFrRHBGLEVBQUUsQ0FBQ29GLE9BQUgsRUFBbEQsQ0FBSixFQUFxRTtBQUNqRUQsUUFBQUEsT0FBTyxHQUFHO0FBQUcsVUFBQSxJQUFJLEVBQUUsS0FBS3BGLEtBQUwsQ0FBV3NGO0FBQXBCLFdBQXlCLDZCQUFDLGFBQUQ7QUFBZSxVQUFBLEVBQUUsRUFBRXJGLEVBQUUsQ0FBQ3NGLEtBQUg7QUFBbkIsVUFBekIsQ0FBVjtBQUNIOztBQUVELGFBQU87QUFBWSxRQUFBLFNBQVMsRUFBQyxnQkFBdEI7QUFBdUMsUUFBQSxHQUFHLEVBQUV0RixFQUFFLENBQUNPLEtBQUg7QUFBNUMsU0FDRDRFLE9BREMsRUFFSCw2QkFBQyxTQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUVuRixFQURiO0FBRUksUUFBQSxTQUFTLEVBQUMsT0FGZDtBQUdJLFFBQUEsZUFBZSxFQUFFLEtBQUtELEtBQUwsQ0FBV2lELGVBSGhDO0FBSUksUUFBQSxnQkFBZ0IsRUFBRSxLQUFLakQsS0FBTCxDQUFXa0MsZ0JBSmpDO0FBS0ksUUFBQSxVQUFVLEVBQUVqQyxFQUFFLENBQUNpQixVQUFILEVBTGhCO0FBTUksUUFBQSxZQUFZLEVBQUVzRSx1QkFBY0MsUUFBZCxDQUF1QiwwQkFBdkI7QUFObEIsUUFGRyxDQUFQO0FBVUgsS0FqQmUsQ0FBaEI7QUFtQkEsV0FBTywwQ0FDSCwwQ0FBT2YsTUFBUCxDQURHLEVBRUgsMENBQU9TLE9BQVAsQ0FGRyxDQUFQO0FBSUg7O0FBclRvRDs7OzhCQUFwQ3ZGLFcsZUFDRTtBQUNmO0FBQ0FvRCxFQUFBQSxRQUFRLEVBQUUwQyxtQkFBVUMsVUFBVixDQUFxQkMsd0JBQXJCLENBRks7QUFHZjtBQUNBM0MsRUFBQUEsZUFBZSxFQUFFeUMsbUJBQVVHLElBQVYsQ0FBZUMsVUFKakI7QUFLZjVELEVBQUFBLGdCQUFnQixFQUFFd0QsbUJBQVVDLFVBQVYsQ0FBcUJJLGdDQUFyQixFQUEyQ0Q7QUFMOUMsQzs4QkFERmxHLFcsaUJBU0lvRyw0QiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHtfdH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IHt3YW50c0RhdGVTZXBhcmF0b3J9IGZyb20gJy4uLy4uLy4uL0RhdGVVdGlscyc7XHJcbmltcG9ydCB7TWF0cml4RXZlbnR9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQge21ha2VVc2VyUGVybWFsaW5rLCBSb29tUGVybWFsaW5rQ3JlYXRvcn0gZnJvbSBcIi4uLy4uLy4uL3V0aWxzL3Blcm1hbGlua3MvUGVybWFsaW5rc1wiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgZXNjYXBlSHRtbCBmcm9tIFwiZXNjYXBlLWh0bWxcIjtcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuXHJcbi8vIFRoaXMgY29tcG9uZW50IGRvZXMgbm8gY3ljbGUgZGV0ZWN0aW9uLCBzaW1wbHkgYmVjYXVzZSB0aGUgb25seSB3YXkgdG8gbWFrZSBzdWNoIGEgY3ljbGUgd291bGQgYmUgdG9cclxuLy8gY3JhZnQgZXZlbnRfaWQncywgdXNpbmcgYSBob21lc2VydmVyIHRoYXQgZ2VuZXJhdGVzIHByZWRpY3RhYmxlIGV2ZW50IElEczsgZXZlbiB0aGVuIHRoZSBpbXBhY3Qgd291bGRcclxuLy8gYmUgbG93IGFzIGVhY2ggZXZlbnQgYmVpbmcgbG9hZGVkIChhZnRlciB0aGUgZmlyc3QpIGlzIHRyaWdnZXJlZCBieSBhbiBleHBsaWNpdCB1c2VyIGFjdGlvbi5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmVwbHlUaHJlYWQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyB0aGUgbGF0ZXN0IGV2ZW50IGluIHRoaXMgY2hhaW4gb2YgcmVwbGllc1xyXG4gICAgICAgIHBhcmVudEV2OiBQcm9wVHlwZXMuaW5zdGFuY2VPZihNYXRyaXhFdmVudCksXHJcbiAgICAgICAgLy8gY2FsbGVkIHdoZW4gdGhlIFJlcGx5VGhyZWFkIGNvbnRlbnRzIGhhcyBjaGFuZ2VkLCBpbmNsdWRpbmcgRXZlbnRUaWxlcyB0aGVyZW9mXHJcbiAgICAgICAgb25IZWlnaHRDaGFuZ2VkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHBlcm1hbGlua0NyZWF0b3I6IFByb3BUeXBlcy5pbnN0YW5jZU9mKFJvb21QZXJtYWxpbmtDcmVhdG9yKS5pc1JlcXVpcmVkLFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgY29udGV4dFR5cGUgPSBNYXRyaXhDbGllbnRDb250ZXh0O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICAvLyBUaGUgbG9hZGVkIGV2ZW50cyB0byBiZSByZW5kZXJlZCBhcyBsaW5lYXItcmVwbGllc1xyXG4gICAgICAgICAgICBldmVudHM6IFtdLFxyXG5cclxuICAgICAgICAgICAgLy8gVGhlIGxhdGVzdCBsb2FkZWQgZXZlbnQgd2hpY2ggaGFzIG5vdCB5ZXQgYmVlbiBzaG93blxyXG4gICAgICAgICAgICBsb2FkZWRFdjogbnVsbCxcclxuICAgICAgICAgICAgLy8gV2hldGhlciB0aGUgY29tcG9uZW50IGlzIHN0aWxsIGxvYWRpbmcgbW9yZSBldmVudHNcclxuICAgICAgICAgICAgbG9hZGluZzogdHJ1ZSxcclxuXHJcbiAgICAgICAgICAgIC8vIFdoZXRoZXIgYXMgZXJyb3Igd2FzIGVuY291bnRlcmVkIGZldGNoaW5nIGEgcmVwbGllZCB0byBldmVudC5cclxuICAgICAgICAgICAgZXJyOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLm9uUXVvdGVDbGljayA9IHRoaXMub25RdW90ZUNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5jYW5Db2xsYXBzZSA9IHRoaXMuY2FuQ29sbGFwc2UuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLmNvbGxhcHNlID0gdGhpcy5jb2xsYXBzZS5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXRQYXJlbnRFdmVudElkKGV2KSB7XHJcbiAgICAgICAgaWYgKCFldiB8fCBldi5pc1JlZGFjdGVkKCkpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gWFhYOiBGb3IgbmV3ZXIgcmVsYXRpb25zIChhbm5vdGF0aW9ucywgcmVwbGFjZW1lbnRzLCBldGMuKSwgd2Ugbm93XHJcbiAgICAgICAgLy8gaGF2ZSBhIGBnZXRSZWxhdGlvbmAgaGVscGVyIG9uIHRoZSBldmVudCwgYW5kIHlvdSBtaWdodCBhc3N1bWUgaXRcclxuICAgICAgICAvLyBjb3VsZCBiZSB1c2VkIGhlcmUgZm9yIHJlcGxpZXMgYXMgd2VsbC4uLiBIb3dldmVyLCB0aGUgaGVscGVyXHJcbiAgICAgICAgLy8gY3VycmVudGx5IGFzc3VtZXMgdGhlIHJlbGF0aW9uIGhhcyBhIGByZWxfdHlwZWAsIHdoaWNoIG9sZGVyIHJlcGxpZXNcclxuICAgICAgICAvLyBkbyBub3QsIHNvIHRoaXMgYmxvY2sgaXMgbGVmdCBhcy1pcyBmb3Igbm93LlxyXG4gICAgICAgIGNvbnN0IG1SZWxhdGVzVG8gPSBldi5nZXRXaXJlQ29udGVudCgpWydtLnJlbGF0ZXNfdG8nXTtcclxuICAgICAgICBpZiAobVJlbGF0ZXNUbyAmJiBtUmVsYXRlc1RvWydtLmluX3JlcGx5X3RvJ10pIHtcclxuICAgICAgICAgICAgY29uc3QgbUluUmVwbHlUbyA9IG1SZWxhdGVzVG9bJ20uaW5fcmVwbHlfdG8nXTtcclxuICAgICAgICAgICAgaWYgKG1JblJlcGx5VG8gJiYgbUluUmVwbHlUb1snZXZlbnRfaWQnXSkgcmV0dXJuIG1JblJlcGx5VG9bJ2V2ZW50X2lkJ107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFBhcnQgb2YgUmVwbGllcyBmYWxsYmFjayBzdXBwb3J0XHJcbiAgICBzdGF0aWMgc3RyaXBQbGFpblJlcGx5KGJvZHkpIHtcclxuICAgICAgICAvLyBSZW1vdmVzIGxpbmVzIGJlZ2lubmluZyB3aXRoIGA+IGAgdW50aWwgeW91IHJlYWNoIG9uZSB0aGF0IGRvZXNuJ3QuXHJcbiAgICAgICAgY29uc3QgbGluZXMgPSBib2R5LnNwbGl0KCdcXG4nKTtcclxuICAgICAgICB3aGlsZSAobGluZXMubGVuZ3RoICYmIGxpbmVzWzBdLnN0YXJ0c1dpdGgoJz4gJykpIGxpbmVzLnNoaWZ0KCk7XHJcbiAgICAgICAgLy8gUmVwbHkgZmFsbGJhY2sgaGFzIGEgYmxhbmsgbGluZSBhZnRlciBpdCwgc28gcmVtb3ZlIGl0IHRvIHByZXZlbnQgbGVhZGluZyBuZXdsaW5lXHJcbiAgICAgICAgaWYgKGxpbmVzWzBdID09PSAnJykgbGluZXMuc2hpZnQoKTtcclxuICAgICAgICByZXR1cm4gbGluZXMuam9pbignXFxuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUGFydCBvZiBSZXBsaWVzIGZhbGxiYWNrIHN1cHBvcnRcclxuICAgIHN0YXRpYyBzdHJpcEhUTUxSZXBseShodG1sKSB7XHJcbiAgICAgICAgcmV0dXJuIGh0bWwucmVwbGFjZSgvXjxteC1yZXBseT5bXFxzXFxTXSs/PFxcL214LXJlcGx5Pi8sICcnKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQYXJ0IG9mIFJlcGxpZXMgZmFsbGJhY2sgc3VwcG9ydFxyXG4gICAgc3RhdGljIGdldE5lc3RlZFJlcGx5VGV4dChldiwgcGVybWFsaW5rQ3JlYXRvcikge1xyXG4gICAgICAgIGlmICghZXYpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBsZXQge2JvZHksIGZvcm1hdHRlZF9ib2R5OiBodG1sfSA9IGV2LmdldENvbnRlbnQoKTtcclxuICAgICAgICBpZiAodGhpcy5nZXRQYXJlbnRFdmVudElkKGV2KSkge1xyXG4gICAgICAgICAgICBpZiAoYm9keSkgYm9keSA9IHRoaXMuc3RyaXBQbGFpblJlcGx5KGJvZHkpO1xyXG4gICAgICAgICAgICBpZiAoaHRtbCkgaHRtbCA9IHRoaXMuc3RyaXBIVE1MUmVwbHkoaHRtbCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWJvZHkpIGJvZHkgPSBcIlwiOyAvLyBBbHdheXMgZW5zdXJlIHdlIGhhdmUgYSBib2R5LCBmb3IgcmVhc29ucy5cclxuXHJcbiAgICAgICAgLy8gRXNjYXBlIHRoZSBib2R5IHRvIHVzZSBhcyBIVE1MIGJlbG93LlxyXG4gICAgICAgIC8vIFdlIGFsc28gcnVuIGEgbmwyYnIgb3ZlciB0aGUgcmVzdWx0IHRvIGZpeCB0aGUgZmFsbGJhY2sgcmVwcmVzZW50YXRpb24uIFdlIGRvIHRoaXNcclxuICAgICAgICAvLyBhZnRlciBjb252ZXJ0aW5nIHRoZSB0ZXh0IHRvIHNhZmUgSFRNTCB0byBhdm9pZCB1c2VyLXByb3ZpZGVkIEJSJ3MgZnJvbSBiZWluZyBjb252ZXJ0ZWQuXHJcbiAgICAgICAgaWYgKCFodG1sKSBodG1sID0gZXNjYXBlSHRtbChib2R5KS5yZXBsYWNlKC9cXG4vZywgJzxici8+Jyk7XHJcblxyXG4gICAgICAgIC8vIGRldiBub3RlOiBkbyBub3QgcmVseSBvbiBgYm9keWAgYmVpbmcgc2FmZSBmb3IgSFRNTCB1c2FnZSBiZWxvdy5cclxuXHJcbiAgICAgICAgY29uc3QgZXZMaW5rID0gcGVybWFsaW5rQ3JlYXRvci5mb3JFdmVudChldi5nZXRJZCgpKTtcclxuICAgICAgICBjb25zdCB1c2VyTGluayA9IG1ha2VVc2VyUGVybWFsaW5rKGV2LmdldFNlbmRlcigpKTtcclxuICAgICAgICBjb25zdCBteGlkID0gZXYuZ2V0U2VuZGVyKCk7XHJcblxyXG4gICAgICAgIC8vIFRoaXMgZmFsbGJhY2sgY29udGFpbnMgdGV4dCB0aGF0IGlzIGV4cGxpY2l0bHkgRU4uXHJcbiAgICAgICAgc3dpdGNoIChldi5nZXRDb250ZW50KCkubXNndHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlICdtLnRleHQnOlxyXG4gICAgICAgICAgICBjYXNlICdtLm5vdGljZSc6IHtcclxuICAgICAgICAgICAgICAgIGh0bWwgPSBgPG14LXJlcGx5PjxibG9ja3F1b3RlPjxhIGhyZWY9XCIke2V2TGlua31cIj5JbiByZXBseSB0bzwvYT4gPGEgaHJlZj1cIiR7dXNlckxpbmt9XCI+JHtteGlkfTwvYT5gXHJcbiAgICAgICAgICAgICAgICAgICAgKyBgPGJyPiR7aHRtbH08L2Jsb2NrcXVvdGU+PC9teC1yZXBseT5gO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbGluZXMgPSBib2R5LnRyaW0oKS5zcGxpdCgnXFxuJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobGluZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpbmVzWzBdID0gYDwke214aWR9PiAke2xpbmVzWzBdfWA7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9keSA9IGxpbmVzLm1hcCgobGluZSkgPT4gYD4gJHtsaW5lfWApLmpvaW4oJ1xcbicpICsgJ1xcblxcbic7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlICdtLmltYWdlJzpcclxuICAgICAgICAgICAgICAgIGh0bWwgPSBgPG14LXJlcGx5PjxibG9ja3F1b3RlPjxhIGhyZWY9XCIke2V2TGlua31cIj5JbiByZXBseSB0bzwvYT4gPGEgaHJlZj1cIiR7dXNlckxpbmt9XCI+JHtteGlkfTwvYT5gXHJcbiAgICAgICAgICAgICAgICAgICAgKyBgPGJyPnNlbnQgYW4gaW1hZ2UuPC9ibG9ja3F1b3RlPjwvbXgtcmVwbHk+YDtcclxuICAgICAgICAgICAgICAgIGJvZHkgPSBgPiA8JHtteGlkfT4gc2VudCBhbiBpbWFnZS5cXG5cXG5gO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ20udmlkZW8nOlxyXG4gICAgICAgICAgICAgICAgaHRtbCA9IGA8bXgtcmVwbHk+PGJsb2NrcXVvdGU+PGEgaHJlZj1cIiR7ZXZMaW5rfVwiPkluIHJlcGx5IHRvPC9hPiA8YSBocmVmPVwiJHt1c2VyTGlua31cIj4ke214aWR9PC9hPmBcclxuICAgICAgICAgICAgICAgICAgICArIGA8YnI+c2VudCBhIHZpZGVvLjwvYmxvY2txdW90ZT48L214LXJlcGx5PmA7XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gYD4gPCR7bXhpZH0+IHNlbnQgYSB2aWRlby5cXG5cXG5gO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ20uYXVkaW8nOlxyXG4gICAgICAgICAgICAgICAgaHRtbCA9IGA8bXgtcmVwbHk+PGJsb2NrcXVvdGU+PGEgaHJlZj1cIiR7ZXZMaW5rfVwiPkluIHJlcGx5IHRvPC9hPiA8YSBocmVmPVwiJHt1c2VyTGlua31cIj4ke214aWR9PC9hPmBcclxuICAgICAgICAgICAgICAgICAgICArIGA8YnI+c2VudCBhbiBhdWRpbyBmaWxlLjwvYmxvY2txdW90ZT48L214LXJlcGx5PmA7XHJcbiAgICAgICAgICAgICAgICBib2R5ID0gYD4gPCR7bXhpZH0+IHNlbnQgYW4gYXVkaW8gZmlsZS5cXG5cXG5gO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ20uZmlsZSc6XHJcbiAgICAgICAgICAgICAgICBodG1sID0gYDxteC1yZXBseT48YmxvY2txdW90ZT48YSBocmVmPVwiJHtldkxpbmt9XCI+SW4gcmVwbHkgdG88L2E+IDxhIGhyZWY9XCIke3VzZXJMaW5rfVwiPiR7bXhpZH08L2E+YFxyXG4gICAgICAgICAgICAgICAgICAgICsgYDxicj5zZW50IGEgZmlsZS48L2Jsb2NrcXVvdGU+PC9teC1yZXBseT5gO1xyXG4gICAgICAgICAgICAgICAgYm9keSA9IGA+IDwke214aWR9PiBzZW50IGEgZmlsZS5cXG5cXG5gO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ20uZW1vdGUnOiB7XHJcbiAgICAgICAgICAgICAgICBodG1sID0gYDxteC1yZXBseT48YmxvY2txdW90ZT48YSBocmVmPVwiJHtldkxpbmt9XCI+SW4gcmVwbHkgdG88L2E+ICogYFxyXG4gICAgICAgICAgICAgICAgICAgICsgYDxhIGhyZWY9XCIke3VzZXJMaW5rfVwiPiR7bXhpZH08L2E+PGJyPiR7aHRtbH08L2Jsb2NrcXVvdGU+PC9teC1yZXBseT5gO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbGluZXMgPSBib2R5LnRyaW0oKS5zcGxpdCgnXFxuJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobGluZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpbmVzWzBdID0gYCogPCR7bXhpZH0+ICR7bGluZXNbMF19YDtcclxuICAgICAgICAgICAgICAgICAgICBib2R5ID0gbGluZXMubWFwKChsaW5lKSA9PiBgPiAke2xpbmV9YCkuam9pbignXFxuJykgKyAnXFxuXFxuJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB7Ym9keSwgaHRtbH07XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIG1ha2VSZXBseU1peEluKGV2KSB7XHJcbiAgICAgICAgaWYgKCFldikgcmV0dXJuIHt9O1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICdtLnJlbGF0ZXNfdG8nOiB7XHJcbiAgICAgICAgICAgICAgICAnbS5pbl9yZXBseV90byc6IHtcclxuICAgICAgICAgICAgICAgICAgICAnZXZlbnRfaWQnOiBldi5nZXRJZCgpLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBtYWtlVGhyZWFkKHBhcmVudEV2LCBvbkhlaWdodENoYW5nZWQsIHBlcm1hbGlua0NyZWF0b3IsIHJlZikge1xyXG4gICAgICAgIGlmICghUmVwbHlUaHJlYWQuZ2V0UGFyZW50RXZlbnRJZChwYXJlbnRFdikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYgLz47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8UmVwbHlUaHJlYWQgcGFyZW50RXY9e3BhcmVudEV2fSBvbkhlaWdodENoYW5nZWQ9e29uSGVpZ2h0Q2hhbmdlZH1cclxuICAgICAgICAgICAgcmVmPXtyZWZ9IHBlcm1hbGlua0NyZWF0b3I9e3Blcm1hbGlua0NyZWF0b3J9IC8+O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIHRoaXMudW5tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5yb29tID0gdGhpcy5jb250ZXh0LmdldFJvb20odGhpcy5wcm9wcy5wYXJlbnRFdi5nZXRSb29tSWQoKSk7XHJcbiAgICAgICAgdGhpcy5yb29tLm9uKFwiUm9vbS5yZWRhY3Rpb25cIiwgdGhpcy5vblJvb21SZWRhY3Rpb24pO1xyXG4gICAgICAgIC8vIHNhbWUgZXZlbnQgaGFuZGxlciBhcyBSb29tLnJlZGFjdGlvbiBhcyBmb3IgYm90aCB3ZSBqdXN0IGRvIGZvcmNlVXBkYXRlXHJcbiAgICAgICAgdGhpcy5yb29tLm9uKFwiUm9vbS5yZWRhY3Rpb25DYW5jZWxsZWRcIiwgdGhpcy5vblJvb21SZWRhY3Rpb24pO1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uSGVpZ2h0Q2hhbmdlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIHRoaXMudW5tb3VudGVkID0gdHJ1ZTtcclxuICAgICAgICBpZiAodGhpcy5yb29tKSB7XHJcbiAgICAgICAgICAgIHRoaXMucm9vbS5yZW1vdmVMaXN0ZW5lcihcIlJvb20ucmVkYWN0aW9uXCIsIHRoaXMub25Sb29tUmVkYWN0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5yb29tLnJlbW92ZUxpc3RlbmVyKFwiUm9vbS5yZWRhY3Rpb25DYW5jZWxsZWRcIiwgdGhpcy5vblJvb21SZWRhY3Rpb24pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblJvb21SZWRhY3Rpb24gPSAoZXYsIHJvb20pID0+IHtcclxuICAgICAgICBpZiAodGhpcy51bm1vdW50ZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gSWYgb25lIG9mIHRoZSBldmVudHMgd2UgYXJlIHJlbmRlcmluZyBnZXRzIHJlZGFjdGVkLCBmb3JjZSBhIHJlLXJlbmRlclxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmV2ZW50cy5zb21lKGV2ZW50ID0+IGV2ZW50LmdldElkKCkgPT09IGV2LmdldElkKCkpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGFzeW5jIGluaXRpYWxpemUoKSB7XHJcbiAgICAgICAgY29uc3Qge3BhcmVudEV2fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgLy8gYXQgdGltZSBvZiBtYWtpbmcgdGhpcyBjb21wb25lbnQgd2UgY2hlY2tlZCB0aGF0IHByb3BzLnBhcmVudEV2IGhhcyBhIHBhcmVudEV2ZW50SWRcclxuICAgICAgICBjb25zdCBldiA9IGF3YWl0IHRoaXMuZ2V0RXZlbnQoUmVwbHlUaHJlYWQuZ2V0UGFyZW50RXZlbnRJZChwYXJlbnRFdikpO1xyXG4gICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoZXYpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBldmVudHM6IFtldl0sXHJcbiAgICAgICAgICAgIH0sIHRoaXMubG9hZE5leHRFdmVudCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZXJyOiB0cnVlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGxvYWROZXh0RXZlbnQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgY29uc3QgZXYgPSB0aGlzLnN0YXRlLmV2ZW50c1swXTtcclxuICAgICAgICBjb25zdCBpblJlcGx5VG9FdmVudElkID0gUmVwbHlUaHJlYWQuZ2V0UGFyZW50RXZlbnRJZChldik7XHJcblxyXG4gICAgICAgIGlmICghaW5SZXBseVRvRXZlbnRJZCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbG9hZGVkRXYgPSBhd2FpdCB0aGlzLmdldEV2ZW50KGluUmVwbHlUb0V2ZW50SWQpO1xyXG4gICAgICAgIGlmICh0aGlzLnVubW91bnRlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAobG9hZGVkRXYpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bG9hZGVkRXZ9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtlcnI6IHRydWV9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0RXZlbnQoZXZlbnRJZCkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50ID0gdGhpcy5yb29tLmZpbmRFdmVudEJ5SWQoZXZlbnRJZCk7XHJcbiAgICAgICAgaWYgKGV2ZW50KSByZXR1cm4gZXZlbnQ7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIC8vIGFzayB0aGUgY2xpZW50IHRvIGZldGNoIHRoZSBldmVudCB3ZSB3YW50IHVzaW5nIHRoZSBjb250ZXh0IEFQSSwgb25seSBpbnRlcmZhY2UgdG8gZG8gc28gaXMgdG8gYXNrXHJcbiAgICAgICAgICAgIC8vIGZvciBhIHRpbWVsaW5lIHdpdGggdGhhdCBldmVudCwgYnV0IG9uY2UgaXQgaXMgbG9hZGVkIHdlIGNhbiB1c2UgZmluZEV2ZW50QnlJZCB0byBsb29rIHVwIHRoZSBldiBtYXBcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5jb250ZXh0LmdldEV2ZW50VGltZWxpbmUodGhpcy5yb29tLmdldFVuZmlsdGVyZWRUaW1lbGluZVNldCgpLCBldmVudElkKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIC8vIGlmIGl0IGZhaWxzIGNhdGNoIHRoZSBlcnJvciBhbmQgcmV0dXJuIGVhcmx5LCB0aGVyZSdzIG5vIHBvaW50IHRyeWluZyB0byBmaW5kIHRoZSBldmVudCBpbiB0aGlzIGNhc2UuXHJcbiAgICAgICAgICAgIC8vIFJldHVybiBudWxsIGFzIGl0IGlzIGZhbHNleSBhbmQgdGh1cyBzaG91bGQgYmUgdHJlYXRlZCBhcyBhbiBlcnJvciAoYXMgdGhlIGV2ZW50IGNhbm5vdCBiZSByZXNvbHZlZCkuXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5yb29tLmZpbmRFdmVudEJ5SWQoZXZlbnRJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQ29sbGFwc2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuZXZlbnRzLmxlbmd0aCA+IDE7XHJcbiAgICB9XHJcblxyXG4gICAgY29sbGFwc2UoKSB7XHJcbiAgICAgICAgdGhpcy5pbml0aWFsaXplKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25RdW90ZUNsaWNrKCkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50cyA9IFt0aGlzLnN0YXRlLmxvYWRlZEV2LCAuLi50aGlzLnN0YXRlLmV2ZW50c107XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBsb2FkZWRFdjogbnVsbCxcclxuICAgICAgICAgICAgZXZlbnRzLFxyXG4gICAgICAgIH0sIHRoaXMubG9hZE5leHRFdmVudCk7XHJcblxyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnZm9jdXNfY29tcG9zZXInfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBoZWFkZXIgPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnIpIHtcclxuICAgICAgICAgICAgaGVhZGVyID0gPGJsb2NrcXVvdGUgY2xhc3NOYW1lPVwibXhfUmVwbHlUaHJlYWQgbXhfUmVwbHlUaHJlYWRfZXJyb3JcIj5cclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBfdCgnVW5hYmxlIHRvIGxvYWQgZXZlbnQgdGhhdCB3YXMgcmVwbGllZCB0bywgJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdpdCBlaXRoZXIgZG9lcyBub3QgZXhpc3Qgb3IgeW91IGRvIG5vdCBoYXZlIHBlcm1pc3Npb24gdG8gdmlldyBpdC4nKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L2Jsb2NrcXVvdGU+O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5sb2FkZWRFdikge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IHRoaXMuc3RhdGUubG9hZGVkRXY7XHJcbiAgICAgICAgICAgIGNvbnN0IFBpbGwgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5QaWxsJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb20gPSB0aGlzLmNvbnRleHQuZ2V0Um9vbShldi5nZXRSb29tSWQoKSk7XHJcbiAgICAgICAgICAgIGhlYWRlciA9IDxibG9ja3F1b3RlIGNsYXNzTmFtZT1cIm14X1JlcGx5VGhyZWFkXCI+XHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3QoJzxhPkluIHJlcGx5IHRvPC9hPiA8cGlsbD4nLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnYSc6IChzdWIpID0+IDxhIG9uQ2xpY2s9e3RoaXMub25RdW90ZUNsaWNrfSBjbGFzc05hbWU9XCJteF9SZXBseVRocmVhZF9zaG93XCI+eyBzdWIgfTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdwaWxsJzogPFBpbGwgdHlwZT17UGlsbC5UWVBFX1VTRVJfTUVOVElPTn0gcm9vbT17cm9vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw9e21ha2VVc2VyUGVybWFsaW5rKGV2LmdldFNlbmRlcigpKX0gc2hvdWxkU2hvd1BpbGxBdmF0YXI9e3RydWV9IC8+LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDwvYmxvY2txdW90ZT47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmxvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICBoZWFkZXIgPSA8U3Bpbm5lciB3PXsxNn0gaD17MTZ9IC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgRXZlbnRUaWxlID0gc2RrLmdldENvbXBvbmVudCgndmlld3Mucm9vbXMuRXZlbnRUaWxlJyk7XHJcbiAgICAgICAgY29uc3QgRGF0ZVNlcGFyYXRvciA9IHNkay5nZXRDb21wb25lbnQoJ21lc3NhZ2VzLkRhdGVTZXBhcmF0b3InKTtcclxuICAgICAgICBjb25zdCBldlRpbGVzID0gdGhpcy5zdGF0ZS5ldmVudHMubWFwKChldikgPT4ge1xyXG4gICAgICAgICAgICBsZXQgZGF0ZVNlcCA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICBpZiAod2FudHNEYXRlU2VwYXJhdG9yKHRoaXMucHJvcHMucGFyZW50RXYuZ2V0RGF0ZSgpLCBldi5nZXREYXRlKCkpKSB7XHJcbiAgICAgICAgICAgICAgICBkYXRlU2VwID0gPGEgaHJlZj17dGhpcy5wcm9wcy51cmx9PjxEYXRlU2VwYXJhdG9yIHRzPXtldi5nZXRUcygpfSAvPjwvYT47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiA8YmxvY2txdW90ZSBjbGFzc05hbWU9XCJteF9SZXBseVRocmVhZFwiIGtleT17ZXYuZ2V0SWQoKX0+XHJcbiAgICAgICAgICAgICAgICB7IGRhdGVTZXAgfVxyXG4gICAgICAgICAgICAgICAgPEV2ZW50VGlsZVxyXG4gICAgICAgICAgICAgICAgICAgIG14RXZlbnQ9e2V2fVxyXG4gICAgICAgICAgICAgICAgICAgIHRpbGVTaGFwZT1cInJlcGx5XCJcclxuICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9e3RoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkfVxyXG4gICAgICAgICAgICAgICAgICAgIHBlcm1hbGlua0NyZWF0b3I9e3RoaXMucHJvcHMucGVybWFsaW5rQ3JlYXRvcn1cclxuICAgICAgICAgICAgICAgICAgICBpc1JlZGFjdGVkPXtldi5pc1JlZGFjdGVkKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgaXNUd2VsdmVIb3VyPXtTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwic2hvd1R3ZWx2ZUhvdXJUaW1lc3RhbXBzXCIpfSAvPlxyXG4gICAgICAgICAgICA8L2Jsb2NrcXVvdGU+O1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPGRpdj57IGhlYWRlciB9PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXY+eyBldlRpbGVzIH08L2Rpdj5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuIl19