"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2017 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const RoomDirectoryButton = function (props) {
  const ActionButton = sdk.getComponent('elements.ActionButton');
  return _react.default.createElement(ActionButton, {
    action: "view_room_directory",
    mouseOverAction: props.callout ? "callout_room_directory" : null,
    label: (0, _languageHandler._t)("Room directory"),
    iconPath: require("../../../../res/img/icons-directory.svg"),
    size: props.size,
    tooltip: props.tooltip
  });
};

RoomDirectoryButton.propTypes = {
  size: _propTypes.default.string,
  tooltip: _propTypes.default.bool
};
var _default = RoomDirectoryButton;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1Jvb21EaXJlY3RvcnlCdXR0b24uanMiXSwibmFtZXMiOlsiUm9vbURpcmVjdG9yeUJ1dHRvbiIsInByb3BzIiwiQWN0aW9uQnV0dG9uIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiY2FsbG91dCIsInJlcXVpcmUiLCJzaXplIiwidG9vbHRpcCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQW5CQTs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBLE1BQU1BLG1CQUFtQixHQUFHLFVBQVNDLEtBQVQsRUFBZ0I7QUFDeEMsUUFBTUMsWUFBWSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUJBQWpCLENBQXJCO0FBQ0EsU0FDSSw2QkFBQyxZQUFEO0FBQWMsSUFBQSxNQUFNLEVBQUMscUJBQXJCO0FBQ0ksSUFBQSxlQUFlLEVBQUVILEtBQUssQ0FBQ0ksT0FBTixHQUFnQix3QkFBaEIsR0FBMkMsSUFEaEU7QUFFSSxJQUFBLEtBQUssRUFBRSx5QkFBRyxnQkFBSCxDQUZYO0FBR0ksSUFBQSxRQUFRLEVBQUVDLE9BQU8sQ0FBQyx5Q0FBRCxDQUhyQjtBQUlJLElBQUEsSUFBSSxFQUFFTCxLQUFLLENBQUNNLElBSmhCO0FBS0ksSUFBQSxPQUFPLEVBQUVOLEtBQUssQ0FBQ087QUFMbkIsSUFESjtBQVNILENBWEQ7O0FBYUFSLG1CQUFtQixDQUFDUyxTQUFwQixHQUFnQztBQUM1QkYsRUFBQUEsSUFBSSxFQUFFRyxtQkFBVUMsTUFEWTtBQUU1QkgsRUFBQUEsT0FBTyxFQUFFRSxtQkFBVUU7QUFGUyxDQUFoQztlQUtlWixtQiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5cclxuY29uc3QgUm9vbURpcmVjdG9yeUJ1dHRvbiA9IGZ1bmN0aW9uKHByb3BzKSB7XHJcbiAgICBjb25zdCBBY3Rpb25CdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY3Rpb25CdXR0b24nKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEFjdGlvbkJ1dHRvbiBhY3Rpb249XCJ2aWV3X3Jvb21fZGlyZWN0b3J5XCJcclxuICAgICAgICAgICAgbW91c2VPdmVyQWN0aW9uPXtwcm9wcy5jYWxsb3V0ID8gXCJjYWxsb3V0X3Jvb21fZGlyZWN0b3J5XCIgOiBudWxsfVxyXG4gICAgICAgICAgICBsYWJlbD17X3QoXCJSb29tIGRpcmVjdG9yeVwiKX1cclxuICAgICAgICAgICAgaWNvblBhdGg9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ljb25zLWRpcmVjdG9yeS5zdmdcIil9XHJcbiAgICAgICAgICAgIHNpemU9e3Byb3BzLnNpemV9XHJcbiAgICAgICAgICAgIHRvb2x0aXA9e3Byb3BzLnRvb2x0aXB9XHJcbiAgICAgICAgLz5cclxuICAgICk7XHJcbn07XHJcblxyXG5Sb29tRGlyZWN0b3J5QnV0dG9uLnByb3BUeXBlcyA9IHtcclxuICAgIHNpemU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICB0b29sdGlwOiBQcm9wVHlwZXMuYm9vbCxcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFJvb21EaXJlY3RvcnlCdXR0b247XHJcbiJdfQ==