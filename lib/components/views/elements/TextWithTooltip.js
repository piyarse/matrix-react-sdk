"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

/*
 Copyright 2019 New Vector Ltd.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
class TextWithTooltip extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onMouseOver", () => {
      this.setState({
        hover: true
      });
    });
    (0, _defineProperty2.default)(this, "onMouseOut", () => {
      this.setState({
        hover: false
      });
    });
    this.state = {
      hover: false
    };
  }

  render() {
    const Tooltip = sdk.getComponent("elements.Tooltip");
    return _react.default.createElement("span", {
      onMouseOver: this.onMouseOver,
      onMouseOut: this.onMouseOut,
      className: this.props.class
    }, this.props.children, _react.default.createElement(Tooltip, {
      label: this.props.tooltip,
      visible: this.state.hover,
      tooltipClassName: this.props.tooltipClass,
      className: "mx_TextWithTooltip_tooltip"
    }));
  }

}

exports.default = TextWithTooltip;
(0, _defineProperty2.default)(TextWithTooltip, "propTypes", {
  class: _propTypes.default.string,
  tooltipClass: _propTypes.default.string,
  tooltip: _propTypes.default.node.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1RleHRXaXRoVG9vbHRpcC5qcyJdLCJuYW1lcyI6WyJUZXh0V2l0aFRvb2x0aXAiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwic2V0U3RhdGUiLCJob3ZlciIsInN0YXRlIiwicmVuZGVyIiwiVG9vbHRpcCIsInNkayIsImdldENvbXBvbmVudCIsIm9uTW91c2VPdmVyIiwib25Nb3VzZU91dCIsInByb3BzIiwiY2xhc3MiLCJjaGlsZHJlbiIsInRvb2x0aXAiLCJ0b29sdGlwQ2xhc3MiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJub2RlIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFsQkE7Ozs7Ozs7Ozs7Ozs7OztBQW9CZSxNQUFNQSxlQUFOLFNBQThCQyxlQUFNQyxTQUFwQyxDQUE4QztBQU96REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1Y7QUFEVSx1REFRQSxNQUFNO0FBQ2hCLFdBQUtDLFFBQUwsQ0FBYztBQUFDQyxRQUFBQSxLQUFLLEVBQUU7QUFBUixPQUFkO0FBQ0gsS0FWYTtBQUFBLHNEQVlELE1BQU07QUFDZixXQUFLRCxRQUFMLENBQWM7QUFBQ0MsUUFBQUEsS0FBSyxFQUFFO0FBQVIsT0FBZDtBQUNILEtBZGE7QUFHVixTQUFLQyxLQUFMLEdBQWE7QUFDVEQsTUFBQUEsS0FBSyxFQUFFO0FBREUsS0FBYjtBQUdIOztBQVVERSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFFQSxXQUNJO0FBQU0sTUFBQSxXQUFXLEVBQUUsS0FBS0MsV0FBeEI7QUFBcUMsTUFBQSxVQUFVLEVBQUUsS0FBS0MsVUFBdEQ7QUFBa0UsTUFBQSxTQUFTLEVBQUUsS0FBS0MsS0FBTCxDQUFXQztBQUF4RixPQUNLLEtBQUtELEtBQUwsQ0FBV0UsUUFEaEIsRUFFSSw2QkFBQyxPQUFEO0FBQ0ksTUFBQSxLQUFLLEVBQUUsS0FBS0YsS0FBTCxDQUFXRyxPQUR0QjtBQUVJLE1BQUEsT0FBTyxFQUFFLEtBQUtWLEtBQUwsQ0FBV0QsS0FGeEI7QUFHSSxNQUFBLGdCQUFnQixFQUFFLEtBQUtRLEtBQUwsQ0FBV0ksWUFIakM7QUFJSSxNQUFBLFNBQVMsRUFBRTtBQUpmLE1BRkosQ0FESjtBQVVIOztBQXBDd0Q7Ozs4QkFBeENqQixlLGVBQ0U7QUFDZmMsRUFBQUEsS0FBSyxFQUFFSSxtQkFBVUMsTUFERjtBQUVmRixFQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxNQUZUO0FBR2ZILEVBQUFBLE9BQU8sRUFBRUUsbUJBQVVFLElBQVYsQ0FBZUM7QUFIVCxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuIENvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkLlxyXG5cclxuIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcbiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXh0V2l0aFRvb2x0aXAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBjbGFzczogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICB0b29sdGlwQ2xhc3M6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgdG9vbHRpcDogUHJvcFR5cGVzLm5vZGUuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaG92ZXI6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgb25Nb3VzZU92ZXIgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aG92ZXI6IHRydWV9KTtcclxuICAgIH07XHJcblxyXG4gICAgb25Nb3VzZU91dCA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtob3ZlcjogZmFsc2V9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVG9vbHRpcFwiKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPHNwYW4gb25Nb3VzZU92ZXI9e3RoaXMub25Nb3VzZU92ZXJ9IG9uTW91c2VPdXQ9e3RoaXMub25Nb3VzZU91dH0gY2xhc3NOYW1lPXt0aGlzLnByb3BzLmNsYXNzfT5cclxuICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxyXG4gICAgICAgICAgICAgICAgPFRvb2x0aXBcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17dGhpcy5wcm9wcy50b29sdGlwfVxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU9e3RoaXMuc3RhdGUuaG92ZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgdG9vbHRpcENsYXNzTmFtZT17dGhpcy5wcm9wcy50b29sdGlwQ2xhc3N9XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtcIm14X1RleHRXaXRoVG9vbHRpcF90b29sdGlwXCJ9IC8+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==