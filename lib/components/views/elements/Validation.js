"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = withValidation;

var _classnames = _interopRequireDefault(require("classnames"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* eslint-disable babel/no-invalid-this */

/**
 * Creates a validation function from a set of rules describing what to validate.
 *
 * @param {Function} description
 *     Function that returns a string summary of the kind of value that will
 *     meet the validation rules. Shown at the top of the validation feedback.
 * @param {Object} rules
 *     An array of rules describing how to check to input value. Each rule in an object
 *     and may have the following properties:
 *     - `key`: A unique ID for the rule. Required.
 *     - `skip`: A function used to determine whether the rule should even be evaluated.
 *     - `test`: A function used to determine the rule's current validity. Required.
 *     - `valid`: Function returning text to show when the rule is valid. Only shown if set.
 *     - `invalid`: Function returning text to show when the rule is invalid. Only shown if set.
 *     - `final`: A Boolean if true states that this rule will only be considered if all rules before it returned valid.
 * @returns {Function}
 *     A validation function that takes in the current input value and returns
 *     the overall validity and a feedback UI that can be rendered for more detail.
 */
function withValidation({
  description,
  rules
}) {
  return async function onValidate({
    value,
    focused,
    allowEmpty = true
  }) {
    if (!value && allowEmpty) {
      return {
        valid: null,
        feedback: null
      };
    }

    const results = [];
    let valid = true;

    if (rules && rules.length) {
      for (const rule of rules) {
        if (!rule.key || !rule.test) {
          continue;
        }

        if (!valid && rule.final) {
          continue;
        }

        const data = {
          value,
          allowEmpty
        };

        if (rule.skip && rule.skip.call(this, data)) {
          continue;
        } // We're setting `this` to whichever component holds the validation
        // function. That allows rules to access the state of the component.


        const ruleValid = await rule.test.call(this, data);
        valid = valid && ruleValid;

        if (ruleValid && rule.valid) {
          // If the rule's result is valid and has text to show for
          // the valid state, show it.
          const text = rule.valid.call(this);

          if (!text) {
            continue;
          }

          results.push({
            key: rule.key,
            valid: true,
            text
          });
        } else if (!ruleValid && rule.invalid) {
          // If the rule's result is invalid and has text to show for
          // the invalid state, show it.
          const text = rule.invalid.call(this);

          if (!text) {
            continue;
          }

          results.push({
            key: rule.key,
            valid: false,
            text
          });
        }
      }
    } // Hide feedback when not focused


    if (!focused) {
      return {
        valid,
        feedback: null
      };
    }

    let details;

    if (results && results.length) {
      details = React.createElement("ul", {
        className: "mx_Validation_details"
      }, results.map(result => {
        const classes = (0, _classnames.default)({
          "mx_Validation_detail": true,
          "mx_Validation_valid": result.valid,
          "mx_Validation_invalid": !result.valid
        });
        return React.createElement("li", {
          key: result.key,
          className: classes
        }, result.text);
      }));
    }

    let summary;

    if (description) {
      // We're setting `this` to whichever component holds the validation
      // function. That allows rules to access the state of the component.
      const content = description.call(this);
      summary = React.createElement("div", {
        className: "mx_Validation_description"
      }, content);
    }

    let feedback;

    if (summary || details) {
      feedback = React.createElement("div", {
        className: "mx_Validation"
      }, summary, details);
    }

    return {
      valid,
      feedback
    };
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1ZhbGlkYXRpb24uanMiXSwibmFtZXMiOlsid2l0aFZhbGlkYXRpb24iLCJkZXNjcmlwdGlvbiIsInJ1bGVzIiwib25WYWxpZGF0ZSIsInZhbHVlIiwiZm9jdXNlZCIsImFsbG93RW1wdHkiLCJ2YWxpZCIsImZlZWRiYWNrIiwicmVzdWx0cyIsImxlbmd0aCIsInJ1bGUiLCJrZXkiLCJ0ZXN0IiwiZmluYWwiLCJkYXRhIiwic2tpcCIsImNhbGwiLCJydWxlVmFsaWQiLCJ0ZXh0IiwicHVzaCIsImludmFsaWQiLCJkZXRhaWxzIiwibWFwIiwicmVzdWx0IiwiY2xhc3NlcyIsInN1bW1hcnkiLCJjb250ZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFrQkE7O0FBbEJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJlLFNBQVNBLGNBQVQsQ0FBd0I7QUFBRUMsRUFBQUEsV0FBRjtBQUFlQyxFQUFBQTtBQUFmLENBQXhCLEVBQWdEO0FBQzNELFNBQU8sZUFBZUMsVUFBZixDQUEwQjtBQUFFQyxJQUFBQSxLQUFGO0FBQVNDLElBQUFBLE9BQVQ7QUFBa0JDLElBQUFBLFVBQVUsR0FBRztBQUEvQixHQUExQixFQUFpRTtBQUNwRSxRQUFJLENBQUNGLEtBQUQsSUFBVUUsVUFBZCxFQUEwQjtBQUN0QixhQUFPO0FBQ0hDLFFBQUFBLEtBQUssRUFBRSxJQURKO0FBRUhDLFFBQUFBLFFBQVEsRUFBRTtBQUZQLE9BQVA7QUFJSDs7QUFFRCxVQUFNQyxPQUFPLEdBQUcsRUFBaEI7QUFDQSxRQUFJRixLQUFLLEdBQUcsSUFBWjs7QUFDQSxRQUFJTCxLQUFLLElBQUlBLEtBQUssQ0FBQ1EsTUFBbkIsRUFBMkI7QUFDdkIsV0FBSyxNQUFNQyxJQUFYLElBQW1CVCxLQUFuQixFQUEwQjtBQUN0QixZQUFJLENBQUNTLElBQUksQ0FBQ0MsR0FBTixJQUFhLENBQUNELElBQUksQ0FBQ0UsSUFBdkIsRUFBNkI7QUFDekI7QUFDSDs7QUFFRCxZQUFJLENBQUNOLEtBQUQsSUFBVUksSUFBSSxDQUFDRyxLQUFuQixFQUEwQjtBQUN0QjtBQUNIOztBQUVELGNBQU1DLElBQUksR0FBRztBQUFFWCxVQUFBQSxLQUFGO0FBQVNFLFVBQUFBO0FBQVQsU0FBYjs7QUFFQSxZQUFJSyxJQUFJLENBQUNLLElBQUwsSUFBYUwsSUFBSSxDQUFDSyxJQUFMLENBQVVDLElBQVYsQ0FBZSxJQUFmLEVBQXFCRixJQUFyQixDQUFqQixFQUE2QztBQUN6QztBQUNILFNBYnFCLENBZXRCO0FBQ0E7OztBQUNBLGNBQU1HLFNBQVMsR0FBRyxNQUFNUCxJQUFJLENBQUNFLElBQUwsQ0FBVUksSUFBVixDQUFlLElBQWYsRUFBcUJGLElBQXJCLENBQXhCO0FBQ0FSLFFBQUFBLEtBQUssR0FBR0EsS0FBSyxJQUFJVyxTQUFqQjs7QUFDQSxZQUFJQSxTQUFTLElBQUlQLElBQUksQ0FBQ0osS0FBdEIsRUFBNkI7QUFDekI7QUFDQTtBQUNBLGdCQUFNWSxJQUFJLEdBQUdSLElBQUksQ0FBQ0osS0FBTCxDQUFXVSxJQUFYLENBQWdCLElBQWhCLENBQWI7O0FBQ0EsY0FBSSxDQUFDRSxJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNEVixVQUFBQSxPQUFPLENBQUNXLElBQVIsQ0FBYTtBQUNUUixZQUFBQSxHQUFHLEVBQUVELElBQUksQ0FBQ0MsR0FERDtBQUVUTCxZQUFBQSxLQUFLLEVBQUUsSUFGRTtBQUdUWSxZQUFBQTtBQUhTLFdBQWI7QUFLSCxTQVpELE1BWU8sSUFBSSxDQUFDRCxTQUFELElBQWNQLElBQUksQ0FBQ1UsT0FBdkIsRUFBZ0M7QUFDbkM7QUFDQTtBQUNBLGdCQUFNRixJQUFJLEdBQUdSLElBQUksQ0FBQ1UsT0FBTCxDQUFhSixJQUFiLENBQWtCLElBQWxCLENBQWI7O0FBQ0EsY0FBSSxDQUFDRSxJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNEVixVQUFBQSxPQUFPLENBQUNXLElBQVIsQ0FBYTtBQUNUUixZQUFBQSxHQUFHLEVBQUVELElBQUksQ0FBQ0MsR0FERDtBQUVUTCxZQUFBQSxLQUFLLEVBQUUsS0FGRTtBQUdUWSxZQUFBQTtBQUhTLFdBQWI7QUFLSDtBQUNKO0FBQ0osS0F4RG1FLENBMERwRTs7O0FBQ0EsUUFBSSxDQUFDZCxPQUFMLEVBQWM7QUFDVixhQUFPO0FBQ0hFLFFBQUFBLEtBREc7QUFFSEMsUUFBQUEsUUFBUSxFQUFFO0FBRlAsT0FBUDtBQUlIOztBQUVELFFBQUljLE9BQUo7O0FBQ0EsUUFBSWIsT0FBTyxJQUFJQSxPQUFPLENBQUNDLE1BQXZCLEVBQStCO0FBQzNCWSxNQUFBQSxPQUFPLEdBQUc7QUFBSSxRQUFBLFNBQVMsRUFBQztBQUFkLFNBQ0xiLE9BQU8sQ0FBQ2MsR0FBUixDQUFZQyxNQUFNLElBQUk7QUFDbkIsY0FBTUMsT0FBTyxHQUFHLHlCQUFXO0FBQ3ZCLGtDQUF3QixJQUREO0FBRXZCLGlDQUF1QkQsTUFBTSxDQUFDakIsS0FGUDtBQUd2QixtQ0FBeUIsQ0FBQ2lCLE1BQU0sQ0FBQ2pCO0FBSFYsU0FBWCxDQUFoQjtBQUtBLGVBQU87QUFBSSxVQUFBLEdBQUcsRUFBRWlCLE1BQU0sQ0FBQ1osR0FBaEI7QUFBcUIsVUFBQSxTQUFTLEVBQUVhO0FBQWhDLFdBQ0ZELE1BQU0sQ0FBQ0wsSUFETCxDQUFQO0FBR0gsT0FUQSxDQURLLENBQVY7QUFZSDs7QUFFRCxRQUFJTyxPQUFKOztBQUNBLFFBQUl6QixXQUFKLEVBQWlCO0FBQ2I7QUFDQTtBQUNBLFlBQU0wQixPQUFPLEdBQUcxQixXQUFXLENBQUNnQixJQUFaLENBQWlCLElBQWpCLENBQWhCO0FBQ0FTLE1BQUFBLE9BQU8sR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBNENDLE9BQTVDLENBQVY7QUFDSDs7QUFFRCxRQUFJbkIsUUFBSjs7QUFDQSxRQUFJa0IsT0FBTyxJQUFJSixPQUFmLEVBQXdCO0FBQ3BCZCxNQUFBQSxRQUFRLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ05rQixPQURNLEVBRU5KLE9BRk0sQ0FBWDtBQUlIOztBQUVELFdBQU87QUFDSGYsTUFBQUEsS0FERztBQUVIQyxNQUFBQTtBQUZHLEtBQVA7QUFJSCxHQXRHRDtBQXVHSCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLyogZXNsaW50LWRpc2FibGUgYmFiZWwvbm8taW52YWxpZC10aGlzICovXHJcblxyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuXHJcbi8qKlxyXG4gKiBDcmVhdGVzIGEgdmFsaWRhdGlvbiBmdW5jdGlvbiBmcm9tIGEgc2V0IG9mIHJ1bGVzIGRlc2NyaWJpbmcgd2hhdCB0byB2YWxpZGF0ZS5cclxuICpcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gZGVzY3JpcHRpb25cclxuICogICAgIEZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIHN0cmluZyBzdW1tYXJ5IG9mIHRoZSBraW5kIG9mIHZhbHVlIHRoYXQgd2lsbFxyXG4gKiAgICAgbWVldCB0aGUgdmFsaWRhdGlvbiBydWxlcy4gU2hvd24gYXQgdGhlIHRvcCBvZiB0aGUgdmFsaWRhdGlvbiBmZWVkYmFjay5cclxuICogQHBhcmFtIHtPYmplY3R9IHJ1bGVzXHJcbiAqICAgICBBbiBhcnJheSBvZiBydWxlcyBkZXNjcmliaW5nIGhvdyB0byBjaGVjayB0byBpbnB1dCB2YWx1ZS4gRWFjaCBydWxlIGluIGFuIG9iamVjdFxyXG4gKiAgICAgYW5kIG1heSBoYXZlIHRoZSBmb2xsb3dpbmcgcHJvcGVydGllczpcclxuICogICAgIC0gYGtleWA6IEEgdW5pcXVlIElEIGZvciB0aGUgcnVsZS4gUmVxdWlyZWQuXHJcbiAqICAgICAtIGBza2lwYDogQSBmdW5jdGlvbiB1c2VkIHRvIGRldGVybWluZSB3aGV0aGVyIHRoZSBydWxlIHNob3VsZCBldmVuIGJlIGV2YWx1YXRlZC5cclxuICogICAgIC0gYHRlc3RgOiBBIGZ1bmN0aW9uIHVzZWQgdG8gZGV0ZXJtaW5lIHRoZSBydWxlJ3MgY3VycmVudCB2YWxpZGl0eS4gUmVxdWlyZWQuXHJcbiAqICAgICAtIGB2YWxpZGA6IEZ1bmN0aW9uIHJldHVybmluZyB0ZXh0IHRvIHNob3cgd2hlbiB0aGUgcnVsZSBpcyB2YWxpZC4gT25seSBzaG93biBpZiBzZXQuXHJcbiAqICAgICAtIGBpbnZhbGlkYDogRnVuY3Rpb24gcmV0dXJuaW5nIHRleHQgdG8gc2hvdyB3aGVuIHRoZSBydWxlIGlzIGludmFsaWQuIE9ubHkgc2hvd24gaWYgc2V0LlxyXG4gKiAgICAgLSBgZmluYWxgOiBBIEJvb2xlYW4gaWYgdHJ1ZSBzdGF0ZXMgdGhhdCB0aGlzIHJ1bGUgd2lsbCBvbmx5IGJlIGNvbnNpZGVyZWQgaWYgYWxsIHJ1bGVzIGJlZm9yZSBpdCByZXR1cm5lZCB2YWxpZC5cclxuICogQHJldHVybnMge0Z1bmN0aW9ufVxyXG4gKiAgICAgQSB2YWxpZGF0aW9uIGZ1bmN0aW9uIHRoYXQgdGFrZXMgaW4gdGhlIGN1cnJlbnQgaW5wdXQgdmFsdWUgYW5kIHJldHVybnNcclxuICogICAgIHRoZSBvdmVyYWxsIHZhbGlkaXR5IGFuZCBhIGZlZWRiYWNrIFVJIHRoYXQgY2FuIGJlIHJlbmRlcmVkIGZvciBtb3JlIGRldGFpbC5cclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHdpdGhWYWxpZGF0aW9uKHsgZGVzY3JpcHRpb24sIHJ1bGVzIH0pIHtcclxuICAgIHJldHVybiBhc3luYyBmdW5jdGlvbiBvblZhbGlkYXRlKHsgdmFsdWUsIGZvY3VzZWQsIGFsbG93RW1wdHkgPSB0cnVlIH0pIHtcclxuICAgICAgICBpZiAoIXZhbHVlICYmIGFsbG93RW1wdHkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgZmVlZGJhY2s6IG51bGwsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgbGV0IHZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBpZiAocnVsZXMgJiYgcnVsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcnVsZSBvZiBydWxlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFydWxlLmtleSB8fCAhcnVsZS50ZXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCF2YWxpZCAmJiBydWxlLmZpbmFsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgZGF0YSA9IHsgdmFsdWUsIGFsbG93RW1wdHkgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZS5za2lwICYmIHJ1bGUuc2tpcC5jYWxsKHRoaXMsIGRhdGEpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2UncmUgc2V0dGluZyBgdGhpc2AgdG8gd2hpY2hldmVyIGNvbXBvbmVudCBob2xkcyB0aGUgdmFsaWRhdGlvblxyXG4gICAgICAgICAgICAgICAgLy8gZnVuY3Rpb24uIFRoYXQgYWxsb3dzIHJ1bGVzIHRvIGFjY2VzcyB0aGUgc3RhdGUgb2YgdGhlIGNvbXBvbmVudC5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJ1bGVWYWxpZCA9IGF3YWl0IHJ1bGUudGVzdC5jYWxsKHRoaXMsIGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgdmFsaWQgPSB2YWxpZCAmJiBydWxlVmFsaWQ7XHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZVZhbGlkICYmIHJ1bGUudmFsaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiB0aGUgcnVsZSdzIHJlc3VsdCBpcyB2YWxpZCBhbmQgaGFzIHRleHQgdG8gc2hvdyBmb3JcclxuICAgICAgICAgICAgICAgICAgICAvLyB0aGUgdmFsaWQgc3RhdGUsIHNob3cgaXQuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9IHJ1bGUudmFsaWQuY2FsbCh0aGlzKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXRleHQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogcnVsZS5rZXksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghcnVsZVZhbGlkICYmIHJ1bGUuaW52YWxpZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHRoZSBydWxlJ3MgcmVzdWx0IGlzIGludmFsaWQgYW5kIGhhcyB0ZXh0IHRvIHNob3cgZm9yXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlIGludmFsaWQgc3RhdGUsIHNob3cgaXQuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9IHJ1bGUuaW52YWxpZC5jYWxsKHRoaXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdGV4dCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiBydWxlLmtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBIaWRlIGZlZWRiYWNrIHdoZW4gbm90IGZvY3VzZWRcclxuICAgICAgICBpZiAoIWZvY3VzZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkLFxyXG4gICAgICAgICAgICAgICAgZmVlZGJhY2s6IG51bGwsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZGV0YWlscztcclxuICAgICAgICBpZiAocmVzdWx0cyAmJiByZXN1bHRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBkZXRhaWxzID0gPHVsIGNsYXNzTmFtZT1cIm14X1ZhbGlkYXRpb25fZGV0YWlsc1wiPlxyXG4gICAgICAgICAgICAgICAge3Jlc3VsdHMubWFwKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIm14X1ZhbGlkYXRpb25fZGV0YWlsXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibXhfVmFsaWRhdGlvbl92YWxpZFwiOiByZXN1bHQudmFsaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibXhfVmFsaWRhdGlvbl9pbnZhbGlkXCI6ICFyZXN1bHQudmFsaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxsaSBrZXk9e3Jlc3VsdC5rZXl9IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtyZXN1bHQudGV4dH1cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPjtcclxuICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICA8L3VsPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzdW1tYXJ5O1xyXG4gICAgICAgIGlmIChkZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgICAvLyBXZSdyZSBzZXR0aW5nIGB0aGlzYCB0byB3aGljaGV2ZXIgY29tcG9uZW50IGhvbGRzIHRoZSB2YWxpZGF0aW9uXHJcbiAgICAgICAgICAgIC8vIGZ1bmN0aW9uLiBUaGF0IGFsbG93cyBydWxlcyB0byBhY2Nlc3MgdGhlIHN0YXRlIG9mIHRoZSBjb21wb25lbnQuXHJcbiAgICAgICAgICAgIGNvbnN0IGNvbnRlbnQgPSBkZXNjcmlwdGlvbi5jYWxsKHRoaXMpO1xyXG4gICAgICAgICAgICBzdW1tYXJ5ID0gPGRpdiBjbGFzc05hbWU9XCJteF9WYWxpZGF0aW9uX2Rlc2NyaXB0aW9uXCI+e2NvbnRlbnR9PC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGZlZWRiYWNrO1xyXG4gICAgICAgIGlmIChzdW1tYXJ5IHx8IGRldGFpbHMpIHtcclxuICAgICAgICAgICAgZmVlZGJhY2sgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1ZhbGlkYXRpb25cIj5cclxuICAgICAgICAgICAgICAgIHtzdW1tYXJ5fVxyXG4gICAgICAgICAgICAgICAge2RldGFpbHN9XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHZhbGlkLFxyXG4gICAgICAgICAgICBmZWVkYmFjayxcclxuICAgICAgICB9O1xyXG4gICAgfTtcclxufVxyXG4iXX0=