"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _groups = require("../../../groups");

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const GroupRoomTile = (0, _createReactClass.default)({
  displayName: 'GroupRoomTile',
  propTypes: {
    groupId: _propTypes.default.string.isRequired,
    groupRoom: _groups.GroupRoomType.isRequired
  },
  onClick: function (e) {
    _dispatcher.default.dispatch({
      action: 'view_group_room',
      groupId: this.props.groupId,
      groupRoomId: this.props.groupRoom.roomId
    });
  },
  render: function () {
    const BaseAvatar = sdk.getComponent('avatars.BaseAvatar');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const avatarUrl = this.context.mxcUrlToHttp(this.props.groupRoom.avatarUrl, 36, 36, 'crop');

    const av = _react.default.createElement(BaseAvatar, {
      name: this.props.groupRoom.displayname,
      width: 36,
      height: 36,
      url: avatarUrl
    });

    return _react.default.createElement(AccessibleButton, {
      className: "mx_GroupRoomTile",
      onClick: this.onClick
    }, _react.default.createElement("div", {
      className: "mx_GroupRoomTile_avatar"
    }, av), _react.default.createElement("div", {
      className: "mx_GroupRoomTile_name"
    }, this.props.groupRoom.displayname));
  }
});
GroupRoomTile.contextType = _MatrixClientContext.default;
var _default = GroupRoomTile;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dyb3Vwcy9Hcm91cFJvb21UaWxlLmpzIl0sIm5hbWVzIjpbIkdyb3VwUm9vbVRpbGUiLCJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsImdyb3VwSWQiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiZ3JvdXBSb29tIiwiR3JvdXBSb29tVHlwZSIsIm9uQ2xpY2siLCJlIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJwcm9wcyIsImdyb3VwUm9vbUlkIiwicm9vbUlkIiwicmVuZGVyIiwiQmFzZUF2YXRhciIsInNkayIsImdldENvbXBvbmVudCIsIkFjY2Vzc2libGVCdXR0b24iLCJhdmF0YXJVcmwiLCJjb250ZXh0IiwibXhjVXJsVG9IdHRwIiwiYXYiLCJkaXNwbGF5bmFtZSIsImNvbnRleHRUeXBlIiwiTWF0cml4Q2xpZW50Q29udGV4dCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdEJBOzs7Ozs7Ozs7Ozs7Ozs7QUF3QkEsTUFBTUEsYUFBYSxHQUFHLCtCQUFpQjtBQUNuQ0MsRUFBQUEsV0FBVyxFQUFFLGVBRHNCO0FBR25DQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsT0FBTyxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEbkI7QUFFUEMsSUFBQUEsU0FBUyxFQUFFQyxzQkFBY0Y7QUFGbEIsR0FId0I7QUFRbkNHLEVBQUFBLE9BQU8sRUFBRSxVQUFTQyxDQUFULEVBQVk7QUFDakJDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLGlCQURDO0FBRVRWLE1BQUFBLE9BQU8sRUFBRSxLQUFLVyxLQUFMLENBQVdYLE9BRlg7QUFHVFksTUFBQUEsV0FBVyxFQUFFLEtBQUtELEtBQUwsQ0FBV1AsU0FBWCxDQUFxQlM7QUFIekIsS0FBYjtBQUtILEdBZGtDO0FBZ0JuQ0MsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFVBQU1FLFNBQVMsR0FBRyxLQUFLQyxPQUFMLENBQWFDLFlBQWIsQ0FDZCxLQUFLVixLQUFMLENBQVdQLFNBQVgsQ0FBcUJlLFNBRFAsRUFFZCxFQUZjLEVBRVYsRUFGVSxFQUVOLE1BRk0sQ0FBbEI7O0FBS0EsVUFBTUcsRUFBRSxHQUNKLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLElBQUksRUFBRSxLQUFLWCxLQUFMLENBQVdQLFNBQVgsQ0FBcUJtQixXQUF2QztBQUNJLE1BQUEsS0FBSyxFQUFFLEVBRFg7QUFDZSxNQUFBLE1BQU0sRUFBRSxFQUR2QjtBQUVJLE1BQUEsR0FBRyxFQUFFSjtBQUZULE1BREo7O0FBT0EsV0FDSSw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBQyxrQkFBNUI7QUFBK0MsTUFBQSxPQUFPLEVBQUUsS0FBS2I7QUFBN0QsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTWdCLEVBRE4sQ0FESixFQUlJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLEtBQUtYLEtBQUwsQ0FBV1AsU0FBWCxDQUFxQm1CLFdBRDNCLENBSkosQ0FESjtBQVVIO0FBekNrQyxDQUFqQixDQUF0QjtBQTRDQTFCLGFBQWEsQ0FBQzJCLFdBQWQsR0FBNEJDLDRCQUE1QjtlQUdlNUIsYSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IHsgR3JvdXBSb29tVHlwZSB9IGZyb20gJy4uLy4uLy4uL2dyb3Vwcyc7XHJcbmltcG9ydCBNYXRyaXhDbGllbnRDb250ZXh0IGZyb20gXCIuLi8uLi8uLi9jb250ZXh0cy9NYXRyaXhDbGllbnRDb250ZXh0XCI7XHJcblxyXG5jb25zdCBHcm91cFJvb21UaWxlID0gY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0dyb3VwUm9vbVRpbGUnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIGdyb3VwSWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBncm91cFJvb206IEdyb3VwUm9vbVR5cGUuaXNSZXF1aXJlZCxcclxuICAgIH0sXHJcblxyXG4gICAgb25DbGljazogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfZ3JvdXBfcm9vbScsXHJcbiAgICAgICAgICAgIGdyb3VwSWQ6IHRoaXMucHJvcHMuZ3JvdXBJZCxcclxuICAgICAgICAgICAgZ3JvdXBSb29tSWQ6IHRoaXMucHJvcHMuZ3JvdXBSb29tLnJvb21JZCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudCgnYXZhdGFycy5CYXNlQXZhdGFyJyk7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgICAgICBjb25zdCBhdmF0YXJVcmwgPSB0aGlzLmNvbnRleHQubXhjVXJsVG9IdHRwKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLmdyb3VwUm9vbS5hdmF0YXJVcmwsXHJcbiAgICAgICAgICAgIDM2LCAzNiwgJ2Nyb3AnLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGNvbnN0IGF2ID0gKFxyXG4gICAgICAgICAgICA8QmFzZUF2YXRhciBuYW1lPXt0aGlzLnByb3BzLmdyb3VwUm9vbS5kaXNwbGF5bmFtZX1cclxuICAgICAgICAgICAgICAgIHdpZHRoPXszNn0gaGVpZ2h0PXszNn1cclxuICAgICAgICAgICAgICAgIHVybD17YXZhdGFyVXJsfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0dyb3VwUm9vbVRpbGVcIiBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFJvb21UaWxlX2F2YXRhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgYXYgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0dyb3VwUm9vbVRpbGVfbmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5wcm9wcy5ncm91cFJvb20uZGlzcGxheW5hbWUgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcblxyXG5Hcm91cFJvb21UaWxlLmNvbnRleHRUeXBlID0gTWF0cml4Q2xpZW50Q29udGV4dDtcclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBHcm91cFJvb21UaWxlO1xyXG4iXX0=