"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'GroupUserSettings',
  statics: {
    contextType: _MatrixClientContext.default
  },

  getInitialState() {
    return {
      error: null,
      groups: null
    };
  },

  componentDidMount: function () {
    this.context.getJoinedGroups().then(result => {
      this.setState({
        groups: result.groups || [],
        error: null
      });
    }, err => {
      console.error(err);
      this.setState({
        groups: null,
        error: err
      });
    });
  },

  render() {
    let text = "";
    let groupPublicityToggles = null;
    const groups = this.state.groups;

    if (this.state.error) {
      text = (0, _languageHandler._t)('Something went wrong when trying to get your communities.');
    } else if (groups === null) {
      text = (0, _languageHandler._t)('Loading...');
    } else if (groups.length > 0) {
      const GroupPublicityToggle = sdk.getComponent('groups.GroupPublicityToggle');
      groupPublicityToggles = groups.map((groupId, index) => {
        return _react.default.createElement(GroupPublicityToggle, {
          key: index,
          groupId: groupId
        });
      });
      text = (0, _languageHandler._t)('Display your community flair in rooms configured to show it.');
    } else {
      text = (0, _languageHandler._t)("You're not currently a member of any communities.");
    }

    return _react.default.createElement("div", null, _react.default.createElement("p", {
      className: "mx_SettingsTab_subsectionText"
    }, text), _react.default.createElement("div", {
      className: "mx_SettingsTab_subsectionText"
    }, groupPublicityToggles));
  }

});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dyb3Vwcy9Hcm91cFVzZXJTZXR0aW5ncy5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsInN0YXRpY3MiLCJjb250ZXh0VHlwZSIsIk1hdHJpeENsaWVudENvbnRleHQiLCJnZXRJbml0aWFsU3RhdGUiLCJlcnJvciIsImdyb3VwcyIsImNvbXBvbmVudERpZE1vdW50IiwiY29udGV4dCIsImdldEpvaW5lZEdyb3VwcyIsInRoZW4iLCJyZXN1bHQiLCJzZXRTdGF0ZSIsImVyciIsImNvbnNvbGUiLCJyZW5kZXIiLCJ0ZXh0IiwiZ3JvdXBQdWJsaWNpdHlUb2dnbGVzIiwic3RhdGUiLCJsZW5ndGgiLCJHcm91cFB1YmxpY2l0eVRvZ2dsZSIsInNkayIsImdldENvbXBvbmVudCIsIm1hcCIsImdyb3VwSWQiLCJpbmRleCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBcEJBOzs7Ozs7Ozs7Ozs7Ozs7ZUFzQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsbUJBRGU7QUFHNUJDLEVBQUFBLE9BQU8sRUFBRTtBQUNMQyxJQUFBQSxXQUFXLEVBQUVDO0FBRFIsR0FIbUI7O0FBTzVCQyxFQUFBQSxlQUFlLEdBQUc7QUFDZCxXQUFPO0FBQ0hDLE1BQUFBLEtBQUssRUFBRSxJQURKO0FBRUhDLE1BQUFBLE1BQU0sRUFBRTtBQUZMLEtBQVA7QUFJSCxHQVoyQjs7QUFjNUJDLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsU0FBS0MsT0FBTCxDQUFhQyxlQUFiLEdBQStCQyxJQUEvQixDQUFxQ0MsTUFBRCxJQUFZO0FBQzVDLFdBQUtDLFFBQUwsQ0FBYztBQUFDTixRQUFBQSxNQUFNLEVBQUVLLE1BQU0sQ0FBQ0wsTUFBUCxJQUFpQixFQUExQjtBQUE4QkQsUUFBQUEsS0FBSyxFQUFFO0FBQXJDLE9BQWQ7QUFDSCxLQUZELEVBRUlRLEdBQUQsSUFBUztBQUNSQyxNQUFBQSxPQUFPLENBQUNULEtBQVIsQ0FBY1EsR0FBZDtBQUNBLFdBQUtELFFBQUwsQ0FBYztBQUFDTixRQUFBQSxNQUFNLEVBQUUsSUFBVDtBQUFlRCxRQUFBQSxLQUFLLEVBQUVRO0FBQXRCLE9BQWQ7QUFDSCxLQUxEO0FBTUgsR0FyQjJCOztBQXVCNUJFLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUlDLElBQUksR0FBRyxFQUFYO0FBQ0EsUUFBSUMscUJBQXFCLEdBQUcsSUFBNUI7QUFDQSxVQUFNWCxNQUFNLEdBQUcsS0FBS1ksS0FBTCxDQUFXWixNQUExQjs7QUFFQSxRQUFJLEtBQUtZLEtBQUwsQ0FBV2IsS0FBZixFQUFzQjtBQUNsQlcsTUFBQUEsSUFBSSxHQUFHLHlCQUFHLDJEQUFILENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSVYsTUFBTSxLQUFLLElBQWYsRUFBcUI7QUFDeEJVLE1BQUFBLElBQUksR0FBRyx5QkFBRyxZQUFILENBQVA7QUFDSCxLQUZNLE1BRUEsSUFBSVYsTUFBTSxDQUFDYSxNQUFQLEdBQWdCLENBQXBCLEVBQXVCO0FBQzFCLFlBQU1DLG9CQUFvQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTdCO0FBQ0FMLE1BQUFBLHFCQUFxQixHQUFHWCxNQUFNLENBQUNpQixHQUFQLENBQVcsQ0FBQ0MsT0FBRCxFQUFVQyxLQUFWLEtBQW9CO0FBQ25ELGVBQU8sNkJBQUMsb0JBQUQ7QUFBc0IsVUFBQSxHQUFHLEVBQUVBLEtBQTNCO0FBQWtDLFVBQUEsT0FBTyxFQUFFRDtBQUEzQyxVQUFQO0FBQ0gsT0FGdUIsQ0FBeEI7QUFHQVIsTUFBQUEsSUFBSSxHQUFHLHlCQUFHLDhEQUFILENBQVA7QUFDSCxLQU5NLE1BTUE7QUFDSEEsTUFBQUEsSUFBSSxHQUFHLHlCQUFHLG1EQUFILENBQVA7QUFDSDs7QUFFRCxXQUNJLDBDQUNJO0FBQUcsTUFBQSxTQUFTLEVBQUM7QUFBYixPQUErQ0EsSUFBL0MsQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNQyxxQkFETixDQUZKLENBREo7QUFRSDs7QUFsRDJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdHcm91cFVzZXJTZXR0aW5ncycsXHJcblxyXG4gICAgc3RhdGljczoge1xyXG4gICAgICAgIGNvbnRleHRUeXBlOiBNYXRyaXhDbGllbnRDb250ZXh0LFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgIGdyb3VwczogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0LmdldEpvaW5lZEdyb3VwcygpLnRoZW4oKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtncm91cHM6IHJlc3VsdC5ncm91cHMgfHwgW10sIGVycm9yOiBudWxsfSk7XHJcbiAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2dyb3VwczogbnVsbCwgZXJyb3I6IGVycn0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgbGV0IHRleHQgPSBcIlwiO1xyXG4gICAgICAgIGxldCBncm91cFB1YmxpY2l0eVRvZ2dsZXMgPSBudWxsO1xyXG4gICAgICAgIGNvbnN0IGdyb3VwcyA9IHRoaXMuc3RhdGUuZ3JvdXBzO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvcikge1xyXG4gICAgICAgICAgICB0ZXh0ID0gX3QoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoZW4gdHJ5aW5nIHRvIGdldCB5b3VyIGNvbW11bml0aWVzLicpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZ3JvdXBzID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHRleHQgPSBfdCgnTG9hZGluZy4uLicpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZ3JvdXBzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgR3JvdXBQdWJsaWNpdHlUb2dnbGUgPSBzZGsuZ2V0Q29tcG9uZW50KCdncm91cHMuR3JvdXBQdWJsaWNpdHlUb2dnbGUnKTtcclxuICAgICAgICAgICAgZ3JvdXBQdWJsaWNpdHlUb2dnbGVzID0gZ3JvdXBzLm1hcCgoZ3JvdXBJZCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiA8R3JvdXBQdWJsaWNpdHlUb2dnbGUga2V5PXtpbmRleH0gZ3JvdXBJZD17Z3JvdXBJZH0gLz47XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0ZXh0ID0gX3QoJ0Rpc3BsYXkgeW91ciBjb21tdW5pdHkgZmxhaXIgaW4gcm9vbXMgY29uZmlndXJlZCB0byBzaG93IGl0LicpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRleHQgPSBfdChcIllvdSdyZSBub3QgY3VycmVudGx5IGEgbWVtYmVyIG9mIGFueSBjb21tdW5pdGllcy5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwibXhfU2V0dGluZ3NUYWJfc3Vic2VjdGlvblRleHRcIj57IHRleHQgfTwvcD5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TZXR0aW5nc1RhYl9zdWJzZWN0aW9uVGV4dCc+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBncm91cFB1YmxpY2l0eVRvZ2dsZXMgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19