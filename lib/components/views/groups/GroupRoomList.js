"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _GroupStore = _interopRequireDefault(require("../../../stores/GroupStore"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _GroupAddressPicker = require("../../../GroupAddressPicker");

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _TintableSvg = _interopRequireDefault(require("../elements/TintableSvg"));

var _AutoHideScrollbar = _interopRequireDefault(require("../../structures/AutoHideScrollbar"));

/*
Copyright 2017 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const INITIAL_LOAD_NUM_ROOMS = 30;

var _default = (0, _createReactClass.default)({
  displayName: "GroupRoomList",
  propTypes: {
    groupId: _propTypes.default.string.isRequired
  },
  getInitialState: function () {
    return {
      rooms: null,
      truncateAt: INITIAL_LOAD_NUM_ROOMS,
      searchQuery: ""
    };
  },
  componentDidMount: function () {
    this._unmounted = false;

    this._initGroupStore(this.props.groupId);
  },

  componentWillUnmount() {
    this._unmounted = true;

    this._unregisterGroupStore();
  },

  _unregisterGroupStore() {
    _GroupStore.default.unregisterListener(this.onGroupStoreUpdated);
  },

  _initGroupStore: function (groupId) {
    _GroupStore.default.registerListener(groupId, this.onGroupStoreUpdated); // XXX: This should be more fluxy - let's get the error from GroupStore .getError or something
    // XXX: This is also leaked - we should remove it when unmounting


    _GroupStore.default.on('error', (err, errorGroupId) => {
      if (errorGroupId !== groupId) return;
      this.setState({
        rooms: null
      });
    });
  },
  onGroupStoreUpdated: function () {
    if (this._unmounted) return;
    this.setState({
      rooms: _GroupStore.default.getGroupRooms(this.props.groupId)
    });
  },
  _createOverflowTile: function (overflowCount, totalCount) {
    // For now we'll pretend this is any entity. It should probably be a separate tile.
    const EntityTile = sdk.getComponent("rooms.EntityTile");
    const BaseAvatar = sdk.getComponent("avatars.BaseAvatar");
    const text = (0, _languageHandler._t)("and %(count)s others...", {
      count: overflowCount
    });
    return _react.default.createElement(EntityTile, {
      className: "mx_EntityTile_ellipsis",
      avatarJsx: _react.default.createElement(BaseAvatar, {
        url: require("../../../../res/img/ellipsis.svg"),
        name: "...",
        width: 36,
        height: 36
      }),
      name: text,
      presenceState: "online",
      suppressOnHover: true,
      onClick: this._showFullRoomList
    });
  },
  _showFullRoomList: function () {
    this.setState({
      truncateAt: -1
    });
  },
  onSearchQueryChanged: function (ev) {
    this.setState({
      searchQuery: ev.target.value
    });
  },

  onAddRoomToGroupButtonClick() {
    (0, _GroupAddressPicker.showGroupAddRoomDialog)(this.props.groupId).then(() => {
      this.forceUpdate();
    });
  },

  makeGroupRoomTiles: function (query) {
    const GroupRoomTile = sdk.getComponent("groups.GroupRoomTile");
    query = (query || "").toLowerCase();
    let roomList = this.state.rooms;

    if (query) {
      roomList = roomList.filter(room => {
        const matchesName = (room.name || "").toLowerCase().includes(query);
        const matchesAlias = (room.canonicalAlias || "").toLowerCase().includes(query);
        return matchesName || matchesAlias;
      });
    }

    roomList = roomList.map((groupRoom, index) => {
      return _react.default.createElement(GroupRoomTile, {
        key: index,
        groupId: this.props.groupId,
        groupRoom: groupRoom
      });
    });
    return roomList;
  },
  render: function () {
    if (this.state.rooms === null) {
      return null;
    }

    let inviteButton;

    if (_GroupStore.default.isUserPrivileged(this.props.groupId)) {
      inviteButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RightPanel_invite",
        onClick: this.onAddRoomToGroupButtonClick
      }, _react.default.createElement("div", {
        className: "mx_RightPanel_icon"
      }, _react.default.createElement(_TintableSvg.default, {
        src: require("../../../../res/img/icons-room-add.svg"),
        width: "18",
        height: "14"
      })), _react.default.createElement("div", {
        className: "mx_RightPanel_message"
      }, (0, _languageHandler._t)('Add rooms to this community')));
    }

    const inputBox = _react.default.createElement("input", {
      className: "mx_GroupRoomList_query mx_textinput",
      id: "mx_GroupRoomList_query",
      type: "text",
      onChange: this.onSearchQueryChanged,
      value: this.state.searchQuery,
      placeholder: (0, _languageHandler._t)('Filter community rooms'),
      autoComplete: "off"
    });

    const TruncatedList = sdk.getComponent("elements.TruncatedList");
    return _react.default.createElement("div", {
      className: "mx_GroupRoomList",
      role: "tabpanel"
    }, inviteButton, _react.default.createElement(_AutoHideScrollbar.default, {
      className: "mx_GroupRoomList_joined mx_GroupRoomList_outerWrapper"
    }, _react.default.createElement(TruncatedList, {
      className: "mx_GroupRoomList_wrapper",
      truncateAt: this.state.truncateAt,
      createOverflowElement: this._createOverflowTile
    }, this.makeGroupRoomTiles(this.state.searchQuery))), inputBox);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dyb3Vwcy9Hcm91cFJvb21MaXN0LmpzIl0sIm5hbWVzIjpbIklOSVRJQUxfTE9BRF9OVU1fUk9PTVMiLCJwcm9wVHlwZXMiLCJncm91cElkIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsImdldEluaXRpYWxTdGF0ZSIsInJvb21zIiwidHJ1bmNhdGVBdCIsInNlYXJjaFF1ZXJ5IiwiY29tcG9uZW50RGlkTW91bnQiLCJfdW5tb3VudGVkIiwiX2luaXRHcm91cFN0b3JlIiwicHJvcHMiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsIl91bnJlZ2lzdGVyR3JvdXBTdG9yZSIsIkdyb3VwU3RvcmUiLCJ1bnJlZ2lzdGVyTGlzdGVuZXIiLCJvbkdyb3VwU3RvcmVVcGRhdGVkIiwicmVnaXN0ZXJMaXN0ZW5lciIsIm9uIiwiZXJyIiwiZXJyb3JHcm91cElkIiwic2V0U3RhdGUiLCJnZXRHcm91cFJvb21zIiwiX2NyZWF0ZU92ZXJmbG93VGlsZSIsIm92ZXJmbG93Q291bnQiLCJ0b3RhbENvdW50IiwiRW50aXR5VGlsZSIsInNkayIsImdldENvbXBvbmVudCIsIkJhc2VBdmF0YXIiLCJ0ZXh0IiwiY291bnQiLCJyZXF1aXJlIiwiX3Nob3dGdWxsUm9vbUxpc3QiLCJvblNlYXJjaFF1ZXJ5Q2hhbmdlZCIsImV2IiwidGFyZ2V0IiwidmFsdWUiLCJvbkFkZFJvb21Ub0dyb3VwQnV0dG9uQ2xpY2siLCJ0aGVuIiwiZm9yY2VVcGRhdGUiLCJtYWtlR3JvdXBSb29tVGlsZXMiLCJxdWVyeSIsIkdyb3VwUm9vbVRpbGUiLCJ0b0xvd2VyQ2FzZSIsInJvb21MaXN0Iiwic3RhdGUiLCJmaWx0ZXIiLCJyb29tIiwibWF0Y2hlc05hbWUiLCJuYW1lIiwiaW5jbHVkZXMiLCJtYXRjaGVzQWxpYXMiLCJjYW5vbmljYWxBbGlhcyIsIm1hcCIsImdyb3VwUm9vbSIsImluZGV4IiwicmVuZGVyIiwiaW52aXRlQnV0dG9uIiwiaXNVc2VyUHJpdmlsZWdlZCIsImlucHV0Qm94IiwiVHJ1bmNhdGVkTGlzdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFlQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7OztBQTBCQSxNQUFNQSxzQkFBc0IsR0FBRyxFQUEvQjs7ZUFFZSwrQkFBaUI7QUFBQTtBQUM1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLE9BQU8sRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBRG5CLEdBRGlCO0FBSzVCQyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLEtBQUssRUFBRSxJQURKO0FBRUhDLE1BQUFBLFVBQVUsRUFBRVIsc0JBRlQ7QUFHSFMsTUFBQUEsV0FBVyxFQUFFO0FBSFYsS0FBUDtBQUtILEdBWDJCO0FBYTVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLFVBQUwsR0FBa0IsS0FBbEI7O0FBQ0EsU0FBS0MsZUFBTCxDQUFxQixLQUFLQyxLQUFMLENBQVdYLE9BQWhDO0FBQ0gsR0FoQjJCOztBQWtCNUJZLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFNBQUtILFVBQUwsR0FBa0IsSUFBbEI7O0FBQ0EsU0FBS0kscUJBQUw7QUFDSCxHQXJCMkI7O0FBdUI1QkEsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEJDLHdCQUFXQyxrQkFBWCxDQUE4QixLQUFLQyxtQkFBbkM7QUFDSCxHQXpCMkI7O0FBMkI1Qk4sRUFBQUEsZUFBZSxFQUFFLFVBQVNWLE9BQVQsRUFBa0I7QUFDL0JjLHdCQUFXRyxnQkFBWCxDQUE0QmpCLE9BQTVCLEVBQXFDLEtBQUtnQixtQkFBMUMsRUFEK0IsQ0FFL0I7QUFDQTs7O0FBQ0FGLHdCQUFXSSxFQUFYLENBQWMsT0FBZCxFQUF1QixDQUFDQyxHQUFELEVBQU1DLFlBQU4sS0FBdUI7QUFDMUMsVUFBSUEsWUFBWSxLQUFLcEIsT0FBckIsRUFBOEI7QUFDOUIsV0FBS3FCLFFBQUwsQ0FBYztBQUNWaEIsUUFBQUEsS0FBSyxFQUFFO0FBREcsT0FBZDtBQUdILEtBTEQ7QUFNSCxHQXJDMkI7QUF1QzVCVyxFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCLFFBQUksS0FBS1AsVUFBVCxFQUFxQjtBQUNyQixTQUFLWSxRQUFMLENBQWM7QUFDVmhCLE1BQUFBLEtBQUssRUFBRVMsb0JBQVdRLGFBQVgsQ0FBeUIsS0FBS1gsS0FBTCxDQUFXWCxPQUFwQztBQURHLEtBQWQ7QUFHSCxHQTVDMkI7QUE4QzVCdUIsRUFBQUEsbUJBQW1CLEVBQUUsVUFBU0MsYUFBVCxFQUF3QkMsVUFBeEIsRUFBb0M7QUFDckQ7QUFDQSxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBbkI7QUFDQSxVQUFNQyxVQUFVLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7QUFDQSxVQUFNRSxJQUFJLEdBQUcseUJBQUcseUJBQUgsRUFBOEI7QUFBRUMsTUFBQUEsS0FBSyxFQUFFUDtBQUFULEtBQTlCLENBQWI7QUFDQSxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQyx3QkFBdEI7QUFBK0MsTUFBQSxTQUFTLEVBQ3BELDZCQUFDLFVBQUQ7QUFBWSxRQUFBLEdBQUcsRUFBRVEsT0FBTyxDQUFDLGtDQUFELENBQXhCO0FBQThELFFBQUEsSUFBSSxFQUFDLEtBQW5FO0FBQXlFLFFBQUEsS0FBSyxFQUFFLEVBQWhGO0FBQW9GLFFBQUEsTUFBTSxFQUFFO0FBQTVGLFFBREo7QUFFRSxNQUFBLElBQUksRUFBRUYsSUFGUjtBQUVjLE1BQUEsYUFBYSxFQUFDLFFBRjVCO0FBRXFDLE1BQUEsZUFBZSxFQUFFLElBRnREO0FBR0EsTUFBQSxPQUFPLEVBQUUsS0FBS0c7QUFIZCxNQURKO0FBTUgsR0F6RDJCO0FBMkQ1QkEsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLWixRQUFMLENBQWM7QUFDVmYsTUFBQUEsVUFBVSxFQUFFLENBQUM7QUFESCxLQUFkO0FBR0gsR0EvRDJCO0FBaUU1QjRCLEVBQUFBLG9CQUFvQixFQUFFLFVBQVNDLEVBQVQsRUFBYTtBQUMvQixTQUFLZCxRQUFMLENBQWM7QUFBRWQsTUFBQUEsV0FBVyxFQUFFNEIsRUFBRSxDQUFDQyxNQUFILENBQVVDO0FBQXpCLEtBQWQ7QUFDSCxHQW5FMkI7O0FBcUU1QkMsRUFBQUEsMkJBQTJCLEdBQUc7QUFDMUIsb0RBQXVCLEtBQUszQixLQUFMLENBQVdYLE9BQWxDLEVBQTJDdUMsSUFBM0MsQ0FBZ0QsTUFBTTtBQUNsRCxXQUFLQyxXQUFMO0FBQ0gsS0FGRDtBQUdILEdBekUyQjs7QUEyRTVCQyxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTQyxLQUFULEVBQWdCO0FBQ2hDLFVBQU1DLGFBQWEsR0FBR2hCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBdEI7QUFDQWMsSUFBQUEsS0FBSyxHQUFHLENBQUNBLEtBQUssSUFBSSxFQUFWLEVBQWNFLFdBQWQsRUFBUjtBQUVBLFFBQUlDLFFBQVEsR0FBRyxLQUFLQyxLQUFMLENBQVd6QyxLQUExQjs7QUFDQSxRQUFJcUMsS0FBSixFQUFXO0FBQ1BHLE1BQUFBLFFBQVEsR0FBR0EsUUFBUSxDQUFDRSxNQUFULENBQWlCQyxJQUFELElBQVU7QUFDakMsY0FBTUMsV0FBVyxHQUFHLENBQUNELElBQUksQ0FBQ0UsSUFBTCxJQUFhLEVBQWQsRUFBa0JOLFdBQWxCLEdBQWdDTyxRQUFoQyxDQUF5Q1QsS0FBekMsQ0FBcEI7QUFDQSxjQUFNVSxZQUFZLEdBQUcsQ0FBQ0osSUFBSSxDQUFDSyxjQUFMLElBQXVCLEVBQXhCLEVBQTRCVCxXQUE1QixHQUEwQ08sUUFBMUMsQ0FBbURULEtBQW5ELENBQXJCO0FBQ0EsZUFBT08sV0FBVyxJQUFJRyxZQUF0QjtBQUNILE9BSlUsQ0FBWDtBQUtIOztBQUVEUCxJQUFBQSxRQUFRLEdBQUdBLFFBQVEsQ0FBQ1MsR0FBVCxDQUFhLENBQUNDLFNBQUQsRUFBWUMsS0FBWixLQUFzQjtBQUMxQyxhQUNJLDZCQUFDLGFBQUQ7QUFDSSxRQUFBLEdBQUcsRUFBRUEsS0FEVDtBQUVJLFFBQUEsT0FBTyxFQUFFLEtBQUs3QyxLQUFMLENBQVdYLE9BRnhCO0FBR0ksUUFBQSxTQUFTLEVBQUV1RDtBQUhmLFFBREo7QUFNSCxLQVBVLENBQVg7QUFTQSxXQUFPVixRQUFQO0FBQ0gsR0FsRzJCO0FBb0c1QlksRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJLEtBQUtYLEtBQUwsQ0FBV3pDLEtBQVgsS0FBcUIsSUFBekIsRUFBK0I7QUFDM0IsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsUUFBSXFELFlBQUo7O0FBQ0EsUUFBSTVDLG9CQUFXNkMsZ0JBQVgsQ0FBNEIsS0FBS2hELEtBQUwsQ0FBV1gsT0FBdkMsQ0FBSixFQUFxRDtBQUNqRDBELE1BQUFBLFlBQVksR0FDUiw2QkFBQyx5QkFBRDtBQUNJLFFBQUEsU0FBUyxFQUFDLHNCQURkO0FBRUksUUFBQSxPQUFPLEVBQUUsS0FBS3BCO0FBRmxCLFNBSUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsb0JBQUQ7QUFBYSxRQUFBLEdBQUcsRUFBRU4sT0FBTyxDQUFDLHdDQUFELENBQXpCO0FBQXFFLFFBQUEsS0FBSyxFQUFDLElBQTNFO0FBQWdGLFFBQUEsTUFBTSxFQUFDO0FBQXZGLFFBREosQ0FKSixFQU9JO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUF5Qyx5QkFBRyw2QkFBSCxDQUF6QyxDQVBKLENBREo7QUFXSDs7QUFDRCxVQUFNNEIsUUFBUSxHQUNWO0FBQU8sTUFBQSxTQUFTLEVBQUMscUNBQWpCO0FBQXVELE1BQUEsRUFBRSxFQUFDLHdCQUExRDtBQUFtRixNQUFBLElBQUksRUFBQyxNQUF4RjtBQUNRLE1BQUEsUUFBUSxFQUFFLEtBQUsxQixvQkFEdkI7QUFDNkMsTUFBQSxLQUFLLEVBQUUsS0FBS1ksS0FBTCxDQUFXdkMsV0FEL0Q7QUFFUSxNQUFBLFdBQVcsRUFBRSx5QkFBRyx3QkFBSCxDQUZyQjtBQUVtRCxNQUFBLFlBQVksRUFBQztBQUZoRSxNQURKOztBQU1BLFVBQU1zRCxhQUFhLEdBQUdsQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCO0FBQ0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLGtCQUFmO0FBQWtDLE1BQUEsSUFBSSxFQUFDO0FBQXZDLE9BQ004QixZQUROLEVBRUksNkJBQUMsMEJBQUQ7QUFBbUIsTUFBQSxTQUFTLEVBQUM7QUFBN0IsT0FDSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxTQUFTLEVBQUMsMEJBQXpCO0FBQW9ELE1BQUEsVUFBVSxFQUFFLEtBQUtaLEtBQUwsQ0FBV3hDLFVBQTNFO0FBQ1EsTUFBQSxxQkFBcUIsRUFBRSxLQUFLaUI7QUFEcEMsT0FFTSxLQUFLa0Isa0JBQUwsQ0FBd0IsS0FBS0ssS0FBTCxDQUFXdkMsV0FBbkMsQ0FGTixDQURKLENBRkosRUFRTXFELFFBUk4sQ0FESjtBQVlIO0FBMUkyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGQuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCBHcm91cFN0b3JlIGZyb20gJy4uLy4uLy4uL3N0b3Jlcy9Hcm91cFN0b3JlJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgc2hvd0dyb3VwQWRkUm9vbURpYWxvZyB9IGZyb20gJy4uLy4uLy4uL0dyb3VwQWRkcmVzc1BpY2tlcic7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b24nO1xyXG5pbXBvcnQgVGludGFibGVTdmcgZnJvbSAnLi4vZWxlbWVudHMvVGludGFibGVTdmcnO1xyXG5pbXBvcnQgQXV0b0hpZGVTY3JvbGxiYXIgZnJvbSBcIi4uLy4uL3N0cnVjdHVyZXMvQXV0b0hpZGVTY3JvbGxiYXJcIjtcclxuXHJcbmNvbnN0IElOSVRJQUxfTE9BRF9OVU1fUk9PTVMgPSAzMDtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgZ3JvdXBJZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJvb21zOiBudWxsLFxyXG4gICAgICAgICAgICB0cnVuY2F0ZUF0OiBJTklUSUFMX0xPQURfTlVNX1JPT01TLFxyXG4gICAgICAgICAgICBzZWFyY2hRdWVyeTogXCJcIixcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5faW5pdEdyb3VwU3RvcmUodGhpcy5wcm9wcy5ncm91cElkKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl91bnJlZ2lzdGVyR3JvdXBTdG9yZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdW5yZWdpc3Rlckdyb3VwU3RvcmUoKSB7XHJcbiAgICAgICAgR3JvdXBTdG9yZS51bnJlZ2lzdGVyTGlzdGVuZXIodGhpcy5vbkdyb3VwU3RvcmVVcGRhdGVkKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2luaXRHcm91cFN0b3JlOiBmdW5jdGlvbihncm91cElkKSB7XHJcbiAgICAgICAgR3JvdXBTdG9yZS5yZWdpc3Rlckxpc3RlbmVyKGdyb3VwSWQsIHRoaXMub25Hcm91cFN0b3JlVXBkYXRlZCk7XHJcbiAgICAgICAgLy8gWFhYOiBUaGlzIHNob3VsZCBiZSBtb3JlIGZsdXh5IC0gbGV0J3MgZ2V0IHRoZSBlcnJvciBmcm9tIEdyb3VwU3RvcmUgLmdldEVycm9yIG9yIHNvbWV0aGluZ1xyXG4gICAgICAgIC8vIFhYWDogVGhpcyBpcyBhbHNvIGxlYWtlZCAtIHdlIHNob3VsZCByZW1vdmUgaXQgd2hlbiB1bm1vdW50aW5nXHJcbiAgICAgICAgR3JvdXBTdG9yZS5vbignZXJyb3InLCAoZXJyLCBlcnJvckdyb3VwSWQpID0+IHtcclxuICAgICAgICAgICAgaWYgKGVycm9yR3JvdXBJZCAhPT0gZ3JvdXBJZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHJvb21zOiBudWxsLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Hcm91cFN0b3JlVXBkYXRlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICByb29tczogR3JvdXBTdG9yZS5nZXRHcm91cFJvb21zKHRoaXMucHJvcHMuZ3JvdXBJZCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jcmVhdGVPdmVyZmxvd1RpbGU6IGZ1bmN0aW9uKG92ZXJmbG93Q291bnQsIHRvdGFsQ291bnQpIHtcclxuICAgICAgICAvLyBGb3Igbm93IHdlJ2xsIHByZXRlbmQgdGhpcyBpcyBhbnkgZW50aXR5LiBJdCBzaG91bGQgcHJvYmFibHkgYmUgYSBzZXBhcmF0ZSB0aWxlLlxyXG4gICAgICAgIGNvbnN0IEVudGl0eVRpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuRW50aXR5VGlsZVwiKTtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcImF2YXRhcnMuQmFzZUF2YXRhclwiKTtcclxuICAgICAgICBjb25zdCB0ZXh0ID0gX3QoXCJhbmQgJShjb3VudClzIG90aGVycy4uLlwiLCB7IGNvdW50OiBvdmVyZmxvd0NvdW50IH0pO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxFbnRpdHlUaWxlIGNsYXNzTmFtZT1cIm14X0VudGl0eVRpbGVfZWxsaXBzaXNcIiBhdmF0YXJKc3g9e1xyXG4gICAgICAgICAgICAgICAgPEJhc2VBdmF0YXIgdXJsPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9lbGxpcHNpcy5zdmdcIil9IG5hbWU9XCIuLi5cIiB3aWR0aD17MzZ9IGhlaWdodD17MzZ9IC8+XHJcbiAgICAgICAgICAgIH0gbmFtZT17dGV4dH0gcHJlc2VuY2VTdGF0ZT1cIm9ubGluZVwiIHN1cHByZXNzT25Ib3Zlcj17dHJ1ZX1cclxuICAgICAgICAgICAgb25DbGljaz17dGhpcy5fc2hvd0Z1bGxSb29tTGlzdH0gLz5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvd0Z1bGxSb29tTGlzdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHRydW5jYXRlQXQ6IC0xLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblNlYXJjaFF1ZXJ5Q2hhbmdlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgc2VhcmNoUXVlcnk6IGV2LnRhcmdldC52YWx1ZSB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BZGRSb29tVG9Hcm91cEJ1dHRvbkNsaWNrKCkge1xyXG4gICAgICAgIHNob3dHcm91cEFkZFJvb21EaWFsb2codGhpcy5wcm9wcy5ncm91cElkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBtYWtlR3JvdXBSb29tVGlsZXM6IGZ1bmN0aW9uKHF1ZXJ5KSB7XHJcbiAgICAgICAgY29uc3QgR3JvdXBSb29tVGlsZSA9IHNkay5nZXRDb21wb25lbnQoXCJncm91cHMuR3JvdXBSb29tVGlsZVwiKTtcclxuICAgICAgICBxdWVyeSA9IChxdWVyeSB8fCBcIlwiKS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICBsZXQgcm9vbUxpc3QgPSB0aGlzLnN0YXRlLnJvb21zO1xyXG4gICAgICAgIGlmIChxdWVyeSkge1xyXG4gICAgICAgICAgICByb29tTGlzdCA9IHJvb21MaXN0LmZpbHRlcigocm9vbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWF0Y2hlc05hbWUgPSAocm9vbS5uYW1lIHx8IFwiXCIpLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMocXVlcnkpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWF0Y2hlc0FsaWFzID0gKHJvb20uY2Fub25pY2FsQWxpYXMgfHwgXCJcIikudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhxdWVyeSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbWF0Y2hlc05hbWUgfHwgbWF0Y2hlc0FsaWFzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJvb21MaXN0ID0gcm9vbUxpc3QubWFwKChncm91cFJvb20sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8R3JvdXBSb29tVGlsZVxyXG4gICAgICAgICAgICAgICAgICAgIGtleT17aW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBJZD17dGhpcy5wcm9wcy5ncm91cElkfVxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwUm9vbT17Z3JvdXBSb29tfSAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcm9vbUxpc3Q7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucm9vbXMgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaW52aXRlQnV0dG9uO1xyXG4gICAgICAgIGlmIChHcm91cFN0b3JlLmlzVXNlclByaXZpbGVnZWQodGhpcy5wcm9wcy5ncm91cElkKSkge1xyXG4gICAgICAgICAgICBpbnZpdGVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1JpZ2h0UGFuZWxfaW52aXRlXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQWRkUm9vbVRvR3JvdXBCdXR0b25DbGlja31cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1JpZ2h0UGFuZWxfaWNvblwiID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFRpbnRhYmxlU3ZnIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbnMtcm9vbS1hZGQuc3ZnXCIpfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMTRcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUmlnaHRQYW5lbF9tZXNzYWdlXCI+eyBfdCgnQWRkIHJvb21zIHRvIHRoaXMgY29tbXVuaXR5JykgfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpbnB1dEJveCA9IChcclxuICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cIm14X0dyb3VwUm9vbUxpc3RfcXVlcnkgbXhfdGV4dGlucHV0XCIgaWQ9XCJteF9Hcm91cFJvb21MaXN0X3F1ZXJ5XCIgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uU2VhcmNoUXVlcnlDaGFuZ2VkfSB2YWx1ZT17dGhpcy5zdGF0ZS5zZWFyY2hRdWVyeX1cclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17X3QoJ0ZpbHRlciBjb21tdW5pdHkgcm9vbXMnKX0gYXV0b0NvbXBsZXRlPVwib2ZmXCIgLz5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBUcnVuY2F0ZWRMaXN0ID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlRydW5jYXRlZExpc3RcIik7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Hcm91cFJvb21MaXN0XCIgcm9sZT1cInRhYnBhbmVsXCI+XHJcbiAgICAgICAgICAgICAgICB7IGludml0ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICA8QXV0b0hpZGVTY3JvbGxiYXIgY2xhc3NOYW1lPVwibXhfR3JvdXBSb29tTGlzdF9qb2luZWQgbXhfR3JvdXBSb29tTGlzdF9vdXRlcldyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8VHJ1bmNhdGVkTGlzdCBjbGFzc05hbWU9XCJteF9Hcm91cFJvb21MaXN0X3dyYXBwZXJcIiB0cnVuY2F0ZUF0PXt0aGlzLnN0YXRlLnRydW5jYXRlQXR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVPdmVyZmxvd0VsZW1lbnQ9e3RoaXMuX2NyZWF0ZU92ZXJmbG93VGlsZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhpcy5tYWtlR3JvdXBSb29tVGlsZXModGhpcy5zdGF0ZS5zZWFyY2hRdWVyeSkgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvVHJ1bmNhdGVkTGlzdD5cclxuICAgICAgICAgICAgICAgIDwvQXV0b0hpZGVTY3JvbGxiYXI+XHJcbiAgICAgICAgICAgICAgICB7IGlucHV0Qm94IH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=