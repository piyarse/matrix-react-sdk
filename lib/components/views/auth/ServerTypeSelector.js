"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTypeFromServerConfig = getTypeFromServerConfig;
exports.default = exports.TYPES = exports.ADVANCED = exports.PREMIUM = exports.FREE = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _classnames = _interopRequireDefault(require("classnames"));

var _AutoDiscoveryUtils = require("../../../utils/AutoDiscoveryUtils");

var _TypeUtils = require("../../../utils/TypeUtils");

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const MODULAR_URL = 'https://modular.im/?utm_source=riot-web&utm_medium=web&utm_campaign=riot-web-authentication';
const FREE = 'Free';
exports.FREE = FREE;
const PREMIUM = 'Premium';
exports.PREMIUM = PREMIUM;
const ADVANCED = 'Advanced';
exports.ADVANCED = ADVANCED;
const TYPES = {
  FREE: {
    id: FREE,
    label: () => (0, _languageHandler._t)('Free'),
    logo: () => _react.default.createElement("img", {
      src: require('../../../../res/img/matrix-org-bw-logo.svg')
    }),
    description: () => (0, _languageHandler._t)('Join millions for free on the largest public server'),
    serverConfig: (0, _TypeUtils.makeType)(_AutoDiscoveryUtils.ValidatedServerConfig, {
      hsUrl: "https://matrix.piyarse.com",
      hsName: "matrix.org",
      hsNameIsDifferent: false,
      isUrl: "https://vector.im"
    })
  },
  PREMIUM: {
    id: PREMIUM,
    label: () => (0, _languageHandler._t)('Premium'),
    logo: () => _react.default.createElement("img", {
      src: require('../../../../res/img/modular-bw-logo.svg')
    }),
    description: () => (0, _languageHandler._t)('Premium hosting for organisations <a>Learn more</a>', {}, {
      a: sub => _react.default.createElement("a", {
        href: MODULAR_URL,
        target: "_blank",
        rel: "noreferrer noopener"
      }, sub)
    }),
    identityServerUrl: "https://vector.im"
  },
  ADVANCED: {
    id: ADVANCED,
    label: () => (0, _languageHandler._t)('Advanced'),
    logo: () => _react.default.createElement("div", null, _react.default.createElement("img", {
      src: require('../../../../res/img/feather-customised/globe.svg')
    }), (0, _languageHandler._t)('Other')),
    description: () => (0, _languageHandler._t)('Find other public servers or use a custom server')
  }
};
exports.TYPES = TYPES;

function getTypeFromServerConfig(config) {
  const {
    hsUrl
  } = config;

  if (!hsUrl) {
    return null;
  } else if (hsUrl === TYPES.FREE.serverConfig.hsUrl) {
    return FREE;
  } else if (new URL(hsUrl).hostname.endsWith('.modular.im')) {
    // This is an unlikely case to reach, as Modular defaults to hiding the
    // server type selector.
    return PREMIUM;
  } else {
    return ADVANCED;
  }
}

class ServerTypeSelector extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onClick", e => {
      e.stopPropagation();
      const type = e.currentTarget.dataset.id;
      this.updateSelectedType(type);
    });
    const {
      selected
    } = props;
    this.state = {
      selected
    };
  }

  updateSelectedType(type) {
    if (this.state.selected === type) {
      return;
    }

    this.setState({
      selected: type
    });

    if (this.props.onChange) {
      this.props.onChange(type);
    }
  }

  render() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const serverTypes = [];

    for (const type of Object.values(TYPES)) {
      const {
        id,
        label,
        logo,
        description
      } = type;
      const classes = (0, _classnames.default)("mx_ServerTypeSelector_type", "mx_ServerTypeSelector_type_".concat(id), {
        "mx_ServerTypeSelector_type_selected": id === this.state.selected
      });
      serverTypes.push(_react.default.createElement("div", {
        className: classes,
        key: id
      }, _react.default.createElement("div", {
        className: "mx_ServerTypeSelector_label"
      }, label()), _react.default.createElement(AccessibleButton, {
        onClick: this.onClick,
        "data-id": id
      }, _react.default.createElement("div", {
        className: "mx_ServerTypeSelector_logo"
      }, logo()), _react.default.createElement("div", {
        className: "mx_ServerTypeSelector_description"
      }, description()))));
    }

    return _react.default.createElement("div", {
      className: "mx_ServerTypeSelector"
    }, serverTypes);
  }

}

exports.default = ServerTypeSelector;
(0, _defineProperty2.default)(ServerTypeSelector, "propTypes", {
  // The default selected type.
  selected: _propTypes.default.string,
  // Handler called when the selected type changes.
  onChange: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2F1dGgvU2VydmVyVHlwZVNlbGVjdG9yLmpzIl0sIm5hbWVzIjpbIk1PRFVMQVJfVVJMIiwiRlJFRSIsIlBSRU1JVU0iLCJBRFZBTkNFRCIsIlRZUEVTIiwiaWQiLCJsYWJlbCIsImxvZ28iLCJyZXF1aXJlIiwiZGVzY3JpcHRpb24iLCJzZXJ2ZXJDb25maWciLCJWYWxpZGF0ZWRTZXJ2ZXJDb25maWciLCJoc1VybCIsImhzTmFtZSIsImhzTmFtZUlzRGlmZmVyZW50IiwiaXNVcmwiLCJhIiwic3ViIiwiaWRlbnRpdHlTZXJ2ZXJVcmwiLCJnZXRUeXBlRnJvbVNlcnZlckNvbmZpZyIsImNvbmZpZyIsIlVSTCIsImhvc3RuYW1lIiwiZW5kc1dpdGgiLCJTZXJ2ZXJUeXBlU2VsZWN0b3IiLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiZSIsInN0b3BQcm9wYWdhdGlvbiIsInR5cGUiLCJjdXJyZW50VGFyZ2V0IiwiZGF0YXNldCIsInVwZGF0ZVNlbGVjdGVkVHlwZSIsInNlbGVjdGVkIiwic3RhdGUiLCJzZXRTdGF0ZSIsIm9uQ2hhbmdlIiwicmVuZGVyIiwiQWNjZXNzaWJsZUJ1dHRvbiIsInNkayIsImdldENvbXBvbmVudCIsInNlcnZlclR5cGVzIiwiT2JqZWN0IiwidmFsdWVzIiwiY2xhc3NlcyIsInB1c2giLCJvbkNsaWNrIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiZnVuYyIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBLE1BQU1BLFdBQVcsR0FBRyw2RkFBcEI7QUFFTyxNQUFNQyxJQUFJLEdBQUcsTUFBYjs7QUFDQSxNQUFNQyxPQUFPLEdBQUcsU0FBaEI7O0FBQ0EsTUFBTUMsUUFBUSxHQUFHLFVBQWpCOztBQUVBLE1BQU1DLEtBQUssR0FBRztBQUNqQkgsRUFBQUEsSUFBSSxFQUFFO0FBQ0ZJLElBQUFBLEVBQUUsRUFBRUosSUFERjtBQUVGSyxJQUFBQSxLQUFLLEVBQUUsTUFBTSx5QkFBRyxNQUFILENBRlg7QUFHRkMsSUFBQUEsSUFBSSxFQUFFLE1BQU07QUFBSyxNQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLDRDQUFEO0FBQWpCLE1BSFY7QUFJRkMsSUFBQUEsV0FBVyxFQUFFLE1BQU0seUJBQUcscURBQUgsQ0FKakI7QUFLRkMsSUFBQUEsWUFBWSxFQUFFLHlCQUFTQyx5Q0FBVCxFQUFnQztBQUMxQ0MsTUFBQUEsS0FBSyxFQUFFLDRCQURtQztBQUUxQ0MsTUFBQUEsTUFBTSxFQUFFLFlBRmtDO0FBRzFDQyxNQUFBQSxpQkFBaUIsRUFBRSxLQUh1QjtBQUkxQ0MsTUFBQUEsS0FBSyxFQUFFO0FBSm1DLEtBQWhDO0FBTFosR0FEVztBQWFqQmIsRUFBQUEsT0FBTyxFQUFFO0FBQ0xHLElBQUFBLEVBQUUsRUFBRUgsT0FEQztBQUVMSSxJQUFBQSxLQUFLLEVBQUUsTUFBTSx5QkFBRyxTQUFILENBRlI7QUFHTEMsSUFBQUEsSUFBSSxFQUFFLE1BQU07QUFBSyxNQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLHlDQUFEO0FBQWpCLE1BSFA7QUFJTEMsSUFBQUEsV0FBVyxFQUFFLE1BQU0seUJBQUcscURBQUgsRUFBMEQsRUFBMUQsRUFBOEQ7QUFDN0VPLE1BQUFBLENBQUMsRUFBRUMsR0FBRyxJQUFJO0FBQUcsUUFBQSxJQUFJLEVBQUVqQixXQUFUO0FBQXNCLFFBQUEsTUFBTSxFQUFDLFFBQTdCO0FBQXNDLFFBQUEsR0FBRyxFQUFDO0FBQTFDLFNBQ0xpQixHQURLO0FBRG1FLEtBQTlELENBSmQ7QUFTTEMsSUFBQUEsaUJBQWlCLEVBQUU7QUFUZCxHQWJRO0FBd0JqQmYsRUFBQUEsUUFBUSxFQUFFO0FBQ05FLElBQUFBLEVBQUUsRUFBRUYsUUFERTtBQUVORyxJQUFBQSxLQUFLLEVBQUUsTUFBTSx5QkFBRyxVQUFILENBRlA7QUFHTkMsSUFBQUEsSUFBSSxFQUFFLE1BQU0sMENBQ1I7QUFBSyxNQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLGtEQUFEO0FBQWpCLE1BRFEsRUFFUCx5QkFBRyxPQUFILENBRk8sQ0FITjtBQU9OQyxJQUFBQSxXQUFXLEVBQUUsTUFBTSx5QkFBRyxrREFBSDtBQVBiO0FBeEJPLENBQWQ7OztBQW1DQSxTQUFTVSx1QkFBVCxDQUFpQ0MsTUFBakMsRUFBeUM7QUFDNUMsUUFBTTtBQUFDUixJQUFBQTtBQUFELE1BQVVRLE1BQWhCOztBQUNBLE1BQUksQ0FBQ1IsS0FBTCxFQUFZO0FBQ1IsV0FBTyxJQUFQO0FBQ0gsR0FGRCxNQUVPLElBQUlBLEtBQUssS0FBS1IsS0FBSyxDQUFDSCxJQUFOLENBQVdTLFlBQVgsQ0FBd0JFLEtBQXRDLEVBQTZDO0FBQ2hELFdBQU9YLElBQVA7QUFDSCxHQUZNLE1BRUEsSUFBSSxJQUFJb0IsR0FBSixDQUFRVCxLQUFSLEVBQWVVLFFBQWYsQ0FBd0JDLFFBQXhCLENBQWlDLGFBQWpDLENBQUosRUFBcUQ7QUFDeEQ7QUFDQTtBQUNBLFdBQU9yQixPQUFQO0FBQ0gsR0FKTSxNQUlBO0FBQ0gsV0FBT0MsUUFBUDtBQUNIO0FBQ0o7O0FBRWMsTUFBTXFCLGtCQUFOLFNBQWlDQyxlQUFNQyxhQUF2QyxDQUFxRDtBQVFoRUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsbURBd0JSQyxDQUFELElBQU87QUFDYkEsTUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0EsWUFBTUMsSUFBSSxHQUFHRixDQUFDLENBQUNHLGFBQUYsQ0FBZ0JDLE9BQWhCLENBQXdCNUIsRUFBckM7QUFDQSxXQUFLNkIsa0JBQUwsQ0FBd0JILElBQXhCO0FBQ0gsS0E1QmtCO0FBR2YsVUFBTTtBQUNGSSxNQUFBQTtBQURFLFFBRUZQLEtBRko7QUFJQSxTQUFLUSxLQUFMLEdBQWE7QUFDVEQsTUFBQUE7QUFEUyxLQUFiO0FBR0g7O0FBRURELEVBQUFBLGtCQUFrQixDQUFDSCxJQUFELEVBQU87QUFDckIsUUFBSSxLQUFLSyxLQUFMLENBQVdELFFBQVgsS0FBd0JKLElBQTVCLEVBQWtDO0FBQzlCO0FBQ0g7O0FBQ0QsU0FBS00sUUFBTCxDQUFjO0FBQ1ZGLE1BQUFBLFFBQVEsRUFBRUo7QUFEQSxLQUFkOztBQUdBLFFBQUksS0FBS0gsS0FBTCxDQUFXVSxRQUFmLEVBQXlCO0FBQ3JCLFdBQUtWLEtBQUwsQ0FBV1UsUUFBWCxDQUFvQlAsSUFBcEI7QUFDSDtBQUNKOztBQVFEUSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxnQkFBZ0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFVBQU1DLFdBQVcsR0FBRyxFQUFwQjs7QUFDQSxTQUFLLE1BQU1aLElBQVgsSUFBbUJhLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjekMsS0FBZCxDQUFuQixFQUF5QztBQUNyQyxZQUFNO0FBQUVDLFFBQUFBLEVBQUY7QUFBTUMsUUFBQUEsS0FBTjtBQUFhQyxRQUFBQSxJQUFiO0FBQW1CRSxRQUFBQTtBQUFuQixVQUFtQ3NCLElBQXpDO0FBQ0EsWUFBTWUsT0FBTyxHQUFHLHlCQUNaLDRCQURZLHVDQUVrQnpDLEVBRmxCLEdBR1o7QUFDSSwrQ0FBdUNBLEVBQUUsS0FBSyxLQUFLK0IsS0FBTCxDQUFXRDtBQUQ3RCxPQUhZLENBQWhCO0FBUUFRLE1BQUFBLFdBQVcsQ0FBQ0ksSUFBWixDQUFpQjtBQUFLLFFBQUEsU0FBUyxFQUFFRCxPQUFoQjtBQUF5QixRQUFBLEdBQUcsRUFBRXpDO0FBQTlCLFNBQ2I7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0tDLEtBQUssRUFEVixDQURhLEVBSWIsNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxPQUFPLEVBQUUsS0FBSzBDLE9BQWhDO0FBQXlDLG1CQUFTM0M7QUFBbEQsU0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDS0UsSUFBSSxFQURULENBREosRUFJSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDS0UsV0FBVyxFQURoQixDQUpKLENBSmEsQ0FBakI7QUFhSDs7QUFFRCxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNGa0MsV0FERSxDQUFQO0FBR0g7O0FBdEUrRDs7OzhCQUEvQ25CLGtCLGVBQ0U7QUFDZjtBQUNBVyxFQUFBQSxRQUFRLEVBQUVjLG1CQUFVQyxNQUZMO0FBR2Y7QUFDQVosRUFBQUEsUUFBUSxFQUFFVyxtQkFBVUUsSUFBVixDQUFlQztBQUpWLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCB7VmFsaWRhdGVkU2VydmVyQ29uZmlnfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvQXV0b0Rpc2NvdmVyeVV0aWxzXCI7XHJcbmltcG9ydCB7bWFrZVR5cGV9IGZyb20gXCIuLi8uLi8uLi91dGlscy9UeXBlVXRpbHNcIjtcclxuXHJcbmNvbnN0IE1PRFVMQVJfVVJMID0gJ2h0dHBzOi8vbW9kdWxhci5pbS8/dXRtX3NvdXJjZT1yaW90LXdlYiZ1dG1fbWVkaXVtPXdlYiZ1dG1fY2FtcGFpZ249cmlvdC13ZWItYXV0aGVudGljYXRpb24nO1xyXG5cclxuZXhwb3J0IGNvbnN0IEZSRUUgPSAnRnJlZSc7XHJcbmV4cG9ydCBjb25zdCBQUkVNSVVNID0gJ1ByZW1pdW0nO1xyXG5leHBvcnQgY29uc3QgQURWQU5DRUQgPSAnQWR2YW5jZWQnO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRZUEVTID0ge1xyXG4gICAgRlJFRToge1xyXG4gICAgICAgIGlkOiBGUkVFLFxyXG4gICAgICAgIGxhYmVsOiAoKSA9PiBfdCgnRnJlZScpLFxyXG4gICAgICAgIGxvZ286ICgpID0+IDxpbWcgc3JjPXtyZXF1aXJlKCcuLi8uLi8uLi8uLi9yZXMvaW1nL21hdHJpeC1vcmctYnctbG9nby5zdmcnKX0gLz4sXHJcbiAgICAgICAgZGVzY3JpcHRpb246ICgpID0+IF90KCdKb2luIG1pbGxpb25zIGZvciBmcmVlIG9uIHRoZSBsYXJnZXN0IHB1YmxpYyBzZXJ2ZXInKSxcclxuICAgICAgICBzZXJ2ZXJDb25maWc6IG1ha2VUeXBlKFZhbGlkYXRlZFNlcnZlckNvbmZpZywge1xyXG4gICAgICAgICAgICBoc1VybDogXCJodHRwczovL21hdHJpeC5waXlhcnNlLmNvbVwiLFxyXG4gICAgICAgICAgICBoc05hbWU6IFwibWF0cml4Lm9yZ1wiLFxyXG4gICAgICAgICAgICBoc05hbWVJc0RpZmZlcmVudDogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzVXJsOiBcImh0dHBzOi8vdmVjdG9yLmltXCIsXHJcbiAgICAgICAgfSksXHJcbiAgICB9LFxyXG4gICAgUFJFTUlVTToge1xyXG4gICAgICAgIGlkOiBQUkVNSVVNLFxyXG4gICAgICAgIGxhYmVsOiAoKSA9PiBfdCgnUHJlbWl1bScpLFxyXG4gICAgICAgIGxvZ286ICgpID0+IDxpbWcgc3JjPXtyZXF1aXJlKCcuLi8uLi8uLi8uLi9yZXMvaW1nL21vZHVsYXItYnctbG9nby5zdmcnKX0gLz4sXHJcbiAgICAgICAgZGVzY3JpcHRpb246ICgpID0+IF90KCdQcmVtaXVtIGhvc3RpbmcgZm9yIG9yZ2FuaXNhdGlvbnMgPGE+TGVhcm4gbW9yZTwvYT4nLCB7fSwge1xyXG4gICAgICAgICAgICBhOiBzdWIgPT4gPGEgaHJlZj17TU9EVUxBUl9VUkx9IHRhcmdldD1cIl9ibGFua1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIj5cclxuICAgICAgICAgICAgICAgIHtzdWJ9XHJcbiAgICAgICAgICAgIDwvYT4sXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgaWRlbnRpdHlTZXJ2ZXJVcmw6IFwiaHR0cHM6Ly92ZWN0b3IuaW1cIixcclxuICAgIH0sXHJcbiAgICBBRFZBTkNFRDoge1xyXG4gICAgICAgIGlkOiBBRFZBTkNFRCxcclxuICAgICAgICBsYWJlbDogKCkgPT4gX3QoJ0FkdmFuY2VkJyksXHJcbiAgICAgICAgbG9nbzogKCkgPT4gPGRpdj5cclxuICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoJy4uLy4uLy4uLy4uL3Jlcy9pbWcvZmVhdGhlci1jdXN0b21pc2VkL2dsb2JlLnN2ZycpfSAvPlxyXG4gICAgICAgICAgICB7X3QoJ090aGVyJyl9XHJcbiAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiAoKSA9PiBfdCgnRmluZCBvdGhlciBwdWJsaWMgc2VydmVycyBvciB1c2UgYSBjdXN0b20gc2VydmVyJyksXHJcbiAgICB9LFxyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldFR5cGVGcm9tU2VydmVyQ29uZmlnKGNvbmZpZykge1xyXG4gICAgY29uc3Qge2hzVXJsfSA9IGNvbmZpZztcclxuICAgIGlmICghaHNVcmwpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0gZWxzZSBpZiAoaHNVcmwgPT09IFRZUEVTLkZSRUUuc2VydmVyQ29uZmlnLmhzVXJsKSB7XHJcbiAgICAgICAgcmV0dXJuIEZSRUU7XHJcbiAgICB9IGVsc2UgaWYgKG5ldyBVUkwoaHNVcmwpLmhvc3RuYW1lLmVuZHNXaXRoKCcubW9kdWxhci5pbScpKSB7XHJcbiAgICAgICAgLy8gVGhpcyBpcyBhbiB1bmxpa2VseSBjYXNlIHRvIHJlYWNoLCBhcyBNb2R1bGFyIGRlZmF1bHRzIHRvIGhpZGluZyB0aGVcclxuICAgICAgICAvLyBzZXJ2ZXIgdHlwZSBzZWxlY3Rvci5cclxuICAgICAgICByZXR1cm4gUFJFTUlVTTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIEFEVkFOQ0VEO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZXJ2ZXJUeXBlU2VsZWN0b3IgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLy8gVGhlIGRlZmF1bHQgc2VsZWN0ZWQgdHlwZS5cclxuICAgICAgICBzZWxlY3RlZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICAvLyBIYW5kbGVyIGNhbGxlZCB3aGVuIHRoZSBzZWxlY3RlZCB0eXBlIGNoYW5nZXMuXHJcbiAgICAgICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICBjb25zdCB7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkLFxyXG4gICAgICAgIH0gPSBwcm9wcztcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgc2VsZWN0ZWQsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVTZWxlY3RlZFR5cGUodHlwZSkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlbGVjdGVkID09PSB0eXBlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkOiB0eXBlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uQ2hhbmdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2UodHlwZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgY29uc3QgdHlwZSA9IGUuY3VycmVudFRhcmdldC5kYXRhc2V0LmlkO1xyXG4gICAgICAgIHRoaXMudXBkYXRlU2VsZWN0ZWRUeXBlKHR5cGUpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc2VydmVyVHlwZXMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IHR5cGUgb2YgT2JqZWN0LnZhbHVlcyhUWVBFUykpIHtcclxuICAgICAgICAgICAgY29uc3QgeyBpZCwgbGFiZWwsIGxvZ28sIGRlc2NyaXB0aW9uIH0gPSB0eXBlO1xyXG4gICAgICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NuYW1lcyhcclxuICAgICAgICAgICAgICAgIFwibXhfU2VydmVyVHlwZVNlbGVjdG9yX3R5cGVcIixcclxuICAgICAgICAgICAgICAgIGBteF9TZXJ2ZXJUeXBlU2VsZWN0b3JfdHlwZV8ke2lkfWAsXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJteF9TZXJ2ZXJUeXBlU2VsZWN0b3JfdHlwZV9zZWxlY3RlZFwiOiBpZCA9PT0gdGhpcy5zdGF0ZS5zZWxlY3RlZCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBzZXJ2ZXJUeXBlcy5wdXNoKDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzfSBrZXk9e2lkfSA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NlcnZlclR5cGVTZWxlY3Rvcl9sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtsYWJlbCgpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9IGRhdGEtaWQ9e2lkfT5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NlcnZlclR5cGVTZWxlY3Rvcl9sb2dvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtsb2dvKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9TZXJ2ZXJUeXBlU2VsZWN0b3JfZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge2Rlc2NyaXB0aW9uKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2Pik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9TZXJ2ZXJUeXBlU2VsZWN0b3JcIj5cclxuICAgICAgICAgICAge3NlcnZlclR5cGVzfVxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxufVxyXG4iXX0=