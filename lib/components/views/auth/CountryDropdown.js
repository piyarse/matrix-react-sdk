"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _phonenumber = require("../../../phonenumber");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2017 Vector Creations Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const COUNTRIES_BY_ISO2 = {};

for (const c of _phonenumber.COUNTRIES) {
  COUNTRIES_BY_ISO2[c.iso2] = c;
}

function countryMatchesSearchQuery(query, country) {
  // Remove '+' if present (when searching for a prefix)
  if (query[0] === '+') {
    query = query.slice(1);
  }

  if (country.name.toUpperCase().indexOf(query.toUpperCase()) == 0) return true;
  if (country.iso2 == query.toUpperCase()) return true;
  if (country.prefix.indexOf(query) !== -1) return true;
  return false;
}

class CountryDropdown extends _react.default.Component {
  constructor(props) {
    super(props);
    this._onSearchChange = this._onSearchChange.bind(this);
    this._onOptionChange = this._onOptionChange.bind(this);
    this._getShortOption = this._getShortOption.bind(this);
    let defaultCountry = _phonenumber.COUNTRIES[0];

    const defaultCountryCode = _SdkConfig.default.get()["defaultCountryCode"];

    if (defaultCountryCode) {
      const country = _phonenumber.COUNTRIES.find(c => c.iso2 === defaultCountryCode.toUpperCase());

      if (country) defaultCountry = country;
    }

    this.state = {
      searchQuery: '',
      defaultCountry
    };
  }

  componentDidMount() {
    if (!this.props.value) {
      // If no value is given, we start with the default
      // country selected, but our parent component
      // doesn't know this, therefore we do this.
      this.props.onOptionChange(this.state.defaultCountry);
    }
  }

  _onSearchChange(search) {
    this.setState({
      searchQuery: search
    });
  }

  _onOptionChange(iso2) {
    this.props.onOptionChange(COUNTRIES_BY_ISO2[iso2]);
  }

  _flagImgForIso2(iso2) {
    return _react.default.createElement("img", {
      src: require("../../../../res/img/flags/".concat(iso2, ".png"))
    });
  }

  _getShortOption(iso2) {
    if (!this.props.isSmall) {
      return undefined;
    }

    let countryPrefix;

    if (this.props.showPrefix) {
      countryPrefix = '+' + COUNTRIES_BY_ISO2[iso2].prefix;
    }

    return _react.default.createElement("span", {
      className: "mx_CountryDropdown_shortOption"
    }, this._flagImgForIso2(iso2), countryPrefix);
  }

  render() {
    const Dropdown = sdk.getComponent('elements.Dropdown');
    let displayedCountries;

    if (this.state.searchQuery) {
      displayedCountries = _phonenumber.COUNTRIES.filter(countryMatchesSearchQuery.bind(this, this.state.searchQuery));

      if (this.state.searchQuery.length == 2 && COUNTRIES_BY_ISO2[this.state.searchQuery.toUpperCase()]) {
        // exact ISO2 country name match: make the first result the matches ISO2
        const matched = COUNTRIES_BY_ISO2[this.state.searchQuery.toUpperCase()];
        displayedCountries = displayedCountries.filter(c => {
          return c.iso2 != matched.iso2;
        });
        displayedCountries.unshift(matched);
      }
    } else {
      displayedCountries = _phonenumber.COUNTRIES;
    }

    const options = displayedCountries.map(country => {
      return _react.default.createElement("div", {
        className: "mx_CountryDropdown_option",
        key: country.iso2
      }, this._flagImgForIso2(country.iso2), country.name, " (+", country.prefix, ")");
    }); // default value here too, otherwise we need to handle null / undefined
    // values between mounting and the initial value propgating

    const value = this.props.value || this.state.defaultCountry.iso2;
    return _react.default.createElement(Dropdown, {
      id: "mx_CountryDropdown",
      className: this.props.className + " mx_CountryDropdown",
      onOptionChange: this._onOptionChange,
      onSearchChange: this._onSearchChange,
      menuWidth: 298,
      getShortOption: this._getShortOption,
      value: value,
      searchEnabled: true,
      disabled: this.props.disabled,
      label: (0, _languageHandler._t)("Country Dropdown")
    }, options);
  }

}

exports.default = CountryDropdown;
CountryDropdown.propTypes = {
  className: _propTypes.default.string,
  isSmall: _propTypes.default.bool,
  // if isSmall, show +44 in the selected value
  showPrefix: _propTypes.default.bool,
  onOptionChange: _propTypes.default.func.isRequired,
  value: _propTypes.default.string,
  disabled: _propTypes.default.bool
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2F1dGgvQ291bnRyeURyb3Bkb3duLmpzIl0sIm5hbWVzIjpbIkNPVU5UUklFU19CWV9JU08yIiwiYyIsIkNPVU5UUklFUyIsImlzbzIiLCJjb3VudHJ5TWF0Y2hlc1NlYXJjaFF1ZXJ5IiwicXVlcnkiLCJjb3VudHJ5Iiwic2xpY2UiLCJuYW1lIiwidG9VcHBlckNhc2UiLCJpbmRleE9mIiwicHJlZml4IiwiQ291bnRyeURyb3Bkb3duIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiX29uU2VhcmNoQ2hhbmdlIiwiYmluZCIsIl9vbk9wdGlvbkNoYW5nZSIsIl9nZXRTaG9ydE9wdGlvbiIsImRlZmF1bHRDb3VudHJ5IiwiZGVmYXVsdENvdW50cnlDb2RlIiwiU2RrQ29uZmlnIiwiZ2V0IiwiZmluZCIsInN0YXRlIiwic2VhcmNoUXVlcnkiLCJjb21wb25lbnREaWRNb3VudCIsInZhbHVlIiwib25PcHRpb25DaGFuZ2UiLCJzZWFyY2giLCJzZXRTdGF0ZSIsIl9mbGFnSW1nRm9ySXNvMiIsInJlcXVpcmUiLCJpc1NtYWxsIiwidW5kZWZpbmVkIiwiY291bnRyeVByZWZpeCIsInNob3dQcmVmaXgiLCJyZW5kZXIiLCJEcm9wZG93biIsInNkayIsImdldENvbXBvbmVudCIsImRpc3BsYXllZENvdW50cmllcyIsImZpbHRlciIsImxlbmd0aCIsIm1hdGNoZWQiLCJ1bnNoaWZ0Iiwib3B0aW9ucyIsIm1hcCIsImNsYXNzTmFtZSIsImRpc2FibGVkIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiYm9vbCIsImZ1bmMiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7OztBQXlCQSxNQUFNQSxpQkFBaUIsR0FBRyxFQUExQjs7QUFDQSxLQUFLLE1BQU1DLENBQVgsSUFBZ0JDLHNCQUFoQixFQUEyQjtBQUN2QkYsRUFBQUEsaUJBQWlCLENBQUNDLENBQUMsQ0FBQ0UsSUFBSCxDQUFqQixHQUE0QkYsQ0FBNUI7QUFDSDs7QUFFRCxTQUFTRyx5QkFBVCxDQUFtQ0MsS0FBbkMsRUFBMENDLE9BQTFDLEVBQW1EO0FBQy9DO0FBQ0EsTUFBSUQsS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFhLEdBQWpCLEVBQXNCO0FBQ2xCQSxJQUFBQSxLQUFLLEdBQUdBLEtBQUssQ0FBQ0UsS0FBTixDQUFZLENBQVosQ0FBUjtBQUNIOztBQUVELE1BQUlELE9BQU8sQ0FBQ0UsSUFBUixDQUFhQyxXQUFiLEdBQTJCQyxPQUEzQixDQUFtQ0wsS0FBSyxDQUFDSSxXQUFOLEVBQW5DLEtBQTJELENBQS9ELEVBQWtFLE9BQU8sSUFBUDtBQUNsRSxNQUFJSCxPQUFPLENBQUNILElBQVIsSUFBZ0JFLEtBQUssQ0FBQ0ksV0FBTixFQUFwQixFQUF5QyxPQUFPLElBQVA7QUFDekMsTUFBSUgsT0FBTyxDQUFDSyxNQUFSLENBQWVELE9BQWYsQ0FBdUJMLEtBQXZCLE1BQWtDLENBQUMsQ0FBdkMsRUFBMEMsT0FBTyxJQUFQO0FBQzFDLFNBQU8sS0FBUDtBQUNIOztBQUVjLE1BQU1PLGVBQU4sU0FBOEJDLGVBQU1DLFNBQXBDLENBQThDO0FBQ3pEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFDQSxTQUFLQyxlQUFMLEdBQXVCLEtBQUtBLGVBQUwsQ0FBcUJDLElBQXJCLENBQTBCLElBQTFCLENBQXZCO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QixLQUFLQSxlQUFMLENBQXFCRCxJQUFyQixDQUEwQixJQUExQixDQUF2QjtBQUNBLFNBQUtFLGVBQUwsR0FBdUIsS0FBS0EsZUFBTCxDQUFxQkYsSUFBckIsQ0FBMEIsSUFBMUIsQ0FBdkI7QUFFQSxRQUFJRyxjQUFjLEdBQUduQix1QkFBVSxDQUFWLENBQXJCOztBQUNBLFVBQU1vQixrQkFBa0IsR0FBR0MsbUJBQVVDLEdBQVYsR0FBZ0Isb0JBQWhCLENBQTNCOztBQUNBLFFBQUlGLGtCQUFKLEVBQXdCO0FBQ3BCLFlBQU1oQixPQUFPLEdBQUdKLHVCQUFVdUIsSUFBVixDQUFleEIsQ0FBQyxJQUFJQSxDQUFDLENBQUNFLElBQUYsS0FBV21CLGtCQUFrQixDQUFDYixXQUFuQixFQUEvQixDQUFoQjs7QUFDQSxVQUFJSCxPQUFKLEVBQWFlLGNBQWMsR0FBR2YsT0FBakI7QUFDaEI7O0FBRUQsU0FBS29CLEtBQUwsR0FBYTtBQUNUQyxNQUFBQSxXQUFXLEVBQUUsRUFESjtBQUVUTixNQUFBQTtBQUZTLEtBQWI7QUFJSDs7QUFFRE8sRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsUUFBSSxDQUFDLEtBQUtaLEtBQUwsQ0FBV2EsS0FBaEIsRUFBdUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsV0FBS2IsS0FBTCxDQUFXYyxjQUFYLENBQTBCLEtBQUtKLEtBQUwsQ0FBV0wsY0FBckM7QUFDSDtBQUNKOztBQUVESixFQUFBQSxlQUFlLENBQUNjLE1BQUQsRUFBUztBQUNwQixTQUFLQyxRQUFMLENBQWM7QUFDVkwsTUFBQUEsV0FBVyxFQUFFSTtBQURILEtBQWQ7QUFHSDs7QUFFRFosRUFBQUEsZUFBZSxDQUFDaEIsSUFBRCxFQUFPO0FBQ2xCLFNBQUthLEtBQUwsQ0FBV2MsY0FBWCxDQUEwQjlCLGlCQUFpQixDQUFDRyxJQUFELENBQTNDO0FBQ0g7O0FBRUQ4QixFQUFBQSxlQUFlLENBQUM5QixJQUFELEVBQU87QUFDbEIsV0FBTztBQUFLLE1BQUEsR0FBRyxFQUFFK0IsT0FBTyxxQ0FBOEIvQixJQUE5QjtBQUFqQixNQUFQO0FBQ0g7O0FBRURpQixFQUFBQSxlQUFlLENBQUNqQixJQUFELEVBQU87QUFDbEIsUUFBSSxDQUFDLEtBQUthLEtBQUwsQ0FBV21CLE9BQWhCLEVBQXlCO0FBQ3JCLGFBQU9DLFNBQVA7QUFDSDs7QUFDRCxRQUFJQyxhQUFKOztBQUNBLFFBQUksS0FBS3JCLEtBQUwsQ0FBV3NCLFVBQWYsRUFBMkI7QUFDdkJELE1BQUFBLGFBQWEsR0FBRyxNQUFNckMsaUJBQWlCLENBQUNHLElBQUQsQ0FBakIsQ0FBd0JRLE1BQTlDO0FBQ0g7O0FBQ0QsV0FBTztBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ0QsS0FBS3NCLGVBQUwsQ0FBcUI5QixJQUFyQixDQURDLEVBRURrQyxhQUZDLENBQVA7QUFJSDs7QUFFREUsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsUUFBUSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsbUJBQWpCLENBQWpCO0FBRUEsUUFBSUMsa0JBQUo7O0FBQ0EsUUFBSSxLQUFLakIsS0FBTCxDQUFXQyxXQUFmLEVBQTRCO0FBQ3hCZ0IsTUFBQUEsa0JBQWtCLEdBQUd6Qyx1QkFBVTBDLE1BQVYsQ0FDakJ4Qyx5QkFBeUIsQ0FBQ2MsSUFBMUIsQ0FBK0IsSUFBL0IsRUFBcUMsS0FBS1EsS0FBTCxDQUFXQyxXQUFoRCxDQURpQixDQUFyQjs7QUFHQSxVQUNJLEtBQUtELEtBQUwsQ0FBV0MsV0FBWCxDQUF1QmtCLE1BQXZCLElBQWlDLENBQWpDLElBQ0E3QyxpQkFBaUIsQ0FBQyxLQUFLMEIsS0FBTCxDQUFXQyxXQUFYLENBQXVCbEIsV0FBdkIsRUFBRCxDQUZyQixFQUdFO0FBQ0U7QUFDQSxjQUFNcUMsT0FBTyxHQUFHOUMsaUJBQWlCLENBQUMsS0FBSzBCLEtBQUwsQ0FBV0MsV0FBWCxDQUF1QmxCLFdBQXZCLEVBQUQsQ0FBakM7QUFDQWtDLFFBQUFBLGtCQUFrQixHQUFHQSxrQkFBa0IsQ0FBQ0MsTUFBbkIsQ0FBMkIzQyxDQUFELElBQU87QUFDbEQsaUJBQU9BLENBQUMsQ0FBQ0UsSUFBRixJQUFVMkMsT0FBTyxDQUFDM0MsSUFBekI7QUFDSCxTQUZvQixDQUFyQjtBQUdBd0MsUUFBQUEsa0JBQWtCLENBQUNJLE9BQW5CLENBQTJCRCxPQUEzQjtBQUNIO0FBQ0osS0FmRCxNQWVPO0FBQ0hILE1BQUFBLGtCQUFrQixHQUFHekMsc0JBQXJCO0FBQ0g7O0FBRUQsVUFBTThDLE9BQU8sR0FBR0wsa0JBQWtCLENBQUNNLEdBQW5CLENBQXdCM0MsT0FBRCxJQUFhO0FBQ2hELGFBQU87QUFBSyxRQUFBLFNBQVMsRUFBQywyQkFBZjtBQUEyQyxRQUFBLEdBQUcsRUFBRUEsT0FBTyxDQUFDSDtBQUF4RCxTQUNELEtBQUs4QixlQUFMLENBQXFCM0IsT0FBTyxDQUFDSCxJQUE3QixDQURDLEVBRURHLE9BQU8sQ0FBQ0UsSUFGUCxTQUVrQkYsT0FBTyxDQUFDSyxNQUYxQixNQUFQO0FBSUgsS0FMZSxDQUFoQixDQXZCSyxDQThCTDtBQUNBOztBQUNBLFVBQU1rQixLQUFLLEdBQUcsS0FBS2IsS0FBTCxDQUFXYSxLQUFYLElBQW9CLEtBQUtILEtBQUwsQ0FBV0wsY0FBWCxDQUEwQmxCLElBQTVEO0FBRUEsV0FBTyw2QkFBQyxRQUFEO0FBQ0gsTUFBQSxFQUFFLEVBQUMsb0JBREE7QUFFSCxNQUFBLFNBQVMsRUFBRSxLQUFLYSxLQUFMLENBQVdrQyxTQUFYLEdBQXVCLHFCQUYvQjtBQUdILE1BQUEsY0FBYyxFQUFFLEtBQUsvQixlQUhsQjtBQUlILE1BQUEsY0FBYyxFQUFFLEtBQUtGLGVBSmxCO0FBS0gsTUFBQSxTQUFTLEVBQUUsR0FMUjtBQU1ILE1BQUEsY0FBYyxFQUFFLEtBQUtHLGVBTmxCO0FBT0gsTUFBQSxLQUFLLEVBQUVTLEtBUEo7QUFRSCxNQUFBLGFBQWEsRUFBRSxJQVJaO0FBU0gsTUFBQSxRQUFRLEVBQUUsS0FBS2IsS0FBTCxDQUFXbUMsUUFUbEI7QUFVSCxNQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSDtBQVZKLE9BWURILE9BWkMsQ0FBUDtBQWNIOztBQXpHd0Q7OztBQTRHN0RwQyxlQUFlLENBQUN3QyxTQUFoQixHQUE0QjtBQUN4QkYsRUFBQUEsU0FBUyxFQUFFRyxtQkFBVUMsTUFERztBQUV4Qm5CLEVBQUFBLE9BQU8sRUFBRWtCLG1CQUFVRSxJQUZLO0FBR3hCO0FBQ0FqQixFQUFBQSxVQUFVLEVBQUVlLG1CQUFVRSxJQUpFO0FBS3hCekIsRUFBQUEsY0FBYyxFQUFFdUIsbUJBQVVHLElBQVYsQ0FBZUMsVUFMUDtBQU14QjVCLEVBQUFBLEtBQUssRUFBRXdCLG1CQUFVQyxNQU5PO0FBT3hCSCxFQUFBQSxRQUFRLEVBQUVFLG1CQUFVRTtBQVBJLENBQTVCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5cclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuXHJcbmltcG9ydCB7IENPVU5UUklFUyB9IGZyb20gJy4uLy4uLy4uL3Bob25lbnVtYmVyJztcclxuaW1wb3J0IFNka0NvbmZpZyBmcm9tIFwiLi4vLi4vLi4vU2RrQ29uZmlnXCI7XHJcbmltcG9ydCB7IF90IH0gZnJvbSBcIi4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5cclxuY29uc3QgQ09VTlRSSUVTX0JZX0lTTzIgPSB7fTtcclxuZm9yIChjb25zdCBjIG9mIENPVU5UUklFUykge1xyXG4gICAgQ09VTlRSSUVTX0JZX0lTTzJbYy5pc28yXSA9IGM7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNvdW50cnlNYXRjaGVzU2VhcmNoUXVlcnkocXVlcnksIGNvdW50cnkpIHtcclxuICAgIC8vIFJlbW92ZSAnKycgaWYgcHJlc2VudCAod2hlbiBzZWFyY2hpbmcgZm9yIGEgcHJlZml4KVxyXG4gICAgaWYgKHF1ZXJ5WzBdID09PSAnKycpIHtcclxuICAgICAgICBxdWVyeSA9IHF1ZXJ5LnNsaWNlKDEpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChjb3VudHJ5Lm5hbWUudG9VcHBlckNhc2UoKS5pbmRleE9mKHF1ZXJ5LnRvVXBwZXJDYXNlKCkpID09IDApIHJldHVybiB0cnVlO1xyXG4gICAgaWYgKGNvdW50cnkuaXNvMiA9PSBxdWVyeS50b1VwcGVyQ2FzZSgpKSByZXR1cm4gdHJ1ZTtcclxuICAgIGlmIChjb3VudHJ5LnByZWZpeC5pbmRleE9mKHF1ZXJ5KSAhPT0gLTEpIHJldHVybiB0cnVlO1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb3VudHJ5RHJvcGRvd24gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5fb25TZWFyY2hDaGFuZ2UgPSB0aGlzLl9vblNlYXJjaENoYW5nZS5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uT3B0aW9uQ2hhbmdlID0gdGhpcy5fb25PcHRpb25DaGFuZ2UuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9nZXRTaG9ydE9wdGlvbiA9IHRoaXMuX2dldFNob3J0T3B0aW9uLmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIGxldCBkZWZhdWx0Q291bnRyeSA9IENPVU5UUklFU1swXTtcclxuICAgICAgICBjb25zdCBkZWZhdWx0Q291bnRyeUNvZGUgPSBTZGtDb25maWcuZ2V0KClbXCJkZWZhdWx0Q291bnRyeUNvZGVcIl07XHJcbiAgICAgICAgaWYgKGRlZmF1bHRDb3VudHJ5Q29kZSkge1xyXG4gICAgICAgICAgICBjb25zdCBjb3VudHJ5ID0gQ09VTlRSSUVTLmZpbmQoYyA9PiBjLmlzbzIgPT09IGRlZmF1bHRDb3VudHJ5Q29kZS50b1VwcGVyQ2FzZSgpKTtcclxuICAgICAgICAgICAgaWYgKGNvdW50cnkpIGRlZmF1bHRDb3VudHJ5ID0gY291bnRyeTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHNlYXJjaFF1ZXJ5OiAnJyxcclxuICAgICAgICAgICAgZGVmYXVsdENvdW50cnksXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMudmFsdWUpIHtcclxuICAgICAgICAgICAgLy8gSWYgbm8gdmFsdWUgaXMgZ2l2ZW4sIHdlIHN0YXJ0IHdpdGggdGhlIGRlZmF1bHRcclxuICAgICAgICAgICAgLy8gY291bnRyeSBzZWxlY3RlZCwgYnV0IG91ciBwYXJlbnQgY29tcG9uZW50XHJcbiAgICAgICAgICAgIC8vIGRvZXNuJ3Qga25vdyB0aGlzLCB0aGVyZWZvcmUgd2UgZG8gdGhpcy5cclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbk9wdGlvbkNoYW5nZSh0aGlzLnN0YXRlLmRlZmF1bHRDb3VudHJ5KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2VhcmNoQ2hhbmdlKHNlYXJjaCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzZWFyY2hRdWVyeTogc2VhcmNoLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbk9wdGlvbkNoYW5nZShpc28yKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbk9wdGlvbkNoYW5nZShDT1VOVFJJRVNfQllfSVNPMltpc28yXSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2ZsYWdJbWdGb3JJc28yKGlzbzIpIHtcclxuICAgICAgICByZXR1cm4gPGltZyBzcmM9e3JlcXVpcmUoYC4uLy4uLy4uLy4uL3Jlcy9pbWcvZmxhZ3MvJHtpc28yfS5wbmdgKX0gLz47XHJcbiAgICB9XHJcblxyXG4gICAgX2dldFNob3J0T3B0aW9uKGlzbzIpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuaXNTbWFsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgY291bnRyeVByZWZpeDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93UHJlZml4KSB7XHJcbiAgICAgICAgICAgIGNvdW50cnlQcmVmaXggPSAnKycgKyBDT1VOVFJJRVNfQllfSVNPMltpc28yXS5wcmVmaXg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8c3BhbiBjbGFzc05hbWU9XCJteF9Db3VudHJ5RHJvcGRvd25fc2hvcnRPcHRpb25cIj5cclxuICAgICAgICAgICAgeyB0aGlzLl9mbGFnSW1nRm9ySXNvMihpc28yKSB9XHJcbiAgICAgICAgICAgIHsgY291bnRyeVByZWZpeCB9XHJcbiAgICAgICAgPC9zcGFuPjtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgRHJvcGRvd24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5Ecm9wZG93bicpO1xyXG5cclxuICAgICAgICBsZXQgZGlzcGxheWVkQ291bnRyaWVzO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXllZENvdW50cmllcyA9IENPVU5UUklFUy5maWx0ZXIoXHJcbiAgICAgICAgICAgICAgICBjb3VudHJ5TWF0Y2hlc1NlYXJjaFF1ZXJ5LmJpbmQodGhpcywgdGhpcy5zdGF0ZS5zZWFyY2hRdWVyeSksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuc2VhcmNoUXVlcnkubGVuZ3RoID09IDIgJiZcclxuICAgICAgICAgICAgICAgIENPVU5UUklFU19CWV9JU08yW3RoaXMuc3RhdGUuc2VhcmNoUXVlcnkudG9VcHBlckNhc2UoKV1cclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBleGFjdCBJU08yIGNvdW50cnkgbmFtZSBtYXRjaDogbWFrZSB0aGUgZmlyc3QgcmVzdWx0IHRoZSBtYXRjaGVzIElTTzJcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZWQgPSBDT1VOVFJJRVNfQllfSVNPMlt0aGlzLnN0YXRlLnNlYXJjaFF1ZXJ5LnRvVXBwZXJDYXNlKCldO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheWVkQ291bnRyaWVzID0gZGlzcGxheWVkQ291bnRyaWVzLmZpbHRlcigoYykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjLmlzbzIgIT0gbWF0Y2hlZC5pc28yO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5ZWRDb3VudHJpZXMudW5zaGlmdChtYXRjaGVkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXllZENvdW50cmllcyA9IENPVU5UUklFUztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG9wdGlvbnMgPSBkaXNwbGF5ZWRDb3VudHJpZXMubWFwKChjb3VudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cIm14X0NvdW50cnlEcm9wZG93bl9vcHRpb25cIiBrZXk9e2NvdW50cnkuaXNvMn0+XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuX2ZsYWdJbWdGb3JJc28yKGNvdW50cnkuaXNvMikgfVxyXG4gICAgICAgICAgICAgICAgeyBjb3VudHJ5Lm5hbWUgfSAoK3sgY291bnRyeS5wcmVmaXggfSlcclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBkZWZhdWx0IHZhbHVlIGhlcmUgdG9vLCBvdGhlcndpc2Ugd2UgbmVlZCB0byBoYW5kbGUgbnVsbCAvIHVuZGVmaW5lZFxyXG4gICAgICAgIC8vIHZhbHVlcyBiZXR3ZWVuIG1vdW50aW5nIGFuZCB0aGUgaW5pdGlhbCB2YWx1ZSBwcm9wZ2F0aW5nXHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLnByb3BzLnZhbHVlIHx8IHRoaXMuc3RhdGUuZGVmYXVsdENvdW50cnkuaXNvMjtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxEcm9wZG93blxyXG4gICAgICAgICAgICBpZD1cIm14X0NvdW50cnlEcm9wZG93blwiXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc05hbWUgKyBcIiBteF9Db3VudHJ5RHJvcGRvd25cIn1cclxuICAgICAgICAgICAgb25PcHRpb25DaGFuZ2U9e3RoaXMuX29uT3B0aW9uQ2hhbmdlfVxyXG4gICAgICAgICAgICBvblNlYXJjaENoYW5nZT17dGhpcy5fb25TZWFyY2hDaGFuZ2V9XHJcbiAgICAgICAgICAgIG1lbnVXaWR0aD17Mjk4fVxyXG4gICAgICAgICAgICBnZXRTaG9ydE9wdGlvbj17dGhpcy5fZ2V0U2hvcnRPcHRpb259XHJcbiAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cclxuICAgICAgICAgICAgc2VhcmNoRW5hYmxlZD17dHJ1ZX1cclxuICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMucHJvcHMuZGlzYWJsZWR9XHJcbiAgICAgICAgICAgIGxhYmVsPXtfdChcIkNvdW50cnkgRHJvcGRvd25cIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgICB7IG9wdGlvbnMgfVxyXG4gICAgICAgIDwvRHJvcGRvd24+O1xyXG4gICAgfVxyXG59XHJcblxyXG5Db3VudHJ5RHJvcGRvd24ucHJvcFR5cGVzID0ge1xyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgaXNTbWFsbDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAvLyBpZiBpc1NtYWxsLCBzaG93ICs0NCBpbiB0aGUgc2VsZWN0ZWQgdmFsdWVcclxuICAgIHNob3dQcmVmaXg6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgb25PcHRpb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxufTtcclxuIl19