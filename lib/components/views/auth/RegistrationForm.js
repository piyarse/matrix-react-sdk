"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard3(require("../../../index"));

var Email = _interopRequireWildcard3(require("../../../email"));

var _phonenumber = require("../../../phonenumber");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _languageHandler = require("../../../languageHandler");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _Registration = require("../../../Registration");

var _Validation = _interopRequireDefault(require("../elements/Validation"));

var _AutoDiscoveryUtils = require("../../../utils/AutoDiscoveryUtils");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const FIELD_EMAIL = 'field_email';
const FIELD_PHONE_NUMBER = 'field_phone_number';
const FIELD_USERNAME = 'field_username';
const FIELD_PASSWORD = 'field_password';
const FIELD_PASSWORD_CONFIRM = 'field_password_confirm';
const PASSWORD_MIN_SCORE = 3; // safely unguessable: moderate protection from offline slow-hash scenario.

/**
 * A pure UI component which displays a registration form.
 */

var _default = (0, _createReactClass.default)({
  displayName: 'RegistrationForm',
  propTypes: {
    // Values pre-filled in the input boxes when the component loads
    defaultEmail: _propTypes.default.string,
    defaultPhoneCountry: _propTypes.default.string,
    defaultPhoneNumber: _propTypes.default.string,
    defaultUsername: _propTypes.default.string,
    defaultPassword: _propTypes.default.string,
    onRegisterClick: _propTypes.default.func.isRequired,
    // onRegisterClick(Object) => ?Promise
    onEditServerDetailsClick: _propTypes.default.func,
    flows: _propTypes.default.arrayOf(_propTypes.default.object).isRequired,
    serverConfig: _propTypes.default.instanceOf(_AutoDiscoveryUtils.ValidatedServerConfig).isRequired,
    canSubmit: _propTypes.default.bool,
    serverRequiresIdServer: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      onValidationChange: console.error,
      canSubmit: true
    };
  },
  getInitialState: function () {
    return {
      // Field error codes by field ID
      fieldValid: {},
      // The ISO2 country code selected in the phone number entry
      phoneCountry: this.props.defaultPhoneCountry,
      username: this.props.defaultUsername || "",
      email: this.props.defaultEmail || "",
      phoneNumber: this.props.defaultPhoneNumber || "",
      password: this.props.defaultPassword || "",
      passwordConfirm: "",
      passwordComplexity: null,
      passwordSafe: false
    };
  },
  onSubmit: async function (ev) {
    ev.preventDefault();
    if (!this.props.canSubmit) return;
    const allFieldsValid = await this.verifyFieldsBeforeSubmit();

    if (!allFieldsValid) {
      return;
    }

    const self = this;

    if (this.state.email === '') {
      const haveIs = Boolean(this.props.serverConfig.isUrl);
      let desc;

      if (this.props.serverRequiresIdServer && !haveIs) {
        desc = (0, _languageHandler._t)("No identity server is configured so you cannot add an email address in order to " + "reset your password in the future.");
      } else {
        desc = (0, _languageHandler._t)("If you don't specify an email address, you won't be able to reset your password. " + "Are you sure?");
      }

      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      _Modal.default.createTrackedDialog('If you don\'t specify an email address...', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Warning!"),
        description: desc,
        button: (0, _languageHandler._t)("Continue"),
        onFinished: function (confirmed) {
          if (confirmed) {
            self._doSubmit(ev);
          }
        }
      });
    } else {
      self._doSubmit(ev);
    }
  },
  _doSubmit: function (ev) {
    const email = this.state.email.trim();
    const promise = this.props.onRegisterClick({
      username: this.state.username.trim(),
      password: this.state.password.trim(),
      email: email,
      phoneCountry: this.state.phoneCountry,
      phoneNumber: this.state.phoneNumber
    });

    if (promise) {
      ev.target.disabled = true;
      promise.finally(function () {
        ev.target.disabled = false;
      });
    }
  },

  async verifyFieldsBeforeSubmit() {
    // Blur the active element if any, so we first run its blur validation,
    // which is less strict than the pass we're about to do below for all fields.
    const activeElement = document.activeElement;

    if (activeElement) {
      activeElement.blur();
    }

    const fieldIDsInDisplayOrder = [FIELD_USERNAME, FIELD_PASSWORD, FIELD_PASSWORD_CONFIRM, FIELD_EMAIL, FIELD_PHONE_NUMBER]; // Run all fields with stricter validation that no longer allows empty
    // values for required fields.

    for (const fieldID of fieldIDsInDisplayOrder) {
      const field = this[fieldID];

      if (!field) {
        continue;
      } // We must wait for these validations to finish before queueing
      // up the setState below so our setState goes in the queue after
      // all the setStates from these validate calls (that's how we
      // know they've finished).


      await field.validate({
        allowEmpty: false
      });
    } // Validation and state updates are async, so we need to wait for them to complete
    // first. Queue a `setState` callback and wait for it to resolve.


    await new Promise(resolve => this.setState({}, resolve));

    if (this.allFieldsValid()) {
      return true;
    }

    const invalidField = this.findFirstInvalidField(fieldIDsInDisplayOrder);

    if (!invalidField) {
      return true;
    } // Focus the first invalid field and show feedback in the stricter mode
    // that no longer allows empty values for required fields.


    invalidField.focus();
    invalidField.validate({
      allowEmpty: false,
      focused: true
    });
    return false;
  },

  /**
   * @returns {boolean} true if all fields were valid last time they were validated.
   */
  allFieldsValid: function () {
    const keys = Object.keys(this.state.fieldValid);

    for (let i = 0; i < keys.length; ++i) {
      if (!this.state.fieldValid[keys[i]]) {
        return false;
      }
    }

    return true;
  },

  findFirstInvalidField(fieldIDs) {
    for (const fieldID of fieldIDs) {
      if (!this.state.fieldValid[fieldID] && this[fieldID]) {
        return this[fieldID];
      }
    }

    return null;
  },

  markFieldValid: function (fieldID, valid) {
    const {
      fieldValid
    } = this.state;
    fieldValid[fieldID] = valid;
    this.setState({
      fieldValid
    });
  },

  onEmailChange(ev) {
    this.setState({
      email: ev.target.value
    });
  },

  async onEmailValidate(fieldState) {
    const result = await this.validateEmailRules(fieldState);
    this.markFieldValid(FIELD_EMAIL, result.valid);
    return result;
  },

  validateEmailRules: (0, _Validation.default)({
    description: () => (0, _languageHandler._t)("Use an email address to recover your account"),
    rules: [{
      key: "required",
      test: function ({
        value,
        allowEmpty
      }) {
        return allowEmpty || !this._authStepIsRequired('m.login.email.identity') || !!value;
      },
      invalid: () => (0, _languageHandler._t)("Enter email address (required on this homeserver)")
    }, {
      key: "email",
      test: ({
        value
      }) => !value || Email.looksValid(value),
      invalid: () => (0, _languageHandler._t)("Doesn't look like a valid email address")
    }]
  }),

  onPasswordChange(ev) {
    this.setState({
      password: ev.target.value
    });
  },

  async onPasswordValidate(fieldState) {
    const result = await this.validatePasswordRules(fieldState);
    this.markFieldValid(FIELD_PASSWORD, result.valid);
    return result;
  },

  validatePasswordRules: (0, _Validation.default)({
    description: function () {
      const complexity = this.state.passwordComplexity;
      const score = complexity ? complexity.score : 0;
      return _react.default.createElement("progress", {
        className: "mx_AuthBody_passwordScore",
        max: PASSWORD_MIN_SCORE,
        value: score
      });
    },
    rules: [{
      key: "required",
      test: ({
        value,
        allowEmpty
      }) => allowEmpty || !!value,
      invalid: () => (0, _languageHandler._t)("Enter password")
    }, {
      key: "complexity",
      test: async function ({
        value
      }) {
        if (!value) {
          return false;
        }

        const {
          scorePassword
        } = await Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require('../../../utils/PasswordScorer')));
        const complexity = scorePassword(value);
        const safe = complexity.score >= PASSWORD_MIN_SCORE;

        const allowUnsafe = _SdkConfig.default.get()["dangerously_allow_unsafe_and_insecure_passwords"];

        this.setState({
          passwordComplexity: complexity,
          passwordSafe: safe
        });
        return allowUnsafe || safe;
      },
      valid: function () {
        // Unsafe passwords that are valid are only possible through a
        // configuration flag. We'll print some helper text to signal
        // to the user that their password is allowed, but unsafe.
        if (!this.state.passwordSafe) {
          return (0, _languageHandler._t)("Password is allowed, but unsafe");
        }

        return (0, _languageHandler._t)("Nice, strong password!");
      },
      invalid: function () {
        const complexity = this.state.passwordComplexity;

        if (!complexity) {
          return null;
        }

        const {
          feedback
        } = complexity;
        return feedback.warning || feedback.suggestions[0] || (0, _languageHandler._t)("Keep going...");
      }
    }]
  }),

  onPasswordConfirmChange(ev) {
    this.setState({
      passwordConfirm: ev.target.value
    });
  },

  async onPasswordConfirmValidate(fieldState) {
    const result = await this.validatePasswordConfirmRules(fieldState);
    this.markFieldValid(FIELD_PASSWORD_CONFIRM, result.valid);
    return result;
  },

  validatePasswordConfirmRules: (0, _Validation.default)({
    rules: [{
      key: "required",
      test: ({
        value,
        allowEmpty
      }) => allowEmpty || !!value,
      invalid: () => (0, _languageHandler._t)("Confirm password")
    }, {
      key: "match",
      test: function ({
        value
      }) {
        return !value || value === this.state.password;
      },
      invalid: () => (0, _languageHandler._t)("Passwords don't match")
    }]
  }),

  onPhoneCountryChange(newVal) {
    this.setState({
      phoneCountry: newVal.iso2,
      phonePrefix: newVal.prefix
    });
  },

  onPhoneNumberChange(ev) {
    this.setState({
      phoneNumber: ev.target.value
    });
  },

  async onPhoneNumberValidate(fieldState) {
    const result = await this.validatePhoneNumberRules(fieldState);
    this.markFieldValid(FIELD_PHONE_NUMBER, result.valid);
    return result;
  },

  validatePhoneNumberRules: (0, _Validation.default)({
    description: () => (0, _languageHandler._t)("Other users can invite you to rooms using your contact details"),
    rules: [{
      key: "required",
      test: function ({
        value,
        allowEmpty
      }) {
        return allowEmpty || !this._authStepIsRequired('m.login.msisdn') || !!value;
      },
      invalid: () => (0, _languageHandler._t)("Enter phone number (required on this homeserver)")
    }, {
      key: "email",
      test: ({
        value
      }) => !value || (0, _phonenumber.looksValid)(value),
      invalid: () => (0, _languageHandler._t)("Doesn't look like a valid phone number")
    }]
  }),

  onUsernameChange(ev) {
    this.setState({
      username: ev.target.value
    });
  },

  async onUsernameValidate(fieldState) {
    const result = await this.validateUsernameRules(fieldState);
    this.markFieldValid(FIELD_USERNAME, result.valid);
    return result;
  },

  validateUsernameRules: (0, _Validation.default)({
    description: () => (0, _languageHandler._t)("Use lowercase letters, numbers, dashes and underscores only"),
    rules: [{
      key: "required",
      test: ({
        value,
        allowEmpty
      }) => allowEmpty || !!value,
      invalid: () => (0, _languageHandler._t)("Enter username")
    }, {
      key: "safeLocalpart",
      test: ({
        value
      }) => !value || _Registration.SAFE_LOCALPART_REGEX.test(value),
      invalid: () => (0, _languageHandler._t)("Some characters not allowed")
    }]
  }),

  /**
   * A step is required if all flows include that step.
   *
   * @param {string} step A stage name to check
   * @returns {boolean} Whether it is required
   */
  _authStepIsRequired(step) {
    return this.props.flows.every(flow => {
      return flow.stages.includes(step);
    });
  },

  /**
   * A step is used if any flows include that step.
   *
   * @param {string} step A stage name to check
   * @returns {boolean} Whether it is used
   */
  _authStepIsUsed(step) {
    return this.props.flows.some(flow => {
      return flow.stages.includes(step);
    });
  },

  _showEmail() {
    const haveIs = Boolean(this.props.serverConfig.isUrl);

    if (this.props.serverRequiresIdServer && !haveIs || !this._authStepIsUsed('m.login.email.identity')) {
      return false;
    }

    return true;
  },

  _showPhoneNumber() {
    const threePidLogin = !_SdkConfig.default.get().disable_3pid_login;
    const haveIs = Boolean(this.props.serverConfig.isUrl);

    if (!threePidLogin || this.props.serverRequiresIdServer && !haveIs || !this._authStepIsUsed('m.login.msisdn')) {
      return false;
    }

    return true;
  },

  renderEmail() {
    if (!this._showEmail()) {
      return null;
    }

    const Field = sdk.getComponent('elements.Field');
    const emailPlaceholder = this._authStepIsRequired('m.login.email.identity') ? (0, _languageHandler._t)("Email") : (0, _languageHandler._t)("Email (optional)");
    return _react.default.createElement(Field, {
      ref: field => this[FIELD_EMAIL] = field,
      type: "text",
      label: emailPlaceholder,
      value: this.state.email,
      onChange: this.onEmailChange,
      onValidate: this.onEmailValidate
    });
  },

  renderPassword() {
    const Field = sdk.getComponent('elements.Field');
    return _react.default.createElement(Field, {
      id: "mx_RegistrationForm_password",
      ref: field => this[FIELD_PASSWORD] = field,
      type: "password",
      autoComplete: "new-password",
      label: (0, _languageHandler._t)("Password"),
      value: this.state.password,
      onChange: this.onPasswordChange,
      onValidate: this.onPasswordValidate
    });
  },

  renderPasswordConfirm() {
    const Field = sdk.getComponent('elements.Field');
    return _react.default.createElement(Field, {
      id: "mx_RegistrationForm_passwordConfirm",
      ref: field => this[FIELD_PASSWORD_CONFIRM] = field,
      type: "password",
      autoComplete: "new-password",
      label: (0, _languageHandler._t)("Confirm"),
      value: this.state.passwordConfirm,
      onChange: this.onPasswordConfirmChange,
      onValidate: this.onPasswordConfirmValidate
    });
  },

  renderPhoneNumber() {
    if (!this._showPhoneNumber()) {
      return null;
    }

    const CountryDropdown = sdk.getComponent('views.auth.CountryDropdown');
    const Field = sdk.getComponent('elements.Field');
    const phoneLabel = this._authStepIsRequired('m.login.msisdn') ? (0, _languageHandler._t)("Phone") : (0, _languageHandler._t)("Phone (optional)");

    const phoneCountry = _react.default.createElement(CountryDropdown, {
      value: this.state.phoneCountry,
      isSmall: true,
      showPrefix: true,
      onOptionChange: this.onPhoneCountryChange
    });

    return _react.default.createElement(Field, {
      ref: field => this[FIELD_PHONE_NUMBER] = field,
      type: "text",
      label: phoneLabel,
      value: this.state.phoneNumber,
      prefix: phoneCountry,
      onChange: this.onPhoneNumberChange,
      onValidate: this.onPhoneNumberValidate
    });
  },

  renderUsername() {
    const Field = sdk.getComponent('elements.Field');
    return _react.default.createElement(Field, {
      id: "mx_RegistrationForm_username",
      ref: field => this[FIELD_USERNAME] = field,
      type: "text",
      autoFocus: true,
      label: (0, _languageHandler._t)("Username"),
      value: this.state.username,
      onChange: this.onUsernameChange,
      onValidate: this.onUsernameValidate
    });
  },

  render: function () {
    let yourMatrixAccountText = (0, _languageHandler._t)('Create your Matrix account on %(serverName)s', {
      serverName: this.props.serverConfig.hsName
    });

    if (this.props.serverConfig.hsNameIsDifferent) {
      const TextWithTooltip = sdk.getComponent("elements.TextWithTooltip");
      yourMatrixAccountText = (0, _languageHandler._t)('Create your Matrix account on <underlinedServerName />', {}, {
        'underlinedServerName': () => {
          return _react.default.createElement(TextWithTooltip, {
            class: "mx_Login_underlinedServerName",
            tooltip: this.props.serverConfig.hsUrl
          }, this.props.serverConfig.hsName);
        }
      });
    }

    let editLink = null;

    if (this.props.onEditServerDetailsClick) {
      editLink = _react.default.createElement("a", {
        className: "mx_AuthBody_editServerDetails",
        href: "#",
        onClick: this.props.onEditServerDetailsClick
      }, (0, _languageHandler._t)('Change'));
    }

    const registerButton = _react.default.createElement("input", {
      className: "mx_Login_submit",
      type: "submit",
      value: (0, _languageHandler._t)("Register"),
      disabled: !this.props.canSubmit
    });

    let emailHelperText = null;

    if (this._showEmail()) {
      if (this._showPhoneNumber()) {
        emailHelperText = _react.default.createElement("div", null, (0, _languageHandler._t)("Set an email for account recovery. " + "Use email or phone to optionally be discoverable by existing contacts."));
      } else {
        emailHelperText = _react.default.createElement("div", null, (0, _languageHandler._t)("Set an email for account recovery. " + "Use email to optionally be discoverable by existing contacts."));
      }
    }

    const haveIs = Boolean(this.props.serverConfig.isUrl);
    let noIsText = null;

    if (this.props.serverRequiresIdServer && !haveIs) {
      noIsText = _react.default.createElement("div", null, (0, _languageHandler._t)("No identity server is configured so you cannot add an email address in order to " + "reset your password in the future."));
    }

    return _react.default.createElement("div", null, _react.default.createElement("h3", null, yourMatrixAccountText, editLink), _react.default.createElement("form", {
      onSubmit: this.onSubmit
    }, _react.default.createElement("div", {
      className: "mx_AuthBody_fieldRow"
    }, this.renderUsername()), _react.default.createElement("div", {
      className: "mx_AuthBody_fieldRow"
    }, this.renderPassword(), this.renderPasswordConfirm()), _react.default.createElement("div", {
      className: "mx_AuthBody_fieldRow"
    }, this.renderEmail(), this.renderPhoneNumber()), emailHelperText, noIsText, registerButton));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2F1dGgvUmVnaXN0cmF0aW9uRm9ybS5qcyJdLCJuYW1lcyI6WyJGSUVMRF9FTUFJTCIsIkZJRUxEX1BIT05FX05VTUJFUiIsIkZJRUxEX1VTRVJOQU1FIiwiRklFTERfUEFTU1dPUkQiLCJGSUVMRF9QQVNTV09SRF9DT05GSVJNIiwiUEFTU1dPUkRfTUlOX1NDT1JFIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJkZWZhdWx0RW1haWwiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJkZWZhdWx0UGhvbmVDb3VudHJ5IiwiZGVmYXVsdFBob25lTnVtYmVyIiwiZGVmYXVsdFVzZXJuYW1lIiwiZGVmYXVsdFBhc3N3b3JkIiwib25SZWdpc3RlckNsaWNrIiwiZnVuYyIsImlzUmVxdWlyZWQiLCJvbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2siLCJmbG93cyIsImFycmF5T2YiLCJvYmplY3QiLCJzZXJ2ZXJDb25maWciLCJpbnN0YW5jZU9mIiwiVmFsaWRhdGVkU2VydmVyQ29uZmlnIiwiY2FuU3VibWl0IiwiYm9vbCIsInNlcnZlclJlcXVpcmVzSWRTZXJ2ZXIiLCJnZXREZWZhdWx0UHJvcHMiLCJvblZhbGlkYXRpb25DaGFuZ2UiLCJjb25zb2xlIiwiZXJyb3IiLCJnZXRJbml0aWFsU3RhdGUiLCJmaWVsZFZhbGlkIiwicGhvbmVDb3VudHJ5IiwicHJvcHMiLCJ1c2VybmFtZSIsImVtYWlsIiwicGhvbmVOdW1iZXIiLCJwYXNzd29yZCIsInBhc3N3b3JkQ29uZmlybSIsInBhc3N3b3JkQ29tcGxleGl0eSIsInBhc3N3b3JkU2FmZSIsIm9uU3VibWl0IiwiZXYiLCJwcmV2ZW50RGVmYXVsdCIsImFsbEZpZWxkc1ZhbGlkIiwidmVyaWZ5RmllbGRzQmVmb3JlU3VibWl0Iiwic2VsZiIsInN0YXRlIiwiaGF2ZUlzIiwiQm9vbGVhbiIsImlzVXJsIiwiZGVzYyIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImJ1dHRvbiIsIm9uRmluaXNoZWQiLCJjb25maXJtZWQiLCJfZG9TdWJtaXQiLCJ0cmltIiwicHJvbWlzZSIsInRhcmdldCIsImRpc2FibGVkIiwiZmluYWxseSIsImFjdGl2ZUVsZW1lbnQiLCJkb2N1bWVudCIsImJsdXIiLCJmaWVsZElEc0luRGlzcGxheU9yZGVyIiwiZmllbGRJRCIsImZpZWxkIiwidmFsaWRhdGUiLCJhbGxvd0VtcHR5IiwiUHJvbWlzZSIsInJlc29sdmUiLCJzZXRTdGF0ZSIsImludmFsaWRGaWVsZCIsImZpbmRGaXJzdEludmFsaWRGaWVsZCIsImZvY3VzIiwiZm9jdXNlZCIsImtleXMiLCJPYmplY3QiLCJpIiwibGVuZ3RoIiwiZmllbGRJRHMiLCJtYXJrRmllbGRWYWxpZCIsInZhbGlkIiwib25FbWFpbENoYW5nZSIsInZhbHVlIiwib25FbWFpbFZhbGlkYXRlIiwiZmllbGRTdGF0ZSIsInJlc3VsdCIsInZhbGlkYXRlRW1haWxSdWxlcyIsInJ1bGVzIiwia2V5IiwidGVzdCIsIl9hdXRoU3RlcElzUmVxdWlyZWQiLCJpbnZhbGlkIiwiRW1haWwiLCJsb29rc1ZhbGlkIiwib25QYXNzd29yZENoYW5nZSIsIm9uUGFzc3dvcmRWYWxpZGF0ZSIsInZhbGlkYXRlUGFzc3dvcmRSdWxlcyIsImNvbXBsZXhpdHkiLCJzY29yZSIsInNjb3JlUGFzc3dvcmQiLCJzYWZlIiwiYWxsb3dVbnNhZmUiLCJTZGtDb25maWciLCJnZXQiLCJmZWVkYmFjayIsIndhcm5pbmciLCJzdWdnZXN0aW9ucyIsIm9uUGFzc3dvcmRDb25maXJtQ2hhbmdlIiwib25QYXNzd29yZENvbmZpcm1WYWxpZGF0ZSIsInZhbGlkYXRlUGFzc3dvcmRDb25maXJtUnVsZXMiLCJvblBob25lQ291bnRyeUNoYW5nZSIsIm5ld1ZhbCIsImlzbzIiLCJwaG9uZVByZWZpeCIsInByZWZpeCIsIm9uUGhvbmVOdW1iZXJDaGFuZ2UiLCJvblBob25lTnVtYmVyVmFsaWRhdGUiLCJ2YWxpZGF0ZVBob25lTnVtYmVyUnVsZXMiLCJvblVzZXJuYW1lQ2hhbmdlIiwib25Vc2VybmFtZVZhbGlkYXRlIiwidmFsaWRhdGVVc2VybmFtZVJ1bGVzIiwiU0FGRV9MT0NBTFBBUlRfUkVHRVgiLCJzdGVwIiwiZXZlcnkiLCJmbG93Iiwic3RhZ2VzIiwiaW5jbHVkZXMiLCJfYXV0aFN0ZXBJc1VzZWQiLCJzb21lIiwiX3Nob3dFbWFpbCIsIl9zaG93UGhvbmVOdW1iZXIiLCJ0aHJlZVBpZExvZ2luIiwiZGlzYWJsZV8zcGlkX2xvZ2luIiwicmVuZGVyRW1haWwiLCJGaWVsZCIsImVtYWlsUGxhY2Vob2xkZXIiLCJyZW5kZXJQYXNzd29yZCIsInJlbmRlclBhc3N3b3JkQ29uZmlybSIsInJlbmRlclBob25lTnVtYmVyIiwiQ291bnRyeURyb3Bkb3duIiwicGhvbmVMYWJlbCIsInJlbmRlclVzZXJuYW1lIiwicmVuZGVyIiwieW91ck1hdHJpeEFjY291bnRUZXh0Iiwic2VydmVyTmFtZSIsImhzTmFtZSIsImhzTmFtZUlzRGlmZmVyZW50IiwiVGV4dFdpdGhUb29sdGlwIiwiaHNVcmwiLCJlZGl0TGluayIsInJlZ2lzdGVyQnV0dG9uIiwiZW1haWxIZWxwZXJUZXh0Iiwibm9Jc1RleHQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBOUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQ0EsTUFBTUEsV0FBVyxHQUFHLGFBQXBCO0FBQ0EsTUFBTUMsa0JBQWtCLEdBQUcsb0JBQTNCO0FBQ0EsTUFBTUMsY0FBYyxHQUFHLGdCQUF2QjtBQUNBLE1BQU1DLGNBQWMsR0FBRyxnQkFBdkI7QUFDQSxNQUFNQyxzQkFBc0IsR0FBRyx3QkFBL0I7QUFFQSxNQUFNQyxrQkFBa0IsR0FBRyxDQUEzQixDLENBQThCOztBQUU5Qjs7OztlQUdlLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLGtCQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBQyxJQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxNQUZqQjtBQUdQQyxJQUFBQSxtQkFBbUIsRUFBRUYsbUJBQVVDLE1BSHhCO0FBSVBFLElBQUFBLGtCQUFrQixFQUFFSCxtQkFBVUMsTUFKdkI7QUFLUEcsSUFBQUEsZUFBZSxFQUFFSixtQkFBVUMsTUFMcEI7QUFNUEksSUFBQUEsZUFBZSxFQUFFTCxtQkFBVUMsTUFOcEI7QUFPUEssSUFBQUEsZUFBZSxFQUFFTixtQkFBVU8sSUFBVixDQUFlQyxVQVB6QjtBQU9xQztBQUM1Q0MsSUFBQUEsd0JBQXdCLEVBQUVULG1CQUFVTyxJQVI3QjtBQVNQRyxJQUFBQSxLQUFLLEVBQUVWLG1CQUFVVyxPQUFWLENBQWtCWCxtQkFBVVksTUFBNUIsRUFBb0NKLFVBVHBDO0FBVVBLLElBQUFBLFlBQVksRUFBRWIsbUJBQVVjLFVBQVYsQ0FBcUJDLHlDQUFyQixFQUE0Q1AsVUFWbkQ7QUFXUFEsSUFBQUEsU0FBUyxFQUFFaEIsbUJBQVVpQixJQVhkO0FBWVBDLElBQUFBLHNCQUFzQixFQUFFbEIsbUJBQVVpQjtBQVozQixHQUhpQjtBQWtCNUJFLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsa0JBQWtCLEVBQUVDLE9BQU8sQ0FBQ0MsS0FEekI7QUFFSE4sTUFBQUEsU0FBUyxFQUFFO0FBRlIsS0FBUDtBQUlILEdBdkIyQjtBQXlCNUJPLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSDtBQUNBQyxNQUFBQSxVQUFVLEVBQUUsRUFGVDtBQUdIO0FBQ0FDLE1BQUFBLFlBQVksRUFBRSxLQUFLQyxLQUFMLENBQVd4QixtQkFKdEI7QUFLSHlCLE1BQUFBLFFBQVEsRUFBRSxLQUFLRCxLQUFMLENBQVd0QixlQUFYLElBQThCLEVBTHJDO0FBTUh3QixNQUFBQSxLQUFLLEVBQUUsS0FBS0YsS0FBTCxDQUFXM0IsWUFBWCxJQUEyQixFQU4vQjtBQU9IOEIsTUFBQUEsV0FBVyxFQUFFLEtBQUtILEtBQUwsQ0FBV3ZCLGtCQUFYLElBQWlDLEVBUDNDO0FBUUgyQixNQUFBQSxRQUFRLEVBQUUsS0FBS0osS0FBTCxDQUFXckIsZUFBWCxJQUE4QixFQVJyQztBQVNIMEIsTUFBQUEsZUFBZSxFQUFFLEVBVGQ7QUFVSEMsTUFBQUEsa0JBQWtCLEVBQUUsSUFWakI7QUFXSEMsTUFBQUEsWUFBWSxFQUFFO0FBWFgsS0FBUDtBQWFILEdBdkMyQjtBQXlDNUJDLEVBQUFBLFFBQVEsRUFBRSxnQkFBZUMsRUFBZixFQUFtQjtBQUN6QkEsSUFBQUEsRUFBRSxDQUFDQyxjQUFIO0FBRUEsUUFBSSxDQUFDLEtBQUtWLEtBQUwsQ0FBV1YsU0FBaEIsRUFBMkI7QUFFM0IsVUFBTXFCLGNBQWMsR0FBRyxNQUFNLEtBQUtDLHdCQUFMLEVBQTdCOztBQUNBLFFBQUksQ0FBQ0QsY0FBTCxFQUFxQjtBQUNqQjtBQUNIOztBQUVELFVBQU1FLElBQUksR0FBRyxJQUFiOztBQUNBLFFBQUksS0FBS0MsS0FBTCxDQUFXWixLQUFYLEtBQXFCLEVBQXpCLEVBQTZCO0FBQ3pCLFlBQU1hLE1BQU0sR0FBR0MsT0FBTyxDQUFDLEtBQUtoQixLQUFMLENBQVdiLFlBQVgsQ0FBd0I4QixLQUF6QixDQUF0QjtBQUVBLFVBQUlDLElBQUo7O0FBQ0EsVUFBSSxLQUFLbEIsS0FBTCxDQUFXUixzQkFBWCxJQUFxQyxDQUFDdUIsTUFBMUMsRUFBa0Q7QUFDOUNHLFFBQUFBLElBQUksR0FBRyx5QkFDSCxxRkFDQSxvQ0FGRyxDQUFQO0FBSUgsT0FMRCxNQUtPO0FBQ0hBLFFBQUFBLElBQUksR0FBRyx5QkFDSCxzRkFDQSxlQUZHLENBQVA7QUFJSDs7QUFFRCxZQUFNQyxjQUFjLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQiwyQ0FBMUIsRUFBdUUsRUFBdkUsRUFBMkVKLGNBQTNFLEVBQTJGO0FBQ3ZGSyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsVUFBSCxDQURnRjtBQUV2RkMsUUFBQUEsV0FBVyxFQUFFUCxJQUYwRTtBQUd2RlEsUUFBQUEsTUFBTSxFQUFFLHlCQUFHLFVBQUgsQ0FIK0U7QUFJdkZDLFFBQUFBLFVBQVUsRUFBRSxVQUFTQyxTQUFULEVBQW9CO0FBQzVCLGNBQUlBLFNBQUosRUFBZTtBQUNYZixZQUFBQSxJQUFJLENBQUNnQixTQUFMLENBQWVwQixFQUFmO0FBQ0g7QUFDSjtBQVJzRixPQUEzRjtBQVVILEtBM0JELE1BMkJPO0FBQ0hJLE1BQUFBLElBQUksQ0FBQ2dCLFNBQUwsQ0FBZXBCLEVBQWY7QUFDSDtBQUNKLEdBbEYyQjtBQW9GNUJvQixFQUFBQSxTQUFTLEVBQUUsVUFBU3BCLEVBQVQsRUFBYTtBQUNwQixVQUFNUCxLQUFLLEdBQUcsS0FBS1ksS0FBTCxDQUFXWixLQUFYLENBQWlCNEIsSUFBakIsRUFBZDtBQUNBLFVBQU1DLE9BQU8sR0FBRyxLQUFLL0IsS0FBTCxDQUFXcEIsZUFBWCxDQUEyQjtBQUN2Q3FCLE1BQUFBLFFBQVEsRUFBRSxLQUFLYSxLQUFMLENBQVdiLFFBQVgsQ0FBb0I2QixJQUFwQixFQUQ2QjtBQUV2QzFCLE1BQUFBLFFBQVEsRUFBRSxLQUFLVSxLQUFMLENBQVdWLFFBQVgsQ0FBb0IwQixJQUFwQixFQUY2QjtBQUd2QzVCLE1BQUFBLEtBQUssRUFBRUEsS0FIZ0M7QUFJdkNILE1BQUFBLFlBQVksRUFBRSxLQUFLZSxLQUFMLENBQVdmLFlBSmM7QUFLdkNJLE1BQUFBLFdBQVcsRUFBRSxLQUFLVyxLQUFMLENBQVdYO0FBTGUsS0FBM0IsQ0FBaEI7O0FBUUEsUUFBSTRCLE9BQUosRUFBYTtBQUNUdEIsTUFBQUEsRUFBRSxDQUFDdUIsTUFBSCxDQUFVQyxRQUFWLEdBQXFCLElBQXJCO0FBQ0FGLE1BQUFBLE9BQU8sQ0FBQ0csT0FBUixDQUFnQixZQUFXO0FBQ3ZCekIsUUFBQUEsRUFBRSxDQUFDdUIsTUFBSCxDQUFVQyxRQUFWLEdBQXFCLEtBQXJCO0FBQ0gsT0FGRDtBQUdIO0FBQ0osR0FwRzJCOztBQXNHNUIsUUFBTXJCLHdCQUFOLEdBQWlDO0FBQzdCO0FBQ0E7QUFDQSxVQUFNdUIsYUFBYSxHQUFHQyxRQUFRLENBQUNELGFBQS9COztBQUNBLFFBQUlBLGFBQUosRUFBbUI7QUFDZkEsTUFBQUEsYUFBYSxDQUFDRSxJQUFkO0FBQ0g7O0FBRUQsVUFBTUMsc0JBQXNCLEdBQUcsQ0FDM0J2RSxjQUQyQixFQUUzQkMsY0FGMkIsRUFHM0JDLHNCQUgyQixFQUkzQkosV0FKMkIsRUFLM0JDLGtCQUwyQixDQUEvQixDQVI2QixDQWdCN0I7QUFDQTs7QUFDQSxTQUFLLE1BQU15RSxPQUFYLElBQXNCRCxzQkFBdEIsRUFBOEM7QUFDMUMsWUFBTUUsS0FBSyxHQUFHLEtBQUtELE9BQUwsQ0FBZDs7QUFDQSxVQUFJLENBQUNDLEtBQUwsRUFBWTtBQUNSO0FBQ0gsT0FKeUMsQ0FLMUM7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFlBQU1BLEtBQUssQ0FBQ0MsUUFBTixDQUFlO0FBQUVDLFFBQUFBLFVBQVUsRUFBRTtBQUFkLE9BQWYsQ0FBTjtBQUNILEtBNUI0QixDQThCN0I7QUFDQTs7O0FBQ0EsVUFBTSxJQUFJQyxPQUFKLENBQVlDLE9BQU8sSUFBSSxLQUFLQyxRQUFMLENBQWMsRUFBZCxFQUFrQkQsT0FBbEIsQ0FBdkIsQ0FBTjs7QUFFQSxRQUFJLEtBQUtqQyxjQUFMLEVBQUosRUFBMkI7QUFDdkIsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTW1DLFlBQVksR0FBRyxLQUFLQyxxQkFBTCxDQUEyQlQsc0JBQTNCLENBQXJCOztBQUVBLFFBQUksQ0FBQ1EsWUFBTCxFQUFtQjtBQUNmLGFBQU8sSUFBUDtBQUNILEtBMUM0QixDQTRDN0I7QUFDQTs7O0FBQ0FBLElBQUFBLFlBQVksQ0FBQ0UsS0FBYjtBQUNBRixJQUFBQSxZQUFZLENBQUNMLFFBQWIsQ0FBc0I7QUFBRUMsTUFBQUEsVUFBVSxFQUFFLEtBQWQ7QUFBcUJPLE1BQUFBLE9BQU8sRUFBRTtBQUE5QixLQUF0QjtBQUNBLFdBQU8sS0FBUDtBQUNILEdBdkoyQjs7QUF5SjVCOzs7QUFHQXRDLEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCLFVBQU11QyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZLEtBQUtwQyxLQUFMLENBQVdoQixVQUF2QixDQUFiOztBQUNBLFNBQUssSUFBSXNELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekIsRUFBaUMsRUFBRUQsQ0FBbkMsRUFBc0M7QUFDbEMsVUFBSSxDQUFDLEtBQUt0QyxLQUFMLENBQVdoQixVQUFYLENBQXNCb0QsSUFBSSxDQUFDRSxDQUFELENBQTFCLENBQUwsRUFBcUM7QUFDakMsZUFBTyxLQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLElBQVA7QUFDSCxHQXBLMkI7O0FBc0s1QkwsRUFBQUEscUJBQXFCLENBQUNPLFFBQUQsRUFBVztBQUM1QixTQUFLLE1BQU1mLE9BQVgsSUFBc0JlLFFBQXRCLEVBQWdDO0FBQzVCLFVBQUksQ0FBQyxLQUFLeEMsS0FBTCxDQUFXaEIsVUFBWCxDQUFzQnlDLE9BQXRCLENBQUQsSUFBbUMsS0FBS0EsT0FBTCxDQUF2QyxFQUFzRDtBQUNsRCxlQUFPLEtBQUtBLE9BQUwsQ0FBUDtBQUNIO0FBQ0o7O0FBQ0QsV0FBTyxJQUFQO0FBQ0gsR0E3SzJCOztBQStLNUJnQixFQUFBQSxjQUFjLEVBQUUsVUFBU2hCLE9BQVQsRUFBa0JpQixLQUFsQixFQUF5QjtBQUNyQyxVQUFNO0FBQUUxRCxNQUFBQTtBQUFGLFFBQWlCLEtBQUtnQixLQUE1QjtBQUNBaEIsSUFBQUEsVUFBVSxDQUFDeUMsT0FBRCxDQUFWLEdBQXNCaUIsS0FBdEI7QUFDQSxTQUFLWCxRQUFMLENBQWM7QUFDVi9DLE1BQUFBO0FBRFUsS0FBZDtBQUdILEdBckwyQjs7QUF1TDVCMkQsRUFBQUEsYUFBYSxDQUFDaEQsRUFBRCxFQUFLO0FBQ2QsU0FBS29DLFFBQUwsQ0FBYztBQUNWM0MsTUFBQUEsS0FBSyxFQUFFTyxFQUFFLENBQUN1QixNQUFILENBQVUwQjtBQURQLEtBQWQ7QUFHSCxHQTNMMkI7O0FBNkw1QixRQUFNQyxlQUFOLENBQXNCQyxVQUF0QixFQUFrQztBQUM5QixVQUFNQyxNQUFNLEdBQUcsTUFBTSxLQUFLQyxrQkFBTCxDQUF3QkYsVUFBeEIsQ0FBckI7QUFDQSxTQUFLTCxjQUFMLENBQW9CMUYsV0FBcEIsRUFBaUNnRyxNQUFNLENBQUNMLEtBQXhDO0FBQ0EsV0FBT0ssTUFBUDtBQUNILEdBak0yQjs7QUFtTTVCQyxFQUFBQSxrQkFBa0IsRUFBRSx5QkFBZTtBQUMvQnJDLElBQUFBLFdBQVcsRUFBRSxNQUFNLHlCQUFHLDhDQUFILENBRFk7QUFFL0JzQyxJQUFBQSxLQUFLLEVBQUUsQ0FDSDtBQUNJQyxNQUFBQSxHQUFHLEVBQUUsVUFEVDtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsVUFBUztBQUFFUCxRQUFBQSxLQUFGO0FBQVNoQixRQUFBQTtBQUFULE9BQVQsRUFBZ0M7QUFDbEMsZUFBT0EsVUFBVSxJQUFJLENBQUMsS0FBS3dCLG1CQUFMLENBQXlCLHdCQUF6QixDQUFmLElBQXFFLENBQUMsQ0FBQ1IsS0FBOUU7QUFDSCxPQUpMO0FBS0lTLE1BQUFBLE9BQU8sRUFBRSxNQUFNLHlCQUFHLG1EQUFIO0FBTG5CLEtBREcsRUFRSDtBQUNJSCxNQUFBQSxHQUFHLEVBQUUsT0FEVDtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsQ0FBQztBQUFFUCxRQUFBQTtBQUFGLE9BQUQsS0FBZSxDQUFDQSxLQUFELElBQVVVLEtBQUssQ0FBQ0MsVUFBTixDQUFpQlgsS0FBakIsQ0FGbkM7QUFHSVMsTUFBQUEsT0FBTyxFQUFFLE1BQU0seUJBQUcseUNBQUg7QUFIbkIsS0FSRztBQUZ3QixHQUFmLENBbk1ROztBQXFONUJHLEVBQUFBLGdCQUFnQixDQUFDN0QsRUFBRCxFQUFLO0FBQ2pCLFNBQUtvQyxRQUFMLENBQWM7QUFDVnpDLE1BQUFBLFFBQVEsRUFBRUssRUFBRSxDQUFDdUIsTUFBSCxDQUFVMEI7QUFEVixLQUFkO0FBR0gsR0F6TjJCOztBQTJONUIsUUFBTWEsa0JBQU4sQ0FBeUJYLFVBQXpCLEVBQXFDO0FBQ2pDLFVBQU1DLE1BQU0sR0FBRyxNQUFNLEtBQUtXLHFCQUFMLENBQTJCWixVQUEzQixDQUFyQjtBQUNBLFNBQUtMLGNBQUwsQ0FBb0J2RixjQUFwQixFQUFvQzZGLE1BQU0sQ0FBQ0wsS0FBM0M7QUFDQSxXQUFPSyxNQUFQO0FBQ0gsR0EvTjJCOztBQWlPNUJXLEVBQUFBLHFCQUFxQixFQUFFLHlCQUFlO0FBQ2xDL0MsSUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEIsWUFBTWdELFVBQVUsR0FBRyxLQUFLM0QsS0FBTCxDQUFXUixrQkFBOUI7QUFDQSxZQUFNb0UsS0FBSyxHQUFHRCxVQUFVLEdBQUdBLFVBQVUsQ0FBQ0MsS0FBZCxHQUFzQixDQUE5QztBQUNBLGFBQU87QUFDSCxRQUFBLFNBQVMsRUFBQywyQkFEUDtBQUVILFFBQUEsR0FBRyxFQUFFeEcsa0JBRkY7QUFHSCxRQUFBLEtBQUssRUFBRXdHO0FBSEosUUFBUDtBQUtILEtBVGlDO0FBVWxDWCxJQUFBQSxLQUFLLEVBQUUsQ0FDSDtBQUNJQyxNQUFBQSxHQUFHLEVBQUUsVUFEVDtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsQ0FBQztBQUFFUCxRQUFBQSxLQUFGO0FBQVNoQixRQUFBQTtBQUFULE9BQUQsS0FBMkJBLFVBQVUsSUFBSSxDQUFDLENBQUNnQixLQUZyRDtBQUdJUyxNQUFBQSxPQUFPLEVBQUUsTUFBTSx5QkFBRyxnQkFBSDtBQUhuQixLQURHLEVBTUg7QUFDSUgsTUFBQUEsR0FBRyxFQUFFLFlBRFQ7QUFFSUMsTUFBQUEsSUFBSSxFQUFFLGdCQUFlO0FBQUVQLFFBQUFBO0FBQUYsT0FBZixFQUEwQjtBQUM1QixZQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNSLGlCQUFPLEtBQVA7QUFDSDs7QUFDRCxjQUFNO0FBQUVpQixVQUFBQTtBQUFGLFlBQW9CLGlGQUFhLCtCQUFiLEdBQTFCO0FBQ0EsY0FBTUYsVUFBVSxHQUFHRSxhQUFhLENBQUNqQixLQUFELENBQWhDO0FBQ0EsY0FBTWtCLElBQUksR0FBR0gsVUFBVSxDQUFDQyxLQUFYLElBQW9CeEcsa0JBQWpDOztBQUNBLGNBQU0yRyxXQUFXLEdBQUdDLG1CQUFVQyxHQUFWLEdBQWdCLGlEQUFoQixDQUFwQjs7QUFDQSxhQUFLbEMsUUFBTCxDQUFjO0FBQ1Z2QyxVQUFBQSxrQkFBa0IsRUFBRW1FLFVBRFY7QUFFVmxFLFVBQUFBLFlBQVksRUFBRXFFO0FBRkosU0FBZDtBQUlBLGVBQU9DLFdBQVcsSUFBSUQsSUFBdEI7QUFDSCxPQWZMO0FBZ0JJcEIsTUFBQUEsS0FBSyxFQUFFLFlBQVc7QUFDZDtBQUNBO0FBQ0E7QUFDQSxZQUFJLENBQUMsS0FBSzFDLEtBQUwsQ0FBV1AsWUFBaEIsRUFBOEI7QUFDMUIsaUJBQU8seUJBQUcsaUNBQUgsQ0FBUDtBQUNIOztBQUNELGVBQU8seUJBQUcsd0JBQUgsQ0FBUDtBQUNILE9BeEJMO0FBeUJJNEQsTUFBQUEsT0FBTyxFQUFFLFlBQVc7QUFDaEIsY0FBTU0sVUFBVSxHQUFHLEtBQUszRCxLQUFMLENBQVdSLGtCQUE5Qjs7QUFDQSxZQUFJLENBQUNtRSxVQUFMLEVBQWlCO0FBQ2IsaUJBQU8sSUFBUDtBQUNIOztBQUNELGNBQU07QUFBRU8sVUFBQUE7QUFBRixZQUFlUCxVQUFyQjtBQUNBLGVBQU9PLFFBQVEsQ0FBQ0MsT0FBVCxJQUFvQkQsUUFBUSxDQUFDRSxXQUFULENBQXFCLENBQXJCLENBQXBCLElBQStDLHlCQUFHLGVBQUgsQ0FBdEQ7QUFDSDtBQWhDTCxLQU5HO0FBVjJCLEdBQWYsQ0FqT0s7O0FBc1I1QkMsRUFBQUEsdUJBQXVCLENBQUMxRSxFQUFELEVBQUs7QUFDeEIsU0FBS29DLFFBQUwsQ0FBYztBQUNWeEMsTUFBQUEsZUFBZSxFQUFFSSxFQUFFLENBQUN1QixNQUFILENBQVUwQjtBQURqQixLQUFkO0FBR0gsR0ExUjJCOztBQTRSNUIsUUFBTTBCLHlCQUFOLENBQWdDeEIsVUFBaEMsRUFBNEM7QUFDeEMsVUFBTUMsTUFBTSxHQUFHLE1BQU0sS0FBS3dCLDRCQUFMLENBQWtDekIsVUFBbEMsQ0FBckI7QUFDQSxTQUFLTCxjQUFMLENBQW9CdEYsc0JBQXBCLEVBQTRDNEYsTUFBTSxDQUFDTCxLQUFuRDtBQUNBLFdBQU9LLE1BQVA7QUFDSCxHQWhTMkI7O0FBa1M1QndCLEVBQUFBLDRCQUE0QixFQUFFLHlCQUFlO0FBQ3pDdEIsSUFBQUEsS0FBSyxFQUFFLENBQ0g7QUFDSUMsTUFBQUEsR0FBRyxFQUFFLFVBRFQ7QUFFSUMsTUFBQUEsSUFBSSxFQUFFLENBQUM7QUFBRVAsUUFBQUEsS0FBRjtBQUFTaEIsUUFBQUE7QUFBVCxPQUFELEtBQTJCQSxVQUFVLElBQUksQ0FBQyxDQUFDZ0IsS0FGckQ7QUFHSVMsTUFBQUEsT0FBTyxFQUFFLE1BQU0seUJBQUcsa0JBQUg7QUFIbkIsS0FERyxFQU1IO0FBQ0lILE1BQUFBLEdBQUcsRUFBRSxPQURUO0FBRUlDLE1BQUFBLElBQUksRUFBRSxVQUFTO0FBQUVQLFFBQUFBO0FBQUYsT0FBVCxFQUFvQjtBQUN0QixlQUFPLENBQUNBLEtBQUQsSUFBVUEsS0FBSyxLQUFLLEtBQUs1QyxLQUFMLENBQVdWLFFBQXRDO0FBQ0gsT0FKTDtBQUtJK0QsTUFBQUEsT0FBTyxFQUFFLE1BQU0seUJBQUcsdUJBQUg7QUFMbkIsS0FORztBQURrQyxHQUFmLENBbFNGOztBQW1UNUJtQixFQUFBQSxvQkFBb0IsQ0FBQ0MsTUFBRCxFQUFTO0FBQ3pCLFNBQUsxQyxRQUFMLENBQWM7QUFDVjlDLE1BQUFBLFlBQVksRUFBRXdGLE1BQU0sQ0FBQ0MsSUFEWDtBQUVWQyxNQUFBQSxXQUFXLEVBQUVGLE1BQU0sQ0FBQ0c7QUFGVixLQUFkO0FBSUgsR0F4VDJCOztBQTBUNUJDLEVBQUFBLG1CQUFtQixDQUFDbEYsRUFBRCxFQUFLO0FBQ3BCLFNBQUtvQyxRQUFMLENBQWM7QUFDVjFDLE1BQUFBLFdBQVcsRUFBRU0sRUFBRSxDQUFDdUIsTUFBSCxDQUFVMEI7QUFEYixLQUFkO0FBR0gsR0E5VDJCOztBQWdVNUIsUUFBTWtDLHFCQUFOLENBQTRCaEMsVUFBNUIsRUFBd0M7QUFDcEMsVUFBTUMsTUFBTSxHQUFHLE1BQU0sS0FBS2dDLHdCQUFMLENBQThCakMsVUFBOUIsQ0FBckI7QUFDQSxTQUFLTCxjQUFMLENBQW9CekYsa0JBQXBCLEVBQXdDK0YsTUFBTSxDQUFDTCxLQUEvQztBQUNBLFdBQU9LLE1BQVA7QUFDSCxHQXBVMkI7O0FBc1U1QmdDLEVBQUFBLHdCQUF3QixFQUFFLHlCQUFlO0FBQ3JDcEUsSUFBQUEsV0FBVyxFQUFFLE1BQU0seUJBQUcsZ0VBQUgsQ0FEa0I7QUFFckNzQyxJQUFBQSxLQUFLLEVBQUUsQ0FDSDtBQUNJQyxNQUFBQSxHQUFHLEVBQUUsVUFEVDtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsVUFBUztBQUFFUCxRQUFBQSxLQUFGO0FBQVNoQixRQUFBQTtBQUFULE9BQVQsRUFBZ0M7QUFDbEMsZUFBT0EsVUFBVSxJQUFJLENBQUMsS0FBS3dCLG1CQUFMLENBQXlCLGdCQUF6QixDQUFmLElBQTZELENBQUMsQ0FBQ1IsS0FBdEU7QUFDSCxPQUpMO0FBS0lTLE1BQUFBLE9BQU8sRUFBRSxNQUFNLHlCQUFHLGtEQUFIO0FBTG5CLEtBREcsRUFRSDtBQUNJSCxNQUFBQSxHQUFHLEVBQUUsT0FEVDtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsQ0FBQztBQUFFUCxRQUFBQTtBQUFGLE9BQUQsS0FBZSxDQUFDQSxLQUFELElBQVUsNkJBQXNCQSxLQUF0QixDQUZuQztBQUdJUyxNQUFBQSxPQUFPLEVBQUUsTUFBTSx5QkFBRyx3Q0FBSDtBQUhuQixLQVJHO0FBRjhCLEdBQWYsQ0F0VUU7O0FBd1Y1QjJCLEVBQUFBLGdCQUFnQixDQUFDckYsRUFBRCxFQUFLO0FBQ2pCLFNBQUtvQyxRQUFMLENBQWM7QUFDVjVDLE1BQUFBLFFBQVEsRUFBRVEsRUFBRSxDQUFDdUIsTUFBSCxDQUFVMEI7QUFEVixLQUFkO0FBR0gsR0E1VjJCOztBQThWNUIsUUFBTXFDLGtCQUFOLENBQXlCbkMsVUFBekIsRUFBcUM7QUFDakMsVUFBTUMsTUFBTSxHQUFHLE1BQU0sS0FBS21DLHFCQUFMLENBQTJCcEMsVUFBM0IsQ0FBckI7QUFDQSxTQUFLTCxjQUFMLENBQW9CeEYsY0FBcEIsRUFBb0M4RixNQUFNLENBQUNMLEtBQTNDO0FBQ0EsV0FBT0ssTUFBUDtBQUNILEdBbFcyQjs7QUFvVzVCbUMsRUFBQUEscUJBQXFCLEVBQUUseUJBQWU7QUFDbEN2RSxJQUFBQSxXQUFXLEVBQUUsTUFBTSx5QkFBRyw2REFBSCxDQURlO0FBRWxDc0MsSUFBQUEsS0FBSyxFQUFFLENBQ0g7QUFDSUMsTUFBQUEsR0FBRyxFQUFFLFVBRFQ7QUFFSUMsTUFBQUEsSUFBSSxFQUFFLENBQUM7QUFBRVAsUUFBQUEsS0FBRjtBQUFTaEIsUUFBQUE7QUFBVCxPQUFELEtBQTJCQSxVQUFVLElBQUksQ0FBQyxDQUFDZ0IsS0FGckQ7QUFHSVMsTUFBQUEsT0FBTyxFQUFFLE1BQU0seUJBQUcsZ0JBQUg7QUFIbkIsS0FERyxFQU1IO0FBQ0lILE1BQUFBLEdBQUcsRUFBRSxlQURUO0FBRUlDLE1BQUFBLElBQUksRUFBRSxDQUFDO0FBQUVQLFFBQUFBO0FBQUYsT0FBRCxLQUFlLENBQUNBLEtBQUQsSUFBVXVDLG1DQUFxQmhDLElBQXJCLENBQTBCUCxLQUExQixDQUZuQztBQUdJUyxNQUFBQSxPQUFPLEVBQUUsTUFBTSx5QkFBRyw2QkFBSDtBQUhuQixLQU5HO0FBRjJCLEdBQWYsQ0FwV0s7O0FBb1g1Qjs7Ozs7O0FBTUFELEVBQUFBLG1CQUFtQixDQUFDZ0MsSUFBRCxFQUFPO0FBQ3RCLFdBQU8sS0FBS2xHLEtBQUwsQ0FBV2hCLEtBQVgsQ0FBaUJtSCxLQUFqQixDQUF3QkMsSUFBRCxJQUFVO0FBQ3BDLGFBQU9BLElBQUksQ0FBQ0MsTUFBTCxDQUFZQyxRQUFaLENBQXFCSixJQUFyQixDQUFQO0FBQ0gsS0FGTSxDQUFQO0FBR0gsR0E5WDJCOztBQWdZNUI7Ozs7OztBQU1BSyxFQUFBQSxlQUFlLENBQUNMLElBQUQsRUFBTztBQUNsQixXQUFPLEtBQUtsRyxLQUFMLENBQVdoQixLQUFYLENBQWlCd0gsSUFBakIsQ0FBdUJKLElBQUQsSUFBVTtBQUNuQyxhQUFPQSxJQUFJLENBQUNDLE1BQUwsQ0FBWUMsUUFBWixDQUFxQkosSUFBckIsQ0FBUDtBQUNILEtBRk0sQ0FBUDtBQUdILEdBMVkyQjs7QUE0WTVCTyxFQUFBQSxVQUFVLEdBQUc7QUFDVCxVQUFNMUYsTUFBTSxHQUFHQyxPQUFPLENBQUMsS0FBS2hCLEtBQUwsQ0FBV2IsWUFBWCxDQUF3QjhCLEtBQXpCLENBQXRCOztBQUNBLFFBQ0ssS0FBS2pCLEtBQUwsQ0FBV1Isc0JBQVgsSUFBcUMsQ0FBQ3VCLE1BQXZDLElBQ0EsQ0FBQyxLQUFLd0YsZUFBTCxDQUFxQix3QkFBckIsQ0FGTCxFQUdFO0FBQ0UsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsV0FBTyxJQUFQO0FBQ0gsR0FyWjJCOztBQXVaNUJHLEVBQUFBLGdCQUFnQixHQUFHO0FBQ2YsVUFBTUMsYUFBYSxHQUFHLENBQUM3QixtQkFBVUMsR0FBVixHQUFnQjZCLGtCQUF2QztBQUNBLFVBQU03RixNQUFNLEdBQUdDLE9BQU8sQ0FBQyxLQUFLaEIsS0FBTCxDQUFXYixZQUFYLENBQXdCOEIsS0FBekIsQ0FBdEI7O0FBQ0EsUUFDSSxDQUFDMEYsYUFBRCxJQUNDLEtBQUszRyxLQUFMLENBQVdSLHNCQUFYLElBQXFDLENBQUN1QixNQUR2QyxJQUVBLENBQUMsS0FBS3dGLGVBQUwsQ0FBcUIsZ0JBQXJCLENBSEwsRUFJRTtBQUNFLGFBQU8sS0FBUDtBQUNIOztBQUNELFdBQU8sSUFBUDtBQUNILEdBbGEyQjs7QUFvYTVCTSxFQUFBQSxXQUFXLEdBQUc7QUFDVixRQUFJLENBQUMsS0FBS0osVUFBTCxFQUFMLEVBQXdCO0FBQ3BCLGFBQU8sSUFBUDtBQUNIOztBQUNELFVBQU1LLEtBQUssR0FBRzFGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBZDtBQUNBLFVBQU0wRixnQkFBZ0IsR0FBRyxLQUFLN0MsbUJBQUwsQ0FBeUIsd0JBQXpCLElBQ3JCLHlCQUFHLE9BQUgsQ0FEcUIsR0FFckIseUJBQUcsa0JBQUgsQ0FGSjtBQUdBLFdBQU8sNkJBQUMsS0FBRDtBQUNILE1BQUEsR0FBRyxFQUFFMUIsS0FBSyxJQUFJLEtBQUszRSxXQUFMLElBQW9CMkUsS0FEL0I7QUFFSCxNQUFBLElBQUksRUFBQyxNQUZGO0FBR0gsTUFBQSxLQUFLLEVBQUV1RSxnQkFISjtBQUlILE1BQUEsS0FBSyxFQUFFLEtBQUtqRyxLQUFMLENBQVdaLEtBSmY7QUFLSCxNQUFBLFFBQVEsRUFBRSxLQUFLdUQsYUFMWjtBQU1ILE1BQUEsVUFBVSxFQUFFLEtBQUtFO0FBTmQsTUFBUDtBQVFILEdBcGIyQjs7QUFzYjVCcUQsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsVUFBTUYsS0FBSyxHQUFHMUYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGdCQUFqQixDQUFkO0FBQ0EsV0FBTyw2QkFBQyxLQUFEO0FBQ0gsTUFBQSxFQUFFLEVBQUMsOEJBREE7QUFFSCxNQUFBLEdBQUcsRUFBRW1CLEtBQUssSUFBSSxLQUFLeEUsY0FBTCxJQUF1QndFLEtBRmxDO0FBR0gsTUFBQSxJQUFJLEVBQUMsVUFIRjtBQUlILE1BQUEsWUFBWSxFQUFDLGNBSlY7QUFLSCxNQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFILENBTEo7QUFNSCxNQUFBLEtBQUssRUFBRSxLQUFLMUIsS0FBTCxDQUFXVixRQU5mO0FBT0gsTUFBQSxRQUFRLEVBQUUsS0FBS2tFLGdCQVBaO0FBUUgsTUFBQSxVQUFVLEVBQUUsS0FBS0M7QUFSZCxNQUFQO0FBVUgsR0FsYzJCOztBQW9jNUIwQyxFQUFBQSxxQkFBcUIsR0FBRztBQUNwQixVQUFNSCxLQUFLLEdBQUcxRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsZ0JBQWpCLENBQWQ7QUFDQSxXQUFPLDZCQUFDLEtBQUQ7QUFDSCxNQUFBLEVBQUUsRUFBQyxxQ0FEQTtBQUVILE1BQUEsR0FBRyxFQUFFbUIsS0FBSyxJQUFJLEtBQUt2RSxzQkFBTCxJQUErQnVFLEtBRjFDO0FBR0gsTUFBQSxJQUFJLEVBQUMsVUFIRjtBQUlILE1BQUEsWUFBWSxFQUFDLGNBSlY7QUFLSCxNQUFBLEtBQUssRUFBRSx5QkFBRyxTQUFILENBTEo7QUFNSCxNQUFBLEtBQUssRUFBRSxLQUFLMUIsS0FBTCxDQUFXVCxlQU5mO0FBT0gsTUFBQSxRQUFRLEVBQUUsS0FBSzhFLHVCQVBaO0FBUUgsTUFBQSxVQUFVLEVBQUUsS0FBS0M7QUFSZCxNQUFQO0FBVUgsR0FoZDJCOztBQWtkNUI4QixFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixRQUFJLENBQUMsS0FBS1IsZ0JBQUwsRUFBTCxFQUE4QjtBQUMxQixhQUFPLElBQVA7QUFDSDs7QUFDRCxVQUFNUyxlQUFlLEdBQUcvRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNEJBQWpCLENBQXhCO0FBQ0EsVUFBTXlGLEtBQUssR0FBRzFGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBZDtBQUNBLFVBQU0rRixVQUFVLEdBQUcsS0FBS2xELG1CQUFMLENBQXlCLGdCQUF6QixJQUNmLHlCQUFHLE9BQUgsQ0FEZSxHQUVmLHlCQUFHLGtCQUFILENBRko7O0FBR0EsVUFBTW5FLFlBQVksR0FBRyw2QkFBQyxlQUFEO0FBQ2pCLE1BQUEsS0FBSyxFQUFFLEtBQUtlLEtBQUwsQ0FBV2YsWUFERDtBQUVqQixNQUFBLE9BQU8sRUFBRSxJQUZRO0FBR2pCLE1BQUEsVUFBVSxFQUFFLElBSEs7QUFJakIsTUFBQSxjQUFjLEVBQUUsS0FBS3VGO0FBSkosTUFBckI7O0FBTUEsV0FBTyw2QkFBQyxLQUFEO0FBQ0gsTUFBQSxHQUFHLEVBQUU5QyxLQUFLLElBQUksS0FBSzFFLGtCQUFMLElBQTJCMEUsS0FEdEM7QUFFSCxNQUFBLElBQUksRUFBQyxNQUZGO0FBR0gsTUFBQSxLQUFLLEVBQUU0RSxVQUhKO0FBSUgsTUFBQSxLQUFLLEVBQUUsS0FBS3RHLEtBQUwsQ0FBV1gsV0FKZjtBQUtILE1BQUEsTUFBTSxFQUFFSixZQUxMO0FBTUgsTUFBQSxRQUFRLEVBQUUsS0FBSzRGLG1CQU5aO0FBT0gsTUFBQSxVQUFVLEVBQUUsS0FBS0M7QUFQZCxNQUFQO0FBU0gsR0ExZTJCOztBQTRlNUJ5QixFQUFBQSxjQUFjLEdBQUc7QUFDYixVQUFNUCxLQUFLLEdBQUcxRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsZ0JBQWpCLENBQWQ7QUFDQSxXQUFPLDZCQUFDLEtBQUQ7QUFDSCxNQUFBLEVBQUUsRUFBQyw4QkFEQTtBQUVILE1BQUEsR0FBRyxFQUFFbUIsS0FBSyxJQUFJLEtBQUt6RSxjQUFMLElBQXVCeUUsS0FGbEM7QUFHSCxNQUFBLElBQUksRUFBQyxNQUhGO0FBSUgsTUFBQSxTQUFTLEVBQUUsSUFKUjtBQUtILE1BQUEsS0FBSyxFQUFFLHlCQUFHLFVBQUgsQ0FMSjtBQU1ILE1BQUEsS0FBSyxFQUFFLEtBQUsxQixLQUFMLENBQVdiLFFBTmY7QUFPSCxNQUFBLFFBQVEsRUFBRSxLQUFLNkYsZ0JBUFo7QUFRSCxNQUFBLFVBQVUsRUFBRSxLQUFLQztBQVJkLE1BQVA7QUFVSCxHQXhmMkI7O0FBMGY1QnVCLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsUUFBSUMscUJBQXFCLEdBQUcseUJBQUcsOENBQUgsRUFBbUQ7QUFDM0VDLE1BQUFBLFVBQVUsRUFBRSxLQUFLeEgsS0FBTCxDQUFXYixZQUFYLENBQXdCc0k7QUFEdUMsS0FBbkQsQ0FBNUI7O0FBR0EsUUFBSSxLQUFLekgsS0FBTCxDQUFXYixZQUFYLENBQXdCdUksaUJBQTVCLEVBQStDO0FBQzNDLFlBQU1DLGVBQWUsR0FBR3ZHLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBeEI7QUFFQWtHLE1BQUFBLHFCQUFxQixHQUFHLHlCQUFHLHdEQUFILEVBQTZELEVBQTdELEVBQWlFO0FBQ3JGLGdDQUF3QixNQUFNO0FBQzFCLGlCQUFPLDZCQUFDLGVBQUQ7QUFDSCxZQUFBLEtBQUssRUFBQywrQkFESDtBQUVILFlBQUEsT0FBTyxFQUFFLEtBQUt2SCxLQUFMLENBQVdiLFlBQVgsQ0FBd0J5STtBQUY5QixhQUlGLEtBQUs1SCxLQUFMLENBQVdiLFlBQVgsQ0FBd0JzSSxNQUp0QixDQUFQO0FBTUg7QUFSb0YsT0FBakUsQ0FBeEI7QUFVSDs7QUFFRCxRQUFJSSxRQUFRLEdBQUcsSUFBZjs7QUFDQSxRQUFJLEtBQUs3SCxLQUFMLENBQVdqQix3QkFBZixFQUF5QztBQUNyQzhJLE1BQUFBLFFBQVEsR0FBRztBQUFHLFFBQUEsU0FBUyxFQUFDLCtCQUFiO0FBQ1AsUUFBQSxJQUFJLEVBQUMsR0FERTtBQUNFLFFBQUEsT0FBTyxFQUFFLEtBQUs3SCxLQUFMLENBQVdqQjtBQUR0QixTQUdOLHlCQUFHLFFBQUgsQ0FITSxDQUFYO0FBS0g7O0FBRUQsVUFBTStJLGNBQWMsR0FDaEI7QUFBTyxNQUFBLFNBQVMsRUFBQyxpQkFBakI7QUFBbUMsTUFBQSxJQUFJLEVBQUMsUUFBeEM7QUFBaUQsTUFBQSxLQUFLLEVBQUUseUJBQUcsVUFBSCxDQUF4RDtBQUF3RSxNQUFBLFFBQVEsRUFBRSxDQUFDLEtBQUs5SCxLQUFMLENBQVdWO0FBQTlGLE1BREo7O0FBSUEsUUFBSXlJLGVBQWUsR0FBRyxJQUF0Qjs7QUFDQSxRQUFJLEtBQUt0QixVQUFMLEVBQUosRUFBdUI7QUFDbkIsVUFBSSxLQUFLQyxnQkFBTCxFQUFKLEVBQTZCO0FBQ3pCcUIsUUFBQUEsZUFBZSxHQUFHLDBDQUNiLHlCQUNHLHdDQUNBLHdFQUZILENBRGEsQ0FBbEI7QUFNSCxPQVBELE1BT087QUFDSEEsUUFBQUEsZUFBZSxHQUFHLDBDQUNiLHlCQUNHLHdDQUNBLCtEQUZILENBRGEsQ0FBbEI7QUFNSDtBQUNKOztBQUNELFVBQU1oSCxNQUFNLEdBQUdDLE9BQU8sQ0FBQyxLQUFLaEIsS0FBTCxDQUFXYixZQUFYLENBQXdCOEIsS0FBekIsQ0FBdEI7QUFDQSxRQUFJK0csUUFBUSxHQUFHLElBQWY7O0FBQ0EsUUFBSSxLQUFLaEksS0FBTCxDQUFXUixzQkFBWCxJQUFxQyxDQUFDdUIsTUFBMUMsRUFBa0Q7QUFDOUNpSCxNQUFBQSxRQUFRLEdBQUcsMENBQ04seUJBQ0cscUZBQ0Esb0NBRkgsQ0FETSxDQUFYO0FBTUg7O0FBRUQsV0FDSSwwQ0FDSSx5Q0FDS1QscUJBREwsRUFFS00sUUFGTCxDQURKLEVBS0k7QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLckg7QUFBckIsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLNkcsY0FBTCxFQURMLENBREosRUFJSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLTCxjQUFMLEVBREwsRUFFSyxLQUFLQyxxQkFBTCxFQUZMLENBSkosRUFRSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyxLQUFLSixXQUFMLEVBREwsRUFFSyxLQUFLSyxpQkFBTCxFQUZMLENBUkosRUFZTWEsZUFaTixFQWFNQyxRQWJOLEVBY01GLGNBZE4sQ0FMSixDQURKO0FBd0JIO0FBL2tCMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE4LCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCAqIGFzIEVtYWlsIGZyb20gJy4uLy4uLy4uL2VtYWlsJztcclxuaW1wb3J0IHsgbG9va3NWYWxpZCBhcyBwaG9uZU51bWJlckxvb2tzVmFsaWQgfSBmcm9tICcuLi8uLi8uLi9waG9uZW51bWJlcic7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi8uLi8uLi9Nb2RhbCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IFNka0NvbmZpZyBmcm9tICcuLi8uLi8uLi9TZGtDb25maWcnO1xyXG5pbXBvcnQgeyBTQUZFX0xPQ0FMUEFSVF9SRUdFWCB9IGZyb20gJy4uLy4uLy4uL1JlZ2lzdHJhdGlvbic7XHJcbmltcG9ydCB3aXRoVmFsaWRhdGlvbiBmcm9tICcuLi9lbGVtZW50cy9WYWxpZGF0aW9uJztcclxuaW1wb3J0IHtWYWxpZGF0ZWRTZXJ2ZXJDb25maWd9IGZyb20gXCIuLi8uLi8uLi91dGlscy9BdXRvRGlzY292ZXJ5VXRpbHNcIjtcclxuXHJcbmNvbnN0IEZJRUxEX0VNQUlMID0gJ2ZpZWxkX2VtYWlsJztcclxuY29uc3QgRklFTERfUEhPTkVfTlVNQkVSID0gJ2ZpZWxkX3Bob25lX251bWJlcic7XHJcbmNvbnN0IEZJRUxEX1VTRVJOQU1FID0gJ2ZpZWxkX3VzZXJuYW1lJztcclxuY29uc3QgRklFTERfUEFTU1dPUkQgPSAnZmllbGRfcGFzc3dvcmQnO1xyXG5jb25zdCBGSUVMRF9QQVNTV09SRF9DT05GSVJNID0gJ2ZpZWxkX3Bhc3N3b3JkX2NvbmZpcm0nO1xyXG5cclxuY29uc3QgUEFTU1dPUkRfTUlOX1NDT1JFID0gMzsgLy8gc2FmZWx5IHVuZ3Vlc3NhYmxlOiBtb2RlcmF0ZSBwcm90ZWN0aW9uIGZyb20gb2ZmbGluZSBzbG93LWhhc2ggc2NlbmFyaW8uXHJcblxyXG4vKipcclxuICogQSBwdXJlIFVJIGNvbXBvbmVudCB3aGljaCBkaXNwbGF5cyBhIHJlZ2lzdHJhdGlvbiBmb3JtLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1JlZ2lzdHJhdGlvbkZvcm0nLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIFZhbHVlcyBwcmUtZmlsbGVkIGluIHRoZSBpbnB1dCBib3hlcyB3aGVuIHRoZSBjb21wb25lbnQgbG9hZHNcclxuICAgICAgICBkZWZhdWx0RW1haWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZGVmYXVsdFBob25lQ291bnRyeTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBkZWZhdWx0UGhvbmVOdW1iZXI6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZGVmYXVsdFVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGRlZmF1bHRQYXNzd29yZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBvblJlZ2lzdGVyQ2xpY2s6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsIC8vIG9uUmVnaXN0ZXJDbGljayhPYmplY3QpID0+ID9Qcm9taXNlXHJcbiAgICAgICAgb25FZGl0U2VydmVyRGV0YWlsc0NsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBmbG93czogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9iamVjdCkuaXNSZXF1aXJlZCxcclxuICAgICAgICBzZXJ2ZXJDb25maWc6IFByb3BUeXBlcy5pbnN0YW5jZU9mKFZhbGlkYXRlZFNlcnZlckNvbmZpZykuaXNSZXF1aXJlZCxcclxuICAgICAgICBjYW5TdWJtaXQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIHNlcnZlclJlcXVpcmVzSWRTZXJ2ZXI6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG9uVmFsaWRhdGlvbkNoYW5nZTogY29uc29sZS5lcnJvcixcclxuICAgICAgICAgICAgY2FuU3VibWl0OiB0cnVlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgLy8gRmllbGQgZXJyb3IgY29kZXMgYnkgZmllbGQgSURcclxuICAgICAgICAgICAgZmllbGRWYWxpZDoge30sXHJcbiAgICAgICAgICAgIC8vIFRoZSBJU08yIGNvdW50cnkgY29kZSBzZWxlY3RlZCBpbiB0aGUgcGhvbmUgbnVtYmVyIGVudHJ5XHJcbiAgICAgICAgICAgIHBob25lQ291bnRyeTogdGhpcy5wcm9wcy5kZWZhdWx0UGhvbmVDb3VudHJ5LFxyXG4gICAgICAgICAgICB1c2VybmFtZTogdGhpcy5wcm9wcy5kZWZhdWx0VXNlcm5hbWUgfHwgXCJcIixcclxuICAgICAgICAgICAgZW1haWw6IHRoaXMucHJvcHMuZGVmYXVsdEVtYWlsIHx8IFwiXCIsXHJcbiAgICAgICAgICAgIHBob25lTnVtYmVyOiB0aGlzLnByb3BzLmRlZmF1bHRQaG9uZU51bWJlciB8fCBcIlwiLFxyXG4gICAgICAgICAgICBwYXNzd29yZDogdGhpcy5wcm9wcy5kZWZhdWx0UGFzc3dvcmQgfHwgXCJcIixcclxuICAgICAgICAgICAgcGFzc3dvcmRDb25maXJtOiBcIlwiLFxyXG4gICAgICAgICAgICBwYXNzd29yZENvbXBsZXhpdHk6IG51bGwsXHJcbiAgICAgICAgICAgIHBhc3N3b3JkU2FmZTogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TdWJtaXQ6IGFzeW5jIGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLmNhblN1Ym1pdCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBhbGxGaWVsZHNWYWxpZCA9IGF3YWl0IHRoaXMudmVyaWZ5RmllbGRzQmVmb3JlU3VibWl0KCk7XHJcbiAgICAgICAgaWYgKCFhbGxGaWVsZHNWYWxpZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lbWFpbCA9PT0gJycpIHtcclxuICAgICAgICAgICAgY29uc3QgaGF2ZUlzID0gQm9vbGVhbih0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5pc1VybCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgZGVzYztcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuc2VydmVyUmVxdWlyZXNJZFNlcnZlciAmJiAhaGF2ZUlzKSB7XHJcbiAgICAgICAgICAgICAgICBkZXNjID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJObyBpZGVudGl0eSBzZXJ2ZXIgaXMgY29uZmlndXJlZCBzbyB5b3UgY2Fubm90IGFkZCBhbiBlbWFpbCBhZGRyZXNzIGluIG9yZGVyIHRvIFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcInJlc2V0IHlvdXIgcGFzc3dvcmQgaW4gdGhlIGZ1dHVyZS5cIixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkZXNjID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJJZiB5b3UgZG9uJ3Qgc3BlY2lmeSBhbiBlbWFpbCBhZGRyZXNzLCB5b3Ugd29uJ3QgYmUgYWJsZSB0byByZXNldCB5b3VyIHBhc3N3b3JkLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJBcmUgeW91IHN1cmU/XCIsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdJZiB5b3UgZG9uXFwndCBzcGVjaWZ5IGFuIGVtYWlsIGFkZHJlc3MuLi4nLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIldhcm5pbmchXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGRlc2MsXHJcbiAgICAgICAgICAgICAgICBidXR0b246IF90KFwiQ29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiBmdW5jdGlvbihjb25maXJtZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuX2RvU3VibWl0KGV2KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZWxmLl9kb1N1Ym1pdChldik7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfZG9TdWJtaXQ6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgY29uc3QgZW1haWwgPSB0aGlzLnN0YXRlLmVtYWlsLnRyaW0oKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5wcm9wcy5vblJlZ2lzdGVyQ2xpY2soe1xyXG4gICAgICAgICAgICB1c2VybmFtZTogdGhpcy5zdGF0ZS51c2VybmFtZS50cmltKCksXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiB0aGlzLnN0YXRlLnBhc3N3b3JkLnRyaW0oKSxcclxuICAgICAgICAgICAgZW1haWw6IGVtYWlsLFxyXG4gICAgICAgICAgICBwaG9uZUNvdW50cnk6IHRoaXMuc3RhdGUucGhvbmVDb3VudHJ5LFxyXG4gICAgICAgICAgICBwaG9uZU51bWJlcjogdGhpcy5zdGF0ZS5waG9uZU51bWJlcixcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHByb21pc2UpIHtcclxuICAgICAgICAgICAgZXYudGFyZ2V0LmRpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgcHJvbWlzZS5maW5hbGx5KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgZXYudGFyZ2V0LmRpc2FibGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgYXN5bmMgdmVyaWZ5RmllbGRzQmVmb3JlU3VibWl0KCkge1xyXG4gICAgICAgIC8vIEJsdXIgdGhlIGFjdGl2ZSBlbGVtZW50IGlmIGFueSwgc28gd2UgZmlyc3QgcnVuIGl0cyBibHVyIHZhbGlkYXRpb24sXHJcbiAgICAgICAgLy8gd2hpY2ggaXMgbGVzcyBzdHJpY3QgdGhhbiB0aGUgcGFzcyB3ZSdyZSBhYm91dCB0byBkbyBiZWxvdyBmb3IgYWxsIGZpZWxkcy5cclxuICAgICAgICBjb25zdCBhY3RpdmVFbGVtZW50ID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudDtcclxuICAgICAgICBpZiAoYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICAgICAgICBhY3RpdmVFbGVtZW50LmJsdXIoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZpZWxkSURzSW5EaXNwbGF5T3JkZXIgPSBbXHJcbiAgICAgICAgICAgIEZJRUxEX1VTRVJOQU1FLFxyXG4gICAgICAgICAgICBGSUVMRF9QQVNTV09SRCxcclxuICAgICAgICAgICAgRklFTERfUEFTU1dPUkRfQ09ORklSTSxcclxuICAgICAgICAgICAgRklFTERfRU1BSUwsXHJcbiAgICAgICAgICAgIEZJRUxEX1BIT05FX05VTUJFUixcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICAvLyBSdW4gYWxsIGZpZWxkcyB3aXRoIHN0cmljdGVyIHZhbGlkYXRpb24gdGhhdCBubyBsb25nZXIgYWxsb3dzIGVtcHR5XHJcbiAgICAgICAgLy8gdmFsdWVzIGZvciByZXF1aXJlZCBmaWVsZHMuXHJcbiAgICAgICAgZm9yIChjb25zdCBmaWVsZElEIG9mIGZpZWxkSURzSW5EaXNwbGF5T3JkZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzW2ZpZWxkSURdO1xyXG4gICAgICAgICAgICBpZiAoIWZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBXZSBtdXN0IHdhaXQgZm9yIHRoZXNlIHZhbGlkYXRpb25zIHRvIGZpbmlzaCBiZWZvcmUgcXVldWVpbmdcclxuICAgICAgICAgICAgLy8gdXAgdGhlIHNldFN0YXRlIGJlbG93IHNvIG91ciBzZXRTdGF0ZSBnb2VzIGluIHRoZSBxdWV1ZSBhZnRlclxyXG4gICAgICAgICAgICAvLyBhbGwgdGhlIHNldFN0YXRlcyBmcm9tIHRoZXNlIHZhbGlkYXRlIGNhbGxzICh0aGF0J3MgaG93IHdlXHJcbiAgICAgICAgICAgIC8vIGtub3cgdGhleSd2ZSBmaW5pc2hlZCkuXHJcbiAgICAgICAgICAgIGF3YWl0IGZpZWxkLnZhbGlkYXRlKHsgYWxsb3dFbXB0eTogZmFsc2UgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBWYWxpZGF0aW9uIGFuZCBzdGF0ZSB1cGRhdGVzIGFyZSBhc3luYywgc28gd2UgbmVlZCB0byB3YWl0IGZvciB0aGVtIHRvIGNvbXBsZXRlXHJcbiAgICAgICAgLy8gZmlyc3QuIFF1ZXVlIGEgYHNldFN0YXRlYCBjYWxsYmFjayBhbmQgd2FpdCBmb3IgaXQgdG8gcmVzb2x2ZS5cclxuICAgICAgICBhd2FpdCBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHRoaXMuc2V0U3RhdGUoe30sIHJlc29sdmUpKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYWxsRmllbGRzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGludmFsaWRGaWVsZCA9IHRoaXMuZmluZEZpcnN0SW52YWxpZEZpZWxkKGZpZWxkSURzSW5EaXNwbGF5T3JkZXIpO1xyXG5cclxuICAgICAgICBpZiAoIWludmFsaWRGaWVsZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEZvY3VzIHRoZSBmaXJzdCBpbnZhbGlkIGZpZWxkIGFuZCBzaG93IGZlZWRiYWNrIGluIHRoZSBzdHJpY3RlciBtb2RlXHJcbiAgICAgICAgLy8gdGhhdCBubyBsb25nZXIgYWxsb3dzIGVtcHR5IHZhbHVlcyBmb3IgcmVxdWlyZWQgZmllbGRzLlxyXG4gICAgICAgIGludmFsaWRGaWVsZC5mb2N1cygpO1xyXG4gICAgICAgIGludmFsaWRGaWVsZC52YWxpZGF0ZSh7IGFsbG93RW1wdHk6IGZhbHNlLCBmb2N1c2VkOiB0cnVlIH0pO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gdHJ1ZSBpZiBhbGwgZmllbGRzIHdlcmUgdmFsaWQgbGFzdCB0aW1lIHRoZXkgd2VyZSB2YWxpZGF0ZWQuXHJcbiAgICAgKi9cclxuICAgIGFsbEZpZWxkc1ZhbGlkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBrZXlzID0gT2JqZWN0LmtleXModGhpcy5zdGF0ZS5maWVsZFZhbGlkKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmZpZWxkVmFsaWRba2V5c1tpXV0pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0sXHJcblxyXG4gICAgZmluZEZpcnN0SW52YWxpZEZpZWxkKGZpZWxkSURzKSB7XHJcbiAgICAgICAgZm9yIChjb25zdCBmaWVsZElEIG9mIGZpZWxkSURzKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZS5maWVsZFZhbGlkW2ZpZWxkSURdICYmIHRoaXNbZmllbGRJRF0pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzW2ZpZWxkSURdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBtYXJrRmllbGRWYWxpZDogZnVuY3Rpb24oZmllbGRJRCwgdmFsaWQpIHtcclxuICAgICAgICBjb25zdCB7IGZpZWxkVmFsaWQgfSA9IHRoaXMuc3RhdGU7XHJcbiAgICAgICAgZmllbGRWYWxpZFtmaWVsZElEXSA9IHZhbGlkO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBmaWVsZFZhbGlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkVtYWlsQ2hhbmdlKGV2KSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGVtYWlsOiBldi50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGFzeW5jIG9uRW1haWxWYWxpZGF0ZShmaWVsZFN0YXRlKSB7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy52YWxpZGF0ZUVtYWlsUnVsZXMoZmllbGRTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5tYXJrRmllbGRWYWxpZChGSUVMRF9FTUFJTCwgcmVzdWx0LnZhbGlkKTtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfSxcclxuXHJcbiAgICB2YWxpZGF0ZUVtYWlsUnVsZXM6IHdpdGhWYWxpZGF0aW9uKHtcclxuICAgICAgICBkZXNjcmlwdGlvbjogKCkgPT4gX3QoXCJVc2UgYW4gZW1haWwgYWRkcmVzcyB0byByZWNvdmVyIHlvdXIgYWNjb3VudFwiKSxcclxuICAgICAgICBydWxlczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwicmVxdWlyZWRcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6IGZ1bmN0aW9uKHsgdmFsdWUsIGFsbG93RW1wdHkgfSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBhbGxvd0VtcHR5IHx8ICF0aGlzLl9hdXRoU3RlcElzUmVxdWlyZWQoJ20ubG9naW4uZW1haWwuaWRlbnRpdHknKSB8fCAhIXZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICgpID0+IF90KFwiRW50ZXIgZW1haWwgYWRkcmVzcyAocmVxdWlyZWQgb24gdGhpcyBob21lc2VydmVyKVwiKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAga2V5OiBcImVtYWlsXCIsXHJcbiAgICAgICAgICAgICAgICB0ZXN0OiAoeyB2YWx1ZSB9KSA9PiAhdmFsdWUgfHwgRW1haWwubG9va3NWYWxpZCh2YWx1ZSksXHJcbiAgICAgICAgICAgICAgICBpbnZhbGlkOiAoKSA9PiBfdChcIkRvZXNuJ3QgbG9vayBsaWtlIGEgdmFsaWQgZW1haWwgYWRkcmVzc1wiKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgfSksXHJcblxyXG4gICAgb25QYXNzd29yZENoYW5nZShldikge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwYXNzd29yZDogZXYudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBhc3luYyBvblBhc3N3b3JkVmFsaWRhdGUoZmllbGRTdGF0ZSkge1xyXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMudmFsaWRhdGVQYXNzd29yZFJ1bGVzKGZpZWxkU3RhdGUpO1xyXG4gICAgICAgIHRoaXMubWFya0ZpZWxkVmFsaWQoRklFTERfUEFTU1dPUkQsIHJlc3VsdC52YWxpZCk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH0sXHJcblxyXG4gICAgdmFsaWRhdGVQYXNzd29yZFJ1bGVzOiB3aXRoVmFsaWRhdGlvbih7XHJcbiAgICAgICAgZGVzY3JpcHRpb246IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBjb25zdCBjb21wbGV4aXR5ID0gdGhpcy5zdGF0ZS5wYXNzd29yZENvbXBsZXhpdHk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNjb3JlID0gY29tcGxleGl0eSA/IGNvbXBsZXhpdHkuc2NvcmUgOiAwO1xyXG4gICAgICAgICAgICByZXR1cm4gPHByb2dyZXNzXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9wYXNzd29yZFNjb3JlXCJcclxuICAgICAgICAgICAgICAgIG1heD17UEFTU1dPUkRfTUlOX1NDT1JFfVxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e3Njb3JlfVxyXG4gICAgICAgICAgICAvPjtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJ1bGVzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGtleTogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgdGVzdDogKHsgdmFsdWUsIGFsbG93RW1wdHkgfSkgPT4gYWxsb3dFbXB0eSB8fCAhIXZhbHVlLFxyXG4gICAgICAgICAgICAgICAgaW52YWxpZDogKCkgPT4gX3QoXCJFbnRlciBwYXNzd29yZFwiKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAga2V5OiBcImNvbXBsZXhpdHlcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6IGFzeW5jIGZ1bmN0aW9uKHsgdmFsdWUgfSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB7IHNjb3JlUGFzc3dvcmQgfSA9IGF3YWl0IGltcG9ydCgnLi4vLi4vLi4vdXRpbHMvUGFzc3dvcmRTY29yZXInKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb21wbGV4aXR5ID0gc2NvcmVQYXNzd29yZCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2FmZSA9IGNvbXBsZXhpdHkuc2NvcmUgPj0gUEFTU1dPUkRfTUlOX1NDT1JFO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFsbG93VW5zYWZlID0gU2RrQ29uZmlnLmdldCgpW1wiZGFuZ2Vyb3VzbHlfYWxsb3dfdW5zYWZlX2FuZF9pbnNlY3VyZV9wYXNzd29yZHNcIl07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhc3N3b3JkQ29tcGxleGl0eTogY29tcGxleGl0eSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFzc3dvcmRTYWZlOiBzYWZlLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBhbGxvd1Vuc2FmZSB8fCBzYWZlO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHZhbGlkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBVbnNhZmUgcGFzc3dvcmRzIHRoYXQgYXJlIHZhbGlkIGFyZSBvbmx5IHBvc3NpYmxlIHRocm91Z2ggYVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbmZpZ3VyYXRpb24gZmxhZy4gV2UnbGwgcHJpbnQgc29tZSBoZWxwZXIgdGV4dCB0byBzaWduYWxcclxuICAgICAgICAgICAgICAgICAgICAvLyB0byB0aGUgdXNlciB0aGF0IHRoZWlyIHBhc3N3b3JkIGlzIGFsbG93ZWQsIGJ1dCB1bnNhZmUuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnBhc3N3b3JkU2FmZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoXCJQYXNzd29yZCBpcyBhbGxvd2VkLCBidXQgdW5zYWZlXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoXCJOaWNlLCBzdHJvbmcgcGFzc3dvcmQhXCIpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbXBsZXhpdHkgPSB0aGlzLnN0YXRlLnBhc3N3b3JkQ29tcGxleGl0eTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWNvbXBsZXhpdHkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHsgZmVlZGJhY2sgfSA9IGNvbXBsZXhpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZlZWRiYWNrLndhcm5pbmcgfHwgZmVlZGJhY2suc3VnZ2VzdGlvbnNbMF0gfHwgX3QoXCJLZWVwIGdvaW5nLi4uXCIpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgfSksXHJcblxyXG4gICAgb25QYXNzd29yZENvbmZpcm1DaGFuZ2UoZXYpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGFzc3dvcmRDb25maXJtOiBldi50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGFzeW5jIG9uUGFzc3dvcmRDb25maXJtVmFsaWRhdGUoZmllbGRTdGF0ZSkge1xyXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMudmFsaWRhdGVQYXNzd29yZENvbmZpcm1SdWxlcyhmaWVsZFN0YXRlKTtcclxuICAgICAgICB0aGlzLm1hcmtGaWVsZFZhbGlkKEZJRUxEX1BBU1NXT1JEX0NPTkZJUk0sIHJlc3VsdC52YWxpZCk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH0sXHJcblxyXG4gICAgdmFsaWRhdGVQYXNzd29yZENvbmZpcm1SdWxlczogd2l0aFZhbGlkYXRpb24oe1xyXG4gICAgICAgIHJ1bGVzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGtleTogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgdGVzdDogKHsgdmFsdWUsIGFsbG93RW1wdHkgfSkgPT4gYWxsb3dFbXB0eSB8fCAhIXZhbHVlLFxyXG4gICAgICAgICAgICAgICAgaW52YWxpZDogKCkgPT4gX3QoXCJDb25maXJtIHBhc3N3b3JkXCIpLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwibWF0Y2hcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6IGZ1bmN0aW9uKHsgdmFsdWUgfSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAhdmFsdWUgfHwgdmFsdWUgPT09IHRoaXMuc3RhdGUucGFzc3dvcmQ7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgaW52YWxpZDogKCkgPT4gX3QoXCJQYXNzd29yZHMgZG9uJ3QgbWF0Y2hcIiksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgIF0sXHJcbiAgICB9KSxcclxuXHJcbiAgICBvblBob25lQ291bnRyeUNoYW5nZShuZXdWYWwpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhvbmVDb3VudHJ5OiBuZXdWYWwuaXNvMixcclxuICAgICAgICAgICAgcGhvbmVQcmVmaXg6IG5ld1ZhbC5wcmVmaXgsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUGhvbmVOdW1iZXJDaGFuZ2UoZXYpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhvbmVOdW1iZXI6IGV2LnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgYXN5bmMgb25QaG9uZU51bWJlclZhbGlkYXRlKGZpZWxkU3RhdGUpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLnZhbGlkYXRlUGhvbmVOdW1iZXJSdWxlcyhmaWVsZFN0YXRlKTtcclxuICAgICAgICB0aGlzLm1hcmtGaWVsZFZhbGlkKEZJRUxEX1BIT05FX05VTUJFUiwgcmVzdWx0LnZhbGlkKTtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfSxcclxuXHJcbiAgICB2YWxpZGF0ZVBob25lTnVtYmVyUnVsZXM6IHdpdGhWYWxpZGF0aW9uKHtcclxuICAgICAgICBkZXNjcmlwdGlvbjogKCkgPT4gX3QoXCJPdGhlciB1c2VycyBjYW4gaW52aXRlIHlvdSB0byByb29tcyB1c2luZyB5b3VyIGNvbnRhY3QgZGV0YWlsc1wiKSxcclxuICAgICAgICBydWxlczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXk6IFwicmVxdWlyZWRcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6IGZ1bmN0aW9uKHsgdmFsdWUsIGFsbG93RW1wdHkgfSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBhbGxvd0VtcHR5IHx8ICF0aGlzLl9hdXRoU3RlcElzUmVxdWlyZWQoJ20ubG9naW4ubXNpc2RuJykgfHwgISF2YWx1ZTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBpbnZhbGlkOiAoKSA9PiBfdChcIkVudGVyIHBob25lIG51bWJlciAocmVxdWlyZWQgb24gdGhpcyBob21lc2VydmVyKVwiKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAga2V5OiBcImVtYWlsXCIsXHJcbiAgICAgICAgICAgICAgICB0ZXN0OiAoeyB2YWx1ZSB9KSA9PiAhdmFsdWUgfHwgcGhvbmVOdW1iZXJMb29rc1ZhbGlkKHZhbHVlKSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICgpID0+IF90KFwiRG9lc24ndCBsb29rIGxpa2UgYSB2YWxpZCBwaG9uZSBudW1iZXJcIiksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgIH0pLFxyXG5cclxuICAgIG9uVXNlcm5hbWVDaGFuZ2UoZXYpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IGV2LnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgYXN5bmMgb25Vc2VybmFtZVZhbGlkYXRlKGZpZWxkU3RhdGUpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLnZhbGlkYXRlVXNlcm5hbWVSdWxlcyhmaWVsZFN0YXRlKTtcclxuICAgICAgICB0aGlzLm1hcmtGaWVsZFZhbGlkKEZJRUxEX1VTRVJOQU1FLCByZXN1bHQudmFsaWQpO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9LFxyXG5cclxuICAgIHZhbGlkYXRlVXNlcm5hbWVSdWxlczogd2l0aFZhbGlkYXRpb24oe1xyXG4gICAgICAgIGRlc2NyaXB0aW9uOiAoKSA9PiBfdChcIlVzZSBsb3dlcmNhc2UgbGV0dGVycywgbnVtYmVycywgZGFzaGVzIGFuZCB1bmRlcnNjb3JlcyBvbmx5XCIpLFxyXG4gICAgICAgIHJ1bGVzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGtleTogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgdGVzdDogKHsgdmFsdWUsIGFsbG93RW1wdHkgfSkgPT4gYWxsb3dFbXB0eSB8fCAhIXZhbHVlLFxyXG4gICAgICAgICAgICAgICAgaW52YWxpZDogKCkgPT4gX3QoXCJFbnRlciB1c2VybmFtZVwiKSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAga2V5OiBcInNhZmVMb2NhbHBhcnRcIixcclxuICAgICAgICAgICAgICAgIHRlc3Q6ICh7IHZhbHVlIH0pID0+ICF2YWx1ZSB8fCBTQUZFX0xPQ0FMUEFSVF9SRUdFWC50ZXN0KHZhbHVlKSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICgpID0+IF90KFwiU29tZSBjaGFyYWN0ZXJzIG5vdCBhbGxvd2VkXCIpLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICB9KSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgc3RlcCBpcyByZXF1aXJlZCBpZiBhbGwgZmxvd3MgaW5jbHVkZSB0aGF0IHN0ZXAuXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHN0ZXAgQSBzdGFnZSBuYW1lIHRvIGNoZWNrXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gV2hldGhlciBpdCBpcyByZXF1aXJlZFxyXG4gICAgICovXHJcbiAgICBfYXV0aFN0ZXBJc1JlcXVpcmVkKHN0ZXApIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5mbG93cy5ldmVyeSgoZmxvdykgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZmxvdy5zdGFnZXMuaW5jbHVkZXMoc3RlcCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBzdGVwIGlzIHVzZWQgaWYgYW55IGZsb3dzIGluY2x1ZGUgdGhhdCBzdGVwLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzdGVwIEEgc3RhZ2UgbmFtZSB0byBjaGVja1xyXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59IFdoZXRoZXIgaXQgaXMgdXNlZFxyXG4gICAgICovXHJcbiAgICBfYXV0aFN0ZXBJc1VzZWQoc3RlcCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLmZsb3dzLnNvbWUoKGZsb3cpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGZsb3cuc3RhZ2VzLmluY2x1ZGVzKHN0ZXApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvd0VtYWlsKCkge1xyXG4gICAgICAgIGNvbnN0IGhhdmVJcyA9IEJvb2xlYW4odGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmwpO1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgKHRoaXMucHJvcHMuc2VydmVyUmVxdWlyZXNJZFNlcnZlciAmJiAhaGF2ZUlzKSB8fFxyXG4gICAgICAgICAgICAhdGhpcy5fYXV0aFN0ZXBJc1VzZWQoJ20ubG9naW4uZW1haWwuaWRlbnRpdHknKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvd1Bob25lTnVtYmVyKCkge1xyXG4gICAgICAgIGNvbnN0IHRocmVlUGlkTG9naW4gPSAhU2RrQ29uZmlnLmdldCgpLmRpc2FibGVfM3BpZF9sb2dpbjtcclxuICAgICAgICBjb25zdCBoYXZlSXMgPSBCb29sZWFuKHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmlzVXJsKTtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICF0aHJlZVBpZExvZ2luIHx8XHJcbiAgICAgICAgICAgICh0aGlzLnByb3BzLnNlcnZlclJlcXVpcmVzSWRTZXJ2ZXIgJiYgIWhhdmVJcykgfHxcclxuICAgICAgICAgICAgIXRoaXMuX2F1dGhTdGVwSXNVc2VkKCdtLmxvZ2luLm1zaXNkbicpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlckVtYWlsKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fc2hvd0VtYWlsKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCBlbWFpbFBsYWNlaG9sZGVyID0gdGhpcy5fYXV0aFN0ZXBJc1JlcXVpcmVkKCdtLmxvZ2luLmVtYWlsLmlkZW50aXR5JykgP1xyXG4gICAgICAgICAgICBfdChcIkVtYWlsXCIpIDpcclxuICAgICAgICAgICAgX3QoXCJFbWFpbCAob3B0aW9uYWwpXCIpO1xyXG4gICAgICAgIHJldHVybiA8RmllbGRcclxuICAgICAgICAgICAgcmVmPXtmaWVsZCA9PiB0aGlzW0ZJRUxEX0VNQUlMXSA9IGZpZWxkfVxyXG4gICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgIGxhYmVsPXtlbWFpbFBsYWNlaG9sZGVyfVxyXG4gICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5lbWFpbH1cclxuICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25FbWFpbENoYW5nZX1cclxuICAgICAgICAgICAgb25WYWxpZGF0ZT17dGhpcy5vbkVtYWlsVmFsaWRhdGV9XHJcbiAgICAgICAgLz47XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlclBhc3N3b3JkKCkge1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICByZXR1cm4gPEZpZWxkXHJcbiAgICAgICAgICAgIGlkPVwibXhfUmVnaXN0cmF0aW9uRm9ybV9wYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHJlZj17ZmllbGQgPT4gdGhpc1tGSUVMRF9QQVNTV09SRF0gPSBmaWVsZH1cclxuICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgYXV0b0NvbXBsZXRlPVwibmV3LXBhc3N3b3JkXCJcclxuICAgICAgICAgICAgbGFiZWw9e190KFwiUGFzc3dvcmRcIil9XHJcbiAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnBhc3N3b3JkfVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vblBhc3N3b3JkQ2hhbmdlfVxyXG4gICAgICAgICAgICBvblZhbGlkYXRlPXt0aGlzLm9uUGFzc3dvcmRWYWxpZGF0ZX1cclxuICAgICAgICAvPjtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyUGFzc3dvcmRDb25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICByZXR1cm4gPEZpZWxkXHJcbiAgICAgICAgICAgIGlkPVwibXhfUmVnaXN0cmF0aW9uRm9ybV9wYXNzd29yZENvbmZpcm1cIlxyXG4gICAgICAgICAgICByZWY9e2ZpZWxkID0+IHRoaXNbRklFTERfUEFTU1dPUkRfQ09ORklSTV0gPSBmaWVsZH1cclxuICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgYXV0b0NvbXBsZXRlPVwibmV3LXBhc3N3b3JkXCJcclxuICAgICAgICAgICAgbGFiZWw9e190KFwiQ29uZmlybVwiKX1cclxuICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc3dvcmRDb25maXJtfVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vblBhc3N3b3JkQ29uZmlybUNoYW5nZX1cclxuICAgICAgICAgICAgb25WYWxpZGF0ZT17dGhpcy5vblBhc3N3b3JkQ29uZmlybVZhbGlkYXRlfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJQaG9uZU51bWJlcigpIHtcclxuICAgICAgICBpZiAoIXRoaXMuX3Nob3dQaG9uZU51bWJlcigpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBDb3VudHJ5RHJvcGRvd24gPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5hdXRoLkNvdW50cnlEcm9wZG93bicpO1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCBwaG9uZUxhYmVsID0gdGhpcy5fYXV0aFN0ZXBJc1JlcXVpcmVkKCdtLmxvZ2luLm1zaXNkbicpID9cclxuICAgICAgICAgICAgX3QoXCJQaG9uZVwiKSA6XHJcbiAgICAgICAgICAgIF90KFwiUGhvbmUgKG9wdGlvbmFsKVwiKTtcclxuICAgICAgICBjb25zdCBwaG9uZUNvdW50cnkgPSA8Q291bnRyeURyb3Bkb3duXHJcbiAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnBob25lQ291bnRyeX1cclxuICAgICAgICAgICAgaXNTbWFsbD17dHJ1ZX1cclxuICAgICAgICAgICAgc2hvd1ByZWZpeD17dHJ1ZX1cclxuICAgICAgICAgICAgb25PcHRpb25DaGFuZ2U9e3RoaXMub25QaG9uZUNvdW50cnlDaGFuZ2V9XHJcbiAgICAgICAgLz47XHJcbiAgICAgICAgcmV0dXJuIDxGaWVsZFxyXG4gICAgICAgICAgICByZWY9e2ZpZWxkID0+IHRoaXNbRklFTERfUEhPTkVfTlVNQkVSXSA9IGZpZWxkfVxyXG4gICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgIGxhYmVsPXtwaG9uZUxhYmVsfVxyXG4gICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5waG9uZU51bWJlcn1cclxuICAgICAgICAgICAgcHJlZml4PXtwaG9uZUNvdW50cnl9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uUGhvbmVOdW1iZXJDaGFuZ2V9XHJcbiAgICAgICAgICAgIG9uVmFsaWRhdGU9e3RoaXMub25QaG9uZU51bWJlclZhbGlkYXRlfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXJVc2VybmFtZSgpIHtcclxuICAgICAgICBjb25zdCBGaWVsZCA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkZpZWxkJyk7XHJcbiAgICAgICAgcmV0dXJuIDxGaWVsZFxyXG4gICAgICAgICAgICBpZD1cIm14X1JlZ2lzdHJhdGlvbkZvcm1fdXNlcm5hbWVcIlxyXG4gICAgICAgICAgICByZWY9e2ZpZWxkID0+IHRoaXNbRklFTERfVVNFUk5BTUVdID0gZmllbGR9XHJcbiAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICBsYWJlbD17X3QoXCJVc2VybmFtZVwiKX1cclxuICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUudXNlcm5hbWV9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uVXNlcm5hbWVDaGFuZ2V9XHJcbiAgICAgICAgICAgIG9uVmFsaWRhdGU9e3RoaXMub25Vc2VybmFtZVZhbGlkYXRlfVxyXG4gICAgICAgIC8+O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCB5b3VyTWF0cml4QWNjb3VudFRleHQgPSBfdCgnQ3JlYXRlIHlvdXIgTWF0cml4IGFjY291bnQgb24gJShzZXJ2ZXJOYW1lKXMnLCB7XHJcbiAgICAgICAgICAgIHNlcnZlck5hbWU6IHRoaXMucHJvcHMuc2VydmVyQ29uZmlnLmhzTmFtZSxcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaHNOYW1lSXNEaWZmZXJlbnQpIHtcclxuICAgICAgICAgICAgY29uc3QgVGV4dFdpdGhUb29sdGlwID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlRleHRXaXRoVG9vbHRpcFwiKTtcclxuXHJcbiAgICAgICAgICAgIHlvdXJNYXRyaXhBY2NvdW50VGV4dCA9IF90KCdDcmVhdGUgeW91ciBNYXRyaXggYWNjb3VudCBvbiA8dW5kZXJsaW5lZFNlcnZlck5hbWUgLz4nLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgJ3VuZGVybGluZWRTZXJ2ZXJOYW1lJzogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiA8VGV4dFdpdGhUb29sdGlwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwibXhfTG9naW5fdW5kZXJsaW5lZFNlcnZlck5hbWVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwPXt0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc1VybH1cclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLnNlcnZlckNvbmZpZy5oc05hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9UZXh0V2l0aFRvb2x0aXA+O1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZWRpdExpbmsgPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRWRpdFNlcnZlckRldGFpbHNDbGljaykge1xyXG4gICAgICAgICAgICBlZGl0TGluayA9IDxhIGNsYXNzTmFtZT1cIm14X0F1dGhCb2R5X2VkaXRTZXJ2ZXJEZXRhaWxzXCJcclxuICAgICAgICAgICAgICAgIGhyZWY9XCIjXCIgb25DbGljaz17dGhpcy5wcm9wcy5vbkVkaXRTZXJ2ZXJEZXRhaWxzQ2xpY2t9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtfdCgnQ2hhbmdlJyl9XHJcbiAgICAgICAgICAgIDwvYT47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByZWdpc3RlckJ1dHRvbiA9IChcclxuICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cIm14X0xvZ2luX3N1Ym1pdFwiIHR5cGU9XCJzdWJtaXRcIiB2YWx1ZT17X3QoXCJSZWdpc3RlclwiKX0gZGlzYWJsZWQ9eyF0aGlzLnByb3BzLmNhblN1Ym1pdH0gLz5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBsZXQgZW1haWxIZWxwZXJUZXh0ID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5fc2hvd0VtYWlsKCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3Nob3dQaG9uZU51bWJlcigpKSB7XHJcbiAgICAgICAgICAgICAgICBlbWFpbEhlbHBlclRleHQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJTZXQgYW4gZW1haWwgZm9yIGFjY291bnQgcmVjb3ZlcnkuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJVc2UgZW1haWwgb3IgcGhvbmUgdG8gb3B0aW9uYWxseSBiZSBkaXNjb3ZlcmFibGUgYnkgZXhpc3RpbmcgY29udGFjdHMuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGVtYWlsSGVscGVyVGV4dCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlNldCBhbiBlbWFpbCBmb3IgYWNjb3VudCByZWNvdmVyeS4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlVzZSBlbWFpbCB0byBvcHRpb25hbGx5IGJlIGRpc2NvdmVyYWJsZSBieSBleGlzdGluZyBjb250YWN0cy5cIixcclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGhhdmVJcyA9IEJvb2xlYW4odGhpcy5wcm9wcy5zZXJ2ZXJDb25maWcuaXNVcmwpO1xyXG4gICAgICAgIGxldCBub0lzVGV4dCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuc2VydmVyUmVxdWlyZXNJZFNlcnZlciAmJiAhaGF2ZUlzKSB7XHJcbiAgICAgICAgICAgIG5vSXNUZXh0ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgICAgICBcIk5vIGlkZW50aXR5IHNlcnZlciBpcyBjb25maWd1cmVkIHNvIHlvdSBjYW5ub3QgYWRkIGFuIGVtYWlsIGFkZHJlc3MgaW4gb3JkZXIgdG8gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwicmVzZXQgeW91ciBwYXNzd29yZCBpbiB0aGUgZnV0dXJlLlwiLFxyXG4gICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMz5cclxuICAgICAgICAgICAgICAgICAgICB7eW91ck1hdHJpeEFjY291bnRUZXh0fVxyXG4gICAgICAgICAgICAgICAgICAgIHtlZGl0TGlua31cclxuICAgICAgICAgICAgICAgIDwvaDM+XHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5vblN1Ym1pdH0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9maWVsZFJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJVc2VybmFtZSgpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQXV0aEJvZHlfZmllbGRSb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucmVuZGVyUGFzc3dvcmQoKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAge3RoaXMucmVuZGVyUGFzc3dvcmRDb25maXJtKCl9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9BdXRoQm9keV9maWVsZFJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJFbWFpbCgpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJQaG9uZU51bWJlcigpfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgZW1haWxIZWxwZXJUZXh0IH1cclxuICAgICAgICAgICAgICAgICAgICB7IG5vSXNUZXh0IH1cclxuICAgICAgICAgICAgICAgICAgICB7IHJlZ2lzdGVyQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=