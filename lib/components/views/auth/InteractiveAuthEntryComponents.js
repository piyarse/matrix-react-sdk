"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getEntryComponentForLoginType;
exports.FallbackAuthEntry = exports.SSOAuthEntry = exports.MsisdnAuthEntry = exports.EmailIdentityAuthEntry = exports.TermsAuthEntry = exports.RecaptchaAuthEntry = exports.PasswordAuthEntry = exports.DEFAULT_PHASE = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _url = _interopRequireDefault(require("url"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* This file contains a collection of components which are used by the
 * InteractiveAuth to prompt the user to enter the information needed
 * for an auth stage. (The intention is that they could also be used for other
 * components, such as the registration flow).
 *
 * Call getEntryComponentForLoginType() to get a component suitable for a
 * particular login type. Each component requires the same properties:
 *
 * matrixClient:           A matrix client. May be a different one to the one
 *                         currently being used generally (eg. to register with
 *                         one HS whilst beign a guest on another).
 * loginType:              the login type of the auth stage being attempted
 * authSessionId:          session id from the server
 * clientSecret:           The client secret in use for ID server auth sessions
 * stageParams:            params from the server for the stage being attempted
 * errorText:              error message from a previous attempt to authenticate
 * submitAuthDict:         a function which will be called with the new auth dict
 * busy:                   a boolean indicating whether the auth logic is doing something
 *                         the user needs to wait for.
 * inputs:                 Object of inputs provided by the user, as in js-sdk
 *                         interactive-auth
 * stageState:             Stage-specific object used for communicating state information
 *                         to the UI from the state-specific auth logic.
 *                         Defined keys for stages are:
 *                             m.login.email.identity:
 *                              * emailSid: string representing the sid of the active
 *                                          verification session from the ID server, or
 *                                          null if no session is active.
 * fail:                   a function which should be called with an error object if an
 *                         error occurred during the auth stage. This will cause the auth
 *                         session to be failed and the process to go back to the start.
 * setEmailSid:            m.login.email.identity only: a function to be called with the
 *                         email sid after a token is requested.
 * onPhaseChange:          A function which is called when the stage's phase changes. If
 *                         the stage has no phases, call this with DEFAULT_PHASE. Takes
 *                         one argument, the phase, and is always defined/required.
 * continueText:           For stages which have a continue button, the text to use.
 * continueKind:           For stages which have a continue button, the style of button to
 *                         use. For example, 'danger' or 'primary'.
 * onCancel                A function with no arguments which is called by the stage if the
 *                         user knowingly cancelled/dismissed the authentication attempt.
 *
 * Each component may also provide the following functions (beyond the standard React ones):
 *    focus: set the input focus appropriately in the form.
 */
const DEFAULT_PHASE = 0;
exports.DEFAULT_PHASE = DEFAULT_PHASE;
const PasswordAuthEntry = (0, _createReactClass.default)({
  displayName: 'PasswordAuthEntry',
  statics: {
    LOGIN_TYPE: "m.login.password"
  },
  propTypes: {
    matrixClient: _propTypes.default.object.isRequired,
    submitAuthDict: _propTypes.default.func.isRequired,
    errorText: _propTypes.default.string,
    // is the auth logic currently waiting for something to
    // happen?
    busy: _propTypes.default.bool,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
  },
  getInitialState: function () {
    return {
      password: ""
    };
  },
  _onSubmit: function (e) {
    e.preventDefault();
    if (this.props.busy) return;
    this.props.submitAuthDict({
      type: PasswordAuthEntry.LOGIN_TYPE,
      // TODO: Remove `user` once servers support proper UIA
      // See https://github.com/vector-im/riot-web/issues/10312
      user: this.props.matrixClient.credentials.userId,
      identifier: {
        type: "m.id.user",
        user: this.props.matrixClient.credentials.userId
      },
      password: this.state.password
    });
  },
  _onPasswordFieldChange: function (ev) {
    // enable the submit button iff the password is non-empty
    this.setState({
      password: ev.target.value
    });
  },
  render: function () {
    const passwordBoxClass = (0, _classnames.default)({
      "error": this.props.errorText
    });
    let submitButtonOrSpinner;

    if (this.props.busy) {
      const Loader = sdk.getComponent("elements.Spinner");
      submitButtonOrSpinner = _react.default.createElement(Loader, null);
    } else {
      submitButtonOrSpinner = _react.default.createElement("input", {
        type: "submit",
        className: "mx_Dialog_primary",
        disabled: !this.state.password,
        value: (0, _languageHandler._t)("Continue")
      });
    }

    let errorSection;

    if (this.props.errorText) {
      errorSection = _react.default.createElement("div", {
        className: "error",
        role: "alert"
      }, this.props.errorText);
    }

    const Field = sdk.getComponent('elements.Field');
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Confirm your identity by entering your account password below.")), _react.default.createElement("form", {
      onSubmit: this._onSubmit,
      className: "mx_InteractiveAuthEntryComponents_passwordSection"
    }, _react.default.createElement(Field, {
      className: passwordBoxClass,
      type: "password",
      name: "passwordField",
      label: (0, _languageHandler._t)('Password'),
      autoFocus: true,
      value: this.state.password,
      onChange: this._onPasswordFieldChange
    }), _react.default.createElement("div", {
      className: "mx_button_row"
    }, submitButtonOrSpinner)), errorSection);
  }
});
exports.PasswordAuthEntry = PasswordAuthEntry;
const RecaptchaAuthEntry = (0, _createReactClass.default)({
  displayName: 'RecaptchaAuthEntry',
  statics: {
    LOGIN_TYPE: "m.login.recaptcha"
  },
  propTypes: {
    submitAuthDict: _propTypes.default.func.isRequired,
    stageParams: _propTypes.default.object.isRequired,
    errorText: _propTypes.default.string,
    busy: _propTypes.default.bool,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
  },
  _onCaptchaResponse: function (response) {
    this.props.submitAuthDict({
      type: RecaptchaAuthEntry.LOGIN_TYPE,
      response: response
    });
  },
  render: function () {
    if (this.props.busy) {
      const Loader = sdk.getComponent("elements.Spinner");
      return _react.default.createElement(Loader, null);
    }

    let errorText = this.props.errorText;
    const CaptchaForm = sdk.getComponent("views.auth.CaptchaForm");
    let sitePublicKey;

    if (!this.props.stageParams || !this.props.stageParams.public_key) {
      errorText = (0, _languageHandler._t)("Missing captcha public key in homeserver configuration. Please report " + "this to your homeserver administrator.");
    } else {
      sitePublicKey = this.props.stageParams.public_key;
    }

    let errorSection;

    if (errorText) {
      errorSection = _react.default.createElement("div", {
        className: "error",
        role: "alert"
      }, errorText);
    }

    return _react.default.createElement("div", null, _react.default.createElement(CaptchaForm, {
      sitePublicKey: sitePublicKey,
      onCaptchaResponse: this._onCaptchaResponse
    }), errorSection);
  }
});
exports.RecaptchaAuthEntry = RecaptchaAuthEntry;
const TermsAuthEntry = (0, _createReactClass.default)({
  displayName: 'TermsAuthEntry',
  statics: {
    LOGIN_TYPE: "m.login.terms"
  },
  propTypes: {
    submitAuthDict: _propTypes.default.func.isRequired,
    stageParams: _propTypes.default.object.isRequired,
    errorText: _propTypes.default.string,
    busy: _propTypes.default.bool,
    showContinue: _propTypes.default.bool,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
  },
  // TODO: [REACT-WARNING] Move this to constructor
  componentWillMount: function () {
    // example stageParams:
    //
    // {
    //     "policies": {
    //         "privacy_policy": {
    //             "version": "1.0",
    //             "en": {
    //                 "name": "Privacy Policy",
    //                 "url": "https://example.org/privacy-1.0-en.html",
    //             },
    //             "fr": {
    //                 "name": "Politique de confidentialité",
    //                 "url": "https://example.org/privacy-1.0-fr.html",
    //             },
    //         },
    //         "other_policy": { ... },
    //     }
    // }
    const allPolicies = this.props.stageParams.policies || {};

    const prefLang = _SettingsStore.default.getValue("language");

    const initToggles = {};
    const pickedPolicies = [];

    for (const policyId of Object.keys(allPolicies)) {
      const policy = allPolicies[policyId]; // Pick a language based on the user's language, falling back to english,
      // and finally to the first language available. If there's still no policy
      // available then the homeserver isn't respecting the spec.

      let langPolicy = policy[prefLang];
      if (!langPolicy) langPolicy = policy["en"];

      if (!langPolicy) {
        // last resort
        const firstLang = Object.keys(policy).find(e => e !== "version");
        langPolicy = policy[firstLang];
      }

      if (!langPolicy) throw new Error("Failed to find a policy to show the user");
      initToggles[policyId] = false;
      langPolicy.id = policyId;
      pickedPolicies.push(langPolicy);
    }

    this.setState({
      "toggledPolicies": initToggles,
      "policies": pickedPolicies
    });
  },
  tryContinue: function () {
    this._trySubmit();
  },
  _togglePolicy: function (policyId) {
    const newToggles = {};

    for (const policy of this.state.policies) {
      let checked = this.state.toggledPolicies[policy.id];
      if (policy.id === policyId) checked = !checked;
      newToggles[policy.id] = checked;
    }

    this.setState({
      "toggledPolicies": newToggles
    });
  },
  _trySubmit: function () {
    let allChecked = true;

    for (const policy of this.state.policies) {
      const checked = this.state.toggledPolicies[policy.id];
      allChecked = allChecked && checked;
    }

    if (allChecked) this.props.submitAuthDict({
      type: TermsAuthEntry.LOGIN_TYPE
    });else this.setState({
      errorText: (0, _languageHandler._t)("Please review and accept all of the homeserver's policies")
    });
  },
  render: function () {
    if (this.props.busy) {
      const Loader = sdk.getComponent("elements.Spinner");
      return _react.default.createElement(Loader, null);
    }

    const checkboxes = [];
    let allChecked = true;

    for (const policy of this.state.policies) {
      const checked = this.state.toggledPolicies[policy.id];
      allChecked = allChecked && checked;
      checkboxes.push(_react.default.createElement("label", {
        key: "policy_checkbox_" + policy.id,
        className: "mx_InteractiveAuthEntryComponents_termsPolicy"
      }, _react.default.createElement("input", {
        type: "checkbox",
        onChange: () => this._togglePolicy(policy.id),
        checked: checked
      }), _react.default.createElement("a", {
        href: policy.url,
        target: "_blank",
        rel: "noreferrer noopener"
      }, policy.name)));
    }

    let errorSection;

    if (this.props.errorText || this.state.errorText) {
      errorSection = _react.default.createElement("div", {
        className: "error",
        role: "alert"
      }, this.props.errorText || this.state.errorText);
    }

    let submitButton;

    if (this.props.showContinue !== false) {
      // XXX: button classes
      submitButton = _react.default.createElement("button", {
        className: "mx_InteractiveAuthEntryComponents_termsSubmit mx_GeneralButton",
        onClick: this._trySubmit,
        disabled: !allChecked
      }, (0, _languageHandler._t)("Accept"));
    }

    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Please review and accept the policies of this homeserver:")), checkboxes, errorSection, submitButton);
  }
});
exports.TermsAuthEntry = TermsAuthEntry;
const EmailIdentityAuthEntry = (0, _createReactClass.default)({
  displayName: 'EmailIdentityAuthEntry',
  statics: {
    LOGIN_TYPE: "m.login.email.identity"
  },
  propTypes: {
    matrixClient: _propTypes.default.object.isRequired,
    submitAuthDict: _propTypes.default.func.isRequired,
    authSessionId: _propTypes.default.string.isRequired,
    clientSecret: _propTypes.default.string.isRequired,
    inputs: _propTypes.default.object.isRequired,
    stageState: _propTypes.default.object.isRequired,
    fail: _propTypes.default.func.isRequired,
    setEmailSid: _propTypes.default.func.isRequired,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
  },
  getInitialState: function () {
    return {
      requestingToken: false
    };
  },
  render: function () {
    if (this.state.requestingToken) {
      const Loader = sdk.getComponent("elements.Spinner");
      return _react.default.createElement(Loader, null);
    } else {
      return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("An email has been sent to %(emailAddress)s", {
        emailAddress: sub => _react.default.createElement("i", null, this.props.inputs.emailAddress)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Please check your email to continue registration.")));
    }
  }
});
exports.EmailIdentityAuthEntry = EmailIdentityAuthEntry;
const MsisdnAuthEntry = (0, _createReactClass.default)({
  displayName: 'MsisdnAuthEntry',
  statics: {
    LOGIN_TYPE: "m.login.msisdn"
  },
  propTypes: {
    inputs: _propTypes.default.shape({
      phoneCountry: _propTypes.default.string,
      phoneNumber: _propTypes.default.string
    }),
    fail: _propTypes.default.func,
    clientSecret: _propTypes.default.func,
    submitAuthDict: _propTypes.default.func.isRequired,
    matrixClient: _propTypes.default.object,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  getInitialState: function () {
    return {
      token: '',
      requestingToken: false
    };
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
    this._submitUrl = null;
    this._sid = null;
    this._msisdn = null;
    this._tokenBox = null;
    this.setState({
      requestingToken: true
    });

    this._requestMsisdnToken().catch(e => {
      this.props.fail(e);
    }).finally(() => {
      this.setState({
        requestingToken: false
      });
    });
  },

  /*
   * Requests a verification token by SMS.
   */
  _requestMsisdnToken: function () {
    return this.props.matrixClient.requestRegisterMsisdnToken(this.props.inputs.phoneCountry, this.props.inputs.phoneNumber, this.props.clientSecret, 1 // TODO: Multiple send attempts?
    ).then(result => {
      this._submitUrl = result.submit_url;
      this._sid = result.sid;
      this._msisdn = result.msisdn;
    });
  },
  _onTokenChange: function (e) {
    this.setState({
      token: e.target.value
    });
  },
  _onFormSubmit: async function (e) {
    e.preventDefault();
    if (this.state.token == '') return;
    this.setState({
      errorText: null
    });

    try {
      const requiresIdServerParam = await this.props.matrixClient.doesServerRequireIdServerParam();
      let result;

      if (this._submitUrl) {
        result = await this.props.matrixClient.submitMsisdnTokenOtherUrl(this._submitUrl, this._sid, this.props.clientSecret, this.state.token);
      } else if (requiresIdServerParam) {
        result = await this.props.matrixClient.submitMsisdnToken(this._sid, this.props.clientSecret, this.state.token);
      } else {
        throw new Error("The registration with MSISDN flow is misconfigured");
      }

      if (result.success) {
        const creds = {
          sid: this._sid,
          client_secret: this.props.clientSecret
        };

        if (requiresIdServerParam) {
          const idServerParsedUrl = _url.default.parse(this.props.matrixClient.getIdentityServerUrl());

          creds.id_server = idServerParsedUrl.host;
        }

        this.props.submitAuthDict({
          type: MsisdnAuthEntry.LOGIN_TYPE,
          // TODO: Remove `threepid_creds` once servers support proper UIA
          // See https://github.com/vector-im/riot-web/issues/10312
          threepid_creds: creds,
          threepidCreds: creds
        });
      } else {
        this.setState({
          errorText: (0, _languageHandler._t)("Token incorrect")
        });
      }
    } catch (e) {
      this.props.fail(e);
      console.log("Failed to submit msisdn token");
    }
  },
  render: function () {
    if (this.state.requestingToken) {
      const Loader = sdk.getComponent("elements.Spinner");
      return _react.default.createElement(Loader, null);
    } else {
      const enableSubmit = Boolean(this.state.token);
      const submitClasses = (0, _classnames.default)({
        mx_InteractiveAuthEntryComponents_msisdnSubmit: true,
        mx_GeneralButton: true
      });
      let errorSection;

      if (this.state.errorText) {
        errorSection = _react.default.createElement("div", {
          className: "error",
          role: "alert"
        }, this.state.errorText);
      }

      return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("A text message has been sent to %(msisdn)s", {
        msisdn: _react.default.createElement("i", null, this._msisdn)
      })), _react.default.createElement("p", null, (0, _languageHandler._t)("Please enter the code it contains:")), _react.default.createElement("div", {
        className: "mx_InteractiveAuthEntryComponents_msisdnWrapper"
      }, _react.default.createElement("form", {
        onSubmit: this._onFormSubmit
      }, _react.default.createElement("input", {
        type: "text",
        className: "mx_InteractiveAuthEntryComponents_msisdnEntry",
        value: this.state.token,
        onChange: this._onTokenChange,
        "aria-label": (0, _languageHandler._t)("Code")
      }), _react.default.createElement("br", null), _react.default.createElement("input", {
        type: "submit",
        value: (0, _languageHandler._t)("Submit"),
        className: submitClasses,
        disabled: !enableSubmit
      })), errorSection));
    }
  }
});
exports.MsisdnAuthEntry = MsisdnAuthEntry;

class SSOAuthEntry extends _react.default.Component {
  // button to start SSO
  // button to confirm SSO completed
  constructor(props) {
    super(props); // We actually send the user through fallback auth so we don't have to
    // deal with a redirect back to us, losing application context.

    (0, _defineProperty2.default)(this, "_ssoUrl", void 0);
    (0, _defineProperty2.default)(this, "onStartAuthClick", () => {
      // Note: We don't use PlatformPeg's startSsoAuth functions because we almost
      // certainly will need to open the thing in a new tab to avoid losing application
      // context.
      window.open(this._ssoUrl, '_blank');
      this.setState({
        phase: SSOAuthEntry.PHASE_POSTAUTH
      });
      this.props.onPhaseChange(SSOAuthEntry.PHASE_POSTAUTH);
    });
    (0, _defineProperty2.default)(this, "onConfirmClick", () => {
      this.props.submitAuthDict({});
    });
    this._ssoUrl = props.matrixClient.getFallbackAuthUrl(this.props.loginType, this.props.authSessionId);
    this.state = {
      phase: SSOAuthEntry.PHASE_PREAUTH
    };
  }

  componentDidMount()
  /*: void*/
  {
    this.props.onPhaseChange(SSOAuthEntry.PHASE_PREAUTH);
  }

  render() {
    let continueButton = null;

    const cancelButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: this.props.onCancel,
      kind: this.props.continueKind ? this.props.continueKind + '_outline' : 'primary_outline'
    }, (0, _languageHandler._t)("Cancel"));

    if (this.state.phase === SSOAuthEntry.PHASE_PREAUTH) {
      continueButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onStartAuthClick,
        kind: this.props.continueKind || 'primary'
      }, this.props.continueText || (0, _languageHandler._t)("Single Sign On"));
    } else {
      continueButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onConfirmClick,
        kind: this.props.continueKind || 'primary'
      }, this.props.continueText || (0, _languageHandler._t)("Confirm"));
    }

    return _react.default.createElement("div", {
      className: "mx_InteractiveAuthEntryComponents_sso_buttons"
    }, cancelButton, continueButton);
  }

}

exports.SSOAuthEntry = SSOAuthEntry;
(0, _defineProperty2.default)(SSOAuthEntry, "propTypes", {
  matrixClient: _propTypes.default.object.isRequired,
  authSessionId: _propTypes.default.string.isRequired,
  loginType: _propTypes.default.string.isRequired,
  submitAuthDict: _propTypes.default.func.isRequired,
  errorText: _propTypes.default.string,
  onPhaseChange: _propTypes.default.func.isRequired,
  continueText: _propTypes.default.string,
  continueKind: _propTypes.default.string,
  onCancel: _propTypes.default.func
});
(0, _defineProperty2.default)(SSOAuthEntry, "LOGIN_TYPE", "m.login.sso");
(0, _defineProperty2.default)(SSOAuthEntry, "UNSTABLE_LOGIN_TYPE", "org.matrix.login.sso");
(0, _defineProperty2.default)(SSOAuthEntry, "PHASE_PREAUTH", 1);
(0, _defineProperty2.default)(SSOAuthEntry, "PHASE_POSTAUTH", 2);
const FallbackAuthEntry = (0, _createReactClass.default)({
  displayName: 'FallbackAuthEntry',
  propTypes: {
    matrixClient: _propTypes.default.object.isRequired,
    authSessionId: _propTypes.default.string.isRequired,
    loginType: _propTypes.default.string.isRequired,
    submitAuthDict: _propTypes.default.func.isRequired,
    errorText: _propTypes.default.string,
    onPhaseChange: _propTypes.default.func.isRequired
  },
  componentDidMount: function () {
    this.props.onPhaseChange(DEFAULT_PHASE);
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    // we have to make the user click a button, as browsers will block
    // the popup if we open it immediately.
    this._popupWindow = null;
    window.addEventListener("message", this._onReceiveMessage);
    this._fallbackButton = (0, _react.createRef)();
  },
  componentWillUnmount: function () {
    window.removeEventListener("message", this._onReceiveMessage);

    if (this._popupWindow) {
      this._popupWindow.close();
    }
  },
  focus: function () {
    if (this._fallbackButton.current) {
      this._fallbackButton.current.focus();
    }
  },
  _onShowFallbackClick: function (e) {
    e.preventDefault();
    e.stopPropagation();
    const url = this.props.matrixClient.getFallbackAuthUrl(this.props.loginType, this.props.authSessionId);
    this._popupWindow = window.open(url);
    this._popupWindow.opener = null;
  },
  _onReceiveMessage: function (event) {
    if (event.data === "authDone" && event.origin === this.props.matrixClient.getHomeserverUrl()) {
      this.props.submitAuthDict({});
    }
  },
  render: function () {
    let errorSection;

    if (this.props.errorText) {
      errorSection = _react.default.createElement("div", {
        className: "error",
        role: "alert"
      }, this.props.errorText);
    }

    return _react.default.createElement("div", null, _react.default.createElement("a", {
      href: "",
      ref: this._fallbackButton,
      onClick: this._onShowFallbackClick
    }, (0, _languageHandler._t)("Start authentication")), errorSection);
  }
});
exports.FallbackAuthEntry = FallbackAuthEntry;
const AuthEntryComponents = [PasswordAuthEntry, RecaptchaAuthEntry, EmailIdentityAuthEntry, MsisdnAuthEntry, TermsAuthEntry, SSOAuthEntry];

function getEntryComponentForLoginType(loginType) {
  for (const c of AuthEntryComponents) {
    if (c.LOGIN_TYPE === loginType || c.UNSTABLE_LOGIN_TYPE === loginType) {
      return c;
    }
  }

  return FallbackAuthEntry;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2F1dGgvSW50ZXJhY3RpdmVBdXRoRW50cnlDb21wb25lbnRzLmpzIl0sIm5hbWVzIjpbIkRFRkFVTFRfUEhBU0UiLCJQYXNzd29yZEF1dGhFbnRyeSIsImRpc3BsYXlOYW1lIiwic3RhdGljcyIsIkxPR0lOX1RZUEUiLCJwcm9wVHlwZXMiLCJtYXRyaXhDbGllbnQiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3VibWl0QXV0aERpY3QiLCJmdW5jIiwiZXJyb3JUZXh0Iiwic3RyaW5nIiwiYnVzeSIsImJvb2wiLCJvblBoYXNlQ2hhbmdlIiwiY29tcG9uZW50RGlkTW91bnQiLCJwcm9wcyIsImdldEluaXRpYWxTdGF0ZSIsInBhc3N3b3JkIiwiX29uU3VibWl0IiwiZSIsInByZXZlbnREZWZhdWx0IiwidHlwZSIsInVzZXIiLCJjcmVkZW50aWFscyIsInVzZXJJZCIsImlkZW50aWZpZXIiLCJzdGF0ZSIsIl9vblBhc3N3b3JkRmllbGRDaGFuZ2UiLCJldiIsInNldFN0YXRlIiwidGFyZ2V0IiwidmFsdWUiLCJyZW5kZXIiLCJwYXNzd29yZEJveENsYXNzIiwic3VibWl0QnV0dG9uT3JTcGlubmVyIiwiTG9hZGVyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZXJyb3JTZWN0aW9uIiwiRmllbGQiLCJSZWNhcHRjaGFBdXRoRW50cnkiLCJzdGFnZVBhcmFtcyIsIl9vbkNhcHRjaGFSZXNwb25zZSIsInJlc3BvbnNlIiwiQ2FwdGNoYUZvcm0iLCJzaXRlUHVibGljS2V5IiwicHVibGljX2tleSIsIlRlcm1zQXV0aEVudHJ5Iiwic2hvd0NvbnRpbnVlIiwiY29tcG9uZW50V2lsbE1vdW50IiwiYWxsUG9saWNpZXMiLCJwb2xpY2llcyIsInByZWZMYW5nIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwiaW5pdFRvZ2dsZXMiLCJwaWNrZWRQb2xpY2llcyIsInBvbGljeUlkIiwiT2JqZWN0Iiwia2V5cyIsInBvbGljeSIsImxhbmdQb2xpY3kiLCJmaXJzdExhbmciLCJmaW5kIiwiRXJyb3IiLCJpZCIsInB1c2giLCJ0cnlDb250aW51ZSIsIl90cnlTdWJtaXQiLCJfdG9nZ2xlUG9saWN5IiwibmV3VG9nZ2xlcyIsImNoZWNrZWQiLCJ0b2dnbGVkUG9saWNpZXMiLCJhbGxDaGVja2VkIiwiY2hlY2tib3hlcyIsInVybCIsIm5hbWUiLCJzdWJtaXRCdXR0b24iLCJFbWFpbElkZW50aXR5QXV0aEVudHJ5IiwiYXV0aFNlc3Npb25JZCIsImNsaWVudFNlY3JldCIsImlucHV0cyIsInN0YWdlU3RhdGUiLCJmYWlsIiwic2V0RW1haWxTaWQiLCJyZXF1ZXN0aW5nVG9rZW4iLCJlbWFpbEFkZHJlc3MiLCJzdWIiLCJNc2lzZG5BdXRoRW50cnkiLCJzaGFwZSIsInBob25lQ291bnRyeSIsInBob25lTnVtYmVyIiwidG9rZW4iLCJfc3VibWl0VXJsIiwiX3NpZCIsIl9tc2lzZG4iLCJfdG9rZW5Cb3giLCJfcmVxdWVzdE1zaXNkblRva2VuIiwiY2F0Y2giLCJmaW5hbGx5IiwicmVxdWVzdFJlZ2lzdGVyTXNpc2RuVG9rZW4iLCJ0aGVuIiwicmVzdWx0Iiwic3VibWl0X3VybCIsInNpZCIsIm1zaXNkbiIsIl9vblRva2VuQ2hhbmdlIiwiX29uRm9ybVN1Ym1pdCIsInJlcXVpcmVzSWRTZXJ2ZXJQYXJhbSIsImRvZXNTZXJ2ZXJSZXF1aXJlSWRTZXJ2ZXJQYXJhbSIsInN1Ym1pdE1zaXNkblRva2VuT3RoZXJVcmwiLCJzdWJtaXRNc2lzZG5Ub2tlbiIsInN1Y2Nlc3MiLCJjcmVkcyIsImNsaWVudF9zZWNyZXQiLCJpZFNlcnZlclBhcnNlZFVybCIsInBhcnNlIiwiZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwiLCJpZF9zZXJ2ZXIiLCJob3N0IiwidGhyZWVwaWRfY3JlZHMiLCJ0aHJlZXBpZENyZWRzIiwiY29uc29sZSIsImxvZyIsImVuYWJsZVN1Ym1pdCIsIkJvb2xlYW4iLCJzdWJtaXRDbGFzc2VzIiwibXhfSW50ZXJhY3RpdmVBdXRoRW50cnlDb21wb25lbnRzX21zaXNkblN1Ym1pdCIsIm14X0dlbmVyYWxCdXR0b24iLCJTU09BdXRoRW50cnkiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwid2luZG93Iiwib3BlbiIsIl9zc29VcmwiLCJwaGFzZSIsIlBIQVNFX1BPU1RBVVRIIiwiZ2V0RmFsbGJhY2tBdXRoVXJsIiwibG9naW5UeXBlIiwiUEhBU0VfUFJFQVVUSCIsImNvbnRpbnVlQnV0dG9uIiwiY2FuY2VsQnV0dG9uIiwib25DYW5jZWwiLCJjb250aW51ZUtpbmQiLCJvblN0YXJ0QXV0aENsaWNrIiwiY29udGludWVUZXh0Iiwib25Db25maXJtQ2xpY2siLCJGYWxsYmFja0F1dGhFbnRyeSIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJfcG9wdXBXaW5kb3ciLCJhZGRFdmVudExpc3RlbmVyIiwiX29uUmVjZWl2ZU1lc3NhZ2UiLCJfZmFsbGJhY2tCdXR0b24iLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJjbG9zZSIsImZvY3VzIiwiY3VycmVudCIsIl9vblNob3dGYWxsYmFja0NsaWNrIiwic3RvcFByb3BhZ2F0aW9uIiwib3BlbmVyIiwiZXZlbnQiLCJkYXRhIiwib3JpZ2luIiwiZ2V0SG9tZXNlcnZlclVybCIsIkF1dGhFbnRyeUNvbXBvbmVudHMiLCJnZXRFbnRyeUNvbXBvbmVudEZvckxvZ2luVHlwZSIsImMiLCJVTlNUQUJMRV9MT0dJTl9UWVBFIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUEzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBOENPLE1BQU1BLGFBQWEsR0FBRyxDQUF0Qjs7QUFFQSxNQUFNQyxpQkFBaUIsR0FBRywrQkFBaUI7QUFDOUNDLEVBQUFBLFdBQVcsRUFBRSxtQkFEaUM7QUFHOUNDLEVBQUFBLE9BQU8sRUFBRTtBQUNMQyxJQUFBQSxVQUFVLEVBQUU7QUFEUCxHQUhxQztBQU85Q0MsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLFlBQVksRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRHhCO0FBRVBDLElBQUFBLGNBQWMsRUFBRUgsbUJBQVVJLElBQVYsQ0FBZUYsVUFGeEI7QUFHUEcsSUFBQUEsU0FBUyxFQUFFTCxtQkFBVU0sTUFIZDtBQUlQO0FBQ0E7QUFDQUMsSUFBQUEsSUFBSSxFQUFFUCxtQkFBVVEsSUFOVDtBQU9QQyxJQUFBQSxhQUFhLEVBQUVULG1CQUFVSSxJQUFWLENBQWVGO0FBUHZCLEdBUG1DO0FBaUI5Q1EsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLQyxLQUFMLENBQVdGLGFBQVgsQ0FBeUJoQixhQUF6QjtBQUNILEdBbkI2QztBQXFCOUNtQixFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLFFBQVEsRUFBRTtBQURQLEtBQVA7QUFHSCxHQXpCNkM7QUEyQjlDQyxFQUFBQSxTQUFTLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQ25CQSxJQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQSxRQUFJLEtBQUtMLEtBQUwsQ0FBV0osSUFBZixFQUFxQjtBQUVyQixTQUFLSSxLQUFMLENBQVdSLGNBQVgsQ0FBMEI7QUFDdEJjLE1BQUFBLElBQUksRUFBRXZCLGlCQUFpQixDQUFDRyxVQURGO0FBRXRCO0FBQ0E7QUFDQXFCLE1BQUFBLElBQUksRUFBRSxLQUFLUCxLQUFMLENBQVdaLFlBQVgsQ0FBd0JvQixXQUF4QixDQUFvQ0MsTUFKcEI7QUFLdEJDLE1BQUFBLFVBQVUsRUFBRTtBQUNSSixRQUFBQSxJQUFJLEVBQUUsV0FERTtBQUVSQyxRQUFBQSxJQUFJLEVBQUUsS0FBS1AsS0FBTCxDQUFXWixZQUFYLENBQXdCb0IsV0FBeEIsQ0FBb0NDO0FBRmxDLE9BTFU7QUFTdEJQLE1BQUFBLFFBQVEsRUFBRSxLQUFLUyxLQUFMLENBQVdUO0FBVEMsS0FBMUI7QUFXSCxHQTFDNkM7QUE0QzlDVSxFQUFBQSxzQkFBc0IsRUFBRSxVQUFTQyxFQUFULEVBQWE7QUFDakM7QUFDQSxTQUFLQyxRQUFMLENBQWM7QUFDVlosTUFBQUEsUUFBUSxFQUFFVyxFQUFFLENBQUNFLE1BQUgsQ0FBVUM7QUFEVixLQUFkO0FBR0gsR0FqRDZDO0FBbUQ5Q0MsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxnQkFBZ0IsR0FBRyx5QkFBVztBQUNoQyxlQUFTLEtBQUtsQixLQUFMLENBQVdOO0FBRFksS0FBWCxDQUF6QjtBQUlBLFFBQUl5QixxQkFBSjs7QUFDQSxRQUFJLEtBQUtuQixLQUFMLENBQVdKLElBQWYsRUFBcUI7QUFDakIsWUFBTXdCLE1BQU0sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFmO0FBQ0FILE1BQUFBLHFCQUFxQixHQUFHLDZCQUFDLE1BQUQsT0FBeEI7QUFDSCxLQUhELE1BR087QUFDSEEsTUFBQUEscUJBQXFCLEdBQ2pCO0FBQU8sUUFBQSxJQUFJLEVBQUMsUUFBWjtBQUNJLFFBQUEsU0FBUyxFQUFDLG1CQURkO0FBRUksUUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLUixLQUFMLENBQVdULFFBRjFCO0FBR0ksUUFBQSxLQUFLLEVBQUUseUJBQUcsVUFBSDtBQUhYLFFBREo7QUFPSDs7QUFFRCxRQUFJcUIsWUFBSjs7QUFDQSxRQUFJLEtBQUt2QixLQUFMLENBQVdOLFNBQWYsRUFBMEI7QUFDdEI2QixNQUFBQSxZQUFZLEdBQ1I7QUFBSyxRQUFBLFNBQVMsRUFBQyxPQUFmO0FBQXVCLFFBQUEsSUFBSSxFQUFDO0FBQTVCLFNBQ00sS0FBS3ZCLEtBQUwsQ0FBV04sU0FEakIsQ0FESjtBQUtIOztBQUVELFVBQU04QixLQUFLLEdBQUdILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBZDtBQUVBLFdBQ0ksMENBQ0ksd0NBQUsseUJBQUcsZ0VBQUgsQ0FBTCxDQURKLEVBRUk7QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLbkIsU0FBckI7QUFBZ0MsTUFBQSxTQUFTLEVBQUM7QUFBMUMsT0FDSSw2QkFBQyxLQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUVlLGdCQURmO0FBRUksTUFBQSxJQUFJLEVBQUMsVUFGVDtBQUdJLE1BQUEsSUFBSSxFQUFDLGVBSFQ7QUFJSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFILENBSlg7QUFLSSxNQUFBLFNBQVMsRUFBRSxJQUxmO0FBTUksTUFBQSxLQUFLLEVBQUUsS0FBS1AsS0FBTCxDQUFXVCxRQU50QjtBQU9JLE1BQUEsUUFBUSxFQUFFLEtBQUtVO0FBUG5CLE1BREosRUFVSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTU8scUJBRE4sQ0FWSixDQUZKLEVBZ0JFSSxZQWhCRixDQURKO0FBb0JIO0FBckc2QyxDQUFqQixDQUExQjs7QUF3R0EsTUFBTUUsa0JBQWtCLEdBQUcsK0JBQWlCO0FBQy9DekMsRUFBQUEsV0FBVyxFQUFFLG9CQURrQztBQUcvQ0MsRUFBQUEsT0FBTyxFQUFFO0FBQ0xDLElBQUFBLFVBQVUsRUFBRTtBQURQLEdBSHNDO0FBTy9DQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEssSUFBQUEsY0FBYyxFQUFFSCxtQkFBVUksSUFBVixDQUFlRixVQUR4QjtBQUVQbUMsSUFBQUEsV0FBVyxFQUFFckMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRnZCO0FBR1BHLElBQUFBLFNBQVMsRUFBRUwsbUJBQVVNLE1BSGQ7QUFJUEMsSUFBQUEsSUFBSSxFQUFFUCxtQkFBVVEsSUFKVDtBQUtQQyxJQUFBQSxhQUFhLEVBQUVULG1CQUFVSSxJQUFWLENBQWVGO0FBTHZCLEdBUG9DO0FBZS9DUSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLEtBQUwsQ0FBV0YsYUFBWCxDQUF5QmhCLGFBQXpCO0FBQ0gsR0FqQjhDO0FBbUIvQzZDLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNDLFFBQVQsRUFBbUI7QUFDbkMsU0FBSzVCLEtBQUwsQ0FBV1IsY0FBWCxDQUEwQjtBQUN0QmMsTUFBQUEsSUFBSSxFQUFFbUIsa0JBQWtCLENBQUN2QyxVQURIO0FBRXRCMEMsTUFBQUEsUUFBUSxFQUFFQTtBQUZZLEtBQTFCO0FBSUgsR0F4QjhDO0FBMEIvQ1gsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJLEtBQUtqQixLQUFMLENBQVdKLElBQWYsRUFBcUI7QUFDakIsWUFBTXdCLE1BQU0sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFmO0FBQ0EsYUFBTyw2QkFBQyxNQUFELE9BQVA7QUFDSDs7QUFFRCxRQUFJNUIsU0FBUyxHQUFHLEtBQUtNLEtBQUwsQ0FBV04sU0FBM0I7QUFFQSxVQUFNbUMsV0FBVyxHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXBCO0FBQ0EsUUFBSVEsYUFBSjs7QUFDQSxRQUFJLENBQUMsS0FBSzlCLEtBQUwsQ0FBVzBCLFdBQVosSUFBMkIsQ0FBQyxLQUFLMUIsS0FBTCxDQUFXMEIsV0FBWCxDQUF1QkssVUFBdkQsRUFBbUU7QUFDL0RyQyxNQUFBQSxTQUFTLEdBQUcseUJBQ1IsMkVBQ0Esd0NBRlEsQ0FBWjtBQUlILEtBTEQsTUFLTztBQUNIb0MsTUFBQUEsYUFBYSxHQUFHLEtBQUs5QixLQUFMLENBQVcwQixXQUFYLENBQXVCSyxVQUF2QztBQUNIOztBQUVELFFBQUlSLFlBQUo7O0FBQ0EsUUFBSTdCLFNBQUosRUFBZTtBQUNYNkIsTUFBQUEsWUFBWSxHQUNSO0FBQUssUUFBQSxTQUFTLEVBQUMsT0FBZjtBQUF1QixRQUFBLElBQUksRUFBQztBQUE1QixTQUNNN0IsU0FETixDQURKO0FBS0g7O0FBRUQsV0FDSSwwQ0FDSSw2QkFBQyxXQUFEO0FBQWEsTUFBQSxhQUFhLEVBQUVvQyxhQUE1QjtBQUNJLE1BQUEsaUJBQWlCLEVBQUUsS0FBS0g7QUFENUIsTUFESixFQUlNSixZQUpOLENBREo7QUFRSDtBQTlEOEMsQ0FBakIsQ0FBM0I7O0FBaUVBLE1BQU1TLGNBQWMsR0FBRywrQkFBaUI7QUFDM0NoRCxFQUFBQSxXQUFXLEVBQUUsZ0JBRDhCO0FBRzNDQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsVUFBVSxFQUFFO0FBRFAsR0FIa0M7QUFPM0NDLEVBQUFBLFNBQVMsRUFBRTtBQUNQSyxJQUFBQSxjQUFjLEVBQUVILG1CQUFVSSxJQUFWLENBQWVGLFVBRHhCO0FBRVBtQyxJQUFBQSxXQUFXLEVBQUVyQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFGdkI7QUFHUEcsSUFBQUEsU0FBUyxFQUFFTCxtQkFBVU0sTUFIZDtBQUlQQyxJQUFBQSxJQUFJLEVBQUVQLG1CQUFVUSxJQUpUO0FBS1BvQyxJQUFBQSxZQUFZLEVBQUU1QyxtQkFBVVEsSUFMakI7QUFNUEMsSUFBQUEsYUFBYSxFQUFFVCxtQkFBVUksSUFBVixDQUFlRjtBQU52QixHQVBnQztBQWdCM0NRLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsU0FBS0MsS0FBTCxDQUFXRixhQUFYLENBQXlCaEIsYUFBekI7QUFDSCxHQWxCMEM7QUFvQjNDO0FBQ0FvRCxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLFVBQU1DLFdBQVcsR0FBRyxLQUFLbkMsS0FBTCxDQUFXMEIsV0FBWCxDQUF1QlUsUUFBdkIsSUFBbUMsRUFBdkQ7O0FBQ0EsVUFBTUMsUUFBUSxHQUFHQyx1QkFBY0MsUUFBZCxDQUF1QixVQUF2QixDQUFqQjs7QUFDQSxVQUFNQyxXQUFXLEdBQUcsRUFBcEI7QUFDQSxVQUFNQyxjQUFjLEdBQUcsRUFBdkI7O0FBQ0EsU0FBSyxNQUFNQyxRQUFYLElBQXVCQyxNQUFNLENBQUNDLElBQVAsQ0FBWVQsV0FBWixDQUF2QixFQUFpRDtBQUM3QyxZQUFNVSxNQUFNLEdBQUdWLFdBQVcsQ0FBQ08sUUFBRCxDQUExQixDQUQ2QyxDQUc3QztBQUNBO0FBQ0E7O0FBQ0EsVUFBSUksVUFBVSxHQUFHRCxNQUFNLENBQUNSLFFBQUQsQ0FBdkI7QUFDQSxVQUFJLENBQUNTLFVBQUwsRUFBaUJBLFVBQVUsR0FBR0QsTUFBTSxDQUFDLElBQUQsQ0FBbkI7O0FBQ2pCLFVBQUksQ0FBQ0MsVUFBTCxFQUFpQjtBQUNiO0FBQ0EsY0FBTUMsU0FBUyxHQUFHSixNQUFNLENBQUNDLElBQVAsQ0FBWUMsTUFBWixFQUFvQkcsSUFBcEIsQ0FBeUI1QyxDQUFDLElBQUlBLENBQUMsS0FBSyxTQUFwQyxDQUFsQjtBQUNBMEMsUUFBQUEsVUFBVSxHQUFHRCxNQUFNLENBQUNFLFNBQUQsQ0FBbkI7QUFDSDs7QUFDRCxVQUFJLENBQUNELFVBQUwsRUFBaUIsTUFBTSxJQUFJRyxLQUFKLENBQVUsMENBQVYsQ0FBTjtBQUVqQlQsTUFBQUEsV0FBVyxDQUFDRSxRQUFELENBQVgsR0FBd0IsS0FBeEI7QUFFQUksTUFBQUEsVUFBVSxDQUFDSSxFQUFYLEdBQWdCUixRQUFoQjtBQUNBRCxNQUFBQSxjQUFjLENBQUNVLElBQWYsQ0FBb0JMLFVBQXBCO0FBQ0g7O0FBRUQsU0FBS2hDLFFBQUwsQ0FBYztBQUNWLHlCQUFtQjBCLFdBRFQ7QUFFVixrQkFBWUM7QUFGRixLQUFkO0FBSUgsR0F0RTBDO0FBd0UzQ1csRUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEIsU0FBS0MsVUFBTDtBQUNILEdBMUUwQztBQTRFM0NDLEVBQUFBLGFBQWEsRUFBRSxVQUFTWixRQUFULEVBQW1CO0FBQzlCLFVBQU1hLFVBQVUsR0FBRyxFQUFuQjs7QUFDQSxTQUFLLE1BQU1WLE1BQVgsSUFBcUIsS0FBS2xDLEtBQUwsQ0FBV3lCLFFBQWhDLEVBQTBDO0FBQ3RDLFVBQUlvQixPQUFPLEdBQUcsS0FBSzdDLEtBQUwsQ0FBVzhDLGVBQVgsQ0FBMkJaLE1BQU0sQ0FBQ0ssRUFBbEMsQ0FBZDtBQUNBLFVBQUlMLE1BQU0sQ0FBQ0ssRUFBUCxLQUFjUixRQUFsQixFQUE0QmMsT0FBTyxHQUFHLENBQUNBLE9BQVg7QUFFNUJELE1BQUFBLFVBQVUsQ0FBQ1YsTUFBTSxDQUFDSyxFQUFSLENBQVYsR0FBd0JNLE9BQXhCO0FBQ0g7O0FBQ0QsU0FBSzFDLFFBQUwsQ0FBYztBQUFDLHlCQUFtQnlDO0FBQXBCLEtBQWQ7QUFDSCxHQXJGMEM7QUF1RjNDRixFQUFBQSxVQUFVLEVBQUUsWUFBVztBQUNuQixRQUFJSyxVQUFVLEdBQUcsSUFBakI7O0FBQ0EsU0FBSyxNQUFNYixNQUFYLElBQXFCLEtBQUtsQyxLQUFMLENBQVd5QixRQUFoQyxFQUEwQztBQUN0QyxZQUFNb0IsT0FBTyxHQUFHLEtBQUs3QyxLQUFMLENBQVc4QyxlQUFYLENBQTJCWixNQUFNLENBQUNLLEVBQWxDLENBQWhCO0FBQ0FRLE1BQUFBLFVBQVUsR0FBR0EsVUFBVSxJQUFJRixPQUEzQjtBQUNIOztBQUVELFFBQUlFLFVBQUosRUFBZ0IsS0FBSzFELEtBQUwsQ0FBV1IsY0FBWCxDQUEwQjtBQUFDYyxNQUFBQSxJQUFJLEVBQUUwQixjQUFjLENBQUM5QztBQUF0QixLQUExQixFQUFoQixLQUNLLEtBQUs0QixRQUFMLENBQWM7QUFBQ3BCLE1BQUFBLFNBQVMsRUFBRSx5QkFBRywyREFBSDtBQUFaLEtBQWQ7QUFDUixHQWhHMEM7QUFrRzNDdUIsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJLEtBQUtqQixLQUFMLENBQVdKLElBQWYsRUFBcUI7QUFDakIsWUFBTXdCLE1BQU0sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFmO0FBQ0EsYUFBTyw2QkFBQyxNQUFELE9BQVA7QUFDSDs7QUFFRCxVQUFNcUMsVUFBVSxHQUFHLEVBQW5CO0FBQ0EsUUFBSUQsVUFBVSxHQUFHLElBQWpCOztBQUNBLFNBQUssTUFBTWIsTUFBWCxJQUFxQixLQUFLbEMsS0FBTCxDQUFXeUIsUUFBaEMsRUFBMEM7QUFDdEMsWUFBTW9CLE9BQU8sR0FBRyxLQUFLN0MsS0FBTCxDQUFXOEMsZUFBWCxDQUEyQlosTUFBTSxDQUFDSyxFQUFsQyxDQUFoQjtBQUNBUSxNQUFBQSxVQUFVLEdBQUdBLFVBQVUsSUFBSUYsT0FBM0I7QUFFQUcsTUFBQUEsVUFBVSxDQUFDUixJQUFYLENBQ0k7QUFBTyxRQUFBLEdBQUcsRUFBRSxxQkFBcUJOLE1BQU0sQ0FBQ0ssRUFBeEM7QUFBNEMsUUFBQSxTQUFTLEVBQUM7QUFBdEQsU0FDSTtBQUFPLFFBQUEsSUFBSSxFQUFDLFVBQVo7QUFBdUIsUUFBQSxRQUFRLEVBQUUsTUFBTSxLQUFLSSxhQUFMLENBQW1CVCxNQUFNLENBQUNLLEVBQTFCLENBQXZDO0FBQXNFLFFBQUEsT0FBTyxFQUFFTTtBQUEvRSxRQURKLEVBRUk7QUFBRyxRQUFBLElBQUksRUFBRVgsTUFBTSxDQUFDZSxHQUFoQjtBQUFxQixRQUFBLE1BQU0sRUFBQyxRQUE1QjtBQUFxQyxRQUFBLEdBQUcsRUFBQztBQUF6QyxTQUFpRWYsTUFBTSxDQUFDZ0IsSUFBeEUsQ0FGSixDQURKO0FBTUg7O0FBRUQsUUFBSXRDLFlBQUo7O0FBQ0EsUUFBSSxLQUFLdkIsS0FBTCxDQUFXTixTQUFYLElBQXdCLEtBQUtpQixLQUFMLENBQVdqQixTQUF2QyxFQUFrRDtBQUM5QzZCLE1BQUFBLFlBQVksR0FDUjtBQUFLLFFBQUEsU0FBUyxFQUFDLE9BQWY7QUFBdUIsUUFBQSxJQUFJLEVBQUM7QUFBNUIsU0FDTSxLQUFLdkIsS0FBTCxDQUFXTixTQUFYLElBQXdCLEtBQUtpQixLQUFMLENBQVdqQixTQUR6QyxDQURKO0FBS0g7O0FBRUQsUUFBSW9FLFlBQUo7O0FBQ0EsUUFBSSxLQUFLOUQsS0FBTCxDQUFXaUMsWUFBWCxLQUE0QixLQUFoQyxFQUF1QztBQUNuQztBQUNBNkIsTUFBQUEsWUFBWSxHQUFHO0FBQVEsUUFBQSxTQUFTLEVBQUMsZ0VBQWxCO0FBQ1EsUUFBQSxPQUFPLEVBQUUsS0FBS1QsVUFEdEI7QUFDa0MsUUFBQSxRQUFRLEVBQUUsQ0FBQ0s7QUFEN0MsU0FDMEQseUJBQUcsUUFBSCxDQUQxRCxDQUFmO0FBRUg7O0FBRUQsV0FDSSwwQ0FDSSx3Q0FBSSx5QkFBRywyREFBSCxDQUFKLENBREosRUFFTUMsVUFGTixFQUdNcEMsWUFITixFQUlNdUMsWUFKTixDQURKO0FBUUg7QUE5STBDLENBQWpCLENBQXZCOztBQWlKQSxNQUFNQyxzQkFBc0IsR0FBRywrQkFBaUI7QUFDbkQvRSxFQUFBQSxXQUFXLEVBQUUsd0JBRHNDO0FBR25EQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsVUFBVSxFQUFFO0FBRFAsR0FIMEM7QUFPbkRDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQUR4QjtBQUVQQyxJQUFBQSxjQUFjLEVBQUVILG1CQUFVSSxJQUFWLENBQWVGLFVBRnhCO0FBR1B5RSxJQUFBQSxhQUFhLEVBQUUzRSxtQkFBVU0sTUFBVixDQUFpQkosVUFIekI7QUFJUDBFLElBQUFBLFlBQVksRUFBRTVFLG1CQUFVTSxNQUFWLENBQWlCSixVQUp4QjtBQUtQMkUsSUFBQUEsTUFBTSxFQUFFN0UsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBTGxCO0FBTVA0RSxJQUFBQSxVQUFVLEVBQUU5RSxtQkFBVUMsTUFBVixDQUFpQkMsVUFOdEI7QUFPUDZFLElBQUFBLElBQUksRUFBRS9FLG1CQUFVSSxJQUFWLENBQWVGLFVBUGQ7QUFRUDhFLElBQUFBLFdBQVcsRUFBRWhGLG1CQUFVSSxJQUFWLENBQWVGLFVBUnJCO0FBU1BPLElBQUFBLGFBQWEsRUFBRVQsbUJBQVVJLElBQVYsQ0FBZUY7QUFUdkIsR0FQd0M7QUFtQm5EUSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLEtBQUwsQ0FBV0YsYUFBWCxDQUF5QmhCLGFBQXpCO0FBQ0gsR0FyQmtEO0FBdUJuRG1CLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSHFFLE1BQUFBLGVBQWUsRUFBRTtBQURkLEtBQVA7QUFHSCxHQTNCa0Q7QUE2Qm5EckQsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJLEtBQUtOLEtBQUwsQ0FBVzJELGVBQWYsRUFBZ0M7QUFDNUIsWUFBTWxELE1BQU0sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFmO0FBQ0EsYUFBTyw2QkFBQyxNQUFELE9BQVA7QUFDSCxLQUhELE1BR087QUFDSCxhQUNJLDBDQUNJLHdDQUFLLHlCQUFHLDRDQUFILEVBQ0Q7QUFBRWlELFFBQUFBLFlBQVksRUFBR0MsR0FBRCxJQUFTLHdDQUFLLEtBQUt4RSxLQUFMLENBQVdrRSxNQUFYLENBQWtCSyxZQUF2QjtBQUF6QixPQURDLENBQUwsQ0FESixFQUtJLHdDQUFLLHlCQUFHLG1EQUFILENBQUwsQ0FMSixDQURKO0FBU0g7QUFDSjtBQTVDa0QsQ0FBakIsQ0FBL0I7O0FBK0NBLE1BQU1FLGVBQWUsR0FBRywrQkFBaUI7QUFDNUN6RixFQUFBQSxXQUFXLEVBQUUsaUJBRCtCO0FBRzVDQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsVUFBVSxFQUFFO0FBRFAsR0FIbUM7QUFPNUNDLEVBQUFBLFNBQVMsRUFBRTtBQUNQK0UsSUFBQUEsTUFBTSxFQUFFN0UsbUJBQVVxRixLQUFWLENBQWdCO0FBQ3BCQyxNQUFBQSxZQUFZLEVBQUV0RixtQkFBVU0sTUFESjtBQUVwQmlGLE1BQUFBLFdBQVcsRUFBRXZGLG1CQUFVTTtBQUZILEtBQWhCLENBREQ7QUFLUHlFLElBQUFBLElBQUksRUFBRS9FLG1CQUFVSSxJQUxUO0FBTVB3RSxJQUFBQSxZQUFZLEVBQUU1RSxtQkFBVUksSUFOakI7QUFPUEQsSUFBQUEsY0FBYyxFQUFFSCxtQkFBVUksSUFBVixDQUFlRixVQVB4QjtBQVFQSCxJQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxNQVJqQjtBQVNQUSxJQUFBQSxhQUFhLEVBQUVULG1CQUFVSSxJQUFWLENBQWVGO0FBVHZCLEdBUGlDO0FBbUI1Q1UsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNINEUsTUFBQUEsS0FBSyxFQUFFLEVBREo7QUFFSFAsTUFBQUEsZUFBZSxFQUFFO0FBRmQsS0FBUDtBQUlILEdBeEIyQztBQTBCNUN2RSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLEtBQUwsQ0FBV0YsYUFBWCxDQUF5QmhCLGFBQXpCO0FBRUEsU0FBS2dHLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxTQUFLQyxJQUFMLEdBQVksSUFBWjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUVBLFNBQUtuRSxRQUFMLENBQWM7QUFBQ3dELE1BQUFBLGVBQWUsRUFBRTtBQUFsQixLQUFkOztBQUNBLFNBQUtZLG1CQUFMLEdBQTJCQyxLQUEzQixDQUFrQy9FLENBQUQsSUFBTztBQUNwQyxXQUFLSixLQUFMLENBQVdvRSxJQUFYLENBQWdCaEUsQ0FBaEI7QUFDSCxLQUZELEVBRUdnRixPQUZILENBRVcsTUFBTTtBQUNiLFdBQUt0RSxRQUFMLENBQWM7QUFBQ3dELFFBQUFBLGVBQWUsRUFBRTtBQUFsQixPQUFkO0FBQ0gsS0FKRDtBQUtILEdBeEMyQzs7QUEwQzVDOzs7QUFHQVksRUFBQUEsbUJBQW1CLEVBQUUsWUFBVztBQUM1QixXQUFPLEtBQUtsRixLQUFMLENBQVdaLFlBQVgsQ0FBd0JpRywwQkFBeEIsQ0FDSCxLQUFLckYsS0FBTCxDQUFXa0UsTUFBWCxDQUFrQlMsWUFEZixFQUVILEtBQUszRSxLQUFMLENBQVdrRSxNQUFYLENBQWtCVSxXQUZmLEVBR0gsS0FBSzVFLEtBQUwsQ0FBV2lFLFlBSFIsRUFJSCxDQUpHLENBSUE7QUFKQSxNQUtMcUIsSUFMSyxDQUtDQyxNQUFELElBQVk7QUFDZixXQUFLVCxVQUFMLEdBQWtCUyxNQUFNLENBQUNDLFVBQXpCO0FBQ0EsV0FBS1QsSUFBTCxHQUFZUSxNQUFNLENBQUNFLEdBQW5CO0FBQ0EsV0FBS1QsT0FBTCxHQUFlTyxNQUFNLENBQUNHLE1BQXRCO0FBQ0gsS0FUTSxDQUFQO0FBVUgsR0F4RDJDO0FBMEQ1Q0MsRUFBQUEsY0FBYyxFQUFFLFVBQVN2RixDQUFULEVBQVk7QUFDeEIsU0FBS1UsUUFBTCxDQUFjO0FBQ1YrRCxNQUFBQSxLQUFLLEVBQUV6RSxDQUFDLENBQUNXLE1BQUYsQ0FBU0M7QUFETixLQUFkO0FBR0gsR0E5RDJDO0FBZ0U1QzRFLEVBQUFBLGFBQWEsRUFBRSxnQkFBZXhGLENBQWYsRUFBa0I7QUFDN0JBLElBQUFBLENBQUMsQ0FBQ0MsY0FBRjtBQUNBLFFBQUksS0FBS00sS0FBTCxDQUFXa0UsS0FBWCxJQUFvQixFQUF4QixFQUE0QjtBQUU1QixTQUFLL0QsUUFBTCxDQUFjO0FBQ1ZwQixNQUFBQSxTQUFTLEVBQUU7QUFERCxLQUFkOztBQUlBLFFBQUk7QUFDQSxZQUFNbUcscUJBQXFCLEdBQ3ZCLE1BQU0sS0FBSzdGLEtBQUwsQ0FBV1osWUFBWCxDQUF3QjBHLDhCQUF4QixFQURWO0FBRUEsVUFBSVAsTUFBSjs7QUFDQSxVQUFJLEtBQUtULFVBQVQsRUFBcUI7QUFDakJTLFFBQUFBLE1BQU0sR0FBRyxNQUFNLEtBQUt2RixLQUFMLENBQVdaLFlBQVgsQ0FBd0IyRyx5QkFBeEIsQ0FDWCxLQUFLakIsVUFETSxFQUNNLEtBQUtDLElBRFgsRUFDaUIsS0FBSy9FLEtBQUwsQ0FBV2lFLFlBRDVCLEVBQzBDLEtBQUt0RCxLQUFMLENBQVdrRSxLQURyRCxDQUFmO0FBR0gsT0FKRCxNQUlPLElBQUlnQixxQkFBSixFQUEyQjtBQUM5Qk4sUUFBQUEsTUFBTSxHQUFHLE1BQU0sS0FBS3ZGLEtBQUwsQ0FBV1osWUFBWCxDQUF3QjRHLGlCQUF4QixDQUNYLEtBQUtqQixJQURNLEVBQ0EsS0FBSy9FLEtBQUwsQ0FBV2lFLFlBRFgsRUFDeUIsS0FBS3RELEtBQUwsQ0FBV2tFLEtBRHBDLENBQWY7QUFHSCxPQUpNLE1BSUE7QUFDSCxjQUFNLElBQUk1QixLQUFKLENBQVUsb0RBQVYsQ0FBTjtBQUNIOztBQUNELFVBQUlzQyxNQUFNLENBQUNVLE9BQVgsRUFBb0I7QUFDaEIsY0FBTUMsS0FBSyxHQUFHO0FBQ1ZULFVBQUFBLEdBQUcsRUFBRSxLQUFLVixJQURBO0FBRVZvQixVQUFBQSxhQUFhLEVBQUUsS0FBS25HLEtBQUwsQ0FBV2lFO0FBRmhCLFNBQWQ7O0FBSUEsWUFBSTRCLHFCQUFKLEVBQTJCO0FBQ3ZCLGdCQUFNTyxpQkFBaUIsR0FBR3hDLGFBQUl5QyxLQUFKLENBQ3RCLEtBQUtyRyxLQUFMLENBQVdaLFlBQVgsQ0FBd0JrSCxvQkFBeEIsRUFEc0IsQ0FBMUI7O0FBR0FKLFVBQUFBLEtBQUssQ0FBQ0ssU0FBTixHQUFrQkgsaUJBQWlCLENBQUNJLElBQXBDO0FBQ0g7O0FBQ0QsYUFBS3hHLEtBQUwsQ0FBV1IsY0FBWCxDQUEwQjtBQUN0QmMsVUFBQUEsSUFBSSxFQUFFbUUsZUFBZSxDQUFDdkYsVUFEQTtBQUV0QjtBQUNBO0FBQ0F1SCxVQUFBQSxjQUFjLEVBQUVQLEtBSk07QUFLdEJRLFVBQUFBLGFBQWEsRUFBRVI7QUFMTyxTQUExQjtBQU9ILE9BbEJELE1Ba0JPO0FBQ0gsYUFBS3BGLFFBQUwsQ0FBYztBQUNWcEIsVUFBQUEsU0FBUyxFQUFFLHlCQUFHLGlCQUFIO0FBREQsU0FBZDtBQUdIO0FBQ0osS0F0Q0QsQ0FzQ0UsT0FBT1UsQ0FBUCxFQUFVO0FBQ1IsV0FBS0osS0FBTCxDQUFXb0UsSUFBWCxDQUFnQmhFLENBQWhCO0FBQ0F1RyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSwrQkFBWjtBQUNIO0FBQ0osR0FsSDJDO0FBb0g1QzNGLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsUUFBSSxLQUFLTixLQUFMLENBQVcyRCxlQUFmLEVBQWdDO0FBQzVCLFlBQU1sRCxNQUFNLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBZjtBQUNBLGFBQU8sNkJBQUMsTUFBRCxPQUFQO0FBQ0gsS0FIRCxNQUdPO0FBQ0gsWUFBTXVGLFlBQVksR0FBR0MsT0FBTyxDQUFDLEtBQUtuRyxLQUFMLENBQVdrRSxLQUFaLENBQTVCO0FBQ0EsWUFBTWtDLGFBQWEsR0FBRyx5QkFBVztBQUM3QkMsUUFBQUEsOENBQThDLEVBQUUsSUFEbkI7QUFFN0JDLFFBQUFBLGdCQUFnQixFQUFFO0FBRlcsT0FBWCxDQUF0QjtBQUlBLFVBQUkxRixZQUFKOztBQUNBLFVBQUksS0FBS1osS0FBTCxDQUFXakIsU0FBZixFQUEwQjtBQUN0QjZCLFFBQUFBLFlBQVksR0FDUjtBQUFLLFVBQUEsU0FBUyxFQUFDLE9BQWY7QUFBdUIsVUFBQSxJQUFJLEVBQUM7QUFBNUIsV0FDTSxLQUFLWixLQUFMLENBQVdqQixTQURqQixDQURKO0FBS0g7O0FBQ0QsYUFDSSwwQ0FDSSx3Q0FBSyx5QkFBRyw0Q0FBSCxFQUNEO0FBQUVnRyxRQUFBQSxNQUFNLEVBQUUsd0NBQUssS0FBS1YsT0FBVjtBQUFWLE9BREMsQ0FBTCxDQURKLEVBS0ksd0NBQUsseUJBQUcsb0NBQUgsQ0FBTCxDQUxKLEVBTUk7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBTSxRQUFBLFFBQVEsRUFBRSxLQUFLWTtBQUFyQixTQUNJO0FBQU8sUUFBQSxJQUFJLEVBQUMsTUFBWjtBQUNJLFFBQUEsU0FBUyxFQUFDLCtDQURkO0FBRUksUUFBQSxLQUFLLEVBQUUsS0FBS2pGLEtBQUwsQ0FBV2tFLEtBRnRCO0FBR0ksUUFBQSxRQUFRLEVBQUUsS0FBS2MsY0FIbkI7QUFJSSxzQkFBYSx5QkFBRyxNQUFIO0FBSmpCLFFBREosRUFPSSx3Q0FQSixFQVFJO0FBQU8sUUFBQSxJQUFJLEVBQUMsUUFBWjtBQUFxQixRQUFBLEtBQUssRUFBRSx5QkFBRyxRQUFILENBQTVCO0FBQ0ksUUFBQSxTQUFTLEVBQUVvQixhQURmO0FBRUksUUFBQSxRQUFRLEVBQUUsQ0FBQ0Y7QUFGZixRQVJKLENBREosRUFjS3RGLFlBZEwsQ0FOSixDQURKO0FBeUJIO0FBQ0o7QUFoSzJDLENBQWpCLENBQXhCOzs7QUFtS0EsTUFBTTJGLFlBQU4sU0FBMkJDLGVBQU1DLFNBQWpDLENBQTJDO0FBZ0JwQjtBQUNDO0FBSTNCQyxFQUFBQSxXQUFXLENBQUNySCxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOLEVBRGUsQ0FHZjtBQUNBOztBQUplO0FBQUEsNERBbUJBLE1BQU07QUFDckI7QUFDQTtBQUNBO0FBRUFzSCxNQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWSxLQUFLQyxPQUFqQixFQUEwQixRQUExQjtBQUNBLFdBQUsxRyxRQUFMLENBQWM7QUFBQzJHLFFBQUFBLEtBQUssRUFBRVAsWUFBWSxDQUFDUTtBQUFyQixPQUFkO0FBQ0EsV0FBSzFILEtBQUwsQ0FBV0YsYUFBWCxDQUF5Qm9ILFlBQVksQ0FBQ1EsY0FBdEM7QUFDSCxLQTNCa0I7QUFBQSwwREE2QkYsTUFBTTtBQUNuQixXQUFLMUgsS0FBTCxDQUFXUixjQUFYLENBQTBCLEVBQTFCO0FBQ0gsS0EvQmtCO0FBS2YsU0FBS2dJLE9BQUwsR0FBZXhILEtBQUssQ0FBQ1osWUFBTixDQUFtQnVJLGtCQUFuQixDQUNYLEtBQUszSCxLQUFMLENBQVc0SCxTQURBLEVBRVgsS0FBSzVILEtBQUwsQ0FBV2dFLGFBRkEsQ0FBZjtBQUtBLFNBQUtyRCxLQUFMLEdBQWE7QUFDVDhHLE1BQUFBLEtBQUssRUFBRVAsWUFBWSxDQUFDVztBQURYLEtBQWI7QUFHSDs7QUFFRDlILEVBQUFBLGlCQUFpQjtBQUFBO0FBQVM7QUFDdEIsU0FBS0MsS0FBTCxDQUFXRixhQUFYLENBQXlCb0gsWUFBWSxDQUFDVyxhQUF0QztBQUNIOztBQWdCRDVHLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUk2RyxjQUFjLEdBQUcsSUFBckI7O0FBQ0EsVUFBTUMsWUFBWSxHQUNkLDZCQUFDLHlCQUFEO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBSy9ILEtBQUwsQ0FBV2dJLFFBRHhCO0FBRUksTUFBQSxJQUFJLEVBQUUsS0FBS2hJLEtBQUwsQ0FBV2lJLFlBQVgsR0FBMkIsS0FBS2pJLEtBQUwsQ0FBV2lJLFlBQVgsR0FBMEIsVUFBckQsR0FBbUU7QUFGN0UsT0FHRSx5QkFBRyxRQUFILENBSEYsQ0FESjs7QUFNQSxRQUFJLEtBQUt0SCxLQUFMLENBQVc4RyxLQUFYLEtBQXFCUCxZQUFZLENBQUNXLGFBQXRDLEVBQXFEO0FBQ2pEQyxNQUFBQSxjQUFjLEdBQ1YsNkJBQUMseUJBQUQ7QUFDSSxRQUFBLE9BQU8sRUFBRSxLQUFLSSxnQkFEbEI7QUFFSSxRQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXaUksWUFBWCxJQUEyQjtBQUZyQyxTQUdFLEtBQUtqSSxLQUFMLENBQVdtSSxZQUFYLElBQTJCLHlCQUFHLGdCQUFILENBSDdCLENBREo7QUFNSCxLQVBELE1BT087QUFDSEwsTUFBQUEsY0FBYyxHQUNWLDZCQUFDLHlCQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUUsS0FBS00sY0FEbEI7QUFFSSxRQUFBLElBQUksRUFBRSxLQUFLcEksS0FBTCxDQUFXaUksWUFBWCxJQUEyQjtBQUZyQyxTQUdFLEtBQUtqSSxLQUFMLENBQVdtSSxZQUFYLElBQTJCLHlCQUFHLFNBQUgsQ0FIN0IsQ0FESjtBQU1IOztBQUVELFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ZKLFlBREUsRUFFRkQsY0FGRSxDQUFQO0FBSUg7O0FBbEY2Qzs7OzhCQUFyQ1osWSxlQUNVO0FBQ2Y5SCxFQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQURoQjtBQUVmeUUsRUFBQUEsYUFBYSxFQUFFM0UsbUJBQVVNLE1BQVYsQ0FBaUJKLFVBRmpCO0FBR2ZxSSxFQUFBQSxTQUFTLEVBQUV2SSxtQkFBVU0sTUFBVixDQUFpQkosVUFIYjtBQUlmQyxFQUFBQSxjQUFjLEVBQUVILG1CQUFVSSxJQUFWLENBQWVGLFVBSmhCO0FBS2ZHLEVBQUFBLFNBQVMsRUFBRUwsbUJBQVVNLE1BTE47QUFNZkcsRUFBQUEsYUFBYSxFQUFFVCxtQkFBVUksSUFBVixDQUFlRixVQU5mO0FBT2Y0SSxFQUFBQSxZQUFZLEVBQUU5SSxtQkFBVU0sTUFQVDtBQVFmc0ksRUFBQUEsWUFBWSxFQUFFNUksbUJBQVVNLE1BUlQ7QUFTZnFJLEVBQUFBLFFBQVEsRUFBRTNJLG1CQUFVSTtBQVRMLEM7OEJBRFZ5SCxZLGdCQWFXLGE7OEJBYlhBLFkseUJBY29CLHNCOzhCQWRwQkEsWSxtQkFnQmMsQzs4QkFoQmRBLFksb0JBaUJlLEM7QUFvRXJCLE1BQU1tQixpQkFBaUIsR0FBRywrQkFBaUI7QUFDOUNySixFQUFBQSxXQUFXLEVBQUUsbUJBRGlDO0FBRzlDRyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsWUFBWSxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEeEI7QUFFUHlFLElBQUFBLGFBQWEsRUFBRTNFLG1CQUFVTSxNQUFWLENBQWlCSixVQUZ6QjtBQUdQcUksSUFBQUEsU0FBUyxFQUFFdkksbUJBQVVNLE1BQVYsQ0FBaUJKLFVBSHJCO0FBSVBDLElBQUFBLGNBQWMsRUFBRUgsbUJBQVVJLElBQVYsQ0FBZUYsVUFKeEI7QUFLUEcsSUFBQUEsU0FBUyxFQUFFTCxtQkFBVU0sTUFMZDtBQU1QRyxJQUFBQSxhQUFhLEVBQUVULG1CQUFVSSxJQUFWLENBQWVGO0FBTnZCLEdBSG1DO0FBWTlDUSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLEtBQUwsQ0FBV0YsYUFBWCxDQUF5QmhCLGFBQXpCO0FBQ0gsR0FkNkM7QUFnQjlDO0FBQ0F3SixFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDO0FBQ0E7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLElBQXBCO0FBQ0FqQixJQUFBQSxNQUFNLENBQUNrQixnQkFBUCxDQUF3QixTQUF4QixFQUFtQyxLQUFLQyxpQkFBeEM7QUFFQSxTQUFLQyxlQUFMLEdBQXVCLHVCQUF2QjtBQUNILEdBeEI2QztBQTBCOUNDLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0JyQixJQUFBQSxNQUFNLENBQUNzQixtQkFBUCxDQUEyQixTQUEzQixFQUFzQyxLQUFLSCxpQkFBM0M7O0FBQ0EsUUFBSSxLQUFLRixZQUFULEVBQXVCO0FBQ25CLFdBQUtBLFlBQUwsQ0FBa0JNLEtBQWxCO0FBQ0g7QUFDSixHQS9CNkM7QUFpQzlDQyxFQUFBQSxLQUFLLEVBQUUsWUFBVztBQUNkLFFBQUksS0FBS0osZUFBTCxDQUFxQkssT0FBekIsRUFBa0M7QUFDOUIsV0FBS0wsZUFBTCxDQUFxQkssT0FBckIsQ0FBNkJELEtBQTdCO0FBQ0g7QUFDSixHQXJDNkM7QUF1QzlDRSxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTNUksQ0FBVCxFQUFZO0FBQzlCQSxJQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFDQUQsSUFBQUEsQ0FBQyxDQUFDNkksZUFBRjtBQUVBLFVBQU1yRixHQUFHLEdBQUcsS0FBSzVELEtBQUwsQ0FBV1osWUFBWCxDQUF3QnVJLGtCQUF4QixDQUNSLEtBQUszSCxLQUFMLENBQVc0SCxTQURILEVBRVIsS0FBSzVILEtBQUwsQ0FBV2dFLGFBRkgsQ0FBWjtBQUlBLFNBQUt1RSxZQUFMLEdBQW9CakIsTUFBTSxDQUFDQyxJQUFQLENBQVkzRCxHQUFaLENBQXBCO0FBQ0EsU0FBSzJFLFlBQUwsQ0FBa0JXLE1BQWxCLEdBQTJCLElBQTNCO0FBQ0gsR0FqRDZDO0FBbUQ5Q1QsRUFBQUEsaUJBQWlCLEVBQUUsVUFBU1UsS0FBVCxFQUFnQjtBQUMvQixRQUNJQSxLQUFLLENBQUNDLElBQU4sS0FBZSxVQUFmLElBQ0FELEtBQUssQ0FBQ0UsTUFBTixLQUFpQixLQUFLckosS0FBTCxDQUFXWixZQUFYLENBQXdCa0ssZ0JBQXhCLEVBRnJCLEVBR0U7QUFDRSxXQUFLdEosS0FBTCxDQUFXUixjQUFYLENBQTBCLEVBQTFCO0FBQ0g7QUFDSixHQTFENkM7QUE0RDlDeUIsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJTSxZQUFKOztBQUNBLFFBQUksS0FBS3ZCLEtBQUwsQ0FBV04sU0FBZixFQUEwQjtBQUN0QjZCLE1BQUFBLFlBQVksR0FDUjtBQUFLLFFBQUEsU0FBUyxFQUFDLE9BQWY7QUFBdUIsUUFBQSxJQUFJLEVBQUM7QUFBNUIsU0FDTSxLQUFLdkIsS0FBTCxDQUFXTixTQURqQixDQURKO0FBS0g7O0FBQ0QsV0FDSSwwQ0FDSTtBQUFHLE1BQUEsSUFBSSxFQUFDLEVBQVI7QUFBVyxNQUFBLEdBQUcsRUFBRSxLQUFLZ0osZUFBckI7QUFBc0MsTUFBQSxPQUFPLEVBQUUsS0FBS007QUFBcEQsT0FBNEUseUJBQUcsc0JBQUgsQ0FBNUUsQ0FESixFQUVLekgsWUFGTCxDQURKO0FBTUg7QUEzRTZDLENBQWpCLENBQTFCOztBQThFUCxNQUFNZ0ksbUJBQW1CLEdBQUcsQ0FDeEJ4SyxpQkFEd0IsRUFFeEIwQyxrQkFGd0IsRUFHeEJzQyxzQkFId0IsRUFJeEJVLGVBSndCLEVBS3hCekMsY0FMd0IsRUFNeEJrRixZQU53QixDQUE1Qjs7QUFTZSxTQUFTc0MsNkJBQVQsQ0FBdUM1QixTQUF2QyxFQUFrRDtBQUM3RCxPQUFLLE1BQU02QixDQUFYLElBQWdCRixtQkFBaEIsRUFBcUM7QUFDakMsUUFBSUUsQ0FBQyxDQUFDdkssVUFBRixLQUFpQjBJLFNBQWpCLElBQThCNkIsQ0FBQyxDQUFDQyxtQkFBRixLQUEwQjlCLFNBQTVELEVBQXVFO0FBQ25FLGFBQU82QixDQUFQO0FBQ0g7QUFDSjs7QUFDRCxTQUFPcEIsaUJBQVA7QUFDSCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB1cmwgZnJvbSAndXJsJztcclxuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcblxyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcblxyXG4vKiBUaGlzIGZpbGUgY29udGFpbnMgYSBjb2xsZWN0aW9uIG9mIGNvbXBvbmVudHMgd2hpY2ggYXJlIHVzZWQgYnkgdGhlXHJcbiAqIEludGVyYWN0aXZlQXV0aCB0byBwcm9tcHQgdGhlIHVzZXIgdG8gZW50ZXIgdGhlIGluZm9ybWF0aW9uIG5lZWRlZFxyXG4gKiBmb3IgYW4gYXV0aCBzdGFnZS4gKFRoZSBpbnRlbnRpb24gaXMgdGhhdCB0aGV5IGNvdWxkIGFsc28gYmUgdXNlZCBmb3Igb3RoZXJcclxuICogY29tcG9uZW50cywgc3VjaCBhcyB0aGUgcmVnaXN0cmF0aW9uIGZsb3cpLlxyXG4gKlxyXG4gKiBDYWxsIGdldEVudHJ5Q29tcG9uZW50Rm9yTG9naW5UeXBlKCkgdG8gZ2V0IGEgY29tcG9uZW50IHN1aXRhYmxlIGZvciBhXHJcbiAqIHBhcnRpY3VsYXIgbG9naW4gdHlwZS4gRWFjaCBjb21wb25lbnQgcmVxdWlyZXMgdGhlIHNhbWUgcHJvcGVydGllczpcclxuICpcclxuICogbWF0cml4Q2xpZW50OiAgICAgICAgICAgQSBtYXRyaXggY2xpZW50LiBNYXkgYmUgYSBkaWZmZXJlbnQgb25lIHRvIHRoZSBvbmVcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudGx5IGJlaW5nIHVzZWQgZ2VuZXJhbGx5IChlZy4gdG8gcmVnaXN0ZXIgd2l0aFxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICBvbmUgSFMgd2hpbHN0IGJlaWduIGEgZ3Vlc3Qgb24gYW5vdGhlcikuXHJcbiAqIGxvZ2luVHlwZTogICAgICAgICAgICAgIHRoZSBsb2dpbiB0eXBlIG9mIHRoZSBhdXRoIHN0YWdlIGJlaW5nIGF0dGVtcHRlZFxyXG4gKiBhdXRoU2Vzc2lvbklkOiAgICAgICAgICBzZXNzaW9uIGlkIGZyb20gdGhlIHNlcnZlclxyXG4gKiBjbGllbnRTZWNyZXQ6ICAgICAgICAgICBUaGUgY2xpZW50IHNlY3JldCBpbiB1c2UgZm9yIElEIHNlcnZlciBhdXRoIHNlc3Npb25zXHJcbiAqIHN0YWdlUGFyYW1zOiAgICAgICAgICAgIHBhcmFtcyBmcm9tIHRoZSBzZXJ2ZXIgZm9yIHRoZSBzdGFnZSBiZWluZyBhdHRlbXB0ZWRcclxuICogZXJyb3JUZXh0OiAgICAgICAgICAgICAgZXJyb3IgbWVzc2FnZSBmcm9tIGEgcHJldmlvdXMgYXR0ZW1wdCB0byBhdXRoZW50aWNhdGVcclxuICogc3VibWl0QXV0aERpY3Q6ICAgICAgICAgYSBmdW5jdGlvbiB3aGljaCB3aWxsIGJlIGNhbGxlZCB3aXRoIHRoZSBuZXcgYXV0aCBkaWN0XHJcbiAqIGJ1c3k6ICAgICAgICAgICAgICAgICAgIGEgYm9vbGVhbiBpbmRpY2F0aW5nIHdoZXRoZXIgdGhlIGF1dGggbG9naWMgaXMgZG9pbmcgc29tZXRoaW5nXHJcbiAqICAgICAgICAgICAgICAgICAgICAgICAgIHRoZSB1c2VyIG5lZWRzIHRvIHdhaXQgZm9yLlxyXG4gKiBpbnB1dHM6ICAgICAgICAgICAgICAgICBPYmplY3Qgb2YgaW5wdXRzIHByb3ZpZGVkIGJ5IHRoZSB1c2VyLCBhcyBpbiBqcy1zZGtcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgaW50ZXJhY3RpdmUtYXV0aFxyXG4gKiBzdGFnZVN0YXRlOiAgICAgICAgICAgICBTdGFnZS1zcGVjaWZpYyBvYmplY3QgdXNlZCBmb3IgY29tbXVuaWNhdGluZyBzdGF0ZSBpbmZvcm1hdGlvblxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICB0byB0aGUgVUkgZnJvbSB0aGUgc3RhdGUtc3BlY2lmaWMgYXV0aCBsb2dpYy5cclxuICogICAgICAgICAgICAgICAgICAgICAgICAgRGVmaW5lZCBrZXlzIGZvciBzdGFnZXMgYXJlOlxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbS5sb2dpbi5lbWFpbC5pZGVudGl0eTpcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqIGVtYWlsU2lkOiBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSBzaWQgb2YgdGhlIGFjdGl2ZVxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZlcmlmaWNhdGlvbiBzZXNzaW9uIGZyb20gdGhlIElEIHNlcnZlciwgb3JcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsIGlmIG5vIHNlc3Npb24gaXMgYWN0aXZlLlxyXG4gKiBmYWlsOiAgICAgICAgICAgICAgICAgICBhIGZ1bmN0aW9uIHdoaWNoIHNob3VsZCBiZSBjYWxsZWQgd2l0aCBhbiBlcnJvciBvYmplY3QgaWYgYW5cclxuICogICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3Igb2NjdXJyZWQgZHVyaW5nIHRoZSBhdXRoIHN0YWdlLiBUaGlzIHdpbGwgY2F1c2UgdGhlIGF1dGhcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvbiB0byBiZSBmYWlsZWQgYW5kIHRoZSBwcm9jZXNzIHRvIGdvIGJhY2sgdG8gdGhlIHN0YXJ0LlxyXG4gKiBzZXRFbWFpbFNpZDogICAgICAgICAgICBtLmxvZ2luLmVtYWlsLmlkZW50aXR5IG9ubHk6IGEgZnVuY3Rpb24gdG8gYmUgY2FsbGVkIHdpdGggdGhlXHJcbiAqICAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsIHNpZCBhZnRlciBhIHRva2VuIGlzIHJlcXVlc3RlZC5cclxuICogb25QaGFzZUNoYW5nZTogICAgICAgICAgQSBmdW5jdGlvbiB3aGljaCBpcyBjYWxsZWQgd2hlbiB0aGUgc3RhZ2UncyBwaGFzZSBjaGFuZ2VzLiBJZlxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICB0aGUgc3RhZ2UgaGFzIG5vIHBoYXNlcywgY2FsbCB0aGlzIHdpdGggREVGQVVMVF9QSEFTRS4gVGFrZXNcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgb25lIGFyZ3VtZW50LCB0aGUgcGhhc2UsIGFuZCBpcyBhbHdheXMgZGVmaW5lZC9yZXF1aXJlZC5cclxuICogY29udGludWVUZXh0OiAgICAgICAgICAgRm9yIHN0YWdlcyB3aGljaCBoYXZlIGEgY29udGludWUgYnV0dG9uLCB0aGUgdGV4dCB0byB1c2UuXHJcbiAqIGNvbnRpbnVlS2luZDogICAgICAgICAgIEZvciBzdGFnZXMgd2hpY2ggaGF2ZSBhIGNvbnRpbnVlIGJ1dHRvbiwgdGhlIHN0eWxlIG9mIGJ1dHRvbiB0b1xyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICB1c2UuIEZvciBleGFtcGxlLCAnZGFuZ2VyJyBvciAncHJpbWFyeScuXHJcbiAqIG9uQ2FuY2VsICAgICAgICAgICAgICAgIEEgZnVuY3Rpb24gd2l0aCBubyBhcmd1bWVudHMgd2hpY2ggaXMgY2FsbGVkIGJ5IHRoZSBzdGFnZSBpZiB0aGVcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgdXNlciBrbm93aW5nbHkgY2FuY2VsbGVkL2Rpc21pc3NlZCB0aGUgYXV0aGVudGljYXRpb24gYXR0ZW1wdC5cclxuICpcclxuICogRWFjaCBjb21wb25lbnQgbWF5IGFsc28gcHJvdmlkZSB0aGUgZm9sbG93aW5nIGZ1bmN0aW9ucyAoYmV5b25kIHRoZSBzdGFuZGFyZCBSZWFjdCBvbmVzKTpcclxuICogICAgZm9jdXM6IHNldCB0aGUgaW5wdXQgZm9jdXMgYXBwcm9wcmlhdGVseSBpbiB0aGUgZm9ybS5cclxuICovXHJcblxyXG5leHBvcnQgY29uc3QgREVGQVVMVF9QSEFTRSA9IDA7XHJcblxyXG5leHBvcnQgY29uc3QgUGFzc3dvcmRBdXRoRW50cnkgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnUGFzc3dvcmRBdXRoRW50cnknLFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBMT0dJTl9UWVBFOiBcIm0ubG9naW4ucGFzc3dvcmRcIixcclxuICAgIH0sXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgbWF0cml4Q2xpZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc3VibWl0QXV0aERpY3Q6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZXJyb3JUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIC8vIGlzIHRoZSBhdXRoIGxvZ2ljIGN1cnJlbnRseSB3YWl0aW5nIGZvciBzb21ldGhpbmcgdG9cclxuICAgICAgICAvLyBoYXBwZW4/XHJcbiAgICAgICAgYnVzeTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgb25QaGFzZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25QaGFzZUNoYW5nZShERUZBVUxUX1BIQVNFKTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBwYXNzd29yZDogXCJcIixcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25TdWJtaXQ6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuYnVzeSkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLnByb3BzLnN1Ym1pdEF1dGhEaWN0KHtcclxuICAgICAgICAgICAgdHlwZTogUGFzc3dvcmRBdXRoRW50cnkuTE9HSU5fVFlQRSxcclxuICAgICAgICAgICAgLy8gVE9ETzogUmVtb3ZlIGB1c2VyYCBvbmNlIHNlcnZlcnMgc3VwcG9ydCBwcm9wZXIgVUlBXHJcbiAgICAgICAgICAgIC8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8xMDMxMlxyXG4gICAgICAgICAgICB1c2VyOiB0aGlzLnByb3BzLm1hdHJpeENsaWVudC5jcmVkZW50aWFscy51c2VySWQsXHJcbiAgICAgICAgICAgIGlkZW50aWZpZXI6IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6IFwibS5pZC51c2VyXCIsXHJcbiAgICAgICAgICAgICAgICB1c2VyOiB0aGlzLnByb3BzLm1hdHJpeENsaWVudC5jcmVkZW50aWFscy51c2VySWQsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiB0aGlzLnN0YXRlLnBhc3N3b3JkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25QYXNzd29yZEZpZWxkQ2hhbmdlOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIC8vIGVuYWJsZSB0aGUgc3VibWl0IGJ1dHRvbiBpZmYgdGhlIHBhc3N3b3JkIGlzIG5vbi1lbXB0eVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBwYXNzd29yZDogZXYudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHBhc3N3b3JkQm94Q2xhc3MgPSBjbGFzc25hbWVzKHtcclxuICAgICAgICAgICAgXCJlcnJvclwiOiB0aGlzLnByb3BzLmVycm9yVGV4dCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHN1Ym1pdEJ1dHRvbk9yU3Bpbm5lcjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5idXN5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICBzdWJtaXRCdXR0b25PclNwaW5uZXIgPSA8TG9hZGVyIC8+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbk9yU3Bpbm5lciA9IChcclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwic3VibWl0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9EaWFsb2dfcHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyF0aGlzLnN0YXRlLnBhc3N3b3JkfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtfdChcIkNvbnRpbnVlXCIpfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBlcnJvclNlY3Rpb247XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZXJyb3JUZXh0KSB7XHJcbiAgICAgICAgICAgIGVycm9yU2VjdGlvbiA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZXJyb3JcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMucHJvcHMuZXJyb3JUZXh0IH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgRmllbGQgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5GaWVsZCcpO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+eyBfdChcIkNvbmZpcm0geW91ciBpZGVudGl0eSBieSBlbnRlcmluZyB5b3VyIGFjY291bnQgcGFzc3dvcmQgYmVsb3cuXCIpIH08L3A+XHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25TdWJtaXR9IGNsYXNzTmFtZT1cIm14X0ludGVyYWN0aXZlQXV0aEVudHJ5Q29tcG9uZW50c19wYXNzd29yZFNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtwYXNzd29yZEJveENsYXNzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwicGFzc3dvcmRGaWVsZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnUGFzc3dvcmQnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5wYXNzd29yZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uUGFzc3dvcmRGaWVsZENoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfYnV0dG9uX3Jvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN1Ym1pdEJ1dHRvbk9yU3Bpbm5lciB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIHsgZXJyb3JTZWN0aW9uIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG5cclxuZXhwb3J0IGNvbnN0IFJlY2FwdGNoYUF1dGhFbnRyeSA9IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSZWNhcHRjaGFBdXRoRW50cnknLFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBMT0dJTl9UWVBFOiBcIm0ubG9naW4ucmVjYXB0Y2hhXCIsXHJcbiAgICB9LFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHN1Ym1pdEF1dGhEaWN0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHN0YWdlUGFyYW1zOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZXJyb3JUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGJ1c3k6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIG9uUGhhc2VDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uUGhhc2VDaGFuZ2UoREVGQVVMVF9QSEFTRSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNhcHRjaGFSZXNwb25zZTogZnVuY3Rpb24ocmVzcG9uc2UpIHtcclxuICAgICAgICB0aGlzLnByb3BzLnN1Ym1pdEF1dGhEaWN0KHtcclxuICAgICAgICAgICAgdHlwZTogUmVjYXB0Y2hhQXV0aEVudHJ5LkxPR0lOX1RZUEUsXHJcbiAgICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5idXN5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gPExvYWRlciAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBlcnJvclRleHQgPSB0aGlzLnByb3BzLmVycm9yVGV4dDtcclxuXHJcbiAgICAgICAgY29uc3QgQ2FwdGNoYUZvcm0gPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuYXV0aC5DYXB0Y2hhRm9ybVwiKTtcclxuICAgICAgICBsZXQgc2l0ZVB1YmxpY0tleTtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuc3RhZ2VQYXJhbXMgfHwgIXRoaXMucHJvcHMuc3RhZ2VQYXJhbXMucHVibGljX2tleSkge1xyXG4gICAgICAgICAgICBlcnJvclRleHQgPSBfdChcclxuICAgICAgICAgICAgICAgIFwiTWlzc2luZyBjYXB0Y2hhIHB1YmxpYyBrZXkgaW4gaG9tZXNlcnZlciBjb25maWd1cmF0aW9uLiBQbGVhc2UgcmVwb3J0IFwiICtcclxuICAgICAgICAgICAgICAgIFwidGhpcyB0byB5b3VyIGhvbWVzZXJ2ZXIgYWRtaW5pc3RyYXRvci5cIixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzaXRlUHVibGljS2V5ID0gdGhpcy5wcm9wcy5zdGFnZVBhcmFtcy5wdWJsaWNfa2V5O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGVycm9yU2VjdGlvbjtcclxuICAgICAgICBpZiAoZXJyb3JUZXh0KSB7XHJcbiAgICAgICAgICAgIGVycm9yU2VjdGlvbiA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZXJyb3JcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGVycm9yVGV4dCB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8Q2FwdGNoYUZvcm0gc2l0ZVB1YmxpY0tleT17c2l0ZVB1YmxpY0tleX1cclxuICAgICAgICAgICAgICAgICAgICBvbkNhcHRjaGFSZXNwb25zZT17dGhpcy5fb25DYXB0Y2hhUmVzcG9uc2V9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgeyBlcnJvclNlY3Rpb24gfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcblxyXG5leHBvcnQgY29uc3QgVGVybXNBdXRoRW50cnkgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnVGVybXNBdXRoRW50cnknLFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBMT0dJTl9UWVBFOiBcIm0ubG9naW4udGVybXNcIixcclxuICAgIH0sXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgc3VibWl0QXV0aERpY3Q6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc3RhZ2VQYXJhbXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICBlcnJvclRleHQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgYnVzeTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgc2hvd0NvbnRpbnVlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBvblBoYXNlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vblBoYXNlQ2hhbmdlKERFRkFVTFRfUEhBU0UpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gTW92ZSB0aGlzIHRvIGNvbnN0cnVjdG9yXHJcbiAgICBjb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIGV4YW1wbGUgc3RhZ2VQYXJhbXM6XHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyB7XHJcbiAgICAgICAgLy8gICAgIFwicG9saWNpZXNcIjoge1xyXG4gICAgICAgIC8vICAgICAgICAgXCJwcml2YWN5X3BvbGljeVwiOiB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgXCJ2ZXJzaW9uXCI6IFwiMS4wXCIsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgXCJlblwiOiB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIlByaXZhY3kgUG9saWN5XCIsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIFwidXJsXCI6IFwiaHR0cHM6Ly9leGFtcGxlLm9yZy9wcml2YWN5LTEuMC1lbi5odG1sXCIsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgfSxcclxuICAgICAgICAvLyAgICAgICAgICAgICBcImZyXCI6IHtcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiUG9saXRpcXVlIGRlIGNvbmZpZGVudGlhbGl0w6lcIixcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgXCJ1cmxcIjogXCJodHRwczovL2V4YW1wbGUub3JnL3ByaXZhY3ktMS4wLWZyLmh0bWxcIixcclxuICAgICAgICAvLyAgICAgICAgICAgICB9LFxyXG4gICAgICAgIC8vICAgICAgICAgfSxcclxuICAgICAgICAvLyAgICAgICAgIFwib3RoZXJfcG9saWN5XCI6IHsgLi4uIH0sXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIGNvbnN0IGFsbFBvbGljaWVzID0gdGhpcy5wcm9wcy5zdGFnZVBhcmFtcy5wb2xpY2llcyB8fCB7fTtcclxuICAgICAgICBjb25zdCBwcmVmTGFuZyA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJsYW5ndWFnZVwiKTtcclxuICAgICAgICBjb25zdCBpbml0VG9nZ2xlcyA9IHt9O1xyXG4gICAgICAgIGNvbnN0IHBpY2tlZFBvbGljaWVzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCBwb2xpY3lJZCBvZiBPYmplY3Qua2V5cyhhbGxQb2xpY2llcykpIHtcclxuICAgICAgICAgICAgY29uc3QgcG9saWN5ID0gYWxsUG9saWNpZXNbcG9saWN5SWRdO1xyXG5cclxuICAgICAgICAgICAgLy8gUGljayBhIGxhbmd1YWdlIGJhc2VkIG9uIHRoZSB1c2VyJ3MgbGFuZ3VhZ2UsIGZhbGxpbmcgYmFjayB0byBlbmdsaXNoLFxyXG4gICAgICAgICAgICAvLyBhbmQgZmluYWxseSB0byB0aGUgZmlyc3QgbGFuZ3VhZ2UgYXZhaWxhYmxlLiBJZiB0aGVyZSdzIHN0aWxsIG5vIHBvbGljeVxyXG4gICAgICAgICAgICAvLyBhdmFpbGFibGUgdGhlbiB0aGUgaG9tZXNlcnZlciBpc24ndCByZXNwZWN0aW5nIHRoZSBzcGVjLlxyXG4gICAgICAgICAgICBsZXQgbGFuZ1BvbGljeSA9IHBvbGljeVtwcmVmTGFuZ107XHJcbiAgICAgICAgICAgIGlmICghbGFuZ1BvbGljeSkgbGFuZ1BvbGljeSA9IHBvbGljeVtcImVuXCJdO1xyXG4gICAgICAgICAgICBpZiAoIWxhbmdQb2xpY3kpIHtcclxuICAgICAgICAgICAgICAgIC8vIGxhc3QgcmVzb3J0XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaXJzdExhbmcgPSBPYmplY3Qua2V5cyhwb2xpY3kpLmZpbmQoZSA9PiBlICE9PSBcInZlcnNpb25cIik7XHJcbiAgICAgICAgICAgICAgICBsYW5nUG9saWN5ID0gcG9saWN5W2ZpcnN0TGFuZ107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCFsYW5nUG9saWN5KSB0aHJvdyBuZXcgRXJyb3IoXCJGYWlsZWQgdG8gZmluZCBhIHBvbGljeSB0byBzaG93IHRoZSB1c2VyXCIpO1xyXG5cclxuICAgICAgICAgICAgaW5pdFRvZ2dsZXNbcG9saWN5SWRdID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICBsYW5nUG9saWN5LmlkID0gcG9saWN5SWQ7XHJcbiAgICAgICAgICAgIHBpY2tlZFBvbGljaWVzLnB1c2gobGFuZ1BvbGljeSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgXCJ0b2dnbGVkUG9saWNpZXNcIjogaW5pdFRvZ2dsZXMsXHJcbiAgICAgICAgICAgIFwicG9saWNpZXNcIjogcGlja2VkUG9saWNpZXMsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHRyeUNvbnRpbnVlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl90cnlTdWJtaXQoKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3RvZ2dsZVBvbGljeTogZnVuY3Rpb24ocG9saWN5SWQpIHtcclxuICAgICAgICBjb25zdCBuZXdUb2dnbGVzID0ge307XHJcbiAgICAgICAgZm9yIChjb25zdCBwb2xpY3kgb2YgdGhpcy5zdGF0ZS5wb2xpY2llcykge1xyXG4gICAgICAgICAgICBsZXQgY2hlY2tlZCA9IHRoaXMuc3RhdGUudG9nZ2xlZFBvbGljaWVzW3BvbGljeS5pZF07XHJcbiAgICAgICAgICAgIGlmIChwb2xpY3kuaWQgPT09IHBvbGljeUlkKSBjaGVja2VkID0gIWNoZWNrZWQ7XHJcblxyXG4gICAgICAgICAgICBuZXdUb2dnbGVzW3BvbGljeS5pZF0gPSBjaGVja2VkO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcInRvZ2dsZWRQb2xpY2llc1wiOiBuZXdUb2dnbGVzfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF90cnlTdWJtaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCBhbGxDaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICBmb3IgKGNvbnN0IHBvbGljeSBvZiB0aGlzLnN0YXRlLnBvbGljaWVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNoZWNrZWQgPSB0aGlzLnN0YXRlLnRvZ2dsZWRQb2xpY2llc1twb2xpY3kuaWRdO1xyXG4gICAgICAgICAgICBhbGxDaGVja2VkID0gYWxsQ2hlY2tlZCAmJiBjaGVja2VkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFsbENoZWNrZWQpIHRoaXMucHJvcHMuc3VibWl0QXV0aERpY3Qoe3R5cGU6IFRlcm1zQXV0aEVudHJ5LkxPR0lOX1RZUEV9KTtcclxuICAgICAgICBlbHNlIHRoaXMuc2V0U3RhdGUoe2Vycm9yVGV4dDogX3QoXCJQbGVhc2UgcmV2aWV3IGFuZCBhY2NlcHQgYWxsIG9mIHRoZSBob21lc2VydmVyJ3MgcG9saWNpZXNcIil9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5idXN5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IExvYWRlciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gPExvYWRlciAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGNoZWNrYm94ZXMgPSBbXTtcclxuICAgICAgICBsZXQgYWxsQ2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgZm9yIChjb25zdCBwb2xpY3kgb2YgdGhpcy5zdGF0ZS5wb2xpY2llcykge1xyXG4gICAgICAgICAgICBjb25zdCBjaGVja2VkID0gdGhpcy5zdGF0ZS50b2dnbGVkUG9saWNpZXNbcG9saWN5LmlkXTtcclxuICAgICAgICAgICAgYWxsQ2hlY2tlZCA9IGFsbENoZWNrZWQgJiYgY2hlY2tlZDtcclxuXHJcbiAgICAgICAgICAgIGNoZWNrYm94ZXMucHVzaChcclxuICAgICAgICAgICAgICAgIDxsYWJlbCBrZXk9e1wicG9saWN5X2NoZWNrYm94X1wiICsgcG9saWN5LmlkfSBjbGFzc05hbWU9XCJteF9JbnRlcmFjdGl2ZUF1dGhFbnRyeUNvbXBvbmVudHNfdGVybXNQb2xpY3lcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9eygpID0+IHRoaXMuX3RvZ2dsZVBvbGljeShwb2xpY3kuaWQpfSBjaGVja2VkPXtjaGVja2VkfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9e3BvbGljeS51cmx9IHRhcmdldD1cIl9ibGFua1wiIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIj57IHBvbGljeS5uYW1lIH08L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xhYmVsPixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBlcnJvclNlY3Rpb247XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZXJyb3JUZXh0IHx8IHRoaXMuc3RhdGUuZXJyb3JUZXh0KSB7XHJcbiAgICAgICAgICAgIGVycm9yU2VjdGlvbiA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZXJyb3JcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMucHJvcHMuZXJyb3JUZXh0IHx8IHRoaXMuc3RhdGUuZXJyb3JUZXh0IH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHN1Ym1pdEJ1dHRvbjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93Q29udGludWUgIT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIC8vIFhYWDogYnV0dG9uIGNsYXNzZXNcclxuICAgICAgICAgICAgc3VibWl0QnV0dG9uID0gPGJ1dHRvbiBjbGFzc05hbWU9XCJteF9JbnRlcmFjdGl2ZUF1dGhFbnRyeUNvbXBvbmVudHNfdGVybXNTdWJtaXQgbXhfR2VuZXJhbEJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fdHJ5U3VibWl0fSBkaXNhYmxlZD17IWFsbENoZWNrZWR9PntfdChcIkFjY2VwdFwiKX08L2J1dHRvbj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHA+e190KFwiUGxlYXNlIHJldmlldyBhbmQgYWNjZXB0IHRoZSBwb2xpY2llcyBvZiB0aGlzIGhvbWVzZXJ2ZXI6XCIpfTwvcD5cclxuICAgICAgICAgICAgICAgIHsgY2hlY2tib3hlcyB9XHJcbiAgICAgICAgICAgICAgICB7IGVycm9yU2VjdGlvbiB9XHJcbiAgICAgICAgICAgICAgICB7IHN1Ym1pdEJ1dHRvbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbmV4cG9ydCBjb25zdCBFbWFpbElkZW50aXR5QXV0aEVudHJ5ID0gY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ0VtYWlsSWRlbnRpdHlBdXRoRW50cnknLFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBMT0dJTl9UWVBFOiBcIm0ubG9naW4uZW1haWwuaWRlbnRpdHlcIixcclxuICAgIH0sXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgbWF0cml4Q2xpZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc3VibWl0QXV0aERpY3Q6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgYXV0aFNlc3Npb25JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGNsaWVudFNlY3JldDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGlucHV0czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHN0YWdlU3RhdGU6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICBmYWlsOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHNldEVtYWlsU2lkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uUGhhc2VDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uUGhhc2VDaGFuZ2UoREVGQVVMVF9QSEFTRSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVxdWVzdGluZ1Rva2VuOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlcXVlc3RpbmdUb2tlbikge1xyXG4gICAgICAgICAgICBjb25zdCBMb2FkZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIDxMb2FkZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+eyBfdChcIkFuIGVtYWlsIGhhcyBiZWVuIHNlbnQgdG8gJShlbWFpbEFkZHJlc3Mpc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGVtYWlsQWRkcmVzczogKHN1YikgPT4gPGk+eyB0aGlzLnByb3BzLmlucHV0cy5lbWFpbEFkZHJlc3MgfTwvaT4gfSxcclxuICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+eyBfdChcIlBsZWFzZSBjaGVjayB5b3VyIGVtYWlsIHRvIGNvbnRpbnVlIHJlZ2lzdHJhdGlvbi5cIikgfTwvcD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbn0pO1xyXG5cclxuZXhwb3J0IGNvbnN0IE1zaXNkbkF1dGhFbnRyeSA9IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdNc2lzZG5BdXRoRW50cnknLFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBMT0dJTl9UWVBFOiBcIm0ubG9naW4ubXNpc2RuXCIsXHJcbiAgICB9LFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIGlucHV0czogUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgICAgICAgICAgcGhvbmVDb3VudHJ5OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgICAgICBwaG9uZU51bWJlcjogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICB9KSxcclxuICAgICAgICBmYWlsOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBjbGllbnRTZWNyZXQ6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIHN1Ym1pdEF1dGhEaWN0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG1hdHJpeENsaWVudDogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgICAgICBvblBoYXNlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRva2VuOiAnJyxcclxuICAgICAgICAgICAgcmVxdWVzdGluZ1Rva2VuOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vblBoYXNlQ2hhbmdlKERFRkFVTFRfUEhBU0UpO1xyXG5cclxuICAgICAgICB0aGlzLl9zdWJtaXRVcmwgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NpZCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fbXNpc2RuID0gbnVsbDtcclxuICAgICAgICB0aGlzLl90b2tlbkJveCA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3JlcXVlc3RpbmdUb2tlbjogdHJ1ZX0pO1xyXG4gICAgICAgIHRoaXMuX3JlcXVlc3RNc2lzZG5Ub2tlbigpLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuZmFpbChlKTtcclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cmVxdWVzdGluZ1Rva2VuOiBmYWxzZX0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKlxyXG4gICAgICogUmVxdWVzdHMgYSB2ZXJpZmljYXRpb24gdG9rZW4gYnkgU01TLlxyXG4gICAgICovXHJcbiAgICBfcmVxdWVzdE1zaXNkblRva2VuOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5tYXRyaXhDbGllbnQucmVxdWVzdFJlZ2lzdGVyTXNpc2RuVG9rZW4oXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuaW5wdXRzLnBob25lQ291bnRyeSxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5pbnB1dHMucGhvbmVOdW1iZXIsXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuY2xpZW50U2VjcmV0LFxyXG4gICAgICAgICAgICAxLCAvLyBUT0RPOiBNdWx0aXBsZSBzZW5kIGF0dGVtcHRzP1xyXG4gICAgICAgICkudGhlbigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1Ym1pdFVybCA9IHJlc3VsdC5zdWJtaXRfdXJsO1xyXG4gICAgICAgICAgICB0aGlzLl9zaWQgPSByZXN1bHQuc2lkO1xyXG4gICAgICAgICAgICB0aGlzLl9tc2lzZG4gPSByZXN1bHQubXNpc2RuO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25Ub2tlbkNoYW5nZTogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB0b2tlbjogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkZvcm1TdWJtaXQ6IGFzeW5jIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudG9rZW4gPT0gJycpIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGVycm9yVGV4dDogbnVsbCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgcmVxdWlyZXNJZFNlcnZlclBhcmFtID1cclxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucHJvcHMubWF0cml4Q2xpZW50LmRvZXNTZXJ2ZXJSZXF1aXJlSWRTZXJ2ZXJQYXJhbSgpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0O1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fc3VibWl0VXJsKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnByb3BzLm1hdHJpeENsaWVudC5zdWJtaXRNc2lzZG5Ub2tlbk90aGVyVXJsKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3N1Ym1pdFVybCwgdGhpcy5fc2lkLCB0aGlzLnByb3BzLmNsaWVudFNlY3JldCwgdGhpcy5zdGF0ZS50b2tlbixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocmVxdWlyZXNJZFNlcnZlclBhcmFtKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnByb3BzLm1hdHJpeENsaWVudC5zdWJtaXRNc2lzZG5Ub2tlbihcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zaWQsIHRoaXMucHJvcHMuY2xpZW50U2VjcmV0LCB0aGlzLnN0YXRlLnRva2VuLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlRoZSByZWdpc3RyYXRpb24gd2l0aCBNU0lTRE4gZmxvdyBpcyBtaXNjb25maWd1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3JlZHMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2lkOiB0aGlzLl9zaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xpZW50X3NlY3JldDogdGhpcy5wcm9wcy5jbGllbnRTZWNyZXQsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlcXVpcmVzSWRTZXJ2ZXJQYXJhbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGlkU2VydmVyUGFyc2VkVXJsID0gdXJsLnBhcnNlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm1hdHJpeENsaWVudC5nZXRJZGVudGl0eVNlcnZlclVybCgpLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3JlZHMuaWRfc2VydmVyID0gaWRTZXJ2ZXJQYXJzZWRVcmwuaG9zdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuc3VibWl0QXV0aERpY3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IE1zaXNkbkF1dGhFbnRyeS5MT0dJTl9UWVBFLFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IFJlbW92ZSBgdGhyZWVwaWRfY3JlZHNgIG9uY2Ugc2VydmVycyBzdXBwb3J0IHByb3BlciBVSUFcclxuICAgICAgICAgICAgICAgICAgICAvLyBTZWUgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvMTAzMTJcclxuICAgICAgICAgICAgICAgICAgICB0aHJlZXBpZF9jcmVkczogY3JlZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhyZWVwaWRDcmVkczogY3JlZHMsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yVGV4dDogX3QoXCJUb2tlbiBpbmNvcnJlY3RcIiksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5mYWlsKGUpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkZhaWxlZCB0byBzdWJtaXQgbXNpc2RuIHRva2VuXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXF1ZXN0aW5nVG9rZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgICAgIHJldHVybiA8TG9hZGVyIC8+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGVuYWJsZVN1Ym1pdCA9IEJvb2xlYW4odGhpcy5zdGF0ZS50b2tlbik7XHJcbiAgICAgICAgICAgIGNvbnN0IHN1Ym1pdENsYXNzZXMgPSBjbGFzc25hbWVzKHtcclxuICAgICAgICAgICAgICAgIG14X0ludGVyYWN0aXZlQXV0aEVudHJ5Q29tcG9uZW50c19tc2lzZG5TdWJtaXQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBteF9HZW5lcmFsQnV0dG9uOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgbGV0IGVycm9yU2VjdGlvbjtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3JUZXh0KSB7XHJcbiAgICAgICAgICAgICAgICBlcnJvclNlY3Rpb24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJlcnJvclwiIHJvbGU9XCJhbGVydFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuc3RhdGUuZXJyb3JUZXh0IH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+eyBfdChcIkEgdGV4dCBtZXNzYWdlIGhhcyBiZWVuIHNlbnQgdG8gJShtc2lzZG4pc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IG1zaXNkbjogPGk+eyB0aGlzLl9tc2lzZG4gfTwvaT4gfSxcclxuICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+eyBfdChcIlBsZWFzZSBlbnRlciB0aGUgY29kZSBpdCBjb250YWluczpcIikgfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0ludGVyYWN0aXZlQXV0aEVudHJ5Q29tcG9uZW50c19tc2lzZG5XcmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLl9vbkZvcm1TdWJtaXR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9JbnRlcmFjdGl2ZUF1dGhFbnRyeUNvbXBvbmVudHNfbXNpc2RuRW50cnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnRva2VufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblRva2VuQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9eyBfdChcIkNvZGVcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInN1Ym1pdFwiIHZhbHVlPXtfdChcIlN1Ym1pdFwiKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3N1Ym1pdENsYXNzZXN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFlbmFibGVTdWJtaXR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtlcnJvclNlY3Rpb259XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbmV4cG9ydCBjbGFzcyBTU09BdXRoRW50cnkgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBtYXRyaXhDbGllbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICBhdXRoU2Vzc2lvbklkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgbG9naW5UeXBlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc3VibWl0QXV0aERpY3Q6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgZXJyb3JUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIG9uUGhhc2VDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgY29udGludWVUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIGNvbnRpbnVlS2luZDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBvbkNhbmNlbDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBMT0dJTl9UWVBFID0gXCJtLmxvZ2luLnNzb1wiO1xyXG4gICAgc3RhdGljIFVOU1RBQkxFX0xPR0lOX1RZUEUgPSBcIm9yZy5tYXRyaXgubG9naW4uc3NvXCI7XHJcblxyXG4gICAgc3RhdGljIFBIQVNFX1BSRUFVVEggPSAxOyAvLyBidXR0b24gdG8gc3RhcnQgU1NPXHJcbiAgICBzdGF0aWMgUEhBU0VfUE9TVEFVVEggPSAyOyAvLyBidXR0b24gdG8gY29uZmlybSBTU08gY29tcGxldGVkXHJcblxyXG4gICAgX3Nzb1VybDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICAvLyBXZSBhY3R1YWxseSBzZW5kIHRoZSB1c2VyIHRocm91Z2ggZmFsbGJhY2sgYXV0aCBzbyB3ZSBkb24ndCBoYXZlIHRvXHJcbiAgICAgICAgLy8gZGVhbCB3aXRoIGEgcmVkaXJlY3QgYmFjayB0byB1cywgbG9zaW5nIGFwcGxpY2F0aW9uIGNvbnRleHQuXHJcbiAgICAgICAgdGhpcy5fc3NvVXJsID0gcHJvcHMubWF0cml4Q2xpZW50LmdldEZhbGxiYWNrQXV0aFVybChcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5sb2dpblR5cGUsXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuYXV0aFNlc3Npb25JZCxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBwaGFzZTogU1NPQXV0aEVudHJ5LlBIQVNFX1BSRUFVVEgsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uUGhhc2VDaGFuZ2UoU1NPQXV0aEVudHJ5LlBIQVNFX1BSRUFVVEgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3RhcnRBdXRoQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgLy8gTm90ZTogV2UgZG9uJ3QgdXNlIFBsYXRmb3JtUGVnJ3Mgc3RhcnRTc29BdXRoIGZ1bmN0aW9ucyBiZWNhdXNlIHdlIGFsbW9zdFxyXG4gICAgICAgIC8vIGNlcnRhaW5seSB3aWxsIG5lZWQgdG8gb3BlbiB0aGUgdGhpbmcgaW4gYSBuZXcgdGFiIHRvIGF2b2lkIGxvc2luZyBhcHBsaWNhdGlvblxyXG4gICAgICAgIC8vIGNvbnRleHQuXHJcblxyXG4gICAgICAgIHdpbmRvdy5vcGVuKHRoaXMuX3Nzb1VybCwgJ19ibGFuaycpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3BoYXNlOiBTU09BdXRoRW50cnkuUEhBU0VfUE9TVEFVVEh9KTtcclxuICAgICAgICB0aGlzLnByb3BzLm9uUGhhc2VDaGFuZ2UoU1NPQXV0aEVudHJ5LlBIQVNFX1BPU1RBVVRIKTtcclxuICAgIH07XHJcblxyXG4gICAgb25Db25maXJtQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5zdWJtaXRBdXRoRGljdCh7fSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgY29udGludWVCdXR0b24gPSBudWxsO1xyXG4gICAgICAgIGNvbnN0IGNhbmNlbEJ1dHRvbiA9IChcclxuICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMucHJvcHMub25DYW5jZWx9XHJcbiAgICAgICAgICAgICAgICBraW5kPXt0aGlzLnByb3BzLmNvbnRpbnVlS2luZCA/ICh0aGlzLnByb3BzLmNvbnRpbnVlS2luZCArICdfb3V0bGluZScpIDogJ3ByaW1hcnlfb3V0bGluZSd9XHJcbiAgICAgICAgICAgID57X3QoXCJDYW5jZWxcIil9PC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucGhhc2UgPT09IFNTT0F1dGhFbnRyeS5QSEFTRV9QUkVBVVRIKSB7XHJcbiAgICAgICAgICAgIGNvbnRpbnVlQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uU3RhcnRBdXRoQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAga2luZD17dGhpcy5wcm9wcy5jb250aW51ZUtpbmQgfHwgJ3ByaW1hcnknfVxyXG4gICAgICAgICAgICAgICAgPnt0aGlzLnByb3BzLmNvbnRpbnVlVGV4dCB8fCBfdChcIlNpbmdsZSBTaWduIE9uXCIpfTwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb250aW51ZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkNvbmZpcm1DbGlja31cclxuICAgICAgICAgICAgICAgICAgICBraW5kPXt0aGlzLnByb3BzLmNvbnRpbnVlS2luZCB8fCAncHJpbWFyeSd9XHJcbiAgICAgICAgICAgICAgICA+e3RoaXMucHJvcHMuY29udGludWVUZXh0IHx8IF90KFwiQ29uZmlybVwiKX08L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9J214X0ludGVyYWN0aXZlQXV0aEVudHJ5Q29tcG9uZW50c19zc29fYnV0dG9ucyc+XHJcbiAgICAgICAgICAgIHtjYW5jZWxCdXR0b259XHJcbiAgICAgICAgICAgIHtjb250aW51ZUJ1dHRvbn1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBGYWxsYmFja0F1dGhFbnRyeSA9IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdGYWxsYmFja0F1dGhFbnRyeScsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgbWF0cml4Q2xpZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgYXV0aFNlc3Npb25JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGxvZ2luVHlwZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHN1Ym1pdEF1dGhEaWN0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIGVycm9yVGV4dDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBvblBoYXNlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vblBoYXNlQ2hhbmdlKERFRkFVTFRfUEhBU0UpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHdlIGhhdmUgdG8gbWFrZSB0aGUgdXNlciBjbGljayBhIGJ1dHRvbiwgYXMgYnJvd3NlcnMgd2lsbCBibG9ja1xyXG4gICAgICAgIC8vIHRoZSBwb3B1cCBpZiB3ZSBvcGVuIGl0IGltbWVkaWF0ZWx5LlxyXG4gICAgICAgIHRoaXMuX3BvcHVwV2luZG93ID0gbnVsbDtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1lc3NhZ2VcIiwgdGhpcy5fb25SZWNlaXZlTWVzc2FnZSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2ZhbGxiYWNrQnV0dG9uID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm1lc3NhZ2VcIiwgdGhpcy5fb25SZWNlaXZlTWVzc2FnZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuX3BvcHVwV2luZG93KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BvcHVwV2luZG93LmNsb3NlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBmb2N1czogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2ZhbGxiYWNrQnV0dG9uLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5fZmFsbGJhY2tCdXR0b24uY3VycmVudC5mb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX29uU2hvd0ZhbGxiYWNrQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5wcm9wcy5tYXRyaXhDbGllbnQuZ2V0RmFsbGJhY2tBdXRoVXJsKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLmxvZ2luVHlwZSxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5hdXRoU2Vzc2lvbklkLFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy5fcG9wdXBXaW5kb3cgPSB3aW5kb3cub3Blbih1cmwpO1xyXG4gICAgICAgIHRoaXMuX3BvcHVwV2luZG93Lm9wZW5lciA9IG51bGw7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJlY2VpdmVNZXNzYWdlOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgZXZlbnQuZGF0YSA9PT0gXCJhdXRoRG9uZVwiICYmXHJcbiAgICAgICAgICAgIGV2ZW50Lm9yaWdpbiA9PT0gdGhpcy5wcm9wcy5tYXRyaXhDbGllbnQuZ2V0SG9tZXNlcnZlclVybCgpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3VibWl0QXV0aERpY3Qoe30pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgZXJyb3JTZWN0aW9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmVycm9yVGV4dCkge1xyXG4gICAgICAgICAgICBlcnJvclNlY3Rpb24gPSAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImVycm9yXCIgcm9sZT1cImFsZXJ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLmVycm9yVGV4dCB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJcIiByZWY9e3RoaXMuX2ZhbGxiYWNrQnV0dG9ufSBvbkNsaWNrPXt0aGlzLl9vblNob3dGYWxsYmFja0NsaWNrfT57IF90KFwiU3RhcnQgYXV0aGVudGljYXRpb25cIikgfTwvYT5cclxuICAgICAgICAgICAgICAgIHtlcnJvclNlY3Rpb259XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbmNvbnN0IEF1dGhFbnRyeUNvbXBvbmVudHMgPSBbXHJcbiAgICBQYXNzd29yZEF1dGhFbnRyeSxcclxuICAgIFJlY2FwdGNoYUF1dGhFbnRyeSxcclxuICAgIEVtYWlsSWRlbnRpdHlBdXRoRW50cnksXHJcbiAgICBNc2lzZG5BdXRoRW50cnksXHJcbiAgICBUZXJtc0F1dGhFbnRyeSxcclxuICAgIFNTT0F1dGhFbnRyeSxcclxuXTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGdldEVudHJ5Q29tcG9uZW50Rm9yTG9naW5UeXBlKGxvZ2luVHlwZSkge1xyXG4gICAgZm9yIChjb25zdCBjIG9mIEF1dGhFbnRyeUNvbXBvbmVudHMpIHtcclxuICAgICAgICBpZiAoYy5MT0dJTl9UWVBFID09PSBsb2dpblR5cGUgfHwgYy5VTlNUQUJMRV9MT0dJTl9UWVBFID09PSBsb2dpblR5cGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIEZhbGxiYWNrQXV0aEVudHJ5O1xyXG59XHJcbiJdfQ==