"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var AvatarLogic = _interopRequireWildcard(require("../../../Avatar"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'BaseAvatar',
  propTypes: {
    name: _propTypes.default.string.isRequired,
    // The name (first initial used as default)
    idName: _propTypes.default.string,
    // ID for generating hash colours
    title: _propTypes.default.string,
    // onHover title text
    url: _propTypes.default.string,
    // highest priority of them all, shortcut to set in urls[0]
    urls: _propTypes.default.array,
    // [highest_priority, ... , lowest_priority]
    width: _propTypes.default.number,
    height: _propTypes.default.number,
    // XXX resizeMethod not actually used.
    resizeMethod: _propTypes.default.string,
    defaultToInitialLetter: _propTypes.default.bool,
    // true to add default url
    inputRef: _propTypes.default.oneOfType([// Either a function
    _propTypes.default.func, // Or the instance of a DOM native element
    _propTypes.default.shape({
      current: _propTypes.default.instanceOf(Element)
    })])
  },
  statics: {
    contextType: _MatrixClientContext.default
  },
  getDefaultProps: function () {
    return {
      width: 40,
      height: 40,
      resizeMethod: 'crop',
      defaultToInitialLetter: true
    };
  },
  getInitialState: function () {
    return this._getState(this.props);
  },

  componentDidMount() {
    this.unmounted = false;
    this.context.on('sync', this.onClientSync);
  },

  componentWillUnmount() {
    this.unmounted = true;
    this.context.removeListener('sync', this.onClientSync);
  },

  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (nextProps) {
    // work out if we need to call setState (if the image URLs array has changed)
    const newState = this._getState(nextProps);

    const newImageUrls = newState.imageUrls;
    const oldImageUrls = this.state.imageUrls;

    if (newImageUrls.length !== oldImageUrls.length) {
      this.setState(newState); // detected a new entry
    } else {
      // check each one to see if they are the same
      for (let i = 0; i < newImageUrls.length; i++) {
        if (oldImageUrls[i] !== newImageUrls[i]) {
          this.setState(newState); // detected a diff

          break;
        }
      }
    }
  },
  onClientSync: function (syncState, prevState) {
    if (this.unmounted) return; // Consider the client reconnected if there is no error with syncing.
    // This means the state could be RECONNECTING, SYNCING, PREPARED or CATCHUP.

    const reconnected = syncState !== "ERROR" && prevState !== syncState;

    if (reconnected && // Did we fall back?
    this.state.urlsIndex > 0) {
      // Start from the highest priority URL again
      this.setState({
        urlsIndex: 0
      });
    }
  },
  _getState: function (props) {
    // work out the full set of urls to try to load. This is formed like so:
    // imageUrls: [ props.url, props.urls, default image ]
    let urls = [];

    if (!_SettingsStore.default.getValue("lowBandwidth")) {
      urls = props.urls || [];

      if (props.url) {
        urls.unshift(props.url); // put in urls[0]
      }
    }

    let defaultImageUrl = null;

    if (props.defaultToInitialLetter) {
      defaultImageUrl = AvatarLogic.defaultAvatarUrlForString(props.idName || props.name);
      urls.push(defaultImageUrl); // lowest priority
    } // deduplicate URLs


    urls = Array.from(new Set(urls));
    return {
      imageUrls: urls,
      defaultImageUrl: defaultImageUrl,
      urlsIndex: 0
    };
  },
  onError: function (ev) {
    const nextIndex = this.state.urlsIndex + 1;

    if (nextIndex < this.state.imageUrls.length) {
      // try the next one
      this.setState({
        urlsIndex: nextIndex
      });
    }
  },
  render: function () {
    const imageUrl = this.state.imageUrls[this.state.urlsIndex];
    const _this$props = this.props,
          {
      name,
      idName,
      title,
      url,
      urls,
      width,
      height,
      resizeMethod,
      defaultToInitialLetter,
      onClick,
      inputRef
    } = _this$props,
          otherProps = (0, _objectWithoutProperties2.default)(_this$props, ["name", "idName", "title", "url", "urls", "width", "height", "resizeMethod", "defaultToInitialLetter", "onClick", "inputRef"]);

    if (imageUrl === this.state.defaultImageUrl) {
      const initialLetter = AvatarLogic.getInitialLetter(name);

      const textNode = _react.default.createElement("span", {
        className: "mx_BaseAvatar_initial",
        "aria-hidden": "true",
        style: {
          fontSize: width * 0.65 + "px",
          width: width + "px",
          lineHeight: height + "px"
        }
      }, initialLetter);

      const imgNode = _react.default.createElement("img", {
        className: "mx_BaseAvatar_image",
        src: imageUrl,
        alt: "",
        title: title,
        onError: this.onError,
        width: width,
        height: height,
        "aria-hidden": "true"
      });

      if (onClick != null) {
        return _react.default.createElement(_AccessibleButton.default, (0, _extends2.default)({
          element: "span",
          className: "mx_BaseAvatar",
          onClick: onClick,
          inputRef: inputRef
        }, otherProps), textNode, imgNode);
      } else {
        return _react.default.createElement("span", (0, _extends2.default)({
          className: "mx_BaseAvatar",
          ref: inputRef
        }, otherProps), textNode, imgNode);
      }
    }

    if (onClick != null) {
      return _react.default.createElement(_AccessibleButton.default, (0, _extends2.default)({
        className: "mx_BaseAvatar mx_BaseAvatar_image",
        element: "img",
        src: imageUrl,
        onClick: onClick,
        onError: this.onError,
        width: width,
        height: height,
        title: title,
        alt: "",
        inputRef: inputRef
      }, otherProps));
    } else {
      return _react.default.createElement("img", (0, _extends2.default)({
        className: "mx_BaseAvatar mx_BaseAvatar_image",
        src: imageUrl,
        onError: this.onError,
        width: width,
        height: height,
        title: title,
        alt: "",
        ref: inputRef
      }, otherProps));
    }
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2F2YXRhcnMvQmFzZUF2YXRhci5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsIm5hbWUiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiaWROYW1lIiwidGl0bGUiLCJ1cmwiLCJ1cmxzIiwiYXJyYXkiLCJ3aWR0aCIsIm51bWJlciIsImhlaWdodCIsInJlc2l6ZU1ldGhvZCIsImRlZmF1bHRUb0luaXRpYWxMZXR0ZXIiLCJib29sIiwiaW5wdXRSZWYiLCJvbmVPZlR5cGUiLCJmdW5jIiwic2hhcGUiLCJjdXJyZW50IiwiaW5zdGFuY2VPZiIsIkVsZW1lbnQiLCJzdGF0aWNzIiwiY29udGV4dFR5cGUiLCJNYXRyaXhDbGllbnRDb250ZXh0IiwiZ2V0RGVmYXVsdFByb3BzIiwiZ2V0SW5pdGlhbFN0YXRlIiwiX2dldFN0YXRlIiwicHJvcHMiLCJjb21wb25lbnREaWRNb3VudCIsInVubW91bnRlZCIsImNvbnRleHQiLCJvbiIsIm9uQ2xpZW50U3luYyIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyIsIm5leHRQcm9wcyIsIm5ld1N0YXRlIiwibmV3SW1hZ2VVcmxzIiwiaW1hZ2VVcmxzIiwib2xkSW1hZ2VVcmxzIiwic3RhdGUiLCJsZW5ndGgiLCJzZXRTdGF0ZSIsImkiLCJzeW5jU3RhdGUiLCJwcmV2U3RhdGUiLCJyZWNvbm5lY3RlZCIsInVybHNJbmRleCIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsInVuc2hpZnQiLCJkZWZhdWx0SW1hZ2VVcmwiLCJBdmF0YXJMb2dpYyIsImRlZmF1bHRBdmF0YXJVcmxGb3JTdHJpbmciLCJwdXNoIiwiQXJyYXkiLCJmcm9tIiwiU2V0Iiwib25FcnJvciIsImV2IiwibmV4dEluZGV4IiwicmVuZGVyIiwiaW1hZ2VVcmwiLCJvbkNsaWNrIiwib3RoZXJQcm9wcyIsImluaXRpYWxMZXR0ZXIiLCJnZXRJbml0aWFsTGV0dGVyIiwidGV4dE5vZGUiLCJmb250U2l6ZSIsImxpbmVIZWlnaHQiLCJpbWdOb2RlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUEyQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsWUFEZTtBQUc1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLElBQUksRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRGhCO0FBQzRCO0FBQ25DQyxJQUFBQSxNQUFNLEVBQUVILG1CQUFVQyxNQUZYO0FBRW1CO0FBQzFCRyxJQUFBQSxLQUFLLEVBQUVKLG1CQUFVQyxNQUhWO0FBR2tCO0FBQ3pCSSxJQUFBQSxHQUFHLEVBQUVMLG1CQUFVQyxNQUpSO0FBSWdCO0FBQ3ZCSyxJQUFBQSxJQUFJLEVBQUVOLG1CQUFVTyxLQUxUO0FBS2dCO0FBQ3ZCQyxJQUFBQSxLQUFLLEVBQUVSLG1CQUFVUyxNQU5WO0FBT1BDLElBQUFBLE1BQU0sRUFBRVYsbUJBQVVTLE1BUFg7QUFRUDtBQUNBRSxJQUFBQSxZQUFZLEVBQUVYLG1CQUFVQyxNQVRqQjtBQVVQVyxJQUFBQSxzQkFBc0IsRUFBRVosbUJBQVVhLElBVjNCO0FBVWlDO0FBQ3hDQyxJQUFBQSxRQUFRLEVBQUVkLG1CQUFVZSxTQUFWLENBQW9CLENBQzFCO0FBQ0FmLHVCQUFVZ0IsSUFGZ0IsRUFHMUI7QUFDQWhCLHVCQUFVaUIsS0FBVixDQUFnQjtBQUFFQyxNQUFBQSxPQUFPLEVBQUVsQixtQkFBVW1CLFVBQVYsQ0FBcUJDLE9BQXJCO0FBQVgsS0FBaEIsQ0FKMEIsQ0FBcEI7QUFYSCxHQUhpQjtBQXNCNUJDLEVBQUFBLE9BQU8sRUFBRTtBQUNMQyxJQUFBQSxXQUFXLEVBQUVDO0FBRFIsR0F0Qm1CO0FBMEI1QkMsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIaEIsTUFBQUEsS0FBSyxFQUFFLEVBREo7QUFFSEUsTUFBQUEsTUFBTSxFQUFFLEVBRkw7QUFHSEMsTUFBQUEsWUFBWSxFQUFFLE1BSFg7QUFJSEMsTUFBQUEsc0JBQXNCLEVBQUU7QUFKckIsS0FBUDtBQU1ILEdBakMyQjtBQW1DNUJhLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU8sS0FBS0MsU0FBTCxDQUFlLEtBQUtDLEtBQXBCLENBQVA7QUFDSCxHQXJDMkI7O0FBdUM1QkMsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsU0FBS0MsU0FBTCxHQUFpQixLQUFqQjtBQUNBLFNBQUtDLE9BQUwsQ0FBYUMsRUFBYixDQUFnQixNQUFoQixFQUF3QixLQUFLQyxZQUE3QjtBQUNILEdBMUMyQjs7QUE0QzVCQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixTQUFLSixTQUFMLEdBQWlCLElBQWpCO0FBQ0EsU0FBS0MsT0FBTCxDQUFhSSxjQUFiLENBQTRCLE1BQTVCLEVBQW9DLEtBQUtGLFlBQXpDO0FBQ0gsR0EvQzJCOztBQWlENUI7QUFDQUcsRUFBQUEsZ0NBQWdDLEVBQUUsVUFBU0MsU0FBVCxFQUFvQjtBQUNsRDtBQUNBLFVBQU1DLFFBQVEsR0FBRyxLQUFLWCxTQUFMLENBQWVVLFNBQWYsQ0FBakI7O0FBQ0EsVUFBTUUsWUFBWSxHQUFHRCxRQUFRLENBQUNFLFNBQTlCO0FBQ0EsVUFBTUMsWUFBWSxHQUFHLEtBQUtDLEtBQUwsQ0FBV0YsU0FBaEM7O0FBQ0EsUUFBSUQsWUFBWSxDQUFDSSxNQUFiLEtBQXdCRixZQUFZLENBQUNFLE1BQXpDLEVBQWlEO0FBQzdDLFdBQUtDLFFBQUwsQ0FBY04sUUFBZCxFQUQ2QyxDQUNwQjtBQUM1QixLQUZELE1BRU87QUFDSDtBQUNBLFdBQUssSUFBSU8sQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR04sWUFBWSxDQUFDSSxNQUFqQyxFQUF5Q0UsQ0FBQyxFQUExQyxFQUE4QztBQUMxQyxZQUFJSixZQUFZLENBQUNJLENBQUQsQ0FBWixLQUFvQk4sWUFBWSxDQUFDTSxDQUFELENBQXBDLEVBQXlDO0FBQ3JDLGVBQUtELFFBQUwsQ0FBY04sUUFBZCxFQURxQyxDQUNaOztBQUN6QjtBQUNIO0FBQ0o7QUFDSjtBQUNKLEdBbEUyQjtBQW9FNUJMLEVBQUFBLFlBQVksRUFBRSxVQUFTYSxTQUFULEVBQW9CQyxTQUFwQixFQUErQjtBQUN6QyxRQUFJLEtBQUtqQixTQUFULEVBQW9CLE9BRHFCLENBR3pDO0FBQ0E7O0FBQ0EsVUFBTWtCLFdBQVcsR0FBR0YsU0FBUyxLQUFLLE9BQWQsSUFBeUJDLFNBQVMsS0FBS0QsU0FBM0Q7O0FBQ0EsUUFBSUUsV0FBVyxJQUNYO0FBQ0EsU0FBS04sS0FBTCxDQUFXTyxTQUFYLEdBQXVCLENBRjNCLEVBR0U7QUFDRTtBQUNBLFdBQUtMLFFBQUwsQ0FBYztBQUNWSyxRQUFBQSxTQUFTLEVBQUU7QUFERCxPQUFkO0FBR0g7QUFDSixHQW5GMkI7QUFxRjVCdEIsRUFBQUEsU0FBUyxFQUFFLFVBQVNDLEtBQVQsRUFBZ0I7QUFDdkI7QUFDQTtBQUVBLFFBQUlyQixJQUFJLEdBQUcsRUFBWDs7QUFDQSxRQUFJLENBQUMyQyx1QkFBY0MsUUFBZCxDQUF1QixjQUF2QixDQUFMLEVBQTZDO0FBQ3pDNUMsTUFBQUEsSUFBSSxHQUFHcUIsS0FBSyxDQUFDckIsSUFBTixJQUFjLEVBQXJCOztBQUVBLFVBQUlxQixLQUFLLENBQUN0QixHQUFWLEVBQWU7QUFDWEMsUUFBQUEsSUFBSSxDQUFDNkMsT0FBTCxDQUFheEIsS0FBSyxDQUFDdEIsR0FBbkIsRUFEVyxDQUNjO0FBQzVCO0FBQ0o7O0FBRUQsUUFBSStDLGVBQWUsR0FBRyxJQUF0Qjs7QUFDQSxRQUFJekIsS0FBSyxDQUFDZixzQkFBVixFQUFrQztBQUM5QndDLE1BQUFBLGVBQWUsR0FBR0MsV0FBVyxDQUFDQyx5QkFBWixDQUNkM0IsS0FBSyxDQUFDeEIsTUFBTixJQUFnQndCLEtBQUssQ0FBQzVCLElBRFIsQ0FBbEI7QUFHQU8sTUFBQUEsSUFBSSxDQUFDaUQsSUFBTCxDQUFVSCxlQUFWLEVBSjhCLENBSUY7QUFDL0IsS0FuQnNCLENBcUJ2Qjs7O0FBQ0E5QyxJQUFBQSxJQUFJLEdBQUdrRCxLQUFLLENBQUNDLElBQU4sQ0FBVyxJQUFJQyxHQUFKLENBQVFwRCxJQUFSLENBQVgsQ0FBUDtBQUVBLFdBQU87QUFDSGlDLE1BQUFBLFNBQVMsRUFBRWpDLElBRFI7QUFFSDhDLE1BQUFBLGVBQWUsRUFBRUEsZUFGZDtBQUdISixNQUFBQSxTQUFTLEVBQUU7QUFIUixLQUFQO0FBS0gsR0FsSDJCO0FBb0g1QlcsRUFBQUEsT0FBTyxFQUFFLFVBQVNDLEVBQVQsRUFBYTtBQUNsQixVQUFNQyxTQUFTLEdBQUcsS0FBS3BCLEtBQUwsQ0FBV08sU0FBWCxHQUF1QixDQUF6Qzs7QUFDQSxRQUFJYSxTQUFTLEdBQUcsS0FBS3BCLEtBQUwsQ0FBV0YsU0FBWCxDQUFxQkcsTUFBckMsRUFBNkM7QUFDekM7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFDVkssUUFBQUEsU0FBUyxFQUFFYTtBQURELE9BQWQ7QUFHSDtBQUNKLEdBNUgyQjtBQThINUJDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsUUFBUSxHQUFHLEtBQUt0QixLQUFMLENBQVdGLFNBQVgsQ0FBcUIsS0FBS0UsS0FBTCxDQUFXTyxTQUFoQyxDQUFqQjtBQUVBLHdCQUlJLEtBQUtyQixLQUpUO0FBQUEsVUFBTTtBQUNGNUIsTUFBQUEsSUFERTtBQUNJSSxNQUFBQSxNQURKO0FBQ1lDLE1BQUFBLEtBRFo7QUFDbUJDLE1BQUFBLEdBRG5CO0FBQ3dCQyxNQUFBQSxJQUR4QjtBQUM4QkUsTUFBQUEsS0FEOUI7QUFDcUNFLE1BQUFBLE1BRHJDO0FBQzZDQyxNQUFBQSxZQUQ3QztBQUVGQyxNQUFBQSxzQkFGRTtBQUVzQm9ELE1BQUFBLE9BRnRCO0FBRStCbEQsTUFBQUE7QUFGL0IsS0FBTjtBQUFBLFVBR09tRCxVQUhQOztBQU1BLFFBQUlGLFFBQVEsS0FBSyxLQUFLdEIsS0FBTCxDQUFXVyxlQUE1QixFQUE2QztBQUN6QyxZQUFNYyxhQUFhLEdBQUdiLFdBQVcsQ0FBQ2MsZ0JBQVosQ0FBNkJwRSxJQUE3QixDQUF0Qjs7QUFDQSxZQUFNcUUsUUFBUSxHQUNWO0FBQU0sUUFBQSxTQUFTLEVBQUMsdUJBQWhCO0FBQXdDLHVCQUFZLE1BQXBEO0FBQ0ksUUFBQSxLQUFLLEVBQUU7QUFBRUMsVUFBQUEsUUFBUSxFQUFHN0QsS0FBSyxHQUFHLElBQVQsR0FBaUIsSUFBN0I7QUFDUEEsVUFBQUEsS0FBSyxFQUFFQSxLQUFLLEdBQUcsSUFEUjtBQUVQOEQsVUFBQUEsVUFBVSxFQUFFNUQsTUFBTSxHQUFHO0FBRmQ7QUFEWCxTQUtNd0QsYUFMTixDQURKOztBQVNBLFlBQU1LLE9BQU8sR0FDVDtBQUFLLFFBQUEsU0FBUyxFQUFDLHFCQUFmO0FBQXFDLFFBQUEsR0FBRyxFQUFFUixRQUExQztBQUNJLFFBQUEsR0FBRyxFQUFDLEVBRFI7QUFDVyxRQUFBLEtBQUssRUFBRTNELEtBRGxCO0FBQ3lCLFFBQUEsT0FBTyxFQUFFLEtBQUt1RCxPQUR2QztBQUVJLFFBQUEsS0FBSyxFQUFFbkQsS0FGWDtBQUVrQixRQUFBLE1BQU0sRUFBRUUsTUFGMUI7QUFFa0MsdUJBQVk7QUFGOUMsUUFESjs7QUFLQSxVQUFJc0QsT0FBTyxJQUFJLElBQWYsRUFBcUI7QUFDakIsZUFDSSw2QkFBQyx5QkFBRDtBQUFrQixVQUFBLE9BQU8sRUFBQyxNQUExQjtBQUFpQyxVQUFBLFNBQVMsRUFBQyxlQUEzQztBQUNJLFVBQUEsT0FBTyxFQUFFQSxPQURiO0FBQ3NCLFVBQUEsUUFBUSxFQUFFbEQ7QUFEaEMsV0FDOENtRCxVQUQ5QyxHQUdNRyxRQUhOLEVBSU1HLE9BSk4sQ0FESjtBQVFILE9BVEQsTUFTTztBQUNILGVBQ0k7QUFBTSxVQUFBLFNBQVMsRUFBQyxlQUFoQjtBQUFnQyxVQUFBLEdBQUcsRUFBRXpEO0FBQXJDLFdBQW1EbUQsVUFBbkQsR0FDTUcsUUFETixFQUVNRyxPQUZOLENBREo7QUFNSDtBQUNKOztBQUNELFFBQUlQLE9BQU8sSUFBSSxJQUFmLEVBQXFCO0FBQ2pCLGFBQ0ksNkJBQUMseUJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxtQ0FEZDtBQUVJLFFBQUEsT0FBTyxFQUFDLEtBRlo7QUFHSSxRQUFBLEdBQUcsRUFBRUQsUUFIVDtBQUlJLFFBQUEsT0FBTyxFQUFFQyxPQUpiO0FBS0ksUUFBQSxPQUFPLEVBQUUsS0FBS0wsT0FMbEI7QUFNSSxRQUFBLEtBQUssRUFBRW5ELEtBTlg7QUFNa0IsUUFBQSxNQUFNLEVBQUVFLE1BTjFCO0FBT0ksUUFBQSxLQUFLLEVBQUVOLEtBUFg7QUFPa0IsUUFBQSxHQUFHLEVBQUMsRUFQdEI7QUFRSSxRQUFBLFFBQVEsRUFBRVU7QUFSZCxTQVNRbUQsVUFUUixFQURKO0FBWUgsS0FiRCxNQWFPO0FBQ0gsYUFDSTtBQUNJLFFBQUEsU0FBUyxFQUFDLG1DQURkO0FBRUksUUFBQSxHQUFHLEVBQUVGLFFBRlQ7QUFHSSxRQUFBLE9BQU8sRUFBRSxLQUFLSixPQUhsQjtBQUlJLFFBQUEsS0FBSyxFQUFFbkQsS0FKWDtBQUlrQixRQUFBLE1BQU0sRUFBRUUsTUFKMUI7QUFLSSxRQUFBLEtBQUssRUFBRU4sS0FMWDtBQUtrQixRQUFBLEdBQUcsRUFBQyxFQUx0QjtBQU1JLFFBQUEsR0FBRyxFQUFFVTtBQU5ULFNBT1FtRCxVQVBSLEVBREo7QUFVSDtBQUNKO0FBbE0yQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0ICogYXMgQXZhdGFyTG9naWMgZnJvbSAnLi4vLi4vLi4vQXZhdGFyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSAnLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCBNYXRyaXhDbGllbnRDb250ZXh0IGZyb20gXCIuLi8uLi8uLi9jb250ZXh0cy9NYXRyaXhDbGllbnRDb250ZXh0XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnQmFzZUF2YXRhcicsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgbmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLCAvLyBUaGUgbmFtZSAoZmlyc3QgaW5pdGlhbCB1c2VkIGFzIGRlZmF1bHQpXHJcbiAgICAgICAgaWROYW1lOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBJRCBmb3IgZ2VuZXJhdGluZyBoYXNoIGNvbG91cnNcclxuICAgICAgICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZywgLy8gb25Ib3ZlciB0aXRsZSB0ZXh0XHJcbiAgICAgICAgdXJsOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBoaWdoZXN0IHByaW9yaXR5IG9mIHRoZW0gYWxsLCBzaG9ydGN1dCB0byBzZXQgaW4gdXJsc1swXVxyXG4gICAgICAgIHVybHM6IFByb3BUeXBlcy5hcnJheSwgLy8gW2hpZ2hlc3RfcHJpb3JpdHksIC4uLiAsIGxvd2VzdF9wcmlvcml0eV1cclxuICAgICAgICB3aWR0aDogUHJvcFR5cGVzLm51bWJlcixcclxuICAgICAgICBoZWlnaHQ6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICAgICAgLy8gWFhYIHJlc2l6ZU1ldGhvZCBub3QgYWN0dWFsbHkgdXNlZC5cclxuICAgICAgICByZXNpemVNZXRob2Q6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZGVmYXVsdFRvSW5pdGlhbExldHRlcjogUHJvcFR5cGVzLmJvb2wsIC8vIHRydWUgdG8gYWRkIGRlZmF1bHQgdXJsXHJcbiAgICAgICAgaW5wdXRSZWY6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1xyXG4gICAgICAgICAgICAvLyBFaXRoZXIgYSBmdW5jdGlvblxyXG4gICAgICAgICAgICBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICAgICAgLy8gT3IgdGhlIGluc3RhbmNlIG9mIGEgRE9NIG5hdGl2ZSBlbGVtZW50XHJcbiAgICAgICAgICAgIFByb3BUeXBlcy5zaGFwZSh7IGN1cnJlbnQ6IFByb3BUeXBlcy5pbnN0YW5jZU9mKEVsZW1lbnQpIH0pLFxyXG4gICAgICAgIF0pLFxyXG4gICAgfSxcclxuXHJcbiAgICBzdGF0aWNzOiB7XHJcbiAgICAgICAgY29udGV4dFR5cGU6IE1hdHJpeENsaWVudENvbnRleHQsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgd2lkdGg6IDQwLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IDQwLFxyXG4gICAgICAgICAgICByZXNpemVNZXRob2Q6ICdjcm9wJyxcclxuICAgICAgICAgICAgZGVmYXVsdFRvSW5pdGlhbExldHRlcjogdHJ1ZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nZXRTdGF0ZSh0aGlzLnByb3BzKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy51bm1vdW50ZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmNvbnRleHQub24oJ3N5bmMnLCB0aGlzLm9uQ2xpZW50U3luYyk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIHRoaXMudW5tb3VudGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmNvbnRleHQucmVtb3ZlTGlzdGVuZXIoJ3N5bmMnLCB0aGlzLm9uQ2xpZW50U3luYyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XHJcbiAgICAgICAgLy8gd29yayBvdXQgaWYgd2UgbmVlZCB0byBjYWxsIHNldFN0YXRlIChpZiB0aGUgaW1hZ2UgVVJMcyBhcnJheSBoYXMgY2hhbmdlZClcclxuICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHRoaXMuX2dldFN0YXRlKG5leHRQcm9wcyk7XHJcbiAgICAgICAgY29uc3QgbmV3SW1hZ2VVcmxzID0gbmV3U3RhdGUuaW1hZ2VVcmxzO1xyXG4gICAgICAgIGNvbnN0IG9sZEltYWdlVXJscyA9IHRoaXMuc3RhdGUuaW1hZ2VVcmxzO1xyXG4gICAgICAgIGlmIChuZXdJbWFnZVVybHMubGVuZ3RoICE9PSBvbGRJbWFnZVVybHMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpOyAvLyBkZXRlY3RlZCBhIG5ldyBlbnRyeVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIGNoZWNrIGVhY2ggb25lIHRvIHNlZSBpZiB0aGV5IGFyZSB0aGUgc2FtZVxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG5ld0ltYWdlVXJscy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaWYgKG9sZEltYWdlVXJsc1tpXSAhPT0gbmV3SW1hZ2VVcmxzW2ldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7IC8vIGRldGVjdGVkIGEgZGlmZlxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbkNsaWVudFN5bmM6IGZ1bmN0aW9uKHN5bmNTdGF0ZSwgcHJldlN0YXRlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudW5tb3VudGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vIENvbnNpZGVyIHRoZSBjbGllbnQgcmVjb25uZWN0ZWQgaWYgdGhlcmUgaXMgbm8gZXJyb3Igd2l0aCBzeW5jaW5nLlxyXG4gICAgICAgIC8vIFRoaXMgbWVhbnMgdGhlIHN0YXRlIGNvdWxkIGJlIFJFQ09OTkVDVElORywgU1lOQ0lORywgUFJFUEFSRUQgb3IgQ0FUQ0hVUC5cclxuICAgICAgICBjb25zdCByZWNvbm5lY3RlZCA9IHN5bmNTdGF0ZSAhPT0gXCJFUlJPUlwiICYmIHByZXZTdGF0ZSAhPT0gc3luY1N0YXRlO1xyXG4gICAgICAgIGlmIChyZWNvbm5lY3RlZCAmJlxyXG4gICAgICAgICAgICAvLyBEaWQgd2UgZmFsbCBiYWNrP1xyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnVybHNJbmRleCA+IDBcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgLy8gU3RhcnQgZnJvbSB0aGUgaGlnaGVzdCBwcmlvcml0eSBVUkwgYWdhaW5cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICB1cmxzSW5kZXg6IDAsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2dldFN0YXRlOiBmdW5jdGlvbihwcm9wcykge1xyXG4gICAgICAgIC8vIHdvcmsgb3V0IHRoZSBmdWxsIHNldCBvZiB1cmxzIHRvIHRyeSB0byBsb2FkLiBUaGlzIGlzIGZvcm1lZCBsaWtlIHNvOlxyXG4gICAgICAgIC8vIGltYWdlVXJsczogWyBwcm9wcy51cmwsIHByb3BzLnVybHMsIGRlZmF1bHQgaW1hZ2UgXVxyXG5cclxuICAgICAgICBsZXQgdXJscyA9IFtdO1xyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImxvd0JhbmR3aWR0aFwiKSkge1xyXG4gICAgICAgICAgICB1cmxzID0gcHJvcHMudXJscyB8fCBbXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChwcm9wcy51cmwpIHtcclxuICAgICAgICAgICAgICAgIHVybHMudW5zaGlmdChwcm9wcy51cmwpOyAvLyBwdXQgaW4gdXJsc1swXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZGVmYXVsdEltYWdlVXJsID0gbnVsbDtcclxuICAgICAgICBpZiAocHJvcHMuZGVmYXVsdFRvSW5pdGlhbExldHRlcikge1xyXG4gICAgICAgICAgICBkZWZhdWx0SW1hZ2VVcmwgPSBBdmF0YXJMb2dpYy5kZWZhdWx0QXZhdGFyVXJsRm9yU3RyaW5nKFxyXG4gICAgICAgICAgICAgICAgcHJvcHMuaWROYW1lIHx8IHByb3BzLm5hbWUsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHVybHMucHVzaChkZWZhdWx0SW1hZ2VVcmwpOyAvLyBsb3dlc3QgcHJpb3JpdHlcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGRlZHVwbGljYXRlIFVSTHNcclxuICAgICAgICB1cmxzID0gQXJyYXkuZnJvbShuZXcgU2V0KHVybHMpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaW1hZ2VVcmxzOiB1cmxzLFxyXG4gICAgICAgICAgICBkZWZhdWx0SW1hZ2VVcmw6IGRlZmF1bHRJbWFnZVVybCxcclxuICAgICAgICAgICAgdXJsc0luZGV4OiAwLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRXJyb3I6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgY29uc3QgbmV4dEluZGV4ID0gdGhpcy5zdGF0ZS51cmxzSW5kZXggKyAxO1xyXG4gICAgICAgIGlmIChuZXh0SW5kZXggPCB0aGlzLnN0YXRlLmltYWdlVXJscy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgLy8gdHJ5IHRoZSBuZXh0IG9uZVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHVybHNJbmRleDogbmV4dEluZGV4LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgaW1hZ2VVcmwgPSB0aGlzLnN0YXRlLmltYWdlVXJsc1t0aGlzLnN0YXRlLnVybHNJbmRleF07XHJcblxyXG4gICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgbmFtZSwgaWROYW1lLCB0aXRsZSwgdXJsLCB1cmxzLCB3aWR0aCwgaGVpZ2h0LCByZXNpemVNZXRob2QsXHJcbiAgICAgICAgICAgIGRlZmF1bHRUb0luaXRpYWxMZXR0ZXIsIG9uQ2xpY2ssIGlucHV0UmVmLFxyXG4gICAgICAgICAgICAuLi5vdGhlclByb3BzXHJcbiAgICAgICAgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgICAgIGlmIChpbWFnZVVybCA9PT0gdGhpcy5zdGF0ZS5kZWZhdWx0SW1hZ2VVcmwpIHtcclxuICAgICAgICAgICAgY29uc3QgaW5pdGlhbExldHRlciA9IEF2YXRhckxvZ2ljLmdldEluaXRpYWxMZXR0ZXIobmFtZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRleHROb2RlID0gKFxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfQmFzZUF2YXRhcl9pbml0aWFsXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCJcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogKHdpZHRoICogMC42NSkgKyBcInB4XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHdpZHRoICsgXCJweFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IGhlaWdodCArIFwicHhcIiB9fVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgaW5pdGlhbExldHRlciB9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNvbnN0IGltZ05vZGUgPSAoXHJcbiAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X0Jhc2VBdmF0YXJfaW1hZ2VcIiBzcmM9e2ltYWdlVXJsfVxyXG4gICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiIHRpdGxlPXt0aXRsZX0gb25FcnJvcj17dGhpcy5vbkVycm9yfVxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPXt3aWR0aH0gaGVpZ2h0PXtoZWlnaHR9IGFyaWEtaGlkZGVuPVwidHJ1ZVwiIC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmIChvbkNsaWNrICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gZWxlbWVudD0nc3BhbicgY2xhc3NOYW1lPVwibXhfQmFzZUF2YXRhclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e29uQ2xpY2t9IGlucHV0UmVmPXtpbnB1dFJlZn0gey4uLm90aGVyUHJvcHN9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRleHROb2RlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBpbWdOb2RlIH1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9CYXNlQXZhdGFyXCIgcmVmPXtpbnB1dFJlZn0gey4uLm90aGVyUHJvcHN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRleHROb2RlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBpbWdOb2RlIH1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChvbkNsaWNrICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfQmFzZUF2YXRhciBteF9CYXNlQXZhdGFyX2ltYWdlXCJcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50PSdpbWcnXHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtpbWFnZVVybH1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtvbkNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uRXJyb3I9e3RoaXMub25FcnJvcn1cclxuICAgICAgICAgICAgICAgICAgICB3aWR0aD17d2lkdGh9IGhlaWdodD17aGVpZ2h0fVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXt0aXRsZX0gYWx0PVwiXCJcclxuICAgICAgICAgICAgICAgICAgICBpbnB1dFJlZj17aW5wdXRSZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgey4uLm90aGVyUHJvcHN9IC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9CYXNlQXZhdGFyIG14X0Jhc2VBdmF0YXJfaW1hZ2VcIlxyXG4gICAgICAgICAgICAgICAgICAgIHNyYz17aW1hZ2VVcmx9XHJcbiAgICAgICAgICAgICAgICAgICAgb25FcnJvcj17dGhpcy5vbkVycm9yfVxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPXt3aWR0aH0gaGVpZ2h0PXtoZWlnaHR9XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3RpdGxlfSBhbHQ9XCJcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJlZj17aW5wdXRSZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgey4uLm90aGVyUHJvcHN9IC8+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==