"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _ContextMenu = require("../../structures/ContextMenu");

var RoomNotifs = _interopRequireWildcard(require("../../../RoomNotifs"));

var FormattingUtils = _interopRequireWildcard(require("../../../utils/FormattingUtils"));

var _ActiveRoomObserver = _interopRequireDefault(require("../../../ActiveRoomObserver"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _languageHandler = require("../../../languageHandler");

var _RovingTabIndex = require("../../../accessibility/RovingTabIndex");

var _E2EIcon = _interopRequireDefault(require("./E2EIcon"));

var _InviteOnlyIcon = _interopRequireDefault(require("./InviteOnlyIcon"));

var _ratelimitedfunc = _interopRequireDefault(require("../../../ratelimitedfunc"));

var _ShieldUtils = require("../../../utils/ShieldUtils");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd
Copyright 2018 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// eslint-disable-next-line camelcase
var _default = (0, _createReactClass.default)({
  displayName: 'RoomTile',
  propTypes: {
    onClick: _propTypes.default.func,
    room: _propTypes.default.object.isRequired,
    collapsed: _propTypes.default.bool.isRequired,
    unread: _propTypes.default.bool.isRequired,
    highlight: _propTypes.default.bool.isRequired,
    // If true, apply mx_RoomTile_transparent class
    transparent: _propTypes.default.bool,
    isInvite: _propTypes.default.bool.isRequired,
    incomingCall: _propTypes.default.object
  },
  getDefaultProps: function () {
    return {
      isDragging: false
    };
  },
  getInitialState: function () {
    const joinRules = this.props.room.currentState.getStateEvents("m.room.join_rules", "");
    const joinRule = joinRules && joinRules.getContent().join_rule;
    return {
      joinRule,
      hover: false,
      badgeHover: false,
      contextMenuPosition: null,
      // DOM bounding box, null if non-shown
      roomName: this.props.room.name,
      notifState: RoomNotifs.getRoomNotifsState(this.props.room.roomId),
      notificationCount: this.props.room.getUnreadNotificationCount(),
      selected: this.props.room.roomId === _RoomViewStore.default.getRoomId(),
      statusMessage: this._getStatusMessage(),
      e2eStatus: null
    };
  },

  _shouldShowStatusMessage() {
    if (!_SettingsStore.default.isFeatureEnabled("feature_custom_status")) {
      return false;
    }

    const isInvite = this.props.room.getMyMembership() === "invite";
    const isJoined = this.props.room.getMyMembership() === "join";
    const looksLikeDm = this.props.room.getInvitedAndJoinedMemberCount() === 2;
    return !isInvite && isJoined && looksLikeDm;
  },

  _getStatusMessageUser() {
    if (!_MatrixClientPeg.MatrixClientPeg.get()) return null; // We've probably been logged out

    const selfId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

    const otherMember = this.props.room.currentState.getMembersExcept([selfId])[0];

    if (!otherMember) {
      return null;
    }

    return otherMember.user;
  },

  _getStatusMessage() {
    const statusUser = this._getStatusMessageUser();

    if (!statusUser) {
      return "";
    }

    return statusUser._unstable_statusMessage;
  },

  onRoomStateMember: function (ev, state, member) {
    // we only care about leaving users
    // because trust state will change if someone joins a megolm session anyway
    if (member.membership !== "leave") {
      return;
    } // ignore members in other rooms


    if (member.roomId !== this.props.room.roomId) {
      return;
    }

    this._updateE2eStatus();
  },
  onUserVerificationChanged: function (userId, _trustStatus) {
    if (!this.props.room.getMember(userId)) {
      // Not in this room
      return;
    }

    this._updateE2eStatus();
  },
  onRoomTimeline: function (ev, room) {
    if (!room) return;
    if (room.roomId != this.props.room.roomId) return;
    if (ev.getType() !== "m.room.encryption") return;

    _MatrixClientPeg.MatrixClientPeg.get().removeListener("Room.timeline", this.onRoomTimeline);

    this.onFindingRoomToBeEncrypted();
  },
  onFindingRoomToBeEncrypted: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.on("RoomState.members", this.onRoomStateMember);
    cli.on("userTrustStatusChanged", this.onUserVerificationChanged);

    this._updateE2eStatus();
  },
  _updateE2eStatus: async function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (!cli.isRoomEncrypted(this.props.room.roomId)) {
      return;
    }

    if (!_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      return;
    }
    /* At this point, the user has encryption on and cross-signing on */


    this.setState({
      e2eStatus: await (0, _ShieldUtils.shieldStatusForRoom)(cli, this.props.room)
    });
  },
  onRoomName: function (room) {
    if (room !== this.props.room) return;
    this.setState({
      roomName: this.props.room.name
    });
  },
  onJoinRule: function (ev) {
    if (ev.getType() !== "m.room.join_rules") return;
    if (ev.getRoomId() !== this.props.room.roomId) return;
    this.setState({
      joinRule: ev.getContent().join_rule
    });
  },
  onAccountData: function (accountDataEvent) {
    if (accountDataEvent.getType() === 'm.push_rules') {
      this.setState({
        notifState: RoomNotifs.getRoomNotifsState(this.props.room.roomId)
      });
    }
  },
  onAction: function (payload) {
    switch (payload.action) {
      // XXX: slight hack in order to zero the notification count when a room
      // is read. Ideally this state would be given to this via props (as we
      // do with `unread`). This is still better than forceUpdating the entire
      // RoomList when a room is read.
      case 'on_room_read':
        if (payload.roomId !== this.props.room.roomId) break;
        this.setState({
          notificationCount: this.props.room.getUnreadNotificationCount()
        });
        break;
      // RoomTiles are one of the few components that may show custom status and
      // also remain on screen while in Settings toggling the feature.  This ensures
      // you can clearly see the status hide and show when toggling the feature.

      case 'feature_custom_status_changed':
        this.forceUpdate();
        break;

      case 'view_room':
        // when the room is selected make sure its tile is visible, for breadcrumbs/keyboard shortcut access
        if (payload.room_id === this.props.room.roomId && payload.show_room_tile) {
          this._scrollIntoView();
        }

        break;
    }
  },
  _scrollIntoView: function () {
    if (!this._roomTile.current) return;

    this._roomTile.current.scrollIntoView({
      block: "nearest",
      behavior: "auto"
    });
  },
  _onActiveRoomChange: function () {
    this.setState({
      selected: this.props.room.roomId === _RoomViewStore.default.getRoomId()
    });
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._roomTile = (0, _react.createRef)();
  },
  componentDidMount: function () {
    /* We bind here rather than in the definition because otherwise we wind up with the
       method only being callable once every 500ms across all instances, which would be wrong */
    this._updateE2eStatus = (0, _ratelimitedfunc.default)(this._updateE2eStatus, 500);

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.on("accountData", this.onAccountData);
    cli.on("Room.name", this.onRoomName);
    cli.on("RoomState.events", this.onJoinRule);

    if (cli.isRoomEncrypted(this.props.room.roomId)) {
      this.onFindingRoomToBeEncrypted();
    } else {
      cli.on("Room.timeline", this.onRoomTimeline);
    }

    _ActiveRoomObserver.default.addListener(this.props.room.roomId, this._onActiveRoomChange);

    this.dispatcherRef = _dispatcher.default.register(this.onAction);

    if (this._shouldShowStatusMessage()) {
      const statusUser = this._getStatusMessageUser();

      if (statusUser) {
        statusUser.on("User._unstable_statusMessage", this._onStatusMessageCommitted);
      }
    } // when we're first rendered (or our sublist is expanded) make sure we are visible if we're active


    if (this.state.selected) {
      this._scrollIntoView();
    }
  },
  componentWillUnmount: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli) {
      _MatrixClientPeg.MatrixClientPeg.get().removeListener("accountData", this.onAccountData);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("Room.name", this.onRoomName);

      cli.removeListener("RoomState.events", this.onJoinRule);
      cli.removeListener("RoomState.members", this.onRoomStateMember);
      cli.removeListener("userTrustStatusChanged", this.onUserVerificationChanged);
      cli.removeListener("Room.timeline", this.onRoomTimeline);
    }

    _ActiveRoomObserver.default.removeListener(this.props.room.roomId, this._onActiveRoomChange);

    _dispatcher.default.unregister(this.dispatcherRef);

    if (this._shouldShowStatusMessage()) {
      const statusUser = this._getStatusMessageUser();

      if (statusUser) {
        statusUser.removeListener("User._unstable_statusMessage", this._onStatusMessageCommitted);
      }
    }
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (props) {
    // XXX: This could be a lot better - this makes the assumption that
    // the notification count may have changed when the properties of
    // the room tile change.
    this.setState({
      notificationCount: this.props.room.getUnreadNotificationCount()
    });
  },
  // Do a simple shallow comparison of props and state to avoid unnecessary
  // renders. The assumption made here is that only state and props are used
  // in rendering this component and children.
  //
  // RoomList is frequently made to forceUpdate, so this decreases number of
  // RoomTile renderings.
  shouldComponentUpdate: function (newProps, newState) {
    if (Object.keys(newProps).some(k => newProps[k] !== this.props[k])) {
      return true;
    }

    if (Object.keys(newState).some(k => newState[k] !== this.state[k])) {
      return true;
    }

    return false;
  },

  _onStatusMessageCommitted() {
    // The status message `User` object has observed a message change.
    this.setState({
      statusMessage: this._getStatusMessage()
    });
  },

  onClick: function (ev) {
    if (this.props.onClick) {
      this.props.onClick(this.props.room.roomId, ev);
    }
  },
  onMouseEnter: function () {
    this.setState({
      hover: true
    });
    this.badgeOnMouseEnter();
  },
  onMouseLeave: function () {
    this.setState({
      hover: false
    });
    this.badgeOnMouseLeave();
  },
  badgeOnMouseEnter: function () {
    // Only allow non-guests to access the context menu
    // and only change it if it needs to change
    if (!_MatrixClientPeg.MatrixClientPeg.get().isGuest() && !this.state.badgeHover) {
      this.setState({
        badgeHover: true
      });
    }
  },
  badgeOnMouseLeave: function () {
    this.setState({
      badgeHover: false
    });
  },
  _showContextMenu: function (boundingClientRect) {
    // Only allow non-guests to access the context menu
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) return;
    const state = {
      contextMenuPosition: boundingClientRect
    }; // If the badge is clicked, then no longer show tooltip

    if (this.props.collapsed) {
      state.hover = false;
    }

    this.setState(state);
  },
  onContextMenuButtonClick: function (e) {
    // Prevent the RoomTile onClick event firing as well
    e.stopPropagation();
    e.preventDefault();

    this._showContextMenu(e.target.getBoundingClientRect());
  },
  onContextMenu: function (e) {
    // Prevent the native context menu
    e.preventDefault();

    this._showContextMenu({
      right: e.clientX,
      top: e.clientY,
      height: 0
    });
  },
  closeMenu: function () {
    this.setState({
      contextMenuPosition: null
    });
    this.props.refreshSubList();
  },
  render: function () {
    const isInvite = this.props.room.getMyMembership() === "invite";
    const notificationCount = this.props.notificationCount; // var highlightCount = this.props.room.getUnreadNotificationCount("highlight");

    const notifBadges = notificationCount > 0 && RoomNotifs.shouldShowNotifBadge(this.state.notifState);
    const mentionBadges = this.props.highlight && RoomNotifs.shouldShowMentionBadge(this.state.notifState);
    const badges = notifBadges || mentionBadges;
    let subtext = null;

    if (this._shouldShowStatusMessage()) {
      subtext = this.state.statusMessage;
    }

    const isMenuDisplayed = Boolean(this.state.contextMenuPosition);

    const dmUserId = _DMRoomMap.default.shared().getUserIdForRoomId(this.props.room.roomId);

    const classes = (0, _classnames.default)({
      'mx_RoomTile': true,
      'mx_RoomTile_selected': this.state.selected,
      'mx_RoomTile_unread': this.props.unread,
      'mx_RoomTile_unreadNotify': notifBadges,
      'mx_RoomTile_highlight': mentionBadges,
      'mx_RoomTile_invited': isInvite,
      'mx_RoomTile_menuDisplayed': isMenuDisplayed,
      'mx_RoomTile_noBadges': !badges,
      'mx_RoomTile_transparent': this.props.transparent,
      'mx_RoomTile_hasSubtext': subtext && !this.props.collapsed
    });
    const avatarClasses = (0, _classnames.default)({
      'mx_RoomTile_avatar': true
    });
    const badgeClasses = (0, _classnames.default)({
      'mx_RoomTile_badge': true,
      'mx_RoomTile_badgeButton': this.state.badgeHover || isMenuDisplayed
    });
    let name = this.state.roomName;
    if (name == undefined || name == null) name = '';
    name = name.replace(":", ":\u200b"); // add a zero-width space to allow linewrapping after the colon

    let badge;

    if (badges) {
      const limitedCount = FormattingUtils.formatCount(notificationCount);
      const badgeContent = notificationCount ? limitedCount : '!';
      badge = _react.default.createElement("div", {
        className: badgeClasses
      }, badgeContent);
    }

    let label;
    let subtextLabel;
    let tooltip;

    if (!this.props.collapsed) {
      const nameClasses = (0, _classnames.default)({
        'mx_RoomTile_name': true,
        'mx_RoomTile_invite': this.props.isInvite,
        'mx_RoomTile_badgeShown': badges || this.state.badgeHover || isMenuDisplayed
      });
      subtextLabel = subtext ? _react.default.createElement("span", {
        className: "mx_RoomTile_subtext"
      }, subtext) : null; // XXX: this is a workaround for Firefox giving this div a tabstop :( [tabIndex]

      label = _react.default.createElement("div", {
        title: name,
        className: nameClasses,
        tabIndex: -1,
        dir: "auto"
      }, name);
    } else if (this.state.hover) {
      const Tooltip = sdk.getComponent("elements.Tooltip");
      tooltip = _react.default.createElement(Tooltip, {
        className: "mx_RoomTile_tooltip",
        label: this.props.room.name,
        dir: "auto"
      });
    } //var incomingCallBox;
    //if (this.props.incomingCall) {
    //    var IncomingCallBox = sdk.getComponent("voip.IncomingCallBox");
    //    incomingCallBox = <IncomingCallBox incomingCall={ this.props.incomingCall }/>;
    //}


    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let contextMenuButton;

    if (!_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
      contextMenuButton = _react.default.createElement(_ContextMenu.ContextMenuButton, {
        className: "mx_RoomTile_menuButton",
        label: (0, _languageHandler._t)("Options"),
        isExpanded: isMenuDisplayed,
        onClick: this.onContextMenuButtonClick
      });
    }

    const RoomAvatar = sdk.getComponent('avatars.RoomAvatar');
    let ariaLabel = name;
    let dmIndicator;
    let dmOnline;
    /* Post-cross-signing we don't show DM indicators at all, instead relying on user
       context to let them know when that is. */

    if (dmUserId && !_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      dmIndicator = _react.default.createElement("img", {
        src: require("../../../../res/img/icon_person.svg"),
        className: "mx_RoomTile_dm",
        width: "11",
        height: "13",
        alt: "dm"
      });
    }

    const {
      room
    } = this.props;
    const member = room.getMember(dmUserId);

    if (member && member.membership === "join" && room.getJoinedMemberCount() === 2 && _SettingsStore.default.isFeatureEnabled("feature_presence_in_room_list")) {
      const UserOnlineDot = sdk.getComponent('rooms.UserOnlineDot');
      dmOnline = _react.default.createElement(UserOnlineDot, {
        userId: dmUserId
      });
    } // The following labels are written in such a fashion to increase screen reader efficiency (speed).


    if (notifBadges && mentionBadges && !isInvite) {
      ariaLabel += " " + (0, _languageHandler._t)("%(count)s unread messages including mentions.", {
        count: notificationCount
      });
    } else if (notifBadges) {
      ariaLabel += " " + (0, _languageHandler._t)("%(count)s unread messages.", {
        count: notificationCount
      });
    } else if (mentionBadges && !isInvite) {
      ariaLabel += " " + (0, _languageHandler._t)("Unread mentions.");
    } else if (this.props.unread) {
      ariaLabel += " " + (0, _languageHandler._t)("Unread messages.");
    }

    let contextMenu;

    if (isMenuDisplayed) {
      const RoomTileContextMenu = sdk.getComponent('context_menus.RoomTileContextMenu');
      contextMenu = _react.default.createElement(_ContextMenu.ContextMenu, (0, _extends2.default)({}, (0, _ContextMenu.toRightOf)(this.state.contextMenuPosition), {
        onFinished: this.closeMenu
      }), _react.default.createElement(RoomTileContextMenu, {
        room: this.props.room,
        onFinished: this.closeMenu
      }));
    }

    let privateIcon = null;

    if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      if (this.state.joinRule == "invite" && !dmUserId) {
        privateIcon = _react.default.createElement(_InviteOnlyIcon.default, {
          collapsedPanel: this.props.collapsed
        });
      }
    }

    let e2eIcon = null;

    if (this.state.e2eStatus) {
      e2eIcon = _react.default.createElement(_E2EIcon.default, {
        status: this.state.e2eStatus,
        className: "mx_RoomTile_e2eIcon"
      });
    }

    return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_RovingTabIndex.RovingTabIndexWrapper, {
      inputRef: this._roomTile
    }, ({
      onFocus,
      isActive,
      ref
    }) => _react.default.createElement(AccessibleButton, {
      onFocus: onFocus,
      tabIndex: isActive ? 0 : -1,
      inputRef: ref,
      className: classes,
      onClick: this.onClick,
      onMouseEnter: this.onMouseEnter,
      onMouseLeave: this.onMouseLeave,
      onContextMenu: this.onContextMenu,
      "aria-label": ariaLabel,
      "aria-selected": this.state.selected,
      role: "treeitem"
    }, _react.default.createElement("div", {
      className: avatarClasses
    }, _react.default.createElement("div", {
      className: "mx_RoomTile_avatar_container"
    }, _react.default.createElement(RoomAvatar, {
      room: this.props.room,
      width: 24,
      height: 24
    }), dmIndicator, e2eIcon)), privateIcon, _react.default.createElement("div", {
      className: "mx_RoomTile_nameContainer"
    }, _react.default.createElement("div", {
      className: "mx_RoomTile_labelContainer"
    }, label, subtextLabel), dmOnline, contextMenuButton, badge), tooltip)), contextMenu);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21UaWxlLmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwib25DbGljayIsIlByb3BUeXBlcyIsImZ1bmMiLCJyb29tIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImNvbGxhcHNlZCIsImJvb2wiLCJ1bnJlYWQiLCJoaWdobGlnaHQiLCJ0cmFuc3BhcmVudCIsImlzSW52aXRlIiwiaW5jb21pbmdDYWxsIiwiZ2V0RGVmYXVsdFByb3BzIiwiaXNEcmFnZ2luZyIsImdldEluaXRpYWxTdGF0ZSIsImpvaW5SdWxlcyIsInByb3BzIiwiY3VycmVudFN0YXRlIiwiZ2V0U3RhdGVFdmVudHMiLCJqb2luUnVsZSIsImdldENvbnRlbnQiLCJqb2luX3J1bGUiLCJob3ZlciIsImJhZGdlSG92ZXIiLCJjb250ZXh0TWVudVBvc2l0aW9uIiwicm9vbU5hbWUiLCJuYW1lIiwibm90aWZTdGF0ZSIsIlJvb21Ob3RpZnMiLCJnZXRSb29tTm90aWZzU3RhdGUiLCJyb29tSWQiLCJub3RpZmljYXRpb25Db3VudCIsImdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50Iiwic2VsZWN0ZWQiLCJSb29tVmlld1N0b3JlIiwiZ2V0Um9vbUlkIiwic3RhdHVzTWVzc2FnZSIsIl9nZXRTdGF0dXNNZXNzYWdlIiwiZTJlU3RhdHVzIiwiX3Nob3VsZFNob3dTdGF0dXNNZXNzYWdlIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJnZXRNeU1lbWJlcnNoaXAiLCJpc0pvaW5lZCIsImxvb2tzTGlrZURtIiwiZ2V0SW52aXRlZEFuZEpvaW5lZE1lbWJlckNvdW50IiwiX2dldFN0YXR1c01lc3NhZ2VVc2VyIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwic2VsZklkIiwiZ2V0VXNlcklkIiwib3RoZXJNZW1iZXIiLCJnZXRNZW1iZXJzRXhjZXB0IiwidXNlciIsInN0YXR1c1VzZXIiLCJfdW5zdGFibGVfc3RhdHVzTWVzc2FnZSIsIm9uUm9vbVN0YXRlTWVtYmVyIiwiZXYiLCJzdGF0ZSIsIm1lbWJlciIsIm1lbWJlcnNoaXAiLCJfdXBkYXRlRTJlU3RhdHVzIiwib25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCIsInVzZXJJZCIsIl90cnVzdFN0YXR1cyIsImdldE1lbWJlciIsIm9uUm9vbVRpbWVsaW5lIiwiZ2V0VHlwZSIsInJlbW92ZUxpc3RlbmVyIiwib25GaW5kaW5nUm9vbVRvQmVFbmNyeXB0ZWQiLCJjbGkiLCJvbiIsImlzUm9vbUVuY3J5cHRlZCIsInNldFN0YXRlIiwib25Sb29tTmFtZSIsIm9uSm9pblJ1bGUiLCJvbkFjY291bnREYXRhIiwiYWNjb3VudERhdGFFdmVudCIsIm9uQWN0aW9uIiwicGF5bG9hZCIsImFjdGlvbiIsImZvcmNlVXBkYXRlIiwicm9vbV9pZCIsInNob3dfcm9vbV90aWxlIiwiX3Njcm9sbEludG9WaWV3IiwiX3Jvb21UaWxlIiwiY3VycmVudCIsInNjcm9sbEludG9WaWV3IiwiYmxvY2siLCJiZWhhdmlvciIsIl9vbkFjdGl2ZVJvb21DaGFuZ2UiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiY29tcG9uZW50RGlkTW91bnQiLCJBY3RpdmVSb29tT2JzZXJ2ZXIiLCJhZGRMaXN0ZW5lciIsImRpc3BhdGNoZXJSZWYiLCJkaXMiLCJyZWdpc3RlciIsIl9vblN0YXR1c01lc3NhZ2VDb21taXR0ZWQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyIsInNob3VsZENvbXBvbmVudFVwZGF0ZSIsIm5ld1Byb3BzIiwibmV3U3RhdGUiLCJPYmplY3QiLCJrZXlzIiwic29tZSIsImsiLCJvbk1vdXNlRW50ZXIiLCJiYWRnZU9uTW91c2VFbnRlciIsIm9uTW91c2VMZWF2ZSIsImJhZGdlT25Nb3VzZUxlYXZlIiwiaXNHdWVzdCIsIl9zaG93Q29udGV4dE1lbnUiLCJib3VuZGluZ0NsaWVudFJlY3QiLCJvbkNvbnRleHRNZW51QnV0dG9uQ2xpY2siLCJlIiwic3RvcFByb3BhZ2F0aW9uIiwicHJldmVudERlZmF1bHQiLCJ0YXJnZXQiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJvbkNvbnRleHRNZW51IiwicmlnaHQiLCJjbGllbnRYIiwidG9wIiwiY2xpZW50WSIsImhlaWdodCIsImNsb3NlTWVudSIsInJlZnJlc2hTdWJMaXN0IiwicmVuZGVyIiwibm90aWZCYWRnZXMiLCJzaG91bGRTaG93Tm90aWZCYWRnZSIsIm1lbnRpb25CYWRnZXMiLCJzaG91bGRTaG93TWVudGlvbkJhZGdlIiwiYmFkZ2VzIiwic3VidGV4dCIsImlzTWVudURpc3BsYXllZCIsIkJvb2xlYW4iLCJkbVVzZXJJZCIsIkRNUm9vbU1hcCIsInNoYXJlZCIsImdldFVzZXJJZEZvclJvb21JZCIsImNsYXNzZXMiLCJhdmF0YXJDbGFzc2VzIiwiYmFkZ2VDbGFzc2VzIiwidW5kZWZpbmVkIiwicmVwbGFjZSIsImJhZGdlIiwibGltaXRlZENvdW50IiwiRm9ybWF0dGluZ1V0aWxzIiwiZm9ybWF0Q291bnQiLCJiYWRnZUNvbnRlbnQiLCJsYWJlbCIsInN1YnRleHRMYWJlbCIsInRvb2x0aXAiLCJuYW1lQ2xhc3NlcyIsIlRvb2x0aXAiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJBY2Nlc3NpYmxlQnV0dG9uIiwiY29udGV4dE1lbnVCdXR0b24iLCJSb29tQXZhdGFyIiwiYXJpYUxhYmVsIiwiZG1JbmRpY2F0b3IiLCJkbU9ubGluZSIsInJlcXVpcmUiLCJnZXRKb2luZWRNZW1iZXJDb3VudCIsIlVzZXJPbmxpbmVEb3QiLCJjb3VudCIsImNvbnRleHRNZW51IiwiUm9vbVRpbGVDb250ZXh0TWVudSIsInByaXZhdGVJY29uIiwiZTJlSWNvbiIsIm9uRm9jdXMiLCJpc0FjdGl2ZSIsInJlZiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUF2Q0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXFDQTtlQUllLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLFVBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxPQUFPLEVBQUVDLG1CQUFVQyxJQURaO0FBR1BDLElBQUFBLElBQUksRUFBRUYsbUJBQVVHLE1BQVYsQ0FBaUJDLFVBSGhCO0FBSVBDLElBQUFBLFNBQVMsRUFBRUwsbUJBQVVNLElBQVYsQ0FBZUYsVUFKbkI7QUFLUEcsSUFBQUEsTUFBTSxFQUFFUCxtQkFBVU0sSUFBVixDQUFlRixVQUxoQjtBQU1QSSxJQUFBQSxTQUFTLEVBQUVSLG1CQUFVTSxJQUFWLENBQWVGLFVBTm5CO0FBT1A7QUFDQUssSUFBQUEsV0FBVyxFQUFFVCxtQkFBVU0sSUFSaEI7QUFTUEksSUFBQUEsUUFBUSxFQUFFVixtQkFBVU0sSUFBVixDQUFlRixVQVRsQjtBQVVQTyxJQUFBQSxZQUFZLEVBQUVYLG1CQUFVRztBQVZqQixHQUhpQjtBQWdCNUJTLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsVUFBVSxFQUFFO0FBRFQsS0FBUDtBQUdILEdBcEIyQjtBQXNCNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFVBQU1DLFNBQVMsR0FBRyxLQUFLQyxLQUFMLENBQVdkLElBQVgsQ0FBZ0JlLFlBQWhCLENBQTZCQyxjQUE3QixDQUE0QyxtQkFBNUMsRUFBaUUsRUFBakUsQ0FBbEI7QUFDQSxVQUFNQyxRQUFRLEdBQUdKLFNBQVMsSUFBSUEsU0FBUyxDQUFDSyxVQUFWLEdBQXVCQyxTQUFyRDtBQUVBLFdBQVE7QUFDSkYsTUFBQUEsUUFESTtBQUVKRyxNQUFBQSxLQUFLLEVBQUUsS0FGSDtBQUdKQyxNQUFBQSxVQUFVLEVBQUUsS0FIUjtBQUlKQyxNQUFBQSxtQkFBbUIsRUFBRSxJQUpqQjtBQUl1QjtBQUMzQkMsTUFBQUEsUUFBUSxFQUFFLEtBQUtULEtBQUwsQ0FBV2QsSUFBWCxDQUFnQndCLElBTHRCO0FBTUpDLE1BQUFBLFVBQVUsRUFBRUMsVUFBVSxDQUFDQyxrQkFBWCxDQUE4QixLQUFLYixLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUE5QyxDQU5SO0FBT0pDLE1BQUFBLGlCQUFpQixFQUFFLEtBQUtmLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjhCLDBCQUFoQixFQVBmO0FBUUpDLE1BQUFBLFFBQVEsRUFBRSxLQUFLakIsS0FBTCxDQUFXZCxJQUFYLENBQWdCNEIsTUFBaEIsS0FBMkJJLHVCQUFjQyxTQUFkLEVBUmpDO0FBU0pDLE1BQUFBLGFBQWEsRUFBRSxLQUFLQyxpQkFBTCxFQVRYO0FBVUpDLE1BQUFBLFNBQVMsRUFBRTtBQVZQLEtBQVI7QUFZSCxHQXRDMkI7O0FBd0M1QkMsRUFBQUEsd0JBQXdCLEdBQUc7QUFDdkIsUUFBSSxDQUFDQyx1QkFBY0MsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQUwsRUFBOEQ7QUFDMUQsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsVUFBTS9CLFFBQVEsR0FBRyxLQUFLTSxLQUFMLENBQVdkLElBQVgsQ0FBZ0J3QyxlQUFoQixPQUFzQyxRQUF2RDtBQUNBLFVBQU1DLFFBQVEsR0FBRyxLQUFLM0IsS0FBTCxDQUFXZCxJQUFYLENBQWdCd0MsZUFBaEIsT0FBc0MsTUFBdkQ7QUFDQSxVQUFNRSxXQUFXLEdBQUcsS0FBSzVCLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjJDLDhCQUFoQixPQUFxRCxDQUF6RTtBQUNBLFdBQU8sQ0FBQ25DLFFBQUQsSUFBYWlDLFFBQWIsSUFBeUJDLFdBQWhDO0FBQ0gsR0FoRDJCOztBQWtENUJFLEVBQUFBLHFCQUFxQixHQUFHO0FBQ3BCLFFBQUksQ0FBQ0MsaUNBQWdCQyxHQUFoQixFQUFMLEVBQTRCLE9BQU8sSUFBUCxDQURSLENBQ3FCOztBQUV6QyxVQUFNQyxNQUFNLEdBQUdGLGlDQUFnQkMsR0FBaEIsR0FBc0JFLFNBQXRCLEVBQWY7O0FBQ0EsVUFBTUMsV0FBVyxHQUFHLEtBQUtuQyxLQUFMLENBQVdkLElBQVgsQ0FBZ0JlLFlBQWhCLENBQTZCbUMsZ0JBQTdCLENBQThDLENBQUNILE1BQUQsQ0FBOUMsRUFBd0QsQ0FBeEQsQ0FBcEI7O0FBQ0EsUUFBSSxDQUFDRSxXQUFMLEVBQWtCO0FBQ2QsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsV0FBT0EsV0FBVyxDQUFDRSxJQUFuQjtBQUNILEdBM0QyQjs7QUE2RDVCaEIsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsVUFBTWlCLFVBQVUsR0FBRyxLQUFLUixxQkFBTCxFQUFuQjs7QUFDQSxRQUFJLENBQUNRLFVBQUwsRUFBaUI7QUFDYixhQUFPLEVBQVA7QUFDSDs7QUFDRCxXQUFPQSxVQUFVLENBQUNDLHVCQUFsQjtBQUNILEdBbkUyQjs7QUFxRTVCQyxFQUFBQSxpQkFBaUIsRUFBRSxVQUFTQyxFQUFULEVBQWFDLEtBQWIsRUFBb0JDLE1BQXBCLEVBQTRCO0FBQzNDO0FBQ0E7QUFDQSxRQUFJQSxNQUFNLENBQUNDLFVBQVAsS0FBc0IsT0FBMUIsRUFBbUM7QUFDL0I7QUFDSCxLQUwwQyxDQU0zQzs7O0FBQ0EsUUFBSUQsTUFBTSxDQUFDN0IsTUFBUCxLQUFrQixLQUFLZCxLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUF0QyxFQUE4QztBQUMxQztBQUNIOztBQUVELFNBQUsrQixnQkFBTDtBQUNILEdBakYyQjtBQW1GNUJDLEVBQUFBLHlCQUF5QixFQUFFLFVBQVNDLE1BQVQsRUFBaUJDLFlBQWpCLEVBQStCO0FBQ3RELFFBQUksQ0FBQyxLQUFLaEQsS0FBTCxDQUFXZCxJQUFYLENBQWdCK0QsU0FBaEIsQ0FBMEJGLE1BQTFCLENBQUwsRUFBd0M7QUFDcEM7QUFDQTtBQUNIOztBQUNELFNBQUtGLGdCQUFMO0FBQ0gsR0F6RjJCO0FBMkY1QkssRUFBQUEsY0FBYyxFQUFFLFVBQVNULEVBQVQsRUFBYXZELElBQWIsRUFBbUI7QUFDL0IsUUFBSSxDQUFDQSxJQUFMLEVBQVc7QUFDWCxRQUFJQSxJQUFJLENBQUM0QixNQUFMLElBQWUsS0FBS2QsS0FBTCxDQUFXZCxJQUFYLENBQWdCNEIsTUFBbkMsRUFBMkM7QUFDM0MsUUFBSTJCLEVBQUUsQ0FBQ1UsT0FBSCxPQUFpQixtQkFBckIsRUFBMEM7O0FBQzFDcEIscUNBQWdCQyxHQUFoQixHQUFzQm9CLGNBQXRCLENBQXFDLGVBQXJDLEVBQXNELEtBQUtGLGNBQTNEOztBQUNBLFNBQUtHLDBCQUFMO0FBQ0gsR0FqRzJCO0FBbUc1QkEsRUFBQUEsMEJBQTBCLEVBQUUsWUFBVztBQUNuQyxVQUFNQyxHQUFHLEdBQUd2QixpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0FzQixJQUFBQSxHQUFHLENBQUNDLEVBQUosQ0FBTyxtQkFBUCxFQUE0QixLQUFLZixpQkFBakM7QUFDQWMsSUFBQUEsR0FBRyxDQUFDQyxFQUFKLENBQU8sd0JBQVAsRUFBaUMsS0FBS1QseUJBQXRDOztBQUVBLFNBQUtELGdCQUFMO0FBQ0gsR0F6RzJCO0FBMkc1QkEsRUFBQUEsZ0JBQWdCLEVBQUUsa0JBQWlCO0FBQy9CLFVBQU1TLEdBQUcsR0FBR3ZCLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxRQUFJLENBQUNzQixHQUFHLENBQUNFLGVBQUosQ0FBb0IsS0FBS3hELEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjRCLE1BQXBDLENBQUwsRUFBa0Q7QUFDOUM7QUFDSDs7QUFDRCxRQUFJLENBQUNVLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBTCxFQUE4RDtBQUMxRDtBQUNIO0FBRUQ7OztBQUNBLFNBQUtnQyxRQUFMLENBQWM7QUFDVm5DLE1BQUFBLFNBQVMsRUFBRSxNQUFNLHNDQUFvQmdDLEdBQXBCLEVBQXlCLEtBQUt0RCxLQUFMLENBQVdkLElBQXBDO0FBRFAsS0FBZDtBQUdILEdBeEgyQjtBQTBINUJ3RSxFQUFBQSxVQUFVLEVBQUUsVUFBU3hFLElBQVQsRUFBZTtBQUN2QixRQUFJQSxJQUFJLEtBQUssS0FBS2MsS0FBTCxDQUFXZCxJQUF4QixFQUE4QjtBQUM5QixTQUFLdUUsUUFBTCxDQUFjO0FBQ1ZoRCxNQUFBQSxRQUFRLEVBQUUsS0FBS1QsS0FBTCxDQUFXZCxJQUFYLENBQWdCd0I7QUFEaEIsS0FBZDtBQUdILEdBL0gyQjtBQWlJNUJpRCxFQUFBQSxVQUFVLEVBQUUsVUFBU2xCLEVBQVQsRUFBYTtBQUNyQixRQUFJQSxFQUFFLENBQUNVLE9BQUgsT0FBaUIsbUJBQXJCLEVBQTBDO0FBQzFDLFFBQUlWLEVBQUUsQ0FBQ3RCLFNBQUgsT0FBbUIsS0FBS25CLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjRCLE1BQXZDLEVBQStDO0FBQy9DLFNBQUsyQyxRQUFMLENBQWM7QUFBRXRELE1BQUFBLFFBQVEsRUFBRXNDLEVBQUUsQ0FBQ3JDLFVBQUgsR0FBZ0JDO0FBQTVCLEtBQWQ7QUFDSCxHQXJJMkI7QUF1STVCdUQsRUFBQUEsYUFBYSxFQUFFLFVBQVNDLGdCQUFULEVBQTJCO0FBQ3RDLFFBQUlBLGdCQUFnQixDQUFDVixPQUFqQixPQUErQixjQUFuQyxFQUFtRDtBQUMvQyxXQUFLTSxRQUFMLENBQWM7QUFDVjlDLFFBQUFBLFVBQVUsRUFBRUMsVUFBVSxDQUFDQyxrQkFBWCxDQUE4QixLQUFLYixLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUE5QztBQURGLE9BQWQ7QUFHSDtBQUNKLEdBN0kyQjtBQStJNUJnRCxFQUFBQSxRQUFRLEVBQUUsVUFBU0MsT0FBVCxFQUFrQjtBQUN4QixZQUFRQSxPQUFPLENBQUNDLE1BQWhCO0FBQ0k7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFLLGNBQUw7QUFDSSxZQUFJRCxPQUFPLENBQUNqRCxNQUFSLEtBQW1CLEtBQUtkLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjRCLE1BQXZDLEVBQStDO0FBQy9DLGFBQUsyQyxRQUFMLENBQWM7QUFDVjFDLFVBQUFBLGlCQUFpQixFQUFFLEtBQUtmLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQjhCLDBCQUFoQjtBQURULFNBQWQ7QUFHQTtBQUNKO0FBQ0E7QUFDQTs7QUFDQSxXQUFLLCtCQUFMO0FBQ0ksYUFBS2lELFdBQUw7QUFDQTs7QUFFSixXQUFLLFdBQUw7QUFDSTtBQUNBLFlBQUlGLE9BQU8sQ0FBQ0csT0FBUixLQUFvQixLQUFLbEUsS0FBTCxDQUFXZCxJQUFYLENBQWdCNEIsTUFBcEMsSUFBOENpRCxPQUFPLENBQUNJLGNBQTFELEVBQTBFO0FBQ3RFLGVBQUtDLGVBQUw7QUFDSDs7QUFDRDtBQXZCUjtBQXlCSCxHQXpLMkI7QUEySzVCQSxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixRQUFJLENBQUMsS0FBS0MsU0FBTCxDQUFlQyxPQUFwQixFQUE2Qjs7QUFDN0IsU0FBS0QsU0FBTCxDQUFlQyxPQUFmLENBQXVCQyxjQUF2QixDQUFzQztBQUNsQ0MsTUFBQUEsS0FBSyxFQUFFLFNBRDJCO0FBRWxDQyxNQUFBQSxRQUFRLEVBQUU7QUFGd0IsS0FBdEM7QUFJSCxHQWpMMkI7QUFtTDVCQyxFQUFBQSxtQkFBbUIsRUFBRSxZQUFXO0FBQzVCLFNBQUtqQixRQUFMLENBQWM7QUFDVnhDLE1BQUFBLFFBQVEsRUFBRSxLQUFLakIsS0FBTCxDQUFXZCxJQUFYLENBQWdCNEIsTUFBaEIsS0FBMkJJLHVCQUFjQyxTQUFkO0FBRDNCLEtBQWQ7QUFHSCxHQXZMMkI7QUF5TDVCO0FBQ0F3RCxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUtOLFNBQUwsR0FBaUIsdUJBQWpCO0FBQ0gsR0E1TDJCO0FBOEw1Qk8sRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQjs7QUFFQSxTQUFLL0IsZ0JBQUwsR0FBd0IsOEJBQWtCLEtBQUtBLGdCQUF2QixFQUF5QyxHQUF6QyxDQUF4Qjs7QUFFQSxVQUFNUyxHQUFHLEdBQUd2QixpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0FzQixJQUFBQSxHQUFHLENBQUNDLEVBQUosQ0FBTyxhQUFQLEVBQXNCLEtBQUtLLGFBQTNCO0FBQ0FOLElBQUFBLEdBQUcsQ0FBQ0MsRUFBSixDQUFPLFdBQVAsRUFBb0IsS0FBS0csVUFBekI7QUFDQUosSUFBQUEsR0FBRyxDQUFDQyxFQUFKLENBQU8sa0JBQVAsRUFBMkIsS0FBS0ksVUFBaEM7O0FBQ0EsUUFBSUwsR0FBRyxDQUFDRSxlQUFKLENBQW9CLEtBQUt4RCxLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUFwQyxDQUFKLEVBQWlEO0FBQzdDLFdBQUt1QywwQkFBTDtBQUNILEtBRkQsTUFFTztBQUNIQyxNQUFBQSxHQUFHLENBQUNDLEVBQUosQ0FBTyxlQUFQLEVBQXdCLEtBQUtMLGNBQTdCO0FBQ0g7O0FBQ0QyQixnQ0FBbUJDLFdBQW5CLENBQStCLEtBQUs5RSxLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUEvQyxFQUF1RCxLQUFLNEQsbUJBQTVEOztBQUNBLFNBQUtLLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS25CLFFBQWxCLENBQXJCOztBQUVBLFFBQUksS0FBS3ZDLHdCQUFMLEVBQUosRUFBcUM7QUFDakMsWUFBTWUsVUFBVSxHQUFHLEtBQUtSLHFCQUFMLEVBQW5COztBQUNBLFVBQUlRLFVBQUosRUFBZ0I7QUFDWkEsUUFBQUEsVUFBVSxDQUFDaUIsRUFBWCxDQUFjLDhCQUFkLEVBQThDLEtBQUsyQix5QkFBbkQ7QUFDSDtBQUNKLEtBdEJ5QixDQXdCMUI7OztBQUNBLFFBQUksS0FBS3hDLEtBQUwsQ0FBV3pCLFFBQWYsRUFBeUI7QUFDckIsV0FBS21ELGVBQUw7QUFDSDtBQUNKLEdBMU4yQjtBQTRONUJlLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0IsVUFBTTdCLEdBQUcsR0FBR3ZCLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxRQUFJc0IsR0FBSixFQUFTO0FBQ0x2Qix1Q0FBZ0JDLEdBQWhCLEdBQXNCb0IsY0FBdEIsQ0FBcUMsYUFBckMsRUFBb0QsS0FBS1EsYUFBekQ7O0FBQ0E3Qix1Q0FBZ0JDLEdBQWhCLEdBQXNCb0IsY0FBdEIsQ0FBcUMsV0FBckMsRUFBa0QsS0FBS00sVUFBdkQ7O0FBQ0FKLE1BQUFBLEdBQUcsQ0FBQ0YsY0FBSixDQUFtQixrQkFBbkIsRUFBdUMsS0FBS08sVUFBNUM7QUFDQUwsTUFBQUEsR0FBRyxDQUFDRixjQUFKLENBQW1CLG1CQUFuQixFQUF3QyxLQUFLWixpQkFBN0M7QUFDQWMsTUFBQUEsR0FBRyxDQUFDRixjQUFKLENBQW1CLHdCQUFuQixFQUE2QyxLQUFLTix5QkFBbEQ7QUFDQVEsTUFBQUEsR0FBRyxDQUFDRixjQUFKLENBQW1CLGVBQW5CLEVBQW9DLEtBQUtGLGNBQXpDO0FBQ0g7O0FBQ0QyQixnQ0FBbUJ6QixjQUFuQixDQUFrQyxLQUFLcEQsS0FBTCxDQUFXZCxJQUFYLENBQWdCNEIsTUFBbEQsRUFBMEQsS0FBSzRELG1CQUEvRDs7QUFDQU0sd0JBQUlJLFVBQUosQ0FBZSxLQUFLTCxhQUFwQjs7QUFFQSxRQUFJLEtBQUt4RCx3QkFBTCxFQUFKLEVBQXFDO0FBQ2pDLFlBQU1lLFVBQVUsR0FBRyxLQUFLUixxQkFBTCxFQUFuQjs7QUFDQSxVQUFJUSxVQUFKLEVBQWdCO0FBQ1pBLFFBQUFBLFVBQVUsQ0FBQ2MsY0FBWCxDQUNJLDhCQURKLEVBRUksS0FBSzhCLHlCQUZUO0FBSUg7QUFDSjtBQUNKLEdBbFAyQjtBQW9QNUI7QUFDQUcsRUFBQUEsZ0NBQWdDLEVBQUUsVUFBU3JGLEtBQVQsRUFBZ0I7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsU0FBS3lELFFBQUwsQ0FBYztBQUNWMUMsTUFBQUEsaUJBQWlCLEVBQUUsS0FBS2YsS0FBTCxDQUFXZCxJQUFYLENBQWdCOEIsMEJBQWhCO0FBRFQsS0FBZDtBQUdILEdBNVAyQjtBQThQNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FzRSxFQUFBQSxxQkFBcUIsRUFBRSxVQUFTQyxRQUFULEVBQW1CQyxRQUFuQixFQUE2QjtBQUNoRCxRQUFJQyxNQUFNLENBQUNDLElBQVAsQ0FBWUgsUUFBWixFQUFzQkksSUFBdEIsQ0FBNEJDLENBQUQsSUFBT0wsUUFBUSxDQUFDSyxDQUFELENBQVIsS0FBZ0IsS0FBSzVGLEtBQUwsQ0FBVzRGLENBQVgsQ0FBbEQsQ0FBSixFQUFzRTtBQUNsRSxhQUFPLElBQVA7QUFDSDs7QUFDRCxRQUFJSCxNQUFNLENBQUNDLElBQVAsQ0FBWUYsUUFBWixFQUFzQkcsSUFBdEIsQ0FBNEJDLENBQUQsSUFBT0osUUFBUSxDQUFDSSxDQUFELENBQVIsS0FBZ0IsS0FBS2xELEtBQUwsQ0FBV2tELENBQVgsQ0FBbEQsQ0FBSixFQUFzRTtBQUNsRSxhQUFPLElBQVA7QUFDSDs7QUFDRCxXQUFPLEtBQVA7QUFDSCxHQTVRMkI7O0FBOFE1QlYsRUFBQUEseUJBQXlCLEdBQUc7QUFDeEI7QUFDQSxTQUFLekIsUUFBTCxDQUFjO0FBQ1ZyQyxNQUFBQSxhQUFhLEVBQUUsS0FBS0MsaUJBQUw7QUFETCxLQUFkO0FBR0gsR0FuUjJCOztBQXFSNUJ0QyxFQUFBQSxPQUFPLEVBQUUsVUFBUzBELEVBQVQsRUFBYTtBQUNsQixRQUFJLEtBQUt6QyxLQUFMLENBQVdqQixPQUFmLEVBQXdCO0FBQ3BCLFdBQUtpQixLQUFMLENBQVdqQixPQUFYLENBQW1CLEtBQUtpQixLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUFuQyxFQUEyQzJCLEVBQTNDO0FBQ0g7QUFDSixHQXpSMkI7QUEyUjVCb0QsRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckIsU0FBS3BDLFFBQUwsQ0FBZTtBQUFFbkQsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBZjtBQUNBLFNBQUt3RixpQkFBTDtBQUNILEdBOVIyQjtBQWdTNUJDLEVBQUFBLFlBQVksRUFBRSxZQUFXO0FBQ3JCLFNBQUt0QyxRQUFMLENBQWU7QUFBRW5ELE1BQUFBLEtBQUssRUFBRTtBQUFULEtBQWY7QUFDQSxTQUFLMEYsaUJBQUw7QUFDSCxHQW5TMkI7QUFxUzVCRixFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCO0FBQ0E7QUFDQSxRQUFJLENBQUMvRCxpQ0FBZ0JDLEdBQWhCLEdBQXNCaUUsT0FBdEIsRUFBRCxJQUFvQyxDQUFDLEtBQUt2RCxLQUFMLENBQVduQyxVQUFwRCxFQUFnRTtBQUM1RCxXQUFLa0QsUUFBTCxDQUFlO0FBQUVsRCxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQUFmO0FBQ0g7QUFDSixHQTNTMkI7QUE2UzVCeUYsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLdkMsUUFBTCxDQUFlO0FBQUVsRCxNQUFBQSxVQUFVLEVBQUU7QUFBZCxLQUFmO0FBQ0gsR0EvUzJCO0FBaVQ1QjJGLEVBQUFBLGdCQUFnQixFQUFFLFVBQVNDLGtCQUFULEVBQTZCO0FBQzNDO0FBQ0EsUUFBSXBFLGlDQUFnQkMsR0FBaEIsR0FBc0JpRSxPQUF0QixFQUFKLEVBQXFDO0FBRXJDLFVBQU12RCxLQUFLLEdBQUc7QUFDVmxDLE1BQUFBLG1CQUFtQixFQUFFMkY7QUFEWCxLQUFkLENBSjJDLENBUTNDOztBQUNBLFFBQUksS0FBS25HLEtBQUwsQ0FBV1gsU0FBZixFQUEwQjtBQUN0QnFELE1BQUFBLEtBQUssQ0FBQ3BDLEtBQU4sR0FBYyxLQUFkO0FBQ0g7O0FBRUQsU0FBS21ELFFBQUwsQ0FBY2YsS0FBZDtBQUNILEdBL1QyQjtBQWlVNUIwRCxFQUFBQSx3QkFBd0IsRUFBRSxVQUFTQyxDQUFULEVBQVk7QUFDbEM7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDQyxlQUFGO0FBQ0FELElBQUFBLENBQUMsQ0FBQ0UsY0FBRjs7QUFFQSxTQUFLTCxnQkFBTCxDQUFzQkcsQ0FBQyxDQUFDRyxNQUFGLENBQVNDLHFCQUFULEVBQXRCO0FBQ0gsR0F2VTJCO0FBeVU1QkMsRUFBQUEsYUFBYSxFQUFFLFVBQVNMLENBQVQsRUFBWTtBQUN2QjtBQUNBQSxJQUFBQSxDQUFDLENBQUNFLGNBQUY7O0FBRUEsU0FBS0wsZ0JBQUwsQ0FBc0I7QUFDbEJTLE1BQUFBLEtBQUssRUFBRU4sQ0FBQyxDQUFDTyxPQURTO0FBRWxCQyxNQUFBQSxHQUFHLEVBQUVSLENBQUMsQ0FBQ1MsT0FGVztBQUdsQkMsTUFBQUEsTUFBTSxFQUFFO0FBSFUsS0FBdEI7QUFLSCxHQWxWMkI7QUFvVjVCQyxFQUFBQSxTQUFTLEVBQUUsWUFBVztBQUNsQixTQUFLdkQsUUFBTCxDQUFjO0FBQ1ZqRCxNQUFBQSxtQkFBbUIsRUFBRTtBQURYLEtBQWQ7QUFHQSxTQUFLUixLQUFMLENBQVdpSCxjQUFYO0FBQ0gsR0F6VjJCO0FBMlY1QkMsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNeEgsUUFBUSxHQUFHLEtBQUtNLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQndDLGVBQWhCLE9BQXNDLFFBQXZEO0FBQ0EsVUFBTVgsaUJBQWlCLEdBQUcsS0FBS2YsS0FBTCxDQUFXZSxpQkFBckMsQ0FGZSxDQUdmOztBQUVBLFVBQU1vRyxXQUFXLEdBQUdwRyxpQkFBaUIsR0FBRyxDQUFwQixJQUF5QkgsVUFBVSxDQUFDd0csb0JBQVgsQ0FBZ0MsS0FBSzFFLEtBQUwsQ0FBVy9CLFVBQTNDLENBQTdDO0FBQ0EsVUFBTTBHLGFBQWEsR0FBRyxLQUFLckgsS0FBTCxDQUFXUixTQUFYLElBQXdCb0IsVUFBVSxDQUFDMEcsc0JBQVgsQ0FBa0MsS0FBSzVFLEtBQUwsQ0FBVy9CLFVBQTdDLENBQTlDO0FBQ0EsVUFBTTRHLE1BQU0sR0FBR0osV0FBVyxJQUFJRSxhQUE5QjtBQUVBLFFBQUlHLE9BQU8sR0FBRyxJQUFkOztBQUNBLFFBQUksS0FBS2pHLHdCQUFMLEVBQUosRUFBcUM7QUFDakNpRyxNQUFBQSxPQUFPLEdBQUcsS0FBSzlFLEtBQUwsQ0FBV3RCLGFBQXJCO0FBQ0g7O0FBRUQsVUFBTXFHLGVBQWUsR0FBR0MsT0FBTyxDQUFDLEtBQUtoRixLQUFMLENBQVdsQyxtQkFBWixDQUEvQjs7QUFFQSxVQUFNbUgsUUFBUSxHQUFHQyxtQkFBVUMsTUFBVixHQUFtQkMsa0JBQW5CLENBQXNDLEtBQUs5SCxLQUFMLENBQVdkLElBQVgsQ0FBZ0I0QixNQUF0RCxDQUFqQjs7QUFFQSxVQUFNaUgsT0FBTyxHQUFHLHlCQUFXO0FBQ3ZCLHFCQUFlLElBRFE7QUFFdkIsOEJBQXdCLEtBQUtyRixLQUFMLENBQVd6QixRQUZaO0FBR3ZCLDRCQUFzQixLQUFLakIsS0FBTCxDQUFXVCxNQUhWO0FBSXZCLGtDQUE0QjRILFdBSkw7QUFLdkIsK0JBQXlCRSxhQUxGO0FBTXZCLDZCQUF1QjNILFFBTkE7QUFPdkIsbUNBQTZCK0gsZUFQTjtBQVF2Qiw4QkFBd0IsQ0FBQ0YsTUFSRjtBQVN2QixpQ0FBMkIsS0FBS3ZILEtBQUwsQ0FBV1AsV0FUZjtBQVV2QixnQ0FBMEIrSCxPQUFPLElBQUksQ0FBQyxLQUFLeEgsS0FBTCxDQUFXWDtBQVYxQixLQUFYLENBQWhCO0FBYUEsVUFBTTJJLGFBQWEsR0FBRyx5QkFBVztBQUM3Qiw0QkFBc0I7QUFETyxLQUFYLENBQXRCO0FBSUEsVUFBTUMsWUFBWSxHQUFHLHlCQUFXO0FBQzVCLDJCQUFxQixJQURPO0FBRTVCLGlDQUEyQixLQUFLdkYsS0FBTCxDQUFXbkMsVUFBWCxJQUF5QmtIO0FBRnhCLEtBQVgsQ0FBckI7QUFLQSxRQUFJL0csSUFBSSxHQUFHLEtBQUtnQyxLQUFMLENBQVdqQyxRQUF0QjtBQUNBLFFBQUlDLElBQUksSUFBSXdILFNBQVIsSUFBcUJ4SCxJQUFJLElBQUksSUFBakMsRUFBdUNBLElBQUksR0FBRyxFQUFQO0FBQ3ZDQSxJQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ3lILE9BQUwsQ0FBYSxHQUFiLEVBQWtCLFNBQWxCLENBQVAsQ0ExQ2UsQ0EwQ3NCOztBQUdyQyxRQUFJQyxLQUFKOztBQUNBLFFBQUliLE1BQUosRUFBWTtBQUNSLFlBQU1jLFlBQVksR0FBR0MsZUFBZSxDQUFDQyxXQUFoQixDQUE0QnhILGlCQUE1QixDQUFyQjtBQUNBLFlBQU15SCxZQUFZLEdBQUd6SCxpQkFBaUIsR0FBR3NILFlBQUgsR0FBa0IsR0FBeEQ7QUFDQUQsTUFBQUEsS0FBSyxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUVIO0FBQWhCLFNBQWdDTyxZQUFoQyxDQUFSO0FBQ0g7O0FBRUQsUUFBSUMsS0FBSjtBQUNBLFFBQUlDLFlBQUo7QUFDQSxRQUFJQyxPQUFKOztBQUNBLFFBQUksQ0FBQyxLQUFLM0ksS0FBTCxDQUFXWCxTQUFoQixFQUEyQjtBQUN2QixZQUFNdUosV0FBVyxHQUFHLHlCQUFXO0FBQzNCLDRCQUFvQixJQURPO0FBRTNCLDhCQUFzQixLQUFLNUksS0FBTCxDQUFXTixRQUZOO0FBRzNCLGtDQUEwQjZILE1BQU0sSUFBSSxLQUFLN0UsS0FBTCxDQUFXbkMsVUFBckIsSUFBbUNrSDtBQUhsQyxPQUFYLENBQXBCO0FBTUFpQixNQUFBQSxZQUFZLEdBQUdsQixPQUFPLEdBQUc7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUF3Q0EsT0FBeEMsQ0FBSCxHQUE4RCxJQUFwRixDQVB1QixDQVF2Qjs7QUFDQWlCLE1BQUFBLEtBQUssR0FBRztBQUFLLFFBQUEsS0FBSyxFQUFFL0gsSUFBWjtBQUFrQixRQUFBLFNBQVMsRUFBRWtJLFdBQTdCO0FBQTBDLFFBQUEsUUFBUSxFQUFFLENBQUMsQ0FBckQ7QUFBd0QsUUFBQSxHQUFHLEVBQUM7QUFBNUQsU0FBcUVsSSxJQUFyRSxDQUFSO0FBQ0gsS0FWRCxNQVVPLElBQUksS0FBS2dDLEtBQUwsQ0FBV3BDLEtBQWYsRUFBc0I7QUFDekIsWUFBTXVJLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBSixNQUFBQSxPQUFPLEdBQUcsNkJBQUMsT0FBRDtBQUFTLFFBQUEsU0FBUyxFQUFDLHFCQUFuQjtBQUF5QyxRQUFBLEtBQUssRUFBRSxLQUFLM0ksS0FBTCxDQUFXZCxJQUFYLENBQWdCd0IsSUFBaEU7QUFBc0UsUUFBQSxHQUFHLEVBQUM7QUFBMUUsUUFBVjtBQUNILEtBcEVjLENBc0VmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLFVBQU1zSSxnQkFBZ0IsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUVBLFFBQUlFLGlCQUFKOztBQUNBLFFBQUksQ0FBQ2xILGlDQUFnQkMsR0FBaEIsR0FBc0JpRSxPQUF0QixFQUFMLEVBQXNDO0FBQ2xDZ0QsTUFBQUEsaUJBQWlCLEdBQ2IsNkJBQUMsOEJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyx3QkFEZDtBQUVJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLFNBQUgsQ0FGWDtBQUdJLFFBQUEsVUFBVSxFQUFFeEIsZUFIaEI7QUFJSSxRQUFBLE9BQU8sRUFBRSxLQUFLckI7QUFKbEIsUUFESjtBQU9IOztBQUVELFVBQU04QyxVQUFVLEdBQUdKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7QUFFQSxRQUFJSSxTQUFTLEdBQUd6SSxJQUFoQjtBQUVBLFFBQUkwSSxXQUFKO0FBQ0EsUUFBSUMsUUFBSjtBQUNBOzs7QUFFQSxRQUFJMUIsUUFBUSxJQUFJLENBQUNuRyx1QkFBY0MsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQWpCLEVBQTBFO0FBQ3RFMkgsTUFBQUEsV0FBVyxHQUFHO0FBQ1YsUUFBQSxHQUFHLEVBQUVFLE9BQU8sQ0FBQyxxQ0FBRCxDQURGO0FBRVYsUUFBQSxTQUFTLEVBQUMsZ0JBRkE7QUFHVixRQUFBLEtBQUssRUFBQyxJQUhJO0FBSVYsUUFBQSxNQUFNLEVBQUMsSUFKRztBQUtWLFFBQUEsR0FBRyxFQUFDO0FBTE0sUUFBZDtBQU9IOztBQUVELFVBQU07QUFBRXBLLE1BQUFBO0FBQUYsUUFBVyxLQUFLYyxLQUF0QjtBQUNBLFVBQU0yQyxNQUFNLEdBQUd6RCxJQUFJLENBQUMrRCxTQUFMLENBQWUwRSxRQUFmLENBQWY7O0FBQ0EsUUFDSWhGLE1BQU0sSUFBSUEsTUFBTSxDQUFDQyxVQUFQLEtBQXNCLE1BQWhDLElBQTBDMUQsSUFBSSxDQUFDcUssb0JBQUwsT0FBZ0MsQ0FBMUUsSUFDQS9ILHVCQUFjQyxnQkFBZCxDQUErQiwrQkFBL0IsQ0FGSixFQUdFO0FBQ0UsWUFBTStILGFBQWEsR0FBR1YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUF0QjtBQUNBTSxNQUFBQSxRQUFRLEdBQUcsNkJBQUMsYUFBRDtBQUFlLFFBQUEsTUFBTSxFQUFFMUI7QUFBdkIsUUFBWDtBQUNILEtBbkhjLENBcUhmOzs7QUFDQSxRQUFJUixXQUFXLElBQUlFLGFBQWYsSUFBZ0MsQ0FBQzNILFFBQXJDLEVBQStDO0FBQzNDeUosTUFBQUEsU0FBUyxJQUFJLE1BQU0seUJBQUcsK0NBQUgsRUFBb0Q7QUFDbkVNLFFBQUFBLEtBQUssRUFBRTFJO0FBRDRELE9BQXBELENBQW5CO0FBR0gsS0FKRCxNQUlPLElBQUlvRyxXQUFKLEVBQWlCO0FBQ3BCZ0MsTUFBQUEsU0FBUyxJQUFJLE1BQU0seUJBQUcsNEJBQUgsRUFBaUM7QUFBRU0sUUFBQUEsS0FBSyxFQUFFMUk7QUFBVCxPQUFqQyxDQUFuQjtBQUNILEtBRk0sTUFFQSxJQUFJc0csYUFBYSxJQUFJLENBQUMzSCxRQUF0QixFQUFnQztBQUNuQ3lKLE1BQUFBLFNBQVMsSUFBSSxNQUFNLHlCQUFHLGtCQUFILENBQW5CO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBS25KLEtBQUwsQ0FBV1QsTUFBZixFQUF1QjtBQUMxQjRKLE1BQUFBLFNBQVMsSUFBSSxNQUFNLHlCQUFHLGtCQUFILENBQW5CO0FBQ0g7O0FBRUQsUUFBSU8sV0FBSjs7QUFDQSxRQUFJakMsZUFBSixFQUFxQjtBQUNqQixZQUFNa0MsbUJBQW1CLEdBQUdiLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixtQ0FBakIsQ0FBNUI7QUFDQVcsTUFBQUEsV0FBVyxHQUNQLDZCQUFDLHdCQUFELDZCQUFpQiw0QkFBVSxLQUFLaEgsS0FBTCxDQUFXbEMsbUJBQXJCLENBQWpCO0FBQTRELFFBQUEsVUFBVSxFQUFFLEtBQUt3RztBQUE3RSxVQUNJLDZCQUFDLG1CQUFEO0FBQXFCLFFBQUEsSUFBSSxFQUFFLEtBQUtoSCxLQUFMLENBQVdkLElBQXRDO0FBQTRDLFFBQUEsVUFBVSxFQUFFLEtBQUs4SDtBQUE3RCxRQURKLENBREo7QUFLSDs7QUFFRCxRQUFJNEMsV0FBVyxHQUFHLElBQWxCOztBQUNBLFFBQUlwSSx1QkFBY0MsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQUosRUFBNkQ7QUFDekQsVUFBSSxLQUFLaUIsS0FBTCxDQUFXdkMsUUFBWCxJQUF1QixRQUF2QixJQUFtQyxDQUFDd0gsUUFBeEMsRUFBa0Q7QUFDOUNpQyxRQUFBQSxXQUFXLEdBQUcsNkJBQUMsdUJBQUQ7QUFBZ0IsVUFBQSxjQUFjLEVBQUUsS0FBSzVKLEtBQUwsQ0FBV1g7QUFBM0MsVUFBZDtBQUNIO0FBQ0o7O0FBRUQsUUFBSXdLLE9BQU8sR0FBRyxJQUFkOztBQUNBLFFBQUksS0FBS25ILEtBQUwsQ0FBV3BCLFNBQWYsRUFBMEI7QUFDdEJ1SSxNQUFBQSxPQUFPLEdBQUcsNkJBQUMsZ0JBQUQ7QUFBUyxRQUFBLE1BQU0sRUFBRSxLQUFLbkgsS0FBTCxDQUFXcEIsU0FBNUI7QUFBdUMsUUFBQSxTQUFTLEVBQUM7QUFBakQsUUFBVjtBQUNIOztBQUVELFdBQU8sNkJBQUMsY0FBRCxDQUFPLFFBQVAsUUFDSCw2QkFBQyxxQ0FBRDtBQUF1QixNQUFBLFFBQVEsRUFBRSxLQUFLK0M7QUFBdEMsT0FDSyxDQUFDO0FBQUN5RixNQUFBQSxPQUFEO0FBQVVDLE1BQUFBLFFBQVY7QUFBb0JDLE1BQUFBO0FBQXBCLEtBQUQsS0FDRyw2QkFBQyxnQkFBRDtBQUNJLE1BQUEsT0FBTyxFQUFFRixPQURiO0FBRUksTUFBQSxRQUFRLEVBQUVDLFFBQVEsR0FBRyxDQUFILEdBQU8sQ0FBQyxDQUY5QjtBQUdJLE1BQUEsUUFBUSxFQUFFQyxHQUhkO0FBSUksTUFBQSxTQUFTLEVBQUVqQyxPQUpmO0FBS0ksTUFBQSxPQUFPLEVBQUUsS0FBS2hKLE9BTGxCO0FBTUksTUFBQSxZQUFZLEVBQUUsS0FBSzhHLFlBTnZCO0FBT0ksTUFBQSxZQUFZLEVBQUUsS0FBS0UsWUFQdkI7QUFRSSxNQUFBLGFBQWEsRUFBRSxLQUFLVyxhQVJ4QjtBQVNJLG9CQUFZeUMsU0FUaEI7QUFVSSx1QkFBZSxLQUFLekcsS0FBTCxDQUFXekIsUUFWOUI7QUFXSSxNQUFBLElBQUksRUFBQztBQVhULE9BYUk7QUFBSyxNQUFBLFNBQVMsRUFBRStHO0FBQWhCLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsSUFBSSxFQUFFLEtBQUtoSSxLQUFMLENBQVdkLElBQTdCO0FBQW1DLE1BQUEsS0FBSyxFQUFFLEVBQTFDO0FBQThDLE1BQUEsTUFBTSxFQUFFO0FBQXRELE1BREosRUFFTWtLLFdBRk4sRUFHTVMsT0FITixDQURKLENBYkosRUFvQk1ELFdBcEJOLEVBcUJJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNbkIsS0FETixFQUVNQyxZQUZOLENBREosRUFLTVcsUUFMTixFQU1NSixpQkFOTixFQU9NYixLQVBOLENBckJKLEVBK0JNTyxPQS9CTixDQUZSLENBREcsRUF1Q0RlLFdBdkNDLENBQVA7QUF5Q0g7QUE1aEIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTggTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IERNUm9vbU1hcCBmcm9tICcuLi8uLi8uLi91dGlscy9ETVJvb21NYXAnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge0NvbnRleHRNZW51LCBDb250ZXh0TWVudUJ1dHRvbiwgdG9SaWdodE9mfSBmcm9tICcuLi8uLi9zdHJ1Y3R1cmVzL0NvbnRleHRNZW51JztcclxuaW1wb3J0ICogYXMgUm9vbU5vdGlmcyBmcm9tICcuLi8uLi8uLi9Sb29tTm90aWZzJztcclxuaW1wb3J0ICogYXMgRm9ybWF0dGluZ1V0aWxzIGZyb20gJy4uLy4uLy4uL3V0aWxzL0Zvcm1hdHRpbmdVdGlscyc7XHJcbmltcG9ydCBBY3RpdmVSb29tT2JzZXJ2ZXIgZnJvbSAnLi4vLi4vLi4vQWN0aXZlUm9vbU9ic2VydmVyJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi4vLi4vLi4vc3RvcmVzL1Jvb21WaWV3U3RvcmUnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcbmltcG9ydCB7Um92aW5nVGFiSW5kZXhXcmFwcGVyfSBmcm9tIFwiLi4vLi4vLi4vYWNjZXNzaWJpbGl0eS9Sb3ZpbmdUYWJJbmRleFwiO1xyXG5pbXBvcnQgRTJFSWNvbiBmcm9tICcuL0UyRUljb24nO1xyXG5pbXBvcnQgSW52aXRlT25seUljb24gZnJvbSAnLi9JbnZpdGVPbmx5SWNvbic7XHJcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBjYW1lbGNhc2VcclxuaW1wb3J0IHJhdGVfbGltaXRlZF9mdW5jIGZyb20gJy4uLy4uLy4uL3JhdGVsaW1pdGVkZnVuYyc7XHJcbmltcG9ydCB7IHNoaWVsZFN0YXR1c0ZvclJvb20gfSBmcm9tICcuLi8uLi8uLi91dGlscy9TaGllbGRVdGlscyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnUm9vbVRpbGUnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICByb29tOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgY29sbGFwc2VkOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHVucmVhZDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcclxuICAgICAgICBoaWdobGlnaHQ6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLy8gSWYgdHJ1ZSwgYXBwbHkgbXhfUm9vbVRpbGVfdHJhbnNwYXJlbnQgY2xhc3NcclxuICAgICAgICB0cmFuc3BhcmVudDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgaXNJbnZpdGU6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgaW5jb21pbmdDYWxsOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzRHJhZ2dpbmc6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgam9pblJ1bGVzID0gdGhpcy5wcm9wcy5yb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5qb2luX3J1bGVzXCIsIFwiXCIpO1xyXG4gICAgICAgIGNvbnN0IGpvaW5SdWxlID0gam9pblJ1bGVzICYmIGpvaW5SdWxlcy5nZXRDb250ZW50KCkuam9pbl9ydWxlO1xyXG5cclxuICAgICAgICByZXR1cm4gKHtcclxuICAgICAgICAgICAgam9pblJ1bGUsXHJcbiAgICAgICAgICAgIGhvdmVyOiBmYWxzZSxcclxuICAgICAgICAgICAgYmFkZ2VIb3ZlcjogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbnRleHRNZW51UG9zaXRpb246IG51bGwsIC8vIERPTSBib3VuZGluZyBib3gsIG51bGwgaWYgbm9uLXNob3duXHJcbiAgICAgICAgICAgIHJvb21OYW1lOiB0aGlzLnByb3BzLnJvb20ubmFtZSxcclxuICAgICAgICAgICAgbm90aWZTdGF0ZTogUm9vbU5vdGlmcy5nZXRSb29tTm90aWZzU3RhdGUodGhpcy5wcm9wcy5yb29tLnJvb21JZCksXHJcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvbkNvdW50OiB0aGlzLnByb3BzLnJvb20uZ2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQoKSxcclxuICAgICAgICAgICAgc2VsZWN0ZWQ6IHRoaXMucHJvcHMucm9vbS5yb29tSWQgPT09IFJvb21WaWV3U3RvcmUuZ2V0Um9vbUlkKCksXHJcbiAgICAgICAgICAgIHN0YXR1c01lc3NhZ2U6IHRoaXMuX2dldFN0YXR1c01lc3NhZ2UoKSxcclxuICAgICAgICAgICAgZTJlU3RhdHVzOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvdWxkU2hvd1N0YXR1c01lc3NhZ2UoKSB7XHJcbiAgICAgICAgaWYgKCFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2N1c3RvbV9zdGF0dXNcIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpc0ludml0ZSA9IHRoaXMucHJvcHMucm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gXCJpbnZpdGVcIjtcclxuICAgICAgICBjb25zdCBpc0pvaW5lZCA9IHRoaXMucHJvcHMucm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gXCJqb2luXCI7XHJcbiAgICAgICAgY29uc3QgbG9va3NMaWtlRG0gPSB0aGlzLnByb3BzLnJvb20uZ2V0SW52aXRlZEFuZEpvaW5lZE1lbWJlckNvdW50KCkgPT09IDI7XHJcbiAgICAgICAgcmV0dXJuICFpc0ludml0ZSAmJiBpc0pvaW5lZCAmJiBsb29rc0xpa2VEbTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFN0YXR1c01lc3NhZ2VVc2VyKCkge1xyXG4gICAgICAgIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpKSByZXR1cm4gbnVsbDsgLy8gV2UndmUgcHJvYmFibHkgYmVlbiBsb2dnZWQgb3V0XHJcblxyXG4gICAgICAgIGNvbnN0IHNlbGZJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKTtcclxuICAgICAgICBjb25zdCBvdGhlck1lbWJlciA9IHRoaXMucHJvcHMucm9vbS5jdXJyZW50U3RhdGUuZ2V0TWVtYmVyc0V4Y2VwdChbc2VsZklkXSlbMF07XHJcbiAgICAgICAgaWYgKCFvdGhlck1lbWJlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG90aGVyTWVtYmVyLnVzZXI7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRTdGF0dXNNZXNzYWdlKCkge1xyXG4gICAgICAgIGNvbnN0IHN0YXR1c1VzZXIgPSB0aGlzLl9nZXRTdGF0dXNNZXNzYWdlVXNlcigpO1xyXG4gICAgICAgIGlmICghc3RhdHVzVXNlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gXCJcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHN0YXR1c1VzZXIuX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2U7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVN0YXRlTWVtYmVyOiBmdW5jdGlvbihldiwgc3RhdGUsIG1lbWJlcikge1xyXG4gICAgICAgIC8vIHdlIG9ubHkgY2FyZSBhYm91dCBsZWF2aW5nIHVzZXJzXHJcbiAgICAgICAgLy8gYmVjYXVzZSB0cnVzdCBzdGF0ZSB3aWxsIGNoYW5nZSBpZiBzb21lb25lIGpvaW5zIGEgbWVnb2xtIHNlc3Npb24gYW55d2F5XHJcbiAgICAgICAgaWYgKG1lbWJlci5tZW1iZXJzaGlwICE9PSBcImxlYXZlXCIpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBpZ25vcmUgbWVtYmVycyBpbiBvdGhlciByb29tc1xyXG4gICAgICAgIGlmIChtZW1iZXIucm9vbUlkICE9PSB0aGlzLnByb3BzLnJvb20ucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUUyZVN0YXR1cygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblVzZXJWZXJpZmljYXRpb25DaGFuZ2VkOiBmdW5jdGlvbih1c2VySWQsIF90cnVzdFN0YXR1cykge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5yb29tLmdldE1lbWJlcih1c2VySWQpKSB7XHJcbiAgICAgICAgICAgIC8vIE5vdCBpbiB0aGlzIHJvb21cclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91cGRhdGVFMmVTdGF0dXMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tVGltZWxpbmU6IGZ1bmN0aW9uKGV2LCByb29tKSB7XHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcbiAgICAgICAgaWYgKHJvb20ucm9vbUlkICE9IHRoaXMucHJvcHMucm9vbS5yb29tSWQpIHJldHVybjtcclxuICAgICAgICBpZiAoZXYuZ2V0VHlwZSgpICE9PSBcIm0ucm9vbS5lbmNyeXB0aW9uXCIpIHJldHVybjtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoXCJSb29tLnRpbWVsaW5lXCIsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgIHRoaXMub25GaW5kaW5nUm9vbVRvQmVFbmNyeXB0ZWQoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25GaW5kaW5nUm9vbVRvQmVFbmNyeXB0ZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjbGkub24oXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyKTtcclxuICAgICAgICBjbGkub24oXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIHRoaXMub25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCk7XHJcblxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUUyZVN0YXR1cygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfdXBkYXRlRTJlU3RhdHVzOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFjbGkuaXNSb29tRW5jcnlwdGVkKHRoaXMucHJvcHMucm9vbS5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyogQXQgdGhpcyBwb2ludCwgdGhlIHVzZXIgaGFzIGVuY3J5cHRpb24gb24gYW5kIGNyb3NzLXNpZ25pbmcgb24gKi9cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZTJlU3RhdHVzOiBhd2FpdCBzaGllbGRTdGF0dXNGb3JSb29tKGNsaSwgdGhpcy5wcm9wcy5yb29tKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tTmFtZTogZnVuY3Rpb24ocm9vbSkge1xyXG4gICAgICAgIGlmIChyb29tICE9PSB0aGlzLnByb3BzLnJvb20pIHJldHVybjtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcm9vbU5hbWU6IHRoaXMucHJvcHMucm9vbS5uYW1lLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkpvaW5SdWxlOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgIT09IFwibS5yb29tLmpvaW5fcnVsZXNcIikgcmV0dXJuO1xyXG4gICAgICAgIGlmIChldi5nZXRSb29tSWQoKSAhPT0gdGhpcy5wcm9wcy5yb29tLnJvb21JZCkgcmV0dXJuO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBqb2luUnVsZTogZXYuZ2V0Q29udGVudCgpLmpvaW5fcnVsZSB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BY2NvdW50RGF0YTogZnVuY3Rpb24oYWNjb3VudERhdGFFdmVudCkge1xyXG4gICAgICAgIGlmIChhY2NvdW50RGF0YUV2ZW50LmdldFR5cGUoKSA9PT0gJ20ucHVzaF9ydWxlcycpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBub3RpZlN0YXRlOiBSb29tTm90aWZzLmdldFJvb21Ob3RpZnNTdGF0ZSh0aGlzLnByb3BzLnJvb20ucm9vbUlkKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbkFjdGlvbjogZnVuY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgLy8gWFhYOiBzbGlnaHQgaGFjayBpbiBvcmRlciB0byB6ZXJvIHRoZSBub3RpZmljYXRpb24gY291bnQgd2hlbiBhIHJvb21cclxuICAgICAgICAgICAgLy8gaXMgcmVhZC4gSWRlYWxseSB0aGlzIHN0YXRlIHdvdWxkIGJlIGdpdmVuIHRvIHRoaXMgdmlhIHByb3BzIChhcyB3ZVxyXG4gICAgICAgICAgICAvLyBkbyB3aXRoIGB1bnJlYWRgKS4gVGhpcyBpcyBzdGlsbCBiZXR0ZXIgdGhhbiBmb3JjZVVwZGF0aW5nIHRoZSBlbnRpcmVcclxuICAgICAgICAgICAgLy8gUm9vbUxpc3Qgd2hlbiBhIHJvb20gaXMgcmVhZC5cclxuICAgICAgICAgICAgY2FzZSAnb25fcm9vbV9yZWFkJzpcclxuICAgICAgICAgICAgICAgIGlmIChwYXlsb2FkLnJvb21JZCAhPT0gdGhpcy5wcm9wcy5yb29tLnJvb21JZCkgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBub3RpZmljYXRpb25Db3VudDogdGhpcy5wcm9wcy5yb29tLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KCksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAvLyBSb29tVGlsZXMgYXJlIG9uZSBvZiB0aGUgZmV3IGNvbXBvbmVudHMgdGhhdCBtYXkgc2hvdyBjdXN0b20gc3RhdHVzIGFuZFxyXG4gICAgICAgICAgICAvLyBhbHNvIHJlbWFpbiBvbiBzY3JlZW4gd2hpbGUgaW4gU2V0dGluZ3MgdG9nZ2xpbmcgdGhlIGZlYXR1cmUuICBUaGlzIGVuc3VyZXNcclxuICAgICAgICAgICAgLy8geW91IGNhbiBjbGVhcmx5IHNlZSB0aGUgc3RhdHVzIGhpZGUgYW5kIHNob3cgd2hlbiB0b2dnbGluZyB0aGUgZmVhdHVyZS5cclxuICAgICAgICAgICAgY2FzZSAnZmVhdHVyZV9jdXN0b21fc3RhdHVzX2NoYW5nZWQnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3Jvb20nOlxyXG4gICAgICAgICAgICAgICAgLy8gd2hlbiB0aGUgcm9vbSBpcyBzZWxlY3RlZCBtYWtlIHN1cmUgaXRzIHRpbGUgaXMgdmlzaWJsZSwgZm9yIGJyZWFkY3J1bWJzL2tleWJvYXJkIHNob3J0Y3V0IGFjY2Vzc1xyXG4gICAgICAgICAgICAgICAgaWYgKHBheWxvYWQucm9vbV9pZCA9PT0gdGhpcy5wcm9wcy5yb29tLnJvb21JZCAmJiBwYXlsb2FkLnNob3dfcm9vbV90aWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2Nyb2xsSW50b1ZpZXcoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3Njcm9sbEludG9WaWV3OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMuX3Jvb21UaWxlLmN1cnJlbnQpIHJldHVybjtcclxuICAgICAgICB0aGlzLl9yb29tVGlsZS5jdXJyZW50LnNjcm9sbEludG9WaWV3KHtcclxuICAgICAgICAgICAgYmxvY2s6IFwibmVhcmVzdFwiLFxyXG4gICAgICAgICAgICBiZWhhdmlvcjogXCJhdXRvXCIsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkFjdGl2ZVJvb21DaGFuZ2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzZWxlY3RlZDogdGhpcy5wcm9wcy5yb29tLnJvb21JZCA9PT0gUm9vbVZpZXdTdG9yZS5nZXRSb29tSWQoKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2UgY29tcG9uZW50IHdpdGggcmVhbCBjbGFzcywgdXNlIGNvbnN0cnVjdG9yIGZvciByZWZzXHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9yb29tVGlsZSA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLyogV2UgYmluZCBoZXJlIHJhdGhlciB0aGFuIGluIHRoZSBkZWZpbml0aW9uIGJlY2F1c2Ugb3RoZXJ3aXNlIHdlIHdpbmQgdXAgd2l0aCB0aGVcclxuICAgICAgICAgICBtZXRob2Qgb25seSBiZWluZyBjYWxsYWJsZSBvbmNlIGV2ZXJ5IDUwMG1zIGFjcm9zcyBhbGwgaW5zdGFuY2VzLCB3aGljaCB3b3VsZCBiZSB3cm9uZyAqL1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUUyZVN0YXR1cyA9IHJhdGVfbGltaXRlZF9mdW5jKHRoaXMuX3VwZGF0ZUUyZVN0YXR1cywgNTAwKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNsaS5vbihcImFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbS5uYW1lXCIsIHRoaXMub25Sb29tTmFtZSk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uSm9pblJ1bGUpO1xyXG4gICAgICAgIGlmIChjbGkuaXNSb29tRW5jcnlwdGVkKHRoaXMucHJvcHMucm9vbS5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub25GaW5kaW5nUm9vbVRvQmVFbmNyeXB0ZWQoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjbGkub24oXCJSb29tLnRpbWVsaW5lXCIsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBBY3RpdmVSb29tT2JzZXJ2ZXIuYWRkTGlzdGVuZXIodGhpcy5wcm9wcy5yb29tLnJvb21JZCwgdGhpcy5fb25BY3RpdmVSb29tQ2hhbmdlKTtcclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5vbkFjdGlvbik7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9zaG91bGRTaG93U3RhdHVzTWVzc2FnZSgpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1c1VzZXIgPSB0aGlzLl9nZXRTdGF0dXNNZXNzYWdlVXNlcigpO1xyXG4gICAgICAgICAgICBpZiAoc3RhdHVzVXNlcikge1xyXG4gICAgICAgICAgICAgICAgc3RhdHVzVXNlci5vbihcIlVzZXIuX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2VcIiwgdGhpcy5fb25TdGF0dXNNZXNzYWdlQ29tbWl0dGVkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gd2hlbiB3ZSdyZSBmaXJzdCByZW5kZXJlZCAob3Igb3VyIHN1Ymxpc3QgaXMgZXhwYW5kZWQpIG1ha2Ugc3VyZSB3ZSBhcmUgdmlzaWJsZSBpZiB3ZSdyZSBhY3RpdmVcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5zZWxlY3RlZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxJbnRvVmlldygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoY2xpKSB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcImFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcIlJvb20ubmFtZVwiLCB0aGlzLm9uUm9vbU5hbWUpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMub25Kb2luUnVsZSk7XHJcbiAgICAgICAgICAgIGNsaS5yZW1vdmVMaXN0ZW5lcihcIlJvb21TdGF0ZS5tZW1iZXJzXCIsIHRoaXMub25Sb29tU3RhdGVNZW1iZXIpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIHRoaXMub25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCk7XHJcbiAgICAgICAgICAgIGNsaS5yZW1vdmVMaXN0ZW5lcihcIlJvb20udGltZWxpbmVcIiwgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIEFjdGl2ZVJvb21PYnNlcnZlci5yZW1vdmVMaXN0ZW5lcih0aGlzLnByb3BzLnJvb20ucm9vbUlkLCB0aGlzLl9vbkFjdGl2ZVJvb21DaGFuZ2UpO1xyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9zaG91bGRTaG93U3RhdHVzTWVzc2FnZSgpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1c1VzZXIgPSB0aGlzLl9nZXRTdGF0dXNNZXNzYWdlVXNlcigpO1xyXG4gICAgICAgICAgICBpZiAoc3RhdHVzVXNlcikge1xyXG4gICAgICAgICAgICAgICAgc3RhdHVzVXNlci5yZW1vdmVMaXN0ZW5lcihcclxuICAgICAgICAgICAgICAgICAgICBcIlVzZXIuX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2VcIixcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9vblN0YXR1c01lc3NhZ2VDb21taXR0ZWQsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSB3aXRoIGFwcHJvcHJpYXRlIGxpZmVjeWNsZSBldmVudFxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uKHByb3BzKSB7XHJcbiAgICAgICAgLy8gWFhYOiBUaGlzIGNvdWxkIGJlIGEgbG90IGJldHRlciAtIHRoaXMgbWFrZXMgdGhlIGFzc3VtcHRpb24gdGhhdFxyXG4gICAgICAgIC8vIHRoZSBub3RpZmljYXRpb24gY291bnQgbWF5IGhhdmUgY2hhbmdlZCB3aGVuIHRoZSBwcm9wZXJ0aWVzIG9mXHJcbiAgICAgICAgLy8gdGhlIHJvb20gdGlsZSBjaGFuZ2UuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvbkNvdW50OiB0aGlzLnByb3BzLnJvb20uZ2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQoKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gRG8gYSBzaW1wbGUgc2hhbGxvdyBjb21wYXJpc29uIG9mIHByb3BzIGFuZCBzdGF0ZSB0byBhdm9pZCB1bm5lY2Vzc2FyeVxyXG4gICAgLy8gcmVuZGVycy4gVGhlIGFzc3VtcHRpb24gbWFkZSBoZXJlIGlzIHRoYXQgb25seSBzdGF0ZSBhbmQgcHJvcHMgYXJlIHVzZWRcclxuICAgIC8vIGluIHJlbmRlcmluZyB0aGlzIGNvbXBvbmVudCBhbmQgY2hpbGRyZW4uXHJcbiAgICAvL1xyXG4gICAgLy8gUm9vbUxpc3QgaXMgZnJlcXVlbnRseSBtYWRlIHRvIGZvcmNlVXBkYXRlLCBzbyB0aGlzIGRlY3JlYXNlcyBudW1iZXIgb2ZcclxuICAgIC8vIFJvb21UaWxlIHJlbmRlcmluZ3MuXHJcbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IGZ1bmN0aW9uKG5ld1Byb3BzLCBuZXdTdGF0ZSkge1xyXG4gICAgICAgIGlmIChPYmplY3Qua2V5cyhuZXdQcm9wcykuc29tZSgoaykgPT4gbmV3UHJvcHNba10gIT09IHRoaXMucHJvcHNba10pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoT2JqZWN0LmtleXMobmV3U3RhdGUpLnNvbWUoKGspID0+IG5ld1N0YXRlW2tdICE9PSB0aGlzLnN0YXRlW2tdKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25TdGF0dXNNZXNzYWdlQ29tbWl0dGVkKCkge1xyXG4gICAgICAgIC8vIFRoZSBzdGF0dXMgbWVzc2FnZSBgVXNlcmAgb2JqZWN0IGhhcyBvYnNlcnZlZCBhIG1lc3NhZ2UgY2hhbmdlLlxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzdGF0dXNNZXNzYWdlOiB0aGlzLl9nZXRTdGF0dXNNZXNzYWdlKCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQ2xpY2s6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25DbGljaykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uQ2xpY2sodGhpcy5wcm9wcy5yb29tLnJvb21JZCwgZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Nb3VzZUVudGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKCB7IGhvdmVyOiB0cnVlIH0pO1xyXG4gICAgICAgIHRoaXMuYmFkZ2VPbk1vdXNlRW50ZXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Nb3VzZUxlYXZlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKCB7IGhvdmVyOiBmYWxzZSB9KTtcclxuICAgICAgICB0aGlzLmJhZGdlT25Nb3VzZUxlYXZlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGJhZGdlT25Nb3VzZUVudGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBPbmx5IGFsbG93IG5vbi1ndWVzdHMgdG8gYWNjZXNzIHRoZSBjb250ZXh0IG1lbnVcclxuICAgICAgICAvLyBhbmQgb25seSBjaGFuZ2UgaXQgaWYgaXQgbmVlZHMgdG8gY2hhbmdlXHJcbiAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNHdWVzdCgpICYmICF0aGlzLnN0YXRlLmJhZGdlSG92ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSggeyBiYWRnZUhvdmVyOiB0cnVlIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGJhZGdlT25Nb3VzZUxlYXZlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKCB7IGJhZGdlSG92ZXI6IGZhbHNlIH0gKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3Nob3dDb250ZXh0TWVudTogZnVuY3Rpb24oYm91bmRpbmdDbGllbnRSZWN0KSB7XHJcbiAgICAgICAgLy8gT25seSBhbGxvdyBub24tZ3Vlc3RzIHRvIGFjY2VzcyB0aGUgY29udGV4dCBtZW51XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3Qgc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHRNZW51UG9zaXRpb246IGJvdW5kaW5nQ2xpZW50UmVjdCxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBJZiB0aGUgYmFkZ2UgaXMgY2xpY2tlZCwgdGhlbiBubyBsb25nZXIgc2hvdyB0b29sdGlwXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuY29sbGFwc2VkKSB7XHJcbiAgICAgICAgICAgIHN0YXRlLmhvdmVyID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHN0YXRlKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Db250ZXh0TWVudUJ1dHRvbkNsaWNrOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgLy8gUHJldmVudCB0aGUgUm9vbVRpbGUgb25DbGljayBldmVudCBmaXJpbmcgYXMgd2VsbFxyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICB0aGlzLl9zaG93Q29udGV4dE1lbnUoZS50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNvbnRleHRNZW51OiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgLy8gUHJldmVudCB0aGUgbmF0aXZlIGNvbnRleHQgbWVudVxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5fc2hvd0NvbnRleHRNZW51KHtcclxuICAgICAgICAgICAgcmlnaHQ6IGUuY2xpZW50WCxcclxuICAgICAgICAgICAgdG9wOiBlLmNsaWVudFksXHJcbiAgICAgICAgICAgIGhlaWdodDogMCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgY2xvc2VNZW51OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgY29udGV4dE1lbnVQb3NpdGlvbjogbnVsbCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnByb3BzLnJlZnJlc2hTdWJMaXN0KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgaXNJbnZpdGUgPSB0aGlzLnByb3BzLnJvb20uZ2V0TXlNZW1iZXJzaGlwKCkgPT09IFwiaW52aXRlXCI7XHJcbiAgICAgICAgY29uc3Qgbm90aWZpY2F0aW9uQ291bnQgPSB0aGlzLnByb3BzLm5vdGlmaWNhdGlvbkNvdW50O1xyXG4gICAgICAgIC8vIHZhciBoaWdobGlnaHRDb3VudCA9IHRoaXMucHJvcHMucm9vbS5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudChcImhpZ2hsaWdodFwiKTtcclxuXHJcbiAgICAgICAgY29uc3Qgbm90aWZCYWRnZXMgPSBub3RpZmljYXRpb25Db3VudCA+IDAgJiYgUm9vbU5vdGlmcy5zaG91bGRTaG93Tm90aWZCYWRnZSh0aGlzLnN0YXRlLm5vdGlmU3RhdGUpO1xyXG4gICAgICAgIGNvbnN0IG1lbnRpb25CYWRnZXMgPSB0aGlzLnByb3BzLmhpZ2hsaWdodCAmJiBSb29tTm90aWZzLnNob3VsZFNob3dNZW50aW9uQmFkZ2UodGhpcy5zdGF0ZS5ub3RpZlN0YXRlKTtcclxuICAgICAgICBjb25zdCBiYWRnZXMgPSBub3RpZkJhZGdlcyB8fCBtZW50aW9uQmFkZ2VzO1xyXG5cclxuICAgICAgICBsZXQgc3VidGV4dCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Nob3VsZFNob3dTdGF0dXNNZXNzYWdlKCkpIHtcclxuICAgICAgICAgICAgc3VidGV4dCA9IHRoaXMuc3RhdGUuc3RhdHVzTWVzc2FnZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGlzTWVudURpc3BsYXllZCA9IEJvb2xlYW4odGhpcy5zdGF0ZS5jb250ZXh0TWVudVBvc2l0aW9uKTtcclxuXHJcbiAgICAgICAgY29uc3QgZG1Vc2VySWQgPSBETVJvb21NYXAuc2hhcmVkKCkuZ2V0VXNlcklkRm9yUm9vbUlkKHRoaXMucHJvcHMucm9vbS5yb29tSWQpO1xyXG5cclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZSc6IHRydWUsXHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZV9zZWxlY3RlZCc6IHRoaXMuc3RhdGUuc2VsZWN0ZWQsXHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZV91bnJlYWQnOiB0aGlzLnByb3BzLnVucmVhZCxcclxuICAgICAgICAgICAgJ214X1Jvb21UaWxlX3VucmVhZE5vdGlmeSc6IG5vdGlmQmFkZ2VzLFxyXG4gICAgICAgICAgICAnbXhfUm9vbVRpbGVfaGlnaGxpZ2h0JzogbWVudGlvbkJhZGdlcyxcclxuICAgICAgICAgICAgJ214X1Jvb21UaWxlX2ludml0ZWQnOiBpc0ludml0ZSxcclxuICAgICAgICAgICAgJ214X1Jvb21UaWxlX21lbnVEaXNwbGF5ZWQnOiBpc01lbnVEaXNwbGF5ZWQsXHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZV9ub0JhZGdlcyc6ICFiYWRnZXMsXHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZV90cmFuc3BhcmVudCc6IHRoaXMucHJvcHMudHJhbnNwYXJlbnQsXHJcbiAgICAgICAgICAgICdteF9Sb29tVGlsZV9oYXNTdWJ0ZXh0Jzogc3VidGV4dCAmJiAhdGhpcy5wcm9wcy5jb2xsYXBzZWQsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGF2YXRhckNsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgJ214X1Jvb21UaWxlX2F2YXRhcic6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGJhZGdlQ2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICAnbXhfUm9vbVRpbGVfYmFkZ2UnOiB0cnVlLFxyXG4gICAgICAgICAgICAnbXhfUm9vbVRpbGVfYmFkZ2VCdXR0b24nOiB0aGlzLnN0YXRlLmJhZGdlSG92ZXIgfHwgaXNNZW51RGlzcGxheWVkLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgbmFtZSA9IHRoaXMuc3RhdGUucm9vbU5hbWU7XHJcbiAgICAgICAgaWYgKG5hbWUgPT0gdW5kZWZpbmVkIHx8IG5hbWUgPT0gbnVsbCkgbmFtZSA9ICcnO1xyXG4gICAgICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoXCI6XCIsIFwiOlxcdTIwMGJcIik7IC8vIGFkZCBhIHplcm8td2lkdGggc3BhY2UgdG8gYWxsb3cgbGluZXdyYXBwaW5nIGFmdGVyIHRoZSBjb2xvblxyXG5cclxuXHJcbiAgICAgICAgbGV0IGJhZGdlO1xyXG4gICAgICAgIGlmIChiYWRnZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgbGltaXRlZENvdW50ID0gRm9ybWF0dGluZ1V0aWxzLmZvcm1hdENvdW50KG5vdGlmaWNhdGlvbkNvdW50KTtcclxuICAgICAgICAgICAgY29uc3QgYmFkZ2VDb250ZW50ID0gbm90aWZpY2F0aW9uQ291bnQgPyBsaW1pdGVkQ291bnQgOiAnISc7XHJcbiAgICAgICAgICAgIGJhZGdlID0gPGRpdiBjbGFzc05hbWU9e2JhZGdlQ2xhc3Nlc30+eyBiYWRnZUNvbnRlbnQgfTwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBsYWJlbDtcclxuICAgICAgICBsZXQgc3VidGV4dExhYmVsO1xyXG4gICAgICAgIGxldCB0b29sdGlwO1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5jb2xsYXBzZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgbmFtZUNsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgICAgICdteF9Sb29tVGlsZV9uYW1lJzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICdteF9Sb29tVGlsZV9pbnZpdGUnOiB0aGlzLnByb3BzLmlzSW52aXRlLFxyXG4gICAgICAgICAgICAgICAgJ214X1Jvb21UaWxlX2JhZGdlU2hvd24nOiBiYWRnZXMgfHwgdGhpcy5zdGF0ZS5iYWRnZUhvdmVyIHx8IGlzTWVudURpc3BsYXllZCxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBzdWJ0ZXh0TGFiZWwgPSBzdWJ0ZXh0ID8gPHNwYW4gY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVfc3VidGV4dFwiPnsgc3VidGV4dCB9PC9zcGFuPiA6IG51bGw7XHJcbiAgICAgICAgICAgIC8vIFhYWDogdGhpcyBpcyBhIHdvcmthcm91bmQgZm9yIEZpcmVmb3ggZ2l2aW5nIHRoaXMgZGl2IGEgdGFic3RvcCA6KCBbdGFiSW5kZXhdXHJcbiAgICAgICAgICAgIGxhYmVsID0gPGRpdiB0aXRsZT17bmFtZX0gY2xhc3NOYW1lPXtuYW1lQ2xhc3Nlc30gdGFiSW5kZXg9ey0xfSBkaXI9XCJhdXRvXCI+eyBuYW1lIH08L2Rpdj47XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmhvdmVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVG9vbHRpcFwiKTtcclxuICAgICAgICAgICAgdG9vbHRpcCA9IDxUb29sdGlwIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlX3Rvb2x0aXBcIiBsYWJlbD17dGhpcy5wcm9wcy5yb29tLm5hbWV9IGRpcj1cImF1dG9cIiAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vdmFyIGluY29taW5nQ2FsbEJveDtcclxuICAgICAgICAvL2lmICh0aGlzLnByb3BzLmluY29taW5nQ2FsbCkge1xyXG4gICAgICAgIC8vICAgIHZhciBJbmNvbWluZ0NhbGxCb3ggPSBzZGsuZ2V0Q29tcG9uZW50KFwidm9pcC5JbmNvbWluZ0NhbGxCb3hcIik7XHJcbiAgICAgICAgLy8gICAgaW5jb21pbmdDYWxsQm94ID0gPEluY29taW5nQ2FsbEJveCBpbmNvbWluZ0NhbGw9eyB0aGlzLnByb3BzLmluY29taW5nQ2FsbCB9Lz47XHJcbiAgICAgICAgLy99XHJcblxyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcblxyXG4gICAgICAgIGxldCBjb250ZXh0TWVudUJ1dHRvbjtcclxuICAgICAgICBpZiAoIU1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAgICAgY29udGV4dE1lbnVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8Q29udGV4dE1lbnVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tVGlsZV9tZW51QnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJPcHRpb25zXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIGlzRXhwYW5kZWQ9e2lzTWVudURpc3BsYXllZH1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ29udGV4dE1lbnVCdXR0b25DbGlja30gLz5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IFJvb21BdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdhdmF0YXJzLlJvb21BdmF0YXInKTtcclxuXHJcbiAgICAgICAgbGV0IGFyaWFMYWJlbCA9IG5hbWU7XHJcblxyXG4gICAgICAgIGxldCBkbUluZGljYXRvcjtcclxuICAgICAgICBsZXQgZG1PbmxpbmU7XHJcbiAgICAgICAgLyogUG9zdC1jcm9zcy1zaWduaW5nIHdlIGRvbid0IHNob3cgRE0gaW5kaWNhdG9ycyBhdCBhbGwsIGluc3RlYWQgcmVseWluZyBvbiB1c2VyXHJcbiAgICAgICAgICAgY29udGV4dCB0byBsZXQgdGhlbSBrbm93IHdoZW4gdGhhdCBpcy4gKi9cclxuICAgICAgICBpZiAoZG1Vc2VySWQgJiYgIVNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3Jvc3Nfc2lnbmluZ1wiKSkge1xyXG4gICAgICAgICAgICBkbUluZGljYXRvciA9IDxpbWdcclxuICAgICAgICAgICAgICAgIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbl9wZXJzb24uc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVfZG1cIlxyXG4gICAgICAgICAgICAgICAgd2lkdGg9XCIxMVwiXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxM1wiXHJcbiAgICAgICAgICAgICAgICBhbHQ9XCJkbVwiXHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgeyByb29tIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGNvbnN0IG1lbWJlciA9IHJvb20uZ2V0TWVtYmVyKGRtVXNlcklkKTtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIG1lbWJlciAmJiBtZW1iZXIubWVtYmVyc2hpcCA9PT0gXCJqb2luXCIgJiYgcm9vbS5nZXRKb2luZWRNZW1iZXJDb3VudCgpID09PSAyICYmXHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfcHJlc2VuY2VfaW5fcm9vbV9saXN0XCIpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFVzZXJPbmxpbmVEb3QgPSBzZGsuZ2V0Q29tcG9uZW50KCdyb29tcy5Vc2VyT25saW5lRG90Jyk7XHJcbiAgICAgICAgICAgIGRtT25saW5lID0gPFVzZXJPbmxpbmVEb3QgdXNlcklkPXtkbVVzZXJJZH0gLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBUaGUgZm9sbG93aW5nIGxhYmVscyBhcmUgd3JpdHRlbiBpbiBzdWNoIGEgZmFzaGlvbiB0byBpbmNyZWFzZSBzY3JlZW4gcmVhZGVyIGVmZmljaWVuY3kgKHNwZWVkKS5cclxuICAgICAgICBpZiAobm90aWZCYWRnZXMgJiYgbWVudGlvbkJhZGdlcyAmJiAhaXNJbnZpdGUpIHtcclxuICAgICAgICAgICAgYXJpYUxhYmVsICs9IFwiIFwiICsgX3QoXCIlKGNvdW50KXMgdW5yZWFkIG1lc3NhZ2VzIGluY2x1ZGluZyBtZW50aW9ucy5cIiwge1xyXG4gICAgICAgICAgICAgICAgY291bnQ6IG5vdGlmaWNhdGlvbkNvdW50LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5vdGlmQmFkZ2VzKSB7XHJcbiAgICAgICAgICAgIGFyaWFMYWJlbCArPSBcIiBcIiArIF90KFwiJShjb3VudClzIHVucmVhZCBtZXNzYWdlcy5cIiwgeyBjb3VudDogbm90aWZpY2F0aW9uQ291bnQgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChtZW50aW9uQmFkZ2VzICYmICFpc0ludml0ZSkge1xyXG4gICAgICAgICAgICBhcmlhTGFiZWwgKz0gXCIgXCIgKyBfdChcIlVucmVhZCBtZW50aW9ucy5cIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByb3BzLnVucmVhZCkge1xyXG4gICAgICAgICAgICBhcmlhTGFiZWwgKz0gXCIgXCIgKyBfdChcIlVucmVhZCBtZXNzYWdlcy5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY29udGV4dE1lbnU7XHJcbiAgICAgICAgaWYgKGlzTWVudURpc3BsYXllZCkge1xyXG4gICAgICAgICAgICBjb25zdCBSb29tVGlsZUNvbnRleHRNZW51ID0gc2RrLmdldENvbXBvbmVudCgnY29udGV4dF9tZW51cy5Sb29tVGlsZUNvbnRleHRNZW51Jyk7XHJcbiAgICAgICAgICAgIGNvbnRleHRNZW51ID0gKFxyXG4gICAgICAgICAgICAgICAgPENvbnRleHRNZW51IHsuLi50b1JpZ2h0T2YodGhpcy5zdGF0ZS5jb250ZXh0TWVudVBvc2l0aW9uKX0gb25GaW5pc2hlZD17dGhpcy5jbG9zZU1lbnV9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxSb29tVGlsZUNvbnRleHRNZW51IHJvb209e3RoaXMucHJvcHMucm9vbX0gb25GaW5pc2hlZD17dGhpcy5jbG9zZU1lbnV9IC8+XHJcbiAgICAgICAgICAgICAgICA8L0NvbnRleHRNZW51PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHByaXZhdGVJY29uID0gbnVsbDtcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmpvaW5SdWxlID09IFwiaW52aXRlXCIgJiYgIWRtVXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBwcml2YXRlSWNvbiA9IDxJbnZpdGVPbmx5SWNvbiBjb2xsYXBzZWRQYW5lbD17dGhpcy5wcm9wcy5jb2xsYXBzZWR9IC8+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZTJlSWNvbiA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZTJlU3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGUyZUljb24gPSA8RTJFSWNvbiBzdGF0dXM9e3RoaXMuc3RhdGUuZTJlU3RhdHVzfSBjbGFzc05hbWU9XCJteF9Sb29tVGlsZV9lMmVJY29uXCIgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICA8Um92aW5nVGFiSW5kZXhXcmFwcGVyIGlucHV0UmVmPXt0aGlzLl9yb29tVGlsZX0+XHJcbiAgICAgICAgICAgICAgICB7KHtvbkZvY3VzLCBpc0FjdGl2ZSwgcmVmfSkgPT5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZvY3VzPXtvbkZvY3VzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWJJbmRleD17aXNBY3RpdmUgPyAwIDogLTF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0UmVmPXtyZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNvbnRleHRNZW51PXt0aGlzLm9uQ29udGV4dE1lbnV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9e2FyaWFMYWJlbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJpYS1zZWxlY3RlZD17dGhpcy5zdGF0ZS5zZWxlY3RlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9sZT1cInRyZWVpdGVtXCJcclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXthdmF0YXJDbGFzc2VzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVfYXZhdGFyX2NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb29tQXZhdGFyIHJvb209e3RoaXMucHJvcHMucm9vbX0gd2lkdGg9ezI0fSBoZWlnaHQ9ezI0fSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgZG1JbmRpY2F0b3IgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgZTJlSWNvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgcHJpdmF0ZUljb24gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlX25hbWVDb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVfbGFiZWxDb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGxhYmVsIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHN1YnRleHRMYWJlbCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgZG1PbmxpbmUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBjb250ZXh0TWVudUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGJhZGdlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgLyogeyBpbmNvbWluZ0NhbGxCb3ggfSAqLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdG9vbHRpcCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L1JvdmluZ1RhYkluZGV4V3JhcHBlcj5cclxuXHJcbiAgICAgICAgICAgIHsgY29udGV4dE1lbnUgfVxyXG4gICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+O1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==