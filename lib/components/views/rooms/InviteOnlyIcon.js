"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class InviteOnlyIcon extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onHoverStart", () => {
      this.setState({
        hover: true
      });
    });
    (0, _defineProperty2.default)(this, "onHoverEnd", () => {
      this.setState({
        hover: false
      });
    });
    this.state = {
      hover: false
    };
  }

  render() {
    const classes = this.props.collapsedPanel ? "mx_InviteOnlyIcon_small" : "mx_InviteOnlyIcon_large";

    if (!_SettingsStore.default.isFeatureEnabled("feature_invite_only_padlocks")) {
      return null;
    }

    const Tooltip = sdk.getComponent("elements.Tooltip");
    let tooltip;

    if (this.state.hover) {
      tooltip = _react.default.createElement(Tooltip, {
        className: "mx_InviteOnlyIcon_tooltip",
        label: (0, _languageHandler._t)("Invite only"),
        dir: "auto"
      });
    }

    return _react.default.createElement("div", {
      className: classes,
      onMouseEnter: this.onHoverStart,
      onMouseLeave: this.onHoverEnd
    }, tooltip);
  }

}

exports.default = InviteOnlyIcon;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL0ludml0ZU9ubHlJY29uLmpzIl0sIm5hbWVzIjpbIkludml0ZU9ubHlJY29uIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInNldFN0YXRlIiwiaG92ZXIiLCJzdGF0ZSIsInJlbmRlciIsImNsYXNzZXMiLCJwcm9wcyIsImNvbGxhcHNlZFBhbmVsIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJUb29sdGlwIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwidG9vbHRpcCIsIm9uSG92ZXJTdGFydCIsIm9uSG92ZXJFbmQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBbkJBOzs7Ozs7Ozs7Ozs7Ozs7QUFxQmUsTUFBTUEsY0FBTixTQUE2QkMsZUFBTUMsU0FBbkMsQ0FBNkM7QUFDeERDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUsd0RBUUMsTUFBTTtBQUNqQixXQUFLQyxRQUFMLENBQWM7QUFBQ0MsUUFBQUEsS0FBSyxFQUFFO0FBQVIsT0FBZDtBQUNILEtBVmE7QUFBQSxzREFZRCxNQUFNO0FBQ2YsV0FBS0QsUUFBTCxDQUFjO0FBQUNDLFFBQUFBLEtBQUssRUFBRTtBQUFSLE9BQWQ7QUFDSCxLQWRhO0FBR1YsU0FBS0MsS0FBTCxHQUFhO0FBQ1RELE1BQUFBLEtBQUssRUFBRTtBQURFLEtBQWI7QUFHSDs7QUFVREUsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsT0FBTyxHQUFHLEtBQUtDLEtBQUwsQ0FBV0MsY0FBWCxHQUE0Qix5QkFBNUIsR0FBdUQseUJBQXZFOztBQUVBLFFBQUksQ0FBQ0MsdUJBQWNDLGdCQUFkLENBQStCLDhCQUEvQixDQUFMLEVBQXFFO0FBQ2pFLGFBQU8sSUFBUDtBQUNIOztBQUVELFVBQU1DLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFFBQUlDLE9BQUo7O0FBQ0EsUUFBSSxLQUFLVixLQUFMLENBQVdELEtBQWYsRUFBc0I7QUFDbEJXLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxPQUFEO0FBQVMsUUFBQSxTQUFTLEVBQUMsMkJBQW5CO0FBQStDLFFBQUEsS0FBSyxFQUFFLHlCQUFHLGFBQUgsQ0FBdEQ7QUFBeUUsUUFBQSxHQUFHLEVBQUM7QUFBN0UsUUFBVjtBQUNIOztBQUNELFdBQVE7QUFBSyxNQUFBLFNBQVMsRUFBRVIsT0FBaEI7QUFDTixNQUFBLFlBQVksRUFBRSxLQUFLUyxZQURiO0FBRU4sTUFBQSxZQUFZLEVBQUUsS0FBS0M7QUFGYixPQUlKRixPQUpJLENBQVI7QUFNSDs7QUFuQ3VEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gJy4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmUnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSW52aXRlT25seUljb24gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaG92ZXI6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgb25Ib3ZlclN0YXJ0ID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2hvdmVyOiB0cnVlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uSG92ZXJFbmQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aG92ZXI6IGZhbHNlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gdGhpcy5wcm9wcy5jb2xsYXBzZWRQYW5lbCA/IFwibXhfSW52aXRlT25seUljb25fc21hbGxcIjogXCJteF9JbnZpdGVPbmx5SWNvbl9sYXJnZVwiO1xyXG5cclxuICAgICAgICBpZiAoIVNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfaW52aXRlX29ubHlfcGFkbG9ja3NcIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBUb29sdGlwID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlRvb2x0aXBcIik7XHJcbiAgICAgICAgbGV0IHRvb2x0aXA7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuaG92ZXIpIHtcclxuICAgICAgICAgICAgdG9vbHRpcCA9IDxUb29sdGlwIGNsYXNzTmFtZT1cIm14X0ludml0ZU9ubHlJY29uX3Rvb2x0aXBcIiBsYWJlbD17X3QoXCJJbnZpdGUgb25seVwiKX0gZGlyPVwiYXV0b1wiIC8+O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gKDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzfVxyXG4gICAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uSG92ZXJTdGFydH1cclxuICAgICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbkhvdmVyRW5kfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIHsgdG9vbHRpcCB9XHJcbiAgICAgICAgPC9kaXY+KTtcclxuICAgIH1cclxufVxyXG4iXX0=