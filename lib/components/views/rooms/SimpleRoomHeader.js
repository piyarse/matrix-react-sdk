"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CancelButton = CancelButton;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// cancel button which is shared between room header and simple room header
function CancelButton(props) {
  const {
    onClick
  } = props;
  return _react.default.createElement(_AccessibleButton.default, {
    className: "mx_RoomHeader_cancelButton",
    onClick: onClick
  }, _react.default.createElement("img", {
    src: require("../../../../res/img/cancel.svg"),
    className: "mx_filterFlipColor",
    width: "18",
    height: "18",
    alt: (0, _languageHandler._t)("Cancel")
  }));
}
/*
 * A stripped-down room header used for things like the user settings
 * and room directory.
 */


var _default = (0, _createReactClass.default)({
  displayName: 'SimpleRoomHeader',
  propTypes: {
    title: _propTypes.default.string,
    onCancelClick: _propTypes.default.func,
    // `src` to a TintableSvg. Optional.
    icon: _propTypes.default.string
  },
  render: function () {
    let cancelButton;
    let icon;

    if (this.props.onCancelClick) {
      cancelButton = _react.default.createElement(CancelButton, {
        onClick: this.props.onCancelClick
      });
    }

    if (this.props.icon) {
      const TintableSvg = sdk.getComponent('elements.TintableSvg');
      icon = _react.default.createElement(TintableSvg, {
        className: "mx_RoomHeader_icon",
        src: this.props.icon,
        width: "25",
        height: "25"
      });
    }

    return _react.default.createElement("div", {
      className: "mx_RoomHeader"
    }, _react.default.createElement("div", {
      className: "mx_RoomHeader_wrapper"
    }, _react.default.createElement("div", {
      className: "mx_RoomHeader_simpleHeader"
    }, icon, this.props.title, cancelButton)));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1NpbXBsZVJvb21IZWFkZXIuanMiXSwibmFtZXMiOlsiQ2FuY2VsQnV0dG9uIiwicHJvcHMiLCJvbkNsaWNrIiwicmVxdWlyZSIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwidGl0bGUiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJvbkNhbmNlbENsaWNrIiwiZnVuYyIsImljb24iLCJyZW5kZXIiLCJjYW5jZWxCdXR0b24iLCJUaW50YWJsZVN2ZyIsInNkayIsImdldENvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBO0FBQ08sU0FBU0EsWUFBVCxDQUFzQkMsS0FBdEIsRUFBNkI7QUFDaEMsUUFBTTtBQUFDQyxJQUFBQTtBQUFELE1BQVlELEtBQWxCO0FBRUEsU0FDSSw2QkFBQyx5QkFBRDtBQUFrQixJQUFBLFNBQVMsRUFBQyw0QkFBNUI7QUFBeUQsSUFBQSxPQUFPLEVBQUVDO0FBQWxFLEtBQ0k7QUFBSyxJQUFBLEdBQUcsRUFBRUMsT0FBTyxDQUFDLGdDQUFELENBQWpCO0FBQXFELElBQUEsU0FBUyxFQUFDLG9CQUEvRDtBQUNJLElBQUEsS0FBSyxFQUFDLElBRFY7QUFDZSxJQUFBLE1BQU0sRUFBQyxJQUR0QjtBQUMyQixJQUFBLEdBQUcsRUFBRSx5QkFBRyxRQUFIO0FBRGhDLElBREosQ0FESjtBQU1IO0FBRUQ7Ozs7OztlQUllLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLGtCQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsS0FBSyxFQUFFQyxtQkFBVUMsTUFEVjtBQUVQQyxJQUFBQSxhQUFhLEVBQUVGLG1CQUFVRyxJQUZsQjtBQUlQO0FBQ0FDLElBQUFBLElBQUksRUFBRUosbUJBQVVDO0FBTFQsR0FIaUI7QUFXNUJJLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsUUFBSUMsWUFBSjtBQUNBLFFBQUlGLElBQUo7O0FBQ0EsUUFBSSxLQUFLVixLQUFMLENBQVdRLGFBQWYsRUFBOEI7QUFDMUJJLE1BQUFBLFlBQVksR0FBRyw2QkFBQyxZQUFEO0FBQWMsUUFBQSxPQUFPLEVBQUUsS0FBS1osS0FBTCxDQUFXUTtBQUFsQyxRQUFmO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLUixLQUFMLENBQVdVLElBQWYsRUFBcUI7QUFDakIsWUFBTUcsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXBCO0FBQ0FMLE1BQUFBLElBQUksR0FBRyw2QkFBQyxXQUFEO0FBQ0gsUUFBQSxTQUFTLEVBQUMsb0JBRFA7QUFDNEIsUUFBQSxHQUFHLEVBQUUsS0FBS1YsS0FBTCxDQUFXVSxJQUQ1QztBQUVILFFBQUEsS0FBSyxFQUFDLElBRkg7QUFFUSxRQUFBLE1BQU0sRUFBQztBQUZmLFFBQVA7QUFJSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNQSxJQUROLEVBRU0sS0FBS1YsS0FBTCxDQUFXSyxLQUZqQixFQUdNTyxZQUhOLENBREosQ0FESixDQURKO0FBV0g7QUFwQzJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4uL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b24nO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG4vLyBjYW5jZWwgYnV0dG9uIHdoaWNoIGlzIHNoYXJlZCBiZXR3ZWVuIHJvb20gaGVhZGVyIGFuZCBzaW1wbGUgcm9vbSBoZWFkZXJcclxuZXhwb3J0IGZ1bmN0aW9uIENhbmNlbEJ1dHRvbihwcm9wcykge1xyXG4gICAgY29uc3Qge29uQ2xpY2t9ID0gcHJvcHM7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9J214X1Jvb21IZWFkZXJfY2FuY2VsQnV0dG9uJyBvbkNsaWNrPXtvbkNsaWNrfT5cclxuICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2NhbmNlbC5zdmdcIil9IGNsYXNzTmFtZT0nbXhfZmlsdGVyRmxpcENvbG9yJ1xyXG4gICAgICAgICAgICAgICAgd2lkdGg9XCIxOFwiIGhlaWdodD1cIjE4XCIgYWx0PXtfdChcIkNhbmNlbFwiKX0gLz5cclxuICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICApO1xyXG59XHJcblxyXG4vKlxyXG4gKiBBIHN0cmlwcGVkLWRvd24gcm9vbSBoZWFkZXIgdXNlZCBmb3IgdGhpbmdzIGxpa2UgdGhlIHVzZXIgc2V0dGluZ3NcclxuICogYW5kIHJvb20gZGlyZWN0b3J5LlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1NpbXBsZVJvb21IZWFkZXInLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIG9uQ2FuY2VsQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBgc3JjYCB0byBhIFRpbnRhYmxlU3ZnLiBPcHRpb25hbC5cclxuICAgICAgICBpY29uOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCBjYW5jZWxCdXR0b247XHJcbiAgICAgICAgbGV0IGljb247XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25DYW5jZWxDbGljaykge1xyXG4gICAgICAgICAgICBjYW5jZWxCdXR0b24gPSA8Q2FuY2VsQnV0dG9uIG9uQ2xpY2s9e3RoaXMucHJvcHMub25DYW5jZWxDbGlja30gLz47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmljb24pIHtcclxuICAgICAgICAgICAgY29uc3QgVGludGFibGVTdmcgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5UaW50YWJsZVN2ZycpO1xyXG4gICAgICAgICAgICBpY29uID0gPFRpbnRhYmxlU3ZnXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tSGVhZGVyX2ljb25cIiBzcmM9e3RoaXMucHJvcHMuaWNvbn1cclxuICAgICAgICAgICAgICAgIHdpZHRoPVwiMjVcIiBoZWlnaHQ9XCIyNVwiXHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tSGVhZGVyXCIgPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tSGVhZGVyX3dyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfc2ltcGxlSGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgaWNvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhpcy5wcm9wcy50aXRsZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgY2FuY2VsQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==