"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _createRoom = _interopRequireDefault(require("../../../createRoom"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var Unread = _interopRequireWildcard(require("../../../Unread"));

var _Receipt = require("../../../utils/Receipt");

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _MultiInviter = _interopRequireDefault(require("../../../utils/MultiInviter"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _E2EIcon = _interopRequireDefault(require("./E2EIcon"));

var _AutoHideScrollbar = _interopRequireDefault(require("../../structures/AutoHideScrollbar"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = (0, _createReactClass.default)({
  displayName: 'MemberInfo',
  propTypes: {
    member: _propTypes.default.object.isRequired
  },
  getInitialState: function () {
    return {
      can: {
        kick: false,
        ban: false,
        mute: false,
        modifyLevel: false,
        synapseDeactivate: false,
        redactMessages: false
      },
      muted: false,
      isTargetMod: false,
      updating: 0,
      devicesLoading: true,
      devices: null,
      isIgnoring: false
    };
  },
  statics: {
    contextType: _MatrixClientContext.default
  },
  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    this._cancelDeviceList = null;
    const cli = this.context; // only display the devices list if our client supports E2E

    this._enableDevices = cli.isCryptoEnabled();
    cli.on("deviceVerificationChanged", this.onDeviceVerificationChanged);
    cli.on("Room", this.onRoom);
    cli.on("deleteRoom", this.onDeleteRoom);
    cli.on("Room.timeline", this.onRoomTimeline);
    cli.on("Room.name", this.onRoomName);
    cli.on("Room.receipt", this.onRoomReceipt);
    cli.on("RoomState.events", this.onRoomStateEvents);
    cli.on("RoomMember.name", this.onRoomMemberName);
    cli.on("RoomMember.membership", this.onRoomMemberMembership);
    cli.on("accountData", this.onAccountData);

    this._checkIgnoreState();

    this._updateStateForNewMember(this.props.member);
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (newProps) {
    if (this.props.member.userId !== newProps.member.userId) {
      this._updateStateForNewMember(newProps.member);
    }
  },
  componentWillUnmount: function () {
    const client = this.context;

    if (client) {
      client.removeListener("deviceVerificationChanged", this.onDeviceVerificationChanged);
      client.removeListener("Room", this.onRoom);
      client.removeListener("deleteRoom", this.onDeleteRoom);
      client.removeListener("Room.timeline", this.onRoomTimeline);
      client.removeListener("Room.name", this.onRoomName);
      client.removeListener("Room.receipt", this.onRoomReceipt);
      client.removeListener("RoomState.events", this.onRoomStateEvents);
      client.removeListener("RoomMember.name", this.onRoomMemberName);
      client.removeListener("RoomMember.membership", this.onRoomMemberMembership);
      client.removeListener("accountData", this.onAccountData);
    }

    if (this._cancelDeviceList) {
      this._cancelDeviceList();
    }
  },
  _checkIgnoreState: function () {
    const isIgnoring = this.context.isUserIgnored(this.props.member.userId);
    this.setState({
      isIgnoring: isIgnoring
    });
  },
  _disambiguateDevices: function (devices) {
    const names = Object.create(null);

    for (let i = 0; i < devices.length; i++) {
      const name = devices[i].getDisplayName();
      const indexList = names[name] || [];
      indexList.push(i);
      names[name] = indexList;
    }

    for (const name in names) {
      if (names[name].length > 1) {
        names[name].forEach(j => {
          devices[j].ambiguous = true;
        });
      }
    }
  },
  onDeviceVerificationChanged: function (userId, device) {
    if (!this._enableDevices) {
      return;
    }

    if (userId === this.props.member.userId) {
      // no need to re-download the whole thing; just update our copy of
      // the list.
      // Promise.resolve to handle transition from static result to promise; can be removed
      // in future
      Promise.resolve(this.context.getStoredDevicesForUser(userId)).then(devices => {
        this.setState({
          devices: devices,
          e2eStatus: this._getE2EStatus(devices)
        });
      });
    }
  },
  _getE2EStatus: function (devices) {
    const hasUnverifiedDevice = devices.some(device => device.isUnverified());
    return hasUnverifiedDevice ? "warning" : "verified";
  },
  onRoom: function (room) {
    this.forceUpdate();
  },
  onDeleteRoom: function (roomId) {
    this.forceUpdate();
  },
  onRoomTimeline: function (ev, room, toStartOfTimeline) {
    if (toStartOfTimeline) return;
    this.forceUpdate();
  },
  onRoomName: function (room) {
    this.forceUpdate();
  },
  onRoomReceipt: function (receiptEvent, room) {
    // because if we read a notification, it will affect notification count
    // only bother updating if there's a receipt from us
    if ((0, _Receipt.findReadReceiptFromUserId)(receiptEvent, this.context.credentials.userId)) {
      this.forceUpdate();
    }
  },
  onRoomStateEvents: function (ev, state) {
    this.forceUpdate();
  },
  onRoomMemberName: function (ev, member) {
    this.forceUpdate();
  },
  onRoomMemberMembership: function (ev, member) {
    if (this.props.member.userId === member.userId) this.forceUpdate();
  },
  onAccountData: function (ev) {
    if (ev.getType() === 'm.direct') {
      this.forceUpdate();
    }
  },
  _updateStateForNewMember: async function (member) {
    const newState = await this._calculateOpsPermissions(member);
    newState.devicesLoading = true;
    newState.devices = null;
    this.setState(newState);

    if (this._cancelDeviceList) {
      this._cancelDeviceList();

      this._cancelDeviceList = null;
    }

    this._downloadDeviceList(member);
  },
  _downloadDeviceList: function (member) {
    if (!this._enableDevices) {
      return;
    }

    let cancelled = false;

    this._cancelDeviceList = function () {
      cancelled = true;
    };

    const client = this.context;
    const self = this;
    client.downloadKeys([member.userId], true).then(() => {
      return client.getStoredDevicesForUser(member.userId);
    }).finally(function () {
      self._cancelDeviceList = null;
    }).then(function (devices) {
      if (cancelled) {
        // we got cancelled - presumably a different user now
        return;
      }

      self._disambiguateDevices(devices);

      self.setState({
        devicesLoading: false,
        devices: devices,
        e2eStatus: self._getE2EStatus(devices)
      });
    }, function (err) {
      console.log("Error downloading sessions", err);
      self.setState({
        devicesLoading: false
      });
    });
  },
  onIgnoreToggle: function () {
    const ignoredUsers = this.context.getIgnoredUsers();

    if (this.state.isIgnoring) {
      const index = ignoredUsers.indexOf(this.props.member.userId);
      if (index !== -1) ignoredUsers.splice(index, 1);
    } else {
      ignoredUsers.push(this.props.member.userId);
    }

    this.context.setIgnoredUsers(ignoredUsers).then(() => {
      return this.setState({
        isIgnoring: !this.state.isIgnoring
      });
    });
  },
  onKick: function () {
    const membership = this.props.member.membership;
    const ConfirmUserActionDialog = sdk.getComponent("dialogs.ConfirmUserActionDialog");

    _Modal.default.createTrackedDialog('Confirm User Action Dialog', 'onKick', ConfirmUserActionDialog, {
      member: this.props.member,
      action: membership === "invite" ? (0, _languageHandler._t)("Disinvite") : (0, _languageHandler._t)("Kick"),
      title: membership === "invite" ? (0, _languageHandler._t)("Disinvite this user?") : (0, _languageHandler._t)("Kick this user?"),
      askReason: membership === "join",
      danger: true,
      onFinished: (proceed, reason) => {
        if (!proceed) return;
        this.setState({
          updating: this.state.updating + 1
        });
        this.context.kick(this.props.member.roomId, this.props.member.userId, reason || undefined).then(function () {
          // NO-OP; rely on the m.room.member event coming down else we could
          // get out of sync if we force setState here!
          console.log("Kick success");
        }, function (err) {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
          console.error("Kick error: " + err);

          _Modal.default.createTrackedDialog('Failed to kick', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Failed to kick"),
            description: err && err.message ? err.message : "Operation failed"
          });
        }).finally(() => {
          this.setState({
            updating: this.state.updating - 1
          });
        });
      }
    });
  },
  onBanOrUnban: function () {
    const ConfirmUserActionDialog = sdk.getComponent("dialogs.ConfirmUserActionDialog");

    _Modal.default.createTrackedDialog('Confirm User Action Dialog', 'onBanOrUnban', ConfirmUserActionDialog, {
      member: this.props.member,
      action: this.props.member.membership === 'ban' ? (0, _languageHandler._t)("Unban") : (0, _languageHandler._t)("Ban"),
      title: this.props.member.membership === 'ban' ? (0, _languageHandler._t)("Unban this user?") : (0, _languageHandler._t)("Ban this user?"),
      askReason: this.props.member.membership !== 'ban',
      danger: this.props.member.membership !== 'ban',
      onFinished: (proceed, reason) => {
        if (!proceed) return;
        this.setState({
          updating: this.state.updating + 1
        });
        let promise;

        if (this.props.member.membership === 'ban') {
          promise = this.context.unban(this.props.member.roomId, this.props.member.userId);
        } else {
          promise = this.context.ban(this.props.member.roomId, this.props.member.userId, reason || undefined);
        }

        promise.then(function () {
          // NO-OP; rely on the m.room.member event coming down else we could
          // get out of sync if we force setState here!
          console.log("Ban success");
        }, function (err) {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
          console.error("Ban error: " + err);

          _Modal.default.createTrackedDialog('Failed to ban user', '', ErrorDialog, {
            title: (0, _languageHandler._t)("Error"),
            description: (0, _languageHandler._t)("Failed to ban user")
          });
        }).finally(() => {
          this.setState({
            updating: this.state.updating - 1
          });
        });
      }
    });
  },
  onRedactAllMessages: async function () {
    const {
      roomId,
      userId
    } = this.props.member;
    const room = this.context.getRoom(roomId);

    if (!room) {
      return;
    }

    const timelineSet = room.getUnfilteredTimelineSet();
    let eventsToRedact = [];

    for (const timeline of timelineSet.getTimelines()) {
      eventsToRedact = timeline.getEvents().reduce((events, event) => {
        if (event.getSender() === userId && !event.isRedacted()) {
          return events.concat(event);
        } else {
          return events;
        }
      }, eventsToRedact);
    }

    const count = eventsToRedact.length;
    const user = this.props.member.name;

    if (count === 0) {
      const InfoDialog = sdk.getComponent("dialogs.InfoDialog");

      _Modal.default.createTrackedDialog('No user messages found to remove', '', InfoDialog, {
        title: (0, _languageHandler._t)("No recent messages by %(user)s found", {
          user
        }),
        description: _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Try scrolling up in the timeline to see if there are any earlier ones.")))
      });
    } else {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
      const confirmed = await new Promise(resolve => {
        _Modal.default.createTrackedDialog('Remove recent messages by user', '', QuestionDialog, {
          title: (0, _languageHandler._t)("Remove recent messages by %(user)s", {
            user
          }),
          description: _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You are about to remove %(count)s messages by %(user)s. This cannot be undone. Do you wish to continue?", {
            count,
            user
          })), _react.default.createElement("p", null, (0, _languageHandler._t)("For a large amount of messages, this might take some time. Please don't refresh your client in the meantime."))),
          button: (0, _languageHandler._t)("Remove %(count)s messages", {
            count
          }),
          onFinished: resolve
        });
      });

      if (!confirmed) {
        return;
      } // Submitting a large number of redactions freezes the UI,
      // so first yield to allow to rerender after closing the dialog.


      await Promise.resolve();
      console.info("Started redacting recent ".concat(count, " messages for ").concat(user, " in ").concat(roomId));
      await Promise.all(eventsToRedact.map(async event => {
        try {
          await this.context.redactEvent(roomId, event.getId());
        } catch (err) {
          // log and swallow errors
          console.error("Could not redact", event.getId());
          console.error(err);
        }
      }));
      console.info("Finished redacting recent ".concat(count, " messages for ").concat(user, " in ").concat(roomId));
    }
  },
  _warnSelfDemote: function () {
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
    return new Promise(resolve => {
      _Modal.default.createTrackedDialog('Demoting Self', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Demote yourself?"),
        description: _react.default.createElement("div", null, (0, _languageHandler._t)("You will not be able to undo this change as you are demoting yourself, " + "if you are the last privileged user in the room it will be impossible " + "to regain privileges.")),
        button: (0, _languageHandler._t)("Demote"),
        onFinished: resolve
      });
    });
  },
  onMuteToggle: async function () {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const roomId = this.props.member.roomId;
    const target = this.props.member.userId;
    const room = this.context.getRoom(roomId);
    if (!room) return; // if muting self, warn as it may be irreversible

    if (target === this.context.getUserId()) {
      try {
        if (!(await this._warnSelfDemote())) return;
      } catch (e) {
        console.error("Failed to warn about self demotion: ", e);
        return;
      }
    }

    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevelEvent) return;
    const isMuted = this.state.muted;
    const powerLevels = powerLevelEvent.getContent();
    const levelToSend = (powerLevels.events ? powerLevels.events["m.room.message"] : null) || powerLevels.events_default;
    let level;

    if (isMuted) {
      // unmute
      level = levelToSend;
    } else {
      // mute
      level = levelToSend - 1;
    }

    level = parseInt(level);

    if (!isNaN(level)) {
      this.setState({
        updating: this.state.updating + 1
      });
      this.context.setPowerLevel(roomId, target, level, powerLevelEvent).then(function () {
        // NO-OP; rely on the m.room.member event coming down else we could
        // get out of sync if we force setState here!
        console.log("Mute toggle success");
      }, function (err) {
        console.error("Mute error: " + err);

        _Modal.default.createTrackedDialog('Failed to mute user', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Error"),
          description: (0, _languageHandler._t)("Failed to mute user")
        });
      }).finally(() => {
        this.setState({
          updating: this.state.updating - 1
        });
      });
    }
  },
  onModToggle: function () {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
    const roomId = this.props.member.roomId;
    const target = this.props.member.userId;
    const room = this.context.getRoom(roomId);
    if (!room) return;
    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevelEvent) return;
    const me = room.getMember(this.context.credentials.userId);
    if (!me) return;
    const defaultLevel = powerLevelEvent.getContent().users_default;
    let modLevel = me.powerLevel - 1;
    if (modLevel > 50 && defaultLevel < 50) modLevel = 50; // try to stick with the vector level defaults
    // toggle the level

    const newLevel = this.state.isTargetMod ? defaultLevel : modLevel;
    this.setState({
      updating: this.state.updating + 1
    });
    this.context.setPowerLevel(roomId, target, parseInt(newLevel), powerLevelEvent).then(function () {
      // NO-OP; rely on the m.room.member event coming down else we could
      // get out of sync if we force setState here!
      console.log("Mod toggle success");
    }, function (err) {
      if (err.errcode === 'M_GUEST_ACCESS_FORBIDDEN') {
        _dispatcher.default.dispatch({
          action: 'require_registration'
        });
      } else {
        console.error("Toggle moderator error:" + err);

        _Modal.default.createTrackedDialog('Failed to toggle moderator status', '', ErrorDialog, {
          title: (0, _languageHandler._t)("Error"),
          description: (0, _languageHandler._t)("Failed to toggle moderator status")
        });
      }
    }).finally(() => {
      this.setState({
        updating: this.state.updating - 1
      });
    });
  },
  onSynapseDeactivate: function () {
    const QuestionDialog = sdk.getComponent('views.dialogs.QuestionDialog');

    _Modal.default.createTrackedDialog('Synapse User Deactivation', '', QuestionDialog, {
      title: (0, _languageHandler._t)("Deactivate user?"),
      description: _react.default.createElement("div", null, (0, _languageHandler._t)("Deactivating this user will log them out and prevent them from logging back in. Additionally, " + "they will leave all the rooms they are in. This action cannot be reversed. Are you sure you want to " + "deactivate this user?")),
      button: (0, _languageHandler._t)("Deactivate user"),
      danger: true,
      onFinished: accepted => {
        if (!accepted) return;
        this.context.deactivateSynapseUser(this.props.member.userId).catch(e => {
          console.error("Failed to deactivate user");
          console.error(e);
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Failed to deactivate Synapse user', '', ErrorDialog, {
            title: (0, _languageHandler._t)('Failed to deactivate user'),
            description: e && e.message ? e.message : (0, _languageHandler._t)("Operation failed")
          });
        });
      }
    });
  },
  _applyPowerChange: function (roomId, target, powerLevel, powerLevelEvent) {
    this.setState({
      updating: this.state.updating + 1
    });
    this.context.setPowerLevel(roomId, target, parseInt(powerLevel), powerLevelEvent).then(function () {
      // NO-OP; rely on the m.room.member event coming down else we could
      // get out of sync if we force setState here!
      console.log("Power change success");
    }, function (err) {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
      console.error("Failed to change power level " + err);

      _Modal.default.createTrackedDialog('Failed to change power level', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Error"),
        description: (0, _languageHandler._t)("Failed to change power level")
      });
    }).finally(() => {
      this.setState({
        updating: this.state.updating - 1
      });
    });
  },
  onPowerChange: async function (powerLevel) {
    const roomId = this.props.member.roomId;
    const target = this.props.member.userId;
    const room = this.context.getRoom(roomId);
    if (!room) return;
    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevelEvent) return;

    if (!powerLevelEvent.getContent().users) {
      this._applyPowerChange(roomId, target, powerLevel, powerLevelEvent);

      return;
    }

    const myUserId = this.context.getUserId();
    const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog"); // If we are changing our own PL it can only ever be decreasing, which we cannot reverse.

    if (myUserId === target) {
      try {
        if (!(await this._warnSelfDemote())) return;

        this._applyPowerChange(roomId, target, powerLevel, powerLevelEvent);
      } catch (e) {
        console.error("Failed to warn about self demotion: ", e);
      }

      return;
    }

    const myPower = powerLevelEvent.getContent().users[myUserId];

    if (parseInt(myPower) === parseInt(powerLevel)) {
      _Modal.default.createTrackedDialog('Promote to PL100 Warning', '', QuestionDialog, {
        title: (0, _languageHandler._t)("Warning!"),
        description: _react.default.createElement("div", null, (0, _languageHandler._t)("You will not be able to undo this change as you are promoting the user " + "to have the same power level as yourself."), _react.default.createElement("br", null), (0, _languageHandler._t)("Are you sure?")),
        button: (0, _languageHandler._t)("Continue"),
        onFinished: confirmed => {
          if (confirmed) {
            this._applyPowerChange(roomId, target, powerLevel, powerLevelEvent);
          }
        }
      });

      return;
    }

    this._applyPowerChange(roomId, target, powerLevel, powerLevelEvent);
  },
  onNewDMClick: function () {
    this.setState({
      updating: this.state.updating + 1
    });
    (0, _createRoom.default)({
      dmUserId: this.props.member.userId
    }).finally(() => {
      this.setState({
        updating: this.state.updating - 1
      });
    });
  },
  onLeaveClick: function () {
    _dispatcher.default.dispatch({
      action: 'leave_room',
      room_id: this.props.member.roomId
    });
  },
  _calculateOpsPermissions: async function (member) {
    let canDeactivate = false;

    if (this.context) {
      try {
        canDeactivate = await this.context.isSynapseAdministrator();
      } catch (e) {
        console.error(e);
      }
    }

    const defaultPerms = {
      can: {
        // Calculate permissions for Synapse before doing the PL checks
        synapseDeactivate: canDeactivate
      },
      muted: false
    };
    const room = this.context.getRoom(member.roomId);
    if (!room) return defaultPerms;
    const powerLevels = room.currentState.getStateEvents("m.room.power_levels", "");
    if (!powerLevels) return defaultPerms;
    const me = room.getMember(this.context.credentials.userId);
    if (!me) return defaultPerms;
    const them = member;
    return {
      can: _objectSpread({}, defaultPerms.can, {}, (await this._calculateCanPermissions(me, them, powerLevels.getContent()))),
      muted: this._isMuted(them, powerLevels.getContent()),
      isTargetMod: them.powerLevel > powerLevels.getContent().users_default
    };
  },
  _calculateCanPermissions: function (me, them, powerLevels) {
    const isMe = me.userId === them.userId;
    const can = {
      kick: false,
      ban: false,
      mute: false,
      modifyLevel: false,
      modifyLevelMax: 0,
      redactMessages: me.powerLevel >= powerLevels.redact
    };
    const canAffectUser = them.powerLevel < me.powerLevel || isMe;

    if (!canAffectUser) {
      //console.info("Cannot affect user: %s >= %s", them.powerLevel, me.powerLevel);
      return can;
    }

    const editPowerLevel = (powerLevels.events ? powerLevels.events["m.room.power_levels"] : null) || powerLevels.state_default;
    can.kick = me.powerLevel >= powerLevels.kick;
    can.ban = me.powerLevel >= powerLevels.ban;
    can.invite = me.powerLevel >= powerLevels.invite;
    can.mute = me.powerLevel >= editPowerLevel;
    can.modifyLevel = me.powerLevel >= editPowerLevel && (isMe || me.powerLevel > them.powerLevel);
    can.modifyLevelMax = me.powerLevel;
    return can;
  },
  _isMuted: function (member, powerLevelContent) {
    if (!powerLevelContent || !member) return false;
    const levelToSend = (powerLevelContent.events ? powerLevelContent.events["m.room.message"] : null) || powerLevelContent.events_default;
    return member.powerLevel < levelToSend;
  },
  onCancel: function (e) {
    _dispatcher.default.dispatch({
      action: "view_user",
      member: null
    });
  },
  onMemberAvatarClick: function () {
    const member = this.props.member;
    const avatarUrl = member.getMxcAvatarUrl();
    if (!avatarUrl) return;
    const httpUrl = this.context.mxcUrlToHttp(avatarUrl);
    const ImageView = sdk.getComponent("elements.ImageView");
    const params = {
      src: httpUrl,
      name: member.name
    };

    _Modal.default.createDialog(ImageView, params, "mx_Dialog_lightbox");
  },

  onRoomTileClick(roomId) {
    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: roomId
    });
  },

  _renderDevices: function () {
    if (!this._enableDevices) return null;
    const devices = this.state.devices;
    const MemberDeviceInfo = sdk.getComponent('rooms.MemberDeviceInfo');
    const Spinner = sdk.getComponent("elements.Spinner");
    let devComponents;

    if (this.state.devicesLoading) {
      // still loading
      devComponents = _react.default.createElement(Spinner, null);
    } else if (devices === null) {
      devComponents = (0, _languageHandler._t)("Unable to load session list");
    } else if (devices.length === 0) {
      devComponents = (0, _languageHandler._t)("No sessions with registered encryption keys");
    } else {
      devComponents = [];

      for (let i = 0; i < devices.length; i++) {
        devComponents.push(_react.default.createElement(MemberDeviceInfo, {
          key: i,
          userId: this.props.member.userId,
          device: devices[i]
        }));
      }
    }

    return _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)("Sessions")), _react.default.createElement("div", {
      className: "mx_MemberInfo_devices"
    }, devComponents));
  },
  onShareUserClick: function () {
    const ShareDialog = sdk.getComponent("dialogs.ShareDialog");

    _Modal.default.createTrackedDialog('share room member dialog', '', ShareDialog, {
      target: this.props.member
    });
  },
  _renderUserOptions: function () {
    const cli = this.context;
    const member = this.props.member;
    let ignoreButton = null;
    let insertPillButton = null;
    let inviteUserButton = null;
    let readReceiptButton = null; // Only allow the user to ignore the user if its not ourselves
    // same goes for jumping to read receipt

    if (member.userId !== cli.getUserId()) {
      ignoreButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onIgnoreToggle,
        className: "mx_MemberInfo_field"
      }, this.state.isIgnoring ? (0, _languageHandler._t)("Unignore") : (0, _languageHandler._t)("Ignore"));

      if (member.roomId) {
        const room = cli.getRoom(member.roomId);
        const eventId = room.getEventReadUpTo(member.userId);

        const onReadReceiptButton = function () {
          _dispatcher.default.dispatch({
            action: 'view_room',
            highlighted: true,
            event_id: eventId,
            room_id: member.roomId
          });
        };

        const onInsertPillButton = function () {
          _dispatcher.default.dispatch({
            action: 'insert_mention',
            user_id: member.userId
          });
        };

        readReceiptButton = _react.default.createElement(_AccessibleButton.default, {
          onClick: onReadReceiptButton,
          className: "mx_MemberInfo_field"
        }, (0, _languageHandler._t)('Jump to read receipt'));
        insertPillButton = _react.default.createElement(_AccessibleButton.default, {
          onClick: onInsertPillButton,
          className: "mx_MemberInfo_field"
        }, (0, _languageHandler._t)('Mention'));
      }

      if (this.state.can.invite && (!member || !member.membership || member.membership === 'leave')) {
        const roomId = member && member.roomId ? member.roomId : _RoomViewStore.default.getRoomId();

        const onInviteUserButton = async () => {
          try {
            // We use a MultiInviter to re-use the invite logic, even though
            // we're only inviting one user.
            const inviter = new _MultiInviter.default(roomId);
            await inviter.invite([member.userId]).then(() => {
              if (inviter.getCompletionState(member.userId) !== "invited") throw new Error(inviter.getErrorText(member.userId));
            });
          } catch (err) {
            const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

            _Modal.default.createTrackedDialog('Failed to invite', '', ErrorDialog, {
              title: (0, _languageHandler._t)('Failed to invite'),
              description: err && err.message ? err.message : (0, _languageHandler._t)("Operation failed")
            });
          }
        };

        inviteUserButton = _react.default.createElement(_AccessibleButton.default, {
          onClick: onInviteUserButton,
          className: "mx_MemberInfo_field"
        }, (0, _languageHandler._t)('Invite'));
      }
    }

    const shareUserButton = _react.default.createElement(_AccessibleButton.default, {
      onClick: this.onShareUserClick,
      className: "mx_MemberInfo_field"
    }, (0, _languageHandler._t)('Share Link to User'));

    return _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)("User Options")), _react.default.createElement("div", {
      className: "mx_MemberInfo_buttons"
    }, readReceiptButton, shareUserButton, insertPillButton, ignoreButton, inviteUserButton));
  },
  render: function () {
    let startChat;
    let kickButton;
    let banButton;
    let muteButton;
    let giveModButton;
    let redactButton;
    let synapseDeactivateButton;
    let spinner;

    if (this.props.member.userId !== this.context.credentials.userId) {
      // TODO: Immutable DMs replaces a lot of this
      const dmRoomMap = new _DMRoomMap.default(this.context); // dmRooms will not include dmRooms that we have been invited into but did not join.
      // Because DMRoomMap runs off account_data[m.direct] which is only set on join of dm room.
      // XXX: we potentially want DMs we have been invited to, to also show up here :L
      // especially as logic below concerns specially if we haven't joined but have been invited

      const dmRooms = dmRoomMap.getDMRoomsForUserId(this.props.member.userId);
      const RoomTile = sdk.getComponent("rooms.RoomTile");
      const tiles = [];

      for (const roomId of dmRooms) {
        const room = this.context.getRoom(roomId);

        if (room) {
          const myMembership = room.getMyMembership(); // not a DM room if we have are not joined

          if (myMembership !== 'join') continue;
          const them = this.props.member; // not a DM room if they are not joined

          if (!them.membership || them.membership !== 'join') continue;
          const highlight = room.getUnreadNotificationCount('highlight') > 0;
          tiles.push(_react.default.createElement(RoomTile, {
            key: room.roomId,
            room: room,
            transparent: true,
            collapsed: false,
            selected: false,
            unread: Unread.doesRoomHaveUnreadMessages(room),
            highlight: highlight,
            isInvite: false,
            onClick: this.onRoomTileClick
          }));
        }
      }

      const labelClasses = (0, _classnames.default)({
        mx_MemberInfo_createRoom_label: true,
        mx_RoomTile_name: true
      });

      let startNewChat = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_createRoom",
        onClick: this.onNewDMClick
      }, _react.default.createElement("div", {
        className: "mx_RoomTile_avatar"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/create-big.svg"),
        width: "26",
        height: "26"
      })), _react.default.createElement("div", {
        className: labelClasses
      }, _react.default.createElement("i", null, (0, _languageHandler._t)("Start a chat"))));

      if (tiles.length > 0) startNewChat = null; // Don't offer a button for a new chat if we have one.

      startChat = _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)("Direct chats")), tiles, startNewChat);
    }

    if (this.state.updating) {
      const Loader = sdk.getComponent("elements.Spinner");
      spinner = _react.default.createElement(Loader, {
        imgClassName: "mx_ContextualMenu_spinner"
      });
    }

    if (this.state.can.kick) {
      const membership = this.props.member.membership;
      const kickLabel = membership === "invite" ? (0, _languageHandler._t)("Disinvite") : (0, _languageHandler._t)("Kick");
      kickButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_field",
        onClick: this.onKick
      }, kickLabel);
    }

    if (this.state.can.redactMessages) {
      redactButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_field",
        onClick: this.onRedactAllMessages
      }, (0, _languageHandler._t)("Remove recent messages"));
    }

    if (this.state.can.ban) {
      let label = (0, _languageHandler._t)("Ban");

      if (this.props.member.membership === 'ban') {
        label = (0, _languageHandler._t)("Unban");
      }

      banButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_field",
        onClick: this.onBanOrUnban
      }, label);
    }

    if (this.state.can.mute) {
      const muteLabel = this.state.muted ? (0, _languageHandler._t)("Unmute") : (0, _languageHandler._t)("Mute");
      muteButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_field",
        onClick: this.onMuteToggle
      }, muteLabel);
    }

    if (this.state.can.toggleMod) {
      const giveOpLabel = this.state.isTargetMod ? (0, _languageHandler._t)("Revoke Moderator") : (0, _languageHandler._t)("Make Moderator");
      giveModButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_field",
        onClick: this.onModToggle
      }, giveOpLabel);
    } // We don't need a perfect check here, just something to pass as "probably not our homeserver". If
    // someone does figure out how to bypass this check the worst that happens is an error.


    const sameHomeserver = this.props.member.userId.endsWith(":".concat(_MatrixClientPeg.MatrixClientPeg.getHomeserverName()));

    if (this.state.can.synapseDeactivate && sameHomeserver) {
      synapseDeactivateButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onSynapseDeactivate,
        className: "mx_MemberInfo_field"
      }, (0, _languageHandler._t)("Deactivate user"));
    }

    let adminTools;

    if (kickButton || banButton || muteButton || giveModButton || synapseDeactivateButton || redactButton) {
      adminTools = _react.default.createElement("div", null, _react.default.createElement("h3", null, (0, _languageHandler._t)("Admin Tools")), _react.default.createElement("div", {
        className: "mx_MemberInfo_buttons"
      }, muteButton, kickButton, banButton, redactButton, giveModButton, synapseDeactivateButton));
    }

    const memberName = this.props.member.name;
    let presenceState;
    let presenceLastActiveAgo;
    let presenceCurrentlyActive;
    let statusMessage;

    if (this.props.member.user) {
      presenceState = this.props.member.user.presence;
      presenceLastActiveAgo = this.props.member.user.lastActiveAgo;
      presenceCurrentlyActive = this.props.member.user.currentlyActive;

      if (_SettingsStore.default.isFeatureEnabled("feature_custom_status")) {
        statusMessage = this.props.member.user._unstable_statusMessage;
      }
    }

    const room = this.context.getRoom(this.props.member.roomId);
    const powerLevelEvent = room ? room.currentState.getStateEvents("m.room.power_levels", "") : null;
    const powerLevelUsersDefault = powerLevelEvent ? powerLevelEvent.getContent().users_default : 0;

    const enablePresenceByHsUrl = _SdkConfig.default.get()["enable_presence_by_hs_url"];

    const hsUrl = this.context.baseUrl;
    let showPresence = true;

    if (enablePresenceByHsUrl && enablePresenceByHsUrl[hsUrl] !== undefined) {
      showPresence = enablePresenceByHsUrl[hsUrl];
    }

    let presenceLabel = null;

    if (showPresence) {
      const PresenceLabel = sdk.getComponent('rooms.PresenceLabel');
      presenceLabel = _react.default.createElement(PresenceLabel, {
        activeAgo: presenceLastActiveAgo,
        currentlyActive: presenceCurrentlyActive,
        presenceState: presenceState
      });
    }

    let statusLabel = null;

    if (statusMessage) {
      statusLabel = _react.default.createElement("span", {
        className: "mx_MemberInfo_statusMessage"
      }, statusMessage);
    }

    let roomMemberDetails = null;
    let e2eIconElement;

    if (this.props.member.roomId) {
      // is in room
      const PowerSelector = sdk.getComponent('elements.PowerSelector');
      roomMemberDetails = _react.default.createElement("div", null, _react.default.createElement("div", {
        className: "mx_MemberInfo_profileField"
      }, _react.default.createElement(PowerSelector, {
        value: parseInt(this.props.member.powerLevel),
        maxValue: this.state.can.modifyLevelMax,
        disabled: !this.state.can.modifyLevel,
        usersDefault: powerLevelUsersDefault,
        onChange: this.onPowerChange
      })), _react.default.createElement("div", {
        className: "mx_MemberInfo_profileField"
      }, presenceLabel, statusLabel));
      const isEncrypted = this.context.isRoomEncrypted(this.props.member.roomId);

      if (this.state.e2eStatus && isEncrypted) {
        e2eIconElement = _react.default.createElement(_E2EIcon.default, {
          status: this.state.e2eStatus,
          isUser: true
        });
      }
    }

    const {
      member
    } = this.props;
    const avatarUrl = member.avatarUrl || member.getMxcAvatarUrl && member.getMxcAvatarUrl();
    let avatarElement;

    if (avatarUrl) {
      const httpUrl = this.context.mxcUrlToHttp(avatarUrl, 800, 800);
      avatarElement = _react.default.createElement("div", {
        className: "mx_MemberInfo_avatar"
      }, _react.default.createElement("img", {
        src: httpUrl
      }));
    }

    let backButton;

    if (this.props.member.roomId) {
      backButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_MemberInfo_cancel",
        onClick: this.onCancel,
        title: (0, _languageHandler._t)('Close')
      });
    }

    return _react.default.createElement("div", {
      className: "mx_MemberInfo",
      role: "tabpanel"
    }, _react.default.createElement("div", {
      className: "mx_MemberInfo_name"
    }, backButton, e2eIconElement, _react.default.createElement("h2", null, memberName)), avatarElement, _react.default.createElement("div", {
      className: "mx_MemberInfo_container"
    }, _react.default.createElement("div", {
      className: "mx_MemberInfo_profile"
    }, _react.default.createElement("div", {
      className: "mx_MemberInfo_profileField"
    }, this.props.member.userId), roomMemberDetails)), _react.default.createElement(_AutoHideScrollbar.default, {
      className: "mx_MemberInfo_scrollContainer"
    }, _react.default.createElement("div", {
      className: "mx_MemberInfo_container"
    }, this._renderUserOptions(), adminTools, startChat, this._renderDevices(), spinner)));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL01lbWJlckluZm8uanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJtZW1iZXIiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZ2V0SW5pdGlhbFN0YXRlIiwiY2FuIiwia2ljayIsImJhbiIsIm11dGUiLCJtb2RpZnlMZXZlbCIsInN5bmFwc2VEZWFjdGl2YXRlIiwicmVkYWN0TWVzc2FnZXMiLCJtdXRlZCIsImlzVGFyZ2V0TW9kIiwidXBkYXRpbmciLCJkZXZpY2VzTG9hZGluZyIsImRldmljZXMiLCJpc0lnbm9yaW5nIiwic3RhdGljcyIsImNvbnRleHRUeXBlIiwiTWF0cml4Q2xpZW50Q29udGV4dCIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJfY2FuY2VsRGV2aWNlTGlzdCIsImNsaSIsImNvbnRleHQiLCJfZW5hYmxlRGV2aWNlcyIsImlzQ3J5cHRvRW5hYmxlZCIsIm9uIiwib25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkIiwib25Sb29tIiwib25EZWxldGVSb29tIiwib25Sb29tVGltZWxpbmUiLCJvblJvb21OYW1lIiwib25Sb29tUmVjZWlwdCIsIm9uUm9vbVN0YXRlRXZlbnRzIiwib25Sb29tTWVtYmVyTmFtZSIsIm9uUm9vbU1lbWJlck1lbWJlcnNoaXAiLCJvbkFjY291bnREYXRhIiwiX2NoZWNrSWdub3JlU3RhdGUiLCJfdXBkYXRlU3RhdGVGb3JOZXdNZW1iZXIiLCJwcm9wcyIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV3UHJvcHMiLCJ1c2VySWQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNsaWVudCIsInJlbW92ZUxpc3RlbmVyIiwiaXNVc2VySWdub3JlZCIsInNldFN0YXRlIiwiX2Rpc2FtYmlndWF0ZURldmljZXMiLCJuYW1lcyIsIk9iamVjdCIsImNyZWF0ZSIsImkiLCJsZW5ndGgiLCJuYW1lIiwiZ2V0RGlzcGxheU5hbWUiLCJpbmRleExpc3QiLCJwdXNoIiwiZm9yRWFjaCIsImoiLCJhbWJpZ3VvdXMiLCJkZXZpY2UiLCJQcm9taXNlIiwicmVzb2x2ZSIsImdldFN0b3JlZERldmljZXNGb3JVc2VyIiwidGhlbiIsImUyZVN0YXR1cyIsIl9nZXRFMkVTdGF0dXMiLCJoYXNVbnZlcmlmaWVkRGV2aWNlIiwic29tZSIsImlzVW52ZXJpZmllZCIsInJvb20iLCJmb3JjZVVwZGF0ZSIsInJvb21JZCIsImV2IiwidG9TdGFydE9mVGltZWxpbmUiLCJyZWNlaXB0RXZlbnQiLCJjcmVkZW50aWFscyIsInN0YXRlIiwiZ2V0VHlwZSIsIm5ld1N0YXRlIiwiX2NhbGN1bGF0ZU9wc1Blcm1pc3Npb25zIiwiX2Rvd25sb2FkRGV2aWNlTGlzdCIsImNhbmNlbGxlZCIsInNlbGYiLCJkb3dubG9hZEtleXMiLCJmaW5hbGx5IiwiZXJyIiwiY29uc29sZSIsImxvZyIsIm9uSWdub3JlVG9nZ2xlIiwiaWdub3JlZFVzZXJzIiwiZ2V0SWdub3JlZFVzZXJzIiwiaW5kZXgiLCJpbmRleE9mIiwic3BsaWNlIiwic2V0SWdub3JlZFVzZXJzIiwib25LaWNrIiwibWVtYmVyc2hpcCIsIkNvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwiYWN0aW9uIiwidGl0bGUiLCJhc2tSZWFzb24iLCJkYW5nZXIiLCJvbkZpbmlzaGVkIiwicHJvY2VlZCIsInJlYXNvbiIsInVuZGVmaW5lZCIsIkVycm9yRGlhbG9nIiwiZXJyb3IiLCJkZXNjcmlwdGlvbiIsIm1lc3NhZ2UiLCJvbkJhbk9yVW5iYW4iLCJwcm9taXNlIiwidW5iYW4iLCJvblJlZGFjdEFsbE1lc3NhZ2VzIiwiZ2V0Um9vbSIsInRpbWVsaW5lU2V0IiwiZ2V0VW5maWx0ZXJlZFRpbWVsaW5lU2V0IiwiZXZlbnRzVG9SZWRhY3QiLCJ0aW1lbGluZSIsImdldFRpbWVsaW5lcyIsImdldEV2ZW50cyIsInJlZHVjZSIsImV2ZW50cyIsImV2ZW50IiwiZ2V0U2VuZGVyIiwiaXNSZWRhY3RlZCIsImNvbmNhdCIsImNvdW50IiwidXNlciIsIkluZm9EaWFsb2ciLCJRdWVzdGlvbkRpYWxvZyIsImNvbmZpcm1lZCIsImJ1dHRvbiIsImluZm8iLCJhbGwiLCJtYXAiLCJyZWRhY3RFdmVudCIsImdldElkIiwiX3dhcm5TZWxmRGVtb3RlIiwib25NdXRlVG9nZ2xlIiwidGFyZ2V0IiwiZ2V0VXNlcklkIiwiZSIsInBvd2VyTGV2ZWxFdmVudCIsImN1cnJlbnRTdGF0ZSIsImdldFN0YXRlRXZlbnRzIiwiaXNNdXRlZCIsInBvd2VyTGV2ZWxzIiwiZ2V0Q29udGVudCIsImxldmVsVG9TZW5kIiwiZXZlbnRzX2RlZmF1bHQiLCJsZXZlbCIsInBhcnNlSW50IiwiaXNOYU4iLCJzZXRQb3dlckxldmVsIiwib25Nb2RUb2dnbGUiLCJtZSIsImdldE1lbWJlciIsImRlZmF1bHRMZXZlbCIsInVzZXJzX2RlZmF1bHQiLCJtb2RMZXZlbCIsInBvd2VyTGV2ZWwiLCJuZXdMZXZlbCIsImVycmNvZGUiLCJkaXMiLCJkaXNwYXRjaCIsIm9uU3luYXBzZURlYWN0aXZhdGUiLCJhY2NlcHRlZCIsImRlYWN0aXZhdGVTeW5hcHNlVXNlciIsImNhdGNoIiwiX2FwcGx5UG93ZXJDaGFuZ2UiLCJvblBvd2VyQ2hhbmdlIiwidXNlcnMiLCJteVVzZXJJZCIsIm15UG93ZXIiLCJvbk5ld0RNQ2xpY2siLCJkbVVzZXJJZCIsIm9uTGVhdmVDbGljayIsInJvb21faWQiLCJjYW5EZWFjdGl2YXRlIiwiaXNTeW5hcHNlQWRtaW5pc3RyYXRvciIsImRlZmF1bHRQZXJtcyIsInRoZW0iLCJfY2FsY3VsYXRlQ2FuUGVybWlzc2lvbnMiLCJfaXNNdXRlZCIsImlzTWUiLCJtb2RpZnlMZXZlbE1heCIsInJlZGFjdCIsImNhbkFmZmVjdFVzZXIiLCJlZGl0UG93ZXJMZXZlbCIsInN0YXRlX2RlZmF1bHQiLCJpbnZpdGUiLCJwb3dlckxldmVsQ29udGVudCIsIm9uQ2FuY2VsIiwib25NZW1iZXJBdmF0YXJDbGljayIsImF2YXRhclVybCIsImdldE14Y0F2YXRhclVybCIsImh0dHBVcmwiLCJteGNVcmxUb0h0dHAiLCJJbWFnZVZpZXciLCJwYXJhbXMiLCJzcmMiLCJjcmVhdGVEaWFsb2ciLCJvblJvb21UaWxlQ2xpY2siLCJfcmVuZGVyRGV2aWNlcyIsIk1lbWJlckRldmljZUluZm8iLCJTcGlubmVyIiwiZGV2Q29tcG9uZW50cyIsIm9uU2hhcmVVc2VyQ2xpY2siLCJTaGFyZURpYWxvZyIsIl9yZW5kZXJVc2VyT3B0aW9ucyIsImlnbm9yZUJ1dHRvbiIsImluc2VydFBpbGxCdXR0b24iLCJpbnZpdGVVc2VyQnV0dG9uIiwicmVhZFJlY2VpcHRCdXR0b24iLCJldmVudElkIiwiZ2V0RXZlbnRSZWFkVXBUbyIsIm9uUmVhZFJlY2VpcHRCdXR0b24iLCJoaWdobGlnaHRlZCIsImV2ZW50X2lkIiwib25JbnNlcnRQaWxsQnV0dG9uIiwidXNlcl9pZCIsIlJvb21WaWV3U3RvcmUiLCJnZXRSb29tSWQiLCJvbkludml0ZVVzZXJCdXR0b24iLCJpbnZpdGVyIiwiTXVsdGlJbnZpdGVyIiwiZ2V0Q29tcGxldGlvblN0YXRlIiwiRXJyb3IiLCJnZXRFcnJvclRleHQiLCJzaGFyZVVzZXJCdXR0b24iLCJyZW5kZXIiLCJzdGFydENoYXQiLCJraWNrQnV0dG9uIiwiYmFuQnV0dG9uIiwibXV0ZUJ1dHRvbiIsImdpdmVNb2RCdXR0b24iLCJyZWRhY3RCdXR0b24iLCJzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiIsInNwaW5uZXIiLCJkbVJvb21NYXAiLCJETVJvb21NYXAiLCJkbVJvb21zIiwiZ2V0RE1Sb29tc0ZvclVzZXJJZCIsIlJvb21UaWxlIiwidGlsZXMiLCJteU1lbWJlcnNoaXAiLCJnZXRNeU1lbWJlcnNoaXAiLCJoaWdobGlnaHQiLCJnZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCIsIlVucmVhZCIsImRvZXNSb29tSGF2ZVVucmVhZE1lc3NhZ2VzIiwibGFiZWxDbGFzc2VzIiwibXhfTWVtYmVySW5mb19jcmVhdGVSb29tX2xhYmVsIiwibXhfUm9vbVRpbGVfbmFtZSIsInN0YXJ0TmV3Q2hhdCIsInJlcXVpcmUiLCJMb2FkZXIiLCJraWNrTGFiZWwiLCJsYWJlbCIsIm11dGVMYWJlbCIsInRvZ2dsZU1vZCIsImdpdmVPcExhYmVsIiwic2FtZUhvbWVzZXJ2ZXIiLCJlbmRzV2l0aCIsIk1hdHJpeENsaWVudFBlZyIsImdldEhvbWVzZXJ2ZXJOYW1lIiwiYWRtaW5Ub29scyIsIm1lbWJlck5hbWUiLCJwcmVzZW5jZVN0YXRlIiwicHJlc2VuY2VMYXN0QWN0aXZlQWdvIiwicHJlc2VuY2VDdXJyZW50bHlBY3RpdmUiLCJzdGF0dXNNZXNzYWdlIiwicHJlc2VuY2UiLCJsYXN0QWN0aXZlQWdvIiwiY3VycmVudGx5QWN0aXZlIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJfdW5zdGFibGVfc3RhdHVzTWVzc2FnZSIsInBvd2VyTGV2ZWxVc2Vyc0RlZmF1bHQiLCJlbmFibGVQcmVzZW5jZUJ5SHNVcmwiLCJTZGtDb25maWciLCJnZXQiLCJoc1VybCIsImJhc2VVcmwiLCJzaG93UHJlc2VuY2UiLCJwcmVzZW5jZUxhYmVsIiwiUHJlc2VuY2VMYWJlbCIsInN0YXR1c0xhYmVsIiwicm9vbU1lbWJlckRldGFpbHMiLCJlMmVJY29uRWxlbWVudCIsIlBvd2VyU2VsZWN0b3IiLCJpc0VuY3J5cHRlZCIsImlzUm9vbUVuY3J5cHRlZCIsImF2YXRhckVsZW1lbnQiLCJiYWNrQnV0dG9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBNkJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7ZUFFZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxZQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsTUFBTSxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkM7QUFEbEIsR0FIaUI7QUFPNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsR0FBRyxFQUFFO0FBQ0RDLFFBQUFBLElBQUksRUFBRSxLQURMO0FBRURDLFFBQUFBLEdBQUcsRUFBRSxLQUZKO0FBR0RDLFFBQUFBLElBQUksRUFBRSxLQUhMO0FBSURDLFFBQUFBLFdBQVcsRUFBRSxLQUpaO0FBS0RDLFFBQUFBLGlCQUFpQixFQUFFLEtBTGxCO0FBTURDLFFBQUFBLGNBQWMsRUFBRTtBQU5mLE9BREY7QUFTSEMsTUFBQUEsS0FBSyxFQUFFLEtBVEo7QUFVSEMsTUFBQUEsV0FBVyxFQUFFLEtBVlY7QUFXSEMsTUFBQUEsUUFBUSxFQUFFLENBWFA7QUFZSEMsTUFBQUEsY0FBYyxFQUFFLElBWmI7QUFhSEMsTUFBQUEsT0FBTyxFQUFFLElBYk47QUFjSEMsTUFBQUEsVUFBVSxFQUFFO0FBZFQsS0FBUDtBQWdCSCxHQXhCMkI7QUEwQjVCQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsV0FBVyxFQUFFQztBQURSLEdBMUJtQjtBQThCNUI7QUFDQUMsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLQyxpQkFBTCxHQUF5QixJQUF6QjtBQUNBLFVBQU1DLEdBQUcsR0FBRyxLQUFLQyxPQUFqQixDQUZrQyxDQUlsQzs7QUFDQSxTQUFLQyxjQUFMLEdBQXNCRixHQUFHLENBQUNHLGVBQUosRUFBdEI7QUFFQUgsSUFBQUEsR0FBRyxDQUFDSSxFQUFKLENBQU8sMkJBQVAsRUFBb0MsS0FBS0MsMkJBQXpDO0FBQ0FMLElBQUFBLEdBQUcsQ0FBQ0ksRUFBSixDQUFPLE1BQVAsRUFBZSxLQUFLRSxNQUFwQjtBQUNBTixJQUFBQSxHQUFHLENBQUNJLEVBQUosQ0FBTyxZQUFQLEVBQXFCLEtBQUtHLFlBQTFCO0FBQ0FQLElBQUFBLEdBQUcsQ0FBQ0ksRUFBSixDQUFPLGVBQVAsRUFBd0IsS0FBS0ksY0FBN0I7QUFDQVIsSUFBQUEsR0FBRyxDQUFDSSxFQUFKLENBQU8sV0FBUCxFQUFvQixLQUFLSyxVQUF6QjtBQUNBVCxJQUFBQSxHQUFHLENBQUNJLEVBQUosQ0FBTyxjQUFQLEVBQXVCLEtBQUtNLGFBQTVCO0FBQ0FWLElBQUFBLEdBQUcsQ0FBQ0ksRUFBSixDQUFPLGtCQUFQLEVBQTJCLEtBQUtPLGlCQUFoQztBQUNBWCxJQUFBQSxHQUFHLENBQUNJLEVBQUosQ0FBTyxpQkFBUCxFQUEwQixLQUFLUSxnQkFBL0I7QUFDQVosSUFBQUEsR0FBRyxDQUFDSSxFQUFKLENBQU8sdUJBQVAsRUFBZ0MsS0FBS1Msc0JBQXJDO0FBQ0FiLElBQUFBLEdBQUcsQ0FBQ0ksRUFBSixDQUFPLGFBQVAsRUFBc0IsS0FBS1UsYUFBM0I7O0FBRUEsU0FBS0MsaUJBQUw7O0FBRUEsU0FBS0Msd0JBQUwsQ0FBOEIsS0FBS0MsS0FBTCxDQUFXeEMsTUFBekM7QUFDSCxHQXBEMkI7QUFzRDVCO0FBQ0F5QyxFQUFBQSxnQ0FBZ0MsRUFBRSxVQUFTQyxRQUFULEVBQW1CO0FBQ2pELFFBQUksS0FBS0YsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BQWxCLEtBQTZCRCxRQUFRLENBQUMxQyxNQUFULENBQWdCMkMsTUFBakQsRUFBeUQ7QUFDckQsV0FBS0osd0JBQUwsQ0FBOEJHLFFBQVEsQ0FBQzFDLE1BQXZDO0FBQ0g7QUFDSixHQTNEMkI7QUE2RDVCNEMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QixVQUFNQyxNQUFNLEdBQUcsS0FBS3JCLE9BQXBCOztBQUNBLFFBQUlxQixNQUFKLEVBQVk7QUFDUkEsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLDJCQUF0QixFQUFtRCxLQUFLbEIsMkJBQXhEO0FBQ0FpQixNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsTUFBdEIsRUFBOEIsS0FBS2pCLE1BQW5DO0FBQ0FnQixNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsWUFBdEIsRUFBb0MsS0FBS2hCLFlBQXpDO0FBQ0FlLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixlQUF0QixFQUF1QyxLQUFLZixjQUE1QztBQUNBYyxNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsV0FBdEIsRUFBbUMsS0FBS2QsVUFBeEM7QUFDQWEsTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLGNBQXRCLEVBQXNDLEtBQUtiLGFBQTNDO0FBQ0FZLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixrQkFBdEIsRUFBMEMsS0FBS1osaUJBQS9DO0FBQ0FXLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixpQkFBdEIsRUFBeUMsS0FBS1gsZ0JBQTlDO0FBQ0FVLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQix1QkFBdEIsRUFBK0MsS0FBS1Ysc0JBQXBEO0FBQ0FTLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixhQUF0QixFQUFxQyxLQUFLVCxhQUExQztBQUNIOztBQUNELFFBQUksS0FBS2YsaUJBQVQsRUFBNEI7QUFDeEIsV0FBS0EsaUJBQUw7QUFDSDtBQUNKLEdBOUUyQjtBQWdGNUJnQixFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFVBQU1yQixVQUFVLEdBQUcsS0FBS08sT0FBTCxDQUFhdUIsYUFBYixDQUEyQixLQUFLUCxLQUFMLENBQVd4QyxNQUFYLENBQWtCMkMsTUFBN0MsQ0FBbkI7QUFDQSxTQUFLSyxRQUFMLENBQWM7QUFBQy9CLE1BQUFBLFVBQVUsRUFBRUE7QUFBYixLQUFkO0FBQ0gsR0FuRjJCO0FBcUY1QmdDLEVBQUFBLG9CQUFvQixFQUFFLFVBQVNqQyxPQUFULEVBQWtCO0FBQ3BDLFVBQU1rQyxLQUFLLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLElBQWQsQ0FBZDs7QUFDQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdyQyxPQUFPLENBQUNzQyxNQUE1QixFQUFvQ0QsQ0FBQyxFQUFyQyxFQUF5QztBQUNyQyxZQUFNRSxJQUFJLEdBQUd2QyxPQUFPLENBQUNxQyxDQUFELENBQVAsQ0FBV0csY0FBWCxFQUFiO0FBQ0EsWUFBTUMsU0FBUyxHQUFHUCxLQUFLLENBQUNLLElBQUQsQ0FBTCxJQUFlLEVBQWpDO0FBQ0FFLE1BQUFBLFNBQVMsQ0FBQ0MsSUFBVixDQUFlTCxDQUFmO0FBQ0FILE1BQUFBLEtBQUssQ0FBQ0ssSUFBRCxDQUFMLEdBQWNFLFNBQWQ7QUFDSDs7QUFDRCxTQUFLLE1BQU1GLElBQVgsSUFBbUJMLEtBQW5CLEVBQTBCO0FBQ3RCLFVBQUlBLEtBQUssQ0FBQ0ssSUFBRCxDQUFMLENBQVlELE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDeEJKLFFBQUFBLEtBQUssQ0FBQ0ssSUFBRCxDQUFMLENBQVlJLE9BQVosQ0FBcUJDLENBQUQsSUFBSztBQUNyQjVDLFVBQUFBLE9BQU8sQ0FBQzRDLENBQUQsQ0FBUCxDQUFXQyxTQUFYLEdBQXVCLElBQXZCO0FBQ0gsU0FGRDtBQUdIO0FBQ0o7QUFDSixHQXBHMkI7QUFzRzVCakMsRUFBQUEsMkJBQTJCLEVBQUUsVUFBU2UsTUFBVCxFQUFpQm1CLE1BQWpCLEVBQXlCO0FBQ2xELFFBQUksQ0FBQyxLQUFLckMsY0FBVixFQUEwQjtBQUN0QjtBQUNIOztBQUVELFFBQUlrQixNQUFNLEtBQUssS0FBS0gsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BQWpDLEVBQXlDO0FBQ3JDO0FBQ0E7QUFFQTtBQUNBO0FBQ0FvQixNQUFBQSxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsS0FBS3hDLE9BQUwsQ0FBYXlDLHVCQUFiLENBQXFDdEIsTUFBckMsQ0FBaEIsRUFBOER1QixJQUE5RCxDQUFvRWxELE9BQUQsSUFBYTtBQUM1RSxhQUFLZ0MsUUFBTCxDQUFjO0FBQ1ZoQyxVQUFBQSxPQUFPLEVBQUVBLE9BREM7QUFFVm1ELFVBQUFBLFNBQVMsRUFBRSxLQUFLQyxhQUFMLENBQW1CcEQsT0FBbkI7QUFGRCxTQUFkO0FBSUgsT0FMRDtBQU1IO0FBQ0osR0F4SDJCO0FBMEg1Qm9ELEVBQUFBLGFBQWEsRUFBRSxVQUFTcEQsT0FBVCxFQUFrQjtBQUM3QixVQUFNcUQsbUJBQW1CLEdBQUdyRCxPQUFPLENBQUNzRCxJQUFSLENBQWNSLE1BQUQsSUFBWUEsTUFBTSxDQUFDUyxZQUFQLEVBQXpCLENBQTVCO0FBQ0EsV0FBT0YsbUJBQW1CLEdBQUcsU0FBSCxHQUFlLFVBQXpDO0FBQ0gsR0E3SDJCO0FBK0g1QnhDLEVBQUFBLE1BQU0sRUFBRSxVQUFTMkMsSUFBVCxFQUFlO0FBQ25CLFNBQUtDLFdBQUw7QUFDSCxHQWpJMkI7QUFtSTVCM0MsRUFBQUEsWUFBWSxFQUFFLFVBQVM0QyxNQUFULEVBQWlCO0FBQzNCLFNBQUtELFdBQUw7QUFDSCxHQXJJMkI7QUF1STVCMUMsRUFBQUEsY0FBYyxFQUFFLFVBQVM0QyxFQUFULEVBQWFILElBQWIsRUFBbUJJLGlCQUFuQixFQUFzQztBQUNsRCxRQUFJQSxpQkFBSixFQUF1QjtBQUN2QixTQUFLSCxXQUFMO0FBQ0gsR0ExSTJCO0FBNEk1QnpDLEVBQUFBLFVBQVUsRUFBRSxVQUFTd0MsSUFBVCxFQUFlO0FBQ3ZCLFNBQUtDLFdBQUw7QUFDSCxHQTlJMkI7QUFnSjVCeEMsRUFBQUEsYUFBYSxFQUFFLFVBQVM0QyxZQUFULEVBQXVCTCxJQUF2QixFQUE2QjtBQUN4QztBQUNBO0FBQ0EsUUFBSSx3Q0FBMEJLLFlBQTFCLEVBQXdDLEtBQUtyRCxPQUFMLENBQWFzRCxXQUFiLENBQXlCbkMsTUFBakUsQ0FBSixFQUE4RTtBQUMxRSxXQUFLOEIsV0FBTDtBQUNIO0FBQ0osR0F0SjJCO0FBd0o1QnZDLEVBQUFBLGlCQUFpQixFQUFFLFVBQVN5QyxFQUFULEVBQWFJLEtBQWIsRUFBb0I7QUFDbkMsU0FBS04sV0FBTDtBQUNILEdBMUoyQjtBQTRKNUJ0QyxFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTd0MsRUFBVCxFQUFhM0UsTUFBYixFQUFxQjtBQUNuQyxTQUFLeUUsV0FBTDtBQUNILEdBOUoyQjtBQWdLNUJyQyxFQUFBQSxzQkFBc0IsRUFBRSxVQUFTdUMsRUFBVCxFQUFhM0UsTUFBYixFQUFxQjtBQUN6QyxRQUFJLEtBQUt3QyxLQUFMLENBQVd4QyxNQUFYLENBQWtCMkMsTUFBbEIsS0FBNkIzQyxNQUFNLENBQUMyQyxNQUF4QyxFQUFnRCxLQUFLOEIsV0FBTDtBQUNuRCxHQWxLMkI7QUFvSzVCcEMsRUFBQUEsYUFBYSxFQUFFLFVBQVNzQyxFQUFULEVBQWE7QUFDeEIsUUFBSUEsRUFBRSxDQUFDSyxPQUFILE9BQWlCLFVBQXJCLEVBQWlDO0FBQzdCLFdBQUtQLFdBQUw7QUFDSDtBQUNKLEdBeEsyQjtBQTBLNUJsQyxFQUFBQSx3QkFBd0IsRUFBRSxnQkFBZXZDLE1BQWYsRUFBdUI7QUFDN0MsVUFBTWlGLFFBQVEsR0FBRyxNQUFNLEtBQUtDLHdCQUFMLENBQThCbEYsTUFBOUIsQ0FBdkI7QUFDQWlGLElBQUFBLFFBQVEsQ0FBQ2xFLGNBQVQsR0FBMEIsSUFBMUI7QUFDQWtFLElBQUFBLFFBQVEsQ0FBQ2pFLE9BQVQsR0FBbUIsSUFBbkI7QUFDQSxTQUFLZ0MsUUFBTCxDQUFjaUMsUUFBZDs7QUFFQSxRQUFJLEtBQUszRCxpQkFBVCxFQUE0QjtBQUN4QixXQUFLQSxpQkFBTDs7QUFDQSxXQUFLQSxpQkFBTCxHQUF5QixJQUF6QjtBQUNIOztBQUVELFNBQUs2RCxtQkFBTCxDQUF5Qm5GLE1BQXpCO0FBQ0gsR0F0TDJCO0FBd0w1Qm1GLEVBQUFBLG1CQUFtQixFQUFFLFVBQVNuRixNQUFULEVBQWlCO0FBQ2xDLFFBQUksQ0FBQyxLQUFLeUIsY0FBVixFQUEwQjtBQUN0QjtBQUNIOztBQUVELFFBQUkyRCxTQUFTLEdBQUcsS0FBaEI7O0FBQ0EsU0FBSzlELGlCQUFMLEdBQXlCLFlBQVc7QUFBRThELE1BQUFBLFNBQVMsR0FBRyxJQUFaO0FBQW1CLEtBQXpEOztBQUVBLFVBQU12QyxNQUFNLEdBQUcsS0FBS3JCLE9BQXBCO0FBQ0EsVUFBTTZELElBQUksR0FBRyxJQUFiO0FBQ0F4QyxJQUFBQSxNQUFNLENBQUN5QyxZQUFQLENBQW9CLENBQUN0RixNQUFNLENBQUMyQyxNQUFSLENBQXBCLEVBQXFDLElBQXJDLEVBQTJDdUIsSUFBM0MsQ0FBZ0QsTUFBTTtBQUNsRCxhQUFPckIsTUFBTSxDQUFDb0IsdUJBQVAsQ0FBK0JqRSxNQUFNLENBQUMyQyxNQUF0QyxDQUFQO0FBQ0gsS0FGRCxFQUVHNEMsT0FGSCxDQUVXLFlBQVc7QUFDbEJGLE1BQUFBLElBQUksQ0FBQy9ELGlCQUFMLEdBQXlCLElBQXpCO0FBQ0gsS0FKRCxFQUlHNEMsSUFKSCxDQUlRLFVBQVNsRCxPQUFULEVBQWtCO0FBQ3RCLFVBQUlvRSxTQUFKLEVBQWU7QUFDWDtBQUNBO0FBQ0g7O0FBRURDLE1BQUFBLElBQUksQ0FBQ3BDLG9CQUFMLENBQTBCakMsT0FBMUI7O0FBQ0FxRSxNQUFBQSxJQUFJLENBQUNyQyxRQUFMLENBQWM7QUFDVmpDLFFBQUFBLGNBQWMsRUFBRSxLQUROO0FBRVZDLFFBQUFBLE9BQU8sRUFBRUEsT0FGQztBQUdWbUQsUUFBQUEsU0FBUyxFQUFFa0IsSUFBSSxDQUFDakIsYUFBTCxDQUFtQnBELE9BQW5CO0FBSEQsT0FBZDtBQUtILEtBaEJELEVBZ0JHLFVBQVN3RSxHQUFULEVBQWM7QUFDYkMsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksNEJBQVosRUFBMENGLEdBQTFDO0FBQ0FILE1BQUFBLElBQUksQ0FBQ3JDLFFBQUwsQ0FBYztBQUFDakMsUUFBQUEsY0FBYyxFQUFFO0FBQWpCLE9BQWQ7QUFDSCxLQW5CRDtBQW9CSCxHQXROMkI7QUF3TjVCNEUsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIsVUFBTUMsWUFBWSxHQUFHLEtBQUtwRSxPQUFMLENBQWFxRSxlQUFiLEVBQXJCOztBQUNBLFFBQUksS0FBS2QsS0FBTCxDQUFXOUQsVUFBZixFQUEyQjtBQUN2QixZQUFNNkUsS0FBSyxHQUFHRixZQUFZLENBQUNHLE9BQWIsQ0FBcUIsS0FBS3ZELEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUF2QyxDQUFkO0FBQ0EsVUFBSW1ELEtBQUssS0FBSyxDQUFDLENBQWYsRUFBa0JGLFlBQVksQ0FBQ0ksTUFBYixDQUFvQkYsS0FBcEIsRUFBMkIsQ0FBM0I7QUFDckIsS0FIRCxNQUdPO0FBQ0hGLE1BQUFBLFlBQVksQ0FBQ2xDLElBQWIsQ0FBa0IsS0FBS2xCLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUFwQztBQUNIOztBQUVELFNBQUtuQixPQUFMLENBQWF5RSxlQUFiLENBQTZCTCxZQUE3QixFQUEyQzFCLElBQTNDLENBQWdELE1BQU07QUFDbEQsYUFBTyxLQUFLbEIsUUFBTCxDQUFjO0FBQUMvQixRQUFBQSxVQUFVLEVBQUUsQ0FBQyxLQUFLOEQsS0FBTCxDQUFXOUQ7QUFBekIsT0FBZCxDQUFQO0FBQ0gsS0FGRDtBQUdILEdBcE8yQjtBQXNPNUJpRixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFVBQVUsR0FBRyxLQUFLM0QsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQm1HLFVBQXJDO0FBQ0EsVUFBTUMsdUJBQXVCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixpQ0FBakIsQ0FBaEM7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQiw0QkFBMUIsRUFBd0QsUUFBeEQsRUFBa0VKLHVCQUFsRSxFQUEyRjtBQUN2RnBHLE1BQUFBLE1BQU0sRUFBRSxLQUFLd0MsS0FBTCxDQUFXeEMsTUFEb0U7QUFFdkZ5RyxNQUFBQSxNQUFNLEVBQUVOLFVBQVUsS0FBSyxRQUFmLEdBQTBCLHlCQUFHLFdBQUgsQ0FBMUIsR0FBNEMseUJBQUcsTUFBSCxDQUZtQztBQUd2Rk8sTUFBQUEsS0FBSyxFQUFFUCxVQUFVLEtBQUssUUFBZixHQUEwQix5QkFBRyxzQkFBSCxDQUExQixHQUF1RCx5QkFBRyxpQkFBSCxDQUh5QjtBQUl2RlEsTUFBQUEsU0FBUyxFQUFFUixVQUFVLEtBQUssTUFKNkQ7QUFLdkZTLE1BQUFBLE1BQU0sRUFBRSxJQUwrRTtBQU12RkMsTUFBQUEsVUFBVSxFQUFFLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUM3QixZQUFJLENBQUNELE9BQUwsRUFBYztBQUVkLGFBQUs5RCxRQUFMLENBQWM7QUFBRWxDLFVBQUFBLFFBQVEsRUFBRSxLQUFLaUUsS0FBTCxDQUFXakUsUUFBWCxHQUFzQjtBQUFsQyxTQUFkO0FBQ0EsYUFBS1UsT0FBTCxDQUFhbEIsSUFBYixDQUNJLEtBQUtrQyxLQUFMLENBQVd4QyxNQUFYLENBQWtCMEUsTUFEdEIsRUFDOEIsS0FBS2xDLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQURoRCxFQUVJb0UsTUFBTSxJQUFJQyxTQUZkLEVBR0U5QyxJQUhGLENBR08sWUFBVztBQUNWO0FBQ0E7QUFDQXVCLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGNBQVo7QUFDSCxTQVBMLEVBT08sVUFBU0YsR0FBVCxFQUFjO0FBQ2IsZ0JBQU15QixXQUFXLEdBQUdaLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7QUFDQWIsVUFBQUEsT0FBTyxDQUFDeUIsS0FBUixDQUFjLGlCQUFpQjFCLEdBQS9COztBQUNBZSx5QkFBTUMsbUJBQU4sQ0FBMEIsZ0JBQTFCLEVBQTRDLEVBQTVDLEVBQWdEUyxXQUFoRCxFQUE2RDtBQUN6RFAsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGdCQUFILENBRGtEO0FBRXpEUyxZQUFBQSxXQUFXLEVBQUkzQixHQUFHLElBQUlBLEdBQUcsQ0FBQzRCLE9BQVosR0FBdUI1QixHQUFHLENBQUM0QixPQUEzQixHQUFxQztBQUZNLFdBQTdEO0FBSUgsU0FkTCxFQWVFN0IsT0FmRixDQWVVLE1BQUk7QUFDVixlQUFLdkMsUUFBTCxDQUFjO0FBQUVsQyxZQUFBQSxRQUFRLEVBQUUsS0FBS2lFLEtBQUwsQ0FBV2pFLFFBQVgsR0FBc0I7QUFBbEMsV0FBZDtBQUNILFNBakJEO0FBa0JIO0FBNUJzRixLQUEzRjtBQThCSCxHQXZRMkI7QUF5UTVCdUcsRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckIsVUFBTWpCLHVCQUF1QixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsaUNBQWpCLENBQWhDOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsNEJBQTFCLEVBQXdELGNBQXhELEVBQXdFSix1QkFBeEUsRUFBaUc7QUFDN0ZwRyxNQUFBQSxNQUFNLEVBQUUsS0FBS3dDLEtBQUwsQ0FBV3hDLE1BRDBFO0FBRTdGeUcsTUFBQUEsTUFBTSxFQUFFLEtBQUtqRSxLQUFMLENBQVd4QyxNQUFYLENBQWtCbUcsVUFBbEIsS0FBaUMsS0FBakMsR0FBeUMseUJBQUcsT0FBSCxDQUF6QyxHQUF1RCx5QkFBRyxLQUFILENBRjhCO0FBRzdGTyxNQUFBQSxLQUFLLEVBQUUsS0FBS2xFLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0JtRyxVQUFsQixLQUFpQyxLQUFqQyxHQUF5Qyx5QkFBRyxrQkFBSCxDQUF6QyxHQUFrRSx5QkFBRyxnQkFBSCxDQUhvQjtBQUk3RlEsTUFBQUEsU0FBUyxFQUFFLEtBQUtuRSxLQUFMLENBQVd4QyxNQUFYLENBQWtCbUcsVUFBbEIsS0FBaUMsS0FKaUQ7QUFLN0ZTLE1BQUFBLE1BQU0sRUFBRSxLQUFLcEUsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQm1HLFVBQWxCLEtBQWlDLEtBTG9EO0FBTTdGVSxNQUFBQSxVQUFVLEVBQUUsQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQzdCLFlBQUksQ0FBQ0QsT0FBTCxFQUFjO0FBRWQsYUFBSzlELFFBQUwsQ0FBYztBQUFFbEMsVUFBQUEsUUFBUSxFQUFFLEtBQUtpRSxLQUFMLENBQVdqRSxRQUFYLEdBQXNCO0FBQWxDLFNBQWQ7QUFDQSxZQUFJd0csT0FBSjs7QUFDQSxZQUFJLEtBQUs5RSxLQUFMLENBQVd4QyxNQUFYLENBQWtCbUcsVUFBbEIsS0FBaUMsS0FBckMsRUFBNEM7QUFDeENtQixVQUFBQSxPQUFPLEdBQUcsS0FBSzlGLE9BQUwsQ0FBYStGLEtBQWIsQ0FDTixLQUFLL0UsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjBFLE1BRFosRUFDb0IsS0FBS2xDLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUR0QyxDQUFWO0FBR0gsU0FKRCxNQUlPO0FBQ0gyRSxVQUFBQSxPQUFPLEdBQUcsS0FBSzlGLE9BQUwsQ0FBYWpCLEdBQWIsQ0FDTixLQUFLaUMsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjBFLE1BRFosRUFDb0IsS0FBS2xDLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUR0QyxFQUVOb0UsTUFBTSxJQUFJQyxTQUZKLENBQVY7QUFJSDs7QUFDRE0sUUFBQUEsT0FBTyxDQUFDcEQsSUFBUixDQUNJLFlBQVc7QUFDUDtBQUNBO0FBQ0F1QixVQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaO0FBQ0gsU0FMTCxFQUtPLFVBQVNGLEdBQVQsRUFBYztBQUNiLGdCQUFNeUIsV0FBVyxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0FiLFVBQUFBLE9BQU8sQ0FBQ3lCLEtBQVIsQ0FBYyxnQkFBZ0IxQixHQUE5Qjs7QUFDQWUseUJBQU1DLG1CQUFOLENBQTBCLG9CQUExQixFQUFnRCxFQUFoRCxFQUFvRFMsV0FBcEQsRUFBaUU7QUFDN0RQLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRHNEO0FBRTdEUyxZQUFBQSxXQUFXLEVBQUUseUJBQUcsb0JBQUg7QUFGZ0QsV0FBakU7QUFJSCxTQVpMLEVBYUU1QixPQWJGLENBYVUsTUFBSTtBQUNWLGVBQUt2QyxRQUFMLENBQWM7QUFBRWxDLFlBQUFBLFFBQVEsRUFBRSxLQUFLaUUsS0FBTCxDQUFXakUsUUFBWCxHQUFzQjtBQUFsQyxXQUFkO0FBQ0gsU0FmRDtBQWdCSDtBQXJDNEYsS0FBakc7QUF1Q0gsR0FsVDJCO0FBb1Q1QjBHLEVBQUFBLG1CQUFtQixFQUFFLGtCQUFpQjtBQUNsQyxVQUFNO0FBQUM5QyxNQUFBQSxNQUFEO0FBQVMvQixNQUFBQTtBQUFULFFBQW1CLEtBQUtILEtBQUwsQ0FBV3hDLE1BQXBDO0FBQ0EsVUFBTXdFLElBQUksR0FBRyxLQUFLaEQsT0FBTCxDQUFhaUcsT0FBYixDQUFxQi9DLE1BQXJCLENBQWI7O0FBQ0EsUUFBSSxDQUFDRixJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNELFVBQU1rRCxXQUFXLEdBQUdsRCxJQUFJLENBQUNtRCx3QkFBTCxFQUFwQjtBQUNBLFFBQUlDLGNBQWMsR0FBRyxFQUFyQjs7QUFDQSxTQUFLLE1BQU1DLFFBQVgsSUFBdUJILFdBQVcsQ0FBQ0ksWUFBWixFQUF2QixFQUFtRDtBQUMvQ0YsTUFBQUEsY0FBYyxHQUFHQyxRQUFRLENBQUNFLFNBQVQsR0FBcUJDLE1BQXJCLENBQTRCLENBQUNDLE1BQUQsRUFBU0MsS0FBVCxLQUFtQjtBQUM1RCxZQUFJQSxLQUFLLENBQUNDLFNBQU4sT0FBc0J4RixNQUF0QixJQUFnQyxDQUFDdUYsS0FBSyxDQUFDRSxVQUFOLEVBQXJDLEVBQXlEO0FBQ3JELGlCQUFPSCxNQUFNLENBQUNJLE1BQVAsQ0FBY0gsS0FBZCxDQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsaUJBQU9ELE1BQVA7QUFDSDtBQUNKLE9BTmdCLEVBTWRMLGNBTmMsQ0FBakI7QUFPSDs7QUFFRCxVQUFNVSxLQUFLLEdBQUdWLGNBQWMsQ0FBQ3RFLE1BQTdCO0FBQ0EsVUFBTWlGLElBQUksR0FBRyxLQUFLL0YsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQnVELElBQS9COztBQUVBLFFBQUkrRSxLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUNiLFlBQU1FLFVBQVUsR0FBR25DLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQixrQ0FBMUIsRUFBOEQsRUFBOUQsRUFBa0VnQyxVQUFsRSxFQUE4RTtBQUMxRTlCLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxzQ0FBSCxFQUEyQztBQUFDNkIsVUFBQUE7QUFBRCxTQUEzQyxDQURtRTtBQUUxRXBCLFFBQUFBLFdBQVcsRUFDUCwwQ0FDSSx3Q0FBSyx5QkFBRyx3RUFBSCxDQUFMLENBREo7QUFIc0UsT0FBOUU7QUFPSCxLQVRELE1BU087QUFDSCxZQUFNc0IsY0FBYyxHQUFHcEMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2QjtBQUNBLFlBQU1vQyxTQUFTLEdBQUcsTUFBTSxJQUFJM0UsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDN0N1Qyx1QkFBTUMsbUJBQU4sQ0FBMEIsZ0NBQTFCLEVBQTRELEVBQTVELEVBQWdFaUMsY0FBaEUsRUFBZ0Y7QUFDNUUvQixVQUFBQSxLQUFLLEVBQUUseUJBQUcsb0NBQUgsRUFBeUM7QUFBQzZCLFlBQUFBO0FBQUQsV0FBekMsQ0FEcUU7QUFFNUVwQixVQUFBQSxXQUFXLEVBQ1AsMENBQ0ksd0NBQUsseUJBQUcseUdBQUgsRUFBOEc7QUFBQ21CLFlBQUFBLEtBQUQ7QUFBUUMsWUFBQUE7QUFBUixXQUE5RyxDQUFMLENBREosRUFFSSx3Q0FBSyx5QkFBRyw4R0FBSCxDQUFMLENBRkosQ0FId0U7QUFPNUVJLFVBQUFBLE1BQU0sRUFBRSx5QkFBRywyQkFBSCxFQUFnQztBQUFDTCxZQUFBQTtBQUFELFdBQWhDLENBUG9FO0FBUTVFekIsVUFBQUEsVUFBVSxFQUFFN0M7QUFSZ0UsU0FBaEY7QUFVSCxPQVh1QixDQUF4Qjs7QUFhQSxVQUFJLENBQUMwRSxTQUFMLEVBQWdCO0FBQ1o7QUFDSCxPQWpCRSxDQW1CSDtBQUNBOzs7QUFDQSxZQUFNM0UsT0FBTyxDQUFDQyxPQUFSLEVBQU47QUFFQXlCLE1BQUFBLE9BQU8sQ0FBQ21ELElBQVIsb0NBQXlDTixLQUF6QywyQkFBK0RDLElBQS9ELGlCQUEwRTdELE1BQTFFO0FBQ0EsWUFBTVgsT0FBTyxDQUFDOEUsR0FBUixDQUFZakIsY0FBYyxDQUFDa0IsR0FBZixDQUFtQixNQUFNWixLQUFOLElBQWU7QUFDaEQsWUFBSTtBQUNBLGdCQUFNLEtBQUsxRyxPQUFMLENBQWF1SCxXQUFiLENBQXlCckUsTUFBekIsRUFBaUN3RCxLQUFLLENBQUNjLEtBQU4sRUFBakMsQ0FBTjtBQUNILFNBRkQsQ0FFRSxPQUFPeEQsR0FBUCxFQUFZO0FBQ1Y7QUFDQUMsVUFBQUEsT0FBTyxDQUFDeUIsS0FBUixDQUFjLGtCQUFkLEVBQWtDZ0IsS0FBSyxDQUFDYyxLQUFOLEVBQWxDO0FBQ0F2RCxVQUFBQSxPQUFPLENBQUN5QixLQUFSLENBQWMxQixHQUFkO0FBQ0g7QUFDSixPQVJpQixDQUFaLENBQU47QUFTQUMsTUFBQUEsT0FBTyxDQUFDbUQsSUFBUixxQ0FBMENOLEtBQTFDLDJCQUFnRUMsSUFBaEUsaUJBQTJFN0QsTUFBM0U7QUFDSDtBQUNKLEdBclgyQjtBQXVYNUJ1RSxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixVQUFNUixjQUFjLEdBQUdwQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCO0FBQ0EsV0FBTyxJQUFJdkMsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDNUJ1QyxxQkFBTUMsbUJBQU4sQ0FBMEIsZUFBMUIsRUFBMkMsRUFBM0MsRUFBK0NpQyxjQUEvQyxFQUErRDtBQUMzRC9CLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSCxDQURvRDtBQUUzRFMsUUFBQUEsV0FBVyxFQUNQLDBDQUNNLHlCQUFHLDRFQUNELHdFQURDLEdBRUQsdUJBRkYsQ0FETixDQUh1RDtBQVEzRHdCLFFBQUFBLE1BQU0sRUFBRSx5QkFBRyxRQUFILENBUm1EO0FBUzNEOUIsUUFBQUEsVUFBVSxFQUFFN0M7QUFUK0MsT0FBL0Q7QUFXSCxLQVpNLENBQVA7QUFhSCxHQXRZMkI7QUF3WTVCa0YsRUFBQUEsWUFBWSxFQUFFLGtCQUFpQjtBQUMzQixVQUFNakMsV0FBVyxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0EsVUFBTTVCLE1BQU0sR0FBRyxLQUFLbEMsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjBFLE1BQWpDO0FBQ0EsVUFBTXlFLE1BQU0sR0FBRyxLQUFLM0csS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BQWpDO0FBQ0EsVUFBTTZCLElBQUksR0FBRyxLQUFLaEQsT0FBTCxDQUFhaUcsT0FBYixDQUFxQi9DLE1BQXJCLENBQWI7QUFDQSxRQUFJLENBQUNGLElBQUwsRUFBVyxPQUxnQixDQU8zQjs7QUFDQSxRQUFJMkUsTUFBTSxLQUFLLEtBQUszSCxPQUFMLENBQWE0SCxTQUFiLEVBQWYsRUFBeUM7QUFDckMsVUFBSTtBQUNBLFlBQUksRUFBRSxNQUFNLEtBQUtILGVBQUwsRUFBUixDQUFKLEVBQXFDO0FBQ3hDLE9BRkQsQ0FFRSxPQUFPSSxDQUFQLEVBQVU7QUFDUjVELFFBQUFBLE9BQU8sQ0FBQ3lCLEtBQVIsQ0FBYyxzQ0FBZCxFQUFzRG1DLENBQXREO0FBQ0E7QUFDSDtBQUNKOztBQUVELFVBQU1DLGVBQWUsR0FBRzlFLElBQUksQ0FBQytFLFlBQUwsQ0FBa0JDLGNBQWxCLENBQWlDLHFCQUFqQyxFQUF3RCxFQUF4RCxDQUF4QjtBQUNBLFFBQUksQ0FBQ0YsZUFBTCxFQUFzQjtBQUV0QixVQUFNRyxPQUFPLEdBQUcsS0FBSzFFLEtBQUwsQ0FBV25FLEtBQTNCO0FBQ0EsVUFBTThJLFdBQVcsR0FBR0osZUFBZSxDQUFDSyxVQUFoQixFQUFwQjtBQUNBLFVBQU1DLFdBQVcsR0FDYixDQUFDRixXQUFXLENBQUN6QixNQUFaLEdBQXFCeUIsV0FBVyxDQUFDekIsTUFBWixDQUFtQixnQkFBbkIsQ0FBckIsR0FBNEQsSUFBN0QsS0FDQXlCLFdBQVcsQ0FBQ0csY0FGaEI7QUFJQSxRQUFJQyxLQUFKOztBQUNBLFFBQUlMLE9BQUosRUFBYTtBQUFFO0FBQ1hLLE1BQUFBLEtBQUssR0FBR0YsV0FBUjtBQUNILEtBRkQsTUFFTztBQUFFO0FBQ0xFLE1BQUFBLEtBQUssR0FBR0YsV0FBVyxHQUFHLENBQXRCO0FBQ0g7O0FBQ0RFLElBQUFBLEtBQUssR0FBR0MsUUFBUSxDQUFDRCxLQUFELENBQWhCOztBQUVBLFFBQUksQ0FBQ0UsS0FBSyxDQUFDRixLQUFELENBQVYsRUFBbUI7QUFDZixXQUFLOUcsUUFBTCxDQUFjO0FBQUVsQyxRQUFBQSxRQUFRLEVBQUUsS0FBS2lFLEtBQUwsQ0FBV2pFLFFBQVgsR0FBc0I7QUFBbEMsT0FBZDtBQUNBLFdBQUtVLE9BQUwsQ0FBYXlJLGFBQWIsQ0FBMkJ2RixNQUEzQixFQUFtQ3lFLE1BQW5DLEVBQTJDVyxLQUEzQyxFQUFrRFIsZUFBbEQsRUFBbUVwRixJQUFuRSxDQUNJLFlBQVc7QUFDUDtBQUNBO0FBQ0F1QixRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxxQkFBWjtBQUNILE9BTEwsRUFLTyxVQUFTRixHQUFULEVBQWM7QUFDYkMsUUFBQUEsT0FBTyxDQUFDeUIsS0FBUixDQUFjLGlCQUFpQjFCLEdBQS9COztBQUNBZSx1QkFBTUMsbUJBQU4sQ0FBMEIscUJBQTFCLEVBQWlELEVBQWpELEVBQXFEUyxXQUFyRCxFQUFrRTtBQUM5RFAsVUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FEdUQ7QUFFOURTLFVBQUFBLFdBQVcsRUFBRSx5QkFBRyxxQkFBSDtBQUZpRCxTQUFsRTtBQUlILE9BWEwsRUFZRTVCLE9BWkYsQ0FZVSxNQUFJO0FBQ1YsYUFBS3ZDLFFBQUwsQ0FBYztBQUFFbEMsVUFBQUEsUUFBUSxFQUFFLEtBQUtpRSxLQUFMLENBQVdqRSxRQUFYLEdBQXNCO0FBQWxDLFNBQWQ7QUFDSCxPQWREO0FBZUg7QUFDSixHQTViMkI7QUE4YjVCb0osRUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEIsVUFBTWpELFdBQVcsR0FBR1osR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBLFVBQU01QixNQUFNLEdBQUcsS0FBS2xDLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IwRSxNQUFqQztBQUNBLFVBQU15RSxNQUFNLEdBQUcsS0FBSzNHLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUFqQztBQUNBLFVBQU02QixJQUFJLEdBQUcsS0FBS2hELE9BQUwsQ0FBYWlHLE9BQWIsQ0FBcUIvQyxNQUFyQixDQUFiO0FBQ0EsUUFBSSxDQUFDRixJQUFMLEVBQVc7QUFFWCxVQUFNOEUsZUFBZSxHQUFHOUUsSUFBSSxDQUFDK0UsWUFBTCxDQUFrQkMsY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQXhCO0FBQ0EsUUFBSSxDQUFDRixlQUFMLEVBQXNCO0FBRXRCLFVBQU1hLEVBQUUsR0FBRzNGLElBQUksQ0FBQzRGLFNBQUwsQ0FBZSxLQUFLNUksT0FBTCxDQUFhc0QsV0FBYixDQUF5Qm5DLE1BQXhDLENBQVg7QUFDQSxRQUFJLENBQUN3SCxFQUFMLEVBQVM7QUFFVCxVQUFNRSxZQUFZLEdBQUdmLGVBQWUsQ0FBQ0ssVUFBaEIsR0FBNkJXLGFBQWxEO0FBQ0EsUUFBSUMsUUFBUSxHQUFHSixFQUFFLENBQUNLLFVBQUgsR0FBZ0IsQ0FBL0I7QUFDQSxRQUFJRCxRQUFRLEdBQUcsRUFBWCxJQUFpQkYsWUFBWSxHQUFHLEVBQXBDLEVBQXdDRSxRQUFRLEdBQUcsRUFBWCxDQWZwQixDQWVtQztBQUN2RDs7QUFDQSxVQUFNRSxRQUFRLEdBQUcsS0FBSzFGLEtBQUwsQ0FBV2xFLFdBQVgsR0FBeUJ3SixZQUF6QixHQUF3Q0UsUUFBekQ7QUFDQSxTQUFLdkgsUUFBTCxDQUFjO0FBQUVsQyxNQUFBQSxRQUFRLEVBQUUsS0FBS2lFLEtBQUwsQ0FBV2pFLFFBQVgsR0FBc0I7QUFBbEMsS0FBZDtBQUNBLFNBQUtVLE9BQUwsQ0FBYXlJLGFBQWIsQ0FBMkJ2RixNQUEzQixFQUFtQ3lFLE1BQW5DLEVBQTJDWSxRQUFRLENBQUNVLFFBQUQsQ0FBbkQsRUFBK0RuQixlQUEvRCxFQUFnRnBGLElBQWhGLENBQ0ksWUFBVztBQUNQO0FBQ0E7QUFDQXVCLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG9CQUFaO0FBQ0gsS0FMTCxFQUtPLFVBQVNGLEdBQVQsRUFBYztBQUNiLFVBQUlBLEdBQUcsQ0FBQ2tGLE9BQUosS0FBZ0IsMEJBQXBCLEVBQWdEO0FBQzVDQyw0QkFBSUMsUUFBSixDQUFhO0FBQUNuRSxVQUFBQSxNQUFNLEVBQUU7QUFBVCxTQUFiO0FBQ0gsT0FGRCxNQUVPO0FBQ0hoQixRQUFBQSxPQUFPLENBQUN5QixLQUFSLENBQWMsNEJBQTRCMUIsR0FBMUM7O0FBQ0FlLHVCQUFNQyxtQkFBTixDQUEwQixtQ0FBMUIsRUFBK0QsRUFBL0QsRUFBbUVTLFdBQW5FLEVBQWdGO0FBQzVFUCxVQUFBQSxLQUFLLEVBQUUseUJBQUcsT0FBSCxDQURxRTtBQUU1RVMsVUFBQUEsV0FBVyxFQUFFLHlCQUFHLG1DQUFIO0FBRitELFNBQWhGO0FBSUg7QUFDSixLQWZMLEVBZ0JFNUIsT0FoQkYsQ0FnQlUsTUFBSTtBQUNWLFdBQUt2QyxRQUFMLENBQWM7QUFBRWxDLFFBQUFBLFFBQVEsRUFBRSxLQUFLaUUsS0FBTCxDQUFXakUsUUFBWCxHQUFzQjtBQUFsQyxPQUFkO0FBQ0gsS0FsQkQ7QUFtQkgsR0FwZTJCO0FBc2U1QitKLEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUIsVUFBTXBDLGNBQWMsR0FBR3BDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdkI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQiwyQkFBMUIsRUFBdUQsRUFBdkQsRUFBMkRpQyxjQUEzRCxFQUEyRTtBQUN2RS9CLE1BQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSCxDQURnRTtBQUV2RVMsTUFBQUEsV0FBVyxFQUNQLDBDQUFPLHlCQUNILG1HQUNBLHNHQURBLEdBRUEsdUJBSEcsQ0FBUCxDQUhtRTtBQVF2RXdCLE1BQUFBLE1BQU0sRUFBRSx5QkFBRyxpQkFBSCxDQVIrRDtBQVN2RS9CLE1BQUFBLE1BQU0sRUFBRSxJQVQrRDtBQVV2RUMsTUFBQUEsVUFBVSxFQUFHaUUsUUFBRCxJQUFjO0FBQ3RCLFlBQUksQ0FBQ0EsUUFBTCxFQUFlO0FBQ2YsYUFBS3RKLE9BQUwsQ0FBYXVKLHFCQUFiLENBQW1DLEtBQUt2SSxLQUFMLENBQVd4QyxNQUFYLENBQWtCMkMsTUFBckQsRUFBNkRxSSxLQUE3RCxDQUFtRTNCLENBQUMsSUFBSTtBQUNwRTVELFVBQUFBLE9BQU8sQ0FBQ3lCLEtBQVIsQ0FBYywyQkFBZDtBQUNBekIsVUFBQUEsT0FBTyxDQUFDeUIsS0FBUixDQUFjbUMsQ0FBZDtBQUVBLGdCQUFNcEMsV0FBVyxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsbUNBQTFCLEVBQStELEVBQS9ELEVBQW1FUyxXQUFuRSxFQUFnRjtBQUM1RVAsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLDJCQUFILENBRHFFO0FBRTVFUyxZQUFBQSxXQUFXLEVBQUlrQyxDQUFDLElBQUlBLENBQUMsQ0FBQ2pDLE9BQVIsR0FBbUJpQyxDQUFDLENBQUNqQyxPQUFyQixHQUErQix5QkFBRyxrQkFBSDtBQUYrQixXQUFoRjtBQUlILFNBVEQ7QUFVSDtBQXRCc0UsS0FBM0U7QUF3QkgsR0FoZ0IyQjtBQWtnQjVCNkQsRUFBQUEsaUJBQWlCLEVBQUUsVUFBU3ZHLE1BQVQsRUFBaUJ5RSxNQUFqQixFQUF5QnFCLFVBQXpCLEVBQXFDbEIsZUFBckMsRUFBc0Q7QUFDckUsU0FBS3RHLFFBQUwsQ0FBYztBQUFFbEMsTUFBQUEsUUFBUSxFQUFFLEtBQUtpRSxLQUFMLENBQVdqRSxRQUFYLEdBQXNCO0FBQWxDLEtBQWQ7QUFDQSxTQUFLVSxPQUFMLENBQWF5SSxhQUFiLENBQTJCdkYsTUFBM0IsRUFBbUN5RSxNQUFuQyxFQUEyQ1ksUUFBUSxDQUFDUyxVQUFELENBQW5ELEVBQWlFbEIsZUFBakUsRUFBa0ZwRixJQUFsRixDQUNJLFlBQVc7QUFDUDtBQUNBO0FBQ0F1QixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxzQkFBWjtBQUNILEtBTEwsRUFLTyxVQUFTRixHQUFULEVBQWM7QUFDYixZQUFNeUIsV0FBVyxHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0FiLE1BQUFBLE9BQU8sQ0FBQ3lCLEtBQVIsQ0FBYyxrQ0FBa0MxQixHQUFoRDs7QUFDQWUscUJBQU1DLG1CQUFOLENBQTBCLDhCQUExQixFQUEwRCxFQUExRCxFQUE4RFMsV0FBOUQsRUFBMkU7QUFDdkVQLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRGdFO0FBRXZFUyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsOEJBQUg7QUFGMEQsT0FBM0U7QUFJSCxLQVpMLEVBYUU1QixPQWJGLENBYVUsTUFBSTtBQUNWLFdBQUt2QyxRQUFMLENBQWM7QUFBRWxDLFFBQUFBLFFBQVEsRUFBRSxLQUFLaUUsS0FBTCxDQUFXakUsUUFBWCxHQUFzQjtBQUFsQyxPQUFkO0FBQ0gsS0FmRDtBQWdCSCxHQXBoQjJCO0FBc2hCNUJvSyxFQUFBQSxhQUFhLEVBQUUsZ0JBQWVWLFVBQWYsRUFBMkI7QUFDdEMsVUFBTTlGLE1BQU0sR0FBRyxLQUFLbEMsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjBFLE1BQWpDO0FBQ0EsVUFBTXlFLE1BQU0sR0FBRyxLQUFLM0csS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BQWpDO0FBQ0EsVUFBTTZCLElBQUksR0FBRyxLQUFLaEQsT0FBTCxDQUFhaUcsT0FBYixDQUFxQi9DLE1BQXJCLENBQWI7QUFDQSxRQUFJLENBQUNGLElBQUwsRUFBVztBQUVYLFVBQU04RSxlQUFlLEdBQUc5RSxJQUFJLENBQUMrRSxZQUFMLENBQWtCQyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBeEI7QUFDQSxRQUFJLENBQUNGLGVBQUwsRUFBc0I7O0FBRXRCLFFBQUksQ0FBQ0EsZUFBZSxDQUFDSyxVQUFoQixHQUE2QndCLEtBQWxDLEVBQXlDO0FBQ3JDLFdBQUtGLGlCQUFMLENBQXVCdkcsTUFBdkIsRUFBK0J5RSxNQUEvQixFQUF1Q3FCLFVBQXZDLEVBQW1EbEIsZUFBbkQ7O0FBQ0E7QUFDSDs7QUFFRCxVQUFNOEIsUUFBUSxHQUFHLEtBQUs1SixPQUFMLENBQWE0SCxTQUFiLEVBQWpCO0FBQ0EsVUFBTVgsY0FBYyxHQUFHcEMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2QixDQWZzQyxDQWlCdEM7O0FBQ0EsUUFBSThFLFFBQVEsS0FBS2pDLE1BQWpCLEVBQXlCO0FBQ3JCLFVBQUk7QUFDQSxZQUFJLEVBQUUsTUFBTSxLQUFLRixlQUFMLEVBQVIsQ0FBSixFQUFxQzs7QUFDckMsYUFBS2dDLGlCQUFMLENBQXVCdkcsTUFBdkIsRUFBK0J5RSxNQUEvQixFQUF1Q3FCLFVBQXZDLEVBQW1EbEIsZUFBbkQ7QUFDSCxPQUhELENBR0UsT0FBT0QsQ0FBUCxFQUFVO0FBQ1I1RCxRQUFBQSxPQUFPLENBQUN5QixLQUFSLENBQWMsc0NBQWQsRUFBc0RtQyxDQUF0RDtBQUNIOztBQUNEO0FBQ0g7O0FBRUQsVUFBTWdDLE9BQU8sR0FBRy9CLGVBQWUsQ0FBQ0ssVUFBaEIsR0FBNkJ3QixLQUE3QixDQUFtQ0MsUUFBbkMsQ0FBaEI7O0FBQ0EsUUFBSXJCLFFBQVEsQ0FBQ3NCLE9BQUQsQ0FBUixLQUFzQnRCLFFBQVEsQ0FBQ1MsVUFBRCxDQUFsQyxFQUFnRDtBQUM1Q2pFLHFCQUFNQyxtQkFBTixDQUEwQiwwQkFBMUIsRUFBc0QsRUFBdEQsRUFBMERpQyxjQUExRCxFQUEwRTtBQUN0RS9CLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFILENBRCtEO0FBRXRFUyxRQUFBQSxXQUFXLEVBQ1AsMENBQ00seUJBQUcsNEVBQ0QsMkNBREYsQ0FETixFQUVzRCx3Q0FGdEQsRUFHTSx5QkFBRyxlQUFILENBSE4sQ0FIa0U7QUFRdEV3QixRQUFBQSxNQUFNLEVBQUUseUJBQUcsVUFBSCxDQVI4RDtBQVN0RTlCLFFBQUFBLFVBQVUsRUFBRzZCLFNBQUQsSUFBZTtBQUN2QixjQUFJQSxTQUFKLEVBQWU7QUFDWCxpQkFBS3VDLGlCQUFMLENBQXVCdkcsTUFBdkIsRUFBK0J5RSxNQUEvQixFQUF1Q3FCLFVBQXZDLEVBQW1EbEIsZUFBbkQ7QUFDSDtBQUNKO0FBYnFFLE9BQTFFOztBQWVBO0FBQ0g7O0FBQ0QsU0FBSzJCLGlCQUFMLENBQXVCdkcsTUFBdkIsRUFBK0J5RSxNQUEvQixFQUF1Q3FCLFVBQXZDLEVBQW1EbEIsZUFBbkQ7QUFDSCxHQXRrQjJCO0FBd2tCNUJnQyxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQixTQUFLdEksUUFBTCxDQUFjO0FBQUVsQyxNQUFBQSxRQUFRLEVBQUUsS0FBS2lFLEtBQUwsQ0FBV2pFLFFBQVgsR0FBc0I7QUFBbEMsS0FBZDtBQUNBLDZCQUFXO0FBQUN5SyxNQUFBQSxRQUFRLEVBQUUsS0FBSy9JLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQztBQUE3QixLQUFYLEVBQWlENEMsT0FBakQsQ0FBeUQsTUFBTTtBQUMzRCxXQUFLdkMsUUFBTCxDQUFjO0FBQUVsQyxRQUFBQSxRQUFRLEVBQUUsS0FBS2lFLEtBQUwsQ0FBV2pFLFFBQVgsR0FBc0I7QUFBbEMsT0FBZDtBQUNILEtBRkQ7QUFHSCxHQTdrQjJCO0FBK2tCNUIwSyxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQmIsd0JBQUlDLFFBQUosQ0FBYTtBQUNUbkUsTUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVGdGLE1BQUFBLE9BQU8sRUFBRSxLQUFLakosS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjBFO0FBRmxCLEtBQWI7QUFJSCxHQXBsQjJCO0FBc2xCNUJRLEVBQUFBLHdCQUF3QixFQUFFLGdCQUFlbEYsTUFBZixFQUF1QjtBQUM3QyxRQUFJMEwsYUFBYSxHQUFHLEtBQXBCOztBQUNBLFFBQUksS0FBS2xLLE9BQVQsRUFBa0I7QUFDZCxVQUFJO0FBQ0FrSyxRQUFBQSxhQUFhLEdBQUcsTUFBTSxLQUFLbEssT0FBTCxDQUFhbUssc0JBQWIsRUFBdEI7QUFDSCxPQUZELENBRUUsT0FBT3RDLENBQVAsRUFBVTtBQUNSNUQsUUFBQUEsT0FBTyxDQUFDeUIsS0FBUixDQUFjbUMsQ0FBZDtBQUNIO0FBQ0o7O0FBRUQsVUFBTXVDLFlBQVksR0FBRztBQUNqQnZMLE1BQUFBLEdBQUcsRUFBRTtBQUNEO0FBQ0FLLFFBQUFBLGlCQUFpQixFQUFFZ0w7QUFGbEIsT0FEWTtBQUtqQjlLLE1BQUFBLEtBQUssRUFBRTtBQUxVLEtBQXJCO0FBT0EsVUFBTTRELElBQUksR0FBRyxLQUFLaEQsT0FBTCxDQUFhaUcsT0FBYixDQUFxQnpILE1BQU0sQ0FBQzBFLE1BQTVCLENBQWI7QUFDQSxRQUFJLENBQUNGLElBQUwsRUFBVyxPQUFPb0gsWUFBUDtBQUVYLFVBQU1sQyxXQUFXLEdBQUdsRixJQUFJLENBQUMrRSxZQUFMLENBQWtCQyxjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBcEI7QUFDQSxRQUFJLENBQUNFLFdBQUwsRUFBa0IsT0FBT2tDLFlBQVA7QUFFbEIsVUFBTXpCLEVBQUUsR0FBRzNGLElBQUksQ0FBQzRGLFNBQUwsQ0FBZSxLQUFLNUksT0FBTCxDQUFhc0QsV0FBYixDQUF5Qm5DLE1BQXhDLENBQVg7QUFDQSxRQUFJLENBQUN3SCxFQUFMLEVBQVMsT0FBT3lCLFlBQVA7QUFFVCxVQUFNQyxJQUFJLEdBQUc3TCxNQUFiO0FBQ0EsV0FBTztBQUNISyxNQUFBQSxHQUFHLG9CQUNJdUwsWUFBWSxDQUFDdkwsR0FEakIsT0FFSSxNQUFNLEtBQUt5TCx3QkFBTCxDQUE4QjNCLEVBQTlCLEVBQWtDMEIsSUFBbEMsRUFBd0NuQyxXQUFXLENBQUNDLFVBQVosRUFBeEMsQ0FGVixFQURBO0FBS0gvSSxNQUFBQSxLQUFLLEVBQUUsS0FBS21MLFFBQUwsQ0FBY0YsSUFBZCxFQUFvQm5DLFdBQVcsQ0FBQ0MsVUFBWixFQUFwQixDQUxKO0FBTUg5SSxNQUFBQSxXQUFXLEVBQUVnTCxJQUFJLENBQUNyQixVQUFMLEdBQWtCZCxXQUFXLENBQUNDLFVBQVosR0FBeUJXO0FBTnJELEtBQVA7QUFRSCxHQXpuQjJCO0FBMm5CNUJ3QixFQUFBQSx3QkFBd0IsRUFBRSxVQUFTM0IsRUFBVCxFQUFhMEIsSUFBYixFQUFtQm5DLFdBQW5CLEVBQWdDO0FBQ3RELFVBQU1zQyxJQUFJLEdBQUc3QixFQUFFLENBQUN4SCxNQUFILEtBQWNrSixJQUFJLENBQUNsSixNQUFoQztBQUNBLFVBQU10QyxHQUFHLEdBQUc7QUFDUkMsTUFBQUEsSUFBSSxFQUFFLEtBREU7QUFFUkMsTUFBQUEsR0FBRyxFQUFFLEtBRkc7QUFHUkMsTUFBQUEsSUFBSSxFQUFFLEtBSEU7QUFJUkMsTUFBQUEsV0FBVyxFQUFFLEtBSkw7QUFLUndMLE1BQUFBLGNBQWMsRUFBRSxDQUxSO0FBTVJ0TCxNQUFBQSxjQUFjLEVBQUV3SixFQUFFLENBQUNLLFVBQUgsSUFBaUJkLFdBQVcsQ0FBQ3dDO0FBTnJDLEtBQVo7QUFTQSxVQUFNQyxhQUFhLEdBQUdOLElBQUksQ0FBQ3JCLFVBQUwsR0FBa0JMLEVBQUUsQ0FBQ0ssVUFBckIsSUFBbUN3QixJQUF6RDs7QUFDQSxRQUFJLENBQUNHLGFBQUwsRUFBb0I7QUFDaEI7QUFDQSxhQUFPOUwsR0FBUDtBQUNIOztBQUNELFVBQU0rTCxjQUFjLEdBQ2hCLENBQUMxQyxXQUFXLENBQUN6QixNQUFaLEdBQXFCeUIsV0FBVyxDQUFDekIsTUFBWixDQUFtQixxQkFBbkIsQ0FBckIsR0FBaUUsSUFBbEUsS0FDQXlCLFdBQVcsQ0FBQzJDLGFBRmhCO0FBS0FoTSxJQUFBQSxHQUFHLENBQUNDLElBQUosR0FBVzZKLEVBQUUsQ0FBQ0ssVUFBSCxJQUFpQmQsV0FBVyxDQUFDcEosSUFBeEM7QUFDQUQsSUFBQUEsR0FBRyxDQUFDRSxHQUFKLEdBQVU0SixFQUFFLENBQUNLLFVBQUgsSUFBaUJkLFdBQVcsQ0FBQ25KLEdBQXZDO0FBQ0FGLElBQUFBLEdBQUcsQ0FBQ2lNLE1BQUosR0FBYW5DLEVBQUUsQ0FBQ0ssVUFBSCxJQUFpQmQsV0FBVyxDQUFDNEMsTUFBMUM7QUFDQWpNLElBQUFBLEdBQUcsQ0FBQ0csSUFBSixHQUFXMkosRUFBRSxDQUFDSyxVQUFILElBQWlCNEIsY0FBNUI7QUFDQS9MLElBQUFBLEdBQUcsQ0FBQ0ksV0FBSixHQUFrQjBKLEVBQUUsQ0FBQ0ssVUFBSCxJQUFpQjRCLGNBQWpCLEtBQW9DSixJQUFJLElBQUk3QixFQUFFLENBQUNLLFVBQUgsR0FBZ0JxQixJQUFJLENBQUNyQixVQUFqRSxDQUFsQjtBQUNBbkssSUFBQUEsR0FBRyxDQUFDNEwsY0FBSixHQUFxQjlCLEVBQUUsQ0FBQ0ssVUFBeEI7QUFFQSxXQUFPbkssR0FBUDtBQUNILEdBeHBCMkI7QUEwcEI1QjBMLEVBQUFBLFFBQVEsRUFBRSxVQUFTL0wsTUFBVCxFQUFpQnVNLGlCQUFqQixFQUFvQztBQUMxQyxRQUFJLENBQUNBLGlCQUFELElBQXNCLENBQUN2TSxNQUEzQixFQUFtQyxPQUFPLEtBQVA7QUFFbkMsVUFBTTRKLFdBQVcsR0FDYixDQUFDMkMsaUJBQWlCLENBQUN0RSxNQUFsQixHQUEyQnNFLGlCQUFpQixDQUFDdEUsTUFBbEIsQ0FBeUIsZ0JBQXpCLENBQTNCLEdBQXdFLElBQXpFLEtBQ0FzRSxpQkFBaUIsQ0FBQzFDLGNBRnRCO0FBSUEsV0FBTzdKLE1BQU0sQ0FBQ3dLLFVBQVAsR0FBb0JaLFdBQTNCO0FBQ0gsR0FscUIyQjtBQW9xQjVCNEMsRUFBQUEsUUFBUSxFQUFFLFVBQVNuRCxDQUFULEVBQVk7QUFDbEJzQix3QkFBSUMsUUFBSixDQUFhO0FBQ1RuRSxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUekcsTUFBQUEsTUFBTSxFQUFFO0FBRkMsS0FBYjtBQUlILEdBenFCMkI7QUEycUI1QnlNLEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUIsVUFBTXpNLE1BQU0sR0FBRyxLQUFLd0MsS0FBTCxDQUFXeEMsTUFBMUI7QUFDQSxVQUFNME0sU0FBUyxHQUFHMU0sTUFBTSxDQUFDMk0sZUFBUCxFQUFsQjtBQUNBLFFBQUksQ0FBQ0QsU0FBTCxFQUFnQjtBQUVoQixVQUFNRSxPQUFPLEdBQUcsS0FBS3BMLE9BQUwsQ0FBYXFMLFlBQWIsQ0FBMEJILFNBQTFCLENBQWhCO0FBQ0EsVUFBTUksU0FBUyxHQUFHekcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFsQjtBQUNBLFVBQU15RyxNQUFNLEdBQUc7QUFDWEMsTUFBQUEsR0FBRyxFQUFFSixPQURNO0FBRVhySixNQUFBQSxJQUFJLEVBQUV2RCxNQUFNLENBQUN1RDtBQUZGLEtBQWY7O0FBS0FnRCxtQkFBTTBHLFlBQU4sQ0FBbUJILFNBQW5CLEVBQThCQyxNQUE5QixFQUFzQyxvQkFBdEM7QUFDSCxHQXhyQjJCOztBQTByQjVCRyxFQUFBQSxlQUFlLENBQUN4SSxNQUFELEVBQVM7QUFDcEJpRyx3QkFBSUMsUUFBSixDQUFhO0FBQ1RuRSxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUZ0YsTUFBQUEsT0FBTyxFQUFFL0c7QUFGQSxLQUFiO0FBSUgsR0EvckIyQjs7QUFpc0I1QnlJLEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCLFFBQUksQ0FBQyxLQUFLMUwsY0FBVixFQUEwQixPQUFPLElBQVA7QUFFMUIsVUFBTVQsT0FBTyxHQUFHLEtBQUsrRCxLQUFMLENBQVcvRCxPQUEzQjtBQUNBLFVBQU1vTSxnQkFBZ0IsR0FBRy9HLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBekI7QUFDQSxVQUFNK0csT0FBTyxHQUFHaEgsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUVBLFFBQUlnSCxhQUFKOztBQUNBLFFBQUksS0FBS3ZJLEtBQUwsQ0FBV2hFLGNBQWYsRUFBK0I7QUFDM0I7QUFDQXVNLE1BQUFBLGFBQWEsR0FBRyw2QkFBQyxPQUFELE9BQWhCO0FBQ0gsS0FIRCxNQUdPLElBQUl0TSxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDekJzTSxNQUFBQSxhQUFhLEdBQUcseUJBQUcsNkJBQUgsQ0FBaEI7QUFDSCxLQUZNLE1BRUEsSUFBSXRNLE9BQU8sQ0FBQ3NDLE1BQVIsS0FBbUIsQ0FBdkIsRUFBMEI7QUFDN0JnSyxNQUFBQSxhQUFhLEdBQUcseUJBQUcsNkNBQUgsQ0FBaEI7QUFDSCxLQUZNLE1BRUE7QUFDSEEsTUFBQUEsYUFBYSxHQUFHLEVBQWhCOztBQUNBLFdBQUssSUFBSWpLLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdyQyxPQUFPLENBQUNzQyxNQUE1QixFQUFvQ0QsQ0FBQyxFQUFyQyxFQUF5QztBQUNyQ2lLLFFBQUFBLGFBQWEsQ0FBQzVKLElBQWQsQ0FBbUIsNkJBQUMsZ0JBQUQ7QUFBa0IsVUFBQSxHQUFHLEVBQUVMLENBQXZCO0FBQ0ksVUFBQSxNQUFNLEVBQUUsS0FBS2IsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BRDlCO0FBRUksVUFBQSxNQUFNLEVBQUUzQixPQUFPLENBQUNxQyxDQUFEO0FBRm5CLFVBQW5CO0FBR0g7QUFDSjs7QUFFRCxXQUNJLDBDQUNJLHlDQUFNLHlCQUFHLFVBQUgsQ0FBTixDQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ01pSyxhQUROLENBRkosQ0FESjtBQVFILEdBanVCMkI7QUFtdUI1QkMsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixVQUFNQyxXQUFXLEdBQUduSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsMEJBQTFCLEVBQXNELEVBQXRELEVBQTBEZ0gsV0FBMUQsRUFBdUU7QUFDbkVyRSxNQUFBQSxNQUFNLEVBQUUsS0FBSzNHLEtBQUwsQ0FBV3hDO0FBRGdELEtBQXZFO0FBR0gsR0F4dUIyQjtBQTB1QjVCeU4sRUFBQUEsa0JBQWtCLEVBQUUsWUFBVztBQUMzQixVQUFNbE0sR0FBRyxHQUFHLEtBQUtDLE9BQWpCO0FBQ0EsVUFBTXhCLE1BQU0sR0FBRyxLQUFLd0MsS0FBTCxDQUFXeEMsTUFBMUI7QUFFQSxRQUFJME4sWUFBWSxHQUFHLElBQW5CO0FBQ0EsUUFBSUMsZ0JBQWdCLEdBQUcsSUFBdkI7QUFDQSxRQUFJQyxnQkFBZ0IsR0FBRyxJQUF2QjtBQUNBLFFBQUlDLGlCQUFpQixHQUFHLElBQXhCLENBUDJCLENBUzNCO0FBQ0E7O0FBQ0EsUUFBSTdOLE1BQU0sQ0FBQzJDLE1BQVAsS0FBa0JwQixHQUFHLENBQUM2SCxTQUFKLEVBQXRCLEVBQXVDO0FBQ25Dc0UsTUFBQUEsWUFBWSxHQUNSLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUsvSCxjQUFoQztBQUFnRCxRQUFBLFNBQVMsRUFBQztBQUExRCxTQUNNLEtBQUtaLEtBQUwsQ0FBVzlELFVBQVgsR0FBd0IseUJBQUcsVUFBSCxDQUF4QixHQUF5Qyx5QkFBRyxRQUFILENBRC9DLENBREo7O0FBTUEsVUFBSWpCLE1BQU0sQ0FBQzBFLE1BQVgsRUFBbUI7QUFDZixjQUFNRixJQUFJLEdBQUdqRCxHQUFHLENBQUNrRyxPQUFKLENBQVl6SCxNQUFNLENBQUMwRSxNQUFuQixDQUFiO0FBQ0EsY0FBTW9KLE9BQU8sR0FBR3RKLElBQUksQ0FBQ3VKLGdCQUFMLENBQXNCL04sTUFBTSxDQUFDMkMsTUFBN0IsQ0FBaEI7O0FBRUEsY0FBTXFMLG1CQUFtQixHQUFHLFlBQVc7QUFDbkNyRCw4QkFBSUMsUUFBSixDQUFhO0FBQ1RuRSxZQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUd0gsWUFBQUEsV0FBVyxFQUFFLElBRko7QUFHVEMsWUFBQUEsUUFBUSxFQUFFSixPQUhEO0FBSVRyQyxZQUFBQSxPQUFPLEVBQUV6TCxNQUFNLENBQUMwRTtBQUpQLFdBQWI7QUFNSCxTQVBEOztBQVNBLGNBQU15SixrQkFBa0IsR0FBRyxZQUFXO0FBQ2xDeEQsOEJBQUlDLFFBQUosQ0FBYTtBQUNUbkUsWUFBQUEsTUFBTSxFQUFFLGdCQURDO0FBRVQySCxZQUFBQSxPQUFPLEVBQUVwTyxNQUFNLENBQUMyQztBQUZQLFdBQWI7QUFJSCxTQUxEOztBQU9Ba0wsUUFBQUEsaUJBQWlCLEdBQ2IsNkJBQUMseUJBQUQ7QUFBa0IsVUFBQSxPQUFPLEVBQUVHLG1CQUEzQjtBQUFnRCxVQUFBLFNBQVMsRUFBQztBQUExRCxXQUNNLHlCQUFHLHNCQUFILENBRE4sQ0FESjtBQU1BTCxRQUFBQSxnQkFBZ0IsR0FDWiw2QkFBQyx5QkFBRDtBQUFrQixVQUFBLE9BQU8sRUFBRVEsa0JBQTNCO0FBQStDLFVBQUEsU0FBUyxFQUFFO0FBQTFELFdBQ00seUJBQUcsU0FBSCxDQUROLENBREo7QUFLSDs7QUFFRCxVQUFJLEtBQUtwSixLQUFMLENBQVcxRSxHQUFYLENBQWVpTSxNQUFmLEtBQTBCLENBQUN0TSxNQUFELElBQVcsQ0FBQ0EsTUFBTSxDQUFDbUcsVUFBbkIsSUFBaUNuRyxNQUFNLENBQUNtRyxVQUFQLEtBQXNCLE9BQWpGLENBQUosRUFBK0Y7QUFDM0YsY0FBTXpCLE1BQU0sR0FBRzFFLE1BQU0sSUFBSUEsTUFBTSxDQUFDMEUsTUFBakIsR0FBMEIxRSxNQUFNLENBQUMwRSxNQUFqQyxHQUEwQzJKLHVCQUFjQyxTQUFkLEVBQXpEOztBQUNBLGNBQU1DLGtCQUFrQixHQUFHLFlBQVk7QUFDbkMsY0FBSTtBQUNBO0FBQ0E7QUFDQSxrQkFBTUMsT0FBTyxHQUFHLElBQUlDLHFCQUFKLENBQWlCL0osTUFBakIsQ0FBaEI7QUFDQSxrQkFBTThKLE9BQU8sQ0FBQ2xDLE1BQVIsQ0FBZSxDQUFDdE0sTUFBTSxDQUFDMkMsTUFBUixDQUFmLEVBQWdDdUIsSUFBaEMsQ0FBcUMsTUFBTTtBQUM3QyxrQkFBSXNLLE9BQU8sQ0FBQ0Usa0JBQVIsQ0FBMkIxTyxNQUFNLENBQUMyQyxNQUFsQyxNQUE4QyxTQUFsRCxFQUNJLE1BQU0sSUFBSWdNLEtBQUosQ0FBVUgsT0FBTyxDQUFDSSxZQUFSLENBQXFCNU8sTUFBTSxDQUFDMkMsTUFBNUIsQ0FBVixDQUFOO0FBQ1AsYUFISyxDQUFOO0FBSUgsV0FSRCxDQVFFLE9BQU82QyxHQUFQLEVBQVk7QUFDVixrQkFBTXlCLFdBQVcsR0FBR1osR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsMkJBQU1DLG1CQUFOLENBQTBCLGtCQUExQixFQUE4QyxFQUE5QyxFQUFrRFMsV0FBbEQsRUFBK0Q7QUFDM0RQLGNBQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSCxDQURvRDtBQUUzRFMsY0FBQUEsV0FBVyxFQUFJM0IsR0FBRyxJQUFJQSxHQUFHLENBQUM0QixPQUFaLEdBQXVCNUIsR0FBRyxDQUFDNEIsT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGUSxhQUEvRDtBQUlIO0FBQ0osU0FoQkQ7O0FBa0JBd0csUUFBQUEsZ0JBQWdCLEdBQ1osNkJBQUMseUJBQUQ7QUFBa0IsVUFBQSxPQUFPLEVBQUVXLGtCQUEzQjtBQUErQyxVQUFBLFNBQVMsRUFBQztBQUF6RCxXQUNNLHlCQUFHLFFBQUgsQ0FETixDQURKO0FBS0g7QUFDSjs7QUFFRCxVQUFNTSxlQUFlLEdBQ2pCLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFLEtBQUt0QixnQkFBaEM7QUFBa0QsTUFBQSxTQUFTLEVBQUM7QUFBNUQsT0FDTSx5QkFBRyxvQkFBSCxDQUROLENBREo7O0FBTUEsV0FDSSwwQ0FDSSx5Q0FBTSx5QkFBRyxjQUFILENBQU4sQ0FESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNTSxpQkFETixFQUVNZ0IsZUFGTixFQUdNbEIsZ0JBSE4sRUFJTUQsWUFKTixFQUtNRSxnQkFMTixDQUZKLENBREo7QUFZSCxHQTMwQjJCO0FBNjBCNUJrQixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFFBQUlDLFNBQUo7QUFDQSxRQUFJQyxVQUFKO0FBQ0EsUUFBSUMsU0FBSjtBQUNBLFFBQUlDLFVBQUo7QUFDQSxRQUFJQyxhQUFKO0FBQ0EsUUFBSUMsWUFBSjtBQUNBLFFBQUlDLHVCQUFKO0FBQ0EsUUFBSUMsT0FBSjs7QUFFQSxRQUFJLEtBQUs5TSxLQUFMLENBQVd4QyxNQUFYLENBQWtCMkMsTUFBbEIsS0FBNkIsS0FBS25CLE9BQUwsQ0FBYXNELFdBQWIsQ0FBeUJuQyxNQUExRCxFQUFrRTtBQUM5RDtBQUNBLFlBQU00TSxTQUFTLEdBQUcsSUFBSUMsa0JBQUosQ0FBYyxLQUFLaE8sT0FBbkIsQ0FBbEIsQ0FGOEQsQ0FHOUQ7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsWUFBTWlPLE9BQU8sR0FBR0YsU0FBUyxDQUFDRyxtQkFBVixDQUE4QixLQUFLbE4sS0FBTCxDQUFXeEMsTUFBWCxDQUFrQjJDLE1BQWhELENBQWhCO0FBRUEsWUFBTWdOLFFBQVEsR0FBR3RKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixnQkFBakIsQ0FBakI7QUFFQSxZQUFNc0osS0FBSyxHQUFHLEVBQWQ7O0FBQ0EsV0FBSyxNQUFNbEwsTUFBWCxJQUFxQitLLE9BQXJCLEVBQThCO0FBQzFCLGNBQU1qTCxJQUFJLEdBQUcsS0FBS2hELE9BQUwsQ0FBYWlHLE9BQWIsQ0FBcUIvQyxNQUFyQixDQUFiOztBQUNBLFlBQUlGLElBQUosRUFBVTtBQUNOLGdCQUFNcUwsWUFBWSxHQUFHckwsSUFBSSxDQUFDc0wsZUFBTCxFQUFyQixDQURNLENBRU47O0FBQ0EsY0FBSUQsWUFBWSxLQUFLLE1BQXJCLEVBQTZCO0FBRTdCLGdCQUFNaEUsSUFBSSxHQUFHLEtBQUtySixLQUFMLENBQVd4QyxNQUF4QixDQUxNLENBTU47O0FBQ0EsY0FBSSxDQUFDNkwsSUFBSSxDQUFDMUYsVUFBTixJQUFvQjBGLElBQUksQ0FBQzFGLFVBQUwsS0FBb0IsTUFBNUMsRUFBb0Q7QUFFcEQsZ0JBQU00SixTQUFTLEdBQUd2TCxJQUFJLENBQUN3TCwwQkFBTCxDQUFnQyxXQUFoQyxJQUErQyxDQUFqRTtBQUVBSixVQUFBQSxLQUFLLENBQUNsTSxJQUFOLENBQ0ksNkJBQUMsUUFBRDtBQUFVLFlBQUEsR0FBRyxFQUFFYyxJQUFJLENBQUNFLE1BQXBCO0FBQTRCLFlBQUEsSUFBSSxFQUFFRixJQUFsQztBQUNJLFlBQUEsV0FBVyxFQUFFLElBRGpCO0FBRUksWUFBQSxTQUFTLEVBQUUsS0FGZjtBQUdJLFlBQUEsUUFBUSxFQUFFLEtBSGQ7QUFJSSxZQUFBLE1BQU0sRUFBRXlMLE1BQU0sQ0FBQ0MsMEJBQVAsQ0FBa0MxTCxJQUFsQyxDQUpaO0FBS0ksWUFBQSxTQUFTLEVBQUV1TCxTQUxmO0FBTUksWUFBQSxRQUFRLEVBQUUsS0FOZDtBQU9JLFlBQUEsT0FBTyxFQUFFLEtBQUs3QztBQVBsQixZQURKO0FBV0g7QUFDSjs7QUFFRCxZQUFNaUQsWUFBWSxHQUFHLHlCQUFXO0FBQzVCQyxRQUFBQSw4QkFBOEIsRUFBRSxJQURKO0FBRTVCQyxRQUFBQSxnQkFBZ0IsRUFBRTtBQUZVLE9BQVgsQ0FBckI7O0FBSUEsVUFBSUMsWUFBWSxHQUFHLDZCQUFDLHlCQUFEO0FBQ2YsUUFBQSxTQUFTLEVBQUMsMEJBREs7QUFFZixRQUFBLE9BQU8sRUFBRSxLQUFLaEY7QUFGQyxTQUlmO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQUssUUFBQSxHQUFHLEVBQUVpRixPQUFPLENBQUMsb0NBQUQsQ0FBakI7QUFBeUQsUUFBQSxLQUFLLEVBQUMsSUFBL0Q7QUFBb0UsUUFBQSxNQUFNLEVBQUM7QUFBM0UsUUFESixDQUplLEVBT2Y7QUFBSyxRQUFBLFNBQVMsRUFBRUo7QUFBaEIsU0FBOEIsd0NBQUsseUJBQUcsY0FBSCxDQUFMLENBQTlCLENBUGUsQ0FBbkI7O0FBVUEsVUFBSVAsS0FBSyxDQUFDdE0sTUFBTixHQUFlLENBQW5CLEVBQXNCZ04sWUFBWSxHQUFHLElBQWYsQ0FyRHdDLENBcURuQjs7QUFFM0N2QixNQUFBQSxTQUFTLEdBQUcsMENBQ1IseUNBQU0seUJBQUcsY0FBSCxDQUFOLENBRFEsRUFFTmEsS0FGTSxFQUdOVSxZQUhNLENBQVo7QUFLSDs7QUFFRCxRQUFJLEtBQUt2TCxLQUFMLENBQVdqRSxRQUFmLEVBQXlCO0FBQ3JCLFlBQU0wUCxNQUFNLEdBQUduSyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWY7QUFDQWdKLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxNQUFEO0FBQVEsUUFBQSxZQUFZLEVBQUM7QUFBckIsUUFBVjtBQUNIOztBQUVELFFBQUksS0FBS3ZLLEtBQUwsQ0FBVzFFLEdBQVgsQ0FBZUMsSUFBbkIsRUFBeUI7QUFDckIsWUFBTTZGLFVBQVUsR0FBRyxLQUFLM0QsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQm1HLFVBQXJDO0FBQ0EsWUFBTXNLLFNBQVMsR0FBR3RLLFVBQVUsS0FBSyxRQUFmLEdBQTBCLHlCQUFHLFdBQUgsQ0FBMUIsR0FBNEMseUJBQUcsTUFBSCxDQUE5RDtBQUNBNkksTUFBQUEsVUFBVSxHQUNOLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsU0FBUyxFQUFDLHFCQUE1QjtBQUNRLFFBQUEsT0FBTyxFQUFFLEtBQUs5STtBQUR0QixTQUVNdUssU0FGTixDQURKO0FBTUg7O0FBRUQsUUFBSSxLQUFLMUwsS0FBTCxDQUFXMUUsR0FBWCxDQUFlTSxjQUFuQixFQUFtQztBQUMvQnlPLE1BQUFBLFlBQVksR0FDUiw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxxQkFBNUI7QUFBa0QsUUFBQSxPQUFPLEVBQUUsS0FBSzVIO0FBQWhFLFNBQ00seUJBQUcsd0JBQUgsQ0FETixDQURKO0FBS0g7O0FBRUQsUUFBSSxLQUFLekMsS0FBTCxDQUFXMUUsR0FBWCxDQUFlRSxHQUFuQixFQUF3QjtBQUNwQixVQUFJbVEsS0FBSyxHQUFHLHlCQUFHLEtBQUgsQ0FBWjs7QUFDQSxVQUFJLEtBQUtsTyxLQUFMLENBQVd4QyxNQUFYLENBQWtCbUcsVUFBbEIsS0FBaUMsS0FBckMsRUFBNEM7QUFDeEN1SyxRQUFBQSxLQUFLLEdBQUcseUJBQUcsT0FBSCxDQUFSO0FBQ0g7O0FBQ0R6QixNQUFBQSxTQUFTLEdBQ0wsNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMscUJBQTVCO0FBQ1EsUUFBQSxPQUFPLEVBQUUsS0FBSzVIO0FBRHRCLFNBRU1xSixLQUZOLENBREo7QUFNSDs7QUFDRCxRQUFJLEtBQUszTCxLQUFMLENBQVcxRSxHQUFYLENBQWVHLElBQW5CLEVBQXlCO0FBQ3JCLFlBQU1tUSxTQUFTLEdBQUcsS0FBSzVMLEtBQUwsQ0FBV25FLEtBQVgsR0FBbUIseUJBQUcsUUFBSCxDQUFuQixHQUFrQyx5QkFBRyxNQUFILENBQXBEO0FBQ0FzTyxNQUFBQSxVQUFVLEdBQ04sNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMscUJBQTVCO0FBQ1EsUUFBQSxPQUFPLEVBQUUsS0FBS2hHO0FBRHRCLFNBRU15SCxTQUZOLENBREo7QUFNSDs7QUFDRCxRQUFJLEtBQUs1TCxLQUFMLENBQVcxRSxHQUFYLENBQWV1USxTQUFuQixFQUE4QjtBQUMxQixZQUFNQyxXQUFXLEdBQUcsS0FBSzlMLEtBQUwsQ0FBV2xFLFdBQVgsR0FBeUIseUJBQUcsa0JBQUgsQ0FBekIsR0FBa0QseUJBQUcsZ0JBQUgsQ0FBdEU7QUFDQXNPLE1BQUFBLGFBQWEsR0FBRyw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxxQkFBNUI7QUFBa0QsUUFBQSxPQUFPLEVBQUUsS0FBS2pGO0FBQWhFLFNBQ1YyRyxXQURVLENBQWhCO0FBR0gsS0ExSGMsQ0E0SGY7QUFDQTs7O0FBQ0EsVUFBTUMsY0FBYyxHQUFHLEtBQUt0TyxLQUFMLENBQVd4QyxNQUFYLENBQWtCMkMsTUFBbEIsQ0FBeUJvTyxRQUF6QixZQUFzQ0MsaUNBQWdCQyxpQkFBaEIsRUFBdEMsRUFBdkI7O0FBQ0EsUUFBSSxLQUFLbE0sS0FBTCxDQUFXMUUsR0FBWCxDQUFlSyxpQkFBZixJQUFvQ29RLGNBQXhDLEVBQXdEO0FBQ3BEekIsTUFBQUEsdUJBQXVCLEdBQ25CLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFFLEtBQUt4RSxtQkFBaEM7QUFBcUQsUUFBQSxTQUFTLEVBQUM7QUFBL0QsU0FDSyx5QkFBRyxpQkFBSCxDQURMLENBREo7QUFLSDs7QUFFRCxRQUFJcUcsVUFBSjs7QUFDQSxRQUFJbEMsVUFBVSxJQUFJQyxTQUFkLElBQTJCQyxVQUEzQixJQUF5Q0MsYUFBekMsSUFBMERFLHVCQUExRCxJQUFxRkQsWUFBekYsRUFBdUc7QUFDbkc4QixNQUFBQSxVQUFVLEdBQ04sMENBQ0kseUNBQU0seUJBQUcsYUFBSCxDQUFOLENBREosRUFHSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDTWhDLFVBRE4sRUFFTUYsVUFGTixFQUdNQyxTQUhOLEVBSU1HLFlBSk4sRUFLTUQsYUFMTixFQU1NRSx1QkFOTixDQUhKLENBREo7QUFhSDs7QUFFRCxVQUFNOEIsVUFBVSxHQUFHLEtBQUszTyxLQUFMLENBQVd4QyxNQUFYLENBQWtCdUQsSUFBckM7QUFFQSxRQUFJNk4sYUFBSjtBQUNBLFFBQUlDLHFCQUFKO0FBQ0EsUUFBSUMsdUJBQUo7QUFDQSxRQUFJQyxhQUFKOztBQUVBLFFBQUksS0FBSy9PLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0J1SSxJQUF0QixFQUE0QjtBQUN4QjZJLE1BQUFBLGFBQWEsR0FBRyxLQUFLNU8sS0FBTCxDQUFXeEMsTUFBWCxDQUFrQnVJLElBQWxCLENBQXVCaUosUUFBdkM7QUFDQUgsTUFBQUEscUJBQXFCLEdBQUcsS0FBSzdPLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0J1SSxJQUFsQixDQUF1QmtKLGFBQS9DO0FBQ0FILE1BQUFBLHVCQUF1QixHQUFHLEtBQUs5TyxLQUFMLENBQVd4QyxNQUFYLENBQWtCdUksSUFBbEIsQ0FBdUJtSixlQUFqRDs7QUFFQSxVQUFJQyx1QkFBY0MsZ0JBQWQsQ0FBK0IsdUJBQS9CLENBQUosRUFBNkQ7QUFDekRMLFFBQUFBLGFBQWEsR0FBRyxLQUFLL08sS0FBTCxDQUFXeEMsTUFBWCxDQUFrQnVJLElBQWxCLENBQXVCc0osdUJBQXZDO0FBQ0g7QUFDSjs7QUFFRCxVQUFNck4sSUFBSSxHQUFHLEtBQUtoRCxPQUFMLENBQWFpRyxPQUFiLENBQXFCLEtBQUtqRixLQUFMLENBQVd4QyxNQUFYLENBQWtCMEUsTUFBdkMsQ0FBYjtBQUNBLFVBQU00RSxlQUFlLEdBQUc5RSxJQUFJLEdBQUdBLElBQUksQ0FBQytFLFlBQUwsQ0FBa0JDLGNBQWxCLENBQWlDLHFCQUFqQyxFQUF3RCxFQUF4RCxDQUFILEdBQWlFLElBQTdGO0FBQ0EsVUFBTXNJLHNCQUFzQixHQUFHeEksZUFBZSxHQUFHQSxlQUFlLENBQUNLLFVBQWhCLEdBQTZCVyxhQUFoQyxHQUFnRCxDQUE5Rjs7QUFFQSxVQUFNeUgscUJBQXFCLEdBQUdDLG1CQUFVQyxHQUFWLEdBQWdCLDJCQUFoQixDQUE5Qjs7QUFDQSxVQUFNQyxLQUFLLEdBQUcsS0FBSzFRLE9BQUwsQ0FBYTJRLE9BQTNCO0FBQ0EsUUFBSUMsWUFBWSxHQUFHLElBQW5COztBQUNBLFFBQUlMLHFCQUFxQixJQUFJQSxxQkFBcUIsQ0FBQ0csS0FBRCxDQUFyQixLQUFpQ2xMLFNBQTlELEVBQXlFO0FBQ3JFb0wsTUFBQUEsWUFBWSxHQUFHTCxxQkFBcUIsQ0FBQ0csS0FBRCxDQUFwQztBQUNIOztBQUVELFFBQUlHLGFBQWEsR0FBRyxJQUFwQjs7QUFDQSxRQUFJRCxZQUFKLEVBQWtCO0FBQ2QsWUFBTUUsYUFBYSxHQUFHak0sR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUF0QjtBQUNBK0wsTUFBQUEsYUFBYSxHQUFHLDZCQUFDLGFBQUQ7QUFBZSxRQUFBLFNBQVMsRUFBRWhCLHFCQUExQjtBQUNaLFFBQUEsZUFBZSxFQUFFQyx1QkFETDtBQUVaLFFBQUEsYUFBYSxFQUFFRjtBQUZILFFBQWhCO0FBR0g7O0FBRUQsUUFBSW1CLFdBQVcsR0FBRyxJQUFsQjs7QUFDQSxRQUFJaEIsYUFBSixFQUFtQjtBQUNmZ0IsTUFBQUEsV0FBVyxHQUFHO0FBQU0sUUFBQSxTQUFTLEVBQUM7QUFBaEIsU0FBZ0RoQixhQUFoRCxDQUFkO0FBQ0g7O0FBRUQsUUFBSWlCLGlCQUFpQixHQUFHLElBQXhCO0FBQ0EsUUFBSUMsY0FBSjs7QUFFQSxRQUFJLEtBQUtqUSxLQUFMLENBQVd4QyxNQUFYLENBQWtCMEUsTUFBdEIsRUFBOEI7QUFBRTtBQUM1QixZQUFNZ08sYUFBYSxHQUFHck0sR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF0QjtBQUNBa00sTUFBQUEsaUJBQWlCLEdBQUcsMENBQ2hCO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJLDZCQUFDLGFBQUQ7QUFDSSxRQUFBLEtBQUssRUFBRXpJLFFBQVEsQ0FBQyxLQUFLdkgsS0FBTCxDQUFXeEMsTUFBWCxDQUFrQndLLFVBQW5CLENBRG5CO0FBRUksUUFBQSxRQUFRLEVBQUUsS0FBS3pGLEtBQUwsQ0FBVzFFLEdBQVgsQ0FBZTRMLGNBRjdCO0FBR0ksUUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLbEgsS0FBTCxDQUFXMUUsR0FBWCxDQUFlSSxXQUg5QjtBQUlJLFFBQUEsWUFBWSxFQUFFcVIsc0JBSmxCO0FBS0ksUUFBQSxRQUFRLEVBQUUsS0FBSzVHO0FBTG5CLFFBREosQ0FEZ0IsRUFTaEI7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ttSCxhQURMLEVBRUtFLFdBRkwsQ0FUZ0IsQ0FBcEI7QUFlQSxZQUFNSSxXQUFXLEdBQUcsS0FBS25SLE9BQUwsQ0FBYW9SLGVBQWIsQ0FBNkIsS0FBS3BRLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IwRSxNQUEvQyxDQUFwQjs7QUFDQSxVQUFJLEtBQUtLLEtBQUwsQ0FBV1osU0FBWCxJQUF3QndPLFdBQTVCLEVBQXlDO0FBQ3JDRixRQUFBQSxjQUFjLEdBQUksNkJBQUMsZ0JBQUQ7QUFBUyxVQUFBLE1BQU0sRUFBRSxLQUFLMU4sS0FBTCxDQUFXWixTQUE1QjtBQUF1QyxVQUFBLE1BQU0sRUFBRTtBQUEvQyxVQUFsQjtBQUNIO0FBQ0o7O0FBRUQsVUFBTTtBQUFDbkUsTUFBQUE7QUFBRCxRQUFXLEtBQUt3QyxLQUF0QjtBQUNBLFVBQU1rSyxTQUFTLEdBQUcxTSxNQUFNLENBQUMwTSxTQUFQLElBQXFCMU0sTUFBTSxDQUFDMk0sZUFBUCxJQUEwQjNNLE1BQU0sQ0FBQzJNLGVBQVAsRUFBakU7QUFDQSxRQUFJa0csYUFBSjs7QUFDQSxRQUFJbkcsU0FBSixFQUFlO0FBQ1gsWUFBTUUsT0FBTyxHQUFHLEtBQUtwTCxPQUFMLENBQWFxTCxZQUFiLENBQTBCSCxTQUExQixFQUFxQyxHQUFyQyxFQUEwQyxHQUExQyxDQUFoQjtBQUNBbUcsTUFBQUEsYUFBYSxHQUFHO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNaO0FBQUssUUFBQSxHQUFHLEVBQUVqRztBQUFWLFFBRFksQ0FBaEI7QUFHSDs7QUFFRCxRQUFJa0csVUFBSjs7QUFDQSxRQUFJLEtBQUt0USxLQUFMLENBQVd4QyxNQUFYLENBQWtCMEUsTUFBdEIsRUFBOEI7QUFDMUJvTyxNQUFBQSxVQUFVLEdBQUksNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMsc0JBQTVCO0FBQ1YsUUFBQSxPQUFPLEVBQUUsS0FBS3RHLFFBREo7QUFFVixRQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFIO0FBRkcsUUFBZDtBQUlIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQyxlQUFmO0FBQStCLE1BQUEsSUFBSSxFQUFDO0FBQXBDLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ01zRyxVQUROLEVBRU1MLGNBRk4sRUFHSSx5Q0FBTXRCLFVBQU4sQ0FISixDQURKLEVBTU0wQixhQU5OLEVBT0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ00sS0FBS3JRLEtBQUwsQ0FBV3hDLE1BQVgsQ0FBa0IyQyxNQUR4QixDQURKLEVBSU02UCxpQkFKTixDQUZKLENBUEosRUFnQkksNkJBQUMsMEJBQUQ7QUFBbUIsTUFBQSxTQUFTLEVBQUM7QUFBN0IsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTSxLQUFLL0Usa0JBQUwsRUFETixFQUdNeUQsVUFITixFQUtNbkMsU0FMTixFQU9NLEtBQUs1QixjQUFMLEVBUE4sRUFTTW1DLE9BVE4sQ0FESixDQWhCSixDQURKO0FBZ0NIO0FBMWxDMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbi8qXHJcbiAqIFN0YXRlIHZhcnM6XHJcbiAqICdjYW4nOiB7XHJcbiAqICAga2ljazogYm9vbGVhbixcclxuICogICBiYW46IGJvb2xlYW4sXHJcbiAqICAgbXV0ZTogYm9vbGVhbixcclxuICogICBtb2RpZnlMZXZlbDogYm9vbGVhblxyXG4gKiB9LFxyXG4gKiAnbXV0ZWQnOiBib29sZWFuLFxyXG4gKiAnaXNUYXJnZXRNb2QnOiBib29sZWFuXHJcbiAqL1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgY3JlYXRlUm9vbSBmcm9tICcuLi8uLi8uLi9jcmVhdGVSb29tJztcclxuaW1wb3J0IERNUm9vbU1hcCBmcm9tICcuLi8uLi8uLi91dGlscy9ETVJvb21NYXAnO1xyXG5pbXBvcnQgKiBhcyBVbnJlYWQgZnJvbSAnLi4vLi4vLi4vVW5yZWFkJztcclxuaW1wb3J0IHsgZmluZFJlYWRSZWNlaXB0RnJvbVVzZXJJZCB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL1JlY2VpcHQnO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tICcuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi4vLi4vLi4vc3RvcmVzL1Jvb21WaWV3U3RvcmUnO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gJy4uLy4uLy4uL1Nka0NvbmZpZyc7XHJcbmltcG9ydCBNdWx0aUludml0ZXIgZnJvbSBcIi4uLy4uLy4uL3V0aWxzL011bHRpSW52aXRlclwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgRTJFSWNvbiBmcm9tIFwiLi9FMkVJY29uXCI7XHJcbmltcG9ydCBBdXRvSGlkZVNjcm9sbGJhciBmcm9tIFwiLi4vLi4vc3RydWN0dXJlcy9BdXRvSGlkZVNjcm9sbGJhclwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgTWF0cml4Q2xpZW50Q29udGV4dCBmcm9tIFwiLi4vLi4vLi4vY29udGV4dHMvTWF0cml4Q2xpZW50Q29udGV4dFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ01lbWJlckluZm8nLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIG1lbWJlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNhbjoge1xyXG4gICAgICAgICAgICAgICAga2ljazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBiYW46IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgbXV0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBtb2RpZnlMZXZlbDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBzeW5hcHNlRGVhY3RpdmF0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByZWRhY3RNZXNzYWdlczogZmFsc2UsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIG11dGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgaXNUYXJnZXRNb2Q6IGZhbHNlLFxyXG4gICAgICAgICAgICB1cGRhdGluZzogMCxcclxuICAgICAgICAgICAgZGV2aWNlc0xvYWRpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIGRldmljZXM6IG51bGwsXHJcbiAgICAgICAgICAgIGlzSWdub3Jpbmc6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXRpY3M6IHtcclxuICAgICAgICBjb250ZXh0VHlwZTogTWF0cml4Q2xpZW50Q29udGV4dCxcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIE1vdmUgdGhpcyB0byBjb25zdHJ1Y3RvclxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fY2FuY2VsRGV2aWNlTGlzdCA9IG51bGw7XHJcbiAgICAgICAgY29uc3QgY2xpID0gdGhpcy5jb250ZXh0O1xyXG5cclxuICAgICAgICAvLyBvbmx5IGRpc3BsYXkgdGhlIGRldmljZXMgbGlzdCBpZiBvdXIgY2xpZW50IHN1cHBvcnRzIEUyRVxyXG4gICAgICAgIHRoaXMuX2VuYWJsZURldmljZXMgPSBjbGkuaXNDcnlwdG9FbmFibGVkKCk7XHJcblxyXG4gICAgICAgIGNsaS5vbihcImRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWRcIiwgdGhpcy5vbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQpO1xyXG4gICAgICAgIGNsaS5vbihcIlJvb21cIiwgdGhpcy5vblJvb20pO1xyXG4gICAgICAgIGNsaS5vbihcImRlbGV0ZVJvb21cIiwgdGhpcy5vbkRlbGV0ZVJvb20pO1xyXG4gICAgICAgIGNsaS5vbihcIlJvb20udGltZWxpbmVcIiwgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbS5uYW1lXCIsIHRoaXMub25Sb29tTmFtZSk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICBjbGkub24oXCJSb29tTWVtYmVyLm5hbWVcIiwgdGhpcy5vblJvb21NZW1iZXJOYW1lKTtcclxuICAgICAgICBjbGkub24oXCJSb29tTWVtYmVyLm1lbWJlcnNoaXBcIiwgdGhpcy5vblJvb21NZW1iZXJNZW1iZXJzaGlwKTtcclxuICAgICAgICBjbGkub24oXCJhY2NvdW50RGF0YVwiLCB0aGlzLm9uQWNjb3VudERhdGEpO1xyXG5cclxuICAgICAgICB0aGlzLl9jaGVja0lnbm9yZVN0YXRlKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVN0YXRlRm9yTmV3TWVtYmVyKHRoaXMucHJvcHMubWVtYmVyKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2Ugd2l0aCBhcHByb3ByaWF0ZSBsaWZlY3ljbGUgZXZlbnRcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbihuZXdQcm9wcykge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm1lbWJlci51c2VySWQgIT09IG5ld1Byb3BzLm1lbWJlci51c2VySWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fdXBkYXRlU3RhdGVGb3JOZXdNZW1iZXIobmV3UHJvcHMubWVtYmVyKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSB0aGlzLmNvbnRleHQ7XHJcbiAgICAgICAgaWYgKGNsaWVudCkge1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJkZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkXCIsIHRoaXMub25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkKTtcclxuICAgICAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKFwiUm9vbVwiLCB0aGlzLm9uUm9vbSk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcImRlbGV0ZVJvb21cIiwgdGhpcy5vbkRlbGV0ZVJvb20pO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLnRpbWVsaW5lXCIsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLm5hbWVcIiwgdGhpcy5vblJvb21OYW1lKTtcclxuICAgICAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb21TdGF0ZS5ldmVudHNcIiwgdGhpcy5vblJvb21TdGF0ZUV2ZW50cyk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb21NZW1iZXIubmFtZVwiLCB0aGlzLm9uUm9vbU1lbWJlck5hbWUpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tTWVtYmVyLm1lbWJlcnNoaXBcIiwgdGhpcy5vblJvb21NZW1iZXJNZW1iZXJzaGlwKTtcclxuICAgICAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKFwiYWNjb3VudERhdGFcIiwgdGhpcy5vbkFjY291bnREYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX2NhbmNlbERldmljZUxpc3QpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2FuY2VsRGV2aWNlTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2NoZWNrSWdub3JlU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGlzSWdub3JpbmcgPSB0aGlzLmNvbnRleHQuaXNVc2VySWdub3JlZCh0aGlzLnByb3BzLm1lbWJlci51c2VySWQpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzSWdub3Jpbmc6IGlzSWdub3Jpbmd9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2Rpc2FtYmlndWF0ZURldmljZXM6IGZ1bmN0aW9uKGRldmljZXMpIHtcclxuICAgICAgICBjb25zdCBuYW1lcyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBkZXZpY2VzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5hbWUgPSBkZXZpY2VzW2ldLmdldERpc3BsYXlOYW1lKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4TGlzdCA9IG5hbWVzW25hbWVdIHx8IFtdO1xyXG4gICAgICAgICAgICBpbmRleExpc3QucHVzaChpKTtcclxuICAgICAgICAgICAgbmFtZXNbbmFtZV0gPSBpbmRleExpc3Q7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoY29uc3QgbmFtZSBpbiBuYW1lcykge1xyXG4gICAgICAgICAgICBpZiAobmFtZXNbbmFtZV0ubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgbmFtZXNbbmFtZV0uZm9yRWFjaCgoaik9PntcclxuICAgICAgICAgICAgICAgICAgICBkZXZpY2VzW2pdLmFtYmlndW91cyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkOiBmdW5jdGlvbih1c2VySWQsIGRldmljZSkge1xyXG4gICAgICAgIGlmICghdGhpcy5fZW5hYmxlRGV2aWNlcykge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodXNlcklkID09PSB0aGlzLnByb3BzLm1lbWJlci51c2VySWQpIHtcclxuICAgICAgICAgICAgLy8gbm8gbmVlZCB0byByZS1kb3dubG9hZCB0aGUgd2hvbGUgdGhpbmc7IGp1c3QgdXBkYXRlIG91ciBjb3B5IG9mXHJcbiAgICAgICAgICAgIC8vIHRoZSBsaXN0LlxyXG5cclxuICAgICAgICAgICAgLy8gUHJvbWlzZS5yZXNvbHZlIHRvIGhhbmRsZSB0cmFuc2l0aW9uIGZyb20gc3RhdGljIHJlc3VsdCB0byBwcm9taXNlOyBjYW4gYmUgcmVtb3ZlZFxyXG4gICAgICAgICAgICAvLyBpbiBmdXR1cmVcclxuICAgICAgICAgICAgUHJvbWlzZS5yZXNvbHZlKHRoaXMuY29udGV4dC5nZXRTdG9yZWREZXZpY2VzRm9yVXNlcih1c2VySWQpKS50aGVuKChkZXZpY2VzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBkZXZpY2VzOiBkZXZpY2VzLFxyXG4gICAgICAgICAgICAgICAgICAgIGUyZVN0YXR1czogdGhpcy5fZ2V0RTJFU3RhdHVzKGRldmljZXMpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2dldEUyRVN0YXR1czogZnVuY3Rpb24oZGV2aWNlcykge1xyXG4gICAgICAgIGNvbnN0IGhhc1VudmVyaWZpZWREZXZpY2UgPSBkZXZpY2VzLnNvbWUoKGRldmljZSkgPT4gZGV2aWNlLmlzVW52ZXJpZmllZCgpKTtcclxuICAgICAgICByZXR1cm4gaGFzVW52ZXJpZmllZERldmljZSA/IFwid2FybmluZ1wiIDogXCJ2ZXJpZmllZFwiO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb206IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uRGVsZXRlUm9vbTogZnVuY3Rpb24ocm9vbUlkKSB7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21UaW1lbGluZTogZnVuY3Rpb24oZXYsIHJvb20sIHRvU3RhcnRPZlRpbWVsaW5lKSB7XHJcbiAgICAgICAgaWYgKHRvU3RhcnRPZlRpbWVsaW5lKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21OYW1lOiBmdW5jdGlvbihyb29tKSB7XHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21SZWNlaXB0OiBmdW5jdGlvbihyZWNlaXB0RXZlbnQsIHJvb20pIHtcclxuICAgICAgICAvLyBiZWNhdXNlIGlmIHdlIHJlYWQgYSBub3RpZmljYXRpb24sIGl0IHdpbGwgYWZmZWN0IG5vdGlmaWNhdGlvbiBjb3VudFxyXG4gICAgICAgIC8vIG9ubHkgYm90aGVyIHVwZGF0aW5nIGlmIHRoZXJlJ3MgYSByZWNlaXB0IGZyb20gdXNcclxuICAgICAgICBpZiAoZmluZFJlYWRSZWNlaXB0RnJvbVVzZXJJZChyZWNlaXB0RXZlbnQsIHRoaXMuY29udGV4dC5jcmVkZW50aWFscy51c2VySWQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVN0YXRlRXZlbnRzOiBmdW5jdGlvbihldiwgc3RhdGUpIHtcclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbU1lbWJlck5hbWU6IGZ1bmN0aW9uKGV2LCBtZW1iZXIpIHtcclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbU1lbWJlck1lbWJlcnNoaXA6IGZ1bmN0aW9uKGV2LCBtZW1iZXIpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5tZW1iZXIudXNlcklkID09PSBtZW1iZXIudXNlcklkKSB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWNjb3VudERhdGE6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgaWYgKGV2LmdldFR5cGUoKSA9PT0gJ20uZGlyZWN0Jykge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfdXBkYXRlU3RhdGVGb3JOZXdNZW1iZXI6IGFzeW5jIGZ1bmN0aW9uKG1lbWJlcikge1xyXG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0gYXdhaXQgdGhpcy5fY2FsY3VsYXRlT3BzUGVybWlzc2lvbnMobWVtYmVyKTtcclxuICAgICAgICBuZXdTdGF0ZS5kZXZpY2VzTG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgbmV3U3RhdGUuZGV2aWNlcyA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9jYW5jZWxEZXZpY2VMaXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NhbmNlbERldmljZUxpc3QoKTtcclxuICAgICAgICAgICAgdGhpcy5fY2FuY2VsRGV2aWNlTGlzdCA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9kb3dubG9hZERldmljZUxpc3QobWVtYmVyKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2Rvd25sb2FkRGV2aWNlTGlzdDogZnVuY3Rpb24obWVtYmVyKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9lbmFibGVEZXZpY2VzKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBjYW5jZWxsZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9jYW5jZWxEZXZpY2VMaXN0ID0gZnVuY3Rpb24oKSB7IGNhbmNlbGxlZCA9IHRydWU7IH07XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IHRoaXMuY29udGV4dDtcclxuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICBjbGllbnQuZG93bmxvYWRLZXlzKFttZW1iZXIudXNlcklkXSwgdHJ1ZSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBjbGllbnQuZ2V0U3RvcmVkRGV2aWNlc0ZvclVzZXIobWVtYmVyLnVzZXJJZCk7XHJcbiAgICAgICAgfSkuZmluYWxseShmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgc2VsZi5fY2FuY2VsRGV2aWNlTGlzdCA9IG51bGw7XHJcbiAgICAgICAgfSkudGhlbihmdW5jdGlvbihkZXZpY2VzKSB7XHJcbiAgICAgICAgICAgIGlmIChjYW5jZWxsZWQpIHtcclxuICAgICAgICAgICAgICAgIC8vIHdlIGdvdCBjYW5jZWxsZWQgLSBwcmVzdW1hYmx5IGEgZGlmZmVyZW50IHVzZXIgbm93XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHNlbGYuX2Rpc2FtYmlndWF0ZURldmljZXMoZGV2aWNlcyk7XHJcbiAgICAgICAgICAgIHNlbGYuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgZGV2aWNlc0xvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZGV2aWNlczogZGV2aWNlcyxcclxuICAgICAgICAgICAgICAgIGUyZVN0YXR1czogc2VsZi5fZ2V0RTJFU3RhdHVzKGRldmljZXMpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBkb3dubG9hZGluZyBzZXNzaW9uc1wiLCBlcnIpO1xyXG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHtkZXZpY2VzTG9hZGluZzogZmFsc2V9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25JZ25vcmVUb2dnbGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGlnbm9yZWRVc2VycyA9IHRoaXMuY29udGV4dC5nZXRJZ25vcmVkVXNlcnMoKTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5pc0lnbm9yaW5nKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gaWdub3JlZFVzZXJzLmluZGV4T2YodGhpcy5wcm9wcy5tZW1iZXIudXNlcklkKTtcclxuICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkgaWdub3JlZFVzZXJzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWdub3JlZFVzZXJzLnB1c2godGhpcy5wcm9wcy5tZW1iZXIudXNlcklkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dC5zZXRJZ25vcmVkVXNlcnMoaWdub3JlZFVzZXJzKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2V0U3RhdGUoe2lzSWdub3Jpbmc6ICF0aGlzLnN0YXRlLmlzSWdub3Jpbmd9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25LaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBtZW1iZXJzaGlwID0gdGhpcy5wcm9wcy5tZW1iZXIubWVtYmVyc2hpcDtcclxuICAgICAgICBjb25zdCBDb25maXJtVXNlckFjdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkNvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NvbmZpcm0gVXNlciBBY3Rpb24gRGlhbG9nJywgJ29uS2ljaycsIENvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIG1lbWJlcjogdGhpcy5wcm9wcy5tZW1iZXIsXHJcbiAgICAgICAgICAgIGFjdGlvbjogbWVtYmVyc2hpcCA9PT0gXCJpbnZpdGVcIiA/IF90KFwiRGlzaW52aXRlXCIpIDogX3QoXCJLaWNrXCIpLFxyXG4gICAgICAgICAgICB0aXRsZTogbWVtYmVyc2hpcCA9PT0gXCJpbnZpdGVcIiA/IF90KFwiRGlzaW52aXRlIHRoaXMgdXNlcj9cIikgOiBfdChcIktpY2sgdGhpcyB1c2VyP1wiKSxcclxuICAgICAgICAgICAgYXNrUmVhc29uOiBtZW1iZXJzaGlwID09PSBcImpvaW5cIixcclxuICAgICAgICAgICAgZGFuZ2VyOiB0cnVlLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAocHJvY2VlZCwgcmVhc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXByb2NlZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdXBkYXRpbmc6IHRoaXMuc3RhdGUudXBkYXRpbmcgKyAxIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250ZXh0LmtpY2soXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkLCB0aGlzLnByb3BzLm1lbWJlci51c2VySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVhc29uIHx8IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgICAgICkudGhlbihmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gTk8tT1A7IHJlbHkgb24gdGhlIG0ucm9vbS5tZW1iZXIgZXZlbnQgY29taW5nIGRvd24gZWxzZSB3ZSBjb3VsZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBnZXQgb3V0IG9mIHN5bmMgaWYgd2UgZm9yY2Ugc2V0U3RhdGUgaGVyZSFcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJLaWNrIHN1Y2Nlc3NcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJLaWNrIGVycm9yOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBraWNrJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJGYWlsZWQgdG8ga2lja1wiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IFwiT3BlcmF0aW9uIGZhaWxlZFwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICkuZmluYWxseSgoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1cGRhdGluZzogdGhpcy5zdGF0ZS51cGRhdGluZyAtIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25CYW5PclVuYmFuOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBDb25maXJtVXNlckFjdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkNvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NvbmZpcm0gVXNlciBBY3Rpb24gRGlhbG9nJywgJ29uQmFuT3JVbmJhbicsIENvbmZpcm1Vc2VyQWN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIG1lbWJlcjogdGhpcy5wcm9wcy5tZW1iZXIsXHJcbiAgICAgICAgICAgIGFjdGlvbjogdGhpcy5wcm9wcy5tZW1iZXIubWVtYmVyc2hpcCA9PT0gJ2JhbicgPyBfdChcIlVuYmFuXCIpIDogX3QoXCJCYW5cIiksXHJcbiAgICAgICAgICAgIHRpdGxlOiB0aGlzLnByb3BzLm1lbWJlci5tZW1iZXJzaGlwID09PSAnYmFuJyA/IF90KFwiVW5iYW4gdGhpcyB1c2VyP1wiKSA6IF90KFwiQmFuIHRoaXMgdXNlcj9cIiksXHJcbiAgICAgICAgICAgIGFza1JlYXNvbjogdGhpcy5wcm9wcy5tZW1iZXIubWVtYmVyc2hpcCAhPT0gJ2JhbicsXHJcbiAgICAgICAgICAgIGRhbmdlcjogdGhpcy5wcm9wcy5tZW1iZXIubWVtYmVyc2hpcCAhPT0gJ2JhbicsXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IChwcm9jZWVkLCByZWFzb24pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghcHJvY2VlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1cGRhdGluZzogdGhpcy5zdGF0ZS51cGRhdGluZyArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcHJvbWlzZTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm1lbWJlci5tZW1iZXJzaGlwID09PSAnYmFuJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb21pc2UgPSB0aGlzLmNvbnRleHQudW5iYW4oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMubWVtYmVyLnJvb21JZCwgdGhpcy5wcm9wcy5tZW1iZXIudXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb21pc2UgPSB0aGlzLmNvbnRleHQuYmFuKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm1lbWJlci5yb29tSWQsIHRoaXMucHJvcHMubWVtYmVyLnVzZXJJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhc29uIHx8IHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcHJvbWlzZS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBOTy1PUDsgcmVseSBvbiB0aGUgbS5yb29tLm1lbWJlciBldmVudCBjb21pbmcgZG93biBlbHNlIHdlIGNvdWxkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGdldCBvdXQgb2Ygc3luYyBpZiB3ZSBmb3JjZSBzZXRTdGF0ZSBoZXJlIVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkJhbiBzdWNjZXNzXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQmFuIGVycm9yOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBiYW4gdXNlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJGYWlsZWQgdG8gYmFuIHVzZXJcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICApLmZpbmFsbHkoKCk9PntcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdXBkYXRpbmc6IHRoaXMuc3RhdGUudXBkYXRpbmcgLSAxIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVkYWN0QWxsTWVzc2FnZXM6IGFzeW5jIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHtyb29tSWQsIHVzZXJJZH0gPSB0aGlzLnByb3BzLm1lbWJlcjtcclxuICAgICAgICBjb25zdCByb29tID0gdGhpcy5jb250ZXh0LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB0aW1lbGluZVNldCA9IHJvb20uZ2V0VW5maWx0ZXJlZFRpbWVsaW5lU2V0KCk7XHJcbiAgICAgICAgbGV0IGV2ZW50c1RvUmVkYWN0ID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCB0aW1lbGluZSBvZiB0aW1lbGluZVNldC5nZXRUaW1lbGluZXMoKSkge1xyXG4gICAgICAgICAgICBldmVudHNUb1JlZGFjdCA9IHRpbWVsaW5lLmdldEV2ZW50cygpLnJlZHVjZSgoZXZlbnRzLCBldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50LmdldFNlbmRlcigpID09PSB1c2VySWQgJiYgIWV2ZW50LmlzUmVkYWN0ZWQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudHMuY29uY2F0KGV2ZW50KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50cztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXZlbnRzVG9SZWRhY3QpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY291bnQgPSBldmVudHNUb1JlZGFjdC5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgdXNlciA9IHRoaXMucHJvcHMubWVtYmVyLm5hbWU7XHJcblxyXG4gICAgICAgIGlmIChjb3VudCA9PT0gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBJbmZvRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuSW5mb0RpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTm8gdXNlciBtZXNzYWdlcyBmb3VuZCB0byByZW1vdmUnLCAnJywgSW5mb0RpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiTm8gcmVjZW50IG1lc3NhZ2VzIGJ5ICUodXNlcilzIGZvdW5kXCIsIHt1c2VyfSksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD57IF90KFwiVHJ5IHNjcm9sbGluZyB1cCBpbiB0aGUgdGltZWxpbmUgdG8gc2VlIGlmIHRoZXJlIGFyZSBhbnkgZWFybGllciBvbmVzLlwiKSB9PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc3QgY29uZmlybWVkID0gYXdhaXQgbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlbW92ZSByZWNlbnQgbWVzc2FnZXMgYnkgdXNlcicsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlJlbW92ZSByZWNlbnQgbWVzc2FnZXMgYnkgJSh1c2VyKXNcIiwge3VzZXJ9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnsgX3QoXCJZb3UgYXJlIGFib3V0IHRvIHJlbW92ZSAlKGNvdW50KXMgbWVzc2FnZXMgYnkgJSh1c2VyKXMuIFRoaXMgY2Fubm90IGJlIHVuZG9uZS4gRG8geW91IHdpc2ggdG8gY29udGludWU/XCIsIHtjb3VudCwgdXNlcn0pIH08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD57IF90KFwiRm9yIGEgbGFyZ2UgYW1vdW50IG9mIG1lc3NhZ2VzLCB0aGlzIG1pZ2h0IHRha2Ugc29tZSB0aW1lLiBQbGVhc2UgZG9uJ3QgcmVmcmVzaCB5b3VyIGNsaWVudCBpbiB0aGUgbWVhbnRpbWUuXCIpIH08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICAgICAgICAgICAgICBidXR0b246IF90KFwiUmVtb3ZlICUoY291bnQpcyBtZXNzYWdlc1wiLCB7Y291bnR9KSxcclxuICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiByZXNvbHZlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFjb25maXJtZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gU3VibWl0dGluZyBhIGxhcmdlIG51bWJlciBvZiByZWRhY3Rpb25zIGZyZWV6ZXMgdGhlIFVJLFxyXG4gICAgICAgICAgICAvLyBzbyBmaXJzdCB5aWVsZCB0byBhbGxvdyB0byByZXJlbmRlciBhZnRlciBjbG9zaW5nIHRoZSBkaWFsb2cuXHJcbiAgICAgICAgICAgIGF3YWl0IFByb21pc2UucmVzb2x2ZSgpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKGBTdGFydGVkIHJlZGFjdGluZyByZWNlbnQgJHtjb3VudH0gbWVzc2FnZXMgZm9yICR7dXNlcn0gaW4gJHtyb29tSWR9YCk7XHJcbiAgICAgICAgICAgIGF3YWl0IFByb21pc2UuYWxsKGV2ZW50c1RvUmVkYWN0Lm1hcChhc3luYyBldmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuY29udGV4dC5yZWRhY3RFdmVudChyb29tSWQsIGV2ZW50LmdldElkKCkpO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGFuZCBzd2FsbG93IGVycm9yc1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJDb3VsZCBub3QgcmVkYWN0XCIsIGV2ZW50LmdldElkKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oYEZpbmlzaGVkIHJlZGFjdGluZyByZWNlbnQgJHtjb3VudH0gbWVzc2FnZXMgZm9yICR7dXNlcn0gaW4gJHtyb29tSWR9YCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfd2FyblNlbGZEZW1vdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0RlbW90aW5nIFNlbGYnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkRlbW90ZSB5b3Vyc2VsZj9cIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiWW91IHdpbGwgbm90IGJlIGFibGUgdG8gdW5kbyB0aGlzIGNoYW5nZSBhcyB5b3UgYXJlIGRlbW90aW5nIHlvdXJzZWxmLCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlmIHlvdSBhcmUgdGhlIGxhc3QgcHJpdmlsZWdlZCB1c2VyIGluIHRoZSByb29tIGl0IHdpbGwgYmUgaW1wb3NzaWJsZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRvIHJlZ2FpbiBwcml2aWxlZ2VzLlwiKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICAgICAgYnV0dG9uOiBfdChcIkRlbW90ZVwiKSxcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IHJlc29sdmUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbk11dGVUb2dnbGU6IGFzeW5jIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gdGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldCA9IHRoaXMucHJvcHMubWVtYmVyLnVzZXJJZDtcclxuICAgICAgICBjb25zdCByb29tID0gdGhpcy5jb250ZXh0LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20pIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gaWYgbXV0aW5nIHNlbGYsIHdhcm4gYXMgaXQgbWF5IGJlIGlycmV2ZXJzaWJsZVxyXG4gICAgICAgIGlmICh0YXJnZXQgPT09IHRoaXMuY29udGV4dC5nZXRVc2VySWQoKSkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEoYXdhaXQgdGhpcy5fd2FyblNlbGZEZW1vdGUoKSkpIHJldHVybjtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byB3YXJuIGFib3V0IHNlbGYgZGVtb3Rpb246IFwiLCBlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbEV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIFwiXCIpO1xyXG4gICAgICAgIGlmICghcG93ZXJMZXZlbEV2ZW50KSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IGlzTXV0ZWQgPSB0aGlzLnN0YXRlLm11dGVkO1xyXG4gICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxzID0gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICBjb25zdCBsZXZlbFRvU2VuZCA9IChcclxuICAgICAgICAgICAgKHBvd2VyTGV2ZWxzLmV2ZW50cyA/IHBvd2VyTGV2ZWxzLmV2ZW50c1tcIm0ucm9vbS5tZXNzYWdlXCJdIDogbnVsbCkgfHxcclxuICAgICAgICAgICAgcG93ZXJMZXZlbHMuZXZlbnRzX2RlZmF1bHRcclxuICAgICAgICApO1xyXG4gICAgICAgIGxldCBsZXZlbDtcclxuICAgICAgICBpZiAoaXNNdXRlZCkgeyAvLyB1bm11dGVcclxuICAgICAgICAgICAgbGV2ZWwgPSBsZXZlbFRvU2VuZDtcclxuICAgICAgICB9IGVsc2UgeyAvLyBtdXRlXHJcbiAgICAgICAgICAgIGxldmVsID0gbGV2ZWxUb1NlbmQgLSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXZlbCA9IHBhcnNlSW50KGxldmVsKTtcclxuXHJcbiAgICAgICAgaWYgKCFpc05hTihsZXZlbCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHVwZGF0aW5nOiB0aGlzLnN0YXRlLnVwZGF0aW5nICsgMSB9KTtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0LnNldFBvd2VyTGV2ZWwocm9vbUlkLCB0YXJnZXQsIGxldmVsLCBwb3dlckxldmVsRXZlbnQpLnRoZW4oXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBOTy1PUDsgcmVseSBvbiB0aGUgbS5yb29tLm1lbWJlciBldmVudCBjb21pbmcgZG93biBlbHNlIHdlIGNvdWxkXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZ2V0IG91dCBvZiBzeW5jIGlmIHdlIGZvcmNlIHNldFN0YXRlIGhlcmUhXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJNdXRlIHRvZ2dsZSBzdWNjZXNzXCIpO1xyXG4gICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIk11dGUgZXJyb3I6IFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gbXV0ZSB1c2VyJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkVycm9yXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJGYWlsZWQgdG8gbXV0ZSB1c2VyXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKS5maW5hbGx5KCgpPT57XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdXBkYXRpbmc6IHRoaXMuc3RhdGUudXBkYXRpbmcgLSAxIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTW9kVG9nZ2xlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgIGNvbnN0IHJvb21JZCA9IHRoaXMucHJvcHMubWVtYmVyLnJvb21JZDtcclxuICAgICAgICBjb25zdCB0YXJnZXQgPSB0aGlzLnByb3BzLm1lbWJlci51c2VySWQ7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuY29udGV4dC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLnBvd2VyX2xldmVsc1wiLCBcIlwiKTtcclxuICAgICAgICBpZiAoIXBvd2VyTGV2ZWxFdmVudCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBtZSA9IHJvb20uZ2V0TWVtYmVyKHRoaXMuY29udGV4dC5jcmVkZW50aWFscy51c2VySWQpO1xyXG4gICAgICAgIGlmICghbWUpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgZGVmYXVsdExldmVsID0gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKS51c2Vyc19kZWZhdWx0O1xyXG4gICAgICAgIGxldCBtb2RMZXZlbCA9IG1lLnBvd2VyTGV2ZWwgLSAxO1xyXG4gICAgICAgIGlmIChtb2RMZXZlbCA+IDUwICYmIGRlZmF1bHRMZXZlbCA8IDUwKSBtb2RMZXZlbCA9IDUwOyAvLyB0cnkgdG8gc3RpY2sgd2l0aCB0aGUgdmVjdG9yIGxldmVsIGRlZmF1bHRzXHJcbiAgICAgICAgLy8gdG9nZ2xlIHRoZSBsZXZlbFxyXG4gICAgICAgIGNvbnN0IG5ld0xldmVsID0gdGhpcy5zdGF0ZS5pc1RhcmdldE1vZCA/IGRlZmF1bHRMZXZlbCA6IG1vZExldmVsO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1cGRhdGluZzogdGhpcy5zdGF0ZS51cGRhdGluZyArIDEgfSk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0LnNldFBvd2VyTGV2ZWwocm9vbUlkLCB0YXJnZXQsIHBhcnNlSW50KG5ld0xldmVsKSwgcG93ZXJMZXZlbEV2ZW50KS50aGVuKFxyXG4gICAgICAgICAgICBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIC8vIE5PLU9QOyByZWx5IG9uIHRoZSBtLnJvb20ubWVtYmVyIGV2ZW50IGNvbWluZyBkb3duIGVsc2Ugd2UgY291bGRcclxuICAgICAgICAgICAgICAgIC8vIGdldCBvdXQgb2Ygc3luYyBpZiB3ZSBmb3JjZSBzZXRTdGF0ZSBoZXJlIVxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJNb2QgdG9nZ2xlIHN1Y2Nlc3NcIik7XHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlID09PSAnTV9HVUVTVF9BQ0NFU1NfRk9SQklEREVOJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAncmVxdWlyZV9yZWdpc3RyYXRpb24nfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJUb2dnbGUgbW9kZXJhdG9yIGVycm9yOlwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gdG9nZ2xlIG1vZGVyYXRvciBzdGF0dXMnLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBfdChcIkZhaWxlZCB0byB0b2dnbGUgbW9kZXJhdG9yIHN0YXR1c1wiKSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApLmZpbmFsbHkoKCk9PntcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHVwZGF0aW5nOiB0aGlzLnN0YXRlLnVwZGF0aW5nIC0gMSB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TeW5hcHNlRGVhY3RpdmF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLlF1ZXN0aW9uRGlhbG9nJyk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU3luYXBzZSBVc2VyIERlYWN0aXZhdGlvbicsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoXCJEZWFjdGl2YXRlIHVzZXI/XCIpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAgIDxkaXY+eyBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIkRlYWN0aXZhdGluZyB0aGlzIHVzZXIgd2lsbCBsb2cgdGhlbSBvdXQgYW5kIHByZXZlbnQgdGhlbSBmcm9tIGxvZ2dpbmcgYmFjayBpbi4gQWRkaXRpb25hbGx5LCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0aGV5IHdpbGwgbGVhdmUgYWxsIHRoZSByb29tcyB0aGV5IGFyZSBpbi4gVGhpcyBhY3Rpb24gY2Fubm90IGJlIHJldmVyc2VkLiBBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZGVhY3RpdmF0ZSB0aGlzIHVzZXI/XCJcclxuICAgICAgICAgICAgICAgICkgfTwvZGl2PixcclxuICAgICAgICAgICAgYnV0dG9uOiBfdChcIkRlYWN0aXZhdGUgdXNlclwiKSxcclxuICAgICAgICAgICAgZGFuZ2VyOiB0cnVlLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAoYWNjZXB0ZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghYWNjZXB0ZWQpIHJldHVybjtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGV4dC5kZWFjdGl2YXRlU3luYXBzZVVzZXIodGhpcy5wcm9wcy5tZW1iZXIudXNlcklkKS5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIGRlYWN0aXZhdGUgdXNlclwiKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBkZWFjdGl2YXRlIFN5bmFwc2UgdXNlcicsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0ZhaWxlZCB0byBkZWFjdGl2YXRlIHVzZXInKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZSAmJiBlLm1lc3NhZ2UpID8gZS5tZXNzYWdlIDogX3QoXCJPcGVyYXRpb24gZmFpbGVkXCIpKSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfYXBwbHlQb3dlckNoYW5nZTogZnVuY3Rpb24ocm9vbUlkLCB0YXJnZXQsIHBvd2VyTGV2ZWwsIHBvd2VyTGV2ZWxFdmVudCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1cGRhdGluZzogdGhpcy5zdGF0ZS51cGRhdGluZyArIDEgfSk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0LnNldFBvd2VyTGV2ZWwocm9vbUlkLCB0YXJnZXQsIHBhcnNlSW50KHBvd2VyTGV2ZWwpLCBwb3dlckxldmVsRXZlbnQpLnRoZW4oXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTk8tT1A7IHJlbHkgb24gdGhlIG0ucm9vbS5tZW1iZXIgZXZlbnQgY29taW5nIGRvd24gZWxzZSB3ZSBjb3VsZFxyXG4gICAgICAgICAgICAgICAgLy8gZ2V0IG91dCBvZiBzeW5jIGlmIHdlIGZvcmNlIHNldFN0YXRlIGhlcmUhXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlBvd2VyIGNoYW5nZSBzdWNjZXNzXCIpO1xyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIGNoYW5nZSBwb3dlciBsZXZlbCBcIiArIGVycik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gY2hhbmdlIHBvd2VyIGxldmVsJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiRXJyb3JcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFwiRmFpbGVkIHRvIGNoYW5nZSBwb3dlciBsZXZlbFwiKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICkuZmluYWxseSgoKT0+e1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdXBkYXRpbmc6IHRoaXMuc3RhdGUudXBkYXRpbmcgLSAxIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblBvd2VyQ2hhbmdlOiBhc3luYyBmdW5jdGlvbihwb3dlckxldmVsKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gdGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldCA9IHRoaXMucHJvcHMubWVtYmVyLnVzZXJJZDtcclxuICAgICAgICBjb25zdCByb29tID0gdGhpcy5jb250ZXh0LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20pIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbEV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIFwiXCIpO1xyXG4gICAgICAgIGlmICghcG93ZXJMZXZlbEV2ZW50KSByZXR1cm47XHJcblxyXG4gICAgICAgIGlmICghcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKS51c2Vycykge1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseVBvd2VyQ2hhbmdlKHJvb21JZCwgdGFyZ2V0LCBwb3dlckxldmVsLCBwb3dlckxldmVsRXZlbnQpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBteVVzZXJJZCA9IHRoaXMuY29udGV4dC5nZXRVc2VySWQoKTtcclxuICAgICAgICBjb25zdCBRdWVzdGlvbkRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nXCIpO1xyXG5cclxuICAgICAgICAvLyBJZiB3ZSBhcmUgY2hhbmdpbmcgb3VyIG93biBQTCBpdCBjYW4gb25seSBldmVyIGJlIGRlY3JlYXNpbmcsIHdoaWNoIHdlIGNhbm5vdCByZXZlcnNlLlxyXG4gICAgICAgIGlmIChteVVzZXJJZCA9PT0gdGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIShhd2FpdCB0aGlzLl93YXJuU2VsZkRlbW90ZSgpKSkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYXBwbHlQb3dlckNoYW5nZShyb29tSWQsIHRhcmdldCwgcG93ZXJMZXZlbCwgcG93ZXJMZXZlbEV2ZW50KTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byB3YXJuIGFib3V0IHNlbGYgZGVtb3Rpb246IFwiLCBlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBteVBvd2VyID0gcG93ZXJMZXZlbEV2ZW50LmdldENvbnRlbnQoKS51c2Vyc1tteVVzZXJJZF07XHJcbiAgICAgICAgaWYgKHBhcnNlSW50KG15UG93ZXIpID09PSBwYXJzZUludChwb3dlckxldmVsKSkge1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdQcm9tb3RlIHRvIFBMMTAwIFdhcm5pbmcnLCAnJywgUXVlc3Rpb25EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIldhcm5pbmchXCIpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIllvdSB3aWxsIG5vdCBiZSBhYmxlIHRvIHVuZG8gdGhpcyBjaGFuZ2UgYXMgeW91IGFyZSBwcm9tb3RpbmcgdGhlIHVzZXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0byBoYXZlIHRoZSBzYW1lIHBvd2VyIGxldmVsIGFzIHlvdXJzZWxmLlwiKSB9PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJBcmUgeW91IHN1cmU/XCIpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICBidXR0b246IF90KFwiQ29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiAoY29uZmlybWVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvbmZpcm1lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9hcHBseVBvd2VyQ2hhbmdlKHJvb21JZCwgdGFyZ2V0LCBwb3dlckxldmVsLCBwb3dlckxldmVsRXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2FwcGx5UG93ZXJDaGFuZ2Uocm9vbUlkLCB0YXJnZXQsIHBvd2VyTGV2ZWwsIHBvd2VyTGV2ZWxFdmVudCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTmV3RE1DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHVwZGF0aW5nOiB0aGlzLnN0YXRlLnVwZGF0aW5nICsgMSB9KTtcclxuICAgICAgICBjcmVhdGVSb29tKHtkbVVzZXJJZDogdGhpcy5wcm9wcy5tZW1iZXIudXNlcklkfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1cGRhdGluZzogdGhpcy5zdGF0ZS51cGRhdGluZyAtIDEgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTGVhdmVDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnbGVhdmVfcm9vbScsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHRoaXMucHJvcHMubWVtYmVyLnJvb21JZCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2NhbGN1bGF0ZU9wc1Blcm1pc3Npb25zOiBhc3luYyBmdW5jdGlvbihtZW1iZXIpIHtcclxuICAgICAgICBsZXQgY2FuRGVhY3RpdmF0ZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRleHQpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGNhbkRlYWN0aXZhdGUgPSBhd2FpdCB0aGlzLmNvbnRleHQuaXNTeW5hcHNlQWRtaW5pc3RyYXRvcigpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBkZWZhdWx0UGVybXMgPSB7XHJcbiAgICAgICAgICAgIGNhbjoge1xyXG4gICAgICAgICAgICAgICAgLy8gQ2FsY3VsYXRlIHBlcm1pc3Npb25zIGZvciBTeW5hcHNlIGJlZm9yZSBkb2luZyB0aGUgUEwgY2hlY2tzXHJcbiAgICAgICAgICAgICAgICBzeW5hcHNlRGVhY3RpdmF0ZTogY2FuRGVhY3RpdmF0ZSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbXV0ZWQ6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuY29udGV4dC5nZXRSb29tKG1lbWJlci5yb29tSWQpO1xyXG4gICAgICAgIGlmICghcm9vbSkgcmV0dXJuIGRlZmF1bHRQZXJtcztcclxuXHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbHMgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIiwgXCJcIik7XHJcbiAgICAgICAgaWYgKCFwb3dlckxldmVscykgcmV0dXJuIGRlZmF1bHRQZXJtcztcclxuXHJcbiAgICAgICAgY29uc3QgbWUgPSByb29tLmdldE1lbWJlcih0aGlzLmNvbnRleHQuY3JlZGVudGlhbHMudXNlcklkKTtcclxuICAgICAgICBpZiAoIW1lKSByZXR1cm4gZGVmYXVsdFBlcm1zO1xyXG5cclxuICAgICAgICBjb25zdCB0aGVtID0gbWVtYmVyO1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNhbjoge1xyXG4gICAgICAgICAgICAgICAgLi4uZGVmYXVsdFBlcm1zLmNhbixcclxuICAgICAgICAgICAgICAgIC4uLmF3YWl0IHRoaXMuX2NhbGN1bGF0ZUNhblBlcm1pc3Npb25zKG1lLCB0aGVtLCBwb3dlckxldmVscy5nZXRDb250ZW50KCkpLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtdXRlZDogdGhpcy5faXNNdXRlZCh0aGVtLCBwb3dlckxldmVscy5nZXRDb250ZW50KCkpLFxyXG4gICAgICAgICAgICBpc1RhcmdldE1vZDogdGhlbS5wb3dlckxldmVsID4gcG93ZXJMZXZlbHMuZ2V0Q29udGVudCgpLnVzZXJzX2RlZmF1bHQsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgX2NhbGN1bGF0ZUNhblBlcm1pc3Npb25zOiBmdW5jdGlvbihtZSwgdGhlbSwgcG93ZXJMZXZlbHMpIHtcclxuICAgICAgICBjb25zdCBpc01lID0gbWUudXNlcklkID09PSB0aGVtLnVzZXJJZDtcclxuICAgICAgICBjb25zdCBjYW4gPSB7XHJcbiAgICAgICAgICAgIGtpY2s6IGZhbHNlLFxyXG4gICAgICAgICAgICBiYW46IGZhbHNlLFxyXG4gICAgICAgICAgICBtdXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgbW9kaWZ5TGV2ZWw6IGZhbHNlLFxyXG4gICAgICAgICAgICBtb2RpZnlMZXZlbE1heDogMCxcclxuICAgICAgICAgICAgcmVkYWN0TWVzc2FnZXM6IG1lLnBvd2VyTGV2ZWwgPj0gcG93ZXJMZXZlbHMucmVkYWN0LFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGNhbkFmZmVjdFVzZXIgPSB0aGVtLnBvd2VyTGV2ZWwgPCBtZS5wb3dlckxldmVsIHx8IGlzTWU7XHJcbiAgICAgICAgaWYgKCFjYW5BZmZlY3RVc2VyKSB7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5pbmZvKFwiQ2Fubm90IGFmZmVjdCB1c2VyOiAlcyA+PSAlc1wiLCB0aGVtLnBvd2VyTGV2ZWwsIG1lLnBvd2VyTGV2ZWwpO1xyXG4gICAgICAgICAgICByZXR1cm4gY2FuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBlZGl0UG93ZXJMZXZlbCA9IChcclxuICAgICAgICAgICAgKHBvd2VyTGV2ZWxzLmV2ZW50cyA/IHBvd2VyTGV2ZWxzLmV2ZW50c1tcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIl0gOiBudWxsKSB8fFxyXG4gICAgICAgICAgICBwb3dlckxldmVscy5zdGF0ZV9kZWZhdWx0XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgY2FuLmtpY2sgPSBtZS5wb3dlckxldmVsID49IHBvd2VyTGV2ZWxzLmtpY2s7XHJcbiAgICAgICAgY2FuLmJhbiA9IG1lLnBvd2VyTGV2ZWwgPj0gcG93ZXJMZXZlbHMuYmFuO1xyXG4gICAgICAgIGNhbi5pbnZpdGUgPSBtZS5wb3dlckxldmVsID49IHBvd2VyTGV2ZWxzLmludml0ZTtcclxuICAgICAgICBjYW4ubXV0ZSA9IG1lLnBvd2VyTGV2ZWwgPj0gZWRpdFBvd2VyTGV2ZWw7XHJcbiAgICAgICAgY2FuLm1vZGlmeUxldmVsID0gbWUucG93ZXJMZXZlbCA+PSBlZGl0UG93ZXJMZXZlbCAmJiAoaXNNZSB8fCBtZS5wb3dlckxldmVsID4gdGhlbS5wb3dlckxldmVsKTtcclxuICAgICAgICBjYW4ubW9kaWZ5TGV2ZWxNYXggPSBtZS5wb3dlckxldmVsO1xyXG5cclxuICAgICAgICByZXR1cm4gY2FuO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaXNNdXRlZDogZnVuY3Rpb24obWVtYmVyLCBwb3dlckxldmVsQ29udGVudCkge1xyXG4gICAgICAgIGlmICghcG93ZXJMZXZlbENvbnRlbnQgfHwgIW1lbWJlcikgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgICAgICBjb25zdCBsZXZlbFRvU2VuZCA9IChcclxuICAgICAgICAgICAgKHBvd2VyTGV2ZWxDb250ZW50LmV2ZW50cyA/IHBvd2VyTGV2ZWxDb250ZW50LmV2ZW50c1tcIm0ucm9vbS5tZXNzYWdlXCJdIDogbnVsbCkgfHxcclxuICAgICAgICAgICAgcG93ZXJMZXZlbENvbnRlbnQuZXZlbnRzX2RlZmF1bHRcclxuICAgICAgICApO1xyXG4gICAgICAgIHJldHVybiBtZW1iZXIucG93ZXJMZXZlbCA8IGxldmVsVG9TZW5kO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNhbmNlbDogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogXCJ2aWV3X3VzZXJcIixcclxuICAgICAgICAgICAgbWVtYmVyOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbk1lbWJlckF2YXRhckNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBtZW1iZXIgPSB0aGlzLnByb3BzLm1lbWJlcjtcclxuICAgICAgICBjb25zdCBhdmF0YXJVcmwgPSBtZW1iZXIuZ2V0TXhjQXZhdGFyVXJsKCk7XHJcbiAgICAgICAgaWYgKCFhdmF0YXJVcmwpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgaHR0cFVybCA9IHRoaXMuY29udGV4dC5teGNVcmxUb0h0dHAoYXZhdGFyVXJsKTtcclxuICAgICAgICBjb25zdCBJbWFnZVZpZXcgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuSW1hZ2VWaWV3XCIpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgc3JjOiBodHRwVXJsLFxyXG4gICAgICAgICAgICBuYW1lOiBtZW1iZXIubmFtZSxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBNb2RhbC5jcmVhdGVEaWFsb2coSW1hZ2VWaWV3LCBwYXJhbXMsIFwibXhfRGlhbG9nX2xpZ2h0Ym94XCIpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21UaWxlQ2xpY2socm9vbUlkKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogcm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVuZGVyRGV2aWNlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9lbmFibGVEZXZpY2VzKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgZGV2aWNlcyA9IHRoaXMuc3RhdGUuZGV2aWNlcztcclxuICAgICAgICBjb25zdCBNZW1iZXJEZXZpY2VJbmZvID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuTWVtYmVyRGV2aWNlSW5mbycpO1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuU3Bpbm5lclwiKTtcclxuXHJcbiAgICAgICAgbGV0IGRldkNvbXBvbmVudHM7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZGV2aWNlc0xvYWRpbmcpIHtcclxuICAgICAgICAgICAgLy8gc3RpbGwgbG9hZGluZ1xyXG4gICAgICAgICAgICBkZXZDb21wb25lbnRzID0gPFNwaW5uZXIgLz47XHJcbiAgICAgICAgfSBlbHNlIGlmIChkZXZpY2VzID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGRldkNvbXBvbmVudHMgPSBfdChcIlVuYWJsZSB0byBsb2FkIHNlc3Npb24gbGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGRldmljZXMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIGRldkNvbXBvbmVudHMgPSBfdChcIk5vIHNlc3Npb25zIHdpdGggcmVnaXN0ZXJlZCBlbmNyeXB0aW9uIGtleXNcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGV2Q29tcG9uZW50cyA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRldmljZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGRldkNvbXBvbmVudHMucHVzaCg8TWVtYmVyRGV2aWNlSW5mbyBrZXk9e2l9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZD17dGhpcy5wcm9wcy5tZW1iZXIudXNlcklkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXZpY2U9e2RldmljZXNbaV19IC8+KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMz57IF90KFwiU2Vzc2lvbnNcIikgfTwvaDM+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01lbWJlckluZm9fZGV2aWNlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgZGV2Q29tcG9uZW50cyB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TaGFyZVVzZXJDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgU2hhcmVEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5TaGFyZURpYWxvZ1wiKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdzaGFyZSByb29tIG1lbWJlciBkaWFsb2cnLCAnJywgU2hhcmVEaWFsb2csIHtcclxuICAgICAgICAgICAgdGFyZ2V0OiB0aGlzLnByb3BzLm1lbWJlcixcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3JlbmRlclVzZXJPcHRpb25zOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSB0aGlzLmNvbnRleHQ7XHJcbiAgICAgICAgY29uc3QgbWVtYmVyID0gdGhpcy5wcm9wcy5tZW1iZXI7XHJcblxyXG4gICAgICAgIGxldCBpZ25vcmVCdXR0b24gPSBudWxsO1xyXG4gICAgICAgIGxldCBpbnNlcnRQaWxsQnV0dG9uID0gbnVsbDtcclxuICAgICAgICBsZXQgaW52aXRlVXNlckJ1dHRvbiA9IG51bGw7XHJcbiAgICAgICAgbGV0IHJlYWRSZWNlaXB0QnV0dG9uID0gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gT25seSBhbGxvdyB0aGUgdXNlciB0byBpZ25vcmUgdGhlIHVzZXIgaWYgaXRzIG5vdCBvdXJzZWx2ZXNcclxuICAgICAgICAvLyBzYW1lIGdvZXMgZm9yIGp1bXBpbmcgdG8gcmVhZCByZWNlaXB0XHJcbiAgICAgICAgaWYgKG1lbWJlci51c2VySWQgIT09IGNsaS5nZXRVc2VySWQoKSkge1xyXG4gICAgICAgICAgICBpZ25vcmVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uSWdub3JlVG9nZ2xlfSBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnN0YXRlLmlzSWdub3JpbmcgPyBfdChcIlVuaWdub3JlXCIpIDogX3QoXCJJZ25vcmVcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgaWYgKG1lbWJlci5yb29tSWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbShtZW1iZXIucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50SWQgPSByb29tLmdldEV2ZW50UmVhZFVwVG8obWVtYmVyLnVzZXJJZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3Qgb25SZWFkUmVjZWlwdEJ1dHRvbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBldmVudF9pZDogZXZlbnRJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogbWVtYmVyLnJvb21JZCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3Qgb25JbnNlcnRQaWxsQnV0dG9uID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnaW5zZXJ0X21lbnRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyX2lkOiBtZW1iZXIudXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICByZWFkUmVjZWlwdEJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtvblJlYWRSZWNlaXB0QnV0dG9ufSBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ0p1bXAgdG8gcmVhZCByZWNlaXB0JykgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICAgICAgaW5zZXJ0UGlsbEJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBvbkNsaWNrPXtvbkluc2VydFBpbGxCdXR0b259IGNsYXNzTmFtZT17XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCJ9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdNZW50aW9uJykgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmNhbi5pbnZpdGUgJiYgKCFtZW1iZXIgfHwgIW1lbWJlci5tZW1iZXJzaGlwIHx8IG1lbWJlci5tZW1iZXJzaGlwID09PSAnbGVhdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbUlkID0gbWVtYmVyICYmIG1lbWJlci5yb29tSWQgPyBtZW1iZXIucm9vbUlkIDogUm9vbVZpZXdTdG9yZS5nZXRSb29tSWQoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9uSW52aXRlVXNlckJ1dHRvbiA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBXZSB1c2UgYSBNdWx0aUludml0ZXIgdG8gcmUtdXNlIHRoZSBpbnZpdGUgbG9naWMsIGV2ZW4gdGhvdWdoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdlJ3JlIG9ubHkgaW52aXRpbmcgb25lIHVzZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGludml0ZXIgPSBuZXcgTXVsdGlJbnZpdGVyKHJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IGludml0ZXIuaW52aXRlKFttZW1iZXIudXNlcklkXSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW52aXRlci5nZXRDb21wbGV0aW9uU3RhdGUobWVtYmVyLnVzZXJJZCkgIT09IFwiaW52aXRlZFwiKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihpbnZpdGVyLmdldEVycm9yVGV4dChtZW1iZXIudXNlcklkKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuRXJyb3JEaWFsb2cnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGludml0ZScsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gaW52aXRlJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdChcIk9wZXJhdGlvbiBmYWlsZWRcIikpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIGludml0ZVVzZXJCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gb25DbGljaz17b25JbnZpdGVVc2VyQnV0dG9ufSBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ0ludml0ZScpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzaGFyZVVzZXJCdXR0b24gPSAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMub25TaGFyZVVzZXJDbGlja30gY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19maWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgeyBfdCgnU2hhcmUgTGluayB0byBVc2VyJykgfVxyXG4gICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxoMz57IF90KFwiVXNlciBPcHRpb25zXCIpIH08L2gzPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHJlYWRSZWNlaXB0QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHNoYXJlVXNlckJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBpbnNlcnRQaWxsQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IGlnbm9yZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBpbnZpdGVVc2VyQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCBzdGFydENoYXQ7XHJcbiAgICAgICAgbGV0IGtpY2tCdXR0b247XHJcbiAgICAgICAgbGV0IGJhbkJ1dHRvbjtcclxuICAgICAgICBsZXQgbXV0ZUJ1dHRvbjtcclxuICAgICAgICBsZXQgZ2l2ZU1vZEJ1dHRvbjtcclxuICAgICAgICBsZXQgcmVkYWN0QnV0dG9uO1xyXG4gICAgICAgIGxldCBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbjtcclxuICAgICAgICBsZXQgc3Bpbm5lcjtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMubWVtYmVyLnVzZXJJZCAhPT0gdGhpcy5jb250ZXh0LmNyZWRlbnRpYWxzLnVzZXJJZCkge1xyXG4gICAgICAgICAgICAvLyBUT0RPOiBJbW11dGFibGUgRE1zIHJlcGxhY2VzIGEgbG90IG9mIHRoaXNcclxuICAgICAgICAgICAgY29uc3QgZG1Sb29tTWFwID0gbmV3IERNUm9vbU1hcCh0aGlzLmNvbnRleHQpO1xyXG4gICAgICAgICAgICAvLyBkbVJvb21zIHdpbGwgbm90IGluY2x1ZGUgZG1Sb29tcyB0aGF0IHdlIGhhdmUgYmVlbiBpbnZpdGVkIGludG8gYnV0IGRpZCBub3Qgam9pbi5cclxuICAgICAgICAgICAgLy8gQmVjYXVzZSBETVJvb21NYXAgcnVucyBvZmYgYWNjb3VudF9kYXRhW20uZGlyZWN0XSB3aGljaCBpcyBvbmx5IHNldCBvbiBqb2luIG9mIGRtIHJvb20uXHJcbiAgICAgICAgICAgIC8vIFhYWDogd2UgcG90ZW50aWFsbHkgd2FudCBETXMgd2UgaGF2ZSBiZWVuIGludml0ZWQgdG8sIHRvIGFsc28gc2hvdyB1cCBoZXJlIDpMXHJcbiAgICAgICAgICAgIC8vIGVzcGVjaWFsbHkgYXMgbG9naWMgYmVsb3cgY29uY2VybnMgc3BlY2lhbGx5IGlmIHdlIGhhdmVuJ3Qgam9pbmVkIGJ1dCBoYXZlIGJlZW4gaW52aXRlZFxyXG4gICAgICAgICAgICBjb25zdCBkbVJvb21zID0gZG1Sb29tTWFwLmdldERNUm9vbXNGb3JVc2VySWQodGhpcy5wcm9wcy5tZW1iZXIudXNlcklkKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IFJvb21UaWxlID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLlJvb21UaWxlXCIpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdGlsZXMgPSBbXTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCByb29tSWQgb2YgZG1Sb29tcykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuY29udGV4dC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG15TWVtYmVyc2hpcCA9IHJvb20uZ2V0TXlNZW1iZXJzaGlwKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbm90IGEgRE0gcm9vbSBpZiB3ZSBoYXZlIGFyZSBub3Qgam9pbmVkXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG15TWVtYmVyc2hpcCAhPT0gJ2pvaW4nKSBjb250aW51ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGhlbSA9IHRoaXMucHJvcHMubWVtYmVyO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG5vdCBhIERNIHJvb20gaWYgdGhleSBhcmUgbm90IGpvaW5lZFxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdGhlbS5tZW1iZXJzaGlwIHx8IHRoZW0ubWVtYmVyc2hpcCAhPT0gJ2pvaW4nKSBjb250aW51ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaGlnaGxpZ2h0ID0gcm9vbS5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCgnaGlnaGxpZ2h0JykgPiAwO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aWxlcy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Um9vbVRpbGUga2V5PXtyb29tLnJvb21JZH0gcm9vbT17cm9vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zcGFyZW50PXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sbGFwc2VkPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVucmVhZD17VW5yZWFkLmRvZXNSb29tSGF2ZVVucmVhZE1lc3NhZ2VzKHJvb20pfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0PXtoaWdobGlnaHR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0ludml0ZT17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uUm9vbVRpbGVDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgLz4sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgbGFiZWxDbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICAgICBteF9NZW1iZXJJbmZvX2NyZWF0ZVJvb21fbGFiZWw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBteF9Sb29tVGlsZV9uYW1lOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgbGV0IHN0YXJ0TmV3Q2hhdCA9IDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2NyZWF0ZVJvb21cIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbk5ld0RNQ2xpY2t9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVfYXZhdGFyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2NyZWF0ZS1iaWcuc3ZnXCIpfSB3aWR0aD1cIjI2XCIgaGVpZ2h0PVwiMjZcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17bGFiZWxDbGFzc2VzfT48aT57IF90KFwiU3RhcnQgYSBjaGF0XCIpIH08L2k+PC9kaXY+XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcblxyXG4gICAgICAgICAgICBpZiAodGlsZXMubGVuZ3RoID4gMCkgc3RhcnROZXdDaGF0ID0gbnVsbDsgLy8gRG9uJ3Qgb2ZmZXIgYSBidXR0b24gZm9yIGEgbmV3IGNoYXQgaWYgd2UgaGF2ZSBvbmUuXHJcblxyXG4gICAgICAgICAgICBzdGFydENoYXQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGgzPnsgX3QoXCJEaXJlY3QgY2hhdHNcIikgfTwvaDM+XHJcbiAgICAgICAgICAgICAgICB7IHRpbGVzIH1cclxuICAgICAgICAgICAgICAgIHsgc3RhcnROZXdDaGF0IH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUudXBkYXRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgTG9hZGVyID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLlNwaW5uZXJcIik7XHJcbiAgICAgICAgICAgIHNwaW5uZXIgPSA8TG9hZGVyIGltZ0NsYXNzTmFtZT1cIm14X0NvbnRleHR1YWxNZW51X3NwaW5uZXJcIiAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNhbi5raWNrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWJlcnNoaXAgPSB0aGlzLnByb3BzLm1lbWJlci5tZW1iZXJzaGlwO1xyXG4gICAgICAgICAgICBjb25zdCBraWNrTGFiZWwgPSBtZW1iZXJzaGlwID09PSBcImludml0ZVwiID8gX3QoXCJEaXNpbnZpdGVcIikgOiBfdChcIktpY2tcIik7XHJcbiAgICAgICAgICAgIGtpY2tCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbktpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsga2lja0xhYmVsIH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNhbi5yZWRhY3RNZXNzYWdlcykge1xyXG4gICAgICAgICAgICByZWRhY3RCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCIgb25DbGljaz17dGhpcy5vblJlZGFjdEFsbE1lc3NhZ2VzfT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KFwiUmVtb3ZlIHJlY2VudCBtZXNzYWdlc1wiKSB9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jYW4uYmFuKSB7XHJcbiAgICAgICAgICAgIGxldCBsYWJlbCA9IF90KFwiQmFuXCIpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5tZW1iZXIubWVtYmVyc2hpcCA9PT0gJ2JhbicpIHtcclxuICAgICAgICAgICAgICAgIGxhYmVsID0gX3QoXCJVbmJhblwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBiYW5CdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkJhbk9yVW5iYW59PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgbGFiZWwgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jYW4ubXV0ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBtdXRlTGFiZWwgPSB0aGlzLnN0YXRlLm11dGVkID8gX3QoXCJVbm11dGVcIikgOiBfdChcIk11dGVcIik7XHJcbiAgICAgICAgICAgIG11dGVCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX2ZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbk11dGVUb2dnbGV9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgbXV0ZUxhYmVsIH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY2FuLnRvZ2dsZU1vZCkge1xyXG4gICAgICAgICAgICBjb25zdCBnaXZlT3BMYWJlbCA9IHRoaXMuc3RhdGUuaXNUYXJnZXRNb2QgPyBfdChcIlJldm9rZSBNb2RlcmF0b3JcIikgOiBfdChcIk1ha2UgTW9kZXJhdG9yXCIpO1xyXG4gICAgICAgICAgICBnaXZlTW9kQnV0dG9uID0gPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25Nb2RUb2dnbGV9PlxyXG4gICAgICAgICAgICAgICAgeyBnaXZlT3BMYWJlbCB9XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBXZSBkb24ndCBuZWVkIGEgcGVyZmVjdCBjaGVjayBoZXJlLCBqdXN0IHNvbWV0aGluZyB0byBwYXNzIGFzIFwicHJvYmFibHkgbm90IG91ciBob21lc2VydmVyXCIuIElmXHJcbiAgICAgICAgLy8gc29tZW9uZSBkb2VzIGZpZ3VyZSBvdXQgaG93IHRvIGJ5cGFzcyB0aGlzIGNoZWNrIHRoZSB3b3JzdCB0aGF0IGhhcHBlbnMgaXMgYW4gZXJyb3IuXHJcbiAgICAgICAgY29uc3Qgc2FtZUhvbWVzZXJ2ZXIgPSB0aGlzLnByb3BzLm1lbWJlci51c2VySWQuZW5kc1dpdGgoYDoke01hdHJpeENsaWVudFBlZy5nZXRIb21lc2VydmVyTmFtZSgpfWApO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNhbi5zeW5hcHNlRGVhY3RpdmF0ZSAmJiBzYW1lSG9tZXNlcnZlcikge1xyXG4gICAgICAgICAgICBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMub25TeW5hcHNlRGVhY3RpdmF0ZX0gY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19maWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIkRlYWN0aXZhdGUgdXNlclwiKX1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBhZG1pblRvb2xzO1xyXG4gICAgICAgIGlmIChraWNrQnV0dG9uIHx8IGJhbkJ1dHRvbiB8fCBtdXRlQnV0dG9uIHx8IGdpdmVNb2RCdXR0b24gfHwgc3luYXBzZURlYWN0aXZhdGVCdXR0b24gfHwgcmVkYWN0QnV0dG9uKSB7XHJcbiAgICAgICAgICAgIGFkbWluVG9vbHMgPVxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICA8aDM+eyBfdChcIkFkbWluIFRvb2xzXCIpIH08L2gzPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01lbWJlckluZm9fYnV0dG9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IG11dGVCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGtpY2tCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGJhbkJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgcmVkYWN0QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBnaXZlTW9kQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBzeW5hcHNlRGVhY3RpdmF0ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBtZW1iZXJOYW1lID0gdGhpcy5wcm9wcy5tZW1iZXIubmFtZTtcclxuXHJcbiAgICAgICAgbGV0IHByZXNlbmNlU3RhdGU7XHJcbiAgICAgICAgbGV0IHByZXNlbmNlTGFzdEFjdGl2ZUFnbztcclxuICAgICAgICBsZXQgcHJlc2VuY2VDdXJyZW50bHlBY3RpdmU7XHJcbiAgICAgICAgbGV0IHN0YXR1c01lc3NhZ2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm1lbWJlci51c2VyKSB7XHJcbiAgICAgICAgICAgIHByZXNlbmNlU3RhdGUgPSB0aGlzLnByb3BzLm1lbWJlci51c2VyLnByZXNlbmNlO1xyXG4gICAgICAgICAgICBwcmVzZW5jZUxhc3RBY3RpdmVBZ28gPSB0aGlzLnByb3BzLm1lbWJlci51c2VyLmxhc3RBY3RpdmVBZ287XHJcbiAgICAgICAgICAgIHByZXNlbmNlQ3VycmVudGx5QWN0aXZlID0gdGhpcy5wcm9wcy5tZW1iZXIudXNlci5jdXJyZW50bHlBY3RpdmU7XHJcblxyXG4gICAgICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jdXN0b21fc3RhdHVzXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBzdGF0dXNNZXNzYWdlID0gdGhpcy5wcm9wcy5tZW1iZXIudXNlci5fdW5zdGFibGVfc3RhdHVzTWVzc2FnZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuY29udGV4dC5nZXRSb29tKHRoaXMucHJvcHMubWVtYmVyLnJvb21JZCk7XHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbEV2ZW50ID0gcm9vbSA/IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLnBvd2VyX2xldmVsc1wiLCBcIlwiKSA6IG51bGw7XHJcbiAgICAgICAgY29uc3QgcG93ZXJMZXZlbFVzZXJzRGVmYXVsdCA9IHBvd2VyTGV2ZWxFdmVudCA/IHBvd2VyTGV2ZWxFdmVudC5nZXRDb250ZW50KCkudXNlcnNfZGVmYXVsdCA6IDA7XHJcblxyXG4gICAgICAgIGNvbnN0IGVuYWJsZVByZXNlbmNlQnlIc1VybCA9IFNka0NvbmZpZy5nZXQoKVtcImVuYWJsZV9wcmVzZW5jZV9ieV9oc191cmxcIl07XHJcbiAgICAgICAgY29uc3QgaHNVcmwgPSB0aGlzLmNvbnRleHQuYmFzZVVybDtcclxuICAgICAgICBsZXQgc2hvd1ByZXNlbmNlID0gdHJ1ZTtcclxuICAgICAgICBpZiAoZW5hYmxlUHJlc2VuY2VCeUhzVXJsICYmIGVuYWJsZVByZXNlbmNlQnlIc1VybFtoc1VybF0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBzaG93UHJlc2VuY2UgPSBlbmFibGVQcmVzZW5jZUJ5SHNVcmxbaHNVcmxdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHByZXNlbmNlTGFiZWwgPSBudWxsO1xyXG4gICAgICAgIGlmIChzaG93UHJlc2VuY2UpIHtcclxuICAgICAgICAgICAgY29uc3QgUHJlc2VuY2VMYWJlbCA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLlByZXNlbmNlTGFiZWwnKTtcclxuICAgICAgICAgICAgcHJlc2VuY2VMYWJlbCA9IDxQcmVzZW5jZUxhYmVsIGFjdGl2ZUFnbz17cHJlc2VuY2VMYXN0QWN0aXZlQWdvfVxyXG4gICAgICAgICAgICAgICAgY3VycmVudGx5QWN0aXZlPXtwcmVzZW5jZUN1cnJlbnRseUFjdGl2ZX1cclxuICAgICAgICAgICAgICAgIHByZXNlbmNlU3RhdGU9e3ByZXNlbmNlU3RhdGV9IC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHN0YXR1c0xhYmVsID0gbnVsbDtcclxuICAgICAgICBpZiAoc3RhdHVzTWVzc2FnZSkge1xyXG4gICAgICAgICAgICBzdGF0dXNMYWJlbCA9IDxzcGFuIGNsYXNzTmFtZT1cIm14X01lbWJlckluZm9fc3RhdHVzTWVzc2FnZVwiPnsgc3RhdHVzTWVzc2FnZSB9PC9zcGFuPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCByb29tTWVtYmVyRGV0YWlscyA9IG51bGw7XHJcbiAgICAgICAgbGV0IGUyZUljb25FbGVtZW50O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkKSB7IC8vIGlzIGluIHJvb21cclxuICAgICAgICAgICAgY29uc3QgUG93ZXJTZWxlY3RvciA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlBvd2VyU2VsZWN0b3InKTtcclxuICAgICAgICAgICAgcm9vbU1lbWJlckRldGFpbHMgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX3Byb2ZpbGVGaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxQb3dlclNlbGVjdG9yXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtwYXJzZUludCh0aGlzLnByb3BzLm1lbWJlci5wb3dlckxldmVsKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU9e3RoaXMuc3RhdGUuY2FuLm1vZGlmeUxldmVsTWF4fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IXRoaXMuc3RhdGUuY2FuLm1vZGlmeUxldmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2Vyc0RlZmF1bHQ9e3Bvd2VyTGV2ZWxVc2Vyc0RlZmF1bHR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uUG93ZXJDaGFuZ2V9IC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19wcm9maWxlRmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7cHJlc2VuY2VMYWJlbH1cclxuICAgICAgICAgICAgICAgICAgICB7c3RhdHVzTGFiZWx9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgaXNFbmNyeXB0ZWQgPSB0aGlzLmNvbnRleHQuaXNSb29tRW5jcnlwdGVkKHRoaXMucHJvcHMubWVtYmVyLnJvb21JZCk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmUyZVN0YXR1cyAmJiBpc0VuY3J5cHRlZCkge1xyXG4gICAgICAgICAgICAgICAgZTJlSWNvbkVsZW1lbnQgPSAoPEUyRUljb24gc3RhdHVzPXt0aGlzLnN0YXRlLmUyZVN0YXR1c30gaXNVc2VyPXt0cnVlfSAvPik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHttZW1iZXJ9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBjb25zdCBhdmF0YXJVcmwgPSBtZW1iZXIuYXZhdGFyVXJsIHx8IChtZW1iZXIuZ2V0TXhjQXZhdGFyVXJsICYmIG1lbWJlci5nZXRNeGNBdmF0YXJVcmwoKSk7XHJcbiAgICAgICAgbGV0IGF2YXRhckVsZW1lbnQ7XHJcbiAgICAgICAgaWYgKGF2YXRhclVybCkge1xyXG4gICAgICAgICAgICBjb25zdCBodHRwVXJsID0gdGhpcy5jb250ZXh0Lm14Y1VybFRvSHR0cChhdmF0YXJVcmwsIDgwMCwgODAwKTtcclxuICAgICAgICAgICAgYXZhdGFyRWxlbWVudCA9IDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19hdmF0YXJcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtodHRwVXJsfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgYmFja0J1dHRvbjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5tZW1iZXIucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIGJhY2tCdXR0b24gPSAoPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19jYW5jZWxcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkNhbmNlbH1cclxuICAgICAgICAgICAgICAgIHRpdGxlPXtfdCgnQ2xvc2UnKX1cclxuICAgICAgICAgICAgLz4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvXCIgcm9sZT1cInRhYnBhbmVsXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01lbWJlckluZm9fbmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgYmFja0J1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBlMmVJY29uRWxlbWVudCB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGgyPnsgbWVtYmVyTmFtZSB9PC9oMj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgeyBhdmF0YXJFbGVtZW50IH1cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19jb250YWluZXJcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX3Byb2ZpbGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX3Byb2ZpbGVGaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnByb3BzLm1lbWJlci51c2VySWQgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyByb29tTWVtYmVyRGV0YWlscyB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxBdXRvSGlkZVNjcm9sbGJhciBjbGFzc05hbWU9XCJteF9NZW1iZXJJbmZvX3Njcm9sbENvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVySW5mb19jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJVc2VyT3B0aW9ucygpIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgYWRtaW5Ub29scyB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXJ0Q2hhdCB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHRoaXMuX3JlbmRlckRldmljZXMoKSB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHNwaW5uZXIgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9BdXRvSGlkZVNjcm9sbGJhcj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=