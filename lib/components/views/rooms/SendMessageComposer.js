"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMessageContent = createMessageContent;
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _model = _interopRequireDefault(require("../../../editor/model"));

var _serialize = require("../../../editor/serialize");

var _parts = require("../../../editor/parts");

var _BasicMessageComposer = _interopRequireDefault(require("./BasicMessageComposer"));

var _ReplyPreview = _interopRequireDefault(require("./ReplyPreview"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _ReplyThread = _interopRequireDefault(require("../elements/ReplyThread"));

var _deserialize = require("../../../editor/deserialize");

var _EventUtils = require("../../../utils/EventUtils");

var _SendHistoryManager = _interopRequireDefault(require("../../../SendHistoryManager"));

var _SlashCommands = require("../../../SlashCommands");

var sdk = _interopRequireWildcard(require("../../../index"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _languageHandler = require("../../../languageHandler");

var _ContentMessages = _interopRequireDefault(require("../../../ContentMessages"));

var _Keyboard = require("../../../Keyboard");

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _ratelimitedfunc = _interopRequireDefault(require("../../../ratelimitedfunc"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function addReplyToMessageContent(content, repliedToEvent, permalinkCreator) {
  const replyContent = _ReplyThread.default.makeReplyMixIn(repliedToEvent);

  Object.assign(content, replyContent); // Part of Replies fallback support - prepend the text we're sending
  // with the text we're replying to

  const nestedReply = _ReplyThread.default.getNestedReplyText(repliedToEvent, permalinkCreator);

  if (nestedReply) {
    if (content.formatted_body) {
      content.formatted_body = nestedReply.html + content.formatted_body;
    }

    content.body = nestedReply.body + content.body;
  }
} // exported for tests


function createMessageContent(model, permalinkCreator) {
  const isEmote = (0, _serialize.containsEmote)(model);

  if (isEmote) {
    model = (0, _serialize.stripEmoteCommand)(model);
  }

  if ((0, _serialize.startsWith)(model, "//")) {
    model = (0, _serialize.stripPrefix)(model, "/");
  }

  model = (0, _serialize.unescapeMessage)(model);

  const repliedToEvent = _RoomViewStore.default.getQuotingEvent();

  const body = (0, _serialize.textSerialize)(model);
  const content = {
    msgtype: isEmote ? "m.emote" : "m.text",
    body: body
  };
  const formattedBody = (0, _serialize.htmlSerializeIfNeeded)(model, {
    forceHTML: !!repliedToEvent
  });

  if (formattedBody) {
    content.format = "org.matrix.custom.html";
    content.formatted_body = formattedBody;
  }

  if (repliedToEvent) {
    addReplyToMessageContent(content, repliedToEvent, permalinkCreator);
  }

  return content;
}

class SendMessageComposer extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_setEditorRef", ref => {
      this._editorRef = ref;
    });
    (0, _defineProperty2.default)(this, "_onKeyDown", event => {
      // ignore any keypress while doing IME compositions
      if (this._editorRef.isComposing(event)) {
        return;
      }

      const hasModifier = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey;

      if (event.key === _Keyboard.Key.ENTER && !hasModifier) {
        this._sendMessage();

        event.preventDefault();
      } else if (event.key === _Keyboard.Key.ARROW_UP) {
        this.onVerticalArrow(event, true);
      } else if (event.key === _Keyboard.Key.ARROW_DOWN) {
        this.onVerticalArrow(event, false);
      } else if (this._prepareToEncrypt) {
        this._prepareToEncrypt();
      } else if (event.key === _Keyboard.Key.ESCAPE) {
        _dispatcher.default.dispatch({
          action: 'reply_to_event',
          event: null
        });
      }
    });
    (0, _defineProperty2.default)(this, "_saveStoredEditorState", () => {
      if (this.model.isEmpty) {
        this._clearStoredEditorState();
      } else {
        localStorage.setItem(this._editorStateKey, JSON.stringify(this.model.serializeParts()));
      }
    });
    (0, _defineProperty2.default)(this, "onAction", payload => {
      switch (payload.action) {
        case 'reply_to_event':
        case 'focus_composer':
          this._editorRef && this._editorRef.focus();
          break;

        case 'insert_mention':
          this._insertMention(payload.user_id);

          break;

        case 'quote':
          this._insertQuotedMessage(payload.event);

          break;
      }
    });
    (0, _defineProperty2.default)(this, "_onPaste", event => {
      const {
        clipboardData
      } = event;

      if (clipboardData.files.length) {
        // This actually not so much for 'files' as such (at time of writing
        // neither chrome nor firefox let you paste a plain file copied
        // from Finder) but more images copied from a different website
        // / word processor etc.
        _ContentMessages.default.sharedInstance().sendContentListToRoom(Array.from(clipboardData.files), this.props.room.roomId, this.context);
      }
    });
    this.model = null;
    this._editorRef = null;
    this.currentlyComposedEditorState = null;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli.isCryptoEnabled() && cli.isRoomEncrypted(this.props.room.roomId)) {
      this._prepareToEncrypt = new _ratelimitedfunc.default(() => {
        cli.prepareToEncrypt(this.props.room);
      }, 60000);
    }
  }

  onVerticalArrow(e, up) {
    // arrows from an initial-caret composer navigates recent messages to edit
    // ctrl-alt-arrows navigate send history
    if (e.shiftKey || e.metaKey) return;
    const shouldSelectHistory = e.altKey && e.ctrlKey;
    const shouldEditLastMessage = !e.altKey && !e.ctrlKey && up && !_RoomViewStore.default.getQuotingEvent();

    if (shouldSelectHistory) {
      // Try select composer history
      const selected = this.selectSendHistory(up);

      if (selected) {
        // We're selecting history, so prevent the key event from doing anything else
        e.preventDefault();
      }
    } else if (shouldEditLastMessage) {
      // selection must be collapsed and caret at start
      if (this._editorRef.isSelectionCollapsed() && this._editorRef.isCaretAtStart()) {
        const editEvent = (0, _EventUtils.findEditableEvent)(this.props.room, false);

        if (editEvent) {
          // We're selecting history, so prevent the key event from doing anything else
          e.preventDefault();

          _dispatcher.default.dispatch({
            action: 'edit_event',
            event: editEvent
          });
        }
      }
    }
  } // we keep sent messages/commands in a separate history (separate from undo history)
  // so you can alt+up/down in them


  selectSendHistory(up) {
    const delta = up ? -1 : 1; // True if we are not currently selecting history, but composing a message

    if (this.sendHistoryManager.currentIndex === this.sendHistoryManager.history.length) {
      // We can't go any further - there isn't any more history, so nop.
      if (!up) {
        return;
      }

      this.currentlyComposedEditorState = this.model.serializeParts();
    } else if (this.sendHistoryManager.currentIndex + delta === this.sendHistoryManager.history.length) {
      // True when we return to the message being composed currently
      this.model.reset(this.currentlyComposedEditorState);
      this.sendHistoryManager.currentIndex = this.sendHistoryManager.history.length;
      return;
    }

    const serializedParts = this.sendHistoryManager.getItem(delta);

    if (serializedParts) {
      this.model.reset(serializedParts);

      this._editorRef.focus();
    }
  }

  _isSlashCommand() {
    const parts = this.model.parts;
    const firstPart = parts[0];

    if (firstPart) {
      if (firstPart.type === "command" && firstPart.text.startsWith("/") && !firstPart.text.startsWith("//")) {
        return true;
      } // be extra resilient when somehow the AutocompleteWrapperModel or
      // CommandPartCreator fails to insert a command part, so we don't send
      // a command as a message


      if (firstPart.text.startsWith("/") && !firstPart.text.startsWith("//") && (firstPart.type === "plain" || firstPart.type === "pill-candidate")) {
        return true;
      }
    }

    return false;
  }

  _getSlashCommand() {
    const commandText = this.model.parts.reduce((text, part) => {
      // use mxid to textify user pills in a command
      if (part.type === "user-pill") {
        return text + part.resourceId;
      }

      return text + part.text;
    }, "");
    return [(0, _SlashCommands.getCommand)(this.props.room.roomId, commandText), commandText];
  }

  async _runSlashCommand(fn) {
    const cmd = fn();
    let error = cmd.error;

    if (cmd.promise) {
      try {
        await cmd.promise;
      } catch (err) {
        error = err;
      }
    }

    if (error) {
      console.error("Command failure: %s", error);
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog"); // assume the error is a server error when the command is async

      const isServerError = !!cmd.promise;
      const title = isServerError ? (0, _languageHandler._td)("Server error") : (0, _languageHandler._td)("Command error");
      let errText;

      if (typeof error === 'string') {
        errText = error;
      } else if (error.message) {
        errText = error.message;
      } else {
        errText = (0, _languageHandler._t)("Server unavailable, overloaded, or something else went wrong.");
      }

      _Modal.default.createTrackedDialog(title, '', ErrorDialog, {
        title: (0, _languageHandler._t)(title),
        description: errText
      });
    } else {
      console.log("Command success.");
    }
  }

  async _sendMessage() {
    if (this.model.isEmpty) {
      return;
    }

    let shouldSend = true;

    if (!(0, _serialize.containsEmote)(this.model) && this._isSlashCommand()) {
      const [cmd, commandText] = this._getSlashCommand();

      if (cmd) {
        shouldSend = false;

        this._runSlashCommand(cmd);
      } else {
        // ask the user if their unknown command should be sent as a message
        const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

        const {
          finished
        } = _Modal.default.createTrackedDialog("Unknown command", "", QuestionDialog, {
          title: (0, _languageHandler._t)("Unknown Command"),
          description: _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Unrecognised command: %(commandText)s", {
            commandText
          })), _react.default.createElement("p", null, (0, _languageHandler._t)("You can use <code>/help</code> to list available commands. " + "Did you mean to send this as a message?", {}, {
            code: t => _react.default.createElement("code", null, t)
          })), _react.default.createElement("p", null, (0, _languageHandler._t)("Hint: Begin your message with <code>//</code> to start it with a slash.", {}, {
            code: t => _react.default.createElement("code", null, t)
          }))),
          button: (0, _languageHandler._t)('Send as message')
        });

        const [sendAnyway] = await finished; // if !sendAnyway bail to let the user edit the composer and try again

        if (!sendAnyway) return;
      }
    }

    if (shouldSend) {
      const isReply = !!_RoomViewStore.default.getQuotingEvent();
      const {
        roomId
      } = this.props.room;
      const content = createMessageContent(this.model, this.props.permalinkCreator);
      this.context.sendMessage(roomId, content);

      if (isReply) {
        // Clear reply_to_event as we put the message into the queue
        // if the send fails, retry will handle resending.
        _dispatcher.default.dispatch({
          action: 'reply_to_event',
          event: null
        });
      }
    }

    this.sendHistoryManager.save(this.model); // clear composer

    this.model.reset([]);

    this._editorRef.clearUndoHistory();

    this._editorRef.focus();

    this._clearStoredEditorState();
  }

  componentDidMount() {
    this._editorRef.getEditableRootNode().addEventListener("paste", this._onPaste, true);
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this.dispatcherRef);

    this._editorRef.getEditableRootNode().removeEventListener("paste", this._onPaste, true);
  } // TODO: [REACT-WARNING] Move this to constructor


  UNSAFE_componentWillMount() {
    // eslint-disable-line camelcase
    const partCreator = new _parts.CommandPartCreator(this.props.room, this.context);
    const parts = this._restoreStoredEditorState(partCreator) || [];
    this.model = new _model.default(parts, partCreator);
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    this.sendHistoryManager = new _SendHistoryManager.default(this.props.room.roomId, 'mx_cider_composer_history_');
  }

  get _editorStateKey() {
    return "cider_editor_state_".concat(this.props.room.roomId);
  }

  _clearStoredEditorState() {
    localStorage.removeItem(this._editorStateKey);
  }

  _restoreStoredEditorState(partCreator) {
    const json = localStorage.getItem(this._editorStateKey);

    if (json) {
      const serializedParts = JSON.parse(json);
      const parts = serializedParts.map(p => partCreator.deserializePart(p));
      return parts;
    }
  }

  _insertMention(userId) {
    const {
      model
    } = this;
    const {
      partCreator
    } = model;
    const member = this.props.room.getMember(userId);
    const displayName = member ? member.rawDisplayName : userId;

    const caret = this._editorRef.getCaret();

    const position = model.positionForOffset(caret.offset, caret.atNodeEnd); // index is -1 if there are no parts but we only care for if this would be the part in position 0

    const insertIndex = position.index > 0 ? position.index : 0;
    const parts = partCreator.createMentionParts(insertIndex, displayName, userId);
    model.transform(() => {
      const addedLen = model.insert(parts, position);
      return model.positionForOffset(caret.offset + addedLen, true);
    }); // refocus on composer, as we just clicked "Mention"

    this._editorRef && this._editorRef.focus();
  }

  _insertQuotedMessage(event) {
    const {
      model
    } = this;
    const {
      partCreator
    } = model;
    const quoteParts = (0, _deserialize.parseEvent)(event, partCreator, {
      isQuotedMessage: true
    }); // add two newlines

    quoteParts.push(partCreator.newline());
    quoteParts.push(partCreator.newline());
    model.transform(() => {
      const addedLen = model.insert(quoteParts, model.positionForOffset(0));
      return model.positionForOffset(addedLen, true);
    }); // refocus on composer, as we just clicked "Quote"

    this._editorRef && this._editorRef.focus();
  }

  render() {
    return _react.default.createElement("div", {
      className: "mx_SendMessageComposer",
      onClick: this.focusComposer,
      onKeyDown: this._onKeyDown
    }, _react.default.createElement("div", {
      className: "mx_SendMessageComposer_overlayWrapper"
    }, _react.default.createElement(_ReplyPreview.default, {
      permalinkCreator: this.props.permalinkCreator
    })), _react.default.createElement(_BasicMessageComposer.default, {
      ref: this._setEditorRef,
      model: this.model,
      room: this.props.room,
      label: this.props.placeholder,
      placeholder: this.props.placeholder,
      onChange: this._saveStoredEditorState
    }));
  }

}

exports.default = SendMessageComposer;
(0, _defineProperty2.default)(SendMessageComposer, "propTypes", {
  room: _propTypes.default.object.isRequired,
  placeholder: _propTypes.default.string,
  permalinkCreator: _propTypes.default.object.isRequired
});
(0, _defineProperty2.default)(SendMessageComposer, "contextType", _MatrixClientContext.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1NlbmRNZXNzYWdlQ29tcG9zZXIuanMiXSwibmFtZXMiOlsiYWRkUmVwbHlUb01lc3NhZ2VDb250ZW50IiwiY29udGVudCIsInJlcGxpZWRUb0V2ZW50IiwicGVybWFsaW5rQ3JlYXRvciIsInJlcGx5Q29udGVudCIsIlJlcGx5VGhyZWFkIiwibWFrZVJlcGx5TWl4SW4iLCJPYmplY3QiLCJhc3NpZ24iLCJuZXN0ZWRSZXBseSIsImdldE5lc3RlZFJlcGx5VGV4dCIsImZvcm1hdHRlZF9ib2R5IiwiaHRtbCIsImJvZHkiLCJjcmVhdGVNZXNzYWdlQ29udGVudCIsIm1vZGVsIiwiaXNFbW90ZSIsIlJvb21WaWV3U3RvcmUiLCJnZXRRdW90aW5nRXZlbnQiLCJtc2d0eXBlIiwiZm9ybWF0dGVkQm9keSIsImZvcmNlSFRNTCIsImZvcm1hdCIsIlNlbmRNZXNzYWdlQ29tcG9zZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJyZWYiLCJfZWRpdG9yUmVmIiwiZXZlbnQiLCJpc0NvbXBvc2luZyIsImhhc01vZGlmaWVyIiwiYWx0S2V5IiwiY3RybEtleSIsIm1ldGFLZXkiLCJzaGlmdEtleSIsImtleSIsIktleSIsIkVOVEVSIiwiX3NlbmRNZXNzYWdlIiwicHJldmVudERlZmF1bHQiLCJBUlJPV19VUCIsIm9uVmVydGljYWxBcnJvdyIsIkFSUk9XX0RPV04iLCJfcHJlcGFyZVRvRW5jcnlwdCIsIkVTQ0FQRSIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwiaXNFbXB0eSIsIl9jbGVhclN0b3JlZEVkaXRvclN0YXRlIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsIl9lZGl0b3JTdGF0ZUtleSIsIkpTT04iLCJzdHJpbmdpZnkiLCJzZXJpYWxpemVQYXJ0cyIsInBheWxvYWQiLCJmb2N1cyIsIl9pbnNlcnRNZW50aW9uIiwidXNlcl9pZCIsIl9pbnNlcnRRdW90ZWRNZXNzYWdlIiwiY2xpcGJvYXJkRGF0YSIsImZpbGVzIiwibGVuZ3RoIiwiQ29udGVudE1lc3NhZ2VzIiwic2hhcmVkSW5zdGFuY2UiLCJzZW5kQ29udGVudExpc3RUb1Jvb20iLCJBcnJheSIsImZyb20iLCJyb29tIiwicm9vbUlkIiwiY29udGV4dCIsImN1cnJlbnRseUNvbXBvc2VkRWRpdG9yU3RhdGUiLCJjbGkiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJpc0NyeXB0b0VuYWJsZWQiLCJpc1Jvb21FbmNyeXB0ZWQiLCJSYXRlTGltaXRlZEZ1bmMiLCJwcmVwYXJlVG9FbmNyeXB0IiwiZSIsInVwIiwic2hvdWxkU2VsZWN0SGlzdG9yeSIsInNob3VsZEVkaXRMYXN0TWVzc2FnZSIsInNlbGVjdGVkIiwic2VsZWN0U2VuZEhpc3RvcnkiLCJpc1NlbGVjdGlvbkNvbGxhcHNlZCIsImlzQ2FyZXRBdFN0YXJ0IiwiZWRpdEV2ZW50IiwiZGVsdGEiLCJzZW5kSGlzdG9yeU1hbmFnZXIiLCJjdXJyZW50SW5kZXgiLCJoaXN0b3J5IiwicmVzZXQiLCJzZXJpYWxpemVkUGFydHMiLCJnZXRJdGVtIiwiX2lzU2xhc2hDb21tYW5kIiwicGFydHMiLCJmaXJzdFBhcnQiLCJ0eXBlIiwidGV4dCIsInN0YXJ0c1dpdGgiLCJfZ2V0U2xhc2hDb21tYW5kIiwiY29tbWFuZFRleHQiLCJyZWR1Y2UiLCJwYXJ0IiwicmVzb3VyY2VJZCIsIl9ydW5TbGFzaENvbW1hbmQiLCJmbiIsImNtZCIsImVycm9yIiwicHJvbWlzZSIsImVyciIsImNvbnNvbGUiLCJFcnJvckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsImlzU2VydmVyRXJyb3IiLCJ0aXRsZSIsImVyclRleHQiLCJtZXNzYWdlIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwiZGVzY3JpcHRpb24iLCJsb2ciLCJzaG91bGRTZW5kIiwiUXVlc3Rpb25EaWFsb2ciLCJmaW5pc2hlZCIsImNvZGUiLCJ0IiwiYnV0dG9uIiwic2VuZEFueXdheSIsImlzUmVwbHkiLCJzZW5kTWVzc2FnZSIsInNhdmUiLCJjbGVhclVuZG9IaXN0b3J5IiwiY29tcG9uZW50RGlkTW91bnQiLCJnZXRFZGl0YWJsZVJvb3ROb2RlIiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9vblBhc3RlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bnJlZ2lzdGVyIiwiZGlzcGF0Y2hlclJlZiIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwicGFydENyZWF0b3IiLCJDb21tYW5kUGFydENyZWF0b3IiLCJfcmVzdG9yZVN0b3JlZEVkaXRvclN0YXRlIiwiRWRpdG9yTW9kZWwiLCJyZWdpc3RlciIsIm9uQWN0aW9uIiwiU2VuZEhpc3RvcnlNYW5hZ2VyIiwicmVtb3ZlSXRlbSIsImpzb24iLCJwYXJzZSIsIm1hcCIsInAiLCJkZXNlcmlhbGl6ZVBhcnQiLCJ1c2VySWQiLCJtZW1iZXIiLCJnZXRNZW1iZXIiLCJkaXNwbGF5TmFtZSIsInJhd0Rpc3BsYXlOYW1lIiwiY2FyZXQiLCJnZXRDYXJldCIsInBvc2l0aW9uIiwicG9zaXRpb25Gb3JPZmZzZXQiLCJvZmZzZXQiLCJhdE5vZGVFbmQiLCJpbnNlcnRJbmRleCIsImluZGV4IiwiY3JlYXRlTWVudGlvblBhcnRzIiwidHJhbnNmb3JtIiwiYWRkZWRMZW4iLCJpbnNlcnQiLCJxdW90ZVBhcnRzIiwiaXNRdW90ZWRNZXNzYWdlIiwicHVzaCIsIm5ld2xpbmUiLCJyZW5kZXIiLCJmb2N1c0NvbXBvc2VyIiwiX29uS2V5RG93biIsIl9zZXRFZGl0b3JSZWYiLCJwbGFjZWhvbGRlciIsIl9zYXZlU3RvcmVkRWRpdG9yU3RhdGUiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiTWF0cml4Q2xpZW50Q29udGV4dCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBU0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBN0NBOzs7Ozs7Ozs7Ozs7Ozs7O0FBK0NBLFNBQVNBLHdCQUFULENBQWtDQyxPQUFsQyxFQUEyQ0MsY0FBM0MsRUFBMkRDLGdCQUEzRCxFQUE2RTtBQUN6RSxRQUFNQyxZQUFZLEdBQUdDLHFCQUFZQyxjQUFaLENBQTJCSixjQUEzQixDQUFyQjs7QUFDQUssRUFBQUEsTUFBTSxDQUFDQyxNQUFQLENBQWNQLE9BQWQsRUFBdUJHLFlBQXZCLEVBRnlFLENBSXpFO0FBQ0E7O0FBQ0EsUUFBTUssV0FBVyxHQUFHSixxQkFBWUssa0JBQVosQ0FBK0JSLGNBQS9CLEVBQStDQyxnQkFBL0MsQ0FBcEI7O0FBQ0EsTUFBSU0sV0FBSixFQUFpQjtBQUNiLFFBQUlSLE9BQU8sQ0FBQ1UsY0FBWixFQUE0QjtBQUN4QlYsTUFBQUEsT0FBTyxDQUFDVSxjQUFSLEdBQXlCRixXQUFXLENBQUNHLElBQVosR0FBbUJYLE9BQU8sQ0FBQ1UsY0FBcEQ7QUFDSDs7QUFDRFYsSUFBQUEsT0FBTyxDQUFDWSxJQUFSLEdBQWVKLFdBQVcsQ0FBQ0ksSUFBWixHQUFtQlosT0FBTyxDQUFDWSxJQUExQztBQUNIO0FBQ0osQyxDQUVEOzs7QUFDTyxTQUFTQyxvQkFBVCxDQUE4QkMsS0FBOUIsRUFBcUNaLGdCQUFyQyxFQUF1RDtBQUMxRCxRQUFNYSxPQUFPLEdBQUcsOEJBQWNELEtBQWQsQ0FBaEI7O0FBQ0EsTUFBSUMsT0FBSixFQUFhO0FBQ1RELElBQUFBLEtBQUssR0FBRyxrQ0FBa0JBLEtBQWxCLENBQVI7QUFDSDs7QUFDRCxNQUFJLDJCQUFXQSxLQUFYLEVBQWtCLElBQWxCLENBQUosRUFBNkI7QUFDekJBLElBQUFBLEtBQUssR0FBRyw0QkFBWUEsS0FBWixFQUFtQixHQUFuQixDQUFSO0FBQ0g7O0FBQ0RBLEVBQUFBLEtBQUssR0FBRyxnQ0FBZ0JBLEtBQWhCLENBQVI7O0FBQ0EsUUFBTWIsY0FBYyxHQUFHZSx1QkFBY0MsZUFBZCxFQUF2Qjs7QUFFQSxRQUFNTCxJQUFJLEdBQUcsOEJBQWNFLEtBQWQsQ0FBYjtBQUNBLFFBQU1kLE9BQU8sR0FBRztBQUNaa0IsSUFBQUEsT0FBTyxFQUFFSCxPQUFPLEdBQUcsU0FBSCxHQUFlLFFBRG5CO0FBRVpILElBQUFBLElBQUksRUFBRUE7QUFGTSxHQUFoQjtBQUlBLFFBQU1PLGFBQWEsR0FBRyxzQ0FBc0JMLEtBQXRCLEVBQTZCO0FBQUNNLElBQUFBLFNBQVMsRUFBRSxDQUFDLENBQUNuQjtBQUFkLEdBQTdCLENBQXRCOztBQUNBLE1BQUlrQixhQUFKLEVBQW1CO0FBQ2ZuQixJQUFBQSxPQUFPLENBQUNxQixNQUFSLEdBQWlCLHdCQUFqQjtBQUNBckIsSUFBQUEsT0FBTyxDQUFDVSxjQUFSLEdBQXlCUyxhQUF6QjtBQUNIOztBQUVELE1BQUlsQixjQUFKLEVBQW9CO0FBQ2hCRixJQUFBQSx3QkFBd0IsQ0FBQ0MsT0FBRCxFQUFVQyxjQUFWLEVBQTBCQyxnQkFBMUIsQ0FBeEI7QUFDSDs7QUFFRCxTQUFPRixPQUFQO0FBQ0g7O0FBRWMsTUFBTXNCLG1CQUFOLFNBQWtDQyxlQUFNQyxTQUF4QyxDQUFrRDtBQVM3REMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUseURBYUhDLEdBQUcsSUFBSTtBQUNuQixXQUFLQyxVQUFMLEdBQWtCRCxHQUFsQjtBQUNILEtBZmtCO0FBQUEsc0RBaUJMRSxLQUFELElBQVc7QUFDcEI7QUFDQSxVQUFJLEtBQUtELFVBQUwsQ0FBZ0JFLFdBQWhCLENBQTRCRCxLQUE1QixDQUFKLEVBQXdDO0FBQ3BDO0FBQ0g7O0FBQ0QsWUFBTUUsV0FBVyxHQUFHRixLQUFLLENBQUNHLE1BQU4sSUFBZ0JILEtBQUssQ0FBQ0ksT0FBdEIsSUFBaUNKLEtBQUssQ0FBQ0ssT0FBdkMsSUFBa0RMLEtBQUssQ0FBQ00sUUFBNUU7O0FBQ0EsVUFBSU4sS0FBSyxDQUFDTyxHQUFOLEtBQWNDLGNBQUlDLEtBQWxCLElBQTJCLENBQUNQLFdBQWhDLEVBQTZDO0FBQ3pDLGFBQUtRLFlBQUw7O0FBQ0FWLFFBQUFBLEtBQUssQ0FBQ1csY0FBTjtBQUNILE9BSEQsTUFHTyxJQUFJWCxLQUFLLENBQUNPLEdBQU4sS0FBY0MsY0FBSUksUUFBdEIsRUFBZ0M7QUFDbkMsYUFBS0MsZUFBTCxDQUFxQmIsS0FBckIsRUFBNEIsSUFBNUI7QUFDSCxPQUZNLE1BRUEsSUFBSUEsS0FBSyxDQUFDTyxHQUFOLEtBQWNDLGNBQUlNLFVBQXRCLEVBQWtDO0FBQ3JDLGFBQUtELGVBQUwsQ0FBcUJiLEtBQXJCLEVBQTRCLEtBQTVCO0FBQ0gsT0FGTSxNQUVBLElBQUksS0FBS2UsaUJBQVQsRUFBNEI7QUFDL0IsYUFBS0EsaUJBQUw7QUFDSCxPQUZNLE1BRUEsSUFBSWYsS0FBSyxDQUFDTyxHQUFOLEtBQWNDLGNBQUlRLE1BQXRCLEVBQThCO0FBQ2pDQyw0QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxnQkFEQztBQUVUbkIsVUFBQUEsS0FBSyxFQUFFO0FBRkUsU0FBYjtBQUlIO0FBQ0osS0F0Q2tCO0FBQUEsa0VBa1FNLE1BQU07QUFDM0IsVUFBSSxLQUFLZixLQUFMLENBQVdtQyxPQUFmLEVBQXdCO0FBQ3BCLGFBQUtDLHVCQUFMO0FBQ0gsT0FGRCxNQUVPO0FBQ0hDLFFBQUFBLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixLQUFLQyxlQUExQixFQUEyQ0MsSUFBSSxDQUFDQyxTQUFMLENBQWUsS0FBS3pDLEtBQUwsQ0FBVzBDLGNBQVgsRUFBZixDQUEzQztBQUNIO0FBQ0osS0F4UWtCO0FBQUEsb0RBMFFQQyxPQUFELElBQWE7QUFDcEIsY0FBUUEsT0FBTyxDQUFDVCxNQUFoQjtBQUNJLGFBQUssZ0JBQUw7QUFDQSxhQUFLLGdCQUFMO0FBQ0ksZUFBS3BCLFVBQUwsSUFBbUIsS0FBS0EsVUFBTCxDQUFnQjhCLEtBQWhCLEVBQW5CO0FBQ0E7O0FBQ0osYUFBSyxnQkFBTDtBQUNJLGVBQUtDLGNBQUwsQ0FBb0JGLE9BQU8sQ0FBQ0csT0FBNUI7O0FBQ0E7O0FBQ0osYUFBSyxPQUFMO0FBQ0ksZUFBS0Msb0JBQUwsQ0FBMEJKLE9BQU8sQ0FBQzVCLEtBQWxDOztBQUNBO0FBVlI7QUFZSCxLQXZSa0I7QUFBQSxvREEyVFBBLEtBQUQsSUFBVztBQUNsQixZQUFNO0FBQUNpQyxRQUFBQTtBQUFELFVBQWtCakMsS0FBeEI7O0FBQ0EsVUFBSWlDLGFBQWEsQ0FBQ0MsS0FBZCxDQUFvQkMsTUFBeEIsRUFBZ0M7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsaUNBQWdCQyxjQUFoQixHQUFpQ0MscUJBQWpDLENBQ0lDLEtBQUssQ0FBQ0MsSUFBTixDQUFXUCxhQUFhLENBQUNDLEtBQXpCLENBREosRUFDcUMsS0FBS3JDLEtBQUwsQ0FBVzRDLElBQVgsQ0FBZ0JDLE1BRHJELEVBQzZELEtBQUtDLE9BRGxFO0FBR0g7QUFDSixLQXRVa0I7QUFFZixTQUFLMUQsS0FBTCxHQUFhLElBQWI7QUFDQSxTQUFLYyxVQUFMLEdBQWtCLElBQWxCO0FBQ0EsU0FBSzZDLDRCQUFMLEdBQW9DLElBQXBDOztBQUNBLFVBQU1DLEdBQUcsR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFFBQUlGLEdBQUcsQ0FBQ0csZUFBSixNQUF5QkgsR0FBRyxDQUFDSSxlQUFKLENBQW9CLEtBQUtwRCxLQUFMLENBQVc0QyxJQUFYLENBQWdCQyxNQUFwQyxDQUE3QixFQUEwRTtBQUN0RSxXQUFLM0IsaUJBQUwsR0FBeUIsSUFBSW1DLHdCQUFKLENBQW9CLE1BQU07QUFDL0NMLFFBQUFBLEdBQUcsQ0FBQ00sZ0JBQUosQ0FBcUIsS0FBS3RELEtBQUwsQ0FBVzRDLElBQWhDO0FBQ0gsT0FGd0IsRUFFdEIsS0FGc0IsQ0FBekI7QUFHSDtBQUNKOztBQTZCRDVCLEVBQUFBLGVBQWUsQ0FBQ3VDLENBQUQsRUFBSUMsRUFBSixFQUFRO0FBQ25CO0FBQ0E7QUFDQSxRQUFJRCxDQUFDLENBQUM5QyxRQUFGLElBQWM4QyxDQUFDLENBQUMvQyxPQUFwQixFQUE2QjtBQUU3QixVQUFNaUQsbUJBQW1CLEdBQUdGLENBQUMsQ0FBQ2pELE1BQUYsSUFBWWlELENBQUMsQ0FBQ2hELE9BQTFDO0FBQ0EsVUFBTW1ELHFCQUFxQixHQUFHLENBQUNILENBQUMsQ0FBQ2pELE1BQUgsSUFBYSxDQUFDaUQsQ0FBQyxDQUFDaEQsT0FBaEIsSUFBMkJpRCxFQUEzQixJQUFpQyxDQUFDbEUsdUJBQWNDLGVBQWQsRUFBaEU7O0FBRUEsUUFBSWtFLG1CQUFKLEVBQXlCO0FBQ3JCO0FBQ0EsWUFBTUUsUUFBUSxHQUFHLEtBQUtDLGlCQUFMLENBQXVCSixFQUF2QixDQUFqQjs7QUFDQSxVQUFJRyxRQUFKLEVBQWM7QUFDVjtBQUNBSixRQUFBQSxDQUFDLENBQUN6QyxjQUFGO0FBQ0g7QUFDSixLQVBELE1BT08sSUFBSTRDLHFCQUFKLEVBQTJCO0FBQzlCO0FBQ0EsVUFBSSxLQUFLeEQsVUFBTCxDQUFnQjJELG9CQUFoQixNQUEwQyxLQUFLM0QsVUFBTCxDQUFnQjRELGNBQWhCLEVBQTlDLEVBQWdGO0FBQzVFLGNBQU1DLFNBQVMsR0FBRyxtQ0FBa0IsS0FBSy9ELEtBQUwsQ0FBVzRDLElBQTdCLEVBQW1DLEtBQW5DLENBQWxCOztBQUNBLFlBQUltQixTQUFKLEVBQWU7QUFDWDtBQUNBUixVQUFBQSxDQUFDLENBQUN6QyxjQUFGOztBQUNBTSw4QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFlBQUFBLE1BQU0sRUFBRSxZQURDO0FBRVRuQixZQUFBQSxLQUFLLEVBQUU0RDtBQUZFLFdBQWI7QUFJSDtBQUNKO0FBQ0o7QUFDSixHQTlFNEQsQ0FnRjdEO0FBQ0E7OztBQUNBSCxFQUFBQSxpQkFBaUIsQ0FBQ0osRUFBRCxFQUFLO0FBQ2xCLFVBQU1RLEtBQUssR0FBR1IsRUFBRSxHQUFHLENBQUMsQ0FBSixHQUFRLENBQXhCLENBRGtCLENBRWxCOztBQUNBLFFBQUksS0FBS1Msa0JBQUwsQ0FBd0JDLFlBQXhCLEtBQXlDLEtBQUtELGtCQUFMLENBQXdCRSxPQUF4QixDQUFnQzdCLE1BQTdFLEVBQXFGO0FBQ2pGO0FBQ0EsVUFBSSxDQUFDa0IsRUFBTCxFQUFTO0FBQ0w7QUFDSDs7QUFDRCxXQUFLVCw0QkFBTCxHQUFvQyxLQUFLM0QsS0FBTCxDQUFXMEMsY0FBWCxFQUFwQztBQUNILEtBTkQsTUFNTyxJQUFJLEtBQUttQyxrQkFBTCxDQUF3QkMsWUFBeEIsR0FBdUNGLEtBQXZDLEtBQWlELEtBQUtDLGtCQUFMLENBQXdCRSxPQUF4QixDQUFnQzdCLE1BQXJGLEVBQTZGO0FBQ2hHO0FBQ0EsV0FBS2xELEtBQUwsQ0FBV2dGLEtBQVgsQ0FBaUIsS0FBS3JCLDRCQUF0QjtBQUNBLFdBQUtrQixrQkFBTCxDQUF3QkMsWUFBeEIsR0FBdUMsS0FBS0Qsa0JBQUwsQ0FBd0JFLE9BQXhCLENBQWdDN0IsTUFBdkU7QUFDQTtBQUNIOztBQUNELFVBQU0rQixlQUFlLEdBQUcsS0FBS0osa0JBQUwsQ0FBd0JLLE9BQXhCLENBQWdDTixLQUFoQyxDQUF4Qjs7QUFDQSxRQUFJSyxlQUFKLEVBQXFCO0FBQ2pCLFdBQUtqRixLQUFMLENBQVdnRixLQUFYLENBQWlCQyxlQUFqQjs7QUFDQSxXQUFLbkUsVUFBTCxDQUFnQjhCLEtBQWhCO0FBQ0g7QUFDSjs7QUFFRHVDLEVBQUFBLGVBQWUsR0FBRztBQUNkLFVBQU1DLEtBQUssR0FBRyxLQUFLcEYsS0FBTCxDQUFXb0YsS0FBekI7QUFDQSxVQUFNQyxTQUFTLEdBQUdELEtBQUssQ0FBQyxDQUFELENBQXZCOztBQUNBLFFBQUlDLFNBQUosRUFBZTtBQUNYLFVBQUlBLFNBQVMsQ0FBQ0MsSUFBVixLQUFtQixTQUFuQixJQUFnQ0QsU0FBUyxDQUFDRSxJQUFWLENBQWVDLFVBQWYsQ0FBMEIsR0FBMUIsQ0FBaEMsSUFBa0UsQ0FBQ0gsU0FBUyxDQUFDRSxJQUFWLENBQWVDLFVBQWYsQ0FBMEIsSUFBMUIsQ0FBdkUsRUFBd0c7QUFDcEcsZUFBTyxJQUFQO0FBQ0gsT0FIVSxDQUlYO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBSUgsU0FBUyxDQUFDRSxJQUFWLENBQWVDLFVBQWYsQ0FBMEIsR0FBMUIsS0FBa0MsQ0FBQ0gsU0FBUyxDQUFDRSxJQUFWLENBQWVDLFVBQWYsQ0FBMEIsSUFBMUIsQ0FBbkMsS0FDSUgsU0FBUyxDQUFDQyxJQUFWLEtBQW1CLE9BQW5CLElBQThCRCxTQUFTLENBQUNDLElBQVYsS0FBbUIsZ0JBRHJELENBQUosRUFDNEU7QUFDeEUsZUFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLEtBQVA7QUFDSDs7QUFFREcsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixVQUFNQyxXQUFXLEdBQUcsS0FBSzFGLEtBQUwsQ0FBV29GLEtBQVgsQ0FBaUJPLE1BQWpCLENBQXdCLENBQUNKLElBQUQsRUFBT0ssSUFBUCxLQUFnQjtBQUN4RDtBQUNBLFVBQUlBLElBQUksQ0FBQ04sSUFBTCxLQUFjLFdBQWxCLEVBQStCO0FBQzNCLGVBQU9DLElBQUksR0FBR0ssSUFBSSxDQUFDQyxVQUFuQjtBQUNIOztBQUNELGFBQU9OLElBQUksR0FBR0ssSUFBSSxDQUFDTCxJQUFuQjtBQUNILEtBTm1CLEVBTWpCLEVBTmlCLENBQXBCO0FBT0EsV0FBTyxDQUFDLCtCQUFXLEtBQUszRSxLQUFMLENBQVc0QyxJQUFYLENBQWdCQyxNQUEzQixFQUFtQ2lDLFdBQW5DLENBQUQsRUFBa0RBLFdBQWxELENBQVA7QUFDSDs7QUFFRCxRQUFNSSxnQkFBTixDQUF1QkMsRUFBdkIsRUFBMkI7QUFDdkIsVUFBTUMsR0FBRyxHQUFHRCxFQUFFLEVBQWQ7QUFDQSxRQUFJRSxLQUFLLEdBQUdELEdBQUcsQ0FBQ0MsS0FBaEI7O0FBQ0EsUUFBSUQsR0FBRyxDQUFDRSxPQUFSLEVBQWlCO0FBQ2IsVUFBSTtBQUNBLGNBQU1GLEdBQUcsQ0FBQ0UsT0FBVjtBQUNILE9BRkQsQ0FFRSxPQUFPQyxHQUFQLEVBQVk7QUFDVkYsUUFBQUEsS0FBSyxHQUFHRSxHQUFSO0FBQ0g7QUFDSjs7QUFDRCxRQUFJRixLQUFKLEVBQVc7QUFDUEcsTUFBQUEsT0FBTyxDQUFDSCxLQUFSLENBQWMscUJBQWQsRUFBcUNBLEtBQXJDO0FBQ0EsWUFBTUksV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCLENBRk8sQ0FHUDs7QUFDQSxZQUFNQyxhQUFhLEdBQUcsQ0FBQyxDQUFDUixHQUFHLENBQUNFLE9BQTVCO0FBQ0EsWUFBTU8sS0FBSyxHQUFHRCxhQUFhLEdBQUcsMEJBQUksY0FBSixDQUFILEdBQXlCLDBCQUFJLGVBQUosQ0FBcEQ7QUFFQSxVQUFJRSxPQUFKOztBQUNBLFVBQUksT0FBT1QsS0FBUCxLQUFpQixRQUFyQixFQUErQjtBQUMzQlMsUUFBQUEsT0FBTyxHQUFHVCxLQUFWO0FBQ0gsT0FGRCxNQUVPLElBQUlBLEtBQUssQ0FBQ1UsT0FBVixFQUFtQjtBQUN0QkQsUUFBQUEsT0FBTyxHQUFHVCxLQUFLLENBQUNVLE9BQWhCO0FBQ0gsT0FGTSxNQUVBO0FBQ0hELFFBQUFBLE9BQU8sR0FBRyx5QkFBRywrREFBSCxDQUFWO0FBQ0g7O0FBRURFLHFCQUFNQyxtQkFBTixDQUEwQkosS0FBMUIsRUFBaUMsRUFBakMsRUFBcUNKLFdBQXJDLEVBQWtEO0FBQzlDSSxRQUFBQSxLQUFLLEVBQUUseUJBQUdBLEtBQUgsQ0FEdUM7QUFFOUNLLFFBQUFBLFdBQVcsRUFBRUo7QUFGaUMsT0FBbEQ7QUFJSCxLQXBCRCxNQW9CTztBQUNITixNQUFBQSxPQUFPLENBQUNXLEdBQVIsQ0FBWSxrQkFBWjtBQUNIO0FBQ0o7O0FBRUQsUUFBTXRGLFlBQU4sR0FBcUI7QUFDakIsUUFBSSxLQUFLekIsS0FBTCxDQUFXbUMsT0FBZixFQUF3QjtBQUNwQjtBQUNIOztBQUVELFFBQUk2RSxVQUFVLEdBQUcsSUFBakI7O0FBRUEsUUFBSSxDQUFDLDhCQUFjLEtBQUtoSCxLQUFuQixDQUFELElBQThCLEtBQUttRixlQUFMLEVBQWxDLEVBQTBEO0FBQ3RELFlBQU0sQ0FBQ2EsR0FBRCxFQUFNTixXQUFOLElBQXFCLEtBQUtELGdCQUFMLEVBQTNCOztBQUNBLFVBQUlPLEdBQUosRUFBUztBQUNMZ0IsUUFBQUEsVUFBVSxHQUFHLEtBQWI7O0FBQ0EsYUFBS2xCLGdCQUFMLENBQXNCRSxHQUF0QjtBQUNILE9BSEQsTUFHTztBQUNIO0FBQ0EsY0FBTWlCLGNBQWMsR0FBR1gsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQSxjQUFNO0FBQUNXLFVBQUFBO0FBQUQsWUFBYU4sZUFBTUMsbUJBQU4sQ0FBMEIsaUJBQTFCLEVBQTZDLEVBQTdDLEVBQWlESSxjQUFqRCxFQUFpRTtBQUNoRlIsVUFBQUEsS0FBSyxFQUFFLHlCQUFHLGlCQUFILENBRHlFO0FBRWhGSyxVQUFBQSxXQUFXLEVBQUUsMENBQ1Qsd0NBQ00seUJBQUcsdUNBQUgsRUFBNEM7QUFBQ3BCLFlBQUFBO0FBQUQsV0FBNUMsQ0FETixDQURTLEVBSVQsd0NBQ00seUJBQUcsZ0VBQ0QseUNBREYsRUFDNkMsRUFEN0MsRUFDaUQ7QUFDL0N5QixZQUFBQSxJQUFJLEVBQUVDLENBQUMsSUFBSSwyQ0FBUUEsQ0FBUjtBQURvQyxXQURqRCxDQUROLENBSlMsRUFVVCx3Q0FDTSx5QkFBRyx5RUFBSCxFQUE4RSxFQUE5RSxFQUFrRjtBQUNoRkQsWUFBQUEsSUFBSSxFQUFFQyxDQUFDLElBQUksMkNBQVFBLENBQVI7QUFEcUUsV0FBbEYsQ0FETixDQVZTLENBRm1FO0FBa0JoRkMsVUFBQUEsTUFBTSxFQUFFLHlCQUFHLGlCQUFIO0FBbEJ3RSxTQUFqRSxDQUFuQjs7QUFvQkEsY0FBTSxDQUFDQyxVQUFELElBQWUsTUFBTUosUUFBM0IsQ0F2QkcsQ0F3Qkg7O0FBQ0EsWUFBSSxDQUFDSSxVQUFMLEVBQWlCO0FBQ3BCO0FBQ0o7O0FBRUQsUUFBSU4sVUFBSixFQUFnQjtBQUNaLFlBQU1PLE9BQU8sR0FBRyxDQUFDLENBQUNySCx1QkFBY0MsZUFBZCxFQUFsQjtBQUNBLFlBQU07QUFBQ3NELFFBQUFBO0FBQUQsVUFBVyxLQUFLN0MsS0FBTCxDQUFXNEMsSUFBNUI7QUFDQSxZQUFNdEUsT0FBTyxHQUFHYSxvQkFBb0IsQ0FBQyxLQUFLQyxLQUFOLEVBQWEsS0FBS1ksS0FBTCxDQUFXeEIsZ0JBQXhCLENBQXBDO0FBQ0EsV0FBS3NFLE9BQUwsQ0FBYThELFdBQWIsQ0FBeUIvRCxNQUF6QixFQUFpQ3ZFLE9BQWpDOztBQUNBLFVBQUlxSSxPQUFKLEVBQWE7QUFDVDtBQUNBO0FBQ0F2Riw0QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxnQkFEQztBQUVUbkIsVUFBQUEsS0FBSyxFQUFFO0FBRkUsU0FBYjtBQUlIO0FBQ0o7O0FBRUQsU0FBSzhELGtCQUFMLENBQXdCNEMsSUFBeEIsQ0FBNkIsS0FBS3pILEtBQWxDLEVBeERpQixDQXlEakI7O0FBQ0EsU0FBS0EsS0FBTCxDQUFXZ0YsS0FBWCxDQUFpQixFQUFqQjs7QUFDQSxTQUFLbEUsVUFBTCxDQUFnQjRHLGdCQUFoQjs7QUFDQSxTQUFLNUcsVUFBTCxDQUFnQjhCLEtBQWhCOztBQUNBLFNBQUtSLHVCQUFMO0FBQ0g7O0FBRUR1RixFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixTQUFLN0csVUFBTCxDQUFnQjhHLG1CQUFoQixHQUFzQ0MsZ0JBQXRDLENBQXVELE9BQXZELEVBQWdFLEtBQUtDLFFBQXJFLEVBQStFLElBQS9FO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CL0Ysd0JBQUlnRyxVQUFKLENBQWUsS0FBS0MsYUFBcEI7O0FBQ0EsU0FBS25ILFVBQUwsQ0FBZ0I4RyxtQkFBaEIsR0FBc0NNLG1CQUF0QyxDQUEwRCxPQUExRCxFQUFtRSxLQUFLSixRQUF4RSxFQUFrRixJQUFsRjtBQUNILEdBL080RCxDQWlQN0Q7OztBQUNBSyxFQUFBQSx5QkFBeUIsR0FBRztBQUFFO0FBQzFCLFVBQU1DLFdBQVcsR0FBRyxJQUFJQyx5QkFBSixDQUF1QixLQUFLekgsS0FBTCxDQUFXNEMsSUFBbEMsRUFBd0MsS0FBS0UsT0FBN0MsQ0FBcEI7QUFDQSxVQUFNMEIsS0FBSyxHQUFHLEtBQUtrRCx5QkFBTCxDQUErQkYsV0FBL0IsS0FBK0MsRUFBN0Q7QUFDQSxTQUFLcEksS0FBTCxHQUFhLElBQUl1SSxjQUFKLENBQWdCbkQsS0FBaEIsRUFBdUJnRCxXQUF2QixDQUFiO0FBQ0EsU0FBS0gsYUFBTCxHQUFxQmpHLG9CQUFJd0csUUFBSixDQUFhLEtBQUtDLFFBQWxCLENBQXJCO0FBQ0EsU0FBSzVELGtCQUFMLEdBQTBCLElBQUk2RCwyQkFBSixDQUF1QixLQUFLOUgsS0FBTCxDQUFXNEMsSUFBWCxDQUFnQkMsTUFBdkMsRUFBK0MsNEJBQS9DLENBQTFCO0FBQ0g7O0FBRUQsTUFBSWxCLGVBQUosR0FBc0I7QUFDbEIsd0NBQTZCLEtBQUszQixLQUFMLENBQVc0QyxJQUFYLENBQWdCQyxNQUE3QztBQUNIOztBQUVEckIsRUFBQUEsdUJBQXVCLEdBQUc7QUFDdEJDLElBQUFBLFlBQVksQ0FBQ3NHLFVBQWIsQ0FBd0IsS0FBS3BHLGVBQTdCO0FBQ0g7O0FBRUQrRixFQUFBQSx5QkFBeUIsQ0FBQ0YsV0FBRCxFQUFjO0FBQ25DLFVBQU1RLElBQUksR0FBR3ZHLFlBQVksQ0FBQzZDLE9BQWIsQ0FBcUIsS0FBSzNDLGVBQTFCLENBQWI7O0FBQ0EsUUFBSXFHLElBQUosRUFBVTtBQUNOLFlBQU0zRCxlQUFlLEdBQUd6QyxJQUFJLENBQUNxRyxLQUFMLENBQVdELElBQVgsQ0FBeEI7QUFDQSxZQUFNeEQsS0FBSyxHQUFHSCxlQUFlLENBQUM2RCxHQUFoQixDQUFvQkMsQ0FBQyxJQUFJWCxXQUFXLENBQUNZLGVBQVosQ0FBNEJELENBQTVCLENBQXpCLENBQWQ7QUFDQSxhQUFPM0QsS0FBUDtBQUNIO0FBQ0o7O0FBeUJEdkMsRUFBQUEsY0FBYyxDQUFDb0csTUFBRCxFQUFTO0FBQ25CLFVBQU07QUFBQ2pKLE1BQUFBO0FBQUQsUUFBVSxJQUFoQjtBQUNBLFVBQU07QUFBQ29JLE1BQUFBO0FBQUQsUUFBZ0JwSSxLQUF0QjtBQUNBLFVBQU1rSixNQUFNLEdBQUcsS0FBS3RJLEtBQUwsQ0FBVzRDLElBQVgsQ0FBZ0IyRixTQUFoQixDQUEwQkYsTUFBMUIsQ0FBZjtBQUNBLFVBQU1HLFdBQVcsR0FBR0YsTUFBTSxHQUN0QkEsTUFBTSxDQUFDRyxjQURlLEdBQ0VKLE1BRDVCOztBQUVBLFVBQU1LLEtBQUssR0FBRyxLQUFLeEksVUFBTCxDQUFnQnlJLFFBQWhCLEVBQWQ7O0FBQ0EsVUFBTUMsUUFBUSxHQUFHeEosS0FBSyxDQUFDeUosaUJBQU4sQ0FBd0JILEtBQUssQ0FBQ0ksTUFBOUIsRUFBc0NKLEtBQUssQ0FBQ0ssU0FBNUMsQ0FBakIsQ0FQbUIsQ0FRbkI7O0FBQ0EsVUFBTUMsV0FBVyxHQUFHSixRQUFRLENBQUNLLEtBQVQsR0FBaUIsQ0FBakIsR0FBcUJMLFFBQVEsQ0FBQ0ssS0FBOUIsR0FBc0MsQ0FBMUQ7QUFDQSxVQUFNekUsS0FBSyxHQUFHZ0QsV0FBVyxDQUFDMEIsa0JBQVosQ0FBK0JGLFdBQS9CLEVBQTRDUixXQUE1QyxFQUF5REgsTUFBekQsQ0FBZDtBQUNBakosSUFBQUEsS0FBSyxDQUFDK0osU0FBTixDQUFnQixNQUFNO0FBQ2xCLFlBQU1DLFFBQVEsR0FBR2hLLEtBQUssQ0FBQ2lLLE1BQU4sQ0FBYTdFLEtBQWIsRUFBb0JvRSxRQUFwQixDQUFqQjtBQUNBLGFBQU94SixLQUFLLENBQUN5SixpQkFBTixDQUF3QkgsS0FBSyxDQUFDSSxNQUFOLEdBQWVNLFFBQXZDLEVBQWlELElBQWpELENBQVA7QUFDSCxLQUhELEVBWG1CLENBZW5COztBQUNBLFNBQUtsSixVQUFMLElBQW1CLEtBQUtBLFVBQUwsQ0FBZ0I4QixLQUFoQixFQUFuQjtBQUNIOztBQUVERyxFQUFBQSxvQkFBb0IsQ0FBQ2hDLEtBQUQsRUFBUTtBQUN4QixVQUFNO0FBQUNmLE1BQUFBO0FBQUQsUUFBVSxJQUFoQjtBQUNBLFVBQU07QUFBQ29JLE1BQUFBO0FBQUQsUUFBZ0JwSSxLQUF0QjtBQUNBLFVBQU1rSyxVQUFVLEdBQUcsNkJBQVduSixLQUFYLEVBQWtCcUgsV0FBbEIsRUFBK0I7QUFBRStCLE1BQUFBLGVBQWUsRUFBRTtBQUFuQixLQUEvQixDQUFuQixDQUh3QixDQUl4Qjs7QUFDQUQsSUFBQUEsVUFBVSxDQUFDRSxJQUFYLENBQWdCaEMsV0FBVyxDQUFDaUMsT0FBWixFQUFoQjtBQUNBSCxJQUFBQSxVQUFVLENBQUNFLElBQVgsQ0FBZ0JoQyxXQUFXLENBQUNpQyxPQUFaLEVBQWhCO0FBQ0FySyxJQUFBQSxLQUFLLENBQUMrSixTQUFOLENBQWdCLE1BQU07QUFDbEIsWUFBTUMsUUFBUSxHQUFHaEssS0FBSyxDQUFDaUssTUFBTixDQUFhQyxVQUFiLEVBQXlCbEssS0FBSyxDQUFDeUosaUJBQU4sQ0FBd0IsQ0FBeEIsQ0FBekIsQ0FBakI7QUFDQSxhQUFPekosS0FBSyxDQUFDeUosaUJBQU4sQ0FBd0JPLFFBQXhCLEVBQWtDLElBQWxDLENBQVA7QUFDSCxLQUhELEVBUHdCLENBV3hCOztBQUNBLFNBQUtsSixVQUFMLElBQW1CLEtBQUtBLFVBQUwsQ0FBZ0I4QixLQUFoQixFQUFuQjtBQUNIOztBQWVEMEgsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHdCQUFmO0FBQXdDLE1BQUEsT0FBTyxFQUFFLEtBQUtDLGFBQXREO0FBQXFFLE1BQUEsU0FBUyxFQUFFLEtBQUtDO0FBQXJGLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMscUJBQUQ7QUFBYyxNQUFBLGdCQUFnQixFQUFFLEtBQUs1SixLQUFMLENBQVd4QjtBQUEzQyxNQURKLENBREosRUFJSSw2QkFBQyw2QkFBRDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUtxTCxhQURkO0FBRUksTUFBQSxLQUFLLEVBQUUsS0FBS3pLLEtBRmhCO0FBR0ksTUFBQSxJQUFJLEVBQUUsS0FBS1ksS0FBTCxDQUFXNEMsSUFIckI7QUFJSSxNQUFBLEtBQUssRUFBRSxLQUFLNUMsS0FBTCxDQUFXOEosV0FKdEI7QUFLSSxNQUFBLFdBQVcsRUFBRSxLQUFLOUosS0FBTCxDQUFXOEosV0FMNUI7QUFNSSxNQUFBLFFBQVEsRUFBRSxLQUFLQztBQU5uQixNQUpKLENBREo7QUFlSDs7QUFqVzREOzs7OEJBQTVDbkssbUIsZUFDRTtBQUNmZ0QsRUFBQUEsSUFBSSxFQUFFb0gsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRFI7QUFFZkosRUFBQUEsV0FBVyxFQUFFRSxtQkFBVUcsTUFGUjtBQUdmM0wsRUFBQUEsZ0JBQWdCLEVBQUV3TCxtQkFBVUMsTUFBVixDQUFpQkM7QUFIcEIsQzs4QkFERnRLLG1CLGlCQU9Jd0ssNEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IEVkaXRvck1vZGVsIGZyb20gJy4uLy4uLy4uL2VkaXRvci9tb2RlbCc7XHJcbmltcG9ydCB7XHJcbiAgICBodG1sU2VyaWFsaXplSWZOZWVkZWQsXHJcbiAgICB0ZXh0U2VyaWFsaXplLFxyXG4gICAgY29udGFpbnNFbW90ZSxcclxuICAgIHN0cmlwRW1vdGVDb21tYW5kLFxyXG4gICAgdW5lc2NhcGVNZXNzYWdlLFxyXG4gICAgc3RhcnRzV2l0aCxcclxuICAgIHN0cmlwUHJlZml4LFxyXG59IGZyb20gJy4uLy4uLy4uL2VkaXRvci9zZXJpYWxpemUnO1xyXG5pbXBvcnQge0NvbW1hbmRQYXJ0Q3JlYXRvcn0gZnJvbSAnLi4vLi4vLi4vZWRpdG9yL3BhcnRzJztcclxuaW1wb3J0IEJhc2ljTWVzc2FnZUNvbXBvc2VyIGZyb20gXCIuL0Jhc2ljTWVzc2FnZUNvbXBvc2VyXCI7XHJcbmltcG9ydCBSZXBseVByZXZpZXcgZnJvbSBcIi4vUmVwbHlQcmV2aWV3XCI7XHJcbmltcG9ydCBSb29tVmlld1N0b3JlIGZyb20gJy4uLy4uLy4uL3N0b3Jlcy9Sb29tVmlld1N0b3JlJztcclxuaW1wb3J0IFJlcGx5VGhyZWFkIGZyb20gXCIuLi9lbGVtZW50cy9SZXBseVRocmVhZFwiO1xyXG5pbXBvcnQge3BhcnNlRXZlbnR9IGZyb20gJy4uLy4uLy4uL2VkaXRvci9kZXNlcmlhbGl6ZSc7XHJcbmltcG9ydCB7ZmluZEVkaXRhYmxlRXZlbnR9IGZyb20gJy4uLy4uLy4uL3V0aWxzL0V2ZW50VXRpbHMnO1xyXG5pbXBvcnQgU2VuZEhpc3RvcnlNYW5hZ2VyIGZyb20gXCIuLi8uLi8uLi9TZW5kSGlzdG9yeU1hbmFnZXJcIjtcclxuaW1wb3J0IHtnZXRDb21tYW5kfSBmcm9tICcuLi8uLi8uLi9TbGFzaENvbW1hbmRzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuaW1wb3J0IHtfdCwgX3RkfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgQ29udGVudE1lc3NhZ2VzIGZyb20gJy4uLy4uLy4uL0NvbnRlbnRNZXNzYWdlcyc7XHJcbmltcG9ydCB7S2V5fSBmcm9tIFwiLi4vLi4vLi4vS2V5Ym9hcmRcIjtcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gXCIuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWdcIjtcclxuaW1wb3J0IFJhdGVMaW1pdGVkRnVuYyBmcm9tICcuLi8uLi8uLi9yYXRlbGltaXRlZGZ1bmMnO1xyXG5cclxuZnVuY3Rpb24gYWRkUmVwbHlUb01lc3NhZ2VDb250ZW50KGNvbnRlbnQsIHJlcGxpZWRUb0V2ZW50LCBwZXJtYWxpbmtDcmVhdG9yKSB7XHJcbiAgICBjb25zdCByZXBseUNvbnRlbnQgPSBSZXBseVRocmVhZC5tYWtlUmVwbHlNaXhJbihyZXBsaWVkVG9FdmVudCk7XHJcbiAgICBPYmplY3QuYXNzaWduKGNvbnRlbnQsIHJlcGx5Q29udGVudCk7XHJcblxyXG4gICAgLy8gUGFydCBvZiBSZXBsaWVzIGZhbGxiYWNrIHN1cHBvcnQgLSBwcmVwZW5kIHRoZSB0ZXh0IHdlJ3JlIHNlbmRpbmdcclxuICAgIC8vIHdpdGggdGhlIHRleHQgd2UncmUgcmVwbHlpbmcgdG9cclxuICAgIGNvbnN0IG5lc3RlZFJlcGx5ID0gUmVwbHlUaHJlYWQuZ2V0TmVzdGVkUmVwbHlUZXh0KHJlcGxpZWRUb0V2ZW50LCBwZXJtYWxpbmtDcmVhdG9yKTtcclxuICAgIGlmIChuZXN0ZWRSZXBseSkge1xyXG4gICAgICAgIGlmIChjb250ZW50LmZvcm1hdHRlZF9ib2R5KSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuZm9ybWF0dGVkX2JvZHkgPSBuZXN0ZWRSZXBseS5odG1sICsgY29udGVudC5mb3JtYXR0ZWRfYm9keTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29udGVudC5ib2R5ID0gbmVzdGVkUmVwbHkuYm9keSArIGNvbnRlbnQuYm9keTtcclxuICAgIH1cclxufVxyXG5cclxuLy8gZXhwb3J0ZWQgZm9yIHRlc3RzXHJcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVNZXNzYWdlQ29udGVudChtb2RlbCwgcGVybWFsaW5rQ3JlYXRvcikge1xyXG4gICAgY29uc3QgaXNFbW90ZSA9IGNvbnRhaW5zRW1vdGUobW9kZWwpO1xyXG4gICAgaWYgKGlzRW1vdGUpIHtcclxuICAgICAgICBtb2RlbCA9IHN0cmlwRW1vdGVDb21tYW5kKG1vZGVsKTtcclxuICAgIH1cclxuICAgIGlmIChzdGFydHNXaXRoKG1vZGVsLCBcIi8vXCIpKSB7XHJcbiAgICAgICAgbW9kZWwgPSBzdHJpcFByZWZpeChtb2RlbCwgXCIvXCIpO1xyXG4gICAgfVxyXG4gICAgbW9kZWwgPSB1bmVzY2FwZU1lc3NhZ2UobW9kZWwpO1xyXG4gICAgY29uc3QgcmVwbGllZFRvRXZlbnQgPSBSb29tVmlld1N0b3JlLmdldFF1b3RpbmdFdmVudCgpO1xyXG5cclxuICAgIGNvbnN0IGJvZHkgPSB0ZXh0U2VyaWFsaXplKG1vZGVsKTtcclxuICAgIGNvbnN0IGNvbnRlbnQgPSB7XHJcbiAgICAgICAgbXNndHlwZTogaXNFbW90ZSA/IFwibS5lbW90ZVwiIDogXCJtLnRleHRcIixcclxuICAgICAgICBib2R5OiBib2R5LFxyXG4gICAgfTtcclxuICAgIGNvbnN0IGZvcm1hdHRlZEJvZHkgPSBodG1sU2VyaWFsaXplSWZOZWVkZWQobW9kZWwsIHtmb3JjZUhUTUw6ICEhcmVwbGllZFRvRXZlbnR9KTtcclxuICAgIGlmIChmb3JtYXR0ZWRCb2R5KSB7XHJcbiAgICAgICAgY29udGVudC5mb3JtYXQgPSBcIm9yZy5tYXRyaXguY3VzdG9tLmh0bWxcIjtcclxuICAgICAgICBjb250ZW50LmZvcm1hdHRlZF9ib2R5ID0gZm9ybWF0dGVkQm9keTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAocmVwbGllZFRvRXZlbnQpIHtcclxuICAgICAgICBhZGRSZXBseVRvTWVzc2FnZUNvbnRlbnQoY29udGVudCwgcmVwbGllZFRvRXZlbnQsIHBlcm1hbGlua0NyZWF0b3IpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBjb250ZW50O1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZW5kTWVzc2FnZUNvbXBvc2VyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIHBlcm1hbGlua0NyZWF0b3I6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGNvbnRleHRUeXBlID0gTWF0cml4Q2xpZW50Q29udGV4dDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLm1vZGVsID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9lZGl0b3JSZWYgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuY3VycmVudGx5Q29tcG9zZWRFZGl0b3JTdGF0ZSA9IG51bGw7XHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGlmIChjbGkuaXNDcnlwdG9FbmFibGVkKCkgJiYgY2xpLmlzUm9vbUVuY3J5cHRlZCh0aGlzLnByb3BzLnJvb20ucm9vbUlkKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9wcmVwYXJlVG9FbmNyeXB0ID0gbmV3IFJhdGVMaW1pdGVkRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjbGkucHJlcGFyZVRvRW5jcnlwdCh0aGlzLnByb3BzLnJvb20pO1xyXG4gICAgICAgICAgICB9LCA2MDAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9zZXRFZGl0b3JSZWYgPSByZWYgPT4ge1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZiA9IHJlZjtcclxuICAgIH07XHJcblxyXG4gICAgX29uS2V5RG93biA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIC8vIGlnbm9yZSBhbnkga2V5cHJlc3Mgd2hpbGUgZG9pbmcgSU1FIGNvbXBvc2l0aW9uc1xyXG4gICAgICAgIGlmICh0aGlzLl9lZGl0b3JSZWYuaXNDb21wb3NpbmcoZXZlbnQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgaGFzTW9kaWZpZXIgPSBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5tZXRhS2V5IHx8IGV2ZW50LnNoaWZ0S2V5O1xyXG4gICAgICAgIGlmIChldmVudC5rZXkgPT09IEtleS5FTlRFUiAmJiAhaGFzTW9kaWZpZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2VuZE1lc3NhZ2UoKTtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gS2V5LkFSUk9XX1VQKSB7XHJcbiAgICAgICAgICAgIHRoaXMub25WZXJ0aWNhbEFycm93KGV2ZW50LCB0cnVlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gS2V5LkFSUk9XX0RPV04pIHtcclxuICAgICAgICAgICAgdGhpcy5vblZlcnRpY2FsQXJyb3coZXZlbnQsIGZhbHNlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuX3ByZXBhcmVUb0VuY3J5cHQpIHtcclxuICAgICAgICAgICAgdGhpcy5fcHJlcGFyZVRvRW5jcnlwdCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5ID09PSBLZXkuRVNDQVBFKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZXBseV90b19ldmVudCcsXHJcbiAgICAgICAgICAgICAgICBldmVudDogbnVsbCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvblZlcnRpY2FsQXJyb3coZSwgdXApIHtcclxuICAgICAgICAvLyBhcnJvd3MgZnJvbSBhbiBpbml0aWFsLWNhcmV0IGNvbXBvc2VyIG5hdmlnYXRlcyByZWNlbnQgbWVzc2FnZXMgdG8gZWRpdFxyXG4gICAgICAgIC8vIGN0cmwtYWx0LWFycm93cyBuYXZpZ2F0ZSBzZW5kIGhpc3RvcnlcclxuICAgICAgICBpZiAoZS5zaGlmdEtleSB8fCBlLm1ldGFLZXkpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3Qgc2hvdWxkU2VsZWN0SGlzdG9yeSA9IGUuYWx0S2V5ICYmIGUuY3RybEtleTtcclxuICAgICAgICBjb25zdCBzaG91bGRFZGl0TGFzdE1lc3NhZ2UgPSAhZS5hbHRLZXkgJiYgIWUuY3RybEtleSAmJiB1cCAmJiAhUm9vbVZpZXdTdG9yZS5nZXRRdW90aW5nRXZlbnQoKTtcclxuXHJcbiAgICAgICAgaWYgKHNob3VsZFNlbGVjdEhpc3RvcnkpIHtcclxuICAgICAgICAgICAgLy8gVHJ5IHNlbGVjdCBjb21wb3NlciBoaXN0b3J5XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkID0gdGhpcy5zZWxlY3RTZW5kSGlzdG9yeSh1cCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gV2UncmUgc2VsZWN0aW5nIGhpc3RvcnksIHNvIHByZXZlbnQgdGhlIGtleSBldmVudCBmcm9tIGRvaW5nIGFueXRoaW5nIGVsc2VcclxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoc2hvdWxkRWRpdExhc3RNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIC8vIHNlbGVjdGlvbiBtdXN0IGJlIGNvbGxhcHNlZCBhbmQgY2FyZXQgYXQgc3RhcnRcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2VkaXRvclJlZi5pc1NlbGVjdGlvbkNvbGxhcHNlZCgpICYmIHRoaXMuX2VkaXRvclJlZi5pc0NhcmV0QXRTdGFydCgpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBlZGl0RXZlbnQgPSBmaW5kRWRpdGFibGVFdmVudCh0aGlzLnByb3BzLnJvb20sIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIGlmIChlZGl0RXZlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSdyZSBzZWxlY3RpbmcgaGlzdG9yeSwgc28gcHJldmVudCB0aGUga2V5IGV2ZW50IGZyb20gZG9pbmcgYW55dGhpbmcgZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdlZGl0X2V2ZW50JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQ6IGVkaXRFdmVudCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyB3ZSBrZWVwIHNlbnQgbWVzc2FnZXMvY29tbWFuZHMgaW4gYSBzZXBhcmF0ZSBoaXN0b3J5IChzZXBhcmF0ZSBmcm9tIHVuZG8gaGlzdG9yeSlcclxuICAgIC8vIHNvIHlvdSBjYW4gYWx0K3VwL2Rvd24gaW4gdGhlbVxyXG4gICAgc2VsZWN0U2VuZEhpc3RvcnkodXApIHtcclxuICAgICAgICBjb25zdCBkZWx0YSA9IHVwID8gLTEgOiAxO1xyXG4gICAgICAgIC8vIFRydWUgaWYgd2UgYXJlIG5vdCBjdXJyZW50bHkgc2VsZWN0aW5nIGhpc3RvcnksIGJ1dCBjb21wb3NpbmcgYSBtZXNzYWdlXHJcbiAgICAgICAgaWYgKHRoaXMuc2VuZEhpc3RvcnlNYW5hZ2VyLmN1cnJlbnRJbmRleCA9PT0gdGhpcy5zZW5kSGlzdG9yeU1hbmFnZXIuaGlzdG9yeS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgLy8gV2UgY2FuJ3QgZ28gYW55IGZ1cnRoZXIgLSB0aGVyZSBpc24ndCBhbnkgbW9yZSBoaXN0b3J5LCBzbyBub3AuXHJcbiAgICAgICAgICAgIGlmICghdXApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRseUNvbXBvc2VkRWRpdG9yU3RhdGUgPSB0aGlzLm1vZGVsLnNlcmlhbGl6ZVBhcnRzKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnNlbmRIaXN0b3J5TWFuYWdlci5jdXJyZW50SW5kZXggKyBkZWx0YSA9PT0gdGhpcy5zZW5kSGlzdG9yeU1hbmFnZXIuaGlzdG9yeS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgLy8gVHJ1ZSB3aGVuIHdlIHJldHVybiB0byB0aGUgbWVzc2FnZSBiZWluZyBjb21wb3NlZCBjdXJyZW50bHlcclxuICAgICAgICAgICAgdGhpcy5tb2RlbC5yZXNldCh0aGlzLmN1cnJlbnRseUNvbXBvc2VkRWRpdG9yU3RhdGUpO1xyXG4gICAgICAgICAgICB0aGlzLnNlbmRIaXN0b3J5TWFuYWdlci5jdXJyZW50SW5kZXggPSB0aGlzLnNlbmRIaXN0b3J5TWFuYWdlci5oaXN0b3J5Lmxlbmd0aDtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBzZXJpYWxpemVkUGFydHMgPSB0aGlzLnNlbmRIaXN0b3J5TWFuYWdlci5nZXRJdGVtKGRlbHRhKTtcclxuICAgICAgICBpZiAoc2VyaWFsaXplZFBhcnRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubW9kZWwucmVzZXQoc2VyaWFsaXplZFBhcnRzKTtcclxuICAgICAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9pc1NsYXNoQ29tbWFuZCgpIHtcclxuICAgICAgICBjb25zdCBwYXJ0cyA9IHRoaXMubW9kZWwucGFydHM7XHJcbiAgICAgICAgY29uc3QgZmlyc3RQYXJ0ID0gcGFydHNbMF07XHJcbiAgICAgICAgaWYgKGZpcnN0UGFydCkge1xyXG4gICAgICAgICAgICBpZiAoZmlyc3RQYXJ0LnR5cGUgPT09IFwiY29tbWFuZFwiICYmIGZpcnN0UGFydC50ZXh0LnN0YXJ0c1dpdGgoXCIvXCIpICYmICFmaXJzdFBhcnQudGV4dC5zdGFydHNXaXRoKFwiLy9cIikpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGJlIGV4dHJhIHJlc2lsaWVudCB3aGVuIHNvbWVob3cgdGhlIEF1dG9jb21wbGV0ZVdyYXBwZXJNb2RlbCBvclxyXG4gICAgICAgICAgICAvLyBDb21tYW5kUGFydENyZWF0b3IgZmFpbHMgdG8gaW5zZXJ0IGEgY29tbWFuZCBwYXJ0LCBzbyB3ZSBkb24ndCBzZW5kXHJcbiAgICAgICAgICAgIC8vIGEgY29tbWFuZCBhcyBhIG1lc3NhZ2VcclxuICAgICAgICAgICAgaWYgKGZpcnN0UGFydC50ZXh0LnN0YXJ0c1dpdGgoXCIvXCIpICYmICFmaXJzdFBhcnQudGV4dC5zdGFydHNXaXRoKFwiLy9cIilcclxuICAgICAgICAgICAgICAgICYmIChmaXJzdFBhcnQudHlwZSA9PT0gXCJwbGFpblwiIHx8IGZpcnN0UGFydC50eXBlID09PSBcInBpbGwtY2FuZGlkYXRlXCIpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldFNsYXNoQ29tbWFuZCgpIHtcclxuICAgICAgICBjb25zdCBjb21tYW5kVGV4dCA9IHRoaXMubW9kZWwucGFydHMucmVkdWNlKCh0ZXh0LCBwYXJ0KSA9PiB7XHJcbiAgICAgICAgICAgIC8vIHVzZSBteGlkIHRvIHRleHRpZnkgdXNlciBwaWxscyBpbiBhIGNvbW1hbmRcclxuICAgICAgICAgICAgaWYgKHBhcnQudHlwZSA9PT0gXCJ1c2VyLXBpbGxcIikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRleHQgKyBwYXJ0LnJlc291cmNlSWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRleHQgKyBwYXJ0LnRleHQ7XHJcbiAgICAgICAgfSwgXCJcIik7XHJcbiAgICAgICAgcmV0dXJuIFtnZXRDb21tYW5kKHRoaXMucHJvcHMucm9vbS5yb29tSWQsIGNvbW1hbmRUZXh0KSwgY29tbWFuZFRleHRdO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9ydW5TbGFzaENvbW1hbmQoZm4pIHtcclxuICAgICAgICBjb25zdCBjbWQgPSBmbigpO1xyXG4gICAgICAgIGxldCBlcnJvciA9IGNtZC5lcnJvcjtcclxuICAgICAgICBpZiAoY21kLnByb21pc2UpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGF3YWl0IGNtZC5wcm9taXNlO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIGVycm9yID0gZXJyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChlcnJvcikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQ29tbWFuZCBmYWlsdXJlOiAlc1wiLCBlcnJvcik7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIC8vIGFzc3VtZSB0aGUgZXJyb3IgaXMgYSBzZXJ2ZXIgZXJyb3Igd2hlbiB0aGUgY29tbWFuZCBpcyBhc3luY1xyXG4gICAgICAgICAgICBjb25zdCBpc1NlcnZlckVycm9yID0gISFjbWQucHJvbWlzZTtcclxuICAgICAgICAgICAgY29uc3QgdGl0bGUgPSBpc1NlcnZlckVycm9yID8gX3RkKFwiU2VydmVyIGVycm9yXCIpIDogX3RkKFwiQ29tbWFuZCBlcnJvclwiKTtcclxuXHJcbiAgICAgICAgICAgIGxldCBlcnJUZXh0O1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGVycm9yID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgZXJyVGV4dCA9IGVycm9yO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGVycm9yLm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgIGVyclRleHQgPSBlcnJvci5tZXNzYWdlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZXJyVGV4dCA9IF90KFwiU2VydmVyIHVuYXZhaWxhYmxlLCBvdmVybG9hZGVkLCBvciBzb21ldGhpbmcgZWxzZSB3ZW50IHdyb25nLlwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyh0aXRsZSwgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QodGl0bGUpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGVyclRleHQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ29tbWFuZCBzdWNjZXNzLlwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3NlbmRNZXNzYWdlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLm1vZGVsLmlzRW1wdHkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHNob3VsZFNlbmQgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAoIWNvbnRhaW5zRW1vdGUodGhpcy5tb2RlbCkgJiYgdGhpcy5faXNTbGFzaENvbW1hbmQoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBbY21kLCBjb21tYW5kVGV4dF0gPSB0aGlzLl9nZXRTbGFzaENvbW1hbmQoKTtcclxuICAgICAgICAgICAgaWYgKGNtZCkge1xyXG4gICAgICAgICAgICAgICAgc2hvdWxkU2VuZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcnVuU2xhc2hDb21tYW5kKGNtZCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBhc2sgdGhlIHVzZXIgaWYgdGhlaXIgdW5rbm93biBjb21tYW5kIHNob3VsZCBiZSBzZW50IGFzIGEgbWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHtmaW5pc2hlZH0gPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKFwiVW5rbm93biBjb21tYW5kXCIsIFwiXCIsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiVW5rbm93biBDb21tYW5kXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJVbnJlY29nbmlzZWQgY29tbWFuZDogJShjb21tYW5kVGV4dClzXCIsIHtjb21tYW5kVGV4dH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJZb3UgY2FuIHVzZSA8Y29kZT4vaGVscDwvY29kZT4gdG8gbGlzdCBhdmFpbGFibGUgY29tbWFuZHMuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkRpZCB5b3UgbWVhbiB0byBzZW5kIHRoaXMgYXMgYSBtZXNzYWdlP1wiLCB7fSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IHQgPT4gPGNvZGU+eyB0IH08L2NvZGU+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIkhpbnQ6IEJlZ2luIHlvdXIgbWVzc2FnZSB3aXRoIDxjb2RlPi8vPC9jb2RlPiB0byBzdGFydCBpdCB3aXRoIGEgc2xhc2guXCIsIHt9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogdCA9PiA8Y29kZT57IHQgfTwvY29kZT4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uOiBfdCgnU2VuZCBhcyBtZXNzYWdlJyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IFtzZW5kQW55d2F5XSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgIXNlbmRBbnl3YXkgYmFpbCB0byBsZXQgdGhlIHVzZXIgZWRpdCB0aGUgY29tcG9zZXIgYW5kIHRyeSBhZ2FpblxyXG4gICAgICAgICAgICAgICAgaWYgKCFzZW5kQW55d2F5KSByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzaG91bGRTZW5kKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzUmVwbHkgPSAhIVJvb21WaWV3U3RvcmUuZ2V0UXVvdGluZ0V2ZW50KCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHtyb29tSWR9ID0gdGhpcy5wcm9wcy5yb29tO1xyXG4gICAgICAgICAgICBjb25zdCBjb250ZW50ID0gY3JlYXRlTWVzc2FnZUNvbnRlbnQodGhpcy5tb2RlbCwgdGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yKTtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0LnNlbmRNZXNzYWdlKHJvb21JZCwgY29udGVudCk7XHJcbiAgICAgICAgICAgIGlmIChpc1JlcGx5KSB7XHJcbiAgICAgICAgICAgICAgICAvLyBDbGVhciByZXBseV90b19ldmVudCBhcyB3ZSBwdXQgdGhlIG1lc3NhZ2UgaW50byB0aGUgcXVldWVcclxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBzZW5kIGZhaWxzLCByZXRyeSB3aWxsIGhhbmRsZSByZXNlbmRpbmcuXHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3JlcGx5X3RvX2V2ZW50JyxcclxuICAgICAgICAgICAgICAgICAgICBldmVudDogbnVsbCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNlbmRIaXN0b3J5TWFuYWdlci5zYXZlKHRoaXMubW9kZWwpO1xyXG4gICAgICAgIC8vIGNsZWFyIGNvbXBvc2VyXHJcbiAgICAgICAgdGhpcy5tb2RlbC5yZXNldChbXSk7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmNsZWFyVW5kb0hpc3RvcnkoKTtcclxuICAgICAgICB0aGlzLl9lZGl0b3JSZWYuZm9jdXMoKTtcclxuICAgICAgICB0aGlzLl9jbGVhclN0b3JlZEVkaXRvclN0YXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmdldEVkaXRhYmxlUm9vdE5vZGUoKS5hZGRFdmVudExpc3RlbmVyKFwicGFzdGVcIiwgdGhpcy5fb25QYXN0ZSwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgZGlzLnVucmVnaXN0ZXIodGhpcy5kaXNwYXRjaGVyUmVmKTtcclxuICAgICAgICB0aGlzLl9lZGl0b3JSZWYuZ2V0RWRpdGFibGVSb290Tm9kZSgpLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJwYXN0ZVwiLCB0aGlzLl9vblBhc3RlLCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gTW92ZSB0aGlzIHRvIGNvbnN0cnVjdG9yXHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50KCkgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxyXG4gICAgICAgIGNvbnN0IHBhcnRDcmVhdG9yID0gbmV3IENvbW1hbmRQYXJ0Q3JlYXRvcih0aGlzLnByb3BzLnJvb20sIHRoaXMuY29udGV4dCk7XHJcbiAgICAgICAgY29uc3QgcGFydHMgPSB0aGlzLl9yZXN0b3JlU3RvcmVkRWRpdG9yU3RhdGUocGFydENyZWF0b3IpIHx8IFtdO1xyXG4gICAgICAgIHRoaXMubW9kZWwgPSBuZXcgRWRpdG9yTW9kZWwocGFydHMsIHBhcnRDcmVhdG9yKTtcclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5vbkFjdGlvbik7XHJcbiAgICAgICAgdGhpcy5zZW5kSGlzdG9yeU1hbmFnZXIgPSBuZXcgU2VuZEhpc3RvcnlNYW5hZ2VyKHRoaXMucHJvcHMucm9vbS5yb29tSWQsICdteF9jaWRlcl9jb21wb3Nlcl9oaXN0b3J5XycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBfZWRpdG9yU3RhdGVLZXkoKSB7XHJcbiAgICAgICAgcmV0dXJuIGBjaWRlcl9lZGl0b3Jfc3RhdGVfJHt0aGlzLnByb3BzLnJvb20ucm9vbUlkfWA7XHJcbiAgICB9XHJcblxyXG4gICAgX2NsZWFyU3RvcmVkRWRpdG9yU3RhdGUoKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy5fZWRpdG9yU3RhdGVLZXkpO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZXN0b3JlU3RvcmVkRWRpdG9yU3RhdGUocGFydENyZWF0b3IpIHtcclxuICAgICAgICBjb25zdCBqc29uID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5fZWRpdG9yU3RhdGVLZXkpO1xyXG4gICAgICAgIGlmIChqc29uKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlcmlhbGl6ZWRQYXJ0cyA9IEpTT04ucGFyc2UoanNvbik7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcnRzID0gc2VyaWFsaXplZFBhcnRzLm1hcChwID0+IHBhcnRDcmVhdG9yLmRlc2VyaWFsaXplUGFydChwKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBwYXJ0cztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3NhdmVTdG9yZWRFZGl0b3JTdGF0ZSA9ICgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5tb2RlbC5pc0VtcHR5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NsZWFyU3RvcmVkRWRpdG9yU3RhdGUoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLl9lZGl0b3JTdGF0ZUtleSwgSlNPTi5zdHJpbmdpZnkodGhpcy5tb2RlbC5zZXJpYWxpemVQYXJ0cygpKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQWN0aW9uID0gKHBheWxvYWQpID0+IHtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3JlcGx5X3RvX2V2ZW50JzpcclxuICAgICAgICAgICAgY2FzZSAnZm9jdXNfY29tcG9zZXInOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZWRpdG9yUmVmICYmIHRoaXMuX2VkaXRvclJlZi5mb2N1cygpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2luc2VydF9tZW50aW9uJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX2luc2VydE1lbnRpb24ocGF5bG9hZC51c2VyX2lkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdxdW90ZSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9pbnNlcnRRdW90ZWRNZXNzYWdlKHBheWxvYWQuZXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfaW5zZXJ0TWVudGlvbih1c2VySWQpIHtcclxuICAgICAgICBjb25zdCB7bW9kZWx9ID0gdGhpcztcclxuICAgICAgICBjb25zdCB7cGFydENyZWF0b3J9ID0gbW9kZWw7XHJcbiAgICAgICAgY29uc3QgbWVtYmVyID0gdGhpcy5wcm9wcy5yb29tLmdldE1lbWJlcih1c2VySWQpO1xyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlOYW1lID0gbWVtYmVyID9cclxuICAgICAgICAgICAgbWVtYmVyLnJhd0Rpc3BsYXlOYW1lIDogdXNlcklkO1xyXG4gICAgICAgIGNvbnN0IGNhcmV0ID0gdGhpcy5fZWRpdG9yUmVmLmdldENhcmV0KCk7XHJcbiAgICAgICAgY29uc3QgcG9zaXRpb24gPSBtb2RlbC5wb3NpdGlvbkZvck9mZnNldChjYXJldC5vZmZzZXQsIGNhcmV0LmF0Tm9kZUVuZCk7XHJcbiAgICAgICAgLy8gaW5kZXggaXMgLTEgaWYgdGhlcmUgYXJlIG5vIHBhcnRzIGJ1dCB3ZSBvbmx5IGNhcmUgZm9yIGlmIHRoaXMgd291bGQgYmUgdGhlIHBhcnQgaW4gcG9zaXRpb24gMFxyXG4gICAgICAgIGNvbnN0IGluc2VydEluZGV4ID0gcG9zaXRpb24uaW5kZXggPiAwID8gcG9zaXRpb24uaW5kZXggOiAwO1xyXG4gICAgICAgIGNvbnN0IHBhcnRzID0gcGFydENyZWF0b3IuY3JlYXRlTWVudGlvblBhcnRzKGluc2VydEluZGV4LCBkaXNwbGF5TmFtZSwgdXNlcklkKTtcclxuICAgICAgICBtb2RlbC50cmFuc2Zvcm0oKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBhZGRlZExlbiA9IG1vZGVsLmluc2VydChwYXJ0cywgcG9zaXRpb24pO1xyXG4gICAgICAgICAgICByZXR1cm4gbW9kZWwucG9zaXRpb25Gb3JPZmZzZXQoY2FyZXQub2Zmc2V0ICsgYWRkZWRMZW4sIHRydWUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vIHJlZm9jdXMgb24gY29tcG9zZXIsIGFzIHdlIGp1c3QgY2xpY2tlZCBcIk1lbnRpb25cIlxyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZiAmJiB0aGlzLl9lZGl0b3JSZWYuZm9jdXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBfaW5zZXJ0UXVvdGVkTWVzc2FnZShldmVudCkge1xyXG4gICAgICAgIGNvbnN0IHttb2RlbH0gPSB0aGlzO1xyXG4gICAgICAgIGNvbnN0IHtwYXJ0Q3JlYXRvcn0gPSBtb2RlbDtcclxuICAgICAgICBjb25zdCBxdW90ZVBhcnRzID0gcGFyc2VFdmVudChldmVudCwgcGFydENyZWF0b3IsIHsgaXNRdW90ZWRNZXNzYWdlOiB0cnVlIH0pO1xyXG4gICAgICAgIC8vIGFkZCB0d28gbmV3bGluZXNcclxuICAgICAgICBxdW90ZVBhcnRzLnB1c2gocGFydENyZWF0b3IubmV3bGluZSgpKTtcclxuICAgICAgICBxdW90ZVBhcnRzLnB1c2gocGFydENyZWF0b3IubmV3bGluZSgpKTtcclxuICAgICAgICBtb2RlbC50cmFuc2Zvcm0oKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBhZGRlZExlbiA9IG1vZGVsLmluc2VydChxdW90ZVBhcnRzLCBtb2RlbC5wb3NpdGlvbkZvck9mZnNldCgwKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBtb2RlbC5wb3NpdGlvbkZvck9mZnNldChhZGRlZExlbiwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gcmVmb2N1cyBvbiBjb21wb3NlciwgYXMgd2UganVzdCBjbGlja2VkIFwiUXVvdGVcIlxyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZiAmJiB0aGlzLl9lZGl0b3JSZWYuZm9jdXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25QYXN0ZSA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHtjbGlwYm9hcmREYXRhfSA9IGV2ZW50O1xyXG4gICAgICAgIGlmIChjbGlwYm9hcmREYXRhLmZpbGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAvLyBUaGlzIGFjdHVhbGx5IG5vdCBzbyBtdWNoIGZvciAnZmlsZXMnIGFzIHN1Y2ggKGF0IHRpbWUgb2Ygd3JpdGluZ1xyXG4gICAgICAgICAgICAvLyBuZWl0aGVyIGNocm9tZSBub3IgZmlyZWZveCBsZXQgeW91IHBhc3RlIGEgcGxhaW4gZmlsZSBjb3BpZWRcclxuICAgICAgICAgICAgLy8gZnJvbSBGaW5kZXIpIGJ1dCBtb3JlIGltYWdlcyBjb3BpZWQgZnJvbSBhIGRpZmZlcmVudCB3ZWJzaXRlXHJcbiAgICAgICAgICAgIC8vIC8gd29yZCBwcm9jZXNzb3IgZXRjLlxyXG4gICAgICAgICAgICBDb250ZW50TWVzc2FnZXMuc2hhcmVkSW5zdGFuY2UoKS5zZW5kQ29udGVudExpc3RUb1Jvb20oXHJcbiAgICAgICAgICAgICAgICBBcnJheS5mcm9tKGNsaXBib2FyZERhdGEuZmlsZXMpLCB0aGlzLnByb3BzLnJvb20ucm9vbUlkLCB0aGlzLmNvbnRleHQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1NlbmRNZXNzYWdlQ29tcG9zZXJcIiBvbkNsaWNrPXt0aGlzLmZvY3VzQ29tcG9zZXJ9IG9uS2V5RG93bj17dGhpcy5fb25LZXlEb3dufT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU2VuZE1lc3NhZ2VDb21wb3Nlcl9vdmVybGF5V3JhcHBlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxSZXBseVByZXZpZXcgcGVybWFsaW5rQ3JlYXRvcj17dGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8QmFzaWNNZXNzYWdlQ29tcG9zZXJcclxuICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX3NldEVkaXRvclJlZn1cclxuICAgICAgICAgICAgICAgICAgICBtb2RlbD17dGhpcy5tb2RlbH1cclxuICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e3RoaXMucHJvcHMucGxhY2Vob2xkZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3RoaXMucHJvcHMucGxhY2Vob2xkZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX3NhdmVTdG9yZWRFZGl0b3JTdGF0ZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19