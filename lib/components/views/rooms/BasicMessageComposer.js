"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _model = _interopRequireDefault(require("../../../editor/model"));

var _history = _interopRequireDefault(require("../../../editor/history"));

var _caret = require("../../../editor/caret");

var _operations = require("../../../editor/operations");

var _dom = require("../../../editor/dom");

var _Autocomplete = _interopRequireWildcard(require("../rooms/Autocomplete"));

var _parts = require("../../../editor/parts");

var _deserialize = require("../../../editor/deserialize");

var _render = require("../../../editor/render");

var _matrixJsSdk = require("matrix-js-sdk");

var _TypingStore = _interopRequireDefault(require("../../../stores/TypingStore"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _emoticon = _interopRequireDefault(require("emojibase-regex/emoticon"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Keyboard = require("../../../Keyboard");

var _emoji = require("../../../emoji");

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const REGEX_EMOTICON_WHITESPACE = new RegExp('(?:^|\\s)(' + _emoticon.default.source + ')\\s$');
const IS_MAC = navigator.platform.indexOf("Mac") !== -1;

function ctrlShortcutLabel(key) {
  return (IS_MAC ? "⌘" : "Ctrl") + "+" + key;
}

function cloneSelection(selection) {
  return {
    anchorNode: selection.anchorNode,
    anchorOffset: selection.anchorOffset,
    focusNode: selection.focusNode,
    focusOffset: selection.focusOffset,
    isCollapsed: selection.isCollapsed,
    rangeCount: selection.rangeCount,
    type: selection.type
  };
}

function selectionEquals(a
/*: Selection*/
, b
/*: Selection*/
)
/*: boolean*/
{
  return a.anchorNode === b.anchorNode && a.anchorOffset === b.anchorOffset && a.focusNode === b.focusNode && a.focusOffset === b.focusOffset && a.isCollapsed === b.isCollapsed && a.rangeCount === b.rangeCount && a.type === b.type;
}

class BasicMessageEditor extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_replaceEmoticon", (caretPosition, inputType, diff) => {
      const {
        model
      } = this.props;
      const range = model.startRange(caretPosition); // expand range max 8 characters backwards from caretPosition,
      // as a space to look for an emoticon

      let n = 8;
      range.expandBackwardsWhile((index, offset) => {
        const part = model.parts[index];
        n -= 1;
        return n >= 0 && (part.type === "plain" || part.type === "pill-candidate");
      });
      const emoticonMatch = REGEX_EMOTICON_WHITESPACE.exec(range.text);

      if (emoticonMatch) {
        const query = emoticonMatch[1].replace("-", ""); // try both exact match and lower-case, this means that xd won't match xD but :P will match :p

        const data = _emoji.EMOTICON_TO_EMOJI.get(query) || _emoji.EMOTICON_TO_EMOJI.get(query.toLowerCase());

        if (data) {
          const {
            partCreator
          } = model;
          const hasPrecedingSpace = emoticonMatch[0][0] === " "; // we need the range to only comprise of the emoticon
          // because we'll replace the whole range with an emoji,
          // so move the start forward to the start of the emoticon.
          // Take + 1 because index is reported without the possible preceding space.

          range.moveStart(emoticonMatch.index + (hasPrecedingSpace ? 1 : 0)); // this returns the amount of added/removed characters during the replace
          // so the caret position can be adjusted.

          return range.replace([partCreator.plain(data.unicode + " ")]);
        }
      }
    });
    (0, _defineProperty2.default)(this, "_updateEditorState", (selection, inputType, diff) => {
      (0, _render.renderModel)(this._editorRef, this.props.model);

      if (selection) {
        // set the caret/selection
        try {
          (0, _caret.setSelection)(this._editorRef, this.props.model, selection);
        } catch (err) {
          console.error(err);
        } // if caret selection is a range, take the end position


        const position = selection.end || selection;

        this._setLastCaretFromPosition(position);
      }

      const {
        isEmpty
      } = this.props.model;

      if (this.props.placeholder) {
        if (isEmpty) {
          this._showPlaceholder();
        } else {
          this._hidePlaceholder();
        }
      }

      if (isEmpty) {
        this._formatBarRef.hide();
      }

      this.setState({
        autoComplete: this.props.model.autoComplete
      });
      this.historyManager.tryPush(this.props.model, selection, inputType, diff);

      _TypingStore.default.sharedInstance().setSelfTyping(this.props.room.roomId, !this.props.model.isEmpty);

      if (this.props.onChange) {
        this.props.onChange();
      }
    });
    (0, _defineProperty2.default)(this, "_onCompositionStart", event => {
      this._isIMEComposing = true; // even if the model is empty, the composition text shouldn't be mixed with the placeholder

      this._hidePlaceholder();
    });
    (0, _defineProperty2.default)(this, "_onCompositionEnd", event => {
      this._isIMEComposing = false; // some browsers (Chrome) don't fire an input event after ending a composition,
      // so trigger a model update after the composition is done by calling the input handler.
      // however, modifying the DOM (caused by the editor model update) from the compositionend handler seems
      // to confuse the IME in Chrome, likely causing https://github.com/vector-im/riot-web/issues/10913 ,
      // so we do it async
      // however, doing this async seems to break things in Safari for some reason, so browser sniff.

      const ua = navigator.userAgent.toLowerCase();
      const isSafari = ua.includes('safari/') && !ua.includes('chrome/');

      if (isSafari) {
        this._onInput({
          inputType: "insertCompositionText"
        });
      } else {
        Promise.resolve().then(() => {
          this._onInput({
            inputType: "insertCompositionText"
          });
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onCutCopy", (event, type) => {
      const selection = document.getSelection();
      const text = selection.toString();

      if (text) {
        const {
          model
        } = this.props;
        const range = (0, _dom.getRangeForSelection)(this._editorRef, model, selection);
        const selectedParts = range.parts.map(p => p.serialize());
        event.clipboardData.setData("application/x-riot-composer", JSON.stringify(selectedParts));
        event.clipboardData.setData("text/plain", text); // so plain copy/paste works

        if (type === "cut") {
          // Remove the text, updating the model as appropriate
          this._modifiedFlag = true;
          (0, _operations.replaceRangeAndMoveCaret)(range, []);
        }

        event.preventDefault();
      }
    });
    (0, _defineProperty2.default)(this, "_onCopy", event => {
      this._onCutCopy(event, "copy");
    });
    (0, _defineProperty2.default)(this, "_onCut", event => {
      this._onCutCopy(event, "cut");
    });
    (0, _defineProperty2.default)(this, "_onPaste", event => {
      const {
        model
      } = this.props;
      const {
        partCreator
      } = model;
      const partsText = event.clipboardData.getData("application/x-riot-composer");
      let parts;

      if (partsText) {
        const serializedTextParts = JSON.parse(partsText);
        const deserializedParts = serializedTextParts.map(p => partCreator.deserializePart(p));
        parts = deserializedParts;
      } else {
        const text = event.clipboardData.getData("text/plain");
        parts = (0, _deserialize.parsePlainTextMessage)(text, partCreator);
      }

      this._modifiedFlag = true;
      const range = (0, _dom.getRangeForSelection)(this._editorRef, model, document.getSelection());
      (0, _operations.replaceRangeAndMoveCaret)(range, parts);
      event.preventDefault();
    });
    (0, _defineProperty2.default)(this, "_onInput", event => {
      // ignore any input while doing IME compositions
      if (this._isIMEComposing) {
        return;
      }

      this._modifiedFlag = true;
      const sel = document.getSelection();
      const {
        caret,
        text
      } = (0, _dom.getCaretOffsetAndText)(this._editorRef, sel);
      this.props.model.update(text, event.inputType, caret);
    });
    (0, _defineProperty2.default)(this, "_onBlur", () => {
      document.removeEventListener("selectionchange", this._onSelectionChange);
    });
    (0, _defineProperty2.default)(this, "_onFocus", () => {
      document.addEventListener("selectionchange", this._onSelectionChange); // force to recalculate

      this._lastSelection = null;

      this._refreshLastCaretIfNeeded();
    });
    (0, _defineProperty2.default)(this, "_onSelectionChange", () => {
      this._refreshLastCaretIfNeeded();

      const selection = document.getSelection();

      if (this._hasTextSelected && selection.isCollapsed) {
        this._hasTextSelected = false;

        if (this._formatBarRef) {
          this._formatBarRef.hide();
        }
      } else if (!selection.isCollapsed) {
        this._hasTextSelected = true;

        if (this._formatBarRef) {
          const selectionRect = selection.getRangeAt(0).getBoundingClientRect();

          this._formatBarRef.showAt(selectionRect);
        }
      }
    });
    (0, _defineProperty2.default)(this, "_onKeyDown", event => {
      const model = this.props.model;
      const modKey = IS_MAC ? event.metaKey : event.ctrlKey;
      let handled = false; // format bold

      if (modKey && event.key === _Keyboard.Key.B) {
        this._onFormatAction("bold");

        handled = true; // format italics
      } else if (modKey && event.key === _Keyboard.Key.I) {
        this._onFormatAction("italics");

        handled = true; // format quote
      } else if (modKey && event.key === _Keyboard.Key.GREATER_THAN) {
        this._onFormatAction("quote");

        handled = true; // redo
      } else if (!IS_MAC && modKey && event.key === _Keyboard.Key.Y || IS_MAC && modKey && event.shiftKey && event.key === _Keyboard.Key.Z) {
        if (this.historyManager.canRedo()) {
          const {
            parts,
            caret
          } = this.historyManager.redo(); // pass matching inputType so historyManager doesn't push echo
          // when invoked from rerender callback.

          model.reset(parts, caret, "historyRedo");
        }

        handled = true; // undo
      } else if (modKey && event.key === _Keyboard.Key.Z) {
        if (this.historyManager.canUndo()) {
          const {
            parts,
            caret
          } = this.historyManager.undo(this.props.model); // pass matching inputType so historyManager doesn't push echo
          // when invoked from rerender callback.

          model.reset(parts, caret, "historyUndo");
        }

        handled = true; // insert newline on Shift+Enter
      } else if (event.key === _Keyboard.Key.ENTER && (event.shiftKey || IS_MAC && event.altKey)) {
        this._insertText("\n");

        handled = true; // move selection to start of composer
      } else if (modKey && event.key === _Keyboard.Key.HOME && !event.shiftKey) {
        (0, _caret.setSelection)(this._editorRef, model, {
          index: 0,
          offset: 0
        });
        handled = true; // move selection to end of composer
      } else if (modKey && event.key === _Keyboard.Key.END && !event.shiftKey) {
        (0, _caret.setSelection)(this._editorRef, model, {
          index: model.parts.length - 1,
          offset: model.parts[model.parts.length - 1].text.length
        });
        handled = true; // autocomplete or enter to send below shouldn't have any modifier keys pressed.
      } else {
        const metaOrAltPressed = event.metaKey || event.altKey;
        const modifierPressed = metaOrAltPressed || event.shiftKey;

        if (model.autoComplete && model.autoComplete.hasCompletions()) {
          const autoComplete = model.autoComplete;

          switch (event.key) {
            case _Keyboard.Key.ARROW_UP:
              if (!modifierPressed) {
                autoComplete.onUpArrow(event);
                handled = true;
              }

              break;

            case _Keyboard.Key.ARROW_DOWN:
              if (!modifierPressed) {
                autoComplete.onDownArrow(event);
                handled = true;
              }

              break;

            case _Keyboard.Key.TAB:
              if (!metaOrAltPressed) {
                autoComplete.onTab(event);
                handled = true;
              }

              break;

            case _Keyboard.Key.ESCAPE:
              if (!modifierPressed) {
                autoComplete.onEscape(event);
                handled = true;
              }

              break;

            default:
              return;
            // don't preventDefault on anything else
          }
        } else if (event.key === _Keyboard.Key.TAB) {
          this._tabCompleteName();

          handled = true;
        } else if (event.key === _Keyboard.Key.BACKSPACE || event.key === _Keyboard.Key.DELETE) {
          this._formatBarRef.hide();
        }
      }

      if (handled) {
        event.preventDefault();
        event.stopPropagation();
      }
    });
    (0, _defineProperty2.default)(this, "_onAutoCompleteConfirm", completion => {
      this.props.model.autoComplete.onComponentConfirm(completion);
    });
    (0, _defineProperty2.default)(this, "_onAutoCompleteSelectionChange", (completion, completionIndex) => {
      this.props.model.autoComplete.onComponentSelectionChange(completion);
      this.setState({
        completionIndex
      });
    });
    (0, _defineProperty2.default)(this, "_onFormatAction", action => {
      const range = (0, _dom.getRangeForSelection)(this._editorRef, this.props.model, document.getSelection());

      if (range.length === 0) {
        return;
      }

      this.historyManager.ensureLastChangesPushed(this.props.model);
      this._modifiedFlag = true;

      switch (action) {
        case "bold":
          (0, _operations.toggleInlineFormat)(range, "**");
          break;

        case "italics":
          (0, _operations.toggleInlineFormat)(range, "_");
          break;

        case "strikethrough":
          (0, _operations.toggleInlineFormat)(range, "<del>", "</del>");
          break;

        case "code":
          (0, _operations.formatRangeAsCode)(range);
          break;

        case "quote":
          (0, _operations.formatRangeAsQuote)(range);
          break;
      }
    });
    this.state = {
      autoComplete: null
    };
    this._editorRef = null;
    this._autocompleteRef = null;
    this._formatBarRef = null;
    this._modifiedFlag = false;
    this._isIMEComposing = false;
    this._hasTextSelected = false;
    this._emoticonSettingHandle = null;
  } // TODO: [REACT-WARNING] Move into better lifecycle position


  UNSAFE_componentWillUpdate(prevProps) {
    // eslint-disable-line camelcase
    if (this.props.placeholder !== prevProps.placeholder && this.props.placeholder) {
      const {
        isEmpty
      } = this.props.model;

      if (isEmpty) {
        this._showPlaceholder();
      } else {
        this._hidePlaceholder();
      }
    }
  }

  _showPlaceholder() {
    this._editorRef.style.setProperty("--placeholder", "'".concat(this.props.placeholder, "'"));

    this._editorRef.classList.add("mx_BasicMessageComposer_inputEmpty");
  }

  _hidePlaceholder() {
    this._editorRef.classList.remove("mx_BasicMessageComposer_inputEmpty");

    this._editorRef.style.removeProperty("--placeholder");
  }

  isComposing(event) {
    // checking the event.isComposing flag just in case any browser out there
    // emits events related to the composition after compositionend
    // has been fired
    return !!(this._isIMEComposing || event.nativeEvent && event.nativeEvent.isComposing);
  }

  _insertText(textToInsert, inputType = "insertText") {
    const sel = document.getSelection();
    const {
      caret,
      text
    } = (0, _dom.getCaretOffsetAndText)(this._editorRef, sel);
    const newText = text.substr(0, caret.offset) + textToInsert + text.substr(caret.offset);
    caret.offset += textToInsert.length;
    this._modifiedFlag = true;
    this.props.model.update(newText, inputType, caret);
  } // this is used later to see if we need to recalculate the caret
  // on selectionchange. If it is just a consequence of typing
  // we don't need to. But if the user is navigating the caret without input
  // we need to recalculate it, to be able to know where to insert content after
  // losing focus


  _setLastCaretFromPosition(position) {
    const {
      model
    } = this.props;
    this._isCaretAtEnd = position.isAtEnd(model);
    this._lastCaret = position.asOffset(model);
    this._lastSelection = cloneSelection(document.getSelection());
  }

  _refreshLastCaretIfNeeded() {
    // XXX: needed when going up and down in editing messages ... not sure why yet
    // because the editors should stop doing this when when blurred ...
    // maybe it's on focus and the _editorRef isn't available yet or something.
    if (!this._editorRef) {
      return;
    }

    const selection = document.getSelection();

    if (!this._lastSelection || !selectionEquals(this._lastSelection, selection)) {
      this._lastSelection = cloneSelection(selection);
      const {
        caret,
        text
      } = (0, _dom.getCaretOffsetAndText)(this._editorRef, selection);
      this._lastCaret = caret;
      this._isCaretAtEnd = caret.offset === text.length;
    }

    return this._lastCaret;
  }

  clearUndoHistory() {
    this.historyManager.clear();
  }

  getCaret() {
    return this._lastCaret;
  }

  isSelectionCollapsed() {
    return !this._lastSelection || this._lastSelection.isCollapsed;
  }

  isCaretAtStart() {
    return this.getCaret().offset === 0;
  }

  isCaretAtEnd() {
    return this._isCaretAtEnd;
  }

  async _tabCompleteName() {
    try {
      await new Promise(resolve => this.setState({
        showVisualBell: false
      }, resolve));
      const {
        model
      } = this.props;
      const caret = this.getCaret();
      const position = model.positionForOffset(caret.offset, caret.atNodeEnd);
      const range = model.startRange(position);
      range.expandBackwardsWhile((index, offset, part) => {
        return part.text[offset] !== " " && (part.type === "plain" || part.type === "pill-candidate" || part.type === "command");
      });
      const {
        partCreator
      } = model; // await for auto-complete to be open

      await model.transform(() => {
        const addedLen = range.replace([partCreator.pillCandidate(range.text)]);
        return model.positionForOffset(caret.offset + addedLen, true);
      }); // Don't try to do things with the autocomplete if there is none shown

      if (model.autoComplete) {
        await model.autoComplete.onTab();

        if (!model.autoComplete.hasSelection()) {
          this.setState({
            showVisualBell: true
          });
          model.autoComplete.close();
        }
      }
    } catch (err) {
      console.error(err);
    }
  }

  getEditableRootNode() {
    return this._editorRef;
  }

  isModified() {
    return this._modifiedFlag;
  }

  _configureEmoticonAutoReplace() {
    const shouldReplace = _SettingsStore.default.getValue('MessageComposerInput.autoReplaceEmoji');

    this.props.model.setTransformCallback(shouldReplace ? this._replaceEmoticon : null);
  }

  componentWillUnmount() {
    document.removeEventListener("selectionchange", this._onSelectionChange);

    this._editorRef.removeEventListener("input", this._onInput, true);

    this._editorRef.removeEventListener("compositionstart", this._onCompositionStart, true);

    this._editorRef.removeEventListener("compositionend", this._onCompositionEnd, true);

    _SettingsStore.default.unwatchSetting(this._emoticonSettingHandle);
  }

  componentDidMount() {
    const model = this.props.model;
    model.setUpdateCallback(this._updateEditorState);
    this._emoticonSettingHandle = _SettingsStore.default.watchSetting('MessageComposerInput.autoReplaceEmoji', null, () => {
      this._configureEmoticonAutoReplace();
    });

    this._configureEmoticonAutoReplace();

    const partCreator = model.partCreator; // TODO: does this allow us to get rid of EditorStateTransfer?
    // not really, but we could not serialize the parts, and just change the autoCompleter

    partCreator.setAutoCompleteCreator((0, _parts.autoCompleteCreator)(() => this._autocompleteRef, query => new Promise(resolve => this.setState({
      query
    }, resolve))));
    this.historyManager = new _history.default(partCreator); // initial render of model

    this._updateEditorState(this._getInitialCaretPosition()); // attach input listener by hand so React doesn't proxy the events,
    // as the proxied event doesn't support inputType, which we need.


    this._editorRef.addEventListener("input", this._onInput, true);

    this._editorRef.addEventListener("compositionstart", this._onCompositionStart, true);

    this._editorRef.addEventListener("compositionend", this._onCompositionEnd, true);

    this._editorRef.focus();
  }

  _getInitialCaretPosition() {
    let caretPosition;

    if (this.props.initialCaret) {
      // if restoring state from a previous editor,
      // restore caret position from the state
      const caret = this.props.initialCaret;
      caretPosition = this.props.model.positionForOffset(caret.offset, caret.atNodeEnd);
    } else {
      // otherwise, set it at the end
      caretPosition = this.props.model.getPositionAtEnd();
    }

    return caretPosition;
  }

  render() {
    let autoComplete;

    if (this.state.autoComplete) {
      const query = this.state.query;
      const queryLen = query.length;
      autoComplete = _react.default.createElement("div", {
        className: "mx_BasicMessageComposer_AutoCompleteWrapper"
      }, _react.default.createElement(_Autocomplete.default, {
        ref: ref => this._autocompleteRef = ref,
        query: query,
        onConfirm: this._onAutoCompleteConfirm,
        onSelectionChange: this._onAutoCompleteSelectionChange,
        selection: {
          beginning: true,
          end: queryLen,
          start: queryLen
        },
        room: this.props.room
      }));
    }

    const classes = (0, _classnames.default)("mx_BasicMessageComposer", {
      "mx_BasicMessageComposer_input_error": this.state.showVisualBell
    });
    const MessageComposerFormatBar = sdk.getComponent('rooms.MessageComposerFormatBar');
    const shortcuts = {
      bold: ctrlShortcutLabel("B"),
      italics: ctrlShortcutLabel("I"),
      quote: ctrlShortcutLabel(">")
    };
    const {
      completionIndex
    } = this.state;
    return _react.default.createElement("div", {
      className: classes
    }, autoComplete, _react.default.createElement(MessageComposerFormatBar, {
      ref: ref => this._formatBarRef = ref,
      onAction: this._onFormatAction,
      shortcuts: shortcuts
    }), _react.default.createElement("div", {
      className: "mx_BasicMessageComposer_input",
      contentEditable: "true",
      tabIndex: "0",
      onBlur: this._onBlur,
      onFocus: this._onFocus,
      onCopy: this._onCopy,
      onCut: this._onCut,
      onPaste: this._onPaste,
      onKeyDown: this._onKeyDown,
      ref: ref => this._editorRef = ref,
      "aria-label": this.props.label,
      role: "textbox",
      "aria-multiline": "true",
      "aria-autocomplete": "both",
      "aria-haspopup": "listbox",
      "aria-expanded": Boolean(this.state.autoComplete),
      "aria-activedescendant": completionIndex >= 0 ? (0, _Autocomplete.generateCompletionDomId)(completionIndex) : undefined,
      dir: "auto"
    }));
  }

  focus() {
    this._editorRef.focus();
  }

}

exports.default = BasicMessageEditor;
(0, _defineProperty2.default)(BasicMessageEditor, "propTypes", {
  onChange: _propTypes.default.func,
  model: _propTypes.default.instanceOf(_model.default).isRequired,
  room: _propTypes.default.instanceOf(_matrixJsSdk.Room).isRequired,
  placeholder: _propTypes.default.string,
  label: _propTypes.default.string,
  // the aria label
  initialCaret: _propTypes.default.object // See DocumentPosition in editor/model.js

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL0Jhc2ljTWVzc2FnZUNvbXBvc2VyLmpzIl0sIm5hbWVzIjpbIlJFR0VYX0VNT1RJQ09OX1dISVRFU1BBQ0UiLCJSZWdFeHAiLCJFTU9USUNPTl9SRUdFWCIsInNvdXJjZSIsIklTX01BQyIsIm5hdmlnYXRvciIsInBsYXRmb3JtIiwiaW5kZXhPZiIsImN0cmxTaG9ydGN1dExhYmVsIiwia2V5IiwiY2xvbmVTZWxlY3Rpb24iLCJzZWxlY3Rpb24iLCJhbmNob3JOb2RlIiwiYW5jaG9yT2Zmc2V0IiwiZm9jdXNOb2RlIiwiZm9jdXNPZmZzZXQiLCJpc0NvbGxhcHNlZCIsInJhbmdlQ291bnQiLCJ0eXBlIiwic2VsZWN0aW9uRXF1YWxzIiwiYSIsImIiLCJCYXNpY01lc3NhZ2VFZGl0b3IiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJjYXJldFBvc2l0aW9uIiwiaW5wdXRUeXBlIiwiZGlmZiIsIm1vZGVsIiwicmFuZ2UiLCJzdGFydFJhbmdlIiwibiIsImV4cGFuZEJhY2t3YXJkc1doaWxlIiwiaW5kZXgiLCJvZmZzZXQiLCJwYXJ0IiwicGFydHMiLCJlbW90aWNvbk1hdGNoIiwiZXhlYyIsInRleHQiLCJxdWVyeSIsInJlcGxhY2UiLCJkYXRhIiwiRU1PVElDT05fVE9fRU1PSkkiLCJnZXQiLCJ0b0xvd2VyQ2FzZSIsInBhcnRDcmVhdG9yIiwiaGFzUHJlY2VkaW5nU3BhY2UiLCJtb3ZlU3RhcnQiLCJwbGFpbiIsInVuaWNvZGUiLCJfZWRpdG9yUmVmIiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwicG9zaXRpb24iLCJlbmQiLCJfc2V0TGFzdENhcmV0RnJvbVBvc2l0aW9uIiwiaXNFbXB0eSIsInBsYWNlaG9sZGVyIiwiX3Nob3dQbGFjZWhvbGRlciIsIl9oaWRlUGxhY2Vob2xkZXIiLCJfZm9ybWF0QmFyUmVmIiwiaGlkZSIsInNldFN0YXRlIiwiYXV0b0NvbXBsZXRlIiwiaGlzdG9yeU1hbmFnZXIiLCJ0cnlQdXNoIiwiVHlwaW5nU3RvcmUiLCJzaGFyZWRJbnN0YW5jZSIsInNldFNlbGZUeXBpbmciLCJyb29tIiwicm9vbUlkIiwib25DaGFuZ2UiLCJldmVudCIsIl9pc0lNRUNvbXBvc2luZyIsInVhIiwidXNlckFnZW50IiwiaXNTYWZhcmkiLCJpbmNsdWRlcyIsIl9vbklucHV0IiwiUHJvbWlzZSIsInJlc29sdmUiLCJ0aGVuIiwiZG9jdW1lbnQiLCJnZXRTZWxlY3Rpb24iLCJ0b1N0cmluZyIsInNlbGVjdGVkUGFydHMiLCJtYXAiLCJwIiwic2VyaWFsaXplIiwiY2xpcGJvYXJkRGF0YSIsInNldERhdGEiLCJKU09OIiwic3RyaW5naWZ5IiwiX21vZGlmaWVkRmxhZyIsInByZXZlbnREZWZhdWx0IiwiX29uQ3V0Q29weSIsInBhcnRzVGV4dCIsImdldERhdGEiLCJzZXJpYWxpemVkVGV4dFBhcnRzIiwicGFyc2UiLCJkZXNlcmlhbGl6ZWRQYXJ0cyIsImRlc2VyaWFsaXplUGFydCIsInNlbCIsImNhcmV0IiwidXBkYXRlIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsIl9vblNlbGVjdGlvbkNoYW5nZSIsImFkZEV2ZW50TGlzdGVuZXIiLCJfbGFzdFNlbGVjdGlvbiIsIl9yZWZyZXNoTGFzdENhcmV0SWZOZWVkZWQiLCJfaGFzVGV4dFNlbGVjdGVkIiwic2VsZWN0aW9uUmVjdCIsImdldFJhbmdlQXQiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJzaG93QXQiLCJtb2RLZXkiLCJtZXRhS2V5IiwiY3RybEtleSIsImhhbmRsZWQiLCJLZXkiLCJCIiwiX29uRm9ybWF0QWN0aW9uIiwiSSIsIkdSRUFURVJfVEhBTiIsIlkiLCJzaGlmdEtleSIsIloiLCJjYW5SZWRvIiwicmVkbyIsInJlc2V0IiwiY2FuVW5kbyIsInVuZG8iLCJFTlRFUiIsImFsdEtleSIsIl9pbnNlcnRUZXh0IiwiSE9NRSIsIkVORCIsImxlbmd0aCIsIm1ldGFPckFsdFByZXNzZWQiLCJtb2RpZmllclByZXNzZWQiLCJoYXNDb21wbGV0aW9ucyIsIkFSUk9XX1VQIiwib25VcEFycm93IiwiQVJST1dfRE9XTiIsIm9uRG93bkFycm93IiwiVEFCIiwib25UYWIiLCJFU0NBUEUiLCJvbkVzY2FwZSIsIl90YWJDb21wbGV0ZU5hbWUiLCJCQUNLU1BBQ0UiLCJERUxFVEUiLCJzdG9wUHJvcGFnYXRpb24iLCJjb21wbGV0aW9uIiwib25Db21wb25lbnRDb25maXJtIiwiY29tcGxldGlvbkluZGV4Iiwib25Db21wb25lbnRTZWxlY3Rpb25DaGFuZ2UiLCJhY3Rpb24iLCJlbnN1cmVMYXN0Q2hhbmdlc1B1c2hlZCIsInN0YXRlIiwiX2F1dG9jb21wbGV0ZVJlZiIsIl9lbW90aWNvblNldHRpbmdIYW5kbGUiLCJVTlNBRkVfY29tcG9uZW50V2lsbFVwZGF0ZSIsInByZXZQcm9wcyIsInN0eWxlIiwic2V0UHJvcGVydHkiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJyZW1vdmVQcm9wZXJ0eSIsImlzQ29tcG9zaW5nIiwibmF0aXZlRXZlbnQiLCJ0ZXh0VG9JbnNlcnQiLCJuZXdUZXh0Iiwic3Vic3RyIiwiX2lzQ2FyZXRBdEVuZCIsImlzQXRFbmQiLCJfbGFzdENhcmV0IiwiYXNPZmZzZXQiLCJjbGVhclVuZG9IaXN0b3J5IiwiY2xlYXIiLCJnZXRDYXJldCIsImlzU2VsZWN0aW9uQ29sbGFwc2VkIiwiaXNDYXJldEF0U3RhcnQiLCJpc0NhcmV0QXRFbmQiLCJzaG93VmlzdWFsQmVsbCIsInBvc2l0aW9uRm9yT2Zmc2V0IiwiYXROb2RlRW5kIiwidHJhbnNmb3JtIiwiYWRkZWRMZW4iLCJwaWxsQ2FuZGlkYXRlIiwiaGFzU2VsZWN0aW9uIiwiY2xvc2UiLCJnZXRFZGl0YWJsZVJvb3ROb2RlIiwiaXNNb2RpZmllZCIsIl9jb25maWd1cmVFbW90aWNvbkF1dG9SZXBsYWNlIiwic2hvdWxkUmVwbGFjZSIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsInNldFRyYW5zZm9ybUNhbGxiYWNrIiwiX3JlcGxhY2VFbW90aWNvbiIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwiX29uQ29tcG9zaXRpb25TdGFydCIsIl9vbkNvbXBvc2l0aW9uRW5kIiwidW53YXRjaFNldHRpbmciLCJjb21wb25lbnREaWRNb3VudCIsInNldFVwZGF0ZUNhbGxiYWNrIiwiX3VwZGF0ZUVkaXRvclN0YXRlIiwid2F0Y2hTZXR0aW5nIiwic2V0QXV0b0NvbXBsZXRlQ3JlYXRvciIsIkhpc3RvcnlNYW5hZ2VyIiwiX2dldEluaXRpYWxDYXJldFBvc2l0aW9uIiwiZm9jdXMiLCJpbml0aWFsQ2FyZXQiLCJnZXRQb3NpdGlvbkF0RW5kIiwicmVuZGVyIiwicXVlcnlMZW4iLCJyZWYiLCJfb25BdXRvQ29tcGxldGVDb25maXJtIiwiX29uQXV0b0NvbXBsZXRlU2VsZWN0aW9uQ2hhbmdlIiwiYmVnaW5uaW5nIiwic3RhcnQiLCJjbGFzc2VzIiwiTWVzc2FnZUNvbXBvc2VyRm9ybWF0QmFyIiwic2RrIiwiZ2V0Q29tcG9uZW50Iiwic2hvcnRjdXRzIiwiYm9sZCIsIml0YWxpY3MiLCJxdW90ZSIsIl9vbkJsdXIiLCJfb25Gb2N1cyIsIl9vbkNvcHkiLCJfb25DdXQiLCJfb25QYXN0ZSIsIl9vbktleURvd24iLCJsYWJlbCIsIkJvb2xlYW4iLCJ1bmRlZmluZWQiLCJQcm9wVHlwZXMiLCJmdW5jIiwiaW5zdGFuY2VPZiIsIkVkaXRvck1vZGVsIiwiaXNSZXF1aXJlZCIsIlJvb20iLCJzdHJpbmciLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBTUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBeENBOzs7Ozs7Ozs7Ozs7Ozs7O0FBMENBLE1BQU1BLHlCQUF5QixHQUFHLElBQUlDLE1BQUosQ0FBVyxlQUFlQyxrQkFBZUMsTUFBOUIsR0FBdUMsT0FBbEQsQ0FBbEM7QUFFQSxNQUFNQyxNQUFNLEdBQUdDLFNBQVMsQ0FBQ0MsUUFBVixDQUFtQkMsT0FBbkIsQ0FBMkIsS0FBM0IsTUFBc0MsQ0FBQyxDQUF0RDs7QUFFQSxTQUFTQyxpQkFBVCxDQUEyQkMsR0FBM0IsRUFBZ0M7QUFDNUIsU0FBTyxDQUFDTCxNQUFNLEdBQUcsR0FBSCxHQUFTLE1BQWhCLElBQTBCLEdBQTFCLEdBQWdDSyxHQUF2QztBQUNIOztBQUVELFNBQVNDLGNBQVQsQ0FBd0JDLFNBQXhCLEVBQW1DO0FBQy9CLFNBQU87QUFDSEMsSUFBQUEsVUFBVSxFQUFFRCxTQUFTLENBQUNDLFVBRG5CO0FBRUhDLElBQUFBLFlBQVksRUFBRUYsU0FBUyxDQUFDRSxZQUZyQjtBQUdIQyxJQUFBQSxTQUFTLEVBQUVILFNBQVMsQ0FBQ0csU0FIbEI7QUFJSEMsSUFBQUEsV0FBVyxFQUFFSixTQUFTLENBQUNJLFdBSnBCO0FBS0hDLElBQUFBLFdBQVcsRUFBRUwsU0FBUyxDQUFDSyxXQUxwQjtBQU1IQyxJQUFBQSxVQUFVLEVBQUVOLFNBQVMsQ0FBQ00sVUFObkI7QUFPSEMsSUFBQUEsSUFBSSxFQUFFUCxTQUFTLENBQUNPO0FBUGIsR0FBUDtBQVNIOztBQUVELFNBQVNDLGVBQVQsQ0FBeUJDO0FBQXpCO0FBQUEsRUFBdUNDO0FBQXZDO0FBQUE7QUFBQTtBQUE4RDtBQUMxRCxTQUFPRCxDQUFDLENBQUNSLFVBQUYsS0FBaUJTLENBQUMsQ0FBQ1QsVUFBbkIsSUFDSFEsQ0FBQyxDQUFDUCxZQUFGLEtBQW1CUSxDQUFDLENBQUNSLFlBRGxCLElBRUhPLENBQUMsQ0FBQ04sU0FBRixLQUFnQk8sQ0FBQyxDQUFDUCxTQUZmLElBR0hNLENBQUMsQ0FBQ0wsV0FBRixLQUFrQk0sQ0FBQyxDQUFDTixXQUhqQixJQUlISyxDQUFDLENBQUNKLFdBQUYsS0FBa0JLLENBQUMsQ0FBQ0wsV0FKakIsSUFLSEksQ0FBQyxDQUFDSCxVQUFGLEtBQWlCSSxDQUFDLENBQUNKLFVBTGhCLElBTUhHLENBQUMsQ0FBQ0YsSUFBRixLQUFXRyxDQUFDLENBQUNILElBTmpCO0FBT0g7O0FBRWMsTUFBTUksa0JBQU4sU0FBaUNDLGVBQU1DLFNBQXZDLENBQWlEO0FBVTVEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFEZSw0REEwQkEsQ0FBQ0MsYUFBRCxFQUFnQkMsU0FBaEIsRUFBMkJDLElBQTNCLEtBQW9DO0FBQ25ELFlBQU07QUFBQ0MsUUFBQUE7QUFBRCxVQUFVLEtBQUtKLEtBQXJCO0FBQ0EsWUFBTUssS0FBSyxHQUFHRCxLQUFLLENBQUNFLFVBQU4sQ0FBaUJMLGFBQWpCLENBQWQsQ0FGbUQsQ0FHbkQ7QUFDQTs7QUFDQSxVQUFJTSxDQUFDLEdBQUcsQ0FBUjtBQUNBRixNQUFBQSxLQUFLLENBQUNHLG9CQUFOLENBQTJCLENBQUNDLEtBQUQsRUFBUUMsTUFBUixLQUFtQjtBQUMxQyxjQUFNQyxJQUFJLEdBQUdQLEtBQUssQ0FBQ1EsS0FBTixDQUFZSCxLQUFaLENBQWI7QUFDQUYsUUFBQUEsQ0FBQyxJQUFJLENBQUw7QUFDQSxlQUFPQSxDQUFDLElBQUksQ0FBTCxLQUFXSSxJQUFJLENBQUNuQixJQUFMLEtBQWMsT0FBZCxJQUF5Qm1CLElBQUksQ0FBQ25CLElBQUwsS0FBYyxnQkFBbEQsQ0FBUDtBQUNILE9BSkQ7QUFLQSxZQUFNcUIsYUFBYSxHQUFHdkMseUJBQXlCLENBQUN3QyxJQUExQixDQUErQlQsS0FBSyxDQUFDVSxJQUFyQyxDQUF0Qjs7QUFDQSxVQUFJRixhQUFKLEVBQW1CO0FBQ2YsY0FBTUcsS0FBSyxHQUFHSCxhQUFhLENBQUMsQ0FBRCxDQUFiLENBQWlCSSxPQUFqQixDQUF5QixHQUF6QixFQUE4QixFQUE5QixDQUFkLENBRGUsQ0FFZjs7QUFDQSxjQUFNQyxJQUFJLEdBQUdDLHlCQUFrQkMsR0FBbEIsQ0FBc0JKLEtBQXRCLEtBQWdDRyx5QkFBa0JDLEdBQWxCLENBQXNCSixLQUFLLENBQUNLLFdBQU4sRUFBdEIsQ0FBN0M7O0FBRUEsWUFBSUgsSUFBSixFQUFVO0FBQ04sZ0JBQU07QUFBQ0ksWUFBQUE7QUFBRCxjQUFnQmxCLEtBQXRCO0FBQ0EsZ0JBQU1tQixpQkFBaUIsR0FBR1YsYUFBYSxDQUFDLENBQUQsQ0FBYixDQUFpQixDQUFqQixNQUF3QixHQUFsRCxDQUZNLENBR047QUFDQTtBQUNBO0FBQ0E7O0FBQ0FSLFVBQUFBLEtBQUssQ0FBQ21CLFNBQU4sQ0FBZ0JYLGFBQWEsQ0FBQ0osS0FBZCxJQUF1QmMsaUJBQWlCLEdBQUcsQ0FBSCxHQUFPLENBQS9DLENBQWhCLEVBUE0sQ0FRTjtBQUNBOztBQUNBLGlCQUFPbEIsS0FBSyxDQUFDWSxPQUFOLENBQWMsQ0FBQ0ssV0FBVyxDQUFDRyxLQUFaLENBQWtCUCxJQUFJLENBQUNRLE9BQUwsR0FBZSxHQUFqQyxDQUFELENBQWQsQ0FBUDtBQUNIO0FBQ0o7QUFDSixLQXhEa0I7QUFBQSw4REEwREUsQ0FBQ3pDLFNBQUQsRUFBWWlCLFNBQVosRUFBdUJDLElBQXZCLEtBQWdDO0FBQ2pELCtCQUFZLEtBQUt3QixVQUFqQixFQUE2QixLQUFLM0IsS0FBTCxDQUFXSSxLQUF4Qzs7QUFDQSxVQUFJbkIsU0FBSixFQUFlO0FBQUU7QUFDYixZQUFJO0FBQ0EsbUNBQWEsS0FBSzBDLFVBQWxCLEVBQThCLEtBQUszQixLQUFMLENBQVdJLEtBQXpDLEVBQWdEbkIsU0FBaEQ7QUFDSCxTQUZELENBRUUsT0FBTzJDLEdBQVAsRUFBWTtBQUNWQyxVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBZDtBQUNILFNBTFUsQ0FNWDs7O0FBQ0EsY0FBTUcsUUFBUSxHQUFHOUMsU0FBUyxDQUFDK0MsR0FBVixJQUFpQi9DLFNBQWxDOztBQUNBLGFBQUtnRCx5QkFBTCxDQUErQkYsUUFBL0I7QUFDSDs7QUFDRCxZQUFNO0FBQUNHLFFBQUFBO0FBQUQsVUFBWSxLQUFLbEMsS0FBTCxDQUFXSSxLQUE3Qjs7QUFDQSxVQUFJLEtBQUtKLEtBQUwsQ0FBV21DLFdBQWYsRUFBNEI7QUFDeEIsWUFBSUQsT0FBSixFQUFhO0FBQ1QsZUFBS0UsZ0JBQUw7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLQyxnQkFBTDtBQUNIO0FBQ0o7O0FBQ0QsVUFBSUgsT0FBSixFQUFhO0FBQ1QsYUFBS0ksYUFBTCxDQUFtQkMsSUFBbkI7QUFDSDs7QUFDRCxXQUFLQyxRQUFMLENBQWM7QUFBQ0MsUUFBQUEsWUFBWSxFQUFFLEtBQUt6QyxLQUFMLENBQVdJLEtBQVgsQ0FBaUJxQztBQUFoQyxPQUFkO0FBQ0EsV0FBS0MsY0FBTCxDQUFvQkMsT0FBcEIsQ0FBNEIsS0FBSzNDLEtBQUwsQ0FBV0ksS0FBdkMsRUFBOENuQixTQUE5QyxFQUF5RGlCLFNBQXpELEVBQW9FQyxJQUFwRTs7QUFDQXlDLDJCQUFZQyxjQUFaLEdBQTZCQyxhQUE3QixDQUEyQyxLQUFLOUMsS0FBTCxDQUFXK0MsSUFBWCxDQUFnQkMsTUFBM0QsRUFBbUUsQ0FBQyxLQUFLaEQsS0FBTCxDQUFXSSxLQUFYLENBQWlCOEIsT0FBckY7O0FBRUEsVUFBSSxLQUFLbEMsS0FBTCxDQUFXaUQsUUFBZixFQUF5QjtBQUNyQixhQUFLakQsS0FBTCxDQUFXaUQsUUFBWDtBQUNIO0FBQ0osS0F4RmtCO0FBQUEsK0RBb0dJQyxLQUFELElBQVc7QUFDN0IsV0FBS0MsZUFBTCxHQUF1QixJQUF2QixDQUQ2QixDQUU3Qjs7QUFDQSxXQUFLZCxnQkFBTDtBQUNILEtBeEdrQjtBQUFBLDZEQTBHRWEsS0FBRCxJQUFXO0FBQzNCLFdBQUtDLGVBQUwsR0FBdUIsS0FBdkIsQ0FEMkIsQ0FFM0I7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBOztBQUVBLFlBQU1DLEVBQUUsR0FBR3pFLFNBQVMsQ0FBQzBFLFNBQVYsQ0FBb0JoQyxXQUFwQixFQUFYO0FBQ0EsWUFBTWlDLFFBQVEsR0FBR0YsRUFBRSxDQUFDRyxRQUFILENBQVksU0FBWixLQUEwQixDQUFDSCxFQUFFLENBQUNHLFFBQUgsQ0FBWSxTQUFaLENBQTVDOztBQUVBLFVBQUlELFFBQUosRUFBYztBQUNWLGFBQUtFLFFBQUwsQ0FBYztBQUFDdEQsVUFBQUEsU0FBUyxFQUFFO0FBQVosU0FBZDtBQUNILE9BRkQsTUFFTztBQUNIdUQsUUFBQUEsT0FBTyxDQUFDQyxPQUFSLEdBQWtCQyxJQUFsQixDQUF1QixNQUFNO0FBQ3pCLGVBQUtILFFBQUwsQ0FBYztBQUFDdEQsWUFBQUEsU0FBUyxFQUFFO0FBQVosV0FBZDtBQUNILFNBRkQ7QUFHSDtBQUNKLEtBL0hrQjtBQUFBLHNEQXdJTixDQUFDZ0QsS0FBRCxFQUFRMUQsSUFBUixLQUFpQjtBQUMxQixZQUFNUCxTQUFTLEdBQUcyRSxRQUFRLENBQUNDLFlBQVQsRUFBbEI7QUFDQSxZQUFNOUMsSUFBSSxHQUFHOUIsU0FBUyxDQUFDNkUsUUFBVixFQUFiOztBQUNBLFVBQUkvQyxJQUFKLEVBQVU7QUFDTixjQUFNO0FBQUNYLFVBQUFBO0FBQUQsWUFBVSxLQUFLSixLQUFyQjtBQUNBLGNBQU1LLEtBQUssR0FBRywrQkFBcUIsS0FBS3NCLFVBQTFCLEVBQXNDdkIsS0FBdEMsRUFBNkNuQixTQUE3QyxDQUFkO0FBQ0EsY0FBTThFLGFBQWEsR0FBRzFELEtBQUssQ0FBQ08sS0FBTixDQUFZb0QsR0FBWixDQUFnQkMsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLFNBQUYsRUFBckIsQ0FBdEI7QUFDQWhCLFFBQUFBLEtBQUssQ0FBQ2lCLGFBQU4sQ0FBb0JDLE9BQXBCLENBQTRCLDZCQUE1QixFQUEyREMsSUFBSSxDQUFDQyxTQUFMLENBQWVQLGFBQWYsQ0FBM0Q7QUFDQWIsUUFBQUEsS0FBSyxDQUFDaUIsYUFBTixDQUFvQkMsT0FBcEIsQ0FBNEIsWUFBNUIsRUFBMENyRCxJQUExQyxFQUxNLENBSzJDOztBQUNqRCxZQUFJdkIsSUFBSSxLQUFLLEtBQWIsRUFBb0I7QUFDaEI7QUFDQSxlQUFLK0UsYUFBTCxHQUFxQixJQUFyQjtBQUNBLG9EQUF5QmxFLEtBQXpCLEVBQWdDLEVBQWhDO0FBQ0g7O0FBQ0Q2QyxRQUFBQSxLQUFLLENBQUNzQixjQUFOO0FBQ0g7QUFDSixLQXhKa0I7QUFBQSxtREEwSlJ0QixLQUFELElBQVc7QUFDakIsV0FBS3VCLFVBQUwsQ0FBZ0J2QixLQUFoQixFQUF1QixNQUF2QjtBQUNILEtBNUprQjtBQUFBLGtEQThKVEEsS0FBRCxJQUFXO0FBQ2hCLFdBQUt1QixVQUFMLENBQWdCdkIsS0FBaEIsRUFBdUIsS0FBdkI7QUFDSCxLQWhLa0I7QUFBQSxvREFrS1BBLEtBQUQsSUFBVztBQUNsQixZQUFNO0FBQUM5QyxRQUFBQTtBQUFELFVBQVUsS0FBS0osS0FBckI7QUFDQSxZQUFNO0FBQUNzQixRQUFBQTtBQUFELFVBQWdCbEIsS0FBdEI7QUFDQSxZQUFNc0UsU0FBUyxHQUFHeEIsS0FBSyxDQUFDaUIsYUFBTixDQUFvQlEsT0FBcEIsQ0FBNEIsNkJBQTVCLENBQWxCO0FBQ0EsVUFBSS9ELEtBQUo7O0FBQ0EsVUFBSThELFNBQUosRUFBZTtBQUNYLGNBQU1FLG1CQUFtQixHQUFHUCxJQUFJLENBQUNRLEtBQUwsQ0FBV0gsU0FBWCxDQUE1QjtBQUNBLGNBQU1JLGlCQUFpQixHQUFHRixtQkFBbUIsQ0FBQ1osR0FBcEIsQ0FBd0JDLENBQUMsSUFBSTNDLFdBQVcsQ0FBQ3lELGVBQVosQ0FBNEJkLENBQTVCLENBQTdCLENBQTFCO0FBQ0FyRCxRQUFBQSxLQUFLLEdBQUdrRSxpQkFBUjtBQUNILE9BSkQsTUFJTztBQUNILGNBQU0vRCxJQUFJLEdBQUdtQyxLQUFLLENBQUNpQixhQUFOLENBQW9CUSxPQUFwQixDQUE0QixZQUE1QixDQUFiO0FBQ0EvRCxRQUFBQSxLQUFLLEdBQUcsd0NBQXNCRyxJQUF0QixFQUE0Qk8sV0FBNUIsQ0FBUjtBQUNIOztBQUNELFdBQUtpRCxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsWUFBTWxFLEtBQUssR0FBRywrQkFBcUIsS0FBS3NCLFVBQTFCLEVBQXNDdkIsS0FBdEMsRUFBNkN3RCxRQUFRLENBQUNDLFlBQVQsRUFBN0MsQ0FBZDtBQUNBLGdEQUF5QnhELEtBQXpCLEVBQWdDTyxLQUFoQztBQUNBc0MsTUFBQUEsS0FBSyxDQUFDc0IsY0FBTjtBQUNILEtBbkxrQjtBQUFBLG9EQXFMUHRCLEtBQUQsSUFBVztBQUNsQjtBQUNBLFVBQUksS0FBS0MsZUFBVCxFQUEwQjtBQUN0QjtBQUNIOztBQUNELFdBQUtvQixhQUFMLEdBQXFCLElBQXJCO0FBQ0EsWUFBTVMsR0FBRyxHQUFHcEIsUUFBUSxDQUFDQyxZQUFULEVBQVo7QUFDQSxZQUFNO0FBQUNvQixRQUFBQSxLQUFEO0FBQVFsRSxRQUFBQTtBQUFSLFVBQWdCLGdDQUFzQixLQUFLWSxVQUEzQixFQUF1Q3FELEdBQXZDLENBQXRCO0FBQ0EsV0FBS2hGLEtBQUwsQ0FBV0ksS0FBWCxDQUFpQjhFLE1BQWpCLENBQXdCbkUsSUFBeEIsRUFBOEJtQyxLQUFLLENBQUNoRCxTQUFwQyxFQUErQytFLEtBQS9DO0FBQ0gsS0E5TGtCO0FBQUEsbURBMFBULE1BQU07QUFDWnJCLE1BQUFBLFFBQVEsQ0FBQ3VCLG1CQUFULENBQTZCLGlCQUE3QixFQUFnRCxLQUFLQyxrQkFBckQ7QUFDSCxLQTVQa0I7QUFBQSxvREE4UFIsTUFBTTtBQUNieEIsTUFBQUEsUUFBUSxDQUFDeUIsZ0JBQVQsQ0FBMEIsaUJBQTFCLEVBQTZDLEtBQUtELGtCQUFsRCxFQURhLENBRWI7O0FBQ0EsV0FBS0UsY0FBTCxHQUFzQixJQUF0Qjs7QUFDQSxXQUFLQyx5QkFBTDtBQUNILEtBblFrQjtBQUFBLDhEQXFRRSxNQUFNO0FBQ3ZCLFdBQUtBLHlCQUFMOztBQUNBLFlBQU10RyxTQUFTLEdBQUcyRSxRQUFRLENBQUNDLFlBQVQsRUFBbEI7O0FBQ0EsVUFBSSxLQUFLMkIsZ0JBQUwsSUFBeUJ2RyxTQUFTLENBQUNLLFdBQXZDLEVBQW9EO0FBQ2hELGFBQUtrRyxnQkFBTCxHQUF3QixLQUF4Qjs7QUFDQSxZQUFJLEtBQUtsRCxhQUFULEVBQXdCO0FBQ3BCLGVBQUtBLGFBQUwsQ0FBbUJDLElBQW5CO0FBQ0g7QUFDSixPQUxELE1BS08sSUFBSSxDQUFDdEQsU0FBUyxDQUFDSyxXQUFmLEVBQTRCO0FBQy9CLGFBQUtrRyxnQkFBTCxHQUF3QixJQUF4Qjs7QUFDQSxZQUFJLEtBQUtsRCxhQUFULEVBQXdCO0FBQ3BCLGdCQUFNbUQsYUFBYSxHQUFHeEcsU0FBUyxDQUFDeUcsVUFBVixDQUFxQixDQUFyQixFQUF3QkMscUJBQXhCLEVBQXRCOztBQUNBLGVBQUtyRCxhQUFMLENBQW1Cc0QsTUFBbkIsQ0FBMEJILGFBQTFCO0FBQ0g7QUFDSjtBQUNKLEtBcFJrQjtBQUFBLHNEQXNSTHZDLEtBQUQsSUFBVztBQUNwQixZQUFNOUMsS0FBSyxHQUFHLEtBQUtKLEtBQUwsQ0FBV0ksS0FBekI7QUFDQSxZQUFNeUYsTUFBTSxHQUFHbkgsTUFBTSxHQUFHd0UsS0FBSyxDQUFDNEMsT0FBVCxHQUFtQjVDLEtBQUssQ0FBQzZDLE9BQTlDO0FBQ0EsVUFBSUMsT0FBTyxHQUFHLEtBQWQsQ0FIb0IsQ0FJcEI7O0FBQ0EsVUFBSUgsTUFBTSxJQUFJM0MsS0FBSyxDQUFDbkUsR0FBTixLQUFja0gsY0FBSUMsQ0FBaEMsRUFBbUM7QUFDL0IsYUFBS0MsZUFBTCxDQUFxQixNQUFyQjs7QUFDQUgsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FGK0IsQ0FHbkM7QUFDQyxPQUpELE1BSU8sSUFBSUgsTUFBTSxJQUFJM0MsS0FBSyxDQUFDbkUsR0FBTixLQUFja0gsY0FBSUcsQ0FBaEMsRUFBbUM7QUFDdEMsYUFBS0QsZUFBTCxDQUFxQixTQUFyQjs7QUFDQUgsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FGc0MsQ0FHMUM7QUFDQyxPQUpNLE1BSUEsSUFBSUgsTUFBTSxJQUFJM0MsS0FBSyxDQUFDbkUsR0FBTixLQUFja0gsY0FBSUksWUFBaEMsRUFBOEM7QUFDakQsYUFBS0YsZUFBTCxDQUFxQixPQUFyQjs7QUFDQUgsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FGaUQsQ0FHckQ7QUFDQyxPQUpNLE1BSUEsSUFBSyxDQUFDdEgsTUFBRCxJQUFXbUgsTUFBWCxJQUFxQjNDLEtBQUssQ0FBQ25FLEdBQU4sS0FBY2tILGNBQUlLLENBQXhDLElBQ0E1SCxNQUFNLElBQUltSCxNQUFWLElBQW9CM0MsS0FBSyxDQUFDcUQsUUFBMUIsSUFBc0NyRCxLQUFLLENBQUNuRSxHQUFOLEtBQWNrSCxjQUFJTyxDQUQ1RCxFQUNnRTtBQUNuRSxZQUFJLEtBQUs5RCxjQUFMLENBQW9CK0QsT0FBcEIsRUFBSixFQUFtQztBQUMvQixnQkFBTTtBQUFDN0YsWUFBQUEsS0FBRDtBQUFRcUUsWUFBQUE7QUFBUixjQUFpQixLQUFLdkMsY0FBTCxDQUFvQmdFLElBQXBCLEVBQXZCLENBRCtCLENBRS9CO0FBQ0E7O0FBQ0F0RyxVQUFBQSxLQUFLLENBQUN1RyxLQUFOLENBQVkvRixLQUFaLEVBQW1CcUUsS0FBbkIsRUFBMEIsYUFBMUI7QUFDSDs7QUFDRGUsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FQbUUsQ0FRdkU7QUFDQyxPQVZNLE1BVUEsSUFBSUgsTUFBTSxJQUFJM0MsS0FBSyxDQUFDbkUsR0FBTixLQUFja0gsY0FBSU8sQ0FBaEMsRUFBbUM7QUFDdEMsWUFBSSxLQUFLOUQsY0FBTCxDQUFvQmtFLE9BQXBCLEVBQUosRUFBbUM7QUFDL0IsZ0JBQU07QUFBQ2hHLFlBQUFBLEtBQUQ7QUFBUXFFLFlBQUFBO0FBQVIsY0FBaUIsS0FBS3ZDLGNBQUwsQ0FBb0JtRSxJQUFwQixDQUF5QixLQUFLN0csS0FBTCxDQUFXSSxLQUFwQyxDQUF2QixDQUQrQixDQUUvQjtBQUNBOztBQUNBQSxVQUFBQSxLQUFLLENBQUN1RyxLQUFOLENBQVkvRixLQUFaLEVBQW1CcUUsS0FBbkIsRUFBMEIsYUFBMUI7QUFDSDs7QUFDRGUsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FQc0MsQ0FRMUM7QUFDQyxPQVRNLE1BU0EsSUFBSTlDLEtBQUssQ0FBQ25FLEdBQU4sS0FBY2tILGNBQUlhLEtBQWxCLEtBQTRCNUQsS0FBSyxDQUFDcUQsUUFBTixJQUFtQjdILE1BQU0sSUFBSXdFLEtBQUssQ0FBQzZELE1BQS9ELENBQUosRUFBNkU7QUFDaEYsYUFBS0MsV0FBTCxDQUFpQixJQUFqQjs7QUFDQWhCLFFBQUFBLE9BQU8sR0FBRyxJQUFWLENBRmdGLENBR3BGO0FBQ0MsT0FKTSxNQUlBLElBQUlILE1BQU0sSUFBSTNDLEtBQUssQ0FBQ25FLEdBQU4sS0FBY2tILGNBQUlnQixJQUE1QixJQUFvQyxDQUFDL0QsS0FBSyxDQUFDcUQsUUFBL0MsRUFBeUQ7QUFDNUQsaUNBQWEsS0FBSzVFLFVBQWxCLEVBQThCdkIsS0FBOUIsRUFBcUM7QUFDakNLLFVBQUFBLEtBQUssRUFBRSxDQUQwQjtBQUVqQ0MsVUFBQUEsTUFBTSxFQUFFO0FBRnlCLFNBQXJDO0FBSUFzRixRQUFBQSxPQUFPLEdBQUcsSUFBVixDQUw0RCxDQU1oRTtBQUNDLE9BUE0sTUFPQSxJQUFJSCxNQUFNLElBQUkzQyxLQUFLLENBQUNuRSxHQUFOLEtBQWNrSCxjQUFJaUIsR0FBNUIsSUFBbUMsQ0FBQ2hFLEtBQUssQ0FBQ3FELFFBQTlDLEVBQXdEO0FBQzNELGlDQUFhLEtBQUs1RSxVQUFsQixFQUE4QnZCLEtBQTlCLEVBQXFDO0FBQ2pDSyxVQUFBQSxLQUFLLEVBQUVMLEtBQUssQ0FBQ1EsS0FBTixDQUFZdUcsTUFBWixHQUFxQixDQURLO0FBRWpDekcsVUFBQUEsTUFBTSxFQUFFTixLQUFLLENBQUNRLEtBQU4sQ0FBWVIsS0FBSyxDQUFDUSxLQUFOLENBQVl1RyxNQUFaLEdBQXFCLENBQWpDLEVBQW9DcEcsSUFBcEMsQ0FBeUNvRztBQUZoQixTQUFyQztBQUlBbkIsUUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FMMkQsQ0FNL0Q7QUFDQyxPQVBNLE1BT0E7QUFDSCxjQUFNb0IsZ0JBQWdCLEdBQUdsRSxLQUFLLENBQUM0QyxPQUFOLElBQWlCNUMsS0FBSyxDQUFDNkQsTUFBaEQ7QUFDQSxjQUFNTSxlQUFlLEdBQUdELGdCQUFnQixJQUFJbEUsS0FBSyxDQUFDcUQsUUFBbEQ7O0FBQ0EsWUFBSW5HLEtBQUssQ0FBQ3FDLFlBQU4sSUFBc0JyQyxLQUFLLENBQUNxQyxZQUFOLENBQW1CNkUsY0FBbkIsRUFBMUIsRUFBK0Q7QUFDM0QsZ0JBQU03RSxZQUFZLEdBQUdyQyxLQUFLLENBQUNxQyxZQUEzQjs7QUFDQSxrQkFBUVMsS0FBSyxDQUFDbkUsR0FBZDtBQUNJLGlCQUFLa0gsY0FBSXNCLFFBQVQ7QUFDSSxrQkFBSSxDQUFDRixlQUFMLEVBQXNCO0FBQ2xCNUUsZ0JBQUFBLFlBQVksQ0FBQytFLFNBQWIsQ0FBdUJ0RSxLQUF2QjtBQUNBOEMsZ0JBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBQ0Q7O0FBQ0osaUJBQUtDLGNBQUl3QixVQUFUO0FBQ0ksa0JBQUksQ0FBQ0osZUFBTCxFQUFzQjtBQUNsQjVFLGdCQUFBQSxZQUFZLENBQUNpRixXQUFiLENBQXlCeEUsS0FBekI7QUFDQThDLGdCQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEOztBQUNKLGlCQUFLQyxjQUFJMEIsR0FBVDtBQUNJLGtCQUFJLENBQUNQLGdCQUFMLEVBQXVCO0FBQ25CM0UsZ0JBQUFBLFlBQVksQ0FBQ21GLEtBQWIsQ0FBbUIxRSxLQUFuQjtBQUNBOEMsZ0JBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0g7O0FBQ0Q7O0FBQ0osaUJBQUtDLGNBQUk0QixNQUFUO0FBQ0ksa0JBQUksQ0FBQ1IsZUFBTCxFQUFzQjtBQUNsQjVFLGdCQUFBQSxZQUFZLENBQUNxRixRQUFiLENBQXNCNUUsS0FBdEI7QUFDQThDLGdCQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNIOztBQUNEOztBQUNKO0FBQ0k7QUFBUTtBQTFCaEI7QUE0QkgsU0E5QkQsTUE4Qk8sSUFBSTlDLEtBQUssQ0FBQ25FLEdBQU4sS0FBY2tILGNBQUkwQixHQUF0QixFQUEyQjtBQUM5QixlQUFLSSxnQkFBTDs7QUFDQS9CLFVBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0gsU0FITSxNQUdBLElBQUk5QyxLQUFLLENBQUNuRSxHQUFOLEtBQWNrSCxjQUFJK0IsU0FBbEIsSUFBK0I5RSxLQUFLLENBQUNuRSxHQUFOLEtBQWNrSCxjQUFJZ0MsTUFBckQsRUFBNkQ7QUFDaEUsZUFBSzNGLGFBQUwsQ0FBbUJDLElBQW5CO0FBQ0g7QUFDSjs7QUFDRCxVQUFJeUQsT0FBSixFQUFhO0FBQ1Q5QyxRQUFBQSxLQUFLLENBQUNzQixjQUFOO0FBQ0F0QixRQUFBQSxLQUFLLENBQUNnRixlQUFOO0FBQ0g7QUFDSixLQXhYa0I7QUFBQSxrRUFvYU9DLFVBQUQsSUFBZ0I7QUFDckMsV0FBS25JLEtBQUwsQ0FBV0ksS0FBWCxDQUFpQnFDLFlBQWpCLENBQThCMkYsa0JBQTlCLENBQWlERCxVQUFqRDtBQUNILEtBdGFrQjtBQUFBLDBFQXdhYyxDQUFDQSxVQUFELEVBQWFFLGVBQWIsS0FBaUM7QUFDOUQsV0FBS3JJLEtBQUwsQ0FBV0ksS0FBWCxDQUFpQnFDLFlBQWpCLENBQThCNkYsMEJBQTlCLENBQXlESCxVQUF6RDtBQUNBLFdBQUszRixRQUFMLENBQWM7QUFBQzZGLFFBQUFBO0FBQUQsT0FBZDtBQUNILEtBM2FrQjtBQUFBLDJEQWllQUUsTUFBRCxJQUFZO0FBQzFCLFlBQU1sSSxLQUFLLEdBQUcsK0JBQ1YsS0FBS3NCLFVBREssRUFFVixLQUFLM0IsS0FBTCxDQUFXSSxLQUZELEVBR1Z3RCxRQUFRLENBQUNDLFlBQVQsRUFIVSxDQUFkOztBQUlBLFVBQUl4RCxLQUFLLENBQUM4RyxNQUFOLEtBQWlCLENBQXJCLEVBQXdCO0FBQ3BCO0FBQ0g7O0FBQ0QsV0FBS3pFLGNBQUwsQ0FBb0I4Rix1QkFBcEIsQ0FBNEMsS0FBS3hJLEtBQUwsQ0FBV0ksS0FBdkQ7QUFDQSxXQUFLbUUsYUFBTCxHQUFxQixJQUFyQjs7QUFDQSxjQUFRZ0UsTUFBUjtBQUNJLGFBQUssTUFBTDtBQUNJLDhDQUFtQmxJLEtBQW5CLEVBQTBCLElBQTFCO0FBQ0E7O0FBQ0osYUFBSyxTQUFMO0FBQ0ksOENBQW1CQSxLQUFuQixFQUEwQixHQUExQjtBQUNBOztBQUNKLGFBQUssZUFBTDtBQUNJLDhDQUFtQkEsS0FBbkIsRUFBMEIsT0FBMUIsRUFBbUMsUUFBbkM7QUFDQTs7QUFDSixhQUFLLE1BQUw7QUFDSSw2Q0FBa0JBLEtBQWxCO0FBQ0E7O0FBQ0osYUFBSyxPQUFMO0FBQ0ksOENBQW1CQSxLQUFuQjtBQUNBO0FBZlI7QUFpQkgsS0E1ZmtCO0FBRWYsU0FBS29JLEtBQUwsR0FBYTtBQUNUaEcsTUFBQUEsWUFBWSxFQUFFO0FBREwsS0FBYjtBQUdBLFNBQUtkLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxTQUFLK0csZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxTQUFLcEcsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFNBQUtpQyxhQUFMLEdBQXFCLEtBQXJCO0FBQ0EsU0FBS3BCLGVBQUwsR0FBdUIsS0FBdkI7QUFDQSxTQUFLcUMsZ0JBQUwsR0FBd0IsS0FBeEI7QUFDQSxTQUFLbUQsc0JBQUwsR0FBOEIsSUFBOUI7QUFDSCxHQXRCMkQsQ0F3QjVEOzs7QUFDQUMsRUFBQUEsMEJBQTBCLENBQUNDLFNBQUQsRUFBWTtBQUFFO0FBQ3BDLFFBQUksS0FBSzdJLEtBQUwsQ0FBV21DLFdBQVgsS0FBMkIwRyxTQUFTLENBQUMxRyxXQUFyQyxJQUFvRCxLQUFLbkMsS0FBTCxDQUFXbUMsV0FBbkUsRUFBZ0Y7QUFDNUUsWUFBTTtBQUFDRCxRQUFBQTtBQUFELFVBQVksS0FBS2xDLEtBQUwsQ0FBV0ksS0FBN0I7O0FBQ0EsVUFBSThCLE9BQUosRUFBYTtBQUNULGFBQUtFLGdCQUFMO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS0MsZ0JBQUw7QUFDSDtBQUNKO0FBQ0o7O0FBa0VERCxFQUFBQSxnQkFBZ0IsR0FBRztBQUNmLFNBQUtULFVBQUwsQ0FBZ0JtSCxLQUFoQixDQUFzQkMsV0FBdEIsQ0FBa0MsZUFBbEMsYUFBdUQsS0FBSy9JLEtBQUwsQ0FBV21DLFdBQWxFOztBQUNBLFNBQUtSLFVBQUwsQ0FBZ0JxSCxTQUFoQixDQUEwQkMsR0FBMUIsQ0FBOEIsb0NBQTlCO0FBQ0g7O0FBRUQ1RyxFQUFBQSxnQkFBZ0IsR0FBRztBQUNmLFNBQUtWLFVBQUwsQ0FBZ0JxSCxTQUFoQixDQUEwQkUsTUFBMUIsQ0FBaUMsb0NBQWpDOztBQUNBLFNBQUt2SCxVQUFMLENBQWdCbUgsS0FBaEIsQ0FBc0JLLGNBQXRCLENBQXFDLGVBQXJDO0FBQ0g7O0FBK0JEQyxFQUFBQSxXQUFXLENBQUNsRyxLQUFELEVBQVE7QUFDZjtBQUNBO0FBQ0E7QUFDQSxXQUFPLENBQUMsRUFBRSxLQUFLQyxlQUFMLElBQXlCRCxLQUFLLENBQUNtRyxXQUFOLElBQXFCbkcsS0FBSyxDQUFDbUcsV0FBTixDQUFrQkQsV0FBbEUsQ0FBUjtBQUNIOztBQTBERHBDLEVBQUFBLFdBQVcsQ0FBQ3NDLFlBQUQsRUFBZXBKLFNBQVMsR0FBRyxZQUEzQixFQUF5QztBQUNoRCxVQUFNOEUsR0FBRyxHQUFHcEIsUUFBUSxDQUFDQyxZQUFULEVBQVo7QUFDQSxVQUFNO0FBQUNvQixNQUFBQSxLQUFEO0FBQVFsRSxNQUFBQTtBQUFSLFFBQWdCLGdDQUFzQixLQUFLWSxVQUEzQixFQUF1Q3FELEdBQXZDLENBQXRCO0FBQ0EsVUFBTXVFLE9BQU8sR0FBR3hJLElBQUksQ0FBQ3lJLE1BQUwsQ0FBWSxDQUFaLEVBQWV2RSxLQUFLLENBQUN2RSxNQUFyQixJQUErQjRJLFlBQS9CLEdBQThDdkksSUFBSSxDQUFDeUksTUFBTCxDQUFZdkUsS0FBSyxDQUFDdkUsTUFBbEIsQ0FBOUQ7QUFDQXVFLElBQUFBLEtBQUssQ0FBQ3ZFLE1BQU4sSUFBZ0I0SSxZQUFZLENBQUNuQyxNQUE3QjtBQUNBLFNBQUs1QyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsU0FBS3ZFLEtBQUwsQ0FBV0ksS0FBWCxDQUFpQjhFLE1BQWpCLENBQXdCcUUsT0FBeEIsRUFBaUNySixTQUFqQyxFQUE0QytFLEtBQTVDO0FBQ0gsR0FqTjJELENBbU41RDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQWhELEVBQUFBLHlCQUF5QixDQUFDRixRQUFELEVBQVc7QUFDaEMsVUFBTTtBQUFDM0IsTUFBQUE7QUFBRCxRQUFVLEtBQUtKLEtBQXJCO0FBQ0EsU0FBS3lKLGFBQUwsR0FBcUIxSCxRQUFRLENBQUMySCxPQUFULENBQWlCdEosS0FBakIsQ0FBckI7QUFDQSxTQUFLdUosVUFBTCxHQUFrQjVILFFBQVEsQ0FBQzZILFFBQVQsQ0FBa0J4SixLQUFsQixDQUFsQjtBQUNBLFNBQUtrRixjQUFMLEdBQXNCdEcsY0FBYyxDQUFDNEUsUUFBUSxDQUFDQyxZQUFULEVBQUQsQ0FBcEM7QUFDSDs7QUFFRDBCLEVBQUFBLHlCQUF5QixHQUFHO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBLFFBQUksQ0FBQyxLQUFLNUQsVUFBVixFQUFzQjtBQUNsQjtBQUNIOztBQUNELFVBQU0xQyxTQUFTLEdBQUcyRSxRQUFRLENBQUNDLFlBQVQsRUFBbEI7O0FBQ0EsUUFBSSxDQUFDLEtBQUt5QixjQUFOLElBQXdCLENBQUM3RixlQUFlLENBQUMsS0FBSzZGLGNBQU4sRUFBc0JyRyxTQUF0QixDQUE1QyxFQUE4RTtBQUMxRSxXQUFLcUcsY0FBTCxHQUFzQnRHLGNBQWMsQ0FBQ0MsU0FBRCxDQUFwQztBQUNBLFlBQU07QUFBQ2dHLFFBQUFBLEtBQUQ7QUFBUWxFLFFBQUFBO0FBQVIsVUFBZ0IsZ0NBQXNCLEtBQUtZLFVBQTNCLEVBQXVDMUMsU0FBdkMsQ0FBdEI7QUFDQSxXQUFLMEssVUFBTCxHQUFrQjFFLEtBQWxCO0FBQ0EsV0FBS3dFLGFBQUwsR0FBcUJ4RSxLQUFLLENBQUN2RSxNQUFOLEtBQWlCSyxJQUFJLENBQUNvRyxNQUEzQztBQUNIOztBQUNELFdBQU8sS0FBS3dDLFVBQVo7QUFDSDs7QUFFREUsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixTQUFLbkgsY0FBTCxDQUFvQm9ILEtBQXBCO0FBQ0g7O0FBRURDLEVBQUFBLFFBQVEsR0FBRztBQUNQLFdBQU8sS0FBS0osVUFBWjtBQUNIOztBQUVESyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixXQUFPLENBQUMsS0FBSzFFLGNBQU4sSUFBd0IsS0FBS0EsY0FBTCxDQUFvQmhHLFdBQW5EO0FBQ0g7O0FBRUQySyxFQUFBQSxjQUFjLEdBQUc7QUFDYixXQUFPLEtBQUtGLFFBQUwsR0FBZ0JySixNQUFoQixLQUEyQixDQUFsQztBQUNIOztBQUVEd0osRUFBQUEsWUFBWSxHQUFHO0FBQ1gsV0FBTyxLQUFLVCxhQUFaO0FBQ0g7O0FBa0lELFFBQU0xQixnQkFBTixHQUF5QjtBQUNyQixRQUFJO0FBQ0EsWUFBTSxJQUFJdEUsT0FBSixDQUFZQyxPQUFPLElBQUksS0FBS2xCLFFBQUwsQ0FBYztBQUFDMkgsUUFBQUEsY0FBYyxFQUFFO0FBQWpCLE9BQWQsRUFBdUN6RyxPQUF2QyxDQUF2QixDQUFOO0FBQ0EsWUFBTTtBQUFDdEQsUUFBQUE7QUFBRCxVQUFVLEtBQUtKLEtBQXJCO0FBQ0EsWUFBTWlGLEtBQUssR0FBRyxLQUFLOEUsUUFBTCxFQUFkO0FBQ0EsWUFBTWhJLFFBQVEsR0FBRzNCLEtBQUssQ0FBQ2dLLGlCQUFOLENBQXdCbkYsS0FBSyxDQUFDdkUsTUFBOUIsRUFBc0N1RSxLQUFLLENBQUNvRixTQUE1QyxDQUFqQjtBQUNBLFlBQU1oSyxLQUFLLEdBQUdELEtBQUssQ0FBQ0UsVUFBTixDQUFpQnlCLFFBQWpCLENBQWQ7QUFDQTFCLE1BQUFBLEtBQUssQ0FBQ0csb0JBQU4sQ0FBMkIsQ0FBQ0MsS0FBRCxFQUFRQyxNQUFSLEVBQWdCQyxJQUFoQixLQUF5QjtBQUNoRCxlQUFPQSxJQUFJLENBQUNJLElBQUwsQ0FBVUwsTUFBVixNQUFzQixHQUF0QixLQUNIQyxJQUFJLENBQUNuQixJQUFMLEtBQWMsT0FBZCxJQUNBbUIsSUFBSSxDQUFDbkIsSUFBTCxLQUFjLGdCQURkLElBRUFtQixJQUFJLENBQUNuQixJQUFMLEtBQWMsU0FIWCxDQUFQO0FBS0gsT0FORDtBQU9BLFlBQU07QUFBQzhCLFFBQUFBO0FBQUQsVUFBZ0JsQixLQUF0QixDQWJBLENBY0E7O0FBQ0EsWUFBTUEsS0FBSyxDQUFDa0ssU0FBTixDQUFnQixNQUFNO0FBQ3hCLGNBQU1DLFFBQVEsR0FBR2xLLEtBQUssQ0FBQ1ksT0FBTixDQUFjLENBQUNLLFdBQVcsQ0FBQ2tKLGFBQVosQ0FBMEJuSyxLQUFLLENBQUNVLElBQWhDLENBQUQsQ0FBZCxDQUFqQjtBQUNBLGVBQU9YLEtBQUssQ0FBQ2dLLGlCQUFOLENBQXdCbkYsS0FBSyxDQUFDdkUsTUFBTixHQUFlNkosUUFBdkMsRUFBaUQsSUFBakQsQ0FBUDtBQUNILE9BSEssQ0FBTixDQWZBLENBb0JBOztBQUNBLFVBQUluSyxLQUFLLENBQUNxQyxZQUFWLEVBQXdCO0FBQ3BCLGNBQU1yQyxLQUFLLENBQUNxQyxZQUFOLENBQW1CbUYsS0FBbkIsRUFBTjs7QUFDQSxZQUFJLENBQUN4SCxLQUFLLENBQUNxQyxZQUFOLENBQW1CZ0ksWUFBbkIsRUFBTCxFQUF3QztBQUNwQyxlQUFLakksUUFBTCxDQUFjO0FBQUMySCxZQUFBQSxjQUFjLEVBQUU7QUFBakIsV0FBZDtBQUNBL0osVUFBQUEsS0FBSyxDQUFDcUMsWUFBTixDQUFtQmlJLEtBQW5CO0FBQ0g7QUFDSjtBQUNKLEtBNUJELENBNEJFLE9BQU85SSxHQUFQLEVBQVk7QUFDVkMsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQWQ7QUFDSDtBQUNKOztBQUVEK0ksRUFBQUEsbUJBQW1CLEdBQUc7QUFDbEIsV0FBTyxLQUFLaEosVUFBWjtBQUNIOztBQUVEaUosRUFBQUEsVUFBVSxHQUFHO0FBQ1QsV0FBTyxLQUFLckcsYUFBWjtBQUNIOztBQVdEc0csRUFBQUEsNkJBQTZCLEdBQUc7QUFDNUIsVUFBTUMsYUFBYSxHQUFHQyx1QkFBY0MsUUFBZCxDQUF1Qix1Q0FBdkIsQ0FBdEI7O0FBQ0EsU0FBS2hMLEtBQUwsQ0FBV0ksS0FBWCxDQUFpQjZLLG9CQUFqQixDQUFzQ0gsYUFBYSxHQUFHLEtBQUtJLGdCQUFSLEdBQTJCLElBQTlFO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CdkgsSUFBQUEsUUFBUSxDQUFDdUIsbUJBQVQsQ0FBNkIsaUJBQTdCLEVBQWdELEtBQUtDLGtCQUFyRDs7QUFDQSxTQUFLekQsVUFBTCxDQUFnQndELG1CQUFoQixDQUFvQyxPQUFwQyxFQUE2QyxLQUFLM0IsUUFBbEQsRUFBNEQsSUFBNUQ7O0FBQ0EsU0FBSzdCLFVBQUwsQ0FBZ0J3RCxtQkFBaEIsQ0FBb0Msa0JBQXBDLEVBQXdELEtBQUtpRyxtQkFBN0QsRUFBa0YsSUFBbEY7O0FBQ0EsU0FBS3pKLFVBQUwsQ0FBZ0J3RCxtQkFBaEIsQ0FBb0MsZ0JBQXBDLEVBQXNELEtBQUtrRyxpQkFBM0QsRUFBOEUsSUFBOUU7O0FBQ0FOLDJCQUFjTyxjQUFkLENBQTZCLEtBQUszQyxzQkFBbEM7QUFDSDs7QUFFRDRDLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFVBQU1uTCxLQUFLLEdBQUcsS0FBS0osS0FBTCxDQUFXSSxLQUF6QjtBQUNBQSxJQUFBQSxLQUFLLENBQUNvTCxpQkFBTixDQUF3QixLQUFLQyxrQkFBN0I7QUFDQSxTQUFLOUMsc0JBQUwsR0FBOEJvQyx1QkFBY1csWUFBZCxDQUEyQix1Q0FBM0IsRUFBb0UsSUFBcEUsRUFBMEUsTUFBTTtBQUMxRyxXQUFLYiw2QkFBTDtBQUNILEtBRjZCLENBQTlCOztBQUdBLFNBQUtBLDZCQUFMOztBQUNBLFVBQU12SixXQUFXLEdBQUdsQixLQUFLLENBQUNrQixXQUExQixDQVBnQixDQVFoQjtBQUNBOztBQUNBQSxJQUFBQSxXQUFXLENBQUNxSyxzQkFBWixDQUFtQyxnQ0FDL0IsTUFBTSxLQUFLakQsZ0JBRG9CLEVBRS9CMUgsS0FBSyxJQUFJLElBQUl5QyxPQUFKLENBQVlDLE9BQU8sSUFBSSxLQUFLbEIsUUFBTCxDQUFjO0FBQUN4QixNQUFBQTtBQUFELEtBQWQsRUFBdUIwQyxPQUF2QixDQUF2QixDQUZzQixDQUFuQztBQUlBLFNBQUtoQixjQUFMLEdBQXNCLElBQUlrSixnQkFBSixDQUFtQnRLLFdBQW5CLENBQXRCLENBZGdCLENBZWhCOztBQUNBLFNBQUttSyxrQkFBTCxDQUF3QixLQUFLSSx3QkFBTCxFQUF4QixFQWhCZ0IsQ0FpQmhCO0FBQ0E7OztBQUNBLFNBQUtsSyxVQUFMLENBQWdCMEQsZ0JBQWhCLENBQWlDLE9BQWpDLEVBQTBDLEtBQUs3QixRQUEvQyxFQUF5RCxJQUF6RDs7QUFDQSxTQUFLN0IsVUFBTCxDQUFnQjBELGdCQUFoQixDQUFpQyxrQkFBakMsRUFBcUQsS0FBSytGLG1CQUExRCxFQUErRSxJQUEvRTs7QUFDQSxTQUFLekosVUFBTCxDQUFnQjBELGdCQUFoQixDQUFpQyxnQkFBakMsRUFBbUQsS0FBS2dHLGlCQUF4RCxFQUEyRSxJQUEzRTs7QUFDQSxTQUFLMUosVUFBTCxDQUFnQm1LLEtBQWhCO0FBQ0g7O0FBRURELEVBQUFBLHdCQUF3QixHQUFHO0FBQ3ZCLFFBQUk1TCxhQUFKOztBQUNBLFFBQUksS0FBS0QsS0FBTCxDQUFXK0wsWUFBZixFQUE2QjtBQUN6QjtBQUNBO0FBQ0EsWUFBTTlHLEtBQUssR0FBRyxLQUFLakYsS0FBTCxDQUFXK0wsWUFBekI7QUFDQTlMLE1BQUFBLGFBQWEsR0FBRyxLQUFLRCxLQUFMLENBQVdJLEtBQVgsQ0FBaUJnSyxpQkFBakIsQ0FBbUNuRixLQUFLLENBQUN2RSxNQUF6QyxFQUFpRHVFLEtBQUssQ0FBQ29GLFNBQXZELENBQWhCO0FBQ0gsS0FMRCxNQUtPO0FBQ0g7QUFDQXBLLE1BQUFBLGFBQWEsR0FBRyxLQUFLRCxLQUFMLENBQVdJLEtBQVgsQ0FBaUI0TCxnQkFBakIsRUFBaEI7QUFDSDs7QUFDRCxXQUFPL0wsYUFBUDtBQUNIOztBQStCRGdNLEVBQUFBLE1BQU0sR0FBRztBQUNMLFFBQUl4SixZQUFKOztBQUNBLFFBQUksS0FBS2dHLEtBQUwsQ0FBV2hHLFlBQWYsRUFBNkI7QUFDekIsWUFBTXpCLEtBQUssR0FBRyxLQUFLeUgsS0FBTCxDQUFXekgsS0FBekI7QUFDQSxZQUFNa0wsUUFBUSxHQUFHbEwsS0FBSyxDQUFDbUcsTUFBdkI7QUFDQTFFLE1BQUFBLFlBQVksR0FBSTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDWiw2QkFBQyxxQkFBRDtBQUNJLFFBQUEsR0FBRyxFQUFFMEosR0FBRyxJQUFJLEtBQUt6RCxnQkFBTCxHQUF3QnlELEdBRHhDO0FBRUksUUFBQSxLQUFLLEVBQUVuTCxLQUZYO0FBR0ksUUFBQSxTQUFTLEVBQUUsS0FBS29MLHNCQUhwQjtBQUlJLFFBQUEsaUJBQWlCLEVBQUUsS0FBS0MsOEJBSjVCO0FBS0ksUUFBQSxTQUFTLEVBQUU7QUFBQ0MsVUFBQUEsU0FBUyxFQUFFLElBQVo7QUFBa0J0SyxVQUFBQSxHQUFHLEVBQUVrSyxRQUF2QjtBQUFpQ0ssVUFBQUEsS0FBSyxFQUFFTDtBQUF4QyxTQUxmO0FBTUksUUFBQSxJQUFJLEVBQUUsS0FBS2xNLEtBQUwsQ0FBVytDO0FBTnJCLFFBRFksQ0FBaEI7QUFVSDs7QUFDRCxVQUFNeUosT0FBTyxHQUFHLHlCQUFXLHlCQUFYLEVBQXNDO0FBQ2xELDZDQUF1QyxLQUFLL0QsS0FBTCxDQUFXMEI7QUFEQSxLQUF0QyxDQUFoQjtBQUlBLFVBQU1zQyx3QkFBd0IsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGdDQUFqQixDQUFqQztBQUNBLFVBQU1DLFNBQVMsR0FBRztBQUNkQyxNQUFBQSxJQUFJLEVBQUUvTixpQkFBaUIsQ0FBQyxHQUFELENBRFQ7QUFFZGdPLE1BQUFBLE9BQU8sRUFBRWhPLGlCQUFpQixDQUFDLEdBQUQsQ0FGWjtBQUdkaU8sTUFBQUEsS0FBSyxFQUFFak8saUJBQWlCLENBQUMsR0FBRDtBQUhWLEtBQWxCO0FBTUEsVUFBTTtBQUFDdUosTUFBQUE7QUFBRCxRQUFvQixLQUFLSSxLQUEvQjtBQUVBLFdBQVE7QUFBSyxNQUFBLFNBQVMsRUFBRStEO0FBQWhCLE9BQ0YvSixZQURFLEVBRUosNkJBQUMsd0JBQUQ7QUFBMEIsTUFBQSxHQUFHLEVBQUUwSixHQUFHLElBQUksS0FBSzdKLGFBQUwsR0FBcUI2SixHQUEzRDtBQUFnRSxNQUFBLFFBQVEsRUFBRSxLQUFLaEcsZUFBL0U7QUFBZ0csTUFBQSxTQUFTLEVBQUV5RztBQUEzRyxNQUZJLEVBR0o7QUFDSSxNQUFBLFNBQVMsRUFBQywrQkFEZDtBQUVJLE1BQUEsZUFBZSxFQUFDLE1BRnBCO0FBR0ksTUFBQSxRQUFRLEVBQUMsR0FIYjtBQUlJLE1BQUEsTUFBTSxFQUFFLEtBQUtJLE9BSmpCO0FBS0ksTUFBQSxPQUFPLEVBQUUsS0FBS0MsUUFMbEI7QUFNSSxNQUFBLE1BQU0sRUFBRSxLQUFLQyxPQU5qQjtBQU9JLE1BQUEsS0FBSyxFQUFFLEtBQUtDLE1BUGhCO0FBUUksTUFBQSxPQUFPLEVBQUUsS0FBS0MsUUFSbEI7QUFTSSxNQUFBLFNBQVMsRUFBRSxLQUFLQyxVQVRwQjtBQVVJLE1BQUEsR0FBRyxFQUFFbEIsR0FBRyxJQUFJLEtBQUt4SyxVQUFMLEdBQWtCd0ssR0FWbEM7QUFXSSxvQkFBWSxLQUFLbk0sS0FBTCxDQUFXc04sS0FYM0I7QUFZSSxNQUFBLElBQUksRUFBQyxTQVpUO0FBYUksd0JBQWUsTUFibkI7QUFjSSwyQkFBa0IsTUFkdEI7QUFlSSx1QkFBYyxTQWZsQjtBQWdCSSx1QkFBZUMsT0FBTyxDQUFDLEtBQUs5RSxLQUFMLENBQVdoRyxZQUFaLENBaEIxQjtBQWlCSSwrQkFBdUI0RixlQUFlLElBQUksQ0FBbkIsR0FBdUIsMkNBQXdCQSxlQUF4QixDQUF2QixHQUFrRW1GLFNBakI3RjtBQWtCSSxNQUFBLEdBQUcsRUFBQztBQWxCUixNQUhJLENBQVI7QUF3Qkg7O0FBRUQxQixFQUFBQSxLQUFLLEdBQUc7QUFDSixTQUFLbkssVUFBTCxDQUFnQm1LLEtBQWhCO0FBQ0g7O0FBamtCMkQ7Ozs4QkFBM0NsTSxrQixlQUNFO0FBQ2ZxRCxFQUFBQSxRQUFRLEVBQUV3SyxtQkFBVUMsSUFETDtBQUVmdE4sRUFBQUEsS0FBSyxFQUFFcU4sbUJBQVVFLFVBQVYsQ0FBcUJDLGNBQXJCLEVBQWtDQyxVQUYxQjtBQUdmOUssRUFBQUEsSUFBSSxFQUFFMEssbUJBQVVFLFVBQVYsQ0FBcUJHLGlCQUFyQixFQUEyQkQsVUFIbEI7QUFJZjFMLEVBQUFBLFdBQVcsRUFBRXNMLG1CQUFVTSxNQUpSO0FBS2ZULEVBQUFBLEtBQUssRUFBRUcsbUJBQVVNLE1BTEY7QUFLYTtBQUM1QmhDLEVBQUFBLFlBQVksRUFBRTBCLG1CQUFVTyxNQU5ULENBTWlCOztBQU5qQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IEVkaXRvck1vZGVsIGZyb20gJy4uLy4uLy4uL2VkaXRvci9tb2RlbCc7XHJcbmltcG9ydCBIaXN0b3J5TWFuYWdlciBmcm9tICcuLi8uLi8uLi9lZGl0b3IvaGlzdG9yeSc7XHJcbmltcG9ydCB7c2V0U2VsZWN0aW9ufSBmcm9tICcuLi8uLi8uLi9lZGl0b3IvY2FyZXQnO1xyXG5pbXBvcnQge1xyXG4gICAgZm9ybWF0UmFuZ2VBc1F1b3RlLFxyXG4gICAgZm9ybWF0UmFuZ2VBc0NvZGUsXHJcbiAgICB0b2dnbGVJbmxpbmVGb3JtYXQsXHJcbiAgICByZXBsYWNlUmFuZ2VBbmRNb3ZlQ2FyZXQsXHJcbn0gZnJvbSAnLi4vLi4vLi4vZWRpdG9yL29wZXJhdGlvbnMnO1xyXG5pbXBvcnQge2dldENhcmV0T2Zmc2V0QW5kVGV4dCwgZ2V0UmFuZ2VGb3JTZWxlY3Rpb259IGZyb20gJy4uLy4uLy4uL2VkaXRvci9kb20nO1xyXG5pbXBvcnQgQXV0b2NvbXBsZXRlLCB7Z2VuZXJhdGVDb21wbGV0aW9uRG9tSWR9IGZyb20gJy4uL3Jvb21zL0F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7YXV0b0NvbXBsZXRlQ3JlYXRvcn0gZnJvbSAnLi4vLi4vLi4vZWRpdG9yL3BhcnRzJztcclxuaW1wb3J0IHtwYXJzZVBsYWluVGV4dE1lc3NhZ2V9IGZyb20gJy4uLy4uLy4uL2VkaXRvci9kZXNlcmlhbGl6ZSc7XHJcbmltcG9ydCB7cmVuZGVyTW9kZWx9IGZyb20gJy4uLy4uLy4uL2VkaXRvci9yZW5kZXInO1xyXG5pbXBvcnQge1Jvb219IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgVHlwaW5nU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3N0b3Jlcy9UeXBpbmdTdG9yZVwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgRU1PVElDT05fUkVHRVggZnJvbSAnZW1vamliYXNlLXJlZ2V4L2Vtb3RpY29uJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHtLZXl9IGZyb20gXCIuLi8uLi8uLi9LZXlib2FyZFwiO1xyXG5pbXBvcnQge0VNT1RJQ09OX1RPX0VNT0pJfSBmcm9tIFwiLi4vLi4vLi4vZW1vamlcIjtcclxuXHJcbmNvbnN0IFJFR0VYX0VNT1RJQ09OX1dISVRFU1BBQ0UgPSBuZXcgUmVnRXhwKCcoPzpefFxcXFxzKSgnICsgRU1PVElDT05fUkVHRVguc291cmNlICsgJylcXFxccyQnKTtcclxuXHJcbmNvbnN0IElTX01BQyA9IG5hdmlnYXRvci5wbGF0Zm9ybS5pbmRleE9mKFwiTWFjXCIpICE9PSAtMTtcclxuXHJcbmZ1bmN0aW9uIGN0cmxTaG9ydGN1dExhYmVsKGtleSkge1xyXG4gICAgcmV0dXJuIChJU19NQUMgPyBcIuKMmFwiIDogXCJDdHJsXCIpICsgXCIrXCIgKyBrZXk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNsb25lU2VsZWN0aW9uKHNlbGVjdGlvbikge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBhbmNob3JOb2RlOiBzZWxlY3Rpb24uYW5jaG9yTm9kZSxcclxuICAgICAgICBhbmNob3JPZmZzZXQ6IHNlbGVjdGlvbi5hbmNob3JPZmZzZXQsXHJcbiAgICAgICAgZm9jdXNOb2RlOiBzZWxlY3Rpb24uZm9jdXNOb2RlLFxyXG4gICAgICAgIGZvY3VzT2Zmc2V0OiBzZWxlY3Rpb24uZm9jdXNPZmZzZXQsXHJcbiAgICAgICAgaXNDb2xsYXBzZWQ6IHNlbGVjdGlvbi5pc0NvbGxhcHNlZCxcclxuICAgICAgICByYW5nZUNvdW50OiBzZWxlY3Rpb24ucmFuZ2VDb3VudCxcclxuICAgICAgICB0eXBlOiBzZWxlY3Rpb24udHlwZSxcclxuICAgIH07XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNlbGVjdGlvbkVxdWFscyhhOiBTZWxlY3Rpb24sIGI6IFNlbGVjdGlvbik6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIGEuYW5jaG9yTm9kZSA9PT0gYi5hbmNob3JOb2RlICYmXHJcbiAgICAgICAgYS5hbmNob3JPZmZzZXQgPT09IGIuYW5jaG9yT2Zmc2V0ICYmXHJcbiAgICAgICAgYS5mb2N1c05vZGUgPT09IGIuZm9jdXNOb2RlICYmXHJcbiAgICAgICAgYS5mb2N1c09mZnNldCA9PT0gYi5mb2N1c09mZnNldCAmJlxyXG4gICAgICAgIGEuaXNDb2xsYXBzZWQgPT09IGIuaXNDb2xsYXBzZWQgJiZcclxuICAgICAgICBhLnJhbmdlQ291bnQgPT09IGIucmFuZ2VDb3VudCAmJlxyXG4gICAgICAgIGEudHlwZSA9PT0gYi50eXBlO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCYXNpY01lc3NhZ2VFZGl0b3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICAgICAgbW9kZWw6IFByb3BUeXBlcy5pbnN0YW5jZU9mKEVkaXRvck1vZGVsKS5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHJvb206IFByb3BUeXBlcy5pbnN0YW5jZU9mKFJvb20pLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgbGFiZWw6IFByb3BUeXBlcy5zdHJpbmcsICAgIC8vIHRoZSBhcmlhIGxhYmVsXHJcbiAgICAgICAgaW5pdGlhbENhcmV0OiBQcm9wVHlwZXMub2JqZWN0LCAvLyBTZWUgRG9jdW1lbnRQb3NpdGlvbiBpbiBlZGl0b3IvbW9kZWwuanNcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgYXV0b0NvbXBsZXRlOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9hdXRvY29tcGxldGVSZWYgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2Zvcm1hdEJhclJlZiA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fbW9kaWZpZWRGbGFnID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5faXNJTUVDb21wb3NpbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9oYXNUZXh0U2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9lbW90aWNvblNldHRpbmdIYW5kbGUgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBNb3ZlIGludG8gYmV0dGVyIGxpZmVjeWNsZSBwb3NpdGlvblxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxVcGRhdGUocHJldlByb3BzKSB7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgY2FtZWxjYXNlXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucGxhY2Vob2xkZXIgIT09IHByZXZQcm9wcy5wbGFjZWhvbGRlciAmJiB0aGlzLnByb3BzLnBsYWNlaG9sZGVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtpc0VtcHR5fSA9IHRoaXMucHJvcHMubW9kZWw7XHJcbiAgICAgICAgICAgIGlmIChpc0VtcHR5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zaG93UGxhY2Vob2xkZXIoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2hpZGVQbGFjZWhvbGRlcigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9yZXBsYWNlRW1vdGljb24gPSAoY2FyZXRQb3NpdGlvbiwgaW5wdXRUeXBlLCBkaWZmKSA9PiB7XHJcbiAgICAgICAgY29uc3Qge21vZGVsfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgY29uc3QgcmFuZ2UgPSBtb2RlbC5zdGFydFJhbmdlKGNhcmV0UG9zaXRpb24pO1xyXG4gICAgICAgIC8vIGV4cGFuZCByYW5nZSBtYXggOCBjaGFyYWN0ZXJzIGJhY2t3YXJkcyBmcm9tIGNhcmV0UG9zaXRpb24sXHJcbiAgICAgICAgLy8gYXMgYSBzcGFjZSB0byBsb29rIGZvciBhbiBlbW90aWNvblxyXG4gICAgICAgIGxldCBuID0gODtcclxuICAgICAgICByYW5nZS5leHBhbmRCYWNrd2FyZHNXaGlsZSgoaW5kZXgsIG9mZnNldCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJ0ID0gbW9kZWwucGFydHNbaW5kZXhdO1xyXG4gICAgICAgICAgICBuIC09IDE7XHJcbiAgICAgICAgICAgIHJldHVybiBuID49IDAgJiYgKHBhcnQudHlwZSA9PT0gXCJwbGFpblwiIHx8IHBhcnQudHlwZSA9PT0gXCJwaWxsLWNhbmRpZGF0ZVwiKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zdCBlbW90aWNvbk1hdGNoID0gUkVHRVhfRU1PVElDT05fV0hJVEVTUEFDRS5leGVjKHJhbmdlLnRleHQpO1xyXG4gICAgICAgIGlmIChlbW90aWNvbk1hdGNoKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHF1ZXJ5ID0gZW1vdGljb25NYXRjaFsxXS5yZXBsYWNlKFwiLVwiLCBcIlwiKTtcclxuICAgICAgICAgICAgLy8gdHJ5IGJvdGggZXhhY3QgbWF0Y2ggYW5kIGxvd2VyLWNhc2UsIHRoaXMgbWVhbnMgdGhhdCB4ZCB3b24ndCBtYXRjaCB4RCBidXQgOlAgd2lsbCBtYXRjaCA6cFxyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gRU1PVElDT05fVE9fRU1PSkkuZ2V0KHF1ZXJ5KSB8fCBFTU9USUNPTl9UT19FTU9KSS5nZXQocXVlcnkudG9Mb3dlckNhc2UoKSk7XHJcblxyXG4gICAgICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qge3BhcnRDcmVhdG9yfSA9IG1vZGVsO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaGFzUHJlY2VkaW5nU3BhY2UgPSBlbW90aWNvbk1hdGNoWzBdWzBdID09PSBcIiBcIjtcclxuICAgICAgICAgICAgICAgIC8vIHdlIG5lZWQgdGhlIHJhbmdlIHRvIG9ubHkgY29tcHJpc2Ugb2YgdGhlIGVtb3RpY29uXHJcbiAgICAgICAgICAgICAgICAvLyBiZWNhdXNlIHdlJ2xsIHJlcGxhY2UgdGhlIHdob2xlIHJhbmdlIHdpdGggYW4gZW1vamksXHJcbiAgICAgICAgICAgICAgICAvLyBzbyBtb3ZlIHRoZSBzdGFydCBmb3J3YXJkIHRvIHRoZSBzdGFydCBvZiB0aGUgZW1vdGljb24uXHJcbiAgICAgICAgICAgICAgICAvLyBUYWtlICsgMSBiZWNhdXNlIGluZGV4IGlzIHJlcG9ydGVkIHdpdGhvdXQgdGhlIHBvc3NpYmxlIHByZWNlZGluZyBzcGFjZS5cclxuICAgICAgICAgICAgICAgIHJhbmdlLm1vdmVTdGFydChlbW90aWNvbk1hdGNoLmluZGV4ICsgKGhhc1ByZWNlZGluZ1NwYWNlID8gMSA6IDApKTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMgcmV0dXJucyB0aGUgYW1vdW50IG9mIGFkZGVkL3JlbW92ZWQgY2hhcmFjdGVycyBkdXJpbmcgdGhlIHJlcGxhY2VcclxuICAgICAgICAgICAgICAgIC8vIHNvIHRoZSBjYXJldCBwb3NpdGlvbiBjYW4gYmUgYWRqdXN0ZWQuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmFuZ2UucmVwbGFjZShbcGFydENyZWF0b3IucGxhaW4oZGF0YS51bmljb2RlICsgXCIgXCIpXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3VwZGF0ZUVkaXRvclN0YXRlID0gKHNlbGVjdGlvbiwgaW5wdXRUeXBlLCBkaWZmKSA9PiB7XHJcbiAgICAgICAgcmVuZGVyTW9kZWwodGhpcy5fZWRpdG9yUmVmLCB0aGlzLnByb3BzLm1vZGVsKTtcclxuICAgICAgICBpZiAoc2VsZWN0aW9uKSB7IC8vIHNldCB0aGUgY2FyZXQvc2VsZWN0aW9uXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBzZXRTZWxlY3Rpb24odGhpcy5fZWRpdG9yUmVmLCB0aGlzLnByb3BzLm1vZGVsLCBzZWxlY3Rpb24pO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBpZiBjYXJldCBzZWxlY3Rpb24gaXMgYSByYW5nZSwgdGFrZSB0aGUgZW5kIHBvc2l0aW9uXHJcbiAgICAgICAgICAgIGNvbnN0IHBvc2l0aW9uID0gc2VsZWN0aW9uLmVuZCB8fCBzZWxlY3Rpb247XHJcbiAgICAgICAgICAgIHRoaXMuX3NldExhc3RDYXJldEZyb21Qb3NpdGlvbihwb3NpdGlvbik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHtpc0VtcHR5fSA9IHRoaXMucHJvcHMubW9kZWw7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucGxhY2Vob2xkZXIpIHtcclxuICAgICAgICAgICAgaWYgKGlzRW1wdHkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3Nob3dQbGFjZWhvbGRlcigpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5faGlkZVBsYWNlaG9sZGVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRW1wdHkpIHtcclxuICAgICAgICAgICAgdGhpcy5fZm9ybWF0QmFyUmVmLmhpZGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YXV0b0NvbXBsZXRlOiB0aGlzLnByb3BzLm1vZGVsLmF1dG9Db21wbGV0ZX0pO1xyXG4gICAgICAgIHRoaXMuaGlzdG9yeU1hbmFnZXIudHJ5UHVzaCh0aGlzLnByb3BzLm1vZGVsLCBzZWxlY3Rpb24sIGlucHV0VHlwZSwgZGlmZik7XHJcbiAgICAgICAgVHlwaW5nU3RvcmUuc2hhcmVkSW5zdGFuY2UoKS5zZXRTZWxmVHlwaW5nKHRoaXMucHJvcHMucm9vbS5yb29tSWQsICF0aGlzLnByb3BzLm1vZGVsLmlzRW1wdHkpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkNoYW5nZSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9zaG93UGxhY2Vob2xkZXIoKSB7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLnN0eWxlLnNldFByb3BlcnR5KFwiLS1wbGFjZWhvbGRlclwiLCBgJyR7dGhpcy5wcm9wcy5wbGFjZWhvbGRlcn0nYCk7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmNsYXNzTGlzdC5hZGQoXCJteF9CYXNpY01lc3NhZ2VDb21wb3Nlcl9pbnB1dEVtcHR5XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIF9oaWRlUGxhY2Vob2xkZXIoKSB7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmNsYXNzTGlzdC5yZW1vdmUoXCJteF9CYXNpY01lc3NhZ2VDb21wb3Nlcl9pbnB1dEVtcHR5XCIpO1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5zdHlsZS5yZW1vdmVQcm9wZXJ0eShcIi0tcGxhY2Vob2xkZXJcIik7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ29tcG9zaXRpb25TdGFydCA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX2lzSU1FQ29tcG9zaW5nID0gdHJ1ZTtcclxuICAgICAgICAvLyBldmVuIGlmIHRoZSBtb2RlbCBpcyBlbXB0eSwgdGhlIGNvbXBvc2l0aW9uIHRleHQgc2hvdWxkbid0IGJlIG1peGVkIHdpdGggdGhlIHBsYWNlaG9sZGVyXHJcbiAgICAgICAgdGhpcy5faGlkZVBsYWNlaG9sZGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ29tcG9zaXRpb25FbmQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICB0aGlzLl9pc0lNRUNvbXBvc2luZyA9IGZhbHNlO1xyXG4gICAgICAgIC8vIHNvbWUgYnJvd3NlcnMgKENocm9tZSkgZG9uJ3QgZmlyZSBhbiBpbnB1dCBldmVudCBhZnRlciBlbmRpbmcgYSBjb21wb3NpdGlvbixcclxuICAgICAgICAvLyBzbyB0cmlnZ2VyIGEgbW9kZWwgdXBkYXRlIGFmdGVyIHRoZSBjb21wb3NpdGlvbiBpcyBkb25lIGJ5IGNhbGxpbmcgdGhlIGlucHV0IGhhbmRsZXIuXHJcblxyXG4gICAgICAgIC8vIGhvd2V2ZXIsIG1vZGlmeWluZyB0aGUgRE9NIChjYXVzZWQgYnkgdGhlIGVkaXRvciBtb2RlbCB1cGRhdGUpIGZyb20gdGhlIGNvbXBvc2l0aW9uZW5kIGhhbmRsZXIgc2VlbXNcclxuICAgICAgICAvLyB0byBjb25mdXNlIHRoZSBJTUUgaW4gQ2hyb21lLCBsaWtlbHkgY2F1c2luZyBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8xMDkxMyAsXHJcbiAgICAgICAgLy8gc28gd2UgZG8gaXQgYXN5bmNcclxuXHJcbiAgICAgICAgLy8gaG93ZXZlciwgZG9pbmcgdGhpcyBhc3luYyBzZWVtcyB0byBicmVhayB0aGluZ3MgaW4gU2FmYXJpIGZvciBzb21lIHJlYXNvbiwgc28gYnJvd3NlciBzbmlmZi5cclxuXHJcbiAgICAgICAgY29uc3QgdWEgPSBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgY29uc3QgaXNTYWZhcmkgPSB1YS5pbmNsdWRlcygnc2FmYXJpLycpICYmICF1YS5pbmNsdWRlcygnY2hyb21lLycpO1xyXG5cclxuICAgICAgICBpZiAoaXNTYWZhcmkpIHtcclxuICAgICAgICAgICAgdGhpcy5fb25JbnB1dCh7aW5wdXRUeXBlOiBcImluc2VydENvbXBvc2l0aW9uVGV4dFwifSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgUHJvbWlzZS5yZXNvbHZlKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9vbklucHV0KHtpbnB1dFR5cGU6IFwiaW5zZXJ0Q29tcG9zaXRpb25UZXh0XCJ9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlzQ29tcG9zaW5nKGV2ZW50KSB7XHJcbiAgICAgICAgLy8gY2hlY2tpbmcgdGhlIGV2ZW50LmlzQ29tcG9zaW5nIGZsYWcganVzdCBpbiBjYXNlIGFueSBicm93c2VyIG91dCB0aGVyZVxyXG4gICAgICAgIC8vIGVtaXRzIGV2ZW50cyByZWxhdGVkIHRvIHRoZSBjb21wb3NpdGlvbiBhZnRlciBjb21wb3NpdGlvbmVuZFxyXG4gICAgICAgIC8vIGhhcyBiZWVuIGZpcmVkXHJcbiAgICAgICAgcmV0dXJuICEhKHRoaXMuX2lzSU1FQ29tcG9zaW5nIHx8IChldmVudC5uYXRpdmVFdmVudCAmJiBldmVudC5uYXRpdmVFdmVudC5pc0NvbXBvc2luZykpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkN1dENvcHkgPSAoZXZlbnQsIHR5cGUpID0+IHtcclxuICAgICAgICBjb25zdCBzZWxlY3Rpb24gPSBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKTtcclxuICAgICAgICBjb25zdCB0ZXh0ID0gc2VsZWN0aW9uLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgaWYgKHRleHQpIHtcclxuICAgICAgICAgICAgY29uc3Qge21vZGVsfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgICAgIGNvbnN0IHJhbmdlID0gZ2V0UmFuZ2VGb3JTZWxlY3Rpb24odGhpcy5fZWRpdG9yUmVmLCBtb2RlbCwgc2VsZWN0aW9uKTtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRQYXJ0cyA9IHJhbmdlLnBhcnRzLm1hcChwID0+IHAuc2VyaWFsaXplKCkpO1xyXG4gICAgICAgICAgICBldmVudC5jbGlwYm9hcmREYXRhLnNldERhdGEoXCJhcHBsaWNhdGlvbi94LXJpb3QtY29tcG9zZXJcIiwgSlNPTi5zdHJpbmdpZnkoc2VsZWN0ZWRQYXJ0cykpO1xyXG4gICAgICAgICAgICBldmVudC5jbGlwYm9hcmREYXRhLnNldERhdGEoXCJ0ZXh0L3BsYWluXCIsIHRleHQpOyAvLyBzbyBwbGFpbiBjb3B5L3Bhc3RlIHdvcmtzXHJcbiAgICAgICAgICAgIGlmICh0eXBlID09PSBcImN1dFwiKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBSZW1vdmUgdGhlIHRleHQsIHVwZGF0aW5nIHRoZSBtb2RlbCBhcyBhcHByb3ByaWF0ZVxyXG4gICAgICAgICAgICAgICAgdGhpcy5fbW9kaWZpZWRGbGFnID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHJlcGxhY2VSYW5nZUFuZE1vdmVDYXJldChyYW5nZSwgW10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbkNvcHkgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICB0aGlzLl9vbkN1dENvcHkoZXZlbnQsIFwiY29weVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25DdXQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICB0aGlzLl9vbkN1dENvcHkoZXZlbnQsIFwiY3V0XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblBhc3RlID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgY29uc3Qge21vZGVsfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgY29uc3Qge3BhcnRDcmVhdG9yfSA9IG1vZGVsO1xyXG4gICAgICAgIGNvbnN0IHBhcnRzVGV4dCA9IGV2ZW50LmNsaXBib2FyZERhdGEuZ2V0RGF0YShcImFwcGxpY2F0aW9uL3gtcmlvdC1jb21wb3NlclwiKTtcclxuICAgICAgICBsZXQgcGFydHM7XHJcbiAgICAgICAgaWYgKHBhcnRzVGV4dCkge1xyXG4gICAgICAgICAgICBjb25zdCBzZXJpYWxpemVkVGV4dFBhcnRzID0gSlNPTi5wYXJzZShwYXJ0c1RleHQpO1xyXG4gICAgICAgICAgICBjb25zdCBkZXNlcmlhbGl6ZWRQYXJ0cyA9IHNlcmlhbGl6ZWRUZXh0UGFydHMubWFwKHAgPT4gcGFydENyZWF0b3IuZGVzZXJpYWxpemVQYXJ0KHApKTtcclxuICAgICAgICAgICAgcGFydHMgPSBkZXNlcmlhbGl6ZWRQYXJ0cztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCB0ZXh0ID0gZXZlbnQuY2xpcGJvYXJkRGF0YS5nZXREYXRhKFwidGV4dC9wbGFpblwiKTtcclxuICAgICAgICAgICAgcGFydHMgPSBwYXJzZVBsYWluVGV4dE1lc3NhZ2UodGV4dCwgcGFydENyZWF0b3IpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9tb2RpZmllZEZsYWcgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHJhbmdlID0gZ2V0UmFuZ2VGb3JTZWxlY3Rpb24odGhpcy5fZWRpdG9yUmVmLCBtb2RlbCwgZG9jdW1lbnQuZ2V0U2VsZWN0aW9uKCkpO1xyXG4gICAgICAgIHJlcGxhY2VSYW5nZUFuZE1vdmVDYXJldChyYW5nZSwgcGFydHMpO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uSW5wdXQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICAvLyBpZ25vcmUgYW55IGlucHV0IHdoaWxlIGRvaW5nIElNRSBjb21wb3NpdGlvbnNcclxuICAgICAgICBpZiAodGhpcy5faXNJTUVDb21wb3NpbmcpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9tb2RpZmllZEZsYWcgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHNlbCA9IGRvY3VtZW50LmdldFNlbGVjdGlvbigpO1xyXG4gICAgICAgIGNvbnN0IHtjYXJldCwgdGV4dH0gPSBnZXRDYXJldE9mZnNldEFuZFRleHQodGhpcy5fZWRpdG9yUmVmLCBzZWwpO1xyXG4gICAgICAgIHRoaXMucHJvcHMubW9kZWwudXBkYXRlKHRleHQsIGV2ZW50LmlucHV0VHlwZSwgY2FyZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIF9pbnNlcnRUZXh0KHRleHRUb0luc2VydCwgaW5wdXRUeXBlID0gXCJpbnNlcnRUZXh0XCIpIHtcclxuICAgICAgICBjb25zdCBzZWwgPSBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKTtcclxuICAgICAgICBjb25zdCB7Y2FyZXQsIHRleHR9ID0gZ2V0Q2FyZXRPZmZzZXRBbmRUZXh0KHRoaXMuX2VkaXRvclJlZiwgc2VsKTtcclxuICAgICAgICBjb25zdCBuZXdUZXh0ID0gdGV4dC5zdWJzdHIoMCwgY2FyZXQub2Zmc2V0KSArIHRleHRUb0luc2VydCArIHRleHQuc3Vic3RyKGNhcmV0Lm9mZnNldCk7XHJcbiAgICAgICAgY2FyZXQub2Zmc2V0ICs9IHRleHRUb0luc2VydC5sZW5ndGg7XHJcbiAgICAgICAgdGhpcy5fbW9kaWZpZWRGbGFnID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnByb3BzLm1vZGVsLnVwZGF0ZShuZXdUZXh0LCBpbnB1dFR5cGUsIGNhcmV0KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyB0aGlzIGlzIHVzZWQgbGF0ZXIgdG8gc2VlIGlmIHdlIG5lZWQgdG8gcmVjYWxjdWxhdGUgdGhlIGNhcmV0XHJcbiAgICAvLyBvbiBzZWxlY3Rpb25jaGFuZ2UuIElmIGl0IGlzIGp1c3QgYSBjb25zZXF1ZW5jZSBvZiB0eXBpbmdcclxuICAgIC8vIHdlIGRvbid0IG5lZWQgdG8uIEJ1dCBpZiB0aGUgdXNlciBpcyBuYXZpZ2F0aW5nIHRoZSBjYXJldCB3aXRob3V0IGlucHV0XHJcbiAgICAvLyB3ZSBuZWVkIHRvIHJlY2FsY3VsYXRlIGl0LCB0byBiZSBhYmxlIHRvIGtub3cgd2hlcmUgdG8gaW5zZXJ0IGNvbnRlbnQgYWZ0ZXJcclxuICAgIC8vIGxvc2luZyBmb2N1c1xyXG4gICAgX3NldExhc3RDYXJldEZyb21Qb3NpdGlvbihwb3NpdGlvbikge1xyXG4gICAgICAgIGNvbnN0IHttb2RlbH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIHRoaXMuX2lzQ2FyZXRBdEVuZCA9IHBvc2l0aW9uLmlzQXRFbmQobW9kZWwpO1xyXG4gICAgICAgIHRoaXMuX2xhc3RDYXJldCA9IHBvc2l0aW9uLmFzT2Zmc2V0KG1vZGVsKTtcclxuICAgICAgICB0aGlzLl9sYXN0U2VsZWN0aW9uID0gY2xvbmVTZWxlY3Rpb24oZG9jdW1lbnQuZ2V0U2VsZWN0aW9uKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZWZyZXNoTGFzdENhcmV0SWZOZWVkZWQoKSB7XHJcbiAgICAgICAgLy8gWFhYOiBuZWVkZWQgd2hlbiBnb2luZyB1cCBhbmQgZG93biBpbiBlZGl0aW5nIG1lc3NhZ2VzIC4uLiBub3Qgc3VyZSB3aHkgeWV0XHJcbiAgICAgICAgLy8gYmVjYXVzZSB0aGUgZWRpdG9ycyBzaG91bGQgc3RvcCBkb2luZyB0aGlzIHdoZW4gd2hlbiBibHVycmVkIC4uLlxyXG4gICAgICAgIC8vIG1heWJlIGl0J3Mgb24gZm9jdXMgYW5kIHRoZSBfZWRpdG9yUmVmIGlzbid0IGF2YWlsYWJsZSB5ZXQgb3Igc29tZXRoaW5nLlxyXG4gICAgICAgIGlmICghdGhpcy5fZWRpdG9yUmVmKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0aW9uID0gZG9jdW1lbnQuZ2V0U2VsZWN0aW9uKCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9sYXN0U2VsZWN0aW9uIHx8ICFzZWxlY3Rpb25FcXVhbHModGhpcy5fbGFzdFNlbGVjdGlvbiwgc2VsZWN0aW9uKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9sYXN0U2VsZWN0aW9uID0gY2xvbmVTZWxlY3Rpb24oc2VsZWN0aW9uKTtcclxuICAgICAgICAgICAgY29uc3Qge2NhcmV0LCB0ZXh0fSA9IGdldENhcmV0T2Zmc2V0QW5kVGV4dCh0aGlzLl9lZGl0b3JSZWYsIHNlbGVjdGlvbik7XHJcbiAgICAgICAgICAgIHRoaXMuX2xhc3RDYXJldCA9IGNhcmV0O1xyXG4gICAgICAgICAgICB0aGlzLl9pc0NhcmV0QXRFbmQgPSBjYXJldC5vZmZzZXQgPT09IHRleHQubGVuZ3RoO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbGFzdENhcmV0O1xyXG4gICAgfVxyXG5cclxuICAgIGNsZWFyVW5kb0hpc3RvcnkoKSB7XHJcbiAgICAgICAgdGhpcy5oaXN0b3J5TWFuYWdlci5jbGVhcigpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENhcmV0KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9sYXN0Q2FyZXQ7XHJcbiAgICB9XHJcblxyXG4gICAgaXNTZWxlY3Rpb25Db2xsYXBzZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLl9sYXN0U2VsZWN0aW9uIHx8IHRoaXMuX2xhc3RTZWxlY3Rpb24uaXNDb2xsYXBzZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgaXNDYXJldEF0U3RhcnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2FyZXQoKS5vZmZzZXQgPT09IDA7XHJcbiAgICB9XHJcblxyXG4gICAgaXNDYXJldEF0RW5kKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pc0NhcmV0QXRFbmQ7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQmx1ciA9ICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2VsZWN0aW9uY2hhbmdlXCIsIHRoaXMuX29uU2VsZWN0aW9uQ2hhbmdlKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25Gb2N1cyA9ICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwic2VsZWN0aW9uY2hhbmdlXCIsIHRoaXMuX29uU2VsZWN0aW9uQ2hhbmdlKTtcclxuICAgICAgICAvLyBmb3JjZSB0byByZWNhbGN1bGF0ZVxyXG4gICAgICAgIHRoaXMuX2xhc3RTZWxlY3Rpb24gPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3JlZnJlc2hMYXN0Q2FyZXRJZk5lZWRlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblNlbGVjdGlvbkNoYW5nZSA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLl9yZWZyZXNoTGFzdENhcmV0SWZOZWVkZWQoKTtcclxuICAgICAgICBjb25zdCBzZWxlY3Rpb24gPSBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKTtcclxuICAgICAgICBpZiAodGhpcy5faGFzVGV4dFNlbGVjdGVkICYmIHNlbGVjdGlvbi5pc0NvbGxhcHNlZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9oYXNUZXh0U2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2Zvcm1hdEJhclJlZikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZm9ybWF0QmFyUmVmLmhpZGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoIXNlbGVjdGlvbi5pc0NvbGxhcHNlZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9oYXNUZXh0U2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fZm9ybWF0QmFyUmVmKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3Rpb25SZWN0ID0gc2VsZWN0aW9uLmdldFJhbmdlQXQoMCkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9mb3JtYXRCYXJSZWYuc2hvd0F0KHNlbGVjdGlvblJlY3QpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbktleURvd24gPSAoZXZlbnQpID0+IHtcclxuICAgICAgICBjb25zdCBtb2RlbCA9IHRoaXMucHJvcHMubW9kZWw7XHJcbiAgICAgICAgY29uc3QgbW9kS2V5ID0gSVNfTUFDID8gZXZlbnQubWV0YUtleSA6IGV2ZW50LmN0cmxLZXk7XHJcbiAgICAgICAgbGV0IGhhbmRsZWQgPSBmYWxzZTtcclxuICAgICAgICAvLyBmb3JtYXQgYm9sZFxyXG4gICAgICAgIGlmIChtb2RLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuQikge1xyXG4gICAgICAgICAgICB0aGlzLl9vbkZvcm1hdEFjdGlvbihcImJvbGRcIik7XHJcbiAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIGZvcm1hdCBpdGFsaWNzXHJcbiAgICAgICAgfSBlbHNlIGlmIChtb2RLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuSSkge1xyXG4gICAgICAgICAgICB0aGlzLl9vbkZvcm1hdEFjdGlvbihcIml0YWxpY3NcIik7XHJcbiAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIGZvcm1hdCBxdW90ZVxyXG4gICAgICAgIH0gZWxzZSBpZiAobW9kS2V5ICYmIGV2ZW50LmtleSA9PT0gS2V5LkdSRUFURVJfVEhBTikge1xyXG4gICAgICAgICAgICB0aGlzLl9vbkZvcm1hdEFjdGlvbihcInF1b3RlXCIpO1xyXG4gICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAvLyByZWRvXHJcbiAgICAgICAgfSBlbHNlIGlmICgoIUlTX01BQyAmJiBtb2RLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuWSkgfHxcclxuICAgICAgICAgICAgICAgICAgKElTX01BQyAmJiBtb2RLZXkgJiYgZXZlbnQuc2hpZnRLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuWikpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaGlzdG9yeU1hbmFnZXIuY2FuUmVkbygpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB7cGFydHMsIGNhcmV0fSA9IHRoaXMuaGlzdG9yeU1hbmFnZXIucmVkbygpO1xyXG4gICAgICAgICAgICAgICAgLy8gcGFzcyBtYXRjaGluZyBpbnB1dFR5cGUgc28gaGlzdG9yeU1hbmFnZXIgZG9lc24ndCBwdXNoIGVjaG9cclxuICAgICAgICAgICAgICAgIC8vIHdoZW4gaW52b2tlZCBmcm9tIHJlcmVuZGVyIGNhbGxiYWNrLlxyXG4gICAgICAgICAgICAgICAgbW9kZWwucmVzZXQocGFydHMsIGNhcmV0LCBcImhpc3RvcnlSZWRvXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIHVuZG9cclxuICAgICAgICB9IGVsc2UgaWYgKG1vZEtleSAmJiBldmVudC5rZXkgPT09IEtleS5aKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmhpc3RvcnlNYW5hZ2VyLmNhblVuZG8oKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qge3BhcnRzLCBjYXJldH0gPSB0aGlzLmhpc3RvcnlNYW5hZ2VyLnVuZG8odGhpcy5wcm9wcy5tb2RlbCk7XHJcbiAgICAgICAgICAgICAgICAvLyBwYXNzIG1hdGNoaW5nIGlucHV0VHlwZSBzbyBoaXN0b3J5TWFuYWdlciBkb2Vzbid0IHB1c2ggZWNob1xyXG4gICAgICAgICAgICAgICAgLy8gd2hlbiBpbnZva2VkIGZyb20gcmVyZW5kZXIgY2FsbGJhY2suXHJcbiAgICAgICAgICAgICAgICBtb2RlbC5yZXNldChwYXJ0cywgY2FyZXQsIFwiaGlzdG9yeVVuZG9cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgLy8gaW5zZXJ0IG5ld2xpbmUgb24gU2hpZnQrRW50ZXJcclxuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gS2V5LkVOVEVSICYmIChldmVudC5zaGlmdEtleSB8fCAoSVNfTUFDICYmIGV2ZW50LmFsdEtleSkpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2luc2VydFRleHQoXCJcXG5cIik7XHJcbiAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIG1vdmUgc2VsZWN0aW9uIHRvIHN0YXJ0IG9mIGNvbXBvc2VyXHJcbiAgICAgICAgfSBlbHNlIGlmIChtb2RLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuSE9NRSAmJiAhZXZlbnQuc2hpZnRLZXkpIHtcclxuICAgICAgICAgICAgc2V0U2VsZWN0aW9uKHRoaXMuX2VkaXRvclJlZiwgbW9kZWwsIHtcclxuICAgICAgICAgICAgICAgIGluZGV4OiAwLFxyXG4gICAgICAgICAgICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgLy8gbW92ZSBzZWxlY3Rpb24gdG8gZW5kIG9mIGNvbXBvc2VyXHJcbiAgICAgICAgfSBlbHNlIGlmIChtb2RLZXkgJiYgZXZlbnQua2V5ID09PSBLZXkuRU5EICYmICFldmVudC5zaGlmdEtleSkge1xyXG4gICAgICAgICAgICBzZXRTZWxlY3Rpb24odGhpcy5fZWRpdG9yUmVmLCBtb2RlbCwge1xyXG4gICAgICAgICAgICAgICAgaW5kZXg6IG1vZGVsLnBhcnRzLmxlbmd0aCAtIDEsXHJcbiAgICAgICAgICAgICAgICBvZmZzZXQ6IG1vZGVsLnBhcnRzW21vZGVsLnBhcnRzLmxlbmd0aCAtIDFdLnRleHQubGVuZ3RoLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgLy8gYXV0b2NvbXBsZXRlIG9yIGVudGVyIHRvIHNlbmQgYmVsb3cgc2hvdWxkbid0IGhhdmUgYW55IG1vZGlmaWVyIGtleXMgcHJlc3NlZC5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBtZXRhT3JBbHRQcmVzc2VkID0gZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXk7XHJcbiAgICAgICAgICAgIGNvbnN0IG1vZGlmaWVyUHJlc3NlZCA9IG1ldGFPckFsdFByZXNzZWQgfHwgZXZlbnQuc2hpZnRLZXk7XHJcbiAgICAgICAgICAgIGlmIChtb2RlbC5hdXRvQ29tcGxldGUgJiYgbW9kZWwuYXV0b0NvbXBsZXRlLmhhc0NvbXBsZXRpb25zKCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGF1dG9Db21wbGV0ZSA9IG1vZGVsLmF1dG9Db21wbGV0ZTtcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoZXZlbnQua2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBLZXkuQVJST1dfVVA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbW9kaWZpZXJQcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGUub25VcEFycm93KGV2ZW50KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgS2V5LkFSUk9XX0RPV046XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbW9kaWZpZXJQcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGUub25Eb3duQXJyb3coZXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBLZXkuVEFCOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIW1ldGFPckFsdFByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Db21wbGV0ZS5vblRhYihldmVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIEtleS5FU0NBUEU6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbW9kaWZpZXJQcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGUub25Fc2NhcGUoZXZlbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuOyAvLyBkb24ndCBwcmV2ZW50RGVmYXVsdCBvbiBhbnl0aGluZyBlbHNlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5ID09PSBLZXkuVEFCKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90YWJDb21wbGV0ZU5hbWUoKTtcclxuICAgICAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gS2V5LkJBQ0tTUEFDRSB8fCBldmVudC5rZXkgPT09IEtleS5ERUxFVEUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Zvcm1hdEJhclJlZi5oaWRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGhhbmRsZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF90YWJDb21wbGV0ZU5hbWUoKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgbmV3IFByb21pc2UocmVzb2x2ZSA9PiB0aGlzLnNldFN0YXRlKHtzaG93VmlzdWFsQmVsbDogZmFsc2V9LCByZXNvbHZlKSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHttb2RlbH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgICAgICBjb25zdCBjYXJldCA9IHRoaXMuZ2V0Q2FyZXQoKTtcclxuICAgICAgICAgICAgY29uc3QgcG9zaXRpb24gPSBtb2RlbC5wb3NpdGlvbkZvck9mZnNldChjYXJldC5vZmZzZXQsIGNhcmV0LmF0Tm9kZUVuZCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJhbmdlID0gbW9kZWwuc3RhcnRSYW5nZShwb3NpdGlvbik7XHJcbiAgICAgICAgICAgIHJhbmdlLmV4cGFuZEJhY2t3YXJkc1doaWxlKChpbmRleCwgb2Zmc2V0LCBwYXJ0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcGFydC50ZXh0W29mZnNldF0gIT09IFwiIFwiICYmIChcclxuICAgICAgICAgICAgICAgICAgICBwYXJ0LnR5cGUgPT09IFwicGxhaW5cIiB8fFxyXG4gICAgICAgICAgICAgICAgICAgIHBhcnQudHlwZSA9PT0gXCJwaWxsLWNhbmRpZGF0ZVwiIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgcGFydC50eXBlID09PSBcImNvbW1hbmRcIlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHtwYXJ0Q3JlYXRvcn0gPSBtb2RlbDtcclxuICAgICAgICAgICAgLy8gYXdhaXQgZm9yIGF1dG8tY29tcGxldGUgdG8gYmUgb3BlblxyXG4gICAgICAgICAgICBhd2FpdCBtb2RlbC50cmFuc2Zvcm0oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYWRkZWRMZW4gPSByYW5nZS5yZXBsYWNlKFtwYXJ0Q3JlYXRvci5waWxsQ2FuZGlkYXRlKHJhbmdlLnRleHQpXSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9kZWwucG9zaXRpb25Gb3JPZmZzZXQoY2FyZXQub2Zmc2V0ICsgYWRkZWRMZW4sIHRydWUpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIERvbid0IHRyeSB0byBkbyB0aGluZ3Mgd2l0aCB0aGUgYXV0b2NvbXBsZXRlIGlmIHRoZXJlIGlzIG5vbmUgc2hvd25cclxuICAgICAgICAgICAgaWYgKG1vZGVsLmF1dG9Db21wbGV0ZSkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgbW9kZWwuYXV0b0NvbXBsZXRlLm9uVGFiKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIW1vZGVsLmF1dG9Db21wbGV0ZS5oYXNTZWxlY3Rpb24oKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dWaXN1YWxCZWxsOiB0cnVlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbW9kZWwuYXV0b0NvbXBsZXRlLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRFZGl0YWJsZVJvb3ROb2RlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9lZGl0b3JSZWY7XHJcbiAgICB9XHJcblxyXG4gICAgaXNNb2RpZmllZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kaWZpZWRGbGFnO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkF1dG9Db21wbGV0ZUNvbmZpcm0gPSAoY29tcGxldGlvbikgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMubW9kZWwuYXV0b0NvbXBsZXRlLm9uQ29tcG9uZW50Q29uZmlybShjb21wbGV0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25BdXRvQ29tcGxldGVTZWxlY3Rpb25DaGFuZ2UgPSAoY29tcGxldGlvbiwgY29tcGxldGlvbkluZGV4KSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5tb2RlbC5hdXRvQ29tcGxldGUub25Db21wb25lbnRTZWxlY3Rpb25DaGFuZ2UoY29tcGxldGlvbik7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y29tcGxldGlvbkluZGV4fSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2NvbmZpZ3VyZUVtb3RpY29uQXV0b1JlcGxhY2UoKSB7XHJcbiAgICAgICAgY29uc3Qgc2hvdWxkUmVwbGFjZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoJ01lc3NhZ2VDb21wb3NlcklucHV0LmF1dG9SZXBsYWNlRW1vamknKTtcclxuICAgICAgICB0aGlzLnByb3BzLm1vZGVsLnNldFRyYW5zZm9ybUNhbGxiYWNrKHNob3VsZFJlcGxhY2UgPyB0aGlzLl9yZXBsYWNlRW1vdGljb24gOiBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2VsZWN0aW9uY2hhbmdlXCIsIHRoaXMuX29uU2VsZWN0aW9uQ2hhbmdlKTtcclxuICAgICAgICB0aGlzLl9lZGl0b3JSZWYucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImlucHV0XCIsIHRoaXMuX29uSW5wdXQsIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5yZW1vdmVFdmVudExpc3RlbmVyKFwiY29tcG9zaXRpb25zdGFydFwiLCB0aGlzLl9vbkNvbXBvc2l0aW9uU3RhcnQsIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5yZW1vdmVFdmVudExpc3RlbmVyKFwiY29tcG9zaXRpb25lbmRcIiwgdGhpcy5fb25Db21wb3NpdGlvbkVuZCwgdHJ1ZSk7XHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS51bndhdGNoU2V0dGluZyh0aGlzLl9lbW90aWNvblNldHRpbmdIYW5kbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IG1vZGVsID0gdGhpcy5wcm9wcy5tb2RlbDtcclxuICAgICAgICBtb2RlbC5zZXRVcGRhdGVDYWxsYmFjayh0aGlzLl91cGRhdGVFZGl0b3JTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5fZW1vdGljb25TZXR0aW5nSGFuZGxlID0gU2V0dGluZ3NTdG9yZS53YXRjaFNldHRpbmcoJ01lc3NhZ2VDb21wb3NlcklucHV0LmF1dG9SZXBsYWNlRW1vamknLCBudWxsLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NvbmZpZ3VyZUVtb3RpY29uQXV0b1JlcGxhY2UoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl9jb25maWd1cmVFbW90aWNvbkF1dG9SZXBsYWNlKCk7XHJcbiAgICAgICAgY29uc3QgcGFydENyZWF0b3IgPSBtb2RlbC5wYXJ0Q3JlYXRvcjtcclxuICAgICAgICAvLyBUT0RPOiBkb2VzIHRoaXMgYWxsb3cgdXMgdG8gZ2V0IHJpZCBvZiBFZGl0b3JTdGF0ZVRyYW5zZmVyP1xyXG4gICAgICAgIC8vIG5vdCByZWFsbHksIGJ1dCB3ZSBjb3VsZCBub3Qgc2VyaWFsaXplIHRoZSBwYXJ0cywgYW5kIGp1c3QgY2hhbmdlIHRoZSBhdXRvQ29tcGxldGVyXHJcbiAgICAgICAgcGFydENyZWF0b3Iuc2V0QXV0b0NvbXBsZXRlQ3JlYXRvcihhdXRvQ29tcGxldGVDcmVhdG9yKFxyXG4gICAgICAgICAgICAoKSA9PiB0aGlzLl9hdXRvY29tcGxldGVSZWYsXHJcbiAgICAgICAgICAgIHF1ZXJ5ID0+IG5ldyBQcm9taXNlKHJlc29sdmUgPT4gdGhpcy5zZXRTdGF0ZSh7cXVlcnl9LCByZXNvbHZlKSksXHJcbiAgICAgICAgKSk7XHJcbiAgICAgICAgdGhpcy5oaXN0b3J5TWFuYWdlciA9IG5ldyBIaXN0b3J5TWFuYWdlcihwYXJ0Q3JlYXRvcik7XHJcbiAgICAgICAgLy8gaW5pdGlhbCByZW5kZXIgb2YgbW9kZWxcclxuICAgICAgICB0aGlzLl91cGRhdGVFZGl0b3JTdGF0ZSh0aGlzLl9nZXRJbml0aWFsQ2FyZXRQb3NpdGlvbigpKTtcclxuICAgICAgICAvLyBhdHRhY2ggaW5wdXQgbGlzdGVuZXIgYnkgaGFuZCBzbyBSZWFjdCBkb2Vzbid0IHByb3h5IHRoZSBldmVudHMsXHJcbiAgICAgICAgLy8gYXMgdGhlIHByb3hpZWQgZXZlbnQgZG9lc24ndCBzdXBwb3J0IGlucHV0VHlwZSwgd2hpY2ggd2UgbmVlZC5cclxuICAgICAgICB0aGlzLl9lZGl0b3JSZWYuYWRkRXZlbnRMaXN0ZW5lcihcImlucHV0XCIsIHRoaXMuX29uSW5wdXQsIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5hZGRFdmVudExpc3RlbmVyKFwiY29tcG9zaXRpb25zdGFydFwiLCB0aGlzLl9vbkNvbXBvc2l0aW9uU3RhcnQsIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5hZGRFdmVudExpc3RlbmVyKFwiY29tcG9zaXRpb25lbmRcIiwgdGhpcy5fb25Db21wb3NpdGlvbkVuZCwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5fZWRpdG9yUmVmLmZvY3VzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldEluaXRpYWxDYXJldFBvc2l0aW9uKCkge1xyXG4gICAgICAgIGxldCBjYXJldFBvc2l0aW9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmluaXRpYWxDYXJldCkge1xyXG4gICAgICAgICAgICAvLyBpZiByZXN0b3Jpbmcgc3RhdGUgZnJvbSBhIHByZXZpb3VzIGVkaXRvcixcclxuICAgICAgICAgICAgLy8gcmVzdG9yZSBjYXJldCBwb3NpdGlvbiBmcm9tIHRoZSBzdGF0ZVxyXG4gICAgICAgICAgICBjb25zdCBjYXJldCA9IHRoaXMucHJvcHMuaW5pdGlhbENhcmV0O1xyXG4gICAgICAgICAgICBjYXJldFBvc2l0aW9uID0gdGhpcy5wcm9wcy5tb2RlbC5wb3NpdGlvbkZvck9mZnNldChjYXJldC5vZmZzZXQsIGNhcmV0LmF0Tm9kZUVuZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gb3RoZXJ3aXNlLCBzZXQgaXQgYXQgdGhlIGVuZFxyXG4gICAgICAgICAgICBjYXJldFBvc2l0aW9uID0gdGhpcy5wcm9wcy5tb2RlbC5nZXRQb3NpdGlvbkF0RW5kKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjYXJldFBvc2l0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkZvcm1hdEFjdGlvbiA9IChhY3Rpb24pID0+IHtcclxuICAgICAgICBjb25zdCByYW5nZSA9IGdldFJhbmdlRm9yU2VsZWN0aW9uKFxyXG4gICAgICAgICAgICB0aGlzLl9lZGl0b3JSZWYsXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMubW9kZWwsXHJcbiAgICAgICAgICAgIGRvY3VtZW50LmdldFNlbGVjdGlvbigpKTtcclxuICAgICAgICBpZiAocmFuZ2UubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5oaXN0b3J5TWFuYWdlci5lbnN1cmVMYXN0Q2hhbmdlc1B1c2hlZCh0aGlzLnByb3BzLm1vZGVsKTtcclxuICAgICAgICB0aGlzLl9tb2RpZmllZEZsYWcgPSB0cnVlO1xyXG4gICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJib2xkXCI6XHJcbiAgICAgICAgICAgICAgICB0b2dnbGVJbmxpbmVGb3JtYXQocmFuZ2UsIFwiKipcIik7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcIml0YWxpY3NcIjpcclxuICAgICAgICAgICAgICAgIHRvZ2dsZUlubGluZUZvcm1hdChyYW5nZSwgXCJfXCIpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJzdHJpa2V0aHJvdWdoXCI6XHJcbiAgICAgICAgICAgICAgICB0b2dnbGVJbmxpbmVGb3JtYXQocmFuZ2UsIFwiPGRlbD5cIiwgXCI8L2RlbD5cIik7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImNvZGVcIjpcclxuICAgICAgICAgICAgICAgIGZvcm1hdFJhbmdlQXNDb2RlKHJhbmdlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIFwicXVvdGVcIjpcclxuICAgICAgICAgICAgICAgIGZvcm1hdFJhbmdlQXNRdW90ZShyYW5nZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGxldCBhdXRvQ29tcGxldGU7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuYXV0b0NvbXBsZXRlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHF1ZXJ5ID0gdGhpcy5zdGF0ZS5xdWVyeTtcclxuICAgICAgICAgICAgY29uc3QgcXVlcnlMZW4gPSBxdWVyeS5sZW5ndGg7XHJcbiAgICAgICAgICAgIGF1dG9Db21wbGV0ZSA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X0Jhc2ljTWVzc2FnZUNvbXBvc2VyX0F1dG9Db21wbGV0ZVdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgIDxBdXRvY29tcGxldGVcclxuICAgICAgICAgICAgICAgICAgICByZWY9e3JlZiA9PiB0aGlzLl9hdXRvY29tcGxldGVSZWYgPSByZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnk9e3F1ZXJ5fVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ29uZmlybT17dGhpcy5fb25BdXRvQ29tcGxldGVDb25maXJtfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uU2VsZWN0aW9uQ2hhbmdlPXt0aGlzLl9vbkF1dG9Db21wbGV0ZVNlbGVjdGlvbkNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb249e3tiZWdpbm5pbmc6IHRydWUsIGVuZDogcXVlcnlMZW4sIHN0YXJ0OiBxdWVyeUxlbn19XHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbT17dGhpcy5wcm9wcy5yb29tfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9kaXY+KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9CYXNpY01lc3NhZ2VDb21wb3NlclwiLCB7XHJcbiAgICAgICAgICAgIFwibXhfQmFzaWNNZXNzYWdlQ29tcG9zZXJfaW5wdXRfZXJyb3JcIjogdGhpcy5zdGF0ZS5zaG93VmlzdWFsQmVsbCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgTWVzc2FnZUNvbXBvc2VyRm9ybWF0QmFyID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuTWVzc2FnZUNvbXBvc2VyRm9ybWF0QmFyJyk7XHJcbiAgICAgICAgY29uc3Qgc2hvcnRjdXRzID0ge1xyXG4gICAgICAgICAgICBib2xkOiBjdHJsU2hvcnRjdXRMYWJlbChcIkJcIiksXHJcbiAgICAgICAgICAgIGl0YWxpY3M6IGN0cmxTaG9ydGN1dExhYmVsKFwiSVwiKSxcclxuICAgICAgICAgICAgcXVvdGU6IGN0cmxTaG9ydGN1dExhYmVsKFwiPlwiKSxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCB7Y29tcGxldGlvbkluZGV4fSA9IHRoaXMuc3RhdGU7XHJcblxyXG4gICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9PlxyXG4gICAgICAgICAgICB7IGF1dG9Db21wbGV0ZSB9XHJcbiAgICAgICAgICAgIDxNZXNzYWdlQ29tcG9zZXJGb3JtYXRCYXIgcmVmPXtyZWYgPT4gdGhpcy5fZm9ybWF0QmFyUmVmID0gcmVmfSBvbkFjdGlvbj17dGhpcy5fb25Gb3JtYXRBY3Rpb259IHNob3J0Y3V0cz17c2hvcnRjdXRzfSAvPlxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9CYXNpY01lc3NhZ2VDb21wb3Nlcl9pbnB1dFwiXHJcbiAgICAgICAgICAgICAgICBjb250ZW50RWRpdGFibGU9XCJ0cnVlXCJcclxuICAgICAgICAgICAgICAgIHRhYkluZGV4PVwiMFwiXHJcbiAgICAgICAgICAgICAgICBvbkJsdXI9e3RoaXMuX29uQmx1cn1cclxuICAgICAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMuX29uRm9jdXN9XHJcbiAgICAgICAgICAgICAgICBvbkNvcHk9e3RoaXMuX29uQ29weX1cclxuICAgICAgICAgICAgICAgIG9uQ3V0PXt0aGlzLl9vbkN1dH1cclxuICAgICAgICAgICAgICAgIG9uUGFzdGU9e3RoaXMuX29uUGFzdGV9XHJcbiAgICAgICAgICAgICAgICBvbktleURvd249e3RoaXMuX29uS2V5RG93bn1cclxuICAgICAgICAgICAgICAgIHJlZj17cmVmID0+IHRoaXMuX2VkaXRvclJlZiA9IHJlZn1cclxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9e3RoaXMucHJvcHMubGFiZWx9XHJcbiAgICAgICAgICAgICAgICByb2xlPVwidGV4dGJveFwiXHJcbiAgICAgICAgICAgICAgICBhcmlhLW11bHRpbGluZT1cInRydWVcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1hdXRvY29tcGxldGU9XCJib3RoXCJcclxuICAgICAgICAgICAgICAgIGFyaWEtaGFzcG9wdXA9XCJsaXN0Ym94XCJcclxuICAgICAgICAgICAgICAgIGFyaWEtZXhwYW5kZWQ9e0Jvb2xlYW4odGhpcy5zdGF0ZS5hdXRvQ29tcGxldGUpfVxyXG4gICAgICAgICAgICAgICAgYXJpYS1hY3RpdmVkZXNjZW5kYW50PXtjb21wbGV0aW9uSW5kZXggPj0gMCA/IGdlbmVyYXRlQ29tcGxldGlvbkRvbUlkKGNvbXBsZXRpb25JbmRleCkgOiB1bmRlZmluZWR9XHJcbiAgICAgICAgICAgICAgICBkaXI9XCJhdXRvXCJcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICA8L2Rpdj4pO1xyXG4gICAgfVxyXG5cclxuICAgIGZvY3VzKCkge1xyXG4gICAgICAgIHRoaXMuX2VkaXRvclJlZi5mb2N1cygpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==