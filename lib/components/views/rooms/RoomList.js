"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _Timer = _interopRequireDefault(require("../../../utils/Timer"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var utils = _interopRequireWildcard(require("matrix-js-sdk/src/utils"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _ratelimitedfunc = _interopRequireDefault(require("../../../ratelimitedfunc"));

var Rooms = _interopRequireWildcard(require("../../../Rooms"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var _TagOrderStore = _interopRequireDefault(require("../../../stores/TagOrderStore"));

var _RoomListStore = _interopRequireWildcard(require("../../../stores/RoomListStore"));

var _CustomRoomTagStore = _interopRequireDefault(require("../../../stores/CustomRoomTagStore"));

var _GroupStore = _interopRequireDefault(require("../../../stores/GroupStore"));

var _RoomSubList = _interopRequireDefault(require("../../structures/RoomSubList"));

var _ResizeHandle = _interopRequireDefault(require("../elements/ResizeHandle"));

var _CallHandler = _interopRequireDefault(require("../../../CallHandler"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var sdk = _interopRequireWildcard(require("../../../index"));

var Receipt = _interopRequireWildcard(require("../../../utils/Receipt"));

var _resizer = require("../../../resizer");

var _roomsublist = require("../../../resizer/distributors/roomsublist2");

var _RovingTabIndex = require("../../../accessibility/RovingTabIndex");

var Unread = _interopRequireWildcard(require("../../../Unread"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

const HIDE_CONFERENCE_CHANS = true;
const STANDARD_TAGS_REGEX = /^(m\.(favourite|lowpriority|server_notice)|im\.vector\.fake\.(invite|recent|direct|archived))$/;
const HOVER_MOVE_TIMEOUT = 1000;

function labelForTagName(tagName) {
  if (tagName.startsWith('u.')) return tagName.slice(2);
  return tagName;
}

var _default = (0, _createReactClass.default)({
  displayName: 'RoomList',
  propTypes: {
    ConferenceHandler: _propTypes.default.any,
    collapsed: _propTypes.default.bool.isRequired,
    searchFilter: _propTypes.default.string
  },
  getInitialState: function () {
    this._hoverClearTimer = null;
    this._subListRefs = {// key => RoomSubList ref
    };
    const sizesJson = window.localStorage.getItem("mx_roomlist_sizes");
    const collapsedJson = window.localStorage.getItem("mx_roomlist_collapsed");
    this.subListSizes = sizesJson ? JSON.parse(sizesJson) : {};
    this.collapsedState = collapsedJson ? JSON.parse(collapsedJson) : {};
    this._layoutSections = [];
    const unfilteredOptions = {
      allowWhitespace: false,
      handleHeight: 1
    };
    this._unfilteredlayout = new _roomsublist.Layout((key, size) => {
      const subList = this._subListRefs[key];

      if (subList) {
        subList.setHeight(size);
      } // update overflow indicators


      this._checkSubListsOverflow(); // don't store height for collapsed sublists


      if (!this.collapsedState[key]) {
        this.subListSizes[key] = size;
        window.localStorage.setItem("mx_roomlist_sizes", JSON.stringify(this.subListSizes));
      }
    }, this.subListSizes, this.collapsedState, unfilteredOptions);
    this._filteredLayout = new _roomsublist.Layout((key, size) => {
      const subList = this._subListRefs[key];

      if (subList) {
        subList.setHeight(size);
      }
    }, null, null, {
      allowWhitespace: false,
      handleHeight: 0
    });
    this._layout = this._unfilteredlayout;
    return {
      isLoadingLeftRooms: false,
      totalRoomCount: null,
      lists: {},
      incomingCallTag: null,
      incomingCall: null,
      selectedTags: [],
      hover: false,
      customTags: _CustomRoomTagStore.default.getTags()
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, put this in the constructor.
  UNSAFE_componentWillMount: function () {
    this.mounted = false;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.on("Room", this.onRoom);
    cli.on("deleteRoom", this.onDeleteRoom);
    cli.on("Room.receipt", this.onRoomReceipt);
    cli.on("RoomMember.name", this.onRoomMemberName);
    cli.on("Event.decrypted", this.onEventDecrypted);
    cli.on("accountData", this.onAccountData);
    cli.on("Group.myMembership", this._onGroupMyMembership);
    cli.on("RoomState.events", this.onRoomStateEvents);

    const dmRoomMap = _DMRoomMap.default.shared(); // A map between tags which are group IDs and the room IDs of rooms that should be kept
    // in the room list when filtering by that tag.


    this._visibleRoomsForGroup = {// $groupId: [$roomId1, $roomId2, ...],
    }; // All rooms that should be kept in the room list when filtering.
    // By default, show all rooms.

    this._visibleRooms = _MatrixClientPeg.MatrixClientPeg.get().getVisibleRooms(); // Listen to updates to group data. RoomList cares about members and rooms in order
    // to filter the room list when group tags are selected.

    this._groupStoreToken = _GroupStore.default.registerListener(null, () => {
      (_TagOrderStore.default.getOrderedTags() || []).forEach(tag => {
        if (tag[0] !== '+') {
          return;
        } // This group's rooms or members may have updated, update rooms for its tag


        this.updateVisibleRoomsForTag(dmRoomMap, tag);
        this.updateVisibleRooms();
      });
    });
    this._tagStoreToken = _TagOrderStore.default.addListener(() => {
      // Filters themselves have changed
      this.updateVisibleRooms();
    });
    this._roomListStoreToken = _RoomListStore.default.addListener(() => {
      this._delayedRefreshRoomList();
    });

    if (_SettingsStore.default.isFeatureEnabled("feature_custom_tags")) {
      this._customTagStoreToken = _CustomRoomTagStore.default.addListener(() => {
        this.setState({
          customTags: _CustomRoomTagStore.default.getTags()
        });
      });
    }

    this.refreshRoomList(); // order of the sublists
    //this.listOrder = [];
    // loop count to stop a stack overflow if the user keeps waggling the
    // mouse for >30s in a row, or if running under mocha

    this._delayedRefreshRoomListLoopCount = 0;
  },
  componentDidMount: function () {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    const cfg = {
      getLayout: () => this._layout
    };
    this.resizer = new _resizer.Resizer(this.resizeContainer, _roomsublist.Distributor, cfg);
    this.resizer.setClassNames({
      handle: "mx_ResizeHandle",
      vertical: "mx_ResizeHandle_vertical",
      reverse: "mx_ResizeHandle_reverse"
    });

    this._layout.update(this._layoutSections, this.resizeContainer && this.resizeContainer.offsetHeight);

    this._checkSubListsOverflow();

    this.resizer.attach();

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.on("leftPanelResized", this.onResize);
    }

    this.mounted = true;
  },
  componentDidUpdate: function (prevProps) {
    let forceLayoutUpdate = false;

    this._repositionIncomingCallBox(undefined, false);

    if (!this.props.searchFilter && prevProps.searchFilter) {
      this._layout = this._unfilteredlayout;
      forceLayoutUpdate = true;
    } else if (this.props.searchFilter && !prevProps.searchFilter) {
      this._layout = this._filteredLayout;
      forceLayoutUpdate = true;
    }

    this._layout.update(this._layoutSections, this.resizeContainer && this.resizeContainer.clientHeight, forceLayoutUpdate);

    this._checkSubListsOverflow();
  },
  onAction: function (payload) {
    switch (payload.action) {
      case 'view_tooltip':
        this.tooltip = payload.tooltip;
        break;

      case 'call_state':
        var call = _CallHandler.default.getCall(payload.room_id);

        if (call && call.call_state === 'ringing') {
          this.setState({
            incomingCall: call,
            incomingCallTag: this.getTagNameForRoomId(payload.room_id)
          });

          this._repositionIncomingCallBox(undefined, true);
        } else {
          this.setState({
            incomingCall: null,
            incomingCallTag: null
          });
        }

        break;

      case 'view_room_delta':
        {
          const currentRoomId = _RoomViewStore.default.getRoomId();

          const _this$state$lists = this.state.lists,
                {
            "im.vector.fake.invite": inviteRooms,
            "m.favourite": favouriteRooms,
            [_RoomListStore.TAG_DM]: dmRooms,
            "im.vector.fake.recent": recentRooms,
            "m.lowpriority": lowPriorityRooms,
            "im.vector.fake.archived": historicalRooms,
            "m.server_notice": serverNoticeRooms
          } = _this$state$lists,
                tags = (0, _objectWithoutProperties2.default)(_this$state$lists, ["im.vector.fake.invite", "m.favourite", _RoomListStore.TAG_DM, "im.vector.fake.recent", "m.lowpriority", "im.vector.fake.archived", "m.server_notice"].map(_toPropertyKey));
          const shownCustomTagRooms = Object.keys(tags).filter(tagName => {
            return (!this.state.customTags || this.state.customTags[tagName]) && !tagName.match(STANDARD_TAGS_REGEX);
          }).map(tagName => tags[tagName]); // this order matches the one when generating the room sublists below.

          let rooms = this._applySearchFilter([...inviteRooms, ...favouriteRooms, ...dmRooms, ...recentRooms, ...[].concat.apply([], shownCustomTagRooms), // eslint-disable-line prefer-spread
          ...lowPriorityRooms, ...historicalRooms, ...serverNoticeRooms], this.props.searchFilter);

          if (payload.unread) {
            // filter to only notification rooms (and our current active room so we can index properly)
            rooms = rooms.filter(room => {
              return room.roomId === currentRoomId || Unread.doesRoomHaveUnreadMessages(room);
            });
          }

          const currentIndex = rooms.findIndex(room => room.roomId === currentRoomId); // use slice to account for looping around the start

          const [room] = rooms.slice((currentIndex + payload.delta) % rooms.length);

          if (room) {
            _dispatcher.default.dispatch({
              action: 'view_room',
              room_id: room.roomId,
              show_room_tile: true // to make sure the room gets scrolled into view

            });
          }

          break;
        }
    }
  },
  componentWillUnmount: function () {
    this.mounted = false;

    _dispatcher.default.unregister(this.dispatcherRef);

    if (_MatrixClientPeg.MatrixClientPeg.get()) {
      _MatrixClientPeg.MatrixClientPeg.get().removeListener("Room", this.onRoom);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("deleteRoom", this.onDeleteRoom);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("Room.receipt", this.onRoomReceipt);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("RoomMember.name", this.onRoomMemberName);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("Event.decrypted", this.onEventDecrypted);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("accountData", this.onAccountData);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("Group.myMembership", this._onGroupMyMembership);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener("RoomState.events", this.onRoomStateEvents);
    }

    if (this.props.resizeNotifier) {
      this.props.resizeNotifier.removeListener("leftPanelResized", this.onResize);
    }

    if (this._tagStoreToken) {
      this._tagStoreToken.remove();
    }

    if (this._roomListStoreToken) {
      this._roomListStoreToken.remove();
    }

    if (this._customTagStoreToken) {
      this._customTagStoreToken.remove();
    } // NB: GroupStore is not a Flux.Store


    if (this._groupStoreToken) {
      this._groupStoreToken.unregister();
    } // cancel any pending calls to the rate_limited_funcs


    this._delayedRefreshRoomList.cancelPendingCall();
  },
  onResize: function () {
    if (this.mounted && this._layout && this.resizeContainer && Array.isArray(this._layoutSections)) {
      this._layout.update(this._layoutSections, this.resizeContainer.offsetHeight);
    }
  },
  onRoom: function (room) {
    this.updateVisibleRooms();
  },
  onRoomStateEvents: function (ev, state) {
    if (ev.getType() === "m.room.create" || ev.getType() === "m.room.tombstone") {
      this.updateVisibleRooms();
    }
  },
  onDeleteRoom: function (roomId) {
    this.updateVisibleRooms();
  },
  onArchivedHeaderClick: function (isHidden, scrollToPosition) {
    if (!isHidden) {
      const self = this;
      this.setState({
        isLoadingLeftRooms: true
      }); // we don't care about the response since it comes down via "Room"
      // events.

      _MatrixClientPeg.MatrixClientPeg.get().syncLeftRooms().catch(function (err) {
        console.error("Failed to sync left rooms: %s", err);
        console.error(err);
      }).finally(function () {
        self.setState({
          isLoadingLeftRooms: false
        });
      });
    }
  },
  onRoomReceipt: function (receiptEvent, room) {
    // because if we read a notification, it will affect notification count
    // only bother updating if there's a receipt from us
    if (Receipt.findReadReceiptFromUserId(receiptEvent, _MatrixClientPeg.MatrixClientPeg.get().credentials.userId)) {
      this._delayedRefreshRoomList();
    }
  },
  onRoomMemberName: function (ev, member) {
    this._delayedRefreshRoomList();
  },
  onEventDecrypted: function (ev) {
    // An event being decrypted may mean we need to re-order the room list
    this._delayedRefreshRoomList();
  },
  onAccountData: function (ev) {
    if (ev.getType() == 'm.direct') {
      this._delayedRefreshRoomList();
    }
  },
  _onGroupMyMembership: function (group) {
    this.forceUpdate();
  },
  onMouseMove: async function (ev) {
    if (!this._hoverClearTimer) {
      this.setState({
        hover: true
      });
      this._hoverClearTimer = new _Timer.default(HOVER_MOVE_TIMEOUT);

      this._hoverClearTimer.start();

      let finished = true;

      try {
        await this._hoverClearTimer.finished();
      } catch (err) {
        finished = false;
      }

      this._hoverClearTimer = null;

      if (finished) {
        this.setState({
          hover: false
        });

        this._delayedRefreshRoomList();
      }
    } else {
      this._hoverClearTimer.restart();
    }
  },
  onMouseLeave: function (ev) {
    if (this._hoverClearTimer) {
      this._hoverClearTimer.abort();

      this._hoverClearTimer = null;
    }

    this.setState({
      hover: false
    }); // Refresh the room list just in case the user missed something.

    this._delayedRefreshRoomList();
  },
  _delayedRefreshRoomList: (0, _ratelimitedfunc.default)(function () {
    this.refreshRoomList();
  }, 500),
  // Update which rooms and users should appear in RoomList for a given group tag
  updateVisibleRoomsForTag: function (dmRoomMap, tag) {
    if (!this.mounted) return; // For now, only handle group tags

    if (tag[0] !== '+') return;
    this._visibleRoomsForGroup[tag] = [];

    _GroupStore.default.getGroupRooms(tag).forEach(room => this._visibleRoomsForGroup[tag].push(room.roomId));

    _GroupStore.default.getGroupMembers(tag).forEach(member => {
      if (member.userId === _MatrixClientPeg.MatrixClientPeg.get().credentials.userId) return;
      dmRoomMap.getDMRoomsForUserId(member.userId).forEach(roomId => this._visibleRoomsForGroup[tag].push(roomId));
    }); // TODO: Check if room has been tagged to the group by the user

  },
  // Update which rooms and users should appear according to which tags are selected
  updateVisibleRooms: function () {
    const selectedTags = _TagOrderStore.default.getSelectedTags();

    const visibleGroupRooms = [];
    selectedTags.forEach(tag => {
      (this._visibleRoomsForGroup[tag] || []).forEach(roomId => visibleGroupRooms.push(roomId));
    }); // If there are any tags selected, constrain the rooms listed to the
    // visible rooms as determined by visibleGroupRooms. Here, we
    // de-duplicate and filter out rooms that the client doesn't know
    // about (hence the Set and the null-guard on `room`).

    if (selectedTags.length > 0) {
      const roomSet = new Set();
      visibleGroupRooms.forEach(roomId => {
        const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

        if (room) {
          roomSet.add(room);
        }
      });
      this._visibleRooms = Array.from(roomSet);
    } else {
      // Show all rooms
      this._visibleRooms = _MatrixClientPeg.MatrixClientPeg.get().getVisibleRooms();
    }

    this._delayedRefreshRoomList();
  },
  refreshRoomList: function () {
    if (this.state.hover) {
      // Don't re-sort the list if we're hovering over the list
      return;
    } // TODO: ideally we'd calculate this once at start, and then maintain
    // any changes to it incrementally, updating the appropriate sublists
    // as needed.
    // Alternatively we'd do something magical with Immutable.js or similar.


    const lists = this.getRoomLists();
    let totalRooms = 0;

    for (const l of Object.values(lists)) {
      totalRooms += l.length;
    }

    this.setState({
      lists,
      totalRoomCount: totalRooms,
      // Do this here so as to not render every time the selected tags
      // themselves change.
      selectedTags: _TagOrderStore.default.getSelectedTags()
    }, () => {
      // we don't need to restore any size here, do we?
      // i guess we could have triggered a new group to appear
      // that already an explicit size the last time it appeared ...
      this._checkSubListsOverflow();
    }); // this._lastRefreshRoomListTs = Date.now();
  },
  getTagNameForRoomId: function (roomId) {
    const lists = _RoomListStore.default.getRoomLists();

    for (const tagName of Object.keys(lists)) {
      for (const room of lists[tagName]) {
        // Should be impossible, but guard anyways.
        if (!room) {
          continue;
        }

        const myUserId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

        if (HIDE_CONFERENCE_CHANS && Rooms.isConfCallRoom(room, myUserId, this.props.ConferenceHandler)) {
          continue;
        }

        if (room.roomId === roomId) return tagName;
      }
    }

    return null;
  },
  getRoomLists: function () {
    const lists = _RoomListStore.default.getRoomLists();

    const filteredLists = {};
    const isRoomVisible = {// $roomId: true,
    };

    this._visibleRooms.forEach(r => {
      isRoomVisible[r.roomId] = true;
    });

    Object.keys(lists).forEach(tagName => {
      const filteredRooms = lists[tagName].filter(taggedRoom => {
        // Somewhat impossible, but guard against it anyway
        if (!taggedRoom) {
          return;
        }

        const myUserId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

        if (HIDE_CONFERENCE_CHANS && Rooms.isConfCallRoom(taggedRoom, myUserId, this.props.ConferenceHandler)) {
          return;
        }

        return Boolean(isRoomVisible[taggedRoom.roomId]);
      });

      if (filteredRooms.length > 0 || tagName.match(STANDARD_TAGS_REGEX)) {
        filteredLists[tagName] = filteredRooms;
      }
    });
    return filteredLists;
  },
  _getScrollNode: function () {
    if (!this.mounted) return null;

    const panel = _reactDom.default.findDOMNode(this);

    if (!panel) return null;

    if (panel.classList.contains('gm-prevented')) {
      return panel;
    } else {
      return panel.children[2]; // XXX: Fragile!
    }
  },
  _whenScrolling: function (e) {
    this._hideTooltip(e);

    this._repositionIncomingCallBox(e, false);
  },
  _hideTooltip: function (e) {
    // Hide tooltip when scrolling, as we'll no longer be over the one we were on
    if (this.tooltip && this.tooltip.style.display !== "none") {
      this.tooltip.style.display = "none";
    }
  },
  _repositionIncomingCallBox: function (e, firstTime) {
    const incomingCallBox = document.getElementById("incomingCallBox");

    if (incomingCallBox && incomingCallBox.parentElement) {
      const scrollArea = this._getScrollNode();

      if (!scrollArea) return; // Use the offset of the top of the scroll area from the window
      // as this is used to calculate the CSS fixed top position for the stickies

      const scrollAreaOffset = scrollArea.getBoundingClientRect().top + window.pageYOffset; // Use the offset of the top of the component from the window
      // as this is used to calculate the CSS fixed top position for the stickies

      const scrollAreaHeight = _reactDom.default.findDOMNode(this).getBoundingClientRect().height;

      let top = incomingCallBox.parentElement.getBoundingClientRect().top + window.pageYOffset; // Make sure we don't go too far up, if the headers aren't sticky

      top = top < scrollAreaOffset ? scrollAreaOffset : top; // make sure we don't go too far down, if the headers aren't sticky

      const bottomMargin = scrollAreaOffset + (scrollAreaHeight - 45);
      top = top > bottomMargin ? bottomMargin : top;
      incomingCallBox.style.top = top + "px";
      incomingCallBox.style.left = scrollArea.offsetLeft + scrollArea.offsetWidth + 12 + "px";
    }
  },

  _makeGroupInviteTiles(filter) {
    const ret = [];
    const lcFilter = filter && filter.toLowerCase();
    const GroupInviteTile = sdk.getComponent('groups.GroupInviteTile');

    for (const group of _MatrixClientPeg.MatrixClientPeg.get().getGroups()) {
      const {
        groupId,
        name,
        myMembership
      } = group; // filter to only groups in invite state and group_id starts with filter or group name includes it

      if (myMembership !== 'invite') continue;
      if (lcFilter && !groupId.toLowerCase().startsWith(lcFilter) && !(name && name.toLowerCase().includes(lcFilter))) continue;
      ret.push(_react.default.createElement(GroupInviteTile, {
        key: groupId,
        group: group,
        collapsed: this.props.collapsed
      }));
    }

    return ret;
  },

  _applySearchFilter: function (list, filter) {
    if (filter === "") return list;
    const lcFilter = filter.toLowerCase(); // apply toLowerCase before and after removeHiddenChars because different rules get applied
    // e.g M -> M but m -> n, yet some unicode homoglyphs come out as uppercase, e.g 𝚮 -> H

    const fuzzyFilter = utils.removeHiddenChars(lcFilter).toLowerCase(); // case insensitive if room name includes filter,
    // or if starts with `#` and one of room's aliases starts with filter

    return list.filter(room => {
      if (filter[0] === "#") {
        if (room.getCanonicalAlias() && room.getCanonicalAlias().toLowerCase().startsWith(lcFilter)) {
          return true;
        }

        if (room.getAltAliases().some(alias => alias.toLowerCase().startsWith(lcFilter))) {
          return true;
        }
      }

      return room.name && utils.removeHiddenChars(room.name.toLowerCase()).toLowerCase().includes(fuzzyFilter);
    });
  },
  _handleCollapsedState: function (key, collapsed) {
    // persist collapsed state
    this.collapsedState[key] = collapsed;
    window.localStorage.setItem("mx_roomlist_collapsed", JSON.stringify(this.collapsedState)); // load the persisted size configuration of the expanded sub list

    if (collapsed) {
      this._layout.collapseSection(key);
    } else {
      this._layout.expandSection(key, this.subListSizes[key]);
    } // check overflow, as sub lists sizes have changed
    // important this happens after calling resize above


    this._checkSubListsOverflow();
  },

  // check overflow for scroll indicator gradient
  _checkSubListsOverflow() {
    Object.values(this._subListRefs).forEach(l => l.checkOverflow());
  },

  _subListRef: function (key, ref) {
    if (!ref) {
      delete this._subListRefs[key];
    } else {
      this._subListRefs[key] = ref;
    }
  },
  _mapSubListProps: function (subListsProps) {
    this._layoutSections = [];
    const defaultProps = {
      collapsed: this.props.collapsed,
      isFiltered: !!this.props.searchFilter
    };
    subListsProps.forEach(p => {
      p.list = this._applySearchFilter(p.list, this.props.searchFilter);
    });
    subListsProps = subListsProps.filter(props => {
      const len = props.list.length + (props.extraTiles ? props.extraTiles.length : 0);
      return len !== 0 || props.onAddRoom;
    });
    return subListsProps.reduce((components, props, i) => {
      props = _objectSpread({}, defaultProps, {}, props);
      const isLast = i === subListsProps.length - 1;
      const len = props.list.length + (props.extraTiles ? props.extraTiles.length : 0);
      const {
        key,
        label,
        onHeaderClick
      } = props,
            otherProps = (0, _objectWithoutProperties2.default)(props, ["key", "label", "onHeaderClick"]);
      const chosenKey = key || label;

      const onSubListHeaderClick = collapsed => {
        this._handleCollapsedState(chosenKey, collapsed);

        if (onHeaderClick) {
          onHeaderClick(collapsed);
        }
      };

      const startAsHidden = props.startAsHidden || this.collapsedState[chosenKey];

      this._layoutSections.push({
        id: chosenKey,
        count: len
      });

      const subList = _react.default.createElement(_RoomSubList.default, (0, _extends2.default)({
        ref: this._subListRef.bind(this, chosenKey),
        startAsHidden: startAsHidden,
        forceExpand: !!this.props.searchFilter,
        onHeaderClick: onSubListHeaderClick,
        key: chosenKey,
        label: label
      }, otherProps));

      if (!isLast) {
        return components.concat(subList, _react.default.createElement(_ResizeHandle.default, {
          key: chosenKey + "-resizer",
          vertical: true,
          id: chosenKey
        }));
      } else {
        return components.concat(subList);
      }
    }, []);
  },
  _collectResizeContainer: function (el) {
    this.resizeContainer = el;
  },
  render: function () {
    const incomingCallIfTaggedAs = tagName => {
      if (!this.state.incomingCall) return null;
      if (this.state.incomingCallTag !== tagName) return null;
      return this.state.incomingCall;
    };

    let subLists = [{
      list: [],
      extraTiles: this._makeGroupInviteTiles(this.props.searchFilter),
      label: (0, _languageHandler._t)('Community Invites'),
      isInvite: true
    }, {
      list: this.state.lists['im.vector.fake.invite'],
      label: (0, _languageHandler._t)('Invites'),
      incomingCall: incomingCallIfTaggedAs('im.vector.fake.invite'),
      isInvite: true
    }, {
      list: this.state.lists['m.favourite'],
      label: (0, _languageHandler._t)('Favourites'),
      tagName: "m.favourite",
      incomingCall: incomingCallIfTaggedAs('m.favourite')
    }, {
      list: this.state.lists[_RoomListStore.TAG_DM],
      label: (0, _languageHandler._t)('Direct Messages'),
      tagName: _RoomListStore.TAG_DM,
      incomingCall: incomingCallIfTaggedAs(_RoomListStore.TAG_DM),
      onAddRoom: () => {
        _dispatcher.default.dispatch({
          action: 'view_create_chat'
        });
      },
      addRoomLabel: (0, _languageHandler._t)("Start chat")
    }, {
      list: this.state.lists['im.vector.fake.recent'],
      label: (0, _languageHandler._t)('Rooms'),
      incomingCall: incomingCallIfTaggedAs('im.vector.fake.recent'),
      onAddRoom: () => {
        _dispatcher.default.dispatch({
          action: 'view_create_room'
        });
      }
    }];
    const tagSubLists = Object.keys(this.state.lists).filter(tagName => {
      return (!this.state.customTags || this.state.customTags[tagName]) && !tagName.match(STANDARD_TAGS_REGEX);
    }).map(tagName => {
      return {
        list: this.state.lists[tagName],
        key: tagName,
        label: labelForTagName(tagName),
        tagName: tagName,
        incomingCall: incomingCallIfTaggedAs(tagName)
      };
    });
    subLists = subLists.concat(tagSubLists);
    subLists = subLists.concat([{
      list: this.state.lists['m.lowpriority'],
      label: (0, _languageHandler._t)('Low priority'),
      tagName: "m.lowpriority",
      incomingCall: incomingCallIfTaggedAs('m.lowpriority')
    }, {
      list: this.state.lists['im.vector.fake.archived'],
      label: (0, _languageHandler._t)('Historical'),
      incomingCall: incomingCallIfTaggedAs('im.vector.fake.archived'),
      startAsHidden: true,
      showSpinner: this.state.isLoadingLeftRooms,
      onHeaderClick: this.onArchivedHeaderClick
    }, {
      list: this.state.lists['m.server_notice'],
      label: (0, _languageHandler._t)('System Alerts'),
      tagName: "m.lowpriority",
      incomingCall: incomingCallIfTaggedAs('m.server_notice')
    }]);

    const subListComponents = this._mapSubListProps(subLists);

    const _this$props = this.props,
          {
      resizeNotifier,
      collapsed,
      searchFilter,
      ConferenceHandler,
      onKeyDown
    } = _this$props,
          props = (0, _objectWithoutProperties2.default)(_this$props, ["resizeNotifier", "collapsed", "searchFilter", "ConferenceHandler", "onKeyDown"]); // eslint-disable-line

    return _react.default.createElement(_RovingTabIndex.RovingTabIndexProvider, {
      handleHomeEnd: true,
      onKeyDown: onKeyDown
    }, ({
      onKeyDownHandler
    }) => _react.default.createElement("div", (0, _extends2.default)({}, props, {
      onKeyDown: onKeyDownHandler,
      ref: this._collectResizeContainer,
      className: "mx_RoomList",
      role: "tree",
      "aria-label": (0, _languageHandler._t)("Rooms") // Firefox sometimes makes this element focusable due to
      // overflow:scroll;, so force it out of tab order.
      ,
      tabIndex: "-1",
      onMouseMove: this.onMouseMove,
      onMouseLeave: this.onMouseLeave
    }), subListComponents));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21MaXN0LmpzIl0sIm5hbWVzIjpbIkhJREVfQ09ORkVSRU5DRV9DSEFOUyIsIlNUQU5EQVJEX1RBR1NfUkVHRVgiLCJIT1ZFUl9NT1ZFX1RJTUVPVVQiLCJsYWJlbEZvclRhZ05hbWUiLCJ0YWdOYW1lIiwic3RhcnRzV2l0aCIsInNsaWNlIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJDb25mZXJlbmNlSGFuZGxlciIsIlByb3BUeXBlcyIsImFueSIsImNvbGxhcHNlZCIsImJvb2wiLCJpc1JlcXVpcmVkIiwic2VhcmNoRmlsdGVyIiwic3RyaW5nIiwiZ2V0SW5pdGlhbFN0YXRlIiwiX2hvdmVyQ2xlYXJUaW1lciIsIl9zdWJMaXN0UmVmcyIsInNpemVzSnNvbiIsIndpbmRvdyIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJjb2xsYXBzZWRKc29uIiwic3ViTGlzdFNpemVzIiwiSlNPTiIsInBhcnNlIiwiY29sbGFwc2VkU3RhdGUiLCJfbGF5b3V0U2VjdGlvbnMiLCJ1bmZpbHRlcmVkT3B0aW9ucyIsImFsbG93V2hpdGVzcGFjZSIsImhhbmRsZUhlaWdodCIsIl91bmZpbHRlcmVkbGF5b3V0IiwiTGF5b3V0Iiwia2V5Iiwic2l6ZSIsInN1Ykxpc3QiLCJzZXRIZWlnaHQiLCJfY2hlY2tTdWJMaXN0c092ZXJmbG93Iiwic2V0SXRlbSIsInN0cmluZ2lmeSIsIl9maWx0ZXJlZExheW91dCIsIl9sYXlvdXQiLCJpc0xvYWRpbmdMZWZ0Um9vbXMiLCJ0b3RhbFJvb21Db3VudCIsImxpc3RzIiwiaW5jb21pbmdDYWxsVGFnIiwiaW5jb21pbmdDYWxsIiwic2VsZWN0ZWRUYWdzIiwiaG92ZXIiLCJjdXN0b21UYWdzIiwiQ3VzdG9tUm9vbVRhZ1N0b3JlIiwiZ2V0VGFncyIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJtb3VudGVkIiwiY2xpIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwib24iLCJvblJvb20iLCJvbkRlbGV0ZVJvb20iLCJvblJvb21SZWNlaXB0Iiwib25Sb29tTWVtYmVyTmFtZSIsIm9uRXZlbnREZWNyeXB0ZWQiLCJvbkFjY291bnREYXRhIiwiX29uR3JvdXBNeU1lbWJlcnNoaXAiLCJvblJvb21TdGF0ZUV2ZW50cyIsImRtUm9vbU1hcCIsIkRNUm9vbU1hcCIsInNoYXJlZCIsIl92aXNpYmxlUm9vbXNGb3JHcm91cCIsIl92aXNpYmxlUm9vbXMiLCJnZXRWaXNpYmxlUm9vbXMiLCJfZ3JvdXBTdG9yZVRva2VuIiwiR3JvdXBTdG9yZSIsInJlZ2lzdGVyTGlzdGVuZXIiLCJUYWdPcmRlclN0b3JlIiwiZ2V0T3JkZXJlZFRhZ3MiLCJmb3JFYWNoIiwidGFnIiwidXBkYXRlVmlzaWJsZVJvb21zRm9yVGFnIiwidXBkYXRlVmlzaWJsZVJvb21zIiwiX3RhZ1N0b3JlVG9rZW4iLCJhZGRMaXN0ZW5lciIsIl9yb29tTGlzdFN0b3JlVG9rZW4iLCJSb29tTGlzdFN0b3JlIiwiX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3QiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsIl9jdXN0b21UYWdTdG9yZVRva2VuIiwic2V0U3RhdGUiLCJyZWZyZXNoUm9vbUxpc3QiLCJfZGVsYXllZFJlZnJlc2hSb29tTGlzdExvb3BDb3VudCIsImNvbXBvbmVudERpZE1vdW50IiwiZGlzcGF0Y2hlclJlZiIsImRpcyIsInJlZ2lzdGVyIiwib25BY3Rpb24iLCJjZmciLCJnZXRMYXlvdXQiLCJyZXNpemVyIiwiUmVzaXplciIsInJlc2l6ZUNvbnRhaW5lciIsIkRpc3RyaWJ1dG9yIiwic2V0Q2xhc3NOYW1lcyIsImhhbmRsZSIsInZlcnRpY2FsIiwicmV2ZXJzZSIsInVwZGF0ZSIsIm9mZnNldEhlaWdodCIsImF0dGFjaCIsInByb3BzIiwicmVzaXplTm90aWZpZXIiLCJvblJlc2l6ZSIsImNvbXBvbmVudERpZFVwZGF0ZSIsInByZXZQcm9wcyIsImZvcmNlTGF5b3V0VXBkYXRlIiwiX3JlcG9zaXRpb25JbmNvbWluZ0NhbGxCb3giLCJ1bmRlZmluZWQiLCJjbGllbnRIZWlnaHQiLCJwYXlsb2FkIiwiYWN0aW9uIiwidG9vbHRpcCIsImNhbGwiLCJDYWxsSGFuZGxlciIsImdldENhbGwiLCJyb29tX2lkIiwiY2FsbF9zdGF0ZSIsImdldFRhZ05hbWVGb3JSb29tSWQiLCJjdXJyZW50Um9vbUlkIiwiUm9vbVZpZXdTdG9yZSIsImdldFJvb21JZCIsInN0YXRlIiwiaW52aXRlUm9vbXMiLCJmYXZvdXJpdGVSb29tcyIsIlRBR19ETSIsImRtUm9vbXMiLCJyZWNlbnRSb29tcyIsImxvd1ByaW9yaXR5Um9vbXMiLCJoaXN0b3JpY2FsUm9vbXMiLCJzZXJ2ZXJOb3RpY2VSb29tcyIsInRhZ3MiLCJzaG93bkN1c3RvbVRhZ1Jvb21zIiwiT2JqZWN0Iiwia2V5cyIsImZpbHRlciIsIm1hdGNoIiwibWFwIiwicm9vbXMiLCJfYXBwbHlTZWFyY2hGaWx0ZXIiLCJjb25jYXQiLCJhcHBseSIsInVucmVhZCIsInJvb20iLCJyb29tSWQiLCJVbnJlYWQiLCJkb2VzUm9vbUhhdmVVbnJlYWRNZXNzYWdlcyIsImN1cnJlbnRJbmRleCIsImZpbmRJbmRleCIsImRlbHRhIiwibGVuZ3RoIiwiZGlzcGF0Y2giLCJzaG93X3Jvb21fdGlsZSIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwidW5yZWdpc3RlciIsInJlbW92ZUxpc3RlbmVyIiwicmVtb3ZlIiwiY2FuY2VsUGVuZGluZ0NhbGwiLCJBcnJheSIsImlzQXJyYXkiLCJldiIsImdldFR5cGUiLCJvbkFyY2hpdmVkSGVhZGVyQ2xpY2siLCJpc0hpZGRlbiIsInNjcm9sbFRvUG9zaXRpb24iLCJzZWxmIiwic3luY0xlZnRSb29tcyIsImNhdGNoIiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwiZmluYWxseSIsInJlY2VpcHRFdmVudCIsIlJlY2VpcHQiLCJmaW5kUmVhZFJlY2VpcHRGcm9tVXNlcklkIiwiY3JlZGVudGlhbHMiLCJ1c2VySWQiLCJtZW1iZXIiLCJncm91cCIsImZvcmNlVXBkYXRlIiwib25Nb3VzZU1vdmUiLCJUaW1lciIsInN0YXJ0IiwiZmluaXNoZWQiLCJyZXN0YXJ0Iiwib25Nb3VzZUxlYXZlIiwiYWJvcnQiLCJnZXRHcm91cFJvb21zIiwicHVzaCIsImdldEdyb3VwTWVtYmVycyIsImdldERNUm9vbXNGb3JVc2VySWQiLCJnZXRTZWxlY3RlZFRhZ3MiLCJ2aXNpYmxlR3JvdXBSb29tcyIsInJvb21TZXQiLCJTZXQiLCJnZXRSb29tIiwiYWRkIiwiZnJvbSIsImdldFJvb21MaXN0cyIsInRvdGFsUm9vbXMiLCJsIiwidmFsdWVzIiwibXlVc2VySWQiLCJnZXRVc2VySWQiLCJSb29tcyIsImlzQ29uZkNhbGxSb29tIiwiZmlsdGVyZWRMaXN0cyIsImlzUm9vbVZpc2libGUiLCJyIiwiZmlsdGVyZWRSb29tcyIsInRhZ2dlZFJvb20iLCJCb29sZWFuIiwiX2dldFNjcm9sbE5vZGUiLCJwYW5lbCIsIlJlYWN0RE9NIiwiZmluZERPTU5vZGUiLCJjbGFzc0xpc3QiLCJjb250YWlucyIsImNoaWxkcmVuIiwiX3doZW5TY3JvbGxpbmciLCJlIiwiX2hpZGVUb29sdGlwIiwic3R5bGUiLCJkaXNwbGF5IiwiZmlyc3RUaW1lIiwiaW5jb21pbmdDYWxsQm94IiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInBhcmVudEVsZW1lbnQiLCJzY3JvbGxBcmVhIiwic2Nyb2xsQXJlYU9mZnNldCIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsInRvcCIsInBhZ2VZT2Zmc2V0Iiwic2Nyb2xsQXJlYUhlaWdodCIsImhlaWdodCIsImJvdHRvbU1hcmdpbiIsImxlZnQiLCJvZmZzZXRMZWZ0Iiwib2Zmc2V0V2lkdGgiLCJfbWFrZUdyb3VwSW52aXRlVGlsZXMiLCJyZXQiLCJsY0ZpbHRlciIsInRvTG93ZXJDYXNlIiwiR3JvdXBJbnZpdGVUaWxlIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZ2V0R3JvdXBzIiwiZ3JvdXBJZCIsIm5hbWUiLCJteU1lbWJlcnNoaXAiLCJpbmNsdWRlcyIsImxpc3QiLCJmdXp6eUZpbHRlciIsInV0aWxzIiwicmVtb3ZlSGlkZGVuQ2hhcnMiLCJnZXRDYW5vbmljYWxBbGlhcyIsImdldEFsdEFsaWFzZXMiLCJzb21lIiwiYWxpYXMiLCJfaGFuZGxlQ29sbGFwc2VkU3RhdGUiLCJjb2xsYXBzZVNlY3Rpb24iLCJleHBhbmRTZWN0aW9uIiwiY2hlY2tPdmVyZmxvdyIsIl9zdWJMaXN0UmVmIiwicmVmIiwiX21hcFN1Ykxpc3RQcm9wcyIsInN1Ykxpc3RzUHJvcHMiLCJkZWZhdWx0UHJvcHMiLCJpc0ZpbHRlcmVkIiwicCIsImxlbiIsImV4dHJhVGlsZXMiLCJvbkFkZFJvb20iLCJyZWR1Y2UiLCJjb21wb25lbnRzIiwiaSIsImlzTGFzdCIsImxhYmVsIiwib25IZWFkZXJDbGljayIsIm90aGVyUHJvcHMiLCJjaG9zZW5LZXkiLCJvblN1Ykxpc3RIZWFkZXJDbGljayIsInN0YXJ0QXNIaWRkZW4iLCJpZCIsImNvdW50IiwiYmluZCIsIl9jb2xsZWN0UmVzaXplQ29udGFpbmVyIiwiZWwiLCJyZW5kZXIiLCJpbmNvbWluZ0NhbGxJZlRhZ2dlZEFzIiwic3ViTGlzdHMiLCJpc0ludml0ZSIsImFkZFJvb21MYWJlbCIsInRhZ1N1Ykxpc3RzIiwic2hvd1NwaW5uZXIiLCJzdWJMaXN0Q29tcG9uZW50cyIsIm9uS2V5RG93biIsIm9uS2V5RG93bkhhbmRsZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsTUFBTUEscUJBQXFCLEdBQUcsSUFBOUI7QUFDQSxNQUFNQyxtQkFBbUIsR0FBRyxnR0FBNUI7QUFDQSxNQUFNQyxrQkFBa0IsR0FBRyxJQUEzQjs7QUFFQSxTQUFTQyxlQUFULENBQXlCQyxPQUF6QixFQUFrQztBQUM5QixNQUFJQSxPQUFPLENBQUNDLFVBQVIsQ0FBbUIsSUFBbkIsQ0FBSixFQUE4QixPQUFPRCxPQUFPLENBQUNFLEtBQVIsQ0FBYyxDQUFkLENBQVA7QUFDOUIsU0FBT0YsT0FBUDtBQUNIOztlQUVjLCtCQUFpQjtBQUM1QkcsRUFBQUEsV0FBVyxFQUFFLFVBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxpQkFBaUIsRUFBRUMsbUJBQVVDLEdBRHRCO0FBRVBDLElBQUFBLFNBQVMsRUFBRUYsbUJBQVVHLElBQVYsQ0FBZUMsVUFGbkI7QUFHUEMsSUFBQUEsWUFBWSxFQUFFTCxtQkFBVU07QUFIakIsR0FIaUI7QUFTNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBRXhCLFNBQUtDLGdCQUFMLEdBQXdCLElBQXhCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixDQUNoQjtBQURnQixLQUFwQjtBQUlBLFVBQU1DLFNBQVMsR0FBR0MsTUFBTSxDQUFDQyxZQUFQLENBQW9CQyxPQUFwQixDQUE0QixtQkFBNUIsQ0FBbEI7QUFDQSxVQUFNQyxhQUFhLEdBQUdILE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQkMsT0FBcEIsQ0FBNEIsdUJBQTVCLENBQXRCO0FBQ0EsU0FBS0UsWUFBTCxHQUFvQkwsU0FBUyxHQUFHTSxJQUFJLENBQUNDLEtBQUwsQ0FBV1AsU0FBWCxDQUFILEdBQTJCLEVBQXhEO0FBQ0EsU0FBS1EsY0FBTCxHQUFzQkosYUFBYSxHQUFHRSxJQUFJLENBQUNDLEtBQUwsQ0FBV0gsYUFBWCxDQUFILEdBQStCLEVBQWxFO0FBQ0EsU0FBS0ssZUFBTCxHQUF1QixFQUF2QjtBQUVBLFVBQU1DLGlCQUFpQixHQUFHO0FBQ3RCQyxNQUFBQSxlQUFlLEVBQUUsS0FESztBQUV0QkMsTUFBQUEsWUFBWSxFQUFFO0FBRlEsS0FBMUI7QUFJQSxTQUFLQyxpQkFBTCxHQUF5QixJQUFJQyxtQkFBSixDQUFXLENBQUNDLEdBQUQsRUFBTUMsSUFBTixLQUFlO0FBQy9DLFlBQU1DLE9BQU8sR0FBRyxLQUFLbEIsWUFBTCxDQUFrQmdCLEdBQWxCLENBQWhCOztBQUNBLFVBQUlFLE9BQUosRUFBYTtBQUNUQSxRQUFBQSxPQUFPLENBQUNDLFNBQVIsQ0FBa0JGLElBQWxCO0FBQ0gsT0FKOEMsQ0FLL0M7OztBQUNBLFdBQUtHLHNCQUFMLEdBTitDLENBTy9DOzs7QUFDQSxVQUFJLENBQUMsS0FBS1gsY0FBTCxDQUFvQk8sR0FBcEIsQ0FBTCxFQUErQjtBQUMzQixhQUFLVixZQUFMLENBQWtCVSxHQUFsQixJQUF5QkMsSUFBekI7QUFDQWYsUUFBQUEsTUFBTSxDQUFDQyxZQUFQLENBQW9Ca0IsT0FBcEIsQ0FBNEIsbUJBQTVCLEVBQ0lkLElBQUksQ0FBQ2UsU0FBTCxDQUFlLEtBQUtoQixZQUFwQixDQURKO0FBRUg7QUFDSixLQWJ3QixFQWF0QixLQUFLQSxZQWJpQixFQWFILEtBQUtHLGNBYkYsRUFha0JFLGlCQWJsQixDQUF6QjtBQWVBLFNBQUtZLGVBQUwsR0FBdUIsSUFBSVIsbUJBQUosQ0FBVyxDQUFDQyxHQUFELEVBQU1DLElBQU4sS0FBZTtBQUM3QyxZQUFNQyxPQUFPLEdBQUcsS0FBS2xCLFlBQUwsQ0FBa0JnQixHQUFsQixDQUFoQjs7QUFDQSxVQUFJRSxPQUFKLEVBQWE7QUFDVEEsUUFBQUEsT0FBTyxDQUFDQyxTQUFSLENBQWtCRixJQUFsQjtBQUNIO0FBQ0osS0FMc0IsRUFLcEIsSUFMb0IsRUFLZCxJQUxjLEVBS1I7QUFDWEwsTUFBQUEsZUFBZSxFQUFFLEtBRE47QUFFWEMsTUFBQUEsWUFBWSxFQUFFO0FBRkgsS0FMUSxDQUF2QjtBQVVBLFNBQUtXLE9BQUwsR0FBZSxLQUFLVixpQkFBcEI7QUFFQSxXQUFPO0FBQ0hXLE1BQUFBLGtCQUFrQixFQUFFLEtBRGpCO0FBRUhDLE1BQUFBLGNBQWMsRUFBRSxJQUZiO0FBR0hDLE1BQUFBLEtBQUssRUFBRSxFQUhKO0FBSUhDLE1BQUFBLGVBQWUsRUFBRSxJQUpkO0FBS0hDLE1BQUFBLFlBQVksRUFBRSxJQUxYO0FBTUhDLE1BQUFBLFlBQVksRUFBRSxFQU5YO0FBT0hDLE1BQUFBLEtBQUssRUFBRSxLQVBKO0FBUUhDLE1BQUFBLFVBQVUsRUFBRUMsNEJBQW1CQyxPQUFuQjtBQVJULEtBQVA7QUFVSCxHQS9EMkI7QUFpRTVCO0FBQ0FDLEVBQUFBLHlCQUF5QixFQUFFLFlBQVc7QUFDbEMsU0FBS0MsT0FBTCxHQUFlLEtBQWY7O0FBRUEsVUFBTUMsR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBRUFGLElBQUFBLEdBQUcsQ0FBQ0csRUFBSixDQUFPLE1BQVAsRUFBZSxLQUFLQyxNQUFwQjtBQUNBSixJQUFBQSxHQUFHLENBQUNHLEVBQUosQ0FBTyxZQUFQLEVBQXFCLEtBQUtFLFlBQTFCO0FBQ0FMLElBQUFBLEdBQUcsQ0FBQ0csRUFBSixDQUFPLGNBQVAsRUFBdUIsS0FBS0csYUFBNUI7QUFDQU4sSUFBQUEsR0FBRyxDQUFDRyxFQUFKLENBQU8saUJBQVAsRUFBMEIsS0FBS0ksZ0JBQS9CO0FBQ0FQLElBQUFBLEdBQUcsQ0FBQ0csRUFBSixDQUFPLGlCQUFQLEVBQTBCLEtBQUtLLGdCQUEvQjtBQUNBUixJQUFBQSxHQUFHLENBQUNHLEVBQUosQ0FBTyxhQUFQLEVBQXNCLEtBQUtNLGFBQTNCO0FBQ0FULElBQUFBLEdBQUcsQ0FBQ0csRUFBSixDQUFPLG9CQUFQLEVBQTZCLEtBQUtPLG9CQUFsQztBQUNBVixJQUFBQSxHQUFHLENBQUNHLEVBQUosQ0FBTyxrQkFBUCxFQUEyQixLQUFLUSxpQkFBaEM7O0FBRUEsVUFBTUMsU0FBUyxHQUFHQyxtQkFBVUMsTUFBVixFQUFsQixDQWRrQyxDQWVsQztBQUNBOzs7QUFDQSxTQUFLQyxxQkFBTCxHQUE2QixDQUN6QjtBQUR5QixLQUE3QixDQWpCa0MsQ0FvQmxDO0FBQ0E7O0FBQ0EsU0FBS0MsYUFBTCxHQUFxQmYsaUNBQWdCQyxHQUFoQixHQUFzQmUsZUFBdEIsRUFBckIsQ0F0QmtDLENBd0JsQztBQUNBOztBQUNBLFNBQUtDLGdCQUFMLEdBQXdCQyxvQkFBV0MsZ0JBQVgsQ0FBNEIsSUFBNUIsRUFBa0MsTUFBTTtBQUM1RCxPQUFDQyx1QkFBY0MsY0FBZCxNQUFrQyxFQUFuQyxFQUF1Q0MsT0FBdkMsQ0FBZ0RDLEdBQUQsSUFBUztBQUNwRCxZQUFJQSxHQUFHLENBQUMsQ0FBRCxDQUFILEtBQVcsR0FBZixFQUFvQjtBQUNoQjtBQUNILFNBSG1ELENBSXBEOzs7QUFDQSxhQUFLQyx3QkFBTCxDQUE4QmIsU0FBOUIsRUFBeUNZLEdBQXpDO0FBQ0EsYUFBS0Usa0JBQUw7QUFDSCxPQVBEO0FBUUgsS0FUdUIsQ0FBeEI7QUFXQSxTQUFLQyxjQUFMLEdBQXNCTix1QkFBY08sV0FBZCxDQUEwQixNQUFNO0FBQ2xEO0FBQ0EsV0FBS0Ysa0JBQUw7QUFDSCxLQUhxQixDQUF0QjtBQUtBLFNBQUtHLG1CQUFMLEdBQTJCQyx1QkFBY0YsV0FBZCxDQUEwQixNQUFNO0FBQ3ZELFdBQUtHLHVCQUFMO0FBQ0gsS0FGMEIsQ0FBM0I7O0FBS0EsUUFBSUMsdUJBQWNDLGdCQUFkLENBQStCLHFCQUEvQixDQUFKLEVBQTJEO0FBQ3ZELFdBQUtDLG9CQUFMLEdBQTRCdEMsNEJBQW1CZ0MsV0FBbkIsQ0FBK0IsTUFBTTtBQUM3RCxhQUFLTyxRQUFMLENBQWM7QUFDVnhDLFVBQUFBLFVBQVUsRUFBRUMsNEJBQW1CQyxPQUFuQjtBQURGLFNBQWQ7QUFHSCxPQUoyQixDQUE1QjtBQUtIOztBQUVELFNBQUt1QyxlQUFMLEdBdkRrQyxDQXlEbEM7QUFDQTtBQUVBO0FBQ0E7O0FBQ0EsU0FBS0MsZ0NBQUwsR0FBd0MsQ0FBeEM7QUFDSCxHQWpJMkI7QUFtSTVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS0MsUUFBbEIsQ0FBckI7QUFDQSxVQUFNQyxHQUFHLEdBQUc7QUFDUkMsTUFBQUEsU0FBUyxFQUFFLE1BQU0sS0FBS3pEO0FBRGQsS0FBWjtBQUdBLFNBQUswRCxPQUFMLEdBQWUsSUFBSUMsZ0JBQUosQ0FBWSxLQUFLQyxlQUFqQixFQUFrQ0Msd0JBQWxDLEVBQStDTCxHQUEvQyxDQUFmO0FBQ0EsU0FBS0UsT0FBTCxDQUFhSSxhQUFiLENBQTJCO0FBQ3ZCQyxNQUFBQSxNQUFNLEVBQUUsaUJBRGU7QUFFdkJDLE1BQUFBLFFBQVEsRUFBRSwwQkFGYTtBQUd2QkMsTUFBQUEsT0FBTyxFQUFFO0FBSGMsS0FBM0I7O0FBS0EsU0FBS2pFLE9BQUwsQ0FBYWtFLE1BQWIsQ0FDSSxLQUFLaEYsZUFEVCxFQUVJLEtBQUswRSxlQUFMLElBQXdCLEtBQUtBLGVBQUwsQ0FBcUJPLFlBRmpEOztBQUlBLFNBQUt2RSxzQkFBTDs7QUFFQSxTQUFLOEQsT0FBTCxDQUFhVSxNQUFiOztBQUNBLFFBQUksS0FBS0MsS0FBTCxDQUFXQyxjQUFmLEVBQStCO0FBQzNCLFdBQUtELEtBQUwsQ0FBV0MsY0FBWCxDQUEwQnRELEVBQTFCLENBQTZCLGtCQUE3QixFQUFpRCxLQUFLdUQsUUFBdEQ7QUFDSDs7QUFDRCxTQUFLM0QsT0FBTCxHQUFlLElBQWY7QUFDSCxHQXpKMkI7QUEySjVCNEQsRUFBQUEsa0JBQWtCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQjtBQUNwQyxRQUFJQyxpQkFBaUIsR0FBRyxLQUF4Qjs7QUFDQSxTQUFLQywwQkFBTCxDQUFnQ0MsU0FBaEMsRUFBMkMsS0FBM0M7O0FBQ0EsUUFBSSxDQUFDLEtBQUtQLEtBQUwsQ0FBV2pHLFlBQVosSUFBNEJxRyxTQUFTLENBQUNyRyxZQUExQyxFQUF3RDtBQUNwRCxXQUFLNEIsT0FBTCxHQUFlLEtBQUtWLGlCQUFwQjtBQUNBb0YsTUFBQUEsaUJBQWlCLEdBQUcsSUFBcEI7QUFDSCxLQUhELE1BR08sSUFBSSxLQUFLTCxLQUFMLENBQVdqRyxZQUFYLElBQTJCLENBQUNxRyxTQUFTLENBQUNyRyxZQUExQyxFQUF3RDtBQUMzRCxXQUFLNEIsT0FBTCxHQUFlLEtBQUtELGVBQXBCO0FBQ0EyRSxNQUFBQSxpQkFBaUIsR0FBRyxJQUFwQjtBQUNIOztBQUNELFNBQUsxRSxPQUFMLENBQWFrRSxNQUFiLENBQ0ksS0FBS2hGLGVBRFQsRUFFSSxLQUFLMEUsZUFBTCxJQUF3QixLQUFLQSxlQUFMLENBQXFCaUIsWUFGakQsRUFHSUgsaUJBSEo7O0FBS0EsU0FBSzlFLHNCQUFMO0FBQ0gsR0EzSzJCO0FBNks1QjJELEVBQUFBLFFBQVEsRUFBRSxVQUFTdUIsT0FBVCxFQUFrQjtBQUN4QixZQUFRQSxPQUFPLENBQUNDLE1BQWhCO0FBQ0ksV0FBSyxjQUFMO0FBQ0ksYUFBS0MsT0FBTCxHQUFlRixPQUFPLENBQUNFLE9BQXZCO0FBQ0E7O0FBQ0osV0FBSyxZQUFMO0FBQ0ksWUFBSUMsSUFBSSxHQUFHQyxxQkFBWUMsT0FBWixDQUFvQkwsT0FBTyxDQUFDTSxPQUE1QixDQUFYOztBQUNBLFlBQUlILElBQUksSUFBSUEsSUFBSSxDQUFDSSxVQUFMLEtBQW9CLFNBQWhDLEVBQTJDO0FBQ3ZDLGVBQUtyQyxRQUFMLENBQWM7QUFDVjNDLFlBQUFBLFlBQVksRUFBRTRFLElBREo7QUFFVjdFLFlBQUFBLGVBQWUsRUFBRSxLQUFLa0YsbUJBQUwsQ0FBeUJSLE9BQU8sQ0FBQ00sT0FBakM7QUFGUCxXQUFkOztBQUlBLGVBQUtULDBCQUFMLENBQWdDQyxTQUFoQyxFQUEyQyxJQUEzQztBQUNILFNBTkQsTUFNTztBQUNILGVBQUs1QixRQUFMLENBQWM7QUFDVjNDLFlBQUFBLFlBQVksRUFBRSxJQURKO0FBRVZELFlBQUFBLGVBQWUsRUFBRTtBQUZQLFdBQWQ7QUFJSDs7QUFDRDs7QUFDSixXQUFLLGlCQUFMO0FBQXdCO0FBQ3BCLGdCQUFNbUYsYUFBYSxHQUFHQyx1QkFBY0MsU0FBZCxFQUF0Qjs7QUFDQSxvQ0FTSSxLQUFLQyxLQUFMLENBQVd2RixLQVRmO0FBQUEsZ0JBQU07QUFDRixxQ0FBeUJ3RixXQUR2QjtBQUVGLDJCQUFlQyxjQUZiO0FBR0YsYUFBQ0MscUJBQUQsR0FBVUMsT0FIUjtBQUlGLHFDQUF5QkMsV0FKdkI7QUFLRiw2QkFBaUJDLGdCQUxmO0FBTUYsdUNBQTJCQyxlQU56QjtBQU9GLCtCQUFtQkM7QUFQakIsV0FBTjtBQUFBLGdCQVFPQyxJQVJQLHNHQUdLTixxQkFITDtBQVdBLGdCQUFNTyxtQkFBbUIsR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQVosRUFBa0JJLE1BQWxCLENBQXlCOUksT0FBTyxJQUFJO0FBQzVELG1CQUFPLENBQUMsQ0FBQyxLQUFLaUksS0FBTCxDQUFXbEYsVUFBWixJQUEwQixLQUFLa0YsS0FBTCxDQUFXbEYsVUFBWCxDQUFzQi9DLE9BQXRCLENBQTNCLEtBQ0gsQ0FBQ0EsT0FBTyxDQUFDK0ksS0FBUixDQUFjbEosbUJBQWQsQ0FETDtBQUVILFdBSDJCLEVBR3pCbUosR0FIeUIsQ0FHckJoSixPQUFPLElBQUkwSSxJQUFJLENBQUMxSSxPQUFELENBSE0sQ0FBNUIsQ0Fib0IsQ0FrQnBCOztBQUNBLGNBQUlpSixLQUFLLEdBQUcsS0FBS0Msa0JBQUwsQ0FBd0IsQ0FDaEMsR0FBR2hCLFdBRDZCLEVBRWhDLEdBQUdDLGNBRjZCLEVBR2hDLEdBQUdFLE9BSDZCLEVBSWhDLEdBQUdDLFdBSjZCLEVBS2hDLEdBQUcsR0FBR2EsTUFBSCxDQUFVQyxLQUFWLENBQWdCLEVBQWhCLEVBQW9CVCxtQkFBcEIsQ0FMNkIsRUFLYTtBQUM3QyxhQUFHSixnQkFONkIsRUFPaEMsR0FBR0MsZUFQNkIsRUFRaEMsR0FBR0MsaUJBUjZCLENBQXhCLEVBU1QsS0FBSzdCLEtBQUwsQ0FBV2pHLFlBVEYsQ0FBWjs7QUFXQSxjQUFJMEcsT0FBTyxDQUFDZ0MsTUFBWixFQUFvQjtBQUNoQjtBQUNBSixZQUFBQSxLQUFLLEdBQUdBLEtBQUssQ0FBQ0gsTUFBTixDQUFhUSxJQUFJLElBQUk7QUFDekIscUJBQU9BLElBQUksQ0FBQ0MsTUFBTCxLQUFnQnpCLGFBQWhCLElBQWlDMEIsTUFBTSxDQUFDQywwQkFBUCxDQUFrQ0gsSUFBbEMsQ0FBeEM7QUFDSCxhQUZPLENBQVI7QUFHSDs7QUFFRCxnQkFBTUksWUFBWSxHQUFHVCxLQUFLLENBQUNVLFNBQU4sQ0FBZ0JMLElBQUksSUFBSUEsSUFBSSxDQUFDQyxNQUFMLEtBQWdCekIsYUFBeEMsQ0FBckIsQ0FyQ29CLENBc0NwQjs7QUFDQSxnQkFBTSxDQUFDd0IsSUFBRCxJQUFTTCxLQUFLLENBQUMvSSxLQUFOLENBQVksQ0FBQ3dKLFlBQVksR0FBR3JDLE9BQU8sQ0FBQ3VDLEtBQXhCLElBQWlDWCxLQUFLLENBQUNZLE1BQW5ELENBQWY7O0FBQ0EsY0FBSVAsSUFBSixFQUFVO0FBQ04xRCxnQ0FBSWtFLFFBQUosQ0FBYTtBQUNUeEMsY0FBQUEsTUFBTSxFQUFFLFdBREM7QUFFVEssY0FBQUEsT0FBTyxFQUFFMkIsSUFBSSxDQUFDQyxNQUZMO0FBR1RRLGNBQUFBLGNBQWMsRUFBRSxJQUhQLENBR2E7O0FBSGIsYUFBYjtBQUtIOztBQUNEO0FBQ0g7QUFuRUw7QUFxRUgsR0FuUDJCO0FBcVA1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QixTQUFLN0csT0FBTCxHQUFlLEtBQWY7O0FBRUF5Qyx3QkFBSXFFLFVBQUosQ0FBZSxLQUFLdEUsYUFBcEI7O0FBQ0EsUUFBSXRDLGlDQUFnQkMsR0FBaEIsRUFBSixFQUEyQjtBQUN2QkQsdUNBQWdCQyxHQUFoQixHQUFzQjRHLGNBQXRCLENBQXFDLE1BQXJDLEVBQTZDLEtBQUsxRyxNQUFsRDs7QUFDQUgsdUNBQWdCQyxHQUFoQixHQUFzQjRHLGNBQXRCLENBQXFDLFlBQXJDLEVBQW1ELEtBQUt6RyxZQUF4RDs7QUFDQUosdUNBQWdCQyxHQUFoQixHQUFzQjRHLGNBQXRCLENBQXFDLGNBQXJDLEVBQXFELEtBQUt4RyxhQUExRDs7QUFDQUwsdUNBQWdCQyxHQUFoQixHQUFzQjRHLGNBQXRCLENBQXFDLGlCQUFyQyxFQUF3RCxLQUFLdkcsZ0JBQTdEOztBQUNBTix1Q0FBZ0JDLEdBQWhCLEdBQXNCNEcsY0FBdEIsQ0FBcUMsaUJBQXJDLEVBQXdELEtBQUt0RyxnQkFBN0Q7O0FBQ0FQLHVDQUFnQkMsR0FBaEIsR0FBc0I0RyxjQUF0QixDQUFxQyxhQUFyQyxFQUFvRCxLQUFLckcsYUFBekQ7O0FBQ0FSLHVDQUFnQkMsR0FBaEIsR0FBc0I0RyxjQUF0QixDQUFxQyxvQkFBckMsRUFBMkQsS0FBS3BHLG9CQUFoRTs7QUFDQVQsdUNBQWdCQyxHQUFoQixHQUFzQjRHLGNBQXRCLENBQXFDLGtCQUFyQyxFQUF5RCxLQUFLbkcsaUJBQTlEO0FBQ0g7O0FBRUQsUUFBSSxLQUFLNkMsS0FBTCxDQUFXQyxjQUFmLEVBQStCO0FBQzNCLFdBQUtELEtBQUwsQ0FBV0MsY0FBWCxDQUEwQnFELGNBQTFCLENBQXlDLGtCQUF6QyxFQUE2RCxLQUFLcEQsUUFBbEU7QUFDSDs7QUFHRCxRQUFJLEtBQUsvQixjQUFULEVBQXlCO0FBQ3JCLFdBQUtBLGNBQUwsQ0FBb0JvRixNQUFwQjtBQUNIOztBQUVELFFBQUksS0FBS2xGLG1CQUFULEVBQThCO0FBQzFCLFdBQUtBLG1CQUFMLENBQXlCa0YsTUFBekI7QUFDSDs7QUFDRCxRQUFJLEtBQUs3RSxvQkFBVCxFQUErQjtBQUMzQixXQUFLQSxvQkFBTCxDQUEwQjZFLE1BQTFCO0FBQ0gsS0E3QjRCLENBK0I3Qjs7O0FBQ0EsUUFBSSxLQUFLN0YsZ0JBQVQsRUFBMkI7QUFDdkIsV0FBS0EsZ0JBQUwsQ0FBc0IyRixVQUF0QjtBQUNILEtBbEM0QixDQW9DN0I7OztBQUNBLFNBQUs5RSx1QkFBTCxDQUE2QmlGLGlCQUE3QjtBQUNILEdBM1IyQjtBQThSNUJ0RCxFQUFBQSxRQUFRLEVBQUUsWUFBVztBQUNqQixRQUFJLEtBQUszRCxPQUFMLElBQWdCLEtBQUtaLE9BQXJCLElBQWdDLEtBQUs0RCxlQUFyQyxJQUNBa0UsS0FBSyxDQUFDQyxPQUFOLENBQWMsS0FBSzdJLGVBQW5CLENBREosRUFFRTtBQUNFLFdBQUtjLE9BQUwsQ0FBYWtFLE1BQWIsQ0FDSSxLQUFLaEYsZUFEVCxFQUVJLEtBQUswRSxlQUFMLENBQXFCTyxZQUZ6QjtBQUlIO0FBQ0osR0F2UzJCO0FBeVM1QmxELEVBQUFBLE1BQU0sRUFBRSxVQUFTOEYsSUFBVCxFQUFlO0FBQ25CLFNBQUt4RSxrQkFBTDtBQUNILEdBM1MyQjtBQTZTNUJmLEVBQUFBLGlCQUFpQixFQUFFLFVBQVN3RyxFQUFULEVBQWF0QyxLQUFiLEVBQW9CO0FBQ25DLFFBQUlzQyxFQUFFLENBQUNDLE9BQUgsT0FBaUIsZUFBakIsSUFBb0NELEVBQUUsQ0FBQ0MsT0FBSCxPQUFpQixrQkFBekQsRUFBNkU7QUFDekUsV0FBSzFGLGtCQUFMO0FBQ0g7QUFDSixHQWpUMkI7QUFtVDVCckIsRUFBQUEsWUFBWSxFQUFFLFVBQVM4RixNQUFULEVBQWlCO0FBQzNCLFNBQUt6RSxrQkFBTDtBQUNILEdBclQyQjtBQXVUNUIyRixFQUFBQSxxQkFBcUIsRUFBRSxVQUFTQyxRQUFULEVBQW1CQyxnQkFBbkIsRUFBcUM7QUFDeEQsUUFBSSxDQUFDRCxRQUFMLEVBQWU7QUFDWCxZQUFNRSxJQUFJLEdBQUcsSUFBYjtBQUNBLFdBQUtyRixRQUFMLENBQWM7QUFBRS9DLFFBQUFBLGtCQUFrQixFQUFFO0FBQXRCLE9BQWQsRUFGVyxDQUdYO0FBQ0E7O0FBQ0FhLHVDQUFnQkMsR0FBaEIsR0FBc0J1SCxhQUF0QixHQUFzQ0MsS0FBdEMsQ0FBNEMsVUFBU0MsR0FBVCxFQUFjO0FBQ3REQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYywrQkFBZCxFQUErQ0YsR0FBL0M7QUFDQUMsUUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWNGLEdBQWQ7QUFDSCxPQUhELEVBR0dHLE9BSEgsQ0FHVyxZQUFXO0FBQ2xCTixRQUFBQSxJQUFJLENBQUNyRixRQUFMLENBQWM7QUFBRS9DLFVBQUFBLGtCQUFrQixFQUFFO0FBQXRCLFNBQWQ7QUFDSCxPQUxEO0FBTUg7QUFDSixHQXBVMkI7QUFzVTVCa0IsRUFBQUEsYUFBYSxFQUFFLFVBQVN5SCxZQUFULEVBQXVCN0IsSUFBdkIsRUFBNkI7QUFDeEM7QUFDQTtBQUNBLFFBQUk4QixPQUFPLENBQUNDLHlCQUFSLENBQWtDRixZQUFsQyxFQUFnRDlILGlDQUFnQkMsR0FBaEIsR0FBc0JnSSxXQUF0QixDQUFrQ0MsTUFBbEYsQ0FBSixFQUErRjtBQUMzRixXQUFLcEcsdUJBQUw7QUFDSDtBQUNKLEdBNVUyQjtBQThVNUJ4QixFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTNEcsRUFBVCxFQUFhaUIsTUFBYixFQUFxQjtBQUNuQyxTQUFLckcsdUJBQUw7QUFDSCxHQWhWMkI7QUFrVjVCdkIsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBUzJHLEVBQVQsRUFBYTtBQUMzQjtBQUNBLFNBQUtwRix1QkFBTDtBQUNILEdBclYyQjtBQXVWNUJ0QixFQUFBQSxhQUFhLEVBQUUsVUFBUzBHLEVBQVQsRUFBYTtBQUN4QixRQUFJQSxFQUFFLENBQUNDLE9BQUgsTUFBZ0IsVUFBcEIsRUFBZ0M7QUFDNUIsV0FBS3JGLHVCQUFMO0FBQ0g7QUFDSixHQTNWMkI7QUE2VjVCckIsRUFBQUEsb0JBQW9CLEVBQUUsVUFBUzJILEtBQVQsRUFBZ0I7QUFDbEMsU0FBS0MsV0FBTDtBQUNILEdBL1YyQjtBQWlXNUJDLEVBQUFBLFdBQVcsRUFBRSxnQkFBZXBCLEVBQWYsRUFBbUI7QUFDNUIsUUFBSSxDQUFDLEtBQUt6SixnQkFBVixFQUE0QjtBQUN4QixXQUFLeUUsUUFBTCxDQUFjO0FBQUN6QyxRQUFBQSxLQUFLLEVBQUU7QUFBUixPQUFkO0FBQ0EsV0FBS2hDLGdCQUFMLEdBQXdCLElBQUk4SyxjQUFKLENBQVU5TCxrQkFBVixDQUF4Qjs7QUFDQSxXQUFLZ0IsZ0JBQUwsQ0FBc0IrSyxLQUF0Qjs7QUFDQSxVQUFJQyxRQUFRLEdBQUcsSUFBZjs7QUFDQSxVQUFJO0FBQ0EsY0FBTSxLQUFLaEwsZ0JBQUwsQ0FBc0JnTCxRQUF0QixFQUFOO0FBQ0gsT0FGRCxDQUVFLE9BQU9mLEdBQVAsRUFBWTtBQUNWZSxRQUFBQSxRQUFRLEdBQUcsS0FBWDtBQUNIOztBQUNELFdBQUtoTCxnQkFBTCxHQUF3QixJQUF4Qjs7QUFDQSxVQUFJZ0wsUUFBSixFQUFjO0FBQ1YsYUFBS3ZHLFFBQUwsQ0FBYztBQUFDekMsVUFBQUEsS0FBSyxFQUFFO0FBQVIsU0FBZDs7QUFDQSxhQUFLcUMsdUJBQUw7QUFDSDtBQUNKLEtBZkQsTUFlTztBQUNILFdBQUtyRSxnQkFBTCxDQUFzQmlMLE9BQXRCO0FBQ0g7QUFDSixHQXBYMkI7QUFzWDVCQyxFQUFBQSxZQUFZLEVBQUUsVUFBU3pCLEVBQVQsRUFBYTtBQUN2QixRQUFJLEtBQUt6SixnQkFBVCxFQUEyQjtBQUN2QixXQUFLQSxnQkFBTCxDQUFzQm1MLEtBQXRCOztBQUNBLFdBQUtuTCxnQkFBTCxHQUF3QixJQUF4QjtBQUNIOztBQUNELFNBQUt5RSxRQUFMLENBQWM7QUFBQ3pDLE1BQUFBLEtBQUssRUFBRTtBQUFSLEtBQWQsRUFMdUIsQ0FPdkI7O0FBQ0EsU0FBS3FDLHVCQUFMO0FBQ0gsR0EvWDJCO0FBaVk1QkEsRUFBQUEsdUJBQXVCLEVBQUUsOEJBQWtCLFlBQVc7QUFDbEQsU0FBS0ssZUFBTDtBQUNILEdBRndCLEVBRXRCLEdBRnNCLENBallHO0FBcVk1QjtBQUNBWCxFQUFBQSx3QkFBd0IsRUFBRSxVQUFTYixTQUFULEVBQW9CWSxHQUFwQixFQUF5QjtBQUMvQyxRQUFJLENBQUMsS0FBS3pCLE9BQVYsRUFBbUIsT0FENEIsQ0FFL0M7O0FBQ0EsUUFBSXlCLEdBQUcsQ0FBQyxDQUFELENBQUgsS0FBVyxHQUFmLEVBQW9CO0FBRXBCLFNBQUtULHFCQUFMLENBQTJCUyxHQUEzQixJQUFrQyxFQUFsQzs7QUFDQUwsd0JBQVcySCxhQUFYLENBQXlCdEgsR0FBekIsRUFBOEJELE9BQTlCLENBQXVDMkUsSUFBRCxJQUFVLEtBQUtuRixxQkFBTCxDQUEyQlMsR0FBM0IsRUFBZ0N1SCxJQUFoQyxDQUFxQzdDLElBQUksQ0FBQ0MsTUFBMUMsQ0FBaEQ7O0FBQ0FoRix3QkFBVzZILGVBQVgsQ0FBMkJ4SCxHQUEzQixFQUFnQ0QsT0FBaEMsQ0FBeUM2RyxNQUFELElBQVk7QUFDaEQsVUFBSUEsTUFBTSxDQUFDRCxNQUFQLEtBQWtCbEksaUNBQWdCQyxHQUFoQixHQUFzQmdJLFdBQXRCLENBQWtDQyxNQUF4RCxFQUFnRTtBQUNoRXZILE1BQUFBLFNBQVMsQ0FBQ3FJLG1CQUFWLENBQThCYixNQUFNLENBQUNELE1BQXJDLEVBQTZDNUcsT0FBN0MsQ0FDSzRFLE1BQUQsSUFBWSxLQUFLcEYscUJBQUwsQ0FBMkJTLEdBQTNCLEVBQWdDdUgsSUFBaEMsQ0FBcUM1QyxNQUFyQyxDQURoQjtBQUdILEtBTEQsRUFQK0MsQ0FhL0M7O0FBQ0gsR0FwWjJCO0FBc1o1QjtBQUNBekUsRUFBQUEsa0JBQWtCLEVBQUUsWUFBVztBQUMzQixVQUFNakMsWUFBWSxHQUFHNEIsdUJBQWM2SCxlQUFkLEVBQXJCOztBQUNBLFVBQU1DLGlCQUFpQixHQUFHLEVBQTFCO0FBQ0ExSixJQUFBQSxZQUFZLENBQUM4QixPQUFiLENBQXNCQyxHQUFELElBQVM7QUFDMUIsT0FBQyxLQUFLVCxxQkFBTCxDQUEyQlMsR0FBM0IsS0FBbUMsRUFBcEMsRUFBd0NELE9BQXhDLENBQ0s0RSxNQUFELElBQVlnRCxpQkFBaUIsQ0FBQ0osSUFBbEIsQ0FBdUI1QyxNQUF2QixDQURoQjtBQUdILEtBSkQsRUFIMkIsQ0FTM0I7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSTFHLFlBQVksQ0FBQ2dILE1BQWIsR0FBc0IsQ0FBMUIsRUFBNkI7QUFDekIsWUFBTTJDLE9BQU8sR0FBRyxJQUFJQyxHQUFKLEVBQWhCO0FBQ0FGLE1BQUFBLGlCQUFpQixDQUFDNUgsT0FBbEIsQ0FBMkI0RSxNQUFELElBQVk7QUFDbEMsY0FBTUQsSUFBSSxHQUFHakcsaUNBQWdCQyxHQUFoQixHQUFzQm9KLE9BQXRCLENBQThCbkQsTUFBOUIsQ0FBYjs7QUFDQSxZQUFJRCxJQUFKLEVBQVU7QUFDTmtELFVBQUFBLE9BQU8sQ0FBQ0csR0FBUixDQUFZckQsSUFBWjtBQUNIO0FBQ0osT0FMRDtBQU1BLFdBQUtsRixhQUFMLEdBQXFCaUcsS0FBSyxDQUFDdUMsSUFBTixDQUFXSixPQUFYLENBQXJCO0FBQ0gsS0FURCxNQVNPO0FBQ0g7QUFDQSxXQUFLcEksYUFBTCxHQUFxQmYsaUNBQWdCQyxHQUFoQixHQUFzQmUsZUFBdEIsRUFBckI7QUFDSDs7QUFDRCxTQUFLYyx1QkFBTDtBQUNILEdBbGIyQjtBQW9iNUJLLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFFBQUksS0FBS3lDLEtBQUwsQ0FBV25GLEtBQWYsRUFBc0I7QUFDbEI7QUFDQTtBQUNILEtBSnVCLENBTXhCO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxVQUFNSixLQUFLLEdBQUcsS0FBS21LLFlBQUwsRUFBZDtBQUNBLFFBQUlDLFVBQVUsR0FBRyxDQUFqQjs7QUFDQSxTQUFLLE1BQU1DLENBQVgsSUFBZ0JuRSxNQUFNLENBQUNvRSxNQUFQLENBQWN0SyxLQUFkLENBQWhCLEVBQXNDO0FBQ2xDb0ssTUFBQUEsVUFBVSxJQUFJQyxDQUFDLENBQUNsRCxNQUFoQjtBQUNIOztBQUNELFNBQUt0RSxRQUFMLENBQWM7QUFDVjdDLE1BQUFBLEtBRFU7QUFFVkQsTUFBQUEsY0FBYyxFQUFFcUssVUFGTjtBQUdWO0FBQ0E7QUFDQWpLLE1BQUFBLFlBQVksRUFBRTRCLHVCQUFjNkgsZUFBZDtBQUxKLEtBQWQsRUFNRyxNQUFNO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsV0FBS25LLHNCQUFMO0FBQ0gsS0FYRCxFQWZ3QixDQTRCeEI7QUFDSCxHQWpkMkI7QUFtZDVCMEYsRUFBQUEsbUJBQW1CLEVBQUUsVUFBUzBCLE1BQVQsRUFBaUI7QUFDbEMsVUFBTTdHLEtBQUssR0FBR3dDLHVCQUFjMkgsWUFBZCxFQUFkOztBQUNBLFNBQUssTUFBTTdNLE9BQVgsSUFBc0I0SSxNQUFNLENBQUNDLElBQVAsQ0FBWW5HLEtBQVosQ0FBdEIsRUFBMEM7QUFDdEMsV0FBSyxNQUFNNEcsSUFBWCxJQUFtQjVHLEtBQUssQ0FBQzFDLE9BQUQsQ0FBeEIsRUFBbUM7QUFDL0I7QUFDQSxZQUFJLENBQUNzSixJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNELGNBQU0yRCxRQUFRLEdBQUc1SixpQ0FBZ0JDLEdBQWhCLEdBQXNCNEosU0FBdEIsRUFBakI7O0FBQ0EsWUFBSXROLHFCQUFxQixJQUFJdU4sS0FBSyxDQUFDQyxjQUFOLENBQXFCOUQsSUFBckIsRUFBMkIyRCxRQUEzQixFQUFxQyxLQUFLckcsS0FBTCxDQUFXdkcsaUJBQWhELENBQTdCLEVBQWlHO0FBQzdGO0FBQ0g7O0FBRUQsWUFBSWlKLElBQUksQ0FBQ0MsTUFBTCxLQUFnQkEsTUFBcEIsRUFBNEIsT0FBT3ZKLE9BQVA7QUFDL0I7QUFDSjs7QUFFRCxXQUFPLElBQVA7QUFDSCxHQXJlMkI7QUF1ZTVCNk0sRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckIsVUFBTW5LLEtBQUssR0FBR3dDLHVCQUFjMkgsWUFBZCxFQUFkOztBQUVBLFVBQU1RLGFBQWEsR0FBRyxFQUF0QjtBQUVBLFVBQU1DLGFBQWEsR0FBRyxDQUNsQjtBQURrQixLQUF0Qjs7QUFJQSxTQUFLbEosYUFBTCxDQUFtQk8sT0FBbkIsQ0FBNEI0SSxDQUFELElBQU87QUFDOUJELE1BQUFBLGFBQWEsQ0FBQ0MsQ0FBQyxDQUFDaEUsTUFBSCxDQUFiLEdBQTBCLElBQTFCO0FBQ0gsS0FGRDs7QUFJQVgsSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVluRyxLQUFaLEVBQW1CaUMsT0FBbkIsQ0FBNEIzRSxPQUFELElBQWE7QUFDcEMsWUFBTXdOLGFBQWEsR0FBRzlLLEtBQUssQ0FBQzFDLE9BQUQsQ0FBTCxDQUFlOEksTUFBZixDQUF1QjJFLFVBQUQsSUFBZ0I7QUFDeEQ7QUFDQSxZQUFJLENBQUNBLFVBQUwsRUFBaUI7QUFDYjtBQUNIOztBQUNELGNBQU1SLFFBQVEsR0FBRzVKLGlDQUFnQkMsR0FBaEIsR0FBc0I0SixTQUF0QixFQUFqQjs7QUFDQSxZQUFJdE4scUJBQXFCLElBQUl1TixLQUFLLENBQUNDLGNBQU4sQ0FBcUJLLFVBQXJCLEVBQWlDUixRQUFqQyxFQUEyQyxLQUFLckcsS0FBTCxDQUFXdkcsaUJBQXRELENBQTdCLEVBQXVHO0FBQ25HO0FBQ0g7O0FBRUQsZUFBT3FOLE9BQU8sQ0FBQ0osYUFBYSxDQUFDRyxVQUFVLENBQUNsRSxNQUFaLENBQWQsQ0FBZDtBQUNILE9BWHFCLENBQXRCOztBQWFBLFVBQUlpRSxhQUFhLENBQUMzRCxNQUFkLEdBQXVCLENBQXZCLElBQTRCN0osT0FBTyxDQUFDK0ksS0FBUixDQUFjbEosbUJBQWQsQ0FBaEMsRUFBb0U7QUFDaEV3TixRQUFBQSxhQUFhLENBQUNyTixPQUFELENBQWIsR0FBeUJ3TixhQUF6QjtBQUNIO0FBQ0osS0FqQkQ7QUFtQkEsV0FBT0gsYUFBUDtBQUNILEdBeGdCMkI7QUEwZ0I1Qk0sRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIsUUFBSSxDQUFDLEtBQUt4SyxPQUFWLEVBQW1CLE9BQU8sSUFBUDs7QUFDbkIsVUFBTXlLLEtBQUssR0FBR0Msa0JBQVNDLFdBQVQsQ0FBcUIsSUFBckIsQ0FBZDs7QUFDQSxRQUFJLENBQUNGLEtBQUwsRUFBWSxPQUFPLElBQVA7O0FBRVosUUFBSUEsS0FBSyxDQUFDRyxTQUFOLENBQWdCQyxRQUFoQixDQUF5QixjQUF6QixDQUFKLEVBQThDO0FBQzFDLGFBQU9KLEtBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPQSxLQUFLLENBQUNLLFFBQU4sQ0FBZSxDQUFmLENBQVAsQ0FERyxDQUN1QjtBQUM3QjtBQUNKLEdBcGhCMkI7QUFzaEI1QkMsRUFBQUEsY0FBYyxFQUFFLFVBQVNDLENBQVQsRUFBWTtBQUN4QixTQUFLQyxZQUFMLENBQWtCRCxDQUFsQjs7QUFDQSxTQUFLakgsMEJBQUwsQ0FBZ0NpSCxDQUFoQyxFQUFtQyxLQUFuQztBQUNILEdBemhCMkI7QUEyaEI1QkMsRUFBQUEsWUFBWSxFQUFFLFVBQVNELENBQVQsRUFBWTtBQUN0QjtBQUNBLFFBQUksS0FBSzVHLE9BQUwsSUFBZ0IsS0FBS0EsT0FBTCxDQUFhOEcsS0FBYixDQUFtQkMsT0FBbkIsS0FBK0IsTUFBbkQsRUFBMkQ7QUFDdkQsV0FBSy9HLE9BQUwsQ0FBYThHLEtBQWIsQ0FBbUJDLE9BQW5CLEdBQTZCLE1BQTdCO0FBQ0g7QUFDSixHQWhpQjJCO0FBa2lCNUJwSCxFQUFBQSwwQkFBMEIsRUFBRSxVQUFTaUgsQ0FBVCxFQUFZSSxTQUFaLEVBQXVCO0FBQy9DLFVBQU1DLGVBQWUsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLGlCQUF4QixDQUF4Qjs7QUFDQSxRQUFJRixlQUFlLElBQUlBLGVBQWUsQ0FBQ0csYUFBdkMsRUFBc0Q7QUFDbEQsWUFBTUMsVUFBVSxHQUFHLEtBQUtqQixjQUFMLEVBQW5COztBQUNBLFVBQUksQ0FBQ2lCLFVBQUwsRUFBaUIsT0FGaUMsQ0FHbEQ7QUFDQTs7QUFDQSxZQUFNQyxnQkFBZ0IsR0FBR0QsVUFBVSxDQUFDRSxxQkFBWCxHQUFtQ0MsR0FBbkMsR0FBeUM5TixNQUFNLENBQUMrTixXQUF6RSxDQUxrRCxDQU1sRDtBQUNBOztBQUNBLFlBQU1DLGdCQUFnQixHQUFHcEIsa0JBQVNDLFdBQVQsQ0FBcUIsSUFBckIsRUFBMkJnQixxQkFBM0IsR0FBbURJLE1BQTVFOztBQUVBLFVBQUlILEdBQUcsR0FBSVAsZUFBZSxDQUFDRyxhQUFoQixDQUE4QkcscUJBQTlCLEdBQXNEQyxHQUF0RCxHQUE0RDlOLE1BQU0sQ0FBQytOLFdBQTlFLENBVmtELENBV2xEOztBQUNBRCxNQUFBQSxHQUFHLEdBQUlBLEdBQUcsR0FBR0YsZ0JBQVAsR0FBMkJBLGdCQUEzQixHQUE4Q0UsR0FBcEQsQ0Faa0QsQ0FhbEQ7O0FBQ0EsWUFBTUksWUFBWSxHQUFHTixnQkFBZ0IsSUFBSUksZ0JBQWdCLEdBQUcsRUFBdkIsQ0FBckM7QUFDQUYsTUFBQUEsR0FBRyxHQUFJQSxHQUFHLEdBQUdJLFlBQVAsR0FBdUJBLFlBQXZCLEdBQXNDSixHQUE1QztBQUVBUCxNQUFBQSxlQUFlLENBQUNILEtBQWhCLENBQXNCVSxHQUF0QixHQUE0QkEsR0FBRyxHQUFHLElBQWxDO0FBQ0FQLE1BQUFBLGVBQWUsQ0FBQ0gsS0FBaEIsQ0FBc0JlLElBQXRCLEdBQTZCUixVQUFVLENBQUNTLFVBQVgsR0FBd0JULFVBQVUsQ0FBQ1UsV0FBbkMsR0FBaUQsRUFBakQsR0FBc0QsSUFBbkY7QUFDSDtBQUNKLEdBeGpCMkI7O0FBMGpCNUJDLEVBQUFBLHFCQUFxQixDQUFDekcsTUFBRCxFQUFTO0FBQzFCLFVBQU0wRyxHQUFHLEdBQUcsRUFBWjtBQUNBLFVBQU1DLFFBQVEsR0FBRzNHLE1BQU0sSUFBSUEsTUFBTSxDQUFDNEcsV0FBUCxFQUEzQjtBQUVBLFVBQU1DLGVBQWUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF4Qjs7QUFDQSxTQUFLLE1BQU1wRSxLQUFYLElBQW9CcEksaUNBQWdCQyxHQUFoQixHQUFzQndNLFNBQXRCLEVBQXBCLEVBQXVEO0FBQ25ELFlBQU07QUFBQ0MsUUFBQUEsT0FBRDtBQUFVQyxRQUFBQSxJQUFWO0FBQWdCQyxRQUFBQTtBQUFoQixVQUFnQ3hFLEtBQXRDLENBRG1ELENBRW5EOztBQUNBLFVBQUl3RSxZQUFZLEtBQUssUUFBckIsRUFBK0I7QUFDL0IsVUFBSVIsUUFBUSxJQUFJLENBQUNNLE9BQU8sQ0FBQ0wsV0FBUixHQUFzQnpQLFVBQXRCLENBQWlDd1AsUUFBakMsQ0FBYixJQUNBLEVBQUVPLElBQUksSUFBSUEsSUFBSSxDQUFDTixXQUFMLEdBQW1CUSxRQUFuQixDQUE0QlQsUUFBNUIsQ0FBVixDQURKLEVBQ3NEO0FBQ3RERCxNQUFBQSxHQUFHLENBQUNyRCxJQUFKLENBQVMsNkJBQUMsZUFBRDtBQUFpQixRQUFBLEdBQUcsRUFBRTRELE9BQXRCO0FBQStCLFFBQUEsS0FBSyxFQUFFdEUsS0FBdEM7QUFBNkMsUUFBQSxTQUFTLEVBQUUsS0FBSzdFLEtBQUwsQ0FBV3BHO0FBQW5FLFFBQVQ7QUFDSDs7QUFFRCxXQUFPZ1AsR0FBUDtBQUNILEdBemtCMkI7O0FBMmtCNUJ0RyxFQUFBQSxrQkFBa0IsRUFBRSxVQUFTaUgsSUFBVCxFQUFlckgsTUFBZixFQUF1QjtBQUN2QyxRQUFJQSxNQUFNLEtBQUssRUFBZixFQUFtQixPQUFPcUgsSUFBUDtBQUNuQixVQUFNVixRQUFRLEdBQUczRyxNQUFNLENBQUM0RyxXQUFQLEVBQWpCLENBRnVDLENBR3ZDO0FBQ0E7O0FBQ0EsVUFBTVUsV0FBVyxHQUFHQyxLQUFLLENBQUNDLGlCQUFOLENBQXdCYixRQUF4QixFQUFrQ0MsV0FBbEMsRUFBcEIsQ0FMdUMsQ0FNdkM7QUFDQTs7QUFDQSxXQUFPUyxJQUFJLENBQUNySCxNQUFMLENBQWFRLElBQUQsSUFBVTtBQUN6QixVQUFJUixNQUFNLENBQUMsQ0FBRCxDQUFOLEtBQWMsR0FBbEIsRUFBdUI7QUFDbkIsWUFBSVEsSUFBSSxDQUFDaUgsaUJBQUwsTUFBNEJqSCxJQUFJLENBQUNpSCxpQkFBTCxHQUF5QmIsV0FBekIsR0FBdUN6UCxVQUF2QyxDQUFrRHdQLFFBQWxELENBQWhDLEVBQTZGO0FBQ3pGLGlCQUFPLElBQVA7QUFDSDs7QUFDRCxZQUFJbkcsSUFBSSxDQUFDa0gsYUFBTCxHQUFxQkMsSUFBckIsQ0FBMkJDLEtBQUQsSUFBV0EsS0FBSyxDQUFDaEIsV0FBTixHQUFvQnpQLFVBQXBCLENBQStCd1AsUUFBL0IsQ0FBckMsQ0FBSixFQUFvRjtBQUNoRixpQkFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFDRCxhQUFPbkcsSUFBSSxDQUFDMEcsSUFBTCxJQUFhSyxLQUFLLENBQUNDLGlCQUFOLENBQXdCaEgsSUFBSSxDQUFDMEcsSUFBTCxDQUFVTixXQUFWLEVBQXhCLEVBQWlEQSxXQUFqRCxHQUErRFEsUUFBL0QsQ0FBd0VFLFdBQXhFLENBQXBCO0FBQ0gsS0FWTSxDQUFQO0FBV0gsR0E5bEIyQjtBQWdtQjVCTyxFQUFBQSxxQkFBcUIsRUFBRSxVQUFTNU8sR0FBVCxFQUFjdkIsU0FBZCxFQUF5QjtBQUM1QztBQUNBLFNBQUtnQixjQUFMLENBQW9CTyxHQUFwQixJQUEyQnZCLFNBQTNCO0FBQ0FTLElBQUFBLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQmtCLE9BQXBCLENBQTRCLHVCQUE1QixFQUFxRGQsSUFBSSxDQUFDZSxTQUFMLENBQWUsS0FBS2IsY0FBcEIsQ0FBckQsRUFINEMsQ0FJNUM7O0FBQ0EsUUFBSWhCLFNBQUosRUFBZTtBQUNYLFdBQUsrQixPQUFMLENBQWFxTyxlQUFiLENBQTZCN08sR0FBN0I7QUFDSCxLQUZELE1BRU87QUFDSCxXQUFLUSxPQUFMLENBQWFzTyxhQUFiLENBQTJCOU8sR0FBM0IsRUFBZ0MsS0FBS1YsWUFBTCxDQUFrQlUsR0FBbEIsQ0FBaEM7QUFDSCxLQVQyQyxDQVU1QztBQUNBOzs7QUFDQSxTQUFLSSxzQkFBTDtBQUNILEdBN21CMkI7O0FBK21CNUI7QUFDQUEsRUFBQUEsc0JBQXNCLEdBQUc7QUFDckJ5RyxJQUFBQSxNQUFNLENBQUNvRSxNQUFQLENBQWMsS0FBS2pNLFlBQW5CLEVBQWlDNEQsT0FBakMsQ0FBeUNvSSxDQUFDLElBQUlBLENBQUMsQ0FBQytELGFBQUYsRUFBOUM7QUFDSCxHQWxuQjJCOztBQW9uQjVCQyxFQUFBQSxXQUFXLEVBQUUsVUFBU2hQLEdBQVQsRUFBY2lQLEdBQWQsRUFBbUI7QUFDNUIsUUFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDTixhQUFPLEtBQUtqUSxZQUFMLENBQWtCZ0IsR0FBbEIsQ0FBUDtBQUNILEtBRkQsTUFFTztBQUNILFdBQUtoQixZQUFMLENBQWtCZ0IsR0FBbEIsSUFBeUJpUCxHQUF6QjtBQUNIO0FBQ0osR0ExbkIyQjtBQTRuQjVCQyxFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTQyxhQUFULEVBQXdCO0FBQ3RDLFNBQUt6UCxlQUFMLEdBQXVCLEVBQXZCO0FBQ0EsVUFBTTBQLFlBQVksR0FBRztBQUNqQjNRLE1BQUFBLFNBQVMsRUFBRSxLQUFLb0csS0FBTCxDQUFXcEcsU0FETDtBQUVqQjRRLE1BQUFBLFVBQVUsRUFBRSxDQUFDLENBQUMsS0FBS3hLLEtBQUwsQ0FBV2pHO0FBRlIsS0FBckI7QUFLQXVRLElBQUFBLGFBQWEsQ0FBQ3ZNLE9BQWQsQ0FBdUIwTSxDQUFELElBQU87QUFDekJBLE1BQUFBLENBQUMsQ0FBQ2xCLElBQUYsR0FBUyxLQUFLakgsa0JBQUwsQ0FBd0JtSSxDQUFDLENBQUNsQixJQUExQixFQUFnQyxLQUFLdkosS0FBTCxDQUFXakcsWUFBM0MsQ0FBVDtBQUNILEtBRkQ7QUFJQXVRLElBQUFBLGFBQWEsR0FBR0EsYUFBYSxDQUFDcEksTUFBZCxDQUFzQmxDLEtBQUssSUFBSTtBQUMzQyxZQUFNMEssR0FBRyxHQUFHMUssS0FBSyxDQUFDdUosSUFBTixDQUFXdEcsTUFBWCxJQUFxQmpELEtBQUssQ0FBQzJLLFVBQU4sR0FBbUIzSyxLQUFLLENBQUMySyxVQUFOLENBQWlCMUgsTUFBcEMsR0FBNkMsQ0FBbEUsQ0FBWjtBQUNBLGFBQU95SCxHQUFHLEtBQUssQ0FBUixJQUFhMUssS0FBSyxDQUFDNEssU0FBMUI7QUFDSCxLQUhlLENBQWhCO0FBS0EsV0FBT04sYUFBYSxDQUFDTyxNQUFkLENBQXFCLENBQUNDLFVBQUQsRUFBYTlLLEtBQWIsRUFBb0IrSyxDQUFwQixLQUEwQjtBQUNsRC9LLE1BQUFBLEtBQUsscUJBQU91SyxZQUFQLE1BQXdCdkssS0FBeEIsQ0FBTDtBQUNBLFlBQU1nTCxNQUFNLEdBQUdELENBQUMsS0FBS1QsYUFBYSxDQUFDckgsTUFBZCxHQUF1QixDQUE1QztBQUNBLFlBQU15SCxHQUFHLEdBQUcxSyxLQUFLLENBQUN1SixJQUFOLENBQVd0RyxNQUFYLElBQXFCakQsS0FBSyxDQUFDMkssVUFBTixHQUFtQjNLLEtBQUssQ0FBQzJLLFVBQU4sQ0FBaUIxSCxNQUFwQyxHQUE2QyxDQUFsRSxDQUFaO0FBQ0EsWUFBTTtBQUFDOUgsUUFBQUEsR0FBRDtBQUFNOFAsUUFBQUEsS0FBTjtBQUFhQyxRQUFBQTtBQUFiLFVBQTZDbEwsS0FBbkQ7QUFBQSxZQUFxQ21MLFVBQXJDLDBDQUFtRG5MLEtBQW5EO0FBQ0EsWUFBTW9MLFNBQVMsR0FBR2pRLEdBQUcsSUFBSThQLEtBQXpCOztBQUNBLFlBQU1JLG9CQUFvQixHQUFJelIsU0FBRCxJQUFlO0FBQ3hDLGFBQUttUSxxQkFBTCxDQUEyQnFCLFNBQTNCLEVBQXNDeFIsU0FBdEM7O0FBQ0EsWUFBSXNSLGFBQUosRUFBbUI7QUFDZkEsVUFBQUEsYUFBYSxDQUFDdFIsU0FBRCxDQUFiO0FBQ0g7QUFDSixPQUxEOztBQU1BLFlBQU0wUixhQUFhLEdBQUd0TCxLQUFLLENBQUNzTCxhQUFOLElBQXVCLEtBQUsxUSxjQUFMLENBQW9Cd1EsU0FBcEIsQ0FBN0M7O0FBQ0EsV0FBS3ZRLGVBQUwsQ0FBcUIwSyxJQUFyQixDQUEwQjtBQUN0QmdHLFFBQUFBLEVBQUUsRUFBRUgsU0FEa0I7QUFFdEJJLFFBQUFBLEtBQUssRUFBRWQ7QUFGZSxPQUExQjs7QUFJQSxZQUFNclAsT0FBTyxHQUFJLDZCQUFDLG9CQUFEO0FBQ2IsUUFBQSxHQUFHLEVBQUUsS0FBSzhPLFdBQUwsQ0FBaUJzQixJQUFqQixDQUFzQixJQUF0QixFQUE0QkwsU0FBNUIsQ0FEUTtBQUViLFFBQUEsYUFBYSxFQUFFRSxhQUZGO0FBR2IsUUFBQSxXQUFXLEVBQUUsQ0FBQyxDQUFDLEtBQUt0TCxLQUFMLENBQVdqRyxZQUhiO0FBSWIsUUFBQSxhQUFhLEVBQUVzUixvQkFKRjtBQUtiLFFBQUEsR0FBRyxFQUFFRCxTQUxRO0FBTWIsUUFBQSxLQUFLLEVBQUVIO0FBTk0sU0FPVEUsVUFQUyxFQUFqQjs7QUFTQSxVQUFJLENBQUNILE1BQUwsRUFBYTtBQUNULGVBQU9GLFVBQVUsQ0FBQ3ZJLE1BQVgsQ0FDSGxILE9BREcsRUFFSCw2QkFBQyxxQkFBRDtBQUFjLFVBQUEsR0FBRyxFQUFFK1AsU0FBUyxHQUFDLFVBQTdCO0FBQXlDLFVBQUEsUUFBUSxFQUFFLElBQW5EO0FBQXlELFVBQUEsRUFBRSxFQUFFQTtBQUE3RCxVQUZHLENBQVA7QUFJSCxPQUxELE1BS087QUFDSCxlQUFPTixVQUFVLENBQUN2SSxNQUFYLENBQWtCbEgsT0FBbEIsQ0FBUDtBQUNIO0FBQ0osS0FsQ00sRUFrQ0osRUFsQ0ksQ0FBUDtBQW1DSCxHQS9xQjJCO0FBaXJCNUJxUSxFQUFBQSx1QkFBdUIsRUFBRSxVQUFTQyxFQUFULEVBQWE7QUFDbEMsU0FBS3BNLGVBQUwsR0FBdUJvTSxFQUF2QjtBQUNILEdBbnJCMkI7QUFxckI1QkMsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxzQkFBc0IsR0FBSXpTLE9BQUQsSUFBYTtBQUN4QyxVQUFJLENBQUMsS0FBS2lJLEtBQUwsQ0FBV3JGLFlBQWhCLEVBQThCLE9BQU8sSUFBUDtBQUM5QixVQUFJLEtBQUtxRixLQUFMLENBQVd0RixlQUFYLEtBQStCM0MsT0FBbkMsRUFBNEMsT0FBTyxJQUFQO0FBQzVDLGFBQU8sS0FBS2lJLEtBQUwsQ0FBV3JGLFlBQWxCO0FBQ0gsS0FKRDs7QUFNQSxRQUFJOFAsUUFBUSxHQUFHLENBQ1g7QUFDSXZDLE1BQUFBLElBQUksRUFBRSxFQURWO0FBRUlvQixNQUFBQSxVQUFVLEVBQUUsS0FBS2hDLHFCQUFMLENBQTJCLEtBQUszSSxLQUFMLENBQVdqRyxZQUF0QyxDQUZoQjtBQUdJa1IsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLG1CQUFILENBSFg7QUFJSWMsTUFBQUEsUUFBUSxFQUFFO0FBSmQsS0FEVyxFQU9YO0FBQ0l4QyxNQUFBQSxJQUFJLEVBQUUsS0FBS2xJLEtBQUwsQ0FBV3ZGLEtBQVgsQ0FBaUIsdUJBQWpCLENBRFY7QUFFSW1QLE1BQUFBLEtBQUssRUFBRSx5QkFBRyxTQUFILENBRlg7QUFHSWpQLE1BQUFBLFlBQVksRUFBRTZQLHNCQUFzQixDQUFDLHVCQUFELENBSHhDO0FBSUlFLE1BQUFBLFFBQVEsRUFBRTtBQUpkLEtBUFcsRUFhWDtBQUNJeEMsTUFBQUEsSUFBSSxFQUFFLEtBQUtsSSxLQUFMLENBQVd2RixLQUFYLENBQWlCLGFBQWpCLENBRFY7QUFFSW1QLE1BQUFBLEtBQUssRUFBRSx5QkFBRyxZQUFILENBRlg7QUFHSTdSLE1BQUFBLE9BQU8sRUFBRSxhQUhiO0FBSUk0QyxNQUFBQSxZQUFZLEVBQUU2UCxzQkFBc0IsQ0FBQyxhQUFEO0FBSnhDLEtBYlcsRUFtQlg7QUFDSXRDLE1BQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXdkYsS0FBWCxDQUFpQjBGLHFCQUFqQixDQURWO0FBRUl5SixNQUFBQSxLQUFLLEVBQUUseUJBQUcsaUJBQUgsQ0FGWDtBQUdJN1IsTUFBQUEsT0FBTyxFQUFFb0kscUJBSGI7QUFJSXhGLE1BQUFBLFlBQVksRUFBRTZQLHNCQUFzQixDQUFDcksscUJBQUQsQ0FKeEM7QUFLSW9KLE1BQUFBLFNBQVMsRUFBRSxNQUFNO0FBQUM1TCw0QkFBSWtFLFFBQUosQ0FBYTtBQUFDeEMsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjtBQUE0QyxPQUxsRTtBQU1Jc0wsTUFBQUEsWUFBWSxFQUFFLHlCQUFHLFlBQUg7QUFObEIsS0FuQlcsRUEyQlg7QUFDSXpDLE1BQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXdkYsS0FBWCxDQUFpQix1QkFBakIsQ0FEVjtBQUVJbVAsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLE9BQUgsQ0FGWDtBQUdJalAsTUFBQUEsWUFBWSxFQUFFNlAsc0JBQXNCLENBQUMsdUJBQUQsQ0FIeEM7QUFJSWpCLE1BQUFBLFNBQVMsRUFBRSxNQUFNO0FBQUM1TCw0QkFBSWtFLFFBQUosQ0FBYTtBQUFDeEMsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjtBQUE0QztBQUpsRSxLQTNCVyxDQUFmO0FBa0NBLFVBQU11TCxXQUFXLEdBQUdqSyxNQUFNLENBQUNDLElBQVAsQ0FBWSxLQUFLWixLQUFMLENBQVd2RixLQUF2QixFQUNmb0csTUFEZSxDQUNQOUksT0FBRCxJQUFhO0FBQ2pCLGFBQU8sQ0FBQyxDQUFDLEtBQUtpSSxLQUFMLENBQVdsRixVQUFaLElBQTBCLEtBQUtrRixLQUFMLENBQVdsRixVQUFYLENBQXNCL0MsT0FBdEIsQ0FBM0IsS0FDSCxDQUFDQSxPQUFPLENBQUMrSSxLQUFSLENBQWNsSixtQkFBZCxDQURMO0FBRUgsS0FKZSxFQUlibUosR0FKYSxDQUlSaEosT0FBRCxJQUFhO0FBQ2hCLGFBQU87QUFDSG1RLFFBQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXdkYsS0FBWCxDQUFpQjFDLE9BQWpCLENBREg7QUFFSCtCLFFBQUFBLEdBQUcsRUFBRS9CLE9BRkY7QUFHSDZSLFFBQUFBLEtBQUssRUFBRTlSLGVBQWUsQ0FBQ0MsT0FBRCxDQUhuQjtBQUlIQSxRQUFBQSxPQUFPLEVBQUVBLE9BSk47QUFLSDRDLFFBQUFBLFlBQVksRUFBRTZQLHNCQUFzQixDQUFDelMsT0FBRDtBQUxqQyxPQUFQO0FBT0gsS0FaZSxDQUFwQjtBQWFBMFMsSUFBQUEsUUFBUSxHQUFHQSxRQUFRLENBQUN2SixNQUFULENBQWdCMEosV0FBaEIsQ0FBWDtBQUNBSCxJQUFBQSxRQUFRLEdBQUdBLFFBQVEsQ0FBQ3ZKLE1BQVQsQ0FBZ0IsQ0FDdkI7QUFDSWdILE1BQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXdkYsS0FBWCxDQUFpQixlQUFqQixDQURWO0FBRUltUCxNQUFBQSxLQUFLLEVBQUUseUJBQUcsY0FBSCxDQUZYO0FBR0k3UixNQUFBQSxPQUFPLEVBQUUsZUFIYjtBQUlJNEMsTUFBQUEsWUFBWSxFQUFFNlAsc0JBQXNCLENBQUMsZUFBRDtBQUp4QyxLQUR1QixFQU92QjtBQUNJdEMsTUFBQUEsSUFBSSxFQUFFLEtBQUtsSSxLQUFMLENBQVd2RixLQUFYLENBQWlCLHlCQUFqQixDQURWO0FBRUltUCxNQUFBQSxLQUFLLEVBQUUseUJBQUcsWUFBSCxDQUZYO0FBR0lqUCxNQUFBQSxZQUFZLEVBQUU2UCxzQkFBc0IsQ0FBQyx5QkFBRCxDQUh4QztBQUlJUCxNQUFBQSxhQUFhLEVBQUUsSUFKbkI7QUFLSVksTUFBQUEsV0FBVyxFQUFFLEtBQUs3SyxLQUFMLENBQVd6RixrQkFMNUI7QUFNSXNQLE1BQUFBLGFBQWEsRUFBRSxLQUFLckg7QUFOeEIsS0FQdUIsRUFldkI7QUFDSTBGLE1BQUFBLElBQUksRUFBRSxLQUFLbEksS0FBTCxDQUFXdkYsS0FBWCxDQUFpQixpQkFBakIsQ0FEVjtBQUVJbVAsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FGWDtBQUdJN1IsTUFBQUEsT0FBTyxFQUFFLGVBSGI7QUFJSTRDLE1BQUFBLFlBQVksRUFBRTZQLHNCQUFzQixDQUFDLGlCQUFEO0FBSnhDLEtBZnVCLENBQWhCLENBQVg7O0FBdUJBLFVBQU1NLGlCQUFpQixHQUFHLEtBQUs5QixnQkFBTCxDQUFzQnlCLFFBQXRCLENBQTFCOztBQUVBLHdCQUEwRixLQUFLOUwsS0FBL0Y7QUFBQSxVQUFNO0FBQUNDLE1BQUFBLGNBQUQ7QUFBaUJyRyxNQUFBQSxTQUFqQjtBQUE0QkcsTUFBQUEsWUFBNUI7QUFBMENOLE1BQUFBLGlCQUExQztBQUE2RDJTLE1BQUFBO0FBQTdELEtBQU47QUFBQSxVQUFpRnBNLEtBQWpGLDBJQWhGZSxDQWdGdUY7O0FBQ3RHLFdBQ0ksNkJBQUMsc0NBQUQ7QUFBd0IsTUFBQSxhQUFhLEVBQUUsSUFBdkM7QUFBNkMsTUFBQSxTQUFTLEVBQUVvTTtBQUF4RCxPQUNLLENBQUM7QUFBQ0MsTUFBQUE7QUFBRCxLQUFELEtBQXdCLCtEQUNqQnJNLEtBRGlCO0FBRXJCLE1BQUEsU0FBUyxFQUFFcU0sZ0JBRlU7QUFHckIsTUFBQSxHQUFHLEVBQUUsS0FBS1gsdUJBSFc7QUFJckIsTUFBQSxTQUFTLEVBQUMsYUFKVztBQUtyQixNQUFBLElBQUksRUFBQyxNQUxnQjtBQU1yQixvQkFBWSx5QkFBRyxPQUFILENBTlMsQ0FPckI7QUFDQTtBQVJxQjtBQVNyQixNQUFBLFFBQVEsRUFBQyxJQVRZO0FBVXJCLE1BQUEsV0FBVyxFQUFFLEtBQUszRyxXQVZHO0FBV3JCLE1BQUEsWUFBWSxFQUFFLEtBQUtLO0FBWEUsUUFhbkIrRyxpQkFibUIsQ0FEN0IsQ0FESjtBQW1CSDtBQXp4QjJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOCBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBUaW1lciBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvVGltZXJcIjtcclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUmVhY3RET00gZnJvbSBcInJlYWN0LWRvbVwiO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyB1dGlscyBmcm9tIFwibWF0cml4LWpzLXNkay9zcmMvdXRpbHNcIjtcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgcmF0ZV9saW1pdGVkX2Z1bmMgZnJvbSBcIi4uLy4uLy4uL3JhdGVsaW1pdGVkZnVuY1wiO1xyXG5pbXBvcnQgKiBhcyBSb29tcyBmcm9tICcuLi8uLi8uLi9Sb29tcyc7XHJcbmltcG9ydCBETVJvb21NYXAgZnJvbSAnLi4vLi4vLi4vdXRpbHMvRE1Sb29tTWFwJztcclxuaW1wb3J0IFRhZ09yZGVyU3RvcmUgZnJvbSAnLi4vLi4vLi4vc3RvcmVzL1RhZ09yZGVyU3RvcmUnO1xyXG5pbXBvcnQgUm9vbUxpc3RTdG9yZSwge1RBR19ETX0gZnJvbSAnLi4vLi4vLi4vc3RvcmVzL1Jvb21MaXN0U3RvcmUnO1xyXG5pbXBvcnQgQ3VzdG9tUm9vbVRhZ1N0b3JlIGZyb20gJy4uLy4uLy4uL3N0b3Jlcy9DdXN0b21Sb29tVGFnU3RvcmUnO1xyXG5pbXBvcnQgR3JvdXBTdG9yZSBmcm9tICcuLi8uLi8uLi9zdG9yZXMvR3JvdXBTdG9yZSc7XHJcbmltcG9ydCBSb29tU3ViTGlzdCBmcm9tICcuLi8uLi9zdHJ1Y3R1cmVzL1Jvb21TdWJMaXN0JztcclxuaW1wb3J0IFJlc2l6ZUhhbmRsZSBmcm9tICcuLi9lbGVtZW50cy9SZXNpemVIYW5kbGUnO1xyXG5pbXBvcnQgQ2FsbEhhbmRsZXIgZnJvbSBcIi4uLy4uLy4uL0NhbGxIYW5kbGVyXCI7XHJcbmltcG9ydCBkaXMgZnJvbSBcIi4uLy4uLy4uL2Rpc3BhdGNoZXJcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgKiBhcyBSZWNlaXB0IGZyb20gXCIuLi8uLi8uLi91dGlscy9SZWNlaXB0XCI7XHJcbmltcG9ydCB7UmVzaXplcn0gZnJvbSAnLi4vLi4vLi4vcmVzaXplcic7XHJcbmltcG9ydCB7TGF5b3V0LCBEaXN0cmlidXRvcn0gZnJvbSAnLi4vLi4vLi4vcmVzaXplci9kaXN0cmlidXRvcnMvcm9vbXN1Ymxpc3QyJztcclxuaW1wb3J0IHtSb3ZpbmdUYWJJbmRleFByb3ZpZGVyfSBmcm9tIFwiLi4vLi4vLi4vYWNjZXNzaWJpbGl0eS9Sb3ZpbmdUYWJJbmRleFwiO1xyXG5pbXBvcnQgKiBhcyBVbnJlYWQgZnJvbSBcIi4uLy4uLy4uL1VucmVhZFwiO1xyXG5pbXBvcnQgUm9vbVZpZXdTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc3RvcmVzL1Jvb21WaWV3U3RvcmVcIjtcclxuXHJcbmNvbnN0IEhJREVfQ09ORkVSRU5DRV9DSEFOUyA9IHRydWU7XHJcbmNvbnN0IFNUQU5EQVJEX1RBR1NfUkVHRVggPSAvXihtXFwuKGZhdm91cml0ZXxsb3dwcmlvcml0eXxzZXJ2ZXJfbm90aWNlKXxpbVxcLnZlY3RvclxcLmZha2VcXC4oaW52aXRlfHJlY2VudHxkaXJlY3R8YXJjaGl2ZWQpKSQvO1xyXG5jb25zdCBIT1ZFUl9NT1ZFX1RJTUVPVVQgPSAxMDAwO1xyXG5cclxuZnVuY3Rpb24gbGFiZWxGb3JUYWdOYW1lKHRhZ05hbWUpIHtcclxuICAgIGlmICh0YWdOYW1lLnN0YXJ0c1dpdGgoJ3UuJykpIHJldHVybiB0YWdOYW1lLnNsaWNlKDIpO1xyXG4gICAgcmV0dXJuIHRhZ05hbWU7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tTGlzdCcsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXI6IFByb3BUeXBlcy5hbnksXHJcbiAgICAgICAgY29sbGFwc2VkOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHNlYXJjaEZpbHRlcjogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdGhpcy5faG92ZXJDbGVhclRpbWVyID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9zdWJMaXN0UmVmcyA9IHtcclxuICAgICAgICAgICAgLy8ga2V5ID0+IFJvb21TdWJMaXN0IHJlZlxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHNpemVzSnNvbiA9IHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIm14X3Jvb21saXN0X3NpemVzXCIpO1xyXG4gICAgICAgIGNvbnN0IGNvbGxhcHNlZEpzb24gPSB3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJteF9yb29tbGlzdF9jb2xsYXBzZWRcIik7XHJcbiAgICAgICAgdGhpcy5zdWJMaXN0U2l6ZXMgPSBzaXplc0pzb24gPyBKU09OLnBhcnNlKHNpemVzSnNvbikgOiB7fTtcclxuICAgICAgICB0aGlzLmNvbGxhcHNlZFN0YXRlID0gY29sbGFwc2VkSnNvbiA/IEpTT04ucGFyc2UoY29sbGFwc2VkSnNvbikgOiB7fTtcclxuICAgICAgICB0aGlzLl9sYXlvdXRTZWN0aW9ucyA9IFtdO1xyXG5cclxuICAgICAgICBjb25zdCB1bmZpbHRlcmVkT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgYWxsb3dXaGl0ZXNwYWNlOiBmYWxzZSxcclxuICAgICAgICAgICAgaGFuZGxlSGVpZ2h0OiAxLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fdW5maWx0ZXJlZGxheW91dCA9IG5ldyBMYXlvdXQoKGtleSwgc2l6ZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzdWJMaXN0ID0gdGhpcy5fc3ViTGlzdFJlZnNba2V5XTtcclxuICAgICAgICAgICAgaWYgKHN1Ykxpc3QpIHtcclxuICAgICAgICAgICAgICAgIHN1Ykxpc3Quc2V0SGVpZ2h0KHNpemUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHVwZGF0ZSBvdmVyZmxvdyBpbmRpY2F0b3JzXHJcbiAgICAgICAgICAgIHRoaXMuX2NoZWNrU3ViTGlzdHNPdmVyZmxvdygpO1xyXG4gICAgICAgICAgICAvLyBkb24ndCBzdG9yZSBoZWlnaHQgZm9yIGNvbGxhcHNlZCBzdWJsaXN0c1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY29sbGFwc2VkU3RhdGVba2V5XSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJMaXN0U2l6ZXNba2V5XSA9IHNpemU7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJteF9yb29tbGlzdF9zaXplc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KHRoaXMuc3ViTGlzdFNpemVzKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCB0aGlzLnN1Ykxpc3RTaXplcywgdGhpcy5jb2xsYXBzZWRTdGF0ZSwgdW5maWx0ZXJlZE9wdGlvbnMpO1xyXG5cclxuICAgICAgICB0aGlzLl9maWx0ZXJlZExheW91dCA9IG5ldyBMYXlvdXQoKGtleSwgc2l6ZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzdWJMaXN0ID0gdGhpcy5fc3ViTGlzdFJlZnNba2V5XTtcclxuICAgICAgICAgICAgaWYgKHN1Ykxpc3QpIHtcclxuICAgICAgICAgICAgICAgIHN1Ykxpc3Quc2V0SGVpZ2h0KHNpemUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgbnVsbCwgbnVsbCwge1xyXG4gICAgICAgICAgICBhbGxvd1doaXRlc3BhY2U6IGZhbHNlLFxyXG4gICAgICAgICAgICBoYW5kbGVIZWlnaHQ6IDAsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2xheW91dCA9IHRoaXMuX3VuZmlsdGVyZWRsYXlvdXQ7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzTG9hZGluZ0xlZnRSb29tczogZmFsc2UsXHJcbiAgICAgICAgICAgIHRvdGFsUm9vbUNvdW50OiBudWxsLFxyXG4gICAgICAgICAgICBsaXN0czoge30sXHJcbiAgICAgICAgICAgIGluY29taW5nQ2FsbFRhZzogbnVsbCxcclxuICAgICAgICAgICAgaW5jb21pbmdDYWxsOiBudWxsLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFRhZ3M6IFtdLFxyXG4gICAgICAgICAgICBob3ZlcjogZmFsc2UsXHJcbiAgICAgICAgICAgIGN1c3RvbVRhZ3M6IEN1c3RvbVJvb21UYWdTdG9yZS5nZXRUYWdzKCksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gVE9ETzogW1JFQUNULVdBUk5JTkddIFJlcGxhY2UgY29tcG9uZW50IHdpdGggcmVhbCBjbGFzcywgcHV0IHRoaXMgaW4gdGhlIGNvbnN0cnVjdG9yLlxyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5tb3VudGVkID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuXHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbVwiLCB0aGlzLm9uUm9vbSk7XHJcbiAgICAgICAgY2xpLm9uKFwiZGVsZXRlUm9vbVwiLCB0aGlzLm9uRGVsZXRlUm9vbSk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbU1lbWJlci5uYW1lXCIsIHRoaXMub25Sb29tTWVtYmVyTmFtZSk7XHJcbiAgICAgICAgY2xpLm9uKFwiRXZlbnQuZGVjcnlwdGVkXCIsIHRoaXMub25FdmVudERlY3J5cHRlZCk7XHJcbiAgICAgICAgY2xpLm9uKFwiYWNjb3VudERhdGFcIiwgdGhpcy5vbkFjY291bnREYXRhKTtcclxuICAgICAgICBjbGkub24oXCJHcm91cC5teU1lbWJlcnNoaXBcIiwgdGhpcy5fb25Hcm91cE15TWVtYmVyc2hpcCk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnRzKTtcclxuXHJcbiAgICAgICAgY29uc3QgZG1Sb29tTWFwID0gRE1Sb29tTWFwLnNoYXJlZCgpO1xyXG4gICAgICAgIC8vIEEgbWFwIGJldHdlZW4gdGFncyB3aGljaCBhcmUgZ3JvdXAgSURzIGFuZCB0aGUgcm9vbSBJRHMgb2Ygcm9vbXMgdGhhdCBzaG91bGQgYmUga2VwdFxyXG4gICAgICAgIC8vIGluIHRoZSByb29tIGxpc3Qgd2hlbiBmaWx0ZXJpbmcgYnkgdGhhdCB0YWcuXHJcbiAgICAgICAgdGhpcy5fdmlzaWJsZVJvb21zRm9yR3JvdXAgPSB7XHJcbiAgICAgICAgICAgIC8vICRncm91cElkOiBbJHJvb21JZDEsICRyb29tSWQyLCAuLi5dLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgLy8gQWxsIHJvb21zIHRoYXQgc2hvdWxkIGJlIGtlcHQgaW4gdGhlIHJvb20gbGlzdCB3aGVuIGZpbHRlcmluZy5cclxuICAgICAgICAvLyBCeSBkZWZhdWx0LCBzaG93IGFsbCByb29tcy5cclxuICAgICAgICB0aGlzLl92aXNpYmxlUm9vbXMgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VmlzaWJsZVJvb21zKCk7XHJcblxyXG4gICAgICAgIC8vIExpc3RlbiB0byB1cGRhdGVzIHRvIGdyb3VwIGRhdGEuIFJvb21MaXN0IGNhcmVzIGFib3V0IG1lbWJlcnMgYW5kIHJvb21zIGluIG9yZGVyXHJcbiAgICAgICAgLy8gdG8gZmlsdGVyIHRoZSByb29tIGxpc3Qgd2hlbiBncm91cCB0YWdzIGFyZSBzZWxlY3RlZC5cclxuICAgICAgICB0aGlzLl9ncm91cFN0b3JlVG9rZW4gPSBHcm91cFN0b3JlLnJlZ2lzdGVyTGlzdGVuZXIobnVsbCwgKCkgPT4ge1xyXG4gICAgICAgICAgICAoVGFnT3JkZXJTdG9yZS5nZXRPcmRlcmVkVGFncygpIHx8IFtdKS5mb3JFYWNoKCh0YWcpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0YWdbMF0gIT09ICcrJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIFRoaXMgZ3JvdXAncyByb29tcyBvciBtZW1iZXJzIG1heSBoYXZlIHVwZGF0ZWQsIHVwZGF0ZSByb29tcyBmb3IgaXRzIHRhZ1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVWaXNpYmxlUm9vbXNGb3JUYWcoZG1Sb29tTWFwLCB0YWcpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVWaXNpYmxlUm9vbXMoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3RhZ1N0b3JlVG9rZW4gPSBUYWdPcmRlclN0b3JlLmFkZExpc3RlbmVyKCgpID0+IHtcclxuICAgICAgICAgICAgLy8gRmlsdGVycyB0aGVtc2VsdmVzIGhhdmUgY2hhbmdlZFxyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVZpc2libGVSb29tcygpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLl9yb29tTGlzdFN0b3JlVG9rZW4gPSBSb29tTGlzdFN0b3JlLmFkZExpc3RlbmVyKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fZGVsYXllZFJlZnJlc2hSb29tTGlzdCgpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICAgICAgaWYgKFNldHRpbmdzU3RvcmUuaXNGZWF0dXJlRW5hYmxlZChcImZlYXR1cmVfY3VzdG9tX3RhZ3NcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5fY3VzdG9tVGFnU3RvcmVUb2tlbiA9IEN1c3RvbVJvb21UYWdTdG9yZS5hZGRMaXN0ZW5lcigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBjdXN0b21UYWdzOiBDdXN0b21Sb29tVGFnU3RvcmUuZ2V0VGFncygpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5yZWZyZXNoUm9vbUxpc3QoKTtcclxuXHJcbiAgICAgICAgLy8gb3JkZXIgb2YgdGhlIHN1Ymxpc3RzXHJcbiAgICAgICAgLy90aGlzLmxpc3RPcmRlciA9IFtdO1xyXG5cclxuICAgICAgICAvLyBsb29wIGNvdW50IHRvIHN0b3AgYSBzdGFjayBvdmVyZmxvdyBpZiB0aGUgdXNlciBrZWVwcyB3YWdnbGluZyB0aGVcclxuICAgICAgICAvLyBtb3VzZSBmb3IgPjMwcyBpbiBhIHJvdywgb3IgaWYgcnVubmluZyB1bmRlciBtb2NoYVxyXG4gICAgICAgIHRoaXMuX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3RMb29wQ291bnQgPSAwO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwYXRjaGVyUmVmID0gZGlzLnJlZ2lzdGVyKHRoaXMub25BY3Rpb24pO1xyXG4gICAgICAgIGNvbnN0IGNmZyA9IHtcclxuICAgICAgICAgICAgZ2V0TGF5b3V0OiAoKSA9PiB0aGlzLl9sYXlvdXQsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnJlc2l6ZXIgPSBuZXcgUmVzaXplcih0aGlzLnJlc2l6ZUNvbnRhaW5lciwgRGlzdHJpYnV0b3IsIGNmZyk7XHJcbiAgICAgICAgdGhpcy5yZXNpemVyLnNldENsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICBoYW5kbGU6IFwibXhfUmVzaXplSGFuZGxlXCIsXHJcbiAgICAgICAgICAgIHZlcnRpY2FsOiBcIm14X1Jlc2l6ZUhhbmRsZV92ZXJ0aWNhbFwiLFxyXG4gICAgICAgICAgICByZXZlcnNlOiBcIm14X1Jlc2l6ZUhhbmRsZV9yZXZlcnNlXCIsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fbGF5b3V0LnVwZGF0ZShcclxuICAgICAgICAgICAgdGhpcy5fbGF5b3V0U2VjdGlvbnMsXHJcbiAgICAgICAgICAgIHRoaXMucmVzaXplQ29udGFpbmVyICYmIHRoaXMucmVzaXplQ29udGFpbmVyLm9mZnNldEhlaWdodCxcclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMuX2NoZWNrU3ViTGlzdHNPdmVyZmxvdygpO1xyXG5cclxuICAgICAgICB0aGlzLnJlc2l6ZXIuYXR0YWNoKCk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5yZXNpemVOb3RpZmllci5vbihcImxlZnRQYW5lbFJlc2l6ZWRcIiwgdGhpcy5vblJlc2l6ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubW91bnRlZCA9IHRydWU7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24ocHJldlByb3BzKSB7XHJcbiAgICAgICAgbGV0IGZvcmNlTGF5b3V0VXBkYXRlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fcmVwb3NpdGlvbkluY29taW5nQ2FsbEJveCh1bmRlZmluZWQsIGZhbHNlKTtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuc2VhcmNoRmlsdGVyICYmIHByZXZQcm9wcy5zZWFyY2hGaWx0ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fbGF5b3V0ID0gdGhpcy5fdW5maWx0ZXJlZGxheW91dDtcclxuICAgICAgICAgICAgZm9yY2VMYXlvdXRVcGRhdGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5zZWFyY2hGaWx0ZXIgJiYgIXByZXZQcm9wcy5zZWFyY2hGaWx0ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fbGF5b3V0ID0gdGhpcy5fZmlsdGVyZWRMYXlvdXQ7XHJcbiAgICAgICAgICAgIGZvcmNlTGF5b3V0VXBkYXRlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbGF5b3V0LnVwZGF0ZShcclxuICAgICAgICAgICAgdGhpcy5fbGF5b3V0U2VjdGlvbnMsXHJcbiAgICAgICAgICAgIHRoaXMucmVzaXplQ29udGFpbmVyICYmIHRoaXMucmVzaXplQ29udGFpbmVyLmNsaWVudEhlaWdodCxcclxuICAgICAgICAgICAgZm9yY2VMYXlvdXRVcGRhdGUsXHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLl9jaGVja1N1Ykxpc3RzT3ZlcmZsb3coKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BY3Rpb246IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfdG9vbHRpcCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvb2x0aXAgPSBwYXlsb2FkLnRvb2x0aXA7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnY2FsbF9zdGF0ZSc6XHJcbiAgICAgICAgICAgICAgICB2YXIgY2FsbCA9IENhbGxIYW5kbGVyLmdldENhbGwocGF5bG9hZC5yb29tX2lkKTtcclxuICAgICAgICAgICAgICAgIGlmIChjYWxsICYmIGNhbGwuY2FsbF9zdGF0ZSA9PT0gJ3JpbmdpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbDogY2FsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5jb21pbmdDYWxsVGFnOiB0aGlzLmdldFRhZ05hbWVGb3JSb29tSWQocGF5bG9hZC5yb29tX2lkKSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9yZXBvc2l0aW9uSW5jb21pbmdDYWxsQm94KHVuZGVmaW5lZCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmNvbWluZ0NhbGw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbFRhZzogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3Jvb21fZGVsdGEnOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50Um9vbUlkID0gUm9vbVZpZXdTdG9yZS5nZXRSb29tSWQoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgICAgICAgICBcImltLnZlY3Rvci5mYWtlLmludml0ZVwiOiBpbnZpdGVSb29tcyxcclxuICAgICAgICAgICAgICAgICAgICBcIm0uZmF2b3VyaXRlXCI6IGZhdm91cml0ZVJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIFtUQUdfRE1dOiBkbVJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiaW0udmVjdG9yLmZha2UucmVjZW50XCI6IHJlY2VudFJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIFwibS5sb3dwcmlvcml0eVwiOiBsb3dQcmlvcml0eVJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiaW0udmVjdG9yLmZha2UuYXJjaGl2ZWRcIjogaGlzdG9yaWNhbFJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIFwibS5zZXJ2ZXJfbm90aWNlXCI6IHNlcnZlck5vdGljZVJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIC4uLnRhZ3NcclxuICAgICAgICAgICAgICAgIH0gPSB0aGlzLnN0YXRlLmxpc3RzO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHNob3duQ3VzdG9tVGFnUm9vbXMgPSBPYmplY3Qua2V5cyh0YWdzKS5maWx0ZXIodGFnTmFtZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICghdGhpcy5zdGF0ZS5jdXN0b21UYWdzIHx8IHRoaXMuc3RhdGUuY3VzdG9tVGFnc1t0YWdOYW1lXSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgIXRhZ05hbWUubWF0Y2goU1RBTkRBUkRfVEFHU19SRUdFWCk7XHJcbiAgICAgICAgICAgICAgICB9KS5tYXAodGFnTmFtZSA9PiB0YWdzW3RhZ05hbWVdKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzIG9yZGVyIG1hdGNoZXMgdGhlIG9uZSB3aGVuIGdlbmVyYXRpbmcgdGhlIHJvb20gc3VibGlzdHMgYmVsb3cuXHJcbiAgICAgICAgICAgICAgICBsZXQgcm9vbXMgPSB0aGlzLl9hcHBseVNlYXJjaEZpbHRlcihbXHJcbiAgICAgICAgICAgICAgICAgICAgLi4uaW52aXRlUm9vbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgLi4uZmF2b3VyaXRlUm9vbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgLi4uZG1Sb29tcyxcclxuICAgICAgICAgICAgICAgICAgICAuLi5yZWNlbnRSb29tcyxcclxuICAgICAgICAgICAgICAgICAgICAuLi5bXS5jb25jYXQuYXBwbHkoW10sIHNob3duQ3VzdG9tVGFnUm9vbXMpLCAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIHByZWZlci1zcHJlYWRcclxuICAgICAgICAgICAgICAgICAgICAuLi5sb3dQcmlvcml0eVJvb21zLFxyXG4gICAgICAgICAgICAgICAgICAgIC4uLmhpc3RvcmljYWxSb29tcyxcclxuICAgICAgICAgICAgICAgICAgICAuLi5zZXJ2ZXJOb3RpY2VSb29tcyxcclxuICAgICAgICAgICAgICAgIF0sIHRoaXMucHJvcHMuc2VhcmNoRmlsdGVyKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocGF5bG9hZC51bnJlYWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBmaWx0ZXIgdG8gb25seSBub3RpZmljYXRpb24gcm9vbXMgKGFuZCBvdXIgY3VycmVudCBhY3RpdmUgcm9vbSBzbyB3ZSBjYW4gaW5kZXggcHJvcGVybHkpXHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbXMgPSByb29tcy5maWx0ZXIocm9vbSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByb29tLnJvb21JZCA9PT0gY3VycmVudFJvb21JZCB8fCBVbnJlYWQuZG9lc1Jvb21IYXZlVW5yZWFkTWVzc2FnZXMocm9vbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudEluZGV4ID0gcm9vbXMuZmluZEluZGV4KHJvb20gPT4gcm9vbS5yb29tSWQgPT09IGN1cnJlbnRSb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgLy8gdXNlIHNsaWNlIHRvIGFjY291bnQgZm9yIGxvb3BpbmcgYXJvdW5kIHRoZSBzdGFydFxyXG4gICAgICAgICAgICAgICAgY29uc3QgW3Jvb21dID0gcm9vbXMuc2xpY2UoKGN1cnJlbnRJbmRleCArIHBheWxvYWQuZGVsdGEpICUgcm9vbXMubGVuZ3RoKTtcclxuICAgICAgICAgICAgICAgIGlmIChyb29tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogcm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3dfcm9vbV90aWxlOiB0cnVlLCAvLyB0byBtYWtlIHN1cmUgdGhlIHJvb20gZ2V0cyBzY3JvbGxlZCBpbnRvIHZpZXdcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5tb3VudGVkID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKSkge1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoXCJSb29tXCIsIHRoaXMub25Sb29tKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKFwiZGVsZXRlUm9vbVwiLCB0aGlzLm9uRGVsZXRlUm9vbSk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcIlJvb20ucmVjZWlwdFwiLCB0aGlzLm9uUm9vbVJlY2VpcHQpO1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoXCJSb29tTWVtYmVyLm5hbWVcIiwgdGhpcy5vblJvb21NZW1iZXJOYW1lKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKFwiRXZlbnQuZGVjcnlwdGVkXCIsIHRoaXMub25FdmVudERlY3J5cHRlZCk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcImFjY291bnREYXRhXCIsIHRoaXMub25BY2NvdW50RGF0YSk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcihcIkdyb3VwLm15TWVtYmVyc2hpcFwiLCB0aGlzLl9vbkdyb3VwTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlc2l6ZU5vdGlmaWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucmVzaXplTm90aWZpZXIucmVtb3ZlTGlzdGVuZXIoXCJsZWZ0UGFuZWxSZXNpemVkXCIsIHRoaXMub25SZXNpemUpO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIGlmICh0aGlzLl90YWdTdG9yZVRva2VuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RhZ1N0b3JlVG9rZW4ucmVtb3ZlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5fcm9vbUxpc3RTdG9yZVRva2VuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvb21MaXN0U3RvcmVUb2tlbi5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX2N1c3RvbVRhZ1N0b3JlVG9rZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fY3VzdG9tVGFnU3RvcmVUb2tlbi5yZW1vdmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIE5COiBHcm91cFN0b3JlIGlzIG5vdCBhIEZsdXguU3RvcmVcclxuICAgICAgICBpZiAodGhpcy5fZ3JvdXBTdG9yZVRva2VuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2dyb3VwU3RvcmVUb2tlbi51bnJlZ2lzdGVyKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjYW5jZWwgYW55IHBlbmRpbmcgY2FsbHMgdG8gdGhlIHJhdGVfbGltaXRlZF9mdW5jc1xyXG4gICAgICAgIHRoaXMuX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3QuY2FuY2VsUGVuZGluZ0NhbGwoKTtcclxuICAgIH0sXHJcblxyXG5cclxuICAgIG9uUmVzaXplOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5tb3VudGVkICYmIHRoaXMuX2xheW91dCAmJiB0aGlzLnJlc2l6ZUNvbnRhaW5lciAmJlxyXG4gICAgICAgICAgICBBcnJheS5pc0FycmF5KHRoaXMuX2xheW91dFNlY3Rpb25zKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICB0aGlzLl9sYXlvdXQudXBkYXRlKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fbGF5b3V0U2VjdGlvbnMsXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2l6ZUNvbnRhaW5lci5vZmZzZXRIZWlnaHQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb206IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICB0aGlzLnVwZGF0ZVZpc2libGVSb29tcygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21TdGF0ZUV2ZW50czogZnVuY3Rpb24oZXYsIHN0YXRlKSB7XHJcbiAgICAgICAgaWYgKGV2LmdldFR5cGUoKSA9PT0gXCJtLnJvb20uY3JlYXRlXCIgfHwgZXYuZ2V0VHlwZSgpID09PSBcIm0ucm9vbS50b21ic3RvbmVcIikge1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVZpc2libGVSb29tcygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25EZWxldGVSb29tOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICB0aGlzLnVwZGF0ZVZpc2libGVSb29tcygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkFyY2hpdmVkSGVhZGVyQ2xpY2s6IGZ1bmN0aW9uKGlzSGlkZGVuLCBzY3JvbGxUb1Bvc2l0aW9uKSB7XHJcbiAgICAgICAgaWYgKCFpc0hpZGRlbikge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZ0xlZnRSb29tczogdHJ1ZSB9KTtcclxuICAgICAgICAgICAgLy8gd2UgZG9uJ3QgY2FyZSBhYm91dCB0aGUgcmVzcG9uc2Ugc2luY2UgaXQgY29tZXMgZG93biB2aWEgXCJSb29tXCJcclxuICAgICAgICAgICAgLy8gZXZlbnRzLlxyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc3luY0xlZnRSb29tcygpLmNhdGNoKGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBzeW5jIGxlZnQgcm9vbXM6ICVzXCIsIGVycik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIH0pLmZpbmFsbHkoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnNldFN0YXRlKHsgaXNMb2FkaW5nTGVmdFJvb21zOiBmYWxzZSB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21SZWNlaXB0OiBmdW5jdGlvbihyZWNlaXB0RXZlbnQsIHJvb20pIHtcclxuICAgICAgICAvLyBiZWNhdXNlIGlmIHdlIHJlYWQgYSBub3RpZmljYXRpb24sIGl0IHdpbGwgYWZmZWN0IG5vdGlmaWNhdGlvbiBjb3VudFxyXG4gICAgICAgIC8vIG9ubHkgYm90aGVyIHVwZGF0aW5nIGlmIHRoZXJlJ3MgYSByZWNlaXB0IGZyb20gdXNcclxuICAgICAgICBpZiAoUmVjZWlwdC5maW5kUmVhZFJlY2VpcHRGcm9tVXNlcklkKHJlY2VpcHRFdmVudCwgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZCkpIHtcclxuICAgICAgICAgICAgdGhpcy5fZGVsYXllZFJlZnJlc2hSb29tTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tTWVtYmVyTmFtZTogZnVuY3Rpb24oZXYsIG1lbWJlcikge1xyXG4gICAgICAgIHRoaXMuX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3QoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25FdmVudERlY3J5cHRlZDogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICAvLyBBbiBldmVudCBiZWluZyBkZWNyeXB0ZWQgbWF5IG1lYW4gd2UgbmVlZCB0byByZS1vcmRlciB0aGUgcm9vbSBsaXN0XHJcbiAgICAgICAgdGhpcy5fZGVsYXllZFJlZnJlc2hSb29tTGlzdCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkFjY291bnREYXRhOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgPT0gJ20uZGlyZWN0Jykge1xyXG4gICAgICAgICAgICB0aGlzLl9kZWxheWVkUmVmcmVzaFJvb21MaXN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25Hcm91cE15TWVtYmVyc2hpcDogZnVuY3Rpb24oZ3JvdXApIHtcclxuICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uTW91c2VNb3ZlOiBhc3luYyBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmICghdGhpcy5faG92ZXJDbGVhclRpbWVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2hvdmVyOiB0cnVlfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX2hvdmVyQ2xlYXJUaW1lciA9IG5ldyBUaW1lcihIT1ZFUl9NT1ZFX1RJTUVPVVQpO1xyXG4gICAgICAgICAgICB0aGlzLl9ob3ZlckNsZWFyVGltZXIuc3RhcnQoKTtcclxuICAgICAgICAgICAgbGV0IGZpbmlzaGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuX2hvdmVyQ2xlYXJUaW1lci5maW5pc2hlZCgpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIGZpbmlzaGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5faG92ZXJDbGVhclRpbWVyID0gbnVsbDtcclxuICAgICAgICAgICAgaWYgKGZpbmlzaGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtob3ZlcjogZmFsc2V9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3QoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2hvdmVyQ2xlYXJUaW1lci5yZXN0YXJ0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbk1vdXNlTGVhdmU6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2hvdmVyQ2xlYXJUaW1lcikge1xyXG4gICAgICAgICAgICB0aGlzLl9ob3ZlckNsZWFyVGltZXIuYWJvcnQoKTtcclxuICAgICAgICAgICAgdGhpcy5faG92ZXJDbGVhclRpbWVyID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aG92ZXI6IGZhbHNlfSk7XHJcblxyXG4gICAgICAgIC8vIFJlZnJlc2ggdGhlIHJvb20gbGlzdCBqdXN0IGluIGNhc2UgdGhlIHVzZXIgbWlzc2VkIHNvbWV0aGluZy5cclxuICAgICAgICB0aGlzLl9kZWxheWVkUmVmcmVzaFJvb21MaXN0KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9kZWxheWVkUmVmcmVzaFJvb21MaXN0OiByYXRlX2xpbWl0ZWRfZnVuYyhmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hSb29tTGlzdCgpO1xyXG4gICAgfSwgNTAwKSxcclxuXHJcbiAgICAvLyBVcGRhdGUgd2hpY2ggcm9vbXMgYW5kIHVzZXJzIHNob3VsZCBhcHBlYXIgaW4gUm9vbUxpc3QgZm9yIGEgZ2l2ZW4gZ3JvdXAgdGFnXHJcbiAgICB1cGRhdGVWaXNpYmxlUm9vbXNGb3JUYWc6IGZ1bmN0aW9uKGRtUm9vbU1hcCwgdGFnKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm1vdW50ZWQpIHJldHVybjtcclxuICAgICAgICAvLyBGb3Igbm93LCBvbmx5IGhhbmRsZSBncm91cCB0YWdzXHJcbiAgICAgICAgaWYgKHRhZ1swXSAhPT0gJysnKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuX3Zpc2libGVSb29tc0Zvckdyb3VwW3RhZ10gPSBbXTtcclxuICAgICAgICBHcm91cFN0b3JlLmdldEdyb3VwUm9vbXModGFnKS5mb3JFYWNoKChyb29tKSA9PiB0aGlzLl92aXNpYmxlUm9vbXNGb3JHcm91cFt0YWddLnB1c2gocm9vbS5yb29tSWQpKTtcclxuICAgICAgICBHcm91cFN0b3JlLmdldEdyb3VwTWVtYmVycyh0YWcpLmZvckVhY2goKG1lbWJlcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAobWVtYmVyLnVzZXJJZCA9PT0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICBkbVJvb21NYXAuZ2V0RE1Sb29tc0ZvclVzZXJJZChtZW1iZXIudXNlcklkKS5mb3JFYWNoKFxyXG4gICAgICAgICAgICAgICAgKHJvb21JZCkgPT4gdGhpcy5fdmlzaWJsZVJvb21zRm9yR3JvdXBbdGFnXS5wdXNoKHJvb21JZCksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gVE9ETzogQ2hlY2sgaWYgcm9vbSBoYXMgYmVlbiB0YWdnZWQgdG8gdGhlIGdyb3VwIGJ5IHRoZSB1c2VyXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFVwZGF0ZSB3aGljaCByb29tcyBhbmQgdXNlcnMgc2hvdWxkIGFwcGVhciBhY2NvcmRpbmcgdG8gd2hpY2ggdGFncyBhcmUgc2VsZWN0ZWRcclxuICAgIHVwZGF0ZVZpc2libGVSb29tczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRUYWdzID0gVGFnT3JkZXJTdG9yZS5nZXRTZWxlY3RlZFRhZ3MoKTtcclxuICAgICAgICBjb25zdCB2aXNpYmxlR3JvdXBSb29tcyA9IFtdO1xyXG4gICAgICAgIHNlbGVjdGVkVGFncy5mb3JFYWNoKCh0YWcpID0+IHtcclxuICAgICAgICAgICAgKHRoaXMuX3Zpc2libGVSb29tc0Zvckdyb3VwW3RhZ10gfHwgW10pLmZvckVhY2goXHJcbiAgICAgICAgICAgICAgICAocm9vbUlkKSA9PiB2aXNpYmxlR3JvdXBSb29tcy5wdXNoKHJvb21JZCksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIElmIHRoZXJlIGFyZSBhbnkgdGFncyBzZWxlY3RlZCwgY29uc3RyYWluIHRoZSByb29tcyBsaXN0ZWQgdG8gdGhlXHJcbiAgICAgICAgLy8gdmlzaWJsZSByb29tcyBhcyBkZXRlcm1pbmVkIGJ5IHZpc2libGVHcm91cFJvb21zLiBIZXJlLCB3ZVxyXG4gICAgICAgIC8vIGRlLWR1cGxpY2F0ZSBhbmQgZmlsdGVyIG91dCByb29tcyB0aGF0IHRoZSBjbGllbnQgZG9lc24ndCBrbm93XHJcbiAgICAgICAgLy8gYWJvdXQgKGhlbmNlIHRoZSBTZXQgYW5kIHRoZSBudWxsLWd1YXJkIG9uIGByb29tYCkuXHJcbiAgICAgICAgaWYgKHNlbGVjdGVkVGFncy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21TZXQgPSBuZXcgU2V0KCk7XHJcbiAgICAgICAgICAgIHZpc2libGVHcm91cFJvb21zLmZvckVhY2goKHJvb21JZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvb21TZXQuYWRkKHJvb20pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5fdmlzaWJsZVJvb21zID0gQXJyYXkuZnJvbShyb29tU2V0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBTaG93IGFsbCByb29tc1xyXG4gICAgICAgICAgICB0aGlzLl92aXNpYmxlUm9vbXMgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VmlzaWJsZVJvb21zKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2RlbGF5ZWRSZWZyZXNoUm9vbUxpc3QoKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVmcmVzaFJvb21MaXN0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5ob3Zlcikge1xyXG4gICAgICAgICAgICAvLyBEb24ndCByZS1zb3J0IHRoZSBsaXN0IGlmIHdlJ3JlIGhvdmVyaW5nIG92ZXIgdGhlIGxpc3RcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVE9ETzogaWRlYWxseSB3ZSdkIGNhbGN1bGF0ZSB0aGlzIG9uY2UgYXQgc3RhcnQsIGFuZCB0aGVuIG1haW50YWluXHJcbiAgICAgICAgLy8gYW55IGNoYW5nZXMgdG8gaXQgaW5jcmVtZW50YWxseSwgdXBkYXRpbmcgdGhlIGFwcHJvcHJpYXRlIHN1Ymxpc3RzXHJcbiAgICAgICAgLy8gYXMgbmVlZGVkLlxyXG4gICAgICAgIC8vIEFsdGVybmF0aXZlbHkgd2UnZCBkbyBzb21ldGhpbmcgbWFnaWNhbCB3aXRoIEltbXV0YWJsZS5qcyBvciBzaW1pbGFyLlxyXG4gICAgICAgIGNvbnN0IGxpc3RzID0gdGhpcy5nZXRSb29tTGlzdHMoKTtcclxuICAgICAgICBsZXQgdG90YWxSb29tcyA9IDA7XHJcbiAgICAgICAgZm9yIChjb25zdCBsIG9mIE9iamVjdC52YWx1ZXMobGlzdHMpKSB7XHJcbiAgICAgICAgICAgIHRvdGFsUm9vbXMgKz0gbC5sZW5ndGg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBsaXN0cyxcclxuICAgICAgICAgICAgdG90YWxSb29tQ291bnQ6IHRvdGFsUm9vbXMsXHJcbiAgICAgICAgICAgIC8vIERvIHRoaXMgaGVyZSBzbyBhcyB0byBub3QgcmVuZGVyIGV2ZXJ5IHRpbWUgdGhlIHNlbGVjdGVkIHRhZ3NcclxuICAgICAgICAgICAgLy8gdGhlbXNlbHZlcyBjaGFuZ2UuXHJcbiAgICAgICAgICAgIHNlbGVjdGVkVGFnczogVGFnT3JkZXJTdG9yZS5nZXRTZWxlY3RlZFRhZ3MoKSxcclxuICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIHdlIGRvbid0IG5lZWQgdG8gcmVzdG9yZSBhbnkgc2l6ZSBoZXJlLCBkbyB3ZT9cclxuICAgICAgICAgICAgLy8gaSBndWVzcyB3ZSBjb3VsZCBoYXZlIHRyaWdnZXJlZCBhIG5ldyBncm91cCB0byBhcHBlYXJcclxuICAgICAgICAgICAgLy8gdGhhdCBhbHJlYWR5IGFuIGV4cGxpY2l0IHNpemUgdGhlIGxhc3QgdGltZSBpdCBhcHBlYXJlZCAuLi5cclxuICAgICAgICAgICAgdGhpcy5fY2hlY2tTdWJMaXN0c092ZXJmbG93KCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIHRoaXMuX2xhc3RSZWZyZXNoUm9vbUxpc3RUcyA9IERhdGUubm93KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFRhZ05hbWVGb3JSb29tSWQ6IGZ1bmN0aW9uKHJvb21JZCkge1xyXG4gICAgICAgIGNvbnN0IGxpc3RzID0gUm9vbUxpc3RTdG9yZS5nZXRSb29tTGlzdHMoKTtcclxuICAgICAgICBmb3IgKGNvbnN0IHRhZ05hbWUgb2YgT2JqZWN0LmtleXMobGlzdHMpKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qgcm9vbSBvZiBsaXN0c1t0YWdOYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgLy8gU2hvdWxkIGJlIGltcG9zc2libGUsIGJ1dCBndWFyZCBhbnl3YXlzLlxyXG4gICAgICAgICAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjb25zdCBteVVzZXJJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKTtcclxuICAgICAgICAgICAgICAgIGlmIChISURFX0NPTkZFUkVOQ0VfQ0hBTlMgJiYgUm9vbXMuaXNDb25mQ2FsbFJvb20ocm9vbSwgbXlVc2VySWQsIHRoaXMucHJvcHMuQ29uZmVyZW5jZUhhbmRsZXIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJvb20ucm9vbUlkID09PSByb29tSWQpIHJldHVybiB0YWdOYW1lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0Um9vbUxpc3RzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBsaXN0cyA9IFJvb21MaXN0U3RvcmUuZ2V0Um9vbUxpc3RzKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGZpbHRlcmVkTGlzdHMgPSB7fTtcclxuXHJcbiAgICAgICAgY29uc3QgaXNSb29tVmlzaWJsZSA9IHtcclxuICAgICAgICAgICAgLy8gJHJvb21JZDogdHJ1ZSxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLl92aXNpYmxlUm9vbXMuZm9yRWFjaCgocikgPT4ge1xyXG4gICAgICAgICAgICBpc1Jvb21WaXNpYmxlW3Iucm9vbUlkXSA9IHRydWU7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIE9iamVjdC5rZXlzKGxpc3RzKS5mb3JFYWNoKCh0YWdOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlcmVkUm9vbXMgPSBsaXN0c1t0YWdOYW1lXS5maWx0ZXIoKHRhZ2dlZFJvb20pID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIFNvbWV3aGF0IGltcG9zc2libGUsIGJ1dCBndWFyZCBhZ2FpbnN0IGl0IGFueXdheVxyXG4gICAgICAgICAgICAgICAgaWYgKCF0YWdnZWRSb29tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY29uc3QgbXlVc2VySWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoSElERV9DT05GRVJFTkNFX0NIQU5TICYmIFJvb21zLmlzQ29uZkNhbGxSb29tKHRhZ2dlZFJvb20sIG15VXNlcklkLCB0aGlzLnByb3BzLkNvbmZlcmVuY2VIYW5kbGVyKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gQm9vbGVhbihpc1Jvb21WaXNpYmxlW3RhZ2dlZFJvb20ucm9vbUlkXSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKGZpbHRlcmVkUm9vbXMubGVuZ3RoID4gMCB8fCB0YWdOYW1lLm1hdGNoKFNUQU5EQVJEX1RBR1NfUkVHRVgpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJlZExpc3RzW3RhZ05hbWVdID0gZmlsdGVyZWRSb29tcztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRMaXN0cztcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFNjcm9sbE5vZGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5tb3VudGVkKSByZXR1cm4gbnVsbDtcclxuICAgICAgICBjb25zdCBwYW5lbCA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMpO1xyXG4gICAgICAgIGlmICghcGFuZWwpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBpZiAocGFuZWwuY2xhc3NMaXN0LmNvbnRhaW5zKCdnbS1wcmV2ZW50ZWQnKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gcGFuZWw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBhbmVsLmNoaWxkcmVuWzJdOyAvLyBYWFg6IEZyYWdpbGUhXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfd2hlblNjcm9sbGluZzogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHRoaXMuX2hpZGVUb29sdGlwKGUpO1xyXG4gICAgICAgIHRoaXMuX3JlcG9zaXRpb25JbmNvbWluZ0NhbGxCb3goZSwgZmFsc2UpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaGlkZVRvb2x0aXA6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAvLyBIaWRlIHRvb2x0aXAgd2hlbiBzY3JvbGxpbmcsIGFzIHdlJ2xsIG5vIGxvbmdlciBiZSBvdmVyIHRoZSBvbmUgd2Ugd2VyZSBvblxyXG4gICAgICAgIGlmICh0aGlzLnRvb2x0aXAgJiYgdGhpcy50b29sdGlwLnN0eWxlLmRpc3BsYXkgIT09IFwibm9uZVwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbHRpcC5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfcmVwb3NpdGlvbkluY29taW5nQ2FsbEJveDogZnVuY3Rpb24oZSwgZmlyc3RUaW1lKSB7XHJcbiAgICAgICAgY29uc3QgaW5jb21pbmdDYWxsQm94ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbmNvbWluZ0NhbGxCb3hcIik7XHJcbiAgICAgICAgaWYgKGluY29taW5nQ2FsbEJveCAmJiBpbmNvbWluZ0NhbGxCb3gucGFyZW50RWxlbWVudCkge1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxBcmVhID0gdGhpcy5fZ2V0U2Nyb2xsTm9kZSgpO1xyXG4gICAgICAgICAgICBpZiAoIXNjcm9sbEFyZWEpIHJldHVybjtcclxuICAgICAgICAgICAgLy8gVXNlIHRoZSBvZmZzZXQgb2YgdGhlIHRvcCBvZiB0aGUgc2Nyb2xsIGFyZWEgZnJvbSB0aGUgd2luZG93XHJcbiAgICAgICAgICAgIC8vIGFzIHRoaXMgaXMgdXNlZCB0byBjYWxjdWxhdGUgdGhlIENTUyBmaXhlZCB0b3AgcG9zaXRpb24gZm9yIHRoZSBzdGlja2llc1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxBcmVhT2Zmc2V0ID0gc2Nyb2xsQXJlYS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AgKyB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgICAgIC8vIFVzZSB0aGUgb2Zmc2V0IG9mIHRoZSB0b3Agb2YgdGhlIGNvbXBvbmVudCBmcm9tIHRoZSB3aW5kb3dcclxuICAgICAgICAgICAgLy8gYXMgdGhpcyBpcyB1c2VkIHRvIGNhbGN1bGF0ZSB0aGUgQ1NTIGZpeGVkIHRvcCBwb3NpdGlvbiBmb3IgdGhlIHN0aWNraWVzXHJcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbEFyZWFIZWlnaHQgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQ7XHJcblxyXG4gICAgICAgICAgICBsZXQgdG9wID0gKGluY29taW5nQ2FsbEJveC5wYXJlbnRFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCArIHdpbmRvdy5wYWdlWU9mZnNldCk7XHJcbiAgICAgICAgICAgIC8vIE1ha2Ugc3VyZSB3ZSBkb24ndCBnbyB0b28gZmFyIHVwLCBpZiB0aGUgaGVhZGVycyBhcmVuJ3Qgc3RpY2t5XHJcbiAgICAgICAgICAgIHRvcCA9ICh0b3AgPCBzY3JvbGxBcmVhT2Zmc2V0KSA/IHNjcm9sbEFyZWFPZmZzZXQgOiB0b3A7XHJcbiAgICAgICAgICAgIC8vIG1ha2Ugc3VyZSB3ZSBkb24ndCBnbyB0b28gZmFyIGRvd24sIGlmIHRoZSBoZWFkZXJzIGFyZW4ndCBzdGlja3lcclxuICAgICAgICAgICAgY29uc3QgYm90dG9tTWFyZ2luID0gc2Nyb2xsQXJlYU9mZnNldCArIChzY3JvbGxBcmVhSGVpZ2h0IC0gNDUpO1xyXG4gICAgICAgICAgICB0b3AgPSAodG9wID4gYm90dG9tTWFyZ2luKSA/IGJvdHRvbU1hcmdpbiA6IHRvcDtcclxuXHJcbiAgICAgICAgICAgIGluY29taW5nQ2FsbEJveC5zdHlsZS50b3AgPSB0b3AgKyBcInB4XCI7XHJcbiAgICAgICAgICAgIGluY29taW5nQ2FsbEJveC5zdHlsZS5sZWZ0ID0gc2Nyb2xsQXJlYS5vZmZzZXRMZWZ0ICsgc2Nyb2xsQXJlYS5vZmZzZXRXaWR0aCArIDEyICsgXCJweFwiO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX21ha2VHcm91cEludml0ZVRpbGVzKGZpbHRlcikge1xyXG4gICAgICAgIGNvbnN0IHJldCA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGxjRmlsdGVyID0gZmlsdGVyICYmIGZpbHRlci50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICBjb25zdCBHcm91cEludml0ZVRpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KCdncm91cHMuR3JvdXBJbnZpdGVUaWxlJyk7XHJcbiAgICAgICAgZm9yIChjb25zdCBncm91cCBvZiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0R3JvdXBzKCkpIHtcclxuICAgICAgICAgICAgY29uc3Qge2dyb3VwSWQsIG5hbWUsIG15TWVtYmVyc2hpcH0gPSBncm91cDtcclxuICAgICAgICAgICAgLy8gZmlsdGVyIHRvIG9ubHkgZ3JvdXBzIGluIGludml0ZSBzdGF0ZSBhbmQgZ3JvdXBfaWQgc3RhcnRzIHdpdGggZmlsdGVyIG9yIGdyb3VwIG5hbWUgaW5jbHVkZXMgaXRcclxuICAgICAgICAgICAgaWYgKG15TWVtYmVyc2hpcCAhPT0gJ2ludml0ZScpIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICBpZiAobGNGaWx0ZXIgJiYgIWdyb3VwSWQudG9Mb3dlckNhc2UoKS5zdGFydHNXaXRoKGxjRmlsdGVyKSAmJlxyXG4gICAgICAgICAgICAgICAgIShuYW1lICYmIG5hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhsY0ZpbHRlcikpKSBjb250aW51ZTtcclxuICAgICAgICAgICAgcmV0LnB1c2goPEdyb3VwSW52aXRlVGlsZSBrZXk9e2dyb3VwSWR9IGdyb3VwPXtncm91cH0gY29sbGFwc2VkPXt0aGlzLnByb3BzLmNvbGxhcHNlZH0gLz4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJldDtcclxuICAgIH0sXHJcblxyXG4gICAgX2FwcGx5U2VhcmNoRmlsdGVyOiBmdW5jdGlvbihsaXN0LCBmaWx0ZXIpIHtcclxuICAgICAgICBpZiAoZmlsdGVyID09PSBcIlwiKSByZXR1cm4gbGlzdDtcclxuICAgICAgICBjb25zdCBsY0ZpbHRlciA9IGZpbHRlci50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIC8vIGFwcGx5IHRvTG93ZXJDYXNlIGJlZm9yZSBhbmQgYWZ0ZXIgcmVtb3ZlSGlkZGVuQ2hhcnMgYmVjYXVzZSBkaWZmZXJlbnQgcnVsZXMgZ2V0IGFwcGxpZWRcclxuICAgICAgICAvLyBlLmcgTSAtPiBNIGJ1dCBtIC0+IG4sIHlldCBzb21lIHVuaWNvZGUgaG9tb2dseXBocyBjb21lIG91dCBhcyB1cHBlcmNhc2UsIGUuZyDwnZquIC0+IEhcclxuICAgICAgICBjb25zdCBmdXp6eUZpbHRlciA9IHV0aWxzLnJlbW92ZUhpZGRlbkNoYXJzKGxjRmlsdGVyKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIC8vIGNhc2UgaW5zZW5zaXRpdmUgaWYgcm9vbSBuYW1lIGluY2x1ZGVzIGZpbHRlcixcclxuICAgICAgICAvLyBvciBpZiBzdGFydHMgd2l0aCBgI2AgYW5kIG9uZSBvZiByb29tJ3MgYWxpYXNlcyBzdGFydHMgd2l0aCBmaWx0ZXJcclxuICAgICAgICByZXR1cm4gbGlzdC5maWx0ZXIoKHJvb20pID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpbHRlclswXSA9PT0gXCIjXCIpIHtcclxuICAgICAgICAgICAgICAgIGlmIChyb29tLmdldENhbm9uaWNhbEFsaWFzKCkgJiYgcm9vbS5nZXRDYW5vbmljYWxBbGlhcygpLnRvTG93ZXJDYXNlKCkuc3RhcnRzV2l0aChsY0ZpbHRlcikpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChyb29tLmdldEFsdEFsaWFzZXMoKS5zb21lKChhbGlhcykgPT4gYWxpYXMudG9Mb3dlckNhc2UoKS5zdGFydHNXaXRoKGxjRmlsdGVyKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcm9vbS5uYW1lICYmIHV0aWxzLnJlbW92ZUhpZGRlbkNoYXJzKHJvb20ubmFtZS50b0xvd2VyQ2FzZSgpKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKGZ1enp5RmlsdGVyKTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2hhbmRsZUNvbGxhcHNlZFN0YXRlOiBmdW5jdGlvbihrZXksIGNvbGxhcHNlZCkge1xyXG4gICAgICAgIC8vIHBlcnNpc3QgY29sbGFwc2VkIHN0YXRlXHJcbiAgICAgICAgdGhpcy5jb2xsYXBzZWRTdGF0ZVtrZXldID0gY29sbGFwc2VkO1xyXG4gICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm14X3Jvb21saXN0X2NvbGxhcHNlZFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmNvbGxhcHNlZFN0YXRlKSk7XHJcbiAgICAgICAgLy8gbG9hZCB0aGUgcGVyc2lzdGVkIHNpemUgY29uZmlndXJhdGlvbiBvZiB0aGUgZXhwYW5kZWQgc3ViIGxpc3RcclxuICAgICAgICBpZiAoY29sbGFwc2VkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xheW91dC5jb2xsYXBzZVNlY3Rpb24oa2V5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9sYXlvdXQuZXhwYW5kU2VjdGlvbihrZXksIHRoaXMuc3ViTGlzdFNpemVzW2tleV0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBjaGVjayBvdmVyZmxvdywgYXMgc3ViIGxpc3RzIHNpemVzIGhhdmUgY2hhbmdlZFxyXG4gICAgICAgIC8vIGltcG9ydGFudCB0aGlzIGhhcHBlbnMgYWZ0ZXIgY2FsbGluZyByZXNpemUgYWJvdmVcclxuICAgICAgICB0aGlzLl9jaGVja1N1Ykxpc3RzT3ZlcmZsb3coKTtcclxuICAgIH0sXHJcblxyXG4gICAgLy8gY2hlY2sgb3ZlcmZsb3cgZm9yIHNjcm9sbCBpbmRpY2F0b3IgZ3JhZGllbnRcclxuICAgIF9jaGVja1N1Ykxpc3RzT3ZlcmZsb3coKSB7XHJcbiAgICAgICAgT2JqZWN0LnZhbHVlcyh0aGlzLl9zdWJMaXN0UmVmcykuZm9yRWFjaChsID0+IGwuY2hlY2tPdmVyZmxvdygpKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3N1Ykxpc3RSZWY6IGZ1bmN0aW9uKGtleSwgcmVmKSB7XHJcbiAgICAgICAgaWYgKCFyZWYpIHtcclxuICAgICAgICAgICAgZGVsZXRlIHRoaXMuX3N1Ykxpc3RSZWZzW2tleV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViTGlzdFJlZnNba2V5XSA9IHJlZjtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9tYXBTdWJMaXN0UHJvcHM6IGZ1bmN0aW9uKHN1Ykxpc3RzUHJvcHMpIHtcclxuICAgICAgICB0aGlzLl9sYXlvdXRTZWN0aW9ucyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICAgICAgY29sbGFwc2VkOiB0aGlzLnByb3BzLmNvbGxhcHNlZCxcclxuICAgICAgICAgICAgaXNGaWx0ZXJlZDogISF0aGlzLnByb3BzLnNlYXJjaEZpbHRlcixcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBzdWJMaXN0c1Byb3BzLmZvckVhY2goKHApID0+IHtcclxuICAgICAgICAgICAgcC5saXN0ID0gdGhpcy5fYXBwbHlTZWFyY2hGaWx0ZXIocC5saXN0LCB0aGlzLnByb3BzLnNlYXJjaEZpbHRlcik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHN1Ykxpc3RzUHJvcHMgPSBzdWJMaXN0c1Byb3BzLmZpbHRlcigocHJvcHMgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBsZW4gPSBwcm9wcy5saXN0Lmxlbmd0aCArIChwcm9wcy5leHRyYVRpbGVzID8gcHJvcHMuZXh0cmFUaWxlcy5sZW5ndGggOiAwKTtcclxuICAgICAgICAgICAgcmV0dXJuIGxlbiAhPT0gMCB8fCBwcm9wcy5vbkFkZFJvb207XHJcbiAgICAgICAgfSkpO1xyXG5cclxuICAgICAgICByZXR1cm4gc3ViTGlzdHNQcm9wcy5yZWR1Y2UoKGNvbXBvbmVudHMsIHByb3BzLCBpKSA9PiB7XHJcbiAgICAgICAgICAgIHByb3BzID0gey4uLmRlZmF1bHRQcm9wcywgLi4ucHJvcHN9O1xyXG4gICAgICAgICAgICBjb25zdCBpc0xhc3QgPSBpID09PSBzdWJMaXN0c1Byb3BzLmxlbmd0aCAtIDE7XHJcbiAgICAgICAgICAgIGNvbnN0IGxlbiA9IHByb3BzLmxpc3QubGVuZ3RoICsgKHByb3BzLmV4dHJhVGlsZXMgPyBwcm9wcy5leHRyYVRpbGVzLmxlbmd0aCA6IDApO1xyXG4gICAgICAgICAgICBjb25zdCB7a2V5LCBsYWJlbCwgb25IZWFkZXJDbGljaywgLi4ub3RoZXJQcm9wc30gPSBwcm9wcztcclxuICAgICAgICAgICAgY29uc3QgY2hvc2VuS2V5ID0ga2V5IHx8IGxhYmVsO1xyXG4gICAgICAgICAgICBjb25zdCBvblN1Ykxpc3RIZWFkZXJDbGljayA9IChjb2xsYXBzZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2hhbmRsZUNvbGxhcHNlZFN0YXRlKGNob3NlbktleSwgY29sbGFwc2VkKTtcclxuICAgICAgICAgICAgICAgIGlmIChvbkhlYWRlckNsaWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb25IZWFkZXJDbGljayhjb2xsYXBzZWQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBjb25zdCBzdGFydEFzSGlkZGVuID0gcHJvcHMuc3RhcnRBc0hpZGRlbiB8fCB0aGlzLmNvbGxhcHNlZFN0YXRlW2Nob3NlbktleV07XHJcbiAgICAgICAgICAgIHRoaXMuX2xheW91dFNlY3Rpb25zLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgaWQ6IGNob3NlbktleSxcclxuICAgICAgICAgICAgICAgIGNvdW50OiBsZW4sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBzdWJMaXN0ID0gKDxSb29tU3ViTGlzdFxyXG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9zdWJMaXN0UmVmLmJpbmQodGhpcywgY2hvc2VuS2V5KX1cclxuICAgICAgICAgICAgICAgIHN0YXJ0QXNIaWRkZW49e3N0YXJ0QXNIaWRkZW59XHJcbiAgICAgICAgICAgICAgICBmb3JjZUV4cGFuZD17ISF0aGlzLnByb3BzLnNlYXJjaEZpbHRlcn1cclxuICAgICAgICAgICAgICAgIG9uSGVhZGVyQ2xpY2s9e29uU3ViTGlzdEhlYWRlckNsaWNrfVxyXG4gICAgICAgICAgICAgICAga2V5PXtjaG9zZW5LZXl9XHJcbiAgICAgICAgICAgICAgICBsYWJlbD17bGFiZWx9XHJcbiAgICAgICAgICAgICAgICB7Li4ub3RoZXJQcm9wc30gLz4pO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFpc0xhc3QpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb21wb25lbnRzLmNvbmNhdChcclxuICAgICAgICAgICAgICAgICAgICBzdWJMaXN0LFxyXG4gICAgICAgICAgICAgICAgICAgIDxSZXNpemVIYW5kbGUga2V5PXtjaG9zZW5LZXkrXCItcmVzaXplclwifSB2ZXJ0aWNhbD17dHJ1ZX0gaWQ9e2Nob3NlbktleX0gLz5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY29tcG9uZW50cy5jb25jYXQoc3ViTGlzdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCBbXSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jb2xsZWN0UmVzaXplQ29udGFpbmVyOiBmdW5jdGlvbihlbCkge1xyXG4gICAgICAgIHRoaXMucmVzaXplQ29udGFpbmVyID0gZWw7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgaW5jb21pbmdDYWxsSWZUYWdnZWRBcyA9ICh0YWdOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZS5pbmNvbWluZ0NhbGwpIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5pbmNvbWluZ0NhbGxUYWcgIT09IHRhZ05hbWUpIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5pbmNvbWluZ0NhbGw7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IHN1Ykxpc3RzID0gW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsaXN0OiBbXSxcclxuICAgICAgICAgICAgICAgIGV4dHJhVGlsZXM6IHRoaXMuX21ha2VHcm91cEludml0ZVRpbGVzKHRoaXMucHJvcHMuc2VhcmNoRmlsdGVyKSxcclxuICAgICAgICAgICAgICAgIGxhYmVsOiBfdCgnQ29tbXVuaXR5IEludml0ZXMnKSxcclxuICAgICAgICAgICAgICAgIGlzSW52aXRlOiB0cnVlLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsaXN0OiB0aGlzLnN0YXRlLmxpc3RzWydpbS52ZWN0b3IuZmFrZS5pbnZpdGUnXSxcclxuICAgICAgICAgICAgICAgIGxhYmVsOiBfdCgnSW52aXRlcycpLFxyXG4gICAgICAgICAgICAgICAgaW5jb21pbmdDYWxsOiBpbmNvbWluZ0NhbGxJZlRhZ2dlZEFzKCdpbS52ZWN0b3IuZmFrZS5pbnZpdGUnKSxcclxuICAgICAgICAgICAgICAgIGlzSW52aXRlOiB0cnVlLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsaXN0OiB0aGlzLnN0YXRlLmxpc3RzWydtLmZhdm91cml0ZSddLFxyXG4gICAgICAgICAgICAgICAgbGFiZWw6IF90KCdGYXZvdXJpdGVzJyksXHJcbiAgICAgICAgICAgICAgICB0YWdOYW1lOiBcIm0uZmF2b3VyaXRlXCIsXHJcbiAgICAgICAgICAgICAgICBpbmNvbWluZ0NhbGw6IGluY29taW5nQ2FsbElmVGFnZ2VkQXMoJ20uZmF2b3VyaXRlJyksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGxpc3Q6IHRoaXMuc3RhdGUubGlzdHNbVEFHX0RNXSxcclxuICAgICAgICAgICAgICAgIGxhYmVsOiBfdCgnRGlyZWN0IE1lc3NhZ2VzJyksXHJcbiAgICAgICAgICAgICAgICB0YWdOYW1lOiBUQUdfRE0sXHJcbiAgICAgICAgICAgICAgICBpbmNvbWluZ0NhbGw6IGluY29taW5nQ2FsbElmVGFnZ2VkQXMoVEFHX0RNKSxcclxuICAgICAgICAgICAgICAgIG9uQWRkUm9vbTogKCkgPT4ge2Rpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19jcmVhdGVfY2hhdCd9KTt9LFxyXG4gICAgICAgICAgICAgICAgYWRkUm9vbUxhYmVsOiBfdChcIlN0YXJ0IGNoYXRcIiksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGxpc3Q6IHRoaXMuc3RhdGUubGlzdHNbJ2ltLnZlY3Rvci5mYWtlLnJlY2VudCddLFxyXG4gICAgICAgICAgICAgICAgbGFiZWw6IF90KCdSb29tcycpLFxyXG4gICAgICAgICAgICAgICAgaW5jb21pbmdDYWxsOiBpbmNvbWluZ0NhbGxJZlRhZ2dlZEFzKCdpbS52ZWN0b3IuZmFrZS5yZWNlbnQnKSxcclxuICAgICAgICAgICAgICAgIG9uQWRkUm9vbTogKCkgPT4ge2Rpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19jcmVhdGVfcm9vbSd9KTt9LFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF07XHJcbiAgICAgICAgY29uc3QgdGFnU3ViTGlzdHMgPSBPYmplY3Qua2V5cyh0aGlzLnN0YXRlLmxpc3RzKVxyXG4gICAgICAgICAgICAuZmlsdGVyKCh0YWdOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKCF0aGlzLnN0YXRlLmN1c3RvbVRhZ3MgfHwgdGhpcy5zdGF0ZS5jdXN0b21UYWdzW3RhZ05hbWVdKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICF0YWdOYW1lLm1hdGNoKFNUQU5EQVJEX1RBR1NfUkVHRVgpO1xyXG4gICAgICAgICAgICB9KS5tYXAoKHRhZ05hbWUpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGlzdDogdGhpcy5zdGF0ZS5saXN0c1t0YWdOYW1lXSxcclxuICAgICAgICAgICAgICAgICAgICBrZXk6IHRhZ05hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGxhYmVsRm9yVGFnTmFtZSh0YWdOYW1lKSxcclxuICAgICAgICAgICAgICAgICAgICB0YWdOYW1lOiB0YWdOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbDogaW5jb21pbmdDYWxsSWZUYWdnZWRBcyh0YWdOYW1lKSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIHN1Ykxpc3RzID0gc3ViTGlzdHMuY29uY2F0KHRhZ1N1Ykxpc3RzKTtcclxuICAgICAgICBzdWJMaXN0cyA9IHN1Ykxpc3RzLmNvbmNhdChbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGxpc3Q6IHRoaXMuc3RhdGUubGlzdHNbJ20ubG93cHJpb3JpdHknXSxcclxuICAgICAgICAgICAgICAgIGxhYmVsOiBfdCgnTG93IHByaW9yaXR5JyksXHJcbiAgICAgICAgICAgICAgICB0YWdOYW1lOiBcIm0ubG93cHJpb3JpdHlcIixcclxuICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbDogaW5jb21pbmdDYWxsSWZUYWdnZWRBcygnbS5sb3dwcmlvcml0eScpLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsaXN0OiB0aGlzLnN0YXRlLmxpc3RzWydpbS52ZWN0b3IuZmFrZS5hcmNoaXZlZCddLFxyXG4gICAgICAgICAgICAgICAgbGFiZWw6IF90KCdIaXN0b3JpY2FsJyksXHJcbiAgICAgICAgICAgICAgICBpbmNvbWluZ0NhbGw6IGluY29taW5nQ2FsbElmVGFnZ2VkQXMoJ2ltLnZlY3Rvci5mYWtlLmFyY2hpdmVkJyksXHJcbiAgICAgICAgICAgICAgICBzdGFydEFzSGlkZGVuOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgc2hvd1NwaW5uZXI6IHRoaXMuc3RhdGUuaXNMb2FkaW5nTGVmdFJvb21zLFxyXG4gICAgICAgICAgICAgICAgb25IZWFkZXJDbGljazogdGhpcy5vbkFyY2hpdmVkSGVhZGVyQ2xpY2ssXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGxpc3Q6IHRoaXMuc3RhdGUubGlzdHNbJ20uc2VydmVyX25vdGljZSddLFxyXG4gICAgICAgICAgICAgICAgbGFiZWw6IF90KCdTeXN0ZW0gQWxlcnRzJyksXHJcbiAgICAgICAgICAgICAgICB0YWdOYW1lOiBcIm0ubG93cHJpb3JpdHlcIixcclxuICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbDogaW5jb21pbmdDYWxsSWZUYWdnZWRBcygnbS5zZXJ2ZXJfbm90aWNlJyksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSk7XHJcblxyXG4gICAgICAgIGNvbnN0IHN1Ykxpc3RDb21wb25lbnRzID0gdGhpcy5fbWFwU3ViTGlzdFByb3BzKHN1Ykxpc3RzKTtcclxuXHJcbiAgICAgICAgY29uc3Qge3Jlc2l6ZU5vdGlmaWVyLCBjb2xsYXBzZWQsIHNlYXJjaEZpbHRlciwgQ29uZmVyZW5jZUhhbmRsZXIsIG9uS2V5RG93biwgLi4ucHJvcHN9ID0gdGhpcy5wcm9wczsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxSb3ZpbmdUYWJJbmRleFByb3ZpZGVyIGhhbmRsZUhvbWVFbmQ9e3RydWV9IG9uS2V5RG93bj17b25LZXlEb3dufT5cclxuICAgICAgICAgICAgICAgIHsoe29uS2V5RG93bkhhbmRsZXJ9KSA9PiA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgey4uLnByb3BzfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uS2V5RG93bj17b25LZXlEb3duSGFuZGxlcn1cclxuICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX2NvbGxlY3RSZXNpemVDb250YWluZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfUm9vbUxpc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJ0cmVlXCJcclxuICAgICAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPXtfdChcIlJvb21zXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIEZpcmVmb3ggc29tZXRpbWVzIG1ha2VzIHRoaXMgZWxlbWVudCBmb2N1c2FibGUgZHVlIHRvXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb3ZlcmZsb3c6c2Nyb2xsOywgc28gZm9yY2UgaXQgb3V0IG9mIHRhYiBvcmRlci5cclxuICAgICAgICAgICAgICAgICAgICB0YWJJbmRleD1cIi0xXCJcclxuICAgICAgICAgICAgICAgICAgICBvbk1vdXNlTW92ZT17dGhpcy5vbk1vdXNlTW92ZX1cclxuICAgICAgICAgICAgICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgc3ViTGlzdENvbXBvbmVudHMgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+IH1cclxuICAgICAgICAgICAgPC9Sb3ZpbmdUYWJJbmRleFByb3ZpZGVyPlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19