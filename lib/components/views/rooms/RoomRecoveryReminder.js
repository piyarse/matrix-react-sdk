"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard3(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _SettingsStore = _interopRequireWildcard3(require("../../../settings/SettingsStore"));

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class RoomRecoveryReminder extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "showSetupDialog", () => {
      if (this.state.backupInfo) {
        // A key backup exists for this account, but the creating device is not
        // verified, so restore the backup which will give us the keys from it and
        // allow us to trust it (ie. upload keys to it)
        const RestoreKeyBackupDialog = sdk.getComponent('dialogs.keybackup.RestoreKeyBackupDialog');

        _Modal.default.createTrackedDialog('Restore Backup', '', RestoreKeyBackupDialog, null, null,
        /* priority = */
        false,
        /* static = */
        true);
      } else {
        _Modal.default.createTrackedDialogAsync("Key Backup", "Key Backup", Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require("../../../async-components/views/dialogs/keybackup/CreateKeyBackupDialog"))), null, null,
        /* priority = */
        false,
        /* static = */
        true);
      }
    });
    (0, _defineProperty2.default)(this, "onOnNotNowClick", () => {
      this.setState({
        notNowClicked: true
      });
    });
    (0, _defineProperty2.default)(this, "onDontAskAgainClick", () => {
      // When you choose "Don't ask again" from the room reminder, we show a
      // dialog to confirm the choice.
      _Modal.default.createTrackedDialogAsync("Ignore Recovery Reminder", "Ignore Recovery Reminder", Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require("../../../async-components/views/dialogs/keybackup/IgnoreRecoveryReminderDialog"))), {
        onDontAskAgain: async () => {
          await _SettingsStore.default.setValue("showRoomRecoveryReminder", null, _SettingsStore.SettingLevel.ACCOUNT, false);
          this.props.onDontAskAgainSet();
        },
        onSetup: () => {
          this.showSetupDialog();
        }
      });
    });
    (0, _defineProperty2.default)(this, "onSetupClick", () => {
      this.showSetupDialog();
    });
    this.state = {
      loading: true,
      error: null,
      backupInfo: null,
      notNowClicked: false
    };
  }

  componentDidMount() {
    this._loadBackupStatus();
  }

  async _loadBackupStatus() {
    try {
      const backupInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
      this.setState({
        loading: false,
        backupInfo
      });
    } catch (e) {
      console.log("Unable to fetch key backup status", e);
      this.setState({
        loading: false,
        error: e
      });
    }
  }

  render() {
    // If there was an error loading just don't display the banner: we'll try again
    // next time the user switchs to the room.
    if (this.state.error || this.state.loading || this.state.notNowClicked) {
      return null;
    }

    const AccessibleButton = sdk.getComponent("views.elements.AccessibleButton");
    let setupCaption;

    if (this.state.backupInfo) {
      setupCaption = (0, _languageHandler._t)("Connect this session to Key Backup");
    } else {
      setupCaption = (0, _languageHandler._t)("Start using Key Backup");
    }

    return _react.default.createElement("div", {
      className: "mx_RoomRecoveryReminder"
    }, _react.default.createElement("div", {
      className: "mx_RoomRecoveryReminder_header"
    }, (0, _languageHandler._t)("Never lose encrypted messages")), _react.default.createElement("div", {
      className: "mx_RoomRecoveryReminder_body"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Messages in this room are secured with end-to-end " + "encryption. Only you and the recipient(s) have the " + "keys to read these messages.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Securely back up your keys to avoid losing them. " + "<a>Learn more.</a>", {}, {
      // TODO: We don't have this link yet: this will prevent the translators
      // having to re-translate the string when we do.
      a: sub => ''
    }))), _react.default.createElement("div", {
      className: "mx_RoomRecoveryReminder_buttons"
    }, _react.default.createElement(AccessibleButton, {
      className: "mx_RoomRecoveryReminder_button",
      onClick: this.onSetupClick
    }, setupCaption), _react.default.createElement(AccessibleButton, {
      className: "mx_RoomRecoveryReminder_secondary mx_linkButton",
      onClick: this.onOnNotNowClick
    }, (0, _languageHandler._t)("Not now")), _react.default.createElement(AccessibleButton, {
      className: "mx_RoomRecoveryReminder_secondary mx_linkButton",
      onClick: this.onDontAskAgainClick
    }, (0, _languageHandler._t)("Don't ask me again"))));
  }

}

exports.default = RoomRecoveryReminder;
(0, _defineProperty2.default)(RoomRecoveryReminder, "propTypes", {
  // called if the user sets the option to suppress this reminder in the future
  onDontAskAgainSet: _propTypes.default.func
});
(0, _defineProperty2.default)(RoomRecoveryReminder, "defaultProps", {
  onDontAskAgainSet: function () {}
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21SZWNvdmVyeVJlbWluZGVyLmpzIl0sIm5hbWVzIjpbIlJvb21SZWNvdmVyeVJlbWluZGVyIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInN0YXRlIiwiYmFja3VwSW5mbyIsIlJlc3RvcmVLZXlCYWNrdXBEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJjcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMiLCJzZXRTdGF0ZSIsIm5vdE5vd0NsaWNrZWQiLCJvbkRvbnRBc2tBZ2FpbiIsIlNldHRpbmdzU3RvcmUiLCJzZXRWYWx1ZSIsIlNldHRpbmdMZXZlbCIsIkFDQ09VTlQiLCJvbkRvbnRBc2tBZ2FpblNldCIsIm9uU2V0dXAiLCJzaG93U2V0dXBEaWFsb2ciLCJsb2FkaW5nIiwiZXJyb3IiLCJjb21wb25lbnREaWRNb3VudCIsIl9sb2FkQmFja3VwU3RhdHVzIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0S2V5QmFja3VwVmVyc2lvbiIsImUiLCJjb25zb2xlIiwibG9nIiwicmVuZGVyIiwiQWNjZXNzaWJsZUJ1dHRvbiIsInNldHVwQ2FwdGlvbiIsImEiLCJzdWIiLCJvblNldHVwQ2xpY2siLCJvbk9uTm90Tm93Q2xpY2siLCJvbkRvbnRBc2tBZ2FpbkNsaWNrIiwiUHJvcFR5cGVzIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXlCZSxNQUFNQSxvQkFBTixTQUFtQ0MsZUFBTUMsYUFBekMsQ0FBdUQ7QUFVbEVDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLDJEQStCRCxNQUFNO0FBQ3BCLFVBQUksS0FBS0MsS0FBTCxDQUFXQyxVQUFmLEVBQTJCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLGNBQU1DLHNCQUFzQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMENBQWpCLENBQS9COztBQUNBQyx1QkFBTUMsbUJBQU4sQ0FDSSxnQkFESixFQUNzQixFQUR0QixFQUMwQkosc0JBRDFCLEVBQ2tELElBRGxELEVBQ3dELElBRHhEO0FBRUk7QUFBaUIsYUFGckI7QUFFNEI7QUFBZSxZQUYzQztBQUlILE9BVEQsTUFTTztBQUNIRyx1QkFBTUUsd0JBQU4sQ0FBK0IsWUFBL0IsRUFBNkMsWUFBN0MsNkVBQ1cseUVBRFgsS0FFSSxJQUZKLEVBRVUsSUFGVjtBQUVnQjtBQUFpQixhQUZqQztBQUV3QztBQUFlLFlBRnZEO0FBSUg7QUFDSixLQS9Da0I7QUFBQSwyREFpREQsTUFBTTtBQUNwQixXQUFLQyxRQUFMLENBQWM7QUFBQ0MsUUFBQUEsYUFBYSxFQUFFO0FBQWhCLE9BQWQ7QUFDSCxLQW5Ea0I7QUFBQSwrREFxREcsTUFBTTtBQUN4QjtBQUNBO0FBQ0FKLHFCQUFNRSx3QkFBTixDQUErQiwwQkFBL0IsRUFBMkQsMEJBQTNELDZFQUNXLGdGQURYLEtBRUk7QUFDSUcsUUFBQUEsY0FBYyxFQUFFLFlBQVk7QUFDeEIsZ0JBQU1DLHVCQUFjQyxRQUFkLENBQ0YsMEJBREUsRUFFRixJQUZFLEVBR0ZDLDRCQUFhQyxPQUhYLEVBSUYsS0FKRSxDQUFOO0FBTUEsZUFBS2YsS0FBTCxDQUFXZ0IsaUJBQVg7QUFDSCxTQVRMO0FBVUlDLFFBQUFBLE9BQU8sRUFBRSxNQUFNO0FBQ1gsZUFBS0MsZUFBTDtBQUNIO0FBWkwsT0FGSjtBQWlCSCxLQXpFa0I7QUFBQSx3REEyRUosTUFBTTtBQUNqQixXQUFLQSxlQUFMO0FBQ0gsS0E3RWtCO0FBR2YsU0FBS2pCLEtBQUwsR0FBYTtBQUNUa0IsTUFBQUEsT0FBTyxFQUFFLElBREE7QUFFVEMsTUFBQUEsS0FBSyxFQUFFLElBRkU7QUFHVGxCLE1BQUFBLFVBQVUsRUFBRSxJQUhIO0FBSVRRLE1BQUFBLGFBQWEsRUFBRTtBQUpOLEtBQWI7QUFNSDs7QUFFRFcsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsU0FBS0MsaUJBQUw7QUFDSDs7QUFFRCxRQUFNQSxpQkFBTixHQUEwQjtBQUN0QixRQUFJO0FBQ0EsWUFBTXBCLFVBQVUsR0FBRyxNQUFNcUIsaUNBQWdCQyxHQUFoQixHQUFzQkMsbUJBQXRCLEVBQXpCO0FBQ0EsV0FBS2hCLFFBQUwsQ0FBYztBQUNWVSxRQUFBQSxPQUFPLEVBQUUsS0FEQztBQUVWakIsUUFBQUE7QUFGVSxPQUFkO0FBSUgsS0FORCxDQU1FLE9BQU93QixDQUFQLEVBQVU7QUFDUkMsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksbUNBQVosRUFBaURGLENBQWpEO0FBQ0EsV0FBS2pCLFFBQUwsQ0FBYztBQUNWVSxRQUFBQSxPQUFPLEVBQUUsS0FEQztBQUVWQyxRQUFBQSxLQUFLLEVBQUVNO0FBRkcsT0FBZDtBQUlIO0FBQ0o7O0FBa0RERyxFQUFBQSxNQUFNLEdBQUc7QUFDTDtBQUNBO0FBQ0EsUUFBSSxLQUFLNUIsS0FBTCxDQUFXbUIsS0FBWCxJQUFvQixLQUFLbkIsS0FBTCxDQUFXa0IsT0FBL0IsSUFBMEMsS0FBS2xCLEtBQUwsQ0FBV1MsYUFBekQsRUFBd0U7QUFDcEUsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTW9CLGdCQUFnQixHQUFHMUIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGlDQUFqQixDQUF6QjtBQUVBLFFBQUkwQixZQUFKOztBQUNBLFFBQUksS0FBSzlCLEtBQUwsQ0FBV0MsVUFBZixFQUEyQjtBQUN2QjZCLE1BQUFBLFlBQVksR0FBRyx5QkFBRyxvQ0FBSCxDQUFmO0FBQ0gsS0FGRCxNQUVPO0FBQ0hBLE1BQUFBLFlBQVksR0FBRyx5QkFBRyx3QkFBSCxDQUFmO0FBQ0g7O0FBRUQsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBaUQseUJBQzdDLCtCQUQ2QyxDQUFqRCxDQURKLEVBSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksd0NBQUkseUJBQ0EsdURBQ0EscURBREEsR0FFQSw4QkFIQSxDQUFKLENBREosRUFNSSx3Q0FBSSx5QkFDQSxzREFDQSxvQkFGQSxFQUVzQixFQUZ0QixFQUdBO0FBQ0k7QUFDQTtBQUNBQyxNQUFBQSxDQUFDLEVBQUVDLEdBQUcsSUFBSTtBQUhkLEtBSEEsQ0FBSixDQU5KLENBSkosRUFvQkk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsZ0NBQTVCO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFEbEIsT0FFS0gsWUFGTCxDQURKLEVBS0ksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsaURBQTVCO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBS0k7QUFEbEIsT0FFTSx5QkFBRyxTQUFILENBRk4sQ0FMSixFQVNJLDZCQUFDLGdCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLGlEQUE1QjtBQUNJLE1BQUEsT0FBTyxFQUFFLEtBQUtDO0FBRGxCLE9BRU0seUJBQUcsb0JBQUgsQ0FGTixDQVRKLENBcEJKLENBREo7QUFxQ0g7O0FBOUlpRTs7OzhCQUFqRHhDLG9CLGVBQ0U7QUFDZjtBQUNBb0IsRUFBQUEsaUJBQWlCLEVBQUVxQixtQkFBVUM7QUFGZCxDOzhCQURGMUMsb0Isa0JBTUs7QUFDbEJvQixFQUFBQSxpQkFBaUIsRUFBRSxZQUFXLENBQUU7QUFEZCxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSBcInByb3AtdHlwZXNcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gXCIuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi9Nb2RhbFwiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJvb21SZWNvdmVyeVJlbWluZGVyIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIGNhbGxlZCBpZiB0aGUgdXNlciBzZXRzIHRoZSBvcHRpb24gdG8gc3VwcHJlc3MgdGhpcyByZW1pbmRlciBpbiB0aGUgZnV0dXJlXHJcbiAgICAgICAgb25Eb250QXNrQWdhaW5TZXQ6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XHJcbiAgICAgICAgb25Eb250QXNrQWdhaW5TZXQ6IGZ1bmN0aW9uKCkge30sXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGxvYWRpbmc6IHRydWUsXHJcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBiYWNrdXBJbmZvOiBudWxsLFxyXG4gICAgICAgICAgICBub3ROb3dDbGlja2VkOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIHRoaXMuX2xvYWRCYWNrdXBTdGF0dXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfbG9hZEJhY2t1cFN0YXR1cygpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBiYWNrdXBJbmZvID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldEtleUJhY2t1cFZlcnNpb24oKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZmV0Y2gga2V5IGJhY2t1cCBzdGF0dXNcIiwgZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNob3dTZXR1cERpYWxvZyA9ICgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5iYWNrdXBJbmZvKSB7XHJcbiAgICAgICAgICAgIC8vIEEga2V5IGJhY2t1cCBleGlzdHMgZm9yIHRoaXMgYWNjb3VudCwgYnV0IHRoZSBjcmVhdGluZyBkZXZpY2UgaXMgbm90XHJcbiAgICAgICAgICAgIC8vIHZlcmlmaWVkLCBzbyByZXN0b3JlIHRoZSBiYWNrdXAgd2hpY2ggd2lsbCBnaXZlIHVzIHRoZSBrZXlzIGZyb20gaXQgYW5kXHJcbiAgICAgICAgICAgIC8vIGFsbG93IHVzIHRvIHRydXN0IGl0IChpZS4gdXBsb2FkIGtleXMgdG8gaXQpXHJcbiAgICAgICAgICAgIGNvbnN0IFJlc3RvcmVLZXlCYWNrdXBEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLmtleWJhY2t1cC5SZXN0b3JlS2V5QmFja3VwRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICAgICAnUmVzdG9yZSBCYWNrdXAnLCAnJywgUmVzdG9yZUtleUJhY2t1cERpYWxvZywgbnVsbCwgbnVsbCxcclxuICAgICAgICAgICAgICAgIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jKFwiS2V5IEJhY2t1cFwiLCBcIktleSBCYWNrdXBcIixcclxuICAgICAgICAgICAgICAgIGltcG9ydChcIi4uLy4uLy4uL2FzeW5jLWNvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9rZXliYWNrdXAvQ3JlYXRlS2V5QmFja3VwRGlhbG9nXCIpLFxyXG4gICAgICAgICAgICAgICAgbnVsbCwgbnVsbCwgLyogcHJpb3JpdHkgPSAqLyBmYWxzZSwgLyogc3RhdGljID0gKi8gdHJ1ZSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Pbk5vdE5vd0NsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe25vdE5vd0NsaWNrZWQ6IHRydWV9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRvbnRBc2tBZ2FpbkNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIC8vIFdoZW4geW91IGNob29zZSBcIkRvbid0IGFzayBhZ2FpblwiIGZyb20gdGhlIHJvb20gcmVtaW5kZXIsIHdlIHNob3cgYVxyXG4gICAgICAgIC8vIGRpYWxvZyB0byBjb25maXJtIHRoZSBjaG9pY2UuXHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jKFwiSWdub3JlIFJlY292ZXJ5IFJlbWluZGVyXCIsIFwiSWdub3JlIFJlY292ZXJ5IFJlbWluZGVyXCIsXHJcbiAgICAgICAgICAgIGltcG9ydChcIi4uLy4uLy4uL2FzeW5jLWNvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9rZXliYWNrdXAvSWdub3JlUmVjb3ZlcnlSZW1pbmRlckRpYWxvZ1wiKSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgb25Eb250QXNrQWdhaW46IGFzeW5jICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInNob3dSb29tUmVjb3ZlcnlSZW1pbmRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBTZXR0aW5nTGV2ZWwuQUNDT1VOVCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uRG9udEFza0FnYWluU2V0KCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgb25TZXR1cDogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd1NldHVwRGlhbG9nKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TZXR1cENsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2hvd1NldHVwRGlhbG9nKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIC8vIElmIHRoZXJlIHdhcyBhbiBlcnJvciBsb2FkaW5nIGp1c3QgZG9uJ3QgZGlzcGxheSB0aGUgYmFubmVyOiB3ZSdsbCB0cnkgYWdhaW5cclxuICAgICAgICAvLyBuZXh0IHRpbWUgdGhlIHVzZXIgc3dpdGNocyB0byB0aGUgcm9vbS5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvciB8fCB0aGlzLnN0YXRlLmxvYWRpbmcgfHwgdGhpcy5zdGF0ZS5ub3ROb3dDbGlja2VkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5lbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uXCIpO1xyXG5cclxuICAgICAgICBsZXQgc2V0dXBDYXB0aW9uO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmJhY2t1cEluZm8pIHtcclxuICAgICAgICAgICAgc2V0dXBDYXB0aW9uID0gX3QoXCJDb25uZWN0IHRoaXMgc2Vzc2lvbiB0byBLZXkgQmFja3VwXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNldHVwQ2FwdGlvbiA9IF90KFwiU3RhcnQgdXNpbmcgS2V5IEJhY2t1cFwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVJlY292ZXJ5UmVtaW5kZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVJlY292ZXJ5UmVtaW5kZXJfaGVhZGVyXCI+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiTmV2ZXIgbG9zZSBlbmNyeXB0ZWQgbWVzc2FnZXNcIixcclxuICAgICAgICAgICAgICAgICl9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21SZWNvdmVyeVJlbWluZGVyX2JvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiTWVzc2FnZXMgaW4gdGhpcyByb29tIGFyZSBzZWN1cmVkIHdpdGggZW5kLXRvLWVuZCBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZW5jcnlwdGlvbi4gT25seSB5b3UgYW5kIHRoZSByZWNpcGllbnQocykgaGF2ZSB0aGUgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcImtleXMgdG8gcmVhZCB0aGVzZSBtZXNzYWdlcy5cIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiU2VjdXJlbHkgYmFjayB1cCB5b3VyIGtleXMgdG8gYXZvaWQgbG9zaW5nIHRoZW0uIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8YT5MZWFybiBtb3JlLjwvYT5cIiwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IFdlIGRvbid0IGhhdmUgdGhpcyBsaW5rIHlldDogdGhpcyB3aWxsIHByZXZlbnQgdGhlIHRyYW5zbGF0b3JzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYXZpbmcgdG8gcmUtdHJhbnNsYXRlIHRoZSBzdHJpbmcgd2hlbiB3ZSBkby5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGE6IHN1YiA9PiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tUmVjb3ZlcnlSZW1pbmRlcl9idXR0b25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfUm9vbVJlY292ZXJ5UmVtaW5kZXJfYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vblNldHVwQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c2V0dXBDYXB0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Sb29tUmVjb3ZlcnlSZW1pbmRlcl9zZWNvbmRhcnkgbXhfbGlua0J1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25Pbk5vdE5vd0NsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIk5vdCBub3dcIikgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Sb29tUmVjb3ZlcnlSZW1pbmRlcl9zZWNvbmRhcnkgbXhfbGlua0J1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25Eb250QXNrQWdhaW5DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJEb24ndCBhc2sgbWUgYWdhaW5cIikgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==