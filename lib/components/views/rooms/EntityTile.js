"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _languageHandler = require("../../../languageHandler");

var _classnames = _interopRequireDefault(require("classnames"));

var _E2EIcon = _interopRequireDefault(require("./E2EIcon"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const PRESENCE_CLASS = {
  "offline": "mx_EntityTile_offline",
  "online": "mx_EntityTile_online",
  "unavailable": "mx_EntityTile_unavailable"
};

function presenceClassForMember(presenceState, lastActiveAgo, showPresence) {
  if (showPresence === false) {
    return 'mx_EntityTile_online_beenactive';
  } // offline is split into two categories depending on whether we have
  // a last_active_ago for them.


  if (presenceState == 'offline') {
    if (lastActiveAgo) {
      return PRESENCE_CLASS['offline'] + '_beenactive';
    } else {
      return PRESENCE_CLASS['offline'] + '_neveractive';
    }
  } else if (presenceState) {
    return PRESENCE_CLASS[presenceState];
  } else {
    return PRESENCE_CLASS['offline'] + '_neveractive';
  }
}

const EntityTile = (0, _createReactClass.default)({
  displayName: 'EntityTile',
  propTypes: {
    name: _propTypes.default.string,
    title: _propTypes.default.string,
    avatarJsx: _propTypes.default.any,
    // <BaseAvatar />
    className: _propTypes.default.string,
    presenceState: _propTypes.default.string,
    presenceLastActiveAgo: _propTypes.default.number,
    presenceLastTs: _propTypes.default.number,
    presenceCurrentlyActive: _propTypes.default.bool,
    showInviteButton: _propTypes.default.bool,
    shouldComponentUpdate: _propTypes.default.func,
    onClick: _propTypes.default.func,
    suppressOnHover: _propTypes.default.bool,
    showPresence: _propTypes.default.bool,
    subtextLabel: _propTypes.default.string,
    e2eStatus: _propTypes.default.string
  },
  getDefaultProps: function () {
    return {
      shouldComponentUpdate: function (nextProps, nextState) {
        return true;
      },
      onClick: function () {},
      presenceState: "offline",
      presenceLastActiveAgo: 0,
      presenceLastTs: 0,
      showInviteButton: false,
      suppressOnHover: false,
      showPresence: true
    };
  },
  getInitialState: function () {
    return {
      hover: false
    };
  },
  shouldComponentUpdate: function (nextProps, nextState) {
    if (this.state.hover !== nextState.hover) return true;
    return this.props.shouldComponentUpdate(nextProps, nextState);
  },
  render: function () {
    const mainClassNames = {
      "mx_EntityTile": true,
      "mx_EntityTile_noHover": this.props.suppressOnHover
    };
    if (this.props.className) mainClassNames[this.props.className] = true;
    const presenceClass = presenceClassForMember(this.props.presenceState, this.props.presenceLastActiveAgo, this.props.showPresence);
    mainClassNames[presenceClass] = true;
    let nameEl;
    const {
      name
    } = this.props;

    if (!this.props.suppressOnHover) {
      const activeAgo = this.props.presenceLastActiveAgo ? Date.now() - (this.props.presenceLastTs - this.props.presenceLastActiveAgo) : -1;
      const PresenceLabel = sdk.getComponent("rooms.PresenceLabel");
      let presenceLabel = null;

      if (this.props.showPresence) {
        presenceLabel = _react.default.createElement(PresenceLabel, {
          activeAgo: activeAgo,
          currentlyActive: this.props.presenceCurrentlyActive,
          presenceState: this.props.presenceState
        });
      }

      if (this.props.subtextLabel) {
        presenceLabel = _react.default.createElement("span", {
          className: "mx_EntityTile_subtext"
        }, this.props.subtextLabel);
      }

      nameEl = _react.default.createElement("div", {
        className: "mx_EntityTile_details"
      }, _react.default.createElement("div", {
        className: "mx_EntityTile_name",
        dir: "auto"
      }, name), presenceLabel);
    } else if (this.props.subtextLabel) {
      nameEl = _react.default.createElement("div", {
        className: "mx_EntityTile_details"
      }, _react.default.createElement("div", {
        className: "mx_EntityTile_name",
        dir: "auto"
      }, name), _react.default.createElement("span", {
        className: "mx_EntityTile_subtext"
      }, this.props.subtextLabel));
    } else {
      nameEl = _react.default.createElement("div", {
        className: "mx_EntityTile_name",
        dir: "auto"
      }, name);
    }

    let inviteButton;

    if (this.props.showInviteButton) {
      inviteButton = _react.default.createElement("div", {
        className: "mx_EntityTile_invite"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/plus.svg"),
        width: "16",
        height: "16"
      }));
    }

    let powerLabel;
    const powerStatus = this.props.powerStatus;

    if (powerStatus) {
      const powerText = {
        [EntityTile.POWER_STATUS_MODERATOR]: (0, _languageHandler._t)("Mod"),
        [EntityTile.POWER_STATUS_ADMIN]: (0, _languageHandler._t)("Admin")
      }[powerStatus];
      powerLabel = _react.default.createElement("div", {
        className: "mx_EntityTile_power"
      }, powerText);
    }

    let e2eIcon;
    const {
      e2eStatus
    } = this.props;

    if (e2eStatus) {
      e2eIcon = _react.default.createElement(_E2EIcon.default, {
        status: e2eStatus,
        isUser: true
      });
    }

    const BaseAvatar = sdk.getComponent('avatars.BaseAvatar');

    const av = this.props.avatarJsx || _react.default.createElement(BaseAvatar, {
      name: this.props.name,
      width: 36,
      height: 36,
      "aria-hidden": "true"
    }); // The wrapping div is required to make the magic mouse listener work, for some reason.


    return _react.default.createElement("div", {
      ref: c => this.container = c
    }, _react.default.createElement(_AccessibleButton.default, {
      className: (0, _classnames.default)(mainClassNames),
      title: this.props.title,
      onClick: this.props.onClick
    }, _react.default.createElement("div", {
      className: "mx_EntityTile_avatar"
    }, av, e2eIcon), nameEl, powerLabel, inviteButton));
  }
});
EntityTile.POWER_STATUS_MODERATOR = "moderator";
EntityTile.POWER_STATUS_ADMIN = "admin";
var _default = EntityTile;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL0VudGl0eVRpbGUuanMiXSwibmFtZXMiOlsiUFJFU0VOQ0VfQ0xBU1MiLCJwcmVzZW5jZUNsYXNzRm9yTWVtYmVyIiwicHJlc2VuY2VTdGF0ZSIsImxhc3RBY3RpdmVBZ28iLCJzaG93UHJlc2VuY2UiLCJFbnRpdHlUaWxlIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJuYW1lIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwidGl0bGUiLCJhdmF0YXJKc3giLCJhbnkiLCJjbGFzc05hbWUiLCJwcmVzZW5jZUxhc3RBY3RpdmVBZ28iLCJudW1iZXIiLCJwcmVzZW5jZUxhc3RUcyIsInByZXNlbmNlQ3VycmVudGx5QWN0aXZlIiwiYm9vbCIsInNob3dJbnZpdGVCdXR0b24iLCJzaG91bGRDb21wb25lbnRVcGRhdGUiLCJmdW5jIiwib25DbGljayIsInN1cHByZXNzT25Ib3ZlciIsInN1YnRleHRMYWJlbCIsImUyZVN0YXR1cyIsImdldERlZmF1bHRQcm9wcyIsIm5leHRQcm9wcyIsIm5leHRTdGF0ZSIsImdldEluaXRpYWxTdGF0ZSIsImhvdmVyIiwic3RhdGUiLCJwcm9wcyIsInJlbmRlciIsIm1haW5DbGFzc05hbWVzIiwicHJlc2VuY2VDbGFzcyIsIm5hbWVFbCIsImFjdGl2ZUFnbyIsIkRhdGUiLCJub3ciLCJQcmVzZW5jZUxhYmVsIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwicHJlc2VuY2VMYWJlbCIsImludml0ZUJ1dHRvbiIsInJlcXVpcmUiLCJwb3dlckxhYmVsIiwicG93ZXJTdGF0dXMiLCJwb3dlclRleHQiLCJQT1dFUl9TVEFUVVNfTU9ERVJBVE9SIiwiUE9XRVJfU1RBVFVTX0FETUlOIiwiZTJlSWNvbiIsIkJhc2VBdmF0YXIiLCJhdiIsImMiLCJjb250YWluZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXpCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkEsTUFBTUEsY0FBYyxHQUFHO0FBQ25CLGFBQVcsdUJBRFE7QUFFbkIsWUFBVSxzQkFGUztBQUduQixpQkFBZTtBQUhJLENBQXZCOztBQU1BLFNBQVNDLHNCQUFULENBQWdDQyxhQUFoQyxFQUErQ0MsYUFBL0MsRUFBOERDLFlBQTlELEVBQTRFO0FBQ3hFLE1BQUlBLFlBQVksS0FBSyxLQUFyQixFQUE0QjtBQUN4QixXQUFPLGlDQUFQO0FBQ0gsR0FIdUUsQ0FLeEU7QUFDQTs7O0FBQ0EsTUFBSUYsYUFBYSxJQUFJLFNBQXJCLEVBQWdDO0FBQzVCLFFBQUlDLGFBQUosRUFBbUI7QUFDZixhQUFPSCxjQUFjLENBQUMsU0FBRCxDQUFkLEdBQTRCLGFBQW5DO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsYUFBT0EsY0FBYyxDQUFDLFNBQUQsQ0FBZCxHQUE0QixjQUFuQztBQUNIO0FBQ0osR0FORCxNQU1PLElBQUlFLGFBQUosRUFBbUI7QUFDdEIsV0FBT0YsY0FBYyxDQUFDRSxhQUFELENBQXJCO0FBQ0gsR0FGTSxNQUVBO0FBQ0gsV0FBT0YsY0FBYyxDQUFDLFNBQUQsQ0FBZCxHQUE0QixjQUFuQztBQUNIO0FBQ0o7O0FBRUQsTUFBTUssVUFBVSxHQUFHLCtCQUFpQjtBQUNoQ0MsRUFBQUEsV0FBVyxFQUFFLFlBRG1CO0FBR2hDQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsSUFBSSxFQUFFQyxtQkFBVUMsTUFEVDtBQUVQQyxJQUFBQSxLQUFLLEVBQUVGLG1CQUFVQyxNQUZWO0FBR1BFLElBQUFBLFNBQVMsRUFBRUgsbUJBQVVJLEdBSGQ7QUFHbUI7QUFDMUJDLElBQUFBLFNBQVMsRUFBRUwsbUJBQVVDLE1BSmQ7QUFLUFIsSUFBQUEsYUFBYSxFQUFFTyxtQkFBVUMsTUFMbEI7QUFNUEssSUFBQUEscUJBQXFCLEVBQUVOLG1CQUFVTyxNQU4xQjtBQU9QQyxJQUFBQSxjQUFjLEVBQUVSLG1CQUFVTyxNQVBuQjtBQVFQRSxJQUFBQSx1QkFBdUIsRUFBRVQsbUJBQVVVLElBUjVCO0FBU1BDLElBQUFBLGdCQUFnQixFQUFFWCxtQkFBVVUsSUFUckI7QUFVUEUsSUFBQUEscUJBQXFCLEVBQUVaLG1CQUFVYSxJQVYxQjtBQVdQQyxJQUFBQSxPQUFPLEVBQUVkLG1CQUFVYSxJQVhaO0FBWVBFLElBQUFBLGVBQWUsRUFBRWYsbUJBQVVVLElBWnBCO0FBYVBmLElBQUFBLFlBQVksRUFBRUssbUJBQVVVLElBYmpCO0FBY1BNLElBQUFBLFlBQVksRUFBRWhCLG1CQUFVQyxNQWRqQjtBQWVQZ0IsSUFBQUEsU0FBUyxFQUFFakIsbUJBQVVDO0FBZmQsR0FIcUI7QUFxQmhDaUIsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNITixNQUFBQSxxQkFBcUIsRUFBRSxVQUFTTyxTQUFULEVBQW9CQyxTQUFwQixFQUErQjtBQUFFLGVBQU8sSUFBUDtBQUFjLE9BRG5FO0FBRUhOLE1BQUFBLE9BQU8sRUFBRSxZQUFXLENBQUUsQ0FGbkI7QUFHSHJCLE1BQUFBLGFBQWEsRUFBRSxTQUhaO0FBSUhhLE1BQUFBLHFCQUFxQixFQUFFLENBSnBCO0FBS0hFLE1BQUFBLGNBQWMsRUFBRSxDQUxiO0FBTUhHLE1BQUFBLGdCQUFnQixFQUFFLEtBTmY7QUFPSEksTUFBQUEsZUFBZSxFQUFFLEtBUGQ7QUFRSHBCLE1BQUFBLFlBQVksRUFBRTtBQVJYLEtBQVA7QUFVSCxHQWhDK0I7QUFrQ2hDMEIsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxLQUFLLEVBQUU7QUFESixLQUFQO0FBR0gsR0F0QytCO0FBd0NoQ1YsRUFBQUEscUJBQXFCLEVBQUUsVUFBU08sU0FBVCxFQUFvQkMsU0FBcEIsRUFBK0I7QUFDbEQsUUFBSSxLQUFLRyxLQUFMLENBQVdELEtBQVgsS0FBcUJGLFNBQVMsQ0FBQ0UsS0FBbkMsRUFBMEMsT0FBTyxJQUFQO0FBQzFDLFdBQU8sS0FBS0UsS0FBTCxDQUFXWixxQkFBWCxDQUFpQ08sU0FBakMsRUFBNENDLFNBQTVDLENBQVA7QUFDSCxHQTNDK0I7QUE2Q2hDSyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLGNBQWMsR0FBRztBQUNuQix1QkFBaUIsSUFERTtBQUVuQiwrQkFBeUIsS0FBS0YsS0FBTCxDQUFXVDtBQUZqQixLQUF2QjtBQUlBLFFBQUksS0FBS1MsS0FBTCxDQUFXbkIsU0FBZixFQUEwQnFCLGNBQWMsQ0FBQyxLQUFLRixLQUFMLENBQVduQixTQUFaLENBQWQsR0FBdUMsSUFBdkM7QUFFMUIsVUFBTXNCLGFBQWEsR0FBR25DLHNCQUFzQixDQUN4QyxLQUFLZ0MsS0FBTCxDQUFXL0IsYUFENkIsRUFDZCxLQUFLK0IsS0FBTCxDQUFXbEIscUJBREcsRUFDb0IsS0FBS2tCLEtBQUwsQ0FBVzdCLFlBRC9CLENBQTVDO0FBR0ErQixJQUFBQSxjQUFjLENBQUNDLGFBQUQsQ0FBZCxHQUFnQyxJQUFoQztBQUVBLFFBQUlDLE1BQUo7QUFDQSxVQUFNO0FBQUM3QixNQUFBQTtBQUFELFFBQVMsS0FBS3lCLEtBQXBCOztBQUVBLFFBQUksQ0FBQyxLQUFLQSxLQUFMLENBQVdULGVBQWhCLEVBQWlDO0FBQzdCLFlBQU1jLFNBQVMsR0FBRyxLQUFLTCxLQUFMLENBQVdsQixxQkFBWCxHQUNid0IsSUFBSSxDQUFDQyxHQUFMLE1BQWMsS0FBS1AsS0FBTCxDQUFXaEIsY0FBWCxHQUE0QixLQUFLZ0IsS0FBTCxDQUFXbEIscUJBQXJELENBRGEsR0FDa0UsQ0FBQyxDQURyRjtBQUdBLFlBQU0wQixhQUFhLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBdEI7QUFDQSxVQUFJQyxhQUFhLEdBQUcsSUFBcEI7O0FBQ0EsVUFBSSxLQUFLWCxLQUFMLENBQVc3QixZQUFmLEVBQTZCO0FBQ3pCd0MsUUFBQUEsYUFBYSxHQUFHLDZCQUFDLGFBQUQ7QUFBZSxVQUFBLFNBQVMsRUFBRU4sU0FBMUI7QUFDWixVQUFBLGVBQWUsRUFBRSxLQUFLTCxLQUFMLENBQVdmLHVCQURoQjtBQUVaLFVBQUEsYUFBYSxFQUFFLEtBQUtlLEtBQUwsQ0FBVy9CO0FBRmQsVUFBaEI7QUFHSDs7QUFDRCxVQUFJLEtBQUsrQixLQUFMLENBQVdSLFlBQWYsRUFBNkI7QUFDekJtQixRQUFBQSxhQUFhLEdBQUc7QUFBTSxVQUFBLFNBQVMsRUFBQztBQUFoQixXQUF5QyxLQUFLWCxLQUFMLENBQVdSLFlBQXBELENBQWhCO0FBQ0g7O0FBQ0RZLE1BQUFBLE1BQU0sR0FDRjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSTtBQUFLLFFBQUEsU0FBUyxFQUFDLG9CQUFmO0FBQW9DLFFBQUEsR0FBRyxFQUFDO0FBQXhDLFNBQ003QixJQUROLENBREosRUFJS29DLGFBSkwsQ0FESjtBQVFILEtBdEJELE1Bc0JPLElBQUksS0FBS1gsS0FBTCxDQUFXUixZQUFmLEVBQTZCO0FBQ2hDWSxNQUFBQSxNQUFNLEdBQ0Y7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQyxvQkFBZjtBQUFvQyxRQUFBLEdBQUcsRUFBQztBQUF4QyxTQUNLN0IsSUFETCxDQURKLEVBSUk7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUF5QyxLQUFLeUIsS0FBTCxDQUFXUixZQUFwRCxDQUpKLENBREo7QUFRSCxLQVRNLE1BU0E7QUFDSFksTUFBQUEsTUFBTSxHQUNGO0FBQUssUUFBQSxTQUFTLEVBQUMsb0JBQWY7QUFBb0MsUUFBQSxHQUFHLEVBQUM7QUFBeEMsU0FBaUQ3QixJQUFqRCxDQURKO0FBR0g7O0FBRUQsUUFBSXFDLFlBQUo7O0FBQ0EsUUFBSSxLQUFLWixLQUFMLENBQVdiLGdCQUFmLEVBQWlDO0FBQzdCeUIsTUFBQUEsWUFBWSxHQUNSO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNJO0FBQUssUUFBQSxHQUFHLEVBQUVDLE9BQU8sQ0FBQyw4QkFBRCxDQUFqQjtBQUFtRCxRQUFBLEtBQUssRUFBQyxJQUF6RDtBQUE4RCxRQUFBLE1BQU0sRUFBQztBQUFyRSxRQURKLENBREo7QUFLSDs7QUFFRCxRQUFJQyxVQUFKO0FBQ0EsVUFBTUMsV0FBVyxHQUFHLEtBQUtmLEtBQUwsQ0FBV2UsV0FBL0I7O0FBQ0EsUUFBSUEsV0FBSixFQUFpQjtBQUNiLFlBQU1DLFNBQVMsR0FBRztBQUNkLFNBQUM1QyxVQUFVLENBQUM2QyxzQkFBWixHQUFxQyx5QkFBRyxLQUFILENBRHZCO0FBRWQsU0FBQzdDLFVBQVUsQ0FBQzhDLGtCQUFaLEdBQWlDLHlCQUFHLE9BQUg7QUFGbkIsUUFHaEJILFdBSGdCLENBQWxCO0FBSUFELE1BQUFBLFVBQVUsR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBc0NFLFNBQXRDLENBQWI7QUFDSDs7QUFFRCxRQUFJRyxPQUFKO0FBQ0EsVUFBTTtBQUFFMUIsTUFBQUE7QUFBRixRQUFnQixLQUFLTyxLQUEzQjs7QUFDQSxRQUFJUCxTQUFKLEVBQWU7QUFDWDBCLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxnQkFBRDtBQUFTLFFBQUEsTUFBTSxFQUFFMUIsU0FBakI7QUFBNEIsUUFBQSxNQUFNLEVBQUU7QUFBcEMsUUFBVjtBQUNIOztBQUVELFVBQU0yQixVQUFVLEdBQUdYLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBRUEsVUFBTVcsRUFBRSxHQUFHLEtBQUtyQixLQUFMLENBQVdyQixTQUFYLElBQ1AsNkJBQUMsVUFBRDtBQUFZLE1BQUEsSUFBSSxFQUFFLEtBQUtxQixLQUFMLENBQVd6QixJQUE3QjtBQUFtQyxNQUFBLEtBQUssRUFBRSxFQUExQztBQUE4QyxNQUFBLE1BQU0sRUFBRSxFQUF0RDtBQUEwRCxxQkFBWTtBQUF0RSxNQURKLENBL0VlLENBa0ZmOzs7QUFDQSxXQUNJO0FBQUssTUFBQSxHQUFHLEVBQUcrQyxDQUFELElBQU8sS0FBS0MsU0FBTCxHQUFpQkQ7QUFBbEMsT0FDSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBRSx5QkFBV3BCLGNBQVgsQ0FBN0I7QUFBeUQsTUFBQSxLQUFLLEVBQUUsS0FBS0YsS0FBTCxDQUFXdEIsS0FBM0U7QUFDa0IsTUFBQSxPQUFPLEVBQUUsS0FBS3NCLEtBQUwsQ0FBV1Y7QUFEdEMsT0FFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTStCLEVBRE4sRUFFTUYsT0FGTixDQUZKLEVBTU1mLE1BTk4sRUFPTVUsVUFQTixFQVFNRixZQVJOLENBREosQ0FESjtBQWNIO0FBOUkrQixDQUFqQixDQUFuQjtBQWlKQXhDLFVBQVUsQ0FBQzZDLHNCQUFYLEdBQW9DLFdBQXBDO0FBQ0E3QyxVQUFVLENBQUM4QyxrQkFBWCxHQUFnQyxPQUFoQztlQUVlOUMsVSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSAnLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSBcImNsYXNzbmFtZXNcIjtcclxuaW1wb3J0IEUyRUljb24gZnJvbSAnLi9FMkVJY29uJztcclxuXHJcbmNvbnN0IFBSRVNFTkNFX0NMQVNTID0ge1xyXG4gICAgXCJvZmZsaW5lXCI6IFwibXhfRW50aXR5VGlsZV9vZmZsaW5lXCIsXHJcbiAgICBcIm9ubGluZVwiOiBcIm14X0VudGl0eVRpbGVfb25saW5lXCIsXHJcbiAgICBcInVuYXZhaWxhYmxlXCI6IFwibXhfRW50aXR5VGlsZV91bmF2YWlsYWJsZVwiLFxyXG59O1xyXG5cclxuZnVuY3Rpb24gcHJlc2VuY2VDbGFzc0Zvck1lbWJlcihwcmVzZW5jZVN0YXRlLCBsYXN0QWN0aXZlQWdvLCBzaG93UHJlc2VuY2UpIHtcclxuICAgIGlmIChzaG93UHJlc2VuY2UgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgcmV0dXJuICdteF9FbnRpdHlUaWxlX29ubGluZV9iZWVuYWN0aXZlJztcclxuICAgIH1cclxuXHJcbiAgICAvLyBvZmZsaW5lIGlzIHNwbGl0IGludG8gdHdvIGNhdGVnb3JpZXMgZGVwZW5kaW5nIG9uIHdoZXRoZXIgd2UgaGF2ZVxyXG4gICAgLy8gYSBsYXN0X2FjdGl2ZV9hZ28gZm9yIHRoZW0uXHJcbiAgICBpZiAocHJlc2VuY2VTdGF0ZSA9PSAnb2ZmbGluZScpIHtcclxuICAgICAgICBpZiAobGFzdEFjdGl2ZUFnbykge1xyXG4gICAgICAgICAgICByZXR1cm4gUFJFU0VOQ0VfQ0xBU1NbJ29mZmxpbmUnXSArICdfYmVlbmFjdGl2ZSc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIFBSRVNFTkNFX0NMQVNTWydvZmZsaW5lJ10gKyAnX25ldmVyYWN0aXZlJztcclxuICAgICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHByZXNlbmNlU3RhdGUpIHtcclxuICAgICAgICByZXR1cm4gUFJFU0VOQ0VfQ0xBU1NbcHJlc2VuY2VTdGF0ZV07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBQUkVTRU5DRV9DTEFTU1snb2ZmbGluZSddICsgJ19uZXZlcmFjdGl2ZSc7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IEVudGl0eVRpbGUgPSBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnRW50aXR5VGlsZScsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgbmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgICAgICBhdmF0YXJKc3g6IFByb3BUeXBlcy5hbnksIC8vIDxCYXNlQXZhdGFyIC8+XHJcbiAgICAgICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgICAgIHByZXNlbmNlU3RhdGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgcHJlc2VuY2VMYXN0QWN0aXZlQWdvOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIHByZXNlbmNlTGFzdFRzOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgICAgIHByZXNlbmNlQ3VycmVudGx5QWN0aXZlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBzaG93SW52aXRlQnV0dG9uOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIHN1cHByZXNzT25Ib3ZlcjogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgc2hvd1ByZXNlbmNlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBzdWJ0ZXh0TGFiZWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZTJlU3RhdHVzOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHNob3VsZENvbXBvbmVudFVwZGF0ZTogZnVuY3Rpb24obmV4dFByb3BzLCBuZXh0U3RhdGUpIHsgcmV0dXJuIHRydWU7IH0sXHJcbiAgICAgICAgICAgIG9uQ2xpY2s6IGZ1bmN0aW9uKCkge30sXHJcbiAgICAgICAgICAgIHByZXNlbmNlU3RhdGU6IFwib2ZmbGluZVwiLFxyXG4gICAgICAgICAgICBwcmVzZW5jZUxhc3RBY3RpdmVBZ286IDAsXHJcbiAgICAgICAgICAgIHByZXNlbmNlTGFzdFRzOiAwLFxyXG4gICAgICAgICAgICBzaG93SW52aXRlQnV0dG9uOiBmYWxzZSxcclxuICAgICAgICAgICAgc3VwcHJlc3NPbkhvdmVyOiBmYWxzZSxcclxuICAgICAgICAgICAgc2hvd1ByZXNlbmNlOiB0cnVlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaG92ZXI6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3VsZENvbXBvbmVudFVwZGF0ZTogZnVuY3Rpb24obmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5ob3ZlciAhPT0gbmV4dFN0YXRlLmhvdmVyKSByZXR1cm4gdHJ1ZTtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5zaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUpO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IG1haW5DbGFzc05hbWVzID0ge1xyXG4gICAgICAgICAgICBcIm14X0VudGl0eVRpbGVcIjogdHJ1ZSxcclxuICAgICAgICAgICAgXCJteF9FbnRpdHlUaWxlX25vSG92ZXJcIjogdGhpcy5wcm9wcy5zdXBwcmVzc09uSG92ZXIsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jbGFzc05hbWUpIG1haW5DbGFzc05hbWVzW3RoaXMucHJvcHMuY2xhc3NOYW1lXSA9IHRydWU7XHJcblxyXG4gICAgICAgIGNvbnN0IHByZXNlbmNlQ2xhc3MgPSBwcmVzZW5jZUNsYXNzRm9yTWVtYmVyKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLnByZXNlbmNlU3RhdGUsIHRoaXMucHJvcHMucHJlc2VuY2VMYXN0QWN0aXZlQWdvLCB0aGlzLnByb3BzLnNob3dQcmVzZW5jZSxcclxuICAgICAgICApO1xyXG4gICAgICAgIG1haW5DbGFzc05hbWVzW3ByZXNlbmNlQ2xhc3NdID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgbGV0IG5hbWVFbDtcclxuICAgICAgICBjb25zdCB7bmFtZX0gPSB0aGlzLnByb3BzO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMuc3VwcHJlc3NPbkhvdmVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdGl2ZUFnbyA9IHRoaXMucHJvcHMucHJlc2VuY2VMYXN0QWN0aXZlQWdvID9cclxuICAgICAgICAgICAgICAgIChEYXRlLm5vdygpIC0gKHRoaXMucHJvcHMucHJlc2VuY2VMYXN0VHMgLSB0aGlzLnByb3BzLnByZXNlbmNlTGFzdEFjdGl2ZUFnbykpIDogLTE7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBQcmVzZW5jZUxhYmVsID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLlByZXNlbmNlTGFiZWxcIik7XHJcbiAgICAgICAgICAgIGxldCBwcmVzZW5jZUxhYmVsID0gbnVsbDtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuc2hvd1ByZXNlbmNlKSB7XHJcbiAgICAgICAgICAgICAgICBwcmVzZW5jZUxhYmVsID0gPFByZXNlbmNlTGFiZWwgYWN0aXZlQWdvPXthY3RpdmVBZ299XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudGx5QWN0aXZlPXt0aGlzLnByb3BzLnByZXNlbmNlQ3VycmVudGx5QWN0aXZlfVxyXG4gICAgICAgICAgICAgICAgICAgIHByZXNlbmNlU3RhdGU9e3RoaXMucHJvcHMucHJlc2VuY2VTdGF0ZX0gLz47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuc3VidGV4dExhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICBwcmVzZW5jZUxhYmVsID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfRW50aXR5VGlsZV9zdWJ0ZXh0XCI+e3RoaXMucHJvcHMuc3VidGV4dExhYmVsfTwvc3Bhbj47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmFtZUVsID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbnRpdHlUaWxlX2RldGFpbHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0VudGl0eVRpbGVfbmFtZVwiIGRpcj1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBuYW1lIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICB7cHJlc2VuY2VMYWJlbH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5zdWJ0ZXh0TGFiZWwpIHtcclxuICAgICAgICAgICAgbmFtZUVsID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbnRpdHlUaWxlX2RldGFpbHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0VudGl0eVRpbGVfbmFtZVwiIGRpcj1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge25hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfRW50aXR5VGlsZV9zdWJ0ZXh0XCI+e3RoaXMucHJvcHMuc3VidGV4dExhYmVsfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5hbWVFbCA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRW50aXR5VGlsZV9uYW1lXCIgZGlyPVwiYXV0b1wiPnsgbmFtZSB9PC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaW52aXRlQnV0dG9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNob3dJbnZpdGVCdXR0b24pIHtcclxuICAgICAgICAgICAgaW52aXRlQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbnRpdHlUaWxlX2ludml0ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9wbHVzLnN2Z1wiKX0gd2lkdGg9XCIxNlwiIGhlaWdodD1cIjE2XCIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHBvd2VyTGFiZWw7XHJcbiAgICAgICAgY29uc3QgcG93ZXJTdGF0dXMgPSB0aGlzLnByb3BzLnBvd2VyU3RhdHVzO1xyXG4gICAgICAgIGlmIChwb3dlclN0YXR1cykge1xyXG4gICAgICAgICAgICBjb25zdCBwb3dlclRleHQgPSB7XHJcbiAgICAgICAgICAgICAgICBbRW50aXR5VGlsZS5QT1dFUl9TVEFUVVNfTU9ERVJBVE9SXTogX3QoXCJNb2RcIiksXHJcbiAgICAgICAgICAgICAgICBbRW50aXR5VGlsZS5QT1dFUl9TVEFUVVNfQURNSU5dOiBfdChcIkFkbWluXCIpLFxyXG4gICAgICAgICAgICB9W3Bvd2VyU3RhdHVzXTtcclxuICAgICAgICAgICAgcG93ZXJMYWJlbCA9IDxkaXYgY2xhc3NOYW1lPVwibXhfRW50aXR5VGlsZV9wb3dlclwiPntwb3dlclRleHR9PC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGUyZUljb247XHJcbiAgICAgICAgY29uc3QgeyBlMmVTdGF0dXMgfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgaWYgKGUyZVN0YXR1cykge1xyXG4gICAgICAgICAgICBlMmVJY29uID0gPEUyRUljb24gc3RhdHVzPXtlMmVTdGF0dXN9IGlzVXNlcj17dHJ1ZX0gLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudCgnYXZhdGFycy5CYXNlQXZhdGFyJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IGF2ID0gdGhpcy5wcm9wcy5hdmF0YXJKc3ggfHxcclxuICAgICAgICAgICAgPEJhc2VBdmF0YXIgbmFtZT17dGhpcy5wcm9wcy5uYW1lfSB3aWR0aD17MzZ9IGhlaWdodD17MzZ9IGFyaWEtaGlkZGVuPVwidHJ1ZVwiIC8+O1xyXG5cclxuICAgICAgICAvLyBUaGUgd3JhcHBpbmcgZGl2IGlzIHJlcXVpcmVkIHRvIG1ha2UgdGhlIG1hZ2ljIG1vdXNlIGxpc3RlbmVyIHdvcmssIGZvciBzb21lIHJlYXNvbi5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IHJlZj17KGMpID0+IHRoaXMuY29udGFpbmVyID0gY30gPlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPXtjbGFzc05hbWVzKG1haW5DbGFzc05hbWVzKX0gdGl0bGU9e3RoaXMucHJvcHMudGl0bGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLnByb3BzLm9uQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRW50aXR5VGlsZV9hdmF0YXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBhdiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgZTJlSWNvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBuYW1lRWwgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgcG93ZXJMYWJlbCB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBpbnZpdGVCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcblxyXG5FbnRpdHlUaWxlLlBPV0VSX1NUQVRVU19NT0RFUkFUT1IgPSBcIm1vZGVyYXRvclwiO1xyXG5FbnRpdHlUaWxlLlBPV0VSX1NUQVRVU19BRE1JTiA9IFwiYWRtaW5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEVudGl0eVRpbGU7XHJcbiJdfQ==