"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'PresenceLabel',
  propTypes: {
    // number of milliseconds ago this user was last active.
    // zero = unknown
    activeAgo: _propTypes.default.number,
    // if true, activeAgo is an approximation and "Now" should
    // be shown instead
    currentlyActive: _propTypes.default.bool,
    // offline, online, etc
    presenceState: _propTypes.default.string
  },
  getDefaultProps: function () {
    return {
      ago: -1,
      presenceState: null
    };
  },
  // Return duration as a string using appropriate time units
  // XXX: This would be better handled using a culture-aware library, but we don't use one yet.
  getDuration: function (time) {
    if (!time) return;
    const t = parseInt(time / 1000);
    const s = t % 60;
    const m = parseInt(t / 60) % 60;
    const h = parseInt(t / (60 * 60)) % 24;
    const d = parseInt(t / (60 * 60 * 24));

    if (t < 60) {
      if (t < 0) {
        return (0, _languageHandler._t)("%(duration)ss", {
          duration: 0
        });
      }

      return (0, _languageHandler._t)("%(duration)ss", {
        duration: s
      });
    }

    if (t < 60 * 60) {
      return (0, _languageHandler._t)("%(duration)sm", {
        duration: m
      });
    }

    if (t < 24 * 60 * 60) {
      return (0, _languageHandler._t)("%(duration)sh", {
        duration: h
      });
    }

    return (0, _languageHandler._t)("%(duration)sd", {
      duration: d
    });
  },
  getPrettyPresence: function (presence, activeAgo, currentlyActive) {
    if (!currentlyActive && activeAgo !== undefined && activeAgo > 0) {
      const duration = this.getDuration(activeAgo);
      if (presence === "online") return (0, _languageHandler._t)("Online for %(duration)s", {
        duration: duration
      });
      if (presence === "unavailable") return (0, _languageHandler._t)("Idle for %(duration)s", {
        duration: duration
      }); // XXX: is this actually right?

      if (presence === "offline") return (0, _languageHandler._t)("Offline for %(duration)s", {
        duration: duration
      });
      return (0, _languageHandler._t)("Unknown for %(duration)s", {
        duration: duration
      });
    } else {
      if (presence === "online") return (0, _languageHandler._t)("Online");
      if (presence === "unavailable") return (0, _languageHandler._t)("Idle"); // XXX: is this actually right?

      if (presence === "offline") return (0, _languageHandler._t)("Offline");
      return (0, _languageHandler._t)("Unknown");
    }
  },
  render: function () {
    return _react.default.createElement("div", {
      className: "mx_PresenceLabel"
    }, this.getPrettyPresence(this.props.presenceState, this.props.activeAgo, this.props.currentlyActive));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1ByZXNlbmNlTGFiZWwuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJhY3RpdmVBZ28iLCJQcm9wVHlwZXMiLCJudW1iZXIiLCJjdXJyZW50bHlBY3RpdmUiLCJib29sIiwicHJlc2VuY2VTdGF0ZSIsInN0cmluZyIsImdldERlZmF1bHRQcm9wcyIsImFnbyIsImdldER1cmF0aW9uIiwidGltZSIsInQiLCJwYXJzZUludCIsInMiLCJtIiwiaCIsImQiLCJkdXJhdGlvbiIsImdldFByZXR0eVByZXNlbmNlIiwicHJlc2VuY2UiLCJ1bmRlZmluZWQiLCJyZW5kZXIiLCJwcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUVBOztBQXBCQTs7Ozs7Ozs7Ozs7Ozs7O2VBdUJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLGVBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0E7QUFDQUMsSUFBQUEsU0FBUyxFQUFFQyxtQkFBVUMsTUFIZDtBQUtQO0FBQ0E7QUFDQUMsSUFBQUEsZUFBZSxFQUFFRixtQkFBVUcsSUFQcEI7QUFTUDtBQUNBQyxJQUFBQSxhQUFhLEVBQUVKLG1CQUFVSztBQVZsQixHQUhpQjtBQWdCNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSEMsTUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FESDtBQUVISCxNQUFBQSxhQUFhLEVBQUU7QUFGWixLQUFQO0FBSUgsR0FyQjJCO0FBdUI1QjtBQUNBO0FBQ0FJLEVBQUFBLFdBQVcsRUFBRSxVQUFTQyxJQUFULEVBQWU7QUFDeEIsUUFBSSxDQUFDQSxJQUFMLEVBQVc7QUFDWCxVQUFNQyxDQUFDLEdBQUdDLFFBQVEsQ0FBQ0YsSUFBSSxHQUFHLElBQVIsQ0FBbEI7QUFDQSxVQUFNRyxDQUFDLEdBQUdGLENBQUMsR0FBRyxFQUFkO0FBQ0EsVUFBTUcsQ0FBQyxHQUFHRixRQUFRLENBQUNELENBQUMsR0FBRyxFQUFMLENBQVIsR0FBbUIsRUFBN0I7QUFDQSxVQUFNSSxDQUFDLEdBQUdILFFBQVEsQ0FBQ0QsQ0FBQyxJQUFJLEtBQUssRUFBVCxDQUFGLENBQVIsR0FBMEIsRUFBcEM7QUFDQSxVQUFNSyxDQUFDLEdBQUdKLFFBQVEsQ0FBQ0QsQ0FBQyxJQUFJLEtBQUssRUFBTCxHQUFVLEVBQWQsQ0FBRixDQUFsQjs7QUFDQSxRQUFJQSxDQUFDLEdBQUcsRUFBUixFQUFZO0FBQ1IsVUFBSUEsQ0FBQyxHQUFHLENBQVIsRUFBVztBQUNQLGVBQU8seUJBQUcsZUFBSCxFQUFvQjtBQUFDTSxVQUFBQSxRQUFRLEVBQUU7QUFBWCxTQUFwQixDQUFQO0FBQ0g7O0FBQ0QsYUFBTyx5QkFBRyxlQUFILEVBQW9CO0FBQUNBLFFBQUFBLFFBQVEsRUFBRUo7QUFBWCxPQUFwQixDQUFQO0FBQ0g7O0FBQ0QsUUFBSUYsQ0FBQyxHQUFHLEtBQUssRUFBYixFQUFpQjtBQUNiLGFBQU8seUJBQUcsZUFBSCxFQUFvQjtBQUFDTSxRQUFBQSxRQUFRLEVBQUVIO0FBQVgsT0FBcEIsQ0FBUDtBQUNIOztBQUNELFFBQUlILENBQUMsR0FBRyxLQUFLLEVBQUwsR0FBVSxFQUFsQixFQUFzQjtBQUNsQixhQUFPLHlCQUFHLGVBQUgsRUFBb0I7QUFBQ00sUUFBQUEsUUFBUSxFQUFFRjtBQUFYLE9BQXBCLENBQVA7QUFDSDs7QUFDRCxXQUFPLHlCQUFHLGVBQUgsRUFBb0I7QUFBQ0UsTUFBQUEsUUFBUSxFQUFFRDtBQUFYLEtBQXBCLENBQVA7QUFDSCxHQTdDMkI7QUErQzVCRSxFQUFBQSxpQkFBaUIsRUFBRSxVQUFTQyxRQUFULEVBQW1CbkIsU0FBbkIsRUFBOEJHLGVBQTlCLEVBQStDO0FBQzlELFFBQUksQ0FBQ0EsZUFBRCxJQUFvQkgsU0FBUyxLQUFLb0IsU0FBbEMsSUFBK0NwQixTQUFTLEdBQUcsQ0FBL0QsRUFBa0U7QUFDOUQsWUFBTWlCLFFBQVEsR0FBRyxLQUFLUixXQUFMLENBQWlCVCxTQUFqQixDQUFqQjtBQUNBLFVBQUltQixRQUFRLEtBQUssUUFBakIsRUFBMkIsT0FBTyx5QkFBRyx5QkFBSCxFQUE4QjtBQUFFRixRQUFBQSxRQUFRLEVBQUVBO0FBQVosT0FBOUIsQ0FBUDtBQUMzQixVQUFJRSxRQUFRLEtBQUssYUFBakIsRUFBZ0MsT0FBTyx5QkFBRyx1QkFBSCxFQUE0QjtBQUFFRixRQUFBQSxRQUFRLEVBQUVBO0FBQVosT0FBNUIsQ0FBUCxDQUg4QixDQUc4Qjs7QUFDNUYsVUFBSUUsUUFBUSxLQUFLLFNBQWpCLEVBQTRCLE9BQU8seUJBQUcsMEJBQUgsRUFBK0I7QUFBRUYsUUFBQUEsUUFBUSxFQUFFQTtBQUFaLE9BQS9CLENBQVA7QUFDNUIsYUFBTyx5QkFBRywwQkFBSCxFQUErQjtBQUFFQSxRQUFBQSxRQUFRLEVBQUVBO0FBQVosT0FBL0IsQ0FBUDtBQUNILEtBTkQsTUFNTztBQUNILFVBQUlFLFFBQVEsS0FBSyxRQUFqQixFQUEyQixPQUFPLHlCQUFHLFFBQUgsQ0FBUDtBQUMzQixVQUFJQSxRQUFRLEtBQUssYUFBakIsRUFBZ0MsT0FBTyx5QkFBRyxNQUFILENBQVAsQ0FGN0IsQ0FFZ0Q7O0FBQ25ELFVBQUlBLFFBQVEsS0FBSyxTQUFqQixFQUE0QixPQUFPLHlCQUFHLFNBQUgsQ0FBUDtBQUM1QixhQUFPLHlCQUFHLFNBQUgsQ0FBUDtBQUNIO0FBQ0osR0E1RDJCO0FBOEQ1QkUsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNLEtBQUtILGlCQUFMLENBQXVCLEtBQUtJLEtBQUwsQ0FBV2pCLGFBQWxDLEVBQWlELEtBQUtpQixLQUFMLENBQVd0QixTQUE1RCxFQUF1RSxLQUFLc0IsS0FBTCxDQUFXbkIsZUFBbEYsQ0FETixDQURKO0FBS0g7QUFwRTJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcblxyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ1ByZXNlbmNlTGFiZWwnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIG51bWJlciBvZiBtaWxsaXNlY29uZHMgYWdvIHRoaXMgdXNlciB3YXMgbGFzdCBhY3RpdmUuXHJcbiAgICAgICAgLy8gemVybyA9IHVua25vd25cclxuICAgICAgICBhY3RpdmVBZ286IFByb3BUeXBlcy5udW1iZXIsXHJcblxyXG4gICAgICAgIC8vIGlmIHRydWUsIGFjdGl2ZUFnbyBpcyBhbiBhcHByb3hpbWF0aW9uIGFuZCBcIk5vd1wiIHNob3VsZFxyXG4gICAgICAgIC8vIGJlIHNob3duIGluc3RlYWRcclxuICAgICAgICBjdXJyZW50bHlBY3RpdmU6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvLyBvZmZsaW5lLCBvbmxpbmUsIGV0Y1xyXG4gICAgICAgIHByZXNlbmNlU3RhdGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgYWdvOiAtMSxcclxuICAgICAgICAgICAgcHJlc2VuY2VTdGF0ZTogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBSZXR1cm4gZHVyYXRpb24gYXMgYSBzdHJpbmcgdXNpbmcgYXBwcm9wcmlhdGUgdGltZSB1bml0c1xyXG4gICAgLy8gWFhYOiBUaGlzIHdvdWxkIGJlIGJldHRlciBoYW5kbGVkIHVzaW5nIGEgY3VsdHVyZS1hd2FyZSBsaWJyYXJ5LCBidXQgd2UgZG9uJ3QgdXNlIG9uZSB5ZXQuXHJcbiAgICBnZXREdXJhdGlvbjogZnVuY3Rpb24odGltZSkge1xyXG4gICAgICAgIGlmICghdGltZSkgcmV0dXJuO1xyXG4gICAgICAgIGNvbnN0IHQgPSBwYXJzZUludCh0aW1lIC8gMTAwMCk7XHJcbiAgICAgICAgY29uc3QgcyA9IHQgJSA2MDtcclxuICAgICAgICBjb25zdCBtID0gcGFyc2VJbnQodCAvIDYwKSAlIDYwO1xyXG4gICAgICAgIGNvbnN0IGggPSBwYXJzZUludCh0IC8gKDYwICogNjApKSAlIDI0O1xyXG4gICAgICAgIGNvbnN0IGQgPSBwYXJzZUludCh0IC8gKDYwICogNjAgKiAyNCkpO1xyXG4gICAgICAgIGlmICh0IDwgNjApIHtcclxuICAgICAgICAgICAgaWYgKHQgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoXCIlKGR1cmF0aW9uKXNzXCIsIHtkdXJhdGlvbjogMH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBfdChcIiUoZHVyYXRpb24pc3NcIiwge2R1cmF0aW9uOiBzfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0IDwgNjAgKiA2MCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX3QoXCIlKGR1cmF0aW9uKXNtXCIsIHtkdXJhdGlvbjogbX0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodCA8IDI0ICogNjAgKiA2MCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX3QoXCIlKGR1cmF0aW9uKXNoXCIsIHtkdXJhdGlvbjogaH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gX3QoXCIlKGR1cmF0aW9uKXNkXCIsIHtkdXJhdGlvbjogZH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRQcmV0dHlQcmVzZW5jZTogZnVuY3Rpb24ocHJlc2VuY2UsIGFjdGl2ZUFnbywgY3VycmVudGx5QWN0aXZlKSB7XHJcbiAgICAgICAgaWYgKCFjdXJyZW50bHlBY3RpdmUgJiYgYWN0aXZlQWdvICE9PSB1bmRlZmluZWQgJiYgYWN0aXZlQWdvID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBkdXJhdGlvbiA9IHRoaXMuZ2V0RHVyYXRpb24oYWN0aXZlQWdvKTtcclxuICAgICAgICAgICAgaWYgKHByZXNlbmNlID09PSBcIm9ubGluZVwiKSByZXR1cm4gX3QoXCJPbmxpbmUgZm9yICUoZHVyYXRpb24pc1wiLCB7IGR1cmF0aW9uOiBkdXJhdGlvbiB9KTtcclxuICAgICAgICAgICAgaWYgKHByZXNlbmNlID09PSBcInVuYXZhaWxhYmxlXCIpIHJldHVybiBfdChcIklkbGUgZm9yICUoZHVyYXRpb24pc1wiLCB7IGR1cmF0aW9uOiBkdXJhdGlvbiB9KTsgLy8gWFhYOiBpcyB0aGlzIGFjdHVhbGx5IHJpZ2h0P1xyXG4gICAgICAgICAgICBpZiAocHJlc2VuY2UgPT09IFwib2ZmbGluZVwiKSByZXR1cm4gX3QoXCJPZmZsaW5lIGZvciAlKGR1cmF0aW9uKXNcIiwgeyBkdXJhdGlvbjogZHVyYXRpb24gfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBfdChcIlVua25vd24gZm9yICUoZHVyYXRpb24pc1wiLCB7IGR1cmF0aW9uOiBkdXJhdGlvbiB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAocHJlc2VuY2UgPT09IFwib25saW5lXCIpIHJldHVybiBfdChcIk9ubGluZVwiKTtcclxuICAgICAgICAgICAgaWYgKHByZXNlbmNlID09PSBcInVuYXZhaWxhYmxlXCIpIHJldHVybiBfdChcIklkbGVcIik7IC8vIFhYWDogaXMgdGhpcyBhY3R1YWxseSByaWdodD9cclxuICAgICAgICAgICAgaWYgKHByZXNlbmNlID09PSBcIm9mZmxpbmVcIikgcmV0dXJuIF90KFwiT2ZmbGluZVwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIF90KFwiVW5rbm93blwiKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9QcmVzZW5jZUxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICB7IHRoaXMuZ2V0UHJldHR5UHJlc2VuY2UodGhpcy5wcm9wcy5wcmVzZW5jZVN0YXRlLCB0aGlzLnByb3BzLmFjdGl2ZUFnbywgdGhpcy5wcm9wcy5jdXJyZW50bHlBY3RpdmUpIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=