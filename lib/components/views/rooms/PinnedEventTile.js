"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _MessageEvent = _interopRequireDefault(require("../messages/MessageEvent"));

var _MemberAvatar = _interopRequireDefault(require("../avatars/MemberAvatar"));

var _languageHandler = require("../../../languageHandler");

var _DateUtils = require("../../../DateUtils");

/*
Copyright 2017 Travis Ralston

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'PinnedEventTile',
  propTypes: {
    mxRoom: _propTypes.default.object.isRequired,
    mxEvent: _propTypes.default.object.isRequired,
    onUnpinned: _propTypes.default.func
  },
  onTileClicked: function () {
    _dispatcher.default.dispatch({
      action: 'view_room',
      event_id: this.props.mxEvent.getId(),
      highlighted: true,
      room_id: this.props.mxEvent.getRoomId()
    });
  },
  onUnpinClicked: function () {
    const pinnedEvents = this.props.mxRoom.currentState.getStateEvents("m.room.pinned_events", "");

    if (!pinnedEvents || !pinnedEvents.getContent().pinned) {
      // Nothing to do: already unpinned
      if (this.props.onUnpinned) this.props.onUnpinned();
    } else {
      const pinned = pinnedEvents.getContent().pinned;
      const index = pinned.indexOf(this.props.mxEvent.getId());

      if (index !== -1) {
        pinned.splice(index, 1);

        _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(this.props.mxRoom.roomId, 'm.room.pinned_events', {
          pinned
        }, '').then(() => {
          if (this.props.onUnpinned) this.props.onUnpinned();
        });
      } else if (this.props.onUnpinned) this.props.onUnpinned();
    }
  },
  _canUnpin: function () {
    return this.props.mxRoom.currentState.mayClientSendStateEvent('m.room.pinned_events', _MatrixClientPeg.MatrixClientPeg.get());
  },
  render: function () {
    const sender = this.props.mxEvent.getSender(); // Get the latest sender profile rather than historical

    const senderProfile = this.props.mxRoom.getMember(sender);
    const avatarSize = 40;
    let unpinButton = null;

    if (this._canUnpin()) {
      unpinButton = _react.default.createElement(_AccessibleButton.default, {
        onClick: this.onUnpinClicked,
        className: "mx_PinnedEventTile_unpinButton"
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/cancel-red.svg"),
        width: "8",
        height: "8",
        alt: (0, _languageHandler._t)('Unpin Message'),
        title: (0, _languageHandler._t)('Unpin Message')
      }));
    }

    return _react.default.createElement("div", {
      className: "mx_PinnedEventTile"
    }, _react.default.createElement("div", {
      className: "mx_PinnedEventTile_actions"
    }, _react.default.createElement(_AccessibleButton.default, {
      className: "mx_PinnedEventTile_gotoButton mx_textButton",
      onClick: this.onTileClicked
    }, (0, _languageHandler._t)("Jump to message")), unpinButton), _react.default.createElement("span", {
      className: "mx_PinnedEventTile_senderAvatar"
    }, _react.default.createElement(_MemberAvatar.default, {
      member: senderProfile,
      width: avatarSize,
      height: avatarSize,
      fallbackUserId: sender
    })), _react.default.createElement("span", {
      className: "mx_PinnedEventTile_sender"
    }, senderProfile ? senderProfile.name : sender), _react.default.createElement("span", {
      className: "mx_PinnedEventTile_timestamp"
    }, (0, _DateUtils.formatFullDate)(new Date(this.props.mxEvent.getTs()))), _react.default.createElement("div", {
      className: "mx_PinnedEventTile_message"
    }, _react.default.createElement(_MessageEvent.default, {
      mxEvent: this.props.mxEvent,
      className: "mx_PinnedEventTile_body",
      maxImageHeight: 150,
      onHeightChanged: () => {} // we need to give this, apparently

    })));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Bpbm5lZEV2ZW50VGlsZS5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsInByb3BUeXBlcyIsIm14Um9vbSIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJteEV2ZW50Iiwib25VbnBpbm5lZCIsImZ1bmMiLCJvblRpbGVDbGlja2VkIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJldmVudF9pZCIsInByb3BzIiwiZ2V0SWQiLCJoaWdobGlnaHRlZCIsInJvb21faWQiLCJnZXRSb29tSWQiLCJvblVucGluQ2xpY2tlZCIsInBpbm5lZEV2ZW50cyIsImN1cnJlbnRTdGF0ZSIsImdldFN0YXRlRXZlbnRzIiwiZ2V0Q29udGVudCIsInBpbm5lZCIsImluZGV4IiwiaW5kZXhPZiIsInNwbGljZSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInNlbmRTdGF0ZUV2ZW50Iiwicm9vbUlkIiwidGhlbiIsIl9jYW5VbnBpbiIsIm1heUNsaWVudFNlbmRTdGF0ZUV2ZW50IiwicmVuZGVyIiwic2VuZGVyIiwiZ2V0U2VuZGVyIiwic2VuZGVyUHJvZmlsZSIsImdldE1lbWJlciIsImF2YXRhclNpemUiLCJ1bnBpbkJ1dHRvbiIsInJlcXVpcmUiLCJuYW1lIiwiRGF0ZSIsImdldFRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7ZUEyQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsaUJBRGU7QUFFNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxNQUFNLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQURsQjtBQUVQQyxJQUFBQSxPQUFPLEVBQUVILG1CQUFVQyxNQUFWLENBQWlCQyxVQUZuQjtBQUdQRSxJQUFBQSxVQUFVLEVBQUVKLG1CQUFVSztBQUhmLEdBRmlCO0FBTzVCQyxFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QkMsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxNQUFBQSxRQUFRLEVBQUUsS0FBS0MsS0FBTCxDQUFXUixPQUFYLENBQW1CUyxLQUFuQixFQUZEO0FBR1RDLE1BQUFBLFdBQVcsRUFBRSxJQUhKO0FBSVRDLE1BQUFBLE9BQU8sRUFBRSxLQUFLSCxLQUFMLENBQVdSLE9BQVgsQ0FBbUJZLFNBQW5CO0FBSkEsS0FBYjtBQU1ILEdBZDJCO0FBZTVCQyxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QixVQUFNQyxZQUFZLEdBQUcsS0FBS04sS0FBTCxDQUFXWixNQUFYLENBQWtCbUIsWUFBbEIsQ0FBK0JDLGNBQS9CLENBQThDLHNCQUE5QyxFQUFzRSxFQUF0RSxDQUFyQjs7QUFDQSxRQUFJLENBQUNGLFlBQUQsSUFBaUIsQ0FBQ0EsWUFBWSxDQUFDRyxVQUFiLEdBQTBCQyxNQUFoRCxFQUF3RDtBQUNwRDtBQUNBLFVBQUksS0FBS1YsS0FBTCxDQUFXUCxVQUFmLEVBQTJCLEtBQUtPLEtBQUwsQ0FBV1AsVUFBWDtBQUM5QixLQUhELE1BR087QUFDSCxZQUFNaUIsTUFBTSxHQUFHSixZQUFZLENBQUNHLFVBQWIsR0FBMEJDLE1BQXpDO0FBQ0EsWUFBTUMsS0FBSyxHQUFHRCxNQUFNLENBQUNFLE9BQVAsQ0FBZSxLQUFLWixLQUFMLENBQVdSLE9BQVgsQ0FBbUJTLEtBQW5CLEVBQWYsQ0FBZDs7QUFDQSxVQUFJVSxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQ2RELFFBQUFBLE1BQU0sQ0FBQ0csTUFBUCxDQUFjRixLQUFkLEVBQXFCLENBQXJCOztBQUNBRyx5Q0FBZ0JDLEdBQWhCLEdBQXNCQyxjQUF0QixDQUFxQyxLQUFLaEIsS0FBTCxDQUFXWixNQUFYLENBQWtCNkIsTUFBdkQsRUFBK0Qsc0JBQS9ELEVBQXVGO0FBQUNQLFVBQUFBO0FBQUQsU0FBdkYsRUFBaUcsRUFBakcsRUFDQ1EsSUFERCxDQUNNLE1BQU07QUFDUixjQUFJLEtBQUtsQixLQUFMLENBQVdQLFVBQWYsRUFBMkIsS0FBS08sS0FBTCxDQUFXUCxVQUFYO0FBQzlCLFNBSEQ7QUFJSCxPQU5ELE1BTU8sSUFBSSxLQUFLTyxLQUFMLENBQVdQLFVBQWYsRUFBMkIsS0FBS08sS0FBTCxDQUFXUCxVQUFYO0FBQ3JDO0FBQ0osR0EvQjJCO0FBZ0M1QjBCLEVBQUFBLFNBQVMsRUFBRSxZQUFXO0FBQ2xCLFdBQU8sS0FBS25CLEtBQUwsQ0FBV1osTUFBWCxDQUFrQm1CLFlBQWxCLENBQStCYSx1QkFBL0IsQ0FBdUQsc0JBQXZELEVBQStFTixpQ0FBZ0JDLEdBQWhCLEVBQS9FLENBQVA7QUFDSCxHQWxDMkI7QUFtQzVCTSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLE1BQU0sR0FBRyxLQUFLdEIsS0FBTCxDQUFXUixPQUFYLENBQW1CK0IsU0FBbkIsRUFBZixDQURlLENBRWY7O0FBQ0EsVUFBTUMsYUFBYSxHQUFHLEtBQUt4QixLQUFMLENBQVdaLE1BQVgsQ0FBa0JxQyxTQUFsQixDQUE0QkgsTUFBNUIsQ0FBdEI7QUFDQSxVQUFNSSxVQUFVLEdBQUcsRUFBbkI7QUFFQSxRQUFJQyxXQUFXLEdBQUcsSUFBbEI7O0FBQ0EsUUFBSSxLQUFLUixTQUFMLEVBQUosRUFBc0I7QUFDbEJRLE1BQUFBLFdBQVcsR0FDUCw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBRSxLQUFLdEIsY0FBaEM7QUFBZ0QsUUFBQSxTQUFTLEVBQUM7QUFBMUQsU0FDSTtBQUFLLFFBQUEsR0FBRyxFQUFFdUIsT0FBTyxDQUFDLG9DQUFELENBQWpCO0FBQXlELFFBQUEsS0FBSyxFQUFDLEdBQS9EO0FBQW1FLFFBQUEsTUFBTSxFQUFDLEdBQTFFO0FBQThFLFFBQUEsR0FBRyxFQUFFLHlCQUFHLGVBQUgsQ0FBbkY7QUFBd0csUUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSDtBQUEvRyxRQURKLENBREo7QUFLSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLDZDQUE1QjtBQUEwRSxNQUFBLE9BQU8sRUFBRSxLQUFLakM7QUFBeEYsT0FDTSx5QkFBRyxpQkFBSCxDQUROLENBREosRUFJTWdDLFdBSk4sQ0FESixFQVFJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDSSw2QkFBQyxxQkFBRDtBQUFjLE1BQUEsTUFBTSxFQUFFSCxhQUF0QjtBQUFxQyxNQUFBLEtBQUssRUFBRUUsVUFBNUM7QUFBd0QsTUFBQSxNQUFNLEVBQUVBLFVBQWhFO0FBQTRFLE1BQUEsY0FBYyxFQUFFSjtBQUE1RixNQURKLENBUkosRUFXSTtBQUFNLE1BQUEsU0FBUyxFQUFDO0FBQWhCLE9BQ01FLGFBQWEsR0FBR0EsYUFBYSxDQUFDSyxJQUFqQixHQUF3QlAsTUFEM0MsQ0FYSixFQWNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDTSwrQkFBZSxJQUFJUSxJQUFKLENBQVMsS0FBSzlCLEtBQUwsQ0FBV1IsT0FBWCxDQUFtQnVDLEtBQW5CLEVBQVQsQ0FBZixDQUROLENBZEosRUFpQkk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMscUJBQUQ7QUFBYyxNQUFBLE9BQU8sRUFBRSxLQUFLL0IsS0FBTCxDQUFXUixPQUFsQztBQUEyQyxNQUFBLFNBQVMsRUFBQyx5QkFBckQ7QUFBK0UsTUFBQSxjQUFjLEVBQUUsR0FBL0Y7QUFDYyxNQUFBLGVBQWUsRUFBRSxNQUFNLENBQUUsQ0FEdkMsQ0FDeUM7O0FBRHpDLE1BREosQ0FqQkosQ0FESjtBQXlCSDtBQTNFMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFRyYXZpcyBSYWxzdG9uXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uXCI7XHJcbmltcG9ydCBNZXNzYWdlRXZlbnQgZnJvbSBcIi4uL21lc3NhZ2VzL01lc3NhZ2VFdmVudFwiO1xyXG5pbXBvcnQgTWVtYmVyQXZhdGFyIGZyb20gXCIuLi9hdmF0YXJzL01lbWJlckF2YXRhclwiO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7Zm9ybWF0RnVsbERhdGV9IGZyb20gJy4uLy4uLy4uL0RhdGVVdGlscyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnUGlubmVkRXZlbnRUaWxlJyxcclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIG14Um9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG14RXZlbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuICAgICAgICBvblVucGlubmVkOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIH0sXHJcbiAgICBvblRpbGVDbGlja2VkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICBldmVudF9pZDogdGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCksXHJcbiAgICAgICAgICAgIGhpZ2hsaWdodGVkOiB0cnVlLFxyXG4gICAgICAgICAgICByb29tX2lkOiB0aGlzLnByb3BzLm14RXZlbnQuZ2V0Um9vbUlkKCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG4gICAgb25VbnBpbkNsaWNrZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHBpbm5lZEV2ZW50cyA9IHRoaXMucHJvcHMubXhSb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5waW5uZWRfZXZlbnRzXCIsIFwiXCIpO1xyXG4gICAgICAgIGlmICghcGlubmVkRXZlbnRzIHx8ICFwaW5uZWRFdmVudHMuZ2V0Q29udGVudCgpLnBpbm5lZCkge1xyXG4gICAgICAgICAgICAvLyBOb3RoaW5nIHRvIGRvOiBhbHJlYWR5IHVucGlubmVkXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uVW5waW5uZWQpIHRoaXMucHJvcHMub25VbnBpbm5lZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBpbm5lZCA9IHBpbm5lZEV2ZW50cy5nZXRDb250ZW50KCkucGlubmVkO1xyXG4gICAgICAgICAgICBjb25zdCBpbmRleCA9IHBpbm5lZC5pbmRleE9mKHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpKTtcclxuICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgcGlubmVkLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2VuZFN0YXRlRXZlbnQodGhpcy5wcm9wcy5teFJvb20ucm9vbUlkLCAnbS5yb29tLnBpbm5lZF9ldmVudHMnLCB7cGlubmVkfSwgJycpXHJcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMub25VbnBpbm5lZCkgdGhpcy5wcm9wcy5vblVucGlubmVkKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByb3BzLm9uVW5waW5uZWQpIHRoaXMucHJvcHMub25VbnBpbm5lZCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBfY2FuVW5waW46IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLm14Um9vbS5jdXJyZW50U3RhdGUubWF5Q2xpZW50U2VuZFN0YXRlRXZlbnQoJ20ucm9vbS5waW5uZWRfZXZlbnRzJywgTWF0cml4Q2xpZW50UGVnLmdldCgpKTtcclxuICAgIH0sXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHNlbmRlciA9IHRoaXMucHJvcHMubXhFdmVudC5nZXRTZW5kZXIoKTtcclxuICAgICAgICAvLyBHZXQgdGhlIGxhdGVzdCBzZW5kZXIgcHJvZmlsZSByYXRoZXIgdGhhbiBoaXN0b3JpY2FsXHJcbiAgICAgICAgY29uc3Qgc2VuZGVyUHJvZmlsZSA9IHRoaXMucHJvcHMubXhSb29tLmdldE1lbWJlcihzZW5kZXIpO1xyXG4gICAgICAgIGNvbnN0IGF2YXRhclNpemUgPSA0MDtcclxuXHJcbiAgICAgICAgbGV0IHVucGluQnV0dG9uID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5fY2FuVW5waW4oKSkge1xyXG4gICAgICAgICAgICB1bnBpbkJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMub25VbnBpbkNsaWNrZWR9IGNsYXNzTmFtZT1cIm14X1Bpbm5lZEV2ZW50VGlsZV91bnBpbkJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwtcmVkLnN2Z1wiKX0gd2lkdGg9XCI4XCIgaGVpZ2h0PVwiOFwiIGFsdD17X3QoJ1VucGluIE1lc3NhZ2UnKX0gdGl0bGU9e190KCdVbnBpbiBNZXNzYWdlJyl9IC8+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Bpbm5lZEV2ZW50VGlsZVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9QaW5uZWRFdmVudFRpbGVfYWN0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1Bpbm5lZEV2ZW50VGlsZV9nb3RvQnV0dG9uIG14X3RleHRCdXR0b25cIiBvbkNsaWNrPXt0aGlzLm9uVGlsZUNsaWNrZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiSnVtcCB0byBtZXNzYWdlXCIpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB1bnBpbkJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9QaW5uZWRFdmVudFRpbGVfc2VuZGVyQXZhdGFyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPE1lbWJlckF2YXRhciBtZW1iZXI9e3NlbmRlclByb2ZpbGV9IHdpZHRoPXthdmF0YXJTaXplfSBoZWlnaHQ9e2F2YXRhclNpemV9IGZhbGxiYWNrVXNlcklkPXtzZW5kZXJ9IC8+XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9QaW5uZWRFdmVudFRpbGVfc2VuZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBzZW5kZXJQcm9maWxlID8gc2VuZGVyUHJvZmlsZS5uYW1lIDogc2VuZGVyIH1cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X1Bpbm5lZEV2ZW50VGlsZV90aW1lc3RhbXBcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGZvcm1hdEZ1bGxEYXRlKG5ldyBEYXRlKHRoaXMucHJvcHMubXhFdmVudC5nZXRUcygpKSkgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9QaW5uZWRFdmVudFRpbGVfbWVzc2FnZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxNZXNzYWdlRXZlbnQgbXhFdmVudD17dGhpcy5wcm9wcy5teEV2ZW50fSBjbGFzc05hbWU9XCJteF9QaW5uZWRFdmVudFRpbGVfYm9keVwiIG1heEltYWdlSGVpZ2h0PXsxNTB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9eygpID0+IHt9fSAvLyB3ZSBuZWVkIHRvIGdpdmUgdGhpcywgYXBwYXJlbnRseVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=