"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _AutoHideScrollbar = _interopRequireDefault(require("../../structures/AutoHideScrollbar"));

var _RoomInvite = require("../../../RoomInvite");

var _ratelimitedfunc = _interopRequireDefault(require("../../../ratelimitedfunc"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _CallHandler = _interopRequireDefault(require("../../../CallHandler"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const INITIAL_LOAD_NUM_MEMBERS = 30;
const INITIAL_LOAD_NUM_INVITED = 5;
const SHOW_MORE_INCREMENT = 100; // Regex applied to filter our punctuation in member names before applying sort, to fuzzy it a little
// matches all ASCII punctuation: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~

const SORT_REGEX = /[\x21-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]+/g;

var _default = (0, _createReactClass.default)({
  displayName: 'MemberList',
  getInitialState: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli.hasLazyLoadMembersEnabled()) {
      // show an empty list
      return this._getMembersState([]);
    } else {
      return this._getMembersState(this.roomMembers());
    }
  },
  // TODO: [REACT-WARNING] Move this to constructor
  UNSAFE_componentWillMount: function () {
    this._mounted = true;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli.hasLazyLoadMembersEnabled()) {
      this._showMembersAccordingToMembershipWithLL();

      cli.on("Room.myMembership", this.onMyMembership);
    } else {
      this._listenForMembersChanges();
    }

    cli.on("Room", this.onRoom); // invites & joining after peek

    const enablePresenceByHsUrl = _SdkConfig.default.get()["enable_presence_by_hs_url"];

    const hsUrl = _MatrixClientPeg.MatrixClientPeg.get().baseUrl;

    this._showPresence = true;

    if (enablePresenceByHsUrl && enablePresenceByHsUrl[hsUrl] !== undefined) {
      this._showPresence = enablePresenceByHsUrl[hsUrl];
    }
  },
  _listenForMembersChanges: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.on("RoomState.members", this.onRoomStateMember);
    cli.on("RoomMember.name", this.onRoomMemberName);
    cli.on("RoomState.events", this.onRoomStateEvent); // We listen for changes to the lastPresenceTs which is essentially
    // listening for all presence events (we display most of not all of
    // the information contained in presence events).

    cli.on("User.lastPresenceTs", this.onUserPresenceChange);
    cli.on("User.presence", this.onUserPresenceChange);
    cli.on("User.currentlyActive", this.onUserPresenceChange); // cli.on("Room.timeline", this.onRoomTimeline);
  },
  componentWillUnmount: function () {
    this._mounted = false;

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli) {
      cli.removeListener("RoomState.members", this.onRoomStateMember);
      cli.removeListener("RoomMember.name", this.onRoomMemberName);
      cli.removeListener("Room.myMembership", this.onMyMembership);
      cli.removeListener("RoomState.events", this.onRoomStateEvent);
      cli.removeListener("Room", this.onRoom);
      cli.removeListener("User.lastPresenceTs", this.onUserPresenceChange);
      cli.removeListener("User.presence", this.onUserPresenceChange);
      cli.removeListener("User.currentlyActive", this.onUserPresenceChange);
    } // cancel any pending calls to the rate_limited_funcs


    this._updateList.cancelPendingCall();
  },

  /**
   * If lazy loading is enabled, either:
   * show a spinner and load the members if the user is joined,
   * or show the members available so far if the user is invited
   */
  _showMembersAccordingToMembershipWithLL: async function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli.hasLazyLoadMembersEnabled()) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const room = cli.getRoom(this.props.roomId);
      const membership = room && room.getMyMembership();

      if (membership === "join") {
        this.setState({
          loading: true
        });

        try {
          await room.loadMembersIfNeeded();
        } catch (ex) {
          /* already logged in RoomView */
        }

        if (this._mounted) {
          this.setState(this._getMembersState(this.roomMembers()));

          this._listenForMembersChanges();
        }
      } else if (membership === "invite") {
        // show the members we've got when invited
        this.setState(this._getMembersState(this.roomMembers()));
      }
    }
  },
  _getMembersState: function (members) {
    // set the state after determining _showPresence to make sure it's
    // taken into account while rerendering
    return {
      loading: false,
      members: members,
      filteredJoinedMembers: this._filterMembers(members, 'join'),
      filteredInvitedMembers: this._filterMembers(members, 'invite'),
      // ideally we'd size this to the page height, but
      // in practice I find that a little constraining
      truncateAtJoined: INITIAL_LOAD_NUM_MEMBERS,
      truncateAtInvited: INITIAL_LOAD_NUM_INVITED,
      searchQuery: ""
    };
  },

  onUserPresenceChange(event, user) {
    // Attach a SINGLE listener for global presence changes then locate the
    // member tile and re-render it. This is more efficient than every tile
    // ever attaching their own listener.
    const tile = this.refs[user.userId]; // console.log(`Got presence update for ${user.userId}. hasTile=${!!tile}`);

    if (tile) {
      this._updateList(); // reorder the membership list

    }
  },

  onRoom: function (room) {
    if (room.roomId !== this.props.roomId) {
      return;
    } // We listen for room events because when we accept an invite
    // we need to wait till the room is fully populated with state
    // before refreshing the member list else we get a stale list.


    this._showMembersAccordingToMembershipWithLL();
  },
  onMyMembership: function (room, membership, oldMembership) {
    if (room.roomId === this.props.roomId && membership === "join") {
      this._showMembersAccordingToMembershipWithLL();
    }
  },
  onRoomStateMember: function (ev, state, member) {
    if (member.roomId !== this.props.roomId) {
      return;
    }

    this._updateList();
  },
  onRoomMemberName: function (ev, member) {
    if (member.roomId !== this.props.roomId) {
      return;
    }

    this._updateList();
  },
  onRoomStateEvent: function (event, state) {
    if (event.getRoomId() === this.props.roomId && event.getType() === "m.room.third_party_invite") {
      this._updateList();
    }
  },
  _updateList: (0, _ratelimitedfunc.default)(function () {
    this._updateListNow();
  }, 500),
  _updateListNow: function () {
    // console.log("Updating memberlist");
    const newState = {
      loading: false,
      members: this.roomMembers()
    };
    newState.filteredJoinedMembers = this._filterMembers(newState.members, 'join', this.state.searchQuery);
    newState.filteredInvitedMembers = this._filterMembers(newState.members, 'invite', this.state.searchQuery);
    this.setState(newState);
  },
  getMembersWithUser: function () {
    if (!this.props.roomId) return [];

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const room = cli.getRoom(this.props.roomId);
    if (!room) return [];
    const allMembers = Object.values(room.currentState.members);
    allMembers.forEach(function (member) {
      // work around a race where you might have a room member object
      // before the user object exists.  This may or may not cause
      // https://github.com/vector-im/vector-web/issues/186
      if (member.user === null) {
        member.user = cli.getUser(member.userId);
      } // XXX: this user may have no lastPresenceTs value!
      // the right solution here is to fix the race rather than leave it as 0

    });
    return allMembers;
  },
  roomMembers: function () {
    const ConferenceHandler = _CallHandler.default.getConferenceHandler();

    const allMembers = this.getMembersWithUser();
    const filteredAndSortedMembers = allMembers.filter(m => {
      return (m.membership === 'join' || m.membership === 'invite') && (!ConferenceHandler || ConferenceHandler && !ConferenceHandler.isConferenceUser(m.userId));
    });
    filteredAndSortedMembers.sort(this.memberSort);
    return filteredAndSortedMembers;
  },
  _createOverflowTileJoined: function (overflowCount, totalCount) {
    return this._createOverflowTile(overflowCount, totalCount, this._showMoreJoinedMemberList);
  },
  _createOverflowTileInvited: function (overflowCount, totalCount) {
    return this._createOverflowTile(overflowCount, totalCount, this._showMoreInvitedMemberList);
  },
  _createOverflowTile: function (overflowCount, totalCount, onClick) {
    // For now we'll pretend this is any entity. It should probably be a separate tile.
    const EntityTile = sdk.getComponent("rooms.EntityTile");
    const BaseAvatar = sdk.getComponent("avatars.BaseAvatar");
    const text = (0, _languageHandler._t)("and %(count)s others...", {
      count: overflowCount
    });
    return _react.default.createElement(EntityTile, {
      className: "mx_EntityTile_ellipsis",
      avatarJsx: _react.default.createElement(BaseAvatar, {
        url: require("../../../../res/img/ellipsis.svg"),
        name: "...",
        width: 36,
        height: 36
      }),
      name: text,
      presenceState: "online",
      suppressOnHover: true,
      onClick: onClick
    });
  },
  _showMoreJoinedMemberList: function () {
    this.setState({
      truncateAtJoined: this.state.truncateAtJoined + SHOW_MORE_INCREMENT
    });
  },
  _showMoreInvitedMemberList: function () {
    this.setState({
      truncateAtInvited: this.state.truncateAtInvited + SHOW_MORE_INCREMENT
    });
  },
  memberString: function (member) {
    if (!member) {
      return "(null)";
    } else {
      const u = member.user;
      return "(" + member.name + ", " + member.powerLevel + ", " + (u ? u.lastActiveAgo : "<null>") + ", " + (u ? u.getLastActiveTs() : "<null>") + ", " + (u ? u.currentlyActive : "<null>") + ", " + (u ? u.presence : "<null>") + ")";
    }
  },
  // returns negative if a comes before b,
  // returns 0 if a and b are equivalent in ordering
  // returns positive if a comes after b.
  memberSort: function (memberA, memberB) {
    // order by presence, with "active now" first.
    // ...and then by power level
    // ...and then by last active
    // ...and then alphabetically.
    // We could tiebreak instead by "last recently spoken in this room" if we wanted to.
    // console.log(`Comparing userA=${this.memberString(memberA)} userB=${this.memberString(memberB)}`);
    const userA = memberA.user;
    const userB = memberB.user; // if (!userA) console.log("!! MISSING USER FOR A-SIDE: " + memberA.name + " !!");
    // if (!userB) console.log("!! MISSING USER FOR B-SIDE: " + memberB.name + " !!");

    if (!userA && !userB) return 0;
    if (userA && !userB) return -1;
    if (!userA && userB) return 1; // First by presence

    if (this._showPresence) {
      const convertPresence = p => p === 'unavailable' ? 'online' : p;

      const presenceIndex = p => {
        const order = ['active', 'online', 'offline'];
        const idx = order.indexOf(convertPresence(p));
        return idx === -1 ? order.length : idx; // unknown states at the end
      };

      const idxA = presenceIndex(userA.currentlyActive ? 'active' : userA.presence);
      const idxB = presenceIndex(userB.currentlyActive ? 'active' : userB.presence); // console.log(`userA_presenceGroup=${idxA} userB_presenceGroup=${idxB}`);

      if (idxA !== idxB) {
        // console.log("Comparing on presence group - returning");
        return idxA - idxB;
      }
    } // Second by power level


    if (memberA.powerLevel !== memberB.powerLevel) {
      // console.log("Comparing on power level - returning");
      return memberB.powerLevel - memberA.powerLevel;
    } // Third by last active


    if (this._showPresence && userA.getLastActiveTs() !== userB.getLastActiveTs()) {
      // console.log("Comparing on last active timestamp - returning");
      return userB.getLastActiveTs() - userA.getLastActiveTs();
    } // Fourth by name (alphabetical)


    const nameA = (memberA.name[0] === '@' ? memberA.name.substr(1) : memberA.name).replace(SORT_REGEX, "");
    const nameB = (memberB.name[0] === '@' ? memberB.name.substr(1) : memberB.name).replace(SORT_REGEX, ""); // console.log(`Comparing userA_name=${nameA} against userB_name=${nameB} - returning`);

    return nameA.localeCompare(nameB, {
      ignorePunctuation: true,
      sensitivity: "base"
    });
  },
  onSearchQueryChanged: function (searchQuery) {
    this.setState({
      searchQuery,
      filteredJoinedMembers: this._filterMembers(this.state.members, 'join', searchQuery),
      filteredInvitedMembers: this._filterMembers(this.state.members, 'invite', searchQuery)
    });
  },
  _onPending3pidInviteClick: function (inviteEvent) {
    _dispatcher.default.dispatch({
      action: 'view_3pid_invite',
      event: inviteEvent
    });
  },
  _filterMembers: function (members, membership, query) {
    return members.filter(m => {
      if (query) {
        query = query.toLowerCase();
        const matchesName = m.name.toLowerCase().indexOf(query) !== -1;
        const matchesId = m.userId.toLowerCase().indexOf(query) !== -1;

        if (!matchesName && !matchesId) {
          return false;
        }
      }

      return m.membership === membership;
    });
  },
  _getPending3PidInvites: function () {
    // include 3pid invites (m.room.third_party_invite) state events.
    // The HS may have already converted these into m.room.member invites so
    // we shouldn't add them if the 3pid invite state key (token) is in the
    // member invite (content.third_party_invite.signed.token)
    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.roomId);

    if (room) {
      return room.currentState.getStateEvents("m.room.third_party_invite").filter(function (e) {
        if (!(0, _RoomInvite.isValid3pidInvite)(e)) return false; // discard all invites which have a m.room.member event since we've
        // already added them.

        const memberEvent = room.currentState.getInviteForThreePidToken(e.getStateKey());
        if (memberEvent) return false;
        return true;
      });
    }
  },
  _makeMemberTiles: function (members) {
    const MemberTile = sdk.getComponent("rooms.MemberTile");
    const EntityTile = sdk.getComponent("rooms.EntityTile");
    return members.map(m => {
      if (m.userId) {
        // Is a Matrix invite
        return _react.default.createElement(MemberTile, {
          key: m.userId,
          member: m,
          ref: m.userId,
          showPresence: this._showPresence
        });
      } else {
        // Is a 3pid invite
        return _react.default.createElement(EntityTile, {
          key: m.getStateKey(),
          name: m.getContent().display_name,
          suppressOnHover: true,
          onClick: () => this._onPending3pidInviteClick(m)
        });
      }
    });
  },
  _getChildrenJoined: function (start, end) {
    return this._makeMemberTiles(this.state.filteredJoinedMembers.slice(start, end));
  },
  _getChildCountJoined: function () {
    return this.state.filteredJoinedMembers.length;
  },
  _getChildrenInvited: function (start, end) {
    let targets = this.state.filteredInvitedMembers;

    if (end > this.state.filteredInvitedMembers.length) {
      targets = targets.concat(this._getPending3PidInvites());
    }

    return this._makeMemberTiles(targets.slice(start, end));
  },
  _getChildCountInvited: function () {
    return this.state.filteredInvitedMembers.length + (this._getPending3PidInvites() || []).length;
  },
  render: function () {
    if (this.state.loading) {
      const Spinner = sdk.getComponent("elements.Spinner");
      return _react.default.createElement("div", {
        className: "mx_MemberList"
      }, _react.default.createElement(Spinner, null));
    }

    const SearchBox = sdk.getComponent('structures.SearchBox');
    const TruncatedList = sdk.getComponent("elements.TruncatedList");

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const room = cli.getRoom(this.props.roomId);
    let inviteButton;

    if (room && room.getMyMembership() === 'join') {
      // assume we can invite until proven false
      let canInvite = true;
      const plEvent = room.currentState.getStateEvents("m.room.power_levels", "");
      const me = room.getMember(cli.getUserId());

      if (plEvent && me) {
        const content = plEvent.getContent();

        if (content && content.invite > me.powerLevel) {
          canInvite = false;
        }
      }

      const AccessibleButton = sdk.getComponent("elements.AccessibleButton");
      inviteButton = _react.default.createElement(AccessibleButton, {
        className: "mx_MemberList_invite",
        onClick: this.onInviteButtonClick,
        disabled: !canInvite
      }, _react.default.createElement("span", null, (0, _languageHandler._t)('Invite to this room')));
    }

    let invitedHeader;
    let invitedSection;

    if (this._getChildCountInvited() > 0) {
      invitedHeader = _react.default.createElement("h2", null, (0, _languageHandler._t)("Invited"));
      invitedSection = _react.default.createElement(TruncatedList, {
        className: "mx_MemberList_section mx_MemberList_invited",
        truncateAt: this.state.truncateAtInvited,
        createOverflowElement: this._createOverflowTileInvited,
        getChildren: this._getChildrenInvited,
        getChildCount: this._getChildCountInvited
      });
    }

    return _react.default.createElement("div", {
      className: "mx_MemberList",
      role: "tabpanel"
    }, inviteButton, _react.default.createElement(_AutoHideScrollbar.default, null, _react.default.createElement("div", {
      className: "mx_MemberList_wrapper"
    }, _react.default.createElement(TruncatedList, {
      className: "mx_MemberList_section mx_MemberList_joined",
      truncateAt: this.state.truncateAtJoined,
      createOverflowElement: this._createOverflowTileJoined,
      getChildren: this._getChildrenJoined,
      getChildCount: this._getChildCountJoined
    }), invitedHeader, invitedSection)), _react.default.createElement(SearchBox, {
      className: "mx_MemberList_query mx_textinput_icon mx_textinput_search",
      placeholder: (0, _languageHandler._t)('Filter room members'),
      onSearch: this.onSearchQueryChanged
    }));
  },
  onInviteButtonClick: function () {
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
      _dispatcher.default.dispatch({
        action: 'require_registration'
      });

      return;
    } // call AddressPickerDialog


    _dispatcher.default.dispatch({
      action: 'view_invite',
      roomId: this.props.roomId
    });
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL01lbWJlckxpc3QuanMiXSwibmFtZXMiOlsiSU5JVElBTF9MT0FEX05VTV9NRU1CRVJTIiwiSU5JVElBTF9MT0FEX05VTV9JTlZJVEVEIiwiU0hPV19NT1JFX0lOQ1JFTUVOVCIsIlNPUlRfUkVHRVgiLCJkaXNwbGF5TmFtZSIsImdldEluaXRpYWxTdGF0ZSIsImNsaSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImhhc0xhenlMb2FkTWVtYmVyc0VuYWJsZWQiLCJfZ2V0TWVtYmVyc1N0YXRlIiwicm9vbU1lbWJlcnMiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX21vdW50ZWQiLCJfc2hvd01lbWJlcnNBY2NvcmRpbmdUb01lbWJlcnNoaXBXaXRoTEwiLCJvbiIsIm9uTXlNZW1iZXJzaGlwIiwiX2xpc3RlbkZvck1lbWJlcnNDaGFuZ2VzIiwib25Sb29tIiwiZW5hYmxlUHJlc2VuY2VCeUhzVXJsIiwiU2RrQ29uZmlnIiwiaHNVcmwiLCJiYXNlVXJsIiwiX3Nob3dQcmVzZW5jZSIsInVuZGVmaW5lZCIsIm9uUm9vbVN0YXRlTWVtYmVyIiwib25Sb29tTWVtYmVyTmFtZSIsIm9uUm9vbVN0YXRlRXZlbnQiLCJvblVzZXJQcmVzZW5jZUNoYW5nZSIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJfdXBkYXRlTGlzdCIsImNhbmNlbFBlbmRpbmdDYWxsIiwicm9vbSIsImdldFJvb20iLCJwcm9wcyIsInJvb21JZCIsIm1lbWJlcnNoaXAiLCJnZXRNeU1lbWJlcnNoaXAiLCJzZXRTdGF0ZSIsImxvYWRpbmciLCJsb2FkTWVtYmVyc0lmTmVlZGVkIiwiZXgiLCJtZW1iZXJzIiwiZmlsdGVyZWRKb2luZWRNZW1iZXJzIiwiX2ZpbHRlck1lbWJlcnMiLCJmaWx0ZXJlZEludml0ZWRNZW1iZXJzIiwidHJ1bmNhdGVBdEpvaW5lZCIsInRydW5jYXRlQXRJbnZpdGVkIiwic2VhcmNoUXVlcnkiLCJldmVudCIsInVzZXIiLCJ0aWxlIiwicmVmcyIsInVzZXJJZCIsIm9sZE1lbWJlcnNoaXAiLCJldiIsInN0YXRlIiwibWVtYmVyIiwiZ2V0Um9vbUlkIiwiZ2V0VHlwZSIsIl91cGRhdGVMaXN0Tm93IiwibmV3U3RhdGUiLCJnZXRNZW1iZXJzV2l0aFVzZXIiLCJhbGxNZW1iZXJzIiwiT2JqZWN0IiwidmFsdWVzIiwiY3VycmVudFN0YXRlIiwiZm9yRWFjaCIsImdldFVzZXIiLCJDb25mZXJlbmNlSGFuZGxlciIsIkNhbGxIYW5kbGVyIiwiZ2V0Q29uZmVyZW5jZUhhbmRsZXIiLCJmaWx0ZXJlZEFuZFNvcnRlZE1lbWJlcnMiLCJmaWx0ZXIiLCJtIiwiaXNDb25mZXJlbmNlVXNlciIsInNvcnQiLCJtZW1iZXJTb3J0IiwiX2NyZWF0ZU92ZXJmbG93VGlsZUpvaW5lZCIsIm92ZXJmbG93Q291bnQiLCJ0b3RhbENvdW50IiwiX2NyZWF0ZU92ZXJmbG93VGlsZSIsIl9zaG93TW9yZUpvaW5lZE1lbWJlckxpc3QiLCJfY3JlYXRlT3ZlcmZsb3dUaWxlSW52aXRlZCIsIl9zaG93TW9yZUludml0ZWRNZW1iZXJMaXN0Iiwib25DbGljayIsIkVudGl0eVRpbGUiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJCYXNlQXZhdGFyIiwidGV4dCIsImNvdW50IiwicmVxdWlyZSIsIm1lbWJlclN0cmluZyIsInUiLCJuYW1lIiwicG93ZXJMZXZlbCIsImxhc3RBY3RpdmVBZ28iLCJnZXRMYXN0QWN0aXZlVHMiLCJjdXJyZW50bHlBY3RpdmUiLCJwcmVzZW5jZSIsIm1lbWJlckEiLCJtZW1iZXJCIiwidXNlckEiLCJ1c2VyQiIsImNvbnZlcnRQcmVzZW5jZSIsInAiLCJwcmVzZW5jZUluZGV4Iiwib3JkZXIiLCJpZHgiLCJpbmRleE9mIiwibGVuZ3RoIiwiaWR4QSIsImlkeEIiLCJuYW1lQSIsInN1YnN0ciIsInJlcGxhY2UiLCJuYW1lQiIsImxvY2FsZUNvbXBhcmUiLCJpZ25vcmVQdW5jdHVhdGlvbiIsInNlbnNpdGl2aXR5Iiwib25TZWFyY2hRdWVyeUNoYW5nZWQiLCJfb25QZW5kaW5nM3BpZEludml0ZUNsaWNrIiwiaW52aXRlRXZlbnQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInF1ZXJ5IiwidG9Mb3dlckNhc2UiLCJtYXRjaGVzTmFtZSIsIm1hdGNoZXNJZCIsIl9nZXRQZW5kaW5nM1BpZEludml0ZXMiLCJnZXRTdGF0ZUV2ZW50cyIsImUiLCJtZW1iZXJFdmVudCIsImdldEludml0ZUZvclRocmVlUGlkVG9rZW4iLCJnZXRTdGF0ZUtleSIsIl9tYWtlTWVtYmVyVGlsZXMiLCJNZW1iZXJUaWxlIiwibWFwIiwiZ2V0Q29udGVudCIsImRpc3BsYXlfbmFtZSIsIl9nZXRDaGlsZHJlbkpvaW5lZCIsInN0YXJ0IiwiZW5kIiwic2xpY2UiLCJfZ2V0Q2hpbGRDb3VudEpvaW5lZCIsIl9nZXRDaGlsZHJlbkludml0ZWQiLCJ0YXJnZXRzIiwiY29uY2F0IiwiX2dldENoaWxkQ291bnRJbnZpdGVkIiwicmVuZGVyIiwiU3Bpbm5lciIsIlNlYXJjaEJveCIsIlRydW5jYXRlZExpc3QiLCJpbnZpdGVCdXR0b24iLCJjYW5JbnZpdGUiLCJwbEV2ZW50IiwibWUiLCJnZXRNZW1iZXIiLCJnZXRVc2VySWQiLCJjb250ZW50IiwiaW52aXRlIiwiQWNjZXNzaWJsZUJ1dHRvbiIsIm9uSW52aXRlQnV0dG9uQ2xpY2siLCJpbnZpdGVkSGVhZGVyIiwiaW52aXRlZFNlY3Rpb24iLCJpc0d1ZXN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBOEJBLE1BQU1BLHdCQUF3QixHQUFHLEVBQWpDO0FBQ0EsTUFBTUMsd0JBQXdCLEdBQUcsQ0FBakM7QUFDQSxNQUFNQyxtQkFBbUIsR0FBRyxHQUE1QixDLENBRUE7QUFDQTs7QUFDQSxNQUFNQyxVQUFVLEdBQUcsMENBQW5COztlQUVlLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLFlBRGU7QUFHNUJDLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFVBQU1DLEdBQUcsR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFFBQUlGLEdBQUcsQ0FBQ0cseUJBQUosRUFBSixFQUFxQztBQUNqQztBQUNBLGFBQU8sS0FBS0MsZ0JBQUwsQ0FBc0IsRUFBdEIsQ0FBUDtBQUNILEtBSEQsTUFHTztBQUNILGFBQU8sS0FBS0EsZ0JBQUwsQ0FBc0IsS0FBS0MsV0FBTCxFQUF0QixDQUFQO0FBQ0g7QUFDSixHQVgyQjtBQWE1QjtBQUNBQyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7O0FBQ0EsVUFBTVAsR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsUUFBSUYsR0FBRyxDQUFDRyx5QkFBSixFQUFKLEVBQXFDO0FBQ2pDLFdBQUtLLHVDQUFMOztBQUNBUixNQUFBQSxHQUFHLENBQUNTLEVBQUosQ0FBTyxtQkFBUCxFQUE0QixLQUFLQyxjQUFqQztBQUNILEtBSEQsTUFHTztBQUNILFdBQUtDLHdCQUFMO0FBQ0g7O0FBQ0RYLElBQUFBLEdBQUcsQ0FBQ1MsRUFBSixDQUFPLE1BQVAsRUFBZSxLQUFLRyxNQUFwQixFQVRrQyxDQVNMOztBQUM3QixVQUFNQyxxQkFBcUIsR0FBR0MsbUJBQVVaLEdBQVYsR0FBZ0IsMkJBQWhCLENBQTlCOztBQUNBLFVBQU1hLEtBQUssR0FBR2QsaUNBQWdCQyxHQUFoQixHQUFzQmMsT0FBcEM7O0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixJQUFyQjs7QUFDQSxRQUFJSixxQkFBcUIsSUFBSUEscUJBQXFCLENBQUNFLEtBQUQsQ0FBckIsS0FBaUNHLFNBQTlELEVBQXlFO0FBQ3JFLFdBQUtELGFBQUwsR0FBcUJKLHFCQUFxQixDQUFDRSxLQUFELENBQTFDO0FBQ0g7QUFDSixHQTlCMkI7QUFnQzVCSixFQUFBQSx3QkFBd0IsRUFBRSxZQUFXO0FBQ2pDLFVBQU1YLEdBQUcsR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBRixJQUFBQSxHQUFHLENBQUNTLEVBQUosQ0FBTyxtQkFBUCxFQUE0QixLQUFLVSxpQkFBakM7QUFDQW5CLElBQUFBLEdBQUcsQ0FBQ1MsRUFBSixDQUFPLGlCQUFQLEVBQTBCLEtBQUtXLGdCQUEvQjtBQUNBcEIsSUFBQUEsR0FBRyxDQUFDUyxFQUFKLENBQU8sa0JBQVAsRUFBMkIsS0FBS1ksZ0JBQWhDLEVBSmlDLENBS2pDO0FBQ0E7QUFDQTs7QUFDQXJCLElBQUFBLEdBQUcsQ0FBQ1MsRUFBSixDQUFPLHFCQUFQLEVBQThCLEtBQUthLG9CQUFuQztBQUNBdEIsSUFBQUEsR0FBRyxDQUFDUyxFQUFKLENBQU8sZUFBUCxFQUF3QixLQUFLYSxvQkFBN0I7QUFDQXRCLElBQUFBLEdBQUcsQ0FBQ1MsRUFBSixDQUFPLHNCQUFQLEVBQStCLEtBQUthLG9CQUFwQyxFQVZpQyxDQVdqQztBQUNILEdBNUMyQjtBQThDNUJDLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0IsU0FBS2hCLFFBQUwsR0FBZ0IsS0FBaEI7O0FBQ0EsVUFBTVAsR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsUUFBSUYsR0FBSixFQUFTO0FBQ0xBLE1BQUFBLEdBQUcsQ0FBQ3dCLGNBQUosQ0FBbUIsbUJBQW5CLEVBQXdDLEtBQUtMLGlCQUE3QztBQUNBbkIsTUFBQUEsR0FBRyxDQUFDd0IsY0FBSixDQUFtQixpQkFBbkIsRUFBc0MsS0FBS0osZ0JBQTNDO0FBQ0FwQixNQUFBQSxHQUFHLENBQUN3QixjQUFKLENBQW1CLG1CQUFuQixFQUF3QyxLQUFLZCxjQUE3QztBQUNBVixNQUFBQSxHQUFHLENBQUN3QixjQUFKLENBQW1CLGtCQUFuQixFQUF1QyxLQUFLSCxnQkFBNUM7QUFDQXJCLE1BQUFBLEdBQUcsQ0FBQ3dCLGNBQUosQ0FBbUIsTUFBbkIsRUFBMkIsS0FBS1osTUFBaEM7QUFDQVosTUFBQUEsR0FBRyxDQUFDd0IsY0FBSixDQUFtQixxQkFBbkIsRUFBMEMsS0FBS0Ysb0JBQS9DO0FBQ0F0QixNQUFBQSxHQUFHLENBQUN3QixjQUFKLENBQW1CLGVBQW5CLEVBQW9DLEtBQUtGLG9CQUF6QztBQUNBdEIsTUFBQUEsR0FBRyxDQUFDd0IsY0FBSixDQUFtQixzQkFBbkIsRUFBMkMsS0FBS0Ysb0JBQWhEO0FBQ0gsS0FaNEIsQ0FjN0I7OztBQUNBLFNBQUtHLFdBQUwsQ0FBaUJDLGlCQUFqQjtBQUNILEdBOUQyQjs7QUFnRTVCOzs7OztBQUtBbEIsRUFBQUEsdUNBQXVDLEVBQUUsa0JBQWlCO0FBQ3RELFVBQU1SLEdBQUcsR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFFBQUlGLEdBQUcsQ0FBQ0cseUJBQUosRUFBSixFQUFxQztBQUNqQyxZQUFNSCxHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxZQUFNeUIsSUFBSSxHQUFHM0IsR0FBRyxDQUFDNEIsT0FBSixDQUFZLEtBQUtDLEtBQUwsQ0FBV0MsTUFBdkIsQ0FBYjtBQUNBLFlBQU1DLFVBQVUsR0FBR0osSUFBSSxJQUFJQSxJQUFJLENBQUNLLGVBQUwsRUFBM0I7O0FBQ0EsVUFBSUQsVUFBVSxLQUFLLE1BQW5CLEVBQTJCO0FBQ3ZCLGFBQUtFLFFBQUwsQ0FBYztBQUFDQyxVQUFBQSxPQUFPLEVBQUU7QUFBVixTQUFkOztBQUNBLFlBQUk7QUFDQSxnQkFBTVAsSUFBSSxDQUFDUSxtQkFBTCxFQUFOO0FBQ0gsU0FGRCxDQUVFLE9BQU9DLEVBQVAsRUFBVztBQUFDO0FBQWlDOztBQUMvQyxZQUFJLEtBQUs3QixRQUFULEVBQW1CO0FBQ2YsZUFBSzBCLFFBQUwsQ0FBYyxLQUFLN0IsZ0JBQUwsQ0FBc0IsS0FBS0MsV0FBTCxFQUF0QixDQUFkOztBQUNBLGVBQUtNLHdCQUFMO0FBQ0g7QUFDSixPQVRELE1BU08sSUFBSW9CLFVBQVUsS0FBSyxRQUFuQixFQUE2QjtBQUNoQztBQUNBLGFBQUtFLFFBQUwsQ0FBYyxLQUFLN0IsZ0JBQUwsQ0FBc0IsS0FBS0MsV0FBTCxFQUF0QixDQUFkO0FBQ0g7QUFDSjtBQUNKLEdBekYyQjtBQTJGNUJELEVBQUFBLGdCQUFnQixFQUFFLFVBQVNpQyxPQUFULEVBQWtCO0FBQ2hDO0FBQ0E7QUFDQSxXQUFPO0FBQ0hILE1BQUFBLE9BQU8sRUFBRSxLQUROO0FBRUhHLE1BQUFBLE9BQU8sRUFBRUEsT0FGTjtBQUdIQyxNQUFBQSxxQkFBcUIsRUFBRSxLQUFLQyxjQUFMLENBQW9CRixPQUFwQixFQUE2QixNQUE3QixDQUhwQjtBQUlIRyxNQUFBQSxzQkFBc0IsRUFBRSxLQUFLRCxjQUFMLENBQW9CRixPQUFwQixFQUE2QixRQUE3QixDQUpyQjtBQU1IO0FBQ0E7QUFDQUksTUFBQUEsZ0JBQWdCLEVBQUUvQyx3QkFSZjtBQVNIZ0QsTUFBQUEsaUJBQWlCLEVBQUUvQyx3QkFUaEI7QUFVSGdELE1BQUFBLFdBQVcsRUFBRTtBQVZWLEtBQVA7QUFZSCxHQTFHMkI7O0FBNEc1QnJCLEVBQUFBLG9CQUFvQixDQUFDc0IsS0FBRCxFQUFRQyxJQUFSLEVBQWM7QUFDOUI7QUFDQTtBQUNBO0FBQ0EsVUFBTUMsSUFBSSxHQUFHLEtBQUtDLElBQUwsQ0FBVUYsSUFBSSxDQUFDRyxNQUFmLENBQWIsQ0FKOEIsQ0FLOUI7O0FBQ0EsUUFBSUYsSUFBSixFQUFVO0FBQ04sV0FBS3JCLFdBQUwsR0FETSxDQUNjOztBQUN2QjtBQUNKLEdBckgyQjs7QUF1SDVCYixFQUFBQSxNQUFNLEVBQUUsVUFBU2UsSUFBVCxFQUFlO0FBQ25CLFFBQUlBLElBQUksQ0FBQ0csTUFBTCxLQUFnQixLQUFLRCxLQUFMLENBQVdDLE1BQS9CLEVBQXVDO0FBQ25DO0FBQ0gsS0FIa0IsQ0FJbkI7QUFDQTtBQUNBOzs7QUFDQSxTQUFLdEIsdUNBQUw7QUFDSCxHQS9IMkI7QUFpSTVCRSxFQUFBQSxjQUFjLEVBQUUsVUFBU2lCLElBQVQsRUFBZUksVUFBZixFQUEyQmtCLGFBQTNCLEVBQTBDO0FBQ3RELFFBQUl0QixJQUFJLENBQUNHLE1BQUwsS0FBZ0IsS0FBS0QsS0FBTCxDQUFXQyxNQUEzQixJQUFxQ0MsVUFBVSxLQUFLLE1BQXhELEVBQWdFO0FBQzVELFdBQUt2Qix1Q0FBTDtBQUNIO0FBQ0osR0FySTJCO0FBdUk1QlcsRUFBQUEsaUJBQWlCLEVBQUUsVUFBUytCLEVBQVQsRUFBYUMsS0FBYixFQUFvQkMsTUFBcEIsRUFBNEI7QUFDM0MsUUFBSUEsTUFBTSxDQUFDdEIsTUFBUCxLQUFrQixLQUFLRCxLQUFMLENBQVdDLE1BQWpDLEVBQXlDO0FBQ3JDO0FBQ0g7O0FBQ0QsU0FBS0wsV0FBTDtBQUNILEdBNUkyQjtBQThJNUJMLEVBQUFBLGdCQUFnQixFQUFFLFVBQVM4QixFQUFULEVBQWFFLE1BQWIsRUFBcUI7QUFDbkMsUUFBSUEsTUFBTSxDQUFDdEIsTUFBUCxLQUFrQixLQUFLRCxLQUFMLENBQVdDLE1BQWpDLEVBQXlDO0FBQ3JDO0FBQ0g7O0FBQ0QsU0FBS0wsV0FBTDtBQUNILEdBbkoyQjtBQXFKNUJKLEVBQUFBLGdCQUFnQixFQUFFLFVBQVN1QixLQUFULEVBQWdCTyxLQUFoQixFQUF1QjtBQUNyQyxRQUFJUCxLQUFLLENBQUNTLFNBQU4sT0FBc0IsS0FBS3hCLEtBQUwsQ0FBV0MsTUFBakMsSUFDQWMsS0FBSyxDQUFDVSxPQUFOLE9BQW9CLDJCQUR4QixFQUNxRDtBQUNqRCxXQUFLN0IsV0FBTDtBQUNIO0FBQ0osR0ExSjJCO0FBNEo1QkEsRUFBQUEsV0FBVyxFQUFFLDhCQUFrQixZQUFXO0FBQ3RDLFNBQUs4QixjQUFMO0FBQ0gsR0FGWSxFQUVWLEdBRlUsQ0E1SmU7QUFnSzVCQSxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QjtBQUNBLFVBQU1DLFFBQVEsR0FBRztBQUNidEIsTUFBQUEsT0FBTyxFQUFFLEtBREk7QUFFYkcsTUFBQUEsT0FBTyxFQUFFLEtBQUtoQyxXQUFMO0FBRkksS0FBakI7QUFJQW1ELElBQUFBLFFBQVEsQ0FBQ2xCLHFCQUFULEdBQWlDLEtBQUtDLGNBQUwsQ0FBb0JpQixRQUFRLENBQUNuQixPQUE3QixFQUFzQyxNQUF0QyxFQUE4QyxLQUFLYyxLQUFMLENBQVdSLFdBQXpELENBQWpDO0FBQ0FhLElBQUFBLFFBQVEsQ0FBQ2hCLHNCQUFULEdBQWtDLEtBQUtELGNBQUwsQ0FBb0JpQixRQUFRLENBQUNuQixPQUE3QixFQUFzQyxRQUF0QyxFQUFnRCxLQUFLYyxLQUFMLENBQVdSLFdBQTNELENBQWxDO0FBQ0EsU0FBS1YsUUFBTCxDQUFjdUIsUUFBZDtBQUNILEdBeksyQjtBQTJLNUJDLEVBQUFBLGtCQUFrQixFQUFFLFlBQVc7QUFDM0IsUUFBSSxDQUFDLEtBQUs1QixLQUFMLENBQVdDLE1BQWhCLEVBQXdCLE9BQU8sRUFBUDs7QUFDeEIsVUFBTTlCLEdBQUcsR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFVBQU15QixJQUFJLEdBQUczQixHQUFHLENBQUM0QixPQUFKLENBQVksS0FBS0MsS0FBTCxDQUFXQyxNQUF2QixDQUFiO0FBQ0EsUUFBSSxDQUFDSCxJQUFMLEVBQVcsT0FBTyxFQUFQO0FBRVgsVUFBTStCLFVBQVUsR0FBR0MsTUFBTSxDQUFDQyxNQUFQLENBQWNqQyxJQUFJLENBQUNrQyxZQUFMLENBQWtCeEIsT0FBaEMsQ0FBbkI7QUFFQXFCLElBQUFBLFVBQVUsQ0FBQ0ksT0FBWCxDQUFtQixVQUFTVixNQUFULEVBQWlCO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBLFVBQUlBLE1BQU0sQ0FBQ1AsSUFBUCxLQUFnQixJQUFwQixFQUEwQjtBQUN0Qk8sUUFBQUEsTUFBTSxDQUFDUCxJQUFQLEdBQWM3QyxHQUFHLENBQUMrRCxPQUFKLENBQVlYLE1BQU0sQ0FBQ0osTUFBbkIsQ0FBZDtBQUNILE9BTitCLENBUWhDO0FBQ0E7O0FBQ0gsS0FWRDtBQVlBLFdBQU9VLFVBQVA7QUFDSCxHQWhNMkI7QUFrTTVCckQsRUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEIsVUFBTTJELGlCQUFpQixHQUFHQyxxQkFBWUMsb0JBQVosRUFBMUI7O0FBRUEsVUFBTVIsVUFBVSxHQUFHLEtBQUtELGtCQUFMLEVBQW5CO0FBQ0EsVUFBTVUsd0JBQXdCLEdBQUdULFVBQVUsQ0FBQ1UsTUFBWCxDQUFtQkMsQ0FBRCxJQUFPO0FBQ3RELGFBQU8sQ0FDSEEsQ0FBQyxDQUFDdEMsVUFBRixLQUFpQixNQUFqQixJQUEyQnNDLENBQUMsQ0FBQ3RDLFVBQUYsS0FBaUIsUUFEekMsTUFHSCxDQUFDaUMsaUJBQUQsSUFDQ0EsaUJBQWlCLElBQUksQ0FBQ0EsaUJBQWlCLENBQUNNLGdCQUFsQixDQUFtQ0QsQ0FBQyxDQUFDckIsTUFBckMsQ0FKcEIsQ0FBUDtBQU1ILEtBUGdDLENBQWpDO0FBUUFtQixJQUFBQSx3QkFBd0IsQ0FBQ0ksSUFBekIsQ0FBOEIsS0FBS0MsVUFBbkM7QUFDQSxXQUFPTCx3QkFBUDtBQUNILEdBaE4yQjtBQWtONUJNLEVBQUFBLHlCQUF5QixFQUFFLFVBQVNDLGFBQVQsRUFBd0JDLFVBQXhCLEVBQW9DO0FBQzNELFdBQU8sS0FBS0MsbUJBQUwsQ0FBeUJGLGFBQXpCLEVBQXdDQyxVQUF4QyxFQUFvRCxLQUFLRSx5QkFBekQsQ0FBUDtBQUNILEdBcE4yQjtBQXNONUJDLEVBQUFBLDBCQUEwQixFQUFFLFVBQVNKLGFBQVQsRUFBd0JDLFVBQXhCLEVBQW9DO0FBQzVELFdBQU8sS0FBS0MsbUJBQUwsQ0FBeUJGLGFBQXpCLEVBQXdDQyxVQUF4QyxFQUFvRCxLQUFLSSwwQkFBekQsQ0FBUDtBQUNILEdBeE4yQjtBQTBONUJILEVBQUFBLG1CQUFtQixFQUFFLFVBQVNGLGFBQVQsRUFBd0JDLFVBQXhCLEVBQW9DSyxPQUFwQyxFQUE2QztBQUM5RDtBQUNBLFVBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFuQjtBQUNBLFVBQU1DLFVBQVUsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjtBQUNBLFVBQU1FLElBQUksR0FBRyx5QkFBRyx5QkFBSCxFQUE4QjtBQUFFQyxNQUFBQSxLQUFLLEVBQUVaO0FBQVQsS0FBOUIsQ0FBYjtBQUNBLFdBQ0ksNkJBQUMsVUFBRDtBQUFZLE1BQUEsU0FBUyxFQUFDLHdCQUF0QjtBQUErQyxNQUFBLFNBQVMsRUFDcEQsNkJBQUMsVUFBRDtBQUFZLFFBQUEsR0FBRyxFQUFFYSxPQUFPLENBQUMsa0NBQUQsQ0FBeEI7QUFBOEQsUUFBQSxJQUFJLEVBQUMsS0FBbkU7QUFBeUUsUUFBQSxLQUFLLEVBQUUsRUFBaEY7QUFBb0YsUUFBQSxNQUFNLEVBQUU7QUFBNUYsUUFESjtBQUVFLE1BQUEsSUFBSSxFQUFFRixJQUZSO0FBRWMsTUFBQSxhQUFhLEVBQUMsUUFGNUI7QUFFcUMsTUFBQSxlQUFlLEVBQUUsSUFGdEQ7QUFHQSxNQUFBLE9BQU8sRUFBRUw7QUFIVCxNQURKO0FBTUgsR0FyTzJCO0FBdU81QkgsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLNUMsUUFBTCxDQUFjO0FBQ1ZRLE1BQUFBLGdCQUFnQixFQUFFLEtBQUtVLEtBQUwsQ0FBV1YsZ0JBQVgsR0FBOEI3QztBQUR0QyxLQUFkO0FBR0gsR0EzTzJCO0FBNk81Qm1GLEVBQUFBLDBCQUEwQixFQUFFLFlBQVc7QUFDbkMsU0FBSzlDLFFBQUwsQ0FBYztBQUNWUyxNQUFBQSxpQkFBaUIsRUFBRSxLQUFLUyxLQUFMLENBQVdULGlCQUFYLEdBQStCOUM7QUFEeEMsS0FBZDtBQUdILEdBalAyQjtBQW1QNUI0RixFQUFBQSxZQUFZLEVBQUUsVUFBU3BDLE1BQVQsRUFBaUI7QUFDM0IsUUFBSSxDQUFDQSxNQUFMLEVBQWE7QUFDVCxhQUFPLFFBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxZQUFNcUMsQ0FBQyxHQUFHckMsTUFBTSxDQUFDUCxJQUFqQjtBQUNBLGFBQU8sTUFBTU8sTUFBTSxDQUFDc0MsSUFBYixHQUFvQixJQUFwQixHQUEyQnRDLE1BQU0sQ0FBQ3VDLFVBQWxDLEdBQStDLElBQS9DLElBQXVERixDQUFDLEdBQUdBLENBQUMsQ0FBQ0csYUFBTCxHQUFxQixRQUE3RSxJQUF5RixJQUF6RixJQUFpR0gsQ0FBQyxHQUFHQSxDQUFDLENBQUNJLGVBQUYsRUFBSCxHQUF5QixRQUEzSCxJQUF1SSxJQUF2SSxJQUErSUosQ0FBQyxHQUFHQSxDQUFDLENBQUNLLGVBQUwsR0FBdUIsUUFBdkssSUFBbUwsSUFBbkwsSUFBMkxMLENBQUMsR0FBR0EsQ0FBQyxDQUFDTSxRQUFMLEdBQWdCLFFBQTVNLElBQXdOLEdBQS9OO0FBQ0g7QUFDSixHQTFQMkI7QUE0UDVCO0FBQ0E7QUFDQTtBQUNBdkIsRUFBQUEsVUFBVSxFQUFFLFVBQVN3QixPQUFULEVBQWtCQyxPQUFsQixFQUEyQjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQSxVQUFNQyxLQUFLLEdBQUdGLE9BQU8sQ0FBQ25ELElBQXRCO0FBQ0EsVUFBTXNELEtBQUssR0FBR0YsT0FBTyxDQUFDcEQsSUFBdEIsQ0FWbUMsQ0FZbkM7QUFDQTs7QUFFQSxRQUFJLENBQUNxRCxLQUFELElBQVUsQ0FBQ0MsS0FBZixFQUFzQixPQUFPLENBQVA7QUFDdEIsUUFBSUQsS0FBSyxJQUFJLENBQUNDLEtBQWQsRUFBcUIsT0FBTyxDQUFDLENBQVI7QUFDckIsUUFBSSxDQUFDRCxLQUFELElBQVVDLEtBQWQsRUFBcUIsT0FBTyxDQUFQLENBakJjLENBbUJuQzs7QUFDQSxRQUFJLEtBQUtsRixhQUFULEVBQXdCO0FBQ3BCLFlBQU1tRixlQUFlLEdBQUlDLENBQUQsSUFBT0EsQ0FBQyxLQUFLLGFBQU4sR0FBc0IsUUFBdEIsR0FBaUNBLENBQWhFOztBQUNBLFlBQU1DLGFBQWEsR0FBR0QsQ0FBQyxJQUFJO0FBQ3ZCLGNBQU1FLEtBQUssR0FBRyxDQUFDLFFBQUQsRUFBVyxRQUFYLEVBQXFCLFNBQXJCLENBQWQ7QUFDQSxjQUFNQyxHQUFHLEdBQUdELEtBQUssQ0FBQ0UsT0FBTixDQUFjTCxlQUFlLENBQUNDLENBQUQsQ0FBN0IsQ0FBWjtBQUNBLGVBQU9HLEdBQUcsS0FBSyxDQUFDLENBQVQsR0FBYUQsS0FBSyxDQUFDRyxNQUFuQixHQUE0QkYsR0FBbkMsQ0FIdUIsQ0FHaUI7QUFDM0MsT0FKRDs7QUFNQSxZQUFNRyxJQUFJLEdBQUdMLGFBQWEsQ0FBQ0osS0FBSyxDQUFDSixlQUFOLEdBQXdCLFFBQXhCLEdBQW1DSSxLQUFLLENBQUNILFFBQTFDLENBQTFCO0FBQ0EsWUFBTWEsSUFBSSxHQUFHTixhQUFhLENBQUNILEtBQUssQ0FBQ0wsZUFBTixHQUF3QixRQUF4QixHQUFtQ0ssS0FBSyxDQUFDSixRQUExQyxDQUExQixDQVRvQixDQVVwQjs7QUFDQSxVQUFJWSxJQUFJLEtBQUtDLElBQWIsRUFBbUI7QUFDZjtBQUNBLGVBQU9ELElBQUksR0FBR0MsSUFBZDtBQUNIO0FBQ0osS0FuQ2tDLENBcUNuQzs7O0FBQ0EsUUFBSVosT0FBTyxDQUFDTCxVQUFSLEtBQXVCTSxPQUFPLENBQUNOLFVBQW5DLEVBQStDO0FBQzNDO0FBQ0EsYUFBT00sT0FBTyxDQUFDTixVQUFSLEdBQXFCSyxPQUFPLENBQUNMLFVBQXBDO0FBQ0gsS0F6Q2tDLENBMkNuQzs7O0FBQ0EsUUFBSSxLQUFLMUUsYUFBTCxJQUFzQmlGLEtBQUssQ0FBQ0wsZUFBTixPQUE0Qk0sS0FBSyxDQUFDTixlQUFOLEVBQXRELEVBQStFO0FBQzNFO0FBQ0EsYUFBT00sS0FBSyxDQUFDTixlQUFOLEtBQTBCSyxLQUFLLENBQUNMLGVBQU4sRUFBakM7QUFDSCxLQS9Da0MsQ0FpRG5DOzs7QUFDQSxVQUFNZ0IsS0FBSyxHQUFHLENBQUNiLE9BQU8sQ0FBQ04sSUFBUixDQUFhLENBQWIsTUFBb0IsR0FBcEIsR0FBMEJNLE9BQU8sQ0FBQ04sSUFBUixDQUFhb0IsTUFBYixDQUFvQixDQUFwQixDQUExQixHQUFtRGQsT0FBTyxDQUFDTixJQUE1RCxFQUFrRXFCLE9BQWxFLENBQTBFbEgsVUFBMUUsRUFBc0YsRUFBdEYsQ0FBZDtBQUNBLFVBQU1tSCxLQUFLLEdBQUcsQ0FBQ2YsT0FBTyxDQUFDUCxJQUFSLENBQWEsQ0FBYixNQUFvQixHQUFwQixHQUEwQk8sT0FBTyxDQUFDUCxJQUFSLENBQWFvQixNQUFiLENBQW9CLENBQXBCLENBQTFCLEdBQW1EYixPQUFPLENBQUNQLElBQTVELEVBQWtFcUIsT0FBbEUsQ0FBMEVsSCxVQUExRSxFQUFzRixFQUF0RixDQUFkLENBbkRtQyxDQW9EbkM7O0FBQ0EsV0FBT2dILEtBQUssQ0FBQ0ksYUFBTixDQUFvQkQsS0FBcEIsRUFBMkI7QUFDOUJFLE1BQUFBLGlCQUFpQixFQUFFLElBRFc7QUFFOUJDLE1BQUFBLFdBQVcsRUFBRTtBQUZpQixLQUEzQixDQUFQO0FBSUgsR0F4VDJCO0FBMFQ1QkMsRUFBQUEsb0JBQW9CLEVBQUUsVUFBU3pFLFdBQVQsRUFBc0I7QUFDeEMsU0FBS1YsUUFBTCxDQUFjO0FBQ1ZVLE1BQUFBLFdBRFU7QUFFVkwsTUFBQUEscUJBQXFCLEVBQUUsS0FBS0MsY0FBTCxDQUFvQixLQUFLWSxLQUFMLENBQVdkLE9BQS9CLEVBQXdDLE1BQXhDLEVBQWdETSxXQUFoRCxDQUZiO0FBR1ZILE1BQUFBLHNCQUFzQixFQUFFLEtBQUtELGNBQUwsQ0FBb0IsS0FBS1ksS0FBTCxDQUFXZCxPQUEvQixFQUF3QyxRQUF4QyxFQUFrRE0sV0FBbEQ7QUFIZCxLQUFkO0FBS0gsR0FoVTJCO0FBa1U1QjBFLEVBQUFBLHlCQUF5QixFQUFFLFVBQVNDLFdBQVQsRUFBc0I7QUFDN0NDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLGtCQURDO0FBRVQ3RSxNQUFBQSxLQUFLLEVBQUUwRTtBQUZFLEtBQWI7QUFJSCxHQXZVMkI7QUF5VTVCL0UsRUFBQUEsY0FBYyxFQUFFLFVBQVNGLE9BQVQsRUFBa0JOLFVBQWxCLEVBQThCMkYsS0FBOUIsRUFBcUM7QUFDakQsV0FBT3JGLE9BQU8sQ0FBQytCLE1BQVIsQ0FBZ0JDLENBQUQsSUFBTztBQUN6QixVQUFJcUQsS0FBSixFQUFXO0FBQ1BBLFFBQUFBLEtBQUssR0FBR0EsS0FBSyxDQUFDQyxXQUFOLEVBQVI7QUFDQSxjQUFNQyxXQUFXLEdBQUd2RCxDQUFDLENBQUNxQixJQUFGLENBQU9pQyxXQUFQLEdBQXFCbEIsT0FBckIsQ0FBNkJpQixLQUE3QixNQUF3QyxDQUFDLENBQTdEO0FBQ0EsY0FBTUcsU0FBUyxHQUFHeEQsQ0FBQyxDQUFDckIsTUFBRixDQUFTMkUsV0FBVCxHQUF1QmxCLE9BQXZCLENBQStCaUIsS0FBL0IsTUFBMEMsQ0FBQyxDQUE3RDs7QUFFQSxZQUFJLENBQUNFLFdBQUQsSUFBZ0IsQ0FBQ0MsU0FBckIsRUFBZ0M7QUFDNUIsaUJBQU8sS0FBUDtBQUNIO0FBQ0o7O0FBRUQsYUFBT3hELENBQUMsQ0FBQ3RDLFVBQUYsS0FBaUJBLFVBQXhCO0FBQ0gsS0FaTSxDQUFQO0FBYUgsR0F2VjJCO0FBeVY1QitGLEVBQUFBLHNCQUFzQixFQUFFLFlBQVc7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFNbkcsSUFBSSxHQUFHMUIsaUNBQWdCQyxHQUFoQixHQUFzQjBCLE9BQXRCLENBQThCLEtBQUtDLEtBQUwsQ0FBV0MsTUFBekMsQ0FBYjs7QUFFQSxRQUFJSCxJQUFKLEVBQVU7QUFDTixhQUFPQSxJQUFJLENBQUNrQyxZQUFMLENBQWtCa0UsY0FBbEIsQ0FBaUMsMkJBQWpDLEVBQThEM0QsTUFBOUQsQ0FBcUUsVUFBUzRELENBQVQsRUFBWTtBQUNwRixZQUFJLENBQUMsbUNBQWtCQSxDQUFsQixDQUFMLEVBQTJCLE9BQU8sS0FBUCxDQUR5RCxDQUdwRjtBQUNBOztBQUNBLGNBQU1DLFdBQVcsR0FBR3RHLElBQUksQ0FBQ2tDLFlBQUwsQ0FBa0JxRSx5QkFBbEIsQ0FBNENGLENBQUMsQ0FBQ0csV0FBRixFQUE1QyxDQUFwQjtBQUNBLFlBQUlGLFdBQUosRUFBaUIsT0FBTyxLQUFQO0FBQ2pCLGVBQU8sSUFBUDtBQUNILE9BUk0sQ0FBUDtBQVNIO0FBQ0osR0EzVzJCO0FBNlc1QkcsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBUy9GLE9BQVQsRUFBa0I7QUFDaEMsVUFBTWdHLFVBQVUsR0FBR25ELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBbkI7QUFDQSxVQUFNRixVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBbkI7QUFFQSxXQUFPOUMsT0FBTyxDQUFDaUcsR0FBUixDQUFhakUsQ0FBRCxJQUFPO0FBQ3RCLFVBQUlBLENBQUMsQ0FBQ3JCLE1BQU4sRUFBYztBQUNWO0FBQ0EsZUFBTyw2QkFBQyxVQUFEO0FBQVksVUFBQSxHQUFHLEVBQUVxQixDQUFDLENBQUNyQixNQUFuQjtBQUEyQixVQUFBLE1BQU0sRUFBRXFCLENBQW5DO0FBQXNDLFVBQUEsR0FBRyxFQUFFQSxDQUFDLENBQUNyQixNQUE3QztBQUFxRCxVQUFBLFlBQVksRUFBRSxLQUFLL0I7QUFBeEUsVUFBUDtBQUNILE9BSEQsTUFHTztBQUNIO0FBQ0EsZUFBTyw2QkFBQyxVQUFEO0FBQVksVUFBQSxHQUFHLEVBQUVvRCxDQUFDLENBQUM4RCxXQUFGLEVBQWpCO0FBQWtDLFVBQUEsSUFBSSxFQUFFOUQsQ0FBQyxDQUFDa0UsVUFBRixHQUFlQyxZQUF2RDtBQUFxRSxVQUFBLGVBQWUsRUFBRSxJQUF0RjtBQUNZLFVBQUEsT0FBTyxFQUFFLE1BQU0sS0FBS25CLHlCQUFMLENBQStCaEQsQ0FBL0I7QUFEM0IsVUFBUDtBQUVIO0FBQ0osS0FUTSxDQUFQO0FBVUgsR0EzWDJCO0FBNlg1Qm9FLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNDLEtBQVQsRUFBZ0JDLEdBQWhCLEVBQXFCO0FBQ3JDLFdBQU8sS0FBS1AsZ0JBQUwsQ0FBc0IsS0FBS2pGLEtBQUwsQ0FBV2IscUJBQVgsQ0FBaUNzRyxLQUFqQyxDQUF1Q0YsS0FBdkMsRUFBOENDLEdBQTlDLENBQXRCLENBQVA7QUFDSCxHQS9YMkI7QUFpWTVCRSxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFdBQU8sS0FBSzFGLEtBQUwsQ0FBV2IscUJBQVgsQ0FBaUNvRSxNQUF4QztBQUNILEdBblkyQjtBQXFZNUJvQyxFQUFBQSxtQkFBbUIsRUFBRSxVQUFTSixLQUFULEVBQWdCQyxHQUFoQixFQUFxQjtBQUN0QyxRQUFJSSxPQUFPLEdBQUcsS0FBSzVGLEtBQUwsQ0FBV1gsc0JBQXpCOztBQUNBLFFBQUltRyxHQUFHLEdBQUcsS0FBS3hGLEtBQUwsQ0FBV1gsc0JBQVgsQ0FBa0NrRSxNQUE1QyxFQUFvRDtBQUNoRHFDLE1BQUFBLE9BQU8sR0FBR0EsT0FBTyxDQUFDQyxNQUFSLENBQWUsS0FBS2xCLHNCQUFMLEVBQWYsQ0FBVjtBQUNIOztBQUVELFdBQU8sS0FBS00sZ0JBQUwsQ0FBc0JXLE9BQU8sQ0FBQ0gsS0FBUixDQUFjRixLQUFkLEVBQXFCQyxHQUFyQixDQUF0QixDQUFQO0FBQ0gsR0E1WTJCO0FBOFk1Qk0sRUFBQUEscUJBQXFCLEVBQUUsWUFBVztBQUM5QixXQUFPLEtBQUs5RixLQUFMLENBQVdYLHNCQUFYLENBQWtDa0UsTUFBbEMsR0FBMkMsQ0FBQyxLQUFLb0Isc0JBQUwsTUFBaUMsRUFBbEMsRUFBc0NwQixNQUF4RjtBQUNILEdBaFoyQjtBQWtaNUJ3QyxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFFBQUksS0FBSy9GLEtBQUwsQ0FBV2pCLE9BQWYsRUFBd0I7QUFDcEIsWUFBTWlILE9BQU8sR0FBR2pFLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxhQUFPO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUErQiw2QkFBQyxPQUFELE9BQS9CLENBQVA7QUFDSDs7QUFFRCxVQUFNaUUsU0FBUyxHQUFHbEUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFsQjtBQUNBLFVBQU1rRSxhQUFhLEdBQUduRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXRCOztBQUVBLFVBQU1uRixHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxVQUFNeUIsSUFBSSxHQUFHM0IsR0FBRyxDQUFDNEIsT0FBSixDQUFZLEtBQUtDLEtBQUwsQ0FBV0MsTUFBdkIsQ0FBYjtBQUNBLFFBQUl3SCxZQUFKOztBQUVBLFFBQUkzSCxJQUFJLElBQUlBLElBQUksQ0FBQ0ssZUFBTCxPQUEyQixNQUF2QyxFQUErQztBQUMzQztBQUNBLFVBQUl1SCxTQUFTLEdBQUcsSUFBaEI7QUFFQSxZQUFNQyxPQUFPLEdBQUc3SCxJQUFJLENBQUNrQyxZQUFMLENBQWtCa0UsY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQWhCO0FBQ0EsWUFBTTBCLEVBQUUsR0FBRzlILElBQUksQ0FBQytILFNBQUwsQ0FBZTFKLEdBQUcsQ0FBQzJKLFNBQUosRUFBZixDQUFYOztBQUNBLFVBQUlILE9BQU8sSUFBSUMsRUFBZixFQUFtQjtBQUNmLGNBQU1HLE9BQU8sR0FBR0osT0FBTyxDQUFDakIsVUFBUixFQUFoQjs7QUFDQSxZQUFJcUIsT0FBTyxJQUFJQSxPQUFPLENBQUNDLE1BQVIsR0FBaUJKLEVBQUUsQ0FBQzlELFVBQW5DLEVBQStDO0FBQzNDNEQsVUFBQUEsU0FBUyxHQUFHLEtBQVo7QUFDSDtBQUNKOztBQUVELFlBQU1PLGdCQUFnQixHQUFHNUUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBbUUsTUFBQUEsWUFBWSxHQUNSLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsU0FBUyxFQUFDLHNCQUE1QjtBQUFtRCxRQUFBLE9BQU8sRUFBRSxLQUFLUyxtQkFBakU7QUFBc0YsUUFBQSxRQUFRLEVBQUUsQ0FBQ1I7QUFBakcsU0FDSSwyQ0FBUSx5QkFBRyxxQkFBSCxDQUFSLENBREosQ0FESjtBQUlIOztBQUVELFFBQUlTLGFBQUo7QUFDQSxRQUFJQyxjQUFKOztBQUNBLFFBQUksS0FBS2hCLHFCQUFMLEtBQStCLENBQW5DLEVBQXNDO0FBQ2xDZSxNQUFBQSxhQUFhLEdBQUcseUNBQU0seUJBQUcsU0FBSCxDQUFOLENBQWhCO0FBQ0FDLE1BQUFBLGNBQWMsR0FBRyw2QkFBQyxhQUFEO0FBQWUsUUFBQSxTQUFTLEVBQUMsNkNBQXpCO0FBQXVFLFFBQUEsVUFBVSxFQUFFLEtBQUs5RyxLQUFMLENBQVdULGlCQUE5RjtBQUNMLFFBQUEscUJBQXFCLEVBQUUsS0FBS29DLDBCQUR2QjtBQUVMLFFBQUEsV0FBVyxFQUFFLEtBQUtnRSxtQkFGYjtBQUdMLFFBQUEsYUFBYSxFQUFFLEtBQUtHO0FBSGYsUUFBakI7QUFLSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsZUFBZjtBQUErQixNQUFBLElBQUksRUFBQztBQUFwQyxPQUNNSyxZQUROLEVBRUksNkJBQUMsMEJBQUQsUUFDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxTQUFTLEVBQUMsNENBQXpCO0FBQXNFLE1BQUEsVUFBVSxFQUFFLEtBQUtuRyxLQUFMLENBQVdWLGdCQUE3RjtBQUNJLE1BQUEscUJBQXFCLEVBQUUsS0FBS2dDLHlCQURoQztBQUVJLE1BQUEsV0FBVyxFQUFFLEtBQUtnRSxrQkFGdEI7QUFHSSxNQUFBLGFBQWEsRUFBRSxLQUFLSTtBQUh4QixNQURKLEVBS01tQixhQUxOLEVBTU1DLGNBTk4sQ0FESixDQUZKLEVBYUksNkJBQUMsU0FBRDtBQUFXLE1BQUEsU0FBUyxFQUFDLDJEQUFyQjtBQUNXLE1BQUEsV0FBVyxFQUFHLHlCQUFHLHFCQUFILENBRHpCO0FBRVcsTUFBQSxRQUFRLEVBQUcsS0FBSzdDO0FBRjNCLE1BYkosQ0FESjtBQW1CSCxHQWpkMkI7QUFtZDVCMkMsRUFBQUEsbUJBQW1CLEVBQUUsWUFBVztBQUM1QixRQUFJOUosaUNBQWdCQyxHQUFoQixHQUFzQmdLLE9BQXRCLEVBQUosRUFBcUM7QUFDakMzQywwQkFBSUMsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRTtBQUFULE9BQWI7O0FBQ0E7QUFDSCxLQUoyQixDQU01Qjs7O0FBQ0FGLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLGFBREM7QUFFVDNGLE1BQUFBLE1BQU0sRUFBRSxLQUFLRCxLQUFMLENBQVdDO0FBRlYsS0FBYjtBQUlIO0FBOWQyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSAnLi4vLi4vLi4vU2RrQ29uZmlnJztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IEF1dG9IaWRlU2Nyb2xsYmFyIGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL0F1dG9IaWRlU2Nyb2xsYmFyXCI7XHJcbmltcG9ydCB7aXNWYWxpZDNwaWRJbnZpdGV9IGZyb20gXCIuLi8uLi8uLi9Sb29tSW52aXRlXCI7XHJcbmltcG9ydCByYXRlX2xpbWl0ZWRfZnVuYyBmcm9tIFwiLi4vLi4vLi4vcmF0ZWxpbWl0ZWRmdW5jXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IENhbGxIYW5kbGVyIGZyb20gXCIuLi8uLi8uLi9DYWxsSGFuZGxlclwiO1xyXG5cclxuY29uc3QgSU5JVElBTF9MT0FEX05VTV9NRU1CRVJTID0gMzA7XHJcbmNvbnN0IElOSVRJQUxfTE9BRF9OVU1fSU5WSVRFRCA9IDU7XHJcbmNvbnN0IFNIT1dfTU9SRV9JTkNSRU1FTlQgPSAxMDA7XHJcblxyXG4vLyBSZWdleCBhcHBsaWVkIHRvIGZpbHRlciBvdXIgcHVuY3R1YXRpb24gaW4gbWVtYmVyIG5hbWVzIGJlZm9yZSBhcHBseWluZyBzb3J0LCB0byBmdXp6eSBpdCBhIGxpdHRsZVxyXG4vLyBtYXRjaGVzIGFsbCBBU0NJSSBwdW5jdHVhdGlvbjogIVwiIyQlJicoKSorLC0uLzo7PD0+P0BbXFxdXl9ge3x9flxyXG5jb25zdCBTT1JUX1JFR0VYID0gL1tcXHgyMS1cXHgyRlxceDNBLVxceDQwXFx4NUItXFx4NjBcXHg3Qi1cXHg3RV0rL2c7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnTWVtYmVyTGlzdCcsXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaS5oYXNMYXp5TG9hZE1lbWJlcnNFbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgLy8gc2hvdyBhbiBlbXB0eSBsaXN0XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9nZXRNZW1iZXJzU3RhdGUoW10pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9nZXRNZW1iZXJzU3RhdGUodGhpcy5yb29tTWVtYmVycygpKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBNb3ZlIHRoaXMgdG8gY29uc3RydWN0b3JcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX21vdW50ZWQgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoY2xpLmhhc0xhenlMb2FkTWVtYmVyc0VuYWJsZWQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zaG93TWVtYmVyc0FjY29yZGluZ1RvTWVtYmVyc2hpcFdpdGhMTCgpO1xyXG4gICAgICAgICAgICBjbGkub24oXCJSb29tLm15TWVtYmVyc2hpcFwiLCB0aGlzLm9uTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9saXN0ZW5Gb3JNZW1iZXJzQ2hhbmdlcygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjbGkub24oXCJSb29tXCIsIHRoaXMub25Sb29tKTsgLy8gaW52aXRlcyAmIGpvaW5pbmcgYWZ0ZXIgcGVla1xyXG4gICAgICAgIGNvbnN0IGVuYWJsZVByZXNlbmNlQnlIc1VybCA9IFNka0NvbmZpZy5nZXQoKVtcImVuYWJsZV9wcmVzZW5jZV9ieV9oc191cmxcIl07XHJcbiAgICAgICAgY29uc3QgaHNVcmwgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuYmFzZVVybDtcclxuICAgICAgICB0aGlzLl9zaG93UHJlc2VuY2UgPSB0cnVlO1xyXG4gICAgICAgIGlmIChlbmFibGVQcmVzZW5jZUJ5SHNVcmwgJiYgZW5hYmxlUHJlc2VuY2VCeUhzVXJsW2hzVXJsXSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Nob3dQcmVzZW5jZSA9IGVuYWJsZVByZXNlbmNlQnlIc1VybFtoc1VybF07XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfbGlzdGVuRm9yTWVtYmVyc0NoYW5nZXM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjbGkub24oXCJSb29tU3RhdGUubWVtYmVyc1wiLCB0aGlzLm9uUm9vbVN0YXRlTWVtYmVyKTtcclxuICAgICAgICBjbGkub24oXCJSb29tTWVtYmVyLm5hbWVcIiwgdGhpcy5vblJvb21NZW1iZXJOYW1lKTtcclxuICAgICAgICBjbGkub24oXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMub25Sb29tU3RhdGVFdmVudCk7XHJcbiAgICAgICAgLy8gV2UgbGlzdGVuIGZvciBjaGFuZ2VzIHRvIHRoZSBsYXN0UHJlc2VuY2VUcyB3aGljaCBpcyBlc3NlbnRpYWxseVxyXG4gICAgICAgIC8vIGxpc3RlbmluZyBmb3IgYWxsIHByZXNlbmNlIGV2ZW50cyAod2UgZGlzcGxheSBtb3N0IG9mIG5vdCBhbGwgb2ZcclxuICAgICAgICAvLyB0aGUgaW5mb3JtYXRpb24gY29udGFpbmVkIGluIHByZXNlbmNlIGV2ZW50cykuXHJcbiAgICAgICAgY2xpLm9uKFwiVXNlci5sYXN0UHJlc2VuY2VUc1wiLCB0aGlzLm9uVXNlclByZXNlbmNlQ2hhbmdlKTtcclxuICAgICAgICBjbGkub24oXCJVc2VyLnByZXNlbmNlXCIsIHRoaXMub25Vc2VyUHJlc2VuY2VDaGFuZ2UpO1xyXG4gICAgICAgIGNsaS5vbihcIlVzZXIuY3VycmVudGx5QWN0aXZlXCIsIHRoaXMub25Vc2VyUHJlc2VuY2VDaGFuZ2UpO1xyXG4gICAgICAgIC8vIGNsaS5vbihcIlJvb20udGltZWxpbmVcIiwgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9tb3VudGVkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGlmIChjbGkpIHtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiUm9vbVN0YXRlLm1lbWJlcnNcIiwgdGhpcy5vblJvb21TdGF0ZU1lbWJlcik7XHJcbiAgICAgICAgICAgIGNsaS5yZW1vdmVMaXN0ZW5lcihcIlJvb21NZW1iZXIubmFtZVwiLCB0aGlzLm9uUm9vbU1lbWJlck5hbWUpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJSb29tLm15TWVtYmVyc2hpcFwiLCB0aGlzLm9uTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlRXZlbnQpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJSb29tXCIsIHRoaXMub25Sb29tKTtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiVXNlci5sYXN0UHJlc2VuY2VUc1wiLCB0aGlzLm9uVXNlclByZXNlbmNlQ2hhbmdlKTtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiVXNlci5wcmVzZW5jZVwiLCB0aGlzLm9uVXNlclByZXNlbmNlQ2hhbmdlKTtcclxuICAgICAgICAgICAgY2xpLnJlbW92ZUxpc3RlbmVyKFwiVXNlci5jdXJyZW50bHlBY3RpdmVcIiwgdGhpcy5vblVzZXJQcmVzZW5jZUNoYW5nZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjYW5jZWwgYW55IHBlbmRpbmcgY2FsbHMgdG8gdGhlIHJhdGVfbGltaXRlZF9mdW5jc1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUxpc3QuY2FuY2VsUGVuZGluZ0NhbGwoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZiBsYXp5IGxvYWRpbmcgaXMgZW5hYmxlZCwgZWl0aGVyOlxyXG4gICAgICogc2hvdyBhIHNwaW5uZXIgYW5kIGxvYWQgdGhlIG1lbWJlcnMgaWYgdGhlIHVzZXIgaXMgam9pbmVkLFxyXG4gICAgICogb3Igc2hvdyB0aGUgbWVtYmVycyBhdmFpbGFibGUgc28gZmFyIGlmIHRoZSB1c2VyIGlzIGludml0ZWRcclxuICAgICAqL1xyXG4gICAgX3Nob3dNZW1iZXJzQWNjb3JkaW5nVG9NZW1iZXJzaGlwV2l0aExMOiBhc3luYyBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaS5oYXNMYXp5TG9hZE1lbWJlcnNFbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20odGhpcy5wcm9wcy5yb29tSWQpO1xyXG4gICAgICAgICAgICBjb25zdCBtZW1iZXJzaGlwID0gcm9vbSAmJiByb29tLmdldE15TWVtYmVyc2hpcCgpO1xyXG4gICAgICAgICAgICBpZiAobWVtYmVyc2hpcCA9PT0gXCJqb2luXCIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2xvYWRpbmc6IHRydWV9KTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgcm9vbS5sb2FkTWVtYmVyc0lmTmVlZGVkKCk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChleCkgey8qIGFscmVhZHkgbG9nZ2VkIGluIFJvb21WaWV3ICovfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX21vdW50ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHRoaXMuX2dldE1lbWJlcnNTdGF0ZSh0aGlzLnJvb21NZW1iZXJzKCkpKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9saXN0ZW5Gb3JNZW1iZXJzQ2hhbmdlcygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKG1lbWJlcnNoaXAgPT09IFwiaW52aXRlXCIpIHtcclxuICAgICAgICAgICAgICAgIC8vIHNob3cgdGhlIG1lbWJlcnMgd2UndmUgZ290IHdoZW4gaW52aXRlZFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh0aGlzLl9nZXRNZW1iZXJzU3RhdGUodGhpcy5yb29tTWVtYmVycygpKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRNZW1iZXJzU3RhdGU6IGZ1bmN0aW9uKG1lbWJlcnMpIHtcclxuICAgICAgICAvLyBzZXQgdGhlIHN0YXRlIGFmdGVyIGRldGVybWluaW5nIF9zaG93UHJlc2VuY2UgdG8gbWFrZSBzdXJlIGl0J3NcclxuICAgICAgICAvLyB0YWtlbiBpbnRvIGFjY291bnQgd2hpbGUgcmVyZW5kZXJpbmdcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgbWVtYmVyczogbWVtYmVycyxcclxuICAgICAgICAgICAgZmlsdGVyZWRKb2luZWRNZW1iZXJzOiB0aGlzLl9maWx0ZXJNZW1iZXJzKG1lbWJlcnMsICdqb2luJyksXHJcbiAgICAgICAgICAgIGZpbHRlcmVkSW52aXRlZE1lbWJlcnM6IHRoaXMuX2ZpbHRlck1lbWJlcnMobWVtYmVycywgJ2ludml0ZScpLFxyXG5cclxuICAgICAgICAgICAgLy8gaWRlYWxseSB3ZSdkIHNpemUgdGhpcyB0byB0aGUgcGFnZSBoZWlnaHQsIGJ1dFxyXG4gICAgICAgICAgICAvLyBpbiBwcmFjdGljZSBJIGZpbmQgdGhhdCBhIGxpdHRsZSBjb25zdHJhaW5pbmdcclxuICAgICAgICAgICAgdHJ1bmNhdGVBdEpvaW5lZDogSU5JVElBTF9MT0FEX05VTV9NRU1CRVJTLFxyXG4gICAgICAgICAgICB0cnVuY2F0ZUF0SW52aXRlZDogSU5JVElBTF9MT0FEX05VTV9JTlZJVEVELFxyXG4gICAgICAgICAgICBzZWFyY2hRdWVyeTogXCJcIixcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBvblVzZXJQcmVzZW5jZUNoYW5nZShldmVudCwgdXNlcikge1xyXG4gICAgICAgIC8vIEF0dGFjaCBhIFNJTkdMRSBsaXN0ZW5lciBmb3IgZ2xvYmFsIHByZXNlbmNlIGNoYW5nZXMgdGhlbiBsb2NhdGUgdGhlXHJcbiAgICAgICAgLy8gbWVtYmVyIHRpbGUgYW5kIHJlLXJlbmRlciBpdC4gVGhpcyBpcyBtb3JlIGVmZmljaWVudCB0aGFuIGV2ZXJ5IHRpbGVcclxuICAgICAgICAvLyBldmVyIGF0dGFjaGluZyB0aGVpciBvd24gbGlzdGVuZXIuXHJcbiAgICAgICAgY29uc3QgdGlsZSA9IHRoaXMucmVmc1t1c2VyLnVzZXJJZF07XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coYEdvdCBwcmVzZW5jZSB1cGRhdGUgZm9yICR7dXNlci51c2VySWR9LiBoYXNUaWxlPSR7ISF0aWxlfWApO1xyXG4gICAgICAgIGlmICh0aWxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZUxpc3QoKTsgLy8gcmVvcmRlciB0aGUgbWVtYmVyc2hpcCBsaXN0XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb206IGZ1bmN0aW9uKHJvb20pIHtcclxuICAgICAgICBpZiAocm9vbS5yb29tSWQgIT09IHRoaXMucHJvcHMucm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gV2UgbGlzdGVuIGZvciByb29tIGV2ZW50cyBiZWNhdXNlIHdoZW4gd2UgYWNjZXB0IGFuIGludml0ZVxyXG4gICAgICAgIC8vIHdlIG5lZWQgdG8gd2FpdCB0aWxsIHRoZSByb29tIGlzIGZ1bGx5IHBvcHVsYXRlZCB3aXRoIHN0YXRlXHJcbiAgICAgICAgLy8gYmVmb3JlIHJlZnJlc2hpbmcgdGhlIG1lbWJlciBsaXN0IGVsc2Ugd2UgZ2V0IGEgc3RhbGUgbGlzdC5cclxuICAgICAgICB0aGlzLl9zaG93TWVtYmVyc0FjY29yZGluZ1RvTWVtYmVyc2hpcFdpdGhMTCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbk15TWVtYmVyc2hpcDogZnVuY3Rpb24ocm9vbSwgbWVtYmVyc2hpcCwgb2xkTWVtYmVyc2hpcCkge1xyXG4gICAgICAgIGlmIChyb29tLnJvb21JZCA9PT0gdGhpcy5wcm9wcy5yb29tSWQgJiYgbWVtYmVyc2hpcCA9PT0gXCJqb2luXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2hvd01lbWJlcnNBY2NvcmRpbmdUb01lbWJlcnNoaXBXaXRoTEwoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUm9vbVN0YXRlTWVtYmVyOiBmdW5jdGlvbihldiwgc3RhdGUsIG1lbWJlcikge1xyXG4gICAgICAgIGlmIChtZW1iZXIucm9vbUlkICE9PSB0aGlzLnByb3BzLnJvb21JZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUxpc3QoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tTWVtYmVyTmFtZTogZnVuY3Rpb24oZXYsIG1lbWJlcikge1xyXG4gICAgICAgIGlmIChtZW1iZXIucm9vbUlkICE9PSB0aGlzLnByb3BzLnJvb21JZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUxpc3QoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Sb29tU3RhdGVFdmVudDogZnVuY3Rpb24oZXZlbnQsIHN0YXRlKSB7XHJcbiAgICAgICAgaWYgKGV2ZW50LmdldFJvb21JZCgpID09PSB0aGlzLnByb3BzLnJvb21JZCAmJlxyXG4gICAgICAgICAgICBldmVudC5nZXRUeXBlKCkgPT09IFwibS5yb29tLnRoaXJkX3BhcnR5X2ludml0ZVwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZUxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF91cGRhdGVMaXN0OiByYXRlX2xpbWl0ZWRfZnVuYyhmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl91cGRhdGVMaXN0Tm93KCk7XHJcbiAgICB9LCA1MDApLFxyXG5cclxuICAgIF91cGRhdGVMaXN0Tm93OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIlVwZGF0aW5nIG1lbWJlcmxpc3RcIik7XHJcbiAgICAgICAgY29uc3QgbmV3U3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICBtZW1iZXJzOiB0aGlzLnJvb21NZW1iZXJzKCksXHJcbiAgICAgICAgfTtcclxuICAgICAgICBuZXdTdGF0ZS5maWx0ZXJlZEpvaW5lZE1lbWJlcnMgPSB0aGlzLl9maWx0ZXJNZW1iZXJzKG5ld1N0YXRlLm1lbWJlcnMsICdqb2luJywgdGhpcy5zdGF0ZS5zZWFyY2hRdWVyeSk7XHJcbiAgICAgICAgbmV3U3RhdGUuZmlsdGVyZWRJbnZpdGVkTWVtYmVycyA9IHRoaXMuX2ZpbHRlck1lbWJlcnMobmV3U3RhdGUubWVtYmVycywgJ2ludml0ZScsIHRoaXMuc3RhdGUuc2VhcmNoUXVlcnkpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRNZW1iZXJzV2l0aFVzZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5yb29tSWQpIHJldHVybiBbXTtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IGNsaS5nZXRSb29tKHRoaXMucHJvcHMucm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20pIHJldHVybiBbXTtcclxuXHJcbiAgICAgICAgY29uc3QgYWxsTWVtYmVycyA9IE9iamVjdC52YWx1ZXMocm9vbS5jdXJyZW50U3RhdGUubWVtYmVycyk7XHJcblxyXG4gICAgICAgIGFsbE1lbWJlcnMuZm9yRWFjaChmdW5jdGlvbihtZW1iZXIpIHtcclxuICAgICAgICAgICAgLy8gd29yayBhcm91bmQgYSByYWNlIHdoZXJlIHlvdSBtaWdodCBoYXZlIGEgcm9vbSBtZW1iZXIgb2JqZWN0XHJcbiAgICAgICAgICAgIC8vIGJlZm9yZSB0aGUgdXNlciBvYmplY3QgZXhpc3RzLiAgVGhpcyBtYXkgb3IgbWF5IG5vdCBjYXVzZVxyXG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3ZlY3Rvci13ZWIvaXNzdWVzLzE4NlxyXG4gICAgICAgICAgICBpZiAobWVtYmVyLnVzZXIgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG1lbWJlci51c2VyID0gY2xpLmdldFVzZXIobWVtYmVyLnVzZXJJZCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIFhYWDogdGhpcyB1c2VyIG1heSBoYXZlIG5vIGxhc3RQcmVzZW5jZVRzIHZhbHVlIVxyXG4gICAgICAgICAgICAvLyB0aGUgcmlnaHQgc29sdXRpb24gaGVyZSBpcyB0byBmaXggdGhlIHJhY2UgcmF0aGVyIHRoYW4gbGVhdmUgaXQgYXMgMFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gYWxsTWVtYmVycztcclxuICAgIH0sXHJcblxyXG4gICAgcm9vbU1lbWJlcnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IENvbmZlcmVuY2VIYW5kbGVyID0gQ2FsbEhhbmRsZXIuZ2V0Q29uZmVyZW5jZUhhbmRsZXIoKTtcclxuXHJcbiAgICAgICAgY29uc3QgYWxsTWVtYmVycyA9IHRoaXMuZ2V0TWVtYmVyc1dpdGhVc2VyKCk7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyZWRBbmRTb3J0ZWRNZW1iZXJzID0gYWxsTWVtYmVycy5maWx0ZXIoKG0pID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIG0ubWVtYmVyc2hpcCA9PT0gJ2pvaW4nIHx8IG0ubWVtYmVyc2hpcCA9PT0gJ2ludml0ZSdcclxuICAgICAgICAgICAgKSAmJiAoXHJcbiAgICAgICAgICAgICAgICAhQ29uZmVyZW5jZUhhbmRsZXIgfHxcclxuICAgICAgICAgICAgICAgIChDb25mZXJlbmNlSGFuZGxlciAmJiAhQ29uZmVyZW5jZUhhbmRsZXIuaXNDb25mZXJlbmNlVXNlcihtLnVzZXJJZCkpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZmlsdGVyZWRBbmRTb3J0ZWRNZW1iZXJzLnNvcnQodGhpcy5tZW1iZXJTb3J0KTtcclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRBbmRTb3J0ZWRNZW1iZXJzO1xyXG4gICAgfSxcclxuXHJcbiAgICBfY3JlYXRlT3ZlcmZsb3dUaWxlSm9pbmVkOiBmdW5jdGlvbihvdmVyZmxvd0NvdW50LCB0b3RhbENvdW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NyZWF0ZU92ZXJmbG93VGlsZShvdmVyZmxvd0NvdW50LCB0b3RhbENvdW50LCB0aGlzLl9zaG93TW9yZUpvaW5lZE1lbWJlckxpc3QpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfY3JlYXRlT3ZlcmZsb3dUaWxlSW52aXRlZDogZnVuY3Rpb24ob3ZlcmZsb3dDb3VudCwgdG90YWxDb3VudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jcmVhdGVPdmVyZmxvd1RpbGUob3ZlcmZsb3dDb3VudCwgdG90YWxDb3VudCwgdGhpcy5fc2hvd01vcmVJbnZpdGVkTWVtYmVyTGlzdCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9jcmVhdGVPdmVyZmxvd1RpbGU6IGZ1bmN0aW9uKG92ZXJmbG93Q291bnQsIHRvdGFsQ291bnQsIG9uQ2xpY2spIHtcclxuICAgICAgICAvLyBGb3Igbm93IHdlJ2xsIHByZXRlbmQgdGhpcyBpcyBhbnkgZW50aXR5LiBJdCBzaG91bGQgcHJvYmFibHkgYmUgYSBzZXBhcmF0ZSB0aWxlLlxyXG4gICAgICAgIGNvbnN0IEVudGl0eVRpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuRW50aXR5VGlsZVwiKTtcclxuICAgICAgICBjb25zdCBCYXNlQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcImF2YXRhcnMuQmFzZUF2YXRhclwiKTtcclxuICAgICAgICBjb25zdCB0ZXh0ID0gX3QoXCJhbmQgJShjb3VudClzIG90aGVycy4uLlwiLCB7IGNvdW50OiBvdmVyZmxvd0NvdW50IH0pO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxFbnRpdHlUaWxlIGNsYXNzTmFtZT1cIm14X0VudGl0eVRpbGVfZWxsaXBzaXNcIiBhdmF0YXJKc3g9e1xyXG4gICAgICAgICAgICAgICAgPEJhc2VBdmF0YXIgdXJsPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9lbGxpcHNpcy5zdmdcIil9IG5hbWU9XCIuLi5cIiB3aWR0aD17MzZ9IGhlaWdodD17MzZ9IC8+XHJcbiAgICAgICAgICAgIH0gbmFtZT17dGV4dH0gcHJlc2VuY2VTdGF0ZT1cIm9ubGluZVwiIHN1cHByZXNzT25Ib3Zlcj17dHJ1ZX1cclxuICAgICAgICAgICAgb25DbGljaz17b25DbGlja30gLz5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2hvd01vcmVKb2luZWRNZW1iZXJMaXN0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgdHJ1bmNhdGVBdEpvaW5lZDogdGhpcy5zdGF0ZS50cnVuY2F0ZUF0Sm9pbmVkICsgU0hPV19NT1JFX0lOQ1JFTUVOVCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3Nob3dNb3JlSW52aXRlZE1lbWJlckxpc3Q6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB0cnVuY2F0ZUF0SW52aXRlZDogdGhpcy5zdGF0ZS50cnVuY2F0ZUF0SW52aXRlZCArIFNIT1dfTU9SRV9JTkNSRU1FTlQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1lbWJlclN0cmluZzogZnVuY3Rpb24obWVtYmVyKSB7XHJcbiAgICAgICAgaWYgKCFtZW1iZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFwiKG51bGwpXCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgdSA9IG1lbWJlci51c2VyO1xyXG4gICAgICAgICAgICByZXR1cm4gXCIoXCIgKyBtZW1iZXIubmFtZSArIFwiLCBcIiArIG1lbWJlci5wb3dlckxldmVsICsgXCIsIFwiICsgKHUgPyB1Lmxhc3RBY3RpdmVBZ28gOiBcIjxudWxsPlwiKSArIFwiLCBcIiArICh1ID8gdS5nZXRMYXN0QWN0aXZlVHMoKSA6IFwiPG51bGw+XCIpICsgXCIsIFwiICsgKHUgPyB1LmN1cnJlbnRseUFjdGl2ZSA6IFwiPG51bGw+XCIpICsgXCIsIFwiICsgKHUgPyB1LnByZXNlbmNlIDogXCI8bnVsbD5cIikgKyBcIilcIjtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHJldHVybnMgbmVnYXRpdmUgaWYgYSBjb21lcyBiZWZvcmUgYixcclxuICAgIC8vIHJldHVybnMgMCBpZiBhIGFuZCBiIGFyZSBlcXVpdmFsZW50IGluIG9yZGVyaW5nXHJcbiAgICAvLyByZXR1cm5zIHBvc2l0aXZlIGlmIGEgY29tZXMgYWZ0ZXIgYi5cclxuICAgIG1lbWJlclNvcnQ6IGZ1bmN0aW9uKG1lbWJlckEsIG1lbWJlckIpIHtcclxuICAgICAgICAvLyBvcmRlciBieSBwcmVzZW5jZSwgd2l0aCBcImFjdGl2ZSBub3dcIiBmaXJzdC5cclxuICAgICAgICAvLyAuLi5hbmQgdGhlbiBieSBwb3dlciBsZXZlbFxyXG4gICAgICAgIC8vIC4uLmFuZCB0aGVuIGJ5IGxhc3QgYWN0aXZlXHJcbiAgICAgICAgLy8gLi4uYW5kIHRoZW4gYWxwaGFiZXRpY2FsbHkuXHJcbiAgICAgICAgLy8gV2UgY291bGQgdGllYnJlYWsgaW5zdGVhZCBieSBcImxhc3QgcmVjZW50bHkgc3Bva2VuIGluIHRoaXMgcm9vbVwiIGlmIHdlIHdhbnRlZCB0by5cclxuXHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coYENvbXBhcmluZyB1c2VyQT0ke3RoaXMubWVtYmVyU3RyaW5nKG1lbWJlckEpfSB1c2VyQj0ke3RoaXMubWVtYmVyU3RyaW5nKG1lbWJlckIpfWApO1xyXG5cclxuICAgICAgICBjb25zdCB1c2VyQSA9IG1lbWJlckEudXNlcjtcclxuICAgICAgICBjb25zdCB1c2VyQiA9IG1lbWJlckIudXNlcjtcclxuXHJcbiAgICAgICAgLy8gaWYgKCF1c2VyQSkgY29uc29sZS5sb2coXCIhISBNSVNTSU5HIFVTRVIgRk9SIEEtU0lERTogXCIgKyBtZW1iZXJBLm5hbWUgKyBcIiAhIVwiKTtcclxuICAgICAgICAvLyBpZiAoIXVzZXJCKSBjb25zb2xlLmxvZyhcIiEhIE1JU1NJTkcgVVNFUiBGT1IgQi1TSURFOiBcIiArIG1lbWJlckIubmFtZSArIFwiICEhXCIpO1xyXG5cclxuICAgICAgICBpZiAoIXVzZXJBICYmICF1c2VyQikgcmV0dXJuIDA7XHJcbiAgICAgICAgaWYgKHVzZXJBICYmICF1c2VyQikgcmV0dXJuIC0xO1xyXG4gICAgICAgIGlmICghdXNlckEgJiYgdXNlckIpIHJldHVybiAxO1xyXG5cclxuICAgICAgICAvLyBGaXJzdCBieSBwcmVzZW5jZVxyXG4gICAgICAgIGlmICh0aGlzLl9zaG93UHJlc2VuY2UpIHtcclxuICAgICAgICAgICAgY29uc3QgY29udmVydFByZXNlbmNlID0gKHApID0+IHAgPT09ICd1bmF2YWlsYWJsZScgPyAnb25saW5lJyA6IHA7XHJcbiAgICAgICAgICAgIGNvbnN0IHByZXNlbmNlSW5kZXggPSBwID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9yZGVyID0gWydhY3RpdmUnLCAnb25saW5lJywgJ29mZmxpbmUnXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlkeCA9IG9yZGVyLmluZGV4T2YoY29udmVydFByZXNlbmNlKHApKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpZHggPT09IC0xID8gb3JkZXIubGVuZ3RoIDogaWR4OyAvLyB1bmtub3duIHN0YXRlcyBhdCB0aGUgZW5kXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBpZHhBID0gcHJlc2VuY2VJbmRleCh1c2VyQS5jdXJyZW50bHlBY3RpdmUgPyAnYWN0aXZlJyA6IHVzZXJBLnByZXNlbmNlKTtcclxuICAgICAgICAgICAgY29uc3QgaWR4QiA9IHByZXNlbmNlSW5kZXgodXNlckIuY3VycmVudGx5QWN0aXZlID8gJ2FjdGl2ZScgOiB1c2VyQi5wcmVzZW5jZSk7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGB1c2VyQV9wcmVzZW5jZUdyb3VwPSR7aWR4QX0gdXNlckJfcHJlc2VuY2VHcm91cD0ke2lkeEJ9YCk7XHJcbiAgICAgICAgICAgIGlmIChpZHhBICE9PSBpZHhCKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhcIkNvbXBhcmluZyBvbiBwcmVzZW5jZSBncm91cCAtIHJldHVybmluZ1wiKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpZHhBIC0gaWR4QjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gU2Vjb25kIGJ5IHBvd2VyIGxldmVsXHJcbiAgICAgICAgaWYgKG1lbWJlckEucG93ZXJMZXZlbCAhPT0gbWVtYmVyQi5wb3dlckxldmVsKSB7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiQ29tcGFyaW5nIG9uIHBvd2VyIGxldmVsIC0gcmV0dXJuaW5nXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gbWVtYmVyQi5wb3dlckxldmVsIC0gbWVtYmVyQS5wb3dlckxldmVsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVGhpcmQgYnkgbGFzdCBhY3RpdmVcclxuICAgICAgICBpZiAodGhpcy5fc2hvd1ByZXNlbmNlICYmIHVzZXJBLmdldExhc3RBY3RpdmVUcygpICE9PSB1c2VyQi5nZXRMYXN0QWN0aXZlVHMoKSkge1xyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhcIkNvbXBhcmluZyBvbiBsYXN0IGFjdGl2ZSB0aW1lc3RhbXAgLSByZXR1cm5pbmdcIik7XHJcbiAgICAgICAgICAgIHJldHVybiB1c2VyQi5nZXRMYXN0QWN0aXZlVHMoKSAtIHVzZXJBLmdldExhc3RBY3RpdmVUcygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRm91cnRoIGJ5IG5hbWUgKGFscGhhYmV0aWNhbClcclxuICAgICAgICBjb25zdCBuYW1lQSA9IChtZW1iZXJBLm5hbWVbMF0gPT09ICdAJyA/IG1lbWJlckEubmFtZS5zdWJzdHIoMSkgOiBtZW1iZXJBLm5hbWUpLnJlcGxhY2UoU09SVF9SRUdFWCwgXCJcIik7XHJcbiAgICAgICAgY29uc3QgbmFtZUIgPSAobWVtYmVyQi5uYW1lWzBdID09PSAnQCcgPyBtZW1iZXJCLm5hbWUuc3Vic3RyKDEpIDogbWVtYmVyQi5uYW1lKS5yZXBsYWNlKFNPUlRfUkVHRVgsIFwiXCIpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGBDb21wYXJpbmcgdXNlckFfbmFtZT0ke25hbWVBfSBhZ2FpbnN0IHVzZXJCX25hbWU9JHtuYW1lQn0gLSByZXR1cm5pbmdgKTtcclxuICAgICAgICByZXR1cm4gbmFtZUEubG9jYWxlQ29tcGFyZShuYW1lQiwge1xyXG4gICAgICAgICAgICBpZ25vcmVQdW5jdHVhdGlvbjogdHJ1ZSxcclxuICAgICAgICAgICAgc2Vuc2l0aXZpdHk6IFwiYmFzZVwiLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblNlYXJjaFF1ZXJ5Q2hhbmdlZDogZnVuY3Rpb24oc2VhcmNoUXVlcnkpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2VhcmNoUXVlcnksXHJcbiAgICAgICAgICAgIGZpbHRlcmVkSm9pbmVkTWVtYmVyczogdGhpcy5fZmlsdGVyTWVtYmVycyh0aGlzLnN0YXRlLm1lbWJlcnMsICdqb2luJywgc2VhcmNoUXVlcnkpLFxyXG4gICAgICAgICAgICBmaWx0ZXJlZEludml0ZWRNZW1iZXJzOiB0aGlzLl9maWx0ZXJNZW1iZXJzKHRoaXMuc3RhdGUubWVtYmVycywgJ2ludml0ZScsIHNlYXJjaFF1ZXJ5KSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uUGVuZGluZzNwaWRJbnZpdGVDbGljazogZnVuY3Rpb24oaW52aXRlRXZlbnQpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3XzNwaWRfaW52aXRlJyxcclxuICAgICAgICAgICAgZXZlbnQ6IGludml0ZUV2ZW50LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZmlsdGVyTWVtYmVyczogZnVuY3Rpb24obWVtYmVycywgbWVtYmVyc2hpcCwgcXVlcnkpIHtcclxuICAgICAgICByZXR1cm4gbWVtYmVycy5maWx0ZXIoKG0pID0+IHtcclxuICAgICAgICAgICAgaWYgKHF1ZXJ5KSB7XHJcbiAgICAgICAgICAgICAgICBxdWVyeSA9IHF1ZXJ5LnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXRjaGVzTmFtZSA9IG0ubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YocXVlcnkpICE9PSAtMTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXNJZCA9IG0udXNlcklkLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihxdWVyeSkgIT09IC0xO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghbWF0Y2hlc05hbWUgJiYgIW1hdGNoZXNJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIG0ubWVtYmVyc2hpcCA9PT0gbWVtYmVyc2hpcDtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFBlbmRpbmczUGlkSW52aXRlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gaW5jbHVkZSAzcGlkIGludml0ZXMgKG0ucm9vbS50aGlyZF9wYXJ0eV9pbnZpdGUpIHN0YXRlIGV2ZW50cy5cclxuICAgICAgICAvLyBUaGUgSFMgbWF5IGhhdmUgYWxyZWFkeSBjb252ZXJ0ZWQgdGhlc2UgaW50byBtLnJvb20ubWVtYmVyIGludml0ZXMgc29cclxuICAgICAgICAvLyB3ZSBzaG91bGRuJ3QgYWRkIHRoZW0gaWYgdGhlIDNwaWQgaW52aXRlIHN0YXRlIGtleSAodG9rZW4pIGlzIGluIHRoZVxyXG4gICAgICAgIC8vIG1lbWJlciBpbnZpdGUgKGNvbnRlbnQudGhpcmRfcGFydHlfaW52aXRlLnNpZ25lZC50b2tlbilcclxuICAgICAgICBjb25zdCByb29tID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20odGhpcy5wcm9wcy5yb29tSWQpO1xyXG5cclxuICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICByZXR1cm4gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20udGhpcmRfcGFydHlfaW52aXRlXCIpLmZpbHRlcihmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWlzVmFsaWQzcGlkSW52aXRlKGUpKSByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gZGlzY2FyZCBhbGwgaW52aXRlcyB3aGljaCBoYXZlIGEgbS5yb29tLm1lbWJlciBldmVudCBzaW5jZSB3ZSd2ZVxyXG4gICAgICAgICAgICAgICAgLy8gYWxyZWFkeSBhZGRlZCB0aGVtLlxyXG4gICAgICAgICAgICAgICAgY29uc3QgbWVtYmVyRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRJbnZpdGVGb3JUaHJlZVBpZFRva2VuKGUuZ2V0U3RhdGVLZXkoKSk7XHJcbiAgICAgICAgICAgICAgICBpZiAobWVtYmVyRXZlbnQpIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9tYWtlTWVtYmVyVGlsZXM6IGZ1bmN0aW9uKG1lbWJlcnMpIHtcclxuICAgICAgICBjb25zdCBNZW1iZXJUaWxlID0gc2RrLmdldENvbXBvbmVudChcInJvb21zLk1lbWJlclRpbGVcIik7XHJcbiAgICAgICAgY29uc3QgRW50aXR5VGlsZSA9IHNkay5nZXRDb21wb25lbnQoXCJyb29tcy5FbnRpdHlUaWxlXCIpO1xyXG5cclxuICAgICAgICByZXR1cm4gbWVtYmVycy5tYXAoKG0pID0+IHtcclxuICAgICAgICAgICAgaWYgKG0udXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBJcyBhIE1hdHJpeCBpbnZpdGVcclxuICAgICAgICAgICAgICAgIHJldHVybiA8TWVtYmVyVGlsZSBrZXk9e20udXNlcklkfSBtZW1iZXI9e219IHJlZj17bS51c2VySWR9IHNob3dQcmVzZW5jZT17dGhpcy5fc2hvd1ByZXNlbmNlfSAvPjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIElzIGEgM3BpZCBpbnZpdGVcclxuICAgICAgICAgICAgICAgIHJldHVybiA8RW50aXR5VGlsZSBrZXk9e20uZ2V0U3RhdGVLZXkoKX0gbmFtZT17bS5nZXRDb250ZW50KCkuZGlzcGxheV9uYW1lfSBzdXBwcmVzc09uSG92ZXI9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5fb25QZW5kaW5nM3BpZEludml0ZUNsaWNrKG0pfSAvPjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0Q2hpbGRyZW5Kb2luZWQ6IGZ1bmN0aW9uKHN0YXJ0LCBlbmQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbWFrZU1lbWJlclRpbGVzKHRoaXMuc3RhdGUuZmlsdGVyZWRKb2luZWRNZW1iZXJzLnNsaWNlKHN0YXJ0LCBlbmQpKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldENoaWxkQ291bnRKb2luZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN0YXRlLmZpbHRlcmVkSm9pbmVkTWVtYmVycy5sZW5ndGg7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRDaGlsZHJlbkludml0ZWQ6IGZ1bmN0aW9uKHN0YXJ0LCBlbmQpIHtcclxuICAgICAgICBsZXQgdGFyZ2V0cyA9IHRoaXMuc3RhdGUuZmlsdGVyZWRJbnZpdGVkTWVtYmVycztcclxuICAgICAgICBpZiAoZW5kID4gdGhpcy5zdGF0ZS5maWx0ZXJlZEludml0ZWRNZW1iZXJzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0YXJnZXRzID0gdGFyZ2V0cy5jb25jYXQodGhpcy5fZ2V0UGVuZGluZzNQaWRJbnZpdGVzKCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21ha2VNZW1iZXJUaWxlcyh0YXJnZXRzLnNsaWNlKHN0YXJ0LCBlbmQpKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldENoaWxkQ291bnRJbnZpdGVkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS5maWx0ZXJlZEludml0ZWRNZW1iZXJzLmxlbmd0aCArICh0aGlzLl9nZXRQZW5kaW5nM1BpZEludml0ZXMoKSB8fCBbXSkubGVuZ3RoO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmxvYWRpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5TcGlubmVyXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9NZW1iZXJMaXN0XCI+PFNwaW5uZXIgLz48L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBTZWFyY2hCb3ggPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlNlYXJjaEJveCcpO1xyXG4gICAgICAgIGNvbnN0IFRydW5jYXRlZExpc3QgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuVHJ1bmNhdGVkTGlzdFwiKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbSh0aGlzLnByb3BzLnJvb21JZCk7XHJcbiAgICAgICAgbGV0IGludml0ZUJ1dHRvbjtcclxuXHJcbiAgICAgICAgaWYgKHJvb20gJiYgcm9vbS5nZXRNeU1lbWJlcnNoaXAoKSA9PT0gJ2pvaW4nKSB7XHJcbiAgICAgICAgICAgIC8vIGFzc3VtZSB3ZSBjYW4gaW52aXRlIHVudGlsIHByb3ZlbiBmYWxzZVxyXG4gICAgICAgICAgICBsZXQgY2FuSW52aXRlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHBsRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5wb3dlcl9sZXZlbHNcIiwgXCJcIik7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lID0gcm9vbS5nZXRNZW1iZXIoY2xpLmdldFVzZXJJZCgpKTtcclxuICAgICAgICAgICAgaWYgKHBsRXZlbnQgJiYgbWUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvbnRlbnQgPSBwbEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICAgICAgICAgIGlmIChjb250ZW50ICYmIGNvbnRlbnQuaW52aXRlID4gbWUucG93ZXJMZXZlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbkludml0ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudChcImVsZW1lbnRzLkFjY2Vzc2libGVCdXR0b25cIik7XHJcbiAgICAgICAgICAgIGludml0ZUJ1dHRvbiA9XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZW1iZXJMaXN0X2ludml0ZVwiIG9uQ2xpY2s9e3RoaXMub25JbnZpdGVCdXR0b25DbGlja30gZGlzYWJsZWQ9eyFjYW5JbnZpdGV9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPnsgX3QoJ0ludml0ZSB0byB0aGlzIHJvb20nKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBpbnZpdGVkSGVhZGVyO1xyXG4gICAgICAgIGxldCBpbnZpdGVkU2VjdGlvbjtcclxuICAgICAgICBpZiAodGhpcy5fZ2V0Q2hpbGRDb3VudEludml0ZWQoKSA+IDApIHtcclxuICAgICAgICAgICAgaW52aXRlZEhlYWRlciA9IDxoMj57IF90KFwiSW52aXRlZFwiKSB9PC9oMj47XHJcbiAgICAgICAgICAgIGludml0ZWRTZWN0aW9uID0gPFRydW5jYXRlZExpc3QgY2xhc3NOYW1lPVwibXhfTWVtYmVyTGlzdF9zZWN0aW9uIG14X01lbWJlckxpc3RfaW52aXRlZFwiIHRydW5jYXRlQXQ9e3RoaXMuc3RhdGUudHJ1bmNhdGVBdEludml0ZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZU92ZXJmbG93RWxlbWVudD17dGhpcy5fY3JlYXRlT3ZlcmZsb3dUaWxlSW52aXRlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2V0Q2hpbGRyZW49e3RoaXMuX2dldENoaWxkcmVuSW52aXRlZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2V0Q2hpbGRDb3VudD17dGhpcy5fZ2V0Q2hpbGRDb3VudEludml0ZWR9XHJcbiAgICAgICAgICAgICAgICAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVyTGlzdFwiIHJvbGU9XCJ0YWJwYW5lbFwiPlxyXG4gICAgICAgICAgICAgICAgeyBpbnZpdGVCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgPEF1dG9IaWRlU2Nyb2xsYmFyPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVtYmVyTGlzdF93cmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUcnVuY2F0ZWRMaXN0IGNsYXNzTmFtZT1cIm14X01lbWJlckxpc3Rfc2VjdGlvbiBteF9NZW1iZXJMaXN0X2pvaW5lZFwiIHRydW5jYXRlQXQ9e3RoaXMuc3RhdGUudHJ1bmNhdGVBdEpvaW5lZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZU92ZXJmbG93RWxlbWVudD17dGhpcy5fY3JlYXRlT3ZlcmZsb3dUaWxlSm9pbmVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0Q2hpbGRyZW49e3RoaXMuX2dldENoaWxkcmVuSm9pbmVkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0Q2hpbGRDb3VudD17dGhpcy5fZ2V0Q2hpbGRDb3VudEpvaW5lZH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBpbnZpdGVkSGVhZGVyIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBpbnZpdGVkU2VjdGlvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0F1dG9IaWRlU2Nyb2xsYmFyPlxyXG5cclxuICAgICAgICAgICAgICAgIDxTZWFyY2hCb3ggY2xhc3NOYW1lPVwibXhfTWVtYmVyTGlzdF9xdWVyeSBteF90ZXh0aW5wdXRfaWNvbiBteF90ZXh0aW5wdXRfc2VhcmNoXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eyBfdCgnRmlsdGVyIHJvb20gbWVtYmVycycpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25TZWFyY2g9eyB0aGlzLm9uU2VhcmNoUXVlcnlDaGFuZ2VkIH0gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25JbnZpdGVCdXR0b25DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdyZXF1aXJlX3JlZ2lzdHJhdGlvbid9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY2FsbCBBZGRyZXNzUGlja2VyRGlhbG9nXHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19pbnZpdGUnLFxyXG4gICAgICAgICAgICByb29tSWQ6IHRoaXMucHJvcHMucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==