"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _CallHandler = _interopRequireDefault(require("../../../CallHandler"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _Stickerpicker = _interopRequireDefault(require("./Stickerpicker"));

var _Permalinks = require("../../../utils/permalinks/Permalinks");

var _ContentMessages = _interopRequireDefault(require("../../../ContentMessages"));

var _E2EIcon = _interopRequireDefault(require("./E2EIcon"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function ComposerAvatar(props) {
  const MemberStatusMessageAvatar = sdk.getComponent('avatars.MemberStatusMessageAvatar');
  return _react.default.createElement("div", {
    className: "mx_MessageComposer_avatar"
  }, _react.default.createElement(MemberStatusMessageAvatar, {
    member: props.me,
    width: 24,
    height: 24
  }));
}

ComposerAvatar.propTypes = {
  me: _propTypes.default.object.isRequired
};

function CallButton(props) {
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');

  const onVoiceCallClick = ev => {
    _dispatcher.default.dispatch({
      action: 'place_call',
      type: "voice",
      room_id: props.roomId
    });
  };

  return _react.default.createElement(AccessibleButton, {
    className: "mx_MessageComposer_button mx_MessageComposer_voicecall",
    onClick: onVoiceCallClick,
    title: (0, _languageHandler._t)('Voice call')
  });
}

CallButton.propTypes = {
  roomId: _propTypes.default.string.isRequired
};

function VideoCallButton(props) {
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');

  const onCallClick = ev => {
    _dispatcher.default.dispatch({
      action: 'place_call',
      type: ev.shiftKey ? "screensharing" : "video",
      room_id: props.roomId
    });
  };

  return _react.default.createElement(AccessibleButton, {
    className: "mx_MessageComposer_button mx_MessageComposer_videocall",
    onClick: onCallClick,
    title: (0, _languageHandler._t)('Video call')
  });
}

VideoCallButton.propTypes = {
  roomId: _propTypes.default.string.isRequired
};

function HangupButton(props) {
  const AccessibleButton = sdk.getComponent('elements.AccessibleButton');

  const onHangupClick = () => {
    const call = _CallHandler.default.getCallForRoom(props.roomId);

    if (!call) {
      return;
    }

    _dispatcher.default.dispatch({
      action: 'hangup',
      // hangup the call for this room, which may not be the room in props
      // (e.g. conferences which will hangup the 1:1 room instead)
      room_id: call.roomId
    });
  };

  return _react.default.createElement(AccessibleButton, {
    className: "mx_MessageComposer_button mx_MessageComposer_hangup",
    onClick: onHangupClick,
    title: (0, _languageHandler._t)('Hangup')
  });
}

HangupButton.propTypes = {
  roomId: _propTypes.default.string.isRequired
};

class UploadButton extends _react.default.Component {
  constructor(props) {
    super(props);
    this.onUploadClick = this.onUploadClick.bind(this);
    this.onUploadFileInputChange = this.onUploadFileInputChange.bind(this);
    this._uploadInput = (0, _react.createRef)();
  }

  onUploadClick(ev) {
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
      _dispatcher.default.dispatch({
        action: 'require_registration'
      });

      return;
    }

    this._uploadInput.current.click();
  }

  onUploadFileInputChange(ev) {
    if (ev.target.files.length === 0) return; // take a copy so we can safely reset the value of the form control
    // (Note it is a FileList: we can't use slice or sesnible iteration).

    const tfiles = [];

    for (let i = 0; i < ev.target.files.length; ++i) {
      tfiles.push(ev.target.files[i]);
    }

    _ContentMessages.default.sharedInstance().sendContentListToRoom(tfiles, this.props.roomId, _MatrixClientPeg.MatrixClientPeg.get()); // This is the onChange handler for a file form control, but we're
    // not keeping any state, so reset the value of the form control
    // to empty.
    // NB. we need to set 'value': the 'files' property is immutable.


    ev.target.value = '';
  }

  render() {
    const uploadInputStyle = {
      display: 'none'
    };
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement(AccessibleButton, {
      className: "mx_MessageComposer_button mx_MessageComposer_upload",
      onClick: this.onUploadClick,
      title: (0, _languageHandler._t)('Upload file')
    }, _react.default.createElement("input", {
      ref: this._uploadInput,
      type: "file",
      style: uploadInputStyle,
      multiple: true,
      onChange: this.onUploadFileInputChange
    }));
  }

}

(0, _defineProperty2.default)(UploadButton, "propTypes", {
  roomId: _propTypes.default.string.isRequired
});

class MessageComposer extends _react.default.Component {
  constructor(props) {
    super(props);
    this.onInputStateChanged = this.onInputStateChanged.bind(this);
    this._onRoomStateEvents = this._onRoomStateEvents.bind(this);
    this._onRoomViewStoreUpdate = this._onRoomViewStoreUpdate.bind(this);
    this._onTombstoneClick = this._onTombstoneClick.bind(this);
    this.renderPlaceholderText = this.renderPlaceholderText.bind(this);
    this.state = {
      isQuoting: Boolean(_RoomViewStore.default.getQuotingEvent()),
      tombstone: this._getRoomTombstone(),
      canSendMessages: this.props.room.maySendMessage(),
      showCallButtons: _SettingsStore.default.getValue("showCallButtonsInComposer")
    };
  }

  componentDidMount() {
    _MatrixClientPeg.MatrixClientPeg.get().on("RoomState.events", this._onRoomStateEvents);

    this._roomStoreToken = _RoomViewStore.default.addListener(this._onRoomViewStoreUpdate);

    this._waitForOwnMember();
  }

  _waitForOwnMember() {
    // if we have the member already, do that
    const me = this.props.room.getMember(_MatrixClientPeg.MatrixClientPeg.get().getUserId());

    if (me) {
      this.setState({
        me
      });
      return;
    } // Otherwise, wait for member loading to finish and then update the member for the avatar.
    // The members should already be loading, and loadMembersIfNeeded
    // will return the promise for the existing operation


    this.props.room.loadMembersIfNeeded().then(() => {
      const me = this.props.room.getMember(_MatrixClientPeg.MatrixClientPeg.get().getUserId());
      this.setState({
        me
      });
    });
  }

  componentWillUnmount() {
    if (_MatrixClientPeg.MatrixClientPeg.get()) {
      _MatrixClientPeg.MatrixClientPeg.get().removeListener("RoomState.events", this._onRoomStateEvents);
    }

    if (this._roomStoreToken) {
      this._roomStoreToken.remove();
    }
  }

  _onRoomStateEvents(ev, state) {
    if (ev.getRoomId() !== this.props.room.roomId) return;

    if (ev.getType() === 'm.room.tombstone') {
      this.setState({
        tombstone: this._getRoomTombstone()
      });
    }

    if (ev.getType() === 'm.room.power_levels') {
      this.setState({
        canSendMessages: this.props.room.maySendMessage()
      });
    }
  }

  _getRoomTombstone() {
    return this.props.room.currentState.getStateEvents('m.room.tombstone', '');
  }

  _onRoomViewStoreUpdate() {
    const isQuoting = Boolean(_RoomViewStore.default.getQuotingEvent());
    if (this.state.isQuoting === isQuoting) return;
    this.setState({
      isQuoting
    });
  }

  onInputStateChanged(inputState) {
    // Merge the new input state with old to support partial updates
    inputState = Object.assign({}, this.state.inputState, inputState);
    this.setState({
      inputState
    });
  }

  _onTombstoneClick(ev) {
    ev.preventDefault();
    const replacementRoomId = this.state.tombstone.getContent()['replacement_room'];

    const replacementRoom = _MatrixClientPeg.MatrixClientPeg.get().getRoom(replacementRoomId);

    let createEventId = null;

    if (replacementRoom) {
      const createEvent = replacementRoom.currentState.getStateEvents('m.room.create', '');
      if (createEvent && createEvent.getId()) createEventId = createEvent.getId();
    }

    const viaServers = [this.state.tombstone.getSender().split(':').splice(1).join(':')];

    _dispatcher.default.dispatch({
      action: 'view_room',
      highlighted: true,
      event_id: createEventId,
      room_id: replacementRoomId,
      auto_join: true,
      // Try to join via the server that sent the event. This converts @something:example.org
      // into a server domain by splitting on colons and ignoring the first entry ("@something").
      via_servers: viaServers,
      opts: {
        // These are passed down to the js-sdk's /join call
        viaServers: viaServers
      }
    });
  }

  renderPlaceholderText() {
    if (_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      if (this.state.isQuoting) {
        if (this.props.e2eStatus) {
          return (0, _languageHandler._t)('Send an encrypted reply…');
        } else {
          return (0, _languageHandler._t)('Send a reply…');
        }
      } else {
        if (this.props.e2eStatus) {
          return (0, _languageHandler._t)('Send an encrypted message…');
        } else {
          return (0, _languageHandler._t)('Send a message…');
        }
      }
    } else {
      if (this.state.isQuoting) {
        if (this.props.e2eStatus) {
          return (0, _languageHandler._t)('Send an encrypted reply…');
        } else {
          return (0, _languageHandler._t)('Send a reply (unencrypted)…');
        }
      } else {
        if (this.props.e2eStatus) {
          return (0, _languageHandler._t)('Send an encrypted message…');
        } else {
          return (0, _languageHandler._t)('Send a message (unencrypted)…');
        }
      }
    }
  }

  render() {
    const controls = [this.state.me ? _react.default.createElement(ComposerAvatar, {
      key: "controls_avatar",
      me: this.state.me
    }) : null, this.props.e2eStatus ? _react.default.createElement(_E2EIcon.default, {
      key: "e2eIcon",
      status: this.props.e2eStatus,
      className: "mx_MessageComposer_e2eIcon"
    }) : null];

    if (!this.state.tombstone && this.state.canSendMessages) {
      // This also currently includes the call buttons. Really we should
      // check separately for whether we can call, but this is slightly
      // complex because of conference calls.
      const SendMessageComposer = sdk.getComponent("rooms.SendMessageComposer");
      const callInProgress = this.props.callState && this.props.callState !== 'ended';
      controls.push(_react.default.createElement(SendMessageComposer, {
        ref: c => this.messageComposerInput = c,
        key: "controls_input",
        room: this.props.room,
        placeholder: this.renderPlaceholderText(),
        permalinkCreator: this.props.permalinkCreator
      }), _react.default.createElement(_Stickerpicker.default, {
        key: "stickerpicker_controls_button",
        room: this.props.room
      }), _react.default.createElement(UploadButton, {
        key: "controls_upload",
        roomId: this.props.room.roomId
      }));

      if (this.state.showCallButtons) {
        if (callInProgress) {
          controls.push(_react.default.createElement(HangupButton, {
            key: "controls_hangup",
            roomId: this.props.room.roomId
          }));
        } else {
          controls.push(_react.default.createElement(CallButton, {
            key: "controls_call",
            roomId: this.props.room.roomId
          }), _react.default.createElement(VideoCallButton, {
            key: "controls_videocall",
            roomId: this.props.room.roomId
          }));
        }
      }
    } else if (this.state.tombstone) {
      const replacementRoomId = this.state.tombstone.getContent()['replacement_room'];
      const continuesLink = replacementRoomId ? _react.default.createElement("a", {
        href: (0, _Permalinks.makeRoomPermalink)(replacementRoomId),
        className: "mx_MessageComposer_roomReplaced_link",
        onClick: this._onTombstoneClick
      }, (0, _languageHandler._t)("The conversation continues here.")) : '';
      controls.push(_react.default.createElement("div", {
        className: "mx_MessageComposer_replaced_wrapper",
        key: "room_replaced"
      }, _react.default.createElement("div", {
        className: "mx_MessageComposer_replaced_valign"
      }, _react.default.createElement("img", {
        className: "mx_MessageComposer_roomReplaced_icon",
        src: require("../../../../res/img/room_replaced.svg")
      }), _react.default.createElement("span", {
        className: "mx_MessageComposer_roomReplaced_header"
      }, (0, _languageHandler._t)("This room has been replaced and is no longer active.")), _react.default.createElement("br", null), continuesLink)));
    } else {
      controls.push(_react.default.createElement("div", {
        key: "controls_error",
        className: "mx_MessageComposer_noperm_error"
      }, (0, _languageHandler._t)('You do not have permission to post to this room')));
    }

    return _react.default.createElement("div", {
      className: "mx_MessageComposer"
    }, _react.default.createElement("div", {
      className: "mx_MessageComposer_wrapper"
    }, _react.default.createElement("div", {
      className: "mx_MessageComposer_row"
    }, controls)));
  }

}

exports.default = MessageComposer;
MessageComposer.propTypes = {
  // js-sdk Room object
  room: _propTypes.default.object.isRequired,
  // string representing the current voip call state
  callState: _propTypes.default.string,
  // string representing the current room app drawer state
  showApps: _propTypes.default.bool
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL01lc3NhZ2VDb21wb3Nlci5qcyJdLCJuYW1lcyI6WyJDb21wb3NlckF2YXRhciIsInByb3BzIiwiTWVtYmVyU3RhdHVzTWVzc2FnZUF2YXRhciIsInNkayIsImdldENvbXBvbmVudCIsIm1lIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIkNhbGxCdXR0b24iLCJBY2Nlc3NpYmxlQnV0dG9uIiwib25Wb2ljZUNhbGxDbGljayIsImV2IiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJ0eXBlIiwicm9vbV9pZCIsInJvb21JZCIsInN0cmluZyIsIlZpZGVvQ2FsbEJ1dHRvbiIsIm9uQ2FsbENsaWNrIiwic2hpZnRLZXkiLCJIYW5ndXBCdXR0b24iLCJvbkhhbmd1cENsaWNrIiwiY2FsbCIsIkNhbGxIYW5kbGVyIiwiZ2V0Q2FsbEZvclJvb20iLCJVcGxvYWRCdXR0b24iLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwib25VcGxvYWRDbGljayIsImJpbmQiLCJvblVwbG9hZEZpbGVJbnB1dENoYW5nZSIsIl91cGxvYWRJbnB1dCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImlzR3Vlc3QiLCJjdXJyZW50IiwiY2xpY2siLCJ0YXJnZXQiLCJmaWxlcyIsImxlbmd0aCIsInRmaWxlcyIsImkiLCJwdXNoIiwiQ29udGVudE1lc3NhZ2VzIiwic2hhcmVkSW5zdGFuY2UiLCJzZW5kQ29udGVudExpc3RUb1Jvb20iLCJ2YWx1ZSIsInJlbmRlciIsInVwbG9hZElucHV0U3R5bGUiLCJkaXNwbGF5IiwiTWVzc2FnZUNvbXBvc2VyIiwib25JbnB1dFN0YXRlQ2hhbmdlZCIsIl9vblJvb21TdGF0ZUV2ZW50cyIsIl9vblJvb21WaWV3U3RvcmVVcGRhdGUiLCJfb25Ub21ic3RvbmVDbGljayIsInJlbmRlclBsYWNlaG9sZGVyVGV4dCIsInN0YXRlIiwiaXNRdW90aW5nIiwiQm9vbGVhbiIsIlJvb21WaWV3U3RvcmUiLCJnZXRRdW90aW5nRXZlbnQiLCJ0b21ic3RvbmUiLCJfZ2V0Um9vbVRvbWJzdG9uZSIsImNhblNlbmRNZXNzYWdlcyIsInJvb20iLCJtYXlTZW5kTWVzc2FnZSIsInNob3dDYWxsQnV0dG9ucyIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsImNvbXBvbmVudERpZE1vdW50Iiwib24iLCJfcm9vbVN0b3JlVG9rZW4iLCJhZGRMaXN0ZW5lciIsIl93YWl0Rm9yT3duTWVtYmVyIiwiZ2V0TWVtYmVyIiwiZ2V0VXNlcklkIiwic2V0U3RhdGUiLCJsb2FkTWVtYmVyc0lmTmVlZGVkIiwidGhlbiIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlTGlzdGVuZXIiLCJyZW1vdmUiLCJnZXRSb29tSWQiLCJnZXRUeXBlIiwiY3VycmVudFN0YXRlIiwiZ2V0U3RhdGVFdmVudHMiLCJpbnB1dFN0YXRlIiwiT2JqZWN0IiwiYXNzaWduIiwicHJldmVudERlZmF1bHQiLCJyZXBsYWNlbWVudFJvb21JZCIsImdldENvbnRlbnQiLCJyZXBsYWNlbWVudFJvb20iLCJnZXRSb29tIiwiY3JlYXRlRXZlbnRJZCIsImNyZWF0ZUV2ZW50IiwiZ2V0SWQiLCJ2aWFTZXJ2ZXJzIiwiZ2V0U2VuZGVyIiwic3BsaXQiLCJzcGxpY2UiLCJqb2luIiwiaGlnaGxpZ2h0ZWQiLCJldmVudF9pZCIsImF1dG9fam9pbiIsInZpYV9zZXJ2ZXJzIiwib3B0cyIsImlzRmVhdHVyZUVuYWJsZWQiLCJlMmVTdGF0dXMiLCJjb250cm9scyIsIlNlbmRNZXNzYWdlQ29tcG9zZXIiLCJjYWxsSW5Qcm9ncmVzcyIsImNhbGxTdGF0ZSIsImMiLCJtZXNzYWdlQ29tcG9zZXJJbnB1dCIsInBlcm1hbGlua0NyZWF0b3IiLCJjb250aW51ZXNMaW5rIiwicmVxdWlyZSIsInNob3dBcHBzIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUE4QkEsU0FBU0EsY0FBVCxDQUF3QkMsS0FBeEIsRUFBK0I7QUFDM0IsUUFBTUMseUJBQXlCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixtQ0FBakIsQ0FBbEM7QUFDQSxTQUFPO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNILDZCQUFDLHlCQUFEO0FBQTJCLElBQUEsTUFBTSxFQUFFSCxLQUFLLENBQUNJLEVBQXpDO0FBQTZDLElBQUEsS0FBSyxFQUFFLEVBQXBEO0FBQXdELElBQUEsTUFBTSxFQUFFO0FBQWhFLElBREcsQ0FBUDtBQUdIOztBQUVETCxjQUFjLENBQUNNLFNBQWYsR0FBMkI7QUFDdkJELEVBQUFBLEVBQUUsRUFBRUUsbUJBQVVDLE1BQVYsQ0FBaUJDO0FBREUsQ0FBM0I7O0FBSUEsU0FBU0MsVUFBVCxDQUFvQlQsS0FBcEIsRUFBMkI7QUFDdkIsUUFBTVUsZ0JBQWdCLEdBQUdSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7O0FBQ0EsUUFBTVEsZ0JBQWdCLEdBQUlDLEVBQUQsSUFBUTtBQUM3QkMsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUQyxNQUFBQSxJQUFJLEVBQUUsT0FGRztBQUdUQyxNQUFBQSxPQUFPLEVBQUVqQixLQUFLLENBQUNrQjtBQUhOLEtBQWI7QUFLSCxHQU5EOztBQVFBLFNBQVEsNkJBQUMsZ0JBQUQ7QUFBa0IsSUFBQSxTQUFTLEVBQUMsd0RBQTVCO0FBQ0EsSUFBQSxPQUFPLEVBQUVQLGdCQURUO0FBRUEsSUFBQSxLQUFLLEVBQUUseUJBQUcsWUFBSDtBQUZQLElBQVI7QUFJSDs7QUFFREYsVUFBVSxDQUFDSixTQUFYLEdBQXVCO0FBQ25CYSxFQUFBQSxNQUFNLEVBQUVaLG1CQUFVYSxNQUFWLENBQWlCWDtBQUROLENBQXZCOztBQUlBLFNBQVNZLGVBQVQsQ0FBeUJwQixLQUF6QixFQUFnQztBQUM1QixRQUFNVSxnQkFBZ0IsR0FBR1IsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6Qjs7QUFDQSxRQUFNa0IsV0FBVyxHQUFJVCxFQUFELElBQVE7QUFDeEJDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVEMsTUFBQUEsSUFBSSxFQUFFSixFQUFFLENBQUNVLFFBQUgsR0FBYyxlQUFkLEdBQWdDLE9BRjdCO0FBR1RMLE1BQUFBLE9BQU8sRUFBRWpCLEtBQUssQ0FBQ2tCO0FBSE4sS0FBYjtBQUtILEdBTkQ7O0FBUUEsU0FBTyw2QkFBQyxnQkFBRDtBQUFrQixJQUFBLFNBQVMsRUFBQyx3REFBNUI7QUFDSCxJQUFBLE9BQU8sRUFBRUcsV0FETjtBQUVILElBQUEsS0FBSyxFQUFFLHlCQUFHLFlBQUg7QUFGSixJQUFQO0FBSUg7O0FBRURELGVBQWUsQ0FBQ2YsU0FBaEIsR0FBNEI7QUFDeEJhLEVBQUFBLE1BQU0sRUFBRVosbUJBQVVhLE1BQVYsQ0FBaUJYO0FBREQsQ0FBNUI7O0FBSUEsU0FBU2UsWUFBVCxDQUFzQnZCLEtBQXRCLEVBQTZCO0FBQ3pCLFFBQU1VLGdCQUFnQixHQUFHUixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCOztBQUNBLFFBQU1xQixhQUFhLEdBQUcsTUFBTTtBQUN4QixVQUFNQyxJQUFJLEdBQUdDLHFCQUFZQyxjQUFaLENBQTJCM0IsS0FBSyxDQUFDa0IsTUFBakMsQ0FBYjs7QUFDQSxRQUFJLENBQUNPLElBQUwsRUFBVztBQUNQO0FBQ0g7O0FBQ0RaLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLFFBREM7QUFFVDtBQUNBO0FBQ0FFLE1BQUFBLE9BQU8sRUFBRVEsSUFBSSxDQUFDUDtBQUpMLEtBQWI7QUFNSCxHQVhEOztBQVlBLFNBQVEsNkJBQUMsZ0JBQUQ7QUFBa0IsSUFBQSxTQUFTLEVBQUMscURBQTVCO0FBQ0EsSUFBQSxPQUFPLEVBQUVNLGFBRFQ7QUFFQSxJQUFBLEtBQUssRUFBRSx5QkFBRyxRQUFIO0FBRlAsSUFBUjtBQUlIOztBQUVERCxZQUFZLENBQUNsQixTQUFiLEdBQXlCO0FBQ3JCYSxFQUFBQSxNQUFNLEVBQUVaLG1CQUFVYSxNQUFWLENBQWlCWDtBQURKLENBQXpCOztBQUlBLE1BQU1vQixZQUFOLFNBQTJCQyxlQUFNQyxTQUFqQyxDQUEyQztBQUt2Q0MsRUFBQUEsV0FBVyxDQUFDL0IsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUtnQyxhQUFMLEdBQXFCLEtBQUtBLGFBQUwsQ0FBbUJDLElBQW5CLENBQXdCLElBQXhCLENBQXJCO0FBQ0EsU0FBS0MsdUJBQUwsR0FBK0IsS0FBS0EsdUJBQUwsQ0FBNkJELElBQTdCLENBQWtDLElBQWxDLENBQS9CO0FBRUEsU0FBS0UsWUFBTCxHQUFvQix1QkFBcEI7QUFDSDs7QUFFREgsRUFBQUEsYUFBYSxDQUFDcEIsRUFBRCxFQUFLO0FBQ2QsUUFBSXdCLGlDQUFnQkMsR0FBaEIsR0FBc0JDLE9BQXRCLEVBQUosRUFBcUM7QUFDakN6QiwwQkFBSUMsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRTtBQUFULE9BQWI7O0FBQ0E7QUFDSDs7QUFDRCxTQUFLb0IsWUFBTCxDQUFrQkksT0FBbEIsQ0FBMEJDLEtBQTFCO0FBQ0g7O0FBRUROLEVBQUFBLHVCQUF1QixDQUFDdEIsRUFBRCxFQUFLO0FBQ3hCLFFBQUlBLEVBQUUsQ0FBQzZCLE1BQUgsQ0FBVUMsS0FBVixDQUFnQkMsTUFBaEIsS0FBMkIsQ0FBL0IsRUFBa0MsT0FEVixDQUd4QjtBQUNBOztBQUNBLFVBQU1DLE1BQU0sR0FBRyxFQUFmOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR2pDLEVBQUUsQ0FBQzZCLE1BQUgsQ0FBVUMsS0FBVixDQUFnQkMsTUFBcEMsRUFBNEMsRUFBRUUsQ0FBOUMsRUFBaUQ7QUFDN0NELE1BQUFBLE1BQU0sQ0FBQ0UsSUFBUCxDQUFZbEMsRUFBRSxDQUFDNkIsTUFBSCxDQUFVQyxLQUFWLENBQWdCRyxDQUFoQixDQUFaO0FBQ0g7O0FBRURFLDZCQUFnQkMsY0FBaEIsR0FBaUNDLHFCQUFqQyxDQUNJTCxNQURKLEVBQ1ksS0FBSzVDLEtBQUwsQ0FBV2tCLE1BRHZCLEVBQytCa0IsaUNBQWdCQyxHQUFoQixFQUQvQixFQVZ3QixDQWN4QjtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0F6QixJQUFBQSxFQUFFLENBQUM2QixNQUFILENBQVVTLEtBQVYsR0FBa0IsRUFBbEI7QUFDSDs7QUFFREMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsZ0JBQWdCLEdBQUc7QUFBQ0MsTUFBQUEsT0FBTyxFQUFFO0FBQVYsS0FBekI7QUFDQSxVQUFNM0MsZ0JBQWdCLEdBQUdSLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxXQUNJLDZCQUFDLGdCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLHFEQUE1QjtBQUNJLE1BQUEsT0FBTyxFQUFFLEtBQUs2QixhQURsQjtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGFBQUg7QUFGWCxPQUlJO0FBQ0ksTUFBQSxHQUFHLEVBQUUsS0FBS0csWUFEZDtBQUVJLE1BQUEsSUFBSSxFQUFDLE1BRlQ7QUFHSSxNQUFBLEtBQUssRUFBRWlCLGdCQUhYO0FBSUksTUFBQSxRQUFRLE1BSlo7QUFLSSxNQUFBLFFBQVEsRUFBRSxLQUFLbEI7QUFMbkIsTUFKSixDQURKO0FBY0g7O0FBM0RzQzs7OEJBQXJDTixZLGVBQ2lCO0FBQ2ZWLEVBQUFBLE1BQU0sRUFBRVosbUJBQVVhLE1BQVYsQ0FBaUJYO0FBRFYsQzs7QUE2RFIsTUFBTThDLGVBQU4sU0FBOEJ6QixlQUFNQyxTQUFwQyxDQUE4QztBQUN6REMsRUFBQUEsV0FBVyxDQUFDL0IsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUt1RCxtQkFBTCxHQUEyQixLQUFLQSxtQkFBTCxDQUF5QnRCLElBQXpCLENBQThCLElBQTlCLENBQTNCO0FBQ0EsU0FBS3VCLGtCQUFMLEdBQTBCLEtBQUtBLGtCQUFMLENBQXdCdkIsSUFBeEIsQ0FBNkIsSUFBN0IsQ0FBMUI7QUFDQSxTQUFLd0Isc0JBQUwsR0FBOEIsS0FBS0Esc0JBQUwsQ0FBNEJ4QixJQUE1QixDQUFpQyxJQUFqQyxDQUE5QjtBQUNBLFNBQUt5QixpQkFBTCxHQUF5QixLQUFLQSxpQkFBTCxDQUF1QnpCLElBQXZCLENBQTRCLElBQTVCLENBQXpCO0FBQ0EsU0FBSzBCLHFCQUFMLEdBQTZCLEtBQUtBLHFCQUFMLENBQTJCMUIsSUFBM0IsQ0FBZ0MsSUFBaEMsQ0FBN0I7QUFFQSxTQUFLMkIsS0FBTCxHQUFhO0FBQ1RDLE1BQUFBLFNBQVMsRUFBRUMsT0FBTyxDQUFDQyx1QkFBY0MsZUFBZCxFQUFELENBRFQ7QUFFVEMsTUFBQUEsU0FBUyxFQUFFLEtBQUtDLGlCQUFMLEVBRkY7QUFHVEMsTUFBQUEsZUFBZSxFQUFFLEtBQUtuRSxLQUFMLENBQVdvRSxJQUFYLENBQWdCQyxjQUFoQixFQUhSO0FBSVRDLE1BQUFBLGVBQWUsRUFBRUMsdUJBQWNDLFFBQWQsQ0FBdUIsMkJBQXZCO0FBSlIsS0FBYjtBQU1IOztBQUVEQyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQnJDLHFDQUFnQkMsR0FBaEIsR0FBc0JxQyxFQUF0QixDQUF5QixrQkFBekIsRUFBNkMsS0FBS2xCLGtCQUFsRDs7QUFDQSxTQUFLbUIsZUFBTCxHQUF1QlosdUJBQWNhLFdBQWQsQ0FBMEIsS0FBS25CLHNCQUEvQixDQUF2Qjs7QUFDQSxTQUFLb0IsaUJBQUw7QUFDSDs7QUFFREEsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEI7QUFDQSxVQUFNekUsRUFBRSxHQUFHLEtBQUtKLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JVLFNBQWhCLENBQTBCMUMsaUNBQWdCQyxHQUFoQixHQUFzQjBDLFNBQXRCLEVBQTFCLENBQVg7O0FBQ0EsUUFBSTNFLEVBQUosRUFBUTtBQUNKLFdBQUs0RSxRQUFMLENBQWM7QUFBQzVFLFFBQUFBO0FBQUQsT0FBZDtBQUNBO0FBQ0gsS0FOZSxDQU9oQjtBQUNBO0FBQ0E7OztBQUNBLFNBQUtKLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JhLG1CQUFoQixHQUFzQ0MsSUFBdEMsQ0FBMkMsTUFBTTtBQUM3QyxZQUFNOUUsRUFBRSxHQUFHLEtBQUtKLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JVLFNBQWhCLENBQTBCMUMsaUNBQWdCQyxHQUFoQixHQUFzQjBDLFNBQXRCLEVBQTFCLENBQVg7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFBQzVFLFFBQUFBO0FBQUQsT0FBZDtBQUNILEtBSEQ7QUFJSDs7QUFFRCtFLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFFBQUkvQyxpQ0FBZ0JDLEdBQWhCLEVBQUosRUFBMkI7QUFDdkJELHVDQUFnQkMsR0FBaEIsR0FBc0IrQyxjQUF0QixDQUFxQyxrQkFBckMsRUFBeUQsS0FBSzVCLGtCQUE5RDtBQUNIOztBQUNELFFBQUksS0FBS21CLGVBQVQsRUFBMEI7QUFDdEIsV0FBS0EsZUFBTCxDQUFxQlUsTUFBckI7QUFDSDtBQUNKOztBQUVEN0IsRUFBQUEsa0JBQWtCLENBQUM1QyxFQUFELEVBQUtnRCxLQUFMLEVBQVk7QUFDMUIsUUFBSWhELEVBQUUsQ0FBQzBFLFNBQUgsT0FBbUIsS0FBS3RGLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JsRCxNQUF2QyxFQUErQzs7QUFFL0MsUUFBSU4sRUFBRSxDQUFDMkUsT0FBSCxPQUFpQixrQkFBckIsRUFBeUM7QUFDckMsV0FBS1AsUUFBTCxDQUFjO0FBQUNmLFFBQUFBLFNBQVMsRUFBRSxLQUFLQyxpQkFBTDtBQUFaLE9BQWQ7QUFDSDs7QUFDRCxRQUFJdEQsRUFBRSxDQUFDMkUsT0FBSCxPQUFpQixxQkFBckIsRUFBNEM7QUFDeEMsV0FBS1AsUUFBTCxDQUFjO0FBQUNiLFFBQUFBLGVBQWUsRUFBRSxLQUFLbkUsS0FBTCxDQUFXb0UsSUFBWCxDQUFnQkMsY0FBaEI7QUFBbEIsT0FBZDtBQUNIO0FBQ0o7O0FBRURILEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFdBQU8sS0FBS2xFLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JvQixZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsa0JBQTVDLEVBQWdFLEVBQWhFLENBQVA7QUFDSDs7QUFFRGhDLEVBQUFBLHNCQUFzQixHQUFHO0FBQ3JCLFVBQU1JLFNBQVMsR0FBR0MsT0FBTyxDQUFDQyx1QkFBY0MsZUFBZCxFQUFELENBQXpCO0FBQ0EsUUFBSSxLQUFLSixLQUFMLENBQVdDLFNBQVgsS0FBeUJBLFNBQTdCLEVBQXdDO0FBQ3hDLFNBQUttQixRQUFMLENBQWM7QUFBRW5CLE1BQUFBO0FBQUYsS0FBZDtBQUNIOztBQUVETixFQUFBQSxtQkFBbUIsQ0FBQ21DLFVBQUQsRUFBYTtBQUM1QjtBQUNBQSxJQUFBQSxVQUFVLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS2hDLEtBQUwsQ0FBVzhCLFVBQTdCLEVBQXlDQSxVQUF6QyxDQUFiO0FBQ0EsU0FBS1YsUUFBTCxDQUFjO0FBQUNVLE1BQUFBO0FBQUQsS0FBZDtBQUNIOztBQUVEaEMsRUFBQUEsaUJBQWlCLENBQUM5QyxFQUFELEVBQUs7QUFDbEJBLElBQUFBLEVBQUUsQ0FBQ2lGLGNBQUg7QUFFQSxVQUFNQyxpQkFBaUIsR0FBRyxLQUFLbEMsS0FBTCxDQUFXSyxTQUFYLENBQXFCOEIsVUFBckIsR0FBa0Msa0JBQWxDLENBQTFCOztBQUNBLFVBQU1DLGVBQWUsR0FBRzVELGlDQUFnQkMsR0FBaEIsR0FBc0I0RCxPQUF0QixDQUE4QkgsaUJBQTlCLENBQXhCOztBQUNBLFFBQUlJLGFBQWEsR0FBRyxJQUFwQjs7QUFDQSxRQUFJRixlQUFKLEVBQXFCO0FBQ2pCLFlBQU1HLFdBQVcsR0FBR0gsZUFBZSxDQUFDUixZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsZUFBNUMsRUFBNkQsRUFBN0QsQ0FBcEI7QUFDQSxVQUFJVSxXQUFXLElBQUlBLFdBQVcsQ0FBQ0MsS0FBWixFQUFuQixFQUF3Q0YsYUFBYSxHQUFHQyxXQUFXLENBQUNDLEtBQVosRUFBaEI7QUFDM0M7O0FBRUQsVUFBTUMsVUFBVSxHQUFHLENBQUMsS0FBS3pDLEtBQUwsQ0FBV0ssU0FBWCxDQUFxQnFDLFNBQXJCLEdBQWlDQyxLQUFqQyxDQUF1QyxHQUF2QyxFQUE0Q0MsTUFBNUMsQ0FBbUQsQ0FBbkQsRUFBc0RDLElBQXRELENBQTJELEdBQTNELENBQUQsQ0FBbkI7O0FBQ0E1Rix3QkFBSUMsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxXQURDO0FBRVQyRixNQUFBQSxXQUFXLEVBQUUsSUFGSjtBQUdUQyxNQUFBQSxRQUFRLEVBQUVULGFBSEQ7QUFJVGpGLE1BQUFBLE9BQU8sRUFBRTZFLGlCQUpBO0FBS1RjLE1BQUFBLFNBQVMsRUFBRSxJQUxGO0FBT1Q7QUFDQTtBQUNBQyxNQUFBQSxXQUFXLEVBQUVSLFVBVEo7QUFVVFMsTUFBQUEsSUFBSSxFQUFFO0FBQ0Y7QUFDQVQsUUFBQUEsVUFBVSxFQUFFQTtBQUZWO0FBVkcsS0FBYjtBQWVIOztBQUVEMUMsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsUUFBSVksdUJBQWN3QyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBSixFQUE2RDtBQUN6RCxVQUFJLEtBQUtuRCxLQUFMLENBQVdDLFNBQWYsRUFBMEI7QUFDdEIsWUFBSSxLQUFLN0QsS0FBTCxDQUFXZ0gsU0FBZixFQUEwQjtBQUN0QixpQkFBTyx5QkFBRywwQkFBSCxDQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsaUJBQU8seUJBQUcsZUFBSCxDQUFQO0FBQ0g7QUFDSixPQU5ELE1BTU87QUFDSCxZQUFJLEtBQUtoSCxLQUFMLENBQVdnSCxTQUFmLEVBQTBCO0FBQ3RCLGlCQUFPLHlCQUFHLDRCQUFILENBQVA7QUFDSCxTQUZELE1BRU87QUFDSCxpQkFBTyx5QkFBRyxpQkFBSCxDQUFQO0FBQ0g7QUFDSjtBQUNKLEtBZEQsTUFjTztBQUNILFVBQUksS0FBS3BELEtBQUwsQ0FBV0MsU0FBZixFQUEwQjtBQUN0QixZQUFJLEtBQUs3RCxLQUFMLENBQVdnSCxTQUFmLEVBQTBCO0FBQ3RCLGlCQUFPLHlCQUFHLDBCQUFILENBQVA7QUFDSCxTQUZELE1BRU87QUFDSCxpQkFBTyx5QkFBRyw2QkFBSCxDQUFQO0FBQ0g7QUFDSixPQU5ELE1BTU87QUFDSCxZQUFJLEtBQUtoSCxLQUFMLENBQVdnSCxTQUFmLEVBQTBCO0FBQ3RCLGlCQUFPLHlCQUFHLDRCQUFILENBQVA7QUFDSCxTQUZELE1BRU87QUFDSCxpQkFBTyx5QkFBRywrQkFBSCxDQUFQO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7O0FBRUQ3RCxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNOEQsUUFBUSxHQUFHLENBQ2IsS0FBS3JELEtBQUwsQ0FBV3hELEVBQVgsR0FBZ0IsNkJBQUMsY0FBRDtBQUFnQixNQUFBLEdBQUcsRUFBQyxpQkFBcEI7QUFBc0MsTUFBQSxFQUFFLEVBQUUsS0FBS3dELEtBQUwsQ0FBV3hEO0FBQXJELE1BQWhCLEdBQThFLElBRGpFLEVBRWIsS0FBS0osS0FBTCxDQUFXZ0gsU0FBWCxHQUNJLDZCQUFDLGdCQUFEO0FBQVMsTUFBQSxHQUFHLEVBQUMsU0FBYjtBQUF1QixNQUFBLE1BQU0sRUFBRSxLQUFLaEgsS0FBTCxDQUFXZ0gsU0FBMUM7QUFBcUQsTUFBQSxTQUFTLEVBQUM7QUFBL0QsTUFESixHQUVJLElBSlMsQ0FBakI7O0FBT0EsUUFBSSxDQUFDLEtBQUtwRCxLQUFMLENBQVdLLFNBQVosSUFBeUIsS0FBS0wsS0FBTCxDQUFXTyxlQUF4QyxFQUF5RDtBQUNyRDtBQUNBO0FBQ0E7QUFFQSxZQUFNK0MsbUJBQW1CLEdBQUdoSCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQTVCO0FBQ0EsWUFBTWdILGNBQWMsR0FBRyxLQUFLbkgsS0FBTCxDQUFXb0gsU0FBWCxJQUF3QixLQUFLcEgsS0FBTCxDQUFXb0gsU0FBWCxLQUF5QixPQUF4RTtBQUVBSCxNQUFBQSxRQUFRLENBQUNuRSxJQUFULENBQ0ksNkJBQUMsbUJBQUQ7QUFDSSxRQUFBLEdBQUcsRUFBR3VFLENBQUQsSUFBTyxLQUFLQyxvQkFBTCxHQUE0QkQsQ0FENUM7QUFFSSxRQUFBLEdBQUcsRUFBQyxnQkFGUjtBQUdJLFFBQUEsSUFBSSxFQUFFLEtBQUtySCxLQUFMLENBQVdvRSxJQUhyQjtBQUlJLFFBQUEsV0FBVyxFQUFFLEtBQUtULHFCQUFMLEVBSmpCO0FBS0ksUUFBQSxnQkFBZ0IsRUFBRSxLQUFLM0QsS0FBTCxDQUFXdUg7QUFMakMsUUFESixFQU9JLDZCQUFDLHNCQUFEO0FBQWUsUUFBQSxHQUFHLEVBQUMsK0JBQW5CO0FBQW1ELFFBQUEsSUFBSSxFQUFFLEtBQUt2SCxLQUFMLENBQVdvRTtBQUFwRSxRQVBKLEVBUUksNkJBQUMsWUFBRDtBQUFjLFFBQUEsR0FBRyxFQUFDLGlCQUFsQjtBQUFvQyxRQUFBLE1BQU0sRUFBRSxLQUFLcEUsS0FBTCxDQUFXb0UsSUFBWCxDQUFnQmxEO0FBQTVELFFBUko7O0FBV0EsVUFBSSxLQUFLMEMsS0FBTCxDQUFXVSxlQUFmLEVBQWdDO0FBQzVCLFlBQUk2QyxjQUFKLEVBQW9CO0FBQ2hCRixVQUFBQSxRQUFRLENBQUNuRSxJQUFULENBQ0ksNkJBQUMsWUFBRDtBQUFjLFlBQUEsR0FBRyxFQUFDLGlCQUFsQjtBQUFvQyxZQUFBLE1BQU0sRUFBRSxLQUFLOUMsS0FBTCxDQUFXb0UsSUFBWCxDQUFnQmxEO0FBQTVELFlBREo7QUFHSCxTQUpELE1BSU87QUFDSCtGLFVBQUFBLFFBQVEsQ0FBQ25FLElBQVQsQ0FDSSw2QkFBQyxVQUFEO0FBQVksWUFBQSxHQUFHLEVBQUMsZUFBaEI7QUFBZ0MsWUFBQSxNQUFNLEVBQUUsS0FBSzlDLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JsRDtBQUF4RCxZQURKLEVBRUksNkJBQUMsZUFBRDtBQUFpQixZQUFBLEdBQUcsRUFBQyxvQkFBckI7QUFBMEMsWUFBQSxNQUFNLEVBQUUsS0FBS2xCLEtBQUwsQ0FBV29FLElBQVgsQ0FBZ0JsRDtBQUFsRSxZQUZKO0FBSUg7QUFDSjtBQUNKLEtBL0JELE1BK0JPLElBQUksS0FBSzBDLEtBQUwsQ0FBV0ssU0FBZixFQUEwQjtBQUM3QixZQUFNNkIsaUJBQWlCLEdBQUcsS0FBS2xDLEtBQUwsQ0FBV0ssU0FBWCxDQUFxQjhCLFVBQXJCLEdBQWtDLGtCQUFsQyxDQUExQjtBQUVBLFlBQU15QixhQUFhLEdBQUcxQixpQkFBaUIsR0FDbkM7QUFBRyxRQUFBLElBQUksRUFBRSxtQ0FBa0JBLGlCQUFsQixDQUFUO0FBQ0ksUUFBQSxTQUFTLEVBQUMsc0NBRGQ7QUFFSSxRQUFBLE9BQU8sRUFBRSxLQUFLcEM7QUFGbEIsU0FJSyx5QkFBRyxrQ0FBSCxDQUpMLENBRG1DLEdBT25DLEVBUEo7QUFTQXVELE1BQUFBLFFBQVEsQ0FBQ25FLElBQVQsQ0FBYztBQUFLLFFBQUEsU0FBUyxFQUFDLHFDQUFmO0FBQXFELFFBQUEsR0FBRyxFQUFDO0FBQXpELFNBQ1Y7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFBSyxRQUFBLFNBQVMsRUFBQyxzQ0FBZjtBQUFzRCxRQUFBLEdBQUcsRUFBRTJFLE9BQU8sQ0FBQyx1Q0FBRDtBQUFsRSxRQURKLEVBRUk7QUFBTSxRQUFBLFNBQVMsRUFBQztBQUFoQixTQUNLLHlCQUFHLHNEQUFILENBREwsQ0FGSixFQUlXLHdDQUpYLEVBS01ELGFBTE4sQ0FEVSxDQUFkO0FBU0gsS0FyQk0sTUFxQkE7QUFDSFAsTUFBQUEsUUFBUSxDQUFDbkUsSUFBVCxDQUNJO0FBQUssUUFBQSxHQUFHLEVBQUMsZ0JBQVQ7QUFBMEIsUUFBQSxTQUFTLEVBQUM7QUFBcEMsU0FDTSx5QkFBRyxpREFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNbUUsUUFETixDQURKLENBREosQ0FESjtBQVNIOztBQXJOd0Q7OztBQXdON0QzRCxlQUFlLENBQUNqRCxTQUFoQixHQUE0QjtBQUN4QjtBQUNBK0QsRUFBQUEsSUFBSSxFQUFFOUQsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRkM7QUFJeEI7QUFDQTRHLEVBQUFBLFNBQVMsRUFBRTlHLG1CQUFVYSxNQUxHO0FBT3hCO0FBQ0F1RyxFQUFBQSxRQUFRLEVBQUVwSCxtQkFBVXFIO0FBUkksQ0FBNUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IENhbGxIYW5kbGVyIGZyb20gJy4uLy4uLy4uL0NhbGxIYW5kbGVyJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBSb29tVmlld1N0b3JlIGZyb20gJy4uLy4uLy4uL3N0b3Jlcy9Sb29tVmlld1N0b3JlJztcclxuaW1wb3J0IFN0aWNrZXJwaWNrZXIgZnJvbSAnLi9TdGlja2VycGlja2VyJztcclxuaW1wb3J0IHsgbWFrZVJvb21QZXJtYWxpbmsgfSBmcm9tICcuLi8uLi8uLi91dGlscy9wZXJtYWxpbmtzL1Blcm1hbGlua3MnO1xyXG5pbXBvcnQgQ29udGVudE1lc3NhZ2VzIGZyb20gJy4uLy4uLy4uL0NvbnRlbnRNZXNzYWdlcyc7XHJcbmltcG9ydCBFMkVJY29uIGZyb20gJy4vRTJFSWNvbic7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcblxyXG5mdW5jdGlvbiBDb21wb3NlckF2YXRhcihwcm9wcykge1xyXG4gICAgY29uc3QgTWVtYmVyU3RhdHVzTWVzc2FnZUF2YXRhciA9IHNkay5nZXRDb21wb25lbnQoJ2F2YXRhcnMuTWVtYmVyU3RhdHVzTWVzc2FnZUF2YXRhcicpO1xyXG4gICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX2F2YXRhclwiPlxyXG4gICAgICAgIDxNZW1iZXJTdGF0dXNNZXNzYWdlQXZhdGFyIG1lbWJlcj17cHJvcHMubWV9IHdpZHRoPXsyNH0gaGVpZ2h0PXsyNH0gLz5cclxuICAgIDwvZGl2PjtcclxufVxyXG5cclxuQ29tcG9zZXJBdmF0YXIucHJvcFR5cGVzID0ge1xyXG4gICAgbWU6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxufTtcclxuXHJcbmZ1bmN0aW9uIENhbGxCdXR0b24ocHJvcHMpIHtcclxuICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICBjb25zdCBvblZvaWNlQ2FsbENsaWNrID0gKGV2KSA9PiB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAncGxhY2VfY2FsbCcsXHJcbiAgICAgICAgICAgIHR5cGU6IFwidm9pY2VcIixcclxuICAgICAgICAgICAgcm9vbV9pZDogcHJvcHMucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gKDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9idXR0b24gbXhfTWVzc2FnZUNvbXBvc2VyX3ZvaWNlY2FsbFwiXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9e29uVm9pY2VDYWxsQ2xpY2t9XHJcbiAgICAgICAgICAgIHRpdGxlPXtfdCgnVm9pY2UgY2FsbCcpfVxyXG4gICAgICAgIC8+KTtcclxufVxyXG5cclxuQ2FsbEJ1dHRvbi5wcm9wVHlwZXMgPSB7XHJcbiAgICByb29tSWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxufTtcclxuXHJcbmZ1bmN0aW9uIFZpZGVvQ2FsbEJ1dHRvbihwcm9wcykge1xyXG4gICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgIGNvbnN0IG9uQ2FsbENsaWNrID0gKGV2KSA9PiB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAncGxhY2VfY2FsbCcsXHJcbiAgICAgICAgICAgIHR5cGU6IGV2LnNoaWZ0S2V5ID8gXCJzY3JlZW5zaGFyaW5nXCIgOiBcInZpZGVvXCIsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHByb3BzLnJvb21JZCxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9idXR0b24gbXhfTWVzc2FnZUNvbXBvc2VyX3ZpZGVvY2FsbFwiXHJcbiAgICAgICAgb25DbGljaz17b25DYWxsQ2xpY2t9XHJcbiAgICAgICAgdGl0bGU9e190KCdWaWRlbyBjYWxsJyl9XHJcbiAgICAvPjtcclxufVxyXG5cclxuVmlkZW9DYWxsQnV0dG9uLnByb3BUeXBlcyA9IHtcclxuICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG59O1xyXG5cclxuZnVuY3Rpb24gSGFuZ3VwQnV0dG9uKHByb3BzKSB7XHJcbiAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgY29uc3Qgb25IYW5ndXBDbGljayA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBjYWxsID0gQ2FsbEhhbmRsZXIuZ2V0Q2FsbEZvclJvb20ocHJvcHMucm9vbUlkKTtcclxuICAgICAgICBpZiAoIWNhbGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdoYW5ndXAnLFxyXG4gICAgICAgICAgICAvLyBoYW5ndXAgdGhlIGNhbGwgZm9yIHRoaXMgcm9vbSwgd2hpY2ggbWF5IG5vdCBiZSB0aGUgcm9vbSBpbiBwcm9wc1xyXG4gICAgICAgICAgICAvLyAoZS5nLiBjb25mZXJlbmNlcyB3aGljaCB3aWxsIGhhbmd1cCB0aGUgMToxIHJvb20gaW5zdGVhZClcclxuICAgICAgICAgICAgcm9vbV9pZDogY2FsbC5yb29tSWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuICg8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29tcG9zZXJfYnV0dG9uIG14X01lc3NhZ2VDb21wb3Nlcl9oYW5ndXBcIlxyXG4gICAgICAgICAgICBvbkNsaWNrPXtvbkhhbmd1cENsaWNrfVxyXG4gICAgICAgICAgICB0aXRsZT17X3QoJ0hhbmd1cCcpfVxyXG4gICAgICAgIC8+KTtcclxufVxyXG5cclxuSGFuZ3VwQnV0dG9uLnByb3BUeXBlcyA9IHtcclxuICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG59O1xyXG5cclxuY2xhc3MgVXBsb2FkQnV0dG9uIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgcm9vbUlkOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5vblVwbG9hZENsaWNrID0gdGhpcy5vblVwbG9hZENsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vblVwbG9hZEZpbGVJbnB1dENoYW5nZSA9IHRoaXMub25VcGxvYWRGaWxlSW5wdXRDaGFuZ2UuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fdXBsb2FkSW5wdXQgPSBjcmVhdGVSZWYoKTtcclxuICAgIH1cclxuXHJcbiAgICBvblVwbG9hZENsaWNrKGV2KSB7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdyZXF1aXJlX3JlZ2lzdHJhdGlvbid9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91cGxvYWRJbnB1dC5jdXJyZW50LmNsaWNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25VcGxvYWRGaWxlSW5wdXRDaGFuZ2UoZXYpIHtcclxuICAgICAgICBpZiAoZXYudGFyZ2V0LmZpbGVzLmxlbmd0aCA9PT0gMCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyB0YWtlIGEgY29weSBzbyB3ZSBjYW4gc2FmZWx5IHJlc2V0IHRoZSB2YWx1ZSBvZiB0aGUgZm9ybSBjb250cm9sXHJcbiAgICAgICAgLy8gKE5vdGUgaXQgaXMgYSBGaWxlTGlzdDogd2UgY2FuJ3QgdXNlIHNsaWNlIG9yIHNlc25pYmxlIGl0ZXJhdGlvbikuXHJcbiAgICAgICAgY29uc3QgdGZpbGVzID0gW107XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBldi50YXJnZXQuZmlsZXMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgdGZpbGVzLnB1c2goZXYudGFyZ2V0LmZpbGVzW2ldKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIENvbnRlbnRNZXNzYWdlcy5zaGFyZWRJbnN0YW5jZSgpLnNlbmRDb250ZW50TGlzdFRvUm9vbShcclxuICAgICAgICAgICAgdGZpbGVzLCB0aGlzLnByb3BzLnJvb21JZCwgTWF0cml4Q2xpZW50UGVnLmdldCgpLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIC8vIFRoaXMgaXMgdGhlIG9uQ2hhbmdlIGhhbmRsZXIgZm9yIGEgZmlsZSBmb3JtIGNvbnRyb2wsIGJ1dCB3ZSdyZVxyXG4gICAgICAgIC8vIG5vdCBrZWVwaW5nIGFueSBzdGF0ZSwgc28gcmVzZXQgdGhlIHZhbHVlIG9mIHRoZSBmb3JtIGNvbnRyb2xcclxuICAgICAgICAvLyB0byBlbXB0eS5cclxuICAgICAgICAvLyBOQi4gd2UgbmVlZCB0byBzZXQgJ3ZhbHVlJzogdGhlICdmaWxlcycgcHJvcGVydHkgaXMgaW1tdXRhYmxlLlxyXG4gICAgICAgIGV2LnRhcmdldC52YWx1ZSA9ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCB1cGxvYWRJbnB1dFN0eWxlID0ge2Rpc3BsYXk6ICdub25lJ307XHJcbiAgICAgICAgY29uc3QgQWNjZXNzaWJsZUJ1dHRvbiA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLkFjY2Vzc2libGVCdXR0b24nKTtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29tcG9zZXJfYnV0dG9uIG14X01lc3NhZ2VDb21wb3Nlcl91cGxvYWRcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vblVwbG9hZENsaWNrfVxyXG4gICAgICAgICAgICAgICAgdGl0bGU9e190KCdVcGxvYWQgZmlsZScpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX3VwbG9hZElucHV0fVxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17dXBsb2FkSW5wdXRTdHlsZX1cclxuICAgICAgICAgICAgICAgICAgICBtdWx0aXBsZVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uVXBsb2FkRmlsZUlucHV0Q2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1lc3NhZ2VDb21wb3NlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLm9uSW5wdXRTdGF0ZUNoYW5nZWQgPSB0aGlzLm9uSW5wdXRTdGF0ZUNoYW5nZWQuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vblJvb21TdGF0ZUV2ZW50cyA9IHRoaXMuX29uUm9vbVN0YXRlRXZlbnRzLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25Sb29tVmlld1N0b3JlVXBkYXRlID0gdGhpcy5fb25Sb29tVmlld1N0b3JlVXBkYXRlLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25Ub21ic3RvbmVDbGljayA9IHRoaXMuX29uVG9tYnN0b25lQ2xpY2suYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLnJlbmRlclBsYWNlaG9sZGVyVGV4dCA9IHRoaXMucmVuZGVyUGxhY2Vob2xkZXJUZXh0LmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIGlzUXVvdGluZzogQm9vbGVhbihSb29tVmlld1N0b3JlLmdldFF1b3RpbmdFdmVudCgpKSxcclxuICAgICAgICAgICAgdG9tYnN0b25lOiB0aGlzLl9nZXRSb29tVG9tYnN0b25lKCksXHJcbiAgICAgICAgICAgIGNhblNlbmRNZXNzYWdlczogdGhpcy5wcm9wcy5yb29tLm1heVNlbmRNZXNzYWdlKCksXHJcbiAgICAgICAgICAgIHNob3dDYWxsQnV0dG9uczogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dDYWxsQnV0dG9uc0luQ29tcG9zZXJcIiksXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMuX29uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICB0aGlzLl9yb29tU3RvcmVUb2tlbiA9IFJvb21WaWV3U3RvcmUuYWRkTGlzdGVuZXIodGhpcy5fb25Sb29tVmlld1N0b3JlVXBkYXRlKTtcclxuICAgICAgICB0aGlzLl93YWl0Rm9yT3duTWVtYmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX3dhaXRGb3JPd25NZW1iZXIoKSB7XHJcbiAgICAgICAgLy8gaWYgd2UgaGF2ZSB0aGUgbWVtYmVyIGFscmVhZHksIGRvIHRoYXRcclxuICAgICAgICBjb25zdCBtZSA9IHRoaXMucHJvcHMucm9vbS5nZXRNZW1iZXIoTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFVzZXJJZCgpKTtcclxuICAgICAgICBpZiAobWUpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7bWV9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBPdGhlcndpc2UsIHdhaXQgZm9yIG1lbWJlciBsb2FkaW5nIHRvIGZpbmlzaCBhbmQgdGhlbiB1cGRhdGUgdGhlIG1lbWJlciBmb3IgdGhlIGF2YXRhci5cclxuICAgICAgICAvLyBUaGUgbWVtYmVycyBzaG91bGQgYWxyZWFkeSBiZSBsb2FkaW5nLCBhbmQgbG9hZE1lbWJlcnNJZk5lZWRlZFxyXG4gICAgICAgIC8vIHdpbGwgcmV0dXJuIHRoZSBwcm9taXNlIGZvciB0aGUgZXhpc3Rpbmcgb3BlcmF0aW9uXHJcbiAgICAgICAgdGhpcy5wcm9wcy5yb29tLmxvYWRNZW1iZXJzSWZOZWVkZWQoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbWUgPSB0aGlzLnByb3BzLnJvb20uZ2V0TWVtYmVyKE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21lfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKSkge1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMuX29uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3Jvb21TdG9yZVRva2VuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvb21TdG9yZVRva2VuLnJlbW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25Sb29tU3RhdGVFdmVudHMoZXYsIHN0YXRlKSB7XHJcbiAgICAgICAgaWYgKGV2LmdldFJvb21JZCgpICE9PSB0aGlzLnByb3BzLnJvb20ucm9vbUlkKSByZXR1cm47XHJcblxyXG4gICAgICAgIGlmIChldi5nZXRUeXBlKCkgPT09ICdtLnJvb20udG9tYnN0b25lJykge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHt0b21ic3RvbmU6IHRoaXMuX2dldFJvb21Ub21ic3RvbmUoKX0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXYuZ2V0VHlwZSgpID09PSAnbS5yb29tLnBvd2VyX2xldmVscycpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y2FuU2VuZE1lc3NhZ2VzOiB0aGlzLnByb3BzLnJvb20ubWF5U2VuZE1lc3NhZ2UoKX0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfZ2V0Um9vbVRvbWJzdG9uZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5yb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLnRvbWJzdG9uZScsICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25Sb29tVmlld1N0b3JlVXBkYXRlKCkge1xyXG4gICAgICAgIGNvbnN0IGlzUXVvdGluZyA9IEJvb2xlYW4oUm9vbVZpZXdTdG9yZS5nZXRRdW90aW5nRXZlbnQoKSk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuaXNRdW90aW5nID09PSBpc1F1b3RpbmcpIHJldHVybjtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgaXNRdW90aW5nIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uSW5wdXRTdGF0ZUNoYW5nZWQoaW5wdXRTdGF0ZSkge1xyXG4gICAgICAgIC8vIE1lcmdlIHRoZSBuZXcgaW5wdXQgc3RhdGUgd2l0aCBvbGQgdG8gc3VwcG9ydCBwYXJ0aWFsIHVwZGF0ZXNcclxuICAgICAgICBpbnB1dFN0YXRlID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5zdGF0ZS5pbnB1dFN0YXRlLCBpbnB1dFN0YXRlKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtpbnB1dFN0YXRlfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uVG9tYnN0b25lQ2xpY2soZXYpIHtcclxuICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBjb25zdCByZXBsYWNlbWVudFJvb21JZCA9IHRoaXMuc3RhdGUudG9tYnN0b25lLmdldENvbnRlbnQoKVsncmVwbGFjZW1lbnRfcm9vbSddO1xyXG4gICAgICAgIGNvbnN0IHJlcGxhY2VtZW50Um9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHJlcGxhY2VtZW50Um9vbUlkKTtcclxuICAgICAgICBsZXQgY3JlYXRlRXZlbnRJZCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHJlcGxhY2VtZW50Um9vbSkge1xyXG4gICAgICAgICAgICBjb25zdCBjcmVhdGVFdmVudCA9IHJlcGxhY2VtZW50Um9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5jcmVhdGUnLCAnJyk7XHJcbiAgICAgICAgICAgIGlmIChjcmVhdGVFdmVudCAmJiBjcmVhdGVFdmVudC5nZXRJZCgpKSBjcmVhdGVFdmVudElkID0gY3JlYXRlRXZlbnQuZ2V0SWQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHZpYVNlcnZlcnMgPSBbdGhpcy5zdGF0ZS50b21ic3RvbmUuZ2V0U2VuZGVyKCkuc3BsaXQoJzonKS5zcGxpY2UoMSkuam9pbignOicpXTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICBoaWdobGlnaHRlZDogdHJ1ZSxcclxuICAgICAgICAgICAgZXZlbnRfaWQ6IGNyZWF0ZUV2ZW50SWQsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHJlcGxhY2VtZW50Um9vbUlkLFxyXG4gICAgICAgICAgICBhdXRvX2pvaW46IHRydWUsXHJcblxyXG4gICAgICAgICAgICAvLyBUcnkgdG8gam9pbiB2aWEgdGhlIHNlcnZlciB0aGF0IHNlbnQgdGhlIGV2ZW50LiBUaGlzIGNvbnZlcnRzIEBzb21ldGhpbmc6ZXhhbXBsZS5vcmdcclxuICAgICAgICAgICAgLy8gaW50byBhIHNlcnZlciBkb21haW4gYnkgc3BsaXR0aW5nIG9uIGNvbG9ucyBhbmQgaWdub3JpbmcgdGhlIGZpcnN0IGVudHJ5IChcIkBzb21ldGhpbmdcIikuXHJcbiAgICAgICAgICAgIHZpYV9zZXJ2ZXJzOiB2aWFTZXJ2ZXJzLFxyXG4gICAgICAgICAgICBvcHRzOiB7XHJcbiAgICAgICAgICAgICAgICAvLyBUaGVzZSBhcmUgcGFzc2VkIGRvd24gdG8gdGhlIGpzLXNkaydzIC9qb2luIGNhbGxcclxuICAgICAgICAgICAgICAgIHZpYVNlcnZlcnM6IHZpYVNlcnZlcnMsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyUGxhY2Vob2xkZXJUZXh0KCkge1xyXG4gICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaXNRdW90aW5nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5lMmVTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1NlbmQgYW4gZW5jcnlwdGVkIHJlcGx54oCmJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU2VuZCBhIHJlcGx54oCmJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5lMmVTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1NlbmQgYW4gZW5jcnlwdGVkIG1lc3NhZ2XigKYnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90KCdTZW5kIGEgbWVzc2FnZeKApicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaXNRdW90aW5nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5lMmVTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1NlbmQgYW4gZW5jcnlwdGVkIHJlcGx54oCmJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU2VuZCBhIHJlcGx5ICh1bmVuY3J5cHRlZCnigKYnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmUyZVN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU2VuZCBhbiBlbmNyeXB0ZWQgbWVzc2FnZeKApicpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1NlbmQgYSBtZXNzYWdlICh1bmVuY3J5cHRlZCnigKYnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgY29udHJvbHMgPSBbXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUubWUgPyA8Q29tcG9zZXJBdmF0YXIga2V5PVwiY29udHJvbHNfYXZhdGFyXCIgbWU9e3RoaXMuc3RhdGUubWV9IC8+IDogbnVsbCxcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5lMmVTdGF0dXMgP1xyXG4gICAgICAgICAgICAgICAgPEUyRUljb24ga2V5PVwiZTJlSWNvblwiIHN0YXR1cz17dGhpcy5wcm9wcy5lMmVTdGF0dXN9IGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9lMmVJY29uXCIgLz4gOlxyXG4gICAgICAgICAgICAgICAgbnVsbCxcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUudG9tYnN0b25lICYmIHRoaXMuc3RhdGUuY2FuU2VuZE1lc3NhZ2VzKSB7XHJcbiAgICAgICAgICAgIC8vIFRoaXMgYWxzbyBjdXJyZW50bHkgaW5jbHVkZXMgdGhlIGNhbGwgYnV0dG9ucy4gUmVhbGx5IHdlIHNob3VsZFxyXG4gICAgICAgICAgICAvLyBjaGVjayBzZXBhcmF0ZWx5IGZvciB3aGV0aGVyIHdlIGNhbiBjYWxsLCBidXQgdGhpcyBpcyBzbGlnaHRseVxyXG4gICAgICAgICAgICAvLyBjb21wbGV4IGJlY2F1c2Ugb2YgY29uZmVyZW5jZSBjYWxscy5cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IFNlbmRNZXNzYWdlQ29tcG9zZXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwicm9vbXMuU2VuZE1lc3NhZ2VDb21wb3NlclwiKTtcclxuICAgICAgICAgICAgY29uc3QgY2FsbEluUHJvZ3Jlc3MgPSB0aGlzLnByb3BzLmNhbGxTdGF0ZSAmJiB0aGlzLnByb3BzLmNhbGxTdGF0ZSAhPT0gJ2VuZGVkJztcclxuXHJcbiAgICAgICAgICAgIGNvbnRyb2xzLnB1c2goXHJcbiAgICAgICAgICAgICAgICA8U2VuZE1lc3NhZ2VDb21wb3NlclxyXG4gICAgICAgICAgICAgICAgICAgIHJlZj17KGMpID0+IHRoaXMubWVzc2FnZUNvbXBvc2VySW5wdXQgPSBjfVxyXG4gICAgICAgICAgICAgICAgICAgIGtleT1cImNvbnRyb2xzX2lucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICByb29tPXt0aGlzLnByb3BzLnJvb219XHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3RoaXMucmVuZGVyUGxhY2Vob2xkZXJUZXh0KCl9XHJcbiAgICAgICAgICAgICAgICAgICAgcGVybWFsaW5rQ3JlYXRvcj17dGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yfSAvPixcclxuICAgICAgICAgICAgICAgIDxTdGlja2VycGlja2VyIGtleT0nc3RpY2tlcnBpY2tlcl9jb250cm9sc19idXR0b24nIHJvb209e3RoaXMucHJvcHMucm9vbX0gLz4sXHJcbiAgICAgICAgICAgICAgICA8VXBsb2FkQnV0dG9uIGtleT1cImNvbnRyb2xzX3VwbG9hZFwiIHJvb21JZD17dGhpcy5wcm9wcy5yb29tLnJvb21JZH0gLz4sXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5zaG93Q2FsbEJ1dHRvbnMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChjYWxsSW5Qcm9ncmVzcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xzLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxIYW5ndXBCdXR0b24ga2V5PVwiY29udHJvbHNfaGFuZ3VwXCIgcm9vbUlkPXt0aGlzLnByb3BzLnJvb20ucm9vbUlkfSAvPixcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb250cm9scy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Q2FsbEJ1dHRvbiBrZXk9XCJjb250cm9sc19jYWxsXCIgcm9vbUlkPXt0aGlzLnByb3BzLnJvb20ucm9vbUlkfSAvPixcclxuICAgICAgICAgICAgICAgICAgICAgICAgPFZpZGVvQ2FsbEJ1dHRvbiBrZXk9XCJjb250cm9sc192aWRlb2NhbGxcIiByb29tSWQ9e3RoaXMucHJvcHMucm9vbS5yb29tSWR9IC8+LFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUudG9tYnN0b25lKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcGxhY2VtZW50Um9vbUlkID0gdGhpcy5zdGF0ZS50b21ic3RvbmUuZ2V0Q29udGVudCgpWydyZXBsYWNlbWVudF9yb29tJ107XHJcblxyXG4gICAgICAgICAgICBjb25zdCBjb250aW51ZXNMaW5rID0gcmVwbGFjZW1lbnRSb29tSWQgPyAoXHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPXttYWtlUm9vbVBlcm1hbGluayhyZXBsYWNlbWVudFJvb21JZCl9XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX3Jvb21SZXBsYWNlZF9saW5rXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblRvbWJzdG9uZUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIlRoZSBjb252ZXJzYXRpb24gY29udGludWVzIGhlcmUuXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICApIDogJyc7XHJcblxyXG4gICAgICAgICAgICBjb250cm9scy5wdXNoKDxkaXYgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX3JlcGxhY2VkX3dyYXBwZXJcIiBrZXk9XCJyb29tX3JlcGxhY2VkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9yZXBsYWNlZF92YWxpZ25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9yb29tUmVwbGFjZWRfaWNvblwiIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvcm9vbV9yZXBsYWNlZC5zdmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX3Jvb21SZXBsYWNlZF9oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge190KFwiVGhpcyByb29tIGhhcyBiZWVuIHJlcGxhY2VkIGFuZCBpcyBubyBsb25nZXIgYWN0aXZlLlwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBjb250aW51ZXNMaW5rIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj4pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnRyb2xzLnB1c2goXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGtleT1cImNvbnRyb2xzX2Vycm9yXCIgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX25vcGVybV9lcnJvclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoJ1lvdSBkbyBub3QgaGF2ZSBwZXJtaXNzaW9uIHRvIHBvc3QgdG8gdGhpcyByb29tJykgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29tcG9zZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX3dyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9yb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBjb250cm9scyB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG5cclxuTWVzc2FnZUNvbXBvc2VyLnByb3BUeXBlcyA9IHtcclxuICAgIC8vIGpzLXNkayBSb29tIG9iamVjdFxyXG4gICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG5cclxuICAgIC8vIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIGN1cnJlbnQgdm9pcCBjYWxsIHN0YXRlXHJcbiAgICBjYWxsU3RhdGU6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgLy8gc3RyaW5nIHJlcHJlc2VudGluZyB0aGUgY3VycmVudCByb29tIGFwcCBkcmF3ZXIgc3RhdGVcclxuICAgIHNob3dBcHBzOiBQcm9wVHlwZXMuYm9vbCxcclxufTtcclxuIl19