"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../languageHandler");

var _AppTile = _interopRequireDefault(require("../elements/AppTile"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _WidgetUtils = _interopRequireDefault(require("../../../utils/WidgetUtils"));

var _ActiveWidgetStore = _interopRequireDefault(require("../../../stores/ActiveWidgetStore"));

var _PersistedElement = _interopRequireDefault(require("../elements/PersistedElement"));

var _IntegrationManagers = require("../../../integrations/IntegrationManagers");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _ContextMenu = require("../../structures/ContextMenu");

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const widgetType = 'm.stickerpicker'; // This should be below the dialog level (4000), but above the rest of the UI (1000-2000).
// We sit in a context menu, so this should be given to the context menu.

const STICKERPICKER_Z_INDEX = 3500; // Key to store the widget's AppTile under in PersistedElement

const PERSISTED_ELEMENT_KEY = "stickerPicker";

class Stickerpicker extends _react.default.Component {
  constructor(props) {
    super(props);
    this._onShowStickersClick = this._onShowStickersClick.bind(this);
    this._onHideStickersClick = this._onHideStickersClick.bind(this);
    this._launchManageIntegrations = this._launchManageIntegrations.bind(this);
    this._removeStickerpickerWidgets = this._removeStickerpickerWidgets.bind(this);
    this._updateWidget = this._updateWidget.bind(this);
    this._onWidgetAction = this._onWidgetAction.bind(this);
    this._onResize = this._onResize.bind(this);
    this._onFinished = this._onFinished.bind(this);
    this.popoverWidth = 300;
    this.popoverHeight = 300; // This is loaded by _acquireScalarClient on an as-needed basis.

    this.scalarClient = null;
    this.state = {
      showStickers: false,
      imError: null,
      stickerpickerX: null,
      stickerpickerY: null,
      stickerpickerWidget: null,
      widgetId: null
    };
  }

  _acquireScalarClient() {
    if (this.scalarClient) return Promise.resolve(this.scalarClient); // TODO: Pick the right manager for the widget

    if (_IntegrationManagers.IntegrationManagers.sharedInstance().hasManager()) {
      this.scalarClient = _IntegrationManagers.IntegrationManagers.sharedInstance().getPrimaryManager().getScalarClient();
      return this.scalarClient.connect().then(() => {
        this.forceUpdate();
        return this.scalarClient;
      }).catch(e => {
        this._imError((0, _languageHandler._td)("Failed to connect to integration manager"), e);
      });
    } else {
      _IntegrationManagers.IntegrationManagers.sharedInstance().openNoManagerDialog();
    }
  }

  async _removeStickerpickerWidgets() {
    const scalarClient = await this._acquireScalarClient();
    console.log('Removing Stickerpicker widgets');

    if (this.state.widgetId) {
      if (scalarClient) {
        scalarClient.disableWidgetAssets(widgetType, this.state.widgetId).then(() => {
          console.log('Assets disabled');
        }).catch(err => {
          console.error('Failed to disable assets');
        });
      } else {
        console.error("Cannot disable assets: no scalar client");
      }
    } else {
      console.warn('No widget ID specified, not disabling assets');
    }

    this.setState({
      showStickers: false
    });

    _WidgetUtils.default.removeStickerpickerWidgets().then(() => {
      this.forceUpdate();
    }).catch(e => {
      console.error('Failed to remove sticker picker widget', e);
    });
  }

  componentDidMount() {
    // Close the sticker picker when the window resizes
    window.addEventListener('resize', this._onResize);
    this.dispatcherRef = _dispatcher.default.register(this._onWidgetAction); // Track updates to widget state in account data

    _MatrixClientPeg.MatrixClientPeg.get().on('accountData', this._updateWidget); // Initialise widget state from current account data


    this._updateWidget();
  }

  componentWillUnmount() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client) client.removeListener('accountData', this._updateWidget);
    window.removeEventListener('resize', this._onResize);

    if (this.dispatcherRef) {
      _dispatcher.default.unregister(this.dispatcherRef);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    this._sendVisibilityToWidget(this.state.showStickers);
  }

  _imError(errorMsg, e) {
    console.error(errorMsg, e);
    this.setState({
      showStickers: false,
      imError: (0, _languageHandler._t)(errorMsg)
    });
  }

  _updateWidget() {
    const stickerpickerWidget = _WidgetUtils.default.getStickerpickerWidgets()[0];

    if (!stickerpickerWidget) {
      Stickerpicker.currentWidget = null;
      this.setState({
        stickerpickerWidget: null,
        widgetId: null
      });
      return;
    }

    const currentWidget = Stickerpicker.currentWidget;
    let currentUrl = null;

    if (currentWidget && currentWidget.content && currentWidget.content.url) {
      currentUrl = currentWidget.content.url;
    }

    let newUrl = null;

    if (stickerpickerWidget && stickerpickerWidget.content && stickerpickerWidget.content.url) {
      newUrl = stickerpickerWidget.content.url;
    }

    if (newUrl !== currentUrl) {
      // Destroy the existing frame so a new one can be created
      _PersistedElement.default.destroyElement(PERSISTED_ELEMENT_KEY);
    }

    Stickerpicker.currentWidget = stickerpickerWidget;
    this.setState({
      stickerpickerWidget,
      widgetId: stickerpickerWidget ? stickerpickerWidget.id : null
    });
  }

  _onWidgetAction(payload) {
    switch (payload.action) {
      case "user_widget_updated":
        this.forceUpdate();
        break;

      case "stickerpicker_close":
        this.setState({
          showStickers: false
        });
        break;

      case "after_right_panel_phase_change":
      case "show_left_panel":
      case "hide_left_panel":
        this.setState({
          showStickers: false
        });
        break;
    }
  }

  _defaultStickerpickerContent() {
    return _react.default.createElement(_AccessibleButton.default, {
      onClick: this._launchManageIntegrations,
      className: "mx_Stickers_contentPlaceholder"
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("You don't currently have any stickerpacks enabled")), _react.default.createElement("p", {
      className: "mx_Stickers_addLink"
    }, (0, _languageHandler._t)("Add some now")), _react.default.createElement("img", {
      src: require("../../../../res/img/stickerpack-placeholder.png"),
      alt: ""
    }));
  }

  _errorStickerpickerContent() {
    return _react.default.createElement("div", {
      style: {
        "text-align": "center"
      },
      className: "error"
    }, _react.default.createElement("p", null, " ", this.state.imError, " "));
  }

  _sendVisibilityToWidget(visible) {
    if (!this.state.stickerpickerWidget) return;

    const widgetMessaging = _ActiveWidgetStore.default.getWidgetMessaging(this.state.stickerpickerWidget.id);

    if (widgetMessaging && visible !== this._prevSentVisibility) {
      widgetMessaging.sendVisibility(visible);
      this._prevSentVisibility = visible;
    }
  }

  _getStickerpickerContent() {
    // Handle Integration Manager errors
    if (this.state._imError) {
      return this._errorStickerpickerContent();
    } // Stickers
    // TODO - Add support for Stickerpickers from multiple app stores.
    // Render content from multiple stickerpack sources, each within their
    // own iframe, within the stickerpicker UI element.


    const stickerpickerWidget = this.state.stickerpickerWidget;
    let stickersContent; // Use a separate ReactDOM tree to render the AppTile separately so that it persists and does
    // not unmount when we (a) close the sticker picker (b) switch rooms. It's properties are still
    // updated.

    const PersistedElement = sdk.getComponent("elements.PersistedElement"); // Load stickerpack content

    if (stickerpickerWidget && stickerpickerWidget.content && stickerpickerWidget.content.url) {
      // Set default name
      stickerpickerWidget.content.name = stickerpickerWidget.name || (0, _languageHandler._t)("Stickerpack"); // FIXME: could this use the same code as other apps?

      const stickerApp = {
        id: stickerpickerWidget.id,
        url: stickerpickerWidget.content.url,
        name: stickerpickerWidget.content.name,
        type: stickerpickerWidget.content.type
      };
      stickersContent = _react.default.createElement("div", {
        className: "mx_Stickers_content_container"
      }, _react.default.createElement("div", {
        id: "stickersContent",
        className: "mx_Stickers_content",
        style: {
          border: 'none',
          height: this.popoverHeight,
          width: this.popoverWidth
        }
      }, _react.default.createElement(PersistedElement, {
        persistKey: PERSISTED_ELEMENT_KEY,
        style: {
          zIndex: STICKERPICKER_Z_INDEX
        }
      }, _react.default.createElement(_AppTile.default, {
        app: stickerApp,
        room: this.props.room,
        fullWidth: true,
        userId: _MatrixClientPeg.MatrixClientPeg.get().credentials.userId,
        creatorUserId: stickerpickerWidget.sender || _MatrixClientPeg.MatrixClientPeg.get().credentials.userId,
        waitForIframeLoad: true,
        show: true,
        showMenubar: true,
        onEditClick: this._launchManageIntegrations,
        onDeleteClick: this._removeStickerpickerWidgets,
        showTitle: false,
        showMinimise: true,
        showDelete: false,
        showCancel: false,
        showPopout: false,
        onMinimiseClick: this._onHideStickersClick,
        handleMinimisePointerEvents: true,
        whitelistCapabilities: ['m.sticker', 'visibility'],
        userWidget: true
      }))));
    } else {
      // Default content to show if stickerpicker widget not added
      stickersContent = this._defaultStickerpickerContent();
    }

    return stickersContent;
  } // Dev note: this isn't jsdoc because it's angry.

  /*
   * Show the sticker picker overlay
   * If no stickerpacks have been added, show a link to the integration manager add sticker packs page.
   */


  _onShowStickersClick(e) {
    if (!_SettingsStore.default.getValue("integrationProvisioning")) {
      // Intercept this case and spawn a warning.
      return _IntegrationManagers.IntegrationManagers.sharedInstance().showDisabledDialog();
    } // XXX: Simplify by using a context menu that is positioned relative to the sticker picker button


    const buttonRect = e.target.getBoundingClientRect(); // The window X and Y offsets are to adjust position when zoomed in to page

    let x = buttonRect.right + window.pageXOffset - 41; // Amount of horizontal space between the right of menu and the right of the viewport
    //  (10 = amount needed to make chevron centrally aligned)

    const rightPad = 10; // When the sticker picker would be displayed off of the viewport, adjust x
    //  (302 = width of context menu, including borders)

    x = Math.min(x, document.body.clientWidth - (302 + rightPad)); // Offset the chevron location, which is relative to the left of the context menu
    //  (10 = offset when context menu would not be displayed off viewport)
    //  (2 = context menu borders)

    const stickerPickerChevronOffset = Math.max(10, 2 + window.pageXOffset + buttonRect.left - x);
    const y = buttonRect.top + buttonRect.height / 2 + window.pageYOffset - 19;
    this.setState({
      showStickers: true,
      stickerPickerX: x,
      stickerPickerY: y,
      stickerPickerChevronOffset
    });
  }
  /**
   * Trigger hiding of the sticker picker overlay
   * @param  {Event} ev Event that triggered the function call
   */


  _onHideStickersClick(ev) {
    this.setState({
      showStickers: false
    });
  }
  /**
   * Called when the window is resized
   */


  _onResize() {
    this.setState({
      showStickers: false
    });
  }
  /**
   * The stickers picker was hidden
   */


  _onFinished() {
    this.setState({
      showStickers: false
    });
  }
  /**
   * Launch the integration manager on the stickers integration page
   */


  _launchManageIntegrations() {
    // TODO: Open the right integration manager for the widget
    if (_SettingsStore.default.isFeatureEnabled("feature_many_integration_managers")) {
      _IntegrationManagers.IntegrationManagers.sharedInstance().openAll(this.props.room, "type_".concat(widgetType), this.state.widgetId);
    } else {
      _IntegrationManagers.IntegrationManagers.sharedInstance().getPrimaryManager().open(this.props.room, "type_".concat(widgetType), this.state.widgetId);
    }
  }

  render() {
    let stickerPicker;
    let stickersButton;

    if (this.state.showStickers) {
      // Show hide-stickers button
      stickersButton = _react.default.createElement(_AccessibleButton.default, {
        id: "stickersButton",
        key: "controls_hide_stickers",
        className: "mx_MessageComposer_button mx_MessageComposer_stickers mx_Stickers_hideStickers",
        onClick: this._onHideStickersClick,
        title: (0, _languageHandler._t)("Hide Stickers")
      });
      const GenericElementContextMenu = sdk.getComponent('context_menus.GenericElementContextMenu');
      stickerPicker = _react.default.createElement(_ContextMenu.ContextMenu, {
        chevronOffset: this.state.stickerPickerChevronOffset,
        chevronFace: "bottom",
        left: this.state.stickerPickerX,
        top: this.state.stickerPickerY,
        menuWidth: this.popoverWidth,
        menuHeight: this.popoverHeight,
        onFinished: this._onFinished,
        menuPaddingTop: 0,
        menuPaddingLeft: 0,
        menuPaddingRight: 0,
        zIndex: STICKERPICKER_Z_INDEX
      }, _react.default.createElement(GenericElementContextMenu, {
        element: this._getStickerpickerContent(),
        onResize: this._onFinished
      }));
    } else {
      // Show show-stickers button
      stickersButton = _react.default.createElement(_AccessibleButton.default, {
        id: "stickersButton",
        key: "controls_show_stickers",
        className: "mx_MessageComposer_button mx_MessageComposer_stickers",
        onClick: this._onShowStickersClick,
        title: (0, _languageHandler._t)("Show Stickers")
      });
    }

    return _react.default.createElement(_react.default.Fragment, null, stickersButton, stickerPicker);
  }

}

exports.default = Stickerpicker;
(0, _defineProperty2.default)(Stickerpicker, "currentWidget", void 0);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1N0aWNrZXJwaWNrZXIuanMiXSwibmFtZXMiOlsid2lkZ2V0VHlwZSIsIlNUSUNLRVJQSUNLRVJfWl9JTkRFWCIsIlBFUlNJU1RFRF9FTEVNRU5UX0tFWSIsIlN0aWNrZXJwaWNrZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJfb25TaG93U3RpY2tlcnNDbGljayIsImJpbmQiLCJfb25IaWRlU3RpY2tlcnNDbGljayIsIl9sYXVuY2hNYW5hZ2VJbnRlZ3JhdGlvbnMiLCJfcmVtb3ZlU3RpY2tlcnBpY2tlcldpZGdldHMiLCJfdXBkYXRlV2lkZ2V0IiwiX29uV2lkZ2V0QWN0aW9uIiwiX29uUmVzaXplIiwiX29uRmluaXNoZWQiLCJwb3BvdmVyV2lkdGgiLCJwb3BvdmVySGVpZ2h0Iiwic2NhbGFyQ2xpZW50Iiwic3RhdGUiLCJzaG93U3RpY2tlcnMiLCJpbUVycm9yIiwic3RpY2tlcnBpY2tlclgiLCJzdGlja2VycGlja2VyWSIsInN0aWNrZXJwaWNrZXJXaWRnZXQiLCJ3aWRnZXRJZCIsIl9hY3F1aXJlU2NhbGFyQ2xpZW50IiwiUHJvbWlzZSIsInJlc29sdmUiLCJJbnRlZ3JhdGlvbk1hbmFnZXJzIiwic2hhcmVkSW5zdGFuY2UiLCJoYXNNYW5hZ2VyIiwiZ2V0UHJpbWFyeU1hbmFnZXIiLCJnZXRTY2FsYXJDbGllbnQiLCJjb25uZWN0IiwidGhlbiIsImZvcmNlVXBkYXRlIiwiY2F0Y2giLCJlIiwiX2ltRXJyb3IiLCJvcGVuTm9NYW5hZ2VyRGlhbG9nIiwiY29uc29sZSIsImxvZyIsImRpc2FibGVXaWRnZXRBc3NldHMiLCJlcnIiLCJlcnJvciIsIndhcm4iLCJzZXRTdGF0ZSIsIldpZGdldFV0aWxzIiwicmVtb3ZlU3RpY2tlcnBpY2tlcldpZGdldHMiLCJjb21wb25lbnREaWRNb3VudCIsIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJkaXNwYXRjaGVyUmVmIiwiZGlzIiwicmVnaXN0ZXIiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJvbiIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwiY2xpZW50IiwicmVtb3ZlTGlzdGVuZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwidW5yZWdpc3RlciIsImNvbXBvbmVudERpZFVwZGF0ZSIsInByZXZQcm9wcyIsInByZXZTdGF0ZSIsIl9zZW5kVmlzaWJpbGl0eVRvV2lkZ2V0IiwiZXJyb3JNc2ciLCJnZXRTdGlja2VycGlja2VyV2lkZ2V0cyIsImN1cnJlbnRXaWRnZXQiLCJjdXJyZW50VXJsIiwiY29udGVudCIsInVybCIsIm5ld1VybCIsIlBlcnNpc3RlZEVsZW1lbnQiLCJkZXN0cm95RWxlbWVudCIsImlkIiwicGF5bG9hZCIsImFjdGlvbiIsIl9kZWZhdWx0U3RpY2tlcnBpY2tlckNvbnRlbnQiLCJyZXF1aXJlIiwiX2Vycm9yU3RpY2tlcnBpY2tlckNvbnRlbnQiLCJ2aXNpYmxlIiwid2lkZ2V0TWVzc2FnaW5nIiwiQWN0aXZlV2lkZ2V0U3RvcmUiLCJnZXRXaWRnZXRNZXNzYWdpbmciLCJfcHJldlNlbnRWaXNpYmlsaXR5Iiwic2VuZFZpc2liaWxpdHkiLCJfZ2V0U3RpY2tlcnBpY2tlckNvbnRlbnQiLCJzdGlja2Vyc0NvbnRlbnQiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJuYW1lIiwic3RpY2tlckFwcCIsInR5cGUiLCJib3JkZXIiLCJoZWlnaHQiLCJ3aWR0aCIsInpJbmRleCIsInJvb20iLCJjcmVkZW50aWFscyIsInVzZXJJZCIsInNlbmRlciIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsInNob3dEaXNhYmxlZERpYWxvZyIsImJ1dHRvblJlY3QiLCJ0YXJnZXQiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJ4IiwicmlnaHQiLCJwYWdlWE9mZnNldCIsInJpZ2h0UGFkIiwiTWF0aCIsIm1pbiIsImRvY3VtZW50IiwiYm9keSIsImNsaWVudFdpZHRoIiwic3RpY2tlclBpY2tlckNoZXZyb25PZmZzZXQiLCJtYXgiLCJsZWZ0IiwieSIsInRvcCIsInBhZ2VZT2Zmc2V0Iiwic3RpY2tlclBpY2tlclgiLCJzdGlja2VyUGlja2VyWSIsImV2IiwiaXNGZWF0dXJlRW5hYmxlZCIsIm9wZW5BbGwiLCJvcGVuIiwicmVuZGVyIiwic3RpY2tlclBpY2tlciIsInN0aWNrZXJzQnV0dG9uIiwiR2VuZXJpY0VsZW1lbnRDb250ZXh0TWVudSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTNCQTs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBLE1BQU1BLFVBQVUsR0FBRyxpQkFBbkIsQyxDQUVBO0FBQ0E7O0FBQ0EsTUFBTUMscUJBQXFCLEdBQUcsSUFBOUIsQyxDQUVBOztBQUNBLE1BQU1DLHFCQUFxQixHQUFHLGVBQTlCOztBQUVlLE1BQU1DLGFBQU4sU0FBNEJDLGVBQU1DLFNBQWxDLENBQTRDO0FBR3ZEQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFDQSxTQUFLQyxvQkFBTCxHQUE0QixLQUFLQSxvQkFBTCxDQUEwQkMsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFDQSxTQUFLQyxvQkFBTCxHQUE0QixLQUFLQSxvQkFBTCxDQUEwQkQsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFDQSxTQUFLRSx5QkFBTCxHQUFpQyxLQUFLQSx5QkFBTCxDQUErQkYsSUFBL0IsQ0FBb0MsSUFBcEMsQ0FBakM7QUFDQSxTQUFLRywyQkFBTCxHQUFtQyxLQUFLQSwyQkFBTCxDQUFpQ0gsSUFBakMsQ0FBc0MsSUFBdEMsQ0FBbkM7QUFDQSxTQUFLSSxhQUFMLEdBQXFCLEtBQUtBLGFBQUwsQ0FBbUJKLElBQW5CLENBQXdCLElBQXhCLENBQXJCO0FBQ0EsU0FBS0ssZUFBTCxHQUF1QixLQUFLQSxlQUFMLENBQXFCTCxJQUFyQixDQUEwQixJQUExQixDQUF2QjtBQUNBLFNBQUtNLFNBQUwsR0FBaUIsS0FBS0EsU0FBTCxDQUFlTixJQUFmLENBQW9CLElBQXBCLENBQWpCO0FBQ0EsU0FBS08sV0FBTCxHQUFtQixLQUFLQSxXQUFMLENBQWlCUCxJQUFqQixDQUFzQixJQUF0QixDQUFuQjtBQUVBLFNBQUtRLFlBQUwsR0FBb0IsR0FBcEI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCLEdBQXJCLENBWmUsQ0FjZjs7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLElBQXBCO0FBRUEsU0FBS0MsS0FBTCxHQUFhO0FBQ1RDLE1BQUFBLFlBQVksRUFBRSxLQURMO0FBRVRDLE1BQUFBLE9BQU8sRUFBRSxJQUZBO0FBR1RDLE1BQUFBLGNBQWMsRUFBRSxJQUhQO0FBSVRDLE1BQUFBLGNBQWMsRUFBRSxJQUpQO0FBS1RDLE1BQUFBLG1CQUFtQixFQUFFLElBTFo7QUFNVEMsTUFBQUEsUUFBUSxFQUFFO0FBTkQsS0FBYjtBQVFIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixRQUFJLEtBQUtSLFlBQVQsRUFBdUIsT0FBT1MsT0FBTyxDQUFDQyxPQUFSLENBQWdCLEtBQUtWLFlBQXJCLENBQVAsQ0FESixDQUVuQjs7QUFDQSxRQUFJVyx5Q0FBb0JDLGNBQXBCLEdBQXFDQyxVQUFyQyxFQUFKLEVBQXVEO0FBQ25ELFdBQUtiLFlBQUwsR0FBb0JXLHlDQUFvQkMsY0FBcEIsR0FBcUNFLGlCQUFyQyxHQUF5REMsZUFBekQsRUFBcEI7QUFDQSxhQUFPLEtBQUtmLFlBQUwsQ0FBa0JnQixPQUFsQixHQUE0QkMsSUFBNUIsQ0FBaUMsTUFBTTtBQUMxQyxhQUFLQyxXQUFMO0FBQ0EsZUFBTyxLQUFLbEIsWUFBWjtBQUNILE9BSE0sRUFHSm1CLEtBSEksQ0FHR0MsQ0FBRCxJQUFPO0FBQ1osYUFBS0MsUUFBTCxDQUFjLDBCQUFJLDBDQUFKLENBQWQsRUFBK0RELENBQS9EO0FBQ0gsT0FMTSxDQUFQO0FBTUgsS0FSRCxNQVFPO0FBQ0hULCtDQUFvQkMsY0FBcEIsR0FBcUNVLG1CQUFyQztBQUNIO0FBQ0o7O0FBRUQsUUFBTTdCLDJCQUFOLEdBQW9DO0FBQ2hDLFVBQU1PLFlBQVksR0FBRyxNQUFNLEtBQUtRLG9CQUFMLEVBQTNCO0FBQ0FlLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdDQUFaOztBQUNBLFFBQUksS0FBS3ZCLEtBQUwsQ0FBV00sUUFBZixFQUF5QjtBQUNyQixVQUFJUCxZQUFKLEVBQWtCO0FBQ2RBLFFBQUFBLFlBQVksQ0FBQ3lCLG1CQUFiLENBQWlDNUMsVUFBakMsRUFBNkMsS0FBS29CLEtBQUwsQ0FBV00sUUFBeEQsRUFBa0VVLElBQWxFLENBQXVFLE1BQU07QUFDekVNLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGlCQUFaO0FBQ0gsU0FGRCxFQUVHTCxLQUZILENBRVVPLEdBQUQsSUFBUztBQUNkSCxVQUFBQSxPQUFPLENBQUNJLEtBQVIsQ0FBYywwQkFBZDtBQUNILFNBSkQ7QUFLSCxPQU5ELE1BTU87QUFDSEosUUFBQUEsT0FBTyxDQUFDSSxLQUFSLENBQWMseUNBQWQ7QUFDSDtBQUNKLEtBVkQsTUFVTztBQUNISixNQUFBQSxPQUFPLENBQUNLLElBQVIsQ0FBYSw4Q0FBYjtBQUNIOztBQUVELFNBQUtDLFFBQUwsQ0FBYztBQUFDM0IsTUFBQUEsWUFBWSxFQUFFO0FBQWYsS0FBZDs7QUFDQTRCLHlCQUFZQywwQkFBWixHQUF5Q2QsSUFBekMsQ0FBOEMsTUFBTTtBQUNoRCxXQUFLQyxXQUFMO0FBQ0gsS0FGRCxFQUVHQyxLQUZILENBRVVDLENBQUQsSUFBTztBQUNaRyxNQUFBQSxPQUFPLENBQUNJLEtBQVIsQ0FBYyx3Q0FBZCxFQUF3RFAsQ0FBeEQ7QUFDSCxLQUpEO0FBS0g7O0FBRURZLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCO0FBQ0FDLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsS0FBS3RDLFNBQXZDO0FBRUEsU0FBS3VDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBSzFDLGVBQWxCLENBQXJCLENBSmdCLENBTWhCOztBQUNBMkMscUNBQWdCQyxHQUFoQixHQUFzQkMsRUFBdEIsQ0FBeUIsYUFBekIsRUFBd0MsS0FBSzlDLGFBQTdDLEVBUGdCLENBU2hCOzs7QUFDQSxTQUFLQSxhQUFMO0FBQ0g7O0FBRUQrQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixVQUFNQyxNQUFNLEdBQUdKLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxRQUFJRyxNQUFKLEVBQVlBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixhQUF0QixFQUFxQyxLQUFLakQsYUFBMUM7QUFFWnVDLElBQUFBLE1BQU0sQ0FBQ1csbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUMsS0FBS2hELFNBQTFDOztBQUNBLFFBQUksS0FBS3VDLGFBQVQsRUFBd0I7QUFDcEJDLDBCQUFJUyxVQUFKLENBQWUsS0FBS1YsYUFBcEI7QUFDSDtBQUNKOztBQUVEVyxFQUFBQSxrQkFBa0IsQ0FBQ0MsU0FBRCxFQUFZQyxTQUFaLEVBQXVCO0FBQ3JDLFNBQUtDLHVCQUFMLENBQTZCLEtBQUtoRCxLQUFMLENBQVdDLFlBQXhDO0FBQ0g7O0FBRURtQixFQUFBQSxRQUFRLENBQUM2QixRQUFELEVBQVc5QixDQUFYLEVBQWM7QUFDbEJHLElBQUFBLE9BQU8sQ0FBQ0ksS0FBUixDQUFjdUIsUUFBZCxFQUF3QjlCLENBQXhCO0FBQ0EsU0FBS1MsUUFBTCxDQUFjO0FBQ1YzQixNQUFBQSxZQUFZLEVBQUUsS0FESjtBQUVWQyxNQUFBQSxPQUFPLEVBQUUseUJBQUcrQyxRQUFIO0FBRkMsS0FBZDtBQUlIOztBQUVEeEQsRUFBQUEsYUFBYSxHQUFHO0FBQ1osVUFBTVksbUJBQW1CLEdBQUd3QixxQkFBWXFCLHVCQUFaLEdBQXNDLENBQXRDLENBQTVCOztBQUNBLFFBQUksQ0FBQzdDLG1CQUFMLEVBQTBCO0FBQ3RCdEIsTUFBQUEsYUFBYSxDQUFDb0UsYUFBZCxHQUE4QixJQUE5QjtBQUNBLFdBQUt2QixRQUFMLENBQWM7QUFBQ3ZCLFFBQUFBLG1CQUFtQixFQUFFLElBQXRCO0FBQTRCQyxRQUFBQSxRQUFRLEVBQUU7QUFBdEMsT0FBZDtBQUNBO0FBQ0g7O0FBRUQsVUFBTTZDLGFBQWEsR0FBR3BFLGFBQWEsQ0FBQ29FLGFBQXBDO0FBQ0EsUUFBSUMsVUFBVSxHQUFHLElBQWpCOztBQUNBLFFBQUlELGFBQWEsSUFBSUEsYUFBYSxDQUFDRSxPQUEvQixJQUEwQ0YsYUFBYSxDQUFDRSxPQUFkLENBQXNCQyxHQUFwRSxFQUF5RTtBQUNyRUYsTUFBQUEsVUFBVSxHQUFHRCxhQUFhLENBQUNFLE9BQWQsQ0FBc0JDLEdBQW5DO0FBQ0g7O0FBRUQsUUFBSUMsTUFBTSxHQUFHLElBQWI7O0FBQ0EsUUFBSWxELG1CQUFtQixJQUFJQSxtQkFBbUIsQ0FBQ2dELE9BQTNDLElBQXNEaEQsbUJBQW1CLENBQUNnRCxPQUFwQixDQUE0QkMsR0FBdEYsRUFBMkY7QUFDdkZDLE1BQUFBLE1BQU0sR0FBR2xELG1CQUFtQixDQUFDZ0QsT0FBcEIsQ0FBNEJDLEdBQXJDO0FBQ0g7O0FBRUQsUUFBSUMsTUFBTSxLQUFLSCxVQUFmLEVBQTJCO0FBQ3ZCO0FBQ0FJLGdDQUFpQkMsY0FBakIsQ0FBZ0MzRSxxQkFBaEM7QUFDSDs7QUFFREMsSUFBQUEsYUFBYSxDQUFDb0UsYUFBZCxHQUE4QjlDLG1CQUE5QjtBQUNBLFNBQUt1QixRQUFMLENBQWM7QUFDVnZCLE1BQUFBLG1CQURVO0FBRVZDLE1BQUFBLFFBQVEsRUFBRUQsbUJBQW1CLEdBQUdBLG1CQUFtQixDQUFDcUQsRUFBdkIsR0FBNEI7QUFGL0MsS0FBZDtBQUlIOztBQUVEaEUsRUFBQUEsZUFBZSxDQUFDaUUsT0FBRCxFQUFVO0FBQ3JCLFlBQVFBLE9BQU8sQ0FBQ0MsTUFBaEI7QUFDSSxXQUFLLHFCQUFMO0FBQ0ksYUFBSzNDLFdBQUw7QUFDQTs7QUFDSixXQUFLLHFCQUFMO0FBQ0ksYUFBS1csUUFBTCxDQUFjO0FBQUMzQixVQUFBQSxZQUFZLEVBQUU7QUFBZixTQUFkO0FBQ0E7O0FBQ0osV0FBSyxnQ0FBTDtBQUNBLFdBQUssaUJBQUw7QUFDQSxXQUFLLGlCQUFMO0FBQ0ksYUFBSzJCLFFBQUwsQ0FBYztBQUFDM0IsVUFBQUEsWUFBWSxFQUFFO0FBQWYsU0FBZDtBQUNBO0FBWFI7QUFhSDs7QUFFRDRELEVBQUFBLDRCQUE0QixHQUFHO0FBQzNCLFdBQ0ksNkJBQUMseUJBQUQ7QUFBa0IsTUFBQSxPQUFPLEVBQUUsS0FBS3RFLHlCQUFoQztBQUNJLE1BQUEsU0FBUyxFQUFDO0FBRGQsT0FFSSx3Q0FBSyx5QkFBRyxtREFBSCxDQUFMLENBRkosRUFHSTtBQUFHLE1BQUEsU0FBUyxFQUFDO0FBQWIsT0FBcUMseUJBQUcsY0FBSCxDQUFyQyxDQUhKLEVBSUk7QUFBSyxNQUFBLEdBQUcsRUFBRXVFLE9BQU8sQ0FBQyxpREFBRCxDQUFqQjtBQUFzRSxNQUFBLEdBQUcsRUFBQztBQUExRSxNQUpKLENBREo7QUFRSDs7QUFFREMsRUFBQUEsMEJBQTBCLEdBQUc7QUFDekIsV0FDSTtBQUFLLE1BQUEsS0FBSyxFQUFFO0FBQUMsc0JBQWM7QUFBZixPQUFaO0FBQXNDLE1BQUEsU0FBUyxFQUFDO0FBQWhELE9BQ0ksNkNBQU0sS0FBSy9ELEtBQUwsQ0FBV0UsT0FBakIsTUFESixDQURKO0FBS0g7O0FBRUQ4QyxFQUFBQSx1QkFBdUIsQ0FBQ2dCLE9BQUQsRUFBVTtBQUM3QixRQUFJLENBQUMsS0FBS2hFLEtBQUwsQ0FBV0ssbUJBQWhCLEVBQXFDOztBQUNyQyxVQUFNNEQsZUFBZSxHQUFHQywyQkFBa0JDLGtCQUFsQixDQUFxQyxLQUFLbkUsS0FBTCxDQUFXSyxtQkFBWCxDQUErQnFELEVBQXBFLENBQXhCOztBQUNBLFFBQUlPLGVBQWUsSUFBSUQsT0FBTyxLQUFLLEtBQUtJLG1CQUF4QyxFQUE2RDtBQUN6REgsTUFBQUEsZUFBZSxDQUFDSSxjQUFoQixDQUErQkwsT0FBL0I7QUFDQSxXQUFLSSxtQkFBTCxHQUEyQkosT0FBM0I7QUFDSDtBQUNKOztBQUVETSxFQUFBQSx3QkFBd0IsR0FBRztBQUN2QjtBQUNBLFFBQUksS0FBS3RFLEtBQUwsQ0FBV29CLFFBQWYsRUFBeUI7QUFDckIsYUFBTyxLQUFLMkMsMEJBQUwsRUFBUDtBQUNILEtBSnNCLENBTXZCO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxVQUFNMUQsbUJBQW1CLEdBQUcsS0FBS0wsS0FBTCxDQUFXSyxtQkFBdkM7QUFDQSxRQUFJa0UsZUFBSixDQVh1QixDQWF2QjtBQUNBO0FBQ0E7O0FBQ0EsVUFBTWYsZ0JBQWdCLEdBQUdnQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCLENBaEJ1QixDQWtCdkI7O0FBQ0EsUUFBSXBFLG1CQUFtQixJQUFJQSxtQkFBbUIsQ0FBQ2dELE9BQTNDLElBQXNEaEQsbUJBQW1CLENBQUNnRCxPQUFwQixDQUE0QkMsR0FBdEYsRUFBMkY7QUFDdkY7QUFDQWpELE1BQUFBLG1CQUFtQixDQUFDZ0QsT0FBcEIsQ0FBNEJxQixJQUE1QixHQUFtQ3JFLG1CQUFtQixDQUFDcUUsSUFBcEIsSUFBNEIseUJBQUcsYUFBSCxDQUEvRCxDQUZ1RixDQUl2Rjs7QUFDQSxZQUFNQyxVQUFVLEdBQUc7QUFDZmpCLFFBQUFBLEVBQUUsRUFBRXJELG1CQUFtQixDQUFDcUQsRUFEVDtBQUVmSixRQUFBQSxHQUFHLEVBQUVqRCxtQkFBbUIsQ0FBQ2dELE9BQXBCLENBQTRCQyxHQUZsQjtBQUdmb0IsUUFBQUEsSUFBSSxFQUFFckUsbUJBQW1CLENBQUNnRCxPQUFwQixDQUE0QnFCLElBSG5CO0FBSWZFLFFBQUFBLElBQUksRUFBRXZFLG1CQUFtQixDQUFDZ0QsT0FBcEIsQ0FBNEJ1QjtBQUpuQixPQUFuQjtBQU9BTCxNQUFBQSxlQUFlLEdBQ1g7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0k7QUFDSSxRQUFBLEVBQUUsRUFBQyxpQkFEUDtBQUVJLFFBQUEsU0FBUyxFQUFDLHFCQUZkO0FBR0ksUUFBQSxLQUFLLEVBQUU7QUFDSE0sVUFBQUEsTUFBTSxFQUFFLE1BREw7QUFFSEMsVUFBQUEsTUFBTSxFQUFFLEtBQUtoRixhQUZWO0FBR0hpRixVQUFBQSxLQUFLLEVBQUUsS0FBS2xGO0FBSFQ7QUFIWCxTQVNBLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsVUFBVSxFQUFFZixxQkFBOUI7QUFBcUQsUUFBQSxLQUFLLEVBQUU7QUFBQ2tHLFVBQUFBLE1BQU0sRUFBRW5HO0FBQVQ7QUFBNUQsU0FDSSw2QkFBQyxnQkFBRDtBQUNJLFFBQUEsR0FBRyxFQUFFOEYsVUFEVDtBQUVJLFFBQUEsSUFBSSxFQUFFLEtBQUt4RixLQUFMLENBQVc4RixJQUZyQjtBQUdJLFFBQUEsU0FBUyxFQUFFLElBSGY7QUFJSSxRQUFBLE1BQU0sRUFBRTVDLGlDQUFnQkMsR0FBaEIsR0FBc0I0QyxXQUF0QixDQUFrQ0MsTUFKOUM7QUFLSSxRQUFBLGFBQWEsRUFBRTlFLG1CQUFtQixDQUFDK0UsTUFBcEIsSUFBOEIvQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCNEMsV0FBdEIsQ0FBa0NDLE1BTG5GO0FBTUksUUFBQSxpQkFBaUIsRUFBRSxJQU52QjtBQU9JLFFBQUEsSUFBSSxFQUFFLElBUFY7QUFRSSxRQUFBLFdBQVcsRUFBRSxJQVJqQjtBQVNJLFFBQUEsV0FBVyxFQUFFLEtBQUs1Rix5QkFUdEI7QUFVSSxRQUFBLGFBQWEsRUFBRSxLQUFLQywyQkFWeEI7QUFXSSxRQUFBLFNBQVMsRUFBRSxLQVhmO0FBWUksUUFBQSxZQUFZLEVBQUUsSUFabEI7QUFhSSxRQUFBLFVBQVUsRUFBRSxLQWJoQjtBQWNJLFFBQUEsVUFBVSxFQUFFLEtBZGhCO0FBZUksUUFBQSxVQUFVLEVBQUUsS0FmaEI7QUFnQkksUUFBQSxlQUFlLEVBQUUsS0FBS0Ysb0JBaEIxQjtBQWlCSSxRQUFBLDJCQUEyQixFQUFFLElBakJqQztBQWtCSSxRQUFBLHFCQUFxQixFQUFFLENBQUMsV0FBRCxFQUFjLFlBQWQsQ0FsQjNCO0FBbUJJLFFBQUEsVUFBVSxFQUFFO0FBbkJoQixRQURKLENBVEEsQ0FESixDQURKO0FBcUNILEtBakRELE1BaURPO0FBQ0g7QUFDQWlGLE1BQUFBLGVBQWUsR0FBRyxLQUFLViw0QkFBTCxFQUFsQjtBQUNIOztBQUNELFdBQU9VLGVBQVA7QUFDSCxHQTlQc0QsQ0FnUXZEOztBQUNBOzs7Ozs7QUFJQW5GLEVBQUFBLG9CQUFvQixDQUFDK0IsQ0FBRCxFQUFJO0FBQ3BCLFFBQUksQ0FBQ2tFLHVCQUFjQyxRQUFkLENBQXVCLHlCQUF2QixDQUFMLEVBQXdEO0FBQ3BEO0FBQ0EsYUFBTzVFLHlDQUFvQkMsY0FBcEIsR0FBcUM0RSxrQkFBckMsRUFBUDtBQUNILEtBSm1CLENBTXBCOzs7QUFFQSxVQUFNQyxVQUFVLEdBQUdyRSxDQUFDLENBQUNzRSxNQUFGLENBQVNDLHFCQUFULEVBQW5CLENBUm9CLENBVXBCOztBQUNBLFFBQUlDLENBQUMsR0FBR0gsVUFBVSxDQUFDSSxLQUFYLEdBQW1CNUQsTUFBTSxDQUFDNkQsV0FBMUIsR0FBd0MsRUFBaEQsQ0FYb0IsQ0FhcEI7QUFDQTs7QUFDQSxVQUFNQyxRQUFRLEdBQUcsRUFBakIsQ0Fmb0IsQ0FpQnBCO0FBQ0E7O0FBQ0FILElBQUFBLENBQUMsR0FBR0ksSUFBSSxDQUFDQyxHQUFMLENBQVNMLENBQVQsRUFBWU0sUUFBUSxDQUFDQyxJQUFULENBQWNDLFdBQWQsSUFBNkIsTUFBTUwsUUFBbkMsQ0FBWixDQUFKLENBbkJvQixDQXFCcEI7QUFDQTtBQUNBOztBQUNBLFVBQU1NLDBCQUEwQixHQUFHTCxJQUFJLENBQUNNLEdBQUwsQ0FBUyxFQUFULEVBQWEsSUFBSXJFLE1BQU0sQ0FBQzZELFdBQVgsR0FBeUJMLFVBQVUsQ0FBQ2MsSUFBcEMsR0FBMkNYLENBQXhELENBQW5DO0FBRUEsVUFBTVksQ0FBQyxHQUFJZixVQUFVLENBQUNnQixHQUFYLEdBQWtCaEIsVUFBVSxDQUFDVixNQUFYLEdBQW9CLENBQXRDLEdBQTJDOUMsTUFBTSxDQUFDeUUsV0FBbkQsR0FBa0UsRUFBNUU7QUFFQSxTQUFLN0UsUUFBTCxDQUFjO0FBQ1YzQixNQUFBQSxZQUFZLEVBQUUsSUFESjtBQUVWeUcsTUFBQUEsY0FBYyxFQUFFZixDQUZOO0FBR1ZnQixNQUFBQSxjQUFjLEVBQUVKLENBSE47QUFJVkgsTUFBQUE7QUFKVSxLQUFkO0FBTUg7QUFFRDs7Ozs7O0FBSUE5RyxFQUFBQSxvQkFBb0IsQ0FBQ3NILEVBQUQsRUFBSztBQUNyQixTQUFLaEYsUUFBTCxDQUFjO0FBQUMzQixNQUFBQSxZQUFZLEVBQUU7QUFBZixLQUFkO0FBQ0g7QUFFRDs7Ozs7QUFHQU4sRUFBQUEsU0FBUyxHQUFHO0FBQ1IsU0FBS2lDLFFBQUwsQ0FBYztBQUFDM0IsTUFBQUEsWUFBWSxFQUFFO0FBQWYsS0FBZDtBQUNIO0FBRUQ7Ozs7O0FBR0FMLEVBQUFBLFdBQVcsR0FBRztBQUNWLFNBQUtnQyxRQUFMLENBQWM7QUFBQzNCLE1BQUFBLFlBQVksRUFBRTtBQUFmLEtBQWQ7QUFDSDtBQUVEOzs7OztBQUdBVixFQUFBQSx5QkFBeUIsR0FBRztBQUN4QjtBQUNBLFFBQUk4Rix1QkFBY3dCLGdCQUFkLENBQStCLG1DQUEvQixDQUFKLEVBQXlFO0FBQ3JFbkcsK0NBQW9CQyxjQUFwQixHQUFxQ21HLE9BQXJDLENBQ0ksS0FBSzNILEtBQUwsQ0FBVzhGLElBRGYsaUJBRVlyRyxVQUZaLEdBR0ksS0FBS29CLEtBQUwsQ0FBV00sUUFIZjtBQUtILEtBTkQsTUFNTztBQUNISSwrQ0FBb0JDLGNBQXBCLEdBQXFDRSxpQkFBckMsR0FBeURrRyxJQUF6RCxDQUNJLEtBQUs1SCxLQUFMLENBQVc4RixJQURmLGlCQUVZckcsVUFGWixHQUdJLEtBQUtvQixLQUFMLENBQVdNLFFBSGY7QUFLSDtBQUNKOztBQUVEMEcsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsUUFBSUMsYUFBSjtBQUNBLFFBQUlDLGNBQUo7O0FBQ0EsUUFBSSxLQUFLbEgsS0FBTCxDQUFXQyxZQUFmLEVBQTZCO0FBQ3pCO0FBQ0FpSCxNQUFBQSxjQUFjLEdBQ1YsNkJBQUMseUJBQUQ7QUFDSSxRQUFBLEVBQUUsRUFBQyxnQkFEUDtBQUVJLFFBQUEsR0FBRyxFQUFDLHdCQUZSO0FBR0ksUUFBQSxTQUFTLEVBQUMsZ0ZBSGQ7QUFJSSxRQUFBLE9BQU8sRUFBRSxLQUFLNUgsb0JBSmxCO0FBS0ksUUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSDtBQUxYLFFBREo7QUFVQSxZQUFNNkgseUJBQXlCLEdBQUczQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIseUNBQWpCLENBQWxDO0FBQ0F3QyxNQUFBQSxhQUFhLEdBQUcsNkJBQUMsd0JBQUQ7QUFDWixRQUFBLGFBQWEsRUFBRSxLQUFLakgsS0FBTCxDQUFXb0csMEJBRGQ7QUFFWixRQUFBLFdBQVcsRUFBQyxRQUZBO0FBR1osUUFBQSxJQUFJLEVBQUUsS0FBS3BHLEtBQUwsQ0FBVzBHLGNBSEw7QUFJWixRQUFBLEdBQUcsRUFBRSxLQUFLMUcsS0FBTCxDQUFXMkcsY0FKSjtBQUtaLFFBQUEsU0FBUyxFQUFFLEtBQUs5RyxZQUxKO0FBTVosUUFBQSxVQUFVLEVBQUUsS0FBS0MsYUFOTDtBQU9aLFFBQUEsVUFBVSxFQUFFLEtBQUtGLFdBUEw7QUFRWixRQUFBLGNBQWMsRUFBRSxDQVJKO0FBU1osUUFBQSxlQUFlLEVBQUUsQ0FUTDtBQVVaLFFBQUEsZ0JBQWdCLEVBQUUsQ0FWTjtBQVdaLFFBQUEsTUFBTSxFQUFFZjtBQVhJLFNBYVosNkJBQUMseUJBQUQ7QUFBMkIsUUFBQSxPQUFPLEVBQUUsS0FBS3lGLHdCQUFMLEVBQXBDO0FBQXFFLFFBQUEsUUFBUSxFQUFFLEtBQUsxRTtBQUFwRixRQWJZLENBQWhCO0FBZUgsS0E1QkQsTUE0Qk87QUFDSDtBQUNBc0gsTUFBQUEsY0FBYyxHQUNWLDZCQUFDLHlCQUFEO0FBQ0ksUUFBQSxFQUFFLEVBQUMsZ0JBRFA7QUFFSSxRQUFBLEdBQUcsRUFBQyx3QkFGUjtBQUdJLFFBQUEsU0FBUyxFQUFDLHVEQUhkO0FBSUksUUFBQSxPQUFPLEVBQUUsS0FBSzlILG9CQUpsQjtBQUtJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUg7QUFMWCxRQURKO0FBU0g7O0FBQ0QsV0FBTyw2QkFBQyxjQUFELENBQU8sUUFBUCxRQUNEOEgsY0FEQyxFQUVERCxhQUZDLENBQVA7QUFJSDs7QUFsWXNEOzs7OEJBQXRDbEksYSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge190LCBfdGR9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBBcHBUaWxlIGZyb20gJy4uL2VsZW1lbnRzL0FwcFRpbGUnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSAnLi4vZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvbic7XHJcbmltcG9ydCBXaWRnZXRVdGlscyBmcm9tICcuLi8uLi8uLi91dGlscy9XaWRnZXRVdGlscyc7XHJcbmltcG9ydCBBY3RpdmVXaWRnZXRTdG9yZSBmcm9tICcuLi8uLi8uLi9zdG9yZXMvQWN0aXZlV2lkZ2V0U3RvcmUnO1xyXG5pbXBvcnQgUGVyc2lzdGVkRWxlbWVudCBmcm9tIFwiLi4vZWxlbWVudHMvUGVyc2lzdGVkRWxlbWVudFwiO1xyXG5pbXBvcnQge0ludGVncmF0aW9uTWFuYWdlcnN9IGZyb20gXCIuLi8uLi8uLi9pbnRlZ3JhdGlvbnMvSW50ZWdyYXRpb25NYW5hZ2Vyc1wiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQge0NvbnRleHRNZW51fSBmcm9tIFwiLi4vLi4vc3RydWN0dXJlcy9Db250ZXh0TWVudVwiO1xyXG5cclxuY29uc3Qgd2lkZ2V0VHlwZSA9ICdtLnN0aWNrZXJwaWNrZXInO1xyXG5cclxuLy8gVGhpcyBzaG91bGQgYmUgYmVsb3cgdGhlIGRpYWxvZyBsZXZlbCAoNDAwMCksIGJ1dCBhYm92ZSB0aGUgcmVzdCBvZiB0aGUgVUkgKDEwMDAtMjAwMCkuXHJcbi8vIFdlIHNpdCBpbiBhIGNvbnRleHQgbWVudSwgc28gdGhpcyBzaG91bGQgYmUgZ2l2ZW4gdG8gdGhlIGNvbnRleHQgbWVudS5cclxuY29uc3QgU1RJQ0tFUlBJQ0tFUl9aX0lOREVYID0gMzUwMDtcclxuXHJcbi8vIEtleSB0byBzdG9yZSB0aGUgd2lkZ2V0J3MgQXBwVGlsZSB1bmRlciBpbiBQZXJzaXN0ZWRFbGVtZW50XHJcbmNvbnN0IFBFUlNJU1RFRF9FTEVNRU5UX0tFWSA9IFwic3RpY2tlclBpY2tlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3RpY2tlcnBpY2tlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgY3VycmVudFdpZGdldDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLl9vblNob3dTdGlja2Vyc0NsaWNrID0gdGhpcy5fb25TaG93U3RpY2tlcnNDbGljay5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uSGlkZVN0aWNrZXJzQ2xpY2sgPSB0aGlzLl9vbkhpZGVTdGlja2Vyc0NsaWNrLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fbGF1bmNoTWFuYWdlSW50ZWdyYXRpb25zID0gdGhpcy5fbGF1bmNoTWFuYWdlSW50ZWdyYXRpb25zLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fcmVtb3ZlU3RpY2tlcnBpY2tlcldpZGdldHMgPSB0aGlzLl9yZW1vdmVTdGlja2VycGlja2VyV2lkZ2V0cy5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVdpZGdldCA9IHRoaXMuX3VwZGF0ZVdpZGdldC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX29uV2lkZ2V0QWN0aW9uID0gdGhpcy5fb25XaWRnZXRBY3Rpb24uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9vblJlc2l6ZSA9IHRoaXMuX29uUmVzaXplLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fb25GaW5pc2hlZCA9IHRoaXMuX29uRmluaXNoZWQuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5wb3BvdmVyV2lkdGggPSAzMDA7XHJcbiAgICAgICAgdGhpcy5wb3BvdmVySGVpZ2h0ID0gMzAwO1xyXG5cclxuICAgICAgICAvLyBUaGlzIGlzIGxvYWRlZCBieSBfYWNxdWlyZVNjYWxhckNsaWVudCBvbiBhbiBhcy1uZWVkZWQgYmFzaXMuXHJcbiAgICAgICAgdGhpcy5zY2FsYXJDbGllbnQgPSBudWxsO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBzaG93U3RpY2tlcnM6IGZhbHNlLFxyXG4gICAgICAgICAgICBpbUVycm9yOiBudWxsLFxyXG4gICAgICAgICAgICBzdGlja2VycGlja2VyWDogbnVsbCxcclxuICAgICAgICAgICAgc3RpY2tlcnBpY2tlclk6IG51bGwsXHJcbiAgICAgICAgICAgIHN0aWNrZXJwaWNrZXJXaWRnZXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHdpZGdldElkOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgX2FjcXVpcmVTY2FsYXJDbGllbnQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2NhbGFyQ2xpZW50KSByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRoaXMuc2NhbGFyQ2xpZW50KTtcclxuICAgICAgICAvLyBUT0RPOiBQaWNrIHRoZSByaWdodCBtYW5hZ2VyIGZvciB0aGUgd2lkZ2V0XHJcbiAgICAgICAgaWYgKEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5oYXNNYW5hZ2VyKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zY2FsYXJDbGllbnQgPSBJbnRlZ3JhdGlvbk1hbmFnZXJzLnNoYXJlZEluc3RhbmNlKCkuZ2V0UHJpbWFyeU1hbmFnZXIoKS5nZXRTY2FsYXJDbGllbnQoKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2NhbGFyQ2xpZW50LmNvbm5lY3QoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNjYWxhckNsaWVudDtcclxuICAgICAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2ltRXJyb3IoX3RkKFwiRmFpbGVkIHRvIGNvbm5lY3QgdG8gaW50ZWdyYXRpb24gbWFuYWdlclwiKSwgZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5vcGVuTm9NYW5hZ2VyRGlhbG9nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9yZW1vdmVTdGlja2VycGlja2VyV2lkZ2V0cygpIHtcclxuICAgICAgICBjb25zdCBzY2FsYXJDbGllbnQgPSBhd2FpdCB0aGlzLl9hY3F1aXJlU2NhbGFyQ2xpZW50KCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ1JlbW92aW5nIFN0aWNrZXJwaWNrZXIgd2lkZ2V0cycpO1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLndpZGdldElkKSB7XHJcbiAgICAgICAgICAgIGlmIChzY2FsYXJDbGllbnQpIHtcclxuICAgICAgICAgICAgICAgIHNjYWxhckNsaWVudC5kaXNhYmxlV2lkZ2V0QXNzZXRzKHdpZGdldFR5cGUsIHRoaXMuc3RhdGUud2lkZ2V0SWQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdBc3NldHMgZGlzYWJsZWQnKTtcclxuICAgICAgICAgICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdGYWlsZWQgdG8gZGlzYWJsZSBhc3NldHMnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkNhbm5vdCBkaXNhYmxlIGFzc2V0czogbm8gc2NhbGFyIGNsaWVudFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gd2lkZ2V0IElEIHNwZWNpZmllZCwgbm90IGRpc2FibGluZyBhc3NldHMnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dTdGlja2VyczogZmFsc2V9KTtcclxuICAgICAgICBXaWRnZXRVdGlscy5yZW1vdmVTdGlja2VycGlja2VyV2lkZ2V0cygpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRmFpbGVkIHRvIHJlbW92ZSBzdGlja2VyIHBpY2tlciB3aWRnZXQnLCBlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICAvLyBDbG9zZSB0aGUgc3RpY2tlciBwaWNrZXIgd2hlbiB0aGUgd2luZG93IHJlc2l6ZXNcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5fb25SZXNpemUpO1xyXG5cclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5fb25XaWRnZXRBY3Rpb24pO1xyXG5cclxuICAgICAgICAvLyBUcmFjayB1cGRhdGVzIHRvIHdpZGdldCBzdGF0ZSBpbiBhY2NvdW50IGRhdGFcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oJ2FjY291bnREYXRhJywgdGhpcy5fdXBkYXRlV2lkZ2V0KTtcclxuXHJcbiAgICAgICAgLy8gSW5pdGlhbGlzZSB3aWRnZXQgc3RhdGUgZnJvbSBjdXJyZW50IGFjY291bnQgZGF0YVxyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVdpZGdldCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoY2xpZW50KSBjbGllbnQucmVtb3ZlTGlzdGVuZXIoJ2FjY291bnREYXRhJywgdGhpcy5fdXBkYXRlV2lkZ2V0KTtcclxuXHJcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMuX29uUmVzaXplKTtcclxuICAgICAgICBpZiAodGhpcy5kaXNwYXRjaGVyUmVmKSB7XHJcbiAgICAgICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xyXG4gICAgICAgIHRoaXMuX3NlbmRWaXNpYmlsaXR5VG9XaWRnZXQodGhpcy5zdGF0ZS5zaG93U3RpY2tlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIF9pbUVycm9yKGVycm9yTXNnLCBlKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvck1zZywgZSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHNob3dTdGlja2VyczogZmFsc2UsXHJcbiAgICAgICAgICAgIGltRXJyb3I6IF90KGVycm9yTXNnKSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfdXBkYXRlV2lkZ2V0KCkge1xyXG4gICAgICAgIGNvbnN0IHN0aWNrZXJwaWNrZXJXaWRnZXQgPSBXaWRnZXRVdGlscy5nZXRTdGlja2VycGlja2VyV2lkZ2V0cygpWzBdO1xyXG4gICAgICAgIGlmICghc3RpY2tlcnBpY2tlcldpZGdldCkge1xyXG4gICAgICAgICAgICBTdGlja2VycGlja2VyLmN1cnJlbnRXaWRnZXQgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzdGlja2VycGlja2VyV2lkZ2V0OiBudWxsLCB3aWRnZXRJZDogbnVsbH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjdXJyZW50V2lkZ2V0ID0gU3RpY2tlcnBpY2tlci5jdXJyZW50V2lkZ2V0O1xyXG4gICAgICAgIGxldCBjdXJyZW50VXJsID0gbnVsbDtcclxuICAgICAgICBpZiAoY3VycmVudFdpZGdldCAmJiBjdXJyZW50V2lkZ2V0LmNvbnRlbnQgJiYgY3VycmVudFdpZGdldC5jb250ZW50LnVybCkge1xyXG4gICAgICAgICAgICBjdXJyZW50VXJsID0gY3VycmVudFdpZGdldC5jb250ZW50LnVybDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBuZXdVcmwgPSBudWxsO1xyXG4gICAgICAgIGlmIChzdGlja2VycGlja2VyV2lkZ2V0ICYmIHN0aWNrZXJwaWNrZXJXaWRnZXQuY29udGVudCAmJiBzdGlja2VycGlja2VyV2lkZ2V0LmNvbnRlbnQudXJsKSB7XHJcbiAgICAgICAgICAgIG5ld1VybCA9IHN0aWNrZXJwaWNrZXJXaWRnZXQuY29udGVudC51cmw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAobmV3VXJsICE9PSBjdXJyZW50VXJsKSB7XHJcbiAgICAgICAgICAgIC8vIERlc3Ryb3kgdGhlIGV4aXN0aW5nIGZyYW1lIHNvIGEgbmV3IG9uZSBjYW4gYmUgY3JlYXRlZFxyXG4gICAgICAgICAgICBQZXJzaXN0ZWRFbGVtZW50LmRlc3Ryb3lFbGVtZW50KFBFUlNJU1RFRF9FTEVNRU5UX0tFWSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBTdGlja2VycGlja2VyLmN1cnJlbnRXaWRnZXQgPSBzdGlja2VycGlja2VyV2lkZ2V0O1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBzdGlja2VycGlja2VyV2lkZ2V0LFxyXG4gICAgICAgICAgICB3aWRnZXRJZDogc3RpY2tlcnBpY2tlcldpZGdldCA/IHN0aWNrZXJwaWNrZXJXaWRnZXQuaWQgOiBudWxsLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbldpZGdldEFjdGlvbihwYXlsb2FkKSB7XHJcbiAgICAgICAgc3dpdGNoIChwYXlsb2FkLmFjdGlvbikge1xyXG4gICAgICAgICAgICBjYXNlIFwidXNlcl93aWRnZXRfdXBkYXRlZFwiOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJzdGlja2VycGlja2VyX2Nsb3NlXCI6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzaG93U3RpY2tlcnM6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImFmdGVyX3JpZ2h0X3BhbmVsX3BoYXNlX2NoYW5nZVwiOlxyXG4gICAgICAgICAgICBjYXNlIFwic2hvd19sZWZ0X3BhbmVsXCI6XHJcbiAgICAgICAgICAgIGNhc2UgXCJoaWRlX2xlZnRfcGFuZWxcIjpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dTdGlja2VyczogZmFsc2V9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfZGVmYXVsdFN0aWNrZXJwaWNrZXJDb250ZW50KCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIG9uQ2xpY2s9e3RoaXMuX2xhdW5jaE1hbmFnZUludGVncmF0aW9uc31cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0nbXhfU3RpY2tlcnNfY29udGVudFBsYWNlaG9sZGVyJz5cclxuICAgICAgICAgICAgICAgIDxwPnsgX3QoXCJZb3UgZG9uJ3QgY3VycmVudGx5IGhhdmUgYW55IHN0aWNrZXJwYWNrcyBlbmFibGVkXCIpIH08L3A+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9J214X1N0aWNrZXJzX2FkZExpbmsnPnsgX3QoXCJBZGQgc29tZSBub3dcIikgfTwvcD5cclxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9zdGlja2VycGFjay1wbGFjZWhvbGRlci5wbmdcIil9IGFsdD1cIlwiIC8+XHJcbiAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIF9lcnJvclN0aWNrZXJwaWNrZXJDb250ZW50KCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgc3R5bGU9e3tcInRleHQtYWxpZ25cIjogXCJjZW50ZXJcIn19IGNsYXNzTmFtZT1cImVycm9yXCI+XHJcbiAgICAgICAgICAgICAgICA8cD4geyB0aGlzLnN0YXRlLmltRXJyb3IgfSA8L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgX3NlbmRWaXNpYmlsaXR5VG9XaWRnZXQodmlzaWJsZSkge1xyXG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5zdGlja2VycGlja2VyV2lkZ2V0KSByZXR1cm47XHJcbiAgICAgICAgY29uc3Qgd2lkZ2V0TWVzc2FnaW5nID0gQWN0aXZlV2lkZ2V0U3RvcmUuZ2V0V2lkZ2V0TWVzc2FnaW5nKHRoaXMuc3RhdGUuc3RpY2tlcnBpY2tlcldpZGdldC5pZCk7XHJcbiAgICAgICAgaWYgKHdpZGdldE1lc3NhZ2luZyAmJiB2aXNpYmxlICE9PSB0aGlzLl9wcmV2U2VudFZpc2liaWxpdHkpIHtcclxuICAgICAgICAgICAgd2lkZ2V0TWVzc2FnaW5nLnNlbmRWaXNpYmlsaXR5KHZpc2libGUpO1xyXG4gICAgICAgICAgICB0aGlzLl9wcmV2U2VudFZpc2liaWxpdHkgPSB2aXNpYmxlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfZ2V0U3RpY2tlcnBpY2tlckNvbnRlbnQoKSB7XHJcbiAgICAgICAgLy8gSGFuZGxlIEludGVncmF0aW9uIE1hbmFnZXIgZXJyb3JzXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuX2ltRXJyb3IpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2Vycm9yU3RpY2tlcnBpY2tlckNvbnRlbnQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFN0aWNrZXJzXHJcbiAgICAgICAgLy8gVE9ETyAtIEFkZCBzdXBwb3J0IGZvciBTdGlja2VycGlja2VycyBmcm9tIG11bHRpcGxlIGFwcCBzdG9yZXMuXHJcbiAgICAgICAgLy8gUmVuZGVyIGNvbnRlbnQgZnJvbSBtdWx0aXBsZSBzdGlja2VycGFjayBzb3VyY2VzLCBlYWNoIHdpdGhpbiB0aGVpclxyXG4gICAgICAgIC8vIG93biBpZnJhbWUsIHdpdGhpbiB0aGUgc3RpY2tlcnBpY2tlciBVSSBlbGVtZW50LlxyXG4gICAgICAgIGNvbnN0IHN0aWNrZXJwaWNrZXJXaWRnZXQgPSB0aGlzLnN0YXRlLnN0aWNrZXJwaWNrZXJXaWRnZXQ7XHJcbiAgICAgICAgbGV0IHN0aWNrZXJzQ29udGVudDtcclxuXHJcbiAgICAgICAgLy8gVXNlIGEgc2VwYXJhdGUgUmVhY3RET00gdHJlZSB0byByZW5kZXIgdGhlIEFwcFRpbGUgc2VwYXJhdGVseSBzbyB0aGF0IGl0IHBlcnNpc3RzIGFuZCBkb2VzXHJcbiAgICAgICAgLy8gbm90IHVubW91bnQgd2hlbiB3ZSAoYSkgY2xvc2UgdGhlIHN0aWNrZXIgcGlja2VyIChiKSBzd2l0Y2ggcm9vbXMuIEl0J3MgcHJvcGVydGllcyBhcmUgc3RpbGxcclxuICAgICAgICAvLyB1cGRhdGVkLlxyXG4gICAgICAgIGNvbnN0IFBlcnNpc3RlZEVsZW1lbnQgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZWxlbWVudHMuUGVyc2lzdGVkRWxlbWVudFwiKTtcclxuXHJcbiAgICAgICAgLy8gTG9hZCBzdGlja2VycGFjayBjb250ZW50XHJcbiAgICAgICAgaWYgKHN0aWNrZXJwaWNrZXJXaWRnZXQgJiYgc3RpY2tlcnBpY2tlcldpZGdldC5jb250ZW50ICYmIHN0aWNrZXJwaWNrZXJXaWRnZXQuY29udGVudC51cmwpIHtcclxuICAgICAgICAgICAgLy8gU2V0IGRlZmF1bHQgbmFtZVxyXG4gICAgICAgICAgICBzdGlja2VycGlja2VyV2lkZ2V0LmNvbnRlbnQubmFtZSA9IHN0aWNrZXJwaWNrZXJXaWRnZXQubmFtZSB8fCBfdChcIlN0aWNrZXJwYWNrXCIpO1xyXG5cclxuICAgICAgICAgICAgLy8gRklYTUU6IGNvdWxkIHRoaXMgdXNlIHRoZSBzYW1lIGNvZGUgYXMgb3RoZXIgYXBwcz9cclxuICAgICAgICAgICAgY29uc3Qgc3RpY2tlckFwcCA9IHtcclxuICAgICAgICAgICAgICAgIGlkOiBzdGlja2VycGlja2VyV2lkZ2V0LmlkLFxyXG4gICAgICAgICAgICAgICAgdXJsOiBzdGlja2VycGlja2VyV2lkZ2V0LmNvbnRlbnQudXJsLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogc3RpY2tlcnBpY2tlcldpZGdldC5jb250ZW50Lm5hbWUsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiBzdGlja2VycGlja2VyV2lkZ2V0LmNvbnRlbnQudHlwZSxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHN0aWNrZXJzQ29udGVudCA9IChcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdteF9TdGlja2Vyc19jb250ZW50X2NvbnRhaW5lcic+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD0nc3RpY2tlcnNDb250ZW50J1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9J214X1N0aWNrZXJzX2NvbnRlbnQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6ICdub25lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogdGhpcy5wb3BvdmVySGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHRoaXMucG9wb3ZlcldpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8UGVyc2lzdGVkRWxlbWVudCBwZXJzaXN0S2V5PXtQRVJTSVNURURfRUxFTUVOVF9LRVl9IHN0eWxlPXt7ekluZGV4OiBTVElDS0VSUElDS0VSX1pfSU5ERVh9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFwcFRpbGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcD17c3RpY2tlckFwcH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvb209e3RoaXMucHJvcHMucm9vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bGxXaWR0aD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZD17TWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0b3JVc2VySWQ9e3N0aWNrZXJwaWNrZXJXaWRnZXQuc2VuZGVyIHx8IE1hdHJpeENsaWVudFBlZy5nZXQoKS5jcmVkZW50aWFscy51c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3YWl0Rm9ySWZyYW1lTG9hZD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3c9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93TWVudWJhcj17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRWRpdENsaWNrPXt0aGlzLl9sYXVuY2hNYW5hZ2VJbnRlZ3JhdGlvbnN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkRlbGV0ZUNsaWNrPXt0aGlzLl9yZW1vdmVTdGlja2VycGlja2VyV2lkZ2V0c31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dUaXRsZT17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93TWluaW1pc2U9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93RGVsZXRlPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvd1BvcG91dD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbk1pbmltaXNlQ2xpY2s9e3RoaXMuX29uSGlkZVN0aWNrZXJzQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVNaW5pbWlzZVBvaW50ZXJFdmVudHM9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aGl0ZWxpc3RDYXBhYmlsaXRpZXM9e1snbS5zdGlja2VyJywgJ3Zpc2liaWxpdHknXX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJXaWRnZXQ9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9QZXJzaXN0ZWRFbGVtZW50PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gRGVmYXVsdCBjb250ZW50IHRvIHNob3cgaWYgc3RpY2tlcnBpY2tlciB3aWRnZXQgbm90IGFkZGVkXHJcbiAgICAgICAgICAgIHN0aWNrZXJzQ29udGVudCA9IHRoaXMuX2RlZmF1bHRTdGlja2VycGlja2VyQ29udGVudCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc3RpY2tlcnNDb250ZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIERldiBub3RlOiB0aGlzIGlzbid0IGpzZG9jIGJlY2F1c2UgaXQncyBhbmdyeS5cclxuICAgIC8qXHJcbiAgICAgKiBTaG93IHRoZSBzdGlja2VyIHBpY2tlciBvdmVybGF5XHJcbiAgICAgKiBJZiBubyBzdGlja2VycGFja3MgaGF2ZSBiZWVuIGFkZGVkLCBzaG93IGEgbGluayB0byB0aGUgaW50ZWdyYXRpb24gbWFuYWdlciBhZGQgc3RpY2tlciBwYWNrcyBwYWdlLlxyXG4gICAgICovXHJcbiAgICBfb25TaG93U3RpY2tlcnNDbGljayhlKSB7XHJcbiAgICAgICAgaWYgKCFTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiaW50ZWdyYXRpb25Qcm92aXNpb25pbmdcIikpIHtcclxuICAgICAgICAgICAgLy8gSW50ZXJjZXB0IHRoaXMgY2FzZSBhbmQgc3Bhd24gYSB3YXJuaW5nLlxyXG4gICAgICAgICAgICByZXR1cm4gSW50ZWdyYXRpb25NYW5hZ2Vycy5zaGFyZWRJbnN0YW5jZSgpLnNob3dEaXNhYmxlZERpYWxvZygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gWFhYOiBTaW1wbGlmeSBieSB1c2luZyBhIGNvbnRleHQgbWVudSB0aGF0IGlzIHBvc2l0aW9uZWQgcmVsYXRpdmUgdG8gdGhlIHN0aWNrZXIgcGlja2VyIGJ1dHRvblxyXG5cclxuICAgICAgICBjb25zdCBidXR0b25SZWN0ID0gZS50YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblxyXG4gICAgICAgIC8vIFRoZSB3aW5kb3cgWCBhbmQgWSBvZmZzZXRzIGFyZSB0byBhZGp1c3QgcG9zaXRpb24gd2hlbiB6b29tZWQgaW4gdG8gcGFnZVxyXG4gICAgICAgIGxldCB4ID0gYnV0dG9uUmVjdC5yaWdodCArIHdpbmRvdy5wYWdlWE9mZnNldCAtIDQxO1xyXG5cclxuICAgICAgICAvLyBBbW91bnQgb2YgaG9yaXpvbnRhbCBzcGFjZSBiZXR3ZWVuIHRoZSByaWdodCBvZiBtZW51IGFuZCB0aGUgcmlnaHQgb2YgdGhlIHZpZXdwb3J0XHJcbiAgICAgICAgLy8gICgxMCA9IGFtb3VudCBuZWVkZWQgdG8gbWFrZSBjaGV2cm9uIGNlbnRyYWxseSBhbGlnbmVkKVxyXG4gICAgICAgIGNvbnN0IHJpZ2h0UGFkID0gMTA7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gdGhlIHN0aWNrZXIgcGlja2VyIHdvdWxkIGJlIGRpc3BsYXllZCBvZmYgb2YgdGhlIHZpZXdwb3J0LCBhZGp1c3QgeFxyXG4gICAgICAgIC8vICAoMzAyID0gd2lkdGggb2YgY29udGV4dCBtZW51LCBpbmNsdWRpbmcgYm9yZGVycylcclxuICAgICAgICB4ID0gTWF0aC5taW4oeCwgZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aCAtICgzMDIgKyByaWdodFBhZCkpO1xyXG5cclxuICAgICAgICAvLyBPZmZzZXQgdGhlIGNoZXZyb24gbG9jYXRpb24sIHdoaWNoIGlzIHJlbGF0aXZlIHRvIHRoZSBsZWZ0IG9mIHRoZSBjb250ZXh0IG1lbnVcclxuICAgICAgICAvLyAgKDEwID0gb2Zmc2V0IHdoZW4gY29udGV4dCBtZW51IHdvdWxkIG5vdCBiZSBkaXNwbGF5ZWQgb2ZmIHZpZXdwb3J0KVxyXG4gICAgICAgIC8vICAoMiA9IGNvbnRleHQgbWVudSBib3JkZXJzKVxyXG4gICAgICAgIGNvbnN0IHN0aWNrZXJQaWNrZXJDaGV2cm9uT2Zmc2V0ID0gTWF0aC5tYXgoMTAsIDIgKyB3aW5kb3cucGFnZVhPZmZzZXQgKyBidXR0b25SZWN0LmxlZnQgLSB4KTtcclxuXHJcbiAgICAgICAgY29uc3QgeSA9IChidXR0b25SZWN0LnRvcCArIChidXR0b25SZWN0LmhlaWdodCAvIDIpICsgd2luZG93LnBhZ2VZT2Zmc2V0KSAtIDE5O1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgc2hvd1N0aWNrZXJzOiB0cnVlLFxyXG4gICAgICAgICAgICBzdGlja2VyUGlja2VyWDogeCxcclxuICAgICAgICAgICAgc3RpY2tlclBpY2tlclk6IHksXHJcbiAgICAgICAgICAgIHN0aWNrZXJQaWNrZXJDaGV2cm9uT2Zmc2V0LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHJpZ2dlciBoaWRpbmcgb2YgdGhlIHN0aWNrZXIgcGlja2VyIG92ZXJsYXlcclxuICAgICAqIEBwYXJhbSAge0V2ZW50fSBldiBFdmVudCB0aGF0IHRyaWdnZXJlZCB0aGUgZnVuY3Rpb24gY2FsbFxyXG4gICAgICovXHJcbiAgICBfb25IaWRlU3RpY2tlcnNDbGljayhldikge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dTdGlja2VyczogZmFsc2V9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGxlZCB3aGVuIHRoZSB3aW5kb3cgaXMgcmVzaXplZFxyXG4gICAgICovXHJcbiAgICBfb25SZXNpemUoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2hvd1N0aWNrZXJzOiBmYWxzZX0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIHN0aWNrZXJzIHBpY2tlciB3YXMgaGlkZGVuXHJcbiAgICAgKi9cclxuICAgIF9vbkZpbmlzaGVkKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dTdGlja2VyczogZmFsc2V9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExhdW5jaCB0aGUgaW50ZWdyYXRpb24gbWFuYWdlciBvbiB0aGUgc3RpY2tlcnMgaW50ZWdyYXRpb24gcGFnZVxyXG4gICAgICovXHJcbiAgICBfbGF1bmNoTWFuYWdlSW50ZWdyYXRpb25zKCkge1xyXG4gICAgICAgIC8vIFRPRE86IE9wZW4gdGhlIHJpZ2h0IGludGVncmF0aW9uIG1hbmFnZXIgZm9yIHRoZSB3aWRnZXRcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9tYW55X2ludGVncmF0aW9uX21hbmFnZXJzXCIpKSB7XHJcbiAgICAgICAgICAgIEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5vcGVuQWxsKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5yb29tLFxyXG4gICAgICAgICAgICAgICAgYHR5cGVfJHt3aWRnZXRUeXBlfWAsXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLndpZGdldElkLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5nZXRQcmltYXJ5TWFuYWdlcigpLm9wZW4oXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnJvb20sXHJcbiAgICAgICAgICAgICAgICBgdHlwZV8ke3dpZGdldFR5cGV9YCxcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUud2lkZ2V0SWQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgc3RpY2tlclBpY2tlcjtcclxuICAgICAgICBsZXQgc3RpY2tlcnNCdXR0b247XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2hvd1N0aWNrZXJzKSB7XHJcbiAgICAgICAgICAgIC8vIFNob3cgaGlkZS1zdGlja2VycyBidXR0b25cclxuICAgICAgICAgICAgc3RpY2tlcnNCdXR0b24gPVxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBpZD0nc3RpY2tlcnNCdXR0b24nXHJcbiAgICAgICAgICAgICAgICAgICAga2V5PVwiY29udHJvbHNfaGlkZV9zdGlja2Vyc1wiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbXBvc2VyX2J1dHRvbiBteF9NZXNzYWdlQ29tcG9zZXJfc3RpY2tlcnMgbXhfU3RpY2tlcnNfaGlkZVN0aWNrZXJzXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkhpZGVTdGlja2Vyc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIkhpZGUgU3RpY2tlcnNcIil9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgR2VuZXJpY0VsZW1lbnRDb250ZXh0TWVudSA9IHNkay5nZXRDb21wb25lbnQoJ2NvbnRleHRfbWVudXMuR2VuZXJpY0VsZW1lbnRDb250ZXh0TWVudScpO1xyXG4gICAgICAgICAgICBzdGlja2VyUGlja2VyID0gPENvbnRleHRNZW51XHJcbiAgICAgICAgICAgICAgICBjaGV2cm9uT2Zmc2V0PXt0aGlzLnN0YXRlLnN0aWNrZXJQaWNrZXJDaGV2cm9uT2Zmc2V0fVxyXG4gICAgICAgICAgICAgICAgY2hldnJvbkZhY2U9XCJib3R0b21cIlxyXG4gICAgICAgICAgICAgICAgbGVmdD17dGhpcy5zdGF0ZS5zdGlja2VyUGlja2VyWH1cclxuICAgICAgICAgICAgICAgIHRvcD17dGhpcy5zdGF0ZS5zdGlja2VyUGlja2VyWX1cclxuICAgICAgICAgICAgICAgIG1lbnVXaWR0aD17dGhpcy5wb3BvdmVyV2lkdGh9XHJcbiAgICAgICAgICAgICAgICBtZW51SGVpZ2h0PXt0aGlzLnBvcG92ZXJIZWlnaHR9XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLl9vbkZpbmlzaGVkfVxyXG4gICAgICAgICAgICAgICAgbWVudVBhZGRpbmdUb3A9ezB9XHJcbiAgICAgICAgICAgICAgICBtZW51UGFkZGluZ0xlZnQ9ezB9XHJcbiAgICAgICAgICAgICAgICBtZW51UGFkZGluZ1JpZ2h0PXswfVxyXG4gICAgICAgICAgICAgICAgekluZGV4PXtTVElDS0VSUElDS0VSX1pfSU5ERVh9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxHZW5lcmljRWxlbWVudENvbnRleHRNZW51IGVsZW1lbnQ9e3RoaXMuX2dldFN0aWNrZXJwaWNrZXJDb250ZW50KCl9IG9uUmVzaXplPXt0aGlzLl9vbkZpbmlzaGVkfSAvPlxyXG4gICAgICAgICAgICA8L0NvbnRleHRNZW51PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBTaG93IHNob3ctc3RpY2tlcnMgYnV0dG9uXHJcbiAgICAgICAgICAgIHN0aWNrZXJzQnV0dG9uID1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgaWQ9J3N0aWNrZXJzQnV0dG9uJ1xyXG4gICAgICAgICAgICAgICAgICAgIGtleT1cImNvbnRyb2xzX3Nob3dfc3RpY2tlcnNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb21wb3Nlcl9idXR0b24gbXhfTWVzc2FnZUNvbXBvc2VyX3N0aWNrZXJzXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblNob3dTdGlja2Vyc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlNob3cgU3RpY2tlcnNcIil9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICB7IHN0aWNrZXJzQnV0dG9uIH1cclxuICAgICAgICAgICAgeyBzdGlja2VyUGlja2VyIH1cclxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PjtcclxuICAgIH1cclxufVxyXG4iXX0=