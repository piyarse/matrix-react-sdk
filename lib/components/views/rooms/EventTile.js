"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getHandlerTile = getHandlerTile;
exports.haveTileForEvent = haveTileForEvent;
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _ReplyThread = _interopRequireDefault(require("../elements/ReplyThread"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var _languageHandler = require("../../../languageHandler");

var TextForEvent = _interopRequireWildcard(require("../../../TextForEvent"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _matrixJsSdk = require("matrix-js-sdk");

var _DateUtils = require("../../../DateUtils");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _BanList = require("../../../mjolnir/BanList");

var ObjectUtils = _interopRequireWildcard(require("../../../ObjectUtils"));

var _MatrixClientContext = _interopRequireDefault(require("../../../contexts/MatrixClientContext"));

var _E2EIcon = require("./E2EIcon");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const eventTileTypes = {
  'm.room.message': 'messages.MessageEvent',
  'm.sticker': 'messages.MessageEvent',
  'm.key.verification.cancel': 'messages.MKeyVerificationConclusion',
  'm.key.verification.done': 'messages.MKeyVerificationConclusion',
  'm.room.encryption': 'messages.EncryptionEvent',
  'm.call.invite': 'messages.TextualEvent',
  'm.call.answer': 'messages.TextualEvent',
  'm.call.hangup': 'messages.TextualEvent'
};
const stateEventTileTypes = {
  'm.room.encryption': 'messages.EncryptionEvent',
  'm.room.canonical_alias': 'messages.TextualEvent',
  'm.room.create': 'messages.RoomCreate',
  'm.room.member': 'messages.TextualEvent',
  'm.room.name': 'messages.TextualEvent',
  'm.room.avatar': 'messages.RoomAvatarEvent',
  'm.room.third_party_invite': 'messages.TextualEvent',
  'm.room.history_visibility': 'messages.TextualEvent',
  'm.room.topic': 'messages.TextualEvent',
  'm.room.power_levels': 'messages.TextualEvent',
  'm.room.pinned_events': 'messages.TextualEvent',
  'm.room.server_acl': 'messages.TextualEvent',
  'im.vector.modular.widgets': 'messages.TextualEvent',
  'm.room.tombstone': 'messages.TextualEvent',
  'm.room.join_rules': 'messages.TextualEvent',
  'm.room.guest_access': 'messages.TextualEvent',
  'm.room.related_groups': 'messages.TextualEvent'
}; // Add all the Mjolnir stuff to the renderer

for (const evType of _BanList.ALL_RULE_TYPES) {
  stateEventTileTypes[evType] = 'messages.TextualEvent';
}

function getHandlerTile(ev) {
  const type = ev.getType(); // don't show verification requests we're not involved in,
  // not even when showing hidden events

  if (type === "m.room.message") {
    const content = ev.getContent();

    if (content && content.msgtype === "m.key.verification.request") {
      const client = _MatrixClientPeg.MatrixClientPeg.get();

      const me = client && client.getUserId();

      if (ev.getSender() !== me && content.to !== me) {
        return undefined;
      } else {
        return "messages.MKeyVerificationRequest";
      }
    }
  } // these events are sent by both parties during verification, but we only want to render one
  // tile once the verification concludes, so filter out the one from the other party.


  if (type === "m.key.verification.done") {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const me = client && client.getUserId();

    if (ev.getSender() !== me) {
      return undefined;
    }
  } // sometimes MKeyVerificationConclusion declines to render.  Jankily decline to render and
  // fall back to showing hidden events, if we're viewing hidden events
  // XXX: This is extremely a hack. Possibly these components should have an interface for
  // declining to render?


  if (type === "m.key.verification.cancel" && _SettingsStore.default.getValue("showHiddenEventsInTimeline")) {
    const MKeyVerificationConclusion = sdk.getComponent("messages.MKeyVerificationConclusion");

    if (!MKeyVerificationConclusion.prototype._shouldRender.call(null, ev, ev.request)) {
      return;
    }
  }

  return ev.isState() ? stateEventTileTypes[type] : eventTileTypes[type];
}

const MAX_READ_AVATARS = 5; // Our component structure for EventTiles on the timeline is:
//
// .-EventTile------------------------------------------------.
// | MemberAvatar (SenderProfile)                   TimeStamp |
// |    .-{Message,Textual}Event---------------. Read Avatars |
// |    |   .-MFooBody-------------------.     |              |
// |    |   |  (only if MessageEvent)    |     |              |
// |    |   '----------------------------'     |              |
// |    '--------------------------------------'              |
// '----------------------------------------------------------'

var _default = (0, _createReactClass.default)({
  displayName: 'EventTile',
  propTypes: {
    /* the MatrixEvent to show */
    mxEvent: _propTypes.default.object.isRequired,

    /* true if mxEvent is redacted. This is a prop because using mxEvent.isRedacted()
     * might not be enough when deciding shouldComponentUpdate - prevProps.mxEvent
     * references the same this.props.mxEvent.
     */
    isRedacted: _propTypes.default.bool,

    /* true if this is a continuation of the previous event (which has the
     * effect of not showing another avatar/displayname
     */
    continuation: _propTypes.default.bool,

    /* true if this is the last event in the timeline (which has the effect
     * of always showing the timestamp)
     */
    last: _propTypes.default.bool,

    /* true if this is search context (which has the effect of greying out
     * the text
     */
    contextual: _propTypes.default.bool,

    /* a list of words to highlight, ordered by longest first */
    highlights: _propTypes.default.array,

    /* link URL for the highlights */
    highlightLink: _propTypes.default.string,

    /* should show URL previews for this event */
    showUrlPreview: _propTypes.default.bool,

    /* is this the focused event */
    isSelectedEvent: _propTypes.default.bool,

    /* callback called when dynamic content in events are loaded */
    onHeightChanged: _propTypes.default.func,

    /* a list of read-receipts we should show. Each object has a 'roomMember' and 'ts'. */
    readReceipts: _propTypes.default.arrayOf(_propTypes.default.object),

    /* opaque readreceipt info for each userId; used by ReadReceiptMarker
     * to manage its animations. Should be an empty object when the room
     * first loads
     */
    readReceiptMap: _propTypes.default.object,

    /* A function which is used to check if the parent panel is being
     * unmounted, to avoid unnecessary work. Should return true if we
     * are being unmounted.
     */
    checkUnmounting: _propTypes.default.func,

    /* the status of this event - ie, mxEvent.status. Denormalised to here so
     * that we can tell when it changes. */
    eventSendStatus: _propTypes.default.string,

    /* the shape of the tile. by default, the layout is intended for the
     * normal room timeline.  alternative values are: "file_list", "file_grid"
     * and "notif".  This could be done by CSS, but it'd be horribly inefficient.
     * It could also be done by subclassing EventTile, but that'd be quite
     * boiilerplatey.  So just make the necessary render decisions conditional
     * for now.
     */
    tileShape: _propTypes.default.string,
    // show twelve hour timestamps
    isTwelveHour: _propTypes.default.bool,
    // helper function to access relations for this event
    getRelationsForEvent: _propTypes.default.func,
    // whether to show reactions for this event
    showReactions: _propTypes.default.bool
  },
  getDefaultProps: function () {
    return {
      // no-op function because onHeightChanged is optional yet some sub-components assume its existence
      onHeightChanged: function () {}
    };
  },
  getInitialState: function () {
    return {
      // Whether the action bar is focused.
      actionBarFocused: false,
      // Whether all read receipts are being displayed. If not, only display
      // a truncation of them.
      allReadAvatars: false,
      // Whether the event's sender has been verified.
      verified: null,
      // Whether onRequestKeysClick has been called since mounting.
      previouslyRequestedKeys: false,
      // The Relations model from the JS SDK for reactions to `mxEvent`
      reactions: this.getReactions()
    };
  },
  statics: {
    contextType: _MatrixClientContext.default
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    // don't do RR animations until we are mounted
    this._suppressReadReceiptAnimation = true;

    this._verifyEvent(this.props.mxEvent);

    this._tile = (0, _react.createRef)();
    this._replyThread = (0, _react.createRef)();
  },
  componentDidMount: function () {
    this._suppressReadReceiptAnimation = false;
    const client = this.context;
    client.on("deviceVerificationChanged", this.onDeviceVerificationChanged);
    client.on("userTrustStatusChanged", this.onUserVerificationChanged);
    this.props.mxEvent.on("Event.decrypted", this._onDecrypted);

    if (this.props.showReactions) {
      this.props.mxEvent.on("Event.relationsCreated", this._onReactionsCreated);
    }
  },
  // TODO: [REACT-WARNING] Replace with appropriate lifecycle event
  UNSAFE_componentWillReceiveProps: function (nextProps) {
    // re-check the sender verification as outgoing events progress through
    // the send process.
    if (nextProps.eventSendStatus !== this.props.eventSendStatus) {
      this._verifyEvent(nextProps.mxEvent);
    }
  },
  shouldComponentUpdate: function (nextProps, nextState) {
    if (!ObjectUtils.shallowEqual(this.state, nextState)) {
      return true;
    }

    return !this._propsEqual(this.props, nextProps);
  },
  componentWillUnmount: function () {
    const client = this.context;
    client.removeListener("deviceVerificationChanged", this.onDeviceVerificationChanged);
    client.removeListener("userTrustStatusChanged", this.onUserVerificationChanged);
    this.props.mxEvent.removeListener("Event.decrypted", this._onDecrypted);

    if (this.props.showReactions) {
      this.props.mxEvent.removeListener("Event.relationsCreated", this._onReactionsCreated);
    }
  },

  /** called when the event is decrypted after we show it.
   */
  _onDecrypted: function () {
    // we need to re-verify the sending device.
    // (we call onHeightChanged in _verifyEvent to handle the case where decryption
    // has caused a change in size of the event tile)
    this._verifyEvent(this.props.mxEvent);

    this.forceUpdate();
  },
  onDeviceVerificationChanged: function (userId, device) {
    if (userId === this.props.mxEvent.getSender()) {
      this._verifyEvent(this.props.mxEvent);
    }
  },
  onUserVerificationChanged: function (userId, _trustStatus) {
    if (userId === this.props.mxEvent.getSender()) {
      this._verifyEvent(this.props.mxEvent);
    }
  },
  _verifyEvent: async function (mxEvent) {
    if (!mxEvent.isEncrypted()) {
      return;
    } // If we directly trust the device, short-circuit here


    const verified = await this.context.isEventSenderVerified(mxEvent);

    if (verified) {
      this.setState({
        verified: _E2EIcon.E2E_STATE.VERIFIED
      }, () => {
        // Decryption may have caused a change in size
        this.props.onHeightChanged();
      });
      return;
    } // If cross-signing is off, the old behaviour is to scream at the user
    // as if they've done something wrong, which they haven't


    if (!_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      this.setState({
        verified: _E2EIcon.E2E_STATE.WARNING
      }, this.props.onHeightChanged);
      return;
    }

    if (!this.context.checkUserTrust(mxEvent.getSender()).isCrossSigningVerified()) {
      this.setState({
        verified: _E2EIcon.E2E_STATE.NORMAL
      }, this.props.onHeightChanged);
      return;
    }

    const eventSenderTrust = await this.context.checkEventSenderTrust(mxEvent);

    if (!eventSenderTrust) {
      this.setState({
        verified: _E2EIcon.E2E_STATE.UNKNOWN
      }, this.props.onHeightChanged); // Decryption may have cause a change in size

      return;
    }

    this.setState({
      verified: eventSenderTrust.isVerified() ? _E2EIcon.E2E_STATE.VERIFIED : _E2EIcon.E2E_STATE.WARNING
    }, this.props.onHeightChanged); // Decryption may have caused a change in size
  },
  _propsEqual: function (objA, objB) {
    const keysA = Object.keys(objA);
    const keysB = Object.keys(objB);

    if (keysA.length !== keysB.length) {
      return false;
    }

    for (let i = 0; i < keysA.length; i++) {
      const key = keysA[i];

      if (!objB.hasOwnProperty(key)) {
        return false;
      } // need to deep-compare readReceipts


      if (key === 'readReceipts') {
        const rA = objA[key];
        const rB = objB[key];

        if (rA === rB) {
          continue;
        }

        if (!rA || !rB) {
          return false;
        }

        if (rA.length !== rB.length) {
          return false;
        }

        for (let j = 0; j < rA.length; j++) {
          if (rA[j].userId !== rB[j].userId) {
            return false;
          } // one has a member set and the other doesn't?


          if (rA[j].roomMember !== rB[j].roomMember) {
            return false;
          }
        }
      } else {
        if (objA[key] !== objB[key]) {
          return false;
        }
      }
    }

    return true;
  },
  shouldHighlight: function () {
    const actions = this.context.getPushActionsForEvent(this.props.mxEvent);

    if (!actions || !actions.tweaks) {
      return false;
    } // don't show self-highlights from another of our clients


    if (this.props.mxEvent.getSender() === this.context.credentials.userId) {
      return false;
    }

    return actions.tweaks.highlight;
  },
  toggleAllReadAvatars: function () {
    this.setState({
      allReadAvatars: !this.state.allReadAvatars
    });
  },
  getReadAvatars: function () {
    // return early if there are no read receipts
    if (!this.props.readReceipts || this.props.readReceipts.length === 0) {
      return _react.default.createElement("span", {
        className: "mx_EventTile_readAvatars"
      });
    }

    const ReadReceiptMarker = sdk.getComponent('rooms.ReadReceiptMarker');
    const avatars = [];
    const receiptOffset = 15;
    let left = 0;
    const receipts = this.props.readReceipts || [];

    for (let i = 0; i < receipts.length; ++i) {
      const receipt = receipts[i];
      let hidden = true;

      if (i < MAX_READ_AVATARS || this.state.allReadAvatars) {
        hidden = false;
      } // TODO: we keep the extra read avatars in the dom to make animation simpler
      // we could optimise this to reduce the dom size.
      // If hidden, set offset equal to the offset of the final visible avatar or
      // else set it proportional to index


      left = (hidden ? MAX_READ_AVATARS - 1 : i) * -receiptOffset;
      const userId = receipt.userId;
      let readReceiptInfo;

      if (this.props.readReceiptMap) {
        readReceiptInfo = this.props.readReceiptMap[userId];

        if (!readReceiptInfo) {
          readReceiptInfo = {};
          this.props.readReceiptMap[userId] = readReceiptInfo;
        }
      } // add to the start so the most recent is on the end (ie. ends up rightmost)


      avatars.unshift(_react.default.createElement(ReadReceiptMarker, {
        key: userId,
        member: receipt.roomMember,
        fallbackUserId: userId,
        leftOffset: left,
        hidden: hidden,
        readReceiptInfo: readReceiptInfo,
        checkUnmounting: this.props.checkUnmounting,
        suppressAnimation: this._suppressReadReceiptAnimation,
        onClick: this.toggleAllReadAvatars,
        timestamp: receipt.ts,
        showTwelveHour: this.props.isTwelveHour
      }));
    }

    let remText;

    if (!this.state.allReadAvatars) {
      const remainder = receipts.length - MAX_READ_AVATARS;

      if (remainder > 0) {
        remText = _react.default.createElement("span", {
          className: "mx_EventTile_readAvatarRemainder",
          onClick: this.toggleAllReadAvatars,
          style: {
            right: -(left - receiptOffset)
          }
        }, remainder, "+");
      }
    }

    return _react.default.createElement("span", {
      className: "mx_EventTile_readAvatars"
    }, remText, avatars);
  },
  onSenderProfileClick: function (event) {
    const mxEvent = this.props.mxEvent;

    _dispatcher.default.dispatch({
      action: 'insert_mention',
      user_id: mxEvent.getSender()
    });
  },
  onRequestKeysClick: function () {
    this.setState({
      // Indicate in the UI that the keys have been requested (this is expected to
      // be reset if the component is mounted in the future).
      previouslyRequestedKeys: true
    }); // Cancel any outgoing key request for this event and resend it. If a response
    // is received for the request with the required keys, the event could be
    // decrypted successfully.

    this.context.cancelAndResendEventRoomKeyRequest(this.props.mxEvent);
  },
  onPermalinkClicked: function (e) {
    // This allows the permalink to be opened in a new tab/window or copied as
    // matrix.to, but also for it to enable routing within Riot when clicked.
    e.preventDefault();

    _dispatcher.default.dispatch({
      action: 'view_room',
      event_id: this.props.mxEvent.getId(),
      highlighted: true,
      room_id: this.props.mxEvent.getRoomId()
    });
  },
  _renderE2EPadlock: function () {
    const ev = this.props.mxEvent; // event could not be decrypted

    if (ev.getContent().msgtype === 'm.bad.encrypted') {
      return _react.default.createElement(E2ePadlockUndecryptable, null);
    } // event is encrypted, display padlock corresponding to whether or not it is verified


    if (ev.isEncrypted()) {
      if (this.state.verified === _E2EIcon.E2E_STATE.NORMAL) {
        return; // no icon if we've not even cross-signed the user
      } else if (this.state.verified === _E2EIcon.E2E_STATE.VERIFIED) {
        return; // no icon for verified
      } else if (this.state.verified === _E2EIcon.E2E_STATE.UNKNOWN) {
        return _react.default.createElement(E2ePadlockUnknown, null);
      } else {
        return _react.default.createElement(E2ePadlockUnverified, null);
      }
    }

    if (this.context.isRoomEncrypted(ev.getRoomId())) {
      // else if room is encrypted
      // and event is being encrypted or is not_sent (Unknown Devices/Network Error)
      if (ev.status === _matrixJsSdk.EventStatus.ENCRYPTING) {
        return;
      }

      if (ev.status === _matrixJsSdk.EventStatus.NOT_SENT) {
        return;
      }

      if (ev.isState()) {
        return; // we expect this to be unencrypted
      } // if the event is not encrypted, but it's an e2e room, show the open padlock


      return _react.default.createElement(E2ePadlockUnencrypted, null);
    } // no padlock needed


    return null;
  },

  onActionBarFocusChange(focused) {
    this.setState({
      actionBarFocused: focused
    });
  },

  getTile() {
    return this._tile.current;
  },

  getReplyThread() {
    return this._replyThread.current;
  },

  getReactions() {
    if (!this.props.showReactions || !this.props.getRelationsForEvent) {
      return null;
    }

    const eventId = this.props.mxEvent.getId();

    if (!eventId) {
      // XXX: Temporary diagnostic logging for https://github.com/vector-im/riot-web/issues/11120
      console.error("EventTile attempted to get relations for an event without an ID"); // Use event's special `toJSON` method to log key data.

      console.log(JSON.stringify(this.props.mxEvent, null, 4));
      console.trace("Stacktrace for https://github.com/vector-im/riot-web/issues/11120");
    }

    return this.props.getRelationsForEvent(eventId, "m.annotation", "m.reaction");
  },

  _onReactionsCreated(relationType, eventType) {
    if (relationType !== "m.annotation" || eventType !== "m.reaction") {
      return;
    }

    this.props.mxEvent.removeListener("Event.relationsCreated", this._onReactionsCreated);
    this.setState({
      reactions: this.getReactions()
    });
  },

  render: function () {
    const MessageTimestamp = sdk.getComponent('messages.MessageTimestamp');
    const SenderProfile = sdk.getComponent('messages.SenderProfile');
    const MemberAvatar = sdk.getComponent('avatars.MemberAvatar'); //console.info("EventTile showUrlPreview for %s is %s", this.props.mxEvent.getId(), this.props.showUrlPreview);

    const content = this.props.mxEvent.getContent();
    const msgtype = content.msgtype;
    const eventType = this.props.mxEvent.getType(); // Info messages are basically information about commands processed on a room

    const isBubbleMessage = eventType.startsWith("m.key.verification") || eventType === "m.room.message" && msgtype && msgtype.startsWith("m.key.verification") || eventType === "m.room.encryption";
    let isInfoMessage = !isBubbleMessage && eventType !== 'm.room.message' && eventType !== 'm.sticker' && eventType !== 'm.room.create';
    let tileHandler = getHandlerTile(this.props.mxEvent); // If we're showing hidden events in the timeline, we should use the
    // source tile when there's no regular tile for an event and also for
    // replace relations (which otherwise would display as a confusing
    // duplicate of the thing they are replacing).

    const useSource = !tileHandler || this.props.mxEvent.isRelation("m.replace");

    if (useSource && _SettingsStore.default.getValue("showHiddenEventsInTimeline")) {
      tileHandler = "messages.ViewSourceEvent"; // Reuse info message avatar and sender profile styling

      isInfoMessage = true;
    } // This shouldn't happen: the caller should check we support this type
    // before trying to instantiate us


    if (!tileHandler) {
      const {
        mxEvent
      } = this.props;
      console.warn("Event type not supported: type:".concat(mxEvent.getType(), " isState:").concat(mxEvent.isState()));
      return _react.default.createElement("div", {
        className: "mx_EventTile mx_EventTile_info mx_MNoticeBody"
      }, _react.default.createElement("div", {
        className: "mx_EventTile_line"
      }, (0, _languageHandler._t)('This event could not be displayed')));
    }

    const EventTileType = sdk.getComponent(tileHandler);
    const isSending = ['sending', 'queued', 'encrypting'].indexOf(this.props.eventSendStatus) !== -1;
    const isRedacted = isMessageEvent(this.props.mxEvent) && this.props.isRedacted;
    const isEncryptionFailure = this.props.mxEvent.isDecryptionFailure();
    const isEditing = !!this.props.editState;
    const classes = (0, _classnames.default)({
      mx_EventTile_bubbleContainer: isBubbleMessage,
      mx_EventTile: true,
      mx_EventTile_isEditing: isEditing,
      mx_EventTile_info: isInfoMessage,
      mx_EventTile_12hr: this.props.isTwelveHour,
      mx_EventTile_encrypting: this.props.eventSendStatus === 'encrypting',
      mx_EventTile_sending: !isEditing && isSending,
      mx_EventTile_notSent: this.props.eventSendStatus === 'not_sent',
      mx_EventTile_highlight: this.props.tileShape === 'notif' ? false : this.shouldHighlight(),
      mx_EventTile_selected: this.props.isSelectedEvent,
      mx_EventTile_continuation: this.props.tileShape ? '' : this.props.continuation,
      mx_EventTile_last: this.props.last,
      mx_EventTile_contextual: this.props.contextual,
      mx_EventTile_actionBarFocused: this.state.actionBarFocused,
      mx_EventTile_verified: !isBubbleMessage && this.state.verified === _E2EIcon.E2E_STATE.VERIFIED,
      mx_EventTile_unverified: !isBubbleMessage && this.state.verified === _E2EIcon.E2E_STATE.WARNING,
      mx_EventTile_unknown: !isBubbleMessage && this.state.verified === _E2EIcon.E2E_STATE.UNKNOWN,
      mx_EventTile_bad: isEncryptionFailure,
      mx_EventTile_emote: msgtype === 'm.emote',
      mx_EventTile_redacted: isRedacted
    });
    let permalink = "#";

    if (this.props.permalinkCreator) {
      permalink = this.props.permalinkCreator.forEvent(this.props.mxEvent.getId());
    }

    const readAvatars = this.getReadAvatars();
    let avatar;
    let sender;
    let avatarSize;
    let needsSenderProfile;

    if (this.props.tileShape === "notif") {
      avatarSize = 24;
      needsSenderProfile = true;
    } else if (tileHandler === 'messages.RoomCreate' || isBubbleMessage) {
      avatarSize = 0;
      needsSenderProfile = false;
    } else if (isInfoMessage) {
      // a small avatar, with no sender profile, for
      // joins/parts/etc
      avatarSize = 14;
      needsSenderProfile = false;
    } else if (this.props.continuation && this.props.tileShape !== "file_grid") {
      // no avatar or sender profile for continuation messages
      avatarSize = 0;
      needsSenderProfile = false;
    } else {
      avatarSize = 30;
      needsSenderProfile = true;
    }

    if (this.props.mxEvent.sender && avatarSize) {
      avatar = _react.default.createElement("div", {
        className: "mx_EventTile_avatar"
      }, _react.default.createElement(MemberAvatar, {
        member: this.props.mxEvent.sender,
        width: avatarSize,
        height: avatarSize,
        viewUserOnClick: true
      }));
    }

    if (needsSenderProfile) {
      let text = null;

      if (!this.props.tileShape || this.props.tileShape === 'reply' || this.props.tileShape === 'reply_preview') {
        if (msgtype === 'm.image') text = (0, _languageHandler._td)('%(senderName)s sent an image');else if (msgtype === 'm.video') text = (0, _languageHandler._td)('%(senderName)s sent a video');else if (msgtype === 'm.file') text = (0, _languageHandler._td)('%(senderName)s uploaded a file');
        sender = _react.default.createElement(SenderProfile, {
          onClick: this.onSenderProfileClick,
          mxEvent: this.props.mxEvent,
          enableFlair: !text,
          text: text
        });
      } else {
        sender = _react.default.createElement(SenderProfile, {
          mxEvent: this.props.mxEvent,
          enableFlair: true
        });
      }
    }

    const MessageActionBar = sdk.getComponent('messages.MessageActionBar');
    const actionBar = !isEditing ? _react.default.createElement(MessageActionBar, {
      mxEvent: this.props.mxEvent,
      reactions: this.state.reactions,
      permalinkCreator: this.props.permalinkCreator,
      getTile: this.getTile,
      getReplyThread: this.getReplyThread,
      onFocusChange: this.onActionBarFocusChange
    }) : undefined;
    const timestamp = this.props.mxEvent.getTs() ? _react.default.createElement(MessageTimestamp, {
      showTwelveHour: this.props.isTwelveHour,
      ts: this.props.mxEvent.getTs()
    }) : null;

    const keyRequestHelpText = _react.default.createElement("div", {
      className: "mx_EventTile_keyRequestInfo_tooltip_contents"
    }, _react.default.createElement("p", null, this.state.previouslyRequestedKeys ? (0, _languageHandler._t)('Your key share request has been sent - please check your other sessions ' + 'for key share requests.') : (0, _languageHandler._t)('Key share requests are sent to your other sessions automatically. If you ' + 'rejected or dismissed the key share request on your other sessions, click ' + 'here to request the keys for this session again.')), _react.default.createElement("p", null, (0, _languageHandler._t)('If your other sessions do not have the key for this message you will not ' + 'be able to decrypt them.')));

    const keyRequestInfoContent = this.state.previouslyRequestedKeys ? (0, _languageHandler._t)('Key request sent.') : (0, _languageHandler._t)('<requestLink>Re-request encryption keys</requestLink> from your other sessions.', {}, {
      'requestLink': sub => _react.default.createElement("a", {
        onClick: this.onRequestKeysClick
      }, sub)
    });
    const TooltipButton = sdk.getComponent('elements.TooltipButton');
    const keyRequestInfo = isEncryptionFailure ? _react.default.createElement("div", {
      className: "mx_EventTile_keyRequestInfo"
    }, _react.default.createElement("span", {
      className: "mx_EventTile_keyRequestInfo_text"
    }, keyRequestInfoContent), _react.default.createElement(TooltipButton, {
      helpText: keyRequestHelpText
    })) : null;
    let reactionsRow;

    if (!isRedacted) {
      const ReactionsRow = sdk.getComponent('messages.ReactionsRow');
      reactionsRow = _react.default.createElement(ReactionsRow, {
        mxEvent: this.props.mxEvent,
        reactions: this.state.reactions
      });
    }

    switch (this.props.tileShape) {
      case 'notif':
        {
          const room = this.context.getRoom(this.props.mxEvent.getRoomId());
          return _react.default.createElement("div", {
            className: classes
          }, _react.default.createElement("div", {
            className: "mx_EventTile_roomName"
          }, _react.default.createElement("a", {
            href: permalink,
            onClick: this.onPermalinkClicked
          }, room ? room.name : '')), _react.default.createElement("div", {
            className: "mx_EventTile_senderDetails"
          }, avatar, _react.default.createElement("a", {
            href: permalink,
            onClick: this.onPermalinkClicked
          }, sender, timestamp)), _react.default.createElement("div", {
            className: "mx_EventTile_line"
          }, _react.default.createElement(EventTileType, {
            ref: this._tile,
            mxEvent: this.props.mxEvent,
            highlights: this.props.highlights,
            highlightLink: this.props.highlightLink,
            showUrlPreview: this.props.showUrlPreview,
            onHeightChanged: this.props.onHeightChanged
          })));
        }

      case 'file_grid':
        {
          return _react.default.createElement("div", {
            className: classes
          }, _react.default.createElement("div", {
            className: "mx_EventTile_line"
          }, _react.default.createElement(EventTileType, {
            ref: this._tile,
            mxEvent: this.props.mxEvent,
            highlights: this.props.highlights,
            highlightLink: this.props.highlightLink,
            showUrlPreview: this.props.showUrlPreview,
            tileShape: this.props.tileShape,
            onHeightChanged: this.props.onHeightChanged
          })), _react.default.createElement("a", {
            className: "mx_EventTile_senderDetailsLink",
            href: permalink,
            onClick: this.onPermalinkClicked
          }, _react.default.createElement("div", {
            className: "mx_EventTile_senderDetails"
          }, sender, timestamp)));
        }

      case 'reply':
      case 'reply_preview':
        {
          let thread;

          if (this.props.tileShape === 'reply_preview') {
            thread = _ReplyThread.default.makeThread(this.props.mxEvent, this.props.onHeightChanged, this.props.permalinkCreator, this._replyThread);
          }

          return _react.default.createElement("div", {
            className: classes
          }, avatar, sender, _react.default.createElement("div", {
            className: "mx_EventTile_reply"
          }, _react.default.createElement("a", {
            href: permalink,
            onClick: this.onPermalinkClicked
          }, timestamp), !isBubbleMessage && this._renderE2EPadlock(), thread, _react.default.createElement(EventTileType, {
            ref: this._tile,
            mxEvent: this.props.mxEvent,
            highlights: this.props.highlights,
            highlightLink: this.props.highlightLink,
            onHeightChanged: this.props.onHeightChanged,
            showUrlPreview: false
          })));
        }

      default:
        {
          const thread = _ReplyThread.default.makeThread(this.props.mxEvent, this.props.onHeightChanged, this.props.permalinkCreator, this._replyThread); // tab-index=-1 to allow it to be focusable but do not add tab stop for it, primarily for screen readers


          return _react.default.createElement("div", {
            className: classes,
            tabIndex: -1
          }, _react.default.createElement("div", {
            className: "mx_EventTile_msgOption"
          }, readAvatars), sender, _react.default.createElement("div", {
            className: "mx_EventTile_line"
          }, _react.default.createElement("a", {
            href: permalink,
            onClick: this.onPermalinkClicked,
            "aria-label": (0, _DateUtils.formatTime)(new Date(this.props.mxEvent.getTs()), this.props.isTwelveHour)
          }, timestamp), !isBubbleMessage && this._renderE2EPadlock(), thread, _react.default.createElement(EventTileType, {
            ref: this._tile,
            mxEvent: this.props.mxEvent,
            replacingEventId: this.props.replacingEventId,
            editState: this.props.editState,
            highlights: this.props.highlights,
            highlightLink: this.props.highlightLink,
            showUrlPreview: this.props.showUrlPreview,
            onHeightChanged: this.props.onHeightChanged
          }), keyRequestInfo, reactionsRow, actionBar), avatar);
        }
    }
  }
}); // XXX this'll eventually be dynamic based on the fields once we have extensible event types


exports.default = _default;
const messageTypes = ['m.room.message', 'm.sticker'];

function isMessageEvent(ev) {
  return messageTypes.includes(ev.getType());
}

function haveTileForEvent(e) {
  // Only messages have a tile (black-rectangle) if redacted
  if (e.isRedacted() && !isMessageEvent(e)) return false; // No tile for replacement events since they update the original tile

  if (e.isRelation("m.replace")) return false;
  const handler = getHandlerTile(e);
  if (handler === undefined) return false;

  if (handler === 'messages.TextualEvent') {
    return TextForEvent.textForEvent(e) !== '';
  } else if (handler === 'messages.RoomCreate') {
    return Boolean(e.getContent()['predecessor']);
  } else {
    return true;
  }
}

function E2ePadlockUndecryptable(props) {
  return _react.default.createElement(E2ePadlock, (0, _extends2.default)({
    title: (0, _languageHandler._t)("This message cannot be decrypted"),
    icon: "undecryptable"
  }, props));
}

function E2ePadlockUnverified(props) {
  return _react.default.createElement(E2ePadlock, (0, _extends2.default)({
    title: (0, _languageHandler._t)("Encrypted by an unverified session"),
    icon: "unverified"
  }, props));
}

function E2ePadlockUnencrypted(props) {
  return _react.default.createElement(E2ePadlock, (0, _extends2.default)({
    title: (0, _languageHandler._t)("Unencrypted"),
    icon: "unencrypted"
  }, props));
}

function E2ePadlockUnknown(props) {
  return _react.default.createElement(E2ePadlock, (0, _extends2.default)({
    title: (0, _languageHandler._t)("Encrypted by a deleted session"),
    icon: "unknown"
  }, props));
}

class E2ePadlock extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onHoverStart", () => {
      this.setState({
        hover: true
      });
    });
    (0, _defineProperty2.default)(this, "onHoverEnd", () => {
      this.setState({
        hover: false
      });
    });
    this.state = {
      hover: false
    };
  }

  render() {
    let tooltip = null;

    if (this.state.hover) {
      const Tooltip = sdk.getComponent("elements.Tooltip");
      tooltip = _react.default.createElement(Tooltip, {
        className: "mx_EventTile_e2eIcon_tooltip",
        label: this.props.title,
        dir: "auto"
      });
    }

    let classes = "mx_EventTile_e2eIcon mx_EventTile_e2eIcon_".concat(this.props.icon);

    if (!_SettingsStore.default.getValue("alwaysShowEncryptionIcons")) {
      classes += ' mx_EventTile_e2eIcon_hidden';
    }

    return _react.default.createElement("div", {
      className: classes,
      onClick: this.onClick,
      onMouseEnter: this.onHoverStart,
      onMouseLeave: this.onHoverEnd
    }, tooltip);
  }

}

(0, _defineProperty2.default)(E2ePadlock, "propTypes", {
  icon: _propTypes.default.string.isRequired,
  title: _propTypes.default.string.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL0V2ZW50VGlsZS5qcyJdLCJuYW1lcyI6WyJldmVudFRpbGVUeXBlcyIsInN0YXRlRXZlbnRUaWxlVHlwZXMiLCJldlR5cGUiLCJBTExfUlVMRV9UWVBFUyIsImdldEhhbmRsZXJUaWxlIiwiZXYiLCJ0eXBlIiwiZ2V0VHlwZSIsImNvbnRlbnQiLCJnZXRDb250ZW50IiwibXNndHlwZSIsImNsaWVudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm1lIiwiZ2V0VXNlcklkIiwiZ2V0U2VuZGVyIiwidG8iLCJ1bmRlZmluZWQiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJNS2V5VmVyaWZpY2F0aW9uQ29uY2x1c2lvbiIsInNkayIsImdldENvbXBvbmVudCIsInByb3RvdHlwZSIsIl9zaG91bGRSZW5kZXIiLCJjYWxsIiwicmVxdWVzdCIsImlzU3RhdGUiLCJNQVhfUkVBRF9BVkFUQVJTIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJteEV2ZW50IiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImlzUmVkYWN0ZWQiLCJib29sIiwiY29udGludWF0aW9uIiwibGFzdCIsImNvbnRleHR1YWwiLCJoaWdobGlnaHRzIiwiYXJyYXkiLCJoaWdobGlnaHRMaW5rIiwic3RyaW5nIiwic2hvd1VybFByZXZpZXciLCJpc1NlbGVjdGVkRXZlbnQiLCJvbkhlaWdodENoYW5nZWQiLCJmdW5jIiwicmVhZFJlY2VpcHRzIiwiYXJyYXlPZiIsInJlYWRSZWNlaXB0TWFwIiwiY2hlY2tVbm1vdW50aW5nIiwiZXZlbnRTZW5kU3RhdHVzIiwidGlsZVNoYXBlIiwiaXNUd2VsdmVIb3VyIiwiZ2V0UmVsYXRpb25zRm9yRXZlbnQiLCJzaG93UmVhY3Rpb25zIiwiZ2V0RGVmYXVsdFByb3BzIiwiZ2V0SW5pdGlhbFN0YXRlIiwiYWN0aW9uQmFyRm9jdXNlZCIsImFsbFJlYWRBdmF0YXJzIiwidmVyaWZpZWQiLCJwcmV2aW91c2x5UmVxdWVzdGVkS2V5cyIsInJlYWN0aW9ucyIsImdldFJlYWN0aW9ucyIsInN0YXRpY3MiLCJjb250ZXh0VHlwZSIsIk1hdHJpeENsaWVudENvbnRleHQiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiX3N1cHByZXNzUmVhZFJlY2VpcHRBbmltYXRpb24iLCJfdmVyaWZ5RXZlbnQiLCJwcm9wcyIsIl90aWxlIiwiX3JlcGx5VGhyZWFkIiwiY29tcG9uZW50RGlkTW91bnQiLCJjb250ZXh0Iiwib24iLCJvbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQiLCJvblVzZXJWZXJpZmljYXRpb25DaGFuZ2VkIiwiX29uRGVjcnlwdGVkIiwiX29uUmVhY3Rpb25zQ3JlYXRlZCIsIlVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIiwibmV4dFByb3BzIiwic2hvdWxkQ29tcG9uZW50VXBkYXRlIiwibmV4dFN0YXRlIiwiT2JqZWN0VXRpbHMiLCJzaGFsbG93RXF1YWwiLCJzdGF0ZSIsIl9wcm9wc0VxdWFsIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZW1vdmVMaXN0ZW5lciIsImZvcmNlVXBkYXRlIiwidXNlcklkIiwiZGV2aWNlIiwiX3RydXN0U3RhdHVzIiwiaXNFbmNyeXB0ZWQiLCJpc0V2ZW50U2VuZGVyVmVyaWZpZWQiLCJzZXRTdGF0ZSIsIkUyRV9TVEFURSIsIlZFUklGSUVEIiwiaXNGZWF0dXJlRW5hYmxlZCIsIldBUk5JTkciLCJjaGVja1VzZXJUcnVzdCIsImlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQiLCJOT1JNQUwiLCJldmVudFNlbmRlclRydXN0IiwiY2hlY2tFdmVudFNlbmRlclRydXN0IiwiVU5LTk9XTiIsImlzVmVyaWZpZWQiLCJvYmpBIiwib2JqQiIsImtleXNBIiwiT2JqZWN0Iiwia2V5cyIsImtleXNCIiwibGVuZ3RoIiwiaSIsImtleSIsImhhc093blByb3BlcnR5IiwickEiLCJyQiIsImoiLCJyb29tTWVtYmVyIiwic2hvdWxkSGlnaGxpZ2h0IiwiYWN0aW9ucyIsImdldFB1c2hBY3Rpb25zRm9yRXZlbnQiLCJ0d2Vha3MiLCJjcmVkZW50aWFscyIsImhpZ2hsaWdodCIsInRvZ2dsZUFsbFJlYWRBdmF0YXJzIiwiZ2V0UmVhZEF2YXRhcnMiLCJSZWFkUmVjZWlwdE1hcmtlciIsImF2YXRhcnMiLCJyZWNlaXB0T2Zmc2V0IiwibGVmdCIsInJlY2VpcHRzIiwicmVjZWlwdCIsImhpZGRlbiIsInJlYWRSZWNlaXB0SW5mbyIsInVuc2hpZnQiLCJ0cyIsInJlbVRleHQiLCJyZW1haW5kZXIiLCJyaWdodCIsIm9uU2VuZGVyUHJvZmlsZUNsaWNrIiwiZXZlbnQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInVzZXJfaWQiLCJvblJlcXVlc3RLZXlzQ2xpY2siLCJjYW5jZWxBbmRSZXNlbmRFdmVudFJvb21LZXlSZXF1ZXN0Iiwib25QZXJtYWxpbmtDbGlja2VkIiwiZSIsInByZXZlbnREZWZhdWx0IiwiZXZlbnRfaWQiLCJnZXRJZCIsImhpZ2hsaWdodGVkIiwicm9vbV9pZCIsImdldFJvb21JZCIsIl9yZW5kZXJFMkVQYWRsb2NrIiwiaXNSb29tRW5jcnlwdGVkIiwic3RhdHVzIiwiRXZlbnRTdGF0dXMiLCJFTkNSWVBUSU5HIiwiTk9UX1NFTlQiLCJvbkFjdGlvbkJhckZvY3VzQ2hhbmdlIiwiZm9jdXNlZCIsImdldFRpbGUiLCJjdXJyZW50IiwiZ2V0UmVwbHlUaHJlYWQiLCJldmVudElkIiwiY29uc29sZSIsImVycm9yIiwibG9nIiwiSlNPTiIsInN0cmluZ2lmeSIsInRyYWNlIiwicmVsYXRpb25UeXBlIiwiZXZlbnRUeXBlIiwicmVuZGVyIiwiTWVzc2FnZVRpbWVzdGFtcCIsIlNlbmRlclByb2ZpbGUiLCJNZW1iZXJBdmF0YXIiLCJpc0J1YmJsZU1lc3NhZ2UiLCJzdGFydHNXaXRoIiwiaXNJbmZvTWVzc2FnZSIsInRpbGVIYW5kbGVyIiwidXNlU291cmNlIiwiaXNSZWxhdGlvbiIsIndhcm4iLCJFdmVudFRpbGVUeXBlIiwiaXNTZW5kaW5nIiwiaW5kZXhPZiIsImlzTWVzc2FnZUV2ZW50IiwiaXNFbmNyeXB0aW9uRmFpbHVyZSIsImlzRGVjcnlwdGlvbkZhaWx1cmUiLCJpc0VkaXRpbmciLCJlZGl0U3RhdGUiLCJjbGFzc2VzIiwibXhfRXZlbnRUaWxlX2J1YmJsZUNvbnRhaW5lciIsIm14X0V2ZW50VGlsZSIsIm14X0V2ZW50VGlsZV9pc0VkaXRpbmciLCJteF9FdmVudFRpbGVfaW5mbyIsIm14X0V2ZW50VGlsZV8xMmhyIiwibXhfRXZlbnRUaWxlX2VuY3J5cHRpbmciLCJteF9FdmVudFRpbGVfc2VuZGluZyIsIm14X0V2ZW50VGlsZV9ub3RTZW50IiwibXhfRXZlbnRUaWxlX2hpZ2hsaWdodCIsIm14X0V2ZW50VGlsZV9zZWxlY3RlZCIsIm14X0V2ZW50VGlsZV9jb250aW51YXRpb24iLCJteF9FdmVudFRpbGVfbGFzdCIsIm14X0V2ZW50VGlsZV9jb250ZXh0dWFsIiwibXhfRXZlbnRUaWxlX2FjdGlvbkJhckZvY3VzZWQiLCJteF9FdmVudFRpbGVfdmVyaWZpZWQiLCJteF9FdmVudFRpbGVfdW52ZXJpZmllZCIsIm14X0V2ZW50VGlsZV91bmtub3duIiwibXhfRXZlbnRUaWxlX2JhZCIsIm14X0V2ZW50VGlsZV9lbW90ZSIsIm14X0V2ZW50VGlsZV9yZWRhY3RlZCIsInBlcm1hbGluayIsInBlcm1hbGlua0NyZWF0b3IiLCJmb3JFdmVudCIsInJlYWRBdmF0YXJzIiwiYXZhdGFyIiwic2VuZGVyIiwiYXZhdGFyU2l6ZSIsIm5lZWRzU2VuZGVyUHJvZmlsZSIsInRleHQiLCJNZXNzYWdlQWN0aW9uQmFyIiwiYWN0aW9uQmFyIiwidGltZXN0YW1wIiwiZ2V0VHMiLCJrZXlSZXF1ZXN0SGVscFRleHQiLCJrZXlSZXF1ZXN0SW5mb0NvbnRlbnQiLCJzdWIiLCJUb29sdGlwQnV0dG9uIiwia2V5UmVxdWVzdEluZm8iLCJyZWFjdGlvbnNSb3ciLCJSZWFjdGlvbnNSb3ciLCJyb29tIiwiZ2V0Um9vbSIsIm5hbWUiLCJ0aHJlYWQiLCJSZXBseVRocmVhZCIsIm1ha2VUaHJlYWQiLCJEYXRlIiwicmVwbGFjaW5nRXZlbnRJZCIsIm1lc3NhZ2VUeXBlcyIsImluY2x1ZGVzIiwiaGF2ZVRpbGVGb3JFdmVudCIsImhhbmRsZXIiLCJUZXh0Rm9yRXZlbnQiLCJ0ZXh0Rm9yRXZlbnQiLCJCb29sZWFuIiwiRTJlUGFkbG9ja1VuZGVjcnlwdGFibGUiLCJFMmVQYWRsb2NrVW52ZXJpZmllZCIsIkUyZVBhZGxvY2tVbmVuY3J5cHRlZCIsIkUyZVBhZGxvY2tVbmtub3duIiwiRTJlUGFkbG9jayIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJob3ZlciIsInRvb2x0aXAiLCJUb29sdGlwIiwidGl0bGUiLCJpY29uIiwib25DbGljayIsIm9uSG92ZXJTdGFydCIsIm9uSG92ZXJFbmQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQW5DQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUNBLE1BQU1BLGNBQWMsR0FBRztBQUNuQixvQkFBa0IsdUJBREM7QUFFbkIsZUFBYSx1QkFGTTtBQUduQiwrQkFBNkIscUNBSFY7QUFJbkIsNkJBQTJCLHFDQUpSO0FBS25CLHVCQUFxQiwwQkFMRjtBQU1uQixtQkFBaUIsdUJBTkU7QUFPbkIsbUJBQWlCLHVCQVBFO0FBUW5CLG1CQUFpQjtBQVJFLENBQXZCO0FBV0EsTUFBTUMsbUJBQW1CLEdBQUc7QUFDeEIsdUJBQXFCLDBCQURHO0FBRXhCLDRCQUEwQix1QkFGRjtBQUd4QixtQkFBaUIscUJBSE87QUFJeEIsbUJBQWlCLHVCQUpPO0FBS3hCLGlCQUFlLHVCQUxTO0FBTXhCLG1CQUFpQiwwQkFOTztBQU94QiwrQkFBNkIsdUJBUEw7QUFReEIsK0JBQTZCLHVCQVJMO0FBU3hCLGtCQUFnQix1QkFUUTtBQVV4Qix5QkFBdUIsdUJBVkM7QUFXeEIsMEJBQXdCLHVCQVhBO0FBWXhCLHVCQUFxQix1QkFaRztBQWF4QiwrQkFBNkIsdUJBYkw7QUFjeEIsc0JBQW9CLHVCQWRJO0FBZXhCLHVCQUFxQix1QkFmRztBQWdCeEIseUJBQXVCLHVCQWhCQztBQWlCeEIsMkJBQXlCO0FBakJELENBQTVCLEMsQ0FvQkE7O0FBQ0EsS0FBSyxNQUFNQyxNQUFYLElBQXFCQyx1QkFBckIsRUFBcUM7QUFDakNGLEVBQUFBLG1CQUFtQixDQUFDQyxNQUFELENBQW5CLEdBQThCLHVCQUE5QjtBQUNIOztBQUVNLFNBQVNFLGNBQVQsQ0FBd0JDLEVBQXhCLEVBQTRCO0FBQy9CLFFBQU1DLElBQUksR0FBR0QsRUFBRSxDQUFDRSxPQUFILEVBQWIsQ0FEK0IsQ0FHL0I7QUFDQTs7QUFDQSxNQUFJRCxJQUFJLEtBQUssZ0JBQWIsRUFBK0I7QUFDM0IsVUFBTUUsT0FBTyxHQUFHSCxFQUFFLENBQUNJLFVBQUgsRUFBaEI7O0FBQ0EsUUFBSUQsT0FBTyxJQUFJQSxPQUFPLENBQUNFLE9BQVIsS0FBb0IsNEJBQW5DLEVBQWlFO0FBQzdELFlBQU1DLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFlBQU1DLEVBQUUsR0FBR0gsTUFBTSxJQUFJQSxNQUFNLENBQUNJLFNBQVAsRUFBckI7O0FBQ0EsVUFBSVYsRUFBRSxDQUFDVyxTQUFILE9BQW1CRixFQUFuQixJQUF5Qk4sT0FBTyxDQUFDUyxFQUFSLEtBQWVILEVBQTVDLEVBQWdEO0FBQzVDLGVBQU9JLFNBQVA7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLGtDQUFQO0FBQ0g7QUFDSjtBQUNKLEdBaEI4QixDQWlCL0I7QUFDQTs7O0FBQ0EsTUFBSVosSUFBSSxLQUFLLHlCQUFiLEVBQXdDO0FBQ3BDLFVBQU1LLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFVBQU1DLEVBQUUsR0FBR0gsTUFBTSxJQUFJQSxNQUFNLENBQUNJLFNBQVAsRUFBckI7O0FBQ0EsUUFBSVYsRUFBRSxDQUFDVyxTQUFILE9BQW1CRixFQUF2QixFQUEyQjtBQUN2QixhQUFPSSxTQUFQO0FBQ0g7QUFDSixHQXpCOEIsQ0EyQi9CO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFJWixJQUFJLEtBQUssMkJBQVQsSUFBd0NhLHVCQUFjQyxRQUFkLENBQXVCLDRCQUF2QixDQUE1QyxFQUFrRztBQUM5RixVQUFNQywwQkFBMEIsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFDQUFqQixDQUFuQzs7QUFDQSxRQUFJLENBQUNGLDBCQUEwQixDQUFDRyxTQUEzQixDQUFxQ0MsYUFBckMsQ0FBbURDLElBQW5ELENBQXdELElBQXhELEVBQThEckIsRUFBOUQsRUFBa0VBLEVBQUUsQ0FBQ3NCLE9BQXJFLENBQUwsRUFBb0Y7QUFDaEY7QUFDSDtBQUNKOztBQUVELFNBQU90QixFQUFFLENBQUN1QixPQUFILEtBQWUzQixtQkFBbUIsQ0FBQ0ssSUFBRCxDQUFsQyxHQUEyQ04sY0FBYyxDQUFDTSxJQUFELENBQWhFO0FBQ0g7O0FBRUQsTUFBTXVCLGdCQUFnQixHQUFHLENBQXpCLEMsQ0FFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7ZUFFZSwrQkFBaUI7QUFDNUJDLEVBQUFBLFdBQVcsRUFBRSxXQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBQyxJQUFBQSxPQUFPLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQUZuQjs7QUFJUDs7OztBQUlBQyxJQUFBQSxVQUFVLEVBQUVILG1CQUFVSSxJQVJmOztBQVVQOzs7QUFHQUMsSUFBQUEsWUFBWSxFQUFFTCxtQkFBVUksSUFiakI7O0FBZVA7OztBQUdBRSxJQUFBQSxJQUFJLEVBQUVOLG1CQUFVSSxJQWxCVDs7QUFvQlA7OztBQUdBRyxJQUFBQSxVQUFVLEVBQUVQLG1CQUFVSSxJQXZCZjs7QUF5QlA7QUFDQUksSUFBQUEsVUFBVSxFQUFFUixtQkFBVVMsS0ExQmY7O0FBNEJQO0FBQ0FDLElBQUFBLGFBQWEsRUFBRVYsbUJBQVVXLE1BN0JsQjs7QUErQlA7QUFDQUMsSUFBQUEsY0FBYyxFQUFFWixtQkFBVUksSUFoQ25COztBQWtDUDtBQUNBUyxJQUFBQSxlQUFlLEVBQUViLG1CQUFVSSxJQW5DcEI7O0FBcUNQO0FBQ0FVLElBQUFBLGVBQWUsRUFBRWQsbUJBQVVlLElBdENwQjs7QUF3Q1A7QUFDQUMsSUFBQUEsWUFBWSxFQUFFaEIsbUJBQVVpQixPQUFWLENBQWtCakIsbUJBQVVDLE1BQTVCLENBekNQOztBQTJDUDs7OztBQUlBaUIsSUFBQUEsY0FBYyxFQUFFbEIsbUJBQVVDLE1BL0NuQjs7QUFpRFA7Ozs7QUFJQWtCLElBQUFBLGVBQWUsRUFBRW5CLG1CQUFVZSxJQXJEcEI7O0FBdURQOztBQUVBSyxJQUFBQSxlQUFlLEVBQUVwQixtQkFBVVcsTUF6RHBCOztBQTJEUDs7Ozs7OztBQU9BVSxJQUFBQSxTQUFTLEVBQUVyQixtQkFBVVcsTUFsRWQ7QUFvRVA7QUFDQVcsSUFBQUEsWUFBWSxFQUFFdEIsbUJBQVVJLElBckVqQjtBQXVFUDtBQUNBbUIsSUFBQUEsb0JBQW9CLEVBQUV2QixtQkFBVWUsSUF4RXpCO0FBMEVQO0FBQ0FTLElBQUFBLGFBQWEsRUFBRXhCLG1CQUFVSTtBQTNFbEIsR0FIaUI7QUFpRjVCcUIsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIO0FBQ0FYLE1BQUFBLGVBQWUsRUFBRSxZQUFXLENBQUU7QUFGM0IsS0FBUDtBQUlILEdBdEYyQjtBQXdGNUJZLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSDtBQUNBQyxNQUFBQSxnQkFBZ0IsRUFBRSxLQUZmO0FBR0g7QUFDQTtBQUNBQyxNQUFBQSxjQUFjLEVBQUUsS0FMYjtBQU1IO0FBQ0FDLE1BQUFBLFFBQVEsRUFBRSxJQVBQO0FBUUg7QUFDQUMsTUFBQUEsdUJBQXVCLEVBQUUsS0FUdEI7QUFVSDtBQUNBQyxNQUFBQSxTQUFTLEVBQUUsS0FBS0MsWUFBTDtBQVhSLEtBQVA7QUFhSCxHQXRHMkI7QUF3RzVCQyxFQUFBQSxPQUFPLEVBQUU7QUFDTEMsSUFBQUEsV0FBVyxFQUFFQztBQURSLEdBeEdtQjtBQTRHNUI7QUFDQUMsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQztBQUNBLFNBQUtDLDZCQUFMLEdBQXFDLElBQXJDOztBQUNBLFNBQUtDLFlBQUwsQ0FBa0IsS0FBS0MsS0FBTCxDQUFXeEMsT0FBN0I7O0FBRUEsU0FBS3lDLEtBQUwsR0FBYSx1QkFBYjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsdUJBQXBCO0FBQ0gsR0FwSDJCO0FBc0g1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLTCw2QkFBTCxHQUFxQyxLQUFyQztBQUNBLFVBQU0zRCxNQUFNLEdBQUcsS0FBS2lFLE9BQXBCO0FBQ0FqRSxJQUFBQSxNQUFNLENBQUNrRSxFQUFQLENBQVUsMkJBQVYsRUFBdUMsS0FBS0MsMkJBQTVDO0FBQ0FuRSxJQUFBQSxNQUFNLENBQUNrRSxFQUFQLENBQVUsd0JBQVYsRUFBb0MsS0FBS0UseUJBQXpDO0FBQ0EsU0FBS1AsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQjZDLEVBQW5CLENBQXNCLGlCQUF0QixFQUF5QyxLQUFLRyxZQUE5Qzs7QUFDQSxRQUFJLEtBQUtSLEtBQUwsQ0FBV2YsYUFBZixFQUE4QjtBQUMxQixXQUFLZSxLQUFMLENBQVd4QyxPQUFYLENBQW1CNkMsRUFBbkIsQ0FBc0Isd0JBQXRCLEVBQWdELEtBQUtJLG1CQUFyRDtBQUNIO0FBQ0osR0EvSDJCO0FBaUk1QjtBQUNBQyxFQUFBQSxnQ0FBZ0MsRUFBRSxVQUFTQyxTQUFULEVBQW9CO0FBQ2xEO0FBQ0E7QUFDQSxRQUFJQSxTQUFTLENBQUM5QixlQUFWLEtBQThCLEtBQUttQixLQUFMLENBQVduQixlQUE3QyxFQUE4RDtBQUMxRCxXQUFLa0IsWUFBTCxDQUFrQlksU0FBUyxDQUFDbkQsT0FBNUI7QUFDSDtBQUNKLEdBeEkyQjtBQTBJNUJvRCxFQUFBQSxxQkFBcUIsRUFBRSxVQUFTRCxTQUFULEVBQW9CRSxTQUFwQixFQUErQjtBQUNsRCxRQUFJLENBQUNDLFdBQVcsQ0FBQ0MsWUFBWixDQUF5QixLQUFLQyxLQUE5QixFQUFxQ0gsU0FBckMsQ0FBTCxFQUFzRDtBQUNsRCxhQUFPLElBQVA7QUFDSDs7QUFFRCxXQUFPLENBQUMsS0FBS0ksV0FBTCxDQUFpQixLQUFLakIsS0FBdEIsRUFBNkJXLFNBQTdCLENBQVI7QUFDSCxHQWhKMkI7QUFrSjVCTyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFVBQU0vRSxNQUFNLEdBQUcsS0FBS2lFLE9BQXBCO0FBQ0FqRSxJQUFBQSxNQUFNLENBQUNnRixjQUFQLENBQXNCLDJCQUF0QixFQUFtRCxLQUFLYiwyQkFBeEQ7QUFDQW5FLElBQUFBLE1BQU0sQ0FBQ2dGLGNBQVAsQ0FBc0Isd0JBQXRCLEVBQWdELEtBQUtaLHlCQUFyRDtBQUNBLFNBQUtQLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUIyRCxjQUFuQixDQUFrQyxpQkFBbEMsRUFBcUQsS0FBS1gsWUFBMUQ7O0FBQ0EsUUFBSSxLQUFLUixLQUFMLENBQVdmLGFBQWYsRUFBOEI7QUFDMUIsV0FBS2UsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQjJELGNBQW5CLENBQWtDLHdCQUFsQyxFQUE0RCxLQUFLVixtQkFBakU7QUFDSDtBQUNKLEdBMUoyQjs7QUE0SjVCOztBQUVBRCxFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQjtBQUNBO0FBQ0E7QUFDQSxTQUFLVCxZQUFMLENBQWtCLEtBQUtDLEtBQUwsQ0FBV3hDLE9BQTdCOztBQUNBLFNBQUs0RCxXQUFMO0FBQ0gsR0FwSzJCO0FBc0s1QmQsRUFBQUEsMkJBQTJCLEVBQUUsVUFBU2UsTUFBVCxFQUFpQkMsTUFBakIsRUFBeUI7QUFDbEQsUUFBSUQsTUFBTSxLQUFLLEtBQUtyQixLQUFMLENBQVd4QyxPQUFYLENBQW1CaEIsU0FBbkIsRUFBZixFQUErQztBQUMzQyxXQUFLdUQsWUFBTCxDQUFrQixLQUFLQyxLQUFMLENBQVd4QyxPQUE3QjtBQUNIO0FBQ0osR0ExSzJCO0FBNEs1QitDLEVBQUFBLHlCQUF5QixFQUFFLFVBQVNjLE1BQVQsRUFBaUJFLFlBQWpCLEVBQStCO0FBQ3RELFFBQUlGLE1BQU0sS0FBSyxLQUFLckIsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQmhCLFNBQW5CLEVBQWYsRUFBK0M7QUFDM0MsV0FBS3VELFlBQUwsQ0FBa0IsS0FBS0MsS0FBTCxDQUFXeEMsT0FBN0I7QUFDSDtBQUNKLEdBaEwyQjtBQWtMNUJ1QyxFQUFBQSxZQUFZLEVBQUUsZ0JBQWV2QyxPQUFmLEVBQXdCO0FBQ2xDLFFBQUksQ0FBQ0EsT0FBTyxDQUFDZ0UsV0FBUixFQUFMLEVBQTRCO0FBQ3hCO0FBQ0gsS0FIaUMsQ0FLbEM7OztBQUNBLFVBQU1sQyxRQUFRLEdBQUcsTUFBTSxLQUFLYyxPQUFMLENBQWFxQixxQkFBYixDQUFtQ2pFLE9BQW5DLENBQXZCOztBQUNBLFFBQUk4QixRQUFKLEVBQWM7QUFDVixXQUFLb0MsUUFBTCxDQUFjO0FBQ1ZwQyxRQUFBQSxRQUFRLEVBQUVxQyxtQkFBVUM7QUFEVixPQUFkLEVBRUcsTUFBTTtBQUNMO0FBQ0EsYUFBSzVCLEtBQUwsQ0FBV3pCLGVBQVg7QUFDSCxPQUxEO0FBTUE7QUFDSCxLQWZpQyxDQWlCbEM7QUFDQTs7O0FBQ0EsUUFBSSxDQUFDNUIsdUJBQWNrRixnQkFBZCxDQUErQix1QkFBL0IsQ0FBTCxFQUE4RDtBQUMxRCxXQUFLSCxRQUFMLENBQWM7QUFDVnBDLFFBQUFBLFFBQVEsRUFBRXFDLG1CQUFVRztBQURWLE9BQWQsRUFFRyxLQUFLOUIsS0FBTCxDQUFXekIsZUFGZDtBQUdBO0FBQ0g7O0FBRUQsUUFBSSxDQUFDLEtBQUs2QixPQUFMLENBQWEyQixjQUFiLENBQTRCdkUsT0FBTyxDQUFDaEIsU0FBUixFQUE1QixFQUFpRHdGLHNCQUFqRCxFQUFMLEVBQWdGO0FBQzVFLFdBQUtOLFFBQUwsQ0FBYztBQUNWcEMsUUFBQUEsUUFBUSxFQUFFcUMsbUJBQVVNO0FBRFYsT0FBZCxFQUVHLEtBQUtqQyxLQUFMLENBQVd6QixlQUZkO0FBR0E7QUFDSDs7QUFFRCxVQUFNMkQsZ0JBQWdCLEdBQUcsTUFBTSxLQUFLOUIsT0FBTCxDQUFhK0IscUJBQWIsQ0FBbUMzRSxPQUFuQyxDQUEvQjs7QUFDQSxRQUFJLENBQUMwRSxnQkFBTCxFQUF1QjtBQUNuQixXQUFLUixRQUFMLENBQWM7QUFDVnBDLFFBQUFBLFFBQVEsRUFBRXFDLG1CQUFVUztBQURWLE9BQWQsRUFFRyxLQUFLcEMsS0FBTCxDQUFXekIsZUFGZCxFQURtQixDQUdhOztBQUNoQztBQUNIOztBQUVELFNBQUttRCxRQUFMLENBQWM7QUFDVnBDLE1BQUFBLFFBQVEsRUFBRTRDLGdCQUFnQixDQUFDRyxVQUFqQixLQUFnQ1YsbUJBQVVDLFFBQTFDLEdBQXFERCxtQkFBVUc7QUFEL0QsS0FBZCxFQUVHLEtBQUs5QixLQUFMLENBQVd6QixlQUZkLEVBekNrQyxDQTJDRjtBQUNuQyxHQTlOMkI7QUFnTzVCMEMsRUFBQUEsV0FBVyxFQUFFLFVBQVNxQixJQUFULEVBQWVDLElBQWYsRUFBcUI7QUFDOUIsVUFBTUMsS0FBSyxHQUFHQyxNQUFNLENBQUNDLElBQVAsQ0FBWUosSUFBWixDQUFkO0FBQ0EsVUFBTUssS0FBSyxHQUFHRixNQUFNLENBQUNDLElBQVAsQ0FBWUgsSUFBWixDQUFkOztBQUVBLFFBQUlDLEtBQUssQ0FBQ0ksTUFBTixLQUFpQkQsS0FBSyxDQUFDQyxNQUEzQixFQUFtQztBQUMvQixhQUFPLEtBQVA7QUFDSDs7QUFFRCxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdMLEtBQUssQ0FBQ0ksTUFBMUIsRUFBa0NDLENBQUMsRUFBbkMsRUFBdUM7QUFDbkMsWUFBTUMsR0FBRyxHQUFHTixLQUFLLENBQUNLLENBQUQsQ0FBakI7O0FBRUEsVUFBSSxDQUFDTixJQUFJLENBQUNRLGNBQUwsQ0FBb0JELEdBQXBCLENBQUwsRUFBK0I7QUFDM0IsZUFBTyxLQUFQO0FBQ0gsT0FMa0MsQ0FPbkM7OztBQUNBLFVBQUlBLEdBQUcsS0FBSyxjQUFaLEVBQTRCO0FBQ3hCLGNBQU1FLEVBQUUsR0FBR1YsSUFBSSxDQUFDUSxHQUFELENBQWY7QUFDQSxjQUFNRyxFQUFFLEdBQUdWLElBQUksQ0FBQ08sR0FBRCxDQUFmOztBQUNBLFlBQUlFLEVBQUUsS0FBS0MsRUFBWCxFQUFlO0FBQ1g7QUFDSDs7QUFFRCxZQUFJLENBQUNELEVBQUQsSUFBTyxDQUFDQyxFQUFaLEVBQWdCO0FBQ1osaUJBQU8sS0FBUDtBQUNIOztBQUVELFlBQUlELEVBQUUsQ0FBQ0osTUFBSCxLQUFjSyxFQUFFLENBQUNMLE1BQXJCLEVBQTZCO0FBQ3pCLGlCQUFPLEtBQVA7QUFDSDs7QUFDRCxhQUFLLElBQUlNLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdGLEVBQUUsQ0FBQ0osTUFBdkIsRUFBK0JNLENBQUMsRUFBaEMsRUFBb0M7QUFDaEMsY0FBSUYsRUFBRSxDQUFDRSxDQUFELENBQUYsQ0FBTTdCLE1BQU4sS0FBaUI0QixFQUFFLENBQUNDLENBQUQsQ0FBRixDQUFNN0IsTUFBM0IsRUFBbUM7QUFDL0IsbUJBQU8sS0FBUDtBQUNILFdBSCtCLENBSWhDOzs7QUFDQSxjQUFJMkIsRUFBRSxDQUFDRSxDQUFELENBQUYsQ0FBTUMsVUFBTixLQUFxQkYsRUFBRSxDQUFDQyxDQUFELENBQUYsQ0FBTUMsVUFBL0IsRUFBMkM7QUFDdkMsbUJBQU8sS0FBUDtBQUNIO0FBQ0o7QUFDSixPQXZCRCxNQXVCTztBQUNILFlBQUliLElBQUksQ0FBQ1EsR0FBRCxDQUFKLEtBQWNQLElBQUksQ0FBQ08sR0FBRCxDQUF0QixFQUE2QjtBQUN6QixpQkFBTyxLQUFQO0FBQ0g7QUFDSjtBQUNKOztBQUNELFdBQU8sSUFBUDtBQUNILEdBOVEyQjtBQWdSNUJNLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFVBQU1DLE9BQU8sR0FBRyxLQUFLakQsT0FBTCxDQUFha0Qsc0JBQWIsQ0FBb0MsS0FBS3RELEtBQUwsQ0FBV3hDLE9BQS9DLENBQWhCOztBQUNBLFFBQUksQ0FBQzZGLE9BQUQsSUFBWSxDQUFDQSxPQUFPLENBQUNFLE1BQXpCLEVBQWlDO0FBQUUsYUFBTyxLQUFQO0FBQWUsS0FGMUIsQ0FJeEI7OztBQUNBLFFBQUksS0FBS3ZELEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUJoQixTQUFuQixPQUFtQyxLQUFLNEQsT0FBTCxDQUFhb0QsV0FBYixDQUF5Qm5DLE1BQWhFLEVBQXdFO0FBQ3BFLGFBQU8sS0FBUDtBQUNIOztBQUVELFdBQU9nQyxPQUFPLENBQUNFLE1BQVIsQ0FBZUUsU0FBdEI7QUFDSCxHQTFSMkI7QUE0UjVCQyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFNBQUtoQyxRQUFMLENBQWM7QUFDVnJDLE1BQUFBLGNBQWMsRUFBRSxDQUFDLEtBQUsyQixLQUFMLENBQVczQjtBQURsQixLQUFkO0FBR0gsR0FoUzJCO0FBa1M1QnNFLEVBQUFBLGNBQWMsRUFBRSxZQUFXO0FBQ3ZCO0FBQ0EsUUFBSSxDQUFDLEtBQUszRCxLQUFMLENBQVd2QixZQUFaLElBQTRCLEtBQUt1QixLQUFMLENBQVd2QixZQUFYLENBQXdCbUUsTUFBeEIsS0FBbUMsQ0FBbkUsRUFBc0U7QUFDbEUsYUFBUTtBQUFNLFFBQUEsU0FBUyxFQUFDO0FBQWhCLFFBQVI7QUFDSDs7QUFFRCxVQUFNZ0IsaUJBQWlCLEdBQUc5RyxHQUFHLENBQUNDLFlBQUosQ0FBaUIseUJBQWpCLENBQTFCO0FBQ0EsVUFBTThHLE9BQU8sR0FBRyxFQUFoQjtBQUNBLFVBQU1DLGFBQWEsR0FBRyxFQUF0QjtBQUNBLFFBQUlDLElBQUksR0FBRyxDQUFYO0FBRUEsVUFBTUMsUUFBUSxHQUFHLEtBQUtoRSxLQUFMLENBQVd2QixZQUFYLElBQTJCLEVBQTVDOztBQUNBLFNBQUssSUFBSW9FLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdtQixRQUFRLENBQUNwQixNQUE3QixFQUFxQyxFQUFFQyxDQUF2QyxFQUEwQztBQUN0QyxZQUFNb0IsT0FBTyxHQUFHRCxRQUFRLENBQUNuQixDQUFELENBQXhCO0FBRUEsVUFBSXFCLE1BQU0sR0FBRyxJQUFiOztBQUNBLFVBQUtyQixDQUFDLEdBQUd4RixnQkFBTCxJQUEwQixLQUFLMkQsS0FBTCxDQUFXM0IsY0FBekMsRUFBeUQ7QUFDckQ2RSxRQUFBQSxNQUFNLEdBQUcsS0FBVDtBQUNILE9BTnFDLENBT3RDO0FBQ0E7QUFFQTtBQUNBOzs7QUFDQUgsTUFBQUEsSUFBSSxHQUFHLENBQUNHLE1BQU0sR0FBRzdHLGdCQUFnQixHQUFHLENBQXRCLEdBQTBCd0YsQ0FBakMsSUFBc0MsQ0FBQ2lCLGFBQTlDO0FBRUEsWUFBTXpDLE1BQU0sR0FBRzRDLE9BQU8sQ0FBQzVDLE1BQXZCO0FBQ0EsVUFBSThDLGVBQUo7O0FBRUEsVUFBSSxLQUFLbkUsS0FBTCxDQUFXckIsY0FBZixFQUErQjtBQUMzQndGLFFBQUFBLGVBQWUsR0FBRyxLQUFLbkUsS0FBTCxDQUFXckIsY0FBWCxDQUEwQjBDLE1BQTFCLENBQWxCOztBQUNBLFlBQUksQ0FBQzhDLGVBQUwsRUFBc0I7QUFDbEJBLFVBQUFBLGVBQWUsR0FBRyxFQUFsQjtBQUNBLGVBQUtuRSxLQUFMLENBQVdyQixjQUFYLENBQTBCMEMsTUFBMUIsSUFBb0M4QyxlQUFwQztBQUNIO0FBQ0osT0F2QnFDLENBeUJ0Qzs7O0FBQ0FOLE1BQUFBLE9BQU8sQ0FBQ08sT0FBUixDQUNJLDZCQUFDLGlCQUFEO0FBQW1CLFFBQUEsR0FBRyxFQUFFL0MsTUFBeEI7QUFBZ0MsUUFBQSxNQUFNLEVBQUU0QyxPQUFPLENBQUNkLFVBQWhEO0FBQ0ksUUFBQSxjQUFjLEVBQUU5QixNQURwQjtBQUVJLFFBQUEsVUFBVSxFQUFFMEMsSUFGaEI7QUFFc0IsUUFBQSxNQUFNLEVBQUVHLE1BRjlCO0FBR0ksUUFBQSxlQUFlLEVBQUVDLGVBSHJCO0FBSUksUUFBQSxlQUFlLEVBQUUsS0FBS25FLEtBQUwsQ0FBV3BCLGVBSmhDO0FBS0ksUUFBQSxpQkFBaUIsRUFBRSxLQUFLa0IsNkJBTDVCO0FBTUksUUFBQSxPQUFPLEVBQUUsS0FBSzRELG9CQU5sQjtBQU9JLFFBQUEsU0FBUyxFQUFFTyxPQUFPLENBQUNJLEVBUHZCO0FBUUksUUFBQSxjQUFjLEVBQUUsS0FBS3JFLEtBQUwsQ0FBV2pCO0FBUi9CLFFBREo7QUFZSDs7QUFDRCxRQUFJdUYsT0FBSjs7QUFDQSxRQUFJLENBQUMsS0FBS3RELEtBQUwsQ0FBVzNCLGNBQWhCLEVBQWdDO0FBQzVCLFlBQU1rRixTQUFTLEdBQUdQLFFBQVEsQ0FBQ3BCLE1BQVQsR0FBa0J2RixnQkFBcEM7O0FBQ0EsVUFBSWtILFNBQVMsR0FBRyxDQUFoQixFQUFtQjtBQUNmRCxRQUFBQSxPQUFPLEdBQUc7QUFBTSxVQUFBLFNBQVMsRUFBQyxrQ0FBaEI7QUFDTixVQUFBLE9BQU8sRUFBRSxLQUFLWixvQkFEUjtBQUVOLFVBQUEsS0FBSyxFQUFFO0FBQUVjLFlBQUFBLEtBQUssRUFBRSxFQUFFVCxJQUFJLEdBQUdELGFBQVQ7QUFBVDtBQUZELFdBRXVDUyxTQUZ2QyxNQUFWO0FBSUg7QUFDSjs7QUFFRCxXQUFPO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDREQsT0FEQyxFQUVEVCxPQUZDLENBQVA7QUFJSCxHQXBXMkI7QUFzVzVCWSxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTQyxLQUFULEVBQWdCO0FBQ2xDLFVBQU1sSCxPQUFPLEdBQUcsS0FBS3dDLEtBQUwsQ0FBV3hDLE9BQTNCOztBQUNBbUgsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsZ0JBREM7QUFFVEMsTUFBQUEsT0FBTyxFQUFFdEgsT0FBTyxDQUFDaEIsU0FBUjtBQUZBLEtBQWI7QUFJSCxHQTVXMkI7QUE4VzVCdUksRUFBQUEsa0JBQWtCLEVBQUUsWUFBVztBQUMzQixTQUFLckQsUUFBTCxDQUFjO0FBQ1Y7QUFDQTtBQUNBbkMsTUFBQUEsdUJBQXVCLEVBQUU7QUFIZixLQUFkLEVBRDJCLENBTzNCO0FBQ0E7QUFDQTs7QUFDQSxTQUFLYSxPQUFMLENBQWE0RSxrQ0FBYixDQUFnRCxLQUFLaEYsS0FBTCxDQUFXeEMsT0FBM0Q7QUFDSCxHQXpYMkI7QUEyWDVCeUgsRUFBQUEsa0JBQWtCLEVBQUUsVUFBU0MsQ0FBVCxFQUFZO0FBQzVCO0FBQ0E7QUFDQUEsSUFBQUEsQ0FBQyxDQUFDQyxjQUFGOztBQUNBUix3QkFBSUMsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxXQURDO0FBRVRPLE1BQUFBLFFBQVEsRUFBRSxLQUFLcEYsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQjZILEtBQW5CLEVBRkQ7QUFHVEMsTUFBQUEsV0FBVyxFQUFFLElBSEo7QUFJVEMsTUFBQUEsT0FBTyxFQUFFLEtBQUt2RixLQUFMLENBQVd4QyxPQUFYLENBQW1CZ0ksU0FBbkI7QUFKQSxLQUFiO0FBTUgsR0FyWTJCO0FBdVk1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixVQUFNNUosRUFBRSxHQUFHLEtBQUttRSxLQUFMLENBQVd4QyxPQUF0QixDQUQwQixDQUcxQjs7QUFDQSxRQUFJM0IsRUFBRSxDQUFDSSxVQUFILEdBQWdCQyxPQUFoQixLQUE0QixpQkFBaEMsRUFBbUQ7QUFDL0MsYUFBTyw2QkFBQyx1QkFBRCxPQUFQO0FBQ0gsS0FOeUIsQ0FRMUI7OztBQUNBLFFBQUlMLEVBQUUsQ0FBQzJGLFdBQUgsRUFBSixFQUFzQjtBQUNsQixVQUFJLEtBQUtSLEtBQUwsQ0FBVzFCLFFBQVgsS0FBd0JxQyxtQkFBVU0sTUFBdEMsRUFBOEM7QUFDMUMsZUFEMEMsQ0FDbEM7QUFDWCxPQUZELE1BRU8sSUFBSSxLQUFLakIsS0FBTCxDQUFXMUIsUUFBWCxLQUF3QnFDLG1CQUFVQyxRQUF0QyxFQUFnRDtBQUNuRCxlQURtRCxDQUMzQztBQUNYLE9BRk0sTUFFQSxJQUFJLEtBQUtaLEtBQUwsQ0FBVzFCLFFBQVgsS0FBd0JxQyxtQkFBVVMsT0FBdEMsRUFBK0M7QUFDbEQsZUFBUSw2QkFBQyxpQkFBRCxPQUFSO0FBQ0gsT0FGTSxNQUVBO0FBQ0gsZUFBUSw2QkFBQyxvQkFBRCxPQUFSO0FBQ0g7QUFDSjs7QUFFRCxRQUFJLEtBQUtoQyxPQUFMLENBQWFzRixlQUFiLENBQTZCN0osRUFBRSxDQUFDMkosU0FBSCxFQUE3QixDQUFKLEVBQWtEO0FBQzlDO0FBQ0E7QUFDQSxVQUFJM0osRUFBRSxDQUFDOEosTUFBSCxLQUFjQyx5QkFBWUMsVUFBOUIsRUFBMEM7QUFDdEM7QUFDSDs7QUFDRCxVQUFJaEssRUFBRSxDQUFDOEosTUFBSCxLQUFjQyx5QkFBWUUsUUFBOUIsRUFBd0M7QUFDcEM7QUFDSDs7QUFDRCxVQUFJakssRUFBRSxDQUFDdUIsT0FBSCxFQUFKLEVBQWtCO0FBQ2QsZUFEYyxDQUNOO0FBQ1gsT0FYNkMsQ0FZOUM7OztBQUNBLGFBQU8sNkJBQUMscUJBQUQsT0FBUDtBQUNILEtBbkN5QixDQXFDMUI7OztBQUNBLFdBQU8sSUFBUDtBQUNILEdBOWEyQjs7QUFnYjVCMkksRUFBQUEsc0JBQXNCLENBQUNDLE9BQUQsRUFBVTtBQUM1QixTQUFLdEUsUUFBTCxDQUFjO0FBQ1Z0QyxNQUFBQSxnQkFBZ0IsRUFBRTRHO0FBRFIsS0FBZDtBQUdILEdBcGIyQjs7QUFzYjVCQyxFQUFBQSxPQUFPLEdBQUc7QUFDTixXQUFPLEtBQUtoRyxLQUFMLENBQVdpRyxPQUFsQjtBQUNILEdBeGIyQjs7QUEwYjVCQyxFQUFBQSxjQUFjLEdBQUc7QUFDYixXQUFPLEtBQUtqRyxZQUFMLENBQWtCZ0csT0FBekI7QUFDSCxHQTViMkI7O0FBOGI1QnpHLEVBQUFBLFlBQVksR0FBRztBQUNYLFFBQ0ksQ0FBQyxLQUFLTyxLQUFMLENBQVdmLGFBQVosSUFDQSxDQUFDLEtBQUtlLEtBQUwsQ0FBV2hCLG9CQUZoQixFQUdFO0FBQ0UsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsVUFBTW9ILE9BQU8sR0FBRyxLQUFLcEcsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQjZILEtBQW5CLEVBQWhCOztBQUNBLFFBQUksQ0FBQ2UsT0FBTCxFQUFjO0FBQ1Y7QUFDQUMsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsaUVBQWQsRUFGVSxDQUdWOztBQUNBRCxNQUFBQSxPQUFPLENBQUNFLEdBQVIsQ0FBWUMsSUFBSSxDQUFDQyxTQUFMLENBQWUsS0FBS3pHLEtBQUwsQ0FBV3hDLE9BQTFCLEVBQW1DLElBQW5DLEVBQXlDLENBQXpDLENBQVo7QUFDQTZJLE1BQUFBLE9BQU8sQ0FBQ0ssS0FBUixDQUFjLG1FQUFkO0FBQ0g7O0FBQ0QsV0FBTyxLQUFLMUcsS0FBTCxDQUFXaEIsb0JBQVgsQ0FBZ0NvSCxPQUFoQyxFQUF5QyxjQUF6QyxFQUF5RCxZQUF6RCxDQUFQO0FBQ0gsR0E5YzJCOztBQWdkNUIzRixFQUFBQSxtQkFBbUIsQ0FBQ2tHLFlBQUQsRUFBZUMsU0FBZixFQUEwQjtBQUN6QyxRQUFJRCxZQUFZLEtBQUssY0FBakIsSUFBbUNDLFNBQVMsS0FBSyxZQUFyRCxFQUFtRTtBQUMvRDtBQUNIOztBQUNELFNBQUs1RyxLQUFMLENBQVd4QyxPQUFYLENBQW1CMkQsY0FBbkIsQ0FBa0Msd0JBQWxDLEVBQTRELEtBQUtWLG1CQUFqRTtBQUNBLFNBQUtpQixRQUFMLENBQWM7QUFDVmxDLE1BQUFBLFNBQVMsRUFBRSxLQUFLQyxZQUFMO0FBREQsS0FBZDtBQUdILEdBeGQyQjs7QUEwZDVCb0gsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxnQkFBZ0IsR0FBR2hLLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxVQUFNZ0ssYUFBYSxHQUFHakssR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF0QjtBQUNBLFVBQU1pSyxZQUFZLEdBQUdsSyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsc0JBQWpCLENBQXJCLENBSGUsQ0FLZjs7QUFFQSxVQUFNZixPQUFPLEdBQUcsS0FBS2dFLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUJ2QixVQUFuQixFQUFoQjtBQUNBLFVBQU1DLE9BQU8sR0FBR0YsT0FBTyxDQUFDRSxPQUF4QjtBQUNBLFVBQU0wSyxTQUFTLEdBQUcsS0FBSzVHLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUJ6QixPQUFuQixFQUFsQixDQVRlLENBV2Y7O0FBQ0EsVUFBTWtMLGVBQWUsR0FBR0wsU0FBUyxDQUFDTSxVQUFWLENBQXFCLG9CQUFyQixLQUNuQk4sU0FBUyxLQUFLLGdCQUFkLElBQWtDMUssT0FBbEMsSUFBNkNBLE9BQU8sQ0FBQ2dMLFVBQVIsQ0FBbUIsb0JBQW5CLENBRDFCLElBRW5CTixTQUFTLEtBQUssbUJBRm5CO0FBR0EsUUFBSU8sYUFBYSxHQUNiLENBQUNGLGVBQUQsSUFBb0JMLFNBQVMsS0FBSyxnQkFBbEMsSUFDQUEsU0FBUyxLQUFLLFdBRGQsSUFDNkJBLFNBQVMsS0FBSyxlQUYvQztBQUtBLFFBQUlRLFdBQVcsR0FBR3hMLGNBQWMsQ0FBQyxLQUFLb0UsS0FBTCxDQUFXeEMsT0FBWixDQUFoQyxDQXBCZSxDQXFCZjtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxVQUFNNkosU0FBUyxHQUFHLENBQUNELFdBQUQsSUFBZ0IsS0FBS3BILEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUI4SixVQUFuQixDQUE4QixXQUE5QixDQUFsQzs7QUFDQSxRQUFJRCxTQUFTLElBQUkxSyx1QkFBY0MsUUFBZCxDQUF1Qiw0QkFBdkIsQ0FBakIsRUFBdUU7QUFDbkV3SyxNQUFBQSxXQUFXLEdBQUcsMEJBQWQsQ0FEbUUsQ0FFbkU7O0FBQ0FELE1BQUFBLGFBQWEsR0FBRyxJQUFoQjtBQUNILEtBOUJjLENBK0JmO0FBQ0E7OztBQUNBLFFBQUksQ0FBQ0MsV0FBTCxFQUFrQjtBQUNkLFlBQU07QUFBQzVKLFFBQUFBO0FBQUQsVUFBWSxLQUFLd0MsS0FBdkI7QUFDQXFHLE1BQUFBLE9BQU8sQ0FBQ2tCLElBQVIsMENBQStDL0osT0FBTyxDQUFDekIsT0FBUixFQUEvQyxzQkFBNEV5QixPQUFPLENBQUNKLE9BQVIsRUFBNUU7QUFDQSxhQUFPO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNIO0FBQUssUUFBQSxTQUFTLEVBQUM7QUFBZixTQUNNLHlCQUFHLG1DQUFILENBRE4sQ0FERyxDQUFQO0FBS0g7O0FBQ0QsVUFBTW9LLGFBQWEsR0FBRzFLLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQnFLLFdBQWpCLENBQXRCO0FBRUEsVUFBTUssU0FBUyxHQUFJLENBQUMsU0FBRCxFQUFZLFFBQVosRUFBc0IsWUFBdEIsRUFBb0NDLE9BQXBDLENBQTRDLEtBQUsxSCxLQUFMLENBQVduQixlQUF2RCxNQUE0RSxDQUFDLENBQWhHO0FBQ0EsVUFBTWpCLFVBQVUsR0FBRytKLGNBQWMsQ0FBQyxLQUFLM0gsS0FBTCxDQUFXeEMsT0FBWixDQUFkLElBQXNDLEtBQUt3QyxLQUFMLENBQVdwQyxVQUFwRTtBQUNBLFVBQU1nSyxtQkFBbUIsR0FBRyxLQUFLNUgsS0FBTCxDQUFXeEMsT0FBWCxDQUFtQnFLLG1CQUFuQixFQUE1QjtBQUVBLFVBQU1DLFNBQVMsR0FBRyxDQUFDLENBQUMsS0FBSzlILEtBQUwsQ0FBVytILFNBQS9CO0FBQ0EsVUFBTUMsT0FBTyxHQUFHLHlCQUFXO0FBQ3ZCQyxNQUFBQSw0QkFBNEIsRUFBRWhCLGVBRFA7QUFFdkJpQixNQUFBQSxZQUFZLEVBQUUsSUFGUztBQUd2QkMsTUFBQUEsc0JBQXNCLEVBQUVMLFNBSEQ7QUFJdkJNLE1BQUFBLGlCQUFpQixFQUFFakIsYUFKSTtBQUt2QmtCLE1BQUFBLGlCQUFpQixFQUFFLEtBQUtySSxLQUFMLENBQVdqQixZQUxQO0FBTXZCdUosTUFBQUEsdUJBQXVCLEVBQUUsS0FBS3RJLEtBQUwsQ0FBV25CLGVBQVgsS0FBK0IsWUFOakM7QUFPdkIwSixNQUFBQSxvQkFBb0IsRUFBRSxDQUFDVCxTQUFELElBQWNMLFNBUGI7QUFRdkJlLE1BQUFBLG9CQUFvQixFQUFFLEtBQUt4SSxLQUFMLENBQVduQixlQUFYLEtBQStCLFVBUjlCO0FBU3ZCNEosTUFBQUEsc0JBQXNCLEVBQUUsS0FBS3pJLEtBQUwsQ0FBV2xCLFNBQVgsS0FBeUIsT0FBekIsR0FBbUMsS0FBbkMsR0FBMkMsS0FBS3NFLGVBQUwsRUFUNUM7QUFVdkJzRixNQUFBQSxxQkFBcUIsRUFBRSxLQUFLMUksS0FBTCxDQUFXMUIsZUFWWDtBQVd2QnFLLE1BQUFBLHlCQUF5QixFQUFFLEtBQUszSSxLQUFMLENBQVdsQixTQUFYLEdBQXVCLEVBQXZCLEdBQTRCLEtBQUtrQixLQUFMLENBQVdsQyxZQVgzQztBQVl2QjhLLE1BQUFBLGlCQUFpQixFQUFFLEtBQUs1SSxLQUFMLENBQVdqQyxJQVpQO0FBYXZCOEssTUFBQUEsdUJBQXVCLEVBQUUsS0FBSzdJLEtBQUwsQ0FBV2hDLFVBYmI7QUFjdkI4SyxNQUFBQSw2QkFBNkIsRUFBRSxLQUFLOUgsS0FBTCxDQUFXNUIsZ0JBZG5CO0FBZXZCMkosTUFBQUEscUJBQXFCLEVBQUUsQ0FBQzlCLGVBQUQsSUFBb0IsS0FBS2pHLEtBQUwsQ0FBVzFCLFFBQVgsS0FBd0JxQyxtQkFBVUMsUUFmdEQ7QUFnQnZCb0gsTUFBQUEsdUJBQXVCLEVBQUUsQ0FBQy9CLGVBQUQsSUFBb0IsS0FBS2pHLEtBQUwsQ0FBVzFCLFFBQVgsS0FBd0JxQyxtQkFBVUcsT0FoQnhEO0FBaUJ2Qm1ILE1BQUFBLG9CQUFvQixFQUFFLENBQUNoQyxlQUFELElBQW9CLEtBQUtqRyxLQUFMLENBQVcxQixRQUFYLEtBQXdCcUMsbUJBQVVTLE9BakJyRDtBQWtCdkI4RyxNQUFBQSxnQkFBZ0IsRUFBRXRCLG1CQWxCSztBQW1CdkJ1QixNQUFBQSxrQkFBa0IsRUFBRWpOLE9BQU8sS0FBSyxTQW5CVDtBQW9CdkJrTixNQUFBQSxxQkFBcUIsRUFBRXhMO0FBcEJBLEtBQVgsQ0FBaEI7QUF1QkEsUUFBSXlMLFNBQVMsR0FBRyxHQUFoQjs7QUFDQSxRQUFJLEtBQUtySixLQUFMLENBQVdzSixnQkFBZixFQUFpQztBQUM3QkQsTUFBQUEsU0FBUyxHQUFHLEtBQUtySixLQUFMLENBQVdzSixnQkFBWCxDQUE0QkMsUUFBNUIsQ0FBcUMsS0FBS3ZKLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUI2SCxLQUFuQixFQUFyQyxDQUFaO0FBQ0g7O0FBRUQsVUFBTW1FLFdBQVcsR0FBRyxLQUFLN0YsY0FBTCxFQUFwQjtBQUVBLFFBQUk4RixNQUFKO0FBQ0EsUUFBSUMsTUFBSjtBQUNBLFFBQUlDLFVBQUo7QUFDQSxRQUFJQyxrQkFBSjs7QUFFQSxRQUFJLEtBQUs1SixLQUFMLENBQVdsQixTQUFYLEtBQXlCLE9BQTdCLEVBQXNDO0FBQ2xDNkssTUFBQUEsVUFBVSxHQUFHLEVBQWI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsSUFBckI7QUFDSCxLQUhELE1BR08sSUFBSXhDLFdBQVcsS0FBSyxxQkFBaEIsSUFBeUNILGVBQTdDLEVBQThEO0FBQ2pFMEMsTUFBQUEsVUFBVSxHQUFHLENBQWI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsS0FBckI7QUFDSCxLQUhNLE1BR0EsSUFBSXpDLGFBQUosRUFBbUI7QUFDdEI7QUFDQTtBQUNBd0MsTUFBQUEsVUFBVSxHQUFHLEVBQWI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsS0FBckI7QUFDSCxLQUxNLE1BS0EsSUFBSSxLQUFLNUosS0FBTCxDQUFXbEMsWUFBWCxJQUEyQixLQUFLa0MsS0FBTCxDQUFXbEIsU0FBWCxLQUF5QixXQUF4RCxFQUFxRTtBQUN4RTtBQUNBNkssTUFBQUEsVUFBVSxHQUFHLENBQWI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsS0FBckI7QUFDSCxLQUpNLE1BSUE7QUFDSEQsTUFBQUEsVUFBVSxHQUFHLEVBQWI7QUFDQUMsTUFBQUEsa0JBQWtCLEdBQUcsSUFBckI7QUFDSDs7QUFFRCxRQUFJLEtBQUs1SixLQUFMLENBQVd4QyxPQUFYLENBQW1Ca00sTUFBbkIsSUFBNkJDLFVBQWpDLEVBQTZDO0FBQ3pDRixNQUFBQSxNQUFNLEdBQ0U7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ0ksNkJBQUMsWUFBRDtBQUFjLFFBQUEsTUFBTSxFQUFFLEtBQUt6SixLQUFMLENBQVd4QyxPQUFYLENBQW1Ca00sTUFBekM7QUFDSSxRQUFBLEtBQUssRUFBRUMsVUFEWDtBQUN1QixRQUFBLE1BQU0sRUFBRUEsVUFEL0I7QUFFSSxRQUFBLGVBQWUsRUFBRTtBQUZyQixRQURKLENBRFI7QUFRSDs7QUFFRCxRQUFJQyxrQkFBSixFQUF3QjtBQUNwQixVQUFJQyxJQUFJLEdBQUcsSUFBWDs7QUFDQSxVQUFJLENBQUMsS0FBSzdKLEtBQUwsQ0FBV2xCLFNBQVosSUFBeUIsS0FBS2tCLEtBQUwsQ0FBV2xCLFNBQVgsS0FBeUIsT0FBbEQsSUFBNkQsS0FBS2tCLEtBQUwsQ0FBV2xCLFNBQVgsS0FBeUIsZUFBMUYsRUFBMkc7QUFDdkcsWUFBSTVDLE9BQU8sS0FBSyxTQUFoQixFQUEyQjJOLElBQUksR0FBRywwQkFBSSw4QkFBSixDQUFQLENBQTNCLEtBQ0ssSUFBSTNOLE9BQU8sS0FBSyxTQUFoQixFQUEyQjJOLElBQUksR0FBRywwQkFBSSw2QkFBSixDQUFQLENBQTNCLEtBQ0EsSUFBSTNOLE9BQU8sS0FBSyxRQUFoQixFQUEwQjJOLElBQUksR0FBRywwQkFBSSxnQ0FBSixDQUFQO0FBQy9CSCxRQUFBQSxNQUFNLEdBQUcsNkJBQUMsYUFBRDtBQUFlLFVBQUEsT0FBTyxFQUFFLEtBQUtqRixvQkFBN0I7QUFDZSxVQUFBLE9BQU8sRUFBRSxLQUFLekUsS0FBTCxDQUFXeEMsT0FEbkM7QUFFZSxVQUFBLFdBQVcsRUFBRSxDQUFDcU0sSUFGN0I7QUFHZSxVQUFBLElBQUksRUFBRUE7QUFIckIsVUFBVDtBQUlILE9BUkQsTUFRTztBQUNISCxRQUFBQSxNQUFNLEdBQUcsNkJBQUMsYUFBRDtBQUFlLFVBQUEsT0FBTyxFQUFFLEtBQUsxSixLQUFMLENBQVd4QyxPQUFuQztBQUE0QyxVQUFBLFdBQVcsRUFBRTtBQUF6RCxVQUFUO0FBQ0g7QUFDSjs7QUFFRCxVQUFNc00sZ0JBQWdCLEdBQUdoTixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTWdOLFNBQVMsR0FBRyxDQUFDakMsU0FBRCxHQUFhLDZCQUFDLGdCQUFEO0FBQzNCLE1BQUEsT0FBTyxFQUFFLEtBQUs5SCxLQUFMLENBQVd4QyxPQURPO0FBRTNCLE1BQUEsU0FBUyxFQUFFLEtBQUt3RCxLQUFMLENBQVd4QixTQUZLO0FBRzNCLE1BQUEsZ0JBQWdCLEVBQUUsS0FBS1EsS0FBTCxDQUFXc0osZ0JBSEY7QUFJM0IsTUFBQSxPQUFPLEVBQUUsS0FBS3JELE9BSmE7QUFLM0IsTUFBQSxjQUFjLEVBQUUsS0FBS0UsY0FMTTtBQU0zQixNQUFBLGFBQWEsRUFBRSxLQUFLSjtBQU5PLE1BQWIsR0FPYnJKLFNBUEw7QUFTQSxVQUFNc04sU0FBUyxHQUFHLEtBQUtoSyxLQUFMLENBQVd4QyxPQUFYLENBQW1CeU0sS0FBbkIsS0FDZCw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLGNBQWMsRUFBRSxLQUFLakssS0FBTCxDQUFXakIsWUFBN0M7QUFBMkQsTUFBQSxFQUFFLEVBQUUsS0FBS2lCLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUJ5TSxLQUFuQjtBQUEvRCxNQURjLEdBQ2tGLElBRHBHOztBQUdBLFVBQU1DLGtCQUFrQixHQUNwQjtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSx3Q0FDTSxLQUFLbEosS0FBTCxDQUFXekIsdUJBQVgsR0FDRSx5QkFBSSw2RUFDQSx5QkFESixDQURGLEdBR0UseUJBQUksOEVBQ0EsNEVBREEsR0FFQSxrREFGSixDQUpSLENBREosRUFVSSx3Q0FDTSx5QkFBSSw4RUFDRSwwQkFETixDQUROLENBVkosQ0FESjs7QUFpQkEsVUFBTTRLLHFCQUFxQixHQUFHLEtBQUtuSixLQUFMLENBQVd6Qix1QkFBWCxHQUMxQix5QkFBRyxtQkFBSCxDQUQwQixHQUUxQix5QkFDSSxpRkFESixFQUVJLEVBRkosRUFHSTtBQUFDLHFCQUFnQjZLLEdBQUQsSUFBUztBQUFHLFFBQUEsT0FBTyxFQUFFLEtBQUtyRjtBQUFqQixTQUF1Q3FGLEdBQXZDO0FBQXpCLEtBSEosQ0FGSjtBQVFBLFVBQU1DLGFBQWEsR0FBR3ZOLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7QUFDQSxVQUFNdU4sY0FBYyxHQUFHMUMsbUJBQW1CLEdBQ3RDO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDTXVDLHFCQUROLENBREosRUFJSSw2QkFBQyxhQUFEO0FBQWUsTUFBQSxRQUFRLEVBQUVEO0FBQXpCLE1BSkosQ0FEc0MsR0FNN0IsSUFOYjtBQVFBLFFBQUlLLFlBQUo7O0FBQ0EsUUFBSSxDQUFDM00sVUFBTCxFQUFpQjtBQUNiLFlBQU00TSxZQUFZLEdBQUcxTixHQUFHLENBQUNDLFlBQUosQ0FBaUIsdUJBQWpCLENBQXJCO0FBQ0F3TixNQUFBQSxZQUFZLEdBQUcsNkJBQUMsWUFBRDtBQUNYLFFBQUEsT0FBTyxFQUFFLEtBQUt2SyxLQUFMLENBQVd4QyxPQURUO0FBRVgsUUFBQSxTQUFTLEVBQUUsS0FBS3dELEtBQUwsQ0FBV3hCO0FBRlgsUUFBZjtBQUlIOztBQUVELFlBQVEsS0FBS1EsS0FBTCxDQUFXbEIsU0FBbkI7QUFDSSxXQUFLLE9BQUw7QUFBYztBQUNWLGdCQUFNMkwsSUFBSSxHQUFHLEtBQUtySyxPQUFMLENBQWFzSyxPQUFiLENBQXFCLEtBQUsxSyxLQUFMLENBQVd4QyxPQUFYLENBQW1CZ0ksU0FBbkIsRUFBckIsQ0FBYjtBQUNBLGlCQUNJO0FBQUssWUFBQSxTQUFTLEVBQUV3QztBQUFoQixhQUNJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNJO0FBQUcsWUFBQSxJQUFJLEVBQUVxQixTQUFUO0FBQW9CLFlBQUEsT0FBTyxFQUFFLEtBQUtwRTtBQUFsQyxhQUNNd0YsSUFBSSxHQUFHQSxJQUFJLENBQUNFLElBQVIsR0FBZSxFQUR6QixDQURKLENBREosRUFNSTtBQUFLLFlBQUEsU0FBUyxFQUFDO0FBQWYsYUFDTWxCLE1BRE4sRUFFSTtBQUFHLFlBQUEsSUFBSSxFQUFFSixTQUFUO0FBQW9CLFlBQUEsT0FBTyxFQUFFLEtBQUtwRTtBQUFsQyxhQUNNeUUsTUFETixFQUVNTSxTQUZOLENBRkosQ0FOSixFQWFJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNJLDZCQUFDLGFBQUQ7QUFBZSxZQUFBLEdBQUcsRUFBRSxLQUFLL0osS0FBekI7QUFDZSxZQUFBLE9BQU8sRUFBRSxLQUFLRCxLQUFMLENBQVd4QyxPQURuQztBQUVlLFlBQUEsVUFBVSxFQUFFLEtBQUt3QyxLQUFMLENBQVcvQixVQUZ0QztBQUdlLFlBQUEsYUFBYSxFQUFFLEtBQUsrQixLQUFMLENBQVc3QixhQUh6QztBQUllLFlBQUEsY0FBYyxFQUFFLEtBQUs2QixLQUFMLENBQVczQixjQUoxQztBQUtlLFlBQUEsZUFBZSxFQUFFLEtBQUsyQixLQUFMLENBQVd6QjtBQUwzQyxZQURKLENBYkosQ0FESjtBQXdCSDs7QUFDRCxXQUFLLFdBQUw7QUFBa0I7QUFDZCxpQkFDSTtBQUFLLFlBQUEsU0FBUyxFQUFFeUo7QUFBaEIsYUFDSTtBQUFLLFlBQUEsU0FBUyxFQUFDO0FBQWYsYUFDSSw2QkFBQyxhQUFEO0FBQWUsWUFBQSxHQUFHLEVBQUUsS0FBSy9ILEtBQXpCO0FBQ2UsWUFBQSxPQUFPLEVBQUUsS0FBS0QsS0FBTCxDQUFXeEMsT0FEbkM7QUFFZSxZQUFBLFVBQVUsRUFBRSxLQUFLd0MsS0FBTCxDQUFXL0IsVUFGdEM7QUFHZSxZQUFBLGFBQWEsRUFBRSxLQUFLK0IsS0FBTCxDQUFXN0IsYUFIekM7QUFJZSxZQUFBLGNBQWMsRUFBRSxLQUFLNkIsS0FBTCxDQUFXM0IsY0FKMUM7QUFLZSxZQUFBLFNBQVMsRUFBRSxLQUFLMkIsS0FBTCxDQUFXbEIsU0FMckM7QUFNZSxZQUFBLGVBQWUsRUFBRSxLQUFLa0IsS0FBTCxDQUFXekI7QUFOM0MsWUFESixDQURKLEVBVUk7QUFDSSxZQUFBLFNBQVMsRUFBQyxnQ0FEZDtBQUVJLFlBQUEsSUFBSSxFQUFFOEssU0FGVjtBQUdJLFlBQUEsT0FBTyxFQUFFLEtBQUtwRTtBQUhsQixhQUtJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNNeUUsTUFETixFQUVNTSxTQUZOLENBTEosQ0FWSixDQURKO0FBdUJIOztBQUVELFdBQUssT0FBTDtBQUNBLFdBQUssZUFBTDtBQUFzQjtBQUNsQixjQUFJWSxNQUFKOztBQUNBLGNBQUksS0FBSzVLLEtBQUwsQ0FBV2xCLFNBQVgsS0FBeUIsZUFBN0IsRUFBOEM7QUFDMUM4TCxZQUFBQSxNQUFNLEdBQUdDLHFCQUFZQyxVQUFaLENBQ0wsS0FBSzlLLEtBQUwsQ0FBV3hDLE9BRE4sRUFFTCxLQUFLd0MsS0FBTCxDQUFXekIsZUFGTixFQUdMLEtBQUt5QixLQUFMLENBQVdzSixnQkFITixFQUlMLEtBQUtwSixZQUpBLENBQVQ7QUFNSDs7QUFDRCxpQkFDSTtBQUFLLFlBQUEsU0FBUyxFQUFFOEg7QUFBaEIsYUFDTXlCLE1BRE4sRUFFTUMsTUFGTixFQUdJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNJO0FBQUcsWUFBQSxJQUFJLEVBQUVMLFNBQVQ7QUFBb0IsWUFBQSxPQUFPLEVBQUUsS0FBS3BFO0FBQWxDLGFBQ00rRSxTQUROLENBREosRUFJTSxDQUFDL0MsZUFBRCxJQUFvQixLQUFLeEIsaUJBQUwsRUFKMUIsRUFLTW1GLE1BTE4sRUFNSSw2QkFBQyxhQUFEO0FBQWUsWUFBQSxHQUFHLEVBQUUsS0FBSzNLLEtBQXpCO0FBQ2UsWUFBQSxPQUFPLEVBQUUsS0FBS0QsS0FBTCxDQUFXeEMsT0FEbkM7QUFFZSxZQUFBLFVBQVUsRUFBRSxLQUFLd0MsS0FBTCxDQUFXL0IsVUFGdEM7QUFHZSxZQUFBLGFBQWEsRUFBRSxLQUFLK0IsS0FBTCxDQUFXN0IsYUFIekM7QUFJZSxZQUFBLGVBQWUsRUFBRSxLQUFLNkIsS0FBTCxDQUFXekIsZUFKM0M7QUFLZSxZQUFBLGNBQWMsRUFBRTtBQUwvQixZQU5KLENBSEosQ0FESjtBQW1CSDs7QUFDRDtBQUFTO0FBQ0wsZ0JBQU1xTSxNQUFNLEdBQUdDLHFCQUFZQyxVQUFaLENBQ1gsS0FBSzlLLEtBQUwsQ0FBV3hDLE9BREEsRUFFWCxLQUFLd0MsS0FBTCxDQUFXekIsZUFGQSxFQUdYLEtBQUt5QixLQUFMLENBQVdzSixnQkFIQSxFQUlYLEtBQUtwSixZQUpNLENBQWYsQ0FESyxDQU9MOzs7QUFDQSxpQkFDSTtBQUFLLFlBQUEsU0FBUyxFQUFFOEgsT0FBaEI7QUFBeUIsWUFBQSxRQUFRLEVBQUUsQ0FBQztBQUFwQyxhQUNJO0FBQUssWUFBQSxTQUFTLEVBQUM7QUFBZixhQUNNd0IsV0FETixDQURKLEVBSU1FLE1BSk4sRUFLSTtBQUFLLFlBQUEsU0FBUyxFQUFDO0FBQWYsYUFDSTtBQUNJLFlBQUEsSUFBSSxFQUFFTCxTQURWO0FBRUksWUFBQSxPQUFPLEVBQUUsS0FBS3BFLGtCQUZsQjtBQUdJLDBCQUFZLDJCQUFXLElBQUk4RixJQUFKLENBQVMsS0FBSy9LLEtBQUwsQ0FBV3hDLE9BQVgsQ0FBbUJ5TSxLQUFuQixFQUFULENBQVgsRUFBaUQsS0FBS2pLLEtBQUwsQ0FBV2pCLFlBQTVEO0FBSGhCLGFBS01pTCxTQUxOLENBREosRUFRTSxDQUFDL0MsZUFBRCxJQUFvQixLQUFLeEIsaUJBQUwsRUFSMUIsRUFTTW1GLE1BVE4sRUFVSSw2QkFBQyxhQUFEO0FBQWUsWUFBQSxHQUFHLEVBQUUsS0FBSzNLLEtBQXpCO0FBQ2UsWUFBQSxPQUFPLEVBQUUsS0FBS0QsS0FBTCxDQUFXeEMsT0FEbkM7QUFFZSxZQUFBLGdCQUFnQixFQUFFLEtBQUt3QyxLQUFMLENBQVdnTCxnQkFGNUM7QUFHZSxZQUFBLFNBQVMsRUFBRSxLQUFLaEwsS0FBTCxDQUFXK0gsU0FIckM7QUFJZSxZQUFBLFVBQVUsRUFBRSxLQUFLL0gsS0FBTCxDQUFXL0IsVUFKdEM7QUFLZSxZQUFBLGFBQWEsRUFBRSxLQUFLK0IsS0FBTCxDQUFXN0IsYUFMekM7QUFNZSxZQUFBLGNBQWMsRUFBRSxLQUFLNkIsS0FBTCxDQUFXM0IsY0FOMUM7QUFPZSxZQUFBLGVBQWUsRUFBRSxLQUFLMkIsS0FBTCxDQUFXekI7QUFQM0MsWUFWSixFQWtCTStMLGNBbEJOLEVBbUJNQyxZQW5CTixFQW9CTVIsU0FwQk4sQ0FMSixFQWdDTU4sTUFoQ04sQ0FESjtBQW9DSDtBQWpJTDtBQW1JSDtBQXZ4QjJCLENBQWpCLEMsRUEweEJmOzs7O0FBQ0EsTUFBTXdCLFlBQVksR0FBRyxDQUFDLGdCQUFELEVBQW1CLFdBQW5CLENBQXJCOztBQUNBLFNBQVN0RCxjQUFULENBQXdCOUwsRUFBeEIsRUFBNEI7QUFDeEIsU0FBUW9QLFlBQVksQ0FBQ0MsUUFBYixDQUFzQnJQLEVBQUUsQ0FBQ0UsT0FBSCxFQUF0QixDQUFSO0FBQ0g7O0FBRU0sU0FBU29QLGdCQUFULENBQTBCakcsQ0FBMUIsRUFBNkI7QUFDaEM7QUFDQSxNQUFJQSxDQUFDLENBQUN0SCxVQUFGLE1BQWtCLENBQUMrSixjQUFjLENBQUN6QyxDQUFELENBQXJDLEVBQTBDLE9BQU8sS0FBUCxDQUZWLENBSWhDOztBQUNBLE1BQUlBLENBQUMsQ0FBQ29DLFVBQUYsQ0FBYSxXQUFiLENBQUosRUFBK0IsT0FBTyxLQUFQO0FBRS9CLFFBQU04RCxPQUFPLEdBQUd4UCxjQUFjLENBQUNzSixDQUFELENBQTlCO0FBQ0EsTUFBSWtHLE9BQU8sS0FBSzFPLFNBQWhCLEVBQTJCLE9BQU8sS0FBUDs7QUFDM0IsTUFBSTBPLE9BQU8sS0FBSyx1QkFBaEIsRUFBeUM7QUFDckMsV0FBT0MsWUFBWSxDQUFDQyxZQUFiLENBQTBCcEcsQ0FBMUIsTUFBaUMsRUFBeEM7QUFDSCxHQUZELE1BRU8sSUFBSWtHLE9BQU8sS0FBSyxxQkFBaEIsRUFBdUM7QUFDMUMsV0FBT0csT0FBTyxDQUFDckcsQ0FBQyxDQUFDakosVUFBRixHQUFlLGFBQWYsQ0FBRCxDQUFkO0FBQ0gsR0FGTSxNQUVBO0FBQ0gsV0FBTyxJQUFQO0FBQ0g7QUFDSjs7QUFFRCxTQUFTdVAsdUJBQVQsQ0FBaUN4TCxLQUFqQyxFQUF3QztBQUNwQyxTQUNJLDZCQUFDLFVBQUQ7QUFBWSxJQUFBLEtBQUssRUFBRSx5QkFBRyxrQ0FBSCxDQUFuQjtBQUEyRCxJQUFBLElBQUksRUFBQztBQUFoRSxLQUFvRkEsS0FBcEYsRUFESjtBQUdIOztBQUVELFNBQVN5TCxvQkFBVCxDQUE4QnpMLEtBQTlCLEVBQXFDO0FBQ2pDLFNBQ0ksNkJBQUMsVUFBRDtBQUFZLElBQUEsS0FBSyxFQUFFLHlCQUFHLG9DQUFILENBQW5CO0FBQTZELElBQUEsSUFBSSxFQUFDO0FBQWxFLEtBQW1GQSxLQUFuRixFQURKO0FBR0g7O0FBRUQsU0FBUzBMLHFCQUFULENBQStCMUwsS0FBL0IsRUFBc0M7QUFDbEMsU0FDSSw2QkFBQyxVQUFEO0FBQVksSUFBQSxLQUFLLEVBQUUseUJBQUcsYUFBSCxDQUFuQjtBQUFzQyxJQUFBLElBQUksRUFBQztBQUEzQyxLQUE2REEsS0FBN0QsRUFESjtBQUdIOztBQUVELFNBQVMyTCxpQkFBVCxDQUEyQjNMLEtBQTNCLEVBQWtDO0FBQzlCLFNBQ0ksNkJBQUMsVUFBRDtBQUFZLElBQUEsS0FBSyxFQUFFLHlCQUFHLGdDQUFILENBQW5CO0FBQXlELElBQUEsSUFBSSxFQUFDO0FBQTlELEtBQTRFQSxLQUE1RSxFQURKO0FBR0g7O0FBRUQsTUFBTTRMLFVBQU4sU0FBeUJDLGVBQU1DLFNBQS9CLENBQXlDO0FBTXJDQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLHdEQVFDLE1BQU07QUFDakIsV0FBS3JLLFFBQUwsQ0FBYztBQUFDc0ssUUFBQUEsS0FBSyxFQUFFO0FBQVIsT0FBZDtBQUNILEtBVmE7QUFBQSxzREFZRCxNQUFNO0FBQ2YsV0FBS3RLLFFBQUwsQ0FBYztBQUFDc0ssUUFBQUEsS0FBSyxFQUFFO0FBQVIsT0FBZDtBQUNILEtBZGE7QUFHVixTQUFLaEwsS0FBTCxHQUFhO0FBQ1RnTCxNQUFBQSxLQUFLLEVBQUU7QUFERSxLQUFiO0FBR0g7O0FBVURuRixFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJb0YsT0FBTyxHQUFHLElBQWQ7O0FBQ0EsUUFBSSxLQUFLakwsS0FBTCxDQUFXZ0wsS0FBZixFQUFzQjtBQUNsQixZQUFNRSxPQUFPLEdBQUdwUCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsa0JBQWpCLENBQWhCO0FBQ0FrUCxNQUFBQSxPQUFPLEdBQUcsNkJBQUMsT0FBRDtBQUFTLFFBQUEsU0FBUyxFQUFDLDhCQUFuQjtBQUFrRCxRQUFBLEtBQUssRUFBRSxLQUFLak0sS0FBTCxDQUFXbU0sS0FBcEU7QUFBMkUsUUFBQSxHQUFHLEVBQUM7QUFBL0UsUUFBVjtBQUNIOztBQUVELFFBQUluRSxPQUFPLHVEQUFnRCxLQUFLaEksS0FBTCxDQUFXb00sSUFBM0QsQ0FBWDs7QUFDQSxRQUFJLENBQUN6UCx1QkFBY0MsUUFBZCxDQUF1QiwyQkFBdkIsQ0FBTCxFQUEwRDtBQUN0RG9MLE1BQUFBLE9BQU8sSUFBSSw4QkFBWDtBQUNIOztBQUVELFdBQ0k7QUFDSSxNQUFBLFNBQVMsRUFBRUEsT0FEZjtBQUVJLE1BQUEsT0FBTyxFQUFFLEtBQUtxRSxPQUZsQjtBQUdJLE1BQUEsWUFBWSxFQUFFLEtBQUtDLFlBSHZCO0FBSUksTUFBQSxZQUFZLEVBQUUsS0FBS0M7QUFKdkIsT0FLRU4sT0FMRixDQURKO0FBUUg7O0FBMUNvQzs7OEJBQW5DTCxVLGVBQ2lCO0FBQ2ZRLEVBQUFBLElBQUksRUFBRTNPLG1CQUFVVyxNQUFWLENBQWlCVCxVQURSO0FBRWZ3TyxFQUFBQSxLQUFLLEVBQUUxTyxtQkFBVVcsTUFBVixDQUFpQlQ7QUFGVCxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlcGx5VGhyZWFkIGZyb20gXCIuLi9lbGVtZW50cy9SZXBseVRocmVhZFwiO1xyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSBcImNsYXNzbmFtZXNcIjtcclxuaW1wb3J0IHsgX3QsIF90ZCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIFRleHRGb3JFdmVudCBmcm9tIFwiLi4vLi4vLi4vVGV4dEZvckV2ZW50XCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IGRpcyBmcm9tICcuLi8uLi8uLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IHtFdmVudFN0YXR1c30gZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcbmltcG9ydCB7Zm9ybWF0VGltZX0gZnJvbSBcIi4uLy4uLy4uL0RhdGVVdGlsc1wiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IHtBTExfUlVMRV9UWVBFU30gZnJvbSBcIi4uLy4uLy4uL21qb2xuaXIvQmFuTGlzdFwiO1xyXG5pbXBvcnQgKiBhcyBPYmplY3RVdGlscyBmcm9tIFwiLi4vLi4vLi4vT2JqZWN0VXRpbHNcIjtcclxuaW1wb3J0IE1hdHJpeENsaWVudENvbnRleHQgZnJvbSBcIi4uLy4uLy4uL2NvbnRleHRzL01hdHJpeENsaWVudENvbnRleHRcIjtcclxuaW1wb3J0IHtFMkVfU1RBVEV9IGZyb20gXCIuL0UyRUljb25cIjtcclxuXHJcbmNvbnN0IGV2ZW50VGlsZVR5cGVzID0ge1xyXG4gICAgJ20ucm9vbS5tZXNzYWdlJzogJ21lc3NhZ2VzLk1lc3NhZ2VFdmVudCcsXHJcbiAgICAnbS5zdGlja2VyJzogJ21lc3NhZ2VzLk1lc3NhZ2VFdmVudCcsXHJcbiAgICAnbS5rZXkudmVyaWZpY2F0aW9uLmNhbmNlbCc6ICdtZXNzYWdlcy5NS2V5VmVyaWZpY2F0aW9uQ29uY2x1c2lvbicsXHJcbiAgICAnbS5rZXkudmVyaWZpY2F0aW9uLmRvbmUnOiAnbWVzc2FnZXMuTUtleVZlcmlmaWNhdGlvbkNvbmNsdXNpb24nLFxyXG4gICAgJ20ucm9vbS5lbmNyeXB0aW9uJzogJ21lc3NhZ2VzLkVuY3J5cHRpb25FdmVudCcsXHJcbiAgICAnbS5jYWxsLmludml0ZSc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20uY2FsbC5hbnN3ZXInOiAnbWVzc2FnZXMuVGV4dHVhbEV2ZW50JyxcclxuICAgICdtLmNhbGwuaGFuZ3VwJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbn07XHJcblxyXG5jb25zdCBzdGF0ZUV2ZW50VGlsZVR5cGVzID0ge1xyXG4gICAgJ20ucm9vbS5lbmNyeXB0aW9uJzogJ21lc3NhZ2VzLkVuY3J5cHRpb25FdmVudCcsXHJcbiAgICAnbS5yb29tLmNhbm9uaWNhbF9hbGlhcyc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20ucm9vbS5jcmVhdGUnOiAnbWVzc2FnZXMuUm9vbUNyZWF0ZScsXHJcbiAgICAnbS5yb29tLm1lbWJlcic6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20ucm9vbS5uYW1lJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLmF2YXRhcic6ICdtZXNzYWdlcy5Sb29tQXZhdGFyRXZlbnQnLFxyXG4gICAgJ20ucm9vbS50aGlyZF9wYXJ0eV9pbnZpdGUnOiAnbWVzc2FnZXMuVGV4dHVhbEV2ZW50JyxcclxuICAgICdtLnJvb20uaGlzdG9yeV92aXNpYmlsaXR5JzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLnRvcGljJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLnBvd2VyX2xldmVscyc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20ucm9vbS5waW5uZWRfZXZlbnRzJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLnNlcnZlcl9hY2wnOiAnbWVzc2FnZXMuVGV4dHVhbEV2ZW50JyxcclxuICAgICdpbS52ZWN0b3IubW9kdWxhci53aWRnZXRzJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLnRvbWJzdG9uZSc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20ucm9vbS5qb2luX3J1bGVzJzogJ21lc3NhZ2VzLlRleHR1YWxFdmVudCcsXHJcbiAgICAnbS5yb29tLmd1ZXN0X2FjY2Vzcyc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG4gICAgJ20ucm9vbS5yZWxhdGVkX2dyb3Vwcyc6ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnLFxyXG59O1xyXG5cclxuLy8gQWRkIGFsbCB0aGUgTWpvbG5pciBzdHVmZiB0byB0aGUgcmVuZGVyZXJcclxuZm9yIChjb25zdCBldlR5cGUgb2YgQUxMX1JVTEVfVFlQRVMpIHtcclxuICAgIHN0YXRlRXZlbnRUaWxlVHlwZXNbZXZUeXBlXSA9ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0SGFuZGxlclRpbGUoZXYpIHtcclxuICAgIGNvbnN0IHR5cGUgPSBldi5nZXRUeXBlKCk7XHJcblxyXG4gICAgLy8gZG9uJ3Qgc2hvdyB2ZXJpZmljYXRpb24gcmVxdWVzdHMgd2UncmUgbm90IGludm9sdmVkIGluLFxyXG4gICAgLy8gbm90IGV2ZW4gd2hlbiBzaG93aW5nIGhpZGRlbiBldmVudHNcclxuICAgIGlmICh0eXBlID09PSBcIm0ucm9vbS5tZXNzYWdlXCIpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gZXYuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgIGlmIChjb250ZW50ICYmIGNvbnRlbnQubXNndHlwZSA9PT0gXCJtLmtleS52ZXJpZmljYXRpb24ucmVxdWVzdFwiKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICAgICAgY29uc3QgbWUgPSBjbGllbnQgJiYgY2xpZW50LmdldFVzZXJJZCgpO1xyXG4gICAgICAgICAgICBpZiAoZXYuZ2V0U2VuZGVyKCkgIT09IG1lICYmIGNvbnRlbnQudG8gIT09IG1lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwibWVzc2FnZXMuTUtleVZlcmlmaWNhdGlvblJlcXVlc3RcIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHRoZXNlIGV2ZW50cyBhcmUgc2VudCBieSBib3RoIHBhcnRpZXMgZHVyaW5nIHZlcmlmaWNhdGlvbiwgYnV0IHdlIG9ubHkgd2FudCB0byByZW5kZXIgb25lXHJcbiAgICAvLyB0aWxlIG9uY2UgdGhlIHZlcmlmaWNhdGlvbiBjb25jbHVkZXMsIHNvIGZpbHRlciBvdXQgdGhlIG9uZSBmcm9tIHRoZSBvdGhlciBwYXJ0eS5cclxuICAgIGlmICh0eXBlID09PSBcIm0ua2V5LnZlcmlmaWNhdGlvbi5kb25lXCIpIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3QgbWUgPSBjbGllbnQgJiYgY2xpZW50LmdldFVzZXJJZCgpO1xyXG4gICAgICAgIGlmIChldi5nZXRTZW5kZXIoKSAhPT0gbWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gc29tZXRpbWVzIE1LZXlWZXJpZmljYXRpb25Db25jbHVzaW9uIGRlY2xpbmVzIHRvIHJlbmRlci4gIEphbmtpbHkgZGVjbGluZSB0byByZW5kZXIgYW5kXHJcbiAgICAvLyBmYWxsIGJhY2sgdG8gc2hvd2luZyBoaWRkZW4gZXZlbnRzLCBpZiB3ZSdyZSB2aWV3aW5nIGhpZGRlbiBldmVudHNcclxuICAgIC8vIFhYWDogVGhpcyBpcyBleHRyZW1lbHkgYSBoYWNrLiBQb3NzaWJseSB0aGVzZSBjb21wb25lbnRzIHNob3VsZCBoYXZlIGFuIGludGVyZmFjZSBmb3JcclxuICAgIC8vIGRlY2xpbmluZyB0byByZW5kZXI/XHJcbiAgICBpZiAodHlwZSA9PT0gXCJtLmtleS52ZXJpZmljYXRpb24uY2FuY2VsXCIgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lXCIpKSB7XHJcbiAgICAgICAgY29uc3QgTUtleVZlcmlmaWNhdGlvbkNvbmNsdXNpb24gPSBzZGsuZ2V0Q29tcG9uZW50KFwibWVzc2FnZXMuTUtleVZlcmlmaWNhdGlvbkNvbmNsdXNpb25cIik7XHJcbiAgICAgICAgaWYgKCFNS2V5VmVyaWZpY2F0aW9uQ29uY2x1c2lvbi5wcm90b3R5cGUuX3Nob3VsZFJlbmRlci5jYWxsKG51bGwsIGV2LCBldi5yZXF1ZXN0KSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBldi5pc1N0YXRlKCkgPyBzdGF0ZUV2ZW50VGlsZVR5cGVzW3R5cGVdIDogZXZlbnRUaWxlVHlwZXNbdHlwZV07XHJcbn1cclxuXHJcbmNvbnN0IE1BWF9SRUFEX0FWQVRBUlMgPSA1O1xyXG5cclxuLy8gT3VyIGNvbXBvbmVudCBzdHJ1Y3R1cmUgZm9yIEV2ZW50VGlsZXMgb24gdGhlIHRpbWVsaW5lIGlzOlxyXG4vL1xyXG4vLyAuLUV2ZW50VGlsZS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS5cclxuLy8gfCBNZW1iZXJBdmF0YXIgKFNlbmRlclByb2ZpbGUpICAgICAgICAgICAgICAgICAgIFRpbWVTdGFtcCB8XHJcbi8vIHwgICAgLi17TWVzc2FnZSxUZXh0dWFsfUV2ZW50LS0tLS0tLS0tLS0tLS0tLiBSZWFkIEF2YXRhcnMgfFxyXG4vLyB8ICAgIHwgICAuLU1Gb29Cb2R5LS0tLS0tLS0tLS0tLS0tLS0tLS4gICAgIHwgICAgICAgICAgICAgIHxcclxuLy8gfCAgICB8ICAgfCAgKG9ubHkgaWYgTWVzc2FnZUV2ZW50KSAgICB8ICAgICB8ICAgICAgICAgICAgICB8XHJcbi8vIHwgICAgfCAgICctLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tJyAgICAgfCAgICAgICAgICAgICAgfFxyXG4vLyB8ICAgICctLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLScgICAgICAgICAgICAgIHxcclxuLy8gJy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0nXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnRXZlbnRUaWxlJyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICAvKiB0aGUgTWF0cml4RXZlbnQgdG8gc2hvdyAqL1xyXG4gICAgICAgIG14RXZlbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLyogdHJ1ZSBpZiBteEV2ZW50IGlzIHJlZGFjdGVkLiBUaGlzIGlzIGEgcHJvcCBiZWNhdXNlIHVzaW5nIG14RXZlbnQuaXNSZWRhY3RlZCgpXHJcbiAgICAgICAgICogbWlnaHQgbm90IGJlIGVub3VnaCB3aGVuIGRlY2lkaW5nIHNob3VsZENvbXBvbmVudFVwZGF0ZSAtIHByZXZQcm9wcy5teEV2ZW50XHJcbiAgICAgICAgICogcmVmZXJlbmNlcyB0aGUgc2FtZSB0aGlzLnByb3BzLm14RXZlbnQuXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgaXNSZWRhY3RlZDogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIHRydWUgaWYgdGhpcyBpcyBhIGNvbnRpbnVhdGlvbiBvZiB0aGUgcHJldmlvdXMgZXZlbnQgKHdoaWNoIGhhcyB0aGVcclxuICAgICAgICAgKiBlZmZlY3Qgb2Ygbm90IHNob3dpbmcgYW5vdGhlciBhdmF0YXIvZGlzcGxheW5hbWVcclxuICAgICAgICAgKi9cclxuICAgICAgICBjb250aW51YXRpb246IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvKiB0cnVlIGlmIHRoaXMgaXMgdGhlIGxhc3QgZXZlbnQgaW4gdGhlIHRpbWVsaW5lICh3aGljaCBoYXMgdGhlIGVmZmVjdFxyXG4gICAgICAgICAqIG9mIGFsd2F5cyBzaG93aW5nIHRoZSB0aW1lc3RhbXApXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgbGFzdDogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIHRydWUgaWYgdGhpcyBpcyBzZWFyY2ggY29udGV4dCAod2hpY2ggaGFzIHRoZSBlZmZlY3Qgb2YgZ3JleWluZyBvdXRcclxuICAgICAgICAgKiB0aGUgdGV4dFxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGNvbnRleHR1YWw6IFByb3BUeXBlcy5ib29sLFxyXG5cclxuICAgICAgICAvKiBhIGxpc3Qgb2Ygd29yZHMgdG8gaGlnaGxpZ2h0LCBvcmRlcmVkIGJ5IGxvbmdlc3QgZmlyc3QgKi9cclxuICAgICAgICBoaWdobGlnaHRzOiBQcm9wVHlwZXMuYXJyYXksXHJcblxyXG4gICAgICAgIC8qIGxpbmsgVVJMIGZvciB0aGUgaGlnaGxpZ2h0cyAqL1xyXG4gICAgICAgIGhpZ2hsaWdodExpbms6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8qIHNob3VsZCBzaG93IFVSTCBwcmV2aWV3cyBmb3IgdGhpcyBldmVudCAqL1xyXG4gICAgICAgIHNob3dVcmxQcmV2aWV3OiBQcm9wVHlwZXMuYm9vbCxcclxuXHJcbiAgICAgICAgLyogaXMgdGhpcyB0aGUgZm9jdXNlZCBldmVudCAqL1xyXG4gICAgICAgIGlzU2VsZWN0ZWRFdmVudDogUHJvcFR5cGVzLmJvb2wsXHJcblxyXG4gICAgICAgIC8qIGNhbGxiYWNrIGNhbGxlZCB3aGVuIGR5bmFtaWMgY29udGVudCBpbiBldmVudHMgYXJlIGxvYWRlZCAqL1xyXG4gICAgICAgIG9uSGVpZ2h0Q2hhbmdlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8qIGEgbGlzdCBvZiByZWFkLXJlY2VpcHRzIHdlIHNob3VsZCBzaG93LiBFYWNoIG9iamVjdCBoYXMgYSAncm9vbU1lbWJlcicgYW5kICd0cycuICovXHJcbiAgICAgICAgcmVhZFJlY2VpcHRzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMub2JqZWN0KSxcclxuXHJcbiAgICAgICAgLyogb3BhcXVlIHJlYWRyZWNlaXB0IGluZm8gZm9yIGVhY2ggdXNlcklkOyB1c2VkIGJ5IFJlYWRSZWNlaXB0TWFya2VyXHJcbiAgICAgICAgICogdG8gbWFuYWdlIGl0cyBhbmltYXRpb25zLiBTaG91bGQgYmUgYW4gZW1wdHkgb2JqZWN0IHdoZW4gdGhlIHJvb21cclxuICAgICAgICAgKiBmaXJzdCBsb2Fkc1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHJlYWRSZWNlaXB0TWFwOiBQcm9wVHlwZXMub2JqZWN0LFxyXG5cclxuICAgICAgICAvKiBBIGZ1bmN0aW9uIHdoaWNoIGlzIHVzZWQgdG8gY2hlY2sgaWYgdGhlIHBhcmVudCBwYW5lbCBpcyBiZWluZ1xyXG4gICAgICAgICAqIHVubW91bnRlZCwgdG8gYXZvaWQgdW5uZWNlc3Nhcnkgd29yay4gU2hvdWxkIHJldHVybiB0cnVlIGlmIHdlXHJcbiAgICAgICAgICogYXJlIGJlaW5nIHVubW91bnRlZC5cclxuICAgICAgICAgKi9cclxuICAgICAgICBjaGVja1VubW91bnRpbmc6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvKiB0aGUgc3RhdHVzIG9mIHRoaXMgZXZlbnQgLSBpZSwgbXhFdmVudC5zdGF0dXMuIERlbm9ybWFsaXNlZCB0byBoZXJlIHNvXHJcbiAgICAgICAgICogdGhhdCB3ZSBjYW4gdGVsbCB3aGVuIGl0IGNoYW5nZXMuICovXHJcbiAgICAgICAgZXZlbnRTZW5kU3RhdHVzOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvKiB0aGUgc2hhcGUgb2YgdGhlIHRpbGUuIGJ5IGRlZmF1bHQsIHRoZSBsYXlvdXQgaXMgaW50ZW5kZWQgZm9yIHRoZVxyXG4gICAgICAgICAqIG5vcm1hbCByb29tIHRpbWVsaW5lLiAgYWx0ZXJuYXRpdmUgdmFsdWVzIGFyZTogXCJmaWxlX2xpc3RcIiwgXCJmaWxlX2dyaWRcIlxyXG4gICAgICAgICAqIGFuZCBcIm5vdGlmXCIuICBUaGlzIGNvdWxkIGJlIGRvbmUgYnkgQ1NTLCBidXQgaXQnZCBiZSBob3JyaWJseSBpbmVmZmljaWVudC5cclxuICAgICAgICAgKiBJdCBjb3VsZCBhbHNvIGJlIGRvbmUgYnkgc3ViY2xhc3NpbmcgRXZlbnRUaWxlLCBidXQgdGhhdCdkIGJlIHF1aXRlXHJcbiAgICAgICAgICogYm9paWxlcnBsYXRleS4gIFNvIGp1c3QgbWFrZSB0aGUgbmVjZXNzYXJ5IHJlbmRlciBkZWNpc2lvbnMgY29uZGl0aW9uYWxcclxuICAgICAgICAgKiBmb3Igbm93LlxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHRpbGVTaGFwZTogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLy8gc2hvdyB0d2VsdmUgaG91ciB0aW1lc3RhbXBzXHJcbiAgICAgICAgaXNUd2VsdmVIb3VyOiBQcm9wVHlwZXMuYm9vbCxcclxuXHJcbiAgICAgICAgLy8gaGVscGVyIGZ1bmN0aW9uIHRvIGFjY2VzcyByZWxhdGlvbnMgZm9yIHRoaXMgZXZlbnRcclxuICAgICAgICBnZXRSZWxhdGlvbnNGb3JFdmVudDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIHdoZXRoZXIgdG8gc2hvdyByZWFjdGlvbnMgZm9yIHRoaXMgZXZlbnRcclxuICAgICAgICBzaG93UmVhY3Rpb25zOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAvLyBuby1vcCBmdW5jdGlvbiBiZWNhdXNlIG9uSGVpZ2h0Q2hhbmdlZCBpcyBvcHRpb25hbCB5ZXQgc29tZSBzdWItY29tcG9uZW50cyBhc3N1bWUgaXRzIGV4aXN0ZW5jZVxyXG4gICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ6IGZ1bmN0aW9uKCkge30sXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAvLyBXaGV0aGVyIHRoZSBhY3Rpb24gYmFyIGlzIGZvY3VzZWQuXHJcbiAgICAgICAgICAgIGFjdGlvbkJhckZvY3VzZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAvLyBXaGV0aGVyIGFsbCByZWFkIHJlY2VpcHRzIGFyZSBiZWluZyBkaXNwbGF5ZWQuIElmIG5vdCwgb25seSBkaXNwbGF5XHJcbiAgICAgICAgICAgIC8vIGEgdHJ1bmNhdGlvbiBvZiB0aGVtLlxyXG4gICAgICAgICAgICBhbGxSZWFkQXZhdGFyczogZmFsc2UsXHJcbiAgICAgICAgICAgIC8vIFdoZXRoZXIgdGhlIGV2ZW50J3Mgc2VuZGVyIGhhcyBiZWVuIHZlcmlmaWVkLlxyXG4gICAgICAgICAgICB2ZXJpZmllZDogbnVsbCxcclxuICAgICAgICAgICAgLy8gV2hldGhlciBvblJlcXVlc3RLZXlzQ2xpY2sgaGFzIGJlZW4gY2FsbGVkIHNpbmNlIG1vdW50aW5nLlxyXG4gICAgICAgICAgICBwcmV2aW91c2x5UmVxdWVzdGVkS2V5czogZmFsc2UsXHJcbiAgICAgICAgICAgIC8vIFRoZSBSZWxhdGlvbnMgbW9kZWwgZnJvbSB0aGUgSlMgU0RLIGZvciByZWFjdGlvbnMgdG8gYG14RXZlbnRgXHJcbiAgICAgICAgICAgIHJlYWN0aW9uczogdGhpcy5nZXRSZWFjdGlvbnMoKSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBzdGF0aWNzOiB7XHJcbiAgICAgICAgY29udGV4dFR5cGU6IE1hdHJpeENsaWVudENvbnRleHQsXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIGNvbXBvbmVudCB3aXRoIHJlYWwgY2xhc3MsIHVzZSBjb25zdHJ1Y3RvciBmb3IgcmVmc1xyXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gZG9uJ3QgZG8gUlIgYW5pbWF0aW9ucyB1bnRpbCB3ZSBhcmUgbW91bnRlZFxyXG4gICAgICAgIHRoaXMuX3N1cHByZXNzUmVhZFJlY2VpcHRBbmltYXRpb24gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX3ZlcmlmeUV2ZW50KHRoaXMucHJvcHMubXhFdmVudCk7XHJcblxyXG4gICAgICAgIHRoaXMuX3RpbGUgPSBjcmVhdGVSZWYoKTtcclxuICAgICAgICB0aGlzLl9yZXBseVRocmVhZCA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fc3VwcHJlc3NSZWFkUmVjZWlwdEFuaW1hdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IHRoaXMuY29udGV4dDtcclxuICAgICAgICBjbGllbnQub24oXCJkZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkXCIsIHRoaXMub25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkKTtcclxuICAgICAgICBjbGllbnQub24oXCJ1c2VyVHJ1c3RTdGF0dXNDaGFuZ2VkXCIsIHRoaXMub25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZCk7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5teEV2ZW50Lm9uKFwiRXZlbnQuZGVjcnlwdGVkXCIsIHRoaXMuX29uRGVjcnlwdGVkKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaG93UmVhY3Rpb25zKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMubXhFdmVudC5vbihcIkV2ZW50LnJlbGF0aW9uc0NyZWF0ZWRcIiwgdGhpcy5fb25SZWFjdGlvbnNDcmVhdGVkKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBSZXBsYWNlIHdpdGggYXBwcm9wcmlhdGUgbGlmZWN5Y2xlIGV2ZW50XHJcbiAgICBVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XHJcbiAgICAgICAgLy8gcmUtY2hlY2sgdGhlIHNlbmRlciB2ZXJpZmljYXRpb24gYXMgb3V0Z29pbmcgZXZlbnRzIHByb2dyZXNzIHRocm91Z2hcclxuICAgICAgICAvLyB0aGUgc2VuZCBwcm9jZXNzLlxyXG4gICAgICAgIGlmIChuZXh0UHJvcHMuZXZlbnRTZW5kU3RhdHVzICE9PSB0aGlzLnByb3BzLmV2ZW50U2VuZFN0YXR1cykge1xyXG4gICAgICAgICAgICB0aGlzLl92ZXJpZnlFdmVudChuZXh0UHJvcHMubXhFdmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGU6IGZ1bmN0aW9uKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XHJcbiAgICAgICAgaWYgKCFPYmplY3RVdGlscy5zaGFsbG93RXF1YWwodGhpcy5zdGF0ZSwgbmV4dFN0YXRlKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAhdGhpcy5fcHJvcHNFcXVhbCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gdGhpcy5jb250ZXh0O1xyXG4gICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcImRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWRcIiwgdGhpcy5vbkRldmljZVZlcmlmaWNhdGlvbkNoYW5nZWQpO1xyXG4gICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcInVzZXJUcnVzdFN0YXR1c0NoYW5nZWRcIiwgdGhpcy5vblVzZXJWZXJpZmljYXRpb25DaGFuZ2VkKTtcclxuICAgICAgICB0aGlzLnByb3BzLm14RXZlbnQucmVtb3ZlTGlzdGVuZXIoXCJFdmVudC5kZWNyeXB0ZWRcIiwgdGhpcy5fb25EZWNyeXB0ZWQpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNob3dSZWFjdGlvbnMpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5teEV2ZW50LnJlbW92ZUxpc3RlbmVyKFwiRXZlbnQucmVsYXRpb25zQ3JlYXRlZFwiLCB0aGlzLl9vblJlYWN0aW9uc0NyZWF0ZWQpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLyoqIGNhbGxlZCB3aGVuIHRoZSBldmVudCBpcyBkZWNyeXB0ZWQgYWZ0ZXIgd2Ugc2hvdyBpdC5cclxuICAgICAqL1xyXG4gICAgX29uRGVjcnlwdGVkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyB3ZSBuZWVkIHRvIHJlLXZlcmlmeSB0aGUgc2VuZGluZyBkZXZpY2UuXHJcbiAgICAgICAgLy8gKHdlIGNhbGwgb25IZWlnaHRDaGFuZ2VkIGluIF92ZXJpZnlFdmVudCB0byBoYW5kbGUgdGhlIGNhc2Ugd2hlcmUgZGVjcnlwdGlvblxyXG4gICAgICAgIC8vIGhhcyBjYXVzZWQgYSBjaGFuZ2UgaW4gc2l6ZSBvZiB0aGUgZXZlbnQgdGlsZSlcclxuICAgICAgICB0aGlzLl92ZXJpZnlFdmVudCh0aGlzLnByb3BzLm14RXZlbnQpO1xyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25EZXZpY2VWZXJpZmljYXRpb25DaGFuZ2VkOiBmdW5jdGlvbih1c2VySWQsIGRldmljZSkge1xyXG4gICAgICAgIGlmICh1c2VySWQgPT09IHRoaXMucHJvcHMubXhFdmVudC5nZXRTZW5kZXIoKSkge1xyXG4gICAgICAgICAgICB0aGlzLl92ZXJpZnlFdmVudCh0aGlzLnByb3BzLm14RXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgb25Vc2VyVmVyaWZpY2F0aW9uQ2hhbmdlZDogZnVuY3Rpb24odXNlcklkLCBfdHJ1c3RTdGF0dXMpIHtcclxuICAgICAgICBpZiAodXNlcklkID09PSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0U2VuZGVyKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5fdmVyaWZ5RXZlbnQodGhpcy5wcm9wcy5teEV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF92ZXJpZnlFdmVudDogYXN5bmMgZnVuY3Rpb24obXhFdmVudCkge1xyXG4gICAgICAgIGlmICghbXhFdmVudC5pc0VuY3J5cHRlZCgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIElmIHdlIGRpcmVjdGx5IHRydXN0IHRoZSBkZXZpY2UsIHNob3J0LWNpcmN1aXQgaGVyZVxyXG4gICAgICAgIGNvbnN0IHZlcmlmaWVkID0gYXdhaXQgdGhpcy5jb250ZXh0LmlzRXZlbnRTZW5kZXJWZXJpZmllZChteEV2ZW50KTtcclxuICAgICAgICBpZiAodmVyaWZpZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICB2ZXJpZmllZDogRTJFX1NUQVRFLlZFUklGSUVELFxyXG4gICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBEZWNyeXB0aW9uIG1heSBoYXZlIGNhdXNlZCBhIGNoYW5nZSBpbiBzaXplXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uSGVpZ2h0Q2hhbmdlZCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgY3Jvc3Mtc2lnbmluZyBpcyBvZmYsIHRoZSBvbGQgYmVoYXZpb3VyIGlzIHRvIHNjcmVhbSBhdCB0aGUgdXNlclxyXG4gICAgICAgIC8vIGFzIGlmIHRoZXkndmUgZG9uZSBzb21ldGhpbmcgd3JvbmcsIHdoaWNoIHRoZXkgaGF2ZW4ndFxyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgdmVyaWZpZWQ6IEUyRV9TVEFURS5XQVJOSU5HLFxyXG4gICAgICAgICAgICB9LCB0aGlzLnByb3BzLm9uSGVpZ2h0Q2hhbmdlZCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5jb250ZXh0LmNoZWNrVXNlclRydXN0KG14RXZlbnQuZ2V0U2VuZGVyKCkpLmlzQ3Jvc3NTaWduaW5nVmVyaWZpZWQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHZlcmlmaWVkOiBFMkVfU1RBVEUuTk9STUFMLFxyXG4gICAgICAgICAgICB9LCB0aGlzLnByb3BzLm9uSGVpZ2h0Q2hhbmdlZCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50U2VuZGVyVHJ1c3QgPSBhd2FpdCB0aGlzLmNvbnRleHQuY2hlY2tFdmVudFNlbmRlclRydXN0KG14RXZlbnQpO1xyXG4gICAgICAgIGlmICghZXZlbnRTZW5kZXJUcnVzdCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHZlcmlmaWVkOiBFMkVfU1RBVEUuVU5LTk9XTixcclxuICAgICAgICAgICAgfSwgdGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWQpOyAvLyBEZWNyeXB0aW9uIG1heSBoYXZlIGNhdXNlIGEgY2hhbmdlIGluIHNpemVcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHZlcmlmaWVkOiBldmVudFNlbmRlclRydXN0LmlzVmVyaWZpZWQoKSA/IEUyRV9TVEFURS5WRVJJRklFRCA6IEUyRV9TVEFURS5XQVJOSU5HLFxyXG4gICAgICAgIH0sIHRoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkKTsgLy8gRGVjcnlwdGlvbiBtYXkgaGF2ZSBjYXVzZWQgYSBjaGFuZ2UgaW4gc2l6ZVxyXG4gICAgfSxcclxuXHJcbiAgICBfcHJvcHNFcXVhbDogZnVuY3Rpb24ob2JqQSwgb2JqQikge1xyXG4gICAgICAgIGNvbnN0IGtleXNBID0gT2JqZWN0LmtleXMob2JqQSk7XHJcbiAgICAgICAgY29uc3Qga2V5c0IgPSBPYmplY3Qua2V5cyhvYmpCKTtcclxuXHJcbiAgICAgICAgaWYgKGtleXNBLmxlbmd0aCAhPT0ga2V5c0IubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwga2V5c0EubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3Qga2V5ID0ga2V5c0FbaV07XHJcblxyXG4gICAgICAgICAgICBpZiAoIW9iakIuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBuZWVkIHRvIGRlZXAtY29tcGFyZSByZWFkUmVjZWlwdHNcclxuICAgICAgICAgICAgaWYgKGtleSA9PT0gJ3JlYWRSZWNlaXB0cycpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJBID0gb2JqQVtrZXldO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgckIgPSBvYmpCW2tleV07XHJcbiAgICAgICAgICAgICAgICBpZiAockEgPT09IHJCKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFyQSB8fCAhckIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJBLmxlbmd0aCAhPT0gckIubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCByQS5sZW5ndGg7IGorKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyQVtqXS51c2VySWQgIT09IHJCW2pdLnVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9uZSBoYXMgYSBtZW1iZXIgc2V0IGFuZCB0aGUgb3RoZXIgZG9lc24ndD9cclxuICAgICAgICAgICAgICAgICAgICBpZiAockFbal0ucm9vbU1lbWJlciAhPT0gckJbal0ucm9vbU1lbWJlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKG9iakFba2V5XSAhPT0gb2JqQltrZXldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSxcclxuXHJcbiAgICBzaG91bGRIaWdobGlnaHQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGFjdGlvbnMgPSB0aGlzLmNvbnRleHQuZ2V0UHVzaEFjdGlvbnNGb3JFdmVudCh0aGlzLnByb3BzLm14RXZlbnQpO1xyXG4gICAgICAgIGlmICghYWN0aW9ucyB8fCAhYWN0aW9ucy50d2Vha3MpIHsgcmV0dXJuIGZhbHNlOyB9XHJcblxyXG4gICAgICAgIC8vIGRvbid0IHNob3cgc2VsZi1oaWdobGlnaHRzIGZyb20gYW5vdGhlciBvZiBvdXIgY2xpZW50c1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm14RXZlbnQuZ2V0U2VuZGVyKCkgPT09IHRoaXMuY29udGV4dC5jcmVkZW50aWFscy51c2VySWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFjdGlvbnMudHdlYWtzLmhpZ2hsaWdodDtcclxuICAgIH0sXHJcblxyXG4gICAgdG9nZ2xlQWxsUmVhZEF2YXRhcnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBhbGxSZWFkQXZhdGFyczogIXRoaXMuc3RhdGUuYWxsUmVhZEF2YXRhcnMsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFJlYWRBdmF0YXJzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyByZXR1cm4gZWFybHkgaWYgdGhlcmUgYXJlIG5vIHJlYWQgcmVjZWlwdHNcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMucmVhZFJlY2VpcHRzIHx8IHRoaXMucHJvcHMucmVhZFJlY2VpcHRzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKDxzcGFuIGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9yZWFkQXZhdGFyc1wiIC8+KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IFJlYWRSZWNlaXB0TWFya2VyID0gc2RrLmdldENvbXBvbmVudCgncm9vbXMuUmVhZFJlY2VpcHRNYXJrZXInKTtcclxuICAgICAgICBjb25zdCBhdmF0YXJzID0gW107XHJcbiAgICAgICAgY29uc3QgcmVjZWlwdE9mZnNldCA9IDE1O1xyXG4gICAgICAgIGxldCBsZWZ0ID0gMDtcclxuXHJcbiAgICAgICAgY29uc3QgcmVjZWlwdHMgPSB0aGlzLnByb3BzLnJlYWRSZWNlaXB0cyB8fCBbXTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlY2VpcHRzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlY2VpcHQgPSByZWNlaXB0c1tpXTtcclxuXHJcbiAgICAgICAgICAgIGxldCBoaWRkZW4gPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAoKGkgPCBNQVhfUkVBRF9BVkFUQVJTKSB8fCB0aGlzLnN0YXRlLmFsbFJlYWRBdmF0YXJzKSB7XHJcbiAgICAgICAgICAgICAgICBoaWRkZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBUT0RPOiB3ZSBrZWVwIHRoZSBleHRyYSByZWFkIGF2YXRhcnMgaW4gdGhlIGRvbSB0byBtYWtlIGFuaW1hdGlvbiBzaW1wbGVyXHJcbiAgICAgICAgICAgIC8vIHdlIGNvdWxkIG9wdGltaXNlIHRoaXMgdG8gcmVkdWNlIHRoZSBkb20gc2l6ZS5cclxuXHJcbiAgICAgICAgICAgIC8vIElmIGhpZGRlbiwgc2V0IG9mZnNldCBlcXVhbCB0byB0aGUgb2Zmc2V0IG9mIHRoZSBmaW5hbCB2aXNpYmxlIGF2YXRhciBvclxyXG4gICAgICAgICAgICAvLyBlbHNlIHNldCBpdCBwcm9wb3J0aW9uYWwgdG8gaW5kZXhcclxuICAgICAgICAgICAgbGVmdCA9IChoaWRkZW4gPyBNQVhfUkVBRF9BVkFUQVJTIC0gMSA6IGkpICogLXJlY2VpcHRPZmZzZXQ7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1c2VySWQgPSByZWNlaXB0LnVzZXJJZDtcclxuICAgICAgICAgICAgbGV0IHJlYWRSZWNlaXB0SW5mbztcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLnJlYWRSZWNlaXB0TWFwKSB7XHJcbiAgICAgICAgICAgICAgICByZWFkUmVjZWlwdEluZm8gPSB0aGlzLnByb3BzLnJlYWRSZWNlaXB0TWFwW3VzZXJJZF07XHJcbiAgICAgICAgICAgICAgICBpZiAoIXJlYWRSZWNlaXB0SW5mbykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWRSZWNlaXB0SW5mbyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMucmVhZFJlY2VpcHRNYXBbdXNlcklkXSA9IHJlYWRSZWNlaXB0SW5mbztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gYWRkIHRvIHRoZSBzdGFydCBzbyB0aGUgbW9zdCByZWNlbnQgaXMgb24gdGhlIGVuZCAoaWUuIGVuZHMgdXAgcmlnaHRtb3N0KVxyXG4gICAgICAgICAgICBhdmF0YXJzLnVuc2hpZnQoXHJcbiAgICAgICAgICAgICAgICA8UmVhZFJlY2VpcHRNYXJrZXIga2V5PXt1c2VySWR9IG1lbWJlcj17cmVjZWlwdC5yb29tTWVtYmVyfVxyXG4gICAgICAgICAgICAgICAgICAgIGZhbGxiYWNrVXNlcklkPXt1c2VySWR9XHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdE9mZnNldD17bGVmdH0gaGlkZGVuPXtoaWRkZW59XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZFJlY2VpcHRJbmZvPXtyZWFkUmVjZWlwdEluZm99XHJcbiAgICAgICAgICAgICAgICAgICAgY2hlY2tVbm1vdW50aW5nPXt0aGlzLnByb3BzLmNoZWNrVW5tb3VudGluZ31cclxuICAgICAgICAgICAgICAgICAgICBzdXBwcmVzc0FuaW1hdGlvbj17dGhpcy5fc3VwcHJlc3NSZWFkUmVjZWlwdEFuaW1hdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLnRvZ2dsZUFsbFJlYWRBdmF0YXJzfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVzdGFtcD17cmVjZWlwdC50c31cclxuICAgICAgICAgICAgICAgICAgICBzaG93VHdlbHZlSG91cj17dGhpcy5wcm9wcy5pc1R3ZWx2ZUhvdXJ9XHJcbiAgICAgICAgICAgICAgICAvPixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHJlbVRleHQ7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmFsbFJlYWRBdmF0YXJzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlbWFpbmRlciA9IHJlY2VpcHRzLmxlbmd0aCAtIE1BWF9SRUFEX0FWQVRBUlM7XHJcbiAgICAgICAgICAgIGlmIChyZW1haW5kZXIgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICByZW1UZXh0ID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX3JlYWRBdmF0YXJSZW1haW5kZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMudG9nZ2xlQWxsUmVhZEF2YXRhcnN9XHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgcmlnaHQ6IC0obGVmdCAtIHJlY2VpcHRPZmZzZXQpIH19PnsgcmVtYWluZGVyIH0rXHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPHNwYW4gY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX3JlYWRBdmF0YXJzXCI+XHJcbiAgICAgICAgICAgIHsgcmVtVGV4dCB9XHJcbiAgICAgICAgICAgIHsgYXZhdGFycyB9XHJcbiAgICAgICAgPC9zcGFuPjtcclxuICAgIH0sXHJcblxyXG4gICAgb25TZW5kZXJQcm9maWxlQ2xpY2s6IGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgY29uc3QgbXhFdmVudCA9IHRoaXMucHJvcHMubXhFdmVudDtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdpbnNlcnRfbWVudGlvbicsXHJcbiAgICAgICAgICAgIHVzZXJfaWQ6IG14RXZlbnQuZ2V0U2VuZGVyKCksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVxdWVzdEtleXNDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIC8vIEluZGljYXRlIGluIHRoZSBVSSB0aGF0IHRoZSBrZXlzIGhhdmUgYmVlbiByZXF1ZXN0ZWQgKHRoaXMgaXMgZXhwZWN0ZWQgdG9cclxuICAgICAgICAgICAgLy8gYmUgcmVzZXQgaWYgdGhlIGNvbXBvbmVudCBpcyBtb3VudGVkIGluIHRoZSBmdXR1cmUpLlxyXG4gICAgICAgICAgICBwcmV2aW91c2x5UmVxdWVzdGVkS2V5czogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gQ2FuY2VsIGFueSBvdXRnb2luZyBrZXkgcmVxdWVzdCBmb3IgdGhpcyBldmVudCBhbmQgcmVzZW5kIGl0LiBJZiBhIHJlc3BvbnNlXHJcbiAgICAgICAgLy8gaXMgcmVjZWl2ZWQgZm9yIHRoZSByZXF1ZXN0IHdpdGggdGhlIHJlcXVpcmVkIGtleXMsIHRoZSBldmVudCBjb3VsZCBiZVxyXG4gICAgICAgIC8vIGRlY3J5cHRlZCBzdWNjZXNzZnVsbHkuXHJcbiAgICAgICAgdGhpcy5jb250ZXh0LmNhbmNlbEFuZFJlc2VuZEV2ZW50Um9vbUtleVJlcXVlc3QodGhpcy5wcm9wcy5teEV2ZW50KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25QZXJtYWxpbmtDbGlja2VkOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgLy8gVGhpcyBhbGxvd3MgdGhlIHBlcm1hbGluayB0byBiZSBvcGVuZWQgaW4gYSBuZXcgdGFiL3dpbmRvdyBvciBjb3BpZWQgYXNcclxuICAgICAgICAvLyBtYXRyaXgudG8sIGJ1dCBhbHNvIGZvciBpdCB0byBlbmFibGUgcm91dGluZyB3aXRoaW4gUmlvdCB3aGVuIGNsaWNrZWQuXHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfcm9vbScsXHJcbiAgICAgICAgICAgIGV2ZW50X2lkOiB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHRoaXMucHJvcHMubXhFdmVudC5nZXRSb29tSWQoKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX3JlbmRlckUyRVBhZGxvY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGV2ID0gdGhpcy5wcm9wcy5teEV2ZW50O1xyXG5cclxuICAgICAgICAvLyBldmVudCBjb3VsZCBub3QgYmUgZGVjcnlwdGVkXHJcbiAgICAgICAgaWYgKGV2LmdldENvbnRlbnQoKS5tc2d0eXBlID09PSAnbS5iYWQuZW5jcnlwdGVkJykge1xyXG4gICAgICAgICAgICByZXR1cm4gPEUyZVBhZGxvY2tVbmRlY3J5cHRhYmxlIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZXZlbnQgaXMgZW5jcnlwdGVkLCBkaXNwbGF5IHBhZGxvY2sgY29ycmVzcG9uZGluZyB0byB3aGV0aGVyIG9yIG5vdCBpdCBpcyB2ZXJpZmllZFxyXG4gICAgICAgIGlmIChldi5pc0VuY3J5cHRlZCgpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnZlcmlmaWVkID09PSBFMkVfU1RBVEUuTk9STUFMKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vIG5vIGljb24gaWYgd2UndmUgbm90IGV2ZW4gY3Jvc3Mtc2lnbmVkIHRoZSB1c2VyXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52ZXJpZmllZCA9PT0gRTJFX1NUQVRFLlZFUklGSUVEKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vIG5vIGljb24gZm9yIHZlcmlmaWVkXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS52ZXJpZmllZCA9PT0gRTJFX1NUQVRFLlVOS05PV04pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoPEUyZVBhZGxvY2tVbmtub3duIC8+KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoPEUyZVBhZGxvY2tVbnZlcmlmaWVkIC8+KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY29udGV4dC5pc1Jvb21FbmNyeXB0ZWQoZXYuZ2V0Um9vbUlkKCkpKSB7XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgcm9vbSBpcyBlbmNyeXB0ZWRcclxuICAgICAgICAgICAgLy8gYW5kIGV2ZW50IGlzIGJlaW5nIGVuY3J5cHRlZCBvciBpcyBub3Rfc2VudCAoVW5rbm93biBEZXZpY2VzL05ldHdvcmsgRXJyb3IpXHJcbiAgICAgICAgICAgIGlmIChldi5zdGF0dXMgPT09IEV2ZW50U3RhdHVzLkVOQ1JZUFRJTkcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZXYuc3RhdHVzID09PSBFdmVudFN0YXR1cy5OT1RfU0VOVCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChldi5pc1N0YXRlKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjsgLy8gd2UgZXhwZWN0IHRoaXMgdG8gYmUgdW5lbmNyeXB0ZWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBpZiB0aGUgZXZlbnQgaXMgbm90IGVuY3J5cHRlZCwgYnV0IGl0J3MgYW4gZTJlIHJvb20sIHNob3cgdGhlIG9wZW4gcGFkbG9ja1xyXG4gICAgICAgICAgICByZXR1cm4gPEUyZVBhZGxvY2tVbmVuY3J5cHRlZCAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIG5vIHBhZGxvY2sgbmVlZGVkXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uQWN0aW9uQmFyRm9jdXNDaGFuZ2UoZm9jdXNlZCkge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBhY3Rpb25CYXJGb2N1c2VkOiBmb2N1c2VkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRUaWxlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90aWxlLmN1cnJlbnQ7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFJlcGx5VGhyZWFkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yZXBseVRocmVhZC5jdXJyZW50O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRSZWFjdGlvbnMoKSB7XHJcbiAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAhdGhpcy5wcm9wcy5zaG93UmVhY3Rpb25zIHx8XHJcbiAgICAgICAgICAgICF0aGlzLnByb3BzLmdldFJlbGF0aW9uc0ZvckV2ZW50XHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBldmVudElkID0gdGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCk7XHJcbiAgICAgICAgaWYgKCFldmVudElkKSB7XHJcbiAgICAgICAgICAgIC8vIFhYWDogVGVtcG9yYXJ5IGRpYWdub3N0aWMgbG9nZ2luZyBmb3IgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvMTExMjBcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkV2ZW50VGlsZSBhdHRlbXB0ZWQgdG8gZ2V0IHJlbGF0aW9ucyBmb3IgYW4gZXZlbnQgd2l0aG91dCBhbiBJRFwiKTtcclxuICAgICAgICAgICAgLy8gVXNlIGV2ZW50J3Mgc3BlY2lhbCBgdG9KU09OYCBtZXRob2QgdG8gbG9nIGtleSBkYXRhLlxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLnByb3BzLm14RXZlbnQsIG51bGwsIDQpKTtcclxuICAgICAgICAgICAgY29uc29sZS50cmFjZShcIlN0YWNrdHJhY2UgZm9yIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzExMTIwXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5nZXRSZWxhdGlvbnNGb3JFdmVudChldmVudElkLCBcIm0uYW5ub3RhdGlvblwiLCBcIm0ucmVhY3Rpb25cIik7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJlYWN0aW9uc0NyZWF0ZWQocmVsYXRpb25UeXBlLCBldmVudFR5cGUpIHtcclxuICAgICAgICBpZiAocmVsYXRpb25UeXBlICE9PSBcIm0uYW5ub3RhdGlvblwiIHx8IGV2ZW50VHlwZSAhPT0gXCJtLnJlYWN0aW9uXCIpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnByb3BzLm14RXZlbnQucmVtb3ZlTGlzdGVuZXIoXCJFdmVudC5yZWxhdGlvbnNDcmVhdGVkXCIsIHRoaXMuX29uUmVhY3Rpb25zQ3JlYXRlZCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJlYWN0aW9uczogdGhpcy5nZXRSZWFjdGlvbnMoKSxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBNZXNzYWdlVGltZXN0YW1wID0gc2RrLmdldENvbXBvbmVudCgnbWVzc2FnZXMuTWVzc2FnZVRpbWVzdGFtcCcpO1xyXG4gICAgICAgIGNvbnN0IFNlbmRlclByb2ZpbGUgPSBzZGsuZ2V0Q29tcG9uZW50KCdtZXNzYWdlcy5TZW5kZXJQcm9maWxlJyk7XHJcbiAgICAgICAgY29uc3QgTWVtYmVyQXZhdGFyID0gc2RrLmdldENvbXBvbmVudCgnYXZhdGFycy5NZW1iZXJBdmF0YXInKTtcclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oXCJFdmVudFRpbGUgc2hvd1VybFByZXZpZXcgZm9yICVzIGlzICVzXCIsIHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpLCB0aGlzLnByb3BzLnNob3dVcmxQcmV2aWV3KTtcclxuXHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IHRoaXMucHJvcHMubXhFdmVudC5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgY29uc3QgbXNndHlwZSA9IGNvbnRlbnQubXNndHlwZTtcclxuICAgICAgICBjb25zdCBldmVudFR5cGUgPSB0aGlzLnByb3BzLm14RXZlbnQuZ2V0VHlwZSgpO1xyXG5cclxuICAgICAgICAvLyBJbmZvIG1lc3NhZ2VzIGFyZSBiYXNpY2FsbHkgaW5mb3JtYXRpb24gYWJvdXQgY29tbWFuZHMgcHJvY2Vzc2VkIG9uIGEgcm9vbVxyXG4gICAgICAgIGNvbnN0IGlzQnViYmxlTWVzc2FnZSA9IGV2ZW50VHlwZS5zdGFydHNXaXRoKFwibS5rZXkudmVyaWZpY2F0aW9uXCIpIHx8XHJcbiAgICAgICAgICAgIChldmVudFR5cGUgPT09IFwibS5yb29tLm1lc3NhZ2VcIiAmJiBtc2d0eXBlICYmIG1zZ3R5cGUuc3RhcnRzV2l0aChcIm0ua2V5LnZlcmlmaWNhdGlvblwiKSkgfHxcclxuICAgICAgICAgICAgKGV2ZW50VHlwZSA9PT0gXCJtLnJvb20uZW5jcnlwdGlvblwiKTtcclxuICAgICAgICBsZXQgaXNJbmZvTWVzc2FnZSA9IChcclxuICAgICAgICAgICAgIWlzQnViYmxlTWVzc2FnZSAmJiBldmVudFR5cGUgIT09ICdtLnJvb20ubWVzc2FnZScgJiZcclxuICAgICAgICAgICAgZXZlbnRUeXBlICE9PSAnbS5zdGlja2VyJyAmJiBldmVudFR5cGUgIT09ICdtLnJvb20uY3JlYXRlJ1xyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGxldCB0aWxlSGFuZGxlciA9IGdldEhhbmRsZXJUaWxlKHRoaXMucHJvcHMubXhFdmVudCk7XHJcbiAgICAgICAgLy8gSWYgd2UncmUgc2hvd2luZyBoaWRkZW4gZXZlbnRzIGluIHRoZSB0aW1lbGluZSwgd2Ugc2hvdWxkIHVzZSB0aGVcclxuICAgICAgICAvLyBzb3VyY2UgdGlsZSB3aGVuIHRoZXJlJ3Mgbm8gcmVndWxhciB0aWxlIGZvciBhbiBldmVudCBhbmQgYWxzbyBmb3JcclxuICAgICAgICAvLyByZXBsYWNlIHJlbGF0aW9ucyAod2hpY2ggb3RoZXJ3aXNlIHdvdWxkIGRpc3BsYXkgYXMgYSBjb25mdXNpbmdcclxuICAgICAgICAvLyBkdXBsaWNhdGUgb2YgdGhlIHRoaW5nIHRoZXkgYXJlIHJlcGxhY2luZykuXHJcbiAgICAgICAgY29uc3QgdXNlU291cmNlID0gIXRpbGVIYW5kbGVyIHx8IHRoaXMucHJvcHMubXhFdmVudC5pc1JlbGF0aW9uKFwibS5yZXBsYWNlXCIpO1xyXG4gICAgICAgIGlmICh1c2VTb3VyY2UgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lXCIpKSB7XHJcbiAgICAgICAgICAgIHRpbGVIYW5kbGVyID0gXCJtZXNzYWdlcy5WaWV3U291cmNlRXZlbnRcIjtcclxuICAgICAgICAgICAgLy8gUmV1c2UgaW5mbyBtZXNzYWdlIGF2YXRhciBhbmQgc2VuZGVyIHByb2ZpbGUgc3R5bGluZ1xyXG4gICAgICAgICAgICBpc0luZm9NZXNzYWdlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gVGhpcyBzaG91bGRuJ3QgaGFwcGVuOiB0aGUgY2FsbGVyIHNob3VsZCBjaGVjayB3ZSBzdXBwb3J0IHRoaXMgdHlwZVxyXG4gICAgICAgIC8vIGJlZm9yZSB0cnlpbmcgdG8gaW5zdGFudGlhdGUgdXNcclxuICAgICAgICBpZiAoIXRpbGVIYW5kbGVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtteEV2ZW50fSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgRXZlbnQgdHlwZSBub3Qgc3VwcG9ydGVkOiB0eXBlOiR7bXhFdmVudC5nZXRUeXBlKCl9IGlzU3RhdGU6JHtteEV2ZW50LmlzU3RhdGUoKX1gKTtcclxuICAgICAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlIG14X0V2ZW50VGlsZV9pbmZvIG14X01Ob3RpY2VCb2R5XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9saW5lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnVGhpcyBldmVudCBjb3VsZCBub3QgYmUgZGlzcGxheWVkJykgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgRXZlbnRUaWxlVHlwZSA9IHNkay5nZXRDb21wb25lbnQodGlsZUhhbmRsZXIpO1xyXG5cclxuICAgICAgICBjb25zdCBpc1NlbmRpbmcgPSAoWydzZW5kaW5nJywgJ3F1ZXVlZCcsICdlbmNyeXB0aW5nJ10uaW5kZXhPZih0aGlzLnByb3BzLmV2ZW50U2VuZFN0YXR1cykgIT09IC0xKTtcclxuICAgICAgICBjb25zdCBpc1JlZGFjdGVkID0gaXNNZXNzYWdlRXZlbnQodGhpcy5wcm9wcy5teEV2ZW50KSAmJiB0aGlzLnByb3BzLmlzUmVkYWN0ZWQ7XHJcbiAgICAgICAgY29uc3QgaXNFbmNyeXB0aW9uRmFpbHVyZSA9IHRoaXMucHJvcHMubXhFdmVudC5pc0RlY3J5cHRpb25GYWlsdXJlKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGlzRWRpdGluZyA9ICEhdGhpcy5wcm9wcy5lZGl0U3RhdGU7XHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoe1xyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfYnViYmxlQ29udGFpbmVyOiBpc0J1YmJsZU1lc3NhZ2UsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZTogdHJ1ZSxcclxuICAgICAgICAgICAgbXhfRXZlbnRUaWxlX2lzRWRpdGluZzogaXNFZGl0aW5nLFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfaW5mbzogaXNJbmZvTWVzc2FnZSxcclxuICAgICAgICAgICAgbXhfRXZlbnRUaWxlXzEyaHI6IHRoaXMucHJvcHMuaXNUd2VsdmVIb3VyLFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfZW5jcnlwdGluZzogdGhpcy5wcm9wcy5ldmVudFNlbmRTdGF0dXMgPT09ICdlbmNyeXB0aW5nJyxcclxuICAgICAgICAgICAgbXhfRXZlbnRUaWxlX3NlbmRpbmc6ICFpc0VkaXRpbmcgJiYgaXNTZW5kaW5nLFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfbm90U2VudDogdGhpcy5wcm9wcy5ldmVudFNlbmRTdGF0dXMgPT09ICdub3Rfc2VudCcsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV9oaWdobGlnaHQ6IHRoaXMucHJvcHMudGlsZVNoYXBlID09PSAnbm90aWYnID8gZmFsc2UgOiB0aGlzLnNob3VsZEhpZ2hsaWdodCgpLFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfc2VsZWN0ZWQ6IHRoaXMucHJvcHMuaXNTZWxlY3RlZEV2ZW50LFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfY29udGludWF0aW9uOiB0aGlzLnByb3BzLnRpbGVTaGFwZSA/ICcnIDogdGhpcy5wcm9wcy5jb250aW51YXRpb24sXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV9sYXN0OiB0aGlzLnByb3BzLmxhc3QsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV9jb250ZXh0dWFsOiB0aGlzLnByb3BzLmNvbnRleHR1YWwsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV9hY3Rpb25CYXJGb2N1c2VkOiB0aGlzLnN0YXRlLmFjdGlvbkJhckZvY3VzZWQsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV92ZXJpZmllZDogIWlzQnViYmxlTWVzc2FnZSAmJiB0aGlzLnN0YXRlLnZlcmlmaWVkID09PSBFMkVfU1RBVEUuVkVSSUZJRUQsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV91bnZlcmlmaWVkOiAhaXNCdWJibGVNZXNzYWdlICYmIHRoaXMuc3RhdGUudmVyaWZpZWQgPT09IEUyRV9TVEFURS5XQVJOSU5HLFxyXG4gICAgICAgICAgICBteF9FdmVudFRpbGVfdW5rbm93bjogIWlzQnViYmxlTWVzc2FnZSAmJiB0aGlzLnN0YXRlLnZlcmlmaWVkID09PSBFMkVfU1RBVEUuVU5LTk9XTixcclxuICAgICAgICAgICAgbXhfRXZlbnRUaWxlX2JhZDogaXNFbmNyeXB0aW9uRmFpbHVyZSxcclxuICAgICAgICAgICAgbXhfRXZlbnRUaWxlX2Vtb3RlOiBtc2d0eXBlID09PSAnbS5lbW90ZScsXHJcbiAgICAgICAgICAgIG14X0V2ZW50VGlsZV9yZWRhY3RlZDogaXNSZWRhY3RlZCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHBlcm1hbGluayA9IFwiI1wiO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnBlcm1hbGlua0NyZWF0b3IpIHtcclxuICAgICAgICAgICAgcGVybWFsaW5rID0gdGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yLmZvckV2ZW50KHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHJlYWRBdmF0YXJzID0gdGhpcy5nZXRSZWFkQXZhdGFycygpO1xyXG5cclxuICAgICAgICBsZXQgYXZhdGFyO1xyXG4gICAgICAgIGxldCBzZW5kZXI7XHJcbiAgICAgICAgbGV0IGF2YXRhclNpemU7XHJcbiAgICAgICAgbGV0IG5lZWRzU2VuZGVyUHJvZmlsZTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMudGlsZVNoYXBlID09PSBcIm5vdGlmXCIpIHtcclxuICAgICAgICAgICAgYXZhdGFyU2l6ZSA9IDI0O1xyXG4gICAgICAgICAgICBuZWVkc1NlbmRlclByb2ZpbGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGlsZUhhbmRsZXIgPT09ICdtZXNzYWdlcy5Sb29tQ3JlYXRlJyB8fCBpc0J1YmJsZU1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgYXZhdGFyU2l6ZSA9IDA7XHJcbiAgICAgICAgICAgIG5lZWRzU2VuZGVyUHJvZmlsZSA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNJbmZvTWVzc2FnZSkge1xyXG4gICAgICAgICAgICAvLyBhIHNtYWxsIGF2YXRhciwgd2l0aCBubyBzZW5kZXIgcHJvZmlsZSwgZm9yXHJcbiAgICAgICAgICAgIC8vIGpvaW5zL3BhcnRzL2V0Y1xyXG4gICAgICAgICAgICBhdmF0YXJTaXplID0gMTQ7XHJcbiAgICAgICAgICAgIG5lZWRzU2VuZGVyUHJvZmlsZSA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5jb250aW51YXRpb24gJiYgdGhpcy5wcm9wcy50aWxlU2hhcGUgIT09IFwiZmlsZV9ncmlkXCIpIHtcclxuICAgICAgICAgICAgLy8gbm8gYXZhdGFyIG9yIHNlbmRlciBwcm9maWxlIGZvciBjb250aW51YXRpb24gbWVzc2FnZXNcclxuICAgICAgICAgICAgYXZhdGFyU2l6ZSA9IDA7XHJcbiAgICAgICAgICAgIG5lZWRzU2VuZGVyUHJvZmlsZSA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGF2YXRhclNpemUgPSAzMDtcclxuICAgICAgICAgICAgbmVlZHNTZW5kZXJQcm9maWxlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm14RXZlbnQuc2VuZGVyICYmIGF2YXRhclNpemUpIHtcclxuICAgICAgICAgICAgYXZhdGFyID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2F2YXRhclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8TWVtYmVyQXZhdGFyIG1lbWJlcj17dGhpcy5wcm9wcy5teEV2ZW50LnNlbmRlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPXthdmF0YXJTaXplfSBoZWlnaHQ9e2F2YXRhclNpemV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aWV3VXNlck9uQ2xpY2s9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAobmVlZHNTZW5kZXJQcm9maWxlKSB7XHJcbiAgICAgICAgICAgIGxldCB0ZXh0ID0gbnVsbDtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnByb3BzLnRpbGVTaGFwZSB8fCB0aGlzLnByb3BzLnRpbGVTaGFwZSA9PT0gJ3JlcGx5JyB8fCB0aGlzLnByb3BzLnRpbGVTaGFwZSA9PT0gJ3JlcGx5X3ByZXZpZXcnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobXNndHlwZSA9PT0gJ20uaW1hZ2UnKSB0ZXh0ID0gX3RkKCclKHNlbmRlck5hbWUpcyBzZW50IGFuIGltYWdlJyk7XHJcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChtc2d0eXBlID09PSAnbS52aWRlbycpIHRleHQgPSBfdGQoJyUoc2VuZGVyTmFtZSlzIHNlbnQgYSB2aWRlbycpO1xyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAobXNndHlwZSA9PT0gJ20uZmlsZScpIHRleHQgPSBfdGQoJyUoc2VuZGVyTmFtZSlzIHVwbG9hZGVkIGEgZmlsZScpO1xyXG4gICAgICAgICAgICAgICAgc2VuZGVyID0gPFNlbmRlclByb2ZpbGUgb25DbGljaz17dGhpcy5vblNlbmRlclByb2ZpbGVDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG14RXZlbnQ9e3RoaXMucHJvcHMubXhFdmVudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVuYWJsZUZsYWlyPXshdGV4dH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ9e3RleHR9IC8+O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgc2VuZGVyID0gPFNlbmRlclByb2ZpbGUgbXhFdmVudD17dGhpcy5wcm9wcy5teEV2ZW50fSBlbmFibGVGbGFpcj17dHJ1ZX0gLz47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IE1lc3NhZ2VBY3Rpb25CYXIgPSBzZGsuZ2V0Q29tcG9uZW50KCdtZXNzYWdlcy5NZXNzYWdlQWN0aW9uQmFyJyk7XHJcbiAgICAgICAgY29uc3QgYWN0aW9uQmFyID0gIWlzRWRpdGluZyA/IDxNZXNzYWdlQWN0aW9uQmFyXHJcbiAgICAgICAgICAgIG14RXZlbnQ9e3RoaXMucHJvcHMubXhFdmVudH1cclxuICAgICAgICAgICAgcmVhY3Rpb25zPXt0aGlzLnN0YXRlLnJlYWN0aW9uc31cclxuICAgICAgICAgICAgcGVybWFsaW5rQ3JlYXRvcj17dGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yfVxyXG4gICAgICAgICAgICBnZXRUaWxlPXt0aGlzLmdldFRpbGV9XHJcbiAgICAgICAgICAgIGdldFJlcGx5VGhyZWFkPXt0aGlzLmdldFJlcGx5VGhyZWFkfVxyXG4gICAgICAgICAgICBvbkZvY3VzQ2hhbmdlPXt0aGlzLm9uQWN0aW9uQmFyRm9jdXNDaGFuZ2V9XHJcbiAgICAgICAgLz4gOiB1bmRlZmluZWQ7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpbWVzdGFtcCA9IHRoaXMucHJvcHMubXhFdmVudC5nZXRUcygpID9cclxuICAgICAgICAgICAgPE1lc3NhZ2VUaW1lc3RhbXAgc2hvd1R3ZWx2ZUhvdXI9e3RoaXMucHJvcHMuaXNUd2VsdmVIb3VyfSB0cz17dGhpcy5wcm9wcy5teEV2ZW50LmdldFRzKCl9IC8+IDogbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3Qga2V5UmVxdWVzdEhlbHBUZXh0ID1cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfa2V5UmVxdWVzdEluZm9fdG9vbHRpcF9jb250ZW50c1wiPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnN0YXRlLnByZXZpb3VzbHlSZXF1ZXN0ZWRLZXlzID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgX3QoICdZb3VyIGtleSBzaGFyZSByZXF1ZXN0IGhhcyBiZWVuIHNlbnQgLSBwbGVhc2UgY2hlY2sgeW91ciBvdGhlciBzZXNzaW9ucyAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb3Iga2V5IHNoYXJlIHJlcXVlc3RzLicpIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3QoICdLZXkgc2hhcmUgcmVxdWVzdHMgYXJlIHNlbnQgdG8geW91ciBvdGhlciBzZXNzaW9ucyBhdXRvbWF0aWNhbGx5LiBJZiB5b3UgJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAncmVqZWN0ZWQgb3IgZGlzbWlzc2VkIHRoZSBrZXkgc2hhcmUgcmVxdWVzdCBvbiB5b3VyIG90aGVyIHNlc3Npb25zLCBjbGljayAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoZXJlIHRvIHJlcXVlc3QgdGhlIGtleXMgZm9yIHRoaXMgc2Vzc2lvbiBhZ2Fpbi4nKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoICdJZiB5b3VyIG90aGVyIHNlc3Npb25zIGRvIG5vdCBoYXZlIHRoZSBrZXkgZm9yIHRoaXMgbWVzc2FnZSB5b3Ugd2lsbCBub3QgJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmUgYWJsZSB0byBkZWNyeXB0IHRoZW0uJylcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICBjb25zdCBrZXlSZXF1ZXN0SW5mb0NvbnRlbnQgPSB0aGlzLnN0YXRlLnByZXZpb3VzbHlSZXF1ZXN0ZWRLZXlzID9cclxuICAgICAgICAgICAgX3QoJ0tleSByZXF1ZXN0IHNlbnQuJykgOlxyXG4gICAgICAgICAgICBfdChcclxuICAgICAgICAgICAgICAgICc8cmVxdWVzdExpbms+UmUtcmVxdWVzdCBlbmNyeXB0aW9uIGtleXM8L3JlcXVlc3RMaW5rPiBmcm9tIHlvdXIgb3RoZXIgc2Vzc2lvbnMuJyxcclxuICAgICAgICAgICAgICAgIHt9LFxyXG4gICAgICAgICAgICAgICAgeydyZXF1ZXN0TGluayc6IChzdWIpID0+IDxhIG9uQ2xpY2s9e3RoaXMub25SZXF1ZXN0S2V5c0NsaWNrfT57IHN1YiB9PC9hPn0sXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgIGNvbnN0IFRvb2x0aXBCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5Ub29sdGlwQnV0dG9uJyk7XHJcbiAgICAgICAgY29uc3Qga2V5UmVxdWVzdEluZm8gPSBpc0VuY3J5cHRpb25GYWlsdXJlID9cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfa2V5UmVxdWVzdEluZm9cIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9rZXlSZXF1ZXN0SW5mb190ZXh0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBrZXlSZXF1ZXN0SW5mb0NvbnRlbnQgfVxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPFRvb2x0aXBCdXR0b24gaGVscFRleHQ9e2tleVJlcXVlc3RIZWxwVGV4dH0gLz5cclxuICAgICAgICAgICAgPC9kaXY+IDogbnVsbDtcclxuXHJcbiAgICAgICAgbGV0IHJlYWN0aW9uc1JvdztcclxuICAgICAgICBpZiAoIWlzUmVkYWN0ZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgUmVhY3Rpb25zUm93ID0gc2RrLmdldENvbXBvbmVudCgnbWVzc2FnZXMuUmVhY3Rpb25zUm93Jyk7XHJcbiAgICAgICAgICAgIHJlYWN0aW9uc1JvdyA9IDxSZWFjdGlvbnNSb3dcclxuICAgICAgICAgICAgICAgIG14RXZlbnQ9e3RoaXMucHJvcHMubXhFdmVudH1cclxuICAgICAgICAgICAgICAgIHJlYWN0aW9ucz17dGhpcy5zdGF0ZS5yZWFjdGlvbnN9XHJcbiAgICAgICAgICAgIC8+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnByb3BzLnRpbGVTaGFwZSkge1xyXG4gICAgICAgICAgICBjYXNlICdub3RpZic6IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSB0aGlzLmNvbnRleHQuZ2V0Um9vbSh0aGlzLnByb3BzLm14RXZlbnQuZ2V0Um9vbUlkKCkpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX3Jvb21OYW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPXtwZXJtYWxpbmt9IG9uQ2xpY2s9e3RoaXMub25QZXJtYWxpbmtDbGlja2VkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHJvb20gPyByb29tLm5hbWUgOiAnJyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9zZW5kZXJEZXRhaWxzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGF2YXRhciB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPXtwZXJtYWxpbmt9IG9uQ2xpY2s9e3RoaXMub25QZXJtYWxpbmtDbGlja2VkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHNlbmRlciB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aW1lc3RhbXAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FdmVudFRpbGVfbGluZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEV2ZW50VGlsZVR5cGUgcmVmPXt0aGlzLl90aWxlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbXhFdmVudD17dGhpcy5wcm9wcy5teEV2ZW50fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0cz17dGhpcy5wcm9wcy5oaWdobGlnaHRzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0TGluaz17dGhpcy5wcm9wcy5oaWdobGlnaHRMaW5rfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvd1VybFByZXZpZXc9e3RoaXMucHJvcHMuc2hvd1VybFByZXZpZXd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9e3RoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAnZmlsZV9ncmlkJzoge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2xpbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxFdmVudFRpbGVUeXBlIHJlZj17dGhpcy5fdGlsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG14RXZlbnQ9e3RoaXMucHJvcHMubXhFdmVudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodHM9e3RoaXMucHJvcHMuaGlnaGxpZ2h0c31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodExpbms9e3RoaXMucHJvcHMuaGlnaGxpZ2h0TGlua31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dVcmxQcmV2aWV3PXt0aGlzLnByb3BzLnNob3dVcmxQcmV2aWV3fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGlsZVNoYXBlPXt0aGlzLnByb3BzLnRpbGVTaGFwZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uSGVpZ2h0Q2hhbmdlZD17dGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX3NlbmRlckRldGFpbHNMaW5rXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9e3Blcm1hbGlua31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25QZXJtYWxpbmtDbGlja2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0V2ZW50VGlsZV9zZW5kZXJEZXRhaWxzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBzZW5kZXIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgdGltZXN0YW1wIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjYXNlICdyZXBseSc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3JlcGx5X3ByZXZpZXcnOiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdGhyZWFkO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMudGlsZVNoYXBlID09PSAncmVwbHlfcHJldmlldycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aHJlYWQgPSBSZXBseVRocmVhZC5tYWtlVGhyZWFkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm14RXZlbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnBlcm1hbGlua0NyZWF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3JlcGx5VGhyZWFkLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBhdmF0YXIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHNlbmRlciB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX3JlcGx5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPXtwZXJtYWxpbmt9IG9uQ2xpY2s9e3RoaXMub25QZXJtYWxpbmtDbGlja2VkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHRpbWVzdGFtcCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ICFpc0J1YmJsZU1lc3NhZ2UgJiYgdGhpcy5fcmVuZGVyRTJFUGFkbG9jaygpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgdGhyZWFkIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxFdmVudFRpbGVUeXBlIHJlZj17dGhpcy5fdGlsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG14RXZlbnQ9e3RoaXMucHJvcHMubXhFdmVudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodHM9e3RoaXMucHJvcHMuaGlnaGxpZ2h0c31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZ2hsaWdodExpbms9e3RoaXMucHJvcHMuaGlnaGxpZ2h0TGlua31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uSGVpZ2h0Q2hhbmdlZD17dGhpcy5wcm9wcy5vbkhlaWdodENoYW5nZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93VXJsUHJldmlldz17ZmFsc2V9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkZWZhdWx0OiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0aHJlYWQgPSBSZXBseVRocmVhZC5tYWtlVGhyZWFkKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMubXhFdmVudCxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uSGVpZ2h0Q2hhbmdlZCxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnBlcm1hbGlua0NyZWF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcmVwbHlUaHJlYWQsXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgLy8gdGFiLWluZGV4PS0xIHRvIGFsbG93IGl0IHRvIGJlIGZvY3VzYWJsZSBidXQgZG8gbm90IGFkZCB0YWIgc3RvcCBmb3IgaXQsIHByaW1hcmlseSBmb3Igc2NyZWVuIHJlYWRlcnNcclxuICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9IHRhYkluZGV4PXstMX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX21zZ09wdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyByZWFkQXZhdGFycyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHNlbmRlciB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2xpbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZj17cGVybWFsaW5rfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25QZXJtYWxpbmtDbGlja2VkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9e2Zvcm1hdFRpbWUobmV3IERhdGUodGhpcy5wcm9wcy5teEV2ZW50LmdldFRzKCkpLCB0aGlzLnByb3BzLmlzVHdlbHZlSG91cil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0aW1lc3RhbXAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyAhaXNCdWJibGVNZXNzYWdlICYmIHRoaXMuX3JlbmRlckUyRVBhZGxvY2soKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHRocmVhZCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RXZlbnRUaWxlVHlwZSByZWY9e3RoaXMuX3RpbGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBteEV2ZW50PXt0aGlzLnByb3BzLm14RXZlbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNpbmdFdmVudElkPXt0aGlzLnByb3BzLnJlcGxhY2luZ0V2ZW50SWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlZGl0U3RhdGU9e3RoaXMucHJvcHMuZWRpdFN0YXRlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0cz17dGhpcy5wcm9wcy5oaWdobGlnaHRzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlnaGxpZ2h0TGluaz17dGhpcy5wcm9wcy5oaWdobGlnaHRMaW5rfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvd1VybFByZXZpZXc9e3RoaXMucHJvcHMuc2hvd1VybFByZXZpZXd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkhlaWdodENoYW5nZWQ9e3RoaXMucHJvcHMub25IZWlnaHRDaGFuZ2VkfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBrZXlSZXF1ZXN0SW5mbyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHJlYWN0aW9uc1JvdyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGFjdGlvbkJhciB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGUgYXZhdGFyIGdvZXMgYWZ0ZXIgdGhlIGV2ZW50IHRpbGUgYXMgaXQncyBhYnNvbHV0ZWx5IHBvc2l0aW9uZWQgdG8gYmUgb3ZlciB0aGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGV2ZW50IHRpbGUgbGluZSwgc28gbmVlZHMgdG8gYmUgbGF0ZXIgaW4gdGhlIERPTSBzbyBpdCBhcHBlYXJzIG9uIHRvcCAodGhpcyBhdm9pZHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSBuZWVkIGZvciBmdXJ0aGVyIHotaW5kZXhpbmcgY2hhb3MpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBhdmF0YXIgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbn0pO1xyXG5cclxuLy8gWFhYIHRoaXMnbGwgZXZlbnR1YWxseSBiZSBkeW5hbWljIGJhc2VkIG9uIHRoZSBmaWVsZHMgb25jZSB3ZSBoYXZlIGV4dGVuc2libGUgZXZlbnQgdHlwZXNcclxuY29uc3QgbWVzc2FnZVR5cGVzID0gWydtLnJvb20ubWVzc2FnZScsICdtLnN0aWNrZXInXTtcclxuZnVuY3Rpb24gaXNNZXNzYWdlRXZlbnQoZXYpIHtcclxuICAgIHJldHVybiAobWVzc2FnZVR5cGVzLmluY2x1ZGVzKGV2LmdldFR5cGUoKSkpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaGF2ZVRpbGVGb3JFdmVudChlKSB7XHJcbiAgICAvLyBPbmx5IG1lc3NhZ2VzIGhhdmUgYSB0aWxlIChibGFjay1yZWN0YW5nbGUpIGlmIHJlZGFjdGVkXHJcbiAgICBpZiAoZS5pc1JlZGFjdGVkKCkgJiYgIWlzTWVzc2FnZUV2ZW50KGUpKSByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgLy8gTm8gdGlsZSBmb3IgcmVwbGFjZW1lbnQgZXZlbnRzIHNpbmNlIHRoZXkgdXBkYXRlIHRoZSBvcmlnaW5hbCB0aWxlXHJcbiAgICBpZiAoZS5pc1JlbGF0aW9uKFwibS5yZXBsYWNlXCIpKSByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgY29uc3QgaGFuZGxlciA9IGdldEhhbmRsZXJUaWxlKGUpO1xyXG4gICAgaWYgKGhhbmRsZXIgPT09IHVuZGVmaW5lZCkgcmV0dXJuIGZhbHNlO1xyXG4gICAgaWYgKGhhbmRsZXIgPT09ICdtZXNzYWdlcy5UZXh0dWFsRXZlbnQnKSB7XHJcbiAgICAgICAgcmV0dXJuIFRleHRGb3JFdmVudC50ZXh0Rm9yRXZlbnQoZSkgIT09ICcnO1xyXG4gICAgfSBlbHNlIGlmIChoYW5kbGVyID09PSAnbWVzc2FnZXMuUm9vbUNyZWF0ZScpIHtcclxuICAgICAgICByZXR1cm4gQm9vbGVhbihlLmdldENvbnRlbnQoKVsncHJlZGVjZXNzb3InXSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBFMmVQYWRsb2NrVW5kZWNyeXB0YWJsZShwcm9wcykge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8RTJlUGFkbG9jayB0aXRsZT17X3QoXCJUaGlzIG1lc3NhZ2UgY2Fubm90IGJlIGRlY3J5cHRlZFwiKX0gaWNvbj1cInVuZGVjcnlwdGFibGVcIiB7Li4ucHJvcHN9IC8+XHJcbiAgICApO1xyXG59XHJcblxyXG5mdW5jdGlvbiBFMmVQYWRsb2NrVW52ZXJpZmllZChwcm9wcykge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8RTJlUGFkbG9jayB0aXRsZT17X3QoXCJFbmNyeXB0ZWQgYnkgYW4gdW52ZXJpZmllZCBzZXNzaW9uXCIpfSBpY29uPVwidW52ZXJpZmllZFwiIHsuLi5wcm9wc30gLz5cclxuICAgICk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEUyZVBhZGxvY2tVbmVuY3J5cHRlZChwcm9wcykge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8RTJlUGFkbG9jayB0aXRsZT17X3QoXCJVbmVuY3J5cHRlZFwiKX0gaWNvbj1cInVuZW5jcnlwdGVkXCIgey4uLnByb3BzfSAvPlxyXG4gICAgKTtcclxufVxyXG5cclxuZnVuY3Rpb24gRTJlUGFkbG9ja1Vua25vd24ocHJvcHMpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPEUyZVBhZGxvY2sgdGl0bGU9e190KFwiRW5jcnlwdGVkIGJ5IGEgZGVsZXRlZCBzZXNzaW9uXCIpfSBpY29uPVwidW5rbm93blwiIHsuLi5wcm9wc30gLz5cclxuICAgICk7XHJcbn1cclxuXHJcbmNsYXNzIEUyZVBhZGxvY2sgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBpY29uOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgaG92ZXI6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgb25Ib3ZlclN0YXJ0ID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2hvdmVyOiB0cnVlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uSG92ZXJFbmQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aG92ZXI6IGZhbHNlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBsZXQgdG9vbHRpcCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuaG92ZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgVG9vbHRpcCA9IHNkay5nZXRDb21wb25lbnQoXCJlbGVtZW50cy5Ub29sdGlwXCIpO1xyXG4gICAgICAgICAgICB0b29sdGlwID0gPFRvb2x0aXAgY2xhc3NOYW1lPVwibXhfRXZlbnRUaWxlX2UyZUljb25fdG9vbHRpcFwiIGxhYmVsPXt0aGlzLnByb3BzLnRpdGxlfSBkaXI9XCJhdXRvXCIgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY2xhc3NlcyA9IGBteF9FdmVudFRpbGVfZTJlSWNvbiBteF9FdmVudFRpbGVfZTJlSWNvbl8ke3RoaXMucHJvcHMuaWNvbn1gO1xyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImFsd2F5c1Nob3dFbmNyeXB0aW9uSWNvbnNcIikpIHtcclxuICAgICAgICAgICAgY2xhc3NlcyArPSAnIG14X0V2ZW50VGlsZV9lMmVJY29uX2hpZGRlbic7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXN9XHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Ib3ZlclN0YXJ0fVxyXG4gICAgICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uSG92ZXJFbmR9XHJcbiAgICAgICAgICAgID57dG9vbHRpcH08L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==