"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _SettingsStore = _interopRequireWildcard(require("../../../settings/SettingsStore"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _RoomAvatar = _interopRequireDefault(require("../avatars/RoomAvatar"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Analytics = _interopRequireDefault(require("../../../Analytics"));

var RoomNotifs = _interopRequireWildcard(require("../../../RoomNotifs"));

var FormattingUtils = _interopRequireWildcard(require("../../../utils/FormattingUtils"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var _languageHandler = require("../../../languageHandler");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

const MAX_ROOMS = 20;
const MIN_ROOMS_BEFORE_ENABLED = 10; // The threshold time in milliseconds to wait for an autojoined room to show up.

const AUTOJOIN_WAIT_THRESHOLD_MS = 90000; // 90 seconds

class RoomBreadcrumbs extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "onMyMembership", (room, membership) => {
      if (membership === "leave" || membership === "ban") {
        const rooms = this.state.rooms.slice();
        const roomState = rooms.find(r => r.room.roomId === room.roomId);

        if (roomState) {
          roomState.left = true;
          this.setState({
            rooms
          });
        }
      }

      this.onRoomMembershipChanged();
    });
    (0, _defineProperty2.default)(this, "onRoomReceipt", (event, room) => {
      if (this.state.rooms.map(r => r.room.roomId).includes(room.roomId)) {
        this._calculateRoomBadges(room);
      }
    });
    (0, _defineProperty2.default)(this, "onRoomTimeline", (event, room) => {
      if (!room) return; // Can be null for the notification timeline, etc.

      if (this.state.rooms.map(r => r.room.roomId).includes(room.roomId)) {
        this._calculateRoomBadges(room);
      }
    });
    (0, _defineProperty2.default)(this, "onEventDecrypted", event => {
      if (this.state.rooms.map(r => r.room.roomId).includes(event.getRoomId())) {
        this._calculateRoomBadges(_MatrixClientPeg.MatrixClientPeg.get().getRoom(event.getRoomId()));
      }
    });
    (0, _defineProperty2.default)(this, "onBreadcrumbsChanged", (settingName, roomId, level, valueAtLevel, value) => {
      if (!value) return;
      const currentState = this.state.rooms.map(r => r.room.roomId);

      if (currentState.length === value.length) {
        let changed = false;

        for (let i = 0; i < currentState.length; i++) {
          if (currentState[i] !== value[i]) {
            changed = true;
            break;
          }
        }

        if (!changed) return;
      }

      this._loadRoomIds(value);
    });
    (0, _defineProperty2.default)(this, "onRoomMembershipChanged", () => {
      if (!this.state.enabled && this._shouldEnable()) {
        this.setState({
          enabled: true
        });
      }
    });
    (0, _defineProperty2.default)(this, "onRoom", room => {
      // Always check for membership changes when we see new rooms
      this.onRoomMembershipChanged();

      const waitingRoom = this._waitingRoomQueue.find(r => r.roomId === room.roomId);

      if (!waitingRoom) return;

      this._waitingRoomQueue.splice(this._waitingRoomQueue.indexOf(waitingRoom), 1);

      const now = new Date().getTime();
      if (now - waitingRoom.addedTs > AUTOJOIN_WAIT_THRESHOLD_MS) return; // Too long ago.

      this._appendRoomId(room.roomId); // add the room we've been waiting for

    });
    this.state = {
      rooms: [],
      enabled: false
    };
    this.onAction = this.onAction.bind(this);
    this._dispatcherRef = null; // The room IDs we're waiting to come down the Room handler and when we
    // started waiting for them. Used to track a room over an upgrade/autojoin.

    this._waitingRoomQueue = [
      /* { roomId, addedTs } */
    ];
    this._scroller = (0, _react.createRef)();
  } // TODO: [REACT-WARNING] Move this to constructor


  UNSAFE_componentWillMount() {
    // eslint-disable-line camelcase
    this._dispatcherRef = _dispatcher.default.register(this.onAction);

    const storedRooms = _SettingsStore.default.getValue("breadcrumb_rooms");

    this._loadRoomIds(storedRooms || []);

    this._settingWatchRef = _SettingsStore.default.watchSetting("breadcrumb_rooms", null, this.onBreadcrumbsChanged);
    this.setState({
      enabled: this._shouldEnable()
    });

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.myMembership", this.onMyMembership);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.receipt", this.onRoomReceipt);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room.timeline", this.onRoomTimeline);

    _MatrixClientPeg.MatrixClientPeg.get().on("Event.decrypted", this.onEventDecrypted);

    _MatrixClientPeg.MatrixClientPeg.get().on("Room", this.onRoom);
  }

  componentWillUnmount() {
    _dispatcher.default.unregister(this._dispatcherRef);

    _SettingsStore.default.unwatchSetting(this._settingWatchRef);

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client) {
      client.removeListener("Room.myMembership", this.onMyMembership);
      client.removeListener("Room.receipt", this.onRoomReceipt);
      client.removeListener("Room.timeline", this.onRoomTimeline);
      client.removeListener("Event.decrypted", this.onEventDecrypted);
      client.removeListener("Room", this.onRoom);
    }
  }

  componentDidUpdate() {
    const rooms = this.state.rooms.slice();

    if (rooms.length) {
      const roomModel = rooms[0];

      if (!roomModel.animated) {
        roomModel.animated = true;
        setTimeout(() => this.setState({
          rooms
        }), 0);
      }
    }
  }

  onAction(payload) {
    switch (payload.action) {
      case 'view_room':
        if (payload.auto_join && !_MatrixClientPeg.MatrixClientPeg.get().getRoom(payload.room_id)) {
          // Queue the room instead of pushing it immediately - we're probably just waiting
          // for a join to complete (ie: joining the upgraded room).
          this._waitingRoomQueue.push({
            roomId: payload.room_id,
            addedTs: new Date().getTime()
          });

          break;
        }

        this._appendRoomId(payload.room_id);

        break;
      // XXX: slight hack in order to zero the notification count when a room
      // is read. Copied from RoomTile

      case 'on_room_read':
        {
          const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(payload.roomId);

          this._calculateRoomBadges(room,
          /*zero=*/
          true);

          break;
        }
    }
  }

  _shouldEnable() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const joinedRoomCount = client.getRooms().reduce((count, r) => {
      return count + (r.getMyMembership() === "join" ? 1 : 0);
    }, 0);
    return joinedRoomCount >= MIN_ROOMS_BEFORE_ENABLED;
  }

  _loadRoomIds(roomIds) {
    if (!roomIds || roomIds.length <= 0) return; // Skip updates with no rooms
    // If we're here, the list changed.

    const rooms = roomIds.map(r => _MatrixClientPeg.MatrixClientPeg.get().getRoom(r)).filter(r => r).map(r => {
      const badges = this._calculateBadgesForRoom(r) || {};
      return _objectSpread({
        room: r,
        animated: false
      }, badges);
    });
    this.setState({
      rooms: rooms
    });
  }

  _calculateBadgesForRoom(room, zero = false) {
    if (!room) return null; // Reset the notification variables for simplicity

    const roomModel = {
      redBadge: false,
      formattedCount: "0",
      showCount: false
    };
    if (zero) return roomModel;
    const notifState = RoomNotifs.getRoomNotifsState(room.roomId);

    if (RoomNotifs.MENTION_BADGE_STATES.includes(notifState)) {
      const highlightNotifs = RoomNotifs.getUnreadNotificationCount(room, 'highlight');
      const unreadNotifs = RoomNotifs.getUnreadNotificationCount(room);
      const redBadge = highlightNotifs > 0;
      const greyBadge = redBadge || unreadNotifs > 0 && RoomNotifs.BADGE_STATES.includes(notifState);

      if (redBadge || greyBadge) {
        const notifCount = redBadge ? highlightNotifs : unreadNotifs;
        const limitedCount = FormattingUtils.formatCount(notifCount);
        roomModel.redBadge = redBadge;
        roomModel.formattedCount = limitedCount;
        roomModel.showCount = true;
      }
    }

    return roomModel;
  }

  _calculateRoomBadges(room, zero = false) {
    if (!room) return;
    const rooms = this.state.rooms.slice();
    const roomModel = rooms.find(r => r.room.roomId === room.roomId);
    if (!roomModel) return; // No applicable room, so don't do math on it

    const badges = this._calculateBadgesForRoom(room, zero);

    if (!badges) return; // No badges for some reason

    Object.assign(roomModel, badges);
    this.setState({
      rooms
    });
  }

  _appendRoomId(roomId) {
    let room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

    if (!room) return;
    const rooms = this.state.rooms.slice(); // If the room is upgraded, use that room instead. We'll also splice out
    // any children of the room.

    const history = _MatrixClientPeg.MatrixClientPeg.get().getRoomUpgradeHistory(roomId);

    if (history.length > 1) {
      room = history[history.length - 1]; // Last room is most recent
      // Take out any room that isn't the most recent room

      for (let i = 0; i < history.length - 1; i++) {
        const idx = rooms.findIndex(r => r.room.roomId === history[i].roomId);
        if (idx !== -1) rooms.splice(idx, 1);
      }
    }

    const existingIdx = rooms.findIndex(r => r.room.roomId === room.roomId);

    if (existingIdx !== -1) {
      rooms.splice(existingIdx, 1);
    }

    rooms.splice(0, 0, {
      room,
      animated: false
    });

    if (rooms.length > MAX_ROOMS) {
      rooms.splice(MAX_ROOMS, rooms.length - MAX_ROOMS);
    }

    this.setState({
      rooms
    });

    if (this._scroller.current) {
      this._scroller.current.moveToOrigin();
    } // We don't track room aesthetics (badges, membership, etc) over the wire so we
    // don't need to do this elsewhere in the file. Just where we alter the room IDs
    // and their order.


    const roomIds = rooms.map(r => r.room.roomId);

    if (roomIds.length > 0) {
      _SettingsStore.default.setValue("breadcrumb_rooms", null, _SettingsStore.SettingLevel.ACCOUNT, roomIds);
    }
  }

  _viewRoom(room, index) {
    _Analytics.default.trackEvent("Breadcrumbs", "click_node", index);

    _dispatcher.default.dispatch({
      action: "view_room",
      room_id: room.roomId
    });
  }

  _onMouseEnter(room) {
    this._onHover(room);
  }

  _onMouseLeave(room) {
    this._onHover(null); // clear hover states

  }

  _onHover(room) {
    const rooms = this.state.rooms.slice();

    for (const r of rooms) {
      r.hover = room && r.room.roomId === room.roomId;
    }

    this.setState({
      rooms
    });
  }

  _isDmRoom(room) {
    const dmRooms = _DMRoomMap.default.shared().getUserIdForRoomId(room.roomId);

    return Boolean(dmRooms);
  }

  render() {
    const Tooltip = sdk.getComponent('elements.Tooltip');
    const IndicatorScrollbar = sdk.getComponent('structures.IndicatorScrollbar'); // check for collapsed here and not at parent so we keep rooms in our state
    // when collapsing and expanding

    if (this.props.collapsed || !this.state.enabled) {
      return null;
    }

    const rooms = this.state.rooms;
    const avatars = rooms.map((r, i) => {
      const isFirst = i === 0;
      const classes = (0, _classnames.default)({
        "mx_RoomBreadcrumbs_crumb": true,
        "mx_RoomBreadcrumbs_preAnimate": isFirst && !r.animated,
        "mx_RoomBreadcrumbs_animate": isFirst,
        "mx_RoomBreadcrumbs_left": r.left
      });
      let tooltip = null;

      if (r.hover) {
        tooltip = _react.default.createElement(Tooltip, {
          label: r.room.name
        });
      }

      let badge;

      if (r.showCount) {
        const badgeClasses = (0, _classnames.default)({
          'mx_RoomTile_badge': true,
          'mx_RoomTile_badgeButton': true,
          'mx_RoomTile_badgeRed': r.redBadge,
          'mx_RoomTile_badgeUnread': !r.redBadge
        });
        badge = _react.default.createElement("div", {
          className: badgeClasses
        }, r.formattedCount);
      }

      let dmIndicator;

      if (this._isDmRoom(r.room) && !_SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
        dmIndicator = _react.default.createElement("img", {
          src: require("../../../../res/img/icon_person.svg"),
          className: "mx_RoomBreadcrumbs_dmIndicator",
          width: "13",
          height: "15",
          alt: (0, _languageHandler._t)("Direct Chat")
        });
      }

      return _react.default.createElement(_AccessibleButton.default, {
        className: classes,
        key: r.room.roomId,
        onClick: () => this._viewRoom(r.room, i),
        onMouseEnter: () => this._onMouseEnter(r.room),
        onMouseLeave: () => this._onMouseLeave(r.room),
        "aria-label": (0, _languageHandler._t)("Room %(name)s", {
          name: r.room.name
        })
      }, _react.default.createElement(_RoomAvatar.default, {
        room: r.room,
        width: 32,
        height: 32
      }), badge, dmIndicator, tooltip);
    });
    return _react.default.createElement("div", {
      role: "toolbar",
      "aria-label": (0, _languageHandler._t)("Recent rooms")
    }, _react.default.createElement(IndicatorScrollbar, {
      ref: this._scroller,
      className: "mx_RoomBreadcrumbs",
      trackHorizontalOverflow: true,
      verticalScrollsHorizontally: true
    }, avatars));
  }

}

exports.default = RoomBreadcrumbs;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21CcmVhZGNydW1icy5qcyJdLCJuYW1lcyI6WyJNQVhfUk9PTVMiLCJNSU5fUk9PTVNfQkVGT1JFX0VOQUJMRUQiLCJBVVRPSk9JTl9XQUlUX1RIUkVTSE9MRF9NUyIsIlJvb21CcmVhZGNydW1icyIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInJvb20iLCJtZW1iZXJzaGlwIiwicm9vbXMiLCJzdGF0ZSIsInNsaWNlIiwicm9vbVN0YXRlIiwiZmluZCIsInIiLCJyb29tSWQiLCJsZWZ0Iiwic2V0U3RhdGUiLCJvblJvb21NZW1iZXJzaGlwQ2hhbmdlZCIsImV2ZW50IiwibWFwIiwiaW5jbHVkZXMiLCJfY2FsY3VsYXRlUm9vbUJhZGdlcyIsImdldFJvb21JZCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFJvb20iLCJzZXR0aW5nTmFtZSIsImxldmVsIiwidmFsdWVBdExldmVsIiwidmFsdWUiLCJjdXJyZW50U3RhdGUiLCJsZW5ndGgiLCJjaGFuZ2VkIiwiaSIsIl9sb2FkUm9vbUlkcyIsImVuYWJsZWQiLCJfc2hvdWxkRW5hYmxlIiwid2FpdGluZ1Jvb20iLCJfd2FpdGluZ1Jvb21RdWV1ZSIsInNwbGljZSIsImluZGV4T2YiLCJub3ciLCJEYXRlIiwiZ2V0VGltZSIsImFkZGVkVHMiLCJfYXBwZW5kUm9vbUlkIiwib25BY3Rpb24iLCJiaW5kIiwiX2Rpc3BhdGNoZXJSZWYiLCJfc2Nyb2xsZXIiLCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50IiwiZGlzIiwicmVnaXN0ZXIiLCJzdG9yZWRSb29tcyIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsIl9zZXR0aW5nV2F0Y2hSZWYiLCJ3YXRjaFNldHRpbmciLCJvbkJyZWFkY3J1bWJzQ2hhbmdlZCIsIm9uIiwib25NeU1lbWJlcnNoaXAiLCJvblJvb21SZWNlaXB0Iiwib25Sb29tVGltZWxpbmUiLCJvbkV2ZW50RGVjcnlwdGVkIiwib25Sb29tIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1bnJlZ2lzdGVyIiwidW53YXRjaFNldHRpbmciLCJjbGllbnQiLCJyZW1vdmVMaXN0ZW5lciIsImNvbXBvbmVudERpZFVwZGF0ZSIsInJvb21Nb2RlbCIsImFuaW1hdGVkIiwic2V0VGltZW91dCIsInBheWxvYWQiLCJhY3Rpb24iLCJhdXRvX2pvaW4iLCJyb29tX2lkIiwicHVzaCIsImpvaW5lZFJvb21Db3VudCIsImdldFJvb21zIiwicmVkdWNlIiwiY291bnQiLCJnZXRNeU1lbWJlcnNoaXAiLCJyb29tSWRzIiwiZmlsdGVyIiwiYmFkZ2VzIiwiX2NhbGN1bGF0ZUJhZGdlc0ZvclJvb20iLCJ6ZXJvIiwicmVkQmFkZ2UiLCJmb3JtYXR0ZWRDb3VudCIsInNob3dDb3VudCIsIm5vdGlmU3RhdGUiLCJSb29tTm90aWZzIiwiZ2V0Um9vbU5vdGlmc1N0YXRlIiwiTUVOVElPTl9CQURHRV9TVEFURVMiLCJoaWdobGlnaHROb3RpZnMiLCJnZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCIsInVucmVhZE5vdGlmcyIsImdyZXlCYWRnZSIsIkJBREdFX1NUQVRFUyIsIm5vdGlmQ291bnQiLCJsaW1pdGVkQ291bnQiLCJGb3JtYXR0aW5nVXRpbHMiLCJmb3JtYXRDb3VudCIsIk9iamVjdCIsImFzc2lnbiIsImhpc3RvcnkiLCJnZXRSb29tVXBncmFkZUhpc3RvcnkiLCJpZHgiLCJmaW5kSW5kZXgiLCJleGlzdGluZ0lkeCIsImN1cnJlbnQiLCJtb3ZlVG9PcmlnaW4iLCJzZXRWYWx1ZSIsIlNldHRpbmdMZXZlbCIsIkFDQ09VTlQiLCJfdmlld1Jvb20iLCJpbmRleCIsIkFuYWx5dGljcyIsInRyYWNrRXZlbnQiLCJkaXNwYXRjaCIsIl9vbk1vdXNlRW50ZXIiLCJfb25Ib3ZlciIsIl9vbk1vdXNlTGVhdmUiLCJob3ZlciIsIl9pc0RtUm9vbSIsImRtUm9vbXMiLCJETVJvb21NYXAiLCJzaGFyZWQiLCJnZXRVc2VySWRGb3JSb29tSWQiLCJCb29sZWFuIiwicmVuZGVyIiwiVG9vbHRpcCIsInNkayIsImdldENvbXBvbmVudCIsIkluZGljYXRvclNjcm9sbGJhciIsImNvbGxhcHNlZCIsImF2YXRhcnMiLCJpc0ZpcnN0IiwiY2xhc3NlcyIsInRvb2x0aXAiLCJuYW1lIiwiYmFkZ2UiLCJiYWRnZUNsYXNzZXMiLCJkbUluZGljYXRvciIsImlzRmVhdHVyZUVuYWJsZWQiLCJyZXF1aXJlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxNQUFNQSxTQUFTLEdBQUcsRUFBbEI7QUFDQSxNQUFNQyx3QkFBd0IsR0FBRyxFQUFqQyxDLENBRUE7O0FBQ0EsTUFBTUMsMEJBQTBCLEdBQUcsS0FBbkMsQyxDQUEwQzs7QUFFM0IsTUFBTUMsZUFBTixTQUE4QkMsZUFBTUMsU0FBcEMsQ0FBOEM7QUFDekRDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLDBEQWlGRixDQUFDQyxJQUFELEVBQU9DLFVBQVAsS0FBc0I7QUFDbkMsVUFBSUEsVUFBVSxLQUFLLE9BQWYsSUFBMEJBLFVBQVUsS0FBSyxLQUE3QyxFQUFvRDtBQUNoRCxjQUFNQyxLQUFLLEdBQUcsS0FBS0MsS0FBTCxDQUFXRCxLQUFYLENBQWlCRSxLQUFqQixFQUFkO0FBQ0EsY0FBTUMsU0FBUyxHQUFHSCxLQUFLLENBQUNJLElBQU4sQ0FBWUMsQ0FBRCxJQUFPQSxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBUCxLQUFrQlIsSUFBSSxDQUFDUSxNQUF6QyxDQUFsQjs7QUFDQSxZQUFJSCxTQUFKLEVBQWU7QUFDWEEsVUFBQUEsU0FBUyxDQUFDSSxJQUFWLEdBQWlCLElBQWpCO0FBQ0EsZUFBS0MsUUFBTCxDQUFjO0FBQUNSLFlBQUFBO0FBQUQsV0FBZDtBQUNIO0FBQ0o7O0FBQ0QsV0FBS1MsdUJBQUw7QUFDSCxLQTNGa0I7QUFBQSx5REE2RkgsQ0FBQ0MsS0FBRCxFQUFRWixJQUFSLEtBQWlCO0FBQzdCLFVBQUksS0FBS0csS0FBTCxDQUFXRCxLQUFYLENBQWlCVyxHQUFqQixDQUFxQk4sQ0FBQyxJQUFJQSxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBakMsRUFBeUNNLFFBQXpDLENBQWtEZCxJQUFJLENBQUNRLE1BQXZELENBQUosRUFBb0U7QUFDaEUsYUFBS08sb0JBQUwsQ0FBMEJmLElBQTFCO0FBQ0g7QUFDSixLQWpHa0I7QUFBQSwwREFtR0YsQ0FBQ1ksS0FBRCxFQUFRWixJQUFSLEtBQWlCO0FBQzlCLFVBQUksQ0FBQ0EsSUFBTCxFQUFXLE9BRG1CLENBQ1g7O0FBQ25CLFVBQUksS0FBS0csS0FBTCxDQUFXRCxLQUFYLENBQWlCVyxHQUFqQixDQUFxQk4sQ0FBQyxJQUFJQSxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBakMsRUFBeUNNLFFBQXpDLENBQWtEZCxJQUFJLENBQUNRLE1BQXZELENBQUosRUFBb0U7QUFDaEUsYUFBS08sb0JBQUwsQ0FBMEJmLElBQTFCO0FBQ0g7QUFDSixLQXhHa0I7QUFBQSw0REEwR0NZLEtBQUQsSUFBVztBQUMxQixVQUFJLEtBQUtULEtBQUwsQ0FBV0QsS0FBWCxDQUFpQlcsR0FBakIsQ0FBcUJOLENBQUMsSUFBSUEsQ0FBQyxDQUFDUCxJQUFGLENBQU9RLE1BQWpDLEVBQXlDTSxRQUF6QyxDQUFrREYsS0FBSyxDQUFDSSxTQUFOLEVBQWxELENBQUosRUFBMEU7QUFDdEUsYUFBS0Qsb0JBQUwsQ0FBMEJFLGlDQUFnQkMsR0FBaEIsR0FBc0JDLE9BQXRCLENBQThCUCxLQUFLLENBQUNJLFNBQU4sRUFBOUIsQ0FBMUI7QUFDSDtBQUNKLEtBOUdrQjtBQUFBLGdFQWdISSxDQUFDSSxXQUFELEVBQWNaLE1BQWQsRUFBc0JhLEtBQXRCLEVBQTZCQyxZQUE3QixFQUEyQ0MsS0FBM0MsS0FBcUQ7QUFDeEUsVUFBSSxDQUFDQSxLQUFMLEVBQVk7QUFFWixZQUFNQyxZQUFZLEdBQUcsS0FBS3JCLEtBQUwsQ0FBV0QsS0FBWCxDQUFpQlcsR0FBakIsQ0FBc0JOLENBQUQsSUFBT0EsQ0FBQyxDQUFDUCxJQUFGLENBQU9RLE1BQW5DLENBQXJCOztBQUNBLFVBQUlnQixZQUFZLENBQUNDLE1BQWIsS0FBd0JGLEtBQUssQ0FBQ0UsTUFBbEMsRUFBMEM7QUFDdEMsWUFBSUMsT0FBTyxHQUFHLEtBQWQ7O0FBQ0EsYUFBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxZQUFZLENBQUNDLE1BQWpDLEVBQXlDRSxDQUFDLEVBQTFDLEVBQThDO0FBQzFDLGNBQUlILFlBQVksQ0FBQ0csQ0FBRCxDQUFaLEtBQW9CSixLQUFLLENBQUNJLENBQUQsQ0FBN0IsRUFBa0M7QUFDOUJELFlBQUFBLE9BQU8sR0FBRyxJQUFWO0FBQ0E7QUFDSDtBQUNKOztBQUNELFlBQUksQ0FBQ0EsT0FBTCxFQUFjO0FBQ2pCOztBQUVELFdBQUtFLFlBQUwsQ0FBa0JMLEtBQWxCO0FBQ0gsS0FoSWtCO0FBQUEsbUVBa0lPLE1BQU07QUFDNUIsVUFBSSxDQUFDLEtBQUtwQixLQUFMLENBQVcwQixPQUFaLElBQXVCLEtBQUtDLGFBQUwsRUFBM0IsRUFBaUQ7QUFDN0MsYUFBS3BCLFFBQUwsQ0FBYztBQUFDbUIsVUFBQUEsT0FBTyxFQUFFO0FBQVYsU0FBZDtBQUNIO0FBQ0osS0F0SWtCO0FBQUEsa0RBd0lUN0IsSUFBRCxJQUFVO0FBQ2Y7QUFDQSxXQUFLVyx1QkFBTDs7QUFFQSxZQUFNb0IsV0FBVyxHQUFHLEtBQUtDLGlCQUFMLENBQXVCMUIsSUFBdkIsQ0FBNEJDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxNQUFGLEtBQWFSLElBQUksQ0FBQ1EsTUFBbkQsQ0FBcEI7O0FBQ0EsVUFBSSxDQUFDdUIsV0FBTCxFQUFrQjs7QUFDbEIsV0FBS0MsaUJBQUwsQ0FBdUJDLE1BQXZCLENBQThCLEtBQUtELGlCQUFMLENBQXVCRSxPQUF2QixDQUErQkgsV0FBL0IsQ0FBOUIsRUFBMkUsQ0FBM0U7O0FBRUEsWUFBTUksR0FBRyxHQUFJLElBQUlDLElBQUosRUFBRCxDQUFhQyxPQUFiLEVBQVo7QUFDQSxVQUFLRixHQUFHLEdBQUdKLFdBQVcsQ0FBQ08sT0FBbkIsR0FBOEI1QywwQkFBbEMsRUFBOEQsT0FUL0MsQ0FTdUQ7O0FBQ3RFLFdBQUs2QyxhQUFMLENBQW1CdkMsSUFBSSxDQUFDUSxNQUF4QixFQVZlLENBVWtCOztBQUNwQyxLQW5Ka0I7QUFFZixTQUFLTCxLQUFMLEdBQWE7QUFBQ0QsTUFBQUEsS0FBSyxFQUFFLEVBQVI7QUFBWTJCLE1BQUFBLE9BQU8sRUFBRTtBQUFyQixLQUFiO0FBRUEsU0FBS1csUUFBTCxHQUFnQixLQUFLQSxRQUFMLENBQWNDLElBQWQsQ0FBbUIsSUFBbkIsQ0FBaEI7QUFDQSxTQUFLQyxjQUFMLEdBQXNCLElBQXRCLENBTGUsQ0FPZjtBQUNBOztBQUNBLFNBQUtWLGlCQUFMLEdBQXlCO0FBQUM7QUFBRCxLQUF6QjtBQUVBLFNBQUtXLFNBQUwsR0FBaUIsdUJBQWpCO0FBQ0gsR0Fid0QsQ0FlekQ7OztBQUNBQyxFQUFBQSx5QkFBeUIsR0FBRztBQUFFO0FBQzFCLFNBQUtGLGNBQUwsR0FBc0JHLG9CQUFJQyxRQUFKLENBQWEsS0FBS04sUUFBbEIsQ0FBdEI7O0FBRUEsVUFBTU8sV0FBVyxHQUFHQyx1QkFBY0MsUUFBZCxDQUF1QixrQkFBdkIsQ0FBcEI7O0FBQ0EsU0FBS3JCLFlBQUwsQ0FBa0JtQixXQUFXLElBQUksRUFBakM7O0FBRUEsU0FBS0csZ0JBQUwsR0FBd0JGLHVCQUFjRyxZQUFkLENBQTJCLGtCQUEzQixFQUErQyxJQUEvQyxFQUFxRCxLQUFLQyxvQkFBMUQsQ0FBeEI7QUFFQSxTQUFLMUMsUUFBTCxDQUFjO0FBQUNtQixNQUFBQSxPQUFPLEVBQUUsS0FBS0MsYUFBTDtBQUFWLEtBQWQ7O0FBRUFiLHFDQUFnQkMsR0FBaEIsR0FBc0JtQyxFQUF0QixDQUF5QixtQkFBekIsRUFBOEMsS0FBS0MsY0FBbkQ7O0FBQ0FyQyxxQ0FBZ0JDLEdBQWhCLEdBQXNCbUMsRUFBdEIsQ0FBeUIsY0FBekIsRUFBeUMsS0FBS0UsYUFBOUM7O0FBQ0F0QyxxQ0FBZ0JDLEdBQWhCLEdBQXNCbUMsRUFBdEIsQ0FBeUIsZUFBekIsRUFBMEMsS0FBS0csY0FBL0M7O0FBQ0F2QyxxQ0FBZ0JDLEdBQWhCLEdBQXNCbUMsRUFBdEIsQ0FBeUIsaUJBQXpCLEVBQTRDLEtBQUtJLGdCQUFqRDs7QUFDQXhDLHFDQUFnQkMsR0FBaEIsR0FBc0JtQyxFQUF0QixDQUF5QixNQUF6QixFQUFpQyxLQUFLSyxNQUF0QztBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQmQsd0JBQUllLFVBQUosQ0FBZSxLQUFLbEIsY0FBcEI7O0FBRUFNLDJCQUFjYSxjQUFkLENBQTZCLEtBQUtYLGdCQUFsQzs7QUFFQSxVQUFNWSxNQUFNLEdBQUc3QyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsUUFBSTRDLE1BQUosRUFBWTtBQUNSQSxNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsbUJBQXRCLEVBQTJDLEtBQUtULGNBQWhEO0FBQ0FRLE1BQUFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixjQUF0QixFQUFzQyxLQUFLUixhQUEzQztBQUNBTyxNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsZUFBdEIsRUFBdUMsS0FBS1AsY0FBNUM7QUFDQU0sTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLGlCQUF0QixFQUF5QyxLQUFLTixnQkFBOUM7QUFDQUssTUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCLE1BQXRCLEVBQThCLEtBQUtMLE1BQW5DO0FBQ0g7QUFDSjs7QUFFRE0sRUFBQUEsa0JBQWtCLEdBQUc7QUFDakIsVUFBTTlELEtBQUssR0FBRyxLQUFLQyxLQUFMLENBQVdELEtBQVgsQ0FBaUJFLEtBQWpCLEVBQWQ7O0FBRUEsUUFBSUYsS0FBSyxDQUFDdUIsTUFBVixFQUFrQjtBQUNkLFlBQU13QyxTQUFTLEdBQUcvRCxLQUFLLENBQUMsQ0FBRCxDQUF2Qjs7QUFDQSxVQUFJLENBQUMrRCxTQUFTLENBQUNDLFFBQWYsRUFBeUI7QUFDckJELFFBQUFBLFNBQVMsQ0FBQ0MsUUFBVixHQUFxQixJQUFyQjtBQUNBQyxRQUFBQSxVQUFVLENBQUMsTUFBTSxLQUFLekQsUUFBTCxDQUFjO0FBQUNSLFVBQUFBO0FBQUQsU0FBZCxDQUFQLEVBQStCLENBQS9CLENBQVY7QUFDSDtBQUNKO0FBQ0o7O0FBRURzQyxFQUFBQSxRQUFRLENBQUM0QixPQUFELEVBQVU7QUFDZCxZQUFRQSxPQUFPLENBQUNDLE1BQWhCO0FBQ0ksV0FBSyxXQUFMO0FBQ0ksWUFBSUQsT0FBTyxDQUFDRSxTQUFSLElBQXFCLENBQUNyRCxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxPQUF0QixDQUE4QmlELE9BQU8sQ0FBQ0csT0FBdEMsQ0FBMUIsRUFBMEU7QUFDdEU7QUFDQTtBQUNBLGVBQUt2QyxpQkFBTCxDQUF1QndDLElBQXZCLENBQTRCO0FBQUNoRSxZQUFBQSxNQUFNLEVBQUU0RCxPQUFPLENBQUNHLE9BQWpCO0FBQTBCakMsWUFBQUEsT0FBTyxFQUFHLElBQUlGLElBQUosRUFBRCxDQUFXQyxPQUFYO0FBQW5DLFdBQTVCOztBQUNBO0FBQ0g7O0FBQ0QsYUFBS0UsYUFBTCxDQUFtQjZCLE9BQU8sQ0FBQ0csT0FBM0I7O0FBQ0E7QUFFSjtBQUNBOztBQUNBLFdBQUssY0FBTDtBQUFxQjtBQUNqQixnQkFBTXZFLElBQUksR0FBR2lCLGlDQUFnQkMsR0FBaEIsR0FBc0JDLE9BQXRCLENBQThCaUQsT0FBTyxDQUFDNUQsTUFBdEMsQ0FBYjs7QUFDQSxlQUFLTyxvQkFBTCxDQUEwQmYsSUFBMUI7QUFBZ0M7QUFBUyxjQUF6Qzs7QUFDQTtBQUNIO0FBakJMO0FBbUJIOztBQXNFRDhCLEVBQUFBLGFBQWEsR0FBRztBQUNaLFVBQU1nQyxNQUFNLEdBQUc3QyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTXVELGVBQWUsR0FBR1gsTUFBTSxDQUFDWSxRQUFQLEdBQWtCQyxNQUFsQixDQUF5QixDQUFDQyxLQUFELEVBQVFyRSxDQUFSLEtBQWM7QUFDM0QsYUFBT3FFLEtBQUssSUFBSXJFLENBQUMsQ0FBQ3NFLGVBQUYsT0FBd0IsTUFBeEIsR0FBaUMsQ0FBakMsR0FBcUMsQ0FBekMsQ0FBWjtBQUNILEtBRnVCLEVBRXJCLENBRnFCLENBQXhCO0FBR0EsV0FBT0osZUFBZSxJQUFJaEYsd0JBQTFCO0FBQ0g7O0FBRURtQyxFQUFBQSxZQUFZLENBQUNrRCxPQUFELEVBQVU7QUFDbEIsUUFBSSxDQUFDQSxPQUFELElBQVlBLE9BQU8sQ0FBQ3JELE1BQVIsSUFBa0IsQ0FBbEMsRUFBcUMsT0FEbkIsQ0FDMkI7QUFFN0M7O0FBQ0EsVUFBTXZCLEtBQUssR0FBRzRFLE9BQU8sQ0FBQ2pFLEdBQVIsQ0FBYU4sQ0FBRCxJQUFPVSxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxPQUF0QixDQUE4QlosQ0FBOUIsQ0FBbkIsRUFBcUR3RSxNQUFyRCxDQUE2RHhFLENBQUQsSUFBT0EsQ0FBbkUsRUFBc0VNLEdBQXRFLENBQTJFTixDQUFELElBQU87QUFDM0YsWUFBTXlFLE1BQU0sR0FBRyxLQUFLQyx1QkFBTCxDQUE2QjFFLENBQTdCLEtBQW1DLEVBQWxEO0FBQ0E7QUFDSVAsUUFBQUEsSUFBSSxFQUFFTyxDQURWO0FBRUkyRCxRQUFBQSxRQUFRLEVBQUU7QUFGZCxTQUdPYyxNQUhQO0FBS0gsS0FQYSxDQUFkO0FBUUEsU0FBS3RFLFFBQUwsQ0FBYztBQUNWUixNQUFBQSxLQUFLLEVBQUVBO0FBREcsS0FBZDtBQUdIOztBQUVEK0UsRUFBQUEsdUJBQXVCLENBQUNqRixJQUFELEVBQU9rRixJQUFJLEdBQUMsS0FBWixFQUFtQjtBQUN0QyxRQUFJLENBQUNsRixJQUFMLEVBQVcsT0FBTyxJQUFQLENBRDJCLENBR3RDOztBQUNBLFVBQU1pRSxTQUFTLEdBQUc7QUFDZGtCLE1BQUFBLFFBQVEsRUFBRSxLQURJO0FBRWRDLE1BQUFBLGNBQWMsRUFBRSxHQUZGO0FBR2RDLE1BQUFBLFNBQVMsRUFBRTtBQUhHLEtBQWxCO0FBTUEsUUFBSUgsSUFBSixFQUFVLE9BQU9qQixTQUFQO0FBRVYsVUFBTXFCLFVBQVUsR0FBR0MsVUFBVSxDQUFDQyxrQkFBWCxDQUE4QnhGLElBQUksQ0FBQ1EsTUFBbkMsQ0FBbkI7O0FBQ0EsUUFBSStFLFVBQVUsQ0FBQ0Usb0JBQVgsQ0FBZ0MzRSxRQUFoQyxDQUF5Q3dFLFVBQXpDLENBQUosRUFBMEQ7QUFDdEQsWUFBTUksZUFBZSxHQUFHSCxVQUFVLENBQUNJLDBCQUFYLENBQXNDM0YsSUFBdEMsRUFBNEMsV0FBNUMsQ0FBeEI7QUFDQSxZQUFNNEYsWUFBWSxHQUFHTCxVQUFVLENBQUNJLDBCQUFYLENBQXNDM0YsSUFBdEMsQ0FBckI7QUFFQSxZQUFNbUYsUUFBUSxHQUFHTyxlQUFlLEdBQUcsQ0FBbkM7QUFDQSxZQUFNRyxTQUFTLEdBQUdWLFFBQVEsSUFBS1MsWUFBWSxHQUFHLENBQWYsSUFBb0JMLFVBQVUsQ0FBQ08sWUFBWCxDQUF3QmhGLFFBQXhCLENBQWlDd0UsVUFBakMsQ0FBbkQ7O0FBRUEsVUFBSUgsUUFBUSxJQUFJVSxTQUFoQixFQUEyQjtBQUN2QixjQUFNRSxVQUFVLEdBQUdaLFFBQVEsR0FBR08sZUFBSCxHQUFxQkUsWUFBaEQ7QUFDQSxjQUFNSSxZQUFZLEdBQUdDLGVBQWUsQ0FBQ0MsV0FBaEIsQ0FBNEJILFVBQTVCLENBQXJCO0FBRUE5QixRQUFBQSxTQUFTLENBQUNrQixRQUFWLEdBQXFCQSxRQUFyQjtBQUNBbEIsUUFBQUEsU0FBUyxDQUFDbUIsY0FBVixHQUEyQlksWUFBM0I7QUFDQS9CLFFBQUFBLFNBQVMsQ0FBQ29CLFNBQVYsR0FBc0IsSUFBdEI7QUFDSDtBQUNKOztBQUVELFdBQU9wQixTQUFQO0FBQ0g7O0FBRURsRCxFQUFBQSxvQkFBb0IsQ0FBQ2YsSUFBRCxFQUFPa0YsSUFBSSxHQUFDLEtBQVosRUFBbUI7QUFDbkMsUUFBSSxDQUFDbEYsSUFBTCxFQUFXO0FBRVgsVUFBTUUsS0FBSyxHQUFHLEtBQUtDLEtBQUwsQ0FBV0QsS0FBWCxDQUFpQkUsS0FBakIsRUFBZDtBQUNBLFVBQU02RCxTQUFTLEdBQUcvRCxLQUFLLENBQUNJLElBQU4sQ0FBWUMsQ0FBRCxJQUFPQSxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBUCxLQUFrQlIsSUFBSSxDQUFDUSxNQUF6QyxDQUFsQjtBQUNBLFFBQUksQ0FBQ3lELFNBQUwsRUFBZ0IsT0FMbUIsQ0FLWDs7QUFFeEIsVUFBTWUsTUFBTSxHQUFHLEtBQUtDLHVCQUFMLENBQTZCakYsSUFBN0IsRUFBbUNrRixJQUFuQyxDQUFmOztBQUNBLFFBQUksQ0FBQ0YsTUFBTCxFQUFhLE9BUnNCLENBUWQ7O0FBRXJCbUIsSUFBQUEsTUFBTSxDQUFDQyxNQUFQLENBQWNuQyxTQUFkLEVBQXlCZSxNQUF6QjtBQUNBLFNBQUt0RSxRQUFMLENBQWM7QUFBQ1IsTUFBQUE7QUFBRCxLQUFkO0FBQ0g7O0FBRURxQyxFQUFBQSxhQUFhLENBQUMvQixNQUFELEVBQVM7QUFDbEIsUUFBSVIsSUFBSSxHQUFHaUIsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEJYLE1BQTlCLENBQVg7O0FBQ0EsUUFBSSxDQUFDUixJQUFMLEVBQVc7QUFFWCxVQUFNRSxLQUFLLEdBQUcsS0FBS0MsS0FBTCxDQUFXRCxLQUFYLENBQWlCRSxLQUFqQixFQUFkLENBSmtCLENBTWxCO0FBQ0E7O0FBQ0EsVUFBTWlHLE9BQU8sR0FBR3BGLGlDQUFnQkMsR0FBaEIsR0FBc0JvRixxQkFBdEIsQ0FBNEM5RixNQUE1QyxDQUFoQjs7QUFDQSxRQUFJNkYsT0FBTyxDQUFDNUUsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQnpCLE1BQUFBLElBQUksR0FBR3FHLE9BQU8sQ0FBQ0EsT0FBTyxDQUFDNUUsTUFBUixHQUFpQixDQUFsQixDQUFkLENBRG9CLENBQ2dCO0FBRXBDOztBQUNBLFdBQUssSUFBSUUsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRzBFLE9BQU8sQ0FBQzVFLE1BQVIsR0FBaUIsQ0FBckMsRUFBd0NFLENBQUMsRUFBekMsRUFBNkM7QUFDekMsY0FBTTRFLEdBQUcsR0FBR3JHLEtBQUssQ0FBQ3NHLFNBQU4sQ0FBaUJqRyxDQUFELElBQU9BLENBQUMsQ0FBQ1AsSUFBRixDQUFPUSxNQUFQLEtBQWtCNkYsT0FBTyxDQUFDMUUsQ0FBRCxDQUFQLENBQVduQixNQUFwRCxDQUFaO0FBQ0EsWUFBSStGLEdBQUcsS0FBSyxDQUFDLENBQWIsRUFBZ0JyRyxLQUFLLENBQUMrQixNQUFOLENBQWFzRSxHQUFiLEVBQWtCLENBQWxCO0FBQ25CO0FBQ0o7O0FBRUQsVUFBTUUsV0FBVyxHQUFHdkcsS0FBSyxDQUFDc0csU0FBTixDQUFpQmpHLENBQUQsSUFBT0EsQ0FBQyxDQUFDUCxJQUFGLENBQU9RLE1BQVAsS0FBa0JSLElBQUksQ0FBQ1EsTUFBOUMsQ0FBcEI7O0FBQ0EsUUFBSWlHLFdBQVcsS0FBSyxDQUFDLENBQXJCLEVBQXdCO0FBQ3BCdkcsTUFBQUEsS0FBSyxDQUFDK0IsTUFBTixDQUFhd0UsV0FBYixFQUEwQixDQUExQjtBQUNIOztBQUVEdkcsSUFBQUEsS0FBSyxDQUFDK0IsTUFBTixDQUFhLENBQWIsRUFBZ0IsQ0FBaEIsRUFBbUI7QUFBQ2pDLE1BQUFBLElBQUQ7QUFBT2tFLE1BQUFBLFFBQVEsRUFBRTtBQUFqQixLQUFuQjs7QUFFQSxRQUFJaEUsS0FBSyxDQUFDdUIsTUFBTixHQUFlakMsU0FBbkIsRUFBOEI7QUFDMUJVLE1BQUFBLEtBQUssQ0FBQytCLE1BQU4sQ0FBYXpDLFNBQWIsRUFBd0JVLEtBQUssQ0FBQ3VCLE1BQU4sR0FBZWpDLFNBQXZDO0FBQ0g7O0FBQ0QsU0FBS2tCLFFBQUwsQ0FBYztBQUFDUixNQUFBQTtBQUFELEtBQWQ7O0FBRUEsUUFBSSxLQUFLeUMsU0FBTCxDQUFlK0QsT0FBbkIsRUFBNEI7QUFDeEIsV0FBSy9ELFNBQUwsQ0FBZStELE9BQWYsQ0FBdUJDLFlBQXZCO0FBQ0gsS0FqQ2lCLENBbUNsQjtBQUNBO0FBQ0E7OztBQUNBLFVBQU03QixPQUFPLEdBQUc1RSxLQUFLLENBQUNXLEdBQU4sQ0FBV04sQ0FBRCxJQUFPQSxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBeEIsQ0FBaEI7O0FBQ0EsUUFBSXNFLE9BQU8sQ0FBQ3JELE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEJ1Qiw2QkFBYzRELFFBQWQsQ0FBdUIsa0JBQXZCLEVBQTJDLElBQTNDLEVBQWlEQyw0QkFBYUMsT0FBOUQsRUFBdUVoQyxPQUF2RTtBQUNIO0FBQ0o7O0FBRURpQyxFQUFBQSxTQUFTLENBQUMvRyxJQUFELEVBQU9nSCxLQUFQLEVBQWM7QUFDbkJDLHVCQUFVQyxVQUFWLENBQXFCLGFBQXJCLEVBQW9DLFlBQXBDLEVBQWtERixLQUFsRDs7QUFDQW5FLHdCQUFJc0UsUUFBSixDQUFhO0FBQUM5QyxNQUFBQSxNQUFNLEVBQUUsV0FBVDtBQUFzQkUsTUFBQUEsT0FBTyxFQUFFdkUsSUFBSSxDQUFDUTtBQUFwQyxLQUFiO0FBQ0g7O0FBRUQ0RyxFQUFBQSxhQUFhLENBQUNwSCxJQUFELEVBQU87QUFDaEIsU0FBS3FILFFBQUwsQ0FBY3JILElBQWQ7QUFDSDs7QUFFRHNILEVBQUFBLGFBQWEsQ0FBQ3RILElBQUQsRUFBTztBQUNoQixTQUFLcUgsUUFBTCxDQUFjLElBQWQsRUFEZ0IsQ0FDSzs7QUFDeEI7O0FBRURBLEVBQUFBLFFBQVEsQ0FBQ3JILElBQUQsRUFBTztBQUNYLFVBQU1FLEtBQUssR0FBRyxLQUFLQyxLQUFMLENBQVdELEtBQVgsQ0FBaUJFLEtBQWpCLEVBQWQ7O0FBQ0EsU0FBSyxNQUFNRyxDQUFYLElBQWdCTCxLQUFoQixFQUF1QjtBQUNuQkssTUFBQUEsQ0FBQyxDQUFDZ0gsS0FBRixHQUFVdkgsSUFBSSxJQUFJTyxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFBUCxLQUFrQlIsSUFBSSxDQUFDUSxNQUF6QztBQUNIOztBQUNELFNBQUtFLFFBQUwsQ0FBYztBQUFDUixNQUFBQTtBQUFELEtBQWQ7QUFDSDs7QUFFRHNILEVBQUFBLFNBQVMsQ0FBQ3hILElBQUQsRUFBTztBQUNaLFVBQU15SCxPQUFPLEdBQUdDLG1CQUFVQyxNQUFWLEdBQW1CQyxrQkFBbkIsQ0FBc0M1SCxJQUFJLENBQUNRLE1BQTNDLENBQWhCOztBQUNBLFdBQU9xSCxPQUFPLENBQUNKLE9BQUQsQ0FBZDtBQUNIOztBQUVESyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixrQkFBakIsQ0FBaEI7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUEzQixDQUZLLENBSUw7QUFDQTs7QUFDQSxRQUFJLEtBQUtsSSxLQUFMLENBQVdvSSxTQUFYLElBQXdCLENBQUMsS0FBS2hJLEtBQUwsQ0FBVzBCLE9BQXhDLEVBQWlEO0FBQzdDLGFBQU8sSUFBUDtBQUNIOztBQUVELFVBQU0zQixLQUFLLEdBQUcsS0FBS0MsS0FBTCxDQUFXRCxLQUF6QjtBQUNBLFVBQU1rSSxPQUFPLEdBQUdsSSxLQUFLLENBQUNXLEdBQU4sQ0FBVSxDQUFDTixDQUFELEVBQUlvQixDQUFKLEtBQVU7QUFDaEMsWUFBTTBHLE9BQU8sR0FBRzFHLENBQUMsS0FBSyxDQUF0QjtBQUNBLFlBQU0yRyxPQUFPLEdBQUcseUJBQVc7QUFDdkIsb0NBQTRCLElBREw7QUFFdkIseUNBQWlDRCxPQUFPLElBQUksQ0FBQzlILENBQUMsQ0FBQzJELFFBRnhCO0FBR3ZCLHNDQUE4Qm1FLE9BSFA7QUFJdkIsbUNBQTJCOUgsQ0FBQyxDQUFDRTtBQUpOLE9BQVgsQ0FBaEI7QUFPQSxVQUFJOEgsT0FBTyxHQUFHLElBQWQ7O0FBQ0EsVUFBSWhJLENBQUMsQ0FBQ2dILEtBQU4sRUFBYTtBQUNUZ0IsUUFBQUEsT0FBTyxHQUFHLDZCQUFDLE9BQUQ7QUFBUyxVQUFBLEtBQUssRUFBRWhJLENBQUMsQ0FBQ1AsSUFBRixDQUFPd0k7QUFBdkIsVUFBVjtBQUNIOztBQUVELFVBQUlDLEtBQUo7O0FBQ0EsVUFBSWxJLENBQUMsQ0FBQzhFLFNBQU4sRUFBaUI7QUFDYixjQUFNcUQsWUFBWSxHQUFHLHlCQUFXO0FBQzVCLCtCQUFxQixJQURPO0FBRTVCLHFDQUEyQixJQUZDO0FBRzVCLGtDQUF3Qm5JLENBQUMsQ0FBQzRFLFFBSEU7QUFJNUIscUNBQTJCLENBQUM1RSxDQUFDLENBQUM0RTtBQUpGLFNBQVgsQ0FBckI7QUFPQXNELFFBQUFBLEtBQUssR0FBRztBQUFLLFVBQUEsU0FBUyxFQUFFQztBQUFoQixXQUErQm5JLENBQUMsQ0FBQzZFLGNBQWpDLENBQVI7QUFDSDs7QUFFRCxVQUFJdUQsV0FBSjs7QUFDQSxVQUFJLEtBQUtuQixTQUFMLENBQWVqSCxDQUFDLENBQUNQLElBQWpCLEtBQTBCLENBQUNnRCx1QkFBYzRGLGdCQUFkLENBQStCLHVCQUEvQixDQUEvQixFQUF3RjtBQUNwRkQsUUFBQUEsV0FBVyxHQUFHO0FBQ1YsVUFBQSxHQUFHLEVBQUVFLE9BQU8sQ0FBQyxxQ0FBRCxDQURGO0FBRVYsVUFBQSxTQUFTLEVBQUMsZ0NBRkE7QUFHVixVQUFBLEtBQUssRUFBQyxJQUhJO0FBSVYsVUFBQSxNQUFNLEVBQUMsSUFKRztBQUtWLFVBQUEsR0FBRyxFQUFFLHlCQUFHLGFBQUg7QUFMSyxVQUFkO0FBT0g7O0FBRUQsYUFDSSw2QkFBQyx5QkFBRDtBQUNJLFFBQUEsU0FBUyxFQUFFUCxPQURmO0FBRUksUUFBQSxHQUFHLEVBQUUvSCxDQUFDLENBQUNQLElBQUYsQ0FBT1EsTUFGaEI7QUFHSSxRQUFBLE9BQU8sRUFBRSxNQUFNLEtBQUt1RyxTQUFMLENBQWV4RyxDQUFDLENBQUNQLElBQWpCLEVBQXVCMkIsQ0FBdkIsQ0FIbkI7QUFJSSxRQUFBLFlBQVksRUFBRSxNQUFNLEtBQUt5RixhQUFMLENBQW1CN0csQ0FBQyxDQUFDUCxJQUFyQixDQUp4QjtBQUtJLFFBQUEsWUFBWSxFQUFFLE1BQU0sS0FBS3NILGFBQUwsQ0FBbUIvRyxDQUFDLENBQUNQLElBQXJCLENBTHhCO0FBTUksc0JBQVkseUJBQUcsZUFBSCxFQUFvQjtBQUFDd0ksVUFBQUEsSUFBSSxFQUFFakksQ0FBQyxDQUFDUCxJQUFGLENBQU93STtBQUFkLFNBQXBCO0FBTmhCLFNBUUksNkJBQUMsbUJBQUQ7QUFBWSxRQUFBLElBQUksRUFBRWpJLENBQUMsQ0FBQ1AsSUFBcEI7QUFBMEIsUUFBQSxLQUFLLEVBQUUsRUFBakM7QUFBcUMsUUFBQSxNQUFNLEVBQUU7QUFBN0MsUUFSSixFQVNLeUksS0FUTCxFQVVLRSxXQVZMLEVBV0tKLE9BWEwsQ0FESjtBQWVILEtBcERlLENBQWhCO0FBcURBLFdBQ0k7QUFBSyxNQUFBLElBQUksRUFBQyxTQUFWO0FBQW9CLG9CQUFZLHlCQUFHLGNBQUg7QUFBaEMsT0FDSSw2QkFBQyxrQkFBRDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUs1RixTQURkO0FBRUksTUFBQSxTQUFTLEVBQUMsb0JBRmQ7QUFHSSxNQUFBLHVCQUF1QixFQUFFLElBSDdCO0FBSUksTUFBQSwyQkFBMkIsRUFBRTtBQUpqQyxPQU1NeUYsT0FOTixDQURKLENBREo7QUFZSDs7QUFoWHdEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tIFwiLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnXCI7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlLCB7U2V0dGluZ0xldmVsfSBmcm9tIFwiLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tICcuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uJztcclxuaW1wb3J0IFJvb21BdmF0YXIgZnJvbSAnLi4vYXZhdGFycy9Sb29tQXZhdGFyJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IEFuYWx5dGljcyBmcm9tIFwiLi4vLi4vLi4vQW5hbHl0aWNzXCI7XHJcbmltcG9ydCAqIGFzIFJvb21Ob3RpZnMgZnJvbSAnLi4vLi4vLi4vUm9vbU5vdGlmcyc7XHJcbmltcG9ydCAqIGFzIEZvcm1hdHRpbmdVdGlscyBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvRm9ybWF0dGluZ1V0aWxzXCI7XHJcbmltcG9ydCBETVJvb21NYXAgZnJvbSBcIi4uLy4uLy4uL3V0aWxzL0RNUm9vbU1hcFwiO1xyXG5pbXBvcnQge190fSBmcm9tIFwiLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyXCI7XHJcblxyXG5jb25zdCBNQVhfUk9PTVMgPSAyMDtcclxuY29uc3QgTUlOX1JPT01TX0JFRk9SRV9FTkFCTEVEID0gMTA7XHJcblxyXG4vLyBUaGUgdGhyZXNob2xkIHRpbWUgaW4gbWlsbGlzZWNvbmRzIHRvIHdhaXQgZm9yIGFuIGF1dG9qb2luZWQgcm9vbSB0byBzaG93IHVwLlxyXG5jb25zdCBBVVRPSk9JTl9XQUlUX1RIUkVTSE9MRF9NUyA9IDkwMDAwOyAvLyA5MCBzZWNvbmRzXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb29tQnJlYWRjcnVtYnMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtyb29tczogW10sIGVuYWJsZWQ6IGZhbHNlfTtcclxuXHJcbiAgICAgICAgdGhpcy5vbkFjdGlvbiA9IHRoaXMub25BY3Rpb24uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9kaXNwYXRjaGVyUmVmID0gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gVGhlIHJvb20gSURzIHdlJ3JlIHdhaXRpbmcgdG8gY29tZSBkb3duIHRoZSBSb29tIGhhbmRsZXIgYW5kIHdoZW4gd2VcclxuICAgICAgICAvLyBzdGFydGVkIHdhaXRpbmcgZm9yIHRoZW0uIFVzZWQgdG8gdHJhY2sgYSByb29tIG92ZXIgYW4gdXBncmFkZS9hdXRvam9pbi5cclxuICAgICAgICB0aGlzLl93YWl0aW5nUm9vbVF1ZXVlID0gWy8qIHsgcm9vbUlkLCBhZGRlZFRzIH0gKi9dO1xyXG5cclxuICAgICAgICB0aGlzLl9zY3JvbGxlciA9IGNyZWF0ZVJlZigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IFtSRUFDVC1XQVJOSU5HXSBNb3ZlIHRoaXMgdG8gY29uc3RydWN0b3JcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQoKSB7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgY2FtZWxjYXNlXHJcbiAgICAgICAgdGhpcy5fZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLm9uQWN0aW9uKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc3RvcmVkUm9vbXMgPSBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiYnJlYWRjcnVtYl9yb29tc1wiKTtcclxuICAgICAgICB0aGlzLl9sb2FkUm9vbUlkcyhzdG9yZWRSb29tcyB8fCBbXSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3NldHRpbmdXYXRjaFJlZiA9IFNldHRpbmdzU3RvcmUud2F0Y2hTZXR0aW5nKFwiYnJlYWRjcnVtYl9yb29tc1wiLCBudWxsLCB0aGlzLm9uQnJlYWRjcnVtYnNDaGFuZ2VkKTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZW5hYmxlZDogdGhpcy5fc2hvdWxkRW5hYmxlKCl9KTtcclxuXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS5teU1lbWJlcnNoaXBcIiwgdGhpcy5vbk15TWVtYmVyc2hpcCk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKFwiUm9vbS50aW1lbGluZVwiLCB0aGlzLm9uUm9vbVRpbWVsaW5lKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJFdmVudC5kZWNyeXB0ZWRcIiwgdGhpcy5vbkV2ZW50RGVjcnlwdGVkKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJSb29tXCIsIHRoaXMub25Sb29tKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBkaXMudW5yZWdpc3Rlcih0aGlzLl9kaXNwYXRjaGVyUmVmKTtcclxuXHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS51bndhdGNoU2V0dGluZyh0aGlzLl9zZXR0aW5nV2F0Y2hSZWYpO1xyXG5cclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaWVudCkge1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tLm15TWVtYmVyc2hpcFwiLCB0aGlzLm9uTXlNZW1iZXJzaGlwKTtcclxuICAgICAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKFwiUm9vbS5yZWNlaXB0XCIsIHRoaXMub25Sb29tUmVjZWlwdCk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIlJvb20udGltZWxpbmVcIiwgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICAgICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcihcIkV2ZW50LmRlY3J5cHRlZFwiLCB0aGlzLm9uRXZlbnREZWNyeXB0ZWQpO1xyXG4gICAgICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoXCJSb29tXCIsIHRoaXMub25Sb29tKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb21zID0gdGhpcy5zdGF0ZS5yb29tcy5zbGljZSgpO1xyXG5cclxuICAgICAgICBpZiAocm9vbXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21Nb2RlbCA9IHJvb21zWzBdO1xyXG4gICAgICAgICAgICBpZiAoIXJvb21Nb2RlbC5hbmltYXRlZCkge1xyXG4gICAgICAgICAgICAgICAgcm9vbU1vZGVsLmFuaW1hdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5zZXRTdGF0ZSh7cm9vbXN9KSwgMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25BY3Rpb24ocGF5bG9hZCkge1xyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgY2FzZSAndmlld19yb29tJzpcclxuICAgICAgICAgICAgICAgIGlmIChwYXlsb2FkLmF1dG9fam9pbiAmJiAhTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20ocGF5bG9hZC5yb29tX2lkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFF1ZXVlIHRoZSByb29tIGluc3RlYWQgb2YgcHVzaGluZyBpdCBpbW1lZGlhdGVseSAtIHdlJ3JlIHByb2JhYmx5IGp1c3Qgd2FpdGluZ1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGZvciBhIGpvaW4gdG8gY29tcGxldGUgKGllOiBqb2luaW5nIHRoZSB1cGdyYWRlZCByb29tKS5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl93YWl0aW5nUm9vbVF1ZXVlLnB1c2goe3Jvb21JZDogcGF5bG9hZC5yb29tX2lkLCBhZGRlZFRzOiAobmV3IERhdGUpLmdldFRpbWUoKX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5fYXBwZW5kUm9vbUlkKHBheWxvYWQucm9vbV9pZCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIC8vIFhYWDogc2xpZ2h0IGhhY2sgaW4gb3JkZXIgdG8gemVybyB0aGUgbm90aWZpY2F0aW9uIGNvdW50IHdoZW4gYSByb29tXHJcbiAgICAgICAgICAgIC8vIGlzIHJlYWQuIENvcGllZCBmcm9tIFJvb21UaWxlXHJcbiAgICAgICAgICAgIGNhc2UgJ29uX3Jvb21fcmVhZCc6IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShwYXlsb2FkLnJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jYWxjdWxhdGVSb29tQmFkZ2VzKHJvb20sIC8qemVybz0qL3RydWUpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25NeU1lbWJlcnNoaXAgPSAocm9vbSwgbWVtYmVyc2hpcCkgPT4ge1xyXG4gICAgICAgIGlmIChtZW1iZXJzaGlwID09PSBcImxlYXZlXCIgfHwgbWVtYmVyc2hpcCA9PT0gXCJiYW5cIikge1xyXG4gICAgICAgICAgICBjb25zdCByb29tcyA9IHRoaXMuc3RhdGUucm9vbXMuc2xpY2UoKTtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbVN0YXRlID0gcm9vbXMuZmluZCgocikgPT4gci5yb29tLnJvb21JZCA9PT0gcm9vbS5yb29tSWQpO1xyXG4gICAgICAgICAgICBpZiAocm9vbVN0YXRlKSB7XHJcbiAgICAgICAgICAgICAgICByb29tU3RhdGUubGVmdCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtyb29tc30pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub25Sb29tTWVtYmVyc2hpcENoYW5nZWQoKTtcclxuICAgIH07XHJcblxyXG4gICAgb25Sb29tUmVjZWlwdCA9IChldmVudCwgcm9vbSkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJvb21zLm1hcChyID0+IHIucm9vbS5yb29tSWQpLmluY2x1ZGVzKHJvb20ucm9vbUlkKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9jYWxjdWxhdGVSb29tQmFkZ2VzKHJvb20pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25Sb29tVGltZWxpbmUgPSAoZXZlbnQsIHJvb20pID0+IHtcclxuICAgICAgICBpZiAoIXJvb20pIHJldHVybjsgLy8gQ2FuIGJlIG51bGwgZm9yIHRoZSBub3RpZmljYXRpb24gdGltZWxpbmUsIGV0Yy5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yb29tcy5tYXAociA9PiByLnJvb20ucm9vbUlkKS5pbmNsdWRlcyhyb29tLnJvb21JZCkpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2FsY3VsYXRlUm9vbUJhZGdlcyhyb29tKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9uRXZlbnREZWNyeXB0ZWQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yb29tcy5tYXAociA9PiByLnJvb20ucm9vbUlkKS5pbmNsdWRlcyhldmVudC5nZXRSb29tSWQoKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2FsY3VsYXRlUm9vbUJhZGdlcyhNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShldmVudC5nZXRSb29tSWQoKSkpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb25CcmVhZGNydW1ic0NoYW5nZWQgPSAoc2V0dGluZ05hbWUsIHJvb21JZCwgbGV2ZWwsIHZhbHVlQXRMZXZlbCwgdmFsdWUpID0+IHtcclxuICAgICAgICBpZiAoIXZhbHVlKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRTdGF0ZSA9IHRoaXMuc3RhdGUucm9vbXMubWFwKChyKSA9PiByLnJvb20ucm9vbUlkKTtcclxuICAgICAgICBpZiAoY3VycmVudFN0YXRlLmxlbmd0aCA9PT0gdmFsdWUubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGxldCBjaGFuZ2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY3VycmVudFN0YXRlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFN0YXRlW2ldICE9PSB2YWx1ZVtpXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICghY2hhbmdlZCkgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fbG9hZFJvb21JZHModmFsdWUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvblJvb21NZW1iZXJzaGlwQ2hhbmdlZCA9ICgpID0+IHtcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuZW5hYmxlZCAmJiB0aGlzLl9zaG91bGRFbmFibGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtlbmFibGVkOiB0cnVlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvblJvb20gPSAocm9vbSkgPT4ge1xyXG4gICAgICAgIC8vIEFsd2F5cyBjaGVjayBmb3IgbWVtYmVyc2hpcCBjaGFuZ2VzIHdoZW4gd2Ugc2VlIG5ldyByb29tc1xyXG4gICAgICAgIHRoaXMub25Sb29tTWVtYmVyc2hpcENoYW5nZWQoKTtcclxuXHJcbiAgICAgICAgY29uc3Qgd2FpdGluZ1Jvb20gPSB0aGlzLl93YWl0aW5nUm9vbVF1ZXVlLmZpbmQociA9PiByLnJvb21JZCA9PT0gcm9vbS5yb29tSWQpO1xyXG4gICAgICAgIGlmICghd2FpdGluZ1Jvb20pIHJldHVybjtcclxuICAgICAgICB0aGlzLl93YWl0aW5nUm9vbVF1ZXVlLnNwbGljZSh0aGlzLl93YWl0aW5nUm9vbVF1ZXVlLmluZGV4T2Yod2FpdGluZ1Jvb20pLCAxKTtcclxuXHJcbiAgICAgICAgY29uc3Qgbm93ID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcclxuICAgICAgICBpZiAoKG5vdyAtIHdhaXRpbmdSb29tLmFkZGVkVHMpID4gQVVUT0pPSU5fV0FJVF9USFJFU0hPTERfTVMpIHJldHVybjsgLy8gVG9vIGxvbmcgYWdvLlxyXG4gICAgICAgIHRoaXMuX2FwcGVuZFJvb21JZChyb29tLnJvb21JZCk7IC8vIGFkZCB0aGUgcm9vbSB3ZSd2ZSBiZWVuIHdhaXRpbmcgZm9yXHJcbiAgICB9O1xyXG5cclxuICAgIF9zaG91bGRFbmFibGUoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IGpvaW5lZFJvb21Db3VudCA9IGNsaWVudC5nZXRSb29tcygpLnJlZHVjZSgoY291bnQsIHIpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGNvdW50ICsgKHIuZ2V0TXlNZW1iZXJzaGlwKCkgPT09IFwiam9pblwiID8gMSA6IDApO1xyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHJldHVybiBqb2luZWRSb29tQ291bnQgPj0gTUlOX1JPT01TX0JFRk9SRV9FTkFCTEVEO1xyXG4gICAgfVxyXG5cclxuICAgIF9sb2FkUm9vbUlkcyhyb29tSWRzKSB7XHJcbiAgICAgICAgaWYgKCFyb29tSWRzIHx8IHJvb21JZHMubGVuZ3RoIDw9IDApIHJldHVybjsgLy8gU2tpcCB1cGRhdGVzIHdpdGggbm8gcm9vbXNcclxuXHJcbiAgICAgICAgLy8gSWYgd2UncmUgaGVyZSwgdGhlIGxpc3QgY2hhbmdlZC5cclxuICAgICAgICBjb25zdCByb29tcyA9IHJvb21JZHMubWFwKChyKSA9PiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShyKSkuZmlsdGVyKChyKSA9PiByKS5tYXAoKHIpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgYmFkZ2VzID0gdGhpcy5fY2FsY3VsYXRlQmFkZ2VzRm9yUm9vbShyKSB8fCB7fTtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHJvb206IHIsXHJcbiAgICAgICAgICAgICAgICBhbmltYXRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAuLi5iYWRnZXMsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHJvb21zOiByb29tcyxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfY2FsY3VsYXRlQmFkZ2VzRm9yUm9vbShyb29tLCB6ZXJvPWZhbHNlKSB7XHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm4gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gUmVzZXQgdGhlIG5vdGlmaWNhdGlvbiB2YXJpYWJsZXMgZm9yIHNpbXBsaWNpdHlcclxuICAgICAgICBjb25zdCByb29tTW9kZWwgPSB7XHJcbiAgICAgICAgICAgIHJlZEJhZGdlOiBmYWxzZSxcclxuICAgICAgICAgICAgZm9ybWF0dGVkQ291bnQ6IFwiMFwiLFxyXG4gICAgICAgICAgICBzaG93Q291bnQ6IGZhbHNlLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGlmICh6ZXJvKSByZXR1cm4gcm9vbU1vZGVsO1xyXG5cclxuICAgICAgICBjb25zdCBub3RpZlN0YXRlID0gUm9vbU5vdGlmcy5nZXRSb29tTm90aWZzU3RhdGUocm9vbS5yb29tSWQpO1xyXG4gICAgICAgIGlmIChSb29tTm90aWZzLk1FTlRJT05fQkFER0VfU1RBVEVTLmluY2x1ZGVzKG5vdGlmU3RhdGUpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGhpZ2hsaWdodE5vdGlmcyA9IFJvb21Ob3RpZnMuZ2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQocm9vbSwgJ2hpZ2hsaWdodCcpO1xyXG4gICAgICAgICAgICBjb25zdCB1bnJlYWROb3RpZnMgPSBSb29tTm90aWZzLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KHJvb20pO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcmVkQmFkZ2UgPSBoaWdobGlnaHROb3RpZnMgPiAwO1xyXG4gICAgICAgICAgICBjb25zdCBncmV5QmFkZ2UgPSByZWRCYWRnZSB8fCAodW5yZWFkTm90aWZzID4gMCAmJiBSb29tTm90aWZzLkJBREdFX1NUQVRFUy5pbmNsdWRlcyhub3RpZlN0YXRlKSk7XHJcblxyXG4gICAgICAgICAgICBpZiAocmVkQmFkZ2UgfHwgZ3JleUJhZGdlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBub3RpZkNvdW50ID0gcmVkQmFkZ2UgPyBoaWdobGlnaHROb3RpZnMgOiB1bnJlYWROb3RpZnM7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBsaW1pdGVkQ291bnQgPSBGb3JtYXR0aW5nVXRpbHMuZm9ybWF0Q291bnQobm90aWZDb3VudCk7XHJcblxyXG4gICAgICAgICAgICAgICAgcm9vbU1vZGVsLnJlZEJhZGdlID0gcmVkQmFkZ2U7XHJcbiAgICAgICAgICAgICAgICByb29tTW9kZWwuZm9ybWF0dGVkQ291bnQgPSBsaW1pdGVkQ291bnQ7XHJcbiAgICAgICAgICAgICAgICByb29tTW9kZWwuc2hvd0NvdW50ID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJvb21Nb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICBfY2FsY3VsYXRlUm9vbUJhZGdlcyhyb29tLCB6ZXJvPWZhbHNlKSB7XHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21zID0gdGhpcy5zdGF0ZS5yb29tcy5zbGljZSgpO1xyXG4gICAgICAgIGNvbnN0IHJvb21Nb2RlbCA9IHJvb21zLmZpbmQoKHIpID0+IHIucm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb21Nb2RlbCkgcmV0dXJuOyAvLyBObyBhcHBsaWNhYmxlIHJvb20sIHNvIGRvbid0IGRvIG1hdGggb24gaXRcclxuXHJcbiAgICAgICAgY29uc3QgYmFkZ2VzID0gdGhpcy5fY2FsY3VsYXRlQmFkZ2VzRm9yUm9vbShyb29tLCB6ZXJvKTtcclxuICAgICAgICBpZiAoIWJhZGdlcykgcmV0dXJuOyAvLyBObyBiYWRnZXMgZm9yIHNvbWUgcmVhc29uXHJcblxyXG4gICAgICAgIE9iamVjdC5hc3NpZ24ocm9vbU1vZGVsLCBiYWRnZXMpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Jvb21zfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2FwcGVuZFJvb21JZChyb29tSWQpIHtcclxuICAgICAgICBsZXQgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21zID0gdGhpcy5zdGF0ZS5yb29tcy5zbGljZSgpO1xyXG5cclxuICAgICAgICAvLyBJZiB0aGUgcm9vbSBpcyB1cGdyYWRlZCwgdXNlIHRoYXQgcm9vbSBpbnN0ZWFkLiBXZSdsbCBhbHNvIHNwbGljZSBvdXRcclxuICAgICAgICAvLyBhbnkgY2hpbGRyZW4gb2YgdGhlIHJvb20uXHJcbiAgICAgICAgY29uc3QgaGlzdG9yeSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tVXBncmFkZUhpc3Rvcnkocm9vbUlkKTtcclxuICAgICAgICBpZiAoaGlzdG9yeS5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgIHJvb20gPSBoaXN0b3J5W2hpc3RvcnkubGVuZ3RoIC0gMV07IC8vIExhc3Qgcm9vbSBpcyBtb3N0IHJlY2VudFxyXG5cclxuICAgICAgICAgICAgLy8gVGFrZSBvdXQgYW55IHJvb20gdGhhdCBpc24ndCB0aGUgbW9zdCByZWNlbnQgcm9vbVxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGhpc3RvcnkubGVuZ3RoIC0gMTsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpZHggPSByb29tcy5maW5kSW5kZXgoKHIpID0+IHIucm9vbS5yb29tSWQgPT09IGhpc3RvcnlbaV0ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGlmIChpZHggIT09IC0xKSByb29tcy5zcGxpY2UoaWR4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZXhpc3RpbmdJZHggPSByb29tcy5maW5kSW5kZXgoKHIpID0+IHIucm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICBpZiAoZXhpc3RpbmdJZHggIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHJvb21zLnNwbGljZShleGlzdGluZ0lkeCwgMSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByb29tcy5zcGxpY2UoMCwgMCwge3Jvb20sIGFuaW1hdGVkOiBmYWxzZX0pO1xyXG5cclxuICAgICAgICBpZiAocm9vbXMubGVuZ3RoID4gTUFYX1JPT01TKSB7XHJcbiAgICAgICAgICAgIHJvb21zLnNwbGljZShNQVhfUk9PTVMsIHJvb21zLmxlbmd0aCAtIE1BWF9ST09NUyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Jvb21zfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9zY3JvbGxlci5jdXJyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Njcm9sbGVyLmN1cnJlbnQubW92ZVRvT3JpZ2luKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBXZSBkb24ndCB0cmFjayByb29tIGFlc3RoZXRpY3MgKGJhZGdlcywgbWVtYmVyc2hpcCwgZXRjKSBvdmVyIHRoZSB3aXJlIHNvIHdlXHJcbiAgICAgICAgLy8gZG9uJ3QgbmVlZCB0byBkbyB0aGlzIGVsc2V3aGVyZSBpbiB0aGUgZmlsZS4gSnVzdCB3aGVyZSB3ZSBhbHRlciB0aGUgcm9vbSBJRHNcclxuICAgICAgICAvLyBhbmQgdGhlaXIgb3JkZXIuXHJcbiAgICAgICAgY29uc3Qgcm9vbUlkcyA9IHJvb21zLm1hcCgocikgPT4gci5yb29tLnJvb21JZCk7XHJcbiAgICAgICAgaWYgKHJvb21JZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFwiYnJlYWRjcnVtYl9yb29tc1wiLCBudWxsLCBTZXR0aW5nTGV2ZWwuQUNDT1VOVCwgcm9vbUlkcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF92aWV3Um9vbShyb29tLCBpbmRleCkge1xyXG4gICAgICAgIEFuYWx5dGljcy50cmFja0V2ZW50KFwiQnJlYWRjcnVtYnNcIiwgXCJjbGlja19ub2RlXCIsIGluZGV4KTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogXCJ2aWV3X3Jvb21cIiwgcm9vbV9pZDogcm9vbS5yb29tSWR9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25Nb3VzZUVudGVyKHJvb20pIHtcclxuICAgICAgICB0aGlzLl9vbkhvdmVyKHJvb20pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbk1vdXNlTGVhdmUocm9vbSkge1xyXG4gICAgICAgIHRoaXMuX29uSG92ZXIobnVsbCk7IC8vIGNsZWFyIGhvdmVyIHN0YXRlc1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkhvdmVyKHJvb20pIHtcclxuICAgICAgICBjb25zdCByb29tcyA9IHRoaXMuc3RhdGUucm9vbXMuc2xpY2UoKTtcclxuICAgICAgICBmb3IgKGNvbnN0IHIgb2Ygcm9vbXMpIHtcclxuICAgICAgICAgICAgci5ob3ZlciA9IHJvb20gJiYgci5yb29tLnJvb21JZCA9PT0gcm9vbS5yb29tSWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3Jvb21zfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2lzRG1Sb29tKHJvb20pIHtcclxuICAgICAgICBjb25zdCBkbVJvb21zID0gRE1Sb29tTWFwLnNoYXJlZCgpLmdldFVzZXJJZEZvclJvb21JZChyb29tLnJvb21JZCk7XHJcbiAgICAgICAgcmV0dXJuIEJvb2xlYW4oZG1Sb29tcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IFRvb2x0aXAgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5Ub29sdGlwJyk7XHJcbiAgICAgICAgY29uc3QgSW5kaWNhdG9yU2Nyb2xsYmFyID0gc2RrLmdldENvbXBvbmVudCgnc3RydWN0dXJlcy5JbmRpY2F0b3JTY3JvbGxiYXInKTtcclxuXHJcbiAgICAgICAgLy8gY2hlY2sgZm9yIGNvbGxhcHNlZCBoZXJlIGFuZCBub3QgYXQgcGFyZW50IHNvIHdlIGtlZXAgcm9vbXMgaW4gb3VyIHN0YXRlXHJcbiAgICAgICAgLy8gd2hlbiBjb2xsYXBzaW5nIGFuZCBleHBhbmRpbmdcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5jb2xsYXBzZWQgfHwgIXRoaXMuc3RhdGUuZW5hYmxlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21zID0gdGhpcy5zdGF0ZS5yb29tcztcclxuICAgICAgICBjb25zdCBhdmF0YXJzID0gcm9vbXMubWFwKChyLCBpKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzRmlyc3QgPSBpID09PSAwO1xyXG4gICAgICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgICAgICAgICBcIm14X1Jvb21CcmVhZGNydW1ic19jcnVtYlwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Sb29tQnJlYWRjcnVtYnNfcHJlQW5pbWF0ZVwiOiBpc0ZpcnN0ICYmICFyLmFuaW1hdGVkLFxyXG4gICAgICAgICAgICAgICAgXCJteF9Sb29tQnJlYWRjcnVtYnNfYW5pbWF0ZVwiOiBpc0ZpcnN0LFxyXG4gICAgICAgICAgICAgICAgXCJteF9Sb29tQnJlYWRjcnVtYnNfbGVmdFwiOiByLmxlZnQsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgbGV0IHRvb2x0aXAgPSBudWxsO1xyXG4gICAgICAgICAgICBpZiAoci5ob3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdG9vbHRpcCA9IDxUb29sdGlwIGxhYmVsPXtyLnJvb20ubmFtZX0gLz47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBiYWRnZTtcclxuICAgICAgICAgICAgaWYgKHIuc2hvd0NvdW50KSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBiYWRnZUNsYXNzZXMgPSBjbGFzc05hbWVzKHtcclxuICAgICAgICAgICAgICAgICAgICAnbXhfUm9vbVRpbGVfYmFkZ2UnOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICdteF9Sb29tVGlsZV9iYWRnZUJ1dHRvbic6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgJ214X1Jvb21UaWxlX2JhZGdlUmVkJzogci5yZWRCYWRnZSxcclxuICAgICAgICAgICAgICAgICAgICAnbXhfUm9vbVRpbGVfYmFkZ2VVbnJlYWQnOiAhci5yZWRCYWRnZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGJhZGdlID0gPGRpdiBjbGFzc05hbWU9e2JhZGdlQ2xhc3Nlc30+e3IuZm9ybWF0dGVkQ291bnR9PC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgZG1JbmRpY2F0b3I7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0RtUm9vbShyLnJvb20pICYmICFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgICAgIGRtSW5kaWNhdG9yID0gPGltZ1xyXG4gICAgICAgICAgICAgICAgICAgIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbl9wZXJzb24uc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X1Jvb21CcmVhZGNydW1ic19kbUluZGljYXRvclwiXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxM1wiXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PVwiMTVcIlxyXG4gICAgICAgICAgICAgICAgICAgIGFsdD17X3QoXCJEaXJlY3QgQ2hhdFwiKX1cclxuICAgICAgICAgICAgICAgIC8+O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXN9XHJcbiAgICAgICAgICAgICAgICAgICAga2V5PXtyLnJvb20ucm9vbUlkfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuX3ZpZXdSb29tKHIucm9vbSwgaSl9XHJcbiAgICAgICAgICAgICAgICAgICAgb25Nb3VzZUVudGVyPXsoKSA9PiB0aGlzLl9vbk1vdXNlRW50ZXIoci5yb29tKX1cclxuICAgICAgICAgICAgICAgICAgICBvbk1vdXNlTGVhdmU9eygpID0+IHRoaXMuX29uTW91c2VMZWF2ZShyLnJvb20pfVxyXG4gICAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9e190KFwiUm9vbSAlKG5hbWUpc1wiLCB7bmFtZTogci5yb29tLm5hbWV9KX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8Um9vbUF2YXRhciByb29tPXtyLnJvb219IHdpZHRoPXszMn0gaGVpZ2h0PXszMn0gLz5cclxuICAgICAgICAgICAgICAgICAgICB7YmFkZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAge2RtSW5kaWNhdG9yfVxyXG4gICAgICAgICAgICAgICAgICAgIHt0b29sdGlwfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgcm9sZT1cInRvb2xiYXJcIiBhcmlhLWxhYmVsPXtfdChcIlJlY2VudCByb29tc1wiKX0+XHJcbiAgICAgICAgICAgICAgICA8SW5kaWNhdG9yU2Nyb2xsYmFyXHJcbiAgICAgICAgICAgICAgICAgICAgcmVmPXt0aGlzLl9zY3JvbGxlcn1cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9Sb29tQnJlYWRjcnVtYnNcIlxyXG4gICAgICAgICAgICAgICAgICAgIHRyYWNrSG9yaXpvbnRhbE92ZXJmbG93PXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIHZlcnRpY2FsU2Nyb2xsc0hvcml6b250YWxseT17dHJ1ZX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICB7IGF2YXRhcnMgfVxyXG4gICAgICAgICAgICAgICAgPC9JbmRpY2F0b3JTY3JvbGxiYXI+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19