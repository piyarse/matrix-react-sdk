"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _ratelimitedfunc = _interopRequireDefault(require("../../../ratelimitedfunc"));

var _HtmlUtils = require("../../../HtmlUtils");

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

var _ManageIntegsButton = _interopRequireDefault(require("../elements/ManageIntegsButton"));

var _SimpleRoomHeader = require("./SimpleRoomHeader");

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _RoomHeaderButtons = _interopRequireDefault(require("../right_panel/RoomHeaderButtons"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var _E2EIcon = _interopRequireDefault(require("./E2EIcon"));

var _InviteOnlyIcon = _interopRequireDefault(require("./InviteOnlyIcon"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'RoomHeader',
  propTypes: {
    room: _propTypes.default.object,
    oobData: _propTypes.default.object,
    inRoom: _propTypes.default.bool,
    onSettingsClick: _propTypes.default.func,
    onPinnedClick: _propTypes.default.func,
    onSearchClick: _propTypes.default.func,
    onLeaveClick: _propTypes.default.func,
    onCancelClick: _propTypes.default.func,
    e2eStatus: _propTypes.default.string
  },
  getDefaultProps: function () {
    return {
      editing: false,
      inRoom: false,
      onCancelClick: null
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._topic = (0, _react.createRef)();
  },
  componentDidMount: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    cli.on("RoomState.events", this._onRoomStateEvents);
    cli.on("Room.accountData", this._onRoomAccountData); // When a room name occurs, RoomState.events is fired *before*
    // room.name is updated. So we have to listen to Room.name as well as
    // RoomState.events.

    if (this.props.room) {
      this.props.room.on("Room.name", this._onRoomNameChange);
    }
  },
  componentDidUpdate: function () {
    if (this._topic.current) {
      (0, _HtmlUtils.linkifyElement)(this._topic.current);
    }
  },
  componentWillUnmount: function () {
    if (this.props.room) {
      this.props.room.removeListener("Room.name", this._onRoomNameChange);
    }

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli) {
      cli.removeListener("RoomState.events", this._onRoomStateEvents);
      cli.removeListener("Room.accountData", this._onRoomAccountData);
    }
  },
  _onRoomStateEvents: function (event, state) {
    if (!this.props.room || event.getRoomId() !== this.props.room.roomId) {
      return;
    } // redisplay the room name, topic, etc.


    this._rateLimitedUpdate();
  },
  _onRoomAccountData: function (event, room) {
    if (!this.props.room || room.roomId !== this.props.room.roomId) return;
    if (event.getType() !== "im.vector.room.read_pins") return;

    this._rateLimitedUpdate();
  },
  _rateLimitedUpdate: new _ratelimitedfunc.default(function () {
    /* eslint-disable babel/no-invalid-this */
    this.forceUpdate();
  }, 500),
  _onRoomNameChange: function (room) {
    this.forceUpdate();
  },
  onShareRoomClick: function (ev) {
    const ShareDialog = sdk.getComponent("dialogs.ShareDialog");

    _Modal.default.createTrackedDialog('share room dialog', '', ShareDialog, {
      target: this.props.room
    });
  },
  _hasUnreadPins: function () {
    const currentPinEvent = this.props.room.currentState.getStateEvents("m.room.pinned_events", '');
    if (!currentPinEvent) return false;

    if (currentPinEvent.getContent().pinned && currentPinEvent.getContent().pinned.length <= 0) {
      return false; // no pins == nothing to read
    }

    const readPinsEvent = this.props.room.getAccountData("im.vector.room.read_pins");

    if (readPinsEvent && readPinsEvent.getContent()) {
      const readStateEvents = readPinsEvent.getContent().event_ids || [];

      if (readStateEvents) {
        return !readStateEvents.includes(currentPinEvent.getId());
      }
    } // There's pins, and we haven't read any of them


    return true;
  },
  _hasPins: function () {
    const currentPinEvent = this.props.room.currentState.getStateEvents("m.room.pinned_events", '');
    if (!currentPinEvent) return false;
    return !(currentPinEvent.getContent().pinned && currentPinEvent.getContent().pinned.length <= 0);
  },
  render: function () {
    const RoomAvatar = sdk.getComponent("avatars.RoomAvatar");
    let searchStatus = null;
    let cancelButton = null;
    let settingsButton = null;
    let pinnedEventsButton = null;
    const e2eIcon = this.props.e2eStatus ? _react.default.createElement(_E2EIcon.default, {
      status: this.props.e2eStatus
    }) : undefined;

    const dmUserId = _DMRoomMap.default.shared().getUserIdForRoomId(this.props.room.roomId);

    const joinRules = this.props.room && this.props.room.currentState.getStateEvents("m.room.join_rules", "");
    const joinRule = joinRules && joinRules.getContent().join_rule;
    let privateIcon; // Don't show an invite-only icon for DMs. Users know they're invite-only.

    if (!dmUserId && _SettingsStore.default.isFeatureEnabled("feature_cross_signing")) {
      if (joinRule == "invite") {
        privateIcon = _react.default.createElement(_InviteOnlyIcon.default, null);
      }
    }

    if (this.props.onCancelClick) {
      cancelButton = _react.default.createElement(_SimpleRoomHeader.CancelButton, {
        onClick: this.props.onCancelClick
      });
    } // don't display the search count until the search completes and
    // gives us a valid (possibly zero) searchCount.


    if (this.props.searchInfo && this.props.searchInfo.searchCount !== undefined && this.props.searchInfo.searchCount !== null) {
      searchStatus = _react.default.createElement("div", {
        className: "mx_RoomHeader_searchStatus"
      }, "\xA0", (0, _languageHandler._t)("(~%(count)s results)", {
        count: this.props.searchInfo.searchCount
      }));
    } // XXX: this is a bit inefficient - we could just compare room.name for 'Empty room'...


    let settingsHint = false;
    const members = this.props.room ? this.props.room.getJoinedMembers() : undefined;

    if (members) {
      if (members.length === 1 && members[0].userId === _MatrixClientPeg.MatrixClientPeg.get().credentials.userId) {
        const nameEvent = this.props.room.currentState.getStateEvents('m.room.name', '');

        if (!nameEvent || !nameEvent.getContent().name) {
          settingsHint = true;
        }
      }
    }

    let roomName = (0, _languageHandler._t)("Join Room");

    if (this.props.oobData && this.props.oobData.name) {
      roomName = this.props.oobData.name;
    } else if (this.props.room) {
      roomName = this.props.room.name;
    }

    const textClasses = (0, _classnames.default)('mx_RoomHeader_nametext', {
      mx_RoomHeader_settingsHint: settingsHint
    });

    const name = _react.default.createElement("div", {
      className: "mx_RoomHeader_name",
      onClick: this.props.onSettingsClick
    }, _react.default.createElement("div", {
      dir: "auto",
      className: textClasses,
      title: roomName
    }, roomName), searchStatus);

    let topic;

    if (this.props.room) {
      const ev = this.props.room.currentState.getStateEvents('m.room.topic', '');

      if (ev) {
        topic = ev.getContent().topic;
      }
    }

    const topicElement = _react.default.createElement("div", {
      className: "mx_RoomHeader_topic",
      ref: this._topic,
      title: topic,
      dir: "auto"
    }, topic);

    const avatarSize = 28;
    let roomAvatar;

    if (this.props.room) {
      roomAvatar = _react.default.createElement(RoomAvatar, {
        room: this.props.room,
        width: avatarSize,
        height: avatarSize,
        oobData: this.props.oobData,
        viewAvatarOnClick: true
      });
    }

    if (this.props.onSettingsClick) {
      settingsButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RoomHeader_button mx_RoomHeader_settingsButton",
        onClick: this.props.onSettingsClick,
        title: (0, _languageHandler._t)("Settings")
      });
    }

    if (this.props.onPinnedClick && _SettingsStore.default.isFeatureEnabled('feature_pinning')) {
      let pinsIndicator = null;

      if (this._hasUnreadPins()) {
        pinsIndicator = _react.default.createElement("div", {
          className: "mx_RoomHeader_pinsIndicator mx_RoomHeader_pinsIndicatorUnread"
        });
      } else if (this._hasPins()) {
        pinsIndicator = _react.default.createElement("div", {
          className: "mx_RoomHeader_pinsIndicator"
        });
      }

      pinnedEventsButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RoomHeader_button mx_RoomHeader_pinnedButton",
        onClick: this.props.onPinnedClick,
        title: (0, _languageHandler._t)("Pinned Messages")
      }, pinsIndicator);
    } //        var leave_button;
    //        if (this.props.onLeaveClick) {
    //            leave_button =
    //                <div className="mx_RoomHeader_button" onClick={this.props.onLeaveClick} title="Leave room">
    //                    <TintableSvg src={require("../../../../res/img/leave.svg")} width="26" height="20"/>
    //                </div>;
    //        }


    let forgetButton;

    if (this.props.onForgetClick) {
      forgetButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RoomHeader_button mx_RoomHeader_forgetButton",
        onClick: this.props.onForgetClick,
        title: (0, _languageHandler._t)("Forget room")
      });
    }

    let searchButton;

    if (this.props.onSearchClick && this.props.inRoom) {
      searchButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RoomHeader_button mx_RoomHeader_searchButton",
        onClick: this.props.onSearchClick,
        title: (0, _languageHandler._t)("Search")
      });
    }

    let shareRoomButton;

    if (this.props.inRoom) {
      shareRoomButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_RoomHeader_button mx_RoomHeader_shareButton",
        onClick: this.onShareRoomClick,
        title: (0, _languageHandler._t)('Share room')
      });
    }

    let manageIntegsButton;

    if (this.props.room && this.props.room.roomId && this.props.inRoom) {
      manageIntegsButton = _react.default.createElement(_ManageIntegsButton.default, {
        room: this.props.room
      });
    }

    const rightRow = _react.default.createElement("div", {
      className: "mx_RoomHeader_buttons"
    }, settingsButton, pinnedEventsButton, shareRoomButton, manageIntegsButton, forgetButton, searchButton);

    return _react.default.createElement("div", {
      className: "mx_RoomHeader light-panel"
    }, _react.default.createElement("div", {
      className: "mx_RoomHeader_wrapper",
      "aria-owns": "mx_RightPanel"
    }, _react.default.createElement("div", {
      className: "mx_RoomHeader_avatar"
    }, roomAvatar, e2eIcon), privateIcon, name, topicElement, cancelButton, rightRow, _react.default.createElement(_RoomHeaderButtons.default, null)));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21IZWFkZXIuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJyb29tIiwiUHJvcFR5cGVzIiwib2JqZWN0Iiwib29iRGF0YSIsImluUm9vbSIsImJvb2wiLCJvblNldHRpbmdzQ2xpY2siLCJmdW5jIiwib25QaW5uZWRDbGljayIsIm9uU2VhcmNoQ2xpY2siLCJvbkxlYXZlQ2xpY2siLCJvbkNhbmNlbENsaWNrIiwiZTJlU3RhdHVzIiwic3RyaW5nIiwiZ2V0RGVmYXVsdFByb3BzIiwiZWRpdGluZyIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJfdG9waWMiLCJjb21wb25lbnREaWRNb3VudCIsImNsaSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm9uIiwiX29uUm9vbVN0YXRlRXZlbnRzIiwiX29uUm9vbUFjY291bnREYXRhIiwicHJvcHMiLCJfb25Sb29tTmFtZUNoYW5nZSIsImNvbXBvbmVudERpZFVwZGF0ZSIsImN1cnJlbnQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUxpc3RlbmVyIiwiZXZlbnQiLCJzdGF0ZSIsImdldFJvb21JZCIsInJvb21JZCIsIl9yYXRlTGltaXRlZFVwZGF0ZSIsImdldFR5cGUiLCJSYXRlTGltaXRlZEZ1bmMiLCJmb3JjZVVwZGF0ZSIsIm9uU2hhcmVSb29tQ2xpY2siLCJldiIsIlNoYXJlRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGFyZ2V0IiwiX2hhc1VucmVhZFBpbnMiLCJjdXJyZW50UGluRXZlbnQiLCJjdXJyZW50U3RhdGUiLCJnZXRTdGF0ZUV2ZW50cyIsImdldENvbnRlbnQiLCJwaW5uZWQiLCJsZW5ndGgiLCJyZWFkUGluc0V2ZW50IiwiZ2V0QWNjb3VudERhdGEiLCJyZWFkU3RhdGVFdmVudHMiLCJldmVudF9pZHMiLCJpbmNsdWRlcyIsImdldElkIiwiX2hhc1BpbnMiLCJyZW5kZXIiLCJSb29tQXZhdGFyIiwic2VhcmNoU3RhdHVzIiwiY2FuY2VsQnV0dG9uIiwic2V0dGluZ3NCdXR0b24iLCJwaW5uZWRFdmVudHNCdXR0b24iLCJlMmVJY29uIiwidW5kZWZpbmVkIiwiZG1Vc2VySWQiLCJETVJvb21NYXAiLCJzaGFyZWQiLCJnZXRVc2VySWRGb3JSb29tSWQiLCJqb2luUnVsZXMiLCJqb2luUnVsZSIsImpvaW5fcnVsZSIsInByaXZhdGVJY29uIiwiU2V0dGluZ3NTdG9yZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJzZWFyY2hJbmZvIiwic2VhcmNoQ291bnQiLCJjb3VudCIsInNldHRpbmdzSGludCIsIm1lbWJlcnMiLCJnZXRKb2luZWRNZW1iZXJzIiwidXNlcklkIiwiY3JlZGVudGlhbHMiLCJuYW1lRXZlbnQiLCJuYW1lIiwicm9vbU5hbWUiLCJ0ZXh0Q2xhc3NlcyIsIm14X1Jvb21IZWFkZXJfc2V0dGluZ3NIaW50IiwidG9waWMiLCJ0b3BpY0VsZW1lbnQiLCJhdmF0YXJTaXplIiwicm9vbUF2YXRhciIsInBpbnNJbmRpY2F0b3IiLCJmb3JnZXRCdXR0b24iLCJvbkZvcmdldENsaWNrIiwic2VhcmNoQnV0dG9uIiwic2hhcmVSb29tQnV0dG9uIiwibWFuYWdlSW50ZWdzQnV0dG9uIiwicmlnaHRSb3ciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQW5DQTs7Ozs7Ozs7Ozs7Ozs7OztlQXFDZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxZQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsSUFBSSxFQUFFQyxtQkFBVUMsTUFEVDtBQUVQQyxJQUFBQSxPQUFPLEVBQUVGLG1CQUFVQyxNQUZaO0FBR1BFLElBQUFBLE1BQU0sRUFBRUgsbUJBQVVJLElBSFg7QUFJUEMsSUFBQUEsZUFBZSxFQUFFTCxtQkFBVU0sSUFKcEI7QUFLUEMsSUFBQUEsYUFBYSxFQUFFUCxtQkFBVU0sSUFMbEI7QUFNUEUsSUFBQUEsYUFBYSxFQUFFUixtQkFBVU0sSUFObEI7QUFPUEcsSUFBQUEsWUFBWSxFQUFFVCxtQkFBVU0sSUFQakI7QUFRUEksSUFBQUEsYUFBYSxFQUFFVixtQkFBVU0sSUFSbEI7QUFTUEssSUFBQUEsU0FBUyxFQUFFWCxtQkFBVVk7QUFUZCxHQUhpQjtBQWU1QkMsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxPQUFPLEVBQUUsS0FETjtBQUVIWCxNQUFBQSxNQUFNLEVBQUUsS0FGTDtBQUdITyxNQUFBQSxhQUFhLEVBQUU7QUFIWixLQUFQO0FBS0gsR0FyQjJCO0FBdUI1QjtBQUNBSyxFQUFBQSx5QkFBeUIsRUFBRSxZQUFXO0FBQ2xDLFNBQUtDLE1BQUwsR0FBYyx1QkFBZDtBQUNILEdBMUIyQjtBQTRCNUJDLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsVUFBTUMsR0FBRyxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0FGLElBQUFBLEdBQUcsQ0FBQ0csRUFBSixDQUFPLGtCQUFQLEVBQTJCLEtBQUtDLGtCQUFoQztBQUNBSixJQUFBQSxHQUFHLENBQUNHLEVBQUosQ0FBTyxrQkFBUCxFQUEyQixLQUFLRSxrQkFBaEMsRUFIMEIsQ0FLMUI7QUFDQTtBQUNBOztBQUNBLFFBQUksS0FBS0MsS0FBTCxDQUFXekIsSUFBZixFQUFxQjtBQUNqQixXQUFLeUIsS0FBTCxDQUFXekIsSUFBWCxDQUFnQnNCLEVBQWhCLENBQW1CLFdBQW5CLEVBQWdDLEtBQUtJLGlCQUFyQztBQUNIO0FBQ0osR0F2QzJCO0FBeUM1QkMsRUFBQUEsa0JBQWtCLEVBQUUsWUFBVztBQUMzQixRQUFJLEtBQUtWLE1BQUwsQ0FBWVcsT0FBaEIsRUFBeUI7QUFDckIscUNBQWUsS0FBS1gsTUFBTCxDQUFZVyxPQUEzQjtBQUNIO0FBQ0osR0E3QzJCO0FBK0M1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QixRQUFJLEtBQUtKLEtBQUwsQ0FBV3pCLElBQWYsRUFBcUI7QUFDakIsV0FBS3lCLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0I4QixjQUFoQixDQUErQixXQUEvQixFQUE0QyxLQUFLSixpQkFBakQ7QUFDSDs7QUFDRCxVQUFNUCxHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxRQUFJRixHQUFKLEVBQVM7QUFDTEEsTUFBQUEsR0FBRyxDQUFDVyxjQUFKLENBQW1CLGtCQUFuQixFQUF1QyxLQUFLUCxrQkFBNUM7QUFDQUosTUFBQUEsR0FBRyxDQUFDVyxjQUFKLENBQW1CLGtCQUFuQixFQUF1QyxLQUFLTixrQkFBNUM7QUFDSDtBQUNKLEdBeEQyQjtBQTBENUJELEVBQUFBLGtCQUFrQixFQUFFLFVBQVNRLEtBQVQsRUFBZ0JDLEtBQWhCLEVBQXVCO0FBQ3ZDLFFBQUksQ0FBQyxLQUFLUCxLQUFMLENBQVd6QixJQUFaLElBQW9CK0IsS0FBSyxDQUFDRSxTQUFOLE9BQXNCLEtBQUtSLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JrQyxNQUE5RCxFQUFzRTtBQUNsRTtBQUNILEtBSHNDLENBS3ZDOzs7QUFDQSxTQUFLQyxrQkFBTDtBQUNILEdBakUyQjtBQW1FNUJYLEVBQUFBLGtCQUFrQixFQUFFLFVBQVNPLEtBQVQsRUFBZ0IvQixJQUFoQixFQUFzQjtBQUN0QyxRQUFJLENBQUMsS0FBS3lCLEtBQUwsQ0FBV3pCLElBQVosSUFBb0JBLElBQUksQ0FBQ2tDLE1BQUwsS0FBZ0IsS0FBS1QsS0FBTCxDQUFXekIsSUFBWCxDQUFnQmtDLE1BQXhELEVBQWdFO0FBQ2hFLFFBQUlILEtBQUssQ0FBQ0ssT0FBTixPQUFvQiwwQkFBeEIsRUFBb0Q7O0FBRXBELFNBQUtELGtCQUFMO0FBQ0gsR0F4RTJCO0FBMEU1QkEsRUFBQUEsa0JBQWtCLEVBQUUsSUFBSUUsd0JBQUosQ0FBb0IsWUFBVztBQUMvQztBQUNBLFNBQUtDLFdBQUw7QUFDSCxHQUhtQixFQUdqQixHQUhpQixDQTFFUTtBQStFNUJaLEVBQUFBLGlCQUFpQixFQUFFLFVBQVMxQixJQUFULEVBQWU7QUFDOUIsU0FBS3NDLFdBQUw7QUFDSCxHQWpGMkI7QUFtRjVCQyxFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTQyxFQUFULEVBQWE7QUFDM0IsVUFBTUMsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsbUJBQTFCLEVBQStDLEVBQS9DLEVBQW1ESixXQUFuRCxFQUFnRTtBQUM1REssTUFBQUEsTUFBTSxFQUFFLEtBQUtyQixLQUFMLENBQVd6QjtBQUR5QyxLQUFoRTtBQUdILEdBeEYyQjtBQTBGNUIrQyxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QixVQUFNQyxlQUFlLEdBQUcsS0FBS3ZCLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JpRCxZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsc0JBQTVDLEVBQW9FLEVBQXBFLENBQXhCO0FBQ0EsUUFBSSxDQUFDRixlQUFMLEVBQXNCLE9BQU8sS0FBUDs7QUFDdEIsUUFBSUEsZUFBZSxDQUFDRyxVQUFoQixHQUE2QkMsTUFBN0IsSUFBdUNKLGVBQWUsQ0FBQ0csVUFBaEIsR0FBNkJDLE1BQTdCLENBQW9DQyxNQUFwQyxJQUE4QyxDQUF6RixFQUE0RjtBQUN4RixhQUFPLEtBQVAsQ0FEd0YsQ0FDMUU7QUFDakI7O0FBRUQsVUFBTUMsYUFBYSxHQUFHLEtBQUs3QixLQUFMLENBQVd6QixJQUFYLENBQWdCdUQsY0FBaEIsQ0FBK0IsMEJBQS9CLENBQXRCOztBQUNBLFFBQUlELGFBQWEsSUFBSUEsYUFBYSxDQUFDSCxVQUFkLEVBQXJCLEVBQWlEO0FBQzdDLFlBQU1LLGVBQWUsR0FBR0YsYUFBYSxDQUFDSCxVQUFkLEdBQTJCTSxTQUEzQixJQUF3QyxFQUFoRTs7QUFDQSxVQUFJRCxlQUFKLEVBQXFCO0FBQ2pCLGVBQU8sQ0FBQ0EsZUFBZSxDQUFDRSxRQUFoQixDQUF5QlYsZUFBZSxDQUFDVyxLQUFoQixFQUF6QixDQUFSO0FBQ0g7QUFDSixLQWJzQixDQWV2Qjs7O0FBQ0EsV0FBTyxJQUFQO0FBQ0gsR0EzRzJCO0FBNkc1QkMsRUFBQUEsUUFBUSxFQUFFLFlBQVc7QUFDakIsVUFBTVosZUFBZSxHQUFHLEtBQUt2QixLQUFMLENBQVd6QixJQUFYLENBQWdCaUQsWUFBaEIsQ0FBNkJDLGNBQTdCLENBQTRDLHNCQUE1QyxFQUFvRSxFQUFwRSxDQUF4QjtBQUNBLFFBQUksQ0FBQ0YsZUFBTCxFQUFzQixPQUFPLEtBQVA7QUFFdEIsV0FBTyxFQUFFQSxlQUFlLENBQUNHLFVBQWhCLEdBQTZCQyxNQUE3QixJQUF1Q0osZUFBZSxDQUFDRyxVQUFoQixHQUE2QkMsTUFBN0IsQ0FBb0NDLE1BQXBDLElBQThDLENBQXZGLENBQVA7QUFDSCxHQWxIMkI7QUFvSDVCUSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFVBQVUsR0FBR3BCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7QUFFQSxRQUFJb0IsWUFBWSxHQUFHLElBQW5CO0FBQ0EsUUFBSUMsWUFBWSxHQUFHLElBQW5CO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLElBQXJCO0FBQ0EsUUFBSUMsa0JBQWtCLEdBQUcsSUFBekI7QUFFQSxVQUFNQyxPQUFPLEdBQUcsS0FBSzFDLEtBQUwsQ0FBV2IsU0FBWCxHQUNaLDZCQUFDLGdCQUFEO0FBQVMsTUFBQSxNQUFNLEVBQUUsS0FBS2EsS0FBTCxDQUFXYjtBQUE1QixNQURZLEdBRVp3RCxTQUZKOztBQUlBLFVBQU1DLFFBQVEsR0FBR0MsbUJBQVVDLE1BQVYsR0FBbUJDLGtCQUFuQixDQUFzQyxLQUFLL0MsS0FBTCxDQUFXekIsSUFBWCxDQUFnQmtDLE1BQXRELENBQWpCOztBQUNBLFVBQU11QyxTQUFTLEdBQUcsS0FBS2hELEtBQUwsQ0FBV3pCLElBQVgsSUFBbUIsS0FBS3lCLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JpRCxZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsbUJBQTVDLEVBQWlFLEVBQWpFLENBQXJDO0FBQ0EsVUFBTXdCLFFBQVEsR0FBR0QsU0FBUyxJQUFJQSxTQUFTLENBQUN0QixVQUFWLEdBQXVCd0IsU0FBckQ7QUFDQSxRQUFJQyxXQUFKLENBZmUsQ0FnQmY7O0FBQ0EsUUFBSSxDQUFDUCxRQUFELElBQWFRLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsQ0FBakIsRUFBMEU7QUFDdEUsVUFBSUosUUFBUSxJQUFJLFFBQWhCLEVBQTBCO0FBQ3RCRSxRQUFBQSxXQUFXLEdBQUcsNkJBQUMsdUJBQUQsT0FBZDtBQUNIO0FBQ0o7O0FBRUQsUUFBSSxLQUFLbkQsS0FBTCxDQUFXZCxhQUFmLEVBQThCO0FBQzFCcUQsTUFBQUEsWUFBWSxHQUFHLDZCQUFDLDhCQUFEO0FBQWMsUUFBQSxPQUFPLEVBQUUsS0FBS3ZDLEtBQUwsQ0FBV2Q7QUFBbEMsUUFBZjtBQUNILEtBekJjLENBMkJmO0FBQ0E7OztBQUNBLFFBQUksS0FBS2MsS0FBTCxDQUFXc0QsVUFBWCxJQUNBLEtBQUt0RCxLQUFMLENBQVdzRCxVQUFYLENBQXNCQyxXQUF0QixLQUFzQ1osU0FEdEMsSUFFQSxLQUFLM0MsS0FBTCxDQUFXc0QsVUFBWCxDQUFzQkMsV0FBdEIsS0FBc0MsSUFGMUMsRUFFZ0Q7QUFDNUNqQixNQUFBQSxZQUFZLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLGlCQUNULHlCQUFHLHNCQUFILEVBQTJCO0FBQUVrQixRQUFBQSxLQUFLLEVBQUUsS0FBS3hELEtBQUwsQ0FBV3NELFVBQVgsQ0FBc0JDO0FBQS9CLE9BQTNCLENBRFMsQ0FBZjtBQUdILEtBbkNjLENBcUNmOzs7QUFDQSxRQUFJRSxZQUFZLEdBQUcsS0FBbkI7QUFDQSxVQUFNQyxPQUFPLEdBQUcsS0FBSzFELEtBQUwsQ0FBV3pCLElBQVgsR0FBa0IsS0FBS3lCLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JvRixnQkFBaEIsRUFBbEIsR0FBdURoQixTQUF2RTs7QUFDQSxRQUFJZSxPQUFKLEVBQWE7QUFDVCxVQUFJQSxPQUFPLENBQUM5QixNQUFSLEtBQW1CLENBQW5CLElBQXdCOEIsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXRSxNQUFYLEtBQXNCakUsaUNBQWdCQyxHQUFoQixHQUFzQmlFLFdBQXRCLENBQWtDRCxNQUFwRixFQUE0RjtBQUN4RixjQUFNRSxTQUFTLEdBQUcsS0FBSzlELEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JpRCxZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsYUFBNUMsRUFBMkQsRUFBM0QsQ0FBbEI7O0FBQ0EsWUFBSSxDQUFDcUMsU0FBRCxJQUFjLENBQUNBLFNBQVMsQ0FBQ3BDLFVBQVYsR0FBdUJxQyxJQUExQyxFQUFnRDtBQUM1Q04sVUFBQUEsWUFBWSxHQUFHLElBQWY7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsUUFBSU8sUUFBUSxHQUFHLHlCQUFHLFdBQUgsQ0FBZjs7QUFDQSxRQUFJLEtBQUtoRSxLQUFMLENBQVd0QixPQUFYLElBQXNCLEtBQUtzQixLQUFMLENBQVd0QixPQUFYLENBQW1CcUYsSUFBN0MsRUFBbUQ7QUFDL0NDLE1BQUFBLFFBQVEsR0FBRyxLQUFLaEUsS0FBTCxDQUFXdEIsT0FBWCxDQUFtQnFGLElBQTlCO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBSy9ELEtBQUwsQ0FBV3pCLElBQWYsRUFBcUI7QUFDeEJ5RixNQUFBQSxRQUFRLEdBQUcsS0FBS2hFLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0J3RixJQUEzQjtBQUNIOztBQUVELFVBQU1FLFdBQVcsR0FBRyx5QkFBVyx3QkFBWCxFQUFxQztBQUFFQyxNQUFBQSwwQkFBMEIsRUFBRVQ7QUFBOUIsS0FBckMsQ0FBcEI7O0FBQ0EsVUFBTU0sSUFBSSxHQUNOO0FBQUssTUFBQSxTQUFTLEVBQUMsb0JBQWY7QUFBb0MsTUFBQSxPQUFPLEVBQUUsS0FBSy9ELEtBQUwsQ0FBV25CO0FBQXhELE9BQ0k7QUFBSyxNQUFBLEdBQUcsRUFBQyxNQUFUO0FBQWdCLE1BQUEsU0FBUyxFQUFFb0YsV0FBM0I7QUFBd0MsTUFBQSxLQUFLLEVBQUVEO0FBQS9DLE9BQTJEQSxRQUEzRCxDQURKLEVBRU0xQixZQUZOLENBREo7O0FBTUEsUUFBSTZCLEtBQUo7O0FBQ0EsUUFBSSxLQUFLbkUsS0FBTCxDQUFXekIsSUFBZixFQUFxQjtBQUNqQixZQUFNd0MsRUFBRSxHQUFHLEtBQUtmLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JpRCxZQUFoQixDQUE2QkMsY0FBN0IsQ0FBNEMsY0FBNUMsRUFBNEQsRUFBNUQsQ0FBWDs7QUFDQSxVQUFJVixFQUFKLEVBQVE7QUFDSm9ELFFBQUFBLEtBQUssR0FBR3BELEVBQUUsQ0FBQ1csVUFBSCxHQUFnQnlDLEtBQXhCO0FBQ0g7QUFDSjs7QUFDRCxVQUFNQyxZQUFZLEdBQ2Q7QUFBSyxNQUFBLFNBQVMsRUFBQyxxQkFBZjtBQUFxQyxNQUFBLEdBQUcsRUFBRSxLQUFLNUUsTUFBL0M7QUFBdUQsTUFBQSxLQUFLLEVBQUUyRSxLQUE5RDtBQUFxRSxNQUFBLEdBQUcsRUFBQztBQUF6RSxPQUFrRkEsS0FBbEYsQ0FESjs7QUFFQSxVQUFNRSxVQUFVLEdBQUcsRUFBbkI7QUFDQSxRQUFJQyxVQUFKOztBQUNBLFFBQUksS0FBS3RFLEtBQUwsQ0FBV3pCLElBQWYsRUFBcUI7QUFDakIrRixNQUFBQSxVQUFVLEdBQUksNkJBQUMsVUFBRDtBQUNWLFFBQUEsSUFBSSxFQUFFLEtBQUt0RSxLQUFMLENBQVd6QixJQURQO0FBRVYsUUFBQSxLQUFLLEVBQUU4RixVQUZHO0FBR1YsUUFBQSxNQUFNLEVBQUVBLFVBSEU7QUFJVixRQUFBLE9BQU8sRUFBRSxLQUFLckUsS0FBTCxDQUFXdEIsT0FKVjtBQUtWLFFBQUEsaUJBQWlCLEVBQUU7QUFMVCxRQUFkO0FBTUg7O0FBRUQsUUFBSSxLQUFLc0IsS0FBTCxDQUFXbkIsZUFBZixFQUFnQztBQUM1QjJELE1BQUFBLGNBQWMsR0FDViw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxtREFBNUI7QUFDSSxRQUFBLE9BQU8sRUFBRSxLQUFLeEMsS0FBTCxDQUFXbkIsZUFEeEI7QUFFSSxRQUFBLEtBQUssRUFBRSx5QkFBRyxVQUFIO0FBRlgsUUFESjtBQU1IOztBQUVELFFBQUksS0FBS21CLEtBQUwsQ0FBV2pCLGFBQVgsSUFBNEJxRSx1QkFBY0MsZ0JBQWQsQ0FBK0IsaUJBQS9CLENBQWhDLEVBQW1GO0FBQy9FLFVBQUlrQixhQUFhLEdBQUcsSUFBcEI7O0FBQ0EsVUFBSSxLQUFLakQsY0FBTCxFQUFKLEVBQTJCO0FBQ3ZCaUQsUUFBQUEsYUFBYSxHQUFJO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixVQUFqQjtBQUNILE9BRkQsTUFFTyxJQUFJLEtBQUtwQyxRQUFMLEVBQUosRUFBcUI7QUFDeEJvQyxRQUFBQSxhQUFhLEdBQUk7QUFBSyxVQUFBLFNBQVMsRUFBQztBQUFmLFVBQWpCO0FBQ0g7O0FBRUQ5QixNQUFBQSxrQkFBa0IsR0FDZCw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxpREFBNUI7QUFDa0IsUUFBQSxPQUFPLEVBQUUsS0FBS3pDLEtBQUwsQ0FBV2pCLGFBRHRDO0FBQ3FELFFBQUEsS0FBSyxFQUFFLHlCQUFHLGlCQUFIO0FBRDVELFNBRU13RixhQUZOLENBREo7QUFLSCxLQXpHYyxDQTJHdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVRLFFBQUlDLFlBQUo7O0FBQ0EsUUFBSSxLQUFLeEUsS0FBTCxDQUFXeUUsYUFBZixFQUE4QjtBQUMxQkQsTUFBQUEsWUFBWSxHQUNSLDZCQUFDLHlCQUFEO0FBQWtCLFFBQUEsU0FBUyxFQUFDLGlEQUE1QjtBQUNJLFFBQUEsT0FBTyxFQUFFLEtBQUt4RSxLQUFMLENBQVd5RSxhQUR4QjtBQUVJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLGFBQUg7QUFGWCxRQURKO0FBTUg7O0FBRUQsUUFBSUMsWUFBSjs7QUFDQSxRQUFJLEtBQUsxRSxLQUFMLENBQVdoQixhQUFYLElBQTRCLEtBQUtnQixLQUFMLENBQVdyQixNQUEzQyxFQUFtRDtBQUMvQytGLE1BQUFBLFlBQVksR0FDUiw2QkFBQyx5QkFBRDtBQUFrQixRQUFBLFNBQVMsRUFBQyxpREFBNUI7QUFDSSxRQUFBLE9BQU8sRUFBRSxLQUFLMUUsS0FBTCxDQUFXaEIsYUFEeEI7QUFFSSxRQUFBLEtBQUssRUFBRSx5QkFBRyxRQUFIO0FBRlgsUUFESjtBQU1IOztBQUVELFFBQUkyRixlQUFKOztBQUNBLFFBQUksS0FBSzNFLEtBQUwsQ0FBV3JCLE1BQWYsRUFBdUI7QUFDbkJnRyxNQUFBQSxlQUFlLEdBQ1gsNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMsZ0RBQTVCO0FBQ0ksUUFBQSxPQUFPLEVBQUUsS0FBSzdELGdCQURsQjtBQUVJLFFBQUEsS0FBSyxFQUFFLHlCQUFHLFlBQUg7QUFGWCxRQURKO0FBTUg7O0FBRUQsUUFBSThELGtCQUFKOztBQUNBLFFBQUksS0FBSzVFLEtBQUwsQ0FBV3pCLElBQVgsSUFBbUIsS0FBS3lCLEtBQUwsQ0FBV3pCLElBQVgsQ0FBZ0JrQyxNQUFuQyxJQUE2QyxLQUFLVCxLQUFMLENBQVdyQixNQUE1RCxFQUFvRTtBQUNoRWlHLE1BQUFBLGtCQUFrQixHQUFHLDZCQUFDLDJCQUFEO0FBQ2pCLFFBQUEsSUFBSSxFQUFFLEtBQUs1RSxLQUFMLENBQVd6QjtBQURBLFFBQXJCO0FBR0g7O0FBRUQsVUFBTXNHLFFBQVEsR0FDVjtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTXJDLGNBRE4sRUFFTUMsa0JBRk4sRUFHTWtDLGVBSE4sRUFJTUMsa0JBSk4sRUFLTUosWUFMTixFQU1NRSxZQU5OLENBREo7O0FBVUEsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHVCQUFmO0FBQXVDLG1CQUFVO0FBQWpELE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQXdDSixVQUF4QyxFQUFzRDVCLE9BQXRELENBREosRUFFTVMsV0FGTixFQUdNWSxJQUhOLEVBSU1LLFlBSk4sRUFLTTdCLFlBTE4sRUFNTXNDLFFBTk4sRUFPSSw2QkFBQywwQkFBRCxPQVBKLENBREosQ0FESjtBQWFIO0FBblMyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QsIHtjcmVhdGVSZWZ9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vLi4vLi4vTW9kYWxcIjtcclxuaW1wb3J0IFJhdGVMaW1pdGVkRnVuYyBmcm9tICcuLi8uLi8uLi9yYXRlbGltaXRlZGZ1bmMnO1xyXG5cclxuaW1wb3J0IHsgbGlua2lmeUVsZW1lbnQgfSBmcm9tICcuLi8uLi8uLi9IdG1sVXRpbHMnO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tICcuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uJztcclxuaW1wb3J0IE1hbmFnZUludGVnc0J1dHRvbiBmcm9tICcuLi9lbGVtZW50cy9NYW5hZ2VJbnRlZ3NCdXR0b24nO1xyXG5pbXBvcnQge0NhbmNlbEJ1dHRvbn0gZnJvbSAnLi9TaW1wbGVSb29tSGVhZGVyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uLy4uLy4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IFJvb21IZWFkZXJCdXR0b25zIGZyb20gJy4uL3JpZ2h0X3BhbmVsL1Jvb21IZWFkZXJCdXR0b25zJztcclxuaW1wb3J0IERNUm9vbU1hcCBmcm9tICcuLi8uLi8uLi91dGlscy9ETVJvb21NYXAnO1xyXG5pbXBvcnQgRTJFSWNvbiBmcm9tICcuL0UyRUljb24nO1xyXG5pbXBvcnQgSW52aXRlT25seUljb24gZnJvbSAnLi9JbnZpdGVPbmx5SWNvbic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnUm9vbUhlYWRlcicsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgcm9vbTogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgICAgICBvb2JEYXRhOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgICAgIGluUm9vbTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgb25TZXR0aW5nc0NsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBvblBpbm5lZENsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBvblNlYXJjaENsaWNrOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgICAgICBvbkxlYXZlQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uQ2FuY2VsQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIGUyZVN0YXR1czogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBlZGl0aW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgaW5Sb29tOiBmYWxzZSxcclxuICAgICAgICAgICAgb25DYW5jZWxDbGljazogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3RvcGljID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLl9vblJvb21TdGF0ZUV2ZW50cyk7XHJcbiAgICAgICAgY2xpLm9uKFwiUm9vbS5hY2NvdW50RGF0YVwiLCB0aGlzLl9vblJvb21BY2NvdW50RGF0YSk7XHJcblxyXG4gICAgICAgIC8vIFdoZW4gYSByb29tIG5hbWUgb2NjdXJzLCBSb29tU3RhdGUuZXZlbnRzIGlzIGZpcmVkICpiZWZvcmUqXHJcbiAgICAgICAgLy8gcm9vbS5uYW1lIGlzIHVwZGF0ZWQuIFNvIHdlIGhhdmUgdG8gbGlzdGVuIHRvIFJvb20ubmFtZSBhcyB3ZWxsIGFzXHJcbiAgICAgICAgLy8gUm9vbVN0YXRlLmV2ZW50cy5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5yb29tKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucm9vbS5vbihcIlJvb20ubmFtZVwiLCB0aGlzLl9vblJvb21OYW1lQ2hhbmdlKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3RvcGljLmN1cnJlbnQpIHtcclxuICAgICAgICAgICAgbGlua2lmeUVsZW1lbnQodGhpcy5fdG9waWMuY3VycmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucm9vbSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLnJvb20ucmVtb3ZlTGlzdGVuZXIoXCJSb29tLm5hbWVcIiwgdGhpcy5fb25Sb29tTmFtZUNoYW5nZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoY2xpKSB7XHJcbiAgICAgICAgICAgIGNsaS5yZW1vdmVMaXN0ZW5lcihcIlJvb21TdGF0ZS5ldmVudHNcIiwgdGhpcy5fb25Sb29tU3RhdGVFdmVudHMpO1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoXCJSb29tLmFjY291bnREYXRhXCIsIHRoaXMuX29uUm9vbUFjY291bnREYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJvb21TdGF0ZUV2ZW50czogZnVuY3Rpb24oZXZlbnQsIHN0YXRlKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLnJvb20gfHwgZXZlbnQuZ2V0Um9vbUlkKCkgIT09IHRoaXMucHJvcHMucm9vbS5yb29tSWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcmVkaXNwbGF5IHRoZSByb29tIG5hbWUsIHRvcGljLCBldGMuXHJcbiAgICAgICAgdGhpcy5fcmF0ZUxpbWl0ZWRVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uUm9vbUFjY291bnREYXRhOiBmdW5jdGlvbihldmVudCwgcm9vbSkge1xyXG4gICAgICAgIGlmICghdGhpcy5wcm9wcy5yb29tIHx8IHJvb20ucm9vbUlkICE9PSB0aGlzLnByb3BzLnJvb20ucm9vbUlkKSByZXR1cm47XHJcbiAgICAgICAgaWYgKGV2ZW50LmdldFR5cGUoKSAhPT0gXCJpbS52ZWN0b3Iucm9vbS5yZWFkX3BpbnNcIikgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLl9yYXRlTGltaXRlZFVwZGF0ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmF0ZUxpbWl0ZWRVcGRhdGU6IG5ldyBSYXRlTGltaXRlZEZ1bmMoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLyogZXNsaW50LWRpc2FibGUgYmFiZWwvbm8taW52YWxpZC10aGlzICovXHJcbiAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xyXG4gICAgfSwgNTAwKSxcclxuXHJcbiAgICBfb25Sb29tTmFtZUNoYW5nZTogZnVuY3Rpb24ocm9vbSkge1xyXG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25TaGFyZVJvb21DbGljazogZnVuY3Rpb24oZXYpIHtcclxuICAgICAgICBjb25zdCBTaGFyZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlNoYXJlRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ3NoYXJlIHJvb20gZGlhbG9nJywgJycsIFNoYXJlRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRhcmdldDogdGhpcy5wcm9wcy5yb29tLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaGFzVW5yZWFkUGluczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFBpbkV2ZW50ID0gdGhpcy5wcm9wcy5yb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cyhcIm0ucm9vbS5waW5uZWRfZXZlbnRzXCIsICcnKTtcclxuICAgICAgICBpZiAoIWN1cnJlbnRQaW5FdmVudCkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIGlmIChjdXJyZW50UGluRXZlbnQuZ2V0Q29udGVudCgpLnBpbm5lZCAmJiBjdXJyZW50UGluRXZlbnQuZ2V0Q29udGVudCgpLnBpbm5lZC5sZW5ndGggPD0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7IC8vIG5vIHBpbnMgPT0gbm90aGluZyB0byByZWFkXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByZWFkUGluc0V2ZW50ID0gdGhpcy5wcm9wcy5yb29tLmdldEFjY291bnREYXRhKFwiaW0udmVjdG9yLnJvb20ucmVhZF9waW5zXCIpO1xyXG4gICAgICAgIGlmIChyZWFkUGluc0V2ZW50ICYmIHJlYWRQaW5zRXZlbnQuZ2V0Q29udGVudCgpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlYWRTdGF0ZUV2ZW50cyA9IHJlYWRQaW5zRXZlbnQuZ2V0Q29udGVudCgpLmV2ZW50X2lkcyB8fCBbXTtcclxuICAgICAgICAgICAgaWYgKHJlYWRTdGF0ZUV2ZW50cykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICFyZWFkU3RhdGVFdmVudHMuaW5jbHVkZXMoY3VycmVudFBpbkV2ZW50LmdldElkKCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBUaGVyZSdzIHBpbnMsIGFuZCB3ZSBoYXZlbid0IHJlYWQgYW55IG9mIHRoZW1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0sXHJcblxyXG4gICAgX2hhc1BpbnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRQaW5FdmVudCA9IHRoaXMucHJvcHMucm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20ucGlubmVkX2V2ZW50c1wiLCAnJyk7XHJcbiAgICAgICAgaWYgKCFjdXJyZW50UGluRXZlbnQpIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgcmV0dXJuICEoY3VycmVudFBpbkV2ZW50LmdldENvbnRlbnQoKS5waW5uZWQgJiYgY3VycmVudFBpbkV2ZW50LmdldENvbnRlbnQoKS5waW5uZWQubGVuZ3RoIDw9IDApO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFJvb21BdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KFwiYXZhdGFycy5Sb29tQXZhdGFyXCIpO1xyXG5cclxuICAgICAgICBsZXQgc2VhcmNoU3RhdHVzID0gbnVsbDtcclxuICAgICAgICBsZXQgY2FuY2VsQnV0dG9uID0gbnVsbDtcclxuICAgICAgICBsZXQgc2V0dGluZ3NCdXR0b24gPSBudWxsO1xyXG4gICAgICAgIGxldCBwaW5uZWRFdmVudHNCdXR0b24gPSBudWxsO1xyXG5cclxuICAgICAgICBjb25zdCBlMmVJY29uID0gdGhpcy5wcm9wcy5lMmVTdGF0dXMgP1xyXG4gICAgICAgICAgICA8RTJFSWNvbiBzdGF0dXM9e3RoaXMucHJvcHMuZTJlU3RhdHVzfSAvPiA6XHJcbiAgICAgICAgICAgIHVuZGVmaW5lZDtcclxuXHJcbiAgICAgICAgY29uc3QgZG1Vc2VySWQgPSBETVJvb21NYXAuc2hhcmVkKCkuZ2V0VXNlcklkRm9yUm9vbUlkKHRoaXMucHJvcHMucm9vbS5yb29tSWQpO1xyXG4gICAgICAgIGNvbnN0IGpvaW5SdWxlcyA9IHRoaXMucHJvcHMucm9vbSAmJiB0aGlzLnByb3BzLnJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLmpvaW5fcnVsZXNcIiwgXCJcIik7XHJcbiAgICAgICAgY29uc3Qgam9pblJ1bGUgPSBqb2luUnVsZXMgJiYgam9pblJ1bGVzLmdldENvbnRlbnQoKS5qb2luX3J1bGU7XHJcbiAgICAgICAgbGV0IHByaXZhdGVJY29uO1xyXG4gICAgICAgIC8vIERvbid0IHNob3cgYW4gaW52aXRlLW9ubHkgaWNvbiBmb3IgRE1zLiBVc2VycyBrbm93IHRoZXkncmUgaW52aXRlLW9ubHkuXHJcbiAgICAgICAgaWYgKCFkbVVzZXJJZCAmJiBTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikpIHtcclxuICAgICAgICAgICAgaWYgKGpvaW5SdWxlID09IFwiaW52aXRlXCIpIHtcclxuICAgICAgICAgICAgICAgIHByaXZhdGVJY29uID0gPEludml0ZU9ubHlJY29uIC8+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkNhbmNlbENsaWNrKSB7XHJcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvbiA9IDxDYW5jZWxCdXR0b24gb25DbGljaz17dGhpcy5wcm9wcy5vbkNhbmNlbENsaWNrfSAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGRvbid0IGRpc3BsYXkgdGhlIHNlYXJjaCBjb3VudCB1bnRpbCB0aGUgc2VhcmNoIGNvbXBsZXRlcyBhbmRcclxuICAgICAgICAvLyBnaXZlcyB1cyBhIHZhbGlkIChwb3NzaWJseSB6ZXJvKSBzZWFyY2hDb3VudC5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5zZWFyY2hJbmZvICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc2VhcmNoSW5mby5zZWFyY2hDb3VudCAhPT0gdW5kZWZpbmVkICYmXHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc2VhcmNoSW5mby5zZWFyY2hDb3VudCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBzZWFyY2hTdGF0dXMgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfc2VhcmNoU3RhdHVzXCI+Jm5ic3A7XHJcbiAgICAgICAgICAgICAgICB7IF90KFwiKH4lKGNvdW50KXMgcmVzdWx0cylcIiwgeyBjb3VudDogdGhpcy5wcm9wcy5zZWFyY2hJbmZvLnNlYXJjaENvdW50IH0pIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gWFhYOiB0aGlzIGlzIGEgYml0IGluZWZmaWNpZW50IC0gd2UgY291bGQganVzdCBjb21wYXJlIHJvb20ubmFtZSBmb3IgJ0VtcHR5IHJvb20nLi4uXHJcbiAgICAgICAgbGV0IHNldHRpbmdzSGludCA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IG1lbWJlcnMgPSB0aGlzLnByb3BzLnJvb20gPyB0aGlzLnByb3BzLnJvb20uZ2V0Sm9pbmVkTWVtYmVycygpIDogdW5kZWZpbmVkO1xyXG4gICAgICAgIGlmIChtZW1iZXJzKSB7XHJcbiAgICAgICAgICAgIGlmIChtZW1iZXJzLmxlbmd0aCA9PT0gMSAmJiBtZW1iZXJzWzBdLnVzZXJJZCA9PT0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWRlbnRpYWxzLnVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbmFtZUV2ZW50ID0gdGhpcy5wcm9wcy5yb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLm5hbWUnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIW5hbWVFdmVudCB8fCAhbmFtZUV2ZW50LmdldENvbnRlbnQoKS5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3NIaW50ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHJvb21OYW1lID0gX3QoXCJKb2luIFJvb21cIik7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub29iRGF0YSAmJiB0aGlzLnByb3BzLm9vYkRhdGEubmFtZSkge1xyXG4gICAgICAgICAgICByb29tTmFtZSA9IHRoaXMucHJvcHMub29iRGF0YS5uYW1lO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5yb29tKSB7XHJcbiAgICAgICAgICAgIHJvb21OYW1lID0gdGhpcy5wcm9wcy5yb29tLm5hbWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0ZXh0Q2xhc3NlcyA9IGNsYXNzTmFtZXMoJ214X1Jvb21IZWFkZXJfbmFtZXRleHQnLCB7IG14X1Jvb21IZWFkZXJfc2V0dGluZ3NIaW50OiBzZXR0aW5nc0hpbnQgfSk7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl9uYW1lXCIgb25DbGljaz17dGhpcy5wcm9wcy5vblNldHRpbmdzQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBkaXI9XCJhdXRvXCIgY2xhc3NOYW1lPXt0ZXh0Q2xhc3Nlc30gdGl0bGU9e3Jvb21OYW1lfT57IHJvb21OYW1lIH08L2Rpdj5cclxuICAgICAgICAgICAgICAgIHsgc2VhcmNoU3RhdHVzIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG5cclxuICAgICAgICBsZXQgdG9waWM7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucm9vbSkge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IHRoaXMucHJvcHMucm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS50b3BpYycsICcnKTtcclxuICAgICAgICAgICAgaWYgKGV2KSB7XHJcbiAgICAgICAgICAgICAgICB0b3BpYyA9IGV2LmdldENvbnRlbnQoKS50b3BpYztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB0b3BpY0VsZW1lbnQgPVxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfdG9waWNcIiByZWY9e3RoaXMuX3RvcGljfSB0aXRsZT17dG9waWN9IGRpcj1cImF1dG9cIj57IHRvcGljIH08L2Rpdj47XHJcbiAgICAgICAgY29uc3QgYXZhdGFyU2l6ZSA9IDI4O1xyXG4gICAgICAgIGxldCByb29tQXZhdGFyO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJvb20pIHtcclxuICAgICAgICAgICAgcm9vbUF2YXRhciA9ICg8Um9vbUF2YXRhclxyXG4gICAgICAgICAgICAgICAgcm9vbT17dGhpcy5wcm9wcy5yb29tfVxyXG4gICAgICAgICAgICAgICAgd2lkdGg9e2F2YXRhclNpemV9XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ9e2F2YXRhclNpemV9XHJcbiAgICAgICAgICAgICAgICBvb2JEYXRhPXt0aGlzLnByb3BzLm9vYkRhdGF9XHJcbiAgICAgICAgICAgICAgICB2aWV3QXZhdGFyT25DbGljaz17dHJ1ZX0gLz4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25TZXR0aW5nc0NsaWNrKSB7XHJcbiAgICAgICAgICAgIHNldHRpbmdzQnV0dG9uID1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfYnV0dG9uIG14X1Jvb21IZWFkZXJfc2V0dGluZ3NCdXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMucHJvcHMub25TZXR0aW5nc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlNldHRpbmdzXCIpfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uUGlubmVkQ2xpY2sgJiYgU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKCdmZWF0dXJlX3Bpbm5pbmcnKSkge1xyXG4gICAgICAgICAgICBsZXQgcGluc0luZGljYXRvciA9IG51bGw7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9oYXNVbnJlYWRQaW5zKCkpIHtcclxuICAgICAgICAgICAgICAgIHBpbnNJbmRpY2F0b3IgPSAoPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tSGVhZGVyX3BpbnNJbmRpY2F0b3IgbXhfUm9vbUhlYWRlcl9waW5zSW5kaWNhdG9yVW5yZWFkXCIgLz4pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuX2hhc1BpbnMoKSkge1xyXG4gICAgICAgICAgICAgICAgcGluc0luZGljYXRvciA9ICg8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfcGluc0luZGljYXRvclwiIC8+KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcGlubmVkRXZlbnRzQnV0dG9uID1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfYnV0dG9uIG14X1Jvb21IZWFkZXJfcGlubmVkQnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMucHJvcHMub25QaW5uZWRDbGlja30gdGl0bGU9e190KFwiUGlubmVkIE1lc3NhZ2VzXCIpfT5cclxuICAgICAgICAgICAgICAgICAgICB7IHBpbnNJbmRpY2F0b3IgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4vLyAgICAgICAgdmFyIGxlYXZlX2J1dHRvbjtcclxuLy8gICAgICAgIGlmICh0aGlzLnByb3BzLm9uTGVhdmVDbGljaykge1xyXG4vLyAgICAgICAgICAgIGxlYXZlX2J1dHRvbiA9XHJcbi8vICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl9idXR0b25cIiBvbkNsaWNrPXt0aGlzLnByb3BzLm9uTGVhdmVDbGlja30gdGl0bGU9XCJMZWF2ZSByb29tXCI+XHJcbi8vICAgICAgICAgICAgICAgICAgICA8VGludGFibGVTdmcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9sZWF2ZS5zdmdcIil9IHdpZHRoPVwiMjZcIiBoZWlnaHQ9XCIyMFwiLz5cclxuLy8gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4vLyAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZm9yZ2V0QnV0dG9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRm9yZ2V0Q2xpY2spIHtcclxuICAgICAgICAgICAgZm9yZ2V0QnV0dG9uID1cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXJfYnV0dG9uIG14X1Jvb21IZWFkZXJfZm9yZ2V0QnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLnByb3BzLm9uRm9yZ2V0Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU9e190KFwiRm9yZ2V0IHJvb21cIil9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHNlYXJjaEJ1dHRvbjtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vblNlYXJjaENsaWNrICYmIHRoaXMucHJvcHMuaW5Sb29tKSB7XHJcbiAgICAgICAgICAgIHNlYXJjaEJ1dHRvbiA9XHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9Sb29tSGVhZGVyX2J1dHRvbiBteF9Sb29tSGVhZGVyX3NlYXJjaEJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5wcm9wcy5vblNlYXJjaENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdChcIlNlYXJjaFwiKX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2hhcmVSb29tQnV0dG9uO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmluUm9vbSkge1xyXG4gICAgICAgICAgICBzaGFyZVJvb21CdXR0b24gPVxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl9idXR0b24gbXhfUm9vbUhlYWRlcl9zaGFyZUJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vblNoYXJlUm9vbUNsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtfdCgnU2hhcmUgcm9vbScpfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBtYW5hZ2VJbnRlZ3NCdXR0b247XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucm9vbSAmJiB0aGlzLnByb3BzLnJvb20ucm9vbUlkICYmIHRoaXMucHJvcHMuaW5Sb29tKSB7XHJcbiAgICAgICAgICAgIG1hbmFnZUludGVnc0J1dHRvbiA9IDxNYW5hZ2VJbnRlZ3NCdXR0b25cclxuICAgICAgICAgICAgICAgIHJvb209e3RoaXMucHJvcHMucm9vbX1cclxuICAgICAgICAgICAgLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByaWdodFJvdyA9XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl9idXR0b25zXCI+XHJcbiAgICAgICAgICAgICAgICB7IHNldHRpbmdzQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgcGlubmVkRXZlbnRzQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgc2hhcmVSb29tQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgbWFuYWdlSW50ZWdzQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgZm9yZ2V0QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgc2VhcmNoQnV0dG9uIH1cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1Jvb21IZWFkZXIgbGlnaHQtcGFuZWxcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl93cmFwcGVyXCIgYXJpYS1vd25zPVwibXhfUmlnaHRQYW5lbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbUhlYWRlcl9hdmF0YXJcIj57IHJvb21BdmF0YXIgfXsgZTJlSWNvbiB9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBwcml2YXRlSWNvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBuYW1lIH1cclxuICAgICAgICAgICAgICAgICAgICB7IHRvcGljRWxlbWVudCB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBjYW5jZWxCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgcmlnaHRSb3cgfVxyXG4gICAgICAgICAgICAgICAgICAgIDxSb29tSGVhZGVyQnV0dG9ucyAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19