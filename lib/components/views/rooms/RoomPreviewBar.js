"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _classnames = _interopRequireDefault(require("classnames"));

var _languageHandler = require("../../../languageHandler");

var _IdentityAuthClient = _interopRequireDefault(require("../../../IdentityAuthClient"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const MessageCase = Object.freeze({
  NotLoggedIn: "NotLoggedIn",
  Joining: "Joining",
  Loading: "Loading",
  Rejecting: "Rejecting",
  Kicked: "Kicked",
  Banned: "Banned",
  OtherThreePIDError: "OtherThreePIDError",
  InvitedEmailNotFoundInAccount: "InvitedEmailNotFoundInAccount",
  InvitedEmailNoIdentityServer: "InvitedEmailNoIdentityServer",
  InvitedEmailMismatch: "InvitedEmailMismatch",
  Invite: "Invite",
  ViewingRoom: "ViewingRoom",
  RoomNotFound: "RoomNotFound",
  OtherError: "OtherError"
});

var _default = (0, _createReactClass.default)({
  displayName: 'RoomPreviewBar',
  propTypes: {
    onJoinClick: _propTypes.default.func,
    onRejectClick: _propTypes.default.func,
    onRejectAndIgnoreClick: _propTypes.default.func,
    onForgetClick: _propTypes.default.func,
    // if inviterName is specified, the preview bar will shown an invite to the room.
    // You should also specify onRejectClick if specifiying inviterName
    inviterName: _propTypes.default.string,
    // If invited by 3rd party invite, the email address the invite was sent to
    invitedEmail: _propTypes.default.string,
    // For third party invites, information passed about the room out-of-band
    oobData: _propTypes.default.object,
    // For third party invites, a URL for a 3pid invite signing service
    signUrl: _propTypes.default.string,
    // A standard client/server API error object. If supplied, indicates that the
    // caller was unable to fetch details about the room for the given reason.
    error: _propTypes.default.object,
    canPreview: _propTypes.default.bool,
    previewLoading: _propTypes.default.bool,
    room: _propTypes.default.object,
    // When a spinner is present, a spinnerState can be specified to indicate the
    // purpose of the spinner.
    spinner: _propTypes.default.bool,
    spinnerState: _propTypes.default.oneOf(["joining"]),
    loading: _propTypes.default.bool,
    joining: _propTypes.default.bool,
    rejecting: _propTypes.default.bool,
    // The alias that was used to access this room, if appropriate
    // If given, this will be how the room is referred to (eg.
    // in error messages).
    roomAlias: _propTypes.default.string
  },
  getDefaultProps: function () {
    return {
      onJoinClick: function () {}
    };
  },
  getInitialState: function () {
    return {
      busy: false
    };
  },
  componentDidMount: function () {
    this._checkInvitedEmail();
  },
  componentDidUpdate: function (prevProps, prevState) {
    if (this.props.invitedEmail !== prevProps.invitedEmail || this.props.inviterName !== prevProps.inviterName) {
      this._checkInvitedEmail();
    }
  },
  _checkInvitedEmail: async function () {
    // If this is an invite and we've been told what email address was
    // invited, fetch the user's account emails and discovery bindings so we
    // can check them against the email that was invited.
    if (this.props.inviterName && this.props.invitedEmail) {
      this.setState({
        busy: true
      });

      try {
        // Gather the account 3PIDs
        const account3pids = await _MatrixClientPeg.MatrixClientPeg.get().getThreePids();
        this.setState({
          accountEmails: account3pids.threepids.filter(b => b.medium === 'email').map(b => b.address)
        }); // If we have an IS connected, use that to lookup the email and
        // check the bound MXID.

        if (!_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()) {
          this.setState({
            busy: false
          });
          return;
        }

        const authClient = new _IdentityAuthClient.default();
        const identityAccessToken = await authClient.getAccessToken();
        const result = await _MatrixClientPeg.MatrixClientPeg.get().lookupThreePid('email', this.props.invitedEmail, undefined
        /* callback */
        , identityAccessToken);
        this.setState({
          invitedEmailMxid: result.mxid
        });
      } catch (err) {
        this.setState({
          threePidFetchError: err
        });
      }

      this.setState({
        busy: false
      });
    }
  },

  _getMessageCase() {
    const isGuest = _MatrixClientPeg.MatrixClientPeg.get().isGuest();

    if (isGuest) {
      return MessageCase.NotLoggedIn;
    }

    const myMember = this._getMyMember();

    if (myMember) {
      if (myMember.isKicked()) {
        return MessageCase.Kicked;
      } else if (myMember.membership === "ban") {
        return MessageCase.Banned;
      }
    }

    if (this.props.joining) {
      return MessageCase.Joining;
    } else if (this.props.rejecting) {
      return MessageCase.Rejecting;
    } else if (this.props.loading || this.state.busy) {
      return MessageCase.Loading;
    }

    if (this.props.inviterName) {
      if (this.props.invitedEmail) {
        if (this.state.threePidFetchError) {
          return MessageCase.OtherThreePIDError;
        } else if (this.state.accountEmails && !this.state.accountEmails.includes(this.props.invitedEmail)) {
          return MessageCase.InvitedEmailNotFoundInAccount;
        } else if (!_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()) {
          return MessageCase.InvitedEmailNoIdentityServer;
        } else if (this.state.invitedEmailMxid != _MatrixClientPeg.MatrixClientPeg.get().getUserId()) {
          return MessageCase.InvitedEmailMismatch;
        }
      }

      return MessageCase.Invite;
    } else if (this.props.error) {
      if (this.props.error.errcode == 'M_NOT_FOUND') {
        return MessageCase.RoomNotFound;
      } else {
        return MessageCase.OtherError;
      }
    } else {
      return MessageCase.ViewingRoom;
    }
  },

  _getKickOrBanInfo() {
    const myMember = this._getMyMember();

    if (!myMember) {
      return {};
    }

    const kickerMember = this.props.room.currentState.getMember(myMember.events.member.getSender());
    const memberName = kickerMember ? kickerMember.name : myMember.events.member.getSender();
    const reason = myMember.events.member.getContent().reason;
    return {
      memberName,
      reason
    };
  },

  _joinRule: function () {
    const room = this.props.room;

    if (room) {
      const joinRules = room.currentState.getStateEvents('m.room.join_rules', '');

      if (joinRules) {
        return joinRules.getContent().join_rule;
      }
    }
  },
  _roomName: function (atStart = false) {
    const name = this.props.room ? this.props.room.name : this.props.roomAlias;

    if (name) {
      return name;
    } else if (atStart) {
      return (0, _languageHandler._t)("This room");
    } else {
      return (0, _languageHandler._t)("this room");
    }
  },

  _getMyMember() {
    return this.props.room && this.props.room.getMember(_MatrixClientPeg.MatrixClientPeg.get().getUserId());
  },

  _getInviteMember: function () {
    const {
      room
    } = this.props;

    if (!room) {
      return;
    }

    const myUserId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

    const inviteEvent = room.currentState.getMember(myUserId);

    if (!inviteEvent) {
      return;
    }

    const inviterUserId = inviteEvent.events.member.getSender();
    return room.currentState.getMember(inviterUserId);
  },

  _isDMInvite() {
    const myMember = this._getMyMember();

    if (!myMember) {
      return false;
    }

    const memberEvent = myMember.events.member;
    const memberContent = memberEvent.getContent();
    return memberContent.membership === "invite" && memberContent.is_direct;
  },

  _makeScreenAfterLogin() {
    return {
      screen: 'room',
      params: {
        email: this.props.invitedEmail,
        signurl: this.props.signUrl,
        room_name: this.props.oobData.room_name,
        room_avatar_url: this.props.oobData.avatarUrl,
        inviter_name: this.props.oobData.inviterName
      }
    };
  },

  onLoginClick: function () {
    _dispatcher.default.dispatch({
      action: 'start_login',
      screenAfterLogin: this._makeScreenAfterLogin()
    });
  },
  onRegisterClick: function () {
    _dispatcher.default.dispatch({
      action: 'start_registration',
      screenAfterLogin: this._makeScreenAfterLogin()
    });
  },
  render: function () {
    const Spinner = sdk.getComponent('elements.Spinner');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let showSpinner = false;
    let darkStyle = false;
    let title;
    let subTitle;
    let primaryActionHandler;
    let primaryActionLabel;
    let secondaryActionHandler;
    let secondaryActionLabel;
    let footer;
    const extraComponents = [];

    const messageCase = this._getMessageCase();

    switch (messageCase) {
      case MessageCase.Joining:
        {
          title = (0, _languageHandler._t)("Joining room …");
          showSpinner = true;
          break;
        }

      case MessageCase.Loading:
        {
          title = (0, _languageHandler._t)("Loading …");
          showSpinner = true;
          break;
        }

      case MessageCase.Rejecting:
        {
          title = (0, _languageHandler._t)("Rejecting invite …");
          showSpinner = true;
          break;
        }

      case MessageCase.NotLoggedIn:
        {
          darkStyle = true;
          title = (0, _languageHandler._t)("Join the conversation with an account");
          primaryActionLabel = (0, _languageHandler._t)("Sign Up");
          primaryActionHandler = this.onRegisterClick;
          secondaryActionLabel = (0, _languageHandler._t)("Sign In");
          secondaryActionHandler = this.onLoginClick;

          if (this.props.previewLoading) {
            footer = _react.default.createElement("div", null, _react.default.createElement(Spinner, {
              w: 20,
              h: 20
            }), (0, _languageHandler._t)("Loading room preview"));
          }

          break;
        }

      case MessageCase.Kicked:
        {
          const {
            memberName,
            reason
          } = this._getKickOrBanInfo();

          title = (0, _languageHandler._t)("You were kicked from %(roomName)s by %(memberName)s", {
            memberName,
            roomName: this._roomName()
          });
          subTitle = reason ? (0, _languageHandler._t)("Reason: %(reason)s", {
            reason
          }) : null;

          if (this._joinRule() === "invite") {
            primaryActionLabel = (0, _languageHandler._t)("Forget this room");
            primaryActionHandler = this.props.onForgetClick;
          } else {
            primaryActionLabel = (0, _languageHandler._t)("Re-join");
            primaryActionHandler = this.props.onJoinClick;
            secondaryActionLabel = (0, _languageHandler._t)("Forget this room");
            secondaryActionHandler = this.props.onForgetClick;
          }

          break;
        }

      case MessageCase.Banned:
        {
          const {
            memberName,
            reason
          } = this._getKickOrBanInfo();

          title = (0, _languageHandler._t)("You were banned from %(roomName)s by %(memberName)s", {
            memberName,
            roomName: this._roomName()
          });
          subTitle = reason ? (0, _languageHandler._t)("Reason: %(reason)s", {
            reason
          }) : null;
          primaryActionLabel = (0, _languageHandler._t)("Forget this room");
          primaryActionHandler = this.props.onForgetClick;
          break;
        }

      case MessageCase.OtherThreePIDError:
        {
          title = (0, _languageHandler._t)("Something went wrong with your invite to %(roomName)s", {
            roomName: this._roomName()
          });

          const joinRule = this._joinRule();

          const errCodeMessage = (0, _languageHandler._t)("An error (%(errcode)s) was returned while trying to validate your " + "invite. You could try to pass this information on to a room admin.", {
            errcode: this.state.threePidFetchError.errcode || (0, _languageHandler._t)("unknown error code")
          });

          switch (joinRule) {
            case "invite":
              subTitle = [(0, _languageHandler._t)("You can only join it with a working invite."), errCodeMessage];
              primaryActionLabel = (0, _languageHandler._t)("Try to join anyway");
              primaryActionHandler = this.props.onJoinClick;
              break;

            case "public":
              subTitle = (0, _languageHandler._t)("You can still join it because this is a public room.");
              primaryActionLabel = (0, _languageHandler._t)("Join the discussion");
              primaryActionHandler = this.props.onJoinClick;
              break;

            default:
              subTitle = errCodeMessage;
              primaryActionLabel = (0, _languageHandler._t)("Try to join anyway");
              primaryActionHandler = this.props.onJoinClick;
              break;
          }

          break;
        }

      case MessageCase.InvitedEmailNotFoundInAccount:
        {
          title = (0, _languageHandler._t)("This invite to %(roomName)s was sent to %(email)s which is not " + "associated with your account", {
            roomName: this._roomName(),
            email: this.props.invitedEmail
          });
          subTitle = (0, _languageHandler._t)("Link this email with your account in Settings to receive invites " + "directly in Riot.");
          primaryActionLabel = (0, _languageHandler._t)("Join the discussion");
          primaryActionHandler = this.props.onJoinClick;
          break;
        }

      case MessageCase.InvitedEmailNoIdentityServer:
        {
          title = (0, _languageHandler._t)("This invite to %(roomName)s was sent to %(email)s", {
            roomName: this._roomName(),
            email: this.props.invitedEmail
          });
          subTitle = (0, _languageHandler._t)("Use an identity server in Settings to receive invites directly in Riot.");
          primaryActionLabel = (0, _languageHandler._t)("Join the discussion");
          primaryActionHandler = this.props.onJoinClick;
          break;
        }

      case MessageCase.InvitedEmailMismatch:
        {
          title = (0, _languageHandler._t)("This invite to %(roomName)s was sent to %(email)s", {
            roomName: this._roomName(),
            email: this.props.invitedEmail
          });
          subTitle = (0, _languageHandler._t)("Share this email in Settings to receive invites directly in Riot.");
          primaryActionLabel = (0, _languageHandler._t)("Join the discussion");
          primaryActionHandler = this.props.onJoinClick;
          break;
        }

      case MessageCase.Invite:
        {
          const RoomAvatar = sdk.getComponent("views.avatars.RoomAvatar");

          const avatar = _react.default.createElement(RoomAvatar, {
            room: this.props.room,
            oobData: this.props.oobData
          });

          const inviteMember = this._getInviteMember();

          let inviterElement;

          if (inviteMember) {
            inviterElement = _react.default.createElement("span", null, _react.default.createElement("span", {
              className: "mx_RoomPreviewBar_inviter"
            }, inviteMember.rawDisplayName), " (", inviteMember.userId, ")");
          } else {
            inviterElement = _react.default.createElement("span", {
              className: "mx_RoomPreviewBar_inviter"
            }, this.props.inviterName);
          }

          const isDM = this._isDMInvite();

          if (isDM) {
            title = (0, _languageHandler._t)("Do you want to chat with %(user)s?", {
              user: inviteMember.name
            });
            subTitle = [avatar, (0, _languageHandler._t)("<userName/> wants to chat", {}, {
              userName: () => inviterElement
            })];
            primaryActionLabel = (0, _languageHandler._t)("Start chatting");
          } else {
            title = (0, _languageHandler._t)("Do you want to join %(roomName)s?", {
              roomName: this._roomName()
            });
            subTitle = [avatar, (0, _languageHandler._t)("<userName/> invited you", {}, {
              userName: () => inviterElement
            })];
            primaryActionLabel = (0, _languageHandler._t)("Accept");
          }

          primaryActionHandler = this.props.onJoinClick;
          secondaryActionLabel = (0, _languageHandler._t)("Reject");
          secondaryActionHandler = this.props.onRejectClick;

          if (this.props.onRejectAndIgnoreClick) {
            extraComponents.push(_react.default.createElement(AccessibleButton, {
              kind: "secondary",
              onClick: this.props.onRejectAndIgnoreClick,
              key: "ignore"
            }, (0, _languageHandler._t)("Reject & Ignore user")));
          }

          break;
        }

      case MessageCase.ViewingRoom:
        {
          if (this.props.canPreview) {
            title = (0, _languageHandler._t)("You're previewing %(roomName)s. Want to join it?", {
              roomName: this._roomName()
            });
          } else {
            title = (0, _languageHandler._t)("%(roomName)s can't be previewed. Do you want to join it?", {
              roomName: this._roomName(true)
            });
          }

          primaryActionLabel = (0, _languageHandler._t)("Join the discussion");
          primaryActionHandler = this.props.onJoinClick;
          break;
        }

      case MessageCase.RoomNotFound:
        {
          title = (0, _languageHandler._t)("%(roomName)s does not exist.", {
            roomName: this._roomName(true)
          });
          subTitle = (0, _languageHandler._t)("This room doesn't exist. Are you sure you're at the right place?");
          break;
        }

      case MessageCase.OtherError:
        {
          title = (0, _languageHandler._t)("%(roomName)s is not accessible at this time.", {
            roomName: this._roomName(true)
          });
          subTitle = [(0, _languageHandler._t)("Try again later, or ask a room admin to check if you have access."), (0, _languageHandler._t)("%(errcode)s was returned while trying to access the room. " + "If you think you're seeing this message in error, please " + "<issueLink>submit a bug report</issueLink>.", {
            errcode: this.props.error.errcode
          }, {
            issueLink: label => _react.default.createElement("a", {
              href: "https://github.com/vector-im/riot-web/issues/new/choose",
              target: "_blank",
              rel: "noreferrer noopener"
            }, label)
          })];
          break;
        }
    }

    let subTitleElements;

    if (subTitle) {
      if (!Array.isArray(subTitle)) {
        subTitle = [subTitle];
      }

      subTitleElements = subTitle.map((t, i) => _react.default.createElement("p", {
        key: "subTitle".concat(i)
      }, t));
    }

    let titleElement;

    if (showSpinner) {
      titleElement = _react.default.createElement("h3", {
        className: "mx_RoomPreviewBar_spinnerTitle"
      }, _react.default.createElement(Spinner, null), title);
    } else {
      titleElement = _react.default.createElement("h3", null, title);
    }

    let primaryButton;

    if (primaryActionHandler) {
      primaryButton = _react.default.createElement(AccessibleButton, {
        kind: "primary",
        onClick: primaryActionHandler
      }, primaryActionLabel);
    }

    let secondaryButton;

    if (secondaryActionHandler) {
      secondaryButton = _react.default.createElement(AccessibleButton, {
        kind: "secondary",
        onClick: secondaryActionHandler
      }, secondaryActionLabel);
    }

    const classes = (0, _classnames.default)("mx_RoomPreviewBar", "dark-panel", "mx_RoomPreviewBar_".concat(messageCase), {
      "mx_RoomPreviewBar_panel": this.props.canPreview,
      "mx_RoomPreviewBar_dialog": !this.props.canPreview,
      "mx_RoomPreviewBar_dark": darkStyle
    });
    return _react.default.createElement("div", {
      className: classes
    }, _react.default.createElement("div", {
      className: "mx_RoomPreviewBar_message"
    }, titleElement, subTitleElements), _react.default.createElement("div", {
      className: "mx_RoomPreviewBar_actions"
    }, secondaryButton, extraComponents, primaryButton), _react.default.createElement("div", {
      className: "mx_RoomPreviewBar_footer"
    }, footer));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21QcmV2aWV3QmFyLmpzIl0sIm5hbWVzIjpbIk1lc3NhZ2VDYXNlIiwiT2JqZWN0IiwiZnJlZXplIiwiTm90TG9nZ2VkSW4iLCJKb2luaW5nIiwiTG9hZGluZyIsIlJlamVjdGluZyIsIktpY2tlZCIsIkJhbm5lZCIsIk90aGVyVGhyZWVQSURFcnJvciIsIkludml0ZWRFbWFpbE5vdEZvdW5kSW5BY2NvdW50IiwiSW52aXRlZEVtYWlsTm9JZGVudGl0eVNlcnZlciIsIkludml0ZWRFbWFpbE1pc21hdGNoIiwiSW52aXRlIiwiVmlld2luZ1Jvb20iLCJSb29tTm90Rm91bmQiLCJPdGhlckVycm9yIiwiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJvbkpvaW5DbGljayIsIlByb3BUeXBlcyIsImZ1bmMiLCJvblJlamVjdENsaWNrIiwib25SZWplY3RBbmRJZ25vcmVDbGljayIsIm9uRm9yZ2V0Q2xpY2siLCJpbnZpdGVyTmFtZSIsInN0cmluZyIsImludml0ZWRFbWFpbCIsIm9vYkRhdGEiLCJvYmplY3QiLCJzaWduVXJsIiwiZXJyb3IiLCJjYW5QcmV2aWV3IiwiYm9vbCIsInByZXZpZXdMb2FkaW5nIiwicm9vbSIsInNwaW5uZXIiLCJzcGlubmVyU3RhdGUiLCJvbmVPZiIsImxvYWRpbmciLCJqb2luaW5nIiwicmVqZWN0aW5nIiwicm9vbUFsaWFzIiwiZ2V0RGVmYXVsdFByb3BzIiwiZ2V0SW5pdGlhbFN0YXRlIiwiYnVzeSIsImNvbXBvbmVudERpZE1vdW50IiwiX2NoZWNrSW52aXRlZEVtYWlsIiwiY29tcG9uZW50RGlkVXBkYXRlIiwicHJldlByb3BzIiwicHJldlN0YXRlIiwicHJvcHMiLCJzZXRTdGF0ZSIsImFjY291bnQzcGlkcyIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFRocmVlUGlkcyIsImFjY291bnRFbWFpbHMiLCJ0aHJlZXBpZHMiLCJmaWx0ZXIiLCJiIiwibWVkaXVtIiwibWFwIiwiYWRkcmVzcyIsImdldElkZW50aXR5U2VydmVyVXJsIiwiYXV0aENsaWVudCIsIklkZW50aXR5QXV0aENsaWVudCIsImlkZW50aXR5QWNjZXNzVG9rZW4iLCJnZXRBY2Nlc3NUb2tlbiIsInJlc3VsdCIsImxvb2t1cFRocmVlUGlkIiwidW5kZWZpbmVkIiwiaW52aXRlZEVtYWlsTXhpZCIsIm14aWQiLCJlcnIiLCJ0aHJlZVBpZEZldGNoRXJyb3IiLCJfZ2V0TWVzc2FnZUNhc2UiLCJpc0d1ZXN0IiwibXlNZW1iZXIiLCJfZ2V0TXlNZW1iZXIiLCJpc0tpY2tlZCIsIm1lbWJlcnNoaXAiLCJzdGF0ZSIsImluY2x1ZGVzIiwiZ2V0VXNlcklkIiwiZXJyY29kZSIsIl9nZXRLaWNrT3JCYW5JbmZvIiwia2lja2VyTWVtYmVyIiwiY3VycmVudFN0YXRlIiwiZ2V0TWVtYmVyIiwiZXZlbnRzIiwibWVtYmVyIiwiZ2V0U2VuZGVyIiwibWVtYmVyTmFtZSIsIm5hbWUiLCJyZWFzb24iLCJnZXRDb250ZW50IiwiX2pvaW5SdWxlIiwiam9pblJ1bGVzIiwiZ2V0U3RhdGVFdmVudHMiLCJqb2luX3J1bGUiLCJfcm9vbU5hbWUiLCJhdFN0YXJ0IiwiX2dldEludml0ZU1lbWJlciIsIm15VXNlcklkIiwiaW52aXRlRXZlbnQiLCJpbnZpdGVyVXNlcklkIiwiX2lzRE1JbnZpdGUiLCJtZW1iZXJFdmVudCIsIm1lbWJlckNvbnRlbnQiLCJpc19kaXJlY3QiLCJfbWFrZVNjcmVlbkFmdGVyTG9naW4iLCJzY3JlZW4iLCJwYXJhbXMiLCJlbWFpbCIsInNpZ251cmwiLCJyb29tX25hbWUiLCJyb29tX2F2YXRhcl91cmwiLCJhdmF0YXJVcmwiLCJpbnZpdGVyX25hbWUiLCJvbkxvZ2luQ2xpY2siLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInNjcmVlbkFmdGVyTG9naW4iLCJvblJlZ2lzdGVyQ2xpY2siLCJyZW5kZXIiLCJTcGlubmVyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiQWNjZXNzaWJsZUJ1dHRvbiIsInNob3dTcGlubmVyIiwiZGFya1N0eWxlIiwidGl0bGUiLCJzdWJUaXRsZSIsInByaW1hcnlBY3Rpb25IYW5kbGVyIiwicHJpbWFyeUFjdGlvbkxhYmVsIiwic2Vjb25kYXJ5QWN0aW9uSGFuZGxlciIsInNlY29uZGFyeUFjdGlvbkxhYmVsIiwiZm9vdGVyIiwiZXh0cmFDb21wb25lbnRzIiwibWVzc2FnZUNhc2UiLCJyb29tTmFtZSIsImpvaW5SdWxlIiwiZXJyQ29kZU1lc3NhZ2UiLCJSb29tQXZhdGFyIiwiYXZhdGFyIiwiaW52aXRlTWVtYmVyIiwiaW52aXRlckVsZW1lbnQiLCJyYXdEaXNwbGF5TmFtZSIsInVzZXJJZCIsImlzRE0iLCJ1c2VyIiwidXNlck5hbWUiLCJwdXNoIiwiaXNzdWVMaW5rIiwibGFiZWwiLCJzdWJUaXRsZUVsZW1lbnRzIiwiQXJyYXkiLCJpc0FycmF5IiwidCIsImkiLCJ0aXRsZUVsZW1lbnQiLCJwcmltYXJ5QnV0dG9uIiwic2Vjb25kYXJ5QnV0dG9uIiwiY2xhc3NlcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFrQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQSxNQUFNQSxXQUFXLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjO0FBQzlCQyxFQUFBQSxXQUFXLEVBQUUsYUFEaUI7QUFFOUJDLEVBQUFBLE9BQU8sRUFBRSxTQUZxQjtBQUc5QkMsRUFBQUEsT0FBTyxFQUFFLFNBSHFCO0FBSTlCQyxFQUFBQSxTQUFTLEVBQUUsV0FKbUI7QUFLOUJDLEVBQUFBLE1BQU0sRUFBRSxRQUxzQjtBQU05QkMsRUFBQUEsTUFBTSxFQUFFLFFBTnNCO0FBTzlCQyxFQUFBQSxrQkFBa0IsRUFBRSxvQkFQVTtBQVE5QkMsRUFBQUEsNkJBQTZCLEVBQUUsK0JBUkQ7QUFTOUJDLEVBQUFBLDRCQUE0QixFQUFFLDhCQVRBO0FBVTlCQyxFQUFBQSxvQkFBb0IsRUFBRSxzQkFWUTtBQVc5QkMsRUFBQUEsTUFBTSxFQUFFLFFBWHNCO0FBWTlCQyxFQUFBQSxXQUFXLEVBQUUsYUFaaUI7QUFhOUJDLEVBQUFBLFlBQVksRUFBRSxjQWJnQjtBQWM5QkMsRUFBQUEsVUFBVSxFQUFFO0FBZGtCLENBQWQsQ0FBcEI7O2VBaUJlLCtCQUFpQjtBQUM1QkMsRUFBQUEsV0FBVyxFQUFFLGdCQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsV0FBVyxFQUFFQyxtQkFBVUMsSUFEaEI7QUFFUEMsSUFBQUEsYUFBYSxFQUFFRixtQkFBVUMsSUFGbEI7QUFHUEUsSUFBQUEsc0JBQXNCLEVBQUVILG1CQUFVQyxJQUgzQjtBQUlQRyxJQUFBQSxhQUFhLEVBQUVKLG1CQUFVQyxJQUpsQjtBQUtQO0FBQ0E7QUFDQUksSUFBQUEsV0FBVyxFQUFFTCxtQkFBVU0sTUFQaEI7QUFTUDtBQUNBQyxJQUFBQSxZQUFZLEVBQUVQLG1CQUFVTSxNQVZqQjtBQVlQO0FBQ0FFLElBQUFBLE9BQU8sRUFBRVIsbUJBQVVTLE1BYlo7QUFlUDtBQUNBQyxJQUFBQSxPQUFPLEVBQUVWLG1CQUFVTSxNQWhCWjtBQWtCUDtBQUNBO0FBQ0FLLElBQUFBLEtBQUssRUFBRVgsbUJBQVVTLE1BcEJWO0FBc0JQRyxJQUFBQSxVQUFVLEVBQUVaLG1CQUFVYSxJQXRCZjtBQXVCUEMsSUFBQUEsY0FBYyxFQUFFZCxtQkFBVWEsSUF2Qm5CO0FBd0JQRSxJQUFBQSxJQUFJLEVBQUVmLG1CQUFVUyxNQXhCVDtBQTBCUDtBQUNBO0FBQ0FPLElBQUFBLE9BQU8sRUFBRWhCLG1CQUFVYSxJQTVCWjtBQTZCUEksSUFBQUEsWUFBWSxFQUFFakIsbUJBQVVrQixLQUFWLENBQWdCLENBQUMsU0FBRCxDQUFoQixDQTdCUDtBQThCUEMsSUFBQUEsT0FBTyxFQUFFbkIsbUJBQVVhLElBOUJaO0FBK0JQTyxJQUFBQSxPQUFPLEVBQUVwQixtQkFBVWEsSUEvQlo7QUFnQ1BRLElBQUFBLFNBQVMsRUFBRXJCLG1CQUFVYSxJQWhDZDtBQWlDUDtBQUNBO0FBQ0E7QUFDQVMsSUFBQUEsU0FBUyxFQUFFdEIsbUJBQVVNO0FBcENkLEdBSGlCO0FBMEM1QmlCLEVBQUFBLGVBQWUsRUFBRSxZQUFXO0FBQ3hCLFdBQU87QUFDSHhCLE1BQUFBLFdBQVcsRUFBRSxZQUFXLENBQUU7QUFEdkIsS0FBUDtBQUdILEdBOUMyQjtBQWdENUJ5QixFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hDLE1BQUFBLElBQUksRUFBRTtBQURILEtBQVA7QUFHSCxHQXBEMkI7QUFzRDVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLGtCQUFMO0FBQ0gsR0F4RDJCO0FBMEQ1QkMsRUFBQUEsa0JBQWtCLEVBQUUsVUFBU0MsU0FBVCxFQUFvQkMsU0FBcEIsRUFBK0I7QUFDL0MsUUFBSSxLQUFLQyxLQUFMLENBQVd4QixZQUFYLEtBQTRCc0IsU0FBUyxDQUFDdEIsWUFBdEMsSUFBc0QsS0FBS3dCLEtBQUwsQ0FBVzFCLFdBQVgsS0FBMkJ3QixTQUFTLENBQUN4QixXQUEvRixFQUE0RztBQUN4RyxXQUFLc0Isa0JBQUw7QUFDSDtBQUNKLEdBOUQyQjtBQWdFNUJBLEVBQUFBLGtCQUFrQixFQUFFLGtCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQSxRQUFJLEtBQUtJLEtBQUwsQ0FBVzFCLFdBQVgsSUFBMEIsS0FBSzBCLEtBQUwsQ0FBV3hCLFlBQXpDLEVBQXVEO0FBQ25ELFdBQUt5QixRQUFMLENBQWM7QUFBQ1AsUUFBQUEsSUFBSSxFQUFFO0FBQVAsT0FBZDs7QUFDQSxVQUFJO0FBQ0E7QUFDQSxjQUFNUSxZQUFZLEdBQUcsTUFBTUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsWUFBdEIsRUFBM0I7QUFDQSxhQUFLSixRQUFMLENBQWM7QUFDVkssVUFBQUEsYUFBYSxFQUFFSixZQUFZLENBQUNLLFNBQWIsQ0FDVkMsTUFEVSxDQUNIQyxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsTUFBRixLQUFhLE9BRGYsRUFDd0JDLEdBRHhCLENBQzRCRixDQUFDLElBQUlBLENBQUMsQ0FBQ0csT0FEbkM7QUFETCxTQUFkLEVBSEEsQ0FPQTtBQUNBOztBQUNBLFlBQUksQ0FBQ1QsaUNBQWdCQyxHQUFoQixHQUFzQlMsb0JBQXRCLEVBQUwsRUFBbUQ7QUFDL0MsZUFBS1osUUFBTCxDQUFjO0FBQUNQLFlBQUFBLElBQUksRUFBRTtBQUFQLFdBQWQ7QUFDQTtBQUNIOztBQUNELGNBQU1vQixVQUFVLEdBQUcsSUFBSUMsMkJBQUosRUFBbkI7QUFDQSxjQUFNQyxtQkFBbUIsR0FBRyxNQUFNRixVQUFVLENBQUNHLGNBQVgsRUFBbEM7QUFDQSxjQUFNQyxNQUFNLEdBQUcsTUFBTWYsaUNBQWdCQyxHQUFoQixHQUFzQmUsY0FBdEIsQ0FDakIsT0FEaUIsRUFFakIsS0FBS25CLEtBQUwsQ0FBV3hCLFlBRk0sRUFHakI0QztBQUFVO0FBSE8sVUFJakJKLG1CQUppQixDQUFyQjtBQU1BLGFBQUtmLFFBQUwsQ0FBYztBQUFDb0IsVUFBQUEsZ0JBQWdCLEVBQUVILE1BQU0sQ0FBQ0k7QUFBMUIsU0FBZDtBQUNILE9BdEJELENBc0JFLE9BQU9DLEdBQVAsRUFBWTtBQUNWLGFBQUt0QixRQUFMLENBQWM7QUFBQ3VCLFVBQUFBLGtCQUFrQixFQUFFRDtBQUFyQixTQUFkO0FBQ0g7O0FBQ0QsV0FBS3RCLFFBQUwsQ0FBYztBQUFDUCxRQUFBQSxJQUFJLEVBQUU7QUFBUCxPQUFkO0FBQ0g7QUFDSixHQWpHMkI7O0FBbUc1QitCLEVBQUFBLGVBQWUsR0FBRztBQUNkLFVBQU1DLE9BQU8sR0FBR3ZCLGlDQUFnQkMsR0FBaEIsR0FBc0JzQixPQUF0QixFQUFoQjs7QUFFQSxRQUFJQSxPQUFKLEVBQWE7QUFDVCxhQUFPN0UsV0FBVyxDQUFDRyxXQUFuQjtBQUNIOztBQUVELFVBQU0yRSxRQUFRLEdBQUcsS0FBS0MsWUFBTCxFQUFqQjs7QUFFQSxRQUFJRCxRQUFKLEVBQWM7QUFDVixVQUFJQSxRQUFRLENBQUNFLFFBQVQsRUFBSixFQUF5QjtBQUNyQixlQUFPaEYsV0FBVyxDQUFDTyxNQUFuQjtBQUNILE9BRkQsTUFFTyxJQUFJdUUsUUFBUSxDQUFDRyxVQUFULEtBQXdCLEtBQTVCLEVBQW1DO0FBQ3RDLGVBQU9qRixXQUFXLENBQUNRLE1BQW5CO0FBQ0g7QUFDSjs7QUFFRCxRQUFJLEtBQUsyQyxLQUFMLENBQVdYLE9BQWYsRUFBd0I7QUFDcEIsYUFBT3hDLFdBQVcsQ0FBQ0ksT0FBbkI7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLK0MsS0FBTCxDQUFXVixTQUFmLEVBQTBCO0FBQzdCLGFBQU96QyxXQUFXLENBQUNNLFNBQW5CO0FBQ0gsS0FGTSxNQUVBLElBQUksS0FBSzZDLEtBQUwsQ0FBV1osT0FBWCxJQUFzQixLQUFLMkMsS0FBTCxDQUFXckMsSUFBckMsRUFBMkM7QUFDOUMsYUFBTzdDLFdBQVcsQ0FBQ0ssT0FBbkI7QUFDSDs7QUFFRCxRQUFJLEtBQUs4QyxLQUFMLENBQVcxQixXQUFmLEVBQTRCO0FBQ3hCLFVBQUksS0FBSzBCLEtBQUwsQ0FBV3hCLFlBQWYsRUFBNkI7QUFDekIsWUFBSSxLQUFLdUQsS0FBTCxDQUFXUCxrQkFBZixFQUFtQztBQUMvQixpQkFBTzNFLFdBQVcsQ0FBQ1Msa0JBQW5CO0FBQ0gsU0FGRCxNQUVPLElBQ0gsS0FBS3lFLEtBQUwsQ0FBV3pCLGFBQVgsSUFDQSxDQUFDLEtBQUt5QixLQUFMLENBQVd6QixhQUFYLENBQXlCMEIsUUFBekIsQ0FBa0MsS0FBS2hDLEtBQUwsQ0FBV3hCLFlBQTdDLENBRkUsRUFHTDtBQUNFLGlCQUFPM0IsV0FBVyxDQUFDVSw2QkFBbkI7QUFDSCxTQUxNLE1BS0EsSUFBSSxDQUFDNEMsaUNBQWdCQyxHQUFoQixHQUFzQlMsb0JBQXRCLEVBQUwsRUFBbUQ7QUFDdEQsaUJBQU9oRSxXQUFXLENBQUNXLDRCQUFuQjtBQUNILFNBRk0sTUFFQSxJQUFJLEtBQUt1RSxLQUFMLENBQVdWLGdCQUFYLElBQStCbEIsaUNBQWdCQyxHQUFoQixHQUFzQjZCLFNBQXRCLEVBQW5DLEVBQXNFO0FBQ3pFLGlCQUFPcEYsV0FBVyxDQUFDWSxvQkFBbkI7QUFDSDtBQUNKOztBQUNELGFBQU9aLFdBQVcsQ0FBQ2EsTUFBbkI7QUFDSCxLQWhCRCxNQWdCTyxJQUFJLEtBQUtzQyxLQUFMLENBQVdwQixLQUFmLEVBQXNCO0FBQ3pCLFVBQUksS0FBS29CLEtBQUwsQ0FBV3BCLEtBQVgsQ0FBaUJzRCxPQUFqQixJQUE0QixhQUFoQyxFQUErQztBQUMzQyxlQUFPckYsV0FBVyxDQUFDZSxZQUFuQjtBQUNILE9BRkQsTUFFTztBQUNILGVBQU9mLFdBQVcsQ0FBQ2dCLFVBQW5CO0FBQ0g7QUFDSixLQU5NLE1BTUE7QUFDSCxhQUFPaEIsV0FBVyxDQUFDYyxXQUFuQjtBQUNIO0FBQ0osR0FySjJCOztBQXVKNUJ3RSxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixVQUFNUixRQUFRLEdBQUcsS0FBS0MsWUFBTCxFQUFqQjs7QUFDQSxRQUFJLENBQUNELFFBQUwsRUFBZTtBQUNYLGFBQU8sRUFBUDtBQUNIOztBQUNELFVBQU1TLFlBQVksR0FBRyxLQUFLcEMsS0FBTCxDQUFXaEIsSUFBWCxDQUFnQnFELFlBQWhCLENBQTZCQyxTQUE3QixDQUNqQlgsUUFBUSxDQUFDWSxNQUFULENBQWdCQyxNQUFoQixDQUF1QkMsU0FBdkIsRUFEaUIsQ0FBckI7QUFHQSxVQUFNQyxVQUFVLEdBQUdOLFlBQVksR0FDM0JBLFlBQVksQ0FBQ08sSUFEYyxHQUNQaEIsUUFBUSxDQUFDWSxNQUFULENBQWdCQyxNQUFoQixDQUF1QkMsU0FBdkIsRUFEeEI7QUFFQSxVQUFNRyxNQUFNLEdBQUdqQixRQUFRLENBQUNZLE1BQVQsQ0FBZ0JDLE1BQWhCLENBQXVCSyxVQUF2QixHQUFvQ0QsTUFBbkQ7QUFDQSxXQUFPO0FBQUNGLE1BQUFBLFVBQUQ7QUFBYUUsTUFBQUE7QUFBYixLQUFQO0FBQ0gsR0FuSzJCOztBQXFLNUJFLEVBQUFBLFNBQVMsRUFBRSxZQUFXO0FBQ2xCLFVBQU05RCxJQUFJLEdBQUcsS0FBS2dCLEtBQUwsQ0FBV2hCLElBQXhCOztBQUNBLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU0rRCxTQUFTLEdBQUcvRCxJQUFJLENBQUNxRCxZQUFMLENBQWtCVyxjQUFsQixDQUFpQyxtQkFBakMsRUFBc0QsRUFBdEQsQ0FBbEI7O0FBQ0EsVUFBSUQsU0FBSixFQUFlO0FBQ1gsZUFBT0EsU0FBUyxDQUFDRixVQUFWLEdBQXVCSSxTQUE5QjtBQUNIO0FBQ0o7QUFDSixHQTdLMkI7QUErSzVCQyxFQUFBQSxTQUFTLEVBQUUsVUFBU0MsT0FBTyxHQUFHLEtBQW5CLEVBQTBCO0FBQ2pDLFVBQU1SLElBQUksR0FBRyxLQUFLM0MsS0FBTCxDQUFXaEIsSUFBWCxHQUFrQixLQUFLZ0IsS0FBTCxDQUFXaEIsSUFBWCxDQUFnQjJELElBQWxDLEdBQXlDLEtBQUszQyxLQUFMLENBQVdULFNBQWpFOztBQUNBLFFBQUlvRCxJQUFKLEVBQVU7QUFDTixhQUFPQSxJQUFQO0FBQ0gsS0FGRCxNQUVPLElBQUlRLE9BQUosRUFBYTtBQUNoQixhQUFPLHlCQUFHLFdBQUgsQ0FBUDtBQUNILEtBRk0sTUFFQTtBQUNILGFBQU8seUJBQUcsV0FBSCxDQUFQO0FBQ0g7QUFDSixHQXhMMkI7O0FBMEw1QnZCLEVBQUFBLFlBQVksR0FBRztBQUNYLFdBQ0ksS0FBSzVCLEtBQUwsQ0FBV2hCLElBQVgsSUFDQSxLQUFLZ0IsS0FBTCxDQUFXaEIsSUFBWCxDQUFnQnNELFNBQWhCLENBQTBCbkMsaUNBQWdCQyxHQUFoQixHQUFzQjZCLFNBQXRCLEVBQTFCLENBRko7QUFJSCxHQS9MMkI7O0FBaU01Qm1CLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsVUFBTTtBQUFDcEUsTUFBQUE7QUFBRCxRQUFTLEtBQUtnQixLQUFwQjs7QUFDQSxRQUFJLENBQUNoQixJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUNELFVBQU1xRSxRQUFRLEdBQUdsRCxpQ0FBZ0JDLEdBQWhCLEdBQXNCNkIsU0FBdEIsRUFBakI7O0FBQ0EsVUFBTXFCLFdBQVcsR0FBR3RFLElBQUksQ0FBQ3FELFlBQUwsQ0FBa0JDLFNBQWxCLENBQTRCZSxRQUE1QixDQUFwQjs7QUFDQSxRQUFJLENBQUNDLFdBQUwsRUFBa0I7QUFDZDtBQUNIOztBQUNELFVBQU1DLGFBQWEsR0FBR0QsV0FBVyxDQUFDZixNQUFaLENBQW1CQyxNQUFuQixDQUEwQkMsU0FBMUIsRUFBdEI7QUFDQSxXQUFPekQsSUFBSSxDQUFDcUQsWUFBTCxDQUFrQkMsU0FBbEIsQ0FBNEJpQixhQUE1QixDQUFQO0FBQ0gsR0E3TTJCOztBQStNNUJDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFVBQU03QixRQUFRLEdBQUcsS0FBS0MsWUFBTCxFQUFqQjs7QUFDQSxRQUFJLENBQUNELFFBQUwsRUFBZTtBQUNYLGFBQU8sS0FBUDtBQUNIOztBQUNELFVBQU04QixXQUFXLEdBQUc5QixRQUFRLENBQUNZLE1BQVQsQ0FBZ0JDLE1BQXBDO0FBQ0EsVUFBTWtCLGFBQWEsR0FBR0QsV0FBVyxDQUFDWixVQUFaLEVBQXRCO0FBQ0EsV0FBT2EsYUFBYSxDQUFDNUIsVUFBZCxLQUE2QixRQUE3QixJQUF5QzRCLGFBQWEsQ0FBQ0MsU0FBOUQ7QUFDSCxHQXZOMkI7O0FBeU41QkMsRUFBQUEscUJBQXFCLEdBQUc7QUFDcEIsV0FBTztBQUNIQyxNQUFBQSxNQUFNLEVBQUUsTUFETDtBQUVIQyxNQUFBQSxNQUFNLEVBQUU7QUFDSkMsUUFBQUEsS0FBSyxFQUFFLEtBQUsvRCxLQUFMLENBQVd4QixZQURkO0FBRUp3RixRQUFBQSxPQUFPLEVBQUUsS0FBS2hFLEtBQUwsQ0FBV3JCLE9BRmhCO0FBR0pzRixRQUFBQSxTQUFTLEVBQUUsS0FBS2pFLEtBQUwsQ0FBV3ZCLE9BQVgsQ0FBbUJ3RixTQUgxQjtBQUlKQyxRQUFBQSxlQUFlLEVBQUUsS0FBS2xFLEtBQUwsQ0FBV3ZCLE9BQVgsQ0FBbUIwRixTQUpoQztBQUtKQyxRQUFBQSxZQUFZLEVBQUUsS0FBS3BFLEtBQUwsQ0FBV3ZCLE9BQVgsQ0FBbUJIO0FBTDdCO0FBRkwsS0FBUDtBQVVILEdBcE8yQjs7QUFzTzVCK0YsRUFBQUEsWUFBWSxFQUFFLFlBQVc7QUFDckJDLHdCQUFJQyxRQUFKLENBQWE7QUFBRUMsTUFBQUEsTUFBTSxFQUFFLGFBQVY7QUFBeUJDLE1BQUFBLGdCQUFnQixFQUFFLEtBQUtiLHFCQUFMO0FBQTNDLEtBQWI7QUFDSCxHQXhPMkI7QUEwTzVCYyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4Qkosd0JBQUlDLFFBQUosQ0FBYTtBQUFFQyxNQUFBQSxNQUFNLEVBQUUsb0JBQVY7QUFBZ0NDLE1BQUFBLGdCQUFnQixFQUFFLEtBQUtiLHFCQUFMO0FBQWxELEtBQWI7QUFDSCxHQTVPMkI7QUE4TzVCZSxFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtCQUFqQixDQUFoQjtBQUNBLFVBQU1DLGdCQUFnQixHQUFHRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBRUEsUUFBSUUsV0FBVyxHQUFHLEtBQWxCO0FBQ0EsUUFBSUMsU0FBUyxHQUFHLEtBQWhCO0FBQ0EsUUFBSUMsS0FBSjtBQUNBLFFBQUlDLFFBQUo7QUFDQSxRQUFJQyxvQkFBSjtBQUNBLFFBQUlDLGtCQUFKO0FBQ0EsUUFBSUMsc0JBQUo7QUFDQSxRQUFJQyxvQkFBSjtBQUNBLFFBQUlDLE1BQUo7QUFDQSxVQUFNQyxlQUFlLEdBQUcsRUFBeEI7O0FBRUEsVUFBTUMsV0FBVyxHQUFHLEtBQUtqRSxlQUFMLEVBQXBCOztBQUNBLFlBQVFpRSxXQUFSO0FBQ0ksV0FBSzdJLFdBQVcsQ0FBQ0ksT0FBakI7QUFBMEI7QUFDdEJpSSxVQUFBQSxLQUFLLEdBQUcseUJBQUcsZ0JBQUgsQ0FBUjtBQUNBRixVQUFBQSxXQUFXLEdBQUcsSUFBZDtBQUNBO0FBQ0g7O0FBQ0QsV0FBS25JLFdBQVcsQ0FBQ0ssT0FBakI7QUFBMEI7QUFDdEJnSSxVQUFBQSxLQUFLLEdBQUcseUJBQUcsV0FBSCxDQUFSO0FBQ0FGLFVBQUFBLFdBQVcsR0FBRyxJQUFkO0FBQ0E7QUFDSDs7QUFDRCxXQUFLbkksV0FBVyxDQUFDTSxTQUFqQjtBQUE0QjtBQUN4QitILFVBQUFBLEtBQUssR0FBRyx5QkFBRyxvQkFBSCxDQUFSO0FBQ0FGLFVBQUFBLFdBQVcsR0FBRyxJQUFkO0FBQ0E7QUFDSDs7QUFDRCxXQUFLbkksV0FBVyxDQUFDRyxXQUFqQjtBQUE4QjtBQUMxQmlJLFVBQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0FDLFVBQUFBLEtBQUssR0FBRyx5QkFBRyx1Q0FBSCxDQUFSO0FBQ0FHLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLFNBQUgsQ0FBckI7QUFDQUQsVUFBQUEsb0JBQW9CLEdBQUcsS0FBS1YsZUFBNUI7QUFDQWEsVUFBQUEsb0JBQW9CLEdBQUcseUJBQUcsU0FBSCxDQUF2QjtBQUNBRCxVQUFBQSxzQkFBc0IsR0FBRyxLQUFLakIsWUFBOUI7O0FBQ0EsY0FBSSxLQUFLckUsS0FBTCxDQUFXakIsY0FBZixFQUErQjtBQUMzQnlHLFlBQUFBLE1BQU0sR0FDRiwwQ0FDSSw2QkFBQyxPQUFEO0FBQVMsY0FBQSxDQUFDLEVBQUUsRUFBWjtBQUFnQixjQUFBLENBQUMsRUFBRTtBQUFuQixjQURKLEVBRUsseUJBQUcsc0JBQUgsQ0FGTCxDQURKO0FBTUg7O0FBQ0Q7QUFDSDs7QUFDRCxXQUFLM0ksV0FBVyxDQUFDTyxNQUFqQjtBQUF5QjtBQUNyQixnQkFBTTtBQUFDc0YsWUFBQUEsVUFBRDtBQUFhRSxZQUFBQTtBQUFiLGNBQXVCLEtBQUtULGlCQUFMLEVBQTdCOztBQUNBK0MsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLHFEQUFILEVBQ0o7QUFBQ3hDLFlBQUFBLFVBQUQ7QUFBYWlELFlBQUFBLFFBQVEsRUFBRSxLQUFLekMsU0FBTDtBQUF2QixXQURJLENBQVI7QUFFQWlDLFVBQUFBLFFBQVEsR0FBR3ZDLE1BQU0sR0FBRyx5QkFBRyxvQkFBSCxFQUF5QjtBQUFDQSxZQUFBQTtBQUFELFdBQXpCLENBQUgsR0FBd0MsSUFBekQ7O0FBRUEsY0FBSSxLQUFLRSxTQUFMLE9BQXFCLFFBQXpCLEVBQW1DO0FBQy9CdUMsWUFBQUEsa0JBQWtCLEdBQUcseUJBQUcsa0JBQUgsQ0FBckI7QUFDQUQsWUFBQUEsb0JBQW9CLEdBQUcsS0FBS3BGLEtBQUwsQ0FBVzNCLGFBQWxDO0FBQ0gsV0FIRCxNQUdPO0FBQ0hnSCxZQUFBQSxrQkFBa0IsR0FBRyx5QkFBRyxTQUFILENBQXJCO0FBQ0FELFlBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBdUgsWUFBQUEsb0JBQW9CLEdBQUcseUJBQUcsa0JBQUgsQ0FBdkI7QUFDQUQsWUFBQUEsc0JBQXNCLEdBQUcsS0FBS3RGLEtBQUwsQ0FBVzNCLGFBQXBDO0FBQ0g7O0FBQ0Q7QUFDSDs7QUFDRCxXQUFLeEIsV0FBVyxDQUFDUSxNQUFqQjtBQUF5QjtBQUNyQixnQkFBTTtBQUFDcUYsWUFBQUEsVUFBRDtBQUFhRSxZQUFBQTtBQUFiLGNBQXVCLEtBQUtULGlCQUFMLEVBQTdCOztBQUNBK0MsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLHFEQUFILEVBQ0o7QUFBQ3hDLFlBQUFBLFVBQUQ7QUFBYWlELFlBQUFBLFFBQVEsRUFBRSxLQUFLekMsU0FBTDtBQUF2QixXQURJLENBQVI7QUFFQWlDLFVBQUFBLFFBQVEsR0FBR3ZDLE1BQU0sR0FBRyx5QkFBRyxvQkFBSCxFQUF5QjtBQUFDQSxZQUFBQTtBQUFELFdBQXpCLENBQUgsR0FBd0MsSUFBekQ7QUFDQXlDLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLGtCQUFILENBQXJCO0FBQ0FELFVBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVczQixhQUFsQztBQUNBO0FBQ0g7O0FBQ0QsV0FBS3hCLFdBQVcsQ0FBQ1Msa0JBQWpCO0FBQXFDO0FBQ2pDNEgsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLHVEQUFILEVBQ0o7QUFBQ1MsWUFBQUEsUUFBUSxFQUFFLEtBQUt6QyxTQUFMO0FBQVgsV0FESSxDQUFSOztBQUVBLGdCQUFNMEMsUUFBUSxHQUFHLEtBQUs5QyxTQUFMLEVBQWpCOztBQUNBLGdCQUFNK0MsY0FBYyxHQUFHLHlCQUNuQix1RUFDQSxvRUFGbUIsRUFHbkI7QUFBQzNELFlBQUFBLE9BQU8sRUFBRSxLQUFLSCxLQUFMLENBQVdQLGtCQUFYLENBQThCVSxPQUE5QixJQUF5Qyx5QkFBRyxvQkFBSDtBQUFuRCxXQUhtQixDQUF2Qjs7QUFLQSxrQkFBUTBELFFBQVI7QUFDSSxpQkFBSyxRQUFMO0FBQ0lULGNBQUFBLFFBQVEsR0FBRyxDQUNQLHlCQUFHLDZDQUFILENBRE8sRUFFUFUsY0FGTyxDQUFYO0FBSUFSLGNBQUFBLGtCQUFrQixHQUFHLHlCQUFHLG9CQUFILENBQXJCO0FBQ0FELGNBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBOztBQUNKLGlCQUFLLFFBQUw7QUFDSW1ILGNBQUFBLFFBQVEsR0FBRyx5QkFBRyxzREFBSCxDQUFYO0FBQ0FFLGNBQUFBLGtCQUFrQixHQUFHLHlCQUFHLHFCQUFILENBQXJCO0FBQ0FELGNBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBOztBQUNKO0FBQ0ltSCxjQUFBQSxRQUFRLEdBQUdVLGNBQVg7QUFDQVIsY0FBQUEsa0JBQWtCLEdBQUcseUJBQUcsb0JBQUgsQ0FBckI7QUFDQUQsY0FBQUEsb0JBQW9CLEdBQUcsS0FBS3BGLEtBQUwsQ0FBV2hDLFdBQWxDO0FBQ0E7QUFsQlI7O0FBb0JBO0FBQ0g7O0FBQ0QsV0FBS25CLFdBQVcsQ0FBQ1UsNkJBQWpCO0FBQWdEO0FBQzVDMkgsVUFBQUEsS0FBSyxHQUFHLHlCQUNKLG9FQUNBLDhCQUZJLEVBR0o7QUFDSVMsWUFBQUEsUUFBUSxFQUFFLEtBQUt6QyxTQUFMLEVBRGQ7QUFFSWEsWUFBQUEsS0FBSyxFQUFFLEtBQUsvRCxLQUFMLENBQVd4QjtBQUZ0QixXQUhJLENBQVI7QUFRQTJHLFVBQUFBLFFBQVEsR0FBRyx5QkFDUCxzRUFDQSxtQkFGTyxDQUFYO0FBSUFFLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLHFCQUFILENBQXJCO0FBQ0FELFVBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBO0FBQ0g7O0FBQ0QsV0FBS25CLFdBQVcsQ0FBQ1csNEJBQWpCO0FBQStDO0FBQzNDMEgsVUFBQUEsS0FBSyxHQUFHLHlCQUNKLG1EQURJLEVBRUo7QUFDSVMsWUFBQUEsUUFBUSxFQUFFLEtBQUt6QyxTQUFMLEVBRGQ7QUFFSWEsWUFBQUEsS0FBSyxFQUFFLEtBQUsvRCxLQUFMLENBQVd4QjtBQUZ0QixXQUZJLENBQVI7QUFPQTJHLFVBQUFBLFFBQVEsR0FBRyx5QkFDUCx5RUFETyxDQUFYO0FBR0FFLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLHFCQUFILENBQXJCO0FBQ0FELFVBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBO0FBQ0g7O0FBQ0QsV0FBS25CLFdBQVcsQ0FBQ1ksb0JBQWpCO0FBQXVDO0FBQ25DeUgsVUFBQUEsS0FBSyxHQUFHLHlCQUNKLG1EQURJLEVBRUo7QUFDSVMsWUFBQUEsUUFBUSxFQUFFLEtBQUt6QyxTQUFMLEVBRGQ7QUFFSWEsWUFBQUEsS0FBSyxFQUFFLEtBQUsvRCxLQUFMLENBQVd4QjtBQUZ0QixXQUZJLENBQVI7QUFPQTJHLFVBQUFBLFFBQVEsR0FBRyx5QkFDUCxtRUFETyxDQUFYO0FBR0FFLFVBQUFBLGtCQUFrQixHQUFHLHlCQUFHLHFCQUFILENBQXJCO0FBQ0FELFVBQUFBLG9CQUFvQixHQUFHLEtBQUtwRixLQUFMLENBQVdoQyxXQUFsQztBQUNBO0FBQ0g7O0FBQ0QsV0FBS25CLFdBQVcsQ0FBQ2EsTUFBakI7QUFBeUI7QUFDckIsZ0JBQU1vSSxVQUFVLEdBQUdqQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5COztBQUNBLGdCQUFNaUIsTUFBTSxHQUFHLDZCQUFDLFVBQUQ7QUFBWSxZQUFBLElBQUksRUFBRSxLQUFLL0YsS0FBTCxDQUFXaEIsSUFBN0I7QUFBbUMsWUFBQSxPQUFPLEVBQUUsS0FBS2dCLEtBQUwsQ0FBV3ZCO0FBQXZELFlBQWY7O0FBRUEsZ0JBQU11SCxZQUFZLEdBQUcsS0FBSzVDLGdCQUFMLEVBQXJCOztBQUNBLGNBQUk2QyxjQUFKOztBQUNBLGNBQUlELFlBQUosRUFBa0I7QUFDZEMsWUFBQUEsY0FBYyxHQUFHLDJDQUNiO0FBQU0sY0FBQSxTQUFTLEVBQUM7QUFBaEIsZUFDS0QsWUFBWSxDQUFDRSxjQURsQixDQURhLFFBR0hGLFlBQVksQ0FBQ0csTUFIVixNQUFqQjtBQUtILFdBTkQsTUFNTztBQUNIRixZQUFBQSxjQUFjLEdBQUk7QUFBTSxjQUFBLFNBQVMsRUFBQztBQUFoQixlQUE2QyxLQUFLakcsS0FBTCxDQUFXMUIsV0FBeEQsQ0FBbEI7QUFDSDs7QUFFRCxnQkFBTThILElBQUksR0FBRyxLQUFLNUMsV0FBTCxFQUFiOztBQUNBLGNBQUk0QyxJQUFKLEVBQVU7QUFDTmxCLFlBQUFBLEtBQUssR0FBRyx5QkFBRyxvQ0FBSCxFQUNKO0FBQUVtQixjQUFBQSxJQUFJLEVBQUVMLFlBQVksQ0FBQ3JEO0FBQXJCLGFBREksQ0FBUjtBQUVBd0MsWUFBQUEsUUFBUSxHQUFHLENBQ1BZLE1BRE8sRUFFUCx5QkFBRywyQkFBSCxFQUFnQyxFQUFoQyxFQUFvQztBQUFDTyxjQUFBQSxRQUFRLEVBQUUsTUFBTUw7QUFBakIsYUFBcEMsQ0FGTyxDQUFYO0FBSUFaLFlBQUFBLGtCQUFrQixHQUFHLHlCQUFHLGdCQUFILENBQXJCO0FBQ0gsV0FSRCxNQVFPO0FBQ0hILFlBQUFBLEtBQUssR0FBRyx5QkFBRyxtQ0FBSCxFQUNKO0FBQUVTLGNBQUFBLFFBQVEsRUFBRSxLQUFLekMsU0FBTDtBQUFaLGFBREksQ0FBUjtBQUVBaUMsWUFBQUEsUUFBUSxHQUFHLENBQ1BZLE1BRE8sRUFFUCx5QkFBRyx5QkFBSCxFQUE4QixFQUE5QixFQUFrQztBQUFDTyxjQUFBQSxRQUFRLEVBQUUsTUFBTUw7QUFBakIsYUFBbEMsQ0FGTyxDQUFYO0FBSUFaLFlBQUFBLGtCQUFrQixHQUFHLHlCQUFHLFFBQUgsQ0FBckI7QUFDSDs7QUFFREQsVUFBQUEsb0JBQW9CLEdBQUcsS0FBS3BGLEtBQUwsQ0FBV2hDLFdBQWxDO0FBQ0F1SCxVQUFBQSxvQkFBb0IsR0FBRyx5QkFBRyxRQUFILENBQXZCO0FBQ0FELFVBQUFBLHNCQUFzQixHQUFHLEtBQUt0RixLQUFMLENBQVc3QixhQUFwQzs7QUFFQSxjQUFJLEtBQUs2QixLQUFMLENBQVc1QixzQkFBZixFQUF1QztBQUNuQ3FILFlBQUFBLGVBQWUsQ0FBQ2MsSUFBaEIsQ0FDSSw2QkFBQyxnQkFBRDtBQUFrQixjQUFBLElBQUksRUFBQyxXQUF2QjtBQUFtQyxjQUFBLE9BQU8sRUFBRSxLQUFLdkcsS0FBTCxDQUFXNUIsc0JBQXZEO0FBQStFLGNBQUEsR0FBRyxFQUFDO0FBQW5GLGVBQ00seUJBQUcsc0JBQUgsQ0FETixDQURKO0FBS0g7O0FBQ0Q7QUFDSDs7QUFDRCxXQUFLdkIsV0FBVyxDQUFDYyxXQUFqQjtBQUE4QjtBQUMxQixjQUFJLEtBQUtxQyxLQUFMLENBQVduQixVQUFmLEVBQTJCO0FBQ3ZCcUcsWUFBQUEsS0FBSyxHQUFHLHlCQUFHLGtEQUFILEVBQ0o7QUFBQ1MsY0FBQUEsUUFBUSxFQUFFLEtBQUt6QyxTQUFMO0FBQVgsYUFESSxDQUFSO0FBRUgsV0FIRCxNQUdPO0FBQ0hnQyxZQUFBQSxLQUFLLEdBQUcseUJBQUcsMERBQUgsRUFDSjtBQUFDUyxjQUFBQSxRQUFRLEVBQUUsS0FBS3pDLFNBQUwsQ0FBZSxJQUFmO0FBQVgsYUFESSxDQUFSO0FBRUg7O0FBQ0RtQyxVQUFBQSxrQkFBa0IsR0FBRyx5QkFBRyxxQkFBSCxDQUFyQjtBQUNBRCxVQUFBQSxvQkFBb0IsR0FBRyxLQUFLcEYsS0FBTCxDQUFXaEMsV0FBbEM7QUFDQTtBQUNIOztBQUNELFdBQUtuQixXQUFXLENBQUNlLFlBQWpCO0FBQStCO0FBQzNCc0gsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLDhCQUFILEVBQW1DO0FBQUNTLFlBQUFBLFFBQVEsRUFBRSxLQUFLekMsU0FBTCxDQUFlLElBQWY7QUFBWCxXQUFuQyxDQUFSO0FBQ0FpQyxVQUFBQSxRQUFRLEdBQUcseUJBQUcsa0VBQUgsQ0FBWDtBQUNBO0FBQ0g7O0FBQ0QsV0FBS3RJLFdBQVcsQ0FBQ2dCLFVBQWpCO0FBQTZCO0FBQ3pCcUgsVUFBQUEsS0FBSyxHQUFHLHlCQUFHLDhDQUFILEVBQW1EO0FBQUNTLFlBQUFBLFFBQVEsRUFBRSxLQUFLekMsU0FBTCxDQUFlLElBQWY7QUFBWCxXQUFuRCxDQUFSO0FBQ0FpQyxVQUFBQSxRQUFRLEdBQUcsQ0FDUCx5QkFBRyxtRUFBSCxDQURPLEVBRVAseUJBQ0ksK0RBQ0EsMkRBREEsR0FFQSw2Q0FISixFQUlJO0FBQUVqRCxZQUFBQSxPQUFPLEVBQUUsS0FBS2xDLEtBQUwsQ0FBV3BCLEtBQVgsQ0FBaUJzRDtBQUE1QixXQUpKLEVBS0k7QUFBRXNFLFlBQUFBLFNBQVMsRUFBRUMsS0FBSyxJQUFJO0FBQUcsY0FBQSxJQUFJLEVBQUMseURBQVI7QUFDbEIsY0FBQSxNQUFNLEVBQUMsUUFEVztBQUNGLGNBQUEsR0FBRyxFQUFDO0FBREYsZUFDMEJBLEtBRDFCO0FBQXRCLFdBTEosQ0FGTyxDQUFYO0FBV0E7QUFDSDtBQXhOTDs7QUEyTkEsUUFBSUMsZ0JBQUo7O0FBQ0EsUUFBSXZCLFFBQUosRUFBYztBQUNWLFVBQUksQ0FBQ3dCLEtBQUssQ0FBQ0MsT0FBTixDQUFjekIsUUFBZCxDQUFMLEVBQThCO0FBQzFCQSxRQUFBQSxRQUFRLEdBQUcsQ0FBQ0EsUUFBRCxDQUFYO0FBQ0g7O0FBQ0R1QixNQUFBQSxnQkFBZ0IsR0FBR3ZCLFFBQVEsQ0FBQ3hFLEdBQVQsQ0FBYSxDQUFDa0csQ0FBRCxFQUFJQyxDQUFKLEtBQVU7QUFBRyxRQUFBLEdBQUcsb0JBQWFBLENBQWI7QUFBTixTQUF5QkQsQ0FBekIsQ0FBdkIsQ0FBbkI7QUFDSDs7QUFFRCxRQUFJRSxZQUFKOztBQUNBLFFBQUkvQixXQUFKLEVBQWlCO0FBQ2IrQixNQUFBQSxZQUFZLEdBQUc7QUFBSSxRQUFBLFNBQVMsRUFBQztBQUFkLFNBQStDLDZCQUFDLE9BQUQsT0FBL0MsRUFBNEQ3QixLQUE1RCxDQUFmO0FBQ0gsS0FGRCxNQUVPO0FBQ0g2QixNQUFBQSxZQUFZLEdBQUcseUNBQU03QixLQUFOLENBQWY7QUFDSDs7QUFFRCxRQUFJOEIsYUFBSjs7QUFDQSxRQUFJNUIsb0JBQUosRUFBMEI7QUFDdEI0QixNQUFBQSxhQUFhLEdBQ1QsNkJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsUUFBQSxPQUFPLEVBQUU1QjtBQUExQyxTQUNNQyxrQkFETixDQURKO0FBS0g7O0FBRUQsUUFBSTRCLGVBQUo7O0FBQ0EsUUFBSTNCLHNCQUFKLEVBQTRCO0FBQ3hCMkIsTUFBQUEsZUFBZSxHQUNYLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsSUFBSSxFQUFDLFdBQXZCO0FBQW1DLFFBQUEsT0FBTyxFQUFFM0I7QUFBNUMsU0FDTUMsb0JBRE4sQ0FESjtBQUtIOztBQUVELFVBQU0yQixPQUFPLEdBQUcseUJBQVcsbUJBQVgsRUFBZ0MsWUFBaEMsOEJBQW1FeEIsV0FBbkUsR0FBa0Y7QUFDOUYsaUNBQTJCLEtBQUsxRixLQUFMLENBQVduQixVQUR3RDtBQUU5RixrQ0FBNEIsQ0FBQyxLQUFLbUIsS0FBTCxDQUFXbkIsVUFGc0Q7QUFHOUYsZ0NBQTBCb0c7QUFIb0UsS0FBbEYsQ0FBaEI7QUFNQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUVpQztBQUFoQixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNSCxZQUROLEVBRU1MLGdCQUZOLENBREosRUFLSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTU8sZUFETixFQUVNeEIsZUFGTixFQUdNdUIsYUFITixDQUxKLEVBVUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ014QixNQUROLENBVkosQ0FESjtBQWdCSDtBQWhoQjJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgSWRlbnRpdHlBdXRoQ2xpZW50IGZyb20gJy4uLy4uLy4uL0lkZW50aXR5QXV0aENsaWVudCc7XHJcblxyXG5jb25zdCBNZXNzYWdlQ2FzZSA9IE9iamVjdC5mcmVlemUoe1xyXG4gICAgTm90TG9nZ2VkSW46IFwiTm90TG9nZ2VkSW5cIixcclxuICAgIEpvaW5pbmc6IFwiSm9pbmluZ1wiLFxyXG4gICAgTG9hZGluZzogXCJMb2FkaW5nXCIsXHJcbiAgICBSZWplY3Rpbmc6IFwiUmVqZWN0aW5nXCIsXHJcbiAgICBLaWNrZWQ6IFwiS2lja2VkXCIsXHJcbiAgICBCYW5uZWQ6IFwiQmFubmVkXCIsXHJcbiAgICBPdGhlclRocmVlUElERXJyb3I6IFwiT3RoZXJUaHJlZVBJREVycm9yXCIsXHJcbiAgICBJbnZpdGVkRW1haWxOb3RGb3VuZEluQWNjb3VudDogXCJJbnZpdGVkRW1haWxOb3RGb3VuZEluQWNjb3VudFwiLFxyXG4gICAgSW52aXRlZEVtYWlsTm9JZGVudGl0eVNlcnZlcjogXCJJbnZpdGVkRW1haWxOb0lkZW50aXR5U2VydmVyXCIsXHJcbiAgICBJbnZpdGVkRW1haWxNaXNtYXRjaDogXCJJbnZpdGVkRW1haWxNaXNtYXRjaFwiLFxyXG4gICAgSW52aXRlOiBcIkludml0ZVwiLFxyXG4gICAgVmlld2luZ1Jvb206IFwiVmlld2luZ1Jvb21cIixcclxuICAgIFJvb21Ob3RGb3VuZDogXCJSb29tTm90Rm91bmRcIixcclxuICAgIE90aGVyRXJyb3I6IFwiT3RoZXJFcnJvclwiLFxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tUHJldmlld0JhcicsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgb25Kb2luQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uUmVqZWN0Q2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uUmVqZWN0QW5kSWdub3JlQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIG9uRm9yZ2V0Q2xpY2s6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgICAgIC8vIGlmIGludml0ZXJOYW1lIGlzIHNwZWNpZmllZCwgdGhlIHByZXZpZXcgYmFyIHdpbGwgc2hvd24gYW4gaW52aXRlIHRvIHRoZSByb29tLlxyXG4gICAgICAgIC8vIFlvdSBzaG91bGQgYWxzbyBzcGVjaWZ5IG9uUmVqZWN0Q2xpY2sgaWYgc3BlY2lmaXlpbmcgaW52aXRlck5hbWVcclxuICAgICAgICBpbnZpdGVyTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuXHJcbiAgICAgICAgLy8gSWYgaW52aXRlZCBieSAzcmQgcGFydHkgaW52aXRlLCB0aGUgZW1haWwgYWRkcmVzcyB0aGUgaW52aXRlIHdhcyBzZW50IHRvXHJcbiAgICAgICAgaW52aXRlZEVtYWlsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG5cclxuICAgICAgICAvLyBGb3IgdGhpcmQgcGFydHkgaW52aXRlcywgaW5mb3JtYXRpb24gcGFzc2VkIGFib3V0IHRoZSByb29tIG91dC1vZi1iYW5kXHJcbiAgICAgICAgb29iRGF0YTogUHJvcFR5cGVzLm9iamVjdCxcclxuXHJcbiAgICAgICAgLy8gRm9yIHRoaXJkIHBhcnR5IGludml0ZXMsIGEgVVJMIGZvciBhIDNwaWQgaW52aXRlIHNpZ25pbmcgc2VydmljZVxyXG4gICAgICAgIHNpZ25Vcmw6IFByb3BUeXBlcy5zdHJpbmcsXHJcblxyXG4gICAgICAgIC8vIEEgc3RhbmRhcmQgY2xpZW50L3NlcnZlciBBUEkgZXJyb3Igb2JqZWN0LiBJZiBzdXBwbGllZCwgaW5kaWNhdGVzIHRoYXQgdGhlXHJcbiAgICAgICAgLy8gY2FsbGVyIHdhcyB1bmFibGUgdG8gZmV0Y2ggZGV0YWlscyBhYm91dCB0aGUgcm9vbSBmb3IgdGhlIGdpdmVuIHJlYXNvbi5cclxuICAgICAgICBlcnJvcjogUHJvcFR5cGVzLm9iamVjdCxcclxuXHJcbiAgICAgICAgY2FuUHJldmlldzogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICAgICAgcHJldmlld0xvYWRpbmc6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIHJvb206IFByb3BUeXBlcy5vYmplY3QsXHJcblxyXG4gICAgICAgIC8vIFdoZW4gYSBzcGlubmVyIGlzIHByZXNlbnQsIGEgc3Bpbm5lclN0YXRlIGNhbiBiZSBzcGVjaWZpZWQgdG8gaW5kaWNhdGUgdGhlXHJcbiAgICAgICAgLy8gcHVycG9zZSBvZiB0aGUgc3Bpbm5lci5cclxuICAgICAgICBzcGlubmVyOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBzcGlubmVyU3RhdGU6IFByb3BUeXBlcy5vbmVPZihbXCJqb2luaW5nXCJdKSxcclxuICAgICAgICBsb2FkaW5nOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBqb2luaW5nOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICByZWplY3Rpbmc6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgICAgIC8vIFRoZSBhbGlhcyB0aGF0IHdhcyB1c2VkIHRvIGFjY2VzcyB0aGlzIHJvb20sIGlmIGFwcHJvcHJpYXRlXHJcbiAgICAgICAgLy8gSWYgZ2l2ZW4sIHRoaXMgd2lsbCBiZSBob3cgdGhlIHJvb20gaXMgcmVmZXJyZWQgdG8gKGVnLlxyXG4gICAgICAgIC8vIGluIGVycm9yIG1lc3NhZ2VzKS5cclxuICAgICAgICByb29tQWxpYXM6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgb25Kb2luQ2xpY2s6IGZ1bmN0aW9uKCkge30sXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBidXN5OiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fY2hlY2tJbnZpdGVkRW1haWwoKTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbihwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmludml0ZWRFbWFpbCAhPT0gcHJldlByb3BzLmludml0ZWRFbWFpbCB8fCB0aGlzLnByb3BzLmludml0ZXJOYW1lICE9PSBwcmV2UHJvcHMuaW52aXRlck5hbWUpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2hlY2tJbnZpdGVkRW1haWwoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9jaGVja0ludml0ZWRFbWFpbDogYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gSWYgdGhpcyBpcyBhbiBpbnZpdGUgYW5kIHdlJ3ZlIGJlZW4gdG9sZCB3aGF0IGVtYWlsIGFkZHJlc3Mgd2FzXHJcbiAgICAgICAgLy8gaW52aXRlZCwgZmV0Y2ggdGhlIHVzZXIncyBhY2NvdW50IGVtYWlscyBhbmQgZGlzY292ZXJ5IGJpbmRpbmdzIHNvIHdlXHJcbiAgICAgICAgLy8gY2FuIGNoZWNrIHRoZW0gYWdhaW5zdCB0aGUgZW1haWwgdGhhdCB3YXMgaW52aXRlZC5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5pbnZpdGVyTmFtZSAmJiB0aGlzLnByb3BzLmludml0ZWRFbWFpbCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtidXN5OiB0cnVlfSk7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAvLyBHYXRoZXIgdGhlIGFjY291bnQgM1BJRHNcclxuICAgICAgICAgICAgICAgIGNvbnN0IGFjY291bnQzcGlkcyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRUaHJlZVBpZHMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjY291bnRFbWFpbHM6IGFjY291bnQzcGlkcy50aHJlZXBpZHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbHRlcihiID0+IGIubWVkaXVtID09PSAnZW1haWwnKS5tYXAoYiA9PiBiLmFkZHJlc3MpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAvLyBJZiB3ZSBoYXZlIGFuIElTIGNvbm5lY3RlZCwgdXNlIHRoYXQgdG8gbG9va3VwIHRoZSBlbWFpbCBhbmRcclxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIHRoZSBib3VuZCBNWElELlxyXG4gICAgICAgICAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2J1c3k6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY29uc3QgYXV0aENsaWVudCA9IG5ldyBJZGVudGl0eUF1dGhDbGllbnQoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlkZW50aXR5QWNjZXNzVG9rZW4gPSBhd2FpdCBhdXRoQ2xpZW50LmdldEFjY2Vzc1Rva2VuKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkubG9va3VwVGhyZWVQaWQoXHJcbiAgICAgICAgICAgICAgICAgICAgJ2VtYWlsJyxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmludml0ZWRFbWFpbCxcclxuICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWQgLyogY2FsbGJhY2sgKi8sXHJcbiAgICAgICAgICAgICAgICAgICAgaWRlbnRpdHlBY2Nlc3NUb2tlbixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpbnZpdGVkRW1haWxNeGlkOiByZXN1bHQubXhpZH0pO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3RocmVlUGlkRmV0Y2hFcnJvcjogZXJyfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YnVzeTogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRNZXNzYWdlQ2FzZSgpIHtcclxuICAgICAgICBjb25zdCBpc0d1ZXN0ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzR3Vlc3QoKTtcclxuXHJcbiAgICAgICAgaWYgKGlzR3Vlc3QpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE1lc3NhZ2VDYXNlLk5vdExvZ2dlZEluO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbXlNZW1iZXIgPSB0aGlzLl9nZXRNeU1lbWJlcigpO1xyXG5cclxuICAgICAgICBpZiAobXlNZW1iZXIpIHtcclxuICAgICAgICAgICAgaWYgKG15TWVtYmVyLmlzS2lja2VkKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5LaWNrZWQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobXlNZW1iZXIubWVtYmVyc2hpcCA9PT0gXCJiYW5cIikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIE1lc3NhZ2VDYXNlLkJhbm5lZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuam9pbmluZykge1xyXG4gICAgICAgICAgICByZXR1cm4gTWVzc2FnZUNhc2UuSm9pbmluZztcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMucmVqZWN0aW5nKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5SZWplY3Rpbmc7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByb3BzLmxvYWRpbmcgfHwgdGhpcy5zdGF0ZS5idXN5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5Mb2FkaW5nO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuaW52aXRlck5hbWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMuaW52aXRlZEVtYWlsKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS50aHJlZVBpZEZldGNoRXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gTWVzc2FnZUNhc2UuT3RoZXJUaHJlZVBJREVycm9yO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmFjY291bnRFbWFpbHMgJiZcclxuICAgICAgICAgICAgICAgICAgICAhdGhpcy5zdGF0ZS5hY2NvdW50RW1haWxzLmluY2x1ZGVzKHRoaXMucHJvcHMuaW52aXRlZEVtYWlsKVxyXG4gICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE1lc3NhZ2VDYXNlLkludml0ZWRFbWFpbE5vdEZvdW5kSW5BY2NvdW50O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gTWVzc2FnZUNhc2UuSW52aXRlZEVtYWlsTm9JZGVudGl0eVNlcnZlcjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5pbnZpdGVkRW1haWxNeGlkICE9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5JbnZpdGVkRW1haWxNaXNtYXRjaDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gTWVzc2FnZUNhc2UuSW52aXRlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy5lcnJvcikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5lcnJvci5lcnJjb2RlID09ICdNX05PVF9GT1VORCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5Sb29tTm90Rm91bmQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWVzc2FnZUNhc2UuT3RoZXJFcnJvcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNZXNzYWdlQ2FzZS5WaWV3aW5nUm9vbTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRLaWNrT3JCYW5JbmZvKCkge1xyXG4gICAgICAgIGNvbnN0IG15TWVtYmVyID0gdGhpcy5fZ2V0TXlNZW1iZXIoKTtcclxuICAgICAgICBpZiAoIW15TWVtYmVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3Qga2lja2VyTWVtYmVyID0gdGhpcy5wcm9wcy5yb29tLmN1cnJlbnRTdGF0ZS5nZXRNZW1iZXIoXHJcbiAgICAgICAgICAgIG15TWVtYmVyLmV2ZW50cy5tZW1iZXIuZ2V0U2VuZGVyKCksXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zdCBtZW1iZXJOYW1lID0ga2lja2VyTWVtYmVyID9cclxuICAgICAgICAgICAga2lja2VyTWVtYmVyLm5hbWUgOiBteU1lbWJlci5ldmVudHMubWVtYmVyLmdldFNlbmRlcigpO1xyXG4gICAgICAgIGNvbnN0IHJlYXNvbiA9IG15TWVtYmVyLmV2ZW50cy5tZW1iZXIuZ2V0Q29udGVudCgpLnJlYXNvbjtcclxuICAgICAgICByZXR1cm4ge21lbWJlck5hbWUsIHJlYXNvbn07XHJcbiAgICB9LFxyXG5cclxuICAgIF9qb2luUnVsZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMucHJvcHMucm9vbTtcclxuICAgICAgICBpZiAocm9vbSkge1xyXG4gICAgICAgICAgICBjb25zdCBqb2luUnVsZXMgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLmpvaW5fcnVsZXMnLCAnJyk7XHJcbiAgICAgICAgICAgIGlmIChqb2luUnVsZXMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBqb2luUnVsZXMuZ2V0Q29udGVudCgpLmpvaW5fcnVsZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX3Jvb21OYW1lOiBmdW5jdGlvbihhdFN0YXJ0ID0gZmFsc2UpIHtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5wcm9wcy5yb29tID8gdGhpcy5wcm9wcy5yb29tLm5hbWUgOiB0aGlzLnByb3BzLnJvb21BbGlhcztcclxuICAgICAgICBpZiAobmFtZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmFtZTtcclxuICAgICAgICB9IGVsc2UgaWYgKGF0U3RhcnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90KFwiVGhpcyByb29tXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdChcInRoaXMgcm9vbVwiKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRNeU1lbWJlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICB0aGlzLnByb3BzLnJvb20gJiZcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5yb29tLmdldE1lbWJlcihNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCkpXHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldEludml0ZU1lbWJlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qge3Jvb219ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBteVVzZXJJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKTtcclxuICAgICAgICBjb25zdCBpbnZpdGVFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldE1lbWJlcihteVVzZXJJZCk7XHJcbiAgICAgICAgaWYgKCFpbnZpdGVFdmVudCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGludml0ZXJVc2VySWQgPSBpbnZpdGVFdmVudC5ldmVudHMubWVtYmVyLmdldFNlbmRlcigpO1xyXG4gICAgICAgIHJldHVybiByb29tLmN1cnJlbnRTdGF0ZS5nZXRNZW1iZXIoaW52aXRlclVzZXJJZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9pc0RNSW52aXRlKCkge1xyXG4gICAgICAgIGNvbnN0IG15TWVtYmVyID0gdGhpcy5fZ2V0TXlNZW1iZXIoKTtcclxuICAgICAgICBpZiAoIW15TWVtYmVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbWVtYmVyRXZlbnQgPSBteU1lbWJlci5ldmVudHMubWVtYmVyO1xyXG4gICAgICAgIGNvbnN0IG1lbWJlckNvbnRlbnQgPSBtZW1iZXJFdmVudC5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgcmV0dXJuIG1lbWJlckNvbnRlbnQubWVtYmVyc2hpcCA9PT0gXCJpbnZpdGVcIiAmJiBtZW1iZXJDb250ZW50LmlzX2RpcmVjdDtcclxuICAgIH0sXHJcblxyXG4gICAgX21ha2VTY3JlZW5BZnRlckxvZ2luKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHNjcmVlbjogJ3Jvb20nLFxyXG4gICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB0aGlzLnByb3BzLmludml0ZWRFbWFpbCxcclxuICAgICAgICAgICAgICAgIHNpZ251cmw6IHRoaXMucHJvcHMuc2lnblVybCxcclxuICAgICAgICAgICAgICAgIHJvb21fbmFtZTogdGhpcy5wcm9wcy5vb2JEYXRhLnJvb21fbmFtZSxcclxuICAgICAgICAgICAgICAgIHJvb21fYXZhdGFyX3VybDogdGhpcy5wcm9wcy5vb2JEYXRhLmF2YXRhclVybCxcclxuICAgICAgICAgICAgICAgIGludml0ZXJfbmFtZTogdGhpcy5wcm9wcy5vb2JEYXRhLmludml0ZXJOYW1lLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgb25Mb2dpbkNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICdzdGFydF9sb2dpbicsIHNjcmVlbkFmdGVyTG9naW46IHRoaXMuX21ha2VTY3JlZW5BZnRlckxvZ2luKCkgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVnaXN0ZXJDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHsgYWN0aW9uOiAnc3RhcnRfcmVnaXN0cmF0aW9uJywgc2NyZWVuQWZ0ZXJMb2dpbjogdGhpcy5fbWFrZVNjcmVlbkFmdGVyTG9naW4oKSB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBTcGlubmVyID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuU3Bpbm5lcicpO1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcblxyXG4gICAgICAgIGxldCBzaG93U3Bpbm5lciA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBkYXJrU3R5bGUgPSBmYWxzZTtcclxuICAgICAgICBsZXQgdGl0bGU7XHJcbiAgICAgICAgbGV0IHN1YlRpdGxlO1xyXG4gICAgICAgIGxldCBwcmltYXJ5QWN0aW9uSGFuZGxlcjtcclxuICAgICAgICBsZXQgcHJpbWFyeUFjdGlvbkxhYmVsO1xyXG4gICAgICAgIGxldCBzZWNvbmRhcnlBY3Rpb25IYW5kbGVyO1xyXG4gICAgICAgIGxldCBzZWNvbmRhcnlBY3Rpb25MYWJlbDtcclxuICAgICAgICBsZXQgZm9vdGVyO1xyXG4gICAgICAgIGNvbnN0IGV4dHJhQ29tcG9uZW50cyA9IFtdO1xyXG5cclxuICAgICAgICBjb25zdCBtZXNzYWdlQ2FzZSA9IHRoaXMuX2dldE1lc3NhZ2VDYXNlKCk7XHJcbiAgICAgICAgc3dpdGNoIChtZXNzYWdlQ2FzZSkge1xyXG4gICAgICAgICAgICBjYXNlIE1lc3NhZ2VDYXNlLkpvaW5pbmc6IHtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXCJKb2luaW5nIHJvb20g4oCmXCIpO1xyXG4gICAgICAgICAgICAgICAgc2hvd1NwaW5uZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5Mb2FkaW5nOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiTG9hZGluZyDigKZcIik7XHJcbiAgICAgICAgICAgICAgICBzaG93U3Bpbm5lciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIE1lc3NhZ2VDYXNlLlJlamVjdGluZzoge1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBfdChcIlJlamVjdGluZyBpbnZpdGUg4oCmXCIpO1xyXG4gICAgICAgICAgICAgICAgc2hvd1NwaW5uZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5Ob3RMb2dnZWRJbjoge1xyXG4gICAgICAgICAgICAgICAgZGFya1N0eWxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXCJKb2luIHRoZSBjb252ZXJzYXRpb24gd2l0aCBhbiBhY2NvdW50XCIpO1xyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJTaWduIFVwXCIpO1xyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLm9uUmVnaXN0ZXJDbGljaztcclxuICAgICAgICAgICAgICAgIHNlY29uZGFyeUFjdGlvbkxhYmVsID0gX3QoXCJTaWduIEluXCIpO1xyXG4gICAgICAgICAgICAgICAgc2Vjb25kYXJ5QWN0aW9uSGFuZGxlciA9IHRoaXMub25Mb2dpbkNsaWNrO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMucHJldmlld0xvYWRpbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb290ZXIgPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3Bpbm5lciB3PXsyMH0gaD17MjB9Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkxvYWRpbmcgcm9vbSBwcmV2aWV3XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5LaWNrZWQ6IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHttZW1iZXJOYW1lLCByZWFzb259ID0gdGhpcy5fZ2V0S2lja09yQmFuSW5mbygpO1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBfdChcIllvdSB3ZXJlIGtpY2tlZCBmcm9tICUocm9vbU5hbWUpcyBieSAlKG1lbWJlck5hbWUpc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHttZW1iZXJOYW1lLCByb29tTmFtZTogdGhpcy5fcm9vbU5hbWUoKX0pO1xyXG4gICAgICAgICAgICAgICAgc3ViVGl0bGUgPSByZWFzb24gPyBfdChcIlJlYXNvbjogJShyZWFzb24pc1wiLCB7cmVhc29ufSkgOiBudWxsO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9qb2luUnVsZSgpID09PSBcImludml0ZVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJGb3JnZXQgdGhpcyByb29tXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25IYW5kbGVyID0gdGhpcy5wcm9wcy5vbkZvcmdldENsaWNrO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uTGFiZWwgPSBfdChcIlJlLWpvaW5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLnByb3BzLm9uSm9pbkNsaWNrO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlY29uZGFyeUFjdGlvbkxhYmVsID0gX3QoXCJGb3JnZXQgdGhpcyByb29tXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlY29uZGFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLnByb3BzLm9uRm9yZ2V0Q2xpY2s7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIE1lc3NhZ2VDYXNlLkJhbm5lZDoge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qge21lbWJlck5hbWUsIHJlYXNvbn0gPSB0aGlzLl9nZXRLaWNrT3JCYW5JbmZvKCk7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiWW91IHdlcmUgYmFubmVkIGZyb20gJShyb29tTmFtZSlzIGJ5ICUobWVtYmVyTmFtZSlzXCIsXHJcbiAgICAgICAgICAgICAgICAgICAge21lbWJlck5hbWUsIHJvb21OYW1lOiB0aGlzLl9yb29tTmFtZSgpfSk7XHJcbiAgICAgICAgICAgICAgICBzdWJUaXRsZSA9IHJlYXNvbiA/IF90KFwiUmVhc29uOiAlKHJlYXNvbilzXCIsIHtyZWFzb259KSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uTGFiZWwgPSBfdChcIkZvcmdldCB0aGlzIHJvb21cIik7XHJcbiAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uSGFuZGxlciA9IHRoaXMucHJvcHMub25Gb3JnZXRDbGljaztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhc2UgTWVzc2FnZUNhc2UuT3RoZXJUaHJlZVBJREVycm9yOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2l0aCB5b3VyIGludml0ZSB0byAlKHJvb21OYW1lKXNcIixcclxuICAgICAgICAgICAgICAgICAgICB7cm9vbU5hbWU6IHRoaXMuX3Jvb21OYW1lKCl9KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGpvaW5SdWxlID0gdGhpcy5fam9pblJ1bGUoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGVyckNvZGVNZXNzYWdlID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJBbiBlcnJvciAoJShlcnJjb2RlKXMpIHdhcyByZXR1cm5lZCB3aGlsZSB0cnlpbmcgdG8gdmFsaWRhdGUgeW91ciBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJpbnZpdGUuIFlvdSBjb3VsZCB0cnkgdG8gcGFzcyB0aGlzIGluZm9ybWF0aW9uIG9uIHRvIGEgcm9vbSBhZG1pbi5cIixcclxuICAgICAgICAgICAgICAgICAgICB7ZXJyY29kZTogdGhpcy5zdGF0ZS50aHJlZVBpZEZldGNoRXJyb3IuZXJyY29kZSB8fCBfdChcInVua25vd24gZXJyb3IgY29kZVwiKX0sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChqb2luUnVsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJpbnZpdGVcIjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3ViVGl0bGUgPSBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdChcIllvdSBjYW4gb25seSBqb2luIGl0IHdpdGggYSB3b3JraW5nIGludml0ZS5cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJDb2RlTWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJUcnkgdG8gam9pbiBhbnl3YXlcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25IYW5kbGVyID0gdGhpcy5wcm9wcy5vbkpvaW5DbGljaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcInB1YmxpY1wiOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJUaXRsZSA9IF90KFwiWW91IGNhbiBzdGlsbCBqb2luIGl0IGJlY2F1c2UgdGhpcyBpcyBhIHB1YmxpYyByb29tLlwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJKb2luIHRoZSBkaXNjdXNzaW9uXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uSGFuZGxlciA9IHRoaXMucHJvcHMub25Kb2luQ2xpY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1YlRpdGxlID0gZXJyQ29kZU1lc3NhZ2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25MYWJlbCA9IF90KFwiVHJ5IHRvIGpvaW4gYW55d2F5XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uSGFuZGxlciA9IHRoaXMucHJvcHMub25Kb2luQ2xpY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5JbnZpdGVkRW1haWxOb3RGb3VuZEluQWNjb3VudDoge1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlRoaXMgaW52aXRlIHRvICUocm9vbU5hbWUpcyB3YXMgc2VudCB0byAlKGVtYWlsKXMgd2hpY2ggaXMgbm90IFwiICtcclxuICAgICAgICAgICAgICAgICAgICBcImFzc29jaWF0ZWQgd2l0aCB5b3VyIGFjY291bnRcIixcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvb21OYW1lOiB0aGlzLl9yb29tTmFtZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogdGhpcy5wcm9wcy5pbnZpdGVkRW1haWwsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBzdWJUaXRsZSA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiTGluayB0aGlzIGVtYWlsIHdpdGggeW91ciBhY2NvdW50IGluIFNldHRpbmdzIHRvIHJlY2VpdmUgaW52aXRlcyBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCJkaXJlY3RseSBpbiBSaW90LlwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25MYWJlbCA9IF90KFwiSm9pbiB0aGUgZGlzY3Vzc2lvblwiKTtcclxuICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25IYW5kbGVyID0gdGhpcy5wcm9wcy5vbkpvaW5DbGljaztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhc2UgTWVzc2FnZUNhc2UuSW52aXRlZEVtYWlsTm9JZGVudGl0eVNlcnZlcjoge1xyXG4gICAgICAgICAgICAgICAgdGl0bGUgPSBfdChcclxuICAgICAgICAgICAgICAgICAgICBcIlRoaXMgaW52aXRlIHRvICUocm9vbU5hbWUpcyB3YXMgc2VudCB0byAlKGVtYWlsKXNcIixcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvb21OYW1lOiB0aGlzLl9yb29tTmFtZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogdGhpcy5wcm9wcy5pbnZpdGVkRW1haWwsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBzdWJUaXRsZSA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVXNlIGFuIGlkZW50aXR5IHNlcnZlciBpbiBTZXR0aW5ncyB0byByZWNlaXZlIGludml0ZXMgZGlyZWN0bHkgaW4gUmlvdC5cIixcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uTGFiZWwgPSBfdChcIkpvaW4gdGhlIGRpc2N1c3Npb25cIik7XHJcbiAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uSGFuZGxlciA9IHRoaXMucHJvcHMub25Kb2luQ2xpY2s7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIE1lc3NhZ2VDYXNlLkludml0ZWRFbWFpbE1pc21hdGNoOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVGhpcyBpbnZpdGUgdG8gJShyb29tTmFtZSlzIHdhcyBzZW50IHRvICUoZW1haWwpc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbU5hbWU6IHRoaXMuX3Jvb21OYW1lKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0aGlzLnByb3BzLmludml0ZWRFbWFpbCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHN1YlRpdGxlID0gX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJTaGFyZSB0aGlzIGVtYWlsIGluIFNldHRpbmdzIHRvIHJlY2VpdmUgaW52aXRlcyBkaXJlY3RseSBpbiBSaW90LlwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25MYWJlbCA9IF90KFwiSm9pbiB0aGUgZGlzY3Vzc2lvblwiKTtcclxuICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb25IYW5kbGVyID0gdGhpcy5wcm9wcy5vbkpvaW5DbGljaztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhc2UgTWVzc2FnZUNhc2UuSW52aXRlOiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBSb29tQXZhdGFyID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmF2YXRhcnMuUm9vbUF2YXRhclwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGF2YXRhciA9IDxSb29tQXZhdGFyIHJvb209e3RoaXMucHJvcHMucm9vbX0gb29iRGF0YT17dGhpcy5wcm9wcy5vb2JEYXRhfSAvPjtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbnZpdGVNZW1iZXIgPSB0aGlzLl9nZXRJbnZpdGVNZW1iZXIoKTtcclxuICAgICAgICAgICAgICAgIGxldCBpbnZpdGVyRWxlbWVudDtcclxuICAgICAgICAgICAgICAgIGlmIChpbnZpdGVNZW1iZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBpbnZpdGVyRWxlbWVudCA9IDxzcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJteF9Sb29tUHJldmlld0Jhcl9pbnZpdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7aW52aXRlTWVtYmVyLnJhd0Rpc3BsYXlOYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+ICh7aW52aXRlTWVtYmVyLnVzZXJJZH0pXHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW52aXRlckVsZW1lbnQgPSAoPHNwYW4gY2xhc3NOYW1lPVwibXhfUm9vbVByZXZpZXdCYXJfaW52aXRlclwiPnt0aGlzLnByb3BzLmludml0ZXJOYW1lfTwvc3Bhbj4pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGlzRE0gPSB0aGlzLl9pc0RNSW52aXRlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXNETSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXCJEbyB5b3Ugd2FudCB0byBjaGF0IHdpdGggJSh1c2VyKXM/XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdXNlcjogaW52aXRlTWVtYmVyLm5hbWUgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc3ViVGl0bGUgPSBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF2YXRhcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3QoXCI8dXNlck5hbWUvPiB3YW50cyB0byBjaGF0XCIsIHt9LCB7dXNlck5hbWU6ICgpID0+IGludml0ZXJFbGVtZW50fSksXHJcbiAgICAgICAgICAgICAgICAgICAgXTtcclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QWN0aW9uTGFiZWwgPSBfdChcIlN0YXJ0IGNoYXR0aW5nXCIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiRG8geW91IHdhbnQgdG8gam9pbiAlKHJvb21OYW1lKXM/XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgcm9vbU5hbWU6IHRoaXMuX3Jvb21OYW1lKCkgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc3ViVGl0bGUgPSBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF2YXRhcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3QoXCI8dXNlck5hbWUvPiBpbnZpdGVkIHlvdVwiLCB7fSwge3VzZXJOYW1lOiAoKSA9PiBpbnZpdGVyRWxlbWVudH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIF07XHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJBY2NlcHRcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLnByb3BzLm9uSm9pbkNsaWNrO1xyXG4gICAgICAgICAgICAgICAgc2Vjb25kYXJ5QWN0aW9uTGFiZWwgPSBfdChcIlJlamVjdFwiKTtcclxuICAgICAgICAgICAgICAgIHNlY29uZGFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLnByb3BzLm9uUmVqZWN0Q2xpY2s7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMub25SZWplY3RBbmRJZ25vcmVDbGljaykge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4dHJhQ29tcG9uZW50cy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBraW5kPVwic2Vjb25kYXJ5XCIgb25DbGljaz17dGhpcy5wcm9wcy5vblJlamVjdEFuZElnbm9yZUNsaWNrfSBrZXk9XCJpZ25vcmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoXCJSZWplY3QgJiBJZ25vcmUgdXNlclwiKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj4sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhc2UgTWVzc2FnZUNhc2UuVmlld2luZ1Jvb206IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLmNhblByZXZpZXcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiWW91J3JlIHByZXZpZXdpbmcgJShyb29tTmFtZSlzLiBXYW50IHRvIGpvaW4gaXQ/XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtyb29tTmFtZTogdGhpcy5fcm9vbU5hbWUoKX0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiJShyb29tTmFtZSlzIGNhbid0IGJlIHByZXZpZXdlZC4gRG8geW91IHdhbnQgdG8gam9pbiBpdD9cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAge3Jvb21OYW1lOiB0aGlzLl9yb29tTmFtZSh0cnVlKX0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkxhYmVsID0gX3QoXCJKb2luIHRoZSBkaXNjdXNzaW9uXCIpO1xyXG4gICAgICAgICAgICAgICAgcHJpbWFyeUFjdGlvbkhhbmRsZXIgPSB0aGlzLnByb3BzLm9uSm9pbkNsaWNrO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5Sb29tTm90Rm91bmQ6IHtcclxuICAgICAgICAgICAgICAgIHRpdGxlID0gX3QoXCIlKHJvb21OYW1lKXMgZG9lcyBub3QgZXhpc3QuXCIsIHtyb29tTmFtZTogdGhpcy5fcm9vbU5hbWUodHJ1ZSl9KTtcclxuICAgICAgICAgICAgICAgIHN1YlRpdGxlID0gX3QoXCJUaGlzIHJvb20gZG9lc24ndCBleGlzdC4gQXJlIHlvdSBzdXJlIHlvdSdyZSBhdCB0aGUgcmlnaHQgcGxhY2U/XCIpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSBNZXNzYWdlQ2FzZS5PdGhlckVycm9yOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IF90KFwiJShyb29tTmFtZSlzIGlzIG5vdCBhY2Nlc3NpYmxlIGF0IHRoaXMgdGltZS5cIiwge3Jvb21OYW1lOiB0aGlzLl9yb29tTmFtZSh0cnVlKX0pO1xyXG4gICAgICAgICAgICAgICAgc3ViVGl0bGUgPSBbXHJcbiAgICAgICAgICAgICAgICAgICAgX3QoXCJUcnkgYWdhaW4gbGF0ZXIsIG9yIGFzayBhIHJvb20gYWRtaW4gdG8gY2hlY2sgaWYgeW91IGhhdmUgYWNjZXNzLlwiKSxcclxuICAgICAgICAgICAgICAgICAgICBfdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCIlKGVycmNvZGUpcyB3YXMgcmV0dXJuZWQgd2hpbGUgdHJ5aW5nIHRvIGFjY2VzcyB0aGUgcm9vbS4gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIklmIHlvdSB0aGluayB5b3UncmUgc2VlaW5nIHRoaXMgbWVzc2FnZSBpbiBlcnJvciwgcGxlYXNlIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8aXNzdWVMaW5rPnN1Ym1pdCBhIGJ1ZyByZXBvcnQ8L2lzc3VlTGluaz4uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgZXJyY29kZTogdGhpcy5wcm9wcy5lcnJvci5lcnJjb2RlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgaXNzdWVMaW5rOiBsYWJlbCA9PiA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvbmV3L2Nob29zZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCI+eyBsYWJlbCB9PC9hPiB9LFxyXG4gICAgICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICBdO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzdWJUaXRsZUVsZW1lbnRzO1xyXG4gICAgICAgIGlmIChzdWJUaXRsZSkge1xyXG4gICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoc3ViVGl0bGUpKSB7XHJcbiAgICAgICAgICAgICAgICBzdWJUaXRsZSA9IFtzdWJUaXRsZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc3ViVGl0bGVFbGVtZW50cyA9IHN1YlRpdGxlLm1hcCgodCwgaSkgPT4gPHAga2V5PXtgc3ViVGl0bGUke2l9YH0+e3R9PC9wPik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdGl0bGVFbGVtZW50O1xyXG4gICAgICAgIGlmIChzaG93U3Bpbm5lcikge1xyXG4gICAgICAgICAgICB0aXRsZUVsZW1lbnQgPSA8aDMgY2xhc3NOYW1lPVwibXhfUm9vbVByZXZpZXdCYXJfc3Bpbm5lclRpdGxlXCI+PFNwaW5uZXIgLz57IHRpdGxlIH08L2gzPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aXRsZUVsZW1lbnQgPSA8aDM+eyB0aXRsZSB9PC9oMz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcHJpbWFyeUJ1dHRvbjtcclxuICAgICAgICBpZiAocHJpbWFyeUFjdGlvbkhhbmRsZXIpIHtcclxuICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9XCJwcmltYXJ5XCIgb25DbGljaz17cHJpbWFyeUFjdGlvbkhhbmRsZXJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgcHJpbWFyeUFjdGlvbkxhYmVsIH1cclxuICAgICAgICAgICAgICAgIDwvQWNjZXNzaWJsZUJ1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzZWNvbmRhcnlCdXR0b247XHJcbiAgICAgICAgaWYgKHNlY29uZGFyeUFjdGlvbkhhbmRsZXIpIHtcclxuICAgICAgICAgICAgc2Vjb25kYXJ5QnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD1cInNlY29uZGFyeVwiIG9uQ2xpY2s9e3NlY29uZGFyeUFjdGlvbkhhbmRsZXJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgc2Vjb25kYXJ5QWN0aW9uTGFiZWwgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoXCJteF9Sb29tUHJldmlld0JhclwiLCBcImRhcmstcGFuZWxcIiwgYG14X1Jvb21QcmV2aWV3QmFyXyR7bWVzc2FnZUNhc2V9YCwge1xyXG4gICAgICAgICAgICBcIm14X1Jvb21QcmV2aWV3QmFyX3BhbmVsXCI6IHRoaXMucHJvcHMuY2FuUHJldmlldyxcclxuICAgICAgICAgICAgXCJteF9Sb29tUHJldmlld0Jhcl9kaWFsb2dcIjogIXRoaXMucHJvcHMuY2FuUHJldmlldyxcclxuICAgICAgICAgICAgXCJteF9Sb29tUHJldmlld0Jhcl9kYXJrXCI6IGRhcmtTdHlsZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXN9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tUHJldmlld0Jhcl9tZXNzYWdlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aXRsZUVsZW1lbnQgfVxyXG4gICAgICAgICAgICAgICAgICAgIHsgc3ViVGl0bGVFbGVtZW50cyB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfUm9vbVByZXZpZXdCYXJfYWN0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgc2Vjb25kYXJ5QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgICAgICB7IGV4dHJhQ29tcG9uZW50cyB9XHJcbiAgICAgICAgICAgICAgICAgICAgeyBwcmltYXJ5QnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tUHJldmlld0Jhcl9mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGZvb3RlciB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbn0pO1xyXG4iXX0=