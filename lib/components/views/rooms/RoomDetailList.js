"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var sdk = _interopRequireWildcard(require("../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../../../languageHandler");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var _RoomDetailRow = require("./RoomDetailRow");

/*
Copyright 2017 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'RoomDetailList',
  propTypes: {
    rooms: _propTypes.default.arrayOf(_RoomDetailRow.roomShape),
    className: _propTypes.default.string
  },
  getRows: function () {
    if (!this.props.rooms) return [];
    const RoomDetailRow = sdk.getComponent('rooms.RoomDetailRow');
    return this.props.rooms.map((room, index) => {
      return _react.default.createElement(RoomDetailRow, {
        key: index,
        room: room,
        onClick: this.onDetailsClick
      });
    });
  },
  onDetailsClick: function (ev, room) {
    _dispatcher.default.dispatch({
      action: 'view_room',
      room_id: room.roomId,
      room_alias: room.canonicalAlias || (room.aliases || [])[0]
    });
  },

  render() {
    const rows = this.getRows();
    let rooms;

    if (rows.length === 0) {
      rooms = _react.default.createElement("i", null, (0, _languageHandler._t)('No rooms to show'));
    } else {
      rooms = _react.default.createElement("table", {
        className: "mx_RoomDirectory_table"
      }, _react.default.createElement("tbody", null, this.getRows()));
    }

    return _react.default.createElement("div", {
      className: (0, _classnames.default)("mx_RoomDetailList", this.props.className)
    }, rooms);
  }

});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3Jvb21zL1Jvb21EZXRhaWxMaXN0LmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwicm9vbXMiLCJQcm9wVHlwZXMiLCJhcnJheU9mIiwicm9vbVNoYXBlIiwiY2xhc3NOYW1lIiwic3RyaW5nIiwiZ2V0Um93cyIsInByb3BzIiwiUm9vbURldGFpbFJvdyIsInNkayIsImdldENvbXBvbmVudCIsIm1hcCIsInJvb20iLCJpbmRleCIsIm9uRGV0YWlsc0NsaWNrIiwiZXYiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJvb21faWQiLCJyb29tSWQiLCJyb29tX2FsaWFzIiwiY2Fub25pY2FsQWxpYXMiLCJhbGlhc2VzIiwicmVuZGVyIiwicm93cyIsImxlbmd0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBeEJBOzs7Ozs7Ozs7Ozs7Ozs7ZUEwQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsZ0JBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxLQUFLLEVBQUVDLG1CQUFVQyxPQUFWLENBQWtCQyx3QkFBbEIsQ0FEQTtBQUVQQyxJQUFBQSxTQUFTLEVBQUVILG1CQUFVSTtBQUZkLEdBSGlCO0FBUTVCQyxFQUFBQSxPQUFPLEVBQUUsWUFBVztBQUNoQixRQUFJLENBQUMsS0FBS0MsS0FBTCxDQUFXUCxLQUFoQixFQUF1QixPQUFPLEVBQVA7QUFFdkIsVUFBTVEsYUFBYSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXRCO0FBQ0EsV0FBTyxLQUFLSCxLQUFMLENBQVdQLEtBQVgsQ0FBaUJXLEdBQWpCLENBQXFCLENBQUNDLElBQUQsRUFBT0MsS0FBUCxLQUFpQjtBQUN6QyxhQUFPLDZCQUFDLGFBQUQ7QUFBZSxRQUFBLEdBQUcsRUFBRUEsS0FBcEI7QUFBMkIsUUFBQSxJQUFJLEVBQUVELElBQWpDO0FBQXVDLFFBQUEsT0FBTyxFQUFFLEtBQUtFO0FBQXJELFFBQVA7QUFDSCxLQUZNLENBQVA7QUFHSCxHQWYyQjtBQWlCNUJBLEVBQUFBLGNBQWMsRUFBRSxVQUFTQyxFQUFULEVBQWFILElBQWIsRUFBbUI7QUFDL0JJLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVEMsTUFBQUEsT0FBTyxFQUFFUCxJQUFJLENBQUNRLE1BRkw7QUFHVEMsTUFBQUEsVUFBVSxFQUFFVCxJQUFJLENBQUNVLGNBQUwsSUFBdUIsQ0FBQ1YsSUFBSSxDQUFDVyxPQUFMLElBQWdCLEVBQWpCLEVBQXFCLENBQXJCO0FBSDFCLEtBQWI7QUFLSCxHQXZCMkI7O0FBeUI1QkMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsSUFBSSxHQUFHLEtBQUtuQixPQUFMLEVBQWI7QUFDQSxRQUFJTixLQUFKOztBQUNBLFFBQUl5QixJQUFJLENBQUNDLE1BQUwsS0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkIxQixNQUFBQSxLQUFLLEdBQUcsd0NBQUsseUJBQUcsa0JBQUgsQ0FBTCxDQUFSO0FBQ0gsS0FGRCxNQUVPO0FBQ0hBLE1BQUFBLEtBQUssR0FBRztBQUFPLFFBQUEsU0FBUyxFQUFDO0FBQWpCLFNBQ0osNENBQ00sS0FBS00sT0FBTCxFQUROLENBREksQ0FBUjtBQUtIOztBQUNELFdBQU87QUFBSyxNQUFBLFNBQVMsRUFBRSx5QkFBVyxtQkFBWCxFQUFnQyxLQUFLQyxLQUFMLENBQVdILFNBQTNDO0FBQWhCLE9BQ0RKLEtBREMsQ0FBUDtBQUdIOztBQXhDMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IGNyZWF0ZVJlYWN0Q2xhc3MgZnJvbSAnY3JlYXRlLXJlYWN0LWNsYXNzJztcclxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XHJcblxyXG5pbXBvcnQge3Jvb21TaGFwZX0gZnJvbSAnLi9Sb29tRGV0YWlsUm93JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tRGV0YWlsTGlzdCcsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgcm9vbXM6IFByb3BUeXBlcy5hcnJheU9mKHJvb21TaGFwZSksXHJcbiAgICAgICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRSb3dzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXRoaXMucHJvcHMucm9vbXMpIHJldHVybiBbXTtcclxuXHJcbiAgICAgICAgY29uc3QgUm9vbURldGFpbFJvdyA9IHNkay5nZXRDb21wb25lbnQoJ3Jvb21zLlJvb21EZXRhaWxSb3cnKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5yb29tcy5tYXAoKHJvb20sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiA8Um9vbURldGFpbFJvdyBrZXk9e2luZGV4fSByb29tPXtyb29tfSBvbkNsaWNrPXt0aGlzLm9uRGV0YWlsc0NsaWNrfSAvPjtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25EZXRhaWxzQ2xpY2s6IGZ1bmN0aW9uKGV2LCByb29tKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogcm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgIHJvb21fYWxpYXM6IHJvb20uY2Fub25pY2FsQWxpYXMgfHwgKHJvb20uYWxpYXNlcyB8fCBbXSlbMF0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCByb3dzID0gdGhpcy5nZXRSb3dzKCk7XHJcbiAgICAgICAgbGV0IHJvb21zO1xyXG4gICAgICAgIGlmIChyb3dzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICByb29tcyA9IDxpPnsgX3QoJ05vIHJvb21zIHRvIHNob3cnKSB9PC9pPjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByb29tcyA9IDx0YWJsZSBjbGFzc05hbWU9XCJteF9Sb29tRGlyZWN0b3J5X3RhYmxlXCI+XHJcbiAgICAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLmdldFJvd3MoKSB9XHJcbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKFwibXhfUm9vbURldGFpbExpc3RcIiwgdGhpcy5wcm9wcy5jbGFzc05hbWUpfT5cclxuICAgICAgICAgICAgeyByb29tcyB9XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==