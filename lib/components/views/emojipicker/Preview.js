"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

/*
Copyright 2019 Tulir Asokan <tulir@maunium.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class Preview extends _react.default.PureComponent {
  render() {
    const {
      unicode = "",
      annotation = "",
      shortcodes: [shortcode = ""]
    } = this.props.emoji || {};
    return _react.default.createElement("div", {
      className: "mx_EmojiPicker_footer mx_EmojiPicker_preview"
    }, _react.default.createElement("div", {
      className: "mx_EmojiPicker_preview_emoji"
    }, unicode), _react.default.createElement("div", {
      className: "mx_EmojiPicker_preview_text"
    }, _react.default.createElement("div", {
      className: "mx_EmojiPicker_name mx_EmojiPicker_preview_name"
    }, annotation), _react.default.createElement("div", {
      className: "mx_EmojiPicker_shortcode"
    }, shortcode)));
  }

}

(0, _defineProperty2.default)(Preview, "propTypes", {
  emoji: _propTypes.default.object
});
var _default = Preview;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2Vtb2ppcGlja2VyL1ByZXZpZXcuanMiXSwibmFtZXMiOlsiUHJldmlldyIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsInJlbmRlciIsInVuaWNvZGUiLCJhbm5vdGF0aW9uIiwic2hvcnRjb2RlcyIsInNob3J0Y29kZSIsInByb3BzIiwiZW1vamkiLCJQcm9wVHlwZXMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQWpCQTs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE1BQU1BLE9BQU4sU0FBc0JDLGVBQU1DLGFBQTVCLENBQTBDO0FBS3RDQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNO0FBQ0ZDLE1BQUFBLE9BQU8sR0FBRyxFQURSO0FBRUZDLE1BQUFBLFVBQVUsR0FBRyxFQUZYO0FBR0ZDLE1BQUFBLFVBQVUsRUFBRSxDQUFDQyxTQUFTLEdBQUcsRUFBYjtBQUhWLFFBSUYsS0FBS0MsS0FBTCxDQUFXQyxLQUFYLElBQW9CLEVBSnhCO0FBS0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS0wsT0FETCxDQURKLEVBSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0tDLFVBREwsQ0FESixFQUlJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLRSxTQURMLENBSkosQ0FKSixDQURKO0FBZUg7O0FBMUJxQzs7OEJBQXBDUCxPLGVBQ2lCO0FBQ2ZTLEVBQUFBLEtBQUssRUFBRUMsbUJBQVVDO0FBREYsQztlQTRCUlgsTyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFR1bGlyIEFzb2thbiA8dHVsaXJAbWF1bml1bS5uZXQ+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuXHJcbmNsYXNzIFByZXZpZXcgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgZW1vamk6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCB7XHJcbiAgICAgICAgICAgIHVuaWNvZGUgPSBcIlwiLFxyXG4gICAgICAgICAgICBhbm5vdGF0aW9uID0gXCJcIixcclxuICAgICAgICAgICAgc2hvcnRjb2RlczogW3Nob3J0Y29kZSA9IFwiXCJdLFxyXG4gICAgICAgIH0gPSB0aGlzLnByb3BzLmVtb2ppIHx8IHt9O1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRW1vamlQaWNrZXJfZm9vdGVyIG14X0Vtb2ppUGlja2VyX3ByZXZpZXdcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRW1vamlQaWNrZXJfcHJldmlld19lbW9qaVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt1bmljb2RlfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0Vtb2ppUGlja2VyX3ByZXZpZXdfdGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRW1vamlQaWNrZXJfbmFtZSBteF9FbW9qaVBpY2tlcl9wcmV2aWV3X25hbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge2Fubm90YXRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9FbW9qaVBpY2tlcl9zaG9ydGNvZGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3Nob3J0Y29kZX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQcmV2aWV3O1xyXG4iXX0=