"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _EmojiPicker = require("./EmojiPicker");

var sdk = _interopRequireWildcard(require("../../../index"));

/*
Copyright 2019 Tulir Asokan <tulir@maunium.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const OVERFLOW_ROWS = 3;

class Category extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "_renderEmojiRow", rowIndex => {
      const {
        onClick,
        onMouseEnter,
        onMouseLeave,
        selectedEmojis,
        emojis
      } = this.props;
      const emojisForRow = emojis.slice(rowIndex * 8, (rowIndex + 1) * 8);
      const Emoji = sdk.getComponent("emojipicker.Emoji");
      return _react.default.createElement("div", {
        key: rowIndex
      }, emojisForRow.map(emoji => _react.default.createElement(Emoji, {
        key: emoji.hexcode,
        emoji: emoji,
        selectedEmojis: selectedEmojis,
        onClick: onClick,
        onMouseEnter: onMouseEnter,
        onMouseLeave: onMouseLeave
      })));
    });
  }

  render() {
    const {
      emojis,
      name,
      heightBefore,
      viewportHeight,
      scrollTop
    } = this.props;

    if (!emojis || emojis.length === 0) {
      return null;
    }

    const rows = new Array(Math.ceil(emojis.length / _EmojiPicker.EMOJIS_PER_ROW));

    for (let counter = 0; counter < rows.length; ++counter) {
      rows[counter] = counter;
    }

    const LazyRenderList = sdk.getComponent('elements.LazyRenderList');
    const viewportTop = scrollTop;
    const viewportBottom = viewportTop + viewportHeight;
    const listTop = heightBefore + _EmojiPicker.CATEGORY_HEADER_HEIGHT;
    const listBottom = listTop + rows.length * _EmojiPicker.EMOJI_HEIGHT;
    const top = Math.max(viewportTop, listTop);
    const bottom = Math.min(viewportBottom, listBottom); // the viewport height and scrollTop passed to the LazyRenderList
    // is capped at the intersection with the real viewport, so lists
    // out of view are passed height 0, so they won't render any items.

    const localHeight = Math.max(0, bottom - top);
    const localScrollTop = Math.max(0, scrollTop - listTop);
    return _react.default.createElement("section", {
      className: "mx_EmojiPicker_category",
      "data-category-id": this.props.id
    }, _react.default.createElement("h2", {
      className: "mx_EmojiPicker_category_label"
    }, name), _react.default.createElement(LazyRenderList, {
      element: "ul",
      className: "mx_EmojiPicker_list",
      itemHeight: _EmojiPicker.EMOJI_HEIGHT,
      items: rows,
      scrollTop: localScrollTop,
      height: localHeight,
      overflowItems: OVERFLOW_ROWS,
      overflowMargin: 0,
      renderItem: this._renderEmojiRow
    }));
  }

}

(0, _defineProperty2.default)(Category, "propTypes", {
  emojis: _propTypes.default.arrayOf(_propTypes.default.object).isRequired,
  name: _propTypes.default.string.isRequired,
  id: _propTypes.default.string.isRequired,
  onMouseEnter: _propTypes.default.func.isRequired,
  onMouseLeave: _propTypes.default.func.isRequired,
  onClick: _propTypes.default.func.isRequired,
  selectedEmojis: _propTypes.default.instanceOf(Set)
});
var _default = Category;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2Vtb2ppcGlja2VyL0NhdGVnb3J5LmpzIl0sIm5hbWVzIjpbIk9WRVJGTE9XX1JPV1MiLCJDYXRlZ29yeSIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsInJvd0luZGV4Iiwib25DbGljayIsIm9uTW91c2VFbnRlciIsIm9uTW91c2VMZWF2ZSIsInNlbGVjdGVkRW1vamlzIiwiZW1vamlzIiwicHJvcHMiLCJlbW9qaXNGb3JSb3ciLCJzbGljZSIsIkVtb2ppIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwibWFwIiwiZW1vamkiLCJoZXhjb2RlIiwicmVuZGVyIiwibmFtZSIsImhlaWdodEJlZm9yZSIsInZpZXdwb3J0SGVpZ2h0Iiwic2Nyb2xsVG9wIiwibGVuZ3RoIiwicm93cyIsIkFycmF5IiwiTWF0aCIsImNlaWwiLCJFTU9KSVNfUEVSX1JPVyIsImNvdW50ZXIiLCJMYXp5UmVuZGVyTGlzdCIsInZpZXdwb3J0VG9wIiwidmlld3BvcnRCb3R0b20iLCJsaXN0VG9wIiwiQ0FURUdPUllfSEVBREVSX0hFSUdIVCIsImxpc3RCb3R0b20iLCJFTU9KSV9IRUlHSFQiLCJ0b3AiLCJtYXgiLCJib3R0b20iLCJtaW4iLCJsb2NhbEhlaWdodCIsImxvY2FsU2Nyb2xsVG9wIiwiaWQiLCJfcmVuZGVyRW1vamlSb3ciLCJQcm9wVHlwZXMiLCJhcnJheU9mIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsInN0cmluZyIsImZ1bmMiLCJpbnN0YW5jZU9mIiwiU2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQW5CQTs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBLE1BQU1BLGFBQWEsR0FBRyxDQUF0Qjs7QUFFQSxNQUFNQyxRQUFOLFNBQXVCQyxlQUFNQyxhQUE3QixDQUEyQztBQUFBO0FBQUE7QUFBQSwyREFXcEJDLFFBQUQsSUFBYztBQUM1QixZQUFNO0FBQUVDLFFBQUFBLE9BQUY7QUFBV0MsUUFBQUEsWUFBWDtBQUF5QkMsUUFBQUEsWUFBekI7QUFBdUNDLFFBQUFBLGNBQXZDO0FBQXVEQyxRQUFBQTtBQUF2RCxVQUFrRSxLQUFLQyxLQUE3RTtBQUNBLFlBQU1DLFlBQVksR0FBR0YsTUFBTSxDQUFDRyxLQUFQLENBQWFSLFFBQVEsR0FBRyxDQUF4QixFQUEyQixDQUFDQSxRQUFRLEdBQUcsQ0FBWixJQUFpQixDQUE1QyxDQUFyQjtBQUNBLFlBQU1TLEtBQUssR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG1CQUFqQixDQUFkO0FBQ0EsYUFBUTtBQUFLLFFBQUEsR0FBRyxFQUFFWDtBQUFWLFNBQ0pPLFlBQVksQ0FBQ0ssR0FBYixDQUFpQkMsS0FBSyxJQUNsQiw2QkFBQyxLQUFEO0FBQU8sUUFBQSxHQUFHLEVBQUVBLEtBQUssQ0FBQ0MsT0FBbEI7QUFBMkIsUUFBQSxLQUFLLEVBQUVELEtBQWxDO0FBQXlDLFFBQUEsY0FBYyxFQUFFVCxjQUF6RDtBQUNJLFFBQUEsT0FBTyxFQUFFSCxPQURiO0FBQ3NCLFFBQUEsWUFBWSxFQUFFQyxZQURwQztBQUNrRCxRQUFBLFlBQVksRUFBRUM7QUFEaEUsUUFESixDQURJLENBQVI7QUFLSCxLQXBCc0M7QUFBQTs7QUFzQnZDWSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNO0FBQUVWLE1BQUFBLE1BQUY7QUFBVVcsTUFBQUEsSUFBVjtBQUFnQkMsTUFBQUEsWUFBaEI7QUFBOEJDLE1BQUFBLGNBQTlCO0FBQThDQyxNQUFBQTtBQUE5QyxRQUE0RCxLQUFLYixLQUF2RTs7QUFDQSxRQUFJLENBQUNELE1BQUQsSUFBV0EsTUFBTSxDQUFDZSxNQUFQLEtBQWtCLENBQWpDLEVBQW9DO0FBQ2hDLGFBQU8sSUFBUDtBQUNIOztBQUNELFVBQU1DLElBQUksR0FBRyxJQUFJQyxLQUFKLENBQVVDLElBQUksQ0FBQ0MsSUFBTCxDQUFVbkIsTUFBTSxDQUFDZSxNQUFQLEdBQWdCSywyQkFBMUIsQ0FBVixDQUFiOztBQUNBLFNBQUssSUFBSUMsT0FBTyxHQUFHLENBQW5CLEVBQXNCQSxPQUFPLEdBQUdMLElBQUksQ0FBQ0QsTUFBckMsRUFBNkMsRUFBRU0sT0FBL0MsRUFBd0Q7QUFDcERMLE1BQUFBLElBQUksQ0FBQ0ssT0FBRCxDQUFKLEdBQWdCQSxPQUFoQjtBQUNIOztBQUNELFVBQU1DLGNBQWMsR0FBR2pCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBdkI7QUFFQSxVQUFNaUIsV0FBVyxHQUFHVCxTQUFwQjtBQUNBLFVBQU1VLGNBQWMsR0FBR0QsV0FBVyxHQUFHVixjQUFyQztBQUNBLFVBQU1ZLE9BQU8sR0FBR2IsWUFBWSxHQUFHYyxtQ0FBL0I7QUFDQSxVQUFNQyxVQUFVLEdBQUdGLE9BQU8sR0FBSVQsSUFBSSxDQUFDRCxNQUFMLEdBQWNhLHlCQUE1QztBQUNBLFVBQU1DLEdBQUcsR0FBR1gsSUFBSSxDQUFDWSxHQUFMLENBQVNQLFdBQVQsRUFBc0JFLE9BQXRCLENBQVo7QUFDQSxVQUFNTSxNQUFNLEdBQUdiLElBQUksQ0FBQ2MsR0FBTCxDQUFTUixjQUFULEVBQXlCRyxVQUF6QixDQUFmLENBaEJLLENBaUJMO0FBQ0E7QUFDQTs7QUFDQSxVQUFNTSxXQUFXLEdBQUdmLElBQUksQ0FBQ1ksR0FBTCxDQUFTLENBQVQsRUFBWUMsTUFBTSxHQUFHRixHQUFyQixDQUFwQjtBQUNBLFVBQU1LLGNBQWMsR0FBR2hCLElBQUksQ0FBQ1ksR0FBTCxDQUFTLENBQVQsRUFBWWhCLFNBQVMsR0FBR1csT0FBeEIsQ0FBdkI7QUFFQSxXQUNJO0FBQVMsTUFBQSxTQUFTLEVBQUMseUJBQW5CO0FBQTZDLDBCQUFrQixLQUFLeEIsS0FBTCxDQUFXa0M7QUFBMUUsT0FDSTtBQUFJLE1BQUEsU0FBUyxFQUFDO0FBQWQsT0FDS3hCLElBREwsQ0FESixFQUlJLDZCQUFDLGNBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBQyxJQURaO0FBQ2lCLE1BQUEsU0FBUyxFQUFDLHFCQUQzQjtBQUVJLE1BQUEsVUFBVSxFQUFFaUIseUJBRmhCO0FBRThCLE1BQUEsS0FBSyxFQUFFWixJQUZyQztBQUdJLE1BQUEsU0FBUyxFQUFFa0IsY0FIZjtBQUlJLE1BQUEsTUFBTSxFQUFFRCxXQUpaO0FBS0ksTUFBQSxhQUFhLEVBQUUxQyxhQUxuQjtBQU1JLE1BQUEsY0FBYyxFQUFFLENBTnBCO0FBT0ksTUFBQSxVQUFVLEVBQUUsS0FBSzZDO0FBUHJCLE1BSkosQ0FESjtBQWdCSDs7QUE3RHNDOzs4QkFBckM1QyxRLGVBQ2lCO0FBQ2ZRLEVBQUFBLE1BQU0sRUFBRXFDLG1CQUFVQyxPQUFWLENBQWtCRCxtQkFBVUUsTUFBNUIsRUFBb0NDLFVBRDdCO0FBRWY3QixFQUFBQSxJQUFJLEVBQUUwQixtQkFBVUksTUFBVixDQUFpQkQsVUFGUjtBQUdmTCxFQUFBQSxFQUFFLEVBQUVFLG1CQUFVSSxNQUFWLENBQWlCRCxVQUhOO0FBSWYzQyxFQUFBQSxZQUFZLEVBQUV3QyxtQkFBVUssSUFBVixDQUFlRixVQUpkO0FBS2YxQyxFQUFBQSxZQUFZLEVBQUV1QyxtQkFBVUssSUFBVixDQUFlRixVQUxkO0FBTWY1QyxFQUFBQSxPQUFPLEVBQUV5QyxtQkFBVUssSUFBVixDQUFlRixVQU5UO0FBT2Z6QyxFQUFBQSxjQUFjLEVBQUVzQyxtQkFBVU0sVUFBVixDQUFxQkMsR0FBckI7QUFQRCxDO2VBK0RScEQsUSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFR1bGlyIEFzb2thbiA8dHVsaXJAbWF1bml1bS5uZXQ+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgQ0FURUdPUllfSEVBREVSX0hFSUdIVCwgRU1PSklfSEVJR0hULCBFTU9KSVNfUEVSX1JPVyB9IGZyb20gXCIuL0Vtb2ppUGlja2VyXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcblxyXG5jb25zdCBPVkVSRkxPV19ST1dTID0gMztcclxuXHJcbmNsYXNzIENhdGVnb3J5IGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIGVtb2ppczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9iamVjdCkuaXNSZXF1aXJlZCxcclxuICAgICAgICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBvbk1vdXNlRW50ZXI6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgb25Nb3VzZUxlYXZlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgc2VsZWN0ZWRFbW9qaXM6IFByb3BUeXBlcy5pbnN0YW5jZU9mKFNldCksXHJcbiAgICB9O1xyXG5cclxuICAgIF9yZW5kZXJFbW9qaVJvdyA9IChyb3dJbmRleCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHsgb25DbGljaywgb25Nb3VzZUVudGVyLCBvbk1vdXNlTGVhdmUsIHNlbGVjdGVkRW1vamlzLCBlbW9qaXMgfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgY29uc3QgZW1vamlzRm9yUm93ID0gZW1vamlzLnNsaWNlKHJvd0luZGV4ICogOCwgKHJvd0luZGV4ICsgMSkgKiA4KTtcclxuICAgICAgICBjb25zdCBFbW9qaSA9IHNkay5nZXRDb21wb25lbnQoXCJlbW9qaXBpY2tlci5FbW9qaVwiKTtcclxuICAgICAgICByZXR1cm4gKDxkaXYga2V5PXtyb3dJbmRleH0+e1xyXG4gICAgICAgICAgICBlbW9qaXNGb3JSb3cubWFwKGVtb2ppID0+XHJcbiAgICAgICAgICAgICAgICA8RW1vamkga2V5PXtlbW9qaS5oZXhjb2RlfSBlbW9qaT17ZW1vaml9IHNlbGVjdGVkRW1vamlzPXtzZWxlY3RlZEVtb2ppc31cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtvbkNsaWNrfSBvbk1vdXNlRW50ZXI9e29uTW91c2VFbnRlcn0gb25Nb3VzZUxlYXZlPXtvbk1vdXNlTGVhdmV9IC8+KVxyXG4gICAgICAgIH08L2Rpdj4pO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgeyBlbW9qaXMsIG5hbWUsIGhlaWdodEJlZm9yZSwgdmlld3BvcnRIZWlnaHQsIHNjcm9sbFRvcCB9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAoIWVtb2ppcyB8fCBlbW9qaXMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCByb3dzID0gbmV3IEFycmF5KE1hdGguY2VpbChlbW9qaXMubGVuZ3RoIC8gRU1PSklTX1BFUl9ST1cpKTtcclxuICAgICAgICBmb3IgKGxldCBjb3VudGVyID0gMDsgY291bnRlciA8IHJvd3MubGVuZ3RoOyArK2NvdW50ZXIpIHtcclxuICAgICAgICAgICAgcm93c1tjb3VudGVyXSA9IGNvdW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IExhenlSZW5kZXJMaXN0ID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuTGF6eVJlbmRlckxpc3QnKTtcclxuXHJcbiAgICAgICAgY29uc3Qgdmlld3BvcnRUb3AgPSBzY3JvbGxUb3A7XHJcbiAgICAgICAgY29uc3Qgdmlld3BvcnRCb3R0b20gPSB2aWV3cG9ydFRvcCArIHZpZXdwb3J0SGVpZ2h0O1xyXG4gICAgICAgIGNvbnN0IGxpc3RUb3AgPSBoZWlnaHRCZWZvcmUgKyBDQVRFR09SWV9IRUFERVJfSEVJR0hUO1xyXG4gICAgICAgIGNvbnN0IGxpc3RCb3R0b20gPSBsaXN0VG9wICsgKHJvd3MubGVuZ3RoICogRU1PSklfSEVJR0hUKTtcclxuICAgICAgICBjb25zdCB0b3AgPSBNYXRoLm1heCh2aWV3cG9ydFRvcCwgbGlzdFRvcCk7XHJcbiAgICAgICAgY29uc3QgYm90dG9tID0gTWF0aC5taW4odmlld3BvcnRCb3R0b20sIGxpc3RCb3R0b20pO1xyXG4gICAgICAgIC8vIHRoZSB2aWV3cG9ydCBoZWlnaHQgYW5kIHNjcm9sbFRvcCBwYXNzZWQgdG8gdGhlIExhenlSZW5kZXJMaXN0XHJcbiAgICAgICAgLy8gaXMgY2FwcGVkIGF0IHRoZSBpbnRlcnNlY3Rpb24gd2l0aCB0aGUgcmVhbCB2aWV3cG9ydCwgc28gbGlzdHNcclxuICAgICAgICAvLyBvdXQgb2YgdmlldyBhcmUgcGFzc2VkIGhlaWdodCAwLCBzbyB0aGV5IHdvbid0IHJlbmRlciBhbnkgaXRlbXMuXHJcbiAgICAgICAgY29uc3QgbG9jYWxIZWlnaHQgPSBNYXRoLm1heCgwLCBib3R0b20gLSB0b3ApO1xyXG4gICAgICAgIGNvbnN0IGxvY2FsU2Nyb2xsVG9wID0gTWF0aC5tYXgoMCwgc2Nyb2xsVG9wIC0gbGlzdFRvcCk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cIm14X0Vtb2ppUGlja2VyX2NhdGVnb3J5XCIgZGF0YS1jYXRlZ29yeS1pZD17dGhpcy5wcm9wcy5pZH0+XHJcbiAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwibXhfRW1vamlQaWNrZXJfY2F0ZWdvcnlfbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICB7bmFtZX1cclxuICAgICAgICAgICAgICAgIDwvaDI+XHJcbiAgICAgICAgICAgICAgICA8TGF6eVJlbmRlckxpc3RcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50PVwidWxcIiBjbGFzc05hbWU9XCJteF9FbW9qaVBpY2tlcl9saXN0XCJcclxuICAgICAgICAgICAgICAgICAgICBpdGVtSGVpZ2h0PXtFTU9KSV9IRUlHSFR9IGl0ZW1zPXtyb3dzfVxyXG4gICAgICAgICAgICAgICAgICAgIHNjcm9sbFRvcD17bG9jYWxTY3JvbGxUb3B9XHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PXtsb2NhbEhlaWdodH1cclxuICAgICAgICAgICAgICAgICAgICBvdmVyZmxvd0l0ZW1zPXtPVkVSRkxPV19ST1dTfVxyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93TWFyZ2luPXswfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09e3RoaXMuX3JlbmRlckVtb2ppUm93fT5cclxuICAgICAgICAgICAgICAgIDwvTGF6eVJlbmRlckxpc3Q+XHJcbiAgICAgICAgICAgIDwvc2VjdGlvbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDYXRlZ29yeTtcclxuIl19