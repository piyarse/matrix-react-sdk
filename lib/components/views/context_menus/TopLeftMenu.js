"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _languageHandler = require("../../../languageHandler");

var _LogoutDialog = _interopRequireDefault(require("../dialogs/LogoutDialog"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _SdkConfig = _interopRequireDefault(require("../../../SdkConfig"));

var _HostingLink = require("../../../utils/HostingLink");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _ContextMenu = require("../../structures/ContextMenu");

var sdk = _interopRequireWildcard(require("../../../index"));

var _pages = require("../../../utils/pages");

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class TopLeftMenu extends _react.default.Component {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "openHelp", () => {
      this.closeMenu();
      const RedesignFeedbackDialog = sdk.getComponent("views.dialogs.RedesignFeedbackDialog");

      _Modal.default.createTrackedDialog('Report bugs & give feedback', '', RedesignFeedbackDialog);
    });
    this.viewHomePage = this.viewHomePage.bind(this);
    this.openSettings = this.openSettings.bind(this);
    this.signIn = this.signIn.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  hasHomePage() {
    return !!(0, _pages.getHomePageUrl)(_SdkConfig.default.get());
  }

  render() {
    const isGuest = _MatrixClientPeg.MatrixClientPeg.get().isGuest();

    const hostingSignupLink = (0, _HostingLink.getHostingLink)('user-context-menu');
    let hostingSignup = null;

    if (hostingSignupLink) {
      hostingSignup = _react.default.createElement("div", {
        className: "mx_TopLeftMenu_upgradeLink"
      }, (0, _languageHandler._t)("<a>Upgrade</a> to your own domain", {}, {
        a: sub => _react.default.createElement("a", {
          href: hostingSignupLink,
          target: "_blank",
          rel: "noreferrer noopener",
          tabIndex: -1
        }, sub)
      }), _react.default.createElement("a", {
        href: hostingSignupLink,
        target: "_blank",
        rel: "noreferrer noopener",
        role: "presentation",
        "aria-hidden": true,
        tabIndex: -1
      }, _react.default.createElement("img", {
        src: require("../../../../res/img/external-link.svg"),
        width: "11",
        height: "10",
        alt: ""
      })));
    }

    let homePageItem = null;

    if (this.hasHomePage()) {
      homePageItem = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_TopLeftMenu_icon_home",
        onClick: this.viewHomePage
      }, (0, _languageHandler._t)("Home"));
    }

    let signInOutItem;

    if (isGuest) {
      signInOutItem = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_TopLeftMenu_icon_signin",
        onClick: this.signIn
      }, (0, _languageHandler._t)("Sign in"));
    } else {
      signInOutItem = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_TopLeftMenu_icon_signout",
        onClick: this.signOut
      }, (0, _languageHandler._t)("Sign out"));
    }

    const helpItem = _react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_TopLeftMenu_icon_help",
      onClick: this.openHelp
    }, (0, _languageHandler._t)("Help"));

    const settingsItem = _react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_TopLeftMenu_icon_settings",
      onClick: this.openSettings
    }, (0, _languageHandler._t)("Settings"));

    return _react.default.createElement("div", {
      className: "mx_TopLeftMenu",
      ref: this.props.containerRef,
      role: "menu"
    }, _react.default.createElement("div", {
      className: "mx_TopLeftMenu_section_noIcon",
      "aria-readonly": true,
      tabIndex: -1
    }, _react.default.createElement("div", null, this.props.displayName), _react.default.createElement("div", {
      className: "mx_TopLeftMenu_greyedText",
      "aria-hidden": true
    }, this.props.userId), hostingSignup), _react.default.createElement("ul", {
      className: "mx_TopLeftMenu_section_withIcon",
      role: "none"
    }, homePageItem, settingsItem, helpItem, signInOutItem));
  }

  viewHomePage() {
    _dispatcher.default.dispatch({
      action: 'view_home_page'
    });

    this.closeMenu();
  }

  openSettings() {
    _dispatcher.default.dispatch({
      action: 'view_user_settings'
    });

    this.closeMenu();
  }

  signIn() {
    _dispatcher.default.dispatch({
      action: 'start_login'
    });

    this.closeMenu();
  }

  signOut() {
    _Modal.default.createTrackedDialog('Logout E2E Export', '', _LogoutDialog.default);

    this.closeMenu();
  }

  closeMenu() {
    if (this.props.onFinished) this.props.onFinished();
  }

}

exports.default = TopLeftMenu;
(0, _defineProperty2.default)(TopLeftMenu, "propTypes", {
  displayName: _propTypes.default.string.isRequired,
  userId: _propTypes.default.string.isRequired,
  onFinished: _propTypes.default.func,
  // Optional function to collect a reference to the container
  // of this component directly.
  containerRef: _propTypes.default.func
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvVG9wTGVmdE1lbnUuanMiXSwibmFtZXMiOlsiVG9wTGVmdE1lbnUiLCJSZWFjdCIsIkNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwiY2xvc2VNZW51IiwiUmVkZXNpZ25GZWVkYmFja0RpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInZpZXdIb21lUGFnZSIsImJpbmQiLCJvcGVuU2V0dGluZ3MiLCJzaWduSW4iLCJzaWduT3V0IiwiaGFzSG9tZVBhZ2UiLCJTZGtDb25maWciLCJnZXQiLCJyZW5kZXIiLCJpc0d1ZXN0IiwiTWF0cml4Q2xpZW50UGVnIiwiaG9zdGluZ1NpZ251cExpbmsiLCJob3N0aW5nU2lnbnVwIiwiYSIsInN1YiIsInJlcXVpcmUiLCJob21lUGFnZUl0ZW0iLCJzaWduSW5PdXRJdGVtIiwiaGVscEl0ZW0iLCJvcGVuSGVscCIsInNldHRpbmdzSXRlbSIsInByb3BzIiwiY29udGFpbmVyUmVmIiwiZGlzcGxheU5hbWUiLCJ1c2VySWQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsIkxvZ291dERpYWxvZyIsIm9uRmluaXNoZWQiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUE4QmUsTUFBTUEsV0FBTixTQUEwQkMsZUFBTUMsU0FBaEMsQ0FBMEM7QUFXckRDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRFUsb0RBbUZILE1BQU07QUFDYixXQUFLQyxTQUFMO0FBQ0EsWUFBTUMsc0JBQXNCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQ0FBakIsQ0FBL0I7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQiw2QkFBMUIsRUFBeUQsRUFBekQsRUFBNkRKLHNCQUE3RDtBQUNILEtBdkZhO0FBRVYsU0FBS0ssWUFBTCxHQUFvQixLQUFLQSxZQUFMLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUFwQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsS0FBS0EsWUFBTCxDQUFrQkQsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEI7QUFDQSxTQUFLRSxNQUFMLEdBQWMsS0FBS0EsTUFBTCxDQUFZRixJQUFaLENBQWlCLElBQWpCLENBQWQ7QUFDQSxTQUFLRyxPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhSCxJQUFiLENBQWtCLElBQWxCLENBQWY7QUFDSDs7QUFFREksRUFBQUEsV0FBVyxHQUFHO0FBQ1YsV0FBTyxDQUFDLENBQUMsMkJBQWVDLG1CQUFVQyxHQUFWLEVBQWYsQ0FBVDtBQUNIOztBQUVEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxPQUFPLEdBQUdDLGlDQUFnQkgsR0FBaEIsR0FBc0JFLE9BQXRCLEVBQWhCOztBQUVBLFVBQU1FLGlCQUFpQixHQUFHLGlDQUFlLG1CQUFmLENBQTFCO0FBQ0EsUUFBSUMsYUFBYSxHQUFHLElBQXBCOztBQUNBLFFBQUlELGlCQUFKLEVBQXVCO0FBQ25CQyxNQUFBQSxhQUFhLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ1gseUJBQ0csbUNBREgsRUFDd0MsRUFEeEMsRUFFRztBQUNJQyxRQUFBQSxDQUFDLEVBQUVDLEdBQUcsSUFDRjtBQUFHLFVBQUEsSUFBSSxFQUFFSCxpQkFBVDtBQUE0QixVQUFBLE1BQU0sRUFBQyxRQUFuQztBQUE0QyxVQUFBLEdBQUcsRUFBQyxxQkFBaEQ7QUFBc0UsVUFBQSxRQUFRLEVBQUUsQ0FBQztBQUFqRixXQUFxRkcsR0FBckY7QUFGUixPQUZILENBRFcsRUFRWjtBQUFHLFFBQUEsSUFBSSxFQUFFSCxpQkFBVDtBQUE0QixRQUFBLE1BQU0sRUFBQyxRQUFuQztBQUE0QyxRQUFBLEdBQUcsRUFBQyxxQkFBaEQ7QUFBc0UsUUFBQSxJQUFJLEVBQUMsY0FBM0U7QUFBMEYsdUJBQWEsSUFBdkc7QUFBNkcsUUFBQSxRQUFRLEVBQUUsQ0FBQztBQUF4SCxTQUNJO0FBQUssUUFBQSxHQUFHLEVBQUVJLE9BQU8sQ0FBQyx1Q0FBRCxDQUFqQjtBQUE0RCxRQUFBLEtBQUssRUFBQyxJQUFsRTtBQUF1RSxRQUFBLE1BQU0sRUFBQyxJQUE5RTtBQUFtRixRQUFBLEdBQUcsRUFBQztBQUF2RixRQURKLENBUlksQ0FBaEI7QUFZSDs7QUFFRCxRQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBQ0EsUUFBSSxLQUFLWCxXQUFMLEVBQUosRUFBd0I7QUFDcEJXLE1BQUFBLFlBQVksR0FDUiw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDBCQUFwQjtBQUErQyxRQUFBLE9BQU8sRUFBRSxLQUFLaEI7QUFBN0QsU0FDSyx5QkFBRyxNQUFILENBREwsQ0FESjtBQUtIOztBQUVELFFBQUlpQixhQUFKOztBQUNBLFFBQUlSLE9BQUosRUFBYTtBQUNUUSxNQUFBQSxhQUFhLEdBQ1QsNkJBQUMscUJBQUQ7QUFBVSxRQUFBLFNBQVMsRUFBQyw0QkFBcEI7QUFBaUQsUUFBQSxPQUFPLEVBQUUsS0FBS2Q7QUFBL0QsU0FDSyx5QkFBRyxTQUFILENBREwsQ0FESjtBQUtILEtBTkQsTUFNTztBQUNIYyxNQUFBQSxhQUFhLEdBQ1QsNkJBQUMscUJBQUQ7QUFBVSxRQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsUUFBQSxPQUFPLEVBQUUsS0FBS2I7QUFBaEUsU0FDSyx5QkFBRyxVQUFILENBREwsQ0FESjtBQUtIOztBQUVELFVBQU1jLFFBQVEsR0FDViw2QkFBQyxxQkFBRDtBQUFVLE1BQUEsU0FBUyxFQUFDLDBCQUFwQjtBQUErQyxNQUFBLE9BQU8sRUFBRSxLQUFLQztBQUE3RCxPQUNLLHlCQUFHLE1BQUgsQ0FETCxDQURKOztBQU1BLFVBQU1DLFlBQVksR0FDZCw2QkFBQyxxQkFBRDtBQUFVLE1BQUEsU0FBUyxFQUFDLDhCQUFwQjtBQUFtRCxNQUFBLE9BQU8sRUFBRSxLQUFLbEI7QUFBakUsT0FDSyx5QkFBRyxVQUFILENBREwsQ0FESjs7QUFNQSxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUMsZ0JBQWY7QUFBZ0MsTUFBQSxHQUFHLEVBQUUsS0FBS21CLEtBQUwsQ0FBV0MsWUFBaEQ7QUFBOEQsTUFBQSxJQUFJLEVBQUM7QUFBbkUsT0FDSDtBQUFLLE1BQUEsU0FBUyxFQUFDLCtCQUFmO0FBQStDLHVCQUFlLElBQTlEO0FBQW9FLE1BQUEsUUFBUSxFQUFFLENBQUM7QUFBL0UsT0FDSSwwQ0FBTSxLQUFLRCxLQUFMLENBQVdFLFdBQWpCLENBREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDLDJCQUFmO0FBQTJDLHFCQUFhO0FBQXhELE9BQStELEtBQUtGLEtBQUwsQ0FBV0csTUFBMUUsQ0FGSixFQUdLWixhQUhMLENBREcsRUFNSDtBQUFJLE1BQUEsU0FBUyxFQUFDLGlDQUFkO0FBQWdELE1BQUEsSUFBSSxFQUFDO0FBQXJELE9BQ0tJLFlBREwsRUFFS0ksWUFGTCxFQUdLRixRQUhMLEVBSUtELGFBSkwsQ0FORyxDQUFQO0FBYUg7O0FBUURqQixFQUFBQSxZQUFZLEdBQUc7QUFDWHlCLHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjs7QUFDQSxTQUFLakMsU0FBTDtBQUNIOztBQUVEUSxFQUFBQSxZQUFZLEdBQUc7QUFDWHVCLHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjs7QUFDQSxTQUFLakMsU0FBTDtBQUNIOztBQUVEUyxFQUFBQSxNQUFNLEdBQUc7QUFDTHNCLHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYjs7QUFDQSxTQUFLakMsU0FBTDtBQUNIOztBQUVEVSxFQUFBQSxPQUFPLEdBQUc7QUFDTk4sbUJBQU1DLG1CQUFOLENBQTBCLG1CQUExQixFQUErQyxFQUEvQyxFQUFtRDZCLHFCQUFuRDs7QUFDQSxTQUFLbEMsU0FBTDtBQUNIOztBQUVEQSxFQUFBQSxTQUFTLEdBQUc7QUFDUixRQUFJLEtBQUsyQixLQUFMLENBQVdRLFVBQWYsRUFBMkIsS0FBS1IsS0FBTCxDQUFXUSxVQUFYO0FBQzlCOztBQTFIb0Q7Ozs4QkFBcEN2QyxXLGVBQ0U7QUFDZmlDLEVBQUFBLFdBQVcsRUFBRU8sbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRGY7QUFFZlIsRUFBQUEsTUFBTSxFQUFFTSxtQkFBVUMsTUFBVixDQUFpQkMsVUFGVjtBQUdmSCxFQUFBQSxVQUFVLEVBQUVDLG1CQUFVRyxJQUhQO0FBS2Y7QUFDQTtBQUNBWCxFQUFBQSxZQUFZLEVBQUVRLG1CQUFVRztBQVBULEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCwgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IExvZ291dERpYWxvZyBmcm9tIFwiLi4vZGlhbG9ncy9Mb2dvdXREaWFsb2dcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi9Nb2RhbFwiO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gJy4uLy4uLy4uL1Nka0NvbmZpZyc7XHJcbmltcG9ydCB7IGdldEhvc3RpbmdMaW5rIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvSG9zdGluZ0xpbmsnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSBcIi4uLy4uL3N0cnVjdHVyZXMvQ29udGV4dE1lbnVcIjtcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gXCIuLi8uLi8uLi9pbmRleFwiO1xyXG5pbXBvcnQge2dldEhvbWVQYWdlVXJsfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvcGFnZXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRvcExlZnRNZW51IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgZGlzcGxheU5hbWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICB1c2VySWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gT3B0aW9uYWwgZnVuY3Rpb24gdG8gY29sbGVjdCBhIHJlZmVyZW5jZSB0byB0aGUgY29udGFpbmVyXHJcbiAgICAgICAgLy8gb2YgdGhpcyBjb21wb25lbnQgZGlyZWN0bHkuXHJcbiAgICAgICAgY29udGFpbmVyUmVmOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLnZpZXdIb21lUGFnZSA9IHRoaXMudmlld0hvbWVQYWdlLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vcGVuU2V0dGluZ3MgPSB0aGlzLm9wZW5TZXR0aW5ncy5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuc2lnbkluID0gdGhpcy5zaWduSW4uYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLnNpZ25PdXQgPSB0aGlzLnNpZ25PdXQuYmluZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNIb21lUGFnZSgpIHtcclxuICAgICAgICByZXR1cm4gISFnZXRIb21lUGFnZVVybChTZGtDb25maWcuZ2V0KCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBpc0d1ZXN0ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzR3Vlc3QoKTtcclxuXHJcbiAgICAgICAgY29uc3QgaG9zdGluZ1NpZ251cExpbmsgPSBnZXRIb3N0aW5nTGluaygndXNlci1jb250ZXh0LW1lbnUnKTtcclxuICAgICAgICBsZXQgaG9zdGluZ1NpZ251cCA9IG51bGw7XHJcbiAgICAgICAgaWYgKGhvc3RpbmdTaWdudXBMaW5rKSB7XHJcbiAgICAgICAgICAgIGhvc3RpbmdTaWdudXAgPSA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvcExlZnRNZW51X3VwZ3JhZGVMaW5rXCI+XHJcbiAgICAgICAgICAgICAgICB7X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8YT5VcGdyYWRlPC9hPiB0byB5b3VyIG93biBkb21haW5cIiwge30sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhOiBzdWIgPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9e2hvc3RpbmdTaWdudXBMaW5rfSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCIgdGFiSW5kZXg9ey0xfT57c3VifTwvYT4sXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPXtob3N0aW5nU2lnbnVwTGlua30gdGFyZ2V0PVwiX2JsYW5rXCIgcmVsPVwibm9yZWZlcnJlciBub29wZW5lclwiIHJvbGU9XCJwcmVzZW50YXRpb25cIiBhcmlhLWhpZGRlbj17dHJ1ZX0gdGFiSW5kZXg9ey0xfT5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvZXh0ZXJuYWwtbGluay5zdmdcIil9IHdpZHRoPVwiMTFcIiBoZWlnaHQ9XCIxMFwiIGFsdD0nJyAvPlxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaG9tZVBhZ2VJdGVtID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5oYXNIb21lUGFnZSgpKSB7XHJcbiAgICAgICAgICAgIGhvbWVQYWdlSXRlbSA9IChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9Ub3BMZWZ0TWVudV9pY29uX2hvbWVcIiBvbkNsaWNrPXt0aGlzLnZpZXdIb21lUGFnZX0+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiSG9tZVwiKX1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2lnbkluT3V0SXRlbTtcclxuICAgICAgICBpZiAoaXNHdWVzdCkge1xyXG4gICAgICAgICAgICBzaWduSW5PdXRJdGVtID0gKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X1RvcExlZnRNZW51X2ljb25fc2lnbmluXCIgb25DbGljaz17dGhpcy5zaWduSW59PlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIlNpZ24gaW5cIil9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNpZ25Jbk91dEl0ZW0gPSAoXHJcbiAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfVG9wTGVmdE1lbnVfaWNvbl9zaWdub3V0XCIgb25DbGljaz17dGhpcy5zaWduT3V0fT5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJTaWduIG91dFwiKX1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBoZWxwSXRlbSA9IChcclxuICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X1RvcExlZnRNZW51X2ljb25faGVscFwiIG9uQ2xpY2s9e3RoaXMub3BlbkhlbHB9PlxyXG4gICAgICAgICAgICAgICAge190KFwiSGVscFwiKX1cclxuICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBzZXR0aW5nc0l0ZW0gPSAoXHJcbiAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9Ub3BMZWZ0TWVudV9pY29uX3NldHRpbmdzXCIgb25DbGljaz17dGhpcy5vcGVuU2V0dGluZ3N9PlxyXG4gICAgICAgICAgICAgICAge190KFwiU2V0dGluZ3NcIil9XHJcbiAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwibXhfVG9wTGVmdE1lbnVcIiByZWY9e3RoaXMucHJvcHMuY29udGFpbmVyUmVmfSByb2xlPVwibWVudVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvcExlZnRNZW51X3NlY3Rpb25fbm9JY29uXCIgYXJpYS1yZWFkb25seT17dHJ1ZX0gdGFiSW5kZXg9ey0xfT5cclxuICAgICAgICAgICAgICAgIDxkaXY+e3RoaXMucHJvcHMuZGlzcGxheU5hbWV9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X1RvcExlZnRNZW51X2dyZXllZFRleHRcIiBhcmlhLWhpZGRlbj17dHJ1ZX0+e3RoaXMucHJvcHMudXNlcklkfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAge2hvc3RpbmdTaWdudXB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwibXhfVG9wTGVmdE1lbnVfc2VjdGlvbl93aXRoSWNvblwiIHJvbGU9XCJub25lXCI+XHJcbiAgICAgICAgICAgICAgICB7aG9tZVBhZ2VJdGVtfVxyXG4gICAgICAgICAgICAgICAge3NldHRpbmdzSXRlbX1cclxuICAgICAgICAgICAgICAgIHtoZWxwSXRlbX1cclxuICAgICAgICAgICAgICAgIHtzaWduSW5PdXRJdGVtfVxyXG4gICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuSGVscCA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgICAgIGNvbnN0IFJlZGVzaWduRmVlZGJhY2tEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZGlhbG9ncy5SZWRlc2lnbkZlZWRiYWNrRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlcG9ydCBidWdzICYgZ2l2ZSBmZWVkYmFjaycsICcnLCBSZWRlc2lnbkZlZWRiYWNrRGlhbG9nKTtcclxuICAgIH07XHJcblxyXG4gICAgdmlld0hvbWVQYWdlKCkge1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19ob21lX3BhZ2UnfSk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuU2V0dGluZ3MoKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd2aWV3X3VzZXJfc2V0dGluZ3MnfSk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH1cclxuXHJcbiAgICBzaWduSW4oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICdzdGFydF9sb2dpbid9KTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNpZ25PdXQoKSB7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnTG9nb3V0IEUyRSBFeHBvcnQnLCAnJywgTG9nb3V0RGlhbG9nKTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlTWVudSgpIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkZpbmlzaGVkKSB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgIH1cclxufVxyXG4iXX0=