"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../../../index"));

var _AccessibleButton = _interopRequireDefault(require("../elements/AccessibleButton"));

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class StatusMessageContextMenu extends _react.default.Component {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onStatusMessageCommitted", () => {
      // The `User` object has observed a status message change.
      this.setState({
        message: this.comittedStatusMessage,
        waiting: false
      });
    });
    (0, _defineProperty2.default)(this, "_onClearClick", e => {
      _MatrixClientPeg.MatrixClientPeg.get()._unstable_setStatusMessage("");

      this.setState({
        waiting: true
      });
    });
    (0, _defineProperty2.default)(this, "_onSubmit", e => {
      e.preventDefault();

      _MatrixClientPeg.MatrixClientPeg.get()._unstable_setStatusMessage(this.state.message);

      this.setState({
        waiting: true
      });
    });
    (0, _defineProperty2.default)(this, "_onStatusChange", e => {
      // The input field's value was changed.
      this.setState({
        message: e.target.value
      });
    });
    this.state = {
      message: this.comittedStatusMessage
    };
  }

  componentDidMount() {
    const {
      user
    } = this.props;

    if (!user) {
      return;
    }

    user.on("User._unstable_statusMessage", this._onStatusMessageCommitted);
  }

  componentWillUnmount() {
    const {
      user
    } = this.props;

    if (!user) {
      return;
    }

    user.removeListener("User._unstable_statusMessage", this._onStatusMessageCommitted);
  }

  get comittedStatusMessage() {
    return this.props.user ? this.props.user._unstable_statusMessage : "";
  }

  render() {
    const Spinner = sdk.getComponent('views.elements.Spinner');
    let actionButton;

    if (this.comittedStatusMessage) {
      if (this.state.message === this.comittedStatusMessage) {
        actionButton = _react.default.createElement(_AccessibleButton.default, {
          className: "mx_StatusMessageContextMenu_clear",
          onClick: this._onClearClick
        }, _react.default.createElement("span", null, (0, _languageHandler._t)("Clear status")));
      } else {
        actionButton = _react.default.createElement(_AccessibleButton.default, {
          className: "mx_StatusMessageContextMenu_submit",
          onClick: this._onSubmit
        }, _react.default.createElement("span", null, (0, _languageHandler._t)("Update status")));
      }
    } else {
      actionButton = _react.default.createElement(_AccessibleButton.default, {
        className: "mx_StatusMessageContextMenu_submit",
        disabled: !this.state.message,
        onClick: this._onSubmit
      }, _react.default.createElement("span", null, (0, _languageHandler._t)("Set status")));
    }

    let spinner = null;

    if (this.state.waiting) {
      spinner = _react.default.createElement(Spinner, {
        w: "24",
        h: "24"
      });
    }

    const form = _react.default.createElement("form", {
      className: "mx_StatusMessageContextMenu_form",
      autoComplete: "off",
      onSubmit: this._onSubmit
    }, _react.default.createElement("input", {
      type: "text",
      className: "mx_StatusMessageContextMenu_message",
      key: "message",
      placeholder: (0, _languageHandler._t)("Set a new status..."),
      autoFocus: true,
      maxLength: "60",
      value: this.state.message,
      onChange: this._onStatusChange
    }), _react.default.createElement("div", {
      className: "mx_StatusMessageContextMenu_actionContainer"
    }, actionButton, spinner));

    return _react.default.createElement("div", {
      className: "mx_StatusMessageContextMenu"
    }, form);
  }

}

exports.default = StatusMessageContextMenu;
(0, _defineProperty2.default)(StatusMessageContextMenu, "propTypes", {
  // js-sdk User object. Not required because it might not exist.
  user: _propTypes.default.object
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvU3RhdHVzTWVzc2FnZUNvbnRleHRNZW51LmpzIl0sIm5hbWVzIjpbIlN0YXR1c01lc3NhZ2VDb250ZXh0TWVudSIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInNldFN0YXRlIiwibWVzc2FnZSIsImNvbWl0dGVkU3RhdHVzTWVzc2FnZSIsIndhaXRpbmciLCJlIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiX3Vuc3RhYmxlX3NldFN0YXR1c01lc3NhZ2UiLCJwcmV2ZW50RGVmYXVsdCIsInN0YXRlIiwidGFyZ2V0IiwidmFsdWUiLCJjb21wb25lbnREaWRNb3VudCIsInVzZXIiLCJvbiIsIl9vblN0YXR1c01lc3NhZ2VDb21taXR0ZWQiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZUxpc3RlbmVyIiwiX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2UiLCJyZW5kZXIiLCJTcGlubmVyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiYWN0aW9uQnV0dG9uIiwiX29uQ2xlYXJDbGljayIsIl9vblN1Ym1pdCIsInNwaW5uZXIiLCJmb3JtIiwiX29uU3RhdHVzQ2hhbmdlIiwiUHJvcFR5cGVzIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7O0FBdUJlLE1BQU1BLHdCQUFOLFNBQXVDQyxlQUFNQyxTQUE3QyxDQUF1RDtBQU1sRUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUscUVBK0JTLE1BQU07QUFDOUI7QUFDQSxXQUFLQyxRQUFMLENBQWM7QUFDVkMsUUFBQUEsT0FBTyxFQUFFLEtBQUtDLHFCQURKO0FBRVZDLFFBQUFBLE9BQU8sRUFBRTtBQUZDLE9BQWQ7QUFJSCxLQXJDa0I7QUFBQSx5REF1Q0ZDLENBQUQsSUFBTztBQUNuQkMsdUNBQWdCQyxHQUFoQixHQUFzQkMsMEJBQXRCLENBQWlELEVBQWpEOztBQUNBLFdBQUtQLFFBQUwsQ0FBYztBQUNWRyxRQUFBQSxPQUFPLEVBQUU7QUFEQyxPQUFkO0FBR0gsS0E1Q2tCO0FBQUEscURBOENOQyxDQUFELElBQU87QUFDZkEsTUFBQUEsQ0FBQyxDQUFDSSxjQUFGOztBQUNBSCx1Q0FBZ0JDLEdBQWhCLEdBQXNCQywwQkFBdEIsQ0FBaUQsS0FBS0UsS0FBTCxDQUFXUixPQUE1RDs7QUFDQSxXQUFLRCxRQUFMLENBQWM7QUFDVkcsUUFBQUEsT0FBTyxFQUFFO0FBREMsT0FBZDtBQUdILEtBcERrQjtBQUFBLDJEQXNEQUMsQ0FBRCxJQUFPO0FBQ3JCO0FBQ0EsV0FBS0osUUFBTCxDQUFjO0FBQ1ZDLFFBQUFBLE9BQU8sRUFBRUcsQ0FBQyxDQUFDTSxNQUFGLENBQVNDO0FBRFIsT0FBZDtBQUdILEtBM0RrQjtBQUdmLFNBQUtGLEtBQUwsR0FBYTtBQUNUUixNQUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFETCxLQUFiO0FBR0g7O0FBRURVLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFVBQU07QUFBRUMsTUFBQUE7QUFBRixRQUFXLEtBQUtkLEtBQXRCOztBQUNBLFFBQUksQ0FBQ2MsSUFBTCxFQUFXO0FBQ1A7QUFDSDs7QUFDREEsSUFBQUEsSUFBSSxDQUFDQyxFQUFMLENBQVEsOEJBQVIsRUFBd0MsS0FBS0MseUJBQTdDO0FBQ0g7O0FBRURDLEVBQUFBLG9CQUFvQixHQUFHO0FBQ25CLFVBQU07QUFBRUgsTUFBQUE7QUFBRixRQUFXLEtBQUtkLEtBQXRCOztBQUNBLFFBQUksQ0FBQ2MsSUFBTCxFQUFXO0FBQ1A7QUFDSDs7QUFDREEsSUFBQUEsSUFBSSxDQUFDSSxjQUFMLENBQ0ksOEJBREosRUFFSSxLQUFLRix5QkFGVDtBQUlIOztBQUVELE1BQUliLHFCQUFKLEdBQTRCO0FBQ3hCLFdBQU8sS0FBS0gsS0FBTCxDQUFXYyxJQUFYLEdBQWtCLEtBQUtkLEtBQUwsQ0FBV2MsSUFBWCxDQUFnQkssdUJBQWxDLEdBQTRELEVBQW5FO0FBQ0g7O0FBZ0NEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxPQUFPLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBaEI7QUFFQSxRQUFJQyxZQUFKOztBQUNBLFFBQUksS0FBS3JCLHFCQUFULEVBQWdDO0FBQzVCLFVBQUksS0FBS08sS0FBTCxDQUFXUixPQUFYLEtBQXVCLEtBQUtDLHFCQUFoQyxFQUF1RDtBQUNuRHFCLFFBQUFBLFlBQVksR0FBRyw2QkFBQyx5QkFBRDtBQUFrQixVQUFBLFNBQVMsRUFBQyxtQ0FBNUI7QUFDWCxVQUFBLE9BQU8sRUFBRSxLQUFLQztBQURILFdBR1gsMkNBQU8seUJBQUcsY0FBSCxDQUFQLENBSFcsQ0FBZjtBQUtILE9BTkQsTUFNTztBQUNIRCxRQUFBQSxZQUFZLEdBQUcsNkJBQUMseUJBQUQ7QUFBa0IsVUFBQSxTQUFTLEVBQUMsb0NBQTVCO0FBQ1gsVUFBQSxPQUFPLEVBQUUsS0FBS0U7QUFESCxXQUdYLDJDQUFPLHlCQUFHLGVBQUgsQ0FBUCxDQUhXLENBQWY7QUFLSDtBQUNKLEtBZEQsTUFjTztBQUNIRixNQUFBQSxZQUFZLEdBQUcsNkJBQUMseUJBQUQ7QUFBa0IsUUFBQSxTQUFTLEVBQUMsb0NBQTVCO0FBQ1gsUUFBQSxRQUFRLEVBQUUsQ0FBQyxLQUFLZCxLQUFMLENBQVdSLE9BRFg7QUFDb0IsUUFBQSxPQUFPLEVBQUUsS0FBS3dCO0FBRGxDLFNBR1gsMkNBQU8seUJBQUcsWUFBSCxDQUFQLENBSFcsQ0FBZjtBQUtIOztBQUVELFFBQUlDLE9BQU8sR0FBRyxJQUFkOztBQUNBLFFBQUksS0FBS2pCLEtBQUwsQ0FBV04sT0FBZixFQUF3QjtBQUNwQnVCLE1BQUFBLE9BQU8sR0FBRyw2QkFBQyxPQUFEO0FBQVMsUUFBQSxDQUFDLEVBQUMsSUFBWDtBQUFnQixRQUFBLENBQUMsRUFBQztBQUFsQixRQUFWO0FBQ0g7O0FBRUQsVUFBTUMsSUFBSSxHQUFHO0FBQU0sTUFBQSxTQUFTLEVBQUMsa0NBQWhCO0FBQ1QsTUFBQSxZQUFZLEVBQUMsS0FESjtBQUNVLE1BQUEsUUFBUSxFQUFFLEtBQUtGO0FBRHpCLE9BR1Q7QUFBTyxNQUFBLElBQUksRUFBQyxNQUFaO0FBQW1CLE1BQUEsU0FBUyxFQUFDLHFDQUE3QjtBQUNJLE1BQUEsR0FBRyxFQUFDLFNBRFI7QUFDa0IsTUFBQSxXQUFXLEVBQUUseUJBQUcscUJBQUgsQ0FEL0I7QUFFSSxNQUFBLFNBQVMsRUFBRSxJQUZmO0FBRXFCLE1BQUEsU0FBUyxFQUFDLElBRi9CO0FBRW9DLE1BQUEsS0FBSyxFQUFFLEtBQUtoQixLQUFMLENBQVdSLE9BRnREO0FBR0ksTUFBQSxRQUFRLEVBQUUsS0FBSzJCO0FBSG5CLE1BSFMsRUFRVDtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS0wsWUFETCxFQUVLRyxPQUZMLENBUlMsQ0FBYjs7QUFjQSxXQUFPO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNEQyxJQURDLENBQVA7QUFHSDs7QUFuSGlFOzs7OEJBQWpEaEMsd0IsZUFDRTtBQUNmO0FBQ0FrQixFQUFBQSxJQUFJLEVBQUVnQixtQkFBVUM7QUFGRCxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgQWNjZXNzaWJsZUJ1dHRvbiBmcm9tICcuLi9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0YXR1c01lc3NhZ2VDb250ZXh0TWVudSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIC8vIGpzLXNkayBVc2VyIG9iamVjdC4gTm90IHJlcXVpcmVkIGJlY2F1c2UgaXQgbWlnaHQgbm90IGV4aXN0LlxyXG4gICAgICAgIHVzZXI6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBtZXNzYWdlOiB0aGlzLmNvbWl0dGVkU3RhdHVzTWVzc2FnZSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IHsgdXNlciB9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAoIXVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB1c2VyLm9uKFwiVXNlci5fdW5zdGFibGVfc3RhdHVzTWVzc2FnZVwiLCB0aGlzLl9vblN0YXR1c01lc3NhZ2VDb21taXR0ZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IHsgdXNlciB9ID0gdGhpcy5wcm9wcztcclxuICAgICAgICBpZiAoIXVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB1c2VyLnJlbW92ZUxpc3RlbmVyKFxyXG4gICAgICAgICAgICBcIlVzZXIuX3Vuc3RhYmxlX3N0YXR1c01lc3NhZ2VcIixcclxuICAgICAgICAgICAgdGhpcy5fb25TdGF0dXNNZXNzYWdlQ29tbWl0dGVkLFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNvbWl0dGVkU3RhdHVzTWVzc2FnZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy51c2VyID8gdGhpcy5wcm9wcy51c2VyLl91bnN0YWJsZV9zdGF0dXNNZXNzYWdlIDogXCJcIjtcclxuICAgIH1cclxuXHJcbiAgICBfb25TdGF0dXNNZXNzYWdlQ29tbWl0dGVkID0gKCkgPT4ge1xyXG4gICAgICAgIC8vIFRoZSBgVXNlcmAgb2JqZWN0IGhhcyBvYnNlcnZlZCBhIHN0YXR1cyBtZXNzYWdlIGNoYW5nZS5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgbWVzc2FnZTogdGhpcy5jb21pdHRlZFN0YXR1c01lc3NhZ2UsXHJcbiAgICAgICAgICAgIHdhaXRpbmc6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25DbGVhckNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuX3Vuc3RhYmxlX3NldFN0YXR1c01lc3NhZ2UoXCJcIik7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHdhaXRpbmc6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblN1Ym1pdCA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5fdW5zdGFibGVfc2V0U3RhdHVzTWVzc2FnZSh0aGlzLnN0YXRlLm1lc3NhZ2UpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB3YWl0aW5nOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfb25TdGF0dXNDaGFuZ2UgPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIFRoZSBpbnB1dCBmaWVsZCdzIHZhbHVlIHdhcyBjaGFuZ2VkLlxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBtZXNzYWdlOiBlLnRhcmdldC52YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5TcGlubmVyJyk7XHJcblxyXG4gICAgICAgIGxldCBhY3Rpb25CdXR0b247XHJcbiAgICAgICAgaWYgKHRoaXMuY29taXR0ZWRTdGF0dXNNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLm1lc3NhZ2UgPT09IHRoaXMuY29taXR0ZWRTdGF0dXNNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb25CdXR0b24gPSA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9TdGF0dXNNZXNzYWdlQ29udGV4dE1lbnVfY2xlYXJcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQ2xlYXJDbGlja31cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57X3QoXCJDbGVhciBzdGF0dXNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbkJ1dHRvbiA9IDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1N0YXR1c01lc3NhZ2VDb250ZXh0TWVudV9zdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uU3VibWl0fVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPntfdChcIlVwZGF0ZSBzdGF0dXNcIil9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGFjdGlvbkJ1dHRvbiA9IDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X1N0YXR1c01lc3NhZ2VDb250ZXh0TWVudV9zdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyF0aGlzLnN0YXRlLm1lc3NhZ2V9IG9uQ2xpY2s9e3RoaXMuX29uU3VibWl0fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8c3Bhbj57X3QoXCJTZXQgc3RhdHVzXCIpfTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzcGlubmVyID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS53YWl0aW5nKSB7XHJcbiAgICAgICAgICAgIHNwaW5uZXIgPSA8U3Bpbm5lciB3PVwiMjRcIiBoPVwiMjRcIiAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZvcm0gPSA8Zm9ybSBjbGFzc05hbWU9XCJteF9TdGF0dXNNZXNzYWdlQ29udGV4dE1lbnVfZm9ybVwiXHJcbiAgICAgICAgICAgIGF1dG9Db21wbGV0ZT1cIm9mZlwiIG9uU3VibWl0PXt0aGlzLl9vblN1Ym1pdH1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cIm14X1N0YXR1c01lc3NhZ2VDb250ZXh0TWVudV9tZXNzYWdlXCJcclxuICAgICAgICAgICAgICAgIGtleT1cIm1lc3NhZ2VcIiBwbGFjZWhvbGRlcj17X3QoXCJTZXQgYSBuZXcgc3RhdHVzLi4uXCIpfVxyXG4gICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfSBtYXhMZW5ndGg9XCI2MFwiIHZhbHVlPXt0aGlzLnN0YXRlLm1lc3NhZ2V9XHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25TdGF0dXNDaGFuZ2V9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfU3RhdHVzTWVzc2FnZUNvbnRleHRNZW51X2FjdGlvbkNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAge2FjdGlvbkJ1dHRvbn1cclxuICAgICAgICAgICAgICAgIHtzcGlubmVyfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Zvcm0+O1xyXG5cclxuICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9TdGF0dXNNZXNzYWdlQ29udGV4dE1lbnVcIj5cclxuICAgICAgICAgICAgeyBmb3JtIH1cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuIl19