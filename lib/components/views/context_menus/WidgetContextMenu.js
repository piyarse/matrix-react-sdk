"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("../../../languageHandler");

var _ContextMenu = require("../../structures/ContextMenu");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class WidgetContextMenu extends _react.default.Component {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onEditClicked", () => {
      this.proxyClick(this.props.onEditClicked);
    });
    (0, _defineProperty2.default)(this, "onReloadClicked", () => {
      this.proxyClick(this.props.onReloadClicked);
    });
    (0, _defineProperty2.default)(this, "onSnapshotClicked", () => {
      this.proxyClick(this.props.onSnapshotClicked);
    });
    (0, _defineProperty2.default)(this, "onDeleteClicked", () => {
      this.proxyClick(this.props.onDeleteClicked);
    });
    (0, _defineProperty2.default)(this, "onRevokeClicked", () => {
      this.proxyClick(this.props.onRevokeClicked);
    });
  }

  proxyClick(fn) {
    fn();
    if (this.props.onFinished) this.props.onFinished();
  } // XXX: It's annoying that our context menus require us to hit onFinished() to close :(


  render() {
    const options = [];

    if (this.props.onEditClicked) {
      options.push(_react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_WidgetContextMenu_option",
        onClick: this.onEditClicked,
        key: "edit"
      }, (0, _languageHandler._t)("Edit")));
    }

    if (this.props.onReloadClicked) {
      options.push(_react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_WidgetContextMenu_option",
        onClick: this.onReloadClicked,
        key: "reload"
      }, (0, _languageHandler._t)("Reload")));
    }

    if (this.props.onSnapshotClicked) {
      options.push(_react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_WidgetContextMenu_option",
        onClick: this.onSnapshotClicked,
        key: "snap"
      }, (0, _languageHandler._t)("Take picture")));
    }

    if (this.props.onDeleteClicked) {
      options.push(_react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_WidgetContextMenu_option",
        onClick: this.onDeleteClicked,
        key: "delete"
      }, (0, _languageHandler._t)("Remove for everyone")));
    } // Push this last so it appears last. It's always present.


    options.push(_react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_WidgetContextMenu_option",
      onClick: this.onRevokeClicked,
      key: "revoke"
    }, (0, _languageHandler._t)("Remove for me"))); // Put separators between the options

    if (options.length > 1) {
      const length = options.length;

      for (let i = 0; i < length - 1; i++) {
        const sep = _react.default.createElement("hr", {
          key: i,
          className: "mx_WidgetContextMenu_separator"
        }); // Insert backwards so the insertions don't affect our math on where to place them.
        // We also use our cached length to avoid worrying about options.length changing


        options.splice(length - 1 - i, 0, sep);
      }
    }

    return _react.default.createElement("div", {
      className: "mx_WidgetContextMenu"
    }, options);
  }

}

exports.default = WidgetContextMenu;
(0, _defineProperty2.default)(WidgetContextMenu, "propTypes", {
  onFinished: _propTypes.default.func,
  // Callback for when the revoke button is clicked. Required.
  onRevokeClicked: _propTypes.default.func.isRequired,
  // Callback for when the snapshot button is clicked. Button not shown
  // without a callback.
  onSnapshotClicked: _propTypes.default.func,
  // Callback for when the reload button is clicked. Button not shown
  // without a callback.
  onReloadClicked: _propTypes.default.func,
  // Callback for when the edit button is clicked. Button not shown
  // without a callback.
  onEditClicked: _propTypes.default.func,
  // Callback for when the delete button is clicked. Button not shown
  // without a callback.
  onDeleteClicked: _propTypes.default.func
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvV2lkZ2V0Q29udGV4dE1lbnUuanMiXSwibmFtZXMiOlsiV2lkZ2V0Q29udGV4dE1lbnUiLCJSZWFjdCIsIkNvbXBvbmVudCIsInByb3h5Q2xpY2siLCJwcm9wcyIsIm9uRWRpdENsaWNrZWQiLCJvblJlbG9hZENsaWNrZWQiLCJvblNuYXBzaG90Q2xpY2tlZCIsIm9uRGVsZXRlQ2xpY2tlZCIsIm9uUmV2b2tlQ2xpY2tlZCIsImZuIiwib25GaW5pc2hlZCIsInJlbmRlciIsIm9wdGlvbnMiLCJwdXNoIiwibGVuZ3RoIiwiaSIsInNlcCIsInNwbGljZSIsIlByb3BUeXBlcyIsImZ1bmMiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFuQkE7Ozs7Ozs7Ozs7Ozs7OztBQXFCZSxNQUFNQSxpQkFBTixTQUFnQ0MsZUFBTUMsU0FBdEMsQ0FBZ0Q7QUFBQTtBQUFBO0FBQUEseURBK0IzQyxNQUFNO0FBQ2xCLFdBQUtDLFVBQUwsQ0FBZ0IsS0FBS0MsS0FBTCxDQUFXQyxhQUEzQjtBQUNILEtBakMwRDtBQUFBLDJEQW1DekMsTUFBTTtBQUNwQixXQUFLRixVQUFMLENBQWdCLEtBQUtDLEtBQUwsQ0FBV0UsZUFBM0I7QUFDSCxLQXJDMEQ7QUFBQSw2REF1Q3ZDLE1BQU07QUFDdEIsV0FBS0gsVUFBTCxDQUFnQixLQUFLQyxLQUFMLENBQVdHLGlCQUEzQjtBQUNILEtBekMwRDtBQUFBLDJEQTJDekMsTUFBTTtBQUNwQixXQUFLSixVQUFMLENBQWdCLEtBQUtDLEtBQUwsQ0FBV0ksZUFBM0I7QUFDSCxLQTdDMEQ7QUFBQSwyREErQ3pDLE1BQU07QUFDcEIsV0FBS0wsVUFBTCxDQUFnQixLQUFLQyxLQUFMLENBQVdLLGVBQTNCO0FBQ0gsS0FqRDBEO0FBQUE7O0FBd0IzRE4sRUFBQUEsVUFBVSxDQUFDTyxFQUFELEVBQUs7QUFDWEEsSUFBQUEsRUFBRTtBQUNGLFFBQUksS0FBS04sS0FBTCxDQUFXTyxVQUFmLEVBQTJCLEtBQUtQLEtBQUwsQ0FBV08sVUFBWDtBQUM5QixHQTNCMEQsQ0E2QjNEOzs7QUFzQkFDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLE9BQU8sR0FBRyxFQUFoQjs7QUFFQSxRQUFJLEtBQUtULEtBQUwsQ0FBV0MsYUFBZixFQUE4QjtBQUMxQlEsTUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQ0ksNkJBQUMscUJBQUQ7QUFBVSxRQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsUUFBQSxPQUFPLEVBQUUsS0FBS1QsYUFBaEU7QUFBK0UsUUFBQSxHQUFHLEVBQUM7QUFBbkYsU0FDSyx5QkFBRyxNQUFILENBREwsQ0FESjtBQUtIOztBQUVELFFBQUksS0FBS0QsS0FBTCxDQUFXRSxlQUFmLEVBQWdDO0FBQzVCTyxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FDSSw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLUixlQUFoRTtBQUFpRixRQUFBLEdBQUcsRUFBQztBQUFyRixTQUNLLHlCQUFHLFFBQUgsQ0FETCxDQURKO0FBS0g7O0FBRUQsUUFBSSxLQUFLRixLQUFMLENBQVdHLGlCQUFmLEVBQWtDO0FBQzlCTSxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FDSSw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLUCxpQkFBaEU7QUFBbUYsUUFBQSxHQUFHLEVBQUM7QUFBdkYsU0FDSyx5QkFBRyxjQUFILENBREwsQ0FESjtBQUtIOztBQUVELFFBQUksS0FBS0gsS0FBTCxDQUFXSSxlQUFmLEVBQWdDO0FBQzVCSyxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FDSSw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLTixlQUFoRTtBQUFpRixRQUFBLEdBQUcsRUFBQztBQUFyRixTQUNLLHlCQUFHLHFCQUFILENBREwsQ0FESjtBQUtILEtBakNJLENBbUNMOzs7QUFDQUssSUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQ0ksNkJBQUMscUJBQUQ7QUFBVSxNQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsTUFBQSxPQUFPLEVBQUUsS0FBS0wsZUFBaEU7QUFBaUYsTUFBQSxHQUFHLEVBQUM7QUFBckYsT0FDSyx5QkFBRyxlQUFILENBREwsQ0FESixFQXBDSyxDQTBDTDs7QUFDQSxRQUFJSSxPQUFPLENBQUNFLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEIsWUFBTUEsTUFBTSxHQUFHRixPQUFPLENBQUNFLE1BQXZCOztBQUNBLFdBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0QsTUFBTSxHQUFHLENBQTdCLEVBQWdDQyxDQUFDLEVBQWpDLEVBQXFDO0FBQ2pDLGNBQU1DLEdBQUcsR0FBRztBQUFJLFVBQUEsR0FBRyxFQUFFRCxDQUFUO0FBQVksVUFBQSxTQUFTLEVBQUM7QUFBdEIsVUFBWixDQURpQyxDQUdqQztBQUNBOzs7QUFDQUgsUUFBQUEsT0FBTyxDQUFDSyxNQUFSLENBQWVILE1BQU0sR0FBRyxDQUFULEdBQWFDLENBQTVCLEVBQStCLENBQS9CLEVBQWtDQyxHQUFsQztBQUNIO0FBQ0o7O0FBRUQsV0FBTztBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FBdUNKLE9BQXZDLENBQVA7QUFDSDs7QUExRzBEOzs7OEJBQTFDYixpQixlQUNFO0FBQ2ZXLEVBQUFBLFVBQVUsRUFBRVEsbUJBQVVDLElBRFA7QUFHZjtBQUNBWCxFQUFBQSxlQUFlLEVBQUVVLG1CQUFVQyxJQUFWLENBQWVDLFVBSmpCO0FBTWY7QUFDQTtBQUNBZCxFQUFBQSxpQkFBaUIsRUFBRVksbUJBQVVDLElBUmQ7QUFVZjtBQUNBO0FBQ0FkLEVBQUFBLGVBQWUsRUFBRWEsbUJBQVVDLElBWlo7QUFjZjtBQUNBO0FBQ0FmLEVBQUFBLGFBQWEsRUFBRWMsbUJBQVVDLElBaEJWO0FBa0JmO0FBQ0E7QUFDQVosRUFBQUEsZUFBZSxFQUFFVyxtQkFBVUM7QUFwQlosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHtfdH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSBcIi4uLy4uL3N0cnVjdHVyZXMvQ29udGV4dE1lbnVcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFdpZGdldENvbnRleHRNZW51IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIENhbGxiYWNrIGZvciB3aGVuIHRoZSByZXZva2UgYnV0dG9uIGlzIGNsaWNrZWQuIFJlcXVpcmVkLlxyXG4gICAgICAgIG9uUmV2b2tlQ2xpY2tlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuXHJcbiAgICAgICAgLy8gQ2FsbGJhY2sgZm9yIHdoZW4gdGhlIHNuYXBzaG90IGJ1dHRvbiBpcyBjbGlja2VkLiBCdXR0b24gbm90IHNob3duXHJcbiAgICAgICAgLy8gd2l0aG91dCBhIGNhbGxiYWNrLlxyXG4gICAgICAgIG9uU25hcHNob3RDbGlja2VkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gQ2FsbGJhY2sgZm9yIHdoZW4gdGhlIHJlbG9hZCBidXR0b24gaXMgY2xpY2tlZC4gQnV0dG9uIG5vdCBzaG93blxyXG4gICAgICAgIC8vIHdpdGhvdXQgYSBjYWxsYmFjay5cclxuICAgICAgICBvblJlbG9hZENsaWNrZWQ6IFByb3BUeXBlcy5mdW5jLFxyXG5cclxuICAgICAgICAvLyBDYWxsYmFjayBmb3Igd2hlbiB0aGUgZWRpdCBidXR0b24gaXMgY2xpY2tlZC4gQnV0dG9uIG5vdCBzaG93blxyXG4gICAgICAgIC8vIHdpdGhvdXQgYSBjYWxsYmFjay5cclxuICAgICAgICBvbkVkaXRDbGlja2VkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLy8gQ2FsbGJhY2sgZm9yIHdoZW4gdGhlIGRlbGV0ZSBidXR0b24gaXMgY2xpY2tlZC4gQnV0dG9uIG5vdCBzaG93blxyXG4gICAgICAgIC8vIHdpdGhvdXQgYSBjYWxsYmFjay5cclxuICAgICAgICBvbkRlbGV0ZUNsaWNrZWQ6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgfTtcclxuXHJcbiAgICBwcm94eUNsaWNrKGZuKSB7XHJcbiAgICAgICAgZm4oKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkZpbmlzaGVkKSB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBYWFg6IEl0J3MgYW5ub3lpbmcgdGhhdCBvdXIgY29udGV4dCBtZW51cyByZXF1aXJlIHVzIHRvIGhpdCBvbkZpbmlzaGVkKCkgdG8gY2xvc2UgOihcclxuXHJcbiAgICBvbkVkaXRDbGlja2VkID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJveHlDbGljayh0aGlzLnByb3BzLm9uRWRpdENsaWNrZWQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvblJlbG9hZENsaWNrZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm94eUNsaWNrKHRoaXMucHJvcHMub25SZWxvYWRDbGlja2VkKTtcclxuICAgIH07XHJcblxyXG4gICAgb25TbmFwc2hvdENsaWNrZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm94eUNsaWNrKHRoaXMucHJvcHMub25TbmFwc2hvdENsaWNrZWQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvbkRlbGV0ZUNsaWNrZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm94eUNsaWNrKHRoaXMucHJvcHMub25EZWxldGVDbGlja2VkKTtcclxuICAgIH07XHJcblxyXG4gICAgb25SZXZva2VDbGlja2VkID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJveHlDbGljayh0aGlzLnByb3BzLm9uUmV2b2tlQ2xpY2tlZCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBvcHRpb25zID0gW107XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRWRpdENsaWNrZWQpIHtcclxuICAgICAgICAgICAgb3B0aW9ucy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT0nbXhfV2lkZ2V0Q29udGV4dE1lbnVfb3B0aW9uJyBvbkNsaWNrPXt0aGlzLm9uRWRpdENsaWNrZWR9IGtleT0nZWRpdCc+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiRWRpdFwiKX1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25SZWxvYWRDbGlja2VkKSB7XHJcbiAgICAgICAgICAgIG9wdGlvbnMucHVzaChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9J214X1dpZGdldENvbnRleHRNZW51X29wdGlvbicgb25DbGljaz17dGhpcy5vblJlbG9hZENsaWNrZWR9IGtleT0ncmVsb2FkJz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJSZWxvYWRcIil9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uU25hcHNob3RDbGlja2VkKSB7XHJcbiAgICAgICAgICAgIG9wdGlvbnMucHVzaChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9J214X1dpZGdldENvbnRleHRNZW51X29wdGlvbicgb25DbGljaz17dGhpcy5vblNuYXBzaG90Q2xpY2tlZH0ga2V5PSdzbmFwJz5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJUYWtlIHBpY3R1cmVcIil9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRGVsZXRlQ2xpY2tlZCkge1xyXG4gICAgICAgICAgICBvcHRpb25zLnB1c2goXHJcbiAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPSdteF9XaWRnZXRDb250ZXh0TWVudV9vcHRpb24nIG9uQ2xpY2s9e3RoaXMub25EZWxldGVDbGlja2VkfSBrZXk9J2RlbGV0ZSc+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiUmVtb3ZlIGZvciBldmVyeW9uZVwiKX1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gUHVzaCB0aGlzIGxhc3Qgc28gaXQgYXBwZWFycyBsYXN0LiBJdCdzIGFsd2F5cyBwcmVzZW50LlxyXG4gICAgICAgIG9wdGlvbnMucHVzaChcclxuICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT0nbXhfV2lkZ2V0Q29udGV4dE1lbnVfb3B0aW9uJyBvbkNsaWNrPXt0aGlzLm9uUmV2b2tlQ2xpY2tlZH0ga2V5PSdyZXZva2UnPlxyXG4gICAgICAgICAgICAgICAge190KFwiUmVtb3ZlIGZvciBtZVwiKX1cclxuICAgICAgICAgICAgPC9NZW51SXRlbT4sXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgLy8gUHV0IHNlcGFyYXRvcnMgYmV0d2VlbiB0aGUgb3B0aW9uc1xyXG4gICAgICAgIGlmIChvcHRpb25zLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgY29uc3QgbGVuZ3RoID0gb3B0aW9ucy5sZW5ndGg7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGVuZ3RoIC0gMTsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZXAgPSA8aHIga2V5PXtpfSBjbGFzc05hbWU9XCJteF9XaWRnZXRDb250ZXh0TWVudV9zZXBhcmF0b3JcIiAvPjtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBJbnNlcnQgYmFja3dhcmRzIHNvIHRoZSBpbnNlcnRpb25zIGRvbid0IGFmZmVjdCBvdXIgbWF0aCBvbiB3aGVyZSB0byBwbGFjZSB0aGVtLlxyXG4gICAgICAgICAgICAgICAgLy8gV2UgYWxzbyB1c2Ugb3VyIGNhY2hlZCBsZW5ndGggdG8gYXZvaWQgd29ycnlpbmcgYWJvdXQgb3B0aW9ucy5sZW5ndGggY2hhbmdpbmdcclxuICAgICAgICAgICAgICAgIG9wdGlvbnMuc3BsaWNlKGxlbmd0aCAtIDEgLSBpLCAwLCBzZXApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9XCJteF9XaWRnZXRDb250ZXh0TWVudVwiPntvcHRpb25zfTwvZGl2PjtcclxuICAgIH1cclxufVxyXG4iXX0=