"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _matrixJsSdk = require("matrix-js-sdk");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _Resend = _interopRequireDefault(require("../../../Resend"));

var _SettingsStore = _interopRequireDefault(require("../../../settings/SettingsStore"));

var _HtmlUtils = require("../../../HtmlUtils");

var _EventUtils = require("../../../utils/EventUtils");

var _ContextMenu = require("../../structures/ContextMenu");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function canCancel(eventStatus) {
  return eventStatus === _matrixJsSdk.EventStatus.QUEUED || eventStatus === _matrixJsSdk.EventStatus.NOT_SENT;
}

var _default = (0, _createReactClass.default)({
  displayName: 'MessageContextMenu',
  propTypes: {
    /* the MatrixEvent associated with the context menu */
    mxEvent: _propTypes.default.object.isRequired,

    /* an optional EventTileOps implementation that can be used to unhide preview widgets */
    eventTileOps: _propTypes.default.object,

    /* an optional function to be called when the user clicks collapse thread, if not provided hide button */
    collapseReplyThread: _propTypes.default.func,

    /* callback called when the menu is dismissed */
    onFinished: _propTypes.default.func
  },
  getInitialState: function () {
    return {
      canRedact: false,
      canPin: false
    };
  },
  componentDidMount: function () {
    _MatrixClientPeg.MatrixClientPeg.get().on('RoomMember.powerLevel', this._checkPermissions);

    this._checkPermissions();
  },
  componentWillUnmount: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (cli) {
      cli.removeListener('RoomMember.powerLevel', this._checkPermissions);
    }
  },
  _checkPermissions: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const room = cli.getRoom(this.props.mxEvent.getRoomId());
    const canRedact = room.currentState.maySendRedactionForEvent(this.props.mxEvent, cli.credentials.userId);
    let canPin = room.currentState.mayClientSendStateEvent('m.room.pinned_events', cli); // HACK: Intentionally say we can't pin if the user doesn't want to use the functionality

    if (!_SettingsStore.default.isFeatureEnabled("feature_pinning")) canPin = false;
    this.setState({
      canRedact,
      canPin
    });
  },
  _isPinned: function () {
    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.mxEvent.getRoomId());

    const pinnedEvent = room.currentState.getStateEvents('m.room.pinned_events', '');
    if (!pinnedEvent) return false;
    const content = pinnedEvent.getContent();
    return content.pinned && Array.isArray(content.pinned) && content.pinned.includes(this.props.mxEvent.getId());
  },
  onResendClick: function () {
    _Resend.default.resend(this.props.mxEvent);

    this.closeMenu();
  },
  onResendEditClick: function () {
    _Resend.default.resend(this.props.mxEvent.replacingEvent());

    this.closeMenu();
  },
  onResendRedactionClick: function () {
    _Resend.default.resend(this.props.mxEvent.localRedactionEvent());

    this.closeMenu();
  },
  onResendReactionsClick: function () {
    for (const reaction of this._getUnsentReactions()) {
      _Resend.default.resend(reaction);
    }

    this.closeMenu();
  },
  e2eInfoClicked: function () {
    this.props.e2eInfoCallback();
    this.closeMenu();
  },
  onReportEventClick: function () {
    const ReportEventDialog = sdk.getComponent("dialogs.ReportEventDialog");

    _Modal.default.createTrackedDialog('Report Event', '', ReportEventDialog, {
      mxEvent: this.props.mxEvent
    }, 'mx_Dialog_reportEvent');

    this.closeMenu();
  },
  onViewSourceClick: function () {
    const ViewSource = sdk.getComponent('structures.ViewSource');

    _Modal.default.createTrackedDialog('View Event Source', '', ViewSource, {
      roomId: this.props.mxEvent.getRoomId(),
      eventId: this.props.mxEvent.getId(),
      content: this.props.mxEvent.event
    }, 'mx_Dialog_viewsource');

    this.closeMenu();
  },
  onViewClearSourceClick: function () {
    const ViewSource = sdk.getComponent('structures.ViewSource');

    _Modal.default.createTrackedDialog('View Clear Event Source', '', ViewSource, {
      roomId: this.props.mxEvent.getRoomId(),
      eventId: this.props.mxEvent.getId(),
      // FIXME: _clearEvent is private
      content: this.props.mxEvent._clearEvent
    }, 'mx_Dialog_viewsource');

    this.closeMenu();
  },
  onRedactClick: function () {
    const ConfirmRedactDialog = sdk.getComponent("dialogs.ConfirmRedactDialog");

    _Modal.default.createTrackedDialog('Confirm Redact Dialog', '', ConfirmRedactDialog, {
      onFinished: async proceed => {
        if (!proceed) return;

        const cli = _MatrixClientPeg.MatrixClientPeg.get();

        try {
          await cli.redactEvent(this.props.mxEvent.getRoomId(), this.props.mxEvent.getId());
        } catch (e) {
          const code = e.errcode || e.statusCode; // only show the dialog if failing for something other than a network error
          // (e.g. no errcode or statusCode) as in that case the redactions end up in the
          // detached queue and we show the room status bar to allow retry

          if (typeof code !== "undefined") {
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog"); // display error message stating you couldn't delete this.

            _Modal.default.createTrackedDialog('You cannot delete this message', '', ErrorDialog, {
              title: (0, _languageHandler._t)('Error'),
              description: (0, _languageHandler._t)('You cannot delete this message. (%(code)s)', {
                code
              })
            });
          }
        }
      }
    }, 'mx_Dialog_confirmredact');

    this.closeMenu();
  },
  onCancelSendClick: function () {
    const mxEvent = this.props.mxEvent;
    const editEvent = mxEvent.replacingEvent();
    const redactEvent = mxEvent.localRedactionEvent();

    const pendingReactions = this._getPendingReactions();

    if (editEvent && canCancel(editEvent.status)) {
      _Resend.default.removeFromQueue(editEvent);
    }

    if (redactEvent && canCancel(redactEvent.status)) {
      _Resend.default.removeFromQueue(redactEvent);
    }

    if (pendingReactions.length) {
      for (const reaction of pendingReactions) {
        _Resend.default.removeFromQueue(reaction);
      }
    }

    if (canCancel(mxEvent.status)) {
      _Resend.default.removeFromQueue(this.props.mxEvent);
    }

    this.closeMenu();
  },
  onForwardClick: function () {
    _dispatcher.default.dispatch({
      action: 'forward_event',
      event: this.props.mxEvent
    });

    this.closeMenu();
  },
  onPinClick: function () {
    _MatrixClientPeg.MatrixClientPeg.get().getStateEvent(this.props.mxEvent.getRoomId(), 'm.room.pinned_events', '').catch(e => {
      // Intercept the Event Not Found error and fall through the promise chain with no event.
      if (e.errcode === "M_NOT_FOUND") return null;
      throw e;
    }).then(event => {
      const eventIds = (event ? event.pinned : []) || [];

      if (!eventIds.includes(this.props.mxEvent.getId())) {
        // Not pinned - add
        eventIds.push(this.props.mxEvent.getId());
      } else {
        // Pinned - remove
        eventIds.splice(eventIds.indexOf(this.props.mxEvent.getId()), 1);
      }

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      cli.sendStateEvent(this.props.mxEvent.getRoomId(), 'm.room.pinned_events', {
        pinned: eventIds
      }, '');
    });

    this.closeMenu();
  },
  closeMenu: function () {
    if (this.props.onFinished) this.props.onFinished();
  },
  onUnhidePreviewClick: function () {
    if (this.props.eventTileOps) {
      this.props.eventTileOps.unhideWidget();
    }

    this.closeMenu();
  },
  onQuoteClick: function () {
    _dispatcher.default.dispatch({
      action: 'quote',
      event: this.props.mxEvent
    });

    this.closeMenu();
  },
  onPermalinkClick: function (e
  /*: Event*/
  ) {
    e.preventDefault();
    const ShareDialog = sdk.getComponent("dialogs.ShareDialog");

    _Modal.default.createTrackedDialog('share room message dialog', '', ShareDialog, {
      target: this.props.mxEvent,
      permalinkCreator: this.props.permalinkCreator
    });

    this.closeMenu();
  },
  onCollapseReplyThreadClick: function () {
    this.props.collapseReplyThread();
    this.closeMenu();
  },

  _getReactions(filter) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const room = cli.getRoom(this.props.mxEvent.getRoomId());
    const eventId = this.props.mxEvent.getId();
    return room.getPendingEvents().filter(e => {
      const relation = e.getRelation();
      return relation && relation.rel_type === "m.annotation" && relation.event_id === eventId && filter(e);
    });
  },

  _getPendingReactions() {
    return this._getReactions(e => canCancel(e.status));
  },

  _getUnsentReactions() {
    return this._getReactions(e => e.status === _matrixJsSdk.EventStatus.NOT_SENT);
  },

  render: function () {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const me = cli.getUserId();
    const mxEvent = this.props.mxEvent;
    const eventStatus = mxEvent.status;
    const editStatus = mxEvent.replacingEvent() && mxEvent.replacingEvent().status;
    const redactStatus = mxEvent.localRedactionEvent() && mxEvent.localRedactionEvent().status;

    const unsentReactionsCount = this._getUnsentReactions().length;

    const pendingReactionsCount = this._getPendingReactions().length;

    const allowCancel = canCancel(mxEvent.status) || canCancel(editStatus) || canCancel(redactStatus) || pendingReactionsCount !== 0;
    let resendButton;
    let resendEditButton;
    let resendReactionsButton;
    let resendRedactionButton;
    let redactButton;
    let cancelButton;
    let forwardButton;
    let pinButton;
    let viewClearSourceButton;
    let unhidePreviewButton;
    let externalURLButton;
    let quoteButton;
    let collapseReplyThread; // status is SENT before remote-echo, null after

    const isSent = !eventStatus || eventStatus === _matrixJsSdk.EventStatus.SENT;

    if (!mxEvent.isRedacted()) {
      if (eventStatus === _matrixJsSdk.EventStatus.NOT_SENT) {
        resendButton = _react.default.createElement(_ContextMenu.MenuItem, {
          className: "mx_MessageContextMenu_field",
          onClick: this.onResendClick
        }, (0, _languageHandler._t)('Resend'));
      }

      if (editStatus === _matrixJsSdk.EventStatus.NOT_SENT) {
        resendEditButton = _react.default.createElement(_ContextMenu.MenuItem, {
          className: "mx_MessageContextMenu_field",
          onClick: this.onResendEditClick
        }, (0, _languageHandler._t)('Resend edit'));
      }

      if (unsentReactionsCount !== 0) {
        resendReactionsButton = _react.default.createElement(_ContextMenu.MenuItem, {
          className: "mx_MessageContextMenu_field",
          onClick: this.onResendReactionsClick
        }, (0, _languageHandler._t)('Resend %(unsentCount)s reaction(s)', {
          unsentCount: unsentReactionsCount
        }));
      }
    }

    if (redactStatus === _matrixJsSdk.EventStatus.NOT_SENT) {
      resendRedactionButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onResendRedactionClick
      }, (0, _languageHandler._t)('Resend removal'));
    }

    if (isSent && this.state.canRedact) {
      redactButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onRedactClick
      }, (0, _languageHandler._t)('Remove'));
    }

    if (allowCancel) {
      cancelButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onCancelSendClick
      }, (0, _languageHandler._t)('Cancel Sending'));
    }

    if ((0, _EventUtils.isContentActionable)(mxEvent)) {
      forwardButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onForwardClick
      }, (0, _languageHandler._t)('Forward Message'));

      if (this.state.canPin) {
        pinButton = _react.default.createElement(_ContextMenu.MenuItem, {
          className: "mx_MessageContextMenu_field",
          onClick: this.onPinClick
        }, this._isPinned() ? (0, _languageHandler._t)('Unpin Message') : (0, _languageHandler._t)('Pin Message'));
      }
    }

    const viewSourceButton = _react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_MessageContextMenu_field",
      onClick: this.onViewSourceClick
    }, (0, _languageHandler._t)('View Source'));

    if (mxEvent.getType() !== mxEvent.getWireType()) {
      viewClearSourceButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onViewClearSourceClick
      }, (0, _languageHandler._t)('View Decrypted Source'));
    }

    if (this.props.eventTileOps) {
      if (this.props.eventTileOps.isWidgetHidden()) {
        unhidePreviewButton = _react.default.createElement(_ContextMenu.MenuItem, {
          className: "mx_MessageContextMenu_field",
          onClick: this.onUnhidePreviewClick
        }, (0, _languageHandler._t)('Unhide Preview'));
      }
    }

    let permalink;

    if (this.props.permalinkCreator) {
      permalink = this.props.permalinkCreator.forEvent(this.props.mxEvent.getId());
    } // XXX: if we use room ID, we should also include a server where the event can be found (other than in the domain of the event ID)


    const permalinkButton = _react.default.createElement(_ContextMenu.MenuItem, {
      element: "a",
      className: "mx_MessageContextMenu_field",
      onClick: this.onPermalinkClick,
      href: permalink,
      target: "_blank",
      rel: "noreferrer noopener"
    }, mxEvent.isRedacted() || mxEvent.getType() !== 'm.room.message' ? (0, _languageHandler._t)('Share Permalink') : (0, _languageHandler._t)('Share Message'));

    if (this.props.eventTileOps) {
      // this event is rendered using TextualBody
      quoteButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onQuoteClick
      }, (0, _languageHandler._t)('Quote'));
    } // Bridges can provide a 'external_url' to link back to the source.


    if (typeof mxEvent.event.content.external_url === "string" && (0, _HtmlUtils.isUrlPermitted)(mxEvent.event.content.external_url)) {
      externalURLButton = _react.default.createElement(_ContextMenu.MenuItem, {
        element: "a",
        className: "mx_MessageContextMenu_field",
        target: "_blank",
        rel: "noreferrer noopener",
        onClick: this.closeMenu,
        href: mxEvent.event.content.external_url
      }, (0, _languageHandler._t)('Source URL'));
    }

    if (this.props.collapseReplyThread) {
      collapseReplyThread = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onCollapseReplyThreadClick
      }, (0, _languageHandler._t)('Collapse Reply Thread'));
    }

    let e2eInfo;

    if (this.props.e2eInfoCallback) {
      e2eInfo = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.e2eInfoClicked
      }, (0, _languageHandler._t)('End-to-end encryption information'));
    }

    let reportEventButton;

    if (mxEvent.getSender() !== me) {
      reportEventButton = _react.default.createElement(_ContextMenu.MenuItem, {
        className: "mx_MessageContextMenu_field",
        onClick: this.onReportEventClick
      }, (0, _languageHandler._t)('Report Content'));
    }

    return _react.default.createElement("div", {
      className: "mx_MessageContextMenu"
    }, resendButton, resendEditButton, resendReactionsButton, resendRedactionButton, redactButton, cancelButton, forwardButton, pinButton, viewSourceButton, viewClearSourceButton, unhidePreviewButton, permalinkButton, quoteButton, externalURLButton, collapseReplyThread, e2eInfo, reportEventButton);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvTWVzc2FnZUNvbnRleHRNZW51LmpzIl0sIm5hbWVzIjpbImNhbkNhbmNlbCIsImV2ZW50U3RhdHVzIiwiRXZlbnRTdGF0dXMiLCJRVUVVRUQiLCJOT1RfU0VOVCIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwibXhFdmVudCIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJldmVudFRpbGVPcHMiLCJjb2xsYXBzZVJlcGx5VGhyZWFkIiwiZnVuYyIsIm9uRmluaXNoZWQiLCJnZXRJbml0aWFsU3RhdGUiLCJjYW5SZWRhY3QiLCJjYW5QaW4iLCJjb21wb25lbnREaWRNb3VudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsIm9uIiwiX2NoZWNrUGVybWlzc2lvbnMiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsImNsaSIsInJlbW92ZUxpc3RlbmVyIiwicm9vbSIsImdldFJvb20iLCJwcm9wcyIsImdldFJvb21JZCIsImN1cnJlbnRTdGF0ZSIsIm1heVNlbmRSZWRhY3Rpb25Gb3JFdmVudCIsImNyZWRlbnRpYWxzIiwidXNlcklkIiwibWF5Q2xpZW50U2VuZFN0YXRlRXZlbnQiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsInNldFN0YXRlIiwiX2lzUGlubmVkIiwicGlubmVkRXZlbnQiLCJnZXRTdGF0ZUV2ZW50cyIsImNvbnRlbnQiLCJnZXRDb250ZW50IiwicGlubmVkIiwiQXJyYXkiLCJpc0FycmF5IiwiaW5jbHVkZXMiLCJnZXRJZCIsIm9uUmVzZW5kQ2xpY2siLCJSZXNlbmQiLCJyZXNlbmQiLCJjbG9zZU1lbnUiLCJvblJlc2VuZEVkaXRDbGljayIsInJlcGxhY2luZ0V2ZW50Iiwib25SZXNlbmRSZWRhY3Rpb25DbGljayIsImxvY2FsUmVkYWN0aW9uRXZlbnQiLCJvblJlc2VuZFJlYWN0aW9uc0NsaWNrIiwicmVhY3Rpb24iLCJfZ2V0VW5zZW50UmVhY3Rpb25zIiwiZTJlSW5mb0NsaWNrZWQiLCJlMmVJbmZvQ2FsbGJhY2siLCJvblJlcG9ydEV2ZW50Q2xpY2siLCJSZXBvcnRFdmVudERpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsIm9uVmlld1NvdXJjZUNsaWNrIiwiVmlld1NvdXJjZSIsInJvb21JZCIsImV2ZW50SWQiLCJldmVudCIsIm9uVmlld0NsZWFyU291cmNlQ2xpY2siLCJfY2xlYXJFdmVudCIsIm9uUmVkYWN0Q2xpY2siLCJDb25maXJtUmVkYWN0RGlhbG9nIiwicHJvY2VlZCIsInJlZGFjdEV2ZW50IiwiZSIsImNvZGUiLCJlcnJjb2RlIiwic3RhdHVzQ29kZSIsIkVycm9yRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIm9uQ2FuY2VsU2VuZENsaWNrIiwiZWRpdEV2ZW50IiwicGVuZGluZ1JlYWN0aW9ucyIsIl9nZXRQZW5kaW5nUmVhY3Rpb25zIiwic3RhdHVzIiwicmVtb3ZlRnJvbVF1ZXVlIiwibGVuZ3RoIiwib25Gb3J3YXJkQ2xpY2siLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsIm9uUGluQ2xpY2siLCJnZXRTdGF0ZUV2ZW50IiwiY2F0Y2giLCJ0aGVuIiwiZXZlbnRJZHMiLCJwdXNoIiwic3BsaWNlIiwiaW5kZXhPZiIsInNlbmRTdGF0ZUV2ZW50Iiwib25VbmhpZGVQcmV2aWV3Q2xpY2siLCJ1bmhpZGVXaWRnZXQiLCJvblF1b3RlQ2xpY2siLCJvblBlcm1hbGlua0NsaWNrIiwicHJldmVudERlZmF1bHQiLCJTaGFyZURpYWxvZyIsInRhcmdldCIsInBlcm1hbGlua0NyZWF0b3IiLCJvbkNvbGxhcHNlUmVwbHlUaHJlYWRDbGljayIsIl9nZXRSZWFjdGlvbnMiLCJmaWx0ZXIiLCJnZXRQZW5kaW5nRXZlbnRzIiwicmVsYXRpb24iLCJnZXRSZWxhdGlvbiIsInJlbF90eXBlIiwiZXZlbnRfaWQiLCJyZW5kZXIiLCJtZSIsImdldFVzZXJJZCIsImVkaXRTdGF0dXMiLCJyZWRhY3RTdGF0dXMiLCJ1bnNlbnRSZWFjdGlvbnNDb3VudCIsInBlbmRpbmdSZWFjdGlvbnNDb3VudCIsImFsbG93Q2FuY2VsIiwicmVzZW5kQnV0dG9uIiwicmVzZW5kRWRpdEJ1dHRvbiIsInJlc2VuZFJlYWN0aW9uc0J1dHRvbiIsInJlc2VuZFJlZGFjdGlvbkJ1dHRvbiIsInJlZGFjdEJ1dHRvbiIsImNhbmNlbEJ1dHRvbiIsImZvcndhcmRCdXR0b24iLCJwaW5CdXR0b24iLCJ2aWV3Q2xlYXJTb3VyY2VCdXR0b24iLCJ1bmhpZGVQcmV2aWV3QnV0dG9uIiwiZXh0ZXJuYWxVUkxCdXR0b24iLCJxdW90ZUJ1dHRvbiIsImlzU2VudCIsIlNFTlQiLCJpc1JlZGFjdGVkIiwidW5zZW50Q291bnQiLCJzdGF0ZSIsInZpZXdTb3VyY2VCdXR0b24iLCJnZXRUeXBlIiwiZ2V0V2lyZVR5cGUiLCJpc1dpZGdldEhpZGRlbiIsInBlcm1hbGluayIsImZvckV2ZW50IiwicGVybWFsaW5rQnV0dG9uIiwiZXh0ZXJuYWxfdXJsIiwiZTJlSW5mbyIsInJlcG9ydEV2ZW50QnV0dG9uIiwiZ2V0U2VuZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFqQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxTQUFTQSxTQUFULENBQW1CQyxXQUFuQixFQUFnQztBQUM1QixTQUFPQSxXQUFXLEtBQUtDLHlCQUFZQyxNQUE1QixJQUFzQ0YsV0FBVyxLQUFLQyx5QkFBWUUsUUFBekU7QUFDSDs7ZUFFYywrQkFBaUI7QUFDNUJDLEVBQUFBLFdBQVcsRUFBRSxvQkFEZTtBQUc1QkMsRUFBQUEsU0FBUyxFQUFFO0FBQ1A7QUFDQUMsSUFBQUEsT0FBTyxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFGbkI7O0FBSVA7QUFDQUMsSUFBQUEsWUFBWSxFQUFFSCxtQkFBVUMsTUFMakI7O0FBT1A7QUFDQUcsSUFBQUEsbUJBQW1CLEVBQUVKLG1CQUFVSyxJQVJ4Qjs7QUFVUDtBQUNBQyxJQUFBQSxVQUFVLEVBQUVOLG1CQUFVSztBQVhmLEdBSGlCO0FBaUI1QkUsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxTQUFTLEVBQUUsS0FEUjtBQUVIQyxNQUFBQSxNQUFNLEVBQUU7QUFGTCxLQUFQO0FBSUgsR0F0QjJCO0FBd0I1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQkMscUNBQWdCQyxHQUFoQixHQUFzQkMsRUFBdEIsQ0FBeUIsdUJBQXpCLEVBQWtELEtBQUtDLGlCQUF2RDs7QUFDQSxTQUFLQSxpQkFBTDtBQUNILEdBM0IyQjtBQTZCNUJDLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0IsVUFBTUMsR0FBRyxHQUFHTCxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsUUFBSUksR0FBSixFQUFTO0FBQ0xBLE1BQUFBLEdBQUcsQ0FBQ0MsY0FBSixDQUFtQix1QkFBbkIsRUFBNEMsS0FBS0gsaUJBQWpEO0FBQ0g7QUFDSixHQWxDMkI7QUFvQzVCQSxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFVBQU1FLEdBQUcsR0FBR0wsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFVBQU1NLElBQUksR0FBR0YsR0FBRyxDQUFDRyxPQUFKLENBQVksS0FBS0MsS0FBTCxDQUFXckIsT0FBWCxDQUFtQnNCLFNBQW5CLEVBQVosQ0FBYjtBQUVBLFVBQU1iLFNBQVMsR0FBR1UsSUFBSSxDQUFDSSxZQUFMLENBQWtCQyx3QkFBbEIsQ0FBMkMsS0FBS0gsS0FBTCxDQUFXckIsT0FBdEQsRUFBK0RpQixHQUFHLENBQUNRLFdBQUosQ0FBZ0JDLE1BQS9FLENBQWxCO0FBQ0EsUUFBSWhCLE1BQU0sR0FBR1MsSUFBSSxDQUFDSSxZQUFMLENBQWtCSSx1QkFBbEIsQ0FBMEMsc0JBQTFDLEVBQWtFVixHQUFsRSxDQUFiLENBTDBCLENBTzFCOztBQUNBLFFBQUksQ0FBQ1csdUJBQWNDLGdCQUFkLENBQStCLGlCQUEvQixDQUFMLEVBQXdEbkIsTUFBTSxHQUFHLEtBQVQ7QUFFeEQsU0FBS29CLFFBQUwsQ0FBYztBQUFDckIsTUFBQUEsU0FBRDtBQUFZQyxNQUFBQTtBQUFaLEtBQWQ7QUFDSCxHQS9DMkI7QUFpRDVCcUIsRUFBQUEsU0FBUyxFQUFFLFlBQVc7QUFDbEIsVUFBTVosSUFBSSxHQUFHUCxpQ0FBZ0JDLEdBQWhCLEdBQXNCTyxPQUF0QixDQUE4QixLQUFLQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cc0IsU0FBbkIsRUFBOUIsQ0FBYjs7QUFDQSxVQUFNVSxXQUFXLEdBQUdiLElBQUksQ0FBQ0ksWUFBTCxDQUFrQlUsY0FBbEIsQ0FBaUMsc0JBQWpDLEVBQXlELEVBQXpELENBQXBCO0FBQ0EsUUFBSSxDQUFDRCxXQUFMLEVBQWtCLE9BQU8sS0FBUDtBQUNsQixVQUFNRSxPQUFPLEdBQUdGLFdBQVcsQ0FBQ0csVUFBWixFQUFoQjtBQUNBLFdBQU9ELE9BQU8sQ0FBQ0UsTUFBUixJQUFrQkMsS0FBSyxDQUFDQyxPQUFOLENBQWNKLE9BQU8sQ0FBQ0UsTUFBdEIsQ0FBbEIsSUFBbURGLE9BQU8sQ0FBQ0UsTUFBUixDQUFlRyxRQUFmLENBQXdCLEtBQUtsQixLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFBeEIsQ0FBMUQ7QUFDSCxHQXZEMkI7QUF5RDVCQyxFQUFBQSxhQUFhLEVBQUUsWUFBVztBQUN0QkMsb0JBQU9DLE1BQVAsQ0FBYyxLQUFLdEIsS0FBTCxDQUFXckIsT0FBekI7O0FBQ0EsU0FBSzRDLFNBQUw7QUFDSCxHQTVEMkI7QUE4RDVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCSCxvQkFBT0MsTUFBUCxDQUFjLEtBQUt0QixLQUFMLENBQVdyQixPQUFYLENBQW1COEMsY0FBbkIsRUFBZDs7QUFDQSxTQUFLRixTQUFMO0FBQ0gsR0FqRTJCO0FBbUU1QkcsRUFBQUEsc0JBQXNCLEVBQUUsWUFBVztBQUMvQkwsb0JBQU9DLE1BQVAsQ0FBYyxLQUFLdEIsS0FBTCxDQUFXckIsT0FBWCxDQUFtQmdELG1CQUFuQixFQUFkOztBQUNBLFNBQUtKLFNBQUw7QUFDSCxHQXRFMkI7QUF3RTVCSyxFQUFBQSxzQkFBc0IsRUFBRSxZQUFXO0FBQy9CLFNBQUssTUFBTUMsUUFBWCxJQUF1QixLQUFLQyxtQkFBTCxFQUF2QixFQUFtRDtBQUMvQ1Qsc0JBQU9DLE1BQVAsQ0FBY08sUUFBZDtBQUNIOztBQUNELFNBQUtOLFNBQUw7QUFDSCxHQTdFMkI7QUErRTVCUSxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QixTQUFLL0IsS0FBTCxDQUFXZ0MsZUFBWDtBQUNBLFNBQUtULFNBQUw7QUFDSCxHQWxGMkI7QUFvRjVCVSxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCLFVBQU1DLGlCQUFpQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQTFCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsY0FBMUIsRUFBMEMsRUFBMUMsRUFBOENKLGlCQUE5QyxFQUFpRTtBQUM3RHZELE1BQUFBLE9BQU8sRUFBRSxLQUFLcUIsS0FBTCxDQUFXckI7QUFEeUMsS0FBakUsRUFFRyx1QkFGSDs7QUFHQSxTQUFLNEMsU0FBTDtBQUNILEdBMUYyQjtBQTRGNUJnQixFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFVBQU1DLFVBQVUsR0FBR0wsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFuQjs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLG1CQUExQixFQUErQyxFQUEvQyxFQUFtREUsVUFBbkQsRUFBK0Q7QUFDM0RDLE1BQUFBLE1BQU0sRUFBRSxLQUFLekMsS0FBTCxDQUFXckIsT0FBWCxDQUFtQnNCLFNBQW5CLEVBRG1EO0FBRTNEeUMsTUFBQUEsT0FBTyxFQUFFLEtBQUsxQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFGa0Q7QUFHM0ROLE1BQUFBLE9BQU8sRUFBRSxLQUFLYixLQUFMLENBQVdyQixPQUFYLENBQW1CZ0U7QUFIK0IsS0FBL0QsRUFJRyxzQkFKSDs7QUFLQSxTQUFLcEIsU0FBTDtBQUNILEdBcEcyQjtBQXNHNUJxQixFQUFBQSxzQkFBc0IsRUFBRSxZQUFXO0FBQy9CLFVBQU1KLFVBQVUsR0FBR0wsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVCQUFqQixDQUFuQjs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLHlCQUExQixFQUFxRCxFQUFyRCxFQUF5REUsVUFBekQsRUFBcUU7QUFDakVDLE1BQUFBLE1BQU0sRUFBRSxLQUFLekMsS0FBTCxDQUFXckIsT0FBWCxDQUFtQnNCLFNBQW5CLEVBRHlEO0FBRWpFeUMsTUFBQUEsT0FBTyxFQUFFLEtBQUsxQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFGd0Q7QUFHakU7QUFDQU4sTUFBQUEsT0FBTyxFQUFFLEtBQUtiLEtBQUwsQ0FBV3JCLE9BQVgsQ0FBbUJrRTtBQUpxQyxLQUFyRSxFQUtHLHNCQUxIOztBQU1BLFNBQUt0QixTQUFMO0FBQ0gsR0EvRzJCO0FBaUg1QnVCLEVBQUFBLGFBQWEsRUFBRSxZQUFXO0FBQ3RCLFVBQU1DLG1CQUFtQixHQUFHWixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTVCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVEUyxtQkFBdkQsRUFBNEU7QUFDeEU3RCxNQUFBQSxVQUFVLEVBQUUsTUFBTzhELE9BQVAsSUFBbUI7QUFDM0IsWUFBSSxDQUFDQSxPQUFMLEVBQWM7O0FBRWQsY0FBTXBELEdBQUcsR0FBR0wsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFlBQUk7QUFDQSxnQkFBTUksR0FBRyxDQUFDcUQsV0FBSixDQUNGLEtBQUtqRCxLQUFMLENBQVdyQixPQUFYLENBQW1Cc0IsU0FBbkIsRUFERSxFQUVGLEtBQUtELEtBQUwsQ0FBV3JCLE9BQVgsQ0FBbUJ3QyxLQUFuQixFQUZFLENBQU47QUFJSCxTQUxELENBS0UsT0FBTytCLENBQVAsRUFBVTtBQUNSLGdCQUFNQyxJQUFJLEdBQUdELENBQUMsQ0FBQ0UsT0FBRixJQUFhRixDQUFDLENBQUNHLFVBQTVCLENBRFEsQ0FFUjtBQUNBO0FBQ0E7O0FBQ0EsY0FBSSxPQUFPRixJQUFQLEtBQWdCLFdBQXBCLEVBQWlDO0FBQzdCLGtCQUFNRyxXQUFXLEdBQUduQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCLENBRDZCLENBRTdCOztBQUNBQywyQkFBTUMsbUJBQU4sQ0FBMEIsZ0NBQTFCLEVBQTRELEVBQTVELEVBQWdFZ0IsV0FBaEUsRUFBNkU7QUFDekVDLGNBQUFBLEtBQUssRUFBRSx5QkFBRyxPQUFILENBRGtFO0FBRXpFQyxjQUFBQSxXQUFXLEVBQUUseUJBQUcsNENBQUgsRUFBaUQ7QUFBQ0wsZ0JBQUFBO0FBQUQsZUFBakQ7QUFGNEQsYUFBN0U7QUFJSDtBQUNKO0FBQ0o7QUF4QnVFLEtBQTVFLEVBeUJHLHlCQXpCSDs7QUEwQkEsU0FBSzVCLFNBQUw7QUFDSCxHQTlJMkI7QUFnSjVCa0MsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixVQUFNOUUsT0FBTyxHQUFHLEtBQUtxQixLQUFMLENBQVdyQixPQUEzQjtBQUNBLFVBQU0rRSxTQUFTLEdBQUcvRSxPQUFPLENBQUM4QyxjQUFSLEVBQWxCO0FBQ0EsVUFBTXdCLFdBQVcsR0FBR3RFLE9BQU8sQ0FBQ2dELG1CQUFSLEVBQXBCOztBQUNBLFVBQU1nQyxnQkFBZ0IsR0FBRyxLQUFLQyxvQkFBTCxFQUF6Qjs7QUFFQSxRQUFJRixTQUFTLElBQUl0RixTQUFTLENBQUNzRixTQUFTLENBQUNHLE1BQVgsQ0FBMUIsRUFBOEM7QUFDMUN4QyxzQkFBT3lDLGVBQVAsQ0FBdUJKLFNBQXZCO0FBQ0g7O0FBQ0QsUUFBSVQsV0FBVyxJQUFJN0UsU0FBUyxDQUFDNkUsV0FBVyxDQUFDWSxNQUFiLENBQTVCLEVBQWtEO0FBQzlDeEMsc0JBQU95QyxlQUFQLENBQXVCYixXQUF2QjtBQUNIOztBQUNELFFBQUlVLGdCQUFnQixDQUFDSSxNQUFyQixFQUE2QjtBQUN6QixXQUFLLE1BQU1sQyxRQUFYLElBQXVCOEIsZ0JBQXZCLEVBQXlDO0FBQ3JDdEMsd0JBQU95QyxlQUFQLENBQXVCakMsUUFBdkI7QUFDSDtBQUNKOztBQUNELFFBQUl6RCxTQUFTLENBQUNPLE9BQU8sQ0FBQ2tGLE1BQVQsQ0FBYixFQUErQjtBQUMzQnhDLHNCQUFPeUMsZUFBUCxDQUF1QixLQUFLOUQsS0FBTCxDQUFXckIsT0FBbEM7QUFDSDs7QUFDRCxTQUFLNEMsU0FBTDtBQUNILEdBcksyQjtBQXVLNUJ5QyxFQUFBQSxjQUFjLEVBQUUsWUFBVztBQUN2QkMsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsZUFEQztBQUVUeEIsTUFBQUEsS0FBSyxFQUFFLEtBQUszQyxLQUFMLENBQVdyQjtBQUZULEtBQWI7O0FBSUEsU0FBSzRDLFNBQUw7QUFDSCxHQTdLMkI7QUErSzVCNkMsRUFBQUEsVUFBVSxFQUFFLFlBQVc7QUFDbkI3RSxxQ0FBZ0JDLEdBQWhCLEdBQXNCNkUsYUFBdEIsQ0FBb0MsS0FBS3JFLEtBQUwsQ0FBV3JCLE9BQVgsQ0FBbUJzQixTQUFuQixFQUFwQyxFQUFvRSxzQkFBcEUsRUFBNEYsRUFBNUYsRUFDS3FFLEtBREwsQ0FDWXBCLENBQUQsSUFBTztBQUNWO0FBQ0EsVUFBSUEsQ0FBQyxDQUFDRSxPQUFGLEtBQWMsYUFBbEIsRUFBaUMsT0FBTyxJQUFQO0FBQ2pDLFlBQU1GLENBQU47QUFDSCxLQUxMLEVBTUtxQixJQU5MLENBTVc1QixLQUFELElBQVc7QUFDYixZQUFNNkIsUUFBUSxHQUFHLENBQUM3QixLQUFLLEdBQUdBLEtBQUssQ0FBQzVCLE1BQVQsR0FBa0IsRUFBeEIsS0FBK0IsRUFBaEQ7O0FBQ0EsVUFBSSxDQUFDeUQsUUFBUSxDQUFDdEQsUUFBVCxDQUFrQixLQUFLbEIsS0FBTCxDQUFXckIsT0FBWCxDQUFtQndDLEtBQW5CLEVBQWxCLENBQUwsRUFBb0Q7QUFDaEQ7QUFDQXFELFFBQUFBLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjLEtBQUt6RSxLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFBZDtBQUNILE9BSEQsTUFHTztBQUNIO0FBQ0FxRCxRQUFBQSxRQUFRLENBQUNFLE1BQVQsQ0FBZ0JGLFFBQVEsQ0FBQ0csT0FBVCxDQUFpQixLQUFLM0UsS0FBTCxDQUFXckIsT0FBWCxDQUFtQndDLEtBQW5CLEVBQWpCLENBQWhCLEVBQThELENBQTlEO0FBQ0g7O0FBRUQsWUFBTXZCLEdBQUcsR0FBR0wsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBSSxNQUFBQSxHQUFHLENBQUNnRixjQUFKLENBQW1CLEtBQUs1RSxLQUFMLENBQVdyQixPQUFYLENBQW1Cc0IsU0FBbkIsRUFBbkIsRUFBbUQsc0JBQW5ELEVBQTJFO0FBQUNjLFFBQUFBLE1BQU0sRUFBRXlEO0FBQVQsT0FBM0UsRUFBK0YsRUFBL0Y7QUFDSCxLQWxCTDs7QUFtQkEsU0FBS2pELFNBQUw7QUFDSCxHQXBNMkI7QUFzTTVCQSxFQUFBQSxTQUFTLEVBQUUsWUFBVztBQUNsQixRQUFJLEtBQUt2QixLQUFMLENBQVdkLFVBQWYsRUFBMkIsS0FBS2MsS0FBTCxDQUFXZCxVQUFYO0FBQzlCLEdBeE0yQjtBQTBNNUIyRixFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFFBQUksS0FBSzdFLEtBQUwsQ0FBV2pCLFlBQWYsRUFBNkI7QUFDekIsV0FBS2lCLEtBQUwsQ0FBV2pCLFlBQVgsQ0FBd0IrRixZQUF4QjtBQUNIOztBQUNELFNBQUt2RCxTQUFMO0FBQ0gsR0EvTTJCO0FBaU41QndELEVBQUFBLFlBQVksRUFBRSxZQUFXO0FBQ3JCZCx3QkFBSUMsUUFBSixDQUFhO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRSxPQURDO0FBRVR4QixNQUFBQSxLQUFLLEVBQUUsS0FBSzNDLEtBQUwsQ0FBV3JCO0FBRlQsS0FBYjs7QUFJQSxTQUFLNEMsU0FBTDtBQUNILEdBdk4yQjtBQXlONUJ5RCxFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTOUI7QUFBVDtBQUFBLElBQW1CO0FBQ2pDQSxJQUFBQSxDQUFDLENBQUMrQixjQUFGO0FBQ0EsVUFBTUMsV0FBVyxHQUFHL0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLDJCQUExQixFQUF1RCxFQUF2RCxFQUEyRDRDLFdBQTNELEVBQXdFO0FBQ3BFQyxNQUFBQSxNQUFNLEVBQUUsS0FBS25GLEtBQUwsQ0FBV3JCLE9BRGlEO0FBRXBFeUcsTUFBQUEsZ0JBQWdCLEVBQUUsS0FBS3BGLEtBQUwsQ0FBV29GO0FBRnVDLEtBQXhFOztBQUlBLFNBQUs3RCxTQUFMO0FBQ0gsR0FqTzJCO0FBbU81QjhELEVBQUFBLDBCQUEwQixFQUFFLFlBQVc7QUFDbkMsU0FBS3JGLEtBQUwsQ0FBV2hCLG1CQUFYO0FBQ0EsU0FBS3VDLFNBQUw7QUFDSCxHQXRPMkI7O0FBd081QitELEVBQUFBLGFBQWEsQ0FBQ0MsTUFBRCxFQUFTO0FBQ2xCLFVBQU0zRixHQUFHLEdBQUdMLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxVQUFNTSxJQUFJLEdBQUdGLEdBQUcsQ0FBQ0csT0FBSixDQUFZLEtBQUtDLEtBQUwsQ0FBV3JCLE9BQVgsQ0FBbUJzQixTQUFuQixFQUFaLENBQWI7QUFDQSxVQUFNeUMsT0FBTyxHQUFHLEtBQUsxQyxLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFBaEI7QUFDQSxXQUFPckIsSUFBSSxDQUFDMEYsZ0JBQUwsR0FBd0JELE1BQXhCLENBQStCckMsQ0FBQyxJQUFJO0FBQ3ZDLFlBQU11QyxRQUFRLEdBQUd2QyxDQUFDLENBQUN3QyxXQUFGLEVBQWpCO0FBQ0EsYUFBT0QsUUFBUSxJQUNYQSxRQUFRLENBQUNFLFFBQVQsS0FBc0IsY0FEbkIsSUFFSEYsUUFBUSxDQUFDRyxRQUFULEtBQXNCbEQsT0FGbkIsSUFHSDZDLE1BQU0sQ0FBQ3JDLENBQUQsQ0FIVjtBQUlILEtBTk0sQ0FBUDtBQU9ILEdBblAyQjs7QUFxUDVCVSxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixXQUFPLEtBQUswQixhQUFMLENBQW1CcEMsQ0FBQyxJQUFJOUUsU0FBUyxDQUFDOEUsQ0FBQyxDQUFDVyxNQUFILENBQWpDLENBQVA7QUFDSCxHQXZQMkI7O0FBeVA1Qi9CLEVBQUFBLG1CQUFtQixHQUFHO0FBQ2xCLFdBQU8sS0FBS3dELGFBQUwsQ0FBbUJwQyxDQUFDLElBQUlBLENBQUMsQ0FBQ1csTUFBRixLQUFhdkYseUJBQVlFLFFBQWpELENBQVA7QUFDSCxHQTNQMkI7O0FBNlA1QnFILEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTWpHLEdBQUcsR0FBR0wsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFVBQU1zRyxFQUFFLEdBQUdsRyxHQUFHLENBQUNtRyxTQUFKLEVBQVg7QUFDQSxVQUFNcEgsT0FBTyxHQUFHLEtBQUtxQixLQUFMLENBQVdyQixPQUEzQjtBQUNBLFVBQU1OLFdBQVcsR0FBR00sT0FBTyxDQUFDa0YsTUFBNUI7QUFDQSxVQUFNbUMsVUFBVSxHQUFHckgsT0FBTyxDQUFDOEMsY0FBUixNQUE0QjlDLE9BQU8sQ0FBQzhDLGNBQVIsR0FBeUJvQyxNQUF4RTtBQUNBLFVBQU1vQyxZQUFZLEdBQUd0SCxPQUFPLENBQUNnRCxtQkFBUixNQUFpQ2hELE9BQU8sQ0FBQ2dELG1CQUFSLEdBQThCa0MsTUFBcEY7O0FBQ0EsVUFBTXFDLG9CQUFvQixHQUFHLEtBQUtwRSxtQkFBTCxHQUEyQmlDLE1BQXhEOztBQUNBLFVBQU1vQyxxQkFBcUIsR0FBRyxLQUFLdkMsb0JBQUwsR0FBNEJHLE1BQTFEOztBQUNBLFVBQU1xQyxXQUFXLEdBQUdoSSxTQUFTLENBQUNPLE9BQU8sQ0FBQ2tGLE1BQVQsQ0FBVCxJQUNoQnpGLFNBQVMsQ0FBQzRILFVBQUQsQ0FETyxJQUVoQjVILFNBQVMsQ0FBQzZILFlBQUQsQ0FGTyxJQUdoQkUscUJBQXFCLEtBQUssQ0FIOUI7QUFJQSxRQUFJRSxZQUFKO0FBQ0EsUUFBSUMsZ0JBQUo7QUFDQSxRQUFJQyxxQkFBSjtBQUNBLFFBQUlDLHFCQUFKO0FBQ0EsUUFBSUMsWUFBSjtBQUNBLFFBQUlDLFlBQUo7QUFDQSxRQUFJQyxhQUFKO0FBQ0EsUUFBSUMsU0FBSjtBQUNBLFFBQUlDLHFCQUFKO0FBQ0EsUUFBSUMsbUJBQUo7QUFDQSxRQUFJQyxpQkFBSjtBQUNBLFFBQUlDLFdBQUo7QUFDQSxRQUFJaEksbUJBQUosQ0F6QmUsQ0EyQmY7O0FBQ0EsVUFBTWlJLE1BQU0sR0FBRyxDQUFDNUksV0FBRCxJQUFnQkEsV0FBVyxLQUFLQyx5QkFBWTRJLElBQTNEOztBQUNBLFFBQUksQ0FBQ3ZJLE9BQU8sQ0FBQ3dJLFVBQVIsRUFBTCxFQUEyQjtBQUN2QixVQUFJOUksV0FBVyxLQUFLQyx5QkFBWUUsUUFBaEMsRUFBMEM7QUFDdEM2SCxRQUFBQSxZQUFZLEdBQ1IsNkJBQUMscUJBQUQ7QUFBVSxVQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsVUFBQSxPQUFPLEVBQUUsS0FBS2pGO0FBQWhFLFdBQ00seUJBQUcsUUFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxVQUFJNEUsVUFBVSxLQUFLMUgseUJBQVlFLFFBQS9CLEVBQXlDO0FBQ3JDOEgsUUFBQUEsZ0JBQWdCLEdBQ1osNkJBQUMscUJBQUQ7QUFBVSxVQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsVUFBQSxPQUFPLEVBQUUsS0FBSzlFO0FBQWhFLFdBQ00seUJBQUcsYUFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxVQUFJMEUsb0JBQW9CLEtBQUssQ0FBN0IsRUFBZ0M7QUFDNUJLLFFBQUFBLHFCQUFxQixHQUNqQiw2QkFBQyxxQkFBRDtBQUFVLFVBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxVQUFBLE9BQU8sRUFBRSxLQUFLM0U7QUFBaEUsV0FDTSx5QkFBRyxvQ0FBSCxFQUF5QztBQUFDd0YsVUFBQUEsV0FBVyxFQUFFbEI7QUFBZCxTQUF6QyxDQUROLENBREo7QUFLSDtBQUNKOztBQUVELFFBQUlELFlBQVksS0FBSzNILHlCQUFZRSxRQUFqQyxFQUEyQztBQUN2Q2dJLE1BQUFBLHFCQUFxQixHQUNqQiw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLOUU7QUFBaEUsU0FDTSx5QkFBRyxnQkFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxRQUFJdUYsTUFBTSxJQUFJLEtBQUtJLEtBQUwsQ0FBV2pJLFNBQXpCLEVBQW9DO0FBQ2hDcUgsTUFBQUEsWUFBWSxHQUNSLDZCQUFDLHFCQUFEO0FBQVUsUUFBQSxTQUFTLEVBQUMsNkJBQXBCO0FBQWtELFFBQUEsT0FBTyxFQUFFLEtBQUszRDtBQUFoRSxTQUNNLHlCQUFHLFFBQUgsQ0FETixDQURKO0FBS0g7O0FBRUQsUUFBSXNELFdBQUosRUFBaUI7QUFDYk0sTUFBQUEsWUFBWSxHQUNSLDZCQUFDLHFCQUFEO0FBQVUsUUFBQSxTQUFTLEVBQUMsNkJBQXBCO0FBQWtELFFBQUEsT0FBTyxFQUFFLEtBQUtqRDtBQUFoRSxTQUNNLHlCQUFHLGdCQUFILENBRE4sQ0FESjtBQUtIOztBQUVELFFBQUkscUNBQW9COUUsT0FBcEIsQ0FBSixFQUFrQztBQUM5QmdJLE1BQUFBLGFBQWEsR0FDVCw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLM0M7QUFBaEUsU0FDTSx5QkFBRyxpQkFBSCxDQUROLENBREo7O0FBTUEsVUFBSSxLQUFLcUQsS0FBTCxDQUFXaEksTUFBZixFQUF1QjtBQUNuQnVILFFBQUFBLFNBQVMsR0FDTCw2QkFBQyxxQkFBRDtBQUFVLFVBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxVQUFBLE9BQU8sRUFBRSxLQUFLeEM7QUFBaEUsV0FDTSxLQUFLMUQsU0FBTCxLQUFtQix5QkFBRyxlQUFILENBQW5CLEdBQXlDLHlCQUFHLGFBQUgsQ0FEL0MsQ0FESjtBQUtIO0FBQ0o7O0FBRUQsVUFBTTRHLGdCQUFnQixHQUNsQiw2QkFBQyxxQkFBRDtBQUFVLE1BQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxNQUFBLE9BQU8sRUFBRSxLQUFLL0U7QUFBaEUsT0FDTSx5QkFBRyxhQUFILENBRE4sQ0FESjs7QUFNQSxRQUFJNUQsT0FBTyxDQUFDNEksT0FBUixPQUFzQjVJLE9BQU8sQ0FBQzZJLFdBQVIsRUFBMUIsRUFBaUQ7QUFDN0NYLE1BQUFBLHFCQUFxQixHQUNqQiw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLakU7QUFBaEUsU0FDTSx5QkFBRyx1QkFBSCxDQUROLENBREo7QUFLSDs7QUFFRCxRQUFJLEtBQUs1QyxLQUFMLENBQVdqQixZQUFmLEVBQTZCO0FBQ3pCLFVBQUksS0FBS2lCLEtBQUwsQ0FBV2pCLFlBQVgsQ0FBd0IwSSxjQUF4QixFQUFKLEVBQThDO0FBQzFDWCxRQUFBQSxtQkFBbUIsR0FDZiw2QkFBQyxxQkFBRDtBQUFVLFVBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxVQUFBLE9BQU8sRUFBRSxLQUFLakM7QUFBaEUsV0FDTSx5QkFBRyxnQkFBSCxDQUROLENBREo7QUFLSDtBQUNKOztBQUVELFFBQUk2QyxTQUFKOztBQUNBLFFBQUksS0FBSzFILEtBQUwsQ0FBV29GLGdCQUFmLEVBQWlDO0FBQzdCc0MsTUFBQUEsU0FBUyxHQUFHLEtBQUsxSCxLQUFMLENBQVdvRixnQkFBWCxDQUE0QnVDLFFBQTVCLENBQXFDLEtBQUszSCxLQUFMLENBQVdyQixPQUFYLENBQW1Cd0MsS0FBbkIsRUFBckMsQ0FBWjtBQUNILEtBMUhjLENBMkhmOzs7QUFDQSxVQUFNeUcsZUFBZSxHQUNqQiw2QkFBQyxxQkFBRDtBQUNJLE1BQUEsT0FBTyxFQUFDLEdBRFo7QUFFSSxNQUFBLFNBQVMsRUFBQyw2QkFGZDtBQUdJLE1BQUEsT0FBTyxFQUFFLEtBQUs1QyxnQkFIbEI7QUFJSSxNQUFBLElBQUksRUFBRTBDLFNBSlY7QUFLSSxNQUFBLE1BQU0sRUFBQyxRQUxYO0FBTUksTUFBQSxHQUFHLEVBQUM7QUFOUixPQVFNL0ksT0FBTyxDQUFDd0ksVUFBUixNQUF3QnhJLE9BQU8sQ0FBQzRJLE9BQVIsT0FBc0IsZ0JBQTlDLEdBQ0kseUJBQUcsaUJBQUgsQ0FESixHQUM0Qix5QkFBRyxlQUFILENBVGxDLENBREo7O0FBY0EsUUFBSSxLQUFLdkgsS0FBTCxDQUFXakIsWUFBZixFQUE2QjtBQUFFO0FBQzNCaUksTUFBQUEsV0FBVyxHQUNQLDZCQUFDLHFCQUFEO0FBQVUsUUFBQSxTQUFTLEVBQUMsNkJBQXBCO0FBQWtELFFBQUEsT0FBTyxFQUFFLEtBQUtqQztBQUFoRSxTQUNNLHlCQUFHLE9BQUgsQ0FETixDQURKO0FBS0gsS0FoSmMsQ0FrSmY7OztBQUNBLFFBQ0ksT0FBT3BHLE9BQU8sQ0FBQ2dFLEtBQVIsQ0FBYzlCLE9BQWQsQ0FBc0JnSCxZQUE3QixLQUErQyxRQUEvQyxJQUNBLCtCQUFlbEosT0FBTyxDQUFDZ0UsS0FBUixDQUFjOUIsT0FBZCxDQUFzQmdILFlBQXJDLENBRkosRUFHRTtBQUNFZCxNQUFBQSxpQkFBaUIsR0FDYiw2QkFBQyxxQkFBRDtBQUNJLFFBQUEsT0FBTyxFQUFDLEdBRFo7QUFFSSxRQUFBLFNBQVMsRUFBQyw2QkFGZDtBQUdJLFFBQUEsTUFBTSxFQUFDLFFBSFg7QUFJSSxRQUFBLEdBQUcsRUFBQyxxQkFKUjtBQUtJLFFBQUEsT0FBTyxFQUFFLEtBQUt4RixTQUxsQjtBQU1JLFFBQUEsSUFBSSxFQUFFNUMsT0FBTyxDQUFDZ0UsS0FBUixDQUFjOUIsT0FBZCxDQUFzQmdIO0FBTmhDLFNBUU0seUJBQUcsWUFBSCxDQVJOLENBREo7QUFZSDs7QUFFRCxRQUFJLEtBQUs3SCxLQUFMLENBQVdoQixtQkFBZixFQUFvQztBQUNoQ0EsTUFBQUEsbUJBQW1CLEdBQ2YsNkJBQUMscUJBQUQ7QUFBVSxRQUFBLFNBQVMsRUFBQyw2QkFBcEI7QUFBa0QsUUFBQSxPQUFPLEVBQUUsS0FBS3FHO0FBQWhFLFNBQ00seUJBQUcsdUJBQUgsQ0FETixDQURKO0FBS0g7O0FBRUQsUUFBSXlDLE9BQUo7O0FBQ0EsUUFBSSxLQUFLOUgsS0FBTCxDQUFXZ0MsZUFBZixFQUFnQztBQUM1QjhGLE1BQUFBLE9BQU8sR0FDSCw2QkFBQyxxQkFBRDtBQUFVLFFBQUEsU0FBUyxFQUFDLDZCQUFwQjtBQUFrRCxRQUFBLE9BQU8sRUFBRSxLQUFLL0Y7QUFBaEUsU0FDTSx5QkFBRyxtQ0FBSCxDQUROLENBREo7QUFLSDs7QUFFRCxRQUFJZ0csaUJBQUo7O0FBQ0EsUUFBSXBKLE9BQU8sQ0FBQ3FKLFNBQVIsT0FBd0JsQyxFQUE1QixFQUFnQztBQUM1QmlDLE1BQUFBLGlCQUFpQixHQUNiLDZCQUFDLHFCQUFEO0FBQVUsUUFBQSxTQUFTLEVBQUMsNkJBQXBCO0FBQWtELFFBQUEsT0FBTyxFQUFFLEtBQUs5RjtBQUFoRSxTQUNNLHlCQUFHLGdCQUFILENBRE4sQ0FESjtBQUtIOztBQUVELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ01vRSxZQUROLEVBRU1DLGdCQUZOLEVBR01DLHFCQUhOLEVBSU1DLHFCQUpOLEVBS01DLFlBTE4sRUFNTUMsWUFOTixFQU9NQyxhQVBOLEVBUU1DLFNBUk4sRUFTTVUsZ0JBVE4sRUFVTVQscUJBVk4sRUFXTUMsbUJBWE4sRUFZTWMsZUFaTixFQWFNWixXQWJOLEVBY01ELGlCQWROLEVBZU0vSCxtQkFmTixFQWdCTThJLE9BaEJOLEVBaUJNQyxpQkFqQk4sQ0FESjtBQXFCSDtBQWpkMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCB7RXZlbnRTdGF0dXN9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5cclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uLy4uLy4uL01vZGFsJztcclxuaW1wb3J0IFJlc2VuZCBmcm9tICcuLi8uLi8uLi9SZXNlbmQnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tICcuLi8uLi8uLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlJztcclxuaW1wb3J0IHsgaXNVcmxQZXJtaXR0ZWQgfSBmcm9tICcuLi8uLi8uLi9IdG1sVXRpbHMnO1xyXG5pbXBvcnQgeyBpc0NvbnRlbnRBY3Rpb25hYmxlIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvRXZlbnRVdGlscyc7XHJcbmltcG9ydCB7TWVudUl0ZW19IGZyb20gXCIuLi8uLi9zdHJ1Y3R1cmVzL0NvbnRleHRNZW51XCI7XHJcblxyXG5mdW5jdGlvbiBjYW5DYW5jZWwoZXZlbnRTdGF0dXMpIHtcclxuICAgIHJldHVybiBldmVudFN0YXR1cyA9PT0gRXZlbnRTdGF0dXMuUVVFVUVEIHx8IGV2ZW50U3RhdHVzID09PSBFdmVudFN0YXR1cy5OT1RfU0VOVDtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBkaXNwbGF5TmFtZTogJ01lc3NhZ2VDb250ZXh0TWVudScsXHJcblxyXG4gICAgcHJvcFR5cGVzOiB7XHJcbiAgICAgICAgLyogdGhlIE1hdHJpeEV2ZW50IGFzc29jaWF0ZWQgd2l0aCB0aGUgY29udGV4dCBtZW51ICovXHJcbiAgICAgICAgbXhFdmVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxyXG5cclxuICAgICAgICAvKiBhbiBvcHRpb25hbCBFdmVudFRpbGVPcHMgaW1wbGVtZW50YXRpb24gdGhhdCBjYW4gYmUgdXNlZCB0byB1bmhpZGUgcHJldmlldyB3aWRnZXRzICovXHJcbiAgICAgICAgZXZlbnRUaWxlT3BzOiBQcm9wVHlwZXMub2JqZWN0LFxyXG5cclxuICAgICAgICAvKiBhbiBvcHRpb25hbCBmdW5jdGlvbiB0byBiZSBjYWxsZWQgd2hlbiB0aGUgdXNlciBjbGlja3MgY29sbGFwc2UgdGhyZWFkLCBpZiBub3QgcHJvdmlkZWQgaGlkZSBidXR0b24gKi9cclxuICAgICAgICBjb2xsYXBzZVJlcGx5VGhyZWFkOiBQcm9wVHlwZXMuZnVuYyxcclxuXHJcbiAgICAgICAgLyogY2FsbGJhY2sgY2FsbGVkIHdoZW4gdGhlIG1lbnUgaXMgZGlzbWlzc2VkICovXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY2FuUmVkYWN0OiBmYWxzZSxcclxuICAgICAgICAgICAgY2FuUGluOiBmYWxzZSxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKCdSb29tTWVtYmVyLnBvd2VyTGV2ZWwnLCB0aGlzLl9jaGVja1Blcm1pc3Npb25zKTtcclxuICAgICAgICB0aGlzLl9jaGVja1Blcm1pc3Npb25zKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKGNsaSkge1xyXG4gICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoJ1Jvb21NZW1iZXIucG93ZXJMZXZlbCcsIHRoaXMuX2NoZWNrUGVybWlzc2lvbnMpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2NoZWNrUGVybWlzc2lvbnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20odGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2FuUmVkYWN0ID0gcm9vbS5jdXJyZW50U3RhdGUubWF5U2VuZFJlZGFjdGlvbkZvckV2ZW50KHRoaXMucHJvcHMubXhFdmVudCwgY2xpLmNyZWRlbnRpYWxzLnVzZXJJZCk7XHJcbiAgICAgICAgbGV0IGNhblBpbiA9IHJvb20uY3VycmVudFN0YXRlLm1heUNsaWVudFNlbmRTdGF0ZUV2ZW50KCdtLnJvb20ucGlubmVkX2V2ZW50cycsIGNsaSk7XHJcblxyXG4gICAgICAgIC8vIEhBQ0s6IEludGVudGlvbmFsbHkgc2F5IHdlIGNhbid0IHBpbiBpZiB0aGUgdXNlciBkb2Vzbid0IHdhbnQgdG8gdXNlIHRoZSBmdW5jdGlvbmFsaXR5XHJcbiAgICAgICAgaWYgKCFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX3Bpbm5pbmdcIikpIGNhblBpbiA9IGZhbHNlO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtjYW5SZWRhY3QsIGNhblBpbn0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaXNQaW5uZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbSh0aGlzLnByb3BzLm14RXZlbnQuZ2V0Um9vbUlkKCkpO1xyXG4gICAgICAgIGNvbnN0IHBpbm5lZEV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5waW5uZWRfZXZlbnRzJywgJycpO1xyXG4gICAgICAgIGlmICghcGlubmVkRXZlbnQpIHJldHVybiBmYWxzZTtcclxuICAgICAgICBjb25zdCBjb250ZW50ID0gcGlubmVkRXZlbnQuZ2V0Q29udGVudCgpO1xyXG4gICAgICAgIHJldHVybiBjb250ZW50LnBpbm5lZCAmJiBBcnJheS5pc0FycmF5KGNvbnRlbnQucGlubmVkKSAmJiBjb250ZW50LnBpbm5lZC5pbmNsdWRlcyh0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVzZW5kQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIFJlc2VuZC5yZXNlbmQodGhpcy5wcm9wcy5teEV2ZW50KTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJlc2VuZEVkaXRDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgUmVzZW5kLnJlc2VuZCh0aGlzLnByb3BzLm14RXZlbnQucmVwbGFjaW5nRXZlbnQoKSk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25SZXNlbmRSZWRhY3Rpb25DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgUmVzZW5kLnJlc2VuZCh0aGlzLnByb3BzLm14RXZlbnQubG9jYWxSZWRhY3Rpb25FdmVudCgpKTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJlc2VuZFJlYWN0aW9uc0NsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBmb3IgKGNvbnN0IHJlYWN0aW9uIG9mIHRoaXMuX2dldFVuc2VudFJlYWN0aW9ucygpKSB7XHJcbiAgICAgICAgICAgIFJlc2VuZC5yZXNlbmQocmVhY3Rpb24pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBlMmVJbmZvQ2xpY2tlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5lMmVJbmZvQ2FsbGJhY2soKTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJlcG9ydEV2ZW50Q2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFJlcG9ydEV2ZW50RGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUmVwb3J0RXZlbnREaWFsb2dcIik7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnUmVwb3J0IEV2ZW50JywgJycsIFJlcG9ydEV2ZW50RGlhbG9nLCB7XHJcbiAgICAgICAgICAgIG14RXZlbnQ6IHRoaXMucHJvcHMubXhFdmVudCxcclxuICAgICAgICB9LCAnbXhfRGlhbG9nX3JlcG9ydEV2ZW50Jyk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25WaWV3U291cmNlQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFZpZXdTb3VyY2UgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlZpZXdTb3VyY2UnKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdWaWV3IEV2ZW50IFNvdXJjZScsICcnLCBWaWV3U291cmNlLCB7XHJcbiAgICAgICAgICAgIHJvb21JZDogdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpLFxyXG4gICAgICAgICAgICBldmVudElkOiB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgY29udGVudDogdGhpcy5wcm9wcy5teEV2ZW50LmV2ZW50LFxyXG4gICAgICAgIH0sICdteF9EaWFsb2dfdmlld3NvdXJjZScpO1xyXG4gICAgICAgIHRoaXMuY2xvc2VNZW51KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uVmlld0NsZWFyU291cmNlQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IFZpZXdTb3VyY2UgPSBzZGsuZ2V0Q29tcG9uZW50KCdzdHJ1Y3R1cmVzLlZpZXdTb3VyY2UnKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdWaWV3IENsZWFyIEV2ZW50IFNvdXJjZScsICcnLCBWaWV3U291cmNlLCB7XHJcbiAgICAgICAgICAgIHJvb21JZDogdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpLFxyXG4gICAgICAgICAgICBldmVudElkOiB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgLy8gRklYTUU6IF9jbGVhckV2ZW50IGlzIHByaXZhdGVcclxuICAgICAgICAgICAgY29udGVudDogdGhpcy5wcm9wcy5teEV2ZW50Ll9jbGVhckV2ZW50LFxyXG4gICAgICAgIH0sICdteF9EaWFsb2dfdmlld3NvdXJjZScpO1xyXG4gICAgICAgIHRoaXMuY2xvc2VNZW51KCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG9uUmVkYWN0Q2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IENvbmZpcm1SZWRhY3REaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5Db25maXJtUmVkYWN0RGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NvbmZpcm0gUmVkYWN0IERpYWxvZycsICcnLCBDb25maXJtUmVkYWN0RGlhbG9nLCB7XHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IGFzeW5jIChwcm9jZWVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXByb2NlZWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IGNsaS5yZWRhY3RFdmVudChcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvZGUgPSBlLmVycmNvZGUgfHwgZS5zdGF0dXNDb2RlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgc2hvdyB0aGUgZGlhbG9nIGlmIGZhaWxpbmcgZm9yIHNvbWV0aGluZyBvdGhlciB0aGFuIGEgbmV0d29yayBlcnJvclxyXG4gICAgICAgICAgICAgICAgICAgIC8vIChlLmcuIG5vIGVycmNvZGUgb3Igc3RhdHVzQ29kZSkgYXMgaW4gdGhhdCBjYXNlIHRoZSByZWRhY3Rpb25zIGVuZCB1cCBpbiB0aGVcclxuICAgICAgICAgICAgICAgICAgICAvLyBkZXRhY2hlZCBxdWV1ZSBhbmQgd2Ugc2hvdyB0aGUgcm9vbSBzdGF0dXMgYmFyIHRvIGFsbG93IHJldHJ5XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBjb2RlICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGRpc3BsYXkgZXJyb3IgbWVzc2FnZSBzdGF0aW5nIHlvdSBjb3VsZG4ndCBkZWxldGUgdGhpcy5cclxuICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnWW91IGNhbm5vdCBkZWxldGUgdGhpcyBtZXNzYWdlJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0Vycm9yJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ1lvdSBjYW5ub3QgZGVsZXRlIHRoaXMgbWVzc2FnZS4gKCUoY29kZSlzKScsIHtjb2RlfSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICB9LCAnbXhfRGlhbG9nX2NvbmZpcm1yZWRhY3QnKTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNhbmNlbFNlbmRDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgbXhFdmVudCA9IHRoaXMucHJvcHMubXhFdmVudDtcclxuICAgICAgICBjb25zdCBlZGl0RXZlbnQgPSBteEV2ZW50LnJlcGxhY2luZ0V2ZW50KCk7XHJcbiAgICAgICAgY29uc3QgcmVkYWN0RXZlbnQgPSBteEV2ZW50LmxvY2FsUmVkYWN0aW9uRXZlbnQoKTtcclxuICAgICAgICBjb25zdCBwZW5kaW5nUmVhY3Rpb25zID0gdGhpcy5fZ2V0UGVuZGluZ1JlYWN0aW9ucygpO1xyXG5cclxuICAgICAgICBpZiAoZWRpdEV2ZW50ICYmIGNhbkNhbmNlbChlZGl0RXZlbnQuc3RhdHVzKSkge1xyXG4gICAgICAgICAgICBSZXNlbmQucmVtb3ZlRnJvbVF1ZXVlKGVkaXRFdmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChyZWRhY3RFdmVudCAmJiBjYW5DYW5jZWwocmVkYWN0RXZlbnQuc3RhdHVzKSkge1xyXG4gICAgICAgICAgICBSZXNlbmQucmVtb3ZlRnJvbVF1ZXVlKHJlZGFjdEV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHBlbmRpbmdSZWFjdGlvbnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcmVhY3Rpb24gb2YgcGVuZGluZ1JlYWN0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgUmVzZW5kLnJlbW92ZUZyb21RdWV1ZShyZWFjdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGNhbkNhbmNlbChteEV2ZW50LnN0YXR1cykpIHtcclxuICAgICAgICAgICAgUmVzZW5kLnJlbW92ZUZyb21RdWV1ZSh0aGlzLnByb3BzLm14RXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkZvcndhcmRDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnZm9yd2FyZF9ldmVudCcsXHJcbiAgICAgICAgICAgIGV2ZW50OiB0aGlzLnByb3BzLm14RXZlbnQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25QaW5DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFN0YXRlRXZlbnQodGhpcy5wcm9wcy5teEV2ZW50LmdldFJvb21JZCgpLCAnbS5yb29tLnBpbm5lZF9ldmVudHMnLCAnJylcclxuICAgICAgICAgICAgLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBJbnRlcmNlcHQgdGhlIEV2ZW50IE5vdCBGb3VuZCBlcnJvciBhbmQgZmFsbCB0aHJvdWdoIHRoZSBwcm9taXNlIGNoYWluIHdpdGggbm8gZXZlbnQuXHJcbiAgICAgICAgICAgICAgICBpZiAoZS5lcnJjb2RlID09PSBcIk1fTk9UX0ZPVU5EXCIpIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudElkcyA9IChldmVudCA/IGV2ZW50LnBpbm5lZCA6IFtdKSB8fCBbXTtcclxuICAgICAgICAgICAgICAgIGlmICghZXZlbnRJZHMuaW5jbHVkZXModGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gTm90IHBpbm5lZCAtIGFkZFxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50SWRzLnB1c2godGhpcy5wcm9wcy5teEV2ZW50LmdldElkKCkpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBQaW5uZWQgLSByZW1vdmVcclxuICAgICAgICAgICAgICAgICAgICBldmVudElkcy5zcGxpY2UoZXZlbnRJZHMuaW5kZXhPZih0aGlzLnByb3BzLm14RXZlbnQuZ2V0SWQoKSksIDEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICAgICAgICAgIGNsaS5zZW5kU3RhdGVFdmVudCh0aGlzLnByb3BzLm14RXZlbnQuZ2V0Um9vbUlkKCksICdtLnJvb20ucGlubmVkX2V2ZW50cycsIHtwaW5uZWQ6IGV2ZW50SWRzfSwgJycpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjbG9zZU1lbnU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRmluaXNoZWQpIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblVuaGlkZVByZXZpZXdDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZXZlbnRUaWxlT3BzKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMuZXZlbnRUaWxlT3BzLnVuaGlkZVdpZGdldCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblF1b3RlQ2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ3F1b3RlJyxcclxuICAgICAgICAgICAgZXZlbnQ6IHRoaXMucHJvcHMubXhFdmVudCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblBlcm1hbGlua0NsaWNrOiBmdW5jdGlvbihlOiBFdmVudCkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBTaGFyZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlNoYXJlRGlhbG9nXCIpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ3NoYXJlIHJvb20gbWVzc2FnZSBkaWFsb2cnLCAnJywgU2hhcmVEaWFsb2csIHtcclxuICAgICAgICAgICAgdGFyZ2V0OiB0aGlzLnByb3BzLm14RXZlbnQsXHJcbiAgICAgICAgICAgIHBlcm1hbGlua0NyZWF0b3I6IHRoaXMucHJvcHMucGVybWFsaW5rQ3JlYXRvcixcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkNvbGxhcHNlUmVwbHlUaHJlYWRDbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5jb2xsYXBzZVJlcGx5VGhyZWFkKCk7XHJcbiAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgX2dldFJlYWN0aW9ucyhmaWx0ZXIpIHtcclxuICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IGNsaS5nZXRSb29tKHRoaXMucHJvcHMubXhFdmVudC5nZXRSb29tSWQoKSk7XHJcbiAgICAgICAgY29uc3QgZXZlbnRJZCA9IHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpO1xyXG4gICAgICAgIHJldHVybiByb29tLmdldFBlbmRpbmdFdmVudHMoKS5maWx0ZXIoZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uID0gZS5nZXRSZWxhdGlvbigpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVsYXRpb24gJiZcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uLnJlbF90eXBlID09PSBcIm0uYW5ub3RhdGlvblwiICYmXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbi5ldmVudF9pZCA9PT0gZXZlbnRJZCAmJlxyXG4gICAgICAgICAgICAgICAgZmlsdGVyKGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfZ2V0UGVuZGluZ1JlYWN0aW9ucygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZ2V0UmVhY3Rpb25zKGUgPT4gY2FuQ2FuY2VsKGUuc3RhdHVzKSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9nZXRVbnNlbnRSZWFjdGlvbnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dldFJlYWN0aW9ucyhlID0+IGUuc3RhdHVzID09PSBFdmVudFN0YXR1cy5OT1RfU0VOVCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IG1lID0gY2xpLmdldFVzZXJJZCgpO1xyXG4gICAgICAgIGNvbnN0IG14RXZlbnQgPSB0aGlzLnByb3BzLm14RXZlbnQ7XHJcbiAgICAgICAgY29uc3QgZXZlbnRTdGF0dXMgPSBteEV2ZW50LnN0YXR1cztcclxuICAgICAgICBjb25zdCBlZGl0U3RhdHVzID0gbXhFdmVudC5yZXBsYWNpbmdFdmVudCgpICYmIG14RXZlbnQucmVwbGFjaW5nRXZlbnQoKS5zdGF0dXM7XHJcbiAgICAgICAgY29uc3QgcmVkYWN0U3RhdHVzID0gbXhFdmVudC5sb2NhbFJlZGFjdGlvbkV2ZW50KCkgJiYgbXhFdmVudC5sb2NhbFJlZGFjdGlvbkV2ZW50KCkuc3RhdHVzO1xyXG4gICAgICAgIGNvbnN0IHVuc2VudFJlYWN0aW9uc0NvdW50ID0gdGhpcy5fZ2V0VW5zZW50UmVhY3Rpb25zKCkubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IHBlbmRpbmdSZWFjdGlvbnNDb3VudCA9IHRoaXMuX2dldFBlbmRpbmdSZWFjdGlvbnMoKS5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgYWxsb3dDYW5jZWwgPSBjYW5DYW5jZWwobXhFdmVudC5zdGF0dXMpIHx8XHJcbiAgICAgICAgICAgIGNhbkNhbmNlbChlZGl0U3RhdHVzKSB8fFxyXG4gICAgICAgICAgICBjYW5DYW5jZWwocmVkYWN0U3RhdHVzKSB8fFxyXG4gICAgICAgICAgICBwZW5kaW5nUmVhY3Rpb25zQ291bnQgIT09IDA7XHJcbiAgICAgICAgbGV0IHJlc2VuZEJ1dHRvbjtcclxuICAgICAgICBsZXQgcmVzZW5kRWRpdEJ1dHRvbjtcclxuICAgICAgICBsZXQgcmVzZW5kUmVhY3Rpb25zQnV0dG9uO1xyXG4gICAgICAgIGxldCByZXNlbmRSZWRhY3Rpb25CdXR0b247XHJcbiAgICAgICAgbGV0IHJlZGFjdEJ1dHRvbjtcclxuICAgICAgICBsZXQgY2FuY2VsQnV0dG9uO1xyXG4gICAgICAgIGxldCBmb3J3YXJkQnV0dG9uO1xyXG4gICAgICAgIGxldCBwaW5CdXR0b247XHJcbiAgICAgICAgbGV0IHZpZXdDbGVhclNvdXJjZUJ1dHRvbjtcclxuICAgICAgICBsZXQgdW5oaWRlUHJldmlld0J1dHRvbjtcclxuICAgICAgICBsZXQgZXh0ZXJuYWxVUkxCdXR0b247XHJcbiAgICAgICAgbGV0IHF1b3RlQnV0dG9uO1xyXG4gICAgICAgIGxldCBjb2xsYXBzZVJlcGx5VGhyZWFkO1xyXG5cclxuICAgICAgICAvLyBzdGF0dXMgaXMgU0VOVCBiZWZvcmUgcmVtb3RlLWVjaG8sIG51bGwgYWZ0ZXJcclxuICAgICAgICBjb25zdCBpc1NlbnQgPSAhZXZlbnRTdGF0dXMgfHwgZXZlbnRTdGF0dXMgPT09IEV2ZW50U3RhdHVzLlNFTlQ7XHJcbiAgICAgICAgaWYgKCFteEV2ZW50LmlzUmVkYWN0ZWQoKSkge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnRTdGF0dXMgPT09IEV2ZW50U3RhdHVzLk5PVF9TRU5UKSB7XHJcbiAgICAgICAgICAgICAgICByZXNlbmRCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25SZXNlbmRDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ1Jlc2VuZCcpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGVkaXRTdGF0dXMgPT09IEV2ZW50U3RhdHVzLk5PVF9TRU5UKSB7XHJcbiAgICAgICAgICAgICAgICByZXNlbmRFZGl0QnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIiBvbkNsaWNrPXt0aGlzLm9uUmVzZW5kRWRpdENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBfdCgnUmVzZW5kIGVkaXQnKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh1bnNlbnRSZWFjdGlvbnNDb3VudCAhPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmVzZW5kUmVhY3Rpb25zQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIiBvbkNsaWNrPXt0aGlzLm9uUmVzZW5kUmVhY3Rpb25zQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IF90KCdSZXNlbmQgJSh1bnNlbnRDb3VudClzIHJlYWN0aW9uKHMpJywge3Vuc2VudENvdW50OiB1bnNlbnRSZWFjdGlvbnNDb3VudH0pIH1cclxuICAgICAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHJlZGFjdFN0YXR1cyA9PT0gRXZlbnRTdGF0dXMuTk9UX1NFTlQpIHtcclxuICAgICAgICAgICAgcmVzZW5kUmVkYWN0aW9uQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25SZXNlbmRSZWRhY3Rpb25DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnUmVzZW5kIHJlbW92YWwnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGlzU2VudCAmJiB0aGlzLnN0YXRlLmNhblJlZGFjdCkge1xyXG4gICAgICAgICAgICByZWRhY3RCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51X2ZpZWxkXCIgb25DbGljaz17dGhpcy5vblJlZGFjdENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdSZW1vdmUnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFsbG93Q2FuY2VsKSB7XHJcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIiBvbkNsaWNrPXt0aGlzLm9uQ2FuY2VsU2VuZENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdDYW5jZWwgU2VuZGluZycpIH1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoaXNDb250ZW50QWN0aW9uYWJsZShteEV2ZW50KSkge1xyXG4gICAgICAgICAgICBmb3J3YXJkQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25Gb3J3YXJkQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoJ0ZvcndhcmQgTWVzc2FnZScpIH1cclxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5jYW5QaW4pIHtcclxuICAgICAgICAgICAgICAgIHBpbkJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51X2ZpZWxkXCIgb25DbGljaz17dGhpcy5vblBpbkNsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9pc1Bpbm5lZCgpID8gX3QoJ1VucGluIE1lc3NhZ2UnKSA6IF90KCdQaW4gTWVzc2FnZScpIH1cclxuICAgICAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgdmlld1NvdXJjZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25WaWV3U291cmNlQ2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgeyBfdCgnVmlldyBTb3VyY2UnKSB9XHJcbiAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgaWYgKG14RXZlbnQuZ2V0VHlwZSgpICE9PSBteEV2ZW50LmdldFdpcmVUeXBlKCkpIHtcclxuICAgICAgICAgICAgdmlld0NsZWFyU291cmNlQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiIG9uQ2xpY2s9e3RoaXMub25WaWV3Q2xlYXJTb3VyY2VDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnVmlldyBEZWNyeXB0ZWQgU291cmNlJykgfVxyXG4gICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmV2ZW50VGlsZU9wcykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5ldmVudFRpbGVPcHMuaXNXaWRnZXRIaWRkZW4oKSkge1xyXG4gICAgICAgICAgICAgICAgdW5oaWRlUHJldmlld0J1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51X2ZpZWxkXCIgb25DbGljaz17dGhpcy5vblVuaGlkZVByZXZpZXdDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgX3QoJ1VuaGlkZSBQcmV2aWV3JykgfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcGVybWFsaW5rO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnBlcm1hbGlua0NyZWF0b3IpIHtcclxuICAgICAgICAgICAgcGVybWFsaW5rID0gdGhpcy5wcm9wcy5wZXJtYWxpbmtDcmVhdG9yLmZvckV2ZW50KHRoaXMucHJvcHMubXhFdmVudC5nZXRJZCgpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gWFhYOiBpZiB3ZSB1c2Ugcm9vbSBJRCwgd2Ugc2hvdWxkIGFsc28gaW5jbHVkZSBhIHNlcnZlciB3aGVyZSB0aGUgZXZlbnQgY2FuIGJlIGZvdW5kIChvdGhlciB0aGFuIGluIHRoZSBkb21haW4gb2YgdGhlIGV2ZW50IElEKVxyXG4gICAgICAgIGNvbnN0IHBlcm1hbGlua0J1dHRvbiA9IChcclxuICAgICAgICAgICAgPE1lbnVJdGVtXHJcbiAgICAgICAgICAgICAgICBlbGVtZW50PVwiYVwiXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vblBlcm1hbGlua0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgaHJlZj17cGVybWFsaW5rfVxyXG4gICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcclxuICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXIgbm9vcGVuZXJcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7IG14RXZlbnQuaXNSZWRhY3RlZCgpIHx8IG14RXZlbnQuZ2V0VHlwZSgpICE9PSAnbS5yb29tLm1lc3NhZ2UnXHJcbiAgICAgICAgICAgICAgICAgICAgPyBfdCgnU2hhcmUgUGVybWFsaW5rJykgOiBfdCgnU2hhcmUgTWVzc2FnZScpIH1cclxuICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5ldmVudFRpbGVPcHMpIHsgLy8gdGhpcyBldmVudCBpcyByZW5kZXJlZCB1c2luZyBUZXh0dWFsQm9keVxyXG4gICAgICAgICAgICBxdW90ZUJ1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIiBvbkNsaWNrPXt0aGlzLm9uUXVvdGVDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnUXVvdGUnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQnJpZGdlcyBjYW4gcHJvdmlkZSBhICdleHRlcm5hbF91cmwnIHRvIGxpbmsgYmFjayB0byB0aGUgc291cmNlLlxyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgdHlwZW9mKG14RXZlbnQuZXZlbnQuY29udGVudC5leHRlcm5hbF91cmwpID09PSBcInN0cmluZ1wiICYmXHJcbiAgICAgICAgICAgIGlzVXJsUGVybWl0dGVkKG14RXZlbnQuZXZlbnQuY29udGVudC5leHRlcm5hbF91cmwpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIGV4dGVybmFsVVJMQnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudD1cImFcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X01lc3NhZ2VDb250ZXh0TWVudV9maWVsZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcclxuICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyIG5vb3BlbmVyXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmNsb3NlTWVudX1cclxuICAgICAgICAgICAgICAgICAgICBocmVmPXtteEV2ZW50LmV2ZW50LmNvbnRlbnQuZXh0ZXJuYWxfdXJsfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoJ1NvdXJjZSBVUkwnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmNvbGxhcHNlUmVwbHlUaHJlYWQpIHtcclxuICAgICAgICAgICAgY29sbGFwc2VSZXBseVRocmVhZCA9IChcclxuICAgICAgICAgICAgICAgIDxNZW51SXRlbSBjbGFzc05hbWU9XCJteF9NZXNzYWdlQ29udGV4dE1lbnVfZmllbGRcIiBvbkNsaWNrPXt0aGlzLm9uQ29sbGFwc2VSZXBseVRocmVhZENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdDb2xsYXBzZSBSZXBseSBUaHJlYWQnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGUyZUluZm87XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZTJlSW5mb0NhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgIGUyZUluZm8gPSAoXHJcbiAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51X2ZpZWxkXCIgb25DbGljaz17dGhpcy5lMmVJbmZvQ2xpY2tlZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnRW5kLXRvLWVuZCBlbmNyeXB0aW9uIGluZm9ybWF0aW9uJykgfVxyXG4gICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCByZXBvcnRFdmVudEJ1dHRvbjtcclxuICAgICAgICBpZiAobXhFdmVudC5nZXRTZW5kZXIoKSAhPT0gbWUpIHtcclxuICAgICAgICAgICAgcmVwb3J0RXZlbnRCdXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51X2ZpZWxkXCIgb25DbGljaz17dGhpcy5vblJlcG9ydEV2ZW50Q2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoJ1JlcG9ydCBDb250ZW50JykgfVxyXG4gICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWVzc2FnZUNvbnRleHRNZW51XCI+XHJcbiAgICAgICAgICAgICAgICB7IHJlc2VuZEJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IHJlc2VuZEVkaXRCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyByZXNlbmRSZWFjdGlvbnNCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyByZXNlbmRSZWRhY3Rpb25CdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyByZWRhY3RCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyBjYW5jZWxCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyBmb3J3YXJkQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgcGluQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgdmlld1NvdXJjZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IHZpZXdDbGVhclNvdXJjZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IHVuaGlkZVByZXZpZXdCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyBwZXJtYWxpbmtCdXR0b24gfVxyXG4gICAgICAgICAgICAgICAgeyBxdW90ZUJ1dHRvbiB9XHJcbiAgICAgICAgICAgICAgICB7IGV4dGVybmFsVVJMQnV0dG9uIH1cclxuICAgICAgICAgICAgICAgIHsgY29sbGFwc2VSZXBseVRocmVhZCB9XHJcbiAgICAgICAgICAgICAgICB7IGUyZUluZm8gfVxyXG4gICAgICAgICAgICAgICAgeyByZXBvcnRFdmVudEJ1dHRvbiB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19