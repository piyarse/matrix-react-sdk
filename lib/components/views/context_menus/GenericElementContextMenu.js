"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * This component can be used to display generic HTML content in a contextual
 * menu.
 */
class GenericElementContextMenu extends _react.default.Component {
  constructor(props) {
    super(props);
    this.resize = this.resize.bind(this);
  }

  componentDidMount() {
    this.resize = this.resize.bind(this);
    window.addEventListener("resize", this.resize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }

  resize() {
    if (this.props.onResize) {
      this.props.onResize();
    }
  }

  render() {
    return _react.default.createElement("div", null, this.props.element);
  }

}

exports.default = GenericElementContextMenu;
(0, _defineProperty2.default)(GenericElementContextMenu, "propTypes", {
  element: _propTypes.default.element.isRequired,
  // Function to be called when the parent window is resized
  // This can be used to reposition or close the menu on resize and
  // ensure that it is not displayed in a stale position.
  onResize: _propTypes.default.func
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvR2VuZXJpY0VsZW1lbnRDb250ZXh0TWVudS5qcyJdLCJuYW1lcyI6WyJHZW5lcmljRWxlbWVudENvbnRleHRNZW51IiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwicmVzaXplIiwiYmluZCIsImNvbXBvbmVudERpZE1vdW50Iiwid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsIm9uUmVzaXplIiwicmVuZGVyIiwiZWxlbWVudCIsIlByb3BUeXBlcyIsImlzUmVxdWlyZWQiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFqQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7QUFNZSxNQUFNQSx5QkFBTixTQUF3Q0MsZUFBTUMsU0FBOUMsQ0FBd0Q7QUFTbkVDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlDLElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNIOztBQUVEQyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixTQUFLRixNQUFMLEdBQWMsS0FBS0EsTUFBTCxDQUFZQyxJQUFaLENBQWlCLElBQWpCLENBQWQ7QUFDQUUsSUFBQUEsTUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxLQUFLSixNQUF2QztBQUNIOztBQUVESyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQkYsSUFBQUEsTUFBTSxDQUFDRyxtQkFBUCxDQUEyQixRQUEzQixFQUFxQyxLQUFLTixNQUExQztBQUNIOztBQUVEQSxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUtELEtBQUwsQ0FBV1EsUUFBZixFQUF5QjtBQUNyQixXQUFLUixLQUFMLENBQVdRLFFBQVg7QUFDSDtBQUNKOztBQUVEQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxXQUFPLDBDQUFPLEtBQUtULEtBQUwsQ0FBV1UsT0FBbEIsQ0FBUDtBQUNIOztBQS9Ca0U7Ozs4QkFBbERkLHlCLGVBQ0U7QUFDZmMsRUFBQUEsT0FBTyxFQUFFQyxtQkFBVUQsT0FBVixDQUFrQkUsVUFEWjtBQUVmO0FBQ0E7QUFDQTtBQUNBSixFQUFBQSxRQUFRLEVBQUVHLG1CQUFVRTtBQUxMLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcblxyXG4vKlxyXG4gKiBUaGlzIGNvbXBvbmVudCBjYW4gYmUgdXNlZCB0byBkaXNwbGF5IGdlbmVyaWMgSFRNTCBjb250ZW50IGluIGEgY29udGV4dHVhbFxyXG4gKiBtZW51LlxyXG4gKi9cclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHZW5lcmljRWxlbWVudENvbnRleHRNZW51IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgZWxlbWVudDogUHJvcFR5cGVzLmVsZW1lbnQuaXNSZXF1aXJlZCxcclxuICAgICAgICAvLyBGdW5jdGlvbiB0byBiZSBjYWxsZWQgd2hlbiB0aGUgcGFyZW50IHdpbmRvdyBpcyByZXNpemVkXHJcbiAgICAgICAgLy8gVGhpcyBjYW4gYmUgdXNlZCB0byByZXBvc2l0aW9uIG9yIGNsb3NlIHRoZSBtZW51IG9uIHJlc2l6ZSBhbmRcclxuICAgICAgICAvLyBlbnN1cmUgdGhhdCBpdCBpcyBub3QgZGlzcGxheWVkIGluIGEgc3RhbGUgcG9zaXRpb24uXHJcbiAgICAgICAgb25SZXNpemU6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgICB0aGlzLnJlc2l6ZSA9IHRoaXMucmVzaXplLmJpbmQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5yZXNpemUgPSB0aGlzLnJlc2l6ZS5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIHRoaXMucmVzaXplKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLnJlc2l6ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzaXplKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uUmVzaXplKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25SZXNpemUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIHJldHVybiA8ZGl2PnsgdGhpcy5wcm9wcy5lbGVtZW50IH08L2Rpdj47XHJcbiAgICB9XHJcbn1cclxuIl19