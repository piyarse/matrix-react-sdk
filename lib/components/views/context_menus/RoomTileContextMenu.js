"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _classnames = _interopRequireDefault(require("classnames"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _languageHandler = require("../../../languageHandler");

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _DMRoomMap = _interopRequireDefault(require("../../../utils/DMRoomMap"));

var Rooms = _interopRequireWildcard(require("../../../Rooms"));

var RoomNotifs = _interopRequireWildcard(require("../../../RoomNotifs"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _RoomListActions = _interopRequireDefault(require("../../../actions/RoomListActions"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _promise = require("../../../utils/promise");

var _ContextMenu = require("../../structures/ContextMenu");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const RoomTagOption = ({
  active,
  onClick,
  src,
  srcSet,
  label
}) => {
  const classes = (0, _classnames.default)('mx_RoomTileContextMenu_tag_field', {
    'mx_RoomTileContextMenu_tag_fieldSet': active,
    'mx_RoomTileContextMenu_tag_fieldDisabled': false
  });
  return _react.default.createElement(_ContextMenu.MenuItemCheckbox, {
    className: classes,
    onClick: onClick,
    active: active,
    label: label
  }, _react.default.createElement("img", {
    className: "mx_RoomTileContextMenu_tag_icon",
    src: src,
    width: "15",
    height: "15",
    alt: ""
  }), _react.default.createElement("img", {
    className: "mx_RoomTileContextMenu_tag_icon_set",
    src: srcSet,
    width: "15",
    height: "15",
    alt: ""
  }), label);
};

const NotifOption = ({
  active,
  onClick,
  src,
  label
}) => {
  const classes = (0, _classnames.default)('mx_RoomTileContextMenu_notif_field', {
    'mx_RoomTileContextMenu_notif_fieldSet': active
  });
  return _react.default.createElement(_ContextMenu.MenuItemRadio, {
    className: classes,
    onClick: onClick,
    active: active,
    label: label
  }, _react.default.createElement("img", {
    className: "mx_RoomTileContextMenu_notif_activeIcon",
    src: require("../../../../res/img/notif-active.svg"),
    width: "12",
    height: "12",
    alt: ""
  }), _react.default.createElement("img", {
    className: "mx_RoomTileContextMenu_notif_icon mx_filterFlipColor",
    src: src,
    width: "16",
    height: "12",
    alt: ""
  }), label);
};

var _default = (0, _createReactClass.default)({
  displayName: 'RoomTileContextMenu',
  propTypes: {
    room: _propTypes.default.object.isRequired,

    /* callback called when the menu is dismissed */
    onFinished: _propTypes.default.func
  },

  getInitialState() {
    const dmRoomMap = new _DMRoomMap.default(_MatrixClientPeg.MatrixClientPeg.get());
    return {
      roomNotifState: RoomNotifs.getRoomNotifsState(this.props.room.roomId),
      isFavourite: this.props.room.tags.hasOwnProperty("m.favourite"),
      isLowPriority: this.props.room.tags.hasOwnProperty("m.lowpriority"),
      isDirectMessage: Boolean(dmRoomMap.getUserIdForRoomId(this.props.room.roomId))
    };
  },

  componentDidMount: function () {
    this._unmounted = false;
  },
  componentWillUnmount: function () {
    this._unmounted = true;
  },
  _toggleTag: function (tagNameOn, tagNameOff) {
    if (!_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
      (0, _promise.sleep)(500).then(() => {
        _dispatcher.default.dispatch(_RoomListActions.default.tagRoom(_MatrixClientPeg.MatrixClientPeg.get(), this.props.room, tagNameOff, tagNameOn, undefined, 0), true);

        this.props.onFinished();
      });
    }
  },
  _onClickFavourite: function () {
    // Tag room as 'Favourite'
    if (!this.state.isFavourite && this.state.isLowPriority) {
      this.setState({
        isFavourite: true,
        isLowPriority: false
      });

      this._toggleTag("m.favourite", "m.lowpriority");
    } else if (this.state.isFavourite) {
      this.setState({
        isFavourite: false
      });

      this._toggleTag(null, "m.favourite");
    } else if (!this.state.isFavourite) {
      this.setState({
        isFavourite: true
      });

      this._toggleTag("m.favourite");
    }
  },
  _onClickLowPriority: function () {
    // Tag room as 'Low Priority'
    if (!this.state.isLowPriority && this.state.isFavourite) {
      this.setState({
        isFavourite: false,
        isLowPriority: true
      });

      this._toggleTag("m.lowpriority", "m.favourite");
    } else if (this.state.isLowPriority) {
      this.setState({
        isLowPriority: false
      });

      this._toggleTag(null, "m.lowpriority");
    } else if (!this.state.isLowPriority) {
      this.setState({
        isLowPriority: true
      });

      this._toggleTag("m.lowpriority");
    }
  },
  _onClickDM: function () {
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) return;
    const newIsDirectMessage = !this.state.isDirectMessage;
    this.setState({
      isDirectMessage: newIsDirectMessage
    });
    Rooms.guessAndSetDMRoom(this.props.room, newIsDirectMessage).then((0, _promise.sleep)(500)).finally(() => {
      // Close the context menu
      if (this.props.onFinished) {
        this.props.onFinished();
      }
    }, err => {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to set Direct Message status of room', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Failed to set Direct Message status of room'),
        description: err && err.message ? err.message : (0, _languageHandler._t)('Operation failed')
      });
    });
  },
  _onClickLeave: function () {
    // Leave room
    _dispatcher.default.dispatch({
      action: 'leave_room',
      room_id: this.props.room.roomId
    }); // Close the context menu


    if (this.props.onFinished) {
      this.props.onFinished();
    }
  },
  _onClickReject: function () {
    _dispatcher.default.dispatch({
      action: 'reject_invite',
      room_id: this.props.room.roomId
    }); // Close the context menu


    if (this.props.onFinished) {
      this.props.onFinished();
    }
  },
  _onClickForget: function () {
    // FIXME: duplicated with RoomSettings (and dead code in RoomView)
    _MatrixClientPeg.MatrixClientPeg.get().forget(this.props.room.roomId).then(() => {
      // Switch to another room view if we're currently viewing the
      // historical room
      if (_RoomViewStore.default.getRoomId() === this.props.room.roomId) {
        _dispatcher.default.dispatch({
          action: 'view_next_room'
        });
      }
    }, function (err) {
      const errCode = err.errcode || (0, _languageHandler._td)("unknown error code");
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to forget room', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Failed to forget room %(errCode)s', {
          errCode: errCode
        }),
        description: err && err.message ? err.message : (0, _languageHandler._t)('Operation failed')
      });
    }); // Close the context menu


    if (this.props.onFinished) {
      this.props.onFinished();
    }
  },
  _saveNotifState: function (newState) {
    if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) return;
    const oldState = this.state.roomNotifState;
    const roomId = this.props.room.roomId;
    this.setState({
      roomNotifState: newState
    });
    RoomNotifs.setRoomNotifsState(roomId, newState).then(() => {
      // delay slightly so that the user can see their state change
      // before closing the menu
      return (0, _promise.sleep)(500).then(() => {
        if (this._unmounted) return; // Close the context menu

        if (this.props.onFinished) {
          this.props.onFinished();
        }
      });
    }, error => {
      // TODO: some form of error notification to the user
      // to inform them that their state change failed.
      // For now we at least set the state back
      if (this._unmounted) return;
      this.setState({
        roomNotifState: oldState
      });
    });
  },
  _onClickAlertMe: function () {
    this._saveNotifState(RoomNotifs.ALL_MESSAGES_LOUD);
  },
  _onClickAllNotifs: function () {
    this._saveNotifState(RoomNotifs.ALL_MESSAGES);
  },
  _onClickMentions: function () {
    this._saveNotifState(RoomNotifs.MENTIONS_ONLY);
  },
  _onClickMute: function () {
    this._saveNotifState(RoomNotifs.MUTE);
  },
  _renderNotifMenu: function () {
    return _react.default.createElement("div", {
      className: "mx_RoomTileContextMenu",
      role: "group",
      "aria-label": (0, _languageHandler._t)("Notification settings")
    }, _react.default.createElement("div", {
      className: "mx_RoomTileContextMenu_notif_picker",
      role: "presentation"
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/notif-slider.svg"),
      width: "20",
      height: "107",
      alt: ""
    })), _react.default.createElement(NotifOption, {
      active: this.state.roomNotifState === RoomNotifs.ALL_MESSAGES_LOUD,
      label: (0, _languageHandler._t)('All messages (noisy)'),
      onClick: this._onClickAlertMe,
      src: require("../../../../res/img/icon-context-mute-off-copy.svg")
    }), _react.default.createElement(NotifOption, {
      active: this.state.roomNotifState === RoomNotifs.ALL_MESSAGES,
      label: (0, _languageHandler._t)('All messages'),
      onClick: this._onClickAllNotifs,
      src: require("../../../../res/img/icon-context-mute-off.svg")
    }), _react.default.createElement(NotifOption, {
      active: this.state.roomNotifState === RoomNotifs.MENTIONS_ONLY,
      label: (0, _languageHandler._t)('Mentions only'),
      onClick: this._onClickMentions,
      src: require("../../../../res/img/icon-context-mute-mentions.svg")
    }), _react.default.createElement(NotifOption, {
      active: this.state.roomNotifState === RoomNotifs.MUTE,
      label: (0, _languageHandler._t)('Mute'),
      onClick: this._onClickMute,
      src: require("../../../../res/img/icon-context-mute.svg")
    }));
  },
  _onClickSettings: function () {
    _dispatcher.default.dispatch({
      action: 'open_room_settings',
      room_id: this.props.room.roomId
    });

    if (this.props.onFinished) {
      this.props.onFinished();
    }
  },
  _renderSettingsMenu: function () {
    return _react.default.createElement("div", null, _react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_RoomTileContextMenu_tag_field",
      onClick: this._onClickSettings
    }, _react.default.createElement("img", {
      className: "mx_RoomTileContextMenu_tag_icon",
      src: require("../../../../res/img/feather-customised/settings.svg"),
      width: "15",
      height: "15",
      alt: ""
    }), (0, _languageHandler._t)('Settings')));
  },
  _renderLeaveMenu: function (membership) {
    if (!membership) {
      return null;
    }

    let leaveClickHandler = null;
    let leaveText = null;

    switch (membership) {
      case "join":
        leaveClickHandler = this._onClickLeave;
        leaveText = (0, _languageHandler._t)('Leave');
        break;

      case "leave":
      case "ban":
        leaveClickHandler = this._onClickForget;
        leaveText = (0, _languageHandler._t)('Forget');
        break;

      case "invite":
        leaveClickHandler = this._onClickReject;
        leaveText = (0, _languageHandler._t)('Reject');
        break;
    }

    return _react.default.createElement("div", null, _react.default.createElement(_ContextMenu.MenuItem, {
      className: "mx_RoomTileContextMenu_leave",
      onClick: leaveClickHandler
    }, _react.default.createElement("img", {
      className: "mx_RoomTileContextMenu_tag_icon",
      src: require("../../../../res/img/icon_context_delete.svg"),
      width: "15",
      height: "15",
      alt: ""
    }), leaveText));
  },
  _renderRoomTagMenu: function () {
    return _react.default.createElement("div", null, _react.default.createElement(RoomTagOption, {
      active: this.state.isFavourite,
      label: (0, _languageHandler._t)('Favourite'),
      onClick: this._onClickFavourite,
      src: require("../../../../res/img/icon_context_fave.svg"),
      srcSet: require("../../../../res/img/icon_context_fave_on.svg")
    }), _react.default.createElement(RoomTagOption, {
      active: this.state.isLowPriority,
      label: (0, _languageHandler._t)('Low Priority'),
      onClick: this._onClickLowPriority,
      src: require("../../../../res/img/icon_context_low.svg"),
      srcSet: require("../../../../res/img/icon_context_low_on.svg")
    }), _react.default.createElement(RoomTagOption, {
      active: this.state.isDirectMessage,
      label: (0, _languageHandler._t)('Direct Chat'),
      onClick: this._onClickDM,
      src: require("../../../../res/img/icon_context_person.svg"),
      srcSet: require("../../../../res/img/icon_context_person_on.svg")
    }));
  },
  render: function () {
    const myMembership = this.props.room.getMyMembership();

    switch (myMembership) {
      case 'join':
        return _react.default.createElement("div", null, this._renderNotifMenu(), _react.default.createElement("hr", {
          className: "mx_RoomTileContextMenu_separator",
          role: "separator"
        }), this._renderLeaveMenu(myMembership), _react.default.createElement("hr", {
          className: "mx_RoomTileContextMenu_separator",
          role: "separator"
        }), this._renderRoomTagMenu(), _react.default.createElement("hr", {
          className: "mx_RoomTileContextMenu_separator",
          role: "separator"
        }), this._renderSettingsMenu());

      case 'invite':
        return _react.default.createElement("div", null, this._renderLeaveMenu(myMembership));

      default:
        return _react.default.createElement("div", null, this._renderLeaveMenu(myMembership), _react.default.createElement("hr", {
          className: "mx_RoomTileContextMenu_separator",
          role: "separator"
        }), this._renderSettingsMenu());
    }
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2NvbnRleHRfbWVudXMvUm9vbVRpbGVDb250ZXh0TWVudS5qcyJdLCJuYW1lcyI6WyJSb29tVGFnT3B0aW9uIiwiYWN0aXZlIiwib25DbGljayIsInNyYyIsInNyY1NldCIsImxhYmVsIiwiY2xhc3NlcyIsIk5vdGlmT3B0aW9uIiwicmVxdWlyZSIsImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwicm9vbSIsIlByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJvbkZpbmlzaGVkIiwiZnVuYyIsImdldEluaXRpYWxTdGF0ZSIsImRtUm9vbU1hcCIsIkRNUm9vbU1hcCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInJvb21Ob3RpZlN0YXRlIiwiUm9vbU5vdGlmcyIsImdldFJvb21Ob3RpZnNTdGF0ZSIsInByb3BzIiwicm9vbUlkIiwiaXNGYXZvdXJpdGUiLCJ0YWdzIiwiaGFzT3duUHJvcGVydHkiLCJpc0xvd1ByaW9yaXR5IiwiaXNEaXJlY3RNZXNzYWdlIiwiQm9vbGVhbiIsImdldFVzZXJJZEZvclJvb21JZCIsImNvbXBvbmVudERpZE1vdW50IiwiX3VubW91bnRlZCIsImNvbXBvbmVudFdpbGxVbm1vdW50IiwiX3RvZ2dsZVRhZyIsInRhZ05hbWVPbiIsInRhZ05hbWVPZmYiLCJpc0d1ZXN0IiwidGhlbiIsImRpcyIsImRpc3BhdGNoIiwiUm9vbUxpc3RBY3Rpb25zIiwidGFnUm9vbSIsInVuZGVmaW5lZCIsIl9vbkNsaWNrRmF2b3VyaXRlIiwic3RhdGUiLCJzZXRTdGF0ZSIsIl9vbkNsaWNrTG93UHJpb3JpdHkiLCJfb25DbGlja0RNIiwibmV3SXNEaXJlY3RNZXNzYWdlIiwiUm9vbXMiLCJndWVzc0FuZFNldERNUm9vbSIsImZpbmFsbHkiLCJlcnIiLCJFcnJvckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJtZXNzYWdlIiwiX29uQ2xpY2tMZWF2ZSIsImFjdGlvbiIsInJvb21faWQiLCJfb25DbGlja1JlamVjdCIsIl9vbkNsaWNrRm9yZ2V0IiwiZm9yZ2V0IiwiUm9vbVZpZXdTdG9yZSIsImdldFJvb21JZCIsImVyckNvZGUiLCJlcnJjb2RlIiwiX3NhdmVOb3RpZlN0YXRlIiwibmV3U3RhdGUiLCJvbGRTdGF0ZSIsInNldFJvb21Ob3RpZnNTdGF0ZSIsImVycm9yIiwiX29uQ2xpY2tBbGVydE1lIiwiQUxMX01FU1NBR0VTX0xPVUQiLCJfb25DbGlja0FsbE5vdGlmcyIsIkFMTF9NRVNTQUdFUyIsIl9vbkNsaWNrTWVudGlvbnMiLCJNRU5USU9OU19PTkxZIiwiX29uQ2xpY2tNdXRlIiwiTVVURSIsIl9yZW5kZXJOb3RpZk1lbnUiLCJfb25DbGlja1NldHRpbmdzIiwiX3JlbmRlclNldHRpbmdzTWVudSIsIl9yZW5kZXJMZWF2ZU1lbnUiLCJtZW1iZXJzaGlwIiwibGVhdmVDbGlja0hhbmRsZXIiLCJsZWF2ZVRleHQiLCJfcmVuZGVyUm9vbVRhZ01lbnUiLCJyZW5kZXIiLCJteU1lbWJlcnNoaXAiLCJnZXRNeU1lbWJlcnNoaXAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBbUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQWxDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0NBLE1BQU1BLGFBQWEsR0FBRyxDQUFDO0FBQUNDLEVBQUFBLE1BQUQ7QUFBU0MsRUFBQUEsT0FBVDtBQUFrQkMsRUFBQUEsR0FBbEI7QUFBdUJDLEVBQUFBLE1BQXZCO0FBQStCQyxFQUFBQTtBQUEvQixDQUFELEtBQTJDO0FBQzdELFFBQU1DLE9BQU8sR0FBRyx5QkFBVyxrQ0FBWCxFQUErQztBQUMzRCwyQ0FBdUNMLE1BRG9CO0FBRTNELGdEQUE0QztBQUZlLEdBQS9DLENBQWhCO0FBS0EsU0FDSSw2QkFBQyw2QkFBRDtBQUFrQixJQUFBLFNBQVMsRUFBRUssT0FBN0I7QUFBc0MsSUFBQSxPQUFPLEVBQUVKLE9BQS9DO0FBQXdELElBQUEsTUFBTSxFQUFFRCxNQUFoRTtBQUF3RSxJQUFBLEtBQUssRUFBRUk7QUFBL0UsS0FDSTtBQUFLLElBQUEsU0FBUyxFQUFDLGlDQUFmO0FBQWlELElBQUEsR0FBRyxFQUFFRixHQUF0RDtBQUEyRCxJQUFBLEtBQUssRUFBQyxJQUFqRTtBQUFzRSxJQUFBLE1BQU0sRUFBQyxJQUE3RTtBQUFrRixJQUFBLEdBQUcsRUFBQztBQUF0RixJQURKLEVBRUk7QUFBSyxJQUFBLFNBQVMsRUFBQyxxQ0FBZjtBQUFxRCxJQUFBLEdBQUcsRUFBRUMsTUFBMUQ7QUFBa0UsSUFBQSxLQUFLLEVBQUMsSUFBeEU7QUFBNkUsSUFBQSxNQUFNLEVBQUMsSUFBcEY7QUFBeUYsSUFBQSxHQUFHLEVBQUM7QUFBN0YsSUFGSixFQUdNQyxLQUhOLENBREo7QUFPSCxDQWJEOztBQWVBLE1BQU1FLFdBQVcsR0FBRyxDQUFDO0FBQUNOLEVBQUFBLE1BQUQ7QUFBU0MsRUFBQUEsT0FBVDtBQUFrQkMsRUFBQUEsR0FBbEI7QUFBdUJFLEVBQUFBO0FBQXZCLENBQUQsS0FBbUM7QUFDbkQsUUFBTUMsT0FBTyxHQUFHLHlCQUFXLG9DQUFYLEVBQWlEO0FBQzdELDZDQUF5Q0w7QUFEb0IsR0FBakQsQ0FBaEI7QUFJQSxTQUNJLDZCQUFDLDBCQUFEO0FBQWUsSUFBQSxTQUFTLEVBQUVLLE9BQTFCO0FBQW1DLElBQUEsT0FBTyxFQUFFSixPQUE1QztBQUFxRCxJQUFBLE1BQU0sRUFBRUQsTUFBN0Q7QUFBcUUsSUFBQSxLQUFLLEVBQUVJO0FBQTVFLEtBQ0k7QUFBSyxJQUFBLFNBQVMsRUFBQyx5Q0FBZjtBQUF5RCxJQUFBLEdBQUcsRUFBRUcsT0FBTyxDQUFDLHNDQUFELENBQXJFO0FBQStHLElBQUEsS0FBSyxFQUFDLElBQXJIO0FBQTBILElBQUEsTUFBTSxFQUFDLElBQWpJO0FBQXNJLElBQUEsR0FBRyxFQUFDO0FBQTFJLElBREosRUFFSTtBQUFLLElBQUEsU0FBUyxFQUFDLHNEQUFmO0FBQXNFLElBQUEsR0FBRyxFQUFFTCxHQUEzRTtBQUFnRixJQUFBLEtBQUssRUFBQyxJQUF0RjtBQUEyRixJQUFBLE1BQU0sRUFBQyxJQUFsRztBQUF1RyxJQUFBLEdBQUcsRUFBQztBQUEzRyxJQUZKLEVBR01FLEtBSE4sQ0FESjtBQU9ILENBWkQ7O2VBY2UsK0JBQWlCO0FBQzVCSSxFQUFBQSxXQUFXLEVBQUUscUJBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxJQUFJLEVBQUVDLG1CQUFVQyxNQUFWLENBQWlCQyxVQURoQjs7QUFFUDtBQUNBQyxJQUFBQSxVQUFVLEVBQUVILG1CQUFVSTtBQUhmLEdBSGlCOztBQVM1QkMsRUFBQUEsZUFBZSxHQUFHO0FBQ2QsVUFBTUMsU0FBUyxHQUFHLElBQUlDLGtCQUFKLENBQWNDLGlDQUFnQkMsR0FBaEIsRUFBZCxDQUFsQjtBQUNBLFdBQU87QUFDSEMsTUFBQUEsY0FBYyxFQUFFQyxVQUFVLENBQUNDLGtCQUFYLENBQThCLEtBQUtDLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQmUsTUFBOUMsQ0FEYjtBQUVIQyxNQUFBQSxXQUFXLEVBQUUsS0FBS0YsS0FBTCxDQUFXZCxJQUFYLENBQWdCaUIsSUFBaEIsQ0FBcUJDLGNBQXJCLENBQW9DLGFBQXBDLENBRlY7QUFHSEMsTUFBQUEsYUFBYSxFQUFFLEtBQUtMLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQmlCLElBQWhCLENBQXFCQyxjQUFyQixDQUFvQyxlQUFwQyxDQUhaO0FBSUhFLE1BQUFBLGVBQWUsRUFBRUMsT0FBTyxDQUFDZCxTQUFTLENBQUNlLGtCQUFWLENBQTZCLEtBQUtSLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQmUsTUFBN0MsQ0FBRDtBQUpyQixLQUFQO0FBTUgsR0FqQjJCOztBQW1CNUJRLEVBQUFBLGlCQUFpQixFQUFFLFlBQVc7QUFDMUIsU0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNILEdBckIyQjtBQXVCNUJDLEVBQUFBLG9CQUFvQixFQUFFLFlBQVc7QUFDN0IsU0FBS0QsVUFBTCxHQUFrQixJQUFsQjtBQUNILEdBekIyQjtBQTJCNUJFLEVBQUFBLFVBQVUsRUFBRSxVQUFTQyxTQUFULEVBQW9CQyxVQUFwQixFQUFnQztBQUN4QyxRQUFJLENBQUNuQixpQ0FBZ0JDLEdBQWhCLEdBQXNCbUIsT0FBdEIsRUFBTCxFQUFzQztBQUNsQywwQkFBTSxHQUFOLEVBQVdDLElBQVgsQ0FBZ0IsTUFBTTtBQUNsQkMsNEJBQUlDLFFBQUosQ0FBYUMseUJBQWdCQyxPQUFoQixDQUNUekIsaUNBQWdCQyxHQUFoQixFQURTLEVBRVQsS0FBS0ksS0FBTCxDQUFXZCxJQUZGLEVBR1Q0QixVQUhTLEVBR0dELFNBSEgsRUFJVFEsU0FKUyxFQUlFLENBSkYsQ0FBYixFQUtHLElBTEg7O0FBT0EsYUFBS3JCLEtBQUwsQ0FBV1YsVUFBWDtBQUNILE9BVEQ7QUFVSDtBQUNKLEdBeEMyQjtBQTBDNUJnQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCO0FBQ0EsUUFBSSxDQUFDLEtBQUtDLEtBQUwsQ0FBV3JCLFdBQVosSUFBMkIsS0FBS3FCLEtBQUwsQ0FBV2xCLGFBQTFDLEVBQXlEO0FBQ3JELFdBQUttQixRQUFMLENBQWM7QUFDVnRCLFFBQUFBLFdBQVcsRUFBRSxJQURIO0FBRVZHLFFBQUFBLGFBQWEsRUFBRTtBQUZMLE9BQWQ7O0FBSUEsV0FBS08sVUFBTCxDQUFnQixhQUFoQixFQUErQixlQUEvQjtBQUNILEtBTkQsTUFNTyxJQUFJLEtBQUtXLEtBQUwsQ0FBV3JCLFdBQWYsRUFBNEI7QUFDL0IsV0FBS3NCLFFBQUwsQ0FBYztBQUFDdEIsUUFBQUEsV0FBVyxFQUFFO0FBQWQsT0FBZDs7QUFDQSxXQUFLVSxVQUFMLENBQWdCLElBQWhCLEVBQXNCLGFBQXRCO0FBQ0gsS0FITSxNQUdBLElBQUksQ0FBQyxLQUFLVyxLQUFMLENBQVdyQixXQUFoQixFQUE2QjtBQUNoQyxXQUFLc0IsUUFBTCxDQUFjO0FBQUN0QixRQUFBQSxXQUFXLEVBQUU7QUFBZCxPQUFkOztBQUNBLFdBQUtVLFVBQUwsQ0FBZ0IsYUFBaEI7QUFDSDtBQUNKLEdBekQyQjtBQTJENUJhLEVBQUFBLG1CQUFtQixFQUFFLFlBQVc7QUFDNUI7QUFDQSxRQUFJLENBQUMsS0FBS0YsS0FBTCxDQUFXbEIsYUFBWixJQUE2QixLQUFLa0IsS0FBTCxDQUFXckIsV0FBNUMsRUFBeUQ7QUFDckQsV0FBS3NCLFFBQUwsQ0FBYztBQUNWdEIsUUFBQUEsV0FBVyxFQUFFLEtBREg7QUFFVkcsUUFBQUEsYUFBYSxFQUFFO0FBRkwsT0FBZDs7QUFJQSxXQUFLTyxVQUFMLENBQWdCLGVBQWhCLEVBQWlDLGFBQWpDO0FBQ0gsS0FORCxNQU1PLElBQUksS0FBS1csS0FBTCxDQUFXbEIsYUFBZixFQUE4QjtBQUNqQyxXQUFLbUIsUUFBTCxDQUFjO0FBQUNuQixRQUFBQSxhQUFhLEVBQUU7QUFBaEIsT0FBZDs7QUFDQSxXQUFLTyxVQUFMLENBQWdCLElBQWhCLEVBQXNCLGVBQXRCO0FBQ0gsS0FITSxNQUdBLElBQUksQ0FBQyxLQUFLVyxLQUFMLENBQVdsQixhQUFoQixFQUErQjtBQUNsQyxXQUFLbUIsUUFBTCxDQUFjO0FBQUNuQixRQUFBQSxhQUFhLEVBQUU7QUFBaEIsT0FBZDs7QUFDQSxXQUFLTyxVQUFMLENBQWdCLGVBQWhCO0FBQ0g7QUFDSixHQTFFMkI7QUE0RTVCYyxFQUFBQSxVQUFVLEVBQUUsWUFBVztBQUNuQixRQUFJL0IsaUNBQWdCQyxHQUFoQixHQUFzQm1CLE9BQXRCLEVBQUosRUFBcUM7QUFFckMsVUFBTVksa0JBQWtCLEdBQUcsQ0FBQyxLQUFLSixLQUFMLENBQVdqQixlQUF2QztBQUNBLFNBQUtrQixRQUFMLENBQWM7QUFDVmxCLE1BQUFBLGVBQWUsRUFBRXFCO0FBRFAsS0FBZDtBQUlBQyxJQUFBQSxLQUFLLENBQUNDLGlCQUFOLENBQ0ksS0FBSzdCLEtBQUwsQ0FBV2QsSUFEZixFQUNxQnlDLGtCQURyQixFQUVFWCxJQUZGLENBRU8sb0JBQU0sR0FBTixDQUZQLEVBRW1CYyxPQUZuQixDQUUyQixNQUFNO0FBQzdCO0FBQ0EsVUFBSSxLQUFLOUIsS0FBTCxDQUFXVixVQUFmLEVBQTJCO0FBQ3ZCLGFBQUtVLEtBQUwsQ0FBV1YsVUFBWDtBQUNIO0FBQ0osS0FQRCxFQU9JeUMsR0FBRCxJQUFTO0FBQ1IsWUFBTUMsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsNkNBQTFCLEVBQXlFLEVBQXpFLEVBQTZFSixXQUE3RSxFQUEwRjtBQUN0RkssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLDZDQUFILENBRCtFO0FBRXRGQyxRQUFBQSxXQUFXLEVBQUlQLEdBQUcsSUFBSUEsR0FBRyxDQUFDUSxPQUFaLEdBQXVCUixHQUFHLENBQUNRLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRm1DLE9BQTFGO0FBSUgsS0FiRDtBQWNILEdBbEcyQjtBQW9HNUJDLEVBQUFBLGFBQWEsRUFBRSxZQUFXO0FBQ3RCO0FBQ0F2Qix3QkFBSUMsUUFBSixDQUFhO0FBQ1R1QixNQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUQyxNQUFBQSxPQUFPLEVBQUUsS0FBSzFDLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQmU7QUFGaEIsS0FBYixFQUZzQixDQU90Qjs7O0FBQ0EsUUFBSSxLQUFLRCxLQUFMLENBQVdWLFVBQWYsRUFBMkI7QUFDdkIsV0FBS1UsS0FBTCxDQUFXVixVQUFYO0FBQ0g7QUFDSixHQS9HMkI7QUFpSDVCcUQsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIxQix3QkFBSUMsUUFBSixDQUFhO0FBQ1R1QixNQUFBQSxNQUFNLEVBQUUsZUFEQztBQUVUQyxNQUFBQSxPQUFPLEVBQUUsS0FBSzFDLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQmU7QUFGaEIsS0FBYixFQUR1QixDQU12Qjs7O0FBQ0EsUUFBSSxLQUFLRCxLQUFMLENBQVdWLFVBQWYsRUFBMkI7QUFDdkIsV0FBS1UsS0FBTCxDQUFXVixVQUFYO0FBQ0g7QUFDSixHQTNIMkI7QUE2SDVCc0QsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkI7QUFDQWpELHFDQUFnQkMsR0FBaEIsR0FBc0JpRCxNQUF0QixDQUE2QixLQUFLN0MsS0FBTCxDQUFXZCxJQUFYLENBQWdCZSxNQUE3QyxFQUFxRGUsSUFBckQsQ0FBMEQsTUFBTTtBQUM1RDtBQUNBO0FBQ0EsVUFBSThCLHVCQUFjQyxTQUFkLE9BQThCLEtBQUsvQyxLQUFMLENBQVdkLElBQVgsQ0FBZ0JlLE1BQWxELEVBQTBEO0FBQ3REZ0IsNEJBQUlDLFFBQUosQ0FBYTtBQUFFdUIsVUFBQUEsTUFBTSxFQUFFO0FBQVYsU0FBYjtBQUNIO0FBQ0osS0FORCxFQU1HLFVBQVNWLEdBQVQsRUFBYztBQUNiLFlBQU1pQixPQUFPLEdBQUdqQixHQUFHLENBQUNrQixPQUFKLElBQWUsMEJBQUksb0JBQUosQ0FBL0I7QUFDQSxZQUFNakIsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVESixXQUF2RCxFQUFvRTtBQUNoRUssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLG1DQUFILEVBQXdDO0FBQUNXLFVBQUFBLE9BQU8sRUFBRUE7QUFBVixTQUF4QyxDQUR5RDtBQUVoRVYsUUFBQUEsV0FBVyxFQUFJUCxHQUFHLElBQUlBLEdBQUcsQ0FBQ1EsT0FBWixHQUF1QlIsR0FBRyxDQUFDUSxPQUEzQixHQUFxQyx5QkFBRyxrQkFBSDtBQUZhLE9BQXBFO0FBSUgsS0FiRCxFQUZ1QixDQWlCdkI7OztBQUNBLFFBQUksS0FBS3ZDLEtBQUwsQ0FBV1YsVUFBZixFQUEyQjtBQUN2QixXQUFLVSxLQUFMLENBQVdWLFVBQVg7QUFDSDtBQUNKLEdBbEoyQjtBQW9KNUI0RCxFQUFBQSxlQUFlLEVBQUUsVUFBU0MsUUFBVCxFQUFtQjtBQUNoQyxRQUFJeEQsaUNBQWdCQyxHQUFoQixHQUFzQm1CLE9BQXRCLEVBQUosRUFBcUM7QUFFckMsVUFBTXFDLFFBQVEsR0FBRyxLQUFLN0IsS0FBTCxDQUFXMUIsY0FBNUI7QUFDQSxVQUFNSSxNQUFNLEdBQUcsS0FBS0QsS0FBTCxDQUFXZCxJQUFYLENBQWdCZSxNQUEvQjtBQUVBLFNBQUt1QixRQUFMLENBQWM7QUFDVjNCLE1BQUFBLGNBQWMsRUFBRXNEO0FBRE4sS0FBZDtBQUdBckQsSUFBQUEsVUFBVSxDQUFDdUQsa0JBQVgsQ0FBOEJwRCxNQUE5QixFQUFzQ2tELFFBQXRDLEVBQWdEbkMsSUFBaEQsQ0FBcUQsTUFBTTtBQUN2RDtBQUNBO0FBQ0EsYUFBTyxvQkFBTSxHQUFOLEVBQVdBLElBQVgsQ0FBZ0IsTUFBTTtBQUN6QixZQUFJLEtBQUtOLFVBQVQsRUFBcUIsT0FESSxDQUV6Qjs7QUFDQSxZQUFJLEtBQUtWLEtBQUwsQ0FBV1YsVUFBZixFQUEyQjtBQUN2QixlQUFLVSxLQUFMLENBQVdWLFVBQVg7QUFDSDtBQUNKLE9BTk0sQ0FBUDtBQU9ILEtBVkQsRUFVSWdFLEtBQUQsSUFBVztBQUNWO0FBQ0E7QUFDQTtBQUNBLFVBQUksS0FBSzVDLFVBQVQsRUFBcUI7QUFDckIsV0FBS2MsUUFBTCxDQUFjO0FBQ1YzQixRQUFBQSxjQUFjLEVBQUV1RDtBQUROLE9BQWQ7QUFHSCxLQWxCRDtBQW1CSCxHQWhMMkI7QUFrTDVCRyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixTQUFLTCxlQUFMLENBQXFCcEQsVUFBVSxDQUFDMEQsaUJBQWhDO0FBQ0gsR0FwTDJCO0FBc0w1QkMsRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixTQUFLUCxlQUFMLENBQXFCcEQsVUFBVSxDQUFDNEQsWUFBaEM7QUFDSCxHQXhMMkI7QUEwTDVCQyxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCLFNBQUtULGVBQUwsQ0FBcUJwRCxVQUFVLENBQUM4RCxhQUFoQztBQUNILEdBNUwyQjtBQThMNUJDLEVBQUFBLFlBQVksRUFBRSxZQUFXO0FBQ3JCLFNBQUtYLGVBQUwsQ0FBcUJwRCxVQUFVLENBQUNnRSxJQUFoQztBQUNILEdBaE0yQjtBQWtNNUJDLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLHdCQUFmO0FBQXdDLE1BQUEsSUFBSSxFQUFDLE9BQTdDO0FBQXFELG9CQUFZLHlCQUFHLHVCQUFIO0FBQWpFLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQyxxQ0FBZjtBQUFxRCxNQUFBLElBQUksRUFBQztBQUExRCxPQUNJO0FBQUssTUFBQSxHQUFHLEVBQUVoRixPQUFPLENBQUMsc0NBQUQsQ0FBakI7QUFBMkQsTUFBQSxLQUFLLEVBQUMsSUFBakU7QUFBc0UsTUFBQSxNQUFNLEVBQUMsS0FBN0U7QUFBbUYsTUFBQSxHQUFHLEVBQUM7QUFBdkYsTUFESixDQURKLEVBS0ksNkJBQUMsV0FBRDtBQUNJLE1BQUEsTUFBTSxFQUFFLEtBQUt3QyxLQUFMLENBQVcxQixjQUFYLEtBQThCQyxVQUFVLENBQUMwRCxpQkFEckQ7QUFFSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxzQkFBSCxDQUZYO0FBR0ksTUFBQSxPQUFPLEVBQUUsS0FBS0QsZUFIbEI7QUFJSSxNQUFBLEdBQUcsRUFBRXhFLE9BQU8sQ0FBQyxvREFBRDtBQUpoQixNQUxKLEVBV0ksNkJBQUMsV0FBRDtBQUNJLE1BQUEsTUFBTSxFQUFFLEtBQUt3QyxLQUFMLENBQVcxQixjQUFYLEtBQThCQyxVQUFVLENBQUM0RCxZQURyRDtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGNBQUgsQ0FGWDtBQUdJLE1BQUEsT0FBTyxFQUFFLEtBQUtELGlCQUhsQjtBQUlJLE1BQUEsR0FBRyxFQUFFMUUsT0FBTyxDQUFDLCtDQUFEO0FBSmhCLE1BWEosRUFpQkksNkJBQUMsV0FBRDtBQUNJLE1BQUEsTUFBTSxFQUFFLEtBQUt3QyxLQUFMLENBQVcxQixjQUFYLEtBQThCQyxVQUFVLENBQUM4RCxhQURyRDtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FGWDtBQUdJLE1BQUEsT0FBTyxFQUFFLEtBQUtELGdCQUhsQjtBQUlJLE1BQUEsR0FBRyxFQUFFNUUsT0FBTyxDQUFDLG9EQUFEO0FBSmhCLE1BakJKLEVBdUJJLDZCQUFDLFdBQUQ7QUFDSSxNQUFBLE1BQU0sRUFBRSxLQUFLd0MsS0FBTCxDQUFXMUIsY0FBWCxLQUE4QkMsVUFBVSxDQUFDZ0UsSUFEckQ7QUFFSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxNQUFILENBRlg7QUFHSSxNQUFBLE9BQU8sRUFBRSxLQUFLRCxZQUhsQjtBQUlJLE1BQUEsR0FBRyxFQUFFOUUsT0FBTyxDQUFDLDJDQUFEO0FBSmhCLE1BdkJKLENBREo7QUFnQ0gsR0FuTzJCO0FBcU81QmlGLEVBQUFBLGdCQUFnQixFQUFFLFlBQVc7QUFDekIvQyx3QkFBSUMsUUFBSixDQUFhO0FBQ1R1QixNQUFBQSxNQUFNLEVBQUUsb0JBREM7QUFFVEMsTUFBQUEsT0FBTyxFQUFFLEtBQUsxQyxLQUFMLENBQVdkLElBQVgsQ0FBZ0JlO0FBRmhCLEtBQWI7O0FBSUEsUUFBSSxLQUFLRCxLQUFMLENBQVdWLFVBQWYsRUFBMkI7QUFDdkIsV0FBS1UsS0FBTCxDQUFXVixVQUFYO0FBQ0g7QUFDSixHQTdPMkI7QUErTzVCMkUsRUFBQUEsbUJBQW1CLEVBQUUsWUFBVztBQUM1QixXQUNJLDBDQUNJLDZCQUFDLHFCQUFEO0FBQVUsTUFBQSxTQUFTLEVBQUMsa0NBQXBCO0FBQXVELE1BQUEsT0FBTyxFQUFFLEtBQUtEO0FBQXJFLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQyxpQ0FBZjtBQUFpRCxNQUFBLEdBQUcsRUFBRWpGLE9BQU8sQ0FBQyxxREFBRCxDQUE3RDtBQUFzSCxNQUFBLEtBQUssRUFBQyxJQUE1SDtBQUFpSSxNQUFBLE1BQU0sRUFBQyxJQUF4STtBQUE2SSxNQUFBLEdBQUcsRUFBQztBQUFqSixNQURKLEVBRU0seUJBQUcsVUFBSCxDQUZOLENBREosQ0FESjtBQVFILEdBeFAyQjtBQTBQNUJtRixFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTQyxVQUFULEVBQXFCO0FBQ25DLFFBQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiLGFBQU8sSUFBUDtBQUNIOztBQUVELFFBQUlDLGlCQUFpQixHQUFHLElBQXhCO0FBQ0EsUUFBSUMsU0FBUyxHQUFHLElBQWhCOztBQUVBLFlBQVFGLFVBQVI7QUFDSSxXQUFLLE1BQUw7QUFDSUMsUUFBQUEsaUJBQWlCLEdBQUcsS0FBSzVCLGFBQXpCO0FBQ0E2QixRQUFBQSxTQUFTLEdBQUcseUJBQUcsT0FBSCxDQUFaO0FBQ0E7O0FBQ0osV0FBSyxPQUFMO0FBQ0EsV0FBSyxLQUFMO0FBQ0lELFFBQUFBLGlCQUFpQixHQUFHLEtBQUt4QixjQUF6QjtBQUNBeUIsUUFBQUEsU0FBUyxHQUFHLHlCQUFHLFFBQUgsQ0FBWjtBQUNBOztBQUNKLFdBQUssUUFBTDtBQUNJRCxRQUFBQSxpQkFBaUIsR0FBRyxLQUFLekIsY0FBekI7QUFDQTBCLFFBQUFBLFNBQVMsR0FBRyx5QkFBRyxRQUFILENBQVo7QUFDQTtBQWJSOztBQWdCQSxXQUNJLDBDQUNJLDZCQUFDLHFCQUFEO0FBQVUsTUFBQSxTQUFTLEVBQUMsOEJBQXBCO0FBQW1ELE1BQUEsT0FBTyxFQUFFRDtBQUE1RCxPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsaUNBQWY7QUFBaUQsTUFBQSxHQUFHLEVBQUVyRixPQUFPLENBQUMsNkNBQUQsQ0FBN0Q7QUFBOEcsTUFBQSxLQUFLLEVBQUMsSUFBcEg7QUFBeUgsTUFBQSxNQUFNLEVBQUMsSUFBaEk7QUFBcUksTUFBQSxHQUFHLEVBQUM7QUFBekksTUFESixFQUVNc0YsU0FGTixDQURKLENBREo7QUFRSCxHQTFSMkI7QUE0UjVCQyxFQUFBQSxrQkFBa0IsRUFBRSxZQUFXO0FBQzNCLFdBQ0ksMENBQ0ksNkJBQUMsYUFBRDtBQUNJLE1BQUEsTUFBTSxFQUFFLEtBQUsvQyxLQUFMLENBQVdyQixXQUR2QjtBQUVJLE1BQUEsS0FBSyxFQUFFLHlCQUFHLFdBQUgsQ0FGWDtBQUdJLE1BQUEsT0FBTyxFQUFFLEtBQUtvQixpQkFIbEI7QUFJSSxNQUFBLEdBQUcsRUFBRXZDLE9BQU8sQ0FBQywyQ0FBRCxDQUpoQjtBQUtJLE1BQUEsTUFBTSxFQUFFQSxPQUFPLENBQUMsOENBQUQ7QUFMbkIsTUFESixFQVFJLDZCQUFDLGFBQUQ7QUFDSSxNQUFBLE1BQU0sRUFBRSxLQUFLd0MsS0FBTCxDQUFXbEIsYUFEdkI7QUFFSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxjQUFILENBRlg7QUFHSSxNQUFBLE9BQU8sRUFBRSxLQUFLb0IsbUJBSGxCO0FBSUksTUFBQSxHQUFHLEVBQUUxQyxPQUFPLENBQUMsMENBQUQsQ0FKaEI7QUFLSSxNQUFBLE1BQU0sRUFBRUEsT0FBTyxDQUFDLDZDQUFEO0FBTG5CLE1BUkosRUFlSSw2QkFBQyxhQUFEO0FBQ0ksTUFBQSxNQUFNLEVBQUUsS0FBS3dDLEtBQUwsQ0FBV2pCLGVBRHZCO0FBRUksTUFBQSxLQUFLLEVBQUUseUJBQUcsYUFBSCxDQUZYO0FBR0ksTUFBQSxPQUFPLEVBQUUsS0FBS29CLFVBSGxCO0FBSUksTUFBQSxHQUFHLEVBQUUzQyxPQUFPLENBQUMsNkNBQUQsQ0FKaEI7QUFLSSxNQUFBLE1BQU0sRUFBRUEsT0FBTyxDQUFDLGdEQUFEO0FBTG5CLE1BZkosQ0FESjtBQXlCSCxHQXRUMkI7QUF3VDVCd0YsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxZQUFZLEdBQUcsS0FBS3hFLEtBQUwsQ0FBV2QsSUFBWCxDQUFnQnVGLGVBQWhCLEVBQXJCOztBQUVBLFlBQVFELFlBQVI7QUFDSSxXQUFLLE1BQUw7QUFDSSxlQUFPLDBDQUNELEtBQUtULGdCQUFMLEVBREMsRUFFSDtBQUFJLFVBQUEsU0FBUyxFQUFDLGtDQUFkO0FBQWlELFVBQUEsSUFBSSxFQUFDO0FBQXRELFVBRkcsRUFHRCxLQUFLRyxnQkFBTCxDQUFzQk0sWUFBdEIsQ0FIQyxFQUlIO0FBQUksVUFBQSxTQUFTLEVBQUMsa0NBQWQ7QUFBaUQsVUFBQSxJQUFJLEVBQUM7QUFBdEQsVUFKRyxFQUtELEtBQUtGLGtCQUFMLEVBTEMsRUFNSDtBQUFJLFVBQUEsU0FBUyxFQUFDLGtDQUFkO0FBQWlELFVBQUEsSUFBSSxFQUFDO0FBQXRELFVBTkcsRUFPRCxLQUFLTCxtQkFBTCxFQVBDLENBQVA7O0FBU0osV0FBSyxRQUFMO0FBQ0ksZUFBTywwQ0FDRCxLQUFLQyxnQkFBTCxDQUFzQk0sWUFBdEIsQ0FEQyxDQUFQOztBQUdKO0FBQ0ksZUFBTywwQ0FDRCxLQUFLTixnQkFBTCxDQUFzQk0sWUFBdEIsQ0FEQyxFQUVIO0FBQUksVUFBQSxTQUFTLEVBQUMsa0NBQWQ7QUFBaUQsVUFBQSxJQUFJLEVBQUM7QUFBdEQsVUFGRyxFQUdELEtBQUtQLG1CQUFMLEVBSEMsQ0FBUDtBQWhCUjtBQXNCSDtBQWpWMkIsQ0FBakIsQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBfdCwgX3RkIH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4uLy4uLy4uL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBETVJvb21NYXAgZnJvbSAnLi4vLi4vLi4vdXRpbHMvRE1Sb29tTWFwJztcclxuaW1wb3J0ICogYXMgUm9vbXMgZnJvbSAnLi4vLi4vLi4vUm9vbXMnO1xyXG5pbXBvcnQgKiBhcyBSb29tTm90aWZzIGZyb20gJy4uLy4uLy4uL1Jvb21Ob3RpZnMnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgUm9vbUxpc3RBY3Rpb25zIGZyb20gJy4uLy4uLy4uL2FjdGlvbnMvUm9vbUxpc3RBY3Rpb25zJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi4vLi4vLi4vc3RvcmVzL1Jvb21WaWV3U3RvcmUnO1xyXG5pbXBvcnQge3NsZWVwfSBmcm9tIFwiLi4vLi4vLi4vdXRpbHMvcHJvbWlzZVwiO1xyXG5pbXBvcnQge01lbnVJdGVtLCBNZW51SXRlbUNoZWNrYm94LCBNZW51SXRlbVJhZGlvfSBmcm9tIFwiLi4vLi4vc3RydWN0dXJlcy9Db250ZXh0TWVudVwiO1xyXG5cclxuY29uc3QgUm9vbVRhZ09wdGlvbiA9ICh7YWN0aXZlLCBvbkNsaWNrLCBzcmMsIHNyY1NldCwgbGFiZWx9KSA9PiB7XHJcbiAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcygnbXhfUm9vbVRpbGVDb250ZXh0TWVudV90YWdfZmllbGQnLCB7XHJcbiAgICAgICAgJ214X1Jvb21UaWxlQ29udGV4dE1lbnVfdGFnX2ZpZWxkU2V0JzogYWN0aXZlLFxyXG4gICAgICAgICdteF9Sb29tVGlsZUNvbnRleHRNZW51X3RhZ19maWVsZERpc2FibGVkJzogZmFsc2UsXHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxNZW51SXRlbUNoZWNrYm94IGNsYXNzTmFtZT17Y2xhc3Nlc30gb25DbGljaz17b25DbGlja30gYWN0aXZlPXthY3RpdmV9IGxhYmVsPXtsYWJlbH0+XHJcbiAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVDb250ZXh0TWVudV90YWdfaWNvblwiIHNyYz17c3JjfSB3aWR0aD1cIjE1XCIgaGVpZ2h0PVwiMTVcIiBhbHQ9XCJcIiAvPlxyXG4gICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfdGFnX2ljb25fc2V0XCIgc3JjPXtzcmNTZXR9IHdpZHRoPVwiMTVcIiBoZWlnaHQ9XCIxNVwiIGFsdD1cIlwiIC8+XHJcbiAgICAgICAgICAgIHsgbGFiZWwgfVxyXG4gICAgICAgIDwvTWVudUl0ZW1DaGVja2JveD5cclxuICAgICk7XHJcbn07XHJcblxyXG5jb25zdCBOb3RpZk9wdGlvbiA9ICh7YWN0aXZlLCBvbkNsaWNrLCBzcmMsIGxhYmVsfSkgPT4ge1xyXG4gICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoJ214X1Jvb21UaWxlQ29udGV4dE1lbnVfbm90aWZfZmllbGQnLCB7XHJcbiAgICAgICAgJ214X1Jvb21UaWxlQ29udGV4dE1lbnVfbm90aWZfZmllbGRTZXQnOiBhY3RpdmUsXHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxNZW51SXRlbVJhZGlvIGNsYXNzTmFtZT17Y2xhc3Nlc30gb25DbGljaz17b25DbGlja30gYWN0aXZlPXthY3RpdmV9IGxhYmVsPXtsYWJlbH0+XHJcbiAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVDb250ZXh0TWVudV9ub3RpZl9hY3RpdmVJY29uXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9ub3RpZi1hY3RpdmUuc3ZnXCIpfSB3aWR0aD1cIjEyXCIgaGVpZ2h0PVwiMTJcIiBhbHQ9XCJcIiAvPlxyXG4gICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfbm90aWZfaWNvbiBteF9maWx0ZXJGbGlwQ29sb3JcIiBzcmM9e3NyY30gd2lkdGg9XCIxNlwiIGhlaWdodD1cIjEyXCIgYWx0PVwiXCIgLz5cclxuICAgICAgICAgICAgeyBsYWJlbCB9XHJcbiAgICAgICAgPC9NZW51SXRlbVJhZGlvPlxyXG4gICAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdSb29tVGlsZUNvbnRleHRNZW51JyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICByb29tOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbiAgICAgICAgLyogY2FsbGJhY2sgY2FsbGVkIHdoZW4gdGhlIG1lbnUgaXMgZGlzbWlzc2VkICovXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZSgpIHtcclxuICAgICAgICBjb25zdCBkbVJvb21NYXAgPSBuZXcgRE1Sb29tTWFwKE1hdHJpeENsaWVudFBlZy5nZXQoKSk7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcm9vbU5vdGlmU3RhdGU6IFJvb21Ob3RpZnMuZ2V0Um9vbU5vdGlmc1N0YXRlKHRoaXMucHJvcHMucm9vbS5yb29tSWQpLFxyXG4gICAgICAgICAgICBpc0Zhdm91cml0ZTogdGhpcy5wcm9wcy5yb29tLnRhZ3MuaGFzT3duUHJvcGVydHkoXCJtLmZhdm91cml0ZVwiKSxcclxuICAgICAgICAgICAgaXNMb3dQcmlvcml0eTogdGhpcy5wcm9wcy5yb29tLnRhZ3MuaGFzT3duUHJvcGVydHkoXCJtLmxvd3ByaW9yaXR5XCIpLFxyXG4gICAgICAgICAgICBpc0RpcmVjdE1lc3NhZ2U6IEJvb2xlYW4oZG1Sb29tTWFwLmdldFVzZXJJZEZvclJvb21JZCh0aGlzLnByb3BzLnJvb20ucm9vbUlkKSksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3VubW91bnRlZCA9IGZhbHNlO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fdW5tb3VudGVkID0gdHJ1ZTtcclxuICAgIH0sXHJcblxyXG4gICAgX3RvZ2dsZVRhZzogZnVuY3Rpb24odGFnTmFtZU9uLCB0YWdOYW1lT2ZmKSB7XHJcbiAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNHdWVzdCgpKSB7XHJcbiAgICAgICAgICAgIHNsZWVwKDUwMCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goUm9vbUxpc3RBY3Rpb25zLnRhZ1Jvb20oXHJcbiAgICAgICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMucm9vbSxcclxuICAgICAgICAgICAgICAgICAgICB0YWdOYW1lT2ZmLCB0YWdOYW1lT24sXHJcbiAgICAgICAgICAgICAgICAgICAgdW5kZWZpbmVkLCAwLFxyXG4gICAgICAgICAgICAgICAgKSwgdHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX29uQ2xpY2tGYXZvdXJpdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIFRhZyByb29tIGFzICdGYXZvdXJpdGUnXHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmlzRmF2b3VyaXRlICYmIHRoaXMuc3RhdGUuaXNMb3dQcmlvcml0eSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGlzRmF2b3VyaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgaXNMb3dQcmlvcml0eTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLl90b2dnbGVUYWcoXCJtLmZhdm91cml0ZVwiLCBcIm0ubG93cHJpb3JpdHlcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmlzRmF2b3VyaXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzRmF2b3VyaXRlOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICB0aGlzLl90b2dnbGVUYWcobnVsbCwgXCJtLmZhdm91cml0ZVwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnN0YXRlLmlzRmF2b3VyaXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzRmF2b3VyaXRlOiB0cnVlfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvZ2dsZVRhZyhcIm0uZmF2b3VyaXRlXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX29uQ2xpY2tMb3dQcmlvcml0eTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gVGFnIHJvb20gYXMgJ0xvdyBQcmlvcml0eSdcclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuaXNMb3dQcmlvcml0eSAmJiB0aGlzLnN0YXRlLmlzRmF2b3VyaXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgaXNGYXZvdXJpdGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgaXNMb3dQcmlvcml0eTogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvZ2dsZVRhZyhcIm0ubG93cHJpb3JpdHlcIiwgXCJtLmZhdm91cml0ZVwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuaXNMb3dQcmlvcml0eSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0xvd1ByaW9yaXR5OiBmYWxzZX0pO1xyXG4gICAgICAgICAgICB0aGlzLl90b2dnbGVUYWcobnVsbCwgXCJtLmxvd3ByaW9yaXR5XCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuc3RhdGUuaXNMb3dQcmlvcml0eSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0xvd1ByaW9yaXR5OiB0cnVlfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvZ2dsZVRhZyhcIm0ubG93cHJpb3JpdHlcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25DbGlja0RNOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzR3Vlc3QoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBuZXdJc0RpcmVjdE1lc3NhZ2UgPSAhdGhpcy5zdGF0ZS5pc0RpcmVjdE1lc3NhZ2U7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGlzRGlyZWN0TWVzc2FnZTogbmV3SXNEaXJlY3RNZXNzYWdlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBSb29tcy5ndWVzc0FuZFNldERNUm9vbShcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5yb29tLCBuZXdJc0RpcmVjdE1lc3NhZ2UsXHJcbiAgICAgICAgKS50aGVuKHNsZWVwKDUwMCkpLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBDbG9zZSB0aGUgY29udGV4dCBtZW51XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uRmluaXNoZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gc2V0IERpcmVjdCBNZXNzYWdlIHN0YXR1cyBvZiByb29tJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0ZhaWxlZCB0byBzZXQgRGlyZWN0IE1lc3NhZ2Ugc3RhdHVzIG9mIHJvb20nKSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KCdPcGVyYXRpb24gZmFpbGVkJykpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgX29uQ2xpY2tMZWF2ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gTGVhdmUgcm9vbVxyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ2xlYXZlX3Jvb20nLFxyXG4gICAgICAgICAgICByb29tX2lkOiB0aGlzLnByb3BzLnJvb20ucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBDbG9zZSB0aGUgY29udGV4dCBtZW51XHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25GaW5pc2hlZCkge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrUmVqZWN0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdyZWplY3RfaW52aXRlJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogdGhpcy5wcm9wcy5yb29tLnJvb21JZCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gQ2xvc2UgdGhlIGNvbnRleHQgbWVudVxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRmluaXNoZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25DbGlja0ZvcmdldDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgLy8gRklYTUU6IGR1cGxpY2F0ZWQgd2l0aCBSb29tU2V0dGluZ3MgKGFuZCBkZWFkIGNvZGUgaW4gUm9vbVZpZXcpXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmZvcmdldCh0aGlzLnByb3BzLnJvb20ucm9vbUlkKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgLy8gU3dpdGNoIHRvIGFub3RoZXIgcm9vbSB2aWV3IGlmIHdlJ3JlIGN1cnJlbnRseSB2aWV3aW5nIHRoZVxyXG4gICAgICAgICAgICAvLyBoaXN0b3JpY2FsIHJvb21cclxuICAgICAgICAgICAgaWYgKFJvb21WaWV3U3RvcmUuZ2V0Um9vbUlkKCkgPT09IHRoaXMucHJvcHMucm9vbS5yb29tSWQpIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ3ZpZXdfbmV4dF9yb29tJyB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICBjb25zdCBlcnJDb2RlID0gZXJyLmVycmNvZGUgfHwgX3RkKFwidW5rbm93biBlcnJvciBjb2RlXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdGYWlsZWQgdG8gZm9yZ2V0IHJvb20nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRmFpbGVkIHRvIGZvcmdldCByb29tICUoZXJyQ29kZSlzJywge2VyckNvZGU6IGVyckNvZGV9KSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KCdPcGVyYXRpb24gZmFpbGVkJykpLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gQ2xvc2UgdGhlIGNvbnRleHQgbWVudVxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRmluaXNoZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfc2F2ZU5vdGlmU3RhdGU6IGZ1bmN0aW9uKG5ld1N0YXRlKSB7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3Qgb2xkU3RhdGUgPSB0aGlzLnN0YXRlLnJvb21Ob3RpZlN0YXRlO1xyXG4gICAgICAgIGNvbnN0IHJvb21JZCA9IHRoaXMucHJvcHMucm9vbS5yb29tSWQ7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICByb29tTm90aWZTdGF0ZTogbmV3U3RhdGUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgUm9vbU5vdGlmcy5zZXRSb29tTm90aWZzU3RhdGUocm9vbUlkLCBuZXdTdGF0ZSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGRlbGF5IHNsaWdodGx5IHNvIHRoYXQgdGhlIHVzZXIgY2FuIHNlZSB0aGVpciBzdGF0ZSBjaGFuZ2VcclxuICAgICAgICAgICAgLy8gYmVmb3JlIGNsb3NpbmcgdGhlIG1lbnVcclxuICAgICAgICAgICAgcmV0dXJuIHNsZWVwKDUwMCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5fdW5tb3VudGVkKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAvLyBDbG9zZSB0aGUgY29udGV4dCBtZW51XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkZpbmlzaGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAvLyBUT0RPOiBzb21lIGZvcm0gb2YgZXJyb3Igbm90aWZpY2F0aW9uIHRvIHRoZSB1c2VyXHJcbiAgICAgICAgICAgIC8vIHRvIGluZm9ybSB0aGVtIHRoYXQgdGhlaXIgc3RhdGUgY2hhbmdlIGZhaWxlZC5cclxuICAgICAgICAgICAgLy8gRm9yIG5vdyB3ZSBhdCBsZWFzdCBzZXQgdGhlIHN0YXRlIGJhY2tcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3VubW91bnRlZCkgcmV0dXJuO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHJvb21Ob3RpZlN0YXRlOiBvbGRTdGF0ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrQWxlcnRNZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fc2F2ZU5vdGlmU3RhdGUoUm9vbU5vdGlmcy5BTExfTUVTU0FHRVNfTE9VRCk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrQWxsTm90aWZzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9zYXZlTm90aWZTdGF0ZShSb29tTm90aWZzLkFMTF9NRVNTQUdFUyk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrTWVudGlvbnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3NhdmVOb3RpZlN0YXRlKFJvb21Ob3RpZnMuTUVOVElPTlNfT05MWSk7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vbkNsaWNrTXV0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5fc2F2ZU5vdGlmU3RhdGUoUm9vbU5vdGlmcy5NVVRFKTtcclxuICAgIH0sXHJcblxyXG4gICAgX3JlbmRlck5vdGlmTWVudTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVGlsZUNvbnRleHRNZW51XCIgcm9sZT1cImdyb3VwXCIgYXJpYS1sYWJlbD17X3QoXCJOb3RpZmljYXRpb24gc2V0dGluZ3NcIil9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9Sb29tVGlsZUNvbnRleHRNZW51X25vdGlmX3BpY2tlclwiIHJvbGU9XCJwcmVzZW50YXRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvbm90aWYtc2xpZGVyLnN2Z1wiKX0gd2lkdGg9XCIyMFwiIGhlaWdodD1cIjEwN1wiIGFsdD1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8Tm90aWZPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICBhY3RpdmU9e3RoaXMuc3RhdGUucm9vbU5vdGlmU3RhdGUgPT09IFJvb21Ob3RpZnMuQUxMX01FU1NBR0VTX0xPVUR9XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdBbGwgbWVzc2FnZXMgKG5vaXN5KScpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQ2xpY2tBbGVydE1lfVxyXG4gICAgICAgICAgICAgICAgICAgIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbi1jb250ZXh0LW11dGUtb2ZmLWNvcHkuc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxOb3RpZk9wdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZT17dGhpcy5zdGF0ZS5yb29tTm90aWZTdGF0ZSA9PT0gUm9vbU5vdGlmcy5BTExfTUVTU0FHRVN9XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdBbGwgbWVzc2FnZXMnKX1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrQWxsTm90aWZzfVxyXG4gICAgICAgICAgICAgICAgICAgIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbi1jb250ZXh0LW11dGUtb2ZmLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8Tm90aWZPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICBhY3RpdmU9e3RoaXMuc3RhdGUucm9vbU5vdGlmU3RhdGUgPT09IFJvb21Ob3RpZnMuTUVOVElPTlNfT05MWX1cclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoJ01lbnRpb25zIG9ubHknKX1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrTWVudGlvbnN9XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uLWNvbnRleHQtbXV0ZS1tZW50aW9ucy5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPE5vdGlmT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlPXt0aGlzLnN0YXRlLnJvb21Ob3RpZlN0YXRlID09PSBSb29tTm90aWZzLk1VVEV9XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KCdNdXRlJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25DbGlja011dGV9XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uLWNvbnRleHQtbXV0ZS5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25DbGlja1NldHRpbmdzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdvcGVuX3Jvb21fc2V0dGluZ3MnLFxyXG4gICAgICAgICAgICByb29tX2lkOiB0aGlzLnByb3BzLnJvb20ucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLm9uRmluaXNoZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfcmVuZGVyU2V0dGluZ3NNZW51OiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfdGFnX2ZpZWxkXCIgb25DbGljaz17dGhpcy5fb25DbGlja1NldHRpbmdzfT5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfdGFnX2ljb25cIiBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ZlYXRoZXItY3VzdG9taXNlZC9zZXR0aW5ncy5zdmdcIil9IHdpZHRoPVwiMTVcIiBoZWlnaHQ9XCIxNVwiIGFsdD1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdCgnU2V0dGluZ3MnKSB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVuZGVyTGVhdmVNZW51OiBmdW5jdGlvbihtZW1iZXJzaGlwKSB7XHJcbiAgICAgICAgaWYgKCFtZW1iZXJzaGlwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGxlYXZlQ2xpY2tIYW5kbGVyID0gbnVsbDtcclxuICAgICAgICBsZXQgbGVhdmVUZXh0ID0gbnVsbDtcclxuXHJcbiAgICAgICAgc3dpdGNoIChtZW1iZXJzaGlwKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJqb2luXCI6XHJcbiAgICAgICAgICAgICAgICBsZWF2ZUNsaWNrSGFuZGxlciA9IHRoaXMuX29uQ2xpY2tMZWF2ZTtcclxuICAgICAgICAgICAgICAgIGxlYXZlVGV4dCA9IF90KCdMZWF2ZScpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJsZWF2ZVwiOlxyXG4gICAgICAgICAgICBjYXNlIFwiYmFuXCI6XHJcbiAgICAgICAgICAgICAgICBsZWF2ZUNsaWNrSGFuZGxlciA9IHRoaXMuX29uQ2xpY2tGb3JnZXQ7XHJcbiAgICAgICAgICAgICAgICBsZWF2ZVRleHQgPSBfdCgnRm9yZ2V0Jyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImludml0ZVwiOlxyXG4gICAgICAgICAgICAgICAgbGVhdmVDbGlja0hhbmRsZXIgPSB0aGlzLl9vbkNsaWNrUmVqZWN0O1xyXG4gICAgICAgICAgICAgICAgbGVhdmVUZXh0ID0gX3QoJ1JlamVjdCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfbGVhdmVcIiBvbkNsaWNrPXtsZWF2ZUNsaWNrSGFuZGxlcn0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9XCJteF9Sb29tVGlsZUNvbnRleHRNZW51X3RhZ19pY29uXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uX2NvbnRleHRfZGVsZXRlLnN2Z1wiKX0gd2lkdGg9XCIxNVwiIGhlaWdodD1cIjE1XCIgYWx0PVwiXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICB7IGxlYXZlVGV4dCB9XHJcbiAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICBfcmVuZGVyUm9vbVRhZ01lbnU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8Um9vbVRhZ09wdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZT17dGhpcy5zdGF0ZS5pc0Zhdm91cml0ZX1cclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoJ0Zhdm91cml0ZScpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQ2xpY2tGYXZvdXJpdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uX2NvbnRleHRfZmF2ZS5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjU2V0PXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uX2NvbnRleHRfZmF2ZV9vbi5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPFJvb21UYWdPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICBhY3RpdmU9e3RoaXMuc3RhdGUuaXNMb3dQcmlvcml0eX1cclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoJ0xvdyBQcmlvcml0eScpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuX29uQ2xpY2tMb3dQcmlvcml0eX1cclxuICAgICAgICAgICAgICAgICAgICBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ljb25fY29udGV4dF9sb3cuc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIHNyY1NldD17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvaWNvbl9jb250ZXh0X2xvd19vbi5zdmdcIil9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPFJvb21UYWdPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICBhY3RpdmU9e3RoaXMuc3RhdGUuaXNEaXJlY3RNZXNzYWdlfVxyXG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfdCgnRGlyZWN0IENoYXQnKX1cclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vbkNsaWNrRE19XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9pY29uX2NvbnRleHRfcGVyc29uLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgICAgICBzcmNTZXQ9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL2ljb25fY29udGV4dF9wZXJzb25fb24uc3ZnXCIpfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBteU1lbWJlcnNoaXAgPSB0aGlzLnByb3BzLnJvb20uZ2V0TXlNZW1iZXJzaGlwKCk7XHJcblxyXG4gICAgICAgIHN3aXRjaCAobXlNZW1iZXJzaGlwKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2pvaW4nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJOb3RpZk1lbnUoKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfc2VwYXJhdG9yXCIgcm9sZT1cInNlcGFyYXRvclwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJMZWF2ZU1lbnUobXlNZW1iZXJzaGlwKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfc2VwYXJhdG9yXCIgcm9sZT1cInNlcGFyYXRvclwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJSb29tVGFnTWVudSgpIH1cclxuICAgICAgICAgICAgICAgICAgICA8aHIgY2xhc3NOYW1lPVwibXhfUm9vbVRpbGVDb250ZXh0TWVudV9zZXBhcmF0b3JcIiByb2xlPVwic2VwYXJhdG9yXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICB7IHRoaXMuX3JlbmRlclNldHRpbmdzTWVudSgpIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICAgICAgY2FzZSAnaW52aXRlJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgdGhpcy5fcmVuZGVyTGVhdmVNZW51KG15TWVtYmVyc2hpcCkgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJMZWF2ZU1lbnUobXlNZW1iZXJzaGlwKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cIm14X1Jvb21UaWxlQ29udGV4dE1lbnVfc2VwYXJhdG9yXCIgcm9sZT1cInNlcGFyYXRvclwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLl9yZW5kZXJTZXR0aW5nc01lbnUoKSB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==