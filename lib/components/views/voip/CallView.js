"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _CallHandler = _interopRequireDefault(require("../../../CallHandler"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'CallView',
  propTypes: {
    // js-sdk room object. If set, we will only show calls for the given
    // room; if not, we will show any active call.
    room: _propTypes.default.object,
    // A Conference Handler implementation
    // Must have a function signature:
    //  getConferenceCallForRoom(roomId: string): MatrixCall
    ConferenceHandler: _propTypes.default.object,
    // maxHeight style attribute for the video panel
    maxVideoHeight: _propTypes.default.number,
    // a callback which is called when the user clicks on the video div
    onClick: _propTypes.default.func,
    // a callback which is called when the content in the callview changes
    // in a way that is likely to cause a resize.
    onResize: _propTypes.default.func,
    // render ongoing audio call details - useful when in LeftPanel
    showVoice: _propTypes.default.bool
  },
  getInitialState: function () {
    return {
      // the call this view is displaying (if any)
      call: null
    };
  },
  // TODO: [REACT-WARNING] Replace component with real class, use constructor for refs
  UNSAFE_componentWillMount: function () {
    this._video = (0, _react.createRef)();
  },
  componentDidMount: function () {
    this.dispatcherRef = _dispatcher.default.register(this.onAction);
    this.showCall();
  },
  componentWillUnmount: function () {
    _dispatcher.default.unregister(this.dispatcherRef);
  },
  onAction: function (payload) {
    // don't filter out payloads for room IDs other than props.room because
    // we may be interested in the conf 1:1 room
    if (payload.action !== 'call_state') {
      return;
    }

    this.showCall();
  },
  showCall: function () {
    let call;

    if (this.props.room) {
      const roomId = this.props.room.roomId;
      call = _CallHandler.default.getCallForRoom(roomId) || (this.props.ConferenceHandler ? this.props.ConferenceHandler.getConferenceCallForRoom(roomId) : null);

      if (this.call) {
        this.setState({
          call: call
        });
      }
    } else {
      call = _CallHandler.default.getAnyActiveCall(); // Ignore calls if we can't get the room associated with them.
      // I think the underlying problem is that the js-sdk sends events
      // for calls before it has made the rooms available in the store,
      // although this isn't confirmed.

      if (_MatrixClientPeg.MatrixClientPeg.get().getRoom(call.roomId) === null) {
        call = null;
      }

      this.setState({
        call: call
      });
    }

    if (call) {
      call.setLocalVideoElement(this.getVideoView().getLocalVideoElement());
      call.setRemoteVideoElement(this.getVideoView().getRemoteVideoElement()); // always use a separate element for audio stream playback.
      // this is to let us move CallView around the DOM without interrupting remote audio
      // during playback, by having the audio rendered by a top-level <audio/> element.
      // rather than being rendered by the main remoteVideo <video/> element.

      call.setRemoteAudioElement(this.getVideoView().getRemoteAudioElement());
    }

    if (call && call.type === "video" && call.call_state !== "ended" && call.call_state !== "ringing") {
      // if this call is a conf call, don't display local video as the
      // conference will have us in it
      this.getVideoView().getLocalVideoElement().style.display = call.confUserId ? "none" : "block";
      this.getVideoView().getRemoteVideoElement().style.display = "block";
    } else {
      this.getVideoView().getLocalVideoElement().style.display = "none";
      this.getVideoView().getRemoteVideoElement().style.display = "none";

      _dispatcher.default.dispatch({
        action: 'video_fullscreen',
        fullscreen: false
      });
    }

    if (this.props.onResize) {
      this.props.onResize();
    }
  },
  getVideoView: function () {
    return this._video.current;
  },
  render: function () {
    const VideoView = sdk.getComponent('voip.VideoView');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let voice;

    if (this.state.call && this.state.call.type === "voice" && this.props.showVoice) {
      const callRoom = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.state.call.roomId);

      voice = _react.default.createElement(AccessibleButton, {
        className: "mx_CallView_voice",
        onClick: this.props.onClick
      }, (0, _languageHandler._t)("Active call (%(roomName)s)", {
        roomName: callRoom.name
      }));
    }

    return _react.default.createElement("div", null, _react.default.createElement(VideoView, {
      ref: this._video,
      onClick: this.props.onClick,
      onResize: this.props.onResize,
      maxHeight: this.props.maxVideoHeight
    }), voice);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3ZvaXAvQ2FsbFZpZXcuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJyb29tIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiQ29uZmVyZW5jZUhhbmRsZXIiLCJtYXhWaWRlb0hlaWdodCIsIm51bWJlciIsIm9uQ2xpY2siLCJmdW5jIiwib25SZXNpemUiLCJzaG93Vm9pY2UiLCJib29sIiwiZ2V0SW5pdGlhbFN0YXRlIiwiY2FsbCIsIlVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQiLCJfdmlkZW8iLCJjb21wb25lbnREaWRNb3VudCIsImRpc3BhdGNoZXJSZWYiLCJkaXMiLCJyZWdpc3RlciIsIm9uQWN0aW9uIiwic2hvd0NhbGwiLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInVucmVnaXN0ZXIiLCJwYXlsb2FkIiwiYWN0aW9uIiwicHJvcHMiLCJyb29tSWQiLCJDYWxsSGFuZGxlciIsImdldENhbGxGb3JSb29tIiwiZ2V0Q29uZmVyZW5jZUNhbGxGb3JSb29tIiwic2V0U3RhdGUiLCJnZXRBbnlBY3RpdmVDYWxsIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0Um9vbSIsInNldExvY2FsVmlkZW9FbGVtZW50IiwiZ2V0VmlkZW9WaWV3IiwiZ2V0TG9jYWxWaWRlb0VsZW1lbnQiLCJzZXRSZW1vdGVWaWRlb0VsZW1lbnQiLCJnZXRSZW1vdGVWaWRlb0VsZW1lbnQiLCJzZXRSZW1vdGVBdWRpb0VsZW1lbnQiLCJnZXRSZW1vdGVBdWRpb0VsZW1lbnQiLCJ0eXBlIiwiY2FsbF9zdGF0ZSIsInN0eWxlIiwiZGlzcGxheSIsImNvbmZVc2VySWQiLCJkaXNwYXRjaCIsImZ1bGxzY3JlZW4iLCJjdXJyZW50IiwicmVuZGVyIiwiVmlkZW9WaWV3Iiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiQWNjZXNzaWJsZUJ1dHRvbiIsInZvaWNlIiwic3RhdGUiLCJjYWxsUm9vbSIsInJvb21OYW1lIiwibmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7O2VBeUJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLFVBRGU7QUFHNUJDLEVBQUFBLFNBQVMsRUFBRTtBQUNQO0FBQ0E7QUFDQUMsSUFBQUEsSUFBSSxFQUFFQyxtQkFBVUMsTUFIVDtBQUtQO0FBQ0E7QUFDQTtBQUNBQyxJQUFBQSxpQkFBaUIsRUFBRUYsbUJBQVVDLE1BUnRCO0FBVVA7QUFDQUUsSUFBQUEsY0FBYyxFQUFFSCxtQkFBVUksTUFYbkI7QUFhUDtBQUNBQyxJQUFBQSxPQUFPLEVBQUVMLG1CQUFVTSxJQWRaO0FBZ0JQO0FBQ0E7QUFDQUMsSUFBQUEsUUFBUSxFQUFFUCxtQkFBVU0sSUFsQmI7QUFvQlA7QUFDQUUsSUFBQUEsU0FBUyxFQUFFUixtQkFBVVM7QUFyQmQsR0FIaUI7QUEyQjVCQyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0g7QUFDQUMsTUFBQUEsSUFBSSxFQUFFO0FBRkgsS0FBUDtBQUlILEdBaEMyQjtBQWtDNUI7QUFDQUMsRUFBQUEseUJBQXlCLEVBQUUsWUFBVztBQUNsQyxTQUFLQyxNQUFMLEdBQWMsdUJBQWQ7QUFDSCxHQXJDMkI7QUF1QzVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS0MsUUFBbEIsQ0FBckI7QUFDQSxTQUFLQyxRQUFMO0FBQ0gsR0ExQzJCO0FBNEM1QkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3Qkosd0JBQUlLLFVBQUosQ0FBZSxLQUFLTixhQUFwQjtBQUNILEdBOUMyQjtBQWdENUJHLEVBQUFBLFFBQVEsRUFBRSxVQUFTSSxPQUFULEVBQWtCO0FBQ3hCO0FBQ0E7QUFDQSxRQUFJQSxPQUFPLENBQUNDLE1BQVIsS0FBbUIsWUFBdkIsRUFBcUM7QUFDakM7QUFDSDs7QUFDRCxTQUFLSixRQUFMO0FBQ0gsR0F2RDJCO0FBeUQ1QkEsRUFBQUEsUUFBUSxFQUFFLFlBQVc7QUFDakIsUUFBSVIsSUFBSjs7QUFFQSxRQUFJLEtBQUthLEtBQUwsQ0FBV3pCLElBQWYsRUFBcUI7QUFDakIsWUFBTTBCLE1BQU0sR0FBRyxLQUFLRCxLQUFMLENBQVd6QixJQUFYLENBQWdCMEIsTUFBL0I7QUFDQWQsTUFBQUEsSUFBSSxHQUFHZSxxQkFBWUMsY0FBWixDQUEyQkYsTUFBM0IsTUFDRixLQUFLRCxLQUFMLENBQVd0QixpQkFBWCxHQUNBLEtBQUtzQixLQUFMLENBQVd0QixpQkFBWCxDQUE2QjBCLHdCQUE3QixDQUFzREgsTUFBdEQsQ0FEQSxHQUVBLElBSEUsQ0FBUDs7QUFNQSxVQUFJLEtBQUtkLElBQVQsRUFBZTtBQUNYLGFBQUtrQixRQUFMLENBQWM7QUFBRWxCLFVBQUFBLElBQUksRUFBRUE7QUFBUixTQUFkO0FBQ0g7QUFDSixLQVhELE1BV087QUFDSEEsTUFBQUEsSUFBSSxHQUFHZSxxQkFBWUksZ0JBQVosRUFBUCxDQURHLENBRUg7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsVUFBSUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEJ0QixJQUFJLENBQUNjLE1BQW5DLE1BQStDLElBQW5ELEVBQXlEO0FBQ3JEZCxRQUFBQSxJQUFJLEdBQUcsSUFBUDtBQUNIOztBQUNELFdBQUtrQixRQUFMLENBQWM7QUFBRWxCLFFBQUFBLElBQUksRUFBRUE7QUFBUixPQUFkO0FBQ0g7O0FBRUQsUUFBSUEsSUFBSixFQUFVO0FBQ05BLE1BQUFBLElBQUksQ0FBQ3VCLG9CQUFMLENBQTBCLEtBQUtDLFlBQUwsR0FBb0JDLG9CQUFwQixFQUExQjtBQUNBekIsTUFBQUEsSUFBSSxDQUFDMEIscUJBQUwsQ0FBMkIsS0FBS0YsWUFBTCxHQUFvQkcscUJBQXBCLEVBQTNCLEVBRk0sQ0FHTjtBQUNBO0FBQ0E7QUFDQTs7QUFDQTNCLE1BQUFBLElBQUksQ0FBQzRCLHFCQUFMLENBQTJCLEtBQUtKLFlBQUwsR0FBb0JLLHFCQUFwQixFQUEzQjtBQUNIOztBQUNELFFBQUk3QixJQUFJLElBQUlBLElBQUksQ0FBQzhCLElBQUwsS0FBYyxPQUF0QixJQUFpQzlCLElBQUksQ0FBQytCLFVBQUwsS0FBb0IsT0FBckQsSUFBZ0UvQixJQUFJLENBQUMrQixVQUFMLEtBQW9CLFNBQXhGLEVBQW1HO0FBQy9GO0FBQ0E7QUFDQSxXQUFLUCxZQUFMLEdBQW9CQyxvQkFBcEIsR0FBMkNPLEtBQTNDLENBQWlEQyxPQUFqRCxHQUNJakMsSUFBSSxDQUFDa0MsVUFBTCxHQUFrQixNQUFsQixHQUEyQixPQUQvQjtBQUdBLFdBQUtWLFlBQUwsR0FBb0JHLHFCQUFwQixHQUE0Q0ssS0FBNUMsQ0FBa0RDLE9BQWxELEdBQTRELE9BQTVEO0FBQ0gsS0FQRCxNQU9PO0FBQ0gsV0FBS1QsWUFBTCxHQUFvQkMsb0JBQXBCLEdBQTJDTyxLQUEzQyxDQUFpREMsT0FBakQsR0FBMkQsTUFBM0Q7QUFDQSxXQUFLVCxZQUFMLEdBQW9CRyxxQkFBcEIsR0FBNENLLEtBQTVDLENBQWtEQyxPQUFsRCxHQUE0RCxNQUE1RDs7QUFDQTVCLDBCQUFJOEIsUUFBSixDQUFhO0FBQUN2QixRQUFBQSxNQUFNLEVBQUUsa0JBQVQ7QUFBNkJ3QixRQUFBQSxVQUFVLEVBQUU7QUFBekMsT0FBYjtBQUNIOztBQUVELFFBQUksS0FBS3ZCLEtBQUwsQ0FBV2pCLFFBQWYsRUFBeUI7QUFDckIsV0FBS2lCLEtBQUwsQ0FBV2pCLFFBQVg7QUFDSDtBQUNKLEdBNUcyQjtBQThHNUI0QixFQUFBQSxZQUFZLEVBQUUsWUFBVztBQUNyQixXQUFPLEtBQUt0QixNQUFMLENBQVltQyxPQUFuQjtBQUNILEdBaEgyQjtBQWtINUJDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsU0FBUyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsZ0JBQWpCLENBQWxCO0FBQ0EsVUFBTUMsZ0JBQWdCLEdBQUdGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFFQSxRQUFJRSxLQUFKOztBQUNBLFFBQUksS0FBS0MsS0FBTCxDQUFXNUMsSUFBWCxJQUFtQixLQUFLNEMsS0FBTCxDQUFXNUMsSUFBWCxDQUFnQjhCLElBQWhCLEtBQXlCLE9BQTVDLElBQXVELEtBQUtqQixLQUFMLENBQVdoQixTQUF0RSxFQUFpRjtBQUM3RSxZQUFNZ0QsUUFBUSxHQUFHekIsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEIsS0FBS3NCLEtBQUwsQ0FBVzVDLElBQVgsQ0FBZ0JjLE1BQTlDLENBQWpCOztBQUNBNkIsTUFBQUEsS0FBSyxHQUNELDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsU0FBUyxFQUFDLG1CQUE1QjtBQUFnRCxRQUFBLE9BQU8sRUFBRSxLQUFLOUIsS0FBTCxDQUFXbkI7QUFBcEUsU0FDRSx5QkFBRyw0QkFBSCxFQUFpQztBQUFDb0QsUUFBQUEsUUFBUSxFQUFFRCxRQUFRLENBQUNFO0FBQXBCLE9BQWpDLENBREYsQ0FESjtBQUtIOztBQUVELFdBQ0ksMENBQ0ksNkJBQUMsU0FBRDtBQUNJLE1BQUEsR0FBRyxFQUFFLEtBQUs3QyxNQURkO0FBRUksTUFBQSxPQUFPLEVBQUUsS0FBS1csS0FBTCxDQUFXbkIsT0FGeEI7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLbUIsS0FBTCxDQUFXakIsUUFIekI7QUFJSSxNQUFBLFNBQVMsRUFBRSxLQUFLaUIsS0FBTCxDQUFXckI7QUFKMUIsTUFESixFQU9NbUQsS0FQTixDQURKO0FBV0g7QUEzSTJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuaW1wb3J0IFJlYWN0LCB7Y3JlYXRlUmVmfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBDYWxsSGFuZGxlciBmcm9tICcuLi8uLi8uLi9DYWxsSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnQ2FsbFZpZXcnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIGpzLXNkayByb29tIG9iamVjdC4gSWYgc2V0LCB3ZSB3aWxsIG9ubHkgc2hvdyBjYWxscyBmb3IgdGhlIGdpdmVuXHJcbiAgICAgICAgLy8gcm9vbTsgaWYgbm90LCB3ZSB3aWxsIHNob3cgYW55IGFjdGl2ZSBjYWxsLlxyXG4gICAgICAgIHJvb206IFByb3BUeXBlcy5vYmplY3QsXHJcblxyXG4gICAgICAgIC8vIEEgQ29uZmVyZW5jZSBIYW5kbGVyIGltcGxlbWVudGF0aW9uXHJcbiAgICAgICAgLy8gTXVzdCBoYXZlIGEgZnVuY3Rpb24gc2lnbmF0dXJlOlxyXG4gICAgICAgIC8vICBnZXRDb25mZXJlbmNlQ2FsbEZvclJvb20ocm9vbUlkOiBzdHJpbmcpOiBNYXRyaXhDYWxsXHJcbiAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXI6IFByb3BUeXBlcy5vYmplY3QsXHJcblxyXG4gICAgICAgIC8vIG1heEhlaWdodCBzdHlsZSBhdHRyaWJ1dGUgZm9yIHRoZSB2aWRlbyBwYW5lbFxyXG4gICAgICAgIG1heFZpZGVvSGVpZ2h0OiBQcm9wVHlwZXMubnVtYmVyLFxyXG5cclxuICAgICAgICAvLyBhIGNhbGxiYWNrIHdoaWNoIGlzIGNhbGxlZCB3aGVuIHRoZSB1c2VyIGNsaWNrcyBvbiB0aGUgdmlkZW8gZGl2XHJcbiAgICAgICAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIGEgY2FsbGJhY2sgd2hpY2ggaXMgY2FsbGVkIHdoZW4gdGhlIGNvbnRlbnQgaW4gdGhlIGNhbGx2aWV3IGNoYW5nZXNcclxuICAgICAgICAvLyBpbiBhIHdheSB0aGF0IGlzIGxpa2VseSB0byBjYXVzZSBhIHJlc2l6ZS5cclxuICAgICAgICBvblJlc2l6ZTogUHJvcFR5cGVzLmZ1bmMsXHJcblxyXG4gICAgICAgIC8vIHJlbmRlciBvbmdvaW5nIGF1ZGlvIGNhbGwgZGV0YWlscyAtIHVzZWZ1bCB3aGVuIGluIExlZnRQYW5lbFxyXG4gICAgICAgIHNob3dWb2ljZTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgLy8gdGhlIGNhbGwgdGhpcyB2aWV3IGlzIGRpc3BsYXlpbmcgKGlmIGFueSlcclxuICAgICAgICAgICAgY2FsbDogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICAvLyBUT0RPOiBbUkVBQ1QtV0FSTklOR10gUmVwbGFjZSBjb21wb25lbnQgd2l0aCByZWFsIGNsYXNzLCB1c2UgY29uc3RydWN0b3IgZm9yIHJlZnNcclxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3ZpZGVvID0gY3JlYXRlUmVmKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLmRpc3BhdGNoZXJSZWYgPSBkaXMucmVnaXN0ZXIodGhpcy5vbkFjdGlvbik7XHJcbiAgICAgICAgdGhpcy5zaG93Q2FsbCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGlzLnVucmVnaXN0ZXIodGhpcy5kaXNwYXRjaGVyUmVmKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25BY3Rpb246IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICAvLyBkb24ndCBmaWx0ZXIgb3V0IHBheWxvYWRzIGZvciByb29tIElEcyBvdGhlciB0aGFuIHByb3BzLnJvb20gYmVjYXVzZVxyXG4gICAgICAgIC8vIHdlIG1heSBiZSBpbnRlcmVzdGVkIGluIHRoZSBjb25mIDE6MSByb29tXHJcbiAgICAgICAgaWYgKHBheWxvYWQuYWN0aW9uICE9PSAnY2FsbF9zdGF0ZScpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNob3dDYWxsKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3dDYWxsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgY2FsbDtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMucm9vbSkge1xyXG4gICAgICAgICAgICBjb25zdCByb29tSWQgPSB0aGlzLnByb3BzLnJvb20ucm9vbUlkO1xyXG4gICAgICAgICAgICBjYWxsID0gQ2FsbEhhbmRsZXIuZ2V0Q2FsbEZvclJvb20ocm9vbUlkKSB8fFxyXG4gICAgICAgICAgICAgICAgKHRoaXMucHJvcHMuQ29uZmVyZW5jZUhhbmRsZXIgP1xyXG4gICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuQ29uZmVyZW5jZUhhbmRsZXIuZ2V0Q29uZmVyZW5jZUNhbGxGb3JSb29tKHJvb21JZCkgOlxyXG4gICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5jYWxsKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgY2FsbDogY2FsbCB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNhbGwgPSBDYWxsSGFuZGxlci5nZXRBbnlBY3RpdmVDYWxsKCk7XHJcbiAgICAgICAgICAgIC8vIElnbm9yZSBjYWxscyBpZiB3ZSBjYW4ndCBnZXQgdGhlIHJvb20gYXNzb2NpYXRlZCB3aXRoIHRoZW0uXHJcbiAgICAgICAgICAgIC8vIEkgdGhpbmsgdGhlIHVuZGVybHlpbmcgcHJvYmxlbSBpcyB0aGF0IHRoZSBqcy1zZGsgc2VuZHMgZXZlbnRzXHJcbiAgICAgICAgICAgIC8vIGZvciBjYWxscyBiZWZvcmUgaXQgaGFzIG1hZGUgdGhlIHJvb21zIGF2YWlsYWJsZSBpbiB0aGUgc3RvcmUsXHJcbiAgICAgICAgICAgIC8vIGFsdGhvdWdoIHRoaXMgaXNuJ3QgY29uZmlybWVkLlxyXG4gICAgICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20oY2FsbC5yb29tSWQpID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBjYWxsID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgY2FsbDogY2FsbCB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjYWxsKSB7XHJcbiAgICAgICAgICAgIGNhbGwuc2V0TG9jYWxWaWRlb0VsZW1lbnQodGhpcy5nZXRWaWRlb1ZpZXcoKS5nZXRMb2NhbFZpZGVvRWxlbWVudCgpKTtcclxuICAgICAgICAgICAgY2FsbC5zZXRSZW1vdGVWaWRlb0VsZW1lbnQodGhpcy5nZXRWaWRlb1ZpZXcoKS5nZXRSZW1vdGVWaWRlb0VsZW1lbnQoKSk7XHJcbiAgICAgICAgICAgIC8vIGFsd2F5cyB1c2UgYSBzZXBhcmF0ZSBlbGVtZW50IGZvciBhdWRpbyBzdHJlYW0gcGxheWJhY2suXHJcbiAgICAgICAgICAgIC8vIHRoaXMgaXMgdG8gbGV0IHVzIG1vdmUgQ2FsbFZpZXcgYXJvdW5kIHRoZSBET00gd2l0aG91dCBpbnRlcnJ1cHRpbmcgcmVtb3RlIGF1ZGlvXHJcbiAgICAgICAgICAgIC8vIGR1cmluZyBwbGF5YmFjaywgYnkgaGF2aW5nIHRoZSBhdWRpbyByZW5kZXJlZCBieSBhIHRvcC1sZXZlbCA8YXVkaW8vPiBlbGVtZW50LlxyXG4gICAgICAgICAgICAvLyByYXRoZXIgdGhhbiBiZWluZyByZW5kZXJlZCBieSB0aGUgbWFpbiByZW1vdGVWaWRlbyA8dmlkZW8vPiBlbGVtZW50LlxyXG4gICAgICAgICAgICBjYWxsLnNldFJlbW90ZUF1ZGlvRWxlbWVudCh0aGlzLmdldFZpZGVvVmlldygpLmdldFJlbW90ZUF1ZGlvRWxlbWVudCgpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGNhbGwgJiYgY2FsbC50eXBlID09PSBcInZpZGVvXCIgJiYgY2FsbC5jYWxsX3N0YXRlICE9PSBcImVuZGVkXCIgJiYgY2FsbC5jYWxsX3N0YXRlICE9PSBcInJpbmdpbmdcIikge1xyXG4gICAgICAgICAgICAvLyBpZiB0aGlzIGNhbGwgaXMgYSBjb25mIGNhbGwsIGRvbid0IGRpc3BsYXkgbG9jYWwgdmlkZW8gYXMgdGhlXHJcbiAgICAgICAgICAgIC8vIGNvbmZlcmVuY2Ugd2lsbCBoYXZlIHVzIGluIGl0XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VmlkZW9WaWV3KCkuZ2V0TG9jYWxWaWRlb0VsZW1lbnQoKS5zdHlsZS5kaXNwbGF5ID0gKFxyXG4gICAgICAgICAgICAgICAgY2FsbC5jb25mVXNlcklkID8gXCJub25lXCIgOiBcImJsb2NrXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRWaWRlb1ZpZXcoKS5nZXRSZW1vdGVWaWRlb0VsZW1lbnQoKS5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VmlkZW9WaWV3KCkuZ2V0TG9jYWxWaWRlb0VsZW1lbnQoKS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VmlkZW9WaWV3KCkuZ2V0UmVtb3RlVmlkZW9FbGVtZW50KCkuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3ZpZGVvX2Z1bGxzY3JlZW4nLCBmdWxsc2NyZWVuOiBmYWxzZX0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJvcHMub25SZXNpemUpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblJlc2l6ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgZ2V0VmlkZW9WaWV3OiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdmlkZW8uY3VycmVudDtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBWaWRlb1ZpZXcgPSBzZGsuZ2V0Q29tcG9uZW50KCd2b2lwLlZpZGVvVmlldycpO1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcblxyXG4gICAgICAgIGxldCB2b2ljZTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jYWxsICYmIHRoaXMuc3RhdGUuY2FsbC50eXBlID09PSBcInZvaWNlXCIgJiYgdGhpcy5wcm9wcy5zaG93Vm9pY2UpIHtcclxuICAgICAgICAgICAgY29uc3QgY2FsbFJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbSh0aGlzLnN0YXRlLmNhbGwucm9vbUlkKTtcclxuICAgICAgICAgICAgdm9pY2UgPSAoXHJcbiAgICAgICAgICAgICAgICA8QWNjZXNzaWJsZUJ1dHRvbiBjbGFzc05hbWU9XCJteF9DYWxsVmlld192b2ljZVwiIG9uQ2xpY2s9e3RoaXMucHJvcHMub25DbGlja30+XHJcbiAgICAgICAgICAgICAgICB7IF90KFwiQWN0aXZlIGNhbGwgKCUocm9vbU5hbWUpcylcIiwge3Jvb21OYW1lOiBjYWxsUm9vbS5uYW1lfSkgfVxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxWaWRlb1ZpZXdcclxuICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuX3ZpZGVvfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMucHJvcHMub25DbGlja31cclxuICAgICAgICAgICAgICAgICAgICBvblJlc2l6ZT17dGhpcy5wcm9wcy5vblJlc2l6ZX1cclxuICAgICAgICAgICAgICAgICAgICBtYXhIZWlnaHQ9e3RoaXMucHJvcHMubWF4VmlkZW9IZWlnaHR9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgeyB2b2ljZSB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbiJdfQ==