"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _RoomViewStore = _interopRequireDefault(require("../../../stores/RoomViewStore"));

var _CallHandler = _interopRequireDefault(require("../../../CallHandler"));

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var sdk = _interopRequireWildcard(require("../../../index"));

/*
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'CallPreview',
  propTypes: {
    // A Conference Handler implementation
    // Must have a function signature:
    //  getConferenceCallForRoom(roomId: string): MatrixCall
    ConferenceHandler: _propTypes.default.object
  },
  getInitialState: function () {
    return {
      roomId: _RoomViewStore.default.getRoomId(),
      activeCall: _CallHandler.default.getAnyActiveCall()
    };
  },
  componentDidMount: function () {
    this._roomStoreToken = _RoomViewStore.default.addListener(this._onRoomViewStoreUpdate);
    this.dispatcherRef = _dispatcher.default.register(this._onAction);
  },
  componentWillUnmount: function () {
    if (this._roomStoreToken) {
      this._roomStoreToken.remove();
    }

    _dispatcher.default.unregister(this.dispatcherRef);
  },
  _onRoomViewStoreUpdate: function (payload) {
    if (_RoomViewStore.default.getRoomId() === this.state.roomId) return;
    this.setState({
      roomId: _RoomViewStore.default.getRoomId()
    });
  },
  _onAction: function (payload) {
    switch (payload.action) {
      // listen for call state changes to prod the render method, which
      // may hide the global CallView if the call it is tracking is dead
      case 'call_state':
        this.setState({
          activeCall: _CallHandler.default.getAnyActiveCall()
        });
        break;
    }
  },
  _onCallViewClick: function () {
    const call = _CallHandler.default.getAnyActiveCall();

    if (call) {
      _dispatcher.default.dispatch({
        action: 'view_room',
        room_id: call.groupRoomId || call.roomId
      });
    }
  },
  render: function () {
    const callForRoom = _CallHandler.default.getCallForRoom(this.state.roomId);

    const showCall = this.state.activeCall && this.state.activeCall.call_state === 'connected' && !callForRoom;

    if (showCall) {
      const CallView = sdk.getComponent('voip.CallView');
      return _react.default.createElement(CallView, {
        className: "mx_LeftPanel_callView",
        showVoice: true,
        onClick: this._onCallViewClick,
        ConferenceHandler: this.props.ConferenceHandler
      });
    }

    const PersistentApp = sdk.getComponent('elements.PersistentApp');
    return _react.default.createElement(PersistentApp, null);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3ZvaXAvQ2FsbFByZXZpZXcuanMiXSwibmFtZXMiOlsiZGlzcGxheU5hbWUiLCJwcm9wVHlwZXMiLCJDb25mZXJlbmNlSGFuZGxlciIsIlByb3BUeXBlcyIsIm9iamVjdCIsImdldEluaXRpYWxTdGF0ZSIsInJvb21JZCIsIlJvb21WaWV3U3RvcmUiLCJnZXRSb29tSWQiLCJhY3RpdmVDYWxsIiwiQ2FsbEhhbmRsZXIiLCJnZXRBbnlBY3RpdmVDYWxsIiwiY29tcG9uZW50RGlkTW91bnQiLCJfcm9vbVN0b3JlVG9rZW4iLCJhZGRMaXN0ZW5lciIsIl9vblJvb21WaWV3U3RvcmVVcGRhdGUiLCJkaXNwYXRjaGVyUmVmIiwiZGlzIiwicmVnaXN0ZXIiLCJfb25BY3Rpb24iLCJjb21wb25lbnRXaWxsVW5tb3VudCIsInJlbW92ZSIsInVucmVnaXN0ZXIiLCJwYXlsb2FkIiwic3RhdGUiLCJzZXRTdGF0ZSIsImFjdGlvbiIsIl9vbkNhbGxWaWV3Q2xpY2siLCJjYWxsIiwiZGlzcGF0Y2giLCJyb29tX2lkIiwiZ3JvdXBSb29tSWQiLCJyZW5kZXIiLCJjYWxsRm9yUm9vbSIsImdldENhbGxGb3JSb29tIiwic2hvd0NhbGwiLCJjYWxsX3N0YXRlIiwiQ2FsbFZpZXciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJwcm9wcyIsIlBlcnNpc3RlbnRBcHAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7OztlQXlCZSwrQkFBaUI7QUFDNUJBLEVBQUFBLFdBQVcsRUFBRSxhQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUDtBQUNBO0FBQ0E7QUFDQUMsSUFBQUEsaUJBQWlCLEVBQUVDLG1CQUFVQztBQUp0QixHQUhpQjtBQVU1QkMsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsV0FBTztBQUNIQyxNQUFBQSxNQUFNLEVBQUVDLHVCQUFjQyxTQUFkLEVBREw7QUFFSEMsTUFBQUEsVUFBVSxFQUFFQyxxQkFBWUMsZ0JBQVo7QUFGVCxLQUFQO0FBSUgsR0FmMkI7QUFpQjVCQyxFQUFBQSxpQkFBaUIsRUFBRSxZQUFXO0FBQzFCLFNBQUtDLGVBQUwsR0FBdUJOLHVCQUFjTyxXQUFkLENBQTBCLEtBQUtDLHNCQUEvQixDQUF2QjtBQUNBLFNBQUtDLGFBQUwsR0FBcUJDLG9CQUFJQyxRQUFKLENBQWEsS0FBS0MsU0FBbEIsQ0FBckI7QUFDSCxHQXBCMkI7QUFzQjVCQyxFQUFBQSxvQkFBb0IsRUFBRSxZQUFXO0FBQzdCLFFBQUksS0FBS1AsZUFBVCxFQUEwQjtBQUN0QixXQUFLQSxlQUFMLENBQXFCUSxNQUFyQjtBQUNIOztBQUNESix3QkFBSUssVUFBSixDQUFlLEtBQUtOLGFBQXBCO0FBQ0gsR0EzQjJCO0FBNkI1QkQsRUFBQUEsc0JBQXNCLEVBQUUsVUFBU1EsT0FBVCxFQUFrQjtBQUN0QyxRQUFJaEIsdUJBQWNDLFNBQWQsT0FBOEIsS0FBS2dCLEtBQUwsQ0FBV2xCLE1BQTdDLEVBQXFEO0FBQ3JELFNBQUttQixRQUFMLENBQWM7QUFDVm5CLE1BQUFBLE1BQU0sRUFBRUMsdUJBQWNDLFNBQWQ7QUFERSxLQUFkO0FBR0gsR0FsQzJCO0FBb0M1QlcsRUFBQUEsU0FBUyxFQUFFLFVBQVNJLE9BQVQsRUFBa0I7QUFDekIsWUFBUUEsT0FBTyxDQUFDRyxNQUFoQjtBQUNJO0FBQ0E7QUFDQSxXQUFLLFlBQUw7QUFDSSxhQUFLRCxRQUFMLENBQWM7QUFDVmhCLFVBQUFBLFVBQVUsRUFBRUMscUJBQVlDLGdCQUFaO0FBREYsU0FBZDtBQUdBO0FBUFI7QUFTSCxHQTlDMkI7QUFnRDVCZ0IsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixVQUFNQyxJQUFJLEdBQUdsQixxQkFBWUMsZ0JBQVosRUFBYjs7QUFDQSxRQUFJaUIsSUFBSixFQUFVO0FBQ05YLDBCQUFJWSxRQUFKLENBQWE7QUFDVEgsUUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVEksUUFBQUEsT0FBTyxFQUFFRixJQUFJLENBQUNHLFdBQUwsSUFBb0JILElBQUksQ0FBQ3RCO0FBRnpCLE9BQWI7QUFJSDtBQUNKLEdBeEQyQjtBQTBENUIwQixFQUFBQSxNQUFNLEVBQUUsWUFBVztBQUNmLFVBQU1DLFdBQVcsR0FBR3ZCLHFCQUFZd0IsY0FBWixDQUEyQixLQUFLVixLQUFMLENBQVdsQixNQUF0QyxDQUFwQjs7QUFDQSxVQUFNNkIsUUFBUSxHQUFJLEtBQUtYLEtBQUwsQ0FBV2YsVUFBWCxJQUF5QixLQUFLZSxLQUFMLENBQVdmLFVBQVgsQ0FBc0IyQixVQUF0QixLQUFxQyxXQUE5RCxJQUE2RSxDQUFDSCxXQUFoRzs7QUFFQSxRQUFJRSxRQUFKLEVBQWM7QUFDVixZQUFNRSxRQUFRLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixlQUFqQixDQUFqQjtBQUNBLGFBQ0ksNkJBQUMsUUFBRDtBQUNJLFFBQUEsU0FBUyxFQUFDLHVCQURkO0FBQ3NDLFFBQUEsU0FBUyxFQUFFLElBRGpEO0FBQ3VELFFBQUEsT0FBTyxFQUFFLEtBQUtaLGdCQURyRTtBQUVJLFFBQUEsaUJBQWlCLEVBQUUsS0FBS2EsS0FBTCxDQUFXdEM7QUFGbEMsUUFESjtBQU1IOztBQUNELFVBQU11QyxhQUFhLEdBQUdILEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdEI7QUFDQSxXQUFPLDZCQUFDLGFBQUQsT0FBUDtBQUNIO0FBekUyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgUm9vbVZpZXdTdG9yZSBmcm9tICcuLi8uLi8uLi9zdG9yZXMvUm9vbVZpZXdTdG9yZSc7XHJcbmltcG9ydCBDYWxsSGFuZGxlciBmcm9tICcuLi8uLi8uLi9DYWxsSGFuZGxlcic7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vLi4vLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnQ2FsbFByZXZpZXcnLFxyXG5cclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIC8vIEEgQ29uZmVyZW5jZSBIYW5kbGVyIGltcGxlbWVudGF0aW9uXHJcbiAgICAgICAgLy8gTXVzdCBoYXZlIGEgZnVuY3Rpb24gc2lnbmF0dXJlOlxyXG4gICAgICAgIC8vICBnZXRDb25mZXJlbmNlQ2FsbEZvclJvb20ocm9vbUlkOiBzdHJpbmcpOiBNYXRyaXhDYWxsXHJcbiAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXI6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9LFxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcm9vbUlkOiBSb29tVmlld1N0b3JlLmdldFJvb21JZCgpLFxyXG4gICAgICAgICAgICBhY3RpdmVDYWxsOiBDYWxsSGFuZGxlci5nZXRBbnlBY3RpdmVDYWxsKCksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3Jvb21TdG9yZVRva2VuID0gUm9vbVZpZXdTdG9yZS5hZGRMaXN0ZW5lcih0aGlzLl9vblJvb21WaWV3U3RvcmVVcGRhdGUpO1xyXG4gICAgICAgIHRoaXMuZGlzcGF0Y2hlclJlZiA9IGRpcy5yZWdpc3Rlcih0aGlzLl9vbkFjdGlvbik7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5fcm9vbVN0b3JlVG9rZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fcm9vbVN0b3JlVG9rZW4ucmVtb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRpcy51bnJlZ2lzdGVyKHRoaXMuZGlzcGF0Y2hlclJlZik7XHJcbiAgICB9LFxyXG5cclxuICAgIF9vblJvb21WaWV3U3RvcmVVcGRhdGU6IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICBpZiAoUm9vbVZpZXdTdG9yZS5nZXRSb29tSWQoKSA9PT0gdGhpcy5zdGF0ZS5yb29tSWQpIHJldHVybjtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcm9vbUlkOiBSb29tVmlld1N0b3JlLmdldFJvb21JZCgpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBfb25BY3Rpb246IGZ1bmN0aW9uKHBheWxvYWQpIHtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIC8vIGxpc3RlbiBmb3IgY2FsbCBzdGF0ZSBjaGFuZ2VzIHRvIHByb2QgdGhlIHJlbmRlciBtZXRob2QsIHdoaWNoXHJcbiAgICAgICAgICAgIC8vIG1heSBoaWRlIHRoZSBnbG9iYWwgQ2FsbFZpZXcgaWYgdGhlIGNhbGwgaXQgaXMgdHJhY2tpbmcgaXMgZGVhZFxyXG4gICAgICAgICAgICBjYXNlICdjYWxsX3N0YXRlJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZUNhbGw6IENhbGxIYW5kbGVyLmdldEFueUFjdGl2ZUNhbGwoKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfb25DYWxsVmlld0NsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjYWxsID0gQ2FsbEhhbmRsZXIuZ2V0QW55QWN0aXZlQ2FsbCgpO1xyXG4gICAgICAgIGlmIChjYWxsKSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgcm9vbV9pZDogY2FsbC5ncm91cFJvb21JZCB8fCBjYWxsLnJvb21JZCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGNhbGxGb3JSb29tID0gQ2FsbEhhbmRsZXIuZ2V0Q2FsbEZvclJvb20odGhpcy5zdGF0ZS5yb29tSWQpO1xyXG4gICAgICAgIGNvbnN0IHNob3dDYWxsID0gKHRoaXMuc3RhdGUuYWN0aXZlQ2FsbCAmJiB0aGlzLnN0YXRlLmFjdGl2ZUNhbGwuY2FsbF9zdGF0ZSA9PT0gJ2Nvbm5lY3RlZCcgJiYgIWNhbGxGb3JSb29tKTtcclxuXHJcbiAgICAgICAgaWYgKHNob3dDYWxsKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IENhbGxWaWV3ID0gc2RrLmdldENvbXBvbmVudCgndm9pcC5DYWxsVmlldycpO1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPENhbGxWaWV3XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfTGVmdFBhbmVsX2NhbGxWaWV3XCIgc2hvd1ZvaWNlPXt0cnVlfSBvbkNsaWNrPXt0aGlzLl9vbkNhbGxWaWV3Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXI9e3RoaXMucHJvcHMuQ29uZmVyZW5jZUhhbmRsZXJ9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBQZXJzaXN0ZW50QXBwID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuUGVyc2lzdGVudEFwcCcpO1xyXG4gICAgICAgIHJldHVybiA8UGVyc2lzdGVudEFwcCAvPjtcclxuICAgIH0sXHJcbn0pO1xyXG5cclxuIl19