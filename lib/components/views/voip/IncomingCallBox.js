"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _MatrixClientPeg = require("../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../dispatcher"));

var _languageHandler = require("../../../languageHandler");

var sdk = _interopRequireWildcard(require("../../../index"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'IncomingCallBox',
  propTypes: {
    incomingCall: _propTypes.default.object
  },
  onAnswerClick: function (e) {
    e.stopPropagation();

    _dispatcher.default.dispatch({
      action: 'answer',
      room_id: this.props.incomingCall.roomId
    });
  },
  onRejectClick: function (e) {
    e.stopPropagation();

    _dispatcher.default.dispatch({
      action: 'hangup',
      room_id: this.props.incomingCall.roomId
    });
  },
  render: function () {
    let room = null;

    if (this.props.incomingCall) {
      room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(this.props.incomingCall.roomId);
    }

    const caller = room ? room.name : (0, _languageHandler._t)("unknown caller");
    let incomingCallText = null;

    if (this.props.incomingCall) {
      if (this.props.incomingCall.type === "voice") {
        incomingCallText = (0, _languageHandler._t)("Incoming voice call from %(name)s", {
          name: caller
        });
      } else if (this.props.incomingCall.type === "video") {
        incomingCallText = (0, _languageHandler._t)("Incoming video call from %(name)s", {
          name: caller
        });
      } else {
        incomingCallText = (0, _languageHandler._t)("Incoming call from %(name)s", {
          name: caller
        });
      }
    }

    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement("div", {
      className: "mx_IncomingCallBox",
      id: "incomingCallBox"
    }, _react.default.createElement("img", {
      className: "mx_IncomingCallBox_chevron",
      src: require("../../../../res/img/chevron-left.png"),
      width: "9",
      height: "16"
    }), _react.default.createElement("div", {
      className: "mx_IncomingCallBox_title"
    }, incomingCallText), _react.default.createElement("div", {
      className: "mx_IncomingCallBox_buttons"
    }, _react.default.createElement("div", {
      className: "mx_IncomingCallBox_buttons_cell"
    }, _react.default.createElement(AccessibleButton, {
      className: "mx_IncomingCallBox_buttons_decline",
      onClick: this.onRejectClick
    }, (0, _languageHandler._t)("Decline"))), _react.default.createElement("div", {
      className: "mx_IncomingCallBox_buttons_cell"
    }, _react.default.createElement(AccessibleButton, {
      className: "mx_IncomingCallBox_buttons_accept",
      onClick: this.onAnswerClick
    }, (0, _languageHandler._t)("Accept")))));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL3ZvaXAvSW5jb21pbmdDYWxsQm94LmpzIl0sIm5hbWVzIjpbImRpc3BsYXlOYW1lIiwicHJvcFR5cGVzIiwiaW5jb21pbmdDYWxsIiwiUHJvcFR5cGVzIiwib2JqZWN0Iiwib25BbnN3ZXJDbGljayIsImUiLCJzdG9wUHJvcGFnYXRpb24iLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsInJvb21faWQiLCJwcm9wcyIsInJvb21JZCIsIm9uUmVqZWN0Q2xpY2siLCJyZW5kZXIiLCJyb29tIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0Um9vbSIsImNhbGxlciIsIm5hbWUiLCJpbmNvbWluZ0NhbGxUZXh0IiwidHlwZSIsIkFjY2Vzc2libGVCdXR0b24iLCJzZGsiLCJnZXRDb21wb25lbnQiLCJyZXF1aXJlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBeUJlLCtCQUFpQjtBQUM1QkEsRUFBQUEsV0FBVyxFQUFFLGlCQURlO0FBRzVCQyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsWUFBWSxFQUFFQyxtQkFBVUM7QUFEakIsR0FIaUI7QUFPNUJDLEVBQUFBLGFBQWEsRUFBRSxVQUFTQyxDQUFULEVBQVk7QUFDdkJBLElBQUFBLENBQUMsQ0FBQ0MsZUFBRjs7QUFDQUMsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsUUFEQztBQUVUQyxNQUFBQSxPQUFPLEVBQUUsS0FBS0MsS0FBTCxDQUFXVixZQUFYLENBQXdCVztBQUZ4QixLQUFiO0FBSUgsR0FiMkI7QUFlNUJDLEVBQUFBLGFBQWEsRUFBRSxVQUFTUixDQUFULEVBQVk7QUFDdkJBLElBQUFBLENBQUMsQ0FBQ0MsZUFBRjs7QUFDQUMsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsUUFEQztBQUVUQyxNQUFBQSxPQUFPLEVBQUUsS0FBS0MsS0FBTCxDQUFXVixZQUFYLENBQXdCVztBQUZ4QixLQUFiO0FBSUgsR0FyQjJCO0FBdUI1QkUsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixRQUFJQyxJQUFJLEdBQUcsSUFBWDs7QUFDQSxRQUFJLEtBQUtKLEtBQUwsQ0FBV1YsWUFBZixFQUE2QjtBQUN6QmMsTUFBQUEsSUFBSSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxPQUF0QixDQUE4QixLQUFLUCxLQUFMLENBQVdWLFlBQVgsQ0FBd0JXLE1BQXRELENBQVA7QUFDSDs7QUFFRCxVQUFNTyxNQUFNLEdBQUdKLElBQUksR0FBR0EsSUFBSSxDQUFDSyxJQUFSLEdBQWUseUJBQUcsZ0JBQUgsQ0FBbEM7QUFFQSxRQUFJQyxnQkFBZ0IsR0FBRyxJQUF2Qjs7QUFDQSxRQUFJLEtBQUtWLEtBQUwsQ0FBV1YsWUFBZixFQUE2QjtBQUN6QixVQUFJLEtBQUtVLEtBQUwsQ0FBV1YsWUFBWCxDQUF3QnFCLElBQXhCLEtBQWlDLE9BQXJDLEVBQThDO0FBQzFDRCxRQUFBQSxnQkFBZ0IsR0FBRyx5QkFBRyxtQ0FBSCxFQUF3QztBQUFDRCxVQUFBQSxJQUFJLEVBQUVEO0FBQVAsU0FBeEMsQ0FBbkI7QUFDSCxPQUZELE1BRU8sSUFBSSxLQUFLUixLQUFMLENBQVdWLFlBQVgsQ0FBd0JxQixJQUF4QixLQUFpQyxPQUFyQyxFQUE4QztBQUNqREQsUUFBQUEsZ0JBQWdCLEdBQUcseUJBQUcsbUNBQUgsRUFBd0M7QUFBQ0QsVUFBQUEsSUFBSSxFQUFFRDtBQUFQLFNBQXhDLENBQW5CO0FBQ0gsT0FGTSxNQUVBO0FBQ0hFLFFBQUFBLGdCQUFnQixHQUFHLHlCQUFHLDZCQUFILEVBQWtDO0FBQUNELFVBQUFBLElBQUksRUFBRUQ7QUFBUCxTQUFsQyxDQUFuQjtBQUNIO0FBQ0o7O0FBRUQsVUFBTUksZ0JBQWdCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsb0JBQWY7QUFBb0MsTUFBQSxFQUFFLEVBQUM7QUFBdkMsT0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFDLDRCQUFmO0FBQTRDLE1BQUEsR0FBRyxFQUFFQyxPQUFPLENBQUMsc0NBQUQsQ0FBeEQ7QUFBa0csTUFBQSxLQUFLLEVBQUMsR0FBeEc7QUFBNEcsTUFBQSxNQUFNLEVBQUM7QUFBbkgsTUFESixFQUVJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNNTCxnQkFETixDQUZKLEVBS0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUMsb0NBQTVCO0FBQWlFLE1BQUEsT0FBTyxFQUFFLEtBQUtSO0FBQS9FLE9BQ00seUJBQUcsU0FBSCxDQUROLENBREosQ0FESixFQU1JO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLGdCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLG1DQUE1QjtBQUFnRSxNQUFBLE9BQU8sRUFBRSxLQUFLVDtBQUE5RSxPQUNNLHlCQUFHLFFBQUgsQ0FETixDQURKLENBTkosQ0FMSixDQURKO0FBb0JIO0FBL0QyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uLy4uLy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIGRpc3BsYXlOYW1lOiAnSW5jb21pbmdDYWxsQm94JyxcclxuXHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICBpbmNvbWluZ0NhbGw6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB9LFxyXG5cclxuICAgIG9uQW5zd2VyQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIGFjdGlvbjogJ2Fuc3dlcicsXHJcbiAgICAgICAgICAgIHJvb21faWQ6IHRoaXMucHJvcHMuaW5jb21pbmdDYWxsLnJvb21JZCxcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgb25SZWplY3RDbGljazogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiAnaGFuZ3VwJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogdGhpcy5wcm9wcy5pbmNvbWluZ0NhbGwucm9vbUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCByb29tID0gbnVsbDtcclxuICAgICAgICBpZiAodGhpcy5wcm9wcy5pbmNvbWluZ0NhbGwpIHtcclxuICAgICAgICAgICAgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKHRoaXMucHJvcHMuaW5jb21pbmdDYWxsLnJvb21JZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjYWxsZXIgPSByb29tID8gcm9vbS5uYW1lIDogX3QoXCJ1bmtub3duIGNhbGxlclwiKTtcclxuXHJcbiAgICAgICAgbGV0IGluY29taW5nQ2FsbFRleHQgPSBudWxsO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmluY29taW5nQ2FsbCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5pbmNvbWluZ0NhbGwudHlwZSA9PT0gXCJ2b2ljZVwiKSB7XHJcbiAgICAgICAgICAgICAgICBpbmNvbWluZ0NhbGxUZXh0ID0gX3QoXCJJbmNvbWluZyB2b2ljZSBjYWxsIGZyb20gJShuYW1lKXNcIiwge25hbWU6IGNhbGxlcn0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMuaW5jb21pbmdDYWxsLnR5cGUgPT09IFwidmlkZW9cIikge1xyXG4gICAgICAgICAgICAgICAgaW5jb21pbmdDYWxsVGV4dCA9IF90KFwiSW5jb21pbmcgdmlkZW8gY2FsbCBmcm9tICUobmFtZSlzXCIsIHtuYW1lOiBjYWxsZXJ9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGluY29taW5nQ2FsbFRleHQgPSBfdChcIkluY29taW5nIGNhbGwgZnJvbSAlKG5hbWUpc1wiLCB7bmFtZTogY2FsbGVyfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbmNvbWluZ0NhbGxCb3hcIiBpZD1cImluY29taW5nQ2FsbEJveFwiPlxyXG4gICAgICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9XCJteF9JbmNvbWluZ0NhbGxCb3hfY2hldnJvblwiIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvY2hldnJvbi1sZWZ0LnBuZ1wiKX0gd2lkdGg9XCI5XCIgaGVpZ2h0PVwiMTZcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbmNvbWluZ0NhbGxCb3hfdGl0bGVcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IGluY29taW5nQ2FsbFRleHQgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0luY29taW5nQ2FsbEJveF9idXR0b25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbmNvbWluZ0NhbGxCb3hfYnV0dG9uc19jZWxsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0luY29taW5nQ2FsbEJveF9idXR0b25zX2RlY2xpbmVcIiBvbkNsaWNrPXt0aGlzLm9uUmVqZWN0Q2xpY2t9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBfdChcIkRlY2xpbmVcIikgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9JbmNvbWluZ0NhbGxCb3hfYnV0dG9uc19jZWxsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X0luY29taW5nQ2FsbEJveF9idXR0b25zX2FjY2VwdFwiIG9uQ2xpY2s9e3RoaXMub25BbnN3ZXJDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IF90KFwiQWNjZXB0XCIpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuXHJcbiJdfQ==