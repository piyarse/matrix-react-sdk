"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var _Notifier = _interopRequireDefault(require("../../../Notifier"));

var _AccessibleButton = _interopRequireDefault(require("../../../components/views/elements/AccessibleButton"));

/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: 'MatrixToolbar',
  hideToolbar: function () {
    _Notifier.default.setToolbarHidden(true);
  },
  onClick: function () {
    _Notifier.default.setEnabled(true);
  },
  render: function () {
    return _react.default.createElement("div", {
      className: "mx_MatrixToolbar"
    }, _react.default.createElement("img", {
      className: "mx_MatrixToolbar_warning",
      src: require("../../../../res/img/warning.svg"),
      width: "24",
      height: "23",
      alt: ""
    }), _react.default.createElement("div", {
      className: "mx_MatrixToolbar_content"
    }, (0, _languageHandler._t)('You are not receiving desktop notifications'), " ", _react.default.createElement("a", {
      className: "mx_MatrixToolbar_link",
      onClick: this.onClick
    }, " ", (0, _languageHandler._t)('Enable them now'))), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_MatrixToolbar_close",
      onClick: this.hideToolbar
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/cancel.svg"),
      width: "18",
      height: "18",
      alt: (0, _languageHandler._t)('Close')
    })));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dsb2JhbHMvTWF0cml4VG9vbGJhci5qcyJdLCJuYW1lcyI6WyJkaXNwbGF5TmFtZSIsImhpZGVUb29sYmFyIiwiTm90aWZpZXIiLCJzZXRUb29sYmFySGlkZGVuIiwib25DbGljayIsInNldEVuYWJsZWQiLCJyZW5kZXIiLCJyZXF1aXJlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBcEJBOzs7Ozs7Ozs7Ozs7Ozs7ZUFzQmUsK0JBQWlCO0FBQzVCQSxFQUFBQSxXQUFXLEVBQUUsZUFEZTtBQUc1QkMsRUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEJDLHNCQUFTQyxnQkFBVCxDQUEwQixJQUExQjtBQUNILEdBTDJCO0FBTzVCQyxFQUFBQSxPQUFPLEVBQUUsWUFBVztBQUNoQkYsc0JBQVNHLFVBQVQsQ0FBb0IsSUFBcEI7QUFDSCxHQVQyQjtBQVc1QkMsRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsMEJBQWY7QUFBMEMsTUFBQSxHQUFHLEVBQUVDLE9BQU8sQ0FBQyxpQ0FBRCxDQUF0RDtBQUEyRixNQUFBLEtBQUssRUFBQyxJQUFqRztBQUFzRyxNQUFBLE1BQU0sRUFBQyxJQUE3RztBQUFrSCxNQUFBLEdBQUcsRUFBQztBQUF0SCxNQURKLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0sseUJBQUcsNkNBQUgsQ0FETCxPQUN5RDtBQUFHLE1BQUEsU0FBUyxFQUFDLHVCQUFiO0FBQXFDLE1BQUEsT0FBTyxFQUFHLEtBQUtIO0FBQXBELFlBQWlFLHlCQUFHLGlCQUFILENBQWpFLENBRHpELENBRkosRUFLSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLFNBQVMsRUFBQyx3QkFBNUI7QUFBcUQsTUFBQSxPQUFPLEVBQUcsS0FBS0g7QUFBcEUsT0FBbUY7QUFBSyxNQUFBLEdBQUcsRUFBRU0sT0FBTyxDQUFDLGdDQUFELENBQWpCO0FBQXFELE1BQUEsS0FBSyxFQUFDLElBQTNEO0FBQWdFLE1BQUEsTUFBTSxFQUFDLElBQXZFO0FBQTRFLE1BQUEsR0FBRyxFQUFFLHlCQUFHLE9BQUg7QUFBakYsTUFBbkYsQ0FMSixDQURKO0FBU0g7QUFyQjJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBjcmVhdGVSZWFjdENsYXNzIGZyb20gJ2NyZWF0ZS1yZWFjdC1jbGFzcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi4vLi4vLi4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IE5vdGlmaWVyIGZyb20gJy4uLy4uLy4uL05vdGlmaWVyJztcclxuaW1wb3J0IEFjY2Vzc2libGVCdXR0b24gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy92aWV3cy9lbGVtZW50cy9BY2Nlc3NpYmxlQnV0dG9uJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVJlYWN0Q2xhc3Moe1xyXG4gICAgZGlzcGxheU5hbWU6ICdNYXRyaXhUb29sYmFyJyxcclxuXHJcbiAgICBoaWRlVG9vbGJhcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgTm90aWZpZXIuc2V0VG9vbGJhckhpZGRlbih0cnVlKTtcclxuICAgIH0sXHJcblxyXG4gICAgb25DbGljazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgTm90aWZpZXIuc2V0RW5hYmxlZCh0cnVlKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhcl93YXJuaW5nXCIgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy93YXJuaW5nLnN2Z1wiKX0gd2lkdGg9XCIyNFwiIGhlaWdodD1cIjIzXCIgYWx0PVwiXCIgLz5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhcl9jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICB7IF90KCdZb3UgYXJlIG5vdCByZWNlaXZpbmcgZGVza3RvcCBub3RpZmljYXRpb25zJykgfSA8YSBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX2xpbmtcIiBvbkNsaWNrPXsgdGhpcy5vbkNsaWNrIH0+IHsgX3QoJ0VuYWJsZSB0aGVtIG5vdycpIH08L2E+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfY2xvc2VcIiBvbkNsaWNrPXsgdGhpcy5oaWRlVG9vbGJhciB9ID48aW1nIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvY2FuY2VsLnN2Z1wiKX0gd2lkdGg9XCIxOFwiIGhlaWdodD1cIjE4XCIgYWx0PXtfdCgnQ2xvc2UnKX0gLz48L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19