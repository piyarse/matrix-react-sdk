"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: "PasswordNagBar",
  onUpdateClicked: function () {
    const SetPasswordDialog = sdk.getComponent('dialogs.SetPasswordDialog');

    _Modal.default.createTrackedDialog('Set Password Dialog', 'Password Nag Bar', SetPasswordDialog);
  },
  render: function () {
    const toolbarClasses = "mx_MatrixToolbar mx_MatrixToolbar_clickable";
    return _react.default.createElement("div", {
      className: toolbarClasses,
      onClick: this.onUpdateClicked
    }, _react.default.createElement("img", {
      className: "mx_MatrixToolbar_warning",
      src: require("../../../../res/img/warning.svg"),
      width: "24",
      height: "23",
      alt: ""
    }), _react.default.createElement("div", {
      className: "mx_MatrixToolbar_content"
    }, (0, _languageHandler._t)("To return to your account in future you need to <u>set a password</u>", {}, {
      'u': sub => _react.default.createElement("u", null, sub)
    })), _react.default.createElement("button", {
      className: "mx_MatrixToolbar_action"
    }, (0, _languageHandler._t)("Set Password")));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dsb2JhbHMvUGFzc3dvcmROYWdCYXIuanMiXSwibmFtZXMiOlsib25VcGRhdGVDbGlja2VkIiwiU2V0UGFzc3dvcmREaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJyZW5kZXIiLCJ0b29sYmFyQ2xhc3NlcyIsInJlcXVpcmUiLCJzdWIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7OztlQXVCZSwrQkFBaUI7QUFBQTtBQUM1QkEsRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEIsVUFBTUMsaUJBQWlCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBMUI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQixxQkFBMUIsRUFBaUQsa0JBQWpELEVBQXFFSixpQkFBckU7QUFDSCxHQUoyQjtBQU01QkssRUFBQUEsTUFBTSxFQUFFLFlBQVc7QUFDZixVQUFNQyxjQUFjLEdBQUcsNkNBQXZCO0FBQ0EsV0FDSTtBQUFLLE1BQUEsU0FBUyxFQUFFQSxjQUFoQjtBQUFnQyxNQUFBLE9BQU8sRUFBRSxLQUFLUDtBQUE5QyxPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsMEJBQWY7QUFDSSxNQUFBLEdBQUcsRUFBRVEsT0FBTyxDQUFDLGlDQUFELENBRGhCO0FBRUksTUFBQSxLQUFLLEVBQUMsSUFGVjtBQUdJLE1BQUEsTUFBTSxFQUFDLElBSFg7QUFJSSxNQUFBLEdBQUcsRUFBQztBQUpSLE1BREosRUFPSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDTSx5QkFDRSx1RUFERixFQUVFLEVBRkYsRUFHRTtBQUFFLFdBQU1DLEdBQUQsSUFBUyx3Q0FBS0EsR0FBTDtBQUFoQixLQUhGLENBRE4sQ0FQSixFQWNJO0FBQVEsTUFBQSxTQUFTLEVBQUM7QUFBbEIsT0FDTSx5QkFBRyxjQUFILENBRE4sQ0FkSixDQURKO0FBb0JIO0FBNUIyQixDQUFqQixDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIG9uVXBkYXRlQ2xpY2tlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3QgU2V0UGFzc3dvcmREaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLlNldFBhc3N3b3JkRGlhbG9nJyk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU2V0IFBhc3N3b3JkIERpYWxvZycsICdQYXNzd29yZCBOYWcgQmFyJywgU2V0UGFzc3dvcmREaWFsb2cpO1xyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHRvb2xiYXJDbGFzc2VzID0gXCJteF9NYXRyaXhUb29sYmFyIG14X01hdHJpeFRvb2xiYXJfY2xpY2thYmxlXCI7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Rvb2xiYXJDbGFzc2VzfSBvbkNsaWNrPXt0aGlzLm9uVXBkYXRlQ2xpY2tlZH0+XHJcbiAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfd2FybmluZ1wiXHJcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy93YXJuaW5nLnN2Z1wiKX1cclxuICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjI0XCJcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCIyM1wiXHJcbiAgICAgICAgICAgICAgICAgICAgYWx0PVwiXCJcclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVG8gcmV0dXJuIHRvIHlvdXIgYWNjb3VudCBpbiBmdXR1cmUgeW91IG5lZWQgdG8gPHU+c2V0IGEgcGFzc3dvcmQ8L3U+XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7ICd1JzogKHN1YikgPT4gPHU+eyBzdWIgfTwvdT4gfSxcclxuICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX2FjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJTZXQgUGFzc3dvcmRcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19