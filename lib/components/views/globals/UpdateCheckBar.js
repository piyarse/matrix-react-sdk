"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var _languageHandler = require("../../../languageHandler");

var _PlatformPeg = _interopRequireDefault(require("../../../PlatformPeg"));

var _AccessibleButton = _interopRequireDefault(require("../../../components/views/elements/AccessibleButton"));

/*
Copyright 2017, 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var _default = (0, _createReactClass.default)({
  displayName: "UpdateCheckBar",
  propTypes: {
    status: _propTypes.default.string.isRequired,
    // Currently for error detail but will be usable for download progress
    // once that is a thing that squirrel passes through electron.
    detail: _propTypes.default.string
  },
  getDefaultProps: function () {
    return {
      detail: ''
    };
  },
  getStatusText: function () {
    // we can't import the enum from riot-web as we don't want matrix-react-sdk
    // to depend on riot-web. so we grab it as a normal object via API instead.
    const updateCheckStatusEnum = _PlatformPeg.default.get().getUpdateCheckStatusEnum();

    switch (this.props.status) {
      case updateCheckStatusEnum.ERROR:
        return (0, _languageHandler._t)('Error encountered (%(errorDetail)s).', {
          errorDetail: this.props.detail
        });

      case updateCheckStatusEnum.CHECKING:
        return (0, _languageHandler._t)('Checking for an update...');

      case updateCheckStatusEnum.NOTAVAILABLE:
        return (0, _languageHandler._t)('No update available.');

      case updateCheckStatusEnum.DOWNLOADING:
        return (0, _languageHandler._t)('Downloading update...');
    }
  },
  hideToolbar: function () {
    _PlatformPeg.default.get().stopUpdateCheck();
  },
  render: function () {
    const message = this.getStatusText();
    const warning = (0, _languageHandler._t)('Warning');

    if (!('getUpdateCheckStatusEnum' in _PlatformPeg.default.get())) {
      return _react.default.createElement("div", null);
    }

    const updateCheckStatusEnum = _PlatformPeg.default.get().getUpdateCheckStatusEnum();

    const doneStatuses = [updateCheckStatusEnum.ERROR, updateCheckStatusEnum.NOTAVAILABLE];
    let image;

    if (doneStatuses.includes(this.props.status)) {
      image = _react.default.createElement("img", {
        className: "mx_MatrixToolbar_warning",
        src: require("../../../../res/img/warning.svg"),
        width: "24",
        height: "23",
        alt: ""
      });
    } else {
      image = _react.default.createElement("img", {
        className: "mx_MatrixToolbar_warning",
        src: require("../../../../res/img/spinner.gif"),
        width: "24",
        height: "23",
        alt: ""
      });
    }

    return _react.default.createElement("div", {
      className: "mx_MatrixToolbar"
    }, image, _react.default.createElement("div", {
      className: "mx_MatrixToolbar_content"
    }, message), _react.default.createElement(_AccessibleButton.default, {
      className: "mx_MatrixToolbar_close",
      onClick: this.hideToolbar
    }, _react.default.createElement("img", {
      src: require("../../../../res/img/cancel.svg"),
      width: "18",
      height: "18",
      alt: (0, _languageHandler._t)('Close')
    })));
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dsb2JhbHMvVXBkYXRlQ2hlY2tCYXIuanMiXSwibmFtZXMiOlsicHJvcFR5cGVzIiwic3RhdHVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsImRldGFpbCIsImdldERlZmF1bHRQcm9wcyIsImdldFN0YXR1c1RleHQiLCJ1cGRhdGVDaGVja1N0YXR1c0VudW0iLCJQbGF0Zm9ybVBlZyIsImdldCIsImdldFVwZGF0ZUNoZWNrU3RhdHVzRW51bSIsInByb3BzIiwiRVJST1IiLCJlcnJvckRldGFpbCIsIkNIRUNLSU5HIiwiTk9UQVZBSUxBQkxFIiwiRE9XTkxPQURJTkciLCJoaWRlVG9vbGJhciIsInN0b3BVcGRhdGVDaGVjayIsInJlbmRlciIsIm1lc3NhZ2UiLCJ3YXJuaW5nIiwiZG9uZVN0YXR1c2VzIiwiaW1hZ2UiLCJpbmNsdWRlcyIsInJlcXVpcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQkE7Ozs7Ozs7Ozs7Ozs7OztlQXVCZSwrQkFBaUI7QUFBQTtBQUM1QkEsRUFBQUEsU0FBUyxFQUFFO0FBQ1BDLElBQUFBLE1BQU0sRUFBRUMsbUJBQVVDLE1BQVYsQ0FBaUJDLFVBRGxCO0FBRVA7QUFDQTtBQUNBQyxJQUFBQSxNQUFNLEVBQUVILG1CQUFVQztBQUpYLEdBRGlCO0FBUTVCRyxFQUFBQSxlQUFlLEVBQUUsWUFBVztBQUN4QixXQUFPO0FBQ0hELE1BQUFBLE1BQU0sRUFBRTtBQURMLEtBQVA7QUFHSCxHQVoyQjtBQWM1QkUsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEI7QUFDQTtBQUNBLFVBQU1DLHFCQUFxQixHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsd0JBQWxCLEVBQTlCOztBQUNBLFlBQVEsS0FBS0MsS0FBTCxDQUFXWCxNQUFuQjtBQUNJLFdBQUtPLHFCQUFxQixDQUFDSyxLQUEzQjtBQUNJLGVBQU8seUJBQUcsc0NBQUgsRUFBMkM7QUFBRUMsVUFBQUEsV0FBVyxFQUFFLEtBQUtGLEtBQUwsQ0FBV1A7QUFBMUIsU0FBM0MsQ0FBUDs7QUFDSixXQUFLRyxxQkFBcUIsQ0FBQ08sUUFBM0I7QUFDSSxlQUFPLHlCQUFHLDJCQUFILENBQVA7O0FBQ0osV0FBS1AscUJBQXFCLENBQUNRLFlBQTNCO0FBQ0ksZUFBTyx5QkFBRyxzQkFBSCxDQUFQOztBQUNKLFdBQUtSLHFCQUFxQixDQUFDUyxXQUEzQjtBQUNJLGVBQU8seUJBQUcsdUJBQUgsQ0FBUDtBQVJSO0FBVUgsR0E1QjJCO0FBOEI1QkMsRUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEJULHlCQUFZQyxHQUFaLEdBQWtCUyxlQUFsQjtBQUNILEdBaEMyQjtBQWtDNUJDLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsVUFBTUMsT0FBTyxHQUFHLEtBQUtkLGFBQUwsRUFBaEI7QUFDQSxVQUFNZSxPQUFPLEdBQUcseUJBQUcsU0FBSCxDQUFoQjs7QUFFQSxRQUFJLEVBQUUsOEJBQThCYixxQkFBWUMsR0FBWixFQUFoQyxDQUFKLEVBQXdEO0FBQ3BELGFBQU8seUNBQVA7QUFDSDs7QUFFRCxVQUFNRixxQkFBcUIsR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHdCQUFsQixFQUE5Qjs7QUFDQSxVQUFNWSxZQUFZLEdBQUcsQ0FDakJmLHFCQUFxQixDQUFDSyxLQURMLEVBRWpCTCxxQkFBcUIsQ0FBQ1EsWUFGTCxDQUFyQjtBQUtBLFFBQUlRLEtBQUo7O0FBQ0EsUUFBSUQsWUFBWSxDQUFDRSxRQUFiLENBQXNCLEtBQUtiLEtBQUwsQ0FBV1gsTUFBakMsQ0FBSixFQUE4QztBQUMxQ3VCLE1BQUFBLEtBQUssR0FBRztBQUFLLFFBQUEsU0FBUyxFQUFDLDBCQUFmO0FBQTBDLFFBQUEsR0FBRyxFQUFFRSxPQUFPLENBQUMsaUNBQUQsQ0FBdEQ7QUFBMkYsUUFBQSxLQUFLLEVBQUMsSUFBakc7QUFBc0csUUFBQSxNQUFNLEVBQUMsSUFBN0c7QUFBa0gsUUFBQSxHQUFHLEVBQUM7QUFBdEgsUUFBUjtBQUNILEtBRkQsTUFFTztBQUNIRixNQUFBQSxLQUFLLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQywwQkFBZjtBQUEwQyxRQUFBLEdBQUcsRUFBRUUsT0FBTyxDQUFDLGlDQUFELENBQXREO0FBQTJGLFFBQUEsS0FBSyxFQUFDLElBQWpHO0FBQXNHLFFBQUEsTUFBTSxFQUFDLElBQTdHO0FBQWtILFFBQUEsR0FBRyxFQUFDO0FBQXRILFFBQVI7QUFDSDs7QUFFRCxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLRixLQURMLEVBRUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0tILE9BREwsQ0FGSixFQUtJLDZCQUFDLHlCQUFEO0FBQWtCLE1BQUEsU0FBUyxFQUFDLHdCQUE1QjtBQUFxRCxNQUFBLE9BQU8sRUFBRSxLQUFLSDtBQUFuRSxPQUNJO0FBQUssTUFBQSxHQUFHLEVBQUVRLE9BQU8sQ0FBQyxnQ0FBRCxDQUFqQjtBQUFxRCxNQUFBLEtBQUssRUFBQyxJQUEzRDtBQUFnRSxNQUFBLE1BQU0sRUFBQyxJQUF2RTtBQUE0RSxNQUFBLEdBQUcsRUFBRSx5QkFBRyxPQUFIO0FBQWpGLE1BREosQ0FMSixDQURKO0FBV0g7QUFsRTJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBQbGF0Zm9ybVBlZyBmcm9tICcuLi8uLi8uLi9QbGF0Zm9ybVBlZyc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gJy4uLy4uLy4uL2NvbXBvbmVudHMvdmlld3MvZWxlbWVudHMvQWNjZXNzaWJsZUJ1dHRvbic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVSZWFjdENsYXNzKHtcclxuICAgIHByb3BUeXBlczoge1xyXG4gICAgICAgIHN0YXR1czogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIC8vIEN1cnJlbnRseSBmb3IgZXJyb3IgZGV0YWlsIGJ1dCB3aWxsIGJlIHVzYWJsZSBmb3IgZG93bmxvYWQgcHJvZ3Jlc3NcclxuICAgICAgICAvLyBvbmNlIHRoYXQgaXMgYSB0aGluZyB0aGF0IHNxdWlycmVsIHBhc3NlcyB0aHJvdWdoIGVsZWN0cm9uLlxyXG4gICAgICAgIGRldGFpbDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBkZXRhaWw6ICcnLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFN0YXR1c1RleHQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIC8vIHdlIGNhbid0IGltcG9ydCB0aGUgZW51bSBmcm9tIHJpb3Qtd2ViIGFzIHdlIGRvbid0IHdhbnQgbWF0cml4LXJlYWN0LXNka1xyXG4gICAgICAgIC8vIHRvIGRlcGVuZCBvbiByaW90LXdlYi4gc28gd2UgZ3JhYiBpdCBhcyBhIG5vcm1hbCBvYmplY3QgdmlhIEFQSSBpbnN0ZWFkLlxyXG4gICAgICAgIGNvbnN0IHVwZGF0ZUNoZWNrU3RhdHVzRW51bSA9IFBsYXRmb3JtUGVnLmdldCgpLmdldFVwZGF0ZUNoZWNrU3RhdHVzRW51bSgpO1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5wcm9wcy5zdGF0dXMpIHtcclxuICAgICAgICAgICAgY2FzZSB1cGRhdGVDaGVja1N0YXR1c0VudW0uRVJST1I6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ0Vycm9yIGVuY291bnRlcmVkICglKGVycm9yRGV0YWlsKXMpLicsIHsgZXJyb3JEZXRhaWw6IHRoaXMucHJvcHMuZGV0YWlsIH0pO1xyXG4gICAgICAgICAgICBjYXNlIHVwZGF0ZUNoZWNrU3RhdHVzRW51bS5DSEVDS0lORzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnQ2hlY2tpbmcgZm9yIGFuIHVwZGF0ZS4uLicpO1xyXG4gICAgICAgICAgICBjYXNlIHVwZGF0ZUNoZWNrU3RhdHVzRW51bS5OT1RBVkFJTEFCTEU6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ05vIHVwZGF0ZSBhdmFpbGFibGUuJyk7XHJcbiAgICAgICAgICAgIGNhc2UgdXBkYXRlQ2hlY2tTdGF0dXNFbnVtLkRPV05MT0FESU5HOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdEb3dubG9hZGluZyB1cGRhdGUuLi4nKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGhpZGVUb29sYmFyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5zdG9wVXBkYXRlQ2hlY2soKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBtZXNzYWdlID0gdGhpcy5nZXRTdGF0dXNUZXh0KCk7XHJcbiAgICAgICAgY29uc3Qgd2FybmluZyA9IF90KCdXYXJuaW5nJyk7XHJcblxyXG4gICAgICAgIGlmICghKCdnZXRVcGRhdGVDaGVja1N0YXR1c0VudW0nIGluIFBsYXRmb3JtUGVnLmdldCgpKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gPGRpdj48L2Rpdj47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB1cGRhdGVDaGVja1N0YXR1c0VudW0gPSBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRVcGRhdGVDaGVja1N0YXR1c0VudW0oKTtcclxuICAgICAgICBjb25zdCBkb25lU3RhdHVzZXMgPSBbXHJcbiAgICAgICAgICAgIHVwZGF0ZUNoZWNrU3RhdHVzRW51bS5FUlJPUixcclxuICAgICAgICAgICAgdXBkYXRlQ2hlY2tTdGF0dXNFbnVtLk5PVEFWQUlMQUJMRSxcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICBsZXQgaW1hZ2U7XHJcbiAgICAgICAgaWYgKGRvbmVTdGF0dXNlcy5pbmNsdWRlcyh0aGlzLnByb3BzLnN0YXR1cykpIHtcclxuICAgICAgICAgICAgaW1hZ2UgPSA8aW1nIGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfd2FybmluZ1wiIHNyYz17cmVxdWlyZShcIi4uLy4uLy4uLy4uL3Jlcy9pbWcvd2FybmluZy5zdmdcIil9IHdpZHRoPVwiMjRcIiBoZWlnaHQ9XCIyM1wiIGFsdD1cIlwiIC8+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGltYWdlID0gPGltZyBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX3dhcm5pbmdcIiBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL3NwaW5uZXIuZ2lmXCIpfSB3aWR0aD1cIjI0XCIgaGVpZ2h0PVwiMjNcIiBhbHQ9XCJcIiAvPjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhclwiPlxyXG4gICAgICAgICAgICAgICAge2ltYWdlfVxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7bWVzc2FnZX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24gY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhcl9jbG9zZVwiIG9uQ2xpY2s9e3RoaXMuaGlkZVRvb2xiYXJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vLi4vcmVzL2ltZy9jYW5jZWwuc3ZnXCIpfSB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIiBhbHQ9e190KCdDbG9zZScpfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9BY2Nlc3NpYmxlQnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfSxcclxufSk7XHJcbiJdfQ==