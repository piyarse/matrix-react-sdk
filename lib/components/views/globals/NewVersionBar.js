"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _createReactClass = _interopRequireDefault(require("create-react-class"));

var sdk = _interopRequireWildcard(require("../../../index"));

var _Modal = _interopRequireDefault(require("../../../Modal"));

var _PlatformPeg = _interopRequireDefault(require("../../../PlatformPeg"));

var _languageHandler = require("../../../languageHandler");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Check a version string is compatible with the Changelog
 * dialog ([vectorversion]-react-[react-sdk-version]-js-[js-sdk-version])
 */
function checkVersion(ver) {
  const parts = ver.split('-');
  return parts.length == 5 && parts[1] == 'react' && parts[3] == 'js';
}

var _default = (0, _createReactClass.default)({
  displayName: "NewVersionBar",
  propTypes: {
    version: _propTypes.default.string.isRequired,
    newVersion: _propTypes.default.string.isRequired,
    releaseNotes: _propTypes.default.string
  },
  displayReleaseNotes: function (releaseNotes) {
    const QuestionDialog = sdk.getComponent('dialogs.QuestionDialog');

    _Modal.default.createTrackedDialog('Display release notes', '', QuestionDialog, {
      title: (0, _languageHandler._t)("What's New"),
      description: _react.default.createElement("div", {
        className: "mx_MatrixToolbar_changelog"
      }, releaseNotes),
      button: (0, _languageHandler._t)("Update"),
      onFinished: update => {
        if (update && _PlatformPeg.default.get()) {
          _PlatformPeg.default.get().installUpdate();
        }
      }
    });
  },
  displayChangelog: function () {
    const ChangelogDialog = sdk.getComponent('dialogs.ChangelogDialog');

    _Modal.default.createTrackedDialog('Display Changelog', '', ChangelogDialog, {
      version: this.props.version,
      newVersion: this.props.newVersion,
      onFinished: update => {
        if (update && _PlatformPeg.default.get()) {
          _PlatformPeg.default.get().installUpdate();
        }
      }
    });
  },
  onUpdateClicked: function () {
    _PlatformPeg.default.get().installUpdate();
  },
  render: function () {
    let action_button; // If we have release notes to display, we display them. Otherwise,
    // we display the Changelog Dialog which takes two versions and
    // automatically tells you what's changed (provided the versions
    // are in the right format)

    if (this.props.releaseNotes) {
      action_button = _react.default.createElement("button", {
        className: "mx_MatrixToolbar_action",
        onClick: this.displayReleaseNotes
      }, (0, _languageHandler._t)("What's new?"));
    } else if (checkVersion(this.props.version) && checkVersion(this.props.newVersion)) {
      action_button = _react.default.createElement("button", {
        className: "mx_MatrixToolbar_action",
        onClick: this.displayChangelog
      }, (0, _languageHandler._t)("What's new?"));
    } else if (_PlatformPeg.default.get()) {
      action_button = _react.default.createElement("button", {
        className: "mx_MatrixToolbar_action",
        onClick: this.onUpdateClicked
      }, (0, _languageHandler._t)("Update"));
    }

    return _react.default.createElement("div", {
      className: "mx_MatrixToolbar"
    }, _react.default.createElement("img", {
      className: "mx_MatrixToolbar_warning",
      src: require("../../../../res/img/warning.svg"),
      width: "24",
      height: "23",
      alt: ""
    }), _react.default.createElement("div", {
      className: "mx_MatrixToolbar_content"
    }, (0, _languageHandler._t)("A new version of Riot is available.")), action_button);
  }
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL3ZpZXdzL2dsb2JhbHMvTmV3VmVyc2lvbkJhci5qcyJdLCJuYW1lcyI6WyJjaGVja1ZlcnNpb24iLCJ2ZXIiLCJwYXJ0cyIsInNwbGl0IiwibGVuZ3RoIiwicHJvcFR5cGVzIiwidmVyc2lvbiIsIlByb3BUeXBlcyIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJuZXdWZXJzaW9uIiwicmVsZWFzZU5vdGVzIiwiZGlzcGxheVJlbGVhc2VOb3RlcyIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImJ1dHRvbiIsIm9uRmluaXNoZWQiLCJ1cGRhdGUiLCJQbGF0Zm9ybVBlZyIsImdldCIsImluc3RhbGxVcGRhdGUiLCJkaXNwbGF5Q2hhbmdlbG9nIiwiQ2hhbmdlbG9nRGlhbG9nIiwicHJvcHMiLCJvblVwZGF0ZUNsaWNrZWQiLCJyZW5kZXIiLCJhY3Rpb25fYnV0dG9uIiwicmVxdWlyZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTs7OztBQUlBLFNBQVNBLFlBQVQsQ0FBc0JDLEdBQXRCLEVBQTJCO0FBQ3ZCLFFBQU1DLEtBQUssR0FBR0QsR0FBRyxDQUFDRSxLQUFKLENBQVUsR0FBVixDQUFkO0FBQ0EsU0FBT0QsS0FBSyxDQUFDRSxNQUFOLElBQWdCLENBQWhCLElBQXFCRixLQUFLLENBQUMsQ0FBRCxDQUFMLElBQVksT0FBakMsSUFBNENBLEtBQUssQ0FBQyxDQUFELENBQUwsSUFBWSxJQUEvRDtBQUNIOztlQUVjLCtCQUFpQjtBQUFBO0FBQzVCRyxFQUFBQSxTQUFTLEVBQUU7QUFDUEMsSUFBQUEsT0FBTyxFQUFFQyxtQkFBVUMsTUFBVixDQUFpQkMsVUFEbkI7QUFFUEMsSUFBQUEsVUFBVSxFQUFFSCxtQkFBVUMsTUFBVixDQUFpQkMsVUFGdEI7QUFHUEUsSUFBQUEsWUFBWSxFQUFFSixtQkFBVUM7QUFIakIsR0FEaUI7QUFPNUJJLEVBQUFBLG1CQUFtQixFQUFFLFVBQVNELFlBQVQsRUFBdUI7QUFDeEMsVUFBTUUsY0FBYyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQXZCOztBQUNBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsdUJBQTFCLEVBQW1ELEVBQW5ELEVBQXVESixjQUF2RCxFQUF1RTtBQUNuRUssTUFBQUEsS0FBSyxFQUFFLHlCQUFHLFlBQUgsQ0FENEQ7QUFFbkVDLE1BQUFBLFdBQVcsRUFBRTtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FBNkNSLFlBQTdDLENBRnNEO0FBR25FUyxNQUFBQSxNQUFNLEVBQUUseUJBQUcsUUFBSCxDQUgyRDtBQUluRUMsTUFBQUEsVUFBVSxFQUFHQyxNQUFELElBQVk7QUFDcEIsWUFBSUEsTUFBTSxJQUFJQyxxQkFBWUMsR0FBWixFQUFkLEVBQWlDO0FBQzdCRCwrQkFBWUMsR0FBWixHQUFrQkMsYUFBbEI7QUFDSDtBQUNKO0FBUmtFLEtBQXZFO0FBVUgsR0FuQjJCO0FBcUI1QkMsRUFBQUEsZ0JBQWdCLEVBQUUsWUFBVztBQUN6QixVQUFNQyxlQUFlLEdBQUdiLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5QkFBakIsQ0FBeEI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQixtQkFBMUIsRUFBK0MsRUFBL0MsRUFBbURVLGVBQW5ELEVBQW9FO0FBQ2hFckIsTUFBQUEsT0FBTyxFQUFFLEtBQUtzQixLQUFMLENBQVd0QixPQUQ0QztBQUVoRUksTUFBQUEsVUFBVSxFQUFFLEtBQUtrQixLQUFMLENBQVdsQixVQUZ5QztBQUdoRVcsTUFBQUEsVUFBVSxFQUFHQyxNQUFELElBQVk7QUFDcEIsWUFBSUEsTUFBTSxJQUFJQyxxQkFBWUMsR0FBWixFQUFkLEVBQWlDO0FBQzdCRCwrQkFBWUMsR0FBWixHQUFrQkMsYUFBbEI7QUFDSDtBQUNKO0FBUCtELEtBQXBFO0FBU0gsR0FoQzJCO0FBa0M1QkksRUFBQUEsZUFBZSxFQUFFLFlBQVc7QUFDeEJOLHlCQUFZQyxHQUFaLEdBQWtCQyxhQUFsQjtBQUNILEdBcEMyQjtBQXNDNUJLLEVBQUFBLE1BQU0sRUFBRSxZQUFXO0FBQ2YsUUFBSUMsYUFBSixDQURlLENBRWY7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLSCxLQUFMLENBQVdqQixZQUFmLEVBQTZCO0FBQ3pCb0IsTUFBQUEsYUFBYSxHQUNUO0FBQVEsUUFBQSxTQUFTLEVBQUMseUJBQWxCO0FBQTRDLFFBQUEsT0FBTyxFQUFFLEtBQUtuQjtBQUExRCxTQUNNLHlCQUFHLGFBQUgsQ0FETixDQURKO0FBS0gsS0FORCxNQU1PLElBQUlaLFlBQVksQ0FBQyxLQUFLNEIsS0FBTCxDQUFXdEIsT0FBWixDQUFaLElBQW9DTixZQUFZLENBQUMsS0FBSzRCLEtBQUwsQ0FBV2xCLFVBQVosQ0FBcEQsRUFBNkU7QUFDaEZxQixNQUFBQSxhQUFhLEdBQ1Q7QUFBUSxRQUFBLFNBQVMsRUFBQyx5QkFBbEI7QUFBNEMsUUFBQSxPQUFPLEVBQUUsS0FBS0w7QUFBMUQsU0FDTSx5QkFBRyxhQUFILENBRE4sQ0FESjtBQUtILEtBTk0sTUFNQSxJQUFJSCxxQkFBWUMsR0FBWixFQUFKLEVBQXVCO0FBQzFCTyxNQUFBQSxhQUFhLEdBQ1Q7QUFBUSxRQUFBLFNBQVMsRUFBQyx5QkFBbEI7QUFBNEMsUUFBQSxPQUFPLEVBQUUsS0FBS0Y7QUFBMUQsU0FDTSx5QkFBRyxRQUFILENBRE4sQ0FESjtBQUtIOztBQUNELFdBQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQywwQkFBZjtBQUEwQyxNQUFBLEdBQUcsRUFBRUcsT0FBTyxDQUFDLGlDQUFELENBQXREO0FBQTJGLE1BQUEsS0FBSyxFQUFDLElBQWpHO0FBQXNHLE1BQUEsTUFBTSxFQUFDLElBQTdHO0FBQWtILE1BQUEsR0FBRyxFQUFDO0FBQXRILE1BREosRUFFSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSyx5QkFBRyxxQ0FBSCxDQURMLENBRkosRUFLS0QsYUFMTCxDQURKO0FBU0g7QUF4RTJCLENBQWpCLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgY3JlYXRlUmVhY3RDbGFzcyBmcm9tICdjcmVhdGUtcmVhY3QtY2xhc3MnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSAnLi4vLi4vLi4vUGxhdGZvcm1QZWcnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcblxyXG4vKipcclxuICogQ2hlY2sgYSB2ZXJzaW9uIHN0cmluZyBpcyBjb21wYXRpYmxlIHdpdGggdGhlIENoYW5nZWxvZ1xyXG4gKiBkaWFsb2cgKFt2ZWN0b3J2ZXJzaW9uXS1yZWFjdC1bcmVhY3Qtc2RrLXZlcnNpb25dLWpzLVtqcy1zZGstdmVyc2lvbl0pXHJcbiAqL1xyXG5mdW5jdGlvbiBjaGVja1ZlcnNpb24odmVyKSB7XHJcbiAgICBjb25zdCBwYXJ0cyA9IHZlci5zcGxpdCgnLScpO1xyXG4gICAgcmV0dXJuIHBhcnRzLmxlbmd0aCA9PSA1ICYmIHBhcnRzWzFdID09ICdyZWFjdCcgJiYgcGFydHNbM10gPT0gJ2pzJztcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlUmVhY3RDbGFzcyh7XHJcbiAgICBwcm9wVHlwZXM6IHtcclxuICAgICAgICB2ZXJzaW9uOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXHJcbiAgICAgICAgbmV3VmVyc2lvbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgICAgIHJlbGVhc2VOb3RlczogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIH0sXHJcblxyXG4gICAgZGlzcGxheVJlbGVhc2VOb3RlczogZnVuY3Rpb24ocmVsZWFzZU5vdGVzKSB7XHJcbiAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLlF1ZXN0aW9uRGlhbG9nJyk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRGlzcGxheSByZWxlYXNlIG5vdGVzJywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBfdChcIldoYXQncyBOZXdcIiksXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8ZGl2IGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfY2hhbmdlbG9nXCI+e3JlbGVhc2VOb3Rlc308L2Rpdj4sXHJcbiAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJVcGRhdGVcIiksXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6ICh1cGRhdGUpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh1cGRhdGUgJiYgUGxhdGZvcm1QZWcuZ2V0KCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBQbGF0Zm9ybVBlZy5nZXQoKS5pbnN0YWxsVXBkYXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGRpc3BsYXlDaGFuZ2Vsb2c6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IENoYW5nZWxvZ0RpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuQ2hhbmdlbG9nRGlhbG9nJyk7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRGlzcGxheSBDaGFuZ2Vsb2cnLCAnJywgQ2hhbmdlbG9nRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHZlcnNpb246IHRoaXMucHJvcHMudmVyc2lvbixcclxuICAgICAgICAgICAgbmV3VmVyc2lvbjogdGhpcy5wcm9wcy5uZXdWZXJzaW9uLFxyXG4gICAgICAgICAgICBvbkZpbmlzaGVkOiAodXBkYXRlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodXBkYXRlICYmIFBsYXRmb3JtUGVnLmdldCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgUGxhdGZvcm1QZWcuZ2V0KCkuaW5zdGFsbFVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblVwZGF0ZUNsaWNrZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIFBsYXRmb3JtUGVnLmdldCgpLmluc3RhbGxVcGRhdGUoKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgYWN0aW9uX2J1dHRvbjtcclxuICAgICAgICAvLyBJZiB3ZSBoYXZlIHJlbGVhc2Ugbm90ZXMgdG8gZGlzcGxheSwgd2UgZGlzcGxheSB0aGVtLiBPdGhlcndpc2UsXHJcbiAgICAgICAgLy8gd2UgZGlzcGxheSB0aGUgQ2hhbmdlbG9nIERpYWxvZyB3aGljaCB0YWtlcyB0d28gdmVyc2lvbnMgYW5kXHJcbiAgICAgICAgLy8gYXV0b21hdGljYWxseSB0ZWxscyB5b3Ugd2hhdCdzIGNoYW5nZWQgKHByb3ZpZGVkIHRoZSB2ZXJzaW9uc1xyXG4gICAgICAgIC8vIGFyZSBpbiB0aGUgcmlnaHQgZm9ybWF0KVxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnJlbGVhc2VOb3Rlcykge1xyXG4gICAgICAgICAgICBhY3Rpb25fYnV0dG9uID0gKFxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX2FjdGlvblwiIG9uQ2xpY2s9e3RoaXMuZGlzcGxheVJlbGVhc2VOb3Rlc30+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBfdChcIldoYXQncyBuZXc/XCIpIH1cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoY2hlY2tWZXJzaW9uKHRoaXMucHJvcHMudmVyc2lvbikgJiYgY2hlY2tWZXJzaW9uKHRoaXMucHJvcHMubmV3VmVyc2lvbikpIHtcclxuICAgICAgICAgICAgYWN0aW9uX2J1dHRvbiA9IChcclxuICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhcl9hY3Rpb25cIiBvbkNsaWNrPXt0aGlzLmRpc3BsYXlDaGFuZ2Vsb2d9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJXaGF0J3MgbmV3P1wiKSB9XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKFBsYXRmb3JtUGVnLmdldCgpKSB7XHJcbiAgICAgICAgICAgIGFjdGlvbl9idXR0b24gPSAoXHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cIm14X01hdHJpeFRvb2xiYXJfYWN0aW9uXCIgb25DbGljaz17dGhpcy5vblVwZGF0ZUNsaWNrZWR9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgX3QoXCJVcGRhdGVcIikgfVxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfTWF0cml4VG9vbGJhclwiPlxyXG4gICAgICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX3dhcm5pbmdcIiBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi9yZXMvaW1nL3dhcm5pbmcuc3ZnXCIpfSB3aWR0aD1cIjI0XCIgaGVpZ2h0PVwiMjNcIiBhbHQ9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9NYXRyaXhUb29sYmFyX2NvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7X3QoXCJBIG5ldyB2ZXJzaW9uIG9mIFJpb3QgaXMgYXZhaWxhYmxlLlwiKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAge2FjdGlvbl9idXR0b259XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19