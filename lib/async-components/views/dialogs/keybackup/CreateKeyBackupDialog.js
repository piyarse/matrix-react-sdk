"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _fileSaver = _interopRequireDefault(require("file-saver"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _PasswordScorer = require("../../../../utils/PasswordScorer");

var _languageHandler = require("../../../../languageHandler");

var _CrossSigningManager = require("../../../../CrossSigningManager");

var _SettingsStore = _interopRequireDefault(require("../../../../settings/SettingsStore"));

var _AccessibleButton = _interopRequireDefault(require("../../../../components/views/elements/AccessibleButton"));

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const PHASE_PASSPHRASE = 0;
const PHASE_PASSPHRASE_CONFIRM = 1;
const PHASE_SHOWKEY = 2;
const PHASE_KEEPITSAFE = 3;
const PHASE_BACKINGUP = 4;
const PHASE_DONE = 5;
const PHASE_OPTOUT_CONFIRM = 6;
const PASSWORD_MIN_SCORE = 4; // So secure, many characters, much complex, wow, etc, etc.

const PASSPHRASE_FEEDBACK_DELAY = 500; // How long after keystroke to offer passphrase feedback, ms.
// XXX: copied from ShareDialog: factor out into utils

function selectText(target) {
  const range = document.createRange();
  range.selectNodeContents(target);
  const selection = window.getSelection();
  selection.removeAllRanges();
  selection.addRange(range);
}
/*
 * Walks the user through the process of creating an e2e key backup
 * on the server.
 */


class CreateKeyBackupDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_collectRecoveryKeyNode", n => {
      this._recoveryKeyNode = n;
    });
    (0, _defineProperty2.default)(this, "_onCopyClick", () => {
      selectText(this._recoveryKeyNode);
      const successful = document.execCommand('copy');

      if (successful) {
        this.setState({
          copied: true,
          phase: PHASE_KEEPITSAFE
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onDownloadClick", () => {
      const blob = new Blob([this._keyBackupInfo.recovery_key], {
        type: 'text/plain;charset=us-ascii'
      });

      _fileSaver.default.saveAs(blob, 'recovery-key.txt');

      this.setState({
        downloaded: true,
        phase: PHASE_KEEPITSAFE
      });
    });
    (0, _defineProperty2.default)(this, "_createBackup", async () => {
      const {
        secureSecretStorage
      } = this.state;
      this.setState({
        phase: PHASE_BACKINGUP,
        error: null
      });
      let info;

      try {
        if (secureSecretStorage) {
          await (0, _CrossSigningManager.accessSecretStorage)(async () => {
            info = await _MatrixClientPeg.MatrixClientPeg.get().prepareKeyBackupVersion(null
            /* random key */
            , {
              secureSecretStorage: true
            });
            info = await _MatrixClientPeg.MatrixClientPeg.get().createKeyBackupVersion(info);
          });
        } else {
          info = await _MatrixClientPeg.MatrixClientPeg.get().createKeyBackupVersion(this._keyBackupInfo);
        }

        await _MatrixClientPeg.MatrixClientPeg.get().scheduleAllGroupSessionsForBackup();
        this.setState({
          phase: PHASE_DONE
        });
      } catch (e) {
        console.error("Error creating key backup", e); // TODO: If creating a version succeeds, but backup fails, should we
        // delete the version, disable backup, or do nothing?  If we just
        // disable without deleting, we'll enable on next app reload since
        // it is trusted.

        if (info) {
          _MatrixClientPeg.MatrixClientPeg.get().deleteKeyBackupVersion(info.version);
        }

        this.setState({
          error: e
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onCancel", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onDone", () => {
      this.props.onFinished(true);
    });
    (0, _defineProperty2.default)(this, "_onOptOutClick", () => {
      this.setState({
        phase: PHASE_OPTOUT_CONFIRM
      });
    });
    (0, _defineProperty2.default)(this, "_onSetUpClick", () => {
      this.setState({
        phase: PHASE_PASSPHRASE
      });
    });
    (0, _defineProperty2.default)(this, "_onSkipPassPhraseClick", async () => {
      this._keyBackupInfo = await _MatrixClientPeg.MatrixClientPeg.get().prepareKeyBackupVersion();
      this.setState({
        copied: false,
        downloaded: false,
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseNextClick", async e => {
      e.preventDefault(); // If we're waiting for the timeout before updating the result at this point,
      // skip ahead and do it now, otherwise we'll deny the attempt to proceed
      // even if the user entered a valid passphrase

      if (this._setZxcvbnResultTimeout !== null) {
        clearTimeout(this._setZxcvbnResultTimeout);
        this._setZxcvbnResultTimeout = null;
        await new Promise(resolve => {
          this.setState({
            zxcvbnResult: (0, _PasswordScorer.scorePassword)(this.state.passPhrase)
          }, resolve);
        });
      }

      if (this._passPhraseIsValid()) {
        this.setState({
          phase: PHASE_PASSPHRASE_CONFIRM
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseConfirmNextClick", async e => {
      e.preventDefault();
      if (this.state.passPhrase !== this.state.passPhraseConfirm) return;
      this._keyBackupInfo = await _MatrixClientPeg.MatrixClientPeg.get().prepareKeyBackupVersion(this.state.passPhrase);
      this.setState({
        copied: false,
        downloaded: false,
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onSetAgainClick", () => {
      this.setState({
        passPhrase: '',
        passPhraseConfirm: '',
        phase: PHASE_PASSPHRASE,
        zxcvbnResult: null
      });
    });
    (0, _defineProperty2.default)(this, "_onKeepItSafeBackClick", () => {
      this.setState({
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseChange", e => {
      this.setState({
        passPhrase: e.target.value
      });

      if (this._setZxcvbnResultTimeout !== null) {
        clearTimeout(this._setZxcvbnResultTimeout);
      }

      this._setZxcvbnResultTimeout = setTimeout(() => {
        this._setZxcvbnResultTimeout = null;
        this.setState({
          // precompute this and keep it in state: zxcvbn is fast but
          // we use it in a couple of different places so no point recomputing
          // it unnecessarily.
          zxcvbnResult: (0, _PasswordScorer.scorePassword)(this.state.passPhrase)
        });
      }, PASSPHRASE_FEEDBACK_DELAY);
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseConfirmChange", e => {
      this.setState({
        passPhraseConfirm: e.target.value
      });
    });
    this._recoveryKeyNode = null;
    this._keyBackupInfo = null;
    this._setZxcvbnResultTimeout = null;
    this.state = {
      secureSecretStorage: null,
      phase: PHASE_PASSPHRASE,
      passPhrase: '',
      passPhraseConfirm: '',
      copied: false,
      downloaded: false,
      zxcvbnResult: null
    };
  }

  async componentDidMount() {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const secureSecretStorage = _SettingsStore.default.isFeatureEnabled("feature_cross_signing") && (await cli.doesServerSupportUnstableFeature("org.matrix.e2e_cross_signing"));
    this.setState({
      secureSecretStorage
    }); // If we're using secret storage, skip ahead to the backing up step, as
    // `accessSecretStorage` will handle passphrases as needed.

    if (secureSecretStorage) {
      this.setState({
        phase: PHASE_BACKINGUP
      });

      this._createBackup();
    }
  }

  componentWillUnmount() {
    if (this._setZxcvbnResultTimeout !== null) {
      clearTimeout(this._setZxcvbnResultTimeout);
    }
  }

  _passPhraseIsValid() {
    return this.state.zxcvbnResult && this.state.zxcvbnResult.score >= PASSWORD_MIN_SCORE;
  }

  _renderPhasePassPhrase() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    let strengthMeter;
    let helpText;

    if (this.state.zxcvbnResult) {
      if (this.state.zxcvbnResult.score >= PASSWORD_MIN_SCORE) {
        helpText = (0, _languageHandler._t)("Great! This passphrase looks strong enough.");
      } else {
        const suggestions = [];

        for (let i = 0; i < this.state.zxcvbnResult.feedback.suggestions.length; ++i) {
          suggestions.push(_react.default.createElement("div", {
            key: i
          }, this.state.zxcvbnResult.feedback.suggestions[i]));
        }

        const suggestionBlock = _react.default.createElement("div", null, suggestions.length > 0 ? suggestions : (0, _languageHandler._t)("Keep going..."));

        helpText = _react.default.createElement("div", null, this.state.zxcvbnResult.feedback.warning, suggestionBlock);
      }

      strengthMeter = _react.default.createElement("div", null, _react.default.createElement("progress", {
        max: PASSWORD_MIN_SCORE,
        value: this.state.zxcvbnResult.score
      }));
    }

    return _react.default.createElement("form", {
      onSubmit: this._onPassPhraseNextClick
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("<b>Warning</b>: You should only set up key backup from a trusted computer.", {}, {
      b: sub => _react.default.createElement("b", null, sub)
    })), _react.default.createElement("p", null, (0, _languageHandler._t)("We'll store an encrypted copy of your keys on our server. " + "Protect your backup with a passphrase to keep it secure.")), _react.default.createElement("p", null, (0, _languageHandler._t)("For maximum security, this should be different from your account password.")), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_primaryContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_passPhraseContainer"
    }, _react.default.createElement("input", {
      type: "password",
      onChange: this._onPassPhraseChange,
      value: this.state.passPhrase,
      className: "mx_CreateKeyBackupDialog_passPhraseInput",
      placeholder: (0, _languageHandler._t)("Enter a passphrase..."),
      autoFocus: true
    }), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_passPhraseHelp"
    }, strengthMeter, helpText))), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Next'),
      onPrimaryButtonClick: this._onPassPhraseNextClick,
      hasCancel: false,
      disabled: !this._passPhraseIsValid()
    }), _react.default.createElement("details", null, _react.default.createElement("summary", null, (0, _languageHandler._t)("Advanced")), _react.default.createElement(_AccessibleButton.default, {
      kind: "primary",
      onClick: this._onSkipPassPhraseClick
    }, (0, _languageHandler._t)("Set up with a recovery key"))));
  }

  _renderPhasePassPhraseConfirm() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    let matchText;

    if (this.state.passPhraseConfirm === this.state.passPhrase) {
      matchText = (0, _languageHandler._t)("That matches!");
    } else if (!this.state.passPhrase.startsWith(this.state.passPhraseConfirm)) {
      // only tell them they're wrong if they've actually gone wrong.
      // Security concious readers will note that if you left riot-web unattended
      // on this screen, this would make it easy for a malicious person to guess
      // your passphrase one letter at a time, but they could get this faster by
      // just opening the browser's developer tools and reading it.
      // Note that not having typed anything at all will not hit this clause and
      // fall through so empty box === no hint.
      matchText = (0, _languageHandler._t)("That doesn't match.");
    }

    let passPhraseMatch = null;

    if (matchText) {
      passPhraseMatch = _react.default.createElement("div", {
        className: "mx_CreateKeyBackupDialog_passPhraseMatch"
      }, _react.default.createElement("div", null, matchText), _react.default.createElement("div", null, _react.default.createElement(AccessibleButton, {
        element: "span",
        className: "mx_linkButton",
        onClick: this._onSetAgainClick
      }, (0, _languageHandler._t)("Go back to set it again."))));
    }

    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("form", {
      onSubmit: this._onPassPhraseConfirmNextClick
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Please enter your passphrase a second time to confirm.")), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_primaryContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_passPhraseContainer"
    }, _react.default.createElement("div", null, _react.default.createElement("input", {
      type: "password",
      onChange: this._onPassPhraseConfirmChange,
      value: this.state.passPhraseConfirm,
      className: "mx_CreateKeyBackupDialog_passPhraseInput",
      placeholder: (0, _languageHandler._t)("Repeat your passphrase..."),
      autoFocus: true
    })), passPhraseMatch)), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Next'),
      onPrimaryButtonClick: this._onPassPhraseConfirmNextClick,
      hasCancel: false,
      disabled: this.state.passPhrase !== this.state.passPhraseConfirm
    }));
  }

  _renderPhaseShowKey() {
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your recovery key is a safety net - you can use it to restore " + "access to your encrypted messages if you forget your passphrase.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Keep a copy of it somewhere secure, like a password manager or even a safe.")), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_primaryContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_recoveryKeyHeader"
    }, (0, _languageHandler._t)("Your recovery key")), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_recoveryKeyContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_recoveryKey"
    }, _react.default.createElement("code", {
      ref: this._collectRecoveryKeyNode
    }, this._keyBackupInfo.recovery_key)), _react.default.createElement("div", {
      className: "mx_CreateKeyBackupDialog_recoveryKeyButtons"
    }, _react.default.createElement("button", {
      className: "mx_Dialog_primary",
      onClick: this._onCopyClick
    }, (0, _languageHandler._t)("Copy")), _react.default.createElement("button", {
      className: "mx_Dialog_primary",
      onClick: this._onDownloadClick
    }, (0, _languageHandler._t)("Download"))))));
  }

  _renderPhaseKeepItSafe() {
    let introText;

    if (this.state.copied) {
      introText = (0, _languageHandler._t)("Your recovery key has been <b>copied to your clipboard</b>, paste it to:", {}, {
        b: s => _react.default.createElement("b", null, s)
      });
    } else if (this.state.downloaded) {
      introText = (0, _languageHandler._t)("Your recovery key is in your <b>Downloads</b> folder.", {}, {
        b: s => _react.default.createElement("b", null, s)
      });
    }

    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, introText, _react.default.createElement("ul", null, _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Print it</b> and store it somewhere safe", {}, {
      b: s => _react.default.createElement("b", null, s)
    })), _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Save it</b> on a USB key or backup drive", {}, {
      b: s => _react.default.createElement("b", null, s)
    })), _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Copy it</b> to your personal cloud storage", {}, {
      b: s => _react.default.createElement("b", null, s)
    }))), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Continue"),
      onPrimaryButtonClick: this._createBackup,
      hasCancel: false
    }, _react.default.createElement("button", {
      onClick: this._onKeepItSafeBackClick
    }, (0, _languageHandler._t)("Back"))));
  }

  _renderBusyPhase(text) {
    const Spinner = sdk.getComponent('views.elements.Spinner');
    return _react.default.createElement("div", null, _react.default.createElement(Spinner, null));
  }

  _renderPhaseDone() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your keys are being backed up (the first backup could take a few minutes).")), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('OK'),
      onPrimaryButtonClick: this._onDone,
      hasCancel: false
    }));
  }

  _renderPhaseOptOutConfirm() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, (0, _languageHandler._t)("Without setting up Secure Message Recovery, you won't be able to restore your " + "encrypted message history if you log out or use another session."), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Set up Secure Message Recovery'),
      onPrimaryButtonClick: this._onSetUpClick,
      hasCancel: false
    }, _react.default.createElement("button", {
      onClick: this._onCancel
    }, "I understand, continue without")));
  }

  _titleForPhase(phase) {
    switch (phase) {
      case PHASE_PASSPHRASE:
        return (0, _languageHandler._t)('Secure your backup with a passphrase');

      case PHASE_PASSPHRASE_CONFIRM:
        return (0, _languageHandler._t)('Confirm your passphrase');

      case PHASE_OPTOUT_CONFIRM:
        return (0, _languageHandler._t)('Warning!');

      case PHASE_SHOWKEY:
      case PHASE_KEEPITSAFE:
        return (0, _languageHandler._t)('Make a copy of your recovery key');

      case PHASE_BACKINGUP:
        return (0, _languageHandler._t)('Starting backup...');

      case PHASE_DONE:
        return (0, _languageHandler._t)('Success!');

      default:
        return (0, _languageHandler._t)("Create key backup");
    }
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    let content;

    if (this.state.error) {
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Unable to create key backup")), _react.default.createElement("div", {
        className: "mx_Dialog_buttons"
      }, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Retry'),
        onPrimaryButtonClick: this._createBackup,
        hasCancel: true,
        onCancel: this._onCancel
      })));
    } else {
      switch (this.state.phase) {
        case PHASE_PASSPHRASE:
          content = this._renderPhasePassPhrase();
          break;

        case PHASE_PASSPHRASE_CONFIRM:
          content = this._renderPhasePassPhraseConfirm();
          break;

        case PHASE_SHOWKEY:
          content = this._renderPhaseShowKey();
          break;

        case PHASE_KEEPITSAFE:
          content = this._renderPhaseKeepItSafe();
          break;

        case PHASE_BACKINGUP:
          content = this._renderBusyPhase();
          break;

        case PHASE_DONE:
          content = this._renderPhaseDone();
          break;

        case PHASE_OPTOUT_CONFIRM:
          content = this._renderPhaseOptOutConfirm();
          break;
      }
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_CreateKeyBackupDialog",
      onFinished: this.props.onFinished,
      title: this._titleForPhase(this.state.phase),
      hasCancel: [PHASE_PASSPHRASE, PHASE_DONE].includes(this.state.phase)
    }, _react.default.createElement("div", null, content));
  }

}

exports.default = CreateKeyBackupDialog;
(0, _defineProperty2.default)(CreateKeyBackupDialog, "propTypes", {
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9hc3luYy1jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mva2V5YmFja3VwL0NyZWF0ZUtleUJhY2t1cERpYWxvZy5qcyJdLCJuYW1lcyI6WyJQSEFTRV9QQVNTUEhSQVNFIiwiUEhBU0VfUEFTU1BIUkFTRV9DT05GSVJNIiwiUEhBU0VfU0hPV0tFWSIsIlBIQVNFX0tFRVBJVFNBRkUiLCJQSEFTRV9CQUNLSU5HVVAiLCJQSEFTRV9ET05FIiwiUEhBU0VfT1BUT1VUX0NPTkZJUk0iLCJQQVNTV09SRF9NSU5fU0NPUkUiLCJQQVNTUEhSQVNFX0ZFRURCQUNLX0RFTEFZIiwic2VsZWN0VGV4dCIsInRhcmdldCIsInJhbmdlIiwiZG9jdW1lbnQiLCJjcmVhdGVSYW5nZSIsInNlbGVjdE5vZGVDb250ZW50cyIsInNlbGVjdGlvbiIsIndpbmRvdyIsImdldFNlbGVjdGlvbiIsInJlbW92ZUFsbFJhbmdlcyIsImFkZFJhbmdlIiwiQ3JlYXRlS2V5QmFja3VwRGlhbG9nIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsIm4iLCJfcmVjb3ZlcnlLZXlOb2RlIiwic3VjY2Vzc2Z1bCIsImV4ZWNDb21tYW5kIiwic2V0U3RhdGUiLCJjb3BpZWQiLCJwaGFzZSIsImJsb2IiLCJCbG9iIiwiX2tleUJhY2t1cEluZm8iLCJyZWNvdmVyeV9rZXkiLCJ0eXBlIiwiRmlsZVNhdmVyIiwic2F2ZUFzIiwiZG93bmxvYWRlZCIsInNlY3VyZVNlY3JldFN0b3JhZ2UiLCJzdGF0ZSIsImVycm9yIiwiaW5mbyIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsInByZXBhcmVLZXlCYWNrdXBWZXJzaW9uIiwiY3JlYXRlS2V5QmFja3VwVmVyc2lvbiIsInNjaGVkdWxlQWxsR3JvdXBTZXNzaW9uc0ZvckJhY2t1cCIsImUiLCJjb25zb2xlIiwiZGVsZXRlS2V5QmFja3VwVmVyc2lvbiIsInZlcnNpb24iLCJvbkZpbmlzaGVkIiwicHJldmVudERlZmF1bHQiLCJfc2V0WnhjdmJuUmVzdWx0VGltZW91dCIsImNsZWFyVGltZW91dCIsIlByb21pc2UiLCJyZXNvbHZlIiwienhjdmJuUmVzdWx0IiwicGFzc1BocmFzZSIsIl9wYXNzUGhyYXNlSXNWYWxpZCIsInBhc3NQaHJhc2VDb25maXJtIiwidmFsdWUiLCJzZXRUaW1lb3V0IiwiY29tcG9uZW50RGlkTW91bnQiLCJjbGkiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsImRvZXNTZXJ2ZXJTdXBwb3J0VW5zdGFibGVGZWF0dXJlIiwiX2NyZWF0ZUJhY2t1cCIsImNvbXBvbmVudFdpbGxVbm1vdW50Iiwic2NvcmUiLCJfcmVuZGVyUGhhc2VQYXNzUGhyYXNlIiwiRGlhbG9nQnV0dG9ucyIsInNkayIsImdldENvbXBvbmVudCIsInN0cmVuZ3RoTWV0ZXIiLCJoZWxwVGV4dCIsInN1Z2dlc3Rpb25zIiwiaSIsImZlZWRiYWNrIiwibGVuZ3RoIiwicHVzaCIsInN1Z2dlc3Rpb25CbG9jayIsIndhcm5pbmciLCJfb25QYXNzUGhyYXNlTmV4dENsaWNrIiwiYiIsInN1YiIsIl9vblBhc3NQaHJhc2VDaGFuZ2UiLCJfb25Ta2lwUGFzc1BocmFzZUNsaWNrIiwiX3JlbmRlclBoYXNlUGFzc1BocmFzZUNvbmZpcm0iLCJBY2Nlc3NpYmxlQnV0dG9uIiwibWF0Y2hUZXh0Iiwic3RhcnRzV2l0aCIsInBhc3NQaHJhc2VNYXRjaCIsIl9vblNldEFnYWluQ2xpY2siLCJfb25QYXNzUGhyYXNlQ29uZmlybU5leHRDbGljayIsIl9vblBhc3NQaHJhc2VDb25maXJtQ2hhbmdlIiwiX3JlbmRlclBoYXNlU2hvd0tleSIsIl9jb2xsZWN0UmVjb3ZlcnlLZXlOb2RlIiwiX29uQ29weUNsaWNrIiwiX29uRG93bmxvYWRDbGljayIsIl9yZW5kZXJQaGFzZUtlZXBJdFNhZmUiLCJpbnRyb1RleHQiLCJzIiwiX29uS2VlcEl0U2FmZUJhY2tDbGljayIsIl9yZW5kZXJCdXN5UGhhc2UiLCJ0ZXh0IiwiU3Bpbm5lciIsIl9yZW5kZXJQaGFzZURvbmUiLCJfb25Eb25lIiwiX3JlbmRlclBoYXNlT3B0T3V0Q29uZmlybSIsIl9vblNldFVwQ2xpY2siLCJfb25DYW5jZWwiLCJfdGl0bGVGb3JQaGFzZSIsInJlbmRlciIsIkJhc2VEaWFsb2ciLCJjb250ZW50IiwiaW5jbHVkZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0QkEsTUFBTUEsZ0JBQWdCLEdBQUcsQ0FBekI7QUFDQSxNQUFNQyx3QkFBd0IsR0FBRyxDQUFqQztBQUNBLE1BQU1DLGFBQWEsR0FBRyxDQUF0QjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLENBQXpCO0FBQ0EsTUFBTUMsZUFBZSxHQUFHLENBQXhCO0FBQ0EsTUFBTUMsVUFBVSxHQUFHLENBQW5CO0FBQ0EsTUFBTUMsb0JBQW9CLEdBQUcsQ0FBN0I7QUFFQSxNQUFNQyxrQkFBa0IsR0FBRyxDQUEzQixDLENBQThCOztBQUM5QixNQUFNQyx5QkFBeUIsR0FBRyxHQUFsQyxDLENBQXVDO0FBRXZDOztBQUNBLFNBQVNDLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQ3hCLFFBQU1DLEtBQUssR0FBR0MsUUFBUSxDQUFDQyxXQUFULEVBQWQ7QUFDQUYsRUFBQUEsS0FBSyxDQUFDRyxrQkFBTixDQUF5QkosTUFBekI7QUFFQSxRQUFNSyxTQUFTLEdBQUdDLE1BQU0sQ0FBQ0MsWUFBUCxFQUFsQjtBQUNBRixFQUFBQSxTQUFTLENBQUNHLGVBQVY7QUFDQUgsRUFBQUEsU0FBUyxDQUFDSSxRQUFWLENBQW1CUixLQUFuQjtBQUNIO0FBRUQ7Ozs7OztBQUllLE1BQU1TLHFCQUFOLFNBQW9DQyxlQUFNQyxhQUExQyxDQUF3RDtBQUtuRUMsRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVE7QUFDZixVQUFNQSxLQUFOO0FBRGUsbUVBd0NRQyxDQUFELElBQU87QUFDN0IsV0FBS0MsZ0JBQUwsR0FBd0JELENBQXhCO0FBQ0gsS0ExQ2tCO0FBQUEsd0RBNENKLE1BQU07QUFDakJoQixNQUFBQSxVQUFVLENBQUMsS0FBS2lCLGdCQUFOLENBQVY7QUFDQSxZQUFNQyxVQUFVLEdBQUdmLFFBQVEsQ0FBQ2dCLFdBQVQsQ0FBcUIsTUFBckIsQ0FBbkI7O0FBQ0EsVUFBSUQsVUFBSixFQUFnQjtBQUNaLGFBQUtFLFFBQUwsQ0FBYztBQUNWQyxVQUFBQSxNQUFNLEVBQUUsSUFERTtBQUVWQyxVQUFBQSxLQUFLLEVBQUU1QjtBQUZHLFNBQWQ7QUFJSDtBQUNKLEtBckRrQjtBQUFBLDREQXVEQSxNQUFNO0FBQ3JCLFlBQU02QixJQUFJLEdBQUcsSUFBSUMsSUFBSixDQUFTLENBQUMsS0FBS0MsY0FBTCxDQUFvQkMsWUFBckIsQ0FBVCxFQUE2QztBQUN0REMsUUFBQUEsSUFBSSxFQUFFO0FBRGdELE9BQTdDLENBQWI7O0FBR0FDLHlCQUFVQyxNQUFWLENBQWlCTixJQUFqQixFQUF1QixrQkFBdkI7O0FBRUEsV0FBS0gsUUFBTCxDQUFjO0FBQ1ZVLFFBQUFBLFVBQVUsRUFBRSxJQURGO0FBRVZSLFFBQUFBLEtBQUssRUFBRTVCO0FBRkcsT0FBZDtBQUlILEtBakVrQjtBQUFBLHlEQW1FSCxZQUFZO0FBQ3hCLFlBQU07QUFBRXFDLFFBQUFBO0FBQUYsVUFBMEIsS0FBS0MsS0FBckM7QUFDQSxXQUFLWixRQUFMLENBQWM7QUFDVkUsUUFBQUEsS0FBSyxFQUFFM0IsZUFERztBQUVWc0MsUUFBQUEsS0FBSyxFQUFFO0FBRkcsT0FBZDtBQUlBLFVBQUlDLElBQUo7O0FBQ0EsVUFBSTtBQUNBLFlBQUlILG1CQUFKLEVBQXlCO0FBQ3JCLGdCQUFNLDhDQUFvQixZQUFZO0FBQ2xDRyxZQUFBQSxJQUFJLEdBQUcsTUFBTUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsdUJBQXRCLENBQ1Q7QUFBSztBQURJLGNBRVQ7QUFBRU4sY0FBQUEsbUJBQW1CLEVBQUU7QUFBdkIsYUFGUyxDQUFiO0FBSUFHLFlBQUFBLElBQUksR0FBRyxNQUFNQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCRSxzQkFBdEIsQ0FBNkNKLElBQTdDLENBQWI7QUFDSCxXQU5LLENBQU47QUFPSCxTQVJELE1BUU87QUFDSEEsVUFBQUEsSUFBSSxHQUFHLE1BQU1DLGlDQUFnQkMsR0FBaEIsR0FBc0JFLHNCQUF0QixDQUNULEtBQUtiLGNBREksQ0FBYjtBQUdIOztBQUNELGNBQU1VLGlDQUFnQkMsR0FBaEIsR0FBc0JHLGlDQUF0QixFQUFOO0FBQ0EsYUFBS25CLFFBQUwsQ0FBYztBQUNWRSxVQUFBQSxLQUFLLEVBQUUxQjtBQURHLFNBQWQ7QUFHSCxPQWxCRCxDQWtCRSxPQUFPNEMsQ0FBUCxFQUFVO0FBQ1JDLFFBQUFBLE9BQU8sQ0FBQ1IsS0FBUixDQUFjLDJCQUFkLEVBQTJDTyxDQUEzQyxFQURRLENBRVI7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsWUFBSU4sSUFBSixFQUFVO0FBQ05DLDJDQUFnQkMsR0FBaEIsR0FBc0JNLHNCQUF0QixDQUE2Q1IsSUFBSSxDQUFDUyxPQUFsRDtBQUNIOztBQUNELGFBQUt2QixRQUFMLENBQWM7QUFDVmEsVUFBQUEsS0FBSyxFQUFFTztBQURHLFNBQWQ7QUFHSDtBQUNKLEtBekdrQjtBQUFBLHFEQTJHUCxNQUFNO0FBQ2QsV0FBS3pCLEtBQUwsQ0FBVzZCLFVBQVgsQ0FBc0IsS0FBdEI7QUFDSCxLQTdHa0I7QUFBQSxtREErR1QsTUFBTTtBQUNaLFdBQUs3QixLQUFMLENBQVc2QixVQUFYLENBQXNCLElBQXRCO0FBQ0gsS0FqSGtCO0FBQUEsMERBbUhGLE1BQU07QUFDbkIsV0FBS3hCLFFBQUwsQ0FBYztBQUFDRSxRQUFBQSxLQUFLLEVBQUV6QjtBQUFSLE9BQWQ7QUFDSCxLQXJIa0I7QUFBQSx5REF1SEgsTUFBTTtBQUNsQixXQUFLdUIsUUFBTCxDQUFjO0FBQUNFLFFBQUFBLEtBQUssRUFBRS9CO0FBQVIsT0FBZDtBQUNILEtBekhrQjtBQUFBLGtFQTJITSxZQUFZO0FBQ2pDLFdBQUtrQyxjQUFMLEdBQXNCLE1BQU1VLGlDQUFnQkMsR0FBaEIsR0FBc0JDLHVCQUF0QixFQUE1QjtBQUNBLFdBQUtqQixRQUFMLENBQWM7QUFDVkMsUUFBQUEsTUFBTSxFQUFFLEtBREU7QUFFVlMsUUFBQUEsVUFBVSxFQUFFLEtBRkY7QUFHVlIsUUFBQUEsS0FBSyxFQUFFN0I7QUFIRyxPQUFkO0FBS0gsS0FsSWtCO0FBQUEsa0VBb0lNLE1BQU8rQyxDQUFQLElBQWE7QUFDbENBLE1BQUFBLENBQUMsQ0FBQ0ssY0FBRixHQURrQyxDQUdsQztBQUNBO0FBQ0E7O0FBQ0EsVUFBSSxLQUFLQyx1QkFBTCxLQUFpQyxJQUFyQyxFQUEyQztBQUN2Q0MsUUFBQUEsWUFBWSxDQUFDLEtBQUtELHVCQUFOLENBQVo7QUFDQSxhQUFLQSx1QkFBTCxHQUErQixJQUEvQjtBQUNBLGNBQU0sSUFBSUUsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDM0IsZUFBSzdCLFFBQUwsQ0FBYztBQUNWOEIsWUFBQUEsWUFBWSxFQUFFLG1DQUFjLEtBQUtsQixLQUFMLENBQVdtQixVQUF6QjtBQURKLFdBQWQsRUFFR0YsT0FGSDtBQUdILFNBSkssQ0FBTjtBQUtIOztBQUNELFVBQUksS0FBS0csa0JBQUwsRUFBSixFQUErQjtBQUMzQixhQUFLaEMsUUFBTCxDQUFjO0FBQUNFLFVBQUFBLEtBQUssRUFBRTlCO0FBQVIsU0FBZDtBQUNIO0FBQ0osS0F0SmtCO0FBQUEseUVBd0phLE1BQU9nRCxDQUFQLElBQWE7QUFDekNBLE1BQUFBLENBQUMsQ0FBQ0ssY0FBRjtBQUVBLFVBQUksS0FBS2IsS0FBTCxDQUFXbUIsVUFBWCxLQUEwQixLQUFLbkIsS0FBTCxDQUFXcUIsaUJBQXpDLEVBQTREO0FBRTVELFdBQUs1QixjQUFMLEdBQXNCLE1BQU1VLGlDQUFnQkMsR0FBaEIsR0FBc0JDLHVCQUF0QixDQUE4QyxLQUFLTCxLQUFMLENBQVdtQixVQUF6RCxDQUE1QjtBQUNBLFdBQUsvQixRQUFMLENBQWM7QUFDVkMsUUFBQUEsTUFBTSxFQUFFLEtBREU7QUFFVlMsUUFBQUEsVUFBVSxFQUFFLEtBRkY7QUFHVlIsUUFBQUEsS0FBSyxFQUFFN0I7QUFIRyxPQUFkO0FBS0gsS0FuS2tCO0FBQUEsNERBcUtBLE1BQU07QUFDckIsV0FBSzJCLFFBQUwsQ0FBYztBQUNWK0IsUUFBQUEsVUFBVSxFQUFFLEVBREY7QUFFVkUsUUFBQUEsaUJBQWlCLEVBQUUsRUFGVDtBQUdWL0IsUUFBQUEsS0FBSyxFQUFFL0IsZ0JBSEc7QUFJVjJELFFBQUFBLFlBQVksRUFBRTtBQUpKLE9BQWQ7QUFNSCxLQTVLa0I7QUFBQSxrRUE4S00sTUFBTTtBQUMzQixXQUFLOUIsUUFBTCxDQUFjO0FBQ1ZFLFFBQUFBLEtBQUssRUFBRTdCO0FBREcsT0FBZDtBQUdILEtBbExrQjtBQUFBLCtEQW9MSStDLENBQUQsSUFBTztBQUN6QixXQUFLcEIsUUFBTCxDQUFjO0FBQ1YrQixRQUFBQSxVQUFVLEVBQUVYLENBQUMsQ0FBQ3ZDLE1BQUYsQ0FBU3FEO0FBRFgsT0FBZDs7QUFJQSxVQUFJLEtBQUtSLHVCQUFMLEtBQWlDLElBQXJDLEVBQTJDO0FBQ3ZDQyxRQUFBQSxZQUFZLENBQUMsS0FBS0QsdUJBQU4sQ0FBWjtBQUNIOztBQUNELFdBQUtBLHVCQUFMLEdBQStCUyxVQUFVLENBQUMsTUFBTTtBQUM1QyxhQUFLVCx1QkFBTCxHQUErQixJQUEvQjtBQUNBLGFBQUsxQixRQUFMLENBQWM7QUFDVjtBQUNBO0FBQ0E7QUFDQThCLFVBQUFBLFlBQVksRUFBRSxtQ0FBYyxLQUFLbEIsS0FBTCxDQUFXbUIsVUFBekI7QUFKSixTQUFkO0FBTUgsT0FSd0MsRUFRdENwRCx5QkFSc0MsQ0FBekM7QUFTSCxLQXJNa0I7QUFBQSxzRUF1TVd5QyxDQUFELElBQU87QUFDaEMsV0FBS3BCLFFBQUwsQ0FBYztBQUNWaUMsUUFBQUEsaUJBQWlCLEVBQUViLENBQUMsQ0FBQ3ZDLE1BQUYsQ0FBU3FEO0FBRGxCLE9BQWQ7QUFHSCxLQTNNa0I7QUFHZixTQUFLckMsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxTQUFLUSxjQUFMLEdBQXNCLElBQXRCO0FBQ0EsU0FBS3FCLHVCQUFMLEdBQStCLElBQS9CO0FBRUEsU0FBS2QsS0FBTCxHQUFhO0FBQ1RELE1BQUFBLG1CQUFtQixFQUFFLElBRFo7QUFFVFQsTUFBQUEsS0FBSyxFQUFFL0IsZ0JBRkU7QUFHVDRELE1BQUFBLFVBQVUsRUFBRSxFQUhIO0FBSVRFLE1BQUFBLGlCQUFpQixFQUFFLEVBSlY7QUFLVGhDLE1BQUFBLE1BQU0sRUFBRSxLQUxDO0FBTVRTLE1BQUFBLFVBQVUsRUFBRSxLQU5IO0FBT1RvQixNQUFBQSxZQUFZLEVBQUU7QUFQTCxLQUFiO0FBU0g7O0FBRUQsUUFBTU0saUJBQU4sR0FBMEI7QUFDdEIsVUFBTUMsR0FBRyxHQUFHdEIsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFVBQU1MLG1CQUFtQixHQUNyQjJCLHVCQUFjQyxnQkFBZCxDQUErQix1QkFBL0IsTUFDQSxNQUFNRixHQUFHLENBQUNHLGdDQUFKLENBQXFDLDhCQUFyQyxDQUROLENBREo7QUFJQSxTQUFLeEMsUUFBTCxDQUFjO0FBQUVXLE1BQUFBO0FBQUYsS0FBZCxFQU5zQixDQVF0QjtBQUNBOztBQUNBLFFBQUlBLG1CQUFKLEVBQXlCO0FBQ3JCLFdBQUtYLFFBQUwsQ0FBYztBQUFFRSxRQUFBQSxLQUFLLEVBQUUzQjtBQUFULE9BQWQ7O0FBQ0EsV0FBS2tFLGFBQUw7QUFDSDtBQUNKOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQixRQUFJLEtBQUtoQix1QkFBTCxLQUFpQyxJQUFyQyxFQUEyQztBQUN2Q0MsTUFBQUEsWUFBWSxDQUFDLEtBQUtELHVCQUFOLENBQVo7QUFDSDtBQUNKOztBQXVLRE0sRUFBQUEsa0JBQWtCLEdBQUc7QUFDakIsV0FBTyxLQUFLcEIsS0FBTCxDQUFXa0IsWUFBWCxJQUEyQixLQUFLbEIsS0FBTCxDQUFXa0IsWUFBWCxDQUF3QmEsS0FBeEIsSUFBaUNqRSxrQkFBbkU7QUFDSDs7QUFFRGtFLEVBQUFBLHNCQUFzQixHQUFHO0FBQ3JCLFVBQU1DLGFBQWEsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUVBLFFBQUlDLGFBQUo7QUFDQSxRQUFJQyxRQUFKOztBQUNBLFFBQUksS0FBS3JDLEtBQUwsQ0FBV2tCLFlBQWYsRUFBNkI7QUFDekIsVUFBSSxLQUFLbEIsS0FBTCxDQUFXa0IsWUFBWCxDQUF3QmEsS0FBeEIsSUFBaUNqRSxrQkFBckMsRUFBeUQ7QUFDckR1RSxRQUFBQSxRQUFRLEdBQUcseUJBQUcsNkNBQUgsQ0FBWDtBQUNILE9BRkQsTUFFTztBQUNILGNBQU1DLFdBQVcsR0FBRyxFQUFwQjs7QUFDQSxhQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS3ZDLEtBQUwsQ0FBV2tCLFlBQVgsQ0FBd0JzQixRQUF4QixDQUFpQ0YsV0FBakMsQ0FBNkNHLE1BQWpFLEVBQXlFLEVBQUVGLENBQTNFLEVBQThFO0FBQzFFRCxVQUFBQSxXQUFXLENBQUNJLElBQVosQ0FBaUI7QUFBSyxZQUFBLEdBQUcsRUFBRUg7QUFBVixhQUFjLEtBQUt2QyxLQUFMLENBQVdrQixZQUFYLENBQXdCc0IsUUFBeEIsQ0FBaUNGLFdBQWpDLENBQTZDQyxDQUE3QyxDQUFkLENBQWpCO0FBQ0g7O0FBQ0QsY0FBTUksZUFBZSxHQUFHLDBDQUFNTCxXQUFXLENBQUNHLE1BQVosR0FBcUIsQ0FBckIsR0FBeUJILFdBQXpCLEdBQXVDLHlCQUFHLGVBQUgsQ0FBN0MsQ0FBeEI7O0FBRUFELFFBQUFBLFFBQVEsR0FBRywwQ0FDTixLQUFLckMsS0FBTCxDQUFXa0IsWUFBWCxDQUF3QnNCLFFBQXhCLENBQWlDSSxPQUQzQixFQUVORCxlQUZNLENBQVg7QUFJSDs7QUFDRFAsTUFBQUEsYUFBYSxHQUFHLDBDQUNaO0FBQVUsUUFBQSxHQUFHLEVBQUV0RSxrQkFBZjtBQUFtQyxRQUFBLEtBQUssRUFBRSxLQUFLa0MsS0FBTCxDQUFXa0IsWUFBWCxDQUF3QmE7QUFBbEUsUUFEWSxDQUFoQjtBQUdIOztBQUVELFdBQU87QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLYztBQUFyQixPQUNILHdDQUFJLHlCQUNBLDRFQURBLEVBQzhFLEVBRDlFLEVBRUE7QUFBRUMsTUFBQUEsQ0FBQyxFQUFFQyxHQUFHLElBQUksd0NBQUlBLEdBQUo7QUFBWixLQUZBLENBQUosQ0FERyxFQUtILHdDQUFJLHlCQUNBLCtEQUNBLDBEQUZBLENBQUosQ0FMRyxFQVNILHdDQUFJLHlCQUFHLDRFQUFILENBQUosQ0FURyxFQVdIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsVUFBWjtBQUNJLE1BQUEsUUFBUSxFQUFFLEtBQUtDLG1CQURuQjtBQUVJLE1BQUEsS0FBSyxFQUFFLEtBQUtoRCxLQUFMLENBQVdtQixVQUZ0QjtBQUdJLE1BQUEsU0FBUyxFQUFDLDBDQUhkO0FBSUksTUFBQSxXQUFXLEVBQUUseUJBQUcsdUJBQUgsQ0FKakI7QUFLSSxNQUFBLFNBQVMsRUFBRTtBQUxmLE1BREosRUFRSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDS2lCLGFBREwsRUFFS0MsUUFGTCxDQVJKLENBREosQ0FYRyxFQTJCSCw2QkFBQyxhQUFEO0FBQ0ksTUFBQSxhQUFhLEVBQUUseUJBQUcsTUFBSCxDQURuQjtBQUVJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS1Esc0JBRi9CO0FBR0ksTUFBQSxTQUFTLEVBQUUsS0FIZjtBQUlJLE1BQUEsUUFBUSxFQUFFLENBQUMsS0FBS3pCLGtCQUFMO0FBSmYsTUEzQkcsRUFrQ0gsOENBQ0ksOENBQVUseUJBQUcsVUFBSCxDQUFWLENBREosRUFFSSw2QkFBQyx5QkFBRDtBQUFrQixNQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxNQUFBLE9BQU8sRUFBRSxLQUFLNkI7QUFBL0MsT0FDSyx5QkFBRyw0QkFBSCxDQURMLENBRkosQ0FsQ0csQ0FBUDtBQXlDSDs7QUFFREMsRUFBQUEsNkJBQTZCLEdBQUc7QUFDNUIsVUFBTUMsZ0JBQWdCLEdBQUdqQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBRUEsUUFBSWlCLFNBQUo7O0FBQ0EsUUFBSSxLQUFLcEQsS0FBTCxDQUFXcUIsaUJBQVgsS0FBaUMsS0FBS3JCLEtBQUwsQ0FBV21CLFVBQWhELEVBQTREO0FBQ3hEaUMsTUFBQUEsU0FBUyxHQUFHLHlCQUFHLGVBQUgsQ0FBWjtBQUNILEtBRkQsTUFFTyxJQUFJLENBQUMsS0FBS3BELEtBQUwsQ0FBV21CLFVBQVgsQ0FBc0JrQyxVQUF0QixDQUFpQyxLQUFLckQsS0FBTCxDQUFXcUIsaUJBQTVDLENBQUwsRUFBcUU7QUFDeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQStCLE1BQUFBLFNBQVMsR0FBRyx5QkFBRyxxQkFBSCxDQUFaO0FBQ0g7O0FBRUQsUUFBSUUsZUFBZSxHQUFHLElBQXRCOztBQUNBLFFBQUlGLFNBQUosRUFBZTtBQUNYRSxNQUFBQSxlQUFlLEdBQUc7QUFBSyxRQUFBLFNBQVMsRUFBQztBQUFmLFNBQ2QsMENBQU1GLFNBQU4sQ0FEYyxFQUVkLDBDQUNJLDZCQUFDLGdCQUFEO0FBQWtCLFFBQUEsT0FBTyxFQUFDLE1BQTFCO0FBQWlDLFFBQUEsU0FBUyxFQUFDLGVBQTNDO0FBQTJELFFBQUEsT0FBTyxFQUFFLEtBQUtHO0FBQXpFLFNBQ0sseUJBQUcsMEJBQUgsQ0FETCxDQURKLENBRmMsQ0FBbEI7QUFRSDs7QUFDRCxVQUFNdEIsYUFBYSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FBTztBQUFNLE1BQUEsUUFBUSxFQUFFLEtBQUtxQjtBQUFyQixPQUNILHdDQUFJLHlCQUNBLHdEQURBLENBQUosQ0FERyxFQUlIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDBDQUNJO0FBQU8sTUFBQSxJQUFJLEVBQUMsVUFBWjtBQUNJLE1BQUEsUUFBUSxFQUFFLEtBQUtDLDBCQURuQjtBQUVJLE1BQUEsS0FBSyxFQUFFLEtBQUt6RCxLQUFMLENBQVdxQixpQkFGdEI7QUFHSSxNQUFBLFNBQVMsRUFBQywwQ0FIZDtBQUlJLE1BQUEsV0FBVyxFQUFFLHlCQUFHLDJCQUFILENBSmpCO0FBS0ksTUFBQSxTQUFTLEVBQUU7QUFMZixNQURKLENBREosRUFVS2lDLGVBVkwsQ0FESixDQUpHLEVBa0JILDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxNQUFILENBRG5CO0FBRUksTUFBQSxvQkFBb0IsRUFBRSxLQUFLRSw2QkFGL0I7QUFHSSxNQUFBLFNBQVMsRUFBRSxLQUhmO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS3hELEtBQUwsQ0FBV21CLFVBQVgsS0FBMEIsS0FBS25CLEtBQUwsQ0FBV3FCO0FBSm5ELE1BbEJHLENBQVA7QUF5Qkg7O0FBRURxQyxFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixXQUFPLDBDQUNILHdDQUFJLHlCQUNBLG1FQUNBLGtFQUZBLENBQUosQ0FERyxFQUtILHdDQUFJLHlCQUNBLDZFQURBLENBQUosQ0FMRyxFQVFIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNLLHlCQUFHLG1CQUFILENBREwsQ0FESixFQUlJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQU0sTUFBQSxHQUFHLEVBQUUsS0FBS0M7QUFBaEIsT0FBMEMsS0FBS2xFLGNBQUwsQ0FBb0JDLFlBQTlELENBREosQ0FESixFQUlJO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJO0FBQVEsTUFBQSxTQUFTLEVBQUMsbUJBQWxCO0FBQXNDLE1BQUEsT0FBTyxFQUFFLEtBQUtrRTtBQUFwRCxPQUNLLHlCQUFHLE1BQUgsQ0FETCxDQURKLEVBSUk7QUFBUSxNQUFBLFNBQVMsRUFBQyxtQkFBbEI7QUFBc0MsTUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBcEQsT0FDSyx5QkFBRyxVQUFILENBREwsQ0FKSixDQUpKLENBSkosQ0FSRyxDQUFQO0FBMkJIOztBQUVEQyxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQixRQUFJQyxTQUFKOztBQUNBLFFBQUksS0FBSy9ELEtBQUwsQ0FBV1gsTUFBZixFQUF1QjtBQUNuQjBFLE1BQUFBLFNBQVMsR0FBRyx5QkFDUiwwRUFEUSxFQUVSLEVBRlEsRUFFSjtBQUFDakIsUUFBQUEsQ0FBQyxFQUFFa0IsQ0FBQyxJQUFJLHdDQUFJQSxDQUFKO0FBQVQsT0FGSSxDQUFaO0FBSUgsS0FMRCxNQUtPLElBQUksS0FBS2hFLEtBQUwsQ0FBV0YsVUFBZixFQUEyQjtBQUM5QmlFLE1BQUFBLFNBQVMsR0FBRyx5QkFDUix1REFEUSxFQUVSLEVBRlEsRUFFSjtBQUFDakIsUUFBQUEsQ0FBQyxFQUFFa0IsQ0FBQyxJQUFJLHdDQUFJQSxDQUFKO0FBQVQsT0FGSSxDQUFaO0FBSUg7O0FBQ0QsVUFBTS9CLGFBQWEsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFdBQU8sMENBQ0Y0QixTQURFLEVBRUgseUNBQ0kseUNBQUsseUJBQUcsNkNBQUgsRUFBa0QsRUFBbEQsRUFBc0Q7QUFBQ2pCLE1BQUFBLENBQUMsRUFBRWtCLENBQUMsSUFBSSx3Q0FBSUEsQ0FBSjtBQUFULEtBQXRELENBQUwsQ0FESixFQUVJLHlDQUFLLHlCQUFHLDZDQUFILEVBQWtELEVBQWxELEVBQXNEO0FBQUNsQixNQUFBQSxDQUFDLEVBQUVrQixDQUFDLElBQUksd0NBQUlBLENBQUo7QUFBVCxLQUF0RCxDQUFMLENBRkosRUFHSSx5Q0FBSyx5QkFBRywrQ0FBSCxFQUFvRCxFQUFwRCxFQUF3RDtBQUFDbEIsTUFBQUEsQ0FBQyxFQUFFa0IsQ0FBQyxJQUFJLHdDQUFJQSxDQUFKO0FBQVQsS0FBeEQsQ0FBTCxDQUhKLENBRkcsRUFPSCw2QkFBQyxhQUFEO0FBQWUsTUFBQSxhQUFhLEVBQUUseUJBQUcsVUFBSCxDQUE5QjtBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS25DLGFBRC9CO0FBRUksTUFBQSxTQUFTLEVBQUU7QUFGZixPQUdJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS29DO0FBQXRCLE9BQStDLHlCQUFHLE1BQUgsQ0FBL0MsQ0FISixDQVBHLENBQVA7QUFhSDs7QUFFREMsRUFBQUEsZ0JBQWdCLENBQUNDLElBQUQsRUFBTztBQUNuQixVQUFNQyxPQUFPLEdBQUdsQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQWhCO0FBQ0EsV0FBTywwQ0FDSCw2QkFBQyxPQUFELE9BREcsQ0FBUDtBQUdIOztBQUVEa0MsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixVQUFNcEMsYUFBYSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FBTywwQ0FDSCx3Q0FBSSx5QkFDQSw0RUFEQSxDQUFKLENBREcsRUFJSCw2QkFBQyxhQUFEO0FBQWUsTUFBQSxhQUFhLEVBQUUseUJBQUcsSUFBSCxDQUE5QjtBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS21DLE9BRC9CO0FBRUksTUFBQSxTQUFTLEVBQUU7QUFGZixNQUpHLENBQVA7QUFTSDs7QUFFREMsRUFBQUEseUJBQXlCLEdBQUc7QUFDeEIsVUFBTXRDLGFBQWEsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFdBQU8sMENBQ0YseUJBQ0csbUZBQ0Esa0VBRkgsQ0FERSxFQUtILDZCQUFDLGFBQUQ7QUFBZSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxnQ0FBSCxDQUE5QjtBQUNJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS3FDLGFBRC9CO0FBRUksTUFBQSxTQUFTLEVBQUU7QUFGZixPQUlJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBdEIsd0NBSkosQ0FMRyxDQUFQO0FBWUg7O0FBRURDLEVBQUFBLGNBQWMsQ0FBQ3BGLEtBQUQsRUFBUTtBQUNsQixZQUFRQSxLQUFSO0FBQ0ksV0FBSy9CLGdCQUFMO0FBQ0ksZUFBTyx5QkFBRyxzQ0FBSCxDQUFQOztBQUNKLFdBQUtDLHdCQUFMO0FBQ0ksZUFBTyx5QkFBRyx5QkFBSCxDQUFQOztBQUNKLFdBQUtLLG9CQUFMO0FBQ0ksZUFBTyx5QkFBRyxVQUFILENBQVA7O0FBQ0osV0FBS0osYUFBTDtBQUNBLFdBQUtDLGdCQUFMO0FBQ0ksZUFBTyx5QkFBRyxrQ0FBSCxDQUFQOztBQUNKLFdBQUtDLGVBQUw7QUFDSSxlQUFPLHlCQUFHLG9CQUFILENBQVA7O0FBQ0osV0FBS0MsVUFBTDtBQUNJLGVBQU8seUJBQUcsVUFBSCxDQUFQOztBQUNKO0FBQ0ksZUFBTyx5QkFBRyxtQkFBSCxDQUFQO0FBZlI7QUFpQkg7O0FBRUQrRyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxVQUFVLEdBQUcxQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMEJBQWpCLENBQW5CO0FBRUEsUUFBSTBDLE9BQUo7O0FBQ0EsUUFBSSxLQUFLN0UsS0FBTCxDQUFXQyxLQUFmLEVBQXNCO0FBQ2xCLFlBQU1nQyxhQUFhLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFDQTBDLE1BQUFBLE9BQU8sR0FBRywwQ0FDTix3Q0FBSSx5QkFBRyw2QkFBSCxDQUFKLENBRE0sRUFFTjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyxhQUFEO0FBQWUsUUFBQSxhQUFhLEVBQUUseUJBQUcsT0FBSCxDQUE5QjtBQUNJLFFBQUEsb0JBQW9CLEVBQUUsS0FBS2hELGFBRC9CO0FBRUksUUFBQSxTQUFTLEVBQUUsSUFGZjtBQUdJLFFBQUEsUUFBUSxFQUFFLEtBQUs0QztBQUhuQixRQURKLENBRk0sQ0FBVjtBQVVILEtBWkQsTUFZTztBQUNILGNBQVEsS0FBS3pFLEtBQUwsQ0FBV1YsS0FBbkI7QUFDSSxhQUFLL0IsZ0JBQUw7QUFDSXNILFVBQUFBLE9BQU8sR0FBRyxLQUFLN0Msc0JBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUt4RSx3QkFBTDtBQUNJcUgsVUFBQUEsT0FBTyxHQUFHLEtBQUszQiw2QkFBTCxFQUFWO0FBQ0E7O0FBQ0osYUFBS3pGLGFBQUw7QUFDSW9ILFVBQUFBLE9BQU8sR0FBRyxLQUFLbkIsbUJBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUtoRyxnQkFBTDtBQUNJbUgsVUFBQUEsT0FBTyxHQUFHLEtBQUtmLHNCQUFMLEVBQVY7QUFDQTs7QUFDSixhQUFLbkcsZUFBTDtBQUNJa0gsVUFBQUEsT0FBTyxHQUFHLEtBQUtYLGdCQUFMLEVBQVY7QUFDQTs7QUFDSixhQUFLdEcsVUFBTDtBQUNJaUgsVUFBQUEsT0FBTyxHQUFHLEtBQUtSLGdCQUFMLEVBQVY7QUFDQTs7QUFDSixhQUFLeEcsb0JBQUw7QUFDSWdILFVBQUFBLE9BQU8sR0FBRyxLQUFLTix5QkFBTCxFQUFWO0FBQ0E7QUFyQlI7QUF1Qkg7O0FBRUQsV0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxTQUFTLEVBQUMsMEJBQXRCO0FBQ0ksTUFBQSxVQUFVLEVBQUUsS0FBS3hGLEtBQUwsQ0FBVzZCLFVBRDNCO0FBRUksTUFBQSxLQUFLLEVBQUUsS0FBSzhELGNBQUwsQ0FBb0IsS0FBSzFFLEtBQUwsQ0FBV1YsS0FBL0IsQ0FGWDtBQUdJLE1BQUEsU0FBUyxFQUFFLENBQUMvQixnQkFBRCxFQUFtQkssVUFBbkIsRUFBK0JrSCxRQUEvQixDQUF3QyxLQUFLOUUsS0FBTCxDQUFXVixLQUFuRDtBQUhmLE9BS0EsMENBQ0t1RixPQURMLENBTEEsQ0FESjtBQVdIOztBQTFma0U7Ozs4QkFBbERsRyxxQixlQUNFO0FBQ2ZpQyxFQUFBQSxVQUFVLEVBQUVtRSxtQkFBVUMsSUFBVixDQUFlQztBQURaLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCwgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBGaWxlU2F2ZXIgZnJvbSAnZmlsZS1zYXZlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuLi8uLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyBzY29yZVBhc3N3b3JkIH0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvUGFzc3dvcmRTY29yZXInO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7IGFjY2Vzc1NlY3JldFN0b3JhZ2UgfSBmcm9tICcuLi8uLi8uLi8uLi9Dcm9zc1NpZ25pbmdNYW5hZ2VyJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSAnLi4vLi4vLi4vLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZSc7XHJcbmltcG9ydCBBY2Nlc3NpYmxlQnV0dG9uIGZyb20gXCIuLi8uLi8uLi8uLi9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL0FjY2Vzc2libGVCdXR0b25cIjtcclxuXHJcbmNvbnN0IFBIQVNFX1BBU1NQSFJBU0UgPSAwO1xyXG5jb25zdCBQSEFTRV9QQVNTUEhSQVNFX0NPTkZJUk0gPSAxO1xyXG5jb25zdCBQSEFTRV9TSE9XS0VZID0gMjtcclxuY29uc3QgUEhBU0VfS0VFUElUU0FGRSA9IDM7XHJcbmNvbnN0IFBIQVNFX0JBQ0tJTkdVUCA9IDQ7XHJcbmNvbnN0IFBIQVNFX0RPTkUgPSA1O1xyXG5jb25zdCBQSEFTRV9PUFRPVVRfQ09ORklSTSA9IDY7XHJcblxyXG5jb25zdCBQQVNTV09SRF9NSU5fU0NPUkUgPSA0OyAvLyBTbyBzZWN1cmUsIG1hbnkgY2hhcmFjdGVycywgbXVjaCBjb21wbGV4LCB3b3csIGV0YywgZXRjLlxyXG5jb25zdCBQQVNTUEhSQVNFX0ZFRURCQUNLX0RFTEFZID0gNTAwOyAvLyBIb3cgbG9uZyBhZnRlciBrZXlzdHJva2UgdG8gb2ZmZXIgcGFzc3BocmFzZSBmZWVkYmFjaywgbXMuXHJcblxyXG4vLyBYWFg6IGNvcGllZCBmcm9tIFNoYXJlRGlhbG9nOiBmYWN0b3Igb3V0IGludG8gdXRpbHNcclxuZnVuY3Rpb24gc2VsZWN0VGV4dCh0YXJnZXQpIHtcclxuICAgIGNvbnN0IHJhbmdlID0gZG9jdW1lbnQuY3JlYXRlUmFuZ2UoKTtcclxuICAgIHJhbmdlLnNlbGVjdE5vZGVDb250ZW50cyh0YXJnZXQpO1xyXG5cclxuICAgIGNvbnN0IHNlbGVjdGlvbiA9IHdpbmRvdy5nZXRTZWxlY3Rpb24oKTtcclxuICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcclxuICAgIHNlbGVjdGlvbi5hZGRSYW5nZShyYW5nZSk7XHJcbn1cclxuXHJcbi8qXHJcbiAqIFdhbGtzIHRoZSB1c2VyIHRocm91Z2ggdGhlIHByb2Nlc3Mgb2YgY3JlYXRpbmcgYW4gZTJlIGtleSBiYWNrdXBcclxuICogb24gdGhlIHNlcnZlci5cclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENyZWF0ZUtleUJhY2t1cERpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICBvbkZpbmlzaGVkOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLl9yZWNvdmVyeUtleU5vZGUgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2tleUJhY2t1cEluZm8gPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgPSBudWxsO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXRlID0ge1xyXG4gICAgICAgICAgICBzZWN1cmVTZWNyZXRTdG9yYWdlOiBudWxsLFxyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfUEFTU1BIUkFTRSxcclxuICAgICAgICAgICAgcGFzc1BocmFzZTogJycsXHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2VDb25maXJtOiAnJyxcclxuICAgICAgICAgICAgY29waWVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZG93bmxvYWRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHp4Y3ZiblJlc3VsdDogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCBzZWN1cmVTZWNyZXRTdG9yYWdlID0gKFxyXG4gICAgICAgICAgICBTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZUVuYWJsZWQoXCJmZWF0dXJlX2Nyb3NzX3NpZ25pbmdcIikgJiZcclxuICAgICAgICAgICAgYXdhaXQgY2xpLmRvZXNTZXJ2ZXJTdXBwb3J0VW5zdGFibGVGZWF0dXJlKFwib3JnLm1hdHJpeC5lMmVfY3Jvc3Nfc2lnbmluZ1wiKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHNlY3VyZVNlY3JldFN0b3JhZ2UgfSk7XHJcblxyXG4gICAgICAgIC8vIElmIHdlJ3JlIHVzaW5nIHNlY3JldCBzdG9yYWdlLCBza2lwIGFoZWFkIHRvIHRoZSBiYWNraW5nIHVwIHN0ZXAsIGFzXHJcbiAgICAgICAgLy8gYGFjY2Vzc1NlY3JldFN0b3JhZ2VgIHdpbGwgaGFuZGxlIHBhc3NwaHJhc2VzIGFzIG5lZWRlZC5cclxuICAgICAgICBpZiAoc2VjdXJlU2VjcmV0U3RvcmFnZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcGhhc2U6IFBIQVNFX0JBQ0tJTkdVUCB9KTtcclxuICAgICAgICAgICAgdGhpcy5fY3JlYXRlQmFja3VwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zZXRaeGN2Ym5SZXN1bHRUaW1lb3V0ICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLl9zZXRaeGN2Ym5SZXN1bHRUaW1lb3V0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX2NvbGxlY3RSZWNvdmVyeUtleU5vZGUgPSAobikgPT4ge1xyXG4gICAgICAgIHRoaXMuX3JlY292ZXJ5S2V5Tm9kZSA9IG47XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ29weUNsaWNrID0gKCkgPT4ge1xyXG4gICAgICAgIHNlbGVjdFRleHQodGhpcy5fcmVjb3ZlcnlLZXlOb2RlKTtcclxuICAgICAgICBjb25zdCBzdWNjZXNzZnVsID0gZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ2NvcHknKTtcclxuICAgICAgICBpZiAoc3VjY2Vzc2Z1bCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGNvcGllZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9LRUVQSVRTQUZFLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uRG93bmxvYWRDbGljayA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoW3RoaXMuX2tleUJhY2t1cEluZm8ucmVjb3Zlcnlfa2V5XSwge1xyXG4gICAgICAgICAgICB0eXBlOiAndGV4dC9wbGFpbjtjaGFyc2V0PXVzLWFzY2lpJyxcclxuICAgICAgICB9KTtcclxuICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsICdyZWNvdmVyeS1rZXkudHh0Jyk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBkb3dubG9hZGVkOiB0cnVlLFxyXG4gICAgICAgICAgICBwaGFzZTogUEhBU0VfS0VFUElUU0FGRSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfY3JlYXRlQmFja3VwID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHsgc2VjdXJlU2VjcmV0U3RvcmFnZSB9ID0gdGhpcy5zdGF0ZTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0JBQ0tJTkdVUCxcclxuICAgICAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IGluZm87XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHNlY3VyZVNlY3JldFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgICAgIGF3YWl0IGFjY2Vzc1NlY3JldFN0b3JhZ2UoYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucHJlcGFyZUtleUJhY2t1cFZlcnNpb24oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgLyogcmFuZG9tIGtleSAqLyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBzZWN1cmVTZWNyZXRTdG9yYWdlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICBpbmZvID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWF0ZUtleUJhY2t1cFZlcnNpb24oaW5mbyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuY3JlYXRlS2V5QmFja3VwVmVyc2lvbihcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9rZXlCYWNrdXBJbmZvLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2NoZWR1bGVBbGxHcm91cFNlc3Npb25zRm9yQmFja3VwKCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0RPTkUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIGNyZWF0aW5nIGtleSBiYWNrdXBcIiwgZSk7XHJcbiAgICAgICAgICAgIC8vIFRPRE86IElmIGNyZWF0aW5nIGEgdmVyc2lvbiBzdWNjZWVkcywgYnV0IGJhY2t1cCBmYWlscywgc2hvdWxkIHdlXHJcbiAgICAgICAgICAgIC8vIGRlbGV0ZSB0aGUgdmVyc2lvbiwgZGlzYWJsZSBiYWNrdXAsIG9yIGRvIG5vdGhpbmc/ICBJZiB3ZSBqdXN0XHJcbiAgICAgICAgICAgIC8vIGRpc2FibGUgd2l0aG91dCBkZWxldGluZywgd2UnbGwgZW5hYmxlIG9uIG5leHQgYXBwIHJlbG9hZCBzaW5jZVxyXG4gICAgICAgICAgICAvLyBpdCBpcyB0cnVzdGVkLlxyXG4gICAgICAgICAgICBpZiAoaW5mbykge1xyXG4gICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRlbGV0ZUtleUJhY2t1cFZlcnNpb24oaW5mby52ZXJzaW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGVycm9yOiBlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uQ2FuY2VsID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZChmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uRG9uZSA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uT3B0T3V0Q2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cGhhc2U6IFBIQVNFX09QVE9VVF9DT05GSVJNfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2V0VXBDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfUEFTU1BIUkFTRX0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblNraXBQYXNzUGhyYXNlQ2xpY2sgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fa2V5QmFja3VwSW5mbyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5wcmVwYXJlS2V5QmFja3VwVmVyc2lvbigpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjb3BpZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkb3dubG9hZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dLRVksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUGFzc1BocmFzZU5leHRDbGljayA9IGFzeW5jIChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAvLyBJZiB3ZSdyZSB3YWl0aW5nIGZvciB0aGUgdGltZW91dCBiZWZvcmUgdXBkYXRpbmcgdGhlIHJlc3VsdCBhdCB0aGlzIHBvaW50LFxyXG4gICAgICAgIC8vIHNraXAgYWhlYWQgYW5kIGRvIGl0IG5vdywgb3RoZXJ3aXNlIHdlJ2xsIGRlbnkgdGhlIGF0dGVtcHQgdG8gcHJvY2VlZFxyXG4gICAgICAgIC8vIGV2ZW4gaWYgdGhlIHVzZXIgZW50ZXJlZCBhIHZhbGlkIHBhc3NwaHJhc2VcclxuICAgICAgICBpZiAodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgPSBudWxsO1xyXG4gICAgICAgICAgICBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgenhjdmJuUmVzdWx0OiBzY29yZVBhc3N3b3JkKHRoaXMuc3RhdGUucGFzc1BocmFzZSksXHJcbiAgICAgICAgICAgICAgICB9LCByZXNvbHZlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9wYXNzUGhyYXNlSXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3BoYXNlOiBQSEFTRV9QQVNTUEhSQVNFX0NPTkZJUk19KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblBhc3NQaHJhc2VDb25maXJtTmV4dENsaWNrID0gYXN5bmMgKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBhc3NQaHJhc2UgIT09IHRoaXMuc3RhdGUucGFzc1BocmFzZUNvbmZpcm0pIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5fa2V5QmFja3VwSW5mbyA9IGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5wcmVwYXJlS2V5QmFja3VwVmVyc2lvbih0aGlzLnN0YXRlLnBhc3NQaHJhc2UpO1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBjb3BpZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkb3dubG9hZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dLRVksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9vblNldEFnYWluQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6ICcnLFxyXG4gICAgICAgICAgICBwYXNzUGhyYXNlQ29uZmlybTogJycsXHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9QQVNTUEhSQVNFLFxyXG4gICAgICAgICAgICB6eGN2Ym5SZXN1bHQ6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uS2VlcEl0U2FmZUJhY2tDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dLRVksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUGFzc1BocmFzZUNoYW5nZSA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLy8gcHJlY29tcHV0ZSB0aGlzIGFuZCBrZWVwIGl0IGluIHN0YXRlOiB6eGN2Ym4gaXMgZmFzdCBidXRcclxuICAgICAgICAgICAgICAgIC8vIHdlIHVzZSBpdCBpbiBhIGNvdXBsZSBvZiBkaWZmZXJlbnQgcGxhY2VzIHNvIG5vIHBvaW50IHJlY29tcHV0aW5nXHJcbiAgICAgICAgICAgICAgICAvLyBpdCB1bm5lY2Vzc2FyaWx5LlxyXG4gICAgICAgICAgICAgICAgenhjdmJuUmVzdWx0OiBzY29yZVBhc3N3b3JkKHRoaXMuc3RhdGUucGFzc1BocmFzZSksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIFBBU1NQSFJBU0VfRkVFREJBQ0tfREVMQVkpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblBhc3NQaHJhc2VDb25maXJtQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGFzc1BocmFzZUNvbmZpcm06IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9wYXNzUGhyYXNlSXNWYWxpZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQgJiYgdGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuc2NvcmUgPj0gUEFTU1dPUkRfTUlOX1NDT1JFO1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZVBhc3NQaHJhc2UoKSB7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuXHJcbiAgICAgICAgbGV0IHN0cmVuZ3RoTWV0ZXI7XHJcbiAgICAgICAgbGV0IGhlbHBUZXh0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnp4Y3ZiblJlc3VsdCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuc2NvcmUgPj0gUEFTU1dPUkRfTUlOX1NDT1JFKSB7XHJcbiAgICAgICAgICAgICAgICBoZWxwVGV4dCA9IF90KFwiR3JlYXQhIFRoaXMgcGFzc3BocmFzZSBsb29rcyBzdHJvbmcgZW5vdWdoLlwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHN1Z2dlc3Rpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3RhdGUuenhjdmJuUmVzdWx0LmZlZWRiYWNrLnN1Z2dlc3Rpb25zLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3VnZ2VzdGlvbnMucHVzaCg8ZGl2IGtleT17aX0+e3RoaXMuc3RhdGUuenhjdmJuUmVzdWx0LmZlZWRiYWNrLnN1Z2dlc3Rpb25zW2ldfTwvZGl2Pik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzdWdnZXN0aW9uQmxvY2sgPSA8ZGl2PntzdWdnZXN0aW9ucy5sZW5ndGggPiAwID8gc3VnZ2VzdGlvbnMgOiBfdChcIktlZXAgZ29pbmcuLi5cIil9PC9kaXY+O1xyXG5cclxuICAgICAgICAgICAgICAgIGhlbHBUZXh0ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuZmVlZGJhY2sud2FybmluZ31cclxuICAgICAgICAgICAgICAgICAgICB7c3VnZ2VzdGlvbkJsb2NrfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHN0cmVuZ3RoTWV0ZXIgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHByb2dyZXNzIG1heD17UEFTU1dPUkRfTUlOX1NDT1JFfSB2YWx1ZT17dGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuc2NvcmV9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25QYXNzUGhyYXNlTmV4dENsaWNrfT5cclxuICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgXCI8Yj5XYXJuaW5nPC9iPjogWW91IHNob3VsZCBvbmx5IHNldCB1cCBrZXkgYmFja3VwIGZyb20gYSB0cnVzdGVkIGNvbXB1dGVyLlwiLCB7fSxcclxuICAgICAgICAgICAgICAgIHsgYjogc3ViID0+IDxiPntzdWJ9PC9iPiB9LFxyXG4gICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgXCJXZSdsbCBzdG9yZSBhbiBlbmNyeXB0ZWQgY29weSBvZiB5b3VyIGtleXMgb24gb3VyIHNlcnZlci4gXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJQcm90ZWN0IHlvdXIgYmFja3VwIHdpdGggYSBwYXNzcGhyYXNlIHRvIGtlZXAgaXQgc2VjdXJlLlwiLFxyXG4gICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgPHA+e190KFwiRm9yIG1heGltdW0gc2VjdXJpdHksIHRoaXMgc2hvdWxkIGJlIGRpZmZlcmVudCBmcm9tIHlvdXIgYWNjb3VudCBwYXNzd29yZC5cIil9PC9wPlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9DcmVhdGVLZXlCYWNrdXBEaWFsb2dfcHJpbWFyeUNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9DcmVhdGVLZXlCYWNrdXBEaWFsb2dfcGFzc1BocmFzZUNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25QYXNzUGhyYXNlQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5wYXNzUGhyYXNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9DcmVhdGVLZXlCYWNrdXBEaWFsb2dfcGFzc1BocmFzZUlucHV0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e190KFwiRW50ZXIgYSBwYXNzcGhyYXNlLi4uXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvRm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19wYXNzUGhyYXNlSGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7c3RyZW5ndGhNZXRlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAge2hlbHBUZXh0fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e190KCdOZXh0Jyl9XHJcbiAgICAgICAgICAgICAgICBvblByaW1hcnlCdXR0b25DbGljaz17dGhpcy5fb25QYXNzUGhyYXNlTmV4dENsaWNrfVxyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXtmYWxzZX1cclxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXshdGhpcy5fcGFzc1BocmFzZUlzVmFsaWQoKX1cclxuICAgICAgICAgICAgLz5cclxuXHJcbiAgICAgICAgICAgIDxkZXRhaWxzPlxyXG4gICAgICAgICAgICAgICAgPHN1bW1hcnk+e190KFwiQWR2YW5jZWRcIil9PC9zdW1tYXJ5PlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD0ncHJpbWFyeScgb25DbGljaz17dGhpcy5fb25Ta2lwUGFzc1BocmFzZUNsaWNrfSA+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiU2V0IHVwIHdpdGggYSByZWNvdmVyeSBrZXlcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGV0YWlscz5cclxuICAgICAgICA8L2Zvcm0+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZVBhc3NQaHJhc2VDb25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcblxyXG4gICAgICAgIGxldCBtYXRjaFRleHQ7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucGFzc1BocmFzZUNvbmZpcm0gPT09IHRoaXMuc3RhdGUucGFzc1BocmFzZSkge1xyXG4gICAgICAgICAgICBtYXRjaFRleHQgPSBfdChcIlRoYXQgbWF0Y2hlcyFcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5zdGF0ZS5wYXNzUGhyYXNlLnN0YXJ0c1dpdGgodGhpcy5zdGF0ZS5wYXNzUGhyYXNlQ29uZmlybSkpIHtcclxuICAgICAgICAgICAgLy8gb25seSB0ZWxsIHRoZW0gdGhleSdyZSB3cm9uZyBpZiB0aGV5J3ZlIGFjdHVhbGx5IGdvbmUgd3JvbmcuXHJcbiAgICAgICAgICAgIC8vIFNlY3VyaXR5IGNvbmNpb3VzIHJlYWRlcnMgd2lsbCBub3RlIHRoYXQgaWYgeW91IGxlZnQgcmlvdC13ZWIgdW5hdHRlbmRlZFxyXG4gICAgICAgICAgICAvLyBvbiB0aGlzIHNjcmVlbiwgdGhpcyB3b3VsZCBtYWtlIGl0IGVhc3kgZm9yIGEgbWFsaWNpb3VzIHBlcnNvbiB0byBndWVzc1xyXG4gICAgICAgICAgICAvLyB5b3VyIHBhc3NwaHJhc2Ugb25lIGxldHRlciBhdCBhIHRpbWUsIGJ1dCB0aGV5IGNvdWxkIGdldCB0aGlzIGZhc3RlciBieVxyXG4gICAgICAgICAgICAvLyBqdXN0IG9wZW5pbmcgdGhlIGJyb3dzZXIncyBkZXZlbG9wZXIgdG9vbHMgYW5kIHJlYWRpbmcgaXQuXHJcbiAgICAgICAgICAgIC8vIE5vdGUgdGhhdCBub3QgaGF2aW5nIHR5cGVkIGFueXRoaW5nIGF0IGFsbCB3aWxsIG5vdCBoaXQgdGhpcyBjbGF1c2UgYW5kXHJcbiAgICAgICAgICAgIC8vIGZhbGwgdGhyb3VnaCBzbyBlbXB0eSBib3ggPT09IG5vIGhpbnQuXHJcbiAgICAgICAgICAgIG1hdGNoVGV4dCA9IF90KFwiVGhhdCBkb2Vzbid0IG1hdGNoLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBwYXNzUGhyYXNlTWF0Y2ggPSBudWxsO1xyXG4gICAgICAgIGlmIChtYXRjaFRleHQpIHtcclxuICAgICAgICAgICAgcGFzc1BocmFzZU1hdGNoID0gPGRpdiBjbGFzc05hbWU9XCJteF9DcmVhdGVLZXlCYWNrdXBEaWFsb2dfcGFzc1BocmFzZU1hdGNoXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PnttYXRjaFRleHR9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGVsZW1lbnQ9XCJzcGFuXCIgY2xhc3NOYW1lPVwibXhfbGlua0J1dHRvblwiIG9uQ2xpY2s9e3RoaXMuX29uU2V0QWdhaW5DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkdvIGJhY2sgdG8gc2V0IGl0IGFnYWluLlwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIHJldHVybiA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25QYXNzUGhyYXNlQ29uZmlybU5leHRDbGlja30+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiUGxlYXNlIGVudGVyIHlvdXIgcGFzc3BocmFzZSBhIHNlY29uZCB0aW1lIHRvIGNvbmZpcm0uXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19wcmltYXJ5Q29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19wYXNzUGhyYXNlQ29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25QYXNzUGhyYXNlQ29uZmlybUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnBhc3NQaHJhc2VDb25maXJtfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXhfQ3JlYXRlS2V5QmFja3VwRGlhbG9nX3Bhc3NQaHJhc2VJbnB1dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17X3QoXCJSZXBlYXQgeW91ciBwYXNzcGhyYXNlLi4uXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtwYXNzUGhyYXNlTWF0Y2h9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zXHJcbiAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdCgnTmV4dCcpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uUGFzc1BocmFzZUNvbmZpcm1OZXh0Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUucGFzc1BocmFzZSAhPT0gdGhpcy5zdGF0ZS5wYXNzUGhyYXNlQ29uZmlybX1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICA8L2Zvcm0+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZVNob3dLZXkoKSB7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiWW91ciByZWNvdmVyeSBrZXkgaXMgYSBzYWZldHkgbmV0IC0geW91IGNhbiB1c2UgaXQgdG8gcmVzdG9yZSBcIiArXHJcbiAgICAgICAgICAgICAgICBcImFjY2VzcyB0byB5b3VyIGVuY3J5cHRlZCBtZXNzYWdlcyBpZiB5b3UgZm9yZ2V0IHlvdXIgcGFzc3BocmFzZS5cIixcclxuICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiS2VlcCBhIGNvcHkgb2YgaXQgc29tZXdoZXJlIHNlY3VyZSwgbGlrZSBhIHBhc3N3b3JkIG1hbmFnZXIgb3IgZXZlbiBhIHNhZmUuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19wcmltYXJ5Q29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19yZWNvdmVyeUtleUhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIllvdXIgcmVjb3Zlcnkga2V5XCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19yZWNvdmVyeUtleUNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlS2V5QmFja3VwRGlhbG9nX3JlY292ZXJ5S2V5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxjb2RlIHJlZj17dGhpcy5fY29sbGVjdFJlY292ZXJ5S2V5Tm9kZX0+e3RoaXMuX2tleUJhY2t1cEluZm8ucmVjb3Zlcnlfa2V5fTwvY29kZT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZUtleUJhY2t1cERpYWxvZ19yZWNvdmVyeUtleUJ1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJteF9EaWFsb2dfcHJpbWFyeVwiIG9uQ2xpY2s9e3RoaXMuX29uQ29weUNsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkNvcHlcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cIm14X0RpYWxvZ19wcmltYXJ5XCIgb25DbGljaz17dGhpcy5fb25Eb3dubG9hZENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkRvd25sb2FkXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlS2VlcEl0U2FmZSgpIHtcclxuICAgICAgICBsZXQgaW50cm9UZXh0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmNvcGllZCkge1xyXG4gICAgICAgICAgICBpbnRyb1RleHQgPSBfdChcclxuICAgICAgICAgICAgICAgIFwiWW91ciByZWNvdmVyeSBrZXkgaGFzIGJlZW4gPGI+Y29waWVkIHRvIHlvdXIgY2xpcGJvYXJkPC9iPiwgcGFzdGUgaXQgdG86XCIsXHJcbiAgICAgICAgICAgICAgICB7fSwge2I6IHMgPT4gPGI+e3N9PC9iPn0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmRvd25sb2FkZWQpIHtcclxuICAgICAgICAgICAgaW50cm9UZXh0ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdXIgcmVjb3Zlcnkga2V5IGlzIGluIHlvdXIgPGI+RG93bmxvYWRzPC9iPiBmb2xkZXIuXCIsXHJcbiAgICAgICAgICAgICAgICB7fSwge2I6IHMgPT4gPGI+e3N9PC9iPn0sXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIHtpbnRyb1RleHR9XHJcbiAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgIDxsaT57X3QoXCI8Yj5QcmludCBpdDwvYj4gYW5kIHN0b3JlIGl0IHNvbWV3aGVyZSBzYWZlXCIsIHt9LCB7YjogcyA9PiA8Yj57c308L2I+fSl9PC9saT5cclxuICAgICAgICAgICAgICAgIDxsaT57X3QoXCI8Yj5TYXZlIGl0PC9iPiBvbiBhIFVTQiBrZXkgb3IgYmFja3VwIGRyaXZlXCIsIHt9LCB7YjogcyA9PiA8Yj57c308L2I+fSl9PC9saT5cclxuICAgICAgICAgICAgICAgIDxsaT57X3QoXCI8Yj5Db3B5IGl0PC9iPiB0byB5b3VyIHBlcnNvbmFsIGNsb3VkIHN0b3JhZ2VcIiwge30sIHtiOiBzID0+IDxiPntzfTwvYj59KX08L2xpPlxyXG4gICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdChcIkNvbnRpbnVlXCIpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX2NyZWF0ZUJhY2t1cH1cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXt0aGlzLl9vbktlZXBJdFNhZmVCYWNrQ2xpY2t9PntfdChcIkJhY2tcIil9PC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvRGlhbG9nQnV0dG9ucz5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlckJ1c3lQaGFzZSh0ZXh0KSB7XHJcbiAgICAgICAgY29uc3QgU3Bpbm5lciA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLlNwaW5uZXInKTtcclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPFNwaW5uZXIgLz5cclxuICAgICAgICA8L2Rpdj47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlRG9uZSgpIHtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdXIga2V5cyBhcmUgYmVpbmcgYmFja2VkIHVwICh0aGUgZmlyc3QgYmFja3VwIGNvdWxkIHRha2UgYSBmZXcgbWludXRlcykuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdCgnT0snKX1cclxuICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vbkRvbmV9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyUGhhc2VPcHRPdXRDb25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgIFwiV2l0aG91dCBzZXR0aW5nIHVwIFNlY3VyZSBNZXNzYWdlIFJlY292ZXJ5LCB5b3Ugd29uJ3QgYmUgYWJsZSB0byByZXN0b3JlIHlvdXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJlbmNyeXB0ZWQgbWVzc2FnZSBoaXN0b3J5IGlmIHlvdSBsb2cgb3V0IG9yIHVzZSBhbm90aGVyIHNlc3Npb24uXCIsXHJcbiAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e190KCdTZXQgdXAgU2VjdXJlIE1lc3NhZ2UgUmVjb3ZlcnknKX1cclxuICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9vblNldFVwQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uQ2FuY2VsfT5JIHVuZGVyc3RhbmQsIGNvbnRpbnVlIHdpdGhvdXQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9EaWFsb2dCdXR0b25zPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICBfdGl0bGVGb3JQaGFzZShwaGFzZSkge1xyXG4gICAgICAgIHN3aXRjaCAocGhhc2UpIHtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9QQVNTUEhSQVNFOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdTZWN1cmUgeW91ciBiYWNrdXAgd2l0aCBhIHBhc3NwaHJhc2UnKTtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9QQVNTUEhSQVNFX0NPTkZJUk06XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ0NvbmZpcm0geW91ciBwYXNzcGhyYXNlJyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfT1BUT1VUX0NPTkZJUk06XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1dhcm5pbmchJyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfU0hPV0tFWTpcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9LRUVQSVRTQUZFOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdNYWtlIGEgY29weSBvZiB5b3VyIHJlY292ZXJ5IGtleScpO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX0JBQ0tJTkdVUDpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU3RhcnRpbmcgYmFja3VwLi4uJyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfRE9ORTpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU3VjY2VzcyEnKTtcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdChcIkNyZWF0ZSBrZXkgYmFja3VwXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZycpO1xyXG5cclxuICAgICAgICBsZXQgY29udGVudDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5lcnJvcikge1xyXG4gICAgICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIDxwPntfdChcIlVuYWJsZSB0byBjcmVhdGUga2V5IGJhY2t1cFwiKX08L3A+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0RpYWxvZ19idXR0b25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnMgcHJpbWFyeUJ1dHRvbj17X3QoJ1JldHJ5Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9jcmVhdGVCYWNrdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMuX29uQ2FuY2VsfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAodGhpcy5zdGF0ZS5waGFzZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBQSEFTRV9QQVNTUEhSQVNFOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJQaGFzZVBhc3NQaHJhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfUEFTU1BIUkFTRV9DT05GSVJNOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJQaGFzZVBhc3NQaHJhc2VDb25maXJtKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBIQVNFX1NIT1dLRVk6XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IHRoaXMuX3JlbmRlclBoYXNlU2hvd0tleSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBQSEFTRV9LRUVQSVRTQUZFOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJQaGFzZUtlZXBJdFNhZmUoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfQkFDS0lOR1VQOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJCdXN5UGhhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfRE9ORTpcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gdGhpcy5fcmVuZGVyUGhhc2VEb25lKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBIQVNFX09QVE9VVF9DT05GSVJNOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJQaGFzZU9wdE91dENvbmZpcm0oKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VEaWFsb2cgY2xhc3NOYW1lPSdteF9DcmVhdGVLZXlCYWNrdXBEaWFsb2cnXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGhpcy5fdGl0bGVGb3JQaGFzZSh0aGlzLnN0YXRlLnBoYXNlKX1cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17W1BIQVNFX1BBU1NQSFJBU0UsIFBIQVNFX0RPTkVdLmluY2x1ZGVzKHRoaXMuc3RhdGUucGhhc2UpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICB7Y29udGVudH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvQmFzZURpYWxvZz5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==