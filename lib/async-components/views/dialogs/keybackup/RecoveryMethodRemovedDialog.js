"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard3(require("../../../../index"));

var _dispatcher = _interopRequireDefault(require("../../../../dispatcher"));

var _languageHandler = require("../../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../../Modal"));

/*
Copyright 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class RecoveryMethodRemovedDialog extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onGoToSettingsClick", () => {
      this.props.onFinished();

      _dispatcher.default.dispatch({
        action: 'view_user_settings'
      });
    });
    (0, _defineProperty2.default)(this, "onSetupClick", () => {
      this.props.onFinished();

      _Modal.default.createTrackedDialogAsync("Key Backup", "Key Backup", Promise.resolve().then(() => (0, _interopRequireWildcard2.default)(require("./CreateKeyBackupDialog"))), null, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
  }

  render() {
    const BaseDialog = sdk.getComponent("views.dialogs.BaseDialog");
    const DialogButtons = sdk.getComponent("views.elements.DialogButtons");

    const title = _react.default.createElement("span", {
      className: "mx_KeyBackupFailedDialog_title"
    }, (0, _languageHandler._t)("Recovery Method Removed"));

    return _react.default.createElement(BaseDialog, {
      className: "mx_KeyBackupFailedDialog",
      onFinished: this.props.onFinished,
      title: title
    }, _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("This session has detected that your recovery passphrase and key " + "for Secure Messages have been removed.")), _react.default.createElement("p", null, (0, _languageHandler._t)("If you did this accidentally, you can setup Secure Messages on " + "this session which will re-encrypt this session's message " + "history with a new recovery method.")), _react.default.createElement("p", {
      className: "warning"
    }, (0, _languageHandler._t)("If you didn't remove the recovery method, an " + "attacker may be trying to access your account. " + "Change your account password and set a new recovery " + "method immediately in Settings.")), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Set up Secure Messages"),
      onPrimaryButtonClick: this.onSetupClick,
      cancelButton: (0, _languageHandler._t)("Go to Settings"),
      onCancel: this.onGoToSettingsClick
    })));
  }

}

exports.default = RecoveryMethodRemovedDialog;
(0, _defineProperty2.default)(RecoveryMethodRemovedDialog, "propTypes", {
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9hc3luYy1jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mva2V5YmFja3VwL1JlY292ZXJ5TWV0aG9kUmVtb3ZlZERpYWxvZy5qcyJdLCJuYW1lcyI6WyJSZWNvdmVyeU1ldGhvZFJlbW92ZWREaWFsb2ciLCJSZWFjdCIsIlB1cmVDb21wb25lbnQiLCJwcm9wcyIsIm9uRmluaXNoZWQiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZ0FzeW5jIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIkRpYWxvZ0J1dHRvbnMiLCJ0aXRsZSIsIm9uU2V0dXBDbGljayIsIm9uR29Ub1NldHRpbmdzQ2xpY2siLCJQcm9wVHlwZXMiLCJmdW5jIiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXdCZSxNQUFNQSwyQkFBTixTQUEwQ0MsZUFBTUMsYUFBaEQsQ0FBOEQ7QUFBQTtBQUFBO0FBQUEsK0RBS25ELE1BQU07QUFDeEIsV0FBS0MsS0FBTCxDQUFXQyxVQUFYOztBQUNBQywwQkFBSUMsUUFBSixDQUFhO0FBQUVDLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BQWI7QUFDSCxLQVJ3RTtBQUFBLHdEQVUxRCxNQUFNO0FBQ2pCLFdBQUtKLEtBQUwsQ0FBV0MsVUFBWDs7QUFDQUkscUJBQU1DLHdCQUFOLENBQStCLFlBQS9CLEVBQTZDLFlBQTdDLDZFQUNXLHlCQURYLEtBRUksSUFGSixFQUVVLElBRlY7QUFFZ0I7QUFBaUIsV0FGakM7QUFFd0M7QUFBZSxVQUZ2RDtBQUlILEtBaEJ3RTtBQUFBOztBQWtCekVDLEVBQUFBLE1BQU0sR0FBRztBQUNMLFVBQU1DLFVBQVUsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUNBLFVBQU1DLGFBQWEsR0FBR0YsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0Qjs7QUFFQSxVQUFNRSxLQUFLLEdBQUc7QUFBTSxNQUFBLFNBQVMsRUFBQztBQUFoQixPQUNULHlCQUFHLHlCQUFILENBRFMsQ0FBZDs7QUFJQSxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQywwQkFBdEI7QUFDSSxNQUFBLFVBQVUsRUFBRSxLQUFLWixLQUFMLENBQVdDLFVBRDNCO0FBRUksTUFBQSxLQUFLLEVBQUVXO0FBRlgsT0FJSSwwQ0FDSSx3Q0FBSSx5QkFDQSxxRUFDQSx3Q0FGQSxDQUFKLENBREosRUFLSSx3Q0FBSSx5QkFDQSxvRUFDQSw0REFEQSxHQUVBLHFDQUhBLENBQUosQ0FMSixFQVVJO0FBQUcsTUFBQSxTQUFTLEVBQUM7QUFBYixPQUF3Qix5QkFDcEIsa0RBQ0EsaURBREEsR0FFQSxzREFGQSxHQUdBLGlDQUpvQixDQUF4QixDQVZKLEVBZ0JJLDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyx3QkFBSCxDQURuQjtBQUVJLE1BQUEsb0JBQW9CLEVBQUUsS0FBS0MsWUFGL0I7QUFHSSxNQUFBLFlBQVksRUFBRSx5QkFBRyxnQkFBSCxDQUhsQjtBQUlJLE1BQUEsUUFBUSxFQUFFLEtBQUtDO0FBSm5CLE1BaEJKLENBSkosQ0FESjtBQThCSDs7QUF4RHdFOzs7OEJBQXhEakIsMkIsZUFDRTtBQUNmSSxFQUFBQSxVQUFVLEVBQUVjLG1CQUFVQyxJQUFWLENBQWVDO0FBRFosQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tIFwiLi4vLi4vLi4vLi4vaW5kZXhcIjtcclxuaW1wb3J0IGRpcyBmcm9tIFwiLi4vLi4vLi4vLi4vZGlzcGF0Y2hlclwiO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gXCIuLi8uLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXJcIjtcclxuaW1wb3J0IE1vZGFsIGZyb20gXCIuLi8uLi8uLi8uLi9Nb2RhbFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmVjb3ZlcnlNZXRob2RSZW1vdmVkRGlhbG9nIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xyXG4gICAgICAgIG9uRmluaXNoZWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXHJcbiAgICB9XHJcblxyXG4gICAgb25Hb1RvU2V0dGluZ3NDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICd2aWV3X3VzZXJfc2V0dGluZ3MnIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2V0dXBDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nQXN5bmMoXCJLZXkgQmFja3VwXCIsIFwiS2V5IEJhY2t1cFwiLFxyXG4gICAgICAgICAgICBpbXBvcnQoXCIuL0NyZWF0ZUtleUJhY2t1cERpYWxvZ1wiKSxcclxuICAgICAgICAgICAgbnVsbCwgbnVsbCwgLyogcHJpb3JpdHkgPSAqLyBmYWxzZSwgLyogc3RhdGljID0gKi8gdHJ1ZSxcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICBjb25zdCBCYXNlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmRpYWxvZ3MuQmFzZURpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudChcInZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnNcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpdGxlID0gPHNwYW4gY2xhc3NOYW1lPVwibXhfS2V5QmFja3VwRmFpbGVkRGlhbG9nX3RpdGxlXCI+XHJcbiAgICAgICAgICAgIHtfdChcIlJlY292ZXJ5IE1ldGhvZCBSZW1vdmVkXCIpfVxyXG4gICAgICAgIDwvc3Bhbj47XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nIGNsYXNzTmFtZT1cIm14X0tleUJhY2t1cEZhaWxlZERpYWxvZ1wiXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGl0bGV9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlRoaXMgc2Vzc2lvbiBoYXMgZGV0ZWN0ZWQgdGhhdCB5b3VyIHJlY292ZXJ5IHBhc3NwaHJhc2UgYW5kIGtleSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZm9yIFNlY3VyZSBNZXNzYWdlcyBoYXZlIGJlZW4gcmVtb3ZlZC5cIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiSWYgeW91IGRpZCB0aGlzIGFjY2lkZW50YWxseSwgeW91IGNhbiBzZXR1cCBTZWN1cmUgTWVzc2FnZXMgb24gXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcInRoaXMgc2Vzc2lvbiB3aGljaCB3aWxsIHJlLWVuY3J5cHQgdGhpcyBzZXNzaW9uJ3MgbWVzc2FnZSBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiaGlzdG9yeSB3aXRoIGEgbmV3IHJlY292ZXJ5IG1ldGhvZC5cIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ3YXJuaW5nXCI+e190KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIklmIHlvdSBkaWRuJ3QgcmVtb3ZlIHRoZSByZWNvdmVyeSBtZXRob2QsIGFuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJhdHRhY2tlciBtYXkgYmUgdHJ5aW5nIHRvIGFjY2VzcyB5b3VyIGFjY291bnQuIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJDaGFuZ2UgeW91ciBhY2NvdW50IHBhc3N3b3JkIGFuZCBzZXQgYSBuZXcgcmVjb3ZlcnkgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIm1ldGhvZCBpbW1lZGlhdGVseSBpbiBTZXR0aW5ncy5cIixcclxuICAgICAgICAgICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9uc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdChcIlNldCB1cCBTZWN1cmUgTWVzc2FnZXNcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLm9uU2V0dXBDbGlja31cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uPXtfdChcIkdvIHRvIFNldHRpbmdzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17dGhpcy5vbkdvVG9TZXR0aW5nc0NsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9CYXNlRGlhbG9nPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19