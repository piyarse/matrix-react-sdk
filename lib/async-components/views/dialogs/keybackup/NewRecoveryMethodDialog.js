"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("../../../../dispatcher"));

var _languageHandler = require("../../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../../Modal"));

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class NewRecoveryMethodDialog extends _react.default.PureComponent {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "onOkClick", () => {
      this.props.onFinished();
    });
    (0, _defineProperty2.default)(this, "onGoToSettingsClick", () => {
      this.props.onFinished();

      _dispatcher.default.dispatch({
        action: 'view_user_settings'
      });
    });
    (0, _defineProperty2.default)(this, "onSetupClick", async () => {
      const RestoreKeyBackupDialog = sdk.getComponent('dialogs.keybackup.RestoreKeyBackupDialog');

      _Modal.default.createTrackedDialog('Restore Backup', '', RestoreKeyBackupDialog, {
        onFinished: this.props.onFinished
      }, null,
      /* priority = */
      false,
      /* static = */
      true);
    });
  }

  render() {
    const BaseDialog = sdk.getComponent("views.dialogs.BaseDialog");
    const DialogButtons = sdk.getComponent("views.elements.DialogButtons");

    const title = _react.default.createElement("span", {
      className: "mx_KeyBackupFailedDialog_title"
    }, (0, _languageHandler._t)("New Recovery Method"));

    const newMethodDetected = _react.default.createElement("p", null, (0, _languageHandler._t)("A new recovery passphrase and key for Secure " + "Messages have been detected."));

    const hackWarning = _react.default.createElement("p", {
      className: "warning"
    }, (0, _languageHandler._t)("If you didn't set the new recovery method, an " + "attacker may be trying to access your account. " + "Change your account password and set a new recovery " + "method immediately in Settings."));

    let content;

    if (_MatrixClientPeg.MatrixClientPeg.get().getKeyBackupEnabled()) {
      content = _react.default.createElement("div", null, newMethodDetected, _react.default.createElement("p", null, (0, _languageHandler._t)("This session is encrypting history using the new recovery method.")), hackWarning, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)("OK"),
        onPrimaryButtonClick: this.onOkClick,
        cancelButton: (0, _languageHandler._t)("Go to Settings"),
        onCancel: this.onGoToSettingsClick
      }));
    } else {
      content = _react.default.createElement("div", null, newMethodDetected, hackWarning, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)("Set up Secure Messages"),
        onPrimaryButtonClick: this.onSetupClick,
        cancelButton: (0, _languageHandler._t)("Go to Settings"),
        onCancel: this.onGoToSettingsClick
      }));
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_KeyBackupFailedDialog",
      onFinished: this.props.onFinished,
      title: title
    }, content);
  }

}

exports.default = NewRecoveryMethodDialog;
(0, _defineProperty2.default)(NewRecoveryMethodDialog, "propTypes", {
  // As returned by js-sdk getKeyBackupVersion()
  newVersionInfo: _propTypes.default.object,
  onFinished: _propTypes.default.func.isRequired
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9hc3luYy1jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mva2V5YmFja3VwL05ld1JlY292ZXJ5TWV0aG9kRGlhbG9nLmpzIl0sIm5hbWVzIjpbIk5ld1JlY292ZXJ5TWV0aG9kRGlhbG9nIiwiUmVhY3QiLCJQdXJlQ29tcG9uZW50IiwicHJvcHMiLCJvbkZpbmlzaGVkIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJSZXN0b3JlS2V5QmFja3VwRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsIkRpYWxvZ0J1dHRvbnMiLCJ0aXRsZSIsIm5ld01ldGhvZERldGVjdGVkIiwiaGFja1dhcm5pbmciLCJjb250ZW50IiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0S2V5QmFja3VwRW5hYmxlZCIsIm9uT2tDbGljayIsIm9uR29Ub1NldHRpbmdzQ2xpY2siLCJvblNldHVwQ2xpY2siLCJuZXdWZXJzaW9uSW5mbyIsIlByb3BUeXBlcyIsIm9iamVjdCIsImZ1bmMiLCJpc1JlcXVpcmVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXlCZSxNQUFNQSx1QkFBTixTQUFzQ0MsZUFBTUMsYUFBNUMsQ0FBMEQ7QUFBQTtBQUFBO0FBQUEscURBT3pELE1BQU07QUFDZCxXQUFLQyxLQUFMLENBQVdDLFVBQVg7QUFDSCxLQVRvRTtBQUFBLCtEQVcvQyxNQUFNO0FBQ3hCLFdBQUtELEtBQUwsQ0FBV0MsVUFBWDs7QUFDQUMsMEJBQUlDLFFBQUosQ0FBYTtBQUFFQyxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiO0FBQ0gsS0Fkb0U7QUFBQSx3REFnQnRELFlBQVk7QUFDdkIsWUFBTUMsc0JBQXNCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQ0FBakIsQ0FBL0I7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUNJLGdCQURKLEVBQ3NCLEVBRHRCLEVBQzBCSixzQkFEMUIsRUFDa0Q7QUFDMUNKLFFBQUFBLFVBQVUsRUFBRSxLQUFLRCxLQUFMLENBQVdDO0FBRG1CLE9BRGxELEVBR08sSUFIUDtBQUdhO0FBQWlCLFdBSDlCO0FBR3FDO0FBQWUsVUFIcEQ7QUFLSCxLQXZCb0U7QUFBQTs7QUF5QnJFUyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNQyxVQUFVLEdBQUdMLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7QUFDQSxVQUFNSyxhQUFhLEdBQUdOLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7O0FBRUEsVUFBTU0sS0FBSyxHQUFHO0FBQU0sTUFBQSxTQUFTLEVBQUM7QUFBaEIsT0FDVCx5QkFBRyxxQkFBSCxDQURTLENBQWQ7O0FBSUEsVUFBTUMsaUJBQWlCLEdBQUcsd0NBQUkseUJBQzFCLGtEQUNBLDhCQUYwQixDQUFKLENBQTFCOztBQUtBLFVBQU1DLFdBQVcsR0FBRztBQUFHLE1BQUEsU0FBUyxFQUFDO0FBQWIsT0FBd0IseUJBQ3hDLG1EQUNBLGlEQURBLEdBRUEsc0RBRkEsR0FHQSxpQ0FKd0MsQ0FBeEIsQ0FBcEI7O0FBT0EsUUFBSUMsT0FBSjs7QUFDQSxRQUFJQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxtQkFBdEIsRUFBSixFQUFpRDtBQUM3Q0gsTUFBQUEsT0FBTyxHQUFHLDBDQUNMRixpQkFESyxFQUVOLHdDQUFJLHlCQUNBLG1FQURBLENBQUosQ0FGTSxFQUtMQyxXQUxLLEVBTU4sNkJBQUMsYUFBRDtBQUNJLFFBQUEsYUFBYSxFQUFFLHlCQUFHLElBQUgsQ0FEbkI7QUFFSSxRQUFBLG9CQUFvQixFQUFFLEtBQUtLLFNBRi9CO0FBR0ksUUFBQSxZQUFZLEVBQUUseUJBQUcsZ0JBQUgsQ0FIbEI7QUFJSSxRQUFBLFFBQVEsRUFBRSxLQUFLQztBQUpuQixRQU5NLENBQVY7QUFhSCxLQWRELE1BY087QUFDSEwsTUFBQUEsT0FBTyxHQUFHLDBDQUNMRixpQkFESyxFQUVMQyxXQUZLLEVBR04sNkJBQUMsYUFBRDtBQUNJLFFBQUEsYUFBYSxFQUFFLHlCQUFHLHdCQUFILENBRG5CO0FBRUksUUFBQSxvQkFBb0IsRUFBRSxLQUFLTyxZQUYvQjtBQUdJLFFBQUEsWUFBWSxFQUFFLHlCQUFHLGdCQUFILENBSGxCO0FBSUksUUFBQSxRQUFRLEVBQUUsS0FBS0Q7QUFKbkIsUUFITSxDQUFWO0FBVUg7O0FBRUQsV0FDSSw2QkFBQyxVQUFEO0FBQVksTUFBQSxTQUFTLEVBQUMsMEJBQXRCO0FBQ0ksTUFBQSxVQUFVLEVBQUUsS0FBS3JCLEtBQUwsQ0FBV0MsVUFEM0I7QUFFSSxNQUFBLEtBQUssRUFBRVk7QUFGWCxPQUlLRyxPQUpMLENBREo7QUFRSDs7QUFqRm9FOzs7OEJBQXBEbkIsdUIsZUFDRTtBQUNmO0FBQ0EwQixFQUFBQSxjQUFjLEVBQUVDLG1CQUFVQyxNQUZYO0FBR2Z4QixFQUFBQSxVQUFVLEVBQUV1QixtQkFBVUUsSUFBVixDQUFlQztBQUhaLEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCwgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uLy4uLy4uLy4uL2luZGV4XCI7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi8uLi8uLi8uLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgZGlzIGZyb20gXCIuLi8uLi8uLi8uLi9kaXNwYXRjaGVyXCI7XHJcbmltcG9ydCB7IF90IH0gZnJvbSBcIi4uLy4uLy4uLy4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4uLy4uLy4uLy4uL01vZGFsXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOZXdSZWNvdmVyeU1ldGhvZERpYWxvZyBleHRlbmRzIFJlYWN0LlB1cmVDb21wb25lbnQge1xyXG4gICAgc3RhdGljIHByb3BUeXBlcyA9IHtcclxuICAgICAgICAvLyBBcyByZXR1cm5lZCBieSBqcy1zZGsgZ2V0S2V5QmFja3VwVmVyc2lvbigpXHJcbiAgICAgICAgbmV3VmVyc2lvbkluZm86IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICAgICAgb25GaW5pc2hlZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcclxuICAgIH1cclxuXHJcbiAgICBvbk9rQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Hb1RvU2V0dGluZ3NDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnByb3BzLm9uRmluaXNoZWQoKTtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246ICd2aWV3X3VzZXJfc2V0dGluZ3MnIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2V0dXBDbGljayA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICBjb25zdCBSZXN0b3JlS2V5QmFja3VwRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5rZXliYWNrdXAuUmVzdG9yZUtleUJhY2t1cERpYWxvZycpO1xyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICdSZXN0b3JlIEJhY2t1cCcsICcnLCBSZXN0b3JlS2V5QmFja3VwRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiB0aGlzLnByb3BzLm9uRmluaXNoZWQsXHJcbiAgICAgICAgICAgIH0sIG51bGwsIC8qIHByaW9yaXR5ID0gKi8gZmFsc2UsIC8qIHN0YXRpYyA9ICovIHRydWUsXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3QgQmFzZURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2dcIik7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoXCJ2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zXCIpO1xyXG5cclxuICAgICAgICBjb25zdCB0aXRsZSA9IDxzcGFuIGNsYXNzTmFtZT1cIm14X0tleUJhY2t1cEZhaWxlZERpYWxvZ190aXRsZVwiPlxyXG4gICAgICAgICAgICB7X3QoXCJOZXcgUmVjb3ZlcnkgTWV0aG9kXCIpfVxyXG4gICAgICAgIDwvc3Bhbj47XHJcblxyXG4gICAgICAgIGNvbnN0IG5ld01ldGhvZERldGVjdGVkID0gPHA+e190KFxyXG4gICAgICAgICAgICBcIkEgbmV3IHJlY292ZXJ5IHBhc3NwaHJhc2UgYW5kIGtleSBmb3IgU2VjdXJlIFwiICtcclxuICAgICAgICAgICAgXCJNZXNzYWdlcyBoYXZlIGJlZW4gZGV0ZWN0ZWQuXCIsXHJcbiAgICAgICAgKX08L3A+O1xyXG5cclxuICAgICAgICBjb25zdCBoYWNrV2FybmluZyA9IDxwIGNsYXNzTmFtZT1cIndhcm5pbmdcIj57X3QoXHJcbiAgICAgICAgICAgIFwiSWYgeW91IGRpZG4ndCBzZXQgdGhlIG5ldyByZWNvdmVyeSBtZXRob2QsIGFuIFwiICtcclxuICAgICAgICAgICAgXCJhdHRhY2tlciBtYXkgYmUgdHJ5aW5nIHRvIGFjY2VzcyB5b3VyIGFjY291bnQuIFwiICtcclxuICAgICAgICAgICAgXCJDaGFuZ2UgeW91ciBhY2NvdW50IHBhc3N3b3JkIGFuZCBzZXQgYSBuZXcgcmVjb3ZlcnkgXCIgK1xyXG4gICAgICAgICAgICBcIm1ldGhvZCBpbW1lZGlhdGVseSBpbiBTZXR0aW5ncy5cIixcclxuICAgICAgICApfTwvcD47XHJcblxyXG4gICAgICAgIGxldCBjb250ZW50O1xyXG4gICAgICAgIGlmIChNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0S2V5QmFja3VwRW5hYmxlZCgpKSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAge25ld01ldGhvZERldGVjdGVkfVxyXG4gICAgICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgICAgIFwiVGhpcyBzZXNzaW9uIGlzIGVuY3J5cHRpbmcgaGlzdG9yeSB1c2luZyB0aGUgbmV3IHJlY292ZXJ5IG1ldGhvZC5cIixcclxuICAgICAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICAgICAge2hhY2tXYXJuaW5nfVxyXG4gICAgICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5QnV0dG9uPXtfdChcIk9LXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLm9uT2tDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b249e190KFwiR28gdG8gU2V0dGluZ3NcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMub25Hb1RvU2V0dGluZ3NDbGlja31cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb250ZW50ID0gPGRpdj5cclxuICAgICAgICAgICAgICAgIHtuZXdNZXRob2REZXRlY3RlZH1cclxuICAgICAgICAgICAgICAgIHtoYWNrV2FybmluZ31cclxuICAgICAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpbWFyeUJ1dHRvbj17X3QoXCJTZXQgdXAgU2VjdXJlIE1lc3NhZ2VzXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLm9uU2V0dXBDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b249e190KFwiR28gdG8gU2V0dGluZ3NcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMub25Hb1RvU2V0dGluZ3NDbGlja31cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nIGNsYXNzTmFtZT1cIm14X0tleUJhY2t1cEZhaWxlZERpYWxvZ1wiXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkPXt0aGlzLnByb3BzLm9uRmluaXNoZWR9XHJcbiAgICAgICAgICAgICAgICB0aXRsZT17dGl0bGV9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHtjb250ZW50fVxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=