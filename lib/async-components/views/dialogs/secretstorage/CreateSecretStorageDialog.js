"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var sdk = _interopRequireWildcard(require("../../../../index"));

var _MatrixClientPeg = require("../../../../MatrixClientPeg");

var _PasswordScorer = require("../../../../utils/PasswordScorer");

var _fileSaver = _interopRequireDefault(require("file-saver"));

var _languageHandler = require("../../../../languageHandler");

var _Modal = _interopRequireDefault(require("../../../../Modal"));

var _CrossSigningManager = require("../../../../CrossSigningManager");

/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const PHASE_LOADING = 0;
const PHASE_MIGRATE = 1;
const PHASE_PASSPHRASE = 2;
const PHASE_PASSPHRASE_CONFIRM = 3;
const PHASE_SHOWKEY = 4;
const PHASE_KEEPITSAFE = 5;
const PHASE_STORING = 6;
const PHASE_DONE = 7;
const PHASE_CONFIRM_SKIP = 8;
const PASSWORD_MIN_SCORE = 4; // So secure, many characters, much complex, wow, etc, etc.

const PASSPHRASE_FEEDBACK_DELAY = 500; // How long after keystroke to offer passphrase feedback, ms.
// XXX: copied from ShareDialog: factor out into utils

function selectText(target) {
  const range = document.createRange();
  range.selectNodeContents(target);
  const selection = window.getSelection();
  selection.removeAllRanges();
  selection.addRange(range);
}
/*
 * Walks the user through the process of creating a passphrase to guard Secure
 * Secret Storage in account data.
 */


class CreateSecretStorageDialog extends _react.default.PureComponent {
  constructor(props) {
    super(props);
    (0, _defineProperty2.default)(this, "_onKeyBackupStatusChange", () => {
      if (this.state.phase === PHASE_MIGRATE) this._fetchBackupInfo();
    });
    (0, _defineProperty2.default)(this, "_collectRecoveryKeyNode", n => {
      this._recoveryKeyNode = n;
    });
    (0, _defineProperty2.default)(this, "_onUseKeyBackupChange", enabled => {
      this.setState({
        useKeyBackup: enabled
      });
    });
    (0, _defineProperty2.default)(this, "_onMigrateFormSubmit", e => {
      e.preventDefault();

      if (this.state.backupSigStatus.usable) {
        this._bootstrapSecretStorage();
      } else {
        this._restoreBackup();
      }
    });
    (0, _defineProperty2.default)(this, "_onCopyClick", () => {
      selectText(this._recoveryKeyNode);
      const successful = document.execCommand('copy');

      if (successful) {
        this.setState({
          copied: true,
          phase: PHASE_KEEPITSAFE
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onDownloadClick", () => {
      const blob = new Blob([this._recoveryKey.encodedPrivateKey], {
        type: 'text/plain;charset=us-ascii'
      });

      _fileSaver.default.saveAs(blob, 'recovery-key.txt');

      this.setState({
        downloaded: true,
        phase: PHASE_KEEPITSAFE
      });
    });
    (0, _defineProperty2.default)(this, "_doBootstrapUIAuth", async makeRequest => {
      if (this.state.canUploadKeysWithPasswordOnly && this.state.accountPassword) {
        await makeRequest({
          type: 'm.login.password',
          identifier: {
            type: 'm.id.user',
            user: _MatrixClientPeg.MatrixClientPeg.get().getUserId()
          },
          // https://github.com/matrix-org/synapse/issues/5665
          user: _MatrixClientPeg.MatrixClientPeg.get().getUserId(),
          password: this.state.accountPassword
        });
      } else {
        const InteractiveAuthDialog = sdk.getComponent("dialogs.InteractiveAuthDialog");

        const {
          finished
        } = _Modal.default.createTrackedDialog('Cross-signing keys dialog', '', InteractiveAuthDialog, {
          title: (0, _languageHandler._t)("Setting up keys"),
          matrixClient: _MatrixClientPeg.MatrixClientPeg.get(),
          makeRequest
        });

        const [confirmed] = await finished;

        if (!confirmed) {
          throw new Error("Cross-signing key upload auth canceled");
        }
      }
    });
    (0, _defineProperty2.default)(this, "_bootstrapSecretStorage", async () => {
      this.setState({
        phase: PHASE_STORING,
        error: null
      });

      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const {
        force
      } = this.props;

      try {
        if (force) {
          await cli.bootstrapSecretStorage({
            authUploadDeviceSigningKeys: this._doBootstrapUIAuth,
            createSecretStorageKey: async () => this._recoveryKey,
            setupNewKeyBackup: true,
            setupNewSecretStorage: true
          });
        } else {
          await cli.bootstrapSecretStorage({
            authUploadDeviceSigningKeys: this._doBootstrapUIAuth,
            createSecretStorageKey: async () => this._recoveryKey,
            keyBackupInfo: this.state.backupInfo,
            setupNewKeyBackup: !this.state.backupInfo && this.state.useKeyBackup,
            getKeyBackupPassphrase: _CrossSigningManager.promptForBackupPassphrase
          });
        }

        this.setState({
          phase: PHASE_DONE
        });
      } catch (e) {
        if (this.state.canUploadKeysWithPasswordOnly && e.httpStatus === 401 && e.data.flows) {
          this.setState({
            accountPassword: '',
            accountPasswordCorrect: false,
            phase: PHASE_MIGRATE
          });
        } else {
          this.setState({
            error: e
          });
        }

        console.error("Error bootstrapping secret storage", e);
      }
    });
    (0, _defineProperty2.default)(this, "_onCancel", () => {
      this.props.onFinished(false);
    });
    (0, _defineProperty2.default)(this, "_onDone", () => {
      this.props.onFinished(true);
    });
    (0, _defineProperty2.default)(this, "_restoreBackup", async () => {
      const RestoreKeyBackupDialog = sdk.getComponent('dialogs.keybackup.RestoreKeyBackupDialog');

      const {
        finished
      } = _Modal.default.createTrackedDialog('Restore Backup', '', RestoreKeyBackupDialog, {
        showSummary: false
      }, null,
      /* priority = */
      false,
      /* static = */
      false);

      await finished;
      const {
        backupSigStatus
      } = await this._fetchBackupInfo();

      if (backupSigStatus.usable && this.state.canUploadKeysWithPasswordOnly && this.state.accountPassword) {
        this._bootstrapSecretStorage();
      }
    });
    (0, _defineProperty2.default)(this, "_onSkipSetupClick", () => {
      this.setState({
        phase: PHASE_CONFIRM_SKIP
      });
    });
    (0, _defineProperty2.default)(this, "_onSetUpClick", () => {
      this.setState({
        phase: PHASE_PASSPHRASE
      });
    });
    (0, _defineProperty2.default)(this, "_onSkipPassPhraseClick", async () => {
      this._recoveryKey = await _MatrixClientPeg.MatrixClientPeg.get().createRecoveryKeyFromPassphrase();
      this.setState({
        copied: false,
        downloaded: false,
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseNextClick", async e => {
      e.preventDefault(); // If we're waiting for the timeout before updating the result at this point,
      // skip ahead and do it now, otherwise we'll deny the attempt to proceed
      // even if the user entered a valid passphrase

      if (this._setZxcvbnResultTimeout !== null) {
        clearTimeout(this._setZxcvbnResultTimeout);
        this._setZxcvbnResultTimeout = null;
        await new Promise(resolve => {
          this.setState({
            zxcvbnResult: (0, _PasswordScorer.scorePassword)(this.state.passPhrase)
          }, resolve);
        });
      }

      if (this._passPhraseIsValid()) {
        this.setState({
          phase: PHASE_PASSPHRASE_CONFIRM
        });
      }
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseConfirmNextClick", async e => {
      e.preventDefault();
      if (this.state.passPhrase !== this.state.passPhraseConfirm) return;
      this._recoveryKey = await _MatrixClientPeg.MatrixClientPeg.get().createRecoveryKeyFromPassphrase(this.state.passPhrase);
      this.setState({
        copied: false,
        downloaded: false,
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onSetAgainClick", () => {
      this.setState({
        passPhrase: '',
        passPhraseConfirm: '',
        phase: PHASE_PASSPHRASE,
        zxcvbnResult: null
      });
    });
    (0, _defineProperty2.default)(this, "_onKeepItSafeBackClick", () => {
      this.setState({
        phase: PHASE_SHOWKEY
      });
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseChange", e => {
      this.setState({
        passPhrase: e.target.value
      });

      if (this._setZxcvbnResultTimeout !== null) {
        clearTimeout(this._setZxcvbnResultTimeout);
      }

      this._setZxcvbnResultTimeout = setTimeout(() => {
        this._setZxcvbnResultTimeout = null;
        this.setState({
          // precompute this and keep it in state: zxcvbn is fast but
          // we use it in a couple of different places so no point recomputing
          // it unnecessarily.
          zxcvbnResult: (0, _PasswordScorer.scorePassword)(this.state.passPhrase)
        });
      }, PASSPHRASE_FEEDBACK_DELAY);
    });
    (0, _defineProperty2.default)(this, "_onPassPhraseConfirmChange", e => {
      this.setState({
        passPhraseConfirm: e.target.value
      });
    });
    (0, _defineProperty2.default)(this, "_onAccountPasswordChange", e => {
      this.setState({
        accountPassword: e.target.value
      });
    });
    this._recoveryKey = null;
    this._recoveryKeyNode = null;
    this._setZxcvbnResultTimeout = null;
    this.state = {
      phase: PHASE_LOADING,
      passPhrase: '',
      passPhraseConfirm: '',
      copied: false,
      downloaded: false,
      zxcvbnResult: null,
      backupInfo: null,
      backupSigStatus: null,
      // does the server offer a UI auth flow with just m.login.password
      // for /keys/device_signing/upload?
      canUploadKeysWithPasswordOnly: null,
      accountPassword: props.accountPassword || "",
      accountPasswordCorrect: null,
      // status of the key backup toggle switch
      useKeyBackup: true
    };

    this._fetchBackupInfo();

    this._queryKeyUploadAuth();

    _MatrixClientPeg.MatrixClientPeg.get().on('crypto.keyBackupStatus', this._onKeyBackupStatusChange);
  }

  componentWillUnmount() {
    _MatrixClientPeg.MatrixClientPeg.get().removeListener('crypto.keyBackupStatus', this._onKeyBackupStatusChange);

    if (this._setZxcvbnResultTimeout !== null) {
      clearTimeout(this._setZxcvbnResultTimeout);
    }
  }

  async _fetchBackupInfo() {
    const backupInfo = await _MatrixClientPeg.MatrixClientPeg.get().getKeyBackupVersion();
    const backupSigStatus = // we may not have started crypto yet, in which case we definitely don't trust the backup
    _MatrixClientPeg.MatrixClientPeg.get().isCryptoEnabled() && (await _MatrixClientPeg.MatrixClientPeg.get().isKeyBackupTrusted(backupInfo));
    const {
      force
    } = this.props;
    const phase = backupInfo && !force ? PHASE_MIGRATE : PHASE_PASSPHRASE;
    this.setState({
      phase,
      backupInfo,
      backupSigStatus
    });
    return {
      backupInfo,
      backupSigStatus
    };
  }

  async _queryKeyUploadAuth() {
    try {
      await _MatrixClientPeg.MatrixClientPeg.get().uploadDeviceSigningKeys(null, {}); // We should never get here: the server should always require
      // UI auth to upload device signing keys. If we do, we upload
      // no keys which would be a no-op.

      console.log("uploadDeviceSigningKeys unexpectedly succeeded without UI auth!");
    } catch (error) {
      if (!error.data.flows) {
        console.log("uploadDeviceSigningKeys advertised no flows!");
      }

      const canUploadKeysWithPasswordOnly = error.data.flows.some(f => {
        return f.stages.length === 1 && f.stages[0] === 'm.login.password';
      });
      this.setState({
        canUploadKeysWithPasswordOnly
      });
    }
  }

  _passPhraseIsValid() {
    return this.state.zxcvbnResult && this.state.zxcvbnResult.score >= PASSWORD_MIN_SCORE;
  }

  _renderPhaseMigrate() {
    // TODO: This is a temporary screen so people who have the labs flag turned on and
    // click the button are aware they're making a change to their account.
    // Once we're confident enough in this (and it's supported enough) we can do
    // it automatically.
    // https://github.com/vector-im/riot-web/issues/11696
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const Field = sdk.getComponent('views.elements.Field');
    let authPrompt;
    let nextCaption = (0, _languageHandler._t)("Next");

    if (this.state.canUploadKeysWithPasswordOnly) {
      authPrompt = _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)("Enter your account password to confirm the upgrade:")), _react.default.createElement("div", null, _react.default.createElement(Field, {
        type: "password",
        label: (0, _languageHandler._t)("Password"),
        value: this.state.accountPassword,
        onChange: this._onAccountPasswordChange,
        flagInvalid: this.state.accountPasswordCorrect === false,
        autoFocus: true
      })));
    } else if (!this.state.backupSigStatus.usable) {
      authPrompt = _react.default.createElement("div", null, _react.default.createElement("div", null, (0, _languageHandler._t)("Restore your key backup to upgrade your encryption")));
      nextCaption = (0, _languageHandler._t)("Restore");
    } else {
      authPrompt = _react.default.createElement("p", null, (0, _languageHandler._t)("You'll need to authenticate with the server to confirm the upgrade."));
    }

    return _react.default.createElement("form", {
      onSubmit: this._onMigrateFormSubmit
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Upgrade this session to allow it to verify other sessions, " + "granting them access to encrypted messages and marking them " + "as trusted for other users.")), _react.default.createElement("div", null, authPrompt), _react.default.createElement(DialogButtons, {
      primaryButton: nextCaption,
      onPrimaryButtonClick: this._onMigrateFormSubmit,
      hasCancel: false,
      primaryDisabled: this.state.canUploadKeysWithPasswordOnly && !this.state.accountPassword
    }, _react.default.createElement("button", {
      type: "button",
      className: "danger",
      onClick: this._onSkipSetupClick
    }, (0, _languageHandler._t)('Skip'))));
  }

  _renderPhasePassPhrase() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    const Field = sdk.getComponent('views.elements.Field');
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const LabelledToggleSwitch = sdk.getComponent('views.elements.LabelledToggleSwitch');
    let strengthMeter;
    let helpText;

    if (this.state.zxcvbnResult) {
      if (this.state.zxcvbnResult.score >= PASSWORD_MIN_SCORE) {
        helpText = (0, _languageHandler._t)("Great! This passphrase looks strong enough.");
      } else {
        // We take the warning from zxcvbn or failing that, the first
        // suggestion. In practice The first is generally the most relevant
        // and it's probably better to present the user with one thing to
        // improve about their password than a whole collection - it can
        // spit out a warning and multiple suggestions which starts getting
        // very information-dense.
        const suggestion = this.state.zxcvbnResult.feedback.warning || this.state.zxcvbnResult.feedback.suggestions[0];

        const suggestionBlock = _react.default.createElement("div", null, suggestion || (0, _languageHandler._t)("Keep going..."));

        helpText = _react.default.createElement("div", null, suggestionBlock);
      }

      strengthMeter = _react.default.createElement("div", null, _react.default.createElement("progress", {
        max: PASSWORD_MIN_SCORE,
        value: this.state.zxcvbnResult.score
      }));
    }

    return _react.default.createElement("form", {
      onSubmit: this._onPassPhraseNextClick
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Set up encryption on this session to allow it to verify other sessions, " + "granting them access to encrypted messages and marking them as trusted for other users.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Secure your encryption keys with a passphrase. For maximum security " + "this should be different to your account password:")), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_passPhraseContainer"
    }, _react.default.createElement(Field, {
      type: "password",
      className: "mx_CreateSecretStorageDialog_passPhraseField",
      onChange: this._onPassPhraseChange,
      value: this.state.passPhrase,
      label: (0, _languageHandler._t)("Enter a passphrase"),
      autoFocus: true,
      autoComplete: "new-password"
    }), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_passPhraseHelp"
    }, strengthMeter, helpText)), _react.default.createElement(LabelledToggleSwitch, {
      label: (0, _languageHandler._t)("Back up my encryption keys, securing them with the same passphrase"),
      onChange: this._onUseKeyBackupChange,
      value: this.state.useKeyBackup
    }), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Continue'),
      onPrimaryButtonClick: this._onPassPhraseNextClick,
      hasCancel: false,
      disabled: !this._passPhraseIsValid()
    }, _react.default.createElement("button", {
      type: "button",
      onClick: this._onSkipSetupClick,
      className: "danger"
    }, (0, _languageHandler._t)("Skip"))), _react.default.createElement("details", null, _react.default.createElement("summary", null, (0, _languageHandler._t)("Advanced")), _react.default.createElement(AccessibleButton, {
      kind: "primary",
      onClick: this._onSkipPassPhraseClick
    }, (0, _languageHandler._t)("Set up with a recovery key"))));
  }

  _renderPhasePassPhraseConfirm() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    const Field = sdk.getComponent('views.elements.Field');
    let matchText;

    if (this.state.passPhraseConfirm === this.state.passPhrase) {
      matchText = (0, _languageHandler._t)("That matches!");
    } else if (!this.state.passPhrase.startsWith(this.state.passPhraseConfirm)) {
      // only tell them they're wrong if they've actually gone wrong.
      // Security concious readers will note that if you left riot-web unattended
      // on this screen, this would make it easy for a malicious person to guess
      // your passphrase one letter at a time, but they could get this faster by
      // just opening the browser's developer tools and reading it.
      // Note that not having typed anything at all will not hit this clause and
      // fall through so empty box === no hint.
      matchText = (0, _languageHandler._t)("That doesn't match.");
    }

    let passPhraseMatch = null;

    if (matchText) {
      passPhraseMatch = _react.default.createElement("div", null, _react.default.createElement("div", null, matchText), _react.default.createElement("div", null, _react.default.createElement(AccessibleButton, {
        element: "span",
        className: "mx_linkButton",
        onClick: this._onSetAgainClick
      }, (0, _languageHandler._t)("Go back to set it again."))));
    }

    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("form", {
      onSubmit: this._onPassPhraseConfirmNextClick
    }, _react.default.createElement("p", null, (0, _languageHandler._t)("Enter your passphrase a second time to confirm it.")), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_passPhraseContainer"
    }, _react.default.createElement(Field, {
      type: "password",
      onChange: this._onPassPhraseConfirmChange,
      value: this.state.passPhraseConfirm,
      className: "mx_CreateSecretStorageDialog_passPhraseField",
      label: (0, _languageHandler._t)("Confirm your passphrase"),
      autoFocus: true,
      autoComplete: "new-password"
    }), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_passPhraseMatch"
    }, passPhraseMatch)), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Continue'),
      onPrimaryButtonClick: this._onPassPhraseConfirmNextClick,
      hasCancel: false,
      disabled: this.state.passPhrase !== this.state.passPhraseConfirm
    }, _react.default.createElement("button", {
      type: "button",
      onClick: this._onSkipSetupClick,
      className: "danger"
    }, (0, _languageHandler._t)("Skip"))));
  }

  _renderPhaseShowKey() {
    const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Your recovery key is a safety net - you can use it to restore " + "access to your encrypted messages if you forget your passphrase.")), _react.default.createElement("p", null, (0, _languageHandler._t)("Keep a copy of it somewhere secure, like a password manager or even a safe.")), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_primaryContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_recoveryKeyHeader"
    }, (0, _languageHandler._t)("Your recovery key")), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_recoveryKeyContainer"
    }, _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_recoveryKey"
    }, _react.default.createElement("code", {
      ref: this._collectRecoveryKeyNode
    }, this._recoveryKey.encodedPrivateKey)), _react.default.createElement("div", {
      className: "mx_CreateSecretStorageDialog_recoveryKeyButtons"
    }, _react.default.createElement(AccessibleButton, {
      kind: "primary",
      className: "mx_Dialog_primary",
      onClick: this._onCopyClick
    }, (0, _languageHandler._t)("Copy")), _react.default.createElement(AccessibleButton, {
      kind: "primary",
      className: "mx_Dialog_primary",
      onClick: this._onDownloadClick
    }, (0, _languageHandler._t)("Download"))))));
  }

  _renderPhaseKeepItSafe() {
    let introText;

    if (this.state.copied) {
      introText = (0, _languageHandler._t)("Your recovery key has been <b>copied to your clipboard</b>, paste it to:", {}, {
        b: s => _react.default.createElement("b", null, s)
      });
    } else if (this.state.downloaded) {
      introText = (0, _languageHandler._t)("Your recovery key is in your <b>Downloads</b> folder.", {}, {
        b: s => _react.default.createElement("b", null, s)
      });
    }

    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, introText, _react.default.createElement("ul", null, _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Print it</b> and store it somewhere safe", {}, {
      b: s => _react.default.createElement("b", null, s)
    })), _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Save it</b> on a USB key or backup drive", {}, {
      b: s => _react.default.createElement("b", null, s)
    })), _react.default.createElement("li", null, (0, _languageHandler._t)("<b>Copy it</b> to your personal cloud storage", {}, {
      b: s => _react.default.createElement("b", null, s)
    }))), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)("Continue"),
      onPrimaryButtonClick: this._bootstrapSecretStorage,
      hasCancel: false
    }, _react.default.createElement("button", {
      onClick: this._onKeepItSafeBackClick
    }, (0, _languageHandler._t)("Back"))));
  }

  _renderBusyPhase() {
    const Spinner = sdk.getComponent('views.elements.Spinner');
    return _react.default.createElement("div", null, _react.default.createElement(Spinner, null));
  }

  _renderPhaseDone() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("You can now verify your other devices, " + "and other users to keep your chats safe.")), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('OK'),
      onPrimaryButtonClick: this._onDone,
      hasCancel: false
    }));
  }

  _renderPhaseSkipConfirm() {
    const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
    return _react.default.createElement("div", null, (0, _languageHandler._t)("Without completing security on this session, it won’t have " + "access to encrypted messages."), _react.default.createElement(DialogButtons, {
      primaryButton: (0, _languageHandler._t)('Go back'),
      onPrimaryButtonClick: this._onSetUpClick,
      hasCancel: false
    }, _react.default.createElement("button", {
      type: "button",
      className: "danger",
      onClick: this._onCancel
    }, (0, _languageHandler._t)('Skip'))));
  }

  _titleForPhase(phase) {
    switch (phase) {
      case PHASE_MIGRATE:
        return (0, _languageHandler._t)('Upgrade your encryption');

      case PHASE_PASSPHRASE:
        return (0, _languageHandler._t)('Set up encryption');

      case PHASE_PASSPHRASE_CONFIRM:
        return (0, _languageHandler._t)('Confirm passphrase');

      case PHASE_CONFIRM_SKIP:
        return (0, _languageHandler._t)('Are you sure?');

      case PHASE_SHOWKEY:
      case PHASE_KEEPITSAFE:
        return (0, _languageHandler._t)('Make a copy of your recovery key');

      case PHASE_STORING:
        return (0, _languageHandler._t)('Setting up keys');

      case PHASE_DONE:
        return (0, _languageHandler._t)("You're done!");

      default:
        return '';
    }
  }

  render() {
    const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
    let content;

    if (this.state.error) {
      const DialogButtons = sdk.getComponent('views.elements.DialogButtons');
      content = _react.default.createElement("div", null, _react.default.createElement("p", null, (0, _languageHandler._t)("Unable to set up secret storage")), _react.default.createElement("div", {
        className: "mx_Dialog_buttons"
      }, _react.default.createElement(DialogButtons, {
        primaryButton: (0, _languageHandler._t)('Retry'),
        onPrimaryButtonClick: this._bootstrapSecretStorage,
        hasCancel: true,
        onCancel: this._onCancel
      })));
    } else {
      switch (this.state.phase) {
        case PHASE_LOADING:
          content = this._renderBusyPhase();
          break;

        case PHASE_MIGRATE:
          content = this._renderPhaseMigrate();
          break;

        case PHASE_PASSPHRASE:
          content = this._renderPhasePassPhrase();
          break;

        case PHASE_PASSPHRASE_CONFIRM:
          content = this._renderPhasePassPhraseConfirm();
          break;

        case PHASE_SHOWKEY:
          content = this._renderPhaseShowKey();
          break;

        case PHASE_KEEPITSAFE:
          content = this._renderPhaseKeepItSafe();
          break;

        case PHASE_STORING:
          content = this._renderBusyPhase();
          break;

        case PHASE_DONE:
          content = this._renderPhaseDone();
          break;

        case PHASE_CONFIRM_SKIP:
          content = this._renderPhaseSkipConfirm();
          break;
      }
    }

    let headerImage;

    if (this._titleForPhase(this.state.phase)) {
      headerImage = require("../../../../../res/img/e2e/normal.svg");
    }

    return _react.default.createElement(BaseDialog, {
      className: "mx_CreateSecretStorageDialog",
      onFinished: this.props.onFinished,
      title: this._titleForPhase(this.state.phase),
      headerImage: headerImage,
      hasCancel: this.props.hasCancel && [PHASE_PASSPHRASE].includes(this.state.phase),
      fixedWidth: false
    }, _react.default.createElement("div", null, content));
  }

}

exports.default = CreateSecretStorageDialog;
(0, _defineProperty2.default)(CreateSecretStorageDialog, "propTypes", {
  hasCancel: _propTypes.default.bool,
  accountPassword: _propTypes.default.string,
  force: _propTypes.default.bool
});
(0, _defineProperty2.default)(CreateSecretStorageDialog, "defaultProps", {
  hasCancel: true,
  force: false
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9hc3luYy1jb21wb25lbnRzL3ZpZXdzL2RpYWxvZ3Mvc2VjcmV0c3RvcmFnZS9DcmVhdGVTZWNyZXRTdG9yYWdlRGlhbG9nLmpzIl0sIm5hbWVzIjpbIlBIQVNFX0xPQURJTkciLCJQSEFTRV9NSUdSQVRFIiwiUEhBU0VfUEFTU1BIUkFTRSIsIlBIQVNFX1BBU1NQSFJBU0VfQ09ORklSTSIsIlBIQVNFX1NIT1dLRVkiLCJQSEFTRV9LRUVQSVRTQUZFIiwiUEhBU0VfU1RPUklORyIsIlBIQVNFX0RPTkUiLCJQSEFTRV9DT05GSVJNX1NLSVAiLCJQQVNTV09SRF9NSU5fU0NPUkUiLCJQQVNTUEhSQVNFX0ZFRURCQUNLX0RFTEFZIiwic2VsZWN0VGV4dCIsInRhcmdldCIsInJhbmdlIiwiZG9jdW1lbnQiLCJjcmVhdGVSYW5nZSIsInNlbGVjdE5vZGVDb250ZW50cyIsInNlbGVjdGlvbiIsIndpbmRvdyIsImdldFNlbGVjdGlvbiIsInJlbW92ZUFsbFJhbmdlcyIsImFkZFJhbmdlIiwiQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZyIsIlJlYWN0IiwiUHVyZUNvbXBvbmVudCIsImNvbnN0cnVjdG9yIiwicHJvcHMiLCJzdGF0ZSIsInBoYXNlIiwiX2ZldGNoQmFja3VwSW5mbyIsIm4iLCJfcmVjb3ZlcnlLZXlOb2RlIiwiZW5hYmxlZCIsInNldFN0YXRlIiwidXNlS2V5QmFja3VwIiwiZSIsInByZXZlbnREZWZhdWx0IiwiYmFja3VwU2lnU3RhdHVzIiwidXNhYmxlIiwiX2Jvb3RzdHJhcFNlY3JldFN0b3JhZ2UiLCJfcmVzdG9yZUJhY2t1cCIsInN1Y2Nlc3NmdWwiLCJleGVjQ29tbWFuZCIsImNvcGllZCIsImJsb2IiLCJCbG9iIiwiX3JlY292ZXJ5S2V5IiwiZW5jb2RlZFByaXZhdGVLZXkiLCJ0eXBlIiwiRmlsZVNhdmVyIiwic2F2ZUFzIiwiZG93bmxvYWRlZCIsIm1ha2VSZXF1ZXN0IiwiY2FuVXBsb2FkS2V5c1dpdGhQYXNzd29yZE9ubHkiLCJhY2NvdW50UGFzc3dvcmQiLCJpZGVudGlmaWVyIiwidXNlciIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFVzZXJJZCIsInBhc3N3b3JkIiwiSW50ZXJhY3RpdmVBdXRoRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiZmluaXNoZWQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsIm1hdHJpeENsaWVudCIsImNvbmZpcm1lZCIsIkVycm9yIiwiZXJyb3IiLCJjbGkiLCJmb3JjZSIsImJvb3RzdHJhcFNlY3JldFN0b3JhZ2UiLCJhdXRoVXBsb2FkRGV2aWNlU2lnbmluZ0tleXMiLCJfZG9Cb290c3RyYXBVSUF1dGgiLCJjcmVhdGVTZWNyZXRTdG9yYWdlS2V5Iiwic2V0dXBOZXdLZXlCYWNrdXAiLCJzZXR1cE5ld1NlY3JldFN0b3JhZ2UiLCJrZXlCYWNrdXBJbmZvIiwiYmFja3VwSW5mbyIsImdldEtleUJhY2t1cFBhc3NwaHJhc2UiLCJwcm9tcHRGb3JCYWNrdXBQYXNzcGhyYXNlIiwiaHR0cFN0YXR1cyIsImRhdGEiLCJmbG93cyIsImFjY291bnRQYXNzd29yZENvcnJlY3QiLCJjb25zb2xlIiwib25GaW5pc2hlZCIsIlJlc3RvcmVLZXlCYWNrdXBEaWFsb2ciLCJzaG93U3VtbWFyeSIsImNyZWF0ZVJlY292ZXJ5S2V5RnJvbVBhc3NwaHJhc2UiLCJfc2V0WnhjdmJuUmVzdWx0VGltZW91dCIsImNsZWFyVGltZW91dCIsIlByb21pc2UiLCJyZXNvbHZlIiwienhjdmJuUmVzdWx0IiwicGFzc1BocmFzZSIsIl9wYXNzUGhyYXNlSXNWYWxpZCIsInBhc3NQaHJhc2VDb25maXJtIiwidmFsdWUiLCJzZXRUaW1lb3V0IiwiX3F1ZXJ5S2V5VXBsb2FkQXV0aCIsIm9uIiwiX29uS2V5QmFja3VwU3RhdHVzQ2hhbmdlIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJyZW1vdmVMaXN0ZW5lciIsImdldEtleUJhY2t1cFZlcnNpb24iLCJpc0NyeXB0b0VuYWJsZWQiLCJpc0tleUJhY2t1cFRydXN0ZWQiLCJ1cGxvYWREZXZpY2VTaWduaW5nS2V5cyIsImxvZyIsInNvbWUiLCJmIiwic3RhZ2VzIiwibGVuZ3RoIiwic2NvcmUiLCJfcmVuZGVyUGhhc2VNaWdyYXRlIiwiRGlhbG9nQnV0dG9ucyIsIkZpZWxkIiwiYXV0aFByb21wdCIsIm5leHRDYXB0aW9uIiwiX29uQWNjb3VudFBhc3N3b3JkQ2hhbmdlIiwiX29uTWlncmF0ZUZvcm1TdWJtaXQiLCJfb25Ta2lwU2V0dXBDbGljayIsIl9yZW5kZXJQaGFzZVBhc3NQaHJhc2UiLCJBY2Nlc3NpYmxlQnV0dG9uIiwiTGFiZWxsZWRUb2dnbGVTd2l0Y2giLCJzdHJlbmd0aE1ldGVyIiwiaGVscFRleHQiLCJzdWdnZXN0aW9uIiwiZmVlZGJhY2siLCJ3YXJuaW5nIiwic3VnZ2VzdGlvbnMiLCJzdWdnZXN0aW9uQmxvY2siLCJfb25QYXNzUGhyYXNlTmV4dENsaWNrIiwiX29uUGFzc1BocmFzZUNoYW5nZSIsIl9vblVzZUtleUJhY2t1cENoYW5nZSIsIl9vblNraXBQYXNzUGhyYXNlQ2xpY2siLCJfcmVuZGVyUGhhc2VQYXNzUGhyYXNlQ29uZmlybSIsIm1hdGNoVGV4dCIsInN0YXJ0c1dpdGgiLCJwYXNzUGhyYXNlTWF0Y2giLCJfb25TZXRBZ2FpbkNsaWNrIiwiX29uUGFzc1BocmFzZUNvbmZpcm1OZXh0Q2xpY2siLCJfb25QYXNzUGhyYXNlQ29uZmlybUNoYW5nZSIsIl9yZW5kZXJQaGFzZVNob3dLZXkiLCJfY29sbGVjdFJlY292ZXJ5S2V5Tm9kZSIsIl9vbkNvcHlDbGljayIsIl9vbkRvd25sb2FkQ2xpY2siLCJfcmVuZGVyUGhhc2VLZWVwSXRTYWZlIiwiaW50cm9UZXh0IiwiYiIsInMiLCJfb25LZWVwSXRTYWZlQmFja0NsaWNrIiwiX3JlbmRlckJ1c3lQaGFzZSIsIlNwaW5uZXIiLCJfcmVuZGVyUGhhc2VEb25lIiwiX29uRG9uZSIsIl9yZW5kZXJQaGFzZVNraXBDb25maXJtIiwiX29uU2V0VXBDbGljayIsIl9vbkNhbmNlbCIsIl90aXRsZUZvclBoYXNlIiwicmVuZGVyIiwiQmFzZURpYWxvZyIsImNvbnRlbnQiLCJoZWFkZXJJbWFnZSIsInJlcXVpcmUiLCJoYXNDYW5jZWwiLCJpbmNsdWRlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBMkJBLE1BQU1BLGFBQWEsR0FBRyxDQUF0QjtBQUNBLE1BQU1DLGFBQWEsR0FBRyxDQUF0QjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLENBQXpCO0FBQ0EsTUFBTUMsd0JBQXdCLEdBQUcsQ0FBakM7QUFDQSxNQUFNQyxhQUFhLEdBQUcsQ0FBdEI7QUFDQSxNQUFNQyxnQkFBZ0IsR0FBRyxDQUF6QjtBQUNBLE1BQU1DLGFBQWEsR0FBRyxDQUF0QjtBQUNBLE1BQU1DLFVBQVUsR0FBRyxDQUFuQjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHLENBQTNCO0FBRUEsTUFBTUMsa0JBQWtCLEdBQUcsQ0FBM0IsQyxDQUE4Qjs7QUFDOUIsTUFBTUMseUJBQXlCLEdBQUcsR0FBbEMsQyxDQUF1QztBQUV2Qzs7QUFDQSxTQUFTQyxVQUFULENBQW9CQyxNQUFwQixFQUE0QjtBQUN4QixRQUFNQyxLQUFLLEdBQUdDLFFBQVEsQ0FBQ0MsV0FBVCxFQUFkO0FBQ0FGLEVBQUFBLEtBQUssQ0FBQ0csa0JBQU4sQ0FBeUJKLE1BQXpCO0FBRUEsUUFBTUssU0FBUyxHQUFHQyxNQUFNLENBQUNDLFlBQVAsRUFBbEI7QUFDQUYsRUFBQUEsU0FBUyxDQUFDRyxlQUFWO0FBQ0FILEVBQUFBLFNBQVMsQ0FBQ0ksUUFBVixDQUFtQlIsS0FBbkI7QUFDSDtBQUVEOzs7Ozs7QUFJZSxNQUFNUyx5QkFBTixTQUF3Q0MsZUFBTUMsYUFBOUMsQ0FBNEQ7QUFZdkVDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2YsVUFBTUEsS0FBTjtBQURlLG9FQWdGUSxNQUFNO0FBQzdCLFVBQUksS0FBS0MsS0FBTCxDQUFXQyxLQUFYLEtBQXFCM0IsYUFBekIsRUFBd0MsS0FBSzRCLGdCQUFMO0FBQzNDLEtBbEZrQjtBQUFBLG1FQW9GUUMsQ0FBRCxJQUFPO0FBQzdCLFdBQUtDLGdCQUFMLEdBQXdCRCxDQUF4QjtBQUNILEtBdEZrQjtBQUFBLGlFQXdGTUUsT0FBRCxJQUFhO0FBQ2pDLFdBQUtDLFFBQUwsQ0FBYztBQUNWQyxRQUFBQSxZQUFZLEVBQUVGO0FBREosT0FBZDtBQUdILEtBNUZrQjtBQUFBLGdFQThGS0csQ0FBRCxJQUFPO0FBQzFCQSxNQUFBQSxDQUFDLENBQUNDLGNBQUY7O0FBQ0EsVUFBSSxLQUFLVCxLQUFMLENBQVdVLGVBQVgsQ0FBMkJDLE1BQS9CLEVBQXVDO0FBQ25DLGFBQUtDLHVCQUFMO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS0MsY0FBTDtBQUNIO0FBQ0osS0FyR2tCO0FBQUEsd0RBdUdKLE1BQU07QUFDakI3QixNQUFBQSxVQUFVLENBQUMsS0FBS29CLGdCQUFOLENBQVY7QUFDQSxZQUFNVSxVQUFVLEdBQUczQixRQUFRLENBQUM0QixXQUFULENBQXFCLE1BQXJCLENBQW5COztBQUNBLFVBQUlELFVBQUosRUFBZ0I7QUFDWixhQUFLUixRQUFMLENBQWM7QUFDVlUsVUFBQUEsTUFBTSxFQUFFLElBREU7QUFFVmYsVUFBQUEsS0FBSyxFQUFFdkI7QUFGRyxTQUFkO0FBSUg7QUFDSixLQWhIa0I7QUFBQSw0REFrSEEsTUFBTTtBQUNyQixZQUFNdUMsSUFBSSxHQUFHLElBQUlDLElBQUosQ0FBUyxDQUFDLEtBQUtDLFlBQUwsQ0FBa0JDLGlCQUFuQixDQUFULEVBQWdEO0FBQ3pEQyxRQUFBQSxJQUFJLEVBQUU7QUFEbUQsT0FBaEQsQ0FBYjs7QUFHQUMseUJBQVVDLE1BQVYsQ0FBaUJOLElBQWpCLEVBQXVCLGtCQUF2Qjs7QUFFQSxXQUFLWCxRQUFMLENBQWM7QUFDVmtCLFFBQUFBLFVBQVUsRUFBRSxJQURGO0FBRVZ2QixRQUFBQSxLQUFLLEVBQUV2QjtBQUZHLE9BQWQ7QUFJSCxLQTVIa0I7QUFBQSw4REE4SEUsTUFBTytDLFdBQVAsSUFBdUI7QUFDeEMsVUFBSSxLQUFLekIsS0FBTCxDQUFXMEIsNkJBQVgsSUFBNEMsS0FBSzFCLEtBQUwsQ0FBVzJCLGVBQTNELEVBQTRFO0FBQ3hFLGNBQU1GLFdBQVcsQ0FBQztBQUNkSixVQUFBQSxJQUFJLEVBQUUsa0JBRFE7QUFFZE8sVUFBQUEsVUFBVSxFQUFFO0FBQ1JQLFlBQUFBLElBQUksRUFBRSxXQURFO0FBRVJRLFlBQUFBLElBQUksRUFBRUMsaUNBQWdCQyxHQUFoQixHQUFzQkMsU0FBdEI7QUFGRSxXQUZFO0FBTWQ7QUFDQUgsVUFBQUEsSUFBSSxFQUFFQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxTQUF0QixFQVBRO0FBUWRDLFVBQUFBLFFBQVEsRUFBRSxLQUFLakMsS0FBTCxDQUFXMkI7QUFSUCxTQUFELENBQWpCO0FBVUgsT0FYRCxNQVdPO0FBQ0gsY0FBTU8scUJBQXFCLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwrQkFBakIsQ0FBOUI7O0FBQ0EsY0FBTTtBQUFFQyxVQUFBQTtBQUFGLFlBQWVDLGVBQU1DLG1CQUFOLENBQ2pCLDJCQURpQixFQUNZLEVBRFosRUFDZ0JMLHFCQURoQixFQUVqQjtBQUNJTSxVQUFBQSxLQUFLLEVBQUUseUJBQUcsaUJBQUgsQ0FEWDtBQUVJQyxVQUFBQSxZQUFZLEVBQUVYLGlDQUFnQkMsR0FBaEIsRUFGbEI7QUFHSU4sVUFBQUE7QUFISixTQUZpQixDQUFyQjs7QUFRQSxjQUFNLENBQUNpQixTQUFELElBQWMsTUFBTUwsUUFBMUI7O0FBQ0EsWUFBSSxDQUFDSyxTQUFMLEVBQWdCO0FBQ1osZ0JBQU0sSUFBSUMsS0FBSixDQUFVLHdDQUFWLENBQU47QUFDSDtBQUNKO0FBQ0osS0F6SmtCO0FBQUEsbUVBMkpPLFlBQVk7QUFDbEMsV0FBS3JDLFFBQUwsQ0FBYztBQUNWTCxRQUFBQSxLQUFLLEVBQUV0QixhQURHO0FBRVZpRSxRQUFBQSxLQUFLLEVBQUU7QUFGRyxPQUFkOztBQUtBLFlBQU1DLEdBQUcsR0FBR2YsaUNBQWdCQyxHQUFoQixFQUFaOztBQUVBLFlBQU07QUFBRWUsUUFBQUE7QUFBRixVQUFZLEtBQUsvQyxLQUF2Qjs7QUFFQSxVQUFJO0FBQ0EsWUFBSStDLEtBQUosRUFBVztBQUNQLGdCQUFNRCxHQUFHLENBQUNFLHNCQUFKLENBQTJCO0FBQzdCQyxZQUFBQSwyQkFBMkIsRUFBRSxLQUFLQyxrQkFETDtBQUU3QkMsWUFBQUEsc0JBQXNCLEVBQUUsWUFBWSxLQUFLL0IsWUFGWjtBQUc3QmdDLFlBQUFBLGlCQUFpQixFQUFFLElBSFU7QUFJN0JDLFlBQUFBLHFCQUFxQixFQUFFO0FBSk0sV0FBM0IsQ0FBTjtBQU1ILFNBUEQsTUFPTztBQUNILGdCQUFNUCxHQUFHLENBQUNFLHNCQUFKLENBQTJCO0FBQzdCQyxZQUFBQSwyQkFBMkIsRUFBRSxLQUFLQyxrQkFETDtBQUU3QkMsWUFBQUEsc0JBQXNCLEVBQUUsWUFBWSxLQUFLL0IsWUFGWjtBQUc3QmtDLFlBQUFBLGFBQWEsRUFBRSxLQUFLckQsS0FBTCxDQUFXc0QsVUFIRztBQUk3QkgsWUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxLQUFLbkQsS0FBTCxDQUFXc0QsVUFBWixJQUEwQixLQUFLdEQsS0FBTCxDQUFXTyxZQUozQjtBQUs3QmdELFlBQUFBLHNCQUFzQixFQUFFQztBQUxLLFdBQTNCLENBQU47QUFPSDs7QUFDRCxhQUFLbEQsUUFBTCxDQUFjO0FBQ1ZMLFVBQUFBLEtBQUssRUFBRXJCO0FBREcsU0FBZDtBQUdILE9BcEJELENBb0JFLE9BQU80QixDQUFQLEVBQVU7QUFDUixZQUFJLEtBQUtSLEtBQUwsQ0FBVzBCLDZCQUFYLElBQTRDbEIsQ0FBQyxDQUFDaUQsVUFBRixLQUFpQixHQUE3RCxJQUFvRWpELENBQUMsQ0FBQ2tELElBQUYsQ0FBT0MsS0FBL0UsRUFBc0Y7QUFDbEYsZUFBS3JELFFBQUwsQ0FBYztBQUNWcUIsWUFBQUEsZUFBZSxFQUFFLEVBRFA7QUFFVmlDLFlBQUFBLHNCQUFzQixFQUFFLEtBRmQ7QUFHVjNELFlBQUFBLEtBQUssRUFBRTNCO0FBSEcsV0FBZDtBQUtILFNBTkQsTUFNTztBQUNILGVBQUtnQyxRQUFMLENBQWM7QUFBRXNDLFlBQUFBLEtBQUssRUFBRXBDO0FBQVQsV0FBZDtBQUNIOztBQUNEcUQsUUFBQUEsT0FBTyxDQUFDakIsS0FBUixDQUFjLG9DQUFkLEVBQW9EcEMsQ0FBcEQ7QUFDSDtBQUNKLEtBck1rQjtBQUFBLHFEQXVNUCxNQUFNO0FBQ2QsV0FBS1QsS0FBTCxDQUFXK0QsVUFBWCxDQUFzQixLQUF0QjtBQUNILEtBek1rQjtBQUFBLG1EQTJNVCxNQUFNO0FBQ1osV0FBSy9ELEtBQUwsQ0FBVytELFVBQVgsQ0FBc0IsSUFBdEI7QUFDSCxLQTdNa0I7QUFBQSwwREErTUYsWUFBWTtBQUN6QixZQUFNQyxzQkFBc0IsR0FBRzVCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQ0FBakIsQ0FBL0I7O0FBQ0EsWUFBTTtBQUFFQyxRQUFBQTtBQUFGLFVBQWVDLGVBQU1DLG1CQUFOLENBQ2pCLGdCQURpQixFQUNDLEVBREQsRUFDS3dCLHNCQURMLEVBQzZCO0FBQUNDLFFBQUFBLFdBQVcsRUFBRTtBQUFkLE9BRDdCLEVBQ21ELElBRG5EO0FBRWpCO0FBQWlCLFdBRkE7QUFFTztBQUFlLFdBRnRCLENBQXJCOztBQUtBLFlBQU0zQixRQUFOO0FBQ0EsWUFBTTtBQUFFM0IsUUFBQUE7QUFBRixVQUFzQixNQUFNLEtBQUtSLGdCQUFMLEVBQWxDOztBQUNBLFVBQ0lRLGVBQWUsQ0FBQ0MsTUFBaEIsSUFDQSxLQUFLWCxLQUFMLENBQVcwQiw2QkFEWCxJQUVBLEtBQUsxQixLQUFMLENBQVcyQixlQUhmLEVBSUU7QUFDRSxhQUFLZix1QkFBTDtBQUNIO0FBQ0osS0EvTmtCO0FBQUEsNkRBaU9DLE1BQU07QUFDdEIsV0FBS04sUUFBTCxDQUFjO0FBQUNMLFFBQUFBLEtBQUssRUFBRXBCO0FBQVIsT0FBZDtBQUNILEtBbk9rQjtBQUFBLHlEQXFPSCxNQUFNO0FBQ2xCLFdBQUt5QixRQUFMLENBQWM7QUFBQ0wsUUFBQUEsS0FBSyxFQUFFMUI7QUFBUixPQUFkO0FBQ0gsS0F2T2tCO0FBQUEsa0VBeU9NLFlBQVk7QUFDakMsV0FBSzRDLFlBQUwsR0FDSSxNQUFNVyxpQ0FBZ0JDLEdBQWhCLEdBQXNCa0MsK0JBQXRCLEVBRFY7QUFFQSxXQUFLM0QsUUFBTCxDQUFjO0FBQ1ZVLFFBQUFBLE1BQU0sRUFBRSxLQURFO0FBRVZRLFFBQUFBLFVBQVUsRUFBRSxLQUZGO0FBR1Z2QixRQUFBQSxLQUFLLEVBQUV4QjtBQUhHLE9BQWQ7QUFLSCxLQWpQa0I7QUFBQSxrRUFtUE0sTUFBTytCLENBQVAsSUFBYTtBQUNsQ0EsTUFBQUEsQ0FBQyxDQUFDQyxjQUFGLEdBRGtDLENBR2xDO0FBQ0E7QUFDQTs7QUFDQSxVQUFJLEtBQUt5RCx1QkFBTCxLQUFpQyxJQUFyQyxFQUEyQztBQUN2Q0MsUUFBQUEsWUFBWSxDQUFDLEtBQUtELHVCQUFOLENBQVo7QUFDQSxhQUFLQSx1QkFBTCxHQUErQixJQUEvQjtBQUNBLGNBQU0sSUFBSUUsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDM0IsZUFBSy9ELFFBQUwsQ0FBYztBQUNWZ0UsWUFBQUEsWUFBWSxFQUFFLG1DQUFjLEtBQUt0RSxLQUFMLENBQVd1RSxVQUF6QjtBQURKLFdBQWQsRUFFR0YsT0FGSDtBQUdILFNBSkssQ0FBTjtBQUtIOztBQUNELFVBQUksS0FBS0csa0JBQUwsRUFBSixFQUErQjtBQUMzQixhQUFLbEUsUUFBTCxDQUFjO0FBQUNMLFVBQUFBLEtBQUssRUFBRXpCO0FBQVIsU0FBZDtBQUNIO0FBQ0osS0FyUWtCO0FBQUEseUVBdVFhLE1BQU9nQyxDQUFQLElBQWE7QUFDekNBLE1BQUFBLENBQUMsQ0FBQ0MsY0FBRjtBQUVBLFVBQUksS0FBS1QsS0FBTCxDQUFXdUUsVUFBWCxLQUEwQixLQUFLdkUsS0FBTCxDQUFXeUUsaUJBQXpDLEVBQTREO0FBRTVELFdBQUt0RCxZQUFMLEdBQ0ksTUFBTVcsaUNBQWdCQyxHQUFoQixHQUFzQmtDLCtCQUF0QixDQUFzRCxLQUFLakUsS0FBTCxDQUFXdUUsVUFBakUsQ0FEVjtBQUVBLFdBQUtqRSxRQUFMLENBQWM7QUFDVlUsUUFBQUEsTUFBTSxFQUFFLEtBREU7QUFFVlEsUUFBQUEsVUFBVSxFQUFFLEtBRkY7QUFHVnZCLFFBQUFBLEtBQUssRUFBRXhCO0FBSEcsT0FBZDtBQUtILEtBblJrQjtBQUFBLDREQXFSQSxNQUFNO0FBQ3JCLFdBQUs2QixRQUFMLENBQWM7QUFDVmlFLFFBQUFBLFVBQVUsRUFBRSxFQURGO0FBRVZFLFFBQUFBLGlCQUFpQixFQUFFLEVBRlQ7QUFHVnhFLFFBQUFBLEtBQUssRUFBRTFCLGdCQUhHO0FBSVYrRixRQUFBQSxZQUFZLEVBQUU7QUFKSixPQUFkO0FBTUgsS0E1UmtCO0FBQUEsa0VBOFJNLE1BQU07QUFDM0IsV0FBS2hFLFFBQUwsQ0FBYztBQUNWTCxRQUFBQSxLQUFLLEVBQUV4QjtBQURHLE9BQWQ7QUFHSCxLQWxTa0I7QUFBQSwrREFvU0krQixDQUFELElBQU87QUFDekIsV0FBS0YsUUFBTCxDQUFjO0FBQ1ZpRSxRQUFBQSxVQUFVLEVBQUUvRCxDQUFDLENBQUN2QixNQUFGLENBQVN5RjtBQURYLE9BQWQ7O0FBSUEsVUFBSSxLQUFLUix1QkFBTCxLQUFpQyxJQUFyQyxFQUEyQztBQUN2Q0MsUUFBQUEsWUFBWSxDQUFDLEtBQUtELHVCQUFOLENBQVo7QUFDSDs7QUFDRCxXQUFLQSx1QkFBTCxHQUErQlMsVUFBVSxDQUFDLE1BQU07QUFDNUMsYUFBS1QsdUJBQUwsR0FBK0IsSUFBL0I7QUFDQSxhQUFLNUQsUUFBTCxDQUFjO0FBQ1Y7QUFDQTtBQUNBO0FBQ0FnRSxVQUFBQSxZQUFZLEVBQUUsbUNBQWMsS0FBS3RFLEtBQUwsQ0FBV3VFLFVBQXpCO0FBSkosU0FBZDtBQU1ILE9BUndDLEVBUXRDeEYseUJBUnNDLENBQXpDO0FBU0gsS0FyVGtCO0FBQUEsc0VBdVRXeUIsQ0FBRCxJQUFPO0FBQ2hDLFdBQUtGLFFBQUwsQ0FBYztBQUNWbUUsUUFBQUEsaUJBQWlCLEVBQUVqRSxDQUFDLENBQUN2QixNQUFGLENBQVN5RjtBQURsQixPQUFkO0FBR0gsS0EzVGtCO0FBQUEsb0VBaVVTbEUsQ0FBRCxJQUFPO0FBQzlCLFdBQUtGLFFBQUwsQ0FBYztBQUNWcUIsUUFBQUEsZUFBZSxFQUFFbkIsQ0FBQyxDQUFDdkIsTUFBRixDQUFTeUY7QUFEaEIsT0FBZDtBQUdILEtBclVrQjtBQUdmLFNBQUt2RCxZQUFMLEdBQW9CLElBQXBCO0FBQ0EsU0FBS2YsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxTQUFLOEQsdUJBQUwsR0FBK0IsSUFBL0I7QUFFQSxTQUFLbEUsS0FBTCxHQUFhO0FBQ1RDLE1BQUFBLEtBQUssRUFBRTVCLGFBREU7QUFFVGtHLE1BQUFBLFVBQVUsRUFBRSxFQUZIO0FBR1RFLE1BQUFBLGlCQUFpQixFQUFFLEVBSFY7QUFJVHpELE1BQUFBLE1BQU0sRUFBRSxLQUpDO0FBS1RRLE1BQUFBLFVBQVUsRUFBRSxLQUxIO0FBTVQ4QyxNQUFBQSxZQUFZLEVBQUUsSUFOTDtBQU9UaEIsTUFBQUEsVUFBVSxFQUFFLElBUEg7QUFRVDVDLE1BQUFBLGVBQWUsRUFBRSxJQVJSO0FBU1Q7QUFDQTtBQUNBZ0IsTUFBQUEsNkJBQTZCLEVBQUUsSUFYdEI7QUFZVEMsTUFBQUEsZUFBZSxFQUFFNUIsS0FBSyxDQUFDNEIsZUFBTixJQUF5QixFQVpqQztBQWFUaUMsTUFBQUEsc0JBQXNCLEVBQUUsSUFiZjtBQWNUO0FBQ0FyRCxNQUFBQSxZQUFZLEVBQUU7QUFmTCxLQUFiOztBQWtCQSxTQUFLTCxnQkFBTDs7QUFDQSxTQUFLMEUsbUJBQUw7O0FBRUE5QyxxQ0FBZ0JDLEdBQWhCLEdBQXNCOEMsRUFBdEIsQ0FBeUIsd0JBQXpCLEVBQW1ELEtBQUtDLHdCQUF4RDtBQUNIOztBQUVEQyxFQUFBQSxvQkFBb0IsR0FBRztBQUNuQmpELHFDQUFnQkMsR0FBaEIsR0FBc0JpRCxjQUF0QixDQUFxQyx3QkFBckMsRUFBK0QsS0FBS0Ysd0JBQXBFOztBQUNBLFFBQUksS0FBS1osdUJBQUwsS0FBaUMsSUFBckMsRUFBMkM7QUFDdkNDLE1BQUFBLFlBQVksQ0FBQyxLQUFLRCx1QkFBTixDQUFaO0FBQ0g7QUFDSjs7QUFFRCxRQUFNaEUsZ0JBQU4sR0FBeUI7QUFDckIsVUFBTW9ELFVBQVUsR0FBRyxNQUFNeEIsaUNBQWdCQyxHQUFoQixHQUFzQmtELG1CQUF0QixFQUF6QjtBQUNBLFVBQU12RSxlQUFlLEdBQ2pCO0FBQ0FvQixxQ0FBZ0JDLEdBQWhCLEdBQXNCbUQsZUFBdEIsT0FBMkMsTUFBTXBELGlDQUFnQkMsR0FBaEIsR0FBc0JvRCxrQkFBdEIsQ0FBeUM3QixVQUF6QyxDQUFqRCxDQUZKO0FBS0EsVUFBTTtBQUFFUixNQUFBQTtBQUFGLFFBQVksS0FBSy9DLEtBQXZCO0FBQ0EsVUFBTUUsS0FBSyxHQUFJcUQsVUFBVSxJQUFJLENBQUNSLEtBQWhCLEdBQXlCeEUsYUFBekIsR0FBeUNDLGdCQUF2RDtBQUVBLFNBQUsrQixRQUFMLENBQWM7QUFDVkwsTUFBQUEsS0FEVTtBQUVWcUQsTUFBQUEsVUFGVTtBQUdWNUMsTUFBQUE7QUFIVSxLQUFkO0FBTUEsV0FBTztBQUNINEMsTUFBQUEsVUFERztBQUVINUMsTUFBQUE7QUFGRyxLQUFQO0FBSUg7O0FBRUQsUUFBTWtFLG1CQUFOLEdBQTRCO0FBQ3hCLFFBQUk7QUFDQSxZQUFNOUMsaUNBQWdCQyxHQUFoQixHQUFzQnFELHVCQUF0QixDQUE4QyxJQUE5QyxFQUFvRCxFQUFwRCxDQUFOLENBREEsQ0FFQTtBQUNBO0FBQ0E7O0FBQ0F2QixNQUFBQSxPQUFPLENBQUN3QixHQUFSLENBQVksaUVBQVo7QUFDSCxLQU5ELENBTUUsT0FBT3pDLEtBQVAsRUFBYztBQUNaLFVBQUksQ0FBQ0EsS0FBSyxDQUFDYyxJQUFOLENBQVdDLEtBQWhCLEVBQXVCO0FBQ25CRSxRQUFBQSxPQUFPLENBQUN3QixHQUFSLENBQVksOENBQVo7QUFDSDs7QUFDRCxZQUFNM0QsNkJBQTZCLEdBQUdrQixLQUFLLENBQUNjLElBQU4sQ0FBV0MsS0FBWCxDQUFpQjJCLElBQWpCLENBQXNCQyxDQUFDLElBQUk7QUFDN0QsZUFBT0EsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLE1BQVQsS0FBb0IsQ0FBcEIsSUFBeUJGLENBQUMsQ0FBQ0MsTUFBRixDQUFTLENBQVQsTUFBZ0Isa0JBQWhEO0FBQ0gsT0FGcUMsQ0FBdEM7QUFHQSxXQUFLbEYsUUFBTCxDQUFjO0FBQ1ZvQixRQUFBQTtBQURVLE9BQWQ7QUFHSDtBQUNKOztBQStPRDhDLEVBQUFBLGtCQUFrQixHQUFHO0FBQ2pCLFdBQU8sS0FBS3hFLEtBQUwsQ0FBV3NFLFlBQVgsSUFBMkIsS0FBS3RFLEtBQUwsQ0FBV3NFLFlBQVgsQ0FBd0JvQixLQUF4QixJQUFpQzVHLGtCQUFuRTtBQUNIOztBQVFENkcsRUFBQUEsbUJBQW1CLEdBQUc7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQU1DLGFBQWEsR0FBR3pELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFDQSxVQUFNeUQsS0FBSyxHQUFHMUQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHNCQUFqQixDQUFkO0FBRUEsUUFBSTBELFVBQUo7QUFDQSxRQUFJQyxXQUFXLEdBQUcseUJBQUcsTUFBSCxDQUFsQjs7QUFDQSxRQUFJLEtBQUsvRixLQUFMLENBQVcwQiw2QkFBZixFQUE4QztBQUMxQ29FLE1BQUFBLFVBQVUsR0FBRywwQ0FDVCwwQ0FBTSx5QkFBRyxxREFBSCxDQUFOLENBRFMsRUFFVCwwQ0FBSyw2QkFBQyxLQUFEO0FBQ0QsUUFBQSxJQUFJLEVBQUMsVUFESjtBQUVELFFBQUEsS0FBSyxFQUFFLHlCQUFHLFVBQUgsQ0FGTjtBQUdELFFBQUEsS0FBSyxFQUFFLEtBQUs5RixLQUFMLENBQVcyQixlQUhqQjtBQUlELFFBQUEsUUFBUSxFQUFFLEtBQUtxRSx3QkFKZDtBQUtELFFBQUEsV0FBVyxFQUFFLEtBQUtoRyxLQUFMLENBQVc0RCxzQkFBWCxLQUFzQyxLQUxsRDtBQU1ELFFBQUEsU0FBUyxFQUFFO0FBTlYsUUFBTCxDQUZTLENBQWI7QUFXSCxLQVpELE1BWU8sSUFBSSxDQUFDLEtBQUs1RCxLQUFMLENBQVdVLGVBQVgsQ0FBMkJDLE1BQWhDLEVBQXdDO0FBQzNDbUYsTUFBQUEsVUFBVSxHQUFHLDBDQUNULDBDQUFNLHlCQUFHLG9EQUFILENBQU4sQ0FEUyxDQUFiO0FBR0FDLE1BQUFBLFdBQVcsR0FBRyx5QkFBRyxTQUFILENBQWQ7QUFDSCxLQUxNLE1BS0E7QUFDSEQsTUFBQUEsVUFBVSxHQUFHLHdDQUNSLHlCQUFHLHFFQUFILENBRFEsQ0FBYjtBQUdIOztBQUVELFdBQU87QUFBTSxNQUFBLFFBQVEsRUFBRSxLQUFLRztBQUFyQixPQUNILHdDQUFJLHlCQUNBLGdFQUNBLDhEQURBLEdBRUEsNkJBSEEsQ0FBSixDQURHLEVBTUgsMENBQU1ILFVBQU4sQ0FORyxFQU9ILDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRUMsV0FEbkI7QUFFSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtFLG9CQUYvQjtBQUdJLE1BQUEsU0FBUyxFQUFFLEtBSGY7QUFJSSxNQUFBLGVBQWUsRUFBRSxLQUFLakcsS0FBTCxDQUFXMEIsNkJBQVgsSUFBNEMsQ0FBQyxLQUFLMUIsS0FBTCxDQUFXMkI7QUFKN0UsT0FNSTtBQUFRLE1BQUEsSUFBSSxFQUFDLFFBQWI7QUFBc0IsTUFBQSxTQUFTLEVBQUMsUUFBaEM7QUFBeUMsTUFBQSxPQUFPLEVBQUUsS0FBS3VFO0FBQXZELE9BQ0sseUJBQUcsTUFBSCxDQURMLENBTkosQ0FQRyxDQUFQO0FBa0JIOztBQUVEQyxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQixVQUFNUCxhQUFhLEdBQUd6RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsVUFBTXlELEtBQUssR0FBRzFELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBZDtBQUNBLFVBQU1nRSxnQkFBZ0IsR0FBR2pFLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwyQkFBakIsQ0FBekI7QUFDQSxVQUFNaUUsb0JBQW9CLEdBQUdsRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUNBQWpCLENBQTdCO0FBRUEsUUFBSWtFLGFBQUo7QUFDQSxRQUFJQyxRQUFKOztBQUNBLFFBQUksS0FBS3ZHLEtBQUwsQ0FBV3NFLFlBQWYsRUFBNkI7QUFDekIsVUFBSSxLQUFLdEUsS0FBTCxDQUFXc0UsWUFBWCxDQUF3Qm9CLEtBQXhCLElBQWlDNUcsa0JBQXJDLEVBQXlEO0FBQ3JEeUgsUUFBQUEsUUFBUSxHQUFHLHlCQUFHLDZDQUFILENBQVg7QUFDSCxPQUZELE1BRU87QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFNQyxVQUFVLEdBQ1osS0FBS3hHLEtBQUwsQ0FBV3NFLFlBQVgsQ0FBd0JtQyxRQUF4QixDQUFpQ0MsT0FBakMsSUFDQSxLQUFLMUcsS0FBTCxDQUFXc0UsWUFBWCxDQUF3Qm1DLFFBQXhCLENBQWlDRSxXQUFqQyxDQUE2QyxDQUE3QyxDQUZKOztBQUlBLGNBQU1DLGVBQWUsR0FBRywwQ0FBTUosVUFBVSxJQUFJLHlCQUFHLGVBQUgsQ0FBcEIsQ0FBeEI7O0FBRUFELFFBQUFBLFFBQVEsR0FBRywwQ0FDTkssZUFETSxDQUFYO0FBR0g7O0FBQ0ROLE1BQUFBLGFBQWEsR0FBRywwQ0FDWjtBQUFVLFFBQUEsR0FBRyxFQUFFeEgsa0JBQWY7QUFBbUMsUUFBQSxLQUFLLEVBQUUsS0FBS2tCLEtBQUwsQ0FBV3NFLFlBQVgsQ0FBd0JvQjtBQUFsRSxRQURZLENBQWhCO0FBR0g7O0FBRUQsV0FBTztBQUFNLE1BQUEsUUFBUSxFQUFFLEtBQUttQjtBQUFyQixPQUNILHdDQUFJLHlCQUNBLDZFQUNBLHlGQUZBLENBQUosQ0FERyxFQUtILHdDQUFJLHlCQUNBLHlFQUNBLG9EQUZBLENBQUosQ0FMRyxFQVVIO0FBQUssTUFBQSxTQUFTLEVBQUM7QUFBZixPQUNJLDZCQUFDLEtBQUQ7QUFDSSxNQUFBLElBQUksRUFBQyxVQURUO0FBRUksTUFBQSxTQUFTLEVBQUMsOENBRmQ7QUFHSSxNQUFBLFFBQVEsRUFBRSxLQUFLQyxtQkFIbkI7QUFJSSxNQUFBLEtBQUssRUFBRSxLQUFLOUcsS0FBTCxDQUFXdUUsVUFKdEI7QUFLSSxNQUFBLEtBQUssRUFBRSx5QkFBRyxvQkFBSCxDQUxYO0FBTUksTUFBQSxTQUFTLEVBQUUsSUFOZjtBQU9JLE1BQUEsWUFBWSxFQUFDO0FBUGpCLE1BREosRUFVSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSytCLGFBREwsRUFFS0MsUUFGTCxDQVZKLENBVkcsRUEwQkgsNkJBQUMsb0JBQUQ7QUFDSSxNQUFBLEtBQUssRUFBRyx5QkFBRyxvRUFBSCxDQURaO0FBRUksTUFBQSxRQUFRLEVBQUUsS0FBS1EscUJBRm5CO0FBRTBDLE1BQUEsS0FBSyxFQUFFLEtBQUsvRyxLQUFMLENBQVdPO0FBRjVELE1BMUJHLEVBK0JILDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxVQUFILENBRG5CO0FBRUksTUFBQSxvQkFBb0IsRUFBRSxLQUFLc0csc0JBRi9CO0FBR0ksTUFBQSxTQUFTLEVBQUUsS0FIZjtBQUlJLE1BQUEsUUFBUSxFQUFFLENBQUMsS0FBS3JDLGtCQUFMO0FBSmYsT0FNSTtBQUFRLE1BQUEsSUFBSSxFQUFDLFFBQWI7QUFDSSxNQUFBLE9BQU8sRUFBRSxLQUFLMEIsaUJBRGxCO0FBRUksTUFBQSxTQUFTLEVBQUM7QUFGZCxPQUdFLHlCQUFHLE1BQUgsQ0FIRixDQU5KLENBL0JHLEVBMkNILDhDQUNJLDhDQUFVLHlCQUFHLFVBQUgsQ0FBVixDQURKLEVBRUksNkJBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxJQUFJLEVBQUMsU0FBdkI7QUFBaUMsTUFBQSxPQUFPLEVBQUUsS0FBS2M7QUFBL0MsT0FDSyx5QkFBRyw0QkFBSCxDQURMLENBRkosQ0EzQ0csQ0FBUDtBQWtESDs7QUFFREMsRUFBQUEsNkJBQTZCLEdBQUc7QUFDNUIsVUFBTWIsZ0JBQWdCLEdBQUdqRSxHQUFHLENBQUNDLFlBQUosQ0FBaUIsMkJBQWpCLENBQXpCO0FBQ0EsVUFBTXlELEtBQUssR0FBRzFELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixzQkFBakIsQ0FBZDtBQUVBLFFBQUk4RSxTQUFKOztBQUNBLFFBQUksS0FBS2xILEtBQUwsQ0FBV3lFLGlCQUFYLEtBQWlDLEtBQUt6RSxLQUFMLENBQVd1RSxVQUFoRCxFQUE0RDtBQUN4RDJDLE1BQUFBLFNBQVMsR0FBRyx5QkFBRyxlQUFILENBQVo7QUFDSCxLQUZELE1BRU8sSUFBSSxDQUFDLEtBQUtsSCxLQUFMLENBQVd1RSxVQUFYLENBQXNCNEMsVUFBdEIsQ0FBaUMsS0FBS25ILEtBQUwsQ0FBV3lFLGlCQUE1QyxDQUFMLEVBQXFFO0FBQ3hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0F5QyxNQUFBQSxTQUFTLEdBQUcseUJBQUcscUJBQUgsQ0FBWjtBQUNIOztBQUVELFFBQUlFLGVBQWUsR0FBRyxJQUF0Qjs7QUFDQSxRQUFJRixTQUFKLEVBQWU7QUFDWEUsTUFBQUEsZUFBZSxHQUFHLDBDQUNkLDBDQUFNRixTQUFOLENBRGMsRUFFZCwwQ0FDSSw2QkFBQyxnQkFBRDtBQUFrQixRQUFBLE9BQU8sRUFBQyxNQUExQjtBQUFpQyxRQUFBLFNBQVMsRUFBQyxlQUEzQztBQUEyRCxRQUFBLE9BQU8sRUFBRSxLQUFLRztBQUF6RSxTQUNLLHlCQUFHLDBCQUFILENBREwsQ0FESixDQUZjLENBQWxCO0FBUUg7O0FBQ0QsVUFBTXpCLGFBQWEsR0FBR3pELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFDQSxXQUFPO0FBQU0sTUFBQSxRQUFRLEVBQUUsS0FBS2tGO0FBQXJCLE9BQ0gsd0NBQUkseUJBQ0Esb0RBREEsQ0FBSixDQURHLEVBSUg7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0ksNkJBQUMsS0FBRDtBQUNJLE1BQUEsSUFBSSxFQUFDLFVBRFQ7QUFFSSxNQUFBLFFBQVEsRUFBRSxLQUFLQywwQkFGbkI7QUFHSSxNQUFBLEtBQUssRUFBRSxLQUFLdkgsS0FBTCxDQUFXeUUsaUJBSHRCO0FBSUksTUFBQSxTQUFTLEVBQUMsOENBSmQ7QUFLSSxNQUFBLEtBQUssRUFBRSx5QkFBRyx5QkFBSCxDQUxYO0FBTUksTUFBQSxTQUFTLEVBQUUsSUFOZjtBQU9JLE1BQUEsWUFBWSxFQUFDO0FBUGpCLE1BREosRUFVSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSzJDLGVBREwsQ0FWSixDQUpHLEVBa0JILDZCQUFDLGFBQUQ7QUFDSSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxVQUFILENBRG5CO0FBRUksTUFBQSxvQkFBb0IsRUFBRSxLQUFLRSw2QkFGL0I7QUFHSSxNQUFBLFNBQVMsRUFBRSxLQUhmO0FBSUksTUFBQSxRQUFRLEVBQUUsS0FBS3RILEtBQUwsQ0FBV3VFLFVBQVgsS0FBMEIsS0FBS3ZFLEtBQUwsQ0FBV3lFO0FBSm5ELE9BTUk7QUFBUSxNQUFBLElBQUksRUFBQyxRQUFiO0FBQ0ksTUFBQSxPQUFPLEVBQUUsS0FBS3lCLGlCQURsQjtBQUVJLE1BQUEsU0FBUyxFQUFDO0FBRmQsT0FHRSx5QkFBRyxNQUFILENBSEYsQ0FOSixDQWxCRyxDQUFQO0FBOEJIOztBQUVEc0IsRUFBQUEsbUJBQW1CLEdBQUc7QUFDbEIsVUFBTXBCLGdCQUFnQixHQUFHakUsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDJCQUFqQixDQUF6QjtBQUNBLFdBQU8sMENBQ0gsd0NBQUkseUJBQ0EsbUVBQ0Esa0VBRkEsQ0FBSixDQURHLEVBS0gsd0NBQUkseUJBQ0EsNkVBREEsQ0FBSixDQUxHLEVBUUg7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0sseUJBQUcsbUJBQUgsQ0FETCxDQURKLEVBSUk7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBSyxNQUFBLFNBQVMsRUFBQztBQUFmLE9BQ0k7QUFBTSxNQUFBLEdBQUcsRUFBRSxLQUFLcUY7QUFBaEIsT0FBMEMsS0FBS3RHLFlBQUwsQ0FBa0JDLGlCQUE1RCxDQURKLENBREosRUFJSTtBQUFLLE1BQUEsU0FBUyxFQUFDO0FBQWYsT0FDSSw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxNQUFBLFNBQVMsRUFBQyxtQkFBM0M7QUFBK0QsTUFBQSxPQUFPLEVBQUUsS0FBS3NHO0FBQTdFLE9BQ0sseUJBQUcsTUFBSCxDQURMLENBREosRUFJSSw2QkFBQyxnQkFBRDtBQUFrQixNQUFBLElBQUksRUFBQyxTQUF2QjtBQUFpQyxNQUFBLFNBQVMsRUFBQyxtQkFBM0M7QUFBK0QsTUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBN0UsT0FDSyx5QkFBRyxVQUFILENBREwsQ0FKSixDQUpKLENBSkosQ0FSRyxDQUFQO0FBMkJIOztBQUVEQyxFQUFBQSxzQkFBc0IsR0FBRztBQUNyQixRQUFJQyxTQUFKOztBQUNBLFFBQUksS0FBSzdILEtBQUwsQ0FBV2dCLE1BQWYsRUFBdUI7QUFDbkI2RyxNQUFBQSxTQUFTLEdBQUcseUJBQ1IsMEVBRFEsRUFFUixFQUZRLEVBRUo7QUFBQ0MsUUFBQUEsQ0FBQyxFQUFFQyxDQUFDLElBQUksd0NBQUlBLENBQUo7QUFBVCxPQUZJLENBQVo7QUFJSCxLQUxELE1BS08sSUFBSSxLQUFLL0gsS0FBTCxDQUFXd0IsVUFBZixFQUEyQjtBQUM5QnFHLE1BQUFBLFNBQVMsR0FBRyx5QkFDUix1REFEUSxFQUVSLEVBRlEsRUFFSjtBQUFDQyxRQUFBQSxDQUFDLEVBQUVDLENBQUMsSUFBSSx3Q0FBSUEsQ0FBSjtBQUFULE9BRkksQ0FBWjtBQUlIOztBQUNELFVBQU1uQyxhQUFhLEdBQUd6RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FBTywwQ0FDRnlGLFNBREUsRUFFSCx5Q0FDSSx5Q0FBSyx5QkFBRyw2Q0FBSCxFQUFrRCxFQUFsRCxFQUFzRDtBQUFDQyxNQUFBQSxDQUFDLEVBQUVDLENBQUMsSUFBSSx3Q0FBSUEsQ0FBSjtBQUFULEtBQXRELENBQUwsQ0FESixFQUVJLHlDQUFLLHlCQUFHLDZDQUFILEVBQWtELEVBQWxELEVBQXNEO0FBQUNELE1BQUFBLENBQUMsRUFBRUMsQ0FBQyxJQUFJLHdDQUFJQSxDQUFKO0FBQVQsS0FBdEQsQ0FBTCxDQUZKLEVBR0kseUNBQUsseUJBQUcsK0NBQUgsRUFBb0QsRUFBcEQsRUFBd0Q7QUFBQ0QsTUFBQUEsQ0FBQyxFQUFFQyxDQUFDLElBQUksd0NBQUlBLENBQUo7QUFBVCxLQUF4RCxDQUFMLENBSEosQ0FGRyxFQU9ILDZCQUFDLGFBQUQ7QUFBZSxNQUFBLGFBQWEsRUFBRSx5QkFBRyxVQUFILENBQTlCO0FBQ0ksTUFBQSxvQkFBb0IsRUFBRSxLQUFLbkgsdUJBRC9CO0FBRUksTUFBQSxTQUFTLEVBQUU7QUFGZixPQUdJO0FBQVEsTUFBQSxPQUFPLEVBQUUsS0FBS29IO0FBQXRCLE9BQStDLHlCQUFHLE1BQUgsQ0FBL0MsQ0FISixDQVBHLENBQVA7QUFhSDs7QUFFREMsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixVQUFNQyxPQUFPLEdBQUcvRixHQUFHLENBQUNDLFlBQUosQ0FBaUIsd0JBQWpCLENBQWhCO0FBQ0EsV0FBTywwQ0FDSCw2QkFBQyxPQUFELE9BREcsQ0FBUDtBQUdIOztBQUVEK0YsRUFBQUEsZ0JBQWdCLEdBQUc7QUFDZixVQUFNdkMsYUFBYSxHQUFHekQsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDhCQUFqQixDQUF0QjtBQUNBLFdBQU8sMENBQ0gsd0NBQUkseUJBQ0EsNENBQ0EsMENBRkEsQ0FBSixDQURHLEVBS0gsNkJBQUMsYUFBRDtBQUFlLE1BQUEsYUFBYSxFQUFFLHlCQUFHLElBQUgsQ0FBOUI7QUFDSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtnRyxPQUQvQjtBQUVJLE1BQUEsU0FBUyxFQUFFO0FBRmYsTUFMRyxDQUFQO0FBVUg7O0FBRURDLEVBQUFBLHVCQUF1QixHQUFHO0FBQ3RCLFVBQU16QyxhQUFhLEdBQUd6RCxHQUFHLENBQUNDLFlBQUosQ0FBaUIsOEJBQWpCLENBQXRCO0FBQ0EsV0FBTywwQ0FDRix5QkFDRyxnRUFDQSwrQkFGSCxDQURFLEVBS0gsNkJBQUMsYUFBRDtBQUFlLE1BQUEsYUFBYSxFQUFFLHlCQUFHLFNBQUgsQ0FBOUI7QUFDSSxNQUFBLG9CQUFvQixFQUFFLEtBQUtrRyxhQUQvQjtBQUVJLE1BQUEsU0FBUyxFQUFFO0FBRmYsT0FJSTtBQUFRLE1BQUEsSUFBSSxFQUFDLFFBQWI7QUFBc0IsTUFBQSxTQUFTLEVBQUMsUUFBaEM7QUFBeUMsTUFBQSxPQUFPLEVBQUUsS0FBS0M7QUFBdkQsT0FBbUUseUJBQUcsTUFBSCxDQUFuRSxDQUpKLENBTEcsQ0FBUDtBQVlIOztBQUVEQyxFQUFBQSxjQUFjLENBQUN2SSxLQUFELEVBQVE7QUFDbEIsWUFBUUEsS0FBUjtBQUNJLFdBQUszQixhQUFMO0FBQ0ksZUFBTyx5QkFBRyx5QkFBSCxDQUFQOztBQUNKLFdBQUtDLGdCQUFMO0FBQ0ksZUFBTyx5QkFBRyxtQkFBSCxDQUFQOztBQUNKLFdBQUtDLHdCQUFMO0FBQ0ksZUFBTyx5QkFBRyxvQkFBSCxDQUFQOztBQUNKLFdBQUtLLGtCQUFMO0FBQ0ksZUFBTyx5QkFBRyxlQUFILENBQVA7O0FBQ0osV0FBS0osYUFBTDtBQUNBLFdBQUtDLGdCQUFMO0FBQ0ksZUFBTyx5QkFBRyxrQ0FBSCxDQUFQOztBQUNKLFdBQUtDLGFBQUw7QUFDSSxlQUFPLHlCQUFHLGlCQUFILENBQVA7O0FBQ0osV0FBS0MsVUFBTDtBQUNJLGVBQU8seUJBQUcsY0FBSCxDQUFQOztBQUNKO0FBQ0ksZUFBTyxFQUFQO0FBakJSO0FBbUJIOztBQUVENkosRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTUMsVUFBVSxHQUFHdkcsR0FBRyxDQUFDQyxZQUFKLENBQWlCLDBCQUFqQixDQUFuQjtBQUVBLFFBQUl1RyxPQUFKOztBQUNBLFFBQUksS0FBSzNJLEtBQUwsQ0FBVzRDLEtBQWYsRUFBc0I7QUFDbEIsWUFBTWdELGFBQWEsR0FBR3pELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiw4QkFBakIsQ0FBdEI7QUFDQXVHLE1BQUFBLE9BQU8sR0FBRywwQ0FDTix3Q0FBSSx5QkFBRyxpQ0FBSCxDQUFKLENBRE0sRUFFTjtBQUFLLFFBQUEsU0FBUyxFQUFDO0FBQWYsU0FDSSw2QkFBQyxhQUFEO0FBQWUsUUFBQSxhQUFhLEVBQUUseUJBQUcsT0FBSCxDQUE5QjtBQUNJLFFBQUEsb0JBQW9CLEVBQUUsS0FBSy9ILHVCQUQvQjtBQUVJLFFBQUEsU0FBUyxFQUFFLElBRmY7QUFHSSxRQUFBLFFBQVEsRUFBRSxLQUFLMkg7QUFIbkIsUUFESixDQUZNLENBQVY7QUFVSCxLQVpELE1BWU87QUFDSCxjQUFRLEtBQUt2SSxLQUFMLENBQVdDLEtBQW5CO0FBQ0ksYUFBSzVCLGFBQUw7QUFDSXNLLFVBQUFBLE9BQU8sR0FBRyxLQUFLVixnQkFBTCxFQUFWO0FBQ0E7O0FBQ0osYUFBSzNKLGFBQUw7QUFDSXFLLFVBQUFBLE9BQU8sR0FBRyxLQUFLaEQsbUJBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUtwSCxnQkFBTDtBQUNJb0ssVUFBQUEsT0FBTyxHQUFHLEtBQUt4QyxzQkFBTCxFQUFWO0FBQ0E7O0FBQ0osYUFBSzNILHdCQUFMO0FBQ0ltSyxVQUFBQSxPQUFPLEdBQUcsS0FBSzFCLDZCQUFMLEVBQVY7QUFDQTs7QUFDSixhQUFLeEksYUFBTDtBQUNJa0ssVUFBQUEsT0FBTyxHQUFHLEtBQUtuQixtQkFBTCxFQUFWO0FBQ0E7O0FBQ0osYUFBSzlJLGdCQUFMO0FBQ0lpSyxVQUFBQSxPQUFPLEdBQUcsS0FBS2Ysc0JBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUtqSixhQUFMO0FBQ0lnSyxVQUFBQSxPQUFPLEdBQUcsS0FBS1YsZ0JBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUtySixVQUFMO0FBQ0krSixVQUFBQSxPQUFPLEdBQUcsS0FBS1IsZ0JBQUwsRUFBVjtBQUNBOztBQUNKLGFBQUt0SixrQkFBTDtBQUNJOEosVUFBQUEsT0FBTyxHQUFHLEtBQUtOLHVCQUFMLEVBQVY7QUFDQTtBQTNCUjtBQTZCSDs7QUFFRCxRQUFJTyxXQUFKOztBQUNBLFFBQUksS0FBS0osY0FBTCxDQUFvQixLQUFLeEksS0FBTCxDQUFXQyxLQUEvQixDQUFKLEVBQTJDO0FBQ3ZDMkksTUFBQUEsV0FBVyxHQUFHQyxPQUFPLENBQUMsdUNBQUQsQ0FBckI7QUFDSDs7QUFFRCxXQUNJLDZCQUFDLFVBQUQ7QUFBWSxNQUFBLFNBQVMsRUFBQyw4QkFBdEI7QUFDSSxNQUFBLFVBQVUsRUFBRSxLQUFLOUksS0FBTCxDQUFXK0QsVUFEM0I7QUFFSSxNQUFBLEtBQUssRUFBRSxLQUFLMEUsY0FBTCxDQUFvQixLQUFLeEksS0FBTCxDQUFXQyxLQUEvQixDQUZYO0FBR0ksTUFBQSxXQUFXLEVBQUUySSxXQUhqQjtBQUlJLE1BQUEsU0FBUyxFQUFFLEtBQUs3SSxLQUFMLENBQVcrSSxTQUFYLElBQXdCLENBQUN2SyxnQkFBRCxFQUFtQndLLFFBQW5CLENBQTRCLEtBQUsvSSxLQUFMLENBQVdDLEtBQXZDLENBSnZDO0FBS0ksTUFBQSxVQUFVLEVBQUU7QUFMaEIsT0FPQSwwQ0FDSzBJLE9BREwsQ0FQQSxDQURKO0FBYUg7O0FBcnRCc0U7Ozs4QkFBdERoSix5QixlQUNFO0FBQ2ZtSixFQUFBQSxTQUFTLEVBQUVFLG1CQUFVQyxJQUROO0FBRWZ0SCxFQUFBQSxlQUFlLEVBQUVxSCxtQkFBVUUsTUFGWjtBQUdmcEcsRUFBQUEsS0FBSyxFQUFFa0csbUJBQVVDO0FBSEYsQzs4QkFERnRKLHlCLGtCQU9LO0FBQ2xCbUosRUFBQUEsU0FBUyxFQUFFLElBRE87QUFFbEJoRyxFQUFBQSxLQUFLLEVBQUU7QUFGVyxDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTksIDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vLi4vLi4vLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IHsgc2NvcmVQYXNzd29yZCB9IGZyb20gJy4uLy4uLy4uLy4uL3V0aWxzL1Bhc3N3b3JkU2NvcmVyJztcclxuaW1wb3J0IEZpbGVTYXZlciBmcm9tICdmaWxlLXNhdmVyJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi8uLi8uLi8uLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vLi4vLi4vLi4vTW9kYWwnO1xyXG5pbXBvcnQgeyBwcm9tcHRGb3JCYWNrdXBQYXNzcGhyYXNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vQ3Jvc3NTaWduaW5nTWFuYWdlcic7XHJcblxyXG5jb25zdCBQSEFTRV9MT0FESU5HID0gMDtcclxuY29uc3QgUEhBU0VfTUlHUkFURSA9IDE7XHJcbmNvbnN0IFBIQVNFX1BBU1NQSFJBU0UgPSAyO1xyXG5jb25zdCBQSEFTRV9QQVNTUEhSQVNFX0NPTkZJUk0gPSAzO1xyXG5jb25zdCBQSEFTRV9TSE9XS0VZID0gNDtcclxuY29uc3QgUEhBU0VfS0VFUElUU0FGRSA9IDU7XHJcbmNvbnN0IFBIQVNFX1NUT1JJTkcgPSA2O1xyXG5jb25zdCBQSEFTRV9ET05FID0gNztcclxuY29uc3QgUEhBU0VfQ09ORklSTV9TS0lQID0gODtcclxuXHJcbmNvbnN0IFBBU1NXT1JEX01JTl9TQ09SRSA9IDQ7IC8vIFNvIHNlY3VyZSwgbWFueSBjaGFyYWN0ZXJzLCBtdWNoIGNvbXBsZXgsIHdvdywgZXRjLCBldGMuXHJcbmNvbnN0IFBBU1NQSFJBU0VfRkVFREJBQ0tfREVMQVkgPSA1MDA7IC8vIEhvdyBsb25nIGFmdGVyIGtleXN0cm9rZSB0byBvZmZlciBwYXNzcGhyYXNlIGZlZWRiYWNrLCBtcy5cclxuXHJcbi8vIFhYWDogY29waWVkIGZyb20gU2hhcmVEaWFsb2c6IGZhY3RvciBvdXQgaW50byB1dGlsc1xyXG5mdW5jdGlvbiBzZWxlY3RUZXh0KHRhcmdldCkge1xyXG4gICAgY29uc3QgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xyXG4gICAgcmFuZ2Uuc2VsZWN0Tm9kZUNvbnRlbnRzKHRhcmdldCk7XHJcblxyXG4gICAgY29uc3Qgc2VsZWN0aW9uID0gd2luZG93LmdldFNlbGVjdGlvbigpO1xyXG4gICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xyXG4gICAgc2VsZWN0aW9uLmFkZFJhbmdlKHJhbmdlKTtcclxufVxyXG5cclxuLypcclxuICogV2Fsa3MgdGhlIHVzZXIgdGhyb3VnaCB0aGUgcHJvY2VzcyBvZiBjcmVhdGluZyBhIHBhc3NwaHJhc2UgdG8gZ3VhcmQgU2VjdXJlXHJcbiAqIFNlY3JldCBTdG9yYWdlIGluIGFjY291bnQgZGF0YS5cclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2cgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgaGFzQ2FuY2VsOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgICAgICBhY2NvdW50UGFzc3dvcmQ6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICAgICAgZm9yY2U6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xyXG4gICAgICAgIGhhc0NhbmNlbDogdHJ1ZSxcclxuICAgICAgICBmb3JjZTogZmFsc2UsXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLl9yZWNvdmVyeUtleSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fcmVjb3ZlcnlLZXlOb2RlID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9zZXRaeGN2Ym5SZXN1bHRUaW1lb3V0ID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0xPQURJTkcsXHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6ICcnLFxyXG4gICAgICAgICAgICBwYXNzUGhyYXNlQ29uZmlybTogJycsXHJcbiAgICAgICAgICAgIGNvcGllZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGRvd25sb2FkZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB6eGN2Ym5SZXN1bHQ6IG51bGwsXHJcbiAgICAgICAgICAgIGJhY2t1cEluZm86IG51bGwsXHJcbiAgICAgICAgICAgIGJhY2t1cFNpZ1N0YXR1czogbnVsbCxcclxuICAgICAgICAgICAgLy8gZG9lcyB0aGUgc2VydmVyIG9mZmVyIGEgVUkgYXV0aCBmbG93IHdpdGgganVzdCBtLmxvZ2luLnBhc3N3b3JkXHJcbiAgICAgICAgICAgIC8vIGZvciAva2V5cy9kZXZpY2Vfc2lnbmluZy91cGxvYWQ/XHJcbiAgICAgICAgICAgIGNhblVwbG9hZEtleXNXaXRoUGFzc3dvcmRPbmx5OiBudWxsLFxyXG4gICAgICAgICAgICBhY2NvdW50UGFzc3dvcmQ6IHByb3BzLmFjY291bnRQYXNzd29yZCB8fCBcIlwiLFxyXG4gICAgICAgICAgICBhY2NvdW50UGFzc3dvcmRDb3JyZWN0OiBudWxsLFxyXG4gICAgICAgICAgICAvLyBzdGF0dXMgb2YgdGhlIGtleSBiYWNrdXAgdG9nZ2xlIHN3aXRjaFxyXG4gICAgICAgICAgICB1c2VLZXlCYWNrdXA6IHRydWUsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5fZmV0Y2hCYWNrdXBJbmZvKCk7XHJcbiAgICAgICAgdGhpcy5fcXVlcnlLZXlVcGxvYWRBdXRoKCk7XHJcblxyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbignY3J5cHRvLmtleUJhY2t1cFN0YXR1cycsIHRoaXMuX29uS2V5QmFja3VwU3RhdHVzQ2hhbmdlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoJ2NyeXB0by5rZXlCYWNrdXBTdGF0dXMnLCB0aGlzLl9vbktleUJhY2t1cFN0YXR1c0NoYW5nZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBfZmV0Y2hCYWNrdXBJbmZvKCkge1xyXG4gICAgICAgIGNvbnN0IGJhY2t1cEluZm8gPSBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0S2V5QmFja3VwVmVyc2lvbigpO1xyXG4gICAgICAgIGNvbnN0IGJhY2t1cFNpZ1N0YXR1cyA9IChcclxuICAgICAgICAgICAgLy8gd2UgbWF5IG5vdCBoYXZlIHN0YXJ0ZWQgY3J5cHRvIHlldCwgaW4gd2hpY2ggY2FzZSB3ZSBkZWZpbml0ZWx5IGRvbid0IHRydXN0IHRoZSBiYWNrdXBcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzQ3J5cHRvRW5hYmxlZCgpICYmIGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0tleUJhY2t1cFRydXN0ZWQoYmFja3VwSW5mbylcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCB7IGZvcmNlIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGNvbnN0IHBoYXNlID0gKGJhY2t1cEluZm8gJiYgIWZvcmNlKSA/IFBIQVNFX01JR1JBVEUgOiBQSEFTRV9QQVNTUEhSQVNFO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2UsXHJcbiAgICAgICAgICAgIGJhY2t1cEluZm8sXHJcbiAgICAgICAgICAgIGJhY2t1cFNpZ1N0YXR1cyxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgYmFja3VwSW5mbyxcclxuICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3F1ZXJ5S2V5VXBsb2FkQXV0aCgpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkudXBsb2FkRGV2aWNlU2lnbmluZ0tleXMobnVsbCwge30pO1xyXG4gICAgICAgICAgICAvLyBXZSBzaG91bGQgbmV2ZXIgZ2V0IGhlcmU6IHRoZSBzZXJ2ZXIgc2hvdWxkIGFsd2F5cyByZXF1aXJlXHJcbiAgICAgICAgICAgIC8vIFVJIGF1dGggdG8gdXBsb2FkIGRldmljZSBzaWduaW5nIGtleXMuIElmIHdlIGRvLCB3ZSB1cGxvYWRcclxuICAgICAgICAgICAgLy8gbm8ga2V5cyB3aGljaCB3b3VsZCBiZSBhIG5vLW9wLlxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInVwbG9hZERldmljZVNpZ25pbmdLZXlzIHVuZXhwZWN0ZWRseSBzdWNjZWVkZWQgd2l0aG91dCBVSSBhdXRoIVwiKTtcclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICBpZiAoIWVycm9yLmRhdGEuZmxvd3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidXBsb2FkRGV2aWNlU2lnbmluZ0tleXMgYWR2ZXJ0aXNlZCBubyBmbG93cyFcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgY2FuVXBsb2FkS2V5c1dpdGhQYXNzd29yZE9ubHkgPSBlcnJvci5kYXRhLmZsb3dzLnNvbWUoZiA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZi5zdGFnZXMubGVuZ3RoID09PSAxICYmIGYuc3RhZ2VzWzBdID09PSAnbS5sb2dpbi5wYXNzd29yZCc7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIGNhblVwbG9hZEtleXNXaXRoUGFzc3dvcmRPbmx5LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uS2V5QmFja3VwU3RhdHVzQ2hhbmdlID0gKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBoYXNlID09PSBQSEFTRV9NSUdSQVRFKSB0aGlzLl9mZXRjaEJhY2t1cEluZm8oKTtcclxuICAgIH1cclxuXHJcbiAgICBfY29sbGVjdFJlY292ZXJ5S2V5Tm9kZSA9IChuKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fcmVjb3ZlcnlLZXlOb2RlID0gbjtcclxuICAgIH1cclxuXHJcbiAgICBfb25Vc2VLZXlCYWNrdXBDaGFuZ2UgPSAoZW5hYmxlZCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICB1c2VLZXlCYWNrdXA6IGVuYWJsZWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uTWlncmF0ZUZvcm1TdWJtaXQgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5iYWNrdXBTaWdTdGF0dXMudXNhYmxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2Jvb3RzdHJhcFNlY3JldFN0b3JhZ2UoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZXN0b3JlQmFja3VwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbkNvcHlDbGljayA9ICgpID0+IHtcclxuICAgICAgICBzZWxlY3RUZXh0KHRoaXMuX3JlY292ZXJ5S2V5Tm9kZSk7XHJcbiAgICAgICAgY29uc3Qgc3VjY2Vzc2Z1bCA9IGRvY3VtZW50LmV4ZWNDb21tYW5kKCdjb3B5Jyk7XHJcbiAgICAgICAgaWYgKHN1Y2Nlc3NmdWwpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBjb3BpZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBwaGFzZTogUEhBU0VfS0VFUElUU0FGRSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbkRvd25sb2FkQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFt0aGlzLl9yZWNvdmVyeUtleS5lbmNvZGVkUHJpdmF0ZUtleV0sIHtcclxuICAgICAgICAgICAgdHlwZTogJ3RleHQvcGxhaW47Y2hhcnNldD11cy1hc2NpaScsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCAncmVjb3Zlcnkta2V5LnR4dCcpO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgZG93bmxvYWRlZDogdHJ1ZSxcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX0tFRVBJVFNBRkUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2RvQm9vdHN0cmFwVUlBdXRoID0gYXN5bmMgKG1ha2VSZXF1ZXN0KSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY2FuVXBsb2FkS2V5c1dpdGhQYXNzd29yZE9ubHkgJiYgdGhpcy5zdGF0ZS5hY2NvdW50UGFzc3dvcmQpIHtcclxuICAgICAgICAgICAgYXdhaXQgbWFrZVJlcXVlc3Qoe1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ20ubG9naW4ucGFzc3dvcmQnLFxyXG4gICAgICAgICAgICAgICAgaWRlbnRpZmllcjoge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdtLmlkLnVzZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXI6IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRVc2VySWQoKSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vbWF0cml4LW9yZy9zeW5hcHNlL2lzc3Vlcy81NjY1XHJcbiAgICAgICAgICAgICAgICB1c2VyOiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCksXHJcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogdGhpcy5zdGF0ZS5hY2NvdW50UGFzc3dvcmQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEludGVyYWN0aXZlQXV0aERpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkludGVyYWN0aXZlQXV0aERpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc3QgeyBmaW5pc2hlZCB9ID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZyhcclxuICAgICAgICAgICAgICAgICdDcm9zcy1zaWduaW5nIGtleXMgZGlhbG9nJywgJycsIEludGVyYWN0aXZlQXV0aERpYWxvZyxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJTZXR0aW5nIHVwIGtleXNcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgbWF0cml4Q2xpZW50OiBNYXRyaXhDbGllbnRQZWcuZ2V0KCksXHJcbiAgICAgICAgICAgICAgICAgICAgbWFrZVJlcXVlc3QsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCBbY29uZmlybWVkXSA9IGF3YWl0IGZpbmlzaGVkO1xyXG4gICAgICAgICAgICBpZiAoIWNvbmZpcm1lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ3Jvc3Mtc2lnbmluZyBrZXkgdXBsb2FkIGF1dGggY2FuY2VsZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX2Jvb3RzdHJhcFNlY3JldFN0b3JhZ2UgPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9TVE9SSU5HLFxyXG4gICAgICAgICAgICBlcnJvcjogbnVsbCxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICBjb25zdCB7IGZvcmNlIH0gPSB0aGlzLnByb3BzO1xyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZm9yY2UpIHtcclxuICAgICAgICAgICAgICAgIGF3YWl0IGNsaS5ib290c3RyYXBTZWNyZXRTdG9yYWdlKHtcclxuICAgICAgICAgICAgICAgICAgICBhdXRoVXBsb2FkRGV2aWNlU2lnbmluZ0tleXM6IHRoaXMuX2RvQm9vdHN0cmFwVUlBdXRoLFxyXG4gICAgICAgICAgICAgICAgICAgIGNyZWF0ZVNlY3JldFN0b3JhZ2VLZXk6IGFzeW5jICgpID0+IHRoaXMuX3JlY292ZXJ5S2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIHNldHVwTmV3S2V5QmFja3VwOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHNldHVwTmV3U2VjcmV0U3RvcmFnZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgY2xpLmJvb3RzdHJhcFNlY3JldFN0b3JhZ2Uoe1xyXG4gICAgICAgICAgICAgICAgICAgIGF1dGhVcGxvYWREZXZpY2VTaWduaW5nS2V5czogdGhpcy5fZG9Cb290c3RyYXBVSUF1dGgsXHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlU2VjcmV0U3RvcmFnZUtleTogYXN5bmMgKCkgPT4gdGhpcy5fcmVjb3ZlcnlLZXksXHJcbiAgICAgICAgICAgICAgICAgICAga2V5QmFja3VwSW5mbzogdGhpcy5zdGF0ZS5iYWNrdXBJbmZvLFxyXG4gICAgICAgICAgICAgICAgICAgIHNldHVwTmV3S2V5QmFja3VwOiAhdGhpcy5zdGF0ZS5iYWNrdXBJbmZvICYmIHRoaXMuc3RhdGUudXNlS2V5QmFja3VwLFxyXG4gICAgICAgICAgICAgICAgICAgIGdldEtleUJhY2t1cFBhc3NwaHJhc2U6IHByb21wdEZvckJhY2t1cFBhc3NwaHJhc2UsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9ET05FLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLmNhblVwbG9hZEtleXNXaXRoUGFzc3dvcmRPbmx5ICYmIGUuaHR0cFN0YXR1cyA9PT0gNDAxICYmIGUuZGF0YS5mbG93cykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjb3VudFBhc3N3b3JkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBhY2NvdW50UGFzc3dvcmRDb3JyZWN0OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBwaGFzZTogUEhBU0VfTUlHUkFURSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGVycm9yOiBlIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJFcnJvciBib290c3RyYXBwaW5nIHNlY3JldCBzdG9yYWdlXCIsIGUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfb25DYW5jZWwgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5vbkZpbmlzaGVkKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25Eb25lID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvcHMub25GaW5pc2hlZCh0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVzdG9yZUJhY2t1cCA9IGFzeW5jICgpID0+IHtcclxuICAgICAgICBjb25zdCBSZXN0b3JlS2V5QmFja3VwRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5rZXliYWNrdXAuUmVzdG9yZUtleUJhY2t1cERpYWxvZycpO1xyXG4gICAgICAgIGNvbnN0IHsgZmluaXNoZWQgfSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coXHJcbiAgICAgICAgICAgICdSZXN0b3JlIEJhY2t1cCcsICcnLCBSZXN0b3JlS2V5QmFja3VwRGlhbG9nLCB7c2hvd1N1bW1hcnk6IGZhbHNlfSwgbnVsbCxcclxuICAgICAgICAgICAgLyogcHJpb3JpdHkgPSAqLyBmYWxzZSwgLyogc3RhdGljID0gKi8gZmFsc2UsXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgYXdhaXQgZmluaXNoZWQ7XHJcbiAgICAgICAgY29uc3QgeyBiYWNrdXBTaWdTdGF0dXMgfSA9IGF3YWl0IHRoaXMuX2ZldGNoQmFja3VwSW5mbygpO1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgYmFja3VwU2lnU3RhdHVzLnVzYWJsZSAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmNhblVwbG9hZEtleXNXaXRoUGFzc3dvcmRPbmx5ICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYWNjb3VudFBhc3N3b3JkXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2Jvb3RzdHJhcFNlY3JldFN0b3JhZ2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uU2tpcFNldHVwQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cGhhc2U6IFBIQVNFX0NPTkZJUk1fU0tJUH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblNldFVwQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cGhhc2U6IFBIQVNFX1BBU1NQSFJBU0V9KTtcclxuICAgIH1cclxuXHJcbiAgICBfb25Ta2lwUGFzc1BocmFzZUNsaWNrID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX3JlY292ZXJ5S2V5ID1cclxuICAgICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWF0ZVJlY292ZXJ5S2V5RnJvbVBhc3NwaHJhc2UoKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgY29waWVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZG93bmxvYWRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9TSE9XS0VZLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblBhc3NQaHJhc2VOZXh0Q2xpY2sgPSBhc3luYyAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgLy8gSWYgd2UncmUgd2FpdGluZyBmb3IgdGhlIHRpbWVvdXQgYmVmb3JlIHVwZGF0aW5nIHRoZSByZXN1bHQgYXQgdGhpcyBwb2ludCxcclxuICAgICAgICAvLyBza2lwIGFoZWFkIGFuZCBkbyBpdCBub3csIG90aGVyd2lzZSB3ZSdsbCBkZW55IHRoZSBhdHRlbXB0IHRvIHByb2NlZWRcclxuICAgICAgICAvLyBldmVuIGlmIHRoZSB1c2VyIGVudGVyZWQgYSB2YWxpZCBwYXNzcGhyYXNlXHJcbiAgICAgICAgaWYgKHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQpO1xyXG4gICAgICAgICAgICB0aGlzLl9zZXRaeGN2Ym5SZXN1bHRUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgICAgICAgYXdhaXQgbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHp4Y3ZiblJlc3VsdDogc2NvcmVQYXNzd29yZCh0aGlzLnN0YXRlLnBhc3NQaHJhc2UpLFxyXG4gICAgICAgICAgICAgICAgfSwgcmVzb2x2ZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fcGFzc1BocmFzZUlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwaGFzZTogUEhBU0VfUEFTU1BIUkFTRV9DT05GSVJNfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfb25QYXNzUGhyYXNlQ29uZmlybU5leHRDbGljayA9IGFzeW5jIChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5wYXNzUGhyYXNlICE9PSB0aGlzLnN0YXRlLnBhc3NQaHJhc2VDb25maXJtKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuX3JlY292ZXJ5S2V5ID1cclxuICAgICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNyZWF0ZVJlY292ZXJ5S2V5RnJvbVBhc3NwaHJhc2UodGhpcy5zdGF0ZS5wYXNzUGhyYXNlKTtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgY29waWVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZG93bmxvYWRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9TSE9XS0VZLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblNldEFnYWluQ2xpY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6ICcnLFxyXG4gICAgICAgICAgICBwYXNzUGhyYXNlQ29uZmlybTogJycsXHJcbiAgICAgICAgICAgIHBoYXNlOiBQSEFTRV9QQVNTUEhSQVNFLFxyXG4gICAgICAgICAgICB6eGN2Ym5SZXN1bHQ6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uS2VlcEl0U2FmZUJhY2tDbGljayA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGhhc2U6IFBIQVNFX1NIT1dLRVksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uUGFzc1BocmFzZUNoYW5nZSA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2U6IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3NldFp4Y3ZiblJlc3VsdFRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fc2V0WnhjdmJuUmVzdWx0VGltZW91dCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgLy8gcHJlY29tcHV0ZSB0aGlzIGFuZCBrZWVwIGl0IGluIHN0YXRlOiB6eGN2Ym4gaXMgZmFzdCBidXRcclxuICAgICAgICAgICAgICAgIC8vIHdlIHVzZSBpdCBpbiBhIGNvdXBsZSBvZiBkaWZmZXJlbnQgcGxhY2VzIHNvIG5vIHBvaW50IHJlY29tcHV0aW5nXHJcbiAgICAgICAgICAgICAgICAvLyBpdCB1bm5lY2Vzc2FyaWx5LlxyXG4gICAgICAgICAgICAgICAgenhjdmJuUmVzdWx0OiBzY29yZVBhc3N3b3JkKHRoaXMuc3RhdGUucGFzc1BocmFzZSksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIFBBU1NQSFJBU0VfRkVFREJBQ0tfREVMQVkpO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblBhc3NQaHJhc2VDb25maXJtQ2hhbmdlID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcGFzc1BocmFzZUNvbmZpcm06IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9wYXNzUGhyYXNlSXNWYWxpZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQgJiYgdGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuc2NvcmUgPj0gUEFTU1dPUkRfTUlOX1NDT1JFO1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkFjY291bnRQYXNzd29yZENoYW5nZSA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGFjY291bnRQYXNzd29yZDogZS50YXJnZXQudmFsdWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlTWlncmF0ZSgpIHtcclxuICAgICAgICAvLyBUT0RPOiBUaGlzIGlzIGEgdGVtcG9yYXJ5IHNjcmVlbiBzbyBwZW9wbGUgd2hvIGhhdmUgdGhlIGxhYnMgZmxhZyB0dXJuZWQgb24gYW5kXHJcbiAgICAgICAgLy8gY2xpY2sgdGhlIGJ1dHRvbiBhcmUgYXdhcmUgdGhleSdyZSBtYWtpbmcgYSBjaGFuZ2UgdG8gdGhlaXIgYWNjb3VudC5cclxuICAgICAgICAvLyBPbmNlIHdlJ3JlIGNvbmZpZGVudCBlbm91Z2ggaW4gdGhpcyAoYW5kIGl0J3Mgc3VwcG9ydGVkIGVub3VnaCkgd2UgY2FuIGRvXHJcbiAgICAgICAgLy8gaXQgYXV0b21hdGljYWxseS5cclxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8xMTY5NlxyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgY29uc3QgRmllbGQgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5GaWVsZCcpO1xyXG5cclxuICAgICAgICBsZXQgYXV0aFByb21wdDtcclxuICAgICAgICBsZXQgbmV4dENhcHRpb24gPSBfdChcIk5leHRcIik7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY2FuVXBsb2FkS2V5c1dpdGhQYXNzd29yZE9ubHkpIHtcclxuICAgICAgICAgICAgYXV0aFByb21wdCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PntfdChcIkVudGVyIHlvdXIgYWNjb3VudCBwYXNzd29yZCB0byBjb25maXJtIHRoZSB1cGdyYWRlOlwiKX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXY+PEZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJQYXNzd29yZFwiKX1cclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5hY2NvdW50UGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuX29uQWNjb3VudFBhc3N3b3JkQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIGZsYWdJbnZhbGlkPXt0aGlzLnN0YXRlLmFjY291bnRQYXNzd29yZENvcnJlY3QgPT09IGZhbHNlfVxyXG4gICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgIC8+PC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnN0YXRlLmJhY2t1cFNpZ1N0YXR1cy51c2FibGUpIHtcclxuICAgICAgICAgICAgYXV0aFByb21wdCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PntfdChcIlJlc3RvcmUgeW91ciBrZXkgYmFja3VwIHRvIHVwZ3JhZGUgeW91ciBlbmNyeXB0aW9uXCIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIG5leHRDYXB0aW9uID0gX3QoXCJSZXN0b3JlXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGF1dGhQcm9tcHQgPSA8cD5cclxuICAgICAgICAgICAgICAgIHtfdChcIllvdSdsbCBuZWVkIHRvIGF1dGhlbnRpY2F0ZSB3aXRoIHRoZSBzZXJ2ZXIgdG8gY29uZmlybSB0aGUgdXBncmFkZS5cIil9XHJcbiAgICAgICAgICAgIDwvcD47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gPGZvcm0gb25TdWJtaXQ9e3RoaXMuX29uTWlncmF0ZUZvcm1TdWJtaXR9PlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIlVwZ3JhZGUgdGhpcyBzZXNzaW9uIHRvIGFsbG93IGl0IHRvIHZlcmlmeSBvdGhlciBzZXNzaW9ucywgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJncmFudGluZyB0aGVtIGFjY2VzcyB0byBlbmNyeXB0ZWQgbWVzc2FnZXMgYW5kIG1hcmtpbmcgdGhlbSBcIiArXHJcbiAgICAgICAgICAgICAgICBcImFzIHRydXN0ZWQgZm9yIG90aGVyIHVzZXJzLlwiLFxyXG4gICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgPGRpdj57YXV0aFByb21wdH08L2Rpdj5cclxuICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e25leHRDYXB0aW9ufVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uTWlncmF0ZUZvcm1TdWJtaXR9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgcHJpbWFyeURpc2FibGVkPXt0aGlzLnN0YXRlLmNhblVwbG9hZEtleXNXaXRoUGFzc3dvcmRPbmx5ICYmICF0aGlzLnN0YXRlLmFjY291bnRQYXNzd29yZH1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3NOYW1lPVwiZGFuZ2VyXCIgb25DbGljaz17dGhpcy5fb25Ta2lwU2V0dXBDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAge190KCdTa2lwJyl9XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9EaWFsb2dCdXR0b25zPlxyXG4gICAgICAgIDwvZm9ybT47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlUGFzc1BocmFzZSgpIHtcclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIGNvbnN0IEZpZWxkID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRmllbGQnKTtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIGNvbnN0IExhYmVsbGVkVG9nZ2xlU3dpdGNoID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuTGFiZWxsZWRUb2dnbGVTd2l0Y2gnKTtcclxuXHJcbiAgICAgICAgbGV0IHN0cmVuZ3RoTWV0ZXI7XHJcbiAgICAgICAgbGV0IGhlbHBUZXh0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnp4Y3ZiblJlc3VsdCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuc2NvcmUgPj0gUEFTU1dPUkRfTUlOX1NDT1JFKSB7XHJcbiAgICAgICAgICAgICAgICBoZWxwVGV4dCA9IF90KFwiR3JlYXQhIFRoaXMgcGFzc3BocmFzZSBsb29rcyBzdHJvbmcgZW5vdWdoLlwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIFdlIHRha2UgdGhlIHdhcm5pbmcgZnJvbSB6eGN2Ym4gb3IgZmFpbGluZyB0aGF0LCB0aGUgZmlyc3RcclxuICAgICAgICAgICAgICAgIC8vIHN1Z2dlc3Rpb24uIEluIHByYWN0aWNlIFRoZSBmaXJzdCBpcyBnZW5lcmFsbHkgdGhlIG1vc3QgcmVsZXZhbnRcclxuICAgICAgICAgICAgICAgIC8vIGFuZCBpdCdzIHByb2JhYmx5IGJldHRlciB0byBwcmVzZW50IHRoZSB1c2VyIHdpdGggb25lIHRoaW5nIHRvXHJcbiAgICAgICAgICAgICAgICAvLyBpbXByb3ZlIGFib3V0IHRoZWlyIHBhc3N3b3JkIHRoYW4gYSB3aG9sZSBjb2xsZWN0aW9uIC0gaXQgY2FuXHJcbiAgICAgICAgICAgICAgICAvLyBzcGl0IG91dCBhIHdhcm5pbmcgYW5kIG11bHRpcGxlIHN1Z2dlc3Rpb25zIHdoaWNoIHN0YXJ0cyBnZXR0aW5nXHJcbiAgICAgICAgICAgICAgICAvLyB2ZXJ5IGluZm9ybWF0aW9uLWRlbnNlLlxyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3VnZ2VzdGlvbiA9IChcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLnp4Y3ZiblJlc3VsdC5mZWVkYmFjay53YXJuaW5nIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS56eGN2Ym5SZXN1bHQuZmVlZGJhY2suc3VnZ2VzdGlvbnNbMF1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzdWdnZXN0aW9uQmxvY2sgPSA8ZGl2PntzdWdnZXN0aW9uIHx8IF90KFwiS2VlcCBnb2luZy4uLlwiKX08L2Rpdj47XHJcblxyXG4gICAgICAgICAgICAgICAgaGVscFRleHQgPSA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHtzdWdnZXN0aW9uQmxvY2t9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc3RyZW5ndGhNZXRlciA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cHJvZ3Jlc3MgbWF4PXtQQVNTV09SRF9NSU5fU0NPUkV9IHZhbHVlPXt0aGlzLnN0YXRlLnp4Y3ZiblJlc3VsdC5zY29yZX0gLz5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxmb3JtIG9uU3VibWl0PXt0aGlzLl9vblBhc3NQaHJhc2VOZXh0Q2xpY2t9PlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIlNldCB1cCBlbmNyeXB0aW9uIG9uIHRoaXMgc2Vzc2lvbiB0byBhbGxvdyBpdCB0byB2ZXJpZnkgb3RoZXIgc2Vzc2lvbnMsIFwiICtcclxuICAgICAgICAgICAgICAgIFwiZ3JhbnRpbmcgdGhlbSBhY2Nlc3MgdG8gZW5jcnlwdGVkIG1lc3NhZ2VzIGFuZCBtYXJraW5nIHRoZW0gYXMgdHJ1c3RlZCBmb3Igb3RoZXIgdXNlcnMuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIlNlY3VyZSB5b3VyIGVuY3J5cHRpb24ga2V5cyB3aXRoIGEgcGFzc3BocmFzZS4gRm9yIG1heGltdW0gc2VjdXJpdHkgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJ0aGlzIHNob3VsZCBiZSBkaWZmZXJlbnQgdG8geW91ciBhY2NvdW50IHBhc3N3b3JkOlwiLFxyXG4gICAgICAgICAgICApfTwvcD5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZ19wYXNzUGhyYXNlQ29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0NyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2dfcGFzc1BocmFzZUZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5fb25QYXNzUGhyYXNlQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnBhc3NQaHJhc2V9XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e190KFwiRW50ZXIgYSBwYXNzcGhyYXNlXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1cz17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJuZXctcGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZ19wYXNzUGhyYXNlSGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtzdHJlbmd0aE1ldGVyfVxyXG4gICAgICAgICAgICAgICAgICAgIHtoZWxwVGV4dH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDxMYWJlbGxlZFRvZ2dsZVN3aXRjaFxyXG4gICAgICAgICAgICAgICAgbGFiZWw9eyBfdChcIkJhY2sgdXAgbXkgZW5jcnlwdGlvbiBrZXlzLCBzZWN1cmluZyB0aGVtIHdpdGggdGhlIHNhbWUgcGFzc3BocmFzZVwiKX1cclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblVzZUtleUJhY2t1cENoYW5nZX0gdmFsdWU9e3RoaXMuc3RhdGUudXNlS2V5QmFja3VwfVxyXG4gICAgICAgICAgICAvPlxyXG5cclxuICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e190KCdDb250aW51ZScpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uUGFzc1BocmFzZU5leHRDbGlja31cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlZD17IXRoaXMuX3Bhc3NQaHJhc2VJc1ZhbGlkKCl9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5fb25Ta2lwU2V0dXBDbGlja31cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkYW5nZXJcIlxyXG4gICAgICAgICAgICAgICAgPntfdChcIlNraXBcIil9PC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvRGlhbG9nQnV0dG9ucz5cclxuXHJcbiAgICAgICAgICAgIDxkZXRhaWxzPlxyXG4gICAgICAgICAgICAgICAgPHN1bW1hcnk+e190KFwiQWR2YW5jZWRcIil9PC9zdW1tYXJ5PlxyXG4gICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD0ncHJpbWFyeScgb25DbGljaz17dGhpcy5fb25Ta2lwUGFzc1BocmFzZUNsaWNrfSA+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiU2V0IHVwIHdpdGggYSByZWNvdmVyeSBrZXlcIil9XHJcbiAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgIDwvZGV0YWlscz5cclxuICAgICAgICA8L2Zvcm0+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZVBhc3NQaHJhc2VDb25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IEFjY2Vzc2libGVCdXR0b24gPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5BY2Nlc3NpYmxlQnV0dG9uJyk7XHJcbiAgICAgICAgY29uc3QgRmllbGQgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5GaWVsZCcpO1xyXG5cclxuICAgICAgICBsZXQgbWF0Y2hUZXh0O1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBhc3NQaHJhc2VDb25maXJtID09PSB0aGlzLnN0YXRlLnBhc3NQaHJhc2UpIHtcclxuICAgICAgICAgICAgbWF0Y2hUZXh0ID0gX3QoXCJUaGF0IG1hdGNoZXMhXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuc3RhdGUucGFzc1BocmFzZS5zdGFydHNXaXRoKHRoaXMuc3RhdGUucGFzc1BocmFzZUNvbmZpcm0pKSB7XHJcbiAgICAgICAgICAgIC8vIG9ubHkgdGVsbCB0aGVtIHRoZXkncmUgd3JvbmcgaWYgdGhleSd2ZSBhY3R1YWxseSBnb25lIHdyb25nLlxyXG4gICAgICAgICAgICAvLyBTZWN1cml0eSBjb25jaW91cyByZWFkZXJzIHdpbGwgbm90ZSB0aGF0IGlmIHlvdSBsZWZ0IHJpb3Qtd2ViIHVuYXR0ZW5kZWRcclxuICAgICAgICAgICAgLy8gb24gdGhpcyBzY3JlZW4sIHRoaXMgd291bGQgbWFrZSBpdCBlYXN5IGZvciBhIG1hbGljaW91cyBwZXJzb24gdG8gZ3Vlc3NcclxuICAgICAgICAgICAgLy8geW91ciBwYXNzcGhyYXNlIG9uZSBsZXR0ZXIgYXQgYSB0aW1lLCBidXQgdGhleSBjb3VsZCBnZXQgdGhpcyBmYXN0ZXIgYnlcclxuICAgICAgICAgICAgLy8ganVzdCBvcGVuaW5nIHRoZSBicm93c2VyJ3MgZGV2ZWxvcGVyIHRvb2xzIGFuZCByZWFkaW5nIGl0LlxyXG4gICAgICAgICAgICAvLyBOb3RlIHRoYXQgbm90IGhhdmluZyB0eXBlZCBhbnl0aGluZyBhdCBhbGwgd2lsbCBub3QgaGl0IHRoaXMgY2xhdXNlIGFuZFxyXG4gICAgICAgICAgICAvLyBmYWxsIHRocm91Z2ggc28gZW1wdHkgYm94ID09PSBubyBoaW50LlxyXG4gICAgICAgICAgICBtYXRjaFRleHQgPSBfdChcIlRoYXQgZG9lc24ndCBtYXRjaC5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcGFzc1BocmFzZU1hdGNoID0gbnVsbDtcclxuICAgICAgICBpZiAobWF0Y2hUZXh0KSB7XHJcbiAgICAgICAgICAgIHBhc3NQaHJhc2VNYXRjaCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PnttYXRjaFRleHR9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGVsZW1lbnQ9XCJzcGFuXCIgY2xhc3NOYW1lPVwibXhfbGlua0J1dHRvblwiIG9uQ2xpY2s9e3RoaXMuX29uU2V0QWdhaW5DbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkdvIGJhY2sgdG8gc2V0IGl0IGFnYWluLlwiKX1cclxuICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIHJldHVybiA8Zm9ybSBvblN1Ym1pdD17dGhpcy5fb25QYXNzUGhyYXNlQ29uZmlybU5leHRDbGlja30+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiRW50ZXIgeW91ciBwYXNzcGhyYXNlIGEgc2Vjb25kIHRpbWUgdG8gY29uZmlybSBpdC5cIixcclxuICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZ19wYXNzUGhyYXNlQ29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8RmllbGRcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLl9vblBhc3NQaHJhc2VDb25maXJtQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnBhc3NQaHJhc2VDb25maXJtfVxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm14X0NyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2dfcGFzc1BocmFzZUZpZWxkXCJcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17X3QoXCJDb25maXJtIHlvdXIgcGFzc3BocmFzZVwiKX1cclxuICAgICAgICAgICAgICAgICAgICBhdXRvRm9jdXM9e3RydWV9XHJcbiAgICAgICAgICAgICAgICAgICAgYXV0b0NvbXBsZXRlPVwibmV3LXBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2dfcGFzc1BocmFzZU1hdGNoXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3Bhc3NQaHJhc2VNYXRjaH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnNcclxuICAgICAgICAgICAgICAgIHByaW1hcnlCdXR0b249e190KCdDb250aW51ZScpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uUGFzc1BocmFzZUNvbmZpcm1OZXh0Q2xpY2t9XHJcbiAgICAgICAgICAgICAgICBoYXNDYW5jZWw9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuc3RhdGUucGFzc1BocmFzZSAhPT0gdGhpcy5zdGF0ZS5wYXNzUGhyYXNlQ29uZmlybX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLl9vblNraXBTZXR1cENsaWNrfVxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImRhbmdlclwiXHJcbiAgICAgICAgICAgICAgICA+e190KFwiU2tpcFwiKX08L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9EaWFsb2dCdXR0b25zPlxyXG4gICAgICAgIDwvZm9ybT47XHJcbiAgICB9XHJcblxyXG4gICAgX3JlbmRlclBoYXNlU2hvd0tleSgpIHtcclxuICAgICAgICBjb25zdCBBY2Nlc3NpYmxlQnV0dG9uID0gc2RrLmdldENvbXBvbmVudCgnZWxlbWVudHMuQWNjZXNzaWJsZUJ1dHRvbicpO1xyXG4gICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdXIgcmVjb3Zlcnkga2V5IGlzIGEgc2FmZXR5IG5ldCAtIHlvdSBjYW4gdXNlIGl0IHRvIHJlc3RvcmUgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJhY2Nlc3MgdG8geW91ciBlbmNyeXB0ZWQgbWVzc2FnZXMgaWYgeW91IGZvcmdldCB5b3VyIHBhc3NwaHJhc2UuXCIsXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgICAgICA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICBcIktlZXAgYSBjb3B5IG9mIGl0IHNvbWV3aGVyZSBzZWN1cmUsIGxpa2UgYSBwYXNzd29yZCBtYW5hZ2VyIG9yIGV2ZW4gYSBzYWZlLlwiLFxyXG4gICAgICAgICAgICApfTwvcD5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJteF9DcmVhdGVTZWNyZXRTdG9yYWdlRGlhbG9nX3ByaW1hcnlDb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZ19yZWNvdmVyeUtleUhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHtfdChcIllvdXIgcmVjb3Zlcnkga2V5XCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2dfcmVjb3ZlcnlLZXlDb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm14X0NyZWF0ZVNlY3JldFN0b3JhZ2VEaWFsb2dfcmVjb3ZlcnlLZXlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGNvZGUgcmVmPXt0aGlzLl9jb2xsZWN0UmVjb3ZlcnlLZXlOb2RlfT57dGhpcy5fcmVjb3ZlcnlLZXkuZW5jb2RlZFByaXZhdGVLZXl9PC9jb2RlPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZ19yZWNvdmVyeUtleUJ1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFjY2Vzc2libGVCdXR0b24ga2luZD0ncHJpbWFyeScgY2xhc3NOYW1lPVwibXhfRGlhbG9nX3ByaW1hcnlcIiBvbkNsaWNrPXt0aGlzLl9vbkNvcHlDbGlja30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7X3QoXCJDb3B5XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBY2Nlc3NpYmxlQnV0dG9uIGtpbmQ9J3ByaW1hcnknIGNsYXNzTmFtZT1cIm14X0RpYWxvZ19wcmltYXJ5XCIgb25DbGljaz17dGhpcy5fb25Eb3dubG9hZENsaWNrfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtfdChcIkRvd25sb2FkXCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FjY2Vzc2libGVCdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZUtlZXBJdFNhZmUoKSB7XHJcbiAgICAgICAgbGV0IGludHJvVGV4dDtcclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jb3BpZWQpIHtcclxuICAgICAgICAgICAgaW50cm9UZXh0ID0gX3QoXHJcbiAgICAgICAgICAgICAgICBcIllvdXIgcmVjb3Zlcnkga2V5IGhhcyBiZWVuIDxiPmNvcGllZCB0byB5b3VyIGNsaXBib2FyZDwvYj4sIHBhc3RlIGl0IHRvOlwiLFxyXG4gICAgICAgICAgICAgICAge30sIHtiOiBzID0+IDxiPntzfTwvYj59LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5kb3dubG9hZGVkKSB7XHJcbiAgICAgICAgICAgIGludHJvVGV4dCA9IF90KFxyXG4gICAgICAgICAgICAgICAgXCJZb3VyIHJlY292ZXJ5IGtleSBpcyBpbiB5b3VyIDxiPkRvd25sb2FkczwvYj4gZm9sZGVyLlwiLFxyXG4gICAgICAgICAgICAgICAge30sIHtiOiBzID0+IDxiPntzfTwvYj59LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBEaWFsb2dCdXR0b25zID0gc2RrLmdldENvbXBvbmVudCgndmlld3MuZWxlbWVudHMuRGlhbG9nQnV0dG9ucycpO1xyXG4gICAgICAgIHJldHVybiA8ZGl2PlxyXG4gICAgICAgICAgICB7aW50cm9UZXh0fVxyXG4gICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICA8bGk+e190KFwiPGI+UHJpbnQgaXQ8L2I+IGFuZCBzdG9yZSBpdCBzb21ld2hlcmUgc2FmZVwiLCB7fSwge2I6IHMgPT4gPGI+e3N9PC9iPn0pfTwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGk+e190KFwiPGI+U2F2ZSBpdDwvYj4gb24gYSBVU0Iga2V5IG9yIGJhY2t1cCBkcml2ZVwiLCB7fSwge2I6IHMgPT4gPGI+e3N9PC9iPn0pfTwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGk+e190KFwiPGI+Q29weSBpdDwvYj4gdG8geW91ciBwZXJzb25hbCBjbG91ZCBzdG9yYWdlXCIsIHt9LCB7YjogcyA9PiA8Yj57c308L2I+fSl9PC9saT5cclxuICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPERpYWxvZ0J1dHRvbnMgcHJpbWFyeUJ1dHRvbj17X3QoXCJDb250aW51ZVwiKX1cclxuICAgICAgICAgICAgICAgIG9uUHJpbWFyeUJ1dHRvbkNsaWNrPXt0aGlzLl9ib290c3RyYXBTZWNyZXRTdG9yYWdlfVxyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXtmYWxzZX0+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMuX29uS2VlcEl0U2FmZUJhY2tDbGlja30+e190KFwiQmFja1wiKX08L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9EaWFsb2dCdXR0b25zPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICBfcmVuZGVyQnVzeVBoYXNlKCkge1xyXG4gICAgICAgIGNvbnN0IFNwaW5uZXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5TcGlubmVyJyk7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIDxTcGlubmVyIC8+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZURvbmUoKSB7XHJcbiAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICByZXR1cm4gPGRpdj5cclxuICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgXCJZb3UgY2FuIG5vdyB2ZXJpZnkgeW91ciBvdGhlciBkZXZpY2VzLCBcIiArXHJcbiAgICAgICAgICAgICAgICBcImFuZCBvdGhlciB1c2VycyB0byBrZWVwIHlvdXIgY2hhdHMgc2FmZS5cIixcclxuICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgIDxEaWFsb2dCdXR0b25zIHByaW1hcnlCdXR0b249e190KCdPSycpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uRG9uZX1cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9kaXY+O1xyXG4gICAgfVxyXG5cclxuICAgIF9yZW5kZXJQaGFzZVNraXBDb25maXJtKCkge1xyXG4gICAgICAgIGNvbnN0IERpYWxvZ0J1dHRvbnMgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5lbGVtZW50cy5EaWFsb2dCdXR0b25zJyk7XHJcbiAgICAgICAgcmV0dXJuIDxkaXY+XHJcbiAgICAgICAgICAgIHtfdChcclxuICAgICAgICAgICAgICAgIFwiV2l0aG91dCBjb21wbGV0aW5nIHNlY3VyaXR5IG9uIHRoaXMgc2Vzc2lvbiwgaXQgd29u4oCZdCBoYXZlIFwiICtcclxuICAgICAgICAgICAgICAgIFwiYWNjZXNzIHRvIGVuY3J5cHRlZCBtZXNzYWdlcy5cIixcclxuICAgICAgICApfVxyXG4gICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdCgnR28gYmFjaycpfVxyXG4gICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX29uU2V0VXBDbGlja31cclxuICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17ZmFsc2V9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzTmFtZT1cImRhbmdlclwiIG9uQ2xpY2s9e3RoaXMuX29uQ2FuY2VsfT57X3QoJ1NraXAnKX08L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9EaWFsb2dCdXR0b25zPlxyXG4gICAgICAgIDwvZGl2PjtcclxuICAgIH1cclxuXHJcbiAgICBfdGl0bGVGb3JQaGFzZShwaGFzZSkge1xyXG4gICAgICAgIHN3aXRjaCAocGhhc2UpIHtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9NSUdSQVRFOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdVcGdyYWRlIHlvdXIgZW5jcnlwdGlvbicpO1xyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX1BBU1NQSFJBU0U6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ1NldCB1cCBlbmNyeXB0aW9uJyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfUEFTU1BIUkFTRV9DT05GSVJNOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90KCdDb25maXJtIHBhc3NwaHJhc2UnKTtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9DT05GSVJNX1NLSVA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ0FyZSB5b3Ugc3VyZT8nKTtcclxuICAgICAgICAgICAgY2FzZSBQSEFTRV9TSE9XS0VZOlxyXG4gICAgICAgICAgICBjYXNlIFBIQVNFX0tFRVBJVFNBRkU6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3QoJ01ha2UgYSBjb3B5IG9mIHlvdXIgcmVjb3Zlcnkga2V5Jyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfU1RPUklORzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdCgnU2V0dGluZyB1cCBrZXlzJyk7XHJcbiAgICAgICAgICAgIGNhc2UgUEhBU0VfRE9ORTpcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdChcIllvdSdyZSBkb25lIVwiKTtcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIGNvbnN0IEJhc2VEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLkJhc2VEaWFsb2cnKTtcclxuXHJcbiAgICAgICAgbGV0IGNvbnRlbnQ7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3IpIHtcclxuICAgICAgICAgICAgY29uc3QgRGlhbG9nQnV0dG9ucyA9IHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmVsZW1lbnRzLkRpYWxvZ0J1dHRvbnMnKTtcclxuICAgICAgICAgICAgY29udGVudCA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8cD57X3QoXCJVbmFibGUgdG8gc2V0IHVwIHNlY3JldCBzdG9yYWdlXCIpfTwvcD5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfRGlhbG9nX2J1dHRvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8RGlhbG9nQnV0dG9ucyBwcmltYXJ5QnV0dG9uPXtfdCgnUmV0cnknKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25QcmltYXJ5QnV0dG9uQ2xpY2s9e3RoaXMuX2Jvb3RzdHJhcFNlY3JldFN0b3JhZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0NhbmNlbD17dHJ1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9e3RoaXMuX29uQ2FuY2VsfVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAodGhpcy5zdGF0ZS5waGFzZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBQSEFTRV9MT0FESU5HOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJCdXN5UGhhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfTUlHUkFURTpcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gdGhpcy5fcmVuZGVyUGhhc2VNaWdyYXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBIQVNFX1BBU1NQSFJBU0U6XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IHRoaXMuX3JlbmRlclBoYXNlUGFzc1BocmFzZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBQSEFTRV9QQVNTUEhSQVNFX0NPTkZJUk06XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IHRoaXMuX3JlbmRlclBoYXNlUGFzc1BocmFzZUNvbmZpcm0oKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfU0hPV0tFWTpcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gdGhpcy5fcmVuZGVyUGhhc2VTaG93S2V5KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBIQVNFX0tFRVBJVFNBRkU6XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IHRoaXMuX3JlbmRlclBoYXNlS2VlcEl0U2FmZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBQSEFTRV9TVE9SSU5HOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQgPSB0aGlzLl9yZW5kZXJCdXN5UGhhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgUEhBU0VfRE9ORTpcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gdGhpcy5fcmVuZGVyUGhhc2VEb25lKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBIQVNFX0NPTkZJUk1fU0tJUDpcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gdGhpcy5fcmVuZGVyUGhhc2VTa2lwQ29uZmlybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaGVhZGVySW1hZ2U7XHJcbiAgICAgICAgaWYgKHRoaXMuX3RpdGxlRm9yUGhhc2UodGhpcy5zdGF0ZS5waGFzZSkpIHtcclxuICAgICAgICAgICAgaGVhZGVySW1hZ2UgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vcmVzL2ltZy9lMmUvbm9ybWFsLnN2Z1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlRGlhbG9nIGNsYXNzTmFtZT0nbXhfQ3JlYXRlU2VjcmV0U3RvcmFnZURpYWxvZydcclxuICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ9e3RoaXMucHJvcHMub25GaW5pc2hlZH1cclxuICAgICAgICAgICAgICAgIHRpdGxlPXt0aGlzLl90aXRsZUZvclBoYXNlKHRoaXMuc3RhdGUucGhhc2UpfVxyXG4gICAgICAgICAgICAgICAgaGVhZGVySW1hZ2U9e2hlYWRlckltYWdlfVxyXG4gICAgICAgICAgICAgICAgaGFzQ2FuY2VsPXt0aGlzLnByb3BzLmhhc0NhbmNlbCAmJiBbUEhBU0VfUEFTU1BIUkFTRV0uaW5jbHVkZXModGhpcy5zdGF0ZS5waGFzZSl9XHJcbiAgICAgICAgICAgICAgICBmaXhlZFdpZHRoPXtmYWxzZX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAge2NvbnRlbnR9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L0Jhc2VEaWFsb2c+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=