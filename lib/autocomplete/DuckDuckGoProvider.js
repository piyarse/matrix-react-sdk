"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../languageHandler");

var _AutocompleteProvider = _interopRequireDefault(require("./AutocompleteProvider"));

var _Components = require("./Components");

/*
Copyright 2016 Aviral Dasgupta
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const DDG_REGEX = /\/ddg\s+(.+)$/g;
const REFERRER = 'vector';

class DuckDuckGoProvider extends _AutocompleteProvider.default {
  constructor() {
    super(DDG_REGEX);
  }

  static getQueryUri(query
  /*: String*/
  ) {
    return "https://api.duckduckgo.com/?q=".concat(encodeURIComponent(query)) + "&format=json&no_redirect=1&no_html=1&t=".concat(encodeURIComponent(REFERRER));
  }

  async getCompletions(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*: boolean*/
  = false) {
    const {
      command,
      range
    } = this.getCurrentCommand(query, selection);

    if (!query || !command) {
      return [];
    }

    const response = await fetch(DuckDuckGoProvider.getQueryUri(command[1]), {
      method: 'GET'
    });
    const json = await response.json();
    const results = json.Results.map(result => {
      return {
        completion: result.Text,
        component: _react.default.createElement(_Components.TextualCompletion, {
          title: result.Text,
          description: result.Result
        }),
        range
      };
    });

    if (json.Answer) {
      results.unshift({
        completion: json.Answer,
        component: _react.default.createElement(_Components.TextualCompletion, {
          title: json.Answer,
          description: json.AnswerType
        }),
        range
      });
    }

    if (json.RelatedTopics && json.RelatedTopics.length > 0) {
      results.unshift({
        completion: json.RelatedTopics[0].Text,
        component: _react.default.createElement(_Components.TextualCompletion, {
          title: json.RelatedTopics[0].Text
        }),
        range
      });
    }

    if (json.AbstractText) {
      results.unshift({
        completion: json.AbstractText,
        component: _react.default.createElement(_Components.TextualCompletion, {
          title: json.AbstractText
        }),
        range
      });
    }

    return results;
  }

  getName() {
    return '🔍 ' + (0, _languageHandler._t)('Results from DuckDuckGo');
  }

  renderCompletions(completions
  /*: [React.Component]*/
  )
  /*: ?React.Component*/
  {
    return _react.default.createElement("div", {
      className: "mx_Autocomplete_Completion_container_block",
      role: "listbox",
      "aria-label": (0, _languageHandler._t)("DuckDuckGo Results")
    }, completions);
  }

}

exports.default = DuckDuckGoProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvRHVja0R1Y2tHb1Byb3ZpZGVyLmpzIl0sIm5hbWVzIjpbIkRER19SRUdFWCIsIlJFRkVSUkVSIiwiRHVja0R1Y2tHb1Byb3ZpZGVyIiwiQXV0b2NvbXBsZXRlUHJvdmlkZXIiLCJjb25zdHJ1Y3RvciIsImdldFF1ZXJ5VXJpIiwicXVlcnkiLCJlbmNvZGVVUklDb21wb25lbnQiLCJnZXRDb21wbGV0aW9ucyIsInNlbGVjdGlvbiIsImZvcmNlIiwiY29tbWFuZCIsInJhbmdlIiwiZ2V0Q3VycmVudENvbW1hbmQiLCJyZXNwb25zZSIsImZldGNoIiwibWV0aG9kIiwianNvbiIsInJlc3VsdHMiLCJSZXN1bHRzIiwibWFwIiwicmVzdWx0IiwiY29tcGxldGlvbiIsIlRleHQiLCJjb21wb25lbnQiLCJSZXN1bHQiLCJBbnN3ZXIiLCJ1bnNoaWZ0IiwiQW5zd2VyVHlwZSIsIlJlbGF0ZWRUb3BpY3MiLCJsZW5ndGgiLCJBYnN0cmFjdFRleHQiLCJnZXROYW1lIiwicmVuZGVyQ29tcGxldGlvbnMiLCJjb21wbGV0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUVBOztBQXRCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsTUFBTUEsU0FBUyxHQUFHLGdCQUFsQjtBQUNBLE1BQU1DLFFBQVEsR0FBRyxRQUFqQjs7QUFFZSxNQUFNQyxrQkFBTixTQUFpQ0MsNkJBQWpDLENBQXNEO0FBQ2pFQyxFQUFBQSxXQUFXLEdBQUc7QUFDVixVQUFNSixTQUFOO0FBQ0g7O0FBRUQsU0FBT0ssV0FBUCxDQUFtQkM7QUFBbkI7QUFBQSxJQUFrQztBQUM5QixXQUFPLHdDQUFpQ0Msa0JBQWtCLENBQUNELEtBQUQsQ0FBbkQscURBQ3NDQyxrQkFBa0IsQ0FBQ04sUUFBRCxDQUR4RCxDQUFQO0FBRUg7O0FBRUQsUUFBTU8sY0FBTixDQUFxQkY7QUFBckI7QUFBQSxJQUFvQ0c7QUFBcEM7QUFBQSxJQUErREM7QUFBYztBQUFBLElBQUcsS0FBaEYsRUFBdUY7QUFDbkYsVUFBTTtBQUFDQyxNQUFBQSxPQUFEO0FBQVVDLE1BQUFBO0FBQVYsUUFBbUIsS0FBS0MsaUJBQUwsQ0FBdUJQLEtBQXZCLEVBQThCRyxTQUE5QixDQUF6Qjs7QUFDQSxRQUFJLENBQUNILEtBQUQsSUFBVSxDQUFDSyxPQUFmLEVBQXdCO0FBQ3BCLGFBQU8sRUFBUDtBQUNIOztBQUVELFVBQU1HLFFBQVEsR0FBRyxNQUFNQyxLQUFLLENBQUNiLGtCQUFrQixDQUFDRyxXQUFuQixDQUErQk0sT0FBTyxDQUFDLENBQUQsQ0FBdEMsQ0FBRCxFQUE2QztBQUNyRUssTUFBQUEsTUFBTSxFQUFFO0FBRDZELEtBQTdDLENBQTVCO0FBR0EsVUFBTUMsSUFBSSxHQUFHLE1BQU1ILFFBQVEsQ0FBQ0csSUFBVCxFQUFuQjtBQUNBLFVBQU1DLE9BQU8sR0FBR0QsSUFBSSxDQUFDRSxPQUFMLENBQWFDLEdBQWIsQ0FBa0JDLE1BQUQsSUFBWTtBQUN6QyxhQUFPO0FBQ0hDLFFBQUFBLFVBQVUsRUFBRUQsTUFBTSxDQUFDRSxJQURoQjtBQUVIQyxRQUFBQSxTQUFTLEVBQ0wsNkJBQUMsNkJBQUQ7QUFDSSxVQUFBLEtBQUssRUFBRUgsTUFBTSxDQUFDRSxJQURsQjtBQUVJLFVBQUEsV0FBVyxFQUFFRixNQUFNLENBQUNJO0FBRnhCLFVBSEQ7QUFPSGIsUUFBQUE7QUFQRyxPQUFQO0FBU0gsS0FWZSxDQUFoQjs7QUFXQSxRQUFJSyxJQUFJLENBQUNTLE1BQVQsRUFBaUI7QUFDYlIsTUFBQUEsT0FBTyxDQUFDUyxPQUFSLENBQWdCO0FBQ1pMLFFBQUFBLFVBQVUsRUFBRUwsSUFBSSxDQUFDUyxNQURMO0FBRVpGLFFBQUFBLFNBQVMsRUFDTCw2QkFBQyw2QkFBRDtBQUNJLFVBQUEsS0FBSyxFQUFFUCxJQUFJLENBQUNTLE1BRGhCO0FBRUksVUFBQSxXQUFXLEVBQUVULElBQUksQ0FBQ1c7QUFGdEIsVUFIUTtBQU9aaEIsUUFBQUE7QUFQWSxPQUFoQjtBQVNIOztBQUNELFFBQUlLLElBQUksQ0FBQ1ksYUFBTCxJQUFzQlosSUFBSSxDQUFDWSxhQUFMLENBQW1CQyxNQUFuQixHQUE0QixDQUF0RCxFQUF5RDtBQUNyRFosTUFBQUEsT0FBTyxDQUFDUyxPQUFSLENBQWdCO0FBQ1pMLFFBQUFBLFVBQVUsRUFBRUwsSUFBSSxDQUFDWSxhQUFMLENBQW1CLENBQW5CLEVBQXNCTixJQUR0QjtBQUVaQyxRQUFBQSxTQUFTLEVBQ0wsNkJBQUMsNkJBQUQ7QUFDSSxVQUFBLEtBQUssRUFBRVAsSUFBSSxDQUFDWSxhQUFMLENBQW1CLENBQW5CLEVBQXNCTjtBQURqQyxVQUhRO0FBTVpYLFFBQUFBO0FBTlksT0FBaEI7QUFRSDs7QUFDRCxRQUFJSyxJQUFJLENBQUNjLFlBQVQsRUFBdUI7QUFDbkJiLE1BQUFBLE9BQU8sQ0FBQ1MsT0FBUixDQUFnQjtBQUNaTCxRQUFBQSxVQUFVLEVBQUVMLElBQUksQ0FBQ2MsWUFETDtBQUVaUCxRQUFBQSxTQUFTLEVBQ0wsNkJBQUMsNkJBQUQ7QUFDSSxVQUFBLEtBQUssRUFBRVAsSUFBSSxDQUFDYztBQURoQixVQUhRO0FBTVpuQixRQUFBQTtBQU5ZLE9BQWhCO0FBUUg7O0FBQ0QsV0FBT00sT0FBUDtBQUNIOztBQUVEYyxFQUFBQSxPQUFPLEdBQUc7QUFDTixXQUFPLFFBQVEseUJBQUcseUJBQUgsQ0FBZjtBQUNIOztBQUVEQyxFQUFBQSxpQkFBaUIsQ0FBQ0M7QUFBRDtBQUFBO0FBQUE7QUFBbUQ7QUFDaEUsV0FDSTtBQUNJLE1BQUEsU0FBUyxFQUFDLDRDQURkO0FBRUksTUFBQSxJQUFJLEVBQUMsU0FGVDtBQUdJLG9CQUFZLHlCQUFHLG9CQUFIO0FBSGhCLE9BS01BLFdBTE4sQ0FESjtBQVNIOztBQS9FZ0UiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBBdmlyYWwgRGFzZ3VwdGFcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBfdCB9IGZyb20gJy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBBdXRvY29tcGxldGVQcm92aWRlciBmcm9tICcuL0F1dG9jb21wbGV0ZVByb3ZpZGVyJztcclxuXHJcbmltcG9ydCB7VGV4dHVhbENvbXBsZXRpb259IGZyb20gJy4vQ29tcG9uZW50cyc7XHJcbmltcG9ydCB0eXBlIHtTZWxlY3Rpb25SYW5nZX0gZnJvbSBcIi4vQXV0b2NvbXBsZXRlclwiO1xyXG5cclxuY29uc3QgRERHX1JFR0VYID0gL1xcL2RkZ1xccysoLispJC9nO1xyXG5jb25zdCBSRUZFUlJFUiA9ICd2ZWN0b3InO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRHVja0R1Y2tHb1Byb3ZpZGVyIGV4dGVuZHMgQXV0b2NvbXBsZXRlUHJvdmlkZXIge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoRERHX1JFR0VYKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0UXVlcnlVcmkocXVlcnk6IFN0cmluZykge1xyXG4gICAgICAgIHJldHVybiBgaHR0cHM6Ly9hcGkuZHVja2R1Y2tnby5jb20vP3E9JHtlbmNvZGVVUklDb21wb25lbnQocXVlcnkpfWBcclxuICAgICAgICAgKyBgJmZvcm1hdD1qc29uJm5vX3JlZGlyZWN0PTEmbm9faHRtbD0xJnQ9JHtlbmNvZGVVUklDb21wb25lbnQoUkVGRVJSRVIpfWA7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0Q29tcGxldGlvbnMocXVlcnk6IHN0cmluZywgc2VsZWN0aW9uOiBTZWxlY3Rpb25SYW5nZSwgZm9yY2U6IGJvb2xlYW4gPSBmYWxzZSkge1xyXG4gICAgICAgIGNvbnN0IHtjb21tYW5kLCByYW5nZX0gPSB0aGlzLmdldEN1cnJlbnRDb21tYW5kKHF1ZXJ5LCBzZWxlY3Rpb24pO1xyXG4gICAgICAgIGlmICghcXVlcnkgfHwgIWNvbW1hbmQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaChEdWNrRHVja0dvUHJvdmlkZXIuZ2V0UXVlcnlVcmkoY29tbWFuZFsxXSksIHtcclxuICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xyXG4gICAgICAgIGNvbnN0IHJlc3VsdHMgPSBqc29uLlJlc3VsdHMubWFwKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb246IHJlc3VsdC5UZXh0LFxyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFRleHR1YWxDb21wbGV0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtyZXN1bHQuVGV4dH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb249e3Jlc3VsdC5SZXN1bHR9IC8+XHJcbiAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgcmFuZ2UsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGpzb24uQW5zd2VyKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdHMudW5zaGlmdCh7XHJcbiAgICAgICAgICAgICAgICBjb21wbGV0aW9uOiBqc29uLkFuc3dlcixcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxUZXh0dWFsQ29tcGxldGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17anNvbi5BbnN3ZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uPXtqc29uLkFuc3dlclR5cGV9IC8+XHJcbiAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgcmFuZ2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoanNvbi5SZWxhdGVkVG9waWNzICYmIGpzb24uUmVsYXRlZFRvcGljcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdHMudW5zaGlmdCh7XHJcbiAgICAgICAgICAgICAgICBjb21wbGV0aW9uOiBqc29uLlJlbGF0ZWRUb3BpY3NbMF0uVGV4dCxcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxUZXh0dWFsQ29tcGxldGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17anNvbi5SZWxhdGVkVG9waWNzWzBdLlRleHR9IC8+XHJcbiAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgcmFuZ2UsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoanNvbi5BYnN0cmFjdFRleHQpIHtcclxuICAgICAgICAgICAgcmVzdWx0cy51bnNoaWZ0KHtcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb246IGpzb24uQWJzdHJhY3RUZXh0LFxyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFRleHR1YWxDb21wbGV0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtqc29uLkFic3RyYWN0VGV4dH0gLz5cclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICByYW5nZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHRzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuICfwn5SNICcgKyBfdCgnUmVzdWx0cyBmcm9tIER1Y2tEdWNrR28nKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXJDb21wbGV0aW9ucyhjb21wbGV0aW9uczogW1JlYWN0LkNvbXBvbmVudF0pOiA/UmVhY3QuQ29tcG9uZW50IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BdXRvY29tcGxldGVfQ29tcGxldGlvbl9jb250YWluZXJfYmxvY2tcIlxyXG4gICAgICAgICAgICAgICAgcm9sZT1cImxpc3Rib3hcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17X3QoXCJEdWNrRHVja0dvIFJlc3VsdHNcIil9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHsgY29tcGxldGlvbnMgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==