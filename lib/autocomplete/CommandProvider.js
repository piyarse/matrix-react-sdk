"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../languageHandler");

var _AutocompleteProvider = _interopRequireDefault(require("./AutocompleteProvider"));

var _QueryMatcher = _interopRequireDefault(require("./QueryMatcher"));

var _Components = require("./Components");

var _SlashCommands = require("../SlashCommands");

/*
Copyright 2016 Aviral Dasgupta
Copyright 2017 Vector Creations Ltd
Copyright 2017 New Vector Ltd
Copyright 2018 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const COMMAND_RE = /(^\/\w*)(?: .*)?/g;

class CommandProvider extends _AutocompleteProvider.default {
  constructor() {
    super(COMMAND_RE);
    this.matcher = new _QueryMatcher.default(_SlashCommands.Commands, {
      keys: ['command', 'args', 'description'],
      funcs: [({
        aliases
      }) => aliases.join(" ")] // aliases

    });
  }

  async getCompletions(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*: boolean*/
  )
  /*: Array<Completion>*/
  {
    const {
      command,
      range
    } = this.getCurrentCommand(query, selection);
    if (!command) return [];
    let matches = []; // check if the full match differs from the first word (i.e. returns false if the command has args)

    if (command[0] !== command[1]) {
      // The input looks like a command with arguments, perform exact match
      const name = command[1].substr(1); // strip leading `/`

      if (_SlashCommands.CommandMap.has(name)) {
        // some commands, namely `me` and `ddg` don't suit having the usage shown whilst typing their arguments
        if (_SlashCommands.CommandMap.get(name).hideCompletionAfterSpace) return [];
        matches = [_SlashCommands.CommandMap.get(name)];
      }
    } else {
      if (query === '/') {
        // If they have just entered `/` show everything
        matches = _SlashCommands.Commands;
      } else {
        // otherwise fuzzy match against all of the fields
        matches = this.matcher.match(command[1]);
      }
    }

    return matches.map(result => {
      let completion = result.getCommand() + ' ';
      const usedAlias = result.aliases.find(alias => "/".concat(alias) === command[1]); // If the command (or an alias) is the same as the one they entered, we don't want to discard their arguments

      if (usedAlias || result.getCommand() === command[1]) {
        completion = command[0];
      }

      return {
        completion,
        type: "command",
        component: _react.default.createElement(_Components.TextualCompletion, {
          title: "/".concat(usedAlias || result.command),
          subtitle: result.args,
          description: (0, _languageHandler._t)(result.description)
        }),
        range
      };
    });
  }

  getName() {
    return '*️⃣ ' + (0, _languageHandler._t)('Commands');
  }

  renderCompletions(completions
  /*: [React.Component]*/
  )
  /*: ?React.Component*/
  {
    return _react.default.createElement("div", {
      className: "mx_Autocomplete_Completion_container_block",
      role: "listbox",
      "aria-label": (0, _languageHandler._t)("Command Autocomplete")
    }, completions);
  }

}

exports.default = CommandProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvQ29tbWFuZFByb3ZpZGVyLmpzIl0sIm5hbWVzIjpbIkNPTU1BTkRfUkUiLCJDb21tYW5kUHJvdmlkZXIiLCJBdXRvY29tcGxldGVQcm92aWRlciIsImNvbnN0cnVjdG9yIiwibWF0Y2hlciIsIlF1ZXJ5TWF0Y2hlciIsIkNvbW1hbmRzIiwia2V5cyIsImZ1bmNzIiwiYWxpYXNlcyIsImpvaW4iLCJnZXRDb21wbGV0aW9ucyIsInF1ZXJ5Iiwic2VsZWN0aW9uIiwiZm9yY2UiLCJjb21tYW5kIiwicmFuZ2UiLCJnZXRDdXJyZW50Q29tbWFuZCIsIm1hdGNoZXMiLCJuYW1lIiwic3Vic3RyIiwiQ29tbWFuZE1hcCIsImhhcyIsImdldCIsImhpZGVDb21wbGV0aW9uQWZ0ZXJTcGFjZSIsIm1hdGNoIiwibWFwIiwicmVzdWx0IiwiY29tcGxldGlvbiIsImdldENvbW1hbmQiLCJ1c2VkQWxpYXMiLCJmaW5kIiwiYWxpYXMiLCJ0eXBlIiwiY29tcG9uZW50IiwiYXJncyIsImRlc2NyaXB0aW9uIiwiZ2V0TmFtZSIsInJlbmRlckNvbXBsZXRpb25zIiwiY29tcGxldGlvbnMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUF6QkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTJCQSxNQUFNQSxVQUFVLEdBQUcsbUJBQW5COztBQUVlLE1BQU1DLGVBQU4sU0FBOEJDLDZCQUE5QixDQUFtRDtBQUM5REMsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsVUFBTUgsVUFBTjtBQUNBLFNBQUtJLE9BQUwsR0FBZSxJQUFJQyxxQkFBSixDQUFpQkMsdUJBQWpCLEVBQTJCO0FBQ3RDQyxNQUFBQSxJQUFJLEVBQUUsQ0FBQyxTQUFELEVBQVksTUFBWixFQUFvQixhQUFwQixDQURnQztBQUV0Q0MsTUFBQUEsS0FBSyxFQUFFLENBQUMsQ0FBQztBQUFDQyxRQUFBQTtBQUFELE9BQUQsS0FBZUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsR0FBYixDQUFoQixDQUYrQixDQUVLOztBQUZMLEtBQTNCLENBQWY7QUFJSDs7QUFFRCxRQUFNQyxjQUFOLENBQXFCQztBQUFyQjtBQUFBLElBQW9DQztBQUFwQztBQUFBLElBQStEQztBQUEvRDtBQUFBO0FBQUE7QUFBbUc7QUFDL0YsVUFBTTtBQUFDQyxNQUFBQSxPQUFEO0FBQVVDLE1BQUFBO0FBQVYsUUFBbUIsS0FBS0MsaUJBQUwsQ0FBdUJMLEtBQXZCLEVBQThCQyxTQUE5QixDQUF6QjtBQUNBLFFBQUksQ0FBQ0UsT0FBTCxFQUFjLE9BQU8sRUFBUDtBQUVkLFFBQUlHLE9BQU8sR0FBRyxFQUFkLENBSitGLENBSy9GOztBQUNBLFFBQUlILE9BQU8sQ0FBQyxDQUFELENBQVAsS0FBZUEsT0FBTyxDQUFDLENBQUQsQ0FBMUIsRUFBK0I7QUFDM0I7QUFDQSxZQUFNSSxJQUFJLEdBQUdKLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV0ssTUFBWCxDQUFrQixDQUFsQixDQUFiLENBRjJCLENBRVE7O0FBQ25DLFVBQUlDLDBCQUFXQyxHQUFYLENBQWVILElBQWYsQ0FBSixFQUEwQjtBQUN0QjtBQUNBLFlBQUlFLDBCQUFXRSxHQUFYLENBQWVKLElBQWYsRUFBcUJLLHdCQUF6QixFQUFtRCxPQUFPLEVBQVA7QUFDbkROLFFBQUFBLE9BQU8sR0FBRyxDQUFDRywwQkFBV0UsR0FBWCxDQUFlSixJQUFmLENBQUQsQ0FBVjtBQUNIO0FBQ0osS0FSRCxNQVFPO0FBQ0gsVUFBSVAsS0FBSyxLQUFLLEdBQWQsRUFBbUI7QUFDZjtBQUNBTSxRQUFBQSxPQUFPLEdBQUdaLHVCQUFWO0FBQ0gsT0FIRCxNQUdPO0FBQ0g7QUFDQVksUUFBQUEsT0FBTyxHQUFHLEtBQUtkLE9BQUwsQ0FBYXFCLEtBQWIsQ0FBbUJWLE9BQU8sQ0FBQyxDQUFELENBQTFCLENBQVY7QUFDSDtBQUNKOztBQUdELFdBQU9HLE9BQU8sQ0FBQ1EsR0FBUixDQUFhQyxNQUFELElBQVk7QUFDM0IsVUFBSUMsVUFBVSxHQUFHRCxNQUFNLENBQUNFLFVBQVAsS0FBc0IsR0FBdkM7QUFDQSxZQUFNQyxTQUFTLEdBQUdILE1BQU0sQ0FBQ2xCLE9BQVAsQ0FBZXNCLElBQWYsQ0FBb0JDLEtBQUssSUFBSSxXQUFJQSxLQUFKLE1BQWdCakIsT0FBTyxDQUFDLENBQUQsQ0FBcEQsQ0FBbEIsQ0FGMkIsQ0FHM0I7O0FBQ0EsVUFBSWUsU0FBUyxJQUFJSCxNQUFNLENBQUNFLFVBQVAsT0FBd0JkLE9BQU8sQ0FBQyxDQUFELENBQWhELEVBQXFEO0FBQ2pEYSxRQUFBQSxVQUFVLEdBQUdiLE9BQU8sQ0FBQyxDQUFELENBQXBCO0FBQ0g7O0FBRUQsYUFBTztBQUNIYSxRQUFBQSxVQURHO0FBRUhLLFFBQUFBLElBQUksRUFBRSxTQUZIO0FBR0hDLFFBQUFBLFNBQVMsRUFBRSw2QkFBQyw2QkFBRDtBQUNQLFVBQUEsS0FBSyxhQUFNSixTQUFTLElBQUlILE1BQU0sQ0FBQ1osT0FBMUIsQ0FERTtBQUVQLFVBQUEsUUFBUSxFQUFFWSxNQUFNLENBQUNRLElBRlY7QUFHUCxVQUFBLFdBQVcsRUFBRSx5QkFBR1IsTUFBTSxDQUFDUyxXQUFWO0FBSE4sVUFIUjtBQU9IcEIsUUFBQUE7QUFQRyxPQUFQO0FBU0gsS0FqQk0sQ0FBUDtBQWtCSDs7QUFFRHFCLEVBQUFBLE9BQU8sR0FBRztBQUNOLFdBQU8sU0FBUyx5QkFBRyxVQUFILENBQWhCO0FBQ0g7O0FBRURDLEVBQUFBLGlCQUFpQixDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUFtRDtBQUNoRSxXQUNJO0FBQUssTUFBQSxTQUFTLEVBQUMsNENBQWY7QUFBNEQsTUFBQSxJQUFJLEVBQUMsU0FBakU7QUFBMkUsb0JBQVkseUJBQUcsc0JBQUg7QUFBdkYsT0FDTUEsV0FETixDQURKO0FBS0g7O0FBaEU2RCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE2IEF2aXJhbCBEYXNndXB0YVxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge190fSBmcm9tICcuLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgQXV0b2NvbXBsZXRlUHJvdmlkZXIgZnJvbSAnLi9BdXRvY29tcGxldGVQcm92aWRlcic7XHJcbmltcG9ydCBRdWVyeU1hdGNoZXIgZnJvbSAnLi9RdWVyeU1hdGNoZXInO1xyXG5pbXBvcnQge1RleHR1YWxDb21wbGV0aW9ufSBmcm9tICcuL0NvbXBvbmVudHMnO1xyXG5pbXBvcnQgdHlwZSB7Q29tcGxldGlvbiwgU2VsZWN0aW9uUmFuZ2V9IGZyb20gXCIuL0F1dG9jb21wbGV0ZXJcIjtcclxuaW1wb3J0IHtDb21tYW5kcywgQ29tbWFuZE1hcH0gZnJvbSAnLi4vU2xhc2hDb21tYW5kcyc7XHJcblxyXG5jb25zdCBDT01NQU5EX1JFID0gLyheXFwvXFx3KikoPzogLiopPy9nO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29tbWFuZFByb3ZpZGVyIGV4dGVuZHMgQXV0b2NvbXBsZXRlUHJvdmlkZXIge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoQ09NTUFORF9SRSk7XHJcbiAgICAgICAgdGhpcy5tYXRjaGVyID0gbmV3IFF1ZXJ5TWF0Y2hlcihDb21tYW5kcywge1xyXG4gICAgICAgICAgICBrZXlzOiBbJ2NvbW1hbmQnLCAnYXJncycsICdkZXNjcmlwdGlvbiddLFxyXG4gICAgICAgICAgICBmdW5jczogWyh7YWxpYXNlc30pID0+IGFsaWFzZXMuam9pbihcIiBcIildLCAvLyBhbGlhc2VzXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0Q29tcGxldGlvbnMocXVlcnk6IHN0cmluZywgc2VsZWN0aW9uOiBTZWxlY3Rpb25SYW5nZSwgZm9yY2U/OiBib29sZWFuKTogQXJyYXk8Q29tcGxldGlvbj4ge1xyXG4gICAgICAgIGNvbnN0IHtjb21tYW5kLCByYW5nZX0gPSB0aGlzLmdldEN1cnJlbnRDb21tYW5kKHF1ZXJ5LCBzZWxlY3Rpb24pO1xyXG4gICAgICAgIGlmICghY29tbWFuZCkgcmV0dXJuIFtdO1xyXG5cclxuICAgICAgICBsZXQgbWF0Y2hlcyA9IFtdO1xyXG4gICAgICAgIC8vIGNoZWNrIGlmIHRoZSBmdWxsIG1hdGNoIGRpZmZlcnMgZnJvbSB0aGUgZmlyc3Qgd29yZCAoaS5lLiByZXR1cm5zIGZhbHNlIGlmIHRoZSBjb21tYW5kIGhhcyBhcmdzKVxyXG4gICAgICAgIGlmIChjb21tYW5kWzBdICE9PSBjb21tYW5kWzFdKSB7XHJcbiAgICAgICAgICAgIC8vIFRoZSBpbnB1dCBsb29rcyBsaWtlIGEgY29tbWFuZCB3aXRoIGFyZ3VtZW50cywgcGVyZm9ybSBleGFjdCBtYXRjaFxyXG4gICAgICAgICAgICBjb25zdCBuYW1lID0gY29tbWFuZFsxXS5zdWJzdHIoMSk7IC8vIHN0cmlwIGxlYWRpbmcgYC9gXHJcbiAgICAgICAgICAgIGlmIChDb21tYW5kTWFwLmhhcyhuYW1lKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gc29tZSBjb21tYW5kcywgbmFtZWx5IGBtZWAgYW5kIGBkZGdgIGRvbid0IHN1aXQgaGF2aW5nIHRoZSB1c2FnZSBzaG93biB3aGlsc3QgdHlwaW5nIHRoZWlyIGFyZ3VtZW50c1xyXG4gICAgICAgICAgICAgICAgaWYgKENvbW1hbmRNYXAuZ2V0KG5hbWUpLmhpZGVDb21wbGV0aW9uQWZ0ZXJTcGFjZSkgcmV0dXJuIFtdO1xyXG4gICAgICAgICAgICAgICAgbWF0Y2hlcyA9IFtDb21tYW5kTWFwLmdldChuYW1lKV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAocXVlcnkgPT09ICcvJykge1xyXG4gICAgICAgICAgICAgICAgLy8gSWYgdGhleSBoYXZlIGp1c3QgZW50ZXJlZCBgL2Agc2hvdyBldmVyeXRoaW5nXHJcbiAgICAgICAgICAgICAgICBtYXRjaGVzID0gQ29tbWFuZHM7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBvdGhlcndpc2UgZnV6enkgbWF0Y2ggYWdhaW5zdCBhbGwgb2YgdGhlIGZpZWxkc1xyXG4gICAgICAgICAgICAgICAgbWF0Y2hlcyA9IHRoaXMubWF0Y2hlci5tYXRjaChjb21tYW5kWzFdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHJldHVybiBtYXRjaGVzLm1hcCgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBjb21wbGV0aW9uID0gcmVzdWx0LmdldENvbW1hbmQoKSArICcgJztcclxuICAgICAgICAgICAgY29uc3QgdXNlZEFsaWFzID0gcmVzdWx0LmFsaWFzZXMuZmluZChhbGlhcyA9PiBgLyR7YWxpYXN9YCA9PT0gY29tbWFuZFsxXSk7XHJcbiAgICAgICAgICAgIC8vIElmIHRoZSBjb21tYW5kIChvciBhbiBhbGlhcykgaXMgdGhlIHNhbWUgYXMgdGhlIG9uZSB0aGV5IGVudGVyZWQsIHdlIGRvbid0IHdhbnQgdG8gZGlzY2FyZCB0aGVpciBhcmd1bWVudHNcclxuICAgICAgICAgICAgaWYgKHVzZWRBbGlhcyB8fCByZXN1bHQuZ2V0Q29tbWFuZCgpID09PSBjb21tYW5kWzFdKSB7XHJcbiAgICAgICAgICAgICAgICBjb21wbGV0aW9uID0gY29tbWFuZFswXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb24sXHJcbiAgICAgICAgICAgICAgICB0eXBlOiBcImNvbW1hbmRcIixcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogPFRleHR1YWxDb21wbGV0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU9e2AvJHt1c2VkQWxpYXMgfHwgcmVzdWx0LmNvbW1hbmR9YH1cclxuICAgICAgICAgICAgICAgICAgICBzdWJ0aXRsZT17cmVzdWx0LmFyZ3N9XHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb249e190KHJlc3VsdC5kZXNjcmlwdGlvbil9IC8+LFxyXG4gICAgICAgICAgICAgICAgcmFuZ2UsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gJyrvuI/ig6MgJyArIF90KCdDb21tYW5kcycpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlckNvbXBsZXRpb25zKGNvbXBsZXRpb25zOiBbUmVhY3QuQ29tcG9uZW50XSk6ID9SZWFjdC5Db21wb25lbnQge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXhfQXV0b2NvbXBsZXRlX0NvbXBsZXRpb25fY29udGFpbmVyX2Jsb2NrXCIgcm9sZT1cImxpc3Rib3hcIiBhcmlhLWxhYmVsPXtfdChcIkNvbW1hbmQgQXV0b2NvbXBsZXRlXCIpfT5cclxuICAgICAgICAgICAgICAgIHsgY29tcGxldGlvbnMgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==