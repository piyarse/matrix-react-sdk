"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _at2 = _interopRequireDefault(require("lodash/at"));

var _flatMap2 = _interopRequireDefault(require("lodash/flatMap"));

var _sortBy2 = _interopRequireDefault(require("lodash/sortBy"));

var _uniq2 = _interopRequireDefault(require("lodash/uniq"));

/*
Copyright 2017 Aviral Dasgupta
Copyright 2018 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function stripDiacritics(str
/*: string*/
)
/*: string*/
{
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}
/**
 * Simple search matcher that matches any results with the query string anywhere
 * in the search string. Returns matches in the order the query string appears
 * in the search key, earliest first, then in the order the items appeared in
 * the source array.
 *
 * @param {Object[]} objects Initial list of objects. Equivalent to calling
 *     setObjects() after construction
 * @param {Object} options Options object
 * @param {string[]} options.keys List of keys to use as indexes on the objects
 * @param {function[]} options.funcs List of functions that when called with the
 *     object as an arg will return a string to use as an index
 */


class QueryMatcher {
  constructor(objects
  /*: Array<Object>*/
  , options
  /*: {[Object]: Object}*/
  = {}) {
    this._options = options;
    this._keys = options.keys;
    this._funcs = options.funcs || [];
    this.setObjects(objects); // By default, we remove any non-alphanumeric characters ([^A-Za-z0-9_]) from the
    // query and the value being queried before matching

    if (this._options.shouldMatchWordsOnly === undefined) {
      this._options.shouldMatchWordsOnly = true;
    } // By default, match anywhere in the string being searched. If enabled, only return
    // matches that are prefixed with the query.


    if (this._options.shouldMatchPrefix === undefined) {
      this._options.shouldMatchPrefix = false;
    }
  }

  setObjects(objects
  /*: Array<Object>*/
  ) {
    this._items = new Map();

    for (const object of objects) {
      const keyValues = (0, _at2.default)(object, this._keys);

      for (const f of this._funcs) {
        keyValues.push(f(object));
      }

      for (const keyValue of keyValues) {
        if (!keyValue) continue; // skip falsy keyValues

        const key = stripDiacritics(keyValue).toLowerCase();

        if (!this._items.has(key)) {
          this._items.set(key, []);
        }

        this._items.get(key).push(object);
      }
    }
  }

  match(query
  /*: String*/
  )
  /*: Array<Object>*/
  {
    query = stripDiacritics(query).toLowerCase();

    if (this._options.shouldMatchWordsOnly) {
      query = query.replace(/[^\w]/g, '');
    }

    if (query.length === 0) {
      return [];
    }

    const results = []; // Iterate through the map & check each key.
    // ES6 Map iteration order is defined to be insertion order, so results
    // here will come out in the order they were put in.

    for (const key of this._items.keys()) {
      let resultKey = key;

      if (this._options.shouldMatchWordsOnly) {
        resultKey = resultKey.replace(/[^\w]/g, '');
      }

      const index = resultKey.indexOf(query);

      if (index !== -1 && (!this._options.shouldMatchPrefix || index === 0)) {
        results.push({
          key,
          index
        });
      }
    } // Sort them by where the query appeared in the search key
    // lodash sortBy is a stable sort, so results where the query
    // appeared in the same place will retain their order with
    // respect to each other.


    const sortedResults = (0, _sortBy2.default)(results, candidate => {
      return candidate.index;
    }); // Now map the keys to the result objects. Each result object is a list, so
    // flatMap will flatten those lists out into a single list. Also remove any
    // duplicates.

    return (0, _uniq2.default)((0, _flatMap2.default)(sortedResults, candidate => this._items.get(candidate.key)));
  }

}

exports.default = QueryMatcher;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvUXVlcnlNYXRjaGVyLmpzIl0sIm5hbWVzIjpbInN0cmlwRGlhY3JpdGljcyIsInN0ciIsIm5vcm1hbGl6ZSIsInJlcGxhY2UiLCJRdWVyeU1hdGNoZXIiLCJjb25zdHJ1Y3RvciIsIm9iamVjdHMiLCJvcHRpb25zIiwiX29wdGlvbnMiLCJfa2V5cyIsImtleXMiLCJfZnVuY3MiLCJmdW5jcyIsInNldE9iamVjdHMiLCJzaG91bGRNYXRjaFdvcmRzT25seSIsInVuZGVmaW5lZCIsInNob3VsZE1hdGNoUHJlZml4IiwiX2l0ZW1zIiwiTWFwIiwib2JqZWN0Iiwia2V5VmFsdWVzIiwiZiIsInB1c2giLCJrZXlWYWx1ZSIsImtleSIsInRvTG93ZXJDYXNlIiwiaGFzIiwic2V0IiwiZ2V0IiwibWF0Y2giLCJxdWVyeSIsImxlbmd0aCIsInJlc3VsdHMiLCJyZXN1bHRLZXkiLCJpbmRleCIsImluZGV4T2YiLCJzb3J0ZWRSZXN1bHRzIiwiY2FuZGlkYXRlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFtQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQSxTQUFTQSxlQUFULENBQXlCQztBQUF6QjtBQUFBO0FBQUE7QUFBOEM7QUFDMUMsU0FBT0EsR0FBRyxDQUFDQyxTQUFKLENBQWMsS0FBZCxFQUFxQkMsT0FBckIsQ0FBNkIsa0JBQTdCLEVBQWlELEVBQWpELENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7QUFhZSxNQUFNQyxZQUFOLENBQW1CO0FBQzlCQyxFQUFBQSxXQUFXLENBQUNDO0FBQUQ7QUFBQSxJQUF5QkM7QUFBMkI7QUFBQSxJQUFHLEVBQXZELEVBQTJEO0FBQ2xFLFNBQUtDLFFBQUwsR0FBZ0JELE9BQWhCO0FBQ0EsU0FBS0UsS0FBTCxHQUFhRixPQUFPLENBQUNHLElBQXJCO0FBQ0EsU0FBS0MsTUFBTCxHQUFjSixPQUFPLENBQUNLLEtBQVIsSUFBaUIsRUFBL0I7QUFFQSxTQUFLQyxVQUFMLENBQWdCUCxPQUFoQixFQUxrRSxDQU9sRTtBQUNBOztBQUNBLFFBQUksS0FBS0UsUUFBTCxDQUFjTSxvQkFBZCxLQUF1Q0MsU0FBM0MsRUFBc0Q7QUFDbEQsV0FBS1AsUUFBTCxDQUFjTSxvQkFBZCxHQUFxQyxJQUFyQztBQUNILEtBWGlFLENBYWxFO0FBQ0E7OztBQUNBLFFBQUksS0FBS04sUUFBTCxDQUFjUSxpQkFBZCxLQUFvQ0QsU0FBeEMsRUFBbUQ7QUFDL0MsV0FBS1AsUUFBTCxDQUFjUSxpQkFBZCxHQUFrQyxLQUFsQztBQUNIO0FBQ0o7O0FBRURILEVBQUFBLFVBQVUsQ0FBQ1A7QUFBRDtBQUFBLElBQXlCO0FBQy9CLFNBQUtXLE1BQUwsR0FBYyxJQUFJQyxHQUFKLEVBQWQ7O0FBRUEsU0FBSyxNQUFNQyxNQUFYLElBQXFCYixPQUFyQixFQUE4QjtBQUMxQixZQUFNYyxTQUFTLEdBQUcsa0JBQUlELE1BQUosRUFBWSxLQUFLVixLQUFqQixDQUFsQjs7QUFFQSxXQUFLLE1BQU1ZLENBQVgsSUFBZ0IsS0FBS1YsTUFBckIsRUFBNkI7QUFDekJTLFFBQUFBLFNBQVMsQ0FBQ0UsSUFBVixDQUFlRCxDQUFDLENBQUNGLE1BQUQsQ0FBaEI7QUFDSDs7QUFFRCxXQUFLLE1BQU1JLFFBQVgsSUFBdUJILFNBQXZCLEVBQWtDO0FBQzlCLFlBQUksQ0FBQ0csUUFBTCxFQUFlLFNBRGUsQ0FDTDs7QUFDekIsY0FBTUMsR0FBRyxHQUFHeEIsZUFBZSxDQUFDdUIsUUFBRCxDQUFmLENBQTBCRSxXQUExQixFQUFaOztBQUNBLFlBQUksQ0FBQyxLQUFLUixNQUFMLENBQVlTLEdBQVosQ0FBZ0JGLEdBQWhCLENBQUwsRUFBMkI7QUFDdkIsZUFBS1AsTUFBTCxDQUFZVSxHQUFaLENBQWdCSCxHQUFoQixFQUFxQixFQUFyQjtBQUNIOztBQUNELGFBQUtQLE1BQUwsQ0FBWVcsR0FBWixDQUFnQkosR0FBaEIsRUFBcUJGLElBQXJCLENBQTBCSCxNQUExQjtBQUNIO0FBQ0o7QUFDSjs7QUFFRFUsRUFBQUEsS0FBSyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUErQjtBQUNoQ0EsSUFBQUEsS0FBSyxHQUFHOUIsZUFBZSxDQUFDOEIsS0FBRCxDQUFmLENBQXVCTCxXQUF2QixFQUFSOztBQUNBLFFBQUksS0FBS2pCLFFBQUwsQ0FBY00sb0JBQWxCLEVBQXdDO0FBQ3BDZ0IsTUFBQUEsS0FBSyxHQUFHQSxLQUFLLENBQUMzQixPQUFOLENBQWMsUUFBZCxFQUF3QixFQUF4QixDQUFSO0FBQ0g7O0FBQ0QsUUFBSTJCLEtBQUssQ0FBQ0MsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUNwQixhQUFPLEVBQVA7QUFDSDs7QUFDRCxVQUFNQyxPQUFPLEdBQUcsRUFBaEIsQ0FSZ0MsQ0FTaEM7QUFDQTtBQUNBOztBQUNBLFNBQUssTUFBTVIsR0FBWCxJQUFrQixLQUFLUCxNQUFMLENBQVlQLElBQVosRUFBbEIsRUFBc0M7QUFDbEMsVUFBSXVCLFNBQVMsR0FBR1QsR0FBaEI7O0FBQ0EsVUFBSSxLQUFLaEIsUUFBTCxDQUFjTSxvQkFBbEIsRUFBd0M7QUFDcENtQixRQUFBQSxTQUFTLEdBQUdBLFNBQVMsQ0FBQzlCLE9BQVYsQ0FBa0IsUUFBbEIsRUFBNEIsRUFBNUIsQ0FBWjtBQUNIOztBQUNELFlBQU0rQixLQUFLLEdBQUdELFNBQVMsQ0FBQ0UsT0FBVixDQUFrQkwsS0FBbEIsQ0FBZDs7QUFDQSxVQUFJSSxLQUFLLEtBQUssQ0FBQyxDQUFYLEtBQWlCLENBQUMsS0FBSzFCLFFBQUwsQ0FBY1EsaUJBQWYsSUFBb0NrQixLQUFLLEtBQUssQ0FBL0QsQ0FBSixFQUF1RTtBQUNuRUYsUUFBQUEsT0FBTyxDQUFDVixJQUFSLENBQWE7QUFBQ0UsVUFBQUEsR0FBRDtBQUFNVSxVQUFBQTtBQUFOLFNBQWI7QUFDSDtBQUNKLEtBckIrQixDQXVCaEM7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFVBQU1FLGFBQWEsR0FBRyxzQkFBUUosT0FBUixFQUFrQkssU0FBRCxJQUFlO0FBQ2xELGFBQU9BLFNBQVMsQ0FBQ0gsS0FBakI7QUFDSCxLQUZxQixDQUF0QixDQTNCZ0MsQ0ErQmhDO0FBQ0E7QUFDQTs7QUFDQSxXQUFPLG9CQUFNLHVCQUFTRSxhQUFULEVBQXlCQyxTQUFELElBQWUsS0FBS3BCLE1BQUwsQ0FBWVcsR0FBWixDQUFnQlMsU0FBUyxDQUFDYixHQUExQixDQUF2QyxDQUFOLENBQVA7QUFDSDs7QUE3RTZCIiwic291cmNlc0NvbnRlbnQiOlsiLy9AZmxvd1xyXG4vKlxyXG5Db3B5cmlnaHQgMjAxNyBBdmlyYWwgRGFzZ3VwdGFcclxuQ29weXJpZ2h0IDIwMTggTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IF9hdCBmcm9tICdsb2Rhc2gvYXQnO1xyXG5pbXBvcnQgX2ZsYXRNYXAgZnJvbSAnbG9kYXNoL2ZsYXRNYXAnO1xyXG5pbXBvcnQgX3NvcnRCeSBmcm9tICdsb2Rhc2gvc29ydEJ5JztcclxuaW1wb3J0IF91bmlxIGZyb20gJ2xvZGFzaC91bmlxJztcclxuXHJcbmZ1bmN0aW9uIHN0cmlwRGlhY3JpdGljcyhzdHI6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gc3RyLm5vcm1hbGl6ZSgnTkZEJykucmVwbGFjZSgvW1xcdTAzMDAtXFx1MDM2Zl0vZywgJycpO1xyXG59XHJcblxyXG4vKipcclxuICogU2ltcGxlIHNlYXJjaCBtYXRjaGVyIHRoYXQgbWF0Y2hlcyBhbnkgcmVzdWx0cyB3aXRoIHRoZSBxdWVyeSBzdHJpbmcgYW55d2hlcmVcclxuICogaW4gdGhlIHNlYXJjaCBzdHJpbmcuIFJldHVybnMgbWF0Y2hlcyBpbiB0aGUgb3JkZXIgdGhlIHF1ZXJ5IHN0cmluZyBhcHBlYXJzXHJcbiAqIGluIHRoZSBzZWFyY2gga2V5LCBlYXJsaWVzdCBmaXJzdCwgdGhlbiBpbiB0aGUgb3JkZXIgdGhlIGl0ZW1zIGFwcGVhcmVkIGluXHJcbiAqIHRoZSBzb3VyY2UgYXJyYXkuXHJcbiAqXHJcbiAqIEBwYXJhbSB7T2JqZWN0W119IG9iamVjdHMgSW5pdGlhbCBsaXN0IG9mIG9iamVjdHMuIEVxdWl2YWxlbnQgdG8gY2FsbGluZ1xyXG4gKiAgICAgc2V0T2JqZWN0cygpIGFmdGVyIGNvbnN0cnVjdGlvblxyXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyBPcHRpb25zIG9iamVjdFxyXG4gKiBAcGFyYW0ge3N0cmluZ1tdfSBvcHRpb25zLmtleXMgTGlzdCBvZiBrZXlzIHRvIHVzZSBhcyBpbmRleGVzIG9uIHRoZSBvYmplY3RzXHJcbiAqIEBwYXJhbSB7ZnVuY3Rpb25bXX0gb3B0aW9ucy5mdW5jcyBMaXN0IG9mIGZ1bmN0aW9ucyB0aGF0IHdoZW4gY2FsbGVkIHdpdGggdGhlXHJcbiAqICAgICBvYmplY3QgYXMgYW4gYXJnIHdpbGwgcmV0dXJuIGEgc3RyaW5nIHRvIHVzZSBhcyBhbiBpbmRleFxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUXVlcnlNYXRjaGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKG9iamVjdHM6IEFycmF5PE9iamVjdD4sIG9wdGlvbnM6IHtbT2JqZWN0XTogT2JqZWN0fSA9IHt9KSB7XHJcbiAgICAgICAgdGhpcy5fb3B0aW9ucyA9IG9wdGlvbnM7XHJcbiAgICAgICAgdGhpcy5fa2V5cyA9IG9wdGlvbnMua2V5cztcclxuICAgICAgICB0aGlzLl9mdW5jcyA9IG9wdGlvbnMuZnVuY3MgfHwgW107XHJcblxyXG4gICAgICAgIHRoaXMuc2V0T2JqZWN0cyhvYmplY3RzKTtcclxuXHJcbiAgICAgICAgLy8gQnkgZGVmYXVsdCwgd2UgcmVtb3ZlIGFueSBub24tYWxwaGFudW1lcmljIGNoYXJhY3RlcnMgKFteQS1aYS16MC05X10pIGZyb20gdGhlXHJcbiAgICAgICAgLy8gcXVlcnkgYW5kIHRoZSB2YWx1ZSBiZWluZyBxdWVyaWVkIGJlZm9yZSBtYXRjaGluZ1xyXG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLnNob3VsZE1hdGNoV29yZHNPbmx5ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fb3B0aW9ucy5zaG91bGRNYXRjaFdvcmRzT25seSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBCeSBkZWZhdWx0LCBtYXRjaCBhbnl3aGVyZSBpbiB0aGUgc3RyaW5nIGJlaW5nIHNlYXJjaGVkLiBJZiBlbmFibGVkLCBvbmx5IHJldHVyblxyXG4gICAgICAgIC8vIG1hdGNoZXMgdGhhdCBhcmUgcHJlZml4ZWQgd2l0aCB0aGUgcXVlcnkuXHJcbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuc2hvdWxkTWF0Y2hQcmVmaXggPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9vcHRpb25zLnNob3VsZE1hdGNoUHJlZml4ID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldE9iamVjdHMob2JqZWN0czogQXJyYXk8T2JqZWN0Pikge1xyXG4gICAgICAgIHRoaXMuX2l0ZW1zID0gbmV3IE1hcCgpO1xyXG5cclxuICAgICAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBvYmplY3RzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGtleVZhbHVlcyA9IF9hdChvYmplY3QsIHRoaXMuX2tleXMpO1xyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCBmIG9mIHRoaXMuX2Z1bmNzKSB7XHJcbiAgICAgICAgICAgICAgICBrZXlWYWx1ZXMucHVzaChmKG9iamVjdCkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleVZhbHVlIG9mIGtleVZhbHVlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFrZXlWYWx1ZSkgY29udGludWU7IC8vIHNraXAgZmFsc3kga2V5VmFsdWVzXHJcbiAgICAgICAgICAgICAgICBjb25zdCBrZXkgPSBzdHJpcERpYWNyaXRpY3Moa2V5VmFsdWUpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuX2l0ZW1zLmhhcyhrZXkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5faXRlbXMuc2V0KGtleSwgW10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5faXRlbXMuZ2V0KGtleSkucHVzaChvYmplY3QpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG1hdGNoKHF1ZXJ5OiBTdHJpbmcpOiBBcnJheTxPYmplY3Q+IHtcclxuICAgICAgICBxdWVyeSA9IHN0cmlwRGlhY3JpdGljcyhxdWVyeSkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICBpZiAodGhpcy5fb3B0aW9ucy5zaG91bGRNYXRjaFdvcmRzT25seSkge1xyXG4gICAgICAgICAgICBxdWVyeSA9IHF1ZXJ5LnJlcGxhY2UoL1teXFx3XS9nLCAnJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChxdWVyeS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgLy8gSXRlcmF0ZSB0aHJvdWdoIHRoZSBtYXAgJiBjaGVjayBlYWNoIGtleS5cclxuICAgICAgICAvLyBFUzYgTWFwIGl0ZXJhdGlvbiBvcmRlciBpcyBkZWZpbmVkIHRvIGJlIGluc2VydGlvbiBvcmRlciwgc28gcmVzdWx0c1xyXG4gICAgICAgIC8vIGhlcmUgd2lsbCBjb21lIG91dCBpbiB0aGUgb3JkZXIgdGhleSB3ZXJlIHB1dCBpbi5cclxuICAgICAgICBmb3IgKGNvbnN0IGtleSBvZiB0aGlzLl9pdGVtcy5rZXlzKCkpIHtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdEtleSA9IGtleTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuc2hvdWxkTWF0Y2hXb3Jkc09ubHkpIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdEtleSA9IHJlc3VsdEtleS5yZXBsYWNlKC9bXlxcd10vZywgJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gcmVzdWx0S2V5LmluZGV4T2YocXVlcnkpO1xyXG4gICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xICYmICghdGhpcy5fb3B0aW9ucy5zaG91bGRNYXRjaFByZWZpeCB8fCBpbmRleCA9PT0gMCkpIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaCh7a2V5LCBpbmRleH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTb3J0IHRoZW0gYnkgd2hlcmUgdGhlIHF1ZXJ5IGFwcGVhcmVkIGluIHRoZSBzZWFyY2gga2V5XHJcbiAgICAgICAgLy8gbG9kYXNoIHNvcnRCeSBpcyBhIHN0YWJsZSBzb3J0LCBzbyByZXN1bHRzIHdoZXJlIHRoZSBxdWVyeVxyXG4gICAgICAgIC8vIGFwcGVhcmVkIGluIHRoZSBzYW1lIHBsYWNlIHdpbGwgcmV0YWluIHRoZWlyIG9yZGVyIHdpdGhcclxuICAgICAgICAvLyByZXNwZWN0IHRvIGVhY2ggb3RoZXIuXHJcbiAgICAgICAgY29uc3Qgc29ydGVkUmVzdWx0cyA9IF9zb3J0QnkocmVzdWx0cywgKGNhbmRpZGF0ZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gY2FuZGlkYXRlLmluZGV4O1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBOb3cgbWFwIHRoZSBrZXlzIHRvIHRoZSByZXN1bHQgb2JqZWN0cy4gRWFjaCByZXN1bHQgb2JqZWN0IGlzIGEgbGlzdCwgc29cclxuICAgICAgICAvLyBmbGF0TWFwIHdpbGwgZmxhdHRlbiB0aG9zZSBsaXN0cyBvdXQgaW50byBhIHNpbmdsZSBsaXN0LiBBbHNvIHJlbW92ZSBhbnlcclxuICAgICAgICAvLyBkdXBsaWNhdGVzLlxyXG4gICAgICAgIHJldHVybiBfdW5pcShfZmxhdE1hcChzb3J0ZWRSZXN1bHRzLCAoY2FuZGlkYXRlKSA9PiB0aGlzLl9pdGVtcy5nZXQoY2FuZGlkYXRlLmtleSkpKTtcclxuICAgIH1cclxufVxyXG4iXX0=