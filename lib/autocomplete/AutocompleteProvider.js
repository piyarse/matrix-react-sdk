"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

/*
Copyright 2016 Aviral Dasgupta
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
class AutocompleteProvider {
  constructor(commandRegex
  /*: RegExp*/
  , forcedCommandRegex
  /*: RegExp*/
  ) {
    if (commandRegex) {
      if (!commandRegex.global) {
        throw new Error('commandRegex must have global flag set');
      }

      this.commandRegex = commandRegex;
    }

    if (forcedCommandRegex) {
      if (!forcedCommandRegex.global) {
        throw new Error('forcedCommandRegex must have global flag set');
      }

      this.forcedCommandRegex = forcedCommandRegex;
    }
  }

  destroy() {} // stub

  /**
   * Of the matched commands in the query, returns the first that contains or is contained by the selection, or null.
   * @param {string} query The query string
   * @param {SelectionRange} selection Selection to search
   * @param {boolean} force True if the user is forcing completion
   * @return {object} { command, range } where both objects fields are null if no match
   */


  getCurrentCommand(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*: boolean*/
  = false) {
    let commandRegex = this.commandRegex;

    if (force && this.shouldForceComplete()) {
      commandRegex = this.forcedCommandRegex || /\S+/g;
    }

    if (commandRegex == null) {
      return null;
    }

    commandRegex.lastIndex = 0;
    let match;

    while ((match = commandRegex.exec(query)) != null) {
      const start = match.index;
      const end = start + match[0].length;

      if (selection.start <= end && selection.end >= start) {
        return {
          command: match,
          range: {
            start,
            end
          }
        };
      }
    }

    return {
      command: null,
      range: {
        start: -1,
        end: -1
      }
    };
  }

  async getCompletions(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*: boolean*/
  = false)
  /*: Array<Completion>*/
  {
    return [];
  }

  getName()
  /*: string*/
  {
    return 'Default Provider';
  }

  renderCompletions(completions
  /*: [React.Component]*/
  )
  /*: ?React.Component*/
  {
    console.error('stub; should be implemented in subclasses');
    return null;
  } // Whether we should provide completions even if triggered forcefully, without a sigil.


  shouldForceComplete()
  /*: boolean*/
  {
    return false;
  }

}

exports.default = AutocompleteProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvQXV0b2NvbXBsZXRlUHJvdmlkZXIuanMiXSwibmFtZXMiOlsiQXV0b2NvbXBsZXRlUHJvdmlkZXIiLCJjb25zdHJ1Y3RvciIsImNvbW1hbmRSZWdleCIsImZvcmNlZENvbW1hbmRSZWdleCIsImdsb2JhbCIsIkVycm9yIiwiZGVzdHJveSIsImdldEN1cnJlbnRDb21tYW5kIiwicXVlcnkiLCJzZWxlY3Rpb24iLCJmb3JjZSIsInNob3VsZEZvcmNlQ29tcGxldGUiLCJsYXN0SW5kZXgiLCJtYXRjaCIsImV4ZWMiLCJzdGFydCIsImluZGV4IiwiZW5kIiwibGVuZ3RoIiwiY29tbWFuZCIsInJhbmdlIiwiZ2V0Q29tcGxldGlvbnMiLCJnZXROYW1lIiwicmVuZGVyQ29tcGxldGlvbnMiLCJjb21wbGV0aW9ucyIsImNvbnNvbGUiLCJlcnJvciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBa0JBOztBQWxCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQmUsTUFBTUEsb0JBQU4sQ0FBMkI7QUFDdENDLEVBQUFBLFdBQVcsQ0FBQ0M7QUFBRDtBQUFBLElBQXdCQztBQUF4QjtBQUFBLElBQXFEO0FBQzVELFFBQUlELFlBQUosRUFBa0I7QUFDZCxVQUFJLENBQUNBLFlBQVksQ0FBQ0UsTUFBbEIsRUFBMEI7QUFDdEIsY0FBTSxJQUFJQyxLQUFKLENBQVUsd0NBQVYsQ0FBTjtBQUNIOztBQUNELFdBQUtILFlBQUwsR0FBb0JBLFlBQXBCO0FBQ0g7O0FBQ0QsUUFBSUMsa0JBQUosRUFBd0I7QUFDcEIsVUFBSSxDQUFDQSxrQkFBa0IsQ0FBQ0MsTUFBeEIsRUFBZ0M7QUFDNUIsY0FBTSxJQUFJQyxLQUFKLENBQVUsOENBQVYsQ0FBTjtBQUNIOztBQUNELFdBQUtGLGtCQUFMLEdBQTBCQSxrQkFBMUI7QUFDSDtBQUNKOztBQUVERyxFQUFBQSxPQUFPLEdBQUcsQ0FFVCxDQUZNLENBQ0g7O0FBR0o7Ozs7Ozs7OztBQU9BQyxFQUFBQSxpQkFBaUIsQ0FBQ0M7QUFBRDtBQUFBLElBQWdCQztBQUFoQjtBQUFBLElBQTJDQztBQUFjO0FBQUEsSUFBRyxLQUE1RCxFQUFtRTtBQUNoRixRQUFJUixZQUFZLEdBQUcsS0FBS0EsWUFBeEI7O0FBRUEsUUFBSVEsS0FBSyxJQUFJLEtBQUtDLG1CQUFMLEVBQWIsRUFBeUM7QUFDckNULE1BQUFBLFlBQVksR0FBRyxLQUFLQyxrQkFBTCxJQUEyQixNQUExQztBQUNIOztBQUVELFFBQUlELFlBQVksSUFBSSxJQUFwQixFQUEwQjtBQUN0QixhQUFPLElBQVA7QUFDSDs7QUFFREEsSUFBQUEsWUFBWSxDQUFDVSxTQUFiLEdBQXlCLENBQXpCO0FBRUEsUUFBSUMsS0FBSjs7QUFDQSxXQUFPLENBQUNBLEtBQUssR0FBR1gsWUFBWSxDQUFDWSxJQUFiLENBQWtCTixLQUFsQixDQUFULEtBQXNDLElBQTdDLEVBQW1EO0FBQy9DLFlBQU1PLEtBQUssR0FBR0YsS0FBSyxDQUFDRyxLQUFwQjtBQUNBLFlBQU1DLEdBQUcsR0FBR0YsS0FBSyxHQUFHRixLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNLLE1BQTdCOztBQUNBLFVBQUlULFNBQVMsQ0FBQ00sS0FBVixJQUFtQkUsR0FBbkIsSUFBMEJSLFNBQVMsQ0FBQ1EsR0FBVixJQUFpQkYsS0FBL0MsRUFBc0Q7QUFDbEQsZUFBTztBQUNISSxVQUFBQSxPQUFPLEVBQUVOLEtBRE47QUFFSE8sVUFBQUEsS0FBSyxFQUFFO0FBQ0hMLFlBQUFBLEtBREc7QUFFSEUsWUFBQUE7QUFGRztBQUZKLFNBQVA7QUFPSDtBQUNKOztBQUNELFdBQU87QUFDSEUsTUFBQUEsT0FBTyxFQUFFLElBRE47QUFFSEMsTUFBQUEsS0FBSyxFQUFFO0FBQ0hMLFFBQUFBLEtBQUssRUFBRSxDQUFDLENBREw7QUFFSEUsUUFBQUEsR0FBRyxFQUFFLENBQUM7QUFGSDtBQUZKLEtBQVA7QUFPSDs7QUFFRCxRQUFNSSxjQUFOLENBQXFCYjtBQUFyQjtBQUFBLElBQW9DQztBQUFwQztBQUFBLElBQStEQztBQUFjO0FBQUEsSUFBRyxLQUFoRjtBQUFBO0FBQTBHO0FBQ3RHLFdBQU8sRUFBUDtBQUNIOztBQUVEWSxFQUFBQSxPQUFPO0FBQUE7QUFBVztBQUNkLFdBQU8sa0JBQVA7QUFDSDs7QUFFREMsRUFBQUEsaUJBQWlCLENBQUNDO0FBQUQ7QUFBQTtBQUFBO0FBQW1EO0FBQ2hFQyxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYywyQ0FBZDtBQUNBLFdBQU8sSUFBUDtBQUNILEdBMUVxQyxDQTRFdEM7OztBQUNBZixFQUFBQSxtQkFBbUI7QUFBQTtBQUFZO0FBQzNCLFdBQU8sS0FBUDtBQUNIOztBQS9FcUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBBdmlyYWwgRGFzZ3VwdGFcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgdHlwZSB7Q29tcGxldGlvbiwgU2VsZWN0aW9uUmFuZ2V9IGZyb20gJy4vQXV0b2NvbXBsZXRlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBdXRvY29tcGxldGVQcm92aWRlciB7XHJcbiAgICBjb25zdHJ1Y3Rvcihjb21tYW5kUmVnZXg/OiBSZWdFeHAsIGZvcmNlZENvbW1hbmRSZWdleD86IFJlZ0V4cCkge1xyXG4gICAgICAgIGlmIChjb21tYW5kUmVnZXgpIHtcclxuICAgICAgICAgICAgaWYgKCFjb21tYW5kUmVnZXguZ2xvYmFsKSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ2NvbW1hbmRSZWdleCBtdXN0IGhhdmUgZ2xvYmFsIGZsYWcgc2V0Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jb21tYW5kUmVnZXggPSBjb21tYW5kUmVnZXg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmb3JjZWRDb21tYW5kUmVnZXgpIHtcclxuICAgICAgICAgICAgaWYgKCFmb3JjZWRDb21tYW5kUmVnZXguZ2xvYmFsKSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ2ZvcmNlZENvbW1hbmRSZWdleCBtdXN0IGhhdmUgZ2xvYmFsIGZsYWcgc2V0Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5mb3JjZWRDb21tYW5kUmVnZXggPSBmb3JjZWRDb21tYW5kUmVnZXg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRlc3Ryb3koKSB7XHJcbiAgICAgICAgLy8gc3R1YlxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT2YgdGhlIG1hdGNoZWQgY29tbWFuZHMgaW4gdGhlIHF1ZXJ5LCByZXR1cm5zIHRoZSBmaXJzdCB0aGF0IGNvbnRhaW5zIG9yIGlzIGNvbnRhaW5lZCBieSB0aGUgc2VsZWN0aW9uLCBvciBudWxsLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHF1ZXJ5IFRoZSBxdWVyeSBzdHJpbmdcclxuICAgICAqIEBwYXJhbSB7U2VsZWN0aW9uUmFuZ2V9IHNlbGVjdGlvbiBTZWxlY3Rpb24gdG8gc2VhcmNoXHJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IGZvcmNlIFRydWUgaWYgdGhlIHVzZXIgaXMgZm9yY2luZyBjb21wbGV0aW9uXHJcbiAgICAgKiBAcmV0dXJuIHtvYmplY3R9IHsgY29tbWFuZCwgcmFuZ2UgfSB3aGVyZSBib3RoIG9iamVjdHMgZmllbGRzIGFyZSBudWxsIGlmIG5vIG1hdGNoXHJcbiAgICAgKi9cclxuICAgIGdldEN1cnJlbnRDb21tYW5kKHF1ZXJ5OiBzdHJpbmcsIHNlbGVjdGlvbjogU2VsZWN0aW9uUmFuZ2UsIGZvcmNlOiBib29sZWFuID0gZmFsc2UpIHtcclxuICAgICAgICBsZXQgY29tbWFuZFJlZ2V4ID0gdGhpcy5jb21tYW5kUmVnZXg7XHJcblxyXG4gICAgICAgIGlmIChmb3JjZSAmJiB0aGlzLnNob3VsZEZvcmNlQ29tcGxldGUoKSkge1xyXG4gICAgICAgICAgICBjb21tYW5kUmVnZXggPSB0aGlzLmZvcmNlZENvbW1hbmRSZWdleCB8fCAvXFxTKy9nO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbW1hbmRSZWdleCA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29tbWFuZFJlZ2V4Lmxhc3RJbmRleCA9IDA7XHJcblxyXG4gICAgICAgIGxldCBtYXRjaDtcclxuICAgICAgICB3aGlsZSAoKG1hdGNoID0gY29tbWFuZFJlZ2V4LmV4ZWMocXVlcnkpKSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0ID0gbWF0Y2guaW5kZXg7XHJcbiAgICAgICAgICAgIGNvbnN0IGVuZCA9IHN0YXJ0ICsgbWF0Y2hbMF0ubGVuZ3RoO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0aW9uLnN0YXJ0IDw9IGVuZCAmJiBzZWxlY3Rpb24uZW5kID49IHN0YXJ0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbW1hbmQ6IG1hdGNoLFxyXG4gICAgICAgICAgICAgICAgICAgIHJhbmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY29tbWFuZDogbnVsbCxcclxuICAgICAgICAgICAgcmFuZ2U6IHtcclxuICAgICAgICAgICAgICAgIHN0YXJ0OiAtMSxcclxuICAgICAgICAgICAgICAgIGVuZDogLTEsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXRDb21wbGV0aW9ucyhxdWVyeTogc3RyaW5nLCBzZWxlY3Rpb246IFNlbGVjdGlvblJhbmdlLCBmb3JjZTogYm9vbGVhbiA9IGZhbHNlKTogQXJyYXk8Q29tcGxldGlvbj4ge1xyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuICdEZWZhdWx0IFByb3ZpZGVyJztcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXJDb21wbGV0aW9ucyhjb21wbGV0aW9uczogW1JlYWN0LkNvbXBvbmVudF0pOiA/UmVhY3QuQ29tcG9uZW50IHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKCdzdHViOyBzaG91bGQgYmUgaW1wbGVtZW50ZWQgaW4gc3ViY2xhc3NlcycpO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFdoZXRoZXIgd2Ugc2hvdWxkIHByb3ZpZGUgY29tcGxldGlvbnMgZXZlbiBpZiB0cmlnZ2VyZWQgZm9yY2VmdWxseSwgd2l0aG91dCBhIHNpZ2lsLlxyXG4gICAgc2hvdWxkRm9yY2VDb21wbGV0ZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19