"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("../languageHandler");

var _AutocompleteProvider = _interopRequireDefault(require("./AutocompleteProvider"));

var _MatrixClientPeg = require("../MatrixClientPeg");

var _QueryMatcher = _interopRequireDefault(require("./QueryMatcher"));

var _Components = require("./Components");

var sdk = _interopRequireWildcard(require("../index"));

var _sortBy2 = _interopRequireDefault(require("lodash/sortBy"));

var _Permalinks = require("../utils/permalinks/Permalinks");

var _FlairStore = _interopRequireDefault(require("../stores/FlairStore"));

/*
Copyright 2018 New Vector Ltd
Copyright 2018 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const COMMUNITY_REGEX = /\B\+\S*/g;

function score(query, space) {
  const index = space.indexOf(query);

  if (index === -1) {
    return Infinity;
  } else {
    return index;
  }
}

class CommunityProvider extends _AutocompleteProvider.default {
  constructor() {
    super(COMMUNITY_REGEX);
    this.matcher = new _QueryMatcher.default([], {
      keys: ['groupId', 'name', 'shortDescription']
    });
  }

  async getCompletions(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*: boolean*/
  = false)
  /*: Array<Completion>*/
  {
    const BaseAvatar = sdk.getComponent('views.avatars.BaseAvatar'); // Disable autocompletions when composing commands because of various issues
    // (see https://github.com/vector-im/riot-web/issues/4762)

    if (/^(\/join|\/leave)/.test(query)) {
      return [];
    }

    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    let completions = [];
    const {
      command,
      range
    } = this.getCurrentCommand(query, selection, force);

    if (command) {
      const joinedGroups = cli.getGroups().filter(({
        myMembership
      }) => myMembership === 'join');
      const groups = await Promise.all(joinedGroups.map(async ({
        groupId
      }) => {
        try {
          return _FlairStore.default.getGroupProfileCached(cli, groupId);
        } catch (e) {
          // if FlairStore failed, fall back to just groupId
          return Promise.resolve({
            name: '',
            groupId,
            avatarUrl: '',
            shortDescription: ''
          });
        }
      }));
      this.matcher.setObjects(groups);
      const matchedString = command[0];
      completions = this.matcher.match(matchedString);
      completions = (0, _sortBy2.default)(completions, [c => score(matchedString, c.groupId), c => c.groupId.length]).map(({
        avatarUrl,
        groupId,
        name
      }) => ({
        completion: groupId,
        suffix: ' ',
        type: "community",
        href: (0, _Permalinks.makeGroupPermalink)(groupId),
        component: _react.default.createElement(_Components.PillCompletion, {
          initialComponent: _react.default.createElement(BaseAvatar, {
            name: name || groupId,
            width: 24,
            height: 24,
            url: avatarUrl ? cli.mxcUrlToHttp(avatarUrl, 24, 24) : null
          }),
          title: name,
          description: groupId
        }),
        range
      })).slice(0, 4);
    }

    return completions;
  }

  getName() {
    return '💬 ' + (0, _languageHandler._t)('Communities');
  }

  renderCompletions(completions
  /*: [React.Component]*/
  )
  /*: ?React.Component*/
  {
    return _react.default.createElement("div", {
      className: "mx_Autocomplete_Completion_container_pill mx_Autocomplete_Completion_container_truncate",
      role: "listbox",
      "aria-label": (0, _languageHandler._t)("Community Autocomplete")
    }, completions);
  }

}

exports.default = CommunityProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvQ29tbXVuaXR5UHJvdmlkZXIuanMiXSwibmFtZXMiOlsiQ09NTVVOSVRZX1JFR0VYIiwic2NvcmUiLCJxdWVyeSIsInNwYWNlIiwiaW5kZXgiLCJpbmRleE9mIiwiSW5maW5pdHkiLCJDb21tdW5pdHlQcm92aWRlciIsIkF1dG9jb21wbGV0ZVByb3ZpZGVyIiwiY29uc3RydWN0b3IiLCJtYXRjaGVyIiwiUXVlcnlNYXRjaGVyIiwia2V5cyIsImdldENvbXBsZXRpb25zIiwic2VsZWN0aW9uIiwiZm9yY2UiLCJCYXNlQXZhdGFyIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwidGVzdCIsImNsaSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImNvbXBsZXRpb25zIiwiY29tbWFuZCIsInJhbmdlIiwiZ2V0Q3VycmVudENvbW1hbmQiLCJqb2luZWRHcm91cHMiLCJnZXRHcm91cHMiLCJmaWx0ZXIiLCJteU1lbWJlcnNoaXAiLCJncm91cHMiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiZ3JvdXBJZCIsIkZsYWlyU3RvcmUiLCJnZXRHcm91cFByb2ZpbGVDYWNoZWQiLCJlIiwicmVzb2x2ZSIsIm5hbWUiLCJhdmF0YXJVcmwiLCJzaG9ydERlc2NyaXB0aW9uIiwic2V0T2JqZWN0cyIsIm1hdGNoZWRTdHJpbmciLCJtYXRjaCIsImMiLCJsZW5ndGgiLCJjb21wbGV0aW9uIiwic3VmZml4IiwidHlwZSIsImhyZWYiLCJjb21wb25lbnQiLCJteGNVcmxUb0h0dHAiLCJzbGljZSIsImdldE5hbWUiLCJyZW5kZXJDb21wbGV0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBM0JBOzs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBLE1BQU1BLGVBQWUsR0FBRyxVQUF4Qjs7QUFFQSxTQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0JDLEtBQXRCLEVBQTZCO0FBQ3pCLFFBQU1DLEtBQUssR0FBR0QsS0FBSyxDQUFDRSxPQUFOLENBQWNILEtBQWQsQ0FBZDs7QUFDQSxNQUFJRSxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQ2QsV0FBT0UsUUFBUDtBQUNILEdBRkQsTUFFTztBQUNILFdBQU9GLEtBQVA7QUFDSDtBQUNKOztBQUVjLE1BQU1HLGlCQUFOLFNBQWdDQyw2QkFBaEMsQ0FBcUQ7QUFDaEVDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFVBQU1ULGVBQU47QUFDQSxTQUFLVSxPQUFMLEdBQWUsSUFBSUMscUJBQUosQ0FBaUIsRUFBakIsRUFBcUI7QUFDaENDLE1BQUFBLElBQUksRUFBRSxDQUFDLFNBQUQsRUFBWSxNQUFaLEVBQW9CLGtCQUFwQjtBQUQwQixLQUFyQixDQUFmO0FBR0g7O0FBRUQsUUFBTUMsY0FBTixDQUFxQlg7QUFBckI7QUFBQSxJQUFvQ1k7QUFBcEM7QUFBQSxJQUErREM7QUFBYztBQUFBLElBQUcsS0FBaEY7QUFBQTtBQUEwRztBQUN0RyxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkIsQ0FEc0csQ0FHdEc7QUFDQTs7QUFDQSxRQUFJLG9CQUFvQkMsSUFBcEIsQ0FBeUJqQixLQUF6QixDQUFKLEVBQXFDO0FBQ2pDLGFBQU8sRUFBUDtBQUNIOztBQUVELFVBQU1rQixHQUFHLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxRQUFJQyxXQUFXLEdBQUcsRUFBbEI7QUFDQSxVQUFNO0FBQUNDLE1BQUFBLE9BQUQ7QUFBVUMsTUFBQUE7QUFBVixRQUFtQixLQUFLQyxpQkFBTCxDQUF1QnhCLEtBQXZCLEVBQThCWSxTQUE5QixFQUF5Q0MsS0FBekMsQ0FBekI7O0FBQ0EsUUFBSVMsT0FBSixFQUFhO0FBQ1QsWUFBTUcsWUFBWSxHQUFHUCxHQUFHLENBQUNRLFNBQUosR0FBZ0JDLE1BQWhCLENBQXVCLENBQUM7QUFBQ0MsUUFBQUE7QUFBRCxPQUFELEtBQW9CQSxZQUFZLEtBQUssTUFBNUQsQ0FBckI7QUFFQSxZQUFNQyxNQUFNLEdBQUksTUFBTUMsT0FBTyxDQUFDQyxHQUFSLENBQVlOLFlBQVksQ0FBQ08sR0FBYixDQUFpQixPQUFPO0FBQUNDLFFBQUFBO0FBQUQsT0FBUCxLQUFxQjtBQUNwRSxZQUFJO0FBQ0EsaUJBQU9DLG9CQUFXQyxxQkFBWCxDQUFpQ2pCLEdBQWpDLEVBQXNDZSxPQUF0QyxDQUFQO0FBQ0gsU0FGRCxDQUVFLE9BQU9HLENBQVAsRUFBVTtBQUFFO0FBQ1YsaUJBQU9OLE9BQU8sQ0FBQ08sT0FBUixDQUFnQjtBQUNuQkMsWUFBQUEsSUFBSSxFQUFFLEVBRGE7QUFFbkJMLFlBQUFBLE9BRm1CO0FBR25CTSxZQUFBQSxTQUFTLEVBQUUsRUFIUTtBQUluQkMsWUFBQUEsZ0JBQWdCLEVBQUU7QUFKQyxXQUFoQixDQUFQO0FBTUg7QUFDSixPQVhpQyxDQUFaLENBQXRCO0FBYUEsV0FBS2hDLE9BQUwsQ0FBYWlDLFVBQWIsQ0FBd0JaLE1BQXhCO0FBRUEsWUFBTWEsYUFBYSxHQUFHcEIsT0FBTyxDQUFDLENBQUQsQ0FBN0I7QUFDQUQsTUFBQUEsV0FBVyxHQUFHLEtBQUtiLE9BQUwsQ0FBYW1DLEtBQWIsQ0FBbUJELGFBQW5CLENBQWQ7QUFDQXJCLE1BQUFBLFdBQVcsR0FBRyxzQkFBUUEsV0FBUixFQUFxQixDQUM5QnVCLENBQUQsSUFBTzdDLEtBQUssQ0FBQzJDLGFBQUQsRUFBZ0JFLENBQUMsQ0FBQ1gsT0FBbEIsQ0FEbUIsRUFFOUJXLENBQUQsSUFBT0EsQ0FBQyxDQUFDWCxPQUFGLENBQVVZLE1BRmMsQ0FBckIsRUFHWGIsR0FIVyxDQUdQLENBQUM7QUFBQ08sUUFBQUEsU0FBRDtBQUFZTixRQUFBQSxPQUFaO0FBQXFCSyxRQUFBQTtBQUFyQixPQUFELE1BQWlDO0FBQ3BDUSxRQUFBQSxVQUFVLEVBQUViLE9BRHdCO0FBRXBDYyxRQUFBQSxNQUFNLEVBQUUsR0FGNEI7QUFHcENDLFFBQUFBLElBQUksRUFBRSxXQUg4QjtBQUlwQ0MsUUFBQUEsSUFBSSxFQUFFLG9DQUFtQmhCLE9BQW5CLENBSjhCO0FBS3BDaUIsUUFBQUEsU0FBUyxFQUNMLDZCQUFDLDBCQUFEO0FBQWdCLFVBQUEsZ0JBQWdCLEVBQzVCLDZCQUFDLFVBQUQ7QUFBWSxZQUFBLElBQUksRUFBRVosSUFBSSxJQUFJTCxPQUExQjtBQUNZLFlBQUEsS0FBSyxFQUFFLEVBRG5CO0FBQ3VCLFlBQUEsTUFBTSxFQUFFLEVBRC9CO0FBRVksWUFBQSxHQUFHLEVBQUVNLFNBQVMsR0FBR3JCLEdBQUcsQ0FBQ2lDLFlBQUosQ0FBaUJaLFNBQWpCLEVBQTRCLEVBQTVCLEVBQWdDLEVBQWhDLENBQUgsR0FBeUM7QUFGbkUsWUFESjtBQUlFLFVBQUEsS0FBSyxFQUFFRCxJQUpUO0FBSWUsVUFBQSxXQUFXLEVBQUVMO0FBSjVCLFVBTmdDO0FBWXBDVixRQUFBQTtBQVpvQyxPQUFqQyxDQUhPLEVBaUJiNkIsS0FqQmEsQ0FpQlAsQ0FqQk8sRUFpQkosQ0FqQkksQ0FBZDtBQWtCSDs7QUFDRCxXQUFPL0IsV0FBUDtBQUNIOztBQUVEZ0MsRUFBQUEsT0FBTyxHQUFHO0FBQ04sV0FBTyxRQUFRLHlCQUFHLGFBQUgsQ0FBZjtBQUNIOztBQUVEQyxFQUFBQSxpQkFBaUIsQ0FBQ2pDO0FBQUQ7QUFBQTtBQUFBO0FBQW1EO0FBQ2hFLFdBQ0k7QUFDSSxNQUFBLFNBQVMsRUFBQyx5RkFEZDtBQUVJLE1BQUEsSUFBSSxFQUFDLFNBRlQ7QUFHSSxvQkFBWSx5QkFBRyx3QkFBSDtBQUhoQixPQUtNQSxXQUxOLENBREo7QUFTSDs7QUE1RStEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTggTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgQXV0b2NvbXBsZXRlUHJvdmlkZXIgZnJvbSAnLi9BdXRvY29tcGxldGVQcm92aWRlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgUXVlcnlNYXRjaGVyIGZyb20gJy4vUXVlcnlNYXRjaGVyJztcclxuaW1wb3J0IHtQaWxsQ29tcGxldGlvbn0gZnJvbSAnLi9Db21wb25lbnRzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IF9zb3J0QnkgZnJvbSAnbG9kYXNoL3NvcnRCeSc7XHJcbmltcG9ydCB7bWFrZUdyb3VwUGVybWFsaW5rfSBmcm9tIFwiLi4vdXRpbHMvcGVybWFsaW5rcy9QZXJtYWxpbmtzXCI7XHJcbmltcG9ydCB0eXBlIHtDb21wbGV0aW9uLCBTZWxlY3Rpb25SYW5nZX0gZnJvbSBcIi4vQXV0b2NvbXBsZXRlclwiO1xyXG5pbXBvcnQgRmxhaXJTdG9yZSBmcm9tIFwiLi4vc3RvcmVzL0ZsYWlyU3RvcmVcIjtcclxuXHJcbmNvbnN0IENPTU1VTklUWV9SRUdFWCA9IC9cXEJcXCtcXFMqL2c7XHJcblxyXG5mdW5jdGlvbiBzY29yZShxdWVyeSwgc3BhY2UpIHtcclxuICAgIGNvbnN0IGluZGV4ID0gc3BhY2UuaW5kZXhPZihxdWVyeSk7XHJcbiAgICBpZiAoaW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIEluZmluaXR5O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gaW5kZXg7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbW11bml0eVByb3ZpZGVyIGV4dGVuZHMgQXV0b2NvbXBsZXRlUHJvdmlkZXIge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoQ09NTVVOSVRZX1JFR0VYKTtcclxuICAgICAgICB0aGlzLm1hdGNoZXIgPSBuZXcgUXVlcnlNYXRjaGVyKFtdLCB7XHJcbiAgICAgICAgICAgIGtleXM6IFsnZ3JvdXBJZCcsICduYW1lJywgJ3Nob3J0RGVzY3JpcHRpb24nXSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXRDb21wbGV0aW9ucyhxdWVyeTogc3RyaW5nLCBzZWxlY3Rpb246IFNlbGVjdGlvblJhbmdlLCBmb3JjZTogYm9vbGVhbiA9IGZhbHNlKTogQXJyYXk8Q29tcGxldGlvbj4ge1xyXG4gICAgICAgIGNvbnN0IEJhc2VBdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5hdmF0YXJzLkJhc2VBdmF0YXInKTtcclxuXHJcbiAgICAgICAgLy8gRGlzYWJsZSBhdXRvY29tcGxldGlvbnMgd2hlbiBjb21wb3NpbmcgY29tbWFuZHMgYmVjYXVzZSBvZiB2YXJpb3VzIGlzc3Vlc1xyXG4gICAgICAgIC8vIChzZWUgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvNDc2MilcclxuICAgICAgICBpZiAoL14oXFwvam9pbnxcXC9sZWF2ZSkvLnRlc3QocXVlcnkpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBsZXQgY29tcGxldGlvbnMgPSBbXTtcclxuICAgICAgICBjb25zdCB7Y29tbWFuZCwgcmFuZ2V9ID0gdGhpcy5nZXRDdXJyZW50Q29tbWFuZChxdWVyeSwgc2VsZWN0aW9uLCBmb3JjZSk7XHJcbiAgICAgICAgaWYgKGNvbW1hbmQpIHtcclxuICAgICAgICAgICAgY29uc3Qgam9pbmVkR3JvdXBzID0gY2xpLmdldEdyb3VwcygpLmZpbHRlcigoe215TWVtYmVyc2hpcH0pID0+IG15TWVtYmVyc2hpcCA9PT0gJ2pvaW4nKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGdyb3VwcyA9IChhd2FpdCBQcm9taXNlLmFsbChqb2luZWRHcm91cHMubWFwKGFzeW5jICh7Z3JvdXBJZH0pID0+IHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEZsYWlyU3RvcmUuZ2V0R3JvdXBQcm9maWxlQ2FjaGVkKGNsaSwgZ3JvdXBJZCk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7IC8vIGlmIEZsYWlyU3RvcmUgZmFpbGVkLCBmYWxsIGJhY2sgdG8ganVzdCBncm91cElkXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBncm91cElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJVcmw6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaG9ydERlc2NyaXB0aW9uOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkpKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWF0Y2hlci5zZXRPYmplY3RzKGdyb3Vwcyk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBtYXRjaGVkU3RyaW5nID0gY29tbWFuZFswXTtcclxuICAgICAgICAgICAgY29tcGxldGlvbnMgPSB0aGlzLm1hdGNoZXIubWF0Y2gobWF0Y2hlZFN0cmluZyk7XHJcbiAgICAgICAgICAgIGNvbXBsZXRpb25zID0gX3NvcnRCeShjb21wbGV0aW9ucywgW1xyXG4gICAgICAgICAgICAgICAgKGMpID0+IHNjb3JlKG1hdGNoZWRTdHJpbmcsIGMuZ3JvdXBJZCksXHJcbiAgICAgICAgICAgICAgICAoYykgPT4gYy5ncm91cElkLmxlbmd0aCxcclxuICAgICAgICAgICAgXSkubWFwKCh7YXZhdGFyVXJsLCBncm91cElkLCBuYW1lfSkgPT4gKHtcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb246IGdyb3VwSWQsXHJcbiAgICAgICAgICAgICAgICBzdWZmaXg6ICcgJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6IFwiY29tbXVuaXR5XCIsXHJcbiAgICAgICAgICAgICAgICBocmVmOiBtYWtlR3JvdXBQZXJtYWxpbmsoZ3JvdXBJZCksXHJcbiAgICAgICAgICAgICAgICBjb21wb25lbnQ6IChcclxuICAgICAgICAgICAgICAgICAgICA8UGlsbENvbXBsZXRpb24gaW5pdGlhbENvbXBvbmVudD17XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCYXNlQXZhdGFyIG5hbWU9e25hbWUgfHwgZ3JvdXBJZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezI0fSBoZWlnaHQ9ezI0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw9e2F2YXRhclVybCA/IGNsaS5teGNVcmxUb0h0dHAoYXZhdGFyVXJsLCAyNCwgMjQpIDogbnVsbH0gLz5cclxuICAgICAgICAgICAgICAgICAgICB9IHRpdGxlPXtuYW1lfSBkZXNjcmlwdGlvbj17Z3JvdXBJZH0gLz5cclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICByYW5nZSxcclxuICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIC5zbGljZSgwLCA0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNvbXBsZXRpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuICfwn5KsICcgKyBfdCgnQ29tbXVuaXRpZXMnKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXJDb21wbGV0aW9ucyhjb21wbGV0aW9uczogW1JlYWN0LkNvbXBvbmVudF0pOiA/UmVhY3QuQ29tcG9uZW50IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BdXRvY29tcGxldGVfQ29tcGxldGlvbl9jb250YWluZXJfcGlsbCBteF9BdXRvY29tcGxldGVfQ29tcGxldGlvbl9jb250YWluZXJfdHJ1bmNhdGVcIlxyXG4gICAgICAgICAgICAgICAgcm9sZT1cImxpc3Rib3hcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17X3QoXCJDb21tdW5pdHkgQXV0b2NvbXBsZXRlXCIpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7IGNvbXBsZXRpb25zIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=