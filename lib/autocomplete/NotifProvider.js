"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _AutocompleteProvider = _interopRequireDefault(require("./AutocompleteProvider"));

var _languageHandler = require("../languageHandler");

var _MatrixClientPeg = require("../MatrixClientPeg");

var _Components = require("./Components");

var sdk = _interopRequireWildcard(require("../index"));

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const AT_ROOM_REGEX = /@\S*/g;

class NotifProvider extends _AutocompleteProvider.default {
  constructor(room) {
    super(AT_ROOM_REGEX);
    this.room = room;
  }

  async getCompletions(query
  /*: string*/
  , selection
  /*: SelectionRange*/
  , force
  /*:boolean*/
  = false)
  /*: Array<Completion>*/
  {
    const RoomAvatar = sdk.getComponent('views.avatars.RoomAvatar');

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!this.room.currentState.mayTriggerNotifOfType('room', client.credentials.userId)) return [];
    const {
      command,
      range
    } = this.getCurrentCommand(query, selection, force);

    if (command && command[0] && '@room'.startsWith(command[0]) && command[0].length > 1) {
      return [{
        completion: '@room',
        completionId: '@room',
        type: "at-room",
        suffix: ' ',
        component: _react.default.createElement(_Components.PillCompletion, {
          initialComponent: _react.default.createElement(RoomAvatar, {
            width: 24,
            height: 24,
            room: this.room
          }),
          title: "@room",
          description: (0, _languageHandler._t)("Notify the whole room")
        }),
        range
      }];
    }

    return [];
  }

  getName() {
    return '❗️ ' + (0, _languageHandler._t)('Room Notification');
  }

  renderCompletions(completions
  /*: [React.Component]*/
  )
  /*: ?React.Component*/
  {
    return _react.default.createElement("div", {
      className: "mx_Autocomplete_Completion_container_pill mx_Autocomplete_Completion_container_truncate",
      role: "listbox",
      "aria-label": (0, _languageHandler._t)("Notification Autocomplete")
    }, completions);
  }

}

exports.default = NotifProvider;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRvY29tcGxldGUvTm90aWZQcm92aWRlci5qcyJdLCJuYW1lcyI6WyJBVF9ST09NX1JFR0VYIiwiTm90aWZQcm92aWRlciIsIkF1dG9jb21wbGV0ZVByb3ZpZGVyIiwiY29uc3RydWN0b3IiLCJyb29tIiwiZ2V0Q29tcGxldGlvbnMiLCJxdWVyeSIsInNlbGVjdGlvbiIsImZvcmNlIiwiUm9vbUF2YXRhciIsInNkayIsImdldENvbXBvbmVudCIsImNsaWVudCIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImN1cnJlbnRTdGF0ZSIsIm1heVRyaWdnZXJOb3RpZk9mVHlwZSIsImNyZWRlbnRpYWxzIiwidXNlcklkIiwiY29tbWFuZCIsInJhbmdlIiwiZ2V0Q3VycmVudENvbW1hbmQiLCJzdGFydHNXaXRoIiwibGVuZ3RoIiwiY29tcGxldGlvbiIsImNvbXBsZXRpb25JZCIsInR5cGUiLCJzdWZmaXgiLCJjb21wb25lbnQiLCJnZXROYW1lIiwicmVuZGVyQ29tcGxldGlvbnMiLCJjb21wbGV0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBckJBOzs7Ozs7Ozs7Ozs7Ozs7QUF3QkEsTUFBTUEsYUFBYSxHQUFHLE9BQXRCOztBQUVlLE1BQU1DLGFBQU4sU0FBNEJDLDZCQUE1QixDQUFpRDtBQUM1REMsRUFBQUEsV0FBVyxDQUFDQyxJQUFELEVBQU87QUFDZCxVQUFNSixhQUFOO0FBQ0EsU0FBS0ksSUFBTCxHQUFZQSxJQUFaO0FBQ0g7O0FBRUQsUUFBTUMsY0FBTixDQUFxQkM7QUFBckI7QUFBQSxJQUFvQ0M7QUFBcEM7QUFBQSxJQUErREM7QUFBYTtBQUFBLElBQUcsS0FBL0U7QUFBQTtBQUF5RztBQUNyRyxVQUFNQyxVQUFVLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQiwwQkFBakIsQ0FBbkI7O0FBRUEsVUFBTUMsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBRUEsUUFBSSxDQUFDLEtBQUtWLElBQUwsQ0FBVVcsWUFBVixDQUF1QkMscUJBQXZCLENBQTZDLE1BQTdDLEVBQXFESixNQUFNLENBQUNLLFdBQVAsQ0FBbUJDLE1BQXhFLENBQUwsRUFBc0YsT0FBTyxFQUFQO0FBRXRGLFVBQU07QUFBQ0MsTUFBQUEsT0FBRDtBQUFVQyxNQUFBQTtBQUFWLFFBQW1CLEtBQUtDLGlCQUFMLENBQXVCZixLQUF2QixFQUE4QkMsU0FBOUIsRUFBeUNDLEtBQXpDLENBQXpCOztBQUNBLFFBQUlXLE9BQU8sSUFBSUEsT0FBTyxDQUFDLENBQUQsQ0FBbEIsSUFBeUIsUUFBUUcsVUFBUixDQUFtQkgsT0FBTyxDQUFDLENBQUQsQ0FBMUIsQ0FBekIsSUFBMkRBLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV0ksTUFBWCxHQUFvQixDQUFuRixFQUFzRjtBQUNsRixhQUFPLENBQUM7QUFDSkMsUUFBQUEsVUFBVSxFQUFFLE9BRFI7QUFFSkMsUUFBQUEsWUFBWSxFQUFFLE9BRlY7QUFHSkMsUUFBQUEsSUFBSSxFQUFFLFNBSEY7QUFJSkMsUUFBQUEsTUFBTSxFQUFFLEdBSko7QUFLSkMsUUFBQUEsU0FBUyxFQUNMLDZCQUFDLDBCQUFEO0FBQWdCLFVBQUEsZ0JBQWdCLEVBQUUsNkJBQUMsVUFBRDtBQUFZLFlBQUEsS0FBSyxFQUFFLEVBQW5CO0FBQXVCLFlBQUEsTUFBTSxFQUFFLEVBQS9CO0FBQW1DLFlBQUEsSUFBSSxFQUFFLEtBQUt4QjtBQUE5QyxZQUFsQztBQUEwRixVQUFBLEtBQUssRUFBQyxPQUFoRztBQUF3RyxVQUFBLFdBQVcsRUFBRSx5QkFBRyx1QkFBSDtBQUFySCxVQU5BO0FBUUpnQixRQUFBQTtBQVJJLE9BQUQsQ0FBUDtBQVVIOztBQUNELFdBQU8sRUFBUDtBQUNIOztBQUVEUyxFQUFBQSxPQUFPLEdBQUc7QUFDTixXQUFPLFFBQVEseUJBQUcsbUJBQUgsQ0FBZjtBQUNIOztBQUVEQyxFQUFBQSxpQkFBaUIsQ0FBQ0M7QUFBRDtBQUFBO0FBQUE7QUFBbUQ7QUFDaEUsV0FDSTtBQUNJLE1BQUEsU0FBUyxFQUFDLHlGQURkO0FBRUksTUFBQSxJQUFJLEVBQUMsU0FGVDtBQUdJLG9CQUFZLHlCQUFHLDJCQUFIO0FBSGhCLE9BS01BLFdBTE4sQ0FESjtBQVNIOztBQTNDMkQiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBBdXRvY29tcGxldGVQcm92aWRlciBmcm9tICcuL0F1dG9jb21wbGV0ZVByb3ZpZGVyJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IHtQaWxsQ29tcGxldGlvbn0gZnJvbSAnLi9Db21wb25lbnRzJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHR5cGUge0NvbXBsZXRpb24sIFNlbGVjdGlvblJhbmdlfSBmcm9tIFwiLi9BdXRvY29tcGxldGVyXCI7XHJcblxyXG5jb25zdCBBVF9ST09NX1JFR0VYID0gL0BcXFMqL2c7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOb3RpZlByb3ZpZGVyIGV4dGVuZHMgQXV0b2NvbXBsZXRlUHJvdmlkZXIge1xyXG4gICAgY29uc3RydWN0b3Iocm9vbSkge1xyXG4gICAgICAgIHN1cGVyKEFUX1JPT01fUkVHRVgpO1xyXG4gICAgICAgIHRoaXMucm9vbSA9IHJvb207XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0Q29tcGxldGlvbnMocXVlcnk6IHN0cmluZywgc2VsZWN0aW9uOiBTZWxlY3Rpb25SYW5nZSwgZm9yY2U6Ym9vbGVhbiA9IGZhbHNlKTogQXJyYXk8Q29tcGxldGlvbj4ge1xyXG4gICAgICAgIGNvbnN0IFJvb21BdmF0YXIgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5hdmF0YXJzLlJvb21BdmF0YXInKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMucm9vbS5jdXJyZW50U3RhdGUubWF5VHJpZ2dlck5vdGlmT2ZUeXBlKCdyb29tJywgY2xpZW50LmNyZWRlbnRpYWxzLnVzZXJJZCkpIHJldHVybiBbXTtcclxuXHJcbiAgICAgICAgY29uc3Qge2NvbW1hbmQsIHJhbmdlfSA9IHRoaXMuZ2V0Q3VycmVudENvbW1hbmQocXVlcnksIHNlbGVjdGlvbiwgZm9yY2UpO1xyXG4gICAgICAgIGlmIChjb21tYW5kICYmIGNvbW1hbmRbMF0gJiYgJ0Byb29tJy5zdGFydHNXaXRoKGNvbW1hbmRbMF0pICYmIGNvbW1hbmRbMF0ubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICByZXR1cm4gW3tcclxuICAgICAgICAgICAgICAgIGNvbXBsZXRpb246ICdAcm9vbScsXHJcbiAgICAgICAgICAgICAgICBjb21wbGV0aW9uSWQ6ICdAcm9vbScsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiBcImF0LXJvb21cIixcclxuICAgICAgICAgICAgICAgIHN1ZmZpeDogJyAnLFxyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFBpbGxDb21wbGV0aW9uIGluaXRpYWxDb21wb25lbnQ9ezxSb29tQXZhdGFyIHdpZHRoPXsyNH0gaGVpZ2h0PXsyNH0gcm9vbT17dGhpcy5yb29tfSAvPn0gdGl0bGU9XCJAcm9vbVwiIGRlc2NyaXB0aW9uPXtfdChcIk5vdGlmeSB0aGUgd2hvbGUgcm9vbVwiKX0gLz5cclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICByYW5nZSxcclxuICAgICAgICAgICAgfV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROYW1lKCkge1xyXG4gICAgICAgIHJldHVybiAn4p2X77iPICcgKyBfdCgnUm9vbSBOb3RpZmljYXRpb24nKTtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXJDb21wbGV0aW9ucyhjb21wbGV0aW9uczogW1JlYWN0LkNvbXBvbmVudF0pOiA/UmVhY3QuQ29tcG9uZW50IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJteF9BdXRvY29tcGxldGVfQ29tcGxldGlvbl9jb250YWluZXJfcGlsbCBteF9BdXRvY29tcGxldGVfQ29tcGxldGlvbl9jb250YWluZXJfdHJ1bmNhdGVcIlxyXG4gICAgICAgICAgICAgICAgcm9sZT1cImxpc3Rib3hcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD17X3QoXCJOb3RpZmljYXRpb24gQXV0b2NvbXBsZXRlXCIpfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7IGNvbXBsZXRpb25zIH1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=