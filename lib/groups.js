"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.groupMemberFromApiObject = groupMemberFromApiObject;
exports.groupRoomFromApiObject = groupRoomFromApiObject;
exports.GroupRoomType = exports.GroupMemberType = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _languageHandler = require("./languageHandler.js");

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const GroupMemberType = _propTypes.default.shape({
  userId: _propTypes.default.string.isRequired,
  displayname: _propTypes.default.string,
  avatarUrl: _propTypes.default.string
});

exports.GroupMemberType = GroupMemberType;

const GroupRoomType = _propTypes.default.shape({
  displayname: _propTypes.default.string,
  name: _propTypes.default.string,
  roomId: _propTypes.default.string.isRequired,
  canonicalAlias: _propTypes.default.string,
  avatarUrl: _propTypes.default.string
});

exports.GroupRoomType = GroupRoomType;

function groupMemberFromApiObject(apiObject) {
  return {
    userId: apiObject.user_id,
    displayname: apiObject.displayname,
    avatarUrl: apiObject.avatar_url,
    isPrivileged: apiObject.is_privileged
  };
}

function groupRoomFromApiObject(apiObject) {
  return {
    displayname: apiObject.name || apiObject.canonical_alias || (0, _languageHandler._t)("Unnamed Room"),
    name: apiObject.name,
    roomId: apiObject.room_id,
    canonicalAlias: apiObject.canonical_alias,
    avatarUrl: apiObject.avatar_url,
    topic: apiObject.topic,
    numJoinedMembers: apiObject.num_joined_members,
    worldReadable: apiObject.world_readable,
    guestCanJoin: apiObject.guest_can_join,
    isPublic: apiObject.is_public !== false
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9ncm91cHMuanMiXSwibmFtZXMiOlsiR3JvdXBNZW1iZXJUeXBlIiwiUHJvcFR5cGVzIiwic2hhcGUiLCJ1c2VySWQiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwiZGlzcGxheW5hbWUiLCJhdmF0YXJVcmwiLCJHcm91cFJvb21UeXBlIiwibmFtZSIsInJvb21JZCIsImNhbm9uaWNhbEFsaWFzIiwiZ3JvdXBNZW1iZXJGcm9tQXBpT2JqZWN0IiwiYXBpT2JqZWN0IiwidXNlcl9pZCIsImF2YXRhcl91cmwiLCJpc1ByaXZpbGVnZWQiLCJpc19wcml2aWxlZ2VkIiwiZ3JvdXBSb29tRnJvbUFwaU9iamVjdCIsImNhbm9uaWNhbF9hbGlhcyIsInJvb21faWQiLCJ0b3BpYyIsIm51bUpvaW5lZE1lbWJlcnMiLCJudW1fam9pbmVkX21lbWJlcnMiLCJ3b3JsZFJlYWRhYmxlIiwid29ybGRfcmVhZGFibGUiLCJndWVzdENhbkpvaW4iLCJndWVzdF9jYW5fam9pbiIsImlzUHVibGljIiwiaXNfcHVibGljIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFqQkE7Ozs7Ozs7Ozs7Ozs7OztBQW1CTyxNQUFNQSxlQUFlLEdBQUdDLG1CQUFVQyxLQUFWLENBQWdCO0FBQzNDQyxFQUFBQSxNQUFNLEVBQUVGLG1CQUFVRyxNQUFWLENBQWlCQyxVQURrQjtBQUUzQ0MsRUFBQUEsV0FBVyxFQUFFTCxtQkFBVUcsTUFGb0I7QUFHM0NHLEVBQUFBLFNBQVMsRUFBRU4sbUJBQVVHO0FBSHNCLENBQWhCLENBQXhCOzs7O0FBTUEsTUFBTUksYUFBYSxHQUFHUCxtQkFBVUMsS0FBVixDQUFnQjtBQUN6Q0ksRUFBQUEsV0FBVyxFQUFFTCxtQkFBVUcsTUFEa0I7QUFFekNLLEVBQUFBLElBQUksRUFBRVIsbUJBQVVHLE1BRnlCO0FBR3pDTSxFQUFBQSxNQUFNLEVBQUVULG1CQUFVRyxNQUFWLENBQWlCQyxVQUhnQjtBQUl6Q00sRUFBQUEsY0FBYyxFQUFFVixtQkFBVUcsTUFKZTtBQUt6Q0csRUFBQUEsU0FBUyxFQUFFTixtQkFBVUc7QUFMb0IsQ0FBaEIsQ0FBdEI7Ozs7QUFRQSxTQUFTUSx3QkFBVCxDQUFrQ0MsU0FBbEMsRUFBNkM7QUFDaEQsU0FBTztBQUNIVixJQUFBQSxNQUFNLEVBQUVVLFNBQVMsQ0FBQ0MsT0FEZjtBQUVIUixJQUFBQSxXQUFXLEVBQUVPLFNBQVMsQ0FBQ1AsV0FGcEI7QUFHSEMsSUFBQUEsU0FBUyxFQUFFTSxTQUFTLENBQUNFLFVBSGxCO0FBSUhDLElBQUFBLFlBQVksRUFBRUgsU0FBUyxDQUFDSTtBQUpyQixHQUFQO0FBTUg7O0FBRU0sU0FBU0Msc0JBQVQsQ0FBZ0NMLFNBQWhDLEVBQTJDO0FBQzlDLFNBQU87QUFDSFAsSUFBQUEsV0FBVyxFQUFFTyxTQUFTLENBQUNKLElBQVYsSUFBa0JJLFNBQVMsQ0FBQ00sZUFBNUIsSUFBK0MseUJBQUcsY0FBSCxDQUR6RDtBQUVIVixJQUFBQSxJQUFJLEVBQUVJLFNBQVMsQ0FBQ0osSUFGYjtBQUdIQyxJQUFBQSxNQUFNLEVBQUVHLFNBQVMsQ0FBQ08sT0FIZjtBQUlIVCxJQUFBQSxjQUFjLEVBQUVFLFNBQVMsQ0FBQ00sZUFKdkI7QUFLSFosSUFBQUEsU0FBUyxFQUFFTSxTQUFTLENBQUNFLFVBTGxCO0FBTUhNLElBQUFBLEtBQUssRUFBRVIsU0FBUyxDQUFDUSxLQU5kO0FBT0hDLElBQUFBLGdCQUFnQixFQUFFVCxTQUFTLENBQUNVLGtCQVB6QjtBQVFIQyxJQUFBQSxhQUFhLEVBQUVYLFNBQVMsQ0FBQ1ksY0FSdEI7QUFTSEMsSUFBQUEsWUFBWSxFQUFFYixTQUFTLENBQUNjLGNBVHJCO0FBVUhDLElBQUFBLFFBQVEsRUFBRWYsU0FBUyxDQUFDZ0IsU0FBVixLQUF3QjtBQVYvQixHQUFQO0FBWUgiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi9sYW5ndWFnZUhhbmRsZXIuanMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IEdyb3VwTWVtYmVyVHlwZSA9IFByb3BUeXBlcy5zaGFwZSh7XHJcbiAgICB1c2VySWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcclxuICAgIGRpc3BsYXluYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgYXZhdGFyVXJsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG59KTtcclxuXHJcbmV4cG9ydCBjb25zdCBHcm91cFJvb21UeXBlID0gUHJvcFR5cGVzLnNoYXBlKHtcclxuICAgIGRpc3BsYXluYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgbmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIHJvb21JZDogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxyXG4gICAgY2Fub25pY2FsQWxpYXM6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICBhdmF0YXJVcmw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbn0pO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdyb3VwTWVtYmVyRnJvbUFwaU9iamVjdChhcGlPYmplY3QpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdXNlcklkOiBhcGlPYmplY3QudXNlcl9pZCxcclxuICAgICAgICBkaXNwbGF5bmFtZTogYXBpT2JqZWN0LmRpc3BsYXluYW1lLFxyXG4gICAgICAgIGF2YXRhclVybDogYXBpT2JqZWN0LmF2YXRhcl91cmwsXHJcbiAgICAgICAgaXNQcml2aWxlZ2VkOiBhcGlPYmplY3QuaXNfcHJpdmlsZWdlZCxcclxuICAgIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBncm91cFJvb21Gcm9tQXBpT2JqZWN0KGFwaU9iamVjdCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBkaXNwbGF5bmFtZTogYXBpT2JqZWN0Lm5hbWUgfHwgYXBpT2JqZWN0LmNhbm9uaWNhbF9hbGlhcyB8fCBfdChcIlVubmFtZWQgUm9vbVwiKSxcclxuICAgICAgICBuYW1lOiBhcGlPYmplY3QubmFtZSxcclxuICAgICAgICByb29tSWQ6IGFwaU9iamVjdC5yb29tX2lkLFxyXG4gICAgICAgIGNhbm9uaWNhbEFsaWFzOiBhcGlPYmplY3QuY2Fub25pY2FsX2FsaWFzLFxyXG4gICAgICAgIGF2YXRhclVybDogYXBpT2JqZWN0LmF2YXRhcl91cmwsXHJcbiAgICAgICAgdG9waWM6IGFwaU9iamVjdC50b3BpYyxcclxuICAgICAgICBudW1Kb2luZWRNZW1iZXJzOiBhcGlPYmplY3QubnVtX2pvaW5lZF9tZW1iZXJzLFxyXG4gICAgICAgIHdvcmxkUmVhZGFibGU6IGFwaU9iamVjdC53b3JsZF9yZWFkYWJsZSxcclxuICAgICAgICBndWVzdENhbkpvaW46IGFwaU9iamVjdC5ndWVzdF9jYW5fam9pbixcclxuICAgICAgICBpc1B1YmxpYzogYXBpT2JqZWN0LmlzX3B1YmxpYyAhPT0gZmFsc2UsXHJcbiAgICB9O1xyXG59XHJcbiJdfQ==