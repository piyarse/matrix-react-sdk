"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var _utils = require("flux/utils");

var _MatrixClientPeg = require("../MatrixClientPeg");

var sdk = _interopRequireWildcard(require("../index"));

var _Modal = _interopRequireDefault(require("../Modal"));

var _languageHandler = require("../languageHandler");

var _RoomAliasCache = require("../RoomAliasCache");

/*
Copyright 2017 Vector Creations Ltd
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const INITIAL_STATE = {
  // Whether we're joining the currently viewed room (see isJoining())
  joining: false,
  // Any error that has occurred during joining
  joinError: null,
  // The room ID of the room currently being viewed
  roomId: null,
  // The event to scroll to when the room is first viewed
  initialEventId: null,
  // Whether to highlight the initial event
  isInitialEventHighlighted: false,
  // The room alias of the room (or null if not originally specified in view_room)
  roomAlias: null,
  // Whether the current room is loading
  roomLoading: false,
  // Any error that has occurred during loading
  roomLoadError: null,
  forwardingEvent: null,
  quotingEvent: null,
  matrixClientIsReady: false
};
/**
 * A class for storing application state for RoomView. This is the RoomView's interface
*  with a subset of the js-sdk.
 *  ```
 */

class RoomViewStore extends _utils.Store {
  constructor() {
    super(_dispatcher.default); // Initialise state

    this._state = INITIAL_STATE;

    if (_MatrixClientPeg.MatrixClientPeg.get()) {
      this._state.matrixClientIsReady = _MatrixClientPeg.MatrixClientPeg.get().isInitialSyncComplete();
    }
  }

  _setState(newState) {
    // If values haven't changed, there's nothing to do.
    // This only tries a shallow comparison, so unchanged objects will slip
    // through, but that's probably okay for now.
    let stateChanged = false;

    for (const key of Object.keys(newState)) {
      if (this._state[key] !== newState[key]) {
        stateChanged = true;
        break;
      }
    }

    if (!stateChanged) {
      return;
    }

    this._state = Object.assign(this._state, newState);

    this.__emitChange();
  }

  __onDispatch(payload) {
    switch (payload.action) {
      // view_room:
      //      - room_alias:   '#somealias:matrix.org'
      //      - room_id:      '!roomid123:matrix.org'
      //      - event_id:     '$213456782:matrix.org'
      //      - event_offset: 100
      //      - highlighted:  true
      case 'view_room':
        this._viewRoom(payload);

        break;

      case 'view_my_groups':
      case 'view_group':
        this._setState({
          roomId: null,
          roomAlias: null
        });

        break;

      case 'view_room_error':
        this._viewRoomError(payload);

        break;

      case 'will_join':
        this._setState({
          joining: true
        });

        break;

      case 'cancel_join':
        this._setState({
          joining: false
        });

        break;
      // join_room:
      //      - opts: options for joinRoom

      case 'join_room':
        this._joinRoom(payload);

        break;

      case 'join_room_error':
        this._joinRoomError(payload);

        break;

      case 'join_room_ready':
        this._setState({
          shouldPeek: false
        });

        break;

      case 'on_client_not_viable':
      case 'on_logged_out':
        this.reset();
        break;

      case 'forward_event':
        this._setState({
          forwardingEvent: payload.event
        });

        break;

      case 'reply_to_event':
        // If currently viewed room does not match the room in which we wish to reply then change rooms
        // this can happen when performing a search across all rooms
        if (payload.event && payload.event.getRoomId() !== this._state.roomId) {
          _dispatcher.default.dispatch({
            action: 'view_room',
            room_id: payload.event.getRoomId(),
            replyingToEvent: payload.event
          });
        } else {
          this._setState({
            replyingToEvent: payload.event
          });
        }

        break;

      case 'open_room_settings':
        {
          const RoomSettingsDialog = sdk.getComponent("dialogs.RoomSettingsDialog");

          _Modal.default.createTrackedDialog('Room settings', '', RoomSettingsDialog, {
            roomId: payload.room_id || this._state.roomId
          },
          /*className=*/
          null,
          /*isPriority=*/
          false,
          /*isStatic=*/
          true);

          break;
        }

      case 'sync_state':
        this._setState({
          matrixClientIsReady: _MatrixClientPeg.MatrixClientPeg.get().isInitialSyncComplete()
        });

        break;
    }
  }

  async _viewRoom(payload) {
    if (payload.room_id) {
      const newState = {
        roomId: payload.room_id,
        roomAlias: payload.room_alias,
        initialEventId: payload.event_id,
        isInitialEventHighlighted: payload.highlighted,
        forwardingEvent: null,
        roomLoading: false,
        roomLoadError: null,
        // should peek by default
        shouldPeek: payload.should_peek === undefined ? true : payload.should_peek,
        // have we sent a join request for this room and are waiting for a response?
        joining: payload.joining || false,
        // Reset replyingToEvent because we don't want cross-room because bad UX
        replyingToEvent: null,
        // pull the user out of Room Settings
        isEditingSettings: false
      }; // Allow being given an event to be replied to when switching rooms but sanity check its for this room

      if (payload.replyingToEvent && payload.replyingToEvent.getRoomId() === payload.room_id) {
        newState.replyingToEvent = payload.replyingToEvent;
      }

      if (this._state.forwardingEvent) {
        _dispatcher.default.dispatch({
          action: 'send_event',
          room_id: newState.roomId,
          event: this._state.forwardingEvent
        });
      }

      this._setState(newState);

      if (payload.auto_join) {
        this._joinRoom(payload);
      }
    } else if (payload.room_alias) {
      // Try the room alias to room ID navigation cache first to avoid
      // blocking room navigation on the homeserver.
      let roomId = (0, _RoomAliasCache.getCachedRoomIDForAlias)(payload.room_alias);

      if (!roomId) {
        // Room alias cache miss, so let's ask the homeserver. Resolve the alias
        // and then do a second dispatch with the room ID acquired.
        this._setState({
          roomId: null,
          initialEventId: null,
          initialEventPixelOffset: null,
          isInitialEventHighlighted: null,
          roomAlias: payload.room_alias,
          roomLoading: true,
          roomLoadError: null
        });

        try {
          const result = await _MatrixClientPeg.MatrixClientPeg.get().getRoomIdForAlias(payload.room_alias);
          (0, _RoomAliasCache.storeRoomAliasInCache)(payload.room_alias, result.room_id);
          roomId = result.room_id;
        } catch (err) {
          _dispatcher.default.dispatch({
            action: 'view_room_error',
            room_id: null,
            room_alias: payload.room_alias,
            err
          });

          return;
        }
      }

      _dispatcher.default.dispatch({
        action: 'view_room',
        room_id: roomId,
        event_id: payload.event_id,
        highlighted: payload.highlighted,
        room_alias: payload.room_alias,
        auto_join: payload.auto_join,
        oob_data: payload.oob_data
      });
    }
  }

  _viewRoomError(payload) {
    this._setState({
      roomId: payload.room_id,
      roomAlias: payload.room_alias,
      roomLoading: false,
      roomLoadError: payload.err
    });
  }

  _joinRoom(payload) {
    this._setState({
      joining: true
    });

    _MatrixClientPeg.MatrixClientPeg.get().joinRoom(this._state.roomAlias || this._state.roomId, payload.opts).then(() => {
      // We do *not* clear the 'joining' flag because the Room object and/or our 'joined' member event may not
      // have come down the sync stream yet, and that's the point at which we'd consider the user joined to the
      // room.
      _dispatcher.default.dispatch({
        action: 'join_room_ready'
      });
    }, err => {
      _dispatcher.default.dispatch({
        action: 'join_room_error',
        err: err
      });

      let msg = err.message ? err.message : JSON.stringify(err); // XXX: We are relying on the error message returned by browsers here.
      // This isn't great, but it does generalize the error being shown to users.

      if (msg && msg.startsWith("CORS request rejected")) {
        msg = (0, _languageHandler._t)("There was an error joining the room");
      }

      if (err.errcode === 'M_INCOMPATIBLE_ROOM_VERSION') {
        msg = React.createElement("div", null, (0, _languageHandler._t)("Sorry, your homeserver is too old to participate in this room."), React.createElement("br", null), (0, _languageHandler._t)("Please contact your homeserver administrator."));
      }

      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Failed to join room', '', ErrorDialog, {
        title: (0, _languageHandler._t)("Failed to join room"),
        description: msg
      });
    });
  }

  _joinRoomError(payload) {
    this._setState({
      joining: false,
      joinError: payload.err
    });
  }

  reset() {
    this._state = Object.assign({}, INITIAL_STATE);
  } // The room ID of the room currently being viewed


  getRoomId() {
    return this._state.roomId;
  } // The event to scroll to when the room is first viewed


  getInitialEventId() {
    return this._state.initialEventId;
  } // Whether to highlight the initial event


  isInitialEventHighlighted() {
    return this._state.isInitialEventHighlighted;
  } // The room alias of the room (or null if not originally specified in view_room)


  getRoomAlias() {
    return this._state.roomAlias;
  } // Whether the current room is loading (true whilst resolving an alias)


  isRoomLoading() {
    return this._state.roomLoading;
  } // Any error that has occurred during loading


  getRoomLoadError() {
    return this._state.roomLoadError;
  } // True if we're expecting the user to be joined to the room currently being
  // viewed. Note that this is left true after the join request has finished,
  // since we should still consider a join to be in progress until the room
  // & member events come down the sync.
  //
  // This flag remains true after the room has been sucessfully joined,
  // (this store doesn't listen for the appropriate member events)
  // so you should always observe the joined state from the member event
  // if a room object is present.
  // ie. The correct logic is:
  // if (room) {
  //     if (myMember.membership == 'joined') {
  //         // user is joined to the room
  //     } else {
  //         // Not joined
  //     }
  // } else {
  //     if (RoomViewStore.isJoining()) {
  //         // show spinner
  //     } else {
  //         // show join prompt
  //     }
  // }


  isJoining() {
    return this._state.joining;
  } // Any error that has occurred during joining


  getJoinError() {
    return this._state.joinError;
  } // The mxEvent if one is about to be forwarded


  getForwardingEvent() {
    return this._state.forwardingEvent;
  } // The mxEvent if one is currently being replied to/quoted


  getQuotingEvent() {
    return this._state.replyingToEvent;
  }

  shouldPeek() {
    return this._state.shouldPeek && this._state.matrixClientIsReady;
  }

}

let singletonRoomViewStore = null;

if (!singletonRoomViewStore) {
  singletonRoomViewStore = new RoomViewStore();
}

var _default = singletonRoomViewStore;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zdG9yZXMvUm9vbVZpZXdTdG9yZS5qcyJdLCJuYW1lcyI6WyJJTklUSUFMX1NUQVRFIiwiam9pbmluZyIsImpvaW5FcnJvciIsInJvb21JZCIsImluaXRpYWxFdmVudElkIiwiaXNJbml0aWFsRXZlbnRIaWdobGlnaHRlZCIsInJvb21BbGlhcyIsInJvb21Mb2FkaW5nIiwicm9vbUxvYWRFcnJvciIsImZvcndhcmRpbmdFdmVudCIsInF1b3RpbmdFdmVudCIsIm1hdHJpeENsaWVudElzUmVhZHkiLCJSb29tVmlld1N0b3JlIiwiU3RvcmUiLCJjb25zdHJ1Y3RvciIsImRpcyIsIl9zdGF0ZSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImlzSW5pdGlhbFN5bmNDb21wbGV0ZSIsIl9zZXRTdGF0ZSIsIm5ld1N0YXRlIiwic3RhdGVDaGFuZ2VkIiwia2V5IiwiT2JqZWN0Iiwia2V5cyIsImFzc2lnbiIsIl9fZW1pdENoYW5nZSIsIl9fb25EaXNwYXRjaCIsInBheWxvYWQiLCJhY3Rpb24iLCJfdmlld1Jvb20iLCJfdmlld1Jvb21FcnJvciIsIl9qb2luUm9vbSIsIl9qb2luUm9vbUVycm9yIiwic2hvdWxkUGVlayIsInJlc2V0IiwiZXZlbnQiLCJnZXRSb29tSWQiLCJkaXNwYXRjaCIsInJvb21faWQiLCJyZXBseWluZ1RvRXZlbnQiLCJSb29tU2V0dGluZ3NEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJyb29tX2FsaWFzIiwiZXZlbnRfaWQiLCJoaWdobGlnaHRlZCIsInNob3VsZF9wZWVrIiwidW5kZWZpbmVkIiwiaXNFZGl0aW5nU2V0dGluZ3MiLCJhdXRvX2pvaW4iLCJpbml0aWFsRXZlbnRQaXhlbE9mZnNldCIsInJlc3VsdCIsImdldFJvb21JZEZvckFsaWFzIiwiZXJyIiwib29iX2RhdGEiLCJqb2luUm9vbSIsIm9wdHMiLCJ0aGVuIiwibXNnIiwibWVzc2FnZSIsIkpTT04iLCJzdHJpbmdpZnkiLCJzdGFydHNXaXRoIiwiZXJyY29kZSIsIkVycm9yRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImdldEluaXRpYWxFdmVudElkIiwiZ2V0Um9vbUFsaWFzIiwiaXNSb29tTG9hZGluZyIsImdldFJvb21Mb2FkRXJyb3IiLCJpc0pvaW5pbmciLCJnZXRKb2luRXJyb3IiLCJnZXRGb3J3YXJkaW5nRXZlbnQiLCJnZXRRdW90aW5nRXZlbnQiLCJzaW5nbGV0b25Sb29tVmlld1N0b3JlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBLE1BQU1BLGFBQWEsR0FBRztBQUNsQjtBQUNBQyxFQUFBQSxPQUFPLEVBQUUsS0FGUztBQUdsQjtBQUNBQyxFQUFBQSxTQUFTLEVBQUUsSUFKTztBQUtsQjtBQUNBQyxFQUFBQSxNQUFNLEVBQUUsSUFOVTtBQVFsQjtBQUNBQyxFQUFBQSxjQUFjLEVBQUUsSUFURTtBQVVsQjtBQUNBQyxFQUFBQSx5QkFBeUIsRUFBRSxLQVhUO0FBYWxCO0FBQ0FDLEVBQUFBLFNBQVMsRUFBRSxJQWRPO0FBZWxCO0FBQ0FDLEVBQUFBLFdBQVcsRUFBRSxLQWhCSztBQWlCbEI7QUFDQUMsRUFBQUEsYUFBYSxFQUFFLElBbEJHO0FBb0JsQkMsRUFBQUEsZUFBZSxFQUFFLElBcEJDO0FBc0JsQkMsRUFBQUEsWUFBWSxFQUFFLElBdEJJO0FBdUJsQkMsRUFBQUEsbUJBQW1CLEVBQUU7QUF2QkgsQ0FBdEI7QUEwQkE7Ozs7OztBQUtBLE1BQU1DLGFBQU4sU0FBNEJDLFlBQTVCLENBQWtDO0FBQzlCQyxFQUFBQSxXQUFXLEdBQUc7QUFDVixVQUFNQyxtQkFBTixFQURVLENBR1Y7O0FBQ0EsU0FBS0MsTUFBTCxHQUFjaEIsYUFBZDs7QUFDQSxRQUFJaUIsaUNBQWdCQyxHQUFoQixFQUFKLEVBQTJCO0FBQ3ZCLFdBQUtGLE1BQUwsQ0FBWUwsbUJBQVosR0FBa0NNLGlDQUFnQkMsR0FBaEIsR0FBc0JDLHFCQUF0QixFQUFsQztBQUNIO0FBQ0o7O0FBRURDLEVBQUFBLFNBQVMsQ0FBQ0MsUUFBRCxFQUFXO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBLFFBQUlDLFlBQVksR0FBRyxLQUFuQjs7QUFDQSxTQUFLLE1BQU1DLEdBQVgsSUFBa0JDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSixRQUFaLENBQWxCLEVBQXlDO0FBQ3JDLFVBQUksS0FBS0wsTUFBTCxDQUFZTyxHQUFaLE1BQXFCRixRQUFRLENBQUNFLEdBQUQsQ0FBakMsRUFBd0M7QUFDcENELFFBQUFBLFlBQVksR0FBRyxJQUFmO0FBQ0E7QUFDSDtBQUNKOztBQUNELFFBQUksQ0FBQ0EsWUFBTCxFQUFtQjtBQUNmO0FBQ0g7O0FBRUQsU0FBS04sTUFBTCxHQUFjUSxNQUFNLENBQUNFLE1BQVAsQ0FBYyxLQUFLVixNQUFuQixFQUEyQkssUUFBM0IsQ0FBZDs7QUFDQSxTQUFLTSxZQUFMO0FBQ0g7O0FBRURDLEVBQUFBLFlBQVksQ0FBQ0MsT0FBRCxFQUFVO0FBQ2xCLFlBQVFBLE9BQU8sQ0FBQ0MsTUFBaEI7QUFDSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFLLFdBQUw7QUFDSSxhQUFLQyxTQUFMLENBQWVGLE9BQWY7O0FBQ0E7O0FBQ0osV0FBSyxnQkFBTDtBQUNBLFdBQUssWUFBTDtBQUNJLGFBQUtULFNBQUwsQ0FBZTtBQUNYakIsVUFBQUEsTUFBTSxFQUFFLElBREc7QUFFWEcsVUFBQUEsU0FBUyxFQUFFO0FBRkEsU0FBZjs7QUFJQTs7QUFDSixXQUFLLGlCQUFMO0FBQ0ksYUFBSzBCLGNBQUwsQ0FBb0JILE9BQXBCOztBQUNBOztBQUNKLFdBQUssV0FBTDtBQUNJLGFBQUtULFNBQUwsQ0FBZTtBQUNYbkIsVUFBQUEsT0FBTyxFQUFFO0FBREUsU0FBZjs7QUFHQTs7QUFDSixXQUFLLGFBQUw7QUFDSSxhQUFLbUIsU0FBTCxDQUFlO0FBQ1huQixVQUFBQSxPQUFPLEVBQUU7QUFERSxTQUFmOztBQUdBO0FBQ0o7QUFDQTs7QUFDQSxXQUFLLFdBQUw7QUFDSSxhQUFLZ0MsU0FBTCxDQUFlSixPQUFmOztBQUNBOztBQUNKLFdBQUssaUJBQUw7QUFDSSxhQUFLSyxjQUFMLENBQW9CTCxPQUFwQjs7QUFDQTs7QUFDSixXQUFLLGlCQUFMO0FBQ0ksYUFBS1QsU0FBTCxDQUFlO0FBQUVlLFVBQUFBLFVBQVUsRUFBRTtBQUFkLFNBQWY7O0FBQ0E7O0FBQ0osV0FBSyxzQkFBTDtBQUNBLFdBQUssZUFBTDtBQUNJLGFBQUtDLEtBQUw7QUFDQTs7QUFDSixXQUFLLGVBQUw7QUFDSSxhQUFLaEIsU0FBTCxDQUFlO0FBQ1hYLFVBQUFBLGVBQWUsRUFBRW9CLE9BQU8sQ0FBQ1E7QUFEZCxTQUFmOztBQUdBOztBQUNKLFdBQUssZ0JBQUw7QUFDSTtBQUNBO0FBQ0EsWUFBSVIsT0FBTyxDQUFDUSxLQUFSLElBQWlCUixPQUFPLENBQUNRLEtBQVIsQ0FBY0MsU0FBZCxPQUE4QixLQUFLdEIsTUFBTCxDQUFZYixNQUEvRCxFQUF1RTtBQUNuRVksOEJBQUl3QixRQUFKLENBQWE7QUFDVFQsWUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVFUsWUFBQUEsT0FBTyxFQUFFWCxPQUFPLENBQUNRLEtBQVIsQ0FBY0MsU0FBZCxFQUZBO0FBR1RHLFlBQUFBLGVBQWUsRUFBRVosT0FBTyxDQUFDUTtBQUhoQixXQUFiO0FBS0gsU0FORCxNQU1PO0FBQ0gsZUFBS2pCLFNBQUwsQ0FBZTtBQUNYcUIsWUFBQUEsZUFBZSxFQUFFWixPQUFPLENBQUNRO0FBRGQsV0FBZjtBQUdIOztBQUNEOztBQUNKLFdBQUssb0JBQUw7QUFBMkI7QUFDdkIsZ0JBQU1LLGtCQUFrQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNEJBQWpCLENBQTNCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsZUFBMUIsRUFBMkMsRUFBM0MsRUFBK0NKLGtCQUEvQyxFQUFtRTtBQUMvRHZDLFlBQUFBLE1BQU0sRUFBRTBCLE9BQU8sQ0FBQ1csT0FBUixJQUFtQixLQUFLeEIsTUFBTCxDQUFZYjtBQUR3QixXQUFuRTtBQUVHO0FBQWMsY0FGakI7QUFFdUI7QUFBZSxlQUZ0QztBQUU2QztBQUFhLGNBRjFEOztBQUdBO0FBQ0g7O0FBQ0QsV0FBSyxZQUFMO0FBQ0ksYUFBS2lCLFNBQUwsQ0FBZTtBQUNYVCxVQUFBQSxtQkFBbUIsRUFBRU0saUNBQWdCQyxHQUFoQixHQUFzQkMscUJBQXRCO0FBRFYsU0FBZjs7QUFHQTtBQTVFUjtBQThFSDs7QUFFRCxRQUFNWSxTQUFOLENBQWdCRixPQUFoQixFQUF5QjtBQUNyQixRQUFJQSxPQUFPLENBQUNXLE9BQVosRUFBcUI7QUFDakIsWUFBTW5CLFFBQVEsR0FBRztBQUNibEIsUUFBQUEsTUFBTSxFQUFFMEIsT0FBTyxDQUFDVyxPQURIO0FBRWJsQyxRQUFBQSxTQUFTLEVBQUV1QixPQUFPLENBQUNrQixVQUZOO0FBR2IzQyxRQUFBQSxjQUFjLEVBQUV5QixPQUFPLENBQUNtQixRQUhYO0FBSWIzQyxRQUFBQSx5QkFBeUIsRUFBRXdCLE9BQU8sQ0FBQ29CLFdBSnRCO0FBS2J4QyxRQUFBQSxlQUFlLEVBQUUsSUFMSjtBQU1iRixRQUFBQSxXQUFXLEVBQUUsS0FOQTtBQU9iQyxRQUFBQSxhQUFhLEVBQUUsSUFQRjtBQVFiO0FBQ0EyQixRQUFBQSxVQUFVLEVBQUVOLE9BQU8sQ0FBQ3FCLFdBQVIsS0FBd0JDLFNBQXhCLEdBQW9DLElBQXBDLEdBQTJDdEIsT0FBTyxDQUFDcUIsV0FUbEQ7QUFVYjtBQUNBakQsUUFBQUEsT0FBTyxFQUFFNEIsT0FBTyxDQUFDNUIsT0FBUixJQUFtQixLQVhmO0FBWWI7QUFDQXdDLFFBQUFBLGVBQWUsRUFBRSxJQWJKO0FBY2I7QUFDQVcsUUFBQUEsaUJBQWlCLEVBQUU7QUFmTixPQUFqQixDQURpQixDQW1CakI7O0FBQ0EsVUFBSXZCLE9BQU8sQ0FBQ1ksZUFBUixJQUEyQlosT0FBTyxDQUFDWSxlQUFSLENBQXdCSCxTQUF4QixPQUF3Q1QsT0FBTyxDQUFDVyxPQUEvRSxFQUF3RjtBQUNwRm5CLFFBQUFBLFFBQVEsQ0FBQ29CLGVBQVQsR0FBMkJaLE9BQU8sQ0FBQ1ksZUFBbkM7QUFDSDs7QUFFRCxVQUFJLEtBQUt6QixNQUFMLENBQVlQLGVBQWhCLEVBQWlDO0FBQzdCTSw0QkFBSXdCLFFBQUosQ0FBYTtBQUNUVCxVQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUVSxVQUFBQSxPQUFPLEVBQUVuQixRQUFRLENBQUNsQixNQUZUO0FBR1RrQyxVQUFBQSxLQUFLLEVBQUUsS0FBS3JCLE1BQUwsQ0FBWVA7QUFIVixTQUFiO0FBS0g7O0FBRUQsV0FBS1csU0FBTCxDQUFlQyxRQUFmOztBQUVBLFVBQUlRLE9BQU8sQ0FBQ3dCLFNBQVosRUFBdUI7QUFDbkIsYUFBS3BCLFNBQUwsQ0FBZUosT0FBZjtBQUNIO0FBQ0osS0FyQ0QsTUFxQ08sSUFBSUEsT0FBTyxDQUFDa0IsVUFBWixFQUF3QjtBQUMzQjtBQUNBO0FBQ0EsVUFBSTVDLE1BQU0sR0FBRyw2Q0FBd0IwQixPQUFPLENBQUNrQixVQUFoQyxDQUFiOztBQUNBLFVBQUksQ0FBQzVDLE1BQUwsRUFBYTtBQUNUO0FBQ0E7QUFDQSxhQUFLaUIsU0FBTCxDQUFlO0FBQ1hqQixVQUFBQSxNQUFNLEVBQUUsSUFERztBQUVYQyxVQUFBQSxjQUFjLEVBQUUsSUFGTDtBQUdYa0QsVUFBQUEsdUJBQXVCLEVBQUUsSUFIZDtBQUlYakQsVUFBQUEseUJBQXlCLEVBQUUsSUFKaEI7QUFLWEMsVUFBQUEsU0FBUyxFQUFFdUIsT0FBTyxDQUFDa0IsVUFMUjtBQU1YeEMsVUFBQUEsV0FBVyxFQUFFLElBTkY7QUFPWEMsVUFBQUEsYUFBYSxFQUFFO0FBUEosU0FBZjs7QUFTQSxZQUFJO0FBQ0EsZ0JBQU0rQyxNQUFNLEdBQUcsTUFBTXRDLGlDQUFnQkMsR0FBaEIsR0FBc0JzQyxpQkFBdEIsQ0FBd0MzQixPQUFPLENBQUNrQixVQUFoRCxDQUFyQjtBQUNBLHFEQUFzQmxCLE9BQU8sQ0FBQ2tCLFVBQTlCLEVBQTBDUSxNQUFNLENBQUNmLE9BQWpEO0FBQ0FyQyxVQUFBQSxNQUFNLEdBQUdvRCxNQUFNLENBQUNmLE9BQWhCO0FBQ0gsU0FKRCxDQUlFLE9BQU9pQixHQUFQLEVBQVk7QUFDVjFDLDhCQUFJd0IsUUFBSixDQUFhO0FBQ1RULFlBQUFBLE1BQU0sRUFBRSxpQkFEQztBQUVUVSxZQUFBQSxPQUFPLEVBQUUsSUFGQTtBQUdUTyxZQUFBQSxVQUFVLEVBQUVsQixPQUFPLENBQUNrQixVQUhYO0FBSVRVLFlBQUFBO0FBSlMsV0FBYjs7QUFNQTtBQUNIO0FBQ0o7O0FBRUQxQywwQkFBSXdCLFFBQUosQ0FBYTtBQUNUVCxRQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUVSxRQUFBQSxPQUFPLEVBQUVyQyxNQUZBO0FBR1Q2QyxRQUFBQSxRQUFRLEVBQUVuQixPQUFPLENBQUNtQixRQUhUO0FBSVRDLFFBQUFBLFdBQVcsRUFBRXBCLE9BQU8sQ0FBQ29CLFdBSlo7QUFLVEYsUUFBQUEsVUFBVSxFQUFFbEIsT0FBTyxDQUFDa0IsVUFMWDtBQU1UTSxRQUFBQSxTQUFTLEVBQUV4QixPQUFPLENBQUN3QixTQU5WO0FBT1RLLFFBQUFBLFFBQVEsRUFBRTdCLE9BQU8sQ0FBQzZCO0FBUFQsT0FBYjtBQVNIO0FBQ0o7O0FBRUQxQixFQUFBQSxjQUFjLENBQUNILE9BQUQsRUFBVTtBQUNwQixTQUFLVCxTQUFMLENBQWU7QUFDWGpCLE1BQUFBLE1BQU0sRUFBRTBCLE9BQU8sQ0FBQ1csT0FETDtBQUVYbEMsTUFBQUEsU0FBUyxFQUFFdUIsT0FBTyxDQUFDa0IsVUFGUjtBQUdYeEMsTUFBQUEsV0FBVyxFQUFFLEtBSEY7QUFJWEMsTUFBQUEsYUFBYSxFQUFFcUIsT0FBTyxDQUFDNEI7QUFKWixLQUFmO0FBTUg7O0FBRUR4QixFQUFBQSxTQUFTLENBQUNKLE9BQUQsRUFBVTtBQUNmLFNBQUtULFNBQUwsQ0FBZTtBQUNYbkIsTUFBQUEsT0FBTyxFQUFFO0FBREUsS0FBZjs7QUFHQWdCLHFDQUFnQkMsR0FBaEIsR0FBc0J5QyxRQUF0QixDQUNJLEtBQUszQyxNQUFMLENBQVlWLFNBQVosSUFBeUIsS0FBS1UsTUFBTCxDQUFZYixNQUR6QyxFQUNpRDBCLE9BQU8sQ0FBQytCLElBRHpELEVBRUVDLElBRkYsQ0FFTyxNQUFNO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E5QywwQkFBSXdCLFFBQUosQ0FBYTtBQUFFVCxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiO0FBQ0gsS0FQRCxFQU9JMkIsR0FBRCxJQUFTO0FBQ1IxQywwQkFBSXdCLFFBQUosQ0FBYTtBQUNUVCxRQUFBQSxNQUFNLEVBQUUsaUJBREM7QUFFVDJCLFFBQUFBLEdBQUcsRUFBRUE7QUFGSSxPQUFiOztBQUlBLFVBQUlLLEdBQUcsR0FBR0wsR0FBRyxDQUFDTSxPQUFKLEdBQWNOLEdBQUcsQ0FBQ00sT0FBbEIsR0FBNEJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlUixHQUFmLENBQXRDLENBTFEsQ0FNUjtBQUNBOztBQUNBLFVBQUlLLEdBQUcsSUFBSUEsR0FBRyxDQUFDSSxVQUFKLENBQWUsdUJBQWYsQ0FBWCxFQUFvRDtBQUNoREosUUFBQUEsR0FBRyxHQUFHLHlCQUFHLHFDQUFILENBQU47QUFDSDs7QUFDRCxVQUFJTCxHQUFHLENBQUNVLE9BQUosS0FBZ0IsNkJBQXBCLEVBQW1EO0FBQy9DTCxRQUFBQSxHQUFHLEdBQUcsaUNBQ0QseUJBQUcsZ0VBQUgsQ0FEQyxFQUNvRSwrQkFEcEUsRUFFRCx5QkFBRywrQ0FBSCxDQUZDLENBQU47QUFJSDs7QUFDRCxZQUFNTSxXQUFXLEdBQUd6QixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIscUJBQTFCLEVBQWlELEVBQWpELEVBQXFEc0IsV0FBckQsRUFBa0U7QUFDOURDLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxxQkFBSCxDQUR1RDtBQUU5REMsUUFBQUEsV0FBVyxFQUFFUjtBQUZpRCxPQUFsRTtBQUlILEtBN0JEO0FBOEJIOztBQUVENUIsRUFBQUEsY0FBYyxDQUFDTCxPQUFELEVBQVU7QUFDcEIsU0FBS1QsU0FBTCxDQUFlO0FBQ1huQixNQUFBQSxPQUFPLEVBQUUsS0FERTtBQUVYQyxNQUFBQSxTQUFTLEVBQUUyQixPQUFPLENBQUM0QjtBQUZSLEtBQWY7QUFJSDs7QUFFRHJCLEVBQUFBLEtBQUssR0FBRztBQUNKLFNBQUtwQixNQUFMLEdBQWNRLE1BQU0sQ0FBQ0UsTUFBUCxDQUFjLEVBQWQsRUFBa0IxQixhQUFsQixDQUFkO0FBQ0gsR0F0UDZCLENBd1A5Qjs7O0FBQ0FzQyxFQUFBQSxTQUFTLEdBQUc7QUFDUixXQUFPLEtBQUt0QixNQUFMLENBQVliLE1BQW5CO0FBQ0gsR0EzUDZCLENBNlA5Qjs7O0FBQ0FvRSxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixXQUFPLEtBQUt2RCxNQUFMLENBQVlaLGNBQW5CO0FBQ0gsR0FoUTZCLENBa1E5Qjs7O0FBQ0FDLEVBQUFBLHlCQUF5QixHQUFHO0FBQ3hCLFdBQU8sS0FBS1csTUFBTCxDQUFZWCx5QkFBbkI7QUFDSCxHQXJRNkIsQ0F1UTlCOzs7QUFDQW1FLEVBQUFBLFlBQVksR0FBRztBQUNYLFdBQU8sS0FBS3hELE1BQUwsQ0FBWVYsU0FBbkI7QUFDSCxHQTFRNkIsQ0E0UTlCOzs7QUFDQW1FLEVBQUFBLGFBQWEsR0FBRztBQUNaLFdBQU8sS0FBS3pELE1BQUwsQ0FBWVQsV0FBbkI7QUFDSCxHQS9RNkIsQ0FpUjlCOzs7QUFDQW1FLEVBQUFBLGdCQUFnQixHQUFHO0FBQ2YsV0FBTyxLQUFLMUQsTUFBTCxDQUFZUixhQUFuQjtBQUNILEdBcFI2QixDQXNSOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0FtRSxFQUFBQSxTQUFTLEdBQUc7QUFDUixXQUFPLEtBQUszRCxNQUFMLENBQVlmLE9BQW5CO0FBQ0gsR0EvUzZCLENBaVQ5Qjs7O0FBQ0EyRSxFQUFBQSxZQUFZLEdBQUc7QUFDWCxXQUFPLEtBQUs1RCxNQUFMLENBQVlkLFNBQW5CO0FBQ0gsR0FwVDZCLENBc1Q5Qjs7O0FBQ0EyRSxFQUFBQSxrQkFBa0IsR0FBRztBQUNqQixXQUFPLEtBQUs3RCxNQUFMLENBQVlQLGVBQW5CO0FBQ0gsR0F6VDZCLENBMlQ5Qjs7O0FBQ0FxRSxFQUFBQSxlQUFlLEdBQUc7QUFDZCxXQUFPLEtBQUs5RCxNQUFMLENBQVl5QixlQUFuQjtBQUNIOztBQUVETixFQUFBQSxVQUFVLEdBQUc7QUFDVCxXQUFPLEtBQUtuQixNQUFMLENBQVltQixVQUFaLElBQTBCLEtBQUtuQixNQUFMLENBQVlMLG1CQUE3QztBQUNIOztBQWxVNkI7O0FBcVVsQyxJQUFJb0Usc0JBQXNCLEdBQUcsSUFBN0I7O0FBQ0EsSUFBSSxDQUFDQSxzQkFBTCxFQUE2QjtBQUN6QkEsRUFBQUEsc0JBQXNCLEdBQUcsSUFBSW5FLGFBQUosRUFBekI7QUFDSDs7ZUFDY21FLHNCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTcsIDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7U3RvcmV9IGZyb20gJ2ZsdXgvdXRpbHMnO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4uL01vZGFsJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgeyBnZXRDYWNoZWRSb29tSURGb3JBbGlhcywgc3RvcmVSb29tQWxpYXNJbkNhY2hlIH0gZnJvbSAnLi4vUm9vbUFsaWFzQ2FjaGUnO1xyXG5cclxuY29uc3QgSU5JVElBTF9TVEFURSA9IHtcclxuICAgIC8vIFdoZXRoZXIgd2UncmUgam9pbmluZyB0aGUgY3VycmVudGx5IHZpZXdlZCByb29tIChzZWUgaXNKb2luaW5nKCkpXHJcbiAgICBqb2luaW5nOiBmYWxzZSxcclxuICAgIC8vIEFueSBlcnJvciB0aGF0IGhhcyBvY2N1cnJlZCBkdXJpbmcgam9pbmluZ1xyXG4gICAgam9pbkVycm9yOiBudWxsLFxyXG4gICAgLy8gVGhlIHJvb20gSUQgb2YgdGhlIHJvb20gY3VycmVudGx5IGJlaW5nIHZpZXdlZFxyXG4gICAgcm9vbUlkOiBudWxsLFxyXG5cclxuICAgIC8vIFRoZSBldmVudCB0byBzY3JvbGwgdG8gd2hlbiB0aGUgcm9vbSBpcyBmaXJzdCB2aWV3ZWRcclxuICAgIGluaXRpYWxFdmVudElkOiBudWxsLFxyXG4gICAgLy8gV2hldGhlciB0byBoaWdobGlnaHQgdGhlIGluaXRpYWwgZXZlbnRcclxuICAgIGlzSW5pdGlhbEV2ZW50SGlnaGxpZ2h0ZWQ6IGZhbHNlLFxyXG5cclxuICAgIC8vIFRoZSByb29tIGFsaWFzIG9mIHRoZSByb29tIChvciBudWxsIGlmIG5vdCBvcmlnaW5hbGx5IHNwZWNpZmllZCBpbiB2aWV3X3Jvb20pXHJcbiAgICByb29tQWxpYXM6IG51bGwsXHJcbiAgICAvLyBXaGV0aGVyIHRoZSBjdXJyZW50IHJvb20gaXMgbG9hZGluZ1xyXG4gICAgcm9vbUxvYWRpbmc6IGZhbHNlLFxyXG4gICAgLy8gQW55IGVycm9yIHRoYXQgaGFzIG9jY3VycmVkIGR1cmluZyBsb2FkaW5nXHJcbiAgICByb29tTG9hZEVycm9yOiBudWxsLFxyXG5cclxuICAgIGZvcndhcmRpbmdFdmVudDogbnVsbCxcclxuXHJcbiAgICBxdW90aW5nRXZlbnQ6IG51bGwsXHJcbiAgICBtYXRyaXhDbGllbnRJc1JlYWR5OiBmYWxzZSxcclxufTtcclxuXHJcbi8qKlxyXG4gKiBBIGNsYXNzIGZvciBzdG9yaW5nIGFwcGxpY2F0aW9uIHN0YXRlIGZvciBSb29tVmlldy4gVGhpcyBpcyB0aGUgUm9vbVZpZXcncyBpbnRlcmZhY2VcclxuKiAgd2l0aCBhIHN1YnNldCBvZiB0aGUganMtc2RrLlxyXG4gKiAgYGBgXHJcbiAqL1xyXG5jbGFzcyBSb29tVmlld1N0b3JlIGV4dGVuZHMgU3RvcmUge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoZGlzKTtcclxuXHJcbiAgICAgICAgLy8gSW5pdGlhbGlzZSBzdGF0ZVxyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gSU5JVElBTF9TVEFURTtcclxuICAgICAgICBpZiAoTWF0cml4Q2xpZW50UGVnLmdldCgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N0YXRlLm1hdHJpeENsaWVudElzUmVhZHkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNJbml0aWFsU3luY0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9zZXRTdGF0ZShuZXdTdGF0ZSkge1xyXG4gICAgICAgIC8vIElmIHZhbHVlcyBoYXZlbid0IGNoYW5nZWQsIHRoZXJlJ3Mgbm90aGluZyB0byBkby5cclxuICAgICAgICAvLyBUaGlzIG9ubHkgdHJpZXMgYSBzaGFsbG93IGNvbXBhcmlzb24sIHNvIHVuY2hhbmdlZCBvYmplY3RzIHdpbGwgc2xpcFxyXG4gICAgICAgIC8vIHRocm91Z2gsIGJ1dCB0aGF0J3MgcHJvYmFibHkgb2theSBmb3Igbm93LlxyXG4gICAgICAgIGxldCBzdGF0ZUNoYW5nZWQgPSBmYWxzZTtcclxuICAgICAgICBmb3IgKGNvbnN0IGtleSBvZiBPYmplY3Qua2V5cyhuZXdTdGF0ZSkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3N0YXRlW2tleV0gIT09IG5ld1N0YXRlW2tleV0pIHtcclxuICAgICAgICAgICAgICAgIHN0YXRlQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXN0YXRlQ2hhbmdlZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IE9iamVjdC5hc3NpZ24odGhpcy5fc3RhdGUsIG5ld1N0YXRlKTtcclxuICAgICAgICB0aGlzLl9fZW1pdENoYW5nZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIF9fb25EaXNwYXRjaChwYXlsb2FkKSB7XHJcbiAgICAgICAgc3dpdGNoIChwYXlsb2FkLmFjdGlvbikge1xyXG4gICAgICAgICAgICAvLyB2aWV3X3Jvb206XHJcbiAgICAgICAgICAgIC8vICAgICAgLSByb29tX2FsaWFzOiAgICcjc29tZWFsaWFzOm1hdHJpeC5vcmcnXHJcbiAgICAgICAgICAgIC8vICAgICAgLSByb29tX2lkOiAgICAgICchcm9vbWlkMTIzOm1hdHJpeC5vcmcnXHJcbiAgICAgICAgICAgIC8vICAgICAgLSBldmVudF9pZDogICAgICckMjEzNDU2NzgyOm1hdHJpeC5vcmcnXHJcbiAgICAgICAgICAgIC8vICAgICAgLSBldmVudF9vZmZzZXQ6IDEwMFxyXG4gICAgICAgICAgICAvLyAgICAgIC0gaGlnaGxpZ2h0ZWQ6ICB0cnVlXHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfcm9vbSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl92aWV3Um9vbShwYXlsb2FkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X215X2dyb3Vwcyc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfZ3JvdXAnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHJvb21JZDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICByb29tQWxpYXM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd2aWV3X3Jvb21fZXJyb3InOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdmlld1Jvb21FcnJvcihwYXlsb2FkKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd3aWxsX2pvaW4nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIGpvaW5pbmc6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdjYW5jZWxfam9pbic6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgam9pbmluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAvLyBqb2luX3Jvb206XHJcbiAgICAgICAgICAgIC8vICAgICAgLSBvcHRzOiBvcHRpb25zIGZvciBqb2luUm9vbVxyXG4gICAgICAgICAgICBjYXNlICdqb2luX3Jvb20nOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fam9pblJvb20ocGF5bG9hZCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnam9pbl9yb29tX2Vycm9yJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX2pvaW5Sb29tRXJyb3IocGF5bG9hZCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnam9pbl9yb29tX3JlYWR5JzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHsgc2hvdWxkUGVlazogZmFsc2UgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnb25fY2xpZW50X25vdF92aWFibGUnOlxyXG4gICAgICAgICAgICBjYXNlICdvbl9sb2dnZWRfb3V0JzpcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzZXQoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdmb3J3YXJkX2V2ZW50JzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBmb3J3YXJkaW5nRXZlbnQ6IHBheWxvYWQuZXZlbnQsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdyZXBseV90b19ldmVudCc6XHJcbiAgICAgICAgICAgICAgICAvLyBJZiBjdXJyZW50bHkgdmlld2VkIHJvb20gZG9lcyBub3QgbWF0Y2ggdGhlIHJvb20gaW4gd2hpY2ggd2Ugd2lzaCB0byByZXBseSB0aGVuIGNoYW5nZSByb29tc1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcyBjYW4gaGFwcGVuIHdoZW4gcGVyZm9ybWluZyBhIHNlYXJjaCBhY3Jvc3MgYWxsIHJvb21zXHJcbiAgICAgICAgICAgICAgICBpZiAocGF5bG9hZC5ldmVudCAmJiBwYXlsb2FkLmV2ZW50LmdldFJvb21JZCgpICE9PSB0aGlzLl9zdGF0ZS5yb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tX2lkOiBwYXlsb2FkLmV2ZW50LmdldFJvb21JZCgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBseWluZ1RvRXZlbnQ6IHBheWxvYWQuZXZlbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwbHlpbmdUb0V2ZW50OiBwYXlsb2FkLmV2ZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ29wZW5fcm9vbV9zZXR0aW5ncyc6IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IFJvb21TZXR0aW5nc0RpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLlJvb21TZXR0aW5nc0RpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1Jvb20gc2V0dGluZ3MnLCAnJywgUm9vbVNldHRpbmdzRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbUlkOiBwYXlsb2FkLnJvb21faWQgfHwgdGhpcy5fc3RhdGUucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgfSwgLypjbGFzc05hbWU9Ki9udWxsLCAvKmlzUHJpb3JpdHk9Ki9mYWxzZSwgLyppc1N0YXRpYz0qL3RydWUpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAnc3luY19zdGF0ZSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF0cml4Q2xpZW50SXNSZWFkeTogTWF0cml4Q2xpZW50UGVnLmdldCgpLmlzSW5pdGlhbFN5bmNDb21wbGV0ZSgpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3ZpZXdSb29tKHBheWxvYWQpIHtcclxuICAgICAgICBpZiAocGF5bG9hZC5yb29tX2lkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xyXG4gICAgICAgICAgICAgICAgcm9vbUlkOiBwYXlsb2FkLnJvb21faWQsXHJcbiAgICAgICAgICAgICAgICByb29tQWxpYXM6IHBheWxvYWQucm9vbV9hbGlhcyxcclxuICAgICAgICAgICAgICAgIGluaXRpYWxFdmVudElkOiBwYXlsb2FkLmV2ZW50X2lkLFxyXG4gICAgICAgICAgICAgICAgaXNJbml0aWFsRXZlbnRIaWdobGlnaHRlZDogcGF5bG9hZC5oaWdobGlnaHRlZCxcclxuICAgICAgICAgICAgICAgIGZvcndhcmRpbmdFdmVudDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJvb21Mb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHJvb21Mb2FkRXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAvLyBzaG91bGQgcGVlayBieSBkZWZhdWx0XHJcbiAgICAgICAgICAgICAgICBzaG91bGRQZWVrOiBwYXlsb2FkLnNob3VsZF9wZWVrID09PSB1bmRlZmluZWQgPyB0cnVlIDogcGF5bG9hZC5zaG91bGRfcGVlayxcclxuICAgICAgICAgICAgICAgIC8vIGhhdmUgd2Ugc2VudCBhIGpvaW4gcmVxdWVzdCBmb3IgdGhpcyByb29tIGFuZCBhcmUgd2FpdGluZyBmb3IgYSByZXNwb25zZT9cclxuICAgICAgICAgICAgICAgIGpvaW5pbmc6IHBheWxvYWQuam9pbmluZyB8fCBmYWxzZSxcclxuICAgICAgICAgICAgICAgIC8vIFJlc2V0IHJlcGx5aW5nVG9FdmVudCBiZWNhdXNlIHdlIGRvbid0IHdhbnQgY3Jvc3Mtcm9vbSBiZWNhdXNlIGJhZCBVWFxyXG4gICAgICAgICAgICAgICAgcmVwbHlpbmdUb0V2ZW50OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgLy8gcHVsbCB0aGUgdXNlciBvdXQgb2YgUm9vbSBTZXR0aW5nc1xyXG4gICAgICAgICAgICAgICAgaXNFZGl0aW5nU2V0dGluZ3M6IGZhbHNlLFxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgLy8gQWxsb3cgYmVpbmcgZ2l2ZW4gYW4gZXZlbnQgdG8gYmUgcmVwbGllZCB0byB3aGVuIHN3aXRjaGluZyByb29tcyBidXQgc2FuaXR5IGNoZWNrIGl0cyBmb3IgdGhpcyByb29tXHJcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLnJlcGx5aW5nVG9FdmVudCAmJiBwYXlsb2FkLnJlcGx5aW5nVG9FdmVudC5nZXRSb29tSWQoKSA9PT0gcGF5bG9hZC5yb29tX2lkKSB7XHJcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5yZXBseWluZ1RvRXZlbnQgPSBwYXlsb2FkLnJlcGx5aW5nVG9FdmVudDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX3N0YXRlLmZvcndhcmRpbmdFdmVudCkge1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdzZW5kX2V2ZW50JyxcclxuICAgICAgICAgICAgICAgICAgICByb29tX2lkOiBuZXdTdGF0ZS5yb29tSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQ6IHRoaXMuX3N0YXRlLmZvcndhcmRpbmdFdmVudCxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZShuZXdTdGF0ZSk7XHJcblxyXG4gICAgICAgICAgICBpZiAocGF5bG9hZC5hdXRvX2pvaW4pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2pvaW5Sb29tKHBheWxvYWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChwYXlsb2FkLnJvb21fYWxpYXMpIHtcclxuICAgICAgICAgICAgLy8gVHJ5IHRoZSByb29tIGFsaWFzIHRvIHJvb20gSUQgbmF2aWdhdGlvbiBjYWNoZSBmaXJzdCB0byBhdm9pZFxyXG4gICAgICAgICAgICAvLyBibG9ja2luZyByb29tIG5hdmlnYXRpb24gb24gdGhlIGhvbWVzZXJ2ZXIuXHJcbiAgICAgICAgICAgIGxldCByb29tSWQgPSBnZXRDYWNoZWRSb29tSURGb3JBbGlhcyhwYXlsb2FkLnJvb21fYWxpYXMpO1xyXG4gICAgICAgICAgICBpZiAoIXJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gUm9vbSBhbGlhcyBjYWNoZSBtaXNzLCBzbyBsZXQncyBhc2sgdGhlIGhvbWVzZXJ2ZXIuIFJlc29sdmUgdGhlIGFsaWFzXHJcbiAgICAgICAgICAgICAgICAvLyBhbmQgdGhlbiBkbyBhIHNlY29uZCBkaXNwYXRjaCB3aXRoIHRoZSByb29tIElEIGFjcXVpcmVkLlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHJvb21JZDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBpbml0aWFsRXZlbnRJZDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBpbml0aWFsRXZlbnRQaXhlbE9mZnNldDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxFdmVudEhpZ2hsaWdodGVkOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21BbGlhczogcGF5bG9hZC5yb29tX2FsaWFzLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21Mb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21Mb2FkRXJyb3I6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb21JZEZvckFsaWFzKHBheWxvYWQucm9vbV9hbGlhcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RvcmVSb29tQWxpYXNJbkNhY2hlKHBheWxvYWQucm9vbV9hbGlhcywgcmVzdWx0LnJvb21faWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJvb21JZCA9IHJlc3VsdC5yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tX2Vycm9yJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9hbGlhczogcGF5bG9hZC5yb29tX2FsaWFzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnIsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgICAgIHJvb21faWQ6IHJvb21JZCxcclxuICAgICAgICAgICAgICAgIGV2ZW50X2lkOiBwYXlsb2FkLmV2ZW50X2lkLFxyXG4gICAgICAgICAgICAgICAgaGlnaGxpZ2h0ZWQ6IHBheWxvYWQuaGlnaGxpZ2h0ZWQsXHJcbiAgICAgICAgICAgICAgICByb29tX2FsaWFzOiBwYXlsb2FkLnJvb21fYWxpYXMsXHJcbiAgICAgICAgICAgICAgICBhdXRvX2pvaW46IHBheWxvYWQuYXV0b19qb2luLFxyXG4gICAgICAgICAgICAgICAgb29iX2RhdGE6IHBheWxvYWQub29iX2RhdGEsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfdmlld1Jvb21FcnJvcihwYXlsb2FkKSB7XHJcbiAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICByb29tSWQ6IHBheWxvYWQucm9vbV9pZCxcclxuICAgICAgICAgICAgcm9vbUFsaWFzOiBwYXlsb2FkLnJvb21fYWxpYXMsXHJcbiAgICAgICAgICAgIHJvb21Mb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgcm9vbUxvYWRFcnJvcjogcGF5bG9hZC5lcnIsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2pvaW5Sb29tKHBheWxvYWQpIHtcclxuICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGpvaW5pbmc6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmpvaW5Sb29tKFxyXG4gICAgICAgICAgICB0aGlzLl9zdGF0ZS5yb29tQWxpYXMgfHwgdGhpcy5fc3RhdGUucm9vbUlkLCBwYXlsb2FkLm9wdHMsXHJcbiAgICAgICAgKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgLy8gV2UgZG8gKm5vdCogY2xlYXIgdGhlICdqb2luaW5nJyBmbGFnIGJlY2F1c2UgdGhlIFJvb20gb2JqZWN0IGFuZC9vciBvdXIgJ2pvaW5lZCcgbWVtYmVyIGV2ZW50IG1heSBub3RcclxuICAgICAgICAgICAgLy8gaGF2ZSBjb21lIGRvd24gdGhlIHN5bmMgc3RyZWFtIHlldCwgYW5kIHRoYXQncyB0aGUgcG9pbnQgYXQgd2hpY2ggd2UnZCBjb25zaWRlciB0aGUgdXNlciBqb2luZWQgdG8gdGhlXHJcbiAgICAgICAgICAgIC8vIHJvb20uXHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ2pvaW5fcm9vbV9yZWFkeScgfSk7XHJcbiAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnam9pbl9yb29tX2Vycm9yJyxcclxuICAgICAgICAgICAgICAgIGVycjogZXJyLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgbGV0IG1zZyA9IGVyci5tZXNzYWdlID8gZXJyLm1lc3NhZ2UgOiBKU09OLnN0cmluZ2lmeShlcnIpO1xyXG4gICAgICAgICAgICAvLyBYWFg6IFdlIGFyZSByZWx5aW5nIG9uIHRoZSBlcnJvciBtZXNzYWdlIHJldHVybmVkIGJ5IGJyb3dzZXJzIGhlcmUuXHJcbiAgICAgICAgICAgIC8vIFRoaXMgaXNuJ3QgZ3JlYXQsIGJ1dCBpdCBkb2VzIGdlbmVyYWxpemUgdGhlIGVycm9yIGJlaW5nIHNob3duIHRvIHVzZXJzLlxyXG4gICAgICAgICAgICBpZiAobXNnICYmIG1zZy5zdGFydHNXaXRoKFwiQ09SUyByZXF1ZXN0IHJlamVjdGVkXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBtc2cgPSBfdChcIlRoZXJlIHdhcyBhbiBlcnJvciBqb2luaW5nIHRoZSByb29tXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChlcnIuZXJyY29kZSA9PT0gJ01fSU5DT01QQVRJQkxFX1JPT01fVkVSU0lPTicpIHtcclxuICAgICAgICAgICAgICAgIG1zZyA9IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiU29ycnksIHlvdXIgaG9tZXNlcnZlciBpcyB0b28gb2xkIHRvIHBhcnRpY2lwYXRlIGluIHRoaXMgcm9vbS5cIil9PGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAge190KFwiUGxlYXNlIGNvbnRhY3QgeW91ciBob21lc2VydmVyIGFkbWluaXN0cmF0b3IuXCIpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBqb2luIHJvb20nLCAnJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkZhaWxlZCB0byBqb2luIHJvb21cIiksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbXNnLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfam9pblJvb21FcnJvcihwYXlsb2FkKSB7XHJcbiAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICBqb2luaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgam9pbkVycm9yOiBwYXlsb2FkLmVycixcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldCgpIHtcclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IE9iamVjdC5hc3NpZ24oe30sIElOSVRJQUxfU1RBVEUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZSByb29tIElEIG9mIHRoZSByb29tIGN1cnJlbnRseSBiZWluZyB2aWV3ZWRcclxuICAgIGdldFJvb21JZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUucm9vbUlkO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZSBldmVudCB0byBzY3JvbGwgdG8gd2hlbiB0aGUgcm9vbSBpcyBmaXJzdCB2aWV3ZWRcclxuICAgIGdldEluaXRpYWxFdmVudElkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGF0ZS5pbml0aWFsRXZlbnRJZDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBXaGV0aGVyIHRvIGhpZ2hsaWdodCB0aGUgaW5pdGlhbCBldmVudFxyXG4gICAgaXNJbml0aWFsRXZlbnRIaWdobGlnaHRlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUuaXNJbml0aWFsRXZlbnRIaWdobGlnaHRlZDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUaGUgcm9vbSBhbGlhcyBvZiB0aGUgcm9vbSAob3IgbnVsbCBpZiBub3Qgb3JpZ2luYWxseSBzcGVjaWZpZWQgaW4gdmlld19yb29tKVxyXG4gICAgZ2V0Um9vbUFsaWFzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGF0ZS5yb29tQWxpYXM7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gV2hldGhlciB0aGUgY3VycmVudCByb29tIGlzIGxvYWRpbmcgKHRydWUgd2hpbHN0IHJlc29sdmluZyBhbiBhbGlhcylcclxuICAgIGlzUm9vbUxvYWRpbmcoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlLnJvb21Mb2FkaW5nO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFueSBlcnJvciB0aGF0IGhhcyBvY2N1cnJlZCBkdXJpbmcgbG9hZGluZ1xyXG4gICAgZ2V0Um9vbUxvYWRFcnJvcigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUucm9vbUxvYWRFcnJvcjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUcnVlIGlmIHdlJ3JlIGV4cGVjdGluZyB0aGUgdXNlciB0byBiZSBqb2luZWQgdG8gdGhlIHJvb20gY3VycmVudGx5IGJlaW5nXHJcbiAgICAvLyB2aWV3ZWQuIE5vdGUgdGhhdCB0aGlzIGlzIGxlZnQgdHJ1ZSBhZnRlciB0aGUgam9pbiByZXF1ZXN0IGhhcyBmaW5pc2hlZCxcclxuICAgIC8vIHNpbmNlIHdlIHNob3VsZCBzdGlsbCBjb25zaWRlciBhIGpvaW4gdG8gYmUgaW4gcHJvZ3Jlc3MgdW50aWwgdGhlIHJvb21cclxuICAgIC8vICYgbWVtYmVyIGV2ZW50cyBjb21lIGRvd24gdGhlIHN5bmMuXHJcbiAgICAvL1xyXG4gICAgLy8gVGhpcyBmbGFnIHJlbWFpbnMgdHJ1ZSBhZnRlciB0aGUgcm9vbSBoYXMgYmVlbiBzdWNlc3NmdWxseSBqb2luZWQsXHJcbiAgICAvLyAodGhpcyBzdG9yZSBkb2Vzbid0IGxpc3RlbiBmb3IgdGhlIGFwcHJvcHJpYXRlIG1lbWJlciBldmVudHMpXHJcbiAgICAvLyBzbyB5b3Ugc2hvdWxkIGFsd2F5cyBvYnNlcnZlIHRoZSBqb2luZWQgc3RhdGUgZnJvbSB0aGUgbWVtYmVyIGV2ZW50XHJcbiAgICAvLyBpZiBhIHJvb20gb2JqZWN0IGlzIHByZXNlbnQuXHJcbiAgICAvLyBpZS4gVGhlIGNvcnJlY3QgbG9naWMgaXM6XHJcbiAgICAvLyBpZiAocm9vbSkge1xyXG4gICAgLy8gICAgIGlmIChteU1lbWJlci5tZW1iZXJzaGlwID09ICdqb2luZWQnKSB7XHJcbiAgICAvLyAgICAgICAgIC8vIHVzZXIgaXMgam9pbmVkIHRvIHRoZSByb29tXHJcbiAgICAvLyAgICAgfSBlbHNlIHtcclxuICAgIC8vICAgICAgICAgLy8gTm90IGpvaW5lZFxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgICAgaWYgKFJvb21WaWV3U3RvcmUuaXNKb2luaW5nKCkpIHtcclxuICAgIC8vICAgICAgICAgLy8gc2hvdyBzcGlubmVyXHJcbiAgICAvLyAgICAgfSBlbHNlIHtcclxuICAgIC8vICAgICAgICAgLy8gc2hvdyBqb2luIHByb21wdFxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGlzSm9pbmluZygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUuam9pbmluZztcclxuICAgIH1cclxuXHJcbiAgICAvLyBBbnkgZXJyb3IgdGhhdCBoYXMgb2NjdXJyZWQgZHVyaW5nIGpvaW5pbmdcclxuICAgIGdldEpvaW5FcnJvcigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUuam9pbkVycm9yO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZSBteEV2ZW50IGlmIG9uZSBpcyBhYm91dCB0byBiZSBmb3J3YXJkZWRcclxuICAgIGdldEZvcndhcmRpbmdFdmVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUuZm9yd2FyZGluZ0V2ZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZSBteEV2ZW50IGlmIG9uZSBpcyBjdXJyZW50bHkgYmVpbmcgcmVwbGllZCB0by9xdW90ZWRcclxuICAgIGdldFF1b3RpbmdFdmVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUucmVwbHlpbmdUb0V2ZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHNob3VsZFBlZWsoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlLnNob3VsZFBlZWsgJiYgdGhpcy5fc3RhdGUubWF0cml4Q2xpZW50SXNSZWFkeTtcclxuICAgIH1cclxufVxyXG5cclxubGV0IHNpbmdsZXRvblJvb21WaWV3U3RvcmUgPSBudWxsO1xyXG5pZiAoIXNpbmdsZXRvblJvb21WaWV3U3RvcmUpIHtcclxuICAgIHNpbmdsZXRvblJvb21WaWV3U3RvcmUgPSBuZXcgUm9vbVZpZXdTdG9yZSgpO1xyXG59XHJcbmV4cG9ydCBkZWZhdWx0IHNpbmdsZXRvblJvb21WaWV3U3RvcmU7XHJcbiJdfQ==