"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _events = _interopRequireDefault(require("events"));

/*
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Acts as a place to get & set widget state, storing local echo state and
 * proxying through state from the js-sdk.
 */
class WidgetEchoStore extends _events.default {
  constructor() {
    super();
    this._roomWidgetEcho = {// Map as below. Object is the content of the widget state event,
      // so for widgets that have been deleted locally, the object is empty.
      // roomId: {
      //     widgetId: [object]
      // }
    };
  }
  /**
   * Gets the widgets for a room, substracting those that are pending deletion.
   * Widgets that are pending addition are not included, since widgets are
   * represted as MatrixEvents, so to do this we'd have to create fake MatrixEvents,
   * and we don't really need the actual widget events anyway since we just want to
   * show a spinner / prevent widgets being added twice.
   *
   * @param {Room} roomId The ID of the room to get widgets for
   * @param {MatrixEvent[]} currentRoomWidgets Current widgets for the room
   * @returns {MatrixEvent[]} List of widgets in the room, minus any pending removal
   */


  getEchoedRoomWidgets(roomId, currentRoomWidgets) {
    const echoedWidgets = [];
    const roomEchoState = Object.assign({}, this._roomWidgetEcho[roomId]);

    for (const w of currentRoomWidgets) {
      const widgetId = w.getStateKey(); // If there's no echo, or the echo still has a widget present, show the *old* widget
      // we don't include widgets that have changed for the same reason we don't include new ones,
      // ie. we'd need to fake matrix events to do so and therte's currently no need.

      if (!roomEchoState[widgetId] || Object.keys(roomEchoState[widgetId]).length !== 0) {
        echoedWidgets.push(w);
      }

      delete roomEchoState[widgetId];
    }

    return echoedWidgets;
  }

  roomHasPendingWidgetsOfType(roomId, currentRoomWidgets, type) {
    const roomEchoState = Object.assign({}, this._roomWidgetEcho[roomId]); // any widget IDs that are already in the room are not pending, so
    // echoes for them don't count as pending.

    for (const w of currentRoomWidgets) {
      const widgetId = w.getStateKey();
      delete roomEchoState[widgetId];
    } // if there's anything left then there are pending widgets.


    if (type === undefined) {
      return Object.keys(roomEchoState).length > 0;
    } else {
      return Object.values(roomEchoState).some(widget => {
        return widget.type === type;
      });
    }
  }

  roomHasPendingWidgets(roomId, currentRoomWidgets) {
    return this.roomHasPendingWidgetsOfType(roomId, currentRoomWidgets);
  }

  setRoomWidgetEcho(roomId, widgetId, state) {
    if (this._roomWidgetEcho[roomId] === undefined) this._roomWidgetEcho[roomId] = {};
    this._roomWidgetEcho[roomId][widgetId] = state;
    this.emit('update');
  }

  removeRoomWidgetEcho(roomId, widgetId) {
    delete this._roomWidgetEcho[roomId][widgetId];
    if (Object.keys(this._roomWidgetEcho[roomId]).length === 0) delete this._roomWidgetEcho[roomId];
    this.emit('update');
  }

}

let singletonWidgetEchoStore = null;

if (!singletonWidgetEchoStore) {
  singletonWidgetEchoStore = new WidgetEchoStore();
}

var _default = singletonWidgetEchoStore;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zdG9yZXMvV2lkZ2V0RWNob1N0b3JlLmpzIl0sIm5hbWVzIjpbIldpZGdldEVjaG9TdG9yZSIsIkV2ZW50RW1pdHRlciIsImNvbnN0cnVjdG9yIiwiX3Jvb21XaWRnZXRFY2hvIiwiZ2V0RWNob2VkUm9vbVdpZGdldHMiLCJyb29tSWQiLCJjdXJyZW50Um9vbVdpZGdldHMiLCJlY2hvZWRXaWRnZXRzIiwicm9vbUVjaG9TdGF0ZSIsIk9iamVjdCIsImFzc2lnbiIsInciLCJ3aWRnZXRJZCIsImdldFN0YXRlS2V5Iiwia2V5cyIsImxlbmd0aCIsInB1c2giLCJyb29tSGFzUGVuZGluZ1dpZGdldHNPZlR5cGUiLCJ0eXBlIiwidW5kZWZpbmVkIiwidmFsdWVzIiwic29tZSIsIndpZGdldCIsInJvb21IYXNQZW5kaW5nV2lkZ2V0cyIsInNldFJvb21XaWRnZXRFY2hvIiwic3RhdGUiLCJlbWl0IiwicmVtb3ZlUm9vbVdpZGdldEVjaG8iLCJzaW5nbGV0b25XaWRnZXRFY2hvU3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWlCQTs7QUFqQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7O0FBSUEsTUFBTUEsZUFBTixTQUE4QkMsZUFBOUIsQ0FBMkM7QUFDdkNDLEVBQUFBLFdBQVcsR0FBRztBQUNWO0FBRUEsU0FBS0MsZUFBTCxHQUF1QixDQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTG1CLEtBQXZCO0FBT0g7QUFFRDs7Ozs7Ozs7Ozs7OztBQVdBQyxFQUFBQSxvQkFBb0IsQ0FBQ0MsTUFBRCxFQUFTQyxrQkFBVCxFQUE2QjtBQUM3QyxVQUFNQyxhQUFhLEdBQUcsRUFBdEI7QUFFQSxVQUFNQyxhQUFhLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS1AsZUFBTCxDQUFxQkUsTUFBckIsQ0FBbEIsQ0FBdEI7O0FBRUEsU0FBSyxNQUFNTSxDQUFYLElBQWdCTCxrQkFBaEIsRUFBb0M7QUFDaEMsWUFBTU0sUUFBUSxHQUFHRCxDQUFDLENBQUNFLFdBQUYsRUFBakIsQ0FEZ0MsQ0FFaEM7QUFDQTtBQUNBOztBQUNBLFVBQUksQ0FBQ0wsYUFBYSxDQUFDSSxRQUFELENBQWQsSUFBNEJILE1BQU0sQ0FBQ0ssSUFBUCxDQUFZTixhQUFhLENBQUNJLFFBQUQsQ0FBekIsRUFBcUNHLE1BQXJDLEtBQWdELENBQWhGLEVBQW1GO0FBQy9FUixRQUFBQSxhQUFhLENBQUNTLElBQWQsQ0FBbUJMLENBQW5CO0FBQ0g7O0FBQ0QsYUFBT0gsYUFBYSxDQUFDSSxRQUFELENBQXBCO0FBQ0g7O0FBRUQsV0FBT0wsYUFBUDtBQUNIOztBQUVEVSxFQUFBQSwyQkFBMkIsQ0FBQ1osTUFBRCxFQUFTQyxrQkFBVCxFQUE2QlksSUFBN0IsRUFBbUM7QUFDMUQsVUFBTVYsYUFBYSxHQUFHQyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtQLGVBQUwsQ0FBcUJFLE1BQXJCLENBQWxCLENBQXRCLENBRDBELENBRzFEO0FBQ0E7O0FBQ0EsU0FBSyxNQUFNTSxDQUFYLElBQWdCTCxrQkFBaEIsRUFBb0M7QUFDaEMsWUFBTU0sUUFBUSxHQUFHRCxDQUFDLENBQUNFLFdBQUYsRUFBakI7QUFDQSxhQUFPTCxhQUFhLENBQUNJLFFBQUQsQ0FBcEI7QUFDSCxLQVJ5RCxDQVUxRDs7O0FBQ0EsUUFBSU0sSUFBSSxLQUFLQyxTQUFiLEVBQXdCO0FBQ3BCLGFBQU9WLE1BQU0sQ0FBQ0ssSUFBUCxDQUFZTixhQUFaLEVBQTJCTyxNQUEzQixHQUFvQyxDQUEzQztBQUNILEtBRkQsTUFFTztBQUNILGFBQU9OLE1BQU0sQ0FBQ1csTUFBUCxDQUFjWixhQUFkLEVBQTZCYSxJQUE3QixDQUFtQ0MsTUFBRCxJQUFZO0FBQ2pELGVBQU9BLE1BQU0sQ0FBQ0osSUFBUCxLQUFnQkEsSUFBdkI7QUFDSCxPQUZNLENBQVA7QUFHSDtBQUNKOztBQUVESyxFQUFBQSxxQkFBcUIsQ0FBQ2xCLE1BQUQsRUFBU0Msa0JBQVQsRUFBNkI7QUFDOUMsV0FBTyxLQUFLVywyQkFBTCxDQUFpQ1osTUFBakMsRUFBeUNDLGtCQUF6QyxDQUFQO0FBQ0g7O0FBRURrQixFQUFBQSxpQkFBaUIsQ0FBQ25CLE1BQUQsRUFBU08sUUFBVCxFQUFtQmEsS0FBbkIsRUFBMEI7QUFDdkMsUUFBSSxLQUFLdEIsZUFBTCxDQUFxQkUsTUFBckIsTUFBaUNjLFNBQXJDLEVBQWdELEtBQUtoQixlQUFMLENBQXFCRSxNQUFyQixJQUErQixFQUEvQjtBQUVoRCxTQUFLRixlQUFMLENBQXFCRSxNQUFyQixFQUE2Qk8sUUFBN0IsSUFBeUNhLEtBQXpDO0FBQ0EsU0FBS0MsSUFBTCxDQUFVLFFBQVY7QUFDSDs7QUFFREMsRUFBQUEsb0JBQW9CLENBQUN0QixNQUFELEVBQVNPLFFBQVQsRUFBbUI7QUFDbkMsV0FBTyxLQUFLVCxlQUFMLENBQXFCRSxNQUFyQixFQUE2Qk8sUUFBN0IsQ0FBUDtBQUNBLFFBQUlILE1BQU0sQ0FBQ0ssSUFBUCxDQUFZLEtBQUtYLGVBQUwsQ0FBcUJFLE1BQXJCLENBQVosRUFBMENVLE1BQTFDLEtBQXFELENBQXpELEVBQTRELE9BQU8sS0FBS1osZUFBTCxDQUFxQkUsTUFBckIsQ0FBUDtBQUM1RCxTQUFLcUIsSUFBTCxDQUFVLFFBQVY7QUFDSDs7QUE5RXNDOztBQWlGM0MsSUFBSUUsd0JBQXdCLEdBQUcsSUFBL0I7O0FBQ0EsSUFBSSxDQUFDQSx3QkFBTCxFQUErQjtBQUMzQkEsRUFBQUEsd0JBQXdCLEdBQUcsSUFBSTVCLGVBQUosRUFBM0I7QUFDSDs7ZUFDYzRCLHdCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgRXZlbnRFbWl0dGVyIGZyb20gJ2V2ZW50cyc7XHJcblxyXG4vKipcclxuICogQWN0cyBhcyBhIHBsYWNlIHRvIGdldCAmIHNldCB3aWRnZXQgc3RhdGUsIHN0b3JpbmcgbG9jYWwgZWNobyBzdGF0ZSBhbmRcclxuICogcHJveHlpbmcgdGhyb3VnaCBzdGF0ZSBmcm9tIHRoZSBqcy1zZGsuXHJcbiAqL1xyXG5jbGFzcyBXaWRnZXRFY2hvU3RvcmUgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5fcm9vbVdpZGdldEVjaG8gPSB7XHJcbiAgICAgICAgICAgIC8vIE1hcCBhcyBiZWxvdy4gT2JqZWN0IGlzIHRoZSBjb250ZW50IG9mIHRoZSB3aWRnZXQgc3RhdGUgZXZlbnQsXHJcbiAgICAgICAgICAgIC8vIHNvIGZvciB3aWRnZXRzIHRoYXQgaGF2ZSBiZWVuIGRlbGV0ZWQgbG9jYWxseSwgdGhlIG9iamVjdCBpcyBlbXB0eS5cclxuICAgICAgICAgICAgLy8gcm9vbUlkOiB7XHJcbiAgICAgICAgICAgIC8vICAgICB3aWRnZXRJZDogW29iamVjdF1cclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSB3aWRnZXRzIGZvciBhIHJvb20sIHN1YnN0cmFjdGluZyB0aG9zZSB0aGF0IGFyZSBwZW5kaW5nIGRlbGV0aW9uLlxyXG4gICAgICogV2lkZ2V0cyB0aGF0IGFyZSBwZW5kaW5nIGFkZGl0aW9uIGFyZSBub3QgaW5jbHVkZWQsIHNpbmNlIHdpZGdldHMgYXJlXHJcbiAgICAgKiByZXByZXN0ZWQgYXMgTWF0cml4RXZlbnRzLCBzbyB0byBkbyB0aGlzIHdlJ2QgaGF2ZSB0byBjcmVhdGUgZmFrZSBNYXRyaXhFdmVudHMsXHJcbiAgICAgKiBhbmQgd2UgZG9uJ3QgcmVhbGx5IG5lZWQgdGhlIGFjdHVhbCB3aWRnZXQgZXZlbnRzIGFueXdheSBzaW5jZSB3ZSBqdXN0IHdhbnQgdG9cclxuICAgICAqIHNob3cgYSBzcGlubmVyIC8gcHJldmVudCB3aWRnZXRzIGJlaW5nIGFkZGVkIHR3aWNlLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7Um9vbX0gcm9vbUlkIFRoZSBJRCBvZiB0aGUgcm9vbSB0byBnZXQgd2lkZ2V0cyBmb3JcclxuICAgICAqIEBwYXJhbSB7TWF0cml4RXZlbnRbXX0gY3VycmVudFJvb21XaWRnZXRzIEN1cnJlbnQgd2lkZ2V0cyBmb3IgdGhlIHJvb21cclxuICAgICAqIEByZXR1cm5zIHtNYXRyaXhFdmVudFtdfSBMaXN0IG9mIHdpZGdldHMgaW4gdGhlIHJvb20sIG1pbnVzIGFueSBwZW5kaW5nIHJlbW92YWxcclxuICAgICAqL1xyXG4gICAgZ2V0RWNob2VkUm9vbVdpZGdldHMocm9vbUlkLCBjdXJyZW50Um9vbVdpZGdldHMpIHtcclxuICAgICAgICBjb25zdCBlY2hvZWRXaWRnZXRzID0gW107XHJcblxyXG4gICAgICAgIGNvbnN0IHJvb21FY2hvU3RhdGUgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLl9yb29tV2lkZ2V0RWNob1tyb29tSWRdKTtcclxuXHJcbiAgICAgICAgZm9yIChjb25zdCB3IG9mIGN1cnJlbnRSb29tV2lkZ2V0cykge1xyXG4gICAgICAgICAgICBjb25zdCB3aWRnZXRJZCA9IHcuZ2V0U3RhdGVLZXkoKTtcclxuICAgICAgICAgICAgLy8gSWYgdGhlcmUncyBubyBlY2hvLCBvciB0aGUgZWNobyBzdGlsbCBoYXMgYSB3aWRnZXQgcHJlc2VudCwgc2hvdyB0aGUgKm9sZCogd2lkZ2V0XHJcbiAgICAgICAgICAgIC8vIHdlIGRvbid0IGluY2x1ZGUgd2lkZ2V0cyB0aGF0IGhhdmUgY2hhbmdlZCBmb3IgdGhlIHNhbWUgcmVhc29uIHdlIGRvbid0IGluY2x1ZGUgbmV3IG9uZXMsXHJcbiAgICAgICAgICAgIC8vIGllLiB3ZSdkIG5lZWQgdG8gZmFrZSBtYXRyaXggZXZlbnRzIHRvIGRvIHNvIGFuZCB0aGVydGUncyBjdXJyZW50bHkgbm8gbmVlZC5cclxuICAgICAgICAgICAgaWYgKCFyb29tRWNob1N0YXRlW3dpZGdldElkXSB8fCBPYmplY3Qua2V5cyhyb29tRWNob1N0YXRlW3dpZGdldElkXSkubGVuZ3RoICE9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBlY2hvZWRXaWRnZXRzLnB1c2godyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZGVsZXRlIHJvb21FY2hvU3RhdGVbd2lkZ2V0SWRdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGVjaG9lZFdpZGdldHM7XHJcbiAgICB9XHJcblxyXG4gICAgcm9vbUhhc1BlbmRpbmdXaWRnZXRzT2ZUeXBlKHJvb21JZCwgY3VycmVudFJvb21XaWRnZXRzLCB0eXBlKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbUVjaG9TdGF0ZSA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuX3Jvb21XaWRnZXRFY2hvW3Jvb21JZF0pO1xyXG5cclxuICAgICAgICAvLyBhbnkgd2lkZ2V0IElEcyB0aGF0IGFyZSBhbHJlYWR5IGluIHRoZSByb29tIGFyZSBub3QgcGVuZGluZywgc29cclxuICAgICAgICAvLyBlY2hvZXMgZm9yIHRoZW0gZG9uJ3QgY291bnQgYXMgcGVuZGluZy5cclxuICAgICAgICBmb3IgKGNvbnN0IHcgb2YgY3VycmVudFJvb21XaWRnZXRzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHdpZGdldElkID0gdy5nZXRTdGF0ZUtleSgpO1xyXG4gICAgICAgICAgICBkZWxldGUgcm9vbUVjaG9TdGF0ZVt3aWRnZXRJZF07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZiB0aGVyZSdzIGFueXRoaW5nIGxlZnQgdGhlbiB0aGVyZSBhcmUgcGVuZGluZyB3aWRnZXRzLlxyXG4gICAgICAgIGlmICh0eXBlID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKHJvb21FY2hvU3RhdGUpLmxlbmd0aCA+IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC52YWx1ZXMocm9vbUVjaG9TdGF0ZSkuc29tZSgod2lkZ2V0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gd2lkZ2V0LnR5cGUgPT09IHR5cGU7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByb29tSGFzUGVuZGluZ1dpZGdldHMocm9vbUlkLCBjdXJyZW50Um9vbVdpZGdldHMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yb29tSGFzUGVuZGluZ1dpZGdldHNPZlR5cGUocm9vbUlkLCBjdXJyZW50Um9vbVdpZGdldHMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFJvb21XaWRnZXRFY2hvKHJvb21JZCwgd2lkZ2V0SWQsIHN0YXRlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3Jvb21XaWRnZXRFY2hvW3Jvb21JZF0gPT09IHVuZGVmaW5lZCkgdGhpcy5fcm9vbVdpZGdldEVjaG9bcm9vbUlkXSA9IHt9O1xyXG5cclxuICAgICAgICB0aGlzLl9yb29tV2lkZ2V0RWNob1tyb29tSWRdW3dpZGdldElkXSA9IHN0YXRlO1xyXG4gICAgICAgIHRoaXMuZW1pdCgndXBkYXRlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlUm9vbVdpZGdldEVjaG8ocm9vbUlkLCB3aWRnZXRJZCkge1xyXG4gICAgICAgIGRlbGV0ZSB0aGlzLl9yb29tV2lkZ2V0RWNob1tyb29tSWRdW3dpZGdldElkXTtcclxuICAgICAgICBpZiAoT2JqZWN0LmtleXModGhpcy5fcm9vbVdpZGdldEVjaG9bcm9vbUlkXSkubGVuZ3RoID09PSAwKSBkZWxldGUgdGhpcy5fcm9vbVdpZGdldEVjaG9bcm9vbUlkXTtcclxuICAgICAgICB0aGlzLmVtaXQoJ3VwZGF0ZScpO1xyXG4gICAgfVxyXG59XHJcblxyXG5sZXQgc2luZ2xldG9uV2lkZ2V0RWNob1N0b3JlID0gbnVsbDtcclxuaWYgKCFzaW5nbGV0b25XaWRnZXRFY2hvU3RvcmUpIHtcclxuICAgIHNpbmdsZXRvbldpZGdldEVjaG9TdG9yZSA9IG5ldyBXaWRnZXRFY2hvU3RvcmUoKTtcclxufVxyXG5leHBvcnQgZGVmYXVsdCBzaW5nbGV0b25XaWRnZXRFY2hvU3RvcmU7XHJcbiJdfQ==