"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var _utils = require("flux/utils");

/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const INITIAL_STATE = {
  deferred_action: null
};
/**
 * A class for storing application state to do with authentication. This is a simple flux
 * store that listens for actions and updates its state accordingly, informing any
 * listeners (views) of state changes.
 */

class LifecycleStore extends _utils.Store {
  constructor() {
    super(_dispatcher.default); // Initialise state

    this._state = INITIAL_STATE;
  }

  _setState(newState) {
    this._state = Object.assign(this._state, newState);

    this.__emitChange();
  }

  __onDispatch(payload) {
    switch (payload.action) {
      case 'do_after_sync_prepared':
        this._setState({
          deferred_action: payload.deferred_action
        });

        break;

      case 'cancel_after_sync_prepared':
        this._setState({
          deferred_action: null
        });

        break;

      case 'sync_state':
        {
          if (payload.state !== 'PREPARED') {
            break;
          }

          if (!this._state.deferred_action) break;
          const deferredAction = Object.assign({}, this._state.deferred_action);

          this._setState({
            deferred_action: null
          });

          _dispatcher.default.dispatch(deferredAction);

          break;
        }

      case 'on_client_not_viable':
      case 'on_logged_out':
        this.reset();
        break;
    }
  }

  reset() {
    this._state = Object.assign({}, INITIAL_STATE);
  }

}

let singletonLifecycleStore = null;

if (!singletonLifecycleStore) {
  singletonLifecycleStore = new LifecycleStore();
}

var _default = singletonLifecycleStore;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zdG9yZXMvTGlmZWN5Y2xlU3RvcmUuanMiXSwibmFtZXMiOlsiSU5JVElBTF9TVEFURSIsImRlZmVycmVkX2FjdGlvbiIsIkxpZmVjeWNsZVN0b3JlIiwiU3RvcmUiLCJjb25zdHJ1Y3RvciIsImRpcyIsIl9zdGF0ZSIsIl9zZXRTdGF0ZSIsIm5ld1N0YXRlIiwiT2JqZWN0IiwiYXNzaWduIiwiX19lbWl0Q2hhbmdlIiwiX19vbkRpc3BhdGNoIiwicGF5bG9hZCIsImFjdGlvbiIsInN0YXRlIiwiZGVmZXJyZWRBY3Rpb24iLCJkaXNwYXRjaCIsInJlc2V0Iiwic2luZ2xldG9uTGlmZWN5Y2xlU3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFsQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBLE1BQU1BLGFBQWEsR0FBRztBQUNsQkMsRUFBQUEsZUFBZSxFQUFFO0FBREMsQ0FBdEI7QUFJQTs7Ozs7O0FBS0EsTUFBTUMsY0FBTixTQUE2QkMsWUFBN0IsQ0FBbUM7QUFDL0JDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFVBQU1DLG1CQUFOLEVBRFUsQ0FHVjs7QUFDQSxTQUFLQyxNQUFMLEdBQWNOLGFBQWQ7QUFDSDs7QUFFRE8sRUFBQUEsU0FBUyxDQUFDQyxRQUFELEVBQVc7QUFDaEIsU0FBS0YsTUFBTCxHQUFjRyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxLQUFLSixNQUFuQixFQUEyQkUsUUFBM0IsQ0FBZDs7QUFDQSxTQUFLRyxZQUFMO0FBQ0g7O0FBRURDLEVBQUFBLFlBQVksQ0FBQ0MsT0FBRCxFQUFVO0FBQ2xCLFlBQVFBLE9BQU8sQ0FBQ0MsTUFBaEI7QUFDSSxXQUFLLHdCQUFMO0FBQ0ksYUFBS1AsU0FBTCxDQUFlO0FBQ1hOLFVBQUFBLGVBQWUsRUFBRVksT0FBTyxDQUFDWjtBQURkLFNBQWY7O0FBR0E7O0FBQ0osV0FBSyw0QkFBTDtBQUNJLGFBQUtNLFNBQUwsQ0FBZTtBQUNYTixVQUFBQSxlQUFlLEVBQUU7QUFETixTQUFmOztBQUdBOztBQUNKLFdBQUssWUFBTDtBQUFtQjtBQUNmLGNBQUlZLE9BQU8sQ0FBQ0UsS0FBUixLQUFrQixVQUF0QixFQUFrQztBQUM5QjtBQUNIOztBQUNELGNBQUksQ0FBQyxLQUFLVCxNQUFMLENBQVlMLGVBQWpCLEVBQWtDO0FBQ2xDLGdCQUFNZSxjQUFjLEdBQUdQLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS0osTUFBTCxDQUFZTCxlQUE5QixDQUF2Qjs7QUFDQSxlQUFLTSxTQUFMLENBQWU7QUFDWE4sWUFBQUEsZUFBZSxFQUFFO0FBRE4sV0FBZjs7QUFHQUksOEJBQUlZLFFBQUosQ0FBYUQsY0FBYjs7QUFDQTtBQUNIOztBQUNELFdBQUssc0JBQUw7QUFDQSxXQUFLLGVBQUw7QUFDSSxhQUFLRSxLQUFMO0FBQ0E7QUExQlI7QUE0Qkg7O0FBRURBLEVBQUFBLEtBQUssR0FBRztBQUNKLFNBQUtaLE1BQUwsR0FBY0csTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQlYsYUFBbEIsQ0FBZDtBQUNIOztBQTlDOEI7O0FBaURuQyxJQUFJbUIsdUJBQXVCLEdBQUcsSUFBOUI7O0FBQ0EsSUFBSSxDQUFDQSx1QkFBTCxFQUE4QjtBQUMxQkEsRUFBQUEsdUJBQXVCLEdBQUcsSUFBSWpCLGNBQUosRUFBMUI7QUFDSDs7ZUFDY2lCLHVCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7U3RvcmV9IGZyb20gJ2ZsdXgvdXRpbHMnO1xyXG5cclxuY29uc3QgSU5JVElBTF9TVEFURSA9IHtcclxuICAgIGRlZmVycmVkX2FjdGlvbjogbnVsbCxcclxufTtcclxuXHJcbi8qKlxyXG4gKiBBIGNsYXNzIGZvciBzdG9yaW5nIGFwcGxpY2F0aW9uIHN0YXRlIHRvIGRvIHdpdGggYXV0aGVudGljYXRpb24uIFRoaXMgaXMgYSBzaW1wbGUgZmx1eFxyXG4gKiBzdG9yZSB0aGF0IGxpc3RlbnMgZm9yIGFjdGlvbnMgYW5kIHVwZGF0ZXMgaXRzIHN0YXRlIGFjY29yZGluZ2x5LCBpbmZvcm1pbmcgYW55XHJcbiAqIGxpc3RlbmVycyAodmlld3MpIG9mIHN0YXRlIGNoYW5nZXMuXHJcbiAqL1xyXG5jbGFzcyBMaWZlY3ljbGVTdG9yZSBleHRlbmRzIFN0b3JlIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKGRpcyk7XHJcblxyXG4gICAgICAgIC8vIEluaXRpYWxpc2Ugc3RhdGVcclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IElOSVRJQUxfU1RBVEU7XHJcbiAgICB9XHJcblxyXG4gICAgX3NldFN0YXRlKG5ld1N0YXRlKSB7XHJcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBPYmplY3QuYXNzaWduKHRoaXMuX3N0YXRlLCBuZXdTdGF0ZSk7XHJcbiAgICAgICAgdGhpcy5fX2VtaXRDaGFuZ2UoKTtcclxuICAgIH1cclxuXHJcbiAgICBfX29uRGlzcGF0Y2gocGF5bG9hZCkge1xyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgY2FzZSAnZG9fYWZ0ZXJfc3luY19wcmVwYXJlZCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmZXJyZWRfYWN0aW9uOiBwYXlsb2FkLmRlZmVycmVkX2FjdGlvbixcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2NhbmNlbF9hZnRlcl9zeW5jX3ByZXBhcmVkJzpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZF9hY3Rpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdzeW5jX3N0YXRlJzoge1xyXG4gICAgICAgICAgICAgICAgaWYgKHBheWxvYWQuc3RhdGUgIT09ICdQUkVQQVJFRCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5fc3RhdGUuZGVmZXJyZWRfYWN0aW9uKSBicmVhaztcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlZmVycmVkQWN0aW9uID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5fc3RhdGUuZGVmZXJyZWRfYWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZF9hY3Rpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaChkZWZlcnJlZEFjdGlvbik7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlICdvbl9jbGllbnRfbm90X3ZpYWJsZSc6XHJcbiAgICAgICAgICAgIGNhc2UgJ29uX2xvZ2dlZF9vdXQnOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gT2JqZWN0LmFzc2lnbih7fSwgSU5JVElBTF9TVEFURSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmxldCBzaW5nbGV0b25MaWZlY3ljbGVTdG9yZSA9IG51bGw7XHJcbmlmICghc2luZ2xldG9uTGlmZWN5Y2xlU3RvcmUpIHtcclxuICAgIHNpbmdsZXRvbkxpZmVjeWNsZVN0b3JlID0gbmV3IExpZmVjeWNsZVN0b3JlKCk7XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgc2luZ2xldG9uTGlmZWN5Y2xlU3RvcmU7XHJcbiJdfQ==