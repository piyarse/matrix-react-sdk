"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ALGO_RECENT = exports.ALGO_ALPHABETIC = exports.ALGO_MANUAL = exports.TAG_DM = void 0;

var _utils = require("flux/utils");

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var _DMRoomMap = _interopRequireDefault(require("../utils/DMRoomMap"));

var Unread = _interopRequireWildcard(require("../Unread"));

var _SettingsStore = _interopRequireDefault(require("../settings/SettingsStore"));

/*
Copyright 2018, 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
Room sorting algorithm:
* Always prefer to have red > grey > bold > idle
* The room being viewed should be sticky (not jump down to the idle list)
* When switching to a new room, sort the last sticky room to the top of the idle list.

The approach taken by the store is to generate an initial representation of all the
tagged lists (accepting that it'll take a little bit longer to calculate) and make
small changes to that over time. This results in quick changes to the room list while
also having update operations feel more like popping/pushing to a stack.
 */
const CATEGORY_RED = "red"; // Mentions in the room

const CATEGORY_GREY = "grey"; // Unread notified messages (not mentions)

const CATEGORY_BOLD = "bold"; // Unread messages (not notified, 'Mentions Only' rooms)

const CATEGORY_IDLE = "idle"; // Nothing of interest

const TAG_DM = "im.vector.fake.direct";
/**
 * Identifier for manual sorting behaviour: sort by the user defined order.
 * @type {string}
 */

exports.TAG_DM = TAG_DM;
const ALGO_MANUAL = "manual";
/**
 * Identifier for alphabetic sorting behaviour: sort by the room name alphabetically first.
 * @type {string}
 */

exports.ALGO_MANUAL = ALGO_MANUAL;
const ALGO_ALPHABETIC = "alphabetic";
/**
 * Identifier for classic sorting behaviour: sort by the most recent message first.
 * @type {string}
 */

exports.ALGO_ALPHABETIC = ALGO_ALPHABETIC;
const ALGO_RECENT = "recent";
exports.ALGO_RECENT = ALGO_RECENT;
const CATEGORY_ORDER = [CATEGORY_RED, CATEGORY_GREY, CATEGORY_BOLD, CATEGORY_IDLE];

const getListAlgorithm = (listKey, settingAlgorithm) => {
  // apply manual sorting only to m.favourite, otherwise respect the global setting
  // all the known tags are listed explicitly here to simplify future changes
  switch (listKey) {
    case "im.vector.fake.invite":
    case "im.vector.fake.recent":
    case "im.vector.fake.archived":
    case "m.lowpriority":
    case TAG_DM:
      return settingAlgorithm;

    case "m.favourite":
    default:
      // custom-tags
      return ALGO_MANUAL;
  }
};

const knownLists = new Set(["m.favourite", "im.vector.fake.invite", "im.vector.fake.recent", "im.vector.fake.archived", "m.lowpriority", TAG_DM]);
/**
 * A class for storing application state for categorising rooms in
 * the RoomList.
 */

class RoomListStore extends _utils.Store {
  constructor() {
    super(_dispatcher.default);

    this._init();

    this._getManualComparator = this._getManualComparator.bind(this);
    this._recentsComparator = this._recentsComparator.bind(this);
  }
  /**
   * Changes the sorting algorithm used by the RoomListStore.
   * @param {string} algorithm The new algorithm to use. Should be one of the ALGO_* constants.
   * @param {boolean} orderImportantFirst Whether to sort by categories of importance
   */


  updateSortingAlgorithm(algorithm, orderImportantFirst) {
    // Dev note: We only have two algorithms at the moment, but it isn't impossible that we want
    // multiple in the future. Also constants make things slightly clearer.
    console.log("Updating room sorting algorithm: ", {
      algorithm,
      orderImportantFirst
    });

    this._setState({
      algorithm,
      orderImportantFirst
    }); // Trigger a resort of the entire list to reflect the change in algorithm


    this._generateInitialRoomLists();
  }

  _init() {
    // Initialise state
    const defaultLists = {
      "m.server_notice": [
        /* { room: js-sdk room, category: string } */
      ],
      "im.vector.fake.invite": [],
      "m.favourite": [],
      "im.vector.fake.recent": [],
      [TAG_DM]: [],
      "m.lowpriority": [],
      "im.vector.fake.archived": []
    };
    this._state = {
      // The rooms in these arrays are ordered according to either the
      // 'recents' behaviour or 'manual' behaviour.
      lists: defaultLists,
      presentationLists: defaultLists,
      // like `lists`, but with arrays of rooms instead
      ready: false,
      stickyRoomId: null,
      algorithm: ALGO_RECENT,
      orderImportantFirst: false
    };

    _SettingsStore.default.monitorSetting('RoomList.orderAlphabetically', null);

    _SettingsStore.default.monitorSetting('RoomList.orderByImportance', null);

    _SettingsStore.default.monitorSetting('feature_custom_tags', null);
  }

  _setState(newState) {
    // If we're changing the lists, transparently change the presentation lists (which
    // is given to requesting components). This dramatically simplifies our code elsewhere
    // while also ensuring we don't need to update all the calling components to support
    // categories.
    if (newState['lists']) {
      const presentationLists = {};

      for (const key of Object.keys(newState['lists'])) {
        presentationLists[key] = newState['lists'][key].map(e => e.room);
      }

      newState['presentationLists'] = presentationLists;
    }

    this._state = Object.assign(this._state, newState);

    this.__emitChange();
  }

  __onDispatch(payload) {
    const logicallyReady = this._matrixClient && this._state.ready;

    switch (payload.action) {
      case 'setting_updated':
        {
          if (!logicallyReady) break;

          switch (payload.settingName) {
            case "RoomList.orderAlphabetically":
              this.updateSortingAlgorithm(payload.newValue ? ALGO_ALPHABETIC : ALGO_RECENT, this._state.orderImportantFirst);
              break;

            case "RoomList.orderByImportance":
              this.updateSortingAlgorithm(this._state.algorithm, payload.newValue);
              break;

            case "feature_custom_tags":
              this._setState({
                tagsEnabled: payload.newValue
              });

              this._generateInitialRoomLists(); // Tags means we have to start from scratch


              break;
          }
        }
        break;
      // Initialise state after initial sync

      case 'MatrixActions.sync':
        {
          if (!(payload.prevState !== 'PREPARED' && payload.state === 'PREPARED')) {
            break;
          } // Always ensure that we set any state needed for settings here. It is possible that
          // setting updates trigger on startup before we are ready to sync, so we want to make
          // sure that the right state is in place before we actually react to those changes.


          this._setState({
            tagsEnabled: _SettingsStore.default.isFeatureEnabled("feature_custom_tags")
          });

          this._matrixClient = payload.matrixClient;

          const orderByImportance = _SettingsStore.default.getValue("RoomList.orderByImportance");

          const orderAlphabetically = _SettingsStore.default.getValue("RoomList.orderAlphabetically");

          this.updateSortingAlgorithm(orderAlphabetically ? ALGO_ALPHABETIC : ALGO_RECENT, orderByImportance);
        }
        break;

      case 'MatrixActions.Room.receipt':
        {
          if (!logicallyReady) break; // First see if the receipt event is for our own user. If it was, trigger
          // a room update (we probably read the room on a different device).

          const myUserId = this._matrixClient.getUserId();

          for (const eventId of Object.keys(payload.event.getContent())) {
            const receiptUsers = Object.keys(payload.event.getContent()[eventId]['m.read'] || {});

            if (receiptUsers.includes(myUserId)) {
              this._roomUpdateTriggered(payload.room.roomId);

              return;
            }
          }
        }
        break;

      case 'MatrixActions.Room.tags':
        {
          if (!logicallyReady) break; // TODO: Figure out which rooms changed in the tag and only change those.
          // This is very blunt and wipes out the sticky room stuff

          this._generateInitialRoomLists();
        }
        break;

      case 'MatrixActions.Room.timeline':
        {
          if (!logicallyReady || !payload.isLiveEvent || !payload.isLiveUnfilteredRoomTimelineEvent || !this._eventTriggersRecentReorder(payload.event) || this._state.algorithm !== ALGO_RECENT) {
            break;
          }

          this._roomUpdateTriggered(payload.event.getRoomId());
        }
        break;
      // When an event is decrypted, it could mean we need to reorder the room
      // list because we now know the type of the event.

      case 'MatrixActions.Event.decrypted':
        {
          if (!logicallyReady) break;
          const roomId = payload.event.getRoomId(); // We may have decrypted an event without a roomId (e.g to_device)

          if (!roomId) break;

          const room = this._matrixClient.getRoom(roomId); // We somehow decrypted an event for a room our client is unaware of


          if (!room) break;
          const liveTimeline = room.getLiveTimeline();
          const eventTimeline = room.getTimelineForEvent(payload.event.getId()); // Either this event was not added to the live timeline (e.g. pagination)
          // or it doesn't affect the ordering of the room list.

          if (liveTimeline !== eventTimeline || !this._eventTriggersRecentReorder(payload.event)) {
            break;
          }

          this._roomUpdateTriggered(roomId);
        }
        break;

      case 'MatrixActions.accountData':
        {
          if (!logicallyReady) break;
          if (payload.event_type !== 'm.direct') break; // TODO: Figure out which rooms changed in the direct chat and only change those.
          // This is very blunt and wipes out the sticky room stuff

          this._generateInitialRoomLists();
        }
        break;

      case 'MatrixActions.Room.myMembership':
        {
          if (!logicallyReady) break;

          this._roomUpdateTriggered(payload.room.roomId, true);
        }
        break;
      // This could be a new room that we've been invited to, joined or created
      // we won't get a RoomMember.membership for these cases if we're not already
      // a member.

      case 'MatrixActions.Room':
        {
          if (!logicallyReady) break;

          this._roomUpdateTriggered(payload.room.roomId, true);
        }
        break;
      // TODO: Re-enable optimistic updates when we support dragging again
      // case 'RoomListActions.tagRoom.pending': {
      //     if (!logicallyReady) break;
      //     // XXX: we only show one optimistic update at any one time.
      //     // Ideally we should be making a list of in-flight requests
      //     // that are backed by transaction IDs. Until the js-sdk
      //     // supports this, we're stuck with only being able to use
      //     // the most recent optimistic update.
      //     console.log("!! Optimistic tag: ", payload);
      // }
      // break;
      // case 'RoomListActions.tagRoom.failure': {
      //     if (!logicallyReady) break;
      //     // Reset state according to js-sdk
      //     console.log("!! Optimistic tag failure: ", payload);
      // }
      // break;

      case 'on_client_not_viable':
      case 'on_logged_out':
        {
          // Reset state without pushing an update to the view, which generally assumes that
          // the matrix client isn't `null` and so causing a re-render will cause NPEs.
          this._init();

          this._matrixClient = null;
        }
        break;

      case 'view_room':
        {
          if (!logicallyReady) break; // Note: it is important that we set a new stickyRoomId before setting the old room
          // to IDLE. If we don't, the wrong room gets counted as sticky.

          const currentStickyId = this._state.stickyRoomId;

          this._setState({
            stickyRoomId: payload.room_id
          });

          if (currentStickyId) {
            this._setRoomCategory(this._matrixClient.getRoom(currentStickyId), CATEGORY_IDLE);
          }
        }
        break;
    }
  }

  _roomUpdateTriggered(roomId, ignoreSticky) {
    // We don't calculate categories for sticky rooms because we have a moderate
    // interest in trying to maintain the category that they were last in before
    // being artificially flagged as IDLE. Also, this reduces the amount of time
    // we spend in _setRoomCategory ever so slightly.
    if (this._state.stickyRoomId !== roomId || ignoreSticky) {
      // Micro optimization: Only look up the room if we're confident we'll need it.
      const room = this._matrixClient.getRoom(roomId);

      if (!room) return;

      const category = this._calculateCategory(room);

      this._setRoomCategory(room, category);
    }
  }

  _filterTags(tags) {
    tags = tags ? Object.keys(tags) : [];
    if (this._state.tagsEnabled) return tags;
    return tags.filter(t => knownLists.has(t));
  }

  _getRecommendedTagsForRoom(room) {
    const tags = [];
    const myMembership = room.getMyMembership();

    if (myMembership === 'join' || myMembership === 'invite') {
      // Stack the user's tags on top
      tags.push(...this._filterTags(room.tags)); // Order matters here: The DMRoomMap updates before invites
      // are accepted, so we check to see if the room is an invite
      // first, then if it is a direct chat, and finally default
      // to the "recents" list.

      const dmRoomMap = _DMRoomMap.default.shared();

      if (myMembership === 'invite') {
        tags.push("im.vector.fake.invite");
      } else if (dmRoomMap.getUserIdForRoomId(room.roomId) && tags.length === 0) {
        // We intentionally don't duplicate rooms in other tags into the people list
        // as a feature.
        tags.push(TAG_DM);
      } else if (tags.length === 0) {
        tags.push("im.vector.fake.recent");
      }
    } else if (myMembership) {
      // null-guard as null means it was peeked
      tags.push("im.vector.fake.archived");
    }

    return tags;
  }

  _slotRoomIntoList(room, category, tag, existingEntries, newList, lastTimestampFn) {
    const targetCategoryIndex = CATEGORY_ORDER.indexOf(category);

    let categoryComparator = (a, b) => lastTimestampFn(a.room) >= lastTimestampFn(b.room);

    const sortAlgorithm = getListAlgorithm(tag, this._state.algorithm);

    if (sortAlgorithm === ALGO_RECENT) {
      categoryComparator = (a, b) => this._recentsComparator(a, b, lastTimestampFn);
    } else if (sortAlgorithm === ALGO_ALPHABETIC) {
      categoryComparator = (a, b) => this._lexicographicalComparator(a, b);
    } // The slotting algorithm works by trying to position the room in the most relevant
    // category of the list (red > grey > etc). To accomplish this, we need to consider
    // a couple cases: the category existing in the list but having other rooms in it and
    // the case of the category simply not existing and needing to be started. In order to
    // do this efficiently, we only want to iterate over the list once and solve our sorting
    // problem as we go.
    //
    // Firstly, we'll remove any existing entry that references the room we're trying to
    // insert. We don't really want to consider the old entry and want to recreate it. We
    // also exclude the sticky (currently active) room from the categorization logic and
    // let it pass through wherever it resides in the list: it shouldn't be moving around
    // the list too much, so we want to keep it where it is.
    //
    // The case of the category we want existing is easy to handle: once we hit the category,
    // find the room that has a most recent event later than our own and insert just before
    // that (making us the more recent room). If we end up hitting the next category before
    // we can slot the room in, insert the room at the top of the category as a fallback. We
    // do this to ensure that the room doesn't go too far down the list given it was previously
    // considered important (in the case of going down in category) or is now more important
    // (suddenly becoming red, for instance). The boundary tracking is how we end up achieving
    // this, as described in the next paragraphs.
    //
    // The other case of the category not already existing is a bit more complicated. We track
    // the boundaries of each category relative to the list we're currently building so that
    // when we miss the category we can insert the room at the right spot. Most importantly, we
    // can't assume that the end of the list being built is the right spot because of the last
    // paragraph's requirement: the room should be put to the top of a category if the category
    // runs out of places to put it.
    //
    // All told, our tracking looks something like this:
    //
    // ------ A <- Category boundary (start of red)
    //  RED
    //  RED
    //  RED
    // ------ B <- In this example, we have a grey room we want to insert.
    //  BOLD
    //  BOLD
    // ------ C
    //  IDLE
    //  IDLE
    // ------ D <- End of list
    //
    // Given that example, and our desire to insert a GREY room into the list, this iterates
    // over the room list until it realizes that BOLD comes after GREY and we're no longer
    // in the RED section. Because there's no rooms there, we simply insert there which is
    // also a "category boundary". If we change the example to wanting to insert a BOLD room
    // which can't be ordered by timestamp with the existing couple rooms, we would still make
    // use of the boundary flag to insert at B before changing the boundary indicator to C.


    let desiredCategoryBoundaryIndex = 0;
    let foundBoundary = false;
    let pushedEntry = false;

    for (const entry of existingEntries) {
      // We insert our own record as needed, so don't let the old one through.
      if (entry.room.roomId === room.roomId) {
        continue;
      } // if the list is a recent list, and the room appears in this list, and we're
      // not looking at a sticky room (sticky rooms have unreliable categories), try
      // to slot the new room in


      if (entry.room.roomId !== this._state.stickyRoomId && !pushedEntry) {
        const entryCategoryIndex = CATEGORY_ORDER.indexOf(entry.category); // As per above, check if we're meeting that boundary we wanted to locate.

        if (entryCategoryIndex >= targetCategoryIndex && !foundBoundary) {
          desiredCategoryBoundaryIndex = newList.length - 1;
          foundBoundary = true;
        } // If we've hit the top of a boundary beyond our target category, insert at the top of
        // the grouping to ensure the room isn't slotted incorrectly. Otherwise, try to insert
        // based on most recent timestamp.


        const changedBoundary = entryCategoryIndex > targetCategoryIndex;
        const currentCategory = entryCategoryIndex === targetCategoryIndex;

        if (changedBoundary || currentCategory && categoryComparator({
          room
        }, entry) <= 0) {
          if (changedBoundary) {
            // If we changed a boundary, then we've gone too far - go to the top of the last
            // section instead.
            newList.splice(desiredCategoryBoundaryIndex, 0, {
              room,
              category
            });
          } else {
            // If we're ordering by timestamp, just insert normally
            newList.push({
              room,
              category
            });
          }

          pushedEntry = true;
        }
      } // Fall through and clone the list.


      newList.push(entry);
    }

    if (!pushedEntry && desiredCategoryBoundaryIndex >= 0) {
      console.warn("!! Room ".concat(room.roomId, " nearly lost: Ran off the end of ").concat(tag));
      console.warn("!! Inserting at position ".concat(desiredCategoryBoundaryIndex, " with category ").concat(category));
      newList.splice(desiredCategoryBoundaryIndex, 0, {
        room,
        category
      });
      pushedEntry = true;
    }

    return pushedEntry;
  }

  _setRoomCategory(room, category) {
    if (!room) return; // This should only happen in tests

    const listsClone = {}; // Micro optimization: Support lazily loading the last timestamp in a room

    const timestampCache = {}; // {roomId => ts}

    const lastTimestamp = room => {
      if (!timestampCache[room.roomId]) {
        timestampCache[room.roomId] = this._tsOfNewestEvent(room);
      }

      return timestampCache[room.roomId];
    };

    const targetTags = this._getRecommendedTagsForRoom(room);

    const insertedIntoTags = []; // We need to make sure all the tags (lists) are updated with the room's new position. We
    // generally only get called here when there's a new room to insert or a room has potentially
    // changed positions within the list.
    //
    // We do all our checks by iterating over the rooms in the existing lists, trying to insert
    // our room where we can. As a guiding principle, we should be removing the room from all
    // tags, and insert the room into targetTags. We should perform the deletion before the addition
    // where possible to keep a consistent state. By the end of this, targetTags should be the
    // same as insertedIntoTags.

    for (const key of Object.keys(this._state.lists)) {
      const shouldHaveRoom = targetTags.includes(key); // Speed optimization: Don't do complicated math if we don't have to.

      if (!shouldHaveRoom) {
        listsClone[key] = this._state.lists[key].filter(e => e.room.roomId !== room.roomId);
      } else if (getListAlgorithm(key, this._state.algorithm) === ALGO_MANUAL) {
        // Manually ordered tags are sorted later, so for now we'll just clone the tag
        // and add our room if needed
        listsClone[key] = this._state.lists[key].filter(e => e.room.roomId !== room.roomId);
        listsClone[key].push({
          room,
          category
        });
        insertedIntoTags.push(key);
      } else {
        listsClone[key] = [];

        const pushedEntry = this._slotRoomIntoList(room, category, key, this._state.lists[key], listsClone[key], lastTimestamp);

        if (!pushedEntry) {
          // This should rarely happen: _slotRoomIntoList has several checks which attempt
          // to make sure that a room is not lost in the list. If we do lose the room though,
          // we shouldn't throw it on the floor and forget about it. Instead, we should insert
          // it somewhere. We'll insert it at the top for a couple reasons: 1) it is probably
          // an important room for the user and 2) if this does happen, we'd want a bug report.
          console.warn("!! Room ".concat(room.roomId, " nearly lost: Failed to find a position"));
          console.warn("!! Inserting at position 0 in the list and flagging as inserted");
          console.warn("!! Additional info: ", {
            category,
            key,
            upToIndex: listsClone[key].length,
            expectedCount: this._state.lists[key].length
          });
          listsClone[key].splice(0, 0, {
            room,
            category
          });
        }

        insertedIntoTags.push(key);
      }
    } // Double check that we inserted the room in the right places.
    // There should never be a discrepancy.


    for (const targetTag of targetTags) {
      let count = 0;

      for (const insertedTag of insertedIntoTags) {
        if (insertedTag === targetTag) count++;
      }

      if (count !== 1) {
        console.warn("!! Room ".concat(room.roomId, " inserted ").concat(count, " times to ").concat(targetTag));
      } // This is a workaround for https://github.com/vector-im/riot-web/issues/11303
      // The logging is to try and identify what happened exactly.


      if (count === 0) {
        // Something went very badly wrong - try to recover the room.
        // We don't bother checking how the target list is ordered - we're expecting
        // to just insert it.
        console.warn("!! Recovering ".concat(room.roomId, " for tag ").concat(targetTag, " at position 0"));

        if (!listsClone[targetTag]) {
          console.warn("!! List for tag ".concat(targetTag, " does not exist - creating"));
          listsClone[targetTag] = [];
        }

        listsClone[targetTag].splice(0, 0, {
          room,
          category
        });
      }
    } // Sort the favourites before we set the clone


    for (const tag of Object.keys(listsClone)) {
      if (getListAlgorithm(tag, this._state.algorithm) !== ALGO_MANUAL) continue; // skip recents (pre-sorted)

      listsClone[tag].sort(this._getManualComparator(tag));
    }

    this._setState({
      lists: listsClone
    });
  }

  _generateInitialRoomLists() {
    // Log something to show that we're throwing away the old results. This is for the inevitable
    // question of "why is 100% of my CPU going towards Riot?" - a quick look at the logs would reveal
    // that something is wrong with the RoomListStore.
    console.log("Generating initial room lists");
    const lists = {
      "m.server_notice": [],
      "im.vector.fake.invite": [],
      "m.favourite": [],
      "im.vector.fake.recent": [],
      [TAG_DM]: [],
      "m.lowpriority": [],
      "im.vector.fake.archived": []
    };

    const dmRoomMap = _DMRoomMap.default.shared();

    this._matrixClient.getRooms().forEach(room => {
      const myUserId = this._matrixClient.getUserId();

      const membership = room.getMyMembership();
      const me = room.getMember(myUserId);

      if (membership === "invite") {
        lists["im.vector.fake.invite"].push({
          room,
          category: CATEGORY_RED
        });
      } else if (membership === "join" || membership === "ban" || me && me.isKicked()) {
        // Used to split rooms via tags
        let tagNames = Object.keys(room.tags); // ignore any m. tag names we don't know about

        tagNames = tagNames.filter(t => {
          // Speed optimization: Avoid hitting the SettingsStore at all costs by making it the
          // last condition possible.
          return lists[t] !== undefined || !t.startsWith('m.') && this._state.tagsEnabled;
        });

        if (tagNames.length) {
          for (let i = 0; i < tagNames.length; i++) {
            const tagName = tagNames[i];
            lists[tagName] = lists[tagName] || []; // Default to an arbitrary category for tags which aren't ordered by recents

            let category = CATEGORY_IDLE;

            if (getListAlgorithm(tagName, this._state.algorithm) !== ALGO_MANUAL) {
              category = this._calculateCategory(room);
            }

            lists[tagName].push({
              room,
              category
            });
          }
        } else if (dmRoomMap.getUserIdForRoomId(room.roomId)) {
          // "Direct Message" rooms (that we're still in and that aren't otherwise tagged)
          lists[TAG_DM].push({
            room,
            category: this._calculateCategory(room)
          });
        } else {
          lists["im.vector.fake.recent"].push({
            room,
            category: this._calculateCategory(room)
          });
        }
      } else if (membership === "leave") {
        // The category of these rooms is not super important, so deprioritize it to the lowest
        // possible value.
        lists["im.vector.fake.archived"].push({
          room,
          category: CATEGORY_IDLE
        });
      }
    }); // We use this cache in the recents comparator because _tsOfNewestEvent can take a while. This
    // cache only needs to survive the sort operation below and should not be implemented outside
    // of this function, otherwise the room lists will almost certainly be out of date and wrong.


    const latestEventTsCache = {}; // roomId => timestamp

    const tsOfNewestEventFn = room => {
      if (!room) return Number.MAX_SAFE_INTEGER; // Should only happen in tests

      if (latestEventTsCache[room.roomId]) {
        return latestEventTsCache[room.roomId];
      }

      const ts = this._tsOfNewestEvent(room);

      latestEventTsCache[room.roomId] = ts;
      return ts;
    };

    Object.keys(lists).forEach(listKey => {
      let comparator;

      switch (getListAlgorithm(listKey, this._state.algorithm)) {
        case ALGO_RECENT:
          comparator = (entryA, entryB) => this._recentsComparator(entryA, entryB, tsOfNewestEventFn);

          break;

        case ALGO_ALPHABETIC:
          comparator = this._lexicographicalComparator;
          break;

        case ALGO_MANUAL:
        default:
          comparator = this._getManualComparator(listKey);
          break;
      }

      if (this._state.orderImportantFirst) {
        lists[listKey].sort((entryA, entryB) => {
          if (entryA.category !== entryB.category) {
            const idxA = CATEGORY_ORDER.indexOf(entryA.category);
            const idxB = CATEGORY_ORDER.indexOf(entryB.category);
            if (idxA > idxB) return 1;
            if (idxA < idxB) return -1;
            return 0; // Technically not possible
          }

          return comparator(entryA, entryB);
        });
      } else {
        // skip the category comparison even though it should no-op when orderImportantFirst disabled
        lists[listKey].sort(comparator);
      }
    });

    this._setState({
      lists,
      ready: true // Ready to receive updates to ordering

    });
  }

  _eventTriggersRecentReorder(ev) {
    return ev.getTs() && (Unread.eventTriggersUnreadCount(ev) || ev.getSender() === this._matrixClient.credentials.userId);
  }

  _tsOfNewestEvent(room) {
    // Apparently we can have rooms without timelines, at least under testing
    // environments. Just return MAX_INT when this happens.
    if (!room || !room.timeline) return Number.MAX_SAFE_INTEGER;

    for (let i = room.timeline.length - 1; i >= 0; --i) {
      const ev = room.timeline[i];

      if (this._eventTriggersRecentReorder(ev)) {
        return ev.getTs();
      }
    } // we might only have events that don't trigger the unread indicator,
    // in which case use the oldest event even if normally it wouldn't count.
    // This is better than just assuming the last event was forever ago.


    if (room.timeline.length && room.timeline[0].getTs()) {
      return room.timeline[0].getTs();
    } else {
      return Number.MAX_SAFE_INTEGER;
    }
  }

  _calculateCategory(room) {
    if (!this._state.orderImportantFirst) {
      // Effectively disable the categorization of rooms if we're supposed to
      // be sorting by more recent messages first. This triggers the timestamp
      // comparison bit of _setRoomCategory and _recentsComparator instead of
      // the category ordering.
      return CATEGORY_IDLE;
    }

    const mentions = room.getUnreadNotificationCount("highlight") > 0;
    if (mentions) return CATEGORY_RED;
    let unread = room.getUnreadNotificationCount() > 0;
    if (unread) return CATEGORY_GREY;
    unread = Unread.doesRoomHaveUnreadMessages(room);
    if (unread) return CATEGORY_BOLD;
    return CATEGORY_IDLE;
  }

  _recentsComparator(entryA, entryB, tsOfNewestEventFn) {
    const timestampA = tsOfNewestEventFn(entryA.room);
    const timestampB = tsOfNewestEventFn(entryB.room);
    return timestampB - timestampA;
  }

  _lexicographicalComparator(entryA, entryB) {
    return entryA.room.name.localeCompare(entryB.room.name);
  }

  _getManualComparator(tagName, optimisticRequest) {
    return (entryA, entryB) => {
      const roomA = entryA.room;
      const roomB = entryB.room;
      let metaA = roomA.tags[tagName];
      let metaB = roomB.tags[tagName];
      if (optimisticRequest && roomA === optimisticRequest.room) metaA = optimisticRequest.metaData;
      if (optimisticRequest && roomB === optimisticRequest.room) metaB = optimisticRequest.metaData; // Make sure the room tag has an order element, if not set it to be the bottom

      const a = metaA ? Number(metaA.order) : undefined;
      const b = metaB ? Number(metaB.order) : undefined; // Order undefined room tag orders to the bottom

      if (a === undefined && b !== undefined) {
        return 1;
      } else if (a !== undefined && b === undefined) {
        return -1;
      }

      return a === b ? this._lexicographicalComparator(entryA, entryB) : a > b ? 1 : -1;
    };
  }

  getRoomLists() {
    return this._state.presentationLists;
  }

}

if (global.singletonRoomListStore === undefined) {
  global.singletonRoomListStore = new RoomListStore();
}

var _default = global.singletonRoomListStore;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zdG9yZXMvUm9vbUxpc3RTdG9yZS5qcyJdLCJuYW1lcyI6WyJDQVRFR09SWV9SRUQiLCJDQVRFR09SWV9HUkVZIiwiQ0FURUdPUllfQk9MRCIsIkNBVEVHT1JZX0lETEUiLCJUQUdfRE0iLCJBTEdPX01BTlVBTCIsIkFMR09fQUxQSEFCRVRJQyIsIkFMR09fUkVDRU5UIiwiQ0FURUdPUllfT1JERVIiLCJnZXRMaXN0QWxnb3JpdGhtIiwibGlzdEtleSIsInNldHRpbmdBbGdvcml0aG0iLCJrbm93bkxpc3RzIiwiU2V0IiwiUm9vbUxpc3RTdG9yZSIsIlN0b3JlIiwiY29uc3RydWN0b3IiLCJkaXMiLCJfaW5pdCIsIl9nZXRNYW51YWxDb21wYXJhdG9yIiwiYmluZCIsIl9yZWNlbnRzQ29tcGFyYXRvciIsInVwZGF0ZVNvcnRpbmdBbGdvcml0aG0iLCJhbGdvcml0aG0iLCJvcmRlckltcG9ydGFudEZpcnN0IiwiY29uc29sZSIsImxvZyIsIl9zZXRTdGF0ZSIsIl9nZW5lcmF0ZUluaXRpYWxSb29tTGlzdHMiLCJkZWZhdWx0TGlzdHMiLCJfc3RhdGUiLCJsaXN0cyIsInByZXNlbnRhdGlvbkxpc3RzIiwicmVhZHkiLCJzdGlja3lSb29tSWQiLCJTZXR0aW5nc1N0b3JlIiwibW9uaXRvclNldHRpbmciLCJuZXdTdGF0ZSIsImtleSIsIk9iamVjdCIsImtleXMiLCJtYXAiLCJlIiwicm9vbSIsImFzc2lnbiIsIl9fZW1pdENoYW5nZSIsIl9fb25EaXNwYXRjaCIsInBheWxvYWQiLCJsb2dpY2FsbHlSZWFkeSIsIl9tYXRyaXhDbGllbnQiLCJhY3Rpb24iLCJzZXR0aW5nTmFtZSIsIm5ld1ZhbHVlIiwidGFnc0VuYWJsZWQiLCJwcmV2U3RhdGUiLCJzdGF0ZSIsImlzRmVhdHVyZUVuYWJsZWQiLCJtYXRyaXhDbGllbnQiLCJvcmRlckJ5SW1wb3J0YW5jZSIsImdldFZhbHVlIiwib3JkZXJBbHBoYWJldGljYWxseSIsIm15VXNlcklkIiwiZ2V0VXNlcklkIiwiZXZlbnRJZCIsImV2ZW50IiwiZ2V0Q29udGVudCIsInJlY2VpcHRVc2VycyIsImluY2x1ZGVzIiwiX3Jvb21VcGRhdGVUcmlnZ2VyZWQiLCJyb29tSWQiLCJpc0xpdmVFdmVudCIsImlzTGl2ZVVuZmlsdGVyZWRSb29tVGltZWxpbmVFdmVudCIsIl9ldmVudFRyaWdnZXJzUmVjZW50UmVvcmRlciIsImdldFJvb21JZCIsImdldFJvb20iLCJsaXZlVGltZWxpbmUiLCJnZXRMaXZlVGltZWxpbmUiLCJldmVudFRpbWVsaW5lIiwiZ2V0VGltZWxpbmVGb3JFdmVudCIsImdldElkIiwiZXZlbnRfdHlwZSIsImN1cnJlbnRTdGlja3lJZCIsInJvb21faWQiLCJfc2V0Um9vbUNhdGVnb3J5IiwiaWdub3JlU3RpY2t5IiwiY2F0ZWdvcnkiLCJfY2FsY3VsYXRlQ2F0ZWdvcnkiLCJfZmlsdGVyVGFncyIsInRhZ3MiLCJmaWx0ZXIiLCJ0IiwiaGFzIiwiX2dldFJlY29tbWVuZGVkVGFnc0ZvclJvb20iLCJteU1lbWJlcnNoaXAiLCJnZXRNeU1lbWJlcnNoaXAiLCJwdXNoIiwiZG1Sb29tTWFwIiwiRE1Sb29tTWFwIiwic2hhcmVkIiwiZ2V0VXNlcklkRm9yUm9vbUlkIiwibGVuZ3RoIiwiX3Nsb3RSb29tSW50b0xpc3QiLCJ0YWciLCJleGlzdGluZ0VudHJpZXMiLCJuZXdMaXN0IiwibGFzdFRpbWVzdGFtcEZuIiwidGFyZ2V0Q2F0ZWdvcnlJbmRleCIsImluZGV4T2YiLCJjYXRlZ29yeUNvbXBhcmF0b3IiLCJhIiwiYiIsInNvcnRBbGdvcml0aG0iLCJfbGV4aWNvZ3JhcGhpY2FsQ29tcGFyYXRvciIsImRlc2lyZWRDYXRlZ29yeUJvdW5kYXJ5SW5kZXgiLCJmb3VuZEJvdW5kYXJ5IiwicHVzaGVkRW50cnkiLCJlbnRyeSIsImVudHJ5Q2F0ZWdvcnlJbmRleCIsImNoYW5nZWRCb3VuZGFyeSIsImN1cnJlbnRDYXRlZ29yeSIsInNwbGljZSIsIndhcm4iLCJsaXN0c0Nsb25lIiwidGltZXN0YW1wQ2FjaGUiLCJsYXN0VGltZXN0YW1wIiwiX3RzT2ZOZXdlc3RFdmVudCIsInRhcmdldFRhZ3MiLCJpbnNlcnRlZEludG9UYWdzIiwic2hvdWxkSGF2ZVJvb20iLCJ1cFRvSW5kZXgiLCJleHBlY3RlZENvdW50IiwidGFyZ2V0VGFnIiwiY291bnQiLCJpbnNlcnRlZFRhZyIsInNvcnQiLCJnZXRSb29tcyIsImZvckVhY2giLCJtZW1iZXJzaGlwIiwibWUiLCJnZXRNZW1iZXIiLCJpc0tpY2tlZCIsInRhZ05hbWVzIiwidW5kZWZpbmVkIiwic3RhcnRzV2l0aCIsImkiLCJ0YWdOYW1lIiwibGF0ZXN0RXZlbnRUc0NhY2hlIiwidHNPZk5ld2VzdEV2ZW50Rm4iLCJOdW1iZXIiLCJNQVhfU0FGRV9JTlRFR0VSIiwidHMiLCJjb21wYXJhdG9yIiwiZW50cnlBIiwiZW50cnlCIiwiaWR4QSIsImlkeEIiLCJldiIsImdldFRzIiwiVW5yZWFkIiwiZXZlbnRUcmlnZ2Vyc1VucmVhZENvdW50IiwiZ2V0U2VuZGVyIiwiY3JlZGVudGlhbHMiLCJ1c2VySWQiLCJ0aW1lbGluZSIsIm1lbnRpb25zIiwiZ2V0VW5yZWFkTm90aWZpY2F0aW9uQ291bnQiLCJ1bnJlYWQiLCJkb2VzUm9vbUhhdmVVbnJlYWRNZXNzYWdlcyIsInRpbWVzdGFtcEEiLCJ0aW1lc3RhbXBCIiwibmFtZSIsImxvY2FsZUNvbXBhcmUiLCJvcHRpbWlzdGljUmVxdWVzdCIsInJvb21BIiwicm9vbUIiLCJtZXRhQSIsIm1ldGFCIiwibWV0YURhdGEiLCJvcmRlciIsImdldFJvb21MaXN0cyIsImdsb2JhbCIsInNpbmdsZXRvblJvb21MaXN0U3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBZUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBbkJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBOzs7Ozs7Ozs7OztBQVlBLE1BQU1BLFlBQVksR0FBRyxLQUFyQixDLENBQWdDOztBQUNoQyxNQUFNQyxhQUFhLEdBQUcsTUFBdEIsQyxDQUFnQzs7QUFDaEMsTUFBTUMsYUFBYSxHQUFHLE1BQXRCLEMsQ0FBZ0M7O0FBQ2hDLE1BQU1DLGFBQWEsR0FBRyxNQUF0QixDLENBQWdDOztBQUV6QixNQUFNQyxNQUFNLEdBQUcsdUJBQWY7QUFFUDs7Ozs7O0FBSU8sTUFBTUMsV0FBVyxHQUFHLFFBQXBCO0FBRVA7Ozs7OztBQUlPLE1BQU1DLGVBQWUsR0FBRyxZQUF4QjtBQUVQOzs7Ozs7QUFJTyxNQUFNQyxXQUFXLEdBQUcsUUFBcEI7O0FBRVAsTUFBTUMsY0FBYyxHQUFHLENBQUNSLFlBQUQsRUFBZUMsYUFBZixFQUE4QkMsYUFBOUIsRUFBNkNDLGFBQTdDLENBQXZCOztBQUVBLE1BQU1NLGdCQUFnQixHQUFHLENBQUNDLE9BQUQsRUFBVUMsZ0JBQVYsS0FBK0I7QUFDcEQ7QUFDQTtBQUNBLFVBQVFELE9BQVI7QUFDSSxTQUFLLHVCQUFMO0FBQ0EsU0FBSyx1QkFBTDtBQUNBLFNBQUsseUJBQUw7QUFDQSxTQUFLLGVBQUw7QUFDQSxTQUFLTixNQUFMO0FBQ0ksYUFBT08sZ0JBQVA7O0FBRUosU0FBSyxhQUFMO0FBQ0E7QUFBUztBQUNMLGFBQU9OLFdBQVA7QUFWUjtBQVlILENBZkQ7O0FBaUJBLE1BQU1PLFVBQVUsR0FBRyxJQUFJQyxHQUFKLENBQVEsQ0FDdkIsYUFEdUIsRUFFdkIsdUJBRnVCLEVBR3ZCLHVCQUh1QixFQUl2Qix5QkFKdUIsRUFLdkIsZUFMdUIsRUFNdkJULE1BTnVCLENBQVIsQ0FBbkI7QUFTQTs7Ozs7QUFJQSxNQUFNVSxhQUFOLFNBQTRCQyxZQUE1QixDQUFrQztBQUM5QkMsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsVUFBTUMsbUJBQU47O0FBRUEsU0FBS0MsS0FBTDs7QUFDQSxTQUFLQyxvQkFBTCxHQUE0QixLQUFLQSxvQkFBTCxDQUEwQkMsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBNUI7QUFDQSxTQUFLQyxrQkFBTCxHQUEwQixLQUFLQSxrQkFBTCxDQUF3QkQsSUFBeEIsQ0FBNkIsSUFBN0IsQ0FBMUI7QUFDSDtBQUVEOzs7Ozs7O0FBS0FFLEVBQUFBLHNCQUFzQixDQUFDQyxTQUFELEVBQVlDLG1CQUFaLEVBQWlDO0FBQ25EO0FBQ0E7QUFDQUMsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksbUNBQVosRUFBaUQ7QUFBQ0gsTUFBQUEsU0FBRDtBQUFZQyxNQUFBQTtBQUFaLEtBQWpEOztBQUNBLFNBQUtHLFNBQUwsQ0FBZTtBQUFDSixNQUFBQSxTQUFEO0FBQVlDLE1BQUFBO0FBQVosS0FBZixFQUptRCxDQU1uRDs7O0FBQ0EsU0FBS0kseUJBQUw7QUFDSDs7QUFFRFYsRUFBQUEsS0FBSyxHQUFHO0FBQ0o7QUFDQSxVQUFNVyxZQUFZLEdBQUc7QUFDakIseUJBQW1CO0FBQUM7QUFBRCxPQURGO0FBRWpCLCtCQUF5QixFQUZSO0FBR2pCLHFCQUFlLEVBSEU7QUFJakIsK0JBQXlCLEVBSlI7QUFLakIsT0FBQ3pCLE1BQUQsR0FBVSxFQUxPO0FBTWpCLHVCQUFpQixFQU5BO0FBT2pCLGlDQUEyQjtBQVBWLEtBQXJCO0FBU0EsU0FBSzBCLE1BQUwsR0FBYztBQUNWO0FBQ0E7QUFDQUMsTUFBQUEsS0FBSyxFQUFFRixZQUhHO0FBSVZHLE1BQUFBLGlCQUFpQixFQUFFSCxZQUpUO0FBSXVCO0FBQ2pDSSxNQUFBQSxLQUFLLEVBQUUsS0FMRztBQU1WQyxNQUFBQSxZQUFZLEVBQUUsSUFOSjtBQU9WWCxNQUFBQSxTQUFTLEVBQUVoQixXQVBEO0FBUVZpQixNQUFBQSxtQkFBbUIsRUFBRTtBQVJYLEtBQWQ7O0FBV0FXLDJCQUFjQyxjQUFkLENBQTZCLDhCQUE3QixFQUE2RCxJQUE3RDs7QUFDQUQsMkJBQWNDLGNBQWQsQ0FBNkIsNEJBQTdCLEVBQTJELElBQTNEOztBQUNBRCwyQkFBY0MsY0FBZCxDQUE2QixxQkFBN0IsRUFBb0QsSUFBcEQ7QUFDSDs7QUFFRFQsRUFBQUEsU0FBUyxDQUFDVSxRQUFELEVBQVc7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJQSxRQUFRLENBQUMsT0FBRCxDQUFaLEVBQXVCO0FBQ25CLFlBQU1MLGlCQUFpQixHQUFHLEVBQTFCOztBQUNBLFdBQUssTUFBTU0sR0FBWCxJQUFrQkMsTUFBTSxDQUFDQyxJQUFQLENBQVlILFFBQVEsQ0FBQyxPQUFELENBQXBCLENBQWxCLEVBQWtEO0FBQzlDTCxRQUFBQSxpQkFBaUIsQ0FBQ00sR0FBRCxDQUFqQixHQUF5QkQsUUFBUSxDQUFDLE9BQUQsQ0FBUixDQUFrQkMsR0FBbEIsRUFBdUJHLEdBQXZCLENBQTRCQyxDQUFELElBQU9BLENBQUMsQ0FBQ0MsSUFBcEMsQ0FBekI7QUFDSDs7QUFDRE4sTUFBQUEsUUFBUSxDQUFDLG1CQUFELENBQVIsR0FBZ0NMLGlCQUFoQztBQUNIOztBQUNELFNBQUtGLE1BQUwsR0FBY1MsTUFBTSxDQUFDSyxNQUFQLENBQWMsS0FBS2QsTUFBbkIsRUFBMkJPLFFBQTNCLENBQWQ7O0FBQ0EsU0FBS1EsWUFBTDtBQUNIOztBQUVEQyxFQUFBQSxZQUFZLENBQUNDLE9BQUQsRUFBVTtBQUNsQixVQUFNQyxjQUFjLEdBQUcsS0FBS0MsYUFBTCxJQUFzQixLQUFLbkIsTUFBTCxDQUFZRyxLQUF6RDs7QUFDQSxZQUFRYyxPQUFPLENBQUNHLE1BQWhCO0FBQ0ksV0FBSyxpQkFBTDtBQUF3QjtBQUNwQixjQUFJLENBQUNGLGNBQUwsRUFBcUI7O0FBRXJCLGtCQUFRRCxPQUFPLENBQUNJLFdBQWhCO0FBQ0ksaUJBQUssOEJBQUw7QUFDSSxtQkFBSzdCLHNCQUFMLENBQTRCeUIsT0FBTyxDQUFDSyxRQUFSLEdBQW1COUMsZUFBbkIsR0FBcUNDLFdBQWpFLEVBQ0ksS0FBS3VCLE1BQUwsQ0FBWU4sbUJBRGhCO0FBRUE7O0FBQ0osaUJBQUssNEJBQUw7QUFDSSxtQkFBS0Ysc0JBQUwsQ0FBNEIsS0FBS1EsTUFBTCxDQUFZUCxTQUF4QyxFQUFtRHdCLE9BQU8sQ0FBQ0ssUUFBM0Q7QUFDQTs7QUFDSixpQkFBSyxxQkFBTDtBQUNJLG1CQUFLekIsU0FBTCxDQUFlO0FBQUMwQixnQkFBQUEsV0FBVyxFQUFFTixPQUFPLENBQUNLO0FBQXRCLGVBQWY7O0FBQ0EsbUJBQUt4Qix5QkFBTCxHQUZKLENBRXNDOzs7QUFDbEM7QUFYUjtBQWFIO0FBQ0Q7QUFDQTs7QUFDQSxXQUFLLG9CQUFMO0FBQTJCO0FBQ3ZCLGNBQUksRUFBRW1CLE9BQU8sQ0FBQ08sU0FBUixLQUFzQixVQUF0QixJQUFvQ1AsT0FBTyxDQUFDUSxLQUFSLEtBQWtCLFVBQXhELENBQUosRUFBeUU7QUFDckU7QUFDSCxXQUhzQixDQUt2QjtBQUNBO0FBQ0E7OztBQUVBLGVBQUs1QixTQUFMLENBQWU7QUFBQzBCLFlBQUFBLFdBQVcsRUFBRWxCLHVCQUFjcUIsZ0JBQWQsQ0FBK0IscUJBQS9CO0FBQWQsV0FBZjs7QUFFQSxlQUFLUCxhQUFMLEdBQXFCRixPQUFPLENBQUNVLFlBQTdCOztBQUVBLGdCQUFNQyxpQkFBaUIsR0FBR3ZCLHVCQUFjd0IsUUFBZCxDQUF1Qiw0QkFBdkIsQ0FBMUI7O0FBQ0EsZ0JBQU1DLG1CQUFtQixHQUFHekIsdUJBQWN3QixRQUFkLENBQXVCLDhCQUF2QixDQUE1Qjs7QUFDQSxlQUFLckMsc0JBQUwsQ0FBNEJzQyxtQkFBbUIsR0FBR3RELGVBQUgsR0FBcUJDLFdBQXBFLEVBQWlGbUQsaUJBQWpGO0FBQ0g7QUFDRDs7QUFDQSxXQUFLLDRCQUFMO0FBQW1DO0FBQy9CLGNBQUksQ0FBQ1YsY0FBTCxFQUFxQixNQURVLENBRy9CO0FBQ0E7O0FBQ0EsZ0JBQU1hLFFBQVEsR0FBRyxLQUFLWixhQUFMLENBQW1CYSxTQUFuQixFQUFqQjs7QUFDQSxlQUFLLE1BQU1DLE9BQVgsSUFBc0J4QixNQUFNLENBQUNDLElBQVAsQ0FBWU8sT0FBTyxDQUFDaUIsS0FBUixDQUFjQyxVQUFkLEVBQVosQ0FBdEIsRUFBK0Q7QUFDM0Qsa0JBQU1DLFlBQVksR0FBRzNCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZTyxPQUFPLENBQUNpQixLQUFSLENBQWNDLFVBQWQsR0FBMkJGLE9BQTNCLEVBQW9DLFFBQXBDLEtBQWlELEVBQTdELENBQXJCOztBQUNBLGdCQUFJRyxZQUFZLENBQUNDLFFBQWIsQ0FBc0JOLFFBQXRCLENBQUosRUFBcUM7QUFDakMsbUJBQUtPLG9CQUFMLENBQTBCckIsT0FBTyxDQUFDSixJQUFSLENBQWEwQixNQUF2Qzs7QUFDQTtBQUNIO0FBQ0o7QUFDSjtBQUNEOztBQUNBLFdBQUsseUJBQUw7QUFBZ0M7QUFDNUIsY0FBSSxDQUFDckIsY0FBTCxFQUFxQixNQURPLENBRTVCO0FBQ0E7O0FBQ0EsZUFBS3BCLHlCQUFMO0FBQ0g7QUFDRDs7QUFDQSxXQUFLLDZCQUFMO0FBQW9DO0FBQ2hDLGNBQUksQ0FBQ29CLGNBQUQsSUFDQSxDQUFDRCxPQUFPLENBQUN1QixXQURULElBRUEsQ0FBQ3ZCLE9BQU8sQ0FBQ3dCLGlDQUZULElBR0EsQ0FBQyxLQUFLQywyQkFBTCxDQUFpQ3pCLE9BQU8sQ0FBQ2lCLEtBQXpDLENBSEQsSUFJQSxLQUFLbEMsTUFBTCxDQUFZUCxTQUFaLEtBQTBCaEIsV0FKOUIsRUFLRTtBQUNFO0FBQ0g7O0FBRUQsZUFBSzZELG9CQUFMLENBQTBCckIsT0FBTyxDQUFDaUIsS0FBUixDQUFjUyxTQUFkLEVBQTFCO0FBQ0g7QUFDRDtBQUNBO0FBQ0E7O0FBQ0EsV0FBSywrQkFBTDtBQUFzQztBQUNsQyxjQUFJLENBQUN6QixjQUFMLEVBQXFCO0FBRXJCLGdCQUFNcUIsTUFBTSxHQUFHdEIsT0FBTyxDQUFDaUIsS0FBUixDQUFjUyxTQUFkLEVBQWYsQ0FIa0MsQ0FLbEM7O0FBQ0EsY0FBSSxDQUFDSixNQUFMLEVBQWE7O0FBRWIsZ0JBQU0xQixJQUFJLEdBQUcsS0FBS00sYUFBTCxDQUFtQnlCLE9BQW5CLENBQTJCTCxNQUEzQixDQUFiLENBUmtDLENBVWxDOzs7QUFDQSxjQUFJLENBQUMxQixJQUFMLEVBQVc7QUFFWCxnQkFBTWdDLFlBQVksR0FBR2hDLElBQUksQ0FBQ2lDLGVBQUwsRUFBckI7QUFDQSxnQkFBTUMsYUFBYSxHQUFHbEMsSUFBSSxDQUFDbUMsbUJBQUwsQ0FBeUIvQixPQUFPLENBQUNpQixLQUFSLENBQWNlLEtBQWQsRUFBekIsQ0FBdEIsQ0Fka0MsQ0FnQmxDO0FBQ0E7O0FBQ0EsY0FBSUosWUFBWSxLQUFLRSxhQUFqQixJQUFrQyxDQUFDLEtBQUtMLDJCQUFMLENBQWlDekIsT0FBTyxDQUFDaUIsS0FBekMsQ0FBdkMsRUFBd0Y7QUFDcEY7QUFDSDs7QUFFRCxlQUFLSSxvQkFBTCxDQUEwQkMsTUFBMUI7QUFDSDtBQUNEOztBQUNBLFdBQUssMkJBQUw7QUFBa0M7QUFDOUIsY0FBSSxDQUFDckIsY0FBTCxFQUFxQjtBQUNyQixjQUFJRCxPQUFPLENBQUNpQyxVQUFSLEtBQXVCLFVBQTNCLEVBQXVDLE1BRlQsQ0FHOUI7QUFDQTs7QUFDQSxlQUFLcEQseUJBQUw7QUFDSDtBQUNEOztBQUNBLFdBQUssaUNBQUw7QUFBd0M7QUFDcEMsY0FBSSxDQUFDb0IsY0FBTCxFQUFxQjs7QUFDckIsZUFBS29CLG9CQUFMLENBQTBCckIsT0FBTyxDQUFDSixJQUFSLENBQWEwQixNQUF2QyxFQUErQyxJQUEvQztBQUNIO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsV0FBSyxvQkFBTDtBQUEyQjtBQUN2QixjQUFJLENBQUNyQixjQUFMLEVBQXFCOztBQUNyQixlQUFLb0Isb0JBQUwsQ0FBMEJyQixPQUFPLENBQUNKLElBQVIsQ0FBYTBCLE1BQXZDLEVBQStDLElBQS9DO0FBQ0g7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsV0FBSyxzQkFBTDtBQUNBLFdBQUssZUFBTDtBQUFzQjtBQUNsQjtBQUNBO0FBQ0EsZUFBS25ELEtBQUw7O0FBQ0EsZUFBSytCLGFBQUwsR0FBcUIsSUFBckI7QUFDSDtBQUNEOztBQUNBLFdBQUssV0FBTDtBQUFrQjtBQUNkLGNBQUksQ0FBQ0QsY0FBTCxFQUFxQixNQURQLENBR2Q7QUFDQTs7QUFDQSxnQkFBTWlDLGVBQWUsR0FBRyxLQUFLbkQsTUFBTCxDQUFZSSxZQUFwQzs7QUFDQSxlQUFLUCxTQUFMLENBQWU7QUFBQ08sWUFBQUEsWUFBWSxFQUFFYSxPQUFPLENBQUNtQztBQUF2QixXQUFmOztBQUNBLGNBQUlELGVBQUosRUFBcUI7QUFDakIsaUJBQUtFLGdCQUFMLENBQXNCLEtBQUtsQyxhQUFMLENBQW1CeUIsT0FBbkIsQ0FBMkJPLGVBQTNCLENBQXRCLEVBQW1FOUUsYUFBbkU7QUFDSDtBQUNKO0FBQ0Q7QUE3Sko7QUErSkg7O0FBRURpRSxFQUFBQSxvQkFBb0IsQ0FBQ0MsTUFBRCxFQUFTZSxZQUFULEVBQXVCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSSxLQUFLdEQsTUFBTCxDQUFZSSxZQUFaLEtBQTZCbUMsTUFBN0IsSUFBdUNlLFlBQTNDLEVBQXlEO0FBQ3JEO0FBQ0EsWUFBTXpDLElBQUksR0FBRyxLQUFLTSxhQUFMLENBQW1CeUIsT0FBbkIsQ0FBMkJMLE1BQTNCLENBQWI7O0FBQ0EsVUFBSSxDQUFDMUIsSUFBTCxFQUFXOztBQUVYLFlBQU0wQyxRQUFRLEdBQUcsS0FBS0Msa0JBQUwsQ0FBd0IzQyxJQUF4QixDQUFqQjs7QUFDQSxXQUFLd0MsZ0JBQUwsQ0FBc0J4QyxJQUF0QixFQUE0QjBDLFFBQTVCO0FBQ0g7QUFDSjs7QUFFREUsRUFBQUEsV0FBVyxDQUFDQyxJQUFELEVBQU87QUFDZEEsSUFBQUEsSUFBSSxHQUFHQSxJQUFJLEdBQUdqRCxNQUFNLENBQUNDLElBQVAsQ0FBWWdELElBQVosQ0FBSCxHQUF1QixFQUFsQztBQUNBLFFBQUksS0FBSzFELE1BQUwsQ0FBWXVCLFdBQWhCLEVBQTZCLE9BQU9tQyxJQUFQO0FBQzdCLFdBQU9BLElBQUksQ0FBQ0MsTUFBTCxDQUFhQyxDQUFELElBQU85RSxVQUFVLENBQUMrRSxHQUFYLENBQWVELENBQWYsQ0FBbkIsQ0FBUDtBQUNIOztBQUVERSxFQUFBQSwwQkFBMEIsQ0FBQ2pELElBQUQsRUFBTztBQUM3QixVQUFNNkMsSUFBSSxHQUFHLEVBQWI7QUFFQSxVQUFNSyxZQUFZLEdBQUdsRCxJQUFJLENBQUNtRCxlQUFMLEVBQXJCOztBQUNBLFFBQUlELFlBQVksS0FBSyxNQUFqQixJQUEyQkEsWUFBWSxLQUFLLFFBQWhELEVBQTBEO0FBQ3REO0FBQ0FMLE1BQUFBLElBQUksQ0FBQ08sSUFBTCxDQUFVLEdBQUcsS0FBS1IsV0FBTCxDQUFpQjVDLElBQUksQ0FBQzZDLElBQXRCLENBQWIsRUFGc0QsQ0FJdEQ7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsWUFBTVEsU0FBUyxHQUFHQyxtQkFBVUMsTUFBVixFQUFsQjs7QUFDQSxVQUFJTCxZQUFZLEtBQUssUUFBckIsRUFBK0I7QUFDM0JMLFFBQUFBLElBQUksQ0FBQ08sSUFBTCxDQUFVLHVCQUFWO0FBQ0gsT0FGRCxNQUVPLElBQUlDLFNBQVMsQ0FBQ0csa0JBQVYsQ0FBNkJ4RCxJQUFJLENBQUMwQixNQUFsQyxLQUE2Q21CLElBQUksQ0FBQ1ksTUFBTCxLQUFnQixDQUFqRSxFQUFvRTtBQUN2RTtBQUNBO0FBQ0FaLFFBQUFBLElBQUksQ0FBQ08sSUFBTCxDQUFVM0YsTUFBVjtBQUNILE9BSk0sTUFJQSxJQUFJb0YsSUFBSSxDQUFDWSxNQUFMLEtBQWdCLENBQXBCLEVBQXVCO0FBQzFCWixRQUFBQSxJQUFJLENBQUNPLElBQUwsQ0FBVSx1QkFBVjtBQUNIO0FBQ0osS0FsQkQsTUFrQk8sSUFBSUYsWUFBSixFQUFrQjtBQUFFO0FBQ3ZCTCxNQUFBQSxJQUFJLENBQUNPLElBQUwsQ0FBVSx5QkFBVjtBQUNIOztBQUdELFdBQU9QLElBQVA7QUFDSDs7QUFFRGEsRUFBQUEsaUJBQWlCLENBQUMxRCxJQUFELEVBQU8wQyxRQUFQLEVBQWlCaUIsR0FBakIsRUFBc0JDLGVBQXRCLEVBQXVDQyxPQUF2QyxFQUFnREMsZUFBaEQsRUFBaUU7QUFDOUUsVUFBTUMsbUJBQW1CLEdBQUdsRyxjQUFjLENBQUNtRyxPQUFmLENBQXVCdEIsUUFBdkIsQ0FBNUI7O0FBRUEsUUFBSXVCLGtCQUFrQixHQUFHLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVTCxlQUFlLENBQUNJLENBQUMsQ0FBQ2xFLElBQUgsQ0FBZixJQUEyQjhELGVBQWUsQ0FBQ0ssQ0FBQyxDQUFDbkUsSUFBSCxDQUE3RTs7QUFDQSxVQUFNb0UsYUFBYSxHQUFHdEcsZ0JBQWdCLENBQUM2RixHQUFELEVBQU0sS0FBS3hFLE1BQUwsQ0FBWVAsU0FBbEIsQ0FBdEM7O0FBQ0EsUUFBSXdGLGFBQWEsS0FBS3hHLFdBQXRCLEVBQW1DO0FBQy9CcUcsTUFBQUEsa0JBQWtCLEdBQUcsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVUsS0FBS3pGLGtCQUFMLENBQXdCd0YsQ0FBeEIsRUFBMkJDLENBQTNCLEVBQThCTCxlQUE5QixDQUEvQjtBQUNILEtBRkQsTUFFTyxJQUFJTSxhQUFhLEtBQUt6RyxlQUF0QixFQUF1QztBQUMxQ3NHLE1BQUFBLGtCQUFrQixHQUFHLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVLEtBQUtFLDBCQUFMLENBQWdDSCxDQUFoQyxFQUFtQ0MsQ0FBbkMsQ0FBL0I7QUFDSCxLQVQ2RSxDQVc5RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsUUFBSUcsNEJBQTRCLEdBQUcsQ0FBbkM7QUFDQSxRQUFJQyxhQUFhLEdBQUcsS0FBcEI7QUFDQSxRQUFJQyxXQUFXLEdBQUcsS0FBbEI7O0FBRUEsU0FBSyxNQUFNQyxLQUFYLElBQW9CYixlQUFwQixFQUFxQztBQUNqQztBQUNBLFVBQUlhLEtBQUssQ0FBQ3pFLElBQU4sQ0FBVzBCLE1BQVgsS0FBc0IxQixJQUFJLENBQUMwQixNQUEvQixFQUF1QztBQUNuQztBQUNILE9BSmdDLENBTWpDO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBSStDLEtBQUssQ0FBQ3pFLElBQU4sQ0FBVzBCLE1BQVgsS0FBc0IsS0FBS3ZDLE1BQUwsQ0FBWUksWUFBbEMsSUFBa0QsQ0FBQ2lGLFdBQXZELEVBQW9FO0FBQ2hFLGNBQU1FLGtCQUFrQixHQUFHN0csY0FBYyxDQUFDbUcsT0FBZixDQUF1QlMsS0FBSyxDQUFDL0IsUUFBN0IsQ0FBM0IsQ0FEZ0UsQ0FHaEU7O0FBQ0EsWUFBSWdDLGtCQUFrQixJQUFJWCxtQkFBdEIsSUFBNkMsQ0FBQ1EsYUFBbEQsRUFBaUU7QUFDN0RELFVBQUFBLDRCQUE0QixHQUFHVCxPQUFPLENBQUNKLE1BQVIsR0FBaUIsQ0FBaEQ7QUFDQWMsVUFBQUEsYUFBYSxHQUFHLElBQWhCO0FBQ0gsU0FQK0QsQ0FTaEU7QUFDQTtBQUNBOzs7QUFDQSxjQUFNSSxlQUFlLEdBQUdELGtCQUFrQixHQUFHWCxtQkFBN0M7QUFDQSxjQUFNYSxlQUFlLEdBQUdGLGtCQUFrQixLQUFLWCxtQkFBL0M7O0FBQ0EsWUFBSVksZUFBZSxJQUFLQyxlQUFlLElBQUlYLGtCQUFrQixDQUFDO0FBQUNqRSxVQUFBQTtBQUFELFNBQUQsRUFBU3lFLEtBQVQsQ0FBbEIsSUFBcUMsQ0FBaEYsRUFBb0Y7QUFDaEYsY0FBSUUsZUFBSixFQUFxQjtBQUNqQjtBQUNBO0FBQ0FkLFlBQUFBLE9BQU8sQ0FBQ2dCLE1BQVIsQ0FBZVAsNEJBQWYsRUFBNkMsQ0FBN0MsRUFBZ0Q7QUFBQ3RFLGNBQUFBLElBQUQ7QUFBTzBDLGNBQUFBO0FBQVAsYUFBaEQ7QUFDSCxXQUpELE1BSU87QUFDSDtBQUNBbUIsWUFBQUEsT0FBTyxDQUFDVCxJQUFSLENBQWE7QUFBQ3BELGNBQUFBLElBQUQ7QUFBTzBDLGNBQUFBO0FBQVAsYUFBYjtBQUNIOztBQUNEOEIsVUFBQUEsV0FBVyxHQUFHLElBQWQ7QUFDSDtBQUNKLE9BbENnQyxDQW9DakM7OztBQUNBWCxNQUFBQSxPQUFPLENBQUNULElBQVIsQ0FBYXFCLEtBQWI7QUFDSDs7QUFFRCxRQUFJLENBQUNELFdBQUQsSUFBZ0JGLDRCQUE0QixJQUFJLENBQXBELEVBQXVEO0FBQ25EeEYsTUFBQUEsT0FBTyxDQUFDZ0csSUFBUixtQkFBd0I5RSxJQUFJLENBQUMwQixNQUE3Qiw4Q0FBdUVpQyxHQUF2RTtBQUNBN0UsTUFBQUEsT0FBTyxDQUFDZ0csSUFBUixvQ0FBeUNSLDRCQUF6Qyw0QkFBdUY1QixRQUF2RjtBQUNBbUIsTUFBQUEsT0FBTyxDQUFDZ0IsTUFBUixDQUFlUCw0QkFBZixFQUE2QyxDQUE3QyxFQUFnRDtBQUFDdEUsUUFBQUEsSUFBRDtBQUFPMEMsUUFBQUE7QUFBUCxPQUFoRDtBQUNBOEIsTUFBQUEsV0FBVyxHQUFHLElBQWQ7QUFDSDs7QUFFRCxXQUFPQSxXQUFQO0FBQ0g7O0FBRURoQyxFQUFBQSxnQkFBZ0IsQ0FBQ3hDLElBQUQsRUFBTzBDLFFBQVAsRUFBaUI7QUFDN0IsUUFBSSxDQUFDMUMsSUFBTCxFQUFXLE9BRGtCLENBQ1Y7O0FBRW5CLFVBQU0rRSxVQUFVLEdBQUcsRUFBbkIsQ0FINkIsQ0FLN0I7O0FBQ0EsVUFBTUMsY0FBYyxHQUFHLEVBQXZCLENBTjZCLENBTUY7O0FBQzNCLFVBQU1DLGFBQWEsR0FBSWpGLElBQUQsSUFBVTtBQUM1QixVQUFJLENBQUNnRixjQUFjLENBQUNoRixJQUFJLENBQUMwQixNQUFOLENBQW5CLEVBQWtDO0FBQzlCc0QsUUFBQUEsY0FBYyxDQUFDaEYsSUFBSSxDQUFDMEIsTUFBTixDQUFkLEdBQThCLEtBQUt3RCxnQkFBTCxDQUFzQmxGLElBQXRCLENBQTlCO0FBQ0g7O0FBQ0QsYUFBT2dGLGNBQWMsQ0FBQ2hGLElBQUksQ0FBQzBCLE1BQU4sQ0FBckI7QUFDSCxLQUxEOztBQU1BLFVBQU15RCxVQUFVLEdBQUcsS0FBS2xDLDBCQUFMLENBQWdDakQsSUFBaEMsQ0FBbkI7O0FBQ0EsVUFBTW9GLGdCQUFnQixHQUFHLEVBQXpCLENBZDZCLENBZ0I3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBSyxNQUFNekYsR0FBWCxJQUFrQkMsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBS1YsTUFBTCxDQUFZQyxLQUF4QixDQUFsQixFQUFrRDtBQUM5QyxZQUFNaUcsY0FBYyxHQUFHRixVQUFVLENBQUMzRCxRQUFYLENBQW9CN0IsR0FBcEIsQ0FBdkIsQ0FEOEMsQ0FHOUM7O0FBQ0EsVUFBSSxDQUFDMEYsY0FBTCxFQUFxQjtBQUNqQk4sUUFBQUEsVUFBVSxDQUFDcEYsR0FBRCxDQUFWLEdBQWtCLEtBQUtSLE1BQUwsQ0FBWUMsS0FBWixDQUFrQk8sR0FBbEIsRUFBdUJtRCxNQUF2QixDQUErQi9DLENBQUQsSUFBT0EsQ0FBQyxDQUFDQyxJQUFGLENBQU8wQixNQUFQLEtBQWtCMUIsSUFBSSxDQUFDMEIsTUFBNUQsQ0FBbEI7QUFDSCxPQUZELE1BRU8sSUFBSTVELGdCQUFnQixDQUFDNkIsR0FBRCxFQUFNLEtBQUtSLE1BQUwsQ0FBWVAsU0FBbEIsQ0FBaEIsS0FBaURsQixXQUFyRCxFQUFrRTtBQUNyRTtBQUNBO0FBQ0FxSCxRQUFBQSxVQUFVLENBQUNwRixHQUFELENBQVYsR0FBa0IsS0FBS1IsTUFBTCxDQUFZQyxLQUFaLENBQWtCTyxHQUFsQixFQUF1Qm1ELE1BQXZCLENBQStCL0MsQ0FBRCxJQUFPQSxDQUFDLENBQUNDLElBQUYsQ0FBTzBCLE1BQVAsS0FBa0IxQixJQUFJLENBQUMwQixNQUE1RCxDQUFsQjtBQUNBcUQsUUFBQUEsVUFBVSxDQUFDcEYsR0FBRCxDQUFWLENBQWdCeUQsSUFBaEIsQ0FBcUI7QUFBQ3BELFVBQUFBLElBQUQ7QUFBTzBDLFVBQUFBO0FBQVAsU0FBckI7QUFDQTBDLFFBQUFBLGdCQUFnQixDQUFDaEMsSUFBakIsQ0FBc0J6RCxHQUF0QjtBQUNILE9BTk0sTUFNQTtBQUNIb0YsUUFBQUEsVUFBVSxDQUFDcEYsR0FBRCxDQUFWLEdBQWtCLEVBQWxCOztBQUVBLGNBQU02RSxXQUFXLEdBQUcsS0FBS2QsaUJBQUwsQ0FDaEIxRCxJQURnQixFQUNWMEMsUUFEVSxFQUNBL0MsR0FEQSxFQUNLLEtBQUtSLE1BQUwsQ0FBWUMsS0FBWixDQUFrQk8sR0FBbEIsQ0FETCxFQUM2Qm9GLFVBQVUsQ0FBQ3BGLEdBQUQsQ0FEdkMsRUFDOENzRixhQUQ5QyxDQUFwQjs7QUFHQSxZQUFJLENBQUNULFdBQUwsRUFBa0I7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ExRixVQUFBQSxPQUFPLENBQUNnRyxJQUFSLG1CQUF3QjlFLElBQUksQ0FBQzBCLE1BQTdCO0FBQ0E1QyxVQUFBQSxPQUFPLENBQUNnRyxJQUFSO0FBQ0FoRyxVQUFBQSxPQUFPLENBQUNnRyxJQUFSLENBQWEsc0JBQWIsRUFBcUM7QUFDbENwQyxZQUFBQSxRQURrQztBQUVsQy9DLFlBQUFBLEdBRmtDO0FBR2xDMkYsWUFBQUEsU0FBUyxFQUFFUCxVQUFVLENBQUNwRixHQUFELENBQVYsQ0FBZ0I4RCxNQUhPO0FBSWxDOEIsWUFBQUEsYUFBYSxFQUFFLEtBQUtwRyxNQUFMLENBQVlDLEtBQVosQ0FBa0JPLEdBQWxCLEVBQXVCOEQ7QUFKSixXQUFyQztBQU1Bc0IsVUFBQUEsVUFBVSxDQUFDcEYsR0FBRCxDQUFWLENBQWdCa0YsTUFBaEIsQ0FBdUIsQ0FBdkIsRUFBMEIsQ0FBMUIsRUFBNkI7QUFBQzdFLFlBQUFBLElBQUQ7QUFBTzBDLFlBQUFBO0FBQVAsV0FBN0I7QUFDSDs7QUFDRDBDLFFBQUFBLGdCQUFnQixDQUFDaEMsSUFBakIsQ0FBc0J6RCxHQUF0QjtBQUNIO0FBQ0osS0E5RDRCLENBZ0U3QjtBQUNBOzs7QUFDQSxTQUFLLE1BQU02RixTQUFYLElBQXdCTCxVQUF4QixFQUFvQztBQUNoQyxVQUFJTSxLQUFLLEdBQUcsQ0FBWjs7QUFDQSxXQUFLLE1BQU1DLFdBQVgsSUFBMEJOLGdCQUExQixFQUE0QztBQUN4QyxZQUFJTSxXQUFXLEtBQUtGLFNBQXBCLEVBQStCQyxLQUFLO0FBQ3ZDOztBQUVELFVBQUlBLEtBQUssS0FBSyxDQUFkLEVBQWlCO0FBQ2IzRyxRQUFBQSxPQUFPLENBQUNnRyxJQUFSLG1CQUF3QjlFLElBQUksQ0FBQzBCLE1BQTdCLHVCQUFnRCtELEtBQWhELHVCQUFrRUQsU0FBbEU7QUFDSCxPQVIrQixDQVVoQztBQUNBOzs7QUFDQSxVQUFJQyxLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUNiO0FBQ0E7QUFDQTtBQUNBM0csUUFBQUEsT0FBTyxDQUFDZ0csSUFBUix5QkFBOEI5RSxJQUFJLENBQUMwQixNQUFuQyxzQkFBcUQ4RCxTQUFyRDs7QUFDQSxZQUFJLENBQUNULFVBQVUsQ0FBQ1MsU0FBRCxDQUFmLEVBQTRCO0FBQ3hCMUcsVUFBQUEsT0FBTyxDQUFDZ0csSUFBUiwyQkFBZ0NVLFNBQWhDO0FBQ0FULFVBQUFBLFVBQVUsQ0FBQ1MsU0FBRCxDQUFWLEdBQXdCLEVBQXhCO0FBQ0g7O0FBQ0RULFFBQUFBLFVBQVUsQ0FBQ1MsU0FBRCxDQUFWLENBQXNCWCxNQUF0QixDQUE2QixDQUE3QixFQUFnQyxDQUFoQyxFQUFtQztBQUFDN0UsVUFBQUEsSUFBRDtBQUFPMEMsVUFBQUE7QUFBUCxTQUFuQztBQUNIO0FBQ0osS0F6RjRCLENBMkY3Qjs7O0FBQ0EsU0FBSyxNQUFNaUIsR0FBWCxJQUFrQi9ELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZa0YsVUFBWixDQUFsQixFQUEyQztBQUN2QyxVQUFJakgsZ0JBQWdCLENBQUM2RixHQUFELEVBQU0sS0FBS3hFLE1BQUwsQ0FBWVAsU0FBbEIsQ0FBaEIsS0FBaURsQixXQUFyRCxFQUFrRSxTQUQzQixDQUNxQzs7QUFDNUVxSCxNQUFBQSxVQUFVLENBQUNwQixHQUFELENBQVYsQ0FBZ0JnQyxJQUFoQixDQUFxQixLQUFLbkgsb0JBQUwsQ0FBMEJtRixHQUExQixDQUFyQjtBQUNIOztBQUVELFNBQUszRSxTQUFMLENBQWU7QUFBQ0ksTUFBQUEsS0FBSyxFQUFFMkY7QUFBUixLQUFmO0FBQ0g7O0FBRUQ5RixFQUFBQSx5QkFBeUIsR0FBRztBQUN4QjtBQUNBO0FBQ0E7QUFDQUgsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksK0JBQVo7QUFFQSxVQUFNSyxLQUFLLEdBQUc7QUFDVix5QkFBbUIsRUFEVDtBQUVWLCtCQUF5QixFQUZmO0FBR1YscUJBQWUsRUFITDtBQUlWLCtCQUF5QixFQUpmO0FBS1YsT0FBQzNCLE1BQUQsR0FBVSxFQUxBO0FBTVYsdUJBQWlCLEVBTlA7QUFPVixpQ0FBMkI7QUFQakIsS0FBZDs7QUFVQSxVQUFNNEYsU0FBUyxHQUFHQyxtQkFBVUMsTUFBVixFQUFsQjs7QUFFQSxTQUFLakQsYUFBTCxDQUFtQnNGLFFBQW5CLEdBQThCQyxPQUE5QixDQUF1QzdGLElBQUQsSUFBVTtBQUM1QyxZQUFNa0IsUUFBUSxHQUFHLEtBQUtaLGFBQUwsQ0FBbUJhLFNBQW5CLEVBQWpCOztBQUNBLFlBQU0yRSxVQUFVLEdBQUc5RixJQUFJLENBQUNtRCxlQUFMLEVBQW5CO0FBQ0EsWUFBTTRDLEVBQUUsR0FBRy9GLElBQUksQ0FBQ2dHLFNBQUwsQ0FBZTlFLFFBQWYsQ0FBWDs7QUFFQSxVQUFJNEUsVUFBVSxLQUFLLFFBQW5CLEVBQTZCO0FBQ3pCMUcsUUFBQUEsS0FBSyxDQUFDLHVCQUFELENBQUwsQ0FBK0JnRSxJQUEvQixDQUFvQztBQUFDcEQsVUFBQUEsSUFBRDtBQUFPMEMsVUFBQUEsUUFBUSxFQUFFckY7QUFBakIsU0FBcEM7QUFDSCxPQUZELE1BRU8sSUFBSXlJLFVBQVUsS0FBSyxNQUFmLElBQXlCQSxVQUFVLEtBQUssS0FBeEMsSUFBa0RDLEVBQUUsSUFBSUEsRUFBRSxDQUFDRSxRQUFILEVBQTVELEVBQTRFO0FBQy9FO0FBQ0EsWUFBSUMsUUFBUSxHQUFHdEcsTUFBTSxDQUFDQyxJQUFQLENBQVlHLElBQUksQ0FBQzZDLElBQWpCLENBQWYsQ0FGK0UsQ0FJL0U7O0FBQ0FxRCxRQUFBQSxRQUFRLEdBQUdBLFFBQVEsQ0FBQ3BELE1BQVQsQ0FBaUJDLENBQUQsSUFBTztBQUM5QjtBQUNBO0FBQ0EsaUJBQU8zRCxLQUFLLENBQUMyRCxDQUFELENBQUwsS0FBYW9ELFNBQWIsSUFBMkIsQ0FBQ3BELENBQUMsQ0FBQ3FELFVBQUYsQ0FBYSxJQUFiLENBQUQsSUFBdUIsS0FBS2pILE1BQUwsQ0FBWXVCLFdBQXJFO0FBQ0gsU0FKVSxDQUFYOztBQU1BLFlBQUl3RixRQUFRLENBQUN6QyxNQUFiLEVBQXFCO0FBQ2pCLGVBQUssSUFBSTRDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdILFFBQVEsQ0FBQ3pDLE1BQTdCLEVBQXFDNEMsQ0FBQyxFQUF0QyxFQUEwQztBQUN0QyxrQkFBTUMsT0FBTyxHQUFHSixRQUFRLENBQUNHLENBQUQsQ0FBeEI7QUFDQWpILFlBQUFBLEtBQUssQ0FBQ2tILE9BQUQsQ0FBTCxHQUFpQmxILEtBQUssQ0FBQ2tILE9BQUQsQ0FBTCxJQUFrQixFQUFuQyxDQUZzQyxDQUl0Qzs7QUFDQSxnQkFBSTVELFFBQVEsR0FBR2xGLGFBQWY7O0FBQ0EsZ0JBQUlNLGdCQUFnQixDQUFDd0ksT0FBRCxFQUFVLEtBQUtuSCxNQUFMLENBQVlQLFNBQXRCLENBQWhCLEtBQXFEbEIsV0FBekQsRUFBc0U7QUFDbEVnRixjQUFBQSxRQUFRLEdBQUcsS0FBS0Msa0JBQUwsQ0FBd0IzQyxJQUF4QixDQUFYO0FBQ0g7O0FBQ0RaLFlBQUFBLEtBQUssQ0FBQ2tILE9BQUQsQ0FBTCxDQUFlbEQsSUFBZixDQUFvQjtBQUFDcEQsY0FBQUEsSUFBRDtBQUFPMEMsY0FBQUE7QUFBUCxhQUFwQjtBQUNIO0FBQ0osU0FaRCxNQVlPLElBQUlXLFNBQVMsQ0FBQ0csa0JBQVYsQ0FBNkJ4RCxJQUFJLENBQUMwQixNQUFsQyxDQUFKLEVBQStDO0FBQ2xEO0FBQ0F0QyxVQUFBQSxLQUFLLENBQUMzQixNQUFELENBQUwsQ0FBYzJGLElBQWQsQ0FBbUI7QUFBQ3BELFlBQUFBLElBQUQ7QUFBTzBDLFlBQUFBLFFBQVEsRUFBRSxLQUFLQyxrQkFBTCxDQUF3QjNDLElBQXhCO0FBQWpCLFdBQW5CO0FBQ0gsU0FITSxNQUdBO0FBQ0haLFVBQUFBLEtBQUssQ0FBQyx1QkFBRCxDQUFMLENBQStCZ0UsSUFBL0IsQ0FBb0M7QUFBQ3BELFlBQUFBLElBQUQ7QUFBTzBDLFlBQUFBLFFBQVEsRUFBRSxLQUFLQyxrQkFBTCxDQUF3QjNDLElBQXhCO0FBQWpCLFdBQXBDO0FBQ0g7QUFDSixPQTdCTSxNQTZCQSxJQUFJOEYsVUFBVSxLQUFLLE9BQW5CLEVBQTRCO0FBQy9CO0FBQ0E7QUFDQTFHLFFBQUFBLEtBQUssQ0FBQyx5QkFBRCxDQUFMLENBQWlDZ0UsSUFBakMsQ0FBc0M7QUFBQ3BELFVBQUFBLElBQUQ7QUFBTzBDLFVBQUFBLFFBQVEsRUFBRWxGO0FBQWpCLFNBQXRDO0FBQ0g7QUFDSixLQXpDRCxFQWxCd0IsQ0E2RHhCO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBTStJLGtCQUFrQixHQUFHLEVBQTNCLENBaEV3QixDQWdFTzs7QUFDL0IsVUFBTUMsaUJBQWlCLEdBQUl4RyxJQUFELElBQVU7QUFDaEMsVUFBSSxDQUFDQSxJQUFMLEVBQVcsT0FBT3lHLE1BQU0sQ0FBQ0MsZ0JBQWQsQ0FEcUIsQ0FDVzs7QUFFM0MsVUFBSUgsa0JBQWtCLENBQUN2RyxJQUFJLENBQUMwQixNQUFOLENBQXRCLEVBQXFDO0FBQ2pDLGVBQU82RSxrQkFBa0IsQ0FBQ3ZHLElBQUksQ0FBQzBCLE1BQU4sQ0FBekI7QUFDSDs7QUFFRCxZQUFNaUYsRUFBRSxHQUFHLEtBQUt6QixnQkFBTCxDQUFzQmxGLElBQXRCLENBQVg7O0FBQ0F1RyxNQUFBQSxrQkFBa0IsQ0FBQ3ZHLElBQUksQ0FBQzBCLE1BQU4sQ0FBbEIsR0FBa0NpRixFQUFsQztBQUNBLGFBQU9BLEVBQVA7QUFDSCxLQVZEOztBQVlBL0csSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlULEtBQVosRUFBbUJ5RyxPQUFuQixDQUE0QjlILE9BQUQsSUFBYTtBQUNwQyxVQUFJNkksVUFBSjs7QUFDQSxjQUFROUksZ0JBQWdCLENBQUNDLE9BQUQsRUFBVSxLQUFLb0IsTUFBTCxDQUFZUCxTQUF0QixDQUF4QjtBQUNJLGFBQUtoQixXQUFMO0FBQ0lnSixVQUFBQSxVQUFVLEdBQUcsQ0FBQ0MsTUFBRCxFQUFTQyxNQUFULEtBQW9CLEtBQUtwSSxrQkFBTCxDQUF3Qm1JLE1BQXhCLEVBQWdDQyxNQUFoQyxFQUF3Q04saUJBQXhDLENBQWpDOztBQUNBOztBQUNKLGFBQUs3SSxlQUFMO0FBQ0lpSixVQUFBQSxVQUFVLEdBQUcsS0FBS3ZDLDBCQUFsQjtBQUNBOztBQUNKLGFBQUszRyxXQUFMO0FBQ0E7QUFDSWtKLFVBQUFBLFVBQVUsR0FBRyxLQUFLcEksb0JBQUwsQ0FBMEJULE9BQTFCLENBQWI7QUFDQTtBQVZSOztBQWFBLFVBQUksS0FBS29CLE1BQUwsQ0FBWU4sbUJBQWhCLEVBQXFDO0FBQ2pDTyxRQUFBQSxLQUFLLENBQUNyQixPQUFELENBQUwsQ0FBZTRILElBQWYsQ0FBb0IsQ0FBQ2tCLE1BQUQsRUFBU0MsTUFBVCxLQUFvQjtBQUNwQyxjQUFJRCxNQUFNLENBQUNuRSxRQUFQLEtBQW9Cb0UsTUFBTSxDQUFDcEUsUUFBL0IsRUFBeUM7QUFDckMsa0JBQU1xRSxJQUFJLEdBQUdsSixjQUFjLENBQUNtRyxPQUFmLENBQXVCNkMsTUFBTSxDQUFDbkUsUUFBOUIsQ0FBYjtBQUNBLGtCQUFNc0UsSUFBSSxHQUFHbkosY0FBYyxDQUFDbUcsT0FBZixDQUF1QjhDLE1BQU0sQ0FBQ3BFLFFBQTlCLENBQWI7QUFDQSxnQkFBSXFFLElBQUksR0FBR0MsSUFBWCxFQUFpQixPQUFPLENBQVA7QUFDakIsZ0JBQUlELElBQUksR0FBR0MsSUFBWCxFQUFpQixPQUFPLENBQUMsQ0FBUjtBQUNqQixtQkFBTyxDQUFQLENBTHFDLENBSzNCO0FBQ2I7O0FBQ0QsaUJBQU9KLFVBQVUsQ0FBQ0MsTUFBRCxFQUFTQyxNQUFULENBQWpCO0FBQ0gsU0FURDtBQVVILE9BWEQsTUFXTztBQUNIO0FBQ0ExSCxRQUFBQSxLQUFLLENBQUNyQixPQUFELENBQUwsQ0FBZTRILElBQWYsQ0FBb0JpQixVQUFwQjtBQUNIO0FBQ0osS0E5QkQ7O0FBZ0NBLFNBQUs1SCxTQUFMLENBQWU7QUFDWEksTUFBQUEsS0FEVztBQUVYRSxNQUFBQSxLQUFLLEVBQUUsSUFGSSxDQUVFOztBQUZGLEtBQWY7QUFJSDs7QUFFRHVDLEVBQUFBLDJCQUEyQixDQUFDb0YsRUFBRCxFQUFLO0FBQzVCLFdBQU9BLEVBQUUsQ0FBQ0MsS0FBSCxPQUNIQyxNQUFNLENBQUNDLHdCQUFQLENBQWdDSCxFQUFoQyxLQUNBQSxFQUFFLENBQUNJLFNBQUgsT0FBbUIsS0FBSy9HLGFBQUwsQ0FBbUJnSCxXQUFuQixDQUErQkMsTUFGL0MsQ0FBUDtBQUlIOztBQUVEckMsRUFBQUEsZ0JBQWdCLENBQUNsRixJQUFELEVBQU87QUFDbkI7QUFDQTtBQUNBLFFBQUksQ0FBQ0EsSUFBRCxJQUFTLENBQUNBLElBQUksQ0FBQ3dILFFBQW5CLEVBQTZCLE9BQU9mLE1BQU0sQ0FBQ0MsZ0JBQWQ7O0FBRTdCLFNBQUssSUFBSUwsQ0FBQyxHQUFHckcsSUFBSSxDQUFDd0gsUUFBTCxDQUFjL0QsTUFBZCxHQUF1QixDQUFwQyxFQUF1QzRDLENBQUMsSUFBSSxDQUE1QyxFQUErQyxFQUFFQSxDQUFqRCxFQUFvRDtBQUNoRCxZQUFNWSxFQUFFLEdBQUdqSCxJQUFJLENBQUN3SCxRQUFMLENBQWNuQixDQUFkLENBQVg7O0FBQ0EsVUFBSSxLQUFLeEUsMkJBQUwsQ0FBaUNvRixFQUFqQyxDQUFKLEVBQTBDO0FBQ3RDLGVBQU9BLEVBQUUsQ0FBQ0MsS0FBSCxFQUFQO0FBQ0g7QUFDSixLQVZrQixDQVluQjtBQUNBO0FBQ0E7OztBQUNBLFFBQUlsSCxJQUFJLENBQUN3SCxRQUFMLENBQWMvRCxNQUFkLElBQXdCekQsSUFBSSxDQUFDd0gsUUFBTCxDQUFjLENBQWQsRUFBaUJOLEtBQWpCLEVBQTVCLEVBQXNEO0FBQ2xELGFBQU9sSCxJQUFJLENBQUN3SCxRQUFMLENBQWMsQ0FBZCxFQUFpQk4sS0FBakIsRUFBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU9ULE1BQU0sQ0FBQ0MsZ0JBQWQ7QUFDSDtBQUNKOztBQUVEL0QsRUFBQUEsa0JBQWtCLENBQUMzQyxJQUFELEVBQU87QUFDckIsUUFBSSxDQUFDLEtBQUtiLE1BQUwsQ0FBWU4sbUJBQWpCLEVBQXNDO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBT3JCLGFBQVA7QUFDSDs7QUFFRCxVQUFNaUssUUFBUSxHQUFHekgsSUFBSSxDQUFDMEgsMEJBQUwsQ0FBZ0MsV0FBaEMsSUFBK0MsQ0FBaEU7QUFDQSxRQUFJRCxRQUFKLEVBQWMsT0FBT3BLLFlBQVA7QUFFZCxRQUFJc0ssTUFBTSxHQUFHM0gsSUFBSSxDQUFDMEgsMEJBQUwsS0FBb0MsQ0FBakQ7QUFDQSxRQUFJQyxNQUFKLEVBQVksT0FBT3JLLGFBQVA7QUFFWnFLLElBQUFBLE1BQU0sR0FBR1IsTUFBTSxDQUFDUywwQkFBUCxDQUFrQzVILElBQWxDLENBQVQ7QUFDQSxRQUFJMkgsTUFBSixFQUFZLE9BQU9wSyxhQUFQO0FBRVosV0FBT0MsYUFBUDtBQUNIOztBQUVEa0IsRUFBQUEsa0JBQWtCLENBQUNtSSxNQUFELEVBQVNDLE1BQVQsRUFBaUJOLGlCQUFqQixFQUFvQztBQUNsRCxVQUFNcUIsVUFBVSxHQUFHckIsaUJBQWlCLENBQUNLLE1BQU0sQ0FBQzdHLElBQVIsQ0FBcEM7QUFDQSxVQUFNOEgsVUFBVSxHQUFHdEIsaUJBQWlCLENBQUNNLE1BQU0sQ0FBQzlHLElBQVIsQ0FBcEM7QUFDQSxXQUFPOEgsVUFBVSxHQUFHRCxVQUFwQjtBQUNIOztBQUVEeEQsRUFBQUEsMEJBQTBCLENBQUN3QyxNQUFELEVBQVNDLE1BQVQsRUFBaUI7QUFDdkMsV0FBT0QsTUFBTSxDQUFDN0csSUFBUCxDQUFZK0gsSUFBWixDQUFpQkMsYUFBakIsQ0FBK0JsQixNQUFNLENBQUM5RyxJQUFQLENBQVkrSCxJQUEzQyxDQUFQO0FBQ0g7O0FBRUR2SixFQUFBQSxvQkFBb0IsQ0FBQzhILE9BQUQsRUFBVTJCLGlCQUFWLEVBQTZCO0FBQzdDLFdBQU8sQ0FBQ3BCLE1BQUQsRUFBU0MsTUFBVCxLQUFvQjtBQUN2QixZQUFNb0IsS0FBSyxHQUFHckIsTUFBTSxDQUFDN0csSUFBckI7QUFDQSxZQUFNbUksS0FBSyxHQUFHckIsTUFBTSxDQUFDOUcsSUFBckI7QUFFQSxVQUFJb0ksS0FBSyxHQUFHRixLQUFLLENBQUNyRixJQUFOLENBQVd5RCxPQUFYLENBQVo7QUFDQSxVQUFJK0IsS0FBSyxHQUFHRixLQUFLLENBQUN0RixJQUFOLENBQVd5RCxPQUFYLENBQVo7QUFFQSxVQUFJMkIsaUJBQWlCLElBQUlDLEtBQUssS0FBS0QsaUJBQWlCLENBQUNqSSxJQUFyRCxFQUEyRG9JLEtBQUssR0FBR0gsaUJBQWlCLENBQUNLLFFBQTFCO0FBQzNELFVBQUlMLGlCQUFpQixJQUFJRSxLQUFLLEtBQUtGLGlCQUFpQixDQUFDakksSUFBckQsRUFBMkRxSSxLQUFLLEdBQUdKLGlCQUFpQixDQUFDSyxRQUExQixDQVJwQyxDQVV2Qjs7QUFDQSxZQUFNcEUsQ0FBQyxHQUFHa0UsS0FBSyxHQUFHM0IsTUFBTSxDQUFDMkIsS0FBSyxDQUFDRyxLQUFQLENBQVQsR0FBeUJwQyxTQUF4QztBQUNBLFlBQU1oQyxDQUFDLEdBQUdrRSxLQUFLLEdBQUc1QixNQUFNLENBQUM0QixLQUFLLENBQUNFLEtBQVAsQ0FBVCxHQUF5QnBDLFNBQXhDLENBWnVCLENBY3ZCOztBQUNBLFVBQUlqQyxDQUFDLEtBQUtpQyxTQUFOLElBQW1CaEMsQ0FBQyxLQUFLZ0MsU0FBN0IsRUFBd0M7QUFDcEMsZUFBTyxDQUFQO0FBQ0gsT0FGRCxNQUVPLElBQUlqQyxDQUFDLEtBQUtpQyxTQUFOLElBQW1CaEMsQ0FBQyxLQUFLZ0MsU0FBN0IsRUFBd0M7QUFDM0MsZUFBTyxDQUFDLENBQVI7QUFDSDs7QUFFRCxhQUFPakMsQ0FBQyxLQUFLQyxDQUFOLEdBQVUsS0FBS0UsMEJBQUwsQ0FBZ0N3QyxNQUFoQyxFQUF3Q0MsTUFBeEMsQ0FBVixHQUE2RDVDLENBQUMsR0FBR0MsQ0FBSixHQUFRLENBQVIsR0FBWSxDQUFDLENBQWpGO0FBQ0gsS0F0QkQ7QUF1Qkg7O0FBRURxRSxFQUFBQSxZQUFZLEdBQUc7QUFDWCxXQUFPLEtBQUtySixNQUFMLENBQVlFLGlCQUFuQjtBQUNIOztBQTNyQjZCOztBQThyQmxDLElBQUlvSixNQUFNLENBQUNDLHNCQUFQLEtBQWtDdkMsU0FBdEMsRUFBaUQ7QUFDN0NzQyxFQUFBQSxNQUFNLENBQUNDLHNCQUFQLEdBQWdDLElBQUl2SyxhQUFKLEVBQWhDO0FBQ0g7O2VBQ2NzSyxNQUFNLENBQUNDLHNCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcbmltcG9ydCB7U3RvcmV9IGZyb20gJ2ZsdXgvdXRpbHMnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgRE1Sb29tTWFwIGZyb20gJy4uL3V0aWxzL0RNUm9vbU1hcCc7XHJcbmltcG9ydCAqIGFzIFVucmVhZCBmcm9tICcuLi9VbnJlYWQnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5cclxuLypcclxuUm9vbSBzb3J0aW5nIGFsZ29yaXRobTpcclxuKiBBbHdheXMgcHJlZmVyIHRvIGhhdmUgcmVkID4gZ3JleSA+IGJvbGQgPiBpZGxlXHJcbiogVGhlIHJvb20gYmVpbmcgdmlld2VkIHNob3VsZCBiZSBzdGlja3kgKG5vdCBqdW1wIGRvd24gdG8gdGhlIGlkbGUgbGlzdClcclxuKiBXaGVuIHN3aXRjaGluZyB0byBhIG5ldyByb29tLCBzb3J0IHRoZSBsYXN0IHN0aWNreSByb29tIHRvIHRoZSB0b3Agb2YgdGhlIGlkbGUgbGlzdC5cclxuXHJcblRoZSBhcHByb2FjaCB0YWtlbiBieSB0aGUgc3RvcmUgaXMgdG8gZ2VuZXJhdGUgYW4gaW5pdGlhbCByZXByZXNlbnRhdGlvbiBvZiBhbGwgdGhlXHJcbnRhZ2dlZCBsaXN0cyAoYWNjZXB0aW5nIHRoYXQgaXQnbGwgdGFrZSBhIGxpdHRsZSBiaXQgbG9uZ2VyIHRvIGNhbGN1bGF0ZSkgYW5kIG1ha2Vcclxuc21hbGwgY2hhbmdlcyB0byB0aGF0IG92ZXIgdGltZS4gVGhpcyByZXN1bHRzIGluIHF1aWNrIGNoYW5nZXMgdG8gdGhlIHJvb20gbGlzdCB3aGlsZVxyXG5hbHNvIGhhdmluZyB1cGRhdGUgb3BlcmF0aW9ucyBmZWVsIG1vcmUgbGlrZSBwb3BwaW5nL3B1c2hpbmcgdG8gYSBzdGFjay5cclxuICovXHJcblxyXG5jb25zdCBDQVRFR09SWV9SRUQgPSBcInJlZFwiOyAgICAgLy8gTWVudGlvbnMgaW4gdGhlIHJvb21cclxuY29uc3QgQ0FURUdPUllfR1JFWSA9IFwiZ3JleVwiOyAgIC8vIFVucmVhZCBub3RpZmllZCBtZXNzYWdlcyAobm90IG1lbnRpb25zKVxyXG5jb25zdCBDQVRFR09SWV9CT0xEID0gXCJib2xkXCI7ICAgLy8gVW5yZWFkIG1lc3NhZ2VzIChub3Qgbm90aWZpZWQsICdNZW50aW9ucyBPbmx5JyByb29tcylcclxuY29uc3QgQ0FURUdPUllfSURMRSA9IFwiaWRsZVwiOyAgIC8vIE5vdGhpbmcgb2YgaW50ZXJlc3RcclxuXHJcbmV4cG9ydCBjb25zdCBUQUdfRE0gPSBcImltLnZlY3Rvci5mYWtlLmRpcmVjdFwiO1xyXG5cclxuLyoqXHJcbiAqIElkZW50aWZpZXIgZm9yIG1hbnVhbCBzb3J0aW5nIGJlaGF2aW91cjogc29ydCBieSB0aGUgdXNlciBkZWZpbmVkIG9yZGVyLlxyXG4gKiBAdHlwZSB7c3RyaW5nfVxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IEFMR09fTUFOVUFMID0gXCJtYW51YWxcIjtcclxuXHJcbi8qKlxyXG4gKiBJZGVudGlmaWVyIGZvciBhbHBoYWJldGljIHNvcnRpbmcgYmVoYXZpb3VyOiBzb3J0IGJ5IHRoZSByb29tIG5hbWUgYWxwaGFiZXRpY2FsbHkgZmlyc3QuXHJcbiAqIEB0eXBlIHtzdHJpbmd9XHJcbiAqL1xyXG5leHBvcnQgY29uc3QgQUxHT19BTFBIQUJFVElDID0gXCJhbHBoYWJldGljXCI7XHJcblxyXG4vKipcclxuICogSWRlbnRpZmllciBmb3IgY2xhc3NpYyBzb3J0aW5nIGJlaGF2aW91cjogc29ydCBieSB0aGUgbW9zdCByZWNlbnQgbWVzc2FnZSBmaXJzdC5cclxuICogQHR5cGUge3N0cmluZ31cclxuICovXHJcbmV4cG9ydCBjb25zdCBBTEdPX1JFQ0VOVCA9IFwicmVjZW50XCI7XHJcblxyXG5jb25zdCBDQVRFR09SWV9PUkRFUiA9IFtDQVRFR09SWV9SRUQsIENBVEVHT1JZX0dSRVksIENBVEVHT1JZX0JPTEQsIENBVEVHT1JZX0lETEVdO1xyXG5cclxuY29uc3QgZ2V0TGlzdEFsZ29yaXRobSA9IChsaXN0S2V5LCBzZXR0aW5nQWxnb3JpdGhtKSA9PiB7XHJcbiAgICAvLyBhcHBseSBtYW51YWwgc29ydGluZyBvbmx5IHRvIG0uZmF2b3VyaXRlLCBvdGhlcndpc2UgcmVzcGVjdCB0aGUgZ2xvYmFsIHNldHRpbmdcclxuICAgIC8vIGFsbCB0aGUga25vd24gdGFncyBhcmUgbGlzdGVkIGV4cGxpY2l0bHkgaGVyZSB0byBzaW1wbGlmeSBmdXR1cmUgY2hhbmdlc1xyXG4gICAgc3dpdGNoIChsaXN0S2V5KSB7XHJcbiAgICAgICAgY2FzZSBcImltLnZlY3Rvci5mYWtlLmludml0ZVwiOlxyXG4gICAgICAgIGNhc2UgXCJpbS52ZWN0b3IuZmFrZS5yZWNlbnRcIjpcclxuICAgICAgICBjYXNlIFwiaW0udmVjdG9yLmZha2UuYXJjaGl2ZWRcIjpcclxuICAgICAgICBjYXNlIFwibS5sb3dwcmlvcml0eVwiOlxyXG4gICAgICAgIGNhc2UgVEFHX0RNOlxyXG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ0FsZ29yaXRobTtcclxuXHJcbiAgICAgICAgY2FzZSBcIm0uZmF2b3VyaXRlXCI6XHJcbiAgICAgICAgZGVmYXVsdDogLy8gY3VzdG9tLXRhZ3NcclxuICAgICAgICAgICAgcmV0dXJuIEFMR09fTUFOVUFMO1xyXG4gICAgfVxyXG59O1xyXG5cclxuY29uc3Qga25vd25MaXN0cyA9IG5ldyBTZXQoW1xyXG4gICAgXCJtLmZhdm91cml0ZVwiLFxyXG4gICAgXCJpbS52ZWN0b3IuZmFrZS5pbnZpdGVcIixcclxuICAgIFwiaW0udmVjdG9yLmZha2UucmVjZW50XCIsXHJcbiAgICBcImltLnZlY3Rvci5mYWtlLmFyY2hpdmVkXCIsXHJcbiAgICBcIm0ubG93cHJpb3JpdHlcIixcclxuICAgIFRBR19ETSxcclxuXSk7XHJcblxyXG4vKipcclxuICogQSBjbGFzcyBmb3Igc3RvcmluZyBhcHBsaWNhdGlvbiBzdGF0ZSBmb3IgY2F0ZWdvcmlzaW5nIHJvb21zIGluXHJcbiAqIHRoZSBSb29tTGlzdC5cclxuICovXHJcbmNsYXNzIFJvb21MaXN0U3RvcmUgZXh0ZW5kcyBTdG9yZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcihkaXMpO1xyXG5cclxuICAgICAgICB0aGlzLl9pbml0KCk7XHJcbiAgICAgICAgdGhpcy5fZ2V0TWFudWFsQ29tcGFyYXRvciA9IHRoaXMuX2dldE1hbnVhbENvbXBhcmF0b3IuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9yZWNlbnRzQ29tcGFyYXRvciA9IHRoaXMuX3JlY2VudHNDb21wYXJhdG9yLmJpbmQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGFuZ2VzIHRoZSBzb3J0aW5nIGFsZ29yaXRobSB1c2VkIGJ5IHRoZSBSb29tTGlzdFN0b3JlLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGFsZ29yaXRobSBUaGUgbmV3IGFsZ29yaXRobSB0byB1c2UuIFNob3VsZCBiZSBvbmUgb2YgdGhlIEFMR09fKiBjb25zdGFudHMuXHJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IG9yZGVySW1wb3J0YW50Rmlyc3QgV2hldGhlciB0byBzb3J0IGJ5IGNhdGVnb3JpZXMgb2YgaW1wb3J0YW5jZVxyXG4gICAgICovXHJcbiAgICB1cGRhdGVTb3J0aW5nQWxnb3JpdGhtKGFsZ29yaXRobSwgb3JkZXJJbXBvcnRhbnRGaXJzdCkge1xyXG4gICAgICAgIC8vIERldiBub3RlOiBXZSBvbmx5IGhhdmUgdHdvIGFsZ29yaXRobXMgYXQgdGhlIG1vbWVudCwgYnV0IGl0IGlzbid0IGltcG9zc2libGUgdGhhdCB3ZSB3YW50XHJcbiAgICAgICAgLy8gbXVsdGlwbGUgaW4gdGhlIGZ1dHVyZS4gQWxzbyBjb25zdGFudHMgbWFrZSB0aGluZ3Mgc2xpZ2h0bHkgY2xlYXJlci5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIlVwZGF0aW5nIHJvb20gc29ydGluZyBhbGdvcml0aG06IFwiLCB7YWxnb3JpdGhtLCBvcmRlckltcG9ydGFudEZpcnN0fSk7XHJcbiAgICAgICAgdGhpcy5fc2V0U3RhdGUoe2FsZ29yaXRobSwgb3JkZXJJbXBvcnRhbnRGaXJzdH0pO1xyXG5cclxuICAgICAgICAvLyBUcmlnZ2VyIGEgcmVzb3J0IG9mIHRoZSBlbnRpcmUgbGlzdCB0byByZWZsZWN0IHRoZSBjaGFuZ2UgaW4gYWxnb3JpdGhtXHJcbiAgICAgICAgdGhpcy5fZ2VuZXJhdGVJbml0aWFsUm9vbUxpc3RzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX2luaXQoKSB7XHJcbiAgICAgICAgLy8gSW5pdGlhbGlzZSBzdGF0ZVxyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRMaXN0cyA9IHtcclxuICAgICAgICAgICAgXCJtLnNlcnZlcl9ub3RpY2VcIjogWy8qIHsgcm9vbToganMtc2RrIHJvb20sIGNhdGVnb3J5OiBzdHJpbmcgfSAqL10sXHJcbiAgICAgICAgICAgIFwiaW0udmVjdG9yLmZha2UuaW52aXRlXCI6IFtdLFxyXG4gICAgICAgICAgICBcIm0uZmF2b3VyaXRlXCI6IFtdLFxyXG4gICAgICAgICAgICBcImltLnZlY3Rvci5mYWtlLnJlY2VudFwiOiBbXSxcclxuICAgICAgICAgICAgW1RBR19ETV06IFtdLFxyXG4gICAgICAgICAgICBcIm0ubG93cHJpb3JpdHlcIjogW10sXHJcbiAgICAgICAgICAgIFwiaW0udmVjdG9yLmZha2UuYXJjaGl2ZWRcIjogW10sXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IHtcclxuICAgICAgICAgICAgLy8gVGhlIHJvb21zIGluIHRoZXNlIGFycmF5cyBhcmUgb3JkZXJlZCBhY2NvcmRpbmcgdG8gZWl0aGVyIHRoZVxyXG4gICAgICAgICAgICAvLyAncmVjZW50cycgYmVoYXZpb3VyIG9yICdtYW51YWwnIGJlaGF2aW91ci5cclxuICAgICAgICAgICAgbGlzdHM6IGRlZmF1bHRMaXN0cyxcclxuICAgICAgICAgICAgcHJlc2VudGF0aW9uTGlzdHM6IGRlZmF1bHRMaXN0cywgLy8gbGlrZSBgbGlzdHNgLCBidXQgd2l0aCBhcnJheXMgb2Ygcm9vbXMgaW5zdGVhZFxyXG4gICAgICAgICAgICByZWFkeTogZmFsc2UsXHJcbiAgICAgICAgICAgIHN0aWNreVJvb21JZDogbnVsbCxcclxuICAgICAgICAgICAgYWxnb3JpdGhtOiBBTEdPX1JFQ0VOVCxcclxuICAgICAgICAgICAgb3JkZXJJbXBvcnRhbnRGaXJzdDogZmFsc2UsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS5tb25pdG9yU2V0dGluZygnUm9vbUxpc3Qub3JkZXJBbHBoYWJldGljYWxseScsIG51bGwpO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUubW9uaXRvclNldHRpbmcoJ1Jvb21MaXN0Lm9yZGVyQnlJbXBvcnRhbmNlJywgbnVsbCk7XHJcbiAgICAgICAgU2V0dGluZ3NTdG9yZS5tb25pdG9yU2V0dGluZygnZmVhdHVyZV9jdXN0b21fdGFncycsIG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXRTdGF0ZShuZXdTdGF0ZSkge1xyXG4gICAgICAgIC8vIElmIHdlJ3JlIGNoYW5naW5nIHRoZSBsaXN0cywgdHJhbnNwYXJlbnRseSBjaGFuZ2UgdGhlIHByZXNlbnRhdGlvbiBsaXN0cyAod2hpY2hcclxuICAgICAgICAvLyBpcyBnaXZlbiB0byByZXF1ZXN0aW5nIGNvbXBvbmVudHMpLiBUaGlzIGRyYW1hdGljYWxseSBzaW1wbGlmaWVzIG91ciBjb2RlIGVsc2V3aGVyZVxyXG4gICAgICAgIC8vIHdoaWxlIGFsc28gZW5zdXJpbmcgd2UgZG9uJ3QgbmVlZCB0byB1cGRhdGUgYWxsIHRoZSBjYWxsaW5nIGNvbXBvbmVudHMgdG8gc3VwcG9ydFxyXG4gICAgICAgIC8vIGNhdGVnb3JpZXMuXHJcbiAgICAgICAgaWYgKG5ld1N0YXRlWydsaXN0cyddKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHByZXNlbnRhdGlvbkxpc3RzID0ge307XHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IG9mIE9iamVjdC5rZXlzKG5ld1N0YXRlWydsaXN0cyddKSkge1xyXG4gICAgICAgICAgICAgICAgcHJlc2VudGF0aW9uTGlzdHNba2V5XSA9IG5ld1N0YXRlWydsaXN0cyddW2tleV0ubWFwKChlKSA9PiBlLnJvb20pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG5ld1N0YXRlWydwcmVzZW50YXRpb25MaXN0cyddID0gcHJlc2VudGF0aW9uTGlzdHM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gT2JqZWN0LmFzc2lnbih0aGlzLl9zdGF0ZSwgbmV3U3RhdGUpO1xyXG4gICAgICAgIHRoaXMuX19lbWl0Q2hhbmdlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX19vbkRpc3BhdGNoKHBheWxvYWQpIHtcclxuICAgICAgICBjb25zdCBsb2dpY2FsbHlSZWFkeSA9IHRoaXMuX21hdHJpeENsaWVudCAmJiB0aGlzLl9zdGF0ZS5yZWFkeTtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3NldHRpbmdfdXBkYXRlZCc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghbG9naWNhbGx5UmVhZHkpIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIHN3aXRjaCAocGF5bG9hZC5zZXR0aW5nTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJSb29tTGlzdC5vcmRlckFscGhhYmV0aWNhbGx5XCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlU29ydGluZ0FsZ29yaXRobShwYXlsb2FkLm5ld1ZhbHVlID8gQUxHT19BTFBIQUJFVElDIDogQUxHT19SRUNFTlQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zdGF0ZS5vcmRlckltcG9ydGFudEZpcnN0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIlJvb21MaXN0Lm9yZGVyQnlJbXBvcnRhbmNlXCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlU29ydGluZ0FsZ29yaXRobSh0aGlzLl9zdGF0ZS5hbGdvcml0aG0sIHBheWxvYWQubmV3VmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiZmVhdHVyZV9jdXN0b21fdGFnc1wiOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7dGFnc0VuYWJsZWQ6IHBheWxvYWQubmV3VmFsdWV9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZ2VuZXJhdGVJbml0aWFsUm9vbUxpc3RzKCk7IC8vIFRhZ3MgbWVhbnMgd2UgaGF2ZSB0byBzdGFydCBmcm9tIHNjcmF0Y2hcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIC8vIEluaXRpYWxpc2Ugc3RhdGUgYWZ0ZXIgaW5pdGlhbCBzeW5jXHJcbiAgICAgICAgICAgIGNhc2UgJ01hdHJpeEFjdGlvbnMuc3luYyc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghKHBheWxvYWQucHJldlN0YXRlICE9PSAnUFJFUEFSRUQnICYmIHBheWxvYWQuc3RhdGUgPT09ICdQUkVQQVJFRCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gQWx3YXlzIGVuc3VyZSB0aGF0IHdlIHNldCBhbnkgc3RhdGUgbmVlZGVkIGZvciBzZXR0aW5ncyBoZXJlLiBJdCBpcyBwb3NzaWJsZSB0aGF0XHJcbiAgICAgICAgICAgICAgICAvLyBzZXR0aW5nIHVwZGF0ZXMgdHJpZ2dlciBvbiBzdGFydHVwIGJlZm9yZSB3ZSBhcmUgcmVhZHkgdG8gc3luYywgc28gd2Ugd2FudCB0byBtYWtlXHJcbiAgICAgICAgICAgICAgICAvLyBzdXJlIHRoYXQgdGhlIHJpZ2h0IHN0YXRlIGlzIGluIHBsYWNlIGJlZm9yZSB3ZSBhY3R1YWxseSByZWFjdCB0byB0aG9zZSBjaGFuZ2VzLlxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHt0YWdzRW5hYmxlZDogU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9jdXN0b21fdGFnc1wiKX0pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX21hdHJpeENsaWVudCA9IHBheWxvYWQubWF0cml4Q2xpZW50O1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG9yZGVyQnlJbXBvcnRhbmNlID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcIlJvb21MaXN0Lm9yZGVyQnlJbXBvcnRhbmNlXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3JkZXJBbHBoYWJldGljYWxseSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJSb29tTGlzdC5vcmRlckFscGhhYmV0aWNhbGx5XCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVTb3J0aW5nQWxnb3JpdGhtKG9yZGVyQWxwaGFiZXRpY2FsbHkgPyBBTEdPX0FMUEhBQkVUSUMgOiBBTEdPX1JFQ0VOVCwgb3JkZXJCeUltcG9ydGFuY2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdNYXRyaXhBY3Rpb25zLlJvb20ucmVjZWlwdCc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghbG9naWNhbGx5UmVhZHkpIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIEZpcnN0IHNlZSBpZiB0aGUgcmVjZWlwdCBldmVudCBpcyBmb3Igb3VyIG93biB1c2VyLiBJZiBpdCB3YXMsIHRyaWdnZXJcclxuICAgICAgICAgICAgICAgIC8vIGEgcm9vbSB1cGRhdGUgKHdlIHByb2JhYmx5IHJlYWQgdGhlIHJvb20gb24gYSBkaWZmZXJlbnQgZGV2aWNlKS5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG15VXNlcklkID0gdGhpcy5fbWF0cml4Q2xpZW50LmdldFVzZXJJZCgpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBldmVudElkIG9mIE9iamVjdC5rZXlzKHBheWxvYWQuZXZlbnQuZ2V0Q29udGVudCgpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlY2VpcHRVc2VycyA9IE9iamVjdC5rZXlzKHBheWxvYWQuZXZlbnQuZ2V0Q29udGVudCgpW2V2ZW50SWRdWydtLnJlYWQnXSB8fCB7fSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlY2VpcHRVc2Vycy5pbmNsdWRlcyhteVVzZXJJZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fcm9vbVVwZGF0ZVRyaWdnZXJlZChwYXlsb2FkLnJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnTWF0cml4QWN0aW9ucy5Sb29tLnRhZ3MnOiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWxvZ2ljYWxseVJlYWR5KSBicmVhaztcclxuICAgICAgICAgICAgICAgIC8vIFRPRE86IEZpZ3VyZSBvdXQgd2hpY2ggcm9vbXMgY2hhbmdlZCBpbiB0aGUgdGFnIGFuZCBvbmx5IGNoYW5nZSB0aG9zZS5cclxuICAgICAgICAgICAgICAgIC8vIFRoaXMgaXMgdmVyeSBibHVudCBhbmQgd2lwZXMgb3V0IHRoZSBzdGlja3kgcm9vbSBzdHVmZlxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ2VuZXJhdGVJbml0aWFsUm9vbUxpc3RzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ01hdHJpeEFjdGlvbnMuUm9vbS50aW1lbGluZSc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghbG9naWNhbGx5UmVhZHkgfHxcclxuICAgICAgICAgICAgICAgICAgICAhcGF5bG9hZC5pc0xpdmVFdmVudCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICFwYXlsb2FkLmlzTGl2ZVVuZmlsdGVyZWRSb29tVGltZWxpbmVFdmVudCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICF0aGlzLl9ldmVudFRyaWdnZXJzUmVjZW50UmVvcmRlcihwYXlsb2FkLmV2ZW50KSB8fFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3N0YXRlLmFsZ29yaXRobSAhPT0gQUxHT19SRUNFTlRcclxuICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX3Jvb21VcGRhdGVUcmlnZ2VyZWQocGF5bG9hZC5ldmVudC5nZXRSb29tSWQoKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIC8vIFdoZW4gYW4gZXZlbnQgaXMgZGVjcnlwdGVkLCBpdCBjb3VsZCBtZWFuIHdlIG5lZWQgdG8gcmVvcmRlciB0aGUgcm9vbVxyXG4gICAgICAgICAgICAvLyBsaXN0IGJlY2F1c2Ugd2Ugbm93IGtub3cgdGhlIHR5cGUgb2YgdGhlIGV2ZW50LlxyXG4gICAgICAgICAgICBjYXNlICdNYXRyaXhBY3Rpb25zLkV2ZW50LmRlY3J5cHRlZCc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghbG9naWNhbGx5UmVhZHkpIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb21JZCA9IHBheWxvYWQuZXZlbnQuZ2V0Um9vbUlkKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2UgbWF5IGhhdmUgZGVjcnlwdGVkIGFuIGV2ZW50IHdpdGhvdXQgYSByb29tSWQgKGUuZyB0b19kZXZpY2UpXHJcbiAgICAgICAgICAgICAgICBpZiAoIXJvb21JZCkgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9vbSA9IHRoaXMuX21hdHJpeENsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gV2Ugc29tZWhvdyBkZWNyeXB0ZWQgYW4gZXZlbnQgZm9yIGEgcm9vbSBvdXIgY2xpZW50IGlzIHVuYXdhcmUgb2ZcclxuICAgICAgICAgICAgICAgIGlmICghcm9vbSkgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbGl2ZVRpbWVsaW5lID0gcm9vbS5nZXRMaXZlVGltZWxpbmUoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50VGltZWxpbmUgPSByb29tLmdldFRpbWVsaW5lRm9yRXZlbnQocGF5bG9hZC5ldmVudC5nZXRJZCgpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBFaXRoZXIgdGhpcyBldmVudCB3YXMgbm90IGFkZGVkIHRvIHRoZSBsaXZlIHRpbWVsaW5lIChlLmcuIHBhZ2luYXRpb24pXHJcbiAgICAgICAgICAgICAgICAvLyBvciBpdCBkb2Vzbid0IGFmZmVjdCB0aGUgb3JkZXJpbmcgb2YgdGhlIHJvb20gbGlzdC5cclxuICAgICAgICAgICAgICAgIGlmIChsaXZlVGltZWxpbmUgIT09IGV2ZW50VGltZWxpbmUgfHwgIXRoaXMuX2V2ZW50VHJpZ2dlcnNSZWNlbnRSZW9yZGVyKHBheWxvYWQuZXZlbnQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5fcm9vbVVwZGF0ZVRyaWdnZXJlZChyb29tSWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdNYXRyaXhBY3Rpb25zLmFjY291bnREYXRhJzoge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFsb2dpY2FsbHlSZWFkeSkgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBpZiAocGF5bG9hZC5ldmVudF90eXBlICE9PSAnbS5kaXJlY3QnKSBicmVhaztcclxuICAgICAgICAgICAgICAgIC8vIFRPRE86IEZpZ3VyZSBvdXQgd2hpY2ggcm9vbXMgY2hhbmdlZCBpbiB0aGUgZGlyZWN0IGNoYXQgYW5kIG9ubHkgY2hhbmdlIHRob3NlLlxyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBpcyB2ZXJ5IGJsdW50IGFuZCB3aXBlcyBvdXQgdGhlIHN0aWNreSByb29tIHN0dWZmXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9nZW5lcmF0ZUluaXRpYWxSb29tTGlzdHMoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnTWF0cml4QWN0aW9ucy5Sb29tLm15TWVtYmVyc2hpcCc6IHtcclxuICAgICAgICAgICAgICAgIGlmICghbG9naWNhbGx5UmVhZHkpIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcm9vbVVwZGF0ZVRyaWdnZXJlZChwYXlsb2FkLnJvb20ucm9vbUlkLCB0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgLy8gVGhpcyBjb3VsZCBiZSBhIG5ldyByb29tIHRoYXQgd2UndmUgYmVlbiBpbnZpdGVkIHRvLCBqb2luZWQgb3IgY3JlYXRlZFxyXG4gICAgICAgICAgICAvLyB3ZSB3b24ndCBnZXQgYSBSb29tTWVtYmVyLm1lbWJlcnNoaXAgZm9yIHRoZXNlIGNhc2VzIGlmIHdlJ3JlIG5vdCBhbHJlYWR5XHJcbiAgICAgICAgICAgIC8vIGEgbWVtYmVyLlxyXG4gICAgICAgICAgICBjYXNlICdNYXRyaXhBY3Rpb25zLlJvb20nOiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWxvZ2ljYWxseVJlYWR5KSBicmVhaztcclxuICAgICAgICAgICAgICAgIHRoaXMuX3Jvb21VcGRhdGVUcmlnZ2VyZWQocGF5bG9hZC5yb29tLnJvb21JZCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIC8vIFRPRE86IFJlLWVuYWJsZSBvcHRpbWlzdGljIHVwZGF0ZXMgd2hlbiB3ZSBzdXBwb3J0IGRyYWdnaW5nIGFnYWluXHJcbiAgICAgICAgICAgIC8vIGNhc2UgJ1Jvb21MaXN0QWN0aW9ucy50YWdSb29tLnBlbmRpbmcnOiB7XHJcbiAgICAgICAgICAgIC8vICAgICBpZiAoIWxvZ2ljYWxseVJlYWR5KSBicmVhaztcclxuICAgICAgICAgICAgLy8gICAgIC8vIFhYWDogd2Ugb25seSBzaG93IG9uZSBvcHRpbWlzdGljIHVwZGF0ZSBhdCBhbnkgb25lIHRpbWUuXHJcbiAgICAgICAgICAgIC8vICAgICAvLyBJZGVhbGx5IHdlIHNob3VsZCBiZSBtYWtpbmcgYSBsaXN0IG9mIGluLWZsaWdodCByZXF1ZXN0c1xyXG4gICAgICAgICAgICAvLyAgICAgLy8gdGhhdCBhcmUgYmFja2VkIGJ5IHRyYW5zYWN0aW9uIElEcy4gVW50aWwgdGhlIGpzLXNka1xyXG4gICAgICAgICAgICAvLyAgICAgLy8gc3VwcG9ydHMgdGhpcywgd2UncmUgc3R1Y2sgd2l0aCBvbmx5IGJlaW5nIGFibGUgdG8gdXNlXHJcbiAgICAgICAgICAgIC8vICAgICAvLyB0aGUgbW9zdCByZWNlbnQgb3B0aW1pc3RpYyB1cGRhdGUuXHJcbiAgICAgICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhcIiEhIE9wdGltaXN0aWMgdGFnOiBcIiwgcGF5bG9hZCk7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy8gYnJlYWs7XHJcbiAgICAgICAgICAgIC8vIGNhc2UgJ1Jvb21MaXN0QWN0aW9ucy50YWdSb29tLmZhaWx1cmUnOiB7XHJcbiAgICAgICAgICAgIC8vICAgICBpZiAoIWxvZ2ljYWxseVJlYWR5KSBicmVhaztcclxuICAgICAgICAgICAgLy8gICAgIC8vIFJlc2V0IHN0YXRlIGFjY29yZGluZyB0byBqcy1zZGtcclxuICAgICAgICAgICAgLy8gICAgIGNvbnNvbGUubG9nKFwiISEgT3B0aW1pc3RpYyB0YWcgZmFpbHVyZTogXCIsIHBheWxvYWQpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdvbl9jbGllbnRfbm90X3ZpYWJsZSc6XHJcbiAgICAgICAgICAgIGNhc2UgJ29uX2xvZ2dlZF9vdXQnOiB7XHJcbiAgICAgICAgICAgICAgICAvLyBSZXNldCBzdGF0ZSB3aXRob3V0IHB1c2hpbmcgYW4gdXBkYXRlIHRvIHRoZSB2aWV3LCB3aGljaCBnZW5lcmFsbHkgYXNzdW1lcyB0aGF0XHJcbiAgICAgICAgICAgICAgICAvLyB0aGUgbWF0cml4IGNsaWVudCBpc24ndCBgbnVsbGAgYW5kIHNvIGNhdXNpbmcgYSByZS1yZW5kZXIgd2lsbCBjYXVzZSBOUEVzLlxyXG4gICAgICAgICAgICAgICAgdGhpcy5faW5pdCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50ID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndmlld19yb29tJzoge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFsb2dpY2FsbHlSZWFkeSkgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gTm90ZTogaXQgaXMgaW1wb3J0YW50IHRoYXQgd2Ugc2V0IGEgbmV3IHN0aWNreVJvb21JZCBiZWZvcmUgc2V0dGluZyB0aGUgb2xkIHJvb21cclxuICAgICAgICAgICAgICAgIC8vIHRvIElETEUuIElmIHdlIGRvbid0LCB0aGUgd3Jvbmcgcm9vbSBnZXRzIGNvdW50ZWQgYXMgc3RpY2t5LlxyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudFN0aWNreUlkID0gdGhpcy5fc3RhdGUuc3RpY2t5Um9vbUlkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoe3N0aWNreVJvb21JZDogcGF5bG9hZC5yb29tX2lkfSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFN0aWNreUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2V0Um9vbUNhdGVnb3J5KHRoaXMuX21hdHJpeENsaWVudC5nZXRSb29tKGN1cnJlbnRTdGlja3lJZCksIENBVEVHT1JZX0lETEUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfcm9vbVVwZGF0ZVRyaWdnZXJlZChyb29tSWQsIGlnbm9yZVN0aWNreSkge1xyXG4gICAgICAgIC8vIFdlIGRvbid0IGNhbGN1bGF0ZSBjYXRlZ29yaWVzIGZvciBzdGlja3kgcm9vbXMgYmVjYXVzZSB3ZSBoYXZlIGEgbW9kZXJhdGVcclxuICAgICAgICAvLyBpbnRlcmVzdCBpbiB0cnlpbmcgdG8gbWFpbnRhaW4gdGhlIGNhdGVnb3J5IHRoYXQgdGhleSB3ZXJlIGxhc3QgaW4gYmVmb3JlXHJcbiAgICAgICAgLy8gYmVpbmcgYXJ0aWZpY2lhbGx5IGZsYWdnZWQgYXMgSURMRS4gQWxzbywgdGhpcyByZWR1Y2VzIHRoZSBhbW91bnQgb2YgdGltZVxyXG4gICAgICAgIC8vIHdlIHNwZW5kIGluIF9zZXRSb29tQ2F0ZWdvcnkgZXZlciBzbyBzbGlnaHRseS5cclxuICAgICAgICBpZiAodGhpcy5fc3RhdGUuc3RpY2t5Um9vbUlkICE9PSByb29tSWQgfHwgaWdub3JlU3RpY2t5KSB7XHJcbiAgICAgICAgICAgIC8vIE1pY3JvIG9wdGltaXphdGlvbjogT25seSBsb29rIHVwIHRoZSByb29tIGlmIHdlJ3JlIGNvbmZpZGVudCB3ZSdsbCBuZWVkIGl0LlxyXG4gICAgICAgICAgICBjb25zdCByb29tID0gdGhpcy5fbWF0cml4Q2xpZW50LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICAgICAgaWYgKCFyb29tKSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICBjb25zdCBjYXRlZ29yeSA9IHRoaXMuX2NhbGN1bGF0ZUNhdGVnb3J5KHJvb20pO1xyXG4gICAgICAgICAgICB0aGlzLl9zZXRSb29tQ2F0ZWdvcnkocm9vbSwgY2F0ZWdvcnkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfZmlsdGVyVGFncyh0YWdzKSB7XHJcbiAgICAgICAgdGFncyA9IHRhZ3MgPyBPYmplY3Qua2V5cyh0YWdzKSA6IFtdO1xyXG4gICAgICAgIGlmICh0aGlzLl9zdGF0ZS50YWdzRW5hYmxlZCkgcmV0dXJuIHRhZ3M7XHJcbiAgICAgICAgcmV0dXJuIHRhZ3MuZmlsdGVyKCh0KSA9PiBrbm93bkxpc3RzLmhhcyh0KSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldFJlY29tbWVuZGVkVGFnc0ZvclJvb20ocm9vbSkge1xyXG4gICAgICAgIGNvbnN0IHRhZ3MgPSBbXTtcclxuXHJcbiAgICAgICAgY29uc3QgbXlNZW1iZXJzaGlwID0gcm9vbS5nZXRNeU1lbWJlcnNoaXAoKTtcclxuICAgICAgICBpZiAobXlNZW1iZXJzaGlwID09PSAnam9pbicgfHwgbXlNZW1iZXJzaGlwID09PSAnaW52aXRlJykge1xyXG4gICAgICAgICAgICAvLyBTdGFjayB0aGUgdXNlcidzIHRhZ3Mgb24gdG9wXHJcbiAgICAgICAgICAgIHRhZ3MucHVzaCguLi50aGlzLl9maWx0ZXJUYWdzKHJvb20udGFncykpO1xyXG5cclxuICAgICAgICAgICAgLy8gT3JkZXIgbWF0dGVycyBoZXJlOiBUaGUgRE1Sb29tTWFwIHVwZGF0ZXMgYmVmb3JlIGludml0ZXNcclxuICAgICAgICAgICAgLy8gYXJlIGFjY2VwdGVkLCBzbyB3ZSBjaGVjayB0byBzZWUgaWYgdGhlIHJvb20gaXMgYW4gaW52aXRlXHJcbiAgICAgICAgICAgIC8vIGZpcnN0LCB0aGVuIGlmIGl0IGlzIGEgZGlyZWN0IGNoYXQsIGFuZCBmaW5hbGx5IGRlZmF1bHRcclxuICAgICAgICAgICAgLy8gdG8gdGhlIFwicmVjZW50c1wiIGxpc3QuXHJcbiAgICAgICAgICAgIGNvbnN0IGRtUm9vbU1hcCA9IERNUm9vbU1hcC5zaGFyZWQoKTtcclxuICAgICAgICAgICAgaWYgKG15TWVtYmVyc2hpcCA9PT0gJ2ludml0ZScpIHtcclxuICAgICAgICAgICAgICAgIHRhZ3MucHVzaChcImltLnZlY3Rvci5mYWtlLmludml0ZVwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChkbVJvb21NYXAuZ2V0VXNlcklkRm9yUm9vbUlkKHJvb20ucm9vbUlkKSAmJiB0YWdzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgLy8gV2UgaW50ZW50aW9uYWxseSBkb24ndCBkdXBsaWNhdGUgcm9vbXMgaW4gb3RoZXIgdGFncyBpbnRvIHRoZSBwZW9wbGUgbGlzdFxyXG4gICAgICAgICAgICAgICAgLy8gYXMgYSBmZWF0dXJlLlxyXG4gICAgICAgICAgICAgICAgdGFncy5wdXNoKFRBR19ETSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGFncy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHRhZ3MucHVzaChcImltLnZlY3Rvci5mYWtlLnJlY2VudFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAobXlNZW1iZXJzaGlwKSB7IC8vIG51bGwtZ3VhcmQgYXMgbnVsbCBtZWFucyBpdCB3YXMgcGVla2VkXHJcbiAgICAgICAgICAgIHRhZ3MucHVzaChcImltLnZlY3Rvci5mYWtlLmFyY2hpdmVkXCIpO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHJldHVybiB0YWdzO1xyXG4gICAgfVxyXG5cclxuICAgIF9zbG90Um9vbUludG9MaXN0KHJvb20sIGNhdGVnb3J5LCB0YWcsIGV4aXN0aW5nRW50cmllcywgbmV3TGlzdCwgbGFzdFRpbWVzdGFtcEZuKSB7XHJcbiAgICAgICAgY29uc3QgdGFyZ2V0Q2F0ZWdvcnlJbmRleCA9IENBVEVHT1JZX09SREVSLmluZGV4T2YoY2F0ZWdvcnkpO1xyXG5cclxuICAgICAgICBsZXQgY2F0ZWdvcnlDb21wYXJhdG9yID0gKGEsIGIpID0+IGxhc3RUaW1lc3RhbXBGbihhLnJvb20pID49IGxhc3RUaW1lc3RhbXBGbihiLnJvb20pO1xyXG4gICAgICAgIGNvbnN0IHNvcnRBbGdvcml0aG0gPSBnZXRMaXN0QWxnb3JpdGhtKHRhZywgdGhpcy5fc3RhdGUuYWxnb3JpdGhtKTtcclxuICAgICAgICBpZiAoc29ydEFsZ29yaXRobSA9PT0gQUxHT19SRUNFTlQpIHtcclxuICAgICAgICAgICAgY2F0ZWdvcnlDb21wYXJhdG9yID0gKGEsIGIpID0+IHRoaXMuX3JlY2VudHNDb21wYXJhdG9yKGEsIGIsIGxhc3RUaW1lc3RhbXBGbik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzb3J0QWxnb3JpdGhtID09PSBBTEdPX0FMUEhBQkVUSUMpIHtcclxuICAgICAgICAgICAgY2F0ZWdvcnlDb21wYXJhdG9yID0gKGEsIGIpID0+IHRoaXMuX2xleGljb2dyYXBoaWNhbENvbXBhcmF0b3IoYSwgYik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBUaGUgc2xvdHRpbmcgYWxnb3JpdGhtIHdvcmtzIGJ5IHRyeWluZyB0byBwb3NpdGlvbiB0aGUgcm9vbSBpbiB0aGUgbW9zdCByZWxldmFudFxyXG4gICAgICAgIC8vIGNhdGVnb3J5IG9mIHRoZSBsaXN0IChyZWQgPiBncmV5ID4gZXRjKS4gVG8gYWNjb21wbGlzaCB0aGlzLCB3ZSBuZWVkIHRvIGNvbnNpZGVyXHJcbiAgICAgICAgLy8gYSBjb3VwbGUgY2FzZXM6IHRoZSBjYXRlZ29yeSBleGlzdGluZyBpbiB0aGUgbGlzdCBidXQgaGF2aW5nIG90aGVyIHJvb21zIGluIGl0IGFuZFxyXG4gICAgICAgIC8vIHRoZSBjYXNlIG9mIHRoZSBjYXRlZ29yeSBzaW1wbHkgbm90IGV4aXN0aW5nIGFuZCBuZWVkaW5nIHRvIGJlIHN0YXJ0ZWQuIEluIG9yZGVyIHRvXHJcbiAgICAgICAgLy8gZG8gdGhpcyBlZmZpY2llbnRseSwgd2Ugb25seSB3YW50IHRvIGl0ZXJhdGUgb3ZlciB0aGUgbGlzdCBvbmNlIGFuZCBzb2x2ZSBvdXIgc29ydGluZ1xyXG4gICAgICAgIC8vIHByb2JsZW0gYXMgd2UgZ28uXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBGaXJzdGx5LCB3ZSdsbCByZW1vdmUgYW55IGV4aXN0aW5nIGVudHJ5IHRoYXQgcmVmZXJlbmNlcyB0aGUgcm9vbSB3ZSdyZSB0cnlpbmcgdG9cclxuICAgICAgICAvLyBpbnNlcnQuIFdlIGRvbid0IHJlYWxseSB3YW50IHRvIGNvbnNpZGVyIHRoZSBvbGQgZW50cnkgYW5kIHdhbnQgdG8gcmVjcmVhdGUgaXQuIFdlXHJcbiAgICAgICAgLy8gYWxzbyBleGNsdWRlIHRoZSBzdGlja3kgKGN1cnJlbnRseSBhY3RpdmUpIHJvb20gZnJvbSB0aGUgY2F0ZWdvcml6YXRpb24gbG9naWMgYW5kXHJcbiAgICAgICAgLy8gbGV0IGl0IHBhc3MgdGhyb3VnaCB3aGVyZXZlciBpdCByZXNpZGVzIGluIHRoZSBsaXN0OiBpdCBzaG91bGRuJ3QgYmUgbW92aW5nIGFyb3VuZFxyXG4gICAgICAgIC8vIHRoZSBsaXN0IHRvbyBtdWNoLCBzbyB3ZSB3YW50IHRvIGtlZXAgaXQgd2hlcmUgaXQgaXMuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBUaGUgY2FzZSBvZiB0aGUgY2F0ZWdvcnkgd2Ugd2FudCBleGlzdGluZyBpcyBlYXN5IHRvIGhhbmRsZTogb25jZSB3ZSBoaXQgdGhlIGNhdGVnb3J5LFxyXG4gICAgICAgIC8vIGZpbmQgdGhlIHJvb20gdGhhdCBoYXMgYSBtb3N0IHJlY2VudCBldmVudCBsYXRlciB0aGFuIG91ciBvd24gYW5kIGluc2VydCBqdXN0IGJlZm9yZVxyXG4gICAgICAgIC8vIHRoYXQgKG1ha2luZyB1cyB0aGUgbW9yZSByZWNlbnQgcm9vbSkuIElmIHdlIGVuZCB1cCBoaXR0aW5nIHRoZSBuZXh0IGNhdGVnb3J5IGJlZm9yZVxyXG4gICAgICAgIC8vIHdlIGNhbiBzbG90IHRoZSByb29tIGluLCBpbnNlcnQgdGhlIHJvb20gYXQgdGhlIHRvcCBvZiB0aGUgY2F0ZWdvcnkgYXMgYSBmYWxsYmFjay4gV2VcclxuICAgICAgICAvLyBkbyB0aGlzIHRvIGVuc3VyZSB0aGF0IHRoZSByb29tIGRvZXNuJ3QgZ28gdG9vIGZhciBkb3duIHRoZSBsaXN0IGdpdmVuIGl0IHdhcyBwcmV2aW91c2x5XHJcbiAgICAgICAgLy8gY29uc2lkZXJlZCBpbXBvcnRhbnQgKGluIHRoZSBjYXNlIG9mIGdvaW5nIGRvd24gaW4gY2F0ZWdvcnkpIG9yIGlzIG5vdyBtb3JlIGltcG9ydGFudFxyXG4gICAgICAgIC8vIChzdWRkZW5seSBiZWNvbWluZyByZWQsIGZvciBpbnN0YW5jZSkuIFRoZSBib3VuZGFyeSB0cmFja2luZyBpcyBob3cgd2UgZW5kIHVwIGFjaGlldmluZ1xyXG4gICAgICAgIC8vIHRoaXMsIGFzIGRlc2NyaWJlZCBpbiB0aGUgbmV4dCBwYXJhZ3JhcGhzLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gVGhlIG90aGVyIGNhc2Ugb2YgdGhlIGNhdGVnb3J5IG5vdCBhbHJlYWR5IGV4aXN0aW5nIGlzIGEgYml0IG1vcmUgY29tcGxpY2F0ZWQuIFdlIHRyYWNrXHJcbiAgICAgICAgLy8gdGhlIGJvdW5kYXJpZXMgb2YgZWFjaCBjYXRlZ29yeSByZWxhdGl2ZSB0byB0aGUgbGlzdCB3ZSdyZSBjdXJyZW50bHkgYnVpbGRpbmcgc28gdGhhdFxyXG4gICAgICAgIC8vIHdoZW4gd2UgbWlzcyB0aGUgY2F0ZWdvcnkgd2UgY2FuIGluc2VydCB0aGUgcm9vbSBhdCB0aGUgcmlnaHQgc3BvdC4gTW9zdCBpbXBvcnRhbnRseSwgd2VcclxuICAgICAgICAvLyBjYW4ndCBhc3N1bWUgdGhhdCB0aGUgZW5kIG9mIHRoZSBsaXN0IGJlaW5nIGJ1aWx0IGlzIHRoZSByaWdodCBzcG90IGJlY2F1c2Ugb2YgdGhlIGxhc3RcclxuICAgICAgICAvLyBwYXJhZ3JhcGgncyByZXF1aXJlbWVudDogdGhlIHJvb20gc2hvdWxkIGJlIHB1dCB0byB0aGUgdG9wIG9mIGEgY2F0ZWdvcnkgaWYgdGhlIGNhdGVnb3J5XHJcbiAgICAgICAgLy8gcnVucyBvdXQgb2YgcGxhY2VzIHRvIHB1dCBpdC5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIEFsbCB0b2xkLCBvdXIgdHJhY2tpbmcgbG9va3Mgc29tZXRoaW5nIGxpa2UgdGhpczpcclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIC0tLS0tLSBBIDwtIENhdGVnb3J5IGJvdW5kYXJ5IChzdGFydCBvZiByZWQpXHJcbiAgICAgICAgLy8gIFJFRFxyXG4gICAgICAgIC8vICBSRURcclxuICAgICAgICAvLyAgUkVEXHJcbiAgICAgICAgLy8gLS0tLS0tIEIgPC0gSW4gdGhpcyBleGFtcGxlLCB3ZSBoYXZlIGEgZ3JleSByb29tIHdlIHdhbnQgdG8gaW5zZXJ0LlxyXG4gICAgICAgIC8vICBCT0xEXHJcbiAgICAgICAgLy8gIEJPTERcclxuICAgICAgICAvLyAtLS0tLS0gQ1xyXG4gICAgICAgIC8vICBJRExFXHJcbiAgICAgICAgLy8gIElETEVcclxuICAgICAgICAvLyAtLS0tLS0gRCA8LSBFbmQgb2YgbGlzdFxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gR2l2ZW4gdGhhdCBleGFtcGxlLCBhbmQgb3VyIGRlc2lyZSB0byBpbnNlcnQgYSBHUkVZIHJvb20gaW50byB0aGUgbGlzdCwgdGhpcyBpdGVyYXRlc1xyXG4gICAgICAgIC8vIG92ZXIgdGhlIHJvb20gbGlzdCB1bnRpbCBpdCByZWFsaXplcyB0aGF0IEJPTEQgY29tZXMgYWZ0ZXIgR1JFWSBhbmQgd2UncmUgbm8gbG9uZ2VyXHJcbiAgICAgICAgLy8gaW4gdGhlIFJFRCBzZWN0aW9uLiBCZWNhdXNlIHRoZXJlJ3Mgbm8gcm9vbXMgdGhlcmUsIHdlIHNpbXBseSBpbnNlcnQgdGhlcmUgd2hpY2ggaXNcclxuICAgICAgICAvLyBhbHNvIGEgXCJjYXRlZ29yeSBib3VuZGFyeVwiLiBJZiB3ZSBjaGFuZ2UgdGhlIGV4YW1wbGUgdG8gd2FudGluZyB0byBpbnNlcnQgYSBCT0xEIHJvb21cclxuICAgICAgICAvLyB3aGljaCBjYW4ndCBiZSBvcmRlcmVkIGJ5IHRpbWVzdGFtcCB3aXRoIHRoZSBleGlzdGluZyBjb3VwbGUgcm9vbXMsIHdlIHdvdWxkIHN0aWxsIG1ha2VcclxuICAgICAgICAvLyB1c2Ugb2YgdGhlIGJvdW5kYXJ5IGZsYWcgdG8gaW5zZXJ0IGF0IEIgYmVmb3JlIGNoYW5naW5nIHRoZSBib3VuZGFyeSBpbmRpY2F0b3IgdG8gQy5cclxuXHJcbiAgICAgICAgbGV0IGRlc2lyZWRDYXRlZ29yeUJvdW5kYXJ5SW5kZXggPSAwO1xyXG4gICAgICAgIGxldCBmb3VuZEJvdW5kYXJ5ID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IHB1c2hlZEVudHJ5ID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGZvciAoY29uc3QgZW50cnkgb2YgZXhpc3RpbmdFbnRyaWVzKSB7XHJcbiAgICAgICAgICAgIC8vIFdlIGluc2VydCBvdXIgb3duIHJlY29yZCBhcyBuZWVkZWQsIHNvIGRvbid0IGxldCB0aGUgb2xkIG9uZSB0aHJvdWdoLlxyXG4gICAgICAgICAgICBpZiAoZW50cnkucm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gaWYgdGhlIGxpc3QgaXMgYSByZWNlbnQgbGlzdCwgYW5kIHRoZSByb29tIGFwcGVhcnMgaW4gdGhpcyBsaXN0LCBhbmQgd2UncmVcclxuICAgICAgICAgICAgLy8gbm90IGxvb2tpbmcgYXQgYSBzdGlja3kgcm9vbSAoc3RpY2t5IHJvb21zIGhhdmUgdW5yZWxpYWJsZSBjYXRlZ29yaWVzKSwgdHJ5XHJcbiAgICAgICAgICAgIC8vIHRvIHNsb3QgdGhlIG5ldyByb29tIGluXHJcbiAgICAgICAgICAgIGlmIChlbnRyeS5yb29tLnJvb21JZCAhPT0gdGhpcy5fc3RhdGUuc3RpY2t5Um9vbUlkICYmICFwdXNoZWRFbnRyeSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZW50cnlDYXRlZ29yeUluZGV4ID0gQ0FURUdPUllfT1JERVIuaW5kZXhPZihlbnRyeS5jYXRlZ29yeSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gQXMgcGVyIGFib3ZlLCBjaGVjayBpZiB3ZSdyZSBtZWV0aW5nIHRoYXQgYm91bmRhcnkgd2Ugd2FudGVkIHRvIGxvY2F0ZS5cclxuICAgICAgICAgICAgICAgIGlmIChlbnRyeUNhdGVnb3J5SW5kZXggPj0gdGFyZ2V0Q2F0ZWdvcnlJbmRleCAmJiAhZm91bmRCb3VuZGFyeSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlc2lyZWRDYXRlZ29yeUJvdW5kYXJ5SW5kZXggPSBuZXdMaXN0Lmxlbmd0aCAtIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgZm91bmRCb3VuZGFyeSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gSWYgd2UndmUgaGl0IHRoZSB0b3Agb2YgYSBib3VuZGFyeSBiZXlvbmQgb3VyIHRhcmdldCBjYXRlZ29yeSwgaW5zZXJ0IGF0IHRoZSB0b3Agb2ZcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBncm91cGluZyB0byBlbnN1cmUgdGhlIHJvb20gaXNuJ3Qgc2xvdHRlZCBpbmNvcnJlY3RseS4gT3RoZXJ3aXNlLCB0cnkgdG8gaW5zZXJ0XHJcbiAgICAgICAgICAgICAgICAvLyBiYXNlZCBvbiBtb3N0IHJlY2VudCB0aW1lc3RhbXAuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBjaGFuZ2VkQm91bmRhcnkgPSBlbnRyeUNhdGVnb3J5SW5kZXggPiB0YXJnZXRDYXRlZ29yeUluZGV4O1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudENhdGVnb3J5ID0gZW50cnlDYXRlZ29yeUluZGV4ID09PSB0YXJnZXRDYXRlZ29yeUluZGV4O1xyXG4gICAgICAgICAgICAgICAgaWYgKGNoYW5nZWRCb3VuZGFyeSB8fCAoY3VycmVudENhdGVnb3J5ICYmIGNhdGVnb3J5Q29tcGFyYXRvcih7cm9vbX0sIGVudHJ5KSA8PSAwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjaGFuZ2VkQm91bmRhcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gSWYgd2UgY2hhbmdlZCBhIGJvdW5kYXJ5LCB0aGVuIHdlJ3ZlIGdvbmUgdG9vIGZhciAtIGdvIHRvIHRoZSB0b3Agb2YgdGhlIGxhc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2VjdGlvbiBpbnN0ZWFkLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdMaXN0LnNwbGljZShkZXNpcmVkQ2F0ZWdvcnlCb3VuZGFyeUluZGV4LCAwLCB7cm9vbSwgY2F0ZWdvcnl9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiB3ZSdyZSBvcmRlcmluZyBieSB0aW1lc3RhbXAsIGp1c3QgaW5zZXJ0IG5vcm1hbGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld0xpc3QucHVzaCh7cm9vbSwgY2F0ZWdvcnl9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcHVzaGVkRW50cnkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBGYWxsIHRocm91Z2ggYW5kIGNsb25lIHRoZSBsaXN0LlxyXG4gICAgICAgICAgICBuZXdMaXN0LnB1c2goZW50cnkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFwdXNoZWRFbnRyeSAmJiBkZXNpcmVkQ2F0ZWdvcnlCb3VuZGFyeUluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGAhISBSb29tICR7cm9vbS5yb29tSWR9IG5lYXJseSBsb3N0OiBSYW4gb2ZmIHRoZSBlbmQgb2YgJHt0YWd9YCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgISEgSW5zZXJ0aW5nIGF0IHBvc2l0aW9uICR7ZGVzaXJlZENhdGVnb3J5Qm91bmRhcnlJbmRleH0gd2l0aCBjYXRlZ29yeSAke2NhdGVnb3J5fWApO1xyXG4gICAgICAgICAgICBuZXdMaXN0LnNwbGljZShkZXNpcmVkQ2F0ZWdvcnlCb3VuZGFyeUluZGV4LCAwLCB7cm9vbSwgY2F0ZWdvcnl9KTtcclxuICAgICAgICAgICAgcHVzaGVkRW50cnkgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHB1c2hlZEVudHJ5O1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXRSb29tQ2F0ZWdvcnkocm9vbSwgY2F0ZWdvcnkpIHtcclxuICAgICAgICBpZiAoIXJvb20pIHJldHVybjsgLy8gVGhpcyBzaG91bGQgb25seSBoYXBwZW4gaW4gdGVzdHNcclxuXHJcbiAgICAgICAgY29uc3QgbGlzdHNDbG9uZSA9IHt9O1xyXG5cclxuICAgICAgICAvLyBNaWNybyBvcHRpbWl6YXRpb246IFN1cHBvcnQgbGF6aWx5IGxvYWRpbmcgdGhlIGxhc3QgdGltZXN0YW1wIGluIGEgcm9vbVxyXG4gICAgICAgIGNvbnN0IHRpbWVzdGFtcENhY2hlID0ge307IC8vIHtyb29tSWQgPT4gdHN9XHJcbiAgICAgICAgY29uc3QgbGFzdFRpbWVzdGFtcCA9IChyb29tKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGltZXN0YW1wQ2FjaGVbcm9vbS5yb29tSWRdKSB7XHJcbiAgICAgICAgICAgICAgICB0aW1lc3RhbXBDYWNoZVtyb29tLnJvb21JZF0gPSB0aGlzLl90c09mTmV3ZXN0RXZlbnQocm9vbSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRpbWVzdGFtcENhY2hlW3Jvb20ucm9vbUlkXTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHRhcmdldFRhZ3MgPSB0aGlzLl9nZXRSZWNvbW1lbmRlZFRhZ3NGb3JSb29tKHJvb20pO1xyXG4gICAgICAgIGNvbnN0IGluc2VydGVkSW50b1RhZ3MgPSBbXTtcclxuXHJcbiAgICAgICAgLy8gV2UgbmVlZCB0byBtYWtlIHN1cmUgYWxsIHRoZSB0YWdzIChsaXN0cykgYXJlIHVwZGF0ZWQgd2l0aCB0aGUgcm9vbSdzIG5ldyBwb3NpdGlvbi4gV2VcclxuICAgICAgICAvLyBnZW5lcmFsbHkgb25seSBnZXQgY2FsbGVkIGhlcmUgd2hlbiB0aGVyZSdzIGEgbmV3IHJvb20gdG8gaW5zZXJ0IG9yIGEgcm9vbSBoYXMgcG90ZW50aWFsbHlcclxuICAgICAgICAvLyBjaGFuZ2VkIHBvc2l0aW9ucyB3aXRoaW4gdGhlIGxpc3QuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBXZSBkbyBhbGwgb3VyIGNoZWNrcyBieSBpdGVyYXRpbmcgb3ZlciB0aGUgcm9vbXMgaW4gdGhlIGV4aXN0aW5nIGxpc3RzLCB0cnlpbmcgdG8gaW5zZXJ0XHJcbiAgICAgICAgLy8gb3VyIHJvb20gd2hlcmUgd2UgY2FuLiBBcyBhIGd1aWRpbmcgcHJpbmNpcGxlLCB3ZSBzaG91bGQgYmUgcmVtb3ZpbmcgdGhlIHJvb20gZnJvbSBhbGxcclxuICAgICAgICAvLyB0YWdzLCBhbmQgaW5zZXJ0IHRoZSByb29tIGludG8gdGFyZ2V0VGFncy4gV2Ugc2hvdWxkIHBlcmZvcm0gdGhlIGRlbGV0aW9uIGJlZm9yZSB0aGUgYWRkaXRpb25cclxuICAgICAgICAvLyB3aGVyZSBwb3NzaWJsZSB0byBrZWVwIGEgY29uc2lzdGVudCBzdGF0ZS4gQnkgdGhlIGVuZCBvZiB0aGlzLCB0YXJnZXRUYWdzIHNob3VsZCBiZSB0aGVcclxuICAgICAgICAvLyBzYW1lIGFzIGluc2VydGVkSW50b1RhZ3MuXHJcblxyXG4gICAgICAgIGZvciAoY29uc3Qga2V5IG9mIE9iamVjdC5rZXlzKHRoaXMuX3N0YXRlLmxpc3RzKSkge1xyXG4gICAgICAgICAgICBjb25zdCBzaG91bGRIYXZlUm9vbSA9IHRhcmdldFRhZ3MuaW5jbHVkZXMoa2V5KTtcclxuXHJcbiAgICAgICAgICAgIC8vIFNwZWVkIG9wdGltaXphdGlvbjogRG9uJ3QgZG8gY29tcGxpY2F0ZWQgbWF0aCBpZiB3ZSBkb24ndCBoYXZlIHRvLlxyXG4gICAgICAgICAgICBpZiAoIXNob3VsZEhhdmVSb29tKSB7XHJcbiAgICAgICAgICAgICAgICBsaXN0c0Nsb25lW2tleV0gPSB0aGlzLl9zdGF0ZS5saXN0c1trZXldLmZpbHRlcigoZSkgPT4gZS5yb29tLnJvb21JZCAhPT0gcm9vbS5yb29tSWQpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGdldExpc3RBbGdvcml0aG0oa2V5LCB0aGlzLl9zdGF0ZS5hbGdvcml0aG0pID09PSBBTEdPX01BTlVBTCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTWFudWFsbHkgb3JkZXJlZCB0YWdzIGFyZSBzb3J0ZWQgbGF0ZXIsIHNvIGZvciBub3cgd2UnbGwganVzdCBjbG9uZSB0aGUgdGFnXHJcbiAgICAgICAgICAgICAgICAvLyBhbmQgYWRkIG91ciByb29tIGlmIG5lZWRlZFxyXG4gICAgICAgICAgICAgICAgbGlzdHNDbG9uZVtrZXldID0gdGhpcy5fc3RhdGUubGlzdHNba2V5XS5maWx0ZXIoKGUpID0+IGUucm9vbS5yb29tSWQgIT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGxpc3RzQ2xvbmVba2V5XS5wdXNoKHtyb29tLCBjYXRlZ29yeX0pO1xyXG4gICAgICAgICAgICAgICAgaW5zZXJ0ZWRJbnRvVGFncy5wdXNoKGtleSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsaXN0c0Nsb25lW2tleV0gPSBbXTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBwdXNoZWRFbnRyeSA9IHRoaXMuX3Nsb3RSb29tSW50b0xpc3QoXHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbSwgY2F0ZWdvcnksIGtleSwgdGhpcy5fc3RhdGUubGlzdHNba2V5XSwgbGlzdHNDbG9uZVtrZXldLCBsYXN0VGltZXN0YW1wKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIXB1c2hlZEVudHJ5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVGhpcyBzaG91bGQgcmFyZWx5IGhhcHBlbjogX3Nsb3RSb29tSW50b0xpc3QgaGFzIHNldmVyYWwgY2hlY2tzIHdoaWNoIGF0dGVtcHRcclxuICAgICAgICAgICAgICAgICAgICAvLyB0byBtYWtlIHN1cmUgdGhhdCBhIHJvb20gaXMgbm90IGxvc3QgaW4gdGhlIGxpc3QuIElmIHdlIGRvIGxvc2UgdGhlIHJvb20gdGhvdWdoLFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdlIHNob3VsZG4ndCB0aHJvdyBpdCBvbiB0aGUgZmxvb3IgYW5kIGZvcmdldCBhYm91dCBpdC4gSW5zdGVhZCwgd2Ugc2hvdWxkIGluc2VydFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGl0IHNvbWV3aGVyZS4gV2UnbGwgaW5zZXJ0IGl0IGF0IHRoZSB0b3AgZm9yIGEgY291cGxlIHJlYXNvbnM6IDEpIGl0IGlzIHByb2JhYmx5XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gYW4gaW1wb3J0YW50IHJvb20gZm9yIHRoZSB1c2VyIGFuZCAyKSBpZiB0aGlzIGRvZXMgaGFwcGVuLCB3ZSdkIHdhbnQgYSBidWcgcmVwb3J0LlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgISEgUm9vbSAke3Jvb20ucm9vbUlkfSBuZWFybHkgbG9zdDogRmFpbGVkIHRvIGZpbmQgYSBwb3NpdGlvbmApO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgISEgSW5zZXJ0aW5nIGF0IHBvc2l0aW9uIDAgaW4gdGhlIGxpc3QgYW5kIGZsYWdnaW5nIGFzIGluc2VydGVkYCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKFwiISEgQWRkaXRpb25hbCBpbmZvOiBcIiwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgIGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICB1cFRvSW5kZXg6IGxpc3RzQ2xvbmVba2V5XS5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWRDb3VudDogdGhpcy5fc3RhdGUubGlzdHNba2V5XS5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGlzdHNDbG9uZVtrZXldLnNwbGljZSgwLCAwLCB7cm9vbSwgY2F0ZWdvcnl9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGluc2VydGVkSW50b1RhZ3MucHVzaChrZXkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBEb3VibGUgY2hlY2sgdGhhdCB3ZSBpbnNlcnRlZCB0aGUgcm9vbSBpbiB0aGUgcmlnaHQgcGxhY2VzLlxyXG4gICAgICAgIC8vIFRoZXJlIHNob3VsZCBuZXZlciBiZSBhIGRpc2NyZXBhbmN5LlxyXG4gICAgICAgIGZvciAoY29uc3QgdGFyZ2V0VGFnIG9mIHRhcmdldFRhZ3MpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBpbnNlcnRlZFRhZyBvZiBpbnNlcnRlZEludG9UYWdzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaW5zZXJ0ZWRUYWcgPT09IHRhcmdldFRhZykgY291bnQrKztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGNvdW50ICE9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oYCEhIFJvb20gJHtyb29tLnJvb21JZH0gaW5zZXJ0ZWQgJHtjb3VudH0gdGltZXMgdG8gJHt0YXJnZXRUYWd9YCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIFRoaXMgaXMgYSB3b3JrYXJvdW5kIGZvciBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8xMTMwM1xyXG4gICAgICAgICAgICAvLyBUaGUgbG9nZ2luZyBpcyB0byB0cnkgYW5kIGlkZW50aWZ5IHdoYXQgaGFwcGVuZWQgZXhhY3RseS5cclxuICAgICAgICAgICAgaWYgKGNvdW50ID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBTb21ldGhpbmcgd2VudCB2ZXJ5IGJhZGx5IHdyb25nIC0gdHJ5IHRvIHJlY292ZXIgdGhlIHJvb20uXHJcbiAgICAgICAgICAgICAgICAvLyBXZSBkb24ndCBib3RoZXIgY2hlY2tpbmcgaG93IHRoZSB0YXJnZXQgbGlzdCBpcyBvcmRlcmVkIC0gd2UncmUgZXhwZWN0aW5nXHJcbiAgICAgICAgICAgICAgICAvLyB0byBqdXN0IGluc2VydCBpdC5cclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgISEgUmVjb3ZlcmluZyAke3Jvb20ucm9vbUlkfSBmb3IgdGFnICR7dGFyZ2V0VGFnfSBhdCBwb3NpdGlvbiAwYCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWxpc3RzQ2xvbmVbdGFyZ2V0VGFnXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgISEgTGlzdCBmb3IgdGFnICR7dGFyZ2V0VGFnfSBkb2VzIG5vdCBleGlzdCAtIGNyZWF0aW5nYCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGlzdHNDbG9uZVt0YXJnZXRUYWddID0gW107XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsaXN0c0Nsb25lW3RhcmdldFRhZ10uc3BsaWNlKDAsIDAsIHtyb29tLCBjYXRlZ29yeX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTb3J0IHRoZSBmYXZvdXJpdGVzIGJlZm9yZSB3ZSBzZXQgdGhlIGNsb25lXHJcbiAgICAgICAgZm9yIChjb25zdCB0YWcgb2YgT2JqZWN0LmtleXMobGlzdHNDbG9uZSkpIHtcclxuICAgICAgICAgICAgaWYgKGdldExpc3RBbGdvcml0aG0odGFnLCB0aGlzLl9zdGF0ZS5hbGdvcml0aG0pICE9PSBBTEdPX01BTlVBTCkgY29udGludWU7IC8vIHNraXAgcmVjZW50cyAocHJlLXNvcnRlZClcclxuICAgICAgICAgICAgbGlzdHNDbG9uZVt0YWddLnNvcnQodGhpcy5fZ2V0TWFudWFsQ29tcGFyYXRvcih0YWcpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3NldFN0YXRlKHtsaXN0czogbGlzdHNDbG9uZX0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9nZW5lcmF0ZUluaXRpYWxSb29tTGlzdHMoKSB7XHJcbiAgICAgICAgLy8gTG9nIHNvbWV0aGluZyB0byBzaG93IHRoYXQgd2UncmUgdGhyb3dpbmcgYXdheSB0aGUgb2xkIHJlc3VsdHMuIFRoaXMgaXMgZm9yIHRoZSBpbmV2aXRhYmxlXHJcbiAgICAgICAgLy8gcXVlc3Rpb24gb2YgXCJ3aHkgaXMgMTAwJSBvZiBteSBDUFUgZ29pbmcgdG93YXJkcyBSaW90P1wiIC0gYSBxdWljayBsb29rIGF0IHRoZSBsb2dzIHdvdWxkIHJldmVhbFxyXG4gICAgICAgIC8vIHRoYXQgc29tZXRoaW5nIGlzIHdyb25nIHdpdGggdGhlIFJvb21MaXN0U3RvcmUuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJHZW5lcmF0aW5nIGluaXRpYWwgcm9vbSBsaXN0c1wiKTtcclxuXHJcbiAgICAgICAgY29uc3QgbGlzdHMgPSB7XHJcbiAgICAgICAgICAgIFwibS5zZXJ2ZXJfbm90aWNlXCI6IFtdLFxyXG4gICAgICAgICAgICBcImltLnZlY3Rvci5mYWtlLmludml0ZVwiOiBbXSxcclxuICAgICAgICAgICAgXCJtLmZhdm91cml0ZVwiOiBbXSxcclxuICAgICAgICAgICAgXCJpbS52ZWN0b3IuZmFrZS5yZWNlbnRcIjogW10sXHJcbiAgICAgICAgICAgIFtUQUdfRE1dOiBbXSxcclxuICAgICAgICAgICAgXCJtLmxvd3ByaW9yaXR5XCI6IFtdLFxyXG4gICAgICAgICAgICBcImltLnZlY3Rvci5mYWtlLmFyY2hpdmVkXCI6IFtdLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGRtUm9vbU1hcCA9IERNUm9vbU1hcC5zaGFyZWQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5fbWF0cml4Q2xpZW50LmdldFJvb21zKCkuZm9yRWFjaCgocm9vbSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBteVVzZXJJZCA9IHRoaXMuX21hdHJpeENsaWVudC5nZXRVc2VySWQoKTtcclxuICAgICAgICAgICAgY29uc3QgbWVtYmVyc2hpcCA9IHJvb20uZ2V0TXlNZW1iZXJzaGlwKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lID0gcm9vbS5nZXRNZW1iZXIobXlVc2VySWQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKG1lbWJlcnNoaXAgPT09IFwiaW52aXRlXCIpIHtcclxuICAgICAgICAgICAgICAgIGxpc3RzW1wiaW0udmVjdG9yLmZha2UuaW52aXRlXCJdLnB1c2goe3Jvb20sIGNhdGVnb3J5OiBDQVRFR09SWV9SRUR9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChtZW1iZXJzaGlwID09PSBcImpvaW5cIiB8fCBtZW1iZXJzaGlwID09PSBcImJhblwiIHx8IChtZSAmJiBtZS5pc0tpY2tlZCgpKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gVXNlZCB0byBzcGxpdCByb29tcyB2aWEgdGFnc1xyXG4gICAgICAgICAgICAgICAgbGV0IHRhZ05hbWVzID0gT2JqZWN0LmtleXMocm9vbS50YWdzKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBpZ25vcmUgYW55IG0uIHRhZyBuYW1lcyB3ZSBkb24ndCBrbm93IGFib3V0XHJcbiAgICAgICAgICAgICAgICB0YWdOYW1lcyA9IHRhZ05hbWVzLmZpbHRlcigodCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFNwZWVkIG9wdGltaXphdGlvbjogQXZvaWQgaGl0dGluZyB0aGUgU2V0dGluZ3NTdG9yZSBhdCBhbGwgY29zdHMgYnkgbWFraW5nIGl0IHRoZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGxhc3QgY29uZGl0aW9uIHBvc3NpYmxlLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBsaXN0c1t0XSAhPT0gdW5kZWZpbmVkIHx8ICghdC5zdGFydHNXaXRoKCdtLicpICYmIHRoaXMuX3N0YXRlLnRhZ3NFbmFibGVkKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0YWdOYW1lcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRhZ05hbWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhZ05hbWUgPSB0YWdOYW1lc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGlzdHNbdGFnTmFtZV0gPSBsaXN0c1t0YWdOYW1lXSB8fCBbXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIERlZmF1bHQgdG8gYW4gYXJiaXRyYXJ5IGNhdGVnb3J5IGZvciB0YWdzIHdoaWNoIGFyZW4ndCBvcmRlcmVkIGJ5IHJlY2VudHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNhdGVnb3J5ID0gQ0FURUdPUllfSURMRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGdldExpc3RBbGdvcml0aG0odGFnTmFtZSwgdGhpcy5fc3RhdGUuYWxnb3JpdGhtKSAhPT0gQUxHT19NQU5VQUwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5ID0gdGhpcy5fY2FsY3VsYXRlQ2F0ZWdvcnkocm9vbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGlzdHNbdGFnTmFtZV0ucHVzaCh7cm9vbSwgY2F0ZWdvcnl9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRtUm9vbU1hcC5nZXRVc2VySWRGb3JSb29tSWQocm9vbS5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gXCJEaXJlY3QgTWVzc2FnZVwiIHJvb21zICh0aGF0IHdlJ3JlIHN0aWxsIGluIGFuZCB0aGF0IGFyZW4ndCBvdGhlcndpc2UgdGFnZ2VkKVxyXG4gICAgICAgICAgICAgICAgICAgIGxpc3RzW1RBR19ETV0ucHVzaCh7cm9vbSwgY2F0ZWdvcnk6IHRoaXMuX2NhbGN1bGF0ZUNhdGVnb3J5KHJvb20pfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGxpc3RzW1wiaW0udmVjdG9yLmZha2UucmVjZW50XCJdLnB1c2goe3Jvb20sIGNhdGVnb3J5OiB0aGlzLl9jYWxjdWxhdGVDYXRlZ29yeShyb29tKX0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKG1lbWJlcnNoaXAgPT09IFwibGVhdmVcIikge1xyXG4gICAgICAgICAgICAgICAgLy8gVGhlIGNhdGVnb3J5IG9mIHRoZXNlIHJvb21zIGlzIG5vdCBzdXBlciBpbXBvcnRhbnQsIHNvIGRlcHJpb3JpdGl6ZSBpdCB0byB0aGUgbG93ZXN0XHJcbiAgICAgICAgICAgICAgICAvLyBwb3NzaWJsZSB2YWx1ZS5cclxuICAgICAgICAgICAgICAgIGxpc3RzW1wiaW0udmVjdG9yLmZha2UuYXJjaGl2ZWRcIl0ucHVzaCh7cm9vbSwgY2F0ZWdvcnk6IENBVEVHT1JZX0lETEV9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBXZSB1c2UgdGhpcyBjYWNoZSBpbiB0aGUgcmVjZW50cyBjb21wYXJhdG9yIGJlY2F1c2UgX3RzT2ZOZXdlc3RFdmVudCBjYW4gdGFrZSBhIHdoaWxlLiBUaGlzXHJcbiAgICAgICAgLy8gY2FjaGUgb25seSBuZWVkcyB0byBzdXJ2aXZlIHRoZSBzb3J0IG9wZXJhdGlvbiBiZWxvdyBhbmQgc2hvdWxkIG5vdCBiZSBpbXBsZW1lbnRlZCBvdXRzaWRlXHJcbiAgICAgICAgLy8gb2YgdGhpcyBmdW5jdGlvbiwgb3RoZXJ3aXNlIHRoZSByb29tIGxpc3RzIHdpbGwgYWxtb3N0IGNlcnRhaW5seSBiZSBvdXQgb2YgZGF0ZSBhbmQgd3JvbmcuXHJcbiAgICAgICAgY29uc3QgbGF0ZXN0RXZlbnRUc0NhY2hlID0ge307IC8vIHJvb21JZCA9PiB0aW1lc3RhbXBcclxuICAgICAgICBjb25zdCB0c09mTmV3ZXN0RXZlbnRGbiA9IChyb29tKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghcm9vbSkgcmV0dXJuIE51bWJlci5NQVhfU0FGRV9JTlRFR0VSOyAvLyBTaG91bGQgb25seSBoYXBwZW4gaW4gdGVzdHNcclxuXHJcbiAgICAgICAgICAgIGlmIChsYXRlc3RFdmVudFRzQ2FjaGVbcm9vbS5yb29tSWRdKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGF0ZXN0RXZlbnRUc0NhY2hlW3Jvb20ucm9vbUlkXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgdHMgPSB0aGlzLl90c09mTmV3ZXN0RXZlbnQocm9vbSk7XHJcbiAgICAgICAgICAgIGxhdGVzdEV2ZW50VHNDYWNoZVtyb29tLnJvb21JZF0gPSB0cztcclxuICAgICAgICAgICAgcmV0dXJuIHRzO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIE9iamVjdC5rZXlzKGxpc3RzKS5mb3JFYWNoKChsaXN0S2V5KSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBjb21wYXJhdG9yO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGdldExpc3RBbGdvcml0aG0obGlzdEtleSwgdGhpcy5fc3RhdGUuYWxnb3JpdGhtKSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBBTEdPX1JFQ0VOVDpcclxuICAgICAgICAgICAgICAgICAgICBjb21wYXJhdG9yID0gKGVudHJ5QSwgZW50cnlCKSA9PiB0aGlzLl9yZWNlbnRzQ29tcGFyYXRvcihlbnRyeUEsIGVudHJ5QiwgdHNPZk5ld2VzdEV2ZW50Rm4pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBBTEdPX0FMUEhBQkVUSUM6XHJcbiAgICAgICAgICAgICAgICAgICAgY29tcGFyYXRvciA9IHRoaXMuX2xleGljb2dyYXBoaWNhbENvbXBhcmF0b3I7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEFMR09fTUFOVUFMOlxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBjb21wYXJhdG9yID0gdGhpcy5fZ2V0TWFudWFsQ29tcGFyYXRvcihsaXN0S2V5KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX3N0YXRlLm9yZGVySW1wb3J0YW50Rmlyc3QpIHtcclxuICAgICAgICAgICAgICAgIGxpc3RzW2xpc3RLZXldLnNvcnQoKGVudHJ5QSwgZW50cnlCKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVudHJ5QS5jYXRlZ29yeSAhPT0gZW50cnlCLmNhdGVnb3J5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGlkeEEgPSBDQVRFR09SWV9PUkRFUi5pbmRleE9mKGVudHJ5QS5jYXRlZ29yeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGlkeEIgPSBDQVRFR09SWV9PUkRFUi5pbmRleE9mKGVudHJ5Qi5jYXRlZ29yeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpZHhBID4gaWR4QikgcmV0dXJuIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpZHhBIDwgaWR4QikgcmV0dXJuIC0xO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gMDsgLy8gVGVjaG5pY2FsbHkgbm90IHBvc3NpYmxlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjb21wYXJhdG9yKGVudHJ5QSwgZW50cnlCKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gc2tpcCB0aGUgY2F0ZWdvcnkgY29tcGFyaXNvbiBldmVuIHRob3VnaCBpdCBzaG91bGQgbm8tb3Agd2hlbiBvcmRlckltcG9ydGFudEZpcnN0IGRpc2FibGVkXHJcbiAgICAgICAgICAgICAgICBsaXN0c1tsaXN0S2V5XS5zb3J0KGNvbXBhcmF0b3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3NldFN0YXRlKHtcclxuICAgICAgICAgICAgbGlzdHMsXHJcbiAgICAgICAgICAgIHJlYWR5OiB0cnVlLCAvLyBSZWFkeSB0byByZWNlaXZlIHVwZGF0ZXMgdG8gb3JkZXJpbmdcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfZXZlbnRUcmlnZ2Vyc1JlY2VudFJlb3JkZXIoZXYpIHtcclxuICAgICAgICByZXR1cm4gZXYuZ2V0VHMoKSAmJiAoXHJcbiAgICAgICAgICAgIFVucmVhZC5ldmVudFRyaWdnZXJzVW5yZWFkQ291bnQoZXYpIHx8XHJcbiAgICAgICAgICAgIGV2LmdldFNlbmRlcigpID09PSB0aGlzLl9tYXRyaXhDbGllbnQuY3JlZGVudGlhbHMudXNlcklkXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBfdHNPZk5ld2VzdEV2ZW50KHJvb20pIHtcclxuICAgICAgICAvLyBBcHBhcmVudGx5IHdlIGNhbiBoYXZlIHJvb21zIHdpdGhvdXQgdGltZWxpbmVzLCBhdCBsZWFzdCB1bmRlciB0ZXN0aW5nXHJcbiAgICAgICAgLy8gZW52aXJvbm1lbnRzLiBKdXN0IHJldHVybiBNQVhfSU5UIHdoZW4gdGhpcyBoYXBwZW5zLlxyXG4gICAgICAgIGlmICghcm9vbSB8fCAhcm9vbS50aW1lbGluZSkgcmV0dXJuIE51bWJlci5NQVhfU0FGRV9JTlRFR0VSO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gcm9vbS50aW1lbGluZS5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xyXG4gICAgICAgICAgICBjb25zdCBldiA9IHJvb20udGltZWxpbmVbaV07XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9ldmVudFRyaWdnZXJzUmVjZW50UmVvcmRlcihldikpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBldi5nZXRUcygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB3ZSBtaWdodCBvbmx5IGhhdmUgZXZlbnRzIHRoYXQgZG9uJ3QgdHJpZ2dlciB0aGUgdW5yZWFkIGluZGljYXRvcixcclxuICAgICAgICAvLyBpbiB3aGljaCBjYXNlIHVzZSB0aGUgb2xkZXN0IGV2ZW50IGV2ZW4gaWYgbm9ybWFsbHkgaXQgd291bGRuJ3QgY291bnQuXHJcbiAgICAgICAgLy8gVGhpcyBpcyBiZXR0ZXIgdGhhbiBqdXN0IGFzc3VtaW5nIHRoZSBsYXN0IGV2ZW50IHdhcyBmb3JldmVyIGFnby5cclxuICAgICAgICBpZiAocm9vbS50aW1lbGluZS5sZW5ndGggJiYgcm9vbS50aW1lbGluZVswXS5nZXRUcygpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiByb29tLnRpbWVsaW5lWzBdLmdldFRzKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlci5NQVhfU0FGRV9JTlRFR0VSO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfY2FsY3VsYXRlQ2F0ZWdvcnkocm9vbSkge1xyXG4gICAgICAgIGlmICghdGhpcy5fc3RhdGUub3JkZXJJbXBvcnRhbnRGaXJzdCkge1xyXG4gICAgICAgICAgICAvLyBFZmZlY3RpdmVseSBkaXNhYmxlIHRoZSBjYXRlZ29yaXphdGlvbiBvZiByb29tcyBpZiB3ZSdyZSBzdXBwb3NlZCB0b1xyXG4gICAgICAgICAgICAvLyBiZSBzb3J0aW5nIGJ5IG1vcmUgcmVjZW50IG1lc3NhZ2VzIGZpcnN0LiBUaGlzIHRyaWdnZXJzIHRoZSB0aW1lc3RhbXBcclxuICAgICAgICAgICAgLy8gY29tcGFyaXNvbiBiaXQgb2YgX3NldFJvb21DYXRlZ29yeSBhbmQgX3JlY2VudHNDb21wYXJhdG9yIGluc3RlYWQgb2ZcclxuICAgICAgICAgICAgLy8gdGhlIGNhdGVnb3J5IG9yZGVyaW5nLlxyXG4gICAgICAgICAgICByZXR1cm4gQ0FURUdPUllfSURMRTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG1lbnRpb25zID0gcm9vbS5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudChcImhpZ2hsaWdodFwiKSA+IDA7XHJcbiAgICAgICAgaWYgKG1lbnRpb25zKSByZXR1cm4gQ0FURUdPUllfUkVEO1xyXG5cclxuICAgICAgICBsZXQgdW5yZWFkID0gcm9vbS5nZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCgpID4gMDtcclxuICAgICAgICBpZiAodW5yZWFkKSByZXR1cm4gQ0FURUdPUllfR1JFWTtcclxuXHJcbiAgICAgICAgdW5yZWFkID0gVW5yZWFkLmRvZXNSb29tSGF2ZVVucmVhZE1lc3NhZ2VzKHJvb20pO1xyXG4gICAgICAgIGlmICh1bnJlYWQpIHJldHVybiBDQVRFR09SWV9CT0xEO1xyXG5cclxuICAgICAgICByZXR1cm4gQ0FURUdPUllfSURMRTtcclxuICAgIH1cclxuXHJcbiAgICBfcmVjZW50c0NvbXBhcmF0b3IoZW50cnlBLCBlbnRyeUIsIHRzT2ZOZXdlc3RFdmVudEZuKSB7XHJcbiAgICAgICAgY29uc3QgdGltZXN0YW1wQSA9IHRzT2ZOZXdlc3RFdmVudEZuKGVudHJ5QS5yb29tKTtcclxuICAgICAgICBjb25zdCB0aW1lc3RhbXBCID0gdHNPZk5ld2VzdEV2ZW50Rm4oZW50cnlCLnJvb20pO1xyXG4gICAgICAgIHJldHVybiB0aW1lc3RhbXBCIC0gdGltZXN0YW1wQTtcclxuICAgIH1cclxuXHJcbiAgICBfbGV4aWNvZ3JhcGhpY2FsQ29tcGFyYXRvcihlbnRyeUEsIGVudHJ5Qikge1xyXG4gICAgICAgIHJldHVybiBlbnRyeUEucm9vbS5uYW1lLmxvY2FsZUNvbXBhcmUoZW50cnlCLnJvb20ubmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2dldE1hbnVhbENvbXBhcmF0b3IodGFnTmFtZSwgb3B0aW1pc3RpY1JlcXVlc3QpIHtcclxuICAgICAgICByZXR1cm4gKGVudHJ5QSwgZW50cnlCKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21BID0gZW50cnlBLnJvb207XHJcbiAgICAgICAgICAgIGNvbnN0IHJvb21CID0gZW50cnlCLnJvb207XHJcblxyXG4gICAgICAgICAgICBsZXQgbWV0YUEgPSByb29tQS50YWdzW3RhZ05hbWVdO1xyXG4gICAgICAgICAgICBsZXQgbWV0YUIgPSByb29tQi50YWdzW3RhZ05hbWVdO1xyXG5cclxuICAgICAgICAgICAgaWYgKG9wdGltaXN0aWNSZXF1ZXN0ICYmIHJvb21BID09PSBvcHRpbWlzdGljUmVxdWVzdC5yb29tKSBtZXRhQSA9IG9wdGltaXN0aWNSZXF1ZXN0Lm1ldGFEYXRhO1xyXG4gICAgICAgICAgICBpZiAob3B0aW1pc3RpY1JlcXVlc3QgJiYgcm9vbUIgPT09IG9wdGltaXN0aWNSZXF1ZXN0LnJvb20pIG1ldGFCID0gb3B0aW1pc3RpY1JlcXVlc3QubWV0YURhdGE7XHJcblxyXG4gICAgICAgICAgICAvLyBNYWtlIHN1cmUgdGhlIHJvb20gdGFnIGhhcyBhbiBvcmRlciBlbGVtZW50LCBpZiBub3Qgc2V0IGl0IHRvIGJlIHRoZSBib3R0b21cclxuICAgICAgICAgICAgY29uc3QgYSA9IG1ldGFBID8gTnVtYmVyKG1ldGFBLm9yZGVyKSA6IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgY29uc3QgYiA9IG1ldGFCID8gTnVtYmVyKG1ldGFCLm9yZGVyKSA6IHVuZGVmaW5lZDtcclxuXHJcbiAgICAgICAgICAgIC8vIE9yZGVyIHVuZGVmaW5lZCByb29tIHRhZyBvcmRlcnMgdG8gdGhlIGJvdHRvbVxyXG4gICAgICAgICAgICBpZiAoYSA9PT0gdW5kZWZpbmVkICYmIGIgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYSAhPT0gdW5kZWZpbmVkICYmIGIgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIC0xO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYSA9PT0gYiA/IHRoaXMuX2xleGljb2dyYXBoaWNhbENvbXBhcmF0b3IoZW50cnlBLCBlbnRyeUIpIDogKGEgPiBiID8gMSA6IC0xKTtcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJvb21MaXN0cygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUucHJlc2VudGF0aW9uTGlzdHM7XHJcbiAgICB9XHJcbn1cclxuXHJcbmlmIChnbG9iYWwuc2luZ2xldG9uUm9vbUxpc3RTdG9yZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICBnbG9iYWwuc2luZ2xldG9uUm9vbUxpc3RTdG9yZSA9IG5ldyBSb29tTGlzdFN0b3JlKCk7XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgZ2xvYmFsLnNpbmdsZXRvblJvb21MaXN0U3RvcmU7XHJcbiJdfQ==