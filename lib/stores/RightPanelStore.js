"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var _verification = require("../verification");

var _utils = require("flux/utils");

var _SettingsStore = _interopRequireWildcard(require("../settings/SettingsStore"));

var _RightPanelStorePhases = require("./RightPanelStorePhases");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

const INITIAL_STATE = {
  // Whether or not to show the right panel at all. We split out rooms and groups
  // because they're different flows for the user to follow.
  showRoomPanel: _SettingsStore.default.getValue("showRightPanelInRoom"),
  showGroupPanel: _SettingsStore.default.getValue("showRightPanelInGroup"),
  // The last phase (screen) the right panel was showing
  lastRoomPhase: _SettingsStore.default.getValue("lastRightPanelPhaseForRoom"),
  lastGroupPhase: _SettingsStore.default.getValue("lastRightPanelPhaseForGroup"),
  // Extra information about the last phase
  lastRoomPhaseParams: {}
};
const GROUP_PHASES = Object.keys(_RightPanelStorePhases.RIGHT_PANEL_PHASES).filter(k => k.startsWith("Group"));
const MEMBER_INFO_PHASES = [_RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo, _RightPanelStorePhases.RIGHT_PANEL_PHASES.Room3pidMemberInfo, _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel];
/**
 * A class for tracking the state of the right panel between layouts and
 * sessions.
 */

class RightPanelStore extends _utils.Store {
  constructor() {
    super(_dispatcher.default); // Initialise state

    this._state = INITIAL_STATE;
  }

  get isOpenForRoom()
  /*: boolean*/
  {
    return this._state.showRoomPanel;
  }

  get isOpenForGroup()
  /*: boolean*/
  {
    return this._state.showGroupPanel;
  }

  get roomPanelPhase()
  /*: string*/
  {
    return this._state.lastRoomPhase;
  }

  get groupPanelPhase()
  /*: string*/
  {
    return this._state.lastGroupPhase;
  }

  get visibleRoomPanelPhase()
  /*: string*/
  {
    return this.isOpenForRoom ? this.roomPanelPhase : null;
  }

  get visibleGroupPanelPhase()
  /*: string*/
  {
    return this.isOpenForGroup ? this.groupPanelPhase : null;
  }

  get roomPanelPhaseParams()
  /*: any*/
  {
    return this._state.lastRoomPhaseParams || {};
  }

  _setState(newState) {
    this._state = Object.assign(this._state, newState);

    _SettingsStore.default.setValue("showRightPanelInRoom", null, _SettingsStore.SettingLevel.DEVICE, this._state.showRoomPanel);

    _SettingsStore.default.setValue("showRightPanelInGroup", null, _SettingsStore.SettingLevel.DEVICE, this._state.showGroupPanel);

    if (_RightPanelStorePhases.RIGHT_PANEL_PHASES_NO_ARGS.includes(this._state.lastRoomPhase)) {
      _SettingsStore.default.setValue("lastRightPanelPhaseForRoom", null, _SettingsStore.SettingLevel.DEVICE, this._state.lastRoomPhase);
    }

    if (_RightPanelStorePhases.RIGHT_PANEL_PHASES_NO_ARGS.includes(this._state.lastGroupPhase)) {
      _SettingsStore.default.setValue("lastRightPanelPhaseForGroup", null, _SettingsStore.SettingLevel.DEVICE, this._state.lastGroupPhase);
    }

    this.__emitChange();
  }

  __onDispatch(payload) {
    switch (payload.action) {
      case 'view_room':
      case 'view_group':
        // Reset to the member list if we're viewing member info
        if (MEMBER_INFO_PHASES.includes(this._state.lastRoomPhase)) {
          this._setState({
            lastRoomPhase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberList,
            lastRoomPhaseParams: {}
          });
        } // Do the same for groups


        if (this._state.lastGroupPhase === _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberInfo) {
          this._setState({
            lastGroupPhase: _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList
          });
        }

        break;

      case 'set_right_panel_phase':
        {
          let targetPhase = payload.phase;
          let refireParams = payload.refireParams; // redirect to EncryptionPanel if there is an ongoing verification request

          if (targetPhase === _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo) {
            const {
              member
            } = payload.refireParams;
            const pendingRequest = (0, _verification.pendingVerificationRequestForUser)(member);

            if (pendingRequest) {
              targetPhase = _RightPanelStorePhases.RIGHT_PANEL_PHASES.EncryptionPanel;
              refireParams = {
                verificationRequest: pendingRequest,
                member
              };
            }
          }

          if (!_RightPanelStorePhases.RIGHT_PANEL_PHASES[targetPhase]) {
            console.warn("Tried to switch right panel to unknown phase: ".concat(targetPhase));
            return;
          }

          if (GROUP_PHASES.includes(targetPhase)) {
            if (targetPhase === this._state.lastGroupPhase) {
              this._setState({
                showGroupPanel: !this._state.showGroupPanel
              });
            } else {
              this._setState({
                lastGroupPhase: targetPhase,
                showGroupPanel: true
              });
            }
          } else {
            if (targetPhase === this._state.lastRoomPhase && !refireParams) {
              this._setState({
                showRoomPanel: !this._state.showRoomPanel
              });
            } else {
              this._setState({
                lastRoomPhase: targetPhase,
                showRoomPanel: true,
                lastRoomPhaseParams: refireParams || {}
              });
            }
          } // Let things like the member info panel actually open to the right member.


          _dispatcher.default.dispatch(_objectSpread({
            action: 'after_right_panel_phase_change',
            phase: targetPhase
          }, refireParams || {}));

          break;
        }

      case 'toggle_right_panel':
        if (payload.type === "room") {
          this._setState({
            showRoomPanel: !this._state.showRoomPanel
          });
        } else {
          // group
          this._setState({
            showGroupPanel: !this._state.showGroupPanel
          });
        }

        break;
    }
  }

  static getSharedInstance()
  /*: RightPanelStore*/
  {
    if (!RightPanelStore._instance) {
      RightPanelStore._instance = new RightPanelStore();
    }

    return RightPanelStore._instance;
  }

}

exports.default = RightPanelStore;
(0, _defineProperty2.default)(RightPanelStore, "_instance", void 0);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zdG9yZXMvUmlnaHRQYW5lbFN0b3JlLmpzIl0sIm5hbWVzIjpbIklOSVRJQUxfU1RBVEUiLCJzaG93Um9vbVBhbmVsIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwic2hvd0dyb3VwUGFuZWwiLCJsYXN0Um9vbVBoYXNlIiwibGFzdEdyb3VwUGhhc2UiLCJsYXN0Um9vbVBoYXNlUGFyYW1zIiwiR1JPVVBfUEhBU0VTIiwiT2JqZWN0Iiwia2V5cyIsIlJJR0hUX1BBTkVMX1BIQVNFUyIsImZpbHRlciIsImsiLCJzdGFydHNXaXRoIiwiTUVNQkVSX0lORk9fUEhBU0VTIiwiUm9vbU1lbWJlckluZm8iLCJSb29tM3BpZE1lbWJlckluZm8iLCJFbmNyeXB0aW9uUGFuZWwiLCJSaWdodFBhbmVsU3RvcmUiLCJTdG9yZSIsImNvbnN0cnVjdG9yIiwiZGlzIiwiX3N0YXRlIiwiaXNPcGVuRm9yUm9vbSIsImlzT3BlbkZvckdyb3VwIiwicm9vbVBhbmVsUGhhc2UiLCJncm91cFBhbmVsUGhhc2UiLCJ2aXNpYmxlUm9vbVBhbmVsUGhhc2UiLCJ2aXNpYmxlR3JvdXBQYW5lbFBoYXNlIiwicm9vbVBhbmVsUGhhc2VQYXJhbXMiLCJfc2V0U3RhdGUiLCJuZXdTdGF0ZSIsImFzc2lnbiIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIiwiUklHSFRfUEFORUxfUEhBU0VTX05PX0FSR1MiLCJpbmNsdWRlcyIsIl9fZW1pdENoYW5nZSIsIl9fb25EaXNwYXRjaCIsInBheWxvYWQiLCJhY3Rpb24iLCJSb29tTWVtYmVyTGlzdCIsIkdyb3VwTWVtYmVySW5mbyIsIkdyb3VwTWVtYmVyTGlzdCIsInRhcmdldFBoYXNlIiwicGhhc2UiLCJyZWZpcmVQYXJhbXMiLCJtZW1iZXIiLCJwZW5kaW5nUmVxdWVzdCIsInZlcmlmaWNhdGlvblJlcXVlc3QiLCJjb25zb2xlIiwid2FybiIsImRpc3BhdGNoIiwidHlwZSIsImdldFNoYXJlZEluc3RhbmNlIiwiX2luc3RhbmNlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxNQUFNQSxhQUFhLEdBQUc7QUFDbEI7QUFDQTtBQUNBQyxFQUFBQSxhQUFhLEVBQUVDLHVCQUFjQyxRQUFkLENBQXVCLHNCQUF2QixDQUhHO0FBSWxCQyxFQUFBQSxjQUFjLEVBQUVGLHVCQUFjQyxRQUFkLENBQXVCLHVCQUF2QixDQUpFO0FBTWxCO0FBQ0FFLEVBQUFBLGFBQWEsRUFBRUgsdUJBQWNDLFFBQWQsQ0FBdUIsNEJBQXZCLENBUEc7QUFRbEJHLEVBQUFBLGNBQWMsRUFBRUosdUJBQWNDLFFBQWQsQ0FBdUIsNkJBQXZCLENBUkU7QUFVbEI7QUFDQUksRUFBQUEsbUJBQW1CLEVBQUU7QUFYSCxDQUF0QjtBQWNBLE1BQU1DLFlBQVksR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVlDLHlDQUFaLEVBQWdDQyxNQUFoQyxDQUF1Q0MsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLFVBQUYsQ0FBYSxPQUFiLENBQTVDLENBQXJCO0FBRUEsTUFBTUMsa0JBQWtCLEdBQUcsQ0FDdkJKLDBDQUFtQkssY0FESSxFQUV2QkwsMENBQW1CTSxrQkFGSSxFQUd2Qk4sMENBQW1CTyxlQUhJLENBQTNCO0FBTUE7Ozs7O0FBSWUsTUFBTUMsZUFBTixTQUE4QkMsWUFBOUIsQ0FBb0M7QUFHL0NDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFVBQU1DLG1CQUFOLEVBRFUsQ0FHVjs7QUFDQSxTQUFLQyxNQUFMLEdBQWN2QixhQUFkO0FBQ0g7O0FBRUQsTUFBSXdCLGFBQUo7QUFBQTtBQUE2QjtBQUN6QixXQUFPLEtBQUtELE1BQUwsQ0FBWXRCLGFBQW5CO0FBQ0g7O0FBRUQsTUFBSXdCLGNBQUo7QUFBQTtBQUE4QjtBQUMxQixXQUFPLEtBQUtGLE1BQUwsQ0FBWW5CLGNBQW5CO0FBQ0g7O0FBRUQsTUFBSXNCLGNBQUo7QUFBQTtBQUE2QjtBQUN6QixXQUFPLEtBQUtILE1BQUwsQ0FBWWxCLGFBQW5CO0FBQ0g7O0FBRUQsTUFBSXNCLGVBQUo7QUFBQTtBQUE4QjtBQUMxQixXQUFPLEtBQUtKLE1BQUwsQ0FBWWpCLGNBQW5CO0FBQ0g7O0FBRUQsTUFBSXNCLHFCQUFKO0FBQUE7QUFBb0M7QUFDaEMsV0FBTyxLQUFLSixhQUFMLEdBQXFCLEtBQUtFLGNBQTFCLEdBQTJDLElBQWxEO0FBQ0g7O0FBRUQsTUFBSUcsc0JBQUo7QUFBQTtBQUFxQztBQUNqQyxXQUFPLEtBQUtKLGNBQUwsR0FBc0IsS0FBS0UsZUFBM0IsR0FBNkMsSUFBcEQ7QUFDSDs7QUFFRCxNQUFJRyxvQkFBSjtBQUFBO0FBQWdDO0FBQzVCLFdBQU8sS0FBS1AsTUFBTCxDQUFZaEIsbUJBQVosSUFBbUMsRUFBMUM7QUFDSDs7QUFFRHdCLEVBQUFBLFNBQVMsQ0FBQ0MsUUFBRCxFQUFXO0FBQ2hCLFNBQUtULE1BQUwsR0FBY2QsTUFBTSxDQUFDd0IsTUFBUCxDQUFjLEtBQUtWLE1BQW5CLEVBQTJCUyxRQUEzQixDQUFkOztBQUVBOUIsMkJBQWNnQyxRQUFkLENBQ0ksc0JBREosRUFFSSxJQUZKLEVBR0lDLDRCQUFhQyxNQUhqQixFQUlJLEtBQUtiLE1BQUwsQ0FBWXRCLGFBSmhCOztBQU1BQywyQkFBY2dDLFFBQWQsQ0FDSSx1QkFESixFQUVJLElBRkosRUFHSUMsNEJBQWFDLE1BSGpCLEVBSUksS0FBS2IsTUFBTCxDQUFZbkIsY0FKaEI7O0FBT0EsUUFBSWlDLGtEQUEyQkMsUUFBM0IsQ0FBb0MsS0FBS2YsTUFBTCxDQUFZbEIsYUFBaEQsQ0FBSixFQUFvRTtBQUNoRUgsNkJBQWNnQyxRQUFkLENBQ0ksNEJBREosRUFFSSxJQUZKLEVBR0lDLDRCQUFhQyxNQUhqQixFQUlJLEtBQUtiLE1BQUwsQ0FBWWxCLGFBSmhCO0FBTUg7O0FBQ0QsUUFBSWdDLGtEQUEyQkMsUUFBM0IsQ0FBb0MsS0FBS2YsTUFBTCxDQUFZakIsY0FBaEQsQ0FBSixFQUFxRTtBQUNqRUosNkJBQWNnQyxRQUFkLENBQ0ksNkJBREosRUFFSSxJQUZKLEVBR0lDLDRCQUFhQyxNQUhqQixFQUlJLEtBQUtiLE1BQUwsQ0FBWWpCLGNBSmhCO0FBTUg7O0FBRUQsU0FBS2lDLFlBQUw7QUFDSDs7QUFFREMsRUFBQUEsWUFBWSxDQUFDQyxPQUFELEVBQVU7QUFDbEIsWUFBUUEsT0FBTyxDQUFDQyxNQUFoQjtBQUNJLFdBQUssV0FBTDtBQUNBLFdBQUssWUFBTDtBQUNJO0FBQ0EsWUFBSTNCLGtCQUFrQixDQUFDdUIsUUFBbkIsQ0FBNEIsS0FBS2YsTUFBTCxDQUFZbEIsYUFBeEMsQ0FBSixFQUE0RDtBQUN4RCxlQUFLMEIsU0FBTCxDQUFlO0FBQUMxQixZQUFBQSxhQUFhLEVBQUVNLDBDQUFtQmdDLGNBQW5DO0FBQW1EcEMsWUFBQUEsbUJBQW1CLEVBQUU7QUFBeEUsV0FBZjtBQUNILFNBSkwsQ0FNSTs7O0FBQ0EsWUFBSSxLQUFLZ0IsTUFBTCxDQUFZakIsY0FBWixLQUErQkssMENBQW1CaUMsZUFBdEQsRUFBdUU7QUFDbkUsZUFBS2IsU0FBTCxDQUFlO0FBQUN6QixZQUFBQSxjQUFjLEVBQUVLLDBDQUFtQmtDO0FBQXBDLFdBQWY7QUFDSDs7QUFDRDs7QUFFSixXQUFLLHVCQUFMO0FBQThCO0FBQzFCLGNBQUlDLFdBQVcsR0FBR0wsT0FBTyxDQUFDTSxLQUExQjtBQUNBLGNBQUlDLFlBQVksR0FBR1AsT0FBTyxDQUFDTyxZQUEzQixDQUYwQixDQUcxQjs7QUFDQSxjQUFJRixXQUFXLEtBQUtuQywwQ0FBbUJLLGNBQXZDLEVBQXVEO0FBQ25ELGtCQUFNO0FBQUNpQyxjQUFBQTtBQUFELGdCQUFXUixPQUFPLENBQUNPLFlBQXpCO0FBQ0Esa0JBQU1FLGNBQWMsR0FBRyxxREFBa0NELE1BQWxDLENBQXZCOztBQUNBLGdCQUFJQyxjQUFKLEVBQW9CO0FBQ2hCSixjQUFBQSxXQUFXLEdBQUduQywwQ0FBbUJPLGVBQWpDO0FBQ0E4QixjQUFBQSxZQUFZLEdBQUc7QUFDWEcsZ0JBQUFBLG1CQUFtQixFQUFFRCxjQURWO0FBRVhELGdCQUFBQTtBQUZXLGVBQWY7QUFJSDtBQUNKOztBQUNELGNBQUksQ0FBQ3RDLDBDQUFtQm1DLFdBQW5CLENBQUwsRUFBc0M7QUFDbENNLFlBQUFBLE9BQU8sQ0FBQ0MsSUFBUix5REFBOERQLFdBQTlEO0FBQ0E7QUFDSDs7QUFFRCxjQUFJdEMsWUFBWSxDQUFDOEIsUUFBYixDQUFzQlEsV0FBdEIsQ0FBSixFQUF3QztBQUNwQyxnQkFBSUEsV0FBVyxLQUFLLEtBQUt2QixNQUFMLENBQVlqQixjQUFoQyxFQUFnRDtBQUM1QyxtQkFBS3lCLFNBQUwsQ0FBZTtBQUNYM0IsZ0JBQUFBLGNBQWMsRUFBRSxDQUFDLEtBQUttQixNQUFMLENBQVluQjtBQURsQixlQUFmO0FBR0gsYUFKRCxNQUlPO0FBQ0gsbUJBQUsyQixTQUFMLENBQWU7QUFDWHpCLGdCQUFBQSxjQUFjLEVBQUV3QyxXQURMO0FBRVgxQyxnQkFBQUEsY0FBYyxFQUFFO0FBRkwsZUFBZjtBQUlIO0FBQ0osV0FYRCxNQVdPO0FBQ0gsZ0JBQUkwQyxXQUFXLEtBQUssS0FBS3ZCLE1BQUwsQ0FBWWxCLGFBQTVCLElBQTZDLENBQUMyQyxZQUFsRCxFQUFnRTtBQUM1RCxtQkFBS2pCLFNBQUwsQ0FBZTtBQUNYOUIsZ0JBQUFBLGFBQWEsRUFBRSxDQUFDLEtBQUtzQixNQUFMLENBQVl0QjtBQURqQixlQUFmO0FBR0gsYUFKRCxNQUlPO0FBQ0gsbUJBQUs4QixTQUFMLENBQWU7QUFDWDFCLGdCQUFBQSxhQUFhLEVBQUV5QyxXQURKO0FBRVg3QyxnQkFBQUEsYUFBYSxFQUFFLElBRko7QUFHWE0sZ0JBQUFBLG1CQUFtQixFQUFFeUMsWUFBWSxJQUFJO0FBSDFCLGVBQWY7QUFLSDtBQUNKLFdBM0N5QixDQTZDMUI7OztBQUNBMUIsOEJBQUlnQyxRQUFKO0FBQ0laLFlBQUFBLE1BQU0sRUFBRSxnQ0FEWjtBQUVJSyxZQUFBQSxLQUFLLEVBQUVEO0FBRlgsYUFHUUUsWUFBWSxJQUFJLEVBSHhCOztBQUtBO0FBQ0g7O0FBRUQsV0FBSyxvQkFBTDtBQUNJLFlBQUlQLE9BQU8sQ0FBQ2MsSUFBUixLQUFpQixNQUFyQixFQUE2QjtBQUN6QixlQUFLeEIsU0FBTCxDQUFlO0FBQUU5QixZQUFBQSxhQUFhLEVBQUUsQ0FBQyxLQUFLc0IsTUFBTCxDQUFZdEI7QUFBOUIsV0FBZjtBQUNILFNBRkQsTUFFTztBQUFFO0FBQ0wsZUFBSzhCLFNBQUwsQ0FBZTtBQUFFM0IsWUFBQUEsY0FBYyxFQUFFLENBQUMsS0FBS21CLE1BQUwsQ0FBWW5CO0FBQS9CLFdBQWY7QUFDSDs7QUFDRDtBQTFFUjtBQTRFSDs7QUFFRCxTQUFPb0QsaUJBQVA7QUFBQTtBQUE0QztBQUN4QyxRQUFJLENBQUNyQyxlQUFlLENBQUNzQyxTQUFyQixFQUFnQztBQUM1QnRDLE1BQUFBLGVBQWUsQ0FBQ3NDLFNBQWhCLEdBQTRCLElBQUl0QyxlQUFKLEVBQTVCO0FBQ0g7O0FBQ0QsV0FBT0EsZUFBZSxDQUFDc0MsU0FBdkI7QUFDSDs7QUE5SjhDOzs7OEJBQTlCdEMsZSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IGRpcyBmcm9tICcuLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IHtwZW5kaW5nVmVyaWZpY2F0aW9uUmVxdWVzdEZvclVzZXJ9IGZyb20gJy4uL3ZlcmlmaWNhdGlvbic7XHJcbmltcG9ydCB7U3RvcmV9IGZyb20gJ2ZsdXgvdXRpbHMnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSBcIi4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IHtSSUdIVF9QQU5FTF9QSEFTRVMsIFJJR0hUX1BBTkVMX1BIQVNFU19OT19BUkdTfSBmcm9tIFwiLi9SaWdodFBhbmVsU3RvcmVQaGFzZXNcIjtcclxuXHJcbmNvbnN0IElOSVRJQUxfU1RBVEUgPSB7XHJcbiAgICAvLyBXaGV0aGVyIG9yIG5vdCB0byBzaG93IHRoZSByaWdodCBwYW5lbCBhdCBhbGwuIFdlIHNwbGl0IG91dCByb29tcyBhbmQgZ3JvdXBzXHJcbiAgICAvLyBiZWNhdXNlIHRoZXkncmUgZGlmZmVyZW50IGZsb3dzIGZvciB0aGUgdXNlciB0byBmb2xsb3cuXHJcbiAgICBzaG93Um9vbVBhbmVsOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwic2hvd1JpZ2h0UGFuZWxJblJvb21cIiksXHJcbiAgICBzaG93R3JvdXBQYW5lbDogU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcInNob3dSaWdodFBhbmVsSW5Hcm91cFwiKSxcclxuXHJcbiAgICAvLyBUaGUgbGFzdCBwaGFzZSAoc2NyZWVuKSB0aGUgcmlnaHQgcGFuZWwgd2FzIHNob3dpbmdcclxuICAgIGxhc3RSb29tUGhhc2U6IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJsYXN0UmlnaHRQYW5lbFBoYXNlRm9yUm9vbVwiKSxcclxuICAgIGxhc3RHcm91cFBoYXNlOiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwibGFzdFJpZ2h0UGFuZWxQaGFzZUZvckdyb3VwXCIpLFxyXG5cclxuICAgIC8vIEV4dHJhIGluZm9ybWF0aW9uIGFib3V0IHRoZSBsYXN0IHBoYXNlXHJcbiAgICBsYXN0Um9vbVBoYXNlUGFyYW1zOiB7fSxcclxufTtcclxuXHJcbmNvbnN0IEdST1VQX1BIQVNFUyA9IE9iamVjdC5rZXlzKFJJR0hUX1BBTkVMX1BIQVNFUykuZmlsdGVyKGsgPT4gay5zdGFydHNXaXRoKFwiR3JvdXBcIikpO1xyXG5cclxuY29uc3QgTUVNQkVSX0lORk9fUEhBU0VTID0gW1xyXG4gICAgUklHSFRfUEFORUxfUEhBU0VTLlJvb21NZW1iZXJJbmZvLFxyXG4gICAgUklHSFRfUEFORUxfUEhBU0VTLlJvb20zcGlkTWVtYmVySW5mbyxcclxuICAgIFJJR0hUX1BBTkVMX1BIQVNFUy5FbmNyeXB0aW9uUGFuZWwsXHJcbl07XHJcblxyXG4vKipcclxuICogQSBjbGFzcyBmb3IgdHJhY2tpbmcgdGhlIHN0YXRlIG9mIHRoZSByaWdodCBwYW5lbCBiZXR3ZWVuIGxheW91dHMgYW5kXHJcbiAqIHNlc3Npb25zLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmlnaHRQYW5lbFN0b3JlIGV4dGVuZHMgU3RvcmUge1xyXG4gICAgc3RhdGljIF9pbnN0YW5jZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcihkaXMpO1xyXG5cclxuICAgICAgICAvLyBJbml0aWFsaXNlIHN0YXRlXHJcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBJTklUSUFMX1NUQVRFO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc09wZW5Gb3JSb29tKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGF0ZS5zaG93Um9vbVBhbmVsO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc09wZW5Gb3JHcm91cCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGUuc2hvd0dyb3VwUGFuZWw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJvb21QYW5lbFBoYXNlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlLmxhc3RSb29tUGhhc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGdyb3VwUGFuZWxQaGFzZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGF0ZS5sYXN0R3JvdXBQaGFzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdmlzaWJsZVJvb21QYW5lbFBoYXNlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNPcGVuRm9yUm9vbSA/IHRoaXMucm9vbVBhbmVsUGhhc2UgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB2aXNpYmxlR3JvdXBQYW5lbFBoYXNlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNPcGVuRm9yR3JvdXAgPyB0aGlzLmdyb3VwUGFuZWxQaGFzZSA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJvb21QYW5lbFBoYXNlUGFyYW1zKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlLmxhc3RSb29tUGhhc2VQYXJhbXMgfHwge307XHJcbiAgICB9XHJcblxyXG4gICAgX3NldFN0YXRlKG5ld1N0YXRlKSB7XHJcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBPYmplY3QuYXNzaWduKHRoaXMuX3N0YXRlLCBuZXdTdGF0ZSk7XHJcblxyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXHJcbiAgICAgICAgICAgIFwic2hvd1JpZ2h0UGFuZWxJblJvb21cIixcclxuICAgICAgICAgICAgbnVsbCxcclxuICAgICAgICAgICAgU2V0dGluZ0xldmVsLkRFVklDRSxcclxuICAgICAgICAgICAgdGhpcy5fc3RhdGUuc2hvd1Jvb21QYW5lbCxcclxuICAgICAgICApO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXHJcbiAgICAgICAgICAgIFwic2hvd1JpZ2h0UGFuZWxJbkdyb3VwXCIsXHJcbiAgICAgICAgICAgIG51bGwsXHJcbiAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsXHJcbiAgICAgICAgICAgIHRoaXMuX3N0YXRlLnNob3dHcm91cFBhbmVsLFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGlmIChSSUdIVF9QQU5FTF9QSEFTRVNfTk9fQVJHUy5pbmNsdWRlcyh0aGlzLl9zdGF0ZS5sYXN0Um9vbVBoYXNlKSkge1xyXG4gICAgICAgICAgICBTZXR0aW5nc1N0b3JlLnNldFZhbHVlKFxyXG4gICAgICAgICAgICAgICAgXCJsYXN0UmlnaHRQYW5lbFBoYXNlRm9yUm9vbVwiLFxyXG4gICAgICAgICAgICAgICAgbnVsbCxcclxuICAgICAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zdGF0ZS5sYXN0Um9vbVBoYXNlLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoUklHSFRfUEFORUxfUEhBU0VTX05PX0FSR1MuaW5jbHVkZXModGhpcy5fc3RhdGUubGFzdEdyb3VwUGhhc2UpKSB7XHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXHJcbiAgICAgICAgICAgICAgICBcImxhc3RSaWdodFBhbmVsUGhhc2VGb3JHcm91cFwiLFxyXG4gICAgICAgICAgICAgICAgbnVsbCxcclxuICAgICAgICAgICAgICAgIFNldHRpbmdMZXZlbC5ERVZJQ0UsXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zdGF0ZS5sYXN0R3JvdXBQaGFzZSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX19lbWl0Q2hhbmdlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgX19vbkRpc3BhdGNoKHBheWxvYWQpIHtcclxuICAgICAgICBzd2l0Y2ggKHBheWxvYWQuYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfcm9vbSc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3ZpZXdfZ3JvdXAnOlxyXG4gICAgICAgICAgICAgICAgLy8gUmVzZXQgdG8gdGhlIG1lbWJlciBsaXN0IGlmIHdlJ3JlIHZpZXdpbmcgbWVtYmVyIGluZm9cclxuICAgICAgICAgICAgICAgIGlmIChNRU1CRVJfSU5GT19QSEFTRVMuaW5jbHVkZXModGhpcy5fc3RhdGUubGFzdFJvb21QaGFzZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7bGFzdFJvb21QaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLlJvb21NZW1iZXJMaXN0LCBsYXN0Um9vbVBoYXNlUGFyYW1zOiB7fX0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIERvIHRoZSBzYW1lIGZvciBncm91cHNcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9zdGF0ZS5sYXN0R3JvdXBQaGFzZSA9PT0gUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVySW5mbykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3NldFN0YXRlKHtsYXN0R3JvdXBQaGFzZTogUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVyTGlzdH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdzZXRfcmlnaHRfcGFuZWxfcGhhc2UnOiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdGFyZ2V0UGhhc2UgPSBwYXlsb2FkLnBoYXNlO1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlZmlyZVBhcmFtcyA9IHBheWxvYWQucmVmaXJlUGFyYW1zO1xyXG4gICAgICAgICAgICAgICAgLy8gcmVkaXJlY3QgdG8gRW5jcnlwdGlvblBhbmVsIGlmIHRoZXJlIGlzIGFuIG9uZ29pbmcgdmVyaWZpY2F0aW9uIHJlcXVlc3RcclxuICAgICAgICAgICAgICAgIGlmICh0YXJnZXRQaGFzZSA9PT0gUklHSFRfUEFORUxfUEhBU0VTLlJvb21NZW1iZXJJbmZvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge21lbWJlcn0gPSBwYXlsb2FkLnJlZmlyZVBhcmFtcztcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwZW5kaW5nUmVxdWVzdCA9IHBlbmRpbmdWZXJpZmljYXRpb25SZXF1ZXN0Rm9yVXNlcihtZW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwZW5kaW5nUmVxdWVzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRQaGFzZSA9IFJJR0hUX1BBTkVMX1BIQVNFUy5FbmNyeXB0aW9uUGFuZWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZmlyZVBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZlcmlmaWNhdGlvblJlcXVlc3Q6IHBlbmRpbmdSZXF1ZXN0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghUklHSFRfUEFORUxfUEhBU0VTW3RhcmdldFBoYXNlXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihgVHJpZWQgdG8gc3dpdGNoIHJpZ2h0IHBhbmVsIHRvIHVua25vd24gcGhhc2U6ICR7dGFyZ2V0UGhhc2V9YCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChHUk9VUF9QSEFTRVMuaW5jbHVkZXModGFyZ2V0UGhhc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhcmdldFBoYXNlID09PSB0aGlzLl9zdGF0ZS5sYXN0R3JvdXBQaGFzZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93R3JvdXBQYW5lbDogIXRoaXMuX3N0YXRlLnNob3dHcm91cFBhbmVsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXN0R3JvdXBQaGFzZTogdGFyZ2V0UGhhc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93R3JvdXBQYW5lbDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0UGhhc2UgPT09IHRoaXMuX3N0YXRlLmxhc3RSb29tUGhhc2UgJiYgIXJlZmlyZVBhcmFtcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93Um9vbVBhbmVsOiAhdGhpcy5fc3RhdGUuc2hvd1Jvb21QYW5lbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFzdFJvb21QaGFzZTogdGFyZ2V0UGhhc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93Um9vbVBhbmVsOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFzdFJvb21QaGFzZVBhcmFtczogcmVmaXJlUGFyYW1zIHx8IHt9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gTGV0IHRoaW5ncyBsaWtlIHRoZSBtZW1iZXIgaW5mbyBwYW5lbCBhY3R1YWxseSBvcGVuIHRvIHRoZSByaWdodCBtZW1iZXIuXHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ2FmdGVyX3JpZ2h0X3BhbmVsX3BoYXNlX2NoYW5nZScsXHJcbiAgICAgICAgICAgICAgICAgICAgcGhhc2U6IHRhcmdldFBoYXNlLFxyXG4gICAgICAgICAgICAgICAgICAgIC4uLihyZWZpcmVQYXJhbXMgfHwge30pLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY2FzZSAndG9nZ2xlX3JpZ2h0X3BhbmVsJzpcclxuICAgICAgICAgICAgICAgIGlmIChwYXlsb2FkLnR5cGUgPT09IFwicm9vbVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoeyBzaG93Um9vbVBhbmVsOiAhdGhpcy5fc3RhdGUuc2hvd1Jvb21QYW5lbCB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7IC8vIGdyb3VwXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2V0U3RhdGUoeyBzaG93R3JvdXBQYW5lbDogIXRoaXMuX3N0YXRlLnNob3dHcm91cFBhbmVsIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXRTaGFyZWRJbnN0YW5jZSgpOiBSaWdodFBhbmVsU3RvcmUge1xyXG4gICAgICAgIGlmICghUmlnaHRQYW5lbFN0b3JlLl9pbnN0YW5jZSkge1xyXG4gICAgICAgICAgICBSaWdodFBhbmVsU3RvcmUuX2luc3RhbmNlID0gbmV3IFJpZ2h0UGFuZWxTdG9yZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gUmlnaHRQYW5lbFN0b3JlLl9pbnN0YW5jZTtcclxuICAgIH1cclxufVxyXG4iXX0=