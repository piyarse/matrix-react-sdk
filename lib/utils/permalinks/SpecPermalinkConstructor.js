"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.baseUrl = exports.host = void 0;

var _PermalinkConstructor = _interopRequireWildcard(require("./PermalinkConstructor"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const host = "matrix.to";
exports.host = host;
const baseUrl = "https://".concat(host);
/**
 * Generates matrix.to permalinks
 */

exports.baseUrl = baseUrl;

class SpecPermalinkConstructor extends _PermalinkConstructor.default {
  constructor() {
    super();
  }

  forEvent(roomId
  /*: string*/
  , eventId
  /*: string*/
  , serverCandidates
  /*: string[]*/
  )
  /*: string*/
  {
    return "".concat(baseUrl, "/#/").concat(roomId, "/").concat(eventId).concat(this.encodeServerCandidates(serverCandidates));
  }

  forRoom(roomIdOrAlias
  /*: string*/
  , serverCandidates
  /*: string[]*/
  )
  /*: string*/
  {
    return "".concat(baseUrl, "/#/").concat(roomIdOrAlias).concat(this.encodeServerCandidates(serverCandidates));
  }

  forUser(userId
  /*: string*/
  )
  /*: string*/
  {
    return "".concat(baseUrl, "/#/").concat(userId);
  }

  forGroup(groupId
  /*: string*/
  )
  /*: string*/
  {
    return "".concat(baseUrl, "/#/").concat(groupId);
  }

  forEntity(entityId
  /*: string*/
  )
  /*: string*/
  {
    return "".concat(baseUrl, "/#/").concat(entityId);
  }

  isPermalinkHost(testHost
  /*: string*/
  )
  /*: boolean*/
  {
    return testHost === host;
  }

  encodeServerCandidates(candidates
  /*: string[]*/
  ) {
    if (!candidates || candidates.length === 0) return '';
    return "?via=".concat(candidates.map(c => encodeURIComponent(c)).join("&via="));
  } // Heavily inspired by/borrowed from the matrix-bot-sdk (with permission):
  // https://github.com/turt2live/matrix-js-bot-sdk/blob/7c4665c9a25c2c8e0fe4e509f2616505b5b66a1c/src/Permalinks.ts#L33-L61


  parsePermalink(fullUrl
  /*: string*/
  )
  /*: PermalinkParts*/
  {
    if (!fullUrl || !fullUrl.startsWith(baseUrl)) {
      throw new Error("Does not appear to be a permalink");
    }

    const parts = fullUrl.substring("".concat(baseUrl, "/#/").length).split("/");
    const entity = parts[0];

    if (entity[0] === '@') {
      // Probably a user, no further parsing needed.
      return _PermalinkConstructor.PermalinkParts.forUser(entity);
    } else if (entity[0] === '+') {
      // Probably a group, no further parsing needed.
      return _PermalinkConstructor.PermalinkParts.forGroup(entity);
    } else if (entity[0] === '#' || entity[0] === '!') {
      if (parts.length === 1) {
        // room without event permalink
        const [roomId, query = ""] = entity.split("?");
        const via = query.split(/&?via=/g).filter(p => !!p);
        return _PermalinkConstructor.PermalinkParts.forRoom(roomId, via);
      } // rejoin the rest because v3 events can have slashes (annoyingly)


      const eventIdAndQuery = parts.length > 1 ? parts.slice(1).join('/') : "";
      const [eventId, query = ""] = eventIdAndQuery.split("?");
      const via = query.split(/&?via=/g).filter(p => !!p);
      return _PermalinkConstructor.PermalinkParts.forEvent(entity, eventId, via);
    } else {
      throw new Error("Unknown entity type in permalink");
    }
  }

}

exports.default = SpecPermalinkConstructor;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy91dGlscy9wZXJtYWxpbmtzL1NwZWNQZXJtYWxpbmtDb25zdHJ1Y3Rvci5qcyJdLCJuYW1lcyI6WyJob3N0IiwiYmFzZVVybCIsIlNwZWNQZXJtYWxpbmtDb25zdHJ1Y3RvciIsIlBlcm1hbGlua0NvbnN0cnVjdG9yIiwiY29uc3RydWN0b3IiLCJmb3JFdmVudCIsInJvb21JZCIsImV2ZW50SWQiLCJzZXJ2ZXJDYW5kaWRhdGVzIiwiZW5jb2RlU2VydmVyQ2FuZGlkYXRlcyIsImZvclJvb20iLCJyb29tSWRPckFsaWFzIiwiZm9yVXNlciIsInVzZXJJZCIsImZvckdyb3VwIiwiZ3JvdXBJZCIsImZvckVudGl0eSIsImVudGl0eUlkIiwiaXNQZXJtYWxpbmtIb3N0IiwidGVzdEhvc3QiLCJjYW5kaWRhdGVzIiwibGVuZ3RoIiwibWFwIiwiYyIsImVuY29kZVVSSUNvbXBvbmVudCIsImpvaW4iLCJwYXJzZVBlcm1hbGluayIsImZ1bGxVcmwiLCJzdGFydHNXaXRoIiwiRXJyb3IiLCJwYXJ0cyIsInN1YnN0cmluZyIsInNwbGl0IiwiZW50aXR5IiwiUGVybWFsaW5rUGFydHMiLCJxdWVyeSIsInZpYSIsImZpbHRlciIsInAiLCJldmVudElkQW5kUXVlcnkiLCJzbGljZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBZ0JBOztBQWhCQTs7Ozs7Ozs7Ozs7Ozs7O0FBa0JPLE1BQU1BLElBQUksR0FBRyxXQUFiOztBQUNBLE1BQU1DLE9BQU8scUJBQWNELElBQWQsQ0FBYjtBQUVQOzs7Ozs7QUFHZSxNQUFNRSx3QkFBTixTQUF1Q0MsNkJBQXZDLENBQTREO0FBQ3ZFQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQUNIOztBQUVEQyxFQUFBQSxRQUFRLENBQUNDO0FBQUQ7QUFBQSxJQUFpQkM7QUFBakI7QUFBQSxJQUFrQ0M7QUFBbEM7QUFBQTtBQUFBO0FBQXNFO0FBQzFFLHFCQUFVUCxPQUFWLGdCQUF1QkssTUFBdkIsY0FBaUNDLE9BQWpDLFNBQTJDLEtBQUtFLHNCQUFMLENBQTRCRCxnQkFBNUIsQ0FBM0M7QUFDSDs7QUFFREUsRUFBQUEsT0FBTyxDQUFDQztBQUFEO0FBQUEsSUFBd0JIO0FBQXhCO0FBQUE7QUFBQTtBQUE0RDtBQUMvRCxxQkFBVVAsT0FBVixnQkFBdUJVLGFBQXZCLFNBQXVDLEtBQUtGLHNCQUFMLENBQTRCRCxnQkFBNUIsQ0FBdkM7QUFDSDs7QUFFREksRUFBQUEsT0FBTyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUF5QjtBQUM1QixxQkFBVVosT0FBVixnQkFBdUJZLE1BQXZCO0FBQ0g7O0FBRURDLEVBQUFBLFFBQVEsQ0FBQ0M7QUFBRDtBQUFBO0FBQUE7QUFBMEI7QUFDOUIscUJBQVVkLE9BQVYsZ0JBQXVCYyxPQUF2QjtBQUNIOztBQUVEQyxFQUFBQSxTQUFTLENBQUNDO0FBQUQ7QUFBQTtBQUFBO0FBQTJCO0FBQ2hDLHFCQUFVaEIsT0FBVixnQkFBdUJnQixRQUF2QjtBQUNIOztBQUVEQyxFQUFBQSxlQUFlLENBQUNDO0FBQUQ7QUFBQTtBQUFBO0FBQTRCO0FBQ3ZDLFdBQU9BLFFBQVEsS0FBS25CLElBQXBCO0FBQ0g7O0FBRURTLEVBQUFBLHNCQUFzQixDQUFDVztBQUFEO0FBQUEsSUFBdUI7QUFDekMsUUFBSSxDQUFDQSxVQUFELElBQWVBLFVBQVUsQ0FBQ0MsTUFBWCxLQUFzQixDQUF6QyxFQUE0QyxPQUFPLEVBQVA7QUFDNUMsMEJBQWVELFVBQVUsQ0FBQ0UsR0FBWCxDQUFlQyxDQUFDLElBQUlDLGtCQUFrQixDQUFDRCxDQUFELENBQXRDLEVBQTJDRSxJQUEzQyxDQUFnRCxPQUFoRCxDQUFmO0FBQ0gsR0FoQ3NFLENBa0N2RTtBQUNBOzs7QUFDQUMsRUFBQUEsY0FBYyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUFrQztBQUM1QyxRQUFJLENBQUNBLE9BQUQsSUFBWSxDQUFDQSxPQUFPLENBQUNDLFVBQVIsQ0FBbUIzQixPQUFuQixDQUFqQixFQUE4QztBQUMxQyxZQUFNLElBQUk0QixLQUFKLENBQVUsbUNBQVYsQ0FBTjtBQUNIOztBQUVELFVBQU1DLEtBQUssR0FBR0gsT0FBTyxDQUFDSSxTQUFSLENBQWtCLFVBQUc5QixPQUFILFNBQWdCb0IsTUFBbEMsRUFBMENXLEtBQTFDLENBQWdELEdBQWhELENBQWQ7QUFFQSxVQUFNQyxNQUFNLEdBQUdILEtBQUssQ0FBQyxDQUFELENBQXBCOztBQUNBLFFBQUlHLE1BQU0sQ0FBQyxDQUFELENBQU4sS0FBYyxHQUFsQixFQUF1QjtBQUNuQjtBQUNBLGFBQU9DLHFDQUFldEIsT0FBZixDQUF1QnFCLE1BQXZCLENBQVA7QUFDSCxLQUhELE1BR08sSUFBSUEsTUFBTSxDQUFDLENBQUQsQ0FBTixLQUFjLEdBQWxCLEVBQXVCO0FBQzFCO0FBQ0EsYUFBT0MscUNBQWVwQixRQUFmLENBQXdCbUIsTUFBeEIsQ0FBUDtBQUNILEtBSE0sTUFHQSxJQUFJQSxNQUFNLENBQUMsQ0FBRCxDQUFOLEtBQWMsR0FBZCxJQUFxQkEsTUFBTSxDQUFDLENBQUQsQ0FBTixLQUFjLEdBQXZDLEVBQTRDO0FBQy9DLFVBQUlILEtBQUssQ0FBQ1QsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUFFO0FBQ3RCLGNBQU0sQ0FBQ2YsTUFBRCxFQUFTNkIsS0FBSyxHQUFDLEVBQWYsSUFBcUJGLE1BQU0sQ0FBQ0QsS0FBUCxDQUFhLEdBQWIsQ0FBM0I7QUFDQSxjQUFNSSxHQUFHLEdBQUdELEtBQUssQ0FBQ0gsS0FBTixDQUFZLFNBQVosRUFBdUJLLE1BQXZCLENBQThCQyxDQUFDLElBQUksQ0FBQyxDQUFDQSxDQUFyQyxDQUFaO0FBQ0EsZUFBT0oscUNBQWV4QixPQUFmLENBQXVCSixNQUF2QixFQUErQjhCLEdBQS9CLENBQVA7QUFDSCxPQUw4QyxDQU8vQzs7O0FBQ0EsWUFBTUcsZUFBZSxHQUFHVCxLQUFLLENBQUNULE1BQU4sR0FBZSxDQUFmLEdBQW1CUyxLQUFLLENBQUNVLEtBQU4sQ0FBWSxDQUFaLEVBQWVmLElBQWYsQ0FBb0IsR0FBcEIsQ0FBbkIsR0FBOEMsRUFBdEU7QUFDQSxZQUFNLENBQUNsQixPQUFELEVBQVU0QixLQUFLLEdBQUMsRUFBaEIsSUFBc0JJLGVBQWUsQ0FBQ1AsS0FBaEIsQ0FBc0IsR0FBdEIsQ0FBNUI7QUFDQSxZQUFNSSxHQUFHLEdBQUdELEtBQUssQ0FBQ0gsS0FBTixDQUFZLFNBQVosRUFBdUJLLE1BQXZCLENBQThCQyxDQUFDLElBQUksQ0FBQyxDQUFDQSxDQUFyQyxDQUFaO0FBRUEsYUFBT0oscUNBQWU3QixRQUFmLENBQXdCNEIsTUFBeEIsRUFBZ0MxQixPQUFoQyxFQUF5QzZCLEdBQXpDLENBQVA7QUFDSCxLQWJNLE1BYUE7QUFDSCxZQUFNLElBQUlQLEtBQUosQ0FBVSxrQ0FBVixDQUFOO0FBQ0g7QUFDSjs7QUFsRXNFIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUGVybWFsaW5rQ29uc3RydWN0b3IsIHtQZXJtYWxpbmtQYXJ0c30gZnJvbSBcIi4vUGVybWFsaW5rQ29uc3RydWN0b3JcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBob3N0ID0gXCJtYXRyaXgudG9cIjtcclxuZXhwb3J0IGNvbnN0IGJhc2VVcmwgPSBgaHR0cHM6Ly8ke2hvc3R9YDtcclxuXHJcbi8qKlxyXG4gKiBHZW5lcmF0ZXMgbWF0cml4LnRvIHBlcm1hbGlua3NcclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNwZWNQZXJtYWxpbmtDb25zdHJ1Y3RvciBleHRlbmRzIFBlcm1hbGlua0NvbnN0cnVjdG9yIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yRXZlbnQocm9vbUlkOiBzdHJpbmcsIGV2ZW50SWQ6IHN0cmluZywgc2VydmVyQ2FuZGlkYXRlczogc3RyaW5nW10pOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBgJHtiYXNlVXJsfS8jLyR7cm9vbUlkfS8ke2V2ZW50SWR9JHt0aGlzLmVuY29kZVNlcnZlckNhbmRpZGF0ZXMoc2VydmVyQ2FuZGlkYXRlcyl9YDtcclxuICAgIH1cclxuXHJcbiAgICBmb3JSb29tKHJvb21JZE9yQWxpYXM6IHN0cmluZywgc2VydmVyQ2FuZGlkYXRlczogc3RyaW5nW10pOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBgJHtiYXNlVXJsfS8jLyR7cm9vbUlkT3JBbGlhc30ke3RoaXMuZW5jb2RlU2VydmVyQ2FuZGlkYXRlcyhzZXJ2ZXJDYW5kaWRhdGVzKX1gO1xyXG4gICAgfVxyXG5cclxuICAgIGZvclVzZXIodXNlcklkOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBgJHtiYXNlVXJsfS8jLyR7dXNlcklkfWA7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yR3JvdXAoZ3JvdXBJZDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gYCR7YmFzZVVybH0vIy8ke2dyb3VwSWR9YDtcclxuICAgIH1cclxuXHJcbiAgICBmb3JFbnRpdHkoZW50aXR5SWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGAke2Jhc2VVcmx9LyMvJHtlbnRpdHlJZH1gO1xyXG4gICAgfVxyXG5cclxuICAgIGlzUGVybWFsaW5rSG9zdCh0ZXN0SG9zdDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRlc3RIb3N0ID09PSBob3N0O1xyXG4gICAgfVxyXG5cclxuICAgIGVuY29kZVNlcnZlckNhbmRpZGF0ZXMoY2FuZGlkYXRlczogc3RyaW5nW10pIHtcclxuICAgICAgICBpZiAoIWNhbmRpZGF0ZXMgfHwgY2FuZGlkYXRlcy5sZW5ndGggPT09IDApIHJldHVybiAnJztcclxuICAgICAgICByZXR1cm4gYD92aWE9JHtjYW5kaWRhdGVzLm1hcChjID0+IGVuY29kZVVSSUNvbXBvbmVudChjKSkuam9pbihcIiZ2aWE9XCIpfWA7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSGVhdmlseSBpbnNwaXJlZCBieS9ib3Jyb3dlZCBmcm9tIHRoZSBtYXRyaXgtYm90LXNkayAod2l0aCBwZXJtaXNzaW9uKTpcclxuICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS90dXJ0MmxpdmUvbWF0cml4LWpzLWJvdC1zZGsvYmxvYi83YzQ2NjVjOWEyNWMyYzhlMGZlNGU1MDlmMjYxNjUwNWI1YjY2YTFjL3NyYy9QZXJtYWxpbmtzLnRzI0wzMy1MNjFcclxuICAgIHBhcnNlUGVybWFsaW5rKGZ1bGxVcmw6IHN0cmluZyk6IFBlcm1hbGlua1BhcnRzIHtcclxuICAgICAgICBpZiAoIWZ1bGxVcmwgfHwgIWZ1bGxVcmwuc3RhcnRzV2l0aChiYXNlVXJsKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJEb2VzIG5vdCBhcHBlYXIgdG8gYmUgYSBwZXJtYWxpbmtcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBwYXJ0cyA9IGZ1bGxVcmwuc3Vic3RyaW5nKGAke2Jhc2VVcmx9LyMvYC5sZW5ndGgpLnNwbGl0KFwiL1wiKTtcclxuXHJcbiAgICAgICAgY29uc3QgZW50aXR5ID0gcGFydHNbMF07XHJcbiAgICAgICAgaWYgKGVudGl0eVswXSA9PT0gJ0AnKSB7XHJcbiAgICAgICAgICAgIC8vIFByb2JhYmx5IGEgdXNlciwgbm8gZnVydGhlciBwYXJzaW5nIG5lZWRlZC5cclxuICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvclVzZXIoZW50aXR5KTtcclxuICAgICAgICB9IGVsc2UgaWYgKGVudGl0eVswXSA9PT0gJysnKSB7XHJcbiAgICAgICAgICAgIC8vIFByb2JhYmx5IGEgZ3JvdXAsIG5vIGZ1cnRoZXIgcGFyc2luZyBuZWVkZWQuXHJcbiAgICAgICAgICAgIHJldHVybiBQZXJtYWxpbmtQYXJ0cy5mb3JHcm91cChlbnRpdHkpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZW50aXR5WzBdID09PSAnIycgfHwgZW50aXR5WzBdID09PSAnIScpIHtcclxuICAgICAgICAgICAgaWYgKHBhcnRzLmxlbmd0aCA9PT0gMSkgeyAvLyByb29tIHdpdGhvdXQgZXZlbnQgcGVybWFsaW5rXHJcbiAgICAgICAgICAgICAgICBjb25zdCBbcm9vbUlkLCBxdWVyeT1cIlwiXSA9IGVudGl0eS5zcGxpdChcIj9cIik7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2aWEgPSBxdWVyeS5zcGxpdCgvJj92aWE9L2cpLmZpbHRlcihwID0+ICEhcCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gUGVybWFsaW5rUGFydHMuZm9yUm9vbShyb29tSWQsIHZpYSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHJlam9pbiB0aGUgcmVzdCBiZWNhdXNlIHYzIGV2ZW50cyBjYW4gaGF2ZSBzbGFzaGVzIChhbm5veWluZ2x5KVxyXG4gICAgICAgICAgICBjb25zdCBldmVudElkQW5kUXVlcnkgPSBwYXJ0cy5sZW5ndGggPiAxID8gcGFydHMuc2xpY2UoMSkuam9pbignLycpIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3QgW2V2ZW50SWQsIHF1ZXJ5PVwiXCJdID0gZXZlbnRJZEFuZFF1ZXJ5LnNwbGl0KFwiP1wiKTtcclxuICAgICAgICAgICAgY29uc3QgdmlhID0gcXVlcnkuc3BsaXQoLyY/dmlhPS9nKS5maWx0ZXIocCA9PiAhIXApO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvckV2ZW50KGVudGl0eSwgZXZlbnRJZCwgdmlhKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmtub3duIGVudGl0eSB0eXBlIGluIHBlcm1hbGlua1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19