"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _PermalinkConstructor = _interopRequireWildcard(require("./PermalinkConstructor"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Generates permalinks that self-reference the running webapp
 */
class RiotPermalinkConstructor extends _PermalinkConstructor.default {
  constructor(riotUrl
  /*: string*/
  ) {
    super();
    (0, _defineProperty2.default)(this, "_riotUrl", void 0);
    this._riotUrl = riotUrl;

    if (!this._riotUrl.startsWith("http:") && !this._riotUrl.startsWith("https:")) {
      throw new Error("Riot prefix URL does not appear to be an HTTP(S) URL");
    }
  }

  forEvent(roomId
  /*: string*/
  , eventId
  /*: string*/
  , serverCandidates
  /*: string[]*/
  )
  /*: string*/
  {
    return "".concat(this._riotUrl, "/#/room/").concat(roomId, "/").concat(eventId).concat(this.encodeServerCandidates(serverCandidates));
  }

  forRoom(roomIdOrAlias
  /*: string*/
  , serverCandidates
  /*: string[]*/
  )
  /*: string*/
  {
    return "".concat(this._riotUrl, "/#/room/").concat(roomIdOrAlias).concat(this.encodeServerCandidates(serverCandidates));
  }

  forUser(userId
  /*: string*/
  )
  /*: string*/
  {
    return "".concat(this._riotUrl, "/#/user/").concat(userId);
  }

  forGroup(groupId
  /*: string*/
  )
  /*: string*/
  {
    return "".concat(this._riotUrl, "/#/group/").concat(groupId);
  }

  forEntity(entityId
  /*: string*/
  )
  /*: string*/
  {
    if (entityId[0] === '!' || entityId[0] === '#') {
      return this.forRoom(entityId);
    } else if (entityId[0] === '@') {
      return this.forUser(entityId);
    } else if (entityId[0] === '+') {
      return this.forGroup(entityId);
    } else throw new Error("Unrecognized entity");
  }

  isPermalinkHost(testHost
  /*: string*/
  )
  /*: boolean*/
  {
    const parsedUrl = new URL(this._riotUrl);
    return testHost === (parsedUrl.host || parsedUrl.hostname); // one of the hosts should match
  }

  encodeServerCandidates(candidates
  /*: string[]*/
  ) {
    if (!candidates || candidates.length === 0) return '';
    return "?via=".concat(candidates.map(c => encodeURIComponent(c)).join("&via="));
  } // Heavily inspired by/borrowed from the matrix-bot-sdk (with permission):
  // https://github.com/turt2live/matrix-js-bot-sdk/blob/7c4665c9a25c2c8e0fe4e509f2616505b5b66a1c/src/Permalinks.ts#L33-L61
  // Adapted for Riot's URL format


  parsePermalink(fullUrl
  /*: string*/
  )
  /*: PermalinkParts*/
  {
    if (!fullUrl || !fullUrl.startsWith(this._riotUrl)) {
      throw new Error("Does not appear to be a permalink");
    }

    const parts = fullUrl.substring("".concat(this._riotUrl, "/#/").length).split("/");

    if (parts.length < 2) {
      // we're expecting an entity and an ID of some kind at least
      throw new Error("URL is missing parts");
    }

    const entityType = parts[0];
    const entity = parts[1];

    if (entityType === 'user') {
      // Probably a user, no further parsing needed.
      return _PermalinkConstructor.PermalinkParts.forUser(entity);
    } else if (entityType === 'group') {
      // Probably a group, no further parsing needed.
      return _PermalinkConstructor.PermalinkParts.forGroup(entity);
    } else if (entityType === 'room') {
      if (parts.length === 2) {
        return _PermalinkConstructor.PermalinkParts.forRoom(entity, []);
      } // rejoin the rest because v3 events can have slashes (annoyingly)


      const eventIdAndQuery = parts.length > 2 ? parts.slice(2).join('/') : "";
      const secondaryParts = eventIdAndQuery.split("?");
      const eventId = secondaryParts[0];
      const query = secondaryParts.length > 1 ? secondaryParts[1] : ""; // TODO: Verify Riot works with via args

      const via = query.split("via=").filter(p => !!p);
      return _PermalinkConstructor.PermalinkParts.forEvent(entity, eventId, via);
    } else {
      throw new Error("Unknown entity type in permalink");
    }
  }

}

exports.default = RiotPermalinkConstructor;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy91dGlscy9wZXJtYWxpbmtzL1Jpb3RQZXJtYWxpbmtDb25zdHJ1Y3Rvci5qcyJdLCJuYW1lcyI6WyJSaW90UGVybWFsaW5rQ29uc3RydWN0b3IiLCJQZXJtYWxpbmtDb25zdHJ1Y3RvciIsImNvbnN0cnVjdG9yIiwicmlvdFVybCIsIl9yaW90VXJsIiwic3RhcnRzV2l0aCIsIkVycm9yIiwiZm9yRXZlbnQiLCJyb29tSWQiLCJldmVudElkIiwic2VydmVyQ2FuZGlkYXRlcyIsImVuY29kZVNlcnZlckNhbmRpZGF0ZXMiLCJmb3JSb29tIiwicm9vbUlkT3JBbGlhcyIsImZvclVzZXIiLCJ1c2VySWQiLCJmb3JHcm91cCIsImdyb3VwSWQiLCJmb3JFbnRpdHkiLCJlbnRpdHlJZCIsImlzUGVybWFsaW5rSG9zdCIsInRlc3RIb3N0IiwicGFyc2VkVXJsIiwiVVJMIiwiaG9zdCIsImhvc3RuYW1lIiwiY2FuZGlkYXRlcyIsImxlbmd0aCIsIm1hcCIsImMiLCJlbmNvZGVVUklDb21wb25lbnQiLCJqb2luIiwicGFyc2VQZXJtYWxpbmsiLCJmdWxsVXJsIiwicGFydHMiLCJzdWJzdHJpbmciLCJzcGxpdCIsImVudGl0eVR5cGUiLCJlbnRpdHkiLCJQZXJtYWxpbmtQYXJ0cyIsImV2ZW50SWRBbmRRdWVyeSIsInNsaWNlIiwic2Vjb25kYXJ5UGFydHMiLCJxdWVyeSIsInZpYSIsImZpbHRlciIsInAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBaEJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBOzs7QUFHZSxNQUFNQSx3QkFBTixTQUF1Q0MsNkJBQXZDLENBQTREO0FBR3ZFQyxFQUFBQSxXQUFXLENBQUNDO0FBQUQ7QUFBQSxJQUFrQjtBQUN6QjtBQUR5QjtBQUV6QixTQUFLQyxRQUFMLEdBQWdCRCxPQUFoQjs7QUFFQSxRQUFJLENBQUMsS0FBS0MsUUFBTCxDQUFjQyxVQUFkLENBQXlCLE9BQXpCLENBQUQsSUFBc0MsQ0FBQyxLQUFLRCxRQUFMLENBQWNDLFVBQWQsQ0FBeUIsUUFBekIsQ0FBM0MsRUFBK0U7QUFDM0UsWUFBTSxJQUFJQyxLQUFKLENBQVUsc0RBQVYsQ0FBTjtBQUNIO0FBQ0o7O0FBRURDLEVBQUFBLFFBQVEsQ0FBQ0M7QUFBRDtBQUFBLElBQWlCQztBQUFqQjtBQUFBLElBQWtDQztBQUFsQztBQUFBO0FBQUE7QUFBc0U7QUFDMUUscUJBQVUsS0FBS04sUUFBZixxQkFBa0NJLE1BQWxDLGNBQTRDQyxPQUE1QyxTQUFzRCxLQUFLRSxzQkFBTCxDQUE0QkQsZ0JBQTVCLENBQXREO0FBQ0g7O0FBRURFLEVBQUFBLE9BQU8sQ0FBQ0M7QUFBRDtBQUFBLElBQXdCSDtBQUF4QjtBQUFBO0FBQUE7QUFBNEQ7QUFDL0QscUJBQVUsS0FBS04sUUFBZixxQkFBa0NTLGFBQWxDLFNBQWtELEtBQUtGLHNCQUFMLENBQTRCRCxnQkFBNUIsQ0FBbEQ7QUFDSDs7QUFFREksRUFBQUEsT0FBTyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUF5QjtBQUM1QixxQkFBVSxLQUFLWCxRQUFmLHFCQUFrQ1csTUFBbEM7QUFDSDs7QUFFREMsRUFBQUEsUUFBUSxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUEwQjtBQUM5QixxQkFBVSxLQUFLYixRQUFmLHNCQUFtQ2EsT0FBbkM7QUFDSDs7QUFFREMsRUFBQUEsU0FBUyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUEyQjtBQUNoQyxRQUFJQSxRQUFRLENBQUMsQ0FBRCxDQUFSLEtBQWdCLEdBQWhCLElBQXVCQSxRQUFRLENBQUMsQ0FBRCxDQUFSLEtBQWdCLEdBQTNDLEVBQWdEO0FBQzVDLGFBQU8sS0FBS1AsT0FBTCxDQUFhTyxRQUFiLENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSUEsUUFBUSxDQUFDLENBQUQsQ0FBUixLQUFnQixHQUFwQixFQUF5QjtBQUM1QixhQUFPLEtBQUtMLE9BQUwsQ0FBYUssUUFBYixDQUFQO0FBQ0gsS0FGTSxNQUVBLElBQUlBLFFBQVEsQ0FBQyxDQUFELENBQVIsS0FBZ0IsR0FBcEIsRUFBeUI7QUFDNUIsYUFBTyxLQUFLSCxRQUFMLENBQWNHLFFBQWQsQ0FBUDtBQUNILEtBRk0sTUFFQSxNQUFNLElBQUliLEtBQUosQ0FBVSxxQkFBVixDQUFOO0FBQ1Y7O0FBRURjLEVBQUFBLGVBQWUsQ0FBQ0M7QUFBRDtBQUFBO0FBQUE7QUFBNEI7QUFDdkMsVUFBTUMsU0FBUyxHQUFHLElBQUlDLEdBQUosQ0FBUSxLQUFLbkIsUUFBYixDQUFsQjtBQUNBLFdBQU9pQixRQUFRLE1BQU1DLFNBQVMsQ0FBQ0UsSUFBVixJQUFrQkYsU0FBUyxDQUFDRyxRQUFsQyxDQUFmLENBRnVDLENBRXFCO0FBQy9EOztBQUVEZCxFQUFBQSxzQkFBc0IsQ0FBQ2U7QUFBRDtBQUFBLElBQXVCO0FBQ3pDLFFBQUksQ0FBQ0EsVUFBRCxJQUFlQSxVQUFVLENBQUNDLE1BQVgsS0FBc0IsQ0FBekMsRUFBNEMsT0FBTyxFQUFQO0FBQzVDLDBCQUFlRCxVQUFVLENBQUNFLEdBQVgsQ0FBZUMsQ0FBQyxJQUFJQyxrQkFBa0IsQ0FBQ0QsQ0FBRCxDQUF0QyxFQUEyQ0UsSUFBM0MsQ0FBZ0QsT0FBaEQsQ0FBZjtBQUNILEdBOUNzRSxDQWdEdkU7QUFDQTtBQUNBOzs7QUFDQUMsRUFBQUEsY0FBYyxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUFrQztBQUM1QyxRQUFJLENBQUNBLE9BQUQsSUFBWSxDQUFDQSxPQUFPLENBQUM1QixVQUFSLENBQW1CLEtBQUtELFFBQXhCLENBQWpCLEVBQW9EO0FBQ2hELFlBQU0sSUFBSUUsS0FBSixDQUFVLG1DQUFWLENBQU47QUFDSDs7QUFFRCxVQUFNNEIsS0FBSyxHQUFHRCxPQUFPLENBQUNFLFNBQVIsQ0FBa0IsVUFBRyxLQUFLL0IsUUFBUixTQUFzQnVCLE1BQXhDLEVBQWdEUyxLQUFoRCxDQUFzRCxHQUF0RCxDQUFkOztBQUNBLFFBQUlGLEtBQUssQ0FBQ1AsTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQUU7QUFDcEIsWUFBTSxJQUFJckIsS0FBSixDQUFVLHNCQUFWLENBQU47QUFDSDs7QUFFRCxVQUFNK0IsVUFBVSxHQUFHSCxLQUFLLENBQUMsQ0FBRCxDQUF4QjtBQUNBLFVBQU1JLE1BQU0sR0FBR0osS0FBSyxDQUFDLENBQUQsQ0FBcEI7O0FBQ0EsUUFBSUcsVUFBVSxLQUFLLE1BQW5CLEVBQTJCO0FBQ3ZCO0FBQ0EsYUFBT0UscUNBQWV6QixPQUFmLENBQXVCd0IsTUFBdkIsQ0FBUDtBQUNILEtBSEQsTUFHTyxJQUFJRCxVQUFVLEtBQUssT0FBbkIsRUFBNEI7QUFDL0I7QUFDQSxhQUFPRSxxQ0FBZXZCLFFBQWYsQ0FBd0JzQixNQUF4QixDQUFQO0FBQ0gsS0FITSxNQUdBLElBQUlELFVBQVUsS0FBSyxNQUFuQixFQUEyQjtBQUM5QixVQUFJSCxLQUFLLENBQUNQLE1BQU4sS0FBaUIsQ0FBckIsRUFBd0I7QUFDcEIsZUFBT1kscUNBQWUzQixPQUFmLENBQXVCMEIsTUFBdkIsRUFBK0IsRUFBL0IsQ0FBUDtBQUNILE9BSDZCLENBSzlCOzs7QUFDQSxZQUFNRSxlQUFlLEdBQUdOLEtBQUssQ0FBQ1AsTUFBTixHQUFlLENBQWYsR0FBbUJPLEtBQUssQ0FBQ08sS0FBTixDQUFZLENBQVosRUFBZVYsSUFBZixDQUFvQixHQUFwQixDQUFuQixHQUE4QyxFQUF0RTtBQUNBLFlBQU1XLGNBQWMsR0FBR0YsZUFBZSxDQUFDSixLQUFoQixDQUFzQixHQUF0QixDQUF2QjtBQUVBLFlBQU0zQixPQUFPLEdBQUdpQyxjQUFjLENBQUMsQ0FBRCxDQUE5QjtBQUNBLFlBQU1DLEtBQUssR0FBR0QsY0FBYyxDQUFDZixNQUFmLEdBQXdCLENBQXhCLEdBQTRCZSxjQUFjLENBQUMsQ0FBRCxDQUExQyxHQUFnRCxFQUE5RCxDQVY4QixDQVk5Qjs7QUFDQSxZQUFNRSxHQUFHLEdBQUdELEtBQUssQ0FBQ1AsS0FBTixDQUFZLE1BQVosRUFBb0JTLE1BQXBCLENBQTJCQyxDQUFDLElBQUksQ0FBQyxDQUFDQSxDQUFsQyxDQUFaO0FBRUEsYUFBT1AscUNBQWVoQyxRQUFmLENBQXdCK0IsTUFBeEIsRUFBZ0M3QixPQUFoQyxFQUF5Q21DLEdBQXpDLENBQVA7QUFDSCxLQWhCTSxNQWdCQTtBQUNILFlBQU0sSUFBSXRDLEtBQUosQ0FBVSxrQ0FBVixDQUFOO0FBQ0g7QUFDSjs7QUF4RnNFIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUGVybWFsaW5rQ29uc3RydWN0b3IsIHtQZXJtYWxpbmtQYXJ0c30gZnJvbSBcIi4vUGVybWFsaW5rQ29uc3RydWN0b3JcIjtcclxuXHJcbi8qKlxyXG4gKiBHZW5lcmF0ZXMgcGVybWFsaW5rcyB0aGF0IHNlbGYtcmVmZXJlbmNlIHRoZSBydW5uaW5nIHdlYmFwcFxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmlvdFBlcm1hbGlua0NvbnN0cnVjdG9yIGV4dGVuZHMgUGVybWFsaW5rQ29uc3RydWN0b3Ige1xyXG4gICAgX3Jpb3RVcmw6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihyaW90VXJsOiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuX3Jpb3RVcmwgPSByaW90VXJsO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuX3Jpb3RVcmwuc3RhcnRzV2l0aChcImh0dHA6XCIpICYmICF0aGlzLl9yaW90VXJsLnN0YXJ0c1dpdGgoXCJodHRwczpcIikpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiUmlvdCBwcmVmaXggVVJMIGRvZXMgbm90IGFwcGVhciB0byBiZSBhbiBIVFRQKFMpIFVSTFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9yRXZlbnQocm9vbUlkOiBzdHJpbmcsIGV2ZW50SWQ6IHN0cmluZywgc2VydmVyQ2FuZGlkYXRlczogc3RyaW5nW10pOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLl9yaW90VXJsfS8jL3Jvb20vJHtyb29tSWR9LyR7ZXZlbnRJZH0ke3RoaXMuZW5jb2RlU2VydmVyQ2FuZGlkYXRlcyhzZXJ2ZXJDYW5kaWRhdGVzKX1gO1xyXG4gICAgfVxyXG5cclxuICAgIGZvclJvb20ocm9vbUlkT3JBbGlhczogc3RyaW5nLCBzZXJ2ZXJDYW5kaWRhdGVzOiBzdHJpbmdbXSk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuX3Jpb3RVcmx9LyMvcm9vbS8ke3Jvb21JZE9yQWxpYXN9JHt0aGlzLmVuY29kZVNlcnZlckNhbmRpZGF0ZXMoc2VydmVyQ2FuZGlkYXRlcyl9YDtcclxuICAgIH1cclxuXHJcbiAgICBmb3JVc2VyKHVzZXJJZDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gYCR7dGhpcy5fcmlvdFVybH0vIy91c2VyLyR7dXNlcklkfWA7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yR3JvdXAoZ3JvdXBJZDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gYCR7dGhpcy5fcmlvdFVybH0vIy9ncm91cC8ke2dyb3VwSWR9YDtcclxuICAgIH1cclxuXHJcbiAgICBmb3JFbnRpdHkoZW50aXR5SWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKGVudGl0eUlkWzBdID09PSAnIScgfHwgZW50aXR5SWRbMF0gPT09ICcjJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5mb3JSb29tKGVudGl0eUlkKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGVudGl0eUlkWzBdID09PSAnQCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZm9yVXNlcihlbnRpdHlJZCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChlbnRpdHlJZFswXSA9PT0gJysnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmZvckdyb3VwKGVudGl0eUlkKTtcclxuICAgICAgICB9IGVsc2UgdGhyb3cgbmV3IEVycm9yKFwiVW5yZWNvZ25pemVkIGVudGl0eVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBpc1Blcm1hbGlua0hvc3QodGVzdEhvc3Q6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGNvbnN0IHBhcnNlZFVybCA9IG5ldyBVUkwodGhpcy5fcmlvdFVybCk7XHJcbiAgICAgICAgcmV0dXJuIHRlc3RIb3N0ID09PSAocGFyc2VkVXJsLmhvc3QgfHwgcGFyc2VkVXJsLmhvc3RuYW1lKTsgLy8gb25lIG9mIHRoZSBob3N0cyBzaG91bGQgbWF0Y2hcclxuICAgIH1cclxuXHJcbiAgICBlbmNvZGVTZXJ2ZXJDYW5kaWRhdGVzKGNhbmRpZGF0ZXM6IHN0cmluZ1tdKSB7XHJcbiAgICAgICAgaWYgKCFjYW5kaWRhdGVzIHx8IGNhbmRpZGF0ZXMubGVuZ3RoID09PSAwKSByZXR1cm4gJyc7XHJcbiAgICAgICAgcmV0dXJuIGA/dmlhPSR7Y2FuZGlkYXRlcy5tYXAoYyA9PiBlbmNvZGVVUklDb21wb25lbnQoYykpLmpvaW4oXCImdmlhPVwiKX1gO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEhlYXZpbHkgaW5zcGlyZWQgYnkvYm9ycm93ZWQgZnJvbSB0aGUgbWF0cml4LWJvdC1zZGsgKHdpdGggcGVybWlzc2lvbik6XHJcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vdHVydDJsaXZlL21hdHJpeC1qcy1ib3Qtc2RrL2Jsb2IvN2M0NjY1YzlhMjVjMmM4ZTBmZTRlNTA5ZjI2MTY1MDViNWI2NmExYy9zcmMvUGVybWFsaW5rcy50cyNMMzMtTDYxXHJcbiAgICAvLyBBZGFwdGVkIGZvciBSaW90J3MgVVJMIGZvcm1hdFxyXG4gICAgcGFyc2VQZXJtYWxpbmsoZnVsbFVybDogc3RyaW5nKTogUGVybWFsaW5rUGFydHMge1xyXG4gICAgICAgIGlmICghZnVsbFVybCB8fCAhZnVsbFVybC5zdGFydHNXaXRoKHRoaXMuX3Jpb3RVcmwpKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkRvZXMgbm90IGFwcGVhciB0byBiZSBhIHBlcm1hbGlua1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBhcnRzID0gZnVsbFVybC5zdWJzdHJpbmcoYCR7dGhpcy5fcmlvdFVybH0vIy9gLmxlbmd0aCkuc3BsaXQoXCIvXCIpO1xyXG4gICAgICAgIGlmIChwYXJ0cy5sZW5ndGggPCAyKSB7IC8vIHdlJ3JlIGV4cGVjdGluZyBhbiBlbnRpdHkgYW5kIGFuIElEIG9mIHNvbWUga2luZCBhdCBsZWFzdFxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVUkwgaXMgbWlzc2luZyBwYXJ0c1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGVudGl0eVR5cGUgPSBwYXJ0c1swXTtcclxuICAgICAgICBjb25zdCBlbnRpdHkgPSBwYXJ0c1sxXTtcclxuICAgICAgICBpZiAoZW50aXR5VHlwZSA9PT0gJ3VzZXInKSB7XHJcbiAgICAgICAgICAgIC8vIFByb2JhYmx5IGEgdXNlciwgbm8gZnVydGhlciBwYXJzaW5nIG5lZWRlZC5cclxuICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvclVzZXIoZW50aXR5KTtcclxuICAgICAgICB9IGVsc2UgaWYgKGVudGl0eVR5cGUgPT09ICdncm91cCcpIHtcclxuICAgICAgICAgICAgLy8gUHJvYmFibHkgYSBncm91cCwgbm8gZnVydGhlciBwYXJzaW5nIG5lZWRlZC5cclxuICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvckdyb3VwKGVudGl0eSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChlbnRpdHlUeXBlID09PSAncm9vbScpIHtcclxuICAgICAgICAgICAgaWYgKHBhcnRzLmxlbmd0aCA9PT0gMikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvclJvb20oZW50aXR5LCBbXSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHJlam9pbiB0aGUgcmVzdCBiZWNhdXNlIHYzIGV2ZW50cyBjYW4gaGF2ZSBzbGFzaGVzIChhbm5veWluZ2x5KVxyXG4gICAgICAgICAgICBjb25zdCBldmVudElkQW5kUXVlcnkgPSBwYXJ0cy5sZW5ndGggPiAyID8gcGFydHMuc2xpY2UoMikuam9pbignLycpIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3Qgc2Vjb25kYXJ5UGFydHMgPSBldmVudElkQW5kUXVlcnkuc3BsaXQoXCI/XCIpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnRJZCA9IHNlY29uZGFyeVBhcnRzWzBdO1xyXG4gICAgICAgICAgICBjb25zdCBxdWVyeSA9IHNlY29uZGFyeVBhcnRzLmxlbmd0aCA+IDEgPyBzZWNvbmRhcnlQYXJ0c1sxXSA6IFwiXCI7XHJcblxyXG4gICAgICAgICAgICAvLyBUT0RPOiBWZXJpZnkgUmlvdCB3b3JrcyB3aXRoIHZpYSBhcmdzXHJcbiAgICAgICAgICAgIGNvbnN0IHZpYSA9IHF1ZXJ5LnNwbGl0KFwidmlhPVwiKS5maWx0ZXIocCA9PiAhIXApO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIFBlcm1hbGlua1BhcnRzLmZvckV2ZW50KGVudGl0eSwgZXZlbnRJZCwgdmlhKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmtub3duIGVudGl0eSB0eXBlIGluIHBlcm1hbGlua1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19