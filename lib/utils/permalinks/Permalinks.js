"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeGenericPermalink = makeGenericPermalink;
exports.makeUserPermalink = makeUserPermalink;
exports.makeRoomPermalink = makeRoomPermalink;
exports.makeGroupPermalink = makeGroupPermalink;
exports.isPermalinkHost = isPermalinkHost;
exports.tryTransformEntityToPermalink = tryTransformEntityToPermalink;
exports.tryTransformPermalinkToLocalHref = tryTransformPermalinkToLocalHref;
exports.getPrimaryPermalinkEntity = getPrimaryPermalinkEntity;
exports.parsePermalink = parsePermalink;
exports.RoomPermalinkCreator = void 0;

var _MatrixClientPeg = require("../../MatrixClientPeg");

var _isIp = _interopRequireDefault(require("is-ip"));

var utils = _interopRequireWildcard(require("matrix-js-sdk/src/utils"));

var _SpecPermalinkConstructor = _interopRequireWildcard(require("./SpecPermalinkConstructor"));

var _PermalinkConstructor = _interopRequireWildcard(require("./PermalinkConstructor"));

var _RiotPermalinkConstructor = _interopRequireDefault(require("./RiotPermalinkConstructor"));

var _linkifyMatrix = _interopRequireDefault(require("../../linkify-matrix"));

var _SdkConfig = _interopRequireDefault(require("../../SdkConfig"));

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// The maximum number of servers to pick when working out which servers
// to add to permalinks. The servers are appended as ?via=example.org
const MAX_SERVER_CANDIDATES = 3; // Permalinks can have servers appended to them so that the user
// receiving them can have a fighting chance at joining the room.
// These servers are called "candidates" at this point because
// it is unclear whether they are going to be useful to actually
// join in the future.
//
// We pick 3 servers based on the following criteria:
//
//   Server 1: The highest power level user in the room, provided
//   they are at least PL 50. We don't calculate "what is a moderator"
//   here because it is less relevant for the vast majority of rooms.
//   We also want to ensure that we get an admin or high-ranking mod
//   as they are less likely to leave the room. If no user happens
//   to meet this criteria, we'll pick the most popular server in the
//   room.
//
//   Server 2: The next most popular server in the room (in user
//   distribution). This cannot be the same as Server 1. If no other
//   servers are available then we'll only return Server 1.
//
//   Server 3: The next most popular server by user distribution. This
//   has the same rules as Server 2, with the added exception that it
//   must be unique from Server 1 and 2.
// Rationale for popular servers: It's hard to get rid of people when
// they keep flocking in from a particular server. Sure, the server could
// be ACL'd in the future or for some reason be evicted from the room
// however an event like that is unlikely the larger the room gets. If
// the server is ACL'd at the time of generating the link however, we
// shouldn't pick them. We also don't pick IP addresses.
// Note: we don't pick the server the room was created on because the
// homeserver should already be using that server as a last ditch attempt
// and there's less of a guarantee that the server is a resident server.
// Instead, we actively figure out which servers are likely to be residents
// in the future and try to use those.
// Note: Users receiving permalinks that happen to have all 3 potential
// servers fail them (in terms of joining) are somewhat expected to hunt
// down the person who gave them the link to ask for a participating server.
// The receiving user can then manually append the known-good server to
// the list and magically have the link work.

class RoomPermalinkCreator {
  // We support being given a roomId as a fallback in the event the `room` object
  // doesn't exist or is not healthy for us to rely on. For example, loading a
  // permalink to a room which the MatrixClient doesn't know about.
  constructor(room, roomId = null) {
    this._room = room;
    this._roomId = room ? room.roomId : roomId;
    this._highestPlUserId = null;
    this._populationMap = null;
    this._bannedHostsRegexps = null;
    this._allowedHostsRegexps = null;
    this._serverCandidates = null;
    this._started = false;

    if (!this._roomId) {
      throw new Error("Failed to resolve a roomId for the permalink creator to use");
    }

    this.onMembership = this.onMembership.bind(this);
    this.onRoomState = this.onRoomState.bind(this);
  }

  load() {
    if (!this._room || !this._room.currentState) {
      // Under rare and unknown circumstances it is possible to have a room with no
      // currentState, at least potentially at the early stages of joining a room.
      // To avoid breaking everything, we'll just warn rather than throw as well as
      // not bother updating the various aspects of the share link.
      console.warn("Tried to load a permalink creator with no room state");
      return;
    }

    this._updateAllowedServers();

    this._updateHighestPlUser();

    this._updatePopulationMap();

    this._updateServerCandidates();
  }

  start() {
    this.load();

    this._room.on("RoomMember.membership", this.onMembership);

    this._room.on("RoomState.events", this.onRoomState);

    this._started = true;
  }

  stop() {
    this._room.removeListener("RoomMember.membership", this.onMembership);

    this._room.removeListener("RoomState.events", this.onRoomState);

    this._started = false;
  }

  isStarted() {
    return this._started;
  }

  forEvent(eventId) {
    return getPermalinkConstructor().forEvent(this._roomId, eventId, this._serverCandidates);
  }

  forRoom() {
    return getPermalinkConstructor().forRoom(this._roomId, this._serverCandidates);
  }

  onRoomState(event) {
    switch (event.getType()) {
      case "m.room.server_acl":
        this._updateAllowedServers();

        this._updateHighestPlUser();

        this._updatePopulationMap();

        this._updateServerCandidates();

        return;

      case "m.room.power_levels":
        this._updateHighestPlUser();

        this._updateServerCandidates();

        return;
    }
  }

  onMembership(evt, member, oldMembership) {
    const userId = member.userId;
    const membership = member.membership;
    const serverName = getServerName(userId);
    const hasJoined = oldMembership !== "join" && membership === "join";
    const hasLeft = oldMembership === "join" && membership !== "join";

    if (hasLeft) {
      this._populationMap[serverName]--;
    } else if (hasJoined) {
      this._populationMap[serverName]++;
    }

    this._updateHighestPlUser();

    this._updateServerCandidates();
  }

  _updateHighestPlUser() {
    const plEvent = this._room.currentState.getStateEvents("m.room.power_levels", "");

    if (plEvent) {
      const content = plEvent.getContent();

      if (content) {
        const users = content.users;

        if (users) {
          const entries = Object.entries(users);
          const allowedEntries = entries.filter(([userId]) => {
            const member = this._room.getMember(userId);

            if (!member || member.membership !== "join") {
              return false;
            }

            const serverName = getServerName(userId);
            return !isHostnameIpAddress(serverName) && !isHostInRegex(serverName, this._bannedHostsRegexps) && isHostInRegex(serverName, this._allowedHostsRegexps);
          });
          const maxEntry = allowedEntries.reduce((max, entry) => {
            return entry[1] > max[1] ? entry : max;
          }, [null, 0]);
          const [userId, powerLevel] = maxEntry; // object wasn't empty, and max entry wasn't a demotion from the default

          if (userId !== null && powerLevel >= 50) {
            this._highestPlUserId = userId;
            return;
          }
        }
      }
    }

    this._highestPlUserId = null;
  }

  _updateAllowedServers() {
    const bannedHostsRegexps = [];
    let allowedHostsRegexps = [new RegExp(".*")]; // default allow everyone

    if (this._room.currentState) {
      const aclEvent = this._room.currentState.getStateEvents("m.room.server_acl", "");

      if (aclEvent && aclEvent.getContent()) {
        const getRegex = hostname => new RegExp("^" + utils.globToRegexp(hostname, false) + "$");

        const denied = aclEvent.getContent().deny || [];
        denied.forEach(h => bannedHostsRegexps.push(getRegex(h)));
        const allowed = aclEvent.getContent().allow || [];
        allowedHostsRegexps = []; // we don't want to use the default rule here

        allowed.forEach(h => allowedHostsRegexps.push(getRegex(h)));
      }
    }

    this._bannedHostsRegexps = bannedHostsRegexps;
    this._allowedHostsRegexps = allowedHostsRegexps;
  }

  _updatePopulationMap() {
    const populationMap
    /*: { [server: string]: number }*/
    = {};

    for (const member of this._room.getJoinedMembers()) {
      const serverName = getServerName(member.userId);

      if (!populationMap[serverName]) {
        populationMap[serverName] = 0;
      }

      populationMap[serverName]++;
    }

    this._populationMap = populationMap;
  }

  _updateServerCandidates() {
    let candidates = [];

    if (this._highestPlUserId) {
      candidates.push(getServerName(this._highestPlUserId));
    }

    const serversByPopulation = Object.keys(this._populationMap).sort((a, b) => this._populationMap[b] - this._populationMap[a]).filter(a => {
      return !candidates.includes(a) && !isHostnameIpAddress(a) && !isHostInRegex(a, this._bannedHostsRegexps) && isHostInRegex(a, this._allowedHostsRegexps);
    });
    const remainingServers = serversByPopulation.slice(0, MAX_SERVER_CANDIDATES - candidates.length);
    candidates = candidates.concat(remainingServers);
    this._serverCandidates = candidates;
  }

}

exports.RoomPermalinkCreator = RoomPermalinkCreator;

function makeGenericPermalink(entityId
/*: string*/
)
/*: string*/
{
  return getPermalinkConstructor().forEntity(entityId);
}

function makeUserPermalink(userId) {
  return getPermalinkConstructor().forUser(userId);
}

function makeRoomPermalink(roomId) {
  if (!roomId) {
    throw new Error("can't permalink a falsey roomId");
  } // If the roomId isn't actually a room ID, don't try to list the servers.
  // Aliases are already routable, and don't need extra information.


  if (roomId[0] !== '!') return getPermalinkConstructor().forRoom(roomId, []);

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  const room = client.getRoom(roomId);

  if (!room) {
    return getPermalinkConstructor().forRoom(roomId, []);
  }

  const permalinkCreator = new RoomPermalinkCreator(room);
  permalinkCreator.load();
  return permalinkCreator.forRoom();
}

function makeGroupPermalink(groupId) {
  return getPermalinkConstructor().forGroup(groupId);
}

function isPermalinkHost(host
/*: string*/
)
/*: boolean*/
{
  // Always check if the permalink is a spec permalink (callers are likely to call
  // parsePermalink after this function).
  if (new _SpecPermalinkConstructor.default().isPermalinkHost(host)) return true;
  return getPermalinkConstructor().isPermalinkHost(host);
}
/**
 * Transforms an entity (permalink, room alias, user ID, etc) into a local URL
 * if possible. If the given entity is not found to be valid enough to be converted
 * then a null value will be returned.
 * @param {string} entity The entity to transform.
 * @returns {string|null} The transformed permalink or null if unable.
 */


function tryTransformEntityToPermalink(entity
/*: string*/
)
/*: string*/
{
  if (!entity) return null; // Check to see if it is a bare entity for starters

  if (entity[0] === '#' || entity[0] === '!') return makeRoomPermalink(entity);
  if (entity[0] === '@') return makeUserPermalink(entity);
  if (entity[0] === '+') return makeGroupPermalink(entity); // Then try and merge it into a permalink

  return tryTransformPermalinkToLocalHref(entity);
}
/**
 * Transforms a permalink (or possible permalink) into a local URL if possible. If
 * the given permalink is found to not be a permalink, it'll be returned unaltered.
 * @param {string} permalink The permalink to try and transform.
 * @returns {string} The transformed permalink or original URL if unable.
 */


function tryTransformPermalinkToLocalHref(permalink
/*: string*/
)
/*: string*/
{
  if (!permalink.startsWith("http:") && !permalink.startsWith("https:")) {
    return permalink;
  }

  const m = permalink.match(_linkifyMatrix.default.VECTOR_URL_PATTERN);

  if (m) {
    return m[1];
  } // A bit of a hack to convert permalinks of unknown origin to Riot links


  try {
    const permalinkParts = parsePermalink(permalink);

    if (permalinkParts) {
      if (permalinkParts.roomIdOrAlias) {
        const eventIdPart = permalinkParts.eventId ? "/".concat(permalinkParts.eventId) : '';
        permalink = "#/room/".concat(permalinkParts.roomIdOrAlias).concat(eventIdPart);
      } else if (permalinkParts.groupId) {
        permalink = "#/group/".concat(permalinkParts.groupId);
      } else if (permalinkParts.userId) {
        permalink = "#/user/".concat(permalinkParts.userId);
      } // else not a valid permalink for our purposes - do not handle

    }
  } catch (e) {// Not an href we need to care about
  }

  return permalink;
}

function getPrimaryPermalinkEntity(permalink
/*: string*/
)
/*: string*/
{
  try {
    let permalinkParts = parsePermalink(permalink); // If not a permalink, try the vector patterns.

    if (!permalinkParts) {
      const m = permalink.match(_linkifyMatrix.default.VECTOR_URL_PATTERN);

      if (m) {
        // A bit of a hack, but it gets the job done
        const handler = new _RiotPermalinkConstructor.default("http://localhost");
        const entityInfo = m[1].split('#').slice(1).join('#');
        permalinkParts = handler.parsePermalink("http://localhost/#".concat(entityInfo));
      }
    }

    if (!permalinkParts) return null; // not processable

    if (permalinkParts.userId) return permalinkParts.userId;
    if (permalinkParts.groupId) return permalinkParts.groupId;
    if (permalinkParts.roomIdOrAlias) return permalinkParts.roomIdOrAlias;
  } catch (e) {// no entity - not a permalink
  }

  return null;
}

function getPermalinkConstructor()
/*: PermalinkConstructor*/
{
  const riotPrefix = _SdkConfig.default.get()['permalinkPrefix'];

  if (riotPrefix && riotPrefix !== _SpecPermalinkConstructor.baseUrl) {
    return new _RiotPermalinkConstructor.default(riotPrefix);
  }

  return new _SpecPermalinkConstructor.default();
}

function parsePermalink(fullUrl
/*: string*/
)
/*: PermalinkParts*/
{
  const riotPrefix = _SdkConfig.default.get()['permalinkPrefix'];

  if (fullUrl.startsWith(_SpecPermalinkConstructor.baseUrl)) {
    return new _SpecPermalinkConstructor.default().parsePermalink(fullUrl);
  } else if (riotPrefix && fullUrl.startsWith(riotPrefix)) {
    return new _RiotPermalinkConstructor.default(riotPrefix).parsePermalink(fullUrl);
  }

  return null; // not a permalink we can handle
}

function getServerName(userId) {
  return userId.split(":").splice(1).join(":");
}

function getHostnameFromMatrixDomain(domain) {
  if (!domain) return null;
  return new URL("https://".concat(domain)).hostname;
}

function isHostInRegex(hostname, regexps) {
  hostname = getHostnameFromMatrixDomain(hostname);
  if (!hostname) return true; // assumed

  if (regexps.length > 0 && !regexps[0].test) throw new Error(regexps[0]);
  return regexps.filter(h => h.test(hostname)).length > 0;
}

function isHostnameIpAddress(hostname) {
  hostname = getHostnameFromMatrixDomain(hostname);
  if (!hostname) return false; // is-ip doesn't want IPv6 addresses surrounded by brackets, so
  // take them off.

  if (hostname.startsWith("[") && hostname.endsWith("]")) {
    hostname = hostname.substring(1, hostname.length - 1);
  }

  return (0, _isIp.default)(hostname);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy91dGlscy9wZXJtYWxpbmtzL1Blcm1hbGlua3MuanMiXSwibmFtZXMiOlsiTUFYX1NFUlZFUl9DQU5ESURBVEVTIiwiUm9vbVBlcm1hbGlua0NyZWF0b3IiLCJjb25zdHJ1Y3RvciIsInJvb20iLCJyb29tSWQiLCJfcm9vbSIsIl9yb29tSWQiLCJfaGlnaGVzdFBsVXNlcklkIiwiX3BvcHVsYXRpb25NYXAiLCJfYmFubmVkSG9zdHNSZWdleHBzIiwiX2FsbG93ZWRIb3N0c1JlZ2V4cHMiLCJfc2VydmVyQ2FuZGlkYXRlcyIsIl9zdGFydGVkIiwiRXJyb3IiLCJvbk1lbWJlcnNoaXAiLCJiaW5kIiwib25Sb29tU3RhdGUiLCJsb2FkIiwiY3VycmVudFN0YXRlIiwiY29uc29sZSIsIndhcm4iLCJfdXBkYXRlQWxsb3dlZFNlcnZlcnMiLCJfdXBkYXRlSGlnaGVzdFBsVXNlciIsIl91cGRhdGVQb3B1bGF0aW9uTWFwIiwiX3VwZGF0ZVNlcnZlckNhbmRpZGF0ZXMiLCJzdGFydCIsIm9uIiwic3RvcCIsInJlbW92ZUxpc3RlbmVyIiwiaXNTdGFydGVkIiwiZm9yRXZlbnQiLCJldmVudElkIiwiZ2V0UGVybWFsaW5rQ29uc3RydWN0b3IiLCJmb3JSb29tIiwiZXZlbnQiLCJnZXRUeXBlIiwiZXZ0IiwibWVtYmVyIiwib2xkTWVtYmVyc2hpcCIsInVzZXJJZCIsIm1lbWJlcnNoaXAiLCJzZXJ2ZXJOYW1lIiwiZ2V0U2VydmVyTmFtZSIsImhhc0pvaW5lZCIsImhhc0xlZnQiLCJwbEV2ZW50IiwiZ2V0U3RhdGVFdmVudHMiLCJjb250ZW50IiwiZ2V0Q29udGVudCIsInVzZXJzIiwiZW50cmllcyIsIk9iamVjdCIsImFsbG93ZWRFbnRyaWVzIiwiZmlsdGVyIiwiZ2V0TWVtYmVyIiwiaXNIb3N0bmFtZUlwQWRkcmVzcyIsImlzSG9zdEluUmVnZXgiLCJtYXhFbnRyeSIsInJlZHVjZSIsIm1heCIsImVudHJ5IiwicG93ZXJMZXZlbCIsImJhbm5lZEhvc3RzUmVnZXhwcyIsImFsbG93ZWRIb3N0c1JlZ2V4cHMiLCJSZWdFeHAiLCJhY2xFdmVudCIsImdldFJlZ2V4IiwiaG9zdG5hbWUiLCJ1dGlscyIsImdsb2JUb1JlZ2V4cCIsImRlbmllZCIsImRlbnkiLCJmb3JFYWNoIiwiaCIsInB1c2giLCJhbGxvd2VkIiwiYWxsb3ciLCJwb3B1bGF0aW9uTWFwIiwiZ2V0Sm9pbmVkTWVtYmVycyIsImNhbmRpZGF0ZXMiLCJzZXJ2ZXJzQnlQb3B1bGF0aW9uIiwia2V5cyIsInNvcnQiLCJhIiwiYiIsImluY2x1ZGVzIiwicmVtYWluaW5nU2VydmVycyIsInNsaWNlIiwibGVuZ3RoIiwiY29uY2F0IiwibWFrZUdlbmVyaWNQZXJtYWxpbmsiLCJlbnRpdHlJZCIsImZvckVudGl0eSIsIm1ha2VVc2VyUGVybWFsaW5rIiwiZm9yVXNlciIsIm1ha2VSb29tUGVybWFsaW5rIiwiY2xpZW50IiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwiZ2V0Um9vbSIsInBlcm1hbGlua0NyZWF0b3IiLCJtYWtlR3JvdXBQZXJtYWxpbmsiLCJncm91cElkIiwiZm9yR3JvdXAiLCJpc1Blcm1hbGlua0hvc3QiLCJob3N0IiwiU3BlY1Blcm1hbGlua0NvbnN0cnVjdG9yIiwidHJ5VHJhbnNmb3JtRW50aXR5VG9QZXJtYWxpbmsiLCJlbnRpdHkiLCJ0cnlUcmFuc2Zvcm1QZXJtYWxpbmtUb0xvY2FsSHJlZiIsInBlcm1hbGluayIsInN0YXJ0c1dpdGgiLCJtIiwibWF0Y2giLCJtYXRyaXhMaW5raWZ5IiwiVkVDVE9SX1VSTF9QQVRURVJOIiwicGVybWFsaW5rUGFydHMiLCJwYXJzZVBlcm1hbGluayIsInJvb21JZE9yQWxpYXMiLCJldmVudElkUGFydCIsImUiLCJnZXRQcmltYXJ5UGVybWFsaW5rRW50aXR5IiwiaGFuZGxlciIsIlJpb3RQZXJtYWxpbmtDb25zdHJ1Y3RvciIsImVudGl0eUluZm8iLCJzcGxpdCIsImpvaW4iLCJyaW90UHJlZml4IiwiU2RrQ29uZmlnIiwibWF0cml4dG9CYXNlVXJsIiwiZnVsbFVybCIsInNwbGljZSIsImdldEhvc3RuYW1lRnJvbU1hdHJpeERvbWFpbiIsImRvbWFpbiIsIlVSTCIsInJlZ2V4cHMiLCJ0ZXN0IiwiZW5kc1dpdGgiLCJzdWJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBO0FBQ0E7QUFDQSxNQUFNQSxxQkFBcUIsR0FBRyxDQUE5QixDLENBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVPLE1BQU1DLG9CQUFOLENBQTJCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBQyxFQUFBQSxXQUFXLENBQUNDLElBQUQsRUFBT0MsTUFBTSxHQUFHLElBQWhCLEVBQXNCO0FBQzdCLFNBQUtDLEtBQUwsR0FBYUYsSUFBYjtBQUNBLFNBQUtHLE9BQUwsR0FBZUgsSUFBSSxHQUFHQSxJQUFJLENBQUNDLE1BQVIsR0FBaUJBLE1BQXBDO0FBQ0EsU0FBS0csZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxTQUFLQyxjQUFMLEdBQXNCLElBQXRCO0FBQ0EsU0FBS0MsbUJBQUwsR0FBMkIsSUFBM0I7QUFDQSxTQUFLQyxvQkFBTCxHQUE0QixJQUE1QjtBQUNBLFNBQUtDLGlCQUFMLEdBQXlCLElBQXpCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixLQUFoQjs7QUFFQSxRQUFJLENBQUMsS0FBS04sT0FBVixFQUFtQjtBQUNmLFlBQU0sSUFBSU8sS0FBSixDQUFVLDZEQUFWLENBQU47QUFDSDs7QUFFRCxTQUFLQyxZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixLQUFLQSxXQUFMLENBQWlCRCxJQUFqQixDQUFzQixJQUF0QixDQUFuQjtBQUNIOztBQUVERSxFQUFBQSxJQUFJLEdBQUc7QUFDSCxRQUFJLENBQUMsS0FBS1osS0FBTixJQUFlLENBQUMsS0FBS0EsS0FBTCxDQUFXYSxZQUEvQixFQUE2QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBQyxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxzREFBYjtBQUNBO0FBQ0g7O0FBQ0QsU0FBS0MscUJBQUw7O0FBQ0EsU0FBS0Msb0JBQUw7O0FBQ0EsU0FBS0Msb0JBQUw7O0FBQ0EsU0FBS0MsdUJBQUw7QUFDSDs7QUFFREMsRUFBQUEsS0FBSyxHQUFHO0FBQ0osU0FBS1IsSUFBTDs7QUFDQSxTQUFLWixLQUFMLENBQVdxQixFQUFYLENBQWMsdUJBQWQsRUFBdUMsS0FBS1osWUFBNUM7O0FBQ0EsU0FBS1QsS0FBTCxDQUFXcUIsRUFBWCxDQUFjLGtCQUFkLEVBQWtDLEtBQUtWLFdBQXZDOztBQUNBLFNBQUtKLFFBQUwsR0FBZ0IsSUFBaEI7QUFDSDs7QUFFRGUsRUFBQUEsSUFBSSxHQUFHO0FBQ0gsU0FBS3RCLEtBQUwsQ0FBV3VCLGNBQVgsQ0FBMEIsdUJBQTFCLEVBQW1ELEtBQUtkLFlBQXhEOztBQUNBLFNBQUtULEtBQUwsQ0FBV3VCLGNBQVgsQ0FBMEIsa0JBQTFCLEVBQThDLEtBQUtaLFdBQW5EOztBQUNBLFNBQUtKLFFBQUwsR0FBZ0IsS0FBaEI7QUFDSDs7QUFFRGlCLEVBQUFBLFNBQVMsR0FBRztBQUNSLFdBQU8sS0FBS2pCLFFBQVo7QUFDSDs7QUFFRGtCLEVBQUFBLFFBQVEsQ0FBQ0MsT0FBRCxFQUFVO0FBQ2QsV0FBT0MsdUJBQXVCLEdBQUdGLFFBQTFCLENBQW1DLEtBQUt4QixPQUF4QyxFQUFpRHlCLE9BQWpELEVBQTBELEtBQUtwQixpQkFBL0QsQ0FBUDtBQUNIOztBQUVEc0IsRUFBQUEsT0FBTyxHQUFHO0FBQ04sV0FBT0QsdUJBQXVCLEdBQUdDLE9BQTFCLENBQWtDLEtBQUszQixPQUF2QyxFQUFnRCxLQUFLSyxpQkFBckQsQ0FBUDtBQUNIOztBQUVESyxFQUFBQSxXQUFXLENBQUNrQixLQUFELEVBQVE7QUFDZixZQUFRQSxLQUFLLENBQUNDLE9BQU4sRUFBUjtBQUNJLFdBQUssbUJBQUw7QUFDSSxhQUFLZCxxQkFBTDs7QUFDQSxhQUFLQyxvQkFBTDs7QUFDQSxhQUFLQyxvQkFBTDs7QUFDQSxhQUFLQyx1QkFBTDs7QUFDQTs7QUFDSixXQUFLLHFCQUFMO0FBQ0ksYUFBS0Ysb0JBQUw7O0FBQ0EsYUFBS0UsdUJBQUw7O0FBQ0E7QUFWUjtBQVlIOztBQUVEVixFQUFBQSxZQUFZLENBQUNzQixHQUFELEVBQU1DLE1BQU4sRUFBY0MsYUFBZCxFQUE2QjtBQUNyQyxVQUFNQyxNQUFNLEdBQUdGLE1BQU0sQ0FBQ0UsTUFBdEI7QUFDQSxVQUFNQyxVQUFVLEdBQUdILE1BQU0sQ0FBQ0csVUFBMUI7QUFDQSxVQUFNQyxVQUFVLEdBQUdDLGFBQWEsQ0FBQ0gsTUFBRCxDQUFoQztBQUNBLFVBQU1JLFNBQVMsR0FBR0wsYUFBYSxLQUFLLE1BQWxCLElBQTRCRSxVQUFVLEtBQUssTUFBN0Q7QUFDQSxVQUFNSSxPQUFPLEdBQUdOLGFBQWEsS0FBSyxNQUFsQixJQUE0QkUsVUFBVSxLQUFLLE1BQTNEOztBQUVBLFFBQUlJLE9BQUosRUFBYTtBQUNULFdBQUtwQyxjQUFMLENBQW9CaUMsVUFBcEI7QUFDSCxLQUZELE1BRU8sSUFBSUUsU0FBSixFQUFlO0FBQ2xCLFdBQUtuQyxjQUFMLENBQW9CaUMsVUFBcEI7QUFDSDs7QUFFRCxTQUFLbkIsb0JBQUw7O0FBQ0EsU0FBS0UsdUJBQUw7QUFDSDs7QUFFREYsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsVUFBTXVCLE9BQU8sR0FBRyxLQUFLeEMsS0FBTCxDQUFXYSxZQUFYLENBQXdCNEIsY0FBeEIsQ0FBdUMscUJBQXZDLEVBQThELEVBQTlELENBQWhCOztBQUNBLFFBQUlELE9BQUosRUFBYTtBQUNULFlBQU1FLE9BQU8sR0FBR0YsT0FBTyxDQUFDRyxVQUFSLEVBQWhCOztBQUNBLFVBQUlELE9BQUosRUFBYTtBQUNULGNBQU1FLEtBQUssR0FBR0YsT0FBTyxDQUFDRSxLQUF0Qjs7QUFDQSxZQUFJQSxLQUFKLEVBQVc7QUFDUCxnQkFBTUMsT0FBTyxHQUFHQyxNQUFNLENBQUNELE9BQVAsQ0FBZUQsS0FBZixDQUFoQjtBQUNBLGdCQUFNRyxjQUFjLEdBQUdGLE9BQU8sQ0FBQ0csTUFBUixDQUFlLENBQUMsQ0FBQ2QsTUFBRCxDQUFELEtBQWM7QUFDaEQsa0JBQU1GLE1BQU0sR0FBRyxLQUFLaEMsS0FBTCxDQUFXaUQsU0FBWCxDQUFxQmYsTUFBckIsQ0FBZjs7QUFDQSxnQkFBSSxDQUFDRixNQUFELElBQVdBLE1BQU0sQ0FBQ0csVUFBUCxLQUFzQixNQUFyQyxFQUE2QztBQUN6QyxxQkFBTyxLQUFQO0FBQ0g7O0FBQ0Qsa0JBQU1DLFVBQVUsR0FBR0MsYUFBYSxDQUFDSCxNQUFELENBQWhDO0FBQ0EsbUJBQU8sQ0FBQ2dCLG1CQUFtQixDQUFDZCxVQUFELENBQXBCLElBQ0gsQ0FBQ2UsYUFBYSxDQUFDZixVQUFELEVBQWEsS0FBS2hDLG1CQUFsQixDQURYLElBRUgrQyxhQUFhLENBQUNmLFVBQUQsRUFBYSxLQUFLL0Isb0JBQWxCLENBRmpCO0FBR0gsV0FUc0IsQ0FBdkI7QUFVQSxnQkFBTStDLFFBQVEsR0FBR0wsY0FBYyxDQUFDTSxNQUFmLENBQXNCLENBQUNDLEdBQUQsRUFBTUMsS0FBTixLQUFnQjtBQUNuRCxtQkFBUUEsS0FBSyxDQUFDLENBQUQsQ0FBTCxHQUFXRCxHQUFHLENBQUMsQ0FBRCxDQUFmLEdBQXNCQyxLQUF0QixHQUE4QkQsR0FBckM7QUFDSCxXQUZnQixFQUVkLENBQUMsSUFBRCxFQUFPLENBQVAsQ0FGYyxDQUFqQjtBQUdBLGdCQUFNLENBQUNwQixNQUFELEVBQVNzQixVQUFULElBQXVCSixRQUE3QixDQWZPLENBZ0JQOztBQUNBLGNBQUlsQixNQUFNLEtBQUssSUFBWCxJQUFtQnNCLFVBQVUsSUFBSSxFQUFyQyxFQUF5QztBQUNyQyxpQkFBS3RELGdCQUFMLEdBQXdCZ0MsTUFBeEI7QUFDQTtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUNELFNBQUtoQyxnQkFBTCxHQUF3QixJQUF4QjtBQUNIOztBQUVEYyxFQUFBQSxxQkFBcUIsR0FBRztBQUNwQixVQUFNeUMsa0JBQWtCLEdBQUcsRUFBM0I7QUFDQSxRQUFJQyxtQkFBbUIsR0FBRyxDQUFDLElBQUlDLE1BQUosQ0FBVyxJQUFYLENBQUQsQ0FBMUIsQ0FGb0IsQ0FFMEI7O0FBQzlDLFFBQUksS0FBSzNELEtBQUwsQ0FBV2EsWUFBZixFQUE2QjtBQUN6QixZQUFNK0MsUUFBUSxHQUFHLEtBQUs1RCxLQUFMLENBQVdhLFlBQVgsQ0FBd0I0QixjQUF4QixDQUF1QyxtQkFBdkMsRUFBNEQsRUFBNUQsQ0FBakI7O0FBQ0EsVUFBSW1CLFFBQVEsSUFBSUEsUUFBUSxDQUFDakIsVUFBVCxFQUFoQixFQUF1QztBQUNuQyxjQUFNa0IsUUFBUSxHQUFJQyxRQUFELElBQWMsSUFBSUgsTUFBSixDQUFXLE1BQU1JLEtBQUssQ0FBQ0MsWUFBTixDQUFtQkYsUUFBbkIsRUFBNkIsS0FBN0IsQ0FBTixHQUE0QyxHQUF2RCxDQUEvQjs7QUFFQSxjQUFNRyxNQUFNLEdBQUdMLFFBQVEsQ0FBQ2pCLFVBQVQsR0FBc0J1QixJQUF0QixJQUE4QixFQUE3QztBQUNBRCxRQUFBQSxNQUFNLENBQUNFLE9BQVAsQ0FBZUMsQ0FBQyxJQUFJWCxrQkFBa0IsQ0FBQ1ksSUFBbkIsQ0FBd0JSLFFBQVEsQ0FBQ08sQ0FBRCxDQUFoQyxDQUFwQjtBQUVBLGNBQU1FLE9BQU8sR0FBR1YsUUFBUSxDQUFDakIsVUFBVCxHQUFzQjRCLEtBQXRCLElBQStCLEVBQS9DO0FBQ0FiLFFBQUFBLG1CQUFtQixHQUFHLEVBQXRCLENBUG1DLENBT1Q7O0FBQzFCWSxRQUFBQSxPQUFPLENBQUNILE9BQVIsQ0FBZ0JDLENBQUMsSUFBSVYsbUJBQW1CLENBQUNXLElBQXBCLENBQXlCUixRQUFRLENBQUNPLENBQUQsQ0FBakMsQ0FBckI7QUFDSDtBQUNKOztBQUNELFNBQUtoRSxtQkFBTCxHQUEyQnFELGtCQUEzQjtBQUNBLFNBQUtwRCxvQkFBTCxHQUE0QnFELG1CQUE1QjtBQUNIOztBQUVEeEMsRUFBQUEsb0JBQW9CLEdBQUc7QUFDbkIsVUFBTXNEO0FBQTJDO0FBQUEsTUFBRyxFQUFwRDs7QUFDQSxTQUFLLE1BQU14QyxNQUFYLElBQXFCLEtBQUtoQyxLQUFMLENBQVd5RSxnQkFBWCxFQUFyQixFQUFvRDtBQUNoRCxZQUFNckMsVUFBVSxHQUFHQyxhQUFhLENBQUNMLE1BQU0sQ0FBQ0UsTUFBUixDQUFoQzs7QUFDQSxVQUFJLENBQUNzQyxhQUFhLENBQUNwQyxVQUFELENBQWxCLEVBQWdDO0FBQzVCb0MsUUFBQUEsYUFBYSxDQUFDcEMsVUFBRCxDQUFiLEdBQTRCLENBQTVCO0FBQ0g7O0FBQ0RvQyxNQUFBQSxhQUFhLENBQUNwQyxVQUFELENBQWI7QUFDSDs7QUFDRCxTQUFLakMsY0FBTCxHQUFzQnFFLGFBQXRCO0FBQ0g7O0FBRURyRCxFQUFBQSx1QkFBdUIsR0FBRztBQUN0QixRQUFJdUQsVUFBVSxHQUFHLEVBQWpCOztBQUNBLFFBQUksS0FBS3hFLGdCQUFULEVBQTJCO0FBQ3ZCd0UsTUFBQUEsVUFBVSxDQUFDTCxJQUFYLENBQWdCaEMsYUFBYSxDQUFDLEtBQUtuQyxnQkFBTixDQUE3QjtBQUNIOztBQUVELFVBQU15RSxtQkFBbUIsR0FBRzdCLE1BQU0sQ0FBQzhCLElBQVAsQ0FBWSxLQUFLekUsY0FBakIsRUFDdkIwRSxJQUR1QixDQUNsQixDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVSxLQUFLNUUsY0FBTCxDQUFvQjRFLENBQXBCLElBQXlCLEtBQUs1RSxjQUFMLENBQW9CMkUsQ0FBcEIsQ0FEakIsRUFFdkI5QixNQUZ1QixDQUVoQjhCLENBQUMsSUFBSTtBQUNULGFBQU8sQ0FBQ0osVUFBVSxDQUFDTSxRQUFYLENBQW9CRixDQUFwQixDQUFELElBQ0gsQ0FBQzVCLG1CQUFtQixDQUFDNEIsQ0FBRCxDQURqQixJQUVILENBQUMzQixhQUFhLENBQUMyQixDQUFELEVBQUksS0FBSzFFLG1CQUFULENBRlgsSUFHSCtDLGFBQWEsQ0FBQzJCLENBQUQsRUFBSSxLQUFLekUsb0JBQVQsQ0FIakI7QUFJSCxLQVB1QixDQUE1QjtBQVNBLFVBQU00RSxnQkFBZ0IsR0FBR04sbUJBQW1CLENBQUNPLEtBQXBCLENBQTBCLENBQTFCLEVBQTZCdkYscUJBQXFCLEdBQUcrRSxVQUFVLENBQUNTLE1BQWhFLENBQXpCO0FBQ0FULElBQUFBLFVBQVUsR0FBR0EsVUFBVSxDQUFDVSxNQUFYLENBQWtCSCxnQkFBbEIsQ0FBYjtBQUVBLFNBQUszRSxpQkFBTCxHQUF5Qm9FLFVBQXpCO0FBQ0g7O0FBbEw2Qjs7OztBQXFMM0IsU0FBU1csb0JBQVQsQ0FBOEJDO0FBQTlCO0FBQUE7QUFBQTtBQUF3RDtBQUMzRCxTQUFPM0QsdUJBQXVCLEdBQUc0RCxTQUExQixDQUFvQ0QsUUFBcEMsQ0FBUDtBQUNIOztBQUVNLFNBQVNFLGlCQUFULENBQTJCdEQsTUFBM0IsRUFBbUM7QUFDdEMsU0FBT1AsdUJBQXVCLEdBQUc4RCxPQUExQixDQUFrQ3ZELE1BQWxDLENBQVA7QUFDSDs7QUFFTSxTQUFTd0QsaUJBQVQsQ0FBMkIzRixNQUEzQixFQUFtQztBQUN0QyxNQUFJLENBQUNBLE1BQUwsRUFBYTtBQUNULFVBQU0sSUFBSVMsS0FBSixDQUFVLGlDQUFWLENBQU47QUFDSCxHQUhxQyxDQUt0QztBQUNBOzs7QUFDQSxNQUFJVCxNQUFNLENBQUMsQ0FBRCxDQUFOLEtBQWMsR0FBbEIsRUFBdUIsT0FBTzRCLHVCQUF1QixHQUFHQyxPQUExQixDQUFrQzdCLE1BQWxDLEVBQTBDLEVBQTFDLENBQVA7O0FBRXZCLFFBQU00RixNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxRQUFNL0YsSUFBSSxHQUFHNkYsTUFBTSxDQUFDRyxPQUFQLENBQWUvRixNQUFmLENBQWI7O0FBQ0EsTUFBSSxDQUFDRCxJQUFMLEVBQVc7QUFDUCxXQUFPNkIsdUJBQXVCLEdBQUdDLE9BQTFCLENBQWtDN0IsTUFBbEMsRUFBMEMsRUFBMUMsQ0FBUDtBQUNIOztBQUNELFFBQU1nRyxnQkFBZ0IsR0FBRyxJQUFJbkcsb0JBQUosQ0FBeUJFLElBQXpCLENBQXpCO0FBQ0FpRyxFQUFBQSxnQkFBZ0IsQ0FBQ25GLElBQWpCO0FBQ0EsU0FBT21GLGdCQUFnQixDQUFDbkUsT0FBakIsRUFBUDtBQUNIOztBQUVNLFNBQVNvRSxrQkFBVCxDQUE0QkMsT0FBNUIsRUFBcUM7QUFDeEMsU0FBT3RFLHVCQUF1QixHQUFHdUUsUUFBMUIsQ0FBbUNELE9BQW5DLENBQVA7QUFDSDs7QUFFTSxTQUFTRSxlQUFULENBQXlCQztBQUF6QjtBQUFBO0FBQUE7QUFBZ0Q7QUFDbkQ7QUFDQTtBQUNBLE1BQUksSUFBSUMsaUNBQUosR0FBK0JGLGVBQS9CLENBQStDQyxJQUEvQyxDQUFKLEVBQTBELE9BQU8sSUFBUDtBQUMxRCxTQUFPekUsdUJBQXVCLEdBQUd3RSxlQUExQixDQUEwQ0MsSUFBMUMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9PLFNBQVNFLDZCQUFULENBQXVDQztBQUF2QztBQUFBO0FBQUE7QUFBK0Q7QUFDbEUsTUFBSSxDQUFDQSxNQUFMLEVBQWEsT0FBTyxJQUFQLENBRHFELENBR2xFOztBQUNBLE1BQUlBLE1BQU0sQ0FBQyxDQUFELENBQU4sS0FBYyxHQUFkLElBQXFCQSxNQUFNLENBQUMsQ0FBRCxDQUFOLEtBQWMsR0FBdkMsRUFBNEMsT0FBT2IsaUJBQWlCLENBQUNhLE1BQUQsQ0FBeEI7QUFDNUMsTUFBSUEsTUFBTSxDQUFDLENBQUQsQ0FBTixLQUFjLEdBQWxCLEVBQXVCLE9BQU9mLGlCQUFpQixDQUFDZSxNQUFELENBQXhCO0FBQ3ZCLE1BQUlBLE1BQU0sQ0FBQyxDQUFELENBQU4sS0FBYyxHQUFsQixFQUF1QixPQUFPUCxrQkFBa0IsQ0FBQ08sTUFBRCxDQUF6QixDQU4yQyxDQVFsRTs7QUFDQSxTQUFPQyxnQ0FBZ0MsQ0FBQ0QsTUFBRCxDQUF2QztBQUNIO0FBRUQ7Ozs7Ozs7O0FBTU8sU0FBU0MsZ0NBQVQsQ0FBMENDO0FBQTFDO0FBQUE7QUFBQTtBQUFxRTtBQUN4RSxNQUFJLENBQUNBLFNBQVMsQ0FBQ0MsVUFBVixDQUFxQixPQUFyQixDQUFELElBQWtDLENBQUNELFNBQVMsQ0FBQ0MsVUFBVixDQUFxQixRQUFyQixDQUF2QyxFQUF1RTtBQUNuRSxXQUFPRCxTQUFQO0FBQ0g7O0FBRUQsUUFBTUUsQ0FBQyxHQUFHRixTQUFTLENBQUNHLEtBQVYsQ0FBZ0JDLHVCQUFjQyxrQkFBOUIsQ0FBVjs7QUFDQSxNQUFJSCxDQUFKLEVBQU87QUFDSCxXQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFSO0FBQ0gsR0FSdUUsQ0FVeEU7OztBQUNBLE1BQUk7QUFDQSxVQUFNSSxjQUFjLEdBQUdDLGNBQWMsQ0FBQ1AsU0FBRCxDQUFyQzs7QUFDQSxRQUFJTSxjQUFKLEVBQW9CO0FBQ2hCLFVBQUlBLGNBQWMsQ0FBQ0UsYUFBbkIsRUFBa0M7QUFDOUIsY0FBTUMsV0FBVyxHQUFHSCxjQUFjLENBQUNyRixPQUFmLGNBQTZCcUYsY0FBYyxDQUFDckYsT0FBNUMsSUFBd0QsRUFBNUU7QUFDQStFLFFBQUFBLFNBQVMsb0JBQWFNLGNBQWMsQ0FBQ0UsYUFBNUIsU0FBNENDLFdBQTVDLENBQVQ7QUFDSCxPQUhELE1BR08sSUFBSUgsY0FBYyxDQUFDZCxPQUFuQixFQUE0QjtBQUMvQlEsUUFBQUEsU0FBUyxxQkFBY00sY0FBYyxDQUFDZCxPQUE3QixDQUFUO0FBQ0gsT0FGTSxNQUVBLElBQUljLGNBQWMsQ0FBQzdFLE1BQW5CLEVBQTJCO0FBQzlCdUUsUUFBQUEsU0FBUyxvQkFBYU0sY0FBYyxDQUFDN0UsTUFBNUIsQ0FBVDtBQUNILE9BUmUsQ0FRZDs7QUFDTDtBQUNKLEdBWkQsQ0FZRSxPQUFPaUYsQ0FBUCxFQUFVLENBQ1I7QUFDSDs7QUFFRCxTQUFPVixTQUFQO0FBQ0g7O0FBRU0sU0FBU1cseUJBQVQsQ0FBbUNYO0FBQW5DO0FBQUE7QUFBQTtBQUE4RDtBQUNqRSxNQUFJO0FBQ0EsUUFBSU0sY0FBYyxHQUFHQyxjQUFjLENBQUNQLFNBQUQsQ0FBbkMsQ0FEQSxDQUdBOztBQUNBLFFBQUksQ0FBQ00sY0FBTCxFQUFxQjtBQUNqQixZQUFNSixDQUFDLEdBQUdGLFNBQVMsQ0FBQ0csS0FBVixDQUFnQkMsdUJBQWNDLGtCQUE5QixDQUFWOztBQUNBLFVBQUlILENBQUosRUFBTztBQUNIO0FBQ0EsY0FBTVUsT0FBTyxHQUFHLElBQUlDLGlDQUFKLENBQTZCLGtCQUE3QixDQUFoQjtBQUNBLGNBQU1DLFVBQVUsR0FBR1osQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLYSxLQUFMLENBQVcsR0FBWCxFQUFnQnRDLEtBQWhCLENBQXNCLENBQXRCLEVBQXlCdUMsSUFBekIsQ0FBOEIsR0FBOUIsQ0FBbkI7QUFDQVYsUUFBQUEsY0FBYyxHQUFHTSxPQUFPLENBQUNMLGNBQVIsNkJBQTRDTyxVQUE1QyxFQUFqQjtBQUNIO0FBQ0o7O0FBRUQsUUFBSSxDQUFDUixjQUFMLEVBQXFCLE9BQU8sSUFBUCxDQWRyQixDQWNrQzs7QUFDbEMsUUFBSUEsY0FBYyxDQUFDN0UsTUFBbkIsRUFBMkIsT0FBTzZFLGNBQWMsQ0FBQzdFLE1BQXRCO0FBQzNCLFFBQUk2RSxjQUFjLENBQUNkLE9BQW5CLEVBQTRCLE9BQU9jLGNBQWMsQ0FBQ2QsT0FBdEI7QUFDNUIsUUFBSWMsY0FBYyxDQUFDRSxhQUFuQixFQUFrQyxPQUFPRixjQUFjLENBQUNFLGFBQXRCO0FBQ3JDLEdBbEJELENBa0JFLE9BQU9FLENBQVAsRUFBVSxDQUNSO0FBQ0g7O0FBRUQsU0FBTyxJQUFQO0FBQ0g7O0FBRUQsU0FBU3hGLHVCQUFUO0FBQUE7QUFBeUQ7QUFDckQsUUFBTStGLFVBQVUsR0FBR0MsbUJBQVU5QixHQUFWLEdBQWdCLGlCQUFoQixDQUFuQjs7QUFDQSxNQUFJNkIsVUFBVSxJQUFJQSxVQUFVLEtBQUtFLGlDQUFqQyxFQUFrRDtBQUM5QyxXQUFPLElBQUlOLGlDQUFKLENBQTZCSSxVQUE3QixDQUFQO0FBQ0g7O0FBRUQsU0FBTyxJQUFJckIsaUNBQUosRUFBUDtBQUNIOztBQUVNLFNBQVNXLGNBQVQsQ0FBd0JhO0FBQXhCO0FBQUE7QUFBQTtBQUF5RDtBQUM1RCxRQUFNSCxVQUFVLEdBQUdDLG1CQUFVOUIsR0FBVixHQUFnQixpQkFBaEIsQ0FBbkI7O0FBQ0EsTUFBSWdDLE9BQU8sQ0FBQ25CLFVBQVIsQ0FBbUJrQixpQ0FBbkIsQ0FBSixFQUF5QztBQUNyQyxXQUFPLElBQUl2QixpQ0FBSixHQUErQlcsY0FBL0IsQ0FBOENhLE9BQTlDLENBQVA7QUFDSCxHQUZELE1BRU8sSUFBSUgsVUFBVSxJQUFJRyxPQUFPLENBQUNuQixVQUFSLENBQW1CZ0IsVUFBbkIsQ0FBbEIsRUFBa0Q7QUFDckQsV0FBTyxJQUFJSixpQ0FBSixDQUE2QkksVUFBN0IsRUFBeUNWLGNBQXpDLENBQXdEYSxPQUF4RCxDQUFQO0FBQ0g7O0FBRUQsU0FBTyxJQUFQLENBUjRELENBUS9DO0FBQ2hCOztBQUVELFNBQVN4RixhQUFULENBQXVCSCxNQUF2QixFQUErQjtBQUMzQixTQUFPQSxNQUFNLENBQUNzRixLQUFQLENBQWEsR0FBYixFQUFrQk0sTUFBbEIsQ0FBeUIsQ0FBekIsRUFBNEJMLElBQTVCLENBQWlDLEdBQWpDLENBQVA7QUFDSDs7QUFFRCxTQUFTTSwyQkFBVCxDQUFxQ0MsTUFBckMsRUFBNkM7QUFDekMsTUFBSSxDQUFDQSxNQUFMLEVBQWEsT0FBTyxJQUFQO0FBQ2IsU0FBTyxJQUFJQyxHQUFKLG1CQUFtQkQsTUFBbkIsR0FBNkJsRSxRQUFwQztBQUNIOztBQUVELFNBQVNYLGFBQVQsQ0FBdUJXLFFBQXZCLEVBQWlDb0UsT0FBakMsRUFBMEM7QUFDdENwRSxFQUFBQSxRQUFRLEdBQUdpRSwyQkFBMkIsQ0FBQ2pFLFFBQUQsQ0FBdEM7QUFDQSxNQUFJLENBQUNBLFFBQUwsRUFBZSxPQUFPLElBQVAsQ0FGdUIsQ0FFVjs7QUFDNUIsTUFBSW9FLE9BQU8sQ0FBQy9DLE1BQVIsR0FBaUIsQ0FBakIsSUFBc0IsQ0FBQytDLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV0MsSUFBdEMsRUFBNEMsTUFBTSxJQUFJM0gsS0FBSixDQUFVMEgsT0FBTyxDQUFDLENBQUQsQ0FBakIsQ0FBTjtBQUU1QyxTQUFPQSxPQUFPLENBQUNsRixNQUFSLENBQWVvQixDQUFDLElBQUlBLENBQUMsQ0FBQytELElBQUYsQ0FBT3JFLFFBQVAsQ0FBcEIsRUFBc0NxQixNQUF0QyxHQUErQyxDQUF0RDtBQUNIOztBQUVELFNBQVNqQyxtQkFBVCxDQUE2QlksUUFBN0IsRUFBdUM7QUFDbkNBLEVBQUFBLFFBQVEsR0FBR2lFLDJCQUEyQixDQUFDakUsUUFBRCxDQUF0QztBQUNBLE1BQUksQ0FBQ0EsUUFBTCxFQUFlLE9BQU8sS0FBUCxDQUZvQixDQUluQztBQUNBOztBQUNBLE1BQUlBLFFBQVEsQ0FBQzRDLFVBQVQsQ0FBb0IsR0FBcEIsS0FBNEI1QyxRQUFRLENBQUNzRSxRQUFULENBQWtCLEdBQWxCLENBQWhDLEVBQXdEO0FBQ3BEdEUsSUFBQUEsUUFBUSxHQUFHQSxRQUFRLENBQUN1RSxTQUFULENBQW1CLENBQW5CLEVBQXNCdkUsUUFBUSxDQUFDcUIsTUFBVCxHQUFrQixDQUF4QyxDQUFYO0FBQ0g7O0FBRUQsU0FBTyxtQkFBS3JCLFFBQUwsQ0FBUDtBQUNIIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uLy4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQgaXNJcCBmcm9tIFwiaXMtaXBcIjtcclxuaW1wb3J0ICogYXMgdXRpbHMgZnJvbSAnbWF0cml4LWpzLXNkay9zcmMvdXRpbHMnO1xyXG5pbXBvcnQgU3BlY1Blcm1hbGlua0NvbnN0cnVjdG9yLCB7YmFzZVVybCBhcyBtYXRyaXh0b0Jhc2VVcmx9IGZyb20gXCIuL1NwZWNQZXJtYWxpbmtDb25zdHJ1Y3RvclwiO1xyXG5pbXBvcnQgUGVybWFsaW5rQ29uc3RydWN0b3IsIHtQZXJtYWxpbmtQYXJ0c30gZnJvbSBcIi4vUGVybWFsaW5rQ29uc3RydWN0b3JcIjtcclxuaW1wb3J0IFJpb3RQZXJtYWxpbmtDb25zdHJ1Y3RvciBmcm9tIFwiLi9SaW90UGVybWFsaW5rQ29uc3RydWN0b3JcIjtcclxuaW1wb3J0IG1hdHJpeExpbmtpZnkgZnJvbSBcIi4uLy4uL2xpbmtpZnktbWF0cml4XCI7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSBcIi4uLy4uL1Nka0NvbmZpZ1wiO1xyXG5cclxuLy8gVGhlIG1heGltdW0gbnVtYmVyIG9mIHNlcnZlcnMgdG8gcGljayB3aGVuIHdvcmtpbmcgb3V0IHdoaWNoIHNlcnZlcnNcclxuLy8gdG8gYWRkIHRvIHBlcm1hbGlua3MuIFRoZSBzZXJ2ZXJzIGFyZSBhcHBlbmRlZCBhcyA/dmlhPWV4YW1wbGUub3JnXHJcbmNvbnN0IE1BWF9TRVJWRVJfQ0FORElEQVRFUyA9IDM7XHJcblxyXG5cclxuLy8gUGVybWFsaW5rcyBjYW4gaGF2ZSBzZXJ2ZXJzIGFwcGVuZGVkIHRvIHRoZW0gc28gdGhhdCB0aGUgdXNlclxyXG4vLyByZWNlaXZpbmcgdGhlbSBjYW4gaGF2ZSBhIGZpZ2h0aW5nIGNoYW5jZSBhdCBqb2luaW5nIHRoZSByb29tLlxyXG4vLyBUaGVzZSBzZXJ2ZXJzIGFyZSBjYWxsZWQgXCJjYW5kaWRhdGVzXCIgYXQgdGhpcyBwb2ludCBiZWNhdXNlXHJcbi8vIGl0IGlzIHVuY2xlYXIgd2hldGhlciB0aGV5IGFyZSBnb2luZyB0byBiZSB1c2VmdWwgdG8gYWN0dWFsbHlcclxuLy8gam9pbiBpbiB0aGUgZnV0dXJlLlxyXG4vL1xyXG4vLyBXZSBwaWNrIDMgc2VydmVycyBiYXNlZCBvbiB0aGUgZm9sbG93aW5nIGNyaXRlcmlhOlxyXG4vL1xyXG4vLyAgIFNlcnZlciAxOiBUaGUgaGlnaGVzdCBwb3dlciBsZXZlbCB1c2VyIGluIHRoZSByb29tLCBwcm92aWRlZFxyXG4vLyAgIHRoZXkgYXJlIGF0IGxlYXN0IFBMIDUwLiBXZSBkb24ndCBjYWxjdWxhdGUgXCJ3aGF0IGlzIGEgbW9kZXJhdG9yXCJcclxuLy8gICBoZXJlIGJlY2F1c2UgaXQgaXMgbGVzcyByZWxldmFudCBmb3IgdGhlIHZhc3QgbWFqb3JpdHkgb2Ygcm9vbXMuXHJcbi8vICAgV2UgYWxzbyB3YW50IHRvIGVuc3VyZSB0aGF0IHdlIGdldCBhbiBhZG1pbiBvciBoaWdoLXJhbmtpbmcgbW9kXHJcbi8vICAgYXMgdGhleSBhcmUgbGVzcyBsaWtlbHkgdG8gbGVhdmUgdGhlIHJvb20uIElmIG5vIHVzZXIgaGFwcGVuc1xyXG4vLyAgIHRvIG1lZXQgdGhpcyBjcml0ZXJpYSwgd2UnbGwgcGljayB0aGUgbW9zdCBwb3B1bGFyIHNlcnZlciBpbiB0aGVcclxuLy8gICByb29tLlxyXG4vL1xyXG4vLyAgIFNlcnZlciAyOiBUaGUgbmV4dCBtb3N0IHBvcHVsYXIgc2VydmVyIGluIHRoZSByb29tIChpbiB1c2VyXHJcbi8vICAgZGlzdHJpYnV0aW9uKS4gVGhpcyBjYW5ub3QgYmUgdGhlIHNhbWUgYXMgU2VydmVyIDEuIElmIG5vIG90aGVyXHJcbi8vICAgc2VydmVycyBhcmUgYXZhaWxhYmxlIHRoZW4gd2UnbGwgb25seSByZXR1cm4gU2VydmVyIDEuXHJcbi8vXHJcbi8vICAgU2VydmVyIDM6IFRoZSBuZXh0IG1vc3QgcG9wdWxhciBzZXJ2ZXIgYnkgdXNlciBkaXN0cmlidXRpb24uIFRoaXNcclxuLy8gICBoYXMgdGhlIHNhbWUgcnVsZXMgYXMgU2VydmVyIDIsIHdpdGggdGhlIGFkZGVkIGV4Y2VwdGlvbiB0aGF0IGl0XHJcbi8vICAgbXVzdCBiZSB1bmlxdWUgZnJvbSBTZXJ2ZXIgMSBhbmQgMi5cclxuXHJcbi8vIFJhdGlvbmFsZSBmb3IgcG9wdWxhciBzZXJ2ZXJzOiBJdCdzIGhhcmQgdG8gZ2V0IHJpZCBvZiBwZW9wbGUgd2hlblxyXG4vLyB0aGV5IGtlZXAgZmxvY2tpbmcgaW4gZnJvbSBhIHBhcnRpY3VsYXIgc2VydmVyLiBTdXJlLCB0aGUgc2VydmVyIGNvdWxkXHJcbi8vIGJlIEFDTCdkIGluIHRoZSBmdXR1cmUgb3IgZm9yIHNvbWUgcmVhc29uIGJlIGV2aWN0ZWQgZnJvbSB0aGUgcm9vbVxyXG4vLyBob3dldmVyIGFuIGV2ZW50IGxpa2UgdGhhdCBpcyB1bmxpa2VseSB0aGUgbGFyZ2VyIHRoZSByb29tIGdldHMuIElmXHJcbi8vIHRoZSBzZXJ2ZXIgaXMgQUNMJ2QgYXQgdGhlIHRpbWUgb2YgZ2VuZXJhdGluZyB0aGUgbGluayBob3dldmVyLCB3ZVxyXG4vLyBzaG91bGRuJ3QgcGljayB0aGVtLiBXZSBhbHNvIGRvbid0IHBpY2sgSVAgYWRkcmVzc2VzLlxyXG5cclxuLy8gTm90ZTogd2UgZG9uJ3QgcGljayB0aGUgc2VydmVyIHRoZSByb29tIHdhcyBjcmVhdGVkIG9uIGJlY2F1c2UgdGhlXHJcbi8vIGhvbWVzZXJ2ZXIgc2hvdWxkIGFscmVhZHkgYmUgdXNpbmcgdGhhdCBzZXJ2ZXIgYXMgYSBsYXN0IGRpdGNoIGF0dGVtcHRcclxuLy8gYW5kIHRoZXJlJ3MgbGVzcyBvZiBhIGd1YXJhbnRlZSB0aGF0IHRoZSBzZXJ2ZXIgaXMgYSByZXNpZGVudCBzZXJ2ZXIuXHJcbi8vIEluc3RlYWQsIHdlIGFjdGl2ZWx5IGZpZ3VyZSBvdXQgd2hpY2ggc2VydmVycyBhcmUgbGlrZWx5IHRvIGJlIHJlc2lkZW50c1xyXG4vLyBpbiB0aGUgZnV0dXJlIGFuZCB0cnkgdG8gdXNlIHRob3NlLlxyXG5cclxuLy8gTm90ZTogVXNlcnMgcmVjZWl2aW5nIHBlcm1hbGlua3MgdGhhdCBoYXBwZW4gdG8gaGF2ZSBhbGwgMyBwb3RlbnRpYWxcclxuLy8gc2VydmVycyBmYWlsIHRoZW0gKGluIHRlcm1zIG9mIGpvaW5pbmcpIGFyZSBzb21ld2hhdCBleHBlY3RlZCB0byBodW50XHJcbi8vIGRvd24gdGhlIHBlcnNvbiB3aG8gZ2F2ZSB0aGVtIHRoZSBsaW5rIHRvIGFzayBmb3IgYSBwYXJ0aWNpcGF0aW5nIHNlcnZlci5cclxuLy8gVGhlIHJlY2VpdmluZyB1c2VyIGNhbiB0aGVuIG1hbnVhbGx5IGFwcGVuZCB0aGUga25vd24tZ29vZCBzZXJ2ZXIgdG9cclxuLy8gdGhlIGxpc3QgYW5kIG1hZ2ljYWxseSBoYXZlIHRoZSBsaW5rIHdvcmsuXHJcblxyXG5leHBvcnQgY2xhc3MgUm9vbVBlcm1hbGlua0NyZWF0b3Ige1xyXG4gICAgLy8gV2Ugc3VwcG9ydCBiZWluZyBnaXZlbiBhIHJvb21JZCBhcyBhIGZhbGxiYWNrIGluIHRoZSBldmVudCB0aGUgYHJvb21gIG9iamVjdFxyXG4gICAgLy8gZG9lc24ndCBleGlzdCBvciBpcyBub3QgaGVhbHRoeSBmb3IgdXMgdG8gcmVseSBvbi4gRm9yIGV4YW1wbGUsIGxvYWRpbmcgYVxyXG4gICAgLy8gcGVybWFsaW5rIHRvIGEgcm9vbSB3aGljaCB0aGUgTWF0cml4Q2xpZW50IGRvZXNuJ3Qga25vdyBhYm91dC5cclxuICAgIGNvbnN0cnVjdG9yKHJvb20sIHJvb21JZCA9IG51bGwpIHtcclxuICAgICAgICB0aGlzLl9yb29tID0gcm9vbTtcclxuICAgICAgICB0aGlzLl9yb29tSWQgPSByb29tID8gcm9vbS5yb29tSWQgOiByb29tSWQ7XHJcbiAgICAgICAgdGhpcy5faGlnaGVzdFBsVXNlcklkID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9wb3B1bGF0aW9uTWFwID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9iYW5uZWRIb3N0c1JlZ2V4cHMgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2FsbG93ZWRIb3N0c1JlZ2V4cHMgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NlcnZlckNhbmRpZGF0ZXMgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3N0YXJ0ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLl9yb29tSWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRmFpbGVkIHRvIHJlc29sdmUgYSByb29tSWQgZm9yIHRoZSBwZXJtYWxpbmsgY3JlYXRvciB0byB1c2VcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm9uTWVtYmVyc2hpcCA9IHRoaXMub25NZW1iZXJzaGlwLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5vblJvb21TdGF0ZSA9IHRoaXMub25Sb29tU3RhdGUuYmluZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2FkKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fcm9vbSB8fCAhdGhpcy5fcm9vbS5jdXJyZW50U3RhdGUpIHtcclxuICAgICAgICAgICAgLy8gVW5kZXIgcmFyZSBhbmQgdW5rbm93biBjaXJjdW1zdGFuY2VzIGl0IGlzIHBvc3NpYmxlIHRvIGhhdmUgYSByb29tIHdpdGggbm9cclxuICAgICAgICAgICAgLy8gY3VycmVudFN0YXRlLCBhdCBsZWFzdCBwb3RlbnRpYWxseSBhdCB0aGUgZWFybHkgc3RhZ2VzIG9mIGpvaW5pbmcgYSByb29tLlxyXG4gICAgICAgICAgICAvLyBUbyBhdm9pZCBicmVha2luZyBldmVyeXRoaW5nLCB3ZSdsbCBqdXN0IHdhcm4gcmF0aGVyIHRoYW4gdGhyb3cgYXMgd2VsbCBhc1xyXG4gICAgICAgICAgICAvLyBub3QgYm90aGVyIHVwZGF0aW5nIHRoZSB2YXJpb3VzIGFzcGVjdHMgb2YgdGhlIHNoYXJlIGxpbmsuXHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIlRyaWVkIHRvIGxvYWQgYSBwZXJtYWxpbmsgY3JlYXRvciB3aXRoIG5vIHJvb20gc3RhdGVcIik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQWxsb3dlZFNlcnZlcnMoKTtcclxuICAgICAgICB0aGlzLl91cGRhdGVIaWdoZXN0UGxVc2VyKCk7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlUG9wdWxhdGlvbk1hcCgpO1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVNlcnZlckNhbmRpZGF0ZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLmxvYWQoKTtcclxuICAgICAgICB0aGlzLl9yb29tLm9uKFwiUm9vbU1lbWJlci5tZW1iZXJzaGlwXCIsIHRoaXMub25NZW1iZXJzaGlwKTtcclxuICAgICAgICB0aGlzLl9yb29tLm9uKFwiUm9vbVN0YXRlLmV2ZW50c1wiLCB0aGlzLm9uUm9vbVN0YXRlKTtcclxuICAgICAgICB0aGlzLl9zdGFydGVkID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBzdG9wKCkge1xyXG4gICAgICAgIHRoaXMuX3Jvb20ucmVtb3ZlTGlzdGVuZXIoXCJSb29tTWVtYmVyLm1lbWJlcnNoaXBcIiwgdGhpcy5vbk1lbWJlcnNoaXApO1xyXG4gICAgICAgIHRoaXMuX3Jvb20ucmVtb3ZlTGlzdGVuZXIoXCJSb29tU3RhdGUuZXZlbnRzXCIsIHRoaXMub25Sb29tU3RhdGUpO1xyXG4gICAgICAgIHRoaXMuX3N0YXJ0ZWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1N0YXJ0ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXJ0ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yRXZlbnQoZXZlbnRJZCkge1xyXG4gICAgICAgIHJldHVybiBnZXRQZXJtYWxpbmtDb25zdHJ1Y3RvcigpLmZvckV2ZW50KHRoaXMuX3Jvb21JZCwgZXZlbnRJZCwgdGhpcy5fc2VydmVyQ2FuZGlkYXRlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yUm9vbSgpIHtcclxuICAgICAgICByZXR1cm4gZ2V0UGVybWFsaW5rQ29uc3RydWN0b3IoKS5mb3JSb29tKHRoaXMuX3Jvb21JZCwgdGhpcy5fc2VydmVyQ2FuZGlkYXRlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Sb29tU3RhdGUoZXZlbnQpIHtcclxuICAgICAgICBzd2l0Y2ggKGV2ZW50LmdldFR5cGUoKSkge1xyXG4gICAgICAgICAgICBjYXNlIFwibS5yb29tLnNlcnZlcl9hY2xcIjpcclxuICAgICAgICAgICAgICAgIHRoaXMuX3VwZGF0ZUFsbG93ZWRTZXJ2ZXJzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVIaWdoZXN0UGxVc2VyKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVQb3B1bGF0aW9uTWFwKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVTZXJ2ZXJDYW5kaWRhdGVzKCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIGNhc2UgXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCI6XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVIaWdoZXN0UGxVc2VyKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVTZXJ2ZXJDYW5kaWRhdGVzKCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTWVtYmVyc2hpcChldnQsIG1lbWJlciwgb2xkTWVtYmVyc2hpcCkge1xyXG4gICAgICAgIGNvbnN0IHVzZXJJZCA9IG1lbWJlci51c2VySWQ7XHJcbiAgICAgICAgY29uc3QgbWVtYmVyc2hpcCA9IG1lbWJlci5tZW1iZXJzaGlwO1xyXG4gICAgICAgIGNvbnN0IHNlcnZlck5hbWUgPSBnZXRTZXJ2ZXJOYW1lKHVzZXJJZCk7XHJcbiAgICAgICAgY29uc3QgaGFzSm9pbmVkID0gb2xkTWVtYmVyc2hpcCAhPT0gXCJqb2luXCIgJiYgbWVtYmVyc2hpcCA9PT0gXCJqb2luXCI7XHJcbiAgICAgICAgY29uc3QgaGFzTGVmdCA9IG9sZE1lbWJlcnNoaXAgPT09IFwiam9pblwiICYmIG1lbWJlcnNoaXAgIT09IFwiam9pblwiO1xyXG5cclxuICAgICAgICBpZiAoaGFzTGVmdCkge1xyXG4gICAgICAgICAgICB0aGlzLl9wb3B1bGF0aW9uTWFwW3NlcnZlck5hbWVdLS07XHJcbiAgICAgICAgfSBlbHNlIGlmIChoYXNKb2luZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fcG9wdWxhdGlvbk1hcFtzZXJ2ZXJOYW1lXSsrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fdXBkYXRlSGlnaGVzdFBsVXNlcigpO1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVNlcnZlckNhbmRpZGF0ZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBfdXBkYXRlSGlnaGVzdFBsVXNlcigpIHtcclxuICAgICAgICBjb25zdCBwbEV2ZW50ID0gdGhpcy5fcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIFwiXCIpO1xyXG4gICAgICAgIGlmIChwbEV2ZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbnRlbnQgPSBwbEV2ZW50LmdldENvbnRlbnQoKTtcclxuICAgICAgICAgICAgaWYgKGNvbnRlbnQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gY29udGVudC51c2VycztcclxuICAgICAgICAgICAgICAgIGlmICh1c2Vycykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVudHJpZXMgPSBPYmplY3QuZW50cmllcyh1c2Vycyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYWxsb3dlZEVudHJpZXMgPSBlbnRyaWVzLmZpbHRlcigoW3VzZXJJZF0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVtYmVyID0gdGhpcy5fcm9vbS5nZXRNZW1iZXIodXNlcklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFtZW1iZXIgfHwgbWVtYmVyLm1lbWJlcnNoaXAgIT09IFwiam9pblwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VydmVyTmFtZSA9IGdldFNlcnZlck5hbWUodXNlcklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICFpc0hvc3RuYW1lSXBBZGRyZXNzKHNlcnZlck5hbWUpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAhaXNIb3N0SW5SZWdleChzZXJ2ZXJOYW1lLCB0aGlzLl9iYW5uZWRIb3N0c1JlZ2V4cHMpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0hvc3RJblJlZ2V4KHNlcnZlck5hbWUsIHRoaXMuX2FsbG93ZWRIb3N0c1JlZ2V4cHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1heEVudHJ5ID0gYWxsb3dlZEVudHJpZXMucmVkdWNlKChtYXgsIGVudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoZW50cnlbMV0gPiBtYXhbMV0pID8gZW50cnkgOiBtYXg7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW251bGwsIDBdKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBbdXNlcklkLCBwb3dlckxldmVsXSA9IG1heEVudHJ5O1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9iamVjdCB3YXNuJ3QgZW1wdHksIGFuZCBtYXggZW50cnkgd2Fzbid0IGEgZGVtb3Rpb24gZnJvbSB0aGUgZGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh1c2VySWQgIT09IG51bGwgJiYgcG93ZXJMZXZlbCA+PSA1MCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9oaWdoZXN0UGxVc2VySWQgPSB1c2VySWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5faGlnaGVzdFBsVXNlcklkID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBfdXBkYXRlQWxsb3dlZFNlcnZlcnMoKSB7XHJcbiAgICAgICAgY29uc3QgYmFubmVkSG9zdHNSZWdleHBzID0gW107XHJcbiAgICAgICAgbGV0IGFsbG93ZWRIb3N0c1JlZ2V4cHMgPSBbbmV3IFJlZ0V4cChcIi4qXCIpXTsgLy8gZGVmYXVsdCBhbGxvdyBldmVyeW9uZVxyXG4gICAgICAgIGlmICh0aGlzLl9yb29tLmN1cnJlbnRTdGF0ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBhY2xFdmVudCA9IHRoaXMuX3Jvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKFwibS5yb29tLnNlcnZlcl9hY2xcIiwgXCJcIik7XHJcbiAgICAgICAgICAgIGlmIChhY2xFdmVudCAmJiBhY2xFdmVudC5nZXRDb250ZW50KCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGdldFJlZ2V4ID0gKGhvc3RuYW1lKSA9PiBuZXcgUmVnRXhwKFwiXlwiICsgdXRpbHMuZ2xvYlRvUmVnZXhwKGhvc3RuYW1lLCBmYWxzZSkgKyBcIiRcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgZGVuaWVkID0gYWNsRXZlbnQuZ2V0Q29udGVudCgpLmRlbnkgfHwgW107XHJcbiAgICAgICAgICAgICAgICBkZW5pZWQuZm9yRWFjaChoID0+IGJhbm5lZEhvc3RzUmVnZXhwcy5wdXNoKGdldFJlZ2V4KGgpKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgYWxsb3dlZCA9IGFjbEV2ZW50LmdldENvbnRlbnQoKS5hbGxvdyB8fCBbXTtcclxuICAgICAgICAgICAgICAgIGFsbG93ZWRIb3N0c1JlZ2V4cHMgPSBbXTsgLy8gd2UgZG9uJ3Qgd2FudCB0byB1c2UgdGhlIGRlZmF1bHQgcnVsZSBoZXJlXHJcbiAgICAgICAgICAgICAgICBhbGxvd2VkLmZvckVhY2goaCA9PiBhbGxvd2VkSG9zdHNSZWdleHBzLnB1c2goZ2V0UmVnZXgoaCkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9iYW5uZWRIb3N0c1JlZ2V4cHMgPSBiYW5uZWRIb3N0c1JlZ2V4cHM7XHJcbiAgICAgICAgdGhpcy5fYWxsb3dlZEhvc3RzUmVnZXhwcyA9IGFsbG93ZWRIb3N0c1JlZ2V4cHM7XHJcbiAgICB9XHJcblxyXG4gICAgX3VwZGF0ZVBvcHVsYXRpb25NYXAoKSB7XHJcbiAgICAgICAgY29uc3QgcG9wdWxhdGlvbk1hcDogeyBbc2VydmVyOiBzdHJpbmddOiBudW1iZXIgfSA9IHt9O1xyXG4gICAgICAgIGZvciAoY29uc3QgbWVtYmVyIG9mIHRoaXMuX3Jvb20uZ2V0Sm9pbmVkTWVtYmVycygpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlcnZlck5hbWUgPSBnZXRTZXJ2ZXJOYW1lKG1lbWJlci51c2VySWQpO1xyXG4gICAgICAgICAgICBpZiAoIXBvcHVsYXRpb25NYXBbc2VydmVyTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgIHBvcHVsYXRpb25NYXBbc2VydmVyTmFtZV0gPSAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHBvcHVsYXRpb25NYXBbc2VydmVyTmFtZV0rKztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fcG9wdWxhdGlvbk1hcCA9IHBvcHVsYXRpb25NYXA7XHJcbiAgICB9XHJcblxyXG4gICAgX3VwZGF0ZVNlcnZlckNhbmRpZGF0ZXMoKSB7XHJcbiAgICAgICAgbGV0IGNhbmRpZGF0ZXMgPSBbXTtcclxuICAgICAgICBpZiAodGhpcy5faGlnaGVzdFBsVXNlcklkKSB7XHJcbiAgICAgICAgICAgIGNhbmRpZGF0ZXMucHVzaChnZXRTZXJ2ZXJOYW1lKHRoaXMuX2hpZ2hlc3RQbFVzZXJJZCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2VydmVyc0J5UG9wdWxhdGlvbiA9IE9iamVjdC5rZXlzKHRoaXMuX3BvcHVsYXRpb25NYXApXHJcbiAgICAgICAgICAgIC5zb3J0KChhLCBiKSA9PiB0aGlzLl9wb3B1bGF0aW9uTWFwW2JdIC0gdGhpcy5fcG9wdWxhdGlvbk1hcFthXSlcclxuICAgICAgICAgICAgLmZpbHRlcihhID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAhY2FuZGlkYXRlcy5pbmNsdWRlcyhhKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICFpc0hvc3RuYW1lSXBBZGRyZXNzKGEpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgIWlzSG9zdEluUmVnZXgoYSwgdGhpcy5fYmFubmVkSG9zdHNSZWdleHBzKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGlzSG9zdEluUmVnZXgoYSwgdGhpcy5fYWxsb3dlZEhvc3RzUmVnZXhwcyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCByZW1haW5pbmdTZXJ2ZXJzID0gc2VydmVyc0J5UG9wdWxhdGlvbi5zbGljZSgwLCBNQVhfU0VSVkVSX0NBTkRJREFURVMgLSBjYW5kaWRhdGVzLmxlbmd0aCk7XHJcbiAgICAgICAgY2FuZGlkYXRlcyA9IGNhbmRpZGF0ZXMuY29uY2F0KHJlbWFpbmluZ1NlcnZlcnMpO1xyXG5cclxuICAgICAgICB0aGlzLl9zZXJ2ZXJDYW5kaWRhdGVzID0gY2FuZGlkYXRlcztcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG1ha2VHZW5lcmljUGVybWFsaW5rKGVudGl0eUlkOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIGdldFBlcm1hbGlua0NvbnN0cnVjdG9yKCkuZm9yRW50aXR5KGVudGl0eUlkKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG1ha2VVc2VyUGVybWFsaW5rKHVzZXJJZCkge1xyXG4gICAgcmV0dXJuIGdldFBlcm1hbGlua0NvbnN0cnVjdG9yKCkuZm9yVXNlcih1c2VySWQpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbWFrZVJvb21QZXJtYWxpbmsocm9vbUlkKSB7XHJcbiAgICBpZiAoIXJvb21JZCkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcImNhbid0IHBlcm1hbGluayBhIGZhbHNleSByb29tSWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSWYgdGhlIHJvb21JZCBpc24ndCBhY3R1YWxseSBhIHJvb20gSUQsIGRvbid0IHRyeSB0byBsaXN0IHRoZSBzZXJ2ZXJzLlxyXG4gICAgLy8gQWxpYXNlcyBhcmUgYWxyZWFkeSByb3V0YWJsZSwgYW5kIGRvbid0IG5lZWQgZXh0cmEgaW5mb3JtYXRpb24uXHJcbiAgICBpZiAocm9vbUlkWzBdICE9PSAnIScpIHJldHVybiBnZXRQZXJtYWxpbmtDb25zdHJ1Y3RvcigpLmZvclJvb20ocm9vbUlkLCBbXSk7XHJcblxyXG4gICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICByZXR1cm4gZ2V0UGVybWFsaW5rQ29uc3RydWN0b3IoKS5mb3JSb29tKHJvb21JZCwgW10pO1xyXG4gICAgfVxyXG4gICAgY29uc3QgcGVybWFsaW5rQ3JlYXRvciA9IG5ldyBSb29tUGVybWFsaW5rQ3JlYXRvcihyb29tKTtcclxuICAgIHBlcm1hbGlua0NyZWF0b3IubG9hZCgpO1xyXG4gICAgcmV0dXJuIHBlcm1hbGlua0NyZWF0b3IuZm9yUm9vbSgpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbWFrZUdyb3VwUGVybWFsaW5rKGdyb3VwSWQpIHtcclxuICAgIHJldHVybiBnZXRQZXJtYWxpbmtDb25zdHJ1Y3RvcigpLmZvckdyb3VwKGdyb3VwSWQpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaXNQZXJtYWxpbmtIb3N0KGhvc3Q6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgLy8gQWx3YXlzIGNoZWNrIGlmIHRoZSBwZXJtYWxpbmsgaXMgYSBzcGVjIHBlcm1hbGluayAoY2FsbGVycyBhcmUgbGlrZWx5IHRvIGNhbGxcclxuICAgIC8vIHBhcnNlUGVybWFsaW5rIGFmdGVyIHRoaXMgZnVuY3Rpb24pLlxyXG4gICAgaWYgKG5ldyBTcGVjUGVybWFsaW5rQ29uc3RydWN0b3IoKS5pc1Blcm1hbGlua0hvc3QoaG9zdCkpIHJldHVybiB0cnVlO1xyXG4gICAgcmV0dXJuIGdldFBlcm1hbGlua0NvbnN0cnVjdG9yKCkuaXNQZXJtYWxpbmtIb3N0KGhvc3QpO1xyXG59XHJcblxyXG4vKipcclxuICogVHJhbnNmb3JtcyBhbiBlbnRpdHkgKHBlcm1hbGluaywgcm9vbSBhbGlhcywgdXNlciBJRCwgZXRjKSBpbnRvIGEgbG9jYWwgVVJMXHJcbiAqIGlmIHBvc3NpYmxlLiBJZiB0aGUgZ2l2ZW4gZW50aXR5IGlzIG5vdCBmb3VuZCB0byBiZSB2YWxpZCBlbm91Z2ggdG8gYmUgY29udmVydGVkXHJcbiAqIHRoZW4gYSBudWxsIHZhbHVlIHdpbGwgYmUgcmV0dXJuZWQuXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBlbnRpdHkgVGhlIGVudGl0eSB0byB0cmFuc2Zvcm0uXHJcbiAqIEByZXR1cm5zIHtzdHJpbmd8bnVsbH0gVGhlIHRyYW5zZm9ybWVkIHBlcm1hbGluayBvciBudWxsIGlmIHVuYWJsZS5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiB0cnlUcmFuc2Zvcm1FbnRpdHlUb1Blcm1hbGluayhlbnRpdHk6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBpZiAoIWVudGl0eSkgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgLy8gQ2hlY2sgdG8gc2VlIGlmIGl0IGlzIGEgYmFyZSBlbnRpdHkgZm9yIHN0YXJ0ZXJzXHJcbiAgICBpZiAoZW50aXR5WzBdID09PSAnIycgfHwgZW50aXR5WzBdID09PSAnIScpIHJldHVybiBtYWtlUm9vbVBlcm1hbGluayhlbnRpdHkpO1xyXG4gICAgaWYgKGVudGl0eVswXSA9PT0gJ0AnKSByZXR1cm4gbWFrZVVzZXJQZXJtYWxpbmsoZW50aXR5KTtcclxuICAgIGlmIChlbnRpdHlbMF0gPT09ICcrJykgcmV0dXJuIG1ha2VHcm91cFBlcm1hbGluayhlbnRpdHkpO1xyXG5cclxuICAgIC8vIFRoZW4gdHJ5IGFuZCBtZXJnZSBpdCBpbnRvIGEgcGVybWFsaW5rXHJcbiAgICByZXR1cm4gdHJ5VHJhbnNmb3JtUGVybWFsaW5rVG9Mb2NhbEhyZWYoZW50aXR5KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRyYW5zZm9ybXMgYSBwZXJtYWxpbmsgKG9yIHBvc3NpYmxlIHBlcm1hbGluaykgaW50byBhIGxvY2FsIFVSTCBpZiBwb3NzaWJsZS4gSWZcclxuICogdGhlIGdpdmVuIHBlcm1hbGluayBpcyBmb3VuZCB0byBub3QgYmUgYSBwZXJtYWxpbmssIGl0J2xsIGJlIHJldHVybmVkIHVuYWx0ZXJlZC5cclxuICogQHBhcmFtIHtzdHJpbmd9IHBlcm1hbGluayBUaGUgcGVybWFsaW5rIHRvIHRyeSBhbmQgdHJhbnNmb3JtLlxyXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBUaGUgdHJhbnNmb3JtZWQgcGVybWFsaW5rIG9yIG9yaWdpbmFsIFVSTCBpZiB1bmFibGUuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gdHJ5VHJhbnNmb3JtUGVybWFsaW5rVG9Mb2NhbEhyZWYocGVybWFsaW5rOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCFwZXJtYWxpbmsuc3RhcnRzV2l0aChcImh0dHA6XCIpICYmICFwZXJtYWxpbmsuc3RhcnRzV2l0aChcImh0dHBzOlwiKSkge1xyXG4gICAgICAgIHJldHVybiBwZXJtYWxpbms7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgbSA9IHBlcm1hbGluay5tYXRjaChtYXRyaXhMaW5raWZ5LlZFQ1RPUl9VUkxfUEFUVEVSTik7XHJcbiAgICBpZiAobSkge1xyXG4gICAgICAgIHJldHVybiBtWzFdO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEEgYml0IG9mIGEgaGFjayB0byBjb252ZXJ0IHBlcm1hbGlua3Mgb2YgdW5rbm93biBvcmlnaW4gdG8gUmlvdCBsaW5rc1xyXG4gICAgdHJ5IHtcclxuICAgICAgICBjb25zdCBwZXJtYWxpbmtQYXJ0cyA9IHBhcnNlUGVybWFsaW5rKHBlcm1hbGluayk7XHJcbiAgICAgICAgaWYgKHBlcm1hbGlua1BhcnRzKSB7XHJcbiAgICAgICAgICAgIGlmIChwZXJtYWxpbmtQYXJ0cy5yb29tSWRPckFsaWFzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudElkUGFydCA9IHBlcm1hbGlua1BhcnRzLmV2ZW50SWQgPyBgLyR7cGVybWFsaW5rUGFydHMuZXZlbnRJZH1gIDogJyc7XHJcbiAgICAgICAgICAgICAgICBwZXJtYWxpbmsgPSBgIy9yb29tLyR7cGVybWFsaW5rUGFydHMucm9vbUlkT3JBbGlhc30ke2V2ZW50SWRQYXJ0fWA7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGVybWFsaW5rUGFydHMuZ3JvdXBJZCkge1xyXG4gICAgICAgICAgICAgICAgcGVybWFsaW5rID0gYCMvZ3JvdXAvJHtwZXJtYWxpbmtQYXJ0cy5ncm91cElkfWA7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGVybWFsaW5rUGFydHMudXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBwZXJtYWxpbmsgPSBgIy91c2VyLyR7cGVybWFsaW5rUGFydHMudXNlcklkfWA7XHJcbiAgICAgICAgICAgIH0gLy8gZWxzZSBub3QgYSB2YWxpZCBwZXJtYWxpbmsgZm9yIG91ciBwdXJwb3NlcyAtIGRvIG5vdCBoYW5kbGVcclxuICAgICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgLy8gTm90IGFuIGhyZWYgd2UgbmVlZCB0byBjYXJlIGFib3V0XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHBlcm1hbGluaztcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldFByaW1hcnlQZXJtYWxpbmtFbnRpdHkocGVybWFsaW5rOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgdHJ5IHtcclxuICAgICAgICBsZXQgcGVybWFsaW5rUGFydHMgPSBwYXJzZVBlcm1hbGluayhwZXJtYWxpbmspO1xyXG5cclxuICAgICAgICAvLyBJZiBub3QgYSBwZXJtYWxpbmssIHRyeSB0aGUgdmVjdG9yIHBhdHRlcm5zLlxyXG4gICAgICAgIGlmICghcGVybWFsaW5rUGFydHMpIHtcclxuICAgICAgICAgICAgY29uc3QgbSA9IHBlcm1hbGluay5tYXRjaChtYXRyaXhMaW5raWZ5LlZFQ1RPUl9VUkxfUEFUVEVSTik7XHJcbiAgICAgICAgICAgIGlmIChtKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBBIGJpdCBvZiBhIGhhY2ssIGJ1dCBpdCBnZXRzIHRoZSBqb2IgZG9uZVxyXG4gICAgICAgICAgICAgICAgY29uc3QgaGFuZGxlciA9IG5ldyBSaW90UGVybWFsaW5rQ29uc3RydWN0b3IoXCJodHRwOi8vbG9jYWxob3N0XCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZW50aXR5SW5mbyA9IG1bMV0uc3BsaXQoJyMnKS5zbGljZSgxKS5qb2luKCcjJyk7XHJcbiAgICAgICAgICAgICAgICBwZXJtYWxpbmtQYXJ0cyA9IGhhbmRsZXIucGFyc2VQZXJtYWxpbmsoYGh0dHA6Ly9sb2NhbGhvc3QvIyR7ZW50aXR5SW5mb31gKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFwZXJtYWxpbmtQYXJ0cykgcmV0dXJuIG51bGw7IC8vIG5vdCBwcm9jZXNzYWJsZVxyXG4gICAgICAgIGlmIChwZXJtYWxpbmtQYXJ0cy51c2VySWQpIHJldHVybiBwZXJtYWxpbmtQYXJ0cy51c2VySWQ7XHJcbiAgICAgICAgaWYgKHBlcm1hbGlua1BhcnRzLmdyb3VwSWQpIHJldHVybiBwZXJtYWxpbmtQYXJ0cy5ncm91cElkO1xyXG4gICAgICAgIGlmIChwZXJtYWxpbmtQYXJ0cy5yb29tSWRPckFsaWFzKSByZXR1cm4gcGVybWFsaW5rUGFydHMucm9vbUlkT3JBbGlhcztcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAvLyBubyBlbnRpdHkgLSBub3QgYSBwZXJtYWxpbmtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbnVsbDtcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0UGVybWFsaW5rQ29uc3RydWN0b3IoKTogUGVybWFsaW5rQ29uc3RydWN0b3Ige1xyXG4gICAgY29uc3QgcmlvdFByZWZpeCA9IFNka0NvbmZpZy5nZXQoKVsncGVybWFsaW5rUHJlZml4J107XHJcbiAgICBpZiAocmlvdFByZWZpeCAmJiByaW90UHJlZml4ICE9PSBtYXRyaXh0b0Jhc2VVcmwpIHtcclxuICAgICAgICByZXR1cm4gbmV3IFJpb3RQZXJtYWxpbmtDb25zdHJ1Y3RvcihyaW90UHJlZml4KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbmV3IFNwZWNQZXJtYWxpbmtDb25zdHJ1Y3RvcigpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gcGFyc2VQZXJtYWxpbmsoZnVsbFVybDogc3RyaW5nKTogUGVybWFsaW5rUGFydHMge1xyXG4gICAgY29uc3QgcmlvdFByZWZpeCA9IFNka0NvbmZpZy5nZXQoKVsncGVybWFsaW5rUHJlZml4J107XHJcbiAgICBpZiAoZnVsbFVybC5zdGFydHNXaXRoKG1hdHJpeHRvQmFzZVVybCkpIHtcclxuICAgICAgICByZXR1cm4gbmV3IFNwZWNQZXJtYWxpbmtDb25zdHJ1Y3RvcigpLnBhcnNlUGVybWFsaW5rKGZ1bGxVcmwpO1xyXG4gICAgfSBlbHNlIGlmIChyaW90UHJlZml4ICYmIGZ1bGxVcmwuc3RhcnRzV2l0aChyaW90UHJlZml4KSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUmlvdFBlcm1hbGlua0NvbnN0cnVjdG9yKHJpb3RQcmVmaXgpLnBhcnNlUGVybWFsaW5rKGZ1bGxVcmwpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBudWxsOyAvLyBub3QgYSBwZXJtYWxpbmsgd2UgY2FuIGhhbmRsZVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRTZXJ2ZXJOYW1lKHVzZXJJZCkge1xyXG4gICAgcmV0dXJuIHVzZXJJZC5zcGxpdChcIjpcIikuc3BsaWNlKDEpLmpvaW4oXCI6XCIpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRIb3N0bmFtZUZyb21NYXRyaXhEb21haW4oZG9tYWluKSB7XHJcbiAgICBpZiAoIWRvbWFpbikgcmV0dXJuIG51bGw7XHJcbiAgICByZXR1cm4gbmV3IFVSTChgaHR0cHM6Ly8ke2RvbWFpbn1gKS5ob3N0bmFtZTtcclxufVxyXG5cclxuZnVuY3Rpb24gaXNIb3N0SW5SZWdleChob3N0bmFtZSwgcmVnZXhwcykge1xyXG4gICAgaG9zdG5hbWUgPSBnZXRIb3N0bmFtZUZyb21NYXRyaXhEb21haW4oaG9zdG5hbWUpO1xyXG4gICAgaWYgKCFob3N0bmFtZSkgcmV0dXJuIHRydWU7IC8vIGFzc3VtZWRcclxuICAgIGlmIChyZWdleHBzLmxlbmd0aCA+IDAgJiYgIXJlZ2V4cHNbMF0udGVzdCkgdGhyb3cgbmV3IEVycm9yKHJlZ2V4cHNbMF0pO1xyXG5cclxuICAgIHJldHVybiByZWdleHBzLmZpbHRlcihoID0+IGgudGVzdChob3N0bmFtZSkpLmxlbmd0aCA+IDA7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGlzSG9zdG5hbWVJcEFkZHJlc3MoaG9zdG5hbWUpIHtcclxuICAgIGhvc3RuYW1lID0gZ2V0SG9zdG5hbWVGcm9tTWF0cml4RG9tYWluKGhvc3RuYW1lKTtcclxuICAgIGlmICghaG9zdG5hbWUpIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAvLyBpcy1pcCBkb2Vzbid0IHdhbnQgSVB2NiBhZGRyZXNzZXMgc3Vycm91bmRlZCBieSBicmFja2V0cywgc29cclxuICAgIC8vIHRha2UgdGhlbSBvZmYuXHJcbiAgICBpZiAoaG9zdG5hbWUuc3RhcnRzV2l0aChcIltcIikgJiYgaG9zdG5hbWUuZW5kc1dpdGgoXCJdXCIpKSB7XHJcbiAgICAgICAgaG9zdG5hbWUgPSBob3N0bmFtZS5zdWJzdHJpbmcoMSwgaG9zdG5hbWUubGVuZ3RoIC0gMSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGlzSXAoaG9zdG5hbWUpO1xyXG59XHJcbiJdfQ==