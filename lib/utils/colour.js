"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hueToRGB = hueToRGB;
exports.textToHtmlRainbow = textToHtmlRainbow;

/*
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function hueToRGB(h, s, l) {
  const c = s * (1 - Math.abs(2 * l - 1));
  const x = c * (1 - Math.abs(h / 60 % 2 - 1));
  const m = l - c / 2;
  let r = 0;
  let g = 0;
  let b = 0;

  if (0 <= h && h < 60) {
    r = c;
    g = x;
    b = 0;
  } else if (60 <= h && h < 120) {
    r = x;
    g = c;
    b = 0;
  } else if (120 <= h && h < 180) {
    r = 0;
    g = c;
    b = x;
  } else if (180 <= h && h < 240) {
    r = 0;
    g = x;
    b = c;
  } else if (240 <= h && h < 300) {
    r = x;
    g = 0;
    b = c;
  } else if (300 <= h && h < 360) {
    r = c;
    g = 0;
    b = x;
  }

  return [Math.round((r + m) * 255), Math.round((g + m) * 255), Math.round((b + m) * 255)];
}

function textToHtmlRainbow(str) {
  const frequency = 360 / str.length;
  return Array.from(str).map((c, i) => {
    const [r, g, b] = hueToRGB(i * frequency, 1.0, 0.5);
    return '<font color="#' + r.toString(16).padStart(2, "0") + g.toString(16).padStart(2, "0") + b.toString(16).padStart(2, "0") + '">' + c + '</font>';
  }).join("");
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9jb2xvdXIuanMiXSwibmFtZXMiOlsiaHVlVG9SR0IiLCJoIiwicyIsImwiLCJjIiwiTWF0aCIsImFicyIsIngiLCJtIiwiciIsImciLCJiIiwicm91bmQiLCJ0ZXh0VG9IdG1sUmFpbmJvdyIsInN0ciIsImZyZXF1ZW5jeSIsImxlbmd0aCIsIkFycmF5IiwiZnJvbSIsIm1hcCIsImkiLCJ0b1N0cmluZyIsInBhZFN0YXJ0Iiwiam9pbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JPLFNBQVNBLFFBQVQsQ0FBa0JDLENBQWxCLEVBQXFCQyxDQUFyQixFQUF3QkMsQ0FBeEIsRUFBMkI7QUFDOUIsUUFBTUMsQ0FBQyxHQUFHRixDQUFDLElBQUksSUFBSUcsSUFBSSxDQUFDQyxHQUFMLENBQVMsSUFBSUgsQ0FBSixHQUFRLENBQWpCLENBQVIsQ0FBWDtBQUNBLFFBQU1JLENBQUMsR0FBR0gsQ0FBQyxJQUFJLElBQUlDLElBQUksQ0FBQ0MsR0FBTCxDQUFVTCxDQUFDLEdBQUcsRUFBTCxHQUFXLENBQVgsR0FBZSxDQUF4QixDQUFSLENBQVg7QUFDQSxRQUFNTyxDQUFDLEdBQUdMLENBQUMsR0FBR0MsQ0FBQyxHQUFHLENBQWxCO0FBRUEsTUFBSUssQ0FBQyxHQUFHLENBQVI7QUFDQSxNQUFJQyxDQUFDLEdBQUcsQ0FBUjtBQUNBLE1BQUlDLENBQUMsR0FBRyxDQUFSOztBQUVBLE1BQUksS0FBS1YsQ0FBTCxJQUFVQSxDQUFDLEdBQUcsRUFBbEIsRUFBc0I7QUFDbEJRLElBQUFBLENBQUMsR0FBR0wsQ0FBSjtBQUNBTSxJQUFBQSxDQUFDLEdBQUdILENBQUo7QUFDQUksSUFBQUEsQ0FBQyxHQUFHLENBQUo7QUFDSCxHQUpELE1BSU8sSUFBSSxNQUFNVixDQUFOLElBQVdBLENBQUMsR0FBRyxHQUFuQixFQUF3QjtBQUMzQlEsSUFBQUEsQ0FBQyxHQUFHRixDQUFKO0FBQ0FHLElBQUFBLENBQUMsR0FBR04sQ0FBSjtBQUNBTyxJQUFBQSxDQUFDLEdBQUcsQ0FBSjtBQUNILEdBSk0sTUFJQSxJQUFJLE9BQU9WLENBQVAsSUFBWUEsQ0FBQyxHQUFHLEdBQXBCLEVBQXlCO0FBQzVCUSxJQUFBQSxDQUFDLEdBQUcsQ0FBSjtBQUNBQyxJQUFBQSxDQUFDLEdBQUdOLENBQUo7QUFDQU8sSUFBQUEsQ0FBQyxHQUFHSixDQUFKO0FBQ0gsR0FKTSxNQUlBLElBQUksT0FBT04sQ0FBUCxJQUFZQSxDQUFDLEdBQUcsR0FBcEIsRUFBeUI7QUFDNUJRLElBQUFBLENBQUMsR0FBRyxDQUFKO0FBQ0FDLElBQUFBLENBQUMsR0FBR0gsQ0FBSjtBQUNBSSxJQUFBQSxDQUFDLEdBQUdQLENBQUo7QUFDSCxHQUpNLE1BSUEsSUFBSSxPQUFPSCxDQUFQLElBQVlBLENBQUMsR0FBRyxHQUFwQixFQUF5QjtBQUM1QlEsSUFBQUEsQ0FBQyxHQUFHRixDQUFKO0FBQ0FHLElBQUFBLENBQUMsR0FBRyxDQUFKO0FBQ0FDLElBQUFBLENBQUMsR0FBR1AsQ0FBSjtBQUNILEdBSk0sTUFJQSxJQUFJLE9BQU9ILENBQVAsSUFBWUEsQ0FBQyxHQUFHLEdBQXBCLEVBQXlCO0FBQzVCUSxJQUFBQSxDQUFDLEdBQUdMLENBQUo7QUFDQU0sSUFBQUEsQ0FBQyxHQUFHLENBQUo7QUFDQUMsSUFBQUEsQ0FBQyxHQUFHSixDQUFKO0FBQ0g7O0FBRUQsU0FBTyxDQUFDRixJQUFJLENBQUNPLEtBQUwsQ0FBVyxDQUFDSCxDQUFDLEdBQUdELENBQUwsSUFBVSxHQUFyQixDQUFELEVBQTRCSCxJQUFJLENBQUNPLEtBQUwsQ0FBVyxDQUFDRixDQUFDLEdBQUdGLENBQUwsSUFBVSxHQUFyQixDQUE1QixFQUF1REgsSUFBSSxDQUFDTyxLQUFMLENBQVcsQ0FBQ0QsQ0FBQyxHQUFHSCxDQUFMLElBQVUsR0FBckIsQ0FBdkQsQ0FBUDtBQUNIOztBQUdNLFNBQVNLLGlCQUFULENBQTJCQyxHQUEzQixFQUFnQztBQUNuQyxRQUFNQyxTQUFTLEdBQUcsTUFBTUQsR0FBRyxDQUFDRSxNQUE1QjtBQUVBLFNBQU9DLEtBQUssQ0FBQ0MsSUFBTixDQUFXSixHQUFYLEVBQWdCSyxHQUFoQixDQUFvQixDQUFDZixDQUFELEVBQUlnQixDQUFKLEtBQVU7QUFDakMsVUFBTSxDQUFDWCxDQUFELEVBQUlDLENBQUosRUFBT0MsQ0FBUCxJQUFZWCxRQUFRLENBQUNvQixDQUFDLEdBQUdMLFNBQUwsRUFBZ0IsR0FBaEIsRUFBcUIsR0FBckIsQ0FBMUI7QUFDQSxXQUFPLG1CQUNITixDQUFDLENBQUNZLFFBQUYsQ0FBVyxFQUFYLEVBQWVDLFFBQWYsQ0FBd0IsQ0FBeEIsRUFBMkIsR0FBM0IsQ0FERyxHQUVIWixDQUFDLENBQUNXLFFBQUYsQ0FBVyxFQUFYLEVBQWVDLFFBQWYsQ0FBd0IsQ0FBeEIsRUFBMkIsR0FBM0IsQ0FGRyxHQUdIWCxDQUFDLENBQUNVLFFBQUYsQ0FBVyxFQUFYLEVBQWVDLFFBQWYsQ0FBd0IsQ0FBeEIsRUFBMkIsR0FBM0IsQ0FIRyxHQUlILElBSkcsR0FJSWxCLENBSkosR0FJUSxTQUpmO0FBS0gsR0FQTSxFQU9KbUIsSUFQSSxDQU9DLEVBUEQsQ0FBUDtBQVFIIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGh1ZVRvUkdCKGgsIHMsIGwpIHtcclxuICAgIGNvbnN0IGMgPSBzICogKDEgLSBNYXRoLmFicygyICogbCAtIDEpKTtcclxuICAgIGNvbnN0IHggPSBjICogKDEgLSBNYXRoLmFicygoaCAvIDYwKSAlIDIgLSAxKSk7XHJcbiAgICBjb25zdCBtID0gbCAtIGMgLyAyO1xyXG5cclxuICAgIGxldCByID0gMDtcclxuICAgIGxldCBnID0gMDtcclxuICAgIGxldCBiID0gMDtcclxuXHJcbiAgICBpZiAoMCA8PSBoICYmIGggPCA2MCkge1xyXG4gICAgICAgIHIgPSBjO1xyXG4gICAgICAgIGcgPSB4O1xyXG4gICAgICAgIGIgPSAwO1xyXG4gICAgfSBlbHNlIGlmICg2MCA8PSBoICYmIGggPCAxMjApIHtcclxuICAgICAgICByID0geDtcclxuICAgICAgICBnID0gYztcclxuICAgICAgICBiID0gMDtcclxuICAgIH0gZWxzZSBpZiAoMTIwIDw9IGggJiYgaCA8IDE4MCkge1xyXG4gICAgICAgIHIgPSAwO1xyXG4gICAgICAgIGcgPSBjO1xyXG4gICAgICAgIGIgPSB4O1xyXG4gICAgfSBlbHNlIGlmICgxODAgPD0gaCAmJiBoIDwgMjQwKSB7XHJcbiAgICAgICAgciA9IDA7XHJcbiAgICAgICAgZyA9IHg7XHJcbiAgICAgICAgYiA9IGM7XHJcbiAgICB9IGVsc2UgaWYgKDI0MCA8PSBoICYmIGggPCAzMDApIHtcclxuICAgICAgICByID0geDtcclxuICAgICAgICBnID0gMDtcclxuICAgICAgICBiID0gYztcclxuICAgIH0gZWxzZSBpZiAoMzAwIDw9IGggJiYgaCA8IDM2MCkge1xyXG4gICAgICAgIHIgPSBjO1xyXG4gICAgICAgIGcgPSAwO1xyXG4gICAgICAgIGIgPSB4O1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBbTWF0aC5yb3VuZCgociArIG0pICogMjU1KSwgTWF0aC5yb3VuZCgoZyArIG0pICogMjU1KSwgTWF0aC5yb3VuZCgoYiArIG0pICogMjU1KV07XHJcbn1cclxuXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdGV4dFRvSHRtbFJhaW5ib3coc3RyKSB7XHJcbiAgICBjb25zdCBmcmVxdWVuY3kgPSAzNjAgLyBzdHIubGVuZ3RoO1xyXG5cclxuICAgIHJldHVybiBBcnJheS5mcm9tKHN0cikubWFwKChjLCBpKSA9PiB7XHJcbiAgICAgICAgY29uc3QgW3IsIGcsIGJdID0gaHVlVG9SR0IoaSAqIGZyZXF1ZW5jeSwgMS4wLCAwLjUpO1xyXG4gICAgICAgIHJldHVybiAnPGZvbnQgY29sb3I9XCIjJyArXHJcbiAgICAgICAgICAgIHIudG9TdHJpbmcoMTYpLnBhZFN0YXJ0KDIsIFwiMFwiKSArXHJcbiAgICAgICAgICAgIGcudG9TdHJpbmcoMTYpLnBhZFN0YXJ0KDIsIFwiMFwiKSArXHJcbiAgICAgICAgICAgIGIudG9TdHJpbmcoMTYpLnBhZFN0YXJ0KDIsIFwiMFwiKSArXHJcbiAgICAgICAgICAgICdcIj4nICsgYyArICc8L2ZvbnQ+JztcclxuICAgIH0pLmpvaW4oXCJcIik7XHJcbn1cclxuIl19