"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MatrixClientPeg = require("../MatrixClientPeg");

var _UserAddress = require("../UserAddress");

var _GroupStore = _interopRequireDefault(require("../stores/GroupStore"));

var _languageHandler = require("../languageHandler");

var sdk = _interopRequireWildcard(require("../index"));

var _Modal = _interopRequireDefault(require("../Modal"));

var _SettingsStore = _interopRequireDefault(require("../settings/SettingsStore"));

var _promise = require("./promise");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Invites multiple addresses to a room or group, handling rate limiting from the server
 */
class MultiInviter {
  /**
   * @param {string} targetId The ID of the room or group to invite to
   */
  constructor(targetId) {
    if (targetId[0] === '+') {
      this.roomId = null;
      this.groupId = targetId;
    } else {
      this.roomId = targetId;
      this.groupId = null;
    }

    this.canceled = false;
    this.addrs = [];
    this.busy = false;
    this.completionStates = {}; // State of each address (invited or error)

    this.errors = {}; // { address: {errorText, errcode} }

    this.deferred = null;
  }
  /**
   * Invite users to this room. This may only be called once per
   * instance of the class.
   *
   * @param {array} addrs Array of addresses to invite
   * @returns {Promise} Resolved when all invitations in the queue are complete
   */


  invite(addrs) {
    if (this.addrs.length > 0) {
      throw new Error("Already inviting/invited");
    }

    this.addrs.push(...addrs);

    for (const addr of this.addrs) {
      if ((0, _UserAddress.getAddressType)(addr) === null) {
        this.completionStates[addr] = 'error';
        this.errors[addr] = {
          errcode: 'M_INVALID',
          errorText: (0, _languageHandler._t)('Unrecognised address')
        };
      }
    }

    this.deferred = (0, _promise.defer)();

    this._inviteMore(0);

    return this.deferred.promise;
  }
  /**
   * Stops inviting. Causes promises returned by invite() to be rejected.
   */


  cancel() {
    if (!this.busy) return;
    this._canceled = true;
    this.deferred.reject(new Error('canceled'));
  }

  getCompletionState(addr) {
    return this.completionStates[addr];
  }

  getErrorText(addr) {
    return this.errors[addr] ? this.errors[addr].errorText : null;
  }

  async _inviteToRoom(roomId, addr, ignoreProfile) {
    const addrType = (0, _UserAddress.getAddressType)(addr);

    if (addrType === 'email') {
      return _MatrixClientPeg.MatrixClientPeg.get().inviteByEmail(roomId, addr);
    } else if (addrType === 'mx-user-id') {
      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

      if (!room) throw new Error("Room not found");
      const member = room.getMember(addr);

      if (member && ['join', 'invite'].includes(member.membership)) {
        throw {
          errcode: "RIOT.ALREADY_IN_ROOM",
          error: "Member already invited"
        };
      }

      if (!ignoreProfile && _SettingsStore.default.getValue("promptBeforeInviteUnknownUsers", this.roomId)) {
        try {
          const profile = await _MatrixClientPeg.MatrixClientPeg.get().getProfileInfo(addr);

          if (!profile) {
            // noinspection ExceptionCaughtLocallyJS
            throw new Error("User has no profile");
          }
        } catch (e) {
          throw {
            errcode: "RIOT.USER_NOT_FOUND",
            error: "User does not have a profile or does not exist."
          };
        }
      }

      return _MatrixClientPeg.MatrixClientPeg.get().invite(roomId, addr);
    } else {
      throw new Error('Unsupported address');
    }
  }

  _doInvite(address, ignoreProfile) {
    return new Promise((resolve, reject) => {
      console.log("Inviting ".concat(address));
      let doInvite;

      if (this.groupId !== null) {
        doInvite = _GroupStore.default.inviteUserToGroup(this.groupId, address);
      } else {
        doInvite = this._inviteToRoom(this.roomId, address, ignoreProfile);
      }

      doInvite.then(() => {
        if (this._canceled) {
          return;
        }

        this.completionStates[address] = 'invited';
        delete this.errors[address];
        resolve();
      }).catch(err => {
        if (this._canceled) {
          return;
        }

        console.error(err);
        let errorText;
        let fatal = false;

        if (err.errcode === 'M_FORBIDDEN') {
          fatal = true;
          errorText = (0, _languageHandler._t)('You do not have permission to invite people to this room.');
        } else if (err.errcode === "RIOT.ALREADY_IN_ROOM") {
          errorText = (0, _languageHandler._t)("User %(userId)s is already in the room", {
            userId: address
          });
        } else if (err.errcode === 'M_LIMIT_EXCEEDED') {
          // we're being throttled so wait a bit & try again
          setTimeout(() => {
            this._doInvite(address, ignoreProfile).then(resolve, reject);
          }, 5000);
          return;
        } else if (['M_NOT_FOUND', 'M_USER_NOT_FOUND', 'RIOT.USER_NOT_FOUND'].includes(err.errcode)) {
          errorText = (0, _languageHandler._t)("User %(user_id)s does not exist", {
            user_id: address
          });
        } else if (err.errcode === 'M_PROFILE_UNDISCLOSED') {
          errorText = (0, _languageHandler._t)("User %(user_id)s may or may not exist", {
            user_id: address
          });
        } else if (err.errcode === 'M_PROFILE_NOT_FOUND' && !ignoreProfile) {
          // Invite without the profile check
          console.warn("User ".concat(address, " does not have a profile - inviting anyways automatically"));

          this._doInvite(address, true).then(resolve, reject);
        } else if (err.errcode === "M_BAD_STATE") {
          errorText = (0, _languageHandler._t)("The user must be unbanned before they can be invited.");
        } else if (err.errcode === "M_UNSUPPORTED_ROOM_VERSION") {
          errorText = (0, _languageHandler._t)("The user's homeserver does not support the version of the room.");
        } else {
          errorText = (0, _languageHandler._t)('Unknown server error');
        }

        this.completionStates[address] = 'error';
        this.errors[address] = {
          errorText,
          errcode: err.errcode
        };
        this.busy = !fatal;
        this.fatal = fatal;

        if (fatal) {
          reject();
        } else {
          resolve();
        }
      });
    });
  }

  _inviteMore(nextIndex, ignoreProfile) {
    if (this._canceled) {
      return;
    }

    if (nextIndex === this.addrs.length) {
      this.busy = false;

      if (Object.keys(this.errors).length > 0 && !this.groupId) {
        // There were problems inviting some people - see if we can invite them
        // without caring if they exist or not.
        const unknownProfileErrors = ['M_NOT_FOUND', 'M_USER_NOT_FOUND', 'M_PROFILE_UNDISCLOSED', 'M_PROFILE_NOT_FOUND', 'RIOT.USER_NOT_FOUND'];
        const unknownProfileUsers = Object.keys(this.errors).filter(a => unknownProfileErrors.includes(this.errors[a].errcode));

        if (unknownProfileUsers.length > 0) {
          const inviteUnknowns = () => {
            const promises = unknownProfileUsers.map(u => this._doInvite(u, true));
            Promise.all(promises).then(() => this.deferred.resolve(this.completionStates));
          };

          if (!_SettingsStore.default.getValue("promptBeforeInviteUnknownUsers", this.roomId)) {
            inviteUnknowns();
            return;
          }

          const AskInviteAnywayDialog = sdk.getComponent("dialogs.AskInviteAnywayDialog");
          console.log("Showing failed to invite dialog...");

          _Modal.default.createTrackedDialog('Failed to invite the following users to the room', '', AskInviteAnywayDialog, {
            unknownProfileUsers: unknownProfileUsers.map(u => {
              return {
                userId: u,
                errorText: this.errors[u].errorText
              };
            }),
            onInviteAnyways: () => inviteUnknowns(),
            onGiveUp: () => {
              // Fake all the completion states because we already warned the user
              for (const addr of unknownProfileUsers) {
                this.completionStates[addr] = 'invited';
              }

              this.deferred.resolve(this.completionStates);
            }
          });

          return;
        }
      }

      this.deferred.resolve(this.completionStates);
      return;
    }

    const addr = this.addrs[nextIndex]; // don't try to invite it if it's an invalid address
    // (it will already be marked as an error though,
    // so no need to do so again)

    if ((0, _UserAddress.getAddressType)(addr) === null) {
      this._inviteMore(nextIndex + 1);

      return;
    } // don't re-invite (there's no way in the UI to do this, but
    // for sanity's sake)


    if (this.completionStates[addr] === 'invited') {
      this._inviteMore(nextIndex + 1);

      return;
    }

    this._doInvite(addr, ignoreProfile).then(() => {
      this._inviteMore(nextIndex + 1, ignoreProfile);
    }).catch(() => this.deferred.resolve(this.completionStates));
  }

}

exports.default = MultiInviter;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9NdWx0aUludml0ZXIuanMiXSwibmFtZXMiOlsiTXVsdGlJbnZpdGVyIiwiY29uc3RydWN0b3IiLCJ0YXJnZXRJZCIsInJvb21JZCIsImdyb3VwSWQiLCJjYW5jZWxlZCIsImFkZHJzIiwiYnVzeSIsImNvbXBsZXRpb25TdGF0ZXMiLCJlcnJvcnMiLCJkZWZlcnJlZCIsImludml0ZSIsImxlbmd0aCIsIkVycm9yIiwicHVzaCIsImFkZHIiLCJlcnJjb2RlIiwiZXJyb3JUZXh0IiwiX2ludml0ZU1vcmUiLCJwcm9taXNlIiwiY2FuY2VsIiwiX2NhbmNlbGVkIiwicmVqZWN0IiwiZ2V0Q29tcGxldGlvblN0YXRlIiwiZ2V0RXJyb3JUZXh0IiwiX2ludml0ZVRvUm9vbSIsImlnbm9yZVByb2ZpbGUiLCJhZGRyVHlwZSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImludml0ZUJ5RW1haWwiLCJyb29tIiwiZ2V0Um9vbSIsIm1lbWJlciIsImdldE1lbWJlciIsImluY2x1ZGVzIiwibWVtYmVyc2hpcCIsImVycm9yIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwicHJvZmlsZSIsImdldFByb2ZpbGVJbmZvIiwiZSIsIl9kb0ludml0ZSIsImFkZHJlc3MiLCJQcm9taXNlIiwicmVzb2x2ZSIsImNvbnNvbGUiLCJsb2ciLCJkb0ludml0ZSIsIkdyb3VwU3RvcmUiLCJpbnZpdGVVc2VyVG9Hcm91cCIsInRoZW4iLCJjYXRjaCIsImVyciIsImZhdGFsIiwidXNlcklkIiwic2V0VGltZW91dCIsInVzZXJfaWQiLCJ3YXJuIiwibmV4dEluZGV4IiwiT2JqZWN0Iiwia2V5cyIsInVua25vd25Qcm9maWxlRXJyb3JzIiwidW5rbm93blByb2ZpbGVVc2VycyIsImZpbHRlciIsImEiLCJpbnZpdGVVbmtub3ducyIsInByb21pc2VzIiwibWFwIiwidSIsImFsbCIsIkFza0ludml0ZUFueXdheURpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsIm9uSW52aXRlQW55d2F5cyIsIm9uR2l2ZVVwIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWlCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUF4QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBOzs7QUFHZSxNQUFNQSxZQUFOLENBQW1CO0FBQzlCOzs7QUFHQUMsRUFBQUEsV0FBVyxDQUFDQyxRQUFELEVBQVc7QUFDbEIsUUFBSUEsUUFBUSxDQUFDLENBQUQsQ0FBUixLQUFnQixHQUFwQixFQUF5QjtBQUNyQixXQUFLQyxNQUFMLEdBQWMsSUFBZDtBQUNBLFdBQUtDLE9BQUwsR0FBZUYsUUFBZjtBQUNILEtBSEQsTUFHTztBQUNILFdBQUtDLE1BQUwsR0FBY0QsUUFBZDtBQUNBLFdBQUtFLE9BQUwsR0FBZSxJQUFmO0FBQ0g7O0FBRUQsU0FBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFNBQUtDLEtBQUwsR0FBYSxFQUFiO0FBQ0EsU0FBS0MsSUFBTCxHQUFZLEtBQVo7QUFDQSxTQUFLQyxnQkFBTCxHQUF3QixFQUF4QixDQVprQixDQVlVOztBQUM1QixTQUFLQyxNQUFMLEdBQWMsRUFBZCxDQWJrQixDQWFBOztBQUNsQixTQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0g7QUFFRDs7Ozs7Ozs7O0FBT0FDLEVBQUFBLE1BQU0sQ0FBQ0wsS0FBRCxFQUFRO0FBQ1YsUUFBSSxLQUFLQSxLQUFMLENBQVdNLE1BQVgsR0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsWUFBTSxJQUFJQyxLQUFKLENBQVUsMEJBQVYsQ0FBTjtBQUNIOztBQUNELFNBQUtQLEtBQUwsQ0FBV1EsSUFBWCxDQUFnQixHQUFHUixLQUFuQjs7QUFFQSxTQUFLLE1BQU1TLElBQVgsSUFBbUIsS0FBS1QsS0FBeEIsRUFBK0I7QUFDM0IsVUFBSSxpQ0FBZVMsSUFBZixNQUF5QixJQUE3QixFQUFtQztBQUMvQixhQUFLUCxnQkFBTCxDQUFzQk8sSUFBdEIsSUFBOEIsT0FBOUI7QUFDQSxhQUFLTixNQUFMLENBQVlNLElBQVosSUFBb0I7QUFDaEJDLFVBQUFBLE9BQU8sRUFBRSxXQURPO0FBRWhCQyxVQUFBQSxTQUFTLEVBQUUseUJBQUcsc0JBQUg7QUFGSyxTQUFwQjtBQUlIO0FBQ0o7O0FBQ0QsU0FBS1AsUUFBTCxHQUFnQixxQkFBaEI7O0FBQ0EsU0FBS1EsV0FBTCxDQUFpQixDQUFqQjs7QUFFQSxXQUFPLEtBQUtSLFFBQUwsQ0FBY1MsT0FBckI7QUFDSDtBQUVEOzs7OztBQUdBQyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxRQUFJLENBQUMsS0FBS2IsSUFBVixFQUFnQjtBQUVoQixTQUFLYyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsU0FBS1gsUUFBTCxDQUFjWSxNQUFkLENBQXFCLElBQUlULEtBQUosQ0FBVSxVQUFWLENBQXJCO0FBQ0g7O0FBRURVLEVBQUFBLGtCQUFrQixDQUFDUixJQUFELEVBQU87QUFDckIsV0FBTyxLQUFLUCxnQkFBTCxDQUFzQk8sSUFBdEIsQ0FBUDtBQUNIOztBQUVEUyxFQUFBQSxZQUFZLENBQUNULElBQUQsRUFBTztBQUNmLFdBQU8sS0FBS04sTUFBTCxDQUFZTSxJQUFaLElBQW9CLEtBQUtOLE1BQUwsQ0FBWU0sSUFBWixFQUFrQkUsU0FBdEMsR0FBa0QsSUFBekQ7QUFDSDs7QUFFRCxRQUFNUSxhQUFOLENBQW9CdEIsTUFBcEIsRUFBNEJZLElBQTVCLEVBQWtDVyxhQUFsQyxFQUFpRDtBQUM3QyxVQUFNQyxRQUFRLEdBQUcsaUNBQWVaLElBQWYsQ0FBakI7O0FBRUEsUUFBSVksUUFBUSxLQUFLLE9BQWpCLEVBQTBCO0FBQ3RCLGFBQU9DLGlDQUFnQkMsR0FBaEIsR0FBc0JDLGFBQXRCLENBQW9DM0IsTUFBcEMsRUFBNENZLElBQTVDLENBQVA7QUFDSCxLQUZELE1BRU8sSUFBSVksUUFBUSxLQUFLLFlBQWpCLEVBQStCO0FBQ2xDLFlBQU1JLElBQUksR0FBR0gsaUNBQWdCQyxHQUFoQixHQUFzQkcsT0FBdEIsQ0FBOEI3QixNQUE5QixDQUFiOztBQUNBLFVBQUksQ0FBQzRCLElBQUwsRUFBVyxNQUFNLElBQUlsQixLQUFKLENBQVUsZ0JBQVYsQ0FBTjtBQUVYLFlBQU1vQixNQUFNLEdBQUdGLElBQUksQ0FBQ0csU0FBTCxDQUFlbkIsSUFBZixDQUFmOztBQUNBLFVBQUlrQixNQUFNLElBQUksQ0FBQyxNQUFELEVBQVMsUUFBVCxFQUFtQkUsUUFBbkIsQ0FBNEJGLE1BQU0sQ0FBQ0csVUFBbkMsQ0FBZCxFQUE4RDtBQUMxRCxjQUFNO0FBQUNwQixVQUFBQSxPQUFPLEVBQUUsc0JBQVY7QUFBa0NxQixVQUFBQSxLQUFLLEVBQUU7QUFBekMsU0FBTjtBQUNIOztBQUVELFVBQUksQ0FBQ1gsYUFBRCxJQUFrQlksdUJBQWNDLFFBQWQsQ0FBdUIsZ0NBQXZCLEVBQXlELEtBQUtwQyxNQUE5RCxDQUF0QixFQUE2RjtBQUN6RixZQUFJO0FBQ0EsZ0JBQU1xQyxPQUFPLEdBQUcsTUFBTVosaUNBQWdCQyxHQUFoQixHQUFzQlksY0FBdEIsQ0FBcUMxQixJQUFyQyxDQUF0Qjs7QUFDQSxjQUFJLENBQUN5QixPQUFMLEVBQWM7QUFDVjtBQUNBLGtCQUFNLElBQUkzQixLQUFKLENBQVUscUJBQVYsQ0FBTjtBQUNIO0FBQ0osU0FORCxDQU1FLE9BQU82QixDQUFQLEVBQVU7QUFDUixnQkFBTTtBQUNGMUIsWUFBQUEsT0FBTyxFQUFFLHFCQURQO0FBRUZxQixZQUFBQSxLQUFLLEVBQUU7QUFGTCxXQUFOO0FBSUg7QUFDSjs7QUFFRCxhQUFPVCxpQ0FBZ0JDLEdBQWhCLEdBQXNCbEIsTUFBdEIsQ0FBNkJSLE1BQTdCLEVBQXFDWSxJQUFyQyxDQUFQO0FBQ0gsS0F6Qk0sTUF5QkE7QUFDSCxZQUFNLElBQUlGLEtBQUosQ0FBVSxxQkFBVixDQUFOO0FBQ0g7QUFDSjs7QUFFRDhCLEVBQUFBLFNBQVMsQ0FBQ0MsT0FBRCxFQUFVbEIsYUFBVixFQUF5QjtBQUM5QixXQUFPLElBQUltQixPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVeEIsTUFBVixLQUFxQjtBQUNwQ3lCLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixvQkFBd0JKLE9BQXhCO0FBRUEsVUFBSUssUUFBSjs7QUFDQSxVQUFJLEtBQUs3QyxPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3ZCNkMsUUFBQUEsUUFBUSxHQUFHQyxvQkFBV0MsaUJBQVgsQ0FBNkIsS0FBSy9DLE9BQWxDLEVBQTJDd0MsT0FBM0MsQ0FBWDtBQUNILE9BRkQsTUFFTztBQUNISyxRQUFBQSxRQUFRLEdBQUcsS0FBS3hCLGFBQUwsQ0FBbUIsS0FBS3RCLE1BQXhCLEVBQWdDeUMsT0FBaEMsRUFBeUNsQixhQUF6QyxDQUFYO0FBQ0g7O0FBRUR1QixNQUFBQSxRQUFRLENBQUNHLElBQVQsQ0FBYyxNQUFNO0FBQ2hCLFlBQUksS0FBSy9CLFNBQVQsRUFBb0I7QUFDaEI7QUFDSDs7QUFFRCxhQUFLYixnQkFBTCxDQUFzQm9DLE9BQXRCLElBQWlDLFNBQWpDO0FBQ0EsZUFBTyxLQUFLbkMsTUFBTCxDQUFZbUMsT0FBWixDQUFQO0FBRUFFLFFBQUFBLE9BQU87QUFDVixPQVRELEVBU0dPLEtBVEgsQ0FTVUMsR0FBRCxJQUFTO0FBQ2QsWUFBSSxLQUFLakMsU0FBVCxFQUFvQjtBQUNoQjtBQUNIOztBQUVEMEIsUUFBQUEsT0FBTyxDQUFDVixLQUFSLENBQWNpQixHQUFkO0FBRUEsWUFBSXJDLFNBQUo7QUFDQSxZQUFJc0MsS0FBSyxHQUFHLEtBQVo7O0FBQ0EsWUFBSUQsR0FBRyxDQUFDdEMsT0FBSixLQUFnQixhQUFwQixFQUFtQztBQUMvQnVDLFVBQUFBLEtBQUssR0FBRyxJQUFSO0FBQ0F0QyxVQUFBQSxTQUFTLEdBQUcseUJBQUcsMkRBQUgsQ0FBWjtBQUNILFNBSEQsTUFHTyxJQUFJcUMsR0FBRyxDQUFDdEMsT0FBSixLQUFnQixzQkFBcEIsRUFBNEM7QUFDL0NDLFVBQUFBLFNBQVMsR0FBRyx5QkFBRyx3Q0FBSCxFQUE2QztBQUFDdUMsWUFBQUEsTUFBTSxFQUFFWjtBQUFULFdBQTdDLENBQVo7QUFDSCxTQUZNLE1BRUEsSUFBSVUsR0FBRyxDQUFDdEMsT0FBSixLQUFnQixrQkFBcEIsRUFBd0M7QUFDM0M7QUFDQXlDLFVBQUFBLFVBQVUsQ0FBQyxNQUFNO0FBQ2IsaUJBQUtkLFNBQUwsQ0FBZUMsT0FBZixFQUF3QmxCLGFBQXhCLEVBQXVDMEIsSUFBdkMsQ0FBNENOLE9BQTVDLEVBQXFEeEIsTUFBckQ7QUFDSCxXQUZTLEVBRVAsSUFGTyxDQUFWO0FBR0E7QUFDSCxTQU5NLE1BTUEsSUFBSSxDQUFDLGFBQUQsRUFBZ0Isa0JBQWhCLEVBQW9DLHFCQUFwQyxFQUEyRGEsUUFBM0QsQ0FBb0VtQixHQUFHLENBQUN0QyxPQUF4RSxDQUFKLEVBQXNGO0FBQ3pGQyxVQUFBQSxTQUFTLEdBQUcseUJBQUcsaUNBQUgsRUFBc0M7QUFBQ3lDLFlBQUFBLE9BQU8sRUFBRWQ7QUFBVixXQUF0QyxDQUFaO0FBQ0gsU0FGTSxNQUVBLElBQUlVLEdBQUcsQ0FBQ3RDLE9BQUosS0FBZ0IsdUJBQXBCLEVBQTZDO0FBQ2hEQyxVQUFBQSxTQUFTLEdBQUcseUJBQUcsdUNBQUgsRUFBNEM7QUFBQ3lDLFlBQUFBLE9BQU8sRUFBRWQ7QUFBVixXQUE1QyxDQUFaO0FBQ0gsU0FGTSxNQUVBLElBQUlVLEdBQUcsQ0FBQ3RDLE9BQUosS0FBZ0IscUJBQWhCLElBQXlDLENBQUNVLGFBQTlDLEVBQTZEO0FBQ2hFO0FBQ0FxQixVQUFBQSxPQUFPLENBQUNZLElBQVIsZ0JBQXFCZixPQUFyQjs7QUFDQSxlQUFLRCxTQUFMLENBQWVDLE9BQWYsRUFBd0IsSUFBeEIsRUFBOEJRLElBQTlCLENBQW1DTixPQUFuQyxFQUE0Q3hCLE1BQTVDO0FBQ0gsU0FKTSxNQUlBLElBQUlnQyxHQUFHLENBQUN0QyxPQUFKLEtBQWdCLGFBQXBCLEVBQW1DO0FBQ3RDQyxVQUFBQSxTQUFTLEdBQUcseUJBQUcsdURBQUgsQ0FBWjtBQUNILFNBRk0sTUFFQSxJQUFJcUMsR0FBRyxDQUFDdEMsT0FBSixLQUFnQiw0QkFBcEIsRUFBa0Q7QUFDckRDLFVBQUFBLFNBQVMsR0FBRyx5QkFBRyxpRUFBSCxDQUFaO0FBQ0gsU0FGTSxNQUVBO0FBQ0hBLFVBQUFBLFNBQVMsR0FBRyx5QkFBRyxzQkFBSCxDQUFaO0FBQ0g7O0FBRUQsYUFBS1QsZ0JBQUwsQ0FBc0JvQyxPQUF0QixJQUFpQyxPQUFqQztBQUNBLGFBQUtuQyxNQUFMLENBQVltQyxPQUFaLElBQXVCO0FBQUMzQixVQUFBQSxTQUFEO0FBQVlELFVBQUFBLE9BQU8sRUFBRXNDLEdBQUcsQ0FBQ3RDO0FBQXpCLFNBQXZCO0FBRUEsYUFBS1QsSUFBTCxHQUFZLENBQUNnRCxLQUFiO0FBQ0EsYUFBS0EsS0FBTCxHQUFhQSxLQUFiOztBQUVBLFlBQUlBLEtBQUosRUFBVztBQUNQakMsVUFBQUEsTUFBTTtBQUNULFNBRkQsTUFFTztBQUNId0IsVUFBQUEsT0FBTztBQUNWO0FBQ0osT0F4REQ7QUF5REgsS0FuRU0sQ0FBUDtBQW9FSDs7QUFFRDVCLEVBQUFBLFdBQVcsQ0FBQzBDLFNBQUQsRUFBWWxDLGFBQVosRUFBMkI7QUFDbEMsUUFBSSxLQUFLTCxTQUFULEVBQW9CO0FBQ2hCO0FBQ0g7O0FBRUQsUUFBSXVDLFNBQVMsS0FBSyxLQUFLdEQsS0FBTCxDQUFXTSxNQUE3QixFQUFxQztBQUNqQyxXQUFLTCxJQUFMLEdBQVksS0FBWjs7QUFDQSxVQUFJc0QsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBS3JELE1BQWpCLEVBQXlCRyxNQUF6QixHQUFrQyxDQUFsQyxJQUF1QyxDQUFDLEtBQUtSLE9BQWpELEVBQTBEO0FBQ3REO0FBQ0E7QUFDQSxjQUFNMkQsb0JBQW9CLEdBQUcsQ0FBQyxhQUFELEVBQWdCLGtCQUFoQixFQUFvQyx1QkFBcEMsRUFBNkQscUJBQTdELEVBQW9GLHFCQUFwRixDQUE3QjtBQUNBLGNBQU1DLG1CQUFtQixHQUFHSCxNQUFNLENBQUNDLElBQVAsQ0FBWSxLQUFLckQsTUFBakIsRUFBeUJ3RCxNQUF6QixDQUFnQ0MsQ0FBQyxJQUFJSCxvQkFBb0IsQ0FBQzVCLFFBQXJCLENBQThCLEtBQUsxQixNQUFMLENBQVl5RCxDQUFaLEVBQWVsRCxPQUE3QyxDQUFyQyxDQUE1Qjs7QUFFQSxZQUFJZ0QsbUJBQW1CLENBQUNwRCxNQUFwQixHQUE2QixDQUFqQyxFQUFvQztBQUNoQyxnQkFBTXVELGNBQWMsR0FBRyxNQUFNO0FBQ3pCLGtCQUFNQyxRQUFRLEdBQUdKLG1CQUFtQixDQUFDSyxHQUFwQixDQUF3QkMsQ0FBQyxJQUFJLEtBQUszQixTQUFMLENBQWUyQixDQUFmLEVBQWtCLElBQWxCLENBQTdCLENBQWpCO0FBQ0F6QixZQUFBQSxPQUFPLENBQUMwQixHQUFSLENBQVlILFFBQVosRUFBc0JoQixJQUF0QixDQUEyQixNQUFNLEtBQUsxQyxRQUFMLENBQWNvQyxPQUFkLENBQXNCLEtBQUt0QyxnQkFBM0IsQ0FBakM7QUFDSCxXQUhEOztBQUtBLGNBQUksQ0FBQzhCLHVCQUFjQyxRQUFkLENBQXVCLGdDQUF2QixFQUF5RCxLQUFLcEMsTUFBOUQsQ0FBTCxFQUE0RTtBQUN4RWdFLFlBQUFBLGNBQWM7QUFDZDtBQUNIOztBQUVELGdCQUFNSyxxQkFBcUIsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUE5QjtBQUNBM0IsVUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksb0NBQVo7O0FBQ0EyQix5QkFBTUMsbUJBQU4sQ0FBMEIsa0RBQTFCLEVBQThFLEVBQTlFLEVBQWtGSixxQkFBbEYsRUFBeUc7QUFDckdSLFlBQUFBLG1CQUFtQixFQUFFQSxtQkFBbUIsQ0FBQ0ssR0FBcEIsQ0FBd0JDLENBQUMsSUFBSTtBQUFDLHFCQUFPO0FBQUNkLGdCQUFBQSxNQUFNLEVBQUVjLENBQVQ7QUFBWXJELGdCQUFBQSxTQUFTLEVBQUUsS0FBS1IsTUFBTCxDQUFZNkQsQ0FBWixFQUFlckQ7QUFBdEMsZUFBUDtBQUF5RCxhQUF2RixDQURnRjtBQUVyRzRELFlBQUFBLGVBQWUsRUFBRSxNQUFNVixjQUFjLEVBRmdFO0FBR3JHVyxZQUFBQSxRQUFRLEVBQUUsTUFBTTtBQUNaO0FBQ0EsbUJBQUssTUFBTS9ELElBQVgsSUFBbUJpRCxtQkFBbkIsRUFBd0M7QUFDcEMscUJBQUt4RCxnQkFBTCxDQUFzQk8sSUFBdEIsSUFBOEIsU0FBOUI7QUFDSDs7QUFDRCxtQkFBS0wsUUFBTCxDQUFjb0MsT0FBZCxDQUFzQixLQUFLdEMsZ0JBQTNCO0FBQ0g7QUFUb0csV0FBekc7O0FBV0E7QUFDSDtBQUNKOztBQUNELFdBQUtFLFFBQUwsQ0FBY29DLE9BQWQsQ0FBc0IsS0FBS3RDLGdCQUEzQjtBQUNBO0FBQ0g7O0FBRUQsVUFBTU8sSUFBSSxHQUFHLEtBQUtULEtBQUwsQ0FBV3NELFNBQVgsQ0FBYixDQTVDa0MsQ0E4Q2xDO0FBQ0E7QUFDQTs7QUFDQSxRQUFJLGlDQUFlN0MsSUFBZixNQUF5QixJQUE3QixFQUFtQztBQUMvQixXQUFLRyxXQUFMLENBQWlCMEMsU0FBUyxHQUFHLENBQTdCOztBQUNBO0FBQ0gsS0FwRGlDLENBc0RsQztBQUNBOzs7QUFDQSxRQUFJLEtBQUtwRCxnQkFBTCxDQUFzQk8sSUFBdEIsTUFBZ0MsU0FBcEMsRUFBK0M7QUFDM0MsV0FBS0csV0FBTCxDQUFpQjBDLFNBQVMsR0FBRyxDQUE3Qjs7QUFDQTtBQUNIOztBQUVELFNBQUtqQixTQUFMLENBQWU1QixJQUFmLEVBQXFCVyxhQUFyQixFQUFvQzBCLElBQXBDLENBQXlDLE1BQU07QUFDM0MsV0FBS2xDLFdBQUwsQ0FBaUIwQyxTQUFTLEdBQUcsQ0FBN0IsRUFBZ0NsQyxhQUFoQztBQUNILEtBRkQsRUFFRzJCLEtBRkgsQ0FFUyxNQUFNLEtBQUszQyxRQUFMLENBQWNvQyxPQUFkLENBQXNCLEtBQUt0QyxnQkFBM0IsQ0FGZjtBQUdIOztBQTdPNkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNywgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQge2dldEFkZHJlc3NUeXBlfSBmcm9tICcuLi9Vc2VyQWRkcmVzcyc7XHJcbmltcG9ydCBHcm91cFN0b3JlIGZyb20gJy4uL3N0b3Jlcy9Hcm91cFN0b3JlJztcclxuaW1wb3J0IHtfdH0gZnJvbSBcIi4uL2xhbmd1YWdlSGFuZGxlclwiO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSBcIi4uL2luZGV4XCI7XHJcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi4vTW9kYWxcIjtcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUgZnJvbSBcIi4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IHtkZWZlcn0gZnJvbSBcIi4vcHJvbWlzZVwiO1xyXG5cclxuLyoqXHJcbiAqIEludml0ZXMgbXVsdGlwbGUgYWRkcmVzc2VzIHRvIGEgcm9vbSBvciBncm91cCwgaGFuZGxpbmcgcmF0ZSBsaW1pdGluZyBmcm9tIHRoZSBzZXJ2ZXJcclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE11bHRpSW52aXRlciB7XHJcbiAgICAvKipcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0YXJnZXRJZCBUaGUgSUQgb2YgdGhlIHJvb20gb3IgZ3JvdXAgdG8gaW52aXRlIHRvXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHRhcmdldElkKSB7XHJcbiAgICAgICAgaWYgKHRhcmdldElkWzBdID09PSAnKycpIHtcclxuICAgICAgICAgICAgdGhpcy5yb29tSWQgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmdyb3VwSWQgPSB0YXJnZXRJZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnJvb21JZCA9IHRhcmdldElkO1xyXG4gICAgICAgICAgICB0aGlzLmdyb3VwSWQgPSBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jYW5jZWxlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuYWRkcnMgPSBbXTtcclxuICAgICAgICB0aGlzLmJ1c3kgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmNvbXBsZXRpb25TdGF0ZXMgPSB7fTsgLy8gU3RhdGUgb2YgZWFjaCBhZGRyZXNzIChpbnZpdGVkIG9yIGVycm9yKVxyXG4gICAgICAgIHRoaXMuZXJyb3JzID0ge307IC8vIHsgYWRkcmVzczoge2Vycm9yVGV4dCwgZXJyY29kZX0gfVxyXG4gICAgICAgIHRoaXMuZGVmZXJyZWQgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52aXRlIHVzZXJzIHRvIHRoaXMgcm9vbS4gVGhpcyBtYXkgb25seSBiZSBjYWxsZWQgb25jZSBwZXJcclxuICAgICAqIGluc3RhbmNlIG9mIHRoZSBjbGFzcy5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge2FycmF5fSBhZGRycyBBcnJheSBvZiBhZGRyZXNzZXMgdG8gaW52aXRlXHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX0gUmVzb2x2ZWQgd2hlbiBhbGwgaW52aXRhdGlvbnMgaW4gdGhlIHF1ZXVlIGFyZSBjb21wbGV0ZVxyXG4gICAgICovXHJcbiAgICBpbnZpdGUoYWRkcnMpIHtcclxuICAgICAgICBpZiAodGhpcy5hZGRycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkFscmVhZHkgaW52aXRpbmcvaW52aXRlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5hZGRycy5wdXNoKC4uLmFkZHJzKTtcclxuXHJcbiAgICAgICAgZm9yIChjb25zdCBhZGRyIG9mIHRoaXMuYWRkcnMpIHtcclxuICAgICAgICAgICAgaWYgKGdldEFkZHJlc3NUeXBlKGFkZHIpID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbXBsZXRpb25TdGF0ZXNbYWRkcl0gPSAnZXJyb3InO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvcnNbYWRkcl0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyY29kZTogJ01fSU5WQUxJRCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0OiBfdCgnVW5yZWNvZ25pc2VkIGFkZHJlc3MnKSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kZWZlcnJlZCA9IGRlZmVyKCk7XHJcbiAgICAgICAgdGhpcy5faW52aXRlTW9yZSgwKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVmZXJyZWQucHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0b3BzIGludml0aW5nLiBDYXVzZXMgcHJvbWlzZXMgcmV0dXJuZWQgYnkgaW52aXRlKCkgdG8gYmUgcmVqZWN0ZWQuXHJcbiAgICAgKi9cclxuICAgIGNhbmNlbCgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuYnVzeSkgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLl9jYW5jZWxlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kZWZlcnJlZC5yZWplY3QobmV3IEVycm9yKCdjYW5jZWxlZCcpKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb21wbGV0aW9uU3RhdGUoYWRkcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbXBsZXRpb25TdGF0ZXNbYWRkcl07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RXJyb3JUZXh0KGFkZHIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lcnJvcnNbYWRkcl0gPyB0aGlzLmVycm9yc1thZGRyXS5lcnJvclRleHQgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIF9pbnZpdGVUb1Jvb20ocm9vbUlkLCBhZGRyLCBpZ25vcmVQcm9maWxlKSB7XHJcbiAgICAgICAgY29uc3QgYWRkclR5cGUgPSBnZXRBZGRyZXNzVHlwZShhZGRyKTtcclxuXHJcbiAgICAgICAgaWYgKGFkZHJUeXBlID09PSAnZW1haWwnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaW52aXRlQnlFbWFpbChyb29tSWQsIGFkZHIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoYWRkclR5cGUgPT09ICdteC11c2VyLWlkJykge1xyXG4gICAgICAgICAgICBjb25zdCByb29tID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICAgICAgaWYgKCFyb29tKSB0aHJvdyBuZXcgRXJyb3IoXCJSb29tIG5vdCBmb3VuZFwiKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IHJvb20uZ2V0TWVtYmVyKGFkZHIpO1xyXG4gICAgICAgICAgICBpZiAobWVtYmVyICYmIFsnam9pbicsICdpbnZpdGUnXS5pbmNsdWRlcyhtZW1iZXIubWVtYmVyc2hpcCkpIHtcclxuICAgICAgICAgICAgICAgIHRocm93IHtlcnJjb2RlOiBcIlJJT1QuQUxSRUFEWV9JTl9ST09NXCIsIGVycm9yOiBcIk1lbWJlciBhbHJlYWR5IGludml0ZWRcIn07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICghaWdub3JlUHJvZmlsZSAmJiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwicHJvbXB0QmVmb3JlSW52aXRlVW5rbm93blVzZXJzXCIsIHRoaXMucm9vbUlkKSkge1xyXG4gICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9maWxlID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFByb2ZpbGVJbmZvKGFkZHIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcHJvZmlsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBub2luc3BlY3Rpb24gRXhjZXB0aW9uQ2F1Z2h0TG9jYWxseUpTXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVzZXIgaGFzIG5vIHByb2ZpbGVcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRocm93IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyY29kZTogXCJSSU9ULlVTRVJfTk9UX0ZPVU5EXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOiBcIlVzZXIgZG9lcyBub3QgaGF2ZSBhIHByb2ZpbGUgb3IgZG9lcyBub3QgZXhpc3QuXCJcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLmludml0ZShyb29tSWQsIGFkZHIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQgYWRkcmVzcycpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfZG9JbnZpdGUoYWRkcmVzcywgaWdub3JlUHJvZmlsZSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGBJbnZpdGluZyAke2FkZHJlc3N9YCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgZG9JbnZpdGU7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmdyb3VwSWQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGRvSW52aXRlID0gR3JvdXBTdG9yZS5pbnZpdGVVc2VyVG9Hcm91cCh0aGlzLmdyb3VwSWQsIGFkZHJlc3MpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZG9JbnZpdGUgPSB0aGlzLl9pbnZpdGVUb1Jvb20odGhpcy5yb29tSWQsIGFkZHJlc3MsIGlnbm9yZVByb2ZpbGUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBkb0ludml0ZS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9jYW5jZWxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbXBsZXRpb25TdGF0ZXNbYWRkcmVzc10gPSAnaW52aXRlZCc7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5lcnJvcnNbYWRkcmVzc107XHJcblxyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5fY2FuY2VsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBlcnJvclRleHQ7XHJcbiAgICAgICAgICAgICAgICBsZXQgZmF0YWwgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmIChlcnIuZXJyY29kZSA9PT0gJ01fRk9SQklEREVOJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGZhdGFsID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvclRleHQgPSBfdCgnWW91IGRvIG5vdCBoYXZlIHBlcm1pc3Npb24gdG8gaW52aXRlIHBlb3BsZSB0byB0aGlzIHJvb20uJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVyci5lcnJjb2RlID09PSBcIlJJT1QuQUxSRUFEWV9JTl9ST09NXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvclRleHQgPSBfdChcIlVzZXIgJSh1c2VySWQpcyBpcyBhbHJlYWR5IGluIHRoZSByb29tXCIsIHt1c2VySWQ6IGFkZHJlc3N9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXJyLmVycmNvZGUgPT09ICdNX0xJTUlUX0VYQ0VFREVEJykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdlJ3JlIGJlaW5nIHRocm90dGxlZCBzbyB3YWl0IGEgYml0ICYgdHJ5IGFnYWluXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2RvSW52aXRlKGFkZHJlc3MsIGlnbm9yZVByb2ZpbGUpLnRoZW4ocmVzb2x2ZSwgcmVqZWN0KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCA1MDAwKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFsnTV9OT1RfRk9VTkQnLCAnTV9VU0VSX05PVF9GT1VORCcsICdSSU9ULlVTRVJfTk9UX0ZPVU5EJ10uaW5jbHVkZXMoZXJyLmVycmNvZGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0ID0gX3QoXCJVc2VyICUodXNlcl9pZClzIGRvZXMgbm90IGV4aXN0XCIsIHt1c2VyX2lkOiBhZGRyZXNzfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVyci5lcnJjb2RlID09PSAnTV9QUk9GSUxFX1VORElTQ0xPU0VEJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IF90KFwiVXNlciAlKHVzZXJfaWQpcyBtYXkgb3IgbWF5IG5vdCBleGlzdFwiLCB7dXNlcl9pZDogYWRkcmVzc30pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChlcnIuZXJyY29kZSA9PT0gJ01fUFJPRklMRV9OT1RfRk9VTkQnICYmICFpZ25vcmVQcm9maWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSW52aXRlIHdpdGhvdXQgdGhlIHByb2ZpbGUgY2hlY2tcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oYFVzZXIgJHthZGRyZXNzfSBkb2VzIG5vdCBoYXZlIGEgcHJvZmlsZSAtIGludml0aW5nIGFueXdheXMgYXV0b21hdGljYWxseWApO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2RvSW52aXRlKGFkZHJlc3MsIHRydWUpLnRoZW4ocmVzb2x2ZSwgcmVqZWN0KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXJyLmVycmNvZGUgPT09IFwiTV9CQURfU1RBVEVcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yVGV4dCA9IF90KFwiVGhlIHVzZXIgbXVzdCBiZSB1bmJhbm5lZCBiZWZvcmUgdGhleSBjYW4gYmUgaW52aXRlZC5cIik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVyci5lcnJjb2RlID09PSBcIk1fVU5TVVBQT1JURURfUk9PTV9WRVJTSU9OXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvclRleHQgPSBfdChcIlRoZSB1c2VyJ3MgaG9tZXNlcnZlciBkb2VzIG5vdCBzdXBwb3J0IHRoZSB2ZXJzaW9uIG9mIHRoZSByb29tLlwiKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUZXh0ID0gX3QoJ1Vua25vd24gc2VydmVyIGVycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb21wbGV0aW9uU3RhdGVzW2FkZHJlc3NdID0gJ2Vycm9yJztcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JzW2FkZHJlc3NdID0ge2Vycm9yVGV4dCwgZXJyY29kZTogZXJyLmVycmNvZGV9O1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuYnVzeSA9ICFmYXRhbDtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmF0YWwgPSBmYXRhbDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZmF0YWwpIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfaW52aXRlTW9yZShuZXh0SW5kZXgsIGlnbm9yZVByb2ZpbGUpIHtcclxuICAgICAgICBpZiAodGhpcy5fY2FuY2VsZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG5leHRJbmRleCA9PT0gdGhpcy5hZGRycy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5idXN5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmIChPYmplY3Qua2V5cyh0aGlzLmVycm9ycykubGVuZ3RoID4gMCAmJiAhdGhpcy5ncm91cElkKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBUaGVyZSB3ZXJlIHByb2JsZW1zIGludml0aW5nIHNvbWUgcGVvcGxlIC0gc2VlIGlmIHdlIGNhbiBpbnZpdGUgdGhlbVxyXG4gICAgICAgICAgICAgICAgLy8gd2l0aG91dCBjYXJpbmcgaWYgdGhleSBleGlzdCBvciBub3QuXHJcbiAgICAgICAgICAgICAgICBjb25zdCB1bmtub3duUHJvZmlsZUVycm9ycyA9IFsnTV9OT1RfRk9VTkQnLCAnTV9VU0VSX05PVF9GT1VORCcsICdNX1BST0ZJTEVfVU5ESVNDTE9TRUQnLCAnTV9QUk9GSUxFX05PVF9GT1VORCcsICdSSU9ULlVTRVJfTk9UX0ZPVU5EJ107XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1bmtub3duUHJvZmlsZVVzZXJzID0gT2JqZWN0LmtleXModGhpcy5lcnJvcnMpLmZpbHRlcihhID0+IHVua25vd25Qcm9maWxlRXJyb3JzLmluY2x1ZGVzKHRoaXMuZXJyb3JzW2FdLmVycmNvZGUpKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodW5rbm93blByb2ZpbGVVc2Vycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaW52aXRlVW5rbm93bnMgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb21pc2VzID0gdW5rbm93blByb2ZpbGVVc2Vycy5tYXAodSA9PiB0aGlzLl9kb0ludml0ZSh1LCB0cnVlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb21pc2UuYWxsKHByb21pc2VzKS50aGVuKCgpID0+IHRoaXMuZGVmZXJyZWQucmVzb2x2ZSh0aGlzLmNvbXBsZXRpb25TdGF0ZXMpKTtcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoIVNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJwcm9tcHRCZWZvcmVJbnZpdGVVbmtub3duVXNlcnNcIiwgdGhpcy5yb29tSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGludml0ZVVua25vd25zKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IEFza0ludml0ZUFueXdheURpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkFza0ludml0ZUFueXdheURpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNob3dpbmcgZmFpbGVkIHRvIGludml0ZSBkaWFsb2cuLi5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGludml0ZSB0aGUgZm9sbG93aW5nIHVzZXJzIHRvIHRoZSByb29tJywgJycsIEFza0ludml0ZUFueXdheURpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1bmtub3duUHJvZmlsZVVzZXJzOiB1bmtub3duUHJvZmlsZVVzZXJzLm1hcCh1ID0+IHtyZXR1cm4ge3VzZXJJZDogdSwgZXJyb3JUZXh0OiB0aGlzLmVycm9yc1t1XS5lcnJvclRleHR9O30pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkludml0ZUFueXdheXM6ICgpID0+IGludml0ZVVua25vd25zKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uR2l2ZVVwOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBGYWtlIGFsbCB0aGUgY29tcGxldGlvbiBzdGF0ZXMgYmVjYXVzZSB3ZSBhbHJlYWR5IHdhcm5lZCB0aGUgdXNlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBhZGRyIG9mIHVua25vd25Qcm9maWxlVXNlcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbXBsZXRpb25TdGF0ZXNbYWRkcl0gPSAnaW52aXRlZCc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlZmVycmVkLnJlc29sdmUodGhpcy5jb21wbGV0aW9uU3RhdGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5kZWZlcnJlZC5yZXNvbHZlKHRoaXMuY29tcGxldGlvblN0YXRlcyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGFkZHIgPSB0aGlzLmFkZHJzW25leHRJbmRleF07XHJcblxyXG4gICAgICAgIC8vIGRvbid0IHRyeSB0byBpbnZpdGUgaXQgaWYgaXQncyBhbiBpbnZhbGlkIGFkZHJlc3NcclxuICAgICAgICAvLyAoaXQgd2lsbCBhbHJlYWR5IGJlIG1hcmtlZCBhcyBhbiBlcnJvciB0aG91Z2gsXHJcbiAgICAgICAgLy8gc28gbm8gbmVlZCB0byBkbyBzbyBhZ2FpbilcclxuICAgICAgICBpZiAoZ2V0QWRkcmVzc1R5cGUoYWRkcikgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5faW52aXRlTW9yZShuZXh0SW5kZXggKyAxKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZG9uJ3QgcmUtaW52aXRlICh0aGVyZSdzIG5vIHdheSBpbiB0aGUgVUkgdG8gZG8gdGhpcywgYnV0XHJcbiAgICAgICAgLy8gZm9yIHNhbml0eSdzIHNha2UpXHJcbiAgICAgICAgaWYgKHRoaXMuY29tcGxldGlvblN0YXRlc1thZGRyXSA9PT0gJ2ludml0ZWQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2ludml0ZU1vcmUobmV4dEluZGV4ICsgMSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX2RvSW52aXRlKGFkZHIsIGlnbm9yZVByb2ZpbGUpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9pbnZpdGVNb3JlKG5leHRJbmRleCArIDEsIGlnbm9yZVByb2ZpbGUpO1xyXG4gICAgICAgIH0pLmNhdGNoKCgpID0+IHRoaXMuZGVmZXJyZWQucmVzb2x2ZSh0aGlzLmNvbXBsZXRpb25TdGF0ZXMpKTtcclxuICAgIH1cclxufVxyXG4iXX0=