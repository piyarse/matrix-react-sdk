"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeType = makeType;

/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Creates a class of a given type using the objects defined. This
 * is a stopgap function while we don't have TypeScript interfaces.
 * In future, we'd define the `type` as an interface and just cast
 * it instead of cheating like we are here.
 * @param {Type} Type The type of class to construct.
 * @param {*} opts The options (properties) to set on the object.
 * @returns {*} The created object.
 */
function makeType(Type
/*: any*/
, opts
/*: any*/
) {
  const c = new Type();
  Object.assign(c, opts);
  return c;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9UeXBlVXRpbHMuanMiXSwibmFtZXMiOlsibWFrZVR5cGUiLCJUeXBlIiwib3B0cyIsImMiLCJPYmplY3QiLCJhc3NpZ24iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTs7Ozs7Ozs7O0FBU08sU0FBU0EsUUFBVCxDQUFrQkM7QUFBbEI7QUFBQSxFQUE2QkM7QUFBN0I7QUFBQSxFQUF3QztBQUMzQyxRQUFNQyxDQUFDLEdBQUcsSUFBSUYsSUFBSixFQUFWO0FBQ0FHLEVBQUFBLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjRixDQUFkLEVBQWlCRCxJQUFqQjtBQUNBLFNBQU9DLENBQVA7QUFDSCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLyoqXHJcbiAqIENyZWF0ZXMgYSBjbGFzcyBvZiBhIGdpdmVuIHR5cGUgdXNpbmcgdGhlIG9iamVjdHMgZGVmaW5lZC4gVGhpc1xyXG4gKiBpcyBhIHN0b3BnYXAgZnVuY3Rpb24gd2hpbGUgd2UgZG9uJ3QgaGF2ZSBUeXBlU2NyaXB0IGludGVyZmFjZXMuXHJcbiAqIEluIGZ1dHVyZSwgd2UnZCBkZWZpbmUgdGhlIGB0eXBlYCBhcyBhbiBpbnRlcmZhY2UgYW5kIGp1c3QgY2FzdFxyXG4gKiBpdCBpbnN0ZWFkIG9mIGNoZWF0aW5nIGxpa2Ugd2UgYXJlIGhlcmUuXHJcbiAqIEBwYXJhbSB7VHlwZX0gVHlwZSBUaGUgdHlwZSBvZiBjbGFzcyB0byBjb25zdHJ1Y3QuXHJcbiAqIEBwYXJhbSB7Kn0gb3B0cyBUaGUgb3B0aW9ucyAocHJvcGVydGllcykgdG8gc2V0IG9uIHRoZSBvYmplY3QuXHJcbiAqIEByZXR1cm5zIHsqfSBUaGUgY3JlYXRlZCBvYmplY3QuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gbWFrZVR5cGUoVHlwZTogYW55LCBvcHRzOiBhbnkpIHtcclxuICAgIGNvbnN0IGMgPSBuZXcgVHlwZSgpO1xyXG4gICAgT2JqZWN0LmFzc2lnbihjLCBvcHRzKTtcclxuICAgIHJldHVybiBjO1xyXG59XHJcbiJdfQ==