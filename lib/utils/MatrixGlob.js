"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MatrixGlob = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _globToRegexp = _interopRequireDefault(require("glob-to-regexp"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Taken with permission from matrix-bot-sdk:
// https://github.com/turt2live/matrix-js-bot-sdk/blob/eb148c2ecec7bf3ade801d73deb43df042d55aef/src/MatrixGlob.ts

/**
 * Represents a common Matrix glob. This is commonly used
 * for server ACLs and similar functions.
 */
class MatrixGlob {
  /**
   * Creates a new Matrix Glob
   * @param {string} glob The glob to convert. Eg: "*.example.org"
   */
  constructor(glob
  /*: string*/
  ) {
    (0, _defineProperty2.default)(this, "_regex", void 0);
    const globRegex = (0, _globToRegexp.default)(glob, {
      extended: false,
      globstar: false
    }); // We need to convert `?` manually because globToRegexp's extended mode
    // does more than we want it to.

    const replaced = globRegex.toString().replace(/\\\?/g, ".");
    this._regex = new RegExp(replaced.substring(1, replaced.length - 1));
  }
  /**
   * Tests the glob against a value, returning true if it matches.
   * @param {string} val The value to test.
   * @returns {boolean} True if the value matches the glob, false otherwise.
   */


  test(val
  /*: string*/
  )
  /*: boolean*/
  {
    return this._regex.test(val);
  }

}

exports.MatrixGlob = MatrixGlob;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9NYXRyaXhHbG9iLmpzIl0sIm5hbWVzIjpbIk1hdHJpeEdsb2IiLCJjb25zdHJ1Y3RvciIsImdsb2IiLCJnbG9iUmVnZXgiLCJleHRlbmRlZCIsImdsb2JzdGFyIiwicmVwbGFjZWQiLCJ0b1N0cmluZyIsInJlcGxhY2UiLCJfcmVnZXgiLCJSZWdFeHAiLCJzdWJzdHJpbmciLCJsZW5ndGgiLCJ0ZXN0IiwidmFsIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFoQkE7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBOztBQUVBOzs7O0FBSU8sTUFBTUEsVUFBTixDQUFpQjtBQUdwQjs7OztBQUlBQyxFQUFBQSxXQUFXLENBQUNDO0FBQUQ7QUFBQSxJQUFlO0FBQUE7QUFDdEIsVUFBTUMsU0FBUyxHQUFHLDJCQUFhRCxJQUFiLEVBQW1CO0FBQ2pDRSxNQUFBQSxRQUFRLEVBQUUsS0FEdUI7QUFFakNDLE1BQUFBLFFBQVEsRUFBRTtBQUZ1QixLQUFuQixDQUFsQixDQURzQixDQU10QjtBQUNBOztBQUNBLFVBQU1DLFFBQVEsR0FBR0gsU0FBUyxDQUFDSSxRQUFWLEdBQXFCQyxPQUFyQixDQUE2QixPQUE3QixFQUFzQyxHQUF0QyxDQUFqQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxJQUFJQyxNQUFKLENBQVdKLFFBQVEsQ0FBQ0ssU0FBVCxDQUFtQixDQUFuQixFQUFzQkwsUUFBUSxDQUFDTSxNQUFULEdBQWtCLENBQXhDLENBQVgsQ0FBZDtBQUNIO0FBRUQ7Ozs7Ozs7QUFLQUMsRUFBQUEsSUFBSSxDQUFDQztBQUFEO0FBQUE7QUFBQTtBQUF1QjtBQUN2QixXQUFPLEtBQUtMLE1BQUwsQ0FBWUksSUFBWixDQUFpQkMsR0FBakIsQ0FBUDtBQUNIOztBQTFCbUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBnbG9iVG9SZWdleHAgZnJvbSBcImdsb2ItdG8tcmVnZXhwXCI7XHJcblxyXG4vLyBUYWtlbiB3aXRoIHBlcm1pc3Npb24gZnJvbSBtYXRyaXgtYm90LXNkazpcclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL3R1cnQybGl2ZS9tYXRyaXgtanMtYm90LXNkay9ibG9iL2ViMTQ4YzJlY2VjN2JmM2FkZTgwMWQ3M2RlYjQzZGYwNDJkNTVhZWYvc3JjL01hdHJpeEdsb2IudHNcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgY29tbW9uIE1hdHJpeCBnbG9iLiBUaGlzIGlzIGNvbW1vbmx5IHVzZWRcclxuICogZm9yIHNlcnZlciBBQ0xzIGFuZCBzaW1pbGFyIGZ1bmN0aW9ucy5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBNYXRyaXhHbG9iIHtcclxuICAgIF9yZWdleDogUmVnRXhwO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyBNYXRyaXggR2xvYlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGdsb2IgVGhlIGdsb2IgdG8gY29udmVydC4gRWc6IFwiKi5leGFtcGxlLm9yZ1wiXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKGdsb2I6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGdsb2JSZWdleCA9IGdsb2JUb1JlZ2V4cChnbG9iLCB7XHJcbiAgICAgICAgICAgIGV4dGVuZGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZ2xvYnN0YXI6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBXZSBuZWVkIHRvIGNvbnZlcnQgYD9gIG1hbnVhbGx5IGJlY2F1c2UgZ2xvYlRvUmVnZXhwJ3MgZXh0ZW5kZWQgbW9kZVxyXG4gICAgICAgIC8vIGRvZXMgbW9yZSB0aGFuIHdlIHdhbnQgaXQgdG8uXHJcbiAgICAgICAgY29uc3QgcmVwbGFjZWQgPSBnbG9iUmVnZXgudG9TdHJpbmcoKS5yZXBsYWNlKC9cXFxcXFw/L2csIFwiLlwiKTtcclxuICAgICAgICB0aGlzLl9yZWdleCA9IG5ldyBSZWdFeHAocmVwbGFjZWQuc3Vic3RyaW5nKDEsIHJlcGxhY2VkLmxlbmd0aCAtIDEpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRlc3RzIHRoZSBnbG9iIGFnYWluc3QgYSB2YWx1ZSwgcmV0dXJuaW5nIHRydWUgaWYgaXQgbWF0Y2hlcy5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB2YWwgVGhlIHZhbHVlIHRvIHRlc3QuXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgdmFsdWUgbWF0Y2hlcyB0aGUgZ2xvYiwgZmFsc2Ugb3RoZXJ3aXNlLlxyXG4gICAgICovXHJcbiAgICB0ZXN0KHZhbDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlZ2V4LnRlc3QodmFsKTtcclxuICAgIH1cclxufVxyXG4iXX0=