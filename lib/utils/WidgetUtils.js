"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MatrixClientPeg = require("../MatrixClientPeg");

var _SdkConfig = _interopRequireDefault(require("../SdkConfig"));

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var url = _interopRequireWildcard(require("url"));

var _WidgetEchoStore = _interopRequireDefault(require("../stores/WidgetEchoStore"));

var _SettingsStore = _interopRequireDefault(require("../settings/SettingsStore"));

var _ActiveWidgetStore = _interopRequireDefault(require("../stores/ActiveWidgetStore"));

var _IntegrationManagers = require("../integrations/IntegrationManagers");

var _WidgetApi = require("../widgets/WidgetApi");

/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Travis Ralston

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// How long we wait for the state event echo to come back from the server
// before waitFor[Room/User]Widget rejects its promise
const WIDGET_WAIT_TIME = 20000;

class WidgetUtils {
  /* Returns true if user is able to send state events to modify widgets in this room
   * (Does not apply to non-room-based / user widgets)
   * @param roomId -- The ID of the room to check
   * @return Boolean -- true if the user can modify widgets in this room
   * @throws Error -- specifies the error reason
   */
  static canUserModifyWidgets(roomId) {
    if (!roomId) {
      console.warn('No room ID specified');
      return false;
    }

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!client) {
      console.warn('User must be be logged in');
      return false;
    }

    const room = client.getRoom(roomId);

    if (!room) {
      console.warn("Room ID ".concat(roomId, " is not recognised"));
      return false;
    }

    const me = client.credentials.userId;

    if (!me) {
      console.warn('Failed to get user ID');
      return false;
    }

    if (room.getMyMembership() !== "join") {
      console.warn("User ".concat(me, " is not in room ").concat(roomId));
      return false;
    }

    return room.currentState.maySendStateEvent('im.vector.modular.widgets', me);
  }
  /**
   * Returns true if specified url is a scalar URL, typically https://scalar.vector.im/api
   * @param  {[type]}  testUrlString URL to check
   * @return {Boolean} True if specified URL is a scalar URL
   */


  static isScalarUrl(testUrlString) {
    if (!testUrlString) {
      console.error('Scalar URL check failed. No URL specified');
      return false;
    }

    const testUrl = url.parse(testUrlString);

    let scalarUrls = _SdkConfig.default.get().integrations_widgets_urls;

    if (!scalarUrls || scalarUrls.length === 0) {
      const defaultManager = _IntegrationManagers.IntegrationManagers.sharedInstance().getPrimaryManager();

      if (defaultManager) {
        scalarUrls = [defaultManager.apiUrl];
      } else {
        scalarUrls = [];
      }
    }

    for (let i = 0; i < scalarUrls.length; i++) {
      const scalarUrl = url.parse(scalarUrls[i]);

      if (testUrl && scalarUrl) {
        if (testUrl.protocol === scalarUrl.protocol && testUrl.host === scalarUrl.host && testUrl.pathname.startsWith(scalarUrl.pathname)) {
          return true;
        }
      }
    }

    return false;
  }
  /**
   * Returns a promise that resolves when a widget with the given
   * ID has been added as a user widget (ie. the accountData event
   * arrives) or rejects after a timeout
   *
   * @param {string} widgetId The ID of the widget to wait for
   * @param {boolean} add True to wait for the widget to be added,
   *     false to wait for it to be deleted.
   * @returns {Promise} that resolves when the widget is in the
   *     requested state according to the `add` param
   */


  static waitForUserWidget(widgetId, add) {
    return new Promise((resolve, reject) => {
      // Tests an account data event, returning true if it's in the state
      // we're waiting for it to be in
      function eventInIntendedState(ev) {
        if (!ev || !ev.getContent()) return false;

        if (add) {
          return ev.getContent()[widgetId] !== undefined;
        } else {
          return ev.getContent()[widgetId] === undefined;
        }
      }

      const startingAccountDataEvent = _MatrixClientPeg.MatrixClientPeg.get().getAccountData('m.widgets');

      if (eventInIntendedState(startingAccountDataEvent)) {
        resolve();
        return;
      }

      function onAccountData(ev) {
        const currentAccountDataEvent = _MatrixClientPeg.MatrixClientPeg.get().getAccountData('m.widgets');

        if (eventInIntendedState(currentAccountDataEvent)) {
          _MatrixClientPeg.MatrixClientPeg.get().removeListener('accountData', onAccountData);

          clearTimeout(timerId);
          resolve();
        }
      }

      const timerId = setTimeout(() => {
        _MatrixClientPeg.MatrixClientPeg.get().removeListener('accountData', onAccountData);

        reject(new Error("Timed out waiting for widget ID " + widgetId + " to appear"));
      }, WIDGET_WAIT_TIME);

      _MatrixClientPeg.MatrixClientPeg.get().on('accountData', onAccountData);
    });
  }
  /**
   * Returns a promise that resolves when a widget with the given
   * ID has been added as a room widget in the given room (ie. the
   * room state event arrives) or rejects after a timeout
   *
   * @param {string} widgetId The ID of the widget to wait for
   * @param {string} roomId The ID of the room to wait for the widget in
   * @param {boolean} add True to wait for the widget to be added,
   *     false to wait for it to be deleted.
   * @returns {Promise} that resolves when the widget is in the
   *     requested state according to the `add` param
   */


  static waitForRoomWidget(widgetId, roomId, add) {
    return new Promise((resolve, reject) => {
      // Tests a list of state events, returning true if it's in the state
      // we're waiting for it to be in
      function eventsInIntendedState(evList) {
        const widgetPresent = evList.some(ev => {
          return ev.getContent() && ev.getContent()['id'] === widgetId;
        });

        if (add) {
          return widgetPresent;
        } else {
          return !widgetPresent;
        }
      }

      const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

      const startingWidgetEvents = room.currentState.getStateEvents('im.vector.modular.widgets');

      if (eventsInIntendedState(startingWidgetEvents)) {
        resolve();
        return;
      }

      function onRoomStateEvents(ev) {
        if (ev.getRoomId() !== roomId) return;
        const currentWidgetEvents = room.currentState.getStateEvents('im.vector.modular.widgets');

        if (eventsInIntendedState(currentWidgetEvents)) {
          _MatrixClientPeg.MatrixClientPeg.get().removeListener('RoomState.events', onRoomStateEvents);

          clearTimeout(timerId);
          resolve();
        }
      }

      const timerId = setTimeout(() => {
        _MatrixClientPeg.MatrixClientPeg.get().removeListener('RoomState.events', onRoomStateEvents);

        reject(new Error("Timed out waiting for widget ID " + widgetId + " to appear"));
      }, WIDGET_WAIT_TIME);

      _MatrixClientPeg.MatrixClientPeg.get().on('RoomState.events', onRoomStateEvents);
    });
  }

  static setUserWidget(widgetId, widgetType, widgetUrl, widgetName, widgetData) {
    const content = {
      type: widgetType,
      url: widgetUrl,
      name: widgetName,
      data: widgetData
    };

    const client = _MatrixClientPeg.MatrixClientPeg.get(); // Get the current widgets and clone them before we modify them, otherwise
    // we'll modify the content of the old event.


    const userWidgets = JSON.parse(JSON.stringify(WidgetUtils.getUserWidgets())); // Delete existing widget with ID

    try {
      delete userWidgets[widgetId];
    } catch (e) {
      console.error("$widgetId is non-configurable");
    }

    const addingWidget = Boolean(widgetUrl); // Add new widget / update

    if (addingWidget) {
      userWidgets[widgetId] = {
        content: content,
        sender: client.getUserId(),
        state_key: widgetId,
        type: 'm.widget',
        id: widgetId
      };
    } // This starts listening for when the echo comes back from the server
    // since the widget won't appear added until this happens. If we don't
    // wait for this, the action will complete but if the user is fast enough,
    // the widget still won't actually be there.


    return client.setAccountData('m.widgets', userWidgets).then(() => {
      return WidgetUtils.waitForUserWidget(widgetId, addingWidget);
    }).then(() => {
      _dispatcher.default.dispatch({
        action: "user_widget_updated"
      });
    });
  }

  static setRoomWidget(roomId, widgetId, widgetType, widgetUrl, widgetName, widgetData) {
    let content;
    const addingWidget = Boolean(widgetUrl);

    if (addingWidget) {
      content = {
        type: widgetType,
        url: widgetUrl,
        name: widgetName,
        data: widgetData
      };
    } else {
      content = {};
    }

    _WidgetEchoStore.default.setRoomWidgetEcho(roomId, widgetId, content);

    const client = _MatrixClientPeg.MatrixClientPeg.get(); // TODO - Room widgets need to be moved to 'm.widget' state events
    // https://docs.google.com/document/d/1uPF7XWY_dXTKVKV7jZQ2KmsI19wn9-kFRgQ1tFQP7wQ/edit?usp=sharing


    return client.sendStateEvent(roomId, "im.vector.modular.widgets", content, widgetId).then(() => {
      return WidgetUtils.waitForRoomWidget(widgetId, roomId, addingWidget);
    }).finally(() => {
      _WidgetEchoStore.default.removeRoomWidgetEcho(roomId, widgetId);
    });
  }
  /**
   * Get room specific widgets
   * @param  {object} room The room to get widgets force
   * @return {[object]} Array containing current / active room widgets
   */


  static getRoomWidgets(room) {
    const appsStateEvents = room.currentState.getStateEvents('im.vector.modular.widgets');

    if (!appsStateEvents) {
      return [];
    }

    return appsStateEvents.filter(ev => {
      return ev.getContent().type && ev.getContent().url;
    });
  }
  /**
   * Get user specific widgets (not linked to a specific room)
   * @return {object} Event content object containing current / active user widgets
   */


  static getUserWidgets() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!client) {
      throw new Error('User not logged in');
    }

    const userWidgets = client.getAccountData('m.widgets');

    if (userWidgets && userWidgets.getContent()) {
      return userWidgets.getContent();
    }

    return {};
  }
  /**
   * Get user specific widgets (not linked to a specific room) as an array
   * @return {[object]} Array containing current / active user widgets
   */


  static getUserWidgetsArray() {
    return Object.values(WidgetUtils.getUserWidgets());
  }
  /**
   * Get active stickerpicker widgets (stickerpickers are user widgets by nature)
   * @return {[object]} Array containing current / active stickerpicker widgets
   */


  static getStickerpickerWidgets() {
    const widgets = WidgetUtils.getUserWidgetsArray();
    return widgets.filter(widget => widget.content && widget.content.type === "m.stickerpicker");
  }
  /**
   * Get all integration manager widgets for this user.
   * @returns {Object[]} An array of integration manager user widgets.
   */


  static getIntegrationManagerWidgets() {
    const widgets = WidgetUtils.getUserWidgetsArray();
    return widgets.filter(w => w.content && w.content.type === "m.integration_manager");
  }

  static removeIntegrationManagerWidgets() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!client) {
      throw new Error('User not logged in');
    }

    const widgets = client.getAccountData('m.widgets');
    if (!widgets) return;
    const userWidgets = widgets.getContent() || {};
    Object.entries(userWidgets).forEach(([key, widget]) => {
      if (widget.content && widget.content.type === "m.integration_manager") {
        delete userWidgets[key];
      }
    });
    return client.setAccountData('m.widgets', userWidgets);
  }

  static addIntegrationManagerWidget(name
  /*: string*/
  , uiUrl
  /*: string*/
  , apiUrl
  /*: string*/
  ) {
    return WidgetUtils.setUserWidget("integration_manager_" + new Date().getTime(), "m.integration_manager", uiUrl, "Integration Manager: " + name, {
      "api_url": apiUrl
    });
  }
  /**
   * Remove all stickerpicker widgets (stickerpickers are user widgets by nature)
   * @return {Promise} Resolves on account data updated
   */


  static removeStickerpickerWidgets() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!client) {
      throw new Error('User not logged in');
    }

    const widgets = client.getAccountData('m.widgets');
    if (!widgets) return;
    const userWidgets = widgets.getContent() || {};
    Object.entries(userWidgets).forEach(([key, widget]) => {
      if (widget.content && widget.content.type === 'm.stickerpicker') {
        delete userWidgets[key];
      }
    });
    return client.setAccountData('m.widgets', userWidgets);
  }

  static makeAppConfig(appId, app, senderUserId, roomId, eventId) {
    if (!senderUserId) {
      throw new Error("Widgets must be created by someone - provide a senderUserId");
    }

    app.creatorUserId = senderUserId;
    app.id = appId;
    app.eventId = eventId;
    app.name = app.name || app.type;
    return app;
  }

  static getCapWhitelistForAppTypeInRoomId(appType, roomId) {
    const enableScreenshots = _SettingsStore.default.getValue("enableWidgetScreenshots", roomId);

    const capWhitelist = enableScreenshots ? [_WidgetApi.Capability.Screenshot] : []; // Obviously anyone that can add a widget can claim it's a jitsi widget,
    // so this doesn't really offer much over the set of domains we load
    // widgets from at all, but it probably makes sense for sanity.

    if (appType === 'jitsi') {
      capWhitelist.push(_WidgetApi.Capability.AlwaysOnScreen);
    }

    return capWhitelist;
  }

  static getWidgetSecurityKey(widgetId, widgetUrl, isUserWidget) {
    let widgetLocation = _ActiveWidgetStore.default.getRoomId(widgetId);

    if (isUserWidget) {
      const userWidget = WidgetUtils.getUserWidgetsArray().find(w => w.id === widgetId && w.content && w.content.url === widgetUrl);

      if (!userWidget) {
        throw new Error("No matching user widget to form security key");
      }

      widgetLocation = userWidget.sender;
    }

    if (!widgetLocation) {
      throw new Error("Failed to locate where the widget resides");
    }

    return encodeURIComponent("".concat(widgetLocation, "::").concat(widgetUrl));
  }

  static getLocalJitsiWrapperUrl(opts
  /*: {forLocalRender?: boolean}*/
  = {}) {
    // NB. we can't just encodeURIComponent all of these because the $ signs need to be there
    const queryString = ['conferenceDomain=$domain', 'conferenceId=$conferenceId', 'isAudioOnly=$isAudioOnly', 'displayName=$matrix_display_name', 'avatarUrl=$matrix_avatar_url', 'userId=$matrix_user_id'].join('&');
    let baseUrl = window.location;

    if (window.location.protocol !== "https:" && !opts.forLocalRender) {
      // Use an external wrapper if we're not locally rendering the widget. This is usually
      // the URL that will end up in the widget event, so we want to make sure it's relatively
      // safe to send.
      // We'll end up using a local render URL when we see a Jitsi widget anyways, so this is
      // really just for backwards compatibility and to appease the spec.
      baseUrl = "https://riot.im/app/";
    }

    const url = new URL("jitsi.html#" + queryString, baseUrl); // this strips hash fragment from baseUrl

    return url.href;
  }

}

exports.default = WidgetUtils;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9XaWRnZXRVdGlscy5qcyJdLCJuYW1lcyI6WyJXSURHRVRfV0FJVF9USU1FIiwiV2lkZ2V0VXRpbHMiLCJjYW5Vc2VyTW9kaWZ5V2lkZ2V0cyIsInJvb21JZCIsImNvbnNvbGUiLCJ3YXJuIiwiY2xpZW50IiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0Iiwicm9vbSIsImdldFJvb20iLCJtZSIsImNyZWRlbnRpYWxzIiwidXNlcklkIiwiZ2V0TXlNZW1iZXJzaGlwIiwiY3VycmVudFN0YXRlIiwibWF5U2VuZFN0YXRlRXZlbnQiLCJpc1NjYWxhclVybCIsInRlc3RVcmxTdHJpbmciLCJlcnJvciIsInRlc3RVcmwiLCJ1cmwiLCJwYXJzZSIsInNjYWxhclVybHMiLCJTZGtDb25maWciLCJpbnRlZ3JhdGlvbnNfd2lkZ2V0c191cmxzIiwibGVuZ3RoIiwiZGVmYXVsdE1hbmFnZXIiLCJJbnRlZ3JhdGlvbk1hbmFnZXJzIiwic2hhcmVkSW5zdGFuY2UiLCJnZXRQcmltYXJ5TWFuYWdlciIsImFwaVVybCIsImkiLCJzY2FsYXJVcmwiLCJwcm90b2NvbCIsImhvc3QiLCJwYXRobmFtZSIsInN0YXJ0c1dpdGgiLCJ3YWl0Rm9yVXNlcldpZGdldCIsIndpZGdldElkIiwiYWRkIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJldmVudEluSW50ZW5kZWRTdGF0ZSIsImV2IiwiZ2V0Q29udGVudCIsInVuZGVmaW5lZCIsInN0YXJ0aW5nQWNjb3VudERhdGFFdmVudCIsImdldEFjY291bnREYXRhIiwib25BY2NvdW50RGF0YSIsImN1cnJlbnRBY2NvdW50RGF0YUV2ZW50IiwicmVtb3ZlTGlzdGVuZXIiLCJjbGVhclRpbWVvdXQiLCJ0aW1lcklkIiwic2V0VGltZW91dCIsIkVycm9yIiwib24iLCJ3YWl0Rm9yUm9vbVdpZGdldCIsImV2ZW50c0luSW50ZW5kZWRTdGF0ZSIsImV2TGlzdCIsIndpZGdldFByZXNlbnQiLCJzb21lIiwic3RhcnRpbmdXaWRnZXRFdmVudHMiLCJnZXRTdGF0ZUV2ZW50cyIsIm9uUm9vbVN0YXRlRXZlbnRzIiwiZ2V0Um9vbUlkIiwiY3VycmVudFdpZGdldEV2ZW50cyIsInNldFVzZXJXaWRnZXQiLCJ3aWRnZXRUeXBlIiwid2lkZ2V0VXJsIiwid2lkZ2V0TmFtZSIsIndpZGdldERhdGEiLCJjb250ZW50IiwidHlwZSIsIm5hbWUiLCJkYXRhIiwidXNlcldpZGdldHMiLCJKU09OIiwic3RyaW5naWZ5IiwiZ2V0VXNlcldpZGdldHMiLCJlIiwiYWRkaW5nV2lkZ2V0IiwiQm9vbGVhbiIsInNlbmRlciIsImdldFVzZXJJZCIsInN0YXRlX2tleSIsImlkIiwic2V0QWNjb3VudERhdGEiLCJ0aGVuIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJzZXRSb29tV2lkZ2V0IiwiV2lkZ2V0RWNob1N0b3JlIiwic2V0Um9vbVdpZGdldEVjaG8iLCJzZW5kU3RhdGVFdmVudCIsImZpbmFsbHkiLCJyZW1vdmVSb29tV2lkZ2V0RWNobyIsImdldFJvb21XaWRnZXRzIiwiYXBwc1N0YXRlRXZlbnRzIiwiZmlsdGVyIiwiZ2V0VXNlcldpZGdldHNBcnJheSIsIk9iamVjdCIsInZhbHVlcyIsImdldFN0aWNrZXJwaWNrZXJXaWRnZXRzIiwid2lkZ2V0cyIsIndpZGdldCIsImdldEludGVncmF0aW9uTWFuYWdlcldpZGdldHMiLCJ3IiwicmVtb3ZlSW50ZWdyYXRpb25NYW5hZ2VyV2lkZ2V0cyIsImVudHJpZXMiLCJmb3JFYWNoIiwia2V5IiwiYWRkSW50ZWdyYXRpb25NYW5hZ2VyV2lkZ2V0IiwidWlVcmwiLCJEYXRlIiwiZ2V0VGltZSIsInJlbW92ZVN0aWNrZXJwaWNrZXJXaWRnZXRzIiwibWFrZUFwcENvbmZpZyIsImFwcElkIiwiYXBwIiwic2VuZGVyVXNlcklkIiwiZXZlbnRJZCIsImNyZWF0b3JVc2VySWQiLCJnZXRDYXBXaGl0ZWxpc3RGb3JBcHBUeXBlSW5Sb29tSWQiLCJhcHBUeXBlIiwiZW5hYmxlU2NyZWVuc2hvdHMiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJjYXBXaGl0ZWxpc3QiLCJDYXBhYmlsaXR5IiwiU2NyZWVuc2hvdCIsInB1c2giLCJBbHdheXNPblNjcmVlbiIsImdldFdpZGdldFNlY3VyaXR5S2V5IiwiaXNVc2VyV2lkZ2V0Iiwid2lkZ2V0TG9jYXRpb24iLCJBY3RpdmVXaWRnZXRTdG9yZSIsInVzZXJXaWRnZXQiLCJmaW5kIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwiZ2V0TG9jYWxKaXRzaVdyYXBwZXJVcmwiLCJvcHRzIiwicXVlcnlTdHJpbmciLCJqb2luIiwiYmFzZVVybCIsIndpbmRvdyIsImxvY2F0aW9uIiwiZm9yTG9jYWxSZW5kZXIiLCJVUkwiLCJocmVmIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWtCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFLQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBO0FBQ0E7QUFDQSxNQUFNQSxnQkFBZ0IsR0FBRyxLQUF6Qjs7QUFNZSxNQUFNQyxXQUFOLENBQWtCO0FBQzdCOzs7Ozs7QUFNQSxTQUFPQyxvQkFBUCxDQUE0QkMsTUFBNUIsRUFBb0M7QUFDaEMsUUFBSSxDQUFDQSxNQUFMLEVBQWE7QUFDVEMsTUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsc0JBQWI7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFFRCxVQUFNQyxNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxRQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNURixNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSwyQkFBYjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFVBQU1JLElBQUksR0FBR0gsTUFBTSxDQUFDSSxPQUFQLENBQWVQLE1BQWYsQ0FBYjs7QUFDQSxRQUFJLENBQUNNLElBQUwsRUFBVztBQUNQTCxNQUFBQSxPQUFPLENBQUNDLElBQVIsbUJBQXdCRixNQUF4QjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFVBQU1RLEVBQUUsR0FBR0wsTUFBTSxDQUFDTSxXQUFQLENBQW1CQyxNQUE5Qjs7QUFDQSxRQUFJLENBQUNGLEVBQUwsRUFBUztBQUNMUCxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSx1QkFBYjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFFBQUlJLElBQUksQ0FBQ0ssZUFBTCxPQUEyQixNQUEvQixFQUF1QztBQUNuQ1YsTUFBQUEsT0FBTyxDQUFDQyxJQUFSLGdCQUFxQk0sRUFBckIsNkJBQTBDUixNQUExQztBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFdBQU9NLElBQUksQ0FBQ00sWUFBTCxDQUFrQkMsaUJBQWxCLENBQW9DLDJCQUFwQyxFQUFpRUwsRUFBakUsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7QUFLQSxTQUFPTSxXQUFQLENBQW1CQyxhQUFuQixFQUFrQztBQUM5QixRQUFJLENBQUNBLGFBQUwsRUFBb0I7QUFDaEJkLE1BQUFBLE9BQU8sQ0FBQ2UsS0FBUixDQUFjLDJDQUFkO0FBQ0EsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsVUFBTUMsT0FBTyxHQUFHQyxHQUFHLENBQUNDLEtBQUosQ0FBVUosYUFBVixDQUFoQjs7QUFDQSxRQUFJSyxVQUFVLEdBQUdDLG1CQUFVaEIsR0FBVixHQUFnQmlCLHlCQUFqQzs7QUFDQSxRQUFJLENBQUNGLFVBQUQsSUFBZUEsVUFBVSxDQUFDRyxNQUFYLEtBQXNCLENBQXpDLEVBQTRDO0FBQ3hDLFlBQU1DLGNBQWMsR0FBR0MseUNBQW9CQyxjQUFwQixHQUFxQ0MsaUJBQXJDLEVBQXZCOztBQUNBLFVBQUlILGNBQUosRUFBb0I7QUFDaEJKLFFBQUFBLFVBQVUsR0FBRyxDQUFDSSxjQUFjLENBQUNJLE1BQWhCLENBQWI7QUFDSCxPQUZELE1BRU87QUFDSFIsUUFBQUEsVUFBVSxHQUFHLEVBQWI7QUFDSDtBQUNKOztBQUVELFNBQUssSUFBSVMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1QsVUFBVSxDQUFDRyxNQUEvQixFQUF1Q00sQ0FBQyxFQUF4QyxFQUE0QztBQUN4QyxZQUFNQyxTQUFTLEdBQUdaLEdBQUcsQ0FBQ0MsS0FBSixDQUFVQyxVQUFVLENBQUNTLENBQUQsQ0FBcEIsQ0FBbEI7O0FBQ0EsVUFBSVosT0FBTyxJQUFJYSxTQUFmLEVBQTBCO0FBQ3RCLFlBQ0liLE9BQU8sQ0FBQ2MsUUFBUixLQUFxQkQsU0FBUyxDQUFDQyxRQUEvQixJQUNBZCxPQUFPLENBQUNlLElBQVIsS0FBaUJGLFNBQVMsQ0FBQ0UsSUFEM0IsSUFFQWYsT0FBTyxDQUFDZ0IsUUFBUixDQUFpQkMsVUFBakIsQ0FBNEJKLFNBQVMsQ0FBQ0csUUFBdEMsQ0FISixFQUlFO0FBQ0UsaUJBQU8sSUFBUDtBQUNIO0FBQ0o7QUFDSjs7QUFDRCxXQUFPLEtBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O0FBV0EsU0FBT0UsaUJBQVAsQ0FBeUJDLFFBQXpCLEVBQW1DQyxHQUFuQyxFQUF3QztBQUNwQyxXQUFPLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDcEM7QUFDQTtBQUNBLGVBQVNDLG9CQUFULENBQThCQyxFQUE5QixFQUFrQztBQUM5QixZQUFJLENBQUNBLEVBQUQsSUFBTyxDQUFDQSxFQUFFLENBQUNDLFVBQUgsRUFBWixFQUE2QixPQUFPLEtBQVA7O0FBQzdCLFlBQUlOLEdBQUosRUFBUztBQUNMLGlCQUFPSyxFQUFFLENBQUNDLFVBQUgsR0FBZ0JQLFFBQWhCLE1BQThCUSxTQUFyQztBQUNILFNBRkQsTUFFTztBQUNILGlCQUFPRixFQUFFLENBQUNDLFVBQUgsR0FBZ0JQLFFBQWhCLE1BQThCUSxTQUFyQztBQUNIO0FBQ0o7O0FBRUQsWUFBTUMsd0JBQXdCLEdBQUd6QyxpQ0FBZ0JDLEdBQWhCLEdBQXNCeUMsY0FBdEIsQ0FBcUMsV0FBckMsQ0FBakM7O0FBQ0EsVUFBSUwsb0JBQW9CLENBQUNJLHdCQUFELENBQXhCLEVBQW9EO0FBQ2hETixRQUFBQSxPQUFPO0FBQ1A7QUFDSDs7QUFFRCxlQUFTUSxhQUFULENBQXVCTCxFQUF2QixFQUEyQjtBQUN2QixjQUFNTSx1QkFBdUIsR0FBRzVDLGlDQUFnQkMsR0FBaEIsR0FBc0J5QyxjQUF0QixDQUFxQyxXQUFyQyxDQUFoQzs7QUFDQSxZQUFJTCxvQkFBb0IsQ0FBQ08sdUJBQUQsQ0FBeEIsRUFBbUQ7QUFDL0M1QywyQ0FBZ0JDLEdBQWhCLEdBQXNCNEMsY0FBdEIsQ0FBcUMsYUFBckMsRUFBb0RGLGFBQXBEOztBQUNBRyxVQUFBQSxZQUFZLENBQUNDLE9BQUQsQ0FBWjtBQUNBWixVQUFBQSxPQUFPO0FBQ1Y7QUFDSjs7QUFDRCxZQUFNWSxPQUFPLEdBQUdDLFVBQVUsQ0FBQyxNQUFNO0FBQzdCaEQseUNBQWdCQyxHQUFoQixHQUFzQjRDLGNBQXRCLENBQXFDLGFBQXJDLEVBQW9ERixhQUFwRDs7QUFDQVAsUUFBQUEsTUFBTSxDQUFDLElBQUlhLEtBQUosQ0FBVSxxQ0FBcUNqQixRQUFyQyxHQUFnRCxZQUExRCxDQUFELENBQU47QUFDSCxPQUh5QixFQUd2QnZDLGdCQUh1QixDQUExQjs7QUFJQU8sdUNBQWdCQyxHQUFoQixHQUFzQmlELEVBQXRCLENBQXlCLGFBQXpCLEVBQXdDUCxhQUF4QztBQUNILEtBL0JNLENBQVA7QUFnQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7QUFZQSxTQUFPUSxpQkFBUCxDQUF5Qm5CLFFBQXpCLEVBQW1DcEMsTUFBbkMsRUFBMkNxQyxHQUEzQyxFQUFnRDtBQUM1QyxXQUFPLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDcEM7QUFDQTtBQUNBLGVBQVNnQixxQkFBVCxDQUErQkMsTUFBL0IsRUFBdUM7QUFDbkMsY0FBTUMsYUFBYSxHQUFHRCxNQUFNLENBQUNFLElBQVAsQ0FBYWpCLEVBQUQsSUFBUTtBQUN0QyxpQkFBT0EsRUFBRSxDQUFDQyxVQUFILE1BQW1CRCxFQUFFLENBQUNDLFVBQUgsR0FBZ0IsSUFBaEIsTUFBMEJQLFFBQXBEO0FBQ0gsU0FGcUIsQ0FBdEI7O0FBR0EsWUFBSUMsR0FBSixFQUFTO0FBQ0wsaUJBQU9xQixhQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsaUJBQU8sQ0FBQ0EsYUFBUjtBQUNIO0FBQ0o7O0FBRUQsWUFBTXBELElBQUksR0FBR0YsaUNBQWdCQyxHQUFoQixHQUFzQkUsT0FBdEIsQ0FBOEJQLE1BQTlCLENBQWI7O0FBQ0EsWUFBTTRELG9CQUFvQixHQUFHdEQsSUFBSSxDQUFDTSxZQUFMLENBQWtCaUQsY0FBbEIsQ0FBaUMsMkJBQWpDLENBQTdCOztBQUNBLFVBQUlMLHFCQUFxQixDQUFDSSxvQkFBRCxDQUF6QixFQUFpRDtBQUM3Q3JCLFFBQUFBLE9BQU87QUFDUDtBQUNIOztBQUVELGVBQVN1QixpQkFBVCxDQUEyQnBCLEVBQTNCLEVBQStCO0FBQzNCLFlBQUlBLEVBQUUsQ0FBQ3FCLFNBQUgsT0FBbUIvRCxNQUF2QixFQUErQjtBQUUvQixjQUFNZ0UsbUJBQW1CLEdBQUcxRCxJQUFJLENBQUNNLFlBQUwsQ0FBa0JpRCxjQUFsQixDQUFpQywyQkFBakMsQ0FBNUI7O0FBRUEsWUFBSUwscUJBQXFCLENBQUNRLG1CQUFELENBQXpCLEVBQWdEO0FBQzVDNUQsMkNBQWdCQyxHQUFoQixHQUFzQjRDLGNBQXRCLENBQXFDLGtCQUFyQyxFQUF5RGEsaUJBQXpEOztBQUNBWixVQUFBQSxZQUFZLENBQUNDLE9BQUQsQ0FBWjtBQUNBWixVQUFBQSxPQUFPO0FBQ1Y7QUFDSjs7QUFDRCxZQUFNWSxPQUFPLEdBQUdDLFVBQVUsQ0FBQyxNQUFNO0FBQzdCaEQseUNBQWdCQyxHQUFoQixHQUFzQjRDLGNBQXRCLENBQXFDLGtCQUFyQyxFQUF5RGEsaUJBQXpEOztBQUNBdEIsUUFBQUEsTUFBTSxDQUFDLElBQUlhLEtBQUosQ0FBVSxxQ0FBcUNqQixRQUFyQyxHQUFnRCxZQUExRCxDQUFELENBQU47QUFDSCxPQUh5QixFQUd2QnZDLGdCQUh1QixDQUExQjs7QUFJQU8sdUNBQWdCQyxHQUFoQixHQUFzQmlELEVBQXRCLENBQXlCLGtCQUF6QixFQUE2Q1EsaUJBQTdDO0FBQ0gsS0FyQ00sQ0FBUDtBQXNDSDs7QUFFRCxTQUFPRyxhQUFQLENBQXFCN0IsUUFBckIsRUFBK0I4QixVQUEvQixFQUEyQ0MsU0FBM0MsRUFBc0RDLFVBQXRELEVBQWtFQyxVQUFsRSxFQUE4RTtBQUMxRSxVQUFNQyxPQUFPLEdBQUc7QUFDWkMsTUFBQUEsSUFBSSxFQUFFTCxVQURNO0FBRVpoRCxNQUFBQSxHQUFHLEVBQUVpRCxTQUZPO0FBR1pLLE1BQUFBLElBQUksRUFBRUosVUFITTtBQUlaSyxNQUFBQSxJQUFJLEVBQUVKO0FBSk0sS0FBaEI7O0FBT0EsVUFBTWxFLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmLENBUjBFLENBUzFFO0FBQ0E7OztBQUNBLFVBQU1xRSxXQUFXLEdBQUdDLElBQUksQ0FBQ3hELEtBQUwsQ0FBV3dELElBQUksQ0FBQ0MsU0FBTCxDQUFlOUUsV0FBVyxDQUFDK0UsY0FBWixFQUFmLENBQVgsQ0FBcEIsQ0FYMEUsQ0FhMUU7O0FBQ0EsUUFBSTtBQUNBLGFBQU9ILFdBQVcsQ0FBQ3RDLFFBQUQsQ0FBbEI7QUFDSCxLQUZELENBRUUsT0FBTzBDLENBQVAsRUFBVTtBQUNSN0UsTUFBQUEsT0FBTyxDQUFDZSxLQUFSO0FBQ0g7O0FBRUQsVUFBTStELFlBQVksR0FBR0MsT0FBTyxDQUFDYixTQUFELENBQTVCLENBcEIwRSxDQXNCMUU7O0FBQ0EsUUFBSVksWUFBSixFQUFrQjtBQUNkTCxNQUFBQSxXQUFXLENBQUN0QyxRQUFELENBQVgsR0FBd0I7QUFDcEJrQyxRQUFBQSxPQUFPLEVBQUVBLE9BRFc7QUFFcEJXLFFBQUFBLE1BQU0sRUFBRTlFLE1BQU0sQ0FBQytFLFNBQVAsRUFGWTtBQUdwQkMsUUFBQUEsU0FBUyxFQUFFL0MsUUFIUztBQUlwQm1DLFFBQUFBLElBQUksRUFBRSxVQUpjO0FBS3BCYSxRQUFBQSxFQUFFLEVBQUVoRDtBQUxnQixPQUF4QjtBQU9ILEtBL0J5RSxDQWlDMUU7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFdBQU9qQyxNQUFNLENBQUNrRixjQUFQLENBQXNCLFdBQXRCLEVBQW1DWCxXQUFuQyxFQUFnRFksSUFBaEQsQ0FBcUQsTUFBTTtBQUM5RCxhQUFPeEYsV0FBVyxDQUFDcUMsaUJBQVosQ0FBOEJDLFFBQTlCLEVBQXdDMkMsWUFBeEMsQ0FBUDtBQUNILEtBRk0sRUFFSk8sSUFGSSxDQUVDLE1BQU07QUFDVkMsMEJBQUlDLFFBQUosQ0FBYTtBQUFFQyxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFiO0FBQ0gsS0FKTSxDQUFQO0FBS0g7O0FBRUQsU0FBT0MsYUFBUCxDQUFxQjFGLE1BQXJCLEVBQTZCb0MsUUFBN0IsRUFBdUM4QixVQUF2QyxFQUFtREMsU0FBbkQsRUFBOERDLFVBQTlELEVBQTBFQyxVQUExRSxFQUFzRjtBQUNsRixRQUFJQyxPQUFKO0FBRUEsVUFBTVMsWUFBWSxHQUFHQyxPQUFPLENBQUNiLFNBQUQsQ0FBNUI7O0FBRUEsUUFBSVksWUFBSixFQUFrQjtBQUNkVCxNQUFBQSxPQUFPLEdBQUc7QUFDTkMsUUFBQUEsSUFBSSxFQUFFTCxVQURBO0FBRU5oRCxRQUFBQSxHQUFHLEVBQUVpRCxTQUZDO0FBR05LLFFBQUFBLElBQUksRUFBRUosVUFIQTtBQUlOSyxRQUFBQSxJQUFJLEVBQUVKO0FBSkEsT0FBVjtBQU1ILEtBUEQsTUFPTztBQUNIQyxNQUFBQSxPQUFPLEdBQUcsRUFBVjtBQUNIOztBQUVEcUIsNkJBQWdCQyxpQkFBaEIsQ0FBa0M1RixNQUFsQyxFQUEwQ29DLFFBQTFDLEVBQW9Ea0MsT0FBcEQ7O0FBRUEsVUFBTW5FLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmLENBbEJrRixDQW1CbEY7QUFDQTs7O0FBQ0EsV0FBT0YsTUFBTSxDQUFDMEYsY0FBUCxDQUFzQjdGLE1BQXRCLEVBQThCLDJCQUE5QixFQUEyRHNFLE9BQTNELEVBQW9FbEMsUUFBcEUsRUFBOEVrRCxJQUE5RSxDQUFtRixNQUFNO0FBQzVGLGFBQU94RixXQUFXLENBQUN5RCxpQkFBWixDQUE4Qm5CLFFBQTlCLEVBQXdDcEMsTUFBeEMsRUFBZ0QrRSxZQUFoRCxDQUFQO0FBQ0gsS0FGTSxFQUVKZSxPQUZJLENBRUksTUFBTTtBQUNiSCwrQkFBZ0JJLG9CQUFoQixDQUFxQy9GLE1BQXJDLEVBQTZDb0MsUUFBN0M7QUFDSCxLQUpNLENBQVA7QUFLSDtBQUVEOzs7Ozs7O0FBS0EsU0FBTzRELGNBQVAsQ0FBc0IxRixJQUF0QixFQUE0QjtBQUN4QixVQUFNMkYsZUFBZSxHQUFHM0YsSUFBSSxDQUFDTSxZQUFMLENBQWtCaUQsY0FBbEIsQ0FBaUMsMkJBQWpDLENBQXhCOztBQUNBLFFBQUksQ0FBQ29DLGVBQUwsRUFBc0I7QUFDbEIsYUFBTyxFQUFQO0FBQ0g7O0FBRUQsV0FBT0EsZUFBZSxDQUFDQyxNQUFoQixDQUF3QnhELEVBQUQsSUFBUTtBQUNsQyxhQUFPQSxFQUFFLENBQUNDLFVBQUgsR0FBZ0I0QixJQUFoQixJQUF3QjdCLEVBQUUsQ0FBQ0MsVUFBSCxHQUFnQnpCLEdBQS9DO0FBQ0gsS0FGTSxDQUFQO0FBR0g7QUFFRDs7Ozs7O0FBSUEsU0FBTzJELGNBQVAsR0FBd0I7QUFDcEIsVUFBTTFFLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLFFBQUksQ0FBQ0YsTUFBTCxFQUFhO0FBQ1QsWUFBTSxJQUFJa0QsS0FBSixDQUFVLG9CQUFWLENBQU47QUFDSDs7QUFDRCxVQUFNcUIsV0FBVyxHQUFHdkUsTUFBTSxDQUFDMkMsY0FBUCxDQUFzQixXQUF0QixDQUFwQjs7QUFDQSxRQUFJNEIsV0FBVyxJQUFJQSxXQUFXLENBQUMvQixVQUFaLEVBQW5CLEVBQTZDO0FBQ3pDLGFBQU8rQixXQUFXLENBQUMvQixVQUFaLEVBQVA7QUFDSDs7QUFDRCxXQUFPLEVBQVA7QUFDSDtBQUVEOzs7Ozs7QUFJQSxTQUFPd0QsbUJBQVAsR0FBNkI7QUFDekIsV0FBT0MsTUFBTSxDQUFDQyxNQUFQLENBQWN2RyxXQUFXLENBQUMrRSxjQUFaLEVBQWQsQ0FBUDtBQUNIO0FBRUQ7Ozs7OztBQUlBLFNBQU95Qix1QkFBUCxHQUFpQztBQUM3QixVQUFNQyxPQUFPLEdBQUd6RyxXQUFXLENBQUNxRyxtQkFBWixFQUFoQjtBQUNBLFdBQU9JLE9BQU8sQ0FBQ0wsTUFBUixDQUFnQk0sTUFBRCxJQUFZQSxNQUFNLENBQUNsQyxPQUFQLElBQWtCa0MsTUFBTSxDQUFDbEMsT0FBUCxDQUFlQyxJQUFmLEtBQXdCLGlCQUFyRSxDQUFQO0FBQ0g7QUFFRDs7Ozs7O0FBSUEsU0FBT2tDLDRCQUFQLEdBQXNDO0FBQ2xDLFVBQU1GLE9BQU8sR0FBR3pHLFdBQVcsQ0FBQ3FHLG1CQUFaLEVBQWhCO0FBQ0EsV0FBT0ksT0FBTyxDQUFDTCxNQUFSLENBQWVRLENBQUMsSUFBSUEsQ0FBQyxDQUFDcEMsT0FBRixJQUFhb0MsQ0FBQyxDQUFDcEMsT0FBRixDQUFVQyxJQUFWLEtBQW1CLHVCQUFwRCxDQUFQO0FBQ0g7O0FBRUQsU0FBT29DLCtCQUFQLEdBQXlDO0FBQ3JDLFVBQU14RyxNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxRQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNULFlBQU0sSUFBSWtELEtBQUosQ0FBVSxvQkFBVixDQUFOO0FBQ0g7O0FBQ0QsVUFBTWtELE9BQU8sR0FBR3BHLE1BQU0sQ0FBQzJDLGNBQVAsQ0FBc0IsV0FBdEIsQ0FBaEI7QUFDQSxRQUFJLENBQUN5RCxPQUFMLEVBQWM7QUFDZCxVQUFNN0IsV0FBVyxHQUFHNkIsT0FBTyxDQUFDNUQsVUFBUixNQUF3QixFQUE1QztBQUNBeUQsSUFBQUEsTUFBTSxDQUFDUSxPQUFQLENBQWVsQyxXQUFmLEVBQTRCbUMsT0FBNUIsQ0FBb0MsQ0FBQyxDQUFDQyxHQUFELEVBQU1OLE1BQU4sQ0FBRCxLQUFtQjtBQUNuRCxVQUFJQSxNQUFNLENBQUNsQyxPQUFQLElBQWtCa0MsTUFBTSxDQUFDbEMsT0FBUCxDQUFlQyxJQUFmLEtBQXdCLHVCQUE5QyxFQUF1RTtBQUNuRSxlQUFPRyxXQUFXLENBQUNvQyxHQUFELENBQWxCO0FBQ0g7QUFDSixLQUpEO0FBS0EsV0FBTzNHLE1BQU0sQ0FBQ2tGLGNBQVAsQ0FBc0IsV0FBdEIsRUFBbUNYLFdBQW5DLENBQVA7QUFDSDs7QUFFRCxTQUFPcUMsMkJBQVAsQ0FBbUN2QztBQUFuQztBQUFBLElBQWlEd0M7QUFBakQ7QUFBQSxJQUFnRXBGO0FBQWhFO0FBQUEsSUFBZ0Y7QUFDNUUsV0FBTzlCLFdBQVcsQ0FBQ21FLGFBQVosQ0FDSCx5QkFBMEIsSUFBSWdELElBQUosR0FBV0MsT0FBWCxFQUR2QixFQUVILHVCQUZHLEVBR0hGLEtBSEcsRUFJSCwwQkFBMEJ4QyxJQUp2QixFQUtIO0FBQUMsaUJBQVc1QztBQUFaLEtBTEcsQ0FBUDtBQU9IO0FBRUQ7Ozs7OztBQUlBLFNBQU91RiwwQkFBUCxHQUFvQztBQUNoQyxVQUFNaEgsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsUUFBSSxDQUFDRixNQUFMLEVBQWE7QUFDVCxZQUFNLElBQUlrRCxLQUFKLENBQVUsb0JBQVYsQ0FBTjtBQUNIOztBQUNELFVBQU1rRCxPQUFPLEdBQUdwRyxNQUFNLENBQUMyQyxjQUFQLENBQXNCLFdBQXRCLENBQWhCO0FBQ0EsUUFBSSxDQUFDeUQsT0FBTCxFQUFjO0FBQ2QsVUFBTTdCLFdBQVcsR0FBRzZCLE9BQU8sQ0FBQzVELFVBQVIsTUFBd0IsRUFBNUM7QUFDQXlELElBQUFBLE1BQU0sQ0FBQ1EsT0FBUCxDQUFlbEMsV0FBZixFQUE0Qm1DLE9BQTVCLENBQW9DLENBQUMsQ0FBQ0MsR0FBRCxFQUFNTixNQUFOLENBQUQsS0FBbUI7QUFDbkQsVUFBSUEsTUFBTSxDQUFDbEMsT0FBUCxJQUFrQmtDLE1BQU0sQ0FBQ2xDLE9BQVAsQ0FBZUMsSUFBZixLQUF3QixpQkFBOUMsRUFBaUU7QUFDN0QsZUFBT0csV0FBVyxDQUFDb0MsR0FBRCxDQUFsQjtBQUNIO0FBQ0osS0FKRDtBQUtBLFdBQU8zRyxNQUFNLENBQUNrRixjQUFQLENBQXNCLFdBQXRCLEVBQW1DWCxXQUFuQyxDQUFQO0FBQ0g7O0FBRUQsU0FBTzBDLGFBQVAsQ0FBcUJDLEtBQXJCLEVBQTRCQyxHQUE1QixFQUFpQ0MsWUFBakMsRUFBK0N2SCxNQUEvQyxFQUF1RHdILE9BQXZELEVBQWdFO0FBQzVELFFBQUksQ0FBQ0QsWUFBTCxFQUFtQjtBQUNmLFlBQU0sSUFBSWxFLEtBQUosQ0FBVSw2REFBVixDQUFOO0FBQ0g7O0FBQ0RpRSxJQUFBQSxHQUFHLENBQUNHLGFBQUosR0FBb0JGLFlBQXBCO0FBRUFELElBQUFBLEdBQUcsQ0FBQ2xDLEVBQUosR0FBU2lDLEtBQVQ7QUFDQUMsSUFBQUEsR0FBRyxDQUFDRSxPQUFKLEdBQWNBLE9BQWQ7QUFDQUYsSUFBQUEsR0FBRyxDQUFDOUMsSUFBSixHQUFXOEMsR0FBRyxDQUFDOUMsSUFBSixJQUFZOEMsR0FBRyxDQUFDL0MsSUFBM0I7QUFFQSxXQUFPK0MsR0FBUDtBQUNIOztBQUVELFNBQU9JLGlDQUFQLENBQXlDQyxPQUF6QyxFQUFrRDNILE1BQWxELEVBQTBEO0FBQ3RELFVBQU00SCxpQkFBaUIsR0FBR0MsdUJBQWNDLFFBQWQsQ0FBdUIseUJBQXZCLEVBQWtEOUgsTUFBbEQsQ0FBMUI7O0FBRUEsVUFBTStILFlBQVksR0FBR0gsaUJBQWlCLEdBQUcsQ0FBQ0ksc0JBQVdDLFVBQVosQ0FBSCxHQUE2QixFQUFuRSxDQUhzRCxDQUt0RDtBQUNBO0FBQ0E7O0FBQ0EsUUFBSU4sT0FBTyxLQUFLLE9BQWhCLEVBQXlCO0FBQ3JCSSxNQUFBQSxZQUFZLENBQUNHLElBQWIsQ0FBa0JGLHNCQUFXRyxjQUE3QjtBQUNIOztBQUVELFdBQU9KLFlBQVA7QUFDSDs7QUFFRCxTQUFPSyxvQkFBUCxDQUE0QmhHLFFBQTVCLEVBQXNDK0IsU0FBdEMsRUFBaURrRSxZQUFqRCxFQUErRDtBQUMzRCxRQUFJQyxjQUFjLEdBQUdDLDJCQUFrQnhFLFNBQWxCLENBQTRCM0IsUUFBNUIsQ0FBckI7O0FBRUEsUUFBSWlHLFlBQUosRUFBa0I7QUFDZCxZQUFNRyxVQUFVLEdBQUcxSSxXQUFXLENBQUNxRyxtQkFBWixHQUNkc0MsSUFEYyxDQUNSL0IsQ0FBRCxJQUFPQSxDQUFDLENBQUN0QixFQUFGLEtBQVNoRCxRQUFULElBQXFCc0UsQ0FBQyxDQUFDcEMsT0FBdkIsSUFBa0NvQyxDQUFDLENBQUNwQyxPQUFGLENBQVVwRCxHQUFWLEtBQWtCaUQsU0FEbEQsQ0FBbkI7O0FBR0EsVUFBSSxDQUFDcUUsVUFBTCxFQUFpQjtBQUNiLGNBQU0sSUFBSW5GLEtBQUosQ0FBVSw4Q0FBVixDQUFOO0FBQ0g7O0FBRURpRixNQUFBQSxjQUFjLEdBQUdFLFVBQVUsQ0FBQ3ZELE1BQTVCO0FBQ0g7O0FBRUQsUUFBSSxDQUFDcUQsY0FBTCxFQUFxQjtBQUNqQixZQUFNLElBQUlqRixLQUFKLENBQVUsMkNBQVYsQ0FBTjtBQUNIOztBQUVELFdBQU9xRixrQkFBa0IsV0FBSUosY0FBSixlQUF1Qm5FLFNBQXZCLEVBQXpCO0FBQ0g7O0FBRUQsU0FBT3dFLHVCQUFQLENBQStCQztBQUFnQztBQUFBLElBQUMsRUFBaEUsRUFBb0U7QUFDaEU7QUFDQSxVQUFNQyxXQUFXLEdBQUcsQ0FDaEIsMEJBRGdCLEVBRWhCLDRCQUZnQixFQUdoQiwwQkFIZ0IsRUFJaEIsa0NBSmdCLEVBS2hCLDhCQUxnQixFQU1oQix3QkFOZ0IsRUFPbEJDLElBUGtCLENBT2IsR0FQYSxDQUFwQjtBQVNBLFFBQUlDLE9BQU8sR0FBR0MsTUFBTSxDQUFDQyxRQUFyQjs7QUFDQSxRQUFJRCxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JsSCxRQUFoQixLQUE2QixRQUE3QixJQUF5QyxDQUFDNkcsSUFBSSxDQUFDTSxjQUFuRCxFQUFtRTtBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FILE1BQUFBLE9BQU8sR0FBRyxzQkFBVjtBQUNIOztBQUNELFVBQU03SCxHQUFHLEdBQUcsSUFBSWlJLEdBQUosQ0FBUSxnQkFBZ0JOLFdBQXhCLEVBQXFDRSxPQUFyQyxDQUFaLENBcEJnRSxDQW9CTDs7QUFDM0QsV0FBTzdILEdBQUcsQ0FBQ2tJLElBQVg7QUFDSDs7QUF0YTRCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVHJhdmlzIFJhbHN0b25cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IFNka0NvbmZpZyBmcm9tIFwiLi4vU2RrQ29uZmlnXCI7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCAqIGFzIHVybCBmcm9tIFwidXJsXCI7XHJcbmltcG9ydCBXaWRnZXRFY2hvU3RvcmUgZnJvbSAnLi4vc3RvcmVzL1dpZGdldEVjaG9TdG9yZSc7XHJcblxyXG4vLyBIb3cgbG9uZyB3ZSB3YWl0IGZvciB0aGUgc3RhdGUgZXZlbnQgZWNobyB0byBjb21lIGJhY2sgZnJvbSB0aGUgc2VydmVyXHJcbi8vIGJlZm9yZSB3YWl0Rm9yW1Jvb20vVXNlcl1XaWRnZXQgcmVqZWN0cyBpdHMgcHJvbWlzZVxyXG5jb25zdCBXSURHRVRfV0FJVF9USU1FID0gMjAwMDA7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuLi9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlXCI7XHJcbmltcG9ydCBBY3RpdmVXaWRnZXRTdG9yZSBmcm9tIFwiLi4vc3RvcmVzL0FjdGl2ZVdpZGdldFN0b3JlXCI7XHJcbmltcG9ydCB7SW50ZWdyYXRpb25NYW5hZ2Vyc30gZnJvbSBcIi4uL2ludGVncmF0aW9ucy9JbnRlZ3JhdGlvbk1hbmFnZXJzXCI7XHJcbmltcG9ydCB7Q2FwYWJpbGl0eX0gZnJvbSBcIi4uL3dpZGdldHMvV2lkZ2V0QXBpXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBXaWRnZXRVdGlscyB7XHJcbiAgICAvKiBSZXR1cm5zIHRydWUgaWYgdXNlciBpcyBhYmxlIHRvIHNlbmQgc3RhdGUgZXZlbnRzIHRvIG1vZGlmeSB3aWRnZXRzIGluIHRoaXMgcm9vbVxyXG4gICAgICogKERvZXMgbm90IGFwcGx5IHRvIG5vbi1yb29tLWJhc2VkIC8gdXNlciB3aWRnZXRzKVxyXG4gICAgICogQHBhcmFtIHJvb21JZCAtLSBUaGUgSUQgb2YgdGhlIHJvb20gdG8gY2hlY2tcclxuICAgICAqIEByZXR1cm4gQm9vbGVhbiAtLSB0cnVlIGlmIHRoZSB1c2VyIGNhbiBtb2RpZnkgd2lkZ2V0cyBpbiB0aGlzIHJvb21cclxuICAgICAqIEB0aHJvd3MgRXJyb3IgLS0gc3BlY2lmaWVzIHRoZSBlcnJvciByZWFzb25cclxuICAgICAqL1xyXG4gICAgc3RhdGljIGNhblVzZXJNb2RpZnlXaWRnZXRzKHJvb21JZCkge1xyXG4gICAgICAgIGlmICghcm9vbUlkKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gcm9vbSBJRCBzcGVjaWZpZWQnKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGlmICghY2xpZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignVXNlciBtdXN0IGJlIGJlIGxvZ2dlZCBpbicpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByb29tID0gY2xpZW50LmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGBSb29tIElEICR7cm9vbUlkfSBpcyBub3QgcmVjb2duaXNlZGApO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBtZSA9IGNsaWVudC5jcmVkZW50aWFscy51c2VySWQ7XHJcbiAgICAgICAgaWYgKCFtZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ0ZhaWxlZCB0byBnZXQgdXNlciBJRCcpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAocm9vbS5nZXRNeU1lbWJlcnNoaXAoKSAhPT0gXCJqb2luXCIpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGBVc2VyICR7bWV9IGlzIG5vdCBpbiByb29tICR7cm9vbUlkfWApO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcm9vbS5jdXJyZW50U3RhdGUubWF5U2VuZFN0YXRlRXZlbnQoJ2ltLnZlY3Rvci5tb2R1bGFyLndpZGdldHMnLCBtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgc3BlY2lmaWVkIHVybCBpcyBhIHNjYWxhciBVUkwsIHR5cGljYWxseSBodHRwczovL3NjYWxhci52ZWN0b3IuaW0vYXBpXHJcbiAgICAgKiBAcGFyYW0gIHtbdHlwZV19ICB0ZXN0VXJsU3RyaW5nIFVSTCB0byBjaGVja1xyXG4gICAgICogQHJldHVybiB7Qm9vbGVhbn0gVHJ1ZSBpZiBzcGVjaWZpZWQgVVJMIGlzIGEgc2NhbGFyIFVSTFxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgaXNTY2FsYXJVcmwodGVzdFVybFN0cmluZykge1xyXG4gICAgICAgIGlmICghdGVzdFVybFN0cmluZykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdTY2FsYXIgVVJMIGNoZWNrIGZhaWxlZC4gTm8gVVJMIHNwZWNpZmllZCcpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0ZXN0VXJsID0gdXJsLnBhcnNlKHRlc3RVcmxTdHJpbmcpO1xyXG4gICAgICAgIGxldCBzY2FsYXJVcmxzID0gU2RrQ29uZmlnLmdldCgpLmludGVncmF0aW9uc193aWRnZXRzX3VybHM7XHJcbiAgICAgICAgaWYgKCFzY2FsYXJVcmxzIHx8IHNjYWxhclVybHMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRlZmF1bHRNYW5hZ2VyID0gSW50ZWdyYXRpb25NYW5hZ2Vycy5zaGFyZWRJbnN0YW5jZSgpLmdldFByaW1hcnlNYW5hZ2VyKCk7XHJcbiAgICAgICAgICAgIGlmIChkZWZhdWx0TWFuYWdlcikge1xyXG4gICAgICAgICAgICAgICAgc2NhbGFyVXJscyA9IFtkZWZhdWx0TWFuYWdlci5hcGlVcmxdO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgc2NhbGFyVXJscyA9IFtdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNjYWxhclVybHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3Qgc2NhbGFyVXJsID0gdXJsLnBhcnNlKHNjYWxhclVybHNbaV0pO1xyXG4gICAgICAgICAgICBpZiAodGVzdFVybCAmJiBzY2FsYXJVcmwpIHtcclxuICAgICAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgICAgICB0ZXN0VXJsLnByb3RvY29sID09PSBzY2FsYXJVcmwucHJvdG9jb2wgJiZcclxuICAgICAgICAgICAgICAgICAgICB0ZXN0VXJsLmhvc3QgPT09IHNjYWxhclVybC5ob3N0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgdGVzdFVybC5wYXRobmFtZS5zdGFydHNXaXRoKHNjYWxhclVybC5wYXRobmFtZSlcclxuICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2hlbiBhIHdpZGdldCB3aXRoIHRoZSBnaXZlblxyXG4gICAgICogSUQgaGFzIGJlZW4gYWRkZWQgYXMgYSB1c2VyIHdpZGdldCAoaWUuIHRoZSBhY2NvdW50RGF0YSBldmVudFxyXG4gICAgICogYXJyaXZlcykgb3IgcmVqZWN0cyBhZnRlciBhIHRpbWVvdXRcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gd2lkZ2V0SWQgVGhlIElEIG9mIHRoZSB3aWRnZXQgdG8gd2FpdCBmb3JcclxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gYWRkIFRydWUgdG8gd2FpdCBmb3IgdGhlIHdpZGdldCB0byBiZSBhZGRlZCxcclxuICAgICAqICAgICBmYWxzZSB0byB3YWl0IGZvciBpdCB0byBiZSBkZWxldGVkLlxyXG4gICAgICogQHJldHVybnMge1Byb21pc2V9IHRoYXQgcmVzb2x2ZXMgd2hlbiB0aGUgd2lkZ2V0IGlzIGluIHRoZVxyXG4gICAgICogICAgIHJlcXVlc3RlZCBzdGF0ZSBhY2NvcmRpbmcgdG8gdGhlIGBhZGRgIHBhcmFtXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyB3YWl0Rm9yVXNlcldpZGdldCh3aWRnZXRJZCwgYWRkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgLy8gVGVzdHMgYW4gYWNjb3VudCBkYXRhIGV2ZW50LCByZXR1cm5pbmcgdHJ1ZSBpZiBpdCdzIGluIHRoZSBzdGF0ZVxyXG4gICAgICAgICAgICAvLyB3ZSdyZSB3YWl0aW5nIGZvciBpdCB0byBiZSBpblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBldmVudEluSW50ZW5kZWRTdGF0ZShldikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFldiB8fCAhZXYuZ2V0Q29udGVudCgpKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBpZiAoYWRkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2LmdldENvbnRlbnQoKVt3aWRnZXRJZF0gIT09IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2LmdldENvbnRlbnQoKVt3aWRnZXRJZF0gPT09IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3Qgc3RhcnRpbmdBY2NvdW50RGF0YUV2ZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldEFjY291bnREYXRhKCdtLndpZGdldHMnKTtcclxuICAgICAgICAgICAgaWYgKGV2ZW50SW5JbnRlbmRlZFN0YXRlKHN0YXJ0aW5nQWNjb3VudERhdGFFdmVudCkpIHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gb25BY2NvdW50RGF0YShldikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudEFjY291bnREYXRhRXZlbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0QWNjb3VudERhdGEoJ20ud2lkZ2V0cycpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50SW5JbnRlbmRlZFN0YXRlKGN1cnJlbnRBY2NvdW50RGF0YUV2ZW50KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcignYWNjb3VudERhdGEnLCBvbkFjY291bnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGltZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHRpbWVySWQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcignYWNjb3VudERhdGEnLCBvbkFjY291bnREYXRhKTtcclxuICAgICAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoXCJUaW1lZCBvdXQgd2FpdGluZyBmb3Igd2lkZ2V0IElEIFwiICsgd2lkZ2V0SWQgKyBcIiB0byBhcHBlYXJcIikpO1xyXG4gICAgICAgICAgICB9LCBXSURHRVRfV0FJVF9USU1FKTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKCdhY2NvdW50RGF0YScsIG9uQWNjb3VudERhdGEpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIGEgd2lkZ2V0IHdpdGggdGhlIGdpdmVuXHJcbiAgICAgKiBJRCBoYXMgYmVlbiBhZGRlZCBhcyBhIHJvb20gd2lkZ2V0IGluIHRoZSBnaXZlbiByb29tIChpZS4gdGhlXHJcbiAgICAgKiByb29tIHN0YXRlIGV2ZW50IGFycml2ZXMpIG9yIHJlamVjdHMgYWZ0ZXIgYSB0aW1lb3V0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHdpZGdldElkIFRoZSBJRCBvZiB0aGUgd2lkZ2V0IHRvIHdhaXQgZm9yXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcm9vbUlkIFRoZSBJRCBvZiB0aGUgcm9vbSB0byB3YWl0IGZvciB0aGUgd2lkZ2V0IGluXHJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IGFkZCBUcnVlIHRvIHdhaXQgZm9yIHRoZSB3aWRnZXQgdG8gYmUgYWRkZWQsXHJcbiAgICAgKiAgICAgZmFsc2UgdG8gd2FpdCBmb3IgaXQgdG8gYmUgZGVsZXRlZC5cclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfSB0aGF0IHJlc29sdmVzIHdoZW4gdGhlIHdpZGdldCBpcyBpbiB0aGVcclxuICAgICAqICAgICByZXF1ZXN0ZWQgc3RhdGUgYWNjb3JkaW5nIHRvIHRoZSBgYWRkYCBwYXJhbVxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgd2FpdEZvclJvb21XaWRnZXQod2lkZ2V0SWQsIHJvb21JZCwgYWRkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgLy8gVGVzdHMgYSBsaXN0IG9mIHN0YXRlIGV2ZW50cywgcmV0dXJuaW5nIHRydWUgaWYgaXQncyBpbiB0aGUgc3RhdGVcclxuICAgICAgICAgICAgLy8gd2UncmUgd2FpdGluZyBmb3IgaXQgdG8gYmUgaW5cclxuICAgICAgICAgICAgZnVuY3Rpb24gZXZlbnRzSW5JbnRlbmRlZFN0YXRlKGV2TGlzdCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd2lkZ2V0UHJlc2VudCA9IGV2TGlzdC5zb21lKChldikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldi5nZXRDb250ZW50KCkgJiYgZXYuZ2V0Q29udGVudCgpWydpZCddID09PSB3aWRnZXRJZDtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKGFkZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB3aWRnZXRQcmVzZW50O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gIXdpZGdldFByZXNlbnQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgICAgICAgICBjb25zdCBzdGFydGluZ1dpZGdldEV2ZW50cyA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKCdpbS52ZWN0b3IubW9kdWxhci53aWRnZXRzJyk7XHJcbiAgICAgICAgICAgIGlmIChldmVudHNJbkludGVuZGVkU3RhdGUoc3RhcnRpbmdXaWRnZXRFdmVudHMpKSB7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIG9uUm9vbVN0YXRlRXZlbnRzKGV2KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXYuZ2V0Um9vbUlkKCkgIT09IHJvb21JZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRXaWRnZXRFdmVudHMgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnaW0udmVjdG9yLm1vZHVsYXIud2lkZ2V0cycpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChldmVudHNJbkludGVuZGVkU3RhdGUoY3VycmVudFdpZGdldEV2ZW50cykpIHtcclxuICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoJ1Jvb21TdGF0ZS5ldmVudHMnLCBvblJvb21TdGF0ZUV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRpbWVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCB0aW1lcklkID0gc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoJ1Jvb21TdGF0ZS5ldmVudHMnLCBvblJvb21TdGF0ZUV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICByZWplY3QobmV3IEVycm9yKFwiVGltZWQgb3V0IHdhaXRpbmcgZm9yIHdpZGdldCBJRCBcIiArIHdpZGdldElkICsgXCIgdG8gYXBwZWFyXCIpKTtcclxuICAgICAgICAgICAgfSwgV0lER0VUX1dBSVRfVElNRSk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5vbignUm9vbVN0YXRlLmV2ZW50cycsIG9uUm9vbVN0YXRlRXZlbnRzKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgc2V0VXNlcldpZGdldCh3aWRnZXRJZCwgd2lkZ2V0VHlwZSwgd2lkZ2V0VXJsLCB3aWRnZXROYW1lLCB3aWRnZXREYXRhKSB7XHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IHtcclxuICAgICAgICAgICAgdHlwZTogd2lkZ2V0VHlwZSxcclxuICAgICAgICAgICAgdXJsOiB3aWRnZXRVcmwsXHJcbiAgICAgICAgICAgIG5hbWU6IHdpZGdldE5hbWUsXHJcbiAgICAgICAgICAgIGRhdGE6IHdpZGdldERhdGEsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIC8vIEdldCB0aGUgY3VycmVudCB3aWRnZXRzIGFuZCBjbG9uZSB0aGVtIGJlZm9yZSB3ZSBtb2RpZnkgdGhlbSwgb3RoZXJ3aXNlXHJcbiAgICAgICAgLy8gd2UnbGwgbW9kaWZ5IHRoZSBjb250ZW50IG9mIHRoZSBvbGQgZXZlbnQuXHJcbiAgICAgICAgY29uc3QgdXNlcldpZGdldHMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KFdpZGdldFV0aWxzLmdldFVzZXJXaWRnZXRzKCkpKTtcclxuXHJcbiAgICAgICAgLy8gRGVsZXRlIGV4aXN0aW5nIHdpZGdldCB3aXRoIElEXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgZGVsZXRlIHVzZXJXaWRnZXRzW3dpZGdldElkXTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoYCR3aWRnZXRJZCBpcyBub24tY29uZmlndXJhYmxlYCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBhZGRpbmdXaWRnZXQgPSBCb29sZWFuKHdpZGdldFVybCk7XHJcblxyXG4gICAgICAgIC8vIEFkZCBuZXcgd2lkZ2V0IC8gdXBkYXRlXHJcbiAgICAgICAgaWYgKGFkZGluZ1dpZGdldCkge1xyXG4gICAgICAgICAgICB1c2VyV2lkZ2V0c1t3aWRnZXRJZF0gPSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBjb250ZW50LFxyXG4gICAgICAgICAgICAgICAgc2VuZGVyOiBjbGllbnQuZ2V0VXNlcklkKCksXHJcbiAgICAgICAgICAgICAgICBzdGF0ZV9rZXk6IHdpZGdldElkLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ20ud2lkZ2V0JyxcclxuICAgICAgICAgICAgICAgIGlkOiB3aWRnZXRJZCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRoaXMgc3RhcnRzIGxpc3RlbmluZyBmb3Igd2hlbiB0aGUgZWNobyBjb21lcyBiYWNrIGZyb20gdGhlIHNlcnZlclxyXG4gICAgICAgIC8vIHNpbmNlIHRoZSB3aWRnZXQgd29uJ3QgYXBwZWFyIGFkZGVkIHVudGlsIHRoaXMgaGFwcGVucy4gSWYgd2UgZG9uJ3RcclxuICAgICAgICAvLyB3YWl0IGZvciB0aGlzLCB0aGUgYWN0aW9uIHdpbGwgY29tcGxldGUgYnV0IGlmIHRoZSB1c2VyIGlzIGZhc3QgZW5vdWdoLFxyXG4gICAgICAgIC8vIHRoZSB3aWRnZXQgc3RpbGwgd29uJ3QgYWN0dWFsbHkgYmUgdGhlcmUuXHJcbiAgICAgICAgcmV0dXJuIGNsaWVudC5zZXRBY2NvdW50RGF0YSgnbS53aWRnZXRzJywgdXNlcldpZGdldHMpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gV2lkZ2V0VXRpbHMud2FpdEZvclVzZXJXaWRnZXQod2lkZ2V0SWQsIGFkZGluZ1dpZGdldCk7XHJcbiAgICAgICAgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogXCJ1c2VyX3dpZGdldF91cGRhdGVkXCIgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHNldFJvb21XaWRnZXQocm9vbUlkLCB3aWRnZXRJZCwgd2lkZ2V0VHlwZSwgd2lkZ2V0VXJsLCB3aWRnZXROYW1lLCB3aWRnZXREYXRhKSB7XHJcbiAgICAgICAgbGV0IGNvbnRlbnQ7XHJcblxyXG4gICAgICAgIGNvbnN0IGFkZGluZ1dpZGdldCA9IEJvb2xlYW4od2lkZ2V0VXJsKTtcclxuXHJcbiAgICAgICAgaWYgKGFkZGluZ1dpZGdldCkge1xyXG4gICAgICAgICAgICBjb250ZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogd2lkZ2V0VHlwZSxcclxuICAgICAgICAgICAgICAgIHVybDogd2lkZ2V0VXJsLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogd2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHdpZGdldERhdGEsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29udGVudCA9IHt9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgV2lkZ2V0RWNob1N0b3JlLnNldFJvb21XaWRnZXRFY2hvKHJvb21JZCwgd2lkZ2V0SWQsIGNvbnRlbnQpO1xyXG5cclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgLy8gVE9ETyAtIFJvb20gd2lkZ2V0cyBuZWVkIHRvIGJlIG1vdmVkIHRvICdtLndpZGdldCcgc3RhdGUgZXZlbnRzXHJcbiAgICAgICAgLy8gaHR0cHM6Ly9kb2NzLmdvb2dsZS5jb20vZG9jdW1lbnQvZC8xdVBGN1hXWV9kWFRLVktWN2paUTJLbXNJMTl3bjkta0ZSZ1ExdEZRUDd3US9lZGl0P3VzcD1zaGFyaW5nXHJcbiAgICAgICAgcmV0dXJuIGNsaWVudC5zZW5kU3RhdGVFdmVudChyb29tSWQsIFwiaW0udmVjdG9yLm1vZHVsYXIud2lkZ2V0c1wiLCBjb250ZW50LCB3aWRnZXRJZCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBXaWRnZXRVdGlscy53YWl0Rm9yUm9vbVdpZGdldCh3aWRnZXRJZCwgcm9vbUlkLCBhZGRpbmdXaWRnZXQpO1xyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICBXaWRnZXRFY2hvU3RvcmUucmVtb3ZlUm9vbVdpZGdldEVjaG8ocm9vbUlkLCB3aWRnZXRJZCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgcm9vbSBzcGVjaWZpYyB3aWRnZXRzXHJcbiAgICAgKiBAcGFyYW0gIHtvYmplY3R9IHJvb20gVGhlIHJvb20gdG8gZ2V0IHdpZGdldHMgZm9yY2VcclxuICAgICAqIEByZXR1cm4ge1tvYmplY3RdfSBBcnJheSBjb250YWluaW5nIGN1cnJlbnQgLyBhY3RpdmUgcm9vbSB3aWRnZXRzXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBnZXRSb29tV2lkZ2V0cyhyb29tKSB7XHJcbiAgICAgICAgY29uc3QgYXBwc1N0YXRlRXZlbnRzID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ2ltLnZlY3Rvci5tb2R1bGFyLndpZGdldHMnKTtcclxuICAgICAgICBpZiAoIWFwcHNTdGF0ZUV2ZW50cykge1xyXG4gICAgICAgICAgICByZXR1cm4gW107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYXBwc1N0YXRlRXZlbnRzLmZpbHRlcigoZXYpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGV2LmdldENvbnRlbnQoKS50eXBlICYmIGV2LmdldENvbnRlbnQoKS51cmw7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdXNlciBzcGVjaWZpYyB3aWRnZXRzIChub3QgbGlua2VkIHRvIGEgc3BlY2lmaWMgcm9vbSlcclxuICAgICAqIEByZXR1cm4ge29iamVjdH0gRXZlbnQgY29udGVudCBvYmplY3QgY29udGFpbmluZyBjdXJyZW50IC8gYWN0aXZlIHVzZXIgd2lkZ2V0c1xyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0VXNlcldpZGdldHMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGlmICghY2xpZW50KSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVXNlciBub3QgbG9nZ2VkIGluJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHVzZXJXaWRnZXRzID0gY2xpZW50LmdldEFjY291bnREYXRhKCdtLndpZGdldHMnKTtcclxuICAgICAgICBpZiAodXNlcldpZGdldHMgJiYgdXNlcldpZGdldHMuZ2V0Q29udGVudCgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB1c2VyV2lkZ2V0cy5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB7fTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB1c2VyIHNwZWNpZmljIHdpZGdldHMgKG5vdCBsaW5rZWQgdG8gYSBzcGVjaWZpYyByb29tKSBhcyBhbiBhcnJheVxyXG4gICAgICogQHJldHVybiB7W29iamVjdF19IEFycmF5IGNvbnRhaW5pbmcgY3VycmVudCAvIGFjdGl2ZSB1c2VyIHdpZGdldHNcclxuICAgICAqL1xyXG4gICAgc3RhdGljIGdldFVzZXJXaWRnZXRzQXJyYXkoKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC52YWx1ZXMoV2lkZ2V0VXRpbHMuZ2V0VXNlcldpZGdldHMoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYWN0aXZlIHN0aWNrZXJwaWNrZXIgd2lkZ2V0cyAoc3RpY2tlcnBpY2tlcnMgYXJlIHVzZXIgd2lkZ2V0cyBieSBuYXR1cmUpXHJcbiAgICAgKiBAcmV0dXJuIHtbb2JqZWN0XX0gQXJyYXkgY29udGFpbmluZyBjdXJyZW50IC8gYWN0aXZlIHN0aWNrZXJwaWNrZXIgd2lkZ2V0c1xyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0U3RpY2tlcnBpY2tlcldpZGdldHMoKSB7XHJcbiAgICAgICAgY29uc3Qgd2lkZ2V0cyA9IFdpZGdldFV0aWxzLmdldFVzZXJXaWRnZXRzQXJyYXkoKTtcclxuICAgICAgICByZXR1cm4gd2lkZ2V0cy5maWx0ZXIoKHdpZGdldCkgPT4gd2lkZ2V0LmNvbnRlbnQgJiYgd2lkZ2V0LmNvbnRlbnQudHlwZSA9PT0gXCJtLnN0aWNrZXJwaWNrZXJcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYWxsIGludGVncmF0aW9uIG1hbmFnZXIgd2lkZ2V0cyBmb3IgdGhpcyB1c2VyLlxyXG4gICAgICogQHJldHVybnMge09iamVjdFtdfSBBbiBhcnJheSBvZiBpbnRlZ3JhdGlvbiBtYW5hZ2VyIHVzZXIgd2lkZ2V0cy5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIGdldEludGVncmF0aW9uTWFuYWdlcldpZGdldHMoKSB7XHJcbiAgICAgICAgY29uc3Qgd2lkZ2V0cyA9IFdpZGdldFV0aWxzLmdldFVzZXJXaWRnZXRzQXJyYXkoKTtcclxuICAgICAgICByZXR1cm4gd2lkZ2V0cy5maWx0ZXIodyA9PiB3LmNvbnRlbnQgJiYgdy5jb250ZW50LnR5cGUgPT09IFwibS5pbnRlZ3JhdGlvbl9tYW5hZ2VyXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyByZW1vdmVJbnRlZ3JhdGlvbk1hbmFnZXJXaWRnZXRzKCkge1xyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1VzZXIgbm90IGxvZ2dlZCBpbicpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB3aWRnZXRzID0gY2xpZW50LmdldEFjY291bnREYXRhKCdtLndpZGdldHMnKTtcclxuICAgICAgICBpZiAoIXdpZGdldHMpIHJldHVybjtcclxuICAgICAgICBjb25zdCB1c2VyV2lkZ2V0cyA9IHdpZGdldHMuZ2V0Q29udGVudCgpIHx8IHt9O1xyXG4gICAgICAgIE9iamVjdC5lbnRyaWVzKHVzZXJXaWRnZXRzKS5mb3JFYWNoKChba2V5LCB3aWRnZXRdKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh3aWRnZXQuY29udGVudCAmJiB3aWRnZXQuY29udGVudC50eXBlID09PSBcIm0uaW50ZWdyYXRpb25fbWFuYWdlclwiKSB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgdXNlcldpZGdldHNba2V5XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBjbGllbnQuc2V0QWNjb3VudERhdGEoJ20ud2lkZ2V0cycsIHVzZXJXaWRnZXRzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgYWRkSW50ZWdyYXRpb25NYW5hZ2VyV2lkZ2V0KG5hbWU6IHN0cmluZywgdWlVcmw6IHN0cmluZywgYXBpVXJsOiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gV2lkZ2V0VXRpbHMuc2V0VXNlcldpZGdldChcclxuICAgICAgICAgICAgXCJpbnRlZ3JhdGlvbl9tYW5hZ2VyX1wiICsgKG5ldyBEYXRlKCkuZ2V0VGltZSgpKSxcclxuICAgICAgICAgICAgXCJtLmludGVncmF0aW9uX21hbmFnZXJcIixcclxuICAgICAgICAgICAgdWlVcmwsXHJcbiAgICAgICAgICAgIFwiSW50ZWdyYXRpb24gTWFuYWdlcjogXCIgKyBuYW1lLFxyXG4gICAgICAgICAgICB7XCJhcGlfdXJsXCI6IGFwaVVybH0sXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSBhbGwgc3RpY2tlcnBpY2tlciB3aWRnZXRzIChzdGlja2VycGlja2VycyBhcmUgdXNlciB3aWRnZXRzIGJ5IG5hdHVyZSlcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IFJlc29sdmVzIG9uIGFjY291bnQgZGF0YSB1cGRhdGVkXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyByZW1vdmVTdGlja2VycGlja2VyV2lkZ2V0cygpIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFjbGllbnQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdVc2VyIG5vdCBsb2dnZWQgaW4nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3Qgd2lkZ2V0cyA9IGNsaWVudC5nZXRBY2NvdW50RGF0YSgnbS53aWRnZXRzJyk7XHJcbiAgICAgICAgaWYgKCF3aWRnZXRzKSByZXR1cm47XHJcbiAgICAgICAgY29uc3QgdXNlcldpZGdldHMgPSB3aWRnZXRzLmdldENvbnRlbnQoKSB8fCB7fTtcclxuICAgICAgICBPYmplY3QuZW50cmllcyh1c2VyV2lkZ2V0cykuZm9yRWFjaCgoW2tleSwgd2lkZ2V0XSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAod2lkZ2V0LmNvbnRlbnQgJiYgd2lkZ2V0LmNvbnRlbnQudHlwZSA9PT0gJ20uc3RpY2tlcnBpY2tlcicpIHtcclxuICAgICAgICAgICAgICAgIGRlbGV0ZSB1c2VyV2lkZ2V0c1trZXldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGNsaWVudC5zZXRBY2NvdW50RGF0YSgnbS53aWRnZXRzJywgdXNlcldpZGdldHMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBtYWtlQXBwQ29uZmlnKGFwcElkLCBhcHAsIHNlbmRlclVzZXJJZCwgcm9vbUlkLCBldmVudElkKSB7XHJcbiAgICAgICAgaWYgKCFzZW5kZXJVc2VySWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiV2lkZ2V0cyBtdXN0IGJlIGNyZWF0ZWQgYnkgc29tZW9uZSAtIHByb3ZpZGUgYSBzZW5kZXJVc2VySWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFwcC5jcmVhdG9yVXNlcklkID0gc2VuZGVyVXNlcklkO1xyXG5cclxuICAgICAgICBhcHAuaWQgPSBhcHBJZDtcclxuICAgICAgICBhcHAuZXZlbnRJZCA9IGV2ZW50SWQ7XHJcbiAgICAgICAgYXBwLm5hbWUgPSBhcHAubmFtZSB8fCBhcHAudHlwZTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGFwcDtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0Q2FwV2hpdGVsaXN0Rm9yQXBwVHlwZUluUm9vbUlkKGFwcFR5cGUsIHJvb21JZCkge1xyXG4gICAgICAgIGNvbnN0IGVuYWJsZVNjcmVlbnNob3RzID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImVuYWJsZVdpZGdldFNjcmVlbnNob3RzXCIsIHJvb21JZCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGNhcFdoaXRlbGlzdCA9IGVuYWJsZVNjcmVlbnNob3RzID8gW0NhcGFiaWxpdHkuU2NyZWVuc2hvdF0gOiBbXTtcclxuXHJcbiAgICAgICAgLy8gT2J2aW91c2x5IGFueW9uZSB0aGF0IGNhbiBhZGQgYSB3aWRnZXQgY2FuIGNsYWltIGl0J3MgYSBqaXRzaSB3aWRnZXQsXHJcbiAgICAgICAgLy8gc28gdGhpcyBkb2Vzbid0IHJlYWxseSBvZmZlciBtdWNoIG92ZXIgdGhlIHNldCBvZiBkb21haW5zIHdlIGxvYWRcclxuICAgICAgICAvLyB3aWRnZXRzIGZyb20gYXQgYWxsLCBidXQgaXQgcHJvYmFibHkgbWFrZXMgc2Vuc2UgZm9yIHNhbml0eS5cclxuICAgICAgICBpZiAoYXBwVHlwZSA9PT0gJ2ppdHNpJykge1xyXG4gICAgICAgICAgICBjYXBXaGl0ZWxpc3QucHVzaChDYXBhYmlsaXR5LkFsd2F5c09uU2NyZWVuKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBjYXBXaGl0ZWxpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGdldFdpZGdldFNlY3VyaXR5S2V5KHdpZGdldElkLCB3aWRnZXRVcmwsIGlzVXNlcldpZGdldCkge1xyXG4gICAgICAgIGxldCB3aWRnZXRMb2NhdGlvbiA9IEFjdGl2ZVdpZGdldFN0b3JlLmdldFJvb21JZCh3aWRnZXRJZCk7XHJcblxyXG4gICAgICAgIGlmIChpc1VzZXJXaWRnZXQpIHtcclxuICAgICAgICAgICAgY29uc3QgdXNlcldpZGdldCA9IFdpZGdldFV0aWxzLmdldFVzZXJXaWRnZXRzQXJyYXkoKVxyXG4gICAgICAgICAgICAgICAgLmZpbmQoKHcpID0+IHcuaWQgPT09IHdpZGdldElkICYmIHcuY29udGVudCAmJiB3LmNvbnRlbnQudXJsID09PSB3aWRnZXRVcmwpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCF1c2VyV2lkZ2V0KSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBtYXRjaGluZyB1c2VyIHdpZGdldCB0byBmb3JtIHNlY3VyaXR5IGtleVwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgd2lkZ2V0TG9jYXRpb24gPSB1c2VyV2lkZ2V0LnNlbmRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghd2lkZ2V0TG9jYXRpb24pIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRmFpbGVkIHRvIGxvY2F0ZSB3aGVyZSB0aGUgd2lkZ2V0IHJlc2lkZXNcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGAke3dpZGdldExvY2F0aW9ufTo6JHt3aWRnZXRVcmx9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGdldExvY2FsSml0c2lXcmFwcGVyVXJsKG9wdHM6IHtmb3JMb2NhbFJlbmRlcj86IGJvb2xlYW59PXt9KSB7XHJcbiAgICAgICAgLy8gTkIuIHdlIGNhbid0IGp1c3QgZW5jb2RlVVJJQ29tcG9uZW50IGFsbCBvZiB0aGVzZSBiZWNhdXNlIHRoZSAkIHNpZ25zIG5lZWQgdG8gYmUgdGhlcmVcclxuICAgICAgICBjb25zdCBxdWVyeVN0cmluZyA9IFtcclxuICAgICAgICAgICAgJ2NvbmZlcmVuY2VEb21haW49JGRvbWFpbicsXHJcbiAgICAgICAgICAgICdjb25mZXJlbmNlSWQ9JGNvbmZlcmVuY2VJZCcsXHJcbiAgICAgICAgICAgICdpc0F1ZGlvT25seT0kaXNBdWRpb09ubHknLFxyXG4gICAgICAgICAgICAnZGlzcGxheU5hbWU9JG1hdHJpeF9kaXNwbGF5X25hbWUnLFxyXG4gICAgICAgICAgICAnYXZhdGFyVXJsPSRtYXRyaXhfYXZhdGFyX3VybCcsXHJcbiAgICAgICAgICAgICd1c2VySWQ9JG1hdHJpeF91c2VyX2lkJyxcclxuICAgICAgICBdLmpvaW4oJyYnKTtcclxuXHJcbiAgICAgICAgbGV0IGJhc2VVcmwgPSB3aW5kb3cubG9jYXRpb247XHJcbiAgICAgICAgaWYgKHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCAhPT0gXCJodHRwczpcIiAmJiAhb3B0cy5mb3JMb2NhbFJlbmRlcikge1xyXG4gICAgICAgICAgICAvLyBVc2UgYW4gZXh0ZXJuYWwgd3JhcHBlciBpZiB3ZSdyZSBub3QgbG9jYWxseSByZW5kZXJpbmcgdGhlIHdpZGdldC4gVGhpcyBpcyB1c3VhbGx5XHJcbiAgICAgICAgICAgIC8vIHRoZSBVUkwgdGhhdCB3aWxsIGVuZCB1cCBpbiB0aGUgd2lkZ2V0IGV2ZW50LCBzbyB3ZSB3YW50IHRvIG1ha2Ugc3VyZSBpdCdzIHJlbGF0aXZlbHlcclxuICAgICAgICAgICAgLy8gc2FmZSB0byBzZW5kLlxyXG4gICAgICAgICAgICAvLyBXZSdsbCBlbmQgdXAgdXNpbmcgYSBsb2NhbCByZW5kZXIgVVJMIHdoZW4gd2Ugc2VlIGEgSml0c2kgd2lkZ2V0IGFueXdheXMsIHNvIHRoaXMgaXNcclxuICAgICAgICAgICAgLy8gcmVhbGx5IGp1c3QgZm9yIGJhY2t3YXJkcyBjb21wYXRpYmlsaXR5IGFuZCB0byBhcHBlYXNlIHRoZSBzcGVjLlxyXG4gICAgICAgICAgICBiYXNlVXJsID0gXCJodHRwczovL3Jpb3QuaW0vYXBwL1wiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmwgPSBuZXcgVVJMKFwiaml0c2kuaHRtbCNcIiArIHF1ZXJ5U3RyaW5nLCBiYXNlVXJsKTsgLy8gdGhpcyBzdHJpcHMgaGFzaCBmcmFnbWVudCBmcm9tIGJhc2VVcmxcclxuICAgICAgICByZXR1cm4gdXJsLmhyZWY7XHJcbiAgICB9XHJcbn1cclxuIl19