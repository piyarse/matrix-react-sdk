"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
A countdown timer, exposing a promise api.
A timer starts in a non-started state,
and needs to be started by calling `start()`` on it first.

Timers can be `abort()`-ed which makes the promise reject prematurely.

Once a timer is finished or aborted, it can't be started again
(because the promise should not be replaced). Instead, create
a new one through `clone()` or `cloneIfRun()`.
*/
class Timer {
  constructor(timeout) {
    this._timeout = timeout;
    this._onTimeout = this._onTimeout.bind(this);

    this._setNotStarted();
  }

  _setNotStarted() {
    this._timerHandle = null;
    this._startTs = null;
    this._promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
      this._reject = reject;
    }).finally(() => {
      this._timerHandle = null;
    });
  }

  _onTimeout() {
    const now = Date.now();
    const elapsed = now - this._startTs;

    if (elapsed >= this._timeout) {
      this._resolve();

      this._setNotStarted();
    } else {
      const delta = this._timeout - elapsed;
      this._timerHandle = setTimeout(this._onTimeout, delta);
    }
  }

  changeTimeout(timeout) {
    if (timeout === this._timeout) {
      return;
    }

    const isSmallerTimeout = timeout < this._timeout;
    this._timeout = timeout;

    if (this.isRunning() && isSmallerTimeout) {
      clearTimeout(this._timerHandle);

      this._onTimeout();
    }
  }
  /**
   * if not started before, starts the timer.
   * @returns {Timer} the same timer
   */


  start() {
    if (!this.isRunning()) {
      this._startTs = Date.now();
      this._timerHandle = setTimeout(this._onTimeout, this._timeout);
    }

    return this;
  }
  /**
   * (re)start the timer. If it's running, reset the timeout. If not, start it.
   * @returns {Timer} the same timer
   */


  restart() {
    if (this.isRunning()) {
      // don't clearTimeout here as this method
      // can be called in fast succession,
      // instead just take note and compare
      // when the already running timeout expires
      this._startTs = Date.now();
      return this;
    } else {
      return this.start();
    }
  }
  /**
   * if the timer is running, abort it,
   * and reject the promise for this timer.
   * @returns {Timer} the same timer
   */


  abort() {
    if (this.isRunning()) {
      clearTimeout(this._timerHandle);

      this._reject(new Error("Timer was aborted."));

      this._setNotStarted();
    }

    return this;
  }
  /**
   *promise that will resolve when the timer elapses,
   *or is rejected when abort is called
   *@return {Promise}
   */


  finished() {
    return this._promise;
  }

  isRunning() {
    return this._timerHandle !== null;
  }

}

exports.default = Timer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9UaW1lci5qcyJdLCJuYW1lcyI6WyJUaW1lciIsImNvbnN0cnVjdG9yIiwidGltZW91dCIsIl90aW1lb3V0IiwiX29uVGltZW91dCIsImJpbmQiLCJfc2V0Tm90U3RhcnRlZCIsIl90aW1lckhhbmRsZSIsIl9zdGFydFRzIiwiX3Byb21pc2UiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsIl9yZXNvbHZlIiwiX3JlamVjdCIsImZpbmFsbHkiLCJub3ciLCJEYXRlIiwiZWxhcHNlZCIsImRlbHRhIiwic2V0VGltZW91dCIsImNoYW5nZVRpbWVvdXQiLCJpc1NtYWxsZXJUaW1lb3V0IiwiaXNSdW5uaW5nIiwiY2xlYXJUaW1lb3V0Iiwic3RhcnQiLCJyZXN0YXJ0IiwiYWJvcnQiLCJFcnJvciIsImZpbmlzaGVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7Ozs7Ozs7Ozs7O0FBV2UsTUFBTUEsS0FBTixDQUFZO0FBQ3ZCQyxFQUFBQSxXQUFXLENBQUNDLE9BQUQsRUFBVTtBQUNqQixTQUFLQyxRQUFMLEdBQWdCRCxPQUFoQjtBQUNBLFNBQUtFLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxDQUFnQkMsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBbEI7O0FBQ0EsU0FBS0MsY0FBTDtBQUNIOztBQUVEQSxFQUFBQSxjQUFjLEdBQUc7QUFDYixTQUFLQyxZQUFMLEdBQW9CLElBQXBCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsSUFBSUMsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUM3QyxXQUFLQyxRQUFMLEdBQWdCRixPQUFoQjtBQUNBLFdBQUtHLE9BQUwsR0FBZUYsTUFBZjtBQUNILEtBSGUsRUFHYkcsT0FIYSxDQUdMLE1BQU07QUFDYixXQUFLUixZQUFMLEdBQW9CLElBQXBCO0FBQ0gsS0FMZSxDQUFoQjtBQU1IOztBQUVESCxFQUFBQSxVQUFVLEdBQUc7QUFDVCxVQUFNWSxHQUFHLEdBQUdDLElBQUksQ0FBQ0QsR0FBTCxFQUFaO0FBQ0EsVUFBTUUsT0FBTyxHQUFHRixHQUFHLEdBQUcsS0FBS1IsUUFBM0I7O0FBQ0EsUUFBSVUsT0FBTyxJQUFJLEtBQUtmLFFBQXBCLEVBQThCO0FBQzFCLFdBQUtVLFFBQUw7O0FBQ0EsV0FBS1AsY0FBTDtBQUNILEtBSEQsTUFHTztBQUNILFlBQU1hLEtBQUssR0FBRyxLQUFLaEIsUUFBTCxHQUFnQmUsT0FBOUI7QUFDQSxXQUFLWCxZQUFMLEdBQW9CYSxVQUFVLENBQUMsS0FBS2hCLFVBQU4sRUFBa0JlLEtBQWxCLENBQTlCO0FBQ0g7QUFDSjs7QUFFREUsRUFBQUEsYUFBYSxDQUFDbkIsT0FBRCxFQUFVO0FBQ25CLFFBQUlBLE9BQU8sS0FBSyxLQUFLQyxRQUFyQixFQUErQjtBQUMzQjtBQUNIOztBQUNELFVBQU1tQixnQkFBZ0IsR0FBR3BCLE9BQU8sR0FBRyxLQUFLQyxRQUF4QztBQUNBLFNBQUtBLFFBQUwsR0FBZ0JELE9BQWhCOztBQUNBLFFBQUksS0FBS3FCLFNBQUwsTUFBb0JELGdCQUF4QixFQUEwQztBQUN0Q0UsTUFBQUEsWUFBWSxDQUFDLEtBQUtqQixZQUFOLENBQVo7O0FBQ0EsV0FBS0gsVUFBTDtBQUNIO0FBQ0o7QUFFRDs7Ozs7O0FBSUFxQixFQUFBQSxLQUFLLEdBQUc7QUFDSixRQUFJLENBQUMsS0FBS0YsU0FBTCxFQUFMLEVBQXVCO0FBQ25CLFdBQUtmLFFBQUwsR0FBZ0JTLElBQUksQ0FBQ0QsR0FBTCxFQUFoQjtBQUNBLFdBQUtULFlBQUwsR0FBb0JhLFVBQVUsQ0FBQyxLQUFLaEIsVUFBTixFQUFrQixLQUFLRCxRQUF2QixDQUE5QjtBQUNIOztBQUNELFdBQU8sSUFBUDtBQUNIO0FBRUQ7Ozs7OztBQUlBdUIsRUFBQUEsT0FBTyxHQUFHO0FBQ04sUUFBSSxLQUFLSCxTQUFMLEVBQUosRUFBc0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFLZixRQUFMLEdBQWdCUyxJQUFJLENBQUNELEdBQUwsRUFBaEI7QUFDQSxhQUFPLElBQVA7QUFDSCxLQVBELE1BT087QUFDSCxhQUFPLEtBQUtTLEtBQUwsRUFBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7OztBQUtBRSxFQUFBQSxLQUFLLEdBQUc7QUFDSixRQUFJLEtBQUtKLFNBQUwsRUFBSixFQUFzQjtBQUNsQkMsTUFBQUEsWUFBWSxDQUFDLEtBQUtqQixZQUFOLENBQVo7O0FBQ0EsV0FBS08sT0FBTCxDQUFhLElBQUljLEtBQUosQ0FBVSxvQkFBVixDQUFiOztBQUNBLFdBQUt0QixjQUFMO0FBQ0g7O0FBQ0QsV0FBTyxJQUFQO0FBQ0g7QUFFRDs7Ozs7OztBQUtBdUIsRUFBQUEsUUFBUSxHQUFHO0FBQ1AsV0FBTyxLQUFLcEIsUUFBWjtBQUNIOztBQUVEYyxFQUFBQSxTQUFTLEdBQUc7QUFDUixXQUFPLEtBQUtoQixZQUFMLEtBQXNCLElBQTdCO0FBQ0g7O0FBaEdzQiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLyoqXHJcbkEgY291bnRkb3duIHRpbWVyLCBleHBvc2luZyBhIHByb21pc2UgYXBpLlxyXG5BIHRpbWVyIHN0YXJ0cyBpbiBhIG5vbi1zdGFydGVkIHN0YXRlLFxyXG5hbmQgbmVlZHMgdG8gYmUgc3RhcnRlZCBieSBjYWxsaW5nIGBzdGFydCgpYGAgb24gaXQgZmlyc3QuXHJcblxyXG5UaW1lcnMgY2FuIGJlIGBhYm9ydCgpYC1lZCB3aGljaCBtYWtlcyB0aGUgcHJvbWlzZSByZWplY3QgcHJlbWF0dXJlbHkuXHJcblxyXG5PbmNlIGEgdGltZXIgaXMgZmluaXNoZWQgb3IgYWJvcnRlZCwgaXQgY2FuJ3QgYmUgc3RhcnRlZCBhZ2FpblxyXG4oYmVjYXVzZSB0aGUgcHJvbWlzZSBzaG91bGQgbm90IGJlIHJlcGxhY2VkKS4gSW5zdGVhZCwgY3JlYXRlXHJcbmEgbmV3IG9uZSB0aHJvdWdoIGBjbG9uZSgpYCBvciBgY2xvbmVJZlJ1bigpYC5cclxuKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGltZXIge1xyXG4gICAgY29uc3RydWN0b3IodGltZW91dCkge1xyXG4gICAgICAgIHRoaXMuX3RpbWVvdXQgPSB0aW1lb3V0O1xyXG4gICAgICAgIHRoaXMuX29uVGltZW91dCA9IHRoaXMuX29uVGltZW91dC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuX3NldE5vdFN0YXJ0ZWQoKTtcclxuICAgIH1cclxuXHJcbiAgICBfc2V0Tm90U3RhcnRlZCgpIHtcclxuICAgICAgICB0aGlzLl90aW1lckhhbmRsZSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRUcyA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fcHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzb2x2ZSA9IHJlc29sdmU7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlamVjdCA9IHJlamVjdDtcclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fdGltZXJIYW5kbGUgPSBudWxsO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF9vblRpbWVvdXQoKSB7XHJcbiAgICAgICAgY29uc3Qgbm93ID0gRGF0ZS5ub3coKTtcclxuICAgICAgICBjb25zdCBlbGFwc2VkID0gbm93IC0gdGhpcy5fc3RhcnRUcztcclxuICAgICAgICBpZiAoZWxhcHNlZCA+PSB0aGlzLl90aW1lb3V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jlc29sdmUoKTtcclxuICAgICAgICAgICAgdGhpcy5fc2V0Tm90U3RhcnRlZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRlbHRhID0gdGhpcy5fdGltZW91dCAtIGVsYXBzZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX3RpbWVySGFuZGxlID0gc2V0VGltZW91dCh0aGlzLl9vblRpbWVvdXQsIGRlbHRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlVGltZW91dCh0aW1lb3V0KSB7XHJcbiAgICAgICAgaWYgKHRpbWVvdXQgPT09IHRoaXMuX3RpbWVvdXQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpc1NtYWxsZXJUaW1lb3V0ID0gdGltZW91dCA8IHRoaXMuX3RpbWVvdXQ7XHJcbiAgICAgICAgdGhpcy5fdGltZW91dCA9IHRpbWVvdXQ7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNSdW5uaW5nKCkgJiYgaXNTbWFsbGVyVGltZW91dCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fdGltZXJIYW5kbGUpO1xyXG4gICAgICAgICAgICB0aGlzLl9vblRpbWVvdXQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBpZiBub3Qgc3RhcnRlZCBiZWZvcmUsIHN0YXJ0cyB0aGUgdGltZXIuXHJcbiAgICAgKiBAcmV0dXJucyB7VGltZXJ9IHRoZSBzYW1lIHRpbWVyXHJcbiAgICAgKi9cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5pc1J1bm5pbmcoKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdGFydFRzID0gRGF0ZS5ub3coKTtcclxuICAgICAgICAgICAgdGhpcy5fdGltZXJIYW5kbGUgPSBzZXRUaW1lb3V0KHRoaXMuX29uVGltZW91dCwgdGhpcy5fdGltZW91dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogKHJlKXN0YXJ0IHRoZSB0aW1lci4gSWYgaXQncyBydW5uaW5nLCByZXNldCB0aGUgdGltZW91dC4gSWYgbm90LCBzdGFydCBpdC5cclxuICAgICAqIEByZXR1cm5zIHtUaW1lcn0gdGhlIHNhbWUgdGltZXJcclxuICAgICAqL1xyXG4gICAgcmVzdGFydCgpIHtcclxuICAgICAgICBpZiAodGhpcy5pc1J1bm5pbmcoKSkge1xyXG4gICAgICAgICAgICAvLyBkb24ndCBjbGVhclRpbWVvdXQgaGVyZSBhcyB0aGlzIG1ldGhvZFxyXG4gICAgICAgICAgICAvLyBjYW4gYmUgY2FsbGVkIGluIGZhc3Qgc3VjY2Vzc2lvbixcclxuICAgICAgICAgICAgLy8gaW5zdGVhZCBqdXN0IHRha2Ugbm90ZSBhbmQgY29tcGFyZVxyXG4gICAgICAgICAgICAvLyB3aGVuIHRoZSBhbHJlYWR5IHJ1bm5pbmcgdGltZW91dCBleHBpcmVzXHJcbiAgICAgICAgICAgIHRoaXMuX3N0YXJ0VHMgPSBEYXRlLm5vdygpO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGFydCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGlmIHRoZSB0aW1lciBpcyBydW5uaW5nLCBhYm9ydCBpdCxcclxuICAgICAqIGFuZCByZWplY3QgdGhlIHByb21pc2UgZm9yIHRoaXMgdGltZXIuXHJcbiAgICAgKiBAcmV0dXJucyB7VGltZXJ9IHRoZSBzYW1lIHRpbWVyXHJcbiAgICAgKi9cclxuICAgIGFib3J0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUnVubmluZygpKSB7XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90aW1lckhhbmRsZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlamVjdChuZXcgRXJyb3IoXCJUaW1lciB3YXMgYWJvcnRlZC5cIikpO1xyXG4gICAgICAgICAgICB0aGlzLl9zZXROb3RTdGFydGVkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICpwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdoZW4gdGhlIHRpbWVyIGVsYXBzZXMsXHJcbiAgICAgKm9yIGlzIHJlamVjdGVkIHdoZW4gYWJvcnQgaXMgY2FsbGVkXHJcbiAgICAgKkByZXR1cm4ge1Byb21pc2V9XHJcbiAgICAgKi9cclxuICAgIGZpbmlzaGVkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wcm9taXNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzUnVubmluZygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdGltZXJIYW5kbGUgIT09IG51bGw7XHJcbiAgICB9XHJcbn1cclxuIl19