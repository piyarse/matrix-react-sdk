"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pillifyLinks = pillifyLinks;
exports.unmountPills = unmountPills;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _MatrixClientPeg = require("../MatrixClientPeg");

var _SettingsStore = _interopRequireDefault(require("../settings/SettingsStore"));

var _pushprocessor = require("matrix-js-sdk/src/pushprocessor");

var sdk = _interopRequireWildcard(require("../index"));

/*
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Recurses depth-first through a DOM tree, converting matrix.to links
 * into pills based on the context of a given room.  Returns a list of
 * the resulting React nodes so they can be unmounted rather than leaking.
 *
 * @param {Node[]} nodes - a list of sibling DOM nodes to traverse to try
 *   to turn into pills.
 * @param {MatrixEvent} mxEvent - the matrix event which the DOM nodes are
 *   part of representing.
 * @param {Node[]} pills: an accumulator of the DOM nodes which contain
 *   React components which have been mounted as part of this.
 *   The initial caller should pass in an empty array to seed the accumulator.
 */
function pillifyLinks(nodes, mxEvent, pills) {
  const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(mxEvent.getRoomId());

  const shouldShowPillAvatar = _SettingsStore.default.getValue("Pill.shouldShowPillAvatar");

  let node = nodes[0];

  while (node) {
    let pillified = false;

    if (node.tagName === "A" && node.getAttribute("href")) {
      const href = node.getAttribute("href"); // If the link is a (localised) matrix.to link, replace it with a pill

      const Pill = sdk.getComponent('elements.Pill');

      if (Pill.isMessagePillUrl(href)) {
        const pillContainer = document.createElement('span');

        const pill = _react.default.createElement(Pill, {
          url: href,
          inMessage: true,
          room: room,
          shouldShowPillAvatar: shouldShowPillAvatar
        });

        _reactDom.default.render(pill, pillContainer);

        node.parentNode.replaceChild(pillContainer, node);
        pills.push(pillContainer); // Pills within pills aren't going to go well, so move on

        pillified = true; // update the current node with one that's now taken its place

        node = pillContainer;
      }
    } else if (node.nodeType === Node.TEXT_NODE && // as applying pills happens outside of react, make sure we're not doubly
    // applying @room pills here, as a rerender with the same content won't touch the DOM
    // to clear the pills from the last run of pillifyLinks
    !node.parentElement.classList.contains("mx_AtRoomPill")) {
      const Pill = sdk.getComponent('elements.Pill');
      let currentTextNode = node;
      const roomNotifTextNodes = []; // Take a textNode and break it up to make all the instances of @room their
      // own textNode, adding those nodes to roomNotifTextNodes

      while (currentTextNode !== null) {
        const roomNotifPos = Pill.roomNotifPos(currentTextNode.textContent);
        let nextTextNode = null;

        if (roomNotifPos > -1) {
          let roomTextNode = currentTextNode;
          if (roomNotifPos > 0) roomTextNode = roomTextNode.splitText(roomNotifPos);

          if (roomTextNode.textContent.length > Pill.roomNotifLen()) {
            nextTextNode = roomTextNode.splitText(Pill.roomNotifLen());
          }

          roomNotifTextNodes.push(roomTextNode);
        }

        currentTextNode = nextTextNode;
      }

      if (roomNotifTextNodes.length > 0) {
        const pushProcessor = new _pushprocessor.PushProcessor(_MatrixClientPeg.MatrixClientPeg.get());
        const atRoomRule = pushProcessor.getPushRuleById(".m.rule.roomnotif");

        if (atRoomRule && pushProcessor.ruleMatchesEvent(atRoomRule, mxEvent)) {
          // Now replace all those nodes with Pills
          for (const roomNotifTextNode of roomNotifTextNodes) {
            // Set the next node to be processed to the one after the node
            // we're adding now, since we've just inserted nodes into the structure
            // we're iterating over.
            // Note we've checked roomNotifTextNodes.length > 0 so we'll do this at least once
            node = roomNotifTextNode.nextSibling;
            const pillContainer = document.createElement('span');

            const pill = _react.default.createElement(Pill, {
              type: Pill.TYPE_AT_ROOM_MENTION,
              inMessage: true,
              room: room,
              shouldShowPillAvatar: true
            });

            _reactDom.default.render(pill, pillContainer);

            roomNotifTextNode.parentNode.replaceChild(pillContainer, roomNotifTextNode);
            pills.push(pillContainer);
          } // Nothing else to do for a text node (and we don't need to advance
          // the loop pointer because we did it above)


          continue;
        }
      }
    }

    if (node.childNodes && node.childNodes.length && !pillified) {
      pillifyLinks(node.childNodes, mxEvent, pills);
    }

    node = node.nextSibling;
  }
}
/**
 * Unmount all the pill containers from React created by pillifyLinks.
 *
 * It's critical to call this after pillifyLinks, otherwise
 * Pills will leak, leaking entire DOM trees via the event
 * emitter on BaseAvatar as per
 * https://github.com/vector-im/riot-web/issues/12417
 *
 * @param {Node[]} pills - array of pill containers whose React
 *   components should be unmounted.
 */


function unmountPills(pills) {
  for (const pillContainer of pills) {
    _reactDom.default.unmountComponentAtNode(pillContainer);
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9waWxsaWZ5LmpzIl0sIm5hbWVzIjpbInBpbGxpZnlMaW5rcyIsIm5vZGVzIiwibXhFdmVudCIsInBpbGxzIiwicm9vbSIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImdldFJvb20iLCJnZXRSb29tSWQiLCJzaG91bGRTaG93UGlsbEF2YXRhciIsIlNldHRpbmdzU3RvcmUiLCJnZXRWYWx1ZSIsIm5vZGUiLCJwaWxsaWZpZWQiLCJ0YWdOYW1lIiwiZ2V0QXR0cmlidXRlIiwiaHJlZiIsIlBpbGwiLCJzZGsiLCJnZXRDb21wb25lbnQiLCJpc01lc3NhZ2VQaWxsVXJsIiwicGlsbENvbnRhaW5lciIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsInBpbGwiLCJSZWFjdERPTSIsInJlbmRlciIsInBhcmVudE5vZGUiLCJyZXBsYWNlQ2hpbGQiLCJwdXNoIiwibm9kZVR5cGUiLCJOb2RlIiwiVEVYVF9OT0RFIiwicGFyZW50RWxlbWVudCIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwiY3VycmVudFRleHROb2RlIiwicm9vbU5vdGlmVGV4dE5vZGVzIiwicm9vbU5vdGlmUG9zIiwidGV4dENvbnRlbnQiLCJuZXh0VGV4dE5vZGUiLCJyb29tVGV4dE5vZGUiLCJzcGxpdFRleHQiLCJsZW5ndGgiLCJyb29tTm90aWZMZW4iLCJwdXNoUHJvY2Vzc29yIiwiUHVzaFByb2Nlc3NvciIsImF0Um9vbVJ1bGUiLCJnZXRQdXNoUnVsZUJ5SWQiLCJydWxlTWF0Y2hlc0V2ZW50Iiwicm9vbU5vdGlmVGV4dE5vZGUiLCJuZXh0U2libGluZyIsIlRZUEVfQVRfUk9PTV9NRU5USU9OIiwiY2hpbGROb2RlcyIsInVubW91bnRQaWxscyIsInVubW91bnRDb21wb25lbnRBdE5vZGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkE7Ozs7Ozs7Ozs7Ozs7QUFhTyxTQUFTQSxZQUFULENBQXNCQyxLQUF0QixFQUE2QkMsT0FBN0IsRUFBc0NDLEtBQXRDLEVBQTZDO0FBQ2hELFFBQU1DLElBQUksR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEJMLE9BQU8sQ0FBQ00sU0FBUixFQUE5QixDQUFiOztBQUNBLFFBQU1DLG9CQUFvQixHQUFHQyx1QkFBY0MsUUFBZCxDQUF1QiwyQkFBdkIsQ0FBN0I7O0FBQ0EsTUFBSUMsSUFBSSxHQUFHWCxLQUFLLENBQUMsQ0FBRCxDQUFoQjs7QUFDQSxTQUFPVyxJQUFQLEVBQWE7QUFDVCxRQUFJQyxTQUFTLEdBQUcsS0FBaEI7O0FBRUEsUUFBSUQsSUFBSSxDQUFDRSxPQUFMLEtBQWlCLEdBQWpCLElBQXdCRixJQUFJLENBQUNHLFlBQUwsQ0FBa0IsTUFBbEIsQ0FBNUIsRUFBdUQ7QUFDbkQsWUFBTUMsSUFBSSxHQUFHSixJQUFJLENBQUNHLFlBQUwsQ0FBa0IsTUFBbEIsQ0FBYixDQURtRCxDQUduRDs7QUFDQSxZQUFNRSxJQUFJLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixlQUFqQixDQUFiOztBQUNBLFVBQUlGLElBQUksQ0FBQ0csZ0JBQUwsQ0FBc0JKLElBQXRCLENBQUosRUFBaUM7QUFDN0IsY0FBTUssYUFBYSxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsTUFBdkIsQ0FBdEI7O0FBRUEsY0FBTUMsSUFBSSxHQUFHLDZCQUFDLElBQUQ7QUFDVCxVQUFBLEdBQUcsRUFBRVIsSUFESTtBQUVULFVBQUEsU0FBUyxFQUFFLElBRkY7QUFHVCxVQUFBLElBQUksRUFBRVosSUFIRztBQUlULFVBQUEsb0JBQW9CLEVBQUVLO0FBSmIsVUFBYjs7QUFPQWdCLDBCQUFTQyxNQUFULENBQWdCRixJQUFoQixFQUFzQkgsYUFBdEI7O0FBQ0FULFFBQUFBLElBQUksQ0FBQ2UsVUFBTCxDQUFnQkMsWUFBaEIsQ0FBNkJQLGFBQTdCLEVBQTRDVCxJQUE1QztBQUNBVCxRQUFBQSxLQUFLLENBQUMwQixJQUFOLENBQVdSLGFBQVgsRUFaNkIsQ0FhN0I7O0FBQ0FSLFFBQUFBLFNBQVMsR0FBRyxJQUFaLENBZDZCLENBZ0I3Qjs7QUFDQUQsUUFBQUEsSUFBSSxHQUFHUyxhQUFQO0FBQ0g7QUFDSixLQXhCRCxNQXdCTyxJQUNIVCxJQUFJLENBQUNrQixRQUFMLEtBQWtCQyxJQUFJLENBQUNDLFNBQXZCLElBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBQ3BCLElBQUksQ0FBQ3FCLGFBQUwsQ0FBbUJDLFNBQW5CLENBQTZCQyxRQUE3QixDQUFzQyxlQUF0QyxDQUxFLEVBTUw7QUFDRSxZQUFNbEIsSUFBSSxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsZUFBakIsQ0FBYjtBQUVBLFVBQUlpQixlQUFlLEdBQUd4QixJQUF0QjtBQUNBLFlBQU15QixrQkFBa0IsR0FBRyxFQUEzQixDQUpGLENBTUU7QUFDQTs7QUFDQSxhQUFPRCxlQUFlLEtBQUssSUFBM0IsRUFBaUM7QUFDN0IsY0FBTUUsWUFBWSxHQUFHckIsSUFBSSxDQUFDcUIsWUFBTCxDQUFrQkYsZUFBZSxDQUFDRyxXQUFsQyxDQUFyQjtBQUNBLFlBQUlDLFlBQVksR0FBRyxJQUFuQjs7QUFDQSxZQUFJRixZQUFZLEdBQUcsQ0FBQyxDQUFwQixFQUF1QjtBQUNuQixjQUFJRyxZQUFZLEdBQUdMLGVBQW5CO0FBRUEsY0FBSUUsWUFBWSxHQUFHLENBQW5CLEVBQXNCRyxZQUFZLEdBQUdBLFlBQVksQ0FBQ0MsU0FBYixDQUF1QkosWUFBdkIsQ0FBZjs7QUFDdEIsY0FBSUcsWUFBWSxDQUFDRixXQUFiLENBQXlCSSxNQUF6QixHQUFrQzFCLElBQUksQ0FBQzJCLFlBQUwsRUFBdEMsRUFBMkQ7QUFDdkRKLFlBQUFBLFlBQVksR0FBR0MsWUFBWSxDQUFDQyxTQUFiLENBQXVCekIsSUFBSSxDQUFDMkIsWUFBTCxFQUF2QixDQUFmO0FBQ0g7O0FBQ0RQLFVBQUFBLGtCQUFrQixDQUFDUixJQUFuQixDQUF3QlksWUFBeEI7QUFDSDs7QUFDREwsUUFBQUEsZUFBZSxHQUFHSSxZQUFsQjtBQUNIOztBQUVELFVBQUlILGtCQUFrQixDQUFDTSxNQUFuQixHQUE0QixDQUFoQyxFQUFtQztBQUMvQixjQUFNRSxhQUFhLEdBQUcsSUFBSUMsNEJBQUosQ0FBa0J6QyxpQ0FBZ0JDLEdBQWhCLEVBQWxCLENBQXRCO0FBQ0EsY0FBTXlDLFVBQVUsR0FBR0YsYUFBYSxDQUFDRyxlQUFkLENBQThCLG1CQUE5QixDQUFuQjs7QUFDQSxZQUFJRCxVQUFVLElBQUlGLGFBQWEsQ0FBQ0ksZ0JBQWQsQ0FBK0JGLFVBQS9CLEVBQTJDN0MsT0FBM0MsQ0FBbEIsRUFBdUU7QUFDbkU7QUFDQSxlQUFLLE1BQU1nRCxpQkFBWCxJQUFnQ2Isa0JBQWhDLEVBQW9EO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0F6QixZQUFBQSxJQUFJLEdBQUdzQyxpQkFBaUIsQ0FBQ0MsV0FBekI7QUFFQSxrQkFBTTlCLGFBQWEsR0FBR0MsUUFBUSxDQUFDQyxhQUFULENBQXVCLE1BQXZCLENBQXRCOztBQUNBLGtCQUFNQyxJQUFJLEdBQUcsNkJBQUMsSUFBRDtBQUNULGNBQUEsSUFBSSxFQUFFUCxJQUFJLENBQUNtQyxvQkFERjtBQUVULGNBQUEsU0FBUyxFQUFFLElBRkY7QUFHVCxjQUFBLElBQUksRUFBRWhELElBSEc7QUFJVCxjQUFBLG9CQUFvQixFQUFFO0FBSmIsY0FBYjs7QUFPQXFCLDhCQUFTQyxNQUFULENBQWdCRixJQUFoQixFQUFzQkgsYUFBdEI7O0FBQ0E2QixZQUFBQSxpQkFBaUIsQ0FBQ3ZCLFVBQWxCLENBQTZCQyxZQUE3QixDQUEwQ1AsYUFBMUMsRUFBeUQ2QixpQkFBekQ7QUFDQS9DLFlBQUFBLEtBQUssQ0FBQzBCLElBQU4sQ0FBV1IsYUFBWDtBQUNILFdBcEJrRSxDQXFCbkU7QUFDQTs7O0FBQ0E7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsUUFBSVQsSUFBSSxDQUFDeUMsVUFBTCxJQUFtQnpDLElBQUksQ0FBQ3lDLFVBQUwsQ0FBZ0JWLE1BQW5DLElBQTZDLENBQUM5QixTQUFsRCxFQUE2RDtBQUN6RGIsTUFBQUEsWUFBWSxDQUFDWSxJQUFJLENBQUN5QyxVQUFOLEVBQWtCbkQsT0FBbEIsRUFBMkJDLEtBQTNCLENBQVo7QUFDSDs7QUFFRFMsSUFBQUEsSUFBSSxHQUFHQSxJQUFJLENBQUN1QyxXQUFaO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7Ozs7Ozs7O0FBV08sU0FBU0csWUFBVCxDQUFzQm5ELEtBQXRCLEVBQTZCO0FBQ2hDLE9BQUssTUFBTWtCLGFBQVgsSUFBNEJsQixLQUE1QixFQUFtQztBQUMvQnNCLHNCQUFTOEIsc0JBQVQsQ0FBZ0NsQyxhQUFoQztBQUNIO0FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSBmcm9tIFwiLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQge1B1c2hQcm9jZXNzb3J9IGZyb20gJ21hdHJpeC1qcy1zZGsvc3JjL3B1c2hwcm9jZXNzb3InO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vaW5kZXgnO1xyXG5cclxuLyoqXHJcbiAqIFJlY3Vyc2VzIGRlcHRoLWZpcnN0IHRocm91Z2ggYSBET00gdHJlZSwgY29udmVydGluZyBtYXRyaXgudG8gbGlua3NcclxuICogaW50byBwaWxscyBiYXNlZCBvbiB0aGUgY29udGV4dCBvZiBhIGdpdmVuIHJvb20uICBSZXR1cm5zIGEgbGlzdCBvZlxyXG4gKiB0aGUgcmVzdWx0aW5nIFJlYWN0IG5vZGVzIHNvIHRoZXkgY2FuIGJlIHVubW91bnRlZCByYXRoZXIgdGhhbiBsZWFraW5nLlxyXG4gKlxyXG4gKiBAcGFyYW0ge05vZGVbXX0gbm9kZXMgLSBhIGxpc3Qgb2Ygc2libGluZyBET00gbm9kZXMgdG8gdHJhdmVyc2UgdG8gdHJ5XHJcbiAqICAgdG8gdHVybiBpbnRvIHBpbGxzLlxyXG4gKiBAcGFyYW0ge01hdHJpeEV2ZW50fSBteEV2ZW50IC0gdGhlIG1hdHJpeCBldmVudCB3aGljaCB0aGUgRE9NIG5vZGVzIGFyZVxyXG4gKiAgIHBhcnQgb2YgcmVwcmVzZW50aW5nLlxyXG4gKiBAcGFyYW0ge05vZGVbXX0gcGlsbHM6IGFuIGFjY3VtdWxhdG9yIG9mIHRoZSBET00gbm9kZXMgd2hpY2ggY29udGFpblxyXG4gKiAgIFJlYWN0IGNvbXBvbmVudHMgd2hpY2ggaGF2ZSBiZWVuIG1vdW50ZWQgYXMgcGFydCBvZiB0aGlzLlxyXG4gKiAgIFRoZSBpbml0aWFsIGNhbGxlciBzaG91bGQgcGFzcyBpbiBhbiBlbXB0eSBhcnJheSB0byBzZWVkIHRoZSBhY2N1bXVsYXRvci5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBwaWxsaWZ5TGlua3Mobm9kZXMsIG14RXZlbnQsIHBpbGxzKSB7XHJcbiAgICBjb25zdCByb29tID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20obXhFdmVudC5nZXRSb29tSWQoKSk7XHJcbiAgICBjb25zdCBzaG91bGRTaG93UGlsbEF2YXRhciA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJQaWxsLnNob3VsZFNob3dQaWxsQXZhdGFyXCIpO1xyXG4gICAgbGV0IG5vZGUgPSBub2Rlc1swXTtcclxuICAgIHdoaWxlIChub2RlKSB7XHJcbiAgICAgICAgbGV0IHBpbGxpZmllZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAobm9kZS50YWdOYW1lID09PSBcIkFcIiAmJiBub2RlLmdldEF0dHJpYnV0ZShcImhyZWZcIikpIHtcclxuICAgICAgICAgICAgY29uc3QgaHJlZiA9IG5vZGUuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKTtcclxuXHJcbiAgICAgICAgICAgIC8vIElmIHRoZSBsaW5rIGlzIGEgKGxvY2FsaXNlZCkgbWF0cml4LnRvIGxpbmssIHJlcGxhY2UgaXQgd2l0aCBhIHBpbGxcclxuICAgICAgICAgICAgY29uc3QgUGlsbCA9IHNkay5nZXRDb21wb25lbnQoJ2VsZW1lbnRzLlBpbGwnKTtcclxuICAgICAgICAgICAgaWYgKFBpbGwuaXNNZXNzYWdlUGlsbFVybChocmVmKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGlsbENvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBwaWxsID0gPFBpbGxcclxuICAgICAgICAgICAgICAgICAgICB1cmw9e2hyZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgaW5NZXNzYWdlPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgIHJvb209e3Jvb219XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvdWxkU2hvd1BpbGxBdmF0YXI9e3Nob3VsZFNob3dQaWxsQXZhdGFyfVxyXG4gICAgICAgICAgICAgICAgLz47XHJcblxyXG4gICAgICAgICAgICAgICAgUmVhY3RET00ucmVuZGVyKHBpbGwsIHBpbGxDb250YWluZXIpO1xyXG4gICAgICAgICAgICAgICAgbm9kZS5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChwaWxsQ29udGFpbmVyLCBub2RlKTtcclxuICAgICAgICAgICAgICAgIHBpbGxzLnB1c2gocGlsbENvbnRhaW5lcik7XHJcbiAgICAgICAgICAgICAgICAvLyBQaWxscyB3aXRoaW4gcGlsbHMgYXJlbid0IGdvaW5nIHRvIGdvIHdlbGwsIHNvIG1vdmUgb25cclxuICAgICAgICAgICAgICAgIHBpbGxpZmllZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gdXBkYXRlIHRoZSBjdXJyZW50IG5vZGUgd2l0aCBvbmUgdGhhdCdzIG5vdyB0YWtlbiBpdHMgcGxhY2VcclxuICAgICAgICAgICAgICAgIG5vZGUgPSBwaWxsQ29udGFpbmVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICAgICAgbm9kZS5ub2RlVHlwZSA9PT0gTm9kZS5URVhUX05PREUgJiZcclxuICAgICAgICAgICAgLy8gYXMgYXBwbHlpbmcgcGlsbHMgaGFwcGVucyBvdXRzaWRlIG9mIHJlYWN0LCBtYWtlIHN1cmUgd2UncmUgbm90IGRvdWJseVxyXG4gICAgICAgICAgICAvLyBhcHBseWluZyBAcm9vbSBwaWxscyBoZXJlLCBhcyBhIHJlcmVuZGVyIHdpdGggdGhlIHNhbWUgY29udGVudCB3b24ndCB0b3VjaCB0aGUgRE9NXHJcbiAgICAgICAgICAgIC8vIHRvIGNsZWFyIHRoZSBwaWxscyBmcm9tIHRoZSBsYXN0IHJ1biBvZiBwaWxsaWZ5TGlua3NcclxuICAgICAgICAgICAgIW5vZGUucGFyZW50RWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoXCJteF9BdFJvb21QaWxsXCIpXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFBpbGwgPSBzZGsuZ2V0Q29tcG9uZW50KCdlbGVtZW50cy5QaWxsJyk7XHJcblxyXG4gICAgICAgICAgICBsZXQgY3VycmVudFRleHROb2RlID0gbm9kZTtcclxuICAgICAgICAgICAgY29uc3Qgcm9vbU5vdGlmVGV4dE5vZGVzID0gW107XHJcblxyXG4gICAgICAgICAgICAvLyBUYWtlIGEgdGV4dE5vZGUgYW5kIGJyZWFrIGl0IHVwIHRvIG1ha2UgYWxsIHRoZSBpbnN0YW5jZXMgb2YgQHJvb20gdGhlaXJcclxuICAgICAgICAgICAgLy8gb3duIHRleHROb2RlLCBhZGRpbmcgdGhvc2Ugbm9kZXMgdG8gcm9vbU5vdGlmVGV4dE5vZGVzXHJcbiAgICAgICAgICAgIHdoaWxlIChjdXJyZW50VGV4dE5vZGUgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb21Ob3RpZlBvcyA9IFBpbGwucm9vbU5vdGlmUG9zKGN1cnJlbnRUZXh0Tm9kZS50ZXh0Q29udGVudCk7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmV4dFRleHROb2RlID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIGlmIChyb29tTm90aWZQb3MgPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByb29tVGV4dE5vZGUgPSBjdXJyZW50VGV4dE5vZGU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyb29tTm90aWZQb3MgPiAwKSByb29tVGV4dE5vZGUgPSByb29tVGV4dE5vZGUuc3BsaXRUZXh0KHJvb21Ob3RpZlBvcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJvb21UZXh0Tm9kZS50ZXh0Q29udGVudC5sZW5ndGggPiBQaWxsLnJvb21Ob3RpZkxlbigpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5leHRUZXh0Tm9kZSA9IHJvb21UZXh0Tm9kZS5zcGxpdFRleHQoUGlsbC5yb29tTm90aWZMZW4oKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJvb21Ob3RpZlRleHROb2Rlcy5wdXNoKHJvb21UZXh0Tm9kZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50VGV4dE5vZGUgPSBuZXh0VGV4dE5vZGU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChyb29tTm90aWZUZXh0Tm9kZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcHVzaFByb2Nlc3NvciA9IG5ldyBQdXNoUHJvY2Vzc29yKE1hdHJpeENsaWVudFBlZy5nZXQoKSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhdFJvb21SdWxlID0gcHVzaFByb2Nlc3Nvci5nZXRQdXNoUnVsZUJ5SWQoXCIubS5ydWxlLnJvb21ub3RpZlwiKTtcclxuICAgICAgICAgICAgICAgIGlmIChhdFJvb21SdWxlICYmIHB1c2hQcm9jZXNzb3IucnVsZU1hdGNoZXNFdmVudChhdFJvb21SdWxlLCBteEV2ZW50KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIE5vdyByZXBsYWNlIGFsbCB0aG9zZSBub2RlcyB3aXRoIFBpbGxzXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCByb29tTm90aWZUZXh0Tm9kZSBvZiByb29tTm90aWZUZXh0Tm9kZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gU2V0IHRoZSBuZXh0IG5vZGUgdG8gYmUgcHJvY2Vzc2VkIHRvIHRoZSBvbmUgYWZ0ZXIgdGhlIG5vZGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2UncmUgYWRkaW5nIG5vdywgc2luY2Ugd2UndmUganVzdCBpbnNlcnRlZCBub2RlcyBpbnRvIHRoZSBzdHJ1Y3R1cmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2UncmUgaXRlcmF0aW5nIG92ZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIE5vdGUgd2UndmUgY2hlY2tlZCByb29tTm90aWZUZXh0Tm9kZXMubGVuZ3RoID4gMCBzbyB3ZSdsbCBkbyB0aGlzIGF0IGxlYXN0IG9uY2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgbm9kZSA9IHJvb21Ob3RpZlRleHROb2RlLm5leHRTaWJsaW5nO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGlsbENvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGlsbCA9IDxQaWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPXtQaWxsLlRZUEVfQVRfUk9PTV9NRU5USU9OfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5NZXNzYWdlPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9vbT17cm9vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNob3dQaWxsQXZhdGFyPXt0cnVlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0RE9NLnJlbmRlcihwaWxsLCBwaWxsQ29udGFpbmVyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbU5vdGlmVGV4dE5vZGUucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQocGlsbENvbnRhaW5lciwgcm9vbU5vdGlmVGV4dE5vZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwaWxscy5wdXNoKHBpbGxDb250YWluZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAvLyBOb3RoaW5nIGVsc2UgdG8gZG8gZm9yIGEgdGV4dCBub2RlIChhbmQgd2UgZG9uJ3QgbmVlZCB0byBhZHZhbmNlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlIGxvb3AgcG9pbnRlciBiZWNhdXNlIHdlIGRpZCBpdCBhYm92ZSlcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG5vZGUuY2hpbGROb2RlcyAmJiBub2RlLmNoaWxkTm9kZXMubGVuZ3RoICYmICFwaWxsaWZpZWQpIHtcclxuICAgICAgICAgICAgcGlsbGlmeUxpbmtzKG5vZGUuY2hpbGROb2RlcywgbXhFdmVudCwgcGlsbHMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbm9kZSA9IG5vZGUubmV4dFNpYmxpbmc7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBVbm1vdW50IGFsbCB0aGUgcGlsbCBjb250YWluZXJzIGZyb20gUmVhY3QgY3JlYXRlZCBieSBwaWxsaWZ5TGlua3MuXHJcbiAqXHJcbiAqIEl0J3MgY3JpdGljYWwgdG8gY2FsbCB0aGlzIGFmdGVyIHBpbGxpZnlMaW5rcywgb3RoZXJ3aXNlXHJcbiAqIFBpbGxzIHdpbGwgbGVhaywgbGVha2luZyBlbnRpcmUgRE9NIHRyZWVzIHZpYSB0aGUgZXZlbnRcclxuICogZW1pdHRlciBvbiBCYXNlQXZhdGFyIGFzIHBlclxyXG4gKiBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy8xMjQxN1xyXG4gKlxyXG4gKiBAcGFyYW0ge05vZGVbXX0gcGlsbHMgLSBhcnJheSBvZiBwaWxsIGNvbnRhaW5lcnMgd2hvc2UgUmVhY3RcclxuICogICBjb21wb25lbnRzIHNob3VsZCBiZSB1bm1vdW50ZWQuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gdW5tb3VudFBpbGxzKHBpbGxzKSB7XHJcbiAgICBmb3IgKGNvbnN0IHBpbGxDb250YWluZXIgb2YgcGlsbHMpIHtcclxuICAgICAgICBSZWFjdERPTS51bm1vdW50Q29tcG9uZW50QXROb2RlKHBpbGxDb250YWluZXIpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==