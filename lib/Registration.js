"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startAnyRegistrationFlow = startAnyRegistrationFlow;
exports.SAFE_LOCALPART_REGEX = void 0;

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var sdk = _interopRequireWildcard(require("./index"));

var _Modal = _interopRequireDefault(require("./Modal"));

var _languageHandler = require("./languageHandler");

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Utility code for registering with a homeserver
 * Note that this is currently *not* used by the actual
 * registration code.
 */
// import {MatrixClientPeg} from './MatrixClientPeg';
// Regex for what a "safe" or "Matrix-looking" localpart would be.
// TODO: Update as needed for https://github.com/matrix-org/matrix-doc/issues/1514
const SAFE_LOCALPART_REGEX = /^[a-z0-9=_\-./]+$/;
/**
 * Starts either the ILAG or full registration flow, depending
 * on what the HS supports
 *
 * @param {object} options
 * @param {bool} options.go_home_on_cancel
 *     If true, goes to the home page if the user cancels the action
 * @param {bool} options.go_welcome_on_cancel
 *     If true, goes to the welcome page if the user cancels the action
 * @param {bool} options.screen_after
 *     If present the screen to redirect to after a successful login or register.
 */

exports.SAFE_LOCALPART_REGEX = SAFE_LOCALPART_REGEX;

async function startAnyRegistrationFlow(options) {
  if (options === undefined) options = {}; // look for an ILAG compatible flow. We define this as one
  // which has only dummy or recaptcha flows. In practice it
  // would support any stage InteractiveAuth supports, just not
  // ones like email & msisdn which require the user to supply
  // the relevant details in advance. We err on the side of
  // caution though.
  // XXX: ILAG is disabled for now,
  // see https://github.com/vector-im/riot-web/issues/8222
  // const flows = await _getRegistrationFlows();
  // const hasIlagFlow = flows.some((flow) => {
  //     return flow.stages.every((stage) => {
  //         return ['m.login.dummy', 'm.login.recaptcha', 'm.login.terms'].includes(stage);
  //     });
  // });
  // if (hasIlagFlow) {
  //     dis.dispatch({
  //         action: 'view_set_mxid',
  //         go_home_on_cancel: options.go_home_on_cancel,
  //     });
  //} else {

  const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

  const modal = _Modal.default.createTrackedDialog('Registration required', '', QuestionDialog, {
    hasCancelButton: true,
    quitOnly: true,
    title: (0, _languageHandler._t)("Sign In or Create Account"),
    description: (0, _languageHandler._t)("Use your account or create a new one to continue."),
    button: (0, _languageHandler._t)("Create Account"),
    extraButtons: [React.createElement("button", {
      key: "start_login",
      onClick: () => {
        modal.close();

        _dispatcher.default.dispatch({
          action: 'start_login',
          screenAfterLogin: options.screen_after
        });
      }
    }, (0, _languageHandler._t)('Sign In'))],
    onFinished: proceed => {
      if (proceed) {
        _dispatcher.default.dispatch({
          action: 'start_registration',
          screenAfterLogin: options.screen_after
        });
      } else if (options.go_home_on_cancel) {
        _dispatcher.default.dispatch({
          action: 'view_home_page'
        });
      } else if (options.go_welcome_on_cancel) {
        _dispatcher.default.dispatch({
          action: 'view_welcome_page'
        });
      }
    }
  }); //}

} // async function _getRegistrationFlows() {
//     try {
//         await MatrixClientPeg.get().register(
//             null,
//             null,
//             undefined,
//             {},
//             {},
//         );
//         console.log("Register request succeeded when it should have returned 401!");
//     } catch (e) {
//         if (e.httpStatus === 401) {
//             return e.data.flows;
//         }
//         throw e;
//     }
//     throw new Error("Register request succeeded when it should have returned 401!");
// }
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9SZWdpc3RyYXRpb24uanMiXSwibmFtZXMiOlsiU0FGRV9MT0NBTFBBUlRfUkVHRVgiLCJzdGFydEFueVJlZ2lzdHJhdGlvbkZsb3ciLCJvcHRpb25zIiwidW5kZWZpbmVkIiwiUXVlc3Rpb25EaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJtb2RhbCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsImhhc0NhbmNlbEJ1dHRvbiIsInF1aXRPbmx5IiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImJ1dHRvbiIsImV4dHJhQnV0dG9ucyIsImNsb3NlIiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJzY3JlZW5BZnRlckxvZ2luIiwic2NyZWVuX2FmdGVyIiwib25GaW5pc2hlZCIsInByb2NlZWQiLCJnb19ob21lX29uX2NhbmNlbCIsImdvX3dlbGNvbWVfb25fY2FuY2VsIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFzQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOzs7OztBQVVBO0FBRUE7QUFDQTtBQUNPLE1BQU1BLG9CQUFvQixHQUFHLG1CQUE3QjtBQUVQOzs7Ozs7Ozs7Ozs7Ozs7QUFZTyxlQUFlQyx3QkFBZixDQUF3Q0MsT0FBeEMsRUFBaUQ7QUFDcEQsTUFBSUEsT0FBTyxLQUFLQyxTQUFoQixFQUEyQkQsT0FBTyxHQUFHLEVBQVYsQ0FEeUIsQ0FFcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSSxRQUFNRSxjQUFjLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7O0FBQ0EsUUFBTUMsS0FBSyxHQUFHQyxlQUFNQyxtQkFBTixDQUEwQix1QkFBMUIsRUFBbUQsRUFBbkQsRUFBdURMLGNBQXZELEVBQXVFO0FBQ2pGTSxJQUFBQSxlQUFlLEVBQUUsSUFEZ0U7QUFFakZDLElBQUFBLFFBQVEsRUFBRSxJQUZ1RTtBQUdqRkMsSUFBQUEsS0FBSyxFQUFFLHlCQUFHLDJCQUFILENBSDBFO0FBSWpGQyxJQUFBQSxXQUFXLEVBQUUseUJBQUcsbURBQUgsQ0FKb0U7QUFLakZDLElBQUFBLE1BQU0sRUFBRSx5QkFBRyxnQkFBSCxDQUx5RTtBQU1qRkMsSUFBQUEsWUFBWSxFQUFFLENBQ1Y7QUFBUSxNQUFBLEdBQUcsRUFBQyxhQUFaO0FBQTBCLE1BQUEsT0FBTyxFQUFFLE1BQU07QUFDckNSLFFBQUFBLEtBQUssQ0FBQ1MsS0FBTjs7QUFDQUMsNEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUUsYUFBVDtBQUF3QkMsVUFBQUEsZ0JBQWdCLEVBQUVsQixPQUFPLENBQUNtQjtBQUFsRCxTQUFiO0FBQ0g7QUFIRCxPQUdLLHlCQUFHLFNBQUgsQ0FITCxDQURVLENBTm1FO0FBWWpGQyxJQUFBQSxVQUFVLEVBQUdDLE9BQUQsSUFBYTtBQUNyQixVQUFJQSxPQUFKLEVBQWE7QUFDVE4sNEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxVQUFBQSxNQUFNLEVBQUUsb0JBQVQ7QUFBK0JDLFVBQUFBLGdCQUFnQixFQUFFbEIsT0FBTyxDQUFDbUI7QUFBekQsU0FBYjtBQUNILE9BRkQsTUFFTyxJQUFJbkIsT0FBTyxDQUFDc0IsaUJBQVosRUFBK0I7QUFDbENQLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjtBQUNILE9BRk0sTUFFQSxJQUFJakIsT0FBTyxDQUFDdUIsb0JBQVosRUFBa0M7QUFDckNSLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjtBQUNIO0FBQ0o7QUFwQmdGLEdBQXZFLENBQWQsQ0ExQmdELENBZ0RwRDs7QUFDSCxDLENBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG4vKipcclxuICogVXRpbGl0eSBjb2RlIGZvciByZWdpc3RlcmluZyB3aXRoIGEgaG9tZXNlcnZlclxyXG4gKiBOb3RlIHRoYXQgdGhpcyBpcyBjdXJyZW50bHkgKm5vdCogdXNlZCBieSB0aGUgYWN0dWFsXHJcbiAqIHJlZ2lzdHJhdGlvbiBjb2RlLlxyXG4gKi9cclxuXHJcbmltcG9ydCBkaXMgZnJvbSAnLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi9Nb2RhbCc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi9sYW5ndWFnZUhhbmRsZXInO1xyXG4vLyBpbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5cclxuLy8gUmVnZXggZm9yIHdoYXQgYSBcInNhZmVcIiBvciBcIk1hdHJpeC1sb29raW5nXCIgbG9jYWxwYXJ0IHdvdWxkIGJlLlxyXG4vLyBUT0RPOiBVcGRhdGUgYXMgbmVlZGVkIGZvciBodHRwczovL2dpdGh1Yi5jb20vbWF0cml4LW9yZy9tYXRyaXgtZG9jL2lzc3Vlcy8xNTE0XHJcbmV4cG9ydCBjb25zdCBTQUZFX0xPQ0FMUEFSVF9SRUdFWCA9IC9eW2EtejAtOT1fXFwtLi9dKyQvO1xyXG5cclxuLyoqXHJcbiAqIFN0YXJ0cyBlaXRoZXIgdGhlIElMQUcgb3IgZnVsbCByZWdpc3RyYXRpb24gZmxvdywgZGVwZW5kaW5nXHJcbiAqIG9uIHdoYXQgdGhlIEhTIHN1cHBvcnRzXHJcbiAqXHJcbiAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXHJcbiAqIEBwYXJhbSB7Ym9vbH0gb3B0aW9ucy5nb19ob21lX29uX2NhbmNlbFxyXG4gKiAgICAgSWYgdHJ1ZSwgZ29lcyB0byB0aGUgaG9tZSBwYWdlIGlmIHRoZSB1c2VyIGNhbmNlbHMgdGhlIGFjdGlvblxyXG4gKiBAcGFyYW0ge2Jvb2x9IG9wdGlvbnMuZ29fd2VsY29tZV9vbl9jYW5jZWxcclxuICogICAgIElmIHRydWUsIGdvZXMgdG8gdGhlIHdlbGNvbWUgcGFnZSBpZiB0aGUgdXNlciBjYW5jZWxzIHRoZSBhY3Rpb25cclxuICogQHBhcmFtIHtib29sfSBvcHRpb25zLnNjcmVlbl9hZnRlclxyXG4gKiAgICAgSWYgcHJlc2VudCB0aGUgc2NyZWVuIHRvIHJlZGlyZWN0IHRvIGFmdGVyIGEgc3VjY2Vzc2Z1bCBsb2dpbiBvciByZWdpc3Rlci5cclxuICovXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBzdGFydEFueVJlZ2lzdHJhdGlvbkZsb3cob3B0aW9ucykge1xyXG4gICAgaWYgKG9wdGlvbnMgPT09IHVuZGVmaW5lZCkgb3B0aW9ucyA9IHt9O1xyXG4gICAgLy8gbG9vayBmb3IgYW4gSUxBRyBjb21wYXRpYmxlIGZsb3cuIFdlIGRlZmluZSB0aGlzIGFzIG9uZVxyXG4gICAgLy8gd2hpY2ggaGFzIG9ubHkgZHVtbXkgb3IgcmVjYXB0Y2hhIGZsb3dzLiBJbiBwcmFjdGljZSBpdFxyXG4gICAgLy8gd291bGQgc3VwcG9ydCBhbnkgc3RhZ2UgSW50ZXJhY3RpdmVBdXRoIHN1cHBvcnRzLCBqdXN0IG5vdFxyXG4gICAgLy8gb25lcyBsaWtlIGVtYWlsICYgbXNpc2RuIHdoaWNoIHJlcXVpcmUgdGhlIHVzZXIgdG8gc3VwcGx5XHJcbiAgICAvLyB0aGUgcmVsZXZhbnQgZGV0YWlscyBpbiBhZHZhbmNlLiBXZSBlcnIgb24gdGhlIHNpZGUgb2ZcclxuICAgIC8vIGNhdXRpb24gdGhvdWdoLlxyXG5cclxuICAgIC8vIFhYWDogSUxBRyBpcyBkaXNhYmxlZCBmb3Igbm93LFxyXG4gICAgLy8gc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS92ZWN0b3ItaW0vcmlvdC13ZWIvaXNzdWVzLzgyMjJcclxuXHJcbiAgICAvLyBjb25zdCBmbG93cyA9IGF3YWl0IF9nZXRSZWdpc3RyYXRpb25GbG93cygpO1xyXG4gICAgLy8gY29uc3QgaGFzSWxhZ0Zsb3cgPSBmbG93cy5zb21lKChmbG93KSA9PiB7XHJcbiAgICAvLyAgICAgcmV0dXJuIGZsb3cuc3RhZ2VzLmV2ZXJ5KChzdGFnZSkgPT4ge1xyXG4gICAgLy8gICAgICAgICByZXR1cm4gWydtLmxvZ2luLmR1bW15JywgJ20ubG9naW4ucmVjYXB0Y2hhJywgJ20ubG9naW4udGVybXMnXS5pbmNsdWRlcyhzdGFnZSk7XHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvLyB9KTtcclxuXHJcbiAgICAvLyBpZiAoaGFzSWxhZ0Zsb3cpIHtcclxuICAgIC8vICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgLy8gICAgICAgICBhY3Rpb246ICd2aWV3X3NldF9teGlkJyxcclxuICAgIC8vICAgICAgICAgZ29faG9tZV9vbl9jYW5jZWw6IG9wdGlvbnMuZ29faG9tZV9vbl9jYW5jZWwsXHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvL30gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICBjb25zdCBtb2RhbCA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1JlZ2lzdHJhdGlvbiByZXF1aXJlZCcsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgICAgICBoYXNDYW5jZWxCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgIHF1aXRPbmx5OiB0cnVlLFxyXG4gICAgICAgICAgICB0aXRsZTogX3QoXCJTaWduIEluIG9yIENyZWF0ZSBBY2NvdW50XCIpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJVc2UgeW91ciBhY2NvdW50IG9yIGNyZWF0ZSBhIG5ldyBvbmUgdG8gY29udGludWUuXCIpLFxyXG4gICAgICAgICAgICBidXR0b246IF90KFwiQ3JlYXRlIEFjY291bnRcIiksXHJcbiAgICAgICAgICAgIGV4dHJhQnV0dG9uczogW1xyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBrZXk9XCJzdGFydF9sb2dpblwiIG9uQ2xpY2s9eygpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBtb2RhbC5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnc3RhcnRfbG9naW4nLCBzY3JlZW5BZnRlckxvZ2luOiBvcHRpb25zLnNjcmVlbl9hZnRlcn0pO1xyXG4gICAgICAgICAgICAgICAgfX0+eyBfdCgnU2lnbiBJbicpIH08L2J1dHRvbj4sXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IChwcm9jZWVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvY2VlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnc3RhcnRfcmVnaXN0cmF0aW9uJywgc2NyZWVuQWZ0ZXJMb2dpbjogb3B0aW9ucy5zY3JlZW5fYWZ0ZXJ9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAob3B0aW9ucy5nb19ob21lX29uX2NhbmNlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19ob21lX3BhZ2UnfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG9wdGlvbnMuZ29fd2VsY29tZV9vbl9jYW5jZWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3ZpZXdfd2VsY29tZV9wYWdlJ30pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgLy99XHJcbn1cclxuXHJcbi8vIGFzeW5jIGZ1bmN0aW9uIF9nZXRSZWdpc3RyYXRpb25GbG93cygpIHtcclxuLy8gICAgIHRyeSB7XHJcbi8vICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlZ2lzdGVyKFxyXG4vLyAgICAgICAgICAgICBudWxsLFxyXG4vLyAgICAgICAgICAgICBudWxsLFxyXG4vLyAgICAgICAgICAgICB1bmRlZmluZWQsXHJcbi8vICAgICAgICAgICAgIHt9LFxyXG4vLyAgICAgICAgICAgICB7fSxcclxuLy8gICAgICAgICApO1xyXG4vLyAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVnaXN0ZXIgcmVxdWVzdCBzdWNjZWVkZWQgd2hlbiBpdCBzaG91bGQgaGF2ZSByZXR1cm5lZCA0MDEhXCIpO1xyXG4vLyAgICAgfSBjYXRjaCAoZSkge1xyXG4vLyAgICAgICAgIGlmIChlLmh0dHBTdGF0dXMgPT09IDQwMSkge1xyXG4vLyAgICAgICAgICAgICByZXR1cm4gZS5kYXRhLmZsb3dzO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgICB0aHJvdyBlO1xyXG4vLyAgICAgfVxyXG4vLyAgICAgdGhyb3cgbmV3IEVycm9yKFwiUmVnaXN0ZXIgcmVxdWVzdCBzdWNjZWVkZWQgd2hlbiBpdCBzaG91bGQgaGF2ZSByZXR1cm5lZCA0MDEhXCIpO1xyXG4vLyB9XHJcbiJdfQ==