/*
Copyright 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ContentRules = void 0;

var _PushRuleVectorState = require("./PushRuleVectorState");

class ContentRules {
  /**
   * Extract the keyword rules from a list of rules, and parse them
   * into a form which is useful for Vector's UI.
   *
   * Returns an object containing:
   *   rules: the primary list of keyword rules
   *   vectorState: a PushRuleVectorState indicating whether those rules are
   *      OFF/ON/LOUD
   *   externalRules: a list of other keyword rules, with states other than
   *      vectorState
   */
  static parseContentRules(rulesets) {
    // first categorise the keyword rules in terms of their actions
    const contentRules = this._categoriseContentRules(rulesets); // Decide which content rules to display in Vector UI.
    // Vector displays a single global rule for a list of keywords
    // whereas Matrix has a push rule per keyword.
    // Vector can set the unique rule in ON, LOUD or OFF state.
    // Matrix has enabled/disabled plus a combination of (highlight, sound) tweaks.
    // The code below determines which set of user's content push rules can be
    // displayed by the vector UI.
    // Push rules that does not fit, ie defined by another Matrix client, ends
    // in externalRules.
    // There is priority in the determination of which set will be the displayed one.
    // The set with rules that have LOUD tweaks is the first choice. Then, the ones
    // with ON tweaks (no tweaks).


    if (contentRules.loud.length) {
      return {
        vectorState: _PushRuleVectorState.PushRuleVectorState.LOUD,
        rules: contentRules.loud,
        externalRules: [].concat(contentRules.loud_but_disabled, contentRules.on, contentRules.on_but_disabled, contentRules.other)
      };
    } else if (contentRules.loud_but_disabled.length) {
      return {
        vectorState: _PushRuleVectorState.PushRuleVectorState.OFF,
        rules: contentRules.loud_but_disabled,
        externalRules: [].concat(contentRules.on, contentRules.on_but_disabled, contentRules.other)
      };
    } else if (contentRules.on.length) {
      return {
        vectorState: _PushRuleVectorState.PushRuleVectorState.ON,
        rules: contentRules.on,
        externalRules: [].concat(contentRules.on_but_disabled, contentRules.other)
      };
    } else if (contentRules.on_but_disabled.length) {
      return {
        vectorState: _PushRuleVectorState.PushRuleVectorState.OFF,
        rules: contentRules.on_but_disabled,
        externalRules: contentRules.other
      };
    } else {
      return {
        vectorState: _PushRuleVectorState.PushRuleVectorState.ON,
        rules: [],
        externalRules: contentRules.other
      };
    }
  }

  static _categoriseContentRules(rulesets) {
    const contentRules = {
      on: [],
      on_but_disabled: [],
      loud: [],
      loud_but_disabled: [],
      other: []
    };

    for (const kind in rulesets.global) {
      for (let i = 0; i < Object.keys(rulesets.global[kind]).length; ++i) {
        const r = rulesets.global[kind][i]; // check it's not a default rule

        if (r.rule_id[0] === '.' || kind !== 'content') {
          continue;
        }

        r.kind = kind; // is this needed? not sure

        switch (_PushRuleVectorState.PushRuleVectorState.contentRuleVectorStateKind(r)) {
          case _PushRuleVectorState.PushRuleVectorState.ON:
            if (r.enabled) {
              contentRules.on.push(r);
            } else {
              contentRules.on_but_disabled.push(r);
            }

            break;

          case _PushRuleVectorState.PushRuleVectorState.LOUD:
            if (r.enabled) {
              contentRules.loud.push(r);
            } else {
              contentRules.loud_but_disabled.push(r);
            }

            break;

          default:
            contentRules.other.push(r);
            break;
        }
      }
    }

    return contentRules;
  }

}

exports.ContentRules = ContentRules;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ub3RpZmljYXRpb25zL0NvbnRlbnRSdWxlcy5qcyJdLCJuYW1lcyI6WyJDb250ZW50UnVsZXMiLCJwYXJzZUNvbnRlbnRSdWxlcyIsInJ1bGVzZXRzIiwiY29udGVudFJ1bGVzIiwiX2NhdGVnb3Jpc2VDb250ZW50UnVsZXMiLCJsb3VkIiwibGVuZ3RoIiwidmVjdG9yU3RhdGUiLCJQdXNoUnVsZVZlY3RvclN0YXRlIiwiTE9VRCIsInJ1bGVzIiwiZXh0ZXJuYWxSdWxlcyIsImNvbmNhdCIsImxvdWRfYnV0X2Rpc2FibGVkIiwib24iLCJvbl9idXRfZGlzYWJsZWQiLCJvdGhlciIsIk9GRiIsIk9OIiwia2luZCIsImdsb2JhbCIsImkiLCJPYmplY3QiLCJrZXlzIiwiciIsInJ1bGVfaWQiLCJjb250ZW50UnVsZVZlY3RvclN0YXRlS2luZCIsImVuYWJsZWQiLCJwdXNoIl0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7Ozs7OztBQUVBOztBQUVPLE1BQU1BLFlBQU4sQ0FBbUI7QUFDdEI7Ozs7Ozs7Ozs7O0FBV0EsU0FBT0MsaUJBQVAsQ0FBeUJDLFFBQXpCLEVBQW1DO0FBQy9CO0FBQ0EsVUFBTUMsWUFBWSxHQUFHLEtBQUtDLHVCQUFMLENBQTZCRixRQUE3QixDQUFyQixDQUYrQixDQUkvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLFFBQUlDLFlBQVksQ0FBQ0UsSUFBYixDQUFrQkMsTUFBdEIsRUFBOEI7QUFDMUIsYUFBTztBQUNIQyxRQUFBQSxXQUFXLEVBQUVDLHlDQUFvQkMsSUFEOUI7QUFFSEMsUUFBQUEsS0FBSyxFQUFFUCxZQUFZLENBQUNFLElBRmpCO0FBR0hNLFFBQUFBLGFBQWEsRUFBRSxHQUFHQyxNQUFILENBQVVULFlBQVksQ0FBQ1UsaUJBQXZCLEVBQTBDVixZQUFZLENBQUNXLEVBQXZELEVBQTJEWCxZQUFZLENBQUNZLGVBQXhFLEVBQXlGWixZQUFZLENBQUNhLEtBQXRHO0FBSFosT0FBUDtBQUtILEtBTkQsTUFNTyxJQUFJYixZQUFZLENBQUNVLGlCQUFiLENBQStCUCxNQUFuQyxFQUEyQztBQUM5QyxhQUFPO0FBQ0hDLFFBQUFBLFdBQVcsRUFBRUMseUNBQW9CUyxHQUQ5QjtBQUVIUCxRQUFBQSxLQUFLLEVBQUVQLFlBQVksQ0FBQ1UsaUJBRmpCO0FBR0hGLFFBQUFBLGFBQWEsRUFBRSxHQUFHQyxNQUFILENBQVVULFlBQVksQ0FBQ1csRUFBdkIsRUFBMkJYLFlBQVksQ0FBQ1ksZUFBeEMsRUFBeURaLFlBQVksQ0FBQ2EsS0FBdEU7QUFIWixPQUFQO0FBS0gsS0FOTSxNQU1BLElBQUliLFlBQVksQ0FBQ1csRUFBYixDQUFnQlIsTUFBcEIsRUFBNEI7QUFDL0IsYUFBTztBQUNIQyxRQUFBQSxXQUFXLEVBQUVDLHlDQUFvQlUsRUFEOUI7QUFFSFIsUUFBQUEsS0FBSyxFQUFFUCxZQUFZLENBQUNXLEVBRmpCO0FBR0hILFFBQUFBLGFBQWEsRUFBRSxHQUFHQyxNQUFILENBQVVULFlBQVksQ0FBQ1ksZUFBdkIsRUFBd0NaLFlBQVksQ0FBQ2EsS0FBckQ7QUFIWixPQUFQO0FBS0gsS0FOTSxNQU1BLElBQUliLFlBQVksQ0FBQ1ksZUFBYixDQUE2QlQsTUFBakMsRUFBeUM7QUFDNUMsYUFBTztBQUNIQyxRQUFBQSxXQUFXLEVBQUVDLHlDQUFvQlMsR0FEOUI7QUFFSFAsUUFBQUEsS0FBSyxFQUFFUCxZQUFZLENBQUNZLGVBRmpCO0FBR0hKLFFBQUFBLGFBQWEsRUFBRVIsWUFBWSxDQUFDYTtBQUh6QixPQUFQO0FBS0gsS0FOTSxNQU1BO0FBQ0gsYUFBTztBQUNIVCxRQUFBQSxXQUFXLEVBQUVDLHlDQUFvQlUsRUFEOUI7QUFFSFIsUUFBQUEsS0FBSyxFQUFFLEVBRko7QUFHSEMsUUFBQUEsYUFBYSxFQUFFUixZQUFZLENBQUNhO0FBSHpCLE9BQVA7QUFLSDtBQUNKOztBQUVELFNBQU9aLHVCQUFQLENBQStCRixRQUEvQixFQUF5QztBQUNyQyxVQUFNQyxZQUFZLEdBQUc7QUFBQ1csTUFBQUEsRUFBRSxFQUFFLEVBQUw7QUFBU0MsTUFBQUEsZUFBZSxFQUFFLEVBQTFCO0FBQThCVixNQUFBQSxJQUFJLEVBQUUsRUFBcEM7QUFBd0NRLE1BQUFBLGlCQUFpQixFQUFFLEVBQTNEO0FBQStERyxNQUFBQSxLQUFLLEVBQUU7QUFBdEUsS0FBckI7O0FBQ0EsU0FBSyxNQUFNRyxJQUFYLElBQW1CakIsUUFBUSxDQUFDa0IsTUFBNUIsRUFBb0M7QUFDaEMsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHQyxNQUFNLENBQUNDLElBQVAsQ0FBWXJCLFFBQVEsQ0FBQ2tCLE1BQVQsQ0FBZ0JELElBQWhCLENBQVosRUFBbUNiLE1BQXZELEVBQStELEVBQUVlLENBQWpFLEVBQW9FO0FBQ2hFLGNBQU1HLENBQUMsR0FBR3RCLFFBQVEsQ0FBQ2tCLE1BQVQsQ0FBZ0JELElBQWhCLEVBQXNCRSxDQUF0QixDQUFWLENBRGdFLENBR2hFOztBQUNBLFlBQUlHLENBQUMsQ0FBQ0MsT0FBRixDQUFVLENBQVYsTUFBaUIsR0FBakIsSUFBd0JOLElBQUksS0FBSyxTQUFyQyxFQUFnRDtBQUM1QztBQUNIOztBQUVESyxRQUFBQSxDQUFDLENBQUNMLElBQUYsR0FBU0EsSUFBVCxDQVJnRSxDQVFqRDs7QUFFZixnQkFBUVgseUNBQW9Ca0IsMEJBQXBCLENBQStDRixDQUEvQyxDQUFSO0FBQ0ksZUFBS2hCLHlDQUFvQlUsRUFBekI7QUFDSSxnQkFBSU0sQ0FBQyxDQUFDRyxPQUFOLEVBQWU7QUFDWHhCLGNBQUFBLFlBQVksQ0FBQ1csRUFBYixDQUFnQmMsSUFBaEIsQ0FBcUJKLENBQXJCO0FBQ0gsYUFGRCxNQUVPO0FBQ0hyQixjQUFBQSxZQUFZLENBQUNZLGVBQWIsQ0FBNkJhLElBQTdCLENBQWtDSixDQUFsQztBQUNIOztBQUNEOztBQUNKLGVBQUtoQix5Q0FBb0JDLElBQXpCO0FBQ0ksZ0JBQUllLENBQUMsQ0FBQ0csT0FBTixFQUFlO0FBQ1h4QixjQUFBQSxZQUFZLENBQUNFLElBQWIsQ0FBa0J1QixJQUFsQixDQUF1QkosQ0FBdkI7QUFDSCxhQUZELE1BRU87QUFDSHJCLGNBQUFBLFlBQVksQ0FBQ1UsaUJBQWIsQ0FBK0JlLElBQS9CLENBQW9DSixDQUFwQztBQUNIOztBQUNEOztBQUNKO0FBQ0lyQixZQUFBQSxZQUFZLENBQUNhLEtBQWIsQ0FBbUJZLElBQW5CLENBQXdCSixDQUF4QjtBQUNBO0FBakJSO0FBbUJIO0FBQ0o7O0FBQ0QsV0FBT3JCLFlBQVA7QUFDSDs7QUFsR3FCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQge1B1c2hSdWxlVmVjdG9yU3RhdGV9IGZyb20gXCIuL1B1c2hSdWxlVmVjdG9yU3RhdGVcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBDb250ZW50UnVsZXMge1xyXG4gICAgLyoqXHJcbiAgICAgKiBFeHRyYWN0IHRoZSBrZXl3b3JkIHJ1bGVzIGZyb20gYSBsaXN0IG9mIHJ1bGVzLCBhbmQgcGFyc2UgdGhlbVxyXG4gICAgICogaW50byBhIGZvcm0gd2hpY2ggaXMgdXNlZnVsIGZvciBWZWN0b3IncyBVSS5cclxuICAgICAqXHJcbiAgICAgKiBSZXR1cm5zIGFuIG9iamVjdCBjb250YWluaW5nOlxyXG4gICAgICogICBydWxlczogdGhlIHByaW1hcnkgbGlzdCBvZiBrZXl3b3JkIHJ1bGVzXHJcbiAgICAgKiAgIHZlY3RvclN0YXRlOiBhIFB1c2hSdWxlVmVjdG9yU3RhdGUgaW5kaWNhdGluZyB3aGV0aGVyIHRob3NlIHJ1bGVzIGFyZVxyXG4gICAgICogICAgICBPRkYvT04vTE9VRFxyXG4gICAgICogICBleHRlcm5hbFJ1bGVzOiBhIGxpc3Qgb2Ygb3RoZXIga2V5d29yZCBydWxlcywgd2l0aCBzdGF0ZXMgb3RoZXIgdGhhblxyXG4gICAgICogICAgICB2ZWN0b3JTdGF0ZVxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgcGFyc2VDb250ZW50UnVsZXMocnVsZXNldHMpIHtcclxuICAgICAgICAvLyBmaXJzdCBjYXRlZ29yaXNlIHRoZSBrZXl3b3JkIHJ1bGVzIGluIHRlcm1zIG9mIHRoZWlyIGFjdGlvbnNcclxuICAgICAgICBjb25zdCBjb250ZW50UnVsZXMgPSB0aGlzLl9jYXRlZ29yaXNlQ29udGVudFJ1bGVzKHJ1bGVzZXRzKTtcclxuXHJcbiAgICAgICAgLy8gRGVjaWRlIHdoaWNoIGNvbnRlbnQgcnVsZXMgdG8gZGlzcGxheSBpbiBWZWN0b3IgVUkuXHJcbiAgICAgICAgLy8gVmVjdG9yIGRpc3BsYXlzIGEgc2luZ2xlIGdsb2JhbCBydWxlIGZvciBhIGxpc3Qgb2Yga2V5d29yZHNcclxuICAgICAgICAvLyB3aGVyZWFzIE1hdHJpeCBoYXMgYSBwdXNoIHJ1bGUgcGVyIGtleXdvcmQuXHJcbiAgICAgICAgLy8gVmVjdG9yIGNhbiBzZXQgdGhlIHVuaXF1ZSBydWxlIGluIE9OLCBMT1VEIG9yIE9GRiBzdGF0ZS5cclxuICAgICAgICAvLyBNYXRyaXggaGFzIGVuYWJsZWQvZGlzYWJsZWQgcGx1cyBhIGNvbWJpbmF0aW9uIG9mIChoaWdobGlnaHQsIHNvdW5kKSB0d2Vha3MuXHJcblxyXG4gICAgICAgIC8vIFRoZSBjb2RlIGJlbG93IGRldGVybWluZXMgd2hpY2ggc2V0IG9mIHVzZXIncyBjb250ZW50IHB1c2ggcnVsZXMgY2FuIGJlXHJcbiAgICAgICAgLy8gZGlzcGxheWVkIGJ5IHRoZSB2ZWN0b3IgVUkuXHJcbiAgICAgICAgLy8gUHVzaCBydWxlcyB0aGF0IGRvZXMgbm90IGZpdCwgaWUgZGVmaW5lZCBieSBhbm90aGVyIE1hdHJpeCBjbGllbnQsIGVuZHNcclxuICAgICAgICAvLyBpbiBleHRlcm5hbFJ1bGVzLlxyXG4gICAgICAgIC8vIFRoZXJlIGlzIHByaW9yaXR5IGluIHRoZSBkZXRlcm1pbmF0aW9uIG9mIHdoaWNoIHNldCB3aWxsIGJlIHRoZSBkaXNwbGF5ZWQgb25lLlxyXG4gICAgICAgIC8vIFRoZSBzZXQgd2l0aCBydWxlcyB0aGF0IGhhdmUgTE9VRCB0d2Vha3MgaXMgdGhlIGZpcnN0IGNob2ljZS4gVGhlbiwgdGhlIG9uZXNcclxuICAgICAgICAvLyB3aXRoIE9OIHR3ZWFrcyAobm8gdHdlYWtzKS5cclxuXHJcbiAgICAgICAgaWYgKGNvbnRlbnRSdWxlcy5sb3VkLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgdmVjdG9yU3RhdGU6IFB1c2hSdWxlVmVjdG9yU3RhdGUuTE9VRCxcclxuICAgICAgICAgICAgICAgIHJ1bGVzOiBjb250ZW50UnVsZXMubG91ZCxcclxuICAgICAgICAgICAgICAgIGV4dGVybmFsUnVsZXM6IFtdLmNvbmNhdChjb250ZW50UnVsZXMubG91ZF9idXRfZGlzYWJsZWQsIGNvbnRlbnRSdWxlcy5vbiwgY29udGVudFJ1bGVzLm9uX2J1dF9kaXNhYmxlZCwgY29udGVudFJ1bGVzLm90aGVyKSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRlbnRSdWxlcy5sb3VkX2J1dF9kaXNhYmxlZC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHZlY3RvclN0YXRlOiBQdXNoUnVsZVZlY3RvclN0YXRlLk9GRixcclxuICAgICAgICAgICAgICAgIHJ1bGVzOiBjb250ZW50UnVsZXMubG91ZF9idXRfZGlzYWJsZWQsXHJcbiAgICAgICAgICAgICAgICBleHRlcm5hbFJ1bGVzOiBbXS5jb25jYXQoY29udGVudFJ1bGVzLm9uLCBjb250ZW50UnVsZXMub25fYnV0X2Rpc2FibGVkLCBjb250ZW50UnVsZXMub3RoZXIpLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoY29udGVudFJ1bGVzLm9uLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgdmVjdG9yU3RhdGU6IFB1c2hSdWxlVmVjdG9yU3RhdGUuT04sXHJcbiAgICAgICAgICAgICAgICBydWxlczogY29udGVudFJ1bGVzLm9uLFxyXG4gICAgICAgICAgICAgICAgZXh0ZXJuYWxSdWxlczogW10uY29uY2F0KGNvbnRlbnRSdWxlcy5vbl9idXRfZGlzYWJsZWQsIGNvbnRlbnRSdWxlcy5vdGhlciksXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIGlmIChjb250ZW50UnVsZXMub25fYnV0X2Rpc2FibGVkLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgdmVjdG9yU3RhdGU6IFB1c2hSdWxlVmVjdG9yU3RhdGUuT0ZGLFxyXG4gICAgICAgICAgICAgICAgcnVsZXM6IGNvbnRlbnRSdWxlcy5vbl9idXRfZGlzYWJsZWQsXHJcbiAgICAgICAgICAgICAgICBleHRlcm5hbFJ1bGVzOiBjb250ZW50UnVsZXMub3RoZXIsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHZlY3RvclN0YXRlOiBQdXNoUnVsZVZlY3RvclN0YXRlLk9OLFxyXG4gICAgICAgICAgICAgICAgcnVsZXM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZXh0ZXJuYWxSdWxlczogY29udGVudFJ1bGVzLm90aGVyLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgX2NhdGVnb3Jpc2VDb250ZW50UnVsZXMocnVsZXNldHMpIHtcclxuICAgICAgICBjb25zdCBjb250ZW50UnVsZXMgPSB7b246IFtdLCBvbl9idXRfZGlzYWJsZWQ6IFtdLCBsb3VkOiBbXSwgbG91ZF9idXRfZGlzYWJsZWQ6IFtdLCBvdGhlcjogW119O1xyXG4gICAgICAgIGZvciAoY29uc3Qga2luZCBpbiBydWxlc2V0cy5nbG9iYWwpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBPYmplY3Qua2V5cyhydWxlc2V0cy5nbG9iYWxba2luZF0pLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByID0gcnVsZXNldHMuZ2xvYmFsW2tpbmRdW2ldO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGl0J3Mgbm90IGEgZGVmYXVsdCBydWxlXHJcbiAgICAgICAgICAgICAgICBpZiAoci5ydWxlX2lkWzBdID09PSAnLicgfHwga2luZCAhPT0gJ2NvbnRlbnQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgci5raW5kID0ga2luZDsgLy8gaXMgdGhpcyBuZWVkZWQ/IG5vdCBzdXJlXHJcblxyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChQdXNoUnVsZVZlY3RvclN0YXRlLmNvbnRlbnRSdWxlVmVjdG9yU3RhdGVLaW5kKHIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBQdXNoUnVsZVZlY3RvclN0YXRlLk9OOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoci5lbmFibGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50UnVsZXMub24ucHVzaChyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRSdWxlcy5vbl9idXRfZGlzYWJsZWQucHVzaChyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFB1c2hSdWxlVmVjdG9yU3RhdGUuTE9VRDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHIuZW5hYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudFJ1bGVzLmxvdWQucHVzaChyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRSdWxlcy5sb3VkX2J1dF9kaXNhYmxlZC5wdXNoKHIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRSdWxlcy5vdGhlci5wdXNoKHIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY29udGVudFJ1bGVzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==