/*
Copyright 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotificationUtils = void 0;

class NotificationUtils {
  // Encodes a dictionary of {
  //   "notify": true/false,
  //   "sound": string or undefined,
  //   "highlight: true/false,
  // }
  // to a list of push actions.
  static encodeActions(action) {
    const notify = action.notify;
    const sound = action.sound;
    const highlight = action.highlight;

    if (notify) {
      const actions = ["notify"];

      if (sound) {
        actions.push({
          "set_tweak": "sound",
          "value": sound
        });
      }

      if (highlight) {
        actions.push({
          "set_tweak": "highlight"
        });
      } else {
        actions.push({
          "set_tweak": "highlight",
          "value": false
        });
      }

      return actions;
    } else {
      return ["dont_notify"];
    }
  } // Decode a list of actions to a dictionary of {
  //   "notify": true/false,
  //   "sound": string or undefined,
  //   "highlight: true/false,
  // }
  // If the actions couldn't be decoded then returns null.


  static decodeActions(actions) {
    let notify = false;
    let sound = null;
    let highlight = false;

    for (let i = 0; i < actions.length; ++i) {
      const action = actions[i];

      if (action === "notify") {
        notify = true;
      } else if (action === "dont_notify") {
        notify = false;
      } else if (typeof action === 'object') {
        if (action.set_tweak === "sound") {
          sound = action.value;
        } else if (action.set_tweak === "highlight") {
          highlight = action.value;
        } else {
          // We don't understand this kind of tweak, so give up.
          return null;
        }
      } else {
        // We don't understand this kind of action, so give up.
        return null;
      }
    }

    if (highlight === undefined) {
      // If a highlight tweak is missing a value then it defaults to true.
      highlight = true;
    }

    const result = {
      notify: notify,
      highlight: highlight
    };

    if (sound !== null) {
      result.sound = sound;
    }

    return result;
  }

}

exports.NotificationUtils = NotificationUtils;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ub3RpZmljYXRpb25zL05vdGlmaWNhdGlvblV0aWxzLmpzIl0sIm5hbWVzIjpbIk5vdGlmaWNhdGlvblV0aWxzIiwiZW5jb2RlQWN0aW9ucyIsImFjdGlvbiIsIm5vdGlmeSIsInNvdW5kIiwiaGlnaGxpZ2h0IiwiYWN0aW9ucyIsInB1c2giLCJkZWNvZGVBY3Rpb25zIiwiaSIsImxlbmd0aCIsInNldF90d2VhayIsInZhbHVlIiwidW5kZWZpbmVkIiwicmVzdWx0Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7Ozs7OztBQUVPLE1BQU1BLGlCQUFOLENBQXdCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQU9DLGFBQVAsQ0FBcUJDLE1BQXJCLEVBQTZCO0FBQ3pCLFVBQU1DLE1BQU0sR0FBR0QsTUFBTSxDQUFDQyxNQUF0QjtBQUNBLFVBQU1DLEtBQUssR0FBR0YsTUFBTSxDQUFDRSxLQUFyQjtBQUNBLFVBQU1DLFNBQVMsR0FBR0gsTUFBTSxDQUFDRyxTQUF6Qjs7QUFDQSxRQUFJRixNQUFKLEVBQVk7QUFDUixZQUFNRyxPQUFPLEdBQUcsQ0FBQyxRQUFELENBQWhCOztBQUNBLFVBQUlGLEtBQUosRUFBVztBQUNQRSxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYTtBQUFDLHVCQUFhLE9BQWQ7QUFBdUIsbUJBQVNIO0FBQWhDLFNBQWI7QUFDSDs7QUFDRCxVQUFJQyxTQUFKLEVBQWU7QUFDWEMsUUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWE7QUFBQyx1QkFBYTtBQUFkLFNBQWI7QUFDSCxPQUZELE1BRU87QUFDSEQsUUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWE7QUFBQyx1QkFBYSxXQUFkO0FBQTJCLG1CQUFTO0FBQXBDLFNBQWI7QUFDSDs7QUFDRCxhQUFPRCxPQUFQO0FBQ0gsS0FYRCxNQVdPO0FBQ0gsYUFBTyxDQUFDLGFBQUQsQ0FBUDtBQUNIO0FBQ0osR0F6QjBCLENBMkIzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQU9FLGFBQVAsQ0FBcUJGLE9BQXJCLEVBQThCO0FBQzFCLFFBQUlILE1BQU0sR0FBRyxLQUFiO0FBQ0EsUUFBSUMsS0FBSyxHQUFHLElBQVo7QUFDQSxRQUFJQyxTQUFTLEdBQUcsS0FBaEI7O0FBRUEsU0FBSyxJQUFJSSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxPQUFPLENBQUNJLE1BQTVCLEVBQW9DLEVBQUVELENBQXRDLEVBQXlDO0FBQ3JDLFlBQU1QLE1BQU0sR0FBR0ksT0FBTyxDQUFDRyxDQUFELENBQXRCOztBQUNBLFVBQUlQLE1BQU0sS0FBSyxRQUFmLEVBQXlCO0FBQ3JCQyxRQUFBQSxNQUFNLEdBQUcsSUFBVDtBQUNILE9BRkQsTUFFTyxJQUFJRCxNQUFNLEtBQUssYUFBZixFQUE4QjtBQUNqQ0MsUUFBQUEsTUFBTSxHQUFHLEtBQVQ7QUFDSCxPQUZNLE1BRUEsSUFBSSxPQUFPRCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQ25DLFlBQUlBLE1BQU0sQ0FBQ1MsU0FBUCxLQUFxQixPQUF6QixFQUFrQztBQUM5QlAsVUFBQUEsS0FBSyxHQUFHRixNQUFNLENBQUNVLEtBQWY7QUFDSCxTQUZELE1BRU8sSUFBSVYsTUFBTSxDQUFDUyxTQUFQLEtBQXFCLFdBQXpCLEVBQXNDO0FBQ3pDTixVQUFBQSxTQUFTLEdBQUdILE1BQU0sQ0FBQ1UsS0FBbkI7QUFDSCxTQUZNLE1BRUE7QUFDSDtBQUNBLGlCQUFPLElBQVA7QUFDSDtBQUNKLE9BVE0sTUFTQTtBQUNIO0FBQ0EsZUFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFFRCxRQUFJUCxTQUFTLEtBQUtRLFNBQWxCLEVBQTZCO0FBQ3pCO0FBQ0FSLE1BQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0g7O0FBRUQsVUFBTVMsTUFBTSxHQUFHO0FBQUNYLE1BQUFBLE1BQU0sRUFBRUEsTUFBVDtBQUFpQkUsTUFBQUEsU0FBUyxFQUFFQTtBQUE1QixLQUFmOztBQUNBLFFBQUlELEtBQUssS0FBSyxJQUFkLEVBQW9CO0FBQ2hCVSxNQUFBQSxNQUFNLENBQUNWLEtBQVAsR0FBZUEsS0FBZjtBQUNIOztBQUNELFdBQU9VLE1BQVA7QUFDSDs7QUFyRTBCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uVXRpbHMge1xyXG4gICAgLy8gRW5jb2RlcyBhIGRpY3Rpb25hcnkgb2Yge1xyXG4gICAgLy8gICBcIm5vdGlmeVwiOiB0cnVlL2ZhbHNlLFxyXG4gICAgLy8gICBcInNvdW5kXCI6IHN0cmluZyBvciB1bmRlZmluZWQsXHJcbiAgICAvLyAgIFwiaGlnaGxpZ2h0OiB0cnVlL2ZhbHNlLFxyXG4gICAgLy8gfVxyXG4gICAgLy8gdG8gYSBsaXN0IG9mIHB1c2ggYWN0aW9ucy5cclxuICAgIHN0YXRpYyBlbmNvZGVBY3Rpb25zKGFjdGlvbikge1xyXG4gICAgICAgIGNvbnN0IG5vdGlmeSA9IGFjdGlvbi5ub3RpZnk7XHJcbiAgICAgICAgY29uc3Qgc291bmQgPSBhY3Rpb24uc291bmQ7XHJcbiAgICAgICAgY29uc3QgaGlnaGxpZ2h0ID0gYWN0aW9uLmhpZ2hsaWdodDtcclxuICAgICAgICBpZiAobm90aWZ5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdGlvbnMgPSBbXCJub3RpZnlcIl07XHJcbiAgICAgICAgICAgIGlmIChzb3VuZCkge1xyXG4gICAgICAgICAgICAgICAgYWN0aW9ucy5wdXNoKHtcInNldF90d2Vha1wiOiBcInNvdW5kXCIsIFwidmFsdWVcIjogc291bmR9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoaGlnaGxpZ2h0KSB7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb25zLnB1c2goe1wic2V0X3R3ZWFrXCI6IFwiaGlnaGxpZ2h0XCJ9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbnMucHVzaCh7XCJzZXRfdHdlYWtcIjogXCJoaWdobGlnaHRcIiwgXCJ2YWx1ZVwiOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBhY3Rpb25zO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXCJkb250X25vdGlmeVwiXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRGVjb2RlIGEgbGlzdCBvZiBhY3Rpb25zIHRvIGEgZGljdGlvbmFyeSBvZiB7XHJcbiAgICAvLyAgIFwibm90aWZ5XCI6IHRydWUvZmFsc2UsXHJcbiAgICAvLyAgIFwic291bmRcIjogc3RyaW5nIG9yIHVuZGVmaW5lZCxcclxuICAgIC8vICAgXCJoaWdobGlnaHQ6IHRydWUvZmFsc2UsXHJcbiAgICAvLyB9XHJcbiAgICAvLyBJZiB0aGUgYWN0aW9ucyBjb3VsZG4ndCBiZSBkZWNvZGVkIHRoZW4gcmV0dXJucyBudWxsLlxyXG4gICAgc3RhdGljIGRlY29kZUFjdGlvbnMoYWN0aW9ucykge1xyXG4gICAgICAgIGxldCBub3RpZnkgPSBmYWxzZTtcclxuICAgICAgICBsZXQgc291bmQgPSBudWxsO1xyXG4gICAgICAgIGxldCBoaWdobGlnaHQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhY3Rpb25zLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdGlvbiA9IGFjdGlvbnNbaV07XHJcbiAgICAgICAgICAgIGlmIChhY3Rpb24gPT09IFwibm90aWZ5XCIpIHtcclxuICAgICAgICAgICAgICAgIG5vdGlmeSA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSBcImRvbnRfbm90aWZ5XCIpIHtcclxuICAgICAgICAgICAgICAgIG5vdGlmeSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBhY3Rpb24gPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoYWN0aW9uLnNldF90d2VhayA9PT0gXCJzb3VuZFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc291bmQgPSBhY3Rpb24udmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGFjdGlvbi5zZXRfdHdlYWsgPT09IFwiaGlnaGxpZ2h0XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBoaWdobGlnaHQgPSBhY3Rpb24udmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFdlIGRvbid0IHVuZGVyc3RhbmQgdGhpcyBraW5kIG9mIHR3ZWFrLCBzbyBnaXZlIHVwLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gV2UgZG9uJ3QgdW5kZXJzdGFuZCB0aGlzIGtpbmQgb2YgYWN0aW9uLCBzbyBnaXZlIHVwLlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChoaWdobGlnaHQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAvLyBJZiBhIGhpZ2hsaWdodCB0d2VhayBpcyBtaXNzaW5nIGEgdmFsdWUgdGhlbiBpdCBkZWZhdWx0cyB0byB0cnVlLlxyXG4gICAgICAgICAgICBoaWdobGlnaHQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0ge25vdGlmeTogbm90aWZ5LCBoaWdobGlnaHQ6IGhpZ2hsaWdodH07XHJcbiAgICAgICAgaWYgKHNvdW5kICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdC5zb3VuZCA9IHNvdW5kO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==