"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/*
Copyright 2015 OpenMarket Ltd
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const DEBUG = 0; // utility to turn #rrggbb or rgb(r,g,b) into [red,green,blue]

function colorToRgb(color) {
  if (!color) {
    return [0, 0, 0];
  }

  if (color[0] === '#') {
    color = color.slice(1);

    if (color.length === 3) {
      color = color[0] + color[0] + color[1] + color[1] + color[2] + color[2];
    }

    const val = parseInt(color, 16);
    const r = val >> 16 & 255;
    const g = val >> 8 & 255;
    const b = val & 255;
    return [r, g, b];
  } else {
    const match = color.match(/rgb\((.*?),(.*?),(.*?)\)/);

    if (match) {
      return [parseInt(match[1]), parseInt(match[2]), parseInt(match[3])];
    }
  }

  return [0, 0, 0];
} // utility to turn [red,green,blue] into #rrggbb


function rgbToColor(rgb) {
  const val = rgb[0] << 16 | rgb[1] << 8 | rgb[2];
  return '#' + (0x1000000 + val).toString(16).slice(1);
}

class Tinter {
  constructor() {
    // The default colour keys to be replaced as referred to in CSS
    // (should be overridden by .mx_theme_accentColor and .mx_theme_secondaryAccentColor)
    this.keyRgb = ["rgb(118, 207, 166)", // Vector Green
    "rgb(234, 245, 240)", // Vector Light Green
    "rgb(211, 239, 225)" // roomsublist-label-bg-color (20% Green overlaid on Light Green)
    ]; // Some algebra workings for calculating the tint % of Vector Green & Light Green
    // x * 118 + (1 - x) * 255 = 234
    // x * 118 + 255 - 255 * x = 234
    // x * 118 - x * 255 = 234 - 255
    // (255 - 118) x = 255 - 234
    // x = (255 - 234) / (255 - 118) = 0.16
    // The colour keys to be replaced as referred to in SVGs

    this.keyHex = ["#76CFA6", // Vector Green
    "#EAF5F0", // Vector Light Green
    "#D3EFE1", // roomsublist-label-bg-color (20% Green overlaid on Light Green)
    "#FFFFFF", // white highlights of the SVGs (for switching to dark theme)
    "#000000" // black lowlights of the SVGs (for switching to dark theme)
    ]; // track the replacement colours actually being used
    // defaults to our keys.

    this.colors = [this.keyHex[0], this.keyHex[1], this.keyHex[2], this.keyHex[3], this.keyHex[4]]; // track the most current tint request inputs (which may differ from the
    // end result stored in this.colors

    this.currentTint = [undefined, undefined, undefined, undefined, undefined];
    this.cssFixups = [// { theme: {
      //      style: a style object that should be fixed up taken from a stylesheet
      //      attr: name of the attribute to be clobbered, e.g. 'color'
      //      index: ordinal of primary, secondary or tertiary
      //   },
      // }
    ]; // CSS attributes to be fixed up

    this.cssAttrs = ["color", "backgroundColor", "borderColor", "borderTopColor", "borderBottomColor", "borderLeftColor"];
    this.svgAttrs = ["fill", "stroke"]; // List of functions to call when the tint changes.

    this.tintables = []; // the currently loaded theme (if any)

    this.theme = undefined; // whether to force a tint (e.g. after changing theme)

    this.forceTint = false;
  }
  /**
   * Register a callback to fire when the tint changes.
   * This is used to rewrite the tintable SVGs with the new tint.
   *
   * It's not possible to unregister a tintable callback. So this can only be
   * used to register a static callback. If a set of tintables will change
   * over time then the best bet is to register a single callback for the
   * entire set.
   *
   * To ensure the tintable work happens at least once, it is also called as
   * part of registration.
   *
   * @param {Function} tintable Function to call when the tint changes.
   */


  registerTintable(tintable) {
    this.tintables.push(tintable);
    tintable();
  }

  getKeyRgb() {
    return this.keyRgb;
  }

  tint(primaryColor, secondaryColor, tertiaryColor) {
    return; // eslint-disable-next-line no-unreachable

    this.currentTint[0] = primaryColor;
    this.currentTint[1] = secondaryColor;
    this.currentTint[2] = tertiaryColor;
    this.calcCssFixups();

    if (DEBUG) {
      console.log("Tinter.tint(" + primaryColor + ", " + secondaryColor + ", " + tertiaryColor + ")");
    }

    if (!primaryColor) {
      primaryColor = this.keyRgb[0];
      secondaryColor = this.keyRgb[1];
      tertiaryColor = this.keyRgb[2];
    }

    if (!secondaryColor) {
      const x = 0.16; // average weighting factor calculated from vector green & light green

      const rgb = colorToRgb(primaryColor);
      rgb[0] = x * rgb[0] + (1 - x) * 255;
      rgb[1] = x * rgb[1] + (1 - x) * 255;
      rgb[2] = x * rgb[2] + (1 - x) * 255;
      secondaryColor = rgbToColor(rgb);
    }

    if (!tertiaryColor) {
      const x = 0.19;
      const rgb1 = colorToRgb(primaryColor);
      const rgb2 = colorToRgb(secondaryColor);
      rgb1[0] = x * rgb1[0] + (1 - x) * rgb2[0];
      rgb1[1] = x * rgb1[1] + (1 - x) * rgb2[1];
      rgb1[2] = x * rgb1[2] + (1 - x) * rgb2[2];
      tertiaryColor = rgbToColor(rgb1);
    }

    if (this.forceTint == false && this.colors[0] === primaryColor && this.colors[1] === secondaryColor && this.colors[2] === tertiaryColor) {
      return;
    }

    this.forceTint = false;
    this.colors[0] = primaryColor;
    this.colors[1] = secondaryColor;
    this.colors[2] = tertiaryColor;

    if (DEBUG) {
      console.log("Tinter.tint final: (" + primaryColor + ", " + secondaryColor + ", " + tertiaryColor + ")");
    } // go through manually fixing up the stylesheets.


    this.applyCssFixups(); // tell all the SVGs to go fix themselves up
    // we don't do this as a dispatch otherwise it will visually lag

    this.tintables.forEach(function (tintable) {
      tintable();
    });
  }

  tintSvgWhite(whiteColor) {
    this.currentTint[3] = whiteColor;

    if (!whiteColor) {
      whiteColor = this.colors[3];
    }

    if (this.colors[3] === whiteColor) {
      return;
    }

    this.colors[3] = whiteColor;
    this.tintables.forEach(function (tintable) {
      tintable();
    });
  }

  tintSvgBlack(blackColor) {
    this.currentTint[4] = blackColor;

    if (!blackColor) {
      blackColor = this.colors[4];
    }

    if (this.colors[4] === blackColor) {
      return;
    }

    this.colors[4] = blackColor;
    this.tintables.forEach(function (tintable) {
      tintable();
    });
  }

  setTheme(theme) {
    this.theme = theme; // update keyRgb from the current theme CSS itself, if it defines it

    if (document.getElementById('mx_theme_accentColor')) {
      this.keyRgb[0] = window.getComputedStyle(document.getElementById('mx_theme_accentColor')).color;
    }

    if (document.getElementById('mx_theme_secondaryAccentColor')) {
      this.keyRgb[1] = window.getComputedStyle(document.getElementById('mx_theme_secondaryAccentColor')).color;
    }

    if (document.getElementById('mx_theme_tertiaryAccentColor')) {
      this.keyRgb[2] = window.getComputedStyle(document.getElementById('mx_theme_tertiaryAccentColor')).color;
    }

    this.calcCssFixups();
    this.forceTint = true;
    this.tint(this.currentTint[0], this.currentTint[1], this.currentTint[2]);

    if (theme === 'dark') {
      // abuse the tinter to change all the SVG's #fff to #2d2d2d
      // XXX: obviously this shouldn't be hardcoded here.
      this.tintSvgWhite('#2d2d2d');
      this.tintSvgBlack('#dddddd');
    } else {
      this.tintSvgWhite('#ffffff');
      this.tintSvgBlack('#000000');
    }
  }

  calcCssFixups() {
    // cache our fixups
    if (this.cssFixups[this.theme]) return;

    if (DEBUG) {
      console.debug("calcCssFixups start for " + this.theme + " (checking " + document.styleSheets.length + " stylesheets)");
    }

    this.cssFixups[this.theme] = [];

    for (let i = 0; i < document.styleSheets.length; i++) {
      const ss = document.styleSheets[i];

      try {
        if (!ss) continue; // well done safari >:(
        // Chromium apparently sometimes returns null here; unsure why.
        // see $14534907369972FRXBx:matrix.org in HQ
        // ...ah, it's because there's a third party extension like
        // privacybadger inserting its own stylesheet in there with a
        // resource:// URI or something which results in a XSS error.
        // See also #vector:matrix.org/$145357669685386ebCfr:matrix.org
        // ...except some browsers apparently return stylesheets without
        // hrefs, which we have no choice but ignore right now
        // XXX seriously? we are hardcoding the name of vector's CSS file in
        // here?
        //
        // Why do we need to limit it to vector's CSS file anyway - if there
        // are other CSS files affecting the doc don't we want to apply the
        // same transformations to them?
        //
        // Iterating through the CSS looking for matches to hack on feels
        // pretty horrible anyway. And what if the application skin doesn't use
        // Vector Green as its primary color?
        // --richvdh
        // Yes, tinting assumes that you are using the Riot skin for now.
        // The right solution will be to move the CSS over to react-sdk.
        // And yes, the default assets for the base skin might as well use
        // Vector Green as any other colour.
        // --matthew
        // stylesheets we don't have permission to access (eg. ones from extensions) have a null
        // href and will throw exceptions if we try to access their rules.

        if (!ss.href || !ss.href.match(new RegExp('/theme-' + this.theme + '.css$'))) continue;
        if (ss.disabled) continue;
        if (!ss.cssRules) continue;
        if (DEBUG) console.debug("calcCssFixups checking " + ss.cssRules.length + " rules for " + ss.href);

        for (let j = 0; j < ss.cssRules.length; j++) {
          const rule = ss.cssRules[j];
          if (!rule.style) continue;
          if (rule.selectorText && rule.selectorText.match(/#mx_theme/)) continue;

          for (let k = 0; k < this.cssAttrs.length; k++) {
            const attr = this.cssAttrs[k];

            for (let l = 0; l < this.keyRgb.length; l++) {
              if (rule.style[attr] === this.keyRgb[l]) {
                this.cssFixups[this.theme].push({
                  style: rule.style,
                  attr: attr,
                  index: l
                });
              }
            }
          }
        }
      } catch (e) {
        // Catch any random exceptions that happen here: all sorts of things can go
        // wrong with this (nulls, SecurityErrors) and mostly it's for other
        // stylesheets that we don't want to proces anyway. We should not propagate an
        // exception out since this will cause the app to fail to start.
        console.log("Failed to calculate CSS fixups for a stylesheet: " + ss.href, e);
      }
    }

    if (DEBUG) {
      console.log("calcCssFixups end (" + this.cssFixups[this.theme].length + " fixups)");
    }
  }

  applyCssFixups() {
    if (DEBUG) {
      console.log("applyCssFixups start (" + this.cssFixups[this.theme].length + " fixups)");
    }

    for (let i = 0; i < this.cssFixups[this.theme].length; i++) {
      const cssFixup = this.cssFixups[this.theme][i];

      try {
        cssFixup.style[cssFixup.attr] = this.colors[cssFixup.index];
      } catch (e) {
        // Firefox Quantum explodes if you manually edit the CSS in the
        // inspector and then try to do a tint, as apparently all the
        // fixups are then stale.
        console.error("Failed to apply cssFixup in Tinter! ", e.name);
      }
    }

    if (DEBUG) console.log("applyCssFixups end");
  } // XXX: we could just move this all into TintableSvg, but as it's so similar
  // to the CSS fixup stuff in Tinter (just that the fixups are stored in TintableSvg)
  // keeping it here for now.


  calcSvgFixups(svgs) {
    // go through manually fixing up SVG colours.
    // we could do this by stylesheets, but keeping the stylesheets
    // updated would be a PITA, so just brute-force search for the
    // key colour; cache the element and apply.
    if (DEBUG) console.log("calcSvgFixups start for " + svgs);
    const fixups = [];

    for (let i = 0; i < svgs.length; i++) {
      let svgDoc;

      try {
        svgDoc = svgs[i].contentDocument;
      } catch (e) {
        let msg = 'Failed to get svg.contentDocument of ' + svgs[i].toString();

        if (e.message) {
          msg += e.message;
        }

        if (e.stack) {
          msg += ' | stack: ' + e.stack;
        }

        console.error(msg);
      }

      if (!svgDoc) continue;
      const tags = svgDoc.getElementsByTagName("*");

      for (let j = 0; j < tags.length; j++) {
        const tag = tags[j];

        for (let k = 0; k < this.svgAttrs.length; k++) {
          const attr = this.svgAttrs[k];

          for (let l = 0; l < this.keyHex.length; l++) {
            if (tag.getAttribute(attr) && tag.getAttribute(attr).toUpperCase() === this.keyHex[l]) {
              fixups.push({
                node: tag,
                attr: attr,
                index: l
              });
            }
          }
        }
      }
    }

    if (DEBUG) console.log("calcSvgFixups end");
    return fixups;
  }

  applySvgFixups(fixups) {
    if (DEBUG) console.log("applySvgFixups start for " + fixups);

    for (let i = 0; i < fixups.length; i++) {
      const svgFixup = fixups[i];
      svgFixup.node.setAttribute(svgFixup.attr, this.colors[svgFixup.index]);
    }

    if (DEBUG) console.log("applySvgFixups end");
  }

}

if (global.singletonTinter === undefined) {
  global.singletonTinter = new Tinter();
}

var _default = global.singletonTinter;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9UaW50ZXIuanMiXSwibmFtZXMiOlsiREVCVUciLCJjb2xvclRvUmdiIiwiY29sb3IiLCJzbGljZSIsImxlbmd0aCIsInZhbCIsInBhcnNlSW50IiwiciIsImciLCJiIiwibWF0Y2giLCJyZ2JUb0NvbG9yIiwicmdiIiwidG9TdHJpbmciLCJUaW50ZXIiLCJjb25zdHJ1Y3RvciIsImtleVJnYiIsImtleUhleCIsImNvbG9ycyIsImN1cnJlbnRUaW50IiwidW5kZWZpbmVkIiwiY3NzRml4dXBzIiwiY3NzQXR0cnMiLCJzdmdBdHRycyIsInRpbnRhYmxlcyIsInRoZW1lIiwiZm9yY2VUaW50IiwicmVnaXN0ZXJUaW50YWJsZSIsInRpbnRhYmxlIiwicHVzaCIsImdldEtleVJnYiIsInRpbnQiLCJwcmltYXJ5Q29sb3IiLCJzZWNvbmRhcnlDb2xvciIsInRlcnRpYXJ5Q29sb3IiLCJjYWxjQ3NzRml4dXBzIiwiY29uc29sZSIsImxvZyIsIngiLCJyZ2IxIiwicmdiMiIsImFwcGx5Q3NzRml4dXBzIiwiZm9yRWFjaCIsInRpbnRTdmdXaGl0ZSIsIndoaXRlQ29sb3IiLCJ0aW50U3ZnQmxhY2siLCJibGFja0NvbG9yIiwic2V0VGhlbWUiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwid2luZG93IiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImRlYnVnIiwic3R5bGVTaGVldHMiLCJpIiwic3MiLCJocmVmIiwiUmVnRXhwIiwiZGlzYWJsZWQiLCJjc3NSdWxlcyIsImoiLCJydWxlIiwic3R5bGUiLCJzZWxlY3RvclRleHQiLCJrIiwiYXR0ciIsImwiLCJpbmRleCIsImUiLCJjc3NGaXh1cCIsImVycm9yIiwibmFtZSIsImNhbGNTdmdGaXh1cHMiLCJzdmdzIiwiZml4dXBzIiwic3ZnRG9jIiwiY29udGVudERvY3VtZW50IiwibXNnIiwibWVzc2FnZSIsInN0YWNrIiwidGFncyIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwidGFnIiwiZ2V0QXR0cmlidXRlIiwidG9VcHBlckNhc2UiLCJub2RlIiwiYXBwbHlTdmdGaXh1cHMiLCJzdmdGaXh1cCIsInNldEF0dHJpYnV0ZSIsImdsb2JhbCIsInNpbmdsZXRvblRpbnRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE1BQU1BLEtBQUssR0FBRyxDQUFkLEMsQ0FFQTs7QUFDQSxTQUFTQyxVQUFULENBQW9CQyxLQUFwQixFQUEyQjtBQUN2QixNQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNSLFdBQU8sQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsQ0FBUDtBQUNIOztBQUVELE1BQUlBLEtBQUssQ0FBQyxDQUFELENBQUwsS0FBYSxHQUFqQixFQUFzQjtBQUNsQkEsSUFBQUEsS0FBSyxHQUFHQSxLQUFLLENBQUNDLEtBQU4sQ0FBWSxDQUFaLENBQVI7O0FBQ0EsUUFBSUQsS0FBSyxDQUFDRSxNQUFOLEtBQWlCLENBQXJCLEVBQXdCO0FBQ3BCRixNQUFBQSxLQUFLLEdBQUdBLEtBQUssQ0FBQyxDQUFELENBQUwsR0FBV0EsS0FBSyxDQUFDLENBQUQsQ0FBaEIsR0FDQUEsS0FBSyxDQUFDLENBQUQsQ0FETCxHQUNXQSxLQUFLLENBQUMsQ0FBRCxDQURoQixHQUVBQSxLQUFLLENBQUMsQ0FBRCxDQUZMLEdBRVdBLEtBQUssQ0FBQyxDQUFELENBRnhCO0FBR0g7O0FBQ0QsVUFBTUcsR0FBRyxHQUFHQyxRQUFRLENBQUNKLEtBQUQsRUFBUSxFQUFSLENBQXBCO0FBQ0EsVUFBTUssQ0FBQyxHQUFJRixHQUFHLElBQUksRUFBUixHQUFjLEdBQXhCO0FBQ0EsVUFBTUcsQ0FBQyxHQUFJSCxHQUFHLElBQUksQ0FBUixHQUFhLEdBQXZCO0FBQ0EsVUFBTUksQ0FBQyxHQUFHSixHQUFHLEdBQUcsR0FBaEI7QUFDQSxXQUFPLENBQUNFLENBQUQsRUFBSUMsQ0FBSixFQUFPQyxDQUFQLENBQVA7QUFDSCxHQVpELE1BWU87QUFDSCxVQUFNQyxLQUFLLEdBQUdSLEtBQUssQ0FBQ1EsS0FBTixDQUFZLDBCQUFaLENBQWQ7O0FBQ0EsUUFBSUEsS0FBSixFQUFXO0FBQ1AsYUFBTyxDQUNISixRQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FETCxFQUVISixRQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FGTCxFQUdISixRQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FITCxDQUFQO0FBS0g7QUFDSjs7QUFDRCxTQUFPLENBQUMsQ0FBRCxFQUFJLENBQUosRUFBTyxDQUFQLENBQVA7QUFDSCxDLENBRUQ7OztBQUNBLFNBQVNDLFVBQVQsQ0FBb0JDLEdBQXBCLEVBQXlCO0FBQ3JCLFFBQU1QLEdBQUcsR0FBSU8sR0FBRyxDQUFDLENBQUQsQ0FBSCxJQUFVLEVBQVgsR0FBa0JBLEdBQUcsQ0FBQyxDQUFELENBQUgsSUFBVSxDQUE1QixHQUFpQ0EsR0FBRyxDQUFDLENBQUQsQ0FBaEQ7QUFDQSxTQUFPLE1BQU0sQ0FBQyxZQUFZUCxHQUFiLEVBQWtCUSxRQUFsQixDQUEyQixFQUEzQixFQUErQlYsS0FBL0IsQ0FBcUMsQ0FBckMsQ0FBYjtBQUNIOztBQUVELE1BQU1XLE1BQU4sQ0FBYTtBQUNUQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQUNBO0FBQ0EsU0FBS0MsTUFBTCxHQUFjLENBQ1Ysb0JBRFUsRUFDWTtBQUN0Qix3QkFGVSxFQUVZO0FBQ3RCLHdCQUhVLENBR1k7QUFIWixLQUFkLENBSFUsQ0FTVjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFDQSxTQUFLQyxNQUFMLEdBQWMsQ0FDVixTQURVLEVBQ0M7QUFDWCxhQUZVLEVBRUM7QUFDWCxhQUhVLEVBR0M7QUFDWCxhQUpVLEVBSUM7QUFDWCxhQUxVLENBS0M7QUFMRCxLQUFkLENBakJVLENBeUJWO0FBQ0E7O0FBQ0EsU0FBS0MsTUFBTCxHQUFjLENBQ1YsS0FBS0QsTUFBTCxDQUFZLENBQVosQ0FEVSxFQUVWLEtBQUtBLE1BQUwsQ0FBWSxDQUFaLENBRlUsRUFHVixLQUFLQSxNQUFMLENBQVksQ0FBWixDQUhVLEVBSVYsS0FBS0EsTUFBTCxDQUFZLENBQVosQ0FKVSxFQUtWLEtBQUtBLE1BQUwsQ0FBWSxDQUFaLENBTFUsQ0FBZCxDQTNCVSxDQW1DVjtBQUNBOztBQUNBLFNBQUtFLFdBQUwsR0FBbUIsQ0FDZkMsU0FEZSxFQUVmQSxTQUZlLEVBR2ZBLFNBSGUsRUFJZkEsU0FKZSxFQUtmQSxTQUxlLENBQW5CO0FBUUEsU0FBS0MsU0FBTCxHQUFpQixDQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5hLEtBQWpCLENBN0NVLENBc0RWOztBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsQ0FDWixPQURZLEVBRVosaUJBRlksRUFHWixhQUhZLEVBSVosZ0JBSlksRUFLWixtQkFMWSxFQU1aLGlCQU5ZLENBQWhCO0FBU0EsU0FBS0MsUUFBTCxHQUFnQixDQUNaLE1BRFksRUFFWixRQUZZLENBQWhCLENBaEVVLENBcUVWOztBQUNBLFNBQUtDLFNBQUwsR0FBaUIsRUFBakIsQ0F0RVUsQ0F3RVY7O0FBQ0EsU0FBS0MsS0FBTCxHQUFhTCxTQUFiLENBekVVLENBMkVWOztBQUNBLFNBQUtNLFNBQUwsR0FBaUIsS0FBakI7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7O0FBY0FDLEVBQUFBLGdCQUFnQixDQUFDQyxRQUFELEVBQVc7QUFDdkIsU0FBS0osU0FBTCxDQUFlSyxJQUFmLENBQW9CRCxRQUFwQjtBQUNBQSxJQUFBQSxRQUFRO0FBQ1g7O0FBRURFLEVBQUFBLFNBQVMsR0FBRztBQUNSLFdBQU8sS0FBS2QsTUFBWjtBQUNIOztBQUVEZSxFQUFBQSxJQUFJLENBQUNDLFlBQUQsRUFBZUMsY0FBZixFQUErQkMsYUFBL0IsRUFBOEM7QUFDOUMsV0FEOEMsQ0FFOUM7O0FBQ0EsU0FBS2YsV0FBTCxDQUFpQixDQUFqQixJQUFzQmEsWUFBdEI7QUFDQSxTQUFLYixXQUFMLENBQWlCLENBQWpCLElBQXNCYyxjQUF0QjtBQUNBLFNBQUtkLFdBQUwsQ0FBaUIsQ0FBakIsSUFBc0JlLGFBQXRCO0FBRUEsU0FBS0MsYUFBTDs7QUFFQSxRQUFJbkMsS0FBSixFQUFXO0FBQ1BvQyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBaUJMLFlBQWpCLEdBQWdDLElBQWhDLEdBQ1JDLGNBRFEsR0FDUyxJQURULEdBRVJDLGFBRlEsR0FFUSxHQUZwQjtBQUdIOztBQUVELFFBQUksQ0FBQ0YsWUFBTCxFQUFtQjtBQUNmQSxNQUFBQSxZQUFZLEdBQUcsS0FBS2hCLE1BQUwsQ0FBWSxDQUFaLENBQWY7QUFDQWlCLE1BQUFBLGNBQWMsR0FBRyxLQUFLakIsTUFBTCxDQUFZLENBQVosQ0FBakI7QUFDQWtCLE1BQUFBLGFBQWEsR0FBRyxLQUFLbEIsTUFBTCxDQUFZLENBQVosQ0FBaEI7QUFDSDs7QUFFRCxRQUFJLENBQUNpQixjQUFMLEVBQXFCO0FBQ2pCLFlBQU1LLENBQUMsR0FBRyxJQUFWLENBRGlCLENBQ0Q7O0FBQ2hCLFlBQU0xQixHQUFHLEdBQUdYLFVBQVUsQ0FBQytCLFlBQUQsQ0FBdEI7QUFDQXBCLE1BQUFBLEdBQUcsQ0FBQyxDQUFELENBQUgsR0FBUzBCLENBQUMsR0FBRzFCLEdBQUcsQ0FBQyxDQUFELENBQVAsR0FBYSxDQUFDLElBQUkwQixDQUFMLElBQVUsR0FBaEM7QUFDQTFCLE1BQUFBLEdBQUcsQ0FBQyxDQUFELENBQUgsR0FBUzBCLENBQUMsR0FBRzFCLEdBQUcsQ0FBQyxDQUFELENBQVAsR0FBYSxDQUFDLElBQUkwQixDQUFMLElBQVUsR0FBaEM7QUFDQTFCLE1BQUFBLEdBQUcsQ0FBQyxDQUFELENBQUgsR0FBUzBCLENBQUMsR0FBRzFCLEdBQUcsQ0FBQyxDQUFELENBQVAsR0FBYSxDQUFDLElBQUkwQixDQUFMLElBQVUsR0FBaEM7QUFDQUwsTUFBQUEsY0FBYyxHQUFHdEIsVUFBVSxDQUFDQyxHQUFELENBQTNCO0FBQ0g7O0FBRUQsUUFBSSxDQUFDc0IsYUFBTCxFQUFvQjtBQUNoQixZQUFNSSxDQUFDLEdBQUcsSUFBVjtBQUNBLFlBQU1DLElBQUksR0FBR3RDLFVBQVUsQ0FBQytCLFlBQUQsQ0FBdkI7QUFDQSxZQUFNUSxJQUFJLEdBQUd2QyxVQUFVLENBQUNnQyxjQUFELENBQXZCO0FBQ0FNLE1BQUFBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVUQsQ0FBQyxHQUFHQyxJQUFJLENBQUMsQ0FBRCxDQUFSLEdBQWMsQ0FBQyxJQUFJRCxDQUFMLElBQVVFLElBQUksQ0FBQyxDQUFELENBQXRDO0FBQ0FELE1BQUFBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVUQsQ0FBQyxHQUFHQyxJQUFJLENBQUMsQ0FBRCxDQUFSLEdBQWMsQ0FBQyxJQUFJRCxDQUFMLElBQVVFLElBQUksQ0FBQyxDQUFELENBQXRDO0FBQ0FELE1BQUFBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVUQsQ0FBQyxHQUFHQyxJQUFJLENBQUMsQ0FBRCxDQUFSLEdBQWMsQ0FBQyxJQUFJRCxDQUFMLElBQVVFLElBQUksQ0FBQyxDQUFELENBQXRDO0FBQ0FOLE1BQUFBLGFBQWEsR0FBR3ZCLFVBQVUsQ0FBQzRCLElBQUQsQ0FBMUI7QUFDSDs7QUFFRCxRQUFJLEtBQUtiLFNBQUwsSUFBa0IsS0FBbEIsSUFDQSxLQUFLUixNQUFMLENBQVksQ0FBWixNQUFtQmMsWUFEbkIsSUFFQSxLQUFLZCxNQUFMLENBQVksQ0FBWixNQUFtQmUsY0FGbkIsSUFHQSxLQUFLZixNQUFMLENBQVksQ0FBWixNQUFtQmdCLGFBSHZCLEVBR3NDO0FBQ2xDO0FBQ0g7O0FBRUQsU0FBS1IsU0FBTCxHQUFpQixLQUFqQjtBQUVBLFNBQUtSLE1BQUwsQ0FBWSxDQUFaLElBQWlCYyxZQUFqQjtBQUNBLFNBQUtkLE1BQUwsQ0FBWSxDQUFaLElBQWlCZSxjQUFqQjtBQUNBLFNBQUtmLE1BQUwsQ0FBWSxDQUFaLElBQWlCZ0IsYUFBakI7O0FBRUEsUUFBSWxDLEtBQUosRUFBVztBQUNQb0MsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkseUJBQXlCTCxZQUF6QixHQUF3QyxJQUF4QyxHQUNSQyxjQURRLEdBQ1MsSUFEVCxHQUVSQyxhQUZRLEdBRVEsR0FGcEI7QUFHSCxLQXpENkMsQ0EyRDlDOzs7QUFDQSxTQUFLTyxjQUFMLEdBNUQ4QyxDQThEOUM7QUFDQTs7QUFDQSxTQUFLakIsU0FBTCxDQUFla0IsT0FBZixDQUF1QixVQUFTZCxRQUFULEVBQW1CO0FBQ3RDQSxNQUFBQSxRQUFRO0FBQ1gsS0FGRDtBQUdIOztBQUVEZSxFQUFBQSxZQUFZLENBQUNDLFVBQUQsRUFBYTtBQUNyQixTQUFLekIsV0FBTCxDQUFpQixDQUFqQixJQUFzQnlCLFVBQXRCOztBQUVBLFFBQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiQSxNQUFBQSxVQUFVLEdBQUcsS0FBSzFCLE1BQUwsQ0FBWSxDQUFaLENBQWI7QUFDSDs7QUFDRCxRQUFJLEtBQUtBLE1BQUwsQ0FBWSxDQUFaLE1BQW1CMEIsVUFBdkIsRUFBbUM7QUFDL0I7QUFDSDs7QUFDRCxTQUFLMUIsTUFBTCxDQUFZLENBQVosSUFBaUIwQixVQUFqQjtBQUNBLFNBQUtwQixTQUFMLENBQWVrQixPQUFmLENBQXVCLFVBQVNkLFFBQVQsRUFBbUI7QUFDdENBLE1BQUFBLFFBQVE7QUFDWCxLQUZEO0FBR0g7O0FBRURpQixFQUFBQSxZQUFZLENBQUNDLFVBQUQsRUFBYTtBQUNyQixTQUFLM0IsV0FBTCxDQUFpQixDQUFqQixJQUFzQjJCLFVBQXRCOztBQUVBLFFBQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiQSxNQUFBQSxVQUFVLEdBQUcsS0FBSzVCLE1BQUwsQ0FBWSxDQUFaLENBQWI7QUFDSDs7QUFDRCxRQUFJLEtBQUtBLE1BQUwsQ0FBWSxDQUFaLE1BQW1CNEIsVUFBdkIsRUFBbUM7QUFDL0I7QUFDSDs7QUFDRCxTQUFLNUIsTUFBTCxDQUFZLENBQVosSUFBaUI0QixVQUFqQjtBQUNBLFNBQUt0QixTQUFMLENBQWVrQixPQUFmLENBQXVCLFVBQVNkLFFBQVQsRUFBbUI7QUFDdENBLE1BQUFBLFFBQVE7QUFDWCxLQUZEO0FBR0g7O0FBR0RtQixFQUFBQSxRQUFRLENBQUN0QixLQUFELEVBQVE7QUFDWixTQUFLQSxLQUFMLEdBQWFBLEtBQWIsQ0FEWSxDQUdaOztBQUNBLFFBQUl1QixRQUFRLENBQUNDLGNBQVQsQ0FBd0Isc0JBQXhCLENBQUosRUFBcUQ7QUFDakQsV0FBS2pDLE1BQUwsQ0FBWSxDQUFaLElBQWlCa0MsTUFBTSxDQUFDQyxnQkFBUCxDQUNiSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0Isc0JBQXhCLENBRGEsRUFDb0MvQyxLQURyRDtBQUVIOztBQUNELFFBQUk4QyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsK0JBQXhCLENBQUosRUFBOEQ7QUFDMUQsV0FBS2pDLE1BQUwsQ0FBWSxDQUFaLElBQWlCa0MsTUFBTSxDQUFDQyxnQkFBUCxDQUNiSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsK0JBQXhCLENBRGEsRUFDNkMvQyxLQUQ5RDtBQUVIOztBQUNELFFBQUk4QyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsOEJBQXhCLENBQUosRUFBNkQ7QUFDekQsV0FBS2pDLE1BQUwsQ0FBWSxDQUFaLElBQWlCa0MsTUFBTSxDQUFDQyxnQkFBUCxDQUNiSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsOEJBQXhCLENBRGEsRUFDNEMvQyxLQUQ3RDtBQUVIOztBQUVELFNBQUtpQyxhQUFMO0FBQ0EsU0FBS1QsU0FBTCxHQUFpQixJQUFqQjtBQUVBLFNBQUtLLElBQUwsQ0FBVSxLQUFLWixXQUFMLENBQWlCLENBQWpCLENBQVYsRUFBK0IsS0FBS0EsV0FBTCxDQUFpQixDQUFqQixDQUEvQixFQUFvRCxLQUFLQSxXQUFMLENBQWlCLENBQWpCLENBQXBEOztBQUVBLFFBQUlNLEtBQUssS0FBSyxNQUFkLEVBQXNCO0FBQ2xCO0FBQ0E7QUFDQSxXQUFLa0IsWUFBTCxDQUFrQixTQUFsQjtBQUNBLFdBQUtFLFlBQUwsQ0FBa0IsU0FBbEI7QUFDSCxLQUxELE1BS087QUFDSCxXQUFLRixZQUFMLENBQWtCLFNBQWxCO0FBQ0EsV0FBS0UsWUFBTCxDQUFrQixTQUFsQjtBQUNIO0FBQ0o7O0FBRURWLEVBQUFBLGFBQWEsR0FBRztBQUNaO0FBQ0EsUUFBSSxLQUFLZCxTQUFMLENBQWUsS0FBS0ksS0FBcEIsQ0FBSixFQUFnQzs7QUFFaEMsUUFBSXpCLEtBQUosRUFBVztBQUNQb0MsTUFBQUEsT0FBTyxDQUFDZ0IsS0FBUixDQUFjLDZCQUE2QixLQUFLM0IsS0FBbEMsR0FBMEMsYUFBMUMsR0FDVnVCLFFBQVEsQ0FBQ0ssV0FBVCxDQUFxQmpELE1BRFgsR0FFVixlQUZKO0FBR0g7O0FBRUQsU0FBS2lCLFNBQUwsQ0FBZSxLQUFLSSxLQUFwQixJQUE2QixFQUE3Qjs7QUFFQSxTQUFLLElBQUk2QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHTixRQUFRLENBQUNLLFdBQVQsQ0FBcUJqRCxNQUF6QyxFQUFpRGtELENBQUMsRUFBbEQsRUFBc0Q7QUFDbEQsWUFBTUMsRUFBRSxHQUFHUCxRQUFRLENBQUNLLFdBQVQsQ0FBcUJDLENBQXJCLENBQVg7O0FBQ0EsVUFBSTtBQUNBLFlBQUksQ0FBQ0MsRUFBTCxFQUFTLFNBRFQsQ0FDbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFDQSxZQUFJLENBQUNBLEVBQUUsQ0FBQ0MsSUFBSixJQUFZLENBQUNELEVBQUUsQ0FBQ0MsSUFBSCxDQUFROUMsS0FBUixDQUFjLElBQUkrQyxNQUFKLENBQVcsWUFBWSxLQUFLaEMsS0FBakIsR0FBeUIsT0FBcEMsQ0FBZCxDQUFqQixFQUE4RTtBQUM5RSxZQUFJOEIsRUFBRSxDQUFDRyxRQUFQLEVBQWlCO0FBQ2pCLFlBQUksQ0FBQ0gsRUFBRSxDQUFDSSxRQUFSLEVBQWtCO0FBRWxCLFlBQUkzRCxLQUFKLEVBQVdvQyxPQUFPLENBQUNnQixLQUFSLENBQWMsNEJBQTRCRyxFQUFFLENBQUNJLFFBQUgsQ0FBWXZELE1BQXhDLEdBQWlELGFBQWpELEdBQWlFbUQsRUFBRSxDQUFDQyxJQUFsRjs7QUFFWCxhQUFLLElBQUlJLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdMLEVBQUUsQ0FBQ0ksUUFBSCxDQUFZdkQsTUFBaEMsRUFBd0N3RCxDQUFDLEVBQXpDLEVBQTZDO0FBQ3pDLGdCQUFNQyxJQUFJLEdBQUdOLEVBQUUsQ0FBQ0ksUUFBSCxDQUFZQyxDQUFaLENBQWI7QUFDQSxjQUFJLENBQUNDLElBQUksQ0FBQ0MsS0FBVixFQUFpQjtBQUNqQixjQUFJRCxJQUFJLENBQUNFLFlBQUwsSUFBcUJGLElBQUksQ0FBQ0UsWUFBTCxDQUFrQnJELEtBQWxCLENBQXdCLFdBQXhCLENBQXpCLEVBQStEOztBQUMvRCxlQUFLLElBQUlzRCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLEtBQUsxQyxRQUFMLENBQWNsQixNQUFsQyxFQUEwQzRELENBQUMsRUFBM0MsRUFBK0M7QUFDM0Msa0JBQU1DLElBQUksR0FBRyxLQUFLM0MsUUFBTCxDQUFjMEMsQ0FBZCxDQUFiOztBQUNBLGlCQUFLLElBQUlFLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS2xELE1BQUwsQ0FBWVosTUFBaEMsRUFBd0M4RCxDQUFDLEVBQXpDLEVBQTZDO0FBQ3pDLGtCQUFJTCxJQUFJLENBQUNDLEtBQUwsQ0FBV0csSUFBWCxNQUFxQixLQUFLakQsTUFBTCxDQUFZa0QsQ0FBWixDQUF6QixFQUF5QztBQUNyQyxxQkFBSzdDLFNBQUwsQ0FBZSxLQUFLSSxLQUFwQixFQUEyQkksSUFBM0IsQ0FBZ0M7QUFDNUJpQyxrQkFBQUEsS0FBSyxFQUFFRCxJQUFJLENBQUNDLEtBRGdCO0FBRTVCRyxrQkFBQUEsSUFBSSxFQUFFQSxJQUZzQjtBQUc1QkUsa0JBQUFBLEtBQUssRUFBRUQ7QUFIcUIsaUJBQWhDO0FBS0g7QUFDSjtBQUNKO0FBQ0o7QUFDSixPQXRERCxDQXNERSxPQUFPRSxDQUFQLEVBQVU7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBaEMsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksc0RBQXNEa0IsRUFBRSxDQUFDQyxJQUFyRSxFQUEyRVksQ0FBM0U7QUFDSDtBQUNKOztBQUNELFFBQUlwRSxLQUFKLEVBQVc7QUFDUG9DLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHdCQUNSLEtBQUtoQixTQUFMLENBQWUsS0FBS0ksS0FBcEIsRUFBMkJyQixNQURuQixHQUVSLFVBRko7QUFHSDtBQUNKOztBQUVEcUMsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsUUFBSXpDLEtBQUosRUFBVztBQUNQb0MsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksMkJBQ1IsS0FBS2hCLFNBQUwsQ0FBZSxLQUFLSSxLQUFwQixFQUEyQnJCLE1BRG5CLEdBRVIsVUFGSjtBQUdIOztBQUNELFNBQUssSUFBSWtELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS2pDLFNBQUwsQ0FBZSxLQUFLSSxLQUFwQixFQUEyQnJCLE1BQS9DLEVBQXVEa0QsQ0FBQyxFQUF4RCxFQUE0RDtBQUN4RCxZQUFNZSxRQUFRLEdBQUcsS0FBS2hELFNBQUwsQ0FBZSxLQUFLSSxLQUFwQixFQUEyQjZCLENBQTNCLENBQWpCOztBQUNBLFVBQUk7QUFDQWUsUUFBQUEsUUFBUSxDQUFDUCxLQUFULENBQWVPLFFBQVEsQ0FBQ0osSUFBeEIsSUFBZ0MsS0FBSy9DLE1BQUwsQ0FBWW1ELFFBQVEsQ0FBQ0YsS0FBckIsQ0FBaEM7QUFDSCxPQUZELENBRUUsT0FBT0MsQ0FBUCxFQUFVO0FBQ1I7QUFDQTtBQUNBO0FBQ0FoQyxRQUFBQSxPQUFPLENBQUNrQyxLQUFSLENBQWMsc0NBQWQsRUFBc0RGLENBQUMsQ0FBQ0csSUFBeEQ7QUFDSDtBQUNKOztBQUNELFFBQUl2RSxLQUFKLEVBQVdvQyxPQUFPLENBQUNDLEdBQVIsQ0FBWSxvQkFBWjtBQUNkLEdBalZRLENBbVZUO0FBQ0E7QUFDQTs7O0FBQ0FtQyxFQUFBQSxhQUFhLENBQUNDLElBQUQsRUFBTztBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUVBLFFBQUl6RSxLQUFKLEVBQVdvQyxPQUFPLENBQUNDLEdBQVIsQ0FBWSw2QkFBNkJvQyxJQUF6QztBQUNYLFVBQU1DLE1BQU0sR0FBRyxFQUFmOztBQUNBLFNBQUssSUFBSXBCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdtQixJQUFJLENBQUNyRSxNQUF6QixFQUFpQ2tELENBQUMsRUFBbEMsRUFBc0M7QUFDbEMsVUFBSXFCLE1BQUo7O0FBQ0EsVUFBSTtBQUNBQSxRQUFBQSxNQUFNLEdBQUdGLElBQUksQ0FBQ25CLENBQUQsQ0FBSixDQUFRc0IsZUFBakI7QUFDSCxPQUZELENBRUUsT0FBT1IsQ0FBUCxFQUFVO0FBQ1IsWUFBSVMsR0FBRyxHQUFHLDBDQUEwQ0osSUFBSSxDQUFDbkIsQ0FBRCxDQUFKLENBQVF6QyxRQUFSLEVBQXBEOztBQUNBLFlBQUl1RCxDQUFDLENBQUNVLE9BQU4sRUFBZTtBQUNYRCxVQUFBQSxHQUFHLElBQUlULENBQUMsQ0FBQ1UsT0FBVDtBQUNIOztBQUNELFlBQUlWLENBQUMsQ0FBQ1csS0FBTixFQUFhO0FBQ1RGLFVBQUFBLEdBQUcsSUFBSSxlQUFlVCxDQUFDLENBQUNXLEtBQXhCO0FBQ0g7O0FBQ0QzQyxRQUFBQSxPQUFPLENBQUNrQyxLQUFSLENBQWNPLEdBQWQ7QUFDSDs7QUFDRCxVQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNiLFlBQU1LLElBQUksR0FBR0wsTUFBTSxDQUFDTSxvQkFBUCxDQUE0QixHQUE1QixDQUFiOztBQUNBLFdBQUssSUFBSXJCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdvQixJQUFJLENBQUM1RSxNQUF6QixFQUFpQ3dELENBQUMsRUFBbEMsRUFBc0M7QUFDbEMsY0FBTXNCLEdBQUcsR0FBR0YsSUFBSSxDQUFDcEIsQ0FBRCxDQUFoQjs7QUFDQSxhQUFLLElBQUlJLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS3pDLFFBQUwsQ0FBY25CLE1BQWxDLEVBQTBDNEQsQ0FBQyxFQUEzQyxFQUErQztBQUMzQyxnQkFBTUMsSUFBSSxHQUFHLEtBQUsxQyxRQUFMLENBQWN5QyxDQUFkLENBQWI7O0FBQ0EsZUFBSyxJQUFJRSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLEtBQUtqRCxNQUFMLENBQVliLE1BQWhDLEVBQXdDOEQsQ0FBQyxFQUF6QyxFQUE2QztBQUN6QyxnQkFBSWdCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQmxCLElBQWpCLEtBQ0FpQixHQUFHLENBQUNDLFlBQUosQ0FBaUJsQixJQUFqQixFQUF1Qm1CLFdBQXZCLE9BQXlDLEtBQUtuRSxNQUFMLENBQVlpRCxDQUFaLENBRDdDLEVBQzZEO0FBQ3pEUSxjQUFBQSxNQUFNLENBQUM3QyxJQUFQLENBQVk7QUFDUndELGdCQUFBQSxJQUFJLEVBQUVILEdBREU7QUFFUmpCLGdCQUFBQSxJQUFJLEVBQUVBLElBRkU7QUFHUkUsZ0JBQUFBLEtBQUssRUFBRUQ7QUFIQyxlQUFaO0FBS0g7QUFDSjtBQUNKO0FBQ0o7QUFDSjs7QUFDRCxRQUFJbEUsS0FBSixFQUFXb0MsT0FBTyxDQUFDQyxHQUFSLENBQVksbUJBQVo7QUFFWCxXQUFPcUMsTUFBUDtBQUNIOztBQUVEWSxFQUFBQSxjQUFjLENBQUNaLE1BQUQsRUFBUztBQUNuQixRQUFJMUUsS0FBSixFQUFXb0MsT0FBTyxDQUFDQyxHQUFSLENBQVksOEJBQThCcUMsTUFBMUM7O0FBQ1gsU0FBSyxJQUFJcEIsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR29CLE1BQU0sQ0FBQ3RFLE1BQTNCLEVBQW1Da0QsQ0FBQyxFQUFwQyxFQUF3QztBQUNwQyxZQUFNaUMsUUFBUSxHQUFHYixNQUFNLENBQUNwQixDQUFELENBQXZCO0FBQ0FpQyxNQUFBQSxRQUFRLENBQUNGLElBQVQsQ0FBY0csWUFBZCxDQUEyQkQsUUFBUSxDQUFDdEIsSUFBcEMsRUFBMEMsS0FBSy9DLE1BQUwsQ0FBWXFFLFFBQVEsQ0FBQ3BCLEtBQXJCLENBQTFDO0FBQ0g7O0FBQ0QsUUFBSW5FLEtBQUosRUFBV29DLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG9CQUFaO0FBQ2Q7O0FBM1lROztBQThZYixJQUFJb0QsTUFBTSxDQUFDQyxlQUFQLEtBQTJCdEUsU0FBL0IsRUFBMEM7QUFDdENxRSxFQUFBQSxNQUFNLENBQUNDLGVBQVAsR0FBeUIsSUFBSTVFLE1BQUosRUFBekI7QUFDSDs7ZUFDYzJFLE1BQU0sQ0FBQ0MsZSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuY29uc3QgREVCVUcgPSAwO1xyXG5cclxuLy8gdXRpbGl0eSB0byB0dXJuICNycmdnYmIgb3IgcmdiKHIsZyxiKSBpbnRvIFtyZWQsZ3JlZW4sYmx1ZV1cclxuZnVuY3Rpb24gY29sb3JUb1JnYihjb2xvcikge1xyXG4gICAgaWYgKCFjb2xvcikge1xyXG4gICAgICAgIHJldHVybiBbMCwgMCwgMF07XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNvbG9yWzBdID09PSAnIycpIHtcclxuICAgICAgICBjb2xvciA9IGNvbG9yLnNsaWNlKDEpO1xyXG4gICAgICAgIGlmIChjb2xvci5sZW5ndGggPT09IDMpIHtcclxuICAgICAgICAgICAgY29sb3IgPSBjb2xvclswXSArIGNvbG9yWzBdICtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvclsxXSArIGNvbG9yWzFdICtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvclsyXSArIGNvbG9yWzJdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB2YWwgPSBwYXJzZUludChjb2xvciwgMTYpO1xyXG4gICAgICAgIGNvbnN0IHIgPSAodmFsID4+IDE2KSAmIDI1NTtcclxuICAgICAgICBjb25zdCBnID0gKHZhbCA+PiA4KSAmIDI1NTtcclxuICAgICAgICBjb25zdCBiID0gdmFsICYgMjU1O1xyXG4gICAgICAgIHJldHVybiBbciwgZywgYl07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IG1hdGNoID0gY29sb3IubWF0Y2goL3JnYlxcKCguKj8pLCguKj8pLCguKj8pXFwpLyk7XHJcbiAgICAgICAgaWYgKG1hdGNoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXHJcbiAgICAgICAgICAgICAgICBwYXJzZUludChtYXRjaFsxXSksXHJcbiAgICAgICAgICAgICAgICBwYXJzZUludChtYXRjaFsyXSksXHJcbiAgICAgICAgICAgICAgICBwYXJzZUludChtYXRjaFszXSksXHJcbiAgICAgICAgICAgIF07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIFswLCAwLCAwXTtcclxufVxyXG5cclxuLy8gdXRpbGl0eSB0byB0dXJuIFtyZWQsZ3JlZW4sYmx1ZV0gaW50byAjcnJnZ2JiXHJcbmZ1bmN0aW9uIHJnYlRvQ29sb3IocmdiKSB7XHJcbiAgICBjb25zdCB2YWwgPSAocmdiWzBdIDw8IDE2KSB8IChyZ2JbMV0gPDwgOCkgfCByZ2JbMl07XHJcbiAgICByZXR1cm4gJyMnICsgKDB4MTAwMDAwMCArIHZhbCkudG9TdHJpbmcoMTYpLnNsaWNlKDEpO1xyXG59XHJcblxyXG5jbGFzcyBUaW50ZXIge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy8gVGhlIGRlZmF1bHQgY29sb3VyIGtleXMgdG8gYmUgcmVwbGFjZWQgYXMgcmVmZXJyZWQgdG8gaW4gQ1NTXHJcbiAgICAgICAgLy8gKHNob3VsZCBiZSBvdmVycmlkZGVuIGJ5IC5teF90aGVtZV9hY2NlbnRDb2xvciBhbmQgLm14X3RoZW1lX3NlY29uZGFyeUFjY2VudENvbG9yKVxyXG4gICAgICAgIHRoaXMua2V5UmdiID0gW1xyXG4gICAgICAgICAgICBcInJnYigxMTgsIDIwNywgMTY2KVwiLCAvLyBWZWN0b3IgR3JlZW5cclxuICAgICAgICAgICAgXCJyZ2IoMjM0LCAyNDUsIDI0MClcIiwgLy8gVmVjdG9yIExpZ2h0IEdyZWVuXHJcbiAgICAgICAgICAgIFwicmdiKDIxMSwgMjM5LCAyMjUpXCIsIC8vIHJvb21zdWJsaXN0LWxhYmVsLWJnLWNvbG9yICgyMCUgR3JlZW4gb3ZlcmxhaWQgb24gTGlnaHQgR3JlZW4pXHJcbiAgICAgICAgXTtcclxuXHJcbiAgICAgICAgLy8gU29tZSBhbGdlYnJhIHdvcmtpbmdzIGZvciBjYWxjdWxhdGluZyB0aGUgdGludCAlIG9mIFZlY3RvciBHcmVlbiAmIExpZ2h0IEdyZWVuXHJcbiAgICAgICAgLy8geCAqIDExOCArICgxIC0geCkgKiAyNTUgPSAyMzRcclxuICAgICAgICAvLyB4ICogMTE4ICsgMjU1IC0gMjU1ICogeCA9IDIzNFxyXG4gICAgICAgIC8vIHggKiAxMTggLSB4ICogMjU1ID0gMjM0IC0gMjU1XHJcbiAgICAgICAgLy8gKDI1NSAtIDExOCkgeCA9IDI1NSAtIDIzNFxyXG4gICAgICAgIC8vIHggPSAoMjU1IC0gMjM0KSAvICgyNTUgLSAxMTgpID0gMC4xNlxyXG5cclxuICAgICAgICAvLyBUaGUgY29sb3VyIGtleXMgdG8gYmUgcmVwbGFjZWQgYXMgcmVmZXJyZWQgdG8gaW4gU1ZHc1xyXG4gICAgICAgIHRoaXMua2V5SGV4ID0gW1xyXG4gICAgICAgICAgICBcIiM3NkNGQTZcIiwgLy8gVmVjdG9yIEdyZWVuXHJcbiAgICAgICAgICAgIFwiI0VBRjVGMFwiLCAvLyBWZWN0b3IgTGlnaHQgR3JlZW5cclxuICAgICAgICAgICAgXCIjRDNFRkUxXCIsIC8vIHJvb21zdWJsaXN0LWxhYmVsLWJnLWNvbG9yICgyMCUgR3JlZW4gb3ZlcmxhaWQgb24gTGlnaHQgR3JlZW4pXHJcbiAgICAgICAgICAgIFwiI0ZGRkZGRlwiLCAvLyB3aGl0ZSBoaWdobGlnaHRzIG9mIHRoZSBTVkdzIChmb3Igc3dpdGNoaW5nIHRvIGRhcmsgdGhlbWUpXHJcbiAgICAgICAgICAgIFwiIzAwMDAwMFwiLCAvLyBibGFjayBsb3dsaWdodHMgb2YgdGhlIFNWR3MgKGZvciBzd2l0Y2hpbmcgdG8gZGFyayB0aGVtZSlcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICAvLyB0cmFjayB0aGUgcmVwbGFjZW1lbnQgY29sb3VycyBhY3R1YWxseSBiZWluZyB1c2VkXHJcbiAgICAgICAgLy8gZGVmYXVsdHMgdG8gb3VyIGtleXMuXHJcbiAgICAgICAgdGhpcy5jb2xvcnMgPSBbXHJcbiAgICAgICAgICAgIHRoaXMua2V5SGV4WzBdLFxyXG4gICAgICAgICAgICB0aGlzLmtleUhleFsxXSxcclxuICAgICAgICAgICAgdGhpcy5rZXlIZXhbMl0sXHJcbiAgICAgICAgICAgIHRoaXMua2V5SGV4WzNdLFxyXG4gICAgICAgICAgICB0aGlzLmtleUhleFs0XSxcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICAvLyB0cmFjayB0aGUgbW9zdCBjdXJyZW50IHRpbnQgcmVxdWVzdCBpbnB1dHMgKHdoaWNoIG1heSBkaWZmZXIgZnJvbSB0aGVcclxuICAgICAgICAvLyBlbmQgcmVzdWx0IHN0b3JlZCBpbiB0aGlzLmNvbG9yc1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRpbnQgPSBbXHJcbiAgICAgICAgICAgIHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgdW5kZWZpbmVkLFxyXG4gICAgICAgICAgICB1bmRlZmluZWQsXHJcbiAgICAgICAgICAgIHVuZGVmaW5lZCxcclxuICAgICAgICAgICAgdW5kZWZpbmVkLFxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIHRoaXMuY3NzRml4dXBzID0gW1xyXG4gICAgICAgICAgICAvLyB7IHRoZW1lOiB7XHJcbiAgICAgICAgICAgIC8vICAgICAgc3R5bGU6IGEgc3R5bGUgb2JqZWN0IHRoYXQgc2hvdWxkIGJlIGZpeGVkIHVwIHRha2VuIGZyb20gYSBzdHlsZXNoZWV0XHJcbiAgICAgICAgICAgIC8vICAgICAgYXR0cjogbmFtZSBvZiB0aGUgYXR0cmlidXRlIHRvIGJlIGNsb2JiZXJlZCwgZS5nLiAnY29sb3InXHJcbiAgICAgICAgICAgIC8vICAgICAgaW5kZXg6IG9yZGluYWwgb2YgcHJpbWFyeSwgc2Vjb25kYXJ5IG9yIHRlcnRpYXJ5XHJcbiAgICAgICAgICAgIC8vICAgfSxcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIC8vIENTUyBhdHRyaWJ1dGVzIHRvIGJlIGZpeGVkIHVwXHJcbiAgICAgICAgdGhpcy5jc3NBdHRycyA9IFtcclxuICAgICAgICAgICAgXCJjb2xvclwiLFxyXG4gICAgICAgICAgICBcImJhY2tncm91bmRDb2xvclwiLFxyXG4gICAgICAgICAgICBcImJvcmRlckNvbG9yXCIsXHJcbiAgICAgICAgICAgIFwiYm9yZGVyVG9wQ29sb3JcIixcclxuICAgICAgICAgICAgXCJib3JkZXJCb3R0b21Db2xvclwiLFxyXG4gICAgICAgICAgICBcImJvcmRlckxlZnRDb2xvclwiLFxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIHRoaXMuc3ZnQXR0cnMgPSBbXHJcbiAgICAgICAgICAgIFwiZmlsbFwiLFxyXG4gICAgICAgICAgICBcInN0cm9rZVwiLFxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIC8vIExpc3Qgb2YgZnVuY3Rpb25zIHRvIGNhbGwgd2hlbiB0aGUgdGludCBjaGFuZ2VzLlxyXG4gICAgICAgIHRoaXMudGludGFibGVzID0gW107XHJcblxyXG4gICAgICAgIC8vIHRoZSBjdXJyZW50bHkgbG9hZGVkIHRoZW1lIChpZiBhbnkpXHJcbiAgICAgICAgdGhpcy50aGVtZSA9IHVuZGVmaW5lZDtcclxuXHJcbiAgICAgICAgLy8gd2hldGhlciB0byBmb3JjZSBhIHRpbnQgKGUuZy4gYWZ0ZXIgY2hhbmdpbmcgdGhlbWUpXHJcbiAgICAgICAgdGhpcy5mb3JjZVRpbnQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlZ2lzdGVyIGEgY2FsbGJhY2sgdG8gZmlyZSB3aGVuIHRoZSB0aW50IGNoYW5nZXMuXHJcbiAgICAgKiBUaGlzIGlzIHVzZWQgdG8gcmV3cml0ZSB0aGUgdGludGFibGUgU1ZHcyB3aXRoIHRoZSBuZXcgdGludC5cclxuICAgICAqXHJcbiAgICAgKiBJdCdzIG5vdCBwb3NzaWJsZSB0byB1bnJlZ2lzdGVyIGEgdGludGFibGUgY2FsbGJhY2suIFNvIHRoaXMgY2FuIG9ubHkgYmVcclxuICAgICAqIHVzZWQgdG8gcmVnaXN0ZXIgYSBzdGF0aWMgY2FsbGJhY2suIElmIGEgc2V0IG9mIHRpbnRhYmxlcyB3aWxsIGNoYW5nZVxyXG4gICAgICogb3ZlciB0aW1lIHRoZW4gdGhlIGJlc3QgYmV0IGlzIHRvIHJlZ2lzdGVyIGEgc2luZ2xlIGNhbGxiYWNrIGZvciB0aGVcclxuICAgICAqIGVudGlyZSBzZXQuXHJcbiAgICAgKlxyXG4gICAgICogVG8gZW5zdXJlIHRoZSB0aW50YWJsZSB3b3JrIGhhcHBlbnMgYXQgbGVhc3Qgb25jZSwgaXQgaXMgYWxzbyBjYWxsZWQgYXNcclxuICAgICAqIHBhcnQgb2YgcmVnaXN0cmF0aW9uLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IHRpbnRhYmxlIEZ1bmN0aW9uIHRvIGNhbGwgd2hlbiB0aGUgdGludCBjaGFuZ2VzLlxyXG4gICAgICovXHJcbiAgICByZWdpc3RlclRpbnRhYmxlKHRpbnRhYmxlKSB7XHJcbiAgICAgICAgdGhpcy50aW50YWJsZXMucHVzaCh0aW50YWJsZSk7XHJcbiAgICAgICAgdGludGFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRLZXlSZ2IoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMua2V5UmdiO1xyXG4gICAgfVxyXG5cclxuICAgIHRpbnQocHJpbWFyeUNvbG9yLCBzZWNvbmRhcnlDb2xvciwgdGVydGlhcnlDb2xvcikge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5yZWFjaGFibGVcclxuICAgICAgICB0aGlzLmN1cnJlbnRUaW50WzBdID0gcHJpbWFyeUNvbG9yO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRpbnRbMV0gPSBzZWNvbmRhcnlDb2xvcjtcclxuICAgICAgICB0aGlzLmN1cnJlbnRUaW50WzJdID0gdGVydGlhcnlDb2xvcjtcclxuXHJcbiAgICAgICAgdGhpcy5jYWxjQ3NzRml4dXBzKCk7XHJcblxyXG4gICAgICAgIGlmIChERUJVRykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlRpbnRlci50aW50KFwiICsgcHJpbWFyeUNvbG9yICsgXCIsIFwiICtcclxuICAgICAgICAgICAgICAgIHNlY29uZGFyeUNvbG9yICsgXCIsIFwiICtcclxuICAgICAgICAgICAgICAgIHRlcnRpYXJ5Q29sb3IgKyBcIilcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXByaW1hcnlDb2xvcikge1xyXG4gICAgICAgICAgICBwcmltYXJ5Q29sb3IgPSB0aGlzLmtleVJnYlswXTtcclxuICAgICAgICAgICAgc2Vjb25kYXJ5Q29sb3IgPSB0aGlzLmtleVJnYlsxXTtcclxuICAgICAgICAgICAgdGVydGlhcnlDb2xvciA9IHRoaXMua2V5UmdiWzJdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFzZWNvbmRhcnlDb2xvcikge1xyXG4gICAgICAgICAgICBjb25zdCB4ID0gMC4xNjsgLy8gYXZlcmFnZSB3ZWlnaHRpbmcgZmFjdG9yIGNhbGN1bGF0ZWQgZnJvbSB2ZWN0b3IgZ3JlZW4gJiBsaWdodCBncmVlblxyXG4gICAgICAgICAgICBjb25zdCByZ2IgPSBjb2xvclRvUmdiKHByaW1hcnlDb2xvcik7XHJcbiAgICAgICAgICAgIHJnYlswXSA9IHggKiByZ2JbMF0gKyAoMSAtIHgpICogMjU1O1xyXG4gICAgICAgICAgICByZ2JbMV0gPSB4ICogcmdiWzFdICsgKDEgLSB4KSAqIDI1NTtcclxuICAgICAgICAgICAgcmdiWzJdID0geCAqIHJnYlsyXSArICgxIC0geCkgKiAyNTU7XHJcbiAgICAgICAgICAgIHNlY29uZGFyeUNvbG9yID0gcmdiVG9Db2xvcihyZ2IpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0ZXJ0aWFyeUNvbG9yKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHggPSAwLjE5O1xyXG4gICAgICAgICAgICBjb25zdCByZ2IxID0gY29sb3JUb1JnYihwcmltYXJ5Q29sb3IpO1xyXG4gICAgICAgICAgICBjb25zdCByZ2IyID0gY29sb3JUb1JnYihzZWNvbmRhcnlDb2xvcik7XHJcbiAgICAgICAgICAgIHJnYjFbMF0gPSB4ICogcmdiMVswXSArICgxIC0geCkgKiByZ2IyWzBdO1xyXG4gICAgICAgICAgICByZ2IxWzFdID0geCAqIHJnYjFbMV0gKyAoMSAtIHgpICogcmdiMlsxXTtcclxuICAgICAgICAgICAgcmdiMVsyXSA9IHggKiByZ2IxWzJdICsgKDEgLSB4KSAqIHJnYjJbMl07XHJcbiAgICAgICAgICAgIHRlcnRpYXJ5Q29sb3IgPSByZ2JUb0NvbG9yKHJnYjEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZm9yY2VUaW50ID09IGZhbHNlICYmXHJcbiAgICAgICAgICAgIHRoaXMuY29sb3JzWzBdID09PSBwcmltYXJ5Q29sb3IgJiZcclxuICAgICAgICAgICAgdGhpcy5jb2xvcnNbMV0gPT09IHNlY29uZGFyeUNvbG9yICYmXHJcbiAgICAgICAgICAgIHRoaXMuY29sb3JzWzJdID09PSB0ZXJ0aWFyeUNvbG9yKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZm9yY2VUaW50ID0gZmFsc2U7XHJcblxyXG4gICAgICAgIHRoaXMuY29sb3JzWzBdID0gcHJpbWFyeUNvbG9yO1xyXG4gICAgICAgIHRoaXMuY29sb3JzWzFdID0gc2Vjb25kYXJ5Q29sb3I7XHJcbiAgICAgICAgdGhpcy5jb2xvcnNbMl0gPSB0ZXJ0aWFyeUNvbG9yO1xyXG5cclxuICAgICAgICBpZiAoREVCVUcpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJUaW50ZXIudGludCBmaW5hbDogKFwiICsgcHJpbWFyeUNvbG9yICsgXCIsIFwiICtcclxuICAgICAgICAgICAgICAgIHNlY29uZGFyeUNvbG9yICsgXCIsIFwiICtcclxuICAgICAgICAgICAgICAgIHRlcnRpYXJ5Q29sb3IgKyBcIilcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBnbyB0aHJvdWdoIG1hbnVhbGx5IGZpeGluZyB1cCB0aGUgc3R5bGVzaGVldHMuXHJcbiAgICAgICAgdGhpcy5hcHBseUNzc0ZpeHVwcygpO1xyXG5cclxuICAgICAgICAvLyB0ZWxsIGFsbCB0aGUgU1ZHcyB0byBnbyBmaXggdGhlbXNlbHZlcyB1cFxyXG4gICAgICAgIC8vIHdlIGRvbid0IGRvIHRoaXMgYXMgYSBkaXNwYXRjaCBvdGhlcndpc2UgaXQgd2lsbCB2aXN1YWxseSBsYWdcclxuICAgICAgICB0aGlzLnRpbnRhYmxlcy5mb3JFYWNoKGZ1bmN0aW9uKHRpbnRhYmxlKSB7XHJcbiAgICAgICAgICAgIHRpbnRhYmxlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGludFN2Z1doaXRlKHdoaXRlQ29sb3IpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRUaW50WzNdID0gd2hpdGVDb2xvcjtcclxuXHJcbiAgICAgICAgaWYgKCF3aGl0ZUNvbG9yKSB7XHJcbiAgICAgICAgICAgIHdoaXRlQ29sb3IgPSB0aGlzLmNvbG9yc1szXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY29sb3JzWzNdID09PSB3aGl0ZUNvbG9yKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jb2xvcnNbM10gPSB3aGl0ZUNvbG9yO1xyXG4gICAgICAgIHRoaXMudGludGFibGVzLmZvckVhY2goZnVuY3Rpb24odGludGFibGUpIHtcclxuICAgICAgICAgICAgdGludGFibGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0aW50U3ZnQmxhY2soYmxhY2tDb2xvcikge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRpbnRbNF0gPSBibGFja0NvbG9yO1xyXG5cclxuICAgICAgICBpZiAoIWJsYWNrQ29sb3IpIHtcclxuICAgICAgICAgICAgYmxhY2tDb2xvciA9IHRoaXMuY29sb3JzWzRdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5jb2xvcnNbNF0gPT09IGJsYWNrQ29sb3IpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNvbG9yc1s0XSA9IGJsYWNrQ29sb3I7XHJcbiAgICAgICAgdGhpcy50aW50YWJsZXMuZm9yRWFjaChmdW5jdGlvbih0aW50YWJsZSkge1xyXG4gICAgICAgICAgICB0aW50YWJsZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBzZXRUaGVtZSh0aGVtZSkge1xyXG4gICAgICAgIHRoaXMudGhlbWUgPSB0aGVtZTtcclxuXHJcbiAgICAgICAgLy8gdXBkYXRlIGtleVJnYiBmcm9tIHRoZSBjdXJyZW50IHRoZW1lIENTUyBpdHNlbGYsIGlmIGl0IGRlZmluZXMgaXRcclxuICAgICAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ214X3RoZW1lX2FjY2VudENvbG9yJykpIHtcclxuICAgICAgICAgICAgdGhpcy5rZXlSZ2JbMF0gPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdteF90aGVtZV9hY2NlbnRDb2xvcicpKS5jb2xvcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdteF90aGVtZV9zZWNvbmRhcnlBY2NlbnRDb2xvcicpKSB7XHJcbiAgICAgICAgICAgIHRoaXMua2V5UmdiWzFdID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoXHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXhfdGhlbWVfc2Vjb25kYXJ5QWNjZW50Q29sb3InKSkuY29sb3I7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXhfdGhlbWVfdGVydGlhcnlBY2NlbnRDb2xvcicpKSB7XHJcbiAgICAgICAgICAgIHRoaXMua2V5UmdiWzJdID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoXHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXhfdGhlbWVfdGVydGlhcnlBY2NlbnRDb2xvcicpKS5jb2xvcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuY2FsY0Nzc0ZpeHVwcygpO1xyXG4gICAgICAgIHRoaXMuZm9yY2VUaW50ID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy50aW50KHRoaXMuY3VycmVudFRpbnRbMF0sIHRoaXMuY3VycmVudFRpbnRbMV0sIHRoaXMuY3VycmVudFRpbnRbMl0pO1xyXG5cclxuICAgICAgICBpZiAodGhlbWUgPT09ICdkYXJrJykge1xyXG4gICAgICAgICAgICAvLyBhYnVzZSB0aGUgdGludGVyIHRvIGNoYW5nZSBhbGwgdGhlIFNWRydzICNmZmYgdG8gIzJkMmQyZFxyXG4gICAgICAgICAgICAvLyBYWFg6IG9idmlvdXNseSB0aGlzIHNob3VsZG4ndCBiZSBoYXJkY29kZWQgaGVyZS5cclxuICAgICAgICAgICAgdGhpcy50aW50U3ZnV2hpdGUoJyMyZDJkMmQnKTtcclxuICAgICAgICAgICAgdGhpcy50aW50U3ZnQmxhY2soJyNkZGRkZGQnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRpbnRTdmdXaGl0ZSgnI2ZmZmZmZicpO1xyXG4gICAgICAgICAgICB0aGlzLnRpbnRTdmdCbGFjaygnIzAwMDAwMCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjYWxjQ3NzRml4dXBzKCkge1xyXG4gICAgICAgIC8vIGNhY2hlIG91ciBmaXh1cHNcclxuICAgICAgICBpZiAodGhpcy5jc3NGaXh1cHNbdGhpcy50aGVtZV0pIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKERFQlVHKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGVidWcoXCJjYWxjQ3NzRml4dXBzIHN0YXJ0IGZvciBcIiArIHRoaXMudGhlbWUgKyBcIiAoY2hlY2tpbmcgXCIgK1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuc3R5bGVTaGVldHMubGVuZ3RoICtcclxuICAgICAgICAgICAgICAgIFwiIHN0eWxlc2hlZXRzKVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuY3NzRml4dXBzW3RoaXMudGhlbWVdID0gW107XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZG9jdW1lbnQuc3R5bGVTaGVldHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3Qgc3MgPSBkb2N1bWVudC5zdHlsZVNoZWV0c1tpXTtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGlmICghc3MpIGNvbnRpbnVlOyAvLyB3ZWxsIGRvbmUgc2FmYXJpID46KFxyXG4gICAgICAgICAgICAgICAgLy8gQ2hyb21pdW0gYXBwYXJlbnRseSBzb21ldGltZXMgcmV0dXJucyBudWxsIGhlcmU7IHVuc3VyZSB3aHkuXHJcbiAgICAgICAgICAgICAgICAvLyBzZWUgJDE0NTM0OTA3MzY5OTcyRlJYQng6bWF0cml4Lm9yZyBpbiBIUVxyXG4gICAgICAgICAgICAgICAgLy8gLi4uYWgsIGl0J3MgYmVjYXVzZSB0aGVyZSdzIGEgdGhpcmQgcGFydHkgZXh0ZW5zaW9uIGxpa2VcclxuICAgICAgICAgICAgICAgIC8vIHByaXZhY3liYWRnZXIgaW5zZXJ0aW5nIGl0cyBvd24gc3R5bGVzaGVldCBpbiB0aGVyZSB3aXRoIGFcclxuICAgICAgICAgICAgICAgIC8vIHJlc291cmNlOi8vIFVSSSBvciBzb21ldGhpbmcgd2hpY2ggcmVzdWx0cyBpbiBhIFhTUyBlcnJvci5cclxuICAgICAgICAgICAgICAgIC8vIFNlZSBhbHNvICN2ZWN0b3I6bWF0cml4Lm9yZy8kMTQ1MzU3NjY5Njg1Mzg2ZWJDZnI6bWF0cml4Lm9yZ1xyXG4gICAgICAgICAgICAgICAgLy8gLi4uZXhjZXB0IHNvbWUgYnJvd3NlcnMgYXBwYXJlbnRseSByZXR1cm4gc3R5bGVzaGVldHMgd2l0aG91dFxyXG4gICAgICAgICAgICAgICAgLy8gaHJlZnMsIHdoaWNoIHdlIGhhdmUgbm8gY2hvaWNlIGJ1dCBpZ25vcmUgcmlnaHQgbm93XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gWFhYIHNlcmlvdXNseT8gd2UgYXJlIGhhcmRjb2RpbmcgdGhlIG5hbWUgb2YgdmVjdG9yJ3MgQ1NTIGZpbGUgaW5cclxuICAgICAgICAgICAgICAgIC8vIGhlcmU/XHJcbiAgICAgICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAgICAgLy8gV2h5IGRvIHdlIG5lZWQgdG8gbGltaXQgaXQgdG8gdmVjdG9yJ3MgQ1NTIGZpbGUgYW55d2F5IC0gaWYgdGhlcmVcclxuICAgICAgICAgICAgICAgIC8vIGFyZSBvdGhlciBDU1MgZmlsZXMgYWZmZWN0aW5nIHRoZSBkb2MgZG9uJ3Qgd2Ugd2FudCB0byBhcHBseSB0aGVcclxuICAgICAgICAgICAgICAgIC8vIHNhbWUgdHJhbnNmb3JtYXRpb25zIHRvIHRoZW0/XHJcbiAgICAgICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAgICAgLy8gSXRlcmF0aW5nIHRocm91Z2ggdGhlIENTUyBsb29raW5nIGZvciBtYXRjaGVzIHRvIGhhY2sgb24gZmVlbHNcclxuICAgICAgICAgICAgICAgIC8vIHByZXR0eSBob3JyaWJsZSBhbnl3YXkuIEFuZCB3aGF0IGlmIHRoZSBhcHBsaWNhdGlvbiBza2luIGRvZXNuJ3QgdXNlXHJcbiAgICAgICAgICAgICAgICAvLyBWZWN0b3IgR3JlZW4gYXMgaXRzIHByaW1hcnkgY29sb3I/XHJcbiAgICAgICAgICAgICAgICAvLyAtLXJpY2h2ZGhcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBZZXMsIHRpbnRpbmcgYXNzdW1lcyB0aGF0IHlvdSBhcmUgdXNpbmcgdGhlIFJpb3Qgc2tpbiBmb3Igbm93LlxyXG4gICAgICAgICAgICAgICAgLy8gVGhlIHJpZ2h0IHNvbHV0aW9uIHdpbGwgYmUgdG8gbW92ZSB0aGUgQ1NTIG92ZXIgdG8gcmVhY3Qtc2RrLlxyXG4gICAgICAgICAgICAgICAgLy8gQW5kIHllcywgdGhlIGRlZmF1bHQgYXNzZXRzIGZvciB0aGUgYmFzZSBza2luIG1pZ2h0IGFzIHdlbGwgdXNlXHJcbiAgICAgICAgICAgICAgICAvLyBWZWN0b3IgR3JlZW4gYXMgYW55IG90aGVyIGNvbG91ci5cclxuICAgICAgICAgICAgICAgIC8vIC0tbWF0dGhld1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIHN0eWxlc2hlZXRzIHdlIGRvbid0IGhhdmUgcGVybWlzc2lvbiB0byBhY2Nlc3MgKGVnLiBvbmVzIGZyb20gZXh0ZW5zaW9ucykgaGF2ZSBhIG51bGxcclxuICAgICAgICAgICAgICAgIC8vIGhyZWYgYW5kIHdpbGwgdGhyb3cgZXhjZXB0aW9ucyBpZiB3ZSB0cnkgdG8gYWNjZXNzIHRoZWlyIHJ1bGVzLlxyXG4gICAgICAgICAgICAgICAgaWYgKCFzcy5ocmVmIHx8ICFzcy5ocmVmLm1hdGNoKG5ldyBSZWdFeHAoJy90aGVtZS0nICsgdGhpcy50aGVtZSArICcuY3NzJCcpKSkgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3MuZGlzYWJsZWQpIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFzcy5jc3NSdWxlcykgY29udGludWU7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKERFQlVHKSBjb25zb2xlLmRlYnVnKFwiY2FsY0Nzc0ZpeHVwcyBjaGVja2luZyBcIiArIHNzLmNzc1J1bGVzLmxlbmd0aCArIFwiIHJ1bGVzIGZvciBcIiArIHNzLmhyZWYpO1xyXG5cclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgc3MuY3NzUnVsZXMubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBydWxlID0gc3MuY3NzUnVsZXNbal07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFydWxlLnN0eWxlKSBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocnVsZS5zZWxlY3RvclRleHQgJiYgcnVsZS5zZWxlY3RvclRleHQubWF0Y2goLyNteF90aGVtZS8pKSBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBrID0gMDsgayA8IHRoaXMuY3NzQXR0cnMubGVuZ3RoOyBrKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXR0ciA9IHRoaXMuY3NzQXR0cnNba107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGwgPSAwOyBsIDwgdGhpcy5rZXlSZ2IubGVuZ3RoOyBsKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChydWxlLnN0eWxlW2F0dHJdID09PSB0aGlzLmtleVJnYltsXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3NzRml4dXBzW3RoaXMudGhlbWVdLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZTogcnVsZS5zdHlsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cjogYXR0cixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg6IGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgIC8vIENhdGNoIGFueSByYW5kb20gZXhjZXB0aW9ucyB0aGF0IGhhcHBlbiBoZXJlOiBhbGwgc29ydHMgb2YgdGhpbmdzIGNhbiBnb1xyXG4gICAgICAgICAgICAgICAgLy8gd3Jvbmcgd2l0aCB0aGlzIChudWxscywgU2VjdXJpdHlFcnJvcnMpIGFuZCBtb3N0bHkgaXQncyBmb3Igb3RoZXJcclxuICAgICAgICAgICAgICAgIC8vIHN0eWxlc2hlZXRzIHRoYXQgd2UgZG9uJ3Qgd2FudCB0byBwcm9jZXMgYW55d2F5LiBXZSBzaG91bGQgbm90IHByb3BhZ2F0ZSBhblxyXG4gICAgICAgICAgICAgICAgLy8gZXhjZXB0aW9uIG91dCBzaW5jZSB0aGlzIHdpbGwgY2F1c2UgdGhlIGFwcCB0byBmYWlsIHRvIHN0YXJ0LlxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJGYWlsZWQgdG8gY2FsY3VsYXRlIENTUyBmaXh1cHMgZm9yIGEgc3R5bGVzaGVldDogXCIgKyBzcy5ocmVmLCBlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoREVCVUcpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJjYWxjQ3NzRml4dXBzIGVuZCAoXCIgK1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jc3NGaXh1cHNbdGhpcy50aGVtZV0ubGVuZ3RoICtcclxuICAgICAgICAgICAgICAgIFwiIGZpeHVwcylcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFwcGx5Q3NzRml4dXBzKCkge1xyXG4gICAgICAgIGlmIChERUJVRykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImFwcGx5Q3NzRml4dXBzIHN0YXJ0IChcIiArXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNzc0ZpeHVwc1t0aGlzLnRoZW1lXS5sZW5ndGggK1xyXG4gICAgICAgICAgICAgICAgXCIgZml4dXBzKVwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmNzc0ZpeHVwc1t0aGlzLnRoZW1lXS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBjc3NGaXh1cCA9IHRoaXMuY3NzRml4dXBzW3RoaXMudGhlbWVdW2ldO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY3NzRml4dXAuc3R5bGVbY3NzRml4dXAuYXR0cl0gPSB0aGlzLmNvbG9yc1tjc3NGaXh1cC5pbmRleF07XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgIC8vIEZpcmVmb3ggUXVhbnR1bSBleHBsb2RlcyBpZiB5b3UgbWFudWFsbHkgZWRpdCB0aGUgQ1NTIGluIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gaW5zcGVjdG9yIGFuZCB0aGVuIHRyeSB0byBkbyBhIHRpbnQsIGFzIGFwcGFyZW50bHkgYWxsIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gZml4dXBzIGFyZSB0aGVuIHN0YWxlLlxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBhcHBseSBjc3NGaXh1cCBpbiBUaW50ZXIhIFwiLCBlLm5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChERUJVRykgY29uc29sZS5sb2coXCJhcHBseUNzc0ZpeHVwcyBlbmRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gWFhYOiB3ZSBjb3VsZCBqdXN0IG1vdmUgdGhpcyBhbGwgaW50byBUaW50YWJsZVN2ZywgYnV0IGFzIGl0J3Mgc28gc2ltaWxhclxyXG4gICAgLy8gdG8gdGhlIENTUyBmaXh1cCBzdHVmZiBpbiBUaW50ZXIgKGp1c3QgdGhhdCB0aGUgZml4dXBzIGFyZSBzdG9yZWQgaW4gVGludGFibGVTdmcpXHJcbiAgICAvLyBrZWVwaW5nIGl0IGhlcmUgZm9yIG5vdy5cclxuICAgIGNhbGNTdmdGaXh1cHMoc3Zncykge1xyXG4gICAgICAgIC8vIGdvIHRocm91Z2ggbWFudWFsbHkgZml4aW5nIHVwIFNWRyBjb2xvdXJzLlxyXG4gICAgICAgIC8vIHdlIGNvdWxkIGRvIHRoaXMgYnkgc3R5bGVzaGVldHMsIGJ1dCBrZWVwaW5nIHRoZSBzdHlsZXNoZWV0c1xyXG4gICAgICAgIC8vIHVwZGF0ZWQgd291bGQgYmUgYSBQSVRBLCBzbyBqdXN0IGJydXRlLWZvcmNlIHNlYXJjaCBmb3IgdGhlXHJcbiAgICAgICAgLy8ga2V5IGNvbG91cjsgY2FjaGUgdGhlIGVsZW1lbnQgYW5kIGFwcGx5LlxyXG5cclxuICAgICAgICBpZiAoREVCVUcpIGNvbnNvbGUubG9nKFwiY2FsY1N2Z0ZpeHVwcyBzdGFydCBmb3IgXCIgKyBzdmdzKTtcclxuICAgICAgICBjb25zdCBmaXh1cHMgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHN2Z3MubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IHN2Z0RvYztcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIHN2Z0RvYyA9IHN2Z3NbaV0uY29udGVudERvY3VtZW50O1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbXNnID0gJ0ZhaWxlZCB0byBnZXQgc3ZnLmNvbnRlbnREb2N1bWVudCBvZiAnICsgc3Znc1tpXS50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGUubWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1zZyArPSBlLm1lc3NhZ2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoZS5zdGFjaykge1xyXG4gICAgICAgICAgICAgICAgICAgIG1zZyArPSAnIHwgc3RhY2s6ICcgKyBlLnN0YWNrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihtc2cpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICghc3ZnRG9jKSBjb250aW51ZTtcclxuICAgICAgICAgICAgY29uc3QgdGFncyA9IHN2Z0RvYy5nZXRFbGVtZW50c0J5VGFnTmFtZShcIipcIik7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdGFncy5sZW5ndGg7IGorKykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFnID0gdGFnc1tqXTtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGsgPSAwOyBrIDwgdGhpcy5zdmdBdHRycy5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dHIgPSB0aGlzLnN2Z0F0dHJzW2tdO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGwgPSAwOyBsIDwgdGhpcy5rZXlIZXgubGVuZ3RoOyBsKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhZy5nZXRBdHRyaWJ1dGUoYXR0cikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZy5nZXRBdHRyaWJ1dGUoYXR0cikudG9VcHBlckNhc2UoKSA9PT0gdGhpcy5rZXlIZXhbbF0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpeHVwcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBub2RlOiB0YWcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cjogYXR0cixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleDogbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChERUJVRykgY29uc29sZS5sb2coXCJjYWxjU3ZnRml4dXBzIGVuZFwiKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZpeHVwcztcclxuICAgIH1cclxuXHJcbiAgICBhcHBseVN2Z0ZpeHVwcyhmaXh1cHMpIHtcclxuICAgICAgICBpZiAoREVCVUcpIGNvbnNvbGUubG9nKFwiYXBwbHlTdmdGaXh1cHMgc3RhcnQgZm9yIFwiICsgZml4dXBzKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpeHVwcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBzdmdGaXh1cCA9IGZpeHVwc1tpXTtcclxuICAgICAgICAgICAgc3ZnRml4dXAubm9kZS5zZXRBdHRyaWJ1dGUoc3ZnRml4dXAuYXR0ciwgdGhpcy5jb2xvcnNbc3ZnRml4dXAuaW5kZXhdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKERFQlVHKSBjb25zb2xlLmxvZyhcImFwcGx5U3ZnRml4dXBzIGVuZFwiKTtcclxuICAgIH1cclxufVxyXG5cclxuaWYgKGdsb2JhbC5zaW5nbGV0b25UaW50ZXIgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgZ2xvYmFsLnNpbmdsZXRvblRpbnRlciA9IG5ldyBUaW50ZXIoKTtcclxufVxyXG5leHBvcnQgZGVmYXVsdCBnbG9iYWwuc2luZ2xldG9uVGludGVyO1xyXG4iXX0=