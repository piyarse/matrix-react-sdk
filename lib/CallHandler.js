"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MatrixClientPeg = require("./MatrixClientPeg");

var _PlatformPeg = _interopRequireDefault(require("./PlatformPeg"));

var _Modal = _interopRequireDefault(require("./Modal"));

var sdk = _interopRequireWildcard(require("./index"));

var _languageHandler = require("./languageHandler");

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var _cryptodevices = require("./cryptodevices");

var _WidgetUtils = _interopRequireDefault(require("./utils/WidgetUtils"));

var _WidgetEchoStore = _interopRequireDefault(require("./stores/WidgetEchoStore"));

var _SettingsStore = _interopRequireWildcard(require("./settings/SettingsStore"));

var _NamingUtils = require("./utils/NamingUtils");

var _Jitsi = require("./widgets/Jitsi");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Manages a list of all the currently active calls.
 *
 * This handler dispatches when voip calls are added/updated/removed from this list:
 * {
 *   action: 'call_state'
 *   room_id: <room ID of the call>
 * }
 *
 * To know the state of the call, this handler exposes a getter to
 * obtain the call for a room:
 *   var call = CallHandler.getCall(roomId)
 *   var state = call.call_state; // ringing|ringback|connected|ended|busy|stop_ringback|stop_ringing
 *
 * This handler listens for and handles the following actions:
 * {
 *   action: 'place_call',
 *   type: 'voice|video',
 *   room_id: <room that the place call button was pressed in>
 * }
 *
 * {
 *   action: 'incoming_call'
 *   call: MatrixCall
 * }
 *
 * {
 *   action: 'hangup'
 *   room_id: <room that the hangup button was pressed in>
 * }
 *
 * {
 *   action: 'answer'
 *   room_id: <room that the answer button was pressed in>
 * }
 */
global.mxCalls = {//room_id: MatrixCall
};
const calls = global.mxCalls;
let ConferenceHandler = null;
const audioPromises = {};

function play(audioId) {
  // TODO: Attach an invisible element for this instead
  // which listens?
  const audio = document.getElementById(audioId);

  if (audio) {
    const playAudio = async () => {
      try {
        // This still causes the chrome debugger to break on promise rejection if
        // the promise is rejected, even though we're catching the exception.
        await audio.play();
      } catch (e) {
        // This is usually because the user hasn't interacted with the document,
        // or chrome doesn't think so and is denying the request. Not sure what
        // we can really do here...
        // https://github.com/vector-im/riot-web/issues/7657
        console.log("Unable to play audio clip", e);
      }
    };

    if (audioPromises[audioId]) {
      audioPromises[audioId] = audioPromises[audioId].then(() => {
        audio.load();
        return playAudio();
      });
    } else {
      audioPromises[audioId] = playAudio();
    }
  }
}

function pause(audioId) {
  // TODO: Attach an invisible element for this instead
  // which listens?
  const audio = document.getElementById(audioId);

  if (audio) {
    if (audioPromises[audioId]) {
      audioPromises[audioId] = audioPromises[audioId].then(() => audio.pause());
    } else {
      // pause doesn't actually return a promise, but might as well do this for symmetry with play();
      audioPromises[audioId] = audio.pause();
    }
  }
}

function _reAttemptCall(call) {
  if (call.direction === 'outbound') {
    _dispatcher.default.dispatch({
      action: 'place_call',
      room_id: call.roomId,
      type: call.type
    });
  } else {
    call.answer();
  }
}

function _setCallListeners(call) {
  call.on("error", function (err) {
    console.error("Call error:", err);

    if (err.code === 'unknown_devices') {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

      _Modal.default.createTrackedDialog('Call Failed', '', QuestionDialog, {
        title: (0, _languageHandler._t)('Call Failed'),
        description: (0, _languageHandler._t)("There are unknown sessions in this room: " + "if you proceed without verifying them, it will be " + "possible for someone to eavesdrop on your call."),
        button: (0, _languageHandler._t)('Review Sessions'),
        onFinished: function (confirmed) {
          if (confirmed) {
            const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(call.roomId);

            (0, _cryptodevices.showUnknownDeviceDialogForCalls)(_MatrixClientPeg.MatrixClientPeg.get(), room, () => {
              _reAttemptCall(call);
            }, call.direction === 'outbound' ? (0, _languageHandler._t)("Call Anyway") : (0, _languageHandler._t)("Answer Anyway"), call.direction === 'outbound' ? (0, _languageHandler._t)("Call") : (0, _languageHandler._t)("Answer"));
          }
        }
      });
    } else {
      if (_MatrixClientPeg.MatrixClientPeg.get().getTurnServers().length === 0 && _SettingsStore.default.getValue("fallbackICEServerAllowed") === null) {
        _showICEFallbackPrompt();

        return;
      }

      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Call Failed', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Call Failed'),
        description: err.message
      });
    }
  });
  call.on("hangup", function () {
    _setCallState(undefined, call.roomId, "ended");
  }); // map web rtc states to dummy UI state
  // ringing|ringback|connected|ended|busy|stop_ringback|stop_ringing

  call.on("state", function (newState, oldState) {
    if (newState === "ringing") {
      _setCallState(call, call.roomId, "ringing");

      pause("ringbackAudio");
    } else if (newState === "invite_sent") {
      _setCallState(call, call.roomId, "ringback");

      play("ringbackAudio");
    } else if (newState === "ended" && oldState === "connected") {
      _setCallState(undefined, call.roomId, "ended");

      pause("ringbackAudio");
      play("callendAudio");
    } else if (newState === "ended" && oldState === "invite_sent" && (call.hangupParty === "remote" || call.hangupParty === "local" && call.hangupReason === "invite_timeout")) {
      _setCallState(call, call.roomId, "busy");

      pause("ringbackAudio");
      play("busyAudio");
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Call Handler', 'Call Timeout', ErrorDialog, {
        title: (0, _languageHandler._t)('Call Timeout'),
        description: (0, _languageHandler._t)('The remote side failed to pick up') + '.'
      });
    } else if (oldState === "invite_sent") {
      _setCallState(call, call.roomId, "stop_ringback");

      pause("ringbackAudio");
    } else if (oldState === "ringing") {
      _setCallState(call, call.roomId, "stop_ringing");

      pause("ringbackAudio");
    } else if (newState === "connected") {
      _setCallState(call, call.roomId, "connected");

      pause("ringbackAudio");
    }
  });
}

function _setCallState(call, roomId, status) {
  console.log("Call state in ".concat(roomId, " changed to ").concat(status, " (").concat(call ? call.call_state : "-", ")"));
  calls[roomId] = call;

  if (status === "ringing") {
    play("ringAudio");
  } else if (call && call.call_state === "ringing") {
    pause("ringAudio");
  }

  if (call) {
    call.call_state = status;
  }

  _dispatcher.default.dispatch({
    action: 'call_state',
    room_id: roomId,
    state: status
  });
}

function _showICEFallbackPrompt() {
  const cli = _MatrixClientPeg.MatrixClientPeg.get();

  const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");

  const code = sub => React.createElement("code", null, sub);

  _Modal.default.createTrackedDialog('No TURN servers', '', QuestionDialog, {
    title: (0, _languageHandler._t)("Call failed due to misconfigured server"),
    description: React.createElement("div", null, React.createElement("p", null, (0, _languageHandler._t)("Please ask the administrator of your homeserver " + "(<code>%(homeserverDomain)s</code>) to configure a TURN server in " + "order for calls to work reliably.", {
      homeserverDomain: cli.getDomain()
    }, {
      code
    })), React.createElement("p", null, (0, _languageHandler._t)("Alternatively, you can try to use the public server at " + "<code>turn.matrix.org</code>, but this will not be as reliable, and " + "it will share your IP address with that server. You can also manage " + "this in Settings.", null, {
      code
    }))),
    button: (0, _languageHandler._t)('Try using turn.matrix.org'),
    cancelButton: (0, _languageHandler._t)('OK'),
    onFinished: allow => {
      _SettingsStore.default.setValue("fallbackICEServerAllowed", null, _SettingsStore.SettingLevel.DEVICE, allow);

      cli.setFallbackICEServerAllowed(allow);
    }
  }, null, true);
}

function _onAction(payload) {
  function placeCall(newCall) {
    _setCallListeners(newCall);

    if (payload.type === 'voice') {
      newCall.placeVoiceCall();
    } else if (payload.type === 'video') {
      newCall.placeVideoCall(payload.remote_element, payload.local_element);
    } else if (payload.type === 'screensharing') {
      const screenCapErrorString = _PlatformPeg.default.get().screenCaptureErrorString();

      if (screenCapErrorString) {
        _setCallState(undefined, newCall.roomId, "ended");

        console.log("Can't capture screen: " + screenCapErrorString);
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Call Handler', 'Unable to capture screen', ErrorDialog, {
          title: (0, _languageHandler._t)('Unable to capture screen'),
          description: screenCapErrorString
        });

        return;
      }

      newCall.placeScreenSharingCall(payload.remote_element, payload.local_element);
    } else {
      console.error("Unknown conf call type: %s", payload.type);
    }
  }

  switch (payload.action) {
    case 'place_call':
      {
        if (callHandler.getAnyActiveCall()) {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Call Handler', 'Existing Call', ErrorDialog, {
            title: (0, _languageHandler._t)('Existing Call'),
            description: (0, _languageHandler._t)('You are already in a call.')
          });

          return; // don't allow >1 call to be placed.
        } // if the runtime env doesn't do VoIP, whine.


        if (!_MatrixClientPeg.MatrixClientPeg.get().supportsVoip()) {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Call Handler', 'VoIP is unsupported', ErrorDialog, {
            title: (0, _languageHandler._t)('VoIP is unsupported'),
            description: (0, _languageHandler._t)('You cannot place VoIP calls in this browser.')
          });

          return;
        }

        const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(payload.room_id);

        if (!room) {
          console.error("Room %s does not exist.", payload.room_id);
          return;
        }

        const members = room.getJoinedMembers();

        if (members.length <= 1) {
          const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

          _Modal.default.createTrackedDialog('Call Handler', 'Cannot place call with self', ErrorDialog, {
            description: (0, _languageHandler._t)('You cannot place a call with yourself.')
          });

          return;
        } else if (members.length === 2) {
          console.info("Place %s call in %s", payload.type, payload.room_id);

          const call = _matrixJsSdk.default.createNewMatrixCall(_MatrixClientPeg.MatrixClientPeg.get(), payload.room_id);

          placeCall(call);
        } else {
          // > 2
          _dispatcher.default.dispatch({
            action: "place_conference_call",
            room_id: payload.room_id,
            type: payload.type,
            remote_element: payload.remote_element,
            local_element: payload.local_element
          });
        }
      }
      break;

    case 'place_conference_call':
      console.info("Place conference call in %s", payload.room_id);

      _startCallApp(payload.room_id, payload.type);

      break;

    case 'incoming_call':
      {
        if (callHandler.getAnyActiveCall()) {
          // ignore multiple incoming calls. in future, we may want a line-1/line-2 setup.
          // we avoid rejecting with "busy" in case the user wants to answer it on a different device.
          // in future we could signal a "local busy" as a warning to the caller.
          // see https://github.com/vector-im/vector-web/issues/1964
          return;
        } // if the runtime env doesn't do VoIP, stop here.


        if (!_MatrixClientPeg.MatrixClientPeg.get().supportsVoip()) {
          return;
        }

        const call = payload.call;

        _setCallListeners(call);

        _setCallState(call, call.roomId, "ringing");
      }
      break;

    case 'hangup':
      if (!calls[payload.room_id]) {
        return; // no call to hangup
      }

      calls[payload.room_id].hangup();

      _setCallState(null, payload.room_id, "ended");

      break;

    case 'answer':
      if (!calls[payload.room_id]) {
        return; // no call to answer
      }

      calls[payload.room_id].answer();

      _setCallState(calls[payload.room_id], payload.room_id, "connected");

      _dispatcher.default.dispatch({
        action: "view_room",
        room_id: payload.room_id
      });

      break;
  }
}

async function _startCallApp(roomId, type) {
  _dispatcher.default.dispatch({
    action: 'appsDrawer',
    show: true
  });

  const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId);

  const currentRoomWidgets = _WidgetUtils.default.getRoomWidgets(room);

  if (_WidgetEchoStore.default.roomHasPendingWidgetsOfType(roomId, currentRoomWidgets, 'jitsi')) {
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

    _Modal.default.createTrackedDialog('Call already in progress', '', ErrorDialog, {
      title: (0, _languageHandler._t)('Call in Progress'),
      description: (0, _languageHandler._t)('A call is currently being placed!')
    });

    return;
  }

  const currentJitsiWidgets = currentRoomWidgets.filter(ev => {
    return ev.getContent().type === 'jitsi';
  });

  if (currentJitsiWidgets.length > 0) {
    console.warn("Refusing to start conference call widget in " + roomId + " a conference call widget is already present");
    const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

    _Modal.default.createTrackedDialog('Already have Jitsi Widget', '', ErrorDialog, {
      title: (0, _languageHandler._t)('Call in Progress'),
      description: (0, _languageHandler._t)('A call is already in progress!')
    });

    return;
  }

  const confId = "JitsiConference".concat((0, _NamingUtils.generateHumanReadableId)());

  const jitsiDomain = _Jitsi.Jitsi.getInstance().preferredDomain;

  let widgetUrl = _WidgetUtils.default.getLocalJitsiWrapperUrl(); // TODO: Remove URL hacks when the mobile clients eventually support v2 widgets


  const parsedUrl = new URL(widgetUrl);
  parsedUrl.search = ''; // set to empty string to make the URL class use searchParams instead

  parsedUrl.searchParams.set('confId', confId);
  widgetUrl = parsedUrl.toString();
  const widgetData = {
    conferenceId: confId,
    isAudioOnly: type === 'voice',
    domain: jitsiDomain
  };
  const widgetId = 'jitsi_' + _MatrixClientPeg.MatrixClientPeg.get().credentials.userId + '_' + Date.now();

  _WidgetUtils.default.setRoomWidget(roomId, widgetId, 'jitsi', widgetUrl, 'Jitsi', widgetData).then(() => {
    console.log('Jitsi widget added');
  }).catch(e => {
    if (e.errcode === 'M_FORBIDDEN') {
      const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

      _Modal.default.createTrackedDialog('Call Failed', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Permission Required'),
        description: (0, _languageHandler._t)("You do not have permission to start a conference call in this room")
      });
    }

    console.error(e);
  });
} // FIXME: Nasty way of making sure we only register
// with the dispatcher once


if (!global.mxCallHandler) {
  _dispatcher.default.register(_onAction); // add empty handlers for media actions, otherwise the media keys
  // end up causing the audio elements with our ring/ringback etc
  // audio clips in to play.


  if (navigator.mediaSession) {
    navigator.mediaSession.setActionHandler('play', function () {});
    navigator.mediaSession.setActionHandler('pause', function () {});
    navigator.mediaSession.setActionHandler('seekbackward', function () {});
    navigator.mediaSession.setActionHandler('seekforward', function () {});
    navigator.mediaSession.setActionHandler('previoustrack', function () {});
    navigator.mediaSession.setActionHandler('nexttrack', function () {});
  }
}

const callHandler = {
  getCallForRoom: function (roomId) {
    let call = callHandler.getCall(roomId);
    if (call) return call;

    if (ConferenceHandler) {
      call = ConferenceHandler.getConferenceCallForRoom(roomId);
    }

    if (call) return call;
    return null;
  },
  getCall: function (roomId) {
    return calls[roomId] || null;
  },
  getAnyActiveCall: function () {
    const roomsWithCalls = Object.keys(calls);

    for (let i = 0; i < roomsWithCalls.length; i++) {
      if (calls[roomsWithCalls[i]] && calls[roomsWithCalls[i]].call_state !== "ended") {
        return calls[roomsWithCalls[i]];
      }
    }

    return null;
  },

  /**
   * The conference handler is a module that deals with implementation-specific
   * multi-party calling implementations. Riot passes in its own which creates
   * a one-to-one call with a freeswitch conference bridge. As of July 2018,
   * the de-facto way of conference calling is a Jitsi widget, so this is
   * deprecated. It reamins here for two reasons:
   *  1. So Riot still supports joining existing freeswitch conference calls
   *     (but doesn't support creating them). After a transition period, we can
   *     remove support for joining them too.
   *  2. To hide the one-to-one rooms that old-style conferencing creates. This
   *     is much harder to remove: probably either we make Riot leave & forget these
   *     rooms after we remove support for joining freeswitch conferences, or we
   *     accept that random rooms with cryptic users will suddently appear for
   *     anyone who's ever used conference calling, or we are stuck with this
   *     code forever.
   *
   * @param {object} confHandler The conference handler object
   */
  setConferenceHandler: function (confHandler) {
    ConferenceHandler = confHandler;
  },
  getConferenceHandler: function () {
    return ConferenceHandler;
  }
}; // Only things in here which actually need to be global are the
// calls list (done separately) and making sure we only register
// with the dispatcher once (which uses this mechanism but checks
// separately). This could be tidied up.

if (global.mxCallHandler === undefined) {
  global.mxCallHandler = callHandler;
}

var _default = global.mxCallHandler;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9DYWxsSGFuZGxlci5qcyJdLCJuYW1lcyI6WyJnbG9iYWwiLCJteENhbGxzIiwiY2FsbHMiLCJDb25mZXJlbmNlSGFuZGxlciIsImF1ZGlvUHJvbWlzZXMiLCJwbGF5IiwiYXVkaW9JZCIsImF1ZGlvIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInBsYXlBdWRpbyIsImUiLCJjb25zb2xlIiwibG9nIiwidGhlbiIsImxvYWQiLCJwYXVzZSIsIl9yZUF0dGVtcHRDYWxsIiwiY2FsbCIsImRpcmVjdGlvbiIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwicm9vbV9pZCIsInJvb21JZCIsInR5cGUiLCJhbnN3ZXIiLCJfc2V0Q2FsbExpc3RlbmVycyIsIm9uIiwiZXJyIiwiZXJyb3IiLCJjb2RlIiwiUXVlc3Rpb25EaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiYnV0dG9uIiwib25GaW5pc2hlZCIsImNvbmZpcm1lZCIsInJvb20iLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJnZXRSb29tIiwiZ2V0VHVyblNlcnZlcnMiLCJsZW5ndGgiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWUiLCJfc2hvd0lDRUZhbGxiYWNrUHJvbXB0IiwiRXJyb3JEaWFsb2ciLCJtZXNzYWdlIiwiX3NldENhbGxTdGF0ZSIsInVuZGVmaW5lZCIsIm5ld1N0YXRlIiwib2xkU3RhdGUiLCJoYW5ndXBQYXJ0eSIsImhhbmd1cFJlYXNvbiIsInN0YXR1cyIsImNhbGxfc3RhdGUiLCJzdGF0ZSIsImNsaSIsInN1YiIsImhvbWVzZXJ2ZXJEb21haW4iLCJnZXREb21haW4iLCJjYW5jZWxCdXR0b24iLCJhbGxvdyIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIiwic2V0RmFsbGJhY2tJQ0VTZXJ2ZXJBbGxvd2VkIiwiX29uQWN0aW9uIiwicGF5bG9hZCIsInBsYWNlQ2FsbCIsIm5ld0NhbGwiLCJwbGFjZVZvaWNlQ2FsbCIsInBsYWNlVmlkZW9DYWxsIiwicmVtb3RlX2VsZW1lbnQiLCJsb2NhbF9lbGVtZW50Iiwic2NyZWVuQ2FwRXJyb3JTdHJpbmciLCJQbGF0Zm9ybVBlZyIsInNjcmVlbkNhcHR1cmVFcnJvclN0cmluZyIsInBsYWNlU2NyZWVuU2hhcmluZ0NhbGwiLCJjYWxsSGFuZGxlciIsImdldEFueUFjdGl2ZUNhbGwiLCJzdXBwb3J0c1ZvaXAiLCJtZW1iZXJzIiwiZ2V0Sm9pbmVkTWVtYmVycyIsImluZm8iLCJNYXRyaXgiLCJjcmVhdGVOZXdNYXRyaXhDYWxsIiwiX3N0YXJ0Q2FsbEFwcCIsImhhbmd1cCIsInNob3ciLCJjdXJyZW50Um9vbVdpZGdldHMiLCJXaWRnZXRVdGlscyIsImdldFJvb21XaWRnZXRzIiwiV2lkZ2V0RWNob1N0b3JlIiwicm9vbUhhc1BlbmRpbmdXaWRnZXRzT2ZUeXBlIiwiY3VycmVudEppdHNpV2lkZ2V0cyIsImZpbHRlciIsImV2IiwiZ2V0Q29udGVudCIsIndhcm4iLCJjb25mSWQiLCJqaXRzaURvbWFpbiIsIkppdHNpIiwiZ2V0SW5zdGFuY2UiLCJwcmVmZXJyZWREb21haW4iLCJ3aWRnZXRVcmwiLCJnZXRMb2NhbEppdHNpV3JhcHBlclVybCIsInBhcnNlZFVybCIsIlVSTCIsInNlYXJjaCIsInNlYXJjaFBhcmFtcyIsInNldCIsInRvU3RyaW5nIiwid2lkZ2V0RGF0YSIsImNvbmZlcmVuY2VJZCIsImlzQXVkaW9Pbmx5IiwiZG9tYWluIiwid2lkZ2V0SWQiLCJjcmVkZW50aWFscyIsInVzZXJJZCIsIkRhdGUiLCJub3ciLCJzZXRSb29tV2lkZ2V0IiwiY2F0Y2giLCJlcnJjb2RlIiwibXhDYWxsSGFuZGxlciIsInJlZ2lzdGVyIiwibmF2aWdhdG9yIiwibWVkaWFTZXNzaW9uIiwic2V0QWN0aW9uSGFuZGxlciIsImdldENhbGxGb3JSb29tIiwiZ2V0Q2FsbCIsImdldENvbmZlcmVuY2VDYWxsRm9yUm9vbSIsInJvb21zV2l0aENhbGxzIiwiT2JqZWN0Iiwia2V5cyIsImkiLCJzZXRDb25mZXJlbmNlSGFuZGxlciIsImNvbmZIYW5kbGVyIiwiZ2V0Q29uZmVyZW5jZUhhbmRsZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBdURBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQW5FQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtREFBLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQixDQUNiO0FBRGEsQ0FBakI7QUFHQSxNQUFNQyxLQUFLLEdBQUdGLE1BQU0sQ0FBQ0MsT0FBckI7QUFDQSxJQUFJRSxpQkFBaUIsR0FBRyxJQUF4QjtBQUVBLE1BQU1DLGFBQWEsR0FBRyxFQUF0Qjs7QUFFQSxTQUFTQyxJQUFULENBQWNDLE9BQWQsRUFBdUI7QUFDbkI7QUFDQTtBQUNBLFFBQU1DLEtBQUssR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCSCxPQUF4QixDQUFkOztBQUNBLE1BQUlDLEtBQUosRUFBVztBQUNQLFVBQU1HLFNBQVMsR0FBRyxZQUFZO0FBQzFCLFVBQUk7QUFDQTtBQUNBO0FBQ0EsY0FBTUgsS0FBSyxDQUFDRixJQUFOLEVBQU47QUFDSCxPQUpELENBSUUsT0FBT00sQ0FBUCxFQUFVO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksMkJBQVosRUFBeUNGLENBQXpDO0FBQ0g7QUFDSixLQVpEOztBQWFBLFFBQUlQLGFBQWEsQ0FBQ0UsT0FBRCxDQUFqQixFQUE0QjtBQUN4QkYsTUFBQUEsYUFBYSxDQUFDRSxPQUFELENBQWIsR0FBeUJGLGFBQWEsQ0FBQ0UsT0FBRCxDQUFiLENBQXVCUSxJQUF2QixDQUE0QixNQUFJO0FBQ3JEUCxRQUFBQSxLQUFLLENBQUNRLElBQU47QUFDQSxlQUFPTCxTQUFTLEVBQWhCO0FBQ0gsT0FId0IsQ0FBekI7QUFJSCxLQUxELE1BS087QUFDSE4sTUFBQUEsYUFBYSxDQUFDRSxPQUFELENBQWIsR0FBeUJJLFNBQVMsRUFBbEM7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsU0FBU00sS0FBVCxDQUFlVixPQUFmLEVBQXdCO0FBQ3BCO0FBQ0E7QUFDQSxRQUFNQyxLQUFLLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QkgsT0FBeEIsQ0FBZDs7QUFDQSxNQUFJQyxLQUFKLEVBQVc7QUFDUCxRQUFJSCxhQUFhLENBQUNFLE9BQUQsQ0FBakIsRUFBNEI7QUFDeEJGLE1BQUFBLGFBQWEsQ0FBQ0UsT0FBRCxDQUFiLEdBQXlCRixhQUFhLENBQUNFLE9BQUQsQ0FBYixDQUF1QlEsSUFBdkIsQ0FBNEIsTUFBSVAsS0FBSyxDQUFDUyxLQUFOLEVBQWhDLENBQXpCO0FBQ0gsS0FGRCxNQUVPO0FBQ0g7QUFDQVosTUFBQUEsYUFBYSxDQUFDRSxPQUFELENBQWIsR0FBeUJDLEtBQUssQ0FBQ1MsS0FBTixFQUF6QjtBQUNIO0FBQ0o7QUFDSjs7QUFFRCxTQUFTQyxjQUFULENBQXdCQyxJQUF4QixFQUE4QjtBQUMxQixNQUFJQSxJQUFJLENBQUNDLFNBQUwsS0FBbUIsVUFBdkIsRUFBbUM7QUFDL0JDLHdCQUFJQyxRQUFKLENBQWE7QUFDVEMsTUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVEMsTUFBQUEsT0FBTyxFQUFFTCxJQUFJLENBQUNNLE1BRkw7QUFHVEMsTUFBQUEsSUFBSSxFQUFFUCxJQUFJLENBQUNPO0FBSEYsS0FBYjtBQUtILEdBTkQsTUFNTztBQUNIUCxJQUFBQSxJQUFJLENBQUNRLE1BQUw7QUFDSDtBQUNKOztBQUVELFNBQVNDLGlCQUFULENBQTJCVCxJQUEzQixFQUFpQztBQUM3QkEsRUFBQUEsSUFBSSxDQUFDVSxFQUFMLENBQVEsT0FBUixFQUFpQixVQUFTQyxHQUFULEVBQWM7QUFDM0JqQixJQUFBQSxPQUFPLENBQUNrQixLQUFSLENBQWMsYUFBZCxFQUE2QkQsR0FBN0I7O0FBQ0EsUUFBSUEsR0FBRyxDQUFDRSxJQUFKLEtBQWEsaUJBQWpCLEVBQW9DO0FBQ2hDLFlBQU1DLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFFQUMscUJBQU1DLG1CQUFOLENBQTBCLGFBQTFCLEVBQXlDLEVBQXpDLEVBQTZDSixjQUE3QyxFQUE2RDtBQUN6REssUUFBQUEsS0FBSyxFQUFFLHlCQUFHLGFBQUgsQ0FEa0Q7QUFFekRDLFFBQUFBLFdBQVcsRUFBRSx5QkFDVCw4Q0FDQSxvREFEQSxHQUVBLGlEQUhTLENBRjRDO0FBT3pEQyxRQUFBQSxNQUFNLEVBQUUseUJBQUcsaUJBQUgsQ0FQaUQ7QUFRekRDLFFBQUFBLFVBQVUsRUFBRSxVQUFTQyxTQUFULEVBQW9CO0FBQzVCLGNBQUlBLFNBQUosRUFBZTtBQUNYLGtCQUFNQyxJQUFJLEdBQUdDLGlDQUFnQkMsR0FBaEIsR0FBc0JDLE9BQXRCLENBQThCM0IsSUFBSSxDQUFDTSxNQUFuQyxDQUFiOztBQUNBLGdFQUNJbUIsaUNBQWdCQyxHQUFoQixFQURKLEVBRUlGLElBRkosRUFHSSxNQUFNO0FBQ0Z6QixjQUFBQSxjQUFjLENBQUNDLElBQUQsQ0FBZDtBQUNILGFBTEwsRUFNSUEsSUFBSSxDQUFDQyxTQUFMLEtBQW1CLFVBQW5CLEdBQWdDLHlCQUFHLGFBQUgsQ0FBaEMsR0FBb0QseUJBQUcsZUFBSCxDQU54RCxFQU9JRCxJQUFJLENBQUNDLFNBQUwsS0FBbUIsVUFBbkIsR0FBZ0MseUJBQUcsTUFBSCxDQUFoQyxHQUE2Qyx5QkFBRyxRQUFILENBUGpEO0FBU0g7QUFDSjtBQXJCd0QsT0FBN0Q7QUF1QkgsS0ExQkQsTUEwQk87QUFDSCxVQUNJd0IsaUNBQWdCQyxHQUFoQixHQUFzQkUsY0FBdEIsR0FBdUNDLE1BQXZDLEtBQWtELENBQWxELElBQ0FDLHVCQUFjQyxRQUFkLENBQXVCLDBCQUF2QixNQUF1RCxJQUYzRCxFQUdFO0FBQ0VDLFFBQUFBLHNCQUFzQjs7QUFDdEI7QUFDSDs7QUFFRCxZQUFNQyxXQUFXLEdBQUdsQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsYUFBMUIsRUFBeUMsRUFBekMsRUFBNkNlLFdBQTdDLEVBQTBEO0FBQ3REZCxRQUFBQSxLQUFLLEVBQUUseUJBQUcsYUFBSCxDQUQrQztBQUV0REMsUUFBQUEsV0FBVyxFQUFFVCxHQUFHLENBQUN1QjtBQUZxQyxPQUExRDtBQUlIO0FBQ0osR0EzQ0Q7QUE0Q0FsQyxFQUFBQSxJQUFJLENBQUNVLEVBQUwsQ0FBUSxRQUFSLEVBQWtCLFlBQVc7QUFDekJ5QixJQUFBQSxhQUFhLENBQUNDLFNBQUQsRUFBWXBDLElBQUksQ0FBQ00sTUFBakIsRUFBeUIsT0FBekIsQ0FBYjtBQUNILEdBRkQsRUE3QzZCLENBZ0Q3QjtBQUNBOztBQUNBTixFQUFBQSxJQUFJLENBQUNVLEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFVBQVMyQixRQUFULEVBQW1CQyxRQUFuQixFQUE2QjtBQUMxQyxRQUFJRCxRQUFRLEtBQUssU0FBakIsRUFBNEI7QUFDeEJGLE1BQUFBLGFBQWEsQ0FBQ25DLElBQUQsRUFBT0EsSUFBSSxDQUFDTSxNQUFaLEVBQW9CLFNBQXBCLENBQWI7O0FBQ0FSLE1BQUFBLEtBQUssQ0FBQyxlQUFELENBQUw7QUFDSCxLQUhELE1BR08sSUFBSXVDLFFBQVEsS0FBSyxhQUFqQixFQUFnQztBQUNuQ0YsTUFBQUEsYUFBYSxDQUFDbkMsSUFBRCxFQUFPQSxJQUFJLENBQUNNLE1BQVosRUFBb0IsVUFBcEIsQ0FBYjs7QUFDQW5CLE1BQUFBLElBQUksQ0FBQyxlQUFELENBQUo7QUFDSCxLQUhNLE1BR0EsSUFBSWtELFFBQVEsS0FBSyxPQUFiLElBQXdCQyxRQUFRLEtBQUssV0FBekMsRUFBc0Q7QUFDekRILE1BQUFBLGFBQWEsQ0FBQ0MsU0FBRCxFQUFZcEMsSUFBSSxDQUFDTSxNQUFqQixFQUF5QixPQUF6QixDQUFiOztBQUNBUixNQUFBQSxLQUFLLENBQUMsZUFBRCxDQUFMO0FBQ0FYLE1BQUFBLElBQUksQ0FBQyxjQUFELENBQUo7QUFDSCxLQUpNLE1BSUEsSUFBSWtELFFBQVEsS0FBSyxPQUFiLElBQXdCQyxRQUFRLEtBQUssYUFBckMsS0FDRnRDLElBQUksQ0FBQ3VDLFdBQUwsS0FBcUIsUUFBckIsSUFDQXZDLElBQUksQ0FBQ3VDLFdBQUwsS0FBcUIsT0FBckIsSUFBZ0N2QyxJQUFJLENBQUN3QyxZQUFMLEtBQXNCLGdCQUZwRCxDQUFKLEVBR0k7QUFDUEwsTUFBQUEsYUFBYSxDQUFDbkMsSUFBRCxFQUFPQSxJQUFJLENBQUNNLE1BQVosRUFBb0IsTUFBcEIsQ0FBYjs7QUFDQVIsTUFBQUEsS0FBSyxDQUFDLGVBQUQsQ0FBTDtBQUNBWCxNQUFBQSxJQUFJLENBQUMsV0FBRCxDQUFKO0FBQ0EsWUFBTThDLFdBQVcsR0FBR2xCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQixjQUExQixFQUEwQyxjQUExQyxFQUEwRGUsV0FBMUQsRUFBdUU7QUFDbkVkLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxjQUFILENBRDREO0FBRW5FQyxRQUFBQSxXQUFXLEVBQUUseUJBQUcsbUNBQUgsSUFBMEM7QUFGWSxPQUF2RTtBQUlILEtBWk0sTUFZQSxJQUFJa0IsUUFBUSxLQUFLLGFBQWpCLEVBQWdDO0FBQ25DSCxNQUFBQSxhQUFhLENBQUNuQyxJQUFELEVBQU9BLElBQUksQ0FBQ00sTUFBWixFQUFvQixlQUFwQixDQUFiOztBQUNBUixNQUFBQSxLQUFLLENBQUMsZUFBRCxDQUFMO0FBQ0gsS0FITSxNQUdBLElBQUl3QyxRQUFRLEtBQUssU0FBakIsRUFBNEI7QUFDL0JILE1BQUFBLGFBQWEsQ0FBQ25DLElBQUQsRUFBT0EsSUFBSSxDQUFDTSxNQUFaLEVBQW9CLGNBQXBCLENBQWI7O0FBQ0FSLE1BQUFBLEtBQUssQ0FBQyxlQUFELENBQUw7QUFDSCxLQUhNLE1BR0EsSUFBSXVDLFFBQVEsS0FBSyxXQUFqQixFQUE4QjtBQUNqQ0YsTUFBQUEsYUFBYSxDQUFDbkMsSUFBRCxFQUFPQSxJQUFJLENBQUNNLE1BQVosRUFBb0IsV0FBcEIsQ0FBYjs7QUFDQVIsTUFBQUEsS0FBSyxDQUFDLGVBQUQsQ0FBTDtBQUNIO0FBQ0osR0FqQ0Q7QUFrQ0g7O0FBRUQsU0FBU3FDLGFBQVQsQ0FBdUJuQyxJQUF2QixFQUE2Qk0sTUFBN0IsRUFBcUNtQyxNQUFyQyxFQUE2QztBQUN6Qy9DLEVBQUFBLE9BQU8sQ0FBQ0MsR0FBUix5QkFDcUJXLE1BRHJCLHlCQUMwQ21DLE1BRDFDLGVBQ3FEekMsSUFBSSxHQUFHQSxJQUFJLENBQUMwQyxVQUFSLEdBQXFCLEdBRDlFO0FBR0ExRCxFQUFBQSxLQUFLLENBQUNzQixNQUFELENBQUwsR0FBZ0JOLElBQWhCOztBQUVBLE1BQUl5QyxNQUFNLEtBQUssU0FBZixFQUEwQjtBQUN0QnRELElBQUFBLElBQUksQ0FBQyxXQUFELENBQUo7QUFDSCxHQUZELE1BRU8sSUFBSWEsSUFBSSxJQUFJQSxJQUFJLENBQUMwQyxVQUFMLEtBQW9CLFNBQWhDLEVBQTJDO0FBQzlDNUMsSUFBQUEsS0FBSyxDQUFDLFdBQUQsQ0FBTDtBQUNIOztBQUVELE1BQUlFLElBQUosRUFBVTtBQUNOQSxJQUFBQSxJQUFJLENBQUMwQyxVQUFMLEdBQWtCRCxNQUFsQjtBQUNIOztBQUNEdkMsc0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxJQUFBQSxNQUFNLEVBQUUsWUFEQztBQUVUQyxJQUFBQSxPQUFPLEVBQUVDLE1BRkE7QUFHVHFDLElBQUFBLEtBQUssRUFBRUY7QUFIRSxHQUFiO0FBS0g7O0FBRUQsU0FBU1Qsc0JBQVQsR0FBa0M7QUFDOUIsUUFBTVksR0FBRyxHQUFHbkIsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFFBQU1aLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2Qjs7QUFDQSxRQUFNSCxJQUFJLEdBQUdnQyxHQUFHLElBQUksa0NBQU9BLEdBQVAsQ0FBcEI7O0FBQ0E1QixpQkFBTUMsbUJBQU4sQ0FBMEIsaUJBQTFCLEVBQTZDLEVBQTdDLEVBQWlESixjQUFqRCxFQUFpRTtBQUM3REssSUFBQUEsS0FBSyxFQUFFLHlCQUFHLHlDQUFILENBRHNEO0FBRTdEQyxJQUFBQSxXQUFXLEVBQUUsaUNBQ1QsK0JBQUkseUJBQ0EscURBQ0Esb0VBREEsR0FFQSxtQ0FIQSxFQUlBO0FBQUUwQixNQUFBQSxnQkFBZ0IsRUFBRUYsR0FBRyxDQUFDRyxTQUFKO0FBQXBCLEtBSkEsRUFJdUM7QUFBRWxDLE1BQUFBO0FBQUYsS0FKdkMsQ0FBSixDQURTLEVBT1QsK0JBQUkseUJBQ0EsNERBQ0Esc0VBREEsR0FFQSxzRUFGQSxHQUdBLG1CQUpBLEVBS0EsSUFMQSxFQUtNO0FBQUVBLE1BQUFBO0FBQUYsS0FMTixDQUFKLENBUFMsQ0FGZ0Q7QUFpQjdEUSxJQUFBQSxNQUFNLEVBQUUseUJBQUcsMkJBQUgsQ0FqQnFEO0FBa0I3RDJCLElBQUFBLFlBQVksRUFBRSx5QkFBRyxJQUFILENBbEIrQztBQW1CN0QxQixJQUFBQSxVQUFVLEVBQUcyQixLQUFELElBQVc7QUFDbkJuQiw2QkFBY29CLFFBQWQsQ0FBdUIsMEJBQXZCLEVBQW1ELElBQW5ELEVBQXlEQyw0QkFBYUMsTUFBdEUsRUFBOEVILEtBQTlFOztBQUNBTCxNQUFBQSxHQUFHLENBQUNTLDJCQUFKLENBQWdDSixLQUFoQztBQUNIO0FBdEI0RCxHQUFqRSxFQXVCRyxJQXZCSCxFQXVCUyxJQXZCVDtBQXdCSDs7QUFFRCxTQUFTSyxTQUFULENBQW1CQyxPQUFuQixFQUE0QjtBQUN4QixXQUFTQyxTQUFULENBQW1CQyxPQUFuQixFQUE0QjtBQUN4QmhELElBQUFBLGlCQUFpQixDQUFDZ0QsT0FBRCxDQUFqQjs7QUFDQSxRQUFJRixPQUFPLENBQUNoRCxJQUFSLEtBQWlCLE9BQXJCLEVBQThCO0FBQzFCa0QsTUFBQUEsT0FBTyxDQUFDQyxjQUFSO0FBQ0gsS0FGRCxNQUVPLElBQUlILE9BQU8sQ0FBQ2hELElBQVIsS0FBaUIsT0FBckIsRUFBOEI7QUFDakNrRCxNQUFBQSxPQUFPLENBQUNFLGNBQVIsQ0FDSUosT0FBTyxDQUFDSyxjQURaLEVBRUlMLE9BQU8sQ0FBQ00sYUFGWjtBQUlILEtBTE0sTUFLQSxJQUFJTixPQUFPLENBQUNoRCxJQUFSLEtBQWlCLGVBQXJCLEVBQXNDO0FBQ3pDLFlBQU11RCxvQkFBb0IsR0FBR0MscUJBQVlyQyxHQUFaLEdBQWtCc0Msd0JBQWxCLEVBQTdCOztBQUNBLFVBQUlGLG9CQUFKLEVBQTBCO0FBQ3RCM0IsUUFBQUEsYUFBYSxDQUFDQyxTQUFELEVBQVlxQixPQUFPLENBQUNuRCxNQUFwQixFQUE0QixPQUE1QixDQUFiOztBQUNBWixRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSwyQkFBMkJtRSxvQkFBdkM7QUFDQSxjQUFNN0IsV0FBVyxHQUFHbEIsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjs7QUFDQUMsdUJBQU1DLG1CQUFOLENBQTBCLGNBQTFCLEVBQTBDLDBCQUExQyxFQUFzRWUsV0FBdEUsRUFBbUY7QUFDL0VkLFVBQUFBLEtBQUssRUFBRSx5QkFBRywwQkFBSCxDQUR3RTtBQUUvRUMsVUFBQUEsV0FBVyxFQUFFMEM7QUFGa0UsU0FBbkY7O0FBSUE7QUFDSDs7QUFDREwsTUFBQUEsT0FBTyxDQUFDUSxzQkFBUixDQUNJVixPQUFPLENBQUNLLGNBRFosRUFFSUwsT0FBTyxDQUFDTSxhQUZaO0FBSUgsS0FoQk0sTUFnQkE7QUFDSG5FLE1BQUFBLE9BQU8sQ0FBQ2tCLEtBQVIsQ0FBYyw0QkFBZCxFQUE0QzJDLE9BQU8sQ0FBQ2hELElBQXBEO0FBQ0g7QUFDSjs7QUFFRCxVQUFRZ0QsT0FBTyxDQUFDbkQsTUFBaEI7QUFDSSxTQUFLLFlBQUw7QUFDSTtBQUNJLFlBQUk4RCxXQUFXLENBQUNDLGdCQUFaLEVBQUosRUFBb0M7QUFDaEMsZ0JBQU1sQyxXQUFXLEdBQUdsQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsY0FBMUIsRUFBMEMsZUFBMUMsRUFBMkRlLFdBQTNELEVBQXdFO0FBQ3BFZCxZQUFBQSxLQUFLLEVBQUUseUJBQUcsZUFBSCxDQUQ2RDtBQUVwRUMsWUFBQUEsV0FBVyxFQUFFLHlCQUFHLDRCQUFIO0FBRnVELFdBQXhFOztBQUlBLGlCQU5nQyxDQU14QjtBQUNYLFNBUkwsQ0FVSTs7O0FBQ0EsWUFBSSxDQUFDSyxpQ0FBZ0JDLEdBQWhCLEdBQXNCMEMsWUFBdEIsRUFBTCxFQUEyQztBQUN2QyxnQkFBTW5DLFdBQVcsR0FBR2xCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHlCQUFNQyxtQkFBTixDQUEwQixjQUExQixFQUEwQyxxQkFBMUMsRUFBaUVlLFdBQWpFLEVBQThFO0FBQzFFZCxZQUFBQSxLQUFLLEVBQUUseUJBQUcscUJBQUgsQ0FEbUU7QUFFMUVDLFlBQUFBLFdBQVcsRUFBRSx5QkFBRyw4Q0FBSDtBQUY2RCxXQUE5RTs7QUFJQTtBQUNIOztBQUVELGNBQU1JLElBQUksR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEI0QixPQUFPLENBQUNsRCxPQUF0QyxDQUFiOztBQUNBLFlBQUksQ0FBQ21CLElBQUwsRUFBVztBQUNQOUIsVUFBQUEsT0FBTyxDQUFDa0IsS0FBUixDQUFjLHlCQUFkLEVBQXlDMkMsT0FBTyxDQUFDbEQsT0FBakQ7QUFDQTtBQUNIOztBQUVELGNBQU1nRSxPQUFPLEdBQUc3QyxJQUFJLENBQUM4QyxnQkFBTCxFQUFoQjs7QUFDQSxZQUFJRCxPQUFPLENBQUN4QyxNQUFSLElBQWtCLENBQXRCLEVBQXlCO0FBQ3JCLGdCQUFNSSxXQUFXLEdBQUdsQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsY0FBMUIsRUFBMEMsNkJBQTFDLEVBQXlFZSxXQUF6RSxFQUFzRjtBQUNsRmIsWUFBQUEsV0FBVyxFQUFFLHlCQUFHLHdDQUFIO0FBRHFFLFdBQXRGOztBQUdBO0FBQ0gsU0FORCxNQU1PLElBQUlpRCxPQUFPLENBQUN4QyxNQUFSLEtBQW1CLENBQXZCLEVBQTBCO0FBQzdCbkMsVUFBQUEsT0FBTyxDQUFDNkUsSUFBUixDQUFhLHFCQUFiLEVBQW9DaEIsT0FBTyxDQUFDaEQsSUFBNUMsRUFBa0RnRCxPQUFPLENBQUNsRCxPQUExRDs7QUFDQSxnQkFBTUwsSUFBSSxHQUFHd0UscUJBQU9DLG1CQUFQLENBQTJCaEQsaUNBQWdCQyxHQUFoQixFQUEzQixFQUFrRDZCLE9BQU8sQ0FBQ2xELE9BQTFELENBQWI7O0FBQ0FtRCxVQUFBQSxTQUFTLENBQUN4RCxJQUFELENBQVQ7QUFDSCxTQUpNLE1BSUE7QUFBRTtBQUNMRSw4QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFlBQUFBLE1BQU0sRUFBRSx1QkFEQztBQUVUQyxZQUFBQSxPQUFPLEVBQUVrRCxPQUFPLENBQUNsRCxPQUZSO0FBR1RFLFlBQUFBLElBQUksRUFBRWdELE9BQU8sQ0FBQ2hELElBSEw7QUFJVHFELFlBQUFBLGNBQWMsRUFBRUwsT0FBTyxDQUFDSyxjQUpmO0FBS1RDLFlBQUFBLGFBQWEsRUFBRU4sT0FBTyxDQUFDTTtBQUxkLFdBQWI7QUFPSDtBQUNKO0FBQ0Q7O0FBQ0osU0FBSyx1QkFBTDtBQUNJbkUsTUFBQUEsT0FBTyxDQUFDNkUsSUFBUixDQUFhLDZCQUFiLEVBQTRDaEIsT0FBTyxDQUFDbEQsT0FBcEQ7O0FBQ0FxRSxNQUFBQSxhQUFhLENBQUNuQixPQUFPLENBQUNsRCxPQUFULEVBQWtCa0QsT0FBTyxDQUFDaEQsSUFBMUIsQ0FBYjs7QUFDQTs7QUFDSixTQUFLLGVBQUw7QUFDSTtBQUNJLFlBQUkyRCxXQUFXLENBQUNDLGdCQUFaLEVBQUosRUFBb0M7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNILFNBUEwsQ0FTSTs7O0FBQ0EsWUFBSSxDQUFDMUMsaUNBQWdCQyxHQUFoQixHQUFzQjBDLFlBQXRCLEVBQUwsRUFBMkM7QUFDdkM7QUFDSDs7QUFFRCxjQUFNcEUsSUFBSSxHQUFHdUQsT0FBTyxDQUFDdkQsSUFBckI7O0FBQ0FTLFFBQUFBLGlCQUFpQixDQUFDVCxJQUFELENBQWpCOztBQUNBbUMsUUFBQUEsYUFBYSxDQUFDbkMsSUFBRCxFQUFPQSxJQUFJLENBQUNNLE1BQVosRUFBb0IsU0FBcEIsQ0FBYjtBQUNIO0FBQ0Q7O0FBQ0osU0FBSyxRQUFMO0FBQ0ksVUFBSSxDQUFDdEIsS0FBSyxDQUFDdUUsT0FBTyxDQUFDbEQsT0FBVCxDQUFWLEVBQTZCO0FBQ3pCLGVBRHlCLENBQ2pCO0FBQ1g7O0FBQ0RyQixNQUFBQSxLQUFLLENBQUN1RSxPQUFPLENBQUNsRCxPQUFULENBQUwsQ0FBdUJzRSxNQUF2Qjs7QUFDQXhDLE1BQUFBLGFBQWEsQ0FBQyxJQUFELEVBQU9vQixPQUFPLENBQUNsRCxPQUFmLEVBQXdCLE9BQXhCLENBQWI7O0FBQ0E7O0FBQ0osU0FBSyxRQUFMO0FBQ0ksVUFBSSxDQUFDckIsS0FBSyxDQUFDdUUsT0FBTyxDQUFDbEQsT0FBVCxDQUFWLEVBQTZCO0FBQ3pCLGVBRHlCLENBQ2pCO0FBQ1g7O0FBQ0RyQixNQUFBQSxLQUFLLENBQUN1RSxPQUFPLENBQUNsRCxPQUFULENBQUwsQ0FBdUJHLE1BQXZCOztBQUNBMkIsTUFBQUEsYUFBYSxDQUFDbkQsS0FBSyxDQUFDdUUsT0FBTyxDQUFDbEQsT0FBVCxDQUFOLEVBQXlCa0QsT0FBTyxDQUFDbEQsT0FBakMsRUFBMEMsV0FBMUMsQ0FBYjs7QUFDQUgsMEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxRQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxRQUFBQSxPQUFPLEVBQUVrRCxPQUFPLENBQUNsRDtBQUZSLE9BQWI7O0FBSUE7QUEzRlI7QUE2Rkg7O0FBRUQsZUFBZXFFLGFBQWYsQ0FBNkJwRSxNQUE3QixFQUFxQ0MsSUFBckMsRUFBMkM7QUFDdkNMLHNCQUFJQyxRQUFKLENBQWE7QUFDVEMsSUFBQUEsTUFBTSxFQUFFLFlBREM7QUFFVHdFLElBQUFBLElBQUksRUFBRTtBQUZHLEdBQWI7O0FBS0EsUUFBTXBELElBQUksR0FBR0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsT0FBdEIsQ0FBOEJyQixNQUE5QixDQUFiOztBQUNBLFFBQU11RSxrQkFBa0IsR0FBR0MscUJBQVlDLGNBQVosQ0FBMkJ2RCxJQUEzQixDQUEzQjs7QUFFQSxNQUFJd0QseUJBQWdCQywyQkFBaEIsQ0FBNEMzRSxNQUE1QyxFQUFvRHVFLGtCQUFwRCxFQUF3RSxPQUF4RSxDQUFKLEVBQXNGO0FBQ2xGLFVBQU01QyxXQUFXLEdBQUdsQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUVBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsMEJBQTFCLEVBQXNELEVBQXRELEVBQTBEZSxXQUExRCxFQUF1RTtBQUNuRWQsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFILENBRDREO0FBRW5FQyxNQUFBQSxXQUFXLEVBQUUseUJBQUcsbUNBQUg7QUFGc0QsS0FBdkU7O0FBSUE7QUFDSDs7QUFFRCxRQUFNOEQsbUJBQW1CLEdBQUdMLGtCQUFrQixDQUFDTSxNQUFuQixDQUEyQkMsRUFBRCxJQUFRO0FBQzFELFdBQU9BLEVBQUUsQ0FBQ0MsVUFBSCxHQUFnQjlFLElBQWhCLEtBQXlCLE9BQWhDO0FBQ0gsR0FGMkIsQ0FBNUI7O0FBR0EsTUFBSTJFLG1CQUFtQixDQUFDckQsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDaENuQyxJQUFBQSxPQUFPLENBQUM0RixJQUFSLENBQ0ksaURBQWlEaEYsTUFBakQsR0FDQSw4Q0FGSjtBQUlBLFVBQU0yQixXQUFXLEdBQUdsQixHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUVBQyxtQkFBTUMsbUJBQU4sQ0FBMEIsMkJBQTFCLEVBQXVELEVBQXZELEVBQTJEZSxXQUEzRCxFQUF3RTtBQUNwRWQsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLGtCQUFILENBRDZEO0FBRXBFQyxNQUFBQSxXQUFXLEVBQUUseUJBQUcsZ0NBQUg7QUFGdUQsS0FBeEU7O0FBSUE7QUFDSDs7QUFFRCxRQUFNbUUsTUFBTSw0QkFBcUIsMkNBQXJCLENBQVo7O0FBQ0EsUUFBTUMsV0FBVyxHQUFHQyxhQUFNQyxXQUFOLEdBQW9CQyxlQUF4Qzs7QUFFQSxNQUFJQyxTQUFTLEdBQUdkLHFCQUFZZSx1QkFBWixFQUFoQixDQXZDdUMsQ0F5Q3ZDOzs7QUFDQSxRQUFNQyxTQUFTLEdBQUcsSUFBSUMsR0FBSixDQUFRSCxTQUFSLENBQWxCO0FBQ0FFLEVBQUFBLFNBQVMsQ0FBQ0UsTUFBVixHQUFtQixFQUFuQixDQTNDdUMsQ0EyQ2hCOztBQUN2QkYsRUFBQUEsU0FBUyxDQUFDRyxZQUFWLENBQXVCQyxHQUF2QixDQUEyQixRQUEzQixFQUFxQ1gsTUFBckM7QUFDQUssRUFBQUEsU0FBUyxHQUFHRSxTQUFTLENBQUNLLFFBQVYsRUFBWjtBQUVBLFFBQU1DLFVBQVUsR0FBRztBQUNmQyxJQUFBQSxZQUFZLEVBQUVkLE1BREM7QUFFZmUsSUFBQUEsV0FBVyxFQUFFL0YsSUFBSSxLQUFLLE9BRlA7QUFHZmdHLElBQUFBLE1BQU0sRUFBRWY7QUFITyxHQUFuQjtBQU1BLFFBQU1nQixRQUFRLEdBQ1YsV0FDQS9FLGlDQUFnQkMsR0FBaEIsR0FBc0IrRSxXQUF0QixDQUFrQ0MsTUFEbEMsR0FFQSxHQUZBLEdBR0FDLElBQUksQ0FBQ0MsR0FBTCxFQUpKOztBQU9BOUIsdUJBQVkrQixhQUFaLENBQTBCdkcsTUFBMUIsRUFBa0NrRyxRQUFsQyxFQUE0QyxPQUE1QyxFQUFxRFosU0FBckQsRUFBZ0UsT0FBaEUsRUFBeUVRLFVBQXpFLEVBQXFGeEcsSUFBckYsQ0FBMEYsTUFBTTtBQUM1RkYsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksb0JBQVo7QUFDSCxHQUZELEVBRUdtSCxLQUZILENBRVVySCxDQUFELElBQU87QUFDWixRQUFJQSxDQUFDLENBQUNzSCxPQUFGLEtBQWMsYUFBbEIsRUFBaUM7QUFDN0IsWUFBTTlFLFdBQVcsR0FBR2xCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBRUFDLHFCQUFNQyxtQkFBTixDQUEwQixhQUExQixFQUF5QyxFQUF6QyxFQUE2Q2UsV0FBN0MsRUFBMEQ7QUFDdERkLFFBQUFBLEtBQUssRUFBRSx5QkFBRyxxQkFBSCxDQUQrQztBQUV0REMsUUFBQUEsV0FBVyxFQUFFLHlCQUFHLG9FQUFIO0FBRnlDLE9BQTFEO0FBSUg7O0FBQ0QxQixJQUFBQSxPQUFPLENBQUNrQixLQUFSLENBQWNuQixDQUFkO0FBQ0gsR0FaRDtBQWFILEMsQ0FFRDtBQUNBOzs7QUFDQSxJQUFJLENBQUNYLE1BQU0sQ0FBQ2tJLGFBQVosRUFBMkI7QUFDdkI5RyxzQkFBSStHLFFBQUosQ0FBYTNELFNBQWIsRUFEdUIsQ0FFdkI7QUFDQTtBQUNBOzs7QUFDQSxNQUFJNEQsU0FBUyxDQUFDQyxZQUFkLEVBQTRCO0FBQ3hCRCxJQUFBQSxTQUFTLENBQUNDLFlBQVYsQ0FBdUJDLGdCQUF2QixDQUF3QyxNQUF4QyxFQUFnRCxZQUFXLENBQUUsQ0FBN0Q7QUFDQUYsSUFBQUEsU0FBUyxDQUFDQyxZQUFWLENBQXVCQyxnQkFBdkIsQ0FBd0MsT0FBeEMsRUFBaUQsWUFBVyxDQUFFLENBQTlEO0FBQ0FGLElBQUFBLFNBQVMsQ0FBQ0MsWUFBVixDQUF1QkMsZ0JBQXZCLENBQXdDLGNBQXhDLEVBQXdELFlBQVcsQ0FBRSxDQUFyRTtBQUNBRixJQUFBQSxTQUFTLENBQUNDLFlBQVYsQ0FBdUJDLGdCQUF2QixDQUF3QyxhQUF4QyxFQUF1RCxZQUFXLENBQUUsQ0FBcEU7QUFDQUYsSUFBQUEsU0FBUyxDQUFDQyxZQUFWLENBQXVCQyxnQkFBdkIsQ0FBd0MsZUFBeEMsRUFBeUQsWUFBVyxDQUFFLENBQXRFO0FBQ0FGLElBQUFBLFNBQVMsQ0FBQ0MsWUFBVixDQUF1QkMsZ0JBQXZCLENBQXdDLFdBQXhDLEVBQXFELFlBQVcsQ0FBRSxDQUFsRTtBQUNIO0FBQ0o7O0FBRUQsTUFBTWxELFdBQVcsR0FBRztBQUNoQm1ELEVBQUFBLGNBQWMsRUFBRSxVQUFTL0csTUFBVCxFQUFpQjtBQUM3QixRQUFJTixJQUFJLEdBQUdrRSxXQUFXLENBQUNvRCxPQUFaLENBQW9CaEgsTUFBcEIsQ0FBWDtBQUNBLFFBQUlOLElBQUosRUFBVSxPQUFPQSxJQUFQOztBQUVWLFFBQUlmLGlCQUFKLEVBQXVCO0FBQ25CZSxNQUFBQSxJQUFJLEdBQUdmLGlCQUFpQixDQUFDc0ksd0JBQWxCLENBQTJDakgsTUFBM0MsQ0FBUDtBQUNIOztBQUNELFFBQUlOLElBQUosRUFBVSxPQUFPQSxJQUFQO0FBRVYsV0FBTyxJQUFQO0FBQ0gsR0FYZTtBQWFoQnNILEVBQUFBLE9BQU8sRUFBRSxVQUFTaEgsTUFBVCxFQUFpQjtBQUN0QixXQUFPdEIsS0FBSyxDQUFDc0IsTUFBRCxDQUFMLElBQWlCLElBQXhCO0FBQ0gsR0FmZTtBQWlCaEI2RCxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCLFVBQU1xRCxjQUFjLEdBQUdDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZMUksS0FBWixDQUF2Qjs7QUFDQSxTQUFLLElBQUkySSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxjQUFjLENBQUMzRixNQUFuQyxFQUEyQzhGLENBQUMsRUFBNUMsRUFBZ0Q7QUFDNUMsVUFBSTNJLEtBQUssQ0FBQ3dJLGNBQWMsQ0FBQ0csQ0FBRCxDQUFmLENBQUwsSUFDSTNJLEtBQUssQ0FBQ3dJLGNBQWMsQ0FBQ0csQ0FBRCxDQUFmLENBQUwsQ0FBeUJqRixVQUF6QixLQUF3QyxPQURoRCxFQUN5RDtBQUNyRCxlQUFPMUQsS0FBSyxDQUFDd0ksY0FBYyxDQUFDRyxDQUFELENBQWYsQ0FBWjtBQUNIO0FBQ0o7O0FBQ0QsV0FBTyxJQUFQO0FBQ0gsR0ExQmU7O0FBNEJoQjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBQyxFQUFBQSxvQkFBb0IsRUFBRSxVQUFTQyxXQUFULEVBQXNCO0FBQ3hDNUksSUFBQUEsaUJBQWlCLEdBQUc0SSxXQUFwQjtBQUNILEdBaERlO0FBa0RoQkMsRUFBQUEsb0JBQW9CLEVBQUUsWUFBVztBQUM3QixXQUFPN0ksaUJBQVA7QUFDSDtBQXBEZSxDQUFwQixDLENBc0RBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLElBQUlILE1BQU0sQ0FBQ2tJLGFBQVAsS0FBeUI1RSxTQUE3QixFQUF3QztBQUNwQ3RELEVBQUFBLE1BQU0sQ0FBQ2tJLGFBQVAsR0FBdUI5QyxXQUF2QjtBQUNIOztlQUVjcEYsTUFBTSxDQUFDa0ksYSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLypcclxuICogTWFuYWdlcyBhIGxpc3Qgb2YgYWxsIHRoZSBjdXJyZW50bHkgYWN0aXZlIGNhbGxzLlxyXG4gKlxyXG4gKiBUaGlzIGhhbmRsZXIgZGlzcGF0Y2hlcyB3aGVuIHZvaXAgY2FsbHMgYXJlIGFkZGVkL3VwZGF0ZWQvcmVtb3ZlZCBmcm9tIHRoaXMgbGlzdDpcclxuICoge1xyXG4gKiAgIGFjdGlvbjogJ2NhbGxfc3RhdGUnXHJcbiAqICAgcm9vbV9pZDogPHJvb20gSUQgb2YgdGhlIGNhbGw+XHJcbiAqIH1cclxuICpcclxuICogVG8ga25vdyB0aGUgc3RhdGUgb2YgdGhlIGNhbGwsIHRoaXMgaGFuZGxlciBleHBvc2VzIGEgZ2V0dGVyIHRvXHJcbiAqIG9idGFpbiB0aGUgY2FsbCBmb3IgYSByb29tOlxyXG4gKiAgIHZhciBjYWxsID0gQ2FsbEhhbmRsZXIuZ2V0Q2FsbChyb29tSWQpXHJcbiAqICAgdmFyIHN0YXRlID0gY2FsbC5jYWxsX3N0YXRlOyAvLyByaW5naW5nfHJpbmdiYWNrfGNvbm5lY3RlZHxlbmRlZHxidXN5fHN0b3BfcmluZ2JhY2t8c3RvcF9yaW5naW5nXHJcbiAqXHJcbiAqIFRoaXMgaGFuZGxlciBsaXN0ZW5zIGZvciBhbmQgaGFuZGxlcyB0aGUgZm9sbG93aW5nIGFjdGlvbnM6XHJcbiAqIHtcclxuICogICBhY3Rpb246ICdwbGFjZV9jYWxsJyxcclxuICogICB0eXBlOiAndm9pY2V8dmlkZW8nLFxyXG4gKiAgIHJvb21faWQ6IDxyb29tIHRoYXQgdGhlIHBsYWNlIGNhbGwgYnV0dG9uIHdhcyBwcmVzc2VkIGluPlxyXG4gKiB9XHJcbiAqXHJcbiAqIHtcclxuICogICBhY3Rpb246ICdpbmNvbWluZ19jYWxsJ1xyXG4gKiAgIGNhbGw6IE1hdHJpeENhbGxcclxuICogfVxyXG4gKlxyXG4gKiB7XHJcbiAqICAgYWN0aW9uOiAnaGFuZ3VwJ1xyXG4gKiAgIHJvb21faWQ6IDxyb29tIHRoYXQgdGhlIGhhbmd1cCBidXR0b24gd2FzIHByZXNzZWQgaW4+XHJcbiAqIH1cclxuICpcclxuICoge1xyXG4gKiAgIGFjdGlvbjogJ2Fuc3dlcidcclxuICogICByb29tX2lkOiA8cm9vbSB0aGF0IHRoZSBhbnN3ZXIgYnV0dG9uIHdhcyBwcmVzc2VkIGluPlxyXG4gKiB9XHJcbiAqL1xyXG5cclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gJy4vUGxhdGZvcm1QZWcnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi9Nb2RhbCc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBNYXRyaXggZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0IHsgc2hvd1Vua25vd25EZXZpY2VEaWFsb2dGb3JDYWxscyB9IGZyb20gJy4vY3J5cHRvZGV2aWNlcyc7XHJcbmltcG9ydCBXaWRnZXRVdGlscyBmcm9tICcuL3V0aWxzL1dpZGdldFV0aWxzJztcclxuaW1wb3J0IFdpZGdldEVjaG9TdG9yZSBmcm9tICcuL3N0b3Jlcy9XaWRnZXRFY2hvU3RvcmUnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwgeyBTZXR0aW5nTGV2ZWwgfSBmcm9tICcuL3NldHRpbmdzL1NldHRpbmdzU3RvcmUnO1xyXG5pbXBvcnQge2dlbmVyYXRlSHVtYW5SZWFkYWJsZUlkfSBmcm9tIFwiLi91dGlscy9OYW1pbmdVdGlsc1wiO1xyXG5pbXBvcnQge0ppdHNpfSBmcm9tIFwiLi93aWRnZXRzL0ppdHNpXCI7XHJcblxyXG5nbG9iYWwubXhDYWxscyA9IHtcclxuICAgIC8vcm9vbV9pZDogTWF0cml4Q2FsbFxyXG59O1xyXG5jb25zdCBjYWxscyA9IGdsb2JhbC5teENhbGxzO1xyXG5sZXQgQ29uZmVyZW5jZUhhbmRsZXIgPSBudWxsO1xyXG5cclxuY29uc3QgYXVkaW9Qcm9taXNlcyA9IHt9O1xyXG5cclxuZnVuY3Rpb24gcGxheShhdWRpb0lkKSB7XHJcbiAgICAvLyBUT0RPOiBBdHRhY2ggYW4gaW52aXNpYmxlIGVsZW1lbnQgZm9yIHRoaXMgaW5zdGVhZFxyXG4gICAgLy8gd2hpY2ggbGlzdGVucz9cclxuICAgIGNvbnN0IGF1ZGlvID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYXVkaW9JZCk7XHJcbiAgICBpZiAoYXVkaW8pIHtcclxuICAgICAgICBjb25zdCBwbGF5QXVkaW8gPSBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAvLyBUaGlzIHN0aWxsIGNhdXNlcyB0aGUgY2hyb21lIGRlYnVnZ2VyIHRvIGJyZWFrIG9uIHByb21pc2UgcmVqZWN0aW9uIGlmXHJcbiAgICAgICAgICAgICAgICAvLyB0aGUgcHJvbWlzZSBpcyByZWplY3RlZCwgZXZlbiB0aG91Z2ggd2UncmUgY2F0Y2hpbmcgdGhlIGV4Y2VwdGlvbi5cclxuICAgICAgICAgICAgICAgIGF3YWl0IGF1ZGlvLnBsYXkoKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBpcyB1c3VhbGx5IGJlY2F1c2UgdGhlIHVzZXIgaGFzbid0IGludGVyYWN0ZWQgd2l0aCB0aGUgZG9jdW1lbnQsXHJcbiAgICAgICAgICAgICAgICAvLyBvciBjaHJvbWUgZG9lc24ndCB0aGluayBzbyBhbmQgaXMgZGVueWluZyB0aGUgcmVxdWVzdC4gTm90IHN1cmUgd2hhdFxyXG4gICAgICAgICAgICAgICAgLy8gd2UgY2FuIHJlYWxseSBkbyBoZXJlLi4uXHJcbiAgICAgICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vdmVjdG9yLWltL3Jpb3Qtd2ViL2lzc3Vlcy83NjU3XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlVuYWJsZSB0byBwbGF5IGF1ZGlvIGNsaXBcIiwgZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChhdWRpb1Byb21pc2VzW2F1ZGlvSWRdKSB7XHJcbiAgICAgICAgICAgIGF1ZGlvUHJvbWlzZXNbYXVkaW9JZF0gPSBhdWRpb1Byb21pc2VzW2F1ZGlvSWRdLnRoZW4oKCk9PntcclxuICAgICAgICAgICAgICAgIGF1ZGlvLmxvYWQoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwbGF5QXVkaW8oKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgYXVkaW9Qcm9taXNlc1thdWRpb0lkXSA9IHBsYXlBdWRpbygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gcGF1c2UoYXVkaW9JZCkge1xyXG4gICAgLy8gVE9ETzogQXR0YWNoIGFuIGludmlzaWJsZSBlbGVtZW50IGZvciB0aGlzIGluc3RlYWRcclxuICAgIC8vIHdoaWNoIGxpc3RlbnM/XHJcbiAgICBjb25zdCBhdWRpbyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGF1ZGlvSWQpO1xyXG4gICAgaWYgKGF1ZGlvKSB7XHJcbiAgICAgICAgaWYgKGF1ZGlvUHJvbWlzZXNbYXVkaW9JZF0pIHtcclxuICAgICAgICAgICAgYXVkaW9Qcm9taXNlc1thdWRpb0lkXSA9IGF1ZGlvUHJvbWlzZXNbYXVkaW9JZF0udGhlbigoKT0+YXVkaW8ucGF1c2UoKSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gcGF1c2UgZG9lc24ndCBhY3R1YWxseSByZXR1cm4gYSBwcm9taXNlLCBidXQgbWlnaHQgYXMgd2VsbCBkbyB0aGlzIGZvciBzeW1tZXRyeSB3aXRoIHBsYXkoKTtcclxuICAgICAgICAgICAgYXVkaW9Qcm9taXNlc1thdWRpb0lkXSA9IGF1ZGlvLnBhdXNlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBfcmVBdHRlbXB0Q2FsbChjYWxsKSB7XHJcbiAgICBpZiAoY2FsbC5kaXJlY3Rpb24gPT09ICdvdXRib3VuZCcpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246ICdwbGFjZV9jYWxsJyxcclxuICAgICAgICAgICAgcm9vbV9pZDogY2FsbC5yb29tSWQsXHJcbiAgICAgICAgICAgIHR5cGU6IGNhbGwudHlwZSxcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY2FsbC5hbnN3ZXIoKTtcclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gX3NldENhbGxMaXN0ZW5lcnMoY2FsbCkge1xyXG4gICAgY2FsbC5vbihcImVycm9yXCIsIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgIGNvbnNvbGUuZXJyb3IoXCJDYWxsIGVycm9yOlwiLCBlcnIpO1xyXG4gICAgICAgIGlmIChlcnIuY29kZSA9PT0gJ3Vua25vd25fZGV2aWNlcycpIHtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuXHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NhbGwgRmFpbGVkJywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ0NhbGwgRmFpbGVkJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJUaGVyZSBhcmUgdW5rbm93biBzZXNzaW9ucyBpbiB0aGlzIHJvb206IFwiK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiaWYgeW91IHByb2NlZWQgd2l0aG91dCB2ZXJpZnlpbmcgdGhlbSwgaXQgd2lsbCBiZSBcIitcclxuICAgICAgICAgICAgICAgICAgICBcInBvc3NpYmxlIGZvciBzb21lb25lIHRvIGVhdmVzZHJvcCBvbiB5b3VyIGNhbGwuXCIsXHJcbiAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgYnV0dG9uOiBfdCgnUmV2aWV3IFNlc3Npb25zJyksXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiBmdW5jdGlvbihjb25maXJtZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShjYWxsLnJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3dVbmtub3duRGV2aWNlRGlhbG9nRm9yQ2FsbHMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9yZUF0dGVtcHRDYWxsKGNhbGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGwuZGlyZWN0aW9uID09PSAnb3V0Ym91bmQnID8gX3QoXCJDYWxsIEFueXdheVwiKSA6IF90KFwiQW5zd2VyIEFueXdheVwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGwuZGlyZWN0aW9uID09PSAnb3V0Ym91bmQnID8gX3QoXCJDYWxsXCIpIDogX3QoXCJBbnN3ZXJcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFR1cm5TZXJ2ZXJzKCkubGVuZ3RoID09PSAwICYmXHJcbiAgICAgICAgICAgICAgICBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKFwiZmFsbGJhY2tJQ0VTZXJ2ZXJBbGxvd2VkXCIpID09PSBudWxsXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgX3Nob3dJQ0VGYWxsYmFja1Byb21wdCgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEZhaWxlZCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdDYWxsIEZhaWxlZCcpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGVyci5tZXNzYWdlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuICAgIGNhbGwub24oXCJoYW5ndXBcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgX3NldENhbGxTdGF0ZSh1bmRlZmluZWQsIGNhbGwucm9vbUlkLCBcImVuZGVkXCIpO1xyXG4gICAgfSk7XHJcbiAgICAvLyBtYXAgd2ViIHJ0YyBzdGF0ZXMgdG8gZHVtbXkgVUkgc3RhdGVcclxuICAgIC8vIHJpbmdpbmd8cmluZ2JhY2t8Y29ubmVjdGVkfGVuZGVkfGJ1c3l8c3RvcF9yaW5nYmFja3xzdG9wX3JpbmdpbmdcclxuICAgIGNhbGwub24oXCJzdGF0ZVwiLCBmdW5jdGlvbihuZXdTdGF0ZSwgb2xkU3RhdGUpIHtcclxuICAgICAgICBpZiAobmV3U3RhdGUgPT09IFwicmluZ2luZ1wiKSB7XHJcbiAgICAgICAgICAgIF9zZXRDYWxsU3RhdGUoY2FsbCwgY2FsbC5yb29tSWQsIFwicmluZ2luZ1wiKTtcclxuICAgICAgICAgICAgcGF1c2UoXCJyaW5nYmFja0F1ZGlvXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobmV3U3RhdGUgPT09IFwiaW52aXRlX3NlbnRcIikge1xyXG4gICAgICAgICAgICBfc2V0Q2FsbFN0YXRlKGNhbGwsIGNhbGwucm9vbUlkLCBcInJpbmdiYWNrXCIpO1xyXG4gICAgICAgICAgICBwbGF5KFwicmluZ2JhY2tBdWRpb1wiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5ld1N0YXRlID09PSBcImVuZGVkXCIgJiYgb2xkU3RhdGUgPT09IFwiY29ubmVjdGVkXCIpIHtcclxuICAgICAgICAgICAgX3NldENhbGxTdGF0ZSh1bmRlZmluZWQsIGNhbGwucm9vbUlkLCBcImVuZGVkXCIpO1xyXG4gICAgICAgICAgICBwYXVzZShcInJpbmdiYWNrQXVkaW9cIik7XHJcbiAgICAgICAgICAgIHBsYXkoXCJjYWxsZW5kQXVkaW9cIik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChuZXdTdGF0ZSA9PT0gXCJlbmRlZFwiICYmIG9sZFN0YXRlID09PSBcImludml0ZV9zZW50XCIgJiZcclxuICAgICAgICAgICAgICAgIChjYWxsLmhhbmd1cFBhcnR5ID09PSBcInJlbW90ZVwiIHx8XHJcbiAgICAgICAgICAgICAgICAoY2FsbC5oYW5ndXBQYXJ0eSA9PT0gXCJsb2NhbFwiICYmIGNhbGwuaGFuZ3VwUmVhc29uID09PSBcImludml0ZV90aW1lb3V0XCIpXHJcbiAgICAgICAgICAgICAgICApKSB7XHJcbiAgICAgICAgICAgIF9zZXRDYWxsU3RhdGUoY2FsbCwgY2FsbC5yb29tSWQsIFwiYnVzeVwiKTtcclxuICAgICAgICAgICAgcGF1c2UoXCJyaW5nYmFja0F1ZGlvXCIpO1xyXG4gICAgICAgICAgICBwbGF5KFwiYnVzeUF1ZGlvXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEhhbmRsZXInLCAnQ2FsbCBUaW1lb3V0JywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnQ2FsbCBUaW1lb3V0JyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ1RoZSByZW1vdGUgc2lkZSBmYWlsZWQgdG8gcGljayB1cCcpICsgJy4nLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKG9sZFN0YXRlID09PSBcImludml0ZV9zZW50XCIpIHtcclxuICAgICAgICAgICAgX3NldENhbGxTdGF0ZShjYWxsLCBjYWxsLnJvb21JZCwgXCJzdG9wX3JpbmdiYWNrXCIpO1xyXG4gICAgICAgICAgICBwYXVzZShcInJpbmdiYWNrQXVkaW9cIik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChvbGRTdGF0ZSA9PT0gXCJyaW5naW5nXCIpIHtcclxuICAgICAgICAgICAgX3NldENhbGxTdGF0ZShjYWxsLCBjYWxsLnJvb21JZCwgXCJzdG9wX3JpbmdpbmdcIik7XHJcbiAgICAgICAgICAgIHBhdXNlKFwicmluZ2JhY2tBdWRpb1wiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5ld1N0YXRlID09PSBcImNvbm5lY3RlZFwiKSB7XHJcbiAgICAgICAgICAgIF9zZXRDYWxsU3RhdGUoY2FsbCwgY2FsbC5yb29tSWQsIFwiY29ubmVjdGVkXCIpO1xyXG4gICAgICAgICAgICBwYXVzZShcInJpbmdiYWNrQXVkaW9cIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIF9zZXRDYWxsU3RhdGUoY2FsbCwgcm9vbUlkLCBzdGF0dXMpIHtcclxuICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgIGBDYWxsIHN0YXRlIGluICR7cm9vbUlkfSBjaGFuZ2VkIHRvICR7c3RhdHVzfSAoJHtjYWxsID8gY2FsbC5jYWxsX3N0YXRlIDogXCItXCJ9KWAsXHJcbiAgICApO1xyXG4gICAgY2FsbHNbcm9vbUlkXSA9IGNhbGw7XHJcblxyXG4gICAgaWYgKHN0YXR1cyA9PT0gXCJyaW5naW5nXCIpIHtcclxuICAgICAgICBwbGF5KFwicmluZ0F1ZGlvXCIpO1xyXG4gICAgfSBlbHNlIGlmIChjYWxsICYmIGNhbGwuY2FsbF9zdGF0ZSA9PT0gXCJyaW5naW5nXCIpIHtcclxuICAgICAgICBwYXVzZShcInJpbmdBdWRpb1wiKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoY2FsbCkge1xyXG4gICAgICAgIGNhbGwuY2FsbF9zdGF0ZSA9IHN0YXR1cztcclxuICAgIH1cclxuICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgYWN0aW9uOiAnY2FsbF9zdGF0ZScsXHJcbiAgICAgICAgcm9vbV9pZDogcm9vbUlkLFxyXG4gICAgICAgIHN0YXRlOiBzdGF0dXMsXHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gX3Nob3dJQ0VGYWxsYmFja1Byb21wdCgpIHtcclxuICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgIGNvbnN0IFF1ZXN0aW9uRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuUXVlc3Rpb25EaWFsb2dcIik7XHJcbiAgICBjb25zdCBjb2RlID0gc3ViID0+IDxjb2RlPntzdWJ9PC9jb2RlPjtcclxuICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ05vIFRVUk4gc2VydmVycycsICcnLCBRdWVzdGlvbkRpYWxvZywge1xyXG4gICAgICAgIHRpdGxlOiBfdChcIkNhbGwgZmFpbGVkIGR1ZSB0byBtaXNjb25maWd1cmVkIHNlcnZlclwiKSxcclxuICAgICAgICBkZXNjcmlwdGlvbjogPGRpdj5cclxuICAgICAgICAgICAgPHA+e190KFxyXG4gICAgICAgICAgICAgICAgXCJQbGVhc2UgYXNrIHRoZSBhZG1pbmlzdHJhdG9yIG9mIHlvdXIgaG9tZXNlcnZlciBcIiArXHJcbiAgICAgICAgICAgICAgICBcIig8Y29kZT4lKGhvbWVzZXJ2ZXJEb21haW4pczwvY29kZT4pIHRvIGNvbmZpZ3VyZSBhIFRVUk4gc2VydmVyIGluIFwiICtcclxuICAgICAgICAgICAgICAgIFwib3JkZXIgZm9yIGNhbGxzIHRvIHdvcmsgcmVsaWFibHkuXCIsXHJcbiAgICAgICAgICAgICAgICB7IGhvbWVzZXJ2ZXJEb21haW46IGNsaS5nZXREb21haW4oKSB9LCB7IGNvZGUgfSxcclxuICAgICAgICAgICAgKX08L3A+XHJcbiAgICAgICAgICAgIDxwPntfdChcclxuICAgICAgICAgICAgICAgIFwiQWx0ZXJuYXRpdmVseSwgeW91IGNhbiB0cnkgdG8gdXNlIHRoZSBwdWJsaWMgc2VydmVyIGF0IFwiICtcclxuICAgICAgICAgICAgICAgIFwiPGNvZGU+dHVybi5tYXRyaXgub3JnPC9jb2RlPiwgYnV0IHRoaXMgd2lsbCBub3QgYmUgYXMgcmVsaWFibGUsIGFuZCBcIiArXHJcbiAgICAgICAgICAgICAgICBcIml0IHdpbGwgc2hhcmUgeW91ciBJUCBhZGRyZXNzIHdpdGggdGhhdCBzZXJ2ZXIuIFlvdSBjYW4gYWxzbyBtYW5hZ2UgXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJ0aGlzIGluIFNldHRpbmdzLlwiLFxyXG4gICAgICAgICAgICAgICAgbnVsbCwgeyBjb2RlIH0sXHJcbiAgICAgICAgICAgICl9PC9wPlxyXG4gICAgICAgIDwvZGl2PixcclxuICAgICAgICBidXR0b246IF90KCdUcnkgdXNpbmcgdHVybi5tYXRyaXgub3JnJyksXHJcbiAgICAgICAgY2FuY2VsQnV0dG9uOiBfdCgnT0snKSxcclxuICAgICAgICBvbkZpbmlzaGVkOiAoYWxsb3cpID0+IHtcclxuICAgICAgICAgICAgU2V0dGluZ3NTdG9yZS5zZXRWYWx1ZShcImZhbGxiYWNrSUNFU2VydmVyQWxsb3dlZFwiLCBudWxsLCBTZXR0aW5nTGV2ZWwuREVWSUNFLCBhbGxvdyk7XHJcbiAgICAgICAgICAgIGNsaS5zZXRGYWxsYmFja0lDRVNlcnZlckFsbG93ZWQoYWxsb3cpO1xyXG4gICAgICAgIH0sXHJcbiAgICB9LCBudWxsLCB0cnVlKTtcclxufVxyXG5cclxuZnVuY3Rpb24gX29uQWN0aW9uKHBheWxvYWQpIHtcclxuICAgIGZ1bmN0aW9uIHBsYWNlQ2FsbChuZXdDYWxsKSB7XHJcbiAgICAgICAgX3NldENhbGxMaXN0ZW5lcnMobmV3Q2FsbCk7XHJcbiAgICAgICAgaWYgKHBheWxvYWQudHlwZSA9PT0gJ3ZvaWNlJykge1xyXG4gICAgICAgICAgICBuZXdDYWxsLnBsYWNlVm9pY2VDYWxsKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChwYXlsb2FkLnR5cGUgPT09ICd2aWRlbycpIHtcclxuICAgICAgICAgICAgbmV3Q2FsbC5wbGFjZVZpZGVvQ2FsbChcclxuICAgICAgICAgICAgICAgIHBheWxvYWQucmVtb3RlX2VsZW1lbnQsXHJcbiAgICAgICAgICAgICAgICBwYXlsb2FkLmxvY2FsX2VsZW1lbnQsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChwYXlsb2FkLnR5cGUgPT09ICdzY3JlZW5zaGFyaW5nJykge1xyXG4gICAgICAgICAgICBjb25zdCBzY3JlZW5DYXBFcnJvclN0cmluZyA9IFBsYXRmb3JtUGVnLmdldCgpLnNjcmVlbkNhcHR1cmVFcnJvclN0cmluZygpO1xyXG4gICAgICAgICAgICBpZiAoc2NyZWVuQ2FwRXJyb3JTdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIF9zZXRDYWxsU3RhdGUodW5kZWZpbmVkLCBuZXdDYWxsLnJvb21JZCwgXCJlbmRlZFwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ2FuJ3QgY2FwdHVyZSBzY3JlZW46IFwiICsgc2NyZWVuQ2FwRXJyb3JTdHJpbmcpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NhbGwgSGFuZGxlcicsICdVbmFibGUgdG8gY2FwdHVyZSBzY3JlZW4nLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnVW5hYmxlIHRvIGNhcHR1cmUgc2NyZWVuJyksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IHNjcmVlbkNhcEVycm9yU3RyaW5nLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV3Q2FsbC5wbGFjZVNjcmVlblNoYXJpbmdDYWxsKFxyXG4gICAgICAgICAgICAgICAgcGF5bG9hZC5yZW1vdGVfZWxlbWVudCxcclxuICAgICAgICAgICAgICAgIHBheWxvYWQubG9jYWxfZWxlbWVudCxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiVW5rbm93biBjb25mIGNhbGwgdHlwZTogJXNcIiwgcGF5bG9hZC50eXBlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3dpdGNoIChwYXlsb2FkLmFjdGlvbikge1xyXG4gICAgICAgIGNhc2UgJ3BsYWNlX2NhbGwnOlxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2FsbEhhbmRsZXIuZ2V0QW55QWN0aXZlQ2FsbCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEhhbmRsZXInLCAnRXhpc3RpbmcgQ2FsbCcsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRXhpc3RpbmcgQ2FsbCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ1lvdSBhcmUgYWxyZWFkeSBpbiBhIGNhbGwuJyksXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuOyAvLyBkb24ndCBhbGxvdyA+MSBjYWxsIHRvIGJlIHBsYWNlZC5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBpZiB0aGUgcnVudGltZSBlbnYgZG9lc24ndCBkbyBWb0lQLCB3aGluZS5cclxuICAgICAgICAgICAgICAgIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpLnN1cHBvcnRzVm9pcCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEhhbmRsZXInLCAnVm9JUCBpcyB1bnN1cHBvcnRlZCcsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnVm9JUCBpcyB1bnN1cHBvcnRlZCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ1lvdSBjYW5ub3QgcGxhY2UgVm9JUCBjYWxscyBpbiB0aGlzIGJyb3dzZXIuJyksXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0Um9vbShwYXlsb2FkLnJvb21faWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIlJvb20gJXMgZG9lcyBub3QgZXhpc3QuXCIsIHBheWxvYWQucm9vbV9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG1lbWJlcnMgPSByb29tLmdldEpvaW5lZE1lbWJlcnMoKTtcclxuICAgICAgICAgICAgICAgIGlmIChtZW1iZXJzLmxlbmd0aCA8PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEhhbmRsZXInLCAnQ2Fubm90IHBsYWNlIGNhbGwgd2l0aCBzZWxmJywgRXJyb3JEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdZb3UgY2Fubm90IHBsYWNlIGEgY2FsbCB3aXRoIHlvdXJzZWxmLicpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobWVtYmVycy5sZW5ndGggPT09IDIpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmluZm8oXCJQbGFjZSAlcyBjYWxsIGluICVzXCIsIHBheWxvYWQudHlwZSwgcGF5bG9hZC5yb29tX2lkKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjYWxsID0gTWF0cml4LmNyZWF0ZU5ld01hdHJpeENhbGwoTWF0cml4Q2xpZW50UGVnLmdldCgpLCBwYXlsb2FkLnJvb21faWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlQ2FsbChjYWxsKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7IC8vID4gMlxyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogXCJwbGFjZV9jb25mZXJlbmNlX2NhbGxcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogcGF5bG9hZC5yb29tX2lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBwYXlsb2FkLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbW90ZV9lbGVtZW50OiBwYXlsb2FkLnJlbW90ZV9lbGVtZW50LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbF9lbGVtZW50OiBwYXlsb2FkLmxvY2FsX2VsZW1lbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAncGxhY2VfY29uZmVyZW5jZV9jYWxsJzpcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKFwiUGxhY2UgY29uZmVyZW5jZSBjYWxsIGluICVzXCIsIHBheWxvYWQucm9vbV9pZCk7XHJcbiAgICAgICAgICAgIF9zdGFydENhbGxBcHAocGF5bG9hZC5yb29tX2lkLCBwYXlsb2FkLnR5cGUpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlICdpbmNvbWluZ19jYWxsJzpcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxIYW5kbGVyLmdldEFueUFjdGl2ZUNhbGwoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlnbm9yZSBtdWx0aXBsZSBpbmNvbWluZyBjYWxscy4gaW4gZnV0dXJlLCB3ZSBtYXkgd2FudCBhIGxpbmUtMS9saW5lLTIgc2V0dXAuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgYXZvaWQgcmVqZWN0aW5nIHdpdGggXCJidXN5XCIgaW4gY2FzZSB0aGUgdXNlciB3YW50cyB0byBhbnN3ZXIgaXQgb24gYSBkaWZmZXJlbnQgZGV2aWNlLlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGluIGZ1dHVyZSB3ZSBjb3VsZCBzaWduYWwgYSBcImxvY2FsIGJ1c3lcIiBhcyBhIHdhcm5pbmcgdG8gdGhlIGNhbGxlci5cclxuICAgICAgICAgICAgICAgICAgICAvLyBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS92ZWN0b3Itd2ViL2lzc3Vlcy8xOTY0XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBydW50aW1lIGVudiBkb2Vzbid0IGRvIFZvSVAsIHN0b3AgaGVyZS5cclxuICAgICAgICAgICAgICAgIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpLnN1cHBvcnRzVm9pcCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhbGwgPSBwYXlsb2FkLmNhbGw7XHJcbiAgICAgICAgICAgICAgICBfc2V0Q2FsbExpc3RlbmVycyhjYWxsKTtcclxuICAgICAgICAgICAgICAgIF9zZXRDYWxsU3RhdGUoY2FsbCwgY2FsbC5yb29tSWQsIFwicmluZ2luZ1wiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlICdoYW5ndXAnOlxyXG4gICAgICAgICAgICBpZiAoIWNhbGxzW3BheWxvYWQucm9vbV9pZF0pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjsgLy8gbm8gY2FsbCB0byBoYW5ndXBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYWxsc1twYXlsb2FkLnJvb21faWRdLmhhbmd1cCgpO1xyXG4gICAgICAgICAgICBfc2V0Q2FsbFN0YXRlKG51bGwsIHBheWxvYWQucm9vbV9pZCwgXCJlbmRlZFwiKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAnYW5zd2VyJzpcclxuICAgICAgICAgICAgaWYgKCFjYWxsc1twYXlsb2FkLnJvb21faWRdKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vIG5vIGNhbGwgdG8gYW5zd2VyXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FsbHNbcGF5bG9hZC5yb29tX2lkXS5hbnN3ZXIoKTtcclxuICAgICAgICAgICAgX3NldENhbGxTdGF0ZShjYWxsc1twYXlsb2FkLnJvb21faWRdLCBwYXlsb2FkLnJvb21faWQsIFwiY29ubmVjdGVkXCIpO1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBcInZpZXdfcm9vbVwiLFxyXG4gICAgICAgICAgICAgICAgcm9vbV9pZDogcGF5bG9hZC5yb29tX2lkLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbn1cclxuXHJcbmFzeW5jIGZ1bmN0aW9uIF9zdGFydENhbGxBcHAocm9vbUlkLCB0eXBlKSB7XHJcbiAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgIGFjdGlvbjogJ2FwcHNEcmF3ZXInLFxyXG4gICAgICAgIHNob3c6IHRydWUsXHJcbiAgICB9KTtcclxuXHJcbiAgICBjb25zdCByb29tID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20ocm9vbUlkKTtcclxuICAgIGNvbnN0IGN1cnJlbnRSb29tV2lkZ2V0cyA9IFdpZGdldFV0aWxzLmdldFJvb21XaWRnZXRzKHJvb20pO1xyXG5cclxuICAgIGlmIChXaWRnZXRFY2hvU3RvcmUucm9vbUhhc1BlbmRpbmdXaWRnZXRzT2ZUeXBlKHJvb21JZCwgY3VycmVudFJvb21XaWRnZXRzLCAnaml0c2knKSkge1xyXG4gICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcblxyXG4gICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0NhbGwgYWxyZWFkeSBpbiBwcm9ncmVzcycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoJ0NhbGwgaW4gUHJvZ3Jlc3MnKSxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdBIGNhbGwgaXMgY3VycmVudGx5IGJlaW5nIHBsYWNlZCEnKSxcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgY3VycmVudEppdHNpV2lkZ2V0cyA9IGN1cnJlbnRSb29tV2lkZ2V0cy5maWx0ZXIoKGV2KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGV2LmdldENvbnRlbnQoKS50eXBlID09PSAnaml0c2knO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoY3VycmVudEppdHNpV2lkZ2V0cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKFxyXG4gICAgICAgICAgICBcIlJlZnVzaW5nIHRvIHN0YXJ0IGNvbmZlcmVuY2UgY2FsbCB3aWRnZXQgaW4gXCIgKyByb29tSWQgK1xyXG4gICAgICAgICAgICBcIiBhIGNvbmZlcmVuY2UgY2FsbCB3aWRnZXQgaXMgYWxyZWFkeSBwcmVzZW50XCIsXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG5cclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdBbHJlYWR5IGhhdmUgSml0c2kgV2lkZ2V0JywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBfdCgnQ2FsbCBpbiBQcm9ncmVzcycpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoJ0EgY2FsbCBpcyBhbHJlYWR5IGluIHByb2dyZXNzIScpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBjb25mSWQgPSBgSml0c2lDb25mZXJlbmNlJHtnZW5lcmF0ZUh1bWFuUmVhZGFibGVJZCgpfWA7XHJcbiAgICBjb25zdCBqaXRzaURvbWFpbiA9IEppdHNpLmdldEluc3RhbmNlKCkucHJlZmVycmVkRG9tYWluO1xyXG5cclxuICAgIGxldCB3aWRnZXRVcmwgPSBXaWRnZXRVdGlscy5nZXRMb2NhbEppdHNpV3JhcHBlclVybCgpO1xyXG5cclxuICAgIC8vIFRPRE86IFJlbW92ZSBVUkwgaGFja3Mgd2hlbiB0aGUgbW9iaWxlIGNsaWVudHMgZXZlbnR1YWxseSBzdXBwb3J0IHYyIHdpZGdldHNcclxuICAgIGNvbnN0IHBhcnNlZFVybCA9IG5ldyBVUkwod2lkZ2V0VXJsKTtcclxuICAgIHBhcnNlZFVybC5zZWFyY2ggPSAnJzsgLy8gc2V0IHRvIGVtcHR5IHN0cmluZyB0byBtYWtlIHRoZSBVUkwgY2xhc3MgdXNlIHNlYXJjaFBhcmFtcyBpbnN0ZWFkXHJcbiAgICBwYXJzZWRVcmwuc2VhcmNoUGFyYW1zLnNldCgnY29uZklkJywgY29uZklkKTtcclxuICAgIHdpZGdldFVybCA9IHBhcnNlZFVybC50b1N0cmluZygpO1xyXG5cclxuICAgIGNvbnN0IHdpZGdldERhdGEgPSB7XHJcbiAgICAgICAgY29uZmVyZW5jZUlkOiBjb25mSWQsXHJcbiAgICAgICAgaXNBdWRpb09ubHk6IHR5cGUgPT09ICd2b2ljZScsXHJcbiAgICAgICAgZG9tYWluOiBqaXRzaURvbWFpbixcclxuICAgIH07XHJcblxyXG4gICAgY29uc3Qgd2lkZ2V0SWQgPSAoXHJcbiAgICAgICAgJ2ppdHNpXycgK1xyXG4gICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5jcmVkZW50aWFscy51c2VySWQgK1xyXG4gICAgICAgICdfJyArXHJcbiAgICAgICAgRGF0ZS5ub3coKVxyXG4gICAgKTtcclxuXHJcbiAgICBXaWRnZXRVdGlscy5zZXRSb29tV2lkZ2V0KHJvb21JZCwgd2lkZ2V0SWQsICdqaXRzaScsIHdpZGdldFVybCwgJ0ppdHNpJywgd2lkZ2V0RGF0YSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0ppdHNpIHdpZGdldCBhZGRlZCcpO1xyXG4gICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICBpZiAoZS5lcnJjb2RlID09PSAnTV9GT1JCSURERU4nKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcblxyXG4gICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdDYWxsIEZhaWxlZCcsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IF90KCdQZXJtaXNzaW9uIFJlcXVpcmVkJyksXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogX3QoXCJZb3UgZG8gbm90IGhhdmUgcGVybWlzc2lvbiB0byBzdGFydCBhIGNvbmZlcmVuY2UgY2FsbCBpbiB0aGlzIHJvb21cIiksXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbi8vIEZJWE1FOiBOYXN0eSB3YXkgb2YgbWFraW5nIHN1cmUgd2Ugb25seSByZWdpc3RlclxyXG4vLyB3aXRoIHRoZSBkaXNwYXRjaGVyIG9uY2VcclxuaWYgKCFnbG9iYWwubXhDYWxsSGFuZGxlcikge1xyXG4gICAgZGlzLnJlZ2lzdGVyKF9vbkFjdGlvbik7XHJcbiAgICAvLyBhZGQgZW1wdHkgaGFuZGxlcnMgZm9yIG1lZGlhIGFjdGlvbnMsIG90aGVyd2lzZSB0aGUgbWVkaWEga2V5c1xyXG4gICAgLy8gZW5kIHVwIGNhdXNpbmcgdGhlIGF1ZGlvIGVsZW1lbnRzIHdpdGggb3VyIHJpbmcvcmluZ2JhY2sgZXRjXHJcbiAgICAvLyBhdWRpbyBjbGlwcyBpbiB0byBwbGF5LlxyXG4gICAgaWYgKG5hdmlnYXRvci5tZWRpYVNlc3Npb24pIHtcclxuICAgICAgICBuYXZpZ2F0b3IubWVkaWFTZXNzaW9uLnNldEFjdGlvbkhhbmRsZXIoJ3BsYXknLCBmdW5jdGlvbigpIHt9KTtcclxuICAgICAgICBuYXZpZ2F0b3IubWVkaWFTZXNzaW9uLnNldEFjdGlvbkhhbmRsZXIoJ3BhdXNlJywgZnVuY3Rpb24oKSB7fSk7XHJcbiAgICAgICAgbmF2aWdhdG9yLm1lZGlhU2Vzc2lvbi5zZXRBY3Rpb25IYW5kbGVyKCdzZWVrYmFja3dhcmQnLCBmdW5jdGlvbigpIHt9KTtcclxuICAgICAgICBuYXZpZ2F0b3IubWVkaWFTZXNzaW9uLnNldEFjdGlvbkhhbmRsZXIoJ3NlZWtmb3J3YXJkJywgZnVuY3Rpb24oKSB7fSk7XHJcbiAgICAgICAgbmF2aWdhdG9yLm1lZGlhU2Vzc2lvbi5zZXRBY3Rpb25IYW5kbGVyKCdwcmV2aW91c3RyYWNrJywgZnVuY3Rpb24oKSB7fSk7XHJcbiAgICAgICAgbmF2aWdhdG9yLm1lZGlhU2Vzc2lvbi5zZXRBY3Rpb25IYW5kbGVyKCduZXh0dHJhY2snLCBmdW5jdGlvbigpIHt9KTtcclxuICAgIH1cclxufVxyXG5cclxuY29uc3QgY2FsbEhhbmRsZXIgPSB7XHJcbiAgICBnZXRDYWxsRm9yUm9vbTogZnVuY3Rpb24ocm9vbUlkKSB7XHJcbiAgICAgICAgbGV0IGNhbGwgPSBjYWxsSGFuZGxlci5nZXRDYWxsKHJvb21JZCk7XHJcbiAgICAgICAgaWYgKGNhbGwpIHJldHVybiBjYWxsO1xyXG5cclxuICAgICAgICBpZiAoQ29uZmVyZW5jZUhhbmRsZXIpIHtcclxuICAgICAgICAgICAgY2FsbCA9IENvbmZlcmVuY2VIYW5kbGVyLmdldENvbmZlcmVuY2VDYWxsRm9yUm9vbShyb29tSWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY2FsbCkgcmV0dXJuIGNhbGw7XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRDYWxsOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICByZXR1cm4gY2FsbHNbcm9vbUlkXSB8fCBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRBbnlBY3RpdmVDYWxsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCByb29tc1dpdGhDYWxscyA9IE9iamVjdC5rZXlzKGNhbGxzKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJvb21zV2l0aENhbGxzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChjYWxsc1tyb29tc1dpdGhDYWxsc1tpXV0gJiZcclxuICAgICAgICAgICAgICAgICAgICBjYWxsc1tyb29tc1dpdGhDYWxsc1tpXV0uY2FsbF9zdGF0ZSAhPT0gXCJlbmRlZFwiKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY2FsbHNbcm9vbXNXaXRoQ2FsbHNbaV1dO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBjb25mZXJlbmNlIGhhbmRsZXIgaXMgYSBtb2R1bGUgdGhhdCBkZWFscyB3aXRoIGltcGxlbWVudGF0aW9uLXNwZWNpZmljXHJcbiAgICAgKiBtdWx0aS1wYXJ0eSBjYWxsaW5nIGltcGxlbWVudGF0aW9ucy4gUmlvdCBwYXNzZXMgaW4gaXRzIG93biB3aGljaCBjcmVhdGVzXHJcbiAgICAgKiBhIG9uZS10by1vbmUgY2FsbCB3aXRoIGEgZnJlZXN3aXRjaCBjb25mZXJlbmNlIGJyaWRnZS4gQXMgb2YgSnVseSAyMDE4LFxyXG4gICAgICogdGhlIGRlLWZhY3RvIHdheSBvZiBjb25mZXJlbmNlIGNhbGxpbmcgaXMgYSBKaXRzaSB3aWRnZXQsIHNvIHRoaXMgaXNcclxuICAgICAqIGRlcHJlY2F0ZWQuIEl0IHJlYW1pbnMgaGVyZSBmb3IgdHdvIHJlYXNvbnM6XHJcbiAgICAgKiAgMS4gU28gUmlvdCBzdGlsbCBzdXBwb3J0cyBqb2luaW5nIGV4aXN0aW5nIGZyZWVzd2l0Y2ggY29uZmVyZW5jZSBjYWxsc1xyXG4gICAgICogICAgIChidXQgZG9lc24ndCBzdXBwb3J0IGNyZWF0aW5nIHRoZW0pLiBBZnRlciBhIHRyYW5zaXRpb24gcGVyaW9kLCB3ZSBjYW5cclxuICAgICAqICAgICByZW1vdmUgc3VwcG9ydCBmb3Igam9pbmluZyB0aGVtIHRvby5cclxuICAgICAqICAyLiBUbyBoaWRlIHRoZSBvbmUtdG8tb25lIHJvb21zIHRoYXQgb2xkLXN0eWxlIGNvbmZlcmVuY2luZyBjcmVhdGVzLiBUaGlzXHJcbiAgICAgKiAgICAgaXMgbXVjaCBoYXJkZXIgdG8gcmVtb3ZlOiBwcm9iYWJseSBlaXRoZXIgd2UgbWFrZSBSaW90IGxlYXZlICYgZm9yZ2V0IHRoZXNlXHJcbiAgICAgKiAgICAgcm9vbXMgYWZ0ZXIgd2UgcmVtb3ZlIHN1cHBvcnQgZm9yIGpvaW5pbmcgZnJlZXN3aXRjaCBjb25mZXJlbmNlcywgb3Igd2VcclxuICAgICAqICAgICBhY2NlcHQgdGhhdCByYW5kb20gcm9vbXMgd2l0aCBjcnlwdGljIHVzZXJzIHdpbGwgc3VkZGVudGx5IGFwcGVhciBmb3JcclxuICAgICAqICAgICBhbnlvbmUgd2hvJ3MgZXZlciB1c2VkIGNvbmZlcmVuY2UgY2FsbGluZywgb3Igd2UgYXJlIHN0dWNrIHdpdGggdGhpc1xyXG4gICAgICogICAgIGNvZGUgZm9yZXZlci5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gY29uZkhhbmRsZXIgVGhlIGNvbmZlcmVuY2UgaGFuZGxlciBvYmplY3RcclxuICAgICAqL1xyXG4gICAgc2V0Q29uZmVyZW5jZUhhbmRsZXI6IGZ1bmN0aW9uKGNvbmZIYW5kbGVyKSB7XHJcbiAgICAgICAgQ29uZmVyZW5jZUhhbmRsZXIgPSBjb25mSGFuZGxlcjtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0Q29uZmVyZW5jZUhhbmRsZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBDb25mZXJlbmNlSGFuZGxlcjtcclxuICAgIH0sXHJcbn07XHJcbi8vIE9ubHkgdGhpbmdzIGluIGhlcmUgd2hpY2ggYWN0dWFsbHkgbmVlZCB0byBiZSBnbG9iYWwgYXJlIHRoZVxyXG4vLyBjYWxscyBsaXN0IChkb25lIHNlcGFyYXRlbHkpIGFuZCBtYWtpbmcgc3VyZSB3ZSBvbmx5IHJlZ2lzdGVyXHJcbi8vIHdpdGggdGhlIGRpc3BhdGNoZXIgb25jZSAod2hpY2ggdXNlcyB0aGlzIG1lY2hhbmlzbSBidXQgY2hlY2tzXHJcbi8vIHNlcGFyYXRlbHkpLiBUaGlzIGNvdWxkIGJlIHRpZGllZCB1cC5cclxuaWYgKGdsb2JhbC5teENhbGxIYW5kbGVyID09PSB1bmRlZmluZWQpIHtcclxuICAgIGdsb2JhbC5teENhbGxIYW5kbGVyID0gY2FsbEhhbmRsZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdsb2JhbC5teENhbGxIYW5kbGVyO1xyXG4iXX0=