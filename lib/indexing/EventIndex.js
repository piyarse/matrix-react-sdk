"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _PlatformPeg = _interopRequireDefault(require("../PlatformPeg"));

var _MatrixClientPeg = require("../MatrixClientPeg");

var _matrixJsSdk = require("matrix-js-sdk");

var _promise = require("../utils/promise");

var _SettingsStore = _interopRequireWildcard(require("../settings/SettingsStore"));

var _events = require("events");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Event indexing class that wraps the platform specific event indexing.
 */
class EventIndex extends _events.EventEmitter {
  constructor() {
    super();
    (0, _defineProperty2.default)(this, "onSync", async (state, prevState, data) => {
      const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

      if (prevState === "PREPARED" && state === "SYNCING") {
        // If our indexer is empty we're most likely running Riot the
        // first time with indexing support or running it with an
        // initial sync. Add checkpoints to crawl our encrypted rooms.
        const eventIndexWasEmpty = await indexManager.isEventIndexEmpty();
        if (eventIndexWasEmpty) await this.addInitialCheckpoints();
        this.startCrawler();
        return;
      }

      if (prevState === "SYNCING" && state === "SYNCING") {
        // A sync was done, presumably we queued up some live events,
        // commit them now.
        console.log("EventIndex: Committing events");
        await indexManager.commitLiveEvents();
        return;
      }
    });
    (0, _defineProperty2.default)(this, "onRoomTimeline", async (ev, room, toStartOfTimeline, removed, data) => {
      // We only index encrypted rooms locally.
      if (!_MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(room.roomId)) return; // If it isn't a live event or if it's redacted there's nothing to
      // do.

      if (toStartOfTimeline || !data || !data.liveEvent || ev.isRedacted()) {
        return;
      } // If the event is not yet decrypted mark it for the
      // Event.decrypted callback.


      if (ev.isBeingDecrypted()) {
        const eventId = ev.getId();
        this.liveEventsForIndex.add(eventId);
      } else {
        // If the event is decrypted or is unencrypted add it to the
        // index now.
        await this.addLiveEventToIndex(ev);
      }
    });
    (0, _defineProperty2.default)(this, "onEventDecrypted", async (ev, err) => {
      const eventId = ev.getId(); // If the event isn't in our live event set, ignore it.

      if (!this.liveEventsForIndex.delete(eventId)) return;
      if (err) return;
      await this.addLiveEventToIndex(ev);
    });
    (0, _defineProperty2.default)(this, "onRedaction", async (ev, room) => {
      // We only index encrypted rooms locally.
      if (!_MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(room.roomId)) return;

      const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

      try {
        await indexManager.deleteEvent(ev.getAssociatedId());
      } catch (e) {
        console.log("EventIndex: Error deleting event from index", e);
      }
    });
    (0, _defineProperty2.default)(this, "onTimelineReset", async (room, timelineSet, resetAllTimelines) => {
      if (room === null) return;

      const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

      if (!_MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(room.roomId)) return;
      const timeline = room.getLiveTimeline();
      const token = timeline.getPaginationToken("b");
      const backwardsCheckpoint = {
        roomId: room.roomId,
        token: token,
        fullCrawl: false,
        direction: "b"
      };
      console.log("EventIndex: Added checkpoint because of a limited timeline", backwardsCheckpoint);
      await indexManager.addCrawlerCheckpoint(backwardsCheckpoint);
      this.crawlerCheckpoints.push(backwardsCheckpoint);
    });
    this.crawlerCheckpoints = []; // The time in ms that the crawler will wait loop iterations if there
    // have not been any checkpoints to consume in the last iteration.

    this._crawlerIdleTime = 5000; // The maximum number of events our crawler should fetch in a single
    // crawl.

    this._eventsPerCrawl = 100;
    this._crawler = null;
    this._currentCheckpoint = null;
    this.liveEventsForIndex = new Set();
  }

  async init() {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    await indexManager.initEventIndex();
    console.log("EventIndex: Successfully initialized the event index");
    this.crawlerCheckpoints = await indexManager.loadCheckpoints();
    console.log("EventIndex: Loaded checkpoints", this.crawlerCheckpoints);
    this.registerListeners();
  }
  /**
   * Register event listeners that are necessary for the event index to work.
   */


  registerListeners() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    client.on('sync', this.onSync);
    client.on('Room.timeline', this.onRoomTimeline);
    client.on('Event.decrypted', this.onEventDecrypted);
    client.on('Room.timelineReset', this.onTimelineReset);
    client.on('Room.redaction', this.onRedaction);
  }
  /**
   * Remove the event index specific event listeners.
   */


  removeListeners() {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (client === null) return;
    client.removeListener('sync', this.onSync);
    client.removeListener('Room.timeline', this.onRoomTimeline);
    client.removeListener('Event.decrypted', this.onEventDecrypted);
    client.removeListener('Room.timelineReset', this.onTimelineReset);
    client.removeListener('Room.redaction', this.onRedaction);
  }
  /**
   * Get crawler checkpoints for the encrypted rooms and store them in the index.
   */


  async addInitialCheckpoints() {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const rooms = client.getRooms();

    const isRoomEncrypted = room => {
      return client.isRoomEncrypted(room.roomId);
    }; // We only care to crawl the encrypted rooms, non-encrypted
    // rooms can use the search provided by the homeserver.


    const encryptedRooms = rooms.filter(isRoomEncrypted);
    console.log("EventIndex: Adding initial crawler checkpoints"); // Gather the prev_batch tokens and create checkpoints for
    // our message crawler.

    await Promise.all(encryptedRooms.map(async room => {
      const timeline = room.getLiveTimeline();
      const token = timeline.getPaginationToken("b");
      console.log("EventIndex: Got token for indexer", room.roomId, token);
      const backCheckpoint = {
        roomId: room.roomId,
        token: token,
        direction: "b",
        fullCrawl: true
      };
      const forwardCheckpoint = {
        roomId: room.roomId,
        token: token,
        direction: "f"
      };

      try {
        if (backCheckpoint.token) {
          await indexManager.addCrawlerCheckpoint(backCheckpoint);
          this.crawlerCheckpoints.push(backCheckpoint);
        }

        if (forwardCheckpoint.token) {
          await indexManager.addCrawlerCheckpoint(forwardCheckpoint);
          this.crawlerCheckpoints.push(forwardCheckpoint);
        }
      } catch (e) {
        console.log("EventIndex: Error adding initial checkpoints for room", room.roomId, backCheckpoint, forwardCheckpoint, e);
      }
    }));
  }
  /*
   * The sync event listener.
   *
   * The listener has two cases:
   *     - First sync after start up, check if the index is empty, add
   *         initial checkpoints, if so. Start the crawler background task.
   *     - Every other sync, tell the event index to commit all the queued up
   *         live events
   */


  /**
   * Check if an event should be added to the event index.
   *
   * Most notably we filter events for which decryption failed, are redacted
   * or aren't of a type that we know how to index.
   *
   * @param {MatrixEvent} ev The event that should checked.
   * @returns {bool} Returns true if the event can be indexed, false
   * otherwise.
   */
  isValidEvent(ev) {
    const isUsefulType = ["m.room.message", "m.room.name", "m.room.topic"].includes(ev.getType());
    const validEventType = isUsefulType && !ev.isRedacted() && !ev.isDecryptionFailure();
    let validMsgType = true;

    if (ev.getType() === "m.room.message" && !ev.isRedacted()) {
      // Expand this if there are more invalid msgtypes.
      const msgtype = ev.getContent().msgtype;
      if (!msgtype) validMsgType = false;else validMsgType = !msgtype.startsWith("m.key.verification");
    }

    return validEventType && validMsgType;
  }
  /**
   * Queue up live events to be added to the event index.
   *
   * @param {MatrixEvent} ev The event that should be added to the index.
   */


  async addLiveEventToIndex(ev) {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    if (!this.isValidEvent(ev)) return;
    const jsonEvent = ev.toJSON();
    const e = ev.isEncrypted() ? jsonEvent.decrypted : jsonEvent;
    const profile = {
      displayname: ev.sender.rawDisplayName,
      avatar_url: ev.sender.getMxcAvatarUrl()
    };
    indexManager.addEventToIndex(e, profile);
  }
  /**
   * Emmit that the crawler has changed the checkpoint that it's currently
   * handling.
   */


  emitNewCheckpoint() {
    this.emit("changedCheckpoint", this.currentRoom());
  }
  /**
   * The main crawler loop.
   *
   * Goes through crawlerCheckpoints and fetches events from the server to be
   * added to the EventIndex.
   *
   * If a /room/{roomId}/messages request doesn't contain any events, stop the
   * crawl, otherwise create a new checkpoint and push it to the
   * crawlerCheckpoints queue so we go through them in a round-robin way.
   */


  async crawlerFunc() {
    let cancelled = false;
    console.log("EventIndex: Started crawler function");

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    this._crawler = {};

    this._crawler.cancel = () => {
      cancelled = true;
    };

    let idle = false;

    while (!cancelled) {
      let sleepTime = _SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, 'crawlerSleepTime'); // Don't let the user configure a lower sleep time than 100 ms.


      sleepTime = Math.max(sleepTime, 100);

      if (idle) {
        sleepTime = this._crawlerIdleTime;
      }

      if (this._currentCheckpoint !== null) {
        this._currentCheckpoint = null;
        this.emitNewCheckpoint();
      }

      await (0, _promise.sleep)(sleepTime);
      console.log("EventIndex: Running the crawler loop.");

      if (cancelled) {
        break;
      }

      const checkpoint = this.crawlerCheckpoints.shift(); /// There is no checkpoint available currently, one may appear if
      // a sync with limited room timelines happens, so go back to sleep.

      if (checkpoint === undefined) {
        idle = true;
        continue;
      }

      this._currentCheckpoint = checkpoint;
      this.emitNewCheckpoint();
      idle = false;
      console.log("EventIndex: crawling using checkpoint", checkpoint); // We have a checkpoint, let us fetch some messages, again, very
      // conservatively to not bother our homeserver too much.

      const eventMapper = client.getEventMapper(); // TODO we need to ensure to use member lazy loading with this
      // request so we get the correct profiles.

      let res;

      try {
        res = await client._createMessagesRequest(checkpoint.roomId, checkpoint.token, this._eventsPerCrawl, checkpoint.direction);
      } catch (e) {
        if (e.httpStatus === 403) {
          console.log("EventIndex: Removing checkpoint as we don't have ", "permissions to fetch messages from this room.", checkpoint);

          try {
            await indexManager.removeCrawlerCheckpoint(checkpoint);
          } catch (e) {
            console.log("EventIndex: Error removing checkpoint", checkpoint, e); // We don't push the checkpoint here back, it will
            // hopefully be removed after a restart. But let us
            // ignore it for now as we don't want to hammer the
            // endpoint.
          }

          continue;
        }

        console.log("EventIndex: Error crawling events:", e);
        this.crawlerCheckpoints.push(checkpoint);
        continue;
      }

      if (cancelled) {
        this.crawlerCheckpoints.push(checkpoint);
        break;
      }

      if (res.chunk.length === 0) {
        console.log("EventIndex: Done with the checkpoint", checkpoint); // We got to the start/end of our timeline, lets just
        // delete our checkpoint and go back to sleep.

        try {
          await indexManager.removeCrawlerCheckpoint(checkpoint);
        } catch (e) {
          console.log("EventIndex: Error removing checkpoint", checkpoint, e);
        }

        continue;
      } // Convert the plain JSON events into Matrix events so they get
      // decrypted if necessary.


      const matrixEvents = res.chunk.map(eventMapper);
      let stateEvents = [];

      if (res.state !== undefined) {
        stateEvents = res.state.map(eventMapper);
      }

      const profiles = {};
      stateEvents.forEach(ev => {
        if (ev.event.content && ev.event.content.membership === "join") {
          profiles[ev.event.sender] = {
            displayname: ev.event.content.displayname,
            avatar_url: ev.event.content.avatar_url
          };
        }
      });
      const decryptionPromises = [];
      matrixEvents.forEach(ev => {
        if (ev.isBeingDecrypted() || ev.isDecryptionFailure()) {
          // TODO the decryption promise is a private property, this
          // should either be made public or we should convert the
          // event that gets fired when decryption is done into a
          // promise using the once event emitter method:
          // https://nodejs.org/api/events.html#events_events_once_emitter_name
          decryptionPromises.push(ev._decryptionPromise);
        }
      }); // Let us wait for all the events to get decrypted.

      await Promise.all(decryptionPromises); // TODO if there are no events at this point we're missing a lot
      // decryption keys, do we want to retry this checkpoint at a later
      // stage?

      const filteredEvents = matrixEvents.filter(this.isValidEvent);
      const undecryptableEvents = matrixEvents.filter(ev => {
        return ev.isDecryptionFailure();
      }); // Collect the redaction events so we can delete the redacted events
      // from the index.

      const redactionEvents = matrixEvents.filter(ev => {
        return ev.getType() === "m.room.redaction";
      }); // Let us convert the events back into a format that EventIndex can
      // consume.

      const events = filteredEvents.map(ev => {
        const jsonEvent = ev.toJSON();
        const e = ev.isEncrypted() ? jsonEvent.decrypted : jsonEvent;
        let profile = {};
        if (e.sender in profiles) profile = profiles[e.sender];
        const object = {
          event: e,
          profile: profile
        };
        return object;
      }); // Create a new checkpoint so we can continue crawling the room for
      // messages.

      const newCheckpoint = {
        roomId: checkpoint.roomId,
        token: res.end,
        fullCrawl: checkpoint.fullCrawl,
        direction: checkpoint.direction
      };
      console.log("EventIndex: Crawled room", client.getRoom(checkpoint.roomId).name, "and fetched total", matrixEvents.length, "events of which", events.length, "are being added,", redactionEvents.length, "are redacted,", matrixEvents.length - events.length, "are being skipped, undecryptable", undecryptableEvents.length);

      try {
        for (let i = 0; i < redactionEvents.length; i++) {
          const ev = redactionEvents[i];
          await indexManager.deleteEvent(ev.getAssociatedId());
        }

        const eventsAlreadyAdded = await indexManager.addHistoricEvents(events, newCheckpoint, checkpoint); // If all events were already indexed we assume that we catched
        // up with our index and don't need to crawl the room further.
        // Let us delete the checkpoint in that case, otherwise push
        // the new checkpoint to be used by the crawler.

        if (eventsAlreadyAdded === true && newCheckpoint.fullCrawl !== true) {
          console.log("EventIndex: Checkpoint had already all events", "added, stopping the crawl", checkpoint);
          await indexManager.removeCrawlerCheckpoint(newCheckpoint);
        } else {
          this.crawlerCheckpoints.push(newCheckpoint);
        }
      } catch (e) {
        console.log("EventIndex: Error durring a crawl", e); // An error occurred, put the checkpoint back so we
        // can retry.

        this.crawlerCheckpoints.push(checkpoint);
      }
    }

    this._crawler = null;
    console.log("EventIndex: Stopping crawler function");
  }
  /**
   * Start the crawler background task.
   */


  startCrawler() {
    if (this._crawler !== null) return;
    this.crawlerFunc();
  }
  /**
   * Stop the crawler background task.
   */


  stopCrawler() {
    if (this._crawler === null) return;

    this._crawler.cancel();
  }
  /**
   * Close the event index.
   *
   * This removes all the MatrixClient event listeners, stops the crawler
   * task, and closes the index.
   */


  async close() {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    this.removeListeners();
    this.stopCrawler();
    await indexManager.closeEventIndex();
    return;
  }
  /**
   * Search the event index using the given term for matching events.
   *
   * @param {SearchArgs} searchArgs The search configuration for the search,
   * sets the search term and determines the search result contents.
   *
   * @return {Promise<[SearchResult]>} A promise that will resolve to an array
   * of search results once the search is done.
   */


  async search(searchArgs) {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    return indexManager.searchEventIndex(searchArgs);
  }
  /**
   * Load events that contain URLs from the event index.
   *
   * @param {Room} room The room for which we should fetch events containing
   * URLs
   *
   * @param {number} limit The maximum number of events to fetch.
   *
   * @param {string} fromEvent From which event should we continue fetching
   * events from the index. This is only needed if we're continuing to fill
   * the timeline, e.g. if we're paginating. This needs to be set to a event
   * id of an event that was previously fetched with this function.
   *
   * @param {string} direction The direction in which we will continue
   * fetching events. EventTimeline.BACKWARDS to continue fetching events that
   * are older than the event given in fromEvent, EventTimeline.FORWARDS to
   * fetch newer events.
   *
   * @returns {Promise<MatrixEvent[]>} Resolves to an array of events that
   * contain URLs.
   */


  async loadFileEvents(room, limit = 10, fromEvent = null, direction = _matrixJsSdk.EventTimeline.BACKWARDS) {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    const loadArgs = {
      roomId: room.roomId,
      limit: limit
    };

    if (fromEvent) {
      loadArgs.fromEvent = fromEvent;
      loadArgs.direction = direction;
    }

    let events; // Get our events from the event index.

    try {
      events = await indexManager.loadFileEvents(loadArgs);
    } catch (e) {
      console.log("EventIndex: Error getting file events", e);
      return [];
    }

    const eventMapper = client.getEventMapper(); // Turn the events into MatrixEvent objects.

    const matrixEvents = events.map(e => {
      const matrixEvent = eventMapper(e.event);
      const member = new _matrixJsSdk.RoomMember(room.roomId, matrixEvent.getSender()); // We can't really reconstruct the whole room state from our
      // EventIndex to calculate the correct display name. Use the
      // disambiguated form always instead.

      member.name = e.profile.displayname + " (" + matrixEvent.getSender() + ")"; // This is sets the avatar URL.

      const memberEvent = eventMapper({
        content: {
          membership: "join",
          avatar_url: e.profile.avatar_url,
          displayname: e.profile.displayname
        },
        type: "m.room.member",
        event_id: matrixEvent.getId() + ":eventIndex",
        room_id: matrixEvent.getRoomId(),
        sender: matrixEvent.getSender(),
        origin_server_ts: matrixEvent.getTs(),
        state_key: matrixEvent.getSender()
      }); // We set this manually to avoid emitting RoomMember.membership and
      // RoomMember.name events.

      member.events.member = memberEvent;
      matrixEvent.sender = member;
      return matrixEvent;
    });
    return matrixEvents;
  }
  /**
   * Fill a timeline with events that contain URLs.
   *
   * @param {TimelineSet} timelineSet The TimelineSet the Timeline belongs to,
   * used to check if we're adding duplicate events.
   *
   * @param {Timeline} timeline The Timeline which should be filed with
   * events.
   *
   * @param {Room} room The room for which we should fetch events containing
   * URLs
   *
   * @param {number} limit The maximum number of events to fetch.
   *
   * @param {string} fromEvent From which event should we continue fetching
   * events from the index. This is only needed if we're continuing to fill
   * the timeline, e.g. if we're paginating. This needs to be set to a event
   * id of an event that was previously fetched with this function.
   *
   * @param {string} direction The direction in which we will continue
   * fetching events. EventTimeline.BACKWARDS to continue fetching events that
   * are older than the event given in fromEvent, EventTimeline.FORWARDS to
   * fetch newer events.
   *
   * @returns {Promise<boolean>} Resolves to true if events were added to the
   * timeline, false otherwise.
   */


  async populateFileTimeline(timelineSet, timeline, room, limit = 10, fromEvent = null, direction = _matrixJsSdk.EventTimeline.BACKWARDS) {
    const matrixEvents = await this.loadFileEvents(room, limit, fromEvent, direction); // If this is a normal fill request, not a pagination request, we need
    // to get our events in the BACKWARDS direction but populate them in the
    // forwards direction.
    // This needs to happen because a fill request might come with an
    // exisitng timeline e.g. if you close and re-open the FilePanel.

    if (fromEvent === null) {
      matrixEvents.reverse();
      direction = direction == _matrixJsSdk.EventTimeline.BACKWARDS ? _matrixJsSdk.EventTimeline.FORWARDS : _matrixJsSdk.EventTimeline.BACKWARDS;
    } // Add the events to the timeline of the file panel.


    matrixEvents.forEach(e => {
      if (!timelineSet.eventIdToTimeline(e.getId())) {
        timelineSet.addEventToTimeline(e, timeline, direction == _matrixJsSdk.EventTimeline.BACKWARDS);
      }
    });
    let ret = false;
    let paginationToken = ""; // Set the pagination token to the oldest event that we retrieved.

    if (matrixEvents.length > 0) {
      paginationToken = matrixEvents[matrixEvents.length - 1].getId();
      ret = true;
    }

    console.log("EventIndex: Populating file panel with", matrixEvents.length, "events and setting the pagination token to", paginationToken);
    timeline.setPaginationToken(paginationToken, _matrixJsSdk.EventTimeline.BACKWARDS);
    return ret;
  }
  /**
   * Emulate a TimelineWindow pagination() request with the event index as the event source
   *
   * Might not fetch events from the index if the timeline already contains
   * events that the window isn't showing.
   *
   * @param {Room} room The room for which we should fetch events containing
   * URLs
   *
   * @param {TimelineWindow} timelineWindow The timeline window that should be
   * populated with new events.
   *
   * @param {string} direction The direction in which we should paginate.
   * EventTimeline.BACKWARDS to paginate back, EventTimeline.FORWARDS to
   * paginate forwards.
   *
   * @param {number} limit The maximum number of events to fetch while
   * paginating.
   *
   * @returns {Promise<boolean>} Resolves to a boolean which is true if more
   * events were successfully retrieved.
   */


  paginateTimelineWindow(room, timelineWindow, direction, limit) {
    const tl = timelineWindow.getTimelineIndex(direction);
    if (!tl) return Promise.resolve(false);
    if (tl.pendingPaginate) return tl.pendingPaginate;

    if (timelineWindow.extend(direction, limit)) {
      return Promise.resolve(true);
    }

    const paginationMethod = async (timelineWindow, timeline, room, direction, limit) => {
      const timelineSet = timelineWindow._timelineSet;
      const token = timeline.timeline.getPaginationToken(direction);
      const ret = await this.populateFileTimeline(timelineSet, timeline.timeline, room, limit, token, direction);
      timeline.pendingPaginate = null;
      timelineWindow.extend(direction, limit);
      return ret;
    };

    const paginationPromise = paginationMethod(timelineWindow, tl, room, direction, limit);
    tl.pendingPaginate = paginationPromise;
    return paginationPromise;
  }
  /**
   * Get statistical information of the index.
   *
   * @return {Promise<IndexStats>} A promise that will resolve to the index
   * statistics.
   */


  async getStats() {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    return indexManager.getStats();
  }
  /**
   * Get the room that we are currently crawling.
   *
   * @returns {Room} A MatrixRoom that is being currently crawled, null
   * if no room is currently being crawled.
   */


  currentRoom() {
    if (this._currentCheckpoint === null && this.crawlerCheckpoints.length === 0) {
      return null;
    }

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (this._currentCheckpoint !== null) {
      return client.getRoom(this._currentCheckpoint.roomId);
    } else {
      return client.getRoom(this.crawlerCheckpoints[0].roomId);
    }
  }

  crawlingRooms() {
    const totalRooms = new Set();
    const crawlingRooms = new Set();
    this.crawlerCheckpoints.forEach((checkpoint, index) => {
      crawlingRooms.add(checkpoint.roomId);
    });

    if (this._currentCheckpoint !== null) {
      crawlingRooms.add(this._currentCheckpoint.roomId);
    }

    const client = _MatrixClientPeg.MatrixClientPeg.get();

    const rooms = client.getRooms();

    const isRoomEncrypted = room => {
      return client.isRoomEncrypted(room.roomId);
    };

    const encryptedRooms = rooms.filter(isRoomEncrypted);
    encryptedRooms.forEach((room, index) => {
      totalRooms.add(room.roomId);
    });
    return {
      crawlingRooms,
      totalRooms
    };
  }

}

exports.default = EventIndex;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbmRleGluZy9FdmVudEluZGV4LmpzIl0sIm5hbWVzIjpbIkV2ZW50SW5kZXgiLCJFdmVudEVtaXR0ZXIiLCJjb25zdHJ1Y3RvciIsInN0YXRlIiwicHJldlN0YXRlIiwiZGF0YSIsImluZGV4TWFuYWdlciIsIlBsYXRmb3JtUGVnIiwiZ2V0IiwiZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIiLCJldmVudEluZGV4V2FzRW1wdHkiLCJpc0V2ZW50SW5kZXhFbXB0eSIsImFkZEluaXRpYWxDaGVja3BvaW50cyIsInN0YXJ0Q3Jhd2xlciIsImNvbnNvbGUiLCJsb2ciLCJjb21taXRMaXZlRXZlbnRzIiwiZXYiLCJyb29tIiwidG9TdGFydE9mVGltZWxpbmUiLCJyZW1vdmVkIiwiTWF0cml4Q2xpZW50UGVnIiwiaXNSb29tRW5jcnlwdGVkIiwicm9vbUlkIiwibGl2ZUV2ZW50IiwiaXNSZWRhY3RlZCIsImlzQmVpbmdEZWNyeXB0ZWQiLCJldmVudElkIiwiZ2V0SWQiLCJsaXZlRXZlbnRzRm9ySW5kZXgiLCJhZGQiLCJhZGRMaXZlRXZlbnRUb0luZGV4IiwiZXJyIiwiZGVsZXRlIiwiZGVsZXRlRXZlbnQiLCJnZXRBc3NvY2lhdGVkSWQiLCJlIiwidGltZWxpbmVTZXQiLCJyZXNldEFsbFRpbWVsaW5lcyIsInRpbWVsaW5lIiwiZ2V0TGl2ZVRpbWVsaW5lIiwidG9rZW4iLCJnZXRQYWdpbmF0aW9uVG9rZW4iLCJiYWNrd2FyZHNDaGVja3BvaW50IiwiZnVsbENyYXdsIiwiZGlyZWN0aW9uIiwiYWRkQ3Jhd2xlckNoZWNrcG9pbnQiLCJjcmF3bGVyQ2hlY2twb2ludHMiLCJwdXNoIiwiX2NyYXdsZXJJZGxlVGltZSIsIl9ldmVudHNQZXJDcmF3bCIsIl9jcmF3bGVyIiwiX2N1cnJlbnRDaGVja3BvaW50IiwiU2V0IiwiaW5pdCIsImluaXRFdmVudEluZGV4IiwibG9hZENoZWNrcG9pbnRzIiwicmVnaXN0ZXJMaXN0ZW5lcnMiLCJjbGllbnQiLCJvbiIsIm9uU3luYyIsIm9uUm9vbVRpbWVsaW5lIiwib25FdmVudERlY3J5cHRlZCIsIm9uVGltZWxpbmVSZXNldCIsIm9uUmVkYWN0aW9uIiwicmVtb3ZlTGlzdGVuZXJzIiwicmVtb3ZlTGlzdGVuZXIiLCJyb29tcyIsImdldFJvb21zIiwiZW5jcnlwdGVkUm9vbXMiLCJmaWx0ZXIiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiYmFja0NoZWNrcG9pbnQiLCJmb3J3YXJkQ2hlY2twb2ludCIsImlzVmFsaWRFdmVudCIsImlzVXNlZnVsVHlwZSIsImluY2x1ZGVzIiwiZ2V0VHlwZSIsInZhbGlkRXZlbnRUeXBlIiwiaXNEZWNyeXB0aW9uRmFpbHVyZSIsInZhbGlkTXNnVHlwZSIsIm1zZ3R5cGUiLCJnZXRDb250ZW50Iiwic3RhcnRzV2l0aCIsImpzb25FdmVudCIsInRvSlNPTiIsImlzRW5jcnlwdGVkIiwiZGVjcnlwdGVkIiwicHJvZmlsZSIsImRpc3BsYXluYW1lIiwic2VuZGVyIiwicmF3RGlzcGxheU5hbWUiLCJhdmF0YXJfdXJsIiwiZ2V0TXhjQXZhdGFyVXJsIiwiYWRkRXZlbnRUb0luZGV4IiwiZW1pdE5ld0NoZWNrcG9pbnQiLCJlbWl0IiwiY3VycmVudFJvb20iLCJjcmF3bGVyRnVuYyIsImNhbmNlbGxlZCIsImNhbmNlbCIsImlkbGUiLCJzbGVlcFRpbWUiLCJTZXR0aW5nc1N0b3JlIiwiZ2V0VmFsdWVBdCIsIlNldHRpbmdMZXZlbCIsIkRFVklDRSIsIk1hdGgiLCJtYXgiLCJjaGVja3BvaW50Iiwic2hpZnQiLCJ1bmRlZmluZWQiLCJldmVudE1hcHBlciIsImdldEV2ZW50TWFwcGVyIiwicmVzIiwiX2NyZWF0ZU1lc3NhZ2VzUmVxdWVzdCIsImh0dHBTdGF0dXMiLCJyZW1vdmVDcmF3bGVyQ2hlY2twb2ludCIsImNodW5rIiwibGVuZ3RoIiwibWF0cml4RXZlbnRzIiwic3RhdGVFdmVudHMiLCJwcm9maWxlcyIsImZvckVhY2giLCJldmVudCIsImNvbnRlbnQiLCJtZW1iZXJzaGlwIiwiZGVjcnlwdGlvblByb21pc2VzIiwiX2RlY3J5cHRpb25Qcm9taXNlIiwiZmlsdGVyZWRFdmVudHMiLCJ1bmRlY3J5cHRhYmxlRXZlbnRzIiwicmVkYWN0aW9uRXZlbnRzIiwiZXZlbnRzIiwib2JqZWN0IiwibmV3Q2hlY2twb2ludCIsImVuZCIsImdldFJvb20iLCJuYW1lIiwiaSIsImV2ZW50c0FscmVhZHlBZGRlZCIsImFkZEhpc3RvcmljRXZlbnRzIiwic3RvcENyYXdsZXIiLCJjbG9zZSIsImNsb3NlRXZlbnRJbmRleCIsInNlYXJjaCIsInNlYXJjaEFyZ3MiLCJzZWFyY2hFdmVudEluZGV4IiwibG9hZEZpbGVFdmVudHMiLCJsaW1pdCIsImZyb21FdmVudCIsIkV2ZW50VGltZWxpbmUiLCJCQUNLV0FSRFMiLCJsb2FkQXJncyIsIm1hdHJpeEV2ZW50IiwibWVtYmVyIiwiUm9vbU1lbWJlciIsImdldFNlbmRlciIsIm1lbWJlckV2ZW50IiwidHlwZSIsImV2ZW50X2lkIiwicm9vbV9pZCIsImdldFJvb21JZCIsIm9yaWdpbl9zZXJ2ZXJfdHMiLCJnZXRUcyIsInN0YXRlX2tleSIsInBvcHVsYXRlRmlsZVRpbWVsaW5lIiwicmV2ZXJzZSIsIkZPUldBUkRTIiwiZXZlbnRJZFRvVGltZWxpbmUiLCJhZGRFdmVudFRvVGltZWxpbmUiLCJyZXQiLCJwYWdpbmF0aW9uVG9rZW4iLCJzZXRQYWdpbmF0aW9uVG9rZW4iLCJwYWdpbmF0ZVRpbWVsaW5lV2luZG93IiwidGltZWxpbmVXaW5kb3ciLCJ0bCIsImdldFRpbWVsaW5lSW5kZXgiLCJyZXNvbHZlIiwicGVuZGluZ1BhZ2luYXRlIiwiZXh0ZW5kIiwicGFnaW5hdGlvbk1ldGhvZCIsIl90aW1lbGluZVNldCIsInBhZ2luYXRpb25Qcm9taXNlIiwiZ2V0U3RhdHMiLCJjcmF3bGluZ1Jvb21zIiwidG90YWxSb29tcyIsImluZGV4Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQTs7O0FBR2UsTUFBTUEsVUFBTixTQUF5QkMsb0JBQXpCLENBQXNDO0FBQ2pEQyxFQUFBQSxXQUFXLEdBQUc7QUFDVjtBQURVLGtEQXVITCxPQUFPQyxLQUFQLEVBQWNDLFNBQWQsRUFBeUJDLElBQXpCLEtBQWtDO0FBQ3ZDLFlBQU1DLFlBQVksR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHVCQUFsQixFQUFyQjs7QUFFQSxVQUFJTCxTQUFTLEtBQUssVUFBZCxJQUE0QkQsS0FBSyxLQUFLLFNBQTFDLEVBQXFEO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBLGNBQU1PLGtCQUFrQixHQUFHLE1BQU1KLFlBQVksQ0FBQ0ssaUJBQWIsRUFBakM7QUFDQSxZQUFJRCxrQkFBSixFQUF3QixNQUFNLEtBQUtFLHFCQUFMLEVBQU47QUFFeEIsYUFBS0MsWUFBTDtBQUNBO0FBQ0g7O0FBRUQsVUFBSVQsU0FBUyxLQUFLLFNBQWQsSUFBMkJELEtBQUssS0FBSyxTQUF6QyxFQUFvRDtBQUNoRDtBQUNBO0FBQ0FXLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLCtCQUFaO0FBQ0EsY0FBTVQsWUFBWSxDQUFDVSxnQkFBYixFQUFOO0FBQ0E7QUFDSDtBQUNKLEtBNUlhO0FBQUEsMERBc0pHLE9BQU9DLEVBQVAsRUFBV0MsSUFBWCxFQUFpQkMsaUJBQWpCLEVBQW9DQyxPQUFwQyxFQUE2Q2YsSUFBN0MsS0FBc0Q7QUFDbkU7QUFDQSxVQUFJLENBQUNnQixpQ0FBZ0JiLEdBQWhCLEdBQXNCYyxlQUF0QixDQUFzQ0osSUFBSSxDQUFDSyxNQUEzQyxDQUFMLEVBQXlELE9BRlUsQ0FJbkU7QUFDQTs7QUFDQSxVQUFJSixpQkFBaUIsSUFBSSxDQUFDZCxJQUF0QixJQUE4QixDQUFDQSxJQUFJLENBQUNtQixTQUFwQyxJQUNHUCxFQUFFLENBQUNRLFVBQUgsRUFEUCxFQUN3QjtBQUNwQjtBQUNILE9BVGtFLENBV25FO0FBQ0E7OztBQUNBLFVBQUlSLEVBQUUsQ0FBQ1MsZ0JBQUgsRUFBSixFQUEyQjtBQUN2QixjQUFNQyxPQUFPLEdBQUdWLEVBQUUsQ0FBQ1csS0FBSCxFQUFoQjtBQUNBLGFBQUtDLGtCQUFMLENBQXdCQyxHQUF4QixDQUE0QkgsT0FBNUI7QUFDSCxPQUhELE1BR087QUFDSDtBQUNBO0FBQ0EsY0FBTSxLQUFLSSxtQkFBTCxDQUF5QmQsRUFBekIsQ0FBTjtBQUNIO0FBQ0osS0EzS2E7QUFBQSw0REFtTEssT0FBT0EsRUFBUCxFQUFXZSxHQUFYLEtBQW1CO0FBQ2xDLFlBQU1MLE9BQU8sR0FBR1YsRUFBRSxDQUFDVyxLQUFILEVBQWhCLENBRGtDLENBR2xDOztBQUNBLFVBQUksQ0FBQyxLQUFLQyxrQkFBTCxDQUF3QkksTUFBeEIsQ0FBK0JOLE9BQS9CLENBQUwsRUFBOEM7QUFDOUMsVUFBSUssR0FBSixFQUFTO0FBQ1QsWUFBTSxLQUFLRCxtQkFBTCxDQUF5QmQsRUFBekIsQ0FBTjtBQUNILEtBMUxhO0FBQUEsdURBaU1BLE9BQU9BLEVBQVAsRUFBV0MsSUFBWCxLQUFvQjtBQUM5QjtBQUNBLFVBQUksQ0FBQ0csaUNBQWdCYixHQUFoQixHQUFzQmMsZUFBdEIsQ0FBc0NKLElBQUksQ0FBQ0ssTUFBM0MsQ0FBTCxFQUF5RDs7QUFDekQsWUFBTWpCLFlBQVksR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHVCQUFsQixFQUFyQjs7QUFFQSxVQUFJO0FBQ0EsY0FBTUgsWUFBWSxDQUFDNEIsV0FBYixDQUF5QmpCLEVBQUUsQ0FBQ2tCLGVBQUgsRUFBekIsQ0FBTjtBQUNILE9BRkQsQ0FFRSxPQUFPQyxDQUFQLEVBQVU7QUFDUnRCLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLDZDQUFaLEVBQTJEcUIsQ0FBM0Q7QUFDSDtBQUNKLEtBM01hO0FBQUEsMkRBbU5JLE9BQU9sQixJQUFQLEVBQWFtQixXQUFiLEVBQTBCQyxpQkFBMUIsS0FBZ0Q7QUFDOUQsVUFBSXBCLElBQUksS0FBSyxJQUFiLEVBQW1COztBQUVuQixZQUFNWixZQUFZLEdBQUdDLHFCQUFZQyxHQUFaLEdBQWtCQyx1QkFBbEIsRUFBckI7O0FBQ0EsVUFBSSxDQUFDWSxpQ0FBZ0JiLEdBQWhCLEdBQXNCYyxlQUF0QixDQUFzQ0osSUFBSSxDQUFDSyxNQUEzQyxDQUFMLEVBQXlEO0FBRXpELFlBQU1nQixRQUFRLEdBQUdyQixJQUFJLENBQUNzQixlQUFMLEVBQWpCO0FBQ0EsWUFBTUMsS0FBSyxHQUFHRixRQUFRLENBQUNHLGtCQUFULENBQTRCLEdBQTVCLENBQWQ7QUFFQSxZQUFNQyxtQkFBbUIsR0FBRztBQUN4QnBCLFFBQUFBLE1BQU0sRUFBRUwsSUFBSSxDQUFDSyxNQURXO0FBRXhCa0IsUUFBQUEsS0FBSyxFQUFFQSxLQUZpQjtBQUd4QkcsUUFBQUEsU0FBUyxFQUFFLEtBSGE7QUFJeEJDLFFBQUFBLFNBQVMsRUFBRTtBQUphLE9BQTVCO0FBT0EvQixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSw0REFBWixFQUNJNEIsbUJBREo7QUFHQSxZQUFNckMsWUFBWSxDQUFDd0Msb0JBQWIsQ0FBa0NILG1CQUFsQyxDQUFOO0FBRUEsV0FBS0ksa0JBQUwsQ0FBd0JDLElBQXhCLENBQTZCTCxtQkFBN0I7QUFDSCxLQXpPYTtBQUVWLFNBQUtJLGtCQUFMLEdBQTBCLEVBQTFCLENBRlUsQ0FHVjtBQUNBOztBQUNBLFNBQUtFLGdCQUFMLEdBQXdCLElBQXhCLENBTFUsQ0FNVjtBQUNBOztBQUNBLFNBQUtDLGVBQUwsR0FBdUIsR0FBdkI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsU0FBS0Msa0JBQUwsR0FBMEIsSUFBMUI7QUFDQSxTQUFLdkIsa0JBQUwsR0FBMEIsSUFBSXdCLEdBQUosRUFBMUI7QUFDSDs7QUFFRCxRQUFNQyxJQUFOLEdBQWE7QUFDVCxVQUFNaEQsWUFBWSxHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLEVBQXJCOztBQUVBLFVBQU1ILFlBQVksQ0FBQ2lELGNBQWIsRUFBTjtBQUNBekMsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksc0RBQVo7QUFFQSxTQUFLZ0Msa0JBQUwsR0FBMEIsTUFBTXpDLFlBQVksQ0FBQ2tELGVBQWIsRUFBaEM7QUFDQTFDLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdDQUFaLEVBQThDLEtBQUtnQyxrQkFBbkQ7QUFFQSxTQUFLVSxpQkFBTDtBQUNIO0FBRUQ7Ozs7O0FBR0FBLEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFVBQU1DLE1BQU0sR0FBR3JDLGlDQUFnQmIsR0FBaEIsRUFBZjs7QUFFQWtELElBQUFBLE1BQU0sQ0FBQ0MsRUFBUCxDQUFVLE1BQVYsRUFBa0IsS0FBS0MsTUFBdkI7QUFDQUYsSUFBQUEsTUFBTSxDQUFDQyxFQUFQLENBQVUsZUFBVixFQUEyQixLQUFLRSxjQUFoQztBQUNBSCxJQUFBQSxNQUFNLENBQUNDLEVBQVAsQ0FBVSxpQkFBVixFQUE2QixLQUFLRyxnQkFBbEM7QUFDQUosSUFBQUEsTUFBTSxDQUFDQyxFQUFQLENBQVUsb0JBQVYsRUFBZ0MsS0FBS0ksZUFBckM7QUFDQUwsSUFBQUEsTUFBTSxDQUFDQyxFQUFQLENBQVUsZ0JBQVYsRUFBNEIsS0FBS0ssV0FBakM7QUFDSDtBQUVEOzs7OztBQUdBQyxFQUFBQSxlQUFlLEdBQUc7QUFDZCxVQUFNUCxNQUFNLEdBQUdyQyxpQ0FBZ0JiLEdBQWhCLEVBQWY7O0FBQ0EsUUFBSWtELE1BQU0sS0FBSyxJQUFmLEVBQXFCO0FBRXJCQSxJQUFBQSxNQUFNLENBQUNRLGNBQVAsQ0FBc0IsTUFBdEIsRUFBOEIsS0FBS04sTUFBbkM7QUFDQUYsSUFBQUEsTUFBTSxDQUFDUSxjQUFQLENBQXNCLGVBQXRCLEVBQXVDLEtBQUtMLGNBQTVDO0FBQ0FILElBQUFBLE1BQU0sQ0FBQ1EsY0FBUCxDQUFzQixpQkFBdEIsRUFBeUMsS0FBS0osZ0JBQTlDO0FBQ0FKLElBQUFBLE1BQU0sQ0FBQ1EsY0FBUCxDQUFzQixvQkFBdEIsRUFBNEMsS0FBS0gsZUFBakQ7QUFDQUwsSUFBQUEsTUFBTSxDQUFDUSxjQUFQLENBQXNCLGdCQUF0QixFQUF3QyxLQUFLRixXQUE3QztBQUNIO0FBRUQ7Ozs7O0FBR0EsUUFBTXBELHFCQUFOLEdBQThCO0FBQzFCLFVBQU1OLFlBQVksR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHVCQUFsQixFQUFyQjs7QUFDQSxVQUFNaUQsTUFBTSxHQUFHckMsaUNBQWdCYixHQUFoQixFQUFmOztBQUNBLFVBQU0yRCxLQUFLLEdBQUdULE1BQU0sQ0FBQ1UsUUFBUCxFQUFkOztBQUVBLFVBQU05QyxlQUFlLEdBQUlKLElBQUQsSUFBVTtBQUM5QixhQUFPd0MsTUFBTSxDQUFDcEMsZUFBUCxDQUF1QkosSUFBSSxDQUFDSyxNQUE1QixDQUFQO0FBQ0gsS0FGRCxDQUwwQixDQVMxQjtBQUNBOzs7QUFDQSxVQUFNOEMsY0FBYyxHQUFHRixLQUFLLENBQUNHLE1BQU4sQ0FBYWhELGVBQWIsQ0FBdkI7QUFFQVIsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksZ0RBQVosRUFiMEIsQ0FlMUI7QUFDQTs7QUFDQSxVQUFNd0QsT0FBTyxDQUFDQyxHQUFSLENBQVlILGNBQWMsQ0FBQ0ksR0FBZixDQUFtQixNQUFPdkQsSUFBUCxJQUFnQjtBQUNqRCxZQUFNcUIsUUFBUSxHQUFHckIsSUFBSSxDQUFDc0IsZUFBTCxFQUFqQjtBQUNBLFlBQU1DLEtBQUssR0FBR0YsUUFBUSxDQUFDRyxrQkFBVCxDQUE0QixHQUE1QixDQUFkO0FBRUE1QixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxtQ0FBWixFQUNZRyxJQUFJLENBQUNLLE1BRGpCLEVBQ3lCa0IsS0FEekI7QUFHQSxZQUFNaUMsY0FBYyxHQUFHO0FBQ25CbkQsUUFBQUEsTUFBTSxFQUFFTCxJQUFJLENBQUNLLE1BRE07QUFFbkJrQixRQUFBQSxLQUFLLEVBQUVBLEtBRlk7QUFHbkJJLFFBQUFBLFNBQVMsRUFBRSxHQUhRO0FBSW5CRCxRQUFBQSxTQUFTLEVBQUU7QUFKUSxPQUF2QjtBQU9BLFlBQU0rQixpQkFBaUIsR0FBRztBQUN0QnBELFFBQUFBLE1BQU0sRUFBRUwsSUFBSSxDQUFDSyxNQURTO0FBRXRCa0IsUUFBQUEsS0FBSyxFQUFFQSxLQUZlO0FBR3RCSSxRQUFBQSxTQUFTLEVBQUU7QUFIVyxPQUExQjs7QUFNQSxVQUFJO0FBQ0EsWUFBSTZCLGNBQWMsQ0FBQ2pDLEtBQW5CLEVBQTBCO0FBQ3RCLGdCQUFNbkMsWUFBWSxDQUFDd0Msb0JBQWIsQ0FBa0M0QixjQUFsQyxDQUFOO0FBQ0EsZUFBSzNCLGtCQUFMLENBQXdCQyxJQUF4QixDQUE2QjBCLGNBQTdCO0FBQ0g7O0FBRUQsWUFBSUMsaUJBQWlCLENBQUNsQyxLQUF0QixFQUE2QjtBQUN6QixnQkFBTW5DLFlBQVksQ0FBQ3dDLG9CQUFiLENBQWtDNkIsaUJBQWxDLENBQU47QUFDQSxlQUFLNUIsa0JBQUwsQ0FBd0JDLElBQXhCLENBQTZCMkIsaUJBQTdCO0FBQ0g7QUFDSixPQVZELENBVUUsT0FBT3ZDLENBQVAsRUFBVTtBQUNSdEIsUUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksdURBQVosRUFDWUcsSUFBSSxDQUFDSyxNQURqQixFQUN5Qm1ELGNBRHpCLEVBQ3lDQyxpQkFEekMsRUFDNER2QyxDQUQ1RDtBQUVIO0FBQ0osS0FsQ2lCLENBQVosQ0FBTjtBQW1DSDtBQUVEOzs7Ozs7Ozs7OztBQTZIQTs7Ozs7Ozs7OztBQVVBd0MsRUFBQUEsWUFBWSxDQUFDM0QsRUFBRCxFQUFLO0FBQ2IsVUFBTTRELFlBQVksR0FBRyxDQUFDLGdCQUFELEVBQW1CLGFBQW5CLEVBQWtDLGNBQWxDLEVBQWtEQyxRQUFsRCxDQUEyRDdELEVBQUUsQ0FBQzhELE9BQUgsRUFBM0QsQ0FBckI7QUFDQSxVQUFNQyxjQUFjLEdBQUdILFlBQVksSUFBSSxDQUFDNUQsRUFBRSxDQUFDUSxVQUFILEVBQWpCLElBQW9DLENBQUNSLEVBQUUsQ0FBQ2dFLG1CQUFILEVBQTVEO0FBRUEsUUFBSUMsWUFBWSxHQUFHLElBQW5COztBQUVBLFFBQUlqRSxFQUFFLENBQUM4RCxPQUFILE9BQWlCLGdCQUFqQixJQUFxQyxDQUFDOUQsRUFBRSxDQUFDUSxVQUFILEVBQTFDLEVBQTJEO0FBQ3ZEO0FBQ0EsWUFBTTBELE9BQU8sR0FBR2xFLEVBQUUsQ0FBQ21FLFVBQUgsR0FBZ0JELE9BQWhDO0FBRUEsVUFBSSxDQUFDQSxPQUFMLEVBQWNELFlBQVksR0FBRyxLQUFmLENBQWQsS0FDS0EsWUFBWSxHQUFHLENBQUNDLE9BQU8sQ0FBQ0UsVUFBUixDQUFtQixvQkFBbkIsQ0FBaEI7QUFDUjs7QUFFRCxXQUFPTCxjQUFjLElBQUlFLFlBQXpCO0FBQ0g7QUFFRDs7Ozs7OztBQUtBLFFBQU1uRCxtQkFBTixDQUEwQmQsRUFBMUIsRUFBOEI7QUFDMUIsVUFBTVgsWUFBWSxHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLEVBQXJCOztBQUVBLFFBQUksQ0FBQyxLQUFLbUUsWUFBTCxDQUFrQjNELEVBQWxCLENBQUwsRUFBNEI7QUFFNUIsVUFBTXFFLFNBQVMsR0FBR3JFLEVBQUUsQ0FBQ3NFLE1BQUgsRUFBbEI7QUFDQSxVQUFNbkQsQ0FBQyxHQUFHbkIsRUFBRSxDQUFDdUUsV0FBSCxLQUFtQkYsU0FBUyxDQUFDRyxTQUE3QixHQUF5Q0gsU0FBbkQ7QUFFQSxVQUFNSSxPQUFPLEdBQUc7QUFDWkMsTUFBQUEsV0FBVyxFQUFFMUUsRUFBRSxDQUFDMkUsTUFBSCxDQUFVQyxjQURYO0FBRVpDLE1BQUFBLFVBQVUsRUFBRTdFLEVBQUUsQ0FBQzJFLE1BQUgsQ0FBVUcsZUFBVjtBQUZBLEtBQWhCO0FBS0F6RixJQUFBQSxZQUFZLENBQUMwRixlQUFiLENBQTZCNUQsQ0FBN0IsRUFBZ0NzRCxPQUFoQztBQUNIO0FBRUQ7Ozs7OztBQUlBTyxFQUFBQSxpQkFBaUIsR0FBRztBQUNoQixTQUFLQyxJQUFMLENBQVUsbUJBQVYsRUFBK0IsS0FBS0MsV0FBTCxFQUEvQjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVBLFFBQU1DLFdBQU4sR0FBb0I7QUFDaEIsUUFBSUMsU0FBUyxHQUFHLEtBQWhCO0FBRUF2RixJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxzQ0FBWjs7QUFFQSxVQUFNMkMsTUFBTSxHQUFHckMsaUNBQWdCYixHQUFoQixFQUFmOztBQUNBLFVBQU1GLFlBQVksR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHVCQUFsQixFQUFyQjs7QUFFQSxTQUFLMEMsUUFBTCxHQUFnQixFQUFoQjs7QUFFQSxTQUFLQSxRQUFMLENBQWNtRCxNQUFkLEdBQXVCLE1BQU07QUFDekJELE1BQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0gsS0FGRDs7QUFJQSxRQUFJRSxJQUFJLEdBQUcsS0FBWDs7QUFFQSxXQUFPLENBQUNGLFNBQVIsRUFBbUI7QUFDZixVQUFJRyxTQUFTLEdBQUdDLHVCQUFjQyxVQUFkLENBQXlCQyw0QkFBYUMsTUFBdEMsRUFBOEMsa0JBQTlDLENBQWhCLENBRGUsQ0FHZjs7O0FBQ0FKLE1BQUFBLFNBQVMsR0FBR0ssSUFBSSxDQUFDQyxHQUFMLENBQVNOLFNBQVQsRUFBb0IsR0FBcEIsQ0FBWjs7QUFFQSxVQUFJRCxJQUFKLEVBQVU7QUFDTkMsUUFBQUEsU0FBUyxHQUFHLEtBQUt2RCxnQkFBakI7QUFDSDs7QUFFRCxVQUFJLEtBQUtHLGtCQUFMLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDLGFBQUtBLGtCQUFMLEdBQTBCLElBQTFCO0FBQ0EsYUFBSzZDLGlCQUFMO0FBQ0g7O0FBRUQsWUFBTSxvQkFBTU8sU0FBTixDQUFOO0FBRUExRixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSx1Q0FBWjs7QUFFQSxVQUFJc0YsU0FBSixFQUFlO0FBQ1g7QUFDSDs7QUFFRCxZQUFNVSxVQUFVLEdBQUcsS0FBS2hFLGtCQUFMLENBQXdCaUUsS0FBeEIsRUFBbkIsQ0F2QmUsQ0F5QmY7QUFDQTs7QUFDQSxVQUFJRCxVQUFVLEtBQUtFLFNBQW5CLEVBQThCO0FBQzFCVixRQUFBQSxJQUFJLEdBQUcsSUFBUDtBQUNBO0FBQ0g7O0FBRUQsV0FBS25ELGtCQUFMLEdBQTBCMkQsVUFBMUI7QUFDQSxXQUFLZCxpQkFBTDtBQUVBTSxNQUFBQSxJQUFJLEdBQUcsS0FBUDtBQUVBekYsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksdUNBQVosRUFBcURnRyxVQUFyRCxFQXJDZSxDQXVDZjtBQUNBOztBQUNBLFlBQU1HLFdBQVcsR0FBR3hELE1BQU0sQ0FBQ3lELGNBQVAsRUFBcEIsQ0F6Q2UsQ0EwQ2Y7QUFDQTs7QUFDQSxVQUFJQyxHQUFKOztBQUVBLFVBQUk7QUFDQUEsUUFBQUEsR0FBRyxHQUFHLE1BQU0xRCxNQUFNLENBQUMyRCxzQkFBUCxDQUNSTixVQUFVLENBQUN4RixNQURILEVBQ1d3RixVQUFVLENBQUN0RSxLQUR0QixFQUM2QixLQUFLUyxlQURsQyxFQUVSNkQsVUFBVSxDQUFDbEUsU0FGSCxDQUFaO0FBR0gsT0FKRCxDQUlFLE9BQU9ULENBQVAsRUFBVTtBQUNSLFlBQUlBLENBQUMsQ0FBQ2tGLFVBQUYsS0FBaUIsR0FBckIsRUFBMEI7QUFDdEJ4RyxVQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxtREFBWixFQUNZLCtDQURaLEVBQzZEZ0csVUFEN0Q7O0FBRUEsY0FBSTtBQUNBLGtCQUFNekcsWUFBWSxDQUFDaUgsdUJBQWIsQ0FBcUNSLFVBQXJDLENBQU47QUFDSCxXQUZELENBRUUsT0FBTzNFLENBQVAsRUFBVTtBQUNSdEIsWUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksdUNBQVosRUFBcURnRyxVQUFyRCxFQUFpRTNFLENBQWpFLEVBRFEsQ0FFUjtBQUNBO0FBQ0E7QUFDQTtBQUNIOztBQUNEO0FBQ0g7O0FBRUR0QixRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxvQ0FBWixFQUFrRHFCLENBQWxEO0FBQ0EsYUFBS1csa0JBQUwsQ0FBd0JDLElBQXhCLENBQTZCK0QsVUFBN0I7QUFDQTtBQUNIOztBQUVELFVBQUlWLFNBQUosRUFBZTtBQUNYLGFBQUt0RCxrQkFBTCxDQUF3QkMsSUFBeEIsQ0FBNkIrRCxVQUE3QjtBQUNBO0FBQ0g7O0FBRUQsVUFBSUssR0FBRyxDQUFDSSxLQUFKLENBQVVDLE1BQVYsS0FBcUIsQ0FBekIsRUFBNEI7QUFDeEIzRyxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxzQ0FBWixFQUFvRGdHLFVBQXBELEVBRHdCLENBRXhCO0FBQ0E7O0FBQ0EsWUFBSTtBQUNBLGdCQUFNekcsWUFBWSxDQUFDaUgsdUJBQWIsQ0FBcUNSLFVBQXJDLENBQU47QUFDSCxTQUZELENBRUUsT0FBTzNFLENBQVAsRUFBVTtBQUNSdEIsVUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksdUNBQVosRUFBcURnRyxVQUFyRCxFQUFpRTNFLENBQWpFO0FBQ0g7O0FBQ0Q7QUFDSCxPQXRGYyxDQXdGZjtBQUNBOzs7QUFDQSxZQUFNc0YsWUFBWSxHQUFHTixHQUFHLENBQUNJLEtBQUosQ0FBVS9DLEdBQVYsQ0FBY3lDLFdBQWQsQ0FBckI7QUFDQSxVQUFJUyxXQUFXLEdBQUcsRUFBbEI7O0FBQ0EsVUFBSVAsR0FBRyxDQUFDakgsS0FBSixLQUFjOEcsU0FBbEIsRUFBNkI7QUFDekJVLFFBQUFBLFdBQVcsR0FBR1AsR0FBRyxDQUFDakgsS0FBSixDQUFVc0UsR0FBVixDQUFjeUMsV0FBZCxDQUFkO0FBQ0g7O0FBRUQsWUFBTVUsUUFBUSxHQUFHLEVBQWpCO0FBRUFELE1BQUFBLFdBQVcsQ0FBQ0UsT0FBWixDQUFvQjVHLEVBQUUsSUFBSTtBQUN0QixZQUFJQSxFQUFFLENBQUM2RyxLQUFILENBQVNDLE9BQVQsSUFDQTlHLEVBQUUsQ0FBQzZHLEtBQUgsQ0FBU0MsT0FBVCxDQUFpQkMsVUFBakIsS0FBZ0MsTUFEcEMsRUFDNEM7QUFDeENKLFVBQUFBLFFBQVEsQ0FBQzNHLEVBQUUsQ0FBQzZHLEtBQUgsQ0FBU2xDLE1BQVYsQ0FBUixHQUE0QjtBQUN4QkQsWUFBQUEsV0FBVyxFQUFFMUUsRUFBRSxDQUFDNkcsS0FBSCxDQUFTQyxPQUFULENBQWlCcEMsV0FETjtBQUV4QkcsWUFBQUEsVUFBVSxFQUFFN0UsRUFBRSxDQUFDNkcsS0FBSCxDQUFTQyxPQUFULENBQWlCakM7QUFGTCxXQUE1QjtBQUlIO0FBQ0osT0FSRDtBQVVBLFlBQU1tQyxrQkFBa0IsR0FBRyxFQUEzQjtBQUVBUCxNQUFBQSxZQUFZLENBQUNHLE9BQWIsQ0FBcUI1RyxFQUFFLElBQUk7QUFDdkIsWUFBSUEsRUFBRSxDQUFDUyxnQkFBSCxNQUF5QlQsRUFBRSxDQUFDZ0UsbUJBQUgsRUFBN0IsRUFBdUQ7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBZ0QsVUFBQUEsa0JBQWtCLENBQUNqRixJQUFuQixDQUF3Qi9CLEVBQUUsQ0FBQ2lILGtCQUEzQjtBQUNIO0FBQ0osT0FURCxFQTlHZSxDQXlIZjs7QUFDQSxZQUFNM0QsT0FBTyxDQUFDQyxHQUFSLENBQVl5RCxrQkFBWixDQUFOLENBMUhlLENBNEhmO0FBQ0E7QUFDQTs7QUFDQSxZQUFNRSxjQUFjLEdBQUdULFlBQVksQ0FBQ3BELE1BQWIsQ0FBb0IsS0FBS00sWUFBekIsQ0FBdkI7QUFDQSxZQUFNd0QsbUJBQW1CLEdBQUdWLFlBQVksQ0FBQ3BELE1BQWIsQ0FBcUJyRCxFQUFELElBQVE7QUFDcEQsZUFBT0EsRUFBRSxDQUFDZ0UsbUJBQUgsRUFBUDtBQUNILE9BRjJCLENBQTVCLENBaEllLENBb0lmO0FBQ0E7O0FBQ0EsWUFBTW9ELGVBQWUsR0FBR1gsWUFBWSxDQUFDcEQsTUFBYixDQUFxQnJELEVBQUQsSUFBUTtBQUNoRCxlQUFPQSxFQUFFLENBQUM4RCxPQUFILE9BQWlCLGtCQUF4QjtBQUNILE9BRnVCLENBQXhCLENBdEllLENBMElmO0FBQ0E7O0FBQ0EsWUFBTXVELE1BQU0sR0FBR0gsY0FBYyxDQUFDMUQsR0FBZixDQUFvQnhELEVBQUQsSUFBUTtBQUN0QyxjQUFNcUUsU0FBUyxHQUFHckUsRUFBRSxDQUFDc0UsTUFBSCxFQUFsQjtBQUNBLGNBQU1uRCxDQUFDLEdBQUduQixFQUFFLENBQUN1RSxXQUFILEtBQW1CRixTQUFTLENBQUNHLFNBQTdCLEdBQXlDSCxTQUFuRDtBQUVBLFlBQUlJLE9BQU8sR0FBRyxFQUFkO0FBQ0EsWUFBSXRELENBQUMsQ0FBQ3dELE1BQUYsSUFBWWdDLFFBQWhCLEVBQTBCbEMsT0FBTyxHQUFHa0MsUUFBUSxDQUFDeEYsQ0FBQyxDQUFDd0QsTUFBSCxDQUFsQjtBQUMxQixjQUFNMkMsTUFBTSxHQUFHO0FBQ1hULFVBQUFBLEtBQUssRUFBRTFGLENBREk7QUFFWHNELFVBQUFBLE9BQU8sRUFBRUE7QUFGRSxTQUFmO0FBSUEsZUFBTzZDLE1BQVA7QUFDSCxPQVhjLENBQWYsQ0E1SWUsQ0F5SmY7QUFDQTs7QUFDQSxZQUFNQyxhQUFhLEdBQUc7QUFDbEJqSCxRQUFBQSxNQUFNLEVBQUV3RixVQUFVLENBQUN4RixNQUREO0FBRWxCa0IsUUFBQUEsS0FBSyxFQUFFMkUsR0FBRyxDQUFDcUIsR0FGTztBQUdsQjdGLFFBQUFBLFNBQVMsRUFBRW1FLFVBQVUsQ0FBQ25FLFNBSEo7QUFJbEJDLFFBQUFBLFNBQVMsRUFBRWtFLFVBQVUsQ0FBQ2xFO0FBSkosT0FBdEI7QUFPQS9CLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUNJLDBCQURKLEVBRUkyQyxNQUFNLENBQUNnRixPQUFQLENBQWUzQixVQUFVLENBQUN4RixNQUExQixFQUFrQ29ILElBRnRDLEVBR0ksbUJBSEosRUFHeUJqQixZQUFZLENBQUNELE1BSHRDLEVBRzhDLGlCQUg5QyxFQUlJYSxNQUFNLENBQUNiLE1BSlgsRUFJbUIsa0JBSm5CLEVBSXVDWSxlQUFlLENBQUNaLE1BSnZELEVBS0ksZUFMSixFQUtxQkMsWUFBWSxDQUFDRCxNQUFiLEdBQXNCYSxNQUFNLENBQUNiLE1BTGxELEVBTUksa0NBTkosRUFNd0NXLG1CQUFtQixDQUFDWCxNQU41RDs7QUFTQSxVQUFJO0FBQ0EsYUFBSyxJQUFJbUIsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1AsZUFBZSxDQUFDWixNQUFwQyxFQUE0Q21CLENBQUMsRUFBN0MsRUFBaUQ7QUFDN0MsZ0JBQU0zSCxFQUFFLEdBQUdvSCxlQUFlLENBQUNPLENBQUQsQ0FBMUI7QUFDQSxnQkFBTXRJLFlBQVksQ0FBQzRCLFdBQWIsQ0FBeUJqQixFQUFFLENBQUNrQixlQUFILEVBQXpCLENBQU47QUFDSDs7QUFFRCxjQUFNMEcsa0JBQWtCLEdBQUcsTUFBTXZJLFlBQVksQ0FBQ3dJLGlCQUFiLENBQzdCUixNQUQ2QixFQUNyQkUsYUFEcUIsRUFDTnpCLFVBRE0sQ0FBakMsQ0FOQSxDQVFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFlBQUk4QixrQkFBa0IsS0FBSyxJQUF2QixJQUErQkwsYUFBYSxDQUFDNUYsU0FBZCxLQUE0QixJQUEvRCxFQUFxRTtBQUNqRTlCLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLCtDQUFaLEVBQ1ksMkJBRFosRUFDeUNnRyxVQUR6QztBQUVBLGdCQUFNekcsWUFBWSxDQUFDaUgsdUJBQWIsQ0FBcUNpQixhQUFyQyxDQUFOO0FBQ0gsU0FKRCxNQUlPO0FBQ0gsZUFBS3pGLGtCQUFMLENBQXdCQyxJQUF4QixDQUE2QndGLGFBQTdCO0FBQ0g7QUFDSixPQW5CRCxDQW1CRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1J0QixRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxtQ0FBWixFQUFpRHFCLENBQWpELEVBRFEsQ0FFUjtBQUNBOztBQUNBLGFBQUtXLGtCQUFMLENBQXdCQyxJQUF4QixDQUE2QitELFVBQTdCO0FBQ0g7QUFDSjs7QUFFRCxTQUFLNUQsUUFBTCxHQUFnQixJQUFoQjtBQUVBckMsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksdUNBQVo7QUFDSDtBQUVEOzs7OztBQUdBRixFQUFBQSxZQUFZLEdBQUc7QUFDWCxRQUFJLEtBQUtzQyxRQUFMLEtBQWtCLElBQXRCLEVBQTRCO0FBQzVCLFNBQUtpRCxXQUFMO0FBQ0g7QUFFRDs7Ozs7QUFHQTJDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFFBQUksS0FBSzVGLFFBQUwsS0FBa0IsSUFBdEIsRUFBNEI7O0FBQzVCLFNBQUtBLFFBQUwsQ0FBY21ELE1BQWQ7QUFDSDtBQUVEOzs7Ozs7OztBQU1BLFFBQU0wQyxLQUFOLEdBQWM7QUFDVixVQUFNMUksWUFBWSxHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLEVBQXJCOztBQUNBLFNBQUt3RCxlQUFMO0FBQ0EsU0FBSzhFLFdBQUw7QUFDQSxVQUFNekksWUFBWSxDQUFDMkksZUFBYixFQUFOO0FBQ0E7QUFDSDtBQUVEOzs7Ozs7Ozs7OztBQVNBLFFBQU1DLE1BQU4sQ0FBYUMsVUFBYixFQUF5QjtBQUNyQixVQUFNN0ksWUFBWSxHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLEVBQXJCOztBQUNBLFdBQU9ILFlBQVksQ0FBQzhJLGdCQUFiLENBQThCRCxVQUE5QixDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEsUUFBTUUsY0FBTixDQUFxQm5JLElBQXJCLEVBQTJCb0ksS0FBSyxHQUFHLEVBQW5DLEVBQXVDQyxTQUFTLEdBQUcsSUFBbkQsRUFBeUQxRyxTQUFTLEdBQUcyRywyQkFBY0MsU0FBbkYsRUFBOEY7QUFDMUYsVUFBTS9GLE1BQU0sR0FBR3JDLGlDQUFnQmIsR0FBaEIsRUFBZjs7QUFDQSxVQUFNRixZQUFZLEdBQUdDLHFCQUFZQyxHQUFaLEdBQWtCQyx1QkFBbEIsRUFBckI7O0FBRUEsVUFBTWlKLFFBQVEsR0FBRztBQUNibkksTUFBQUEsTUFBTSxFQUFFTCxJQUFJLENBQUNLLE1BREE7QUFFYitILE1BQUFBLEtBQUssRUFBRUE7QUFGTSxLQUFqQjs7QUFLQSxRQUFJQyxTQUFKLEVBQWU7QUFDWEcsTUFBQUEsUUFBUSxDQUFDSCxTQUFULEdBQXFCQSxTQUFyQjtBQUNBRyxNQUFBQSxRQUFRLENBQUM3RyxTQUFULEdBQXFCQSxTQUFyQjtBQUNIOztBQUVELFFBQUl5RixNQUFKLENBZDBGLENBZ0IxRjs7QUFDQSxRQUFJO0FBQ0FBLE1BQUFBLE1BQU0sR0FBRyxNQUFNaEksWUFBWSxDQUFDK0ksY0FBYixDQUE0QkssUUFBNUIsQ0FBZjtBQUNILEtBRkQsQ0FFRSxPQUFPdEgsQ0FBUCxFQUFVO0FBQ1J0QixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSx1Q0FBWixFQUFxRHFCLENBQXJEO0FBQ0EsYUFBTyxFQUFQO0FBQ0g7O0FBRUQsVUFBTThFLFdBQVcsR0FBR3hELE1BQU0sQ0FBQ3lELGNBQVAsRUFBcEIsQ0F4QjBGLENBMEIxRjs7QUFDQSxVQUFNTyxZQUFZLEdBQUdZLE1BQU0sQ0FBQzdELEdBQVAsQ0FBV3JDLENBQUMsSUFBSTtBQUNqQyxZQUFNdUgsV0FBVyxHQUFHekMsV0FBVyxDQUFDOUUsQ0FBQyxDQUFDMEYsS0FBSCxDQUEvQjtBQUVBLFlBQU04QixNQUFNLEdBQUcsSUFBSUMsdUJBQUosQ0FBZTNJLElBQUksQ0FBQ0ssTUFBcEIsRUFBNEJvSSxXQUFXLENBQUNHLFNBQVosRUFBNUIsQ0FBZixDQUhpQyxDQUtqQztBQUNBO0FBQ0E7O0FBQ0FGLE1BQUFBLE1BQU0sQ0FBQ2pCLElBQVAsR0FBY3ZHLENBQUMsQ0FBQ3NELE9BQUYsQ0FBVUMsV0FBVixHQUF3QixJQUF4QixHQUErQmdFLFdBQVcsQ0FBQ0csU0FBWixFQUEvQixHQUF5RCxHQUF2RSxDQVJpQyxDQVVqQzs7QUFDQSxZQUFNQyxXQUFXLEdBQUc3QyxXQUFXLENBQzNCO0FBQ0lhLFFBQUFBLE9BQU8sRUFBRTtBQUNMQyxVQUFBQSxVQUFVLEVBQUUsTUFEUDtBQUVMbEMsVUFBQUEsVUFBVSxFQUFFMUQsQ0FBQyxDQUFDc0QsT0FBRixDQUFVSSxVQUZqQjtBQUdMSCxVQUFBQSxXQUFXLEVBQUV2RCxDQUFDLENBQUNzRCxPQUFGLENBQVVDO0FBSGxCLFNBRGI7QUFNSXFFLFFBQUFBLElBQUksRUFBRSxlQU5WO0FBT0lDLFFBQUFBLFFBQVEsRUFBRU4sV0FBVyxDQUFDL0gsS0FBWixLQUFzQixhQVBwQztBQVFJc0ksUUFBQUEsT0FBTyxFQUFFUCxXQUFXLENBQUNRLFNBQVosRUFSYjtBQVNJdkUsUUFBQUEsTUFBTSxFQUFFK0QsV0FBVyxDQUFDRyxTQUFaLEVBVFo7QUFVSU0sUUFBQUEsZ0JBQWdCLEVBQUVULFdBQVcsQ0FBQ1UsS0FBWixFQVZ0QjtBQVdJQyxRQUFBQSxTQUFTLEVBQUVYLFdBQVcsQ0FBQ0csU0FBWjtBQVhmLE9BRDJCLENBQS9CLENBWGlDLENBMkJqQztBQUNBOztBQUNBRixNQUFBQSxNQUFNLENBQUN0QixNQUFQLENBQWNzQixNQUFkLEdBQXVCRyxXQUF2QjtBQUNBSixNQUFBQSxXQUFXLENBQUMvRCxNQUFaLEdBQXFCZ0UsTUFBckI7QUFFQSxhQUFPRCxXQUFQO0FBQ0gsS0FqQ29CLENBQXJCO0FBbUNBLFdBQU9qQyxZQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkEsUUFBTTZDLG9CQUFOLENBQTJCbEksV0FBM0IsRUFBd0NFLFFBQXhDLEVBQWtEckIsSUFBbEQsRUFBd0RvSSxLQUFLLEdBQUcsRUFBaEUsRUFDMkJDLFNBQVMsR0FBRyxJQUR2QyxFQUM2QzFHLFNBQVMsR0FBRzJHLDJCQUFjQyxTQUR2RSxFQUNrRjtBQUM5RSxVQUFNL0IsWUFBWSxHQUFHLE1BQU0sS0FBSzJCLGNBQUwsQ0FBb0JuSSxJQUFwQixFQUEwQm9JLEtBQTFCLEVBQWlDQyxTQUFqQyxFQUE0QzFHLFNBQTVDLENBQTNCLENBRDhFLENBRzlFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSTBHLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUNwQjdCLE1BQUFBLFlBQVksQ0FBQzhDLE9BQWI7QUFDQTNILE1BQUFBLFNBQVMsR0FBR0EsU0FBUyxJQUFJMkcsMkJBQWNDLFNBQTNCLEdBQXVDRCwyQkFBY2lCLFFBQXJELEdBQStEakIsMkJBQWNDLFNBQXpGO0FBQ0gsS0FYNkUsQ0FhOUU7OztBQUNBL0IsSUFBQUEsWUFBWSxDQUFDRyxPQUFiLENBQXFCekYsQ0FBQyxJQUFJO0FBQ3RCLFVBQUksQ0FBQ0MsV0FBVyxDQUFDcUksaUJBQVosQ0FBOEJ0SSxDQUFDLENBQUNSLEtBQUYsRUFBOUIsQ0FBTCxFQUErQztBQUMzQ1MsUUFBQUEsV0FBVyxDQUFDc0ksa0JBQVosQ0FBK0J2SSxDQUEvQixFQUFrQ0csUUFBbEMsRUFBNENNLFNBQVMsSUFBSTJHLDJCQUFjQyxTQUF2RTtBQUNIO0FBQ0osS0FKRDtBQU1BLFFBQUltQixHQUFHLEdBQUcsS0FBVjtBQUNBLFFBQUlDLGVBQWUsR0FBRyxFQUF0QixDQXJCOEUsQ0F1QjlFOztBQUNBLFFBQUluRCxZQUFZLENBQUNELE1BQWIsR0FBc0IsQ0FBMUIsRUFBNkI7QUFDekJvRCxNQUFBQSxlQUFlLEdBQUduRCxZQUFZLENBQUNBLFlBQVksQ0FBQ0QsTUFBYixHQUFzQixDQUF2QixDQUFaLENBQXNDN0YsS0FBdEMsRUFBbEI7QUFDQWdKLE1BQUFBLEdBQUcsR0FBRyxJQUFOO0FBQ0g7O0FBRUQ5SixJQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSx3Q0FBWixFQUFzRDJHLFlBQVksQ0FBQ0QsTUFBbkUsRUFDWSw0Q0FEWixFQUMwRG9ELGVBRDFEO0FBR0F0SSxJQUFBQSxRQUFRLENBQUN1SSxrQkFBVCxDQUE0QkQsZUFBNUIsRUFBNkNyQiwyQkFBY0MsU0FBM0Q7QUFDQSxXQUFPbUIsR0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQUcsRUFBQUEsc0JBQXNCLENBQUM3SixJQUFELEVBQU84SixjQUFQLEVBQXVCbkksU0FBdkIsRUFBa0N5RyxLQUFsQyxFQUF5QztBQUMzRCxVQUFNMkIsRUFBRSxHQUFHRCxjQUFjLENBQUNFLGdCQUFmLENBQWdDckksU0FBaEMsQ0FBWDtBQUVBLFFBQUksQ0FBQ29JLEVBQUwsRUFBUyxPQUFPMUcsT0FBTyxDQUFDNEcsT0FBUixDQUFnQixLQUFoQixDQUFQO0FBQ1QsUUFBSUYsRUFBRSxDQUFDRyxlQUFQLEVBQXdCLE9BQU9ILEVBQUUsQ0FBQ0csZUFBVjs7QUFFeEIsUUFBSUosY0FBYyxDQUFDSyxNQUFmLENBQXNCeEksU0FBdEIsRUFBaUN5RyxLQUFqQyxDQUFKLEVBQTZDO0FBQ3pDLGFBQU8vRSxPQUFPLENBQUM0RyxPQUFSLENBQWdCLElBQWhCLENBQVA7QUFDSDs7QUFFRCxVQUFNRyxnQkFBZ0IsR0FBRyxPQUFPTixjQUFQLEVBQXVCekksUUFBdkIsRUFBaUNyQixJQUFqQyxFQUF1QzJCLFNBQXZDLEVBQWtEeUcsS0FBbEQsS0FBNEQ7QUFDakYsWUFBTWpILFdBQVcsR0FBRzJJLGNBQWMsQ0FBQ08sWUFBbkM7QUFDQSxZQUFNOUksS0FBSyxHQUFHRixRQUFRLENBQUNBLFFBQVQsQ0FBa0JHLGtCQUFsQixDQUFxQ0csU0FBckMsQ0FBZDtBQUVBLFlBQU0rSCxHQUFHLEdBQUcsTUFBTSxLQUFLTCxvQkFBTCxDQUEwQmxJLFdBQTFCLEVBQXVDRSxRQUFRLENBQUNBLFFBQWhELEVBQTBEckIsSUFBMUQsRUFBZ0VvSSxLQUFoRSxFQUF1RTdHLEtBQXZFLEVBQThFSSxTQUE5RSxDQUFsQjtBQUVBTixNQUFBQSxRQUFRLENBQUM2SSxlQUFULEdBQTJCLElBQTNCO0FBQ0FKLE1BQUFBLGNBQWMsQ0FBQ0ssTUFBZixDQUFzQnhJLFNBQXRCLEVBQWlDeUcsS0FBakM7QUFFQSxhQUFPc0IsR0FBUDtBQUNILEtBVkQ7O0FBWUEsVUFBTVksaUJBQWlCLEdBQUdGLGdCQUFnQixDQUFDTixjQUFELEVBQWlCQyxFQUFqQixFQUFxQi9KLElBQXJCLEVBQTJCMkIsU0FBM0IsRUFBc0N5RyxLQUF0QyxDQUExQztBQUNBMkIsSUFBQUEsRUFBRSxDQUFDRyxlQUFILEdBQXFCSSxpQkFBckI7QUFFQSxXQUFPQSxpQkFBUDtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUEsUUFBTUMsUUFBTixHQUFpQjtBQUNiLFVBQU1uTCxZQUFZLEdBQUdDLHFCQUFZQyxHQUFaLEdBQWtCQyx1QkFBbEIsRUFBckI7O0FBQ0EsV0FBT0gsWUFBWSxDQUFDbUwsUUFBYixFQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQXRGLEVBQUFBLFdBQVcsR0FBRztBQUNWLFFBQUksS0FBSy9DLGtCQUFMLEtBQTRCLElBQTVCLElBQW9DLEtBQUtMLGtCQUFMLENBQXdCMEUsTUFBeEIsS0FBbUMsQ0FBM0UsRUFBOEU7QUFDMUUsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsVUFBTS9ELE1BQU0sR0FBR3JDLGlDQUFnQmIsR0FBaEIsRUFBZjs7QUFFQSxRQUFJLEtBQUs0QyxrQkFBTCxLQUE0QixJQUFoQyxFQUFzQztBQUNsQyxhQUFPTSxNQUFNLENBQUNnRixPQUFQLENBQWUsS0FBS3RGLGtCQUFMLENBQXdCN0IsTUFBdkMsQ0FBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU9tQyxNQUFNLENBQUNnRixPQUFQLENBQWUsS0FBSzNGLGtCQUFMLENBQXdCLENBQXhCLEVBQTJCeEIsTUFBMUMsQ0FBUDtBQUNIO0FBQ0o7O0FBRURtSyxFQUFBQSxhQUFhLEdBQUc7QUFDWixVQUFNQyxVQUFVLEdBQUcsSUFBSXRJLEdBQUosRUFBbkI7QUFDQSxVQUFNcUksYUFBYSxHQUFHLElBQUlySSxHQUFKLEVBQXRCO0FBRUEsU0FBS04sa0JBQUwsQ0FBd0I4RSxPQUF4QixDQUFnQyxDQUFDZCxVQUFELEVBQWE2RSxLQUFiLEtBQXVCO0FBQ25ERixNQUFBQSxhQUFhLENBQUM1SixHQUFkLENBQWtCaUYsVUFBVSxDQUFDeEYsTUFBN0I7QUFDSCxLQUZEOztBQUlBLFFBQUksS0FBSzZCLGtCQUFMLEtBQTRCLElBQWhDLEVBQXNDO0FBQ2xDc0ksTUFBQUEsYUFBYSxDQUFDNUosR0FBZCxDQUFrQixLQUFLc0Isa0JBQUwsQ0FBd0I3QixNQUExQztBQUNIOztBQUVELFVBQU1tQyxNQUFNLEdBQUdyQyxpQ0FBZ0JiLEdBQWhCLEVBQWY7O0FBQ0EsVUFBTTJELEtBQUssR0FBR1QsTUFBTSxDQUFDVSxRQUFQLEVBQWQ7O0FBRUEsVUFBTTlDLGVBQWUsR0FBSUosSUFBRCxJQUFVO0FBQzlCLGFBQU93QyxNQUFNLENBQUNwQyxlQUFQLENBQXVCSixJQUFJLENBQUNLLE1BQTVCLENBQVA7QUFDSCxLQUZEOztBQUlBLFVBQU04QyxjQUFjLEdBQUdGLEtBQUssQ0FBQ0csTUFBTixDQUFhaEQsZUFBYixDQUF2QjtBQUNBK0MsSUFBQUEsY0FBYyxDQUFDd0QsT0FBZixDQUF1QixDQUFDM0csSUFBRCxFQUFPMEssS0FBUCxLQUFpQjtBQUNwQ0QsTUFBQUEsVUFBVSxDQUFDN0osR0FBWCxDQUFlWixJQUFJLENBQUNLLE1BQXBCO0FBQ0gsS0FGRDtBQUlBLFdBQU87QUFBQ21LLE1BQUFBLGFBQUQ7QUFBZ0JDLE1BQUFBO0FBQWhCLEtBQVA7QUFDSDs7QUFyekJnRCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gXCIuLi9QbGF0Zm9ybVBlZ1wiO1xyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSBcIi4uL01hdHJpeENsaWVudFBlZ1wiO1xyXG5pbXBvcnQge0V2ZW50VGltZWxpbmUsIFJvb21NZW1iZXJ9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQge3NsZWVwfSBmcm9tIFwiLi4vdXRpbHMvcHJvbWlzZVwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSBcIi4uL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IHtFdmVudEVtaXR0ZXJ9IGZyb20gXCJldmVudHNcIjtcclxuXHJcbi8qXHJcbiAqIEV2ZW50IGluZGV4aW5nIGNsYXNzIHRoYXQgd3JhcHMgdGhlIHBsYXRmb3JtIHNwZWNpZmljIGV2ZW50IGluZGV4aW5nLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXZlbnRJbmRleCBleHRlbmRzIEV2ZW50RW1pdHRlciB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuY3Jhd2xlckNoZWNrcG9pbnRzID0gW107XHJcbiAgICAgICAgLy8gVGhlIHRpbWUgaW4gbXMgdGhhdCB0aGUgY3Jhd2xlciB3aWxsIHdhaXQgbG9vcCBpdGVyYXRpb25zIGlmIHRoZXJlXHJcbiAgICAgICAgLy8gaGF2ZSBub3QgYmVlbiBhbnkgY2hlY2twb2ludHMgdG8gY29uc3VtZSBpbiB0aGUgbGFzdCBpdGVyYXRpb24uXHJcbiAgICAgICAgdGhpcy5fY3Jhd2xlcklkbGVUaW1lID0gNTAwMDtcclxuICAgICAgICAvLyBUaGUgbWF4aW11bSBudW1iZXIgb2YgZXZlbnRzIG91ciBjcmF3bGVyIHNob3VsZCBmZXRjaCBpbiBhIHNpbmdsZVxyXG4gICAgICAgIC8vIGNyYXdsLlxyXG4gICAgICAgIHRoaXMuX2V2ZW50c1BlckNyYXdsID0gMTAwO1xyXG4gICAgICAgIHRoaXMuX2NyYXdsZXIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2N1cnJlbnRDaGVja3BvaW50ID0gbnVsbDtcclxuICAgICAgICB0aGlzLmxpdmVFdmVudHNGb3JJbmRleCA9IG5ldyBTZXQoKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBpbml0KCkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcblxyXG4gICAgICAgIGF3YWl0IGluZGV4TWFuYWdlci5pbml0RXZlbnRJbmRleCgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogU3VjY2Vzc2Z1bGx5IGluaXRpYWxpemVkIHRoZSBldmVudCBpbmRleFwiKTtcclxuXHJcbiAgICAgICAgdGhpcy5jcmF3bGVyQ2hlY2twb2ludHMgPSBhd2FpdCBpbmRleE1hbmFnZXIubG9hZENoZWNrcG9pbnRzKCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBMb2FkZWQgY2hlY2twb2ludHNcIiwgdGhpcy5jcmF3bGVyQ2hlY2twb2ludHMpO1xyXG5cclxuICAgICAgICB0aGlzLnJlZ2lzdGVyTGlzdGVuZXJzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZWdpc3RlciBldmVudCBsaXN0ZW5lcnMgdGhhdCBhcmUgbmVjZXNzYXJ5IGZvciB0aGUgZXZlbnQgaW5kZXggdG8gd29yay5cclxuICAgICAqL1xyXG4gICAgcmVnaXN0ZXJMaXN0ZW5lcnMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICBjbGllbnQub24oJ3N5bmMnLCB0aGlzLm9uU3luYyk7XHJcbiAgICAgICAgY2xpZW50Lm9uKCdSb29tLnRpbWVsaW5lJywgdGhpcy5vblJvb21UaW1lbGluZSk7XHJcbiAgICAgICAgY2xpZW50Lm9uKCdFdmVudC5kZWNyeXB0ZWQnLCB0aGlzLm9uRXZlbnREZWNyeXB0ZWQpO1xyXG4gICAgICAgIGNsaWVudC5vbignUm9vbS50aW1lbGluZVJlc2V0JywgdGhpcy5vblRpbWVsaW5lUmVzZXQpO1xyXG4gICAgICAgIGNsaWVudC5vbignUm9vbS5yZWRhY3Rpb24nLCB0aGlzLm9uUmVkYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSB0aGUgZXZlbnQgaW5kZXggc3BlY2lmaWMgZXZlbnQgbGlzdGVuZXJzLlxyXG4gICAgICovXHJcbiAgICByZW1vdmVMaXN0ZW5lcnMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGlmIChjbGllbnQgPT09IG51bGwpIHJldHVybjtcclxuXHJcbiAgICAgICAgY2xpZW50LnJlbW92ZUxpc3RlbmVyKCdzeW5jJywgdGhpcy5vblN5bmMpO1xyXG4gICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcignUm9vbS50aW1lbGluZScsIHRoaXMub25Sb29tVGltZWxpbmUpO1xyXG4gICAgICAgIGNsaWVudC5yZW1vdmVMaXN0ZW5lcignRXZlbnQuZGVjcnlwdGVkJywgdGhpcy5vbkV2ZW50RGVjcnlwdGVkKTtcclxuICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoJ1Jvb20udGltZWxpbmVSZXNldCcsIHRoaXMub25UaW1lbGluZVJlc2V0KTtcclxuICAgICAgICBjbGllbnQucmVtb3ZlTGlzdGVuZXIoJ1Jvb20ucmVkYWN0aW9uJywgdGhpcy5vblJlZGFjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgY3Jhd2xlciBjaGVja3BvaW50cyBmb3IgdGhlIGVuY3J5cHRlZCByb29tcyBhbmQgc3RvcmUgdGhlbSBpbiB0aGUgaW5kZXguXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGFkZEluaXRpYWxDaGVja3BvaW50cygpIHtcclxuICAgICAgICBjb25zdCBpbmRleE1hbmFnZXIgPSBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRFdmVudEluZGV4aW5nTWFuYWdlcigpO1xyXG4gICAgICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICBjb25zdCByb29tcyA9IGNsaWVudC5nZXRSb29tcygpO1xyXG5cclxuICAgICAgICBjb25zdCBpc1Jvb21FbmNyeXB0ZWQgPSAocm9vbSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gY2xpZW50LmlzUm9vbUVuY3J5cHRlZChyb29tLnJvb21JZCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gV2Ugb25seSBjYXJlIHRvIGNyYXdsIHRoZSBlbmNyeXB0ZWQgcm9vbXMsIG5vbi1lbmNyeXB0ZWRcclxuICAgICAgICAvLyByb29tcyBjYW4gdXNlIHRoZSBzZWFyY2ggcHJvdmlkZWQgYnkgdGhlIGhvbWVzZXJ2ZXIuXHJcbiAgICAgICAgY29uc3QgZW5jcnlwdGVkUm9vbXMgPSByb29tcy5maWx0ZXIoaXNSb29tRW5jcnlwdGVkKTtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBBZGRpbmcgaW5pdGlhbCBjcmF3bGVyIGNoZWNrcG9pbnRzXCIpO1xyXG5cclxuICAgICAgICAvLyBHYXRoZXIgdGhlIHByZXZfYmF0Y2ggdG9rZW5zIGFuZCBjcmVhdGUgY2hlY2twb2ludHMgZm9yXHJcbiAgICAgICAgLy8gb3VyIG1lc3NhZ2UgY3Jhd2xlci5cclxuICAgICAgICBhd2FpdCBQcm9taXNlLmFsbChlbmNyeXB0ZWRSb29tcy5tYXAoYXN5bmMgKHJvb20pID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdGltZWxpbmUgPSByb29tLmdldExpdmVUaW1lbGluZSgpO1xyXG4gICAgICAgICAgICBjb25zdCB0b2tlbiA9IHRpbWVsaW5lLmdldFBhZ2luYXRpb25Ub2tlbihcImJcIik7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEdvdCB0b2tlbiBmb3IgaW5kZXhlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tLnJvb21JZCwgdG9rZW4pO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYmFja0NoZWNrcG9pbnQgPSB7XHJcbiAgICAgICAgICAgICAgICByb29tSWQ6IHJvb20ucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgdG9rZW46IHRva2VuLFxyXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uOiBcImJcIixcclxuICAgICAgICAgICAgICAgIGZ1bGxDcmF3bDogdHJ1ZSxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGZvcndhcmRDaGVja3BvaW50ID0ge1xyXG4gICAgICAgICAgICAgICAgcm9vbUlkOiByb29tLnJvb21JZCxcclxuICAgICAgICAgICAgICAgIHRva2VuOiB0b2tlbixcclxuICAgICAgICAgICAgICAgIGRpcmVjdGlvbjogXCJmXCIsXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGJhY2tDaGVja3BvaW50LnRva2VuKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLmFkZENyYXdsZXJDaGVja3BvaW50KGJhY2tDaGVja3BvaW50KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNyYXdsZXJDaGVja3BvaW50cy5wdXNoKGJhY2tDaGVja3BvaW50KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZm9yd2FyZENoZWNrcG9pbnQudG9rZW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCBpbmRleE1hbmFnZXIuYWRkQ3Jhd2xlckNoZWNrcG9pbnQoZm9yd2FyZENoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3Jhd2xlckNoZWNrcG9pbnRzLnB1c2goZm9yd2FyZENoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEVycm9yIGFkZGluZyBpbml0aWFsIGNoZWNrcG9pbnRzIGZvciByb29tXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb29tLnJvb21JZCwgYmFja0NoZWNrcG9pbnQsIGZvcndhcmRDaGVja3BvaW50LCBlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVGhlIHN5bmMgZXZlbnQgbGlzdGVuZXIuXHJcbiAgICAgKlxyXG4gICAgICogVGhlIGxpc3RlbmVyIGhhcyB0d28gY2FzZXM6XHJcbiAgICAgKiAgICAgLSBGaXJzdCBzeW5jIGFmdGVyIHN0YXJ0IHVwLCBjaGVjayBpZiB0aGUgaW5kZXggaXMgZW1wdHksIGFkZFxyXG4gICAgICogICAgICAgICBpbml0aWFsIGNoZWNrcG9pbnRzLCBpZiBzby4gU3RhcnQgdGhlIGNyYXdsZXIgYmFja2dyb3VuZCB0YXNrLlxyXG4gICAgICogICAgIC0gRXZlcnkgb3RoZXIgc3luYywgdGVsbCB0aGUgZXZlbnQgaW5kZXggdG8gY29tbWl0IGFsbCB0aGUgcXVldWVkIHVwXHJcbiAgICAgKiAgICAgICAgIGxpdmUgZXZlbnRzXHJcbiAgICAgKi9cclxuICAgIG9uU3luYyA9IGFzeW5jIChzdGF0ZSwgcHJldlN0YXRlLCBkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc3QgaW5kZXhNYW5hZ2VyID0gUGxhdGZvcm1QZWcuZ2V0KCkuZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIoKTtcclxuXHJcbiAgICAgICAgaWYgKHByZXZTdGF0ZSA9PT0gXCJQUkVQQVJFRFwiICYmIHN0YXRlID09PSBcIlNZTkNJTkdcIikge1xyXG4gICAgICAgICAgICAvLyBJZiBvdXIgaW5kZXhlciBpcyBlbXB0eSB3ZSdyZSBtb3N0IGxpa2VseSBydW5uaW5nIFJpb3QgdGhlXHJcbiAgICAgICAgICAgIC8vIGZpcnN0IHRpbWUgd2l0aCBpbmRleGluZyBzdXBwb3J0IG9yIHJ1bm5pbmcgaXQgd2l0aCBhblxyXG4gICAgICAgICAgICAvLyBpbml0aWFsIHN5bmMuIEFkZCBjaGVja3BvaW50cyB0byBjcmF3bCBvdXIgZW5jcnlwdGVkIHJvb21zLlxyXG4gICAgICAgICAgICBjb25zdCBldmVudEluZGV4V2FzRW1wdHkgPSBhd2FpdCBpbmRleE1hbmFnZXIuaXNFdmVudEluZGV4RW1wdHkoKTtcclxuICAgICAgICAgICAgaWYgKGV2ZW50SW5kZXhXYXNFbXB0eSkgYXdhaXQgdGhpcy5hZGRJbml0aWFsQ2hlY2twb2ludHMoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3RhcnRDcmF3bGVyKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChwcmV2U3RhdGUgPT09IFwiU1lOQ0lOR1wiICYmIHN0YXRlID09PSBcIlNZTkNJTkdcIikge1xyXG4gICAgICAgICAgICAvLyBBIHN5bmMgd2FzIGRvbmUsIHByZXN1bWFibHkgd2UgcXVldWVkIHVwIHNvbWUgbGl2ZSBldmVudHMsXHJcbiAgICAgICAgICAgIC8vIGNvbW1pdCB0aGVtIG5vdy5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBDb21taXR0aW5nIGV2ZW50c1wiKTtcclxuICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLmNvbW1pdExpdmVFdmVudHMoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVGhlIFJvb20udGltZWxpbmUgbGlzdGVuZXIuXHJcbiAgICAgKlxyXG4gICAgICogVGhpcyBsaXN0ZW5lciB3YWl0cyBmb3IgbGl2ZSBldmVudHMgaW4gZW5jcnlwdGVkIHJvb21zLCBpZiB0aGV5IGFyZVxyXG4gICAgICogZGVjcnlwdGVkIG9yIHVuZW5jcnlwdGVkIHdlIHF1ZXVlIHRoZW0gdG8gYmUgYWRkZWQgdG8gdGhlIGluZGV4LFxyXG4gICAgICogb3RoZXJ3aXNlIHdlIHNhdmUgdGhlaXIgZXZlbnQgaWQgYW5kIHdhaXQgZm9yIHRoZW0gaW4gdGhlIEV2ZW50LmRlY3J5cHRlZFxyXG4gICAgICogbGlzdGVuZXIuXHJcbiAgICAgKi9cclxuICAgIG9uUm9vbVRpbWVsaW5lID0gYXN5bmMgKGV2LCByb29tLCB0b1N0YXJ0T2ZUaW1lbGluZSwgcmVtb3ZlZCwgZGF0YSkgPT4ge1xyXG4gICAgICAgIC8vIFdlIG9ubHkgaW5kZXggZW5jcnlwdGVkIHJvb21zIGxvY2FsbHkuXHJcbiAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNSb29tRW5jcnlwdGVkKHJvb20ucm9vbUlkKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBJZiBpdCBpc24ndCBhIGxpdmUgZXZlbnQgb3IgaWYgaXQncyByZWRhY3RlZCB0aGVyZSdzIG5vdGhpbmcgdG9cclxuICAgICAgICAvLyBkby5cclxuICAgICAgICBpZiAodG9TdGFydE9mVGltZWxpbmUgfHwgIWRhdGEgfHwgIWRhdGEubGl2ZUV2ZW50XHJcbiAgICAgICAgICAgIHx8IGV2LmlzUmVkYWN0ZWQoKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZiB0aGUgZXZlbnQgaXMgbm90IHlldCBkZWNyeXB0ZWQgbWFyayBpdCBmb3IgdGhlXHJcbiAgICAgICAgLy8gRXZlbnQuZGVjcnlwdGVkIGNhbGxiYWNrLlxyXG4gICAgICAgIGlmIChldi5pc0JlaW5nRGVjcnlwdGVkKCkpIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnRJZCA9IGV2LmdldElkKCk7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZUV2ZW50c0ZvckluZGV4LmFkZChldmVudElkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBJZiB0aGUgZXZlbnQgaXMgZGVjcnlwdGVkIG9yIGlzIHVuZW5jcnlwdGVkIGFkZCBpdCB0byB0aGVcclxuICAgICAgICAgICAgLy8gaW5kZXggbm93LlxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmFkZExpdmVFdmVudFRvSW5kZXgoZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVGhlIEV2ZW50LmRlY3J5cHRlZCBsaXN0ZW5lci5cclxuICAgICAqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIGV2ZW50IHdhcyBtYXJrZWQgZm9yIGFkZGl0aW9uIGluIHRoZSBSb29tLnRpbWVsaW5lXHJcbiAgICAgKiBsaXN0ZW5lciwgaWYgc28gcXVldWVzIGl0IHVwIHRvIGJlIGFkZGVkIHRvIHRoZSBpbmRleC5cclxuICAgICAqL1xyXG4gICAgb25FdmVudERlY3J5cHRlZCA9IGFzeW5jIChldiwgZXJyKSA9PiB7XHJcbiAgICAgICAgY29uc3QgZXZlbnRJZCA9IGV2LmdldElkKCk7XHJcblxyXG4gICAgICAgIC8vIElmIHRoZSBldmVudCBpc24ndCBpbiBvdXIgbGl2ZSBldmVudCBzZXQsIGlnbm9yZSBpdC5cclxuICAgICAgICBpZiAoIXRoaXMubGl2ZUV2ZW50c0ZvckluZGV4LmRlbGV0ZShldmVudElkKSkgcmV0dXJuO1xyXG4gICAgICAgIGlmIChlcnIpIHJldHVybjtcclxuICAgICAgICBhd2FpdCB0aGlzLmFkZExpdmVFdmVudFRvSW5kZXgoZXYpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBUaGUgUm9vbS5yZWRhY3Rpb24gbGlzdGVuZXIuXHJcbiAgICAgKlxyXG4gICAgICogUmVtb3ZlcyBhIHJlZGFjdGVkIGV2ZW50IGZyb20gb3VyIGV2ZW50IGluZGV4LlxyXG4gICAgICovXHJcbiAgICBvblJlZGFjdGlvbiA9IGFzeW5jIChldiwgcm9vbSkgPT4ge1xyXG4gICAgICAgIC8vIFdlIG9ubHkgaW5kZXggZW5jcnlwdGVkIHJvb21zIGxvY2FsbHkuXHJcbiAgICAgICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkuaXNSb29tRW5jcnlwdGVkKHJvb20ucm9vbUlkKSkgcmV0dXJuO1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IGluZGV4TWFuYWdlci5kZWxldGVFdmVudChldi5nZXRBc3NvY2lhdGVkSWQoKSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEVycm9yIGRlbGV0aW5nIGV2ZW50IGZyb20gaW5kZXhcIiwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBUaGUgUm9vbS50aW1lbGluZVJlc2V0IGxpc3RlbmVyLlxyXG4gICAgICpcclxuICAgICAqIExpc3RlbnMgZm9yIHRpbWVsaW5lIHJlc2V0cyB0aGF0IGFyZSBjYXVzZWQgYnkgYSBsaW1pdGVkIHRpbWVsaW5lIHRvXHJcbiAgICAgKiByZS1hZGQgY2hlY2twb2ludHMgZm9yIHJvb21zIHRoYXQgbmVlZCB0byBiZSBjcmF3bGVkIGFnYWluLlxyXG4gICAgICovXHJcbiAgICBvblRpbWVsaW5lUmVzZXQgPSBhc3luYyAocm9vbSwgdGltZWxpbmVTZXQsIHJlc2V0QWxsVGltZWxpbmVzKSA9PiB7XHJcbiAgICAgICAgaWYgKHJvb20gPT09IG51bGwpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QgaW5kZXhNYW5hZ2VyID0gUGxhdGZvcm1QZWcuZ2V0KCkuZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIoKTtcclxuICAgICAgICBpZiAoIU1hdHJpeENsaWVudFBlZy5nZXQoKS5pc1Jvb21FbmNyeXB0ZWQocm9vbS5yb29tSWQpKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IHRpbWVsaW5lID0gcm9vbS5nZXRMaXZlVGltZWxpbmUoKTtcclxuICAgICAgICBjb25zdCB0b2tlbiA9IHRpbWVsaW5lLmdldFBhZ2luYXRpb25Ub2tlbihcImJcIik7XHJcblxyXG4gICAgICAgIGNvbnN0IGJhY2t3YXJkc0NoZWNrcG9pbnQgPSB7XHJcbiAgICAgICAgICAgIHJvb21JZDogcm9vbS5yb29tSWQsXHJcbiAgICAgICAgICAgIHRva2VuOiB0b2tlbixcclxuICAgICAgICAgICAgZnVsbENyYXdsOiBmYWxzZSxcclxuICAgICAgICAgICAgZGlyZWN0aW9uOiBcImJcIixcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEFkZGVkIGNoZWNrcG9pbnQgYmVjYXVzZSBvZiBhIGxpbWl0ZWQgdGltZWxpbmVcIixcclxuICAgICAgICAgICAgYmFja3dhcmRzQ2hlY2twb2ludCk7XHJcblxyXG4gICAgICAgIGF3YWl0IGluZGV4TWFuYWdlci5hZGRDcmF3bGVyQ2hlY2twb2ludChiYWNrd2FyZHNDaGVja3BvaW50KTtcclxuXHJcbiAgICAgICAgdGhpcy5jcmF3bGVyQ2hlY2twb2ludHMucHVzaChiYWNrd2FyZHNDaGVja3BvaW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIGFuIGV2ZW50IHNob3VsZCBiZSBhZGRlZCB0byB0aGUgZXZlbnQgaW5kZXguXHJcbiAgICAgKlxyXG4gICAgICogTW9zdCBub3RhYmx5IHdlIGZpbHRlciBldmVudHMgZm9yIHdoaWNoIGRlY3J5cHRpb24gZmFpbGVkLCBhcmUgcmVkYWN0ZWRcclxuICAgICAqIG9yIGFyZW4ndCBvZiBhIHR5cGUgdGhhdCB3ZSBrbm93IGhvdyB0byBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge01hdHJpeEV2ZW50fSBldiBUaGUgZXZlbnQgdGhhdCBzaG91bGQgY2hlY2tlZC5cclxuICAgICAqIEByZXR1cm5zIHtib29sfSBSZXR1cm5zIHRydWUgaWYgdGhlIGV2ZW50IGNhbiBiZSBpbmRleGVkLCBmYWxzZVxyXG4gICAgICogb3RoZXJ3aXNlLlxyXG4gICAgICovXHJcbiAgICBpc1ZhbGlkRXZlbnQoZXYpIHtcclxuICAgICAgICBjb25zdCBpc1VzZWZ1bFR5cGUgPSBbXCJtLnJvb20ubWVzc2FnZVwiLCBcIm0ucm9vbS5uYW1lXCIsIFwibS5yb29tLnRvcGljXCJdLmluY2x1ZGVzKGV2LmdldFR5cGUoKSk7XHJcbiAgICAgICAgY29uc3QgdmFsaWRFdmVudFR5cGUgPSBpc1VzZWZ1bFR5cGUgJiYgIWV2LmlzUmVkYWN0ZWQoKSAmJiAhZXYuaXNEZWNyeXB0aW9uRmFpbHVyZSgpO1xyXG5cclxuICAgICAgICBsZXQgdmFsaWRNc2dUeXBlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKGV2LmdldFR5cGUoKSA9PT0gXCJtLnJvb20ubWVzc2FnZVwiICYmICFldi5pc1JlZGFjdGVkKCkpIHtcclxuICAgICAgICAgICAgLy8gRXhwYW5kIHRoaXMgaWYgdGhlcmUgYXJlIG1vcmUgaW52YWxpZCBtc2d0eXBlcy5cclxuICAgICAgICAgICAgY29uc3QgbXNndHlwZSA9IGV2LmdldENvbnRlbnQoKS5tc2d0eXBlO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFtc2d0eXBlKSB2YWxpZE1zZ1R5cGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgZWxzZSB2YWxpZE1zZ1R5cGUgPSAhbXNndHlwZS5zdGFydHNXaXRoKFwibS5rZXkudmVyaWZpY2F0aW9uXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHZhbGlkRXZlbnRUeXBlICYmIHZhbGlkTXNnVHlwZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFF1ZXVlIHVwIGxpdmUgZXZlbnRzIHRvIGJlIGFkZGVkIHRvIHRoZSBldmVudCBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge01hdHJpeEV2ZW50fSBldiBUaGUgZXZlbnQgdGhhdCBzaG91bGQgYmUgYWRkZWQgdG8gdGhlIGluZGV4LlxyXG4gICAgICovXHJcbiAgICBhc3luYyBhZGRMaXZlRXZlbnRUb0luZGV4KGV2KSB7XHJcbiAgICAgICAgY29uc3QgaW5kZXhNYW5hZ2VyID0gUGxhdGZvcm1QZWcuZ2V0KCkuZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIoKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmlzVmFsaWRFdmVudChldikpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc3QganNvbkV2ZW50ID0gZXYudG9KU09OKCk7XHJcbiAgICAgICAgY29uc3QgZSA9IGV2LmlzRW5jcnlwdGVkKCkgPyBqc29uRXZlbnQuZGVjcnlwdGVkIDoganNvbkV2ZW50O1xyXG5cclxuICAgICAgICBjb25zdCBwcm9maWxlID0ge1xyXG4gICAgICAgICAgICBkaXNwbGF5bmFtZTogZXYuc2VuZGVyLnJhd0Rpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICBhdmF0YXJfdXJsOiBldi5zZW5kZXIuZ2V0TXhjQXZhdGFyVXJsKCksXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaW5kZXhNYW5hZ2VyLmFkZEV2ZW50VG9JbmRleChlLCBwcm9maWxlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEVtbWl0IHRoYXQgdGhlIGNyYXdsZXIgaGFzIGNoYW5nZWQgdGhlIGNoZWNrcG9pbnQgdGhhdCBpdCdzIGN1cnJlbnRseVxyXG4gICAgICogaGFuZGxpbmcuXHJcbiAgICAgKi9cclxuICAgIGVtaXROZXdDaGVja3BvaW50KCkge1xyXG4gICAgICAgIHRoaXMuZW1pdChcImNoYW5nZWRDaGVja3BvaW50XCIsIHRoaXMuY3VycmVudFJvb20oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbWFpbiBjcmF3bGVyIGxvb3AuXHJcbiAgICAgKlxyXG4gICAgICogR29lcyB0aHJvdWdoIGNyYXdsZXJDaGVja3BvaW50cyBhbmQgZmV0Y2hlcyBldmVudHMgZnJvbSB0aGUgc2VydmVyIHRvIGJlXHJcbiAgICAgKiBhZGRlZCB0byB0aGUgRXZlbnRJbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBJZiBhIC9yb29tL3tyb29tSWR9L21lc3NhZ2VzIHJlcXVlc3QgZG9lc24ndCBjb250YWluIGFueSBldmVudHMsIHN0b3AgdGhlXHJcbiAgICAgKiBjcmF3bCwgb3RoZXJ3aXNlIGNyZWF0ZSBhIG5ldyBjaGVja3BvaW50IGFuZCBwdXNoIGl0IHRvIHRoZVxyXG4gICAgICogY3Jhd2xlckNoZWNrcG9pbnRzIHF1ZXVlIHNvIHdlIGdvIHRocm91Z2ggdGhlbSBpbiBhIHJvdW5kLXJvYmluIHdheS5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgY3Jhd2xlckZ1bmMoKSB7XHJcbiAgICAgICAgbGV0IGNhbmNlbGxlZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IFN0YXJ0ZWQgY3Jhd2xlciBmdW5jdGlvblwiKTtcclxuXHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX2NyYXdsZXIgPSB7fTtcclxuXHJcbiAgICAgICAgdGhpcy5fY3Jhd2xlci5jYW5jZWwgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNhbmNlbGxlZCA9IHRydWU7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IGlkbGUgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgd2hpbGUgKCFjYW5jZWxsZWQpIHtcclxuICAgICAgICAgICAgbGV0IHNsZWVwVGltZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWVBdChTZXR0aW5nTGV2ZWwuREVWSUNFLCAnY3Jhd2xlclNsZWVwVGltZScpO1xyXG5cclxuICAgICAgICAgICAgLy8gRG9uJ3QgbGV0IHRoZSB1c2VyIGNvbmZpZ3VyZSBhIGxvd2VyIHNsZWVwIHRpbWUgdGhhbiAxMDAgbXMuXHJcbiAgICAgICAgICAgIHNsZWVwVGltZSA9IE1hdGgubWF4KHNsZWVwVGltZSwgMTAwKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChpZGxlKSB7XHJcbiAgICAgICAgICAgICAgICBzbGVlcFRpbWUgPSB0aGlzLl9jcmF3bGVySWRsZVRpbWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9jdXJyZW50Q2hlY2twb2ludCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fY3VycmVudENoZWNrcG9pbnQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0TmV3Q2hlY2twb2ludCgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBhd2FpdCBzbGVlcChzbGVlcFRpbWUpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBSdW5uaW5nIHRoZSBjcmF3bGVyIGxvb3AuXCIpO1xyXG5cclxuICAgICAgICAgICAgaWYgKGNhbmNlbGxlZCkge1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGNoZWNrcG9pbnQgPSB0aGlzLmNyYXdsZXJDaGVja3BvaW50cy5zaGlmdCgpO1xyXG5cclxuICAgICAgICAgICAgLy8vIFRoZXJlIGlzIG5vIGNoZWNrcG9pbnQgYXZhaWxhYmxlIGN1cnJlbnRseSwgb25lIG1heSBhcHBlYXIgaWZcclxuICAgICAgICAgICAgLy8gYSBzeW5jIHdpdGggbGltaXRlZCByb29tIHRpbWVsaW5lcyBoYXBwZW5zLCBzbyBnbyBiYWNrIHRvIHNsZWVwLlxyXG4gICAgICAgICAgICBpZiAoY2hlY2twb2ludCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICBpZGxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9jdXJyZW50Q2hlY2twb2ludCA9IGNoZWNrcG9pbnQ7XHJcbiAgICAgICAgICAgIHRoaXMuZW1pdE5ld0NoZWNrcG9pbnQoKTtcclxuXHJcbiAgICAgICAgICAgIGlkbGUgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogY3Jhd2xpbmcgdXNpbmcgY2hlY2twb2ludFwiLCBjaGVja3BvaW50KTtcclxuXHJcbiAgICAgICAgICAgIC8vIFdlIGhhdmUgYSBjaGVja3BvaW50LCBsZXQgdXMgZmV0Y2ggc29tZSBtZXNzYWdlcywgYWdhaW4sIHZlcnlcclxuICAgICAgICAgICAgLy8gY29uc2VydmF0aXZlbHkgdG8gbm90IGJvdGhlciBvdXIgaG9tZXNlcnZlciB0b28gbXVjaC5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnRNYXBwZXIgPSBjbGllbnQuZ2V0RXZlbnRNYXBwZXIoKTtcclxuICAgICAgICAgICAgLy8gVE9ETyB3ZSBuZWVkIHRvIGVuc3VyZSB0byB1c2UgbWVtYmVyIGxhenkgbG9hZGluZyB3aXRoIHRoaXNcclxuICAgICAgICAgICAgLy8gcmVxdWVzdCBzbyB3ZSBnZXQgdGhlIGNvcnJlY3QgcHJvZmlsZXMuXHJcbiAgICAgICAgICAgIGxldCByZXM7XHJcblxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgcmVzID0gYXdhaXQgY2xpZW50Ll9jcmVhdGVNZXNzYWdlc1JlcXVlc3QoXHJcbiAgICAgICAgICAgICAgICAgICAgY2hlY2twb2ludC5yb29tSWQsIGNoZWNrcG9pbnQudG9rZW4sIHRoaXMuX2V2ZW50c1BlckNyYXdsLFxyXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrcG9pbnQuZGlyZWN0aW9uKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGUuaHR0cFN0YXR1cyA9PT0gNDAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBSZW1vdmluZyBjaGVja3BvaW50IGFzIHdlIGRvbid0IGhhdmUgXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwZXJtaXNzaW9ucyB0byBmZXRjaCBtZXNzYWdlcyBmcm9tIHRoaXMgcm9vbS5cIiwgY2hlY2twb2ludCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLnJlbW92ZUNyYXdsZXJDaGVja3BvaW50KGNoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBFcnJvciByZW1vdmluZyBjaGVja3BvaW50XCIsIGNoZWNrcG9pbnQsIGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBXZSBkb24ndCBwdXNoIHRoZSBjaGVja3BvaW50IGhlcmUgYmFjaywgaXQgd2lsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBob3BlZnVsbHkgYmUgcmVtb3ZlZCBhZnRlciBhIHJlc3RhcnQuIEJ1dCBsZXQgdXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWdub3JlIGl0IGZvciBub3cgYXMgd2UgZG9uJ3Qgd2FudCB0byBoYW1tZXIgdGhlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGVuZHBvaW50LlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEVycm9yIGNyYXdsaW5nIGV2ZW50czpcIiwgZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNyYXdsZXJDaGVja3BvaW50cy5wdXNoKGNoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChjYW5jZWxsZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3Jhd2xlckNoZWNrcG9pbnRzLnB1c2goY2hlY2twb2ludCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHJlcy5jaHVuay5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogRG9uZSB3aXRoIHRoZSBjaGVja3BvaW50XCIsIGNoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgLy8gV2UgZ290IHRvIHRoZSBzdGFydC9lbmQgb2Ygb3VyIHRpbWVsaW5lLCBsZXRzIGp1c3RcclxuICAgICAgICAgICAgICAgIC8vIGRlbGV0ZSBvdXIgY2hlY2twb2ludCBhbmQgZ28gYmFjayB0byBzbGVlcC5cclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLnJlbW92ZUNyYXdsZXJDaGVja3BvaW50KGNoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogRXJyb3IgcmVtb3ZpbmcgY2hlY2twb2ludFwiLCBjaGVja3BvaW50LCBlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBDb252ZXJ0IHRoZSBwbGFpbiBKU09OIGV2ZW50cyBpbnRvIE1hdHJpeCBldmVudHMgc28gdGhleSBnZXRcclxuICAgICAgICAgICAgLy8gZGVjcnlwdGVkIGlmIG5lY2Vzc2FyeS5cclxuICAgICAgICAgICAgY29uc3QgbWF0cml4RXZlbnRzID0gcmVzLmNodW5rLm1hcChldmVudE1hcHBlcik7XHJcbiAgICAgICAgICAgIGxldCBzdGF0ZUV2ZW50cyA9IFtdO1xyXG4gICAgICAgICAgICBpZiAocmVzLnN0YXRlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIHN0YXRlRXZlbnRzID0gcmVzLnN0YXRlLm1hcChldmVudE1hcHBlcik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb2ZpbGVzID0ge307XHJcblxyXG4gICAgICAgICAgICBzdGF0ZUV2ZW50cy5mb3JFYWNoKGV2ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChldi5ldmVudC5jb250ZW50ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgZXYuZXZlbnQuY29udGVudC5tZW1iZXJzaGlwID09PSBcImpvaW5cIikge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb2ZpbGVzW2V2LmV2ZW50LnNlbmRlcl0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXluYW1lOiBldi5ldmVudC5jb250ZW50LmRpc3BsYXluYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdmF0YXJfdXJsOiBldi5ldmVudC5jb250ZW50LmF2YXRhcl91cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkZWNyeXB0aW9uUHJvbWlzZXMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIG1hdHJpeEV2ZW50cy5mb3JFYWNoKGV2ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChldi5pc0JlaW5nRGVjcnlwdGVkKCkgfHwgZXYuaXNEZWNyeXB0aW9uRmFpbHVyZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVE9ETyB0aGUgZGVjcnlwdGlvbiBwcm9taXNlIGlzIGEgcHJpdmF0ZSBwcm9wZXJ0eSwgdGhpc1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNob3VsZCBlaXRoZXIgYmUgbWFkZSBwdWJsaWMgb3Igd2Ugc2hvdWxkIGNvbnZlcnQgdGhlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZXZlbnQgdGhhdCBnZXRzIGZpcmVkIHdoZW4gZGVjcnlwdGlvbiBpcyBkb25lIGludG8gYVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHByb21pc2UgdXNpbmcgdGhlIG9uY2UgZXZlbnQgZW1pdHRlciBtZXRob2Q6XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaHR0cHM6Ly9ub2RlanMub3JnL2FwaS9ldmVudHMuaHRtbCNldmVudHNfZXZlbnRzX29uY2VfZW1pdHRlcl9uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgZGVjcnlwdGlvblByb21pc2VzLnB1c2goZXYuX2RlY3J5cHRpb25Qcm9taXNlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBMZXQgdXMgd2FpdCBmb3IgYWxsIHRoZSBldmVudHMgdG8gZ2V0IGRlY3J5cHRlZC5cclxuICAgICAgICAgICAgYXdhaXQgUHJvbWlzZS5hbGwoZGVjcnlwdGlvblByb21pc2VzKTtcclxuXHJcbiAgICAgICAgICAgIC8vIFRPRE8gaWYgdGhlcmUgYXJlIG5vIGV2ZW50cyBhdCB0aGlzIHBvaW50IHdlJ3JlIG1pc3NpbmcgYSBsb3RcclxuICAgICAgICAgICAgLy8gZGVjcnlwdGlvbiBrZXlzLCBkbyB3ZSB3YW50IHRvIHJldHJ5IHRoaXMgY2hlY2twb2ludCBhdCBhIGxhdGVyXHJcbiAgICAgICAgICAgIC8vIHN0YWdlP1xyXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJlZEV2ZW50cyA9IG1hdHJpeEV2ZW50cy5maWx0ZXIodGhpcy5pc1ZhbGlkRXZlbnQpO1xyXG4gICAgICAgICAgICBjb25zdCB1bmRlY3J5cHRhYmxlRXZlbnRzID0gbWF0cml4RXZlbnRzLmZpbHRlcigoZXYpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBldi5pc0RlY3J5cHRpb25GYWlsdXJlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQ29sbGVjdCB0aGUgcmVkYWN0aW9uIGV2ZW50cyBzbyB3ZSBjYW4gZGVsZXRlIHRoZSByZWRhY3RlZCBldmVudHNcclxuICAgICAgICAgICAgLy8gZnJvbSB0aGUgaW5kZXguXHJcbiAgICAgICAgICAgIGNvbnN0IHJlZGFjdGlvbkV2ZW50cyA9IG1hdHJpeEV2ZW50cy5maWx0ZXIoKGV2KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZXYuZ2V0VHlwZSgpID09PSBcIm0ucm9vbS5yZWRhY3Rpb25cIjtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBMZXQgdXMgY29udmVydCB0aGUgZXZlbnRzIGJhY2sgaW50byBhIGZvcm1hdCB0aGF0IEV2ZW50SW5kZXggY2FuXHJcbiAgICAgICAgICAgIC8vIGNvbnN1bWUuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50cyA9IGZpbHRlcmVkRXZlbnRzLm1hcCgoZXYpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGpzb25FdmVudCA9IGV2LnRvSlNPTigpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZSA9IGV2LmlzRW5jcnlwdGVkKCkgPyBqc29uRXZlbnQuZGVjcnlwdGVkIDoganNvbkV2ZW50O1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBwcm9maWxlID0ge307XHJcbiAgICAgICAgICAgICAgICBpZiAoZS5zZW5kZXIgaW4gcHJvZmlsZXMpIHByb2ZpbGUgPSBwcm9maWxlc1tlLnNlbmRlcl07XHJcbiAgICAgICAgICAgICAgICBjb25zdCBvYmplY3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQ6IGUsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvZmlsZTogcHJvZmlsZSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIENyZWF0ZSBhIG5ldyBjaGVja3BvaW50IHNvIHdlIGNhbiBjb250aW51ZSBjcmF3bGluZyB0aGUgcm9vbSBmb3JcclxuICAgICAgICAgICAgLy8gbWVzc2FnZXMuXHJcbiAgICAgICAgICAgIGNvbnN0IG5ld0NoZWNrcG9pbnQgPSB7XHJcbiAgICAgICAgICAgICAgICByb29tSWQ6IGNoZWNrcG9pbnQucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgdG9rZW46IHJlcy5lbmQsXHJcbiAgICAgICAgICAgICAgICBmdWxsQ3Jhd2w6IGNoZWNrcG9pbnQuZnVsbENyYXdsLFxyXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uOiBjaGVja3BvaW50LmRpcmVjdGlvbixcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAgICAgXCJFdmVudEluZGV4OiBDcmF3bGVkIHJvb21cIixcclxuICAgICAgICAgICAgICAgIGNsaWVudC5nZXRSb29tKGNoZWNrcG9pbnQucm9vbUlkKS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgXCJhbmQgZmV0Y2hlZCB0b3RhbFwiLCBtYXRyaXhFdmVudHMubGVuZ3RoLCBcImV2ZW50cyBvZiB3aGljaFwiLFxyXG4gICAgICAgICAgICAgICAgZXZlbnRzLmxlbmd0aCwgXCJhcmUgYmVpbmcgYWRkZWQsXCIsIHJlZGFjdGlvbkV2ZW50cy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICBcImFyZSByZWRhY3RlZCxcIiwgbWF0cml4RXZlbnRzLmxlbmd0aCAtIGV2ZW50cy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICBcImFyZSBiZWluZyBza2lwcGVkLCB1bmRlY3J5cHRhYmxlXCIsIHVuZGVjcnlwdGFibGVFdmVudHMubGVuZ3RoLFxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVkYWN0aW9uRXZlbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXYgPSByZWRhY3Rpb25FdmVudHNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLmRlbGV0ZUV2ZW50KGV2LmdldEFzc29jaWF0ZWRJZCgpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudHNBbHJlYWR5QWRkZWQgPSBhd2FpdCBpbmRleE1hbmFnZXIuYWRkSGlzdG9yaWNFdmVudHMoXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRzLCBuZXdDaGVja3BvaW50LCBjaGVja3BvaW50KTtcclxuICAgICAgICAgICAgICAgIC8vIElmIGFsbCBldmVudHMgd2VyZSBhbHJlYWR5IGluZGV4ZWQgd2UgYXNzdW1lIHRoYXQgd2UgY2F0Y2hlZFxyXG4gICAgICAgICAgICAgICAgLy8gdXAgd2l0aCBvdXIgaW5kZXggYW5kIGRvbid0IG5lZWQgdG8gY3Jhd2wgdGhlIHJvb20gZnVydGhlci5cclxuICAgICAgICAgICAgICAgIC8vIExldCB1cyBkZWxldGUgdGhlIGNoZWNrcG9pbnQgaW4gdGhhdCBjYXNlLCBvdGhlcndpc2UgcHVzaFxyXG4gICAgICAgICAgICAgICAgLy8gdGhlIG5ldyBjaGVja3BvaW50IHRvIGJlIHVzZWQgYnkgdGhlIGNyYXdsZXIuXHJcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnRzQWxyZWFkeUFkZGVkID09PSB0cnVlICYmIG5ld0NoZWNrcG9pbnQuZnVsbENyYXdsICE9PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBDaGVja3BvaW50IGhhZCBhbHJlYWR5IGFsbCBldmVudHNcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFkZGVkLCBzdG9wcGluZyB0aGUgY3Jhd2xcIiwgY2hlY2twb2ludCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLnJlbW92ZUNyYXdsZXJDaGVja3BvaW50KG5ld0NoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNyYXdsZXJDaGVja3BvaW50cy5wdXNoKG5ld0NoZWNrcG9pbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEVycm9yIGR1cnJpbmcgYSBjcmF3bFwiLCBlKTtcclxuICAgICAgICAgICAgICAgIC8vIEFuIGVycm9yIG9jY3VycmVkLCBwdXQgdGhlIGNoZWNrcG9pbnQgYmFjayBzbyB3ZVxyXG4gICAgICAgICAgICAgICAgLy8gY2FuIHJldHJ5LlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jcmF3bGVyQ2hlY2twb2ludHMucHVzaChjaGVja3BvaW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fY3Jhd2xlciA9IG51bGw7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogU3RvcHBpbmcgY3Jhd2xlciBmdW5jdGlvblwiKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0YXJ0IHRoZSBjcmF3bGVyIGJhY2tncm91bmQgdGFzay5cclxuICAgICAqL1xyXG4gICAgc3RhcnRDcmF3bGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jcmF3bGVyICE9PSBudWxsKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5jcmF3bGVyRnVuYygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RvcCB0aGUgY3Jhd2xlciBiYWNrZ3JvdW5kIHRhc2suXHJcbiAgICAgKi9cclxuICAgIHN0b3BDcmF3bGVyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jcmF3bGVyID09PSBudWxsKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5fY3Jhd2xlci5jYW5jZWwoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsb3NlIHRoZSBldmVudCBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBUaGlzIHJlbW92ZXMgYWxsIHRoZSBNYXRyaXhDbGllbnQgZXZlbnQgbGlzdGVuZXJzLCBzdG9wcyB0aGUgY3Jhd2xlclxyXG4gICAgICogdGFzaywgYW5kIGNsb3NlcyB0aGUgaW5kZXguXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGNsb3NlKCkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcbiAgICAgICAgdGhpcy5yZW1vdmVMaXN0ZW5lcnMoKTtcclxuICAgICAgICB0aGlzLnN0b3BDcmF3bGVyKCk7XHJcbiAgICAgICAgYXdhaXQgaW5kZXhNYW5hZ2VyLmNsb3NlRXZlbnRJbmRleCgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlYXJjaCB0aGUgZXZlbnQgaW5kZXggdXNpbmcgdGhlIGdpdmVuIHRlcm0gZm9yIG1hdGNoaW5nIGV2ZW50cy5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1NlYXJjaEFyZ3N9IHNlYXJjaEFyZ3MgVGhlIHNlYXJjaCBjb25maWd1cmF0aW9uIGZvciB0aGUgc2VhcmNoLFxyXG4gICAgICogc2V0cyB0aGUgc2VhcmNoIHRlcm0gYW5kIGRldGVybWluZXMgdGhlIHNlYXJjaCByZXN1bHQgY29udGVudHMuXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxbU2VhcmNoUmVzdWx0XT59IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhbiBhcnJheVxyXG4gICAgICogb2Ygc2VhcmNoIHJlc3VsdHMgb25jZSB0aGUgc2VhcmNoIGlzIGRvbmUuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIHNlYXJjaChzZWFyY2hBcmdzKSB7XHJcbiAgICAgICAgY29uc3QgaW5kZXhNYW5hZ2VyID0gUGxhdGZvcm1QZWcuZ2V0KCkuZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIoKTtcclxuICAgICAgICByZXR1cm4gaW5kZXhNYW5hZ2VyLnNlYXJjaEV2ZW50SW5kZXgoc2VhcmNoQXJncyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2FkIGV2ZW50cyB0aGF0IGNvbnRhaW4gVVJMcyBmcm9tIHRoZSBldmVudCBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1Jvb219IHJvb20gVGhlIHJvb20gZm9yIHdoaWNoIHdlIHNob3VsZCBmZXRjaCBldmVudHMgY29udGFpbmluZ1xyXG4gICAgICogVVJMc1xyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBsaW1pdCBUaGUgbWF4aW11bSBudW1iZXIgb2YgZXZlbnRzIHRvIGZldGNoLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBmcm9tRXZlbnQgRnJvbSB3aGljaCBldmVudCBzaG91bGQgd2UgY29udGludWUgZmV0Y2hpbmdcclxuICAgICAqIGV2ZW50cyBmcm9tIHRoZSBpbmRleC4gVGhpcyBpcyBvbmx5IG5lZWRlZCBpZiB3ZSdyZSBjb250aW51aW5nIHRvIGZpbGxcclxuICAgICAqIHRoZSB0aW1lbGluZSwgZS5nLiBpZiB3ZSdyZSBwYWdpbmF0aW5nLiBUaGlzIG5lZWRzIHRvIGJlIHNldCB0byBhIGV2ZW50XHJcbiAgICAgKiBpZCBvZiBhbiBldmVudCB0aGF0IHdhcyBwcmV2aW91c2x5IGZldGNoZWQgd2l0aCB0aGlzIGZ1bmN0aW9uLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBkaXJlY3Rpb24gVGhlIGRpcmVjdGlvbiBpbiB3aGljaCB3ZSB3aWxsIGNvbnRpbnVlXHJcbiAgICAgKiBmZXRjaGluZyBldmVudHMuIEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTIHRvIGNvbnRpbnVlIGZldGNoaW5nIGV2ZW50cyB0aGF0XHJcbiAgICAgKiBhcmUgb2xkZXIgdGhhbiB0aGUgZXZlbnQgZ2l2ZW4gaW4gZnJvbUV2ZW50LCBFdmVudFRpbWVsaW5lLkZPUldBUkRTIHRvXHJcbiAgICAgKiBmZXRjaCBuZXdlciBldmVudHMuXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8TWF0cml4RXZlbnRbXT59IFJlc29sdmVzIHRvIGFuIGFycmF5IG9mIGV2ZW50cyB0aGF0XHJcbiAgICAgKiBjb250YWluIFVSTHMuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGxvYWRGaWxlRXZlbnRzKHJvb20sIGxpbWl0ID0gMTAsIGZyb21FdmVudCA9IG51bGwsIGRpcmVjdGlvbiA9IEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTKSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGxvYWRBcmdzID0ge1xyXG4gICAgICAgICAgICByb29tSWQ6IHJvb20ucm9vbUlkLFxyXG4gICAgICAgICAgICBsaW1pdDogbGltaXQsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKGZyb21FdmVudCkge1xyXG4gICAgICAgICAgICBsb2FkQXJncy5mcm9tRXZlbnQgPSBmcm9tRXZlbnQ7XHJcbiAgICAgICAgICAgIGxvYWRBcmdzLmRpcmVjdGlvbiA9IGRpcmVjdGlvbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBldmVudHM7XHJcblxyXG4gICAgICAgIC8vIEdldCBvdXIgZXZlbnRzIGZyb20gdGhlIGV2ZW50IGluZGV4LlxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGV2ZW50cyA9IGF3YWl0IGluZGV4TWFuYWdlci5sb2FkRmlsZUV2ZW50cyhsb2FkQXJncyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50SW5kZXg6IEVycm9yIGdldHRpbmcgZmlsZSBldmVudHNcIiwgZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZW50TWFwcGVyID0gY2xpZW50LmdldEV2ZW50TWFwcGVyKCk7XHJcblxyXG4gICAgICAgIC8vIFR1cm4gdGhlIGV2ZW50cyBpbnRvIE1hdHJpeEV2ZW50IG9iamVjdHMuXHJcbiAgICAgICAgY29uc3QgbWF0cml4RXZlbnRzID0gZXZlbnRzLm1hcChlID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbWF0cml4RXZlbnQgPSBldmVudE1hcHBlcihlLmV2ZW50KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IG5ldyBSb29tTWVtYmVyKHJvb20ucm9vbUlkLCBtYXRyaXhFdmVudC5nZXRTZW5kZXIoKSk7XHJcblxyXG4gICAgICAgICAgICAvLyBXZSBjYW4ndCByZWFsbHkgcmVjb25zdHJ1Y3QgdGhlIHdob2xlIHJvb20gc3RhdGUgZnJvbSBvdXJcclxuICAgICAgICAgICAgLy8gRXZlbnRJbmRleCB0byBjYWxjdWxhdGUgdGhlIGNvcnJlY3QgZGlzcGxheSBuYW1lLiBVc2UgdGhlXHJcbiAgICAgICAgICAgIC8vIGRpc2FtYmlndWF0ZWQgZm9ybSBhbHdheXMgaW5zdGVhZC5cclxuICAgICAgICAgICAgbWVtYmVyLm5hbWUgPSBlLnByb2ZpbGUuZGlzcGxheW5hbWUgKyBcIiAoXCIgKyBtYXRyaXhFdmVudC5nZXRTZW5kZXIoKSArIFwiKVwiO1xyXG5cclxuICAgICAgICAgICAgLy8gVGhpcyBpcyBzZXRzIHRoZSBhdmF0YXIgVVJMLlxyXG4gICAgICAgICAgICBjb25zdCBtZW1iZXJFdmVudCA9IGV2ZW50TWFwcGVyKFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVtYmVyc2hpcDogXCJqb2luXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF2YXRhcl91cmw6IGUucHJvZmlsZS5hdmF0YXJfdXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5bmFtZTogZS5wcm9maWxlLmRpc3BsYXluYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJtLnJvb20ubWVtYmVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRfaWQ6IG1hdHJpeEV2ZW50LmdldElkKCkgKyBcIjpldmVudEluZGV4XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogbWF0cml4RXZlbnQuZ2V0Um9vbUlkKCksXHJcbiAgICAgICAgICAgICAgICAgICAgc2VuZGVyOiBtYXRyaXhFdmVudC5nZXRTZW5kZXIoKSxcclxuICAgICAgICAgICAgICAgICAgICBvcmlnaW5fc2VydmVyX3RzOiBtYXRyaXhFdmVudC5nZXRUcygpLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXRlX2tleTogbWF0cml4RXZlbnQuZ2V0U2VuZGVyKCksXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgLy8gV2Ugc2V0IHRoaXMgbWFudWFsbHkgdG8gYXZvaWQgZW1pdHRpbmcgUm9vbU1lbWJlci5tZW1iZXJzaGlwIGFuZFxyXG4gICAgICAgICAgICAvLyBSb29tTWVtYmVyLm5hbWUgZXZlbnRzLlxyXG4gICAgICAgICAgICBtZW1iZXIuZXZlbnRzLm1lbWJlciA9IG1lbWJlckV2ZW50O1xyXG4gICAgICAgICAgICBtYXRyaXhFdmVudC5zZW5kZXIgPSBtZW1iZXI7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gbWF0cml4RXZlbnQ7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBtYXRyaXhFdmVudHM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGaWxsIGEgdGltZWxpbmUgd2l0aCBldmVudHMgdGhhdCBjb250YWluIFVSTHMuXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtUaW1lbGluZVNldH0gdGltZWxpbmVTZXQgVGhlIFRpbWVsaW5lU2V0IHRoZSBUaW1lbGluZSBiZWxvbmdzIHRvLFxyXG4gICAgICogdXNlZCB0byBjaGVjayBpZiB3ZSdyZSBhZGRpbmcgZHVwbGljYXRlIGV2ZW50cy5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1RpbWVsaW5lfSB0aW1lbGluZSBUaGUgVGltZWxpbmUgd2hpY2ggc2hvdWxkIGJlIGZpbGVkIHdpdGhcclxuICAgICAqIGV2ZW50cy5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1Jvb219IHJvb20gVGhlIHJvb20gZm9yIHdoaWNoIHdlIHNob3VsZCBmZXRjaCBldmVudHMgY29udGFpbmluZ1xyXG4gICAgICogVVJMc1xyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBsaW1pdCBUaGUgbWF4aW11bSBudW1iZXIgb2YgZXZlbnRzIHRvIGZldGNoLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBmcm9tRXZlbnQgRnJvbSB3aGljaCBldmVudCBzaG91bGQgd2UgY29udGludWUgZmV0Y2hpbmdcclxuICAgICAqIGV2ZW50cyBmcm9tIHRoZSBpbmRleC4gVGhpcyBpcyBvbmx5IG5lZWRlZCBpZiB3ZSdyZSBjb250aW51aW5nIHRvIGZpbGxcclxuICAgICAqIHRoZSB0aW1lbGluZSwgZS5nLiBpZiB3ZSdyZSBwYWdpbmF0aW5nLiBUaGlzIG5lZWRzIHRvIGJlIHNldCB0byBhIGV2ZW50XHJcbiAgICAgKiBpZCBvZiBhbiBldmVudCB0aGF0IHdhcyBwcmV2aW91c2x5IGZldGNoZWQgd2l0aCB0aGlzIGZ1bmN0aW9uLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBkaXJlY3Rpb24gVGhlIGRpcmVjdGlvbiBpbiB3aGljaCB3ZSB3aWxsIGNvbnRpbnVlXHJcbiAgICAgKiBmZXRjaGluZyBldmVudHMuIEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTIHRvIGNvbnRpbnVlIGZldGNoaW5nIGV2ZW50cyB0aGF0XHJcbiAgICAgKiBhcmUgb2xkZXIgdGhhbiB0aGUgZXZlbnQgZ2l2ZW4gaW4gZnJvbUV2ZW50LCBFdmVudFRpbWVsaW5lLkZPUldBUkRTIHRvXHJcbiAgICAgKiBmZXRjaCBuZXdlciBldmVudHMuXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8Ym9vbGVhbj59IFJlc29sdmVzIHRvIHRydWUgaWYgZXZlbnRzIHdlcmUgYWRkZWQgdG8gdGhlXHJcbiAgICAgKiB0aW1lbGluZSwgZmFsc2Ugb3RoZXJ3aXNlLlxyXG4gICAgICovXHJcbiAgICBhc3luYyBwb3B1bGF0ZUZpbGVUaW1lbGluZSh0aW1lbGluZVNldCwgdGltZWxpbmUsIHJvb20sIGxpbWl0ID0gMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmcm9tRXZlbnQgPSBudWxsLCBkaXJlY3Rpb24gPSBFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUykge1xyXG4gICAgICAgIGNvbnN0IG1hdHJpeEV2ZW50cyA9IGF3YWl0IHRoaXMubG9hZEZpbGVFdmVudHMocm9vbSwgbGltaXQsIGZyb21FdmVudCwgZGlyZWN0aW9uKTtcclxuXHJcbiAgICAgICAgLy8gSWYgdGhpcyBpcyBhIG5vcm1hbCBmaWxsIHJlcXVlc3QsIG5vdCBhIHBhZ2luYXRpb24gcmVxdWVzdCwgd2UgbmVlZFxyXG4gICAgICAgIC8vIHRvIGdldCBvdXIgZXZlbnRzIGluIHRoZSBCQUNLV0FSRFMgZGlyZWN0aW9uIGJ1dCBwb3B1bGF0ZSB0aGVtIGluIHRoZVxyXG4gICAgICAgIC8vIGZvcndhcmRzIGRpcmVjdGlvbi5cclxuICAgICAgICAvLyBUaGlzIG5lZWRzIHRvIGhhcHBlbiBiZWNhdXNlIGEgZmlsbCByZXF1ZXN0IG1pZ2h0IGNvbWUgd2l0aCBhblxyXG4gICAgICAgIC8vIGV4aXNpdG5nIHRpbWVsaW5lIGUuZy4gaWYgeW91IGNsb3NlIGFuZCByZS1vcGVuIHRoZSBGaWxlUGFuZWwuXHJcbiAgICAgICAgaWYgKGZyb21FdmVudCA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICBtYXRyaXhFdmVudHMucmV2ZXJzZSgpO1xyXG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24gPT0gRXZlbnRUaW1lbGluZS5CQUNLV0FSRFMgPyBFdmVudFRpbWVsaW5lLkZPUldBUkRTOiBFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEFkZCB0aGUgZXZlbnRzIHRvIHRoZSB0aW1lbGluZSBvZiB0aGUgZmlsZSBwYW5lbC5cclxuICAgICAgICBtYXRyaXhFdmVudHMuZm9yRWFjaChlID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aW1lbGluZVNldC5ldmVudElkVG9UaW1lbGluZShlLmdldElkKCkpKSB7XHJcbiAgICAgICAgICAgICAgICB0aW1lbGluZVNldC5hZGRFdmVudFRvVGltZWxpbmUoZSwgdGltZWxpbmUsIGRpcmVjdGlvbiA9PSBFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHJldCA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBwYWdpbmF0aW9uVG9rZW4gPSBcIlwiO1xyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIHBhZ2luYXRpb24gdG9rZW4gdG8gdGhlIG9sZGVzdCBldmVudCB0aGF0IHdlIHJldHJpZXZlZC5cclxuICAgICAgICBpZiAobWF0cml4RXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcGFnaW5hdGlvblRva2VuID0gbWF0cml4RXZlbnRzW21hdHJpeEV2ZW50cy5sZW5ndGggLSAxXS5nZXRJZCgpO1xyXG4gICAgICAgICAgICByZXQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBQb3B1bGF0aW5nIGZpbGUgcGFuZWwgd2l0aFwiLCBtYXRyaXhFdmVudHMubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiZXZlbnRzIGFuZCBzZXR0aW5nIHRoZSBwYWdpbmF0aW9uIHRva2VuIHRvXCIsIHBhZ2luYXRpb25Ub2tlbik7XHJcblxyXG4gICAgICAgIHRpbWVsaW5lLnNldFBhZ2luYXRpb25Ub2tlbihwYWdpbmF0aW9uVG9rZW4sIEV2ZW50VGltZWxpbmUuQkFDS1dBUkRTKTtcclxuICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRW11bGF0ZSBhIFRpbWVsaW5lV2luZG93IHBhZ2luYXRpb24oKSByZXF1ZXN0IHdpdGggdGhlIGV2ZW50IGluZGV4IGFzIHRoZSBldmVudCBzb3VyY2VcclxuICAgICAqXHJcbiAgICAgKiBNaWdodCBub3QgZmV0Y2ggZXZlbnRzIGZyb20gdGhlIGluZGV4IGlmIHRoZSB0aW1lbGluZSBhbHJlYWR5IGNvbnRhaW5zXHJcbiAgICAgKiBldmVudHMgdGhhdCB0aGUgd2luZG93IGlzbid0IHNob3dpbmcuXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtSb29tfSByb29tIFRoZSByb29tIGZvciB3aGljaCB3ZSBzaG91bGQgZmV0Y2ggZXZlbnRzIGNvbnRhaW5pbmdcclxuICAgICAqIFVSTHNcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1RpbWVsaW5lV2luZG93fSB0aW1lbGluZVdpbmRvdyBUaGUgdGltZWxpbmUgd2luZG93IHRoYXQgc2hvdWxkIGJlXHJcbiAgICAgKiBwb3B1bGF0ZWQgd2l0aCBuZXcgZXZlbnRzLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBkaXJlY3Rpb24gVGhlIGRpcmVjdGlvbiBpbiB3aGljaCB3ZSBzaG91bGQgcGFnaW5hdGUuXHJcbiAgICAgKiBFdmVudFRpbWVsaW5lLkJBQ0tXQVJEUyB0byBwYWdpbmF0ZSBiYWNrLCBFdmVudFRpbWVsaW5lLkZPUldBUkRTIHRvXHJcbiAgICAgKiBwYWdpbmF0ZSBmb3J3YXJkcy5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gbGltaXQgVGhlIG1heGltdW0gbnVtYmVyIG9mIGV2ZW50cyB0byBmZXRjaCB3aGlsZVxyXG4gICAgICogcGFnaW5hdGluZy5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxib29sZWFuPn0gUmVzb2x2ZXMgdG8gYSBib29sZWFuIHdoaWNoIGlzIHRydWUgaWYgbW9yZVxyXG4gICAgICogZXZlbnRzIHdlcmUgc3VjY2Vzc2Z1bGx5IHJldHJpZXZlZC5cclxuICAgICAqL1xyXG4gICAgcGFnaW5hdGVUaW1lbGluZVdpbmRvdyhyb29tLCB0aW1lbGluZVdpbmRvdywgZGlyZWN0aW9uLCBsaW1pdCkge1xyXG4gICAgICAgIGNvbnN0IHRsID0gdGltZWxpbmVXaW5kb3cuZ2V0VGltZWxpbmVJbmRleChkaXJlY3Rpb24pO1xyXG5cclxuICAgICAgICBpZiAoIXRsKSByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgICAgICBpZiAodGwucGVuZGluZ1BhZ2luYXRlKSByZXR1cm4gdGwucGVuZGluZ1BhZ2luYXRlO1xyXG5cclxuICAgICAgICBpZiAodGltZWxpbmVXaW5kb3cuZXh0ZW5kKGRpcmVjdGlvbiwgbGltaXQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBwYWdpbmF0aW9uTWV0aG9kID0gYXN5bmMgKHRpbWVsaW5lV2luZG93LCB0aW1lbGluZSwgcm9vbSwgZGlyZWN0aW9uLCBsaW1pdCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB0aW1lbGluZVNldCA9IHRpbWVsaW5lV2luZG93Ll90aW1lbGluZVNldDtcclxuICAgICAgICAgICAgY29uc3QgdG9rZW4gPSB0aW1lbGluZS50aW1lbGluZS5nZXRQYWdpbmF0aW9uVG9rZW4oZGlyZWN0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHJldCA9IGF3YWl0IHRoaXMucG9wdWxhdGVGaWxlVGltZWxpbmUodGltZWxpbmVTZXQsIHRpbWVsaW5lLnRpbWVsaW5lLCByb29tLCBsaW1pdCwgdG9rZW4sIGRpcmVjdGlvbik7XHJcblxyXG4gICAgICAgICAgICB0aW1lbGluZS5wZW5kaW5nUGFnaW5hdGUgPSBudWxsO1xyXG4gICAgICAgICAgICB0aW1lbGluZVdpbmRvdy5leHRlbmQoZGlyZWN0aW9uLCBsaW1pdCk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb25Qcm9taXNlID0gcGFnaW5hdGlvbk1ldGhvZCh0aW1lbGluZVdpbmRvdywgdGwsIHJvb20sIGRpcmVjdGlvbiwgbGltaXQpO1xyXG4gICAgICAgIHRsLnBlbmRpbmdQYWdpbmF0ZSA9IHBhZ2luYXRpb25Qcm9taXNlO1xyXG5cclxuICAgICAgICByZXR1cm4gcGFnaW5hdGlvblByb21pc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgc3RhdGlzdGljYWwgaW5mb3JtYXRpb24gb2YgdGhlIGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2U8SW5kZXhTdGF0cz59IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgaW5kZXhcclxuICAgICAqIHN0YXRpc3RpY3MuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGdldFN0YXRzKCkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4TWFuYWdlciA9IFBsYXRmb3JtUGVnLmdldCgpLmdldEV2ZW50SW5kZXhpbmdNYW5hZ2VyKCk7XHJcbiAgICAgICAgcmV0dXJuIGluZGV4TWFuYWdlci5nZXRTdGF0cygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSByb29tIHRoYXQgd2UgYXJlIGN1cnJlbnRseSBjcmF3bGluZy5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJucyB7Um9vbX0gQSBNYXRyaXhSb29tIHRoYXQgaXMgYmVpbmcgY3VycmVudGx5IGNyYXdsZWQsIG51bGxcclxuICAgICAqIGlmIG5vIHJvb20gaXMgY3VycmVudGx5IGJlaW5nIGNyYXdsZWQuXHJcbiAgICAgKi9cclxuICAgIGN1cnJlbnRSb29tKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jdXJyZW50Q2hlY2twb2ludCA9PT0gbnVsbCAmJiB0aGlzLmNyYXdsZXJDaGVja3BvaW50cy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9jdXJyZW50Q2hlY2twb2ludCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gY2xpZW50LmdldFJvb20odGhpcy5fY3VycmVudENoZWNrcG9pbnQucm9vbUlkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gY2xpZW50LmdldFJvb20odGhpcy5jcmF3bGVyQ2hlY2twb2ludHNbMF0ucm9vbUlkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY3Jhd2xpbmdSb29tcygpIHtcclxuICAgICAgICBjb25zdCB0b3RhbFJvb21zID0gbmV3IFNldCgpO1xyXG4gICAgICAgIGNvbnN0IGNyYXdsaW5nUm9vbXMgPSBuZXcgU2V0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuY3Jhd2xlckNoZWNrcG9pbnRzLmZvckVhY2goKGNoZWNrcG9pbnQsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIGNyYXdsaW5nUm9vbXMuYWRkKGNoZWNrcG9pbnQucm9vbUlkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuX2N1cnJlbnRDaGVja3BvaW50ICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGNyYXdsaW5nUm9vbXMuYWRkKHRoaXMuX2N1cnJlbnRDaGVja3BvaW50LnJvb21JZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgY29uc3Qgcm9vbXMgPSBjbGllbnQuZ2V0Um9vbXMoKTtcclxuXHJcbiAgICAgICAgY29uc3QgaXNSb29tRW5jcnlwdGVkID0gKHJvb20pID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGNsaWVudC5pc1Jvb21FbmNyeXB0ZWQocm9vbS5yb29tSWQpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGVuY3J5cHRlZFJvb21zID0gcm9vbXMuZmlsdGVyKGlzUm9vbUVuY3J5cHRlZCk7XHJcbiAgICAgICAgZW5jcnlwdGVkUm9vbXMuZm9yRWFjaCgocm9vbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgdG90YWxSb29tcy5hZGQocm9vbS5yb29tSWQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4ge2NyYXdsaW5nUm9vbXMsIHRvdGFsUm9vbXN9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==