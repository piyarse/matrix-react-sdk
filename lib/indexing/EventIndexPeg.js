"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _PlatformPeg = _interopRequireDefault(require("../PlatformPeg"));

var _EventIndex = _interopRequireDefault(require("../indexing/EventIndex"));

var _SettingsStore = _interopRequireWildcard(require("../settings/SettingsStore"));

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Object holding the global EventIndex object. Can only be initialized if the
 * platform supports event indexing.
 */
class EventIndexPeg {
  constructor() {
    this.index = null;
    this._supportIsInstalled = false;
  }
  /**
   * Initialize the EventIndexPeg and if event indexing is enabled initialize
   * the event index.
   *
   * @return {Promise<boolean>} A promise that will resolve to true if an
   * EventIndex was successfully initialized, false otherwise.
   */


  async init() {
    if (!_SettingsStore.default.isFeatureEnabled("feature_event_indexing")) {
      return false;
    }

    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    if (!indexManager) {
      console.log("EventIndex: Platform doesn't support event indexing, not initializing.");
      return false;
    }

    this._supportIsInstalled = await indexManager.supportsEventIndexing();

    if (!this.supportIsInstalled()) {
      console.log("EventIndex: Event indexing isn't installed for the platform, not initializing.");
      return false;
    }

    if (!_SettingsStore.default.getValueAt(_SettingsStore.SettingLevel.DEVICE, 'enableEventIndexing')) {
      console.log("EventIndex: Event indexing is disabled, not initializing");
      return false;
    }

    return this.initEventIndex();
  }
  /**
   * Initialize the event index.
   *
   * @returns {boolean} True if the event index was succesfully initialized,
   * false otherwise.
   */


  async initEventIndex() {
    const index = new _EventIndex.default();

    try {
      await index.init();
    } catch (e) {
      console.log("EventIndex: Error initializing the event index", e);
      return false;
    }

    this.index = index;
    return true;
  }
  /**
   * Check if the current platform has support for event indexing.
   *
   * @return {boolean} True if it has support, false otherwise. Note that this
   * does not mean that support is installed.
   */


  platformHasSupport()
  /*: boolean*/
  {
    return _PlatformPeg.default.get().getEventIndexingManager() !== null;
  }
  /**
   * Check if event indexing support is installed for the platfrom.
   *
   * Event indexing might require additional optional modules to be installed,
   * this tells us if those are installed. Note that this should only be
   * called after the init() method was called.
   *
   * @return {boolean} True if support is installed, false otherwise.
   */


  supportIsInstalled()
  /*: boolean*/
  {
    return this._supportIsInstalled;
  }
  /**
   * Get the current event index.
   *
   * @return {EventIndex} The current event index.
   */


  get() {
    return this.index;
  }

  start() {
    if (this.index === null) return;
    this.index.startCrawler();
  }

  stop() {
    if (this.index === null) return;
    this.index.stopCrawler();
  }
  /**
   * Unset our event store
   *
   * After a call to this the init() method will need to be called again.
   *
   * @return {Promise} A promise that will resolve once the event index is
   * closed.
   */


  async unset() {
    if (this.index === null) return;
    await this.index.close();
    this.index = null;
  }
  /**
   * Delete our event indexer.
   *
   * After a call to this the init() method will need to be called again.
   *
   * @return {Promise} A promise that will resolve once the event index is
   * deleted.
   */


  async deleteEventIndex() {
    const indexManager = _PlatformPeg.default.get().getEventIndexingManager();

    if (indexManager !== null) {
      await this.unset();
      console.log("EventIndex: Deleting event index.");
      await indexManager.deleteEventIndex();
    }
  }

}

if (!global.mxEventIndexPeg) {
  global.mxEventIndexPeg = new EventIndexPeg();
}

var _default = global.mxEventIndexPeg;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbmRleGluZy9FdmVudEluZGV4UGVnLmpzIl0sIm5hbWVzIjpbIkV2ZW50SW5kZXhQZWciLCJjb25zdHJ1Y3RvciIsImluZGV4IiwiX3N1cHBvcnRJc0luc3RhbGxlZCIsImluaXQiLCJTZXR0aW5nc1N0b3JlIiwiaXNGZWF0dXJlRW5hYmxlZCIsImluZGV4TWFuYWdlciIsIlBsYXRmb3JtUGVnIiwiZ2V0IiwiZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIiLCJjb25zb2xlIiwibG9nIiwic3VwcG9ydHNFdmVudEluZGV4aW5nIiwic3VwcG9ydElzSW5zdGFsbGVkIiwiZ2V0VmFsdWVBdCIsIlNldHRpbmdMZXZlbCIsIkRFVklDRSIsImluaXRFdmVudEluZGV4IiwiRXZlbnRJbmRleCIsImUiLCJwbGF0Zm9ybUhhc1N1cHBvcnQiLCJzdGFydCIsInN0YXJ0Q3Jhd2xlciIsInN0b3AiLCJzdG9wQ3Jhd2xlciIsInVuc2V0IiwiY2xvc2UiLCJkZWxldGVFdmVudEluZGV4IiwiZ2xvYmFsIiwibXhFdmVudEluZGV4UGVnIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQXFCQTs7QUFDQTs7QUFDQTs7QUF2QkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7Ozs7QUFTQSxNQUFNQSxhQUFOLENBQW9CO0FBQ2hCQyxFQUFBQSxXQUFXLEdBQUc7QUFDVixTQUFLQyxLQUFMLEdBQWEsSUFBYjtBQUNBLFNBQUtDLG1CQUFMLEdBQTJCLEtBQTNCO0FBQ0g7QUFFRDs7Ozs7Ozs7O0FBT0EsUUFBTUMsSUFBTixHQUFhO0FBQ1QsUUFBSSxDQUFDQyx1QkFBY0MsZ0JBQWQsQ0FBK0Isd0JBQS9CLENBQUwsRUFBK0Q7QUFDM0QsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsVUFBTUMsWUFBWSxHQUFHQyxxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLEVBQXJCOztBQUNBLFFBQUksQ0FBQ0gsWUFBTCxFQUFtQjtBQUNmSSxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSx3RUFBWjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFNBQUtULG1CQUFMLEdBQTJCLE1BQU1JLFlBQVksQ0FBQ00scUJBQWIsRUFBakM7O0FBRUEsUUFBSSxDQUFDLEtBQUtDLGtCQUFMLEVBQUwsRUFBZ0M7QUFDNUJILE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdGQUFaO0FBQ0EsYUFBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBSSxDQUFDUCx1QkFBY1UsVUFBZCxDQUF5QkMsNEJBQWFDLE1BQXRDLEVBQThDLHFCQUE5QyxDQUFMLEVBQTJFO0FBQ3ZFTixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSwwREFBWjtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFdBQU8sS0FBS00sY0FBTCxFQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQSxRQUFNQSxjQUFOLEdBQXVCO0FBQ25CLFVBQU1oQixLQUFLLEdBQUcsSUFBSWlCLG1CQUFKLEVBQWQ7O0FBRUEsUUFBSTtBQUNBLFlBQU1qQixLQUFLLENBQUNFLElBQU4sRUFBTjtBQUNILEtBRkQsQ0FFRSxPQUFPZ0IsQ0FBUCxFQUFVO0FBQ1JULE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdEQUFaLEVBQThEUSxDQUE5RDtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFNBQUtsQixLQUFMLEdBQWFBLEtBQWI7QUFFQSxXQUFPLElBQVA7QUFDSDtBQUVEOzs7Ozs7OztBQU1BbUIsRUFBQUEsa0JBQWtCO0FBQUE7QUFBWTtBQUMxQixXQUFPYixxQkFBWUMsR0FBWixHQUFrQkMsdUJBQWxCLE9BQWdELElBQXZEO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7QUFTQUksRUFBQUEsa0JBQWtCO0FBQUE7QUFBWTtBQUMxQixXQUFPLEtBQUtYLG1CQUFaO0FBQ0g7QUFFRDs7Ozs7OztBQUtBTSxFQUFBQSxHQUFHLEdBQUc7QUFDRixXQUFPLEtBQUtQLEtBQVo7QUFDSDs7QUFFRG9CLEVBQUFBLEtBQUssR0FBRztBQUNKLFFBQUksS0FBS3BCLEtBQUwsS0FBZSxJQUFuQixFQUF5QjtBQUN6QixTQUFLQSxLQUFMLENBQVdxQixZQUFYO0FBQ0g7O0FBRURDLEVBQUFBLElBQUksR0FBRztBQUNILFFBQUksS0FBS3RCLEtBQUwsS0FBZSxJQUFuQixFQUF5QjtBQUN6QixTQUFLQSxLQUFMLENBQVd1QixXQUFYO0FBQ0g7QUFFRDs7Ozs7Ozs7OztBQVFBLFFBQU1DLEtBQU4sR0FBYztBQUNWLFFBQUksS0FBS3hCLEtBQUwsS0FBZSxJQUFuQixFQUF5QjtBQUN6QixVQUFNLEtBQUtBLEtBQUwsQ0FBV3lCLEtBQVgsRUFBTjtBQUNBLFNBQUt6QixLQUFMLEdBQWEsSUFBYjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7QUFRQSxRQUFNMEIsZ0JBQU4sR0FBeUI7QUFDckIsVUFBTXJCLFlBQVksR0FBR0MscUJBQVlDLEdBQVosR0FBa0JDLHVCQUFsQixFQUFyQjs7QUFFQSxRQUFJSCxZQUFZLEtBQUssSUFBckIsRUFBMkI7QUFDdkIsWUFBTSxLQUFLbUIsS0FBTCxFQUFOO0FBQ0FmLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG1DQUFaO0FBQ0EsWUFBTUwsWUFBWSxDQUFDcUIsZ0JBQWIsRUFBTjtBQUNIO0FBQ0o7O0FBcEllOztBQXVJcEIsSUFBSSxDQUFDQyxNQUFNLENBQUNDLGVBQVosRUFBNkI7QUFDekJELEVBQUFBLE1BQU0sQ0FBQ0MsZUFBUCxHQUF5QixJQUFJOUIsYUFBSixFQUF6QjtBQUNIOztlQUNjNkIsTUFBTSxDQUFDQyxlIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG4vKlxyXG4gKiBPYmplY3QgaG9sZGluZyB0aGUgZ2xvYmFsIEV2ZW50SW5kZXggb2JqZWN0LiBDYW4gb25seSBiZSBpbml0aWFsaXplZCBpZiB0aGVcclxuICogcGxhdGZvcm0gc3VwcG9ydHMgZXZlbnQgaW5kZXhpbmcuXHJcbiAqL1xyXG5cclxuaW1wb3J0IFBsYXRmb3JtUGVnIGZyb20gXCIuLi9QbGF0Zm9ybVBlZ1wiO1xyXG5pbXBvcnQgRXZlbnRJbmRleCBmcm9tIFwiLi4vaW5kZXhpbmcvRXZlbnRJbmRleFwiO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSAnLi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZSc7XHJcblxyXG5jbGFzcyBFdmVudEluZGV4UGVnIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuaW5kZXggPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3N1cHBvcnRJc0luc3RhbGxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5pdGlhbGl6ZSB0aGUgRXZlbnRJbmRleFBlZyBhbmQgaWYgZXZlbnQgaW5kZXhpbmcgaXMgZW5hYmxlZCBpbml0aWFsaXplXHJcbiAgICAgKiB0aGUgZXZlbnQgaW5kZXguXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxib29sZWFuPn0gQSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRydWUgaWYgYW5cclxuICAgICAqIEV2ZW50SW5kZXggd2FzIHN1Y2Nlc3NmdWxseSBpbml0aWFsaXplZCwgZmFsc2Ugb3RoZXJ3aXNlLlxyXG4gICAgICovXHJcbiAgICBhc3luYyBpbml0KCkge1xyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmVFbmFibGVkKFwiZmVhdHVyZV9ldmVudF9pbmRleGluZ1wiKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBpbmRleE1hbmFnZXIgPSBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRFdmVudEluZGV4aW5nTWFuYWdlcigpO1xyXG4gICAgICAgIGlmICghaW5kZXhNYW5hZ2VyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZlbnRJbmRleDogUGxhdGZvcm0gZG9lc24ndCBzdXBwb3J0IGV2ZW50IGluZGV4aW5nLCBub3QgaW5pdGlhbGl6aW5nLlwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fc3VwcG9ydElzSW5zdGFsbGVkID0gYXdhaXQgaW5kZXhNYW5hZ2VyLnN1cHBvcnRzRXZlbnRJbmRleGluZygpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3VwcG9ydElzSW5zdGFsbGVkKCkpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBFdmVudCBpbmRleGluZyBpc24ndCBpbnN0YWxsZWQgZm9yIHRoZSBwbGF0Zm9ybSwgbm90IGluaXRpYWxpemluZy5cIik7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZUF0KFNldHRpbmdMZXZlbC5ERVZJQ0UsICdlbmFibGVFdmVudEluZGV4aW5nJykpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBFdmVudCBpbmRleGluZyBpcyBkaXNhYmxlZCwgbm90IGluaXRpYWxpemluZ1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5pdEV2ZW50SW5kZXgoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEluaXRpYWxpemUgdGhlIGV2ZW50IGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBldmVudCBpbmRleCB3YXMgc3VjY2VzZnVsbHkgaW5pdGlhbGl6ZWQsXHJcbiAgICAgKiBmYWxzZSBvdGhlcndpc2UuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGluaXRFdmVudEluZGV4KCkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gbmV3IEV2ZW50SW5kZXgoKTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgaW5kZXguaW5pdCgpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBFcnJvciBpbml0aWFsaXppbmcgdGhlIGV2ZW50IGluZGV4XCIsIGUpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmluZGV4ID0gaW5kZXg7XHJcblxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgdGhlIGN1cnJlbnQgcGxhdGZvcm0gaGFzIHN1cHBvcnQgZm9yIGV2ZW50IGluZGV4aW5nLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59IFRydWUgaWYgaXQgaGFzIHN1cHBvcnQsIGZhbHNlIG90aGVyd2lzZS4gTm90ZSB0aGF0IHRoaXNcclxuICAgICAqIGRvZXMgbm90IG1lYW4gdGhhdCBzdXBwb3J0IGlzIGluc3RhbGxlZC5cclxuICAgICAqL1xyXG4gICAgcGxhdGZvcm1IYXNTdXBwb3J0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRFdmVudEluZGV4aW5nTWFuYWdlcigpICE9PSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgZXZlbnQgaW5kZXhpbmcgc3VwcG9ydCBpcyBpbnN0YWxsZWQgZm9yIHRoZSBwbGF0ZnJvbS5cclxuICAgICAqXHJcbiAgICAgKiBFdmVudCBpbmRleGluZyBtaWdodCByZXF1aXJlIGFkZGl0aW9uYWwgb3B0aW9uYWwgbW9kdWxlcyB0byBiZSBpbnN0YWxsZWQsXHJcbiAgICAgKiB0aGlzIHRlbGxzIHVzIGlmIHRob3NlIGFyZSBpbnN0YWxsZWQuIE5vdGUgdGhhdCB0aGlzIHNob3VsZCBvbmx5IGJlXHJcbiAgICAgKiBjYWxsZWQgYWZ0ZXIgdGhlIGluaXQoKSBtZXRob2Qgd2FzIGNhbGxlZC5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIHN1cHBvcnQgaXMgaW5zdGFsbGVkLCBmYWxzZSBvdGhlcndpc2UuXHJcbiAgICAgKi9cclxuICAgIHN1cHBvcnRJc0luc3RhbGxlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3VwcG9ydElzSW5zdGFsbGVkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBjdXJyZW50IGV2ZW50IGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge0V2ZW50SW5kZXh9IFRoZSBjdXJyZW50IGV2ZW50IGluZGV4LlxyXG4gICAgICovXHJcbiAgICBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5kZXg7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5kZXggPT09IG51bGwpIHJldHVybjtcclxuICAgICAgICB0aGlzLmluZGV4LnN0YXJ0Q3Jhd2xlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3AoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5kZXggPT09IG51bGwpIHJldHVybjtcclxuICAgICAgICB0aGlzLmluZGV4LnN0b3BDcmF3bGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVbnNldCBvdXIgZXZlbnQgc3RvcmVcclxuICAgICAqXHJcbiAgICAgKiBBZnRlciBhIGNhbGwgdG8gdGhpcyB0aGUgaW5pdCgpIG1ldGhvZCB3aWxsIG5lZWQgdG8gYmUgY2FsbGVkIGFnYWluLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSBvbmNlIHRoZSBldmVudCBpbmRleCBpc1xyXG4gICAgICogY2xvc2VkLlxyXG4gICAgICovXHJcbiAgICBhc3luYyB1bnNldCgpIHtcclxuICAgICAgICBpZiAodGhpcy5pbmRleCA9PT0gbnVsbCkgcmV0dXJuO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuaW5kZXguY2xvc2UoKTtcclxuICAgICAgICB0aGlzLmluZGV4ID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZSBvdXIgZXZlbnQgaW5kZXhlci5cclxuICAgICAqXHJcbiAgICAgKiBBZnRlciBhIGNhbGwgdG8gdGhpcyB0aGUgaW5pdCgpIG1ldGhvZCB3aWxsIG5lZWQgdG8gYmUgY2FsbGVkIGFnYWluLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSBvbmNlIHRoZSBldmVudCBpbmRleCBpc1xyXG4gICAgICogZGVsZXRlZC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgZGVsZXRlRXZlbnRJbmRleCgpIHtcclxuICAgICAgICBjb25zdCBpbmRleE1hbmFnZXIgPSBQbGF0Zm9ybVBlZy5nZXQoKS5nZXRFdmVudEluZGV4aW5nTWFuYWdlcigpO1xyXG5cclxuICAgICAgICBpZiAoaW5kZXhNYW5hZ2VyICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMudW5zZXQoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdmVudEluZGV4OiBEZWxldGluZyBldmVudCBpbmRleC5cIik7XHJcbiAgICAgICAgICAgIGF3YWl0IGluZGV4TWFuYWdlci5kZWxldGVFdmVudEluZGV4KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5pZiAoIWdsb2JhbC5teEV2ZW50SW5kZXhQZWcpIHtcclxuICAgIGdsb2JhbC5teEV2ZW50SW5kZXhQZWcgPSBuZXcgRXZlbnRJbmRleFBlZygpO1xyXG59XHJcbmV4cG9ydCBkZWZhdWx0IGdsb2JhbC5teEV2ZW50SW5kZXhQZWc7XHJcbiJdfQ==