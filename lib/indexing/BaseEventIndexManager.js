"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*:: export interface MatrixEvent {
    type: string;
    sender: string;
    content: {};
    event_id: string;
    origin_server_ts: number;
    unsigned: ?{};
    room_id: string;
}*/

/*:: export interface MatrixProfile {
    avatar_url: string;
    displayname: string;
}*/

/*:: export interface CrawlerCheckpoint {
    roomId: string;
    token: string;
    fullCrawl: boolean;
    direction: string;
}*/

/*:: export interface ResultContext {
    events_before: [MatrixEvent];
    events_after: [MatrixEvent];
    profile_info: Map<string, MatrixProfile>;
}*/

/*:: export interface ResultsElement {
    rank: number;
    result: MatrixEvent;
    context: ResultContext;
}*/

/*:: export interface SearchResult {
    count: number;
    results: [ResultsElement];
    highlights: [string];
}*/

/*:: export interface SearchArgs {
    search_term: string;
    before_limit: number;
    after_limit: number;
    order_by_recency: boolean;
    room_id: ?string;
}*/

/*:: export interface EventAndProfile {
    event: MatrixEvent;
    profile: MatrixProfile;
}*/

/*:: export interface LoadArgs {
    roomId: string;
    limit: number;
    fromEvent: string;
    direction: string;
}*/

/*:: export interface IndexStats {
    size: number;
    event_count: number;
    room_count: number;
}*/

/**
 * Base class for classes that provide platform-specific event indexing.
 *
 * Instances of this class are provided by the application.
 */
class BaseEventIndexManager {
  /**
   * Does our EventIndexManager support event indexing.
   *
   * If an EventIndexManager implementor has runtime dependencies that
   * optionally enable event indexing they may override this method to perform
   * the necessary runtime checks here.
   *
   * @return {Promise} A promise that will resolve to true if event indexing
   * is supported, false otherwise.
   */
  async supportsEventIndexing()
  /*: Promise<boolean>*/
  {
    return true;
  }
  /**
   * Initialize the event index for the given user.
   *
   * @return {Promise} A promise that will resolve when the event index is
   * initialized.
   */


  async initEventIndex()
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Queue up an event to be added to the index.
   *
   * @param {MatrixEvent} ev The event that should be added to the index.
   * @param {MatrixProfile} profile The profile of the event sender at the
   * time of the event receival.
   *
   * @return {Promise} A promise that will resolve when the was queued up for
   * addition.
   */


  async addEventToIndex(ev
  /*: MatrixEvent*/
  , profile
  /*: MatrixProfile*/
  )
  /*: Promise<>*/
  {
    throw new Error("Unimplemented");
  }

  async deleteEvent(eventId
  /*: string*/
  )
  /*: Promise<boolean>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Check if our event index is empty.
   */


  indexIsEmpty()
  /*: Promise<boolean>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Get statistical information of the index.
   *
   * @return {Promise<IndexStats>} A promise that will resolve to the index
   * statistics.
   */


  async getStats()
  /*: Promise<IndexStats>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Commit the previously queued up events to the index.
   *
   * @return {Promise} A promise that will resolve once the queued up events
   * were added to the index.
   */


  async commitLiveEvents()
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Search the event index using the given term for matching events.
   *
   * @param {SearchArgs} searchArgs The search configuration for the search,
   * sets the search term and determines the search result contents.
   *
   * @return {Promise<[SearchResult]>} A promise that will resolve to an array
   * of search results once the search is done.
   */


  async searchEventIndex(searchArgs
  /*: SearchArgs*/
  )
  /*: Promise<SearchResult>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Add events from the room history to the event index.
   *
   * This is used to add a batch of events to the index.
   *
   * @param {[EventAndProfile]} events The list of events and profiles that
   * should be added to the event index.
   * @param {[CrawlerCheckpoint]} checkpoint A new crawler checkpoint that
   * should be stored in the index which should be used to continue crawling
   * the room.
   * @param {[CrawlerCheckpoint]} oldCheckpoint The checkpoint that was used
   * to fetch the current batch of events. This checkpoint will be removed
   * from the index.
   *
   * @return {Promise} A promise that will resolve to true if all the events
   * were already added to the index, false otherwise.
   */


  async addHistoricEvents(events
  /*: [EventAndProfile]*/
  , checkpoint
  /*: CrawlerCheckpoint | null*/
  , oldCheckpoint
  /*: CrawlerCheckpoint | null*/
  )
  /*: Promise<bool>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Add a new crawler checkpoint to the index.
   *
   * @param {CrawlerCheckpoint} checkpoint The checkpoint that should be added
   * to the index.
   *
   * @return {Promise} A promise that will resolve once the checkpoint has
   * been stored.
   */


  async addCrawlerCheckpoint(checkpoint
  /*: CrawlerCheckpoint*/
  )
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Add a new crawler checkpoint to the index.
   *
   * @param {CrawlerCheckpoint} checkpoint The checkpoint that should be
   * removed from the index.
   *
   * @return {Promise} A promise that will resolve once the checkpoint has
   * been removed.
   */


  async removeCrawlerCheckpoint(checkpoint
  /*: CrawlerCheckpoint*/
  )
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Load the stored checkpoints from the index.
   *
   * @return {Promise<[CrawlerCheckpoint]>} A promise that will resolve to an
   * array of crawler checkpoints once they have been loaded from the index.
   */


  async loadCheckpoints()
  /*: Promise<[CrawlerCheckpoint]>*/
  {
    throw new Error("Unimplemented");
  }
  /** Load events that contain an mxc URL to a file from the index.
   *
   * @param  {object} args Arguments object for the method.
   * @param  {string} args.roomId The ID of the room for which the events
   * should be loaded.
   * @param  {number} args.limit The maximum number of events to return.
   * @param  {string} args.fromEvent An event id of a previous event returned
   * by this method. Passing this means that we are going to continue loading
   * events from this point in the history.
   * @param  {string} args.direction The direction to which we should continue
   * loading events from. This is used only if fromEvent is used as well.
   *
   * @return {Promise<[EventAndProfile]>} A promise that will resolve to an
   * array of Matrix events that contain mxc URLs accompanied with the
   * historic profile of the sender.
   */


  async loadFileEvents(args
  /*: LoadArgs*/
  )
  /*: Promise<[EventAndProfile]>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * close our event index.
   *
   * @return {Promise} A promise that will resolve once the event index has
   * been closed.
   */


  async closeEventIndex()
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Delete our current event index.
   *
   * @return {Promise} A promise that will resolve once the event index has
   * been deleted.
   */


  async deleteEventIndex()
  /*: Promise<void>*/
  {
    throw new Error("Unimplemented");
  }

}

exports.default = BaseEventIndexManager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbmRleGluZy9CYXNlRXZlbnRJbmRleE1hbmFnZXIuanMiXSwibmFtZXMiOlsiQmFzZUV2ZW50SW5kZXhNYW5hZ2VyIiwic3VwcG9ydHNFdmVudEluZGV4aW5nIiwiaW5pdEV2ZW50SW5kZXgiLCJFcnJvciIsImFkZEV2ZW50VG9JbmRleCIsImV2IiwicHJvZmlsZSIsImRlbGV0ZUV2ZW50IiwiZXZlbnRJZCIsImluZGV4SXNFbXB0eSIsImdldFN0YXRzIiwiY29tbWl0TGl2ZUV2ZW50cyIsInNlYXJjaEV2ZW50SW5kZXgiLCJzZWFyY2hBcmdzIiwiYWRkSGlzdG9yaWNFdmVudHMiLCJldmVudHMiLCJjaGVja3BvaW50Iiwib2xkQ2hlY2twb2ludCIsImFkZENyYXdsZXJDaGVja3BvaW50IiwicmVtb3ZlQ3Jhd2xlckNoZWNrcG9pbnQiLCJsb2FkQ2hlY2twb2ludHMiLCJsb2FkRmlsZUV2ZW50cyIsImFyZ3MiLCJjbG9zZUV2ZW50SW5kZXgiLCJkZWxldGVFdmVudEluZGV4Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrRkE7Ozs7O0FBS2UsTUFBTUEscUJBQU4sQ0FBNEI7QUFDdkM7Ozs7Ozs7Ozs7QUFVQSxRQUFNQyxxQkFBTjtBQUFBO0FBQWdEO0FBQzVDLFdBQU8sSUFBUDtBQUNIO0FBQ0Q7Ozs7Ozs7O0FBTUEsUUFBTUMsY0FBTjtBQUFBO0FBQXNDO0FBQ2xDLFVBQU0sSUFBSUMsS0FBSixDQUFVLGVBQVYsQ0FBTjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVBLFFBQU1DLGVBQU4sQ0FBc0JDO0FBQXRCO0FBQUEsSUFBdUNDO0FBQXZDO0FBQUE7QUFBQTtBQUEwRTtBQUN0RSxVQUFNLElBQUlILEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDs7QUFFRCxRQUFNSSxXQUFOLENBQWtCQztBQUFsQjtBQUFBO0FBQUE7QUFBcUQ7QUFDakQsVUFBTSxJQUFJTCxLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7QUFHQU0sRUFBQUEsWUFBWTtBQUFBO0FBQXFCO0FBQzdCLFVBQU0sSUFBSU4sS0FBSixDQUFVLGVBQVYsQ0FBTjtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUEsUUFBTU8sUUFBTjtBQUFBO0FBQXNDO0FBQ2xDLFVBQU0sSUFBSVAsS0FBSixDQUFVLGVBQVYsQ0FBTjtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUEsUUFBTVEsZ0JBQU47QUFBQTtBQUF3QztBQUNwQyxVQUFNLElBQUlSLEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDtBQUVEOzs7Ozs7Ozs7OztBQVNBLFFBQU1TLGdCQUFOLENBQXVCQztBQUF2QjtBQUFBO0FBQUE7QUFBc0U7QUFDbEUsVUFBTSxJQUFJVixLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxRQUFNVyxpQkFBTixDQUNJQztBQURKO0FBQUEsSUFFSUM7QUFGSjtBQUFBLElBR0lDO0FBSEo7QUFBQTtBQUFBO0FBSWlCO0FBQ2IsVUFBTSxJQUFJZCxLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7QUFTQSxRQUFNZSxvQkFBTixDQUEyQkY7QUFBM0I7QUFBQTtBQUFBO0FBQXlFO0FBQ3JFLFVBQU0sSUFBSWIsS0FBSixDQUFVLGVBQVYsQ0FBTjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7O0FBU0EsUUFBTWdCLHVCQUFOLENBQThCSDtBQUE5QjtBQUFBO0FBQUE7QUFBNEU7QUFDeEUsVUFBTSxJQUFJYixLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQSxRQUFNaUIsZUFBTjtBQUFBO0FBQXNEO0FBQ2xELFVBQU0sSUFBSWpCLEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkEsUUFBTWtCLGNBQU4sQ0FBcUJDO0FBQXJCO0FBQUE7QUFBQTtBQUFpRTtBQUM3RCxVQUFNLElBQUluQixLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQSxRQUFNb0IsZUFBTjtBQUFBO0FBQXVDO0FBQ25DLFVBQU0sSUFBSXBCLEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDtBQUVEOzs7Ozs7OztBQU1BLFFBQU1xQixnQkFBTjtBQUFBO0FBQXdDO0FBQ3BDLFVBQU0sSUFBSXJCLEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDs7QUFyTHNDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE1hdHJpeEV2ZW50IHtcclxuICAgIHR5cGU6IHN0cmluZztcclxuICAgIHNlbmRlcjogc3RyaW5nO1xyXG4gICAgY29udGVudDoge307XHJcbiAgICBldmVudF9pZDogc3RyaW5nO1xyXG4gICAgb3JpZ2luX3NlcnZlcl90czogbnVtYmVyO1xyXG4gICAgdW5zaWduZWQ6ID97fTtcclxuICAgIHJvb21faWQ6IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBNYXRyaXhQcm9maWxlIHtcclxuICAgIGF2YXRhcl91cmw6IHN0cmluZztcclxuICAgIGRpc3BsYXluYW1lOiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQ3Jhd2xlckNoZWNrcG9pbnQge1xyXG4gICAgcm9vbUlkOiBzdHJpbmc7XHJcbiAgICB0b2tlbjogc3RyaW5nO1xyXG4gICAgZnVsbENyYXdsOiBib29sZWFuO1xyXG4gICAgZGlyZWN0aW9uOiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgUmVzdWx0Q29udGV4dCB7XHJcbiAgICBldmVudHNfYmVmb3JlOiBbTWF0cml4RXZlbnRdO1xyXG4gICAgZXZlbnRzX2FmdGVyOiBbTWF0cml4RXZlbnRdO1xyXG4gICAgcHJvZmlsZV9pbmZvOiBNYXA8c3RyaW5nLCBNYXRyaXhQcm9maWxlPjtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBSZXN1bHRzRWxlbWVudCB7XHJcbiAgICByYW5rOiBudW1iZXI7XHJcbiAgICByZXN1bHQ6IE1hdHJpeEV2ZW50O1xyXG4gICAgY29udGV4dDogUmVzdWx0Q29udGV4dDtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWFyY2hSZXN1bHQge1xyXG4gICAgY291bnQ6IG51bWJlcjtcclxuICAgIHJlc3VsdHM6IFtSZXN1bHRzRWxlbWVudF07XHJcbiAgICBoaWdobGlnaHRzOiBbc3RyaW5nXTtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWFyY2hBcmdzIHtcclxuICAgIHNlYXJjaF90ZXJtOiBzdHJpbmc7XHJcbiAgICBiZWZvcmVfbGltaXQ6IG51bWJlcjtcclxuICAgIGFmdGVyX2xpbWl0OiBudW1iZXI7XHJcbiAgICBvcmRlcl9ieV9yZWNlbmN5OiBib29sZWFuO1xyXG4gICAgcm9vbV9pZDogP3N0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBFdmVudEFuZFByb2ZpbGUge1xyXG4gICAgZXZlbnQ6IE1hdHJpeEV2ZW50O1xyXG4gICAgcHJvZmlsZTogTWF0cml4UHJvZmlsZTtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBMb2FkQXJncyB7XHJcbiAgICByb29tSWQ6IHN0cmluZztcclxuICAgIGxpbWl0OiBudW1iZXI7XHJcbiAgICBmcm9tRXZlbnQ6IHN0cmluZztcclxuICAgIGRpcmVjdGlvbjogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEluZGV4U3RhdHMge1xyXG4gICAgc2l6ZTogbnVtYmVyO1xyXG4gICAgZXZlbnRfY291bnQ6IG51bWJlcjtcclxuICAgIHJvb21fY291bnQ6IG51bWJlcjtcclxufVxyXG5cclxuLyoqXHJcbiAqIEJhc2UgY2xhc3MgZm9yIGNsYXNzZXMgdGhhdCBwcm92aWRlIHBsYXRmb3JtLXNwZWNpZmljIGV2ZW50IGluZGV4aW5nLlxyXG4gKlxyXG4gKiBJbnN0YW5jZXMgb2YgdGhpcyBjbGFzcyBhcmUgcHJvdmlkZWQgYnkgdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZUV2ZW50SW5kZXhNYW5hZ2VyIHtcclxuICAgIC8qKlxyXG4gICAgICogRG9lcyBvdXIgRXZlbnRJbmRleE1hbmFnZXIgc3VwcG9ydCBldmVudCBpbmRleGluZy5cclxuICAgICAqXHJcbiAgICAgKiBJZiBhbiBFdmVudEluZGV4TWFuYWdlciBpbXBsZW1lbnRvciBoYXMgcnVudGltZSBkZXBlbmRlbmNpZXMgdGhhdFxyXG4gICAgICogb3B0aW9uYWxseSBlbmFibGUgZXZlbnQgaW5kZXhpbmcgdGhleSBtYXkgb3ZlcnJpZGUgdGhpcyBtZXRob2QgdG8gcGVyZm9ybVxyXG4gICAgICogdGhlIG5lY2Vzc2FyeSBydW50aW1lIGNoZWNrcyBoZXJlLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0cnVlIGlmIGV2ZW50IGluZGV4aW5nXHJcbiAgICAgKiBpcyBzdXBwb3J0ZWQsIGZhbHNlIG90aGVyd2lzZS5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgc3VwcG9ydHNFdmVudEluZGV4aW5nKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBJbml0aWFsaXplIHRoZSBldmVudCBpbmRleCBmb3IgdGhlIGdpdmVuIHVzZXIuXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdoZW4gdGhlIGV2ZW50IGluZGV4IGlzXHJcbiAgICAgKiBpbml0aWFsaXplZC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgaW5pdEV2ZW50SW5kZXgoKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVW5pbXBsZW1lbnRlZFwiKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFF1ZXVlIHVwIGFuIGV2ZW50IHRvIGJlIGFkZGVkIHRvIHRoZSBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge01hdHJpeEV2ZW50fSBldiBUaGUgZXZlbnQgdGhhdCBzaG91bGQgYmUgYWRkZWQgdG8gdGhlIGluZGV4LlxyXG4gICAgICogQHBhcmFtIHtNYXRyaXhQcm9maWxlfSBwcm9maWxlIFRoZSBwcm9maWxlIG9mIHRoZSBldmVudCBzZW5kZXIgYXQgdGhlXHJcbiAgICAgKiB0aW1lIG9mIHRoZSBldmVudCByZWNlaXZhbC5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSBBIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgd2hlbiB0aGUgd2FzIHF1ZXVlZCB1cCBmb3JcclxuICAgICAqIGFkZGl0aW9uLlxyXG4gICAgICovXHJcbiAgICBhc3luYyBhZGRFdmVudFRvSW5kZXgoZXY6IE1hdHJpeEV2ZW50LCBwcm9maWxlOiBNYXRyaXhQcm9maWxlKTogUHJvbWlzZTw+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGRlbGV0ZUV2ZW50KGV2ZW50SWQ6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiBvdXIgZXZlbnQgaW5kZXggaXMgZW1wdHkuXHJcbiAgICAgKi9cclxuICAgIGluZGV4SXNFbXB0eSgpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHN0YXRpc3RpY2FsIGluZm9ybWF0aW9uIG9mIHRoZSBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPEluZGV4U3RhdHM+fSBBIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGluZGV4XHJcbiAgICAgKiBzdGF0aXN0aWNzLlxyXG4gICAgICovXHJcbiAgICBhc3luYyBnZXRTdGF0cygpOiBQcm9taXNlPEluZGV4U3RhdHM+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29tbWl0IHRoZSBwcmV2aW91c2x5IHF1ZXVlZCB1cCBldmVudHMgdG8gdGhlIGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSBvbmNlIHRoZSBxdWV1ZWQgdXAgZXZlbnRzXHJcbiAgICAgKiB3ZXJlIGFkZGVkIHRvIHRoZSBpbmRleC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgY29tbWl0TGl2ZUV2ZW50cygpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VhcmNoIHRoZSBldmVudCBpbmRleCB1c2luZyB0aGUgZ2l2ZW4gdGVybSBmb3IgbWF0Y2hpbmcgZXZlbnRzLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7U2VhcmNoQXJnc30gc2VhcmNoQXJncyBUaGUgc2VhcmNoIGNvbmZpZ3VyYXRpb24gZm9yIHRoZSBzZWFyY2gsXHJcbiAgICAgKiBzZXRzIHRoZSBzZWFyY2ggdGVybSBhbmQgZGV0ZXJtaW5lcyB0aGUgc2VhcmNoIHJlc3VsdCBjb250ZW50cy5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPFtTZWFyY2hSZXN1bHRdPn0gQSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGFuIGFycmF5XHJcbiAgICAgKiBvZiBzZWFyY2ggcmVzdWx0cyBvbmNlIHRoZSBzZWFyY2ggaXMgZG9uZS5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgc2VhcmNoRXZlbnRJbmRleChzZWFyY2hBcmdzOiBTZWFyY2hBcmdzKTogUHJvbWlzZTxTZWFyY2hSZXN1bHQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGV2ZW50cyBmcm9tIHRoZSByb29tIGhpc3RvcnkgdG8gdGhlIGV2ZW50IGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIFRoaXMgaXMgdXNlZCB0byBhZGQgYSBiYXRjaCBvZiBldmVudHMgdG8gdGhlIGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7W0V2ZW50QW5kUHJvZmlsZV19IGV2ZW50cyBUaGUgbGlzdCBvZiBldmVudHMgYW5kIHByb2ZpbGVzIHRoYXRcclxuICAgICAqIHNob3VsZCBiZSBhZGRlZCB0byB0aGUgZXZlbnQgaW5kZXguXHJcbiAgICAgKiBAcGFyYW0ge1tDcmF3bGVyQ2hlY2twb2ludF19IGNoZWNrcG9pbnQgQSBuZXcgY3Jhd2xlciBjaGVja3BvaW50IHRoYXRcclxuICAgICAqIHNob3VsZCBiZSBzdG9yZWQgaW4gdGhlIGluZGV4IHdoaWNoIHNob3VsZCBiZSB1c2VkIHRvIGNvbnRpbnVlIGNyYXdsaW5nXHJcbiAgICAgKiB0aGUgcm9vbS5cclxuICAgICAqIEBwYXJhbSB7W0NyYXdsZXJDaGVja3BvaW50XX0gb2xkQ2hlY2twb2ludCBUaGUgY2hlY2twb2ludCB0aGF0IHdhcyB1c2VkXHJcbiAgICAgKiB0byBmZXRjaCB0aGUgY3VycmVudCBiYXRjaCBvZiBldmVudHMuIFRoaXMgY2hlY2twb2ludCB3aWxsIGJlIHJlbW92ZWRcclxuICAgICAqIGZyb20gdGhlIGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0cnVlIGlmIGFsbCB0aGUgZXZlbnRzXHJcbiAgICAgKiB3ZXJlIGFscmVhZHkgYWRkZWQgdG8gdGhlIGluZGV4LCBmYWxzZSBvdGhlcndpc2UuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGFkZEhpc3RvcmljRXZlbnRzKFxyXG4gICAgICAgIGV2ZW50czogW0V2ZW50QW5kUHJvZmlsZV0sXHJcbiAgICAgICAgY2hlY2twb2ludDogQ3Jhd2xlckNoZWNrcG9pbnQgfCBudWxsLFxyXG4gICAgICAgIG9sZENoZWNrcG9pbnQ6IENyYXdsZXJDaGVja3BvaW50IHwgbnVsbCxcclxuICAgICk6IFByb21pc2U8Ym9vbD4ge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgYSBuZXcgY3Jhd2xlciBjaGVja3BvaW50IHRvIHRoZSBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge0NyYXdsZXJDaGVja3BvaW50fSBjaGVja3BvaW50IFRoZSBjaGVja3BvaW50IHRoYXQgc2hvdWxkIGJlIGFkZGVkXHJcbiAgICAgKiB0byB0aGUgaW5kZXguXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIG9uY2UgdGhlIGNoZWNrcG9pbnQgaGFzXHJcbiAgICAgKiBiZWVuIHN0b3JlZC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgYWRkQ3Jhd2xlckNoZWNrcG9pbnQoY2hlY2twb2ludDogQ3Jhd2xlckNoZWNrcG9pbnQpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgbmV3IGNyYXdsZXIgY2hlY2twb2ludCB0byB0aGUgaW5kZXguXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtDcmF3bGVyQ2hlY2twb2ludH0gY2hlY2twb2ludCBUaGUgY2hlY2twb2ludCB0aGF0IHNob3VsZCBiZVxyXG4gICAgICogcmVtb3ZlZCBmcm9tIHRoZSBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSBBIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgb25jZSB0aGUgY2hlY2twb2ludCBoYXNcclxuICAgICAqIGJlZW4gcmVtb3ZlZC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgcmVtb3ZlQ3Jhd2xlckNoZWNrcG9pbnQoY2hlY2twb2ludDogQ3Jhd2xlckNoZWNrcG9pbnQpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9hZCB0aGUgc3RvcmVkIGNoZWNrcG9pbnRzIGZyb20gdGhlIGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2U8W0NyYXdsZXJDaGVja3BvaW50XT59IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhblxyXG4gICAgICogYXJyYXkgb2YgY3Jhd2xlciBjaGVja3BvaW50cyBvbmNlIHRoZXkgaGF2ZSBiZWVuIGxvYWRlZCBmcm9tIHRoZSBpbmRleC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgbG9hZENoZWNrcG9pbnRzKCk6IFByb21pc2U8W0NyYXdsZXJDaGVja3BvaW50XT4ge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIExvYWQgZXZlbnRzIHRoYXQgY29udGFpbiBhbiBteGMgVVJMIHRvIGEgZmlsZSBmcm9tIHRoZSBpbmRleC5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gIHtvYmplY3R9IGFyZ3MgQXJndW1lbnRzIG9iamVjdCBmb3IgdGhlIG1ldGhvZC5cclxuICAgICAqIEBwYXJhbSAge3N0cmluZ30gYXJncy5yb29tSWQgVGhlIElEIG9mIHRoZSByb29tIGZvciB3aGljaCB0aGUgZXZlbnRzXHJcbiAgICAgKiBzaG91bGQgYmUgbG9hZGVkLlxyXG4gICAgICogQHBhcmFtICB7bnVtYmVyfSBhcmdzLmxpbWl0IFRoZSBtYXhpbXVtIG51bWJlciBvZiBldmVudHMgdG8gcmV0dXJuLlxyXG4gICAgICogQHBhcmFtICB7c3RyaW5nfSBhcmdzLmZyb21FdmVudCBBbiBldmVudCBpZCBvZiBhIHByZXZpb3VzIGV2ZW50IHJldHVybmVkXHJcbiAgICAgKiBieSB0aGlzIG1ldGhvZC4gUGFzc2luZyB0aGlzIG1lYW5zIHRoYXQgd2UgYXJlIGdvaW5nIHRvIGNvbnRpbnVlIGxvYWRpbmdcclxuICAgICAqIGV2ZW50cyBmcm9tIHRoaXMgcG9pbnQgaW4gdGhlIGhpc3RvcnkuXHJcbiAgICAgKiBAcGFyYW0gIHtzdHJpbmd9IGFyZ3MuZGlyZWN0aW9uIFRoZSBkaXJlY3Rpb24gdG8gd2hpY2ggd2Ugc2hvdWxkIGNvbnRpbnVlXHJcbiAgICAgKiBsb2FkaW5nIGV2ZW50cyBmcm9tLiBUaGlzIGlzIHVzZWQgb25seSBpZiBmcm9tRXZlbnQgaXMgdXNlZCBhcyB3ZWxsLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2U8W0V2ZW50QW5kUHJvZmlsZV0+fSBBIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYW5cclxuICAgICAqIGFycmF5IG9mIE1hdHJpeCBldmVudHMgdGhhdCBjb250YWluIG14YyBVUkxzIGFjY29tcGFuaWVkIHdpdGggdGhlXHJcbiAgICAgKiBoaXN0b3JpYyBwcm9maWxlIG9mIHRoZSBzZW5kZXIuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGxvYWRGaWxlRXZlbnRzKGFyZ3M6IExvYWRBcmdzKTogUHJvbWlzZTxbRXZlbnRBbmRQcm9maWxlXT4ge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBjbG9zZSBvdXIgZXZlbnQgaW5kZXguXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIG9uY2UgdGhlIGV2ZW50IGluZGV4IGhhc1xyXG4gICAgICogYmVlbiBjbG9zZWQuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGNsb3NlRXZlbnRJbmRleCgpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVsZXRlIG91ciBjdXJyZW50IGV2ZW50IGluZGV4LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSBvbmNlIHRoZSBldmVudCBpbmRleCBoYXNcclxuICAgICAqIGJlZW4gZGVsZXRlZC5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgZGVsZXRlRXZlbnRJbmRleCgpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmltcGxlbWVudGVkXCIpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==