"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MatrixClientPeg = require("./MatrixClientPeg");

var _PlatformPeg = _interopRequireDefault(require("./PlatformPeg"));

var TextForEvent = _interopRequireWildcard(require("./TextForEvent"));

var _Analytics = _interopRequireDefault(require("./Analytics"));

var Avatar = _interopRequireWildcard(require("./Avatar"));

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var sdk = _interopRequireWildcard(require("./index"));

var _languageHandler = require("./languageHandler");

var _Modal = _interopRequireDefault(require("./Modal"));

var _SettingsStore = _interopRequireWildcard(require("./settings/SettingsStore"));

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
 * Dispatches:
 * {
 *   action: "notifier_enabled",
 *   value: boolean
 * }
 */
const MAX_PENDING_ENCRYPTED = 20;
const Notifier = {
  notifsByRoom: {},
  // A list of event IDs that we've received but need to wait until
  // they're decrypted until we decide whether to notify for them
  // or not
  pendingEncryptedEventIds: [],
  notificationMessageForEvent: function (ev) {
    return TextForEvent.textForEvent(ev);
  },
  _displayPopupNotification: function (ev, room) {
    const plaf = _PlatformPeg.default.get();

    if (!plaf) {
      return;
    }

    if (!plaf.supportsNotifications() || !plaf.maySendNotifications()) {
      return;
    }

    if (global.document.hasFocus()) {
      return;
    }

    let msg = this.notificationMessageForEvent(ev);
    if (!msg) return;
    let title;

    if (!ev.sender || room.name === ev.sender.name) {
      title = room.name; // notificationMessageForEvent includes sender,
      // but we already have the sender here

      if (ev.getContent().body) msg = ev.getContent().body;
    } else if (ev.getType() === 'm.room.member') {
      // context is all in the message here, we don't need
      // to display sender info
      title = room.name;
    } else if (ev.sender) {
      title = ev.sender.name + " (" + room.name + ")"; // notificationMessageForEvent includes sender,
      // but we've just out sender in the title

      if (ev.getContent().body) msg = ev.getContent().body;
    }

    if (!this.isBodyEnabled()) {
      msg = '';
    }

    let avatarUrl = null;

    if (ev.sender && !_SettingsStore.default.getValue("lowBandwidth")) {
      avatarUrl = Avatar.avatarUrlForMember(ev.sender, 40, 40, 'crop');
    }

    const notif = plaf.displayNotification(title, msg, avatarUrl, room); // if displayNotification returns non-null,  the platform supports
    // clearing notifications later, so keep track of this.

    if (notif) {
      if (this.notifsByRoom[ev.getRoomId()] === undefined) this.notifsByRoom[ev.getRoomId()] = [];
      this.notifsByRoom[ev.getRoomId()].push(notif);
    }
  },
  getSoundForRoom: async function (roomId) {
    // We do no caching here because the SDK caches setting
    // and the browser will cache the sound.
    const content = _SettingsStore.default.getValue("notificationSound", roomId);

    if (!content) {
      return null;
    }

    if (!content.url) {
      console.warn("".concat(roomId, " has custom notification sound event, but no url key"));
      return null;
    }

    if (!content.url.startsWith("mxc://")) {
      console.warn("".concat(roomId, " has custom notification sound event, but url is not a mxc url"));
      return null;
    } // Ideally in here we could use MSC1310 to detect the type of file, and reject it.


    return {
      url: _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(content.url),
      name: content.name,
      type: content.type,
      size: content.size
    };
  },
  _playAudioNotification: async function (ev, room) {
    const sound = await this.getSoundForRoom(room.roomId);
    console.log("Got sound ".concat(sound && sound.name || "default", " for ").concat(room.roomId));

    try {
      const selector = document.querySelector(sound ? "audio[src='".concat(sound.url, "']") : "#messageAudio");
      let audioElement = selector;

      if (!selector) {
        if (!sound) {
          console.error("No audio element or sound to play for notification");
          return;
        }

        audioElement = new Audio(sound.url);

        if (sound.type) {
          audioElement.type = sound.type;
        }

        document.body.appendChild(audioElement);
      }

      await audioElement.play();
    } catch (ex) {
      console.warn("Caught error when trying to fetch room notification sound:", ex);
    }
  },
  start: function () {
    // do not re-bind in the case of repeated call
    this.boundOnEvent = this.boundOnEvent || this.onEvent.bind(this);
    this.boundOnSyncStateChange = this.boundOnSyncStateChange || this.onSyncStateChange.bind(this);
    this.boundOnRoomReceipt = this.boundOnRoomReceipt || this.onRoomReceipt.bind(this);
    this.boundOnEventDecrypted = this.boundOnEventDecrypted || this.onEventDecrypted.bind(this);

    _MatrixClientPeg.MatrixClientPeg.get().on('event', this.boundOnEvent);

    _MatrixClientPeg.MatrixClientPeg.get().on('Room.receipt', this.boundOnRoomReceipt);

    _MatrixClientPeg.MatrixClientPeg.get().on('Event.decrypted', this.boundOnEventDecrypted);

    _MatrixClientPeg.MatrixClientPeg.get().on("sync", this.boundOnSyncStateChange);

    this.toolbarHidden = false;
    this.isSyncing = false;
  },
  stop: function () {
    if (_MatrixClientPeg.MatrixClientPeg.get()) {
      _MatrixClientPeg.MatrixClientPeg.get().removeListener('Event', this.boundOnEvent);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener('Room.receipt', this.boundOnRoomReceipt);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener('Event.decrypted', this.boundOnEventDecrypted);

      _MatrixClientPeg.MatrixClientPeg.get().removeListener('sync', this.boundOnSyncStateChange);
    }

    this.isSyncing = false;
  },
  supportsDesktopNotifications: function () {
    const plaf = _PlatformPeg.default.get();

    return plaf && plaf.supportsNotifications();
  },
  setEnabled: function (enable, callback) {
    const plaf = _PlatformPeg.default.get();

    if (!plaf) return; // Dev note: We don't set the "notificationsEnabled" setting to true here because it is a
    // calculated value. It is determined based upon whether or not the master rule is enabled
    // and other flags. Setting it here would cause a circular reference.

    _Analytics.default.trackEvent('Notifier', 'Set Enabled', enable); // make sure that we persist the current setting audio_enabled setting
    // before changing anything


    if (_SettingsStore.default.isLevelSupported(_SettingsStore.SettingLevel.DEVICE)) {
      _SettingsStore.default.setValue("audioNotificationsEnabled", null, _SettingsStore.SettingLevel.DEVICE, this.isEnabled());
    }

    if (enable) {
      // Attempt to get permission from user
      plaf.requestNotificationPermission().then(result => {
        if (result !== 'granted') {
          // The permission request was dismissed or denied
          // TODO: Support alternative branding in messaging
          const description = result === 'denied' ? (0, _languageHandler._t)('Riot does not have permission to send you notifications - ' + 'please check your browser settings') : (0, _languageHandler._t)('Riot was not given permission to send notifications - please try again');
          const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

          _Modal.default.createTrackedDialog('Unable to enable Notifications', result, ErrorDialog, {
            title: (0, _languageHandler._t)('Unable to enable Notifications'),
            description
          });

          return;
        }

        if (callback) callback();

        _dispatcher.default.dispatch({
          action: "notifier_enabled",
          value: true
        });
      });
    } else {
      _dispatcher.default.dispatch({
        action: "notifier_enabled",
        value: false
      });
    } // set the notifications_hidden flag, as the user has knowingly interacted
    // with the setting we shouldn't nag them any further


    this.setToolbarHidden(true);
  },
  isEnabled: function () {
    return this.isPossible() && _SettingsStore.default.getValue("notificationsEnabled");
  },
  isPossible: function () {
    const plaf = _PlatformPeg.default.get();

    if (!plaf) return false;
    if (!plaf.supportsNotifications()) return false;
    if (!plaf.maySendNotifications()) return false;
    return true; // possible, but not necessarily enabled
  },
  isBodyEnabled: function () {
    return this.isEnabled() && _SettingsStore.default.getValue("notificationBodyEnabled");
  },
  isAudioEnabled: function () {
    return this.isEnabled() && _SettingsStore.default.getValue("audioNotificationsEnabled");
  },
  setToolbarHidden: function (hidden, persistent = true) {
    this.toolbarHidden = hidden;

    _Analytics.default.trackEvent('Notifier', 'Set Toolbar Hidden', hidden); // XXX: why are we dispatching this here?
    // this is nothing to do with notifier_enabled


    _dispatcher.default.dispatch({
      action: "notifier_enabled",
      value: this.isEnabled()
    }); // update the info to localStorage for persistent settings


    if (persistent && global.localStorage) {
      global.localStorage.setItem("notifications_hidden", hidden);
    }
  },
  shouldShowToolbar: function () {
    const client = _MatrixClientPeg.MatrixClientPeg.get();

    if (!client) {
      return false;
    }

    const isGuest = client.isGuest();
    return !isGuest && this.supportsDesktopNotifications() && !this.isEnabled() && !this._isToolbarHidden();
  },
  _isToolbarHidden: function () {
    // Check localStorage for any such meta data
    if (global.localStorage) {
      return global.localStorage.getItem("notifications_hidden") === "true";
    }

    return this.toolbarHidden;
  },
  onSyncStateChange: function (state) {
    if (state === "SYNCING") {
      this.isSyncing = true;
    } else if (state === "STOPPED" || state === "ERROR") {
      this.isSyncing = false;
    }
  },
  onEvent: function (ev) {
    if (!this.isSyncing) return; // don't alert for any messages initially

    if (ev.sender && ev.sender.userId === _MatrixClientPeg.MatrixClientPeg.get().credentials.userId) return; // If it's an encrypted event and the type is still 'm.room.encrypted',
    // it hasn't yet been decrypted, so wait until it is.

    if (ev.isBeingDecrypted() || ev.isDecryptionFailure()) {
      this.pendingEncryptedEventIds.push(ev.getId()); // don't let the list fill up indefinitely

      while (this.pendingEncryptedEventIds.length > MAX_PENDING_ENCRYPTED) {
        this.pendingEncryptedEventIds.shift();
      }

      return;
    }

    this._evaluateEvent(ev);
  },
  onEventDecrypted: function (ev) {
    // 'decrypted' means the decryption process has finished: it may have failed,
    // in which case it might decrypt soon if the keys arrive
    if (ev.isDecryptionFailure()) return;
    const idx = this.pendingEncryptedEventIds.indexOf(ev.getId());
    if (idx === -1) return;
    this.pendingEncryptedEventIds.splice(idx, 1);

    this._evaluateEvent(ev);
  },
  onRoomReceipt: function (ev, room) {
    if (room.getUnreadNotificationCount() === 0) {
      // ideally we would clear each notification when it was read,
      // but we have no way, given a read receipt, to know whether
      // the receipt comes before or after an event, so we can't
      // do this. Instead, clear all notifications for a room once
      // there are no notifs left in that room., which is not quite
      // as good but it's something.
      const plaf = _PlatformPeg.default.get();

      if (!plaf) return;
      if (this.notifsByRoom[room.roomId] === undefined) return;

      for (const notif of this.notifsByRoom[room.roomId]) {
        plaf.clearNotification(notif);
      }

      delete this.notifsByRoom[room.roomId];
    }
  },
  _evaluateEvent: function (ev) {
    const room = _MatrixClientPeg.MatrixClientPeg.get().getRoom(ev.getRoomId());

    const actions = _MatrixClientPeg.MatrixClientPeg.get().getPushActionsForEvent(ev);

    if (actions && actions.notify) {
      if (this.isEnabled()) {
        this._displayPopupNotification(ev, room);
      }

      if (actions.tweaks.sound && this.isAudioEnabled()) {
        _PlatformPeg.default.get().loudNotification(ev, room);

        this._playAudioNotification(ev, room);
      }
    }
  }
};

if (!global.mxNotifier) {
  global.mxNotifier = Notifier;
}

var _default = global.mxNotifier;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Ob3RpZmllci5qcyJdLCJuYW1lcyI6WyJNQVhfUEVORElOR19FTkNSWVBURUQiLCJOb3RpZmllciIsIm5vdGlmc0J5Um9vbSIsInBlbmRpbmdFbmNyeXB0ZWRFdmVudElkcyIsIm5vdGlmaWNhdGlvbk1lc3NhZ2VGb3JFdmVudCIsImV2IiwiVGV4dEZvckV2ZW50IiwidGV4dEZvckV2ZW50IiwiX2Rpc3BsYXlQb3B1cE5vdGlmaWNhdGlvbiIsInJvb20iLCJwbGFmIiwiUGxhdGZvcm1QZWciLCJnZXQiLCJzdXBwb3J0c05vdGlmaWNhdGlvbnMiLCJtYXlTZW5kTm90aWZpY2F0aW9ucyIsImdsb2JhbCIsImRvY3VtZW50IiwiaGFzRm9jdXMiLCJtc2ciLCJ0aXRsZSIsInNlbmRlciIsIm5hbWUiLCJnZXRDb250ZW50IiwiYm9keSIsImdldFR5cGUiLCJpc0JvZHlFbmFibGVkIiwiYXZhdGFyVXJsIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwiQXZhdGFyIiwiYXZhdGFyVXJsRm9yTWVtYmVyIiwibm90aWYiLCJkaXNwbGF5Tm90aWZpY2F0aW9uIiwiZ2V0Um9vbUlkIiwidW5kZWZpbmVkIiwicHVzaCIsImdldFNvdW5kRm9yUm9vbSIsInJvb21JZCIsImNvbnRlbnQiLCJ1cmwiLCJjb25zb2xlIiwid2FybiIsInN0YXJ0c1dpdGgiLCJNYXRyaXhDbGllbnRQZWciLCJteGNVcmxUb0h0dHAiLCJ0eXBlIiwic2l6ZSIsIl9wbGF5QXVkaW9Ob3RpZmljYXRpb24iLCJzb3VuZCIsImxvZyIsInNlbGVjdG9yIiwicXVlcnlTZWxlY3RvciIsImF1ZGlvRWxlbWVudCIsImVycm9yIiwiQXVkaW8iLCJhcHBlbmRDaGlsZCIsInBsYXkiLCJleCIsInN0YXJ0IiwiYm91bmRPbkV2ZW50Iiwib25FdmVudCIsImJpbmQiLCJib3VuZE9uU3luY1N0YXRlQ2hhbmdlIiwib25TeW5jU3RhdGVDaGFuZ2UiLCJib3VuZE9uUm9vbVJlY2VpcHQiLCJvblJvb21SZWNlaXB0IiwiYm91bmRPbkV2ZW50RGVjcnlwdGVkIiwib25FdmVudERlY3J5cHRlZCIsIm9uIiwidG9vbGJhckhpZGRlbiIsImlzU3luY2luZyIsInN0b3AiLCJyZW1vdmVMaXN0ZW5lciIsInN1cHBvcnRzRGVza3RvcE5vdGlmaWNhdGlvbnMiLCJzZXRFbmFibGVkIiwiZW5hYmxlIiwiY2FsbGJhY2siLCJBbmFseXRpY3MiLCJ0cmFja0V2ZW50IiwiaXNMZXZlbFN1cHBvcnRlZCIsIlNldHRpbmdMZXZlbCIsIkRFVklDRSIsInNldFZhbHVlIiwiaXNFbmFibGVkIiwicmVxdWVzdE5vdGlmaWNhdGlvblBlcm1pc3Npb24iLCJ0aGVuIiwicmVzdWx0IiwiZGVzY3JpcHRpb24iLCJFcnJvckRpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwidmFsdWUiLCJzZXRUb29sYmFySGlkZGVuIiwiaXNQb3NzaWJsZSIsImlzQXVkaW9FbmFibGVkIiwiaGlkZGVuIiwicGVyc2lzdGVudCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJzaG91bGRTaG93VG9vbGJhciIsImNsaWVudCIsImlzR3Vlc3QiLCJfaXNUb29sYmFySGlkZGVuIiwiZ2V0SXRlbSIsInN0YXRlIiwidXNlcklkIiwiY3JlZGVudGlhbHMiLCJpc0JlaW5nRGVjcnlwdGVkIiwiaXNEZWNyeXB0aW9uRmFpbHVyZSIsImdldElkIiwibGVuZ3RoIiwic2hpZnQiLCJfZXZhbHVhdGVFdmVudCIsImlkeCIsImluZGV4T2YiLCJzcGxpY2UiLCJnZXRVbnJlYWROb3RpZmljYXRpb25Db3VudCIsImNsZWFyTm90aWZpY2F0aW9uIiwiZ2V0Um9vbSIsImFjdGlvbnMiLCJnZXRQdXNoQWN0aW9uc0ZvckV2ZW50Iiwibm90aWZ5IiwidHdlYWtzIiwibG91ZE5vdGlmaWNhdGlvbiIsIm14Tm90aWZpZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTNCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBOzs7Ozs7O0FBUUEsTUFBTUEscUJBQXFCLEdBQUcsRUFBOUI7QUFFQSxNQUFNQyxRQUFRLEdBQUc7QUFDYkMsRUFBQUEsWUFBWSxFQUFFLEVBREQ7QUFHYjtBQUNBO0FBQ0E7QUFDQUMsRUFBQUEsd0JBQXdCLEVBQUUsRUFOYjtBQVFiQyxFQUFBQSwyQkFBMkIsRUFBRSxVQUFTQyxFQUFULEVBQWE7QUFDdEMsV0FBT0MsWUFBWSxDQUFDQyxZQUFiLENBQTBCRixFQUExQixDQUFQO0FBQ0gsR0FWWTtBQVliRyxFQUFBQSx5QkFBeUIsRUFBRSxVQUFTSCxFQUFULEVBQWFJLElBQWIsRUFBbUI7QUFDMUMsVUFBTUMsSUFBSSxHQUFHQyxxQkFBWUMsR0FBWixFQUFiOztBQUNBLFFBQUksQ0FBQ0YsSUFBTCxFQUFXO0FBQ1A7QUFDSDs7QUFDRCxRQUFJLENBQUNBLElBQUksQ0FBQ0cscUJBQUwsRUFBRCxJQUFpQyxDQUFDSCxJQUFJLENBQUNJLG9CQUFMLEVBQXRDLEVBQW1FO0FBQy9EO0FBQ0g7O0FBQ0QsUUFBSUMsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxRQUFoQixFQUFKLEVBQWdDO0FBQzVCO0FBQ0g7O0FBRUQsUUFBSUMsR0FBRyxHQUFHLEtBQUtkLDJCQUFMLENBQWlDQyxFQUFqQyxDQUFWO0FBQ0EsUUFBSSxDQUFDYSxHQUFMLEVBQVU7QUFFVixRQUFJQyxLQUFKOztBQUNBLFFBQUksQ0FBQ2QsRUFBRSxDQUFDZSxNQUFKLElBQWNYLElBQUksQ0FBQ1ksSUFBTCxLQUFjaEIsRUFBRSxDQUFDZSxNQUFILENBQVVDLElBQTFDLEVBQWdEO0FBQzVDRixNQUFBQSxLQUFLLEdBQUdWLElBQUksQ0FBQ1ksSUFBYixDQUQ0QyxDQUU1QztBQUNBOztBQUNBLFVBQUloQixFQUFFLENBQUNpQixVQUFILEdBQWdCQyxJQUFwQixFQUEwQkwsR0FBRyxHQUFHYixFQUFFLENBQUNpQixVQUFILEdBQWdCQyxJQUF0QjtBQUM3QixLQUxELE1BS08sSUFBSWxCLEVBQUUsQ0FBQ21CLE9BQUgsT0FBaUIsZUFBckIsRUFBc0M7QUFDekM7QUFDQTtBQUNBTCxNQUFBQSxLQUFLLEdBQUdWLElBQUksQ0FBQ1ksSUFBYjtBQUNILEtBSk0sTUFJQSxJQUFJaEIsRUFBRSxDQUFDZSxNQUFQLEVBQWU7QUFDbEJELE1BQUFBLEtBQUssR0FBR2QsRUFBRSxDQUFDZSxNQUFILENBQVVDLElBQVYsR0FBaUIsSUFBakIsR0FBd0JaLElBQUksQ0FBQ1ksSUFBN0IsR0FBb0MsR0FBNUMsQ0FEa0IsQ0FFbEI7QUFDQTs7QUFDQSxVQUFJaEIsRUFBRSxDQUFDaUIsVUFBSCxHQUFnQkMsSUFBcEIsRUFBMEJMLEdBQUcsR0FBR2IsRUFBRSxDQUFDaUIsVUFBSCxHQUFnQkMsSUFBdEI7QUFDN0I7O0FBRUQsUUFBSSxDQUFDLEtBQUtFLGFBQUwsRUFBTCxFQUEyQjtBQUN2QlAsTUFBQUEsR0FBRyxHQUFHLEVBQU47QUFDSDs7QUFFRCxRQUFJUSxTQUFTLEdBQUcsSUFBaEI7O0FBQ0EsUUFBSXJCLEVBQUUsQ0FBQ2UsTUFBSCxJQUFhLENBQUNPLHVCQUFjQyxRQUFkLENBQXVCLGNBQXZCLENBQWxCLEVBQTBEO0FBQ3RERixNQUFBQSxTQUFTLEdBQUdHLE1BQU0sQ0FBQ0Msa0JBQVAsQ0FBMEJ6QixFQUFFLENBQUNlLE1BQTdCLEVBQXFDLEVBQXJDLEVBQXlDLEVBQXpDLEVBQTZDLE1BQTdDLENBQVo7QUFDSDs7QUFFRCxVQUFNVyxLQUFLLEdBQUdyQixJQUFJLENBQUNzQixtQkFBTCxDQUF5QmIsS0FBekIsRUFBZ0NELEdBQWhDLEVBQXFDUSxTQUFyQyxFQUFnRGpCLElBQWhELENBQWQsQ0F6QzBDLENBMkMxQztBQUNBOztBQUNBLFFBQUlzQixLQUFKLEVBQVc7QUFDUCxVQUFJLEtBQUs3QixZQUFMLENBQWtCRyxFQUFFLENBQUM0QixTQUFILEVBQWxCLE1BQXNDQyxTQUExQyxFQUFxRCxLQUFLaEMsWUFBTCxDQUFrQkcsRUFBRSxDQUFDNEIsU0FBSCxFQUFsQixJQUFvQyxFQUFwQztBQUNyRCxXQUFLL0IsWUFBTCxDQUFrQkcsRUFBRSxDQUFDNEIsU0FBSCxFQUFsQixFQUFrQ0UsSUFBbEMsQ0FBdUNKLEtBQXZDO0FBQ0g7QUFDSixHQTdEWTtBQStEYkssRUFBQUEsZUFBZSxFQUFFLGdCQUFlQyxNQUFmLEVBQXVCO0FBQ3BDO0FBQ0E7QUFDQSxVQUFNQyxPQUFPLEdBQUdYLHVCQUFjQyxRQUFkLENBQXVCLG1CQUF2QixFQUE0Q1MsTUFBNUMsQ0FBaEI7O0FBQ0EsUUFBSSxDQUFDQyxPQUFMLEVBQWM7QUFDVixhQUFPLElBQVA7QUFDSDs7QUFFRCxRQUFJLENBQUNBLE9BQU8sQ0FBQ0MsR0FBYixFQUFrQjtBQUNkQyxNQUFBQSxPQUFPLENBQUNDLElBQVIsV0FBZ0JKLE1BQWhCO0FBQ0EsYUFBTyxJQUFQO0FBQ0g7O0FBRUQsUUFBSSxDQUFDQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUcsVUFBWixDQUF1QixRQUF2QixDQUFMLEVBQXVDO0FBQ25DRixNQUFBQSxPQUFPLENBQUNDLElBQVIsV0FBZ0JKLE1BQWhCO0FBQ0EsYUFBTyxJQUFQO0FBQ0gsS0FoQm1DLENBa0JwQzs7O0FBRUEsV0FBTztBQUNIRSxNQUFBQSxHQUFHLEVBQUVJLGlDQUFnQi9CLEdBQWhCLEdBQXNCZ0MsWUFBdEIsQ0FBbUNOLE9BQU8sQ0FBQ0MsR0FBM0MsQ0FERjtBQUVIbEIsTUFBQUEsSUFBSSxFQUFFaUIsT0FBTyxDQUFDakIsSUFGWDtBQUdId0IsTUFBQUEsSUFBSSxFQUFFUCxPQUFPLENBQUNPLElBSFg7QUFJSEMsTUFBQUEsSUFBSSxFQUFFUixPQUFPLENBQUNRO0FBSlgsS0FBUDtBQU1ILEdBekZZO0FBMkZiQyxFQUFBQSxzQkFBc0IsRUFBRSxnQkFBZTFDLEVBQWYsRUFBbUJJLElBQW5CLEVBQXlCO0FBQzdDLFVBQU11QyxLQUFLLEdBQUcsTUFBTSxLQUFLWixlQUFMLENBQXFCM0IsSUFBSSxDQUFDNEIsTUFBMUIsQ0FBcEI7QUFDQUcsSUFBQUEsT0FBTyxDQUFDUyxHQUFSLHFCQUF5QkQsS0FBSyxJQUFJQSxLQUFLLENBQUMzQixJQUFmLElBQXVCLFNBQWhELGtCQUFpRVosSUFBSSxDQUFDNEIsTUFBdEU7O0FBRUEsUUFBSTtBQUNBLFlBQU1hLFFBQVEsR0FBR2xDLFFBQVEsQ0FBQ21DLGFBQVQsQ0FBdUJILEtBQUssd0JBQWlCQSxLQUFLLENBQUNULEdBQXZCLFVBQWlDLGVBQTdELENBQWpCO0FBQ0EsVUFBSWEsWUFBWSxHQUFHRixRQUFuQjs7QUFDQSxVQUFJLENBQUNBLFFBQUwsRUFBZTtBQUNYLFlBQUksQ0FBQ0YsS0FBTCxFQUFZO0FBQ1JSLFVBQUFBLE9BQU8sQ0FBQ2EsS0FBUixDQUFjLG9EQUFkO0FBQ0E7QUFDSDs7QUFDREQsUUFBQUEsWUFBWSxHQUFHLElBQUlFLEtBQUosQ0FBVU4sS0FBSyxDQUFDVCxHQUFoQixDQUFmOztBQUNBLFlBQUlTLEtBQUssQ0FBQ0gsSUFBVixFQUFnQjtBQUNaTyxVQUFBQSxZQUFZLENBQUNQLElBQWIsR0FBb0JHLEtBQUssQ0FBQ0gsSUFBMUI7QUFDSDs7QUFDRDdCLFFBQUFBLFFBQVEsQ0FBQ08sSUFBVCxDQUFjZ0MsV0FBZCxDQUEwQkgsWUFBMUI7QUFDSDs7QUFDRCxZQUFNQSxZQUFZLENBQUNJLElBQWIsRUFBTjtBQUNILEtBZkQsQ0FlRSxPQUFPQyxFQUFQLEVBQVc7QUFDVGpCLE1BQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLDREQUFiLEVBQTJFZ0IsRUFBM0U7QUFDSDtBQUNKLEdBakhZO0FBbUhiQyxFQUFBQSxLQUFLLEVBQUUsWUFBVztBQUNkO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixLQUFLQSxZQUFMLElBQXFCLEtBQUtDLE9BQUwsQ0FBYUMsSUFBYixDQUFrQixJQUFsQixDQUF6QztBQUNBLFNBQUtDLHNCQUFMLEdBQThCLEtBQUtBLHNCQUFMLElBQStCLEtBQUtDLGlCQUFMLENBQXVCRixJQUF2QixDQUE0QixJQUE1QixDQUE3RDtBQUNBLFNBQUtHLGtCQUFMLEdBQTBCLEtBQUtBLGtCQUFMLElBQTJCLEtBQUtDLGFBQUwsQ0FBbUJKLElBQW5CLENBQXdCLElBQXhCLENBQXJEO0FBQ0EsU0FBS0sscUJBQUwsR0FBNkIsS0FBS0EscUJBQUwsSUFBOEIsS0FBS0MsZ0JBQUwsQ0FBc0JOLElBQXRCLENBQTJCLElBQTNCLENBQTNEOztBQUVBbEIscUNBQWdCL0IsR0FBaEIsR0FBc0J3RCxFQUF0QixDQUF5QixPQUF6QixFQUFrQyxLQUFLVCxZQUF2Qzs7QUFDQWhCLHFDQUFnQi9CLEdBQWhCLEdBQXNCd0QsRUFBdEIsQ0FBeUIsY0FBekIsRUFBeUMsS0FBS0osa0JBQTlDOztBQUNBckIscUNBQWdCL0IsR0FBaEIsR0FBc0J3RCxFQUF0QixDQUF5QixpQkFBekIsRUFBNEMsS0FBS0YscUJBQWpEOztBQUNBdkIscUNBQWdCL0IsR0FBaEIsR0FBc0J3RCxFQUF0QixDQUF5QixNQUF6QixFQUFpQyxLQUFLTixzQkFBdEM7O0FBQ0EsU0FBS08sYUFBTCxHQUFxQixLQUFyQjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsS0FBakI7QUFDSCxHQWhJWTtBQWtJYkMsRUFBQUEsSUFBSSxFQUFFLFlBQVc7QUFDYixRQUFJNUIsaUNBQWdCL0IsR0FBaEIsRUFBSixFQUEyQjtBQUN2QitCLHVDQUFnQi9CLEdBQWhCLEdBQXNCNEQsY0FBdEIsQ0FBcUMsT0FBckMsRUFBOEMsS0FBS2IsWUFBbkQ7O0FBQ0FoQix1Q0FBZ0IvQixHQUFoQixHQUFzQjRELGNBQXRCLENBQXFDLGNBQXJDLEVBQXFELEtBQUtSLGtCQUExRDs7QUFDQXJCLHVDQUFnQi9CLEdBQWhCLEdBQXNCNEQsY0FBdEIsQ0FBcUMsaUJBQXJDLEVBQXdELEtBQUtOLHFCQUE3RDs7QUFDQXZCLHVDQUFnQi9CLEdBQWhCLEdBQXNCNEQsY0FBdEIsQ0FBcUMsTUFBckMsRUFBNkMsS0FBS1Ysc0JBQWxEO0FBQ0g7O0FBQ0QsU0FBS1EsU0FBTCxHQUFpQixLQUFqQjtBQUNILEdBMUlZO0FBNEliRyxFQUFBQSw0QkFBNEIsRUFBRSxZQUFXO0FBQ3JDLFVBQU0vRCxJQUFJLEdBQUdDLHFCQUFZQyxHQUFaLEVBQWI7O0FBQ0EsV0FBT0YsSUFBSSxJQUFJQSxJQUFJLENBQUNHLHFCQUFMLEVBQWY7QUFDSCxHQS9JWTtBQWlKYjZELEVBQUFBLFVBQVUsRUFBRSxVQUFTQyxNQUFULEVBQWlCQyxRQUFqQixFQUEyQjtBQUNuQyxVQUFNbEUsSUFBSSxHQUFHQyxxQkFBWUMsR0FBWixFQUFiOztBQUNBLFFBQUksQ0FBQ0YsSUFBTCxFQUFXLE9BRndCLENBSW5DO0FBQ0E7QUFDQTs7QUFFQW1FLHVCQUFVQyxVQUFWLENBQXFCLFVBQXJCLEVBQWlDLGFBQWpDLEVBQWdESCxNQUFoRCxFQVJtQyxDQVVuQztBQUNBOzs7QUFDQSxRQUFJaEQsdUJBQWNvRCxnQkFBZCxDQUErQkMsNEJBQWFDLE1BQTVDLENBQUosRUFBeUQ7QUFDckR0RCw2QkFBY3VELFFBQWQsQ0FBdUIsMkJBQXZCLEVBQW9ELElBQXBELEVBQTBERiw0QkFBYUMsTUFBdkUsRUFBK0UsS0FBS0UsU0FBTCxFQUEvRTtBQUNIOztBQUVELFFBQUlSLE1BQUosRUFBWTtBQUNSO0FBQ0FqRSxNQUFBQSxJQUFJLENBQUMwRSw2QkFBTCxHQUFxQ0MsSUFBckMsQ0FBMkNDLE1BQUQsSUFBWTtBQUNsRCxZQUFJQSxNQUFNLEtBQUssU0FBZixFQUEwQjtBQUN0QjtBQUNBO0FBQ0EsZ0JBQU1DLFdBQVcsR0FBR0QsTUFBTSxLQUFLLFFBQVgsR0FDZCx5QkFBRywrREFDRCxvQ0FERixDQURjLEdBR2QseUJBQUcsd0VBQUgsQ0FITjtBQUlBLGdCQUFNRSxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHlCQUFNQyxtQkFBTixDQUEwQixnQ0FBMUIsRUFBNEROLE1BQTVELEVBQW9FRSxXQUFwRSxFQUFpRjtBQUM3RXJFLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxnQ0FBSCxDQURzRTtBQUU3RW9FLFlBQUFBO0FBRjZFLFdBQWpGOztBQUlBO0FBQ0g7O0FBRUQsWUFBSVgsUUFBSixFQUFjQSxRQUFROztBQUN0QmlCLDRCQUFJQyxRQUFKLENBQWE7QUFDVEMsVUFBQUEsTUFBTSxFQUFFLGtCQURDO0FBRVRDLFVBQUFBLEtBQUssRUFBRTtBQUZFLFNBQWI7QUFJSCxPQXJCRDtBQXNCSCxLQXhCRCxNQXdCTztBQUNISCwwQkFBSUMsUUFBSixDQUFhO0FBQ1RDLFFBQUFBLE1BQU0sRUFBRSxrQkFEQztBQUVUQyxRQUFBQSxLQUFLLEVBQUU7QUFGRSxPQUFiO0FBSUgsS0E3Q2tDLENBOENuQztBQUNBOzs7QUFDQSxTQUFLQyxnQkFBTCxDQUFzQixJQUF0QjtBQUNILEdBbE1ZO0FBb01iZCxFQUFBQSxTQUFTLEVBQUUsWUFBVztBQUNsQixXQUFPLEtBQUtlLFVBQUwsTUFBcUJ2RSx1QkFBY0MsUUFBZCxDQUF1QixzQkFBdkIsQ0FBNUI7QUFDSCxHQXRNWTtBQXdNYnNFLEVBQUFBLFVBQVUsRUFBRSxZQUFXO0FBQ25CLFVBQU14RixJQUFJLEdBQUdDLHFCQUFZQyxHQUFaLEVBQWI7O0FBQ0EsUUFBSSxDQUFDRixJQUFMLEVBQVcsT0FBTyxLQUFQO0FBQ1gsUUFBSSxDQUFDQSxJQUFJLENBQUNHLHFCQUFMLEVBQUwsRUFBbUMsT0FBTyxLQUFQO0FBQ25DLFFBQUksQ0FBQ0gsSUFBSSxDQUFDSSxvQkFBTCxFQUFMLEVBQWtDLE9BQU8sS0FBUDtBQUVsQyxXQUFPLElBQVAsQ0FObUIsQ0FNTjtBQUNoQixHQS9NWTtBQWlOYlcsRUFBQUEsYUFBYSxFQUFFLFlBQVc7QUFDdEIsV0FBTyxLQUFLMEQsU0FBTCxNQUFvQnhELHVCQUFjQyxRQUFkLENBQXVCLHlCQUF2QixDQUEzQjtBQUNILEdBbk5ZO0FBcU5idUUsRUFBQUEsY0FBYyxFQUFFLFlBQVc7QUFDdkIsV0FBTyxLQUFLaEIsU0FBTCxNQUFvQnhELHVCQUFjQyxRQUFkLENBQXVCLDJCQUF2QixDQUEzQjtBQUNILEdBdk5ZO0FBeU5icUUsRUFBQUEsZ0JBQWdCLEVBQUUsVUFBU0csTUFBVCxFQUFpQkMsVUFBVSxHQUFHLElBQTlCLEVBQW9DO0FBQ2xELFNBQUtoQyxhQUFMLEdBQXFCK0IsTUFBckI7O0FBRUF2Qix1QkFBVUMsVUFBVixDQUFxQixVQUFyQixFQUFpQyxvQkFBakMsRUFBdURzQixNQUF2RCxFQUhrRCxDQUtsRDtBQUNBOzs7QUFDQVAsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsa0JBREM7QUFFVEMsTUFBQUEsS0FBSyxFQUFFLEtBQUtiLFNBQUw7QUFGRSxLQUFiLEVBUGtELENBWWxEOzs7QUFDQSxRQUFJa0IsVUFBVSxJQUFJdEYsTUFBTSxDQUFDdUYsWUFBekIsRUFBdUM7QUFDbkN2RixNQUFBQSxNQUFNLENBQUN1RixZQUFQLENBQW9CQyxPQUFwQixDQUE0QixzQkFBNUIsRUFBb0RILE1BQXBEO0FBQ0g7QUFDSixHQXpPWTtBQTJPYkksRUFBQUEsaUJBQWlCLEVBQUUsWUFBVztBQUMxQixVQUFNQyxNQUFNLEdBQUc5RCxpQ0FBZ0IvQixHQUFoQixFQUFmOztBQUNBLFFBQUksQ0FBQzZGLE1BQUwsRUFBYTtBQUNULGFBQU8sS0FBUDtBQUNIOztBQUNELFVBQU1DLE9BQU8sR0FBR0QsTUFBTSxDQUFDQyxPQUFQLEVBQWhCO0FBQ0EsV0FBTyxDQUFDQSxPQUFELElBQVksS0FBS2pDLDRCQUFMLEVBQVosSUFDSCxDQUFDLEtBQUtVLFNBQUwsRUFERSxJQUNrQixDQUFDLEtBQUt3QixnQkFBTCxFQUQxQjtBQUVILEdBblBZO0FBcVBiQSxFQUFBQSxnQkFBZ0IsRUFBRSxZQUFXO0FBQ3pCO0FBQ0EsUUFBSTVGLE1BQU0sQ0FBQ3VGLFlBQVgsRUFBeUI7QUFDckIsYUFBT3ZGLE1BQU0sQ0FBQ3VGLFlBQVAsQ0FBb0JNLE9BQXBCLENBQTRCLHNCQUE1QixNQUF3RCxNQUEvRDtBQUNIOztBQUVELFdBQU8sS0FBS3ZDLGFBQVo7QUFDSCxHQTVQWTtBQThQYk4sRUFBQUEsaUJBQWlCLEVBQUUsVUFBUzhDLEtBQVQsRUFBZ0I7QUFDL0IsUUFBSUEsS0FBSyxLQUFLLFNBQWQsRUFBeUI7QUFDckIsV0FBS3ZDLFNBQUwsR0FBaUIsSUFBakI7QUFDSCxLQUZELE1BRU8sSUFBSXVDLEtBQUssS0FBSyxTQUFWLElBQXVCQSxLQUFLLEtBQUssT0FBckMsRUFBOEM7QUFDakQsV0FBS3ZDLFNBQUwsR0FBaUIsS0FBakI7QUFDSDtBQUNKLEdBcFFZO0FBc1FiVixFQUFBQSxPQUFPLEVBQUUsVUFBU3ZELEVBQVQsRUFBYTtBQUNsQixRQUFJLENBQUMsS0FBS2lFLFNBQVYsRUFBcUIsT0FESCxDQUNXOztBQUM3QixRQUFJakUsRUFBRSxDQUFDZSxNQUFILElBQWFmLEVBQUUsQ0FBQ2UsTUFBSCxDQUFVMEYsTUFBVixLQUFxQm5FLGlDQUFnQi9CLEdBQWhCLEdBQXNCbUcsV0FBdEIsQ0FBa0NELE1BQXhFLEVBQWdGLE9BRjlELENBSWxCO0FBQ0E7O0FBQ0EsUUFBSXpHLEVBQUUsQ0FBQzJHLGdCQUFILE1BQXlCM0csRUFBRSxDQUFDNEcsbUJBQUgsRUFBN0IsRUFBdUQ7QUFDbkQsV0FBSzlHLHdCQUFMLENBQThCZ0MsSUFBOUIsQ0FBbUM5QixFQUFFLENBQUM2RyxLQUFILEVBQW5DLEVBRG1ELENBRW5EOztBQUNBLGFBQU8sS0FBSy9HLHdCQUFMLENBQThCZ0gsTUFBOUIsR0FBdUNuSCxxQkFBOUMsRUFBcUU7QUFDakUsYUFBS0csd0JBQUwsQ0FBOEJpSCxLQUE5QjtBQUNIOztBQUNEO0FBQ0g7O0FBRUQsU0FBS0MsY0FBTCxDQUFvQmhILEVBQXBCO0FBQ0gsR0F0Ulk7QUF3UmI4RCxFQUFBQSxnQkFBZ0IsRUFBRSxVQUFTOUQsRUFBVCxFQUFhO0FBQzNCO0FBQ0E7QUFDQSxRQUFJQSxFQUFFLENBQUM0RyxtQkFBSCxFQUFKLEVBQThCO0FBRTlCLFVBQU1LLEdBQUcsR0FBRyxLQUFLbkgsd0JBQUwsQ0FBOEJvSCxPQUE5QixDQUFzQ2xILEVBQUUsQ0FBQzZHLEtBQUgsRUFBdEMsQ0FBWjtBQUNBLFFBQUlJLEdBQUcsS0FBSyxDQUFDLENBQWIsRUFBZ0I7QUFFaEIsU0FBS25ILHdCQUFMLENBQThCcUgsTUFBOUIsQ0FBcUNGLEdBQXJDLEVBQTBDLENBQTFDOztBQUNBLFNBQUtELGNBQUwsQ0FBb0JoSCxFQUFwQjtBQUNILEdBbFNZO0FBb1NiNEQsRUFBQUEsYUFBYSxFQUFFLFVBQVM1RCxFQUFULEVBQWFJLElBQWIsRUFBbUI7QUFDOUIsUUFBSUEsSUFBSSxDQUFDZ0gsMEJBQUwsT0FBc0MsQ0FBMUMsRUFBNkM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBTS9HLElBQUksR0FBR0MscUJBQVlDLEdBQVosRUFBYjs7QUFDQSxVQUFJLENBQUNGLElBQUwsRUFBVztBQUNYLFVBQUksS0FBS1IsWUFBTCxDQUFrQk8sSUFBSSxDQUFDNEIsTUFBdkIsTUFBbUNILFNBQXZDLEVBQWtEOztBQUNsRCxXQUFLLE1BQU1ILEtBQVgsSUFBb0IsS0FBSzdCLFlBQUwsQ0FBa0JPLElBQUksQ0FBQzRCLE1BQXZCLENBQXBCLEVBQW9EO0FBQ2hEM0IsUUFBQUEsSUFBSSxDQUFDZ0gsaUJBQUwsQ0FBdUIzRixLQUF2QjtBQUNIOztBQUNELGFBQU8sS0FBSzdCLFlBQUwsQ0FBa0JPLElBQUksQ0FBQzRCLE1BQXZCLENBQVA7QUFDSDtBQUNKLEdBcFRZO0FBc1RiZ0YsRUFBQUEsY0FBYyxFQUFFLFVBQVNoSCxFQUFULEVBQWE7QUFDekIsVUFBTUksSUFBSSxHQUFHa0MsaUNBQWdCL0IsR0FBaEIsR0FBc0IrRyxPQUF0QixDQUE4QnRILEVBQUUsQ0FBQzRCLFNBQUgsRUFBOUIsQ0FBYjs7QUFDQSxVQUFNMkYsT0FBTyxHQUFHakYsaUNBQWdCL0IsR0FBaEIsR0FBc0JpSCxzQkFBdEIsQ0FBNkN4SCxFQUE3QyxDQUFoQjs7QUFDQSxRQUFJdUgsT0FBTyxJQUFJQSxPQUFPLENBQUNFLE1BQXZCLEVBQStCO0FBQzNCLFVBQUksS0FBSzNDLFNBQUwsRUFBSixFQUFzQjtBQUNsQixhQUFLM0UseUJBQUwsQ0FBK0JILEVBQS9CLEVBQW1DSSxJQUFuQztBQUNIOztBQUNELFVBQUltSCxPQUFPLENBQUNHLE1BQVIsQ0FBZS9FLEtBQWYsSUFBd0IsS0FBS21ELGNBQUwsRUFBNUIsRUFBbUQ7QUFDL0N4Riw2QkFBWUMsR0FBWixHQUFrQm9ILGdCQUFsQixDQUFtQzNILEVBQW5DLEVBQXVDSSxJQUF2Qzs7QUFDQSxhQUFLc0Msc0JBQUwsQ0FBNEIxQyxFQUE1QixFQUFnQ0ksSUFBaEM7QUFDSDtBQUNKO0FBQ0o7QUFsVVksQ0FBakI7O0FBcVVBLElBQUksQ0FBQ00sTUFBTSxDQUFDa0gsVUFBWixFQUF3QjtBQUNwQmxILEVBQUFBLE1BQU0sQ0FBQ2tILFVBQVAsR0FBb0JoSSxRQUFwQjtBQUNIOztlQUVjYyxNQUFNLENBQUNrSCxVIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTcgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSAnLi9QbGF0Zm9ybVBlZyc7XHJcbmltcG9ydCAqIGFzIFRleHRGb3JFdmVudCBmcm9tICcuL1RleHRGb3JFdmVudCc7XHJcbmltcG9ydCBBbmFseXRpY3MgZnJvbSAnLi9BbmFseXRpY3MnO1xyXG5pbXBvcnQgKiBhcyBBdmF0YXIgZnJvbSAnLi9BdmF0YXInO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuL01vZGFsJztcclxuaW1wb3J0IFNldHRpbmdzU3RvcmUsIHtTZXR0aW5nTGV2ZWx9IGZyb20gXCIuL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuXHJcbi8qXHJcbiAqIERpc3BhdGNoZXM6XHJcbiAqIHtcclxuICogICBhY3Rpb246IFwibm90aWZpZXJfZW5hYmxlZFwiLFxyXG4gKiAgIHZhbHVlOiBib29sZWFuXHJcbiAqIH1cclxuICovXHJcblxyXG5jb25zdCBNQVhfUEVORElOR19FTkNSWVBURUQgPSAyMDtcclxuXHJcbmNvbnN0IE5vdGlmaWVyID0ge1xyXG4gICAgbm90aWZzQnlSb29tOiB7fSxcclxuXHJcbiAgICAvLyBBIGxpc3Qgb2YgZXZlbnQgSURzIHRoYXQgd2UndmUgcmVjZWl2ZWQgYnV0IG5lZWQgdG8gd2FpdCB1bnRpbFxyXG4gICAgLy8gdGhleSdyZSBkZWNyeXB0ZWQgdW50aWwgd2UgZGVjaWRlIHdoZXRoZXIgdG8gbm90aWZ5IGZvciB0aGVtXHJcbiAgICAvLyBvciBub3RcclxuICAgIHBlbmRpbmdFbmNyeXB0ZWRFdmVudElkczogW10sXHJcblxyXG4gICAgbm90aWZpY2F0aW9uTWVzc2FnZUZvckV2ZW50OiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIHJldHVybiBUZXh0Rm9yRXZlbnQudGV4dEZvckV2ZW50KGV2KTtcclxuICAgIH0sXHJcblxyXG4gICAgX2Rpc3BsYXlQb3B1cE5vdGlmaWNhdGlvbjogZnVuY3Rpb24oZXYsIHJvb20pIHtcclxuICAgICAgICBjb25zdCBwbGFmID0gUGxhdGZvcm1QZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFwbGFmKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFwbGFmLnN1cHBvcnRzTm90aWZpY2F0aW9ucygpIHx8ICFwbGFmLm1heVNlbmROb3RpZmljYXRpb25zKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZ2xvYmFsLmRvY3VtZW50Lmhhc0ZvY3VzKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG1zZyA9IHRoaXMubm90aWZpY2F0aW9uTWVzc2FnZUZvckV2ZW50KGV2KTtcclxuICAgICAgICBpZiAoIW1zZykgcmV0dXJuO1xyXG5cclxuICAgICAgICBsZXQgdGl0bGU7XHJcbiAgICAgICAgaWYgKCFldi5zZW5kZXIgfHwgcm9vbS5uYW1lID09PSBldi5zZW5kZXIubmFtZSkge1xyXG4gICAgICAgICAgICB0aXRsZSA9IHJvb20ubmFtZTtcclxuICAgICAgICAgICAgLy8gbm90aWZpY2F0aW9uTWVzc2FnZUZvckV2ZW50IGluY2x1ZGVzIHNlbmRlcixcclxuICAgICAgICAgICAgLy8gYnV0IHdlIGFscmVhZHkgaGF2ZSB0aGUgc2VuZGVyIGhlcmVcclxuICAgICAgICAgICAgaWYgKGV2LmdldENvbnRlbnQoKS5ib2R5KSBtc2cgPSBldi5nZXRDb250ZW50KCkuYm9keTtcclxuICAgICAgICB9IGVsc2UgaWYgKGV2LmdldFR5cGUoKSA9PT0gJ20ucm9vbS5tZW1iZXInKSB7XHJcbiAgICAgICAgICAgIC8vIGNvbnRleHQgaXMgYWxsIGluIHRoZSBtZXNzYWdlIGhlcmUsIHdlIGRvbid0IG5lZWRcclxuICAgICAgICAgICAgLy8gdG8gZGlzcGxheSBzZW5kZXIgaW5mb1xyXG4gICAgICAgICAgICB0aXRsZSA9IHJvb20ubmFtZTtcclxuICAgICAgICB9IGVsc2UgaWYgKGV2LnNlbmRlcikge1xyXG4gICAgICAgICAgICB0aXRsZSA9IGV2LnNlbmRlci5uYW1lICsgXCIgKFwiICsgcm9vbS5uYW1lICsgXCIpXCI7XHJcbiAgICAgICAgICAgIC8vIG5vdGlmaWNhdGlvbk1lc3NhZ2VGb3JFdmVudCBpbmNsdWRlcyBzZW5kZXIsXHJcbiAgICAgICAgICAgIC8vIGJ1dCB3ZSd2ZSBqdXN0IG91dCBzZW5kZXIgaW4gdGhlIHRpdGxlXHJcbiAgICAgICAgICAgIGlmIChldi5nZXRDb250ZW50KCkuYm9keSkgbXNnID0gZXYuZ2V0Q29udGVudCgpLmJvZHk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuaXNCb2R5RW5hYmxlZCgpKSB7XHJcbiAgICAgICAgICAgIG1zZyA9ICcnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGF2YXRhclVybCA9IG51bGw7XHJcbiAgICAgICAgaWYgKGV2LnNlbmRlciAmJiAhU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImxvd0JhbmR3aWR0aFwiKSkge1xyXG4gICAgICAgICAgICBhdmF0YXJVcmwgPSBBdmF0YXIuYXZhdGFyVXJsRm9yTWVtYmVyKGV2LnNlbmRlciwgNDAsIDQwLCAnY3JvcCcpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgbm90aWYgPSBwbGFmLmRpc3BsYXlOb3RpZmljYXRpb24odGl0bGUsIG1zZywgYXZhdGFyVXJsLCByb29tKTtcclxuXHJcbiAgICAgICAgLy8gaWYgZGlzcGxheU5vdGlmaWNhdGlvbiByZXR1cm5zIG5vbi1udWxsLCAgdGhlIHBsYXRmb3JtIHN1cHBvcnRzXHJcbiAgICAgICAgLy8gY2xlYXJpbmcgbm90aWZpY2F0aW9ucyBsYXRlciwgc28ga2VlcCB0cmFjayBvZiB0aGlzLlxyXG4gICAgICAgIGlmIChub3RpZikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZnNCeVJvb21bZXYuZ2V0Um9vbUlkKCldID09PSB1bmRlZmluZWQpIHRoaXMubm90aWZzQnlSb29tW2V2LmdldFJvb21JZCgpXSA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmc0J5Um9vbVtldi5nZXRSb29tSWQoKV0ucHVzaChub3RpZik7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBnZXRTb3VuZEZvclJvb206IGFzeW5jIGZ1bmN0aW9uKHJvb21JZCkge1xyXG4gICAgICAgIC8vIFdlIGRvIG5vIGNhY2hpbmcgaGVyZSBiZWNhdXNlIHRoZSBTREsgY2FjaGVzIHNldHRpbmdcclxuICAgICAgICAvLyBhbmQgdGhlIGJyb3dzZXIgd2lsbCBjYWNoZSB0aGUgc291bmQuXHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJub3RpZmljYXRpb25Tb3VuZFwiLCByb29tSWQpO1xyXG4gICAgICAgIGlmICghY29udGVudCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghY29udGVudC51cmwpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGAke3Jvb21JZH0gaGFzIGN1c3RvbSBub3RpZmljYXRpb24gc291bmQgZXZlbnQsIGJ1dCBubyB1cmwga2V5YCk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFjb250ZW50LnVybC5zdGFydHNXaXRoKFwibXhjOi8vXCIpKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgJHtyb29tSWR9IGhhcyBjdXN0b20gbm90aWZpY2F0aW9uIHNvdW5kIGV2ZW50LCBidXQgdXJsIGlzIG5vdCBhIG14YyB1cmxgKTtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBJZGVhbGx5IGluIGhlcmUgd2UgY291bGQgdXNlIE1TQzEzMTAgdG8gZGV0ZWN0IHRoZSB0eXBlIG9mIGZpbGUsIGFuZCByZWplY3QgaXQuXHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHVybDogTWF0cml4Q2xpZW50UGVnLmdldCgpLm14Y1VybFRvSHR0cChjb250ZW50LnVybCksXHJcbiAgICAgICAgICAgIG5hbWU6IGNvbnRlbnQubmFtZSxcclxuICAgICAgICAgICAgdHlwZTogY29udGVudC50eXBlLFxyXG4gICAgICAgICAgICBzaXplOiBjb250ZW50LnNpemUsXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgX3BsYXlBdWRpb05vdGlmaWNhdGlvbjogYXN5bmMgZnVuY3Rpb24oZXYsIHJvb20pIHtcclxuICAgICAgICBjb25zdCBzb3VuZCA9IGF3YWl0IHRoaXMuZ2V0U291bmRGb3JSb29tKHJvb20ucm9vbUlkKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhgR290IHNvdW5kICR7c291bmQgJiYgc291bmQubmFtZSB8fCBcImRlZmF1bHRcIn0gZm9yICR7cm9vbS5yb29tSWR9YCk7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihzb3VuZCA/IGBhdWRpb1tzcmM9JyR7c291bmQudXJsfSddYCA6IFwiI21lc3NhZ2VBdWRpb1wiKTtcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRWxlbWVudCA9IHNlbGVjdG9yO1xyXG4gICAgICAgICAgICBpZiAoIXNlbGVjdG9yKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXNvdW5kKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIk5vIGF1ZGlvIGVsZW1lbnQgb3Igc291bmQgdG8gcGxheSBmb3Igbm90aWZpY2F0aW9uXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGF1ZGlvRWxlbWVudCA9IG5ldyBBdWRpbyhzb3VuZC51cmwpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNvdW5kLnR5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBhdWRpb0VsZW1lbnQudHlwZSA9IHNvdW5kLnR5cGU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGF1ZGlvRWxlbWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYXdhaXQgYXVkaW9FbGVtZW50LnBsYXkoKTtcclxuICAgICAgICB9IGNhdGNoIChleCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJDYXVnaHQgZXJyb3Igd2hlbiB0cnlpbmcgdG8gZmV0Y2ggcm9vbSBub3RpZmljYXRpb24gc291bmQ6XCIsIGV4KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBkbyBub3QgcmUtYmluZCBpbiB0aGUgY2FzZSBvZiByZXBlYXRlZCBjYWxsXHJcbiAgICAgICAgdGhpcy5ib3VuZE9uRXZlbnQgPSB0aGlzLmJvdW5kT25FdmVudCB8fCB0aGlzLm9uRXZlbnQuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLmJvdW5kT25TeW5jU3RhdGVDaGFuZ2UgPSB0aGlzLmJvdW5kT25TeW5jU3RhdGVDaGFuZ2UgfHwgdGhpcy5vblN5bmNTdGF0ZUNoYW5nZS5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMuYm91bmRPblJvb21SZWNlaXB0ID0gdGhpcy5ib3VuZE9uUm9vbVJlY2VpcHQgfHwgdGhpcy5vblJvb21SZWNlaXB0LmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5ib3VuZE9uRXZlbnREZWNyeXB0ZWQgPSB0aGlzLmJvdW5kT25FdmVudERlY3J5cHRlZCB8fCB0aGlzLm9uRXZlbnREZWNyeXB0ZWQuYmluZCh0aGlzKTtcclxuXHJcbiAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLm9uKCdldmVudCcsIHRoaXMuYm91bmRPbkV2ZW50KTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oJ1Jvb20ucmVjZWlwdCcsIHRoaXMuYm91bmRPblJvb21SZWNlaXB0KTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oJ0V2ZW50LmRlY3J5cHRlZCcsIHRoaXMuYm91bmRPbkV2ZW50RGVjcnlwdGVkKTtcclxuICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkub24oXCJzeW5jXCIsIHRoaXMuYm91bmRPblN5bmNTdGF0ZUNoYW5nZSk7XHJcbiAgICAgICAgdGhpcy50b29sYmFySGlkZGVuID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5pc1N5bmNpbmcgPSBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgc3RvcDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKSkge1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoJ0V2ZW50JywgdGhpcy5ib3VuZE9uRXZlbnQpO1xyXG4gICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkucmVtb3ZlTGlzdGVuZXIoJ1Jvb20ucmVjZWlwdCcsIHRoaXMuYm91bmRPblJvb21SZWNlaXB0KTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLnJlbW92ZUxpc3RlbmVyKCdFdmVudC5kZWNyeXB0ZWQnLCB0aGlzLmJvdW5kT25FdmVudERlY3J5cHRlZCk7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZW1vdmVMaXN0ZW5lcignc3luYycsIHRoaXMuYm91bmRPblN5bmNTdGF0ZUNoYW5nZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNTeW5jaW5nID0gZmFsc2U7XHJcbiAgICB9LFxyXG5cclxuICAgIHN1cHBvcnRzRGVza3RvcE5vdGlmaWNhdGlvbnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHBsYWYgPSBQbGF0Zm9ybVBlZy5nZXQoKTtcclxuICAgICAgICByZXR1cm4gcGxhZiAmJiBwbGFmLnN1cHBvcnRzTm90aWZpY2F0aW9ucygpO1xyXG4gICAgfSxcclxuXHJcbiAgICBzZXRFbmFibGVkOiBmdW5jdGlvbihlbmFibGUsIGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY29uc3QgcGxhZiA9IFBsYXRmb3JtUGVnLmdldCgpO1xyXG4gICAgICAgIGlmICghcGxhZikgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBEZXYgbm90ZTogV2UgZG9uJ3Qgc2V0IHRoZSBcIm5vdGlmaWNhdGlvbnNFbmFibGVkXCIgc2V0dGluZyB0byB0cnVlIGhlcmUgYmVjYXVzZSBpdCBpcyBhXHJcbiAgICAgICAgLy8gY2FsY3VsYXRlZCB2YWx1ZS4gSXQgaXMgZGV0ZXJtaW5lZCBiYXNlZCB1cG9uIHdoZXRoZXIgb3Igbm90IHRoZSBtYXN0ZXIgcnVsZSBpcyBlbmFibGVkXHJcbiAgICAgICAgLy8gYW5kIG90aGVyIGZsYWdzLiBTZXR0aW5nIGl0IGhlcmUgd291bGQgY2F1c2UgYSBjaXJjdWxhciByZWZlcmVuY2UuXHJcblxyXG4gICAgICAgIEFuYWx5dGljcy50cmFja0V2ZW50KCdOb3RpZmllcicsICdTZXQgRW5hYmxlZCcsIGVuYWJsZSk7XHJcblxyXG4gICAgICAgIC8vIG1ha2Ugc3VyZSB0aGF0IHdlIHBlcnNpc3QgdGhlIGN1cnJlbnQgc2V0dGluZyBhdWRpb19lbmFibGVkIHNldHRpbmdcclxuICAgICAgICAvLyBiZWZvcmUgY2hhbmdpbmcgYW55dGhpbmdcclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0xldmVsU3VwcG9ydGVkKFNldHRpbmdMZXZlbC5ERVZJQ0UpKSB7XHJcbiAgICAgICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXCJhdWRpb05vdGlmaWNhdGlvbnNFbmFibGVkXCIsIG51bGwsIFNldHRpbmdMZXZlbC5ERVZJQ0UsIHRoaXMuaXNFbmFibGVkKCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGVuYWJsZSkge1xyXG4gICAgICAgICAgICAvLyBBdHRlbXB0IHRvIGdldCBwZXJtaXNzaW9uIGZyb20gdXNlclxyXG4gICAgICAgICAgICBwbGFmLnJlcXVlc3ROb3RpZmljYXRpb25QZXJtaXNzaW9uKCkudGhlbigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0ICE9PSAnZ3JhbnRlZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBUaGUgcGVybWlzc2lvbiByZXF1ZXN0IHdhcyBkaXNtaXNzZWQgb3IgZGVuaWVkXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVE9ETzogU3VwcG9ydCBhbHRlcm5hdGl2ZSBicmFuZGluZyBpbiBtZXNzYWdpbmdcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZXNjcmlwdGlvbiA9IHJlc3VsdCA9PT0gJ2RlbmllZCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgPyBfdCgnUmlvdCBkb2VzIG5vdCBoYXZlIHBlcm1pc3Npb24gdG8gc2VuZCB5b3Ugbm90aWZpY2F0aW9ucyAtICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3BsZWFzZSBjaGVjayB5b3VyIGJyb3dzZXIgc2V0dGluZ3MnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA6IF90KCdSaW90IHdhcyBub3QgZ2l2ZW4gcGVybWlzc2lvbiB0byBzZW5kIG5vdGlmaWNhdGlvbnMgLSBwbGVhc2UgdHJ5IGFnYWluJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLkVycm9yRGlhbG9nJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVW5hYmxlIHRvIGVuYWJsZSBOb3RpZmljYXRpb25zJywgcmVzdWx0LCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoJ1VuYWJsZSB0byBlbmFibGUgTm90aWZpY2F0aW9ucycpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbixcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSBjYWxsYmFjaygpO1xyXG4gICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246IFwibm90aWZpZXJfZW5hYmxlZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICBhY3Rpb246IFwibm90aWZpZXJfZW5hYmxlZFwiLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IGZhbHNlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gc2V0IHRoZSBub3RpZmljYXRpb25zX2hpZGRlbiBmbGFnLCBhcyB0aGUgdXNlciBoYXMga25vd2luZ2x5IGludGVyYWN0ZWRcclxuICAgICAgICAvLyB3aXRoIHRoZSBzZXR0aW5nIHdlIHNob3VsZG4ndCBuYWcgdGhlbSBhbnkgZnVydGhlclxyXG4gICAgICAgIHRoaXMuc2V0VG9vbGJhckhpZGRlbih0cnVlKTtcclxuICAgIH0sXHJcblxyXG4gICAgaXNFbmFibGVkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc1Bvc3NpYmxlKCkgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcIm5vdGlmaWNhdGlvbnNFbmFibGVkXCIpO1xyXG4gICAgfSxcclxuXHJcbiAgICBpc1Bvc3NpYmxlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBwbGFmID0gUGxhdGZvcm1QZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFwbGFmKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgaWYgKCFwbGFmLnN1cHBvcnRzTm90aWZpY2F0aW9ucygpKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgaWYgKCFwbGFmLm1heVNlbmROb3RpZmljYXRpb25zKCkpIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7IC8vIHBvc3NpYmxlLCBidXQgbm90IG5lY2Vzc2FyaWx5IGVuYWJsZWRcclxuICAgIH0sXHJcblxyXG4gICAgaXNCb2R5RW5hYmxlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNFbmFibGVkKCkgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcIm5vdGlmaWNhdGlvbkJvZHlFbmFibGVkXCIpO1xyXG4gICAgfSxcclxuXHJcbiAgICBpc0F1ZGlvRW5hYmxlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNFbmFibGVkKCkgJiYgU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZShcImF1ZGlvTm90aWZpY2F0aW9uc0VuYWJsZWRcIik7XHJcbiAgICB9LFxyXG5cclxuICAgIHNldFRvb2xiYXJIaWRkZW46IGZ1bmN0aW9uKGhpZGRlbiwgcGVyc2lzdGVudCA9IHRydWUpIHtcclxuICAgICAgICB0aGlzLnRvb2xiYXJIaWRkZW4gPSBoaWRkZW47XHJcblxyXG4gICAgICAgIEFuYWx5dGljcy50cmFja0V2ZW50KCdOb3RpZmllcicsICdTZXQgVG9vbGJhciBIaWRkZW4nLCBoaWRkZW4pO1xyXG5cclxuICAgICAgICAvLyBYWFg6IHdoeSBhcmUgd2UgZGlzcGF0Y2hpbmcgdGhpcyBoZXJlP1xyXG4gICAgICAgIC8vIHRoaXMgaXMgbm90aGluZyB0byBkbyB3aXRoIG5vdGlmaWVyX2VuYWJsZWRcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICBhY3Rpb246IFwibm90aWZpZXJfZW5hYmxlZFwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogdGhpcy5pc0VuYWJsZWQoKSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gdXBkYXRlIHRoZSBpbmZvIHRvIGxvY2FsU3RvcmFnZSBmb3IgcGVyc2lzdGVudCBzZXR0aW5nc1xyXG4gICAgICAgIGlmIChwZXJzaXN0ZW50ICYmIGdsb2JhbC5sb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgZ2xvYmFsLmxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibm90aWZpY2F0aW9uc19oaWRkZW5cIiwgaGlkZGVuKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHNob3VsZFNob3dUb29sYmFyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgaWYgKCFjbGllbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpc0d1ZXN0ID0gY2xpZW50LmlzR3Vlc3QoKTtcclxuICAgICAgICByZXR1cm4gIWlzR3Vlc3QgJiYgdGhpcy5zdXBwb3J0c0Rlc2t0b3BOb3RpZmljYXRpb25zKCkgJiZcclxuICAgICAgICAgICAgIXRoaXMuaXNFbmFibGVkKCkgJiYgIXRoaXMuX2lzVG9vbGJhckhpZGRlbigpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfaXNUb29sYmFySGlkZGVuOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBDaGVjayBsb2NhbFN0b3JhZ2UgZm9yIGFueSBzdWNoIG1ldGEgZGF0YVxyXG4gICAgICAgIGlmIChnbG9iYWwubG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBnbG9iYWwubG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJub3RpZmljYXRpb25zX2hpZGRlblwiKSA9PT0gXCJ0cnVlXCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy50b29sYmFySGlkZGVuO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblN5bmNTdGF0ZUNoYW5nZTogZnVuY3Rpb24oc3RhdGUpIHtcclxuICAgICAgICBpZiAoc3RhdGUgPT09IFwiU1lOQ0lOR1wiKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHN0YXRlID09PSBcIlNUT1BQRURcIiB8fCBzdGF0ZSA9PT0gXCJFUlJPUlwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBvbkV2ZW50OiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIGlmICghdGhpcy5pc1N5bmNpbmcpIHJldHVybjsgLy8gZG9uJ3QgYWxlcnQgZm9yIGFueSBtZXNzYWdlcyBpbml0aWFsbHlcclxuICAgICAgICBpZiAoZXYuc2VuZGVyICYmIGV2LnNlbmRlci51c2VySWQgPT09IE1hdHJpeENsaWVudFBlZy5nZXQoKS5jcmVkZW50aWFscy51c2VySWQpIHJldHVybjtcclxuXHJcbiAgICAgICAgLy8gSWYgaXQncyBhbiBlbmNyeXB0ZWQgZXZlbnQgYW5kIHRoZSB0eXBlIGlzIHN0aWxsICdtLnJvb20uZW5jcnlwdGVkJyxcclxuICAgICAgICAvLyBpdCBoYXNuJ3QgeWV0IGJlZW4gZGVjcnlwdGVkLCBzbyB3YWl0IHVudGlsIGl0IGlzLlxyXG4gICAgICAgIGlmIChldi5pc0JlaW5nRGVjcnlwdGVkKCkgfHwgZXYuaXNEZWNyeXB0aW9uRmFpbHVyZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGVuZGluZ0VuY3J5cHRlZEV2ZW50SWRzLnB1c2goZXYuZ2V0SWQoKSk7XHJcbiAgICAgICAgICAgIC8vIGRvbid0IGxldCB0aGUgbGlzdCBmaWxsIHVwIGluZGVmaW5pdGVseVxyXG4gICAgICAgICAgICB3aGlsZSAodGhpcy5wZW5kaW5nRW5jcnlwdGVkRXZlbnRJZHMubGVuZ3RoID4gTUFYX1BFTkRJTkdfRU5DUllQVEVEKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBlbmRpbmdFbmNyeXB0ZWRFdmVudElkcy5zaGlmdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX2V2YWx1YXRlRXZlbnQoZXYpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvbkV2ZW50RGVjcnlwdGVkOiBmdW5jdGlvbihldikge1xyXG4gICAgICAgIC8vICdkZWNyeXB0ZWQnIG1lYW5zIHRoZSBkZWNyeXB0aW9uIHByb2Nlc3MgaGFzIGZpbmlzaGVkOiBpdCBtYXkgaGF2ZSBmYWlsZWQsXHJcbiAgICAgICAgLy8gaW4gd2hpY2ggY2FzZSBpdCBtaWdodCBkZWNyeXB0IHNvb24gaWYgdGhlIGtleXMgYXJyaXZlXHJcbiAgICAgICAgaWYgKGV2LmlzRGVjcnlwdGlvbkZhaWx1cmUoKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBpZHggPSB0aGlzLnBlbmRpbmdFbmNyeXB0ZWRFdmVudElkcy5pbmRleE9mKGV2LmdldElkKCkpO1xyXG4gICAgICAgIGlmIChpZHggPT09IC0xKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMucGVuZGluZ0VuY3J5cHRlZEV2ZW50SWRzLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgIHRoaXMuX2V2YWx1YXRlRXZlbnQoZXYpO1xyXG4gICAgfSxcclxuXHJcbiAgICBvblJvb21SZWNlaXB0OiBmdW5jdGlvbihldiwgcm9vbSkge1xyXG4gICAgICAgIGlmIChyb29tLmdldFVucmVhZE5vdGlmaWNhdGlvbkNvdW50KCkgPT09IDApIHtcclxuICAgICAgICAgICAgLy8gaWRlYWxseSB3ZSB3b3VsZCBjbGVhciBlYWNoIG5vdGlmaWNhdGlvbiB3aGVuIGl0IHdhcyByZWFkLFxyXG4gICAgICAgICAgICAvLyBidXQgd2UgaGF2ZSBubyB3YXksIGdpdmVuIGEgcmVhZCByZWNlaXB0LCB0byBrbm93IHdoZXRoZXJcclxuICAgICAgICAgICAgLy8gdGhlIHJlY2VpcHQgY29tZXMgYmVmb3JlIG9yIGFmdGVyIGFuIGV2ZW50LCBzbyB3ZSBjYW4ndFxyXG4gICAgICAgICAgICAvLyBkbyB0aGlzLiBJbnN0ZWFkLCBjbGVhciBhbGwgbm90aWZpY2F0aW9ucyBmb3IgYSByb29tIG9uY2VcclxuICAgICAgICAgICAgLy8gdGhlcmUgYXJlIG5vIG5vdGlmcyBsZWZ0IGluIHRoYXQgcm9vbS4sIHdoaWNoIGlzIG5vdCBxdWl0ZVxyXG4gICAgICAgICAgICAvLyBhcyBnb29kIGJ1dCBpdCdzIHNvbWV0aGluZy5cclxuICAgICAgICAgICAgY29uc3QgcGxhZiA9IFBsYXRmb3JtUGVnLmdldCgpO1xyXG4gICAgICAgICAgICBpZiAoIXBsYWYpIHJldHVybjtcclxuICAgICAgICAgICAgaWYgKHRoaXMubm90aWZzQnlSb29tW3Jvb20ucm9vbUlkXSA9PT0gdW5kZWZpbmVkKSByZXR1cm47XHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qgbm90aWYgb2YgdGhpcy5ub3RpZnNCeVJvb21bcm9vbS5yb29tSWRdKSB7XHJcbiAgICAgICAgICAgICAgICBwbGFmLmNsZWFyTm90aWZpY2F0aW9uKG5vdGlmKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBkZWxldGUgdGhpcy5ub3RpZnNCeVJvb21bcm9vbS5yb29tSWRdO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgX2V2YWx1YXRlRXZlbnQ6IGZ1bmN0aW9uKGV2KSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXRSb29tKGV2LmdldFJvb21JZCgpKTtcclxuICAgICAgICBjb25zdCBhY3Rpb25zID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFB1c2hBY3Rpb25zRm9yRXZlbnQoZXYpO1xyXG4gICAgICAgIGlmIChhY3Rpb25zICYmIGFjdGlvbnMubm90aWZ5KSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzRW5hYmxlZCgpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaXNwbGF5UG9wdXBOb3RpZmljYXRpb24oZXYsIHJvb20pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChhY3Rpb25zLnR3ZWFrcy5zb3VuZCAmJiB0aGlzLmlzQXVkaW9FbmFibGVkKCkpIHtcclxuICAgICAgICAgICAgICAgIFBsYXRmb3JtUGVnLmdldCgpLmxvdWROb3RpZmljYXRpb24oZXYsIHJvb20pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcGxheUF1ZGlvTm90aWZpY2F0aW9uKGV2LCByb29tKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbn07XHJcblxyXG5pZiAoIWdsb2JhbC5teE5vdGlmaWVyKSB7XHJcbiAgICBnbG9iYWwubXhOb3RpZmllciA9IE5vdGlmaWVyO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBnbG9iYWwubXhOb3RpZmllcjtcclxuIl19