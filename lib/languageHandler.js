"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newTranslatableError = newTranslatableError;
exports._td = _td;
exports._t = _t;
exports.substitute = substitute;
exports.replaceByRegexes = replaceByRegexes;
exports.setMissingEntryGenerator = setMissingEntryGenerator;
exports.setLanguage = setLanguage;
exports.getAllLanguagesFromJson = getAllLanguagesFromJson;
exports.getLanguagesFromBrowser = getLanguagesFromBrowser;
exports.getLanguageFromBrowser = getLanguageFromBrowser;
exports.getNormalizedLanguageKeys = getNormalizedLanguageKeys;
exports.normalizeLanguageKey = normalizeLanguageKey;
exports.getCurrentLanguage = getCurrentLanguage;
exports.pickBestLanguage = pickBestLanguage;

var _browserRequest = _interopRequireDefault(require("browser-request"));

var _counterpart = _interopRequireDefault(require("counterpart"));

var _react = _interopRequireDefault(require("react"));

var _SettingsStore = _interopRequireWildcard(require("./settings/SettingsStore"));

var _PlatformPeg = _interopRequireDefault(require("./PlatformPeg"));

var _languages = _interopRequireDefault(require("$webapp/i18n/languages.json"));

/*
Copyright 2017 MTRNord and Cooperative EITA
Copyright 2017 Vector Creations Ltd.
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// $webapp is a webpack resolve alias pointing to the output directory, see webpack config
const i18nFolder = 'i18n/'; // Control whether to also return original, untranslated strings
// Useful for debugging and testing

const ANNOTATE_STRINGS = false; // We use english strings as keys, some of which contain full stops

_counterpart.default.setSeparator('|'); // Fall back to English


_counterpart.default.setFallbackLocale('en');
/**
 * Helper function to create an error which has an English message
 * with a translatedMessage property for use by the consumer.
 * @param {string} message Message to translate.
 * @returns {Error} The constructed error.
 */


function newTranslatableError(message) {
  const error = new Error(message);
  error.translatedMessage = _t(message);
  return error;
} // Function which only purpose is to mark that a string is translatable
// Does not actually do anything. It's helpful for automatic extraction of translatable strings


function _td(s) {
  return s;
} // Wrapper for counterpart's translation function so that it handles nulls and undefineds properly
// Takes the same arguments as counterpart.translate()


function safeCounterpartTranslate(text, options) {
  // Horrible hack to avoid https://github.com/vector-im/riot-web/issues/4191
  // The interpolation library that counterpart uses does not support undefined/null
  // values and instead will throw an error. This is a problem since everywhere else
  // in JS land passing undefined/null will simply stringify instead, and when converting
  // valid ES6 template strings to i18n strings it's extremely easy to pass undefined/null
  // if there are no existing null guards. To avoid this making the app completely inoperable,
  // we'll check all the values for undefined/null and stringify them here.
  let count;

  if (options && typeof options === 'object') {
    count = options['count'];
    Object.keys(options).forEach(k => {
      if (options[k] === undefined) {
        console.warn("safeCounterpartTranslate called with undefined interpolation name: " + k);
        options[k] = 'undefined';
      }

      if (options[k] === null) {
        console.warn("safeCounterpartTranslate called with null interpolation name: " + k);
        options[k] = 'null';
      }
    });
  }

  let translated = _counterpart.default.translate(text, options);

  if (translated === undefined && count !== undefined) {
    // counterpart does not do fallback if no pluralisation exists
    // in the preferred language, so do it here
    translated = _counterpart.default.translate(text, Object.assign({}, options, {
      locale: 'en'
    }));
  }

  return translated;
}
/*
 * Translates text and optionally also replaces XML-ish elements in the text with e.g. React components
 * @param {string} text The untranslated text, e.g "click <a>here</a> now to %(foo)s".
 * @param {object} variables Variable substitutions, e.g { foo: 'bar' }
 * @param {object} tags Tag substitutions e.g. { 'a': (sub) => <a>{sub}</a> }
 *
 * In both variables and tags, the values to substitute with can be either simple strings, React components,
 * or functions that return the value to use in the substitution (e.g. return a React component). In case of
 * a tag replacement, the function receives as the argument the text inside the element corresponding to the tag.
 *
 * Use tag substitutions if you need to translate text between tags (e.g. "<a>Click here!</a>"), otherwise
 * you will end up with literal "<a>" in your output, rather than HTML. Note that you can also use variable
 * substitution to insert React components, but you can't use it to translate text between tags.
 *
 * @return a React <span> component if any non-strings were used in substitutions, otherwise a string
 */


function _t(text, variables, tags) {
  // Don't do substitutions in counterpart. We handle it ourselves so we can replace with React components
  // However, still pass the variables to counterpart so that it can choose the correct plural if count is given
  // It is enough to pass the count variable, but in the future counterpart might make use of other information too
  const args = Object.assign({
    interpolate: false
  }, variables); // The translation returns text so there's no XSS vector here (no unsafe HTML, no code execution)

  const translated = safeCounterpartTranslate(text, args);
  const substituted = substitute(translated, variables, tags); // For development/testing purposes it is useful to also output the original string
  // Don't do that for release versions

  if (ANNOTATE_STRINGS) {
    if (typeof substituted === 'string') {
      return "@@".concat(text, "##").concat(substituted, "@@");
    } else {
      return _react.default.createElement("span", {
        className: "translated-string",
        "data-orig-string": text
      }, substituted);
    }
  } else {
    return substituted;
  }
}
/*
 * Similar to _t(), except only does substitutions, and no translation
 * @param {string} text The text, e.g "click <a>here</a> now to %(foo)s".
 * @param {object} variables Variable substitutions, e.g { foo: 'bar' }
 * @param {object} tags Tag substitutions e.g. { 'a': (sub) => <a>{sub}</a> }
 *
 * The values to substitute with can be either simple strings, or functions that return the value to use in
 * the substitution (e.g. return a React component). In case of a tag replacement, the function receives as
 * the argument the text inside the element corresponding to the tag.
 *
 * @return a React <span> component if any non-strings were used in substitutions, otherwise a string
 */


function substitute(text, variables, tags) {
  let result = text;

  if (variables !== undefined) {
    const regexpMapping = {};

    for (const variable in variables) {
      regexpMapping["%\\(".concat(variable, "\\)s")] = variables[variable];
    }

    result = replaceByRegexes(result, regexpMapping);
  }

  if (tags !== undefined) {
    const regexpMapping = {};

    for (const tag in tags) {
      regexpMapping["(<".concat(tag, ">(.*?)<\\/").concat(tag, ">|<").concat(tag, ">|<").concat(tag, "\\s*\\/>)")] = tags[tag];
    }

    result = replaceByRegexes(result, regexpMapping);
  }

  return result;
}
/*
 * Replace parts of a text using regular expressions
 * @param {string} text The text on which to perform substitutions
 * @param {object} mapping A mapping from regular expressions in string form to replacement string or a
 * function which will receive as the argument the capture groups defined in the regexp. E.g.
 * { 'Hello (.?) World': (sub) => sub.toUpperCase() }
 *
 * @return a React <span> component if any non-strings were used in substitutions, otherwise a string
 */


function replaceByRegexes(text, mapping) {
  // We initially store our output as an array of strings and objects (e.g. React components).
  // This will then be converted to a string or a <span> at the end
  const output = [text]; // If we insert any components we need to wrap the output in a span. React doesn't like just an array of components.

  let shouldWrapInSpan = false;

  for (const regexpString in mapping) {
    // TODO: Cache regexps
    const regexp = new RegExp(regexpString, "g"); // Loop over what output we have so far and perform replacements
    // We look for matches: if we find one, we get three parts: everything before the match, the replaced part,
    // and everything after the match. Insert all three into the output. We need to do this because we can insert objects.
    // Otherwise there would be no need for the splitting and we could do simple replacement.

    let matchFoundSomewhere = false; // If we don't find a match anywhere we want to log it

    for (const outputIndex in output) {
      const inputText = output[outputIndex];

      if (typeof inputText !== 'string') {
        // We might have inserted objects earlier, don't try to replace them
        continue;
      } // process every match in the string
      // starting with the first


      let match = regexp.exec(inputText);
      if (!match) continue;
      matchFoundSomewhere = true; // The textual part before the first match

      const head = inputText.substr(0, match.index);
      const parts = []; // keep track of prevMatch

      let prevMatch;

      while (match) {
        // store prevMatch
        prevMatch = match;
        const capturedGroups = match.slice(2);
        let replaced; // If substitution is a function, call it

        if (mapping[regexpString] instanceof Function) {
          replaced = mapping[regexpString].apply(null, capturedGroups);
        } else {
          replaced = mapping[regexpString];
        }

        if (typeof replaced === 'object') {
          shouldWrapInSpan = true;
        } // Here we also need to check that it actually is a string before comparing against one
        // The head and tail are always strings


        if (typeof replaced !== 'string' || replaced !== '') {
          parts.push(replaced);
        } // try the next match


        match = regexp.exec(inputText); // add the text between prevMatch and this one
        // or the end of the string if prevMatch is the last match

        let tail;

        if (match) {
          const startIndex = prevMatch.index + prevMatch[0].length;
          tail = inputText.substr(startIndex, match.index - startIndex);
        } else {
          tail = inputText.substr(prevMatch.index + prevMatch[0].length);
        }

        if (tail) {
          parts.push(tail);
        }
      } // Insert in reverse order as splice does insert-before and this way we get the final order correct
      // remove the old element at the same time


      output.splice(outputIndex, 1, ...parts);

      if (head !== '') {
        // Don't push empty nodes, they are of no use
        output.splice(outputIndex, 0, head);
      }
    }

    if (!matchFoundSomewhere) {
      // The current regexp did not match anything in the input
      // Missing matches is entirely possible because you might choose to show some variables only in the case
      // of e.g. plurals. It's still a bit suspicious, and could be due to an error, so log it.
      // However, not showing count is so common that it's not worth logging. And other commonly unused variables
      // here, if there are any.
      if (regexpString !== '%\\(count\\)s') {
        console.log("Could not find ".concat(regexp, " in ").concat(text));
      }
    }
  }

  if (shouldWrapInSpan) {
    return _react.default.createElement('span', null, ...output);
  } else {
    return output.join('');
  }
} // Allow overriding the text displayed when no translation exists
// Currently only used in unit tests to avoid having to load
// the translations in riot-web


function setMissingEntryGenerator(f) {
  _counterpart.default.setMissingEntryGenerator(f);
}

function setLanguage(preferredLangs) {
  if (!Array.isArray(preferredLangs)) {
    preferredLangs = [preferredLangs];
  }

  const plaf = _PlatformPeg.default.get();

  if (plaf) {
    plaf.setLanguage(preferredLangs);
  }

  let langToUse;
  let availLangs;
  return getLangsJson().then(result => {
    availLangs = result;

    for (let i = 0; i < preferredLangs.length; ++i) {
      if (availLangs.hasOwnProperty(preferredLangs[i])) {
        langToUse = preferredLangs[i];
        break;
      }
    }

    if (!langToUse) {
      // Fallback to en_EN if none is found
      langToUse = 'en';
      console.error("Unable to find an appropriate language");
    }

    return getLanguage(i18nFolder + availLangs[langToUse].fileName);
  }).then(langData => {
    _counterpart.default.registerTranslations(langToUse, langData);

    _counterpart.default.setLocale(langToUse);

    _SettingsStore.default.setValue("language", null, _SettingsStore.SettingLevel.DEVICE, langToUse);

    console.log("set language to " + langToUse); // Set 'en' as fallback language:

    if (langToUse !== "en") {
      return getLanguage(i18nFolder + availLangs['en'].fileName);
    }
  }).then(langData => {
    if (langData) _counterpart.default.registerTranslations('en', langData);
  });
}

function getAllLanguagesFromJson() {
  return getLangsJson().then(langsObject => {
    const langs = [];

    for (const langKey in langsObject) {
      if (langsObject.hasOwnProperty(langKey)) {
        langs.push({
          'value': langKey,
          'label': langsObject[langKey].label
        });
      }
    }

    return langs;
  });
}

function getLanguagesFromBrowser() {
  if (navigator.languages && navigator.languages.length) return navigator.languages;
  if (navigator.language) return [navigator.language];
  return [navigator.userLanguage || "en"];
}

function getLanguageFromBrowser() {
  return getLanguagesFromBrowser()[0];
}
/**
 * Turns a language string, normalises it,
 * (see normalizeLanguageKey) into an array of language strings
 * with fallback to generic languages
 * (eg. 'pt-BR' => ['pt-br', 'pt'])
 *
 * @param {string} language The input language string
 * @return {string[]} List of normalised languages
 */


function getNormalizedLanguageKeys(language) {
  const languageKeys = [];
  const normalizedLanguage = normalizeLanguageKey(language);
  const languageParts = normalizedLanguage.split('-');

  if (languageParts.length === 2 && languageParts[0] === languageParts[1]) {
    languageKeys.push(languageParts[0]);
  } else {
    languageKeys.push(normalizedLanguage);

    if (languageParts.length === 2) {
      languageKeys.push(languageParts[0]);
    }
  }

  return languageKeys;
}
/**
 * Returns a language string with underscores replaced with
 * hyphens, and lowercased.
 *
 * @param {string} language The language string to be normalized
 * @returns {string} The normalized language string
 */


function normalizeLanguageKey(language) {
  return language.toLowerCase().replace("_", "-");
}

function getCurrentLanguage() {
  return _counterpart.default.getLocale();
}
/**
 * Given a list of language codes, pick the most appropriate one
 * given the current language (ie. getCurrentLanguage())
 * English is assumed to be a reasonable default.
 *
 * @param {string[]} langs List of language codes to pick from
 * @returns {string} The most appropriate language code from langs
 */


function pickBestLanguage(langs) {
  const currentLang = getCurrentLanguage();
  const normalisedLangs = langs.map(normalizeLanguageKey);
  {
    // Best is an exact match
    const currentLangIndex = normalisedLangs.indexOf(currentLang);
    if (currentLangIndex > -1) return langs[currentLangIndex];
  }
  {
    // Failing that, a different dialect of the same language
    const closeLangIndex = normalisedLangs.find(l => l.substr(0, 2) === currentLang.substr(0, 2));
    if (closeLangIndex > -1) return langs[closeLangIndex];
  }
  {
    // Neither of those? Try an english variant.
    const enIndex = normalisedLangs.find(l => l.startsWith('en'));
    if (enIndex > -1) return langs[enIndex];
  } // if nothing else, use the first

  return langs[0];
}

function getLangsJson() {
  return new Promise(async (resolve, reject) => {
    let url;

    if (typeof _languages.default === 'string') {
      // in Jest this 'url' isn't a URL, so just fall through
      url = _languages.default;
    } else {
      url = i18nFolder + 'languages.json';
    }

    (0, _browserRequest.default)({
      method: "GET",
      url
    }, (err, response, body) => {
      if (err || response.status < 200 || response.status >= 300) {
        reject({
          err: err,
          response: response
        });
        return;
      }

      resolve(JSON.parse(body));
    });
  });
}

function weblateToCounterpart(inTrs) {
  const outTrs = {};

  for (const key of Object.keys(inTrs)) {
    const keyParts = key.split('|', 2);

    if (keyParts.length === 2) {
      let obj = outTrs[keyParts[0]];

      if (obj === undefined) {
        obj = {};
        outTrs[keyParts[0]] = obj;
      }

      obj[keyParts[1]] = inTrs[key];
    } else {
      outTrs[key] = inTrs[key];
    }
  }

  return outTrs;
}

function getLanguage(langPath) {
  return new Promise((resolve, reject) => {
    (0, _browserRequest.default)({
      method: "GET",
      url: langPath
    }, (err, response, body) => {
      if (err || response.status < 200 || response.status >= 300) {
        reject({
          err: err,
          response: response
        });
        return;
      }

      resolve(weblateToCounterpart(JSON.parse(body)));
    });
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9sYW5ndWFnZUhhbmRsZXIuanMiXSwibmFtZXMiOlsiaTE4bkZvbGRlciIsIkFOTk9UQVRFX1NUUklOR1MiLCJjb3VudGVycGFydCIsInNldFNlcGFyYXRvciIsInNldEZhbGxiYWNrTG9jYWxlIiwibmV3VHJhbnNsYXRhYmxlRXJyb3IiLCJtZXNzYWdlIiwiZXJyb3IiLCJFcnJvciIsInRyYW5zbGF0ZWRNZXNzYWdlIiwiX3QiLCJfdGQiLCJzIiwic2FmZUNvdW50ZXJwYXJ0VHJhbnNsYXRlIiwidGV4dCIsIm9wdGlvbnMiLCJjb3VudCIsIk9iamVjdCIsImtleXMiLCJmb3JFYWNoIiwiayIsInVuZGVmaW5lZCIsImNvbnNvbGUiLCJ3YXJuIiwidHJhbnNsYXRlZCIsInRyYW5zbGF0ZSIsImFzc2lnbiIsImxvY2FsZSIsInZhcmlhYmxlcyIsInRhZ3MiLCJhcmdzIiwiaW50ZXJwb2xhdGUiLCJzdWJzdGl0dXRlZCIsInN1YnN0aXR1dGUiLCJyZXN1bHQiLCJyZWdleHBNYXBwaW5nIiwidmFyaWFibGUiLCJyZXBsYWNlQnlSZWdleGVzIiwidGFnIiwibWFwcGluZyIsIm91dHB1dCIsInNob3VsZFdyYXBJblNwYW4iLCJyZWdleHBTdHJpbmciLCJyZWdleHAiLCJSZWdFeHAiLCJtYXRjaEZvdW5kU29tZXdoZXJlIiwib3V0cHV0SW5kZXgiLCJpbnB1dFRleHQiLCJtYXRjaCIsImV4ZWMiLCJoZWFkIiwic3Vic3RyIiwiaW5kZXgiLCJwYXJ0cyIsInByZXZNYXRjaCIsImNhcHR1cmVkR3JvdXBzIiwic2xpY2UiLCJyZXBsYWNlZCIsIkZ1bmN0aW9uIiwiYXBwbHkiLCJwdXNoIiwidGFpbCIsInN0YXJ0SW5kZXgiLCJsZW5ndGgiLCJzcGxpY2UiLCJsb2ciLCJSZWFjdCIsImNyZWF0ZUVsZW1lbnQiLCJqb2luIiwic2V0TWlzc2luZ0VudHJ5R2VuZXJhdG9yIiwiZiIsInNldExhbmd1YWdlIiwicHJlZmVycmVkTGFuZ3MiLCJBcnJheSIsImlzQXJyYXkiLCJwbGFmIiwiUGxhdGZvcm1QZWciLCJnZXQiLCJsYW5nVG9Vc2UiLCJhdmFpbExhbmdzIiwiZ2V0TGFuZ3NKc29uIiwidGhlbiIsImkiLCJoYXNPd25Qcm9wZXJ0eSIsImdldExhbmd1YWdlIiwiZmlsZU5hbWUiLCJsYW5nRGF0YSIsInJlZ2lzdGVyVHJhbnNsYXRpb25zIiwic2V0TG9jYWxlIiwiU2V0dGluZ3NTdG9yZSIsInNldFZhbHVlIiwiU2V0dGluZ0xldmVsIiwiREVWSUNFIiwiZ2V0QWxsTGFuZ3VhZ2VzRnJvbUpzb24iLCJsYW5nc09iamVjdCIsImxhbmdzIiwibGFuZ0tleSIsImxhYmVsIiwiZ2V0TGFuZ3VhZ2VzRnJvbUJyb3dzZXIiLCJuYXZpZ2F0b3IiLCJsYW5ndWFnZXMiLCJsYW5ndWFnZSIsInVzZXJMYW5ndWFnZSIsImdldExhbmd1YWdlRnJvbUJyb3dzZXIiLCJnZXROb3JtYWxpemVkTGFuZ3VhZ2VLZXlzIiwibGFuZ3VhZ2VLZXlzIiwibm9ybWFsaXplZExhbmd1YWdlIiwibm9ybWFsaXplTGFuZ3VhZ2VLZXkiLCJsYW5ndWFnZVBhcnRzIiwic3BsaXQiLCJ0b0xvd2VyQ2FzZSIsInJlcGxhY2UiLCJnZXRDdXJyZW50TGFuZ3VhZ2UiLCJnZXRMb2NhbGUiLCJwaWNrQmVzdExhbmd1YWdlIiwiY3VycmVudExhbmciLCJub3JtYWxpc2VkTGFuZ3MiLCJtYXAiLCJjdXJyZW50TGFuZ0luZGV4IiwiaW5kZXhPZiIsImNsb3NlTGFuZ0luZGV4IiwiZmluZCIsImwiLCJlbkluZGV4Iiwic3RhcnRzV2l0aCIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwidXJsIiwid2VicGFja0xhbmdKc29uVXJsIiwibWV0aG9kIiwiZXJyIiwicmVzcG9uc2UiLCJib2R5Iiwic3RhdHVzIiwiSlNPTiIsInBhcnNlIiwid2VibGF0ZVRvQ291bnRlcnBhcnQiLCJpblRycyIsIm91dFRycyIsImtleSIsImtleVBhcnRzIiwib2JqIiwibGFuZ1BhdGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFHQTs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTtBQUdBLE1BQU1BLFVBQVUsR0FBRyxPQUFuQixDLENBRUE7QUFDQTs7QUFDQSxNQUFNQyxnQkFBZ0IsR0FBRyxLQUF6QixDLENBRUE7O0FBQ0FDLHFCQUFZQyxZQUFaLENBQXlCLEdBQXpCLEUsQ0FDQTs7O0FBQ0FELHFCQUFZRSxpQkFBWixDQUE4QixJQUE5QjtBQUVBOzs7Ozs7OztBQU1PLFNBQVNDLG9CQUFULENBQThCQyxPQUE5QixFQUF1QztBQUMxQyxRQUFNQyxLQUFLLEdBQUcsSUFBSUMsS0FBSixDQUFVRixPQUFWLENBQWQ7QUFDQUMsRUFBQUEsS0FBSyxDQUFDRSxpQkFBTixHQUEwQkMsRUFBRSxDQUFDSixPQUFELENBQTVCO0FBQ0EsU0FBT0MsS0FBUDtBQUNILEMsQ0FFRDtBQUNBOzs7QUFDTyxTQUFTSSxHQUFULENBQWFDLENBQWIsRUFBZ0I7QUFDbkIsU0FBT0EsQ0FBUDtBQUNILEMsQ0FFRDtBQUNBOzs7QUFDQSxTQUFTQyx3QkFBVCxDQUFrQ0MsSUFBbEMsRUFBd0NDLE9BQXhDLEVBQWlEO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBSUMsS0FBSjs7QUFFQSxNQUFJRCxPQUFPLElBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFsQyxFQUE0QztBQUN4Q0MsSUFBQUEsS0FBSyxHQUFHRCxPQUFPLENBQUMsT0FBRCxDQUFmO0FBQ0FFLElBQUFBLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSCxPQUFaLEVBQXFCSSxPQUFyQixDQUE4QkMsQ0FBRCxJQUFPO0FBQ2hDLFVBQUlMLE9BQU8sQ0FBQ0ssQ0FBRCxDQUFQLEtBQWVDLFNBQW5CLEVBQThCO0FBQzFCQyxRQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSx3RUFBd0VILENBQXJGO0FBQ0FMLFFBQUFBLE9BQU8sQ0FBQ0ssQ0FBRCxDQUFQLEdBQWEsV0FBYjtBQUNIOztBQUNELFVBQUlMLE9BQU8sQ0FBQ0ssQ0FBRCxDQUFQLEtBQWUsSUFBbkIsRUFBeUI7QUFDckJFLFFBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLG1FQUFtRUgsQ0FBaEY7QUFDQUwsUUFBQUEsT0FBTyxDQUFDSyxDQUFELENBQVAsR0FBYSxNQUFiO0FBQ0g7QUFDSixLQVREO0FBVUg7O0FBQ0QsTUFBSUksVUFBVSxHQUFHdEIscUJBQVl1QixTQUFaLENBQXNCWCxJQUF0QixFQUE0QkMsT0FBNUIsQ0FBakI7O0FBQ0EsTUFBSVMsVUFBVSxLQUFLSCxTQUFmLElBQTRCTCxLQUFLLEtBQUtLLFNBQTFDLEVBQXFEO0FBQ2pEO0FBQ0E7QUFDQUcsSUFBQUEsVUFBVSxHQUFHdEIscUJBQVl1QixTQUFaLENBQXNCWCxJQUF0QixFQUE0QkcsTUFBTSxDQUFDUyxNQUFQLENBQWMsRUFBZCxFQUFrQlgsT0FBbEIsRUFBMkI7QUFBQ1ksTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBM0IsQ0FBNUIsQ0FBYjtBQUNIOztBQUNELFNBQU9ILFVBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQk8sU0FBU2QsRUFBVCxDQUFZSSxJQUFaLEVBQWtCYyxTQUFsQixFQUE2QkMsSUFBN0IsRUFBbUM7QUFDdEM7QUFDQTtBQUNBO0FBQ0EsUUFBTUMsSUFBSSxHQUFHYixNQUFNLENBQUNTLE1BQVAsQ0FBYztBQUFFSyxJQUFBQSxXQUFXLEVBQUU7QUFBZixHQUFkLEVBQXNDSCxTQUF0QyxDQUFiLENBSnNDLENBTXRDOztBQUNBLFFBQU1KLFVBQVUsR0FBR1gsd0JBQXdCLENBQUNDLElBQUQsRUFBT2dCLElBQVAsQ0FBM0M7QUFFQSxRQUFNRSxXQUFXLEdBQUdDLFVBQVUsQ0FBQ1QsVUFBRCxFQUFhSSxTQUFiLEVBQXdCQyxJQUF4QixDQUE5QixDQVRzQyxDQVd0QztBQUNBOztBQUNBLE1BQUk1QixnQkFBSixFQUFzQjtBQUNsQixRQUFJLE9BQU8rQixXQUFQLEtBQXVCLFFBQTNCLEVBQXFDO0FBQ2pDLHlCQUFZbEIsSUFBWixlQUFxQmtCLFdBQXJCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsYUFBTztBQUFNLFFBQUEsU0FBUyxFQUFDLG1CQUFoQjtBQUFvQyw0QkFBa0JsQjtBQUF0RCxTQUE2RGtCLFdBQTdELENBQVA7QUFDSDtBQUNKLEdBTkQsTUFNTztBQUNILFdBQU9BLFdBQVA7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7Ozs7Ozs7O0FBWU8sU0FBU0MsVUFBVCxDQUFvQm5CLElBQXBCLEVBQTBCYyxTQUExQixFQUFxQ0MsSUFBckMsRUFBMkM7QUFDOUMsTUFBSUssTUFBTSxHQUFHcEIsSUFBYjs7QUFFQSxNQUFJYyxTQUFTLEtBQUtQLFNBQWxCLEVBQTZCO0FBQ3pCLFVBQU1jLGFBQWEsR0FBRyxFQUF0Qjs7QUFDQSxTQUFLLE1BQU1DLFFBQVgsSUFBdUJSLFNBQXZCLEVBQWtDO0FBQzlCTyxNQUFBQSxhQUFhLGVBQVFDLFFBQVIsVUFBYixHQUF1Q1IsU0FBUyxDQUFDUSxRQUFELENBQWhEO0FBQ0g7O0FBQ0RGLElBQUFBLE1BQU0sR0FBR0csZ0JBQWdCLENBQUNILE1BQUQsRUFBU0MsYUFBVCxDQUF6QjtBQUNIOztBQUVELE1BQUlOLElBQUksS0FBS1IsU0FBYixFQUF3QjtBQUNwQixVQUFNYyxhQUFhLEdBQUcsRUFBdEI7O0FBQ0EsU0FBSyxNQUFNRyxHQUFYLElBQWtCVCxJQUFsQixFQUF3QjtBQUNwQk0sTUFBQUEsYUFBYSxhQUFNRyxHQUFOLHVCQUFzQkEsR0FBdEIsZ0JBQStCQSxHQUEvQixnQkFBd0NBLEdBQXhDLGVBQWIsR0FBdUVULElBQUksQ0FBQ1MsR0FBRCxDQUEzRTtBQUNIOztBQUNESixJQUFBQSxNQUFNLEdBQUdHLGdCQUFnQixDQUFDSCxNQUFELEVBQVNDLGFBQVQsQ0FBekI7QUFDSDs7QUFFRCxTQUFPRCxNQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7QUFTTyxTQUFTRyxnQkFBVCxDQUEwQnZCLElBQTFCLEVBQWdDeUIsT0FBaEMsRUFBeUM7QUFDNUM7QUFDQTtBQUNBLFFBQU1DLE1BQU0sR0FBRyxDQUFDMUIsSUFBRCxDQUFmLENBSDRDLENBSzVDOztBQUNBLE1BQUkyQixnQkFBZ0IsR0FBRyxLQUF2Qjs7QUFFQSxPQUFLLE1BQU1DLFlBQVgsSUFBMkJILE9BQTNCLEVBQW9DO0FBQ2hDO0FBQ0EsVUFBTUksTUFBTSxHQUFHLElBQUlDLE1BQUosQ0FBV0YsWUFBWCxFQUF5QixHQUF6QixDQUFmLENBRmdDLENBSWhDO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFFBQUlHLG1CQUFtQixHQUFHLEtBQTFCLENBUmdDLENBUUM7O0FBQ2pDLFNBQUssTUFBTUMsV0FBWCxJQUEwQk4sTUFBMUIsRUFBa0M7QUFDOUIsWUFBTU8sU0FBUyxHQUFHUCxNQUFNLENBQUNNLFdBQUQsQ0FBeEI7O0FBQ0EsVUFBSSxPQUFPQyxTQUFQLEtBQXFCLFFBQXpCLEVBQW1DO0FBQUU7QUFDakM7QUFDSCxPQUo2QixDQU05QjtBQUNBOzs7QUFDQSxVQUFJQyxLQUFLLEdBQUdMLE1BQU0sQ0FBQ00sSUFBUCxDQUFZRixTQUFaLENBQVo7QUFFQSxVQUFJLENBQUNDLEtBQUwsRUFBWTtBQUNaSCxNQUFBQSxtQkFBbUIsR0FBRyxJQUF0QixDQVg4QixDQWE5Qjs7QUFDQSxZQUFNSyxJQUFJLEdBQUdILFNBQVMsQ0FBQ0ksTUFBVixDQUFpQixDQUFqQixFQUFvQkgsS0FBSyxDQUFDSSxLQUExQixDQUFiO0FBRUEsWUFBTUMsS0FBSyxHQUFHLEVBQWQsQ0FoQjhCLENBaUI5Qjs7QUFDQSxVQUFJQyxTQUFKOztBQUNBLGFBQU9OLEtBQVAsRUFBYztBQUNWO0FBQ0FNLFFBQUFBLFNBQVMsR0FBR04sS0FBWjtBQUNBLGNBQU1PLGNBQWMsR0FBR1AsS0FBSyxDQUFDUSxLQUFOLENBQVksQ0FBWixDQUF2QjtBQUVBLFlBQUlDLFFBQUosQ0FMVSxDQU1WOztBQUNBLFlBQUlsQixPQUFPLENBQUNHLFlBQUQsQ0FBUCxZQUFpQ2dCLFFBQXJDLEVBQStDO0FBQzNDRCxVQUFBQSxRQUFRLEdBQUdsQixPQUFPLENBQUNHLFlBQUQsQ0FBUCxDQUFzQmlCLEtBQXRCLENBQTRCLElBQTVCLEVBQWtDSixjQUFsQyxDQUFYO0FBQ0gsU0FGRCxNQUVPO0FBQ0hFLFVBQUFBLFFBQVEsR0FBR2xCLE9BQU8sQ0FBQ0csWUFBRCxDQUFsQjtBQUNIOztBQUVELFlBQUksT0FBT2UsUUFBUCxLQUFvQixRQUF4QixFQUFrQztBQUM5QmhCLFVBQUFBLGdCQUFnQixHQUFHLElBQW5CO0FBQ0gsU0FmUyxDQWlCVjtBQUNBOzs7QUFDQSxZQUFJLE9BQU9nQixRQUFQLEtBQW9CLFFBQXBCLElBQWdDQSxRQUFRLEtBQUssRUFBakQsRUFBcUQ7QUFDakRKLFVBQUFBLEtBQUssQ0FBQ08sSUFBTixDQUFXSCxRQUFYO0FBQ0gsU0FyQlMsQ0F1QlY7OztBQUNBVCxRQUFBQSxLQUFLLEdBQUdMLE1BQU0sQ0FBQ00sSUFBUCxDQUFZRixTQUFaLENBQVIsQ0F4QlUsQ0EwQlY7QUFDQTs7QUFDQSxZQUFJYyxJQUFKOztBQUNBLFlBQUliLEtBQUosRUFBVztBQUNQLGdCQUFNYyxVQUFVLEdBQUdSLFNBQVMsQ0FBQ0YsS0FBVixHQUFrQkUsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhUyxNQUFsRDtBQUNBRixVQUFBQSxJQUFJLEdBQUdkLFNBQVMsQ0FBQ0ksTUFBVixDQUFpQlcsVUFBakIsRUFBNkJkLEtBQUssQ0FBQ0ksS0FBTixHQUFjVSxVQUEzQyxDQUFQO0FBQ0gsU0FIRCxNQUdPO0FBQ0hELFVBQUFBLElBQUksR0FBR2QsU0FBUyxDQUFDSSxNQUFWLENBQWlCRyxTQUFTLENBQUNGLEtBQVYsR0FBa0JFLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYVMsTUFBaEQsQ0FBUDtBQUNIOztBQUNELFlBQUlGLElBQUosRUFBVTtBQUNOUixVQUFBQSxLQUFLLENBQUNPLElBQU4sQ0FBV0MsSUFBWDtBQUNIO0FBQ0osT0F6RDZCLENBMkQ5QjtBQUNBOzs7QUFDQXJCLE1BQUFBLE1BQU0sQ0FBQ3dCLE1BQVAsQ0FBY2xCLFdBQWQsRUFBMkIsQ0FBM0IsRUFBOEIsR0FBR08sS0FBakM7O0FBRUEsVUFBSUgsSUFBSSxLQUFLLEVBQWIsRUFBaUI7QUFBRTtBQUNmVixRQUFBQSxNQUFNLENBQUN3QixNQUFQLENBQWNsQixXQUFkLEVBQTJCLENBQTNCLEVBQThCSSxJQUE5QjtBQUNIO0FBQ0o7O0FBQ0QsUUFBSSxDQUFDTCxtQkFBTCxFQUEwQjtBQUFFO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBSUgsWUFBWSxLQUFLLGVBQXJCLEVBQXNDO0FBQ2xDcEIsUUFBQUEsT0FBTyxDQUFDMkMsR0FBUiwwQkFBOEJ0QixNQUE5QixpQkFBMkM3QixJQUEzQztBQUNIO0FBQ0o7QUFDSjs7QUFFRCxNQUFJMkIsZ0JBQUosRUFBc0I7QUFDbEIsV0FBT3lCLGVBQU1DLGFBQU4sQ0FBb0IsTUFBcEIsRUFBNEIsSUFBNUIsRUFBa0MsR0FBRzNCLE1BQXJDLENBQVA7QUFDSCxHQUZELE1BRU87QUFDSCxXQUFPQSxNQUFNLENBQUM0QixJQUFQLENBQVksRUFBWixDQUFQO0FBQ0g7QUFDSixDLENBRUQ7QUFDQTtBQUNBOzs7QUFDTyxTQUFTQyx3QkFBVCxDQUFrQ0MsQ0FBbEMsRUFBcUM7QUFDeENwRSx1QkFBWW1FLHdCQUFaLENBQXFDQyxDQUFyQztBQUNIOztBQUVNLFNBQVNDLFdBQVQsQ0FBcUJDLGNBQXJCLEVBQXFDO0FBQ3hDLE1BQUksQ0FBQ0MsS0FBSyxDQUFDQyxPQUFOLENBQWNGLGNBQWQsQ0FBTCxFQUFvQztBQUNoQ0EsSUFBQUEsY0FBYyxHQUFHLENBQUNBLGNBQUQsQ0FBakI7QUFDSDs7QUFFRCxRQUFNRyxJQUFJLEdBQUdDLHFCQUFZQyxHQUFaLEVBQWI7O0FBQ0EsTUFBSUYsSUFBSixFQUFVO0FBQ05BLElBQUFBLElBQUksQ0FBQ0osV0FBTCxDQUFpQkMsY0FBakI7QUFDSDs7QUFFRCxNQUFJTSxTQUFKO0FBQ0EsTUFBSUMsVUFBSjtBQUNBLFNBQU9DLFlBQVksR0FBR0MsSUFBZixDQUFxQi9DLE1BQUQsSUFBWTtBQUNuQzZDLElBQUFBLFVBQVUsR0FBRzdDLE1BQWI7O0FBRUEsU0FBSyxJQUFJZ0QsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1YsY0FBYyxDQUFDVCxNQUFuQyxFQUEyQyxFQUFFbUIsQ0FBN0MsRUFBZ0Q7QUFDNUMsVUFBSUgsVUFBVSxDQUFDSSxjQUFYLENBQTBCWCxjQUFjLENBQUNVLENBQUQsQ0FBeEMsQ0FBSixFQUFrRDtBQUM5Q0osUUFBQUEsU0FBUyxHQUFHTixjQUFjLENBQUNVLENBQUQsQ0FBMUI7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsUUFBSSxDQUFDSixTQUFMLEVBQWdCO0FBQ1o7QUFDQUEsTUFBQUEsU0FBUyxHQUFHLElBQVo7QUFDQXhELE1BQUFBLE9BQU8sQ0FBQ2YsS0FBUixDQUFjLHdDQUFkO0FBQ0g7O0FBRUQsV0FBTzZFLFdBQVcsQ0FBQ3BGLFVBQVUsR0FBRytFLFVBQVUsQ0FBQ0QsU0FBRCxDQUFWLENBQXNCTyxRQUFwQyxDQUFsQjtBQUNILEdBaEJNLEVBZ0JKSixJQWhCSSxDQWdCRUssUUFBRCxJQUFjO0FBQ2xCcEYseUJBQVlxRixvQkFBWixDQUFpQ1QsU0FBakMsRUFBNENRLFFBQTVDOztBQUNBcEYseUJBQVlzRixTQUFaLENBQXNCVixTQUF0Qjs7QUFDQVcsMkJBQWNDLFFBQWQsQ0FBdUIsVUFBdkIsRUFBbUMsSUFBbkMsRUFBeUNDLDRCQUFhQyxNQUF0RCxFQUE4RGQsU0FBOUQ7O0FBQ0F4RCxJQUFBQSxPQUFPLENBQUMyQyxHQUFSLENBQVkscUJBQXFCYSxTQUFqQyxFQUprQixDQU1sQjs7QUFDQSxRQUFJQSxTQUFTLEtBQUssSUFBbEIsRUFBd0I7QUFDcEIsYUFBT00sV0FBVyxDQUFDcEYsVUFBVSxHQUFHK0UsVUFBVSxDQUFDLElBQUQsQ0FBVixDQUFpQk0sUUFBL0IsQ0FBbEI7QUFDSDtBQUNKLEdBMUJNLEVBMEJKSixJQTFCSSxDQTBCRUssUUFBRCxJQUFjO0FBQ2xCLFFBQUlBLFFBQUosRUFBY3BGLHFCQUFZcUYsb0JBQVosQ0FBaUMsSUFBakMsRUFBdUNELFFBQXZDO0FBQ2pCLEdBNUJNLENBQVA7QUE2Qkg7O0FBRU0sU0FBU08sdUJBQVQsR0FBbUM7QUFDdEMsU0FBT2IsWUFBWSxHQUFHQyxJQUFmLENBQXFCYSxXQUFELElBQWlCO0FBQ3hDLFVBQU1DLEtBQUssR0FBRyxFQUFkOztBQUNBLFNBQUssTUFBTUMsT0FBWCxJQUFzQkYsV0FBdEIsRUFBbUM7QUFDL0IsVUFBSUEsV0FBVyxDQUFDWCxjQUFaLENBQTJCYSxPQUEzQixDQUFKLEVBQXlDO0FBQ3JDRCxRQUFBQSxLQUFLLENBQUNuQyxJQUFOLENBQVc7QUFDUCxtQkFBU29DLE9BREY7QUFFUCxtQkFBU0YsV0FBVyxDQUFDRSxPQUFELENBQVgsQ0FBcUJDO0FBRnZCLFNBQVg7QUFJSDtBQUNKOztBQUNELFdBQU9GLEtBQVA7QUFDSCxHQVhNLENBQVA7QUFZSDs7QUFFTSxTQUFTRyx1QkFBVCxHQUFtQztBQUN0QyxNQUFJQyxTQUFTLENBQUNDLFNBQVYsSUFBdUJELFNBQVMsQ0FBQ0MsU0FBVixDQUFvQnJDLE1BQS9DLEVBQXVELE9BQU9vQyxTQUFTLENBQUNDLFNBQWpCO0FBQ3ZELE1BQUlELFNBQVMsQ0FBQ0UsUUFBZCxFQUF3QixPQUFPLENBQUNGLFNBQVMsQ0FBQ0UsUUFBWCxDQUFQO0FBQ3hCLFNBQU8sQ0FBQ0YsU0FBUyxDQUFDRyxZQUFWLElBQTBCLElBQTNCLENBQVA7QUFDSDs7QUFFTSxTQUFTQyxzQkFBVCxHQUFrQztBQUNyQyxTQUFPTCx1QkFBdUIsR0FBRyxDQUFILENBQTlCO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7QUFTTyxTQUFTTSx5QkFBVCxDQUFtQ0gsUUFBbkMsRUFBNkM7QUFDaEQsUUFBTUksWUFBWSxHQUFHLEVBQXJCO0FBQ0EsUUFBTUMsa0JBQWtCLEdBQUdDLG9CQUFvQixDQUFDTixRQUFELENBQS9DO0FBQ0EsUUFBTU8sYUFBYSxHQUFHRixrQkFBa0IsQ0FBQ0csS0FBbkIsQ0FBeUIsR0FBekIsQ0FBdEI7O0FBQ0EsTUFBSUQsYUFBYSxDQUFDN0MsTUFBZCxLQUF5QixDQUF6QixJQUE4QjZDLGFBQWEsQ0FBQyxDQUFELENBQWIsS0FBcUJBLGFBQWEsQ0FBQyxDQUFELENBQXBFLEVBQXlFO0FBQ3JFSCxJQUFBQSxZQUFZLENBQUM3QyxJQUFiLENBQWtCZ0QsYUFBYSxDQUFDLENBQUQsQ0FBL0I7QUFDSCxHQUZELE1BRU87QUFDSEgsSUFBQUEsWUFBWSxDQUFDN0MsSUFBYixDQUFrQjhDLGtCQUFsQjs7QUFDQSxRQUFJRSxhQUFhLENBQUM3QyxNQUFkLEtBQXlCLENBQTdCLEVBQWdDO0FBQzVCMEMsTUFBQUEsWUFBWSxDQUFDN0MsSUFBYixDQUFrQmdELGFBQWEsQ0FBQyxDQUFELENBQS9CO0FBQ0g7QUFDSjs7QUFDRCxTQUFPSCxZQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7O0FBT08sU0FBU0Usb0JBQVQsQ0FBOEJOLFFBQTlCLEVBQXdDO0FBQzNDLFNBQU9BLFFBQVEsQ0FBQ1MsV0FBVCxHQUF1QkMsT0FBdkIsQ0FBK0IsR0FBL0IsRUFBb0MsR0FBcEMsQ0FBUDtBQUNIOztBQUVNLFNBQVNDLGtCQUFULEdBQThCO0FBQ2pDLFNBQU85RyxxQkFBWStHLFNBQVosRUFBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7QUFRTyxTQUFTQyxnQkFBVCxDQUEwQm5CLEtBQTFCLEVBQWlDO0FBQ3BDLFFBQU1vQixXQUFXLEdBQUdILGtCQUFrQixFQUF0QztBQUNBLFFBQU1JLGVBQWUsR0FBR3JCLEtBQUssQ0FBQ3NCLEdBQU4sQ0FBVVYsb0JBQVYsQ0FBeEI7QUFFQTtBQUNJO0FBQ0EsVUFBTVcsZ0JBQWdCLEdBQUdGLGVBQWUsQ0FBQ0csT0FBaEIsQ0FBd0JKLFdBQXhCLENBQXpCO0FBQ0EsUUFBSUcsZ0JBQWdCLEdBQUcsQ0FBQyxDQUF4QixFQUEyQixPQUFPdkIsS0FBSyxDQUFDdUIsZ0JBQUQsQ0FBWjtBQUM5QjtBQUVEO0FBQ0k7QUFDQSxVQUFNRSxjQUFjLEdBQUdKLGVBQWUsQ0FBQ0ssSUFBaEIsQ0FBc0JDLENBQUQsSUFBT0EsQ0FBQyxDQUFDdkUsTUFBRixDQUFTLENBQVQsRUFBWSxDQUFaLE1BQW1CZ0UsV0FBVyxDQUFDaEUsTUFBWixDQUFtQixDQUFuQixFQUFzQixDQUF0QixDQUEvQyxDQUF2QjtBQUNBLFFBQUlxRSxjQUFjLEdBQUcsQ0FBQyxDQUF0QixFQUF5QixPQUFPekIsS0FBSyxDQUFDeUIsY0FBRCxDQUFaO0FBQzVCO0FBRUQ7QUFDSTtBQUNBLFVBQU1HLE9BQU8sR0FBR1AsZUFBZSxDQUFDSyxJQUFoQixDQUFzQkMsQ0FBRCxJQUFPQSxDQUFDLENBQUNFLFVBQUYsQ0FBYSxJQUFiLENBQTVCLENBQWhCO0FBQ0EsUUFBSUQsT0FBTyxHQUFHLENBQUMsQ0FBZixFQUFrQixPQUFPNUIsS0FBSyxDQUFDNEIsT0FBRCxDQUFaO0FBQ3JCLEdBcEJtQyxDQXNCcEM7O0FBQ0EsU0FBTzVCLEtBQUssQ0FBQyxDQUFELENBQVo7QUFDSDs7QUFFRCxTQUFTZixZQUFULEdBQXdCO0FBQ3BCLFNBQU8sSUFBSTZDLE9BQUosQ0FBWSxPQUFPQyxPQUFQLEVBQWdCQyxNQUFoQixLQUEyQjtBQUMxQyxRQUFJQyxHQUFKOztBQUNBLFFBQUksT0FBT0Msa0JBQVAsS0FBK0IsUUFBbkMsRUFBNkM7QUFBRTtBQUMzQ0QsTUFBQUEsR0FBRyxHQUFHQyxrQkFBTjtBQUNILEtBRkQsTUFFTztBQUNIRCxNQUFBQSxHQUFHLEdBQUdoSSxVQUFVLEdBQUcsZ0JBQW5CO0FBQ0g7O0FBQ0QsaUNBQ0k7QUFBRWtJLE1BQUFBLE1BQU0sRUFBRSxLQUFWO0FBQWlCRixNQUFBQTtBQUFqQixLQURKLEVBRUksQ0FBQ0csR0FBRCxFQUFNQyxRQUFOLEVBQWdCQyxJQUFoQixLQUF5QjtBQUNyQixVQUFJRixHQUFHLElBQUlDLFFBQVEsQ0FBQ0UsTUFBVCxHQUFrQixHQUF6QixJQUFnQ0YsUUFBUSxDQUFDRSxNQUFULElBQW1CLEdBQXZELEVBQTREO0FBQ3hEUCxRQUFBQSxNQUFNLENBQUM7QUFBQ0ksVUFBQUEsR0FBRyxFQUFFQSxHQUFOO0FBQVdDLFVBQUFBLFFBQVEsRUFBRUE7QUFBckIsU0FBRCxDQUFOO0FBQ0E7QUFDSDs7QUFDRE4sTUFBQUEsT0FBTyxDQUFDUyxJQUFJLENBQUNDLEtBQUwsQ0FBV0gsSUFBWCxDQUFELENBQVA7QUFDSCxLQVJMO0FBVUgsR0FqQk0sQ0FBUDtBQWtCSDs7QUFFRCxTQUFTSSxvQkFBVCxDQUE4QkMsS0FBOUIsRUFBcUM7QUFDakMsUUFBTUMsTUFBTSxHQUFHLEVBQWY7O0FBRUEsT0FBSyxNQUFNQyxHQUFYLElBQWtCM0gsTUFBTSxDQUFDQyxJQUFQLENBQVl3SCxLQUFaLENBQWxCLEVBQXNDO0FBQ2xDLFVBQU1HLFFBQVEsR0FBR0QsR0FBRyxDQUFDL0IsS0FBSixDQUFVLEdBQVYsRUFBZSxDQUFmLENBQWpCOztBQUNBLFFBQUlnQyxRQUFRLENBQUM5RSxNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLFVBQUkrRSxHQUFHLEdBQUdILE1BQU0sQ0FBQ0UsUUFBUSxDQUFDLENBQUQsQ0FBVCxDQUFoQjs7QUFDQSxVQUFJQyxHQUFHLEtBQUt6SCxTQUFaLEVBQXVCO0FBQ25CeUgsUUFBQUEsR0FBRyxHQUFHLEVBQU47QUFDQUgsUUFBQUEsTUFBTSxDQUFDRSxRQUFRLENBQUMsQ0FBRCxDQUFULENBQU4sR0FBc0JDLEdBQXRCO0FBQ0g7O0FBQ0RBLE1BQUFBLEdBQUcsQ0FBQ0QsUUFBUSxDQUFDLENBQUQsQ0FBVCxDQUFILEdBQW1CSCxLQUFLLENBQUNFLEdBQUQsQ0FBeEI7QUFDSCxLQVBELE1BT087QUFDSEQsTUFBQUEsTUFBTSxDQUFDQyxHQUFELENBQU4sR0FBY0YsS0FBSyxDQUFDRSxHQUFELENBQW5CO0FBQ0g7QUFDSjs7QUFFRCxTQUFPRCxNQUFQO0FBQ0g7O0FBRUQsU0FBU3ZELFdBQVQsQ0FBcUIyRCxRQUFyQixFQUErQjtBQUMzQixTQUFPLElBQUlsQixPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQ3BDLGlDQUNJO0FBQUVHLE1BQUFBLE1BQU0sRUFBRSxLQUFWO0FBQWlCRixNQUFBQSxHQUFHLEVBQUVlO0FBQXRCLEtBREosRUFFSSxDQUFDWixHQUFELEVBQU1DLFFBQU4sRUFBZ0JDLElBQWhCLEtBQXlCO0FBQ3JCLFVBQUlGLEdBQUcsSUFBSUMsUUFBUSxDQUFDRSxNQUFULEdBQWtCLEdBQXpCLElBQWdDRixRQUFRLENBQUNFLE1BQVQsSUFBbUIsR0FBdkQsRUFBNEQ7QUFDeERQLFFBQUFBLE1BQU0sQ0FBQztBQUFDSSxVQUFBQSxHQUFHLEVBQUVBLEdBQU47QUFBV0MsVUFBQUEsUUFBUSxFQUFFQTtBQUFyQixTQUFELENBQU47QUFDQTtBQUNIOztBQUNETixNQUFBQSxPQUFPLENBQUNXLG9CQUFvQixDQUFDRixJQUFJLENBQUNDLEtBQUwsQ0FBV0gsSUFBWCxDQUFELENBQXJCLENBQVA7QUFDSCxLQVJMO0FBVUgsR0FYTSxDQUFQO0FBWUgiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBNVFJOb3JkIGFuZCBDb29wZXJhdGl2ZSBFSVRBXHJcbkNvcHlyaWdodCAyMDE3IFZlY3RvciBDcmVhdGlvbnMgTHRkLlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5Db3B5cmlnaHQgMjAxOSBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgcmVxdWVzdCBmcm9tICdicm93c2VyLXJlcXVlc3QnO1xyXG5pbXBvcnQgY291bnRlcnBhcnQgZnJvbSAnY291bnRlcnBhcnQnO1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgU2V0dGluZ3NTdG9yZSwge1NldHRpbmdMZXZlbH0gZnJvbSBcIi4vc2V0dGluZ3MvU2V0dGluZ3NTdG9yZVwiO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSBcIi4vUGxhdGZvcm1QZWdcIjtcclxuXHJcbi8vICR3ZWJhcHAgaXMgYSB3ZWJwYWNrIHJlc29sdmUgYWxpYXMgcG9pbnRpbmcgdG8gdGhlIG91dHB1dCBkaXJlY3RvcnksIHNlZSB3ZWJwYWNrIGNvbmZpZ1xyXG5pbXBvcnQgd2VicGFja0xhbmdKc29uVXJsIGZyb20gXCIkd2ViYXBwL2kxOG4vbGFuZ3VhZ2VzLmpzb25cIjtcclxuXHJcbmNvbnN0IGkxOG5Gb2xkZXIgPSAnaTE4bi8nO1xyXG5cclxuLy8gQ29udHJvbCB3aGV0aGVyIHRvIGFsc28gcmV0dXJuIG9yaWdpbmFsLCB1bnRyYW5zbGF0ZWQgc3RyaW5nc1xyXG4vLyBVc2VmdWwgZm9yIGRlYnVnZ2luZyBhbmQgdGVzdGluZ1xyXG5jb25zdCBBTk5PVEFURV9TVFJJTkdTID0gZmFsc2U7XHJcblxyXG4vLyBXZSB1c2UgZW5nbGlzaCBzdHJpbmdzIGFzIGtleXMsIHNvbWUgb2Ygd2hpY2ggY29udGFpbiBmdWxsIHN0b3BzXHJcbmNvdW50ZXJwYXJ0LnNldFNlcGFyYXRvcignfCcpO1xyXG4vLyBGYWxsIGJhY2sgdG8gRW5nbGlzaFxyXG5jb3VudGVycGFydC5zZXRGYWxsYmFja0xvY2FsZSgnZW4nKTtcclxuXHJcbi8qKlxyXG4gKiBIZWxwZXIgZnVuY3Rpb24gdG8gY3JlYXRlIGFuIGVycm9yIHdoaWNoIGhhcyBhbiBFbmdsaXNoIG1lc3NhZ2VcclxuICogd2l0aCBhIHRyYW5zbGF0ZWRNZXNzYWdlIHByb3BlcnR5IGZvciB1c2UgYnkgdGhlIGNvbnN1bWVyLlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gbWVzc2FnZSBNZXNzYWdlIHRvIHRyYW5zbGF0ZS5cclxuICogQHJldHVybnMge0Vycm9yfSBUaGUgY29uc3RydWN0ZWQgZXJyb3IuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gbmV3VHJhbnNsYXRhYmxlRXJyb3IobWVzc2FnZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBuZXcgRXJyb3IobWVzc2FnZSk7XHJcbiAgICBlcnJvci50cmFuc2xhdGVkTWVzc2FnZSA9IF90KG1lc3NhZ2UpO1xyXG4gICAgcmV0dXJuIGVycm9yO1xyXG59XHJcblxyXG4vLyBGdW5jdGlvbiB3aGljaCBvbmx5IHB1cnBvc2UgaXMgdG8gbWFyayB0aGF0IGEgc3RyaW5nIGlzIHRyYW5zbGF0YWJsZVxyXG4vLyBEb2VzIG5vdCBhY3R1YWxseSBkbyBhbnl0aGluZy4gSXQncyBoZWxwZnVsIGZvciBhdXRvbWF0aWMgZXh0cmFjdGlvbiBvZiB0cmFuc2xhdGFibGUgc3RyaW5nc1xyXG5leHBvcnQgZnVuY3Rpb24gX3RkKHMpIHtcclxuICAgIHJldHVybiBzO1xyXG59XHJcblxyXG4vLyBXcmFwcGVyIGZvciBjb3VudGVycGFydCdzIHRyYW5zbGF0aW9uIGZ1bmN0aW9uIHNvIHRoYXQgaXQgaGFuZGxlcyBudWxscyBhbmQgdW5kZWZpbmVkcyBwcm9wZXJseVxyXG4vLyBUYWtlcyB0aGUgc2FtZSBhcmd1bWVudHMgYXMgY291bnRlcnBhcnQudHJhbnNsYXRlKClcclxuZnVuY3Rpb24gc2FmZUNvdW50ZXJwYXJ0VHJhbnNsYXRlKHRleHQsIG9wdGlvbnMpIHtcclxuICAgIC8vIEhvcnJpYmxlIGhhY2sgdG8gYXZvaWQgaHR0cHM6Ly9naXRodWIuY29tL3ZlY3Rvci1pbS9yaW90LXdlYi9pc3N1ZXMvNDE5MVxyXG4gICAgLy8gVGhlIGludGVycG9sYXRpb24gbGlicmFyeSB0aGF0IGNvdW50ZXJwYXJ0IHVzZXMgZG9lcyBub3Qgc3VwcG9ydCB1bmRlZmluZWQvbnVsbFxyXG4gICAgLy8gdmFsdWVzIGFuZCBpbnN0ZWFkIHdpbGwgdGhyb3cgYW4gZXJyb3IuIFRoaXMgaXMgYSBwcm9ibGVtIHNpbmNlIGV2ZXJ5d2hlcmUgZWxzZVxyXG4gICAgLy8gaW4gSlMgbGFuZCBwYXNzaW5nIHVuZGVmaW5lZC9udWxsIHdpbGwgc2ltcGx5IHN0cmluZ2lmeSBpbnN0ZWFkLCBhbmQgd2hlbiBjb252ZXJ0aW5nXHJcbiAgICAvLyB2YWxpZCBFUzYgdGVtcGxhdGUgc3RyaW5ncyB0byBpMThuIHN0cmluZ3MgaXQncyBleHRyZW1lbHkgZWFzeSB0byBwYXNzIHVuZGVmaW5lZC9udWxsXHJcbiAgICAvLyBpZiB0aGVyZSBhcmUgbm8gZXhpc3RpbmcgbnVsbCBndWFyZHMuIFRvIGF2b2lkIHRoaXMgbWFraW5nIHRoZSBhcHAgY29tcGxldGVseSBpbm9wZXJhYmxlLFxyXG4gICAgLy8gd2UnbGwgY2hlY2sgYWxsIHRoZSB2YWx1ZXMgZm9yIHVuZGVmaW5lZC9udWxsIGFuZCBzdHJpbmdpZnkgdGhlbSBoZXJlLlxyXG4gICAgbGV0IGNvdW50O1xyXG5cclxuICAgIGlmIChvcHRpb25zICYmIHR5cGVvZiBvcHRpb25zID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgIGNvdW50ID0gb3B0aW9uc1snY291bnQnXTtcclxuICAgICAgICBPYmplY3Qua2V5cyhvcHRpb25zKS5mb3JFYWNoKChrKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChvcHRpb25zW2tdID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcInNhZmVDb3VudGVycGFydFRyYW5zbGF0ZSBjYWxsZWQgd2l0aCB1bmRlZmluZWQgaW50ZXJwb2xhdGlvbiBuYW1lOiBcIiArIGspO1xyXG4gICAgICAgICAgICAgICAgb3B0aW9uc1trXSA9ICd1bmRlZmluZWQnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChvcHRpb25zW2tdID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXCJzYWZlQ291bnRlcnBhcnRUcmFuc2xhdGUgY2FsbGVkIHdpdGggbnVsbCBpbnRlcnBvbGF0aW9uIG5hbWU6IFwiICsgayk7XHJcbiAgICAgICAgICAgICAgICBvcHRpb25zW2tdID0gJ251bGwnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBsZXQgdHJhbnNsYXRlZCA9IGNvdW50ZXJwYXJ0LnRyYW5zbGF0ZSh0ZXh0LCBvcHRpb25zKTtcclxuICAgIGlmICh0cmFuc2xhdGVkID09PSB1bmRlZmluZWQgJiYgY291bnQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIC8vIGNvdW50ZXJwYXJ0IGRvZXMgbm90IGRvIGZhbGxiYWNrIGlmIG5vIHBsdXJhbGlzYXRpb24gZXhpc3RzXHJcbiAgICAgICAgLy8gaW4gdGhlIHByZWZlcnJlZCBsYW5ndWFnZSwgc28gZG8gaXQgaGVyZVxyXG4gICAgICAgIHRyYW5zbGF0ZWQgPSBjb3VudGVycGFydC50cmFuc2xhdGUodGV4dCwgT2JqZWN0LmFzc2lnbih7fSwgb3B0aW9ucywge2xvY2FsZTogJ2VuJ30pKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cmFuc2xhdGVkO1xyXG59XHJcblxyXG4vKlxyXG4gKiBUcmFuc2xhdGVzIHRleHQgYW5kIG9wdGlvbmFsbHkgYWxzbyByZXBsYWNlcyBYTUwtaXNoIGVsZW1lbnRzIGluIHRoZSB0ZXh0IHdpdGggZS5nLiBSZWFjdCBjb21wb25lbnRzXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0IFRoZSB1bnRyYW5zbGF0ZWQgdGV4dCwgZS5nIFwiY2xpY2sgPGE+aGVyZTwvYT4gbm93IHRvICUoZm9vKXNcIi5cclxuICogQHBhcmFtIHtvYmplY3R9IHZhcmlhYmxlcyBWYXJpYWJsZSBzdWJzdGl0dXRpb25zLCBlLmcgeyBmb286ICdiYXInIH1cclxuICogQHBhcmFtIHtvYmplY3R9IHRhZ3MgVGFnIHN1YnN0aXR1dGlvbnMgZS5nLiB7ICdhJzogKHN1YikgPT4gPGE+e3N1Yn08L2E+IH1cclxuICpcclxuICogSW4gYm90aCB2YXJpYWJsZXMgYW5kIHRhZ3MsIHRoZSB2YWx1ZXMgdG8gc3Vic3RpdHV0ZSB3aXRoIGNhbiBiZSBlaXRoZXIgc2ltcGxlIHN0cmluZ3MsIFJlYWN0IGNvbXBvbmVudHMsXHJcbiAqIG9yIGZ1bmN0aW9ucyB0aGF0IHJldHVybiB0aGUgdmFsdWUgdG8gdXNlIGluIHRoZSBzdWJzdGl0dXRpb24gKGUuZy4gcmV0dXJuIGEgUmVhY3QgY29tcG9uZW50KS4gSW4gY2FzZSBvZlxyXG4gKiBhIHRhZyByZXBsYWNlbWVudCwgdGhlIGZ1bmN0aW9uIHJlY2VpdmVzIGFzIHRoZSBhcmd1bWVudCB0aGUgdGV4dCBpbnNpZGUgdGhlIGVsZW1lbnQgY29ycmVzcG9uZGluZyB0byB0aGUgdGFnLlxyXG4gKlxyXG4gKiBVc2UgdGFnIHN1YnN0aXR1dGlvbnMgaWYgeW91IG5lZWQgdG8gdHJhbnNsYXRlIHRleHQgYmV0d2VlbiB0YWdzIChlLmcuIFwiPGE+Q2xpY2sgaGVyZSE8L2E+XCIpLCBvdGhlcndpc2VcclxuICogeW91IHdpbGwgZW5kIHVwIHdpdGggbGl0ZXJhbCBcIjxhPlwiIGluIHlvdXIgb3V0cHV0LCByYXRoZXIgdGhhbiBIVE1MLiBOb3RlIHRoYXQgeW91IGNhbiBhbHNvIHVzZSB2YXJpYWJsZVxyXG4gKiBzdWJzdGl0dXRpb24gdG8gaW5zZXJ0IFJlYWN0IGNvbXBvbmVudHMsIGJ1dCB5b3UgY2FuJ3QgdXNlIGl0IHRvIHRyYW5zbGF0ZSB0ZXh0IGJldHdlZW4gdGFncy5cclxuICpcclxuICogQHJldHVybiBhIFJlYWN0IDxzcGFuPiBjb21wb25lbnQgaWYgYW55IG5vbi1zdHJpbmdzIHdlcmUgdXNlZCBpbiBzdWJzdGl0dXRpb25zLCBvdGhlcndpc2UgYSBzdHJpbmdcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBfdCh0ZXh0LCB2YXJpYWJsZXMsIHRhZ3MpIHtcclxuICAgIC8vIERvbid0IGRvIHN1YnN0aXR1dGlvbnMgaW4gY291bnRlcnBhcnQuIFdlIGhhbmRsZSBpdCBvdXJzZWx2ZXMgc28gd2UgY2FuIHJlcGxhY2Ugd2l0aCBSZWFjdCBjb21wb25lbnRzXHJcbiAgICAvLyBIb3dldmVyLCBzdGlsbCBwYXNzIHRoZSB2YXJpYWJsZXMgdG8gY291bnRlcnBhcnQgc28gdGhhdCBpdCBjYW4gY2hvb3NlIHRoZSBjb3JyZWN0IHBsdXJhbCBpZiBjb3VudCBpcyBnaXZlblxyXG4gICAgLy8gSXQgaXMgZW5vdWdoIHRvIHBhc3MgdGhlIGNvdW50IHZhcmlhYmxlLCBidXQgaW4gdGhlIGZ1dHVyZSBjb3VudGVycGFydCBtaWdodCBtYWtlIHVzZSBvZiBvdGhlciBpbmZvcm1hdGlvbiB0b29cclxuICAgIGNvbnN0IGFyZ3MgPSBPYmplY3QuYXNzaWduKHsgaW50ZXJwb2xhdGU6IGZhbHNlIH0sIHZhcmlhYmxlcyk7XHJcblxyXG4gICAgLy8gVGhlIHRyYW5zbGF0aW9uIHJldHVybnMgdGV4dCBzbyB0aGVyZSdzIG5vIFhTUyB2ZWN0b3IgaGVyZSAobm8gdW5zYWZlIEhUTUwsIG5vIGNvZGUgZXhlY3V0aW9uKVxyXG4gICAgY29uc3QgdHJhbnNsYXRlZCA9IHNhZmVDb3VudGVycGFydFRyYW5zbGF0ZSh0ZXh0LCBhcmdzKTtcclxuXHJcbiAgICBjb25zdCBzdWJzdGl0dXRlZCA9IHN1YnN0aXR1dGUodHJhbnNsYXRlZCwgdmFyaWFibGVzLCB0YWdzKTtcclxuXHJcbiAgICAvLyBGb3IgZGV2ZWxvcG1lbnQvdGVzdGluZyBwdXJwb3NlcyBpdCBpcyB1c2VmdWwgdG8gYWxzbyBvdXRwdXQgdGhlIG9yaWdpbmFsIHN0cmluZ1xyXG4gICAgLy8gRG9uJ3QgZG8gdGhhdCBmb3IgcmVsZWFzZSB2ZXJzaW9uc1xyXG4gICAgaWYgKEFOTk9UQVRFX1NUUklOR1MpIHtcclxuICAgICAgICBpZiAodHlwZW9mIHN1YnN0aXR1dGVkID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICByZXR1cm4gYEBAJHt0ZXh0fSMjJHtzdWJzdGl0dXRlZH1AQGA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxzcGFuIGNsYXNzTmFtZT0ndHJhbnNsYXRlZC1zdHJpbmcnIGRhdGEtb3JpZy1zdHJpbmc9e3RleHR9PntzdWJzdGl0dXRlZH08L3NwYW4+O1xyXG4gICAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIHN1YnN0aXR1dGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKlxyXG4gKiBTaW1pbGFyIHRvIF90KCksIGV4Y2VwdCBvbmx5IGRvZXMgc3Vic3RpdHV0aW9ucywgYW5kIG5vIHRyYW5zbGF0aW9uXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0IFRoZSB0ZXh0LCBlLmcgXCJjbGljayA8YT5oZXJlPC9hPiBub3cgdG8gJShmb28pc1wiLlxyXG4gKiBAcGFyYW0ge29iamVjdH0gdmFyaWFibGVzIFZhcmlhYmxlIHN1YnN0aXR1dGlvbnMsIGUuZyB7IGZvbzogJ2JhcicgfVxyXG4gKiBAcGFyYW0ge29iamVjdH0gdGFncyBUYWcgc3Vic3RpdHV0aW9ucyBlLmcuIHsgJ2EnOiAoc3ViKSA9PiA8YT57c3VifTwvYT4gfVxyXG4gKlxyXG4gKiBUaGUgdmFsdWVzIHRvIHN1YnN0aXR1dGUgd2l0aCBjYW4gYmUgZWl0aGVyIHNpbXBsZSBzdHJpbmdzLCBvciBmdW5jdGlvbnMgdGhhdCByZXR1cm4gdGhlIHZhbHVlIHRvIHVzZSBpblxyXG4gKiB0aGUgc3Vic3RpdHV0aW9uIChlLmcuIHJldHVybiBhIFJlYWN0IGNvbXBvbmVudCkuIEluIGNhc2Ugb2YgYSB0YWcgcmVwbGFjZW1lbnQsIHRoZSBmdW5jdGlvbiByZWNlaXZlcyBhc1xyXG4gKiB0aGUgYXJndW1lbnQgdGhlIHRleHQgaW5zaWRlIHRoZSBlbGVtZW50IGNvcnJlc3BvbmRpbmcgdG8gdGhlIHRhZy5cclxuICpcclxuICogQHJldHVybiBhIFJlYWN0IDxzcGFuPiBjb21wb25lbnQgaWYgYW55IG5vbi1zdHJpbmdzIHdlcmUgdXNlZCBpbiBzdWJzdGl0dXRpb25zLCBvdGhlcndpc2UgYSBzdHJpbmdcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBzdWJzdGl0dXRlKHRleHQsIHZhcmlhYmxlcywgdGFncykge1xyXG4gICAgbGV0IHJlc3VsdCA9IHRleHQ7XHJcblxyXG4gICAgaWYgKHZhcmlhYmxlcyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY29uc3QgcmVnZXhwTWFwcGluZyA9IHt9O1xyXG4gICAgICAgIGZvciAoY29uc3QgdmFyaWFibGUgaW4gdmFyaWFibGVzKSB7XHJcbiAgICAgICAgICAgIHJlZ2V4cE1hcHBpbmdbYCVcXFxcKCR7dmFyaWFibGV9XFxcXClzYF0gPSB2YXJpYWJsZXNbdmFyaWFibGVdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXN1bHQgPSByZXBsYWNlQnlSZWdleGVzKHJlc3VsdCwgcmVnZXhwTWFwcGluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRhZ3MgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIGNvbnN0IHJlZ2V4cE1hcHBpbmcgPSB7fTtcclxuICAgICAgICBmb3IgKGNvbnN0IHRhZyBpbiB0YWdzKSB7XHJcbiAgICAgICAgICAgIHJlZ2V4cE1hcHBpbmdbYCg8JHt0YWd9PiguKj8pPFxcXFwvJHt0YWd9Pnw8JHt0YWd9Pnw8JHt0YWd9XFxcXHMqXFxcXC8+KWBdID0gdGFnc1t0YWddO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXN1bHQgPSByZXBsYWNlQnlSZWdleGVzKHJlc3VsdCwgcmVnZXhwTWFwcGluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuLypcclxuICogUmVwbGFjZSBwYXJ0cyBvZiBhIHRleHQgdXNpbmcgcmVndWxhciBleHByZXNzaW9uc1xyXG4gKiBAcGFyYW0ge3N0cmluZ30gdGV4dCBUaGUgdGV4dCBvbiB3aGljaCB0byBwZXJmb3JtIHN1YnN0aXR1dGlvbnNcclxuICogQHBhcmFtIHtvYmplY3R9IG1hcHBpbmcgQSBtYXBwaW5nIGZyb20gcmVndWxhciBleHByZXNzaW9ucyBpbiBzdHJpbmcgZm9ybSB0byByZXBsYWNlbWVudCBzdHJpbmcgb3IgYVxyXG4gKiBmdW5jdGlvbiB3aGljaCB3aWxsIHJlY2VpdmUgYXMgdGhlIGFyZ3VtZW50IHRoZSBjYXB0dXJlIGdyb3VwcyBkZWZpbmVkIGluIHRoZSByZWdleHAuIEUuZy5cclxuICogeyAnSGVsbG8gKC4/KSBXb3JsZCc6IChzdWIpID0+IHN1Yi50b1VwcGVyQ2FzZSgpIH1cclxuICpcclxuICogQHJldHVybiBhIFJlYWN0IDxzcGFuPiBjb21wb25lbnQgaWYgYW55IG5vbi1zdHJpbmdzIHdlcmUgdXNlZCBpbiBzdWJzdGl0dXRpb25zLCBvdGhlcndpc2UgYSBzdHJpbmdcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiByZXBsYWNlQnlSZWdleGVzKHRleHQsIG1hcHBpbmcpIHtcclxuICAgIC8vIFdlIGluaXRpYWxseSBzdG9yZSBvdXIgb3V0cHV0IGFzIGFuIGFycmF5IG9mIHN0cmluZ3MgYW5kIG9iamVjdHMgKGUuZy4gUmVhY3QgY29tcG9uZW50cykuXHJcbiAgICAvLyBUaGlzIHdpbGwgdGhlbiBiZSBjb252ZXJ0ZWQgdG8gYSBzdHJpbmcgb3IgYSA8c3Bhbj4gYXQgdGhlIGVuZFxyXG4gICAgY29uc3Qgb3V0cHV0ID0gW3RleHRdO1xyXG5cclxuICAgIC8vIElmIHdlIGluc2VydCBhbnkgY29tcG9uZW50cyB3ZSBuZWVkIHRvIHdyYXAgdGhlIG91dHB1dCBpbiBhIHNwYW4uIFJlYWN0IGRvZXNuJ3QgbGlrZSBqdXN0IGFuIGFycmF5IG9mIGNvbXBvbmVudHMuXHJcbiAgICBsZXQgc2hvdWxkV3JhcEluU3BhbiA9IGZhbHNlO1xyXG5cclxuICAgIGZvciAoY29uc3QgcmVnZXhwU3RyaW5nIGluIG1hcHBpbmcpIHtcclxuICAgICAgICAvLyBUT0RPOiBDYWNoZSByZWdleHBzXHJcbiAgICAgICAgY29uc3QgcmVnZXhwID0gbmV3IFJlZ0V4cChyZWdleHBTdHJpbmcsIFwiZ1wiKTtcclxuXHJcbiAgICAgICAgLy8gTG9vcCBvdmVyIHdoYXQgb3V0cHV0IHdlIGhhdmUgc28gZmFyIGFuZCBwZXJmb3JtIHJlcGxhY2VtZW50c1xyXG4gICAgICAgIC8vIFdlIGxvb2sgZm9yIG1hdGNoZXM6IGlmIHdlIGZpbmQgb25lLCB3ZSBnZXQgdGhyZWUgcGFydHM6IGV2ZXJ5dGhpbmcgYmVmb3JlIHRoZSBtYXRjaCwgdGhlIHJlcGxhY2VkIHBhcnQsXHJcbiAgICAgICAgLy8gYW5kIGV2ZXJ5dGhpbmcgYWZ0ZXIgdGhlIG1hdGNoLiBJbnNlcnQgYWxsIHRocmVlIGludG8gdGhlIG91dHB1dC4gV2UgbmVlZCB0byBkbyB0aGlzIGJlY2F1c2Ugd2UgY2FuIGluc2VydCBvYmplY3RzLlxyXG4gICAgICAgIC8vIE90aGVyd2lzZSB0aGVyZSB3b3VsZCBiZSBubyBuZWVkIGZvciB0aGUgc3BsaXR0aW5nIGFuZCB3ZSBjb3VsZCBkbyBzaW1wbGUgcmVwbGFjZW1lbnQuXHJcbiAgICAgICAgbGV0IG1hdGNoRm91bmRTb21ld2hlcmUgPSBmYWxzZTsgLy8gSWYgd2UgZG9uJ3QgZmluZCBhIG1hdGNoIGFueXdoZXJlIHdlIHdhbnQgdG8gbG9nIGl0XHJcbiAgICAgICAgZm9yIChjb25zdCBvdXRwdXRJbmRleCBpbiBvdXRwdXQpIHtcclxuICAgICAgICAgICAgY29uc3QgaW5wdXRUZXh0ID0gb3V0cHV0W291dHB1dEluZGV4XTtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBpbnB1dFRleHQgIT09ICdzdHJpbmcnKSB7IC8vIFdlIG1pZ2h0IGhhdmUgaW5zZXJ0ZWQgb2JqZWN0cyBlYXJsaWVyLCBkb24ndCB0cnkgdG8gcmVwbGFjZSB0aGVtXHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gcHJvY2VzcyBldmVyeSBtYXRjaCBpbiB0aGUgc3RyaW5nXHJcbiAgICAgICAgICAgIC8vIHN0YXJ0aW5nIHdpdGggdGhlIGZpcnN0XHJcbiAgICAgICAgICAgIGxldCBtYXRjaCA9IHJlZ2V4cC5leGVjKGlucHV0VGV4dCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIW1hdGNoKSBjb250aW51ZTtcclxuICAgICAgICAgICAgbWF0Y2hGb3VuZFNvbWV3aGVyZSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAvLyBUaGUgdGV4dHVhbCBwYXJ0IGJlZm9yZSB0aGUgZmlyc3QgbWF0Y2hcclxuICAgICAgICAgICAgY29uc3QgaGVhZCA9IGlucHV0VGV4dC5zdWJzdHIoMCwgbWF0Y2guaW5kZXgpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcGFydHMgPSBbXTtcclxuICAgICAgICAgICAgLy8ga2VlcCB0cmFjayBvZiBwcmV2TWF0Y2hcclxuICAgICAgICAgICAgbGV0IHByZXZNYXRjaDtcclxuICAgICAgICAgICAgd2hpbGUgKG1hdGNoKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzdG9yZSBwcmV2TWF0Y2hcclxuICAgICAgICAgICAgICAgIHByZXZNYXRjaCA9IG1hdGNoO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2FwdHVyZWRHcm91cHMgPSBtYXRjaC5zbGljZSgyKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgcmVwbGFjZWQ7XHJcbiAgICAgICAgICAgICAgICAvLyBJZiBzdWJzdGl0dXRpb24gaXMgYSBmdW5jdGlvbiwgY2FsbCBpdFxyXG4gICAgICAgICAgICAgICAgaWYgKG1hcHBpbmdbcmVnZXhwU3RyaW5nXSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVwbGFjZWQgPSBtYXBwaW5nW3JlZ2V4cFN0cmluZ10uYXBwbHkobnVsbCwgY2FwdHVyZWRHcm91cHMpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXBsYWNlZCA9IG1hcHBpbmdbcmVnZXhwU3RyaW5nXTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlcGxhY2VkID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgICAgIHNob3VsZFdyYXBJblNwYW4gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIEhlcmUgd2UgYWxzbyBuZWVkIHRvIGNoZWNrIHRoYXQgaXQgYWN0dWFsbHkgaXMgYSBzdHJpbmcgYmVmb3JlIGNvbXBhcmluZyBhZ2FpbnN0IG9uZVxyXG4gICAgICAgICAgICAgICAgLy8gVGhlIGhlYWQgYW5kIHRhaWwgYXJlIGFsd2F5cyBzdHJpbmdzXHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlcGxhY2VkICE9PSAnc3RyaW5nJyB8fCByZXBsYWNlZCAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXJ0cy5wdXNoKHJlcGxhY2VkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyB0cnkgdGhlIG5leHQgbWF0Y2hcclxuICAgICAgICAgICAgICAgIG1hdGNoID0gcmVnZXhwLmV4ZWMoaW5wdXRUZXh0KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBhZGQgdGhlIHRleHQgYmV0d2VlbiBwcmV2TWF0Y2ggYW5kIHRoaXMgb25lXHJcbiAgICAgICAgICAgICAgICAvLyBvciB0aGUgZW5kIG9mIHRoZSBzdHJpbmcgaWYgcHJldk1hdGNoIGlzIHRoZSBsYXN0IG1hdGNoXHJcbiAgICAgICAgICAgICAgICBsZXQgdGFpbDtcclxuICAgICAgICAgICAgICAgIGlmIChtYXRjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHN0YXJ0SW5kZXggPSBwcmV2TWF0Y2guaW5kZXggKyBwcmV2TWF0Y2hbMF0ubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgICAgIHRhaWwgPSBpbnB1dFRleHQuc3Vic3RyKHN0YXJ0SW5kZXgsIG1hdGNoLmluZGV4IC0gc3RhcnRJbmRleCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhaWwgPSBpbnB1dFRleHQuc3Vic3RyKHByZXZNYXRjaC5pbmRleCArIHByZXZNYXRjaFswXS5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRhaWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXJ0cy5wdXNoKHRhaWwpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBJbnNlcnQgaW4gcmV2ZXJzZSBvcmRlciBhcyBzcGxpY2UgZG9lcyBpbnNlcnQtYmVmb3JlIGFuZCB0aGlzIHdheSB3ZSBnZXQgdGhlIGZpbmFsIG9yZGVyIGNvcnJlY3RcclxuICAgICAgICAgICAgLy8gcmVtb3ZlIHRoZSBvbGQgZWxlbWVudCBhdCB0aGUgc2FtZSB0aW1lXHJcbiAgICAgICAgICAgIG91dHB1dC5zcGxpY2Uob3V0cHV0SW5kZXgsIDEsIC4uLnBhcnRzKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChoZWFkICE9PSAnJykgeyAvLyBEb24ndCBwdXNoIGVtcHR5IG5vZGVzLCB0aGV5IGFyZSBvZiBubyB1c2VcclxuICAgICAgICAgICAgICAgIG91dHB1dC5zcGxpY2Uob3V0cHV0SW5kZXgsIDAsIGhlYWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbWF0Y2hGb3VuZFNvbWV3aGVyZSkgeyAvLyBUaGUgY3VycmVudCByZWdleHAgZGlkIG5vdCBtYXRjaCBhbnl0aGluZyBpbiB0aGUgaW5wdXRcclxuICAgICAgICAgICAgLy8gTWlzc2luZyBtYXRjaGVzIGlzIGVudGlyZWx5IHBvc3NpYmxlIGJlY2F1c2UgeW91IG1pZ2h0IGNob29zZSB0byBzaG93IHNvbWUgdmFyaWFibGVzIG9ubHkgaW4gdGhlIGNhc2VcclxuICAgICAgICAgICAgLy8gb2YgZS5nLiBwbHVyYWxzLiBJdCdzIHN0aWxsIGEgYml0IHN1c3BpY2lvdXMsIGFuZCBjb3VsZCBiZSBkdWUgdG8gYW4gZXJyb3IsIHNvIGxvZyBpdC5cclxuICAgICAgICAgICAgLy8gSG93ZXZlciwgbm90IHNob3dpbmcgY291bnQgaXMgc28gY29tbW9uIHRoYXQgaXQncyBub3Qgd29ydGggbG9nZ2luZy4gQW5kIG90aGVyIGNvbW1vbmx5IHVudXNlZCB2YXJpYWJsZXNcclxuICAgICAgICAgICAgLy8gaGVyZSwgaWYgdGhlcmUgYXJlIGFueS5cclxuICAgICAgICAgICAgaWYgKHJlZ2V4cFN0cmluZyAhPT0gJyVcXFxcKGNvdW50XFxcXClzJykge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coYENvdWxkIG5vdCBmaW5kICR7cmVnZXhwfSBpbiAke3RleHR9YCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNob3VsZFdyYXBJblNwYW4pIHtcclxuICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudCgnc3BhbicsIG51bGwsIC4uLm91dHB1dCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBvdXRwdXQuam9pbignJyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIEFsbG93IG92ZXJyaWRpbmcgdGhlIHRleHQgZGlzcGxheWVkIHdoZW4gbm8gdHJhbnNsYXRpb24gZXhpc3RzXHJcbi8vIEN1cnJlbnRseSBvbmx5IHVzZWQgaW4gdW5pdCB0ZXN0cyB0byBhdm9pZCBoYXZpbmcgdG8gbG9hZFxyXG4vLyB0aGUgdHJhbnNsYXRpb25zIGluIHJpb3Qtd2ViXHJcbmV4cG9ydCBmdW5jdGlvbiBzZXRNaXNzaW5nRW50cnlHZW5lcmF0b3IoZikge1xyXG4gICAgY291bnRlcnBhcnQuc2V0TWlzc2luZ0VudHJ5R2VuZXJhdG9yKGYpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2V0TGFuZ3VhZ2UocHJlZmVycmVkTGFuZ3MpIHtcclxuICAgIGlmICghQXJyYXkuaXNBcnJheShwcmVmZXJyZWRMYW5ncykpIHtcclxuICAgICAgICBwcmVmZXJyZWRMYW5ncyA9IFtwcmVmZXJyZWRMYW5nc107XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcGxhZiA9IFBsYXRmb3JtUGVnLmdldCgpO1xyXG4gICAgaWYgKHBsYWYpIHtcclxuICAgICAgICBwbGFmLnNldExhbmd1YWdlKHByZWZlcnJlZExhbmdzKTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgbGFuZ1RvVXNlO1xyXG4gICAgbGV0IGF2YWlsTGFuZ3M7XHJcbiAgICByZXR1cm4gZ2V0TGFuZ3NKc29uKCkudGhlbigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgYXZhaWxMYW5ncyA9IHJlc3VsdDtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwcmVmZXJyZWRMYW5ncy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBpZiAoYXZhaWxMYW5ncy5oYXNPd25Qcm9wZXJ0eShwcmVmZXJyZWRMYW5nc1tpXSkpIHtcclxuICAgICAgICAgICAgICAgIGxhbmdUb1VzZSA9IHByZWZlcnJlZExhbmdzW2ldO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFsYW5nVG9Vc2UpIHtcclxuICAgICAgICAgICAgLy8gRmFsbGJhY2sgdG8gZW5fRU4gaWYgbm9uZSBpcyBmb3VuZFxyXG4gICAgICAgICAgICBsYW5nVG9Vc2UgPSAnZW4nO1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiVW5hYmxlIHRvIGZpbmQgYW4gYXBwcm9wcmlhdGUgbGFuZ3VhZ2VcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZ2V0TGFuZ3VhZ2UoaTE4bkZvbGRlciArIGF2YWlsTGFuZ3NbbGFuZ1RvVXNlXS5maWxlTmFtZSk7XHJcbiAgICB9KS50aGVuKChsYW5nRGF0YSkgPT4ge1xyXG4gICAgICAgIGNvdW50ZXJwYXJ0LnJlZ2lzdGVyVHJhbnNsYXRpb25zKGxhbmdUb1VzZSwgbGFuZ0RhdGEpO1xyXG4gICAgICAgIGNvdW50ZXJwYXJ0LnNldExvY2FsZShsYW5nVG9Vc2UpO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoXCJsYW5ndWFnZVwiLCBudWxsLCBTZXR0aW5nTGV2ZWwuREVWSUNFLCBsYW5nVG9Vc2UpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwic2V0IGxhbmd1YWdlIHRvIFwiICsgbGFuZ1RvVXNlKTtcclxuXHJcbiAgICAgICAgLy8gU2V0ICdlbicgYXMgZmFsbGJhY2sgbGFuZ3VhZ2U6XHJcbiAgICAgICAgaWYgKGxhbmdUb1VzZSAhPT0gXCJlblwiKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBnZXRMYW5ndWFnZShpMThuRm9sZGVyICsgYXZhaWxMYW5nc1snZW4nXS5maWxlTmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSkudGhlbigobGFuZ0RhdGEpID0+IHtcclxuICAgICAgICBpZiAobGFuZ0RhdGEpIGNvdW50ZXJwYXJ0LnJlZ2lzdGVyVHJhbnNsYXRpb25zKCdlbicsIGxhbmdEYXRhKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0QWxsTGFuZ3VhZ2VzRnJvbUpzb24oKSB7XHJcbiAgICByZXR1cm4gZ2V0TGFuZ3NKc29uKCkudGhlbigobGFuZ3NPYmplY3QpID0+IHtcclxuICAgICAgICBjb25zdCBsYW5ncyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgbGFuZ0tleSBpbiBsYW5nc09iamVjdCkge1xyXG4gICAgICAgICAgICBpZiAobGFuZ3NPYmplY3QuaGFzT3duUHJvcGVydHkobGFuZ0tleSkpIHtcclxuICAgICAgICAgICAgICAgIGxhbmdzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICd2YWx1ZSc6IGxhbmdLZXksXHJcbiAgICAgICAgICAgICAgICAgICAgJ2xhYmVsJzogbGFuZ3NPYmplY3RbbGFuZ0tleV0ubGFiZWwsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbGFuZ3M7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldExhbmd1YWdlc0Zyb21Ccm93c2VyKCkge1xyXG4gICAgaWYgKG5hdmlnYXRvci5sYW5ndWFnZXMgJiYgbmF2aWdhdG9yLmxhbmd1YWdlcy5sZW5ndGgpIHJldHVybiBuYXZpZ2F0b3IubGFuZ3VhZ2VzO1xyXG4gICAgaWYgKG5hdmlnYXRvci5sYW5ndWFnZSkgcmV0dXJuIFtuYXZpZ2F0b3IubGFuZ3VhZ2VdO1xyXG4gICAgcmV0dXJuIFtuYXZpZ2F0b3IudXNlckxhbmd1YWdlIHx8IFwiZW5cIl07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRMYW5ndWFnZUZyb21Ccm93c2VyKCkge1xyXG4gICAgcmV0dXJuIGdldExhbmd1YWdlc0Zyb21Ccm93c2VyKClbMF07XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUdXJucyBhIGxhbmd1YWdlIHN0cmluZywgbm9ybWFsaXNlcyBpdCxcclxuICogKHNlZSBub3JtYWxpemVMYW5ndWFnZUtleSkgaW50byBhbiBhcnJheSBvZiBsYW5ndWFnZSBzdHJpbmdzXHJcbiAqIHdpdGggZmFsbGJhY2sgdG8gZ2VuZXJpYyBsYW5ndWFnZXNcclxuICogKGVnLiAncHQtQlInID0+IFsncHQtYnInLCAncHQnXSlcclxuICpcclxuICogQHBhcmFtIHtzdHJpbmd9IGxhbmd1YWdlIFRoZSBpbnB1dCBsYW5ndWFnZSBzdHJpbmdcclxuICogQHJldHVybiB7c3RyaW5nW119IExpc3Qgb2Ygbm9ybWFsaXNlZCBsYW5ndWFnZXNcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXROb3JtYWxpemVkTGFuZ3VhZ2VLZXlzKGxhbmd1YWdlKSB7XHJcbiAgICBjb25zdCBsYW5ndWFnZUtleXMgPSBbXTtcclxuICAgIGNvbnN0IG5vcm1hbGl6ZWRMYW5ndWFnZSA9IG5vcm1hbGl6ZUxhbmd1YWdlS2V5KGxhbmd1YWdlKTtcclxuICAgIGNvbnN0IGxhbmd1YWdlUGFydHMgPSBub3JtYWxpemVkTGFuZ3VhZ2Uuc3BsaXQoJy0nKTtcclxuICAgIGlmIChsYW5ndWFnZVBhcnRzLmxlbmd0aCA9PT0gMiAmJiBsYW5ndWFnZVBhcnRzWzBdID09PSBsYW5ndWFnZVBhcnRzWzFdKSB7XHJcbiAgICAgICAgbGFuZ3VhZ2VLZXlzLnB1c2gobGFuZ3VhZ2VQYXJ0c1swXSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxhbmd1YWdlS2V5cy5wdXNoKG5vcm1hbGl6ZWRMYW5ndWFnZSk7XHJcbiAgICAgICAgaWYgKGxhbmd1YWdlUGFydHMubGVuZ3RoID09PSAyKSB7XHJcbiAgICAgICAgICAgIGxhbmd1YWdlS2V5cy5wdXNoKGxhbmd1YWdlUGFydHNbMF0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBsYW5ndWFnZUtleXM7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXR1cm5zIGEgbGFuZ3VhZ2Ugc3RyaW5nIHdpdGggdW5kZXJzY29yZXMgcmVwbGFjZWQgd2l0aFxyXG4gKiBoeXBoZW5zLCBhbmQgbG93ZXJjYXNlZC5cclxuICpcclxuICogQHBhcmFtIHtzdHJpbmd9IGxhbmd1YWdlIFRoZSBsYW5ndWFnZSBzdHJpbmcgdG8gYmUgbm9ybWFsaXplZFxyXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBUaGUgbm9ybWFsaXplZCBsYW5ndWFnZSBzdHJpbmdcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBub3JtYWxpemVMYW5ndWFnZUtleShsYW5ndWFnZSkge1xyXG4gICAgcmV0dXJuIGxhbmd1YWdlLnRvTG93ZXJDYXNlKCkucmVwbGFjZShcIl9cIiwgXCItXCIpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q3VycmVudExhbmd1YWdlKCkge1xyXG4gICAgcmV0dXJuIGNvdW50ZXJwYXJ0LmdldExvY2FsZSgpO1xyXG59XHJcblxyXG4vKipcclxuICogR2l2ZW4gYSBsaXN0IG9mIGxhbmd1YWdlIGNvZGVzLCBwaWNrIHRoZSBtb3N0IGFwcHJvcHJpYXRlIG9uZVxyXG4gKiBnaXZlbiB0aGUgY3VycmVudCBsYW5ndWFnZSAoaWUuIGdldEN1cnJlbnRMYW5ndWFnZSgpKVxyXG4gKiBFbmdsaXNoIGlzIGFzc3VtZWQgdG8gYmUgYSByZWFzb25hYmxlIGRlZmF1bHQuXHJcbiAqXHJcbiAqIEBwYXJhbSB7c3RyaW5nW119IGxhbmdzIExpc3Qgb2YgbGFuZ3VhZ2UgY29kZXMgdG8gcGljayBmcm9tXHJcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBtb3N0IGFwcHJvcHJpYXRlIGxhbmd1YWdlIGNvZGUgZnJvbSBsYW5nc1xyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIHBpY2tCZXN0TGFuZ3VhZ2UobGFuZ3MpIHtcclxuICAgIGNvbnN0IGN1cnJlbnRMYW5nID0gZ2V0Q3VycmVudExhbmd1YWdlKCk7XHJcbiAgICBjb25zdCBub3JtYWxpc2VkTGFuZ3MgPSBsYW5ncy5tYXAobm9ybWFsaXplTGFuZ3VhZ2VLZXkpO1xyXG5cclxuICAgIHtcclxuICAgICAgICAvLyBCZXN0IGlzIGFuIGV4YWN0IG1hdGNoXHJcbiAgICAgICAgY29uc3QgY3VycmVudExhbmdJbmRleCA9IG5vcm1hbGlzZWRMYW5ncy5pbmRleE9mKGN1cnJlbnRMYW5nKTtcclxuICAgICAgICBpZiAoY3VycmVudExhbmdJbmRleCA+IC0xKSByZXR1cm4gbGFuZ3NbY3VycmVudExhbmdJbmRleF07XHJcbiAgICB9XHJcblxyXG4gICAge1xyXG4gICAgICAgIC8vIEZhaWxpbmcgdGhhdCwgYSBkaWZmZXJlbnQgZGlhbGVjdCBvZiB0aGUgc2FtZSBsYW5ndWFnZVxyXG4gICAgICAgIGNvbnN0IGNsb3NlTGFuZ0luZGV4ID0gbm9ybWFsaXNlZExhbmdzLmZpbmQoKGwpID0+IGwuc3Vic3RyKDAsIDIpID09PSBjdXJyZW50TGFuZy5zdWJzdHIoMCwgMikpO1xyXG4gICAgICAgIGlmIChjbG9zZUxhbmdJbmRleCA+IC0xKSByZXR1cm4gbGFuZ3NbY2xvc2VMYW5nSW5kZXhdO1xyXG4gICAgfVxyXG5cclxuICAgIHtcclxuICAgICAgICAvLyBOZWl0aGVyIG9mIHRob3NlPyBUcnkgYW4gZW5nbGlzaCB2YXJpYW50LlxyXG4gICAgICAgIGNvbnN0IGVuSW5kZXggPSBub3JtYWxpc2VkTGFuZ3MuZmluZCgobCkgPT4gbC5zdGFydHNXaXRoKCdlbicpKTtcclxuICAgICAgICBpZiAoZW5JbmRleCA+IC0xKSByZXR1cm4gbGFuZ3NbZW5JbmRleF07XHJcbiAgICB9XHJcblxyXG4gICAgLy8gaWYgbm90aGluZyBlbHNlLCB1c2UgdGhlIGZpcnN0XHJcbiAgICByZXR1cm4gbGFuZ3NbMF07XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldExhbmdzSnNvbigpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgbGV0IHVybDtcclxuICAgICAgICBpZiAodHlwZW9mKHdlYnBhY2tMYW5nSnNvblVybCkgPT09ICdzdHJpbmcnKSB7IC8vIGluIEplc3QgdGhpcyAndXJsJyBpc24ndCBhIFVSTCwgc28ganVzdCBmYWxsIHRocm91Z2hcclxuICAgICAgICAgICAgdXJsID0gd2VicGFja0xhbmdKc29uVXJsO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHVybCA9IGkxOG5Gb2xkZXIgKyAnbGFuZ3VhZ2VzLmpzb24nO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXF1ZXN0KFxyXG4gICAgICAgICAgICB7IG1ldGhvZDogXCJHRVRcIiwgdXJsIH0sXHJcbiAgICAgICAgICAgIChlcnIsIHJlc3BvbnNlLCBib2R5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyIHx8IHJlc3BvbnNlLnN0YXR1cyA8IDIwMCB8fCByZXNwb25zZS5zdGF0dXMgPj0gMzAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KHtlcnI6IGVyciwgcmVzcG9uc2U6IHJlc3BvbnNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZShKU09OLnBhcnNlKGJvZHkpKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHdlYmxhdGVUb0NvdW50ZXJwYXJ0KGluVHJzKSB7XHJcbiAgICBjb25zdCBvdXRUcnMgPSB7fTtcclxuXHJcbiAgICBmb3IgKGNvbnN0IGtleSBvZiBPYmplY3Qua2V5cyhpblRycykpIHtcclxuICAgICAgICBjb25zdCBrZXlQYXJ0cyA9IGtleS5zcGxpdCgnfCcsIDIpO1xyXG4gICAgICAgIGlmIChrZXlQYXJ0cy5sZW5ndGggPT09IDIpIHtcclxuICAgICAgICAgICAgbGV0IG9iaiA9IG91dFRyc1trZXlQYXJ0c1swXV07XHJcbiAgICAgICAgICAgIGlmIChvYmogPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgb2JqID0ge307XHJcbiAgICAgICAgICAgICAgICBvdXRUcnNba2V5UGFydHNbMF1dID0gb2JqO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9ialtrZXlQYXJ0c1sxXV0gPSBpblRyc1trZXldO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG91dFRyc1trZXldID0gaW5UcnNba2V5XTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG91dFRycztcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TGFuZ3VhZ2UobGFuZ1BhdGgpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgcmVxdWVzdChcclxuICAgICAgICAgICAgeyBtZXRob2Q6IFwiR0VUXCIsIHVybDogbGFuZ1BhdGggfSxcclxuICAgICAgICAgICAgKGVyciwgcmVzcG9uc2UsIGJvZHkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlcnIgfHwgcmVzcG9uc2Uuc3RhdHVzIDwgMjAwIHx8IHJlc3BvbnNlLnN0YXR1cyA+PSAzMDApIHtcclxuICAgICAgICAgICAgICAgICAgICByZWplY3Qoe2VycjogZXJyLCByZXNwb25zZTogcmVzcG9uc2V9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHdlYmxhdGVUb0NvdW50ZXJwYXJ0KEpTT04ucGFyc2UoYm9keSkpKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG4gICAgfSk7XHJcbn1cclxuIl19