"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = init;
exports.flush = flush;
exports.cleanup = cleanup;
exports.getLogsForReport = getLogsForReport;

/*
Copyright 2017 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// This module contains all the code needed to log the console, persist it to
// disk and submit bug reports. Rationale is as follows:
//  - Monkey-patching the console is preferable to having a log library because
//    we can catch logs by other libraries more easily, without having to all
//    depend on the same log framework / pass the logger around.
//  - We use IndexedDB to persists logs because it has generous disk space
//    limits compared to local storage. IndexedDB does not work in incognito
//    mode, in which case this module will not be able to write logs to disk.
//    However, the logs will still be stored in-memory, so can still be
//    submitted in a bug report should the user wish to: we can also store more
//    logs in-memory than in local storage, which does work in incognito mode.
//    We also need to handle the case where there are 2+ tabs. Each JS runtime
//    generates a random string which serves as the "ID" for that tab/session.
//    These IDs are stored along with the log lines.
//  - Bug reports are sent as a POST over HTTPS: it purposefully does not use
//    Matrix as bug reports may be made when Matrix is not responsive (which may
//    be the cause of the bug). We send the most recent N MB of UTF-8 log data,
//    starting with the most recent, which we know because the "ID"s are
//    actually timestamps. We then purge the remaining logs. We also do this
//    purge on startup to prevent logs from accumulating.
// the frequency with which we flush to indexeddb
const FLUSH_RATE_MS = 30 * 1000; // the length of log data we keep in indexeddb (and include in the reports)

const MAX_LOG_SIZE = 1024 * 1024 * 1; // 1 MB
// A class which monkey-patches the global console and stores log lines.

class ConsoleLogger {
  constructor() {
    this.logs = "";
  }

  monkeyPatch(consoleObj) {
    // Monkey-patch console logging
    const consoleFunctionsToLevels = {
      log: "I",
      info: "I",
      warn: "W",
      error: "E"
    };
    Object.keys(consoleFunctionsToLevels).forEach(fnName => {
      const level = consoleFunctionsToLevels[fnName];
      const originalFn = consoleObj[fnName].bind(consoleObj);

      consoleObj[fnName] = (...args) => {
        this.log(level, ...args);
        originalFn(...args);
      };
    });
  }

  log(level, ...args) {
    // We don't know what locale the user may be running so use ISO strings
    const ts = new Date().toISOString(); // Convert objects and errors to helpful things

    args = args.map(arg => {
      if (arg instanceof Error) {
        return arg.message + (arg.stack ? "\n".concat(arg.stack) : '');
      } else if (typeof arg === 'object') {
        try {
          return JSON.stringify(arg);
        } catch (e) {
          // In development, it can be useful to log complex cyclic
          // objects to the console for inspection. This is fine for
          // the console, but default `stringify` can't handle that.
          // We workaround this by using a special replacer function
          // to only log values of the root object and avoid cycles.
          return JSON.stringify(arg, (key, value) => {
            if (key && typeof value === "object") {
              return "<object>";
            }

            return value;
          });
        }
      } else {
        return arg;
      }
    }); // Some browsers support string formatting which we're not doing here
    // so the lines are a little more ugly but easy to implement / quick to
    // run.
    // Example line:
    // 2017-01-18T11:23:53.214Z W Failed to set badge count

    let line = "".concat(ts, " ").concat(level, " ").concat(args.join(' '), "\n"); // Do some cleanup

    line = line.replace(/token=[a-zA-Z0-9-]+/gm, 'token=xxxxx'); // Using + really is the quickest way in JS
    // http://jsperf.com/concat-vs-plus-vs-join

    this.logs += line;
  }
  /**
   * Retrieve log lines to flush to disk.
   * @param {boolean} keepLogs True to not delete logs after flushing.
   * @return {string} \n delimited log lines to flush.
   */


  flush(keepLogs) {
    // The ConsoleLogger doesn't care how these end up on disk, it just
    // flushes them to the caller.
    if (keepLogs) {
      return this.logs;
    }

    const logsToFlush = this.logs;
    this.logs = "";
    return logsToFlush;
  }

} // A class which stores log lines in an IndexedDB instance.


class IndexedDBLogStore {
  constructor(indexedDB, logger) {
    this.indexedDB = indexedDB;
    this.logger = logger;
    this.id = "instance-" + Math.random() + Date.now();
    this.index = 0;
    this.db = null; // these promises are cleared as soon as fulfilled

    this.flushPromise = null; // set if flush() is called whilst one is ongoing

    this.flushAgainPromise = null;
  }
  /**
   * @return {Promise} Resolves when the store is ready.
   */


  connect() {
    const req = this.indexedDB.open("logs");
    return new Promise((resolve, reject) => {
      req.onsuccess = event => {
        this.db = event.target.result; // Periodically flush logs to local storage / indexeddb

        setInterval(this.flush.bind(this), FLUSH_RATE_MS);
        resolve();
      };

      req.onerror = event => {
        const err = "Failed to open log database: " + event.target.error.name;
        console.error(err);
        reject(new Error(err));
      }; // First time: Setup the object store


      req.onupgradeneeded = event => {
        const db = event.target.result;
        const logObjStore = db.createObjectStore("logs", {
          keyPath: ["id", "index"]
        }); // Keys in the database look like: [ "instance-148938490", 0 ]
        // Later on we need to query everything based on an instance id.
        // In order to do this, we need to set up indexes "id".

        logObjStore.createIndex("id", "id", {
          unique: false
        });
        logObjStore.add(this._generateLogEntry(new Date() + " ::: Log database was created."));
        const lastModifiedStore = db.createObjectStore("logslastmod", {
          keyPath: "id"
        });
        lastModifiedStore.add(this._generateLastModifiedTime());
      };
    });
  }
  /**
   * Flush logs to disk.
   *
   * There are guards to protect against race conditions in order to ensure
   * that all previous flushes have completed before the most recent flush.
   * Consider without guards:
   *  - A calls flush() periodically.
   *  - B calls flush() and wants to send logs immediately afterwards.
   *  - If B doesn't wait for A's flush to complete, B will be missing the
   *    contents of A's flush.
   * To protect against this, we set 'flushPromise' when a flush is ongoing.
   * Subsequent calls to flush() during this period will chain another flush,
   * then keep returning that same chained flush.
   *
   * This guarantees that we will always eventually do a flush when flush() is
   * called.
   *
   * @return {Promise} Resolved when the logs have been flushed.
   */


  flush() {
    // check if a flush() operation is ongoing
    if (this.flushPromise) {
      if (this.flushAgainPromise) {
        // this is the 3rd+ time we've called flush() : return the same promise.
        return this.flushAgainPromise;
      } // queue up a flush to occur immediately after the pending one completes.


      this.flushAgainPromise = this.flushPromise.then(() => {
        return this.flush();
      }).then(() => {
        this.flushAgainPromise = null;
      });
      return this.flushAgainPromise;
    } // there is no flush promise or there was but it has finished, so do
    // a brand new one, destroying the chain which may have been built up.


    this.flushPromise = new Promise((resolve, reject) => {
      if (!this.db) {
        // not connected yet or user rejected access for us to r/w to the db.
        reject(new Error("No connected database"));
        return;
      }

      const lines = this.logger.flush();

      if (lines.length === 0) {
        resolve();
        return;
      }

      const txn = this.db.transaction(["logs", "logslastmod"], "readwrite");
      const objStore = txn.objectStore("logs");

      txn.oncomplete = event => {
        resolve();
      };

      txn.onerror = event => {
        console.error("Failed to flush logs : ", event);
        reject(new Error("Failed to write logs: " + event.target.errorCode));
      };

      objStore.add(this._generateLogEntry(lines));
      const lastModStore = txn.objectStore("logslastmod");
      lastModStore.put(this._generateLastModifiedTime());
    }).then(() => {
      this.flushPromise = null;
    });
    return this.flushPromise;
  }
  /**
   * Consume the most recent logs and return them. Older logs which are not
   * returned are deleted at the same time, so this can be called at startup
   * to do house-keeping to keep the logs from growing too large.
   *
   * @return {Promise<Object[]>} Resolves to an array of objects. The array is
   * sorted in time (oldest first) based on when the log file was created (the
   * log ID). The objects have said log ID in an "id" field and "lines" which
   * is a big string with all the new-line delimited logs.
   */


  async consume() {
    const db = this.db; // Returns: a string representing the concatenated logs for this ID.
    // Stops adding log fragments when the size exceeds maxSize

    function fetchLogs(id, maxSize) {
      const objectStore = db.transaction("logs", "readonly").objectStore("logs");
      return new Promise((resolve, reject) => {
        const query = objectStore.index("id").openCursor(IDBKeyRange.only(id), 'prev');
        let lines = '';

        query.onerror = event => {
          reject(new Error("Query failed: " + event.target.errorCode));
        };

        query.onsuccess = event => {
          const cursor = event.target.result;

          if (!cursor) {
            resolve(lines);
            return; // end of results
          }

          lines = cursor.value.lines + lines;

          if (lines.length >= maxSize) {
            resolve(lines);
          } else {
            cursor.continue();
          }
        };
      });
    } // Returns: A sorted array of log IDs. (newest first)


    function fetchLogIds() {
      // To gather all the log IDs, query for all records in logslastmod.
      const o = db.transaction("logslastmod", "readonly").objectStore("logslastmod");
      return selectQuery(o, undefined, cursor => {
        return {
          id: cursor.value.id,
          ts: cursor.value.ts
        };
      }).then(res => {
        // Sort IDs by timestamp (newest first)
        return res.sort((a, b) => {
          return b.ts - a.ts;
        }).map(a => a.id);
      });
    }

    function deleteLogs(id) {
      return new Promise((resolve, reject) => {
        const txn = db.transaction(["logs", "logslastmod"], "readwrite");
        const o = txn.objectStore("logs"); // only load the key path, not the data which may be huge

        const query = o.index("id").openKeyCursor(IDBKeyRange.only(id));

        query.onsuccess = event => {
          const cursor = event.target.result;

          if (!cursor) {
            return;
          }

          o.delete(cursor.primaryKey);
          cursor.continue();
        };

        txn.oncomplete = () => {
          resolve();
        };

        txn.onerror = event => {
          reject(new Error("Failed to delete logs for " + "'".concat(id, "' : ").concat(event.target.errorCode)));
        }; // delete last modified entries


        const lastModStore = txn.objectStore("logslastmod");
        lastModStore.delete(id);
      });
    }

    const allLogIds = await fetchLogIds();
    let removeLogIds = [];
    const logs = [];
    let size = 0;

    for (let i = 0; i < allLogIds.length; i++) {
      const lines = await fetchLogs(allLogIds[i], MAX_LOG_SIZE - size); // always add the log file: fetchLogs will truncate once the maxSize we give it is
      // exceeded, so we'll go over the max but only by one fragment's worth.

      logs.push({
        lines: lines,
        id: allLogIds[i]
      });
      size += lines.length; // If fetchLogs truncated we'll now be at or over the size limit,
      // in which case we should stop and remove the rest of the log files.

      if (size >= MAX_LOG_SIZE) {
        // the remaining log IDs should be removed. If we go out of
        // bounds this is just []
        removeLogIds = allLogIds.slice(i + 1);
        break;
      }
    }

    if (removeLogIds.length > 0) {
      console.log("Removing logs: ", removeLogIds); // Don't await this because it's non-fatal if we can't clean up
      // logs.

      Promise.all(removeLogIds.map(id => deleteLogs(id))).then(() => {
        console.log("Removed ".concat(removeLogIds.length, " old logs."));
      }, err => {
        console.error(err);
      });
    }

    return logs;
  }

  _generateLogEntry(lines) {
    return {
      id: this.id,
      lines: lines,
      index: this.index++
    };
  }

  _generateLastModifiedTime() {
    return {
      id: this.id,
      ts: Date.now()
    };
  }

}
/**
 * Helper method to collect results from a Cursor and promiseify it.
 * @param {ObjectStore|Index} store The store to perform openCursor on.
 * @param {IDBKeyRange=} keyRange Optional key range to apply on the cursor.
 * @param {Function} resultMapper A function which is repeatedly called with a
 * Cursor.
 * Return the data you want to keep.
 * @return {Promise<T[]>} Resolves to an array of whatever you returned from
 * resultMapper.
 */


function selectQuery(store, keyRange, resultMapper) {
  const query = store.openCursor(keyRange);
  return new Promise((resolve, reject) => {
    const results = [];

    query.onerror = event => {
      reject(new Error("Query failed: " + event.target.errorCode));
    }; // collect results


    query.onsuccess = event => {
      const cursor = event.target.result;

      if (!cursor) {
        resolve(results);
        return; // end of results
      }

      results.push(resultMapper(cursor));
      cursor.continue();
    };
  });
}
/**
 * Configure rage shaking support for sending bug reports.
 * Modifies globals.
 * @return {Promise} Resolves when set up.
 */


function init() {
  if (global.mx_rage_initPromise) {
    return global.mx_rage_initPromise;
  }

  global.mx_rage_logger = new ConsoleLogger();
  global.mx_rage_logger.monkeyPatch(window.console); // just *accessing* indexedDB throws an exception in firefox with
  // indexeddb disabled.

  let indexedDB;

  try {
    indexedDB = window.indexedDB;
  } catch (e) {}

  if (indexedDB) {
    global.mx_rage_store = new IndexedDBLogStore(indexedDB, global.mx_rage_logger);
    global.mx_rage_initPromise = global.mx_rage_store.connect();
    return global.mx_rage_initPromise;
  }

  global.mx_rage_initPromise = Promise.resolve();
  return global.mx_rage_initPromise;
}

function flush() {
  if (!global.mx_rage_store) {
    return;
  }

  global.mx_rage_store.flush();
}
/**
 * Clean up old logs.
 * @return Promise Resolves if cleaned logs.
 */


async function cleanup() {
  if (!global.mx_rage_store) {
    return;
  }

  await global.mx_rage_store.consume();
}
/**
 * Get a recent snapshot of the logs, ready for attaching to a bug report
 *
 * @return {Array<{lines: string, id, string}>}  list of log data
 */


async function getLogsForReport() {
  if (!global.mx_rage_logger) {
    throw new Error("No console logger, did you forget to call init()?");
  } // If in incognito mode, store is null, but we still want bug report
  // sending to work going off the in-memory console logs.


  if (global.mx_rage_store) {
    // flush most recent logs
    await global.mx_rage_store.flush();
    return await global.mx_rage_store.consume();
  } else {
    return [{
      lines: global.mx_rage_logger.flush(true),
      id: "-"
    }];
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yYWdlc2hha2UvcmFnZXNoYWtlLmpzIl0sIm5hbWVzIjpbIkZMVVNIX1JBVEVfTVMiLCJNQVhfTE9HX1NJWkUiLCJDb25zb2xlTG9nZ2VyIiwiY29uc3RydWN0b3IiLCJsb2dzIiwibW9ua2V5UGF0Y2giLCJjb25zb2xlT2JqIiwiY29uc29sZUZ1bmN0aW9uc1RvTGV2ZWxzIiwibG9nIiwiaW5mbyIsIndhcm4iLCJlcnJvciIsIk9iamVjdCIsImtleXMiLCJmb3JFYWNoIiwiZm5OYW1lIiwibGV2ZWwiLCJvcmlnaW5hbEZuIiwiYmluZCIsImFyZ3MiLCJ0cyIsIkRhdGUiLCJ0b0lTT1N0cmluZyIsIm1hcCIsImFyZyIsIkVycm9yIiwibWVzc2FnZSIsInN0YWNrIiwiSlNPTiIsInN0cmluZ2lmeSIsImUiLCJrZXkiLCJ2YWx1ZSIsImxpbmUiLCJqb2luIiwicmVwbGFjZSIsImZsdXNoIiwia2VlcExvZ3MiLCJsb2dzVG9GbHVzaCIsIkluZGV4ZWREQkxvZ1N0b3JlIiwiaW5kZXhlZERCIiwibG9nZ2VyIiwiaWQiLCJNYXRoIiwicmFuZG9tIiwibm93IiwiaW5kZXgiLCJkYiIsImZsdXNoUHJvbWlzZSIsImZsdXNoQWdhaW5Qcm9taXNlIiwiY29ubmVjdCIsInJlcSIsIm9wZW4iLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsIm9uc3VjY2VzcyIsImV2ZW50IiwidGFyZ2V0IiwicmVzdWx0Iiwic2V0SW50ZXJ2YWwiLCJvbmVycm9yIiwiZXJyIiwibmFtZSIsImNvbnNvbGUiLCJvbnVwZ3JhZGVuZWVkZWQiLCJsb2dPYmpTdG9yZSIsImNyZWF0ZU9iamVjdFN0b3JlIiwia2V5UGF0aCIsImNyZWF0ZUluZGV4IiwidW5pcXVlIiwiYWRkIiwiX2dlbmVyYXRlTG9nRW50cnkiLCJsYXN0TW9kaWZpZWRTdG9yZSIsIl9nZW5lcmF0ZUxhc3RNb2RpZmllZFRpbWUiLCJ0aGVuIiwibGluZXMiLCJsZW5ndGgiLCJ0eG4iLCJ0cmFuc2FjdGlvbiIsIm9ialN0b3JlIiwib2JqZWN0U3RvcmUiLCJvbmNvbXBsZXRlIiwiZXJyb3JDb2RlIiwibGFzdE1vZFN0b3JlIiwicHV0IiwiY29uc3VtZSIsImZldGNoTG9ncyIsIm1heFNpemUiLCJxdWVyeSIsIm9wZW5DdXJzb3IiLCJJREJLZXlSYW5nZSIsIm9ubHkiLCJjdXJzb3IiLCJjb250aW51ZSIsImZldGNoTG9nSWRzIiwibyIsInNlbGVjdFF1ZXJ5IiwidW5kZWZpbmVkIiwicmVzIiwic29ydCIsImEiLCJiIiwiZGVsZXRlTG9ncyIsIm9wZW5LZXlDdXJzb3IiLCJkZWxldGUiLCJwcmltYXJ5S2V5IiwiYWxsTG9nSWRzIiwicmVtb3ZlTG9nSWRzIiwic2l6ZSIsImkiLCJwdXNoIiwic2xpY2UiLCJhbGwiLCJzdG9yZSIsImtleVJhbmdlIiwicmVzdWx0TWFwcGVyIiwicmVzdWx0cyIsImluaXQiLCJnbG9iYWwiLCJteF9yYWdlX2luaXRQcm9taXNlIiwibXhfcmFnZV9sb2dnZXIiLCJ3aW5kb3ciLCJteF9yYWdlX3N0b3JlIiwiY2xlYW51cCIsImdldExvZ3NGb3JSZXBvcnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0EsTUFBTUEsYUFBYSxHQUFHLEtBQUssSUFBM0IsQyxDQUVBOztBQUNBLE1BQU1DLFlBQVksR0FBRyxPQUFPLElBQVAsR0FBYyxDQUFuQyxDLENBQXNDO0FBRXRDOztBQUNBLE1BQU1DLGFBQU4sQ0FBb0I7QUFDaEJDLEVBQUFBLFdBQVcsR0FBRztBQUNWLFNBQUtDLElBQUwsR0FBWSxFQUFaO0FBQ0g7O0FBRURDLEVBQUFBLFdBQVcsQ0FBQ0MsVUFBRCxFQUFhO0FBQ3BCO0FBQ0EsVUFBTUMsd0JBQXdCLEdBQUc7QUFDN0JDLE1BQUFBLEdBQUcsRUFBRSxHQUR3QjtBQUU3QkMsTUFBQUEsSUFBSSxFQUFFLEdBRnVCO0FBRzdCQyxNQUFBQSxJQUFJLEVBQUUsR0FIdUI7QUFJN0JDLE1BQUFBLEtBQUssRUFBRTtBQUpzQixLQUFqQztBQU1BQyxJQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWU4sd0JBQVosRUFBc0NPLE9BQXRDLENBQStDQyxNQUFELElBQVk7QUFDdEQsWUFBTUMsS0FBSyxHQUFHVCx3QkFBd0IsQ0FBQ1EsTUFBRCxDQUF0QztBQUNBLFlBQU1FLFVBQVUsR0FBR1gsVUFBVSxDQUFDUyxNQUFELENBQVYsQ0FBbUJHLElBQW5CLENBQXdCWixVQUF4QixDQUFuQjs7QUFDQUEsTUFBQUEsVUFBVSxDQUFDUyxNQUFELENBQVYsR0FBcUIsQ0FBQyxHQUFHSSxJQUFKLEtBQWE7QUFDOUIsYUFBS1gsR0FBTCxDQUFTUSxLQUFULEVBQWdCLEdBQUdHLElBQW5CO0FBQ0FGLFFBQUFBLFVBQVUsQ0FBQyxHQUFHRSxJQUFKLENBQVY7QUFDSCxPQUhEO0FBSUgsS0FQRDtBQVFIOztBQUVEWCxFQUFBQSxHQUFHLENBQUNRLEtBQUQsRUFBUSxHQUFHRyxJQUFYLEVBQWlCO0FBQ2hCO0FBQ0EsVUFBTUMsRUFBRSxHQUFHLElBQUlDLElBQUosR0FBV0MsV0FBWCxFQUFYLENBRmdCLENBSWhCOztBQUNBSCxJQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ0ksR0FBTCxDQUFVQyxHQUFELElBQVM7QUFDckIsVUFBSUEsR0FBRyxZQUFZQyxLQUFuQixFQUEwQjtBQUN0QixlQUFPRCxHQUFHLENBQUNFLE9BQUosSUFBZUYsR0FBRyxDQUFDRyxLQUFKLGVBQWlCSCxHQUFHLENBQUNHLEtBQXJCLElBQStCLEVBQTlDLENBQVA7QUFDSCxPQUZELE1BRU8sSUFBSSxPQUFRSCxHQUFSLEtBQWlCLFFBQXJCLEVBQStCO0FBQ2xDLFlBQUk7QUFDQSxpQkFBT0ksSUFBSSxDQUFDQyxTQUFMLENBQWVMLEdBQWYsQ0FBUDtBQUNILFNBRkQsQ0FFRSxPQUFPTSxDQUFQLEVBQVU7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQU9GLElBQUksQ0FBQ0MsU0FBTCxDQUFlTCxHQUFmLEVBQW9CLENBQUNPLEdBQUQsRUFBTUMsS0FBTixLQUFnQjtBQUN2QyxnQkFBSUQsR0FBRyxJQUFJLE9BQU9DLEtBQVAsS0FBaUIsUUFBNUIsRUFBc0M7QUFDbEMscUJBQU8sVUFBUDtBQUNIOztBQUNELG1CQUFPQSxLQUFQO0FBQ0gsV0FMTSxDQUFQO0FBTUg7QUFDSixPQWhCTSxNQWdCQTtBQUNILGVBQU9SLEdBQVA7QUFDSDtBQUNKLEtBdEJNLENBQVAsQ0FMZ0IsQ0E2QmhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSVMsSUFBSSxhQUFNYixFQUFOLGNBQVlKLEtBQVosY0FBcUJHLElBQUksQ0FBQ2UsSUFBTCxDQUFVLEdBQVYsQ0FBckIsT0FBUixDQWxDZ0IsQ0FtQ2hCOztBQUNBRCxJQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ0UsT0FBTCxDQUFhLHVCQUFiLEVBQXNDLGFBQXRDLENBQVAsQ0FwQ2dCLENBcUNoQjtBQUNBOztBQUNBLFNBQUsvQixJQUFMLElBQWE2QixJQUFiO0FBQ0g7QUFFRDs7Ozs7OztBQUtBRyxFQUFBQSxLQUFLLENBQUNDLFFBQUQsRUFBVztBQUNaO0FBQ0E7QUFDQSxRQUFJQSxRQUFKLEVBQWM7QUFDVixhQUFPLEtBQUtqQyxJQUFaO0FBQ0g7O0FBQ0QsVUFBTWtDLFdBQVcsR0FBRyxLQUFLbEMsSUFBekI7QUFDQSxTQUFLQSxJQUFMLEdBQVksRUFBWjtBQUNBLFdBQU9rQyxXQUFQO0FBQ0g7O0FBL0VlLEMsQ0FrRnBCOzs7QUFDQSxNQUFNQyxpQkFBTixDQUF3QjtBQUNwQnBDLEVBQUFBLFdBQVcsQ0FBQ3FDLFNBQUQsRUFBWUMsTUFBWixFQUFvQjtBQUMzQixTQUFLRCxTQUFMLEdBQWlCQSxTQUFqQjtBQUNBLFNBQUtDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFNBQUtDLEVBQUwsR0FBVSxjQUFjQyxJQUFJLENBQUNDLE1BQUwsRUFBZCxHQUE4QnZCLElBQUksQ0FBQ3dCLEdBQUwsRUFBeEM7QUFDQSxTQUFLQyxLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUtDLEVBQUwsR0FBVSxJQUFWLENBTDJCLENBTzNCOztBQUNBLFNBQUtDLFlBQUwsR0FBb0IsSUFBcEIsQ0FSMkIsQ0FTM0I7O0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsSUFBekI7QUFDSDtBQUVEOzs7OztBQUdBQyxFQUFBQSxPQUFPLEdBQUc7QUFDTixVQUFNQyxHQUFHLEdBQUcsS0FBS1gsU0FBTCxDQUFlWSxJQUFmLENBQW9CLE1BQXBCLENBQVo7QUFDQSxXQUFPLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDcENKLE1BQUFBLEdBQUcsQ0FBQ0ssU0FBSixHQUFpQkMsS0FBRCxJQUFXO0FBQ3ZCLGFBQUtWLEVBQUwsR0FBVVUsS0FBSyxDQUFDQyxNQUFOLENBQWFDLE1BQXZCLENBRHVCLENBRXZCOztBQUNBQyxRQUFBQSxXQUFXLENBQUMsS0FBS3hCLEtBQUwsQ0FBV2xCLElBQVgsQ0FBZ0IsSUFBaEIsQ0FBRCxFQUF3QmxCLGFBQXhCLENBQVg7QUFDQXNELFFBQUFBLE9BQU87QUFDVixPQUxEOztBQU9BSCxNQUFBQSxHQUFHLENBQUNVLE9BQUosR0FBZUosS0FBRCxJQUFXO0FBQ3JCLGNBQU1LLEdBQUcsR0FDTCxrQ0FBa0NMLEtBQUssQ0FBQ0MsTUFBTixDQUFhL0MsS0FBYixDQUFtQm9ELElBRHpEO0FBR0FDLFFBQUFBLE9BQU8sQ0FBQ3JELEtBQVIsQ0FBY21ELEdBQWQ7QUFDQVAsUUFBQUEsTUFBTSxDQUFDLElBQUk5QixLQUFKLENBQVVxQyxHQUFWLENBQUQsQ0FBTjtBQUNILE9BTkQsQ0FSb0MsQ0FnQnBDOzs7QUFDQVgsTUFBQUEsR0FBRyxDQUFDYyxlQUFKLEdBQXVCUixLQUFELElBQVc7QUFDN0IsY0FBTVYsRUFBRSxHQUFHVSxLQUFLLENBQUNDLE1BQU4sQ0FBYUMsTUFBeEI7QUFDQSxjQUFNTyxXQUFXLEdBQUduQixFQUFFLENBQUNvQixpQkFBSCxDQUFxQixNQUFyQixFQUE2QjtBQUM3Q0MsVUFBQUEsT0FBTyxFQUFFLENBQUMsSUFBRCxFQUFPLE9BQVA7QUFEb0MsU0FBN0IsQ0FBcEIsQ0FGNkIsQ0FLN0I7QUFDQTtBQUNBOztBQUNBRixRQUFBQSxXQUFXLENBQUNHLFdBQVosQ0FBd0IsSUFBeEIsRUFBOEIsSUFBOUIsRUFBb0M7QUFBRUMsVUFBQUEsTUFBTSxFQUFFO0FBQVYsU0FBcEM7QUFFQUosUUFBQUEsV0FBVyxDQUFDSyxHQUFaLENBQ0ksS0FBS0MsaUJBQUwsQ0FDSSxJQUFJbkQsSUFBSixLQUFhLGdDQURqQixDQURKO0FBTUEsY0FBTW9ELGlCQUFpQixHQUFHMUIsRUFBRSxDQUFDb0IsaUJBQUgsQ0FBcUIsYUFBckIsRUFBb0M7QUFDMURDLFVBQUFBLE9BQU8sRUFBRTtBQURpRCxTQUFwQyxDQUExQjtBQUdBSyxRQUFBQSxpQkFBaUIsQ0FBQ0YsR0FBbEIsQ0FBc0IsS0FBS0cseUJBQUwsRUFBdEI7QUFDSCxPQXBCRDtBQXFCSCxLQXRDTSxDQUFQO0FBdUNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQXRDLEVBQUFBLEtBQUssR0FBRztBQUNKO0FBQ0EsUUFBSSxLQUFLWSxZQUFULEVBQXVCO0FBQ25CLFVBQUksS0FBS0MsaUJBQVQsRUFBNEI7QUFDeEI7QUFDQSxlQUFPLEtBQUtBLGlCQUFaO0FBQ0gsT0FKa0IsQ0FLbkI7OztBQUNBLFdBQUtBLGlCQUFMLEdBQXlCLEtBQUtELFlBQUwsQ0FBa0IyQixJQUFsQixDQUF1QixNQUFNO0FBQ2xELGVBQU8sS0FBS3ZDLEtBQUwsRUFBUDtBQUNILE9BRndCLEVBRXRCdUMsSUFGc0IsQ0FFakIsTUFBTTtBQUNWLGFBQUsxQixpQkFBTCxHQUF5QixJQUF6QjtBQUNILE9BSndCLENBQXpCO0FBS0EsYUFBTyxLQUFLQSxpQkFBWjtBQUNILEtBZEcsQ0FlSjtBQUNBOzs7QUFDQSxTQUFLRCxZQUFMLEdBQW9CLElBQUlLLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDakQsVUFBSSxDQUFDLEtBQUtSLEVBQVYsRUFBYztBQUNWO0FBQ0FRLFFBQUFBLE1BQU0sQ0FBQyxJQUFJOUIsS0FBSixDQUFVLHVCQUFWLENBQUQsQ0FBTjtBQUNBO0FBQ0g7O0FBQ0QsWUFBTW1ELEtBQUssR0FBRyxLQUFLbkMsTUFBTCxDQUFZTCxLQUFaLEVBQWQ7O0FBQ0EsVUFBSXdDLEtBQUssQ0FBQ0MsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUNwQnZCLFFBQUFBLE9BQU87QUFDUDtBQUNIOztBQUNELFlBQU13QixHQUFHLEdBQUcsS0FBSy9CLEVBQUwsQ0FBUWdDLFdBQVIsQ0FBb0IsQ0FBQyxNQUFELEVBQVMsYUFBVCxDQUFwQixFQUE2QyxXQUE3QyxDQUFaO0FBQ0EsWUFBTUMsUUFBUSxHQUFHRixHQUFHLENBQUNHLFdBQUosQ0FBZ0IsTUFBaEIsQ0FBakI7O0FBQ0FILE1BQUFBLEdBQUcsQ0FBQ0ksVUFBSixHQUFrQnpCLEtBQUQsSUFBVztBQUN4QkgsUUFBQUEsT0FBTztBQUNWLE9BRkQ7O0FBR0F3QixNQUFBQSxHQUFHLENBQUNqQixPQUFKLEdBQWVKLEtBQUQsSUFBVztBQUNyQk8sUUFBQUEsT0FBTyxDQUFDckQsS0FBUixDQUNJLHlCQURKLEVBQytCOEMsS0FEL0I7QUFHQUYsUUFBQUEsTUFBTSxDQUNGLElBQUk5QixLQUFKLENBQVUsMkJBQTJCZ0MsS0FBSyxDQUFDQyxNQUFOLENBQWF5QixTQUFsRCxDQURFLENBQU47QUFHSCxPQVBEOztBQVFBSCxNQUFBQSxRQUFRLENBQUNULEdBQVQsQ0FBYSxLQUFLQyxpQkFBTCxDQUF1QkksS0FBdkIsQ0FBYjtBQUNBLFlBQU1RLFlBQVksR0FBR04sR0FBRyxDQUFDRyxXQUFKLENBQWdCLGFBQWhCLENBQXJCO0FBQ0FHLE1BQUFBLFlBQVksQ0FBQ0MsR0FBYixDQUFpQixLQUFLWCx5QkFBTCxFQUFqQjtBQUNILEtBM0JtQixFQTJCakJDLElBM0JpQixDQTJCWixNQUFNO0FBQ1YsV0FBSzNCLFlBQUwsR0FBb0IsSUFBcEI7QUFDSCxLQTdCbUIsQ0FBcEI7QUE4QkEsV0FBTyxLQUFLQSxZQUFaO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7O0FBVUEsUUFBTXNDLE9BQU4sR0FBZ0I7QUFDWixVQUFNdkMsRUFBRSxHQUFHLEtBQUtBLEVBQWhCLENBRFksQ0FHWjtBQUNBOztBQUNBLGFBQVN3QyxTQUFULENBQW1CN0MsRUFBbkIsRUFBdUI4QyxPQUF2QixFQUFnQztBQUM1QixZQUFNUCxXQUFXLEdBQUdsQyxFQUFFLENBQUNnQyxXQUFILENBQWUsTUFBZixFQUF1QixVQUF2QixFQUFtQ0UsV0FBbkMsQ0FBK0MsTUFBL0MsQ0FBcEI7QUFFQSxhQUFPLElBQUk1QixPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQ3BDLGNBQU1rQyxLQUFLLEdBQUdSLFdBQVcsQ0FBQ25DLEtBQVosQ0FBa0IsSUFBbEIsRUFBd0I0QyxVQUF4QixDQUFtQ0MsV0FBVyxDQUFDQyxJQUFaLENBQWlCbEQsRUFBakIsQ0FBbkMsRUFBeUQsTUFBekQsQ0FBZDtBQUNBLFlBQUlrQyxLQUFLLEdBQUcsRUFBWjs7QUFDQWEsUUFBQUEsS0FBSyxDQUFDNUIsT0FBTixHQUFpQkosS0FBRCxJQUFXO0FBQ3ZCRixVQUFBQSxNQUFNLENBQUMsSUFBSTlCLEtBQUosQ0FBVSxtQkFBbUJnQyxLQUFLLENBQUNDLE1BQU4sQ0FBYXlCLFNBQTFDLENBQUQsQ0FBTjtBQUNILFNBRkQ7O0FBR0FNLFFBQUFBLEtBQUssQ0FBQ2pDLFNBQU4sR0FBbUJDLEtBQUQsSUFBVztBQUN6QixnQkFBTW9DLE1BQU0sR0FBR3BDLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxNQUE1Qjs7QUFDQSxjQUFJLENBQUNrQyxNQUFMLEVBQWE7QUFDVHZDLFlBQUFBLE9BQU8sQ0FBQ3NCLEtBQUQsQ0FBUDtBQUNBLG1CQUZTLENBRUQ7QUFDWDs7QUFDREEsVUFBQUEsS0FBSyxHQUFHaUIsTUFBTSxDQUFDN0QsS0FBUCxDQUFhNEMsS0FBYixHQUFxQkEsS0FBN0I7O0FBQ0EsY0FBSUEsS0FBSyxDQUFDQyxNQUFOLElBQWdCVyxPQUFwQixFQUE2QjtBQUN6QmxDLFlBQUFBLE9BQU8sQ0FBQ3NCLEtBQUQsQ0FBUDtBQUNILFdBRkQsTUFFTztBQUNIaUIsWUFBQUEsTUFBTSxDQUFDQyxRQUFQO0FBQ0g7QUFDSixTQVpEO0FBYUgsT0FuQk0sQ0FBUDtBQW9CSCxLQTVCVyxDQThCWjs7O0FBQ0EsYUFBU0MsV0FBVCxHQUF1QjtBQUNuQjtBQUNBLFlBQU1DLENBQUMsR0FBR2pELEVBQUUsQ0FBQ2dDLFdBQUgsQ0FBZSxhQUFmLEVBQThCLFVBQTlCLEVBQTBDRSxXQUExQyxDQUNOLGFBRE0sQ0FBVjtBQUdBLGFBQU9nQixXQUFXLENBQUNELENBQUQsRUFBSUUsU0FBSixFQUFnQkwsTUFBRCxJQUFZO0FBQ3pDLGVBQU87QUFDSG5ELFVBQUFBLEVBQUUsRUFBRW1ELE1BQU0sQ0FBQzdELEtBQVAsQ0FBYVUsRUFEZDtBQUVIdEIsVUFBQUEsRUFBRSxFQUFFeUUsTUFBTSxDQUFDN0QsS0FBUCxDQUFhWjtBQUZkLFNBQVA7QUFJSCxPQUxpQixDQUFYLENBS0p1RCxJQUxJLENBS0V3QixHQUFELElBQVM7QUFDYjtBQUNBLGVBQU9BLEdBQUcsQ0FBQ0MsSUFBSixDQUFTLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVO0FBQ3RCLGlCQUFPQSxDQUFDLENBQUNsRixFQUFGLEdBQU9pRixDQUFDLENBQUNqRixFQUFoQjtBQUNILFNBRk0sRUFFSkcsR0FGSSxDQUVDOEUsQ0FBRCxJQUFPQSxDQUFDLENBQUMzRCxFQUZULENBQVA7QUFHSCxPQVZNLENBQVA7QUFXSDs7QUFFRCxhQUFTNkQsVUFBVCxDQUFvQjdELEVBQXBCLEVBQXdCO0FBQ3BCLGFBQU8sSUFBSVcsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUNwQyxjQUFNdUIsR0FBRyxHQUFHL0IsRUFBRSxDQUFDZ0MsV0FBSCxDQUNSLENBQUMsTUFBRCxFQUFTLGFBQVQsQ0FEUSxFQUNpQixXQURqQixDQUFaO0FBR0EsY0FBTWlCLENBQUMsR0FBR2xCLEdBQUcsQ0FBQ0csV0FBSixDQUFnQixNQUFoQixDQUFWLENBSm9DLENBS3BDOztBQUNBLGNBQU1RLEtBQUssR0FBR08sQ0FBQyxDQUFDbEQsS0FBRixDQUFRLElBQVIsRUFBYzBELGFBQWQsQ0FBNEJiLFdBQVcsQ0FBQ0MsSUFBWixDQUFpQmxELEVBQWpCLENBQTVCLENBQWQ7O0FBQ0ErQyxRQUFBQSxLQUFLLENBQUNqQyxTQUFOLEdBQW1CQyxLQUFELElBQVc7QUFDekIsZ0JBQU1vQyxNQUFNLEdBQUdwQyxLQUFLLENBQUNDLE1BQU4sQ0FBYUMsTUFBNUI7O0FBQ0EsY0FBSSxDQUFDa0MsTUFBTCxFQUFhO0FBQ1Q7QUFDSDs7QUFDREcsVUFBQUEsQ0FBQyxDQUFDUyxNQUFGLENBQVNaLE1BQU0sQ0FBQ2EsVUFBaEI7QUFDQWIsVUFBQUEsTUFBTSxDQUFDQyxRQUFQO0FBQ0gsU0FQRDs7QUFRQWhCLFFBQUFBLEdBQUcsQ0FBQ0ksVUFBSixHQUFpQixNQUFNO0FBQ25CNUIsVUFBQUEsT0FBTztBQUNWLFNBRkQ7O0FBR0F3QixRQUFBQSxHQUFHLENBQUNqQixPQUFKLEdBQWVKLEtBQUQsSUFBVztBQUNyQkYsVUFBQUEsTUFBTSxDQUNGLElBQUk5QixLQUFKLENBQ0ksMENBQ0lpQixFQURKLGlCQUNhZSxLQUFLLENBQUNDLE1BQU4sQ0FBYXlCLFNBRDFCLENBREosQ0FERSxDQUFOO0FBTUgsU0FQRCxDQWxCb0MsQ0EwQnBDOzs7QUFDQSxjQUFNQyxZQUFZLEdBQUdOLEdBQUcsQ0FBQ0csV0FBSixDQUFnQixhQUFoQixDQUFyQjtBQUNBRyxRQUFBQSxZQUFZLENBQUNxQixNQUFiLENBQW9CL0QsRUFBcEI7QUFDSCxPQTdCTSxDQUFQO0FBOEJIOztBQUVELFVBQU1pRSxTQUFTLEdBQUcsTUFBTVosV0FBVyxFQUFuQztBQUNBLFFBQUlhLFlBQVksR0FBRyxFQUFuQjtBQUNBLFVBQU14RyxJQUFJLEdBQUcsRUFBYjtBQUNBLFFBQUl5RyxJQUFJLEdBQUcsQ0FBWDs7QUFDQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdILFNBQVMsQ0FBQzlCLE1BQTlCLEVBQXNDaUMsQ0FBQyxFQUF2QyxFQUEyQztBQUN2QyxZQUFNbEMsS0FBSyxHQUFHLE1BQU1XLFNBQVMsQ0FBQ29CLFNBQVMsQ0FBQ0csQ0FBRCxDQUFWLEVBQWU3RyxZQUFZLEdBQUc0RyxJQUE5QixDQUE3QixDQUR1QyxDQUd2QztBQUNBOztBQUNBekcsTUFBQUEsSUFBSSxDQUFDMkcsSUFBTCxDQUFVO0FBQ05uQyxRQUFBQSxLQUFLLEVBQUVBLEtBREQ7QUFFTmxDLFFBQUFBLEVBQUUsRUFBRWlFLFNBQVMsQ0FBQ0csQ0FBRDtBQUZQLE9BQVY7QUFJQUQsTUFBQUEsSUFBSSxJQUFJakMsS0FBSyxDQUFDQyxNQUFkLENBVHVDLENBV3ZDO0FBQ0E7O0FBQ0EsVUFBSWdDLElBQUksSUFBSTVHLFlBQVosRUFBMEI7QUFDdEI7QUFDQTtBQUNBMkcsUUFBQUEsWUFBWSxHQUFHRCxTQUFTLENBQUNLLEtBQVYsQ0FBZ0JGLENBQUMsR0FBRyxDQUFwQixDQUFmO0FBQ0E7QUFDSDtBQUVKOztBQUNELFFBQUlGLFlBQVksQ0FBQy9CLE1BQWIsR0FBc0IsQ0FBMUIsRUFBNkI7QUFDekJiLE1BQUFBLE9BQU8sQ0FBQ3hELEdBQVIsQ0FBWSxpQkFBWixFQUErQm9HLFlBQS9CLEVBRHlCLENBRXpCO0FBQ0E7O0FBQ0F2RCxNQUFBQSxPQUFPLENBQUM0RCxHQUFSLENBQVlMLFlBQVksQ0FBQ3JGLEdBQWIsQ0FBa0JtQixFQUFELElBQVE2RCxVQUFVLENBQUM3RCxFQUFELENBQW5DLENBQVosRUFBc0RpQyxJQUF0RCxDQUEyRCxNQUFNO0FBQzdEWCxRQUFBQSxPQUFPLENBQUN4RCxHQUFSLG1CQUF1Qm9HLFlBQVksQ0FBQy9CLE1BQXBDO0FBQ0gsT0FGRCxFQUVJZixHQUFELElBQVM7QUFDUkUsUUFBQUEsT0FBTyxDQUFDckQsS0FBUixDQUFjbUQsR0FBZDtBQUNILE9BSkQ7QUFLSDs7QUFDRCxXQUFPMUQsSUFBUDtBQUNIOztBQUVEb0UsRUFBQUEsaUJBQWlCLENBQUNJLEtBQUQsRUFBUTtBQUNyQixXQUFPO0FBQ0hsQyxNQUFBQSxFQUFFLEVBQUUsS0FBS0EsRUFETjtBQUVIa0MsTUFBQUEsS0FBSyxFQUFFQSxLQUZKO0FBR0g5QixNQUFBQSxLQUFLLEVBQUUsS0FBS0EsS0FBTDtBQUhKLEtBQVA7QUFLSDs7QUFFRDRCLEVBQUFBLHlCQUF5QixHQUFHO0FBQ3hCLFdBQU87QUFDSGhDLE1BQUFBLEVBQUUsRUFBRSxLQUFLQSxFQUROO0FBRUh0QixNQUFBQSxFQUFFLEVBQUVDLElBQUksQ0FBQ3dCLEdBQUw7QUFGRCxLQUFQO0FBSUg7O0FBaFJtQjtBQW1SeEI7Ozs7Ozs7Ozs7OztBQVVBLFNBQVNvRCxXQUFULENBQXFCaUIsS0FBckIsRUFBNEJDLFFBQTVCLEVBQXNDQyxZQUF0QyxFQUFvRDtBQUNoRCxRQUFNM0IsS0FBSyxHQUFHeUIsS0FBSyxDQUFDeEIsVUFBTixDQUFpQnlCLFFBQWpCLENBQWQ7QUFDQSxTQUFPLElBQUk5RCxPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQ3BDLFVBQU04RCxPQUFPLEdBQUcsRUFBaEI7O0FBQ0E1QixJQUFBQSxLQUFLLENBQUM1QixPQUFOLEdBQWlCSixLQUFELElBQVc7QUFDdkJGLE1BQUFBLE1BQU0sQ0FBQyxJQUFJOUIsS0FBSixDQUFVLG1CQUFtQmdDLEtBQUssQ0FBQ0MsTUFBTixDQUFheUIsU0FBMUMsQ0FBRCxDQUFOO0FBQ0gsS0FGRCxDQUZvQyxDQUtwQzs7O0FBQ0FNLElBQUFBLEtBQUssQ0FBQ2pDLFNBQU4sR0FBbUJDLEtBQUQsSUFBVztBQUN6QixZQUFNb0MsTUFBTSxHQUFHcEMsS0FBSyxDQUFDQyxNQUFOLENBQWFDLE1BQTVCOztBQUNBLFVBQUksQ0FBQ2tDLE1BQUwsRUFBYTtBQUNUdkMsUUFBQUEsT0FBTyxDQUFDK0QsT0FBRCxDQUFQO0FBQ0EsZUFGUyxDQUVEO0FBQ1g7O0FBQ0RBLE1BQUFBLE9BQU8sQ0FBQ04sSUFBUixDQUFhSyxZQUFZLENBQUN2QixNQUFELENBQXpCO0FBQ0FBLE1BQUFBLE1BQU0sQ0FBQ0MsUUFBUDtBQUNILEtBUkQ7QUFTSCxHQWZNLENBQVA7QUFnQkg7QUFFRDs7Ozs7OztBQUtPLFNBQVN3QixJQUFULEdBQWdCO0FBQ25CLE1BQUlDLE1BQU0sQ0FBQ0MsbUJBQVgsRUFBZ0M7QUFDNUIsV0FBT0QsTUFBTSxDQUFDQyxtQkFBZDtBQUNIOztBQUNERCxFQUFBQSxNQUFNLENBQUNFLGNBQVAsR0FBd0IsSUFBSXZILGFBQUosRUFBeEI7QUFDQXFILEVBQUFBLE1BQU0sQ0FBQ0UsY0FBUCxDQUFzQnBILFdBQXRCLENBQWtDcUgsTUFBTSxDQUFDMUQsT0FBekMsRUFMbUIsQ0FPbkI7QUFDQTs7QUFDQSxNQUFJeEIsU0FBSjs7QUFDQSxNQUFJO0FBQ0FBLElBQUFBLFNBQVMsR0FBR2tGLE1BQU0sQ0FBQ2xGLFNBQW5CO0FBQ0gsR0FGRCxDQUVFLE9BQU9WLENBQVAsRUFBVSxDQUFFOztBQUVkLE1BQUlVLFNBQUosRUFBZTtBQUNYK0UsSUFBQUEsTUFBTSxDQUFDSSxhQUFQLEdBQXVCLElBQUlwRixpQkFBSixDQUFzQkMsU0FBdEIsRUFBaUMrRSxNQUFNLENBQUNFLGNBQXhDLENBQXZCO0FBQ0FGLElBQUFBLE1BQU0sQ0FBQ0MsbUJBQVAsR0FBNkJELE1BQU0sQ0FBQ0ksYUFBUCxDQUFxQnpFLE9BQXJCLEVBQTdCO0FBQ0EsV0FBT3FFLE1BQU0sQ0FBQ0MsbUJBQWQ7QUFDSDs7QUFDREQsRUFBQUEsTUFBTSxDQUFDQyxtQkFBUCxHQUE2Qm5FLE9BQU8sQ0FBQ0MsT0FBUixFQUE3QjtBQUNBLFNBQU9pRSxNQUFNLENBQUNDLG1CQUFkO0FBQ0g7O0FBRU0sU0FBU3BGLEtBQVQsR0FBaUI7QUFDcEIsTUFBSSxDQUFDbUYsTUFBTSxDQUFDSSxhQUFaLEVBQTJCO0FBQ3ZCO0FBQ0g7O0FBQ0RKLEVBQUFBLE1BQU0sQ0FBQ0ksYUFBUCxDQUFxQnZGLEtBQXJCO0FBQ0g7QUFFRDs7Ozs7O0FBSU8sZUFBZXdGLE9BQWYsR0FBeUI7QUFDNUIsTUFBSSxDQUFDTCxNQUFNLENBQUNJLGFBQVosRUFBMkI7QUFDdkI7QUFDSDs7QUFDRCxRQUFNSixNQUFNLENBQUNJLGFBQVAsQ0FBcUJyQyxPQUFyQixFQUFOO0FBQ0g7QUFFRDs7Ozs7OztBQUtPLGVBQWV1QyxnQkFBZixHQUFrQztBQUNyQyxNQUFJLENBQUNOLE1BQU0sQ0FBQ0UsY0FBWixFQUE0QjtBQUN4QixVQUFNLElBQUloRyxLQUFKLENBQ0YsbURBREUsQ0FBTjtBQUdILEdBTG9DLENBTXJDO0FBQ0E7OztBQUNBLE1BQUk4RixNQUFNLENBQUNJLGFBQVgsRUFBMEI7QUFDdEI7QUFDQSxVQUFNSixNQUFNLENBQUNJLGFBQVAsQ0FBcUJ2RixLQUFyQixFQUFOO0FBQ0EsV0FBTyxNQUFNbUYsTUFBTSxDQUFDSSxhQUFQLENBQXFCckMsT0FBckIsRUFBYjtBQUNILEdBSkQsTUFJTztBQUNILFdBQU8sQ0FBQztBQUNKVixNQUFBQSxLQUFLLEVBQUUyQyxNQUFNLENBQUNFLGNBQVAsQ0FBc0JyRixLQUF0QixDQUE0QixJQUE1QixDQURIO0FBRUpNLE1BQUFBLEVBQUUsRUFBRTtBQUZBLEtBQUQsQ0FBUDtBQUlIO0FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbi8vIFRoaXMgbW9kdWxlIGNvbnRhaW5zIGFsbCB0aGUgY29kZSBuZWVkZWQgdG8gbG9nIHRoZSBjb25zb2xlLCBwZXJzaXN0IGl0IHRvXHJcbi8vIGRpc2sgYW5kIHN1Ym1pdCBidWcgcmVwb3J0cy4gUmF0aW9uYWxlIGlzIGFzIGZvbGxvd3M6XHJcbi8vICAtIE1vbmtleS1wYXRjaGluZyB0aGUgY29uc29sZSBpcyBwcmVmZXJhYmxlIHRvIGhhdmluZyBhIGxvZyBsaWJyYXJ5IGJlY2F1c2VcclxuLy8gICAgd2UgY2FuIGNhdGNoIGxvZ3MgYnkgb3RoZXIgbGlicmFyaWVzIG1vcmUgZWFzaWx5LCB3aXRob3V0IGhhdmluZyB0byBhbGxcclxuLy8gICAgZGVwZW5kIG9uIHRoZSBzYW1lIGxvZyBmcmFtZXdvcmsgLyBwYXNzIHRoZSBsb2dnZXIgYXJvdW5kLlxyXG4vLyAgLSBXZSB1c2UgSW5kZXhlZERCIHRvIHBlcnNpc3RzIGxvZ3MgYmVjYXVzZSBpdCBoYXMgZ2VuZXJvdXMgZGlzayBzcGFjZVxyXG4vLyAgICBsaW1pdHMgY29tcGFyZWQgdG8gbG9jYWwgc3RvcmFnZS4gSW5kZXhlZERCIGRvZXMgbm90IHdvcmsgaW4gaW5jb2duaXRvXHJcbi8vICAgIG1vZGUsIGluIHdoaWNoIGNhc2UgdGhpcyBtb2R1bGUgd2lsbCBub3QgYmUgYWJsZSB0byB3cml0ZSBsb2dzIHRvIGRpc2suXHJcbi8vICAgIEhvd2V2ZXIsIHRoZSBsb2dzIHdpbGwgc3RpbGwgYmUgc3RvcmVkIGluLW1lbW9yeSwgc28gY2FuIHN0aWxsIGJlXHJcbi8vICAgIHN1Ym1pdHRlZCBpbiBhIGJ1ZyByZXBvcnQgc2hvdWxkIHRoZSB1c2VyIHdpc2ggdG86IHdlIGNhbiBhbHNvIHN0b3JlIG1vcmVcclxuLy8gICAgbG9ncyBpbi1tZW1vcnkgdGhhbiBpbiBsb2NhbCBzdG9yYWdlLCB3aGljaCBkb2VzIHdvcmsgaW4gaW5jb2duaXRvIG1vZGUuXHJcbi8vICAgIFdlIGFsc28gbmVlZCB0byBoYW5kbGUgdGhlIGNhc2Ugd2hlcmUgdGhlcmUgYXJlIDIrIHRhYnMuIEVhY2ggSlMgcnVudGltZVxyXG4vLyAgICBnZW5lcmF0ZXMgYSByYW5kb20gc3RyaW5nIHdoaWNoIHNlcnZlcyBhcyB0aGUgXCJJRFwiIGZvciB0aGF0IHRhYi9zZXNzaW9uLlxyXG4vLyAgICBUaGVzZSBJRHMgYXJlIHN0b3JlZCBhbG9uZyB3aXRoIHRoZSBsb2cgbGluZXMuXHJcbi8vICAtIEJ1ZyByZXBvcnRzIGFyZSBzZW50IGFzIGEgUE9TVCBvdmVyIEhUVFBTOiBpdCBwdXJwb3NlZnVsbHkgZG9lcyBub3QgdXNlXHJcbi8vICAgIE1hdHJpeCBhcyBidWcgcmVwb3J0cyBtYXkgYmUgbWFkZSB3aGVuIE1hdHJpeCBpcyBub3QgcmVzcG9uc2l2ZSAod2hpY2ggbWF5XHJcbi8vICAgIGJlIHRoZSBjYXVzZSBvZiB0aGUgYnVnKS4gV2Ugc2VuZCB0aGUgbW9zdCByZWNlbnQgTiBNQiBvZiBVVEYtOCBsb2cgZGF0YSxcclxuLy8gICAgc3RhcnRpbmcgd2l0aCB0aGUgbW9zdCByZWNlbnQsIHdoaWNoIHdlIGtub3cgYmVjYXVzZSB0aGUgXCJJRFwicyBhcmVcclxuLy8gICAgYWN0dWFsbHkgdGltZXN0YW1wcy4gV2UgdGhlbiBwdXJnZSB0aGUgcmVtYWluaW5nIGxvZ3MuIFdlIGFsc28gZG8gdGhpc1xyXG4vLyAgICBwdXJnZSBvbiBzdGFydHVwIHRvIHByZXZlbnQgbG9ncyBmcm9tIGFjY3VtdWxhdGluZy5cclxuXHJcbi8vIHRoZSBmcmVxdWVuY3kgd2l0aCB3aGljaCB3ZSBmbHVzaCB0byBpbmRleGVkZGJcclxuY29uc3QgRkxVU0hfUkFURV9NUyA9IDMwICogMTAwMDtcclxuXHJcbi8vIHRoZSBsZW5ndGggb2YgbG9nIGRhdGEgd2Uga2VlcCBpbiBpbmRleGVkZGIgKGFuZCBpbmNsdWRlIGluIHRoZSByZXBvcnRzKVxyXG5jb25zdCBNQVhfTE9HX1NJWkUgPSAxMDI0ICogMTAyNCAqIDE7IC8vIDEgTUJcclxuXHJcbi8vIEEgY2xhc3Mgd2hpY2ggbW9ua2V5LXBhdGNoZXMgdGhlIGdsb2JhbCBjb25zb2xlIGFuZCBzdG9yZXMgbG9nIGxpbmVzLlxyXG5jbGFzcyBDb25zb2xlTG9nZ2VyIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMubG9ncyA9IFwiXCI7XHJcbiAgICB9XHJcblxyXG4gICAgbW9ua2V5UGF0Y2goY29uc29sZU9iaikge1xyXG4gICAgICAgIC8vIE1vbmtleS1wYXRjaCBjb25zb2xlIGxvZ2dpbmdcclxuICAgICAgICBjb25zdCBjb25zb2xlRnVuY3Rpb25zVG9MZXZlbHMgPSB7XHJcbiAgICAgICAgICAgIGxvZzogXCJJXCIsXHJcbiAgICAgICAgICAgIGluZm86IFwiSVwiLFxyXG4gICAgICAgICAgICB3YXJuOiBcIldcIixcclxuICAgICAgICAgICAgZXJyb3I6IFwiRVwiLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgT2JqZWN0LmtleXMoY29uc29sZUZ1bmN0aW9uc1RvTGV2ZWxzKS5mb3JFYWNoKChmbk5hbWUpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbGV2ZWwgPSBjb25zb2xlRnVuY3Rpb25zVG9MZXZlbHNbZm5OYW1lXTtcclxuICAgICAgICAgICAgY29uc3Qgb3JpZ2luYWxGbiA9IGNvbnNvbGVPYmpbZm5OYW1lXS5iaW5kKGNvbnNvbGVPYmopO1xyXG4gICAgICAgICAgICBjb25zb2xlT2JqW2ZuTmFtZV0gPSAoLi4uYXJncykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2cobGV2ZWwsIC4uLmFyZ3MpO1xyXG4gICAgICAgICAgICAgICAgb3JpZ2luYWxGbiguLi5hcmdzKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2cobGV2ZWwsIC4uLmFyZ3MpIHtcclxuICAgICAgICAvLyBXZSBkb24ndCBrbm93IHdoYXQgbG9jYWxlIHRoZSB1c2VyIG1heSBiZSBydW5uaW5nIHNvIHVzZSBJU08gc3RyaW5nc1xyXG4gICAgICAgIGNvbnN0IHRzID0gbmV3IERhdGUoKS50b0lTT1N0cmluZygpO1xyXG5cclxuICAgICAgICAvLyBDb252ZXJ0IG9iamVjdHMgYW5kIGVycm9ycyB0byBoZWxwZnVsIHRoaW5nc1xyXG4gICAgICAgIGFyZ3MgPSBhcmdzLm1hcCgoYXJnKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcmcgaW5zdGFuY2VvZiBFcnJvcikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFyZy5tZXNzYWdlICsgKGFyZy5zdGFjayA/IGBcXG4ke2FyZy5zdGFja31gIDogJycpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiAoYXJnKSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGFyZyk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSW4gZGV2ZWxvcG1lbnQsIGl0IGNhbiBiZSB1c2VmdWwgdG8gbG9nIGNvbXBsZXggY3ljbGljXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb2JqZWN0cyB0byB0aGUgY29uc29sZSBmb3IgaW5zcGVjdGlvbi4gVGhpcyBpcyBmaW5lIGZvclxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZSBjb25zb2xlLCBidXQgZGVmYXVsdCBgc3RyaW5naWZ5YCBjYW4ndCBoYW5kbGUgdGhhdC5cclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSB3b3JrYXJvdW5kIHRoaXMgYnkgdXNpbmcgYSBzcGVjaWFsIHJlcGxhY2VyIGZ1bmN0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdG8gb25seSBsb2cgdmFsdWVzIG9mIHRoZSByb290IG9iamVjdCBhbmQgYXZvaWQgY3ljbGVzLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShhcmcsIChrZXksIHZhbHVlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChrZXkgJiYgdHlwZW9mIHZhbHVlID09PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCI8b2JqZWN0PlwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhcmc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gU29tZSBicm93c2VycyBzdXBwb3J0IHN0cmluZyBmb3JtYXR0aW5nIHdoaWNoIHdlJ3JlIG5vdCBkb2luZyBoZXJlXHJcbiAgICAgICAgLy8gc28gdGhlIGxpbmVzIGFyZSBhIGxpdHRsZSBtb3JlIHVnbHkgYnV0IGVhc3kgdG8gaW1wbGVtZW50IC8gcXVpY2sgdG9cclxuICAgICAgICAvLyBydW4uXHJcbiAgICAgICAgLy8gRXhhbXBsZSBsaW5lOlxyXG4gICAgICAgIC8vIDIwMTctMDEtMThUMTE6MjM6NTMuMjE0WiBXIEZhaWxlZCB0byBzZXQgYmFkZ2UgY291bnRcclxuICAgICAgICBsZXQgbGluZSA9IGAke3RzfSAke2xldmVsfSAke2FyZ3Muam9pbignICcpfVxcbmA7XHJcbiAgICAgICAgLy8gRG8gc29tZSBjbGVhbnVwXHJcbiAgICAgICAgbGluZSA9IGxpbmUucmVwbGFjZSgvdG9rZW49W2EtekEtWjAtOS1dKy9nbSwgJ3Rva2VuPXh4eHh4Jyk7XHJcbiAgICAgICAgLy8gVXNpbmcgKyByZWFsbHkgaXMgdGhlIHF1aWNrZXN0IHdheSBpbiBKU1xyXG4gICAgICAgIC8vIGh0dHA6Ly9qc3BlcmYuY29tL2NvbmNhdC12cy1wbHVzLXZzLWpvaW5cclxuICAgICAgICB0aGlzLmxvZ3MgKz0gbGluZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHJpZXZlIGxvZyBsaW5lcyB0byBmbHVzaCB0byBkaXNrLlxyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBrZWVwTG9ncyBUcnVlIHRvIG5vdCBkZWxldGUgbG9ncyBhZnRlciBmbHVzaGluZy5cclxuICAgICAqIEByZXR1cm4ge3N0cmluZ30gXFxuIGRlbGltaXRlZCBsb2cgbGluZXMgdG8gZmx1c2guXHJcbiAgICAgKi9cclxuICAgIGZsdXNoKGtlZXBMb2dzKSB7XHJcbiAgICAgICAgLy8gVGhlIENvbnNvbGVMb2dnZXIgZG9lc24ndCBjYXJlIGhvdyB0aGVzZSBlbmQgdXAgb24gZGlzaywgaXQganVzdFxyXG4gICAgICAgIC8vIGZsdXNoZXMgdGhlbSB0byB0aGUgY2FsbGVyLlxyXG4gICAgICAgIGlmIChrZWVwTG9ncykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sb2dzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBsb2dzVG9GbHVzaCA9IHRoaXMubG9ncztcclxuICAgICAgICB0aGlzLmxvZ3MgPSBcIlwiO1xyXG4gICAgICAgIHJldHVybiBsb2dzVG9GbHVzaDtcclxuICAgIH1cclxufVxyXG5cclxuLy8gQSBjbGFzcyB3aGljaCBzdG9yZXMgbG9nIGxpbmVzIGluIGFuIEluZGV4ZWREQiBpbnN0YW5jZS5cclxuY2xhc3MgSW5kZXhlZERCTG9nU3RvcmUge1xyXG4gICAgY29uc3RydWN0b3IoaW5kZXhlZERCLCBsb2dnZXIpIHtcclxuICAgICAgICB0aGlzLmluZGV4ZWREQiA9IGluZGV4ZWREQjtcclxuICAgICAgICB0aGlzLmxvZ2dlciA9IGxvZ2dlcjtcclxuICAgICAgICB0aGlzLmlkID0gXCJpbnN0YW5jZS1cIiArIE1hdGgucmFuZG9tKCkgKyBEYXRlLm5vdygpO1xyXG4gICAgICAgIHRoaXMuaW5kZXggPSAwO1xyXG4gICAgICAgIHRoaXMuZGIgPSBudWxsO1xyXG5cclxuICAgICAgICAvLyB0aGVzZSBwcm9taXNlcyBhcmUgY2xlYXJlZCBhcyBzb29uIGFzIGZ1bGZpbGxlZFxyXG4gICAgICAgIHRoaXMuZmx1c2hQcm9taXNlID0gbnVsbDtcclxuICAgICAgICAvLyBzZXQgaWYgZmx1c2goKSBpcyBjYWxsZWQgd2hpbHN0IG9uZSBpcyBvbmdvaW5nXHJcbiAgICAgICAgdGhpcy5mbHVzaEFnYWluUHJvbWlzZSA9IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSBSZXNvbHZlcyB3aGVuIHRoZSBzdG9yZSBpcyByZWFkeS5cclxuICAgICAqL1xyXG4gICAgY29ubmVjdCgpIHtcclxuICAgICAgICBjb25zdCByZXEgPSB0aGlzLmluZGV4ZWREQi5vcGVuKFwibG9nc1wiKTtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICByZXEub25zdWNjZXNzID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRiID0gZXZlbnQudGFyZ2V0LnJlc3VsdDtcclxuICAgICAgICAgICAgICAgIC8vIFBlcmlvZGljYWxseSBmbHVzaCBsb2dzIHRvIGxvY2FsIHN0b3JhZ2UgLyBpbmRleGVkZGJcclxuICAgICAgICAgICAgICAgIHNldEludGVydmFsKHRoaXMuZmx1c2guYmluZCh0aGlzKSwgRkxVU0hfUkFURV9NUyk7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICByZXEub25lcnJvciA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXJyID0gKFxyXG4gICAgICAgICAgICAgICAgICAgIFwiRmFpbGVkIHRvIG9wZW4gbG9nIGRhdGFiYXNlOiBcIiArIGV2ZW50LnRhcmdldC5lcnJvci5uYW1lXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihlcnIpKTtcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIC8vIEZpcnN0IHRpbWU6IFNldHVwIHRoZSBvYmplY3Qgc3RvcmVcclxuICAgICAgICAgICAgcmVxLm9udXBncmFkZW5lZWRlZCA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGIgPSBldmVudC50YXJnZXQucmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbG9nT2JqU3RvcmUgPSBkYi5jcmVhdGVPYmplY3RTdG9yZShcImxvZ3NcIiwge1xyXG4gICAgICAgICAgICAgICAgICAgIGtleVBhdGg6IFtcImlkXCIsIFwiaW5kZXhcIl0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIC8vIEtleXMgaW4gdGhlIGRhdGFiYXNlIGxvb2sgbGlrZTogWyBcImluc3RhbmNlLTE0ODkzODQ5MFwiLCAwIF1cclxuICAgICAgICAgICAgICAgIC8vIExhdGVyIG9uIHdlIG5lZWQgdG8gcXVlcnkgZXZlcnl0aGluZyBiYXNlZCBvbiBhbiBpbnN0YW5jZSBpZC5cclxuICAgICAgICAgICAgICAgIC8vIEluIG9yZGVyIHRvIGRvIHRoaXMsIHdlIG5lZWQgdG8gc2V0IHVwIGluZGV4ZXMgXCJpZFwiLlxyXG4gICAgICAgICAgICAgICAgbG9nT2JqU3RvcmUuY3JlYXRlSW5kZXgoXCJpZFwiLCBcImlkXCIsIHsgdW5pcXVlOiBmYWxzZSB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBsb2dPYmpTdG9yZS5hZGQoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZ2VuZXJhdGVMb2dFbnRyeShcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3IERhdGUoKSArIFwiIDo6OiBMb2cgZGF0YWJhc2Ugd2FzIGNyZWF0ZWQuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbGFzdE1vZGlmaWVkU3RvcmUgPSBkYi5jcmVhdGVPYmplY3RTdG9yZShcImxvZ3NsYXN0bW9kXCIsIHtcclxuICAgICAgICAgICAgICAgICAgICBrZXlQYXRoOiBcImlkXCIsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGxhc3RNb2RpZmllZFN0b3JlLmFkZCh0aGlzLl9nZW5lcmF0ZUxhc3RNb2RpZmllZFRpbWUoKSk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGbHVzaCBsb2dzIHRvIGRpc2suXHJcbiAgICAgKlxyXG4gICAgICogVGhlcmUgYXJlIGd1YXJkcyB0byBwcm90ZWN0IGFnYWluc3QgcmFjZSBjb25kaXRpb25zIGluIG9yZGVyIHRvIGVuc3VyZVxyXG4gICAgICogdGhhdCBhbGwgcHJldmlvdXMgZmx1c2hlcyBoYXZlIGNvbXBsZXRlZCBiZWZvcmUgdGhlIG1vc3QgcmVjZW50IGZsdXNoLlxyXG4gICAgICogQ29uc2lkZXIgd2l0aG91dCBndWFyZHM6XHJcbiAgICAgKiAgLSBBIGNhbGxzIGZsdXNoKCkgcGVyaW9kaWNhbGx5LlxyXG4gICAgICogIC0gQiBjYWxscyBmbHVzaCgpIGFuZCB3YW50cyB0byBzZW5kIGxvZ3MgaW1tZWRpYXRlbHkgYWZ0ZXJ3YXJkcy5cclxuICAgICAqICAtIElmIEIgZG9lc24ndCB3YWl0IGZvciBBJ3MgZmx1c2ggdG8gY29tcGxldGUsIEIgd2lsbCBiZSBtaXNzaW5nIHRoZVxyXG4gICAgICogICAgY29udGVudHMgb2YgQSdzIGZsdXNoLlxyXG4gICAgICogVG8gcHJvdGVjdCBhZ2FpbnN0IHRoaXMsIHdlIHNldCAnZmx1c2hQcm9taXNlJyB3aGVuIGEgZmx1c2ggaXMgb25nb2luZy5cclxuICAgICAqIFN1YnNlcXVlbnQgY2FsbHMgdG8gZmx1c2goKSBkdXJpbmcgdGhpcyBwZXJpb2Qgd2lsbCBjaGFpbiBhbm90aGVyIGZsdXNoLFxyXG4gICAgICogdGhlbiBrZWVwIHJldHVybmluZyB0aGF0IHNhbWUgY2hhaW5lZCBmbHVzaC5cclxuICAgICAqXHJcbiAgICAgKiBUaGlzIGd1YXJhbnRlZXMgdGhhdCB3ZSB3aWxsIGFsd2F5cyBldmVudHVhbGx5IGRvIGEgZmx1c2ggd2hlbiBmbHVzaCgpIGlzXHJcbiAgICAgKiBjYWxsZWQuXHJcbiAgICAgKlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gUmVzb2x2ZWQgd2hlbiB0aGUgbG9ncyBoYXZlIGJlZW4gZmx1c2hlZC5cclxuICAgICAqL1xyXG4gICAgZmx1c2goKSB7XHJcbiAgICAgICAgLy8gY2hlY2sgaWYgYSBmbHVzaCgpIG9wZXJhdGlvbiBpcyBvbmdvaW5nXHJcbiAgICAgICAgaWYgKHRoaXMuZmx1c2hQcm9taXNlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZsdXNoQWdhaW5Qcm9taXNlKSB7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzIGlzIHRoZSAzcmQrIHRpbWUgd2UndmUgY2FsbGVkIGZsdXNoKCkgOiByZXR1cm4gdGhlIHNhbWUgcHJvbWlzZS5cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZsdXNoQWdhaW5Qcm9taXNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHF1ZXVlIHVwIGEgZmx1c2ggdG8gb2NjdXIgaW1tZWRpYXRlbHkgYWZ0ZXIgdGhlIHBlbmRpbmcgb25lIGNvbXBsZXRlcy5cclxuICAgICAgICAgICAgdGhpcy5mbHVzaEFnYWluUHJvbWlzZSA9IHRoaXMuZmx1c2hQcm9taXNlLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmx1c2goKTtcclxuICAgICAgICAgICAgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZsdXNoQWdhaW5Qcm9taXNlID0gbnVsbDtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmZsdXNoQWdhaW5Qcm9taXNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0aGVyZSBpcyBubyBmbHVzaCBwcm9taXNlIG9yIHRoZXJlIHdhcyBidXQgaXQgaGFzIGZpbmlzaGVkLCBzbyBkb1xyXG4gICAgICAgIC8vIGEgYnJhbmQgbmV3IG9uZSwgZGVzdHJveWluZyB0aGUgY2hhaW4gd2hpY2ggbWF5IGhhdmUgYmVlbiBidWlsdCB1cC5cclxuICAgICAgICB0aGlzLmZsdXNoUHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmRiKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBub3QgY29ubmVjdGVkIHlldCBvciB1c2VyIHJlamVjdGVkIGFjY2VzcyBmb3IgdXMgdG8gci93IHRvIHRoZSBkYi5cclxuICAgICAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoXCJObyBjb25uZWN0ZWQgZGF0YWJhc2VcIikpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGxpbmVzID0gdGhpcy5sb2dnZXIuZmx1c2goKTtcclxuICAgICAgICAgICAgaWYgKGxpbmVzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHR4biA9IHRoaXMuZGIudHJhbnNhY3Rpb24oW1wibG9nc1wiLCBcImxvZ3NsYXN0bW9kXCJdLCBcInJlYWR3cml0ZVwiKTtcclxuICAgICAgICAgICAgY29uc3Qgb2JqU3RvcmUgPSB0eG4ub2JqZWN0U3RvcmUoXCJsb2dzXCIpO1xyXG4gICAgICAgICAgICB0eG4ub25jb21wbGV0ZSA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0eG4ub25lcnJvciA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcclxuICAgICAgICAgICAgICAgICAgICBcIkZhaWxlZCB0byBmbHVzaCBsb2dzIDogXCIsIGV2ZW50LFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHJlamVjdChcclxuICAgICAgICAgICAgICAgICAgICBuZXcgRXJyb3IoXCJGYWlsZWQgdG8gd3JpdGUgbG9nczogXCIgKyBldmVudC50YXJnZXQuZXJyb3JDb2RlKSxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIG9ialN0b3JlLmFkZCh0aGlzLl9nZW5lcmF0ZUxvZ0VudHJ5KGxpbmVzKSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGxhc3RNb2RTdG9yZSA9IHR4bi5vYmplY3RTdG9yZShcImxvZ3NsYXN0bW9kXCIpO1xyXG4gICAgICAgICAgICBsYXN0TW9kU3RvcmUucHV0KHRoaXMuX2dlbmVyYXRlTGFzdE1vZGlmaWVkVGltZSgpKTtcclxuICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mbHVzaFByb21pc2UgPSBudWxsO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZsdXNoUHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnN1bWUgdGhlIG1vc3QgcmVjZW50IGxvZ3MgYW5kIHJldHVybiB0aGVtLiBPbGRlciBsb2dzIHdoaWNoIGFyZSBub3RcclxuICAgICAqIHJldHVybmVkIGFyZSBkZWxldGVkIGF0IHRoZSBzYW1lIHRpbWUsIHNvIHRoaXMgY2FuIGJlIGNhbGxlZCBhdCBzdGFydHVwXHJcbiAgICAgKiB0byBkbyBob3VzZS1rZWVwaW5nIHRvIGtlZXAgdGhlIGxvZ3MgZnJvbSBncm93aW5nIHRvbyBsYXJnZS5cclxuICAgICAqXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlPE9iamVjdFtdPn0gUmVzb2x2ZXMgdG8gYW4gYXJyYXkgb2Ygb2JqZWN0cy4gVGhlIGFycmF5IGlzXHJcbiAgICAgKiBzb3J0ZWQgaW4gdGltZSAob2xkZXN0IGZpcnN0KSBiYXNlZCBvbiB3aGVuIHRoZSBsb2cgZmlsZSB3YXMgY3JlYXRlZCAodGhlXHJcbiAgICAgKiBsb2cgSUQpLiBUaGUgb2JqZWN0cyBoYXZlIHNhaWQgbG9nIElEIGluIGFuIFwiaWRcIiBmaWVsZCBhbmQgXCJsaW5lc1wiIHdoaWNoXHJcbiAgICAgKiBpcyBhIGJpZyBzdHJpbmcgd2l0aCBhbGwgdGhlIG5ldy1saW5lIGRlbGltaXRlZCBsb2dzLlxyXG4gICAgICovXHJcbiAgICBhc3luYyBjb25zdW1lKCkge1xyXG4gICAgICAgIGNvbnN0IGRiID0gdGhpcy5kYjtcclxuXHJcbiAgICAgICAgLy8gUmV0dXJuczogYSBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSBjb25jYXRlbmF0ZWQgbG9ncyBmb3IgdGhpcyBJRC5cclxuICAgICAgICAvLyBTdG9wcyBhZGRpbmcgbG9nIGZyYWdtZW50cyB3aGVuIHRoZSBzaXplIGV4Y2VlZHMgbWF4U2l6ZVxyXG4gICAgICAgIGZ1bmN0aW9uIGZldGNoTG9ncyhpZCwgbWF4U2l6ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBvYmplY3RTdG9yZSA9IGRiLnRyYW5zYWN0aW9uKFwibG9nc1wiLCBcInJlYWRvbmx5XCIpLm9iamVjdFN0b3JlKFwibG9nc1wiKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBxdWVyeSA9IG9iamVjdFN0b3JlLmluZGV4KFwiaWRcIikub3BlbkN1cnNvcihJREJLZXlSYW5nZS5vbmx5KGlkKSwgJ3ByZXYnKTtcclxuICAgICAgICAgICAgICAgIGxldCBsaW5lcyA9ICcnO1xyXG4gICAgICAgICAgICAgICAgcXVlcnkub25lcnJvciA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoXCJRdWVyeSBmYWlsZWQ6IFwiICsgZXZlbnQudGFyZ2V0LmVycm9yQ29kZSkpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHF1ZXJ5Lm9uc3VjY2VzcyA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGN1cnNvciA9IGV2ZW50LnRhcmdldC5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjdXJzb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShsaW5lcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjsgLy8gZW5kIG9mIHJlc3VsdHNcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgbGluZXMgPSBjdXJzb3IudmFsdWUubGluZXMgKyBsaW5lcztcclxuICAgICAgICAgICAgICAgICAgICBpZiAobGluZXMubGVuZ3RoID49IG1heFNpemUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShsaW5lcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yLmNvbnRpbnVlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZXR1cm5zOiBBIHNvcnRlZCBhcnJheSBvZiBsb2cgSURzLiAobmV3ZXN0IGZpcnN0KVxyXG4gICAgICAgIGZ1bmN0aW9uIGZldGNoTG9nSWRzKCkge1xyXG4gICAgICAgICAgICAvLyBUbyBnYXRoZXIgYWxsIHRoZSBsb2cgSURzLCBxdWVyeSBmb3IgYWxsIHJlY29yZHMgaW4gbG9nc2xhc3Rtb2QuXHJcbiAgICAgICAgICAgIGNvbnN0IG8gPSBkYi50cmFuc2FjdGlvbihcImxvZ3NsYXN0bW9kXCIsIFwicmVhZG9ubHlcIikub2JqZWN0U3RvcmUoXHJcbiAgICAgICAgICAgICAgICBcImxvZ3NsYXN0bW9kXCIsXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHJldHVybiBzZWxlY3RRdWVyeShvLCB1bmRlZmluZWQsIChjdXJzb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IGN1cnNvci52YWx1ZS5pZCxcclxuICAgICAgICAgICAgICAgICAgICB0czogY3Vyc29yLnZhbHVlLnRzLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSkudGhlbigocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBTb3J0IElEcyBieSB0aW1lc3RhbXAgKG5ld2VzdCBmaXJzdClcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXMuc29ydCgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBiLnRzIC0gYS50cztcclxuICAgICAgICAgICAgICAgIH0pLm1hcCgoYSkgPT4gYS5pZCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZGVsZXRlTG9ncyhpZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdHhuID0gZGIudHJhbnNhY3Rpb24oXHJcbiAgICAgICAgICAgICAgICAgICAgW1wibG9nc1wiLCBcImxvZ3NsYXN0bW9kXCJdLCBcInJlYWR3cml0ZVwiLFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG8gPSB0eG4ub2JqZWN0U3RvcmUoXCJsb2dzXCIpO1xyXG4gICAgICAgICAgICAgICAgLy8gb25seSBsb2FkIHRoZSBrZXkgcGF0aCwgbm90IHRoZSBkYXRhIHdoaWNoIG1heSBiZSBodWdlXHJcbiAgICAgICAgICAgICAgICBjb25zdCBxdWVyeSA9IG8uaW5kZXgoXCJpZFwiKS5vcGVuS2V5Q3Vyc29yKElEQktleVJhbmdlLm9ubHkoaWQpKTtcclxuICAgICAgICAgICAgICAgIHF1ZXJ5Lm9uc3VjY2VzcyA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGN1cnNvciA9IGV2ZW50LnRhcmdldC5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjdXJzb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvLmRlbGV0ZShjdXJzb3IucHJpbWFyeUtleSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yLmNvbnRpbnVlKCk7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdHhuLm9uY29tcGxldGUgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHR4bi5vbmVycm9yID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkZhaWxlZCB0byBkZWxldGUgbG9ncyBmb3IgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYCcke2lkfScgOiAke2V2ZW50LnRhcmdldC5lcnJvckNvZGV9YCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIC8vIGRlbGV0ZSBsYXN0IG1vZGlmaWVkIGVudHJpZXNcclxuICAgICAgICAgICAgICAgIGNvbnN0IGxhc3RNb2RTdG9yZSA9IHR4bi5vYmplY3RTdG9yZShcImxvZ3NsYXN0bW9kXCIpO1xyXG4gICAgICAgICAgICAgICAgbGFzdE1vZFN0b3JlLmRlbGV0ZShpZCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgYWxsTG9nSWRzID0gYXdhaXQgZmV0Y2hMb2dJZHMoKTtcclxuICAgICAgICBsZXQgcmVtb3ZlTG9nSWRzID0gW107XHJcbiAgICAgICAgY29uc3QgbG9ncyA9IFtdO1xyXG4gICAgICAgIGxldCBzaXplID0gMDtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFsbExvZ0lkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBsaW5lcyA9IGF3YWl0IGZldGNoTG9ncyhhbGxMb2dJZHNbaV0sIE1BWF9MT0dfU0laRSAtIHNpemUpO1xyXG5cclxuICAgICAgICAgICAgLy8gYWx3YXlzIGFkZCB0aGUgbG9nIGZpbGU6IGZldGNoTG9ncyB3aWxsIHRydW5jYXRlIG9uY2UgdGhlIG1heFNpemUgd2UgZ2l2ZSBpdCBpc1xyXG4gICAgICAgICAgICAvLyBleGNlZWRlZCwgc28gd2UnbGwgZ28gb3ZlciB0aGUgbWF4IGJ1dCBvbmx5IGJ5IG9uZSBmcmFnbWVudCdzIHdvcnRoLlxyXG4gICAgICAgICAgICBsb2dzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbGluZXM6IGxpbmVzLFxyXG4gICAgICAgICAgICAgICAgaWQ6IGFsbExvZ0lkc1tpXSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHNpemUgKz0gbGluZXMubGVuZ3RoO1xyXG5cclxuICAgICAgICAgICAgLy8gSWYgZmV0Y2hMb2dzIHRydW5jYXRlZCB3ZSdsbCBub3cgYmUgYXQgb3Igb3ZlciB0aGUgc2l6ZSBsaW1pdCxcclxuICAgICAgICAgICAgLy8gaW4gd2hpY2ggY2FzZSB3ZSBzaG91bGQgc3RvcCBhbmQgcmVtb3ZlIHRoZSByZXN0IG9mIHRoZSBsb2cgZmlsZXMuXHJcbiAgICAgICAgICAgIGlmIChzaXplID49IE1BWF9MT0dfU0laRSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhlIHJlbWFpbmluZyBsb2cgSURzIHNob3VsZCBiZSByZW1vdmVkLiBJZiB3ZSBnbyBvdXQgb2ZcclxuICAgICAgICAgICAgICAgIC8vIGJvdW5kcyB0aGlzIGlzIGp1c3QgW11cclxuICAgICAgICAgICAgICAgIHJlbW92ZUxvZ0lkcyA9IGFsbExvZ0lkcy5zbGljZShpICsgMSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHJlbW92ZUxvZ0lkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVtb3ZpbmcgbG9nczogXCIsIHJlbW92ZUxvZ0lkcyk7XHJcbiAgICAgICAgICAgIC8vIERvbid0IGF3YWl0IHRoaXMgYmVjYXVzZSBpdCdzIG5vbi1mYXRhbCBpZiB3ZSBjYW4ndCBjbGVhbiB1cFxyXG4gICAgICAgICAgICAvLyBsb2dzLlxyXG4gICAgICAgICAgICBQcm9taXNlLmFsbChyZW1vdmVMb2dJZHMubWFwKChpZCkgPT4gZGVsZXRlTG9ncyhpZCkpKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGBSZW1vdmVkICR7cmVtb3ZlTG9nSWRzLmxlbmd0aH0gb2xkIGxvZ3MuYCk7XHJcbiAgICAgICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBsb2dzO1xyXG4gICAgfVxyXG5cclxuICAgIF9nZW5lcmF0ZUxvZ0VudHJ5KGxpbmVzKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaWQ6IHRoaXMuaWQsXHJcbiAgICAgICAgICAgIGxpbmVzOiBsaW5lcyxcclxuICAgICAgICAgICAgaW5kZXg6IHRoaXMuaW5kZXgrKyxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIF9nZW5lcmF0ZUxhc3RNb2RpZmllZFRpbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaWQ6IHRoaXMuaWQsXHJcbiAgICAgICAgICAgIHRzOiBEYXRlLm5vdygpLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBIZWxwZXIgbWV0aG9kIHRvIGNvbGxlY3QgcmVzdWx0cyBmcm9tIGEgQ3Vyc29yIGFuZCBwcm9taXNlaWZ5IGl0LlxyXG4gKiBAcGFyYW0ge09iamVjdFN0b3JlfEluZGV4fSBzdG9yZSBUaGUgc3RvcmUgdG8gcGVyZm9ybSBvcGVuQ3Vyc29yIG9uLlxyXG4gKiBAcGFyYW0ge0lEQktleVJhbmdlPX0ga2V5UmFuZ2UgT3B0aW9uYWwga2V5IHJhbmdlIHRvIGFwcGx5IG9uIHRoZSBjdXJzb3IuXHJcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlc3VsdE1hcHBlciBBIGZ1bmN0aW9uIHdoaWNoIGlzIHJlcGVhdGVkbHkgY2FsbGVkIHdpdGggYVxyXG4gKiBDdXJzb3IuXHJcbiAqIFJldHVybiB0aGUgZGF0YSB5b3Ugd2FudCB0byBrZWVwLlxyXG4gKiBAcmV0dXJuIHtQcm9taXNlPFRbXT59IFJlc29sdmVzIHRvIGFuIGFycmF5IG9mIHdoYXRldmVyIHlvdSByZXR1cm5lZCBmcm9tXHJcbiAqIHJlc3VsdE1hcHBlci5cclxuICovXHJcbmZ1bmN0aW9uIHNlbGVjdFF1ZXJ5KHN0b3JlLCBrZXlSYW5nZSwgcmVzdWx0TWFwcGVyKSB7XHJcbiAgICBjb25zdCBxdWVyeSA9IHN0b3JlLm9wZW5DdXJzb3Ioa2V5UmFuZ2UpO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICBjb25zdCByZXN1bHRzID0gW107XHJcbiAgICAgICAgcXVlcnkub25lcnJvciA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICByZWplY3QobmV3IEVycm9yKFwiUXVlcnkgZmFpbGVkOiBcIiArIGV2ZW50LnRhcmdldC5lcnJvckNvZGUpKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIGNvbGxlY3QgcmVzdWx0c1xyXG4gICAgICAgIHF1ZXJ5Lm9uc3VjY2VzcyA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBjdXJzb3IgPSBldmVudC50YXJnZXQucmVzdWx0O1xyXG4gICAgICAgICAgICBpZiAoIWN1cnNvcikge1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHRzKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjsgLy8gZW5kIG9mIHJlc3VsdHNcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXN1bHRzLnB1c2gocmVzdWx0TWFwcGVyKGN1cnNvcikpO1xyXG4gICAgICAgICAgICBjdXJzb3IuY29udGludWUoKTtcclxuICAgICAgICB9O1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDb25maWd1cmUgcmFnZSBzaGFraW5nIHN1cHBvcnQgZm9yIHNlbmRpbmcgYnVnIHJlcG9ydHMuXHJcbiAqIE1vZGlmaWVzIGdsb2JhbHMuXHJcbiAqIEByZXR1cm4ge1Byb21pc2V9IFJlc29sdmVzIHdoZW4gc2V0IHVwLlxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGluaXQoKSB7XHJcbiAgICBpZiAoZ2xvYmFsLm14X3JhZ2VfaW5pdFByb21pc2UpIHtcclxuICAgICAgICByZXR1cm4gZ2xvYmFsLm14X3JhZ2VfaW5pdFByb21pc2U7XHJcbiAgICB9XHJcbiAgICBnbG9iYWwubXhfcmFnZV9sb2dnZXIgPSBuZXcgQ29uc29sZUxvZ2dlcigpO1xyXG4gICAgZ2xvYmFsLm14X3JhZ2VfbG9nZ2VyLm1vbmtleVBhdGNoKHdpbmRvdy5jb25zb2xlKTtcclxuXHJcbiAgICAvLyBqdXN0ICphY2Nlc3NpbmcqIGluZGV4ZWREQiB0aHJvd3MgYW4gZXhjZXB0aW9uIGluIGZpcmVmb3ggd2l0aFxyXG4gICAgLy8gaW5kZXhlZGRiIGRpc2FibGVkLlxyXG4gICAgbGV0IGluZGV4ZWREQjtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgaW5kZXhlZERCID0gd2luZG93LmluZGV4ZWREQjtcclxuICAgIH0gY2F0Y2ggKGUpIHt9XHJcblxyXG4gICAgaWYgKGluZGV4ZWREQikge1xyXG4gICAgICAgIGdsb2JhbC5teF9yYWdlX3N0b3JlID0gbmV3IEluZGV4ZWREQkxvZ1N0b3JlKGluZGV4ZWREQiwgZ2xvYmFsLm14X3JhZ2VfbG9nZ2VyKTtcclxuICAgICAgICBnbG9iYWwubXhfcmFnZV9pbml0UHJvbWlzZSA9IGdsb2JhbC5teF9yYWdlX3N0b3JlLmNvbm5lY3QoKTtcclxuICAgICAgICByZXR1cm4gZ2xvYmFsLm14X3JhZ2VfaW5pdFByb21pc2U7XHJcbiAgICB9XHJcbiAgICBnbG9iYWwubXhfcmFnZV9pbml0UHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgcmV0dXJuIGdsb2JhbC5teF9yYWdlX2luaXRQcm9taXNlO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZmx1c2goKSB7XHJcbiAgICBpZiAoIWdsb2JhbC5teF9yYWdlX3N0b3JlKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgZ2xvYmFsLm14X3JhZ2Vfc3RvcmUuZmx1c2goKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIENsZWFuIHVwIG9sZCBsb2dzLlxyXG4gKiBAcmV0dXJuIFByb21pc2UgUmVzb2x2ZXMgaWYgY2xlYW5lZCBsb2dzLlxyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGNsZWFudXAoKSB7XHJcbiAgICBpZiAoIWdsb2JhbC5teF9yYWdlX3N0b3JlKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgYXdhaXQgZ2xvYmFsLm14X3JhZ2Vfc3RvcmUuY29uc3VtZSgpO1xyXG59XHJcblxyXG4vKipcclxuICogR2V0IGEgcmVjZW50IHNuYXBzaG90IG9mIHRoZSBsb2dzLCByZWFkeSBmb3IgYXR0YWNoaW5nIHRvIGEgYnVnIHJlcG9ydFxyXG4gKlxyXG4gKiBAcmV0dXJuIHtBcnJheTx7bGluZXM6IHN0cmluZywgaWQsIHN0cmluZ30+fSAgbGlzdCBvZiBsb2cgZGF0YVxyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldExvZ3NGb3JSZXBvcnQoKSB7XHJcbiAgICBpZiAoIWdsb2JhbC5teF9yYWdlX2xvZ2dlcikge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcclxuICAgICAgICAgICAgXCJObyBjb25zb2xlIGxvZ2dlciwgZGlkIHlvdSBmb3JnZXQgdG8gY2FsbCBpbml0KCk/XCIsXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICAgIC8vIElmIGluIGluY29nbml0byBtb2RlLCBzdG9yZSBpcyBudWxsLCBidXQgd2Ugc3RpbGwgd2FudCBidWcgcmVwb3J0XHJcbiAgICAvLyBzZW5kaW5nIHRvIHdvcmsgZ29pbmcgb2ZmIHRoZSBpbi1tZW1vcnkgY29uc29sZSBsb2dzLlxyXG4gICAgaWYgKGdsb2JhbC5teF9yYWdlX3N0b3JlKSB7XHJcbiAgICAgICAgLy8gZmx1c2ggbW9zdCByZWNlbnQgbG9nc1xyXG4gICAgICAgIGF3YWl0IGdsb2JhbC5teF9yYWdlX3N0b3JlLmZsdXNoKCk7XHJcbiAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbC5teF9yYWdlX3N0b3JlLmNvbnN1bWUoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIFt7XHJcbiAgICAgICAgICAgIGxpbmVzOiBnbG9iYWwubXhfcmFnZV9sb2dnZXIuZmx1c2godHJ1ZSksXHJcbiAgICAgICAgICAgIGlkOiBcIi1cIixcclxuICAgICAgICB9XTtcclxuICAgIH1cclxufVxyXG4iXX0=