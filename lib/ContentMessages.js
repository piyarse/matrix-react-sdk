/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UploadCanceledError = void 0;

var _extend = _interopRequireDefault(require("./extend"));

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var _MatrixClientPeg = require("./MatrixClientPeg");

var sdk = _interopRequireWildcard(require("./index"));

var _languageHandler = require("./languageHandler");

var _Modal = _interopRequireDefault(require("./Modal"));

var _RoomViewStore = _interopRequireDefault(require("./stores/RoomViewStore"));

var _browserEncryptAttachment = _interopRequireDefault(require("browser-encrypt-attachment"));

var _pngChunksExtract = _interopRequireDefault(require("png-chunks-extract"));

require("blueimp-canvas-to-blob");

// Polyfill for Canvas.toBlob API using Canvas.toDataURL
const MAX_WIDTH = 800;
const MAX_HEIGHT = 600; // scraped out of a macOS hidpi (5660ppm) screenshot png
//                  5669 px (x-axis)      , 5669 px (y-axis)      , per metre

const PHYS_HIDPI = [0x00, 0x00, 0x16, 0x25, 0x00, 0x00, 0x16, 0x25, 0x01];

class UploadCanceledError extends Error {}
/**
 * Create a thumbnail for a image DOM element.
 * The image will be smaller than MAX_WIDTH and MAX_HEIGHT.
 * The thumbnail will have the same aspect ratio as the original.
 * Draws the element into a canvas using CanvasRenderingContext2D.drawImage
 * Then calls Canvas.toBlob to get a blob object for the image data.
 *
 * Since it needs to calculate the dimensions of the source image and the
 * thumbnailed image it returns an info object filled out with information
 * about the original image and the thumbnail.
 *
 * @param {HTMLElement} element The element to thumbnail.
 * @param {integer} inputWidth The width of the image in the input element.
 * @param {integer} inputHeight the width of the image in the input element.
 * @param {String} mimeType The mimeType to save the blob as.
 * @return {Promise} A promise that resolves with an object with an info key
 *  and a thumbnail key.
 */


exports.UploadCanceledError = UploadCanceledError;

function createThumbnail(element, inputWidth, inputHeight, mimeType) {
  return new Promise(resolve => {
    let targetWidth = inputWidth;
    let targetHeight = inputHeight;

    if (targetHeight > MAX_HEIGHT) {
      targetWidth = Math.floor(targetWidth * (MAX_HEIGHT / targetHeight));
      targetHeight = MAX_HEIGHT;
    }

    if (targetWidth > MAX_WIDTH) {
      targetHeight = Math.floor(targetHeight * (MAX_WIDTH / targetWidth));
      targetWidth = MAX_WIDTH;
    }

    const canvas = document.createElement("canvas");
    canvas.width = targetWidth;
    canvas.height = targetHeight;
    canvas.getContext("2d").drawImage(element, 0, 0, targetWidth, targetHeight);
    canvas.toBlob(function (thumbnail) {
      resolve({
        info: {
          thumbnail_info: {
            w: targetWidth,
            h: targetHeight,
            mimetype: thumbnail.type,
            size: thumbnail.size
          },
          w: inputWidth,
          h: inputHeight
        },
        thumbnail: thumbnail
      });
    }, mimeType);
  });
}
/**
 * Load a file into a newly created image element.
 *
 * @param {File} imageFile The file to load in an image element.
 * @return {Promise} A promise that resolves with the html image element.
 */


async function loadImageElement(imageFile) {
  // Load the file into an html element
  const img = document.createElement("img");
  const objectUrl = URL.createObjectURL(imageFile);
  const imgPromise = new Promise((resolve, reject) => {
    img.onload = function () {
      URL.revokeObjectURL(objectUrl);
      resolve(img);
    };

    img.onerror = function (e) {
      reject(e);
    };
  });
  img.src = objectUrl; // check for hi-dpi PNGs and fudge display resolution as needed.
  // this is mainly needed for macOS screencaps

  let parsePromise;

  if (imageFile.type === "image/png") {
    // in practice macOS happens to order the chunks so they fall in
    // the first 0x1000 bytes (thanks to a massive ICC header).
    // Thus we could slice the file down to only sniff the first 0x1000
    // bytes (but this makes extractPngChunks choke on the corrupt file)
    const headers = imageFile; //.slice(0, 0x1000);

    parsePromise = readFileAsArrayBuffer(headers).then(arrayBuffer => {
      const buffer = new Uint8Array(arrayBuffer);
      const chunks = (0, _pngChunksExtract.default)(buffer);

      for (const chunk of chunks) {
        if (chunk.name === 'pHYs') {
          if (chunk.data.byteLength !== PHYS_HIDPI.length) return;
          const hidpi = chunk.data.every((val, i) => val === PHYS_HIDPI[i]);
          return hidpi;
        }
      }

      return false;
    });
  }

  const [hidpi] = await Promise.all([parsePromise, imgPromise]);
  const width = hidpi ? img.width >> 1 : img.width;
  const height = hidpi ? img.height >> 1 : img.height;
  return {
    width,
    height,
    img
  };
}
/**
 * Read the metadata for an image file and create and upload a thumbnail of the image.
 *
 * @param {MatrixClient} matrixClient A matrixClient to upload the thumbnail with.
 * @param {String} roomId The ID of the room the image will be uploaded in.
 * @param {File} imageFile The image to read and thumbnail.
 * @return {Promise} A promise that resolves with the attachment info.
 */


function infoForImageFile(matrixClient, roomId, imageFile) {
  let thumbnailType = "image/png";

  if (imageFile.type == "image/jpeg") {
    thumbnailType = "image/jpeg";
  }

  let imageInfo;
  return loadImageElement(imageFile).then(function (r) {
    return createThumbnail(r.img, r.width, r.height, thumbnailType);
  }).then(function (result) {
    imageInfo = result.info;
    return uploadFile(matrixClient, roomId, result.thumbnail);
  }).then(function (result) {
    imageInfo.thumbnail_url = result.url;
    imageInfo.thumbnail_file = result.file;
    return imageInfo;
  });
}
/**
 * Load a file into a newly created video element.
 *
 * @param {File} videoFile The file to load in an video element.
 * @return {Promise} A promise that resolves with the video image element.
 */


function loadVideoElement(videoFile) {
  return new Promise((resolve, reject) => {
    // Load the file into an html element
    const video = document.createElement("video");
    const reader = new FileReader();

    reader.onload = function (e) {
      video.src = e.target.result; // Once ready, returns its size
      // Wait until we have enough data to thumbnail the first frame.

      video.onloadeddata = function () {
        resolve(video);
      };

      video.onerror = function (e) {
        reject(e);
      };
    };

    reader.onerror = function (e) {
      reject(e);
    };

    reader.readAsDataURL(videoFile);
  });
}
/**
 * Read the metadata for a video file and create and upload a thumbnail of the video.
 *
 * @param {MatrixClient} matrixClient A matrixClient to upload the thumbnail with.
 * @param {String} roomId The ID of the room the video will be uploaded to.
 * @param {File} videoFile The video to read and thumbnail.
 * @return {Promise} A promise that resolves with the attachment info.
 */


function infoForVideoFile(matrixClient, roomId, videoFile) {
  const thumbnailType = "image/jpeg";
  let videoInfo;
  return loadVideoElement(videoFile).then(function (video) {
    return createThumbnail(video, video.videoWidth, video.videoHeight, thumbnailType);
  }).then(function (result) {
    videoInfo = result.info;
    return uploadFile(matrixClient, roomId, result.thumbnail);
  }).then(function (result) {
    videoInfo.thumbnail_url = result.url;
    videoInfo.thumbnail_file = result.file;
    return videoInfo;
  });
}
/**
 * Read the file as an ArrayBuffer.
 * @param {File} file The file to read
 * @return {Promise} A promise that resolves with an ArrayBuffer when the file
 *   is read.
 */


function readFileAsArrayBuffer(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = function (e) {
      resolve(e.target.result);
    };

    reader.onerror = function (e) {
      reject(e);
    };

    reader.readAsArrayBuffer(file);
  });
}
/**
 * Upload the file to the content repository.
 * If the room is encrypted then encrypt the file before uploading.
 *
 * @param {MatrixClient} matrixClient The matrix client to upload the file with.
 * @param {String} roomId The ID of the room being uploaded to.
 * @param {File} file The file to upload.
 * @param {Function?} progressHandler optional callback to be called when a chunk of
 *    data is uploaded.
 * @return {Promise} A promise that resolves with an object.
 *  If the file is unencrypted then the object will have a "url" key.
 *  If the file is encrypted then the object will have a "file" key.
 */


function uploadFile(matrixClient, roomId, file, progressHandler) {
  if (matrixClient.isRoomEncrypted(roomId)) {
    // If the room is encrypted then encrypt the file before uploading it.
    // First read the file into memory.
    let canceled = false;
    let uploadPromise;
    let encryptInfo;
    const prom = readFileAsArrayBuffer(file).then(function (data) {
      if (canceled) throw new UploadCanceledError(); // Then encrypt the file.

      return _browserEncryptAttachment.default.encryptAttachment(data);
    }).then(function (encryptResult) {
      if (canceled) throw new UploadCanceledError(); // Record the information needed to decrypt the attachment.

      encryptInfo = encryptResult.info; // Pass the encrypted data as a Blob to the uploader.

      const blob = new Blob([encryptResult.data]);
      uploadPromise = matrixClient.uploadContent(blob, {
        progressHandler: progressHandler,
        includeFilename: false
      });
      return uploadPromise;
    }).then(function (url) {
      // If the attachment is encrypted then bundle the URL along
      // with the information needed to decrypt the attachment and
      // add it under a file key.
      encryptInfo.url = url;

      if (file.type) {
        encryptInfo.mimetype = file.type;
      }

      return {
        "file": encryptInfo
      };
    });

    prom.abort = () => {
      canceled = true;
      if (uploadPromise) _MatrixClientPeg.MatrixClientPeg.get().cancelUpload(uploadPromise);
    };

    return prom;
  } else {
    const basePromise = matrixClient.uploadContent(file, {
      progressHandler: progressHandler
    });
    const promise1 = basePromise.then(function (url) {
      // If the attachment isn't encrypted then include the URL directly.
      return {
        "url": url
      };
    }); // XXX: copy over the abort method to the new promise

    promise1.abort = basePromise.abort;
    return promise1;
  }
}

class ContentMessages {
  constructor() {
    this.inprogress = [];
    this.nextId = 0;
    this._mediaConfig = null;
  }

  static sharedInstance() {
    if (global.mx_ContentMessages === undefined) {
      global.mx_ContentMessages = new ContentMessages();
    }

    return global.mx_ContentMessages;
  }

  _isFileSizeAcceptable(file) {
    if (this._mediaConfig !== null && this._mediaConfig["m.upload.size"] !== undefined && file.size > this._mediaConfig["m.upload.size"]) {
      return false;
    }

    return true;
  }

  _ensureMediaConfigFetched() {
    if (this._mediaConfig !== null) return;
    console.log("[Media Config] Fetching");
    return _MatrixClientPeg.MatrixClientPeg.get().getMediaConfig().then(config => {
      console.log("[Media Config] Fetched config:", config);
      return config;
    }).catch(() => {
      // Media repo can't or won't report limits, so provide an empty object (no limits).
      console.log("[Media Config] Could not fetch config, so not limiting uploads.");
      return {};
    }).then(config => {
      this._mediaConfig = config;
    });
  }

  sendStickerContentToRoom(url, roomId, info, text, matrixClient) {
    return _MatrixClientPeg.MatrixClientPeg.get().sendStickerMessage(roomId, url, info, text).catch(e => {
      console.warn("Failed to send content with URL ".concat(url, " to room ").concat(roomId), e);
      throw e;
    });
  }

  getUploadLimit() {
    if (this._mediaConfig !== null && this._mediaConfig["m.upload.size"] !== undefined) {
      return this._mediaConfig["m.upload.size"];
    } else {
      return null;
    }
  }

  async sendContentListToRoom(files, roomId, matrixClient) {
    if (matrixClient.isGuest()) {
      _dispatcher.default.dispatch({
        action: 'require_registration'
      });

      return;
    }

    const isQuoting = Boolean(_RoomViewStore.default.getQuotingEvent());

    if (isQuoting) {
      const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
      const shouldUpload = await new Promise(resolve => {
        _Modal.default.createTrackedDialog('Upload Reply Warning', '', QuestionDialog, {
          title: (0, _languageHandler._t)('Replying With Files'),
          description: React.createElement("div", null, (0, _languageHandler._t)('At this time it is not possible to reply with a file. ' + 'Would you like to upload this file without replying?')),
          hasCancelButton: true,
          button: (0, _languageHandler._t)("Continue"),
          onFinished: shouldUpload => {
            resolve(shouldUpload);
          }
        });
      });
      if (!shouldUpload) return;
    }

    await this._ensureMediaConfigFetched();
    const tooBigFiles = [];
    const okFiles = [];

    for (let i = 0; i < files.length; ++i) {
      if (this._isFileSizeAcceptable(files[i])) {
        okFiles.push(files[i]);
      } else {
        tooBigFiles.push(files[i]);
      }
    }

    if (tooBigFiles.length > 0) {
      const UploadFailureDialog = sdk.getComponent("dialogs.UploadFailureDialog");
      const uploadFailureDialogPromise = new Promise(resolve => {
        _Modal.default.createTrackedDialog('Upload Failure', '', UploadFailureDialog, {
          badFiles: tooBigFiles,
          totalFiles: files.length,
          contentMessages: this,
          onFinished: shouldContinue => {
            resolve(shouldContinue);
          }
        });
      });
      const shouldContinue = await uploadFailureDialogPromise;
      if (!shouldContinue) return;
    }

    const UploadConfirmDialog = sdk.getComponent("dialogs.UploadConfirmDialog");
    let uploadAll = false; // Promise to complete before sending next file into room, used for synchronisation of file-sending
    // to match the order the files were specified in

    let promBefore = Promise.resolve();

    for (let i = 0; i < okFiles.length; ++i) {
      const file = okFiles[i];

      if (!uploadAll) {
        const shouldContinue = await new Promise(resolve => {
          _Modal.default.createTrackedDialog('Upload Files confirmation', '', UploadConfirmDialog, {
            file,
            currentIndex: i,
            totalFiles: okFiles.length,
            onFinished: (shouldContinue, shouldUploadAll) => {
              if (shouldUploadAll) {
                uploadAll = true;
              }

              resolve(shouldContinue);
            }
          });
        });
        if (!shouldContinue) break;
      }

      promBefore = this._sendContentToRoom(file, roomId, matrixClient, promBefore);
    }
  }

  _sendContentToRoom(file, roomId, matrixClient, promBefore) {
    const content = {
      body: file.name || 'Attachment',
      info: {
        size: file.size
      }
    }; // if we have a mime type for the file, add it to the message metadata

    if (file.type) {
      content.info.mimetype = file.type;
    }

    const prom = new Promise(resolve => {
      if (file.type.indexOf('image/') == 0) {
        content.msgtype = 'm.image';
        infoForImageFile(matrixClient, roomId, file).then(imageInfo => {
          (0, _extend.default)(content.info, imageInfo);
          resolve();
        }, error => {
          console.error(error);
          content.msgtype = 'm.file';
          resolve();
        });
      } else if (file.type.indexOf('audio/') == 0) {
        content.msgtype = 'm.audio';
        resolve();
      } else if (file.type.indexOf('video/') == 0) {
        content.msgtype = 'm.video';
        infoForVideoFile(matrixClient, roomId, file).then(videoInfo => {
          (0, _extend.default)(content.info, videoInfo);
          resolve();
        }, error => {
          content.msgtype = 'm.file';
          resolve();
        });
      } else {
        content.msgtype = 'm.file';
        resolve();
      }
    });
    const upload = {
      fileName: file.name || 'Attachment',
      roomId: roomId,
      total: 0,
      loaded: 0
    };
    this.inprogress.push(upload);

    _dispatcher.default.dispatch({
      action: 'upload_started'
    }); // Focus the composer view


    _dispatcher.default.dispatch({
      action: 'focus_composer'
    });

    let error;

    function onProgress(ev) {
      upload.total = ev.total;
      upload.loaded = ev.loaded;

      _dispatcher.default.dispatch({
        action: 'upload_progress',
        upload: upload
      });
    }

    return prom.then(function () {
      // XXX: upload.promise must be the promise that
      // is returned by uploadFile as it has an abort()
      // method hacked onto it.
      upload.promise = uploadFile(matrixClient, roomId, file, onProgress);
      return upload.promise.then(function (result) {
        content.file = result.file;
        content.url = result.url;
      });
    }).then(url => {
      // Await previous message being sent into the room
      return promBefore;
    }).then(function () {
      return matrixClient.sendMessage(roomId, content);
    }, function (err) {
      error = err;

      if (!upload.canceled) {
        let desc = (0, _languageHandler._t)("The file '%(fileName)s' failed to upload.", {
          fileName: upload.fileName
        });

        if (err.http_status == 413) {
          desc = (0, _languageHandler._t)("The file '%(fileName)s' exceeds this homeserver's size limit for uploads", {
            fileName: upload.fileName
          });
        }

        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

        _Modal.default.createTrackedDialog('Upload failed', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Upload Failed'),
          description: desc
        });
      }
    }).finally(() => {
      const inprogressKeys = Object.keys(this.inprogress);

      for (let i = 0; i < this.inprogress.length; ++i) {
        const k = inprogressKeys[i];

        if (this.inprogress[k].promise === upload.promise) {
          this.inprogress.splice(k, 1);
          break;
        }
      }

      if (error) {
        // 413: File was too big or upset the server in some way:
        // clear the media size limit so we fetch it again next time
        // we try to upload
        if (error && error.http_status === 413) {
          this._mediaConfig = null;
        }

        _dispatcher.default.dispatch({
          action: 'upload_failed',
          upload,
          error
        });
      } else {
        _dispatcher.default.dispatch({
          action: 'upload_finished',
          upload
        });

        _dispatcher.default.dispatch({
          action: 'message_sent'
        });
      }
    });
  }

  getCurrentUploads() {
    return this.inprogress.filter(u => !u.canceled);
  }

  cancelUpload(promise) {
    const inprogressKeys = Object.keys(this.inprogress);
    let upload;

    for (let i = 0; i < this.inprogress.length; ++i) {
      const k = inprogressKeys[i];

      if (this.inprogress[k].promise === promise) {
        upload = this.inprogress[k];
        break;
      }
    }

    if (upload) {
      upload.canceled = true;

      _MatrixClientPeg.MatrixClientPeg.get().cancelUpload(upload.promise);

      _dispatcher.default.dispatch({
        action: 'upload_canceled',
        upload
      });
    }
  }

}

exports.default = ContentMessages;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Db250ZW50TWVzc2FnZXMuanMiXSwibmFtZXMiOlsiTUFYX1dJRFRIIiwiTUFYX0hFSUdIVCIsIlBIWVNfSElEUEkiLCJVcGxvYWRDYW5jZWxlZEVycm9yIiwiRXJyb3IiLCJjcmVhdGVUaHVtYm5haWwiLCJlbGVtZW50IiwiaW5wdXRXaWR0aCIsImlucHV0SGVpZ2h0IiwibWltZVR5cGUiLCJQcm9taXNlIiwicmVzb2x2ZSIsInRhcmdldFdpZHRoIiwidGFyZ2V0SGVpZ2h0IiwiTWF0aCIsImZsb29yIiwiY2FudmFzIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50Iiwid2lkdGgiLCJoZWlnaHQiLCJnZXRDb250ZXh0IiwiZHJhd0ltYWdlIiwidG9CbG9iIiwidGh1bWJuYWlsIiwiaW5mbyIsInRodW1ibmFpbF9pbmZvIiwidyIsImgiLCJtaW1ldHlwZSIsInR5cGUiLCJzaXplIiwibG9hZEltYWdlRWxlbWVudCIsImltYWdlRmlsZSIsImltZyIsIm9iamVjdFVybCIsIlVSTCIsImNyZWF0ZU9iamVjdFVSTCIsImltZ1Byb21pc2UiLCJyZWplY3QiLCJvbmxvYWQiLCJyZXZva2VPYmplY3RVUkwiLCJvbmVycm9yIiwiZSIsInNyYyIsInBhcnNlUHJvbWlzZSIsImhlYWRlcnMiLCJyZWFkRmlsZUFzQXJyYXlCdWZmZXIiLCJ0aGVuIiwiYXJyYXlCdWZmZXIiLCJidWZmZXIiLCJVaW50OEFycmF5IiwiY2h1bmtzIiwiY2h1bmsiLCJuYW1lIiwiZGF0YSIsImJ5dGVMZW5ndGgiLCJsZW5ndGgiLCJoaWRwaSIsImV2ZXJ5IiwidmFsIiwiaSIsImFsbCIsImluZm9Gb3JJbWFnZUZpbGUiLCJtYXRyaXhDbGllbnQiLCJyb29tSWQiLCJ0aHVtYm5haWxUeXBlIiwiaW1hZ2VJbmZvIiwiciIsInJlc3VsdCIsInVwbG9hZEZpbGUiLCJ0aHVtYm5haWxfdXJsIiwidXJsIiwidGh1bWJuYWlsX2ZpbGUiLCJmaWxlIiwibG9hZFZpZGVvRWxlbWVudCIsInZpZGVvRmlsZSIsInZpZGVvIiwicmVhZGVyIiwiRmlsZVJlYWRlciIsInRhcmdldCIsIm9ubG9hZGVkZGF0YSIsInJlYWRBc0RhdGFVUkwiLCJpbmZvRm9yVmlkZW9GaWxlIiwidmlkZW9JbmZvIiwidmlkZW9XaWR0aCIsInZpZGVvSGVpZ2h0IiwicmVhZEFzQXJyYXlCdWZmZXIiLCJwcm9ncmVzc0hhbmRsZXIiLCJpc1Jvb21FbmNyeXB0ZWQiLCJjYW5jZWxlZCIsInVwbG9hZFByb21pc2UiLCJlbmNyeXB0SW5mbyIsInByb20iLCJlbmNyeXB0IiwiZW5jcnlwdEF0dGFjaG1lbnQiLCJlbmNyeXB0UmVzdWx0IiwiYmxvYiIsIkJsb2IiLCJ1cGxvYWRDb250ZW50IiwiaW5jbHVkZUZpbGVuYW1lIiwiYWJvcnQiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJjYW5jZWxVcGxvYWQiLCJiYXNlUHJvbWlzZSIsInByb21pc2UxIiwiQ29udGVudE1lc3NhZ2VzIiwiY29uc3RydWN0b3IiLCJpbnByb2dyZXNzIiwibmV4dElkIiwiX21lZGlhQ29uZmlnIiwic2hhcmVkSW5zdGFuY2UiLCJnbG9iYWwiLCJteF9Db250ZW50TWVzc2FnZXMiLCJ1bmRlZmluZWQiLCJfaXNGaWxlU2l6ZUFjY2VwdGFibGUiLCJfZW5zdXJlTWVkaWFDb25maWdGZXRjaGVkIiwiY29uc29sZSIsImxvZyIsImdldE1lZGlhQ29uZmlnIiwiY29uZmlnIiwiY2F0Y2giLCJzZW5kU3RpY2tlckNvbnRlbnRUb1Jvb20iLCJ0ZXh0Iiwic2VuZFN0aWNrZXJNZXNzYWdlIiwid2FybiIsImdldFVwbG9hZExpbWl0Iiwic2VuZENvbnRlbnRMaXN0VG9Sb29tIiwiZmlsZXMiLCJpc0d1ZXN0IiwiZGlzIiwiZGlzcGF0Y2giLCJhY3Rpb24iLCJpc1F1b3RpbmciLCJCb29sZWFuIiwiUm9vbVZpZXdTdG9yZSIsImdldFF1b3RpbmdFdmVudCIsIlF1ZXN0aW9uRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50Iiwic2hvdWxkVXBsb2FkIiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsImhhc0NhbmNlbEJ1dHRvbiIsImJ1dHRvbiIsIm9uRmluaXNoZWQiLCJ0b29CaWdGaWxlcyIsIm9rRmlsZXMiLCJwdXNoIiwiVXBsb2FkRmFpbHVyZURpYWxvZyIsInVwbG9hZEZhaWx1cmVEaWFsb2dQcm9taXNlIiwiYmFkRmlsZXMiLCJ0b3RhbEZpbGVzIiwiY29udGVudE1lc3NhZ2VzIiwic2hvdWxkQ29udGludWUiLCJVcGxvYWRDb25maXJtRGlhbG9nIiwidXBsb2FkQWxsIiwicHJvbUJlZm9yZSIsImN1cnJlbnRJbmRleCIsInNob3VsZFVwbG9hZEFsbCIsIl9zZW5kQ29udGVudFRvUm9vbSIsImNvbnRlbnQiLCJib2R5IiwiaW5kZXhPZiIsIm1zZ3R5cGUiLCJlcnJvciIsInVwbG9hZCIsImZpbGVOYW1lIiwidG90YWwiLCJsb2FkZWQiLCJvblByb2dyZXNzIiwiZXYiLCJwcm9taXNlIiwic2VuZE1lc3NhZ2UiLCJlcnIiLCJkZXNjIiwiaHR0cF9zdGF0dXMiLCJFcnJvckRpYWxvZyIsImZpbmFsbHkiLCJpbnByb2dyZXNzS2V5cyIsIk9iamVjdCIsImtleXMiLCJrIiwic3BsaWNlIiwiZ2V0Q3VycmVudFVwbG9hZHMiLCJmaWx0ZXIiLCJ1Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7Ozs7Ozs7Ozs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFHQTs7QUFEQTtBQUdBLE1BQU1BLFNBQVMsR0FBRyxHQUFsQjtBQUNBLE1BQU1DLFVBQVUsR0FBRyxHQUFuQixDLENBRUE7QUFDQTs7QUFDQSxNQUFNQyxVQUFVLEdBQUcsQ0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUIsSUFBbkIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUMsSUFBckMsRUFBMkMsSUFBM0MsRUFBaUQsSUFBakQsQ0FBbkI7O0FBRU8sTUFBTUMsbUJBQU4sU0FBa0NDLEtBQWxDLENBQXdDO0FBRS9DOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLFNBQVNDLGVBQVQsQ0FBeUJDLE9BQXpCLEVBQWtDQyxVQUFsQyxFQUE4Q0MsV0FBOUMsRUFBMkRDLFFBQTNELEVBQXFFO0FBQ2pFLFNBQU8sSUFBSUMsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDNUIsUUFBSUMsV0FBVyxHQUFHTCxVQUFsQjtBQUNBLFFBQUlNLFlBQVksR0FBR0wsV0FBbkI7O0FBQ0EsUUFBSUssWUFBWSxHQUFHWixVQUFuQixFQUErQjtBQUMzQlcsTUFBQUEsV0FBVyxHQUFHRSxJQUFJLENBQUNDLEtBQUwsQ0FBV0gsV0FBVyxJQUFJWCxVQUFVLEdBQUdZLFlBQWpCLENBQXRCLENBQWQ7QUFDQUEsTUFBQUEsWUFBWSxHQUFHWixVQUFmO0FBQ0g7O0FBQ0QsUUFBSVcsV0FBVyxHQUFHWixTQUFsQixFQUE2QjtBQUN6QmEsTUFBQUEsWUFBWSxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0YsWUFBWSxJQUFJYixTQUFTLEdBQUdZLFdBQWhCLENBQXZCLENBQWY7QUFDQUEsTUFBQUEsV0FBVyxHQUFHWixTQUFkO0FBQ0g7O0FBRUQsVUFBTWdCLE1BQU0sR0FBR0MsUUFBUSxDQUFDQyxhQUFULENBQXVCLFFBQXZCLENBQWY7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRyxLQUFQLEdBQWVQLFdBQWY7QUFDQUksSUFBQUEsTUFBTSxDQUFDSSxNQUFQLEdBQWdCUCxZQUFoQjtBQUNBRyxJQUFBQSxNQUFNLENBQUNLLFVBQVAsQ0FBa0IsSUFBbEIsRUFBd0JDLFNBQXhCLENBQWtDaEIsT0FBbEMsRUFBMkMsQ0FBM0MsRUFBOEMsQ0FBOUMsRUFBaURNLFdBQWpELEVBQThEQyxZQUE5RDtBQUNBRyxJQUFBQSxNQUFNLENBQUNPLE1BQVAsQ0FBYyxVQUFTQyxTQUFULEVBQW9CO0FBQzlCYixNQUFBQSxPQUFPLENBQUM7QUFDSmMsUUFBQUEsSUFBSSxFQUFFO0FBQ0ZDLFVBQUFBLGNBQWMsRUFBRTtBQUNaQyxZQUFBQSxDQUFDLEVBQUVmLFdBRFM7QUFFWmdCLFlBQUFBLENBQUMsRUFBRWYsWUFGUztBQUdaZ0IsWUFBQUEsUUFBUSxFQUFFTCxTQUFTLENBQUNNLElBSFI7QUFJWkMsWUFBQUEsSUFBSSxFQUFFUCxTQUFTLENBQUNPO0FBSkosV0FEZDtBQU9GSixVQUFBQSxDQUFDLEVBQUVwQixVQVBEO0FBUUZxQixVQUFBQSxDQUFDLEVBQUVwQjtBQVJELFNBREY7QUFXSmdCLFFBQUFBLFNBQVMsRUFBRUE7QUFYUCxPQUFELENBQVA7QUFhSCxLQWRELEVBY0dmLFFBZEg7QUFlSCxHQS9CTSxDQUFQO0FBZ0NIO0FBRUQ7Ozs7Ozs7O0FBTUEsZUFBZXVCLGdCQUFmLENBQWdDQyxTQUFoQyxFQUEyQztBQUN2QztBQUNBLFFBQU1DLEdBQUcsR0FBR2pCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBQ0EsUUFBTWlCLFNBQVMsR0FBR0MsR0FBRyxDQUFDQyxlQUFKLENBQW9CSixTQUFwQixDQUFsQjtBQUNBLFFBQU1LLFVBQVUsR0FBRyxJQUFJNUIsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVTRCLE1BQVYsS0FBcUI7QUFDaERMLElBQUFBLEdBQUcsQ0FBQ00sTUFBSixHQUFhLFlBQVc7QUFDcEJKLE1BQUFBLEdBQUcsQ0FBQ0ssZUFBSixDQUFvQk4sU0FBcEI7QUFDQXhCLE1BQUFBLE9BQU8sQ0FBQ3VCLEdBQUQsQ0FBUDtBQUNILEtBSEQ7O0FBSUFBLElBQUFBLEdBQUcsQ0FBQ1EsT0FBSixHQUFjLFVBQVNDLENBQVQsRUFBWTtBQUN0QkosTUFBQUEsTUFBTSxDQUFDSSxDQUFELENBQU47QUFDSCxLQUZEO0FBR0gsR0FSa0IsQ0FBbkI7QUFTQVQsRUFBQUEsR0FBRyxDQUFDVSxHQUFKLEdBQVVULFNBQVYsQ0FidUMsQ0FldkM7QUFDQTs7QUFDQSxNQUFJVSxZQUFKOztBQUNBLE1BQUlaLFNBQVMsQ0FBQ0gsSUFBVixLQUFtQixXQUF2QixFQUFvQztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQU1nQixPQUFPLEdBQUdiLFNBQWhCLENBTGdDLENBS0w7O0FBQzNCWSxJQUFBQSxZQUFZLEdBQUdFLHFCQUFxQixDQUFDRCxPQUFELENBQXJCLENBQStCRSxJQUEvQixDQUFvQ0MsV0FBVyxJQUFJO0FBQzlELFlBQU1DLE1BQU0sR0FBRyxJQUFJQyxVQUFKLENBQWVGLFdBQWYsQ0FBZjtBQUNBLFlBQU1HLE1BQU0sR0FBRywrQkFBaUJGLE1BQWpCLENBQWY7O0FBQ0EsV0FBSyxNQUFNRyxLQUFYLElBQW9CRCxNQUFwQixFQUE0QjtBQUN4QixZQUFJQyxLQUFLLENBQUNDLElBQU4sS0FBZSxNQUFuQixFQUEyQjtBQUN2QixjQUFJRCxLQUFLLENBQUNFLElBQU4sQ0FBV0MsVUFBWCxLQUEwQnRELFVBQVUsQ0FBQ3VELE1BQXpDLEVBQWlEO0FBQ2pELGdCQUFNQyxLQUFLLEdBQUdMLEtBQUssQ0FBQ0UsSUFBTixDQUFXSSxLQUFYLENBQWlCLENBQUNDLEdBQUQsRUFBTUMsQ0FBTixLQUFZRCxHQUFHLEtBQUsxRCxVQUFVLENBQUMyRCxDQUFELENBQS9DLENBQWQ7QUFDQSxpQkFBT0gsS0FBUDtBQUNIO0FBQ0o7O0FBQ0QsYUFBTyxLQUFQO0FBQ0gsS0FYYyxDQUFmO0FBWUg7O0FBRUQsUUFBTSxDQUFDQSxLQUFELElBQVUsTUFBTWhELE9BQU8sQ0FBQ29ELEdBQVIsQ0FBWSxDQUFDakIsWUFBRCxFQUFlUCxVQUFmLENBQVosQ0FBdEI7QUFDQSxRQUFNbkIsS0FBSyxHQUFHdUMsS0FBSyxHQUFJeEIsR0FBRyxDQUFDZixLQUFKLElBQWEsQ0FBakIsR0FBc0JlLEdBQUcsQ0FBQ2YsS0FBN0M7QUFDQSxRQUFNQyxNQUFNLEdBQUdzQyxLQUFLLEdBQUl4QixHQUFHLENBQUNkLE1BQUosSUFBYyxDQUFsQixHQUF1QmMsR0FBRyxDQUFDZCxNQUEvQztBQUNBLFNBQU87QUFBQ0QsSUFBQUEsS0FBRDtBQUFRQyxJQUFBQSxNQUFSO0FBQWdCYyxJQUFBQTtBQUFoQixHQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7OztBQVFBLFNBQVM2QixnQkFBVCxDQUEwQkMsWUFBMUIsRUFBd0NDLE1BQXhDLEVBQWdEaEMsU0FBaEQsRUFBMkQ7QUFDdkQsTUFBSWlDLGFBQWEsR0FBRyxXQUFwQjs7QUFDQSxNQUFJakMsU0FBUyxDQUFDSCxJQUFWLElBQWtCLFlBQXRCLEVBQW9DO0FBQ2hDb0MsSUFBQUEsYUFBYSxHQUFHLFlBQWhCO0FBQ0g7O0FBRUQsTUFBSUMsU0FBSjtBQUNBLFNBQU9uQyxnQkFBZ0IsQ0FBQ0MsU0FBRCxDQUFoQixDQUE0QmUsSUFBNUIsQ0FBaUMsVUFBU29CLENBQVQsRUFBWTtBQUNoRCxXQUFPL0QsZUFBZSxDQUFDK0QsQ0FBQyxDQUFDbEMsR0FBSCxFQUFRa0MsQ0FBQyxDQUFDakQsS0FBVixFQUFpQmlELENBQUMsQ0FBQ2hELE1BQW5CLEVBQTJCOEMsYUFBM0IsQ0FBdEI7QUFDSCxHQUZNLEVBRUpsQixJQUZJLENBRUMsVUFBU3FCLE1BQVQsRUFBaUI7QUFDckJGLElBQUFBLFNBQVMsR0FBR0UsTUFBTSxDQUFDNUMsSUFBbkI7QUFDQSxXQUFPNkMsVUFBVSxDQUFDTixZQUFELEVBQWVDLE1BQWYsRUFBdUJJLE1BQU0sQ0FBQzdDLFNBQTlCLENBQWpCO0FBQ0gsR0FMTSxFQUtKd0IsSUFMSSxDQUtDLFVBQVNxQixNQUFULEVBQWlCO0FBQ3JCRixJQUFBQSxTQUFTLENBQUNJLGFBQVYsR0FBMEJGLE1BQU0sQ0FBQ0csR0FBakM7QUFDQUwsSUFBQUEsU0FBUyxDQUFDTSxjQUFWLEdBQTJCSixNQUFNLENBQUNLLElBQWxDO0FBQ0EsV0FBT1AsU0FBUDtBQUNILEdBVE0sQ0FBUDtBQVVIO0FBRUQ7Ozs7Ozs7O0FBTUEsU0FBU1EsZ0JBQVQsQ0FBMEJDLFNBQTFCLEVBQXFDO0FBQ2pDLFNBQU8sSUFBSWxFLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVU0QixNQUFWLEtBQXFCO0FBQ3BDO0FBQ0EsVUFBTXNDLEtBQUssR0FBRzVELFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixPQUF2QixDQUFkO0FBRUEsVUFBTTRELE1BQU0sR0FBRyxJQUFJQyxVQUFKLEVBQWY7O0FBRUFELElBQUFBLE1BQU0sQ0FBQ3RDLE1BQVAsR0FBZ0IsVUFBU0csQ0FBVCxFQUFZO0FBQ3hCa0MsTUFBQUEsS0FBSyxDQUFDakMsR0FBTixHQUFZRCxDQUFDLENBQUNxQyxNQUFGLENBQVNYLE1BQXJCLENBRHdCLENBR3hCO0FBQ0E7O0FBQ0FRLE1BQUFBLEtBQUssQ0FBQ0ksWUFBTixHQUFxQixZQUFXO0FBQzVCdEUsUUFBQUEsT0FBTyxDQUFDa0UsS0FBRCxDQUFQO0FBQ0gsT0FGRDs7QUFHQUEsTUFBQUEsS0FBSyxDQUFDbkMsT0FBTixHQUFnQixVQUFTQyxDQUFULEVBQVk7QUFDeEJKLFFBQUFBLE1BQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQ0gsT0FGRDtBQUdILEtBWEQ7O0FBWUFtQyxJQUFBQSxNQUFNLENBQUNwQyxPQUFQLEdBQWlCLFVBQVNDLENBQVQsRUFBWTtBQUN6QkosTUFBQUEsTUFBTSxDQUFDSSxDQUFELENBQU47QUFDSCxLQUZEOztBQUdBbUMsSUFBQUEsTUFBTSxDQUFDSSxhQUFQLENBQXFCTixTQUFyQjtBQUNILEdBdEJNLENBQVA7QUF1Qkg7QUFFRDs7Ozs7Ozs7OztBQVFBLFNBQVNPLGdCQUFULENBQTBCbkIsWUFBMUIsRUFBd0NDLE1BQXhDLEVBQWdEVyxTQUFoRCxFQUEyRDtBQUN2RCxRQUFNVixhQUFhLEdBQUcsWUFBdEI7QUFFQSxNQUFJa0IsU0FBSjtBQUNBLFNBQU9ULGdCQUFnQixDQUFDQyxTQUFELENBQWhCLENBQTRCNUIsSUFBNUIsQ0FBaUMsVUFBUzZCLEtBQVQsRUFBZ0I7QUFDcEQsV0FBT3hFLGVBQWUsQ0FBQ3dFLEtBQUQsRUFBUUEsS0FBSyxDQUFDUSxVQUFkLEVBQTBCUixLQUFLLENBQUNTLFdBQWhDLEVBQTZDcEIsYUFBN0MsQ0FBdEI7QUFDSCxHQUZNLEVBRUpsQixJQUZJLENBRUMsVUFBU3FCLE1BQVQsRUFBaUI7QUFDckJlLElBQUFBLFNBQVMsR0FBR2YsTUFBTSxDQUFDNUMsSUFBbkI7QUFDQSxXQUFPNkMsVUFBVSxDQUFDTixZQUFELEVBQWVDLE1BQWYsRUFBdUJJLE1BQU0sQ0FBQzdDLFNBQTlCLENBQWpCO0FBQ0gsR0FMTSxFQUtKd0IsSUFMSSxDQUtDLFVBQVNxQixNQUFULEVBQWlCO0FBQ3JCZSxJQUFBQSxTQUFTLENBQUNiLGFBQVYsR0FBMEJGLE1BQU0sQ0FBQ0csR0FBakM7QUFDQVksSUFBQUEsU0FBUyxDQUFDWCxjQUFWLEdBQTJCSixNQUFNLENBQUNLLElBQWxDO0FBQ0EsV0FBT1UsU0FBUDtBQUNILEdBVE0sQ0FBUDtBQVVIO0FBRUQ7Ozs7Ozs7O0FBTUEsU0FBU3JDLHFCQUFULENBQStCMkIsSUFBL0IsRUFBcUM7QUFDakMsU0FBTyxJQUFJaEUsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVTRCLE1BQVYsS0FBcUI7QUFDcEMsVUFBTXVDLE1BQU0sR0FBRyxJQUFJQyxVQUFKLEVBQWY7O0FBQ0FELElBQUFBLE1BQU0sQ0FBQ3RDLE1BQVAsR0FBZ0IsVUFBU0csQ0FBVCxFQUFZO0FBQ3hCaEMsTUFBQUEsT0FBTyxDQUFDZ0MsQ0FBQyxDQUFDcUMsTUFBRixDQUFTWCxNQUFWLENBQVA7QUFDSCxLQUZEOztBQUdBUyxJQUFBQSxNQUFNLENBQUNwQyxPQUFQLEdBQWlCLFVBQVNDLENBQVQsRUFBWTtBQUN6QkosTUFBQUEsTUFBTSxDQUFDSSxDQUFELENBQU47QUFDSCxLQUZEOztBQUdBbUMsSUFBQUEsTUFBTSxDQUFDUyxpQkFBUCxDQUF5QmIsSUFBekI7QUFDSCxHQVRNLENBQVA7QUFVSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7QUFhQSxTQUFTSixVQUFULENBQW9CTixZQUFwQixFQUFrQ0MsTUFBbEMsRUFBMENTLElBQTFDLEVBQWdEYyxlQUFoRCxFQUFpRTtBQUM3RCxNQUFJeEIsWUFBWSxDQUFDeUIsZUFBYixDQUE2QnhCLE1BQTdCLENBQUosRUFBMEM7QUFDdEM7QUFDQTtBQUNBLFFBQUl5QixRQUFRLEdBQUcsS0FBZjtBQUNBLFFBQUlDLGFBQUo7QUFDQSxRQUFJQyxXQUFKO0FBQ0EsVUFBTUMsSUFBSSxHQUFHOUMscUJBQXFCLENBQUMyQixJQUFELENBQXJCLENBQTRCMUIsSUFBNUIsQ0FBaUMsVUFBU08sSUFBVCxFQUFlO0FBQ3pELFVBQUltQyxRQUFKLEVBQWMsTUFBTSxJQUFJdkYsbUJBQUosRUFBTixDQUQyQyxDQUV6RDs7QUFDQSxhQUFPMkYsa0NBQVFDLGlCQUFSLENBQTBCeEMsSUFBMUIsQ0FBUDtBQUNILEtBSlksRUFJVlAsSUFKVSxDQUlMLFVBQVNnRCxhQUFULEVBQXdCO0FBQzVCLFVBQUlOLFFBQUosRUFBYyxNQUFNLElBQUl2RixtQkFBSixFQUFOLENBRGMsQ0FFNUI7O0FBQ0F5RixNQUFBQSxXQUFXLEdBQUdJLGFBQWEsQ0FBQ3ZFLElBQTVCLENBSDRCLENBSTVCOztBQUNBLFlBQU13RSxJQUFJLEdBQUcsSUFBSUMsSUFBSixDQUFTLENBQUNGLGFBQWEsQ0FBQ3pDLElBQWYsQ0FBVCxDQUFiO0FBQ0FvQyxNQUFBQSxhQUFhLEdBQUczQixZQUFZLENBQUNtQyxhQUFiLENBQTJCRixJQUEzQixFQUFpQztBQUM3Q1QsUUFBQUEsZUFBZSxFQUFFQSxlQUQ0QjtBQUU3Q1ksUUFBQUEsZUFBZSxFQUFFO0FBRjRCLE9BQWpDLENBQWhCO0FBS0EsYUFBT1QsYUFBUDtBQUNILEtBaEJZLEVBZ0JWM0MsSUFoQlUsQ0FnQkwsVUFBU3dCLEdBQVQsRUFBYztBQUNsQjtBQUNBO0FBQ0E7QUFDQW9CLE1BQUFBLFdBQVcsQ0FBQ3BCLEdBQVosR0FBa0JBLEdBQWxCOztBQUNBLFVBQUlFLElBQUksQ0FBQzVDLElBQVQsRUFBZTtBQUNYOEQsUUFBQUEsV0FBVyxDQUFDL0QsUUFBWixHQUF1QjZDLElBQUksQ0FBQzVDLElBQTVCO0FBQ0g7O0FBQ0QsYUFBTztBQUFDLGdCQUFROEQ7QUFBVCxPQUFQO0FBQ0gsS0F6QlksQ0FBYjs7QUEwQkFDLElBQUFBLElBQUksQ0FBQ1EsS0FBTCxHQUFhLE1BQU07QUFDZlgsTUFBQUEsUUFBUSxHQUFHLElBQVg7QUFDQSxVQUFJQyxhQUFKLEVBQW1CVyxpQ0FBZ0JDLEdBQWhCLEdBQXNCQyxZQUF0QixDQUFtQ2IsYUFBbkM7QUFDdEIsS0FIRDs7QUFJQSxXQUFPRSxJQUFQO0FBQ0gsR0FyQ0QsTUFxQ087QUFDSCxVQUFNWSxXQUFXLEdBQUd6QyxZQUFZLENBQUNtQyxhQUFiLENBQTJCekIsSUFBM0IsRUFBaUM7QUFDakRjLE1BQUFBLGVBQWUsRUFBRUE7QUFEZ0MsS0FBakMsQ0FBcEI7QUFHQSxVQUFNa0IsUUFBUSxHQUFHRCxXQUFXLENBQUN6RCxJQUFaLENBQWlCLFVBQVN3QixHQUFULEVBQWM7QUFDNUM7QUFDQSxhQUFPO0FBQUMsZUFBT0E7QUFBUixPQUFQO0FBQ0gsS0FIZ0IsQ0FBakIsQ0FKRyxDQVFIOztBQUNBa0MsSUFBQUEsUUFBUSxDQUFDTCxLQUFULEdBQWlCSSxXQUFXLENBQUNKLEtBQTdCO0FBQ0EsV0FBT0ssUUFBUDtBQUNIO0FBQ0o7O0FBRWMsTUFBTUMsZUFBTixDQUFzQjtBQUNqQ0MsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsU0FBS0MsVUFBTCxHQUFrQixFQUFsQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxDQUFkO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNIOztBQUVELFNBQU9DLGNBQVAsR0FBd0I7QUFDcEIsUUFBSUMsTUFBTSxDQUFDQyxrQkFBUCxLQUE4QkMsU0FBbEMsRUFBNkM7QUFDekNGLE1BQUFBLE1BQU0sQ0FBQ0Msa0JBQVAsR0FBNEIsSUFBSVAsZUFBSixFQUE1QjtBQUNIOztBQUNELFdBQU9NLE1BQU0sQ0FBQ0Msa0JBQWQ7QUFDSDs7QUFFREUsRUFBQUEscUJBQXFCLENBQUMxQyxJQUFELEVBQU87QUFDeEIsUUFBSSxLQUFLcUMsWUFBTCxLQUFzQixJQUF0QixJQUNBLEtBQUtBLFlBQUwsQ0FBa0IsZUFBbEIsTUFBdUNJLFNBRHZDLElBRUF6QyxJQUFJLENBQUMzQyxJQUFMLEdBQVksS0FBS2dGLFlBQUwsQ0FBa0IsZUFBbEIsQ0FGaEIsRUFFb0Q7QUFDaEQsYUFBTyxLQUFQO0FBQ0g7O0FBQ0QsV0FBTyxJQUFQO0FBQ0g7O0FBRURNLEVBQUFBLHlCQUF5QixHQUFHO0FBQ3hCLFFBQUksS0FBS04sWUFBTCxLQUFzQixJQUExQixFQUFnQztBQUVoQ08sSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVkseUJBQVo7QUFDQSxXQUFPakIsaUNBQWdCQyxHQUFoQixHQUFzQmlCLGNBQXRCLEdBQXVDeEUsSUFBdkMsQ0FBNkN5RSxNQUFELElBQVk7QUFDM0RILE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdDQUFaLEVBQThDRSxNQUE5QztBQUNBLGFBQU9BLE1BQVA7QUFDSCxLQUhNLEVBR0pDLEtBSEksQ0FHRSxNQUFNO0FBQ1g7QUFDQUosTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVksaUVBQVo7QUFDQSxhQUFPLEVBQVA7QUFDSCxLQVBNLEVBT0p2RSxJQVBJLENBT0V5RSxNQUFELElBQVk7QUFDaEIsV0FBS1YsWUFBTCxHQUFvQlUsTUFBcEI7QUFDSCxLQVRNLENBQVA7QUFVSDs7QUFFREUsRUFBQUEsd0JBQXdCLENBQUNuRCxHQUFELEVBQU1QLE1BQU4sRUFBY3hDLElBQWQsRUFBb0JtRyxJQUFwQixFQUEwQjVELFlBQTFCLEVBQXdDO0FBQzVELFdBQU9zQyxpQ0FBZ0JDLEdBQWhCLEdBQXNCc0Isa0JBQXRCLENBQXlDNUQsTUFBekMsRUFBaURPLEdBQWpELEVBQXNEL0MsSUFBdEQsRUFBNERtRyxJQUE1RCxFQUFrRUYsS0FBbEUsQ0FBeUUvRSxDQUFELElBQU87QUFDbEYyRSxNQUFBQSxPQUFPLENBQUNRLElBQVIsMkNBQWdEdEQsR0FBaEQsc0JBQStEUCxNQUEvRCxHQUF5RXRCLENBQXpFO0FBQ0EsWUFBTUEsQ0FBTjtBQUNILEtBSE0sQ0FBUDtBQUlIOztBQUVEb0YsRUFBQUEsY0FBYyxHQUFHO0FBQ2IsUUFBSSxLQUFLaEIsWUFBTCxLQUFzQixJQUF0QixJQUE4QixLQUFLQSxZQUFMLENBQWtCLGVBQWxCLE1BQXVDSSxTQUF6RSxFQUFvRjtBQUNoRixhQUFPLEtBQUtKLFlBQUwsQ0FBa0IsZUFBbEIsQ0FBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sSUFBUDtBQUNIO0FBQ0o7O0FBRUQsUUFBTWlCLHFCQUFOLENBQTRCQyxLQUE1QixFQUFtQ2hFLE1BQW5DLEVBQTJDRCxZQUEzQyxFQUF5RDtBQUNyRCxRQUFJQSxZQUFZLENBQUNrRSxPQUFiLEVBQUosRUFBNEI7QUFDeEJDLDBCQUFJQyxRQUFKLENBQWE7QUFBQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBYjs7QUFDQTtBQUNIOztBQUVELFVBQU1DLFNBQVMsR0FBR0MsT0FBTyxDQUFDQyx1QkFBY0MsZUFBZCxFQUFELENBQXpCOztBQUNBLFFBQUlILFNBQUosRUFBZTtBQUNYLFlBQU1JLGNBQWMsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHdCQUFqQixDQUF2QjtBQUNBLFlBQU1DLFlBQVksR0FBRyxNQUFNLElBQUluSSxPQUFKLENBQWFDLE9BQUQsSUFBYTtBQUNoRG1JLHVCQUFNQyxtQkFBTixDQUEwQixzQkFBMUIsRUFBa0QsRUFBbEQsRUFBc0RMLGNBQXRELEVBQXNFO0FBQ2xFTSxVQUFBQSxLQUFLLEVBQUUseUJBQUcscUJBQUgsQ0FEMkQ7QUFFbEVDLFVBQUFBLFdBQVcsRUFDUCxpQ0FBTSx5QkFDRiwyREFDQSxzREFGRSxDQUFOLENBSDhEO0FBUWxFQyxVQUFBQSxlQUFlLEVBQUUsSUFSaUQ7QUFTbEVDLFVBQUFBLE1BQU0sRUFBRSx5QkFBRyxVQUFILENBVDBEO0FBVWxFQyxVQUFBQSxVQUFVLEVBQUdQLFlBQUQsSUFBa0I7QUFDMUJsSSxZQUFBQSxPQUFPLENBQUNrSSxZQUFELENBQVA7QUFDSDtBQVppRSxTQUF0RTtBQWNILE9BZjBCLENBQTNCO0FBZ0JBLFVBQUksQ0FBQ0EsWUFBTCxFQUFtQjtBQUN0Qjs7QUFFRCxVQUFNLEtBQUt4Qix5QkFBTCxFQUFOO0FBRUEsVUFBTWdDLFdBQVcsR0FBRyxFQUFwQjtBQUNBLFVBQU1DLE9BQU8sR0FBRyxFQUFoQjs7QUFFQSxTQUFLLElBQUl6RixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHb0UsS0FBSyxDQUFDeEUsTUFBMUIsRUFBa0MsRUFBRUksQ0FBcEMsRUFBdUM7QUFDbkMsVUFBSSxLQUFLdUQscUJBQUwsQ0FBMkJhLEtBQUssQ0FBQ3BFLENBQUQsQ0FBaEMsQ0FBSixFQUEwQztBQUN0Q3lGLFFBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhdEIsS0FBSyxDQUFDcEUsQ0FBRCxDQUFsQjtBQUNILE9BRkQsTUFFTztBQUNId0YsUUFBQUEsV0FBVyxDQUFDRSxJQUFaLENBQWlCdEIsS0FBSyxDQUFDcEUsQ0FBRCxDQUF0QjtBQUNIO0FBQ0o7O0FBRUQsUUFBSXdGLFdBQVcsQ0FBQzVGLE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDeEIsWUFBTStGLG1CQUFtQixHQUFHYixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTVCO0FBQ0EsWUFBTWEsMEJBQTBCLEdBQUcsSUFBSS9JLE9BQUosQ0FBYUMsT0FBRCxJQUFhO0FBQ3hEbUksdUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxFQUE1QyxFQUFnRFMsbUJBQWhELEVBQXFFO0FBQ2pFRSxVQUFBQSxRQUFRLEVBQUVMLFdBRHVEO0FBRWpFTSxVQUFBQSxVQUFVLEVBQUUxQixLQUFLLENBQUN4RSxNQUYrQztBQUdqRW1HLFVBQUFBLGVBQWUsRUFBRSxJQUhnRDtBQUlqRVIsVUFBQUEsVUFBVSxFQUFHUyxjQUFELElBQW9CO0FBQzVCbEosWUFBQUEsT0FBTyxDQUFDa0osY0FBRCxDQUFQO0FBQ0g7QUFOZ0UsU0FBckU7QUFRSCxPQVRrQyxDQUFuQztBQVVBLFlBQU1BLGNBQWMsR0FBRyxNQUFNSiwwQkFBN0I7QUFDQSxVQUFJLENBQUNJLGNBQUwsRUFBcUI7QUFDeEI7O0FBRUQsVUFBTUMsbUJBQW1CLEdBQUduQixHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTVCO0FBQ0EsUUFBSW1CLFNBQVMsR0FBRyxLQUFoQixDQTFEcUQsQ0EyRHJEO0FBQ0E7O0FBQ0EsUUFBSUMsVUFBVSxHQUFHdEosT0FBTyxDQUFDQyxPQUFSLEVBQWpCOztBQUNBLFNBQUssSUFBSWtELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUd5RixPQUFPLENBQUM3RixNQUE1QixFQUFvQyxFQUFFSSxDQUF0QyxFQUF5QztBQUNyQyxZQUFNYSxJQUFJLEdBQUc0RSxPQUFPLENBQUN6RixDQUFELENBQXBCOztBQUNBLFVBQUksQ0FBQ2tHLFNBQUwsRUFBZ0I7QUFDWixjQUFNRixjQUFjLEdBQUcsTUFBTSxJQUFJbkosT0FBSixDQUFhQyxPQUFELElBQWE7QUFDbERtSSx5QkFBTUMsbUJBQU4sQ0FBMEIsMkJBQTFCLEVBQXVELEVBQXZELEVBQTJEZSxtQkFBM0QsRUFBZ0Y7QUFDNUVwRixZQUFBQSxJQUQ0RTtBQUU1RXVGLFlBQUFBLFlBQVksRUFBRXBHLENBRjhEO0FBRzVFOEYsWUFBQUEsVUFBVSxFQUFFTCxPQUFPLENBQUM3RixNQUh3RDtBQUk1RTJGLFlBQUFBLFVBQVUsRUFBRSxDQUFDUyxjQUFELEVBQWlCSyxlQUFqQixLQUFxQztBQUM3QyxrQkFBSUEsZUFBSixFQUFxQjtBQUNqQkgsZ0JBQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0g7O0FBQ0RwSixjQUFBQSxPQUFPLENBQUNrSixjQUFELENBQVA7QUFDSDtBQVQyRSxXQUFoRjtBQVdILFNBWjRCLENBQTdCO0FBYUEsWUFBSSxDQUFDQSxjQUFMLEVBQXFCO0FBQ3hCOztBQUNERyxNQUFBQSxVQUFVLEdBQUcsS0FBS0csa0JBQUwsQ0FBd0J6RixJQUF4QixFQUE4QlQsTUFBOUIsRUFBc0NELFlBQXRDLEVBQW9EZ0csVUFBcEQsQ0FBYjtBQUNIO0FBQ0o7O0FBRURHLEVBQUFBLGtCQUFrQixDQUFDekYsSUFBRCxFQUFPVCxNQUFQLEVBQWVELFlBQWYsRUFBNkJnRyxVQUE3QixFQUF5QztBQUN2RCxVQUFNSSxPQUFPLEdBQUc7QUFDWkMsTUFBQUEsSUFBSSxFQUFFM0YsSUFBSSxDQUFDcEIsSUFBTCxJQUFhLFlBRFA7QUFFWjdCLE1BQUFBLElBQUksRUFBRTtBQUNGTSxRQUFBQSxJQUFJLEVBQUUyQyxJQUFJLENBQUMzQztBQURUO0FBRk0sS0FBaEIsQ0FEdUQsQ0FRdkQ7O0FBQ0EsUUFBSTJDLElBQUksQ0FBQzVDLElBQVQsRUFBZTtBQUNYc0ksTUFBQUEsT0FBTyxDQUFDM0ksSUFBUixDQUFhSSxRQUFiLEdBQXdCNkMsSUFBSSxDQUFDNUMsSUFBN0I7QUFDSDs7QUFFRCxVQUFNK0QsSUFBSSxHQUFHLElBQUluRixPQUFKLENBQWFDLE9BQUQsSUFBYTtBQUNsQyxVQUFJK0QsSUFBSSxDQUFDNUMsSUFBTCxDQUFVd0ksT0FBVixDQUFrQixRQUFsQixLQUErQixDQUFuQyxFQUFzQztBQUNsQ0YsUUFBQUEsT0FBTyxDQUFDRyxPQUFSLEdBQWtCLFNBQWxCO0FBQ0F4RyxRQUFBQSxnQkFBZ0IsQ0FBQ0MsWUFBRCxFQUFlQyxNQUFmLEVBQXVCUyxJQUF2QixDQUFoQixDQUE2QzFCLElBQTdDLENBQW1EbUIsU0FBRCxJQUFhO0FBQzNELCtCQUFPaUcsT0FBTyxDQUFDM0ksSUFBZixFQUFxQjBDLFNBQXJCO0FBQ0F4RCxVQUFBQSxPQUFPO0FBQ1YsU0FIRCxFQUdJNkosS0FBRCxJQUFTO0FBQ1JsRCxVQUFBQSxPQUFPLENBQUNrRCxLQUFSLENBQWNBLEtBQWQ7QUFDQUosVUFBQUEsT0FBTyxDQUFDRyxPQUFSLEdBQWtCLFFBQWxCO0FBQ0E1SixVQUFBQSxPQUFPO0FBQ1YsU0FQRDtBQVFILE9BVkQsTUFVTyxJQUFJK0QsSUFBSSxDQUFDNUMsSUFBTCxDQUFVd0ksT0FBVixDQUFrQixRQUFsQixLQUErQixDQUFuQyxFQUFzQztBQUN6Q0YsUUFBQUEsT0FBTyxDQUFDRyxPQUFSLEdBQWtCLFNBQWxCO0FBQ0E1SixRQUFBQSxPQUFPO0FBQ1YsT0FITSxNQUdBLElBQUkrRCxJQUFJLENBQUM1QyxJQUFMLENBQVV3SSxPQUFWLENBQWtCLFFBQWxCLEtBQStCLENBQW5DLEVBQXNDO0FBQ3pDRixRQUFBQSxPQUFPLENBQUNHLE9BQVIsR0FBa0IsU0FBbEI7QUFDQXBGLFFBQUFBLGdCQUFnQixDQUFDbkIsWUFBRCxFQUFlQyxNQUFmLEVBQXVCUyxJQUF2QixDQUFoQixDQUE2QzFCLElBQTdDLENBQW1Eb0MsU0FBRCxJQUFhO0FBQzNELCtCQUFPZ0YsT0FBTyxDQUFDM0ksSUFBZixFQUFxQjJELFNBQXJCO0FBQ0F6RSxVQUFBQSxPQUFPO0FBQ1YsU0FIRCxFQUdJNkosS0FBRCxJQUFTO0FBQ1JKLFVBQUFBLE9BQU8sQ0FBQ0csT0FBUixHQUFrQixRQUFsQjtBQUNBNUosVUFBQUEsT0FBTztBQUNWLFNBTkQ7QUFPSCxPQVRNLE1BU0E7QUFDSHlKLFFBQUFBLE9BQU8sQ0FBQ0csT0FBUixHQUFrQixRQUFsQjtBQUNBNUosUUFBQUEsT0FBTztBQUNWO0FBQ0osS0EzQlksQ0FBYjtBQTZCQSxVQUFNOEosTUFBTSxHQUFHO0FBQ1hDLE1BQUFBLFFBQVEsRUFBRWhHLElBQUksQ0FBQ3BCLElBQUwsSUFBYSxZQURaO0FBRVhXLE1BQUFBLE1BQU0sRUFBRUEsTUFGRztBQUdYMEcsTUFBQUEsS0FBSyxFQUFFLENBSEk7QUFJWEMsTUFBQUEsTUFBTSxFQUFFO0FBSkcsS0FBZjtBQU1BLFNBQUsvRCxVQUFMLENBQWdCMEMsSUFBaEIsQ0FBcUJrQixNQUFyQjs7QUFDQXRDLHdCQUFJQyxRQUFKLENBQWE7QUFBQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQVQsS0FBYixFQWpEdUQsQ0FtRHZEOzs7QUFDQUYsd0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxNQUFBQSxNQUFNLEVBQUU7QUFBVCxLQUFiOztBQUVBLFFBQUltQyxLQUFKOztBQUVBLGFBQVNLLFVBQVQsQ0FBb0JDLEVBQXBCLEVBQXdCO0FBQ3BCTCxNQUFBQSxNQUFNLENBQUNFLEtBQVAsR0FBZUcsRUFBRSxDQUFDSCxLQUFsQjtBQUNBRixNQUFBQSxNQUFNLENBQUNHLE1BQVAsR0FBZ0JFLEVBQUUsQ0FBQ0YsTUFBbkI7O0FBQ0F6QywwQkFBSUMsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRSxpQkFBVDtBQUE0Qm9DLFFBQUFBLE1BQU0sRUFBRUE7QUFBcEMsT0FBYjtBQUNIOztBQUVELFdBQU81RSxJQUFJLENBQUM3QyxJQUFMLENBQVUsWUFBVztBQUN4QjtBQUNBO0FBQ0E7QUFDQXlILE1BQUFBLE1BQU0sQ0FBQ00sT0FBUCxHQUFpQnpHLFVBQVUsQ0FDdkJOLFlBRHVCLEVBQ1RDLE1BRFMsRUFDRFMsSUFEQyxFQUNLbUcsVUFETCxDQUEzQjtBQUdBLGFBQU9KLE1BQU0sQ0FBQ00sT0FBUCxDQUFlL0gsSUFBZixDQUFvQixVQUFTcUIsTUFBVCxFQUFpQjtBQUN4QytGLFFBQUFBLE9BQU8sQ0FBQzFGLElBQVIsR0FBZUwsTUFBTSxDQUFDSyxJQUF0QjtBQUNBMEYsUUFBQUEsT0FBTyxDQUFDNUYsR0FBUixHQUFjSCxNQUFNLENBQUNHLEdBQXJCO0FBQ0gsT0FITSxDQUFQO0FBSUgsS0FYTSxFQVdKeEIsSUFYSSxDQVdFd0IsR0FBRCxJQUFTO0FBQ2I7QUFDQSxhQUFPd0YsVUFBUDtBQUNILEtBZE0sRUFjSmhILElBZEksQ0FjQyxZQUFXO0FBQ2YsYUFBT2dCLFlBQVksQ0FBQ2dILFdBQWIsQ0FBeUIvRyxNQUF6QixFQUFpQ21HLE9BQWpDLENBQVA7QUFDSCxLQWhCTSxFQWdCSixVQUFTYSxHQUFULEVBQWM7QUFDYlQsTUFBQUEsS0FBSyxHQUFHUyxHQUFSOztBQUNBLFVBQUksQ0FBQ1IsTUFBTSxDQUFDL0UsUUFBWixFQUFzQjtBQUNsQixZQUFJd0YsSUFBSSxHQUFHLHlCQUFHLDJDQUFILEVBQWdEO0FBQUNSLFVBQUFBLFFBQVEsRUFBRUQsTUFBTSxDQUFDQztBQUFsQixTQUFoRCxDQUFYOztBQUNBLFlBQUlPLEdBQUcsQ0FBQ0UsV0FBSixJQUFtQixHQUF2QixFQUE0QjtBQUN4QkQsVUFBQUEsSUFBSSxHQUFHLHlCQUNILDBFQURHLEVBRUg7QUFBQ1IsWUFBQUEsUUFBUSxFQUFFRCxNQUFNLENBQUNDO0FBQWxCLFdBRkcsQ0FBUDtBQUlIOztBQUNELGNBQU1VLFdBQVcsR0FBR3pDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FFLHVCQUFNQyxtQkFBTixDQUEwQixlQUExQixFQUEyQyxFQUEzQyxFQUErQ3FDLFdBQS9DLEVBQTREO0FBQ3hEcEMsVUFBQUEsS0FBSyxFQUFFLHlCQUFHLGVBQUgsQ0FEaUQ7QUFFeERDLFVBQUFBLFdBQVcsRUFBRWlDO0FBRjJDLFNBQTVEO0FBSUg7QUFDSixLQWhDTSxFQWdDSkcsT0FoQ0ksQ0FnQ0ksTUFBTTtBQUNiLFlBQU1DLGNBQWMsR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBSzNFLFVBQWpCLENBQXZCOztBQUNBLFdBQUssSUFBSWhELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS2dELFVBQUwsQ0FBZ0JwRCxNQUFwQyxFQUE0QyxFQUFFSSxDQUE5QyxFQUFpRDtBQUM3QyxjQUFNNEgsQ0FBQyxHQUFHSCxjQUFjLENBQUN6SCxDQUFELENBQXhCOztBQUNBLFlBQUksS0FBS2dELFVBQUwsQ0FBZ0I0RSxDQUFoQixFQUFtQlYsT0FBbkIsS0FBK0JOLE1BQU0sQ0FBQ00sT0FBMUMsRUFBbUQ7QUFDL0MsZUFBS2xFLFVBQUwsQ0FBZ0I2RSxNQUFoQixDQUF1QkQsQ0FBdkIsRUFBMEIsQ0FBMUI7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsVUFBSWpCLEtBQUosRUFBVztBQUNQO0FBQ0E7QUFDQTtBQUNBLFlBQUlBLEtBQUssSUFBSUEsS0FBSyxDQUFDVyxXQUFOLEtBQXNCLEdBQW5DLEVBQXdDO0FBQ3BDLGVBQUtwRSxZQUFMLEdBQW9CLElBQXBCO0FBQ0g7O0FBQ0RvQiw0QkFBSUMsUUFBSixDQUFhO0FBQUNDLFVBQUFBLE1BQU0sRUFBRSxlQUFUO0FBQTBCb0MsVUFBQUEsTUFBMUI7QUFBa0NELFVBQUFBO0FBQWxDLFNBQWI7QUFDSCxPQVJELE1BUU87QUFDSHJDLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFLGlCQUFUO0FBQTRCb0MsVUFBQUE7QUFBNUIsU0FBYjs7QUFDQXRDLDRCQUFJQyxRQUFKLENBQWE7QUFBQ0MsVUFBQUEsTUFBTSxFQUFFO0FBQVQsU0FBYjtBQUNIO0FBQ0osS0FyRE0sQ0FBUDtBQXNESDs7QUFFRHNELEVBQUFBLGlCQUFpQixHQUFHO0FBQ2hCLFdBQU8sS0FBSzlFLFVBQUwsQ0FBZ0IrRSxNQUFoQixDQUF1QkMsQ0FBQyxJQUFJLENBQUNBLENBQUMsQ0FBQ25HLFFBQS9CLENBQVA7QUFDSDs7QUFFRGMsRUFBQUEsWUFBWSxDQUFDdUUsT0FBRCxFQUFVO0FBQ2xCLFVBQU1PLGNBQWMsR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBSzNFLFVBQWpCLENBQXZCO0FBQ0EsUUFBSTRELE1BQUo7O0FBQ0EsU0FBSyxJQUFJNUcsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxLQUFLZ0QsVUFBTCxDQUFnQnBELE1BQXBDLEVBQTRDLEVBQUVJLENBQTlDLEVBQWlEO0FBQzdDLFlBQU00SCxDQUFDLEdBQUdILGNBQWMsQ0FBQ3pILENBQUQsQ0FBeEI7O0FBQ0EsVUFBSSxLQUFLZ0QsVUFBTCxDQUFnQjRFLENBQWhCLEVBQW1CVixPQUFuQixLQUErQkEsT0FBbkMsRUFBNEM7QUFDeENOLFFBQUFBLE1BQU0sR0FBRyxLQUFLNUQsVUFBTCxDQUFnQjRFLENBQWhCLENBQVQ7QUFDQTtBQUNIO0FBQ0o7O0FBQ0QsUUFBSWhCLE1BQUosRUFBWTtBQUNSQSxNQUFBQSxNQUFNLENBQUMvRSxRQUFQLEdBQWtCLElBQWxCOztBQUNBWSx1Q0FBZ0JDLEdBQWhCLEdBQXNCQyxZQUF0QixDQUFtQ2lFLE1BQU0sQ0FBQ00sT0FBMUM7O0FBQ0E1QywwQkFBSUMsUUFBSixDQUFhO0FBQUNDLFFBQUFBLE1BQU0sRUFBRSxpQkFBVDtBQUE0Qm9DLFFBQUFBO0FBQTVCLE9BQWI7QUFDSDtBQUNKOztBQW5SZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCBleHRlbmQgZnJvbSAnLi9leHRlbmQnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuL01vZGFsJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi9zdG9yZXMvUm9vbVZpZXdTdG9yZSc7XHJcbmltcG9ydCBlbmNyeXB0IGZyb20gXCJicm93c2VyLWVuY3J5cHQtYXR0YWNobWVudFwiO1xyXG5pbXBvcnQgZXh0cmFjdFBuZ0NodW5rcyBmcm9tIFwicG5nLWNodW5rcy1leHRyYWN0XCI7XHJcblxyXG4vLyBQb2x5ZmlsbCBmb3IgQ2FudmFzLnRvQmxvYiBBUEkgdXNpbmcgQ2FudmFzLnRvRGF0YVVSTFxyXG5pbXBvcnQgXCJibHVlaW1wLWNhbnZhcy10by1ibG9iXCI7XHJcblxyXG5jb25zdCBNQVhfV0lEVEggPSA4MDA7XHJcbmNvbnN0IE1BWF9IRUlHSFQgPSA2MDA7XHJcblxyXG4vLyBzY3JhcGVkIG91dCBvZiBhIG1hY09TIGhpZHBpICg1NjYwcHBtKSBzY3JlZW5zaG90IHBuZ1xyXG4vLyAgICAgICAgICAgICAgICAgIDU2NjkgcHggKHgtYXhpcykgICAgICAsIDU2NjkgcHggKHktYXhpcykgICAgICAsIHBlciBtZXRyZVxyXG5jb25zdCBQSFlTX0hJRFBJID0gWzB4MDAsIDB4MDAsIDB4MTYsIDB4MjUsIDB4MDAsIDB4MDAsIDB4MTYsIDB4MjUsIDB4MDFdO1xyXG5cclxuZXhwb3J0IGNsYXNzIFVwbG9hZENhbmNlbGVkRXJyb3IgZXh0ZW5kcyBFcnJvciB7fVxyXG5cclxuLyoqXHJcbiAqIENyZWF0ZSBhIHRodW1ibmFpbCBmb3IgYSBpbWFnZSBET00gZWxlbWVudC5cclxuICogVGhlIGltYWdlIHdpbGwgYmUgc21hbGxlciB0aGFuIE1BWF9XSURUSCBhbmQgTUFYX0hFSUdIVC5cclxuICogVGhlIHRodW1ibmFpbCB3aWxsIGhhdmUgdGhlIHNhbWUgYXNwZWN0IHJhdGlvIGFzIHRoZSBvcmlnaW5hbC5cclxuICogRHJhd3MgdGhlIGVsZW1lbnQgaW50byBhIGNhbnZhcyB1c2luZyBDYW52YXNSZW5kZXJpbmdDb250ZXh0MkQuZHJhd0ltYWdlXHJcbiAqIFRoZW4gY2FsbHMgQ2FudmFzLnRvQmxvYiB0byBnZXQgYSBibG9iIG9iamVjdCBmb3IgdGhlIGltYWdlIGRhdGEuXHJcbiAqXHJcbiAqIFNpbmNlIGl0IG5lZWRzIHRvIGNhbGN1bGF0ZSB0aGUgZGltZW5zaW9ucyBvZiB0aGUgc291cmNlIGltYWdlIGFuZCB0aGVcclxuICogdGh1bWJuYWlsZWQgaW1hZ2UgaXQgcmV0dXJucyBhbiBpbmZvIG9iamVjdCBmaWxsZWQgb3V0IHdpdGggaW5mb3JtYXRpb25cclxuICogYWJvdXQgdGhlIG9yaWdpbmFsIGltYWdlIGFuZCB0aGUgdGh1bWJuYWlsLlxyXG4gKlxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBlbGVtZW50IFRoZSBlbGVtZW50IHRvIHRodW1ibmFpbC5cclxuICogQHBhcmFtIHtpbnRlZ2VyfSBpbnB1dFdpZHRoIFRoZSB3aWR0aCBvZiB0aGUgaW1hZ2UgaW4gdGhlIGlucHV0IGVsZW1lbnQuXHJcbiAqIEBwYXJhbSB7aW50ZWdlcn0gaW5wdXRIZWlnaHQgdGhlIHdpZHRoIG9mIHRoZSBpbWFnZSBpbiB0aGUgaW5wdXQgZWxlbWVudC5cclxuICogQHBhcmFtIHtTdHJpbmd9IG1pbWVUeXBlIFRoZSBtaW1lVHlwZSB0byBzYXZlIHRoZSBibG9iIGFzLlxyXG4gKiBAcmV0dXJuIHtQcm9taXNlfSBBIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIGFuIG9iamVjdCB3aXRoIGFuIGluZm8ga2V5XHJcbiAqICBhbmQgYSB0aHVtYm5haWwga2V5LlxyXG4gKi9cclxuZnVuY3Rpb24gY3JlYXRlVGh1bWJuYWlsKGVsZW1lbnQsIGlucHV0V2lkdGgsIGlucHV0SGVpZ2h0LCBtaW1lVHlwZSkge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgbGV0IHRhcmdldFdpZHRoID0gaW5wdXRXaWR0aDtcclxuICAgICAgICBsZXQgdGFyZ2V0SGVpZ2h0ID0gaW5wdXRIZWlnaHQ7XHJcbiAgICAgICAgaWYgKHRhcmdldEhlaWdodCA+IE1BWF9IRUlHSFQpIHtcclxuICAgICAgICAgICAgdGFyZ2V0V2lkdGggPSBNYXRoLmZsb29yKHRhcmdldFdpZHRoICogKE1BWF9IRUlHSFQgLyB0YXJnZXRIZWlnaHQpKTtcclxuICAgICAgICAgICAgdGFyZ2V0SGVpZ2h0ID0gTUFYX0hFSUdIVDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRhcmdldFdpZHRoID4gTUFYX1dJRFRIKSB7XHJcbiAgICAgICAgICAgIHRhcmdldEhlaWdodCA9IE1hdGguZmxvb3IodGFyZ2V0SGVpZ2h0ICogKE1BWF9XSURUSCAvIHRhcmdldFdpZHRoKSk7XHJcbiAgICAgICAgICAgIHRhcmdldFdpZHRoID0gTUFYX1dJRFRIO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImNhbnZhc1wiKTtcclxuICAgICAgICBjYW52YXMud2lkdGggPSB0YXJnZXRXaWR0aDtcclxuICAgICAgICBjYW52YXMuaGVpZ2h0ID0gdGFyZ2V0SGVpZ2h0O1xyXG4gICAgICAgIGNhbnZhcy5nZXRDb250ZXh0KFwiMmRcIikuZHJhd0ltYWdlKGVsZW1lbnQsIDAsIDAsIHRhcmdldFdpZHRoLCB0YXJnZXRIZWlnaHQpO1xyXG4gICAgICAgIGNhbnZhcy50b0Jsb2IoZnVuY3Rpb24odGh1bWJuYWlsKSB7XHJcbiAgICAgICAgICAgIHJlc29sdmUoe1xyXG4gICAgICAgICAgICAgICAgaW5mbzoge1xyXG4gICAgICAgICAgICAgICAgICAgIHRodW1ibmFpbF9pbmZvOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHc6IHRhcmdldFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoOiB0YXJnZXRIZWlnaHQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWV0eXBlOiB0aHVtYm5haWwudHlwZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZTogdGh1bWJuYWlsLnNpemUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB3OiBpbnB1dFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgIGg6IGlucHV0SGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHRodW1ibmFpbDogdGh1bWJuYWlsLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBtaW1lVHlwZSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIExvYWQgYSBmaWxlIGludG8gYSBuZXdseSBjcmVhdGVkIGltYWdlIGVsZW1lbnQuXHJcbiAqXHJcbiAqIEBwYXJhbSB7RmlsZX0gaW1hZ2VGaWxlIFRoZSBmaWxlIHRvIGxvYWQgaW4gYW4gaW1hZ2UgZWxlbWVudC5cclxuICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCB0aGUgaHRtbCBpbWFnZSBlbGVtZW50LlxyXG4gKi9cclxuYXN5bmMgZnVuY3Rpb24gbG9hZEltYWdlRWxlbWVudChpbWFnZUZpbGUpIHtcclxuICAgIC8vIExvYWQgdGhlIGZpbGUgaW50byBhbiBodG1sIGVsZW1lbnRcclxuICAgIGNvbnN0IGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIik7XHJcbiAgICBjb25zdCBvYmplY3RVcmwgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGltYWdlRmlsZSk7XHJcbiAgICBjb25zdCBpbWdQcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgIGltZy5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgVVJMLnJldm9rZU9iamVjdFVSTChvYmplY3RVcmwpO1xyXG4gICAgICAgICAgICByZXNvbHZlKGltZyk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBpbWcub25lcnJvciA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgcmVqZWN0KGUpO1xyXG4gICAgICAgIH07XHJcbiAgICB9KTtcclxuICAgIGltZy5zcmMgPSBvYmplY3RVcmw7XHJcblxyXG4gICAgLy8gY2hlY2sgZm9yIGhpLWRwaSBQTkdzIGFuZCBmdWRnZSBkaXNwbGF5IHJlc29sdXRpb24gYXMgbmVlZGVkLlxyXG4gICAgLy8gdGhpcyBpcyBtYWlubHkgbmVlZGVkIGZvciBtYWNPUyBzY3JlZW5jYXBzXHJcbiAgICBsZXQgcGFyc2VQcm9taXNlO1xyXG4gICAgaWYgKGltYWdlRmlsZS50eXBlID09PSBcImltYWdlL3BuZ1wiKSB7XHJcbiAgICAgICAgLy8gaW4gcHJhY3RpY2UgbWFjT1MgaGFwcGVucyB0byBvcmRlciB0aGUgY2h1bmtzIHNvIHRoZXkgZmFsbCBpblxyXG4gICAgICAgIC8vIHRoZSBmaXJzdCAweDEwMDAgYnl0ZXMgKHRoYW5rcyB0byBhIG1hc3NpdmUgSUNDIGhlYWRlcikuXHJcbiAgICAgICAgLy8gVGh1cyB3ZSBjb3VsZCBzbGljZSB0aGUgZmlsZSBkb3duIHRvIG9ubHkgc25pZmYgdGhlIGZpcnN0IDB4MTAwMFxyXG4gICAgICAgIC8vIGJ5dGVzIChidXQgdGhpcyBtYWtlcyBleHRyYWN0UG5nQ2h1bmtzIGNob2tlIG9uIHRoZSBjb3JydXB0IGZpbGUpXHJcbiAgICAgICAgY29uc3QgaGVhZGVycyA9IGltYWdlRmlsZTsgLy8uc2xpY2UoMCwgMHgxMDAwKTtcclxuICAgICAgICBwYXJzZVByb21pc2UgPSByZWFkRmlsZUFzQXJyYXlCdWZmZXIoaGVhZGVycykudGhlbihhcnJheUJ1ZmZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJ1ZmZlciA9IG5ldyBVaW50OEFycmF5KGFycmF5QnVmZmVyKTtcclxuICAgICAgICAgICAgY29uc3QgY2h1bmtzID0gZXh0cmFjdFBuZ0NodW5rcyhidWZmZXIpO1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNodW5rIG9mIGNodW5rcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNodW5rLm5hbWUgPT09ICdwSFlzJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjaHVuay5kYXRhLmJ5dGVMZW5ndGggIT09IFBIWVNfSElEUEkubGVuZ3RoKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaGlkcGkgPSBjaHVuay5kYXRhLmV2ZXJ5KCh2YWwsIGkpID0+IHZhbCA9PT0gUEhZU19ISURQSVtpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGhpZHBpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBbaGlkcGldID0gYXdhaXQgUHJvbWlzZS5hbGwoW3BhcnNlUHJvbWlzZSwgaW1nUHJvbWlzZV0pO1xyXG4gICAgY29uc3Qgd2lkdGggPSBoaWRwaSA/IChpbWcud2lkdGggPj4gMSkgOiBpbWcud2lkdGg7XHJcbiAgICBjb25zdCBoZWlnaHQgPSBoaWRwaSA/IChpbWcuaGVpZ2h0ID4+IDEpIDogaW1nLmhlaWdodDtcclxuICAgIHJldHVybiB7d2lkdGgsIGhlaWdodCwgaW1nfTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlYWQgdGhlIG1ldGFkYXRhIGZvciBhbiBpbWFnZSBmaWxlIGFuZCBjcmVhdGUgYW5kIHVwbG9hZCBhIHRodW1ibmFpbCBvZiB0aGUgaW1hZ2UuXHJcbiAqXHJcbiAqIEBwYXJhbSB7TWF0cml4Q2xpZW50fSBtYXRyaXhDbGllbnQgQSBtYXRyaXhDbGllbnQgdG8gdXBsb2FkIHRoZSB0aHVtYm5haWwgd2l0aC5cclxuICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgSUQgb2YgdGhlIHJvb20gdGhlIGltYWdlIHdpbGwgYmUgdXBsb2FkZWQgaW4uXHJcbiAqIEBwYXJhbSB7RmlsZX0gaW1hZ2VGaWxlIFRoZSBpbWFnZSB0byByZWFkIGFuZCB0aHVtYm5haWwuXHJcbiAqIEByZXR1cm4ge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggdGhlIGF0dGFjaG1lbnQgaW5mby5cclxuICovXHJcbmZ1bmN0aW9uIGluZm9Gb3JJbWFnZUZpbGUobWF0cml4Q2xpZW50LCByb29tSWQsIGltYWdlRmlsZSkge1xyXG4gICAgbGV0IHRodW1ibmFpbFR5cGUgPSBcImltYWdlL3BuZ1wiO1xyXG4gICAgaWYgKGltYWdlRmlsZS50eXBlID09IFwiaW1hZ2UvanBlZ1wiKSB7XHJcbiAgICAgICAgdGh1bWJuYWlsVHlwZSA9IFwiaW1hZ2UvanBlZ1wiO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBpbWFnZUluZm87XHJcbiAgICByZXR1cm4gbG9hZEltYWdlRWxlbWVudChpbWFnZUZpbGUpLnRoZW4oZnVuY3Rpb24ocikge1xyXG4gICAgICAgIHJldHVybiBjcmVhdGVUaHVtYm5haWwoci5pbWcsIHIud2lkdGgsIHIuaGVpZ2h0LCB0aHVtYm5haWxUeXBlKTtcclxuICAgIH0pLnRoZW4oZnVuY3Rpb24ocmVzdWx0KSB7XHJcbiAgICAgICAgaW1hZ2VJbmZvID0gcmVzdWx0LmluZm87XHJcbiAgICAgICAgcmV0dXJuIHVwbG9hZEZpbGUobWF0cml4Q2xpZW50LCByb29tSWQsIHJlc3VsdC50aHVtYm5haWwpO1xyXG4gICAgfSkudGhlbihmdW5jdGlvbihyZXN1bHQpIHtcclxuICAgICAgICBpbWFnZUluZm8udGh1bWJuYWlsX3VybCA9IHJlc3VsdC51cmw7XHJcbiAgICAgICAgaW1hZ2VJbmZvLnRodW1ibmFpbF9maWxlID0gcmVzdWx0LmZpbGU7XHJcbiAgICAgICAgcmV0dXJuIGltYWdlSW5mbztcclxuICAgIH0pO1xyXG59XHJcblxyXG4vKipcclxuICogTG9hZCBhIGZpbGUgaW50byBhIG5ld2x5IGNyZWF0ZWQgdmlkZW8gZWxlbWVudC5cclxuICpcclxuICogQHBhcmFtIHtGaWxlfSB2aWRlb0ZpbGUgVGhlIGZpbGUgdG8gbG9hZCBpbiBhbiB2aWRlbyBlbGVtZW50LlxyXG4gKiBAcmV0dXJuIHtQcm9taXNlfSBBIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIHRoZSB2aWRlbyBpbWFnZSBlbGVtZW50LlxyXG4gKi9cclxuZnVuY3Rpb24gbG9hZFZpZGVvRWxlbWVudCh2aWRlb0ZpbGUpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgLy8gTG9hZCB0aGUgZmlsZSBpbnRvIGFuIGh0bWwgZWxlbWVudFxyXG4gICAgICAgIGNvbnN0IHZpZGVvID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInZpZGVvXCIpO1xyXG5cclxuICAgICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cclxuICAgICAgICByZWFkZXIub25sb2FkID0gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICB2aWRlby5zcmMgPSBlLnRhcmdldC5yZXN1bHQ7XHJcblxyXG4gICAgICAgICAgICAvLyBPbmNlIHJlYWR5LCByZXR1cm5zIGl0cyBzaXplXHJcbiAgICAgICAgICAgIC8vIFdhaXQgdW50aWwgd2UgaGF2ZSBlbm91Z2ggZGF0YSB0byB0aHVtYm5haWwgdGhlIGZpcnN0IGZyYW1lLlxyXG4gICAgICAgICAgICB2aWRlby5vbmxvYWRlZGRhdGEgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUodmlkZW8pO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB2aWRlby5vbmVycm9yID0gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0KGUpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmVhZGVyLm9uZXJyb3IgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIHJlamVjdChlKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKHZpZGVvRmlsZSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlYWQgdGhlIG1ldGFkYXRhIGZvciBhIHZpZGVvIGZpbGUgYW5kIGNyZWF0ZSBhbmQgdXBsb2FkIGEgdGh1bWJuYWlsIG9mIHRoZSB2aWRlby5cclxuICpcclxuICogQHBhcmFtIHtNYXRyaXhDbGllbnR9IG1hdHJpeENsaWVudCBBIG1hdHJpeENsaWVudCB0byB1cGxvYWQgdGhlIHRodW1ibmFpbCB3aXRoLlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSBJRCBvZiB0aGUgcm9vbSB0aGUgdmlkZW8gd2lsbCBiZSB1cGxvYWRlZCB0by5cclxuICogQHBhcmFtIHtGaWxlfSB2aWRlb0ZpbGUgVGhlIHZpZGVvIHRvIHJlYWQgYW5kIHRodW1ibmFpbC5cclxuICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCB0aGUgYXR0YWNobWVudCBpbmZvLlxyXG4gKi9cclxuZnVuY3Rpb24gaW5mb0ZvclZpZGVvRmlsZShtYXRyaXhDbGllbnQsIHJvb21JZCwgdmlkZW9GaWxlKSB7XHJcbiAgICBjb25zdCB0aHVtYm5haWxUeXBlID0gXCJpbWFnZS9qcGVnXCI7XHJcblxyXG4gICAgbGV0IHZpZGVvSW5mbztcclxuICAgIHJldHVybiBsb2FkVmlkZW9FbGVtZW50KHZpZGVvRmlsZSkudGhlbihmdW5jdGlvbih2aWRlbykge1xyXG4gICAgICAgIHJldHVybiBjcmVhdGVUaHVtYm5haWwodmlkZW8sIHZpZGVvLnZpZGVvV2lkdGgsIHZpZGVvLnZpZGVvSGVpZ2h0LCB0aHVtYm5haWxUeXBlKTtcclxuICAgIH0pLnRoZW4oZnVuY3Rpb24ocmVzdWx0KSB7XHJcbiAgICAgICAgdmlkZW9JbmZvID0gcmVzdWx0LmluZm87XHJcbiAgICAgICAgcmV0dXJuIHVwbG9hZEZpbGUobWF0cml4Q2xpZW50LCByb29tSWQsIHJlc3VsdC50aHVtYm5haWwpO1xyXG4gICAgfSkudGhlbihmdW5jdGlvbihyZXN1bHQpIHtcclxuICAgICAgICB2aWRlb0luZm8udGh1bWJuYWlsX3VybCA9IHJlc3VsdC51cmw7XHJcbiAgICAgICAgdmlkZW9JbmZvLnRodW1ibmFpbF9maWxlID0gcmVzdWx0LmZpbGU7XHJcbiAgICAgICAgcmV0dXJuIHZpZGVvSW5mbztcclxuICAgIH0pO1xyXG59XHJcblxyXG4vKipcclxuICogUmVhZCB0aGUgZmlsZSBhcyBhbiBBcnJheUJ1ZmZlci5cclxuICogQHBhcmFtIHtGaWxlfSBmaWxlIFRoZSBmaWxlIHRvIHJlYWRcclxuICogQHJldHVybiB7UHJvbWlzZX0gQSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCBhbiBBcnJheUJ1ZmZlciB3aGVuIHRoZSBmaWxlXHJcbiAqICAgaXMgcmVhZC5cclxuICovXHJcbmZ1bmN0aW9uIHJlYWRGaWxlQXNBcnJheUJ1ZmZlcihmaWxlKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgcmVzb2x2ZShlLnRhcmdldC5yZXN1bHQpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmVhZGVyLm9uZXJyb3IgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIHJlamVjdChlKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJlYWRlci5yZWFkQXNBcnJheUJ1ZmZlcihmaWxlKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG4vKipcclxuICogVXBsb2FkIHRoZSBmaWxlIHRvIHRoZSBjb250ZW50IHJlcG9zaXRvcnkuXHJcbiAqIElmIHRoZSByb29tIGlzIGVuY3J5cHRlZCB0aGVuIGVuY3J5cHQgdGhlIGZpbGUgYmVmb3JlIHVwbG9hZGluZy5cclxuICpcclxuICogQHBhcmFtIHtNYXRyaXhDbGllbnR9IG1hdHJpeENsaWVudCBUaGUgbWF0cml4IGNsaWVudCB0byB1cGxvYWQgdGhlIGZpbGUgd2l0aC5cclxuICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgSUQgb2YgdGhlIHJvb20gYmVpbmcgdXBsb2FkZWQgdG8uXHJcbiAqIEBwYXJhbSB7RmlsZX0gZmlsZSBUaGUgZmlsZSB0byB1cGxvYWQuXHJcbiAqIEBwYXJhbSB7RnVuY3Rpb24/fSBwcm9ncmVzc0hhbmRsZXIgb3B0aW9uYWwgY2FsbGJhY2sgdG8gYmUgY2FsbGVkIHdoZW4gYSBjaHVuayBvZlxyXG4gKiAgICBkYXRhIGlzIHVwbG9hZGVkLlxyXG4gKiBAcmV0dXJuIHtQcm9taXNlfSBBIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIGFuIG9iamVjdC5cclxuICogIElmIHRoZSBmaWxlIGlzIHVuZW5jcnlwdGVkIHRoZW4gdGhlIG9iamVjdCB3aWxsIGhhdmUgYSBcInVybFwiIGtleS5cclxuICogIElmIHRoZSBmaWxlIGlzIGVuY3J5cHRlZCB0aGVuIHRoZSBvYmplY3Qgd2lsbCBoYXZlIGEgXCJmaWxlXCIga2V5LlxyXG4gKi9cclxuZnVuY3Rpb24gdXBsb2FkRmlsZShtYXRyaXhDbGllbnQsIHJvb21JZCwgZmlsZSwgcHJvZ3Jlc3NIYW5kbGVyKSB7XHJcbiAgICBpZiAobWF0cml4Q2xpZW50LmlzUm9vbUVuY3J5cHRlZChyb29tSWQpKSB7XHJcbiAgICAgICAgLy8gSWYgdGhlIHJvb20gaXMgZW5jcnlwdGVkIHRoZW4gZW5jcnlwdCB0aGUgZmlsZSBiZWZvcmUgdXBsb2FkaW5nIGl0LlxyXG4gICAgICAgIC8vIEZpcnN0IHJlYWQgdGhlIGZpbGUgaW50byBtZW1vcnkuXHJcbiAgICAgICAgbGV0IGNhbmNlbGVkID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IHVwbG9hZFByb21pc2U7XHJcbiAgICAgICAgbGV0IGVuY3J5cHRJbmZvO1xyXG4gICAgICAgIGNvbnN0IHByb20gPSByZWFkRmlsZUFzQXJyYXlCdWZmZXIoZmlsZSkudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgICAgIGlmIChjYW5jZWxlZCkgdGhyb3cgbmV3IFVwbG9hZENhbmNlbGVkRXJyb3IoKTtcclxuICAgICAgICAgICAgLy8gVGhlbiBlbmNyeXB0IHRoZSBmaWxlLlxyXG4gICAgICAgICAgICByZXR1cm4gZW5jcnlwdC5lbmNyeXB0QXR0YWNobWVudChkYXRhKTtcclxuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKGVuY3J5cHRSZXN1bHQpIHtcclxuICAgICAgICAgICAgaWYgKGNhbmNlbGVkKSB0aHJvdyBuZXcgVXBsb2FkQ2FuY2VsZWRFcnJvcigpO1xyXG4gICAgICAgICAgICAvLyBSZWNvcmQgdGhlIGluZm9ybWF0aW9uIG5lZWRlZCB0byBkZWNyeXB0IHRoZSBhdHRhY2htZW50LlxyXG4gICAgICAgICAgICBlbmNyeXB0SW5mbyA9IGVuY3J5cHRSZXN1bHQuaW5mbztcclxuICAgICAgICAgICAgLy8gUGFzcyB0aGUgZW5jcnlwdGVkIGRhdGEgYXMgYSBCbG9iIHRvIHRoZSB1cGxvYWRlci5cclxuICAgICAgICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtlbmNyeXB0UmVzdWx0LmRhdGFdKTtcclxuICAgICAgICAgICAgdXBsb2FkUHJvbWlzZSA9IG1hdHJpeENsaWVudC51cGxvYWRDb250ZW50KGJsb2IsIHtcclxuICAgICAgICAgICAgICAgIHByb2dyZXNzSGFuZGxlcjogcHJvZ3Jlc3NIYW5kbGVyLFxyXG4gICAgICAgICAgICAgICAgaW5jbHVkZUZpbGVuYW1lOiBmYWxzZSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdXBsb2FkUHJvbWlzZTtcclxuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKHVybCkge1xyXG4gICAgICAgICAgICAvLyBJZiB0aGUgYXR0YWNobWVudCBpcyBlbmNyeXB0ZWQgdGhlbiBidW5kbGUgdGhlIFVSTCBhbG9uZ1xyXG4gICAgICAgICAgICAvLyB3aXRoIHRoZSBpbmZvcm1hdGlvbiBuZWVkZWQgdG8gZGVjcnlwdCB0aGUgYXR0YWNobWVudCBhbmRcclxuICAgICAgICAgICAgLy8gYWRkIGl0IHVuZGVyIGEgZmlsZSBrZXkuXHJcbiAgICAgICAgICAgIGVuY3J5cHRJbmZvLnVybCA9IHVybDtcclxuICAgICAgICAgICAgaWYgKGZpbGUudHlwZSkge1xyXG4gICAgICAgICAgICAgICAgZW5jcnlwdEluZm8ubWltZXR5cGUgPSBmaWxlLnR5cGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHtcImZpbGVcIjogZW5jcnlwdEluZm99O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHByb20uYWJvcnQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNhbmNlbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYgKHVwbG9hZFByb21pc2UpIE1hdHJpeENsaWVudFBlZy5nZXQoKS5jYW5jZWxVcGxvYWQodXBsb2FkUHJvbWlzZSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gcHJvbTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgYmFzZVByb21pc2UgPSBtYXRyaXhDbGllbnQudXBsb2FkQ29udGVudChmaWxlLCB7XHJcbiAgICAgICAgICAgIHByb2dyZXNzSGFuZGxlcjogcHJvZ3Jlc3NIYW5kbGVyLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UxID0gYmFzZVByb21pc2UudGhlbihmdW5jdGlvbih1cmwpIHtcclxuICAgICAgICAgICAgLy8gSWYgdGhlIGF0dGFjaG1lbnQgaXNuJ3QgZW5jcnlwdGVkIHRoZW4gaW5jbHVkZSB0aGUgVVJMIGRpcmVjdGx5LlxyXG4gICAgICAgICAgICByZXR1cm4ge1widXJsXCI6IHVybH07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gWFhYOiBjb3B5IG92ZXIgdGhlIGFib3J0IG1ldGhvZCB0byB0aGUgbmV3IHByb21pc2VcclxuICAgICAgICBwcm9taXNlMS5hYm9ydCA9IGJhc2VQcm9taXNlLmFib3J0O1xyXG4gICAgICAgIHJldHVybiBwcm9taXNlMTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29udGVudE1lc3NhZ2VzIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuaW5wcm9ncmVzcyA9IFtdO1xyXG4gICAgICAgIHRoaXMubmV4dElkID0gMDtcclxuICAgICAgICB0aGlzLl9tZWRpYUNvbmZpZyA9IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHNoYXJlZEluc3RhbmNlKCkge1xyXG4gICAgICAgIGlmIChnbG9iYWwubXhfQ29udGVudE1lc3NhZ2VzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgZ2xvYmFsLm14X0NvbnRlbnRNZXNzYWdlcyA9IG5ldyBDb250ZW50TWVzc2FnZXMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGdsb2JhbC5teF9Db250ZW50TWVzc2FnZXM7XHJcbiAgICB9XHJcblxyXG4gICAgX2lzRmlsZVNpemVBY2NlcHRhYmxlKGZpbGUpIHtcclxuICAgICAgICBpZiAodGhpcy5fbWVkaWFDb25maWcgIT09IG51bGwgJiZcclxuICAgICAgICAgICAgdGhpcy5fbWVkaWFDb25maWdbXCJtLnVwbG9hZC5zaXplXCJdICE9PSB1bmRlZmluZWQgJiZcclxuICAgICAgICAgICAgZmlsZS5zaXplID4gdGhpcy5fbWVkaWFDb25maWdbXCJtLnVwbG9hZC5zaXplXCJdKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgX2Vuc3VyZU1lZGlhQ29uZmlnRmV0Y2hlZCgpIHtcclxuICAgICAgICBpZiAodGhpcy5fbWVkaWFDb25maWcgIT09IG51bGwpIHJldHVybjtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJbTWVkaWEgQ29uZmlnXSBGZXRjaGluZ1wiKTtcclxuICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldE1lZGlhQ29uZmlnKCkudGhlbigoY29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiW01lZGlhIENvbmZpZ10gRmV0Y2hlZCBjb25maWc6XCIsIGNvbmZpZyk7XHJcbiAgICAgICAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICAgICAgfSkuY2F0Y2goKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBNZWRpYSByZXBvIGNhbid0IG9yIHdvbid0IHJlcG9ydCBsaW1pdHMsIHNvIHByb3ZpZGUgYW4gZW1wdHkgb2JqZWN0IChubyBsaW1pdHMpLlxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIltNZWRpYSBDb25maWddIENvdWxkIG5vdCBmZXRjaCBjb25maWcsIHNvIG5vdCBsaW1pdGluZyB1cGxvYWRzLlwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgICAgIH0pLnRoZW4oKGNvbmZpZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9tZWRpYUNvbmZpZyA9IGNvbmZpZztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZW5kU3RpY2tlckNvbnRlbnRUb1Jvb20odXJsLCByb29tSWQsIGluZm8sIHRleHQsIG1hdHJpeENsaWVudCkge1xyXG4gICAgICAgIHJldHVybiBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2VuZFN0aWNrZXJNZXNzYWdlKHJvb21JZCwgdXJsLCBpbmZvLCB0ZXh0KS5jYXRjaCgoZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oYEZhaWxlZCB0byBzZW5kIGNvbnRlbnQgd2l0aCBVUkwgJHt1cmx9IHRvIHJvb20gJHtyb29tSWR9YCwgZSk7XHJcbiAgICAgICAgICAgIHRocm93IGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXBsb2FkTGltaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX21lZGlhQ29uZmlnICE9PSBudWxsICYmIHRoaXMuX21lZGlhQ29uZmlnW1wibS51cGxvYWQuc2l6ZVwiXSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9tZWRpYUNvbmZpZ1tcIm0udXBsb2FkLnNpemVcIl07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHNlbmRDb250ZW50TGlzdFRvUm9vbShmaWxlcywgcm9vbUlkLCBtYXRyaXhDbGllbnQpIHtcclxuICAgICAgICBpZiAobWF0cml4Q2xpZW50LmlzR3Vlc3QoKSkge1xyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ3JlcXVpcmVfcmVnaXN0cmF0aW9uJ30pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBpc1F1b3RpbmcgPSBCb29sZWFuKFJvb21WaWV3U3RvcmUuZ2V0UXVvdGluZ0V2ZW50KCkpO1xyXG4gICAgICAgIGlmIChpc1F1b3RpbmcpIHtcclxuICAgICAgICAgICAgY29uc3QgUXVlc3Rpb25EaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5RdWVzdGlvbkRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc3Qgc2hvdWxkVXBsb2FkID0gYXdhaXQgbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1VwbG9hZCBSZXBseSBXYXJuaW5nJywgJycsIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdSZXBseWluZyBXaXRoIEZpbGVzJyksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXQgdGhpcyB0aW1lIGl0IGlzIG5vdCBwb3NzaWJsZSB0byByZXBseSB3aXRoIGEgZmlsZS4gJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnV291bGQgeW91IGxpa2UgdG8gdXBsb2FkIHRoaXMgZmlsZSB3aXRob3V0IHJlcGx5aW5nPycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICl9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICBoYXNDYW5jZWxCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uOiBfdChcIkNvbnRpbnVlXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IChzaG91bGRVcGxvYWQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShzaG91bGRVcGxvYWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGlmICghc2hvdWxkVXBsb2FkKSByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhd2FpdCB0aGlzLl9lbnN1cmVNZWRpYUNvbmZpZ0ZldGNoZWQoKTtcclxuXHJcbiAgICAgICAgY29uc3QgdG9vQmlnRmlsZXMgPSBbXTtcclxuICAgICAgICBjb25zdCBva0ZpbGVzID0gW107XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZXMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2lzRmlsZVNpemVBY2NlcHRhYmxlKGZpbGVzW2ldKSkge1xyXG4gICAgICAgICAgICAgICAgb2tGaWxlcy5wdXNoKGZpbGVzW2ldKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRvb0JpZ0ZpbGVzLnB1c2goZmlsZXNbaV0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodG9vQmlnRmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBVcGxvYWRGYWlsdXJlRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuVXBsb2FkRmFpbHVyZURpYWxvZ1wiKTtcclxuICAgICAgICAgICAgY29uc3QgdXBsb2FkRmFpbHVyZURpYWxvZ1Byb21pc2UgPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVXBsb2FkIEZhaWx1cmUnLCAnJywgVXBsb2FkRmFpbHVyZURpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhZEZpbGVzOiB0b29CaWdGaWxlcyxcclxuICAgICAgICAgICAgICAgICAgICB0b3RhbEZpbGVzOiBmaWxlcy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudE1lc3NhZ2VzOiB0aGlzLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uRmluaXNoZWQ6IChzaG91bGRDb250aW51ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHNob3VsZENvbnRpbnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBzaG91bGRDb250aW51ZSA9IGF3YWl0IHVwbG9hZEZhaWx1cmVEaWFsb2dQcm9taXNlO1xyXG4gICAgICAgICAgICBpZiAoIXNob3VsZENvbnRpbnVlKSByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBVcGxvYWRDb25maXJtRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuVXBsb2FkQ29uZmlybURpYWxvZ1wiKTtcclxuICAgICAgICBsZXQgdXBsb2FkQWxsID0gZmFsc2U7XHJcbiAgICAgICAgLy8gUHJvbWlzZSB0byBjb21wbGV0ZSBiZWZvcmUgc2VuZGluZyBuZXh0IGZpbGUgaW50byByb29tLCB1c2VkIGZvciBzeW5jaHJvbmlzYXRpb24gb2YgZmlsZS1zZW5kaW5nXHJcbiAgICAgICAgLy8gdG8gbWF0Y2ggdGhlIG9yZGVyIHRoZSBmaWxlcyB3ZXJlIHNwZWNpZmllZCBpblxyXG4gICAgICAgIGxldCBwcm9tQmVmb3JlID0gUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBva0ZpbGVzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGUgPSBva0ZpbGVzW2ldO1xyXG4gICAgICAgICAgICBpZiAoIXVwbG9hZEFsbCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2hvdWxkQ29udGludWUgPSBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1VwbG9hZCBGaWxlcyBjb25maXJtYXRpb24nLCAnJywgVXBsb2FkQ29uZmlybURpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50SW5kZXg6IGksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsRmlsZXM6IG9rRmlsZXMubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiAoc2hvdWxkQ29udGludWUsIHNob3VsZFVwbG9hZEFsbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNob3VsZFVwbG9hZEFsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVwbG9hZEFsbCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHNob3VsZENvbnRpbnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFzaG91bGRDb250aW51ZSkgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcHJvbUJlZm9yZSA9IHRoaXMuX3NlbmRDb250ZW50VG9Sb29tKGZpbGUsIHJvb21JZCwgbWF0cml4Q2xpZW50LCBwcm9tQmVmb3JlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX3NlbmRDb250ZW50VG9Sb29tKGZpbGUsIHJvb21JZCwgbWF0cml4Q2xpZW50LCBwcm9tQmVmb3JlKSB7XHJcbiAgICAgICAgY29uc3QgY29udGVudCA9IHtcclxuICAgICAgICAgICAgYm9keTogZmlsZS5uYW1lIHx8ICdBdHRhY2htZW50JyxcclxuICAgICAgICAgICAgaW5mbzoge1xyXG4gICAgICAgICAgICAgICAgc2l6ZTogZmlsZS5zaXplLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIGlmIHdlIGhhdmUgYSBtaW1lIHR5cGUgZm9yIHRoZSBmaWxlLCBhZGQgaXQgdG8gdGhlIG1lc3NhZ2UgbWV0YWRhdGFcclxuICAgICAgICBpZiAoZmlsZS50eXBlKSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuaW5mby5taW1ldHlwZSA9IGZpbGUudHlwZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHByb20gPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZmlsZS50eXBlLmluZGV4T2YoJ2ltYWdlLycpID09IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQubXNndHlwZSA9ICdtLmltYWdlJztcclxuICAgICAgICAgICAgICAgIGluZm9Gb3JJbWFnZUZpbGUobWF0cml4Q2xpZW50LCByb29tSWQsIGZpbGUpLnRoZW4oKGltYWdlSW5mbyk9PntcclxuICAgICAgICAgICAgICAgICAgICBleHRlbmQoY29udGVudC5pbmZvLCBpbWFnZUluZm8pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIH0sIChlcnJvcik9PntcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50Lm1zZ3R5cGUgPSAnbS5maWxlJztcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlLnR5cGUuaW5kZXhPZignYXVkaW8vJykgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgY29udGVudC5tc2d0eXBlID0gJ20uYXVkaW8nO1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGUudHlwZS5pbmRleE9mKCd2aWRlby8nKSA9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50Lm1zZ3R5cGUgPSAnbS52aWRlbyc7XHJcbiAgICAgICAgICAgICAgICBpbmZvRm9yVmlkZW9GaWxlKG1hdHJpeENsaWVudCwgcm9vbUlkLCBmaWxlKS50aGVuKCh2aWRlb0luZm8pPT57XHJcbiAgICAgICAgICAgICAgICAgICAgZXh0ZW5kKGNvbnRlbnQuaW5mbywgdmlkZW9JbmZvKTtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAoZXJyb3IpPT57XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudC5tc2d0eXBlID0gJ20uZmlsZSc7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50Lm1zZ3R5cGUgPSAnbS5maWxlJztcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCB1cGxvYWQgPSB7XHJcbiAgICAgICAgICAgIGZpbGVOYW1lOiBmaWxlLm5hbWUgfHwgJ0F0dGFjaG1lbnQnLFxyXG4gICAgICAgICAgICByb29tSWQ6IHJvb21JZCxcclxuICAgICAgICAgICAgdG90YWw6IDAsXHJcbiAgICAgICAgICAgIGxvYWRlZDogMCxcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuaW5wcm9ncmVzcy5wdXNoKHVwbG9hZCk7XHJcbiAgICAgICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd1cGxvYWRfc3RhcnRlZCd9KTtcclxuXHJcbiAgICAgICAgLy8gRm9jdXMgdGhlIGNvbXBvc2VyIHZpZXdcclxuICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ2ZvY3VzX2NvbXBvc2VyJ30pO1xyXG5cclxuICAgICAgICBsZXQgZXJyb3I7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIG9uUHJvZ3Jlc3MoZXYpIHtcclxuICAgICAgICAgICAgdXBsb2FkLnRvdGFsID0gZXYudG90YWw7XHJcbiAgICAgICAgICAgIHVwbG9hZC5sb2FkZWQgPSBldi5sb2FkZWQ7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndXBsb2FkX3Byb2dyZXNzJywgdXBsb2FkOiB1cGxvYWR9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBwcm9tLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIC8vIFhYWDogdXBsb2FkLnByb21pc2UgbXVzdCBiZSB0aGUgcHJvbWlzZSB0aGF0XHJcbiAgICAgICAgICAgIC8vIGlzIHJldHVybmVkIGJ5IHVwbG9hZEZpbGUgYXMgaXQgaGFzIGFuIGFib3J0KClcclxuICAgICAgICAgICAgLy8gbWV0aG9kIGhhY2tlZCBvbnRvIGl0LlxyXG4gICAgICAgICAgICB1cGxvYWQucHJvbWlzZSA9IHVwbG9hZEZpbGUoXHJcbiAgICAgICAgICAgICAgICBtYXRyaXhDbGllbnQsIHJvb21JZCwgZmlsZSwgb25Qcm9ncmVzcyxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwbG9hZC5wcm9taXNlLnRoZW4oZnVuY3Rpb24ocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50LmZpbGUgPSByZXN1bHQuZmlsZTtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQudXJsID0gcmVzdWx0LnVybDtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSkudGhlbigodXJsKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEF3YWl0IHByZXZpb3VzIG1lc3NhZ2UgYmVpbmcgc2VudCBpbnRvIHRoZSByb29tXHJcbiAgICAgICAgICAgIHJldHVybiBwcm9tQmVmb3JlO1xyXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtYXRyaXhDbGllbnQuc2VuZE1lc3NhZ2Uocm9vbUlkLCBjb250ZW50KTtcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgZXJyb3IgPSBlcnI7XHJcbiAgICAgICAgICAgIGlmICghdXBsb2FkLmNhbmNlbGVkKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVzYyA9IF90KFwiVGhlIGZpbGUgJyUoZmlsZU5hbWUpcycgZmFpbGVkIHRvIHVwbG9hZC5cIiwge2ZpbGVOYW1lOiB1cGxvYWQuZmlsZU5hbWV9KTtcclxuICAgICAgICAgICAgICAgIGlmIChlcnIuaHR0cF9zdGF0dXMgPT0gNDEzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVzYyA9IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlRoZSBmaWxlICclKGZpbGVOYW1lKXMnIGV4Y2VlZHMgdGhpcyBob21lc2VydmVyJ3Mgc2l6ZSBsaW1pdCBmb3IgdXBsb2Fkc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7ZmlsZU5hbWU6IHVwbG9hZC5maWxlTmFtZX0sXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuRXJyb3JEaWFsb2dcIik7XHJcbiAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdVcGxvYWQgZmFpbGVkJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdVcGxvYWQgRmFpbGVkJyksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGRlc2MsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpbnByb2dyZXNzS2V5cyA9IE9iamVjdC5rZXlzKHRoaXMuaW5wcm9ncmVzcyk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5pbnByb2dyZXNzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBrID0gaW5wcm9ncmVzc0tleXNbaV07XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pbnByb2dyZXNzW2tdLnByb21pc2UgPT09IHVwbG9hZC5wcm9taXNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbnByb2dyZXNzLnNwbGljZShrLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgIC8vIDQxMzogRmlsZSB3YXMgdG9vIGJpZyBvciB1cHNldCB0aGUgc2VydmVyIGluIHNvbWUgd2F5OlxyXG4gICAgICAgICAgICAgICAgLy8gY2xlYXIgdGhlIG1lZGlhIHNpemUgbGltaXQgc28gd2UgZmV0Y2ggaXQgYWdhaW4gbmV4dCB0aW1lXHJcbiAgICAgICAgICAgICAgICAvLyB3ZSB0cnkgdG8gdXBsb2FkXHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IgJiYgZXJyb3IuaHR0cF9zdGF0dXMgPT09IDQxMykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX21lZGlhQ29uZmlnID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndXBsb2FkX2ZhaWxlZCcsIHVwbG9hZCwgZXJyb3J9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndXBsb2FkX2ZpbmlzaGVkJywgdXBsb2FkfSk7XHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ21lc3NhZ2Vfc2VudCd9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVcGxvYWRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlucHJvZ3Jlc3MuZmlsdGVyKHUgPT4gIXUuY2FuY2VsZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbmNlbFVwbG9hZChwcm9taXNlKSB7XHJcbiAgICAgICAgY29uc3QgaW5wcm9ncmVzc0tleXMgPSBPYmplY3Qua2V5cyh0aGlzLmlucHJvZ3Jlc3MpO1xyXG4gICAgICAgIGxldCB1cGxvYWQ7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmlucHJvZ3Jlc3MubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgY29uc3QgayA9IGlucHJvZ3Jlc3NLZXlzW2ldO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pbnByb2dyZXNzW2tdLnByb21pc2UgPT09IHByb21pc2UpIHtcclxuICAgICAgICAgICAgICAgIHVwbG9hZCA9IHRoaXMuaW5wcm9ncmVzc1trXTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh1cGxvYWQpIHtcclxuICAgICAgICAgICAgdXBsb2FkLmNhbmNlbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmNhbmNlbFVwbG9hZCh1cGxvYWQucHJvbWlzZSk7XHJcbiAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndXBsb2FkX2NhbmNlbGVkJywgdXBsb2FkfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==