"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storeRoomAliasInCache = storeRoomAliasInCache;
exports.getCachedRoomIDForAlias = getCachedRoomIDForAlias;

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is meant to be a cache of room alias to room ID so that moving between
 * rooms happens smoothly (for example using browser back / forward buttons).
 *
 * For the moment, it's in memory only and so only applies for the current
 * session for simplicity, but could be extended further in the future.
 *
 * A similar thing could also be achieved via `pushState` with a state object,
 * but keeping it separate like this seems easier in case we do want to extend.
 */
const aliasToIDMap = new Map();

function storeRoomAliasInCache(alias, id) {
  aliasToIDMap.set(alias, id);
}

function getCachedRoomIDForAlias(alias) {
  return aliasToIDMap.get(alias);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Sb29tQWxpYXNDYWNoZS5qcyJdLCJuYW1lcyI6WyJhbGlhc1RvSURNYXAiLCJNYXAiLCJzdG9yZVJvb21BbGlhc0luQ2FjaGUiLCJhbGlhcyIsImlkIiwic2V0IiwiZ2V0Q2FjaGVkUm9vbUlERm9yQWxpYXMiLCJnZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7Ozs7Ozs7Ozs7QUFVQSxNQUFNQSxZQUFZLEdBQUcsSUFBSUMsR0FBSixFQUFyQjs7QUFFTyxTQUFTQyxxQkFBVCxDQUErQkMsS0FBL0IsRUFBc0NDLEVBQXRDLEVBQTBDO0FBQzdDSixFQUFBQSxZQUFZLENBQUNLLEdBQWIsQ0FBaUJGLEtBQWpCLEVBQXdCQyxFQUF4QjtBQUNIOztBQUVNLFNBQVNFLHVCQUFULENBQWlDSCxLQUFqQyxFQUF3QztBQUMzQyxTQUFPSCxZQUFZLENBQUNPLEdBQWIsQ0FBaUJKLEtBQWpCLENBQVA7QUFDSCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLyoqXHJcbiAqIFRoaXMgaXMgbWVhbnQgdG8gYmUgYSBjYWNoZSBvZiByb29tIGFsaWFzIHRvIHJvb20gSUQgc28gdGhhdCBtb3ZpbmcgYmV0d2VlblxyXG4gKiByb29tcyBoYXBwZW5zIHNtb290aGx5IChmb3IgZXhhbXBsZSB1c2luZyBicm93c2VyIGJhY2sgLyBmb3J3YXJkIGJ1dHRvbnMpLlxyXG4gKlxyXG4gKiBGb3IgdGhlIG1vbWVudCwgaXQncyBpbiBtZW1vcnkgb25seSBhbmQgc28gb25seSBhcHBsaWVzIGZvciB0aGUgY3VycmVudFxyXG4gKiBzZXNzaW9uIGZvciBzaW1wbGljaXR5LCBidXQgY291bGQgYmUgZXh0ZW5kZWQgZnVydGhlciBpbiB0aGUgZnV0dXJlLlxyXG4gKlxyXG4gKiBBIHNpbWlsYXIgdGhpbmcgY291bGQgYWxzbyBiZSBhY2hpZXZlZCB2aWEgYHB1c2hTdGF0ZWAgd2l0aCBhIHN0YXRlIG9iamVjdCxcclxuICogYnV0IGtlZXBpbmcgaXQgc2VwYXJhdGUgbGlrZSB0aGlzIHNlZW1zIGVhc2llciBpbiBjYXNlIHdlIGRvIHdhbnQgdG8gZXh0ZW5kLlxyXG4gKi9cclxuY29uc3QgYWxpYXNUb0lETWFwID0gbmV3IE1hcCgpO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHN0b3JlUm9vbUFsaWFzSW5DYWNoZShhbGlhcywgaWQpIHtcclxuICAgIGFsaWFzVG9JRE1hcC5zZXQoYWxpYXMsIGlkKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldENhY2hlZFJvb21JREZvckFsaWFzKGFsaWFzKSB7XHJcbiAgICByZXR1cm4gYWxpYXNUb0lETWFwLmdldChhbGlhcyk7XHJcbn1cclxuIl19