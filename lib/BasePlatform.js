"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _matrixJsSdk = require("matrix-js-sdk");

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var _BaseEventIndexManager = _interopRequireDefault(require("./indexing/BaseEventIndexManager"));

/*
Copyright 2016 Aviral Dasgupta
Copyright 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Base class for classes that provide platform-specific functionality
 * eg. Setting an application badge or displaying notifications
 *
 * Instances of this class are provided by the application.
 */
class BasePlatform {
  constructor() {
    this.notificationCount = 0;
    this.errorDidOccur = false;

    _dispatcher.default.register(this._onAction.bind(this));
  }

  _onAction(payload
  /*: Object*/
  ) {
    switch (payload.action) {
      case 'on_client_not_viable':
      case 'on_logged_out':
        this.setNotificationCount(0);
        break;
    }
  } // Used primarily for Analytics


  getHumanReadableName()
  /*: string*/
  {
    return 'Base Platform';
  }

  setNotificationCount(count
  /*: number*/
  ) {
    this.notificationCount = count;
  }

  setErrorStatus(errorDidOccur
  /*: boolean*/
  ) {
    this.errorDidOccur = errorDidOccur;
  }
  /**
   * Returns true if the platform supports displaying
   * notifications, otherwise false.
   * @returns {boolean} whether the platform supports displaying notifications
   */


  supportsNotifications()
  /*: boolean*/
  {
    return false;
  }
  /**
   * Returns true if the application currently has permission
   * to display notifications. Otherwise false.
   * @returns {boolean} whether the application has permission to display notifications
   */


  maySendNotifications()
  /*: boolean*/
  {
    return false;
  }
  /**
   * Requests permission to send notifications. Returns
   * a promise that is resolved when the user has responded
   * to the request. The promise has a single string argument
   * that is 'granted' if the user allowed the request or
   * 'denied' otherwise.
   */


  requestNotificationPermission()
  /*: Promise<string>*/
  {}

  displayNotification(title
  /*: string*/
  , msg
  /*: string*/
  , avatarUrl
  /*: string*/
  , room
  /*: Object*/
  ) {}

  loudNotification(ev
  /*: Event*/
  , room
  /*: Object*/
  ) {}
  /**
   * Returns a promise that resolves to a string representing
   * the current version of the application.
   */


  getAppVersion()
  /*: Promise<string>*/
  {
    throw new Error("getAppVersion not implemented!");
  }
  /*
   * If it's not expected that capturing the screen will work
   * with getUserMedia, return a string explaining why not.
   * Otherwise, return null.
   */


  screenCaptureErrorString()
  /*: string*/
  {
    return "Not implemented";
  }
  /**
   * Restarts the application, without neccessarily reloading
   * any application code
   */


  reload() {
    throw new Error("reload not implemented!");
  }

  supportsAutoLaunch()
  /*: boolean*/
  {
    return false;
  } // XXX: Surely this should be a setting like any other?


  async getAutoLaunchEnabled()
  /*: boolean*/
  {
    return false;
  }

  async setAutoLaunchEnabled(enabled
  /*: boolean*/
  )
  /*: void*/
  {
    throw new Error("Unimplemented");
  }

  supportsAutoHideMenuBar()
  /*: boolean*/
  {
    return false;
  }

  async getAutoHideMenuBarEnabled()
  /*: boolean*/
  {
    return false;
  }

  async setAutoHideMenuBarEnabled(enabled
  /*: boolean*/
  )
  /*: void*/
  {
    throw new Error("Unimplemented");
  }

  supportsMinimizeToTray()
  /*: boolean*/
  {
    return false;
  }

  async getMinimizeToTrayEnabled()
  /*: boolean*/
  {
    return false;
  }

  async setMinimizeToTrayEnabled(enabled
  /*: boolean*/
  )
  /*: void*/
  {
    throw new Error("Unimplemented");
  }
  /**
   * Get our platform specific EventIndexManager.
   *
   * @return {BaseEventIndexManager} The EventIndex manager for our platform,
   * can be null if the platform doesn't support event indexing.
   */


  getEventIndexingManager()
  /*: BaseEventIndexManager | null*/
  {
    return null;
  }

  setLanguage(preferredLangs
  /*: string[]*/
  ) {}

  getSSOCallbackUrl(hsUrl
  /*: string*/
  , isUrl
  /*: string*/
  )
  /*: URL*/
  {
    const url = new URL(window.location.href); // XXX: at this point, the fragment will always be #/login, which is no
    // use to anyone. Ideally, we would get the intended fragment from
    // MatrixChat.screenAfterLogin so that you could follow #/room links etc
    // through an SSO login.

    url.hash = "";
    url.searchParams.set("homeserver", hsUrl);
    url.searchParams.set("identityServer", isUrl);
    return url;
  }
  /**
   * Begin Single Sign On flows.
   * @param {MatrixClient} mxClient the matrix client using which we should start the flow
   * @param {"sso"|"cas"} loginType the type of SSO it is, CAS/SSO.
   */


  startSingleSignOn(mxClient
  /*: MatrixClient*/
  , loginType
  /*: "sso"|"cas"*/
  ) {
    const callbackUrl = this.getSSOCallbackUrl(mxClient.getHomeserverUrl(), mxClient.getIdentityServerUrl());
    window.location.href = mxClient.getSsoLoginUrl(callbackUrl.toString(), loginType); // redirect to SSO
  }

}

exports.default = BasePlatform;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9CYXNlUGxhdGZvcm0uanMiXSwibmFtZXMiOlsiQmFzZVBsYXRmb3JtIiwiY29uc3RydWN0b3IiLCJub3RpZmljYXRpb25Db3VudCIsImVycm9yRGlkT2NjdXIiLCJkaXMiLCJyZWdpc3RlciIsIl9vbkFjdGlvbiIsImJpbmQiLCJwYXlsb2FkIiwiYWN0aW9uIiwic2V0Tm90aWZpY2F0aW9uQ291bnQiLCJnZXRIdW1hblJlYWRhYmxlTmFtZSIsImNvdW50Iiwic2V0RXJyb3JTdGF0dXMiLCJzdXBwb3J0c05vdGlmaWNhdGlvbnMiLCJtYXlTZW5kTm90aWZpY2F0aW9ucyIsInJlcXVlc3ROb3RpZmljYXRpb25QZXJtaXNzaW9uIiwiZGlzcGxheU5vdGlmaWNhdGlvbiIsInRpdGxlIiwibXNnIiwiYXZhdGFyVXJsIiwicm9vbSIsImxvdWROb3RpZmljYXRpb24iLCJldiIsImdldEFwcFZlcnNpb24iLCJFcnJvciIsInNjcmVlbkNhcHR1cmVFcnJvclN0cmluZyIsInJlbG9hZCIsInN1cHBvcnRzQXV0b0xhdW5jaCIsImdldEF1dG9MYXVuY2hFbmFibGVkIiwic2V0QXV0b0xhdW5jaEVuYWJsZWQiLCJlbmFibGVkIiwic3VwcG9ydHNBdXRvSGlkZU1lbnVCYXIiLCJnZXRBdXRvSGlkZU1lbnVCYXJFbmFibGVkIiwic2V0QXV0b0hpZGVNZW51QmFyRW5hYmxlZCIsInN1cHBvcnRzTWluaW1pemVUb1RyYXkiLCJnZXRNaW5pbWl6ZVRvVHJheUVuYWJsZWQiLCJzZXRNaW5pbWl6ZVRvVHJheUVuYWJsZWQiLCJnZXRFdmVudEluZGV4aW5nTWFuYWdlciIsInNldExhbmd1YWdlIiwicHJlZmVycmVkTGFuZ3MiLCJnZXRTU09DYWxsYmFja1VybCIsImhzVXJsIiwiaXNVcmwiLCJ1cmwiLCJVUkwiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImhyZWYiLCJoYXNoIiwic2VhcmNoUGFyYW1zIiwic2V0Iiwic3RhcnRTaW5nbGVTaWduT24iLCJteENsaWVudCIsImxvZ2luVHlwZSIsImNhbGxiYWNrVXJsIiwiZ2V0SG9tZXNlcnZlclVybCIsImdldElkZW50aXR5U2VydmVyVXJsIiwiZ2V0U3NvTG9naW5VcmwiLCJ0b1N0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBcUJBOztBQUNBOztBQUNBOztBQXJCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQTs7Ozs7O0FBTWUsTUFBTUEsWUFBTixDQUFtQjtBQUM5QkMsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsU0FBS0MsaUJBQUwsR0FBeUIsQ0FBekI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCLEtBQXJCOztBQUVBQyx3QkFBSUMsUUFBSixDQUFhLEtBQUtDLFNBQUwsQ0FBZUMsSUFBZixDQUFvQixJQUFwQixDQUFiO0FBQ0g7O0FBRURELEVBQUFBLFNBQVMsQ0FBQ0U7QUFBRDtBQUFBLElBQWtCO0FBQ3ZCLFlBQVFBLE9BQU8sQ0FBQ0MsTUFBaEI7QUFDSSxXQUFLLHNCQUFMO0FBQ0EsV0FBSyxlQUFMO0FBQ0ksYUFBS0Msb0JBQUwsQ0FBMEIsQ0FBMUI7QUFDQTtBQUpSO0FBTUgsR0FmNkIsQ0FpQjlCOzs7QUFDQUMsRUFBQUEsb0JBQW9CO0FBQUE7QUFBVztBQUMzQixXQUFPLGVBQVA7QUFDSDs7QUFFREQsRUFBQUEsb0JBQW9CLENBQUNFO0FBQUQ7QUFBQSxJQUFnQjtBQUNoQyxTQUFLVixpQkFBTCxHQUF5QlUsS0FBekI7QUFDSDs7QUFFREMsRUFBQUEsY0FBYyxDQUFDVjtBQUFEO0FBQUEsSUFBeUI7QUFDbkMsU0FBS0EsYUFBTCxHQUFxQkEsYUFBckI7QUFDSDtBQUVEOzs7Ozs7O0FBS0FXLEVBQUFBLHFCQUFxQjtBQUFBO0FBQVk7QUFDN0IsV0FBTyxLQUFQO0FBQ0g7QUFFRDs7Ozs7OztBQUtBQyxFQUFBQSxvQkFBb0I7QUFBQTtBQUFZO0FBQzVCLFdBQU8sS0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9BQyxFQUFBQSw2QkFBNkI7QUFBQTtBQUFvQixHQUNoRDs7QUFFREMsRUFBQUEsbUJBQW1CLENBQUNDO0FBQUQ7QUFBQSxJQUFnQkM7QUFBaEI7QUFBQSxJQUE2QkM7QUFBN0I7QUFBQSxJQUFnREM7QUFBaEQ7QUFBQSxJQUE4RCxDQUNoRjs7QUFFREMsRUFBQUEsZ0JBQWdCLENBQUNDO0FBQUQ7QUFBQSxJQUFZRjtBQUFaO0FBQUEsSUFBMEIsQ0FDekM7QUFFRDs7Ozs7O0FBSUFHLEVBQUFBLGFBQWE7QUFBQTtBQUFvQjtBQUM3QixVQUFNLElBQUlDLEtBQUosQ0FBVSxnQ0FBVixDQUFOO0FBQ0g7QUFFRDs7Ozs7OztBQUtBQyxFQUFBQSx3QkFBd0I7QUFBQTtBQUFXO0FBQy9CLFdBQU8saUJBQVA7QUFDSDtBQUVEOzs7Ozs7QUFJQUMsRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTSxJQUFJRixLQUFKLENBQVUseUJBQVYsQ0FBTjtBQUNIOztBQUVERyxFQUFBQSxrQkFBa0I7QUFBQTtBQUFZO0FBQzFCLFdBQU8sS0FBUDtBQUNILEdBM0Y2QixDQTZGOUI7OztBQUNBLFFBQU1DLG9CQUFOO0FBQUE7QUFBc0M7QUFDbEMsV0FBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBTUMsb0JBQU4sQ0FBMkJDO0FBQTNCO0FBQUE7QUFBQTtBQUFtRDtBQUMvQyxVQUFNLElBQUlOLEtBQUosQ0FBVSxlQUFWLENBQU47QUFDSDs7QUFFRE8sRUFBQUEsdUJBQXVCO0FBQUE7QUFBWTtBQUMvQixXQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFNQyx5QkFBTjtBQUFBO0FBQTJDO0FBQ3ZDLFdBQU8sS0FBUDtBQUNIOztBQUVELFFBQU1DLHlCQUFOLENBQWdDSDtBQUFoQztBQUFBO0FBQUE7QUFBd0Q7QUFDcEQsVUFBTSxJQUFJTixLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0g7O0FBRURVLEVBQUFBLHNCQUFzQjtBQUFBO0FBQVk7QUFDOUIsV0FBTyxLQUFQO0FBQ0g7O0FBRUQsUUFBTUMsd0JBQU47QUFBQTtBQUEwQztBQUN0QyxXQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFNQyx3QkFBTixDQUErQk47QUFBL0I7QUFBQTtBQUFBO0FBQXVEO0FBQ25ELFVBQU0sSUFBSU4sS0FBSixDQUFVLGVBQVYsQ0FBTjtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUFhLEVBQUFBLHVCQUF1QjtBQUFBO0FBQWlDO0FBQ3BELFdBQU8sSUFBUDtBQUNIOztBQUVEQyxFQUFBQSxXQUFXLENBQUNDO0FBQUQ7QUFBQSxJQUEyQixDQUFFOztBQUV4Q0MsRUFBQUEsaUJBQWlCLENBQUNDO0FBQUQ7QUFBQSxJQUFnQkM7QUFBaEI7QUFBQTtBQUFBO0FBQW9DO0FBQ2pELFVBQU1DLEdBQUcsR0FBRyxJQUFJQyxHQUFKLENBQVFDLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsSUFBeEIsQ0FBWixDQURpRCxDQUVqRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQUosSUFBQUEsR0FBRyxDQUFDSyxJQUFKLEdBQVcsRUFBWDtBQUNBTCxJQUFBQSxHQUFHLENBQUNNLFlBQUosQ0FBaUJDLEdBQWpCLENBQXFCLFlBQXJCLEVBQW1DVCxLQUFuQztBQUNBRSxJQUFBQSxHQUFHLENBQUNNLFlBQUosQ0FBaUJDLEdBQWpCLENBQXFCLGdCQUFyQixFQUF1Q1IsS0FBdkM7QUFDQSxXQUFPQyxHQUFQO0FBQ0g7QUFFRDs7Ozs7OztBQUtBUSxFQUFBQSxpQkFBaUIsQ0FBQ0M7QUFBRDtBQUFBLElBQXlCQztBQUF6QjtBQUFBLElBQWlEO0FBQzlELFVBQU1DLFdBQVcsR0FBRyxLQUFLZCxpQkFBTCxDQUF1QlksUUFBUSxDQUFDRyxnQkFBVCxFQUF2QixFQUFvREgsUUFBUSxDQUFDSSxvQkFBVCxFQUFwRCxDQUFwQjtBQUNBWCxJQUFBQSxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCSyxRQUFRLENBQUNLLGNBQVQsQ0FBd0JILFdBQVcsQ0FBQ0ksUUFBWixFQUF4QixFQUFnREwsU0FBaEQsQ0FBdkIsQ0FGOEQsQ0FFcUI7QUFDdEY7O0FBOUo2QiIsInNvdXJjZXNDb250ZW50IjpbIi8vIEBmbG93XHJcblxyXG4vKlxyXG5Db3B5cmlnaHQgMjAxNiBBdmlyYWwgRGFzZ3VwdGFcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudH0gZnJvbSBcIm1hdHJpeC1qcy1zZGtcIjtcclxuaW1wb3J0IGRpcyBmcm9tICcuL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgQmFzZUV2ZW50SW5kZXhNYW5hZ2VyIGZyb20gJy4vaW5kZXhpbmcvQmFzZUV2ZW50SW5kZXhNYW5hZ2VyJztcclxuXHJcbi8qKlxyXG4gKiBCYXNlIGNsYXNzIGZvciBjbGFzc2VzIHRoYXQgcHJvdmlkZSBwbGF0Zm9ybS1zcGVjaWZpYyBmdW5jdGlvbmFsaXR5XHJcbiAqIGVnLiBTZXR0aW5nIGFuIGFwcGxpY2F0aW9uIGJhZGdlIG9yIGRpc3BsYXlpbmcgbm90aWZpY2F0aW9uc1xyXG4gKlxyXG4gKiBJbnN0YW5jZXMgb2YgdGhpcyBjbGFzcyBhcmUgcHJvdmlkZWQgYnkgdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZVBsYXRmb3JtIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uQ291bnQgPSAwO1xyXG4gICAgICAgIHRoaXMuZXJyb3JEaWRPY2N1ciA9IGZhbHNlO1xyXG5cclxuICAgICAgICBkaXMucmVnaXN0ZXIodGhpcy5fb25BY3Rpb24uYmluZCh0aGlzKSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uQWN0aW9uKHBheWxvYWQ6IE9iamVjdCkge1xyXG4gICAgICAgIHN3aXRjaCAocGF5bG9hZC5hY3Rpb24pIHtcclxuICAgICAgICAgICAgY2FzZSAnb25fY2xpZW50X25vdF92aWFibGUnOlxyXG4gICAgICAgICAgICBjYXNlICdvbl9sb2dnZWRfb3V0JzpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0Tm90aWZpY2F0aW9uQ291bnQoMCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVXNlZCBwcmltYXJpbHkgZm9yIEFuYWx5dGljc1xyXG4gICAgZ2V0SHVtYW5SZWFkYWJsZU5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gJ0Jhc2UgUGxhdGZvcm0nO1xyXG4gICAgfVxyXG5cclxuICAgIHNldE5vdGlmaWNhdGlvbkNvdW50KGNvdW50OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkNvdW50ID0gY291bnQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RXJyb3JTdGF0dXMoZXJyb3JEaWRPY2N1cjogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuZXJyb3JEaWRPY2N1ciA9IGVycm9yRGlkT2NjdXI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIHBsYXRmb3JtIHN1cHBvcnRzIGRpc3BsYXlpbmdcclxuICAgICAqIG5vdGlmaWNhdGlvbnMsIG90aGVyd2lzZSBmYWxzZS5cclxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSB3aGV0aGVyIHRoZSBwbGF0Zm9ybSBzdXBwb3J0cyBkaXNwbGF5aW5nIG5vdGlmaWNhdGlvbnNcclxuICAgICAqL1xyXG4gICAgc3VwcG9ydHNOb3RpZmljYXRpb25zKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgYXBwbGljYXRpb24gY3VycmVudGx5IGhhcyBwZXJtaXNzaW9uXHJcbiAgICAgKiB0byBkaXNwbGF5IG5vdGlmaWNhdGlvbnMuIE90aGVyd2lzZSBmYWxzZS5cclxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSB3aGV0aGVyIHRoZSBhcHBsaWNhdGlvbiBoYXMgcGVybWlzc2lvbiB0byBkaXNwbGF5IG5vdGlmaWNhdGlvbnNcclxuICAgICAqL1xyXG4gICAgbWF5U2VuZE5vdGlmaWNhdGlvbnMoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVxdWVzdHMgcGVybWlzc2lvbiB0byBzZW5kIG5vdGlmaWNhdGlvbnMuIFJldHVybnNcclxuICAgICAqIGEgcHJvbWlzZSB0aGF0IGlzIHJlc29sdmVkIHdoZW4gdGhlIHVzZXIgaGFzIHJlc3BvbmRlZFxyXG4gICAgICogdG8gdGhlIHJlcXVlc3QuIFRoZSBwcm9taXNlIGhhcyBhIHNpbmdsZSBzdHJpbmcgYXJndW1lbnRcclxuICAgICAqIHRoYXQgaXMgJ2dyYW50ZWQnIGlmIHRoZSB1c2VyIGFsbG93ZWQgdGhlIHJlcXVlc3Qgb3JcclxuICAgICAqICdkZW5pZWQnIG90aGVyd2lzZS5cclxuICAgICAqL1xyXG4gICAgcmVxdWVzdE5vdGlmaWNhdGlvblBlcm1pc3Npb24oKTogUHJvbWlzZTxzdHJpbmc+IHtcclxuICAgIH1cclxuXHJcbiAgICBkaXNwbGF5Tm90aWZpY2F0aW9uKHRpdGxlOiBzdHJpbmcsIG1zZzogc3RyaW5nLCBhdmF0YXJVcmw6IHN0cmluZywgcm9vbTogT2JqZWN0KSB7XHJcbiAgICB9XHJcblxyXG4gICAgbG91ZE5vdGlmaWNhdGlvbihldjogRXZlbnQsIHJvb206IE9iamVjdCkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB0byBhIHN0cmluZyByZXByZXNlbnRpbmdcclxuICAgICAqIHRoZSBjdXJyZW50IHZlcnNpb24gb2YgdGhlIGFwcGxpY2F0aW9uLlxyXG4gICAgICovXHJcbiAgICBnZXRBcHBWZXJzaW9uKCk6IFByb21pc2U8c3RyaW5nPiB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiZ2V0QXBwVmVyc2lvbiBub3QgaW1wbGVtZW50ZWQhXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBJZiBpdCdzIG5vdCBleHBlY3RlZCB0aGF0IGNhcHR1cmluZyB0aGUgc2NyZWVuIHdpbGwgd29ya1xyXG4gICAgICogd2l0aCBnZXRVc2VyTWVkaWEsIHJldHVybiBhIHN0cmluZyBleHBsYWluaW5nIHdoeSBub3QuXHJcbiAgICAgKiBPdGhlcndpc2UsIHJldHVybiBudWxsLlxyXG4gICAgICovXHJcbiAgICBzY3JlZW5DYXB0dXJlRXJyb3JTdHJpbmcoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gXCJOb3QgaW1wbGVtZW50ZWRcIjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlc3RhcnRzIHRoZSBhcHBsaWNhdGlvbiwgd2l0aG91dCBuZWNjZXNzYXJpbHkgcmVsb2FkaW5nXHJcbiAgICAgKiBhbnkgYXBwbGljYXRpb24gY29kZVxyXG4gICAgICovXHJcbiAgICByZWxvYWQoKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwicmVsb2FkIG5vdCBpbXBsZW1lbnRlZCFcIik7XHJcbiAgICB9XHJcblxyXG4gICAgc3VwcG9ydHNBdXRvTGF1bmNoKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBYWFg6IFN1cmVseSB0aGlzIHNob3VsZCBiZSBhIHNldHRpbmcgbGlrZSBhbnkgb3RoZXI/XHJcbiAgICBhc3luYyBnZXRBdXRvTGF1bmNoRW5hYmxlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgc2V0QXV0b0xhdW5jaEVuYWJsZWQoZW5hYmxlZDogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgc3VwcG9ydHNBdXRvSGlkZU1lbnVCYXIoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGdldEF1dG9IaWRlTWVudUJhckVuYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHNldEF1dG9IaWRlTWVudUJhckVuYWJsZWQoZW5hYmxlZDogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgc3VwcG9ydHNNaW5pbWl6ZVRvVHJheSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0TWluaW1pemVUb1RyYXlFbmFibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBzZXRNaW5pbWl6ZVRvVHJheUVuYWJsZWQoZW5hYmxlZDogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuaW1wbGVtZW50ZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgb3VyIHBsYXRmb3JtIHNwZWNpZmljIEV2ZW50SW5kZXhNYW5hZ2VyLlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4ge0Jhc2VFdmVudEluZGV4TWFuYWdlcn0gVGhlIEV2ZW50SW5kZXggbWFuYWdlciBmb3Igb3VyIHBsYXRmb3JtLFxyXG4gICAgICogY2FuIGJlIG51bGwgaWYgdGhlIHBsYXRmb3JtIGRvZXNuJ3Qgc3VwcG9ydCBldmVudCBpbmRleGluZy5cclxuICAgICAqL1xyXG4gICAgZ2V0RXZlbnRJbmRleGluZ01hbmFnZXIoKTogQmFzZUV2ZW50SW5kZXhNYW5hZ2VyIHwgbnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TGFuZ3VhZ2UocHJlZmVycmVkTGFuZ3M6IHN0cmluZ1tdKSB7fVxyXG5cclxuICAgIGdldFNTT0NhbGxiYWNrVXJsKGhzVXJsOiBzdHJpbmcsIGlzVXJsOiBzdHJpbmcpOiBVUkwge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IG5ldyBVUkwod2luZG93LmxvY2F0aW9uLmhyZWYpO1xyXG4gICAgICAgIC8vIFhYWDogYXQgdGhpcyBwb2ludCwgdGhlIGZyYWdtZW50IHdpbGwgYWx3YXlzIGJlICMvbG9naW4sIHdoaWNoIGlzIG5vXHJcbiAgICAgICAgLy8gdXNlIHRvIGFueW9uZS4gSWRlYWxseSwgd2Ugd291bGQgZ2V0IHRoZSBpbnRlbmRlZCBmcmFnbWVudCBmcm9tXHJcbiAgICAgICAgLy8gTWF0cml4Q2hhdC5zY3JlZW5BZnRlckxvZ2luIHNvIHRoYXQgeW91IGNvdWxkIGZvbGxvdyAjL3Jvb20gbGlua3MgZXRjXHJcbiAgICAgICAgLy8gdGhyb3VnaCBhbiBTU08gbG9naW4uXHJcbiAgICAgICAgdXJsLmhhc2ggPSBcIlwiO1xyXG4gICAgICAgIHVybC5zZWFyY2hQYXJhbXMuc2V0KFwiaG9tZXNlcnZlclwiLCBoc1VybCk7XHJcbiAgICAgICAgdXJsLnNlYXJjaFBhcmFtcy5zZXQoXCJpZGVudGl0eVNlcnZlclwiLCBpc1VybCk7XHJcbiAgICAgICAgcmV0dXJuIHVybDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJlZ2luIFNpbmdsZSBTaWduIE9uIGZsb3dzLlxyXG4gICAgICogQHBhcmFtIHtNYXRyaXhDbGllbnR9IG14Q2xpZW50IHRoZSBtYXRyaXggY2xpZW50IHVzaW5nIHdoaWNoIHdlIHNob3VsZCBzdGFydCB0aGUgZmxvd1xyXG4gICAgICogQHBhcmFtIHtcInNzb1wifFwiY2FzXCJ9IGxvZ2luVHlwZSB0aGUgdHlwZSBvZiBTU08gaXQgaXMsIENBUy9TU08uXHJcbiAgICAgKi9cclxuICAgIHN0YXJ0U2luZ2xlU2lnbk9uKG14Q2xpZW50OiBNYXRyaXhDbGllbnQsIGxvZ2luVHlwZTogXCJzc29cInxcImNhc1wiKSB7XHJcbiAgICAgICAgY29uc3QgY2FsbGJhY2tVcmwgPSB0aGlzLmdldFNTT0NhbGxiYWNrVXJsKG14Q2xpZW50LmdldEhvbWVzZXJ2ZXJVcmwoKSwgbXhDbGllbnQuZ2V0SWRlbnRpdHlTZXJ2ZXJVcmwoKSk7XHJcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBteENsaWVudC5nZXRTc29Mb2dpblVybChjYWxsYmFja1VybC50b1N0cmluZygpLCBsb2dpblR5cGUpOyAvLyByZWRpcmVjdCB0byBTU09cclxuICAgIH1cclxufVxyXG4iXX0=