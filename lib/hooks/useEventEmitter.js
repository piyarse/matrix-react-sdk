"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useEventEmitter = void 0;

var _react = require("react");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Hook to wrap event emitter on and removeListener in hook lifecycle
const useEventEmitter = (emitter, eventName, handler) => {
  // Create a ref that stores handler
  const savedHandler = (0, _react.useRef)(); // Update ref.current value if handler changes.

  (0, _react.useEffect)(() => {
    savedHandler.current = handler;
  }, [handler]);
  (0, _react.useEffect)(() => {
    // allow disabling this hook by passing a falsy emitter
    if (!emitter) return; // Create event listener that calls handler function stored in ref

    const eventListener = event => savedHandler.current(event); // Add event listener


    emitter.on(eventName, eventListener); // Remove event listener on cleanup

    return () => {
      emitter.removeListener(eventName, eventListener);
    };
  }, [eventName, emitter] // Re-run if eventName or emitter changes
  );
};

exports.useEventEmitter = useEventEmitter;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VFdmVudEVtaXR0ZXIuanMiXSwibmFtZXMiOlsidXNlRXZlbnRFbWl0dGVyIiwiZW1pdHRlciIsImV2ZW50TmFtZSIsImhhbmRsZXIiLCJzYXZlZEhhbmRsZXIiLCJjdXJyZW50IiwiZXZlbnRMaXN0ZW5lciIsImV2ZW50Iiwib24iLCJyZW1vdmVMaXN0ZW5lciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQWdCQTs7QUFoQkE7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNPLE1BQU1BLGVBQWUsR0FBRyxDQUFDQyxPQUFELEVBQVVDLFNBQVYsRUFBcUJDLE9BQXJCLEtBQWlDO0FBQzVEO0FBQ0EsUUFBTUMsWUFBWSxHQUFHLG9CQUFyQixDQUY0RCxDQUk1RDs7QUFDQSx3QkFBVSxNQUFNO0FBQ1pBLElBQUFBLFlBQVksQ0FBQ0MsT0FBYixHQUF1QkYsT0FBdkI7QUFDSCxHQUZELEVBRUcsQ0FBQ0EsT0FBRCxDQUZIO0FBSUEsd0JBQ0ksTUFBTTtBQUNGO0FBQ0EsUUFBSSxDQUFDRixPQUFMLEVBQWMsT0FGWixDQUlGOztBQUNBLFVBQU1LLGFBQWEsR0FBR0MsS0FBSyxJQUFJSCxZQUFZLENBQUNDLE9BQWIsQ0FBcUJFLEtBQXJCLENBQS9CLENBTEUsQ0FPRjs7O0FBQ0FOLElBQUFBLE9BQU8sQ0FBQ08sRUFBUixDQUFXTixTQUFYLEVBQXNCSSxhQUF0QixFQVJFLENBVUY7O0FBQ0EsV0FBTyxNQUFNO0FBQ1RMLE1BQUFBLE9BQU8sQ0FBQ1EsY0FBUixDQUF1QlAsU0FBdkIsRUFBa0NJLGFBQWxDO0FBQ0gsS0FGRDtBQUdILEdBZkwsRUFnQkksQ0FBQ0osU0FBRCxFQUFZRCxPQUFaLENBaEJKLENBZ0IwQjtBQWhCMUI7QUFrQkgsQ0EzQk0iLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB7dXNlUmVmLCB1c2VFZmZlY3R9IGZyb20gXCJyZWFjdFwiO1xyXG5cclxuLy8gSG9vayB0byB3cmFwIGV2ZW50IGVtaXR0ZXIgb24gYW5kIHJlbW92ZUxpc3RlbmVyIGluIGhvb2sgbGlmZWN5Y2xlXHJcbmV4cG9ydCBjb25zdCB1c2VFdmVudEVtaXR0ZXIgPSAoZW1pdHRlciwgZXZlbnROYW1lLCBoYW5kbGVyKSA9PiB7XHJcbiAgICAvLyBDcmVhdGUgYSByZWYgdGhhdCBzdG9yZXMgaGFuZGxlclxyXG4gICAgY29uc3Qgc2F2ZWRIYW5kbGVyID0gdXNlUmVmKCk7XHJcblxyXG4gICAgLy8gVXBkYXRlIHJlZi5jdXJyZW50IHZhbHVlIGlmIGhhbmRsZXIgY2hhbmdlcy5cclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgICAgc2F2ZWRIYW5kbGVyLmN1cnJlbnQgPSBoYW5kbGVyO1xyXG4gICAgfSwgW2hhbmRsZXJdKTtcclxuXHJcbiAgICB1c2VFZmZlY3QoXHJcbiAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBhbGxvdyBkaXNhYmxpbmcgdGhpcyBob29rIGJ5IHBhc3NpbmcgYSBmYWxzeSBlbWl0dGVyXHJcbiAgICAgICAgICAgIGlmICghZW1pdHRlcikgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgLy8gQ3JlYXRlIGV2ZW50IGxpc3RlbmVyIHRoYXQgY2FsbHMgaGFuZGxlciBmdW5jdGlvbiBzdG9yZWQgaW4gcmVmXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50TGlzdGVuZXIgPSBldmVudCA9PiBzYXZlZEhhbmRsZXIuY3VycmVudChldmVudCk7XHJcblxyXG4gICAgICAgICAgICAvLyBBZGQgZXZlbnQgbGlzdGVuZXJcclxuICAgICAgICAgICAgZW1pdHRlci5vbihldmVudE5hbWUsIGV2ZW50TGlzdGVuZXIpO1xyXG5cclxuICAgICAgICAgICAgLy8gUmVtb3ZlIGV2ZW50IGxpc3RlbmVyIG9uIGNsZWFudXBcclxuICAgICAgICAgICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGVtaXR0ZXIucmVtb3ZlTGlzdGVuZXIoZXZlbnROYW1lLCBldmVudExpc3RlbmVyKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIFtldmVudE5hbWUsIGVtaXR0ZXJdLCAvLyBSZS1ydW4gaWYgZXZlbnROYW1lIG9yIGVtaXR0ZXIgY2hhbmdlc1xyXG4gICAgKTtcclxufTtcclxuIl19