"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startListening = startListening;
exports.stopListening = stopListening;
exports.setOpenManagerUrl = setOpenManagerUrl;

var _MatrixClientPeg = require("./MatrixClientPeg");

var _matrixJsSdk = require("matrix-js-sdk");

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var _WidgetUtils = _interopRequireDefault(require("./utils/WidgetUtils"));

var _RoomViewStore = _interopRequireDefault(require("./stores/RoomViewStore"));

var _languageHandler = require("./languageHandler");

var _IntegrationManagers = require("./integrations/IntegrationManagers");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
Listens for incoming postMessage requests from the integrations UI URL. The following API is exposed:
{
    action: "invite" | "membership_state" | "bot_options" | "set_bot_options" | etc... ,
    room_id: $ROOM_ID,
    user_id: $USER_ID
    // additional request fields
}

The complete request object is returned to the caller with an additional "response" key like so:
{
    action: "invite" | "membership_state" | "bot_options" | "set_bot_options",
    room_id: $ROOM_ID,
    user_id: $USER_ID,
    // additional request fields
    response: { ... }
}

The "action" determines the format of the request and response. All actions can return an error response.
An error response is a "response" object which consists of a sole "error" key to indicate an error.
They look like:
{
    error: {
        message: "Unable to invite user into room.",
        _error: <Original Error Object>
    }
}
The "message" key should be a human-friendly string.

ACTIONS
=======
All actions can return an error response instead of the response outlined below.

invite
------
Invites a user into a room.

Request:
 - room_id is the room to invite the user into.
 - user_id is the user ID to invite.
 - No additional fields.
Response:
{
    success: true
}
Example:
{
    action: "invite",
    room_id: "!foo:bar",
    user_id: "@invitee:bar",
    response: {
        success: true
    }
}

set_bot_options
---------------
Set the m.room.bot.options state event for a bot user.

Request:
 - room_id is the room to send the state event into.
 - user_id is the user ID of the bot who you're setting options for.
 - "content" is an object consisting of the content you wish to set.
Response:
{
    success: true
}
Example:
{
    action: "set_bot_options",
    room_id: "!foo:bar",
    user_id: "@bot:bar",
    content: {
        default_option: "alpha"
    },
    response: {
        success: true
    }
}

get_membership_count
--------------------
Get the number of joined users in the room.

Request:
 - room_id is the room to get the count in.
Response:
78
Example:
{
    action: "get_membership_count",
    room_id: "!foo:bar",
    response: 78
}

can_send_event
--------------
Check if the client can send the given event into the given room. If the client
is unable to do this, an error response is returned instead of 'response: false'.

Request:
 - room_id is the room to do the check in.
 - event_type is the event type which will be sent.
 - is_state is true if the event to be sent is a state event.
Response:
true
Example:
{
    action: "can_send_event",
    is_state: false,
    event_type: "m.room.message",
    room_id: "!foo:bar",
    response: true
}

set_widget
----------
Set a new widget in the room. Clobbers based on the ID.

Request:
 - `room_id` (String) is the room to set the widget in.
 - `widget_id` (String) is the ID of the widget to add (or replace if it already exists).
   It can be an arbitrary UTF8 string and is purely for distinguishing between widgets.
 - `url` (String) is the URL that clients should load in an iframe to run the widget.
   All widgets must have a valid URL. If the URL is `null` (not `undefined`), the
   widget will be removed from the room.
 - `type` (String) is the type of widget, which is provided as a hint for matrix clients so they
   can configure/lay out the widget in different ways. All widgets must have a type.
 - `name` (String) is an optional human-readable string about the widget.
 - `data` (Object) is some optional data about the widget, and can contain arbitrary key/value pairs.
Response:
{
    success: true
}
Example:
{
    action: "set_widget",
    room_id: "!foo:bar",
    widget_id: "abc123",
    url: "http://widget.url",
    type: "example",
    response: {
        success: true
    }
}

get_widgets
-----------
Get a list of all widgets in the room. The response is an array
of state events.

Request:
 - `room_id` (String) is the room to get the widgets in.
Response:
[
    {
        type: "im.vector.modular.widgets",
        state_key: "wid1",
        content: {
            type: "grafana",
            url: "https://grafanaurl",
            name: "dashboard",
            data: {key: "val"}
        }
        room_id: “!foo:bar”,
        sender: "@alice:localhost"
    }
]
Example:
{
    action: "get_widgets",
    room_id: "!foo:bar",
    response: [
        {
            type: "im.vector.modular.widgets",
            state_key: "wid1",
            content: {
                type: "grafana",
                url: "https://grafanaurl",
                name: "dashboard",
                data: {key: "val"}
            }
            room_id: “!foo:bar”,
            sender: "@alice:localhost"
        }
    ]
}


membership_state AND bot_options
--------------------------------
Get the content of the "m.room.member" or "m.room.bot.options" state event respectively.

NB: Whilst this API is basically equivalent to getStateEvent, we specifically do not
    want external entities to be able to query any state event for any room, hence the
    restrictive API outlined here.

Request:
 - room_id is the room which has the state event.
 - user_id is the state_key parameter which in both cases is a user ID (the member or the bot).
 - No additional fields.
Response:
 - The event content. If there is no state event, the "response" key should be null.
Example:
{
    action: "membership_state",
    room_id: "!foo:bar",
    user_id: "@somemember:bar",
    response: {
        membership: "join",
        displayname: "Bob",
        avatar_url: null
    }
}
*/
function sendResponse(event, res) {
  const data = JSON.parse(JSON.stringify(event.data));
  data.response = res;
  event.source.postMessage(data, event.origin);
}

function sendError(event, msg, nestedError) {
  console.error("Action:" + event.data.action + " failed with message: " + msg);
  const data = JSON.parse(JSON.stringify(event.data));
  data.response = {
    error: {
      message: msg
    }
  };

  if (nestedError) {
    data.response.error._error = nestedError;
  }

  event.source.postMessage(data, event.origin);
}

function inviteUser(event, roomId, userId) {
  console.log("Received request to invite ".concat(userId, " into room ").concat(roomId));

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  const room = client.getRoom(roomId);

  if (room) {
    // if they are already invited we can resolve immediately.
    const member = room.getMember(userId);

    if (member && member.membership === "invite") {
      sendResponse(event, {
        success: true
      });
      return;
    }
  }

  client.invite(roomId, userId).then(function () {
    sendResponse(event, {
      success: true
    });
  }, function (err) {
    sendError(event, (0, _languageHandler._t)('You need to be able to invite users to do that.'), err);
  });
}

function setWidget(event, roomId) {
  const widgetId = event.data.widget_id;
  const widgetType = event.data.type;
  const widgetUrl = event.data.url;
  const widgetName = event.data.name; // optional

  const widgetData = event.data.data; // optional

  const userWidget = event.data.userWidget; // both adding/removing widgets need these checks

  if (!widgetId || widgetUrl === undefined) {
    sendError(event, (0, _languageHandler._t)("Unable to create widget."), new Error("Missing required widget fields."));
    return;
  }

  if (widgetUrl !== null) {
    // if url is null it is being deleted, don't need to check name/type/etc
    // check types of fields
    if (widgetName !== undefined && typeof widgetName !== 'string') {
      sendError(event, (0, _languageHandler._t)("Unable to create widget."), new Error("Optional field 'name' must be a string."));
      return;
    }

    if (widgetData !== undefined && !(widgetData instanceof Object)) {
      sendError(event, (0, _languageHandler._t)("Unable to create widget."), new Error("Optional field 'data' must be an Object."));
      return;
    }

    if (typeof widgetType !== 'string') {
      sendError(event, (0, _languageHandler._t)("Unable to create widget."), new Error("Field 'type' must be a string."));
      return;
    }

    if (typeof widgetUrl !== 'string') {
      sendError(event, (0, _languageHandler._t)("Unable to create widget."), new Error("Field 'url' must be a string or null."));
      return;
    }
  }

  if (userWidget) {
    _WidgetUtils.default.setUserWidget(widgetId, widgetType, widgetUrl, widgetName, widgetData).then(() => {
      sendResponse(event, {
        success: true
      });

      _dispatcher.default.dispatch({
        action: "user_widget_updated"
      });
    }).catch(e => {
      sendError(event, (0, _languageHandler._t)('Unable to create widget.'), e);
    });
  } else {
    // Room widget
    if (!roomId) {
      sendError(event, (0, _languageHandler._t)('Missing roomId.'), null);
    }

    _WidgetUtils.default.setRoomWidget(roomId, widgetId, widgetType, widgetUrl, widgetName, widgetData).then(() => {
      sendResponse(event, {
        success: true
      });
    }, err => {
      sendError(event, (0, _languageHandler._t)('Failed to send request.'), err);
    });
  }
}

function getWidgets(event, roomId) {
  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  let widgetStateEvents = [];

  if (roomId) {
    const room = client.getRoom(roomId);

    if (!room) {
      sendError(event, (0, _languageHandler._t)('This room is not recognised.'));
      return;
    } // XXX: This gets the raw event object (I think because we can't
    // send the MatrixEvent over postMessage?)


    widgetStateEvents = _WidgetUtils.default.getRoomWidgets(room).map(ev => ev.event);
  } // Add user widgets (not linked to a specific room)


  const userWidgets = _WidgetUtils.default.getUserWidgetsArray();

  widgetStateEvents = widgetStateEvents.concat(userWidgets);
  sendResponse(event, widgetStateEvents);
}

function getRoomEncState(event, roomId) {
  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  const room = client.getRoom(roomId);

  if (!room) {
    sendError(event, (0, _languageHandler._t)('This room is not recognised.'));
    return;
  }

  const roomIsEncrypted = _MatrixClientPeg.MatrixClientPeg.get().isRoomEncrypted(roomId);

  sendResponse(event, roomIsEncrypted);
}

function setPlumbingState(event, roomId, status) {
  if (typeof status !== 'string') {
    throw new Error('Plumbing state status should be a string');
  }

  console.log("Received request to set plumbing state to status \"".concat(status, "\" in room ").concat(roomId));

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  client.sendStateEvent(roomId, "m.room.plumbing", {
    status: status
  }).then(() => {
    sendResponse(event, {
      success: true
    });
  }, err => {
    sendError(event, err.message ? err.message : (0, _languageHandler._t)('Failed to send request.'), err);
  });
}

function setBotOptions(event, roomId, userId) {
  console.log("Received request to set options for bot ".concat(userId, " in room ").concat(roomId));

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  client.sendStateEvent(roomId, "m.room.bot.options", event.data.content, "_" + userId).then(() => {
    sendResponse(event, {
      success: true
    });
  }, err => {
    sendError(event, err.message ? err.message : (0, _languageHandler._t)('Failed to send request.'), err);
  });
}

function setBotPower(event, roomId, userId, level) {
  if (!(Number.isInteger(level) && level >= 0)) {
    sendError(event, (0, _languageHandler._t)('Power level must be positive integer.'));
    return;
  }

  console.log("Received request to set power level to ".concat(level, " for bot ").concat(userId, " in room ").concat(roomId, "."));

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  client.getStateEvent(roomId, "m.room.power_levels", "").then(powerLevels => {
    const powerEvent = new _matrixJsSdk.MatrixEvent({
      type: "m.room.power_levels",
      content: powerLevels
    });
    client.setPowerLevel(roomId, userId, level, powerEvent).then(() => {
      sendResponse(event, {
        success: true
      });
    }, err => {
      sendError(event, err.message ? err.message : (0, _languageHandler._t)('Failed to send request.'), err);
    });
  });
}

function getMembershipState(event, roomId, userId) {
  console.log("membership_state of ".concat(userId, " in room ").concat(roomId, " requested."));
  returnStateEvent(event, roomId, "m.room.member", userId);
}

function getJoinRules(event, roomId) {
  console.log("join_rules of ".concat(roomId, " requested."));
  returnStateEvent(event, roomId, "m.room.join_rules", "");
}

function botOptions(event, roomId, userId) {
  console.log("bot_options of ".concat(userId, " in room ").concat(roomId, " requested."));
  returnStateEvent(event, roomId, "m.room.bot.options", "_" + userId);
}

function getMembershipCount(event, roomId) {
  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  const room = client.getRoom(roomId);

  if (!room) {
    sendError(event, (0, _languageHandler._t)('This room is not recognised.'));
    return;
  }

  const count = room.getJoinedMemberCount();
  sendResponse(event, count);
}

function canSendEvent(event, roomId) {
  const evType = "" + event.data.event_type; // force stringify

  const isState = Boolean(event.data.is_state);

  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  const room = client.getRoom(roomId);

  if (!room) {
    sendError(event, (0, _languageHandler._t)('This room is not recognised.'));
    return;
  }

  if (room.getMyMembership() !== "join") {
    sendError(event, (0, _languageHandler._t)('You are not in this room.'));
    return;
  }

  const me = client.credentials.userId;
  let canSend = false;

  if (isState) {
    canSend = room.currentState.maySendStateEvent(evType, me);
  } else {
    canSend = room.currentState.maySendEvent(evType, me);
  }

  if (!canSend) {
    sendError(event, (0, _languageHandler._t)('You do not have permission to do that in this room.'));
    return;
  }

  sendResponse(event, true);
}

function returnStateEvent(event, roomId, eventType, stateKey) {
  const client = _MatrixClientPeg.MatrixClientPeg.get();

  if (!client) {
    sendError(event, (0, _languageHandler._t)('You need to be logged in.'));
    return;
  }

  const room = client.getRoom(roomId);

  if (!room) {
    sendError(event, (0, _languageHandler._t)('This room is not recognised.'));
    return;
  }

  const stateEvent = room.currentState.getStateEvents(eventType, stateKey);

  if (!stateEvent) {
    sendResponse(event, null);
    return;
  }

  sendResponse(event, stateEvent.getContent());
}

const onMessage = function (event) {
  if (!event.origin) {
    // stupid chrome
    event.origin = event.originalEvent.origin;
  } // Check that the integrations UI URL starts with the origin of the event
  // This means the URL could contain a path (like /develop) and still be used
  // to validate event origins, which do not specify paths.
  // (See https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)


  let configUrl;

  try {
    if (!openManagerUrl) openManagerUrl = _IntegrationManagers.IntegrationManagers.sharedInstance().getPrimaryManager().uiUrl;
    configUrl = new URL(openManagerUrl);
  } catch (e) {
    // No integrations UI URL, ignore silently.
    return;
  }

  let eventOriginUrl;

  try {
    eventOriginUrl = new URL(event.origin);
  } catch (e) {
    return;
  } // TODO -- Scalar postMessage API should be namespaced with event.data.api field
  // Fix following "if" statement to respond only to specific API messages.


  if (configUrl.origin !== eventOriginUrl.origin || !event.data.action || event.data.api // Ignore messages with specific API set
  ) {
      // don't log this - debugging APIs and browser add-ons like to spam
      // postMessage which floods the log otherwise
      return;
    }

  if (event.data.action === "close_scalar") {
    _dispatcher.default.dispatch({
      action: "close_scalar"
    });

    sendResponse(event, null);
    return;
  }

  const roomId = event.data.room_id;
  const userId = event.data.user_id;

  if (!roomId) {
    // These APIs don't require roomId
    // Get and set user widgets (not associated with a specific room)
    // If roomId is specified, it must be validated, so room-based widgets agreed
    // handled further down.
    if (event.data.action === "get_widgets") {
      getWidgets(event, null);
      return;
    } else if (event.data.action === "set_widget") {
      setWidget(event, null);
      return;
    } else {
      sendError(event, (0, _languageHandler._t)('Missing room_id in request'));
      return;
    }
  }

  if (roomId !== _RoomViewStore.default.getRoomId()) {
    sendError(event, (0, _languageHandler._t)('Room %(roomId)s not visible', {
      roomId: roomId
    }));
    return;
  } // Get and set room-based widgets


  if (event.data.action === "get_widgets") {
    getWidgets(event, roomId);
    return;
  } else if (event.data.action === "set_widget") {
    setWidget(event, roomId);
    return;
  } // These APIs don't require userId


  if (event.data.action === "join_rules_state") {
    getJoinRules(event, roomId);
    return;
  } else if (event.data.action === "set_plumbing_state") {
    setPlumbingState(event, roomId, event.data.status);
    return;
  } else if (event.data.action === "get_membership_count") {
    getMembershipCount(event, roomId);
    return;
  } else if (event.data.action === "get_room_enc_state") {
    getRoomEncState(event, roomId);
    return;
  } else if (event.data.action === "can_send_event") {
    canSendEvent(event, roomId);
    return;
  }

  if (!userId) {
    sendError(event, (0, _languageHandler._t)('Missing user_id in request'));
    return;
  }

  switch (event.data.action) {
    case "membership_state":
      getMembershipState(event, roomId, userId);
      break;

    case "invite":
      inviteUser(event, roomId, userId);
      break;

    case "bot_options":
      botOptions(event, roomId, userId);
      break;

    case "set_bot_options":
      setBotOptions(event, roomId, userId);
      break;

    case "set_bot_power":
      setBotPower(event, roomId, userId, event.data.level);
      break;

    default:
      console.warn("Unhandled postMessage event with action '" + event.data.action + "'");
      break;
  }
};

let listenerCount = 0;
let openManagerUrl = null;

function startListening() {
  if (listenerCount === 0) {
    window.addEventListener("message", onMessage, false);
  }

  listenerCount += 1;
}

function stopListening() {
  listenerCount -= 1;

  if (listenerCount === 0) {
    window.removeEventListener("message", onMessage);
  }

  if (listenerCount < 0) {
    // Make an error so we get a stack trace
    const e = new Error("ScalarMessaging: mismatched startListening / stopListening detected." + " Negative count");
    console.error(e);
  }
}

function setOpenManagerUrl(url) {
  openManagerUrl = url;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9TY2FsYXJNZXNzYWdpbmcuanMiXSwibmFtZXMiOlsic2VuZFJlc3BvbnNlIiwiZXZlbnQiLCJyZXMiLCJkYXRhIiwiSlNPTiIsInBhcnNlIiwic3RyaW5naWZ5IiwicmVzcG9uc2UiLCJzb3VyY2UiLCJwb3N0TWVzc2FnZSIsIm9yaWdpbiIsInNlbmRFcnJvciIsIm1zZyIsIm5lc3RlZEVycm9yIiwiY29uc29sZSIsImVycm9yIiwiYWN0aW9uIiwibWVzc2FnZSIsIl9lcnJvciIsImludml0ZVVzZXIiLCJyb29tSWQiLCJ1c2VySWQiLCJsb2ciLCJjbGllbnQiLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJyb29tIiwiZ2V0Um9vbSIsIm1lbWJlciIsImdldE1lbWJlciIsIm1lbWJlcnNoaXAiLCJzdWNjZXNzIiwiaW52aXRlIiwidGhlbiIsImVyciIsInNldFdpZGdldCIsIndpZGdldElkIiwid2lkZ2V0X2lkIiwid2lkZ2V0VHlwZSIsInR5cGUiLCJ3aWRnZXRVcmwiLCJ1cmwiLCJ3aWRnZXROYW1lIiwibmFtZSIsIndpZGdldERhdGEiLCJ1c2VyV2lkZ2V0IiwidW5kZWZpbmVkIiwiRXJyb3IiLCJPYmplY3QiLCJXaWRnZXRVdGlscyIsInNldFVzZXJXaWRnZXQiLCJkaXMiLCJkaXNwYXRjaCIsImNhdGNoIiwiZSIsInNldFJvb21XaWRnZXQiLCJnZXRXaWRnZXRzIiwid2lkZ2V0U3RhdGVFdmVudHMiLCJnZXRSb29tV2lkZ2V0cyIsIm1hcCIsImV2IiwidXNlcldpZGdldHMiLCJnZXRVc2VyV2lkZ2V0c0FycmF5IiwiY29uY2F0IiwiZ2V0Um9vbUVuY1N0YXRlIiwicm9vbUlzRW5jcnlwdGVkIiwiaXNSb29tRW5jcnlwdGVkIiwic2V0UGx1bWJpbmdTdGF0ZSIsInN0YXR1cyIsInNlbmRTdGF0ZUV2ZW50Iiwic2V0Qm90T3B0aW9ucyIsImNvbnRlbnQiLCJzZXRCb3RQb3dlciIsImxldmVsIiwiTnVtYmVyIiwiaXNJbnRlZ2VyIiwiZ2V0U3RhdGVFdmVudCIsInBvd2VyTGV2ZWxzIiwicG93ZXJFdmVudCIsIk1hdHJpeEV2ZW50Iiwic2V0UG93ZXJMZXZlbCIsImdldE1lbWJlcnNoaXBTdGF0ZSIsInJldHVyblN0YXRlRXZlbnQiLCJnZXRKb2luUnVsZXMiLCJib3RPcHRpb25zIiwiZ2V0TWVtYmVyc2hpcENvdW50IiwiY291bnQiLCJnZXRKb2luZWRNZW1iZXJDb3VudCIsImNhblNlbmRFdmVudCIsImV2VHlwZSIsImV2ZW50X3R5cGUiLCJpc1N0YXRlIiwiQm9vbGVhbiIsImlzX3N0YXRlIiwiZ2V0TXlNZW1iZXJzaGlwIiwibWUiLCJjcmVkZW50aWFscyIsImNhblNlbmQiLCJjdXJyZW50U3RhdGUiLCJtYXlTZW5kU3RhdGVFdmVudCIsIm1heVNlbmRFdmVudCIsImV2ZW50VHlwZSIsInN0YXRlS2V5Iiwic3RhdGVFdmVudCIsImdldFN0YXRlRXZlbnRzIiwiZ2V0Q29udGVudCIsIm9uTWVzc2FnZSIsIm9yaWdpbmFsRXZlbnQiLCJjb25maWdVcmwiLCJvcGVuTWFuYWdlclVybCIsIkludGVncmF0aW9uTWFuYWdlcnMiLCJzaGFyZWRJbnN0YW5jZSIsImdldFByaW1hcnlNYW5hZ2VyIiwidWlVcmwiLCJVUkwiLCJldmVudE9yaWdpblVybCIsImFwaSIsInJvb21faWQiLCJ1c2VyX2lkIiwiUm9vbVZpZXdTdG9yZSIsImdldFJvb21JZCIsIndhcm4iLCJsaXN0ZW5lckNvdW50Iiwic3RhcnRMaXN0ZW5pbmciLCJ3aW5kb3ciLCJhZGRFdmVudExpc3RlbmVyIiwic3RvcExpc3RlbmluZyIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJzZXRPcGVuTWFuYWdlclVybCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUEwT0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBaFBBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ09BLFNBQVNBLFlBQVQsQ0FBc0JDLEtBQXRCLEVBQTZCQyxHQUE3QixFQUFrQztBQUM5QixRQUFNQyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLFNBQUwsQ0FBZUwsS0FBSyxDQUFDRSxJQUFyQixDQUFYLENBQWI7QUFDQUEsRUFBQUEsSUFBSSxDQUFDSSxRQUFMLEdBQWdCTCxHQUFoQjtBQUNBRCxFQUFBQSxLQUFLLENBQUNPLE1BQU4sQ0FBYUMsV0FBYixDQUF5Qk4sSUFBekIsRUFBK0JGLEtBQUssQ0FBQ1MsTUFBckM7QUFDSDs7QUFFRCxTQUFTQyxTQUFULENBQW1CVixLQUFuQixFQUEwQlcsR0FBMUIsRUFBK0JDLFdBQS9CLEVBQTRDO0FBQ3hDQyxFQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxZQUFZZCxLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBdkIsR0FBZ0Msd0JBQWhDLEdBQTJESixHQUF6RTtBQUNBLFFBQU1ULElBQUksR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsU0FBTCxDQUFlTCxLQUFLLENBQUNFLElBQXJCLENBQVgsQ0FBYjtBQUNBQSxFQUFBQSxJQUFJLENBQUNJLFFBQUwsR0FBZ0I7QUFDWlEsSUFBQUEsS0FBSyxFQUFFO0FBQ0hFLE1BQUFBLE9BQU8sRUFBRUw7QUFETjtBQURLLEdBQWhCOztBQUtBLE1BQUlDLFdBQUosRUFBaUI7QUFDYlYsSUFBQUEsSUFBSSxDQUFDSSxRQUFMLENBQWNRLEtBQWQsQ0FBb0JHLE1BQXBCLEdBQTZCTCxXQUE3QjtBQUNIOztBQUNEWixFQUFBQSxLQUFLLENBQUNPLE1BQU4sQ0FBYUMsV0FBYixDQUF5Qk4sSUFBekIsRUFBK0JGLEtBQUssQ0FBQ1MsTUFBckM7QUFDSDs7QUFFRCxTQUFTUyxVQUFULENBQW9CbEIsS0FBcEIsRUFBMkJtQixNQUEzQixFQUFtQ0MsTUFBbkMsRUFBMkM7QUFDdkNQLEVBQUFBLE9BQU8sQ0FBQ1EsR0FBUixzQ0FBMENELE1BQTFDLHdCQUE4REQsTUFBOUQ7O0FBQ0EsUUFBTUcsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsTUFBSSxDQUFDRixNQUFMLEVBQWE7QUFDVFosSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsMkJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFDRCxRQUFNeUIsSUFBSSxHQUFHSCxNQUFNLENBQUNJLE9BQVAsQ0FBZVAsTUFBZixDQUFiOztBQUNBLE1BQUlNLElBQUosRUFBVTtBQUNOO0FBQ0EsVUFBTUUsTUFBTSxHQUFHRixJQUFJLENBQUNHLFNBQUwsQ0FBZVIsTUFBZixDQUFmOztBQUNBLFFBQUlPLE1BQU0sSUFBSUEsTUFBTSxDQUFDRSxVQUFQLEtBQXNCLFFBQXBDLEVBQThDO0FBQzFDOUIsTUFBQUEsWUFBWSxDQUFDQyxLQUFELEVBQVE7QUFDaEI4QixRQUFBQSxPQUFPLEVBQUU7QUFETyxPQUFSLENBQVo7QUFHQTtBQUNIO0FBQ0o7O0FBRURSLEVBQUFBLE1BQU0sQ0FBQ1MsTUFBUCxDQUFjWixNQUFkLEVBQXNCQyxNQUF0QixFQUE4QlksSUFBOUIsQ0FBbUMsWUFBVztBQUMxQ2pDLElBQUFBLFlBQVksQ0FBQ0MsS0FBRCxFQUFRO0FBQ2hCOEIsTUFBQUEsT0FBTyxFQUFFO0FBRE8sS0FBUixDQUFaO0FBR0gsR0FKRCxFQUlHLFVBQVNHLEdBQVQsRUFBYztBQUNidkIsSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsaURBQUgsQ0FBUixFQUErRGlDLEdBQS9ELENBQVQ7QUFDSCxHQU5EO0FBT0g7O0FBRUQsU0FBU0MsU0FBVCxDQUFtQmxDLEtBQW5CLEVBQTBCbUIsTUFBMUIsRUFBa0M7QUFDOUIsUUFBTWdCLFFBQVEsR0FBR25DLEtBQUssQ0FBQ0UsSUFBTixDQUFXa0MsU0FBNUI7QUFDQSxRQUFNQyxVQUFVLEdBQUdyQyxLQUFLLENBQUNFLElBQU4sQ0FBV29DLElBQTlCO0FBQ0EsUUFBTUMsU0FBUyxHQUFHdkMsS0FBSyxDQUFDRSxJQUFOLENBQVdzQyxHQUE3QjtBQUNBLFFBQU1DLFVBQVUsR0FBR3pDLEtBQUssQ0FBQ0UsSUFBTixDQUFXd0MsSUFBOUIsQ0FKOEIsQ0FJTTs7QUFDcEMsUUFBTUMsVUFBVSxHQUFHM0MsS0FBSyxDQUFDRSxJQUFOLENBQVdBLElBQTlCLENBTDhCLENBS007O0FBQ3BDLFFBQU0wQyxVQUFVLEdBQUc1QyxLQUFLLENBQUNFLElBQU4sQ0FBVzBDLFVBQTlCLENBTjhCLENBUTlCOztBQUNBLE1BQUksQ0FBQ1QsUUFBRCxJQUFhSSxTQUFTLEtBQUtNLFNBQS9CLEVBQTBDO0FBQ3RDbkMsSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsMEJBQUgsQ0FBUixFQUF3QyxJQUFJOEMsS0FBSixDQUFVLGlDQUFWLENBQXhDLENBQVQ7QUFDQTtBQUNIOztBQUVELE1BQUlQLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUFFO0FBQ3RCO0FBQ0EsUUFBSUUsVUFBVSxLQUFLSSxTQUFmLElBQTRCLE9BQU9KLFVBQVAsS0FBc0IsUUFBdEQsRUFBZ0U7QUFDNUQvQixNQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywwQkFBSCxDQUFSLEVBQXdDLElBQUk4QyxLQUFKLENBQVUseUNBQVYsQ0FBeEMsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0QsUUFBSUgsVUFBVSxLQUFLRSxTQUFmLElBQTRCLEVBQUVGLFVBQVUsWUFBWUksTUFBeEIsQ0FBaEMsRUFBaUU7QUFDN0RyQyxNQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywwQkFBSCxDQUFSLEVBQXdDLElBQUk4QyxLQUFKLENBQVUsMENBQVYsQ0FBeEMsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0QsUUFBSSxPQUFPVCxVQUFQLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ2hDM0IsTUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsMEJBQUgsQ0FBUixFQUF3QyxJQUFJOEMsS0FBSixDQUFVLGdDQUFWLENBQXhDLENBQVQ7QUFDQTtBQUNIOztBQUNELFFBQUksT0FBT1AsU0FBUCxLQUFxQixRQUF6QixFQUFtQztBQUMvQjdCLE1BQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDBCQUFILENBQVIsRUFBd0MsSUFBSThDLEtBQUosQ0FBVSx1Q0FBVixDQUF4QyxDQUFUO0FBQ0E7QUFDSDtBQUNKOztBQUVELE1BQUlGLFVBQUosRUFBZ0I7QUFDWkkseUJBQVlDLGFBQVosQ0FBMEJkLFFBQTFCLEVBQW9DRSxVQUFwQyxFQUFnREUsU0FBaEQsRUFBMkRFLFVBQTNELEVBQXVFRSxVQUF2RSxFQUFtRlgsSUFBbkYsQ0FBd0YsTUFBTTtBQUMxRmpDLE1BQUFBLFlBQVksQ0FBQ0MsS0FBRCxFQUFRO0FBQ2hCOEIsUUFBQUEsT0FBTyxFQUFFO0FBRE8sT0FBUixDQUFaOztBQUlBb0IsMEJBQUlDLFFBQUosQ0FBYTtBQUFFcEMsUUFBQUEsTUFBTSxFQUFFO0FBQVYsT0FBYjtBQUNILEtBTkQsRUFNR3FDLEtBTkgsQ0FNVUMsQ0FBRCxJQUFPO0FBQ1ozQyxNQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywwQkFBSCxDQUFSLEVBQXdDcUQsQ0FBeEMsQ0FBVDtBQUNILEtBUkQ7QUFTSCxHQVZELE1BVU87QUFBRTtBQUNMLFFBQUksQ0FBQ2xDLE1BQUwsRUFBYTtBQUNUVCxNQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRyxpQkFBSCxDQUFSLEVBQStCLElBQS9CLENBQVQ7QUFDSDs7QUFDRGdELHlCQUFZTSxhQUFaLENBQTBCbkMsTUFBMUIsRUFBa0NnQixRQUFsQyxFQUE0Q0UsVUFBNUMsRUFBd0RFLFNBQXhELEVBQW1FRSxVQUFuRSxFQUErRUUsVUFBL0UsRUFBMkZYLElBQTNGLENBQWdHLE1BQU07QUFDbEdqQyxNQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUTtBQUNoQjhCLFFBQUFBLE9BQU8sRUFBRTtBQURPLE9BQVIsQ0FBWjtBQUdILEtBSkQsRUFJSUcsR0FBRCxJQUFTO0FBQ1J2QixNQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRyx5QkFBSCxDQUFSLEVBQXVDaUMsR0FBdkMsQ0FBVDtBQUNILEtBTkQ7QUFPSDtBQUNKOztBQUVELFNBQVNzQixVQUFULENBQW9CdkQsS0FBcEIsRUFBMkJtQixNQUEzQixFQUFtQztBQUMvQixRQUFNRyxNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxNQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNUWixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywyQkFBSCxDQUFSLENBQVQ7QUFDQTtBQUNIOztBQUNELE1BQUl3RCxpQkFBaUIsR0FBRyxFQUF4Qjs7QUFFQSxNQUFJckMsTUFBSixFQUFZO0FBQ1IsVUFBTU0sSUFBSSxHQUFHSCxNQUFNLENBQUNJLE9BQVAsQ0FBZVAsTUFBZixDQUFiOztBQUNBLFFBQUksQ0FBQ00sSUFBTCxFQUFXO0FBQ1BmLE1BQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDhCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0gsS0FMTyxDQU1SO0FBQ0E7OztBQUNBd0QsSUFBQUEsaUJBQWlCLEdBQUdSLHFCQUFZUyxjQUFaLENBQTJCaEMsSUFBM0IsRUFBaUNpQyxHQUFqQyxDQUFzQ0MsRUFBRCxJQUFRQSxFQUFFLENBQUMzRCxLQUFoRCxDQUFwQjtBQUNILEdBakI4QixDQW1CL0I7OztBQUNBLFFBQU00RCxXQUFXLEdBQUdaLHFCQUFZYSxtQkFBWixFQUFwQjs7QUFDQUwsRUFBQUEsaUJBQWlCLEdBQUdBLGlCQUFpQixDQUFDTSxNQUFsQixDQUF5QkYsV0FBekIsQ0FBcEI7QUFFQTdELEVBQUFBLFlBQVksQ0FBQ0MsS0FBRCxFQUFRd0QsaUJBQVIsQ0FBWjtBQUNIOztBQUVELFNBQVNPLGVBQVQsQ0FBeUIvRCxLQUF6QixFQUFnQ21CLE1BQWhDLEVBQXdDO0FBQ3BDLFFBQU1HLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLE1BQUksQ0FBQ0YsTUFBTCxFQUFhO0FBQ1RaLElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDJCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0QsUUFBTXlCLElBQUksR0FBR0gsTUFBTSxDQUFDSSxPQUFQLENBQWVQLE1BQWYsQ0FBYjs7QUFDQSxNQUFJLENBQUNNLElBQUwsRUFBVztBQUNQZixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRyw4QkFBSCxDQUFSLENBQVQ7QUFDQTtBQUNIOztBQUNELFFBQU1nRSxlQUFlLEdBQUd6QyxpQ0FBZ0JDLEdBQWhCLEdBQXNCeUMsZUFBdEIsQ0FBc0M5QyxNQUF0QyxDQUF4Qjs7QUFFQXBCLEVBQUFBLFlBQVksQ0FBQ0MsS0FBRCxFQUFRZ0UsZUFBUixDQUFaO0FBQ0g7O0FBRUQsU0FBU0UsZ0JBQVQsQ0FBMEJsRSxLQUExQixFQUFpQ21CLE1BQWpDLEVBQXlDZ0QsTUFBekMsRUFBaUQ7QUFDN0MsTUFBSSxPQUFPQSxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzVCLFVBQU0sSUFBSXJCLEtBQUosQ0FBVSwwQ0FBVixDQUFOO0FBQ0g7O0FBQ0RqQyxFQUFBQSxPQUFPLENBQUNRLEdBQVIsOERBQWlFOEMsTUFBakUsd0JBQW9GaEQsTUFBcEY7O0FBQ0EsUUFBTUcsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsTUFBSSxDQUFDRixNQUFMLEVBQWE7QUFDVFosSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsMkJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFDRHNCLEVBQUFBLE1BQU0sQ0FBQzhDLGNBQVAsQ0FBc0JqRCxNQUF0QixFQUE4QixpQkFBOUIsRUFBaUQ7QUFBRWdELElBQUFBLE1BQU0sRUFBRUE7QUFBVixHQUFqRCxFQUFxRW5DLElBQXJFLENBQTBFLE1BQU07QUFDNUVqQyxJQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUTtBQUNoQjhCLE1BQUFBLE9BQU8sRUFBRTtBQURPLEtBQVIsQ0FBWjtBQUdILEdBSkQsRUFJSUcsR0FBRCxJQUFTO0FBQ1J2QixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUWlDLEdBQUcsQ0FBQ2pCLE9BQUosR0FBY2lCLEdBQUcsQ0FBQ2pCLE9BQWxCLEdBQTRCLHlCQUFHLHlCQUFILENBQXBDLEVBQW1FaUIsR0FBbkUsQ0FBVDtBQUNILEdBTkQ7QUFPSDs7QUFFRCxTQUFTb0MsYUFBVCxDQUF1QnJFLEtBQXZCLEVBQThCbUIsTUFBOUIsRUFBc0NDLE1BQXRDLEVBQThDO0FBQzFDUCxFQUFBQSxPQUFPLENBQUNRLEdBQVIsbURBQXVERCxNQUF2RCxzQkFBeUVELE1BQXpFOztBQUNBLFFBQU1HLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLE1BQUksQ0FBQ0YsTUFBTCxFQUFhO0FBQ1RaLElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDJCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0RzQixFQUFBQSxNQUFNLENBQUM4QyxjQUFQLENBQXNCakQsTUFBdEIsRUFBOEIsb0JBQTlCLEVBQW9EbkIsS0FBSyxDQUFDRSxJQUFOLENBQVdvRSxPQUEvRCxFQUF3RSxNQUFNbEQsTUFBOUUsRUFBc0ZZLElBQXRGLENBQTJGLE1BQU07QUFDN0ZqQyxJQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUTtBQUNoQjhCLE1BQUFBLE9BQU8sRUFBRTtBQURPLEtBQVIsQ0FBWjtBQUdILEdBSkQsRUFJSUcsR0FBRCxJQUFTO0FBQ1J2QixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUWlDLEdBQUcsQ0FBQ2pCLE9BQUosR0FBY2lCLEdBQUcsQ0FBQ2pCLE9BQWxCLEdBQTRCLHlCQUFHLHlCQUFILENBQXBDLEVBQW1FaUIsR0FBbkUsQ0FBVDtBQUNILEdBTkQ7QUFPSDs7QUFFRCxTQUFTc0MsV0FBVCxDQUFxQnZFLEtBQXJCLEVBQTRCbUIsTUFBNUIsRUFBb0NDLE1BQXBDLEVBQTRDb0QsS0FBNUMsRUFBbUQ7QUFDL0MsTUFBSSxFQUFFQyxNQUFNLENBQUNDLFNBQVAsQ0FBaUJGLEtBQWpCLEtBQTJCQSxLQUFLLElBQUksQ0FBdEMsQ0FBSixFQUE4QztBQUMxQzlELElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLHVDQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBRURhLEVBQUFBLE9BQU8sQ0FBQ1EsR0FBUixrREFBc0RtRCxLQUF0RCxzQkFBdUVwRCxNQUF2RSxzQkFBeUZELE1BQXpGOztBQUNBLFFBQU1HLE1BQU0sR0FBR0MsaUNBQWdCQyxHQUFoQixFQUFmOztBQUNBLE1BQUksQ0FBQ0YsTUFBTCxFQUFhO0FBQ1RaLElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDJCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBRURzQixFQUFBQSxNQUFNLENBQUNxRCxhQUFQLENBQXFCeEQsTUFBckIsRUFBNkIscUJBQTdCLEVBQW9ELEVBQXBELEVBQXdEYSxJQUF4RCxDQUE4RDRDLFdBQUQsSUFBaUI7QUFDMUUsVUFBTUMsVUFBVSxHQUFHLElBQUlDLHdCQUFKLENBQ2Y7QUFDSXhDLE1BQUFBLElBQUksRUFBRSxxQkFEVjtBQUVJZ0MsTUFBQUEsT0FBTyxFQUFFTTtBQUZiLEtBRGUsQ0FBbkI7QUFPQXRELElBQUFBLE1BQU0sQ0FBQ3lELGFBQVAsQ0FBcUI1RCxNQUFyQixFQUE2QkMsTUFBN0IsRUFBcUNvRCxLQUFyQyxFQUE0Q0ssVUFBNUMsRUFBd0Q3QyxJQUF4RCxDQUE2RCxNQUFNO0FBQy9EakMsTUFBQUEsWUFBWSxDQUFDQyxLQUFELEVBQVE7QUFDaEI4QixRQUFBQSxPQUFPLEVBQUU7QUFETyxPQUFSLENBQVo7QUFHSCxLQUpELEVBSUlHLEdBQUQsSUFBUztBQUNSdkIsTUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVFpQyxHQUFHLENBQUNqQixPQUFKLEdBQWNpQixHQUFHLENBQUNqQixPQUFsQixHQUE0Qix5QkFBRyx5QkFBSCxDQUFwQyxFQUFtRWlCLEdBQW5FLENBQVQ7QUFDSCxLQU5EO0FBT0gsR0FmRDtBQWdCSDs7QUFFRCxTQUFTK0Msa0JBQVQsQ0FBNEJoRixLQUE1QixFQUFtQ21CLE1BQW5DLEVBQTJDQyxNQUEzQyxFQUFtRDtBQUMvQ1AsRUFBQUEsT0FBTyxDQUFDUSxHQUFSLCtCQUFtQ0QsTUFBbkMsc0JBQXFERCxNQUFyRDtBQUNBOEQsRUFBQUEsZ0JBQWdCLENBQUNqRixLQUFELEVBQVFtQixNQUFSLEVBQWdCLGVBQWhCLEVBQWlDQyxNQUFqQyxDQUFoQjtBQUNIOztBQUVELFNBQVM4RCxZQUFULENBQXNCbEYsS0FBdEIsRUFBNkJtQixNQUE3QixFQUFxQztBQUNqQ04sRUFBQUEsT0FBTyxDQUFDUSxHQUFSLHlCQUE2QkYsTUFBN0I7QUFDQThELEVBQUFBLGdCQUFnQixDQUFDakYsS0FBRCxFQUFRbUIsTUFBUixFQUFnQixtQkFBaEIsRUFBcUMsRUFBckMsQ0FBaEI7QUFDSDs7QUFFRCxTQUFTZ0UsVUFBVCxDQUFvQm5GLEtBQXBCLEVBQTJCbUIsTUFBM0IsRUFBbUNDLE1BQW5DLEVBQTJDO0FBQ3ZDUCxFQUFBQSxPQUFPLENBQUNRLEdBQVIsMEJBQThCRCxNQUE5QixzQkFBZ0RELE1BQWhEO0FBQ0E4RCxFQUFBQSxnQkFBZ0IsQ0FBQ2pGLEtBQUQsRUFBUW1CLE1BQVIsRUFBZ0Isb0JBQWhCLEVBQXNDLE1BQU1DLE1BQTVDLENBQWhCO0FBQ0g7O0FBRUQsU0FBU2dFLGtCQUFULENBQTRCcEYsS0FBNUIsRUFBbUNtQixNQUFuQyxFQUEyQztBQUN2QyxRQUFNRyxNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxNQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNUWixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywyQkFBSCxDQUFSLENBQVQ7QUFDQTtBQUNIOztBQUNELFFBQU15QixJQUFJLEdBQUdILE1BQU0sQ0FBQ0ksT0FBUCxDQUFlUCxNQUFmLENBQWI7O0FBQ0EsTUFBSSxDQUFDTSxJQUFMLEVBQVc7QUFDUGYsSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsOEJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFDRCxRQUFNcUYsS0FBSyxHQUFHNUQsSUFBSSxDQUFDNkQsb0JBQUwsRUFBZDtBQUNBdkYsRUFBQUEsWUFBWSxDQUFDQyxLQUFELEVBQVFxRixLQUFSLENBQVo7QUFDSDs7QUFFRCxTQUFTRSxZQUFULENBQXNCdkYsS0FBdEIsRUFBNkJtQixNQUE3QixFQUFxQztBQUNqQyxRQUFNcUUsTUFBTSxHQUFHLEtBQUt4RixLQUFLLENBQUNFLElBQU4sQ0FBV3VGLFVBQS9CLENBRGlDLENBQ1U7O0FBQzNDLFFBQU1DLE9BQU8sR0FBR0MsT0FBTyxDQUFDM0YsS0FBSyxDQUFDRSxJQUFOLENBQVcwRixRQUFaLENBQXZCOztBQUNBLFFBQU10RSxNQUFNLEdBQUdDLGlDQUFnQkMsR0FBaEIsRUFBZjs7QUFDQSxNQUFJLENBQUNGLE1BQUwsRUFBYTtBQUNUWixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRywyQkFBSCxDQUFSLENBQVQ7QUFDQTtBQUNIOztBQUNELFFBQU15QixJQUFJLEdBQUdILE1BQU0sQ0FBQ0ksT0FBUCxDQUFlUCxNQUFmLENBQWI7O0FBQ0EsTUFBSSxDQUFDTSxJQUFMLEVBQVc7QUFDUGYsSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsOEJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFDRCxNQUFJeUIsSUFBSSxDQUFDb0UsZUFBTCxPQUEyQixNQUEvQixFQUF1QztBQUNuQ25GLElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDJCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0QsUUFBTThGLEVBQUUsR0FBR3hFLE1BQU0sQ0FBQ3lFLFdBQVAsQ0FBbUIzRSxNQUE5QjtBQUVBLE1BQUk0RSxPQUFPLEdBQUcsS0FBZDs7QUFDQSxNQUFJTixPQUFKLEVBQWE7QUFDVE0sSUFBQUEsT0FBTyxHQUFHdkUsSUFBSSxDQUFDd0UsWUFBTCxDQUFrQkMsaUJBQWxCLENBQW9DVixNQUFwQyxFQUE0Q00sRUFBNUMsQ0FBVjtBQUNILEdBRkQsTUFFTztBQUNIRSxJQUFBQSxPQUFPLEdBQUd2RSxJQUFJLENBQUN3RSxZQUFMLENBQWtCRSxZQUFsQixDQUErQlgsTUFBL0IsRUFBdUNNLEVBQXZDLENBQVY7QUFDSDs7QUFFRCxNQUFJLENBQUNFLE9BQUwsRUFBYztBQUNWdEYsSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcscURBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFFREQsRUFBQUEsWUFBWSxDQUFDQyxLQUFELEVBQVEsSUFBUixDQUFaO0FBQ0g7O0FBRUQsU0FBU2lGLGdCQUFULENBQTBCakYsS0FBMUIsRUFBaUNtQixNQUFqQyxFQUF5Q2lGLFNBQXpDLEVBQW9EQyxRQUFwRCxFQUE4RDtBQUMxRCxRQUFNL0UsTUFBTSxHQUFHQyxpQ0FBZ0JDLEdBQWhCLEVBQWY7O0FBQ0EsTUFBSSxDQUFDRixNQUFMLEVBQWE7QUFDVFosSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsMkJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDs7QUFDRCxRQUFNeUIsSUFBSSxHQUFHSCxNQUFNLENBQUNJLE9BQVAsQ0FBZVAsTUFBZixDQUFiOztBQUNBLE1BQUksQ0FBQ00sSUFBTCxFQUFXO0FBQ1BmLElBQUFBLFNBQVMsQ0FBQ1YsS0FBRCxFQUFRLHlCQUFHLDhCQUFILENBQVIsQ0FBVDtBQUNBO0FBQ0g7O0FBQ0QsUUFBTXNHLFVBQVUsR0FBRzdFLElBQUksQ0FBQ3dFLFlBQUwsQ0FBa0JNLGNBQWxCLENBQWlDSCxTQUFqQyxFQUE0Q0MsUUFBNUMsQ0FBbkI7O0FBQ0EsTUFBSSxDQUFDQyxVQUFMLEVBQWlCO0FBQ2J2RyxJQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUSxJQUFSLENBQVo7QUFDQTtBQUNIOztBQUNERCxFQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUXNHLFVBQVUsQ0FBQ0UsVUFBWCxFQUFSLENBQVo7QUFDSDs7QUFFRCxNQUFNQyxTQUFTLEdBQUcsVUFBU3pHLEtBQVQsRUFBZ0I7QUFDOUIsTUFBSSxDQUFDQSxLQUFLLENBQUNTLE1BQVgsRUFBbUI7QUFBRTtBQUNqQlQsSUFBQUEsS0FBSyxDQUFDUyxNQUFOLEdBQWVULEtBQUssQ0FBQzBHLGFBQU4sQ0FBb0JqRyxNQUFuQztBQUNILEdBSDZCLENBSzlCO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFJa0csU0FBSjs7QUFDQSxNQUFJO0FBQ0EsUUFBSSxDQUFDQyxjQUFMLEVBQXFCQSxjQUFjLEdBQUdDLHlDQUFvQkMsY0FBcEIsR0FBcUNDLGlCQUFyQyxHQUF5REMsS0FBMUU7QUFDckJMLElBQUFBLFNBQVMsR0FBRyxJQUFJTSxHQUFKLENBQVFMLGNBQVIsQ0FBWjtBQUNILEdBSEQsQ0FHRSxPQUFPdkQsQ0FBUCxFQUFVO0FBQ1I7QUFDQTtBQUNIOztBQUNELE1BQUk2RCxjQUFKOztBQUNBLE1BQUk7QUFDQUEsSUFBQUEsY0FBYyxHQUFHLElBQUlELEdBQUosQ0FBUWpILEtBQUssQ0FBQ1MsTUFBZCxDQUFqQjtBQUNILEdBRkQsQ0FFRSxPQUFPNEMsQ0FBUCxFQUFVO0FBQ1I7QUFDSCxHQXRCNkIsQ0F1QjlCO0FBQ0E7OztBQUNBLE1BQ0lzRCxTQUFTLENBQUNsRyxNQUFWLEtBQXFCeUcsY0FBYyxDQUFDekcsTUFBcEMsSUFDQSxDQUFDVCxLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFEWixJQUVBZixLQUFLLENBQUNFLElBQU4sQ0FBV2lILEdBSGYsQ0FHbUI7QUFIbkIsSUFJRTtBQUNFO0FBQ0E7QUFDQTtBQUNIOztBQUVELE1BQUluSCxLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBWCxLQUFzQixjQUExQixFQUEwQztBQUN0Q21DLHdCQUFJQyxRQUFKLENBQWE7QUFBRXBDLE1BQUFBLE1BQU0sRUFBRTtBQUFWLEtBQWI7O0FBQ0FoQixJQUFBQSxZQUFZLENBQUNDLEtBQUQsRUFBUSxJQUFSLENBQVo7QUFDQTtBQUNIOztBQUVELFFBQU1tQixNQUFNLEdBQUduQixLQUFLLENBQUNFLElBQU4sQ0FBV2tILE9BQTFCO0FBQ0EsUUFBTWhHLE1BQU0sR0FBR3BCLEtBQUssQ0FBQ0UsSUFBTixDQUFXbUgsT0FBMUI7O0FBRUEsTUFBSSxDQUFDbEcsTUFBTCxFQUFhO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJbkIsS0FBSyxDQUFDRSxJQUFOLENBQVdhLE1BQVgsS0FBc0IsYUFBMUIsRUFBeUM7QUFDckN3QyxNQUFBQSxVQUFVLENBQUN2RCxLQUFELEVBQVEsSUFBUixDQUFWO0FBQ0E7QUFDSCxLQUhELE1BR08sSUFBSUEsS0FBSyxDQUFDRSxJQUFOLENBQVdhLE1BQVgsS0FBc0IsWUFBMUIsRUFBd0M7QUFDM0NtQixNQUFBQSxTQUFTLENBQUNsQyxLQUFELEVBQVEsSUFBUixDQUFUO0FBQ0E7QUFDSCxLQUhNLE1BR0E7QUFDSFUsTUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsNEJBQUgsQ0FBUixDQUFUO0FBQ0E7QUFDSDtBQUNKOztBQUVELE1BQUltQixNQUFNLEtBQUttRyx1QkFBY0MsU0FBZCxFQUFmLEVBQTBDO0FBQ3RDN0csSUFBQUEsU0FBUyxDQUFDVixLQUFELEVBQVEseUJBQUcsNkJBQUgsRUFBa0M7QUFBQ21CLE1BQUFBLE1BQU0sRUFBRUE7QUFBVCxLQUFsQyxDQUFSLENBQVQ7QUFDQTtBQUNILEdBaEU2QixDQWtFOUI7OztBQUNBLE1BQUluQixLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBWCxLQUFzQixhQUExQixFQUF5QztBQUNyQ3dDLElBQUFBLFVBQVUsQ0FBQ3ZELEtBQUQsRUFBUW1CLE1BQVIsQ0FBVjtBQUNBO0FBQ0gsR0FIRCxNQUdPLElBQUluQixLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBWCxLQUFzQixZQUExQixFQUF3QztBQUMzQ21CLElBQUFBLFNBQVMsQ0FBQ2xDLEtBQUQsRUFBUW1CLE1BQVIsQ0FBVDtBQUNBO0FBQ0gsR0F6RTZCLENBMkU5Qjs7O0FBQ0EsTUFBSW5CLEtBQUssQ0FBQ0UsSUFBTixDQUFXYSxNQUFYLEtBQXNCLGtCQUExQixFQUE4QztBQUMxQ21FLElBQUFBLFlBQVksQ0FBQ2xGLEtBQUQsRUFBUW1CLE1BQVIsQ0FBWjtBQUNBO0FBQ0gsR0FIRCxNQUdPLElBQUluQixLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBWCxLQUFzQixvQkFBMUIsRUFBZ0Q7QUFDbkRtRCxJQUFBQSxnQkFBZ0IsQ0FBQ2xFLEtBQUQsRUFBUW1CLE1BQVIsRUFBZ0JuQixLQUFLLENBQUNFLElBQU4sQ0FBV2lFLE1BQTNCLENBQWhCO0FBQ0E7QUFDSCxHQUhNLE1BR0EsSUFBSW5FLEtBQUssQ0FBQ0UsSUFBTixDQUFXYSxNQUFYLEtBQXNCLHNCQUExQixFQUFrRDtBQUNyRHFFLElBQUFBLGtCQUFrQixDQUFDcEYsS0FBRCxFQUFRbUIsTUFBUixDQUFsQjtBQUNBO0FBQ0gsR0FITSxNQUdBLElBQUluQixLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBWCxLQUFzQixvQkFBMUIsRUFBZ0Q7QUFDbkRnRCxJQUFBQSxlQUFlLENBQUMvRCxLQUFELEVBQVFtQixNQUFSLENBQWY7QUFDQTtBQUNILEdBSE0sTUFHQSxJQUFJbkIsS0FBSyxDQUFDRSxJQUFOLENBQVdhLE1BQVgsS0FBc0IsZ0JBQTFCLEVBQTRDO0FBQy9Dd0UsSUFBQUEsWUFBWSxDQUFDdkYsS0FBRCxFQUFRbUIsTUFBUixDQUFaO0FBQ0E7QUFDSDs7QUFFRCxNQUFJLENBQUNDLE1BQUwsRUFBYTtBQUNUVixJQUFBQSxTQUFTLENBQUNWLEtBQUQsRUFBUSx5QkFBRyw0QkFBSCxDQUFSLENBQVQ7QUFDQTtBQUNIOztBQUNELFVBQVFBLEtBQUssQ0FBQ0UsSUFBTixDQUFXYSxNQUFuQjtBQUNJLFNBQUssa0JBQUw7QUFDSWlFLE1BQUFBLGtCQUFrQixDQUFDaEYsS0FBRCxFQUFRbUIsTUFBUixFQUFnQkMsTUFBaEIsQ0FBbEI7QUFDQTs7QUFDSixTQUFLLFFBQUw7QUFDSUYsTUFBQUEsVUFBVSxDQUFDbEIsS0FBRCxFQUFRbUIsTUFBUixFQUFnQkMsTUFBaEIsQ0FBVjtBQUNBOztBQUNKLFNBQUssYUFBTDtBQUNJK0QsTUFBQUEsVUFBVSxDQUFDbkYsS0FBRCxFQUFRbUIsTUFBUixFQUFnQkMsTUFBaEIsQ0FBVjtBQUNBOztBQUNKLFNBQUssaUJBQUw7QUFDSWlELE1BQUFBLGFBQWEsQ0FBQ3JFLEtBQUQsRUFBUW1CLE1BQVIsRUFBZ0JDLE1BQWhCLENBQWI7QUFDQTs7QUFDSixTQUFLLGVBQUw7QUFDSW1ELE1BQUFBLFdBQVcsQ0FBQ3ZFLEtBQUQsRUFBUW1CLE1BQVIsRUFBZ0JDLE1BQWhCLEVBQXdCcEIsS0FBSyxDQUFDRSxJQUFOLENBQVdzRSxLQUFuQyxDQUFYO0FBQ0E7O0FBQ0o7QUFDSTNELE1BQUFBLE9BQU8sQ0FBQzJHLElBQVIsQ0FBYSw4Q0FBOEN4SCxLQUFLLENBQUNFLElBQU4sQ0FBV2EsTUFBekQsR0FBaUUsR0FBOUU7QUFDQTtBQWxCUjtBQW9CSCxDQXJIRDs7QUF1SEEsSUFBSTBHLGFBQWEsR0FBRyxDQUFwQjtBQUNBLElBQUliLGNBQWMsR0FBRyxJQUFyQjs7QUFFTyxTQUFTYyxjQUFULEdBQTBCO0FBQzdCLE1BQUlELGFBQWEsS0FBSyxDQUF0QixFQUF5QjtBQUNyQkUsSUFBQUEsTUFBTSxDQUFDQyxnQkFBUCxDQUF3QixTQUF4QixFQUFtQ25CLFNBQW5DLEVBQThDLEtBQTlDO0FBQ0g7O0FBQ0RnQixFQUFBQSxhQUFhLElBQUksQ0FBakI7QUFDSDs7QUFFTSxTQUFTSSxhQUFULEdBQXlCO0FBQzVCSixFQUFBQSxhQUFhLElBQUksQ0FBakI7O0FBQ0EsTUFBSUEsYUFBYSxLQUFLLENBQXRCLEVBQXlCO0FBQ3JCRSxJQUFBQSxNQUFNLENBQUNHLG1CQUFQLENBQTJCLFNBQTNCLEVBQXNDckIsU0FBdEM7QUFDSDs7QUFDRCxNQUFJZ0IsYUFBYSxHQUFHLENBQXBCLEVBQXVCO0FBQ25CO0FBQ0EsVUFBTXBFLENBQUMsR0FBRyxJQUFJUCxLQUFKLENBQ04seUVBQ0EsaUJBRk0sQ0FBVjtBQUlBakMsSUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWN1QyxDQUFkO0FBQ0g7QUFDSjs7QUFFTSxTQUFTMEUsaUJBQVQsQ0FBMkJ2RixHQUEzQixFQUFnQztBQUNuQ29FLEVBQUFBLGNBQWMsR0FBR3BFLEdBQWpCO0FBQ0giLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbi8qXHJcbkxpc3RlbnMgZm9yIGluY29taW5nIHBvc3RNZXNzYWdlIHJlcXVlc3RzIGZyb20gdGhlIGludGVncmF0aW9ucyBVSSBVUkwuIFRoZSBmb2xsb3dpbmcgQVBJIGlzIGV4cG9zZWQ6XHJcbntcclxuICAgIGFjdGlvbjogXCJpbnZpdGVcIiB8IFwibWVtYmVyc2hpcF9zdGF0ZVwiIHwgXCJib3Rfb3B0aW9uc1wiIHwgXCJzZXRfYm90X29wdGlvbnNcIiB8IGV0Yy4uLiAsXHJcbiAgICByb29tX2lkOiAkUk9PTV9JRCxcclxuICAgIHVzZXJfaWQ6ICRVU0VSX0lEXHJcbiAgICAvLyBhZGRpdGlvbmFsIHJlcXVlc3QgZmllbGRzXHJcbn1cclxuXHJcblRoZSBjb21wbGV0ZSByZXF1ZXN0IG9iamVjdCBpcyByZXR1cm5lZCB0byB0aGUgY2FsbGVyIHdpdGggYW4gYWRkaXRpb25hbCBcInJlc3BvbnNlXCIga2V5IGxpa2Ugc286XHJcbntcclxuICAgIGFjdGlvbjogXCJpbnZpdGVcIiB8IFwibWVtYmVyc2hpcF9zdGF0ZVwiIHwgXCJib3Rfb3B0aW9uc1wiIHwgXCJzZXRfYm90X29wdGlvbnNcIixcclxuICAgIHJvb21faWQ6ICRST09NX0lELFxyXG4gICAgdXNlcl9pZDogJFVTRVJfSUQsXHJcbiAgICAvLyBhZGRpdGlvbmFsIHJlcXVlc3QgZmllbGRzXHJcbiAgICByZXNwb25zZTogeyAuLi4gfVxyXG59XHJcblxyXG5UaGUgXCJhY3Rpb25cIiBkZXRlcm1pbmVzIHRoZSBmb3JtYXQgb2YgdGhlIHJlcXVlc3QgYW5kIHJlc3BvbnNlLiBBbGwgYWN0aW9ucyBjYW4gcmV0dXJuIGFuIGVycm9yIHJlc3BvbnNlLlxyXG5BbiBlcnJvciByZXNwb25zZSBpcyBhIFwicmVzcG9uc2VcIiBvYmplY3Qgd2hpY2ggY29uc2lzdHMgb2YgYSBzb2xlIFwiZXJyb3JcIiBrZXkgdG8gaW5kaWNhdGUgYW4gZXJyb3IuXHJcblRoZXkgbG9vayBsaWtlOlxyXG57XHJcbiAgICBlcnJvcjoge1xyXG4gICAgICAgIG1lc3NhZ2U6IFwiVW5hYmxlIHRvIGludml0ZSB1c2VyIGludG8gcm9vbS5cIixcclxuICAgICAgICBfZXJyb3I6IDxPcmlnaW5hbCBFcnJvciBPYmplY3Q+XHJcbiAgICB9XHJcbn1cclxuVGhlIFwibWVzc2FnZVwiIGtleSBzaG91bGQgYmUgYSBodW1hbi1mcmllbmRseSBzdHJpbmcuXHJcblxyXG5BQ1RJT05TXHJcbj09PT09PT1cclxuQWxsIGFjdGlvbnMgY2FuIHJldHVybiBhbiBlcnJvciByZXNwb25zZSBpbnN0ZWFkIG9mIHRoZSByZXNwb25zZSBvdXRsaW5lZCBiZWxvdy5cclxuXHJcbmludml0ZVxyXG4tLS0tLS1cclxuSW52aXRlcyBhIHVzZXIgaW50byBhIHJvb20uXHJcblxyXG5SZXF1ZXN0OlxyXG4gLSByb29tX2lkIGlzIHRoZSByb29tIHRvIGludml0ZSB0aGUgdXNlciBpbnRvLlxyXG4gLSB1c2VyX2lkIGlzIHRoZSB1c2VyIElEIHRvIGludml0ZS5cclxuIC0gTm8gYWRkaXRpb25hbCBmaWVsZHMuXHJcblJlc3BvbnNlOlxyXG57XHJcbiAgICBzdWNjZXNzOiB0cnVlXHJcbn1cclxuRXhhbXBsZTpcclxue1xyXG4gICAgYWN0aW9uOiBcImludml0ZVwiLFxyXG4gICAgcm9vbV9pZDogXCIhZm9vOmJhclwiLFxyXG4gICAgdXNlcl9pZDogXCJAaW52aXRlZTpiYXJcIixcclxuICAgIHJlc3BvbnNlOiB7XHJcbiAgICAgICAgc3VjY2VzczogdHJ1ZVxyXG4gICAgfVxyXG59XHJcblxyXG5zZXRfYm90X29wdGlvbnNcclxuLS0tLS0tLS0tLS0tLS0tXHJcblNldCB0aGUgbS5yb29tLmJvdC5vcHRpb25zIHN0YXRlIGV2ZW50IGZvciBhIGJvdCB1c2VyLlxyXG5cclxuUmVxdWVzdDpcclxuIC0gcm9vbV9pZCBpcyB0aGUgcm9vbSB0byBzZW5kIHRoZSBzdGF0ZSBldmVudCBpbnRvLlxyXG4gLSB1c2VyX2lkIGlzIHRoZSB1c2VyIElEIG9mIHRoZSBib3Qgd2hvIHlvdSdyZSBzZXR0aW5nIG9wdGlvbnMgZm9yLlxyXG4gLSBcImNvbnRlbnRcIiBpcyBhbiBvYmplY3QgY29uc2lzdGluZyBvZiB0aGUgY29udGVudCB5b3Ugd2lzaCB0byBzZXQuXHJcblJlc3BvbnNlOlxyXG57XHJcbiAgICBzdWNjZXNzOiB0cnVlXHJcbn1cclxuRXhhbXBsZTpcclxue1xyXG4gICAgYWN0aW9uOiBcInNldF9ib3Rfb3B0aW9uc1wiLFxyXG4gICAgcm9vbV9pZDogXCIhZm9vOmJhclwiLFxyXG4gICAgdXNlcl9pZDogXCJAYm90OmJhclwiLFxyXG4gICAgY29udGVudDoge1xyXG4gICAgICAgIGRlZmF1bHRfb3B0aW9uOiBcImFscGhhXCJcclxuICAgIH0sXHJcbiAgICByZXNwb25zZToge1xyXG4gICAgICAgIHN1Y2Nlc3M6IHRydWVcclxuICAgIH1cclxufVxyXG5cclxuZ2V0X21lbWJlcnNoaXBfY291bnRcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuR2V0IHRoZSBudW1iZXIgb2Ygam9pbmVkIHVzZXJzIGluIHRoZSByb29tLlxyXG5cclxuUmVxdWVzdDpcclxuIC0gcm9vbV9pZCBpcyB0aGUgcm9vbSB0byBnZXQgdGhlIGNvdW50IGluLlxyXG5SZXNwb25zZTpcclxuNzhcclxuRXhhbXBsZTpcclxue1xyXG4gICAgYWN0aW9uOiBcImdldF9tZW1iZXJzaGlwX2NvdW50XCIsXHJcbiAgICByb29tX2lkOiBcIiFmb286YmFyXCIsXHJcbiAgICByZXNwb25zZTogNzhcclxufVxyXG5cclxuY2FuX3NlbmRfZXZlbnRcclxuLS0tLS0tLS0tLS0tLS1cclxuQ2hlY2sgaWYgdGhlIGNsaWVudCBjYW4gc2VuZCB0aGUgZ2l2ZW4gZXZlbnQgaW50byB0aGUgZ2l2ZW4gcm9vbS4gSWYgdGhlIGNsaWVudFxyXG5pcyB1bmFibGUgdG8gZG8gdGhpcywgYW4gZXJyb3IgcmVzcG9uc2UgaXMgcmV0dXJuZWQgaW5zdGVhZCBvZiAncmVzcG9uc2U6IGZhbHNlJy5cclxuXHJcblJlcXVlc3Q6XHJcbiAtIHJvb21faWQgaXMgdGhlIHJvb20gdG8gZG8gdGhlIGNoZWNrIGluLlxyXG4gLSBldmVudF90eXBlIGlzIHRoZSBldmVudCB0eXBlIHdoaWNoIHdpbGwgYmUgc2VudC5cclxuIC0gaXNfc3RhdGUgaXMgdHJ1ZSBpZiB0aGUgZXZlbnQgdG8gYmUgc2VudCBpcyBhIHN0YXRlIGV2ZW50LlxyXG5SZXNwb25zZTpcclxudHJ1ZVxyXG5FeGFtcGxlOlxyXG57XHJcbiAgICBhY3Rpb246IFwiY2FuX3NlbmRfZXZlbnRcIixcclxuICAgIGlzX3N0YXRlOiBmYWxzZSxcclxuICAgIGV2ZW50X3R5cGU6IFwibS5yb29tLm1lc3NhZ2VcIixcclxuICAgIHJvb21faWQ6IFwiIWZvbzpiYXJcIixcclxuICAgIHJlc3BvbnNlOiB0cnVlXHJcbn1cclxuXHJcbnNldF93aWRnZXRcclxuLS0tLS0tLS0tLVxyXG5TZXQgYSBuZXcgd2lkZ2V0IGluIHRoZSByb29tLiBDbG9iYmVycyBiYXNlZCBvbiB0aGUgSUQuXHJcblxyXG5SZXF1ZXN0OlxyXG4gLSBgcm9vbV9pZGAgKFN0cmluZykgaXMgdGhlIHJvb20gdG8gc2V0IHRoZSB3aWRnZXQgaW4uXHJcbiAtIGB3aWRnZXRfaWRgIChTdHJpbmcpIGlzIHRoZSBJRCBvZiB0aGUgd2lkZ2V0IHRvIGFkZCAob3IgcmVwbGFjZSBpZiBpdCBhbHJlYWR5IGV4aXN0cykuXHJcbiAgIEl0IGNhbiBiZSBhbiBhcmJpdHJhcnkgVVRGOCBzdHJpbmcgYW5kIGlzIHB1cmVseSBmb3IgZGlzdGluZ3Vpc2hpbmcgYmV0d2VlbiB3aWRnZXRzLlxyXG4gLSBgdXJsYCAoU3RyaW5nKSBpcyB0aGUgVVJMIHRoYXQgY2xpZW50cyBzaG91bGQgbG9hZCBpbiBhbiBpZnJhbWUgdG8gcnVuIHRoZSB3aWRnZXQuXHJcbiAgIEFsbCB3aWRnZXRzIG11c3QgaGF2ZSBhIHZhbGlkIFVSTC4gSWYgdGhlIFVSTCBpcyBgbnVsbGAgKG5vdCBgdW5kZWZpbmVkYCksIHRoZVxyXG4gICB3aWRnZXQgd2lsbCBiZSByZW1vdmVkIGZyb20gdGhlIHJvb20uXHJcbiAtIGB0eXBlYCAoU3RyaW5nKSBpcyB0aGUgdHlwZSBvZiB3aWRnZXQsIHdoaWNoIGlzIHByb3ZpZGVkIGFzIGEgaGludCBmb3IgbWF0cml4IGNsaWVudHMgc28gdGhleVxyXG4gICBjYW4gY29uZmlndXJlL2xheSBvdXQgdGhlIHdpZGdldCBpbiBkaWZmZXJlbnQgd2F5cy4gQWxsIHdpZGdldHMgbXVzdCBoYXZlIGEgdHlwZS5cclxuIC0gYG5hbWVgIChTdHJpbmcpIGlzIGFuIG9wdGlvbmFsIGh1bWFuLXJlYWRhYmxlIHN0cmluZyBhYm91dCB0aGUgd2lkZ2V0LlxyXG4gLSBgZGF0YWAgKE9iamVjdCkgaXMgc29tZSBvcHRpb25hbCBkYXRhIGFib3V0IHRoZSB3aWRnZXQsIGFuZCBjYW4gY29udGFpbiBhcmJpdHJhcnkga2V5L3ZhbHVlIHBhaXJzLlxyXG5SZXNwb25zZTpcclxue1xyXG4gICAgc3VjY2VzczogdHJ1ZVxyXG59XHJcbkV4YW1wbGU6XHJcbntcclxuICAgIGFjdGlvbjogXCJzZXRfd2lkZ2V0XCIsXHJcbiAgICByb29tX2lkOiBcIiFmb286YmFyXCIsXHJcbiAgICB3aWRnZXRfaWQ6IFwiYWJjMTIzXCIsXHJcbiAgICB1cmw6IFwiaHR0cDovL3dpZGdldC51cmxcIixcclxuICAgIHR5cGU6IFwiZXhhbXBsZVwiLFxyXG4gICAgcmVzcG9uc2U6IHtcclxuICAgICAgICBzdWNjZXNzOiB0cnVlXHJcbiAgICB9XHJcbn1cclxuXHJcbmdldF93aWRnZXRzXHJcbi0tLS0tLS0tLS0tXHJcbkdldCBhIGxpc3Qgb2YgYWxsIHdpZGdldHMgaW4gdGhlIHJvb20uIFRoZSByZXNwb25zZSBpcyBhbiBhcnJheVxyXG5vZiBzdGF0ZSBldmVudHMuXHJcblxyXG5SZXF1ZXN0OlxyXG4gLSBgcm9vbV9pZGAgKFN0cmluZykgaXMgdGhlIHJvb20gdG8gZ2V0IHRoZSB3aWRnZXRzIGluLlxyXG5SZXNwb25zZTpcclxuW1xyXG4gICAge1xyXG4gICAgICAgIHR5cGU6IFwiaW0udmVjdG9yLm1vZHVsYXIud2lkZ2V0c1wiLFxyXG4gICAgICAgIHN0YXRlX2tleTogXCJ3aWQxXCIsXHJcbiAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICB0eXBlOiBcImdyYWZhbmFcIixcclxuICAgICAgICAgICAgdXJsOiBcImh0dHBzOi8vZ3JhZmFuYXVybFwiLFxyXG4gICAgICAgICAgICBuYW1lOiBcImRhc2hib2FyZFwiLFxyXG4gICAgICAgICAgICBkYXRhOiB7a2V5OiBcInZhbFwifVxyXG4gICAgICAgIH1cclxuICAgICAgICByb29tX2lkOiDigJwhZm9vOmJhcuKAnSxcclxuICAgICAgICBzZW5kZXI6IFwiQGFsaWNlOmxvY2FsaG9zdFwiXHJcbiAgICB9XHJcbl1cclxuRXhhbXBsZTpcclxue1xyXG4gICAgYWN0aW9uOiBcImdldF93aWRnZXRzXCIsXHJcbiAgICByb29tX2lkOiBcIiFmb286YmFyXCIsXHJcbiAgICByZXNwb25zZTogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdHlwZTogXCJpbS52ZWN0b3IubW9kdWxhci53aWRnZXRzXCIsXHJcbiAgICAgICAgICAgIHN0YXRlX2tleTogXCJ3aWQxXCIsXHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6IFwiZ3JhZmFuYVwiLFxyXG4gICAgICAgICAgICAgICAgdXJsOiBcImh0dHBzOi8vZ3JhZmFuYXVybFwiLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogXCJkYXNoYm9hcmRcIixcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtrZXk6IFwidmFsXCJ9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcm9vbV9pZDog4oCcIWZvbzpiYXLigJ0sXHJcbiAgICAgICAgICAgIHNlbmRlcjogXCJAYWxpY2U6bG9jYWxob3N0XCJcclxuICAgICAgICB9XHJcbiAgICBdXHJcbn1cclxuXHJcblxyXG5tZW1iZXJzaGlwX3N0YXRlIEFORCBib3Rfb3B0aW9uc1xyXG4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5HZXQgdGhlIGNvbnRlbnQgb2YgdGhlIFwibS5yb29tLm1lbWJlclwiIG9yIFwibS5yb29tLmJvdC5vcHRpb25zXCIgc3RhdGUgZXZlbnQgcmVzcGVjdGl2ZWx5LlxyXG5cclxuTkI6IFdoaWxzdCB0aGlzIEFQSSBpcyBiYXNpY2FsbHkgZXF1aXZhbGVudCB0byBnZXRTdGF0ZUV2ZW50LCB3ZSBzcGVjaWZpY2FsbHkgZG8gbm90XHJcbiAgICB3YW50IGV4dGVybmFsIGVudGl0aWVzIHRvIGJlIGFibGUgdG8gcXVlcnkgYW55IHN0YXRlIGV2ZW50IGZvciBhbnkgcm9vbSwgaGVuY2UgdGhlXHJcbiAgICByZXN0cmljdGl2ZSBBUEkgb3V0bGluZWQgaGVyZS5cclxuXHJcblJlcXVlc3Q6XHJcbiAtIHJvb21faWQgaXMgdGhlIHJvb20gd2hpY2ggaGFzIHRoZSBzdGF0ZSBldmVudC5cclxuIC0gdXNlcl9pZCBpcyB0aGUgc3RhdGVfa2V5IHBhcmFtZXRlciB3aGljaCBpbiBib3RoIGNhc2VzIGlzIGEgdXNlciBJRCAodGhlIG1lbWJlciBvciB0aGUgYm90KS5cclxuIC0gTm8gYWRkaXRpb25hbCBmaWVsZHMuXHJcblJlc3BvbnNlOlxyXG4gLSBUaGUgZXZlbnQgY29udGVudC4gSWYgdGhlcmUgaXMgbm8gc3RhdGUgZXZlbnQsIHRoZSBcInJlc3BvbnNlXCIga2V5IHNob3VsZCBiZSBudWxsLlxyXG5FeGFtcGxlOlxyXG57XHJcbiAgICBhY3Rpb246IFwibWVtYmVyc2hpcF9zdGF0ZVwiLFxyXG4gICAgcm9vbV9pZDogXCIhZm9vOmJhclwiLFxyXG4gICAgdXNlcl9pZDogXCJAc29tZW1lbWJlcjpiYXJcIixcclxuICAgIHJlc3BvbnNlOiB7XHJcbiAgICAgICAgbWVtYmVyc2hpcDogXCJqb2luXCIsXHJcbiAgICAgICAgZGlzcGxheW5hbWU6IFwiQm9iXCIsXHJcbiAgICAgICAgYXZhdGFyX3VybDogbnVsbFxyXG4gICAgfVxyXG59XHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgeyBNYXRyaXhFdmVudCB9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4vZGlzcGF0Y2hlcic7XHJcbmltcG9ydCBXaWRnZXRVdGlscyBmcm9tICcuL3V0aWxzL1dpZGdldFV0aWxzJztcclxuaW1wb3J0IFJvb21WaWV3U3RvcmUgZnJvbSAnLi9zdG9yZXMvUm9vbVZpZXdTdG9yZSc7XHJcbmltcG9ydCB7IF90IH0gZnJvbSAnLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQge0ludGVncmF0aW9uTWFuYWdlcnN9IGZyb20gXCIuL2ludGVncmF0aW9ucy9JbnRlZ3JhdGlvbk1hbmFnZXJzXCI7XHJcblxyXG5mdW5jdGlvbiBzZW5kUmVzcG9uc2UoZXZlbnQsIHJlcykge1xyXG4gICAgY29uc3QgZGF0YSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoZXZlbnQuZGF0YSkpO1xyXG4gICAgZGF0YS5yZXNwb25zZSA9IHJlcztcclxuICAgIGV2ZW50LnNvdXJjZS5wb3N0TWVzc2FnZShkYXRhLCBldmVudC5vcmlnaW4pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzZW5kRXJyb3IoZXZlbnQsIG1zZywgbmVzdGVkRXJyb3IpIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoXCJBY3Rpb246XCIgKyBldmVudC5kYXRhLmFjdGlvbiArIFwiIGZhaWxlZCB3aXRoIG1lc3NhZ2U6IFwiICsgbXNnKTtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGV2ZW50LmRhdGEpKTtcclxuICAgIGRhdGEucmVzcG9uc2UgPSB7XHJcbiAgICAgICAgZXJyb3I6IHtcclxuICAgICAgICAgICAgbWVzc2FnZTogbXNnLFxyXG4gICAgICAgIH0sXHJcbiAgICB9O1xyXG4gICAgaWYgKG5lc3RlZEVycm9yKSB7XHJcbiAgICAgICAgZGF0YS5yZXNwb25zZS5lcnJvci5fZXJyb3IgPSBuZXN0ZWRFcnJvcjtcclxuICAgIH1cclxuICAgIGV2ZW50LnNvdXJjZS5wb3N0TWVzc2FnZShkYXRhLCBldmVudC5vcmlnaW4pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBpbnZpdGVVc2VyKGV2ZW50LCByb29tSWQsIHVzZXJJZCkge1xyXG4gICAgY29uc29sZS5sb2coYFJlY2VpdmVkIHJlcXVlc3QgdG8gaW52aXRlICR7dXNlcklkfSBpbnRvIHJvb20gJHtyb29tSWR9YCk7XHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBuZWVkIHRvIGJlIGxvZ2dlZCBpbi4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICBpZiAocm9vbSkge1xyXG4gICAgICAgIC8vIGlmIHRoZXkgYXJlIGFscmVhZHkgaW52aXRlZCB3ZSBjYW4gcmVzb2x2ZSBpbW1lZGlhdGVseS5cclxuICAgICAgICBjb25zdCBtZW1iZXIgPSByb29tLmdldE1lbWJlcih1c2VySWQpO1xyXG4gICAgICAgIGlmIChtZW1iZXIgJiYgbWVtYmVyLm1lbWJlcnNoaXAgPT09IFwiaW52aXRlXCIpIHtcclxuICAgICAgICAgICAgc2VuZFJlc3BvbnNlKGV2ZW50LCB7XHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiB0cnVlLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjbGllbnQuaW52aXRlKHJvb21JZCwgdXNlcklkKS50aGVuKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHNlbmRSZXNwb25zZShldmVudCwge1xyXG4gICAgICAgICAgICBzdWNjZXNzOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnWW91IG5lZWQgdG8gYmUgYWJsZSB0byBpbnZpdGUgdXNlcnMgdG8gZG8gdGhhdC4nKSwgZXJyKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzZXRXaWRnZXQoZXZlbnQsIHJvb21JZCkge1xyXG4gICAgY29uc3Qgd2lkZ2V0SWQgPSBldmVudC5kYXRhLndpZGdldF9pZDtcclxuICAgIGNvbnN0IHdpZGdldFR5cGUgPSBldmVudC5kYXRhLnR5cGU7XHJcbiAgICBjb25zdCB3aWRnZXRVcmwgPSBldmVudC5kYXRhLnVybDtcclxuICAgIGNvbnN0IHdpZGdldE5hbWUgPSBldmVudC5kYXRhLm5hbWU7IC8vIG9wdGlvbmFsXHJcbiAgICBjb25zdCB3aWRnZXREYXRhID0gZXZlbnQuZGF0YS5kYXRhOyAvLyBvcHRpb25hbFxyXG4gICAgY29uc3QgdXNlcldpZGdldCA9IGV2ZW50LmRhdGEudXNlcldpZGdldDtcclxuXHJcbiAgICAvLyBib3RoIGFkZGluZy9yZW1vdmluZyB3aWRnZXRzIG5lZWQgdGhlc2UgY2hlY2tzXHJcbiAgICBpZiAoIXdpZGdldElkIHx8IHdpZGdldFVybCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdChcIlVuYWJsZSB0byBjcmVhdGUgd2lkZ2V0LlwiKSwgbmV3IEVycm9yKFwiTWlzc2luZyByZXF1aXJlZCB3aWRnZXQgZmllbGRzLlwiKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh3aWRnZXRVcmwgIT09IG51bGwpIHsgLy8gaWYgdXJsIGlzIG51bGwgaXQgaXMgYmVpbmcgZGVsZXRlZCwgZG9uJ3QgbmVlZCB0byBjaGVjayBuYW1lL3R5cGUvZXRjXHJcbiAgICAgICAgLy8gY2hlY2sgdHlwZXMgb2YgZmllbGRzXHJcbiAgICAgICAgaWYgKHdpZGdldE5hbWUgIT09IHVuZGVmaW5lZCAmJiB0eXBlb2Ygd2lkZ2V0TmFtZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdChcIlVuYWJsZSB0byBjcmVhdGUgd2lkZ2V0LlwiKSwgbmV3IEVycm9yKFwiT3B0aW9uYWwgZmllbGQgJ25hbWUnIG11c3QgYmUgYSBzdHJpbmcuXCIpKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAod2lkZ2V0RGF0YSAhPT0gdW5kZWZpbmVkICYmICEod2lkZ2V0RGF0YSBpbnN0YW5jZW9mIE9iamVjdCkpIHtcclxuICAgICAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdChcIlVuYWJsZSB0byBjcmVhdGUgd2lkZ2V0LlwiKSwgbmV3IEVycm9yKFwiT3B0aW9uYWwgZmllbGQgJ2RhdGEnIG11c3QgYmUgYW4gT2JqZWN0LlwiKSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiB3aWRnZXRUeXBlICE9PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KFwiVW5hYmxlIHRvIGNyZWF0ZSB3aWRnZXQuXCIpLCBuZXcgRXJyb3IoXCJGaWVsZCAndHlwZScgbXVzdCBiZSBhIHN0cmluZy5cIikpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0eXBlb2Ygd2lkZ2V0VXJsICE9PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KFwiVW5hYmxlIHRvIGNyZWF0ZSB3aWRnZXQuXCIpLCBuZXcgRXJyb3IoXCJGaWVsZCAndXJsJyBtdXN0IGJlIGEgc3RyaW5nIG9yIG51bGwuXCIpKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodXNlcldpZGdldCkge1xyXG4gICAgICAgIFdpZGdldFV0aWxzLnNldFVzZXJXaWRnZXQod2lkZ2V0SWQsIHdpZGdldFR5cGUsIHdpZGdldFVybCwgd2lkZ2V0TmFtZSwgd2lkZ2V0RGF0YSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHNlbmRSZXNwb25zZShldmVudCwge1xyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246IFwidXNlcl93aWRnZXRfdXBkYXRlZFwiIH0pO1xyXG4gICAgICAgIH0pLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1VuYWJsZSB0byBjcmVhdGUgd2lkZ2V0LicpLCBlKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7IC8vIFJvb20gd2lkZ2V0XHJcbiAgICAgICAgaWYgKCFyb29tSWQpIHtcclxuICAgICAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnTWlzc2luZyByb29tSWQuJyksIG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBXaWRnZXRVdGlscy5zZXRSb29tV2lkZ2V0KHJvb21JZCwgd2lkZ2V0SWQsIHdpZGdldFR5cGUsIHdpZGdldFVybCwgd2lkZ2V0TmFtZSwgd2lkZ2V0RGF0YSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHNlbmRSZXNwb25zZShldmVudCwge1xyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSwgKGVycikgPT4ge1xyXG4gICAgICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdGYWlsZWQgdG8gc2VuZCByZXF1ZXN0LicpLCBlcnIpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRXaWRnZXRzKGV2ZW50LCByb29tSWQpIHtcclxuICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgIGlmICghY2xpZW50KSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnWW91IG5lZWQgdG8gYmUgbG9nZ2VkIGluLicpKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBsZXQgd2lkZ2V0U3RhdGVFdmVudHMgPSBbXTtcclxuXHJcbiAgICBpZiAocm9vbUlkKSB7XHJcbiAgICAgICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICAgICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1RoaXMgcm9vbSBpcyBub3QgcmVjb2duaXNlZC4nKSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gWFhYOiBUaGlzIGdldHMgdGhlIHJhdyBldmVudCBvYmplY3QgKEkgdGhpbmsgYmVjYXVzZSB3ZSBjYW4ndFxyXG4gICAgICAgIC8vIHNlbmQgdGhlIE1hdHJpeEV2ZW50IG92ZXIgcG9zdE1lc3NhZ2U/KVxyXG4gICAgICAgIHdpZGdldFN0YXRlRXZlbnRzID0gV2lkZ2V0VXRpbHMuZ2V0Um9vbVdpZGdldHMocm9vbSkubWFwKChldikgPT4gZXYuZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFkZCB1c2VyIHdpZGdldHMgKG5vdCBsaW5rZWQgdG8gYSBzcGVjaWZpYyByb29tKVxyXG4gICAgY29uc3QgdXNlcldpZGdldHMgPSBXaWRnZXRVdGlscy5nZXRVc2VyV2lkZ2V0c0FycmF5KCk7XHJcbiAgICB3aWRnZXRTdGF0ZUV2ZW50cyA9IHdpZGdldFN0YXRlRXZlbnRzLmNvbmNhdCh1c2VyV2lkZ2V0cyk7XHJcblxyXG4gICAgc2VuZFJlc3BvbnNlKGV2ZW50LCB3aWRnZXRTdGF0ZUV2ZW50cyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldFJvb21FbmNTdGF0ZShldmVudCwgcm9vbUlkKSB7XHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBuZWVkIHRvIGJlIGxvZ2dlZCBpbi4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdUaGlzIHJvb20gaXMgbm90IHJlY29nbmlzZWQuJykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNvbnN0IHJvb21Jc0VuY3J5cHRlZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc1Jvb21FbmNyeXB0ZWQocm9vbUlkKTtcclxuXHJcbiAgICBzZW5kUmVzcG9uc2UoZXZlbnQsIHJvb21Jc0VuY3J5cHRlZCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNldFBsdW1iaW5nU3RhdGUoZXZlbnQsIHJvb21JZCwgc3RhdHVzKSB7XHJcbiAgICBpZiAodHlwZW9mIHN0YXR1cyAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1BsdW1iaW5nIHN0YXRlIHN0YXR1cyBzaG91bGQgYmUgYSBzdHJpbmcnKTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKGBSZWNlaXZlZCByZXF1ZXN0IHRvIHNldCBwbHVtYmluZyBzdGF0ZSB0byBzdGF0dXMgXCIke3N0YXR1c31cIiBpbiByb29tICR7cm9vbUlkfWApO1xyXG4gICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgaWYgKCFjbGllbnQpIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdZb3UgbmVlZCB0byBiZSBsb2dnZWQgaW4uJykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNsaWVudC5zZW5kU3RhdGVFdmVudChyb29tSWQsIFwibS5yb29tLnBsdW1iaW5nXCIsIHsgc3RhdHVzOiBzdGF0dXMgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgc2VuZFJlc3BvbnNlKGV2ZW50LCB7XHJcbiAgICAgICAgICAgIHN1Y2Nlc3M6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBlcnIubWVzc2FnZSA/IGVyci5tZXNzYWdlIDogX3QoJ0ZhaWxlZCB0byBzZW5kIHJlcXVlc3QuJyksIGVycik7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2V0Qm90T3B0aW9ucyhldmVudCwgcm9vbUlkLCB1c2VySWQpIHtcclxuICAgIGNvbnNvbGUubG9nKGBSZWNlaXZlZCByZXF1ZXN0IHRvIHNldCBvcHRpb25zIGZvciBib3QgJHt1c2VySWR9IGluIHJvb20gJHtyb29tSWR9YCk7XHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBuZWVkIHRvIGJlIGxvZ2dlZCBpbi4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY2xpZW50LnNlbmRTdGF0ZUV2ZW50KHJvb21JZCwgXCJtLnJvb20uYm90Lm9wdGlvbnNcIiwgZXZlbnQuZGF0YS5jb250ZW50LCBcIl9cIiArIHVzZXJJZCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgc2VuZFJlc3BvbnNlKGV2ZW50LCB7XHJcbiAgICAgICAgICAgIHN1Y2Nlc3M6IHRydWUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBlcnIubWVzc2FnZSA/IGVyci5tZXNzYWdlIDogX3QoJ0ZhaWxlZCB0byBzZW5kIHJlcXVlc3QuJyksIGVycik7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2V0Qm90UG93ZXIoZXZlbnQsIHJvb21JZCwgdXNlcklkLCBsZXZlbCkge1xyXG4gICAgaWYgKCEoTnVtYmVyLmlzSW50ZWdlcihsZXZlbCkgJiYgbGV2ZWwgPj0gMCkpIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdQb3dlciBsZXZlbCBtdXN0IGJlIHBvc2l0aXZlIGludGVnZXIuJykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zb2xlLmxvZyhgUmVjZWl2ZWQgcmVxdWVzdCB0byBzZXQgcG93ZXIgbGV2ZWwgdG8gJHtsZXZlbH0gZm9yIGJvdCAke3VzZXJJZH0gaW4gcm9vbSAke3Jvb21JZH0uYCk7XHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBuZWVkIHRvIGJlIGxvZ2dlZCBpbi4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNsaWVudC5nZXRTdGF0ZUV2ZW50KHJvb21JZCwgXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsIFwiXCIpLnRoZW4oKHBvd2VyTGV2ZWxzKSA9PiB7XHJcbiAgICAgICAgY29uc3QgcG93ZXJFdmVudCA9IG5ldyBNYXRyaXhFdmVudChcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogXCJtLnJvb20ucG93ZXJfbGV2ZWxzXCIsXHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBwb3dlckxldmVscyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBjbGllbnQuc2V0UG93ZXJMZXZlbChyb29tSWQsIHVzZXJJZCwgbGV2ZWwsIHBvd2VyRXZlbnQpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICBzZW5kUmVzcG9uc2UoZXZlbnQsIHtcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IHRydWUsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBlcnIubWVzc2FnZSA/IGVyci5tZXNzYWdlIDogX3QoJ0ZhaWxlZCB0byBzZW5kIHJlcXVlc3QuJyksIGVycik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TWVtYmVyc2hpcFN0YXRlKGV2ZW50LCByb29tSWQsIHVzZXJJZCkge1xyXG4gICAgY29uc29sZS5sb2coYG1lbWJlcnNoaXBfc3RhdGUgb2YgJHt1c2VySWR9IGluIHJvb20gJHtyb29tSWR9IHJlcXVlc3RlZC5gKTtcclxuICAgIHJldHVyblN0YXRlRXZlbnQoZXZlbnQsIHJvb21JZCwgXCJtLnJvb20ubWVtYmVyXCIsIHVzZXJJZCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldEpvaW5SdWxlcyhldmVudCwgcm9vbUlkKSB7XHJcbiAgICBjb25zb2xlLmxvZyhgam9pbl9ydWxlcyBvZiAke3Jvb21JZH0gcmVxdWVzdGVkLmApO1xyXG4gICAgcmV0dXJuU3RhdGVFdmVudChldmVudCwgcm9vbUlkLCBcIm0ucm9vbS5qb2luX3J1bGVzXCIsIFwiXCIpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBib3RPcHRpb25zKGV2ZW50LCByb29tSWQsIHVzZXJJZCkge1xyXG4gICAgY29uc29sZS5sb2coYGJvdF9vcHRpb25zIG9mICR7dXNlcklkfSBpbiByb29tICR7cm9vbUlkfSByZXF1ZXN0ZWQuYCk7XHJcbiAgICByZXR1cm5TdGF0ZUV2ZW50KGV2ZW50LCByb29tSWQsIFwibS5yb29tLmJvdC5vcHRpb25zXCIsIFwiX1wiICsgdXNlcklkKTtcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TWVtYmVyc2hpcENvdW50KGV2ZW50LCByb29tSWQpIHtcclxuICAgIGNvbnN0IGNsaWVudCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgIGlmICghY2xpZW50KSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnWW91IG5lZWQgdG8gYmUgbG9nZ2VkIGluLicpKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCByb29tID0gY2xpZW50LmdldFJvb20ocm9vbUlkKTtcclxuICAgIGlmICghcm9vbSkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1RoaXMgcm9vbSBpcyBub3QgcmVjb2duaXNlZC4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3QgY291bnQgPSByb29tLmdldEpvaW5lZE1lbWJlckNvdW50KCk7XHJcbiAgICBzZW5kUmVzcG9uc2UoZXZlbnQsIGNvdW50KTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2FuU2VuZEV2ZW50KGV2ZW50LCByb29tSWQpIHtcclxuICAgIGNvbnN0IGV2VHlwZSA9IFwiXCIgKyBldmVudC5kYXRhLmV2ZW50X3R5cGU7IC8vIGZvcmNlIHN0cmluZ2lmeVxyXG4gICAgY29uc3QgaXNTdGF0ZSA9IEJvb2xlYW4oZXZlbnQuZGF0YS5pc19zdGF0ZSk7XHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICBpZiAoIWNsaWVudCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBuZWVkIHRvIGJlIGxvZ2dlZCBpbi4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgcm9vbSA9IGNsaWVudC5nZXRSb29tKHJvb21JZCk7XHJcbiAgICBpZiAoIXJvb20pIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdUaGlzIHJvb20gaXMgbm90IHJlY29nbmlzZWQuJykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChyb29tLmdldE15TWVtYmVyc2hpcCgpICE9PSBcImpvaW5cIikge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ1lvdSBhcmUgbm90IGluIHRoaXMgcm9vbS4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3QgbWUgPSBjbGllbnQuY3JlZGVudGlhbHMudXNlcklkO1xyXG5cclxuICAgIGxldCBjYW5TZW5kID0gZmFsc2U7XHJcbiAgICBpZiAoaXNTdGF0ZSkge1xyXG4gICAgICAgIGNhblNlbmQgPSByb29tLmN1cnJlbnRTdGF0ZS5tYXlTZW5kU3RhdGVFdmVudChldlR5cGUsIG1lKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY2FuU2VuZCA9IHJvb20uY3VycmVudFN0YXRlLm1heVNlbmRFdmVudChldlR5cGUsIG1lKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIWNhblNlbmQpIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdZb3UgZG8gbm90IGhhdmUgcGVybWlzc2lvbiB0byBkbyB0aGF0IGluIHRoaXMgcm9vbS4nKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbmRSZXNwb25zZShldmVudCwgdHJ1ZSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJldHVyblN0YXRlRXZlbnQoZXZlbnQsIHJvb21JZCwgZXZlbnRUeXBlLCBzdGF0ZUtleSkge1xyXG4gICAgY29uc3QgY2xpZW50ID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgaWYgKCFjbGllbnQpIHtcclxuICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdZb3UgbmVlZCB0byBiZSBsb2dnZWQgaW4uJykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNvbnN0IHJvb20gPSBjbGllbnQuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgaWYgKCFyb29tKSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnVGhpcyByb29tIGlzIG5vdCByZWNvZ25pc2VkLicpKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBzdGF0ZUV2ZW50ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoZXZlbnRUeXBlLCBzdGF0ZUtleSk7XHJcbiAgICBpZiAoIXN0YXRlRXZlbnQpIHtcclxuICAgICAgICBzZW5kUmVzcG9uc2UoZXZlbnQsIG51bGwpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHNlbmRSZXNwb25zZShldmVudCwgc3RhdGVFdmVudC5nZXRDb250ZW50KCkpO1xyXG59XHJcblxyXG5jb25zdCBvbk1lc3NhZ2UgPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgaWYgKCFldmVudC5vcmlnaW4pIHsgLy8gc3R1cGlkIGNocm9tZVxyXG4gICAgICAgIGV2ZW50Lm9yaWdpbiA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQub3JpZ2luO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENoZWNrIHRoYXQgdGhlIGludGVncmF0aW9ucyBVSSBVUkwgc3RhcnRzIHdpdGggdGhlIG9yaWdpbiBvZiB0aGUgZXZlbnRcclxuICAgIC8vIFRoaXMgbWVhbnMgdGhlIFVSTCBjb3VsZCBjb250YWluIGEgcGF0aCAobGlrZSAvZGV2ZWxvcCkgYW5kIHN0aWxsIGJlIHVzZWRcclxuICAgIC8vIHRvIHZhbGlkYXRlIGV2ZW50IG9yaWdpbnMsIHdoaWNoIGRvIG5vdCBzcGVjaWZ5IHBhdGhzLlxyXG4gICAgLy8gKFNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BUEkvV2luZG93L3Bvc3RNZXNzYWdlKVxyXG4gICAgbGV0IGNvbmZpZ1VybDtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgaWYgKCFvcGVuTWFuYWdlclVybCkgb3Blbk1hbmFnZXJVcmwgPSBJbnRlZ3JhdGlvbk1hbmFnZXJzLnNoYXJlZEluc3RhbmNlKCkuZ2V0UHJpbWFyeU1hbmFnZXIoKS51aVVybDtcclxuICAgICAgICBjb25maWdVcmwgPSBuZXcgVVJMKG9wZW5NYW5hZ2VyVXJsKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAvLyBObyBpbnRlZ3JhdGlvbnMgVUkgVVJMLCBpZ25vcmUgc2lsZW50bHkuXHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgbGV0IGV2ZW50T3JpZ2luVXJsO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICBldmVudE9yaWdpblVybCA9IG5ldyBVUkwoZXZlbnQub3JpZ2luKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyBUT0RPIC0tIFNjYWxhciBwb3N0TWVzc2FnZSBBUEkgc2hvdWxkIGJlIG5hbWVzcGFjZWQgd2l0aCBldmVudC5kYXRhLmFwaSBmaWVsZFxyXG4gICAgLy8gRml4IGZvbGxvd2luZyBcImlmXCIgc3RhdGVtZW50IHRvIHJlc3BvbmQgb25seSB0byBzcGVjaWZpYyBBUEkgbWVzc2FnZXMuXHJcbiAgICBpZiAoXHJcbiAgICAgICAgY29uZmlnVXJsLm9yaWdpbiAhPT0gZXZlbnRPcmlnaW5Vcmwub3JpZ2luIHx8XHJcbiAgICAgICAgIWV2ZW50LmRhdGEuYWN0aW9uIHx8XHJcbiAgICAgICAgZXZlbnQuZGF0YS5hcGkgLy8gSWdub3JlIG1lc3NhZ2VzIHdpdGggc3BlY2lmaWMgQVBJIHNldFxyXG4gICAgKSB7XHJcbiAgICAgICAgLy8gZG9uJ3QgbG9nIHRoaXMgLSBkZWJ1Z2dpbmcgQVBJcyBhbmQgYnJvd3NlciBhZGQtb25zIGxpa2UgdG8gc3BhbVxyXG4gICAgICAgIC8vIHBvc3RNZXNzYWdlIHdoaWNoIGZsb29kcyB0aGUgbG9nIG90aGVyd2lzZVxyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoZXZlbnQuZGF0YS5hY3Rpb24gPT09IFwiY2xvc2Vfc2NhbGFyXCIpIHtcclxuICAgICAgICBkaXMuZGlzcGF0Y2goeyBhY3Rpb246IFwiY2xvc2Vfc2NhbGFyXCIgfSk7XHJcbiAgICAgICAgc2VuZFJlc3BvbnNlKGV2ZW50LCBudWxsKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3Qgcm9vbUlkID0gZXZlbnQuZGF0YS5yb29tX2lkO1xyXG4gICAgY29uc3QgdXNlcklkID0gZXZlbnQuZGF0YS51c2VyX2lkO1xyXG5cclxuICAgIGlmICghcm9vbUlkKSB7XHJcbiAgICAgICAgLy8gVGhlc2UgQVBJcyBkb24ndCByZXF1aXJlIHJvb21JZFxyXG4gICAgICAgIC8vIEdldCBhbmQgc2V0IHVzZXIgd2lkZ2V0cyAobm90IGFzc29jaWF0ZWQgd2l0aCBhIHNwZWNpZmljIHJvb20pXHJcbiAgICAgICAgLy8gSWYgcm9vbUlkIGlzIHNwZWNpZmllZCwgaXQgbXVzdCBiZSB2YWxpZGF0ZWQsIHNvIHJvb20tYmFzZWQgd2lkZ2V0cyBhZ3JlZWRcclxuICAgICAgICAvLyBoYW5kbGVkIGZ1cnRoZXIgZG93bi5cclxuICAgICAgICBpZiAoZXZlbnQuZGF0YS5hY3Rpb24gPT09IFwiZ2V0X3dpZGdldHNcIikge1xyXG4gICAgICAgICAgICBnZXRXaWRnZXRzKGV2ZW50LCBudWxsKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQuZGF0YS5hY3Rpb24gPT09IFwic2V0X3dpZGdldFwiKSB7XHJcbiAgICAgICAgICAgIHNldFdpZGdldChldmVudCwgbnVsbCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZW5kRXJyb3IoZXZlbnQsIF90KCdNaXNzaW5nIHJvb21faWQgaW4gcmVxdWVzdCcpKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAocm9vbUlkICE9PSBSb29tVmlld1N0b3JlLmdldFJvb21JZCgpKSB7XHJcbiAgICAgICAgc2VuZEVycm9yKGV2ZW50LCBfdCgnUm9vbSAlKHJvb21JZClzIG5vdCB2aXNpYmxlJywge3Jvb21JZDogcm9vbUlkfSkpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYW5kIHNldCByb29tLWJhc2VkIHdpZGdldHNcclxuICAgIGlmIChldmVudC5kYXRhLmFjdGlvbiA9PT0gXCJnZXRfd2lkZ2V0c1wiKSB7XHJcbiAgICAgICAgZ2V0V2lkZ2V0cyhldmVudCwgcm9vbUlkKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9IGVsc2UgaWYgKGV2ZW50LmRhdGEuYWN0aW9uID09PSBcInNldF93aWRnZXRcIikge1xyXG4gICAgICAgIHNldFdpZGdldChldmVudCwgcm9vbUlkKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVGhlc2UgQVBJcyBkb24ndCByZXF1aXJlIHVzZXJJZFxyXG4gICAgaWYgKGV2ZW50LmRhdGEuYWN0aW9uID09PSBcImpvaW5fcnVsZXNfc3RhdGVcIikge1xyXG4gICAgICAgIGdldEpvaW5SdWxlcyhldmVudCwgcm9vbUlkKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9IGVsc2UgaWYgKGV2ZW50LmRhdGEuYWN0aW9uID09PSBcInNldF9wbHVtYmluZ19zdGF0ZVwiKSB7XHJcbiAgICAgICAgc2V0UGx1bWJpbmdTdGF0ZShldmVudCwgcm9vbUlkLCBldmVudC5kYXRhLnN0YXR1cyk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfSBlbHNlIGlmIChldmVudC5kYXRhLmFjdGlvbiA9PT0gXCJnZXRfbWVtYmVyc2hpcF9jb3VudFwiKSB7XHJcbiAgICAgICAgZ2V0TWVtYmVyc2hpcENvdW50KGV2ZW50LCByb29tSWQpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH0gZWxzZSBpZiAoZXZlbnQuZGF0YS5hY3Rpb24gPT09IFwiZ2V0X3Jvb21fZW5jX3N0YXRlXCIpIHtcclxuICAgICAgICBnZXRSb29tRW5jU3RhdGUoZXZlbnQsIHJvb21JZCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfSBlbHNlIGlmIChldmVudC5kYXRhLmFjdGlvbiA9PT0gXCJjYW5fc2VuZF9ldmVudFwiKSB7XHJcbiAgICAgICAgY2FuU2VuZEV2ZW50KGV2ZW50LCByb29tSWQpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXVzZXJJZCkge1xyXG4gICAgICAgIHNlbmRFcnJvcihldmVudCwgX3QoJ01pc3NpbmcgdXNlcl9pZCBpbiByZXF1ZXN0JykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHN3aXRjaCAoZXZlbnQuZGF0YS5hY3Rpb24pIHtcclxuICAgICAgICBjYXNlIFwibWVtYmVyc2hpcF9zdGF0ZVwiOlxyXG4gICAgICAgICAgICBnZXRNZW1iZXJzaGlwU3RhdGUoZXZlbnQsIHJvb21JZCwgdXNlcklkKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBcImludml0ZVwiOlxyXG4gICAgICAgICAgICBpbnZpdGVVc2VyKGV2ZW50LCByb29tSWQsIHVzZXJJZCk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgXCJib3Rfb3B0aW9uc1wiOlxyXG4gICAgICAgICAgICBib3RPcHRpb25zKGV2ZW50LCByb29tSWQsIHVzZXJJZCk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgXCJzZXRfYm90X29wdGlvbnNcIjpcclxuICAgICAgICAgICAgc2V0Qm90T3B0aW9ucyhldmVudCwgcm9vbUlkLCB1c2VySWQpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlIFwic2V0X2JvdF9wb3dlclwiOlxyXG4gICAgICAgICAgICBzZXRCb3RQb3dlcihldmVudCwgcm9vbUlkLCB1c2VySWQsIGV2ZW50LmRhdGEubGV2ZWwpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJVbmhhbmRsZWQgcG9zdE1lc3NhZ2UgZXZlbnQgd2l0aCBhY3Rpb24gJ1wiICsgZXZlbnQuZGF0YS5hY3Rpb24gK1wiJ1wiKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbn07XHJcblxyXG5sZXQgbGlzdGVuZXJDb3VudCA9IDA7XHJcbmxldCBvcGVuTWFuYWdlclVybCA9IG51bGw7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc3RhcnRMaXN0ZW5pbmcoKSB7XHJcbiAgICBpZiAobGlzdGVuZXJDb3VudCA9PT0gMCkge1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwibWVzc2FnZVwiLCBvbk1lc3NhZ2UsIGZhbHNlKTtcclxuICAgIH1cclxuICAgIGxpc3RlbmVyQ291bnQgKz0gMTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHN0b3BMaXN0ZW5pbmcoKSB7XHJcbiAgICBsaXN0ZW5lckNvdW50IC09IDE7XHJcbiAgICBpZiAobGlzdGVuZXJDb3VudCA9PT0gMCkge1xyXG4gICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwibWVzc2FnZVwiLCBvbk1lc3NhZ2UpO1xyXG4gICAgfVxyXG4gICAgaWYgKGxpc3RlbmVyQ291bnQgPCAwKSB7XHJcbiAgICAgICAgLy8gTWFrZSBhbiBlcnJvciBzbyB3ZSBnZXQgYSBzdGFjayB0cmFjZVxyXG4gICAgICAgIGNvbnN0IGUgPSBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAgIFwiU2NhbGFyTWVzc2FnaW5nOiBtaXNtYXRjaGVkIHN0YXJ0TGlzdGVuaW5nIC8gc3RvcExpc3RlbmluZyBkZXRlY3RlZC5cIiArXHJcbiAgICAgICAgICAgIFwiIE5lZ2F0aXZlIGNvdW50XCIsXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2V0T3Blbk1hbmFnZXJVcmwodXJsKSB7XHJcbiAgICBvcGVuTWFuYWdlclVybCA9IHVybDtcclxufVxyXG4iXX0=