/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _flux = _interopRequireDefault(require("flux"));

class MatrixDispatcher extends _flux.default.Dispatcher {
  /**
   * @param {Object|function} payload Required. The payload to dispatch.
   *        If an Object, must contain at least an 'action' key.
   *        If a function, must have the signature (dispatch) => {...}.
   * @param {boolean=} sync Optional. Pass true to dispatch
   *        synchronously. This is useful for anything triggering
   *        an operation that the browser requires user interaction
   *        for.
   */
  dispatch(payload, sync) {
    // Allow for asynchronous dispatching by accepting payloads that have the
    // type `function (dispatch) {...}`
    if (typeof payload === 'function') {
      payload(action => {
        this.dispatch(action, sync);
      });
      return;
    }

    if (sync) {
      super.dispatch(payload);
    } else {
      // Unless the caller explicitly asked for us to dispatch synchronously,
      // we always set a timeout to do this: The flux dispatcher complains
      // if you dispatch from within a dispatch, so rather than action
      // handlers having to worry about not calling anything that might
      // then dispatch, we just do dispatches asynchronously.
      setTimeout(super.dispatch.bind(this, payload), 0);
    }
  }

}

if (global.mxDispatcher === undefined) {
  global.mxDispatcher = new MatrixDispatcher();
}

var _default = global.mxDispatcher;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9kaXNwYXRjaGVyLmpzIl0sIm5hbWVzIjpbIk1hdHJpeERpc3BhdGNoZXIiLCJmbHV4IiwiRGlzcGF0Y2hlciIsImRpc3BhdGNoIiwicGF5bG9hZCIsInN5bmMiLCJhY3Rpb24iLCJzZXRUaW1lb3V0IiwiYmluZCIsImdsb2JhbCIsIm14RGlzcGF0Y2hlciIsInVuZGVmaW5lZCJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7Ozs7Ozs7OztBQUVBOztBQUVBLE1BQU1BLGdCQUFOLFNBQStCQyxjQUFLQyxVQUFwQyxDQUErQztBQUMzQzs7Ozs7Ozs7O0FBU0FDLEVBQUFBLFFBQVEsQ0FBQ0MsT0FBRCxFQUFVQyxJQUFWLEVBQWdCO0FBQ3BCO0FBQ0E7QUFDQSxRQUFJLE9BQU9ELE9BQVAsS0FBbUIsVUFBdkIsRUFBbUM7QUFDL0JBLE1BQUFBLE9BQU8sQ0FBRUUsTUFBRCxJQUFZO0FBQ2hCLGFBQUtILFFBQUwsQ0FBY0csTUFBZCxFQUFzQkQsSUFBdEI7QUFDSCxPQUZNLENBQVA7QUFHQTtBQUNIOztBQUVELFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU1GLFFBQU4sQ0FBZUMsT0FBZjtBQUNILEtBRkQsTUFFTztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUcsTUFBQUEsVUFBVSxDQUFDLE1BQU1KLFFBQU4sQ0FBZUssSUFBZixDQUFvQixJQUFwQixFQUEwQkosT0FBMUIsQ0FBRCxFQUFxQyxDQUFyQyxDQUFWO0FBQ0g7QUFDSjs7QUE5QjBDOztBQWlDL0MsSUFBSUssTUFBTSxDQUFDQyxZQUFQLEtBQXdCQyxTQUE1QixFQUF1QztBQUNuQ0YsRUFBQUEsTUFBTSxDQUFDQyxZQUFQLEdBQXNCLElBQUlWLGdCQUFKLEVBQXRCO0FBQ0g7O2VBQ2NTLE1BQU0sQ0FBQ0MsWSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3IE5ldyBWZWN0b3IgTHRkXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuaW1wb3J0IGZsdXggZnJvbSBcImZsdXhcIjtcclxuXHJcbmNsYXNzIE1hdHJpeERpc3BhdGNoZXIgZXh0ZW5kcyBmbHV4LkRpc3BhdGNoZXIge1xyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0ge09iamVjdHxmdW5jdGlvbn0gcGF5bG9hZCBSZXF1aXJlZC4gVGhlIHBheWxvYWQgdG8gZGlzcGF0Y2guXHJcbiAgICAgKiAgICAgICAgSWYgYW4gT2JqZWN0LCBtdXN0IGNvbnRhaW4gYXQgbGVhc3QgYW4gJ2FjdGlvbicga2V5LlxyXG4gICAgICogICAgICAgIElmIGEgZnVuY3Rpb24sIG11c3QgaGF2ZSB0aGUgc2lnbmF0dXJlIChkaXNwYXRjaCkgPT4gey4uLn0uXHJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW49fSBzeW5jIE9wdGlvbmFsLiBQYXNzIHRydWUgdG8gZGlzcGF0Y2hcclxuICAgICAqICAgICAgICBzeW5jaHJvbm91c2x5LiBUaGlzIGlzIHVzZWZ1bCBmb3IgYW55dGhpbmcgdHJpZ2dlcmluZ1xyXG4gICAgICogICAgICAgIGFuIG9wZXJhdGlvbiB0aGF0IHRoZSBicm93c2VyIHJlcXVpcmVzIHVzZXIgaW50ZXJhY3Rpb25cclxuICAgICAqICAgICAgICBmb3IuXHJcbiAgICAgKi9cclxuICAgIGRpc3BhdGNoKHBheWxvYWQsIHN5bmMpIHtcclxuICAgICAgICAvLyBBbGxvdyBmb3IgYXN5bmNocm9ub3VzIGRpc3BhdGNoaW5nIGJ5IGFjY2VwdGluZyBwYXlsb2FkcyB0aGF0IGhhdmUgdGhlXHJcbiAgICAgICAgLy8gdHlwZSBgZnVuY3Rpb24gKGRpc3BhdGNoKSB7Li4ufWBcclxuICAgICAgICBpZiAodHlwZW9mIHBheWxvYWQgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgcGF5bG9hZCgoYWN0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc3BhdGNoKGFjdGlvbiwgc3luYyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3luYykge1xyXG4gICAgICAgICAgICBzdXBlci5kaXNwYXRjaChwYXlsb2FkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBVbmxlc3MgdGhlIGNhbGxlciBleHBsaWNpdGx5IGFza2VkIGZvciB1cyB0byBkaXNwYXRjaCBzeW5jaHJvbm91c2x5LFxyXG4gICAgICAgICAgICAvLyB3ZSBhbHdheXMgc2V0IGEgdGltZW91dCB0byBkbyB0aGlzOiBUaGUgZmx1eCBkaXNwYXRjaGVyIGNvbXBsYWluc1xyXG4gICAgICAgICAgICAvLyBpZiB5b3UgZGlzcGF0Y2ggZnJvbSB3aXRoaW4gYSBkaXNwYXRjaCwgc28gcmF0aGVyIHRoYW4gYWN0aW9uXHJcbiAgICAgICAgICAgIC8vIGhhbmRsZXJzIGhhdmluZyB0byB3b3JyeSBhYm91dCBub3QgY2FsbGluZyBhbnl0aGluZyB0aGF0IG1pZ2h0XHJcbiAgICAgICAgICAgIC8vIHRoZW4gZGlzcGF0Y2gsIHdlIGp1c3QgZG8gZGlzcGF0Y2hlcyBhc3luY2hyb25vdXNseS5cclxuICAgICAgICAgICAgc2V0VGltZW91dChzdXBlci5kaXNwYXRjaC5iaW5kKHRoaXMsIHBheWxvYWQpLCAwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmlmIChnbG9iYWwubXhEaXNwYXRjaGVyID09PSB1bmRlZmluZWQpIHtcclxuICAgIGdsb2JhbC5teERpc3BhdGNoZXIgPSBuZXcgTWF0cml4RGlzcGF0Y2hlcigpO1xyXG59XHJcbmV4cG9ydCBkZWZhdWx0IGdsb2JhbC5teERpc3BhdGNoZXI7XHJcbiJdfQ==