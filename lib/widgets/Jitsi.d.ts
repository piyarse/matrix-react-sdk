export declare class Jitsi {
    private static instance;
    private domain;
    get preferredDomain(): string;
    constructor();
    update(): Promise<any>;
    static getInstance(): Jitsi;
}
