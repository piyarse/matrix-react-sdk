"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/*
Copyright 2017 Travis Ralston
Copyright 2019 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Represents the base class for all level handlers. This class performs no logic
 * and should be overridden.
 */
class SettingsHandler {
  /**
   * Gets the value for a particular setting at this level for a particular room.
   * If no room is applicable, the roomId may be null. The roomId may not be
   * applicable to this level and may be ignored by the handler.
   * @param {string} settingName The name of the setting.
   * @param {String} roomId The room ID to read from, may be null.
   * @returns {*} The setting value, or null if not found.
   */
  getValue(settingName, roomId) {
    console.error("Invalid operation: getValue was not overridden");
    return null;
  }
  /**
   * Sets the value for a particular setting at this level for a particular room.
   * If no room is applicable, the roomId may be null. The roomId may not be
   * applicable to this level and may be ignored by the handler. Setting a value
   * to null will cause the level to remove the value. The current user should be
   * able to set the value prior to calling this.
   * @param {string} settingName The name of the setting to change.
   * @param {String} roomId The room ID to set the value in, may be null.
   * @param {*} newValue The new value for the setting, may be null.
   * @returns {Promise} Resolves when the setting has been saved.
   */


  setValue(settingName, roomId, newValue) {
    console.error("Invalid operation: setValue was not overridden");
    return Promise.reject();
  }
  /**
   * Determines if the current user is able to set the value of the given setting
   * in the given room at this level.
   * @param {string} settingName The name of the setting to check.
   * @param {String} roomId The room ID to check in, may be null
   * @returns {boolean} True if the setting can be set by the user, false otherwise.
   */


  canSetValue(settingName, roomId) {
    return false;
  }
  /**
   * Determines if this level is supported on this device.
   * @returns {boolean} True if this level is supported on the current device.
   */


  isSupported() {
    return false;
  }

}

exports.default = SettingsHandler;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXR0aW5ncy9oYW5kbGVycy9TZXR0aW5nc0hhbmRsZXIuanMiXSwibmFtZXMiOlsiU2V0dGluZ3NIYW5kbGVyIiwiZ2V0VmFsdWUiLCJzZXR0aW5nTmFtZSIsInJvb21JZCIsImNvbnNvbGUiLCJlcnJvciIsInNldFZhbHVlIiwibmV3VmFsdWUiLCJQcm9taXNlIiwicmVqZWN0IiwiY2FuU2V0VmFsdWUiLCJpc1N1cHBvcnRlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTs7OztBQUllLE1BQU1BLGVBQU4sQ0FBc0I7QUFDakM7Ozs7Ozs7O0FBUUFDLEVBQUFBLFFBQVEsQ0FBQ0MsV0FBRCxFQUFjQyxNQUFkLEVBQXNCO0FBQzFCQyxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxnREFBZDtBQUNBLFdBQU8sSUFBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFXQUMsRUFBQUEsUUFBUSxDQUFDSixXQUFELEVBQWNDLE1BQWQsRUFBc0JJLFFBQXRCLEVBQWdDO0FBQ3BDSCxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxnREFBZDtBQUNBLFdBQU9HLE9BQU8sQ0FBQ0MsTUFBUixFQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7O0FBT0FDLEVBQUFBLFdBQVcsQ0FBQ1IsV0FBRCxFQUFjQyxNQUFkLEVBQXNCO0FBQzdCLFdBQU8sS0FBUDtBQUNIO0FBRUQ7Ozs7OztBQUlBUSxFQUFBQSxXQUFXLEdBQUc7QUFDVixXQUFPLEtBQVA7QUFDSDs7QUEvQ2dDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVHJhdmlzIFJhbHN0b25cclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGQuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLyoqXHJcbiAqIFJlcHJlc2VudHMgdGhlIGJhc2UgY2xhc3MgZm9yIGFsbCBsZXZlbCBoYW5kbGVycy4gVGhpcyBjbGFzcyBwZXJmb3JtcyBubyBsb2dpY1xyXG4gKiBhbmQgc2hvdWxkIGJlIG92ZXJyaWRkZW4uXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZXR0aW5nc0hhbmRsZXIge1xyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSB2YWx1ZSBmb3IgYSBwYXJ0aWN1bGFyIHNldHRpbmcgYXQgdGhpcyBsZXZlbCBmb3IgYSBwYXJ0aWN1bGFyIHJvb20uXHJcbiAgICAgKiBJZiBubyByb29tIGlzIGFwcGxpY2FibGUsIHRoZSByb29tSWQgbWF5IGJlIG51bGwuIFRoZSByb29tSWQgbWF5IG5vdCBiZVxyXG4gICAgICogYXBwbGljYWJsZSB0byB0aGlzIGxldmVsIGFuZCBtYXkgYmUgaWdub3JlZCBieSB0aGUgaGFuZGxlci5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzZXR0aW5nTmFtZSBUaGUgbmFtZSBvZiB0aGUgc2V0dGluZy5cclxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSByb29tSWQgVGhlIHJvb20gSUQgdG8gcmVhZCBmcm9tLCBtYXkgYmUgbnVsbC5cclxuICAgICAqIEByZXR1cm5zIHsqfSBUaGUgc2V0dGluZyB2YWx1ZSwgb3IgbnVsbCBpZiBub3QgZm91bmQuXHJcbiAgICAgKi9cclxuICAgIGdldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpIHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKFwiSW52YWxpZCBvcGVyYXRpb246IGdldFZhbHVlIHdhcyBub3Qgb3ZlcnJpZGRlblwiKTtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgdGhlIHZhbHVlIGZvciBhIHBhcnRpY3VsYXIgc2V0dGluZyBhdCB0aGlzIGxldmVsIGZvciBhIHBhcnRpY3VsYXIgcm9vbS5cclxuICAgICAqIElmIG5vIHJvb20gaXMgYXBwbGljYWJsZSwgdGhlIHJvb21JZCBtYXkgYmUgbnVsbC4gVGhlIHJvb21JZCBtYXkgbm90IGJlXHJcbiAgICAgKiBhcHBsaWNhYmxlIHRvIHRoaXMgbGV2ZWwgYW5kIG1heSBiZSBpZ25vcmVkIGJ5IHRoZSBoYW5kbGVyLiBTZXR0aW5nIGEgdmFsdWVcclxuICAgICAqIHRvIG51bGwgd2lsbCBjYXVzZSB0aGUgbGV2ZWwgdG8gcmVtb3ZlIHRoZSB2YWx1ZS4gVGhlIGN1cnJlbnQgdXNlciBzaG91bGQgYmVcclxuICAgICAqIGFibGUgdG8gc2V0IHRoZSB2YWx1ZSBwcmlvciB0byBjYWxsaW5nIHRoaXMuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2V0dGluZ05hbWUgVGhlIG5hbWUgb2YgdGhlIHNldHRpbmcgdG8gY2hhbmdlLlxyXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgcm9vbSBJRCB0byBzZXQgdGhlIHZhbHVlIGluLCBtYXkgYmUgbnVsbC5cclxuICAgICAqIEBwYXJhbSB7Kn0gbmV3VmFsdWUgVGhlIG5ldyB2YWx1ZSBmb3IgdGhlIHNldHRpbmcsIG1heSBiZSBudWxsLlxyXG4gICAgICogQHJldHVybnMge1Byb21pc2V9IFJlc29sdmVzIHdoZW4gdGhlIHNldHRpbmcgaGFzIGJlZW4gc2F2ZWQuXHJcbiAgICAgKi9cclxuICAgIHNldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQsIG5ld1ZhbHVlKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihcIkludmFsaWQgb3BlcmF0aW9uOiBzZXRWYWx1ZSB3YXMgbm90IG92ZXJyaWRkZW5cIik7XHJcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBjdXJyZW50IHVzZXIgaXMgYWJsZSB0byBzZXQgdGhlIHZhbHVlIG9mIHRoZSBnaXZlbiBzZXR0aW5nXHJcbiAgICAgKiBpbiB0aGUgZ2l2ZW4gcm9vbSBhdCB0aGlzIGxldmVsLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNldHRpbmdOYW1lIFRoZSBuYW1lIG9mIHRoZSBzZXR0aW5nIHRvIGNoZWNrLlxyXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgcm9vbSBJRCB0byBjaGVjayBpbiwgbWF5IGJlIG51bGxcclxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBzZXR0aW5nIGNhbiBiZSBzZXQgYnkgdGhlIHVzZXIsIGZhbHNlIG90aGVyd2lzZS5cclxuICAgICAqL1xyXG4gICAgY2FuU2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZXMgaWYgdGhpcyBsZXZlbCBpcyBzdXBwb3J0ZWQgb24gdGhpcyBkZXZpY2UuXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGlzIGxldmVsIGlzIHN1cHBvcnRlZCBvbiB0aGUgY3VycmVudCBkZXZpY2UuXHJcbiAgICAgKi9cclxuICAgIGlzU3VwcG9ydGVkKCkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=