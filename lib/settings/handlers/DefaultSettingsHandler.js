"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _SettingsHandler = _interopRequireDefault(require("./SettingsHandler"));

/*
Copyright 2017 Travis Ralston
Copyright 2019 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Gets settings at the "default" level. This handler does not support setting values.
 * This handler does not make use of the roomId parameter.
 */
class DefaultSettingsHandler extends _SettingsHandler.default {
  /**
   * Creates a new default settings handler with the given defaults
   * @param {object} defaults The default setting values, keyed by setting name.
   * @param {object} invertedDefaults The default inverted setting values, keyed by setting name.
   */
  constructor(defaults, invertedDefaults) {
    super();
    this._defaults = defaults;
    this._invertedDefaults = invertedDefaults;
  }

  getValue(settingName, roomId) {
    let value = this._defaults[settingName];

    if (value === undefined) {
      value = this._invertedDefaults[settingName];
    }

    return value;
  }

  setValue(settingName, roomId, newValue) {
    throw new Error("Cannot set values on the default level handler");
  }

  canSetValue(settingName, roomId) {
    return false;
  }

  isSupported() {
    return true;
  }

}

exports.default = DefaultSettingsHandler;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXR0aW5ncy9oYW5kbGVycy9EZWZhdWx0U2V0dGluZ3NIYW5kbGVyLmpzIl0sIm5hbWVzIjpbIkRlZmF1bHRTZXR0aW5nc0hhbmRsZXIiLCJTZXR0aW5nc0hhbmRsZXIiLCJjb25zdHJ1Y3RvciIsImRlZmF1bHRzIiwiaW52ZXJ0ZWREZWZhdWx0cyIsIl9kZWZhdWx0cyIsIl9pbnZlcnRlZERlZmF1bHRzIiwiZ2V0VmFsdWUiLCJzZXR0aW5nTmFtZSIsInJvb21JZCIsInZhbHVlIiwidW5kZWZpbmVkIiwic2V0VmFsdWUiLCJuZXdWYWx1ZSIsIkVycm9yIiwiY2FuU2V0VmFsdWUiLCJpc1N1cHBvcnRlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBaUJBOztBQWpCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7QUFJZSxNQUFNQSxzQkFBTixTQUFxQ0Msd0JBQXJDLENBQXFEO0FBQ2hFOzs7OztBQUtBQyxFQUFBQSxXQUFXLENBQUNDLFFBQUQsRUFBV0MsZ0JBQVgsRUFBNkI7QUFDcEM7QUFDQSxTQUFLQyxTQUFMLEdBQWlCRixRQUFqQjtBQUNBLFNBQUtHLGlCQUFMLEdBQXlCRixnQkFBekI7QUFDSDs7QUFFREcsRUFBQUEsUUFBUSxDQUFDQyxXQUFELEVBQWNDLE1BQWQsRUFBc0I7QUFDMUIsUUFBSUMsS0FBSyxHQUFHLEtBQUtMLFNBQUwsQ0FBZUcsV0FBZixDQUFaOztBQUNBLFFBQUlFLEtBQUssS0FBS0MsU0FBZCxFQUF5QjtBQUNyQkQsTUFBQUEsS0FBSyxHQUFHLEtBQUtKLGlCQUFMLENBQXVCRSxXQUF2QixDQUFSO0FBQ0g7O0FBQ0QsV0FBT0UsS0FBUDtBQUNIOztBQUVERSxFQUFBQSxRQUFRLENBQUNKLFdBQUQsRUFBY0MsTUFBZCxFQUFzQkksUUFBdEIsRUFBZ0M7QUFDcEMsVUFBTSxJQUFJQyxLQUFKLENBQVUsZ0RBQVYsQ0FBTjtBQUNIOztBQUVEQyxFQUFBQSxXQUFXLENBQUNQLFdBQUQsRUFBY0MsTUFBZCxFQUFzQjtBQUM3QixXQUFPLEtBQVA7QUFDSDs7QUFFRE8sRUFBQUEsV0FBVyxHQUFHO0FBQ1YsV0FBTyxJQUFQO0FBQ0g7O0FBOUIrRCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFRyYXZpcyBSYWxzdG9uXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vU2V0dGluZ3NIYW5kbGVyXCI7XHJcblxyXG4vKipcclxuICogR2V0cyBzZXR0aW5ncyBhdCB0aGUgXCJkZWZhdWx0XCIgbGV2ZWwuIFRoaXMgaGFuZGxlciBkb2VzIG5vdCBzdXBwb3J0IHNldHRpbmcgdmFsdWVzLlxyXG4gKiBUaGlzIGhhbmRsZXIgZG9lcyBub3QgbWFrZSB1c2Ugb2YgdGhlIHJvb21JZCBwYXJhbWV0ZXIuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEZWZhdWx0U2V0dGluZ3NIYW5kbGVyIGV4dGVuZHMgU2V0dGluZ3NIYW5kbGVyIHtcclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyBkZWZhdWx0IHNldHRpbmdzIGhhbmRsZXIgd2l0aCB0aGUgZ2l2ZW4gZGVmYXVsdHNcclxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBkZWZhdWx0cyBUaGUgZGVmYXVsdCBzZXR0aW5nIHZhbHVlcywga2V5ZWQgYnkgc2V0dGluZyBuYW1lLlxyXG4gICAgICogQHBhcmFtIHtvYmplY3R9IGludmVydGVkRGVmYXVsdHMgVGhlIGRlZmF1bHQgaW52ZXJ0ZWQgc2V0dGluZyB2YWx1ZXMsIGtleWVkIGJ5IHNldHRpbmcgbmFtZS5cclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoZGVmYXVsdHMsIGludmVydGVkRGVmYXVsdHMpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuX2RlZmF1bHRzID0gZGVmYXVsdHM7XHJcbiAgICAgICAgdGhpcy5faW52ZXJ0ZWREZWZhdWx0cyA9IGludmVydGVkRGVmYXVsdHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCkge1xyXG4gICAgICAgIGxldCB2YWx1ZSA9IHRoaXMuX2RlZmF1bHRzW3NldHRpbmdOYW1lXTtcclxuICAgICAgICBpZiAodmFsdWUgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHRoaXMuX2ludmVydGVkRGVmYXVsdHNbc2V0dGluZ05hbWVdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCwgbmV3VmFsdWUpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3Qgc2V0IHZhbHVlcyBvbiB0aGUgZGVmYXVsdCBsZXZlbCBoYW5kbGVyXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGNhblNldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19