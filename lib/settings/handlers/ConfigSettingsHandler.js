"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _SettingsHandler = _interopRequireDefault(require("./SettingsHandler"));

var _SdkConfig = _interopRequireDefault(require("../../SdkConfig"));

var _utils = require("matrix-js-sdk/src/utils");

/*
Copyright 2017 Travis Ralston
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Gets and sets settings at the "config" level. This handler does not make use of the
 * roomId parameter.
 */
class ConfigSettingsHandler extends _SettingsHandler.default {
  getValue(settingName, roomId) {
    const config = _SdkConfig.default.get() || {}; // Special case themes

    if (settingName === "theme") {
      return config["default_theme"];
    }

    const settingsConfig = config["settingDefaults"];
    if (!settingsConfig || (0, _utils.isNullOrUndefined)(settingsConfig[settingName])) return null;
    return settingsConfig[settingName];
  }

  setValue(settingName, roomId, newValue) {
    throw new Error("Cannot change settings at the config level");
  }

  canSetValue(settingName, roomId) {
    return false;
  }

  isSupported() {
    return true; // SdkConfig is always there
  }

}

exports.default = ConfigSettingsHandler;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXR0aW5ncy9oYW5kbGVycy9Db25maWdTZXR0aW5nc0hhbmRsZXIuanMiXSwibmFtZXMiOlsiQ29uZmlnU2V0dGluZ3NIYW5kbGVyIiwiU2V0dGluZ3NIYW5kbGVyIiwiZ2V0VmFsdWUiLCJzZXR0aW5nTmFtZSIsInJvb21JZCIsImNvbmZpZyIsIlNka0NvbmZpZyIsImdldCIsInNldHRpbmdzQ29uZmlnIiwic2V0VmFsdWUiLCJuZXdWYWx1ZSIsIkVycm9yIiwiY2FuU2V0VmFsdWUiLCJpc1N1cHBvcnRlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQW5CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkE7Ozs7QUFJZSxNQUFNQSxxQkFBTixTQUFvQ0Msd0JBQXBDLENBQW9EO0FBQy9EQyxFQUFBQSxRQUFRLENBQUNDLFdBQUQsRUFBY0MsTUFBZCxFQUFzQjtBQUMxQixVQUFNQyxNQUFNLEdBQUdDLG1CQUFVQyxHQUFWLE1BQW1CLEVBQWxDLENBRDBCLENBRzFCOztBQUNBLFFBQUlKLFdBQVcsS0FBSyxPQUFwQixFQUE2QjtBQUN6QixhQUFPRSxNQUFNLENBQUMsZUFBRCxDQUFiO0FBQ0g7O0FBRUQsVUFBTUcsY0FBYyxHQUFHSCxNQUFNLENBQUMsaUJBQUQsQ0FBN0I7QUFDQSxRQUFJLENBQUNHLGNBQUQsSUFBbUIsOEJBQWtCQSxjQUFjLENBQUNMLFdBQUQsQ0FBaEMsQ0FBdkIsRUFBdUUsT0FBTyxJQUFQO0FBQ3ZFLFdBQU9LLGNBQWMsQ0FBQ0wsV0FBRCxDQUFyQjtBQUNIOztBQUVETSxFQUFBQSxRQUFRLENBQUNOLFdBQUQsRUFBY0MsTUFBZCxFQUFzQk0sUUFBdEIsRUFBZ0M7QUFDcEMsVUFBTSxJQUFJQyxLQUFKLENBQVUsNENBQVYsQ0FBTjtBQUNIOztBQUVEQyxFQUFBQSxXQUFXLENBQUNULFdBQUQsRUFBY0MsTUFBZCxFQUFzQjtBQUM3QixXQUFPLEtBQVA7QUFDSDs7QUFFRFMsRUFBQUEsV0FBVyxHQUFHO0FBQ1YsV0FBTyxJQUFQLENBRFUsQ0FDRztBQUNoQjs7QUF4QjhEIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVHJhdmlzIFJhbHN0b25cclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgU2V0dGluZ3NIYW5kbGVyIGZyb20gXCIuL1NldHRpbmdzSGFuZGxlclwiO1xyXG5pbXBvcnQgU2RrQ29uZmlnIGZyb20gXCIuLi8uLi9TZGtDb25maWdcIjtcclxuaW1wb3J0IHtpc051bGxPclVuZGVmaW5lZH0gZnJvbSBcIm1hdHJpeC1qcy1zZGsvc3JjL3V0aWxzXCI7XHJcblxyXG4vKipcclxuICogR2V0cyBhbmQgc2V0cyBzZXR0aW5ncyBhdCB0aGUgXCJjb25maWdcIiBsZXZlbC4gVGhpcyBoYW5kbGVyIGRvZXMgbm90IG1ha2UgdXNlIG9mIHRoZVxyXG4gKiByb29tSWQgcGFyYW1ldGVyLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29uZmlnU2V0dGluZ3NIYW5kbGVyIGV4dGVuZHMgU2V0dGluZ3NIYW5kbGVyIHtcclxuICAgIGdldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpIHtcclxuICAgICAgICBjb25zdCBjb25maWcgPSBTZGtDb25maWcuZ2V0KCkgfHwge307XHJcblxyXG4gICAgICAgIC8vIFNwZWNpYWwgY2FzZSB0aGVtZXNcclxuICAgICAgICBpZiAoc2V0dGluZ05hbWUgPT09IFwidGhlbWVcIikge1xyXG4gICAgICAgICAgICByZXR1cm4gY29uZmlnW1wiZGVmYXVsdF90aGVtZVwiXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNldHRpbmdzQ29uZmlnID0gY29uZmlnW1wic2V0dGluZ0RlZmF1bHRzXCJdO1xyXG4gICAgICAgIGlmICghc2V0dGluZ3NDb25maWcgfHwgaXNOdWxsT3JVbmRlZmluZWQoc2V0dGluZ3NDb25maWdbc2V0dGluZ05hbWVdKSkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgcmV0dXJuIHNldHRpbmdzQ29uZmlnW3NldHRpbmdOYW1lXTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRWYWx1ZShzZXR0aW5nTmFtZSwgcm9vbUlkLCBuZXdWYWx1ZSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjaGFuZ2Ugc2V0dGluZ3MgYXQgdGhlIGNvbmZpZyBsZXZlbFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBjYW5TZXRWYWx1ZShzZXR0aW5nTmFtZSwgcm9vbUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzU3VwcG9ydGVkKCkge1xyXG4gICAgICAgIHJldHVybiB0cnVlOyAvLyBTZGtDb25maWcgaXMgYWx3YXlzIHRoZXJlXHJcbiAgICB9XHJcbn1cclxuIl19