"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SETTINGS = void 0;

var _matrixJsSdk = require("matrix-js-sdk");

var _languageHandler = require("../languageHandler");

var _NotificationControllers = require("./controllers/NotificationControllers");

var _CustomStatusController = _interopRequireDefault(require("./controllers/CustomStatusController"));

var _ThemeController = _interopRequireDefault(require("./controllers/ThemeController"));

var _PushToMatrixClientController = _interopRequireDefault(require("./controllers/PushToMatrixClientController"));

var _ReloadOnChangeController = _interopRequireDefault(require("./controllers/ReloadOnChangeController"));

var _RightPanelStorePhases = require("../stores/RightPanelStorePhases");

/*
Copyright 2017 Travis Ralston
Copyright 2018, 2019 New Vector Ltd.
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// These are just a bunch of helper arrays to avoid copy/pasting a bunch of times
const LEVELS_ROOM_SETTINGS = ['device', 'room-device', 'room-account', 'account', 'config'];
const LEVELS_ROOM_OR_ACCOUNT = ['room-account', 'account'];
const LEVELS_ROOM_SETTINGS_WITH_ROOM = ['device', 'room-device', 'room-account', 'account', 'config', 'room'];
const LEVELS_ACCOUNT_SETTINGS = ['device', 'account', 'config'];
const LEVELS_FEATURE = ['device', 'config'];
const LEVELS_DEVICE_ONLY_SETTINGS = ['device'];
const LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG = ['device', 'config'];
const SETTINGS = {
  // EXAMPLE SETTING:
  // "my-setting": {
  //     // Must be set to true for features. Default is 'false'.
  //     isFeature: false,
  //
  //     // Display names are strongly recommended for clarity.
  //     displayName: _td("Cool Name"),
  //
  //     // Display name can also be an object for different levels.
  //     //displayName: {
  //     //    "device": _td("Name for when the setting is used at 'device'"),
  //     //    "room": _td("Name for when the setting is used at 'room'"),
  //     //    "default": _td("The name for all other levels"),
  //     //}
  //
  //     // The supported levels are required. Preferably, use the preset arrays
  //     // at the top of this file to define this rather than a custom array.
  //     supportedLevels: [
  //         // The order does not matter.
  //
  //         "device",        // Affects the current device only
  //         "room-device",   // Affects the current room on the current device
  //         "room-account",  // Affects the current room for the current account
  //         "account",       // Affects the current account
  //         "room",          // Affects the current room (controlled by room admins)
  //         "config",        // Affects the current application
  //
  //         // "default" is always supported and does not get listed here.
  //     ],
  //
  //     // Required. Can be any data type. The value specified here should match
  //     // the data being stored (ie: if a boolean is used, the setting should
  //     // represent a boolean).
  //     default: {
  //         your: "value",
  //     },
  //
  //     // Optional settings controller. See SettingsController for more information.
  //     controller: new MySettingController(),
  //
  //     // Optional flag to make supportedLevels be respected as the order to handle
  //     // settings. The first element is treated as "most preferred". The "default"
  //     // level is always appended to the end.
  //     supportedLevelsAreOrdered: false,
  //
  //     // Optional value to invert a boolean setting's value. The string given will
  //     // be read as the setting's ID instead of the one provided as the key for the
  //     // setting definition. By setting this, the returned value will automatically
  //     // be inverted, except for when the default value is returned. Inversion will
  //     // occur after the controller is asked for an override. This should be used by
  //     // historical settings which we don't want existing user's values be wiped. Do
  //     // not use this for new settings.
  //     invertedSettingName: "my-negative-setting",
  // },
  "feature_pinning": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Message Pinning"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_custom_status": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Custom user status messages"),
    supportedLevels: LEVELS_FEATURE,
    default: false,
    controller: new _CustomStatusController.default()
  },
  "feature_custom_tags": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Group & filter rooms by custom tags (refresh to apply changes)"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_state_counters": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Render simple counters in room header"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_many_integration_managers": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Multiple integration managers"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_mjolnir": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Try out new ways to ignore people (experimental)"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_presence_in_room_list": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Show a presence dot next to DMs in the room list"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_custom_themes": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Support adding custom themes"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "mjolnirRooms": {
    supportedLevels: ['account'],
    default: []
  },
  "mjolnirPersonalRoom": {
    supportedLevels: ['account'],
    default: null
  },
  "feature_cross_signing": {
    isFeature: true,
    displayName: (0, _languageHandler._td)("Enable cross-signing to verify per-user instead of per-session (in development)"),
    supportedLevels: LEVELS_FEATURE,
    default: false
  },
  "feature_event_indexing": {
    isFeature: true,
    supportedLevels: LEVELS_FEATURE,
    displayName: (0, _languageHandler._td)("Enable local event indexing and E2EE search (requires restart)"),
    default: false
  },
  "feature_bridge_state": {
    isFeature: true,
    supportedLevels: LEVELS_FEATURE,
    displayName: (0, _languageHandler._td)("Show info about bridges in room settings"),
    default: false
  },
  "feature_invite_only_padlocks": {
    isFeature: true,
    supportedLevels: LEVELS_FEATURE,
    displayName: (0, _languageHandler._td)("Show padlocks on invite only rooms"),
    default: true
  },
  "MessageComposerInput.suggestEmoji": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Enable Emoji suggestions while typing'),
    default: true,
    invertedSettingName: 'MessageComposerInput.dontSuggestEmoji'
  },
  "useCompactLayout": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Use compact timeline layout'),
    default: false
  },
  "showRedactions": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: (0, _languageHandler._td)('Show a placeholder for removed messages'),
    default: true,
    invertedSettingName: 'hideRedactions'
  },
  "showJoinLeaves": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: (0, _languageHandler._td)('Show join/leave messages (invites/kicks/bans unaffected)'),
    default: true,
    invertedSettingName: 'hideJoinLeaves'
  },
  "showAvatarChanges": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: (0, _languageHandler._td)('Show avatar changes'),
    default: true,
    invertedSettingName: 'hideAvatarChanges'
  },
  "showDisplaynameChanges": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: (0, _languageHandler._td)('Show display name changes'),
    default: true,
    invertedSettingName: 'hideDisplaynameChanges'
  },
  "showReadReceipts": {
    supportedLevels: LEVELS_ROOM_SETTINGS,
    displayName: (0, _languageHandler._td)('Show read receipts sent by other users'),
    default: true,
    invertedSettingName: 'hideReadReceipts'
  },
  "showTwelveHourTimestamps": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Show timestamps in 12 hour format (e.g. 2:30pm)'),
    default: false
  },
  "alwaysShowTimestamps": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Always show message timestamps'),
    default: false
  },
  "autoplayGifsAndVideos": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Autoplay GIFs and videos'),
    default: false
  },
  "alwaysShowEncryptionIcons": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Always show encryption icons'),
    default: true
  },
  "showRoomRecoveryReminder": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Show a reminder to enable Secure Message Recovery in encrypted rooms'),
    default: true
  },
  "enableSyntaxHighlightLanguageDetection": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Enable automatic language detection for syntax highlighting'),
    default: false
  },
  "Pill.shouldShowPillAvatar": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Show avatars in user and room mentions'),
    default: true,
    invertedSettingName: 'Pill.shouldHidePillAvatar'
  },
  "TextualBody.enableBigEmoji": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Enable big emoji in chat'),
    default: true,
    invertedSettingName: 'TextualBody.disableBigEmoji'
  },
  "MessageComposerInput.isRichTextEnabled": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    default: false
  },
  "MessageComposer.showFormatting": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    default: false
  },
  "sendTypingNotifications": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Send typing notifications"),
    default: true,
    invertedSettingName: 'dontSendTypingNotifications'
  },
  "showTypingNotifications": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Show typing notifications"),
    default: true
  },
  "MessageComposerInput.autoReplaceEmoji": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Automatically replace plain text Emoji'),
    default: false
  },
  "VideoView.flipVideoHorizontally": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Mirror local video feed'),
    default: false
  },
  "TagPanel.enableTagPanel": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Enable Community Filter Panel'),
    default: true,
    invertedSettingName: 'TagPanel.disableTagPanel'
  },
  "theme": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    default: "light",
    controller: new _ThemeController.default()
  },
  "custom_themes": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    default: []
  },
  "use_system_theme": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: true,
    displayName: (0, _languageHandler._td)("Match system theme")
  },
  "webRtcAllowPeerToPeer": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    displayName: (0, _languageHandler._td)('Allow Peer-to-Peer for 1:1 calls'),
    default: true,
    invertedSettingName: 'webRtcForceTURN'
  },
  "webrtc_audiooutput": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: null
  },
  "webrtc_audioinput": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: null
  },
  "webrtc_videoinput": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: null
  },
  "language": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: "en"
  },
  "breadcrumb_rooms": {
    supportedLevels: ['account'],
    default: []
  },
  "room_directory_servers": {
    supportedLevels: ['account'],
    default: []
  },
  "integrationProvisioning": {
    supportedLevels: ['account'],
    default: true
  },
  "allowedWidgets": {
    supportedLevels: ['room-account'],
    default: {} // none allowed

  },
  "analyticsOptIn": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    displayName: (0, _languageHandler._td)('Send analytics data'),
    default: false
  },
  "showCookieBar": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: true
  },
  "autocompleteDelay": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: 200
  },
  "readMarkerInViewThresholdMs": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: 3000
  },
  "readMarkerOutOfViewThresholdMs": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: 30000
  },
  "blacklistUnverifiedDevices": {
    // We specifically want to have room-device > device so that users may set a device default
    // with a per-room override.
    supportedLevels: ['room-device', 'device'],
    supportedLevelsAreOrdered: true,
    displayName: {
      "default": (0, _languageHandler._td)('Never send encrypted messages to unverified sessions from this session'),
      "room-device": (0, _languageHandler._td)('Never send encrypted messages to unverified sessions in this room from this session')
    },
    default: false
  },
  "urlPreviewsEnabled": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: {
      "default": (0, _languageHandler._td)('Enable inline URL previews by default'),
      "room-account": (0, _languageHandler._td)("Enable URL previews for this room (only affects you)"),
      "room": (0, _languageHandler._td)("Enable URL previews by default for participants in this room")
    },
    default: true
  },
  "urlPreviewsEnabled_e2ee": {
    supportedLevels: ['room-device', 'room-account'],
    displayName: {
      "room-account": (0, _languageHandler._td)("Enable URL previews for this room (only affects you)")
    },
    default: false
  },
  "roomColor": {
    supportedLevels: LEVELS_ROOM_SETTINGS_WITH_ROOM,
    displayName: (0, _languageHandler._td)("Room Colour"),
    default: {
      primary_color: null,
      // Hex string, eg: #000000
      secondary_color: null // Hex string, eg: #000000

    }
  },
  "notificationsEnabled": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: false,
    controller: new _NotificationControllers.NotificationsEnabledController()
  },
  "notificationSound": {
    supportedLevels: LEVELS_ROOM_OR_ACCOUNT,
    default: false
  },
  "notificationBodyEnabled": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: true,
    controller: new _NotificationControllers.NotificationBodyEnabledController()
  },
  "audioNotificationsEnabled": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: true,
    controller: new _NotificationControllers.AudioNotificationsEnabledController()
  },
  "enableWidgetScreenshots": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Enable widget screenshots on supported widgets'),
    default: false
  },
  "PinnedEvents.isOpen": {
    supportedLevels: ['room-device'],
    default: false
  },
  "promptBeforeInviteUnknownUsers": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Prompt before sending invites to potentially invalid matrix IDs'),
    default: true
  },
  "showDeveloperTools": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)('Show developer tools'),
    default: false
  },
  "widgetOpenIDPermissions": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: {
      allow: [],
      deny: []
    }
  },
  "RoomList.orderAlphabetically": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Order rooms by name"),
    default: false
  },
  "RoomList.orderByImportance": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Show rooms with unread notifications first"),
    default: true
  },
  "breadcrumbs": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Show shortcuts to recently viewed rooms above the room list"),
    default: true
  },
  "showHiddenEventsInTimeline": {
    displayName: (0, _languageHandler._td)("Show hidden events in timeline"),
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: false
  },
  "lowBandwidth": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    displayName: (0, _languageHandler._td)('Low bandwidth mode'),
    default: false,
    controller: new _ReloadOnChangeController.default()
  },
  "fallbackICEServerAllowed": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    displayName: (0, _languageHandler._td)("Allow fallback call assist server turn.matrix.org when your homeserver " + "does not offer one (your IP address would be shared during a call)"),
    // This is a tri-state value, where `null` means "prompt the user".
    default: null
  },
  "sendReadReceipts": {
    supportedLevels: LEVELS_ROOM_SETTINGS,
    displayName: (0, _languageHandler._td)("Send read receipts for messages (requires compatible homeserver to disable)"),
    default: true
  },
  "showImages": {
    supportedLevels: LEVELS_ACCOUNT_SETTINGS,
    displayName: (0, _languageHandler._td)("Show previews/thumbnails for images"),
    default: true
  },
  "showRightPanelInRoom": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: false
  },
  "showRightPanelInGroup": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: false
  },
  "lastRightPanelPhaseForRoom": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: _RightPanelStorePhases.RIGHT_PANEL_PHASES.RoomMemberInfo
  },
  "lastRightPanelPhaseForGroup": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    default: _RightPanelStorePhases.RIGHT_PANEL_PHASES.GroupMemberList
  },
  "enableEventIndexing": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    displayName: (0, _languageHandler._td)("Enable message search in encrypted rooms"),
    default: true
  },
  "keepSecretStoragePassphraseForSession": {
    supportedLevels: ['device', 'config'],
    displayName: (0, _languageHandler._td)("Keep secret storage passphrase in memory for this session"),
    default: false
  },
  "crawlerSleepTime": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    displayName: (0, _languageHandler._td)("How fast should messages be downloaded."),
    default: 3000
  },
  "showCallButtonsInComposer": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS_WITH_CONFIG,
    default: true
  },
  "e2ee.manuallyVerifyAllSessions": {
    supportedLevels: LEVELS_DEVICE_ONLY_SETTINGS,
    displayName: (0, _languageHandler._td)("Manually verify all remote sessions"),
    default: false,
    controller: new _PushToMatrixClientController.default(_matrixJsSdk.MatrixClient.prototype.setCryptoTrustCrossSignedDevices, true)
  }
};
exports.SETTINGS = SETTINGS;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zZXR0aW5ncy9TZXR0aW5ncy5qcyJdLCJuYW1lcyI6WyJMRVZFTFNfUk9PTV9TRVRUSU5HUyIsIkxFVkVMU19ST09NX09SX0FDQ09VTlQiLCJMRVZFTFNfUk9PTV9TRVRUSU5HU19XSVRIX1JPT00iLCJMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyIsIkxFVkVMU19GRUFUVVJFIiwiTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTIiwiTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTX1dJVEhfQ09ORklHIiwiU0VUVElOR1MiLCJpc0ZlYXR1cmUiLCJkaXNwbGF5TmFtZSIsInN1cHBvcnRlZExldmVscyIsImRlZmF1bHQiLCJjb250cm9sbGVyIiwiQ3VzdG9tU3RhdHVzQ29udHJvbGxlciIsImludmVydGVkU2V0dGluZ05hbWUiLCJUaGVtZUNvbnRyb2xsZXIiLCJzdXBwb3J0ZWRMZXZlbHNBcmVPcmRlcmVkIiwicHJpbWFyeV9jb2xvciIsInNlY29uZGFyeV9jb2xvciIsIk5vdGlmaWNhdGlvbnNFbmFibGVkQ29udHJvbGxlciIsIk5vdGlmaWNhdGlvbkJvZHlFbmFibGVkQ29udHJvbGxlciIsIkF1ZGlvTm90aWZpY2F0aW9uc0VuYWJsZWRDb250cm9sbGVyIiwiYWxsb3ciLCJkZW55IiwiUmVsb2FkT25DaGFuZ2VDb250cm9sbGVyIiwiUklHSFRfUEFORUxfUEhBU0VTIiwiUm9vbU1lbWJlckluZm8iLCJHcm91cE1lbWJlckxpc3QiLCJQdXNoVG9NYXRyaXhDbGllbnRDb250cm9sbGVyIiwiTWF0cml4Q2xpZW50IiwicHJvdG90eXBlIiwic2V0Q3J5cHRvVHJ1c3RDcm9zc1NpZ25lZERldmljZXMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWtCQTs7QUFFQTs7QUFDQTs7QUFLQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUE5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0NBO0FBQ0EsTUFBTUEsb0JBQW9CLEdBQUcsQ0FBQyxRQUFELEVBQVcsYUFBWCxFQUEwQixjQUExQixFQUEwQyxTQUExQyxFQUFxRCxRQUFyRCxDQUE3QjtBQUNBLE1BQU1DLHNCQUFzQixHQUFHLENBQUMsY0FBRCxFQUFpQixTQUFqQixDQUEvQjtBQUNBLE1BQU1DLDhCQUE4QixHQUFHLENBQUMsUUFBRCxFQUFXLGFBQVgsRUFBMEIsY0FBMUIsRUFBMEMsU0FBMUMsRUFBcUQsUUFBckQsRUFBK0QsTUFBL0QsQ0FBdkM7QUFDQSxNQUFNQyx1QkFBdUIsR0FBRyxDQUFDLFFBQUQsRUFBVyxTQUFYLEVBQXNCLFFBQXRCLENBQWhDO0FBQ0EsTUFBTUMsY0FBYyxHQUFHLENBQUMsUUFBRCxFQUFXLFFBQVgsQ0FBdkI7QUFDQSxNQUFNQywyQkFBMkIsR0FBRyxDQUFDLFFBQUQsQ0FBcEM7QUFDQSxNQUFNQyx1Q0FBdUMsR0FBRyxDQUFDLFFBQUQsRUFBVyxRQUFYLENBQWhEO0FBRU8sTUFBTUMsUUFBUSxHQUFHO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFtQjtBQUNmQyxJQUFBQSxTQUFTLEVBQUUsSUFESTtBQUVmQyxJQUFBQSxXQUFXLEVBQUUsMEJBQUksaUJBQUosQ0FGRTtBQUdmQyxJQUFBQSxlQUFlLEVBQUVOLGNBSEY7QUFJZk8sSUFBQUEsT0FBTyxFQUFFO0FBSk0sR0F2REM7QUE2RHBCLDJCQUF5QjtBQUNyQkgsSUFBQUEsU0FBUyxFQUFFLElBRFU7QUFFckJDLElBQUFBLFdBQVcsRUFBRSwwQkFBSSw2QkFBSixDQUZRO0FBR3JCQyxJQUFBQSxlQUFlLEVBQUVOLGNBSEk7QUFJckJPLElBQUFBLE9BQU8sRUFBRSxLQUpZO0FBS3JCQyxJQUFBQSxVQUFVLEVBQUUsSUFBSUMsK0JBQUo7QUFMUyxHQTdETDtBQW9FcEIseUJBQXVCO0FBQ25CTCxJQUFBQSxTQUFTLEVBQUUsSUFEUTtBQUVuQkMsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGdFQUFKLENBRk07QUFHbkJDLElBQUFBLGVBQWUsRUFBRU4sY0FIRTtBQUluQk8sSUFBQUEsT0FBTyxFQUFFO0FBSlUsR0FwRUg7QUEwRXBCLDRCQUEwQjtBQUN0QkgsSUFBQUEsU0FBUyxFQUFFLElBRFc7QUFFdEJDLElBQUFBLFdBQVcsRUFBRSwwQkFBSSx1Q0FBSixDQUZTO0FBR3RCQyxJQUFBQSxlQUFlLEVBQUVOLGNBSEs7QUFJdEJPLElBQUFBLE9BQU8sRUFBRTtBQUphLEdBMUVOO0FBZ0ZwQix1Q0FBcUM7QUFDakNILElBQUFBLFNBQVMsRUFBRSxJQURzQjtBQUVqQ0MsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLCtCQUFKLENBRm9CO0FBR2pDQyxJQUFBQSxlQUFlLEVBQUVOLGNBSGdCO0FBSWpDTyxJQUFBQSxPQUFPLEVBQUU7QUFKd0IsR0FoRmpCO0FBc0ZwQixxQkFBbUI7QUFDZkgsSUFBQUEsU0FBUyxFQUFFLElBREk7QUFFZkMsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGtEQUFKLENBRkU7QUFHZkMsSUFBQUEsZUFBZSxFQUFFTixjQUhGO0FBSWZPLElBQUFBLE9BQU8sRUFBRTtBQUpNLEdBdEZDO0FBNEZwQixtQ0FBaUM7QUFDN0JILElBQUFBLFNBQVMsRUFBRSxJQURrQjtBQUU3QkMsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGtEQUFKLENBRmdCO0FBRzdCQyxJQUFBQSxlQUFlLEVBQUVOLGNBSFk7QUFJN0JPLElBQUFBLE9BQU8sRUFBRTtBQUpvQixHQTVGYjtBQWtHcEIsMkJBQXlCO0FBQ3JCSCxJQUFBQSxTQUFTLEVBQUUsSUFEVTtBQUVyQkMsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDhCQUFKLENBRlE7QUFHckJDLElBQUFBLGVBQWUsRUFBRU4sY0FISTtBQUlyQk8sSUFBQUEsT0FBTyxFQUFFO0FBSlksR0FsR0w7QUF3R3BCLGtCQUFnQjtBQUNaRCxJQUFBQSxlQUFlLEVBQUUsQ0FBQyxTQUFELENBREw7QUFFWkMsSUFBQUEsT0FBTyxFQUFFO0FBRkcsR0F4R0k7QUE0R3BCLHlCQUF1QjtBQUNuQkQsSUFBQUEsZUFBZSxFQUFFLENBQUMsU0FBRCxDQURFO0FBRW5CQyxJQUFBQSxPQUFPLEVBQUU7QUFGVSxHQTVHSDtBQWdIcEIsMkJBQXlCO0FBQ3JCSCxJQUFBQSxTQUFTLEVBQUUsSUFEVTtBQUVyQkMsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGlGQUFKLENBRlE7QUFHckJDLElBQUFBLGVBQWUsRUFBRU4sY0FISTtBQUlyQk8sSUFBQUEsT0FBTyxFQUFFO0FBSlksR0FoSEw7QUFzSHBCLDRCQUEwQjtBQUN0QkgsSUFBQUEsU0FBUyxFQUFFLElBRFc7QUFFdEJFLElBQUFBLGVBQWUsRUFBRU4sY0FGSztBQUd0QkssSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGdFQUFKLENBSFM7QUFJdEJFLElBQUFBLE9BQU8sRUFBRTtBQUphLEdBdEhOO0FBNEhwQiwwQkFBd0I7QUFDcEJILElBQUFBLFNBQVMsRUFBRSxJQURTO0FBRXBCRSxJQUFBQSxlQUFlLEVBQUVOLGNBRkc7QUFHcEJLLElBQUFBLFdBQVcsRUFBRSwwQkFBSSwwQ0FBSixDQUhPO0FBSXBCRSxJQUFBQSxPQUFPLEVBQUU7QUFKVyxHQTVISjtBQWtJcEIsa0NBQWdDO0FBQzVCSCxJQUFBQSxTQUFTLEVBQUUsSUFEaUI7QUFFNUJFLElBQUFBLGVBQWUsRUFBRU4sY0FGVztBQUc1QkssSUFBQUEsV0FBVyxFQUFFLDBCQUFJLG9DQUFKLENBSGU7QUFJNUJFLElBQUFBLE9BQU8sRUFBRTtBQUptQixHQWxJWjtBQXdJcEIsdUNBQXFDO0FBQ2pDRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURnQjtBQUVqQ00sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHVDQUFKLENBRm9CO0FBR2pDRSxJQUFBQSxPQUFPLEVBQUUsSUFId0I7QUFJakNHLElBQUFBLG1CQUFtQixFQUFFO0FBSlksR0F4SWpCO0FBOElwQixzQkFBb0I7QUFDaEJKLElBQUFBLGVBQWUsRUFBRVAsdUJBREQ7QUFFaEJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSw2QkFBSixDQUZHO0FBR2hCRSxJQUFBQSxPQUFPLEVBQUU7QUFITyxHQTlJQTtBQW1KcEIsb0JBQWtCO0FBQ2RELElBQUFBLGVBQWUsRUFBRVIsOEJBREg7QUFFZE8sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHlDQUFKLENBRkM7QUFHZEUsSUFBQUEsT0FBTyxFQUFFLElBSEs7QUFJZEcsSUFBQUEsbUJBQW1CLEVBQUU7QUFKUCxHQW5KRTtBQXlKcEIsb0JBQWtCO0FBQ2RKLElBQUFBLGVBQWUsRUFBRVIsOEJBREg7QUFFZE8sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDBEQUFKLENBRkM7QUFHZEUsSUFBQUEsT0FBTyxFQUFFLElBSEs7QUFJZEcsSUFBQUEsbUJBQW1CLEVBQUU7QUFKUCxHQXpKRTtBQStKcEIsdUJBQXFCO0FBQ2pCSixJQUFBQSxlQUFlLEVBQUVSLDhCQURBO0FBRWpCTyxJQUFBQSxXQUFXLEVBQUUsMEJBQUkscUJBQUosQ0FGSTtBQUdqQkUsSUFBQUEsT0FBTyxFQUFFLElBSFE7QUFJakJHLElBQUFBLG1CQUFtQixFQUFFO0FBSkosR0EvSkQ7QUFxS3BCLDRCQUEwQjtBQUN0QkosSUFBQUEsZUFBZSxFQUFFUiw4QkFESztBQUV0Qk8sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDJCQUFKLENBRlM7QUFHdEJFLElBQUFBLE9BQU8sRUFBRSxJQUhhO0FBSXRCRyxJQUFBQSxtQkFBbUIsRUFBRTtBQUpDLEdBcktOO0FBMktwQixzQkFBb0I7QUFDaEJKLElBQUFBLGVBQWUsRUFBRVYsb0JBREQ7QUFFaEJTLElBQUFBLFdBQVcsRUFBRSwwQkFBSSx3Q0FBSixDQUZHO0FBR2hCRSxJQUFBQSxPQUFPLEVBQUUsSUFITztBQUloQkcsSUFBQUEsbUJBQW1CLEVBQUU7QUFKTCxHQTNLQTtBQWlMcEIsOEJBQTRCO0FBQ3hCSixJQUFBQSxlQUFlLEVBQUVQLHVCQURPO0FBRXhCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksaURBQUosQ0FGVztBQUd4QkUsSUFBQUEsT0FBTyxFQUFFO0FBSGUsR0FqTFI7QUFzTHBCLDBCQUF3QjtBQUNwQkQsSUFBQUEsZUFBZSxFQUFFUCx1QkFERztBQUVwQk0sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGdDQUFKLENBRk87QUFHcEJFLElBQUFBLE9BQU8sRUFBRTtBQUhXLEdBdExKO0FBMkxwQiwyQkFBeUI7QUFDckJELElBQUFBLGVBQWUsRUFBRVAsdUJBREk7QUFFckJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSwwQkFBSixDQUZRO0FBR3JCRSxJQUFBQSxPQUFPLEVBQUU7QUFIWSxHQTNMTDtBQWdNcEIsK0JBQTZCO0FBQ3pCRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURRO0FBRXpCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksOEJBQUosQ0FGWTtBQUd6QkUsSUFBQUEsT0FBTyxFQUFFO0FBSGdCLEdBaE1UO0FBcU1wQiw4QkFBNEI7QUFDeEJELElBQUFBLGVBQWUsRUFBRVAsdUJBRE87QUFFeEJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSxzRUFBSixDQUZXO0FBR3hCRSxJQUFBQSxPQUFPLEVBQUU7QUFIZSxHQXJNUjtBQTBNcEIsNENBQTBDO0FBQ3RDRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURxQjtBQUV0Q00sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDZEQUFKLENBRnlCO0FBR3RDRSxJQUFBQSxPQUFPLEVBQUU7QUFINkIsR0ExTXRCO0FBK01wQiwrQkFBNkI7QUFDekJELElBQUFBLGVBQWUsRUFBRVAsdUJBRFE7QUFFekJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSx3Q0FBSixDQUZZO0FBR3pCRSxJQUFBQSxPQUFPLEVBQUUsSUFIZ0I7QUFJekJHLElBQUFBLG1CQUFtQixFQUFFO0FBSkksR0EvTVQ7QUFxTnBCLGdDQUE4QjtBQUMxQkosSUFBQUEsZUFBZSxFQUFFUCx1QkFEUztBQUUxQk0sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDBCQUFKLENBRmE7QUFHMUJFLElBQUFBLE9BQU8sRUFBRSxJQUhpQjtBQUkxQkcsSUFBQUEsbUJBQW1CLEVBQUU7QUFKSyxHQXJOVjtBQTJOcEIsNENBQTBDO0FBQ3RDSixJQUFBQSxlQUFlLEVBQUVQLHVCQURxQjtBQUV0Q1EsSUFBQUEsT0FBTyxFQUFFO0FBRjZCLEdBM050QjtBQStOcEIsb0NBQWtDO0FBQzlCRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURhO0FBRTlCUSxJQUFBQSxPQUFPLEVBQUU7QUFGcUIsR0EvTmQ7QUFtT3BCLDZCQUEyQjtBQUN2QkQsSUFBQUEsZUFBZSxFQUFFUCx1QkFETTtBQUV2Qk0sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDJCQUFKLENBRlU7QUFHdkJFLElBQUFBLE9BQU8sRUFBRSxJQUhjO0FBSXZCRyxJQUFBQSxtQkFBbUIsRUFBRTtBQUpFLEdBbk9QO0FBeU9wQiw2QkFBMkI7QUFDdkJKLElBQUFBLGVBQWUsRUFBRVAsdUJBRE07QUFFdkJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSwyQkFBSixDQUZVO0FBR3ZCRSxJQUFBQSxPQUFPLEVBQUU7QUFIYyxHQXpPUDtBQThPcEIsMkNBQXlDO0FBQ3JDRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURvQjtBQUVyQ00sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHdDQUFKLENBRndCO0FBR3JDRSxJQUFBQSxPQUFPLEVBQUU7QUFINEIsR0E5T3JCO0FBbVBwQixxQ0FBbUM7QUFDL0JELElBQUFBLGVBQWUsRUFBRVAsdUJBRGM7QUFFL0JNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSx5QkFBSixDQUZrQjtBQUcvQkUsSUFBQUEsT0FBTyxFQUFFO0FBSHNCLEdBblBmO0FBd1BwQiw2QkFBMkI7QUFDdkJELElBQUFBLGVBQWUsRUFBRVAsdUJBRE07QUFFdkJNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSwrQkFBSixDQUZVO0FBR3ZCRSxJQUFBQSxPQUFPLEVBQUUsSUFIYztBQUl2QkcsSUFBQUEsbUJBQW1CLEVBQUU7QUFKRSxHQXhQUDtBQThQcEIsV0FBUztBQUNMSixJQUFBQSxlQUFlLEVBQUVQLHVCQURaO0FBRUxRLElBQUFBLE9BQU8sRUFBRSxPQUZKO0FBR0xDLElBQUFBLFVBQVUsRUFBRSxJQUFJRyx3QkFBSjtBQUhQLEdBOVBXO0FBbVFwQixtQkFBaUI7QUFDYkwsSUFBQUEsZUFBZSxFQUFFUCx1QkFESjtBQUViUSxJQUFBQSxPQUFPLEVBQUU7QUFGSSxHQW5RRztBQXVRcEIsc0JBQW9CO0FBQ2hCRCxJQUFBQSxlQUFlLEVBQUVMLDJCQUREO0FBRWhCTSxJQUFBQSxPQUFPLEVBQUUsSUFGTztBQUdoQkYsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLG9CQUFKO0FBSEcsR0F2UUE7QUE0UXBCLDJCQUF5QjtBQUNyQkMsSUFBQUEsZUFBZSxFQUFFSix1Q0FESTtBQUVyQkcsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGtDQUFKLENBRlE7QUFHckJFLElBQUFBLE9BQU8sRUFBRSxJQUhZO0FBSXJCRyxJQUFBQSxtQkFBbUIsRUFBRTtBQUpBLEdBNVFMO0FBa1JwQix3QkFBc0I7QUFDbEJKLElBQUFBLGVBQWUsRUFBRUwsMkJBREM7QUFFbEJNLElBQUFBLE9BQU8sRUFBRTtBQUZTLEdBbFJGO0FBc1JwQix1QkFBcUI7QUFDakJELElBQUFBLGVBQWUsRUFBRUwsMkJBREE7QUFFakJNLElBQUFBLE9BQU8sRUFBRTtBQUZRLEdBdFJEO0FBMFJwQix1QkFBcUI7QUFDakJELElBQUFBLGVBQWUsRUFBRUwsMkJBREE7QUFFakJNLElBQUFBLE9BQU8sRUFBRTtBQUZRLEdBMVJEO0FBOFJwQixjQUFZO0FBQ1JELElBQUFBLGVBQWUsRUFBRUosdUNBRFQ7QUFFUkssSUFBQUEsT0FBTyxFQUFFO0FBRkQsR0E5UlE7QUFrU3BCLHNCQUFvQjtBQUNoQkQsSUFBQUEsZUFBZSxFQUFFLENBQUMsU0FBRCxDQUREO0FBRWhCQyxJQUFBQSxPQUFPLEVBQUU7QUFGTyxHQWxTQTtBQXNTcEIsNEJBQTBCO0FBQ3RCRCxJQUFBQSxlQUFlLEVBQUUsQ0FBQyxTQUFELENBREs7QUFFdEJDLElBQUFBLE9BQU8sRUFBRTtBQUZhLEdBdFNOO0FBMFNwQiw2QkFBMkI7QUFDdkJELElBQUFBLGVBQWUsRUFBRSxDQUFDLFNBQUQsQ0FETTtBQUV2QkMsSUFBQUEsT0FBTyxFQUFFO0FBRmMsR0ExU1A7QUE4U3BCLG9CQUFrQjtBQUNkRCxJQUFBQSxlQUFlLEVBQUUsQ0FBQyxjQUFELENBREg7QUFFZEMsSUFBQUEsT0FBTyxFQUFFLEVBRkssQ0FFRDs7QUFGQyxHQTlTRTtBQWtUcEIsb0JBQWtCO0FBQ2RELElBQUFBLGVBQWUsRUFBRUosdUNBREg7QUFFZEcsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHFCQUFKLENBRkM7QUFHZEUsSUFBQUEsT0FBTyxFQUFFO0FBSEssR0FsVEU7QUF1VHBCLG1CQUFpQjtBQUNiRCxJQUFBQSxlQUFlLEVBQUVKLHVDQURKO0FBRWJLLElBQUFBLE9BQU8sRUFBRTtBQUZJLEdBdlRHO0FBMlRwQix1QkFBcUI7QUFDakJELElBQUFBLGVBQWUsRUFBRUosdUNBREE7QUFFakJLLElBQUFBLE9BQU8sRUFBRTtBQUZRLEdBM1REO0FBK1RwQixpQ0FBK0I7QUFDM0JELElBQUFBLGVBQWUsRUFBRUosdUNBRFU7QUFFM0JLLElBQUFBLE9BQU8sRUFBRTtBQUZrQixHQS9UWDtBQW1VcEIsb0NBQWtDO0FBQzlCRCxJQUFBQSxlQUFlLEVBQUVKLHVDQURhO0FBRTlCSyxJQUFBQSxPQUFPLEVBQUU7QUFGcUIsR0FuVWQ7QUF1VXBCLGdDQUE4QjtBQUMxQjtBQUNBO0FBQ0FELElBQUFBLGVBQWUsRUFBRSxDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FIUztBQUkxQk0sSUFBQUEseUJBQXlCLEVBQUUsSUFKRDtBQUsxQlAsSUFBQUEsV0FBVyxFQUFFO0FBQ1QsaUJBQVcsMEJBQUksd0VBQUosQ0FERjtBQUVULHFCQUFlLDBCQUFJLHFGQUFKO0FBRk4sS0FMYTtBQVMxQkUsSUFBQUEsT0FBTyxFQUFFO0FBVGlCLEdBdlVWO0FBa1ZwQix3QkFBc0I7QUFDbEJELElBQUFBLGVBQWUsRUFBRVIsOEJBREM7QUFFbEJPLElBQUFBLFdBQVcsRUFBRTtBQUNULGlCQUFXLDBCQUFJLHVDQUFKLENBREY7QUFFVCxzQkFBZ0IsMEJBQUksc0RBQUosQ0FGUDtBQUdULGNBQVEsMEJBQUksOERBQUo7QUFIQyxLQUZLO0FBT2xCRSxJQUFBQSxPQUFPLEVBQUU7QUFQUyxHQWxWRjtBQTJWcEIsNkJBQTJCO0FBQ3ZCRCxJQUFBQSxlQUFlLEVBQUUsQ0FBQyxhQUFELEVBQWdCLGNBQWhCLENBRE07QUFFdkJELElBQUFBLFdBQVcsRUFBRTtBQUNULHNCQUFnQiwwQkFBSSxzREFBSjtBQURQLEtBRlU7QUFLdkJFLElBQUFBLE9BQU8sRUFBRTtBQUxjLEdBM1ZQO0FBa1dwQixlQUFhO0FBQ1RELElBQUFBLGVBQWUsRUFBRVIsOEJBRFI7QUFFVE8sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLGFBQUosQ0FGSjtBQUdURSxJQUFBQSxPQUFPLEVBQUU7QUFDTE0sTUFBQUEsYUFBYSxFQUFFLElBRFY7QUFDZ0I7QUFDckJDLE1BQUFBLGVBQWUsRUFBRSxJQUZaLENBRWtCOztBQUZsQjtBQUhBLEdBbFdPO0FBMFdwQiwwQkFBd0I7QUFDcEJSLElBQUFBLGVBQWUsRUFBRUwsMkJBREc7QUFFcEJNLElBQUFBLE9BQU8sRUFBRSxLQUZXO0FBR3BCQyxJQUFBQSxVQUFVLEVBQUUsSUFBSU8sdURBQUo7QUFIUSxHQTFXSjtBQStXcEIsdUJBQXFCO0FBQ2pCVCxJQUFBQSxlQUFlLEVBQUVULHNCQURBO0FBRWpCVSxJQUFBQSxPQUFPLEVBQUU7QUFGUSxHQS9XRDtBQW1YcEIsNkJBQTJCO0FBQ3ZCRCxJQUFBQSxlQUFlLEVBQUVMLDJCQURNO0FBRXZCTSxJQUFBQSxPQUFPLEVBQUUsSUFGYztBQUd2QkMsSUFBQUEsVUFBVSxFQUFFLElBQUlRLDBEQUFKO0FBSFcsR0FuWFA7QUF3WHBCLCtCQUE2QjtBQUN6QlYsSUFBQUEsZUFBZSxFQUFFTCwyQkFEUTtBQUV6Qk0sSUFBQUEsT0FBTyxFQUFFLElBRmdCO0FBR3pCQyxJQUFBQSxVQUFVLEVBQUUsSUFBSVMsNERBQUo7QUFIYSxHQXhYVDtBQTZYcEIsNkJBQTJCO0FBQ3ZCWCxJQUFBQSxlQUFlLEVBQUVQLHVCQURNO0FBRXZCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksZ0RBQUosQ0FGVTtBQUd2QkUsSUFBQUEsT0FBTyxFQUFFO0FBSGMsR0E3WFA7QUFrWXBCLHlCQUF1QjtBQUNuQkQsSUFBQUEsZUFBZSxFQUFFLENBQUMsYUFBRCxDQURFO0FBRW5CQyxJQUFBQSxPQUFPLEVBQUU7QUFGVSxHQWxZSDtBQXNZcEIsb0NBQWtDO0FBQzlCRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURhO0FBRTlCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksaUVBQUosQ0FGaUI7QUFHOUJFLElBQUFBLE9BQU8sRUFBRTtBQUhxQixHQXRZZDtBQTJZcEIsd0JBQXNCO0FBQ2xCRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURDO0FBRWxCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksc0JBQUosQ0FGSztBQUdsQkUsSUFBQUEsT0FBTyxFQUFFO0FBSFMsR0EzWUY7QUFnWnBCLDZCQUEyQjtBQUN2QkQsSUFBQUEsZUFBZSxFQUFFTCwyQkFETTtBQUV2Qk0sSUFBQUEsT0FBTyxFQUFFO0FBQ0xXLE1BQUFBLEtBQUssRUFBRSxFQURGO0FBRUxDLE1BQUFBLElBQUksRUFBRTtBQUZEO0FBRmMsR0FoWlA7QUF1WnBCLGtDQUFnQztBQUM1QmIsSUFBQUEsZUFBZSxFQUFFUCx1QkFEVztBQUU1Qk0sSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHFCQUFKLENBRmU7QUFHNUJFLElBQUFBLE9BQU8sRUFBRTtBQUhtQixHQXZaWjtBQTRacEIsZ0NBQThCO0FBQzFCRCxJQUFBQSxlQUFlLEVBQUVQLHVCQURTO0FBRTFCTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUksNENBQUosQ0FGYTtBQUcxQkUsSUFBQUEsT0FBTyxFQUFFO0FBSGlCLEdBNVpWO0FBaWFwQixpQkFBZTtBQUNYRCxJQUFBQSxlQUFlLEVBQUVQLHVCQUROO0FBRVhNLElBQUFBLFdBQVcsRUFBRSwwQkFBSSw2REFBSixDQUZGO0FBR1hFLElBQUFBLE9BQU8sRUFBRTtBQUhFLEdBamFLO0FBc2FwQixnQ0FBOEI7QUFDMUJGLElBQUFBLFdBQVcsRUFBRSwwQkFBSSxnQ0FBSixDQURhO0FBRTFCQyxJQUFBQSxlQUFlLEVBQUVMLDJCQUZTO0FBRzFCTSxJQUFBQSxPQUFPLEVBQUU7QUFIaUIsR0F0YVY7QUEyYXBCLGtCQUFnQjtBQUNaRCxJQUFBQSxlQUFlLEVBQUVKLHVDQURMO0FBRVpHLElBQUFBLFdBQVcsRUFBRSwwQkFBSSxvQkFBSixDQUZEO0FBR1pFLElBQUFBLE9BQU8sRUFBRSxLQUhHO0FBSVpDLElBQUFBLFVBQVUsRUFBRSxJQUFJWSxpQ0FBSjtBQUpBLEdBM2FJO0FBaWJwQiw4QkFBNEI7QUFDeEJkLElBQUFBLGVBQWUsRUFBRUwsMkJBRE87QUFFeEJJLElBQUFBLFdBQVcsRUFBRSwwQkFDVCw0RUFDQSxvRUFGUyxDQUZXO0FBTXhCO0FBQ0FFLElBQUFBLE9BQU8sRUFBRTtBQVBlLEdBamJSO0FBMGJwQixzQkFBb0I7QUFDaEJELElBQUFBLGVBQWUsRUFBRVYsb0JBREQ7QUFFaEJTLElBQUFBLFdBQVcsRUFBRSwwQkFDVCw2RUFEUyxDQUZHO0FBS2hCRSxJQUFBQSxPQUFPLEVBQUU7QUFMTyxHQTFiQTtBQWljcEIsZ0JBQWM7QUFDVkQsSUFBQUEsZUFBZSxFQUFFUCx1QkFEUDtBQUVWTSxJQUFBQSxXQUFXLEVBQUUsMEJBQUkscUNBQUosQ0FGSDtBQUdWRSxJQUFBQSxPQUFPLEVBQUU7QUFIQyxHQWpjTTtBQXNjcEIsMEJBQXdCO0FBQ3BCRCxJQUFBQSxlQUFlLEVBQUVMLDJCQURHO0FBRXBCTSxJQUFBQSxPQUFPLEVBQUU7QUFGVyxHQXRjSjtBQTBjcEIsMkJBQXlCO0FBQ3JCRCxJQUFBQSxlQUFlLEVBQUVMLDJCQURJO0FBRXJCTSxJQUFBQSxPQUFPLEVBQUU7QUFGWSxHQTFjTDtBQThjcEIsZ0NBQThCO0FBQzFCRCxJQUFBQSxlQUFlLEVBQUVMLDJCQURTO0FBRTFCTSxJQUFBQSxPQUFPLEVBQUVjLDBDQUFtQkM7QUFGRixHQTljVjtBQWtkcEIsaUNBQStCO0FBQzNCaEIsSUFBQUEsZUFBZSxFQUFFTCwyQkFEVTtBQUUzQk0sSUFBQUEsT0FBTyxFQUFFYywwQ0FBbUJFO0FBRkQsR0FsZFg7QUFzZHBCLHlCQUF1QjtBQUNuQmpCLElBQUFBLGVBQWUsRUFBRUwsMkJBREU7QUFFbkJJLElBQUFBLFdBQVcsRUFBRSwwQkFBSSwwQ0FBSixDQUZNO0FBR25CRSxJQUFBQSxPQUFPLEVBQUU7QUFIVSxHQXRkSDtBQTJkcEIsMkNBQXlDO0FBQ3BDRCxJQUFBQSxlQUFlLEVBQUUsQ0FBQyxRQUFELEVBQVcsUUFBWCxDQURtQjtBQUVwQ0QsSUFBQUEsV0FBVyxFQUFFLDBCQUFJLDJEQUFKLENBRnVCO0FBR3BDRSxJQUFBQSxPQUFPLEVBQUU7QUFIMkIsR0EzZHJCO0FBZ2VwQixzQkFBb0I7QUFDaEJELElBQUFBLGVBQWUsRUFBRUwsMkJBREQ7QUFFaEJJLElBQUFBLFdBQVcsRUFBRSwwQkFBSSx5Q0FBSixDQUZHO0FBR2hCRSxJQUFBQSxPQUFPLEVBQUU7QUFITyxHQWhlQTtBQXFlcEIsK0JBQTZCO0FBQ3pCRCxJQUFBQSxlQUFlLEVBQUVKLHVDQURRO0FBRXpCSyxJQUFBQSxPQUFPLEVBQUU7QUFGZ0IsR0FyZVQ7QUF5ZXBCLG9DQUFrQztBQUM5QkQsSUFBQUEsZUFBZSxFQUFFTCwyQkFEYTtBQUU5QkksSUFBQUEsV0FBVyxFQUFFLDBCQUFJLHFDQUFKLENBRmlCO0FBRzlCRSxJQUFBQSxPQUFPLEVBQUUsS0FIcUI7QUFJOUJDLElBQUFBLFVBQVUsRUFBRSxJQUFJZ0IscUNBQUosQ0FDUkMsMEJBQWFDLFNBQWIsQ0FBdUJDLGdDQURmLEVBQ2lELElBRGpEO0FBSmtCO0FBemVkLENBQWpCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTcgVHJhdmlzIFJhbHN0b25cclxuQ29weXJpZ2h0IDIwMTgsIDIwMTkgTmV3IFZlY3RvciBMdGQuXHJcbkNvcHlyaWdodCAyMDE5LCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IHtNYXRyaXhDbGllbnR9IGZyb20gJ21hdHJpeC1qcy1zZGsnO1xyXG5cclxuaW1wb3J0IHtfdGR9IGZyb20gJy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCB7XHJcbiAgICBBdWRpb05vdGlmaWNhdGlvbnNFbmFibGVkQ29udHJvbGxlcixcclxuICAgIE5vdGlmaWNhdGlvbkJvZHlFbmFibGVkQ29udHJvbGxlcixcclxuICAgIE5vdGlmaWNhdGlvbnNFbmFibGVkQ29udHJvbGxlcixcclxufSBmcm9tIFwiLi9jb250cm9sbGVycy9Ob3RpZmljYXRpb25Db250cm9sbGVyc1wiO1xyXG5pbXBvcnQgQ3VzdG9tU3RhdHVzQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9DdXN0b21TdGF0dXNDb250cm9sbGVyXCI7XHJcbmltcG9ydCBUaGVtZUNvbnRyb2xsZXIgZnJvbSAnLi9jb250cm9sbGVycy9UaGVtZUNvbnRyb2xsZXInO1xyXG5pbXBvcnQgUHVzaFRvTWF0cml4Q2xpZW50Q29udHJvbGxlciBmcm9tICcuL2NvbnRyb2xsZXJzL1B1c2hUb01hdHJpeENsaWVudENvbnRyb2xsZXInO1xyXG5pbXBvcnQgUmVsb2FkT25DaGFuZ2VDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL1JlbG9hZE9uQ2hhbmdlQ29udHJvbGxlclwiO1xyXG5pbXBvcnQge1JJR0hUX1BBTkVMX1BIQVNFU30gZnJvbSBcIi4uL3N0b3Jlcy9SaWdodFBhbmVsU3RvcmVQaGFzZXNcIjtcclxuXHJcbi8vIFRoZXNlIGFyZSBqdXN0IGEgYnVuY2ggb2YgaGVscGVyIGFycmF5cyB0byBhdm9pZCBjb3B5L3Bhc3RpbmcgYSBidW5jaCBvZiB0aW1lc1xyXG5jb25zdCBMRVZFTFNfUk9PTV9TRVRUSU5HUyA9IFsnZGV2aWNlJywgJ3Jvb20tZGV2aWNlJywgJ3Jvb20tYWNjb3VudCcsICdhY2NvdW50JywgJ2NvbmZpZyddO1xyXG5jb25zdCBMRVZFTFNfUk9PTV9PUl9BQ0NPVU5UID0gWydyb29tLWFjY291bnQnLCAnYWNjb3VudCddO1xyXG5jb25zdCBMRVZFTFNfUk9PTV9TRVRUSU5HU19XSVRIX1JPT00gPSBbJ2RldmljZScsICdyb29tLWRldmljZScsICdyb29tLWFjY291bnQnLCAnYWNjb3VudCcsICdjb25maWcnLCAncm9vbSddO1xyXG5jb25zdCBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyA9IFsnZGV2aWNlJywgJ2FjY291bnQnLCAnY29uZmlnJ107XHJcbmNvbnN0IExFVkVMU19GRUFUVVJFID0gWydkZXZpY2UnLCAnY29uZmlnJ107XHJcbmNvbnN0IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyA9IFsnZGV2aWNlJ107XHJcbmNvbnN0IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyA9IFsnZGV2aWNlJywgJ2NvbmZpZyddO1xyXG5cclxuZXhwb3J0IGNvbnN0IFNFVFRJTkdTID0ge1xyXG4gICAgLy8gRVhBTVBMRSBTRVRUSU5HOlxyXG4gICAgLy8gXCJteS1zZXR0aW5nXCI6IHtcclxuICAgIC8vICAgICAvLyBNdXN0IGJlIHNldCB0byB0cnVlIGZvciBmZWF0dXJlcy4gRGVmYXVsdCBpcyAnZmFsc2UnLlxyXG4gICAgLy8gICAgIGlzRmVhdHVyZTogZmFsc2UsXHJcbiAgICAvL1xyXG4gICAgLy8gICAgIC8vIERpc3BsYXkgbmFtZXMgYXJlIHN0cm9uZ2x5IHJlY29tbWVuZGVkIGZvciBjbGFyaXR5LlxyXG4gICAgLy8gICAgIGRpc3BsYXlOYW1lOiBfdGQoXCJDb29sIE5hbWVcIiksXHJcbiAgICAvL1xyXG4gICAgLy8gICAgIC8vIERpc3BsYXkgbmFtZSBjYW4gYWxzbyBiZSBhbiBvYmplY3QgZm9yIGRpZmZlcmVudCBsZXZlbHMuXHJcbiAgICAvLyAgICAgLy9kaXNwbGF5TmFtZToge1xyXG4gICAgLy8gICAgIC8vICAgIFwiZGV2aWNlXCI6IF90ZChcIk5hbWUgZm9yIHdoZW4gdGhlIHNldHRpbmcgaXMgdXNlZCBhdCAnZGV2aWNlJ1wiKSxcclxuICAgIC8vICAgICAvLyAgICBcInJvb21cIjogX3RkKFwiTmFtZSBmb3Igd2hlbiB0aGUgc2V0dGluZyBpcyB1c2VkIGF0ICdyb29tJ1wiKSxcclxuICAgIC8vICAgICAvLyAgICBcImRlZmF1bHRcIjogX3RkKFwiVGhlIG5hbWUgZm9yIGFsbCBvdGhlciBsZXZlbHNcIiksXHJcbiAgICAvLyAgICAgLy99XHJcbiAgICAvL1xyXG4gICAgLy8gICAgIC8vIFRoZSBzdXBwb3J0ZWQgbGV2ZWxzIGFyZSByZXF1aXJlZC4gUHJlZmVyYWJseSwgdXNlIHRoZSBwcmVzZXQgYXJyYXlzXHJcbiAgICAvLyAgICAgLy8gYXQgdGhlIHRvcCBvZiB0aGlzIGZpbGUgdG8gZGVmaW5lIHRoaXMgcmF0aGVyIHRoYW4gYSBjdXN0b20gYXJyYXkuXHJcbiAgICAvLyAgICAgc3VwcG9ydGVkTGV2ZWxzOiBbXHJcbiAgICAvLyAgICAgICAgIC8vIFRoZSBvcmRlciBkb2VzIG5vdCBtYXR0ZXIuXHJcbiAgICAvL1xyXG4gICAgLy8gICAgICAgICBcImRldmljZVwiLCAgICAgICAgLy8gQWZmZWN0cyB0aGUgY3VycmVudCBkZXZpY2Ugb25seVxyXG4gICAgLy8gICAgICAgICBcInJvb20tZGV2aWNlXCIsICAgLy8gQWZmZWN0cyB0aGUgY3VycmVudCByb29tIG9uIHRoZSBjdXJyZW50IGRldmljZVxyXG4gICAgLy8gICAgICAgICBcInJvb20tYWNjb3VudFwiLCAgLy8gQWZmZWN0cyB0aGUgY3VycmVudCByb29tIGZvciB0aGUgY3VycmVudCBhY2NvdW50XHJcbiAgICAvLyAgICAgICAgIFwiYWNjb3VudFwiLCAgICAgICAvLyBBZmZlY3RzIHRoZSBjdXJyZW50IGFjY291bnRcclxuICAgIC8vICAgICAgICAgXCJyb29tXCIsICAgICAgICAgIC8vIEFmZmVjdHMgdGhlIGN1cnJlbnQgcm9vbSAoY29udHJvbGxlZCBieSByb29tIGFkbWlucylcclxuICAgIC8vICAgICAgICAgXCJjb25maWdcIiwgICAgICAgIC8vIEFmZmVjdHMgdGhlIGN1cnJlbnQgYXBwbGljYXRpb25cclxuICAgIC8vXHJcbiAgICAvLyAgICAgICAgIC8vIFwiZGVmYXVsdFwiIGlzIGFsd2F5cyBzdXBwb3J0ZWQgYW5kIGRvZXMgbm90IGdldCBsaXN0ZWQgaGVyZS5cclxuICAgIC8vICAgICBdLFxyXG4gICAgLy9cclxuICAgIC8vICAgICAvLyBSZXF1aXJlZC4gQ2FuIGJlIGFueSBkYXRhIHR5cGUuIFRoZSB2YWx1ZSBzcGVjaWZpZWQgaGVyZSBzaG91bGQgbWF0Y2hcclxuICAgIC8vICAgICAvLyB0aGUgZGF0YSBiZWluZyBzdG9yZWQgKGllOiBpZiBhIGJvb2xlYW4gaXMgdXNlZCwgdGhlIHNldHRpbmcgc2hvdWxkXHJcbiAgICAvLyAgICAgLy8gcmVwcmVzZW50IGEgYm9vbGVhbikuXHJcbiAgICAvLyAgICAgZGVmYXVsdDoge1xyXG4gICAgLy8gICAgICAgICB5b3VyOiBcInZhbHVlXCIsXHJcbiAgICAvLyAgICAgfSxcclxuICAgIC8vXHJcbiAgICAvLyAgICAgLy8gT3B0aW9uYWwgc2V0dGluZ3MgY29udHJvbGxlci4gU2VlIFNldHRpbmdzQ29udHJvbGxlciBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cclxuICAgIC8vICAgICBjb250cm9sbGVyOiBuZXcgTXlTZXR0aW5nQ29udHJvbGxlcigpLFxyXG4gICAgLy9cclxuICAgIC8vICAgICAvLyBPcHRpb25hbCBmbGFnIHRvIG1ha2Ugc3VwcG9ydGVkTGV2ZWxzIGJlIHJlc3BlY3RlZCBhcyB0aGUgb3JkZXIgdG8gaGFuZGxlXHJcbiAgICAvLyAgICAgLy8gc2V0dGluZ3MuIFRoZSBmaXJzdCBlbGVtZW50IGlzIHRyZWF0ZWQgYXMgXCJtb3N0IHByZWZlcnJlZFwiLiBUaGUgXCJkZWZhdWx0XCJcclxuICAgIC8vICAgICAvLyBsZXZlbCBpcyBhbHdheXMgYXBwZW5kZWQgdG8gdGhlIGVuZC5cclxuICAgIC8vICAgICBzdXBwb3J0ZWRMZXZlbHNBcmVPcmRlcmVkOiBmYWxzZSxcclxuICAgIC8vXHJcbiAgICAvLyAgICAgLy8gT3B0aW9uYWwgdmFsdWUgdG8gaW52ZXJ0IGEgYm9vbGVhbiBzZXR0aW5nJ3MgdmFsdWUuIFRoZSBzdHJpbmcgZ2l2ZW4gd2lsbFxyXG4gICAgLy8gICAgIC8vIGJlIHJlYWQgYXMgdGhlIHNldHRpbmcncyBJRCBpbnN0ZWFkIG9mIHRoZSBvbmUgcHJvdmlkZWQgYXMgdGhlIGtleSBmb3IgdGhlXHJcbiAgICAvLyAgICAgLy8gc2V0dGluZyBkZWZpbml0aW9uLiBCeSBzZXR0aW5nIHRoaXMsIHRoZSByZXR1cm5lZCB2YWx1ZSB3aWxsIGF1dG9tYXRpY2FsbHlcclxuICAgIC8vICAgICAvLyBiZSBpbnZlcnRlZCwgZXhjZXB0IGZvciB3aGVuIHRoZSBkZWZhdWx0IHZhbHVlIGlzIHJldHVybmVkLiBJbnZlcnNpb24gd2lsbFxyXG4gICAgLy8gICAgIC8vIG9jY3VyIGFmdGVyIHRoZSBjb250cm9sbGVyIGlzIGFza2VkIGZvciBhbiBvdmVycmlkZS4gVGhpcyBzaG91bGQgYmUgdXNlZCBieVxyXG4gICAgLy8gICAgIC8vIGhpc3RvcmljYWwgc2V0dGluZ3Mgd2hpY2ggd2UgZG9uJ3Qgd2FudCBleGlzdGluZyB1c2VyJ3MgdmFsdWVzIGJlIHdpcGVkLiBEb1xyXG4gICAgLy8gICAgIC8vIG5vdCB1c2UgdGhpcyBmb3IgbmV3IHNldHRpbmdzLlxyXG4gICAgLy8gICAgIGludmVydGVkU2V0dGluZ05hbWU6IFwibXktbmVnYXRpdmUtc2V0dGluZ1wiLFxyXG4gICAgLy8gfSxcclxuICAgIFwiZmVhdHVyZV9waW5uaW5nXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIk1lc3NhZ2UgUGlubmluZ1wiKSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwiZmVhdHVyZV9jdXN0b21fc3RhdHVzXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIkN1c3RvbSB1c2VyIHN0YXR1cyBtZXNzYWdlc1wiKSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6IG5ldyBDdXN0b21TdGF0dXNDb250cm9sbGVyKCksXHJcbiAgICB9LFxyXG4gICAgXCJmZWF0dXJlX2N1c3RvbV90YWdzXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIkdyb3VwICYgZmlsdGVyIHJvb21zIGJ5IGN1c3RvbSB0YWdzIChyZWZyZXNoIHRvIGFwcGx5IGNoYW5nZXMpXCIpLFxyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0ZFQVRVUkUsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJmZWF0dXJlX3N0YXRlX2NvdW50ZXJzXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIlJlbmRlciBzaW1wbGUgY291bnRlcnMgaW4gcm9vbSBoZWFkZXJcIiksXHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfRkVBVFVSRSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcImZlYXR1cmVfbWFueV9pbnRlZ3JhdGlvbl9tYW5hZ2Vyc1wiOiB7XHJcbiAgICAgICAgaXNGZWF0dXJlOiB0cnVlLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoXCJNdWx0aXBsZSBpbnRlZ3JhdGlvbiBtYW5hZ2Vyc1wiKSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwiZmVhdHVyZV9tam9sbmlyXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIlRyeSBvdXQgbmV3IHdheXMgdG8gaWdub3JlIHBlb3BsZSAoZXhwZXJpbWVudGFsKVwiKSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwiZmVhdHVyZV9wcmVzZW5jZV9pbl9yb29tX2xpc3RcIjoge1xyXG4gICAgICAgIGlzRmVhdHVyZTogdHJ1ZSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2hvdyBhIHByZXNlbmNlIGRvdCBuZXh0IHRvIERNcyBpbiB0aGUgcm9vbSBsaXN0XCIpLFxyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0ZFQVRVUkUsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJmZWF0dXJlX2N1c3RvbV90aGVtZXNcIjoge1xyXG4gICAgICAgIGlzRmVhdHVyZTogdHJ1ZSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU3VwcG9ydCBhZGRpbmcgY3VzdG9tIHRoZW1lc1wiKSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwibWpvbG5pclJvb21zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IFsnYWNjb3VudCddLFxyXG4gICAgICAgIGRlZmF1bHQ6IFtdLFxyXG4gICAgfSxcclxuICAgIFwibWpvbG5pclBlcnNvbmFsUm9vbVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBbJ2FjY291bnQnXSxcclxuICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgfSxcclxuICAgIFwiZmVhdHVyZV9jcm9zc19zaWduaW5nXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIkVuYWJsZSBjcm9zcy1zaWduaW5nIHRvIHZlcmlmeSBwZXItdXNlciBpbnN0ZWFkIG9mIHBlci1zZXNzaW9uIChpbiBkZXZlbG9wbWVudClcIiksXHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfRkVBVFVSRSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcImZlYXR1cmVfZXZlbnRfaW5kZXhpbmdcIjoge1xyXG4gICAgICAgIGlzRmVhdHVyZTogdHJ1ZSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoXCJFbmFibGUgbG9jYWwgZXZlbnQgaW5kZXhpbmcgYW5kIEUyRUUgc2VhcmNoIChyZXF1aXJlcyByZXN0YXJ0KVwiKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcImZlYXR1cmVfYnJpZGdlX3N0YXRlXCI6IHtcclxuICAgICAgICBpc0ZlYXR1cmU6IHRydWUsXHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfRkVBVFVSRSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2hvdyBpbmZvIGFib3V0IGJyaWRnZXMgaW4gcm9vbSBzZXR0aW5nc1wiKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcImZlYXR1cmVfaW52aXRlX29ubHlfcGFkbG9ja3NcIjoge1xyXG4gICAgICAgIGlzRmVhdHVyZTogdHJ1ZSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19GRUFUVVJFLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoXCJTaG93IHBhZGxvY2tzIG9uIGludml0ZSBvbmx5IHJvb21zXCIpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJNZXNzYWdlQ29tcG9zZXJJbnB1dC5zdWdnZXN0RW1vamlcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnRW5hYmxlIEVtb2ppIHN1Z2dlc3Rpb25zIHdoaWxlIHR5cGluZycpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgaW52ZXJ0ZWRTZXR0aW5nTmFtZTogJ01lc3NhZ2VDb21wb3NlcklucHV0LmRvbnRTdWdnZXN0RW1vamknLFxyXG4gICAgfSxcclxuICAgIFwidXNlQ29tcGFjdExheW91dFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdVc2UgY29tcGFjdCB0aW1lbGluZSBsYXlvdXQnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcInNob3dSZWRhY3Rpb25zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ST09NX1NFVFRJTkdTX1dJVEhfUk9PTSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdTaG93IGEgcGxhY2Vob2xkZXIgZm9yIHJlbW92ZWQgbWVzc2FnZXMnKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdoaWRlUmVkYWN0aW9ucycsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93Sm9pbkxlYXZlc1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfUk9PTV9TRVRUSU5HU19XSVRIX1JPT00sXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnU2hvdyBqb2luL2xlYXZlIG1lc3NhZ2VzIChpbnZpdGVzL2tpY2tzL2JhbnMgdW5hZmZlY3RlZCknKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdoaWRlSm9pbkxlYXZlcycsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93QXZhdGFyQ2hhbmdlc1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfUk9PTV9TRVRUSU5HU19XSVRIX1JPT00sXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnU2hvdyBhdmF0YXIgY2hhbmdlcycpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgaW52ZXJ0ZWRTZXR0aW5nTmFtZTogJ2hpZGVBdmF0YXJDaGFuZ2VzJyxcclxuICAgIH0sXHJcbiAgICBcInNob3dEaXNwbGF5bmFtZUNoYW5nZXNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX1JPT01fU0VUVElOR1NfV0lUSF9ST09NLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ1Nob3cgZGlzcGxheSBuYW1lIGNoYW5nZXMnKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdoaWRlRGlzcGxheW5hbWVDaGFuZ2VzJyxcclxuICAgIH0sXHJcbiAgICBcInNob3dSZWFkUmVjZWlwdHNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX1JPT01fU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnU2hvdyByZWFkIHJlY2VpcHRzIHNlbnQgYnkgb3RoZXIgdXNlcnMnKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdoaWRlUmVhZFJlY2VpcHRzJyxcclxuICAgIH0sXHJcbiAgICBcInNob3dUd2VsdmVIb3VyVGltZXN0YW1wc1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdTaG93IHRpbWVzdGFtcHMgaW4gMTIgaG91ciBmb3JtYXQgKGUuZy4gMjozMHBtKScpLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwiYWx3YXlzU2hvd1RpbWVzdGFtcHNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnQWx3YXlzIHNob3cgbWVzc2FnZSB0aW1lc3RhbXBzJyksXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJhdXRvcGxheUdpZnNBbmRWaWRlb3NcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnQXV0b3BsYXkgR0lGcyBhbmQgdmlkZW9zJyksXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJhbHdheXNTaG93RW5jcnlwdGlvbkljb25zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ0Fsd2F5cyBzaG93IGVuY3J5cHRpb24gaWNvbnMnKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgfSxcclxuICAgIFwic2hvd1Jvb21SZWNvdmVyeVJlbWluZGVyXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ1Nob3cgYSByZW1pbmRlciB0byBlbmFibGUgU2VjdXJlIE1lc3NhZ2UgUmVjb3ZlcnkgaW4gZW5jcnlwdGVkIHJvb21zJyksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcImVuYWJsZVN5bnRheEhpZ2hsaWdodExhbmd1YWdlRGV0ZWN0aW9uXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ0VuYWJsZSBhdXRvbWF0aWMgbGFuZ3VhZ2UgZGV0ZWN0aW9uIGZvciBzeW50YXggaGlnaGxpZ2h0aW5nJyksXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJQaWxsLnNob3VsZFNob3dQaWxsQXZhdGFyXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ1Nob3cgYXZhdGFycyBpbiB1c2VyIGFuZCByb29tIG1lbnRpb25zJyksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBpbnZlcnRlZFNldHRpbmdOYW1lOiAnUGlsbC5zaG91bGRIaWRlUGlsbEF2YXRhcicsXHJcbiAgICB9LFxyXG4gICAgXCJUZXh0dWFsQm9keS5lbmFibGVCaWdFbW9qaVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdFbmFibGUgYmlnIGVtb2ppIGluIGNoYXQnKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdUZXh0dWFsQm9keS5kaXNhYmxlQmlnRW1vamknLFxyXG4gICAgfSxcclxuICAgIFwiTWVzc2FnZUNvbXBvc2VySW5wdXQuaXNSaWNoVGV4dEVuYWJsZWRcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJNZXNzYWdlQ29tcG9zZXIuc2hvd0Zvcm1hdHRpbmdcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJzZW5kVHlwaW5nTm90aWZpY2F0aW9uc1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2VuZCB0eXBpbmcgbm90aWZpY2F0aW9uc1wiKSxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGludmVydGVkU2V0dGluZ05hbWU6ICdkb250U2VuZFR5cGluZ05vdGlmaWNhdGlvbnMnLFxyXG4gICAgfSxcclxuICAgIFwic2hvd1R5cGluZ05vdGlmaWNhdGlvbnNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIlNob3cgdHlwaW5nIG5vdGlmaWNhdGlvbnNcIiksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcIk1lc3NhZ2VDb21wb3NlcklucHV0LmF1dG9SZXBsYWNlRW1vamlcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnQXV0b21hdGljYWxseSByZXBsYWNlIHBsYWluIHRleHQgRW1vamknKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcIlZpZGVvVmlldy5mbGlwVmlkZW9Ib3Jpem9udGFsbHlcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnTWlycm9yIGxvY2FsIHZpZGVvIGZlZWQnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcIlRhZ1BhbmVsLmVuYWJsZVRhZ1BhbmVsXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ0VuYWJsZSBDb21tdW5pdHkgRmlsdGVyIFBhbmVsJyksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBpbnZlcnRlZFNldHRpbmdOYW1lOiAnVGFnUGFuZWwuZGlzYWJsZVRhZ1BhbmVsJyxcclxuICAgIH0sXHJcbiAgICBcInRoZW1lXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IFwibGlnaHRcIixcclxuICAgICAgICBjb250cm9sbGVyOiBuZXcgVGhlbWVDb250cm9sbGVyKCksXHJcbiAgICB9LFxyXG4gICAgXCJjdXN0b21fdGhlbWVzXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IFtdLFxyXG4gICAgfSxcclxuICAgIFwidXNlX3N5c3RlbV90aGVtZVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiTWF0Y2ggc3lzdGVtIHRoZW1lXCIpLFxyXG4gICAgfSxcclxuICAgIFwid2ViUnRjQWxsb3dQZWVyVG9QZWVyXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdBbGxvdyBQZWVyLXRvLVBlZXIgZm9yIDE6MSBjYWxscycpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgaW52ZXJ0ZWRTZXR0aW5nTmFtZTogJ3dlYlJ0Y0ZvcmNlVFVSTicsXHJcbiAgICB9LFxyXG4gICAgXCJ3ZWJydGNfYXVkaW9vdXRwdXRcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICB9LFxyXG4gICAgXCJ3ZWJydGNfYXVkaW9pbnB1dFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogbnVsbCxcclxuICAgIH0sXHJcbiAgICBcIndlYnJ0Y192aWRlb2lucHV0XCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyxcclxuICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgfSxcclxuICAgIFwibGFuZ3VhZ2VcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTX1dJVEhfQ09ORklHLFxyXG4gICAgICAgIGRlZmF1bHQ6IFwiZW5cIixcclxuICAgIH0sXHJcbiAgICBcImJyZWFkY3J1bWJfcm9vbXNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogWydhY2NvdW50J10sXHJcbiAgICAgICAgZGVmYXVsdDogW10sXHJcbiAgICB9LFxyXG4gICAgXCJyb29tX2RpcmVjdG9yeV9zZXJ2ZXJzXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IFsnYWNjb3VudCddLFxyXG4gICAgICAgIGRlZmF1bHQ6IFtdLFxyXG4gICAgfSxcclxuICAgIFwiaW50ZWdyYXRpb25Qcm92aXNpb25pbmdcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogWydhY2NvdW50J10sXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcImFsbG93ZWRXaWRnZXRzXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IFsncm9vbS1hY2NvdW50J10sXHJcbiAgICAgICAgZGVmYXVsdDoge30sIC8vIG5vbmUgYWxsb3dlZFxyXG4gICAgfSxcclxuICAgIFwiYW5hbHl0aWNzT3B0SW5cIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTX1dJVEhfQ09ORklHLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ1NlbmQgYW5hbHl0aWNzIGRhdGEnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcInNob3dDb29raWVCYXJcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTX1dJVEhfQ09ORklHLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJhdXRvY29tcGxldGVEZWxheVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1NfV0lUSF9DT05GSUcsXHJcbiAgICAgICAgZGVmYXVsdDogMjAwLFxyXG4gICAgfSxcclxuICAgIFwicmVhZE1hcmtlckluVmlld1RocmVzaG9sZE1zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyxcclxuICAgICAgICBkZWZhdWx0OiAzMDAwLFxyXG4gICAgfSxcclxuICAgIFwicmVhZE1hcmtlck91dE9mVmlld1RocmVzaG9sZE1zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyxcclxuICAgICAgICBkZWZhdWx0OiAzMDAwMCxcclxuICAgIH0sXHJcbiAgICBcImJsYWNrbGlzdFVudmVyaWZpZWREZXZpY2VzXCI6IHtcclxuICAgICAgICAvLyBXZSBzcGVjaWZpY2FsbHkgd2FudCB0byBoYXZlIHJvb20tZGV2aWNlID4gZGV2aWNlIHNvIHRoYXQgdXNlcnMgbWF5IHNldCBhIGRldmljZSBkZWZhdWx0XHJcbiAgICAgICAgLy8gd2l0aCBhIHBlci1yb29tIG92ZXJyaWRlLlxyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogWydyb29tLWRldmljZScsICdkZXZpY2UnXSxcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHNBcmVPcmRlcmVkOiB0cnVlLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiB7XHJcbiAgICAgICAgICAgIFwiZGVmYXVsdFwiOiBfdGQoJ05ldmVyIHNlbmQgZW5jcnlwdGVkIG1lc3NhZ2VzIHRvIHVudmVyaWZpZWQgc2Vzc2lvbnMgZnJvbSB0aGlzIHNlc3Npb24nKSxcclxuICAgICAgICAgICAgXCJyb29tLWRldmljZVwiOiBfdGQoJ05ldmVyIHNlbmQgZW5jcnlwdGVkIG1lc3NhZ2VzIHRvIHVudmVyaWZpZWQgc2Vzc2lvbnMgaW4gdGhpcyByb29tIGZyb20gdGhpcyBzZXNzaW9uJyksXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcInVybFByZXZpZXdzRW5hYmxlZFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfUk9PTV9TRVRUSU5HU19XSVRIX1JPT00sXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IHtcclxuICAgICAgICAgICAgXCJkZWZhdWx0XCI6IF90ZCgnRW5hYmxlIGlubGluZSBVUkwgcHJldmlld3MgYnkgZGVmYXVsdCcpLFxyXG4gICAgICAgICAgICBcInJvb20tYWNjb3VudFwiOiBfdGQoXCJFbmFibGUgVVJMIHByZXZpZXdzIGZvciB0aGlzIHJvb20gKG9ubHkgYWZmZWN0cyB5b3UpXCIpLFxyXG4gICAgICAgICAgICBcInJvb21cIjogX3RkKFwiRW5hYmxlIFVSTCBwcmV2aWV3cyBieSBkZWZhdWx0IGZvciBwYXJ0aWNpcGFudHMgaW4gdGhpcyByb29tXCIpLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcInVybFByZXZpZXdzRW5hYmxlZF9lMmVlXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IFsncm9vbS1kZXZpY2UnLCAncm9vbS1hY2NvdW50J10sXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IHtcclxuICAgICAgICAgICAgXCJyb29tLWFjY291bnRcIjogX3RkKFwiRW5hYmxlIFVSTCBwcmV2aWV3cyBmb3IgdGhpcyByb29tIChvbmx5IGFmZmVjdHMgeW91KVwiKSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwicm9vbUNvbG9yXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ST09NX1NFVFRJTkdTX1dJVEhfUk9PTSxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiUm9vbSBDb2xvdXJcIiksXHJcbiAgICAgICAgZGVmYXVsdDoge1xyXG4gICAgICAgICAgICBwcmltYXJ5X2NvbG9yOiBudWxsLCAvLyBIZXggc3RyaW5nLCBlZzogIzAwMDAwMFxyXG4gICAgICAgICAgICBzZWNvbmRhcnlfY29sb3I6IG51bGwsIC8vIEhleCBzdHJpbmcsIGVnOiAjMDAwMDAwXHJcbiAgICAgICAgfSxcclxuICAgIH0sXHJcbiAgICBcIm5vdGlmaWNhdGlvbnNFbmFibGVkXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgICAgICBjb250cm9sbGVyOiBuZXcgTm90aWZpY2F0aW9uc0VuYWJsZWRDb250cm9sbGVyKCksXHJcbiAgICB9LFxyXG4gICAgXCJub3RpZmljYXRpb25Tb3VuZFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfUk9PTV9PUl9BQ0NPVU5ULFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwibm90aWZpY2F0aW9uQm9keUVuYWJsZWRcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgY29udHJvbGxlcjogbmV3IE5vdGlmaWNhdGlvbkJvZHlFbmFibGVkQ29udHJvbGxlcigpLFxyXG4gICAgfSxcclxuICAgIFwiYXVkaW9Ob3RpZmljYXRpb25zRW5hYmxlZFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBjb250cm9sbGVyOiBuZXcgQXVkaW9Ob3RpZmljYXRpb25zRW5hYmxlZENvbnRyb2xsZXIoKSxcclxuICAgIH0sXHJcbiAgICBcImVuYWJsZVdpZGdldFNjcmVlbnNob3RzXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ0VuYWJsZSB3aWRnZXQgc2NyZWVuc2hvdHMgb24gc3VwcG9ydGVkIHdpZGdldHMnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcIlBpbm5lZEV2ZW50cy5pc09wZW5cIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogWydyb29tLWRldmljZSddLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwicHJvbXB0QmVmb3JlSW52aXRlVW5rbm93blVzZXJzXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19BQ0NPVU5UX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoJ1Byb21wdCBiZWZvcmUgc2VuZGluZyBpbnZpdGVzIHRvIHBvdGVudGlhbGx5IGludmFsaWQgbWF0cml4IElEcycpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93RGV2ZWxvcGVyVG9vbHNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZCgnU2hvdyBkZXZlbG9wZXIgdG9vbHMnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICBcIndpZGdldE9wZW5JRFBlcm1pc3Npb25zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyxcclxuICAgICAgICBkZWZhdWx0OiB7XHJcbiAgICAgICAgICAgIGFsbG93OiBbXSxcclxuICAgICAgICAgICAgZGVueTogW10sXHJcbiAgICAgICAgfSxcclxuICAgIH0sXHJcbiAgICBcIlJvb21MaXN0Lm9yZGVyQWxwaGFiZXRpY2FsbHlcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIk9yZGVyIHJvb21zIGJ5IG5hbWVcIiksXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJSb29tTGlzdC5vcmRlckJ5SW1wb3J0YW5jZVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2hvdyByb29tcyB3aXRoIHVucmVhZCBub3RpZmljYXRpb25zIGZpcnN0XCIpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJicmVhZGNydW1ic1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfQUNDT1VOVF9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2hvdyBzaG9ydGN1dHMgdG8gcmVjZW50bHkgdmlld2VkIHJvb21zIGFib3ZlIHRoZSByb29tIGxpc3RcIiksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcInNob3dIaWRkZW5FdmVudHNJblRpbWVsaW5lXCI6IHtcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiU2hvdyBoaWRkZW4gZXZlbnRzIGluIHRpbWVsaW5lXCIpLFxyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwibG93QmFuZHdpZHRoXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKCdMb3cgYmFuZHdpZHRoIG1vZGUnKSxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgICAgICBjb250cm9sbGVyOiBuZXcgUmVsb2FkT25DaGFuZ2VDb250cm9sbGVyKCksXHJcbiAgICB9LFxyXG4gICAgXCJmYWxsYmFja0lDRVNlcnZlckFsbG93ZWRcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoXHJcbiAgICAgICAgICAgIFwiQWxsb3cgZmFsbGJhY2sgY2FsbCBhc3Npc3Qgc2VydmVyIHR1cm4ubWF0cml4Lm9yZyB3aGVuIHlvdXIgaG9tZXNlcnZlciBcIiArXHJcbiAgICAgICAgICAgIFwiZG9lcyBub3Qgb2ZmZXIgb25lICh5b3VyIElQIGFkZHJlc3Mgd291bGQgYmUgc2hhcmVkIGR1cmluZyBhIGNhbGwpXCIsXHJcbiAgICAgICAgKSxcclxuICAgICAgICAvLyBUaGlzIGlzIGEgdHJpLXN0YXRlIHZhbHVlLCB3aGVyZSBgbnVsbGAgbWVhbnMgXCJwcm9tcHQgdGhlIHVzZXJcIi5cclxuICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgfSxcclxuICAgIFwic2VuZFJlYWRSZWNlaXB0c1wiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfUk9PTV9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFxyXG4gICAgICAgICAgICBcIlNlbmQgcmVhZCByZWNlaXB0cyBmb3IgbWVzc2FnZXMgKHJlcXVpcmVzIGNvbXBhdGlibGUgaG9tZXNlcnZlciB0byBkaXNhYmxlKVwiLFxyXG4gICAgICAgICksXHJcbiAgICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBcInNob3dJbWFnZXNcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0FDQ09VTlRfU0VUVElOR1MsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIlNob3cgcHJldmlld3MvdGh1bWJuYWlscyBmb3IgaW1hZ2VzXCIpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93UmlnaHRQYW5lbEluUm9vbVwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93UmlnaHRQYW5lbEluR3JvdXBcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIFwibGFzdFJpZ2h0UGFuZWxQaGFzZUZvclJvb21cIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRlZmF1bHQ6IFJJR0hUX1BBTkVMX1BIQVNFUy5Sb29tTWVtYmVySW5mbyxcclxuICAgIH0sXHJcbiAgICBcImxhc3RSaWdodFBhbmVsUGhhc2VGb3JHcm91cFwiOiB7XHJcbiAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBMRVZFTFNfREVWSUNFX09OTFlfU0VUVElOR1MsXHJcbiAgICAgICAgZGVmYXVsdDogUklHSFRfUEFORUxfUEhBU0VTLkdyb3VwTWVtYmVyTGlzdCxcclxuICAgIH0sXHJcbiAgICBcImVuYWJsZUV2ZW50SW5kZXhpbmdcIjoge1xyXG4gICAgICAgIHN1cHBvcnRlZExldmVsczogTEVWRUxTX0RFVklDRV9PTkxZX1NFVFRJTkdTLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiBfdGQoXCJFbmFibGUgbWVzc2FnZSBzZWFyY2ggaW4gZW5jcnlwdGVkIHJvb21zXCIpLFxyXG4gICAgICAgIGRlZmF1bHQ6IHRydWUsXHJcbiAgICB9LFxyXG4gICAgXCJrZWVwU2VjcmV0U3RvcmFnZVBhc3NwaHJhc2VGb3JTZXNzaW9uXCI6IHtcclxuICAgICAgICAgc3VwcG9ydGVkTGV2ZWxzOiBbJ2RldmljZScsICdjb25maWcnXSxcclxuICAgICAgICAgZGlzcGxheU5hbWU6IF90ZChcIktlZXAgc2VjcmV0IHN0b3JhZ2UgcGFzc3BocmFzZSBpbiBtZW1vcnkgZm9yIHRoaXMgc2Vzc2lvblwiKSxcclxuICAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgXCJjcmF3bGVyU2xlZXBUaW1lXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiSG93IGZhc3Qgc2hvdWxkIG1lc3NhZ2VzIGJlIGRvd25sb2FkZWQuXCIpLFxyXG4gICAgICAgIGRlZmF1bHQ6IDMwMDAsXHJcbiAgICB9LFxyXG4gICAgXCJzaG93Q2FsbEJ1dHRvbnNJbkNvbXBvc2VyXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HU19XSVRIX0NPTkZJRyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgfSxcclxuICAgIFwiZTJlZS5tYW51YWxseVZlcmlmeUFsbFNlc3Npb25zXCI6IHtcclxuICAgICAgICBzdXBwb3J0ZWRMZXZlbHM6IExFVkVMU19ERVZJQ0VfT05MWV9TRVRUSU5HUyxcclxuICAgICAgICBkaXNwbGF5TmFtZTogX3RkKFwiTWFudWFsbHkgdmVyaWZ5IGFsbCByZW1vdGUgc2Vzc2lvbnNcIiksXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICAgICAgY29udHJvbGxlcjogbmV3IFB1c2hUb01hdHJpeENsaWVudENvbnRyb2xsZXIoXHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudC5wcm90b3R5cGUuc2V0Q3J5cHRvVHJ1c3RDcm9zc1NpZ25lZERldmljZXMsIHRydWUsXHJcbiAgICAgICAgKSxcclxuICAgIH0sXHJcbn07XHJcbiJdfQ==