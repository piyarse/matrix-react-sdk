"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SettingLevel = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _DeviceSettingsHandler = _interopRequireDefault(require("./handlers/DeviceSettingsHandler"));

var _RoomDeviceSettingsHandler = _interopRequireDefault(require("./handlers/RoomDeviceSettingsHandler"));

var _DefaultSettingsHandler = _interopRequireDefault(require("./handlers/DefaultSettingsHandler"));

var _RoomAccountSettingsHandler = _interopRequireDefault(require("./handlers/RoomAccountSettingsHandler"));

var _AccountSettingsHandler = _interopRequireDefault(require("./handlers/AccountSettingsHandler"));

var _RoomSettingsHandler = _interopRequireDefault(require("./handlers/RoomSettingsHandler"));

var _ConfigSettingsHandler = _interopRequireDefault(require("./handlers/ConfigSettingsHandler"));

var _languageHandler = require("../languageHandler");

var _SdkConfig = _interopRequireDefault(require("../SdkConfig"));

var _dispatcher = _interopRequireDefault(require("../dispatcher"));

var _Settings = require("./Settings");

var _LocalEchoWrapper = _interopRequireDefault(require("./handlers/LocalEchoWrapper"));

var _WatchManager = require("./WatchManager");

/*
Copyright 2017 Travis Ralston
Copyright 2019 New Vector Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Represents the various setting levels supported by the SettingsStore.
 */
const SettingLevel = {
  // Note: This enum is not used in this class or in the Settings file
  // This should always be used elsewhere in the project.
  DEVICE: "device",
  ROOM_DEVICE: "room-device",
  ROOM_ACCOUNT: "room-account",
  ACCOUNT: "account",
  ROOM: "room",
  CONFIG: "config",
  DEFAULT: "default"
};
exports.SettingLevel = SettingLevel;
const defaultWatchManager = new _WatchManager.WatchManager(); // Convert the settings to easier to manage objects for the handlers

const defaultSettings = {};
const invertedDefaultSettings = {};
const featureNames = [];

for (const key of Object.keys(_Settings.SETTINGS)) {
  defaultSettings[key] = _Settings.SETTINGS[key].default;
  if (_Settings.SETTINGS[key].isFeature) featureNames.push(key);

  if (_Settings.SETTINGS[key].invertedSettingName) {
    // Invert now so that the rest of the system will invert it back
    // to what was intended.
    invertedDefaultSettings[key] = !_Settings.SETTINGS[key].default;
  }
}

const LEVEL_HANDLERS = {
  "device": new _DeviceSettingsHandler.default(featureNames, defaultWatchManager),
  "room-device": new _RoomDeviceSettingsHandler.default(defaultWatchManager),
  "room-account": new _RoomAccountSettingsHandler.default(defaultWatchManager),
  "account": new _AccountSettingsHandler.default(defaultWatchManager),
  "room": new _RoomSettingsHandler.default(defaultWatchManager),
  "config": new _ConfigSettingsHandler.default(),
  "default": new _DefaultSettingsHandler.default(defaultSettings, invertedDefaultSettings)
}; // Wrap all the handlers with local echo

for (const key of Object.keys(LEVEL_HANDLERS)) {
  LEVEL_HANDLERS[key] = new _LocalEchoWrapper.default(LEVEL_HANDLERS[key]);
}

const LEVEL_ORDER = ['device', 'room-device', 'room-account', 'account', 'room', 'config', 'default'];
/**
 * Controls and manages application settings by providing varying levels at which the
 * setting value may be specified. The levels are then used to determine what the setting
 * value should be given a set of circumstances. The levels, in priority order, are:
 * - "device"         - Values are determined by the current device
 * - "room-device"    - Values are determined by the current device for a particular room
 * - "room-account"   - Values are determined by the current account for a particular room
 * - "account"        - Values are determined by the current account
 * - "room"           - Values are determined by a particular room (by the room admins)
 * - "config"         - Values are determined by the config.json
 * - "default"        - Values are determined by the hardcoded defaults
 *
 * Each level has a different method to storing the setting value. For implementation
 * specific details, please see the handlers. The "config" and "default" levels are
 * both always supported on all platforms. All other settings should be guarded by
 * isLevelSupported() prior to attempting to set the value.
 *
 * Settings can also represent features. Features are significant portions of the
 * application that warrant a dedicated setting to toggle them on or off. Features are
 * special-cased to ensure that their values respect the configuration (for example, a
 * feature may be reported as disabled even though a user has specifically requested it
 * be enabled).
 */

class SettingsStore {
  // We support watching settings for changes, and do this by tracking which callbacks have
  // been given to us. We end up returning the callbackRef to the caller so they can unsubscribe
  // at a later point.
  //
  // We also maintain a list of monitors which are special watchers: they cause dispatches
  // when the setting changes. We track which rooms we're monitoring though to ensure we
  // don't duplicate updates on the bus.
  // { callbackRef => { callbackFn } }
  // { settingName => { roomId => callbackRef } }
  // Counter used for generation of watcher IDs

  /**
   * Watches for changes in a particular setting. This is done without any local echo
   * wrapping and fires whenever a change is detected in a setting's value, at any level.
   * Watching is intended to be used in scenarios where the app needs to react to changes
   * made by other devices. It is otherwise expected that callers will be able to use the
   * Controller system or track their own changes to settings. Callers should retain the
   * returned reference to later unsubscribe from updates.
   * @param {string} settingName The setting name to watch
   * @param {String} roomId The room ID to watch for changes in. May be null for 'all'.
   * @param {function} callbackFn A function to be called when a setting change is
   * detected. Five arguments can be expected: the setting name, the room ID (may be null),
   * the level the change happened at, the new value at the given level, and finally the new
   * value for the setting regardless of level. The callback is responsible for determining
   * if the change in value is worthwhile enough to react upon.
   * @returns {string} A reference to the watcher that was employed.
   */
  static watchSetting(settingName, roomId, callbackFn) {
    const setting = _Settings.SETTINGS[settingName];
    const originalSettingName = settingName;
    if (!setting) throw new Error("".concat(settingName, " is not a setting"));

    if (setting.invertedSettingName) {
      settingName = setting.invertedSettingName;
    }

    const watcherId = "".concat(new Date().getTime(), "_").concat(SettingsStore._watcherCount++, "_").concat(settingName, "_").concat(roomId);

    const localizedCallback = (changedInRoomId, atLevel, newValAtLevel) => {
      const newValue = SettingsStore.getValue(originalSettingName);
      callbackFn(originalSettingName, changedInRoomId, atLevel, newValAtLevel, newValue);
    };

    console.log("Starting watcher for ".concat(settingName, "@").concat(roomId || '<null room>', " as ID ").concat(watcherId));
    SettingsStore._watchers[watcherId] = localizedCallback;
    defaultWatchManager.watchSetting(settingName, roomId, localizedCallback);
    return watcherId;
  }
  /**
   * Stops the SettingsStore from watching a setting. This is a no-op if the watcher
   * provided is not found.
   * @param {string} watcherReference The watcher reference (received from #watchSetting)
   * to cancel.
   */


  static unwatchSetting(watcherReference) {
    if (!SettingsStore._watchers[watcherReference]) {
      console.warn("Ending non-existent watcher ID ".concat(watcherReference));
      return;
    }

    console.log("Ending watcher ID ".concat(watcherReference));
    defaultWatchManager.unwatchSetting(SettingsStore._watchers[watcherReference]);
    delete SettingsStore._watchers[watcherReference];
  }
  /**
   * Sets up a monitor for a setting. This behaves similar to #watchSetting except instead
   * of making a call to a callback, it forwards all changes to the dispatcher. Callers can
   * expect to listen for the 'setting_updated' action with an object containing settingName,
   * roomId, level, newValueAtLevel, and newValue.
   * @param {string} settingName The setting name to monitor.
   * @param {String} roomId The room ID to monitor for changes in. Use null for all rooms.
   */


  static monitorSetting(settingName, roomId) {
    if (!this._monitors[settingName]) this._monitors[settingName] = {};

    const registerWatcher = () => {
      this._monitors[settingName][roomId] = SettingsStore.watchSetting(settingName, roomId, (settingName, inRoomId, level, newValueAtLevel, newValue) => {
        _dispatcher.default.dispatch({
          action: 'setting_updated',
          settingName,
          roomId: inRoomId,
          level,
          newValueAtLevel,
          newValue
        });
      });
    };

    const hasRoom = Object.keys(this._monitors[settingName]).find(r => r === roomId || r === null);

    if (!hasRoom) {
      registerWatcher();
    } else {
      if (roomId === null) {
        // Unregister all existing watchers and register the new one
        for (const roomId of Object.keys(this._monitors[settingName])) {
          SettingsStore.unwatchSetting(this._monitors[settingName][roomId]);
        }

        this._monitors[settingName] = {};
        registerWatcher();
      } // else a watcher is already registered for the room, so don't bother registering it again

    }
  }
  /**
   * Gets the translated display name for a given setting
   * @param {string} settingName The setting to look up.
   * @param {"device"|"room-device"|"room-account"|"account"|"room"|"config"|"default"} atLevel
   * The level to get the display name for; Defaults to 'default'.
   * @return {String} The display name for the setting, or null if not found.
   */


  static getDisplayName(settingName, atLevel = "default") {
    if (!_Settings.SETTINGS[settingName] || !_Settings.SETTINGS[settingName].displayName) return null;
    let displayName = _Settings.SETTINGS[settingName].displayName;

    if (displayName instanceof Object) {
      if (displayName[atLevel]) displayName = displayName[atLevel];else displayName = displayName["default"];
    }

    return (0, _languageHandler._t)(displayName);
  }
  /**
   * Returns a list of all available labs feature names
   * @returns {string[]} The list of available feature names
   */


  static getLabsFeatures() {
    const possibleFeatures = Object.keys(_Settings.SETTINGS).filter(s => SettingsStore.isFeature(s));

    const enableLabs = _SdkConfig.default.get()["enableLabs"];

    if (enableLabs) return possibleFeatures;
    return possibleFeatures.filter(s => SettingsStore._getFeatureState(s) === "labs");
  }
  /**
   * Determines if a setting is also a feature.
   * @param {string} settingName The setting to look up.
   * @return {boolean} True if the setting is a feature.
   */


  static isFeature(settingName) {
    if (!_Settings.SETTINGS[settingName]) return false;
    return _Settings.SETTINGS[settingName].isFeature;
  }
  /**
   * Determines if a given feature is enabled. The feature given must be a known
   * feature.
   * @param {string} settingName The name of the setting that is a feature.
   * @param {String} roomId The optional room ID to validate in, may be null.
   * @return {boolean} True if the feature is enabled, false otherwise
   */


  static isFeatureEnabled(settingName, roomId = null) {
    if (!SettingsStore.isFeature(settingName)) {
      throw new Error("Setting " + settingName + " is not a feature");
    }

    return SettingsStore.getValue(settingName, roomId);
  }
  /**
   * Sets a feature as enabled or disabled on the current device.
   * @param {string} settingName The name of the setting.
   * @param {boolean} value True to enable the feature, false otherwise.
   * @returns {Promise} Resolves when the setting has been set.
   */


  static setFeatureEnabled(settingName, value) {
    // Verify that the setting is actually a setting
    if (!_Settings.SETTINGS[settingName]) {
      throw new Error("Setting '" + settingName + "' does not appear to be a setting.");
    }

    if (!SettingsStore.isFeature(settingName)) {
      throw new Error("Setting " + settingName + " is not a feature");
    }

    return SettingsStore.setValue(settingName, null, "device", value);
  }
  /**
   * Gets the value of a setting. The room ID is optional if the setting is not to
   * be applied to any particular room, otherwise it should be supplied.
   * @param {string} settingName The name of the setting to read the value of.
   * @param {String} roomId The room ID to read the setting value in, may be null.
   * @param {boolean} excludeDefault True to disable using the default value.
   * @return {*} The value, or null if not found
   */


  static getValue(settingName, roomId = null, excludeDefault = false) {
    // Verify that the setting is actually a setting
    if (!_Settings.SETTINGS[settingName]) {
      throw new Error("Setting '" + settingName + "' does not appear to be a setting.");
    }

    const setting = _Settings.SETTINGS[settingName];
    const levelOrder = setting.supportedLevelsAreOrdered ? setting.supportedLevels : LEVEL_ORDER;
    return SettingsStore.getValueAt(levelOrder[0], settingName, roomId, false, excludeDefault);
  }
  /**
   * Gets a setting's value at a particular level, ignoring all levels that are more specific.
   * @param {"device"|"room-device"|"room-account"|"account"|"room"|"config"|"default"} level The
   * level to look at.
   * @param {string} settingName The name of the setting to read.
   * @param {String} roomId The room ID to read the setting value in, may be null.
   * @param {boolean} explicit If true, this method will not consider other levels, just the one
   * provided. Defaults to false.
   * @param {boolean} excludeDefault True to disable using the default value.
   * @return {*} The value, or null if not found.
   */


  static getValueAt(level, settingName, roomId = null, explicit = false, excludeDefault = false) {
    // Verify that the setting is actually a setting
    const setting = _Settings.SETTINGS[settingName];

    if (!setting) {
      throw new Error("Setting '" + settingName + "' does not appear to be a setting.");
    }

    const levelOrder = setting.supportedLevelsAreOrdered ? setting.supportedLevels : LEVEL_ORDER;
    if (!levelOrder.includes("default")) levelOrder.push("default"); // always include default

    const minIndex = levelOrder.indexOf(level);
    if (minIndex === -1) throw new Error("Level " + level + " is not prioritized");

    if (SettingsStore.isFeature(settingName)) {
      const configValue = SettingsStore._getFeatureState(settingName);

      if (configValue === "enable") return true;
      if (configValue === "disable") return false; // else let it fall through the default process
    }

    const handlers = SettingsStore._getHandlers(settingName); // Check if we need to invert the setting at all. Do this after we get the setting
    // handlers though, otherwise we'll fail to read the value.


    if (setting.invertedSettingName) {
      //console.warn(`Inverting ${settingName} to be ${setting.invertedSettingName} - legacy setting`);
      settingName = setting.invertedSettingName;
    }

    if (explicit) {
      const handler = handlers[level];

      if (!handler) {
        return SettingsStore._getFinalValue(setting, level, roomId, null, null);
      }

      const value = handler.getValue(settingName, roomId);
      return SettingsStore._getFinalValue(setting, level, roomId, value, level);
    }

    for (let i = minIndex; i < levelOrder.length; i++) {
      const handler = handlers[levelOrder[i]];
      if (!handler) continue;
      if (excludeDefault && levelOrder[i] === "default") continue;
      const value = handler.getValue(settingName, roomId);
      if (value === null || value === undefined) continue;
      return SettingsStore._getFinalValue(setting, level, roomId, value, levelOrder[i]);
    }

    return SettingsStore._getFinalValue(setting, level, roomId, null, null);
  }

  static _getFinalValue(setting, level, roomId, calculatedValue, calculatedAtLevel) {
    let resultingValue = calculatedValue;

    if (setting.controller) {
      const actualValue = setting.controller.getValueOverride(level, roomId, calculatedValue, calculatedAtLevel);
      if (actualValue !== undefined && actualValue !== null) resultingValue = actualValue;
    }

    if (setting.invertedSettingName) resultingValue = !resultingValue;
    return resultingValue;
  }
  /* eslint-disable valid-jsdoc */
  //https://github.com/eslint/eslint/issues/7307

  /**
   * Sets the value for a setting. The room ID is optional if the setting is not being
   * set for a particular room, otherwise it should be supplied. The value may be null
   * to indicate that the level should no longer have an override.
   * @param {string} settingName The name of the setting to change.
   * @param {String} roomId The room ID to change the value in, may be null.
   * @param {"device"|"room-device"|"room-account"|"account"|"room"} level The level
   * to change the value at.
   * @param {*} value The new value of the setting, may be null.
   * @return {Promise} Resolves when the setting has been changed.
   */

  /* eslint-enable valid-jsdoc */


  static async setValue(settingName, roomId, level, value) {
    // Verify that the setting is actually a setting
    const setting = _Settings.SETTINGS[settingName];

    if (!setting) {
      throw new Error("Setting '" + settingName + "' does not appear to be a setting.");
    }

    const handler = SettingsStore._getHandler(settingName, level);

    if (!handler) {
      throw new Error("Setting " + settingName + " does not have a handler for " + level);
    }

    if (setting.invertedSettingName) {
      // Note: We can't do this when the `level` is "default", however we also
      // know that the user can't possible change the default value through this
      // function so we don't bother checking it.
      //console.warn(`Inverting ${settingName} to be ${setting.invertedSettingName} - legacy setting`);
      settingName = setting.invertedSettingName;
      value = !value;
    }

    if (!handler.canSetValue(settingName, roomId)) {
      throw new Error("User cannot set " + settingName + " at " + level + " in " + roomId);
    }

    await handler.setValue(settingName, roomId, value);
    const controller = setting.controller;

    if (controller) {
      controller.onChange(level, roomId, value);
    }
  }
  /**
   * Determines if the current user is permitted to set the given setting at the given
   * level for a particular room. The room ID is optional if the setting is not being
   * set for a particular room, otherwise it should be supplied.
   * @param {string} settingName The name of the setting to check.
   * @param {String} roomId The room ID to check in, may be null.
   * @param {"device"|"room-device"|"room-account"|"account"|"room"} level The level to
   * check at.
   * @return {boolean} True if the user may set the setting, false otherwise.
   */


  static canSetValue(settingName, roomId, level) {
    // Verify that the setting is actually a setting
    if (!_Settings.SETTINGS[settingName]) {
      throw new Error("Setting '" + settingName + "' does not appear to be a setting.");
    }

    const handler = SettingsStore._getHandler(settingName, level);

    if (!handler) return false;
    return handler.canSetValue(settingName, roomId);
  }
  /**
   * Determines if the given level is supported on this device.
   * @param {"device"|"room-device"|"room-account"|"account"|"room"} level The level
   * to check the feasibility of.
   * @return {boolean} True if the level is supported, false otherwise.
   */


  static isLevelSupported(level) {
    if (!LEVEL_HANDLERS[level]) return false;
    return LEVEL_HANDLERS[level].isSupported();
  }
  /**
   * Debugging function for reading explicit setting values without going through the
   * complicated/biased functions in the SettingsStore. This will print information to
   * the console for analysis. Not intended to be used within the application.
   * @param {string} realSettingName The setting name to try and read.
   * @param {string} roomId Optional room ID to test the setting in.
   */


  static debugSetting(realSettingName, roomId) {
    console.log("--- DEBUG ".concat(realSettingName)); // Note: we intentionally use JSON.stringify here to avoid the console masking the
    // problem if there's a type representation issue. Also, this way it is guaranteed
    // to show up in a rageshake if required.

    const def = _Settings.SETTINGS[realSettingName];
    console.log("--- definition: ".concat(def ? JSON.stringify(def) : '<NOT_FOUND>'));
    console.log("--- default level order: ".concat(JSON.stringify(LEVEL_ORDER)));
    console.log("--- registered handlers: ".concat(JSON.stringify(Object.keys(LEVEL_HANDLERS))));

    const doChecks = settingName => {
      for (const handlerName of Object.keys(LEVEL_HANDLERS)) {
        const handler = LEVEL_HANDLERS[handlerName];

        try {
          const value = handler.getValue(settingName, roomId);
          console.log("---     ".concat(handlerName, "@").concat(roomId || '<no_room>', " = ").concat(JSON.stringify(value)));
        } catch (e) {
          console.log("---     ".concat(handler, "@").concat(roomId || '<no_room>', " THREW ERROR: ").concat(e.message));
          console.error(e);
        }

        if (roomId) {
          try {
            const value = handler.getValue(settingName, null);
            console.log("---     ".concat(handlerName, "@<no_room> = ").concat(JSON.stringify(value)));
          } catch (e) {
            console.log("---     ".concat(handler, "@<no_room> THREW ERROR: ").concat(e.message));
            console.error(e);
          }
        }
      }

      console.log("--- calculating as returned by SettingsStore");
      console.log("--- these might not match if the setting uses a controller - be warned!");

      try {
        const value = SettingsStore.getValue(settingName, roomId);
        console.log("---     SettingsStore#generic@".concat(roomId || '<no_room>', "  = ").concat(JSON.stringify(value)));
      } catch (e) {
        console.log("---     SettingsStore#generic@".concat(roomId || '<no_room>', " THREW ERROR: ").concat(e.message));
        console.error(e);
      }

      if (roomId) {
        try {
          const value = SettingsStore.getValue(settingName, null);
          console.log("---     SettingsStore#generic@<no_room>  = ".concat(JSON.stringify(value)));
        } catch (e) {
          console.log("---     SettingsStore#generic@$<no_room> THREW ERROR: ".concat(e.message));
          console.error(e);
        }
      }

      for (const level of LEVEL_ORDER) {
        try {
          const value = SettingsStore.getValueAt(level, settingName, roomId);
          console.log("---     SettingsStore#".concat(level, "@").concat(roomId || '<no_room>', " = ").concat(JSON.stringify(value)));
        } catch (e) {
          console.log("---     SettingsStore#".concat(level, "@").concat(roomId || '<no_room>', " THREW ERROR: ").concat(e.message));
          console.error(e);
        }

        if (roomId) {
          try {
            const value = SettingsStore.getValueAt(level, settingName, null);
            console.log("---     SettingsStore#".concat(level, "@<no_room> = ").concat(JSON.stringify(value)));
          } catch (e) {
            console.log("---     SettingsStore#".concat(level, "@$<no_room> THREW ERROR: ").concat(e.message));
            console.error(e);
          }
        }
      }
    };

    doChecks(realSettingName);

    if (def.invertedSettingName) {
      console.log("--- TESTING INVERTED SETTING NAME");
      console.log("--- inverted: ".concat(def.invertedSettingName));
      doChecks(def.invertedSettingName);
    }

    console.log("--- END DEBUG");
  }

  static _getHandler(settingName, level) {
    const handlers = SettingsStore._getHandlers(settingName);

    if (!handlers[level]) return null;
    return handlers[level];
  }

  static _getHandlers(settingName) {
    if (!_Settings.SETTINGS[settingName]) return {};
    const handlers = {};

    for (const level of _Settings.SETTINGS[settingName].supportedLevels) {
      if (!LEVEL_HANDLERS[level]) throw new Error("Unexpected level " + level);
      if (SettingsStore.isLevelSupported(level)) handlers[level] = LEVEL_HANDLERS[level];
    } // Always support 'default'


    if (!handlers['default']) handlers['default'] = LEVEL_HANDLERS['default'];
    return handlers;
  }

  static _getFeatureState(settingName) {
    const featuresConfig = _SdkConfig.default.get()['features'];

    const enableLabs = _SdkConfig.default.get()['enableLabs']; // we'll honour the old flag


    let featureState = enableLabs ? "labs" : "disable";

    if (featuresConfig && featuresConfig[settingName] !== undefined) {
      featureState = featuresConfig[settingName];
    }

    const allowedStates = ['enable', 'disable', 'labs'];

    if (!allowedStates.includes(featureState)) {
      console.warn("Feature state '" + featureState + "' is invalid for " + settingName);
      featureState = "disable"; // to prevent accidental features.
    }

    return featureState;
  }

} // For debugging purposes


exports.default = SettingsStore;
(0, _defineProperty2.default)(SettingsStore, "_watchers", {});
(0, _defineProperty2.default)(SettingsStore, "_monitors", {});
(0, _defineProperty2.default)(SettingsStore, "_watcherCount", 1);
global.mxSettingsStore = SettingsStore;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zZXR0aW5ncy9TZXR0aW5nc1N0b3JlLmpzIl0sIm5hbWVzIjpbIlNldHRpbmdMZXZlbCIsIkRFVklDRSIsIlJPT01fREVWSUNFIiwiUk9PTV9BQ0NPVU5UIiwiQUNDT1VOVCIsIlJPT00iLCJDT05GSUciLCJERUZBVUxUIiwiZGVmYXVsdFdhdGNoTWFuYWdlciIsIldhdGNoTWFuYWdlciIsImRlZmF1bHRTZXR0aW5ncyIsImludmVydGVkRGVmYXVsdFNldHRpbmdzIiwiZmVhdHVyZU5hbWVzIiwia2V5IiwiT2JqZWN0Iiwia2V5cyIsIlNFVFRJTkdTIiwiZGVmYXVsdCIsImlzRmVhdHVyZSIsInB1c2giLCJpbnZlcnRlZFNldHRpbmdOYW1lIiwiTEVWRUxfSEFORExFUlMiLCJEZXZpY2VTZXR0aW5nc0hhbmRsZXIiLCJSb29tRGV2aWNlU2V0dGluZ3NIYW5kbGVyIiwiUm9vbUFjY291bnRTZXR0aW5nc0hhbmRsZXIiLCJBY2NvdW50U2V0dGluZ3NIYW5kbGVyIiwiUm9vbVNldHRpbmdzSGFuZGxlciIsIkNvbmZpZ1NldHRpbmdzSGFuZGxlciIsIkRlZmF1bHRTZXR0aW5nc0hhbmRsZXIiLCJMb2NhbEVjaG9XcmFwcGVyIiwiTEVWRUxfT1JERVIiLCJTZXR0aW5nc1N0b3JlIiwid2F0Y2hTZXR0aW5nIiwic2V0dGluZ05hbWUiLCJyb29tSWQiLCJjYWxsYmFja0ZuIiwic2V0dGluZyIsIm9yaWdpbmFsU2V0dGluZ05hbWUiLCJFcnJvciIsIndhdGNoZXJJZCIsIkRhdGUiLCJnZXRUaW1lIiwiX3dhdGNoZXJDb3VudCIsImxvY2FsaXplZENhbGxiYWNrIiwiY2hhbmdlZEluUm9vbUlkIiwiYXRMZXZlbCIsIm5ld1ZhbEF0TGV2ZWwiLCJuZXdWYWx1ZSIsImdldFZhbHVlIiwiY29uc29sZSIsImxvZyIsIl93YXRjaGVycyIsInVud2F0Y2hTZXR0aW5nIiwid2F0Y2hlclJlZmVyZW5jZSIsIndhcm4iLCJtb25pdG9yU2V0dGluZyIsIl9tb25pdG9ycyIsInJlZ2lzdGVyV2F0Y2hlciIsImluUm9vbUlkIiwibGV2ZWwiLCJuZXdWYWx1ZUF0TGV2ZWwiLCJkaXMiLCJkaXNwYXRjaCIsImFjdGlvbiIsImhhc1Jvb20iLCJmaW5kIiwiciIsImdldERpc3BsYXlOYW1lIiwiZGlzcGxheU5hbWUiLCJnZXRMYWJzRmVhdHVyZXMiLCJwb3NzaWJsZUZlYXR1cmVzIiwiZmlsdGVyIiwicyIsImVuYWJsZUxhYnMiLCJTZGtDb25maWciLCJnZXQiLCJfZ2V0RmVhdHVyZVN0YXRlIiwiaXNGZWF0dXJlRW5hYmxlZCIsInNldEZlYXR1cmVFbmFibGVkIiwidmFsdWUiLCJzZXRWYWx1ZSIsImV4Y2x1ZGVEZWZhdWx0IiwibGV2ZWxPcmRlciIsInN1cHBvcnRlZExldmVsc0FyZU9yZGVyZWQiLCJzdXBwb3J0ZWRMZXZlbHMiLCJnZXRWYWx1ZUF0IiwiZXhwbGljaXQiLCJpbmNsdWRlcyIsIm1pbkluZGV4IiwiaW5kZXhPZiIsImNvbmZpZ1ZhbHVlIiwiaGFuZGxlcnMiLCJfZ2V0SGFuZGxlcnMiLCJoYW5kbGVyIiwiX2dldEZpbmFsVmFsdWUiLCJpIiwibGVuZ3RoIiwidW5kZWZpbmVkIiwiY2FsY3VsYXRlZFZhbHVlIiwiY2FsY3VsYXRlZEF0TGV2ZWwiLCJyZXN1bHRpbmdWYWx1ZSIsImNvbnRyb2xsZXIiLCJhY3R1YWxWYWx1ZSIsImdldFZhbHVlT3ZlcnJpZGUiLCJfZ2V0SGFuZGxlciIsImNhblNldFZhbHVlIiwib25DaGFuZ2UiLCJpc0xldmVsU3VwcG9ydGVkIiwiaXNTdXBwb3J0ZWQiLCJkZWJ1Z1NldHRpbmciLCJyZWFsU2V0dGluZ05hbWUiLCJkZWYiLCJKU09OIiwic3RyaW5naWZ5IiwiZG9DaGVja3MiLCJoYW5kbGVyTmFtZSIsImUiLCJtZXNzYWdlIiwiZXJyb3IiLCJmZWF0dXJlc0NvbmZpZyIsImZlYXR1cmVTdGF0ZSIsImFsbG93ZWRTdGF0ZXMiLCJnbG9iYWwiLCJteFNldHRpbmdzU3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBaUJBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQTdCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQkE7OztBQUdPLE1BQU1BLFlBQVksR0FBRztBQUN4QjtBQUNBO0FBQ0FDLEVBQUFBLE1BQU0sRUFBRSxRQUhnQjtBQUl4QkMsRUFBQUEsV0FBVyxFQUFFLGFBSlc7QUFLeEJDLEVBQUFBLFlBQVksRUFBRSxjQUxVO0FBTXhCQyxFQUFBQSxPQUFPLEVBQUUsU0FOZTtBQU94QkMsRUFBQUEsSUFBSSxFQUFFLE1BUGtCO0FBUXhCQyxFQUFBQSxNQUFNLEVBQUUsUUFSZ0I7QUFTeEJDLEVBQUFBLE9BQU8sRUFBRTtBQVRlLENBQXJCOztBQVlQLE1BQU1DLG1CQUFtQixHQUFHLElBQUlDLDBCQUFKLEVBQTVCLEMsQ0FFQTs7QUFDQSxNQUFNQyxlQUFlLEdBQUcsRUFBeEI7QUFDQSxNQUFNQyx1QkFBdUIsR0FBRyxFQUFoQztBQUNBLE1BQU1DLFlBQVksR0FBRyxFQUFyQjs7QUFDQSxLQUFLLE1BQU1DLEdBQVgsSUFBa0JDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZQyxrQkFBWixDQUFsQixFQUF5QztBQUNyQ04sRUFBQUEsZUFBZSxDQUFDRyxHQUFELENBQWYsR0FBdUJHLG1CQUFTSCxHQUFULEVBQWNJLE9BQXJDO0FBQ0EsTUFBSUQsbUJBQVNILEdBQVQsRUFBY0ssU0FBbEIsRUFBNkJOLFlBQVksQ0FBQ08sSUFBYixDQUFrQk4sR0FBbEI7O0FBQzdCLE1BQUlHLG1CQUFTSCxHQUFULEVBQWNPLG1CQUFsQixFQUF1QztBQUNuQztBQUNBO0FBQ0FULElBQUFBLHVCQUF1QixDQUFDRSxHQUFELENBQXZCLEdBQStCLENBQUNHLG1CQUFTSCxHQUFULEVBQWNJLE9BQTlDO0FBQ0g7QUFDSjs7QUFFRCxNQUFNSSxjQUFjLEdBQUc7QUFDbkIsWUFBVSxJQUFJQyw4QkFBSixDQUEwQlYsWUFBMUIsRUFBd0NKLG1CQUF4QyxDQURTO0FBRW5CLGlCQUFlLElBQUllLGtDQUFKLENBQThCZixtQkFBOUIsQ0FGSTtBQUduQixrQkFBZ0IsSUFBSWdCLG1DQUFKLENBQStCaEIsbUJBQS9CLENBSEc7QUFJbkIsYUFBVyxJQUFJaUIsK0JBQUosQ0FBMkJqQixtQkFBM0IsQ0FKUTtBQUtuQixVQUFRLElBQUlrQiw0QkFBSixDQUF3QmxCLG1CQUF4QixDQUxXO0FBTW5CLFlBQVUsSUFBSW1CLDhCQUFKLEVBTlM7QUFPbkIsYUFBVyxJQUFJQywrQkFBSixDQUEyQmxCLGVBQTNCLEVBQTRDQyx1QkFBNUM7QUFQUSxDQUF2QixDLENBVUE7O0FBQ0EsS0FBSyxNQUFNRSxHQUFYLElBQWtCQyxNQUFNLENBQUNDLElBQVAsQ0FBWU0sY0FBWixDQUFsQixFQUErQztBQUMzQ0EsRUFBQUEsY0FBYyxDQUFDUixHQUFELENBQWQsR0FBc0IsSUFBSWdCLHlCQUFKLENBQXFCUixjQUFjLENBQUNSLEdBQUQsQ0FBbkMsQ0FBdEI7QUFDSDs7QUFFRCxNQUFNaUIsV0FBVyxHQUFHLENBQ2hCLFFBRGdCLEVBQ04sYUFETSxFQUNTLGNBRFQsRUFDeUIsU0FEekIsRUFDb0MsTUFEcEMsRUFDNEMsUUFENUMsRUFDc0QsU0FEdEQsQ0FBcEI7QUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJlLE1BQU1DLGFBQU4sQ0FBb0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDdUI7QUFDQTtBQUV2Qjs7QUFHQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxTQUFPQyxZQUFQLENBQW9CQyxXQUFwQixFQUFpQ0MsTUFBakMsRUFBeUNDLFVBQXpDLEVBQXFEO0FBQ2pELFVBQU1DLE9BQU8sR0FBR3BCLG1CQUFTaUIsV0FBVCxDQUFoQjtBQUNBLFVBQU1JLG1CQUFtQixHQUFHSixXQUE1QjtBQUNBLFFBQUksQ0FBQ0csT0FBTCxFQUFjLE1BQU0sSUFBSUUsS0FBSixXQUFhTCxXQUFiLHVCQUFOOztBQUVkLFFBQUlHLE9BQU8sQ0FBQ2hCLG1CQUFaLEVBQWlDO0FBQzdCYSxNQUFBQSxXQUFXLEdBQUdHLE9BQU8sQ0FBQ2hCLG1CQUF0QjtBQUNIOztBQUVELFVBQU1tQixTQUFTLGFBQU0sSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQU4sY0FBOEJWLGFBQWEsQ0FBQ1csYUFBZCxFQUE5QixjQUErRFQsV0FBL0QsY0FBOEVDLE1BQTlFLENBQWY7O0FBRUEsVUFBTVMsaUJBQWlCLEdBQUcsQ0FBQ0MsZUFBRCxFQUFrQkMsT0FBbEIsRUFBMkJDLGFBQTNCLEtBQTZDO0FBQ25FLFlBQU1DLFFBQVEsR0FBR2hCLGFBQWEsQ0FBQ2lCLFFBQWQsQ0FBdUJYLG1CQUF2QixDQUFqQjtBQUNBRixNQUFBQSxVQUFVLENBQUNFLG1CQUFELEVBQXNCTyxlQUF0QixFQUF1Q0MsT0FBdkMsRUFBZ0RDLGFBQWhELEVBQStEQyxRQUEvRCxDQUFWO0FBQ0gsS0FIRDs7QUFLQUUsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLGdDQUFvQ2pCLFdBQXBDLGNBQW1EQyxNQUFNLElBQUksYUFBN0Qsb0JBQW9GSyxTQUFwRjtBQUNBUixJQUFBQSxhQUFhLENBQUNvQixTQUFkLENBQXdCWixTQUF4QixJQUFxQ0ksaUJBQXJDO0FBQ0FuQyxJQUFBQSxtQkFBbUIsQ0FBQ3dCLFlBQXBCLENBQWlDQyxXQUFqQyxFQUE4Q0MsTUFBOUMsRUFBc0RTLGlCQUF0RDtBQUVBLFdBQU9KLFNBQVA7QUFDSDtBQUVEOzs7Ozs7OztBQU1BLFNBQU9hLGNBQVAsQ0FBc0JDLGdCQUF0QixFQUF3QztBQUNwQyxRQUFJLENBQUN0QixhQUFhLENBQUNvQixTQUFkLENBQXdCRSxnQkFBeEIsQ0FBTCxFQUFnRDtBQUM1Q0osTUFBQUEsT0FBTyxDQUFDSyxJQUFSLDBDQUErQ0QsZ0JBQS9DO0FBQ0E7QUFDSDs7QUFFREosSUFBQUEsT0FBTyxDQUFDQyxHQUFSLDZCQUFpQ0csZ0JBQWpDO0FBQ0E3QyxJQUFBQSxtQkFBbUIsQ0FBQzRDLGNBQXBCLENBQW1DckIsYUFBYSxDQUFDb0IsU0FBZCxDQUF3QkUsZ0JBQXhCLENBQW5DO0FBQ0EsV0FBT3RCLGFBQWEsQ0FBQ29CLFNBQWQsQ0FBd0JFLGdCQUF4QixDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7OztBQVFBLFNBQU9FLGNBQVAsQ0FBc0J0QixXQUF0QixFQUFtQ0MsTUFBbkMsRUFBMkM7QUFDdkMsUUFBSSxDQUFDLEtBQUtzQixTQUFMLENBQWV2QixXQUFmLENBQUwsRUFBa0MsS0FBS3VCLFNBQUwsQ0FBZXZCLFdBQWYsSUFBOEIsRUFBOUI7O0FBRWxDLFVBQU13QixlQUFlLEdBQUcsTUFBTTtBQUMxQixXQUFLRCxTQUFMLENBQWV2QixXQUFmLEVBQTRCQyxNQUE1QixJQUFzQ0gsYUFBYSxDQUFDQyxZQUFkLENBQ2xDQyxXQURrQyxFQUNyQkMsTUFEcUIsRUFDYixDQUFDRCxXQUFELEVBQWN5QixRQUFkLEVBQXdCQyxLQUF4QixFQUErQkMsZUFBL0IsRUFBZ0RiLFFBQWhELEtBQTZEO0FBQzlFYyw0QkFBSUMsUUFBSixDQUFhO0FBQ1RDLFVBQUFBLE1BQU0sRUFBRSxpQkFEQztBQUVUOUIsVUFBQUEsV0FGUztBQUdUQyxVQUFBQSxNQUFNLEVBQUV3QixRQUhDO0FBSVRDLFVBQUFBLEtBSlM7QUFLVEMsVUFBQUEsZUFMUztBQU1UYixVQUFBQTtBQU5TLFNBQWI7QUFRSCxPQVZpQyxDQUF0QztBQVlILEtBYkQ7O0FBZUEsVUFBTWlCLE9BQU8sR0FBR2xELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZLEtBQUt5QyxTQUFMLENBQWV2QixXQUFmLENBQVosRUFBeUNnQyxJQUF6QyxDQUErQ0MsQ0FBRCxJQUFPQSxDQUFDLEtBQUtoQyxNQUFOLElBQWdCZ0MsQ0FBQyxLQUFLLElBQTNFLENBQWhCOztBQUNBLFFBQUksQ0FBQ0YsT0FBTCxFQUFjO0FBQ1ZQLE1BQUFBLGVBQWU7QUFDbEIsS0FGRCxNQUVPO0FBQ0gsVUFBSXZCLE1BQU0sS0FBSyxJQUFmLEVBQXFCO0FBQ2pCO0FBQ0EsYUFBSyxNQUFNQSxNQUFYLElBQXFCcEIsTUFBTSxDQUFDQyxJQUFQLENBQVksS0FBS3lDLFNBQUwsQ0FBZXZCLFdBQWYsQ0FBWixDQUFyQixFQUErRDtBQUMzREYsVUFBQUEsYUFBYSxDQUFDcUIsY0FBZCxDQUE2QixLQUFLSSxTQUFMLENBQWV2QixXQUFmLEVBQTRCQyxNQUE1QixDQUE3QjtBQUNIOztBQUNELGFBQUtzQixTQUFMLENBQWV2QixXQUFmLElBQThCLEVBQTlCO0FBQ0F3QixRQUFBQSxlQUFlO0FBQ2xCLE9BUkUsQ0FRRDs7QUFDTDtBQUNKO0FBRUQ7Ozs7Ozs7OztBQU9BLFNBQU9VLGNBQVAsQ0FBc0JsQyxXQUF0QixFQUFtQ1ksT0FBTyxHQUFHLFNBQTdDLEVBQXdEO0FBQ3BELFFBQUksQ0FBQzdCLG1CQUFTaUIsV0FBVCxDQUFELElBQTBCLENBQUNqQixtQkFBU2lCLFdBQVQsRUFBc0JtQyxXQUFyRCxFQUFrRSxPQUFPLElBQVA7QUFFbEUsUUFBSUEsV0FBVyxHQUFHcEQsbUJBQVNpQixXQUFULEVBQXNCbUMsV0FBeEM7O0FBQ0EsUUFBSUEsV0FBVyxZQUFZdEQsTUFBM0IsRUFBbUM7QUFDL0IsVUFBSXNELFdBQVcsQ0FBQ3ZCLE9BQUQsQ0FBZixFQUEwQnVCLFdBQVcsR0FBR0EsV0FBVyxDQUFDdkIsT0FBRCxDQUF6QixDQUExQixLQUNLdUIsV0FBVyxHQUFHQSxXQUFXLENBQUMsU0FBRCxDQUF6QjtBQUNSOztBQUVELFdBQU8seUJBQUdBLFdBQUgsQ0FBUDtBQUNIO0FBRUQ7Ozs7OztBQUlBLFNBQU9DLGVBQVAsR0FBeUI7QUFDckIsVUFBTUMsZ0JBQWdCLEdBQUd4RCxNQUFNLENBQUNDLElBQVAsQ0FBWUMsa0JBQVosRUFBc0J1RCxNQUF0QixDQUE4QkMsQ0FBRCxJQUFPekMsYUFBYSxDQUFDYixTQUFkLENBQXdCc0QsQ0FBeEIsQ0FBcEMsQ0FBekI7O0FBRUEsVUFBTUMsVUFBVSxHQUFHQyxtQkFBVUMsR0FBVixHQUFnQixZQUFoQixDQUFuQjs7QUFDQSxRQUFJRixVQUFKLEVBQWdCLE9BQU9ILGdCQUFQO0FBRWhCLFdBQU9BLGdCQUFnQixDQUFDQyxNQUFqQixDQUF5QkMsQ0FBRCxJQUFPekMsYUFBYSxDQUFDNkMsZ0JBQWQsQ0FBK0JKLENBQS9CLE1BQXNDLE1BQXJFLENBQVA7QUFDSDtBQUVEOzs7Ozs7O0FBS0EsU0FBT3RELFNBQVAsQ0FBaUJlLFdBQWpCLEVBQThCO0FBQzFCLFFBQUksQ0FBQ2pCLG1CQUFTaUIsV0FBVCxDQUFMLEVBQTRCLE9BQU8sS0FBUDtBQUM1QixXQUFPakIsbUJBQVNpQixXQUFULEVBQXNCZixTQUE3QjtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9BLFNBQU8yRCxnQkFBUCxDQUF3QjVDLFdBQXhCLEVBQXFDQyxNQUFNLEdBQUcsSUFBOUMsRUFBb0Q7QUFDaEQsUUFBSSxDQUFDSCxhQUFhLENBQUNiLFNBQWQsQ0FBd0JlLFdBQXhCLENBQUwsRUFBMkM7QUFDdkMsWUFBTSxJQUFJSyxLQUFKLENBQVUsYUFBYUwsV0FBYixHQUEyQixtQkFBckMsQ0FBTjtBQUNIOztBQUVELFdBQU9GLGFBQWEsQ0FBQ2lCLFFBQWQsQ0FBdUJmLFdBQXZCLEVBQW9DQyxNQUFwQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQSxTQUFPNEMsaUJBQVAsQ0FBeUI3QyxXQUF6QixFQUFzQzhDLEtBQXRDLEVBQTZDO0FBQ3pDO0FBQ0EsUUFBSSxDQUFDL0QsbUJBQVNpQixXQUFULENBQUwsRUFBNEI7QUFDeEIsWUFBTSxJQUFJSyxLQUFKLENBQVUsY0FBY0wsV0FBZCxHQUE0QixvQ0FBdEMsQ0FBTjtBQUNIOztBQUNELFFBQUksQ0FBQ0YsYUFBYSxDQUFDYixTQUFkLENBQXdCZSxXQUF4QixDQUFMLEVBQTJDO0FBQ3ZDLFlBQU0sSUFBSUssS0FBSixDQUFVLGFBQWFMLFdBQWIsR0FBMkIsbUJBQXJDLENBQU47QUFDSDs7QUFFRCxXQUFPRixhQUFhLENBQUNpRCxRQUFkLENBQXVCL0MsV0FBdkIsRUFBb0MsSUFBcEMsRUFBMEMsUUFBMUMsRUFBb0Q4QyxLQUFwRCxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7OztBQVFBLFNBQU8vQixRQUFQLENBQWdCZixXQUFoQixFQUE2QkMsTUFBTSxHQUFHLElBQXRDLEVBQTRDK0MsY0FBYyxHQUFHLEtBQTdELEVBQW9FO0FBQ2hFO0FBQ0EsUUFBSSxDQUFDakUsbUJBQVNpQixXQUFULENBQUwsRUFBNEI7QUFDeEIsWUFBTSxJQUFJSyxLQUFKLENBQVUsY0FBY0wsV0FBZCxHQUE0QixvQ0FBdEMsQ0FBTjtBQUNIOztBQUVELFVBQU1HLE9BQU8sR0FBR3BCLG1CQUFTaUIsV0FBVCxDQUFoQjtBQUNBLFVBQU1pRCxVQUFVLEdBQUk5QyxPQUFPLENBQUMrQyx5QkFBUixHQUFvQy9DLE9BQU8sQ0FBQ2dELGVBQTVDLEdBQThEdEQsV0FBbEY7QUFFQSxXQUFPQyxhQUFhLENBQUNzRCxVQUFkLENBQXlCSCxVQUFVLENBQUMsQ0FBRCxDQUFuQyxFQUF3Q2pELFdBQXhDLEVBQXFEQyxNQUFyRCxFQUE2RCxLQUE3RCxFQUFvRStDLGNBQXBFLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O0FBV0EsU0FBT0ksVUFBUCxDQUFrQjFCLEtBQWxCLEVBQXlCMUIsV0FBekIsRUFBc0NDLE1BQU0sR0FBRyxJQUEvQyxFQUFxRG9ELFFBQVEsR0FBRyxLQUFoRSxFQUF1RUwsY0FBYyxHQUFHLEtBQXhGLEVBQStGO0FBQzNGO0FBQ0EsVUFBTTdDLE9BQU8sR0FBR3BCLG1CQUFTaUIsV0FBVCxDQUFoQjs7QUFDQSxRQUFJLENBQUNHLE9BQUwsRUFBYztBQUNWLFlBQU0sSUFBSUUsS0FBSixDQUFVLGNBQWNMLFdBQWQsR0FBNEIsb0NBQXRDLENBQU47QUFDSDs7QUFFRCxVQUFNaUQsVUFBVSxHQUFJOUMsT0FBTyxDQUFDK0MseUJBQVIsR0FBb0MvQyxPQUFPLENBQUNnRCxlQUE1QyxHQUE4RHRELFdBQWxGO0FBQ0EsUUFBSSxDQUFDb0QsVUFBVSxDQUFDSyxRQUFYLENBQW9CLFNBQXBCLENBQUwsRUFBcUNMLFVBQVUsQ0FBQy9ELElBQVgsQ0FBZ0IsU0FBaEIsRUFSc0QsQ0FRMUI7O0FBRWpFLFVBQU1xRSxRQUFRLEdBQUdOLFVBQVUsQ0FBQ08sT0FBWCxDQUFtQjlCLEtBQW5CLENBQWpCO0FBQ0EsUUFBSTZCLFFBQVEsS0FBSyxDQUFDLENBQWxCLEVBQXFCLE1BQU0sSUFBSWxELEtBQUosQ0FBVSxXQUFXcUIsS0FBWCxHQUFtQixxQkFBN0IsQ0FBTjs7QUFFckIsUUFBSTVCLGFBQWEsQ0FBQ2IsU0FBZCxDQUF3QmUsV0FBeEIsQ0FBSixFQUEwQztBQUN0QyxZQUFNeUQsV0FBVyxHQUFHM0QsYUFBYSxDQUFDNkMsZ0JBQWQsQ0FBK0IzQyxXQUEvQixDQUFwQjs7QUFDQSxVQUFJeUQsV0FBVyxLQUFLLFFBQXBCLEVBQThCLE9BQU8sSUFBUDtBQUM5QixVQUFJQSxXQUFXLEtBQUssU0FBcEIsRUFBK0IsT0FBTyxLQUFQLENBSE8sQ0FJdEM7QUFDSDs7QUFFRCxVQUFNQyxRQUFRLEdBQUc1RCxhQUFhLENBQUM2RCxZQUFkLENBQTJCM0QsV0FBM0IsQ0FBakIsQ0FwQjJGLENBc0IzRjtBQUNBOzs7QUFDQSxRQUFJRyxPQUFPLENBQUNoQixtQkFBWixFQUFpQztBQUM3QjtBQUNBYSxNQUFBQSxXQUFXLEdBQUdHLE9BQU8sQ0FBQ2hCLG1CQUF0QjtBQUNIOztBQUVELFFBQUlrRSxRQUFKLEVBQWM7QUFDVixZQUFNTyxPQUFPLEdBQUdGLFFBQVEsQ0FBQ2hDLEtBQUQsQ0FBeEI7O0FBQ0EsVUFBSSxDQUFDa0MsT0FBTCxFQUFjO0FBQ1YsZUFBTzlELGFBQWEsQ0FBQytELGNBQWQsQ0FBNkIxRCxPQUE3QixFQUFzQ3VCLEtBQXRDLEVBQTZDekIsTUFBN0MsRUFBcUQsSUFBckQsRUFBMkQsSUFBM0QsQ0FBUDtBQUNIOztBQUNELFlBQU02QyxLQUFLLEdBQUdjLE9BQU8sQ0FBQzdDLFFBQVIsQ0FBaUJmLFdBQWpCLEVBQThCQyxNQUE5QixDQUFkO0FBQ0EsYUFBT0gsYUFBYSxDQUFDK0QsY0FBZCxDQUE2QjFELE9BQTdCLEVBQXNDdUIsS0FBdEMsRUFBNkN6QixNQUE3QyxFQUFxRDZDLEtBQXJELEVBQTREcEIsS0FBNUQsQ0FBUDtBQUNIOztBQUVELFNBQUssSUFBSW9DLENBQUMsR0FBR1AsUUFBYixFQUF1Qk8sQ0FBQyxHQUFHYixVQUFVLENBQUNjLE1BQXRDLEVBQThDRCxDQUFDLEVBQS9DLEVBQW1EO0FBQy9DLFlBQU1GLE9BQU8sR0FBR0YsUUFBUSxDQUFDVCxVQUFVLENBQUNhLENBQUQsQ0FBWCxDQUF4QjtBQUNBLFVBQUksQ0FBQ0YsT0FBTCxFQUFjO0FBQ2QsVUFBSVosY0FBYyxJQUFJQyxVQUFVLENBQUNhLENBQUQsQ0FBVixLQUFrQixTQUF4QyxFQUFtRDtBQUVuRCxZQUFNaEIsS0FBSyxHQUFHYyxPQUFPLENBQUM3QyxRQUFSLENBQWlCZixXQUFqQixFQUE4QkMsTUFBOUIsQ0FBZDtBQUNBLFVBQUk2QyxLQUFLLEtBQUssSUFBVixJQUFrQkEsS0FBSyxLQUFLa0IsU0FBaEMsRUFBMkM7QUFDM0MsYUFBT2xFLGFBQWEsQ0FBQytELGNBQWQsQ0FBNkIxRCxPQUE3QixFQUFzQ3VCLEtBQXRDLEVBQTZDekIsTUFBN0MsRUFBcUQ2QyxLQUFyRCxFQUE0REcsVUFBVSxDQUFDYSxDQUFELENBQXRFLENBQVA7QUFDSDs7QUFFRCxXQUFPaEUsYUFBYSxDQUFDK0QsY0FBZCxDQUE2QjFELE9BQTdCLEVBQXNDdUIsS0FBdEMsRUFBNkN6QixNQUE3QyxFQUFxRCxJQUFyRCxFQUEyRCxJQUEzRCxDQUFQO0FBQ0g7O0FBRUQsU0FBTzRELGNBQVAsQ0FBc0IxRCxPQUF0QixFQUErQnVCLEtBQS9CLEVBQXNDekIsTUFBdEMsRUFBOENnRSxlQUE5QyxFQUErREMsaUJBQS9ELEVBQWtGO0FBQzlFLFFBQUlDLGNBQWMsR0FBR0YsZUFBckI7O0FBRUEsUUFBSTlELE9BQU8sQ0FBQ2lFLFVBQVosRUFBd0I7QUFDcEIsWUFBTUMsV0FBVyxHQUFHbEUsT0FBTyxDQUFDaUUsVUFBUixDQUFtQkUsZ0JBQW5CLENBQW9DNUMsS0FBcEMsRUFBMkN6QixNQUEzQyxFQUFtRGdFLGVBQW5ELEVBQW9FQyxpQkFBcEUsQ0FBcEI7QUFDQSxVQUFJRyxXQUFXLEtBQUtMLFNBQWhCLElBQTZCSyxXQUFXLEtBQUssSUFBakQsRUFBdURGLGNBQWMsR0FBR0UsV0FBakI7QUFDMUQ7O0FBRUQsUUFBSWxFLE9BQU8sQ0FBQ2hCLG1CQUFaLEVBQWlDZ0YsY0FBYyxHQUFHLENBQUNBLGNBQWxCO0FBQ2pDLFdBQU9BLGNBQVA7QUFDSDtBQUVEO0FBQWlDOztBQUNqQzs7Ozs7Ozs7Ozs7O0FBV0E7OztBQUNBLGVBQWFwQixRQUFiLENBQXNCL0MsV0FBdEIsRUFBbUNDLE1BQW5DLEVBQTJDeUIsS0FBM0MsRUFBa0RvQixLQUFsRCxFQUF5RDtBQUNyRDtBQUNBLFVBQU0zQyxPQUFPLEdBQUdwQixtQkFBU2lCLFdBQVQsQ0FBaEI7O0FBQ0EsUUFBSSxDQUFDRyxPQUFMLEVBQWM7QUFDVixZQUFNLElBQUlFLEtBQUosQ0FBVSxjQUFjTCxXQUFkLEdBQTRCLG9DQUF0QyxDQUFOO0FBQ0g7O0FBRUQsVUFBTTRELE9BQU8sR0FBRzlELGFBQWEsQ0FBQ3lFLFdBQWQsQ0FBMEJ2RSxXQUExQixFQUF1QzBCLEtBQXZDLENBQWhCOztBQUNBLFFBQUksQ0FBQ2tDLE9BQUwsRUFBYztBQUNWLFlBQU0sSUFBSXZELEtBQUosQ0FBVSxhQUFhTCxXQUFiLEdBQTJCLCtCQUEzQixHQUE2RDBCLEtBQXZFLENBQU47QUFDSDs7QUFFRCxRQUFJdkIsT0FBTyxDQUFDaEIsbUJBQVosRUFBaUM7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQWEsTUFBQUEsV0FBVyxHQUFHRyxPQUFPLENBQUNoQixtQkFBdEI7QUFDQTJELE1BQUFBLEtBQUssR0FBRyxDQUFDQSxLQUFUO0FBQ0g7O0FBRUQsUUFBSSxDQUFDYyxPQUFPLENBQUNZLFdBQVIsQ0FBb0J4RSxXQUFwQixFQUFpQ0MsTUFBakMsQ0FBTCxFQUErQztBQUMzQyxZQUFNLElBQUlJLEtBQUosQ0FBVSxxQkFBcUJMLFdBQXJCLEdBQW1DLE1BQW5DLEdBQTRDMEIsS0FBNUMsR0FBb0QsTUFBcEQsR0FBNkR6QixNQUF2RSxDQUFOO0FBQ0g7O0FBRUQsVUFBTTJELE9BQU8sQ0FBQ2IsUUFBUixDQUFpQi9DLFdBQWpCLEVBQThCQyxNQUE5QixFQUFzQzZDLEtBQXRDLENBQU47QUFFQSxVQUFNc0IsVUFBVSxHQUFHakUsT0FBTyxDQUFDaUUsVUFBM0I7O0FBQ0EsUUFBSUEsVUFBSixFQUFnQjtBQUNaQSxNQUFBQSxVQUFVLENBQUNLLFFBQVgsQ0FBb0IvQyxLQUFwQixFQUEyQnpCLE1BQTNCLEVBQW1DNkMsS0FBbkM7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVBLFNBQU8wQixXQUFQLENBQW1CeEUsV0FBbkIsRUFBZ0NDLE1BQWhDLEVBQXdDeUIsS0FBeEMsRUFBK0M7QUFDM0M7QUFDQSxRQUFJLENBQUMzQyxtQkFBU2lCLFdBQVQsQ0FBTCxFQUE0QjtBQUN4QixZQUFNLElBQUlLLEtBQUosQ0FBVSxjQUFjTCxXQUFkLEdBQTRCLG9DQUF0QyxDQUFOO0FBQ0g7O0FBRUQsVUFBTTRELE9BQU8sR0FBRzlELGFBQWEsQ0FBQ3lFLFdBQWQsQ0FBMEJ2RSxXQUExQixFQUF1QzBCLEtBQXZDLENBQWhCOztBQUNBLFFBQUksQ0FBQ2tDLE9BQUwsRUFBYyxPQUFPLEtBQVA7QUFDZCxXQUFPQSxPQUFPLENBQUNZLFdBQVIsQ0FBb0J4RSxXQUFwQixFQUFpQ0MsTUFBakMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUEsU0FBT3lFLGdCQUFQLENBQXdCaEQsS0FBeEIsRUFBK0I7QUFDM0IsUUFBSSxDQUFDdEMsY0FBYyxDQUFDc0MsS0FBRCxDQUFuQixFQUE0QixPQUFPLEtBQVA7QUFDNUIsV0FBT3RDLGNBQWMsQ0FBQ3NDLEtBQUQsQ0FBZCxDQUFzQmlELFdBQXRCLEVBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7QUFPQSxTQUFPQyxZQUFQLENBQW9CQyxlQUFwQixFQUFxQzVFLE1BQXJDLEVBQTZDO0FBQ3pDZSxJQUFBQSxPQUFPLENBQUNDLEdBQVIscUJBQXlCNEQsZUFBekIsR0FEeUMsQ0FHekM7QUFDQTtBQUNBOztBQUVBLFVBQU1DLEdBQUcsR0FBRy9GLG1CQUFTOEYsZUFBVCxDQUFaO0FBQ0E3RCxJQUFBQSxPQUFPLENBQUNDLEdBQVIsMkJBQStCNkQsR0FBRyxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUYsR0FBZixDQUFILEdBQXlCLGFBQTNEO0FBQ0E5RCxJQUFBQSxPQUFPLENBQUNDLEdBQVIsb0NBQXdDOEQsSUFBSSxDQUFDQyxTQUFMLENBQWVuRixXQUFmLENBQXhDO0FBQ0FtQixJQUFBQSxPQUFPLENBQUNDLEdBQVIsb0NBQXdDOEQsSUFBSSxDQUFDQyxTQUFMLENBQWVuRyxNQUFNLENBQUNDLElBQVAsQ0FBWU0sY0FBWixDQUFmLENBQXhDOztBQUVBLFVBQU02RixRQUFRLEdBQUlqRixXQUFELElBQWlCO0FBQzlCLFdBQUssTUFBTWtGLFdBQVgsSUFBMEJyRyxNQUFNLENBQUNDLElBQVAsQ0FBWU0sY0FBWixDQUExQixFQUF1RDtBQUNuRCxjQUFNd0UsT0FBTyxHQUFHeEUsY0FBYyxDQUFDOEYsV0FBRCxDQUE5Qjs7QUFFQSxZQUFJO0FBQ0EsZ0JBQU1wQyxLQUFLLEdBQUdjLE9BQU8sQ0FBQzdDLFFBQVIsQ0FBaUJmLFdBQWpCLEVBQThCQyxNQUE5QixDQUFkO0FBQ0FlLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixtQkFBdUJpRSxXQUF2QixjQUFzQ2pGLE1BQU0sSUFBSSxXQUFoRCxnQkFBaUU4RSxJQUFJLENBQUNDLFNBQUwsQ0FBZWxDLEtBQWYsQ0FBakU7QUFDSCxTQUhELENBR0UsT0FBT3FDLENBQVAsRUFBVTtBQUNSbkUsVUFBQUEsT0FBTyxDQUFDQyxHQUFSLG1CQUF1QjJDLE9BQXZCLGNBQWtDM0QsTUFBTSxJQUFJLFdBQTVDLDJCQUF3RWtGLENBQUMsQ0FBQ0MsT0FBMUU7QUFDQXBFLFVBQUFBLE9BQU8sQ0FBQ3FFLEtBQVIsQ0FBY0YsQ0FBZDtBQUNIOztBQUVELFlBQUlsRixNQUFKLEVBQVk7QUFDUixjQUFJO0FBQ0Esa0JBQU02QyxLQUFLLEdBQUdjLE9BQU8sQ0FBQzdDLFFBQVIsQ0FBaUJmLFdBQWpCLEVBQThCLElBQTlCLENBQWQ7QUFDQWdCLFlBQUFBLE9BQU8sQ0FBQ0MsR0FBUixtQkFBdUJpRSxXQUF2QiwwQkFBa0RILElBQUksQ0FBQ0MsU0FBTCxDQUFlbEMsS0FBZixDQUFsRDtBQUNILFdBSEQsQ0FHRSxPQUFPcUMsQ0FBUCxFQUFVO0FBQ1JuRSxZQUFBQSxPQUFPLENBQUNDLEdBQVIsbUJBQXVCMkMsT0FBdkIscUNBQXlEdUIsQ0FBQyxDQUFDQyxPQUEzRDtBQUNBcEUsWUFBQUEsT0FBTyxDQUFDcUUsS0FBUixDQUFjRixDQUFkO0FBQ0g7QUFDSjtBQUNKOztBQUVEbkUsTUFBQUEsT0FBTyxDQUFDQyxHQUFSO0FBQ0FELE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUjs7QUFFQSxVQUFJO0FBQ0EsY0FBTTZCLEtBQUssR0FBR2hELGFBQWEsQ0FBQ2lCLFFBQWQsQ0FBdUJmLFdBQXZCLEVBQW9DQyxNQUFwQyxDQUFkO0FBQ0FlLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUix5Q0FBNkNoQixNQUFNLElBQUksV0FBdkQsaUJBQXlFOEUsSUFBSSxDQUFDQyxTQUFMLENBQWVsQyxLQUFmLENBQXpFO0FBQ0gsT0FIRCxDQUdFLE9BQU9xQyxDQUFQLEVBQVU7QUFDUm5FLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUix5Q0FBNkNoQixNQUFNLElBQUksV0FBdkQsMkJBQW1Ga0YsQ0FBQyxDQUFDQyxPQUFyRjtBQUNBcEUsUUFBQUEsT0FBTyxDQUFDcUUsS0FBUixDQUFjRixDQUFkO0FBQ0g7O0FBRUQsVUFBSWxGLE1BQUosRUFBWTtBQUNSLFlBQUk7QUFDQSxnQkFBTTZDLEtBQUssR0FBR2hELGFBQWEsQ0FBQ2lCLFFBQWQsQ0FBdUJmLFdBQXZCLEVBQW9DLElBQXBDLENBQWQ7QUFDQWdCLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixzREFBMEQ4RCxJQUFJLENBQUNDLFNBQUwsQ0FBZWxDLEtBQWYsQ0FBMUQ7QUFDSCxTQUhELENBR0UsT0FBT3FDLENBQVAsRUFBVTtBQUNSbkUsVUFBQUEsT0FBTyxDQUFDQyxHQUFSLGlFQUFxRWtFLENBQUMsQ0FBQ0MsT0FBdkU7QUFDQXBFLFVBQUFBLE9BQU8sQ0FBQ3FFLEtBQVIsQ0FBY0YsQ0FBZDtBQUNIO0FBQ0o7O0FBRUQsV0FBSyxNQUFNekQsS0FBWCxJQUFvQjdCLFdBQXBCLEVBQWlDO0FBQzdCLFlBQUk7QUFDQSxnQkFBTWlELEtBQUssR0FBR2hELGFBQWEsQ0FBQ3NELFVBQWQsQ0FBeUIxQixLQUF6QixFQUFnQzFCLFdBQWhDLEVBQTZDQyxNQUE3QyxDQUFkO0FBQ0FlLFVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixpQ0FBcUNTLEtBQXJDLGNBQThDekIsTUFBTSxJQUFJLFdBQXhELGdCQUF5RThFLElBQUksQ0FBQ0MsU0FBTCxDQUFlbEMsS0FBZixDQUF6RTtBQUNILFNBSEQsQ0FHRSxPQUFPcUMsQ0FBUCxFQUFVO0FBQ1JuRSxVQUFBQSxPQUFPLENBQUNDLEdBQVIsaUNBQXFDUyxLQUFyQyxjQUE4Q3pCLE1BQU0sSUFBSSxXQUF4RCwyQkFBb0ZrRixDQUFDLENBQUNDLE9BQXRGO0FBQ0FwRSxVQUFBQSxPQUFPLENBQUNxRSxLQUFSLENBQWNGLENBQWQ7QUFDSDs7QUFFRCxZQUFJbEYsTUFBSixFQUFZO0FBQ1IsY0FBSTtBQUNBLGtCQUFNNkMsS0FBSyxHQUFHaEQsYUFBYSxDQUFDc0QsVUFBZCxDQUF5QjFCLEtBQXpCLEVBQWdDMUIsV0FBaEMsRUFBNkMsSUFBN0MsQ0FBZDtBQUNBZ0IsWUFBQUEsT0FBTyxDQUFDQyxHQUFSLGlDQUFxQ1MsS0FBckMsMEJBQTBEcUQsSUFBSSxDQUFDQyxTQUFMLENBQWVsQyxLQUFmLENBQTFEO0FBQ0gsV0FIRCxDQUdFLE9BQU9xQyxDQUFQLEVBQVU7QUFDUm5FLFlBQUFBLE9BQU8sQ0FBQ0MsR0FBUixpQ0FBcUNTLEtBQXJDLHNDQUFzRXlELENBQUMsQ0FBQ0MsT0FBeEU7QUFDQXBFLFlBQUFBLE9BQU8sQ0FBQ3FFLEtBQVIsQ0FBY0YsQ0FBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEtBL0REOztBQWlFQUYsSUFBQUEsUUFBUSxDQUFDSixlQUFELENBQVI7O0FBRUEsUUFBSUMsR0FBRyxDQUFDM0YsbUJBQVIsRUFBNkI7QUFDekI2QixNQUFBQSxPQUFPLENBQUNDLEdBQVI7QUFDQUQsTUFBQUEsT0FBTyxDQUFDQyxHQUFSLHlCQUE2QjZELEdBQUcsQ0FBQzNGLG1CQUFqQztBQUNBOEYsTUFBQUEsUUFBUSxDQUFDSCxHQUFHLENBQUMzRixtQkFBTCxDQUFSO0FBQ0g7O0FBRUQ2QixJQUFBQSxPQUFPLENBQUNDLEdBQVI7QUFDSDs7QUFFRCxTQUFPc0QsV0FBUCxDQUFtQnZFLFdBQW5CLEVBQWdDMEIsS0FBaEMsRUFBdUM7QUFDbkMsVUFBTWdDLFFBQVEsR0FBRzVELGFBQWEsQ0FBQzZELFlBQWQsQ0FBMkIzRCxXQUEzQixDQUFqQjs7QUFDQSxRQUFJLENBQUMwRCxRQUFRLENBQUNoQyxLQUFELENBQWIsRUFBc0IsT0FBTyxJQUFQO0FBQ3RCLFdBQU9nQyxRQUFRLENBQUNoQyxLQUFELENBQWY7QUFDSDs7QUFFRCxTQUFPaUMsWUFBUCxDQUFvQjNELFdBQXBCLEVBQWlDO0FBQzdCLFFBQUksQ0FBQ2pCLG1CQUFTaUIsV0FBVCxDQUFMLEVBQTRCLE9BQU8sRUFBUDtBQUU1QixVQUFNMEQsUUFBUSxHQUFHLEVBQWpCOztBQUNBLFNBQUssTUFBTWhDLEtBQVgsSUFBb0IzQyxtQkFBU2lCLFdBQVQsRUFBc0JtRCxlQUExQyxFQUEyRDtBQUN2RCxVQUFJLENBQUMvRCxjQUFjLENBQUNzQyxLQUFELENBQW5CLEVBQTRCLE1BQU0sSUFBSXJCLEtBQUosQ0FBVSxzQkFBc0JxQixLQUFoQyxDQUFOO0FBQzVCLFVBQUk1QixhQUFhLENBQUM0RSxnQkFBZCxDQUErQmhELEtBQS9CLENBQUosRUFBMkNnQyxRQUFRLENBQUNoQyxLQUFELENBQVIsR0FBa0J0QyxjQUFjLENBQUNzQyxLQUFELENBQWhDO0FBQzlDLEtBUDRCLENBUzdCOzs7QUFDQSxRQUFJLENBQUNnQyxRQUFRLENBQUMsU0FBRCxDQUFiLEVBQTBCQSxRQUFRLENBQUMsU0FBRCxDQUFSLEdBQXNCdEUsY0FBYyxDQUFDLFNBQUQsQ0FBcEM7QUFFMUIsV0FBT3NFLFFBQVA7QUFDSDs7QUFFRCxTQUFPZixnQkFBUCxDQUF3QjNDLFdBQXhCLEVBQXFDO0FBQ2pDLFVBQU1zRixjQUFjLEdBQUc3QyxtQkFBVUMsR0FBVixHQUFnQixVQUFoQixDQUF2Qjs7QUFDQSxVQUFNRixVQUFVLEdBQUdDLG1CQUFVQyxHQUFWLEdBQWdCLFlBQWhCLENBQW5CLENBRmlDLENBRWlCOzs7QUFFbEQsUUFBSTZDLFlBQVksR0FBRy9DLFVBQVUsR0FBRyxNQUFILEdBQVksU0FBekM7O0FBQ0EsUUFBSThDLGNBQWMsSUFBSUEsY0FBYyxDQUFDdEYsV0FBRCxDQUFkLEtBQWdDZ0UsU0FBdEQsRUFBaUU7QUFDN0R1QixNQUFBQSxZQUFZLEdBQUdELGNBQWMsQ0FBQ3RGLFdBQUQsQ0FBN0I7QUFDSDs7QUFFRCxVQUFNd0YsYUFBYSxHQUFHLENBQUMsUUFBRCxFQUFXLFNBQVgsRUFBc0IsTUFBdEIsQ0FBdEI7O0FBQ0EsUUFBSSxDQUFDQSxhQUFhLENBQUNsQyxRQUFkLENBQXVCaUMsWUFBdkIsQ0FBTCxFQUEyQztBQUN2Q3ZFLE1BQUFBLE9BQU8sQ0FBQ0ssSUFBUixDQUFhLG9CQUFvQmtFLFlBQXBCLEdBQW1DLG1CQUFuQyxHQUF5RHZGLFdBQXRFO0FBQ0F1RixNQUFBQSxZQUFZLEdBQUcsU0FBZixDQUZ1QyxDQUViO0FBQzdCOztBQUVELFdBQU9BLFlBQVA7QUFDSDs7QUExZThCLEMsQ0E2ZW5DOzs7OzhCQTdlcUJ6RixhLGVBUUUsRTs4QkFSRkEsYSxlQVNFLEU7OEJBVEZBLGEsbUJBWU0sQztBQWtlM0IyRixNQUFNLENBQUNDLGVBQVAsR0FBeUI1RixhQUF6QiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE3IFRyYXZpcyBSYWxzdG9uXHJcbkNvcHlyaWdodCAyMDE5IE5ldyBWZWN0b3IgTHRkLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBEZXZpY2VTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vaGFuZGxlcnMvRGV2aWNlU2V0dGluZ3NIYW5kbGVyXCI7XHJcbmltcG9ydCBSb29tRGV2aWNlU2V0dGluZ3NIYW5kbGVyIGZyb20gXCIuL2hhbmRsZXJzL1Jvb21EZXZpY2VTZXR0aW5nc0hhbmRsZXJcIjtcclxuaW1wb3J0IERlZmF1bHRTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vaGFuZGxlcnMvRGVmYXVsdFNldHRpbmdzSGFuZGxlclwiO1xyXG5pbXBvcnQgUm9vbUFjY291bnRTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vaGFuZGxlcnMvUm9vbUFjY291bnRTZXR0aW5nc0hhbmRsZXJcIjtcclxuaW1wb3J0IEFjY291bnRTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vaGFuZGxlcnMvQWNjb3VudFNldHRpbmdzSGFuZGxlclwiO1xyXG5pbXBvcnQgUm9vbVNldHRpbmdzSGFuZGxlciBmcm9tIFwiLi9oYW5kbGVycy9Sb29tU2V0dGluZ3NIYW5kbGVyXCI7XHJcbmltcG9ydCBDb25maWdTZXR0aW5nc0hhbmRsZXIgZnJvbSBcIi4vaGFuZGxlcnMvQ29uZmlnU2V0dGluZ3NIYW5kbGVyXCI7XHJcbmltcG9ydCB7X3R9IGZyb20gJy4uL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSBcIi4uL1Nka0NvbmZpZ1wiO1xyXG5pbXBvcnQgZGlzIGZyb20gJy4uL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQge1NFVFRJTkdTfSBmcm9tIFwiLi9TZXR0aW5nc1wiO1xyXG5pbXBvcnQgTG9jYWxFY2hvV3JhcHBlciBmcm9tIFwiLi9oYW5kbGVycy9Mb2NhbEVjaG9XcmFwcGVyXCI7XHJcbmltcG9ydCB7V2F0Y2hNYW5hZ2VyfSBmcm9tIFwiLi9XYXRjaE1hbmFnZXJcIjtcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIHRoZSB2YXJpb3VzIHNldHRpbmcgbGV2ZWxzIHN1cHBvcnRlZCBieSB0aGUgU2V0dGluZ3NTdG9yZS5cclxuICovXHJcbmV4cG9ydCBjb25zdCBTZXR0aW5nTGV2ZWwgPSB7XHJcbiAgICAvLyBOb3RlOiBUaGlzIGVudW0gaXMgbm90IHVzZWQgaW4gdGhpcyBjbGFzcyBvciBpbiB0aGUgU2V0dGluZ3MgZmlsZVxyXG4gICAgLy8gVGhpcyBzaG91bGQgYWx3YXlzIGJlIHVzZWQgZWxzZXdoZXJlIGluIHRoZSBwcm9qZWN0LlxyXG4gICAgREVWSUNFOiBcImRldmljZVwiLFxyXG4gICAgUk9PTV9ERVZJQ0U6IFwicm9vbS1kZXZpY2VcIixcclxuICAgIFJPT01fQUNDT1VOVDogXCJyb29tLWFjY291bnRcIixcclxuICAgIEFDQ09VTlQ6IFwiYWNjb3VudFwiLFxyXG4gICAgUk9PTTogXCJyb29tXCIsXHJcbiAgICBDT05GSUc6IFwiY29uZmlnXCIsXHJcbiAgICBERUZBVUxUOiBcImRlZmF1bHRcIixcclxufTtcclxuXHJcbmNvbnN0IGRlZmF1bHRXYXRjaE1hbmFnZXIgPSBuZXcgV2F0Y2hNYW5hZ2VyKCk7XHJcblxyXG4vLyBDb252ZXJ0IHRoZSBzZXR0aW5ncyB0byBlYXNpZXIgdG8gbWFuYWdlIG9iamVjdHMgZm9yIHRoZSBoYW5kbGVyc1xyXG5jb25zdCBkZWZhdWx0U2V0dGluZ3MgPSB7fTtcclxuY29uc3QgaW52ZXJ0ZWREZWZhdWx0U2V0dGluZ3MgPSB7fTtcclxuY29uc3QgZmVhdHVyZU5hbWVzID0gW107XHJcbmZvciAoY29uc3Qga2V5IG9mIE9iamVjdC5rZXlzKFNFVFRJTkdTKSkge1xyXG4gICAgZGVmYXVsdFNldHRpbmdzW2tleV0gPSBTRVRUSU5HU1trZXldLmRlZmF1bHQ7XHJcbiAgICBpZiAoU0VUVElOR1Nba2V5XS5pc0ZlYXR1cmUpIGZlYXR1cmVOYW1lcy5wdXNoKGtleSk7XHJcbiAgICBpZiAoU0VUVElOR1Nba2V5XS5pbnZlcnRlZFNldHRpbmdOYW1lKSB7XHJcbiAgICAgICAgLy8gSW52ZXJ0IG5vdyBzbyB0aGF0IHRoZSByZXN0IG9mIHRoZSBzeXN0ZW0gd2lsbCBpbnZlcnQgaXQgYmFja1xyXG4gICAgICAgIC8vIHRvIHdoYXQgd2FzIGludGVuZGVkLlxyXG4gICAgICAgIGludmVydGVkRGVmYXVsdFNldHRpbmdzW2tleV0gPSAhU0VUVElOR1Nba2V5XS5kZWZhdWx0O1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBMRVZFTF9IQU5ETEVSUyA9IHtcclxuICAgIFwiZGV2aWNlXCI6IG5ldyBEZXZpY2VTZXR0aW5nc0hhbmRsZXIoZmVhdHVyZU5hbWVzLCBkZWZhdWx0V2F0Y2hNYW5hZ2VyKSxcclxuICAgIFwicm9vbS1kZXZpY2VcIjogbmV3IFJvb21EZXZpY2VTZXR0aW5nc0hhbmRsZXIoZGVmYXVsdFdhdGNoTWFuYWdlciksXHJcbiAgICBcInJvb20tYWNjb3VudFwiOiBuZXcgUm9vbUFjY291bnRTZXR0aW5nc0hhbmRsZXIoZGVmYXVsdFdhdGNoTWFuYWdlciksXHJcbiAgICBcImFjY291bnRcIjogbmV3IEFjY291bnRTZXR0aW5nc0hhbmRsZXIoZGVmYXVsdFdhdGNoTWFuYWdlciksXHJcbiAgICBcInJvb21cIjogbmV3IFJvb21TZXR0aW5nc0hhbmRsZXIoZGVmYXVsdFdhdGNoTWFuYWdlciksXHJcbiAgICBcImNvbmZpZ1wiOiBuZXcgQ29uZmlnU2V0dGluZ3NIYW5kbGVyKCksXHJcbiAgICBcImRlZmF1bHRcIjogbmV3IERlZmF1bHRTZXR0aW5nc0hhbmRsZXIoZGVmYXVsdFNldHRpbmdzLCBpbnZlcnRlZERlZmF1bHRTZXR0aW5ncyksXHJcbn07XHJcblxyXG4vLyBXcmFwIGFsbCB0aGUgaGFuZGxlcnMgd2l0aCBsb2NhbCBlY2hvXHJcbmZvciAoY29uc3Qga2V5IG9mIE9iamVjdC5rZXlzKExFVkVMX0hBTkRMRVJTKSkge1xyXG4gICAgTEVWRUxfSEFORExFUlNba2V5XSA9IG5ldyBMb2NhbEVjaG9XcmFwcGVyKExFVkVMX0hBTkRMRVJTW2tleV0pO1xyXG59XHJcblxyXG5jb25zdCBMRVZFTF9PUkRFUiA9IFtcclxuICAgICdkZXZpY2UnLCAncm9vbS1kZXZpY2UnLCAncm9vbS1hY2NvdW50JywgJ2FjY291bnQnLCAncm9vbScsICdjb25maWcnLCAnZGVmYXVsdCcsXHJcbl07XHJcblxyXG4vKipcclxuICogQ29udHJvbHMgYW5kIG1hbmFnZXMgYXBwbGljYXRpb24gc2V0dGluZ3MgYnkgcHJvdmlkaW5nIHZhcnlpbmcgbGV2ZWxzIGF0IHdoaWNoIHRoZVxyXG4gKiBzZXR0aW5nIHZhbHVlIG1heSBiZSBzcGVjaWZpZWQuIFRoZSBsZXZlbHMgYXJlIHRoZW4gdXNlZCB0byBkZXRlcm1pbmUgd2hhdCB0aGUgc2V0dGluZ1xyXG4gKiB2YWx1ZSBzaG91bGQgYmUgZ2l2ZW4gYSBzZXQgb2YgY2lyY3Vtc3RhbmNlcy4gVGhlIGxldmVscywgaW4gcHJpb3JpdHkgb3JkZXIsIGFyZTpcclxuICogLSBcImRldmljZVwiICAgICAgICAgLSBWYWx1ZXMgYXJlIGRldGVybWluZWQgYnkgdGhlIGN1cnJlbnQgZGV2aWNlXHJcbiAqIC0gXCJyb29tLWRldmljZVwiICAgIC0gVmFsdWVzIGFyZSBkZXRlcm1pbmVkIGJ5IHRoZSBjdXJyZW50IGRldmljZSBmb3IgYSBwYXJ0aWN1bGFyIHJvb21cclxuICogLSBcInJvb20tYWNjb3VudFwiICAgLSBWYWx1ZXMgYXJlIGRldGVybWluZWQgYnkgdGhlIGN1cnJlbnQgYWNjb3VudCBmb3IgYSBwYXJ0aWN1bGFyIHJvb21cclxuICogLSBcImFjY291bnRcIiAgICAgICAgLSBWYWx1ZXMgYXJlIGRldGVybWluZWQgYnkgdGhlIGN1cnJlbnQgYWNjb3VudFxyXG4gKiAtIFwicm9vbVwiICAgICAgICAgICAtIFZhbHVlcyBhcmUgZGV0ZXJtaW5lZCBieSBhIHBhcnRpY3VsYXIgcm9vbSAoYnkgdGhlIHJvb20gYWRtaW5zKVxyXG4gKiAtIFwiY29uZmlnXCIgICAgICAgICAtIFZhbHVlcyBhcmUgZGV0ZXJtaW5lZCBieSB0aGUgY29uZmlnLmpzb25cclxuICogLSBcImRlZmF1bHRcIiAgICAgICAgLSBWYWx1ZXMgYXJlIGRldGVybWluZWQgYnkgdGhlIGhhcmRjb2RlZCBkZWZhdWx0c1xyXG4gKlxyXG4gKiBFYWNoIGxldmVsIGhhcyBhIGRpZmZlcmVudCBtZXRob2QgdG8gc3RvcmluZyB0aGUgc2V0dGluZyB2YWx1ZS4gRm9yIGltcGxlbWVudGF0aW9uXHJcbiAqIHNwZWNpZmljIGRldGFpbHMsIHBsZWFzZSBzZWUgdGhlIGhhbmRsZXJzLiBUaGUgXCJjb25maWdcIiBhbmQgXCJkZWZhdWx0XCIgbGV2ZWxzIGFyZVxyXG4gKiBib3RoIGFsd2F5cyBzdXBwb3J0ZWQgb24gYWxsIHBsYXRmb3Jtcy4gQWxsIG90aGVyIHNldHRpbmdzIHNob3VsZCBiZSBndWFyZGVkIGJ5XHJcbiAqIGlzTGV2ZWxTdXBwb3J0ZWQoKSBwcmlvciB0byBhdHRlbXB0aW5nIHRvIHNldCB0aGUgdmFsdWUuXHJcbiAqXHJcbiAqIFNldHRpbmdzIGNhbiBhbHNvIHJlcHJlc2VudCBmZWF0dXJlcy4gRmVhdHVyZXMgYXJlIHNpZ25pZmljYW50IHBvcnRpb25zIG9mIHRoZVxyXG4gKiBhcHBsaWNhdGlvbiB0aGF0IHdhcnJhbnQgYSBkZWRpY2F0ZWQgc2V0dGluZyB0byB0b2dnbGUgdGhlbSBvbiBvciBvZmYuIEZlYXR1cmVzIGFyZVxyXG4gKiBzcGVjaWFsLWNhc2VkIHRvIGVuc3VyZSB0aGF0IHRoZWlyIHZhbHVlcyByZXNwZWN0IHRoZSBjb25maWd1cmF0aW9uIChmb3IgZXhhbXBsZSwgYVxyXG4gKiBmZWF0dXJlIG1heSBiZSByZXBvcnRlZCBhcyBkaXNhYmxlZCBldmVuIHRob3VnaCBhIHVzZXIgaGFzIHNwZWNpZmljYWxseSByZXF1ZXN0ZWQgaXRcclxuICogYmUgZW5hYmxlZCkuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZXR0aW5nc1N0b3JlIHtcclxuICAgIC8vIFdlIHN1cHBvcnQgd2F0Y2hpbmcgc2V0dGluZ3MgZm9yIGNoYW5nZXMsIGFuZCBkbyB0aGlzIGJ5IHRyYWNraW5nIHdoaWNoIGNhbGxiYWNrcyBoYXZlXHJcbiAgICAvLyBiZWVuIGdpdmVuIHRvIHVzLiBXZSBlbmQgdXAgcmV0dXJuaW5nIHRoZSBjYWxsYmFja1JlZiB0byB0aGUgY2FsbGVyIHNvIHRoZXkgY2FuIHVuc3Vic2NyaWJlXHJcbiAgICAvLyBhdCBhIGxhdGVyIHBvaW50LlxyXG4gICAgLy9cclxuICAgIC8vIFdlIGFsc28gbWFpbnRhaW4gYSBsaXN0IG9mIG1vbml0b3JzIHdoaWNoIGFyZSBzcGVjaWFsIHdhdGNoZXJzOiB0aGV5IGNhdXNlIGRpc3BhdGNoZXNcclxuICAgIC8vIHdoZW4gdGhlIHNldHRpbmcgY2hhbmdlcy4gV2UgdHJhY2sgd2hpY2ggcm9vbXMgd2UncmUgbW9uaXRvcmluZyB0aG91Z2ggdG8gZW5zdXJlIHdlXHJcbiAgICAvLyBkb24ndCBkdXBsaWNhdGUgdXBkYXRlcyBvbiB0aGUgYnVzLlxyXG4gICAgc3RhdGljIF93YXRjaGVycyA9IHt9OyAvLyB7IGNhbGxiYWNrUmVmID0+IHsgY2FsbGJhY2tGbiB9IH1cclxuICAgIHN0YXRpYyBfbW9uaXRvcnMgPSB7fTsgLy8geyBzZXR0aW5nTmFtZSA9PiB7IHJvb21JZCA9PiBjYWxsYmFja1JlZiB9IH1cclxuXHJcbiAgICAvLyBDb3VudGVyIHVzZWQgZm9yIGdlbmVyYXRpb24gb2Ygd2F0Y2hlciBJRHNcclxuICAgIHN0YXRpYyBfd2F0Y2hlckNvdW50ID0gMTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFdhdGNoZXMgZm9yIGNoYW5nZXMgaW4gYSBwYXJ0aWN1bGFyIHNldHRpbmcuIFRoaXMgaXMgZG9uZSB3aXRob3V0IGFueSBsb2NhbCBlY2hvXHJcbiAgICAgKiB3cmFwcGluZyBhbmQgZmlyZXMgd2hlbmV2ZXIgYSBjaGFuZ2UgaXMgZGV0ZWN0ZWQgaW4gYSBzZXR0aW5nJ3MgdmFsdWUsIGF0IGFueSBsZXZlbC5cclxuICAgICAqIFdhdGNoaW5nIGlzIGludGVuZGVkIHRvIGJlIHVzZWQgaW4gc2NlbmFyaW9zIHdoZXJlIHRoZSBhcHAgbmVlZHMgdG8gcmVhY3QgdG8gY2hhbmdlc1xyXG4gICAgICogbWFkZSBieSBvdGhlciBkZXZpY2VzLiBJdCBpcyBvdGhlcndpc2UgZXhwZWN0ZWQgdGhhdCBjYWxsZXJzIHdpbGwgYmUgYWJsZSB0byB1c2UgdGhlXHJcbiAgICAgKiBDb250cm9sbGVyIHN5c3RlbSBvciB0cmFjayB0aGVpciBvd24gY2hhbmdlcyB0byBzZXR0aW5ncy4gQ2FsbGVycyBzaG91bGQgcmV0YWluIHRoZVxyXG4gICAgICogcmV0dXJuZWQgcmVmZXJlbmNlIHRvIGxhdGVyIHVuc3Vic2NyaWJlIGZyb20gdXBkYXRlcy5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzZXR0aW5nTmFtZSBUaGUgc2V0dGluZyBuYW1lIHRvIHdhdGNoXHJcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSByb29tIElEIHRvIHdhdGNoIGZvciBjaGFuZ2VzIGluLiBNYXkgYmUgbnVsbCBmb3IgJ2FsbCcuXHJcbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBjYWxsYmFja0ZuIEEgZnVuY3Rpb24gdG8gYmUgY2FsbGVkIHdoZW4gYSBzZXR0aW5nIGNoYW5nZSBpc1xyXG4gICAgICogZGV0ZWN0ZWQuIEZpdmUgYXJndW1lbnRzIGNhbiBiZSBleHBlY3RlZDogdGhlIHNldHRpbmcgbmFtZSwgdGhlIHJvb20gSUQgKG1heSBiZSBudWxsKSxcclxuICAgICAqIHRoZSBsZXZlbCB0aGUgY2hhbmdlIGhhcHBlbmVkIGF0LCB0aGUgbmV3IHZhbHVlIGF0IHRoZSBnaXZlbiBsZXZlbCwgYW5kIGZpbmFsbHkgdGhlIG5ld1xyXG4gICAgICogdmFsdWUgZm9yIHRoZSBzZXR0aW5nIHJlZ2FyZGxlc3Mgb2YgbGV2ZWwuIFRoZSBjYWxsYmFjayBpcyByZXNwb25zaWJsZSBmb3IgZGV0ZXJtaW5pbmdcclxuICAgICAqIGlmIHRoZSBjaGFuZ2UgaW4gdmFsdWUgaXMgd29ydGh3aGlsZSBlbm91Z2ggdG8gcmVhY3QgdXBvbi5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IEEgcmVmZXJlbmNlIHRvIHRoZSB3YXRjaGVyIHRoYXQgd2FzIGVtcGxveWVkLlxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgd2F0Y2hTZXR0aW5nKHNldHRpbmdOYW1lLCByb29tSWQsIGNhbGxiYWNrRm4pIHtcclxuICAgICAgICBjb25zdCBzZXR0aW5nID0gU0VUVElOR1Nbc2V0dGluZ05hbWVdO1xyXG4gICAgICAgIGNvbnN0IG9yaWdpbmFsU2V0dGluZ05hbWUgPSBzZXR0aW5nTmFtZTtcclxuICAgICAgICBpZiAoIXNldHRpbmcpIHRocm93IG5ldyBFcnJvcihgJHtzZXR0aW5nTmFtZX0gaXMgbm90IGEgc2V0dGluZ2ApO1xyXG5cclxuICAgICAgICBpZiAoc2V0dGluZy5pbnZlcnRlZFNldHRpbmdOYW1lKSB7XHJcbiAgICAgICAgICAgIHNldHRpbmdOYW1lID0gc2V0dGluZy5pbnZlcnRlZFNldHRpbmdOYW1lO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgd2F0Y2hlcklkID0gYCR7bmV3IERhdGUoKS5nZXRUaW1lKCl9XyR7U2V0dGluZ3NTdG9yZS5fd2F0Y2hlckNvdW50Kyt9XyR7c2V0dGluZ05hbWV9XyR7cm9vbUlkfWA7XHJcblxyXG4gICAgICAgIGNvbnN0IGxvY2FsaXplZENhbGxiYWNrID0gKGNoYW5nZWRJblJvb21JZCwgYXRMZXZlbCwgbmV3VmFsQXRMZXZlbCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBuZXdWYWx1ZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUob3JpZ2luYWxTZXR0aW5nTmFtZSk7XHJcbiAgICAgICAgICAgIGNhbGxiYWNrRm4ob3JpZ2luYWxTZXR0aW5nTmFtZSwgY2hhbmdlZEluUm9vbUlkLCBhdExldmVsLCBuZXdWYWxBdExldmVsLCBuZXdWYWx1ZSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coYFN0YXJ0aW5nIHdhdGNoZXIgZm9yICR7c2V0dGluZ05hbWV9QCR7cm9vbUlkIHx8ICc8bnVsbCByb29tPid9IGFzIElEICR7d2F0Y2hlcklkfWApO1xyXG4gICAgICAgIFNldHRpbmdzU3RvcmUuX3dhdGNoZXJzW3dhdGNoZXJJZF0gPSBsb2NhbGl6ZWRDYWxsYmFjaztcclxuICAgICAgICBkZWZhdWx0V2F0Y2hNYW5hZ2VyLndhdGNoU2V0dGluZyhzZXR0aW5nTmFtZSwgcm9vbUlkLCBsb2NhbGl6ZWRDYWxsYmFjayk7XHJcblxyXG4gICAgICAgIHJldHVybiB3YXRjaGVySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdG9wcyB0aGUgU2V0dGluZ3NTdG9yZSBmcm9tIHdhdGNoaW5nIGEgc2V0dGluZy4gVGhpcyBpcyBhIG5vLW9wIGlmIHRoZSB3YXRjaGVyXHJcbiAgICAgKiBwcm92aWRlZCBpcyBub3QgZm91bmQuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gd2F0Y2hlclJlZmVyZW5jZSBUaGUgd2F0Y2hlciByZWZlcmVuY2UgKHJlY2VpdmVkIGZyb20gI3dhdGNoU2V0dGluZylcclxuICAgICAqIHRvIGNhbmNlbC5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIHVud2F0Y2hTZXR0aW5nKHdhdGNoZXJSZWZlcmVuY2UpIHtcclxuICAgICAgICBpZiAoIVNldHRpbmdzU3RvcmUuX3dhdGNoZXJzW3dhdGNoZXJSZWZlcmVuY2VdKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgRW5kaW5nIG5vbi1leGlzdGVudCB3YXRjaGVyIElEICR7d2F0Y2hlclJlZmVyZW5jZX1gKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coYEVuZGluZyB3YXRjaGVyIElEICR7d2F0Y2hlclJlZmVyZW5jZX1gKTtcclxuICAgICAgICBkZWZhdWx0V2F0Y2hNYW5hZ2VyLnVud2F0Y2hTZXR0aW5nKFNldHRpbmdzU3RvcmUuX3dhdGNoZXJzW3dhdGNoZXJSZWZlcmVuY2VdKTtcclxuICAgICAgICBkZWxldGUgU2V0dGluZ3NTdG9yZS5fd2F0Y2hlcnNbd2F0Y2hlclJlZmVyZW5jZV07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIHVwIGEgbW9uaXRvciBmb3IgYSBzZXR0aW5nLiBUaGlzIGJlaGF2ZXMgc2ltaWxhciB0byAjd2F0Y2hTZXR0aW5nIGV4Y2VwdCBpbnN0ZWFkXHJcbiAgICAgKiBvZiBtYWtpbmcgYSBjYWxsIHRvIGEgY2FsbGJhY2ssIGl0IGZvcndhcmRzIGFsbCBjaGFuZ2VzIHRvIHRoZSBkaXNwYXRjaGVyLiBDYWxsZXJzIGNhblxyXG4gICAgICogZXhwZWN0IHRvIGxpc3RlbiBmb3IgdGhlICdzZXR0aW5nX3VwZGF0ZWQnIGFjdGlvbiB3aXRoIGFuIG9iamVjdCBjb250YWluaW5nIHNldHRpbmdOYW1lLFxyXG4gICAgICogcm9vbUlkLCBsZXZlbCwgbmV3VmFsdWVBdExldmVsLCBhbmQgbmV3VmFsdWUuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2V0dGluZ05hbWUgVGhlIHNldHRpbmcgbmFtZSB0byBtb25pdG9yLlxyXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgcm9vbSBJRCB0byBtb25pdG9yIGZvciBjaGFuZ2VzIGluLiBVc2UgbnVsbCBmb3IgYWxsIHJvb21zLlxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgbW9uaXRvclNldHRpbmcoc2V0dGluZ05hbWUsIHJvb21JZCkge1xyXG4gICAgICAgIGlmICghdGhpcy5fbW9uaXRvcnNbc2V0dGluZ05hbWVdKSB0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV0gPSB7fTtcclxuXHJcbiAgICAgICAgY29uc3QgcmVnaXN0ZXJXYXRjaGVyID0gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV1bcm9vbUlkXSA9IFNldHRpbmdzU3RvcmUud2F0Y2hTZXR0aW5nKFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ05hbWUsIHJvb21JZCwgKHNldHRpbmdOYW1lLCBpblJvb21JZCwgbGV2ZWwsIG5ld1ZhbHVlQXRMZXZlbCwgbmV3VmFsdWUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdzZXR0aW5nX3VwZGF0ZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXR0aW5nTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbUlkOiBpblJvb21JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV2ZWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld1ZhbHVlQXRMZXZlbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3VmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGhhc1Jvb20gPSBPYmplY3Qua2V5cyh0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV0pLmZpbmQoKHIpID0+IHIgPT09IHJvb21JZCB8fCByID09PSBudWxsKTtcclxuICAgICAgICBpZiAoIWhhc1Jvb20pIHtcclxuICAgICAgICAgICAgcmVnaXN0ZXJXYXRjaGVyKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHJvb21JZCA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgLy8gVW5yZWdpc3RlciBhbGwgZXhpc3Rpbmcgd2F0Y2hlcnMgYW5kIHJlZ2lzdGVyIHRoZSBuZXcgb25lXHJcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHJvb21JZCBvZiBPYmplY3Qua2V5cyh0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgU2V0dGluZ3NTdG9yZS51bndhdGNoU2V0dGluZyh0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV1bcm9vbUlkXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tb25pdG9yc1tzZXR0aW5nTmFtZV0gPSB7fTtcclxuICAgICAgICAgICAgICAgIHJlZ2lzdGVyV2F0Y2hlcigpO1xyXG4gICAgICAgICAgICB9IC8vIGVsc2UgYSB3YXRjaGVyIGlzIGFscmVhZHkgcmVnaXN0ZXJlZCBmb3IgdGhlIHJvb20sIHNvIGRvbid0IGJvdGhlciByZWdpc3RlcmluZyBpdCBhZ2FpblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHRyYW5zbGF0ZWQgZGlzcGxheSBuYW1lIGZvciBhIGdpdmVuIHNldHRpbmdcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzZXR0aW5nTmFtZSBUaGUgc2V0dGluZyB0byBsb29rIHVwLlxyXG4gICAgICogQHBhcmFtIHtcImRldmljZVwifFwicm9vbS1kZXZpY2VcInxcInJvb20tYWNjb3VudFwifFwiYWNjb3VudFwifFwicm9vbVwifFwiY29uZmlnXCJ8XCJkZWZhdWx0XCJ9IGF0TGV2ZWxcclxuICAgICAqIFRoZSBsZXZlbCB0byBnZXQgdGhlIGRpc3BsYXkgbmFtZSBmb3I7IERlZmF1bHRzIHRvICdkZWZhdWx0Jy5cclxuICAgICAqIEByZXR1cm4ge1N0cmluZ30gVGhlIGRpc3BsYXkgbmFtZSBmb3IgdGhlIHNldHRpbmcsIG9yIG51bGwgaWYgbm90IGZvdW5kLlxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0RGlzcGxheU5hbWUoc2V0dGluZ05hbWUsIGF0TGV2ZWwgPSBcImRlZmF1bHRcIikge1xyXG4gICAgICAgIGlmICghU0VUVElOR1Nbc2V0dGluZ05hbWVdIHx8ICFTRVRUSU5HU1tzZXR0aW5nTmFtZV0uZGlzcGxheU5hbWUpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBsZXQgZGlzcGxheU5hbWUgPSBTRVRUSU5HU1tzZXR0aW5nTmFtZV0uZGlzcGxheU5hbWU7XHJcbiAgICAgICAgaWYgKGRpc3BsYXlOYW1lIGluc3RhbmNlb2YgT2JqZWN0KSB7XHJcbiAgICAgICAgICAgIGlmIChkaXNwbGF5TmFtZVthdExldmVsXSkgZGlzcGxheU5hbWUgPSBkaXNwbGF5TmFtZVthdExldmVsXTtcclxuICAgICAgICAgICAgZWxzZSBkaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lW1wiZGVmYXVsdFwiXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBfdChkaXNwbGF5TmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGEgbGlzdCBvZiBhbGwgYXZhaWxhYmxlIGxhYnMgZmVhdHVyZSBuYW1lc1xyXG4gICAgICogQHJldHVybnMge3N0cmluZ1tdfSBUaGUgbGlzdCBvZiBhdmFpbGFibGUgZmVhdHVyZSBuYW1lc1xyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0TGFic0ZlYXR1cmVzKCkge1xyXG4gICAgICAgIGNvbnN0IHBvc3NpYmxlRmVhdHVyZXMgPSBPYmplY3Qua2V5cyhTRVRUSU5HUykuZmlsdGVyKChzKSA9PiBTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZShzKSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGVuYWJsZUxhYnMgPSBTZGtDb25maWcuZ2V0KClbXCJlbmFibGVMYWJzXCJdO1xyXG4gICAgICAgIGlmIChlbmFibGVMYWJzKSByZXR1cm4gcG9zc2libGVGZWF0dXJlcztcclxuXHJcbiAgICAgICAgcmV0dXJuIHBvc3NpYmxlRmVhdHVyZXMuZmlsdGVyKChzKSA9PiBTZXR0aW5nc1N0b3JlLl9nZXRGZWF0dXJlU3RhdGUocykgPT09IFwibGFic1wiKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZXMgaWYgYSBzZXR0aW5nIGlzIGFsc28gYSBmZWF0dXJlLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNldHRpbmdOYW1lIFRoZSBzZXR0aW5nIHRvIGxvb2sgdXAuXHJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIHRoZSBzZXR0aW5nIGlzIGEgZmVhdHVyZS5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIGlzRmVhdHVyZShzZXR0aW5nTmFtZSkge1xyXG4gICAgICAgIGlmICghU0VUVElOR1Nbc2V0dGluZ05hbWVdKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuIFNFVFRJTkdTW3NldHRpbmdOYW1lXS5pc0ZlYXR1cmU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmVzIGlmIGEgZ2l2ZW4gZmVhdHVyZSBpcyBlbmFibGVkLiBUaGUgZmVhdHVyZSBnaXZlbiBtdXN0IGJlIGEga25vd25cclxuICAgICAqIGZlYXR1cmUuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2V0dGluZ05hbWUgVGhlIG5hbWUgb2YgdGhlIHNldHRpbmcgdGhhdCBpcyBhIGZlYXR1cmUuXHJcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSBvcHRpb25hbCByb29tIElEIHRvIHZhbGlkYXRlIGluLCBtYXkgYmUgbnVsbC5cclxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59IFRydWUgaWYgdGhlIGZlYXR1cmUgaXMgZW5hYmxlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBpc0ZlYXR1cmVFbmFibGVkKHNldHRpbmdOYW1lLCByb29tSWQgPSBudWxsKSB7XHJcbiAgICAgICAgaWYgKCFTZXR0aW5nc1N0b3JlLmlzRmVhdHVyZShzZXR0aW5nTmFtZSkpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiU2V0dGluZyBcIiArIHNldHRpbmdOYW1lICsgXCIgaXMgbm90IGEgZmVhdHVyZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyBhIGZlYXR1cmUgYXMgZW5hYmxlZCBvciBkaXNhYmxlZCBvbiB0aGUgY3VycmVudCBkZXZpY2UuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2V0dGluZ05hbWUgVGhlIG5hbWUgb2YgdGhlIHNldHRpbmcuXHJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IHZhbHVlIFRydWUgdG8gZW5hYmxlIHRoZSBmZWF0dXJlLCBmYWxzZSBvdGhlcndpc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX0gUmVzb2x2ZXMgd2hlbiB0aGUgc2V0dGluZyBoYXMgYmVlbiBzZXQuXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBzZXRGZWF0dXJlRW5hYmxlZChzZXR0aW5nTmFtZSwgdmFsdWUpIHtcclxuICAgICAgICAvLyBWZXJpZnkgdGhhdCB0aGUgc2V0dGluZyBpcyBhY3R1YWxseSBhIHNldHRpbmdcclxuICAgICAgICBpZiAoIVNFVFRJTkdTW3NldHRpbmdOYW1lXSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJTZXR0aW5nICdcIiArIHNldHRpbmdOYW1lICsgXCInIGRvZXMgbm90IGFwcGVhciB0byBiZSBhIHNldHRpbmcuXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIVNldHRpbmdzU3RvcmUuaXNGZWF0dXJlKHNldHRpbmdOYW1lKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJTZXR0aW5nIFwiICsgc2V0dGluZ05hbWUgKyBcIiBpcyBub3QgYSBmZWF0dXJlXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIFNldHRpbmdzU3RvcmUuc2V0VmFsdWUoc2V0dGluZ05hbWUsIG51bGwsIFwiZGV2aWNlXCIsIHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHZhbHVlIG9mIGEgc2V0dGluZy4gVGhlIHJvb20gSUQgaXMgb3B0aW9uYWwgaWYgdGhlIHNldHRpbmcgaXMgbm90IHRvXHJcbiAgICAgKiBiZSBhcHBsaWVkIHRvIGFueSBwYXJ0aWN1bGFyIHJvb20sIG90aGVyd2lzZSBpdCBzaG91bGQgYmUgc3VwcGxpZWQuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2V0dGluZ05hbWUgVGhlIG5hbWUgb2YgdGhlIHNldHRpbmcgdG8gcmVhZCB0aGUgdmFsdWUgb2YuXHJcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSByb29tIElEIHRvIHJlYWQgdGhlIHNldHRpbmcgdmFsdWUgaW4sIG1heSBiZSBudWxsLlxyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBleGNsdWRlRGVmYXVsdCBUcnVlIHRvIGRpc2FibGUgdXNpbmcgdGhlIGRlZmF1bHQgdmFsdWUuXHJcbiAgICAgKiBAcmV0dXJuIHsqfSBUaGUgdmFsdWUsIG9yIG51bGwgaWYgbm90IGZvdW5kXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBnZXRWYWx1ZShzZXR0aW5nTmFtZSwgcm9vbUlkID0gbnVsbCwgZXhjbHVkZURlZmF1bHQgPSBmYWxzZSkge1xyXG4gICAgICAgIC8vIFZlcmlmeSB0aGF0IHRoZSBzZXR0aW5nIGlzIGFjdHVhbGx5IGEgc2V0dGluZ1xyXG4gICAgICAgIGlmICghU0VUVElOR1Nbc2V0dGluZ05hbWVdKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlNldHRpbmcgJ1wiICsgc2V0dGluZ05hbWUgKyBcIicgZG9lcyBub3QgYXBwZWFyIHRvIGJlIGEgc2V0dGluZy5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzZXR0aW5nID0gU0VUVElOR1Nbc2V0dGluZ05hbWVdO1xyXG4gICAgICAgIGNvbnN0IGxldmVsT3JkZXIgPSAoc2V0dGluZy5zdXBwb3J0ZWRMZXZlbHNBcmVPcmRlcmVkID8gc2V0dGluZy5zdXBwb3J0ZWRMZXZlbHMgOiBMRVZFTF9PUkRFUik7XHJcblxyXG4gICAgICAgIHJldHVybiBTZXR0aW5nc1N0b3JlLmdldFZhbHVlQXQobGV2ZWxPcmRlclswXSwgc2V0dGluZ05hbWUsIHJvb21JZCwgZmFsc2UsIGV4Y2x1ZGVEZWZhdWx0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBzZXR0aW5nJ3MgdmFsdWUgYXQgYSBwYXJ0aWN1bGFyIGxldmVsLCBpZ25vcmluZyBhbGwgbGV2ZWxzIHRoYXQgYXJlIG1vcmUgc3BlY2lmaWMuXHJcbiAgICAgKiBAcGFyYW0ge1wiZGV2aWNlXCJ8XCJyb29tLWRldmljZVwifFwicm9vbS1hY2NvdW50XCJ8XCJhY2NvdW50XCJ8XCJyb29tXCJ8XCJjb25maWdcInxcImRlZmF1bHRcIn0gbGV2ZWwgVGhlXHJcbiAgICAgKiBsZXZlbCB0byBsb29rIGF0LlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNldHRpbmdOYW1lIFRoZSBuYW1lIG9mIHRoZSBzZXR0aW5nIHRvIHJlYWQuXHJcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSByb29tIElEIHRvIHJlYWQgdGhlIHNldHRpbmcgdmFsdWUgaW4sIG1heSBiZSBudWxsLlxyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBleHBsaWNpdCBJZiB0cnVlLCB0aGlzIG1ldGhvZCB3aWxsIG5vdCBjb25zaWRlciBvdGhlciBsZXZlbHMsIGp1c3QgdGhlIG9uZVxyXG4gICAgICogcHJvdmlkZWQuIERlZmF1bHRzIHRvIGZhbHNlLlxyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBleGNsdWRlRGVmYXVsdCBUcnVlIHRvIGRpc2FibGUgdXNpbmcgdGhlIGRlZmF1bHQgdmFsdWUuXHJcbiAgICAgKiBAcmV0dXJuIHsqfSBUaGUgdmFsdWUsIG9yIG51bGwgaWYgbm90IGZvdW5kLlxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0VmFsdWVBdChsZXZlbCwgc2V0dGluZ05hbWUsIHJvb21JZCA9IG51bGwsIGV4cGxpY2l0ID0gZmFsc2UsIGV4Y2x1ZGVEZWZhdWx0ID0gZmFsc2UpIHtcclxuICAgICAgICAvLyBWZXJpZnkgdGhhdCB0aGUgc2V0dGluZyBpcyBhY3R1YWxseSBhIHNldHRpbmdcclxuICAgICAgICBjb25zdCBzZXR0aW5nID0gU0VUVElOR1Nbc2V0dGluZ05hbWVdO1xyXG4gICAgICAgIGlmICghc2V0dGluZykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJTZXR0aW5nICdcIiArIHNldHRpbmdOYW1lICsgXCInIGRvZXMgbm90IGFwcGVhciB0byBiZSBhIHNldHRpbmcuXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbGV2ZWxPcmRlciA9IChzZXR0aW5nLnN1cHBvcnRlZExldmVsc0FyZU9yZGVyZWQgPyBzZXR0aW5nLnN1cHBvcnRlZExldmVscyA6IExFVkVMX09SREVSKTtcclxuICAgICAgICBpZiAoIWxldmVsT3JkZXIuaW5jbHVkZXMoXCJkZWZhdWx0XCIpKSBsZXZlbE9yZGVyLnB1c2goXCJkZWZhdWx0XCIpOyAvLyBhbHdheXMgaW5jbHVkZSBkZWZhdWx0XHJcblxyXG4gICAgICAgIGNvbnN0IG1pbkluZGV4ID0gbGV2ZWxPcmRlci5pbmRleE9mKGxldmVsKTtcclxuICAgICAgICBpZiAobWluSW5kZXggPT09IC0xKSB0aHJvdyBuZXcgRXJyb3IoXCJMZXZlbCBcIiArIGxldmVsICsgXCIgaXMgbm90IHByaW9yaXRpemVkXCIpO1xyXG5cclxuICAgICAgICBpZiAoU2V0dGluZ3NTdG9yZS5pc0ZlYXR1cmUoc2V0dGluZ05hbWUpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbmZpZ1ZhbHVlID0gU2V0dGluZ3NTdG9yZS5fZ2V0RmVhdHVyZVN0YXRlKHNldHRpbmdOYW1lKTtcclxuICAgICAgICAgICAgaWYgKGNvbmZpZ1ZhbHVlID09PSBcImVuYWJsZVwiKSByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgaWYgKGNvbmZpZ1ZhbHVlID09PSBcImRpc2FibGVcIikgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAvLyBlbHNlIGxldCBpdCBmYWxsIHRocm91Z2ggdGhlIGRlZmF1bHQgcHJvY2Vzc1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaGFuZGxlcnMgPSBTZXR0aW5nc1N0b3JlLl9nZXRIYW5kbGVycyhzZXR0aW5nTmFtZSk7XHJcblxyXG4gICAgICAgIC8vIENoZWNrIGlmIHdlIG5lZWQgdG8gaW52ZXJ0IHRoZSBzZXR0aW5nIGF0IGFsbC4gRG8gdGhpcyBhZnRlciB3ZSBnZXQgdGhlIHNldHRpbmdcclxuICAgICAgICAvLyBoYW5kbGVycyB0aG91Z2gsIG90aGVyd2lzZSB3ZSdsbCBmYWlsIHRvIHJlYWQgdGhlIHZhbHVlLlxyXG4gICAgICAgIGlmIChzZXR0aW5nLmludmVydGVkU2V0dGluZ05hbWUpIHtcclxuICAgICAgICAgICAgLy9jb25zb2xlLndhcm4oYEludmVydGluZyAke3NldHRpbmdOYW1lfSB0byBiZSAke3NldHRpbmcuaW52ZXJ0ZWRTZXR0aW5nTmFtZX0gLSBsZWdhY3kgc2V0dGluZ2ApO1xyXG4gICAgICAgICAgICBzZXR0aW5nTmFtZSA9IHNldHRpbmcuaW52ZXJ0ZWRTZXR0aW5nTmFtZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChleHBsaWNpdCkge1xyXG4gICAgICAgICAgICBjb25zdCBoYW5kbGVyID0gaGFuZGxlcnNbbGV2ZWxdO1xyXG4gICAgICAgICAgICBpZiAoIWhhbmRsZXIpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBTZXR0aW5nc1N0b3JlLl9nZXRGaW5hbFZhbHVlKHNldHRpbmcsIGxldmVsLCByb29tSWQsIG51bGwsIG51bGwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gaGFuZGxlci5nZXRWYWx1ZShzZXR0aW5nTmFtZSwgcm9vbUlkKTtcclxuICAgICAgICAgICAgcmV0dXJuIFNldHRpbmdzU3RvcmUuX2dldEZpbmFsVmFsdWUoc2V0dGluZywgbGV2ZWwsIHJvb21JZCwgdmFsdWUsIGxldmVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSBtaW5JbmRleDsgaSA8IGxldmVsT3JkZXIubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3QgaGFuZGxlciA9IGhhbmRsZXJzW2xldmVsT3JkZXJbaV1dO1xyXG4gICAgICAgICAgICBpZiAoIWhhbmRsZXIpIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICBpZiAoZXhjbHVkZURlZmF1bHQgJiYgbGV2ZWxPcmRlcltpXSA9PT0gXCJkZWZhdWx0XCIpIGNvbnRpbnVlO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBoYW5kbGVyLmdldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpO1xyXG4gICAgICAgICAgICBpZiAodmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCkgY29udGludWU7XHJcbiAgICAgICAgICAgIHJldHVybiBTZXR0aW5nc1N0b3JlLl9nZXRGaW5hbFZhbHVlKHNldHRpbmcsIGxldmVsLCByb29tSWQsIHZhbHVlLCBsZXZlbE9yZGVyW2ldKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBTZXR0aW5nc1N0b3JlLl9nZXRGaW5hbFZhbHVlKHNldHRpbmcsIGxldmVsLCByb29tSWQsIG51bGwsIG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBfZ2V0RmluYWxWYWx1ZShzZXR0aW5nLCBsZXZlbCwgcm9vbUlkLCBjYWxjdWxhdGVkVmFsdWUsIGNhbGN1bGF0ZWRBdExldmVsKSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdGluZ1ZhbHVlID0gY2FsY3VsYXRlZFZhbHVlO1xyXG5cclxuICAgICAgICBpZiAoc2V0dGluZy5jb250cm9sbGVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdHVhbFZhbHVlID0gc2V0dGluZy5jb250cm9sbGVyLmdldFZhbHVlT3ZlcnJpZGUobGV2ZWwsIHJvb21JZCwgY2FsY3VsYXRlZFZhbHVlLCBjYWxjdWxhdGVkQXRMZXZlbCk7XHJcbiAgICAgICAgICAgIGlmIChhY3R1YWxWYWx1ZSAhPT0gdW5kZWZpbmVkICYmIGFjdHVhbFZhbHVlICE9PSBudWxsKSByZXN1bHRpbmdWYWx1ZSA9IGFjdHVhbFZhbHVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHNldHRpbmcuaW52ZXJ0ZWRTZXR0aW5nTmFtZSkgcmVzdWx0aW5nVmFsdWUgPSAhcmVzdWx0aW5nVmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdGluZ1ZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGVzbGludC1kaXNhYmxlIHZhbGlkLWpzZG9jICovIC8vaHR0cHM6Ly9naXRodWIuY29tL2VzbGludC9lc2xpbnQvaXNzdWVzLzczMDdcclxuICAgIC8qKlxyXG4gICAgICogU2V0cyB0aGUgdmFsdWUgZm9yIGEgc2V0dGluZy4gVGhlIHJvb20gSUQgaXMgb3B0aW9uYWwgaWYgdGhlIHNldHRpbmcgaXMgbm90IGJlaW5nXHJcbiAgICAgKiBzZXQgZm9yIGEgcGFydGljdWxhciByb29tLCBvdGhlcndpc2UgaXQgc2hvdWxkIGJlIHN1cHBsaWVkLiBUaGUgdmFsdWUgbWF5IGJlIG51bGxcclxuICAgICAqIHRvIGluZGljYXRlIHRoYXQgdGhlIGxldmVsIHNob3VsZCBubyBsb25nZXIgaGF2ZSBhbiBvdmVycmlkZS5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzZXR0aW5nTmFtZSBUaGUgbmFtZSBvZiB0aGUgc2V0dGluZyB0byBjaGFuZ2UuXHJcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gcm9vbUlkIFRoZSByb29tIElEIHRvIGNoYW5nZSB0aGUgdmFsdWUgaW4sIG1heSBiZSBudWxsLlxyXG4gICAgICogQHBhcmFtIHtcImRldmljZVwifFwicm9vbS1kZXZpY2VcInxcInJvb20tYWNjb3VudFwifFwiYWNjb3VudFwifFwicm9vbVwifSBsZXZlbCBUaGUgbGV2ZWxcclxuICAgICAqIHRvIGNoYW5nZSB0aGUgdmFsdWUgYXQuXHJcbiAgICAgKiBAcGFyYW0geyp9IHZhbHVlIFRoZSBuZXcgdmFsdWUgb2YgdGhlIHNldHRpbmcsIG1heSBiZSBudWxsLlxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gUmVzb2x2ZXMgd2hlbiB0aGUgc2V0dGluZyBoYXMgYmVlbiBjaGFuZ2VkLlxyXG4gICAgICovXHJcbiAgICAvKiBlc2xpbnQtZW5hYmxlIHZhbGlkLWpzZG9jICovXHJcbiAgICBzdGF0aWMgYXN5bmMgc2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCwgbGV2ZWwsIHZhbHVlKSB7XHJcbiAgICAgICAgLy8gVmVyaWZ5IHRoYXQgdGhlIHNldHRpbmcgaXMgYWN0dWFsbHkgYSBzZXR0aW5nXHJcbiAgICAgICAgY29uc3Qgc2V0dGluZyA9IFNFVFRJTkdTW3NldHRpbmdOYW1lXTtcclxuICAgICAgICBpZiAoIXNldHRpbmcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiU2V0dGluZyAnXCIgKyBzZXR0aW5nTmFtZSArIFwiJyBkb2VzIG5vdCBhcHBlYXIgdG8gYmUgYSBzZXR0aW5nLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGhhbmRsZXIgPSBTZXR0aW5nc1N0b3JlLl9nZXRIYW5kbGVyKHNldHRpbmdOYW1lLCBsZXZlbCk7XHJcbiAgICAgICAgaWYgKCFoYW5kbGVyKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlNldHRpbmcgXCIgKyBzZXR0aW5nTmFtZSArIFwiIGRvZXMgbm90IGhhdmUgYSBoYW5kbGVyIGZvciBcIiArIGxldmVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzZXR0aW5nLmludmVydGVkU2V0dGluZ05hbWUpIHtcclxuICAgICAgICAgICAgLy8gTm90ZTogV2UgY2FuJ3QgZG8gdGhpcyB3aGVuIHRoZSBgbGV2ZWxgIGlzIFwiZGVmYXVsdFwiLCBob3dldmVyIHdlIGFsc29cclxuICAgICAgICAgICAgLy8ga25vdyB0aGF0IHRoZSB1c2VyIGNhbid0IHBvc3NpYmxlIGNoYW5nZSB0aGUgZGVmYXVsdCB2YWx1ZSB0aHJvdWdoIHRoaXNcclxuICAgICAgICAgICAgLy8gZnVuY3Rpb24gc28gd2UgZG9uJ3QgYm90aGVyIGNoZWNraW5nIGl0LlxyXG4gICAgICAgICAgICAvL2NvbnNvbGUud2FybihgSW52ZXJ0aW5nICR7c2V0dGluZ05hbWV9IHRvIGJlICR7c2V0dGluZy5pbnZlcnRlZFNldHRpbmdOYW1lfSAtIGxlZ2FjeSBzZXR0aW5nYCk7XHJcbiAgICAgICAgICAgIHNldHRpbmdOYW1lID0gc2V0dGluZy5pbnZlcnRlZFNldHRpbmdOYW1lO1xyXG4gICAgICAgICAgICB2YWx1ZSA9ICF2YWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghaGFuZGxlci5jYW5TZXRWYWx1ZShzZXR0aW5nTmFtZSwgcm9vbUlkKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVc2VyIGNhbm5vdCBzZXQgXCIgKyBzZXR0aW5nTmFtZSArIFwiIGF0IFwiICsgbGV2ZWwgKyBcIiBpbiBcIiArIHJvb21JZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhd2FpdCBoYW5kbGVyLnNldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQsIHZhbHVlKTtcclxuXHJcbiAgICAgICAgY29uc3QgY29udHJvbGxlciA9IHNldHRpbmcuY29udHJvbGxlcjtcclxuICAgICAgICBpZiAoY29udHJvbGxlcikge1xyXG4gICAgICAgICAgICBjb250cm9sbGVyLm9uQ2hhbmdlKGxldmVsLCByb29tSWQsIHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBjdXJyZW50IHVzZXIgaXMgcGVybWl0dGVkIHRvIHNldCB0aGUgZ2l2ZW4gc2V0dGluZyBhdCB0aGUgZ2l2ZW5cclxuICAgICAqIGxldmVsIGZvciBhIHBhcnRpY3VsYXIgcm9vbS4gVGhlIHJvb20gSUQgaXMgb3B0aW9uYWwgaWYgdGhlIHNldHRpbmcgaXMgbm90IGJlaW5nXHJcbiAgICAgKiBzZXQgZm9yIGEgcGFydGljdWxhciByb29tLCBvdGhlcndpc2UgaXQgc2hvdWxkIGJlIHN1cHBsaWVkLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNldHRpbmdOYW1lIFRoZSBuYW1lIG9mIHRoZSBzZXR0aW5nIHRvIGNoZWNrLlxyXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHJvb21JZCBUaGUgcm9vbSBJRCB0byBjaGVjayBpbiwgbWF5IGJlIG51bGwuXHJcbiAgICAgKiBAcGFyYW0ge1wiZGV2aWNlXCJ8XCJyb29tLWRldmljZVwifFwicm9vbS1hY2NvdW50XCJ8XCJhY2NvdW50XCJ8XCJyb29tXCJ9IGxldmVsIFRoZSBsZXZlbCB0b1xyXG4gICAgICogY2hlY2sgYXQuXHJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIHRoZSB1c2VyIG1heSBzZXQgdGhlIHNldHRpbmcsIGZhbHNlIG90aGVyd2lzZS5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIGNhblNldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQsIGxldmVsKSB7XHJcbiAgICAgICAgLy8gVmVyaWZ5IHRoYXQgdGhlIHNldHRpbmcgaXMgYWN0dWFsbHkgYSBzZXR0aW5nXHJcbiAgICAgICAgaWYgKCFTRVRUSU5HU1tzZXR0aW5nTmFtZV0pIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiU2V0dGluZyAnXCIgKyBzZXR0aW5nTmFtZSArIFwiJyBkb2VzIG5vdCBhcHBlYXIgdG8gYmUgYSBzZXR0aW5nLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGhhbmRsZXIgPSBTZXR0aW5nc1N0b3JlLl9nZXRIYW5kbGVyKHNldHRpbmdOYW1lLCBsZXZlbCk7XHJcbiAgICAgICAgaWYgKCFoYW5kbGVyKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuIGhhbmRsZXIuY2FuU2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBnaXZlbiBsZXZlbCBpcyBzdXBwb3J0ZWQgb24gdGhpcyBkZXZpY2UuXHJcbiAgICAgKiBAcGFyYW0ge1wiZGV2aWNlXCJ8XCJyb29tLWRldmljZVwifFwicm9vbS1hY2NvdW50XCJ8XCJhY2NvdW50XCJ8XCJyb29tXCJ9IGxldmVsIFRoZSBsZXZlbFxyXG4gICAgICogdG8gY2hlY2sgdGhlIGZlYXNpYmlsaXR5IG9mLlxyXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgbGV2ZWwgaXMgc3VwcG9ydGVkLCBmYWxzZSBvdGhlcndpc2UuXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBpc0xldmVsU3VwcG9ydGVkKGxldmVsKSB7XHJcbiAgICAgICAgaWYgKCFMRVZFTF9IQU5ETEVSU1tsZXZlbF0pIHJldHVybiBmYWxzZTtcclxuICAgICAgICByZXR1cm4gTEVWRUxfSEFORExFUlNbbGV2ZWxdLmlzU3VwcG9ydGVkKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWJ1Z2dpbmcgZnVuY3Rpb24gZm9yIHJlYWRpbmcgZXhwbGljaXQgc2V0dGluZyB2YWx1ZXMgd2l0aG91dCBnb2luZyB0aHJvdWdoIHRoZVxyXG4gICAgICogY29tcGxpY2F0ZWQvYmlhc2VkIGZ1bmN0aW9ucyBpbiB0aGUgU2V0dGluZ3NTdG9yZS4gVGhpcyB3aWxsIHByaW50IGluZm9ybWF0aW9uIHRvXHJcbiAgICAgKiB0aGUgY29uc29sZSBmb3IgYW5hbHlzaXMuIE5vdCBpbnRlbmRlZCB0byBiZSB1c2VkIHdpdGhpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVhbFNldHRpbmdOYW1lIFRoZSBzZXR0aW5nIG5hbWUgdG8gdHJ5IGFuZCByZWFkLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJvb21JZCBPcHRpb25hbCByb29tIElEIHRvIHRlc3QgdGhlIHNldHRpbmcgaW4uXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBkZWJ1Z1NldHRpbmcocmVhbFNldHRpbmdOYW1lLCByb29tSWQpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhgLS0tIERFQlVHICR7cmVhbFNldHRpbmdOYW1lfWApO1xyXG5cclxuICAgICAgICAvLyBOb3RlOiB3ZSBpbnRlbnRpb25hbGx5IHVzZSBKU09OLnN0cmluZ2lmeSBoZXJlIHRvIGF2b2lkIHRoZSBjb25zb2xlIG1hc2tpbmcgdGhlXHJcbiAgICAgICAgLy8gcHJvYmxlbSBpZiB0aGVyZSdzIGEgdHlwZSByZXByZXNlbnRhdGlvbiBpc3N1ZS4gQWxzbywgdGhpcyB3YXkgaXQgaXMgZ3VhcmFudGVlZFxyXG4gICAgICAgIC8vIHRvIHNob3cgdXAgaW4gYSByYWdlc2hha2UgaWYgcmVxdWlyZWQuXHJcblxyXG4gICAgICAgIGNvbnN0IGRlZiA9IFNFVFRJTkdTW3JlYWxTZXR0aW5nTmFtZV07XHJcbiAgICAgICAgY29uc29sZS5sb2coYC0tLSBkZWZpbml0aW9uOiAke2RlZiA/IEpTT04uc3RyaW5naWZ5KGRlZikgOiAnPE5PVF9GT1VORD4nfWApO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGAtLS0gZGVmYXVsdCBsZXZlbCBvcmRlcjogJHtKU09OLnN0cmluZ2lmeShMRVZFTF9PUkRFUil9YCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coYC0tLSByZWdpc3RlcmVkIGhhbmRsZXJzOiAke0pTT04uc3RyaW5naWZ5KE9iamVjdC5rZXlzKExFVkVMX0hBTkRMRVJTKSl9YCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGRvQ2hlY2tzID0gKHNldHRpbmdOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgaGFuZGxlck5hbWUgb2YgT2JqZWN0LmtleXMoTEVWRUxfSEFORExFUlMpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYW5kbGVyID0gTEVWRUxfSEFORExFUlNbaGFuZGxlck5hbWVdO1xyXG5cclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBoYW5kbGVyLmdldFZhbHVlKHNldHRpbmdOYW1lLCByb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gICAgICR7aGFuZGxlck5hbWV9QCR7cm9vbUlkIHx8ICc8bm9fcm9vbT4nfSA9ICR7SlNPTi5zdHJpbmdpZnkodmFsdWUpfWApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gICAgICR7aGFuZGxlcn1AJHtyb29tSWQgfHwgJzxub19yb29tPid9IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChyb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IGhhbmRsZXIuZ2V0VmFsdWUoc2V0dGluZ05hbWUsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICAke2hhbmRsZXJOYW1lfUA8bm9fcm9vbT4gPSAke0pTT04uc3RyaW5naWZ5KHZhbHVlKX1gKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gICAgICR7aGFuZGxlcn1APG5vX3Jvb20+IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gY2FsY3VsYXRpbmcgYXMgcmV0dXJuZWQgYnkgU2V0dGluZ3NTdG9yZWApO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tIHRoZXNlIG1pZ2h0IG5vdCBtYXRjaCBpZiB0aGUgc2V0dGluZyB1c2VzIGEgY29udHJvbGxlciAtIGJlIHdhcm5lZCFgKTtcclxuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoc2V0dGluZ05hbWUsIHJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICBTZXR0aW5nc1N0b3JlI2dlbmVyaWNAJHtyb29tSWQgfHwgJzxub19yb29tPid9ICA9ICR7SlNPTi5zdHJpbmdpZnkodmFsdWUpfWApO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICBTZXR0aW5nc1N0b3JlI2dlbmVyaWNAJHtyb29tSWQgfHwgJzxub19yb29tPid9IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChyb29tSWQpIHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBTZXR0aW5nc1N0b3JlLmdldFZhbHVlKHNldHRpbmdOYW1lLCBudWxsKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICBTZXR0aW5nc1N0b3JlI2dlbmVyaWNAPG5vX3Jvb20+ICA9ICR7SlNPTi5zdHJpbmdpZnkodmFsdWUpfWApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gICAgIFNldHRpbmdzU3RvcmUjZ2VuZXJpY0AkPG5vX3Jvb20+IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGxldmVsIG9mIExFVkVMX09SREVSKSB7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gU2V0dGluZ3NTdG9yZS5nZXRWYWx1ZUF0KGxldmVsLCBzZXR0aW5nTmFtZSwgcm9vbUlkKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICBTZXR0aW5nc1N0b3JlIyR7bGV2ZWx9QCR7cm9vbUlkIHx8ICc8bm9fcm9vbT4nfSA9ICR7SlNPTi5zdHJpbmdpZnkodmFsdWUpfWApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGAtLS0gICAgIFNldHRpbmdzU3RvcmUjJHtsZXZlbH1AJHtyb29tSWQgfHwgJzxub19yb29tPid9IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChyb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IFNldHRpbmdzU3RvcmUuZ2V0VmFsdWVBdChsZXZlbCwgc2V0dGluZ05hbWUsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tICAgICBTZXR0aW5nc1N0b3JlIyR7bGV2ZWx9QDxub19yb29tPiA9ICR7SlNPTi5zdHJpbmdpZnkodmFsdWUpfWApO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coYC0tLSAgICAgU2V0dGluZ3NTdG9yZSMke2xldmVsfUAkPG5vX3Jvb20+IFRIUkVXIEVSUk9SOiAke2UubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBkb0NoZWNrcyhyZWFsU2V0dGluZ05hbWUpO1xyXG5cclxuICAgICAgICBpZiAoZGVmLmludmVydGVkU2V0dGluZ05hbWUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYC0tLSBURVNUSU5HIElOVkVSVEVEIFNFVFRJTkcgTkFNRWApO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgLS0tIGludmVydGVkOiAke2RlZi5pbnZlcnRlZFNldHRpbmdOYW1lfWApO1xyXG4gICAgICAgICAgICBkb0NoZWNrcyhkZWYuaW52ZXJ0ZWRTZXR0aW5nTmFtZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhgLS0tIEVORCBERUJVR2ApO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBfZ2V0SGFuZGxlcihzZXR0aW5nTmFtZSwgbGV2ZWwpIHtcclxuICAgICAgICBjb25zdCBoYW5kbGVycyA9IFNldHRpbmdzU3RvcmUuX2dldEhhbmRsZXJzKHNldHRpbmdOYW1lKTtcclxuICAgICAgICBpZiAoIWhhbmRsZXJzW2xldmVsXSkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgcmV0dXJuIGhhbmRsZXJzW2xldmVsXTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgX2dldEhhbmRsZXJzKHNldHRpbmdOYW1lKSB7XHJcbiAgICAgICAgaWYgKCFTRVRUSU5HU1tzZXR0aW5nTmFtZV0pIHJldHVybiB7fTtcclxuXHJcbiAgICAgICAgY29uc3QgaGFuZGxlcnMgPSB7fTtcclxuICAgICAgICBmb3IgKGNvbnN0IGxldmVsIG9mIFNFVFRJTkdTW3NldHRpbmdOYW1lXS5zdXBwb3J0ZWRMZXZlbHMpIHtcclxuICAgICAgICAgICAgaWYgKCFMRVZFTF9IQU5ETEVSU1tsZXZlbF0pIHRocm93IG5ldyBFcnJvcihcIlVuZXhwZWN0ZWQgbGV2ZWwgXCIgKyBsZXZlbCk7XHJcbiAgICAgICAgICAgIGlmIChTZXR0aW5nc1N0b3JlLmlzTGV2ZWxTdXBwb3J0ZWQobGV2ZWwpKSBoYW5kbGVyc1tsZXZlbF0gPSBMRVZFTF9IQU5ETEVSU1tsZXZlbF07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBbHdheXMgc3VwcG9ydCAnZGVmYXVsdCdcclxuICAgICAgICBpZiAoIWhhbmRsZXJzWydkZWZhdWx0J10pIGhhbmRsZXJzWydkZWZhdWx0J10gPSBMRVZFTF9IQU5ETEVSU1snZGVmYXVsdCddO1xyXG5cclxuICAgICAgICByZXR1cm4gaGFuZGxlcnM7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIF9nZXRGZWF0dXJlU3RhdGUoc2V0dGluZ05hbWUpIHtcclxuICAgICAgICBjb25zdCBmZWF0dXJlc0NvbmZpZyA9IFNka0NvbmZpZy5nZXQoKVsnZmVhdHVyZXMnXTtcclxuICAgICAgICBjb25zdCBlbmFibGVMYWJzID0gU2RrQ29uZmlnLmdldCgpWydlbmFibGVMYWJzJ107IC8vIHdlJ2xsIGhvbm91ciB0aGUgb2xkIGZsYWdcclxuXHJcbiAgICAgICAgbGV0IGZlYXR1cmVTdGF0ZSA9IGVuYWJsZUxhYnMgPyBcImxhYnNcIiA6IFwiZGlzYWJsZVwiO1xyXG4gICAgICAgIGlmIChmZWF0dXJlc0NvbmZpZyAmJiBmZWF0dXJlc0NvbmZpZ1tzZXR0aW5nTmFtZV0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBmZWF0dXJlU3RhdGUgPSBmZWF0dXJlc0NvbmZpZ1tzZXR0aW5nTmFtZV07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBhbGxvd2VkU3RhdGVzID0gWydlbmFibGUnLCAnZGlzYWJsZScsICdsYWJzJ107XHJcbiAgICAgICAgaWYgKCFhbGxvd2VkU3RhdGVzLmluY2x1ZGVzKGZlYXR1cmVTdGF0ZSkpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiRmVhdHVyZSBzdGF0ZSAnXCIgKyBmZWF0dXJlU3RhdGUgKyBcIicgaXMgaW52YWxpZCBmb3IgXCIgKyBzZXR0aW5nTmFtZSk7XHJcbiAgICAgICAgICAgIGZlYXR1cmVTdGF0ZSA9IFwiZGlzYWJsZVwiOyAvLyB0byBwcmV2ZW50IGFjY2lkZW50YWwgZmVhdHVyZXMuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmVhdHVyZVN0YXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBGb3IgZGVidWdnaW5nIHB1cnBvc2VzXHJcbmdsb2JhbC5teFNldHRpbmdzU3RvcmUgPSBTZXR0aW5nc1N0b3JlO1xyXG4iXX0=