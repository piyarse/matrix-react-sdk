"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RovingTabIndexWrapper = exports.useRovingTabIndex = exports.RovingTabIndexProvider = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Keyboard = require("../Keyboard");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/**
 * Module to simplify implementing the Roving TabIndex accessibility technique
 *
 * Wrap the Widget in an RovingTabIndexContextProvider
 * and then for all buttons make use of useRovingTabIndex or RovingTabIndexWrapper.
 * The code will keep track of which tabIndex was most recently focused and expose that information as `isActive` which
 * can then be used to only set the tabIndex to 0 as expected by the roving tabindex technique.
 * When the active button gets unmounted the closest button will be chosen as expected.
 * Initially the first button to mount will be given active state.
 *
 * https://developer.mozilla.org/en-US/docs/Web/Accessibility/Keyboard-navigable_JavaScript_widgets#Technique_1_Roving_tabindex
 */
const DOCUMENT_POSITION_PRECEDING = 2;
const RovingTabIndexContext = (0, _react.createContext)({
  state: {
    activeRef: null,
    refs: [] // list of refs in DOM order

  },
  dispatch: () => {}
});
RovingTabIndexContext.displayName = "RovingTabIndexContext"; // TODO use a TypeScript type here

const types = {
  REGISTER: "REGISTER",
  UNREGISTER: "UNREGISTER",
  SET_FOCUS: "SET_FOCUS"
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.REGISTER:
      {
        if (state.refs.length === 0) {
          // Our list of refs was empty, set activeRef to this first item
          return _objectSpread({}, state, {
            activeRef: action.payload.ref,
            refs: [action.payload.ref]
          });
        }

        if (state.refs.includes(action.payload.ref)) {
          return state; // already in refs, this should not happen
        } // find the index of the first ref which is not preceding this one in DOM order


        let newIndex = state.refs.findIndex(ref => {
          return ref.current.compareDocumentPosition(action.payload.ref.current) & DOCUMENT_POSITION_PRECEDING;
        });

        if (newIndex < 0) {
          newIndex = state.refs.length; // append to the end
        } // update the refs list


        return _objectSpread({}, state, {
          refs: [...state.refs.slice(0, newIndex), action.payload.ref, ...state.refs.slice(newIndex)]
        });
      }

    case types.UNREGISTER:
      {
        // filter out the ref which we are removing
        const refs = state.refs.filter(r => r !== action.payload.ref);

        if (refs.length === state.refs.length) {
          return state; // already removed, this should not happen
        }

        if (state.activeRef === action.payload.ref) {
          // we just removed the active ref, need to replace it
          // pick the ref which is now in the index the old ref was in
          const oldIndex = state.refs.findIndex(r => r === action.payload.ref);
          return _objectSpread({}, state, {
            activeRef: oldIndex >= refs.length ? refs[refs.length - 1] : refs[oldIndex],
            refs
          });
        } // update the refs list


        return _objectSpread({}, state, {
          refs
        });
      }

    case types.SET_FOCUS:
      {
        // update active ref
        return _objectSpread({}, state, {
          activeRef: action.payload.ref
        });
      }

    default:
      return state;
  }
};

const RovingTabIndexProvider = ({
  children,
  handleHomeEnd,
  onKeyDown
}) => {
  const [state, dispatch] = (0, _react.useReducer)(reducer, {
    activeRef: null,
    refs: []
  });
  const context = (0, _react.useMemo)(() => ({
    state,
    dispatch
  }), [state]);
  const onKeyDownHandler = (0, _react.useCallback)(ev => {
    let handled = false;

    if (handleHomeEnd) {
      // check if we actually have any items
      switch (ev.key) {
        case _Keyboard.Key.HOME:
          handled = true; // move focus to first item

          if (context.state.refs.length > 0) {
            context.state.refs[0].current.focus();
          }

          break;

        case _Keyboard.Key.END:
          handled = true; // move focus to last item

          if (context.state.refs.length > 0) {
            context.state.refs[context.state.refs.length - 1].current.focus();
          }

          break;
      }
    }

    if (handled) {
      ev.preventDefault();
      ev.stopPropagation();
    } else if (onKeyDown) {
      return onKeyDown(ev);
    }
  }, [context.state, onKeyDown, handleHomeEnd]);
  return _react.default.createElement(RovingTabIndexContext.Provider, {
    value: context
  }, children({
    onKeyDownHandler
  }));
};

exports.RovingTabIndexProvider = RovingTabIndexProvider;
RovingTabIndexProvider.propTypes = {
  handleHomeEnd: _propTypes.default.bool,
  onKeyDown: _propTypes.default.func
}; // Hook to register a roving tab index
// inputRef parameter specifies the ref to use
// onFocus should be called when the index gained focus in any manner
// isActive should be used to set tabIndex in a manner such as `tabIndex={isActive ? 0 : -1}`
// ref should be passed to a DOM node which will be used for DOM compareDocumentPosition

const useRovingTabIndex = inputRef => {
  const context = (0, _react.useContext)(RovingTabIndexContext);
  let ref = (0, _react.useRef)(null);

  if (inputRef) {
    // if we are given a ref, use it instead of ours
    ref = inputRef;
  } // setup (after refs)


  (0, _react.useLayoutEffect)(() => {
    context.dispatch({
      type: types.REGISTER,
      payload: {
        ref
      }
    }); // teardown

    return () => {
      context.dispatch({
        type: types.UNREGISTER,
        payload: {
          ref
        }
      });
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onFocus = (0, _react.useCallback)(() => {
    context.dispatch({
      type: types.SET_FOCUS,
      payload: {
        ref
      }
    });
  }, [ref, context]);
  const isActive = context.state.activeRef === ref;
  return [onFocus, isActive, ref];
}; // Wrapper to allow use of useRovingTabIndex outside of React Functional Components.


exports.useRovingTabIndex = useRovingTabIndex;

const RovingTabIndexWrapper = ({
  children,
  inputRef
}) => {
  const [onFocus, isActive, ref] = useRovingTabIndex(inputRef);
  return children({
    onFocus,
    isActive,
    ref
  });
};

exports.RovingTabIndexWrapper = RovingTabIndexWrapper;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hY2Nlc3NpYmlsaXR5L1JvdmluZ1RhYkluZGV4LmpzIl0sIm5hbWVzIjpbIkRPQ1VNRU5UX1BPU0lUSU9OX1BSRUNFRElORyIsIlJvdmluZ1RhYkluZGV4Q29udGV4dCIsInN0YXRlIiwiYWN0aXZlUmVmIiwicmVmcyIsImRpc3BhdGNoIiwiZGlzcGxheU5hbWUiLCJ0eXBlcyIsIlJFR0lTVEVSIiwiVU5SRUdJU1RFUiIsIlNFVF9GT0NVUyIsInJlZHVjZXIiLCJhY3Rpb24iLCJ0eXBlIiwibGVuZ3RoIiwicGF5bG9hZCIsInJlZiIsImluY2x1ZGVzIiwibmV3SW5kZXgiLCJmaW5kSW5kZXgiLCJjdXJyZW50IiwiY29tcGFyZURvY3VtZW50UG9zaXRpb24iLCJzbGljZSIsImZpbHRlciIsInIiLCJvbGRJbmRleCIsIlJvdmluZ1RhYkluZGV4UHJvdmlkZXIiLCJjaGlsZHJlbiIsImhhbmRsZUhvbWVFbmQiLCJvbktleURvd24iLCJjb250ZXh0Iiwib25LZXlEb3duSGFuZGxlciIsImV2IiwiaGFuZGxlZCIsImtleSIsIktleSIsIkhPTUUiLCJmb2N1cyIsIkVORCIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYm9vbCIsImZ1bmMiLCJ1c2VSb3ZpbmdUYWJJbmRleCIsImlucHV0UmVmIiwib25Gb2N1cyIsImlzQWN0aXZlIiwiUm92aW5nVGFiSW5kZXhXcmFwcGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBZ0JBOztBQVNBOztBQUNBOzs7Ozs7QUFFQTs7Ozs7Ozs7Ozs7O0FBYUEsTUFBTUEsMkJBQTJCLEdBQUcsQ0FBcEM7QUFFQSxNQUFNQyxxQkFBcUIsR0FBRywwQkFBYztBQUN4Q0MsRUFBQUEsS0FBSyxFQUFFO0FBQ0hDLElBQUFBLFNBQVMsRUFBRSxJQURSO0FBRUhDLElBQUFBLElBQUksRUFBRSxFQUZILENBRU87O0FBRlAsR0FEaUM7QUFLeENDLEVBQUFBLFFBQVEsRUFBRSxNQUFNLENBQUU7QUFMc0IsQ0FBZCxDQUE5QjtBQU9BSixxQkFBcUIsQ0FBQ0ssV0FBdEIsR0FBb0MsdUJBQXBDLEMsQ0FFQTs7QUFDQSxNQUFNQyxLQUFLLEdBQUc7QUFDVkMsRUFBQUEsUUFBUSxFQUFFLFVBREE7QUFFVkMsRUFBQUEsVUFBVSxFQUFFLFlBRkY7QUFHVkMsRUFBQUEsU0FBUyxFQUFFO0FBSEQsQ0FBZDs7QUFNQSxNQUFNQyxPQUFPLEdBQUcsQ0FBQ1QsS0FBRCxFQUFRVSxNQUFSLEtBQW1CO0FBQy9CLFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUNJLFNBQUtOLEtBQUssQ0FBQ0MsUUFBWDtBQUFxQjtBQUNqQixZQUFJTixLQUFLLENBQUNFLElBQU4sQ0FBV1UsTUFBWCxLQUFzQixDQUExQixFQUE2QjtBQUN6QjtBQUNBLG1DQUNPWixLQURQO0FBRUlDLFlBQUFBLFNBQVMsRUFBRVMsTUFBTSxDQUFDRyxPQUFQLENBQWVDLEdBRjlCO0FBR0laLFlBQUFBLElBQUksRUFBRSxDQUFDUSxNQUFNLENBQUNHLE9BQVAsQ0FBZUMsR0FBaEI7QUFIVjtBQUtIOztBQUVELFlBQUlkLEtBQUssQ0FBQ0UsSUFBTixDQUFXYSxRQUFYLENBQW9CTCxNQUFNLENBQUNHLE9BQVAsQ0FBZUMsR0FBbkMsQ0FBSixFQUE2QztBQUN6QyxpQkFBT2QsS0FBUCxDQUR5QyxDQUMzQjtBQUNqQixTQVpnQixDQWNqQjs7O0FBQ0EsWUFBSWdCLFFBQVEsR0FBR2hCLEtBQUssQ0FBQ0UsSUFBTixDQUFXZSxTQUFYLENBQXFCSCxHQUFHLElBQUk7QUFDdkMsaUJBQU9BLEdBQUcsQ0FBQ0ksT0FBSixDQUFZQyx1QkFBWixDQUFvQ1QsTUFBTSxDQUFDRyxPQUFQLENBQWVDLEdBQWYsQ0FBbUJJLE9BQXZELElBQWtFcEIsMkJBQXpFO0FBQ0gsU0FGYyxDQUFmOztBQUlBLFlBQUlrQixRQUFRLEdBQUcsQ0FBZixFQUFrQjtBQUNkQSxVQUFBQSxRQUFRLEdBQUdoQixLQUFLLENBQUNFLElBQU4sQ0FBV1UsTUFBdEIsQ0FEYyxDQUNnQjtBQUNqQyxTQXJCZ0IsQ0F1QmpCOzs7QUFDQSxpQ0FDT1osS0FEUDtBQUVJRSxVQUFBQSxJQUFJLEVBQUUsQ0FDRixHQUFHRixLQUFLLENBQUNFLElBQU4sQ0FBV2tCLEtBQVgsQ0FBaUIsQ0FBakIsRUFBb0JKLFFBQXBCLENBREQsRUFFRk4sTUFBTSxDQUFDRyxPQUFQLENBQWVDLEdBRmIsRUFHRixHQUFHZCxLQUFLLENBQUNFLElBQU4sQ0FBV2tCLEtBQVgsQ0FBaUJKLFFBQWpCLENBSEQ7QUFGVjtBQVFIOztBQUNELFNBQUtYLEtBQUssQ0FBQ0UsVUFBWDtBQUF1QjtBQUNuQjtBQUNBLGNBQU1MLElBQUksR0FBR0YsS0FBSyxDQUFDRSxJQUFOLENBQVdtQixNQUFYLENBQWtCQyxDQUFDLElBQUlBLENBQUMsS0FBS1osTUFBTSxDQUFDRyxPQUFQLENBQWVDLEdBQTVDLENBQWI7O0FBRUEsWUFBSVosSUFBSSxDQUFDVSxNQUFMLEtBQWdCWixLQUFLLENBQUNFLElBQU4sQ0FBV1UsTUFBL0IsRUFBdUM7QUFDbkMsaUJBQU9aLEtBQVAsQ0FEbUMsQ0FDckI7QUFDakI7O0FBRUQsWUFBSUEsS0FBSyxDQUFDQyxTQUFOLEtBQW9CUyxNQUFNLENBQUNHLE9BQVAsQ0FBZUMsR0FBdkMsRUFBNEM7QUFDeEM7QUFDQTtBQUNBLGdCQUFNUyxRQUFRLEdBQUd2QixLQUFLLENBQUNFLElBQU4sQ0FBV2UsU0FBWCxDQUFxQkssQ0FBQyxJQUFJQSxDQUFDLEtBQUtaLE1BQU0sQ0FBQ0csT0FBUCxDQUFlQyxHQUEvQyxDQUFqQjtBQUNBLG1DQUNPZCxLQURQO0FBRUlDLFlBQUFBLFNBQVMsRUFBRXNCLFFBQVEsSUFBSXJCLElBQUksQ0FBQ1UsTUFBakIsR0FBMEJWLElBQUksQ0FBQ0EsSUFBSSxDQUFDVSxNQUFMLEdBQWMsQ0FBZixDQUE5QixHQUFrRFYsSUFBSSxDQUFDcUIsUUFBRCxDQUZyRTtBQUdJckIsWUFBQUE7QUFISjtBQUtILFNBakJrQixDQW1CbkI7OztBQUNBLGlDQUNPRixLQURQO0FBRUlFLFVBQUFBO0FBRko7QUFJSDs7QUFDRCxTQUFLRyxLQUFLLENBQUNHLFNBQVg7QUFBc0I7QUFDbEI7QUFDQSxpQ0FDT1IsS0FEUDtBQUVJQyxVQUFBQSxTQUFTLEVBQUVTLE1BQU0sQ0FBQ0csT0FBUCxDQUFlQztBQUY5QjtBQUlIOztBQUNEO0FBQ0ksYUFBT2QsS0FBUDtBQW5FUjtBQXFFSCxDQXRFRDs7QUF3RU8sTUFBTXdCLHNCQUFzQixHQUFHLENBQUM7QUFBQ0MsRUFBQUEsUUFBRDtBQUFXQyxFQUFBQSxhQUFYO0FBQTBCQyxFQUFBQTtBQUExQixDQUFELEtBQTBDO0FBQzVFLFFBQU0sQ0FBQzNCLEtBQUQsRUFBUUcsUUFBUixJQUFvQix1QkFBV00sT0FBWCxFQUFvQjtBQUMxQ1IsSUFBQUEsU0FBUyxFQUFFLElBRCtCO0FBRTFDQyxJQUFBQSxJQUFJLEVBQUU7QUFGb0MsR0FBcEIsQ0FBMUI7QUFLQSxRQUFNMEIsT0FBTyxHQUFHLG9CQUFRLE9BQU87QUFBQzVCLElBQUFBLEtBQUQ7QUFBUUcsSUFBQUE7QUFBUixHQUFQLENBQVIsRUFBbUMsQ0FBQ0gsS0FBRCxDQUFuQyxDQUFoQjtBQUVBLFFBQU02QixnQkFBZ0IsR0FBRyx3QkFBYUMsRUFBRCxJQUFRO0FBQ3pDLFFBQUlDLE9BQU8sR0FBRyxLQUFkOztBQUNBLFFBQUlMLGFBQUosRUFBbUI7QUFDZjtBQUNBLGNBQVFJLEVBQUUsQ0FBQ0UsR0FBWDtBQUNJLGFBQUtDLGNBQUlDLElBQVQ7QUFDSUgsVUFBQUEsT0FBTyxHQUFHLElBQVYsQ0FESixDQUVJOztBQUNBLGNBQUlILE9BQU8sQ0FBQzVCLEtBQVIsQ0FBY0UsSUFBZCxDQUFtQlUsTUFBbkIsR0FBNEIsQ0FBaEMsRUFBbUM7QUFDL0JnQixZQUFBQSxPQUFPLENBQUM1QixLQUFSLENBQWNFLElBQWQsQ0FBbUIsQ0FBbkIsRUFBc0JnQixPQUF0QixDQUE4QmlCLEtBQTlCO0FBQ0g7O0FBQ0Q7O0FBQ0osYUFBS0YsY0FBSUcsR0FBVDtBQUNJTCxVQUFBQSxPQUFPLEdBQUcsSUFBVixDQURKLENBRUk7O0FBQ0EsY0FBSUgsT0FBTyxDQUFDNUIsS0FBUixDQUFjRSxJQUFkLENBQW1CVSxNQUFuQixHQUE0QixDQUFoQyxFQUFtQztBQUMvQmdCLFlBQUFBLE9BQU8sQ0FBQzVCLEtBQVIsQ0FBY0UsSUFBZCxDQUFtQjBCLE9BQU8sQ0FBQzVCLEtBQVIsQ0FBY0UsSUFBZCxDQUFtQlUsTUFBbkIsR0FBNEIsQ0FBL0MsRUFBa0RNLE9BQWxELENBQTBEaUIsS0FBMUQ7QUFDSDs7QUFDRDtBQWRSO0FBZ0JIOztBQUVELFFBQUlKLE9BQUosRUFBYTtBQUNURCxNQUFBQSxFQUFFLENBQUNPLGNBQUg7QUFDQVAsTUFBQUEsRUFBRSxDQUFDUSxlQUFIO0FBQ0gsS0FIRCxNQUdPLElBQUlYLFNBQUosRUFBZTtBQUNsQixhQUFPQSxTQUFTLENBQUNHLEVBQUQsQ0FBaEI7QUFDSDtBQUNKLEdBNUJ3QixFQTRCdEIsQ0FBQ0YsT0FBTyxDQUFDNUIsS0FBVCxFQUFnQjJCLFNBQWhCLEVBQTJCRCxhQUEzQixDQTVCc0IsQ0FBekI7QUE4QkEsU0FBTyw2QkFBQyxxQkFBRCxDQUF1QixRQUF2QjtBQUFnQyxJQUFBLEtBQUssRUFBRUU7QUFBdkMsS0FDREgsUUFBUSxDQUFDO0FBQUNJLElBQUFBO0FBQUQsR0FBRCxDQURQLENBQVA7QUFHSCxDQXpDTTs7O0FBMENQTCxzQkFBc0IsQ0FBQ2UsU0FBdkIsR0FBbUM7QUFDL0JiLEVBQUFBLGFBQWEsRUFBRWMsbUJBQVVDLElBRE07QUFFL0JkLEVBQUFBLFNBQVMsRUFBRWEsbUJBQVVFO0FBRlUsQ0FBbkMsQyxDQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ08sTUFBTUMsaUJBQWlCLEdBQUlDLFFBQUQsSUFBYztBQUMzQyxRQUFNaEIsT0FBTyxHQUFHLHVCQUFXN0IscUJBQVgsQ0FBaEI7QUFDQSxNQUFJZSxHQUFHLEdBQUcsbUJBQU8sSUFBUCxDQUFWOztBQUVBLE1BQUk4QixRQUFKLEVBQWM7QUFDVjtBQUNBOUIsSUFBQUEsR0FBRyxHQUFHOEIsUUFBTjtBQUNILEdBUDBDLENBUzNDOzs7QUFDQSw4QkFBZ0IsTUFBTTtBQUNsQmhCLElBQUFBLE9BQU8sQ0FBQ3pCLFFBQVIsQ0FBaUI7QUFDYlEsTUFBQUEsSUFBSSxFQUFFTixLQUFLLENBQUNDLFFBREM7QUFFYk8sTUFBQUEsT0FBTyxFQUFFO0FBQUNDLFFBQUFBO0FBQUQ7QUFGSSxLQUFqQixFQURrQixDQUtsQjs7QUFDQSxXQUFPLE1BQU07QUFDVGMsTUFBQUEsT0FBTyxDQUFDekIsUUFBUixDQUFpQjtBQUNiUSxRQUFBQSxJQUFJLEVBQUVOLEtBQUssQ0FBQ0UsVUFEQztBQUViTSxRQUFBQSxPQUFPLEVBQUU7QUFBQ0MsVUFBQUE7QUFBRDtBQUZJLE9BQWpCO0FBSUgsS0FMRDtBQU1ILEdBWkQsRUFZRyxFQVpILEVBVjJDLENBc0JuQzs7QUFFUixRQUFNK0IsT0FBTyxHQUFHLHdCQUFZLE1BQU07QUFDOUJqQixJQUFBQSxPQUFPLENBQUN6QixRQUFSLENBQWlCO0FBQ2JRLE1BQUFBLElBQUksRUFBRU4sS0FBSyxDQUFDRyxTQURDO0FBRWJLLE1BQUFBLE9BQU8sRUFBRTtBQUFDQyxRQUFBQTtBQUFEO0FBRkksS0FBakI7QUFJSCxHQUxlLEVBS2IsQ0FBQ0EsR0FBRCxFQUFNYyxPQUFOLENBTGEsQ0FBaEI7QUFPQSxRQUFNa0IsUUFBUSxHQUFHbEIsT0FBTyxDQUFDNUIsS0FBUixDQUFjQyxTQUFkLEtBQTRCYSxHQUE3QztBQUNBLFNBQU8sQ0FBQytCLE9BQUQsRUFBVUMsUUFBVixFQUFvQmhDLEdBQXBCLENBQVA7QUFDSCxDQWpDTSxDLENBbUNQOzs7OztBQUNPLE1BQU1pQyxxQkFBcUIsR0FBRyxDQUFDO0FBQUN0QixFQUFBQSxRQUFEO0FBQVdtQixFQUFBQTtBQUFYLENBQUQsS0FBMEI7QUFDM0QsUUFBTSxDQUFDQyxPQUFELEVBQVVDLFFBQVYsRUFBb0JoQyxHQUFwQixJQUEyQjZCLGlCQUFpQixDQUFDQyxRQUFELENBQWxEO0FBQ0EsU0FBT25CLFFBQVEsQ0FBQztBQUFDb0IsSUFBQUEsT0FBRDtBQUFVQyxJQUFBQSxRQUFWO0FBQW9CaEMsSUFBQUE7QUFBcEIsR0FBRCxDQUFmO0FBQ0gsQ0FITSIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuaW1wb3J0IFJlYWN0LCB7XHJcbiAgICBjcmVhdGVDb250ZXh0LFxyXG4gICAgdXNlQ2FsbGJhY2ssXHJcbiAgICB1c2VDb250ZXh0LFxyXG4gICAgdXNlTGF5b3V0RWZmZWN0LFxyXG4gICAgdXNlTWVtbyxcclxuICAgIHVzZVJlZixcclxuICAgIHVzZVJlZHVjZXIsXHJcbn0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSBcInByb3AtdHlwZXNcIjtcclxuaW1wb3J0IHtLZXl9IGZyb20gXCIuLi9LZXlib2FyZFwiO1xyXG5cclxuLyoqXHJcbiAqIE1vZHVsZSB0byBzaW1wbGlmeSBpbXBsZW1lbnRpbmcgdGhlIFJvdmluZyBUYWJJbmRleCBhY2Nlc3NpYmlsaXR5IHRlY2huaXF1ZVxyXG4gKlxyXG4gKiBXcmFwIHRoZSBXaWRnZXQgaW4gYW4gUm92aW5nVGFiSW5kZXhDb250ZXh0UHJvdmlkZXJcclxuICogYW5kIHRoZW4gZm9yIGFsbCBidXR0b25zIG1ha2UgdXNlIG9mIHVzZVJvdmluZ1RhYkluZGV4IG9yIFJvdmluZ1RhYkluZGV4V3JhcHBlci5cclxuICogVGhlIGNvZGUgd2lsbCBrZWVwIHRyYWNrIG9mIHdoaWNoIHRhYkluZGV4IHdhcyBtb3N0IHJlY2VudGx5IGZvY3VzZWQgYW5kIGV4cG9zZSB0aGF0IGluZm9ybWF0aW9uIGFzIGBpc0FjdGl2ZWAgd2hpY2hcclxuICogY2FuIHRoZW4gYmUgdXNlZCB0byBvbmx5IHNldCB0aGUgdGFiSW5kZXggdG8gMCBhcyBleHBlY3RlZCBieSB0aGUgcm92aW5nIHRhYmluZGV4IHRlY2huaXF1ZS5cclxuICogV2hlbiB0aGUgYWN0aXZlIGJ1dHRvbiBnZXRzIHVubW91bnRlZCB0aGUgY2xvc2VzdCBidXR0b24gd2lsbCBiZSBjaG9zZW4gYXMgZXhwZWN0ZWQuXHJcbiAqIEluaXRpYWxseSB0aGUgZmlyc3QgYnV0dG9uIHRvIG1vdW50IHdpbGwgYmUgZ2l2ZW4gYWN0aXZlIHN0YXRlLlxyXG4gKlxyXG4gKiBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BY2Nlc3NpYmlsaXR5L0tleWJvYXJkLW5hdmlnYWJsZV9KYXZhU2NyaXB0X3dpZGdldHMjVGVjaG5pcXVlXzFfUm92aW5nX3RhYmluZGV4XHJcbiAqL1xyXG5cclxuY29uc3QgRE9DVU1FTlRfUE9TSVRJT05fUFJFQ0VESU5HID0gMjtcclxuXHJcbmNvbnN0IFJvdmluZ1RhYkluZGV4Q29udGV4dCA9IGNyZWF0ZUNvbnRleHQoe1xyXG4gICAgc3RhdGU6IHtcclxuICAgICAgICBhY3RpdmVSZWY6IG51bGwsXHJcbiAgICAgICAgcmVmczogW10sIC8vIGxpc3Qgb2YgcmVmcyBpbiBET00gb3JkZXJcclxuICAgIH0sXHJcbiAgICBkaXNwYXRjaDogKCkgPT4ge30sXHJcbn0pO1xyXG5Sb3ZpbmdUYWJJbmRleENvbnRleHQuZGlzcGxheU5hbWUgPSBcIlJvdmluZ1RhYkluZGV4Q29udGV4dFwiO1xyXG5cclxuLy8gVE9ETyB1c2UgYSBUeXBlU2NyaXB0IHR5cGUgaGVyZVxyXG5jb25zdCB0eXBlcyA9IHtcclxuICAgIFJFR0lTVEVSOiBcIlJFR0lTVEVSXCIsXHJcbiAgICBVTlJFR0lTVEVSOiBcIlVOUkVHSVNURVJcIixcclxuICAgIFNFVF9GT0NVUzogXCJTRVRfRk9DVVNcIixcclxufTtcclxuXHJcbmNvbnN0IHJlZHVjZXIgPSAoc3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgdHlwZXMuUkVHSVNURVI6IHtcclxuICAgICAgICAgICAgaWYgKHN0YXRlLnJlZnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBPdXIgbGlzdCBvZiByZWZzIHdhcyBlbXB0eSwgc2V0IGFjdGl2ZVJlZiB0byB0aGlzIGZpcnN0IGl0ZW1cclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlUmVmOiBhY3Rpb24ucGF5bG9hZC5yZWYsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVmczogW2FjdGlvbi5wYXlsb2FkLnJlZl0sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoc3RhdGUucmVmcy5pbmNsdWRlcyhhY3Rpb24ucGF5bG9hZC5yZWYpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc3RhdGU7IC8vIGFscmVhZHkgaW4gcmVmcywgdGhpcyBzaG91bGQgbm90IGhhcHBlblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBmaW5kIHRoZSBpbmRleCBvZiB0aGUgZmlyc3QgcmVmIHdoaWNoIGlzIG5vdCBwcmVjZWRpbmcgdGhpcyBvbmUgaW4gRE9NIG9yZGVyXHJcbiAgICAgICAgICAgIGxldCBuZXdJbmRleCA9IHN0YXRlLnJlZnMuZmluZEluZGV4KHJlZiA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVmLmN1cnJlbnQuY29tcGFyZURvY3VtZW50UG9zaXRpb24oYWN0aW9uLnBheWxvYWQucmVmLmN1cnJlbnQpICYgRE9DVU1FTlRfUE9TSVRJT05fUFJFQ0VESU5HO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdJbmRleCA8IDApIHtcclxuICAgICAgICAgICAgICAgIG5ld0luZGV4ID0gc3RhdGUucmVmcy5sZW5ndGg7IC8vIGFwcGVuZCB0byB0aGUgZW5kXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHVwZGF0ZSB0aGUgcmVmcyBsaXN0XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIHJlZnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAuLi5zdGF0ZS5yZWZzLnNsaWNlKDAsIG5ld0luZGV4KSxcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZC5yZWYsXHJcbiAgICAgICAgICAgICAgICAgICAgLi4uc3RhdGUucmVmcy5zbGljZShuZXdJbmRleCksXHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlIHR5cGVzLlVOUkVHSVNURVI6IHtcclxuICAgICAgICAgICAgLy8gZmlsdGVyIG91dCB0aGUgcmVmIHdoaWNoIHdlIGFyZSByZW1vdmluZ1xyXG4gICAgICAgICAgICBjb25zdCByZWZzID0gc3RhdGUucmVmcy5maWx0ZXIociA9PiByICE9PSBhY3Rpb24ucGF5bG9hZC5yZWYpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHJlZnMubGVuZ3RoID09PSBzdGF0ZS5yZWZzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN0YXRlOyAvLyBhbHJlYWR5IHJlbW92ZWQsIHRoaXMgc2hvdWxkIG5vdCBoYXBwZW5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHN0YXRlLmFjdGl2ZVJlZiA9PT0gYWN0aW9uLnBheWxvYWQucmVmKSB7XHJcbiAgICAgICAgICAgICAgICAvLyB3ZSBqdXN0IHJlbW92ZWQgdGhlIGFjdGl2ZSByZWYsIG5lZWQgdG8gcmVwbGFjZSBpdFxyXG4gICAgICAgICAgICAgICAgLy8gcGljayB0aGUgcmVmIHdoaWNoIGlzIG5vdyBpbiB0aGUgaW5kZXggdGhlIG9sZCByZWYgd2FzIGluXHJcbiAgICAgICAgICAgICAgICBjb25zdCBvbGRJbmRleCA9IHN0YXRlLnJlZnMuZmluZEluZGV4KHIgPT4gciA9PT0gYWN0aW9uLnBheWxvYWQucmVmKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlUmVmOiBvbGRJbmRleCA+PSByZWZzLmxlbmd0aCA/IHJlZnNbcmVmcy5sZW5ndGggLSAxXSA6IHJlZnNbb2xkSW5kZXhdLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlZnMsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyB1cGRhdGUgdGhlIHJlZnMgbGlzdFxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICByZWZzLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlIHR5cGVzLlNFVF9GT0NVUzoge1xyXG4gICAgICAgICAgICAvLyB1cGRhdGUgYWN0aXZlIHJlZlxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBhY3RpdmVSZWY6IGFjdGlvbi5wYXlsb2FkLnJlZixcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xyXG4gICAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IFJvdmluZ1RhYkluZGV4UHJvdmlkZXIgPSAoe2NoaWxkcmVuLCBoYW5kbGVIb21lRW5kLCBvbktleURvd259KSA9PiB7XHJcbiAgICBjb25zdCBbc3RhdGUsIGRpc3BhdGNoXSA9IHVzZVJlZHVjZXIocmVkdWNlciwge1xyXG4gICAgICAgIGFjdGl2ZVJlZjogbnVsbCxcclxuICAgICAgICByZWZzOiBbXSxcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IGNvbnRleHQgPSB1c2VNZW1vKCgpID0+ICh7c3RhdGUsIGRpc3BhdGNofSksIFtzdGF0ZV0pO1xyXG5cclxuICAgIGNvbnN0IG9uS2V5RG93bkhhbmRsZXIgPSB1c2VDYWxsYmFjaygoZXYpID0+IHtcclxuICAgICAgICBsZXQgaGFuZGxlZCA9IGZhbHNlO1xyXG4gICAgICAgIGlmIChoYW5kbGVIb21lRW5kKSB7XHJcbiAgICAgICAgICAgIC8vIGNoZWNrIGlmIHdlIGFjdHVhbGx5IGhhdmUgYW55IGl0ZW1zXHJcbiAgICAgICAgICAgIHN3aXRjaCAoZXYua2V5KSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEtleS5IT01FOlxyXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG1vdmUgZm9jdXMgdG8gZmlyc3QgaXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb250ZXh0LnN0YXRlLnJlZnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZXh0LnN0YXRlLnJlZnNbMF0uY3VycmVudC5mb2N1cygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgS2V5LkVORDpcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBtb3ZlIGZvY3VzIHRvIGxhc3QgaXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb250ZXh0LnN0YXRlLnJlZnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZXh0LnN0YXRlLnJlZnNbY29udGV4dC5zdGF0ZS5yZWZzLmxlbmd0aCAtIDFdLmN1cnJlbnQuZm9jdXMoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChoYW5kbGVkKSB7XHJcbiAgICAgICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAob25LZXlEb3duKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvbktleURvd24oZXYpO1xyXG4gICAgICAgIH1cclxuICAgIH0sIFtjb250ZXh0LnN0YXRlLCBvbktleURvd24sIGhhbmRsZUhvbWVFbmRdKTtcclxuXHJcbiAgICByZXR1cm4gPFJvdmluZ1RhYkluZGV4Q29udGV4dC5Qcm92aWRlciB2YWx1ZT17Y29udGV4dH0+XHJcbiAgICAgICAgeyBjaGlsZHJlbih7b25LZXlEb3duSGFuZGxlcn0pIH1cclxuICAgIDwvUm92aW5nVGFiSW5kZXhDb250ZXh0LlByb3ZpZGVyPjtcclxufTtcclxuUm92aW5nVGFiSW5kZXhQcm92aWRlci5wcm9wVHlwZXMgPSB7XHJcbiAgICBoYW5kbGVIb21lRW5kOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIG9uS2V5RG93bjogUHJvcFR5cGVzLmZ1bmMsXHJcbn07XHJcblxyXG4vLyBIb29rIHRvIHJlZ2lzdGVyIGEgcm92aW5nIHRhYiBpbmRleFxyXG4vLyBpbnB1dFJlZiBwYXJhbWV0ZXIgc3BlY2lmaWVzIHRoZSByZWYgdG8gdXNlXHJcbi8vIG9uRm9jdXMgc2hvdWxkIGJlIGNhbGxlZCB3aGVuIHRoZSBpbmRleCBnYWluZWQgZm9jdXMgaW4gYW55IG1hbm5lclxyXG4vLyBpc0FjdGl2ZSBzaG91bGQgYmUgdXNlZCB0byBzZXQgdGFiSW5kZXggaW4gYSBtYW5uZXIgc3VjaCBhcyBgdGFiSW5kZXg9e2lzQWN0aXZlID8gMCA6IC0xfWBcclxuLy8gcmVmIHNob3VsZCBiZSBwYXNzZWQgdG8gYSBET00gbm9kZSB3aGljaCB3aWxsIGJlIHVzZWQgZm9yIERPTSBjb21wYXJlRG9jdW1lbnRQb3NpdGlvblxyXG5leHBvcnQgY29uc3QgdXNlUm92aW5nVGFiSW5kZXggPSAoaW5wdXRSZWYpID0+IHtcclxuICAgIGNvbnN0IGNvbnRleHQgPSB1c2VDb250ZXh0KFJvdmluZ1RhYkluZGV4Q29udGV4dCk7XHJcbiAgICBsZXQgcmVmID0gdXNlUmVmKG51bGwpO1xyXG5cclxuICAgIGlmIChpbnB1dFJlZikge1xyXG4gICAgICAgIC8vIGlmIHdlIGFyZSBnaXZlbiBhIHJlZiwgdXNlIGl0IGluc3RlYWQgb2Ygb3Vyc1xyXG4gICAgICAgIHJlZiA9IGlucHV0UmVmO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHNldHVwIChhZnRlciByZWZzKVxyXG4gICAgdXNlTGF5b3V0RWZmZWN0KCgpID0+IHtcclxuICAgICAgICBjb250ZXh0LmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgdHlwZTogdHlwZXMuUkVHSVNURVIsXHJcbiAgICAgICAgICAgIHBheWxvYWQ6IHtyZWZ9LFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vIHRlYXJkb3duXHJcbiAgICAgICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgICAgICAgY29udGV4dC5kaXNwYXRjaCh7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiB0eXBlcy5VTlJFR0lTVEVSLFxyXG4gICAgICAgICAgICAgICAgcGF5bG9hZDoge3JlZn0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9LCBbXSk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgcmVhY3QtaG9va3MvZXhoYXVzdGl2ZS1kZXBzXHJcblxyXG4gICAgY29uc3Qgb25Gb2N1cyA9IHVzZUNhbGxiYWNrKCgpID0+IHtcclxuICAgICAgICBjb250ZXh0LmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgdHlwZTogdHlwZXMuU0VUX0ZPQ1VTLFxyXG4gICAgICAgICAgICBwYXlsb2FkOiB7cmVmfSxcclxuICAgICAgICB9KTtcclxuICAgIH0sIFtyZWYsIGNvbnRleHRdKTtcclxuXHJcbiAgICBjb25zdCBpc0FjdGl2ZSA9IGNvbnRleHQuc3RhdGUuYWN0aXZlUmVmID09PSByZWY7XHJcbiAgICByZXR1cm4gW29uRm9jdXMsIGlzQWN0aXZlLCByZWZdO1xyXG59O1xyXG5cclxuLy8gV3JhcHBlciB0byBhbGxvdyB1c2Ugb2YgdXNlUm92aW5nVGFiSW5kZXggb3V0c2lkZSBvZiBSZWFjdCBGdW5jdGlvbmFsIENvbXBvbmVudHMuXHJcbmV4cG9ydCBjb25zdCBSb3ZpbmdUYWJJbmRleFdyYXBwZXIgPSAoe2NoaWxkcmVuLCBpbnB1dFJlZn0pID0+IHtcclxuICAgIGNvbnN0IFtvbkZvY3VzLCBpc0FjdGl2ZSwgcmVmXSA9IHVzZVJvdmluZ1RhYkluZGV4KGlucHV0UmVmKTtcclxuICAgIHJldHVybiBjaGlsZHJlbih7b25Gb2N1cywgaXNBY3RpdmUsIHJlZn0pO1xyXG59O1xyXG5cclxuIl19