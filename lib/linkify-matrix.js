"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _SpecPermalinkConstructor = require("./utils/permalinks/SpecPermalinkConstructor");

var _Permalinks = require("./utils/permalinks/Permalinks");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function matrixLinkify(linkify) {
  // Text tokens
  const TT = linkify.scanner.TOKENS; // Multi tokens

  const MT = linkify.parser.TOKENS;
  const MultiToken = MT.Base;
  const S_START = linkify.parser.start;

  if (TT.UNDERSCORE === undefined) {
    throw new Error("linkify-matrix requires linkifyjs 2.1.1: this version is too old.");
  }

  const ROOMALIAS = function (value) {
    MultiToken.call(this, value);
    this.type = 'roomalias';
    this.isLink = true;
  };

  ROOMALIAS.prototype = new MultiToken();
  const S_HASH = S_START.jump(TT.POUND);
  const S_HASH_NAME = new linkify.parser.State();
  const S_HASH_NAME_COLON = new linkify.parser.State();
  const S_HASH_NAME_COLON_DOMAIN = new linkify.parser.State();
  const S_HASH_NAME_COLON_DOMAIN_DOT = new linkify.parser.State();
  const S_ROOMALIAS = new linkify.parser.State(ROOMALIAS);
  const S_ROOMALIAS_COLON = new linkify.parser.State();
  const S_ROOMALIAS_COLON_NUM = new linkify.parser.State(ROOMALIAS);
  const roomnameTokens = [TT.DOT, TT.PLUS, TT.NUM, TT.DOMAIN, TT.TLD, TT.UNDERSCORE, TT.POUND, // because 'localhost' is tokenised to the localhost token,
  // usernames @localhost:foo.com are otherwise not matched!
  TT.LOCALHOST];
  S_HASH.on(roomnameTokens, S_HASH_NAME);
  S_HASH_NAME.on(roomnameTokens, S_HASH_NAME);
  S_HASH_NAME.on(TT.DOMAIN, S_HASH_NAME);
  S_HASH_NAME.on(TT.COLON, S_HASH_NAME_COLON);
  S_HASH_NAME_COLON.on(TT.DOMAIN, S_HASH_NAME_COLON_DOMAIN);
  S_HASH_NAME_COLON.on(TT.LOCALHOST, S_ROOMALIAS); // accept #foo:localhost

  S_HASH_NAME_COLON.on(TT.TLD, S_ROOMALIAS); // accept #foo:com (mostly for (TLD|DOMAIN)+ mixing)

  S_HASH_NAME_COLON_DOMAIN.on(TT.DOT, S_HASH_NAME_COLON_DOMAIN_DOT);
  S_HASH_NAME_COLON_DOMAIN_DOT.on(TT.DOMAIN, S_HASH_NAME_COLON_DOMAIN);
  S_HASH_NAME_COLON_DOMAIN_DOT.on(TT.TLD, S_ROOMALIAS);
  S_ROOMALIAS.on(TT.DOT, S_HASH_NAME_COLON_DOMAIN_DOT); // accept repeated TLDs (e.g .org.uk)

  S_ROOMALIAS.on(TT.COLON, S_ROOMALIAS_COLON); // do not accept trailing `:`

  S_ROOMALIAS_COLON.on(TT.NUM, S_ROOMALIAS_COLON_NUM); // but do accept :NUM (port specifier)

  const USERID = function (value) {
    MultiToken.call(this, value);
    this.type = 'userid';
    this.isLink = true;
  };

  USERID.prototype = new MultiToken();
  const S_AT = S_START.jump(TT.AT);
  const S_AT_NAME = new linkify.parser.State();
  const S_AT_NAME_COLON = new linkify.parser.State();
  const S_AT_NAME_COLON_DOMAIN = new linkify.parser.State();
  const S_AT_NAME_COLON_DOMAIN_DOT = new linkify.parser.State();
  const S_USERID = new linkify.parser.State(USERID);
  const S_USERID_COLON = new linkify.parser.State();
  const S_USERID_COLON_NUM = new linkify.parser.State(USERID);
  const usernameTokens = [TT.DOT, TT.UNDERSCORE, TT.PLUS, TT.NUM, TT.DOMAIN, TT.TLD, // as in roomnameTokens
  TT.LOCALHOST];
  S_AT.on(usernameTokens, S_AT_NAME);
  S_AT_NAME.on(usernameTokens, S_AT_NAME);
  S_AT_NAME.on(TT.DOMAIN, S_AT_NAME);
  S_AT_NAME.on(TT.COLON, S_AT_NAME_COLON);
  S_AT_NAME_COLON.on(TT.DOMAIN, S_AT_NAME_COLON_DOMAIN);
  S_AT_NAME_COLON.on(TT.LOCALHOST, S_USERID); // accept @foo:localhost

  S_AT_NAME_COLON.on(TT.TLD, S_USERID); // accept @foo:com (mostly for (TLD|DOMAIN)+ mixing)

  S_AT_NAME_COLON_DOMAIN.on(TT.DOT, S_AT_NAME_COLON_DOMAIN_DOT);
  S_AT_NAME_COLON_DOMAIN_DOT.on(TT.DOMAIN, S_AT_NAME_COLON_DOMAIN);
  S_AT_NAME_COLON_DOMAIN_DOT.on(TT.TLD, S_USERID);
  S_USERID.on(TT.DOT, S_AT_NAME_COLON_DOMAIN_DOT); // accept repeated TLDs (e.g .org.uk)

  S_USERID.on(TT.COLON, S_USERID_COLON); // do not accept trailing `:`

  S_USERID_COLON.on(TT.NUM, S_USERID_COLON_NUM); // but do accept :NUM (port specifier)

  const GROUPID = function (value) {
    MultiToken.call(this, value);
    this.type = 'groupid';
    this.isLink = true;
  };

  GROUPID.prototype = new MultiToken();
  const S_PLUS = S_START.jump(TT.PLUS);
  const S_PLUS_NAME = new linkify.parser.State();
  const S_PLUS_NAME_COLON = new linkify.parser.State();
  const S_PLUS_NAME_COLON_DOMAIN = new linkify.parser.State();
  const S_PLUS_NAME_COLON_DOMAIN_DOT = new linkify.parser.State();
  const S_GROUPID = new linkify.parser.State(GROUPID);
  const S_GROUPID_COLON = new linkify.parser.State();
  const S_GROUPID_COLON_NUM = new linkify.parser.State(GROUPID);
  const groupIdTokens = [TT.DOT, TT.UNDERSCORE, TT.PLUS, TT.NUM, TT.DOMAIN, TT.TLD, // as in roomnameTokens
  TT.LOCALHOST];
  S_PLUS.on(groupIdTokens, S_PLUS_NAME);
  S_PLUS_NAME.on(groupIdTokens, S_PLUS_NAME);
  S_PLUS_NAME.on(TT.DOMAIN, S_PLUS_NAME);
  S_PLUS_NAME.on(TT.COLON, S_PLUS_NAME_COLON);
  S_PLUS_NAME_COLON.on(TT.DOMAIN, S_PLUS_NAME_COLON_DOMAIN);
  S_PLUS_NAME_COLON.on(TT.LOCALHOST, S_GROUPID); // accept +foo:localhost

  S_PLUS_NAME_COLON.on(TT.TLD, S_GROUPID); // accept +foo:com (mostly for (TLD|DOMAIN)+ mixing)

  S_PLUS_NAME_COLON_DOMAIN.on(TT.DOT, S_PLUS_NAME_COLON_DOMAIN_DOT);
  S_PLUS_NAME_COLON_DOMAIN_DOT.on(TT.DOMAIN, S_PLUS_NAME_COLON_DOMAIN);
  S_PLUS_NAME_COLON_DOMAIN_DOT.on(TT.TLD, S_GROUPID);
  S_GROUPID.on(TT.DOT, S_PLUS_NAME_COLON_DOMAIN_DOT); // accept repeated TLDs (e.g .org.uk)

  S_GROUPID.on(TT.COLON, S_GROUPID_COLON); // do not accept trailing `:`

  S_GROUPID_COLON.on(TT.NUM, S_GROUPID_COLON_NUM); // but do accept :NUM (port specifier)
} // stubs, overwritten in MatrixChat's componentDidMount


matrixLinkify.onUserClick = function (e, userId) {
  e.preventDefault();
};

matrixLinkify.onAliasClick = function (e, roomAlias) {
  e.preventDefault();
};

matrixLinkify.onGroupClick = function (e, groupId) {
  e.preventDefault();
};

const escapeRegExp = function (string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}; // Recognise URLs from both our local vector and official vector as vector.
// anyone else really should be using matrix.to.


matrixLinkify.VECTOR_URL_PATTERN = "^(?:https?://)?(?:" + escapeRegExp(window.location.host + window.location.pathname) + "|" + "(?:www\\.)?(?:riot|vector)\\.im/(?:app|beta|staging|develop)/" + ")(#.*)";
matrixLinkify.MATRIXTO_URL_PATTERN = "^(?:https?://)?(?:www\\.)?matrix\\.to/#/(([#@!+]).*)";
matrixLinkify.MATRIXTO_MD_LINK_PATTERN = '\\[([^\\]]*)\\]\\((?:https?://)?(?:www\\.)?matrix\\.to/#/([#@!+][^\\)]*)\\)';
matrixLinkify.MATRIXTO_BASE_URL = _SpecPermalinkConstructor.baseUrl;
matrixLinkify.options = {
  events: function (href, type) {
    switch (type) {
      case "url":
        {
          // intercept local permalinks to users and show them like userids (in userinfo of current room)
          try {
            const permalink = (0, _Permalinks.parsePermalink)(href);

            if (permalink && permalink.userId) {
              return {
                click: function (e) {
                  matrixLinkify.onUserClick(e, permalink.userId);
                }
              };
            }
          } catch (e) {// OK fine, it's not actually a permalink
          }

          break;
        }

      case "userid":
        return {
          click: function (e) {
            matrixLinkify.onUserClick(e, href);
          }
        };

      case "roomalias":
        return {
          click: function (e) {
            matrixLinkify.onAliasClick(e, href);
          }
        };

      case "groupid":
        return {
          click: function (e) {
            matrixLinkify.onGroupClick(e, href);
          }
        };
    }
  },
  formatHref: function (href, type) {
    switch (type) {
      case 'roomalias':
      case 'userid':
      case 'groupid':
      default:
        {
          return (0, _Permalinks.tryTransformEntityToPermalink)(href);
        }
    }
  },
  linkAttributes: {
    rel: 'noreferrer noopener'
  },
  target: function (href, type) {
    if (type === 'url') {
      const transformed = (0, _Permalinks.tryTransformPermalinkToLocalHref)(href);

      if (transformed !== href || href.match(matrixLinkify.VECTOR_URL_PATTERN)) {
        return null;
      } else {
        return '_blank';
      }
    }

    return null;
  }
};
var _default = matrixLinkify;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9saW5raWZ5LW1hdHJpeC5qcyJdLCJuYW1lcyI6WyJtYXRyaXhMaW5raWZ5IiwibGlua2lmeSIsIlRUIiwic2Nhbm5lciIsIlRPS0VOUyIsIk1UIiwicGFyc2VyIiwiTXVsdGlUb2tlbiIsIkJhc2UiLCJTX1NUQVJUIiwic3RhcnQiLCJVTkRFUlNDT1JFIiwidW5kZWZpbmVkIiwiRXJyb3IiLCJST09NQUxJQVMiLCJ2YWx1ZSIsImNhbGwiLCJ0eXBlIiwiaXNMaW5rIiwicHJvdG90eXBlIiwiU19IQVNIIiwianVtcCIsIlBPVU5EIiwiU19IQVNIX05BTUUiLCJTdGF0ZSIsIlNfSEFTSF9OQU1FX0NPTE9OIiwiU19IQVNIX05BTUVfQ09MT05fRE9NQUlOIiwiU19IQVNIX05BTUVfQ09MT05fRE9NQUlOX0RPVCIsIlNfUk9PTUFMSUFTIiwiU19ST09NQUxJQVNfQ09MT04iLCJTX1JPT01BTElBU19DT0xPTl9OVU0iLCJyb29tbmFtZVRva2VucyIsIkRPVCIsIlBMVVMiLCJOVU0iLCJET01BSU4iLCJUTEQiLCJMT0NBTEhPU1QiLCJvbiIsIkNPTE9OIiwiVVNFUklEIiwiU19BVCIsIkFUIiwiU19BVF9OQU1FIiwiU19BVF9OQU1FX0NPTE9OIiwiU19BVF9OQU1FX0NPTE9OX0RPTUFJTiIsIlNfQVRfTkFNRV9DT0xPTl9ET01BSU5fRE9UIiwiU19VU0VSSUQiLCJTX1VTRVJJRF9DT0xPTiIsIlNfVVNFUklEX0NPTE9OX05VTSIsInVzZXJuYW1lVG9rZW5zIiwiR1JPVVBJRCIsIlNfUExVUyIsIlNfUExVU19OQU1FIiwiU19QTFVTX05BTUVfQ09MT04iLCJTX1BMVVNfTkFNRV9DT0xPTl9ET01BSU4iLCJTX1BMVVNfTkFNRV9DT0xPTl9ET01BSU5fRE9UIiwiU19HUk9VUElEIiwiU19HUk9VUElEX0NPTE9OIiwiU19HUk9VUElEX0NPTE9OX05VTSIsImdyb3VwSWRUb2tlbnMiLCJvblVzZXJDbGljayIsImUiLCJ1c2VySWQiLCJwcmV2ZW50RGVmYXVsdCIsIm9uQWxpYXNDbGljayIsInJvb21BbGlhcyIsIm9uR3JvdXBDbGljayIsImdyb3VwSWQiLCJlc2NhcGVSZWdFeHAiLCJzdHJpbmciLCJyZXBsYWNlIiwiVkVDVE9SX1VSTF9QQVRURVJOIiwid2luZG93IiwibG9jYXRpb24iLCJob3N0IiwicGF0aG5hbWUiLCJNQVRSSVhUT19VUkxfUEFUVEVSTiIsIk1BVFJJWFRPX01EX0xJTktfUEFUVEVSTiIsIk1BVFJJWFRPX0JBU0VfVVJMIiwiYmFzZVVybCIsIm9wdGlvbnMiLCJldmVudHMiLCJocmVmIiwicGVybWFsaW5rIiwiY2xpY2siLCJmb3JtYXRIcmVmIiwibGlua0F0dHJpYnV0ZXMiLCJyZWwiLCJ0YXJnZXQiLCJ0cmFuc2Zvcm1lZCIsIm1hdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBaUJBOztBQUNBOztBQWxCQTs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxTQUFTQSxhQUFULENBQXVCQyxPQUF2QixFQUFnQztBQUM1QjtBQUNBLFFBQU1DLEVBQUUsR0FBR0QsT0FBTyxDQUFDRSxPQUFSLENBQWdCQyxNQUEzQixDQUY0QixDQUc1Qjs7QUFDQSxRQUFNQyxFQUFFLEdBQUdKLE9BQU8sQ0FBQ0ssTUFBUixDQUFlRixNQUExQjtBQUNBLFFBQU1HLFVBQVUsR0FBR0YsRUFBRSxDQUFDRyxJQUF0QjtBQUNBLFFBQU1DLE9BQU8sR0FBR1IsT0FBTyxDQUFDSyxNQUFSLENBQWVJLEtBQS9COztBQUVBLE1BQUlSLEVBQUUsQ0FBQ1MsVUFBSCxLQUFrQkMsU0FBdEIsRUFBaUM7QUFDN0IsVUFBTSxJQUFJQyxLQUFKLENBQVUsbUVBQVYsQ0FBTjtBQUNIOztBQUVELFFBQU1DLFNBQVMsR0FBRyxVQUFTQyxLQUFULEVBQWdCO0FBQzlCUixJQUFBQSxVQUFVLENBQUNTLElBQVgsQ0FBZ0IsSUFBaEIsRUFBc0JELEtBQXRCO0FBQ0EsU0FBS0UsSUFBTCxHQUFZLFdBQVo7QUFDQSxTQUFLQyxNQUFMLEdBQWMsSUFBZDtBQUNILEdBSkQ7O0FBS0FKLEVBQUFBLFNBQVMsQ0FBQ0ssU0FBVixHQUFzQixJQUFJWixVQUFKLEVBQXRCO0FBRUEsUUFBTWEsTUFBTSxHQUFHWCxPQUFPLENBQUNZLElBQVIsQ0FBYW5CLEVBQUUsQ0FBQ29CLEtBQWhCLENBQWY7QUFDQSxRQUFNQyxXQUFXLEdBQUcsSUFBSXRCLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBcEI7QUFDQSxRQUFNQyxpQkFBaUIsR0FBRyxJQUFJeEIsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUExQjtBQUNBLFFBQU1FLHdCQUF3QixHQUFHLElBQUl6QixPQUFPLENBQUNLLE1BQVIsQ0FBZWtCLEtBQW5CLEVBQWpDO0FBQ0EsUUFBTUcsNEJBQTRCLEdBQUcsSUFBSTFCLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBckM7QUFDQSxRQUFNSSxXQUFXLEdBQUcsSUFBSTNCLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsQ0FBeUJWLFNBQXpCLENBQXBCO0FBQ0EsUUFBTWUsaUJBQWlCLEdBQUcsSUFBSTVCLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBMUI7QUFDQSxRQUFNTSxxQkFBcUIsR0FBRyxJQUFJN0IsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixDQUF5QlYsU0FBekIsQ0FBOUI7QUFFQSxRQUFNaUIsY0FBYyxHQUFHLENBQ25CN0IsRUFBRSxDQUFDOEIsR0FEZ0IsRUFFbkI5QixFQUFFLENBQUMrQixJQUZnQixFQUduQi9CLEVBQUUsQ0FBQ2dDLEdBSGdCLEVBSW5CaEMsRUFBRSxDQUFDaUMsTUFKZ0IsRUFLbkJqQyxFQUFFLENBQUNrQyxHQUxnQixFQU1uQmxDLEVBQUUsQ0FBQ1MsVUFOZ0IsRUFPbkJULEVBQUUsQ0FBQ29CLEtBUGdCLEVBU25CO0FBQ0E7QUFDQXBCLEVBQUFBLEVBQUUsQ0FBQ21DLFNBWGdCLENBQXZCO0FBY0FqQixFQUFBQSxNQUFNLENBQUNrQixFQUFQLENBQVVQLGNBQVYsRUFBMEJSLFdBQTFCO0FBQ0FBLEVBQUFBLFdBQVcsQ0FBQ2UsRUFBWixDQUFlUCxjQUFmLEVBQStCUixXQUEvQjtBQUNBQSxFQUFBQSxXQUFXLENBQUNlLEVBQVosQ0FBZXBDLEVBQUUsQ0FBQ2lDLE1BQWxCLEVBQTBCWixXQUExQjtBQUVBQSxFQUFBQSxXQUFXLENBQUNlLEVBQVosQ0FBZXBDLEVBQUUsQ0FBQ3FDLEtBQWxCLEVBQXlCZCxpQkFBekI7QUFFQUEsRUFBQUEsaUJBQWlCLENBQUNhLEVBQWxCLENBQXFCcEMsRUFBRSxDQUFDaUMsTUFBeEIsRUFBZ0NULHdCQUFoQztBQUNBRCxFQUFBQSxpQkFBaUIsQ0FBQ2EsRUFBbEIsQ0FBcUJwQyxFQUFFLENBQUNtQyxTQUF4QixFQUFtQ1QsV0FBbkMsRUFqRDRCLENBaURxQjs7QUFDakRILEVBQUFBLGlCQUFpQixDQUFDYSxFQUFsQixDQUFxQnBDLEVBQUUsQ0FBQ2tDLEdBQXhCLEVBQTZCUixXQUE3QixFQWxENEIsQ0FrRGU7O0FBQzNDRixFQUFBQSx3QkFBd0IsQ0FBQ1ksRUFBekIsQ0FBNEJwQyxFQUFFLENBQUM4QixHQUEvQixFQUFvQ0wsNEJBQXBDO0FBQ0FBLEVBQUFBLDRCQUE0QixDQUFDVyxFQUE3QixDQUFnQ3BDLEVBQUUsQ0FBQ2lDLE1BQW5DLEVBQTJDVCx3QkFBM0M7QUFDQUMsRUFBQUEsNEJBQTRCLENBQUNXLEVBQTdCLENBQWdDcEMsRUFBRSxDQUFDa0MsR0FBbkMsRUFBd0NSLFdBQXhDO0FBRUFBLEVBQUFBLFdBQVcsQ0FBQ1UsRUFBWixDQUFlcEMsRUFBRSxDQUFDOEIsR0FBbEIsRUFBdUJMLDRCQUF2QixFQXZENEIsQ0F1RDBCOztBQUN0REMsRUFBQUEsV0FBVyxDQUFDVSxFQUFaLENBQWVwQyxFQUFFLENBQUNxQyxLQUFsQixFQUF5QlYsaUJBQXpCLEVBeEQ0QixDQXdEaUI7O0FBQzdDQSxFQUFBQSxpQkFBaUIsQ0FBQ1MsRUFBbEIsQ0FBcUJwQyxFQUFFLENBQUNnQyxHQUF4QixFQUE2QkoscUJBQTdCLEVBekQ0QixDQXlEeUI7O0FBR3JELFFBQU1VLE1BQU0sR0FBRyxVQUFTekIsS0FBVCxFQUFnQjtBQUMzQlIsSUFBQUEsVUFBVSxDQUFDUyxJQUFYLENBQWdCLElBQWhCLEVBQXNCRCxLQUF0QjtBQUNBLFNBQUtFLElBQUwsR0FBWSxRQUFaO0FBQ0EsU0FBS0MsTUFBTCxHQUFjLElBQWQ7QUFDSCxHQUpEOztBQUtBc0IsRUFBQUEsTUFBTSxDQUFDckIsU0FBUCxHQUFtQixJQUFJWixVQUFKLEVBQW5CO0FBRUEsUUFBTWtDLElBQUksR0FBR2hDLE9BQU8sQ0FBQ1ksSUFBUixDQUFhbkIsRUFBRSxDQUFDd0MsRUFBaEIsQ0FBYjtBQUNBLFFBQU1DLFNBQVMsR0FBRyxJQUFJMUMsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUFsQjtBQUNBLFFBQU1vQixlQUFlLEdBQUcsSUFBSTNDLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBeEI7QUFDQSxRQUFNcUIsc0JBQXNCLEdBQUcsSUFBSTVDLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBL0I7QUFDQSxRQUFNc0IsMEJBQTBCLEdBQUcsSUFBSTdDLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBbkM7QUFDQSxRQUFNdUIsUUFBUSxHQUFHLElBQUk5QyxPQUFPLENBQUNLLE1BQVIsQ0FBZWtCLEtBQW5CLENBQXlCZ0IsTUFBekIsQ0FBakI7QUFDQSxRQUFNUSxjQUFjLEdBQUcsSUFBSS9DLE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsRUFBdkI7QUFDQSxRQUFNeUIsa0JBQWtCLEdBQUcsSUFBSWhELE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsQ0FBeUJnQixNQUF6QixDQUEzQjtBQUVBLFFBQU1VLGNBQWMsR0FBRyxDQUNuQmhELEVBQUUsQ0FBQzhCLEdBRGdCLEVBRW5COUIsRUFBRSxDQUFDUyxVQUZnQixFQUduQlQsRUFBRSxDQUFDK0IsSUFIZ0IsRUFJbkIvQixFQUFFLENBQUNnQyxHQUpnQixFQUtuQmhDLEVBQUUsQ0FBQ2lDLE1BTGdCLEVBTW5CakMsRUFBRSxDQUFDa0MsR0FOZ0IsRUFRbkI7QUFDQWxDLEVBQUFBLEVBQUUsQ0FBQ21DLFNBVGdCLENBQXZCO0FBWUFJLEVBQUFBLElBQUksQ0FBQ0gsRUFBTCxDQUFRWSxjQUFSLEVBQXdCUCxTQUF4QjtBQUNBQSxFQUFBQSxTQUFTLENBQUNMLEVBQVYsQ0FBYVksY0FBYixFQUE2QlAsU0FBN0I7QUFDQUEsRUFBQUEsU0FBUyxDQUFDTCxFQUFWLENBQWFwQyxFQUFFLENBQUNpQyxNQUFoQixFQUF3QlEsU0FBeEI7QUFFQUEsRUFBQUEsU0FBUyxDQUFDTCxFQUFWLENBQWFwQyxFQUFFLENBQUNxQyxLQUFoQixFQUF1QkssZUFBdkI7QUFFQUEsRUFBQUEsZUFBZSxDQUFDTixFQUFoQixDQUFtQnBDLEVBQUUsQ0FBQ2lDLE1BQXRCLEVBQThCVSxzQkFBOUI7QUFDQUQsRUFBQUEsZUFBZSxDQUFDTixFQUFoQixDQUFtQnBDLEVBQUUsQ0FBQ21DLFNBQXRCLEVBQWlDVSxRQUFqQyxFQS9GNEIsQ0ErRmdCOztBQUM1Q0gsRUFBQUEsZUFBZSxDQUFDTixFQUFoQixDQUFtQnBDLEVBQUUsQ0FBQ2tDLEdBQXRCLEVBQTJCVyxRQUEzQixFQWhHNEIsQ0FnR1U7O0FBQ3RDRixFQUFBQSxzQkFBc0IsQ0FBQ1AsRUFBdkIsQ0FBMEJwQyxFQUFFLENBQUM4QixHQUE3QixFQUFrQ2MsMEJBQWxDO0FBQ0FBLEVBQUFBLDBCQUEwQixDQUFDUixFQUEzQixDQUE4QnBDLEVBQUUsQ0FBQ2lDLE1BQWpDLEVBQXlDVSxzQkFBekM7QUFDQUMsRUFBQUEsMEJBQTBCLENBQUNSLEVBQTNCLENBQThCcEMsRUFBRSxDQUFDa0MsR0FBakMsRUFBc0NXLFFBQXRDO0FBRUFBLEVBQUFBLFFBQVEsQ0FBQ1QsRUFBVCxDQUFZcEMsRUFBRSxDQUFDOEIsR0FBZixFQUFvQmMsMEJBQXBCLEVBckc0QixDQXFHcUI7O0FBQ2pEQyxFQUFBQSxRQUFRLENBQUNULEVBQVQsQ0FBWXBDLEVBQUUsQ0FBQ3FDLEtBQWYsRUFBc0JTLGNBQXRCLEVBdEc0QixDQXNHVzs7QUFDdkNBLEVBQUFBLGNBQWMsQ0FBQ1YsRUFBZixDQUFrQnBDLEVBQUUsQ0FBQ2dDLEdBQXJCLEVBQTBCZSxrQkFBMUIsRUF2RzRCLENBdUdtQjs7QUFHL0MsUUFBTUUsT0FBTyxHQUFHLFVBQVNwQyxLQUFULEVBQWdCO0FBQzVCUixJQUFBQSxVQUFVLENBQUNTLElBQVgsQ0FBZ0IsSUFBaEIsRUFBc0JELEtBQXRCO0FBQ0EsU0FBS0UsSUFBTCxHQUFZLFNBQVo7QUFDQSxTQUFLQyxNQUFMLEdBQWMsSUFBZDtBQUNILEdBSkQ7O0FBS0FpQyxFQUFBQSxPQUFPLENBQUNoQyxTQUFSLEdBQW9CLElBQUlaLFVBQUosRUFBcEI7QUFFQSxRQUFNNkMsTUFBTSxHQUFHM0MsT0FBTyxDQUFDWSxJQUFSLENBQWFuQixFQUFFLENBQUMrQixJQUFoQixDQUFmO0FBQ0EsUUFBTW9CLFdBQVcsR0FBRyxJQUFJcEQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUFwQjtBQUNBLFFBQU04QixpQkFBaUIsR0FBRyxJQUFJckQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUExQjtBQUNBLFFBQU0rQix3QkFBd0IsR0FBRyxJQUFJdEQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUFqQztBQUNBLFFBQU1nQyw0QkFBNEIsR0FBRyxJQUFJdkQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUFyQztBQUNBLFFBQU1pQyxTQUFTLEdBQUcsSUFBSXhELE9BQU8sQ0FBQ0ssTUFBUixDQUFla0IsS0FBbkIsQ0FBeUIyQixPQUF6QixDQUFsQjtBQUNBLFFBQU1PLGVBQWUsR0FBRyxJQUFJekQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixFQUF4QjtBQUNBLFFBQU1tQyxtQkFBbUIsR0FBRyxJQUFJMUQsT0FBTyxDQUFDSyxNQUFSLENBQWVrQixLQUFuQixDQUF5QjJCLE9BQXpCLENBQTVCO0FBRUEsUUFBTVMsYUFBYSxHQUFHLENBQ2xCMUQsRUFBRSxDQUFDOEIsR0FEZSxFQUVsQjlCLEVBQUUsQ0FBQ1MsVUFGZSxFQUdsQlQsRUFBRSxDQUFDK0IsSUFIZSxFQUlsQi9CLEVBQUUsQ0FBQ2dDLEdBSmUsRUFLbEJoQyxFQUFFLENBQUNpQyxNQUxlLEVBTWxCakMsRUFBRSxDQUFDa0MsR0FOZSxFQVFsQjtBQUNBbEMsRUFBQUEsRUFBRSxDQUFDbUMsU0FUZSxDQUF0QjtBQVlBZSxFQUFBQSxNQUFNLENBQUNkLEVBQVAsQ0FBVXNCLGFBQVYsRUFBeUJQLFdBQXpCO0FBQ0FBLEVBQUFBLFdBQVcsQ0FBQ2YsRUFBWixDQUFlc0IsYUFBZixFQUE4QlAsV0FBOUI7QUFDQUEsRUFBQUEsV0FBVyxDQUFDZixFQUFaLENBQWVwQyxFQUFFLENBQUNpQyxNQUFsQixFQUEwQmtCLFdBQTFCO0FBRUFBLEVBQUFBLFdBQVcsQ0FBQ2YsRUFBWixDQUFlcEMsRUFBRSxDQUFDcUMsS0FBbEIsRUFBeUJlLGlCQUF6QjtBQUVBQSxFQUFBQSxpQkFBaUIsQ0FBQ2hCLEVBQWxCLENBQXFCcEMsRUFBRSxDQUFDaUMsTUFBeEIsRUFBZ0NvQix3QkFBaEM7QUFDQUQsRUFBQUEsaUJBQWlCLENBQUNoQixFQUFsQixDQUFxQnBDLEVBQUUsQ0FBQ21DLFNBQXhCLEVBQW1Db0IsU0FBbkMsRUE3STRCLENBNkltQjs7QUFDL0NILEVBQUFBLGlCQUFpQixDQUFDaEIsRUFBbEIsQ0FBcUJwQyxFQUFFLENBQUNrQyxHQUF4QixFQUE2QnFCLFNBQTdCLEVBOUk0QixDQThJYTs7QUFDekNGLEVBQUFBLHdCQUF3QixDQUFDakIsRUFBekIsQ0FBNEJwQyxFQUFFLENBQUM4QixHQUEvQixFQUFvQ3dCLDRCQUFwQztBQUNBQSxFQUFBQSw0QkFBNEIsQ0FBQ2xCLEVBQTdCLENBQWdDcEMsRUFBRSxDQUFDaUMsTUFBbkMsRUFBMkNvQix3QkFBM0M7QUFDQUMsRUFBQUEsNEJBQTRCLENBQUNsQixFQUE3QixDQUFnQ3BDLEVBQUUsQ0FBQ2tDLEdBQW5DLEVBQXdDcUIsU0FBeEM7QUFFQUEsRUFBQUEsU0FBUyxDQUFDbkIsRUFBVixDQUFhcEMsRUFBRSxDQUFDOEIsR0FBaEIsRUFBcUJ3Qiw0QkFBckIsRUFuSjRCLENBbUp3Qjs7QUFDcERDLEVBQUFBLFNBQVMsQ0FBQ25CLEVBQVYsQ0FBYXBDLEVBQUUsQ0FBQ3FDLEtBQWhCLEVBQXVCbUIsZUFBdkIsRUFwSjRCLENBb0phOztBQUN6Q0EsRUFBQUEsZUFBZSxDQUFDcEIsRUFBaEIsQ0FBbUJwQyxFQUFFLENBQUNnQyxHQUF0QixFQUEyQnlCLG1CQUEzQixFQXJKNEIsQ0FxSnFCO0FBQ3BELEMsQ0FFRDs7O0FBQ0EzRCxhQUFhLENBQUM2RCxXQUFkLEdBQTRCLFVBQVNDLENBQVQsRUFBWUMsTUFBWixFQUFvQjtBQUFFRCxFQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFBcUIsQ0FBdkU7O0FBQ0FoRSxhQUFhLENBQUNpRSxZQUFkLEdBQTZCLFVBQVNILENBQVQsRUFBWUksU0FBWixFQUF1QjtBQUFFSixFQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFBcUIsQ0FBM0U7O0FBQ0FoRSxhQUFhLENBQUNtRSxZQUFkLEdBQTZCLFVBQVNMLENBQVQsRUFBWU0sT0FBWixFQUFxQjtBQUFFTixFQUFBQSxDQUFDLENBQUNFLGNBQUY7QUFBcUIsQ0FBekU7O0FBRUEsTUFBTUssWUFBWSxHQUFHLFVBQVNDLE1BQVQsRUFBaUI7QUFDbEMsU0FBT0EsTUFBTSxDQUFDQyxPQUFQLENBQWUscUJBQWYsRUFBc0MsTUFBdEMsQ0FBUDtBQUNILENBRkQsQyxDQUlBO0FBQ0E7OztBQUNBdkUsYUFBYSxDQUFDd0Usa0JBQWQsR0FBbUMsdUJBQzdCSCxZQUFZLENBQUNJLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsSUFBaEIsR0FBdUJGLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkUsUUFBeEMsQ0FEaUIsR0FDbUMsR0FEbkMsR0FFN0IsK0RBRjZCLEdBRzdCLFFBSE47QUFLQTVFLGFBQWEsQ0FBQzZFLG9CQUFkLEdBQXFDLHNEQUFyQztBQUNBN0UsYUFBYSxDQUFDOEUsd0JBQWQsR0FDSSw2RUFESjtBQUVBOUUsYUFBYSxDQUFDK0UsaUJBQWQsR0FBaUNDLGlDQUFqQztBQUVBaEYsYUFBYSxDQUFDaUYsT0FBZCxHQUF3QjtBQUNwQkMsRUFBQUEsTUFBTSxFQUFFLFVBQVNDLElBQVQsRUFBZWxFLElBQWYsRUFBcUI7QUFDekIsWUFBUUEsSUFBUjtBQUNJLFdBQUssS0FBTDtBQUFZO0FBQ1I7QUFDQSxjQUFJO0FBQ0Esa0JBQU1tRSxTQUFTLEdBQUcsZ0NBQWVELElBQWYsQ0FBbEI7O0FBQ0EsZ0JBQUlDLFNBQVMsSUFBSUEsU0FBUyxDQUFDckIsTUFBM0IsRUFBbUM7QUFDL0IscUJBQU87QUFDSHNCLGdCQUFBQSxLQUFLLEVBQUUsVUFBU3ZCLENBQVQsRUFBWTtBQUNmOUQsa0JBQUFBLGFBQWEsQ0FBQzZELFdBQWQsQ0FBMEJDLENBQTFCLEVBQTZCc0IsU0FBUyxDQUFDckIsTUFBdkM7QUFDSDtBQUhFLGVBQVA7QUFLSDtBQUNKLFdBVEQsQ0FTRSxPQUFPRCxDQUFQLEVBQVUsQ0FDUjtBQUNIOztBQUNEO0FBQ0g7O0FBQ0QsV0FBSyxRQUFMO0FBQ0ksZUFBTztBQUNIdUIsVUFBQUEsS0FBSyxFQUFFLFVBQVN2QixDQUFULEVBQVk7QUFDZjlELFlBQUFBLGFBQWEsQ0FBQzZELFdBQWQsQ0FBMEJDLENBQTFCLEVBQTZCcUIsSUFBN0I7QUFDSDtBQUhFLFNBQVA7O0FBS0osV0FBSyxXQUFMO0FBQ0ksZUFBTztBQUNIRSxVQUFBQSxLQUFLLEVBQUUsVUFBU3ZCLENBQVQsRUFBWTtBQUNmOUQsWUFBQUEsYUFBYSxDQUFDaUUsWUFBZCxDQUEyQkgsQ0FBM0IsRUFBOEJxQixJQUE5QjtBQUNIO0FBSEUsU0FBUDs7QUFLSixXQUFLLFNBQUw7QUFDSSxlQUFPO0FBQ0hFLFVBQUFBLEtBQUssRUFBRSxVQUFTdkIsQ0FBVCxFQUFZO0FBQ2Y5RCxZQUFBQSxhQUFhLENBQUNtRSxZQUFkLENBQTJCTCxDQUEzQixFQUE4QnFCLElBQTlCO0FBQ0g7QUFIRSxTQUFQO0FBOUJSO0FBb0NILEdBdENtQjtBQXdDcEJHLEVBQUFBLFVBQVUsRUFBRSxVQUFTSCxJQUFULEVBQWVsRSxJQUFmLEVBQXFCO0FBQzdCLFlBQVFBLElBQVI7QUFDSSxXQUFLLFdBQUw7QUFDQSxXQUFLLFFBQUw7QUFDQSxXQUFLLFNBQUw7QUFDQTtBQUFTO0FBQ0wsaUJBQU8sK0NBQThCa0UsSUFBOUIsQ0FBUDtBQUNIO0FBTkw7QUFRSCxHQWpEbUI7QUFtRHBCSSxFQUFBQSxjQUFjLEVBQUU7QUFDWkMsSUFBQUEsR0FBRyxFQUFFO0FBRE8sR0FuREk7QUF1RHBCQyxFQUFBQSxNQUFNLEVBQUUsVUFBU04sSUFBVCxFQUFlbEUsSUFBZixFQUFxQjtBQUN6QixRQUFJQSxJQUFJLEtBQUssS0FBYixFQUFvQjtBQUNoQixZQUFNeUUsV0FBVyxHQUFHLGtEQUFpQ1AsSUFBakMsQ0FBcEI7O0FBQ0EsVUFBSU8sV0FBVyxLQUFLUCxJQUFoQixJQUF3QkEsSUFBSSxDQUFDUSxLQUFMLENBQVczRixhQUFhLENBQUN3RSxrQkFBekIsQ0FBNUIsRUFBMEU7QUFDdEUsZUFBTyxJQUFQO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsZUFBTyxRQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLElBQVA7QUFDSDtBQWpFbUIsQ0FBeEI7ZUFvRWV4RSxhIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge2Jhc2VVcmx9IGZyb20gXCIuL3V0aWxzL3Blcm1hbGlua3MvU3BlY1Blcm1hbGlua0NvbnN0cnVjdG9yXCI7XHJcbmltcG9ydCB7XHJcbiAgICBwYXJzZVBlcm1hbGluayxcclxuICAgIHRyeVRyYW5zZm9ybUVudGl0eVRvUGVybWFsaW5rLFxyXG4gICAgdHJ5VHJhbnNmb3JtUGVybWFsaW5rVG9Mb2NhbEhyZWYsXHJcbn0gZnJvbSBcIi4vdXRpbHMvcGVybWFsaW5rcy9QZXJtYWxpbmtzXCI7XHJcblxyXG5mdW5jdGlvbiBtYXRyaXhMaW5raWZ5KGxpbmtpZnkpIHtcclxuICAgIC8vIFRleHQgdG9rZW5zXHJcbiAgICBjb25zdCBUVCA9IGxpbmtpZnkuc2Nhbm5lci5UT0tFTlM7XHJcbiAgICAvLyBNdWx0aSB0b2tlbnNcclxuICAgIGNvbnN0IE1UID0gbGlua2lmeS5wYXJzZXIuVE9LRU5TO1xyXG4gICAgY29uc3QgTXVsdGlUb2tlbiA9IE1ULkJhc2U7XHJcbiAgICBjb25zdCBTX1NUQVJUID0gbGlua2lmeS5wYXJzZXIuc3RhcnQ7XHJcblxyXG4gICAgaWYgKFRULlVOREVSU0NPUkUgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcImxpbmtpZnktbWF0cml4IHJlcXVpcmVzIGxpbmtpZnlqcyAyLjEuMTogdGhpcyB2ZXJzaW9uIGlzIHRvbyBvbGQuXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IFJPT01BTElBUyA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcbiAgICAgICAgTXVsdGlUb2tlbi5jYWxsKHRoaXMsIHZhbHVlKTtcclxuICAgICAgICB0aGlzLnR5cGUgPSAncm9vbWFsaWFzJztcclxuICAgICAgICB0aGlzLmlzTGluayA9IHRydWU7XHJcbiAgICB9O1xyXG4gICAgUk9PTUFMSUFTLnByb3RvdHlwZSA9IG5ldyBNdWx0aVRva2VuKCk7XHJcblxyXG4gICAgY29uc3QgU19IQVNIID0gU19TVEFSVC5qdW1wKFRULlBPVU5EKTtcclxuICAgIGNvbnN0IFNfSEFTSF9OQU1FID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX0hBU0hfTkFNRV9DT0xPTiA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19IQVNIX05BTUVfQ09MT05fRE9NQUlOID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX0hBU0hfTkFNRV9DT0xPTl9ET01BSU5fRE9UID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX1JPT01BTElBUyA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZShST09NQUxJQVMpO1xyXG4gICAgY29uc3QgU19ST09NQUxJQVNfQ09MT04gPSBuZXcgbGlua2lmeS5wYXJzZXIuU3RhdGUoKTtcclxuICAgIGNvbnN0IFNfUk9PTUFMSUFTX0NPTE9OX05VTSA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZShST09NQUxJQVMpO1xyXG5cclxuICAgIGNvbnN0IHJvb21uYW1lVG9rZW5zID0gW1xyXG4gICAgICAgIFRULkRPVCxcclxuICAgICAgICBUVC5QTFVTLFxyXG4gICAgICAgIFRULk5VTSxcclxuICAgICAgICBUVC5ET01BSU4sXHJcbiAgICAgICAgVFQuVExELFxyXG4gICAgICAgIFRULlVOREVSU0NPUkUsXHJcbiAgICAgICAgVFQuUE9VTkQsXHJcblxyXG4gICAgICAgIC8vIGJlY2F1c2UgJ2xvY2FsaG9zdCcgaXMgdG9rZW5pc2VkIHRvIHRoZSBsb2NhbGhvc3QgdG9rZW4sXHJcbiAgICAgICAgLy8gdXNlcm5hbWVzIEBsb2NhbGhvc3Q6Zm9vLmNvbSBhcmUgb3RoZXJ3aXNlIG5vdCBtYXRjaGVkIVxyXG4gICAgICAgIFRULkxPQ0FMSE9TVCxcclxuICAgIF07XHJcblxyXG4gICAgU19IQVNILm9uKHJvb21uYW1lVG9rZW5zLCBTX0hBU0hfTkFNRSk7XHJcbiAgICBTX0hBU0hfTkFNRS5vbihyb29tbmFtZVRva2VucywgU19IQVNIX05BTUUpO1xyXG4gICAgU19IQVNIX05BTUUub24oVFQuRE9NQUlOLCBTX0hBU0hfTkFNRSk7XHJcblxyXG4gICAgU19IQVNIX05BTUUub24oVFQuQ09MT04sIFNfSEFTSF9OQU1FX0NPTE9OKTtcclxuXHJcbiAgICBTX0hBU0hfTkFNRV9DT0xPTi5vbihUVC5ET01BSU4sIFNfSEFTSF9OQU1FX0NPTE9OX0RPTUFJTik7XHJcbiAgICBTX0hBU0hfTkFNRV9DT0xPTi5vbihUVC5MT0NBTEhPU1QsIFNfUk9PTUFMSUFTKTsgLy8gYWNjZXB0ICNmb286bG9jYWxob3N0XHJcbiAgICBTX0hBU0hfTkFNRV9DT0xPTi5vbihUVC5UTEQsIFNfUk9PTUFMSUFTKTsgLy8gYWNjZXB0ICNmb286Y29tIChtb3N0bHkgZm9yIChUTER8RE9NQUlOKSsgbWl4aW5nKVxyXG4gICAgU19IQVNIX05BTUVfQ09MT05fRE9NQUlOLm9uKFRULkRPVCwgU19IQVNIX05BTUVfQ09MT05fRE9NQUlOX0RPVCk7XHJcbiAgICBTX0hBU0hfTkFNRV9DT0xPTl9ET01BSU5fRE9ULm9uKFRULkRPTUFJTiwgU19IQVNIX05BTUVfQ09MT05fRE9NQUlOKTtcclxuICAgIFNfSEFTSF9OQU1FX0NPTE9OX0RPTUFJTl9ET1Qub24oVFQuVExELCBTX1JPT01BTElBUyk7XHJcblxyXG4gICAgU19ST09NQUxJQVMub24oVFQuRE9ULCBTX0hBU0hfTkFNRV9DT0xPTl9ET01BSU5fRE9UKTsgLy8gYWNjZXB0IHJlcGVhdGVkIFRMRHMgKGUuZyAub3JnLnVrKVxyXG4gICAgU19ST09NQUxJQVMub24oVFQuQ09MT04sIFNfUk9PTUFMSUFTX0NPTE9OKTsgLy8gZG8gbm90IGFjY2VwdCB0cmFpbGluZyBgOmBcclxuICAgIFNfUk9PTUFMSUFTX0NPTE9OLm9uKFRULk5VTSwgU19ST09NQUxJQVNfQ09MT05fTlVNKTsgLy8gYnV0IGRvIGFjY2VwdCA6TlVNIChwb3J0IHNwZWNpZmllcilcclxuXHJcblxyXG4gICAgY29uc3QgVVNFUklEID0gZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICBNdWx0aVRva2VuLmNhbGwodGhpcywgdmFsdWUpO1xyXG4gICAgICAgIHRoaXMudHlwZSA9ICd1c2VyaWQnO1xyXG4gICAgICAgIHRoaXMuaXNMaW5rID0gdHJ1ZTtcclxuICAgIH07XHJcbiAgICBVU0VSSUQucHJvdG90eXBlID0gbmV3IE11bHRpVG9rZW4oKTtcclxuXHJcbiAgICBjb25zdCBTX0FUID0gU19TVEFSVC5qdW1wKFRULkFUKTtcclxuICAgIGNvbnN0IFNfQVRfTkFNRSA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19BVF9OQU1FX0NPTE9OID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX0FUX05BTUVfQ09MT05fRE9NQUlOID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX0FUX05BTUVfQ09MT05fRE9NQUlOX0RPVCA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19VU0VSSUQgPSBuZXcgbGlua2lmeS5wYXJzZXIuU3RhdGUoVVNFUklEKTtcclxuICAgIGNvbnN0IFNfVVNFUklEX0NPTE9OID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX1VTRVJJRF9DT0xPTl9OVU0gPSBuZXcgbGlua2lmeS5wYXJzZXIuU3RhdGUoVVNFUklEKTtcclxuXHJcbiAgICBjb25zdCB1c2VybmFtZVRva2VucyA9IFtcclxuICAgICAgICBUVC5ET1QsXHJcbiAgICAgICAgVFQuVU5ERVJTQ09SRSxcclxuICAgICAgICBUVC5QTFVTLFxyXG4gICAgICAgIFRULk5VTSxcclxuICAgICAgICBUVC5ET01BSU4sXHJcbiAgICAgICAgVFQuVExELFxyXG5cclxuICAgICAgICAvLyBhcyBpbiByb29tbmFtZVRva2Vuc1xyXG4gICAgICAgIFRULkxPQ0FMSE9TVCxcclxuICAgIF07XHJcblxyXG4gICAgU19BVC5vbih1c2VybmFtZVRva2VucywgU19BVF9OQU1FKTtcclxuICAgIFNfQVRfTkFNRS5vbih1c2VybmFtZVRva2VucywgU19BVF9OQU1FKTtcclxuICAgIFNfQVRfTkFNRS5vbihUVC5ET01BSU4sIFNfQVRfTkFNRSk7XHJcblxyXG4gICAgU19BVF9OQU1FLm9uKFRULkNPTE9OLCBTX0FUX05BTUVfQ09MT04pO1xyXG5cclxuICAgIFNfQVRfTkFNRV9DT0xPTi5vbihUVC5ET01BSU4sIFNfQVRfTkFNRV9DT0xPTl9ET01BSU4pO1xyXG4gICAgU19BVF9OQU1FX0NPTE9OLm9uKFRULkxPQ0FMSE9TVCwgU19VU0VSSUQpOyAvLyBhY2NlcHQgQGZvbzpsb2NhbGhvc3RcclxuICAgIFNfQVRfTkFNRV9DT0xPTi5vbihUVC5UTEQsIFNfVVNFUklEKTsgLy8gYWNjZXB0IEBmb286Y29tIChtb3N0bHkgZm9yIChUTER8RE9NQUlOKSsgbWl4aW5nKVxyXG4gICAgU19BVF9OQU1FX0NPTE9OX0RPTUFJTi5vbihUVC5ET1QsIFNfQVRfTkFNRV9DT0xPTl9ET01BSU5fRE9UKTtcclxuICAgIFNfQVRfTkFNRV9DT0xPTl9ET01BSU5fRE9ULm9uKFRULkRPTUFJTiwgU19BVF9OQU1FX0NPTE9OX0RPTUFJTik7XHJcbiAgICBTX0FUX05BTUVfQ09MT05fRE9NQUlOX0RPVC5vbihUVC5UTEQsIFNfVVNFUklEKTtcclxuXHJcbiAgICBTX1VTRVJJRC5vbihUVC5ET1QsIFNfQVRfTkFNRV9DT0xPTl9ET01BSU5fRE9UKTsgLy8gYWNjZXB0IHJlcGVhdGVkIFRMRHMgKGUuZyAub3JnLnVrKVxyXG4gICAgU19VU0VSSUQub24oVFQuQ09MT04sIFNfVVNFUklEX0NPTE9OKTsgLy8gZG8gbm90IGFjY2VwdCB0cmFpbGluZyBgOmBcclxuICAgIFNfVVNFUklEX0NPTE9OLm9uKFRULk5VTSwgU19VU0VSSURfQ09MT05fTlVNKTsgLy8gYnV0IGRvIGFjY2VwdCA6TlVNIChwb3J0IHNwZWNpZmllcilcclxuXHJcblxyXG4gICAgY29uc3QgR1JPVVBJRCA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcbiAgICAgICAgTXVsdGlUb2tlbi5jYWxsKHRoaXMsIHZhbHVlKTtcclxuICAgICAgICB0aGlzLnR5cGUgPSAnZ3JvdXBpZCc7XHJcbiAgICAgICAgdGhpcy5pc0xpbmsgPSB0cnVlO1xyXG4gICAgfTtcclxuICAgIEdST1VQSUQucHJvdG90eXBlID0gbmV3IE11bHRpVG9rZW4oKTtcclxuXHJcbiAgICBjb25zdCBTX1BMVVMgPSBTX1NUQVJULmp1bXAoVFQuUExVUyk7XHJcbiAgICBjb25zdCBTX1BMVVNfTkFNRSA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19QTFVTX05BTUVfQ09MT04gPSBuZXcgbGlua2lmeS5wYXJzZXIuU3RhdGUoKTtcclxuICAgIGNvbnN0IFNfUExVU19OQU1FX0NPTE9OX0RPTUFJTiA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19QTFVTX05BTUVfQ09MT05fRE9NQUlOX0RPVCA9IG5ldyBsaW5raWZ5LnBhcnNlci5TdGF0ZSgpO1xyXG4gICAgY29uc3QgU19HUk9VUElEID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKEdST1VQSUQpO1xyXG4gICAgY29uc3QgU19HUk9VUElEX0NPTE9OID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKCk7XHJcbiAgICBjb25zdCBTX0dST1VQSURfQ09MT05fTlVNID0gbmV3IGxpbmtpZnkucGFyc2VyLlN0YXRlKEdST1VQSUQpO1xyXG5cclxuICAgIGNvbnN0IGdyb3VwSWRUb2tlbnMgPSBbXHJcbiAgICAgICAgVFQuRE9ULFxyXG4gICAgICAgIFRULlVOREVSU0NPUkUsXHJcbiAgICAgICAgVFQuUExVUyxcclxuICAgICAgICBUVC5OVU0sXHJcbiAgICAgICAgVFQuRE9NQUlOLFxyXG4gICAgICAgIFRULlRMRCxcclxuXHJcbiAgICAgICAgLy8gYXMgaW4gcm9vbW5hbWVUb2tlbnNcclxuICAgICAgICBUVC5MT0NBTEhPU1QsXHJcbiAgICBdO1xyXG5cclxuICAgIFNfUExVUy5vbihncm91cElkVG9rZW5zLCBTX1BMVVNfTkFNRSk7XHJcbiAgICBTX1BMVVNfTkFNRS5vbihncm91cElkVG9rZW5zLCBTX1BMVVNfTkFNRSk7XHJcbiAgICBTX1BMVVNfTkFNRS5vbihUVC5ET01BSU4sIFNfUExVU19OQU1FKTtcclxuXHJcbiAgICBTX1BMVVNfTkFNRS5vbihUVC5DT0xPTiwgU19QTFVTX05BTUVfQ09MT04pO1xyXG5cclxuICAgIFNfUExVU19OQU1FX0NPTE9OLm9uKFRULkRPTUFJTiwgU19QTFVTX05BTUVfQ09MT05fRE9NQUlOKTtcclxuICAgIFNfUExVU19OQU1FX0NPTE9OLm9uKFRULkxPQ0FMSE9TVCwgU19HUk9VUElEKTsgLy8gYWNjZXB0ICtmb286bG9jYWxob3N0XHJcbiAgICBTX1BMVVNfTkFNRV9DT0xPTi5vbihUVC5UTEQsIFNfR1JPVVBJRCk7IC8vIGFjY2VwdCArZm9vOmNvbSAobW9zdGx5IGZvciAoVExEfERPTUFJTikrIG1peGluZylcclxuICAgIFNfUExVU19OQU1FX0NPTE9OX0RPTUFJTi5vbihUVC5ET1QsIFNfUExVU19OQU1FX0NPTE9OX0RPTUFJTl9ET1QpO1xyXG4gICAgU19QTFVTX05BTUVfQ09MT05fRE9NQUlOX0RPVC5vbihUVC5ET01BSU4sIFNfUExVU19OQU1FX0NPTE9OX0RPTUFJTik7XHJcbiAgICBTX1BMVVNfTkFNRV9DT0xPTl9ET01BSU5fRE9ULm9uKFRULlRMRCwgU19HUk9VUElEKTtcclxuXHJcbiAgICBTX0dST1VQSUQub24oVFQuRE9ULCBTX1BMVVNfTkFNRV9DT0xPTl9ET01BSU5fRE9UKTsgLy8gYWNjZXB0IHJlcGVhdGVkIFRMRHMgKGUuZyAub3JnLnVrKVxyXG4gICAgU19HUk9VUElELm9uKFRULkNPTE9OLCBTX0dST1VQSURfQ09MT04pOyAvLyBkbyBub3QgYWNjZXB0IHRyYWlsaW5nIGA6YFxyXG4gICAgU19HUk9VUElEX0NPTE9OLm9uKFRULk5VTSwgU19HUk9VUElEX0NPTE9OX05VTSk7IC8vIGJ1dCBkbyBhY2NlcHQgOk5VTSAocG9ydCBzcGVjaWZpZXIpXHJcbn1cclxuXHJcbi8vIHN0dWJzLCBvdmVyd3JpdHRlbiBpbiBNYXRyaXhDaGF0J3MgY29tcG9uZW50RGlkTW91bnRcclxubWF0cml4TGlua2lmeS5vblVzZXJDbGljayA9IGZ1bmN0aW9uKGUsIHVzZXJJZCkgeyBlLnByZXZlbnREZWZhdWx0KCk7IH07XHJcbm1hdHJpeExpbmtpZnkub25BbGlhc0NsaWNrID0gZnVuY3Rpb24oZSwgcm9vbUFsaWFzKSB7IGUucHJldmVudERlZmF1bHQoKTsgfTtcclxubWF0cml4TGlua2lmeS5vbkdyb3VwQ2xpY2sgPSBmdW5jdGlvbihlLCBncm91cElkKSB7IGUucHJldmVudERlZmF1bHQoKTsgfTtcclxuXHJcbmNvbnN0IGVzY2FwZVJlZ0V4cCA9IGZ1bmN0aW9uKHN0cmluZykge1xyXG4gICAgcmV0dXJuIHN0cmluZy5yZXBsYWNlKC9bLiorP14ke30oKXxbXFxdXFxcXF0vZywgXCJcXFxcJCZcIik7XHJcbn07XHJcblxyXG4vLyBSZWNvZ25pc2UgVVJMcyBmcm9tIGJvdGggb3VyIGxvY2FsIHZlY3RvciBhbmQgb2ZmaWNpYWwgdmVjdG9yIGFzIHZlY3Rvci5cclxuLy8gYW55b25lIGVsc2UgcmVhbGx5IHNob3VsZCBiZSB1c2luZyBtYXRyaXgudG8uXHJcbm1hdHJpeExpbmtpZnkuVkVDVE9SX1VSTF9QQVRURVJOID0gXCJeKD86aHR0cHM/Oi8vKT8oPzpcIlxyXG4gICAgKyBlc2NhcGVSZWdFeHAod2luZG93LmxvY2F0aW9uLmhvc3QgKyB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUpICsgXCJ8XCJcclxuICAgICsgXCIoPzp3d3dcXFxcLik/KD86cmlvdHx2ZWN0b3IpXFxcXC5pbS8oPzphcHB8YmV0YXxzdGFnaW5nfGRldmVsb3ApL1wiXHJcbiAgICArIFwiKSgjLiopXCI7XHJcblxyXG5tYXRyaXhMaW5raWZ5Lk1BVFJJWFRPX1VSTF9QQVRURVJOID0gXCJeKD86aHR0cHM/Oi8vKT8oPzp3d3dcXFxcLik/bWF0cml4XFxcXC50by8jLygoWyNAIStdKS4qKVwiO1xyXG5tYXRyaXhMaW5raWZ5Lk1BVFJJWFRPX01EX0xJTktfUEFUVEVSTiA9XHJcbiAgICAnXFxcXFsoW15cXFxcXV0qKVxcXFxdXFxcXCgoPzpodHRwcz86Ly8pPyg/Ond3d1xcXFwuKT9tYXRyaXhcXFxcLnRvLyMvKFsjQCErXVteXFxcXCldKilcXFxcKSc7XHJcbm1hdHJpeExpbmtpZnkuTUFUUklYVE9fQkFTRV9VUkw9IGJhc2VVcmw7XHJcblxyXG5tYXRyaXhMaW5raWZ5Lm9wdGlvbnMgPSB7XHJcbiAgICBldmVudHM6IGZ1bmN0aW9uKGhyZWYsIHR5cGUpIHtcclxuICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBcInVybFwiOiB7XHJcbiAgICAgICAgICAgICAgICAvLyBpbnRlcmNlcHQgbG9jYWwgcGVybWFsaW5rcyB0byB1c2VycyBhbmQgc2hvdyB0aGVtIGxpa2UgdXNlcmlkcyAoaW4gdXNlcmluZm8gb2YgY3VycmVudCByb29tKVxyXG4gICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwZXJtYWxpbmsgPSBwYXJzZVBlcm1hbGluayhocmVmKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocGVybWFsaW5rICYmIHBlcm1hbGluay51c2VySWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0cml4TGlua2lmeS5vblVzZXJDbGljayhlLCBwZXJtYWxpbmsudXNlcklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIE9LIGZpbmUsIGl0J3Mgbm90IGFjdHVhbGx5IGEgcGVybWFsaW5rXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIFwidXNlcmlkXCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdHJpeExpbmtpZnkub25Vc2VyQ2xpY2soZSwgaHJlZik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNhc2UgXCJyb29tYWxpYXNcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0cml4TGlua2lmeS5vbkFsaWFzQ2xpY2soZSwgaHJlZik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNhc2UgXCJncm91cGlkXCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdHJpeExpbmtpZnkub25Hcm91cENsaWNrKGUsIGhyZWYpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgZm9ybWF0SHJlZjogZnVuY3Rpb24oaHJlZiwgdHlwZSkge1xyXG4gICAgICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlICdyb29tYWxpYXMnOlxyXG4gICAgICAgICAgICBjYXNlICd1c2VyaWQnOlxyXG4gICAgICAgICAgICBjYXNlICdncm91cGlkJzpcclxuICAgICAgICAgICAgZGVmYXVsdDoge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRyeVRyYW5zZm9ybUVudGl0eVRvUGVybWFsaW5rKGhyZWYpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBsaW5rQXR0cmlidXRlczoge1xyXG4gICAgICAgIHJlbDogJ25vcmVmZXJyZXIgbm9vcGVuZXInLFxyXG4gICAgfSxcclxuXHJcbiAgICB0YXJnZXQ6IGZ1bmN0aW9uKGhyZWYsIHR5cGUpIHtcclxuICAgICAgICBpZiAodHlwZSA9PT0gJ3VybCcpIHtcclxuICAgICAgICAgICAgY29uc3QgdHJhbnNmb3JtZWQgPSB0cnlUcmFuc2Zvcm1QZXJtYWxpbmtUb0xvY2FsSHJlZihocmVmKTtcclxuICAgICAgICAgICAgaWYgKHRyYW5zZm9ybWVkICE9PSBocmVmIHx8IGhyZWYubWF0Y2gobWF0cml4TGlua2lmeS5WRUNUT1JfVVJMX1BBVFRFUk4pKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnX2JsYW5rJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0sXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBtYXRyaXhMaW5raWZ5O1xyXG4iXX0=