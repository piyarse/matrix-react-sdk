"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _MatrixClientPeg = require("./MatrixClientPeg");

var sdk = _interopRequireWildcard(require("./index"));

var _Modal = _interopRequireDefault(require("./Modal"));

var _languageHandler = require("./languageHandler");

var _IdentityAuthClient = _interopRequireDefault(require("./IdentityAuthClient"));

var _InteractiveAuthEntryComponents = require("./components/views/auth/InteractiveAuthEntryComponents");

/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function getIdServerDomain() {
  return _MatrixClientPeg.MatrixClientPeg.get().idBaseUrl.split("://")[1];
}
/**
 * Allows a user to add a third party identifier to their homeserver and,
 * optionally, the identity servers.
 *
 * This involves getting an email token from the identity server to "prove" that
 * the client owns the given email address, which is then passed to the
 * add threepid API on the homeserver.
 *
 * Diagrams of the intended API flows here are available at:
 *
 * https://gist.github.com/jryans/839a09bf0c5a70e2f36ed990d50ed928
 */


class AddThreepid {
  constructor() {
    (0, _defineProperty2.default)(this, "_makeAddThreepidOnlyRequest", auth => {
      return _MatrixClientPeg.MatrixClientPeg.get().addThreePidOnly({
        sid: this.sessionId,
        client_secret: this.clientSecret,
        auth
      });
    });
    this.clientSecret = _MatrixClientPeg.MatrixClientPeg.get().generateClientSecret();
    this.sessionId = null;
    this.submitUrl = null;
  }
  /**
   * Attempt to add an email threepid to the homeserver.
   * This will trigger a side-effect of sending an email to the provided email address.
   * @param {string} emailAddress The email address to add
   * @return {Promise} Resolves when the email has been sent. Then call checkEmailLinkClicked().
   */


  addEmailAddress(emailAddress) {
    return _MatrixClientPeg.MatrixClientPeg.get().requestAdd3pidEmailToken(emailAddress, this.clientSecret, 1).then(res => {
      this.sessionId = res.sid;
      return res;
    }, function (err) {
      if (err.errcode === 'M_THREEPID_IN_USE') {
        err.message = (0, _languageHandler._t)('This email address is already in use');
      } else if (err.httpStatus) {
        err.message = err.message + " (Status ".concat(err.httpStatus, ")");
      }

      throw err;
    });
  }
  /**
   * Attempt to bind an email threepid on the identity server via the homeserver.
   * This will trigger a side-effect of sending an email to the provided email address.
   * @param {string} emailAddress The email address to add
   * @return {Promise} Resolves when the email has been sent. Then call checkEmailLinkClicked().
   */


  async bindEmailAddress(emailAddress) {
    this.bind = true;

    if (await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind()) {
      // For separate bind, request a token directly from the IS.
      const authClient = new _IdentityAuthClient.default();
      const identityAccessToken = await authClient.getAccessToken();
      return _MatrixClientPeg.MatrixClientPeg.get().requestEmailToken(emailAddress, this.clientSecret, 1, undefined, undefined, identityAccessToken).then(res => {
        this.sessionId = res.sid;
        return res;
      }, function (err) {
        if (err.errcode === 'M_THREEPID_IN_USE') {
          err.message = (0, _languageHandler._t)('This email address is already in use');
        } else if (err.httpStatus) {
          err.message = err.message + " (Status ".concat(err.httpStatus, ")");
        }

        throw err;
      });
    } else {
      // For tangled bind, request a token via the HS.
      return this.addEmailAddress(emailAddress);
    }
  }
  /**
   * Attempt to add a MSISDN threepid to the homeserver.
   * This will trigger a side-effect of sending an SMS to the provided phone number.
   * @param {string} phoneCountry The ISO 2 letter code of the country to resolve phoneNumber in
   * @param {string} phoneNumber The national or international formatted phone number to add
   * @return {Promise} Resolves when the text message has been sent. Then call haveMsisdnToken().
   */


  addMsisdn(phoneCountry, phoneNumber) {
    return _MatrixClientPeg.MatrixClientPeg.get().requestAdd3pidMsisdnToken(phoneCountry, phoneNumber, this.clientSecret, 1).then(res => {
      this.sessionId = res.sid;
      this.submitUrl = res.submit_url;
      return res;
    }, function (err) {
      if (err.errcode === 'M_THREEPID_IN_USE') {
        err.message = (0, _languageHandler._t)('This phone number is already in use');
      } else if (err.httpStatus) {
        err.message = err.message + " (Status ".concat(err.httpStatus, ")");
      }

      throw err;
    });
  }
  /**
   * Attempt to bind a MSISDN threepid on the identity server via the homeserver.
   * This will trigger a side-effect of sending an SMS to the provided phone number.
   * @param {string} phoneCountry The ISO 2 letter code of the country to resolve phoneNumber in
   * @param {string} phoneNumber The national or international formatted phone number to add
   * @return {Promise} Resolves when the text message has been sent. Then call haveMsisdnToken().
   */


  async bindMsisdn(phoneCountry, phoneNumber) {
    this.bind = true;

    if (await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind()) {
      // For separate bind, request a token directly from the IS.
      const authClient = new _IdentityAuthClient.default();
      const identityAccessToken = await authClient.getAccessToken();
      return _MatrixClientPeg.MatrixClientPeg.get().requestMsisdnToken(phoneCountry, phoneNumber, this.clientSecret, 1, undefined, undefined, identityAccessToken).then(res => {
        this.sessionId = res.sid;
        return res;
      }, function (err) {
        if (err.errcode === 'M_THREEPID_IN_USE') {
          err.message = (0, _languageHandler._t)('This phone number is already in use');
        } else if (err.httpStatus) {
          err.message = err.message + " (Status ".concat(err.httpStatus, ")");
        }

        throw err;
      });
    } else {
      // For tangled bind, request a token via the HS.
      return this.addMsisdn(phoneCountry, phoneNumber);
    }
  }
  /**
   * Checks if the email link has been clicked by attempting to add the threepid
   * @return {Promise} Resolves if the email address was added. Rejects with an object
   * with a "message" property which contains a human-readable message detailing why
   * the request failed.
   */


  async checkEmailLinkClicked() {
    try {
      if (await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind()) {
        if (this.bind) {
          const authClient = new _IdentityAuthClient.default();
          const identityAccessToken = await authClient.getAccessToken();
          await _MatrixClientPeg.MatrixClientPeg.get().bindThreePid({
            sid: this.sessionId,
            client_secret: this.clientSecret,
            id_server: getIdServerDomain(),
            id_access_token: identityAccessToken
          });
        } else {
          try {
            await this._makeAddThreepidOnlyRequest(); // The spec has always required this to use UI auth but synapse briefly
            // implemented it without, so this may just succeed and that's OK.

            return;
          } catch (e) {
            if (e.httpStatus !== 401 || !e.data || !e.data.flows) {
              // doesn't look like an interactive-auth failure
              throw e;
            } // pop up an interactive auth dialog


            const InteractiveAuthDialog = sdk.getComponent("dialogs.InteractiveAuthDialog");
            const dialogAesthetics = {
              [_InteractiveAuthEntryComponents.SSOAuthEntry.PHASE_PREAUTH]: {
                title: (0, _languageHandler._t)("Use Single Sign On to continue"),
                body: (0, _languageHandler._t)("Confirm adding this email address by using " + "Single Sign On to prove your identity."),
                continueText: (0, _languageHandler._t)("Single Sign On"),
                continueKind: "primary"
              },
              [_InteractiveAuthEntryComponents.SSOAuthEntry.PHASE_POSTAUTH]: {
                title: (0, _languageHandler._t)("Confirm adding email"),
                body: (0, _languageHandler._t)("Click the button below to confirm adding this email address."),
                continueText: (0, _languageHandler._t)("Confirm"),
                continueKind: "primary"
              }
            };

            const {
              finished
            } = _Modal.default.createTrackedDialog('Add Email', '', InteractiveAuthDialog, {
              title: (0, _languageHandler._t)("Add Email Address"),
              matrixClient: _MatrixClientPeg.MatrixClientPeg.get(),
              authData: e.data,
              makeRequest: this._makeAddThreepidOnlyRequest,
              aestheticsForStagePhases: {
                [_InteractiveAuthEntryComponents.SSOAuthEntry.LOGIN_TYPE]: dialogAesthetics,
                [_InteractiveAuthEntryComponents.SSOAuthEntry.UNSTABLE_LOGIN_TYPE]: dialogAesthetics
              }
            });

            return finished;
          }
        }
      } else {
        await _MatrixClientPeg.MatrixClientPeg.get().addThreePid({
          sid: this.sessionId,
          client_secret: this.clientSecret,
          id_server: getIdServerDomain()
        }, this.bind);
      }
    } catch (err) {
      if (err.httpStatus === 401) {
        err.message = (0, _languageHandler._t)('Failed to verify email address: make sure you clicked the link in the email');
      } else if (err.httpStatus) {
        err.message += " (Status ".concat(err.httpStatus, ")");
      }

      throw err;
    }
  }
  /**
   * @param {Object} auth UI auth object
   * @return {Promise<Object>} Response from /3pid/add call (in current spec, an empty object)
   */


  /**
   * Takes a phone number verification code as entered by the user and validates
   * it with the ID server, then if successful, adds the phone number.
   * @param {string} msisdnToken phone number verification code as entered by the user
   * @return {Promise} Resolves if the phone number was added. Rejects with an object
   * with a "message" property which contains a human-readable message detailing why
   * the request failed.
   */
  async haveMsisdnToken(msisdnToken) {
    const authClient = new _IdentityAuthClient.default();
    const supportsSeparateAddAndBind = await _MatrixClientPeg.MatrixClientPeg.get().doesServerSupportSeparateAddAndBind();
    let result;

    if (this.submitUrl) {
      result = await _MatrixClientPeg.MatrixClientPeg.get().submitMsisdnTokenOtherUrl(this.submitUrl, this.sessionId, this.clientSecret, msisdnToken);
    } else if (this.bind || !supportsSeparateAddAndBind) {
      result = await _MatrixClientPeg.MatrixClientPeg.get().submitMsisdnToken(this.sessionId, this.clientSecret, msisdnToken, (await authClient.getAccessToken()));
    } else {
      throw new Error("The add / bind with MSISDN flow is misconfigured");
    }

    if (result.errcode) {
      throw result;
    }

    if (supportsSeparateAddAndBind) {
      if (this.bind) {
        await _MatrixClientPeg.MatrixClientPeg.get().bindThreePid({
          sid: this.sessionId,
          client_secret: this.clientSecret,
          id_server: getIdServerDomain(),
          id_access_token: await authClient.getAccessToken()
        });
      } else {
        try {
          await this._makeAddThreepidOnlyRequest(); // The spec has always required this to use UI auth but synapse briefly
          // implemented it without, so this may just succeed and that's OK.

          return;
        } catch (e) {
          if (e.httpStatus !== 401 || !e.data || !e.data.flows) {
            // doesn't look like an interactive-auth failure
            throw e;
          } // pop up an interactive auth dialog


          const InteractiveAuthDialog = sdk.getComponent("dialogs.InteractiveAuthDialog");
          const dialogAesthetics = {
            [_InteractiveAuthEntryComponents.SSOAuthEntry.PHASE_PREAUTH]: {
              title: (0, _languageHandler._t)("Use Single Sign On to continue"),
              body: (0, _languageHandler._t)("Confirm adding this phone number by using " + "Single Sign On to prove your identity."),
              continueText: (0, _languageHandler._t)("Single Sign On"),
              continueKind: "primary"
            },
            [_InteractiveAuthEntryComponents.SSOAuthEntry.PHASE_POSTAUTH]: {
              title: (0, _languageHandler._t)("Confirm adding phone number"),
              body: (0, _languageHandler._t)("Click the button below to confirm adding this phone number."),
              continueText: (0, _languageHandler._t)("Confirm"),
              continueKind: "primary"
            }
          };

          const {
            finished
          } = _Modal.default.createTrackedDialog('Add MSISDN', '', InteractiveAuthDialog, {
            title: (0, _languageHandler._t)("Add Phone Number"),
            matrixClient: _MatrixClientPeg.MatrixClientPeg.get(),
            authData: e.data,
            makeRequest: this._makeAddThreepidOnlyRequest,
            aestheticsForStagePhases: {
              [_InteractiveAuthEntryComponents.SSOAuthEntry.LOGIN_TYPE]: dialogAesthetics,
              [_InteractiveAuthEntryComponents.SSOAuthEntry.UNSTABLE_LOGIN_TYPE]: dialogAesthetics
            }
          });

          return finished;
        }
      }
    } else {
      await _MatrixClientPeg.MatrixClientPeg.get().addThreePid({
        sid: this.sessionId,
        client_secret: this.clientSecret,
        id_server: getIdServerDomain()
      }, this.bind);
    }
  }

}

exports.default = AddThreepid;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9BZGRUaHJlZXBpZC5qcyJdLCJuYW1lcyI6WyJnZXRJZFNlcnZlckRvbWFpbiIsIk1hdHJpeENsaWVudFBlZyIsImdldCIsImlkQmFzZVVybCIsInNwbGl0IiwiQWRkVGhyZWVwaWQiLCJjb25zdHJ1Y3RvciIsImF1dGgiLCJhZGRUaHJlZVBpZE9ubHkiLCJzaWQiLCJzZXNzaW9uSWQiLCJjbGllbnRfc2VjcmV0IiwiY2xpZW50U2VjcmV0IiwiZ2VuZXJhdGVDbGllbnRTZWNyZXQiLCJzdWJtaXRVcmwiLCJhZGRFbWFpbEFkZHJlc3MiLCJlbWFpbEFkZHJlc3MiLCJyZXF1ZXN0QWRkM3BpZEVtYWlsVG9rZW4iLCJ0aGVuIiwicmVzIiwiZXJyIiwiZXJyY29kZSIsIm1lc3NhZ2UiLCJodHRwU3RhdHVzIiwiYmluZEVtYWlsQWRkcmVzcyIsImJpbmQiLCJkb2VzU2VydmVyU3VwcG9ydFNlcGFyYXRlQWRkQW5kQmluZCIsImF1dGhDbGllbnQiLCJJZGVudGl0eUF1dGhDbGllbnQiLCJpZGVudGl0eUFjY2Vzc1Rva2VuIiwiZ2V0QWNjZXNzVG9rZW4iLCJyZXF1ZXN0RW1haWxUb2tlbiIsInVuZGVmaW5lZCIsImFkZE1zaXNkbiIsInBob25lQ291bnRyeSIsInBob25lTnVtYmVyIiwicmVxdWVzdEFkZDNwaWRNc2lzZG5Ub2tlbiIsInN1Ym1pdF91cmwiLCJiaW5kTXNpc2RuIiwicmVxdWVzdE1zaXNkblRva2VuIiwiY2hlY2tFbWFpbExpbmtDbGlja2VkIiwiYmluZFRocmVlUGlkIiwiaWRfc2VydmVyIiwiaWRfYWNjZXNzX3Rva2VuIiwiX21ha2VBZGRUaHJlZXBpZE9ubHlSZXF1ZXN0IiwiZSIsImRhdGEiLCJmbG93cyIsIkludGVyYWN0aXZlQXV0aERpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsImRpYWxvZ0Flc3RoZXRpY3MiLCJTU09BdXRoRW50cnkiLCJQSEFTRV9QUkVBVVRIIiwidGl0bGUiLCJib2R5IiwiY29udGludWVUZXh0IiwiY29udGludWVLaW5kIiwiUEhBU0VfUE9TVEFVVEgiLCJmaW5pc2hlZCIsIk1vZGFsIiwiY3JlYXRlVHJhY2tlZERpYWxvZyIsIm1hdHJpeENsaWVudCIsImF1dGhEYXRhIiwibWFrZVJlcXVlc3QiLCJhZXN0aGV0aWNzRm9yU3RhZ2VQaGFzZXMiLCJMT0dJTl9UWVBFIiwiVU5TVEFCTEVfTE9HSU5fVFlQRSIsImFkZFRocmVlUGlkIiwiaGF2ZU1zaXNkblRva2VuIiwibXNpc2RuVG9rZW4iLCJzdXBwb3J0c1NlcGFyYXRlQWRkQW5kQmluZCIsInJlc3VsdCIsInN1Ym1pdE1zaXNkblRva2VuT3RoZXJVcmwiLCJzdWJtaXRNc2lzZG5Ub2tlbiIsIkVycm9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBa0JBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQXZCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsU0FBU0EsaUJBQVQsR0FBNkI7QUFDekIsU0FBT0MsaUNBQWdCQyxHQUFoQixHQUFzQkMsU0FBdEIsQ0FBZ0NDLEtBQWhDLENBQXNDLEtBQXRDLEVBQTZDLENBQTdDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7OztBQVllLE1BQU1DLFdBQU4sQ0FBa0I7QUFDN0JDLEVBQUFBLFdBQVcsR0FBRztBQUFBLHVFQXVNaUJDLElBQUQsSUFBVTtBQUNwQyxhQUFPTixpQ0FBZ0JDLEdBQWhCLEdBQXNCTSxlQUF0QixDQUFzQztBQUN6Q0MsUUFBQUEsR0FBRyxFQUFFLEtBQUtDLFNBRCtCO0FBRXpDQyxRQUFBQSxhQUFhLEVBQUUsS0FBS0MsWUFGcUI7QUFHekNMLFFBQUFBO0FBSHlDLE9BQXRDLENBQVA7QUFLSCxLQTdNYTtBQUNWLFNBQUtLLFlBQUwsR0FBb0JYLGlDQUFnQkMsR0FBaEIsR0FBc0JXLG9CQUF0QixFQUFwQjtBQUNBLFNBQUtILFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLSSxTQUFMLEdBQWlCLElBQWpCO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNQUMsRUFBQUEsZUFBZSxDQUFDQyxZQUFELEVBQWU7QUFDMUIsV0FBT2YsaUNBQWdCQyxHQUFoQixHQUFzQmUsd0JBQXRCLENBQStDRCxZQUEvQyxFQUE2RCxLQUFLSixZQUFsRSxFQUFnRixDQUFoRixFQUFtRk0sSUFBbkYsQ0FBeUZDLEdBQUQsSUFBUztBQUNwRyxXQUFLVCxTQUFMLEdBQWlCUyxHQUFHLENBQUNWLEdBQXJCO0FBQ0EsYUFBT1UsR0FBUDtBQUNILEtBSE0sRUFHSixVQUFTQyxHQUFULEVBQWM7QUFDYixVQUFJQSxHQUFHLENBQUNDLE9BQUosS0FBZ0IsbUJBQXBCLEVBQXlDO0FBQ3JDRCxRQUFBQSxHQUFHLENBQUNFLE9BQUosR0FBYyx5QkFBRyxzQ0FBSCxDQUFkO0FBQ0gsT0FGRCxNQUVPLElBQUlGLEdBQUcsQ0FBQ0csVUFBUixFQUFvQjtBQUN2QkgsUUFBQUEsR0FBRyxDQUFDRSxPQUFKLEdBQWNGLEdBQUcsQ0FBQ0UsT0FBSixzQkFBMEJGLEdBQUcsQ0FBQ0csVUFBOUIsTUFBZDtBQUNIOztBQUNELFlBQU1ILEdBQU47QUFDSCxLQVZNLENBQVA7QUFXSDtBQUVEOzs7Ozs7OztBQU1BLFFBQU1JLGdCQUFOLENBQXVCUixZQUF2QixFQUFxQztBQUNqQyxTQUFLUyxJQUFMLEdBQVksSUFBWjs7QUFDQSxRQUFJLE1BQU14QixpQ0FBZ0JDLEdBQWhCLEdBQXNCd0IsbUNBQXRCLEVBQVYsRUFBdUU7QUFDbkU7QUFDQSxZQUFNQyxVQUFVLEdBQUcsSUFBSUMsMkJBQUosRUFBbkI7QUFDQSxZQUFNQyxtQkFBbUIsR0FBRyxNQUFNRixVQUFVLENBQUNHLGNBQVgsRUFBbEM7QUFDQSxhQUFPN0IsaUNBQWdCQyxHQUFoQixHQUFzQjZCLGlCQUF0QixDQUNIZixZQURHLEVBQ1csS0FBS0osWUFEaEIsRUFDOEIsQ0FEOUIsRUFFSG9CLFNBRkcsRUFFUUEsU0FGUixFQUVtQkgsbUJBRm5CLEVBR0xYLElBSEssQ0FHQ0MsR0FBRCxJQUFTO0FBQ1osYUFBS1QsU0FBTCxHQUFpQlMsR0FBRyxDQUFDVixHQUFyQjtBQUNBLGVBQU9VLEdBQVA7QUFDSCxPQU5NLEVBTUosVUFBU0MsR0FBVCxFQUFjO0FBQ2IsWUFBSUEsR0FBRyxDQUFDQyxPQUFKLEtBQWdCLG1CQUFwQixFQUF5QztBQUNyQ0QsVUFBQUEsR0FBRyxDQUFDRSxPQUFKLEdBQWMseUJBQUcsc0NBQUgsQ0FBZDtBQUNILFNBRkQsTUFFTyxJQUFJRixHQUFHLENBQUNHLFVBQVIsRUFBb0I7QUFDdkJILFVBQUFBLEdBQUcsQ0FBQ0UsT0FBSixHQUFjRixHQUFHLENBQUNFLE9BQUosc0JBQTBCRixHQUFHLENBQUNHLFVBQTlCLE1BQWQ7QUFDSDs7QUFDRCxjQUFNSCxHQUFOO0FBQ0gsT0FiTSxDQUFQO0FBY0gsS0FsQkQsTUFrQk87QUFDSDtBQUNBLGFBQU8sS0FBS0wsZUFBTCxDQUFxQkMsWUFBckIsQ0FBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7O0FBT0FpQixFQUFBQSxTQUFTLENBQUNDLFlBQUQsRUFBZUMsV0FBZixFQUE0QjtBQUNqQyxXQUFPbEMsaUNBQWdCQyxHQUFoQixHQUFzQmtDLHlCQUF0QixDQUNIRixZQURHLEVBQ1dDLFdBRFgsRUFDd0IsS0FBS3ZCLFlBRDdCLEVBQzJDLENBRDNDLEVBRUxNLElBRkssQ0FFQ0MsR0FBRCxJQUFTO0FBQ1osV0FBS1QsU0FBTCxHQUFpQlMsR0FBRyxDQUFDVixHQUFyQjtBQUNBLFdBQUtLLFNBQUwsR0FBaUJLLEdBQUcsQ0FBQ2tCLFVBQXJCO0FBQ0EsYUFBT2xCLEdBQVA7QUFDSCxLQU5NLEVBTUosVUFBU0MsR0FBVCxFQUFjO0FBQ2IsVUFBSUEsR0FBRyxDQUFDQyxPQUFKLEtBQWdCLG1CQUFwQixFQUF5QztBQUNyQ0QsUUFBQUEsR0FBRyxDQUFDRSxPQUFKLEdBQWMseUJBQUcscUNBQUgsQ0FBZDtBQUNILE9BRkQsTUFFTyxJQUFJRixHQUFHLENBQUNHLFVBQVIsRUFBb0I7QUFDdkJILFFBQUFBLEdBQUcsQ0FBQ0UsT0FBSixHQUFjRixHQUFHLENBQUNFLE9BQUosc0JBQTBCRixHQUFHLENBQUNHLFVBQTlCLE1BQWQ7QUFDSDs7QUFDRCxZQUFNSCxHQUFOO0FBQ0gsS0FiTSxDQUFQO0FBY0g7QUFFRDs7Ozs7Ozs7O0FBT0EsUUFBTWtCLFVBQU4sQ0FBaUJKLFlBQWpCLEVBQStCQyxXQUEvQixFQUE0QztBQUN4QyxTQUFLVixJQUFMLEdBQVksSUFBWjs7QUFDQSxRQUFJLE1BQU14QixpQ0FBZ0JDLEdBQWhCLEdBQXNCd0IsbUNBQXRCLEVBQVYsRUFBdUU7QUFDbkU7QUFDQSxZQUFNQyxVQUFVLEdBQUcsSUFBSUMsMkJBQUosRUFBbkI7QUFDQSxZQUFNQyxtQkFBbUIsR0FBRyxNQUFNRixVQUFVLENBQUNHLGNBQVgsRUFBbEM7QUFDQSxhQUFPN0IsaUNBQWdCQyxHQUFoQixHQUFzQnFDLGtCQUF0QixDQUNITCxZQURHLEVBQ1dDLFdBRFgsRUFDd0IsS0FBS3ZCLFlBRDdCLEVBQzJDLENBRDNDLEVBRUhvQixTQUZHLEVBRVFBLFNBRlIsRUFFbUJILG1CQUZuQixFQUdMWCxJQUhLLENBR0NDLEdBQUQsSUFBUztBQUNaLGFBQUtULFNBQUwsR0FBaUJTLEdBQUcsQ0FBQ1YsR0FBckI7QUFDQSxlQUFPVSxHQUFQO0FBQ0gsT0FOTSxFQU1KLFVBQVNDLEdBQVQsRUFBYztBQUNiLFlBQUlBLEdBQUcsQ0FBQ0MsT0FBSixLQUFnQixtQkFBcEIsRUFBeUM7QUFDckNELFVBQUFBLEdBQUcsQ0FBQ0UsT0FBSixHQUFjLHlCQUFHLHFDQUFILENBQWQ7QUFDSCxTQUZELE1BRU8sSUFBSUYsR0FBRyxDQUFDRyxVQUFSLEVBQW9CO0FBQ3ZCSCxVQUFBQSxHQUFHLENBQUNFLE9BQUosR0FBY0YsR0FBRyxDQUFDRSxPQUFKLHNCQUEwQkYsR0FBRyxDQUFDRyxVQUE5QixNQUFkO0FBQ0g7O0FBQ0QsY0FBTUgsR0FBTjtBQUNILE9BYk0sQ0FBUDtBQWNILEtBbEJELE1Ba0JPO0FBQ0g7QUFDQSxhQUFPLEtBQUthLFNBQUwsQ0FBZUMsWUFBZixFQUE2QkMsV0FBN0IsQ0FBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7QUFNQSxRQUFNSyxxQkFBTixHQUE4QjtBQUMxQixRQUFJO0FBQ0EsVUFBSSxNQUFNdkMsaUNBQWdCQyxHQUFoQixHQUFzQndCLG1DQUF0QixFQUFWLEVBQXVFO0FBQ25FLFlBQUksS0FBS0QsSUFBVCxFQUFlO0FBQ1gsZ0JBQU1FLFVBQVUsR0FBRyxJQUFJQywyQkFBSixFQUFuQjtBQUNBLGdCQUFNQyxtQkFBbUIsR0FBRyxNQUFNRixVQUFVLENBQUNHLGNBQVgsRUFBbEM7QUFDQSxnQkFBTTdCLGlDQUFnQkMsR0FBaEIsR0FBc0J1QyxZQUF0QixDQUFtQztBQUNyQ2hDLFlBQUFBLEdBQUcsRUFBRSxLQUFLQyxTQUQyQjtBQUVyQ0MsWUFBQUEsYUFBYSxFQUFFLEtBQUtDLFlBRmlCO0FBR3JDOEIsWUFBQUEsU0FBUyxFQUFFMUMsaUJBQWlCLEVBSFM7QUFJckMyQyxZQUFBQSxlQUFlLEVBQUVkO0FBSm9CLFdBQW5DLENBQU47QUFNSCxTQVRELE1BU087QUFDSCxjQUFJO0FBQ0Esa0JBQU0sS0FBS2UsMkJBQUwsRUFBTixDQURBLENBR0E7QUFDQTs7QUFDQTtBQUNILFdBTkQsQ0FNRSxPQUFPQyxDQUFQLEVBQVU7QUFDUixnQkFBSUEsQ0FBQyxDQUFDdEIsVUFBRixLQUFpQixHQUFqQixJQUF3QixDQUFDc0IsQ0FBQyxDQUFDQyxJQUEzQixJQUFtQyxDQUFDRCxDQUFDLENBQUNDLElBQUYsQ0FBT0MsS0FBL0MsRUFBc0Q7QUFDbEQ7QUFDQSxvQkFBTUYsQ0FBTjtBQUNILGFBSk8sQ0FNUjs7O0FBQ0Esa0JBQU1HLHFCQUFxQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsK0JBQWpCLENBQTlCO0FBR0Esa0JBQU1DLGdCQUFnQixHQUFHO0FBQ3JCLGVBQUNDLDZDQUFhQyxhQUFkLEdBQThCO0FBQzFCQyxnQkFBQUEsS0FBSyxFQUFFLHlCQUFHLGdDQUFILENBRG1CO0FBRTFCQyxnQkFBQUEsSUFBSSxFQUFFLHlCQUFHLGdEQUNMLHdDQURFLENBRm9CO0FBSTFCQyxnQkFBQUEsWUFBWSxFQUFFLHlCQUFHLGdCQUFILENBSlk7QUFLMUJDLGdCQUFBQSxZQUFZLEVBQUU7QUFMWSxlQURUO0FBUXJCLGVBQUNMLDZDQUFhTSxjQUFkLEdBQStCO0FBQzNCSixnQkFBQUEsS0FBSyxFQUFFLHlCQUFHLHNCQUFILENBRG9CO0FBRTNCQyxnQkFBQUEsSUFBSSxFQUFFLHlCQUFHLDhEQUFILENBRnFCO0FBRzNCQyxnQkFBQUEsWUFBWSxFQUFFLHlCQUFHLFNBQUgsQ0FIYTtBQUkzQkMsZ0JBQUFBLFlBQVksRUFBRTtBQUphO0FBUlYsYUFBekI7O0FBZUEsa0JBQU07QUFBRUUsY0FBQUE7QUFBRixnQkFBZUMsZUFBTUMsbUJBQU4sQ0FBMEIsV0FBMUIsRUFBdUMsRUFBdkMsRUFBMkNiLHFCQUEzQyxFQUFrRTtBQUNuRk0sY0FBQUEsS0FBSyxFQUFFLHlCQUFHLG1CQUFILENBRDRFO0FBRW5GUSxjQUFBQSxZQUFZLEVBQUU3RCxpQ0FBZ0JDLEdBQWhCLEVBRnFFO0FBR25GNkQsY0FBQUEsUUFBUSxFQUFFbEIsQ0FBQyxDQUFDQyxJQUh1RTtBQUluRmtCLGNBQUFBLFdBQVcsRUFBRSxLQUFLcEIsMkJBSmlFO0FBS25GcUIsY0FBQUEsd0JBQXdCLEVBQUU7QUFDdEIsaUJBQUNiLDZDQUFhYyxVQUFkLEdBQTJCZixnQkFETDtBQUV0QixpQkFBQ0MsNkNBQWFlLG1CQUFkLEdBQW9DaEI7QUFGZDtBQUx5RCxhQUFsRSxDQUFyQjs7QUFVQSxtQkFBT1EsUUFBUDtBQUNIO0FBQ0o7QUFDSixPQXZERCxNQXVETztBQUNILGNBQU0xRCxpQ0FBZ0JDLEdBQWhCLEdBQXNCa0UsV0FBdEIsQ0FBa0M7QUFDcEMzRCxVQUFBQSxHQUFHLEVBQUUsS0FBS0MsU0FEMEI7QUFFcENDLFVBQUFBLGFBQWEsRUFBRSxLQUFLQyxZQUZnQjtBQUdwQzhCLFVBQUFBLFNBQVMsRUFBRTFDLGlCQUFpQjtBQUhRLFNBQWxDLEVBSUgsS0FBS3lCLElBSkYsQ0FBTjtBQUtIO0FBQ0osS0EvREQsQ0ErREUsT0FBT0wsR0FBUCxFQUFZO0FBQ1YsVUFBSUEsR0FBRyxDQUFDRyxVQUFKLEtBQW1CLEdBQXZCLEVBQTRCO0FBQ3hCSCxRQUFBQSxHQUFHLENBQUNFLE9BQUosR0FBYyx5QkFBRyw2RUFBSCxDQUFkO0FBQ0gsT0FGRCxNQUVPLElBQUlGLEdBQUcsQ0FBQ0csVUFBUixFQUFvQjtBQUN2QkgsUUFBQUEsR0FBRyxDQUFDRSxPQUFKLHVCQUEyQkYsR0FBRyxDQUFDRyxVQUEvQjtBQUNIOztBQUNELFlBQU1ILEdBQU47QUFDSDtBQUNKO0FBRUQ7Ozs7OztBQVlBOzs7Ozs7OztBQVFBLFFBQU1pRCxlQUFOLENBQXNCQyxXQUF0QixFQUFtQztBQUMvQixVQUFNM0MsVUFBVSxHQUFHLElBQUlDLDJCQUFKLEVBQW5CO0FBQ0EsVUFBTTJDLDBCQUEwQixHQUM1QixNQUFNdEUsaUNBQWdCQyxHQUFoQixHQUFzQndCLG1DQUF0QixFQURWO0FBR0EsUUFBSThDLE1BQUo7O0FBQ0EsUUFBSSxLQUFLMUQsU0FBVCxFQUFvQjtBQUNoQjBELE1BQUFBLE1BQU0sR0FBRyxNQUFNdkUsaUNBQWdCQyxHQUFoQixHQUFzQnVFLHlCQUF0QixDQUNYLEtBQUszRCxTQURNLEVBRVgsS0FBS0osU0FGTSxFQUdYLEtBQUtFLFlBSE0sRUFJWDBELFdBSlcsQ0FBZjtBQU1ILEtBUEQsTUFPTyxJQUFJLEtBQUs3QyxJQUFMLElBQWEsQ0FBQzhDLDBCQUFsQixFQUE4QztBQUNqREMsTUFBQUEsTUFBTSxHQUFHLE1BQU12RSxpQ0FBZ0JDLEdBQWhCLEdBQXNCd0UsaUJBQXRCLENBQ1gsS0FBS2hFLFNBRE0sRUFFWCxLQUFLRSxZQUZNLEVBR1gwRCxXQUhXLEdBSVgsTUFBTTNDLFVBQVUsQ0FBQ0csY0FBWCxFQUpLLEVBQWY7QUFNSCxLQVBNLE1BT0E7QUFDSCxZQUFNLElBQUk2QyxLQUFKLENBQVUsa0RBQVYsQ0FBTjtBQUNIOztBQUNELFFBQUlILE1BQU0sQ0FBQ25ELE9BQVgsRUFBb0I7QUFDaEIsWUFBTW1ELE1BQU47QUFDSDs7QUFFRCxRQUFJRCwwQkFBSixFQUFnQztBQUM1QixVQUFJLEtBQUs5QyxJQUFULEVBQWU7QUFDWCxjQUFNeEIsaUNBQWdCQyxHQUFoQixHQUFzQnVDLFlBQXRCLENBQW1DO0FBQ3JDaEMsVUFBQUEsR0FBRyxFQUFFLEtBQUtDLFNBRDJCO0FBRXJDQyxVQUFBQSxhQUFhLEVBQUUsS0FBS0MsWUFGaUI7QUFHckM4QixVQUFBQSxTQUFTLEVBQUUxQyxpQkFBaUIsRUFIUztBQUlyQzJDLFVBQUFBLGVBQWUsRUFBRSxNQUFNaEIsVUFBVSxDQUFDRyxjQUFYO0FBSmMsU0FBbkMsQ0FBTjtBQU1ILE9BUEQsTUFPTztBQUNILFlBQUk7QUFDQSxnQkFBTSxLQUFLYywyQkFBTCxFQUFOLENBREEsQ0FHQTtBQUNBOztBQUNBO0FBQ0gsU0FORCxDQU1FLE9BQU9DLENBQVAsRUFBVTtBQUNSLGNBQUlBLENBQUMsQ0FBQ3RCLFVBQUYsS0FBaUIsR0FBakIsSUFBd0IsQ0FBQ3NCLENBQUMsQ0FBQ0MsSUFBM0IsSUFBbUMsQ0FBQ0QsQ0FBQyxDQUFDQyxJQUFGLENBQU9DLEtBQS9DLEVBQXNEO0FBQ2xEO0FBQ0Esa0JBQU1GLENBQU47QUFDSCxXQUpPLENBTVI7OztBQUNBLGdCQUFNRyxxQkFBcUIsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLCtCQUFqQixDQUE5QjtBQUVBLGdCQUFNQyxnQkFBZ0IsR0FBRztBQUNyQixhQUFDQyw2Q0FBYUMsYUFBZCxHQUE4QjtBQUMxQkMsY0FBQUEsS0FBSyxFQUFFLHlCQUFHLGdDQUFILENBRG1CO0FBRTFCQyxjQUFBQSxJQUFJLEVBQUUseUJBQUcsK0NBQ0wsd0NBREUsQ0FGb0I7QUFJMUJDLGNBQUFBLFlBQVksRUFBRSx5QkFBRyxnQkFBSCxDQUpZO0FBSzFCQyxjQUFBQSxZQUFZLEVBQUU7QUFMWSxhQURUO0FBUXJCLGFBQUNMLDZDQUFhTSxjQUFkLEdBQStCO0FBQzNCSixjQUFBQSxLQUFLLEVBQUUseUJBQUcsNkJBQUgsQ0FEb0I7QUFFM0JDLGNBQUFBLElBQUksRUFBRSx5QkFBRyw2REFBSCxDQUZxQjtBQUczQkMsY0FBQUEsWUFBWSxFQUFFLHlCQUFHLFNBQUgsQ0FIYTtBQUkzQkMsY0FBQUEsWUFBWSxFQUFFO0FBSmE7QUFSVixXQUF6Qjs7QUFlQSxnQkFBTTtBQUFFRSxZQUFBQTtBQUFGLGNBQWVDLGVBQU1DLG1CQUFOLENBQTBCLFlBQTFCLEVBQXdDLEVBQXhDLEVBQTRDYixxQkFBNUMsRUFBbUU7QUFDcEZNLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxrQkFBSCxDQUQ2RTtBQUVwRlEsWUFBQUEsWUFBWSxFQUFFN0QsaUNBQWdCQyxHQUFoQixFQUZzRTtBQUdwRjZELFlBQUFBLFFBQVEsRUFBRWxCLENBQUMsQ0FBQ0MsSUFId0U7QUFJcEZrQixZQUFBQSxXQUFXLEVBQUUsS0FBS3BCLDJCQUprRTtBQUtwRnFCLFlBQUFBLHdCQUF3QixFQUFFO0FBQ3RCLGVBQUNiLDZDQUFhYyxVQUFkLEdBQTJCZixnQkFETDtBQUV0QixlQUFDQyw2Q0FBYWUsbUJBQWQsR0FBb0NoQjtBQUZkO0FBTDBELFdBQW5FLENBQXJCOztBQVVBLGlCQUFPUSxRQUFQO0FBQ0g7QUFDSjtBQUNKLEtBcERELE1Bb0RPO0FBQ0gsWUFBTTFELGlDQUFnQkMsR0FBaEIsR0FBc0JrRSxXQUF0QixDQUFrQztBQUNwQzNELFFBQUFBLEdBQUcsRUFBRSxLQUFLQyxTQUQwQjtBQUVwQ0MsUUFBQUEsYUFBYSxFQUFFLEtBQUtDLFlBRmdCO0FBR3BDOEIsUUFBQUEsU0FBUyxFQUFFMUMsaUJBQWlCO0FBSFEsT0FBbEMsRUFJSCxLQUFLeUIsSUFKRixDQUFOO0FBS0g7QUFDSjs7QUE5UzRCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTcgVmVjdG9yIENyZWF0aW9ucyBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi9pbmRleCc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuL01vZGFsJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuL2xhbmd1YWdlSGFuZGxlcic7XHJcbmltcG9ydCBJZGVudGl0eUF1dGhDbGllbnQgZnJvbSAnLi9JZGVudGl0eUF1dGhDbGllbnQnO1xyXG5pbXBvcnQge1NTT0F1dGhFbnRyeX0gZnJvbSBcIi4vY29tcG9uZW50cy92aWV3cy9hdXRoL0ludGVyYWN0aXZlQXV0aEVudHJ5Q29tcG9uZW50c1wiO1xyXG5cclxuZnVuY3Rpb24gZ2V0SWRTZXJ2ZXJEb21haW4oKSB7XHJcbiAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLmlkQmFzZVVybC5zcGxpdChcIjovL1wiKVsxXTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEFsbG93cyBhIHVzZXIgdG8gYWRkIGEgdGhpcmQgcGFydHkgaWRlbnRpZmllciB0byB0aGVpciBob21lc2VydmVyIGFuZCxcclxuICogb3B0aW9uYWxseSwgdGhlIGlkZW50aXR5IHNlcnZlcnMuXHJcbiAqXHJcbiAqIFRoaXMgaW52b2x2ZXMgZ2V0dGluZyBhbiBlbWFpbCB0b2tlbiBmcm9tIHRoZSBpZGVudGl0eSBzZXJ2ZXIgdG8gXCJwcm92ZVwiIHRoYXRcclxuICogdGhlIGNsaWVudCBvd25zIHRoZSBnaXZlbiBlbWFpbCBhZGRyZXNzLCB3aGljaCBpcyB0aGVuIHBhc3NlZCB0byB0aGVcclxuICogYWRkIHRocmVlcGlkIEFQSSBvbiB0aGUgaG9tZXNlcnZlci5cclxuICpcclxuICogRGlhZ3JhbXMgb2YgdGhlIGludGVuZGVkIEFQSSBmbG93cyBoZXJlIGFyZSBhdmFpbGFibGUgYXQ6XHJcbiAqXHJcbiAqIGh0dHBzOi8vZ2lzdC5naXRodWIuY29tL2pyeWFucy84MzlhMDliZjBjNWE3MGUyZjM2ZWQ5OTBkNTBlZDkyOFxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWRkVGhyZWVwaWQge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5jbGllbnRTZWNyZXQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2VuZXJhdGVDbGllbnRTZWNyZXQoKTtcclxuICAgICAgICB0aGlzLnNlc3Npb25JZCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5zdWJtaXRVcmwgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQXR0ZW1wdCB0byBhZGQgYW4gZW1haWwgdGhyZWVwaWQgdG8gdGhlIGhvbWVzZXJ2ZXIuXHJcbiAgICAgKiBUaGlzIHdpbGwgdHJpZ2dlciBhIHNpZGUtZWZmZWN0IG9mIHNlbmRpbmcgYW4gZW1haWwgdG8gdGhlIHByb3ZpZGVkIGVtYWlsIGFkZHJlc3MuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZW1haWxBZGRyZXNzIFRoZSBlbWFpbCBhZGRyZXNzIHRvIGFkZFxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gUmVzb2x2ZXMgd2hlbiB0aGUgZW1haWwgaGFzIGJlZW4gc2VudC4gVGhlbiBjYWxsIGNoZWNrRW1haWxMaW5rQ2xpY2tlZCgpLlxyXG4gICAgICovXHJcbiAgICBhZGRFbWFpbEFkZHJlc3MoZW1haWxBZGRyZXNzKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZXF1ZXN0QWRkM3BpZEVtYWlsVG9rZW4oZW1haWxBZGRyZXNzLCB0aGlzLmNsaWVudFNlY3JldCwgMSkudGhlbigocmVzKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2Vzc2lvbklkID0gcmVzLnNpZDtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcztcclxuICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlID09PSAnTV9USFJFRVBJRF9JTl9VU0UnKSB7XHJcbiAgICAgICAgICAgICAgICBlcnIubWVzc2FnZSA9IF90KCdUaGlzIGVtYWlsIGFkZHJlc3MgaXMgYWxyZWFkeSBpbiB1c2UnKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlcnIuaHR0cFN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBlcnIubWVzc2FnZSArIGAgKFN0YXR1cyAke2Vyci5odHRwU3RhdHVzfSlgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRocm93IGVycjtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEF0dGVtcHQgdG8gYmluZCBhbiBlbWFpbCB0aHJlZXBpZCBvbiB0aGUgaWRlbnRpdHkgc2VydmVyIHZpYSB0aGUgaG9tZXNlcnZlci5cclxuICAgICAqIFRoaXMgd2lsbCB0cmlnZ2VyIGEgc2lkZS1lZmZlY3Qgb2Ygc2VuZGluZyBhbiBlbWFpbCB0byB0aGUgcHJvdmlkZWQgZW1haWwgYWRkcmVzcy5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBlbWFpbEFkZHJlc3MgVGhlIGVtYWlsIGFkZHJlc3MgdG8gYWRkXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSBSZXNvbHZlcyB3aGVuIHRoZSBlbWFpbCBoYXMgYmVlbiBzZW50LiBUaGVuIGNhbGwgY2hlY2tFbWFpbExpbmtDbGlja2VkKCkuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGJpbmRFbWFpbEFkZHJlc3MoZW1haWxBZGRyZXNzKSB7XHJcbiAgICAgICAgdGhpcy5iaW5kID0gdHJ1ZTtcclxuICAgICAgICBpZiAoYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRvZXNTZXJ2ZXJTdXBwb3J0U2VwYXJhdGVBZGRBbmRCaW5kKCkpIHtcclxuICAgICAgICAgICAgLy8gRm9yIHNlcGFyYXRlIGJpbmQsIHJlcXVlc3QgYSB0b2tlbiBkaXJlY3RseSBmcm9tIHRoZSBJUy5cclxuICAgICAgICAgICAgY29uc3QgYXV0aENsaWVudCA9IG5ldyBJZGVudGl0eUF1dGhDbGllbnQoKTtcclxuICAgICAgICAgICAgY29uc3QgaWRlbnRpdHlBY2Nlc3NUb2tlbiA9IGF3YWl0IGF1dGhDbGllbnQuZ2V0QWNjZXNzVG9rZW4oKTtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZXF1ZXN0RW1haWxUb2tlbihcclxuICAgICAgICAgICAgICAgIGVtYWlsQWRkcmVzcywgdGhpcy5jbGllbnRTZWNyZXQsIDEsXHJcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQsIHVuZGVmaW5lZCwgaWRlbnRpdHlBY2Nlc3NUb2tlbixcclxuICAgICAgICAgICAgKS50aGVuKChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2Vzc2lvbklkID0gcmVzLnNpZDtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXM7XHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVyci5lcnJjb2RlID09PSAnTV9USFJFRVBJRF9JTl9VU0UnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBfdCgnVGhpcyBlbWFpbCBhZGRyZXNzIGlzIGFscmVhZHkgaW4gdXNlJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVyci5odHRwU3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBlcnIubWVzc2FnZSArIGAgKFN0YXR1cyAke2Vyci5odHRwU3RhdHVzfSlgO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBGb3IgdGFuZ2xlZCBiaW5kLCByZXF1ZXN0IGEgdG9rZW4gdmlhIHRoZSBIUy5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYWRkRW1haWxBZGRyZXNzKGVtYWlsQWRkcmVzcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQXR0ZW1wdCB0byBhZGQgYSBNU0lTRE4gdGhyZWVwaWQgdG8gdGhlIGhvbWVzZXJ2ZXIuXHJcbiAgICAgKiBUaGlzIHdpbGwgdHJpZ2dlciBhIHNpZGUtZWZmZWN0IG9mIHNlbmRpbmcgYW4gU01TIHRvIHRoZSBwcm92aWRlZCBwaG9uZSBudW1iZXIuXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGhvbmVDb3VudHJ5IFRoZSBJU08gMiBsZXR0ZXIgY29kZSBvZiB0aGUgY291bnRyeSB0byByZXNvbHZlIHBob25lTnVtYmVyIGluXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGhvbmVOdW1iZXIgVGhlIG5hdGlvbmFsIG9yIGludGVybmF0aW9uYWwgZm9ybWF0dGVkIHBob25lIG51bWJlciB0byBhZGRcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IFJlc29sdmVzIHdoZW4gdGhlIHRleHQgbWVzc2FnZSBoYXMgYmVlbiBzZW50LiBUaGVuIGNhbGwgaGF2ZU1zaXNkblRva2VuKCkuXHJcbiAgICAgKi9cclxuICAgIGFkZE1zaXNkbihwaG9uZUNvdW50cnksIHBob25lTnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZXF1ZXN0QWRkM3BpZE1zaXNkblRva2VuKFxyXG4gICAgICAgICAgICBwaG9uZUNvdW50cnksIHBob25lTnVtYmVyLCB0aGlzLmNsaWVudFNlY3JldCwgMSxcclxuICAgICAgICApLnRoZW4oKHJlcykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlc3Npb25JZCA9IHJlcy5zaWQ7XHJcbiAgICAgICAgICAgIHRoaXMuc3VibWl0VXJsID0gcmVzLnN1Ym1pdF91cmw7XHJcbiAgICAgICAgICAgIHJldHVybiByZXM7XHJcbiAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgIGlmIChlcnIuZXJyY29kZSA9PT0gJ01fVEhSRUVQSURfSU5fVVNFJykge1xyXG4gICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBfdCgnVGhpcyBwaG9uZSBudW1iZXIgaXMgYWxyZWFkeSBpbiB1c2UnKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlcnIuaHR0cFN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBlcnIubWVzc2FnZSArIGAgKFN0YXR1cyAke2Vyci5odHRwU3RhdHVzfSlgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRocm93IGVycjtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEF0dGVtcHQgdG8gYmluZCBhIE1TSVNETiB0aHJlZXBpZCBvbiB0aGUgaWRlbnRpdHkgc2VydmVyIHZpYSB0aGUgaG9tZXNlcnZlci5cclxuICAgICAqIFRoaXMgd2lsbCB0cmlnZ2VyIGEgc2lkZS1lZmZlY3Qgb2Ygc2VuZGluZyBhbiBTTVMgdG8gdGhlIHByb3ZpZGVkIHBob25lIG51bWJlci5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwaG9uZUNvdW50cnkgVGhlIElTTyAyIGxldHRlciBjb2RlIG9mIHRoZSBjb3VudHJ5IHRvIHJlc29sdmUgcGhvbmVOdW1iZXIgaW5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwaG9uZU51bWJlciBUaGUgbmF0aW9uYWwgb3IgaW50ZXJuYXRpb25hbCBmb3JtYXR0ZWQgcGhvbmUgbnVtYmVyIHRvIGFkZFxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZX0gUmVzb2x2ZXMgd2hlbiB0aGUgdGV4dCBtZXNzYWdlIGhhcyBiZWVuIHNlbnQuIFRoZW4gY2FsbCBoYXZlTXNpc2RuVG9rZW4oKS5cclxuICAgICAqL1xyXG4gICAgYXN5bmMgYmluZE1zaXNkbihwaG9uZUNvdW50cnksIHBob25lTnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5iaW5kID0gdHJ1ZTtcclxuICAgICAgICBpZiAoYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRvZXNTZXJ2ZXJTdXBwb3J0U2VwYXJhdGVBZGRBbmRCaW5kKCkpIHtcclxuICAgICAgICAgICAgLy8gRm9yIHNlcGFyYXRlIGJpbmQsIHJlcXVlc3QgYSB0b2tlbiBkaXJlY3RseSBmcm9tIHRoZSBJUy5cclxuICAgICAgICAgICAgY29uc3QgYXV0aENsaWVudCA9IG5ldyBJZGVudGl0eUF1dGhDbGllbnQoKTtcclxuICAgICAgICAgICAgY29uc3QgaWRlbnRpdHlBY2Nlc3NUb2tlbiA9IGF3YWl0IGF1dGhDbGllbnQuZ2V0QWNjZXNzVG9rZW4oKTtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5yZXF1ZXN0TXNpc2RuVG9rZW4oXHJcbiAgICAgICAgICAgICAgICBwaG9uZUNvdW50cnksIHBob25lTnVtYmVyLCB0aGlzLmNsaWVudFNlY3JldCwgMSxcclxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCBpZGVudGl0eUFjY2Vzc1Rva2VuLFxyXG4gICAgICAgICAgICApLnRoZW4oKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXNzaW9uSWQgPSByZXMuc2lkO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlcztcclxuICAgICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyLmVycmNvZGUgPT09ICdNX1RIUkVFUElEX0lOX1VTRScpIHtcclxuICAgICAgICAgICAgICAgICAgICBlcnIubWVzc2FnZSA9IF90KCdUaGlzIHBob25lIG51bWJlciBpcyBhbHJlYWR5IGluIHVzZScpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChlcnIuaHR0cFN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgICAgIGVyci5tZXNzYWdlID0gZXJyLm1lc3NhZ2UgKyBgIChTdGF0dXMgJHtlcnIuaHR0cFN0YXR1c30pYDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRocm93IGVycjtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gRm9yIHRhbmdsZWQgYmluZCwgcmVxdWVzdCBhIHRva2VuIHZpYSB0aGUgSFMuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFkZE1zaXNkbihwaG9uZUNvdW50cnksIHBob25lTnVtYmVyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIGVtYWlsIGxpbmsgaGFzIGJlZW4gY2xpY2tlZCBieSBhdHRlbXB0aW5nIHRvIGFkZCB0aGUgdGhyZWVwaWRcclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IFJlc29sdmVzIGlmIHRoZSBlbWFpbCBhZGRyZXNzIHdhcyBhZGRlZC4gUmVqZWN0cyB3aXRoIGFuIG9iamVjdFxyXG4gICAgICogd2l0aCBhIFwibWVzc2FnZVwiIHByb3BlcnR5IHdoaWNoIGNvbnRhaW5zIGEgaHVtYW4tcmVhZGFibGUgbWVzc2FnZSBkZXRhaWxpbmcgd2h5XHJcbiAgICAgKiB0aGUgcmVxdWVzdCBmYWlsZWQuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGNoZWNrRW1haWxMaW5rQ2xpY2tlZCgpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAoYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmRvZXNTZXJ2ZXJTdXBwb3J0U2VwYXJhdGVBZGRBbmRCaW5kKCkpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmJpbmQpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhdXRoQ2xpZW50ID0gbmV3IElkZW50aXR5QXV0aENsaWVudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGlkZW50aXR5QWNjZXNzVG9rZW4gPSBhd2FpdCBhdXRoQ2xpZW50LmdldEFjY2Vzc1Rva2VuKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLmJpbmRUaHJlZVBpZCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpZDogdGhpcy5zZXNzaW9uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaWVudF9zZWNyZXQ6IHRoaXMuY2xpZW50U2VjcmV0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZF9zZXJ2ZXI6IGdldElkU2VydmVyRG9tYWluKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkX2FjY2Vzc190b2tlbjogaWRlbnRpdHlBY2Nlc3NUb2tlbixcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fbWFrZUFkZFRocmVlcGlkT25seVJlcXVlc3QoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRoZSBzcGVjIGhhcyBhbHdheXMgcmVxdWlyZWQgdGhpcyB0byB1c2UgVUkgYXV0aCBidXQgc3luYXBzZSBicmllZmx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGltcGxlbWVudGVkIGl0IHdpdGhvdXQsIHNvIHRoaXMgbWF5IGp1c3Qgc3VjY2VlZCBhbmQgdGhhdCdzIE9LLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZS5odHRwU3RhdHVzICE9PSA0MDEgfHwgIWUuZGF0YSB8fCAhZS5kYXRhLmZsb3dzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBkb2Vzbid0IGxvb2sgbGlrZSBhbiBpbnRlcmFjdGl2ZS1hdXRoIGZhaWx1cmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHBvcCB1cCBhbiBpbnRlcmFjdGl2ZSBhdXRoIGRpYWxvZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBJbnRlcmFjdGl2ZUF1dGhEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5JbnRlcmFjdGl2ZUF1dGhEaWFsb2dcIik7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGlhbG9nQWVzdGhldGljcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtTU09BdXRoRW50cnkuUEhBU0VfUFJFQVVUSF06IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJVc2UgU2luZ2xlIFNpZ24gT24gdG8gY29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9keTogX3QoXCJDb25maXJtIGFkZGluZyB0aGlzIGVtYWlsIGFkZHJlc3MgYnkgdXNpbmcgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlNpbmdsZSBTaWduIE9uIHRvIHByb3ZlIHlvdXIgaWRlbnRpdHkuXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlVGV4dDogX3QoXCJTaW5nbGUgU2lnbiBPblwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZUtpbmQ6IFwicHJpbWFyeVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtTU09BdXRoRW50cnkuUEhBU0VfUE9TVEFVVEhdOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KFwiQ29uZmlybSBhZGRpbmcgZW1haWxcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9keTogX3QoXCJDbGljayB0aGUgYnV0dG9uIGJlbG93IHRvIGNvbmZpcm0gYWRkaW5nIHRoaXMgZW1haWwgYWRkcmVzcy5cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWVUZXh0OiBfdChcIkNvbmZpcm1cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWVLaW5kOiBcInByaW1hcnlcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHsgZmluaXNoZWQgfSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0FkZCBFbWFpbCcsICcnLCBJbnRlcmFjdGl2ZUF1dGhEaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkFkZCBFbWFpbCBBZGRyZXNzXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF0cml4Q2xpZW50OiBNYXRyaXhDbGllbnRQZWcuZ2V0KCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRoRGF0YTogZS5kYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFrZVJlcXVlc3Q6IHRoaXMuX21ha2VBZGRUaHJlZXBpZE9ubHlSZXF1ZXN0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWVzdGhldGljc0ZvclN0YWdlUGhhc2VzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1NTT0F1dGhFbnRyeS5MT0dJTl9UWVBFXTogZGlhbG9nQWVzdGhldGljcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbU1NPQXV0aEVudHJ5LlVOU1RBQkxFX0xPR0lOX1RZUEVdOiBkaWFsb2dBZXN0aGV0aWNzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmaW5pc2hlZDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuYWRkVGhyZWVQaWQoe1xyXG4gICAgICAgICAgICAgICAgICAgIHNpZDogdGhpcy5zZXNzaW9uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xpZW50X3NlY3JldDogdGhpcy5jbGllbnRTZWNyZXQsXHJcbiAgICAgICAgICAgICAgICAgICAgaWRfc2VydmVyOiBnZXRJZFNlcnZlckRvbWFpbigpLFxyXG4gICAgICAgICAgICAgICAgfSwgdGhpcy5iaW5kKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBpZiAoZXJyLmh0dHBTdGF0dXMgPT09IDQwMSkge1xyXG4gICAgICAgICAgICAgICAgZXJyLm1lc3NhZ2UgPSBfdCgnRmFpbGVkIHRvIHZlcmlmeSBlbWFpbCBhZGRyZXNzOiBtYWtlIHN1cmUgeW91IGNsaWNrZWQgdGhlIGxpbmsgaW4gdGhlIGVtYWlsJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXJyLmh0dHBTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIGVyci5tZXNzYWdlICs9IGAgKFN0YXR1cyAke2Vyci5odHRwU3RhdHVzfSlgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRocm93IGVycjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gYXV0aCBVSSBhdXRoIG9iamVjdFxyXG4gICAgICogQHJldHVybiB7UHJvbWlzZTxPYmplY3Q+fSBSZXNwb25zZSBmcm9tIC8zcGlkL2FkZCBjYWxsIChpbiBjdXJyZW50IHNwZWMsIGFuIGVtcHR5IG9iamVjdClcclxuICAgICAqL1xyXG4gICAgX21ha2VBZGRUaHJlZXBpZE9ubHlSZXF1ZXN0ID0gKGF1dGgpID0+IHtcclxuICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLmFkZFRocmVlUGlkT25seSh7XHJcbiAgICAgICAgICAgIHNpZDogdGhpcy5zZXNzaW9uSWQsXHJcbiAgICAgICAgICAgIGNsaWVudF9zZWNyZXQ6IHRoaXMuY2xpZW50U2VjcmV0LFxyXG4gICAgICAgICAgICBhdXRoLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGFrZXMgYSBwaG9uZSBudW1iZXIgdmVyaWZpY2F0aW9uIGNvZGUgYXMgZW50ZXJlZCBieSB0aGUgdXNlciBhbmQgdmFsaWRhdGVzXHJcbiAgICAgKiBpdCB3aXRoIHRoZSBJRCBzZXJ2ZXIsIHRoZW4gaWYgc3VjY2Vzc2Z1bCwgYWRkcyB0aGUgcGhvbmUgbnVtYmVyLlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG1zaXNkblRva2VuIHBob25lIG51bWJlciB2ZXJpZmljYXRpb24gY29kZSBhcyBlbnRlcmVkIGJ5IHRoZSB1c2VyXHJcbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSBSZXNvbHZlcyBpZiB0aGUgcGhvbmUgbnVtYmVyIHdhcyBhZGRlZC4gUmVqZWN0cyB3aXRoIGFuIG9iamVjdFxyXG4gICAgICogd2l0aCBhIFwibWVzc2FnZVwiIHByb3BlcnR5IHdoaWNoIGNvbnRhaW5zIGEgaHVtYW4tcmVhZGFibGUgbWVzc2FnZSBkZXRhaWxpbmcgd2h5XHJcbiAgICAgKiB0aGUgcmVxdWVzdCBmYWlsZWQuXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGhhdmVNc2lzZG5Ub2tlbihtc2lzZG5Ub2tlbikge1xyXG4gICAgICAgIGNvbnN0IGF1dGhDbGllbnQgPSBuZXcgSWRlbnRpdHlBdXRoQ2xpZW50KCk7XHJcbiAgICAgICAgY29uc3Qgc3VwcG9ydHNTZXBhcmF0ZUFkZEFuZEJpbmQgPVxyXG4gICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZG9lc1NlcnZlclN1cHBvcnRTZXBhcmF0ZUFkZEFuZEJpbmQoKTtcclxuXHJcbiAgICAgICAgbGV0IHJlc3VsdDtcclxuICAgICAgICBpZiAodGhpcy5zdWJtaXRVcmwpIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLnN1Ym1pdE1zaXNkblRva2VuT3RoZXJVcmwoXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdFVybCxcclxuICAgICAgICAgICAgICAgIHRoaXMuc2Vzc2lvbklkLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGllbnRTZWNyZXQsXHJcbiAgICAgICAgICAgICAgICBtc2lzZG5Ub2tlbixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuYmluZCB8fCAhc3VwcG9ydHNTZXBhcmF0ZUFkZEFuZEJpbmQpIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gYXdhaXQgTWF0cml4Q2xpZW50UGVnLmdldCgpLnN1Ym1pdE1zaXNkblRva2VuKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXNzaW9uSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsaWVudFNlY3JldCxcclxuICAgICAgICAgICAgICAgIG1zaXNkblRva2VuLFxyXG4gICAgICAgICAgICAgICAgYXdhaXQgYXV0aENsaWVudC5nZXRBY2Nlc3NUb2tlbigpLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlRoZSBhZGQgLyBiaW5kIHdpdGggTVNJU0ROIGZsb3cgaXMgbWlzY29uZmlndXJlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHJlc3VsdC5lcnJjb2RlKSB7XHJcbiAgICAgICAgICAgIHRocm93IHJlc3VsdDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzdXBwb3J0c1NlcGFyYXRlQWRkQW5kQmluZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5iaW5kKSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuYmluZFRocmVlUGlkKHtcclxuICAgICAgICAgICAgICAgICAgICBzaWQ6IHRoaXMuc2Vzc2lvbklkLFxyXG4gICAgICAgICAgICAgICAgICAgIGNsaWVudF9zZWNyZXQ6IHRoaXMuY2xpZW50U2VjcmV0LFxyXG4gICAgICAgICAgICAgICAgICAgIGlkX3NlcnZlcjogZ2V0SWRTZXJ2ZXJEb21haW4oKSxcclxuICAgICAgICAgICAgICAgICAgICBpZF9hY2Nlc3NfdG9rZW46IGF3YWl0IGF1dGhDbGllbnQuZ2V0QWNjZXNzVG9rZW4oKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9tYWtlQWRkVGhyZWVwaWRPbmx5UmVxdWVzdCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBUaGUgc3BlYyBoYXMgYWx3YXlzIHJlcXVpcmVkIHRoaXMgdG8gdXNlIFVJIGF1dGggYnV0IHN5bmFwc2UgYnJpZWZseVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGltcGxlbWVudGVkIGl0IHdpdGhvdXQsIHNvIHRoaXMgbWF5IGp1c3Qgc3VjY2VlZCBhbmQgdGhhdCdzIE9LLlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZS5odHRwU3RhdHVzICE9PSA0MDEgfHwgIWUuZGF0YSB8fCAhZS5kYXRhLmZsb3dzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGRvZXNuJ3QgbG9vayBsaWtlIGFuIGludGVyYWN0aXZlLWF1dGggZmFpbHVyZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcG9wIHVwIGFuIGludGVyYWN0aXZlIGF1dGggZGlhbG9nXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgSW50ZXJhY3RpdmVBdXRoRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuSW50ZXJhY3RpdmVBdXRoRGlhbG9nXCIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkaWFsb2dBZXN0aGV0aWNzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBbU1NPQXV0aEVudHJ5LlBIQVNFX1BSRUFVVEhdOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJVc2UgU2luZ2xlIFNpZ24gT24gdG8gY29udGludWVcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib2R5OiBfdChcIkNvbmZpcm0gYWRkaW5nIHRoaXMgcGhvbmUgbnVtYmVyIGJ5IHVzaW5nIFwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlNpbmdsZSBTaWduIE9uIHRvIHByb3ZlIHlvdXIgaWRlbnRpdHkuXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWVUZXh0OiBfdChcIlNpbmdsZSBTaWduIE9uXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWVLaW5kOiBcInByaW1hcnlcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1NTT0F1dGhFbnRyeS5QSEFTRV9QT1NUQVVUSF06IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIkNvbmZpcm0gYWRkaW5nIHBob25lIG51bWJlclwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvZHk6IF90KFwiQ2xpY2sgdGhlIGJ1dHRvbiBiZWxvdyB0byBjb25maXJtIGFkZGluZyB0aGlzIHBob25lIG51bWJlci5cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZVRleHQ6IF90KFwiQ29uZmlybVwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlS2luZDogXCJwcmltYXJ5XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB7IGZpbmlzaGVkIH0gPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdBZGQgTVNJU0ROJywgJycsIEludGVyYWN0aXZlQXV0aERpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogX3QoXCJBZGQgUGhvbmUgTnVtYmVyXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXRyaXhDbGllbnQ6IE1hdHJpeENsaWVudFBlZy5nZXQoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0aERhdGE6IGUuZGF0YSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFrZVJlcXVlc3Q6IHRoaXMuX21ha2VBZGRUaHJlZXBpZE9ubHlSZXF1ZXN0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhZXN0aGV0aWNzRm9yU3RhZ2VQaGFzZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtTU09BdXRoRW50cnkuTE9HSU5fVFlQRV06IGRpYWxvZ0Flc3RoZXRpY3MsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbU1NPQXV0aEVudHJ5LlVOU1RBQkxFX0xPR0lOX1RZUEVdOiBkaWFsb2dBZXN0aGV0aWNzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmaW5pc2hlZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGF3YWl0IE1hdHJpeENsaWVudFBlZy5nZXQoKS5hZGRUaHJlZVBpZCh7XHJcbiAgICAgICAgICAgICAgICBzaWQ6IHRoaXMuc2Vzc2lvbklkLFxyXG4gICAgICAgICAgICAgICAgY2xpZW50X3NlY3JldDogdGhpcy5jbGllbnRTZWNyZXQsXHJcbiAgICAgICAgICAgICAgICBpZF9zZXJ2ZXI6IGdldElkU2VydmVyRG9tYWluKCksXHJcbiAgICAgICAgICAgIH0sIHRoaXMuYmluZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==