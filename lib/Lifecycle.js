"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadSession = loadSession;
exports.getStoredSessionOwner = getStoredSessionOwner;
exports.getStoredSessionIsGuest = getStoredSessionIsGuest;
exports.attemptTokenLogin = attemptTokenLogin;
exports.handleInvalidStoreError = handleInvalidStoreError;
exports.getLocalStorageSessionVars = getLocalStorageSessionVars;
exports.setLoggedIn = setLoggedIn;
exports.hydrateSession = hydrateSession;
exports.logout = logout;
exports.softLogout = softLogout;
exports.isSoftLogout = isSoftLogout;
exports.isLoggingOut = isLoggingOut;
exports.onLoggedOut = onLoggedOut;
exports.stopMatrixClient = stopMatrixClient;

var _matrixJsSdk = _interopRequireDefault(require("matrix-js-sdk"));

var _MatrixClientPeg = require("./MatrixClientPeg");

var _EventIndexPeg = _interopRequireDefault(require("./indexing/EventIndexPeg"));

var _createMatrixClient = _interopRequireDefault(require("./utils/createMatrixClient"));

var _Analytics = _interopRequireDefault(require("./Analytics"));

var _Notifier = _interopRequireDefault(require("./Notifier"));

var _UserActivity = _interopRequireDefault(require("./UserActivity"));

var _Presence = _interopRequireDefault(require("./Presence"));

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var _DMRoomMap = _interopRequireDefault(require("./utils/DMRoomMap"));

var _Modal = _interopRequireDefault(require("./Modal"));

var sdk = _interopRequireWildcard(require("./index"));

var _ActiveWidgetStore = _interopRequireDefault(require("./stores/ActiveWidgetStore"));

var _PlatformPeg = _interopRequireDefault(require("./PlatformPeg"));

var _Login = require("./Login");

var StorageManager = _interopRequireWildcard(require("./utils/StorageManager"));

var _SettingsStore = _interopRequireDefault(require("./settings/SettingsStore"));

var _TypingStore = _interopRequireDefault(require("./stores/TypingStore"));

var _ToastStore = _interopRequireDefault(require("./stores/ToastStore"));

var _IntegrationManagers = require("./integrations/IntegrationManagers");

var _Mjolnir = require("./mjolnir/Mjolnir");

var _DeviceListener = _interopRequireDefault(require("./DeviceListener"));

var _Jitsi = require("./widgets/Jitsi");

/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Called at startup, to attempt to build a logged-in Matrix session. It tries
 * a number of things:
 *
 *
 * 1. if we have a guest access token in the fragment query params, it uses
 *    that.
 *
 * 2. if an access token is stored in local storage (from a previous session),
 *    it uses that.
 *
 * 3. it attempts to auto-register as a guest user.
 *
 * If any of steps 1-4 are successful, it will call {_doSetLoggedIn}, which in
 * turn will raise on_logged_in and will_start_client events.
 *
 * @param {object} opts
 *
 * @param {object} opts.fragmentQueryParams: string->string map of the
 *     query-parameters extracted from the #-fragment of the starting URI.
 *
 * @param {boolean} opts.enableGuest: set to true to enable guest access tokens
 *     and auto-guest registrations.
 *
 * @params {string} opts.guestHsUrl: homeserver URL. Only used if enableGuest is
 *     true; defines the HS to register against.
 *
 * @params {string} opts.guestIsUrl: homeserver URL. Only used if enableGuest is
 *     true; defines the IS to use.
 *
 * @params {bool} opts.ignoreGuest: If the stored session is a guest account, ignore
 *     it and don't load it.
 *
 * @returns {Promise} a promise which resolves when the above process completes.
 *     Resolves to `true` if we ended up starting a session, or `false` if we
 *     failed.
 */
async function loadSession(opts) {
  try {
    let enableGuest = opts.enableGuest || false;
    const guestHsUrl = opts.guestHsUrl;
    const guestIsUrl = opts.guestIsUrl;
    const fragmentQueryParams = opts.fragmentQueryParams || {};
    const defaultDeviceDisplayName = opts.defaultDeviceDisplayName;

    if (enableGuest && !guestHsUrl) {
      console.warn("Cannot enable guest access: can't determine HS URL to use");
      enableGuest = false;
    }

    if (enableGuest && fragmentQueryParams.guest_user_id && fragmentQueryParams.guest_access_token) {
      console.log("Using guest access credentials");
      return _doSetLoggedIn({
        userId: fragmentQueryParams.guest_user_id,
        accessToken: fragmentQueryParams.guest_access_token,
        homeserverUrl: guestHsUrl,
        identityServerUrl: guestIsUrl,
        guest: true
      }, true).then(() => true);
    }

    const success = await _restoreFromLocalStorage({
      ignoreGuest: Boolean(opts.ignoreGuest)
    });

    if (success) {
      return true;
    }

    if (enableGuest) {
      return _registerAsGuest(guestHsUrl, guestIsUrl, defaultDeviceDisplayName);
    } // fall back to welcome screen


    return false;
  } catch (e) {
    if (e instanceof AbortLoginAndRebuildStorage) {
      // If we're aborting login because of a storage inconsistency, we don't
      // need to show the general failure dialog. Instead, just go back to welcome.
      return false;
    }

    return _handleLoadSessionFailure(e);
  }
}
/**
 * Gets the user ID of the persisted session, if one exists. This does not validate
 * that the user's credentials still work, just that they exist and that a user ID
 * is associated with them. The session is not loaded.
 * @returns {String} The persisted session's owner, if an owner exists. Null otherwise.
 */


function getStoredSessionOwner() {
  const {
    hsUrl,
    userId,
    accessToken
  } = getLocalStorageSessionVars();
  return hsUrl && userId && accessToken ? userId : null;
}
/**
 * @returns {bool} True if the stored session is for a guest user or false if it is
 *     for a real user. If there is no stored session, return null.
 */


function getStoredSessionIsGuest() {
  const sessVars = getLocalStorageSessionVars();
  return sessVars.hsUrl && sessVars.userId && sessVars.accessToken ? sessVars.isGuest : null;
}
/**
 * @param {Object} queryParams    string->string map of the
 *     query-parameters extracted from the real query-string of the starting
 *     URI.
 *
 * @param {String} defaultDeviceDisplayName
 *
 * @returns {Promise} promise which resolves to true if we completed the token
 *    login, else false
 */


function attemptTokenLogin(queryParams, defaultDeviceDisplayName) {
  if (!queryParams.loginToken) {
    return Promise.resolve(false);
  }

  if (!queryParams.homeserver) {
    console.warn("Cannot log in with token: can't determine HS URL to use");
    return Promise.resolve(false);
  }

  return (0, _Login.sendLoginRequest)(queryParams.homeserver, queryParams.identityServer, "m.login.token", {
    token: queryParams.loginToken,
    initial_device_display_name: defaultDeviceDisplayName
  }).then(function (creds) {
    console.log("Logged in with token");
    return _clearStorage().then(() => {
      _persistCredentialsToLocalStorage(creds);

      return true;
    });
  }).catch(err => {
    console.error("Failed to log in with login token: " + err + " " + err.data);
    return false;
  });
}

function handleInvalidStoreError(e) {
  if (e.reason === _matrixJsSdk.default.InvalidStoreError.TOGGLED_LAZY_LOADING) {
    return Promise.resolve().then(() => {
      const lazyLoadEnabled = e.value;

      if (lazyLoadEnabled) {
        const LazyLoadingResyncDialog = sdk.getComponent("views.dialogs.LazyLoadingResyncDialog");
        return new Promise(resolve => {
          _Modal.default.createDialog(LazyLoadingResyncDialog, {
            onFinished: resolve
          });
        });
      } else {
        // show warning about simultaneous use
        // between LL/non-LL version on same host.
        // as disabling LL when previously enabled
        // is a strong indicator of this (/develop & /app)
        const LazyLoadingDisabledDialog = sdk.getComponent("views.dialogs.LazyLoadingDisabledDialog");
        return new Promise(resolve => {
          _Modal.default.createDialog(LazyLoadingDisabledDialog, {
            onFinished: resolve,
            host: window.location.host
          });
        });
      }
    }).then(() => {
      return _MatrixClientPeg.MatrixClientPeg.get().store.deleteAllData();
    }).then(() => {
      _PlatformPeg.default.get().reload();
    });
  }
}

function _registerAsGuest(hsUrl, isUrl, defaultDeviceDisplayName) {
  console.log("Doing guest login on ".concat(hsUrl)); // create a temporary MatrixClient to do the login

  const client = _matrixJsSdk.default.createClient({
    baseUrl: hsUrl
  });

  return client.registerGuest({
    body: {
      initial_device_display_name: defaultDeviceDisplayName
    }
  }).then(creds => {
    console.log("Registered as guest: ".concat(creds.user_id));
    return _doSetLoggedIn({
      userId: creds.user_id,
      deviceId: creds.device_id,
      accessToken: creds.access_token,
      homeserverUrl: hsUrl,
      identityServerUrl: isUrl,
      guest: true
    }, true).then(() => true);
  }, err => {
    console.error("Failed to register as guest", err);
    return false;
  });
}
/**
 * Retrieves information about the stored session in localstorage. The session
 * may not be valid, as it is not tested for consistency here.
 * @returns {Object} Information about the session - see implementation for variables.
 */


function getLocalStorageSessionVars() {
  const hsUrl = localStorage.getItem("mx_hs_url");
  const isUrl = localStorage.getItem("mx_is_url");
  const accessToken = localStorage.getItem("mx_access_token");
  const userId = localStorage.getItem("mx_user_id");
  const deviceId = localStorage.getItem("mx_device_id");
  let isGuest;

  if (localStorage.getItem("mx_is_guest") !== null) {
    isGuest = localStorage.getItem("mx_is_guest") === "true";
  } else {
    // legacy key name
    isGuest = localStorage.getItem("matrix-is-guest") === "true";
  }

  return {
    hsUrl,
    isUrl,
    accessToken,
    userId,
    deviceId,
    isGuest
  };
} // returns a promise which resolves to true if a session is found in
// localstorage
//
// N.B. Lifecycle.js should not maintain any further localStorage state, we
//      are moving towards using SessionStore to keep track of state related
//      to the current session (which is typically backed by localStorage).
//
//      The plan is to gradually move the localStorage access done here into
//      SessionStore to avoid bugs where the view becomes out-of-sync with
//      localStorage (e.g. isGuest etc.)


async function _restoreFromLocalStorage(opts) {
  const ignoreGuest = opts.ignoreGuest;

  if (!localStorage) {
    return false;
  }

  const {
    hsUrl,
    isUrl,
    accessToken,
    userId,
    deviceId,
    isGuest
  } = getLocalStorageSessionVars();

  if (accessToken && userId && hsUrl) {
    if (ignoreGuest && isGuest) {
      console.log("Ignoring stored guest account: " + userId);
      return false;
    }

    console.log("Restoring session for ".concat(userId));
    await _doSetLoggedIn({
      userId: userId,
      deviceId: deviceId,
      accessToken: accessToken,
      homeserverUrl: hsUrl,
      identityServerUrl: isUrl,
      guest: isGuest
    }, false);
    return true;
  } else {
    console.log("No previous session found.");
    return false;
  }
}

async function _handleLoadSessionFailure(e) {
  console.error("Unable to load session", e);
  const SessionRestoreErrorDialog = sdk.getComponent('views.dialogs.SessionRestoreErrorDialog');

  const modal = _Modal.default.createTrackedDialog('Session Restore Error', '', SessionRestoreErrorDialog, {
    error: e.message
  });

  const [success] = await modal.finished;

  if (success) {
    // user clicked continue.
    await _clearStorage();
    return false;
  } // try, try again


  return loadSession();
}
/**
 * Transitions to a logged-in state using the given credentials.
 *
 * Starts the matrix client and all other react-sdk services that
 * listen for events while a session is logged in.
 *
 * Also stops the old MatrixClient and clears old credentials/etc out of
 * storage before starting the new client.
 *
 * @param {MatrixClientCreds} credentials The credentials to use
 *
 * @returns {Promise} promise which resolves to the new MatrixClient once it has been started
 */


function setLoggedIn(credentials) {
  stopMatrixClient();
  return _doSetLoggedIn(credentials, true);
}
/**
 * Hydrates an existing session by using the credentials provided. This will
 * not clear any local storage, unlike setLoggedIn().
 *
 * Stops the existing Matrix client (without clearing its data) and starts a
 * new one in its place. This additionally starts all other react-sdk services
 * which use the new Matrix client.
 *
 * If the credentials belong to a different user from the session already stored,
 * the old session will be cleared automatically.
 *
 * @param {MatrixClientCreds} credentials The credentials to use
 *
 * @returns {Promise} promise which resolves to the new MatrixClient once it has been started
 */


function hydrateSession(credentials) {
  const oldUserId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

  const oldDeviceId = _MatrixClientPeg.MatrixClientPeg.get().getDeviceId();

  stopMatrixClient(); // unsets MatrixClientPeg.get()

  localStorage.removeItem("mx_soft_logout");
  _isLoggingOut = false;
  const overwrite = credentials.userId !== oldUserId || credentials.deviceId !== oldDeviceId;

  if (overwrite) {
    console.warn("Clearing all data: Old session belongs to a different user/session");
  }

  return _doSetLoggedIn(credentials, overwrite);
}
/**
 * fires on_logging_in, optionally clears localstorage, persists new credentials
 * to localstorage, starts the new client.
 *
 * @param {MatrixClientCreds} credentials
 * @param {Boolean} clearStorage
 *
 * @returns {Promise} promise which resolves to the new MatrixClient once it has been started
 */


async function _doSetLoggedIn(credentials, clearStorage) {
  console.log("----credentials---", credentials);
  credentials.guest = Boolean(credentials.guest);
  const softLogout = isSoftLogout();
  console.log("setLoggedIn: mxid: " + credentials.userId + " deviceId: " + credentials.deviceId + " guest: " + credentials.guest + " hs: " + credentials.homeserverUrl + " softLogout: " + softLogout); // This is dispatched to indicate that the user is still in the process of logging in
  // because async code may take some time to resolve, breaking the assumption that
  // `setLoggedIn` takes an "instant" to complete, and dispatch `on_logged_in` a few ms
  // later than MatrixChat might assume.
  //
  // we fire it *synchronously* to make sure it fires before on_logged_in.
  // (dis.dispatch uses `setTimeout`, which does not guarantee ordering.)

  _dispatcher.default.dispatch({
    action: 'on_logging_in'
  }, true);

  if (clearStorage) {
    await _clearStorage();
  }

  const results = await StorageManager.checkConsistency(); // If there's an inconsistency between account data in local storage and the
  // crypto store, we'll be generally confused when handling encrypted data.
  // Show a modal recommending a full reset of storage.

  if (results.dataInLocalStorage && results.cryptoInited && !results.dataInCryptoStore) {
    const signOut = await _showStorageEvictedDialog();

    if (signOut) {
      await _clearStorage(); // This error feels a bit clunky, but we want to make sure we don't go any
      // further and instead head back to sign in.

      throw new AbortLoginAndRebuildStorage("Aborting login in progress because of storage inconsistency");
    }
  }

  _Analytics.default.setLoggedIn(credentials.guest, credentials.homeserverUrl);

  if (localStorage) {
    try {
      _persistCredentialsToLocalStorage(credentials); // The user registered as a PWLU (PassWord-Less User), the generated password
      // is cached here such that the user can change it at a later time.


      if (credentials.password) {
        // Update SessionStore
        _dispatcher.default.dispatch({
          action: 'cached_password',
          cachedPassword: credentials.password
        });
      }
    } catch (e) {
      console.warn("Error using local storage: can't persist session!", e);
    }
  } else {
    console.warn("No local storage available: can't persist session!");
  }

  _MatrixClientPeg.MatrixClientPeg.replaceUsingCreds(credentials);

  _dispatcher.default.dispatch({
    action: 'on_logged_in'
  });

  await startMatrixClient(
  /*startSyncing=*/
  !softLogout);
  return _MatrixClientPeg.MatrixClientPeg.get();
}

function _showStorageEvictedDialog() {
  const StorageEvictedDialog = sdk.getComponent('views.dialogs.StorageEvictedDialog');
  return new Promise(resolve => {
    _Modal.default.createTrackedDialog('Storage evicted', '', StorageEvictedDialog, {
      onFinished: resolve
    });
  });
} // Note: Babel 6 requires the `transform-builtin-extend` plugin for this to satisfy
// `instanceof`. Babel 7 supports this natively in their class handling.


class AbortLoginAndRebuildStorage extends Error {}

function _persistCredentialsToLocalStorage(credentials) {
  console.log("i am here");
  localStorage.setItem("mx_hs_url", credentials.homeserverUrl);

  if (credentials.identityServerUrl) {
    localStorage.setItem("mx_is_url", credentials.identityServerUrl);
  }

  localStorage.setItem("mx_user_id", credentials.userId);
  localStorage.setItem("mx_access_token", credentials.accessToken);
  localStorage.setItem("mx_is_guest", JSON.stringify(credentials.guest)); // if we didn't get a deviceId from the login, leave mx_device_id unset,
  // rather than setting it to "undefined".
  //
  // (in this case MatrixClient doesn't bother with the crypto stuff
  // - that's fine for us).

  if (credentials.deviceId) {
    localStorage.setItem("mx_device_id", credentials.deviceId);
  }

  console.log("Session persisted for ".concat(credentials));
}

let _isLoggingOut = false;
/**
 * Logs the current session out and transitions to the logged-out state
 */

function logout() {
  if (!_MatrixClientPeg.MatrixClientPeg.get()) return;

  if (_MatrixClientPeg.MatrixClientPeg.get().isGuest()) {
    // logout doesn't work for guest sessions
    // Also we sometimes want to re-log in a guest session
    // if we abort the login
    onLoggedOut();
    return;
  }

  _isLoggingOut = true;

  _MatrixClientPeg.MatrixClientPeg.get().logout().then(onLoggedOut, err => {
    // Just throwing an error here is going to be very unhelpful
    // if you're trying to log out because your server's down and
    // you want to log into a different server, so just forget the
    // access token. It's annoying that this will leave the access
    // token still valid, but we should fix this by having access
    // tokens expire (and if you really think you've been compromised,
    // change your password).
    console.log("Failed to call logout API: token will not be invalidated");
    onLoggedOut();
  });
}

function softLogout() {
  if (!_MatrixClientPeg.MatrixClientPeg.get()) return; // Track that we've detected and trapped a soft logout. This helps prevent other
  // parts of the app from starting if there's no point (ie: don't sync if we've
  // been soft logged out, despite having credentials and data for a MatrixClient).

  localStorage.setItem("mx_soft_logout", "true"); // Dev note: please keep this log line around. It can be useful for track down
  // random clients stopping in the middle of the logs.

  console.log("Soft logout initiated");
  _isLoggingOut = true; // to avoid repeated flags
  // Ensure that we dispatch a view change **before** stopping the client so
  // so that React components unmount first. This avoids React soft crashes
  // that can occur when components try to use a null client.

  _dispatcher.default.dispatch({
    action: 'on_client_not_viable'
  }); // generic version of on_logged_out


  stopMatrixClient(
  /*unsetClient=*/
  false); // DO NOT CALL LOGOUT. A soft logout preserves data, logout does not.
}

function isSoftLogout() {
  return localStorage.getItem("mx_soft_logout") === "true";
}

function isLoggingOut() {
  return _isLoggingOut;
}
/**
 * Starts the matrix client and all other react-sdk services that
 * listen for events while a session is logged in.
 * @param {boolean} startSyncing True (default) to actually start
 * syncing the client.
 */


async function startMatrixClient(startSyncing = true) {
  console.log("Lifecycle: Starting MatrixClient"); // dispatch this before starting the matrix client: it's used
  // to add listeners for the 'sync' event so otherwise we'd have
  // a race condition (and we need to dispatch synchronously for this
  // to work).

  _dispatcher.default.dispatch({
    action: 'will_start_client'
  }, true);

  _Notifier.default.start();

  _UserActivity.default.sharedInstance().start();

  _TypingStore.default.sharedInstance().reset(); // just in case


  _ToastStore.default.sharedInstance().reset();

  _DMRoomMap.default.makeShared().start();

  _IntegrationManagers.IntegrationManagers.sharedInstance().startWatching();

  _ActiveWidgetStore.default.start(); // Start Mjolnir even though we haven't checked the feature flag yet. Starting
  // the thing just wastes CPU cycles, but should result in no actual functionality
  // being exposed to the user.


  _Mjolnir.Mjolnir.sharedInstance().start();

  if (startSyncing) {
    // The client might want to populate some views with events from the
    // index (e.g. the FilePanel), therefore initialize the event index
    // before the client.
    await _EventIndexPeg.default.init();
    await _MatrixClientPeg.MatrixClientPeg.start();
  } else {
    console.warn("Caller requested only auxiliary services be started");
    await _MatrixClientPeg.MatrixClientPeg.assign();
  } // This needs to be started after crypto is set up


  _DeviceListener.default.sharedInstance().start(); // Similarly, don't start sending presence updates until we've started
  // the client


  if (!_SettingsStore.default.getValue("lowBandwidth")) {
    _Presence.default.start();
  } // Now that we have a MatrixClientPeg, update the Jitsi info


  await _Jitsi.Jitsi.getInstance().update(); // dispatch that we finished starting up to wire up any other bits
  // of the matrix client that cannot be set prior to starting up.

  _dispatcher.default.dispatch({
    action: 'client_started'
  });

  if (isSoftLogout()) {
    softLogout();
  }
}
/*
 * Stops a running client and all related services, and clears persistent
 * storage. Used after a session has been logged out.
 */


async function onLoggedOut() {
  _isLoggingOut = false; // Ensure that we dispatch a view change **before** stopping the client so
  // so that React components unmount first. This avoids React soft crashes
  // that can occur when components try to use a null client.

  _dispatcher.default.dispatch({
    action: 'on_logged_out'
  }, true);

  stopMatrixClient();
  await _clearStorage();
}
/**
 * @returns {Promise} promise which resolves once the stores have been cleared
 */


async function _clearStorage() {
  _Analytics.default.disable();

  if (window.localStorage) {
    window.localStorage.clear();
  }

  if (window.sessionStorage) {
    window.sessionStorage.clear();
  } // create a temporary client to clear out the persistent stores.


  const cli = (0, _createMatrixClient.default)({
    // we'll never make any requests, so can pass a bogus HS URL
    baseUrl: ""
  });
  await _EventIndexPeg.default.deleteEventIndex();
  await cli.clearStores();
}
/**
 * Stop all the background processes related to the current client.
 * @param {boolean} unsetClient True (default) to abandon the client
 * on MatrixClientPeg after stopping.
 */


function stopMatrixClient(unsetClient = true) {
  _Notifier.default.stop();

  _UserActivity.default.sharedInstance().stop();

  _TypingStore.default.sharedInstance().reset();

  _Presence.default.stop();

  _ActiveWidgetStore.default.stop();

  _IntegrationManagers.IntegrationManagers.sharedInstance().stopWatching();

  _Mjolnir.Mjolnir.sharedInstance().stop();

  _DeviceListener.default.sharedInstance().stop();

  if (_DMRoomMap.default.shared()) _DMRoomMap.default.shared().stop();

  _EventIndexPeg.default.stop();

  const cli = _MatrixClientPeg.MatrixClientPeg.get();

  if (cli) {
    cli.stopClient();
    cli.removeAllListeners();

    if (unsetClient) {
      _MatrixClientPeg.MatrixClientPeg.unset();

      _EventIndexPeg.default.unset();
    }
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9MaWZlY3ljbGUuanMiXSwibmFtZXMiOlsibG9hZFNlc3Npb24iLCJvcHRzIiwiZW5hYmxlR3Vlc3QiLCJndWVzdEhzVXJsIiwiZ3Vlc3RJc1VybCIsImZyYWdtZW50UXVlcnlQYXJhbXMiLCJkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWUiLCJjb25zb2xlIiwid2FybiIsImd1ZXN0X3VzZXJfaWQiLCJndWVzdF9hY2Nlc3NfdG9rZW4iLCJsb2ciLCJfZG9TZXRMb2dnZWRJbiIsInVzZXJJZCIsImFjY2Vzc1Rva2VuIiwiaG9tZXNlcnZlclVybCIsImlkZW50aXR5U2VydmVyVXJsIiwiZ3Vlc3QiLCJ0aGVuIiwic3VjY2VzcyIsIl9yZXN0b3JlRnJvbUxvY2FsU3RvcmFnZSIsImlnbm9yZUd1ZXN0IiwiQm9vbGVhbiIsIl9yZWdpc3RlckFzR3Vlc3QiLCJlIiwiQWJvcnRMb2dpbkFuZFJlYnVpbGRTdG9yYWdlIiwiX2hhbmRsZUxvYWRTZXNzaW9uRmFpbHVyZSIsImdldFN0b3JlZFNlc3Npb25Pd25lciIsImhzVXJsIiwiZ2V0TG9jYWxTdG9yYWdlU2Vzc2lvblZhcnMiLCJnZXRTdG9yZWRTZXNzaW9uSXNHdWVzdCIsInNlc3NWYXJzIiwiaXNHdWVzdCIsImF0dGVtcHRUb2tlbkxvZ2luIiwicXVlcnlQYXJhbXMiLCJsb2dpblRva2VuIiwiUHJvbWlzZSIsInJlc29sdmUiLCJob21lc2VydmVyIiwiaWRlbnRpdHlTZXJ2ZXIiLCJ0b2tlbiIsImluaXRpYWxfZGV2aWNlX2Rpc3BsYXlfbmFtZSIsImNyZWRzIiwiX2NsZWFyU3RvcmFnZSIsIl9wZXJzaXN0Q3JlZGVudGlhbHNUb0xvY2FsU3RvcmFnZSIsImNhdGNoIiwiZXJyIiwiZXJyb3IiLCJkYXRhIiwiaGFuZGxlSW52YWxpZFN0b3JlRXJyb3IiLCJyZWFzb24iLCJNYXRyaXgiLCJJbnZhbGlkU3RvcmVFcnJvciIsIlRPR0dMRURfTEFaWV9MT0FESU5HIiwibGF6eUxvYWRFbmFibGVkIiwidmFsdWUiLCJMYXp5TG9hZGluZ1Jlc3luY0RpYWxvZyIsInNkayIsImdldENvbXBvbmVudCIsIk1vZGFsIiwiY3JlYXRlRGlhbG9nIiwib25GaW5pc2hlZCIsIkxhenlMb2FkaW5nRGlzYWJsZWREaWFsb2ciLCJob3N0Iiwid2luZG93IiwibG9jYXRpb24iLCJNYXRyaXhDbGllbnRQZWciLCJnZXQiLCJzdG9yZSIsImRlbGV0ZUFsbERhdGEiLCJQbGF0Zm9ybVBlZyIsInJlbG9hZCIsImlzVXJsIiwiY2xpZW50IiwiY3JlYXRlQ2xpZW50IiwiYmFzZVVybCIsInJlZ2lzdGVyR3Vlc3QiLCJib2R5IiwidXNlcl9pZCIsImRldmljZUlkIiwiZGV2aWNlX2lkIiwiYWNjZXNzX3Rva2VuIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsIlNlc3Npb25SZXN0b3JlRXJyb3JEaWFsb2ciLCJtb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJtZXNzYWdlIiwiZmluaXNoZWQiLCJzZXRMb2dnZWRJbiIsImNyZWRlbnRpYWxzIiwic3RvcE1hdHJpeENsaWVudCIsImh5ZHJhdGVTZXNzaW9uIiwib2xkVXNlcklkIiwiZ2V0VXNlcklkIiwib2xkRGV2aWNlSWQiLCJnZXREZXZpY2VJZCIsInJlbW92ZUl0ZW0iLCJfaXNMb2dnaW5nT3V0Iiwib3ZlcndyaXRlIiwiY2xlYXJTdG9yYWdlIiwic29mdExvZ291dCIsImlzU29mdExvZ291dCIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwicmVzdWx0cyIsIlN0b3JhZ2VNYW5hZ2VyIiwiY2hlY2tDb25zaXN0ZW5jeSIsImRhdGFJbkxvY2FsU3RvcmFnZSIsImNyeXB0b0luaXRlZCIsImRhdGFJbkNyeXB0b1N0b3JlIiwic2lnbk91dCIsIl9zaG93U3RvcmFnZUV2aWN0ZWREaWFsb2ciLCJBbmFseXRpY3MiLCJwYXNzd29yZCIsImNhY2hlZFBhc3N3b3JkIiwicmVwbGFjZVVzaW5nQ3JlZHMiLCJzdGFydE1hdHJpeENsaWVudCIsIlN0b3JhZ2VFdmljdGVkRGlhbG9nIiwiRXJyb3IiLCJzZXRJdGVtIiwiSlNPTiIsInN0cmluZ2lmeSIsImxvZ291dCIsIm9uTG9nZ2VkT3V0IiwiaXNMb2dnaW5nT3V0Iiwic3RhcnRTeW5jaW5nIiwiTm90aWZpZXIiLCJzdGFydCIsIlVzZXJBY3Rpdml0eSIsInNoYXJlZEluc3RhbmNlIiwiVHlwaW5nU3RvcmUiLCJyZXNldCIsIlRvYXN0U3RvcmUiLCJETVJvb21NYXAiLCJtYWtlU2hhcmVkIiwiSW50ZWdyYXRpb25NYW5hZ2VycyIsInN0YXJ0V2F0Y2hpbmciLCJBY3RpdmVXaWRnZXRTdG9yZSIsIk1qb2xuaXIiLCJFdmVudEluZGV4UGVnIiwiaW5pdCIsImFzc2lnbiIsIkRldmljZUxpc3RlbmVyIiwiU2V0dGluZ3NTdG9yZSIsImdldFZhbHVlIiwiUHJlc2VuY2UiLCJKaXRzaSIsImdldEluc3RhbmNlIiwidXBkYXRlIiwiZGlzYWJsZSIsImNsZWFyIiwic2Vzc2lvblN0b3JhZ2UiLCJjbGkiLCJkZWxldGVFdmVudEluZGV4IiwiY2xlYXJTdG9yZXMiLCJ1bnNldENsaWVudCIsInN0b3AiLCJzdG9wV2F0Y2hpbmciLCJzaGFyZWQiLCJzdG9wQ2xpZW50IiwicmVtb3ZlQWxsTGlzdGVuZXJzIiwidW5zZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUExQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0Q0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ08sZUFBZUEsV0FBZixDQUEyQkMsSUFBM0IsRUFBaUM7QUFDcEMsTUFBSTtBQUNBLFFBQUlDLFdBQVcsR0FBR0QsSUFBSSxDQUFDQyxXQUFMLElBQW9CLEtBQXRDO0FBQ0EsVUFBTUMsVUFBVSxHQUFHRixJQUFJLENBQUNFLFVBQXhCO0FBQ0EsVUFBTUMsVUFBVSxHQUFHSCxJQUFJLENBQUNHLFVBQXhCO0FBQ0EsVUFBTUMsbUJBQW1CLEdBQUdKLElBQUksQ0FBQ0ksbUJBQUwsSUFBNEIsRUFBeEQ7QUFDQSxVQUFNQyx3QkFBd0IsR0FBR0wsSUFBSSxDQUFDSyx3QkFBdEM7O0FBRUEsUUFBSUosV0FBVyxJQUFJLENBQUNDLFVBQXBCLEVBQWdDO0FBQzVCSSxNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSwyREFBYjtBQUNBTixNQUFBQSxXQUFXLEdBQUcsS0FBZDtBQUNIOztBQUVELFFBQUlBLFdBQVcsSUFDWEcsbUJBQW1CLENBQUNJLGFBRHBCLElBRUFKLG1CQUFtQixDQUFDSyxrQkFGeEIsRUFHSztBQUNESCxNQUFBQSxPQUFPLENBQUNJLEdBQVIsQ0FBWSxnQ0FBWjtBQUNBLGFBQU9DLGNBQWMsQ0FBQztBQUNsQkMsUUFBQUEsTUFBTSxFQUFFUixtQkFBbUIsQ0FBQ0ksYUFEVjtBQUVsQkssUUFBQUEsV0FBVyxFQUFFVCxtQkFBbUIsQ0FBQ0ssa0JBRmY7QUFHbEJLLFFBQUFBLGFBQWEsRUFBRVosVUFIRztBQUlsQmEsUUFBQUEsaUJBQWlCLEVBQUVaLFVBSkQ7QUFLbEJhLFFBQUFBLEtBQUssRUFBRTtBQUxXLE9BQUQsRUFNbEIsSUFOa0IsQ0FBZCxDQU1FQyxJQU5GLENBTU8sTUFBTSxJQU5iLENBQVA7QUFPSDs7QUFDRCxVQUFNQyxPQUFPLEdBQUcsTUFBTUMsd0JBQXdCLENBQUM7QUFDM0NDLE1BQUFBLFdBQVcsRUFBRUMsT0FBTyxDQUFDckIsSUFBSSxDQUFDb0IsV0FBTjtBQUR1QixLQUFELENBQTlDOztBQUdBLFFBQUlGLE9BQUosRUFBYTtBQUNULGFBQU8sSUFBUDtBQUNIOztBQUVELFFBQUlqQixXQUFKLEVBQWlCO0FBQ2IsYUFBT3FCLGdCQUFnQixDQUFDcEIsVUFBRCxFQUFhQyxVQUFiLEVBQXlCRSx3QkFBekIsQ0FBdkI7QUFDSCxLQWxDRCxDQW9DQTs7O0FBQ0EsV0FBTyxLQUFQO0FBQ0gsR0F0Q0QsQ0FzQ0UsT0FBT2tCLENBQVAsRUFBVTtBQUNSLFFBQUlBLENBQUMsWUFBWUMsMkJBQWpCLEVBQThDO0FBQzFDO0FBQ0E7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFDRCxXQUFPQyx5QkFBeUIsQ0FBQ0YsQ0FBRCxDQUFoQztBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7QUFNTyxTQUFTRyxxQkFBVCxHQUFpQztBQUNwQyxRQUFNO0FBQUNDLElBQUFBLEtBQUQ7QUFBUWYsSUFBQUEsTUFBUjtBQUFnQkMsSUFBQUE7QUFBaEIsTUFBK0JlLDBCQUEwQixFQUEvRDtBQUNBLFNBQU9ELEtBQUssSUFBSWYsTUFBVCxJQUFtQkMsV0FBbkIsR0FBaUNELE1BQWpDLEdBQTBDLElBQWpEO0FBQ0g7QUFFRDs7Ozs7O0FBSU8sU0FBU2lCLHVCQUFULEdBQW1DO0FBQ3RDLFFBQU1DLFFBQVEsR0FBR0YsMEJBQTBCLEVBQTNDO0FBQ0EsU0FBT0UsUUFBUSxDQUFDSCxLQUFULElBQWtCRyxRQUFRLENBQUNsQixNQUEzQixJQUFxQ2tCLFFBQVEsQ0FBQ2pCLFdBQTlDLEdBQTREaUIsUUFBUSxDQUFDQyxPQUFyRSxHQUErRSxJQUF0RjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OztBQVVPLFNBQVNDLGlCQUFULENBQTJCQyxXQUEzQixFQUF3QzVCLHdCQUF4QyxFQUFrRTtBQUNyRSxNQUFJLENBQUM0QixXQUFXLENBQUNDLFVBQWpCLEVBQTZCO0FBQ3pCLFdBQU9DLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQixLQUFoQixDQUFQO0FBQ0g7O0FBRUQsTUFBSSxDQUFDSCxXQUFXLENBQUNJLFVBQWpCLEVBQTZCO0FBQ3pCL0IsSUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEseURBQWI7QUFDQSxXQUFPNEIsT0FBTyxDQUFDQyxPQUFSLENBQWdCLEtBQWhCLENBQVA7QUFDSDs7QUFFRCxTQUFPLDZCQUNISCxXQUFXLENBQUNJLFVBRFQsRUFFSEosV0FBVyxDQUFDSyxjQUZULEVBR0gsZUFIRyxFQUdjO0FBQ2JDLElBQUFBLEtBQUssRUFBRU4sV0FBVyxDQUFDQyxVQUROO0FBRWJNLElBQUFBLDJCQUEyQixFQUFFbkM7QUFGaEIsR0FIZCxFQU9MWSxJQVBLLENBT0EsVUFBU3dCLEtBQVQsRUFBZ0I7QUFDbkJuQyxJQUFBQSxPQUFPLENBQUNJLEdBQVIsQ0FBWSxzQkFBWjtBQUNBLFdBQU9nQyxhQUFhLEdBQUd6QixJQUFoQixDQUFxQixNQUFNO0FBQzlCMEIsTUFBQUEsaUNBQWlDLENBQUNGLEtBQUQsQ0FBakM7O0FBQ0EsYUFBTyxJQUFQO0FBQ0gsS0FITSxDQUFQO0FBSUgsR0FiTSxFQWFKRyxLQWJJLENBYUdDLEdBQUQsSUFBUztBQUNkdkMsSUFBQUEsT0FBTyxDQUFDd0MsS0FBUixDQUFjLHdDQUF3Q0QsR0FBeEMsR0FBOEMsR0FBOUMsR0FDQUEsR0FBRyxDQUFDRSxJQURsQjtBQUVBLFdBQU8sS0FBUDtBQUNILEdBakJNLENBQVA7QUFrQkg7O0FBRU0sU0FBU0MsdUJBQVQsQ0FBaUN6QixDQUFqQyxFQUFvQztBQUN2QyxNQUFJQSxDQUFDLENBQUMwQixNQUFGLEtBQWFDLHFCQUFPQyxpQkFBUCxDQUF5QkMsb0JBQTFDLEVBQWdFO0FBQzVELFdBQU9qQixPQUFPLENBQUNDLE9BQVIsR0FBa0JuQixJQUFsQixDQUF1QixNQUFNO0FBQ2hDLFlBQU1vQyxlQUFlLEdBQUc5QixDQUFDLENBQUMrQixLQUExQjs7QUFDQSxVQUFJRCxlQUFKLEVBQXFCO0FBQ2pCLGNBQU1FLHVCQUF1QixHQUN6QkMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHVDQUFqQixDQURKO0FBRUEsZUFBTyxJQUFJdEIsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDNUJzQix5QkFBTUMsWUFBTixDQUFtQkosdUJBQW5CLEVBQTRDO0FBQ3hDSyxZQUFBQSxVQUFVLEVBQUV4QjtBQUQ0QixXQUE1QztBQUdILFNBSk0sQ0FBUDtBQUtILE9BUkQsTUFRTztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBTXlCLHlCQUF5QixHQUMzQkwsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHlDQUFqQixDQURKO0FBRUEsZUFBTyxJQUFJdEIsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDNUJzQix5QkFBTUMsWUFBTixDQUFtQkUseUJBQW5CLEVBQThDO0FBQzFDRCxZQUFBQSxVQUFVLEVBQUV4QixPQUQ4QjtBQUUxQzBCLFlBQUFBLElBQUksRUFBRUMsTUFBTSxDQUFDQyxRQUFQLENBQWdCRjtBQUZvQixXQUE5QztBQUlILFNBTE0sQ0FBUDtBQU1IO0FBQ0osS0F4Qk0sRUF3Qko3QyxJQXhCSSxDQXdCQyxNQUFNO0FBQ1YsYUFBT2dELGlDQUFnQkMsR0FBaEIsR0FBc0JDLEtBQXRCLENBQTRCQyxhQUE1QixFQUFQO0FBQ0gsS0ExQk0sRUEwQkpuRCxJQTFCSSxDQTBCQyxNQUFNO0FBQ1ZvRCwyQkFBWUgsR0FBWixHQUFrQkksTUFBbEI7QUFDSCxLQTVCTSxDQUFQO0FBNkJIO0FBQ0o7O0FBRUQsU0FBU2hELGdCQUFULENBQTBCSyxLQUExQixFQUFpQzRDLEtBQWpDLEVBQXdDbEUsd0JBQXhDLEVBQWtFO0FBQzlEQyxFQUFBQSxPQUFPLENBQUNJLEdBQVIsZ0NBQW9DaUIsS0FBcEMsR0FEOEQsQ0FHOUQ7O0FBQ0EsUUFBTTZDLE1BQU0sR0FBR3RCLHFCQUFPdUIsWUFBUCxDQUFvQjtBQUMvQkMsSUFBQUEsT0FBTyxFQUFFL0M7QUFEc0IsR0FBcEIsQ0FBZjs7QUFJQSxTQUFPNkMsTUFBTSxDQUFDRyxhQUFQLENBQXFCO0FBQ3hCQyxJQUFBQSxJQUFJLEVBQUU7QUFDRnBDLE1BQUFBLDJCQUEyQixFQUFFbkM7QUFEM0I7QUFEa0IsR0FBckIsRUFJSlksSUFKSSxDQUlFd0IsS0FBRCxJQUFXO0FBQ2ZuQyxJQUFBQSxPQUFPLENBQUNJLEdBQVIsZ0NBQW9DK0IsS0FBSyxDQUFDb0MsT0FBMUM7QUFDQSxXQUFPbEUsY0FBYyxDQUFDO0FBQ2xCQyxNQUFBQSxNQUFNLEVBQUU2QixLQUFLLENBQUNvQyxPQURJO0FBRWxCQyxNQUFBQSxRQUFRLEVBQUVyQyxLQUFLLENBQUNzQyxTQUZFO0FBR2xCbEUsTUFBQUEsV0FBVyxFQUFFNEIsS0FBSyxDQUFDdUMsWUFIRDtBQUlsQmxFLE1BQUFBLGFBQWEsRUFBRWEsS0FKRztBQUtsQlosTUFBQUEsaUJBQWlCLEVBQUV3RCxLQUxEO0FBTWxCdkQsTUFBQUEsS0FBSyxFQUFFO0FBTlcsS0FBRCxFQU9sQixJQVBrQixDQUFkLENBT0VDLElBUEYsQ0FPTyxNQUFNLElBUGIsQ0FBUDtBQVFILEdBZE0sRUFjSDRCLEdBQUQsSUFBUztBQUNSdkMsSUFBQUEsT0FBTyxDQUFDd0MsS0FBUixDQUFjLDZCQUFkLEVBQTZDRCxHQUE3QztBQUNBLFdBQU8sS0FBUDtBQUNILEdBakJNLENBQVA7QUFrQkg7QUFFRDs7Ozs7OztBQUtPLFNBQVNqQiwwQkFBVCxHQUFzQztBQUN6QyxRQUFNRCxLQUFLLEdBQUdzRCxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsV0FBckIsQ0FBZDtBQUNBLFFBQU1YLEtBQUssR0FBR1UsWUFBWSxDQUFDQyxPQUFiLENBQXFCLFdBQXJCLENBQWQ7QUFDQSxRQUFNckUsV0FBVyxHQUFHb0UsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGlCQUFyQixDQUFwQjtBQUNBLFFBQU10RSxNQUFNLEdBQUdxRSxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsWUFBckIsQ0FBZjtBQUNBLFFBQU1KLFFBQVEsR0FBR0csWUFBWSxDQUFDQyxPQUFiLENBQXFCLGNBQXJCLENBQWpCO0FBRUEsTUFBSW5ELE9BQUo7O0FBQ0EsTUFBSWtELFlBQVksQ0FBQ0MsT0FBYixDQUFxQixhQUFyQixNQUF3QyxJQUE1QyxFQUFrRDtBQUM5Q25ELElBQUFBLE9BQU8sR0FBR2tELFlBQVksQ0FBQ0MsT0FBYixDQUFxQixhQUFyQixNQUF3QyxNQUFsRDtBQUNILEdBRkQsTUFFTztBQUNIO0FBQ0FuRCxJQUFBQSxPQUFPLEdBQUdrRCxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsaUJBQXJCLE1BQTRDLE1BQXREO0FBQ0g7O0FBRUQsU0FBTztBQUFDdkQsSUFBQUEsS0FBRDtBQUFRNEMsSUFBQUEsS0FBUjtBQUFlMUQsSUFBQUEsV0FBZjtBQUE0QkQsSUFBQUEsTUFBNUI7QUFBb0NrRSxJQUFBQSxRQUFwQztBQUE4Qy9DLElBQUFBO0FBQTlDLEdBQVA7QUFDSCxDLENBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLGVBQWVaLHdCQUFmLENBQXdDbkIsSUFBeEMsRUFBOEM7QUFDMUMsUUFBTW9CLFdBQVcsR0FBR3BCLElBQUksQ0FBQ29CLFdBQXpCOztBQUVBLE1BQUksQ0FBQzZELFlBQUwsRUFBbUI7QUFDZixXQUFPLEtBQVA7QUFDSDs7QUFFRCxRQUFNO0FBQUN0RCxJQUFBQSxLQUFEO0FBQVE0QyxJQUFBQSxLQUFSO0FBQWUxRCxJQUFBQSxXQUFmO0FBQTRCRCxJQUFBQSxNQUE1QjtBQUFvQ2tFLElBQUFBLFFBQXBDO0FBQThDL0MsSUFBQUE7QUFBOUMsTUFBeURILDBCQUEwQixFQUF6Rjs7QUFFQSxNQUFJZixXQUFXLElBQUlELE1BQWYsSUFBeUJlLEtBQTdCLEVBQW9DO0FBQ2hDLFFBQUlQLFdBQVcsSUFBSVcsT0FBbkIsRUFBNEI7QUFDeEJ6QixNQUFBQSxPQUFPLENBQUNJLEdBQVIsQ0FBWSxvQ0FBb0NFLE1BQWhEO0FBQ0EsYUFBTyxLQUFQO0FBQ0g7O0FBRUROLElBQUFBLE9BQU8sQ0FBQ0ksR0FBUixpQ0FBcUNFLE1BQXJDO0FBQ0EsVUFBTUQsY0FBYyxDQUFDO0FBQ2pCQyxNQUFBQSxNQUFNLEVBQUVBLE1BRFM7QUFFakJrRSxNQUFBQSxRQUFRLEVBQUVBLFFBRk87QUFHakJqRSxNQUFBQSxXQUFXLEVBQUVBLFdBSEk7QUFJakJDLE1BQUFBLGFBQWEsRUFBRWEsS0FKRTtBQUtqQlosTUFBQUEsaUJBQWlCLEVBQUV3RCxLQUxGO0FBTWpCdkQsTUFBQUEsS0FBSyxFQUFFZTtBQU5VLEtBQUQsRUFPakIsS0FQaUIsQ0FBcEI7QUFRQSxXQUFPLElBQVA7QUFDSCxHQWhCRCxNQWdCTztBQUNIekIsSUFBQUEsT0FBTyxDQUFDSSxHQUFSLENBQVksNEJBQVo7QUFDQSxXQUFPLEtBQVA7QUFDSDtBQUNKOztBQUVELGVBQWVlLHlCQUFmLENBQXlDRixDQUF6QyxFQUE0QztBQUN4Q2pCLEVBQUFBLE9BQU8sQ0FBQ3dDLEtBQVIsQ0FBYyx3QkFBZCxFQUF3Q3ZCLENBQXhDO0FBRUEsUUFBTTRELHlCQUF5QixHQUN6QjNCLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix5Q0FBakIsQ0FETjs7QUFHQSxRQUFNMkIsS0FBSyxHQUFHMUIsZUFBTTJCLG1CQUFOLENBQTBCLHVCQUExQixFQUFtRCxFQUFuRCxFQUF1REYseUJBQXZELEVBQWtGO0FBQzVGckMsSUFBQUEsS0FBSyxFQUFFdkIsQ0FBQyxDQUFDK0Q7QUFEbUYsR0FBbEYsQ0FBZDs7QUFJQSxRQUFNLENBQUNwRSxPQUFELElBQVksTUFBTWtFLEtBQUssQ0FBQ0csUUFBOUI7O0FBQ0EsTUFBSXJFLE9BQUosRUFBYTtBQUNUO0FBQ0EsVUFBTXdCLGFBQWEsRUFBbkI7QUFDQSxXQUFPLEtBQVA7QUFDSCxHQWZ1QyxDQWlCeEM7OztBQUNBLFNBQU8zQyxXQUFXLEVBQWxCO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7O0FBYU8sU0FBU3lGLFdBQVQsQ0FBcUJDLFdBQXJCLEVBQWtDO0FBQ3JDQyxFQUFBQSxnQkFBZ0I7QUFDaEIsU0FBTy9FLGNBQWMsQ0FBQzhFLFdBQUQsRUFBYyxJQUFkLENBQXJCO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFlTyxTQUFTRSxjQUFULENBQXdCRixXQUF4QixFQUFxQztBQUN4QyxRQUFNRyxTQUFTLEdBQUczQixpQ0FBZ0JDLEdBQWhCLEdBQXNCMkIsU0FBdEIsRUFBbEI7O0FBQ0EsUUFBTUMsV0FBVyxHQUFHN0IsaUNBQWdCQyxHQUFoQixHQUFzQjZCLFdBQXRCLEVBQXBCOztBQUVBTCxFQUFBQSxnQkFBZ0IsR0FKd0IsQ0FJcEI7O0FBQ3BCVCxFQUFBQSxZQUFZLENBQUNlLFVBQWIsQ0FBd0IsZ0JBQXhCO0FBQ0FDLEVBQUFBLGFBQWEsR0FBRyxLQUFoQjtBQUVBLFFBQU1DLFNBQVMsR0FBR1QsV0FBVyxDQUFDN0UsTUFBWixLQUF1QmdGLFNBQXZCLElBQW9DSCxXQUFXLENBQUNYLFFBQVosS0FBeUJnQixXQUEvRTs7QUFDQSxNQUFJSSxTQUFKLEVBQWU7QUFDWDVGLElBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLG9FQUFiO0FBQ0g7O0FBRUQsU0FBT0ksY0FBYyxDQUFDOEUsV0FBRCxFQUFjUyxTQUFkLENBQXJCO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7QUFTQSxlQUFldkYsY0FBZixDQUE4QjhFLFdBQTlCLEVBQTJDVSxZQUEzQyxFQUF5RDtBQUNyRDdGLEVBQUFBLE9BQU8sQ0FBQ0ksR0FBUixDQUFZLG9CQUFaLEVBQWtDK0UsV0FBbEM7QUFDQUEsRUFBQUEsV0FBVyxDQUFDekUsS0FBWixHQUFvQkssT0FBTyxDQUFDb0UsV0FBVyxDQUFDekUsS0FBYixDQUEzQjtBQUVBLFFBQU1vRixVQUFVLEdBQUdDLFlBQVksRUFBL0I7QUFFQS9GLEVBQUFBLE9BQU8sQ0FBQ0ksR0FBUixDQUNJLHdCQUF3QitFLFdBQVcsQ0FBQzdFLE1BQXBDLEdBQ0EsYUFEQSxHQUNnQjZFLFdBQVcsQ0FBQ1gsUUFENUIsR0FFQSxVQUZBLEdBRWFXLFdBQVcsQ0FBQ3pFLEtBRnpCLEdBR0EsT0FIQSxHQUdVeUUsV0FBVyxDQUFDM0UsYUFIdEIsR0FJQSxlQUpBLEdBSWtCc0YsVUFMdEIsRUFOcUQsQ0FjckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0FFLHNCQUFJQyxRQUFKLENBQWE7QUFBQ0MsSUFBQUEsTUFBTSxFQUFFO0FBQVQsR0FBYixFQUF3QyxJQUF4Qzs7QUFFQSxNQUFJTCxZQUFKLEVBQWtCO0FBQ2QsVUFBTXpELGFBQWEsRUFBbkI7QUFDSDs7QUFFRCxRQUFNK0QsT0FBTyxHQUFHLE1BQU1DLGNBQWMsQ0FBQ0MsZ0JBQWYsRUFBdEIsQ0EzQnFELENBNEJyRDtBQUNBO0FBQ0E7O0FBQ0EsTUFBSUYsT0FBTyxDQUFDRyxrQkFBUixJQUE4QkgsT0FBTyxDQUFDSSxZQUF0QyxJQUFzRCxDQUFDSixPQUFPLENBQUNLLGlCQUFuRSxFQUFzRjtBQUNsRixVQUFNQyxPQUFPLEdBQUcsTUFBTUMseUJBQXlCLEVBQS9DOztBQUNBLFFBQUlELE9BQUosRUFBYTtBQUNULFlBQU1yRSxhQUFhLEVBQW5CLENBRFMsQ0FFVDtBQUNBOztBQUNBLFlBQU0sSUFBSWxCLDJCQUFKLENBQ0YsNkRBREUsQ0FBTjtBQUdIO0FBQ0o7O0FBRUR5RixxQkFBVXpCLFdBQVYsQ0FBc0JDLFdBQVcsQ0FBQ3pFLEtBQWxDLEVBQXlDeUUsV0FBVyxDQUFDM0UsYUFBckQ7O0FBRUEsTUFBSW1FLFlBQUosRUFBa0I7QUFDZCxRQUFJO0FBQ0F0QyxNQUFBQSxpQ0FBaUMsQ0FBQzhDLFdBQUQsQ0FBakMsQ0FEQSxDQUdBO0FBQ0E7OztBQUNBLFVBQUlBLFdBQVcsQ0FBQ3lCLFFBQWhCLEVBQTBCO0FBQ3RCO0FBQ0FaLDRCQUFJQyxRQUFKLENBQWE7QUFDVEMsVUFBQUEsTUFBTSxFQUFFLGlCQURDO0FBRVRXLFVBQUFBLGNBQWMsRUFBRTFCLFdBQVcsQ0FBQ3lCO0FBRm5CLFNBQWI7QUFJSDtBQUNKLEtBWkQsQ0FZRSxPQUFPM0YsQ0FBUCxFQUFVO0FBQ1JqQixNQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxtREFBYixFQUFrRWdCLENBQWxFO0FBQ0g7QUFDSixHQWhCRCxNQWdCTztBQUNIakIsSUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsb0RBQWI7QUFDSDs7QUFFRDBELG1DQUFnQm1ELGlCQUFoQixDQUFrQzNCLFdBQWxDOztBQUVBYSxzQkFBSUMsUUFBSixDQUFhO0FBQUVDLElBQUFBLE1BQU0sRUFBRTtBQUFWLEdBQWI7O0FBRUEsUUFBTWEsaUJBQWlCO0FBQUM7QUFBaUIsR0FBQ2pCLFVBQW5CLENBQXZCO0FBQ0EsU0FBT25DLGlDQUFnQkMsR0FBaEIsRUFBUDtBQUNIOztBQUVELFNBQVM4Qyx5QkFBVCxHQUFxQztBQUNqQyxRQUFNTSxvQkFBb0IsR0FBRzlELEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQ0FBakIsQ0FBN0I7QUFDQSxTQUFPLElBQUl0QixPQUFKLENBQVlDLE9BQU8sSUFBSTtBQUMxQnNCLG1CQUFNMkIsbUJBQU4sQ0FBMEIsaUJBQTFCLEVBQTZDLEVBQTdDLEVBQWlEaUMsb0JBQWpELEVBQXVFO0FBQ25FMUQsTUFBQUEsVUFBVSxFQUFFeEI7QUFEdUQsS0FBdkU7QUFHSCxHQUpNLENBQVA7QUFLSCxDLENBRUQ7QUFDQTs7O0FBQ0EsTUFBTVosMkJBQU4sU0FBMEMrRixLQUExQyxDQUFnRDs7QUFFaEQsU0FBUzVFLGlDQUFULENBQTJDOEMsV0FBM0MsRUFBd0Q7QUFDcERuRixFQUFBQSxPQUFPLENBQUNJLEdBQVIsQ0FBWSxXQUFaO0FBRUF1RSxFQUFBQSxZQUFZLENBQUN1QyxPQUFiLENBQXFCLFdBQXJCLEVBQWtDL0IsV0FBVyxDQUFDM0UsYUFBOUM7O0FBQ0EsTUFBSTJFLFdBQVcsQ0FBQzFFLGlCQUFoQixFQUFtQztBQUMvQmtFLElBQUFBLFlBQVksQ0FBQ3VDLE9BQWIsQ0FBcUIsV0FBckIsRUFBa0MvQixXQUFXLENBQUMxRSxpQkFBOUM7QUFDSDs7QUFDRGtFLEVBQUFBLFlBQVksQ0FBQ3VDLE9BQWIsQ0FBcUIsWUFBckIsRUFBbUMvQixXQUFXLENBQUM3RSxNQUEvQztBQUNBcUUsRUFBQUEsWUFBWSxDQUFDdUMsT0FBYixDQUFxQixpQkFBckIsRUFBd0MvQixXQUFXLENBQUM1RSxXQUFwRDtBQUNBb0UsRUFBQUEsWUFBWSxDQUFDdUMsT0FBYixDQUFxQixhQUFyQixFQUFvQ0MsSUFBSSxDQUFDQyxTQUFMLENBQWVqQyxXQUFXLENBQUN6RSxLQUEzQixDQUFwQyxFQVRvRCxDQVdwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLE1BQUl5RSxXQUFXLENBQUNYLFFBQWhCLEVBQTBCO0FBQ3RCRyxJQUFBQSxZQUFZLENBQUN1QyxPQUFiLENBQXFCLGNBQXJCLEVBQXFDL0IsV0FBVyxDQUFDWCxRQUFqRDtBQUNIOztBQUNEeEUsRUFBQUEsT0FBTyxDQUFDSSxHQUFSLGlDQUFzQytFLFdBQXRDO0FBQ0g7O0FBRUQsSUFBSVEsYUFBYSxHQUFHLEtBQXBCO0FBRUE7Ozs7QUFHTyxTQUFTMEIsTUFBVCxHQUFrQjtBQUNyQixNQUFJLENBQUMxRCxpQ0FBZ0JDLEdBQWhCLEVBQUwsRUFBNEI7O0FBRTVCLE1BQUlELGlDQUFnQkMsR0FBaEIsR0FBc0JuQyxPQUF0QixFQUFKLEVBQXFDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBNkYsSUFBQUEsV0FBVztBQUNYO0FBQ0g7O0FBRUQzQixFQUFBQSxhQUFhLEdBQUcsSUFBaEI7O0FBQ0FoQyxtQ0FBZ0JDLEdBQWhCLEdBQXNCeUQsTUFBdEIsR0FBK0IxRyxJQUEvQixDQUFvQzJHLFdBQXBDLEVBQ0svRSxHQUFELElBQVM7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBdkMsSUFBQUEsT0FBTyxDQUFDSSxHQUFSLENBQVksMERBQVo7QUFDQWtILElBQUFBLFdBQVc7QUFDZCxHQVhMO0FBYUg7O0FBRU0sU0FBU3hCLFVBQVQsR0FBc0I7QUFDekIsTUFBSSxDQUFDbkMsaUNBQWdCQyxHQUFoQixFQUFMLEVBQTRCLE9BREgsQ0FHekI7QUFDQTtBQUNBOztBQUNBZSxFQUFBQSxZQUFZLENBQUN1QyxPQUFiLENBQXFCLGdCQUFyQixFQUF1QyxNQUF2QyxFQU55QixDQVF6QjtBQUNBOztBQUNBbEgsRUFBQUEsT0FBTyxDQUFDSSxHQUFSLENBQVksdUJBQVo7QUFDQXVGLEVBQUFBLGFBQWEsR0FBRyxJQUFoQixDQVh5QixDQVdIO0FBQ3RCO0FBQ0E7QUFDQTs7QUFDQUssc0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxJQUFBQSxNQUFNLEVBQUU7QUFBVCxHQUFiLEVBZnlCLENBZXVCOzs7QUFDaERkLEVBQUFBLGdCQUFnQjtBQUFDO0FBQWdCLE9BQWpCLENBQWhCLENBaEJ5QixDQWtCekI7QUFDSDs7QUFFTSxTQUFTVyxZQUFULEdBQXdCO0FBQzNCLFNBQU9wQixZQUFZLENBQUNDLE9BQWIsQ0FBcUIsZ0JBQXJCLE1BQTJDLE1BQWxEO0FBQ0g7O0FBRU0sU0FBUzJDLFlBQVQsR0FBd0I7QUFDM0IsU0FBTzVCLGFBQVA7QUFDSDtBQUVEOzs7Ozs7OztBQU1BLGVBQWVvQixpQkFBZixDQUFpQ1MsWUFBWSxHQUFDLElBQTlDLEVBQW9EO0FBQ2hEeEgsRUFBQUEsT0FBTyxDQUFDSSxHQUFSLHFDQURnRCxDQUdoRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQTRGLHNCQUFJQyxRQUFKLENBQWE7QUFBQ0MsSUFBQUEsTUFBTSxFQUFFO0FBQVQsR0FBYixFQUE0QyxJQUE1Qzs7QUFFQXVCLG9CQUFTQyxLQUFUOztBQUNBQyx3QkFBYUMsY0FBYixHQUE4QkYsS0FBOUI7O0FBQ0FHLHVCQUFZRCxjQUFaLEdBQTZCRSxLQUE3QixHQVhnRCxDQVdWOzs7QUFDdENDLHNCQUFXSCxjQUFYLEdBQTRCRSxLQUE1Qjs7QUFDQUUscUJBQVVDLFVBQVYsR0FBdUJQLEtBQXZCOztBQUNBUSwyQ0FBb0JOLGNBQXBCLEdBQXFDTyxhQUFyQzs7QUFDQUMsNkJBQWtCVixLQUFsQixHQWZnRCxDQWlCaEQ7QUFDQTtBQUNBOzs7QUFDQVcsbUJBQVFULGNBQVIsR0FBeUJGLEtBQXpCOztBQUVBLE1BQUlGLFlBQUosRUFBa0I7QUFDZDtBQUNBO0FBQ0E7QUFDQSxVQUFNYyx1QkFBY0MsSUFBZCxFQUFOO0FBQ0EsVUFBTTVFLGlDQUFnQitELEtBQWhCLEVBQU47QUFDSCxHQU5ELE1BTU87QUFDSDFILElBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLHFEQUFiO0FBQ0EsVUFBTTBELGlDQUFnQjZFLE1BQWhCLEVBQU47QUFDSCxHQS9CK0MsQ0FpQ2hEOzs7QUFDQUMsMEJBQWViLGNBQWYsR0FBZ0NGLEtBQWhDLEdBbENnRCxDQW1DaEQ7QUFDQTs7O0FBQ0EsTUFBSSxDQUFDZ0IsdUJBQWNDLFFBQWQsQ0FBdUIsY0FBdkIsQ0FBTCxFQUE2QztBQUN6Q0Msc0JBQVNsQixLQUFUO0FBQ0gsR0F2QytDLENBeUNoRDs7O0FBQ0EsUUFBTW1CLGFBQU1DLFdBQU4sR0FBb0JDLE1BQXBCLEVBQU4sQ0ExQ2dELENBNENoRDtBQUNBOztBQUNBL0Msc0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxJQUFBQSxNQUFNLEVBQUU7QUFBVCxHQUFiOztBQUVBLE1BQUlILFlBQVksRUFBaEIsRUFBb0I7QUFDaEJELElBQUFBLFVBQVU7QUFDYjtBQUNKO0FBRUQ7Ozs7OztBQUlPLGVBQWV3QixXQUFmLEdBQTZCO0FBQ2hDM0IsRUFBQUEsYUFBYSxHQUFHLEtBQWhCLENBRGdDLENBRWhDO0FBQ0E7QUFDQTs7QUFDQUssc0JBQUlDLFFBQUosQ0FBYTtBQUFDQyxJQUFBQSxNQUFNLEVBQUU7QUFBVCxHQUFiLEVBQXdDLElBQXhDOztBQUNBZCxFQUFBQSxnQkFBZ0I7QUFDaEIsUUFBTWhELGFBQWEsRUFBbkI7QUFDSDtBQUVEOzs7OztBQUdBLGVBQWVBLGFBQWYsR0FBK0I7QUFDM0J1RSxxQkFBVXFDLE9BQVY7O0FBRUEsTUFBSXZGLE1BQU0sQ0FBQ2tCLFlBQVgsRUFBeUI7QUFDckJsQixJQUFBQSxNQUFNLENBQUNrQixZQUFQLENBQW9Cc0UsS0FBcEI7QUFDSDs7QUFFRCxNQUFJeEYsTUFBTSxDQUFDeUYsY0FBWCxFQUEyQjtBQUN2QnpGLElBQUFBLE1BQU0sQ0FBQ3lGLGNBQVAsQ0FBc0JELEtBQXRCO0FBQ0gsR0FUMEIsQ0FXM0I7OztBQUNBLFFBQU1FLEdBQUcsR0FBRyxpQ0FBbUI7QUFDM0I7QUFDQS9FLElBQUFBLE9BQU8sRUFBRTtBQUZrQixHQUFuQixDQUFaO0FBS0EsUUFBTWtFLHVCQUFjYyxnQkFBZCxFQUFOO0FBQ0EsUUFBTUQsR0FBRyxDQUFDRSxXQUFKLEVBQU47QUFDSDtBQUVEOzs7Ozs7O0FBS08sU0FBU2pFLGdCQUFULENBQTBCa0UsV0FBVyxHQUFDLElBQXRDLEVBQTRDO0FBQy9DN0Isb0JBQVM4QixJQUFUOztBQUNBNUIsd0JBQWFDLGNBQWIsR0FBOEIyQixJQUE5Qjs7QUFDQTFCLHVCQUFZRCxjQUFaLEdBQTZCRSxLQUE3Qjs7QUFDQWMsb0JBQVNXLElBQVQ7O0FBQ0FuQiw2QkFBa0JtQixJQUFsQjs7QUFDQXJCLDJDQUFvQk4sY0FBcEIsR0FBcUM0QixZQUFyQzs7QUFDQW5CLG1CQUFRVCxjQUFSLEdBQXlCMkIsSUFBekI7O0FBQ0FkLDBCQUFlYixjQUFmLEdBQWdDMkIsSUFBaEM7O0FBQ0EsTUFBSXZCLG1CQUFVeUIsTUFBVixFQUFKLEVBQXdCekIsbUJBQVV5QixNQUFWLEdBQW1CRixJQUFuQjs7QUFDeEJqQix5QkFBY2lCLElBQWQ7O0FBQ0EsUUFBTUosR0FBRyxHQUFHeEYsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLE1BQUl1RixHQUFKLEVBQVM7QUFDTEEsSUFBQUEsR0FBRyxDQUFDTyxVQUFKO0FBQ0FQLElBQUFBLEdBQUcsQ0FBQ1Esa0JBQUo7O0FBRUEsUUFBSUwsV0FBSixFQUFpQjtBQUNiM0YsdUNBQWdCaUcsS0FBaEI7O0FBQ0F0Qiw2QkFBY3NCLEtBQWQ7QUFDSDtBQUNKO0FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNSwgMjAxNiBPcGVuTWFya2V0IEx0ZFxyXG5Db3B5cmlnaHQgMjAxNyBWZWN0b3IgQ3JlYXRpb25zIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5Db3B5cmlnaHQgMjAxOSwgMjAyMCBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBNYXRyaXggZnJvbSAnbWF0cml4LWpzLXNkayc7XHJcblxyXG5pbXBvcnQge01hdHJpeENsaWVudFBlZ30gZnJvbSAnLi9NYXRyaXhDbGllbnRQZWcnO1xyXG5pbXBvcnQgRXZlbnRJbmRleFBlZyBmcm9tICcuL2luZGV4aW5nL0V2ZW50SW5kZXhQZWcnO1xyXG5pbXBvcnQgY3JlYXRlTWF0cml4Q2xpZW50IGZyb20gJy4vdXRpbHMvY3JlYXRlTWF0cml4Q2xpZW50JztcclxuaW1wb3J0IEFuYWx5dGljcyBmcm9tICcuL0FuYWx5dGljcyc7XHJcbmltcG9ydCBOb3RpZmllciBmcm9tICcuL05vdGlmaWVyJztcclxuaW1wb3J0IFVzZXJBY3Rpdml0eSBmcm9tICcuL1VzZXJBY3Rpdml0eSc7XHJcbmltcG9ydCBQcmVzZW5jZSBmcm9tICcuL1ByZXNlbmNlJztcclxuaW1wb3J0IGRpcyBmcm9tICcuL2Rpc3BhdGNoZXInO1xyXG5pbXBvcnQgRE1Sb29tTWFwIGZyb20gJy4vdXRpbHMvRE1Sb29tTWFwJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4vTW9kYWwnO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi9pbmRleCc7XHJcbmltcG9ydCBBY3RpdmVXaWRnZXRTdG9yZSBmcm9tICcuL3N0b3Jlcy9BY3RpdmVXaWRnZXRTdG9yZSc7XHJcbmltcG9ydCBQbGF0Zm9ybVBlZyBmcm9tIFwiLi9QbGF0Zm9ybVBlZ1wiO1xyXG5pbXBvcnQgeyBzZW5kTG9naW5SZXF1ZXN0IH0gZnJvbSBcIi4vTG9naW5cIjtcclxuaW1wb3J0ICogYXMgU3RvcmFnZU1hbmFnZXIgZnJvbSAnLi91dGlscy9TdG9yYWdlTWFuYWdlcic7XHJcbmltcG9ydCBTZXR0aW5nc1N0b3JlIGZyb20gXCIuL3NldHRpbmdzL1NldHRpbmdzU3RvcmVcIjtcclxuaW1wb3J0IFR5cGluZ1N0b3JlIGZyb20gXCIuL3N0b3Jlcy9UeXBpbmdTdG9yZVwiO1xyXG5pbXBvcnQgVG9hc3RTdG9yZSBmcm9tIFwiLi9zdG9yZXMvVG9hc3RTdG9yZVwiO1xyXG5pbXBvcnQge0ludGVncmF0aW9uTWFuYWdlcnN9IGZyb20gXCIuL2ludGVncmF0aW9ucy9JbnRlZ3JhdGlvbk1hbmFnZXJzXCI7XHJcbmltcG9ydCB7TWpvbG5pcn0gZnJvbSBcIi4vbWpvbG5pci9Nam9sbmlyXCI7XHJcbmltcG9ydCBEZXZpY2VMaXN0ZW5lciBmcm9tIFwiLi9EZXZpY2VMaXN0ZW5lclwiO1xyXG5pbXBvcnQge0ppdHNpfSBmcm9tIFwiLi93aWRnZXRzL0ppdHNpXCI7XHJcblxyXG4vKipcclxuICogQ2FsbGVkIGF0IHN0YXJ0dXAsIHRvIGF0dGVtcHQgdG8gYnVpbGQgYSBsb2dnZWQtaW4gTWF0cml4IHNlc3Npb24uIEl0IHRyaWVzXHJcbiAqIGEgbnVtYmVyIG9mIHRoaW5nczpcclxuICpcclxuICpcclxuICogMS4gaWYgd2UgaGF2ZSBhIGd1ZXN0IGFjY2VzcyB0b2tlbiBpbiB0aGUgZnJhZ21lbnQgcXVlcnkgcGFyYW1zLCBpdCB1c2VzXHJcbiAqICAgIHRoYXQuXHJcbiAqXHJcbiAqIDIuIGlmIGFuIGFjY2VzcyB0b2tlbiBpcyBzdG9yZWQgaW4gbG9jYWwgc3RvcmFnZSAoZnJvbSBhIHByZXZpb3VzIHNlc3Npb24pLFxyXG4gKiAgICBpdCB1c2VzIHRoYXQuXHJcbiAqXHJcbiAqIDMuIGl0IGF0dGVtcHRzIHRvIGF1dG8tcmVnaXN0ZXIgYXMgYSBndWVzdCB1c2VyLlxyXG4gKlxyXG4gKiBJZiBhbnkgb2Ygc3RlcHMgMS00IGFyZSBzdWNjZXNzZnVsLCBpdCB3aWxsIGNhbGwge19kb1NldExvZ2dlZElufSwgd2hpY2ggaW5cclxuICogdHVybiB3aWxsIHJhaXNlIG9uX2xvZ2dlZF9pbiBhbmQgd2lsbF9zdGFydF9jbGllbnQgZXZlbnRzLlxyXG4gKlxyXG4gKiBAcGFyYW0ge29iamVjdH0gb3B0c1xyXG4gKlxyXG4gKiBAcGFyYW0ge29iamVjdH0gb3B0cy5mcmFnbWVudFF1ZXJ5UGFyYW1zOiBzdHJpbmctPnN0cmluZyBtYXAgb2YgdGhlXHJcbiAqICAgICBxdWVyeS1wYXJhbWV0ZXJzIGV4dHJhY3RlZCBmcm9tIHRoZSAjLWZyYWdtZW50IG9mIHRoZSBzdGFydGluZyBVUkkuXHJcbiAqXHJcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gb3B0cy5lbmFibGVHdWVzdDogc2V0IHRvIHRydWUgdG8gZW5hYmxlIGd1ZXN0IGFjY2VzcyB0b2tlbnNcclxuICogICAgIGFuZCBhdXRvLWd1ZXN0IHJlZ2lzdHJhdGlvbnMuXHJcbiAqXHJcbiAqIEBwYXJhbXMge3N0cmluZ30gb3B0cy5ndWVzdEhzVXJsOiBob21lc2VydmVyIFVSTC4gT25seSB1c2VkIGlmIGVuYWJsZUd1ZXN0IGlzXHJcbiAqICAgICB0cnVlOyBkZWZpbmVzIHRoZSBIUyB0byByZWdpc3RlciBhZ2FpbnN0LlxyXG4gKlxyXG4gKiBAcGFyYW1zIHtzdHJpbmd9IG9wdHMuZ3Vlc3RJc1VybDogaG9tZXNlcnZlciBVUkwuIE9ubHkgdXNlZCBpZiBlbmFibGVHdWVzdCBpc1xyXG4gKiAgICAgdHJ1ZTsgZGVmaW5lcyB0aGUgSVMgdG8gdXNlLlxyXG4gKlxyXG4gKiBAcGFyYW1zIHtib29sfSBvcHRzLmlnbm9yZUd1ZXN0OiBJZiB0aGUgc3RvcmVkIHNlc3Npb24gaXMgYSBndWVzdCBhY2NvdW50LCBpZ25vcmVcclxuICogICAgIGl0IGFuZCBkb24ndCBsb2FkIGl0LlxyXG4gKlxyXG4gKiBAcmV0dXJucyB7UHJvbWlzZX0gYSBwcm9taXNlIHdoaWNoIHJlc29sdmVzIHdoZW4gdGhlIGFib3ZlIHByb2Nlc3MgY29tcGxldGVzLlxyXG4gKiAgICAgUmVzb2x2ZXMgdG8gYHRydWVgIGlmIHdlIGVuZGVkIHVwIHN0YXJ0aW5nIGEgc2Vzc2lvbiwgb3IgYGZhbHNlYCBpZiB3ZVxyXG4gKiAgICAgZmFpbGVkLlxyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGxvYWRTZXNzaW9uKG9wdHMpIHtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgbGV0IGVuYWJsZUd1ZXN0ID0gb3B0cy5lbmFibGVHdWVzdCB8fCBmYWxzZTtcclxuICAgICAgICBjb25zdCBndWVzdEhzVXJsID0gb3B0cy5ndWVzdEhzVXJsO1xyXG4gICAgICAgIGNvbnN0IGd1ZXN0SXNVcmwgPSBvcHRzLmd1ZXN0SXNVcmw7XHJcbiAgICAgICAgY29uc3QgZnJhZ21lbnRRdWVyeVBhcmFtcyA9IG9wdHMuZnJhZ21lbnRRdWVyeVBhcmFtcyB8fCB7fTtcclxuICAgICAgICBjb25zdCBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWUgPSBvcHRzLmRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZTtcclxuXHJcbiAgICAgICAgaWYgKGVuYWJsZUd1ZXN0ICYmICFndWVzdEhzVXJsKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkNhbm5vdCBlbmFibGUgZ3Vlc3QgYWNjZXNzOiBjYW4ndCBkZXRlcm1pbmUgSFMgVVJMIHRvIHVzZVwiKTtcclxuICAgICAgICAgICAgZW5hYmxlR3Vlc3QgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChlbmFibGVHdWVzdCAmJlxyXG4gICAgICAgICAgICBmcmFnbWVudFF1ZXJ5UGFyYW1zLmd1ZXN0X3VzZXJfaWQgJiZcclxuICAgICAgICAgICAgZnJhZ21lbnRRdWVyeVBhcmFtcy5ndWVzdF9hY2Nlc3NfdG9rZW5cclxuICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVc2luZyBndWVzdCBhY2Nlc3MgY3JlZGVudGlhbHNcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBfZG9TZXRMb2dnZWRJbih7XHJcbiAgICAgICAgICAgICAgICB1c2VySWQ6IGZyYWdtZW50UXVlcnlQYXJhbXMuZ3Vlc3RfdXNlcl9pZCxcclxuICAgICAgICAgICAgICAgIGFjY2Vzc1Rva2VuOiBmcmFnbWVudFF1ZXJ5UGFyYW1zLmd1ZXN0X2FjY2Vzc190b2tlbixcclxuICAgICAgICAgICAgICAgIGhvbWVzZXJ2ZXJVcmw6IGd1ZXN0SHNVcmwsXHJcbiAgICAgICAgICAgICAgICBpZGVudGl0eVNlcnZlclVybDogZ3Vlc3RJc1VybCxcclxuICAgICAgICAgICAgICAgIGd1ZXN0OiB0cnVlLFxyXG4gICAgICAgICAgICB9LCB0cnVlKS50aGVuKCgpID0+IHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBzdWNjZXNzID0gYXdhaXQgX3Jlc3RvcmVGcm9tTG9jYWxTdG9yYWdlKHtcclxuICAgICAgICAgICAgaWdub3JlR3Vlc3Q6IEJvb2xlYW4ob3B0cy5pZ25vcmVHdWVzdCksXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoZW5hYmxlR3Vlc3QpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF9yZWdpc3RlckFzR3Vlc3QoZ3Vlc3RIc1VybCwgZ3Vlc3RJc1VybCwgZGVmYXVsdERldmljZURpc3BsYXlOYW1lKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGZhbGwgYmFjayB0byB3ZWxjb21lIHNjcmVlblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICBpZiAoZSBpbnN0YW5jZW9mIEFib3J0TG9naW5BbmRSZWJ1aWxkU3RvcmFnZSkge1xyXG4gICAgICAgICAgICAvLyBJZiB3ZSdyZSBhYm9ydGluZyBsb2dpbiBiZWNhdXNlIG9mIGEgc3RvcmFnZSBpbmNvbnNpc3RlbmN5LCB3ZSBkb24ndFxyXG4gICAgICAgICAgICAvLyBuZWVkIHRvIHNob3cgdGhlIGdlbmVyYWwgZmFpbHVyZSBkaWFsb2cuIEluc3RlYWQsIGp1c3QgZ28gYmFjayB0byB3ZWxjb21lLlxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBfaGFuZGxlTG9hZFNlc3Npb25GYWlsdXJlKGUpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogR2V0cyB0aGUgdXNlciBJRCBvZiB0aGUgcGVyc2lzdGVkIHNlc3Npb24sIGlmIG9uZSBleGlzdHMuIFRoaXMgZG9lcyBub3QgdmFsaWRhdGVcclxuICogdGhhdCB0aGUgdXNlcidzIGNyZWRlbnRpYWxzIHN0aWxsIHdvcmssIGp1c3QgdGhhdCB0aGV5IGV4aXN0IGFuZCB0aGF0IGEgdXNlciBJRFxyXG4gKiBpcyBhc3NvY2lhdGVkIHdpdGggdGhlbS4gVGhlIHNlc3Npb24gaXMgbm90IGxvYWRlZC5cclxuICogQHJldHVybnMge1N0cmluZ30gVGhlIHBlcnNpc3RlZCBzZXNzaW9uJ3Mgb3duZXIsIGlmIGFuIG93bmVyIGV4aXN0cy4gTnVsbCBvdGhlcndpc2UuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gZ2V0U3RvcmVkU2Vzc2lvbk93bmVyKCkge1xyXG4gICAgY29uc3Qge2hzVXJsLCB1c2VySWQsIGFjY2Vzc1Rva2VufSA9IGdldExvY2FsU3RvcmFnZVNlc3Npb25WYXJzKCk7XHJcbiAgICByZXR1cm4gaHNVcmwgJiYgdXNlcklkICYmIGFjY2Vzc1Rva2VuID8gdXNlcklkIDogbnVsbDtcclxufVxyXG5cclxuLyoqXHJcbiAqIEByZXR1cm5zIHtib29sfSBUcnVlIGlmIHRoZSBzdG9yZWQgc2Vzc2lvbiBpcyBmb3IgYSBndWVzdCB1c2VyIG9yIGZhbHNlIGlmIGl0IGlzXHJcbiAqICAgICBmb3IgYSByZWFsIHVzZXIuIElmIHRoZXJlIGlzIG5vIHN0b3JlZCBzZXNzaW9uLCByZXR1cm4gbnVsbC5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRTdG9yZWRTZXNzaW9uSXNHdWVzdCgpIHtcclxuICAgIGNvbnN0IHNlc3NWYXJzID0gZ2V0TG9jYWxTdG9yYWdlU2Vzc2lvblZhcnMoKTtcclxuICAgIHJldHVybiBzZXNzVmFycy5oc1VybCAmJiBzZXNzVmFycy51c2VySWQgJiYgc2Vzc1ZhcnMuYWNjZXNzVG9rZW4gPyBzZXNzVmFycy5pc0d1ZXN0IDogbnVsbDtcclxufVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBxdWVyeVBhcmFtcyAgICBzdHJpbmctPnN0cmluZyBtYXAgb2YgdGhlXHJcbiAqICAgICBxdWVyeS1wYXJhbWV0ZXJzIGV4dHJhY3RlZCBmcm9tIHRoZSByZWFsIHF1ZXJ5LXN0cmluZyBvZiB0aGUgc3RhcnRpbmdcclxuICogICAgIFVSSS5cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IGRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZVxyXG4gKlxyXG4gKiBAcmV0dXJucyB7UHJvbWlzZX0gcHJvbWlzZSB3aGljaCByZXNvbHZlcyB0byB0cnVlIGlmIHdlIGNvbXBsZXRlZCB0aGUgdG9rZW5cclxuICogICAgbG9naW4sIGVsc2UgZmFsc2VcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBhdHRlbXB0VG9rZW5Mb2dpbihxdWVyeVBhcmFtcywgZGVmYXVsdERldmljZURpc3BsYXlOYW1lKSB7XHJcbiAgICBpZiAoIXF1ZXJ5UGFyYW1zLmxvZ2luVG9rZW4pIHtcclxuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXF1ZXJ5UGFyYW1zLmhvbWVzZXJ2ZXIpIHtcclxuICAgICAgICBjb25zb2xlLndhcm4oXCJDYW5ub3QgbG9nIGluIHdpdGggdG9rZW46IGNhbid0IGRldGVybWluZSBIUyBVUkwgdG8gdXNlXCIpO1xyXG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBzZW5kTG9naW5SZXF1ZXN0KFxyXG4gICAgICAgIHF1ZXJ5UGFyYW1zLmhvbWVzZXJ2ZXIsXHJcbiAgICAgICAgcXVlcnlQYXJhbXMuaWRlbnRpdHlTZXJ2ZXIsXHJcbiAgICAgICAgXCJtLmxvZ2luLnRva2VuXCIsIHtcclxuICAgICAgICAgICAgdG9rZW46IHF1ZXJ5UGFyYW1zLmxvZ2luVG9rZW4sXHJcbiAgICAgICAgICAgIGluaXRpYWxfZGV2aWNlX2Rpc3BsYXlfbmFtZTogZGVmYXVsdERldmljZURpc3BsYXlOYW1lLFxyXG4gICAgICAgIH0sXHJcbiAgICApLnRoZW4oZnVuY3Rpb24oY3JlZHMpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxvZ2dlZCBpbiB3aXRoIHRva2VuXCIpO1xyXG4gICAgICAgIHJldHVybiBfY2xlYXJTdG9yYWdlKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIF9wZXJzaXN0Q3JlZGVudGlhbHNUb0xvY2FsU3RvcmFnZShjcmVkcyk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSkuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gbG9nIGluIHdpdGggbG9naW4gdG9rZW46IFwiICsgZXJyICsgXCIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgZXJyLmRhdGEpO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlSW52YWxpZFN0b3JlRXJyb3IoZSkge1xyXG4gICAgaWYgKGUucmVhc29uID09PSBNYXRyaXguSW52YWxpZFN0b3JlRXJyb3IuVE9HR0xFRF9MQVpZX0xPQURJTkcpIHtcclxuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxhenlMb2FkRW5hYmxlZCA9IGUudmFsdWU7XHJcbiAgICAgICAgICAgIGlmIChsYXp5TG9hZEVuYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IExhenlMb2FkaW5nUmVzeW5jRGlhbG9nID1cclxuICAgICAgICAgICAgICAgICAgICBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZGlhbG9ncy5MYXp5TG9hZGluZ1Jlc3luY0RpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZURpYWxvZyhMYXp5TG9hZGluZ1Jlc3luY0RpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaGVkOiByZXNvbHZlLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzaG93IHdhcm5pbmcgYWJvdXQgc2ltdWx0YW5lb3VzIHVzZVxyXG4gICAgICAgICAgICAgICAgLy8gYmV0d2VlbiBMTC9ub24tTEwgdmVyc2lvbiBvbiBzYW1lIGhvc3QuXHJcbiAgICAgICAgICAgICAgICAvLyBhcyBkaXNhYmxpbmcgTEwgd2hlbiBwcmV2aW91c2x5IGVuYWJsZWRcclxuICAgICAgICAgICAgICAgIC8vIGlzIGEgc3Ryb25nIGluZGljYXRvciBvZiB0aGlzICgvZGV2ZWxvcCAmIC9hcHApXHJcbiAgICAgICAgICAgICAgICBjb25zdCBMYXp5TG9hZGluZ0Rpc2FibGVkRGlhbG9nID1cclxuICAgICAgICAgICAgICAgICAgICBzZGsuZ2V0Q29tcG9uZW50KFwidmlld3MuZGlhbG9ncy5MYXp5TG9hZGluZ0Rpc2FibGVkRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlRGlhbG9nKExhenlMb2FkaW5nRGlzYWJsZWREaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25GaW5pc2hlZDogcmVzb2x2ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaG9zdDogd2luZG93LmxvY2F0aW9uLmhvc3QsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpLnN0b3JlLmRlbGV0ZUFsbERhdGEoKTtcclxuICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgUGxhdGZvcm1QZWcuZ2V0KCkucmVsb2FkKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIF9yZWdpc3RlckFzR3Vlc3QoaHNVcmwsIGlzVXJsLCBkZWZhdWx0RGV2aWNlRGlzcGxheU5hbWUpIHtcclxuICAgIGNvbnNvbGUubG9nKGBEb2luZyBndWVzdCBsb2dpbiBvbiAke2hzVXJsfWApO1xyXG5cclxuICAgIC8vIGNyZWF0ZSBhIHRlbXBvcmFyeSBNYXRyaXhDbGllbnQgdG8gZG8gdGhlIGxvZ2luXHJcbiAgICBjb25zdCBjbGllbnQgPSBNYXRyaXguY3JlYXRlQ2xpZW50KHtcclxuICAgICAgICBiYXNlVXJsOiBoc1VybCxcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBjbGllbnQucmVnaXN0ZXJHdWVzdCh7XHJcbiAgICAgICAgYm9keToge1xyXG4gICAgICAgICAgICBpbml0aWFsX2RldmljZV9kaXNwbGF5X25hbWU6IGRlZmF1bHREZXZpY2VEaXNwbGF5TmFtZSxcclxuICAgICAgICB9LFxyXG4gICAgfSkudGhlbigoY3JlZHMpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhgUmVnaXN0ZXJlZCBhcyBndWVzdDogJHtjcmVkcy51c2VyX2lkfWApO1xyXG4gICAgICAgIHJldHVybiBfZG9TZXRMb2dnZWRJbih7XHJcbiAgICAgICAgICAgIHVzZXJJZDogY3JlZHMudXNlcl9pZCxcclxuICAgICAgICAgICAgZGV2aWNlSWQ6IGNyZWRzLmRldmljZV9pZCxcclxuICAgICAgICAgICAgYWNjZXNzVG9rZW46IGNyZWRzLmFjY2Vzc190b2tlbixcclxuICAgICAgICAgICAgaG9tZXNlcnZlclVybDogaHNVcmwsXHJcbiAgICAgICAgICAgIGlkZW50aXR5U2VydmVyVXJsOiBpc1VybCxcclxuICAgICAgICAgICAgZ3Vlc3Q6IHRydWUsXHJcbiAgICAgICAgfSwgdHJ1ZSkudGhlbigoKSA9PiB0cnVlKTtcclxuICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKFwiRmFpbGVkIHRvIHJlZ2lzdGVyIGFzIGd1ZXN0XCIsIGVycik7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXRyaWV2ZXMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIHN0b3JlZCBzZXNzaW9uIGluIGxvY2Fsc3RvcmFnZS4gVGhlIHNlc3Npb25cclxuICogbWF5IG5vdCBiZSB2YWxpZCwgYXMgaXQgaXMgbm90IHRlc3RlZCBmb3IgY29uc2lzdGVuY3kgaGVyZS5cclxuICogQHJldHVybnMge09iamVjdH0gSW5mb3JtYXRpb24gYWJvdXQgdGhlIHNlc3Npb24gLSBzZWUgaW1wbGVtZW50YXRpb24gZm9yIHZhcmlhYmxlcy5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRMb2NhbFN0b3JhZ2VTZXNzaW9uVmFycygpIHtcclxuICAgIGNvbnN0IGhzVXJsID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJteF9oc191cmxcIik7XHJcbiAgICBjb25zdCBpc1VybCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibXhfaXNfdXJsXCIpO1xyXG4gICAgY29uc3QgYWNjZXNzVG9rZW4gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIm14X2FjY2Vzc190b2tlblwiKTtcclxuICAgIGNvbnN0IHVzZXJJZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibXhfdXNlcl9pZFwiKTtcclxuICAgIGNvbnN0IGRldmljZUlkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJteF9kZXZpY2VfaWRcIik7XHJcblxyXG4gICAgbGV0IGlzR3Vlc3Q7XHJcbiAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJteF9pc19ndWVzdFwiKSAhPT0gbnVsbCkge1xyXG4gICAgICAgIGlzR3Vlc3QgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIm14X2lzX2d1ZXN0XCIpID09PSBcInRydWVcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gbGVnYWN5IGtleSBuYW1lXHJcbiAgICAgICAgaXNHdWVzdCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibWF0cml4LWlzLWd1ZXN0XCIpID09PSBcInRydWVcIjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4ge2hzVXJsLCBpc1VybCwgYWNjZXNzVG9rZW4sIHVzZXJJZCwgZGV2aWNlSWQsIGlzR3Vlc3R9O1xyXG59XHJcblxyXG4vLyByZXR1cm5zIGEgcHJvbWlzZSB3aGljaCByZXNvbHZlcyB0byB0cnVlIGlmIGEgc2Vzc2lvbiBpcyBmb3VuZCBpblxyXG4vLyBsb2NhbHN0b3JhZ2VcclxuLy9cclxuLy8gTi5CLiBMaWZlY3ljbGUuanMgc2hvdWxkIG5vdCBtYWludGFpbiBhbnkgZnVydGhlciBsb2NhbFN0b3JhZ2Ugc3RhdGUsIHdlXHJcbi8vICAgICAgYXJlIG1vdmluZyB0b3dhcmRzIHVzaW5nIFNlc3Npb25TdG9yZSB0byBrZWVwIHRyYWNrIG9mIHN0YXRlIHJlbGF0ZWRcclxuLy8gICAgICB0byB0aGUgY3VycmVudCBzZXNzaW9uICh3aGljaCBpcyB0eXBpY2FsbHkgYmFja2VkIGJ5IGxvY2FsU3RvcmFnZSkuXHJcbi8vXHJcbi8vICAgICAgVGhlIHBsYW4gaXMgdG8gZ3JhZHVhbGx5IG1vdmUgdGhlIGxvY2FsU3RvcmFnZSBhY2Nlc3MgZG9uZSBoZXJlIGludG9cclxuLy8gICAgICBTZXNzaW9uU3RvcmUgdG8gYXZvaWQgYnVncyB3aGVyZSB0aGUgdmlldyBiZWNvbWVzIG91dC1vZi1zeW5jIHdpdGhcclxuLy8gICAgICBsb2NhbFN0b3JhZ2UgKGUuZy4gaXNHdWVzdCBldGMuKVxyXG5hc3luYyBmdW5jdGlvbiBfcmVzdG9yZUZyb21Mb2NhbFN0b3JhZ2Uob3B0cykge1xyXG4gICAgY29uc3QgaWdub3JlR3Vlc3QgPSBvcHRzLmlnbm9yZUd1ZXN0O1xyXG5cclxuICAgIGlmICghbG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHtoc1VybCwgaXNVcmwsIGFjY2Vzc1Rva2VuLCB1c2VySWQsIGRldmljZUlkLCBpc0d1ZXN0fSA9IGdldExvY2FsU3RvcmFnZVNlc3Npb25WYXJzKCk7XHJcblxyXG4gICAgaWYgKGFjY2Vzc1Rva2VuICYmIHVzZXJJZCAmJiBoc1VybCkge1xyXG4gICAgICAgIGlmIChpZ25vcmVHdWVzdCAmJiBpc0d1ZXN0KSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiSWdub3Jpbmcgc3RvcmVkIGd1ZXN0IGFjY291bnQ6IFwiICsgdXNlcklkKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coYFJlc3RvcmluZyBzZXNzaW9uIGZvciAke3VzZXJJZH1gKTtcclxuICAgICAgICBhd2FpdCBfZG9TZXRMb2dnZWRJbih7XHJcbiAgICAgICAgICAgIHVzZXJJZDogdXNlcklkLFxyXG4gICAgICAgICAgICBkZXZpY2VJZDogZGV2aWNlSWQsXHJcbiAgICAgICAgICAgIGFjY2Vzc1Rva2VuOiBhY2Nlc3NUb2tlbixcclxuICAgICAgICAgICAgaG9tZXNlcnZlclVybDogaHNVcmwsXHJcbiAgICAgICAgICAgIGlkZW50aXR5U2VydmVyVXJsOiBpc1VybCxcclxuICAgICAgICAgICAgZ3Vlc3Q6IGlzR3Vlc3QsXHJcbiAgICAgICAgfSwgZmFsc2UpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIk5vIHByZXZpb3VzIHNlc3Npb24gZm91bmQuXCIpO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG5cclxuYXN5bmMgZnVuY3Rpb24gX2hhbmRsZUxvYWRTZXNzaW9uRmFpbHVyZShlKSB7XHJcbiAgICBjb25zb2xlLmVycm9yKFwiVW5hYmxlIHRvIGxvYWQgc2Vzc2lvblwiLCBlKTtcclxuXHJcbiAgICBjb25zdCBTZXNzaW9uUmVzdG9yZUVycm9yRGlhbG9nID1cclxuICAgICAgICAgIHNkay5nZXRDb21wb25lbnQoJ3ZpZXdzLmRpYWxvZ3MuU2Vzc2lvblJlc3RvcmVFcnJvckRpYWxvZycpO1xyXG5cclxuICAgIGNvbnN0IG1vZGFsID0gTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU2Vzc2lvbiBSZXN0b3JlIEVycm9yJywgJycsIFNlc3Npb25SZXN0b3JlRXJyb3JEaWFsb2csIHtcclxuICAgICAgICBlcnJvcjogZS5tZXNzYWdlLFxyXG4gICAgfSk7XHJcblxyXG4gICAgY29uc3QgW3N1Y2Nlc3NdID0gYXdhaXQgbW9kYWwuZmluaXNoZWQ7XHJcbiAgICBpZiAoc3VjY2Vzcykge1xyXG4gICAgICAgIC8vIHVzZXIgY2xpY2tlZCBjb250aW51ZS5cclxuICAgICAgICBhd2FpdCBfY2xlYXJTdG9yYWdlKCk7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHRyeSwgdHJ5IGFnYWluXHJcbiAgICByZXR1cm4gbG9hZFNlc3Npb24oKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRyYW5zaXRpb25zIHRvIGEgbG9nZ2VkLWluIHN0YXRlIHVzaW5nIHRoZSBnaXZlbiBjcmVkZW50aWFscy5cclxuICpcclxuICogU3RhcnRzIHRoZSBtYXRyaXggY2xpZW50IGFuZCBhbGwgb3RoZXIgcmVhY3Qtc2RrIHNlcnZpY2VzIHRoYXRcclxuICogbGlzdGVuIGZvciBldmVudHMgd2hpbGUgYSBzZXNzaW9uIGlzIGxvZ2dlZCBpbi5cclxuICpcclxuICogQWxzbyBzdG9wcyB0aGUgb2xkIE1hdHJpeENsaWVudCBhbmQgY2xlYXJzIG9sZCBjcmVkZW50aWFscy9ldGMgb3V0IG9mXHJcbiAqIHN0b3JhZ2UgYmVmb3JlIHN0YXJ0aW5nIHRoZSBuZXcgY2xpZW50LlxyXG4gKlxyXG4gKiBAcGFyYW0ge01hdHJpeENsaWVudENyZWRzfSBjcmVkZW50aWFscyBUaGUgY3JlZGVudGlhbHMgdG8gdXNlXHJcbiAqXHJcbiAqIEByZXR1cm5zIHtQcm9taXNlfSBwcm9taXNlIHdoaWNoIHJlc29sdmVzIHRvIHRoZSBuZXcgTWF0cml4Q2xpZW50IG9uY2UgaXQgaGFzIGJlZW4gc3RhcnRlZFxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIHNldExvZ2dlZEluKGNyZWRlbnRpYWxzKSB7XHJcbiAgICBzdG9wTWF0cml4Q2xpZW50KCk7XHJcbiAgICByZXR1cm4gX2RvU2V0TG9nZ2VkSW4oY3JlZGVudGlhbHMsIHRydWUpO1xyXG59XHJcblxyXG4vKipcclxuICogSHlkcmF0ZXMgYW4gZXhpc3Rpbmcgc2Vzc2lvbiBieSB1c2luZyB0aGUgY3JlZGVudGlhbHMgcHJvdmlkZWQuIFRoaXMgd2lsbFxyXG4gKiBub3QgY2xlYXIgYW55IGxvY2FsIHN0b3JhZ2UsIHVubGlrZSBzZXRMb2dnZWRJbigpLlxyXG4gKlxyXG4gKiBTdG9wcyB0aGUgZXhpc3RpbmcgTWF0cml4IGNsaWVudCAod2l0aG91dCBjbGVhcmluZyBpdHMgZGF0YSkgYW5kIHN0YXJ0cyBhXHJcbiAqIG5ldyBvbmUgaW4gaXRzIHBsYWNlLiBUaGlzIGFkZGl0aW9uYWxseSBzdGFydHMgYWxsIG90aGVyIHJlYWN0LXNkayBzZXJ2aWNlc1xyXG4gKiB3aGljaCB1c2UgdGhlIG5ldyBNYXRyaXggY2xpZW50LlxyXG4gKlxyXG4gKiBJZiB0aGUgY3JlZGVudGlhbHMgYmVsb25nIHRvIGEgZGlmZmVyZW50IHVzZXIgZnJvbSB0aGUgc2Vzc2lvbiBhbHJlYWR5IHN0b3JlZCxcclxuICogdGhlIG9sZCBzZXNzaW9uIHdpbGwgYmUgY2xlYXJlZCBhdXRvbWF0aWNhbGx5LlxyXG4gKlxyXG4gKiBAcGFyYW0ge01hdHJpeENsaWVudENyZWRzfSBjcmVkZW50aWFscyBUaGUgY3JlZGVudGlhbHMgdG8gdXNlXHJcbiAqXHJcbiAqIEByZXR1cm5zIHtQcm9taXNlfSBwcm9taXNlIHdoaWNoIHJlc29sdmVzIHRvIHRoZSBuZXcgTWF0cml4Q2xpZW50IG9uY2UgaXQgaGFzIGJlZW4gc3RhcnRlZFxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGh5ZHJhdGVTZXNzaW9uKGNyZWRlbnRpYWxzKSB7XHJcbiAgICBjb25zdCBvbGRVc2VySWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCk7XHJcbiAgICBjb25zdCBvbGREZXZpY2VJZCA9IE1hdHJpeENsaWVudFBlZy5nZXQoKS5nZXREZXZpY2VJZCgpO1xyXG5cclxuICAgIHN0b3BNYXRyaXhDbGllbnQoKTsgLy8gdW5zZXRzIE1hdHJpeENsaWVudFBlZy5nZXQoKVxyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJteF9zb2Z0X2xvZ291dFwiKTtcclxuICAgIF9pc0xvZ2dpbmdPdXQgPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdCBvdmVyd3JpdGUgPSBjcmVkZW50aWFscy51c2VySWQgIT09IG9sZFVzZXJJZCB8fCBjcmVkZW50aWFscy5kZXZpY2VJZCAhPT0gb2xkRGV2aWNlSWQ7XHJcbiAgICBpZiAob3ZlcndyaXRlKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKFwiQ2xlYXJpbmcgYWxsIGRhdGE6IE9sZCBzZXNzaW9uIGJlbG9uZ3MgdG8gYSBkaWZmZXJlbnQgdXNlci9zZXNzaW9uXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBfZG9TZXRMb2dnZWRJbihjcmVkZW50aWFscywgb3ZlcndyaXRlKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIGZpcmVzIG9uX2xvZ2dpbmdfaW4sIG9wdGlvbmFsbHkgY2xlYXJzIGxvY2Fsc3RvcmFnZSwgcGVyc2lzdHMgbmV3IGNyZWRlbnRpYWxzXHJcbiAqIHRvIGxvY2Fsc3RvcmFnZSwgc3RhcnRzIHRoZSBuZXcgY2xpZW50LlxyXG4gKlxyXG4gKiBAcGFyYW0ge01hdHJpeENsaWVudENyZWRzfSBjcmVkZW50aWFsc1xyXG4gKiBAcGFyYW0ge0Jvb2xlYW59IGNsZWFyU3RvcmFnZVxyXG4gKlxyXG4gKiBAcmV0dXJucyB7UHJvbWlzZX0gcHJvbWlzZSB3aGljaCByZXNvbHZlcyB0byB0aGUgbmV3IE1hdHJpeENsaWVudCBvbmNlIGl0IGhhcyBiZWVuIHN0YXJ0ZWRcclxuICovXHJcbmFzeW5jIGZ1bmN0aW9uIF9kb1NldExvZ2dlZEluKGNyZWRlbnRpYWxzLCBjbGVhclN0b3JhZ2UpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiLS0tLWNyZWRlbnRpYWxzLS0tXCIsIGNyZWRlbnRpYWxzKVxyXG4gICAgY3JlZGVudGlhbHMuZ3Vlc3QgPSBCb29sZWFuKGNyZWRlbnRpYWxzLmd1ZXN0KTtcclxuXHJcbiAgICBjb25zdCBzb2Z0TG9nb3V0ID0gaXNTb2Z0TG9nb3V0KCk7XHJcblxyXG4gICAgY29uc29sZS5sb2coXHJcbiAgICAgICAgXCJzZXRMb2dnZWRJbjogbXhpZDogXCIgKyBjcmVkZW50aWFscy51c2VySWQgK1xyXG4gICAgICAgIFwiIGRldmljZUlkOiBcIiArIGNyZWRlbnRpYWxzLmRldmljZUlkICtcclxuICAgICAgICBcIiBndWVzdDogXCIgKyBjcmVkZW50aWFscy5ndWVzdCArXHJcbiAgICAgICAgXCIgaHM6IFwiICsgY3JlZGVudGlhbHMuaG9tZXNlcnZlclVybCArXHJcbiAgICAgICAgXCIgc29mdExvZ291dDogXCIgKyBzb2Z0TG9nb3V0LFxyXG4gICAgKTtcclxuXHJcbiAgICAvLyBUaGlzIGlzIGRpc3BhdGNoZWQgdG8gaW5kaWNhdGUgdGhhdCB0aGUgdXNlciBpcyBzdGlsbCBpbiB0aGUgcHJvY2VzcyBvZiBsb2dnaW5nIGluXHJcbiAgICAvLyBiZWNhdXNlIGFzeW5jIGNvZGUgbWF5IHRha2Ugc29tZSB0aW1lIHRvIHJlc29sdmUsIGJyZWFraW5nIHRoZSBhc3N1bXB0aW9uIHRoYXRcclxuICAgIC8vIGBzZXRMb2dnZWRJbmAgdGFrZXMgYW4gXCJpbnN0YW50XCIgdG8gY29tcGxldGUsIGFuZCBkaXNwYXRjaCBgb25fbG9nZ2VkX2luYCBhIGZldyBtc1xyXG4gICAgLy8gbGF0ZXIgdGhhbiBNYXRyaXhDaGF0IG1pZ2h0IGFzc3VtZS5cclxuICAgIC8vXHJcbiAgICAvLyB3ZSBmaXJlIGl0ICpzeW5jaHJvbm91c2x5KiB0byBtYWtlIHN1cmUgaXQgZmlyZXMgYmVmb3JlIG9uX2xvZ2dlZF9pbi5cclxuICAgIC8vIChkaXMuZGlzcGF0Y2ggdXNlcyBgc2V0VGltZW91dGAsIHdoaWNoIGRvZXMgbm90IGd1YXJhbnRlZSBvcmRlcmluZy4pXHJcbiAgICBkaXMuZGlzcGF0Y2goe2FjdGlvbjogJ29uX2xvZ2dpbmdfaW4nfSwgdHJ1ZSk7XHJcblxyXG4gICAgaWYgKGNsZWFyU3RvcmFnZSkge1xyXG4gICAgICAgIGF3YWl0IF9jbGVhclN0b3JhZ2UoKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCByZXN1bHRzID0gYXdhaXQgU3RvcmFnZU1hbmFnZXIuY2hlY2tDb25zaXN0ZW5jeSgpO1xyXG4gICAgLy8gSWYgdGhlcmUncyBhbiBpbmNvbnNpc3RlbmN5IGJldHdlZW4gYWNjb3VudCBkYXRhIGluIGxvY2FsIHN0b3JhZ2UgYW5kIHRoZVxyXG4gICAgLy8gY3J5cHRvIHN0b3JlLCB3ZSdsbCBiZSBnZW5lcmFsbHkgY29uZnVzZWQgd2hlbiBoYW5kbGluZyBlbmNyeXB0ZWQgZGF0YS5cclxuICAgIC8vIFNob3cgYSBtb2RhbCByZWNvbW1lbmRpbmcgYSBmdWxsIHJlc2V0IG9mIHN0b3JhZ2UuXHJcbiAgICBpZiAocmVzdWx0cy5kYXRhSW5Mb2NhbFN0b3JhZ2UgJiYgcmVzdWx0cy5jcnlwdG9Jbml0ZWQgJiYgIXJlc3VsdHMuZGF0YUluQ3J5cHRvU3RvcmUpIHtcclxuICAgICAgICBjb25zdCBzaWduT3V0ID0gYXdhaXQgX3Nob3dTdG9yYWdlRXZpY3RlZERpYWxvZygpO1xyXG4gICAgICAgIGlmIChzaWduT3V0KSB7XHJcbiAgICAgICAgICAgIGF3YWl0IF9jbGVhclN0b3JhZ2UoKTtcclxuICAgICAgICAgICAgLy8gVGhpcyBlcnJvciBmZWVscyBhIGJpdCBjbHVua3ksIGJ1dCB3ZSB3YW50IHRvIG1ha2Ugc3VyZSB3ZSBkb24ndCBnbyBhbnlcclxuICAgICAgICAgICAgLy8gZnVydGhlciBhbmQgaW5zdGVhZCBoZWFkIGJhY2sgdG8gc2lnbiBpbi5cclxuICAgICAgICAgICAgdGhyb3cgbmV3IEFib3J0TG9naW5BbmRSZWJ1aWxkU3RvcmFnZShcclxuICAgICAgICAgICAgICAgIFwiQWJvcnRpbmcgbG9naW4gaW4gcHJvZ3Jlc3MgYmVjYXVzZSBvZiBzdG9yYWdlIGluY29uc2lzdGVuY3lcIixcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQW5hbHl0aWNzLnNldExvZ2dlZEluKGNyZWRlbnRpYWxzLmd1ZXN0LCBjcmVkZW50aWFscy5ob21lc2VydmVyVXJsKTtcclxuXHJcbiAgICBpZiAobG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgX3BlcnNpc3RDcmVkZW50aWFsc1RvTG9jYWxTdG9yYWdlKGNyZWRlbnRpYWxzKTtcclxuXHJcbiAgICAgICAgICAgIC8vIFRoZSB1c2VyIHJlZ2lzdGVyZWQgYXMgYSBQV0xVIChQYXNzV29yZC1MZXNzIFVzZXIpLCB0aGUgZ2VuZXJhdGVkIHBhc3N3b3JkXHJcbiAgICAgICAgICAgIC8vIGlzIGNhY2hlZCBoZXJlIHN1Y2ggdGhhdCB0aGUgdXNlciBjYW4gY2hhbmdlIGl0IGF0IGEgbGF0ZXIgdGltZS5cclxuICAgICAgICAgICAgaWYgKGNyZWRlbnRpYWxzLnBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBVcGRhdGUgU2Vzc2lvblN0b3JlXHJcbiAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ2NhY2hlZF9wYXNzd29yZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FjaGVkUGFzc3dvcmQ6IGNyZWRlbnRpYWxzLnBhc3N3b3JkLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkVycm9yIHVzaW5nIGxvY2FsIHN0b3JhZ2U6IGNhbid0IHBlcnNpc3Qgc2Vzc2lvbiFcIiwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLndhcm4oXCJObyBsb2NhbCBzdG9yYWdlIGF2YWlsYWJsZTogY2FuJ3QgcGVyc2lzdCBzZXNzaW9uIVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBNYXRyaXhDbGllbnRQZWcucmVwbGFjZVVzaW5nQ3JlZHMoY3JlZGVudGlhbHMpO1xyXG5cclxuICAgIGRpcy5kaXNwYXRjaCh7IGFjdGlvbjogJ29uX2xvZ2dlZF9pbicgfSk7XHJcblxyXG4gICAgYXdhaXQgc3RhcnRNYXRyaXhDbGllbnQoLypzdGFydFN5bmNpbmc9Ki8hc29mdExvZ291dCk7XHJcbiAgICByZXR1cm4gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBfc2hvd1N0b3JhZ2VFdmljdGVkRGlhbG9nKCkge1xyXG4gICAgY29uc3QgU3RvcmFnZUV2aWN0ZWREaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCd2aWV3cy5kaWFsb2dzLlN0b3JhZ2VFdmljdGVkRGlhbG9nJyk7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XHJcbiAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU3RvcmFnZSBldmljdGVkJywgJycsIFN0b3JhZ2VFdmljdGVkRGlhbG9nLCB7XHJcbiAgICAgICAgICAgIG9uRmluaXNoZWQ6IHJlc29sdmUsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuLy8gTm90ZTogQmFiZWwgNiByZXF1aXJlcyB0aGUgYHRyYW5zZm9ybS1idWlsdGluLWV4dGVuZGAgcGx1Z2luIGZvciB0aGlzIHRvIHNhdGlzZnlcclxuLy8gYGluc3RhbmNlb2ZgLiBCYWJlbCA3IHN1cHBvcnRzIHRoaXMgbmF0aXZlbHkgaW4gdGhlaXIgY2xhc3MgaGFuZGxpbmcuXHJcbmNsYXNzIEFib3J0TG9naW5BbmRSZWJ1aWxkU3RvcmFnZSBleHRlbmRzIEVycm9yIHsgfVxyXG5cclxuZnVuY3Rpb24gX3BlcnNpc3RDcmVkZW50aWFsc1RvTG9jYWxTdG9yYWdlKGNyZWRlbnRpYWxzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImkgYW0gaGVyZVwiKVxyXG5cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibXhfaHNfdXJsXCIsIGNyZWRlbnRpYWxzLmhvbWVzZXJ2ZXJVcmwpO1xyXG4gICAgaWYgKGNyZWRlbnRpYWxzLmlkZW50aXR5U2VydmVyVXJsKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJteF9pc191cmxcIiwgY3JlZGVudGlhbHMuaWRlbnRpdHlTZXJ2ZXJVcmwpO1xyXG4gICAgfVxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJteF91c2VyX2lkXCIsIGNyZWRlbnRpYWxzLnVzZXJJZCk7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm14X2FjY2Vzc190b2tlblwiLCBjcmVkZW50aWFscy5hY2Nlc3NUb2tlbik7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm14X2lzX2d1ZXN0XCIsIEpTT04uc3RyaW5naWZ5KGNyZWRlbnRpYWxzLmd1ZXN0KSk7XHJcblxyXG4gICAgLy8gaWYgd2UgZGlkbid0IGdldCBhIGRldmljZUlkIGZyb20gdGhlIGxvZ2luLCBsZWF2ZSBteF9kZXZpY2VfaWQgdW5zZXQsXHJcbiAgICAvLyByYXRoZXIgdGhhbiBzZXR0aW5nIGl0IHRvIFwidW5kZWZpbmVkXCIuXHJcbiAgICAvL1xyXG4gICAgLy8gKGluIHRoaXMgY2FzZSBNYXRyaXhDbGllbnQgZG9lc24ndCBib3RoZXIgd2l0aCB0aGUgY3J5cHRvIHN0dWZmXHJcbiAgICAvLyAtIHRoYXQncyBmaW5lIGZvciB1cykuXHJcbiAgICBpZiAoY3JlZGVudGlhbHMuZGV2aWNlSWQpIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm14X2RldmljZV9pZFwiLCBjcmVkZW50aWFscy5kZXZpY2VJZCk7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyhgU2Vzc2lvbiBwZXJzaXN0ZWQgZm9yICR7KGNyZWRlbnRpYWxzKX1gKTtcclxufVxyXG5cclxubGV0IF9pc0xvZ2dpbmdPdXQgPSBmYWxzZTtcclxuXHJcbi8qKlxyXG4gKiBMb2dzIHRoZSBjdXJyZW50IHNlc3Npb24gb3V0IGFuZCB0cmFuc2l0aW9ucyB0byB0aGUgbG9nZ2VkLW91dCBzdGF0ZVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGxvZ291dCgpIHtcclxuICAgIGlmICghTWF0cml4Q2xpZW50UGVnLmdldCgpKSByZXR1cm47XHJcblxyXG4gICAgaWYgKE1hdHJpeENsaWVudFBlZy5nZXQoKS5pc0d1ZXN0KCkpIHtcclxuICAgICAgICAvLyBsb2dvdXQgZG9lc24ndCB3b3JrIGZvciBndWVzdCBzZXNzaW9uc1xyXG4gICAgICAgIC8vIEFsc28gd2Ugc29tZXRpbWVzIHdhbnQgdG8gcmUtbG9nIGluIGEgZ3Vlc3Qgc2Vzc2lvblxyXG4gICAgICAgIC8vIGlmIHdlIGFib3J0IHRoZSBsb2dpblxyXG4gICAgICAgIG9uTG9nZ2VkT3V0KCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIF9pc0xvZ2dpbmdPdXQgPSB0cnVlO1xyXG4gICAgTWF0cml4Q2xpZW50UGVnLmdldCgpLmxvZ291dCgpLnRoZW4ob25Mb2dnZWRPdXQsXHJcbiAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAvLyBKdXN0IHRocm93aW5nIGFuIGVycm9yIGhlcmUgaXMgZ29pbmcgdG8gYmUgdmVyeSB1bmhlbHBmdWxcclxuICAgICAgICAgICAgLy8gaWYgeW91J3JlIHRyeWluZyB0byBsb2cgb3V0IGJlY2F1c2UgeW91ciBzZXJ2ZXIncyBkb3duIGFuZFxyXG4gICAgICAgICAgICAvLyB5b3Ugd2FudCB0byBsb2cgaW50byBhIGRpZmZlcmVudCBzZXJ2ZXIsIHNvIGp1c3QgZm9yZ2V0IHRoZVxyXG4gICAgICAgICAgICAvLyBhY2Nlc3MgdG9rZW4uIEl0J3MgYW5ub3lpbmcgdGhhdCB0aGlzIHdpbGwgbGVhdmUgdGhlIGFjY2Vzc1xyXG4gICAgICAgICAgICAvLyB0b2tlbiBzdGlsbCB2YWxpZCwgYnV0IHdlIHNob3VsZCBmaXggdGhpcyBieSBoYXZpbmcgYWNjZXNzXHJcbiAgICAgICAgICAgIC8vIHRva2VucyBleHBpcmUgKGFuZCBpZiB5b3UgcmVhbGx5IHRoaW5rIHlvdSd2ZSBiZWVuIGNvbXByb21pc2VkLFxyXG4gICAgICAgICAgICAvLyBjaGFuZ2UgeW91ciBwYXNzd29yZCkuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRmFpbGVkIHRvIGNhbGwgbG9nb3V0IEFQSTogdG9rZW4gd2lsbCBub3QgYmUgaW52YWxpZGF0ZWRcIik7XHJcbiAgICAgICAgICAgIG9uTG9nZ2VkT3V0KCk7XHJcbiAgICAgICAgfSxcclxuICAgICk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBzb2Z0TG9nb3V0KCkge1xyXG4gICAgaWYgKCFNYXRyaXhDbGllbnRQZWcuZ2V0KCkpIHJldHVybjtcclxuXHJcbiAgICAvLyBUcmFjayB0aGF0IHdlJ3ZlIGRldGVjdGVkIGFuZCB0cmFwcGVkIGEgc29mdCBsb2dvdXQuIFRoaXMgaGVscHMgcHJldmVudCBvdGhlclxyXG4gICAgLy8gcGFydHMgb2YgdGhlIGFwcCBmcm9tIHN0YXJ0aW5nIGlmIHRoZXJlJ3Mgbm8gcG9pbnQgKGllOiBkb24ndCBzeW5jIGlmIHdlJ3ZlXHJcbiAgICAvLyBiZWVuIHNvZnQgbG9nZ2VkIG91dCwgZGVzcGl0ZSBoYXZpbmcgY3JlZGVudGlhbHMgYW5kIGRhdGEgZm9yIGEgTWF0cml4Q2xpZW50KS5cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibXhfc29mdF9sb2dvdXRcIiwgXCJ0cnVlXCIpO1xyXG5cclxuICAgIC8vIERldiBub3RlOiBwbGVhc2Uga2VlcCB0aGlzIGxvZyBsaW5lIGFyb3VuZC4gSXQgY2FuIGJlIHVzZWZ1bCBmb3IgdHJhY2sgZG93blxyXG4gICAgLy8gcmFuZG9tIGNsaWVudHMgc3RvcHBpbmcgaW4gdGhlIG1pZGRsZSBvZiB0aGUgbG9ncy5cclxuICAgIGNvbnNvbGUubG9nKFwiU29mdCBsb2dvdXQgaW5pdGlhdGVkXCIpO1xyXG4gICAgX2lzTG9nZ2luZ091dCA9IHRydWU7IC8vIHRvIGF2b2lkIHJlcGVhdGVkIGZsYWdzXHJcbiAgICAvLyBFbnN1cmUgdGhhdCB3ZSBkaXNwYXRjaCBhIHZpZXcgY2hhbmdlICoqYmVmb3JlKiogc3RvcHBpbmcgdGhlIGNsaWVudCBzb1xyXG4gICAgLy8gc28gdGhhdCBSZWFjdCBjb21wb25lbnRzIHVubW91bnQgZmlyc3QuIFRoaXMgYXZvaWRzIFJlYWN0IHNvZnQgY3Jhc2hlc1xyXG4gICAgLy8gdGhhdCBjYW4gb2NjdXIgd2hlbiBjb21wb25lbnRzIHRyeSB0byB1c2UgYSBudWxsIGNsaWVudC5cclxuICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnb25fY2xpZW50X25vdF92aWFibGUnfSk7IC8vIGdlbmVyaWMgdmVyc2lvbiBvZiBvbl9sb2dnZWRfb3V0XHJcbiAgICBzdG9wTWF0cml4Q2xpZW50KC8qdW5zZXRDbGllbnQ9Ki9mYWxzZSk7XHJcblxyXG4gICAgLy8gRE8gTk9UIENBTEwgTE9HT1VULiBBIHNvZnQgbG9nb3V0IHByZXNlcnZlcyBkYXRhLCBsb2dvdXQgZG9lcyBub3QuXHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBpc1NvZnRMb2dvdXQoKSB7XHJcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJteF9zb2Z0X2xvZ291dFwiKSA9PT0gXCJ0cnVlXCI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBpc0xvZ2dpbmdPdXQoKSB7XHJcbiAgICByZXR1cm4gX2lzTG9nZ2luZ091dDtcclxufVxyXG5cclxuLyoqXHJcbiAqIFN0YXJ0cyB0aGUgbWF0cml4IGNsaWVudCBhbmQgYWxsIG90aGVyIHJlYWN0LXNkayBzZXJ2aWNlcyB0aGF0XHJcbiAqIGxpc3RlbiBmb3IgZXZlbnRzIHdoaWxlIGEgc2Vzc2lvbiBpcyBsb2dnZWQgaW4uXHJcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gc3RhcnRTeW5jaW5nIFRydWUgKGRlZmF1bHQpIHRvIGFjdHVhbGx5IHN0YXJ0XHJcbiAqIHN5bmNpbmcgdGhlIGNsaWVudC5cclxuICovXHJcbmFzeW5jIGZ1bmN0aW9uIHN0YXJ0TWF0cml4Q2xpZW50KHN0YXJ0U3luY2luZz10cnVlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhgTGlmZWN5Y2xlOiBTdGFydGluZyBNYXRyaXhDbGllbnRgKTtcclxuXHJcbiAgICAvLyBkaXNwYXRjaCB0aGlzIGJlZm9yZSBzdGFydGluZyB0aGUgbWF0cml4IGNsaWVudDogaXQncyB1c2VkXHJcbiAgICAvLyB0byBhZGQgbGlzdGVuZXJzIGZvciB0aGUgJ3N5bmMnIGV2ZW50IHNvIG90aGVyd2lzZSB3ZSdkIGhhdmVcclxuICAgIC8vIGEgcmFjZSBjb25kaXRpb24gKGFuZCB3ZSBuZWVkIHRvIGRpc3BhdGNoIHN5bmNocm9ub3VzbHkgZm9yIHRoaXNcclxuICAgIC8vIHRvIHdvcmspLlxyXG4gICAgZGlzLmRpc3BhdGNoKHthY3Rpb246ICd3aWxsX3N0YXJ0X2NsaWVudCd9LCB0cnVlKTtcclxuXHJcbiAgICBOb3RpZmllci5zdGFydCgpO1xyXG4gICAgVXNlckFjdGl2aXR5LnNoYXJlZEluc3RhbmNlKCkuc3RhcnQoKTtcclxuICAgIFR5cGluZ1N0b3JlLnNoYXJlZEluc3RhbmNlKCkucmVzZXQoKTsgLy8ganVzdCBpbiBjYXNlXHJcbiAgICBUb2FzdFN0b3JlLnNoYXJlZEluc3RhbmNlKCkucmVzZXQoKTtcclxuICAgIERNUm9vbU1hcC5tYWtlU2hhcmVkKCkuc3RhcnQoKTtcclxuICAgIEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5zdGFydFdhdGNoaW5nKCk7XHJcbiAgICBBY3RpdmVXaWRnZXRTdG9yZS5zdGFydCgpO1xyXG5cclxuICAgIC8vIFN0YXJ0IE1qb2xuaXIgZXZlbiB0aG91Z2ggd2UgaGF2ZW4ndCBjaGVja2VkIHRoZSBmZWF0dXJlIGZsYWcgeWV0LiBTdGFydGluZ1xyXG4gICAgLy8gdGhlIHRoaW5nIGp1c3Qgd2FzdGVzIENQVSBjeWNsZXMsIGJ1dCBzaG91bGQgcmVzdWx0IGluIG5vIGFjdHVhbCBmdW5jdGlvbmFsaXR5XHJcbiAgICAvLyBiZWluZyBleHBvc2VkIHRvIHRoZSB1c2VyLlxyXG4gICAgTWpvbG5pci5zaGFyZWRJbnN0YW5jZSgpLnN0YXJ0KCk7XHJcblxyXG4gICAgaWYgKHN0YXJ0U3luY2luZykge1xyXG4gICAgICAgIC8vIFRoZSBjbGllbnQgbWlnaHQgd2FudCB0byBwb3B1bGF0ZSBzb21lIHZpZXdzIHdpdGggZXZlbnRzIGZyb20gdGhlXHJcbiAgICAgICAgLy8gaW5kZXggKGUuZy4gdGhlIEZpbGVQYW5lbCksIHRoZXJlZm9yZSBpbml0aWFsaXplIHRoZSBldmVudCBpbmRleFxyXG4gICAgICAgIC8vIGJlZm9yZSB0aGUgY2xpZW50LlxyXG4gICAgICAgIGF3YWl0IEV2ZW50SW5kZXhQZWcuaW5pdCgpO1xyXG4gICAgICAgIGF3YWl0IE1hdHJpeENsaWVudFBlZy5zdGFydCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLndhcm4oXCJDYWxsZXIgcmVxdWVzdGVkIG9ubHkgYXV4aWxpYXJ5IHNlcnZpY2VzIGJlIHN0YXJ0ZWRcIik7XHJcbiAgICAgICAgYXdhaXQgTWF0cml4Q2xpZW50UGVnLmFzc2lnbigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoaXMgbmVlZHMgdG8gYmUgc3RhcnRlZCBhZnRlciBjcnlwdG8gaXMgc2V0IHVwXHJcbiAgICBEZXZpY2VMaXN0ZW5lci5zaGFyZWRJbnN0YW5jZSgpLnN0YXJ0KCk7XHJcbiAgICAvLyBTaW1pbGFybHksIGRvbid0IHN0YXJ0IHNlbmRpbmcgcHJlc2VuY2UgdXBkYXRlcyB1bnRpbCB3ZSd2ZSBzdGFydGVkXHJcbiAgICAvLyB0aGUgY2xpZW50XHJcbiAgICBpZiAoIVNldHRpbmdzU3RvcmUuZ2V0VmFsdWUoXCJsb3dCYW5kd2lkdGhcIikpIHtcclxuICAgICAgICBQcmVzZW5jZS5zdGFydCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5vdyB0aGF0IHdlIGhhdmUgYSBNYXRyaXhDbGllbnRQZWcsIHVwZGF0ZSB0aGUgSml0c2kgaW5mb1xyXG4gICAgYXdhaXQgSml0c2kuZ2V0SW5zdGFuY2UoKS51cGRhdGUoKTtcclxuXHJcbiAgICAvLyBkaXNwYXRjaCB0aGF0IHdlIGZpbmlzaGVkIHN0YXJ0aW5nIHVwIHRvIHdpcmUgdXAgYW55IG90aGVyIGJpdHNcclxuICAgIC8vIG9mIHRoZSBtYXRyaXggY2xpZW50IHRoYXQgY2Fubm90IGJlIHNldCBwcmlvciB0byBzdGFydGluZyB1cC5cclxuICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnY2xpZW50X3N0YXJ0ZWQnfSk7XHJcblxyXG4gICAgaWYgKGlzU29mdExvZ291dCgpKSB7XHJcbiAgICAgICAgc29mdExvZ291dCgpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKlxyXG4gKiBTdG9wcyBhIHJ1bm5pbmcgY2xpZW50IGFuZCBhbGwgcmVsYXRlZCBzZXJ2aWNlcywgYW5kIGNsZWFycyBwZXJzaXN0ZW50XHJcbiAqIHN0b3JhZ2UuIFVzZWQgYWZ0ZXIgYSBzZXNzaW9uIGhhcyBiZWVuIGxvZ2dlZCBvdXQuXHJcbiAqL1xyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gb25Mb2dnZWRPdXQoKSB7XHJcbiAgICBfaXNMb2dnaW5nT3V0ID0gZmFsc2U7XHJcbiAgICAvLyBFbnN1cmUgdGhhdCB3ZSBkaXNwYXRjaCBhIHZpZXcgY2hhbmdlICoqYmVmb3JlKiogc3RvcHBpbmcgdGhlIGNsaWVudCBzb1xyXG4gICAgLy8gc28gdGhhdCBSZWFjdCBjb21wb25lbnRzIHVubW91bnQgZmlyc3QuIFRoaXMgYXZvaWRzIFJlYWN0IHNvZnQgY3Jhc2hlc1xyXG4gICAgLy8gdGhhdCBjYW4gb2NjdXIgd2hlbiBjb21wb25lbnRzIHRyeSB0byB1c2UgYSBudWxsIGNsaWVudC5cclxuICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAnb25fbG9nZ2VkX291dCd9LCB0cnVlKTtcclxuICAgIHN0b3BNYXRyaXhDbGllbnQoKTtcclxuICAgIGF3YWl0IF9jbGVhclN0b3JhZ2UoKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEByZXR1cm5zIHtQcm9taXNlfSBwcm9taXNlIHdoaWNoIHJlc29sdmVzIG9uY2UgdGhlIHN0b3JlcyBoYXZlIGJlZW4gY2xlYXJlZFxyXG4gKi9cclxuYXN5bmMgZnVuY3Rpb24gX2NsZWFyU3RvcmFnZSgpIHtcclxuICAgIEFuYWx5dGljcy5kaXNhYmxlKCk7XHJcblxyXG4gICAgaWYgKHdpbmRvdy5sb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLmNsZWFyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHdpbmRvdy5zZXNzaW9uU3RvcmFnZSkge1xyXG4gICAgICAgIHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNyZWF0ZSBhIHRlbXBvcmFyeSBjbGllbnQgdG8gY2xlYXIgb3V0IHRoZSBwZXJzaXN0ZW50IHN0b3Jlcy5cclxuICAgIGNvbnN0IGNsaSA9IGNyZWF0ZU1hdHJpeENsaWVudCh7XHJcbiAgICAgICAgLy8gd2UnbGwgbmV2ZXIgbWFrZSBhbnkgcmVxdWVzdHMsIHNvIGNhbiBwYXNzIGEgYm9ndXMgSFMgVVJMXHJcbiAgICAgICAgYmFzZVVybDogXCJcIixcclxuICAgIH0pO1xyXG5cclxuICAgIGF3YWl0IEV2ZW50SW5kZXhQZWcuZGVsZXRlRXZlbnRJbmRleCgpO1xyXG4gICAgYXdhaXQgY2xpLmNsZWFyU3RvcmVzKCk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBTdG9wIGFsbCB0aGUgYmFja2dyb3VuZCBwcm9jZXNzZXMgcmVsYXRlZCB0byB0aGUgY3VycmVudCBjbGllbnQuXHJcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gdW5zZXRDbGllbnQgVHJ1ZSAoZGVmYXVsdCkgdG8gYWJhbmRvbiB0aGUgY2xpZW50XHJcbiAqIG9uIE1hdHJpeENsaWVudFBlZyBhZnRlciBzdG9wcGluZy5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBzdG9wTWF0cml4Q2xpZW50KHVuc2V0Q2xpZW50PXRydWUpIHtcclxuICAgIE5vdGlmaWVyLnN0b3AoKTtcclxuICAgIFVzZXJBY3Rpdml0eS5zaGFyZWRJbnN0YW5jZSgpLnN0b3AoKTtcclxuICAgIFR5cGluZ1N0b3JlLnNoYXJlZEluc3RhbmNlKCkucmVzZXQoKTtcclxuICAgIFByZXNlbmNlLnN0b3AoKTtcclxuICAgIEFjdGl2ZVdpZGdldFN0b3JlLnN0b3AoKTtcclxuICAgIEludGVncmF0aW9uTWFuYWdlcnMuc2hhcmVkSW5zdGFuY2UoKS5zdG9wV2F0Y2hpbmcoKTtcclxuICAgIE1qb2xuaXIuc2hhcmVkSW5zdGFuY2UoKS5zdG9wKCk7XHJcbiAgICBEZXZpY2VMaXN0ZW5lci5zaGFyZWRJbnN0YW5jZSgpLnN0b3AoKTtcclxuICAgIGlmIChETVJvb21NYXAuc2hhcmVkKCkpIERNUm9vbU1hcC5zaGFyZWQoKS5zdG9wKCk7XHJcbiAgICBFdmVudEluZGV4UGVnLnN0b3AoKTtcclxuICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgIGlmIChjbGkpIHtcclxuICAgICAgICBjbGkuc3RvcENsaWVudCgpO1xyXG4gICAgICAgIGNsaS5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxuXHJcbiAgICAgICAgaWYgKHVuc2V0Q2xpZW50KSB7XHJcbiAgICAgICAgICAgIE1hdHJpeENsaWVudFBlZy51bnNldCgpO1xyXG4gICAgICAgICAgICBFdmVudEluZGV4UGVnLnVuc2V0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==