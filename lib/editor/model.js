"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _diff = require("./diff");

var _position = _interopRequireDefault(require("./position"));

var _range = _interopRequireDefault(require("./range"));

/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * @callback ModelCallback
 * @param {DocumentPosition?} caretPosition the position where the caret should be position
 * @param {string?} inputType the inputType of the DOM input event
 * @param {object?} diff an object with `removed` and `added` strings
 */

/**
* @callback TransformCallback
* @param {DocumentPosition?} caretPosition the position where the caret should be position
* @param {string?} inputType the inputType of the DOM input event
* @param {object?} diff an object with `removed` and `added` strings
* @return {Number?} addedLen how many characters were added/removed (-) before the caret during the transformation step.
*    This is used to adjust the caret position.
*/

/**
 * @callback ManualTransformCallback
 * @return the caret position
 */
class EditorModel {
  constructor(parts, partCreator, updateCallback = null) {
    (0, _defineProperty2.default)(this, "_onAutoComplete", ({
      replaceParts,
      close
    }) => {
      let pos;

      if (replaceParts) {
        this._parts.splice(this._autoCompletePartIdx, this._autoCompletePartCount, ...replaceParts);

        this._autoCompletePartCount = replaceParts.length;
        const lastPart = replaceParts[replaceParts.length - 1];
        const lastPartIndex = this._autoCompletePartIdx + replaceParts.length - 1;
        pos = new _position.default(lastPartIndex, lastPart.text.length);
      }

      if (close) {
        this._autoComplete = null;
        this._autoCompletePartIdx = null;
        this._autoCompletePartCount = 0;
      } // rerender even if editor contents didn't change
      // to make sure the MessageEditor checks
      // model.autoComplete being empty and closes it


      this._updateCallback(pos);
    });
    this._parts = parts;
    this._partCreator = partCreator;
    this._activePartIdx = null;
    this._autoComplete = null;
    this._autoCompletePartIdx = null;
    this._autoCompletePartCount = 0;
    this._transformCallback = null;
    this.setUpdateCallback(updateCallback);
  }
  /**
   * Set a callback for the transformation step.
   * While processing an update, right before calling the update callback,
   * a transform callback can be called, which serves to do modifications
   * on the model that can span multiple parts. Also see `startRange()`.
   * @param {TransformCallback} transformCallback
   */


  setTransformCallback(transformCallback) {
    this._transformCallback = transformCallback;
  }
  /**
   * Set a callback for rerendering the model after it has been updated.
   * @param {ModelCallback} updateCallback
   */


  setUpdateCallback(updateCallback) {
    this._updateCallback = updateCallback;
  }

  get partCreator() {
    return this._partCreator;
  }

  get isEmpty() {
    return this._parts.reduce((len, part) => len + part.text.length, 0) === 0;
  }

  clone() {
    return new EditorModel(this._parts, this._partCreator, this._updateCallback);
  }

  _insertPart(index, part) {
    this._parts.splice(index, 0, part);

    if (this._activePartIdx >= index) {
      ++this._activePartIdx;
    }

    if (this._autoCompletePartIdx >= index) {
      ++this._autoCompletePartIdx;
    }
  }

  _removePart(index) {
    this._parts.splice(index, 1);

    if (index === this._activePartIdx) {
      this._activePartIdx = null;
    } else if (this._activePartIdx > index) {
      --this._activePartIdx;
    }

    if (index === this._autoCompletePartIdx) {
      this._autoCompletePartIdx = null;
    } else if (this._autoCompletePartIdx > index) {
      --this._autoCompletePartIdx;
    }
  }

  _replacePart(index, part) {
    this._parts.splice(index, 1, part);
  }

  get parts() {
    return this._parts;
  }

  get autoComplete() {
    if (this._activePartIdx === this._autoCompletePartIdx) {
      return this._autoComplete;
    }

    return null;
  }

  getPositionAtEnd() {
    if (this._parts.length) {
      const index = this._parts.length - 1;
      const part = this._parts[index];
      return new _position.default(index, part.text.length);
    } else {
      // part index -1, as there are no parts to point at
      return new _position.default(-1, 0);
    }
  }

  serializeParts() {
    return this._parts.map(p => p.serialize());
  }

  _diff(newValue, inputType, caret) {
    const previousValue = this.parts.reduce((text, p) => text + p.text, ""); // can't use caret position with drag and drop

    if (inputType === "deleteByDrag") {
      return (0, _diff.diffDeletion)(previousValue, newValue);
    } else {
      return (0, _diff.diffAtCaret)(previousValue, newValue, caret.offset);
    }
  }

  reset(serializedParts, caret, inputType) {
    this._parts = serializedParts.map(p => this._partCreator.deserializePart(p));

    if (!caret) {
      caret = this.getPositionAtEnd();
    } // close auto complete if open
    // this would happen when clearing the composer after sending
    // a message with the autocomplete still open


    if (this._autoComplete) {
      this._autoComplete = null;
      this._autoCompletePartIdx = null;
    }

    this._updateCallback(caret, inputType);
  }
  /**
   * Inserts the given parts at the given position.
   * Should be run inside a `model.transform()` callback.
   * @param {Part[]} parts the parts to replace the range with
   * @param {DocumentPosition} position the position to start inserting at
   * @return {Number} the amount of characters added
   */


  insert(parts, position) {
    const insertIndex = this._splitAt(position);

    let newTextLength = 0;

    for (let i = 0; i < parts.length; ++i) {
      const part = parts[i];
      newTextLength += part.text.length;

      this._insertPart(insertIndex + i, part);
    }

    return newTextLength;
  }

  update(newValue, inputType, caret) {
    const diff = this._diff(newValue, inputType, caret);

    const position = this.positionForOffset(diff.at, caret.atNodeEnd);
    let removedOffsetDecrease = 0;

    if (diff.removed) {
      removedOffsetDecrease = this.removeText(position, diff.removed.length);
    }

    let addedLen = 0;

    if (diff.added) {
      addedLen = this._addText(position, diff.added, inputType);
    }

    this._mergeAdjacentParts();

    const caretOffset = diff.at - removedOffsetDecrease + addedLen;
    let newPosition = this.positionForOffset(caretOffset, true);
    const canOpenAutoComplete = inputType !== "insertFromPaste" && inputType !== "insertFromDrop";

    const acPromise = this._setActivePart(newPosition, canOpenAutoComplete);

    if (this._transformCallback) {
      const transformAddedLen = this._transform(newPosition, inputType, diff);

      newPosition = this.positionForOffset(caretOffset + transformAddedLen, true);
    }

    this._updateCallback(newPosition, inputType, diff);

    return acPromise;
  }

  _transform(newPosition, inputType, diff) {
    const result = this._transformCallback(newPosition, inputType, diff);

    return Number.isFinite(result) ? result : 0;
  }

  _setActivePart(pos, canOpenAutoComplete) {
    const {
      index
    } = pos;
    const part = this._parts[index];

    if (part) {
      if (index !== this._activePartIdx) {
        this._activePartIdx = index;

        if (canOpenAutoComplete && this._activePartIdx !== this._autoCompletePartIdx) {
          // else try to create one
          const ac = part.createAutoComplete(this._onAutoComplete);

          if (ac) {
            // make sure that react picks up the difference between both acs
            this._autoComplete = ac;
            this._autoCompletePartIdx = index;
            this._autoCompletePartCount = 1;
          }
        }
      } // not _autoComplete, only there if active part is autocomplete part


      if (this.autoComplete) {
        return this.autoComplete.onPartUpdate(part, pos);
      }
    } else {
      this._activePartIdx = null;
      this._autoComplete = null;
      this._autoCompletePartIdx = null;
      this._autoCompletePartCount = 0;
    }

    return Promise.resolve();
  }

  _mergeAdjacentParts() {
    let prevPart;

    for (let i = 0; i < this._parts.length; ++i) {
      let part = this._parts[i];
      const isEmpty = !part.text.length;
      const isMerged = !isEmpty && prevPart && prevPart.merge(part);

      if (isEmpty || isMerged) {
        // remove empty or merged part
        part = prevPart;

        this._removePart(i); //repeat this index, as it's removed now


        --i;
      }

      prevPart = part;
    }
  }
  /**
   * removes `len` amount of characters at `pos`.
   * @param {Object} pos
   * @param {Number} len
   * @return {Number} how many characters before pos were also removed,
   * usually because of non-editable parts that can only be removed in their entirety.
   */


  removeText(pos, len) {
    let {
      index,
      offset
    } = pos;
    let removedOffsetDecrease = 0;

    while (len > 0) {
      // part might be undefined here
      let part = this._parts[index];
      const amount = Math.min(len, part.text.length - offset); // don't allow 0 amount deletions

      if (amount) {
        if (part.canEdit) {
          const replaceWith = part.remove(offset, amount);

          if (typeof replaceWith === "string") {
            this._replacePart(index, this._partCreator.createDefaultPart(replaceWith));
          }

          part = this._parts[index]; // remove empty part

          if (!part.text.length) {
            this._removePart(index);
          } else {
            index += 1;
          }
        } else {
          removedOffsetDecrease += offset;

          this._removePart(index);
        }
      } else {
        index += 1;
      }

      len -= amount;
      offset = 0;
    }

    return removedOffsetDecrease;
  } // return part index where insertion will insert between at offset


  _splitAt(pos) {
    if (pos.index === -1) {
      return 0;
    }

    if (pos.offset === 0) {
      return pos.index;
    }

    const part = this._parts[pos.index];

    if (pos.offset >= part.text.length) {
      return pos.index + 1;
    }

    const secondPart = part.split(pos.offset);

    this._insertPart(pos.index + 1, secondPart);

    return pos.index + 1;
  }
  /**
   * inserts `str` into the model at `pos`.
   * @param {Object} pos
   * @param {string} str
   * @param {string} inputType the source of the input, see html InputEvent.inputType
   * @param {bool} options.validate Whether characters will be validated by the part.
   *                                Validating allows the inserted text to be parsed according to the part rules.
   * @return {Number} how far from position (in characters) the insertion ended.
   * This can be more than the length of `str` when crossing non-editable parts, which are skipped.
   */


  _addText(pos, str, inputType) {
    let {
      index
    } = pos;
    const {
      offset
    } = pos;
    let addLen = str.length;
    const part = this._parts[index];

    if (part) {
      if (part.canEdit) {
        if (part.validateAndInsert(offset, str, inputType)) {
          str = null;
        } else {
          const splitPart = part.split(offset);
          index += 1;

          this._insertPart(index, splitPart);
        }
      } else if (offset !== 0) {
        // not-editable part, caret is not at start,
        // so insert str after this part
        addLen += part.text.length - offset;
        index += 1;
      }
    } else if (index < 0) {
      // if position was not found (index: -1, as happens for empty editor)
      // reset it to insert as first part
      index = 0;
    }

    while (str) {
      const newPart = this._partCreator.createPartForInput(str, index, inputType);

      str = newPart.appendUntilRejected(str, inputType);

      this._insertPart(index, newPart);

      index += 1;
    }

    return addLen;
  }

  positionForOffset(totalOffset, atPartEnd) {
    let currentOffset = 0;

    const index = this._parts.findIndex(part => {
      const partLen = part.text.length;

      if (atPartEnd && currentOffset + partLen >= totalOffset || !atPartEnd && currentOffset + partLen > totalOffset) {
        return true;
      }

      currentOffset += partLen;
      return false;
    });

    if (index === -1) {
      return this.getPositionAtEnd();
    } else {
      return new _position.default(index, totalOffset - currentOffset);
    }
  }
  /**
   * Starts a range, which can span across multiple parts, to find and replace text.
   * @param {DocumentPosition} positionA a boundary of the range
   * @param {DocumentPosition?} positionB the other boundary of the range, optional
   * @return {Range}
   */


  startRange(positionA, positionB = positionA) {
    return new _range.default(this, positionA, positionB);
  } // called from Range.replace


  _replaceRange(startPosition, endPosition, parts) {
    // convert end position to offset, so it is independent of how the document is split into parts
    // which we'll change when splitting up at the start position
    const endOffset = endPosition.asOffset(this);

    const newStartPartIndex = this._splitAt(startPosition); // convert it back to position once split at start


    endPosition = endOffset.asPosition(this);

    const newEndPartIndex = this._splitAt(endPosition);

    for (let i = newEndPartIndex - 1; i >= newStartPartIndex; --i) {
      this._removePart(i);
    }

    let insertIdx = newStartPartIndex;

    for (const part of parts) {
      this._insertPart(insertIdx, part);

      insertIdx += 1;
    }

    this._mergeAdjacentParts();
  }
  /**
   * Performs a transformation not part of an update cycle.
   * Modifying the model should only happen inside a transform call if not part of an update call.
   * @param {ManualTransformCallback} callback to run the transformations in
   * @return {Promise} a promise when auto-complete (if applicable) is done updating
   */


  transform(callback) {
    const pos = callback();
    let acPromise = null;

    if (!(pos instanceof _range.default)) {
      acPromise = this._setActivePart(pos, true);
    } else {
      acPromise = Promise.resolve();
    }

    this._updateCallback(pos);

    return acPromise;
  }

}

exports.default = EditorModel;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9lZGl0b3IvbW9kZWwuanMiXSwibmFtZXMiOlsiRWRpdG9yTW9kZWwiLCJjb25zdHJ1Y3RvciIsInBhcnRzIiwicGFydENyZWF0b3IiLCJ1cGRhdGVDYWxsYmFjayIsInJlcGxhY2VQYXJ0cyIsImNsb3NlIiwicG9zIiwiX3BhcnRzIiwic3BsaWNlIiwiX2F1dG9Db21wbGV0ZVBhcnRJZHgiLCJfYXV0b0NvbXBsZXRlUGFydENvdW50IiwibGVuZ3RoIiwibGFzdFBhcnQiLCJsYXN0UGFydEluZGV4IiwiRG9jdW1lbnRQb3NpdGlvbiIsInRleHQiLCJfYXV0b0NvbXBsZXRlIiwiX3VwZGF0ZUNhbGxiYWNrIiwiX3BhcnRDcmVhdG9yIiwiX2FjdGl2ZVBhcnRJZHgiLCJfdHJhbnNmb3JtQ2FsbGJhY2siLCJzZXRVcGRhdGVDYWxsYmFjayIsInNldFRyYW5zZm9ybUNhbGxiYWNrIiwidHJhbnNmb3JtQ2FsbGJhY2siLCJpc0VtcHR5IiwicmVkdWNlIiwibGVuIiwicGFydCIsImNsb25lIiwiX2luc2VydFBhcnQiLCJpbmRleCIsIl9yZW1vdmVQYXJ0IiwiX3JlcGxhY2VQYXJ0IiwiYXV0b0NvbXBsZXRlIiwiZ2V0UG9zaXRpb25BdEVuZCIsInNlcmlhbGl6ZVBhcnRzIiwibWFwIiwicCIsInNlcmlhbGl6ZSIsIl9kaWZmIiwibmV3VmFsdWUiLCJpbnB1dFR5cGUiLCJjYXJldCIsInByZXZpb3VzVmFsdWUiLCJvZmZzZXQiLCJyZXNldCIsInNlcmlhbGl6ZWRQYXJ0cyIsImRlc2VyaWFsaXplUGFydCIsImluc2VydCIsInBvc2l0aW9uIiwiaW5zZXJ0SW5kZXgiLCJfc3BsaXRBdCIsIm5ld1RleHRMZW5ndGgiLCJpIiwidXBkYXRlIiwiZGlmZiIsInBvc2l0aW9uRm9yT2Zmc2V0IiwiYXQiLCJhdE5vZGVFbmQiLCJyZW1vdmVkT2Zmc2V0RGVjcmVhc2UiLCJyZW1vdmVkIiwicmVtb3ZlVGV4dCIsImFkZGVkTGVuIiwiYWRkZWQiLCJfYWRkVGV4dCIsIl9tZXJnZUFkamFjZW50UGFydHMiLCJjYXJldE9mZnNldCIsIm5ld1Bvc2l0aW9uIiwiY2FuT3BlbkF1dG9Db21wbGV0ZSIsImFjUHJvbWlzZSIsIl9zZXRBY3RpdmVQYXJ0IiwidHJhbnNmb3JtQWRkZWRMZW4iLCJfdHJhbnNmb3JtIiwicmVzdWx0IiwiTnVtYmVyIiwiaXNGaW5pdGUiLCJhYyIsImNyZWF0ZUF1dG9Db21wbGV0ZSIsIl9vbkF1dG9Db21wbGV0ZSIsIm9uUGFydFVwZGF0ZSIsIlByb21pc2UiLCJyZXNvbHZlIiwicHJldlBhcnQiLCJpc01lcmdlZCIsIm1lcmdlIiwiYW1vdW50IiwiTWF0aCIsIm1pbiIsImNhbkVkaXQiLCJyZXBsYWNlV2l0aCIsInJlbW92ZSIsImNyZWF0ZURlZmF1bHRQYXJ0Iiwic2Vjb25kUGFydCIsInNwbGl0Iiwic3RyIiwiYWRkTGVuIiwidmFsaWRhdGVBbmRJbnNlcnQiLCJzcGxpdFBhcnQiLCJuZXdQYXJ0IiwiY3JlYXRlUGFydEZvcklucHV0IiwiYXBwZW5kVW50aWxSZWplY3RlZCIsInRvdGFsT2Zmc2V0IiwiYXRQYXJ0RW5kIiwiY3VycmVudE9mZnNldCIsImZpbmRJbmRleCIsInBhcnRMZW4iLCJzdGFydFJhbmdlIiwicG9zaXRpb25BIiwicG9zaXRpb25CIiwiUmFuZ2UiLCJfcmVwbGFjZVJhbmdlIiwic3RhcnRQb3NpdGlvbiIsImVuZFBvc2l0aW9uIiwiZW5kT2Zmc2V0IiwiYXNPZmZzZXQiLCJuZXdTdGFydFBhcnRJbmRleCIsImFzUG9zaXRpb24iLCJuZXdFbmRQYXJ0SW5kZXgiLCJpbnNlcnRJZHgiLCJ0cmFuc2Zvcm0iLCJjYWxsYmFjayJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFpQkE7O0FBQ0E7O0FBQ0E7O0FBbkJBOzs7Ozs7Ozs7Ozs7Ozs7OztBQXFCQTs7Ozs7OztBQU9DOzs7Ozs7Ozs7QUFTRDs7OztBQUtlLE1BQU1BLFdBQU4sQ0FBa0I7QUFDN0JDLEVBQUFBLFdBQVcsQ0FBQ0MsS0FBRCxFQUFRQyxXQUFSLEVBQXFCQyxjQUFjLEdBQUcsSUFBdEMsRUFBNEM7QUFBQSwyREFzTXJDLENBQUM7QUFBQ0MsTUFBQUEsWUFBRDtBQUFlQyxNQUFBQTtBQUFmLEtBQUQsS0FBMkI7QUFDekMsVUFBSUMsR0FBSjs7QUFDQSxVQUFJRixZQUFKLEVBQWtCO0FBQ2QsYUFBS0csTUFBTCxDQUFZQyxNQUFaLENBQW1CLEtBQUtDLG9CQUF4QixFQUE4QyxLQUFLQyxzQkFBbkQsRUFBMkUsR0FBR04sWUFBOUU7O0FBQ0EsYUFBS00sc0JBQUwsR0FBOEJOLFlBQVksQ0FBQ08sTUFBM0M7QUFDQSxjQUFNQyxRQUFRLEdBQUdSLFlBQVksQ0FBQ0EsWUFBWSxDQUFDTyxNQUFiLEdBQXNCLENBQXZCLENBQTdCO0FBQ0EsY0FBTUUsYUFBYSxHQUFHLEtBQUtKLG9CQUFMLEdBQTRCTCxZQUFZLENBQUNPLE1BQXpDLEdBQWtELENBQXhFO0FBQ0FMLFFBQUFBLEdBQUcsR0FBRyxJQUFJUSxpQkFBSixDQUFxQkQsYUFBckIsRUFBb0NELFFBQVEsQ0FBQ0csSUFBVCxDQUFjSixNQUFsRCxDQUFOO0FBQ0g7O0FBQ0QsVUFBSU4sS0FBSixFQUFXO0FBQ1AsYUFBS1csYUFBTCxHQUFxQixJQUFyQjtBQUNBLGFBQUtQLG9CQUFMLEdBQTRCLElBQTVCO0FBQ0EsYUFBS0Msc0JBQUwsR0FBOEIsQ0FBOUI7QUFDSCxPQWJ3QyxDQWN6QztBQUNBO0FBQ0E7OztBQUNBLFdBQUtPLGVBQUwsQ0FBcUJYLEdBQXJCO0FBQ0gsS0F4TnNEO0FBQ25ELFNBQUtDLE1BQUwsR0FBY04sS0FBZDtBQUNBLFNBQUtpQixZQUFMLEdBQW9CaEIsV0FBcEI7QUFDQSxTQUFLaUIsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFNBQUtILGFBQUwsR0FBcUIsSUFBckI7QUFDQSxTQUFLUCxvQkFBTCxHQUE0QixJQUE1QjtBQUNBLFNBQUtDLHNCQUFMLEdBQThCLENBQTlCO0FBQ0EsU0FBS1Usa0JBQUwsR0FBMEIsSUFBMUI7QUFDQSxTQUFLQyxpQkFBTCxDQUF1QmxCLGNBQXZCO0FBQ0g7QUFFRDs7Ozs7Ozs7O0FBT0FtQixFQUFBQSxvQkFBb0IsQ0FBQ0MsaUJBQUQsRUFBb0I7QUFDcEMsU0FBS0gsa0JBQUwsR0FBMEJHLGlCQUExQjtBQUNIO0FBRUQ7Ozs7OztBQUlBRixFQUFBQSxpQkFBaUIsQ0FBQ2xCLGNBQUQsRUFBaUI7QUFDOUIsU0FBS2MsZUFBTCxHQUF1QmQsY0FBdkI7QUFDSDs7QUFFRCxNQUFJRCxXQUFKLEdBQWtCO0FBQ2QsV0FBTyxLQUFLZ0IsWUFBWjtBQUNIOztBQUVELE1BQUlNLE9BQUosR0FBYztBQUNWLFdBQU8sS0FBS2pCLE1BQUwsQ0FBWWtCLE1BQVosQ0FBbUIsQ0FBQ0MsR0FBRCxFQUFNQyxJQUFOLEtBQWVELEdBQUcsR0FBR0MsSUFBSSxDQUFDWixJQUFMLENBQVVKLE1BQWxELEVBQTBELENBQTFELE1BQWlFLENBQXhFO0FBQ0g7O0FBRURpQixFQUFBQSxLQUFLLEdBQUc7QUFDSixXQUFPLElBQUk3QixXQUFKLENBQWdCLEtBQUtRLE1BQXJCLEVBQTZCLEtBQUtXLFlBQWxDLEVBQWdELEtBQUtELGVBQXJELENBQVA7QUFDSDs7QUFFRFksRUFBQUEsV0FBVyxDQUFDQyxLQUFELEVBQVFILElBQVIsRUFBYztBQUNyQixTQUFLcEIsTUFBTCxDQUFZQyxNQUFaLENBQW1Cc0IsS0FBbkIsRUFBMEIsQ0FBMUIsRUFBNkJILElBQTdCOztBQUNBLFFBQUksS0FBS1IsY0FBTCxJQUF1QlcsS0FBM0IsRUFBa0M7QUFDOUIsUUFBRSxLQUFLWCxjQUFQO0FBQ0g7O0FBQ0QsUUFBSSxLQUFLVixvQkFBTCxJQUE2QnFCLEtBQWpDLEVBQXdDO0FBQ3BDLFFBQUUsS0FBS3JCLG9CQUFQO0FBQ0g7QUFDSjs7QUFFRHNCLEVBQUFBLFdBQVcsQ0FBQ0QsS0FBRCxFQUFRO0FBQ2YsU0FBS3ZCLE1BQUwsQ0FBWUMsTUFBWixDQUFtQnNCLEtBQW5CLEVBQTBCLENBQTFCOztBQUNBLFFBQUlBLEtBQUssS0FBSyxLQUFLWCxjQUFuQixFQUFtQztBQUMvQixXQUFLQSxjQUFMLEdBQXNCLElBQXRCO0FBQ0gsS0FGRCxNQUVPLElBQUksS0FBS0EsY0FBTCxHQUFzQlcsS0FBMUIsRUFBaUM7QUFDcEMsUUFBRSxLQUFLWCxjQUFQO0FBQ0g7O0FBQ0QsUUFBSVcsS0FBSyxLQUFLLEtBQUtyQixvQkFBbkIsRUFBeUM7QUFDckMsV0FBS0Esb0JBQUwsR0FBNEIsSUFBNUI7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLQSxvQkFBTCxHQUE0QnFCLEtBQWhDLEVBQXVDO0FBQzFDLFFBQUUsS0FBS3JCLG9CQUFQO0FBQ0g7QUFDSjs7QUFFRHVCLEVBQUFBLFlBQVksQ0FBQ0YsS0FBRCxFQUFRSCxJQUFSLEVBQWM7QUFDdEIsU0FBS3BCLE1BQUwsQ0FBWUMsTUFBWixDQUFtQnNCLEtBQW5CLEVBQTBCLENBQTFCLEVBQTZCSCxJQUE3QjtBQUNIOztBQUVELE1BQUkxQixLQUFKLEdBQVk7QUFDUixXQUFPLEtBQUtNLE1BQVo7QUFDSDs7QUFFRCxNQUFJMEIsWUFBSixHQUFtQjtBQUNmLFFBQUksS0FBS2QsY0FBTCxLQUF3QixLQUFLVixvQkFBakMsRUFBdUQ7QUFDbkQsYUFBTyxLQUFLTyxhQUFaO0FBQ0g7O0FBQ0QsV0FBTyxJQUFQO0FBQ0g7O0FBRURrQixFQUFBQSxnQkFBZ0IsR0FBRztBQUNmLFFBQUksS0FBSzNCLE1BQUwsQ0FBWUksTUFBaEIsRUFBd0I7QUFDcEIsWUFBTW1CLEtBQUssR0FBRyxLQUFLdkIsTUFBTCxDQUFZSSxNQUFaLEdBQXFCLENBQW5DO0FBQ0EsWUFBTWdCLElBQUksR0FBRyxLQUFLcEIsTUFBTCxDQUFZdUIsS0FBWixDQUFiO0FBQ0EsYUFBTyxJQUFJaEIsaUJBQUosQ0FBcUJnQixLQUFyQixFQUE0QkgsSUFBSSxDQUFDWixJQUFMLENBQVVKLE1BQXRDLENBQVA7QUFDSCxLQUpELE1BSU87QUFDSDtBQUNBLGFBQU8sSUFBSUcsaUJBQUosQ0FBcUIsQ0FBQyxDQUF0QixFQUF5QixDQUF6QixDQUFQO0FBQ0g7QUFDSjs7QUFFRHFCLEVBQUFBLGNBQWMsR0FBRztBQUNiLFdBQU8sS0FBSzVCLE1BQUwsQ0FBWTZCLEdBQVosQ0FBZ0JDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxTQUFGLEVBQXJCLENBQVA7QUFDSDs7QUFFREMsRUFBQUEsS0FBSyxDQUFDQyxRQUFELEVBQVdDLFNBQVgsRUFBc0JDLEtBQXRCLEVBQTZCO0FBQzlCLFVBQU1DLGFBQWEsR0FBRyxLQUFLMUMsS0FBTCxDQUFXd0IsTUFBWCxDQUFrQixDQUFDVixJQUFELEVBQU9zQixDQUFQLEtBQWF0QixJQUFJLEdBQUdzQixDQUFDLENBQUN0QixJQUF4QyxFQUE4QyxFQUE5QyxDQUF0QixDQUQ4QixDQUU5Qjs7QUFDQSxRQUFJMEIsU0FBUyxLQUFLLGNBQWxCLEVBQWtDO0FBQzlCLGFBQU8sd0JBQWFFLGFBQWIsRUFBNEJILFFBQTVCLENBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLHVCQUFZRyxhQUFaLEVBQTJCSCxRQUEzQixFQUFxQ0UsS0FBSyxDQUFDRSxNQUEzQyxDQUFQO0FBQ0g7QUFDSjs7QUFFREMsRUFBQUEsS0FBSyxDQUFDQyxlQUFELEVBQWtCSixLQUFsQixFQUF5QkQsU0FBekIsRUFBb0M7QUFDckMsU0FBS2xDLE1BQUwsR0FBY3VDLGVBQWUsQ0FBQ1YsR0FBaEIsQ0FBb0JDLENBQUMsSUFBSSxLQUFLbkIsWUFBTCxDQUFrQjZCLGVBQWxCLENBQWtDVixDQUFsQyxDQUF6QixDQUFkOztBQUNBLFFBQUksQ0FBQ0ssS0FBTCxFQUFZO0FBQ1JBLE1BQUFBLEtBQUssR0FBRyxLQUFLUixnQkFBTCxFQUFSO0FBQ0gsS0FKb0MsQ0FLckM7QUFDQTtBQUNBOzs7QUFDQSxRQUFJLEtBQUtsQixhQUFULEVBQXdCO0FBQ3BCLFdBQUtBLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxXQUFLUCxvQkFBTCxHQUE0QixJQUE1QjtBQUNIOztBQUNELFNBQUtRLGVBQUwsQ0FBcUJ5QixLQUFyQixFQUE0QkQsU0FBNUI7QUFDSDtBQUVEOzs7Ozs7Ozs7QUFPQU8sRUFBQUEsTUFBTSxDQUFDL0MsS0FBRCxFQUFRZ0QsUUFBUixFQUFrQjtBQUNwQixVQUFNQyxXQUFXLEdBQUcsS0FBS0MsUUFBTCxDQUFjRixRQUFkLENBQXBCOztBQUNBLFFBQUlHLGFBQWEsR0FBRyxDQUFwQjs7QUFDQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdwRCxLQUFLLENBQUNVLE1BQTFCLEVBQWtDLEVBQUUwQyxDQUFwQyxFQUF1QztBQUNuQyxZQUFNMUIsSUFBSSxHQUFHMUIsS0FBSyxDQUFDb0QsQ0FBRCxDQUFsQjtBQUNBRCxNQUFBQSxhQUFhLElBQUl6QixJQUFJLENBQUNaLElBQUwsQ0FBVUosTUFBM0I7O0FBQ0EsV0FBS2tCLFdBQUwsQ0FBaUJxQixXQUFXLEdBQUdHLENBQS9CLEVBQWtDMUIsSUFBbEM7QUFDSDs7QUFDRCxXQUFPeUIsYUFBUDtBQUNIOztBQUVERSxFQUFBQSxNQUFNLENBQUNkLFFBQUQsRUFBV0MsU0FBWCxFQUFzQkMsS0FBdEIsRUFBNkI7QUFDL0IsVUFBTWEsSUFBSSxHQUFHLEtBQUtoQixLQUFMLENBQVdDLFFBQVgsRUFBcUJDLFNBQXJCLEVBQWdDQyxLQUFoQyxDQUFiOztBQUNBLFVBQU1PLFFBQVEsR0FBRyxLQUFLTyxpQkFBTCxDQUF1QkQsSUFBSSxDQUFDRSxFQUE1QixFQUFnQ2YsS0FBSyxDQUFDZ0IsU0FBdEMsQ0FBakI7QUFDQSxRQUFJQyxxQkFBcUIsR0FBRyxDQUE1Qjs7QUFDQSxRQUFJSixJQUFJLENBQUNLLE9BQVQsRUFBa0I7QUFDZEQsTUFBQUEscUJBQXFCLEdBQUcsS0FBS0UsVUFBTCxDQUFnQlosUUFBaEIsRUFBMEJNLElBQUksQ0FBQ0ssT0FBTCxDQUFhakQsTUFBdkMsQ0FBeEI7QUFDSDs7QUFDRCxRQUFJbUQsUUFBUSxHQUFHLENBQWY7O0FBQ0EsUUFBSVAsSUFBSSxDQUFDUSxLQUFULEVBQWdCO0FBQ1pELE1BQUFBLFFBQVEsR0FBRyxLQUFLRSxRQUFMLENBQWNmLFFBQWQsRUFBd0JNLElBQUksQ0FBQ1EsS0FBN0IsRUFBb0N0QixTQUFwQyxDQUFYO0FBQ0g7O0FBQ0QsU0FBS3dCLG1CQUFMOztBQUNBLFVBQU1DLFdBQVcsR0FBR1gsSUFBSSxDQUFDRSxFQUFMLEdBQVVFLHFCQUFWLEdBQWtDRyxRQUF0RDtBQUNBLFFBQUlLLFdBQVcsR0FBRyxLQUFLWCxpQkFBTCxDQUF1QlUsV0FBdkIsRUFBb0MsSUFBcEMsQ0FBbEI7QUFDQSxVQUFNRSxtQkFBbUIsR0FBRzNCLFNBQVMsS0FBSyxpQkFBZCxJQUFtQ0EsU0FBUyxLQUFLLGdCQUE3RTs7QUFDQSxVQUFNNEIsU0FBUyxHQUFHLEtBQUtDLGNBQUwsQ0FBb0JILFdBQXBCLEVBQWlDQyxtQkFBakMsQ0FBbEI7O0FBQ0EsUUFBSSxLQUFLaEQsa0JBQVQsRUFBNkI7QUFDekIsWUFBTW1ELGlCQUFpQixHQUFHLEtBQUtDLFVBQUwsQ0FBZ0JMLFdBQWhCLEVBQTZCMUIsU0FBN0IsRUFBd0NjLElBQXhDLENBQTFCOztBQUNBWSxNQUFBQSxXQUFXLEdBQUcsS0FBS1gsaUJBQUwsQ0FBdUJVLFdBQVcsR0FBR0ssaUJBQXJDLEVBQXdELElBQXhELENBQWQ7QUFDSDs7QUFDRCxTQUFLdEQsZUFBTCxDQUFxQmtELFdBQXJCLEVBQWtDMUIsU0FBbEMsRUFBNkNjLElBQTdDOztBQUNBLFdBQU9jLFNBQVA7QUFDSDs7QUFFREcsRUFBQUEsVUFBVSxDQUFDTCxXQUFELEVBQWMxQixTQUFkLEVBQXlCYyxJQUF6QixFQUErQjtBQUNyQyxVQUFNa0IsTUFBTSxHQUFHLEtBQUtyRCxrQkFBTCxDQUF3QitDLFdBQXhCLEVBQXFDMUIsU0FBckMsRUFBZ0RjLElBQWhELENBQWY7O0FBQ0EsV0FBT21CLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkYsTUFBaEIsSUFBMEJBLE1BQTFCLEdBQW1DLENBQTFDO0FBQ0g7O0FBRURILEVBQUFBLGNBQWMsQ0FBQ2hFLEdBQUQsRUFBTThELG1CQUFOLEVBQTJCO0FBQ3JDLFVBQU07QUFBQ3RDLE1BQUFBO0FBQUQsUUFBVXhCLEdBQWhCO0FBQ0EsVUFBTXFCLElBQUksR0FBRyxLQUFLcEIsTUFBTCxDQUFZdUIsS0FBWixDQUFiOztBQUNBLFFBQUlILElBQUosRUFBVTtBQUNOLFVBQUlHLEtBQUssS0FBSyxLQUFLWCxjQUFuQixFQUFtQztBQUMvQixhQUFLQSxjQUFMLEdBQXNCVyxLQUF0Qjs7QUFDQSxZQUFJc0MsbUJBQW1CLElBQUksS0FBS2pELGNBQUwsS0FBd0IsS0FBS1Ysb0JBQXhELEVBQThFO0FBQzFFO0FBQ0EsZ0JBQU1tRSxFQUFFLEdBQUdqRCxJQUFJLENBQUNrRCxrQkFBTCxDQUF3QixLQUFLQyxlQUE3QixDQUFYOztBQUNBLGNBQUlGLEVBQUosRUFBUTtBQUNKO0FBQ0EsaUJBQUs1RCxhQUFMLEdBQXFCNEQsRUFBckI7QUFDQSxpQkFBS25FLG9CQUFMLEdBQTRCcUIsS0FBNUI7QUFDQSxpQkFBS3BCLHNCQUFMLEdBQThCLENBQTlCO0FBQ0g7QUFDSjtBQUNKLE9BYkssQ0FjTjs7O0FBQ0EsVUFBSSxLQUFLdUIsWUFBVCxFQUF1QjtBQUNuQixlQUFPLEtBQUtBLFlBQUwsQ0FBa0I4QyxZQUFsQixDQUErQnBELElBQS9CLEVBQXFDckIsR0FBckMsQ0FBUDtBQUNIO0FBQ0osS0FsQkQsTUFrQk87QUFDSCxXQUFLYSxjQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS0gsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUtQLG9CQUFMLEdBQTRCLElBQTVCO0FBQ0EsV0FBS0Msc0JBQUwsR0FBOEIsQ0FBOUI7QUFDSDs7QUFDRCxXQUFPc0UsT0FBTyxDQUFDQyxPQUFSLEVBQVA7QUFDSDs7QUFzQkRoQixFQUFBQSxtQkFBbUIsR0FBRztBQUNsQixRQUFJaUIsUUFBSjs7QUFDQSxTQUFLLElBQUk3QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLEtBQUs5QyxNQUFMLENBQVlJLE1BQWhDLEVBQXdDLEVBQUUwQyxDQUExQyxFQUE2QztBQUN6QyxVQUFJMUIsSUFBSSxHQUFHLEtBQUtwQixNQUFMLENBQVk4QyxDQUFaLENBQVg7QUFDQSxZQUFNN0IsT0FBTyxHQUFHLENBQUNHLElBQUksQ0FBQ1osSUFBTCxDQUFVSixNQUEzQjtBQUNBLFlBQU13RSxRQUFRLEdBQUcsQ0FBQzNELE9BQUQsSUFBWTBELFFBQVosSUFBd0JBLFFBQVEsQ0FBQ0UsS0FBVCxDQUFlekQsSUFBZixDQUF6Qzs7QUFDQSxVQUFJSCxPQUFPLElBQUkyRCxRQUFmLEVBQXlCO0FBQ3JCO0FBQ0F4RCxRQUFBQSxJQUFJLEdBQUd1RCxRQUFQOztBQUNBLGFBQUtuRCxXQUFMLENBQWlCc0IsQ0FBakIsRUFIcUIsQ0FJckI7OztBQUNBLFVBQUVBLENBQUY7QUFDSDs7QUFDRDZCLE1BQUFBLFFBQVEsR0FBR3ZELElBQVg7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7OztBQU9Ba0MsRUFBQUEsVUFBVSxDQUFDdkQsR0FBRCxFQUFNb0IsR0FBTixFQUFXO0FBQ2pCLFFBQUk7QUFBQ0ksTUFBQUEsS0FBRDtBQUFRYyxNQUFBQTtBQUFSLFFBQWtCdEMsR0FBdEI7QUFDQSxRQUFJcUQscUJBQXFCLEdBQUcsQ0FBNUI7O0FBQ0EsV0FBT2pDLEdBQUcsR0FBRyxDQUFiLEVBQWdCO0FBQ1o7QUFDQSxVQUFJQyxJQUFJLEdBQUcsS0FBS3BCLE1BQUwsQ0FBWXVCLEtBQVosQ0FBWDtBQUNBLFlBQU11RCxNQUFNLEdBQUdDLElBQUksQ0FBQ0MsR0FBTCxDQUFTN0QsR0FBVCxFQUFjQyxJQUFJLENBQUNaLElBQUwsQ0FBVUosTUFBVixHQUFtQmlDLE1BQWpDLENBQWYsQ0FIWSxDQUlaOztBQUNBLFVBQUl5QyxNQUFKLEVBQVk7QUFDUixZQUFJMUQsSUFBSSxDQUFDNkQsT0FBVCxFQUFrQjtBQUNkLGdCQUFNQyxXQUFXLEdBQUc5RCxJQUFJLENBQUMrRCxNQUFMLENBQVk5QyxNQUFaLEVBQW9CeUMsTUFBcEIsQ0FBcEI7O0FBQ0EsY0FBSSxPQUFPSSxXQUFQLEtBQXVCLFFBQTNCLEVBQXFDO0FBQ2pDLGlCQUFLekQsWUFBTCxDQUFrQkYsS0FBbEIsRUFBeUIsS0FBS1osWUFBTCxDQUFrQnlFLGlCQUFsQixDQUFvQ0YsV0FBcEMsQ0FBekI7QUFDSDs7QUFDRDlELFVBQUFBLElBQUksR0FBRyxLQUFLcEIsTUFBTCxDQUFZdUIsS0FBWixDQUFQLENBTGMsQ0FNZDs7QUFDQSxjQUFJLENBQUNILElBQUksQ0FBQ1osSUFBTCxDQUFVSixNQUFmLEVBQXVCO0FBQ25CLGlCQUFLb0IsV0FBTCxDQUFpQkQsS0FBakI7QUFDSCxXQUZELE1BRU87QUFDSEEsWUFBQUEsS0FBSyxJQUFJLENBQVQ7QUFDSDtBQUNKLFNBWkQsTUFZTztBQUNINkIsVUFBQUEscUJBQXFCLElBQUlmLE1BQXpCOztBQUNBLGVBQUtiLFdBQUwsQ0FBaUJELEtBQWpCO0FBQ0g7QUFDSixPQWpCRCxNQWlCTztBQUNIQSxRQUFBQSxLQUFLLElBQUksQ0FBVDtBQUNIOztBQUNESixNQUFBQSxHQUFHLElBQUkyRCxNQUFQO0FBQ0F6QyxNQUFBQSxNQUFNLEdBQUcsQ0FBVDtBQUNIOztBQUNELFdBQU9lLHFCQUFQO0FBQ0gsR0FuUjRCLENBb1I3Qjs7O0FBQ0FSLEVBQUFBLFFBQVEsQ0FBQzdDLEdBQUQsRUFBTTtBQUNWLFFBQUlBLEdBQUcsQ0FBQ3dCLEtBQUosS0FBYyxDQUFDLENBQW5CLEVBQXNCO0FBQ2xCLGFBQU8sQ0FBUDtBQUNIOztBQUNELFFBQUl4QixHQUFHLENBQUNzQyxNQUFKLEtBQWUsQ0FBbkIsRUFBc0I7QUFDbEIsYUFBT3RDLEdBQUcsQ0FBQ3dCLEtBQVg7QUFDSDs7QUFDRCxVQUFNSCxJQUFJLEdBQUcsS0FBS3BCLE1BQUwsQ0FBWUQsR0FBRyxDQUFDd0IsS0FBaEIsQ0FBYjs7QUFDQSxRQUFJeEIsR0FBRyxDQUFDc0MsTUFBSixJQUFjakIsSUFBSSxDQUFDWixJQUFMLENBQVVKLE1BQTVCLEVBQW9DO0FBQ2hDLGFBQU9MLEdBQUcsQ0FBQ3dCLEtBQUosR0FBWSxDQUFuQjtBQUNIOztBQUVELFVBQU04RCxVQUFVLEdBQUdqRSxJQUFJLENBQUNrRSxLQUFMLENBQVd2RixHQUFHLENBQUNzQyxNQUFmLENBQW5COztBQUNBLFNBQUtmLFdBQUwsQ0FBaUJ2QixHQUFHLENBQUN3QixLQUFKLEdBQVksQ0FBN0IsRUFBZ0M4RCxVQUFoQzs7QUFDQSxXQUFPdEYsR0FBRyxDQUFDd0IsS0FBSixHQUFZLENBQW5CO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7O0FBVUFrQyxFQUFBQSxRQUFRLENBQUMxRCxHQUFELEVBQU13RixHQUFOLEVBQVdyRCxTQUFYLEVBQXNCO0FBQzFCLFFBQUk7QUFBQ1gsTUFBQUE7QUFBRCxRQUFVeEIsR0FBZDtBQUNBLFVBQU07QUFBQ3NDLE1BQUFBO0FBQUQsUUFBV3RDLEdBQWpCO0FBQ0EsUUFBSXlGLE1BQU0sR0FBR0QsR0FBRyxDQUFDbkYsTUFBakI7QUFDQSxVQUFNZ0IsSUFBSSxHQUFHLEtBQUtwQixNQUFMLENBQVl1QixLQUFaLENBQWI7O0FBQ0EsUUFBSUgsSUFBSixFQUFVO0FBQ04sVUFBSUEsSUFBSSxDQUFDNkQsT0FBVCxFQUFrQjtBQUNkLFlBQUk3RCxJQUFJLENBQUNxRSxpQkFBTCxDQUF1QnBELE1BQXZCLEVBQStCa0QsR0FBL0IsRUFBb0NyRCxTQUFwQyxDQUFKLEVBQW9EO0FBQ2hEcUQsVUFBQUEsR0FBRyxHQUFHLElBQU47QUFDSCxTQUZELE1BRU87QUFDSCxnQkFBTUcsU0FBUyxHQUFHdEUsSUFBSSxDQUFDa0UsS0FBTCxDQUFXakQsTUFBWCxDQUFsQjtBQUNBZCxVQUFBQSxLQUFLLElBQUksQ0FBVDs7QUFDQSxlQUFLRCxXQUFMLENBQWlCQyxLQUFqQixFQUF3Qm1FLFNBQXhCO0FBQ0g7QUFDSixPQVJELE1BUU8sSUFBSXJELE1BQU0sS0FBSyxDQUFmLEVBQWtCO0FBQ3JCO0FBQ0E7QUFDQW1ELFFBQUFBLE1BQU0sSUFBSXBFLElBQUksQ0FBQ1osSUFBTCxDQUFVSixNQUFWLEdBQW1CaUMsTUFBN0I7QUFDQWQsUUFBQUEsS0FBSyxJQUFJLENBQVQ7QUFDSDtBQUNKLEtBZkQsTUFlTyxJQUFJQSxLQUFLLEdBQUcsQ0FBWixFQUFlO0FBQ2xCO0FBQ0E7QUFDQUEsTUFBQUEsS0FBSyxHQUFHLENBQVI7QUFDSDs7QUFDRCxXQUFPZ0UsR0FBUCxFQUFZO0FBQ1IsWUFBTUksT0FBTyxHQUFHLEtBQUtoRixZQUFMLENBQWtCaUYsa0JBQWxCLENBQXFDTCxHQUFyQyxFQUEwQ2hFLEtBQTFDLEVBQWlEVyxTQUFqRCxDQUFoQjs7QUFDQXFELE1BQUFBLEdBQUcsR0FBR0ksT0FBTyxDQUFDRSxtQkFBUixDQUE0Qk4sR0FBNUIsRUFBaUNyRCxTQUFqQyxDQUFOOztBQUNBLFdBQUtaLFdBQUwsQ0FBaUJDLEtBQWpCLEVBQXdCb0UsT0FBeEI7O0FBQ0FwRSxNQUFBQSxLQUFLLElBQUksQ0FBVDtBQUNIOztBQUNELFdBQU9pRSxNQUFQO0FBQ0g7O0FBRUR2QyxFQUFBQSxpQkFBaUIsQ0FBQzZDLFdBQUQsRUFBY0MsU0FBZCxFQUF5QjtBQUN0QyxRQUFJQyxhQUFhLEdBQUcsQ0FBcEI7O0FBQ0EsVUFBTXpFLEtBQUssR0FBRyxLQUFLdkIsTUFBTCxDQUFZaUcsU0FBWixDQUFzQjdFLElBQUksSUFBSTtBQUN4QyxZQUFNOEUsT0FBTyxHQUFHOUUsSUFBSSxDQUFDWixJQUFMLENBQVVKLE1BQTFCOztBQUNBLFVBQ0syRixTQUFTLElBQUtDLGFBQWEsR0FBR0UsT0FBakIsSUFBNkJKLFdBQTNDLElBQ0MsQ0FBQ0MsU0FBRCxJQUFlQyxhQUFhLEdBQUdFLE9BQWpCLEdBQTRCSixXQUYvQyxFQUdFO0FBQ0UsZUFBTyxJQUFQO0FBQ0g7O0FBQ0RFLE1BQUFBLGFBQWEsSUFBSUUsT0FBakI7QUFDQSxhQUFPLEtBQVA7QUFDSCxLQVZhLENBQWQ7O0FBV0EsUUFBSTNFLEtBQUssS0FBSyxDQUFDLENBQWYsRUFBa0I7QUFDZCxhQUFPLEtBQUtJLGdCQUFMLEVBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLElBQUlwQixpQkFBSixDQUFxQmdCLEtBQXJCLEVBQTRCdUUsV0FBVyxHQUFHRSxhQUExQyxDQUFQO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7OztBQU1BRyxFQUFBQSxVQUFVLENBQUNDLFNBQUQsRUFBWUMsU0FBUyxHQUFHRCxTQUF4QixFQUFtQztBQUN6QyxXQUFPLElBQUlFLGNBQUosQ0FBVSxJQUFWLEVBQWdCRixTQUFoQixFQUEyQkMsU0FBM0IsQ0FBUDtBQUNILEdBOVc0QixDQWdYN0I7OztBQUNBRSxFQUFBQSxhQUFhLENBQUNDLGFBQUQsRUFBZ0JDLFdBQWhCLEVBQTZCL0csS0FBN0IsRUFBb0M7QUFDN0M7QUFDQTtBQUNBLFVBQU1nSCxTQUFTLEdBQUdELFdBQVcsQ0FBQ0UsUUFBWixDQUFxQixJQUFyQixDQUFsQjs7QUFDQSxVQUFNQyxpQkFBaUIsR0FBRyxLQUFLaEUsUUFBTCxDQUFjNEQsYUFBZCxDQUExQixDQUo2QyxDQUs3Qzs7O0FBQ0FDLElBQUFBLFdBQVcsR0FBR0MsU0FBUyxDQUFDRyxVQUFWLENBQXFCLElBQXJCLENBQWQ7O0FBQ0EsVUFBTUMsZUFBZSxHQUFHLEtBQUtsRSxRQUFMLENBQWM2RCxXQUFkLENBQXhCOztBQUNBLFNBQUssSUFBSTNELENBQUMsR0FBR2dFLGVBQWUsR0FBRyxDQUEvQixFQUFrQ2hFLENBQUMsSUFBSThELGlCQUF2QyxFQUEwRCxFQUFFOUQsQ0FBNUQsRUFBK0Q7QUFDM0QsV0FBS3RCLFdBQUwsQ0FBaUJzQixDQUFqQjtBQUNIOztBQUNELFFBQUlpRSxTQUFTLEdBQUdILGlCQUFoQjs7QUFDQSxTQUFLLE1BQU14RixJQUFYLElBQW1CMUIsS0FBbkIsRUFBMEI7QUFDdEIsV0FBSzRCLFdBQUwsQ0FBaUJ5RixTQUFqQixFQUE0QjNGLElBQTVCOztBQUNBMkYsTUFBQUEsU0FBUyxJQUFJLENBQWI7QUFDSDs7QUFDRCxTQUFLckQsbUJBQUw7QUFDSDtBQUVEOzs7Ozs7OztBQU1Bc0QsRUFBQUEsU0FBUyxDQUFDQyxRQUFELEVBQVc7QUFDaEIsVUFBTWxILEdBQUcsR0FBR2tILFFBQVEsRUFBcEI7QUFDQSxRQUFJbkQsU0FBUyxHQUFHLElBQWhCOztBQUNBLFFBQUksRUFBRS9ELEdBQUcsWUFBWXVHLGNBQWpCLENBQUosRUFBNkI7QUFDekJ4QyxNQUFBQSxTQUFTLEdBQUcsS0FBS0MsY0FBTCxDQUFvQmhFLEdBQXBCLEVBQXlCLElBQXpCLENBQVo7QUFDSCxLQUZELE1BRU87QUFDSCtELE1BQUFBLFNBQVMsR0FBR1csT0FBTyxDQUFDQyxPQUFSLEVBQVo7QUFDSDs7QUFDRCxTQUFLaEUsZUFBTCxDQUFxQlgsR0FBckI7O0FBQ0EsV0FBTytELFNBQVA7QUFDSDs7QUFwWjRCIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTkgTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQge2RpZmZBdENhcmV0LCBkaWZmRGVsZXRpb259IGZyb20gXCIuL2RpZmZcIjtcclxuaW1wb3J0IERvY3VtZW50UG9zaXRpb24gZnJvbSBcIi4vcG9zaXRpb25cIjtcclxuaW1wb3J0IFJhbmdlIGZyb20gXCIuL3JhbmdlXCI7XHJcblxyXG4vKipcclxuICogQGNhbGxiYWNrIE1vZGVsQ2FsbGJhY2tcclxuICogQHBhcmFtIHtEb2N1bWVudFBvc2l0aW9uP30gY2FyZXRQb3NpdGlvbiB0aGUgcG9zaXRpb24gd2hlcmUgdGhlIGNhcmV0IHNob3VsZCBiZSBwb3NpdGlvblxyXG4gKiBAcGFyYW0ge3N0cmluZz99IGlucHV0VHlwZSB0aGUgaW5wdXRUeXBlIG9mIHRoZSBET00gaW5wdXQgZXZlbnRcclxuICogQHBhcmFtIHtvYmplY3Q/fSBkaWZmIGFuIG9iamVjdCB3aXRoIGByZW1vdmVkYCBhbmQgYGFkZGVkYCBzdHJpbmdzXHJcbiAqL1xyXG5cclxuIC8qKlxyXG4gKiBAY2FsbGJhY2sgVHJhbnNmb3JtQ2FsbGJhY2tcclxuICogQHBhcmFtIHtEb2N1bWVudFBvc2l0aW9uP30gY2FyZXRQb3NpdGlvbiB0aGUgcG9zaXRpb24gd2hlcmUgdGhlIGNhcmV0IHNob3VsZCBiZSBwb3NpdGlvblxyXG4gKiBAcGFyYW0ge3N0cmluZz99IGlucHV0VHlwZSB0aGUgaW5wdXRUeXBlIG9mIHRoZSBET00gaW5wdXQgZXZlbnRcclxuICogQHBhcmFtIHtvYmplY3Q/fSBkaWZmIGFuIG9iamVjdCB3aXRoIGByZW1vdmVkYCBhbmQgYGFkZGVkYCBzdHJpbmdzXHJcbiAqIEByZXR1cm4ge051bWJlcj99IGFkZGVkTGVuIGhvdyBtYW55IGNoYXJhY3RlcnMgd2VyZSBhZGRlZC9yZW1vdmVkICgtKSBiZWZvcmUgdGhlIGNhcmV0IGR1cmluZyB0aGUgdHJhbnNmb3JtYXRpb24gc3RlcC5cclxuICogICAgVGhpcyBpcyB1c2VkIHRvIGFkanVzdCB0aGUgY2FyZXQgcG9zaXRpb24uXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEBjYWxsYmFjayBNYW51YWxUcmFuc2Zvcm1DYWxsYmFja1xyXG4gKiBAcmV0dXJuIHRoZSBjYXJldCBwb3NpdGlvblxyXG4gKi9cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVkaXRvck1vZGVsIHtcclxuICAgIGNvbnN0cnVjdG9yKHBhcnRzLCBwYXJ0Q3JlYXRvciwgdXBkYXRlQ2FsbGJhY2sgPSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5fcGFydHMgPSBwYXJ0cztcclxuICAgICAgICB0aGlzLl9wYXJ0Q3JlYXRvciA9IHBhcnRDcmVhdG9yO1xyXG4gICAgICAgIHRoaXMuX2FjdGl2ZVBhcnRJZHggPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydENvdW50ID0gMDtcclxuICAgICAgICB0aGlzLl90cmFuc2Zvcm1DYWxsYmFjayA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5zZXRVcGRhdGVDYWxsYmFjayh1cGRhdGVDYWxsYmFjayk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgYSBjYWxsYmFjayBmb3IgdGhlIHRyYW5zZm9ybWF0aW9uIHN0ZXAuXHJcbiAgICAgKiBXaGlsZSBwcm9jZXNzaW5nIGFuIHVwZGF0ZSwgcmlnaHQgYmVmb3JlIGNhbGxpbmcgdGhlIHVwZGF0ZSBjYWxsYmFjayxcclxuICAgICAqIGEgdHJhbnNmb3JtIGNhbGxiYWNrIGNhbiBiZSBjYWxsZWQsIHdoaWNoIHNlcnZlcyB0byBkbyBtb2RpZmljYXRpb25zXHJcbiAgICAgKiBvbiB0aGUgbW9kZWwgdGhhdCBjYW4gc3BhbiBtdWx0aXBsZSBwYXJ0cy4gQWxzbyBzZWUgYHN0YXJ0UmFuZ2UoKWAuXHJcbiAgICAgKiBAcGFyYW0ge1RyYW5zZm9ybUNhbGxiYWNrfSB0cmFuc2Zvcm1DYWxsYmFja1xyXG4gICAgICovXHJcbiAgICBzZXRUcmFuc2Zvcm1DYWxsYmFjayh0cmFuc2Zvcm1DYWxsYmFjaykge1xyXG4gICAgICAgIHRoaXMuX3RyYW5zZm9ybUNhbGxiYWNrID0gdHJhbnNmb3JtQ2FsbGJhY2s7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgYSBjYWxsYmFjayBmb3IgcmVyZW5kZXJpbmcgdGhlIG1vZGVsIGFmdGVyIGl0IGhhcyBiZWVuIHVwZGF0ZWQuXHJcbiAgICAgKiBAcGFyYW0ge01vZGVsQ2FsbGJhY2t9IHVwZGF0ZUNhbGxiYWNrXHJcbiAgICAgKi9cclxuICAgIHNldFVwZGF0ZUNhbGxiYWNrKHVwZGF0ZUNhbGxiYWNrKSB7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQ2FsbGJhY2sgPSB1cGRhdGVDYWxsYmFjaztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGFydENyZWF0b3IoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BhcnRDcmVhdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc0VtcHR5KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wYXJ0cy5yZWR1Y2UoKGxlbiwgcGFydCkgPT4gbGVuICsgcGFydC50ZXh0Lmxlbmd0aCwgMCkgPT09IDA7XHJcbiAgICB9XHJcblxyXG4gICAgY2xvbmUoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBFZGl0b3JNb2RlbCh0aGlzLl9wYXJ0cywgdGhpcy5fcGFydENyZWF0b3IsIHRoaXMuX3VwZGF0ZUNhbGxiYWNrKTtcclxuICAgIH1cclxuXHJcbiAgICBfaW5zZXJ0UGFydChpbmRleCwgcGFydCkge1xyXG4gICAgICAgIHRoaXMuX3BhcnRzLnNwbGljZShpbmRleCwgMCwgcGFydCk7XHJcbiAgICAgICAgaWYgKHRoaXMuX2FjdGl2ZVBhcnRJZHggPj0gaW5kZXgpIHtcclxuICAgICAgICAgICAgKyt0aGlzLl9hY3RpdmVQYXJ0SWR4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCA+PSBpbmRleCkge1xyXG4gICAgICAgICAgICArK3RoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9yZW1vdmVQYXJ0KGluZGV4KSB7XHJcbiAgICAgICAgdGhpcy5fcGFydHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICBpZiAoaW5kZXggPT09IHRoaXMuX2FjdGl2ZVBhcnRJZHgpIHtcclxuICAgICAgICAgICAgdGhpcy5fYWN0aXZlUGFydElkeCA9IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9hY3RpdmVQYXJ0SWR4ID4gaW5kZXgpIHtcclxuICAgICAgICAgICAgLS10aGlzLl9hY3RpdmVQYXJ0SWR4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaW5kZXggPT09IHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHgpIHtcclxuICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCA9IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9hdXRvQ29tcGxldGVQYXJ0SWR4ID4gaW5kZXgpIHtcclxuICAgICAgICAgICAgLS10aGlzLl9hdXRvQ29tcGxldGVQYXJ0SWR4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBfcmVwbGFjZVBhcnQoaW5kZXgsIHBhcnQpIHtcclxuICAgICAgICB0aGlzLl9wYXJ0cy5zcGxpY2UoaW5kZXgsIDEsIHBhcnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwYXJ0cygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcGFydHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGF1dG9Db21wbGV0ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5fYWN0aXZlUGFydElkeCA9PT0gdGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fYXV0b0NvbXBsZXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQb3NpdGlvbkF0RW5kKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9wYXJ0cy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLl9wYXJ0cy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJ0ID0gdGhpcy5fcGFydHNbaW5kZXhdO1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IERvY3VtZW50UG9zaXRpb24oaW5kZXgsIHBhcnQudGV4dC5sZW5ndGgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHBhcnQgaW5kZXggLTEsIGFzIHRoZXJlIGFyZSBubyBwYXJ0cyB0byBwb2ludCBhdFxyXG4gICAgICAgICAgICByZXR1cm4gbmV3IERvY3VtZW50UG9zaXRpb24oLTEsIDApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXJpYWxpemVQYXJ0cygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcGFydHMubWFwKHAgPT4gcC5zZXJpYWxpemUoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2RpZmYobmV3VmFsdWUsIGlucHV0VHlwZSwgY2FyZXQpIHtcclxuICAgICAgICBjb25zdCBwcmV2aW91c1ZhbHVlID0gdGhpcy5wYXJ0cy5yZWR1Y2UoKHRleHQsIHApID0+IHRleHQgKyBwLnRleHQsIFwiXCIpO1xyXG4gICAgICAgIC8vIGNhbid0IHVzZSBjYXJldCBwb3NpdGlvbiB3aXRoIGRyYWcgYW5kIGRyb3BcclxuICAgICAgICBpZiAoaW5wdXRUeXBlID09PSBcImRlbGV0ZUJ5RHJhZ1wiKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkaWZmRGVsZXRpb24ocHJldmlvdXNWYWx1ZSwgbmV3VmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkaWZmQXRDYXJldChwcmV2aW91c1ZhbHVlLCBuZXdWYWx1ZSwgY2FyZXQub2Zmc2V0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoc2VyaWFsaXplZFBhcnRzLCBjYXJldCwgaW5wdXRUeXBlKSB7XHJcbiAgICAgICAgdGhpcy5fcGFydHMgPSBzZXJpYWxpemVkUGFydHMubWFwKHAgPT4gdGhpcy5fcGFydENyZWF0b3IuZGVzZXJpYWxpemVQYXJ0KHApKTtcclxuICAgICAgICBpZiAoIWNhcmV0KSB7XHJcbiAgICAgICAgICAgIGNhcmV0ID0gdGhpcy5nZXRQb3NpdGlvbkF0RW5kKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGNsb3NlIGF1dG8gY29tcGxldGUgaWYgb3BlblxyXG4gICAgICAgIC8vIHRoaXMgd291bGQgaGFwcGVuIHdoZW4gY2xlYXJpbmcgdGhlIGNvbXBvc2VyIGFmdGVyIHNlbmRpbmdcclxuICAgICAgICAvLyBhIG1lc3NhZ2Ugd2l0aCB0aGUgYXV0b2NvbXBsZXRlIHN0aWxsIG9wZW5cclxuICAgICAgICBpZiAodGhpcy5fYXV0b0NvbXBsZXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZSA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHggPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91cGRhdGVDYWxsYmFjayhjYXJldCwgaW5wdXRUeXBlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEluc2VydHMgdGhlIGdpdmVuIHBhcnRzIGF0IHRoZSBnaXZlbiBwb3NpdGlvbi5cclxuICAgICAqIFNob3VsZCBiZSBydW4gaW5zaWRlIGEgYG1vZGVsLnRyYW5zZm9ybSgpYCBjYWxsYmFjay5cclxuICAgICAqIEBwYXJhbSB7UGFydFtdfSBwYXJ0cyB0aGUgcGFydHMgdG8gcmVwbGFjZSB0aGUgcmFuZ2Ugd2l0aFxyXG4gICAgICogQHBhcmFtIHtEb2N1bWVudFBvc2l0aW9ufSBwb3NpdGlvbiB0aGUgcG9zaXRpb24gdG8gc3RhcnQgaW5zZXJ0aW5nIGF0XHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9IHRoZSBhbW91bnQgb2YgY2hhcmFjdGVycyBhZGRlZFxyXG4gICAgICovXHJcbiAgICBpbnNlcnQocGFydHMsIHBvc2l0aW9uKSB7XHJcbiAgICAgICAgY29uc3QgaW5zZXJ0SW5kZXggPSB0aGlzLl9zcGxpdEF0KHBvc2l0aW9uKTtcclxuICAgICAgICBsZXQgbmV3VGV4dExlbmd0aCA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJ0cy5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJ0ID0gcGFydHNbaV07XHJcbiAgICAgICAgICAgIG5ld1RleHRMZW5ndGggKz0gcGFydC50ZXh0Lmxlbmd0aDtcclxuICAgICAgICAgICAgdGhpcy5faW5zZXJ0UGFydChpbnNlcnRJbmRleCArIGksIHBhcnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3VGV4dExlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUobmV3VmFsdWUsIGlucHV0VHlwZSwgY2FyZXQpIHtcclxuICAgICAgICBjb25zdCBkaWZmID0gdGhpcy5fZGlmZihuZXdWYWx1ZSwgaW5wdXRUeXBlLCBjYXJldCk7XHJcbiAgICAgICAgY29uc3QgcG9zaXRpb24gPSB0aGlzLnBvc2l0aW9uRm9yT2Zmc2V0KGRpZmYuYXQsIGNhcmV0LmF0Tm9kZUVuZCk7XHJcbiAgICAgICAgbGV0IHJlbW92ZWRPZmZzZXREZWNyZWFzZSA9IDA7XHJcbiAgICAgICAgaWYgKGRpZmYucmVtb3ZlZCkge1xyXG4gICAgICAgICAgICByZW1vdmVkT2Zmc2V0RGVjcmVhc2UgPSB0aGlzLnJlbW92ZVRleHQocG9zaXRpb24sIGRpZmYucmVtb3ZlZC5sZW5ndGgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgYWRkZWRMZW4gPSAwO1xyXG4gICAgICAgIGlmIChkaWZmLmFkZGVkKSB7XHJcbiAgICAgICAgICAgIGFkZGVkTGVuID0gdGhpcy5fYWRkVGV4dChwb3NpdGlvbiwgZGlmZi5hZGRlZCwgaW5wdXRUeXBlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbWVyZ2VBZGphY2VudFBhcnRzKCk7XHJcbiAgICAgICAgY29uc3QgY2FyZXRPZmZzZXQgPSBkaWZmLmF0IC0gcmVtb3ZlZE9mZnNldERlY3JlYXNlICsgYWRkZWRMZW47XHJcbiAgICAgICAgbGV0IG5ld1Bvc2l0aW9uID0gdGhpcy5wb3NpdGlvbkZvck9mZnNldChjYXJldE9mZnNldCwgdHJ1ZSk7XHJcbiAgICAgICAgY29uc3QgY2FuT3BlbkF1dG9Db21wbGV0ZSA9IGlucHV0VHlwZSAhPT0gXCJpbnNlcnRGcm9tUGFzdGVcIiAmJiBpbnB1dFR5cGUgIT09IFwiaW5zZXJ0RnJvbURyb3BcIjtcclxuICAgICAgICBjb25zdCBhY1Byb21pc2UgPSB0aGlzLl9zZXRBY3RpdmVQYXJ0KG5ld1Bvc2l0aW9uLCBjYW5PcGVuQXV0b0NvbXBsZXRlKTtcclxuICAgICAgICBpZiAodGhpcy5fdHJhbnNmb3JtQ2FsbGJhY2spIHtcclxuICAgICAgICAgICAgY29uc3QgdHJhbnNmb3JtQWRkZWRMZW4gPSB0aGlzLl90cmFuc2Zvcm0obmV3UG9zaXRpb24sIGlucHV0VHlwZSwgZGlmZik7XHJcbiAgICAgICAgICAgIG5ld1Bvc2l0aW9uID0gdGhpcy5wb3NpdGlvbkZvck9mZnNldChjYXJldE9mZnNldCArIHRyYW5zZm9ybUFkZGVkTGVuLCB0cnVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQ2FsbGJhY2sobmV3UG9zaXRpb24sIGlucHV0VHlwZSwgZGlmZik7XHJcbiAgICAgICAgcmV0dXJuIGFjUHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICBfdHJhbnNmb3JtKG5ld1Bvc2l0aW9uLCBpbnB1dFR5cGUsIGRpZmYpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLl90cmFuc2Zvcm1DYWxsYmFjayhuZXdQb3NpdGlvbiwgaW5wdXRUeXBlLCBkaWZmKTtcclxuICAgICAgICByZXR1cm4gTnVtYmVyLmlzRmluaXRlKHJlc3VsdCkgPyByZXN1bHQgOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIF9zZXRBY3RpdmVQYXJ0KHBvcywgY2FuT3BlbkF1dG9Db21wbGV0ZSkge1xyXG4gICAgICAgIGNvbnN0IHtpbmRleH0gPSBwb3M7XHJcbiAgICAgICAgY29uc3QgcGFydCA9IHRoaXMuX3BhcnRzW2luZGV4XTtcclxuICAgICAgICBpZiAocGFydCkge1xyXG4gICAgICAgICAgICBpZiAoaW5kZXggIT09IHRoaXMuX2FjdGl2ZVBhcnRJZHgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2FjdGl2ZVBhcnRJZHggPSBpbmRleDtcclxuICAgICAgICAgICAgICAgIGlmIChjYW5PcGVuQXV0b0NvbXBsZXRlICYmIHRoaXMuX2FjdGl2ZVBhcnRJZHggIT09IHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHgpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBlbHNlIHRyeSB0byBjcmVhdGUgb25lXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYWMgPSBwYXJ0LmNyZWF0ZUF1dG9Db21wbGV0ZSh0aGlzLl9vbkF1dG9Db21wbGV0ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFjKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIG1ha2Ugc3VyZSB0aGF0IHJlYWN0IHBpY2tzIHVwIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gYm90aCBhY3NcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlID0gYWM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHggPSBpbmRleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydENvdW50ID0gMTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gbm90IF9hdXRvQ29tcGxldGUsIG9ubHkgdGhlcmUgaWYgYWN0aXZlIHBhcnQgaXMgYXV0b2NvbXBsZXRlIHBhcnRcclxuICAgICAgICAgICAgaWYgKHRoaXMuYXV0b0NvbXBsZXRlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5hdXRvQ29tcGxldGUub25QYXJ0VXBkYXRlKHBhcnQsIHBvcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9hY3RpdmVQYXJ0SWR4ID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRDb3VudCA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25BdXRvQ29tcGxldGUgPSAoe3JlcGxhY2VQYXJ0cywgY2xvc2V9KSA9PiB7XHJcbiAgICAgICAgbGV0IHBvcztcclxuICAgICAgICBpZiAocmVwbGFjZVBhcnRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BhcnRzLnNwbGljZSh0aGlzLl9hdXRvQ29tcGxldGVQYXJ0SWR4LCB0aGlzLl9hdXRvQ29tcGxldGVQYXJ0Q291bnQsIC4uLnJlcGxhY2VQYXJ0cyk7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRDb3VudCA9IHJlcGxhY2VQYXJ0cy5sZW5ndGg7XHJcbiAgICAgICAgICAgIGNvbnN0IGxhc3RQYXJ0ID0gcmVwbGFjZVBhcnRzW3JlcGxhY2VQYXJ0cy5sZW5ndGggLSAxXTtcclxuICAgICAgICAgICAgY29uc3QgbGFzdFBhcnRJbmRleCA9IHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRJZHggKyByZXBsYWNlUGFydHMubGVuZ3RoIC0gMTtcclxuICAgICAgICAgICAgcG9zID0gbmV3IERvY3VtZW50UG9zaXRpb24obGFzdFBhcnRJbmRleCwgbGFzdFBhcnQudGV4dC5sZW5ndGgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY2xvc2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5fYXV0b0NvbXBsZXRlUGFydElkeCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dG9Db21wbGV0ZVBhcnRDb3VudCA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHJlcmVuZGVyIGV2ZW4gaWYgZWRpdG9yIGNvbnRlbnRzIGRpZG4ndCBjaGFuZ2VcclxuICAgICAgICAvLyB0byBtYWtlIHN1cmUgdGhlIE1lc3NhZ2VFZGl0b3IgY2hlY2tzXHJcbiAgICAgICAgLy8gbW9kZWwuYXV0b0NvbXBsZXRlIGJlaW5nIGVtcHR5IGFuZCBjbG9zZXMgaXRcclxuICAgICAgICB0aGlzLl91cGRhdGVDYWxsYmFjayhwb3MpO1xyXG4gICAgfVxyXG5cclxuICAgIF9tZXJnZUFkamFjZW50UGFydHMoKSB7XHJcbiAgICAgICAgbGV0IHByZXZQYXJ0O1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5fcGFydHMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgbGV0IHBhcnQgPSB0aGlzLl9wYXJ0c1tpXTtcclxuICAgICAgICAgICAgY29uc3QgaXNFbXB0eSA9ICFwYXJ0LnRleHQubGVuZ3RoO1xyXG4gICAgICAgICAgICBjb25zdCBpc01lcmdlZCA9ICFpc0VtcHR5ICYmIHByZXZQYXJ0ICYmIHByZXZQYXJ0Lm1lcmdlKHBhcnQpO1xyXG4gICAgICAgICAgICBpZiAoaXNFbXB0eSB8fCBpc01lcmdlZCkge1xyXG4gICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGVtcHR5IG9yIG1lcmdlZCBwYXJ0XHJcbiAgICAgICAgICAgICAgICBwYXJ0ID0gcHJldlBhcnQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yZW1vdmVQYXJ0KGkpO1xyXG4gICAgICAgICAgICAgICAgLy9yZXBlYXQgdGhpcyBpbmRleCwgYXMgaXQncyByZW1vdmVkIG5vd1xyXG4gICAgICAgICAgICAgICAgLS1pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHByZXZQYXJ0ID0gcGFydDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiByZW1vdmVzIGBsZW5gIGFtb3VudCBvZiBjaGFyYWN0ZXJzIGF0IGBwb3NgLlxyXG4gICAgICogQHBhcmFtIHtPYmplY3R9IHBvc1xyXG4gICAgICogQHBhcmFtIHtOdW1iZXJ9IGxlblxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfSBob3cgbWFueSBjaGFyYWN0ZXJzIGJlZm9yZSBwb3Mgd2VyZSBhbHNvIHJlbW92ZWQsXHJcbiAgICAgKiB1c3VhbGx5IGJlY2F1c2Ugb2Ygbm9uLWVkaXRhYmxlIHBhcnRzIHRoYXQgY2FuIG9ubHkgYmUgcmVtb3ZlZCBpbiB0aGVpciBlbnRpcmV0eS5cclxuICAgICAqL1xyXG4gICAgcmVtb3ZlVGV4dChwb3MsIGxlbikge1xyXG4gICAgICAgIGxldCB7aW5kZXgsIG9mZnNldH0gPSBwb3M7XHJcbiAgICAgICAgbGV0IHJlbW92ZWRPZmZzZXREZWNyZWFzZSA9IDA7XHJcbiAgICAgICAgd2hpbGUgKGxlbiA+IDApIHtcclxuICAgICAgICAgICAgLy8gcGFydCBtaWdodCBiZSB1bmRlZmluZWQgaGVyZVxyXG4gICAgICAgICAgICBsZXQgcGFydCA9IHRoaXMuX3BhcnRzW2luZGV4XTtcclxuICAgICAgICAgICAgY29uc3QgYW1vdW50ID0gTWF0aC5taW4obGVuLCBwYXJ0LnRleHQubGVuZ3RoIC0gb2Zmc2V0KTtcclxuICAgICAgICAgICAgLy8gZG9uJ3QgYWxsb3cgMCBhbW91bnQgZGVsZXRpb25zXHJcbiAgICAgICAgICAgIGlmIChhbW91bnQpIHtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJ0LmNhbkVkaXQpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZXBsYWNlV2l0aCA9IHBhcnQucmVtb3ZlKG9mZnNldCwgYW1vdW50KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlcGxhY2VXaXRoID09PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3JlcGxhY2VQYXJ0KGluZGV4LCB0aGlzLl9wYXJ0Q3JlYXRvci5jcmVhdGVEZWZhdWx0UGFydChyZXBsYWNlV2l0aCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBwYXJ0ID0gdGhpcy5fcGFydHNbaW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBlbXB0eSBwYXJ0XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFwYXJ0LnRleHQubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3JlbW92ZVBhcnQoaW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4ICs9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZW1vdmVkT2Zmc2V0RGVjcmVhc2UgKz0gb2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3JlbW92ZVBhcnQoaW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaW5kZXggKz0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsZW4gLT0gYW1vdW50O1xyXG4gICAgICAgICAgICBvZmZzZXQgPSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVtb3ZlZE9mZnNldERlY3JlYXNlO1xyXG4gICAgfVxyXG4gICAgLy8gcmV0dXJuIHBhcnQgaW5kZXggd2hlcmUgaW5zZXJ0aW9uIHdpbGwgaW5zZXJ0IGJldHdlZW4gYXQgb2Zmc2V0XHJcbiAgICBfc3BsaXRBdChwb3MpIHtcclxuICAgICAgICBpZiAocG9zLmluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHBvcy5vZmZzZXQgPT09IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBvcy5pbmRleDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgcGFydCA9IHRoaXMuX3BhcnRzW3Bvcy5pbmRleF07XHJcbiAgICAgICAgaWYgKHBvcy5vZmZzZXQgPj0gcGFydC50ZXh0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4gcG9zLmluZGV4ICsgMTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNlY29uZFBhcnQgPSBwYXJ0LnNwbGl0KHBvcy5vZmZzZXQpO1xyXG4gICAgICAgIHRoaXMuX2luc2VydFBhcnQocG9zLmluZGV4ICsgMSwgc2Vjb25kUGFydCk7XHJcbiAgICAgICAgcmV0dXJuIHBvcy5pbmRleCArIDE7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBpbnNlcnRzIGBzdHJgIGludG8gdGhlIG1vZGVsIGF0IGBwb3NgLlxyXG4gICAgICogQHBhcmFtIHtPYmplY3R9IHBvc1xyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHN0clxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGlucHV0VHlwZSB0aGUgc291cmNlIG9mIHRoZSBpbnB1dCwgc2VlIGh0bWwgSW5wdXRFdmVudC5pbnB1dFR5cGVcclxuICAgICAqIEBwYXJhbSB7Ym9vbH0gb3B0aW9ucy52YWxpZGF0ZSBXaGV0aGVyIGNoYXJhY3RlcnMgd2lsbCBiZSB2YWxpZGF0ZWQgYnkgdGhlIHBhcnQuXHJcbiAgICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVmFsaWRhdGluZyBhbGxvd3MgdGhlIGluc2VydGVkIHRleHQgdG8gYmUgcGFyc2VkIGFjY29yZGluZyB0byB0aGUgcGFydCBydWxlcy5cclxuICAgICAqIEByZXR1cm4ge051bWJlcn0gaG93IGZhciBmcm9tIHBvc2l0aW9uIChpbiBjaGFyYWN0ZXJzKSB0aGUgaW5zZXJ0aW9uIGVuZGVkLlxyXG4gICAgICogVGhpcyBjYW4gYmUgbW9yZSB0aGFuIHRoZSBsZW5ndGggb2YgYHN0cmAgd2hlbiBjcm9zc2luZyBub24tZWRpdGFibGUgcGFydHMsIHdoaWNoIGFyZSBza2lwcGVkLlxyXG4gICAgICovXHJcbiAgICBfYWRkVGV4dChwb3MsIHN0ciwgaW5wdXRUeXBlKSB7XHJcbiAgICAgICAgbGV0IHtpbmRleH0gPSBwb3M7XHJcbiAgICAgICAgY29uc3Qge29mZnNldH0gPSBwb3M7XHJcbiAgICAgICAgbGV0IGFkZExlbiA9IHN0ci5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgcGFydCA9IHRoaXMuX3BhcnRzW2luZGV4XTtcclxuICAgICAgICBpZiAocGFydCkge1xyXG4gICAgICAgICAgICBpZiAocGFydC5jYW5FZGl0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFydC52YWxpZGF0ZUFuZEluc2VydChvZmZzZXQsIHN0ciwgaW5wdXRUeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0ciA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNwbGl0UGFydCA9IHBhcnQuc3BsaXQob2Zmc2V0KTtcclxuICAgICAgICAgICAgICAgICAgICBpbmRleCArPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2luc2VydFBhcnQoaW5kZXgsIHNwbGl0UGFydCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAob2Zmc2V0ICE9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBub3QtZWRpdGFibGUgcGFydCwgY2FyZXQgaXMgbm90IGF0IHN0YXJ0LFxyXG4gICAgICAgICAgICAgICAgLy8gc28gaW5zZXJ0IHN0ciBhZnRlciB0aGlzIHBhcnRcclxuICAgICAgICAgICAgICAgIGFkZExlbiArPSBwYXJ0LnRleHQubGVuZ3RoIC0gb2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgaW5kZXggKz0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5kZXggPCAwKSB7XHJcbiAgICAgICAgICAgIC8vIGlmIHBvc2l0aW9uIHdhcyBub3QgZm91bmQgKGluZGV4OiAtMSwgYXMgaGFwcGVucyBmb3IgZW1wdHkgZWRpdG9yKVxyXG4gICAgICAgICAgICAvLyByZXNldCBpdCB0byBpbnNlcnQgYXMgZmlyc3QgcGFydFxyXG4gICAgICAgICAgICBpbmRleCA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHdoaWxlIChzdHIpIHtcclxuICAgICAgICAgICAgY29uc3QgbmV3UGFydCA9IHRoaXMuX3BhcnRDcmVhdG9yLmNyZWF0ZVBhcnRGb3JJbnB1dChzdHIsIGluZGV4LCBpbnB1dFR5cGUpO1xyXG4gICAgICAgICAgICBzdHIgPSBuZXdQYXJ0LmFwcGVuZFVudGlsUmVqZWN0ZWQoc3RyLCBpbnB1dFR5cGUpO1xyXG4gICAgICAgICAgICB0aGlzLl9pbnNlcnRQYXJ0KGluZGV4LCBuZXdQYXJ0KTtcclxuICAgICAgICAgICAgaW5kZXggKz0gMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFkZExlbjtcclxuICAgIH1cclxuXHJcbiAgICBwb3NpdGlvbkZvck9mZnNldCh0b3RhbE9mZnNldCwgYXRQYXJ0RW5kKSB7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRPZmZzZXQgPSAwO1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5fcGFydHMuZmluZEluZGV4KHBhcnQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJ0TGVuID0gcGFydC50ZXh0Lmxlbmd0aDtcclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgKGF0UGFydEVuZCAmJiAoY3VycmVudE9mZnNldCArIHBhcnRMZW4pID49IHRvdGFsT2Zmc2V0KSB8fFxyXG4gICAgICAgICAgICAgICAgKCFhdFBhcnRFbmQgJiYgKGN1cnJlbnRPZmZzZXQgKyBwYXJ0TGVuKSA+IHRvdGFsT2Zmc2V0KVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGN1cnJlbnRPZmZzZXQgKz0gcGFydExlbjtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChpbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UG9zaXRpb25BdEVuZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgRG9jdW1lbnRQb3NpdGlvbihpbmRleCwgdG90YWxPZmZzZXQgLSBjdXJyZW50T2Zmc2V0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdGFydHMgYSByYW5nZSwgd2hpY2ggY2FuIHNwYW4gYWNyb3NzIG11bHRpcGxlIHBhcnRzLCB0byBmaW5kIGFuZCByZXBsYWNlIHRleHQuXHJcbiAgICAgKiBAcGFyYW0ge0RvY3VtZW50UG9zaXRpb259IHBvc2l0aW9uQSBhIGJvdW5kYXJ5IG9mIHRoZSByYW5nZVxyXG4gICAgICogQHBhcmFtIHtEb2N1bWVudFBvc2l0aW9uP30gcG9zaXRpb25CIHRoZSBvdGhlciBib3VuZGFyeSBvZiB0aGUgcmFuZ2UsIG9wdGlvbmFsXHJcbiAgICAgKiBAcmV0dXJuIHtSYW5nZX1cclxuICAgICAqL1xyXG4gICAgc3RhcnRSYW5nZShwb3NpdGlvbkEsIHBvc2l0aW9uQiA9IHBvc2l0aW9uQSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUmFuZ2UodGhpcywgcG9zaXRpb25BLCBwb3NpdGlvbkIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNhbGxlZCBmcm9tIFJhbmdlLnJlcGxhY2VcclxuICAgIF9yZXBsYWNlUmFuZ2Uoc3RhcnRQb3NpdGlvbiwgZW5kUG9zaXRpb24sIHBhcnRzKSB7XHJcbiAgICAgICAgLy8gY29udmVydCBlbmQgcG9zaXRpb24gdG8gb2Zmc2V0LCBzbyBpdCBpcyBpbmRlcGVuZGVudCBvZiBob3cgdGhlIGRvY3VtZW50IGlzIHNwbGl0IGludG8gcGFydHNcclxuICAgICAgICAvLyB3aGljaCB3ZSdsbCBjaGFuZ2Ugd2hlbiBzcGxpdHRpbmcgdXAgYXQgdGhlIHN0YXJ0IHBvc2l0aW9uXHJcbiAgICAgICAgY29uc3QgZW5kT2Zmc2V0ID0gZW5kUG9zaXRpb24uYXNPZmZzZXQodGhpcyk7XHJcbiAgICAgICAgY29uc3QgbmV3U3RhcnRQYXJ0SW5kZXggPSB0aGlzLl9zcGxpdEF0KHN0YXJ0UG9zaXRpb24pO1xyXG4gICAgICAgIC8vIGNvbnZlcnQgaXQgYmFjayB0byBwb3NpdGlvbiBvbmNlIHNwbGl0IGF0IHN0YXJ0XHJcbiAgICAgICAgZW5kUG9zaXRpb24gPSBlbmRPZmZzZXQuYXNQb3NpdGlvbih0aGlzKTtcclxuICAgICAgICBjb25zdCBuZXdFbmRQYXJ0SW5kZXggPSB0aGlzLl9zcGxpdEF0KGVuZFBvc2l0aW9uKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gbmV3RW5kUGFydEluZGV4IC0gMTsgaSA+PSBuZXdTdGFydFBhcnRJbmRleDsgLS1pKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbW92ZVBhcnQoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBpbnNlcnRJZHggPSBuZXdTdGFydFBhcnRJbmRleDtcclxuICAgICAgICBmb3IgKGNvbnN0IHBhcnQgb2YgcGFydHMpIHtcclxuICAgICAgICAgICAgdGhpcy5faW5zZXJ0UGFydChpbnNlcnRJZHgsIHBhcnQpO1xyXG4gICAgICAgICAgICBpbnNlcnRJZHggKz0gMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbWVyZ2VBZGphY2VudFBhcnRzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQZXJmb3JtcyBhIHRyYW5zZm9ybWF0aW9uIG5vdCBwYXJ0IG9mIGFuIHVwZGF0ZSBjeWNsZS5cclxuICAgICAqIE1vZGlmeWluZyB0aGUgbW9kZWwgc2hvdWxkIG9ubHkgaGFwcGVuIGluc2lkZSBhIHRyYW5zZm9ybSBjYWxsIGlmIG5vdCBwYXJ0IG9mIGFuIHVwZGF0ZSBjYWxsLlxyXG4gICAgICogQHBhcmFtIHtNYW51YWxUcmFuc2Zvcm1DYWxsYmFja30gY2FsbGJhY2sgdG8gcnVuIHRoZSB0cmFuc2Zvcm1hdGlvbnMgaW5cclxuICAgICAqIEByZXR1cm4ge1Byb21pc2V9IGEgcHJvbWlzZSB3aGVuIGF1dG8tY29tcGxldGUgKGlmIGFwcGxpY2FibGUpIGlzIGRvbmUgdXBkYXRpbmdcclxuICAgICAqL1xyXG4gICAgdHJhbnNmb3JtKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY29uc3QgcG9zID0gY2FsbGJhY2soKTtcclxuICAgICAgICBsZXQgYWNQcm9taXNlID0gbnVsbDtcclxuICAgICAgICBpZiAoIShwb3MgaW5zdGFuY2VvZiBSYW5nZSkpIHtcclxuICAgICAgICAgICAgYWNQcm9taXNlID0gdGhpcy5fc2V0QWN0aXZlUGFydChwb3MsIHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGFjUHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91cGRhdGVDYWxsYmFjayhwb3MpO1xyXG4gICAgICAgIHJldHVybiBhY1Byb21pc2U7XHJcbiAgICB9XHJcbn1cclxuIl19