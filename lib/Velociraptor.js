"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _velocityAnimate = _interopRequireDefault(require("velocity-animate"));

var _propTypes = _interopRequireDefault(require("prop-types"));

/**
 * The Velociraptor contains components and animates transitions with velocity.
 * It will only pick up direct changes to properties ('left', currently), and so
 * will not work for animating positional changes where the position is implicit
 * from DOM order. This makes it a lot simpler and lighter: if you need fully
 * automatic positional animation, look at react-shuffle or similar libraries.
 */
class Velociraptor extends _react.default.Component {
  constructor(props) {
    super(props);
    this.nodes = {};

    this._updateChildren(this.props.children);
  }

  componentDidUpdate() {
    this._updateChildren(this.props.children);
  }

  _updateChildren(newChildren) {
    const oldChildren = this.children || {};
    this.children = {};

    _react.default.Children.toArray(newChildren).forEach(c => {
      if (oldChildren[c.key]) {
        const old = oldChildren[c.key];

        const oldNode = _reactDom.default.findDOMNode(this.nodes[old.key]);

        if (oldNode && oldNode.style.left !== c.props.style.left) {
          (0, _velocityAnimate.default)(oldNode, {
            left: c.props.style.left
          }, this.props.transition).then(() => {
            // special case visibility because it's nonsensical to animate an invisible element
            // so we always hidden->visible pre-transition and visible->hidden after
            if (oldNode.style.visibility === 'visible' && c.props.style.visibility === 'hidden') {
              oldNode.style.visibility = c.props.style.visibility;
            }
          }); //console.log("translation: "+oldNode.style.left+" -> "+c.props.style.left);
        }

        if (oldNode && oldNode.style.visibility === 'hidden' && c.props.style.visibility === 'visible') {
          oldNode.style.visibility = c.props.style.visibility;
        } // clone the old element with the props (and children) of the new element
        // so prop updates are still received by the children.


        this.children[c.key] = _react.default.cloneElement(old, c.props, c.props.children);
      } else {
        // new element. If we have a startStyle, use that as the style and go through
        // the enter animations
        const newProps = {};
        const restingStyle = c.props.style;
        const startStyles = this.props.startStyles;

        if (startStyles.length > 0) {
          const startStyle = startStyles[0];
          newProps.style = startStyle; // console.log("mounted@startstyle0: "+JSON.stringify(startStyle));
        }

        newProps.ref = n => this._collectNode(c.key, n, restingStyle);

        this.children[c.key] = _react.default.cloneElement(c, newProps);
      }
    });
  }

  _collectNode(k, node, restingStyle) {
    if (node && this.nodes[k] === undefined && this.props.startStyles.length > 0) {
      const startStyles = this.props.startStyles;
      const transitionOpts = this.props.enterTransitionOpts;

      const domNode = _reactDom.default.findDOMNode(node); // start from startStyle 1: 0 is the one we gave it
      // to start with, so now we animate 1 etc.


      for (var i = 1; i < startStyles.length; ++i) {
        (0, _velocityAnimate.default)(domNode, startStyles[i], transitionOpts[i - 1]);
        /*
        console.log("start:",
                    JSON.stringify(transitionOpts[i-1]),
                    "->",
                    JSON.stringify(startStyles[i]),
                    );
        */
      } // and then we animate to the resting state


      (0, _velocityAnimate.default)(domNode, restingStyle, transitionOpts[i - 1]).then(() => {
        // once we've reached the resting state, hide the element if
        // appropriate
        domNode.style.visibility = restingStyle.visibility;
      });
      /*
      console.log("enter:",
                  JSON.stringify(transitionOpts[i-1]),
                  "->",
                  JSON.stringify(restingStyle));
      */
    } else if (node === null) {
      // Velocity stores data on elements using the jQuery .data()
      // method, and assumes you'll be using jQuery's .remove() to
      // remove the element, but we don't use jQuery, so we need to
      // blow away the element's data explicitly otherwise it will leak.
      // This uses Velocity's internal jQuery compatible wrapper.
      // See the bug at
      // https://github.com/julianshapiro/velocity/issues/300
      // and the FAQ entry, "Preventing memory leaks when
      // creating/destroying large numbers of elements"
      // (https://github.com/julianshapiro/velocity/issues/47)
      const domNode = _reactDom.default.findDOMNode(this.nodes[k]);

      if (domNode) _velocityAnimate.default.Utilities.removeData(domNode);
    }

    this.nodes[k] = node;
  }

  render() {
    return _react.default.createElement("span", null, Object.values(this.children));
  }

}

exports.default = Velociraptor;
(0, _defineProperty2.default)(Velociraptor, "propTypes", {
  // either a list of child nodes, or a single child.
  children: _propTypes.default.any,
  // optional transition information for changing existing children
  transition: _propTypes.default.object,
  // a list of state objects to apply to each child node in turn
  startStyles: _propTypes.default.array,
  // a list of transition options from the corresponding startStyle
  enterTransitionOpts: _propTypes.default.array
});
(0, _defineProperty2.default)(Velociraptor, "defaultProps", {
  startStyles: [],
  enterTransitionOpts: []
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9WZWxvY2lyYXB0b3IuanMiXSwibmFtZXMiOlsiVmVsb2NpcmFwdG9yIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwibm9kZXMiLCJfdXBkYXRlQ2hpbGRyZW4iLCJjaGlsZHJlbiIsImNvbXBvbmVudERpZFVwZGF0ZSIsIm5ld0NoaWxkcmVuIiwib2xkQ2hpbGRyZW4iLCJDaGlsZHJlbiIsInRvQXJyYXkiLCJmb3JFYWNoIiwiYyIsImtleSIsIm9sZCIsIm9sZE5vZGUiLCJSZWFjdERvbSIsImZpbmRET01Ob2RlIiwic3R5bGUiLCJsZWZ0IiwidHJhbnNpdGlvbiIsInRoZW4iLCJ2aXNpYmlsaXR5IiwiY2xvbmVFbGVtZW50IiwibmV3UHJvcHMiLCJyZXN0aW5nU3R5bGUiLCJzdGFydFN0eWxlcyIsImxlbmd0aCIsInN0YXJ0U3R5bGUiLCJyZWYiLCJuIiwiX2NvbGxlY3ROb2RlIiwiayIsIm5vZGUiLCJ1bmRlZmluZWQiLCJ0cmFuc2l0aW9uT3B0cyIsImVudGVyVHJhbnNpdGlvbk9wdHMiLCJkb21Ob2RlIiwiaSIsIlZlbG9jaXR5IiwiVXRpbGl0aWVzIiwicmVtb3ZlRGF0YSIsInJlbmRlciIsIk9iamVjdCIsInZhbHVlcyIsIlByb3BUeXBlcyIsImFueSIsIm9iamVjdCIsImFycmF5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7O0FBT2UsTUFBTUEsWUFBTixTQUEyQkMsZUFBTUMsU0FBakMsQ0FBMkM7QUFvQnREQyxFQUFBQSxXQUFXLENBQUNDLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47QUFFQSxTQUFLQyxLQUFMLEdBQWEsRUFBYjs7QUFDQSxTQUFLQyxlQUFMLENBQXFCLEtBQUtGLEtBQUwsQ0FBV0csUUFBaEM7QUFDSDs7QUFFREMsRUFBQUEsa0JBQWtCLEdBQUc7QUFDakIsU0FBS0YsZUFBTCxDQUFxQixLQUFLRixLQUFMLENBQVdHLFFBQWhDO0FBQ0g7O0FBRURELEVBQUFBLGVBQWUsQ0FBQ0csV0FBRCxFQUFjO0FBQ3pCLFVBQU1DLFdBQVcsR0FBRyxLQUFLSCxRQUFMLElBQWlCLEVBQXJDO0FBQ0EsU0FBS0EsUUFBTCxHQUFnQixFQUFoQjs7QUFDQU4sbUJBQU1VLFFBQU4sQ0FBZUMsT0FBZixDQUF1QkgsV0FBdkIsRUFBb0NJLE9BQXBDLENBQTZDQyxDQUFELElBQU87QUFDL0MsVUFBSUosV0FBVyxDQUFDSSxDQUFDLENBQUNDLEdBQUgsQ0FBZixFQUF3QjtBQUNwQixjQUFNQyxHQUFHLEdBQUdOLFdBQVcsQ0FBQ0ksQ0FBQyxDQUFDQyxHQUFILENBQXZCOztBQUNBLGNBQU1FLE9BQU8sR0FBR0Msa0JBQVNDLFdBQVQsQ0FBcUIsS0FBS2QsS0FBTCxDQUFXVyxHQUFHLENBQUNELEdBQWYsQ0FBckIsQ0FBaEI7O0FBRUEsWUFBSUUsT0FBTyxJQUFJQSxPQUFPLENBQUNHLEtBQVIsQ0FBY0MsSUFBZCxLQUF1QlAsQ0FBQyxDQUFDVixLQUFGLENBQVFnQixLQUFSLENBQWNDLElBQXBELEVBQTBEO0FBQ3RELHdDQUFTSixPQUFULEVBQWtCO0FBQUVJLFlBQUFBLElBQUksRUFBRVAsQ0FBQyxDQUFDVixLQUFGLENBQVFnQixLQUFSLENBQWNDO0FBQXRCLFdBQWxCLEVBQWdELEtBQUtqQixLQUFMLENBQVdrQixVQUEzRCxFQUF1RUMsSUFBdkUsQ0FBNEUsTUFBTTtBQUM5RTtBQUNBO0FBQ0EsZ0JBQUlOLE9BQU8sQ0FBQ0csS0FBUixDQUFjSSxVQUFkLEtBQTZCLFNBQTdCLElBQTBDVixDQUFDLENBQUNWLEtBQUYsQ0FBUWdCLEtBQVIsQ0FBY0ksVUFBZCxLQUE2QixRQUEzRSxFQUFxRjtBQUNqRlAsY0FBQUEsT0FBTyxDQUFDRyxLQUFSLENBQWNJLFVBQWQsR0FBMkJWLENBQUMsQ0FBQ1YsS0FBRixDQUFRZ0IsS0FBUixDQUFjSSxVQUF6QztBQUNIO0FBQ0osV0FORCxFQURzRCxDQVF0RDtBQUNIOztBQUNELFlBQUlQLE9BQU8sSUFBSUEsT0FBTyxDQUFDRyxLQUFSLENBQWNJLFVBQWQsS0FBNkIsUUFBeEMsSUFBb0RWLENBQUMsQ0FBQ1YsS0FBRixDQUFRZ0IsS0FBUixDQUFjSSxVQUFkLEtBQTZCLFNBQXJGLEVBQWdHO0FBQzVGUCxVQUFBQSxPQUFPLENBQUNHLEtBQVIsQ0FBY0ksVUFBZCxHQUEyQlYsQ0FBQyxDQUFDVixLQUFGLENBQVFnQixLQUFSLENBQWNJLFVBQXpDO0FBQ0gsU0FoQm1CLENBaUJwQjtBQUNBOzs7QUFDQSxhQUFLakIsUUFBTCxDQUFjTyxDQUFDLENBQUNDLEdBQWhCLElBQXVCZCxlQUFNd0IsWUFBTixDQUFtQlQsR0FBbkIsRUFBd0JGLENBQUMsQ0FBQ1YsS0FBMUIsRUFBaUNVLENBQUMsQ0FBQ1YsS0FBRixDQUFRRyxRQUF6QyxDQUF2QjtBQUNILE9BcEJELE1Bb0JPO0FBQ0g7QUFDQTtBQUNBLGNBQU1tQixRQUFRLEdBQUcsRUFBakI7QUFDQSxjQUFNQyxZQUFZLEdBQUdiLENBQUMsQ0FBQ1YsS0FBRixDQUFRZ0IsS0FBN0I7QUFFQSxjQUFNUSxXQUFXLEdBQUcsS0FBS3hCLEtBQUwsQ0FBV3dCLFdBQS9COztBQUNBLFlBQUlBLFdBQVcsQ0FBQ0MsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QixnQkFBTUMsVUFBVSxHQUFHRixXQUFXLENBQUMsQ0FBRCxDQUE5QjtBQUNBRixVQUFBQSxRQUFRLENBQUNOLEtBQVQsR0FBaUJVLFVBQWpCLENBRndCLENBR3hCO0FBQ0g7O0FBRURKLFFBQUFBLFFBQVEsQ0FBQ0ssR0FBVCxHQUFpQkMsQ0FBRCxJQUFPLEtBQUtDLFlBQUwsQ0FDbkJuQixDQUFDLENBQUNDLEdBRGlCLEVBQ1ppQixDQURZLEVBQ1RMLFlBRFMsQ0FBdkI7O0FBSUEsYUFBS3BCLFFBQUwsQ0FBY08sQ0FBQyxDQUFDQyxHQUFoQixJQUF1QmQsZUFBTXdCLFlBQU4sQ0FBbUJYLENBQW5CLEVBQXNCWSxRQUF0QixDQUF2QjtBQUNIO0FBQ0osS0F4Q0Q7QUF5Q0g7O0FBRURPLEVBQUFBLFlBQVksQ0FBQ0MsQ0FBRCxFQUFJQyxJQUFKLEVBQVVSLFlBQVYsRUFBd0I7QUFDaEMsUUFDSVEsSUFBSSxJQUNKLEtBQUs5QixLQUFMLENBQVc2QixDQUFYLE1BQWtCRSxTQURsQixJQUVBLEtBQUtoQyxLQUFMLENBQVd3QixXQUFYLENBQXVCQyxNQUF2QixHQUFnQyxDQUhwQyxFQUlFO0FBQ0UsWUFBTUQsV0FBVyxHQUFHLEtBQUt4QixLQUFMLENBQVd3QixXQUEvQjtBQUNBLFlBQU1TLGNBQWMsR0FBRyxLQUFLakMsS0FBTCxDQUFXa0MsbUJBQWxDOztBQUNBLFlBQU1DLE9BQU8sR0FBR3JCLGtCQUFTQyxXQUFULENBQXFCZ0IsSUFBckIsQ0FBaEIsQ0FIRixDQUlFO0FBQ0E7OztBQUNBLFdBQUssSUFBSUssQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1osV0FBVyxDQUFDQyxNQUFoQyxFQUF3QyxFQUFFVyxDQUExQyxFQUE2QztBQUN6QyxzQ0FBU0QsT0FBVCxFQUFrQlgsV0FBVyxDQUFDWSxDQUFELENBQTdCLEVBQWtDSCxjQUFjLENBQUNHLENBQUMsR0FBQyxDQUFILENBQWhEO0FBQ0E7Ozs7Ozs7QUFPSCxPQWZILENBaUJFOzs7QUFDQSxvQ0FBU0QsT0FBVCxFQUFrQlosWUFBbEIsRUFDSVUsY0FBYyxDQUFDRyxDQUFDLEdBQUMsQ0FBSCxDQURsQixFQUVLakIsSUFGTCxDQUVVLE1BQU07QUFDUjtBQUNBO0FBQ0FnQixRQUFBQSxPQUFPLENBQUNuQixLQUFSLENBQWNJLFVBQWQsR0FBMkJHLFlBQVksQ0FBQ0gsVUFBeEM7QUFDSCxPQU5MO0FBUUE7Ozs7OztBQU1ILEtBcENELE1Bb0NPLElBQUlXLElBQUksS0FBSyxJQUFiLEVBQW1CO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBTUksT0FBTyxHQUFHckIsa0JBQVNDLFdBQVQsQ0FBcUIsS0FBS2QsS0FBTCxDQUFXNkIsQ0FBWCxDQUFyQixDQUFoQjs7QUFDQSxVQUFJSyxPQUFKLEVBQWFFLHlCQUFTQyxTQUFULENBQW1CQyxVQUFuQixDQUE4QkosT0FBOUI7QUFDaEI7O0FBQ0QsU0FBS2xDLEtBQUwsQ0FBVzZCLENBQVgsSUFBZ0JDLElBQWhCO0FBQ0g7O0FBRURTLEVBQUFBLE1BQU0sR0FBRztBQUNMLFdBQ0ksMkNBQ01DLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEtBQUt2QyxRQUFuQixDQUROLENBREo7QUFLSDs7QUF6SXFEOzs7OEJBQXJDUCxZLGVBQ0U7QUFDZjtBQUNBTyxFQUFBQSxRQUFRLEVBQUV3QyxtQkFBVUMsR0FGTDtBQUlmO0FBQ0ExQixFQUFBQSxVQUFVLEVBQUV5QixtQkFBVUUsTUFMUDtBQU9mO0FBQ0FyQixFQUFBQSxXQUFXLEVBQUVtQixtQkFBVUcsS0FSUjtBQVVmO0FBQ0FaLEVBQUFBLG1CQUFtQixFQUFFUyxtQkFBVUc7QUFYaEIsQzs4QkFERmxELFksa0JBZUs7QUFDbEI0QixFQUFBQSxXQUFXLEVBQUUsRUFESztBQUVsQlUsRUFBQUEsbUJBQW1CLEVBQUU7QUFGSCxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUmVhY3REb20gZnJvbSBcInJlYWN0LWRvbVwiO1xyXG5pbXBvcnQgVmVsb2NpdHkgZnJvbSBcInZlbG9jaXR5LWFuaW1hdGVcIjtcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuXHJcbi8qKlxyXG4gKiBUaGUgVmVsb2NpcmFwdG9yIGNvbnRhaW5zIGNvbXBvbmVudHMgYW5kIGFuaW1hdGVzIHRyYW5zaXRpb25zIHdpdGggdmVsb2NpdHkuXHJcbiAqIEl0IHdpbGwgb25seSBwaWNrIHVwIGRpcmVjdCBjaGFuZ2VzIHRvIHByb3BlcnRpZXMgKCdsZWZ0JywgY3VycmVudGx5KSwgYW5kIHNvXHJcbiAqIHdpbGwgbm90IHdvcmsgZm9yIGFuaW1hdGluZyBwb3NpdGlvbmFsIGNoYW5nZXMgd2hlcmUgdGhlIHBvc2l0aW9uIGlzIGltcGxpY2l0XHJcbiAqIGZyb20gRE9NIG9yZGVyLiBUaGlzIG1ha2VzIGl0IGEgbG90IHNpbXBsZXIgYW5kIGxpZ2h0ZXI6IGlmIHlvdSBuZWVkIGZ1bGx5XHJcbiAqIGF1dG9tYXRpYyBwb3NpdGlvbmFsIGFuaW1hdGlvbiwgbG9vayBhdCByZWFjdC1zaHVmZmxlIG9yIHNpbWlsYXIgbGlicmFyaWVzLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVmVsb2NpcmFwdG9yIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIHN0YXRpYyBwcm9wVHlwZXMgPSB7XHJcbiAgICAgICAgLy8gZWl0aGVyIGEgbGlzdCBvZiBjaGlsZCBub2Rlcywgb3IgYSBzaW5nbGUgY2hpbGQuXHJcbiAgICAgICAgY2hpbGRyZW46IFByb3BUeXBlcy5hbnksXHJcblxyXG4gICAgICAgIC8vIG9wdGlvbmFsIHRyYW5zaXRpb24gaW5mb3JtYXRpb24gZm9yIGNoYW5naW5nIGV4aXN0aW5nIGNoaWxkcmVuXHJcbiAgICAgICAgdHJhbnNpdGlvbjogUHJvcFR5cGVzLm9iamVjdCxcclxuXHJcbiAgICAgICAgLy8gYSBsaXN0IG9mIHN0YXRlIG9iamVjdHMgdG8gYXBwbHkgdG8gZWFjaCBjaGlsZCBub2RlIGluIHR1cm5cclxuICAgICAgICBzdGFydFN0eWxlczogUHJvcFR5cGVzLmFycmF5LFxyXG5cclxuICAgICAgICAvLyBhIGxpc3Qgb2YgdHJhbnNpdGlvbiBvcHRpb25zIGZyb20gdGhlIGNvcnJlc3BvbmRpbmcgc3RhcnRTdHlsZVxyXG4gICAgICAgIGVudGVyVHJhbnNpdGlvbk9wdHM6IFByb3BUeXBlcy5hcnJheSxcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcclxuICAgICAgICBzdGFydFN0eWxlczogW10sXHJcbiAgICAgICAgZW50ZXJUcmFuc2l0aW9uT3B0czogW10sXHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG5cclxuICAgICAgICB0aGlzLm5vZGVzID0ge307XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlQ2hpbGRyZW4odGhpcy5wcm9wcy5jaGlsZHJlbik7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50RGlkVXBkYXRlKCkge1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZUNoaWxkcmVuKHRoaXMucHJvcHMuY2hpbGRyZW4pO1xyXG4gICAgfVxyXG5cclxuICAgIF91cGRhdGVDaGlsZHJlbihuZXdDaGlsZHJlbikge1xyXG4gICAgICAgIGNvbnN0IG9sZENoaWxkcmVuID0gdGhpcy5jaGlsZHJlbiB8fCB7fTtcclxuICAgICAgICB0aGlzLmNoaWxkcmVuID0ge307XHJcbiAgICAgICAgUmVhY3QuQ2hpbGRyZW4udG9BcnJheShuZXdDaGlsZHJlbikuZm9yRWFjaCgoYykgPT4ge1xyXG4gICAgICAgICAgICBpZiAob2xkQ2hpbGRyZW5bYy5rZXldKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBvbGQgPSBvbGRDaGlsZHJlbltjLmtleV07XHJcbiAgICAgICAgICAgICAgICBjb25zdCBvbGROb2RlID0gUmVhY3REb20uZmluZERPTU5vZGUodGhpcy5ub2Rlc1tvbGQua2V5XSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKG9sZE5vZGUgJiYgb2xkTm9kZS5zdHlsZS5sZWZ0ICE9PSBjLnByb3BzLnN0eWxlLmxlZnQpIHtcclxuICAgICAgICAgICAgICAgICAgICBWZWxvY2l0eShvbGROb2RlLCB7IGxlZnQ6IGMucHJvcHMuc3R5bGUubGVmdCB9LCB0aGlzLnByb3BzLnRyYW5zaXRpb24pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBzcGVjaWFsIGNhc2UgdmlzaWJpbGl0eSBiZWNhdXNlIGl0J3Mgbm9uc2Vuc2ljYWwgdG8gYW5pbWF0ZSBhbiBpbnZpc2libGUgZWxlbWVudFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBzbyB3ZSBhbHdheXMgaGlkZGVuLT52aXNpYmxlIHByZS10cmFuc2l0aW9uIGFuZCB2aXNpYmxlLT5oaWRkZW4gYWZ0ZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9sZE5vZGUuc3R5bGUudmlzaWJpbGl0eSA9PT0gJ3Zpc2libGUnICYmIGMucHJvcHMuc3R5bGUudmlzaWJpbGl0eSA9PT0gJ2hpZGRlbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9sZE5vZGUuc3R5bGUudmlzaWJpbGl0eSA9IGMucHJvcHMuc3R5bGUudmlzaWJpbGl0eTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJ0cmFuc2xhdGlvbjogXCIrb2xkTm9kZS5zdHlsZS5sZWZ0K1wiIC0+IFwiK2MucHJvcHMuc3R5bGUubGVmdCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAob2xkTm9kZSAmJiBvbGROb2RlLnN0eWxlLnZpc2liaWxpdHkgPT09ICdoaWRkZW4nICYmIGMucHJvcHMuc3R5bGUudmlzaWJpbGl0eSA9PT0gJ3Zpc2libGUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2xkTm9kZS5zdHlsZS52aXNpYmlsaXR5ID0gYy5wcm9wcy5zdHlsZS52aXNpYmlsaXR5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gY2xvbmUgdGhlIG9sZCBlbGVtZW50IHdpdGggdGhlIHByb3BzIChhbmQgY2hpbGRyZW4pIG9mIHRoZSBuZXcgZWxlbWVudFxyXG4gICAgICAgICAgICAgICAgLy8gc28gcHJvcCB1cGRhdGVzIGFyZSBzdGlsbCByZWNlaXZlZCBieSB0aGUgY2hpbGRyZW4uXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoaWxkcmVuW2Mua2V5XSA9IFJlYWN0LmNsb25lRWxlbWVudChvbGQsIGMucHJvcHMsIGMucHJvcHMuY2hpbGRyZW4pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gbmV3IGVsZW1lbnQuIElmIHdlIGhhdmUgYSBzdGFydFN0eWxlLCB1c2UgdGhhdCBhcyB0aGUgc3R5bGUgYW5kIGdvIHRocm91Z2hcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBlbnRlciBhbmltYXRpb25zXHJcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdQcm9wcyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVzdGluZ1N0eWxlID0gYy5wcm9wcy5zdHlsZTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBzdGFydFN0eWxlcyA9IHRoaXMucHJvcHMuc3RhcnRTdHlsZXM7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3RhcnRTdHlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHN0YXJ0U3R5bGUgPSBzdGFydFN0eWxlc1swXTtcclxuICAgICAgICAgICAgICAgICAgICBuZXdQcm9wcy5zdHlsZSA9IHN0YXJ0U3R5bGU7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJtb3VudGVkQHN0YXJ0c3R5bGUwOiBcIitKU09OLnN0cmluZ2lmeShzdGFydFN0eWxlKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgbmV3UHJvcHMucmVmID0gKChuKSA9PiB0aGlzLl9jb2xsZWN0Tm9kZShcclxuICAgICAgICAgICAgICAgICAgICBjLmtleSwgbiwgcmVzdGluZ1N0eWxlLFxyXG4gICAgICAgICAgICAgICAgKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGlsZHJlbltjLmtleV0gPSBSZWFjdC5jbG9uZUVsZW1lbnQoYywgbmV3UHJvcHMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2NvbGxlY3ROb2RlKGssIG5vZGUsIHJlc3RpbmdTdHlsZSkge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgbm9kZSAmJlxyXG4gICAgICAgICAgICB0aGlzLm5vZGVzW2tdID09PSB1bmRlZmluZWQgJiZcclxuICAgICAgICAgICAgdGhpcy5wcm9wcy5zdGFydFN0eWxlcy5sZW5ndGggPiAwXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0U3R5bGVzID0gdGhpcy5wcm9wcy5zdGFydFN0eWxlcztcclxuICAgICAgICAgICAgY29uc3QgdHJhbnNpdGlvbk9wdHMgPSB0aGlzLnByb3BzLmVudGVyVHJhbnNpdGlvbk9wdHM7XHJcbiAgICAgICAgICAgIGNvbnN0IGRvbU5vZGUgPSBSZWFjdERvbS5maW5kRE9NTm9kZShub2RlKTtcclxuICAgICAgICAgICAgLy8gc3RhcnQgZnJvbSBzdGFydFN0eWxlIDE6IDAgaXMgdGhlIG9uZSB3ZSBnYXZlIGl0XHJcbiAgICAgICAgICAgIC8vIHRvIHN0YXJ0IHdpdGgsIHNvIG5vdyB3ZSBhbmltYXRlIDEgZXRjLlxyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHN0YXJ0U3R5bGVzLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBWZWxvY2l0eShkb21Ob2RlLCBzdGFydFN0eWxlc1tpXSwgdHJhbnNpdGlvbk9wdHNbaS0xXSk7XHJcbiAgICAgICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJzdGFydDpcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KHRyYW5zaXRpb25PcHRzW2ktMV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCItPlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkoc3RhcnRTdHlsZXNbaV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGFuZCB0aGVuIHdlIGFuaW1hdGUgdG8gdGhlIHJlc3Rpbmcgc3RhdGVcclxuICAgICAgICAgICAgVmVsb2NpdHkoZG9tTm9kZSwgcmVzdGluZ1N0eWxlLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbk9wdHNbaS0xXSlcclxuICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBvbmNlIHdlJ3ZlIHJlYWNoZWQgdGhlIHJlc3Rpbmcgc3RhdGUsIGhpZGUgdGhlIGVsZW1lbnQgaWZcclxuICAgICAgICAgICAgICAgICAgICAvLyBhcHByb3ByaWF0ZVxyXG4gICAgICAgICAgICAgICAgICAgIGRvbU5vZGUuc3R5bGUudmlzaWJpbGl0eSA9IHJlc3RpbmdTdHlsZS52aXNpYmlsaXR5O1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImVudGVyOlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh0cmFuc2l0aW9uT3B0c1tpLTFdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCItPlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShyZXN0aW5nU3R5bGUpKTtcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICB9IGVsc2UgaWYgKG5vZGUgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgLy8gVmVsb2NpdHkgc3RvcmVzIGRhdGEgb24gZWxlbWVudHMgdXNpbmcgdGhlIGpRdWVyeSAuZGF0YSgpXHJcbiAgICAgICAgICAgIC8vIG1ldGhvZCwgYW5kIGFzc3VtZXMgeW91J2xsIGJlIHVzaW5nIGpRdWVyeSdzIC5yZW1vdmUoKSB0b1xyXG4gICAgICAgICAgICAvLyByZW1vdmUgdGhlIGVsZW1lbnQsIGJ1dCB3ZSBkb24ndCB1c2UgalF1ZXJ5LCBzbyB3ZSBuZWVkIHRvXHJcbiAgICAgICAgICAgIC8vIGJsb3cgYXdheSB0aGUgZWxlbWVudCdzIGRhdGEgZXhwbGljaXRseSBvdGhlcndpc2UgaXQgd2lsbCBsZWFrLlxyXG4gICAgICAgICAgICAvLyBUaGlzIHVzZXMgVmVsb2NpdHkncyBpbnRlcm5hbCBqUXVlcnkgY29tcGF0aWJsZSB3cmFwcGVyLlxyXG4gICAgICAgICAgICAvLyBTZWUgdGhlIGJ1ZyBhdFxyXG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vanVsaWFuc2hhcGlyby92ZWxvY2l0eS9pc3N1ZXMvMzAwXHJcbiAgICAgICAgICAgIC8vIGFuZCB0aGUgRkFRIGVudHJ5LCBcIlByZXZlbnRpbmcgbWVtb3J5IGxlYWtzIHdoZW5cclxuICAgICAgICAgICAgLy8gY3JlYXRpbmcvZGVzdHJveWluZyBsYXJnZSBudW1iZXJzIG9mIGVsZW1lbnRzXCJcclxuICAgICAgICAgICAgLy8gKGh0dHBzOi8vZ2l0aHViLmNvbS9qdWxpYW5zaGFwaXJvL3ZlbG9jaXR5L2lzc3Vlcy80NylcclxuICAgICAgICAgICAgY29uc3QgZG9tTm9kZSA9IFJlYWN0RG9tLmZpbmRET01Ob2RlKHRoaXMubm9kZXNba10pO1xyXG4gICAgICAgICAgICBpZiAoZG9tTm9kZSkgVmVsb2NpdHkuVXRpbGl0aWVzLnJlbW92ZURhdGEoZG9tTm9kZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubm9kZXNba10gPSBub2RlO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8c3Bhbj5cclxuICAgICAgICAgICAgICAgIHsgT2JqZWN0LnZhbHVlcyh0aGlzLmNoaWxkcmVuKSB9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==