"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

/*
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const RoomContext = (0, _react.createContext)({
  canReact: undefined,
  canReply: undefined,
  room: undefined
});
RoomContext.displayName = "RoomContext";
var _default = RoomContext;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb250ZXh0cy9Sb29tQ29udGV4dC5qcyJdLCJuYW1lcyI6WyJSb29tQ29udGV4dCIsImNhblJlYWN0IiwidW5kZWZpbmVkIiwiY2FuUmVwbHkiLCJyb29tIiwiZGlzcGxheU5hbWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFnQkE7O0FBaEJBOzs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsTUFBTUEsV0FBVyxHQUFHLDBCQUFjO0FBQzlCQyxFQUFBQSxRQUFRLEVBQUVDLFNBRG9CO0FBRTlCQyxFQUFBQSxRQUFRLEVBQUVELFNBRm9CO0FBRzlCRSxFQUFBQSxJQUFJLEVBQUVGO0FBSHdCLENBQWQsQ0FBcEI7QUFLQUYsV0FBVyxDQUFDSyxXQUFaLEdBQTBCLGFBQTFCO2VBQ2VMLFciLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB7IGNyZWF0ZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcclxuXHJcbmNvbnN0IFJvb21Db250ZXh0ID0gY3JlYXRlQ29udGV4dCh7XHJcbiAgICBjYW5SZWFjdDogdW5kZWZpbmVkLFxyXG4gICAgY2FuUmVwbHk6IHVuZGVmaW5lZCxcclxuICAgIHJvb206IHVuZGVmaW5lZCxcclxufSk7XHJcblJvb21Db250ZXh0LmRpc3BsYXlOYW1lID0gXCJSb29tQ29udGV4dFwiO1xyXG5leHBvcnQgZGVmYXVsdCBSb29tQ29udGV4dDtcclxuIl19