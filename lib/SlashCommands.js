"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCommand = getCommand;
exports.CommandMap = exports.Commands = exports.CommandCategories = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var React = _interopRequireWildcard(require("react"));

var _MatrixClientPeg = require("./MatrixClientPeg");

var _dispatcher = _interopRequireDefault(require("./dispatcher"));

var sdk = _interopRequireWildcard(require("./index"));

var _languageHandler = require("./languageHandler");

var _Modal = _interopRequireDefault(require("./Modal"));

var _MultiInviter = _interopRequireDefault(require("./utils/MultiInviter"));

var _HtmlUtils = require("./HtmlUtils");

var _QuestionDialog = _interopRequireDefault(require("./components/views/dialogs/QuestionDialog"));

var _WidgetUtils = _interopRequireDefault(require("./utils/WidgetUtils"));

var _colour = require("./utils/colour");

var _UserAddress = require("./UserAddress");

var _UrlUtils = require("./utils/UrlUtils");

var _IdentityServerUtils = require("./utils/IdentityServerUtils");

var _Permalinks = require("./utils/permalinks/Permalinks");

var _RoomInvite = require("./RoomInvite");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

const singleMxcUpload = async () =>
/*: Promise<any>*/
{
  return new Promise(resolve => {
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');

    fileSelector.onchange = (ev
    /*: HTMLInputEvent*/
    ) => {
      const file = ev.target.files[0];
      const UploadConfirmDialog = sdk.getComponent("dialogs.UploadConfirmDialog");

      _Modal.default.createTrackedDialog('Upload Files confirmation', '', UploadConfirmDialog, {
        file,
        onFinished: shouldContinue => {
          resolve(shouldContinue ? _MatrixClientPeg.MatrixClientPeg.get().uploadContent(file) : null);
        }
      });
    };

    fileSelector.click();
  });
};

const CommandCategories = {
  "messages": (0, _languageHandler._td)("Messages"),
  "actions": (0, _languageHandler._td)("Actions"),
  "admin": (0, _languageHandler._td)("Admin"),
  "advanced": (0, _languageHandler._td)("Advanced"),
  "other": (0, _languageHandler._td)("Other")
};
exports.CommandCategories = CommandCategories;

class Command {
  constructor(opts
  /*: ICommandOpts*/
  ) {
    (0, _defineProperty2.default)(this, "command", void 0);
    (0, _defineProperty2.default)(this, "aliases", void 0);
    (0, _defineProperty2.default)(this, "args", void 0);
    (0, _defineProperty2.default)(this, "description", void 0);
    (0, _defineProperty2.default)(this, "runFn", void 0);
    (0, _defineProperty2.default)(this, "category", void 0);
    (0, _defineProperty2.default)(this, "hideCompletionAfterSpace", void 0);
    this.command = opts.command;
    this.aliases = opts.aliases || [];
    this.args = opts.args || "";
    this.description = opts.description;
    this.runFn = opts.runFn;
    this.category = opts.category || CommandCategories.other;
    this.hideCompletionAfterSpace = opts.hideCompletionAfterSpace || false;
  }

  getCommand() {
    return "/".concat(this.command);
  }

  getCommandWithArgs() {
    return this.getCommand() + " " + this.args;
  }

  run(roomId
  /*: string*/
  , args
  /*: string*/
  , cmd
  /*: string*/
  ) {
    // if it has no runFn then its an ignored/nop command (autocomplete only) e.g `/me`
    if (!this.runFn) return;
    return this.runFn.bind(this)(roomId, args, cmd);
  }

  getUsage() {
    return (0, _languageHandler._t)('Usage') + ': ' + this.getCommandWithArgs();
  }

}

function reject(error) {
  return {
    error
  };
}

function success(promise
/*: Promise<any>*/
) {
  return {
    promise
  };
}
/* Disable the "unexpected this" error for these commands - all of the run
 * functions are called with `this` bound to the Command instance.
 */


const Commands = [new Command({
  command: 'shrug',
  args: '<message>',
  description: (0, _languageHandler._td)('Prepends ¯\\_(ツ)_/¯ to a plain-text message'),
  runFn: function (roomId, args) {
    let message = '¯\\_(ツ)_/¯';

    if (args) {
      message = message + ' ' + args;
    }

    return success(_MatrixClientPeg.MatrixClientPeg.get().sendTextMessage(roomId, message));
  },
  category: CommandCategories.messages
}), new Command({
  command: 'plain',
  args: '<message>',
  description: (0, _languageHandler._td)('Sends a message as plain text, without interpreting it as markdown'),
  runFn: function (roomId, messages) {
    return success(_MatrixClientPeg.MatrixClientPeg.get().sendTextMessage(roomId, messages));
  },
  category: CommandCategories.messages
}), new Command({
  command: 'html',
  args: '<message>',
  description: (0, _languageHandler._td)('Sends a message as html, without interpreting it as markdown'),
  runFn: function (roomId, messages) {
    return success(_MatrixClientPeg.MatrixClientPeg.get().sendHtmlMessage(roomId, messages, messages));
  },
  category: CommandCategories.messages
}), new Command({
  command: 'ddg',
  args: '<query>',
  description: (0, _languageHandler._td)('Searches DuckDuckGo for results'),
  runFn: function () {
    const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog'); // TODO Don't explain this away, actually show a search UI here.

    _Modal.default.createTrackedDialog('Slash Commands', '/ddg is not a command', ErrorDialog, {
      title: (0, _languageHandler._t)('/ddg is not a command'),
      description: (0, _languageHandler._t)('To use it, just wait for autocomplete results to load and tab through them.')
    });

    return success();
  },
  category: CommandCategories.actions,
  hideCompletionAfterSpace: true
}), new Command({
  command: 'upgraderoom',
  args: '<new_version>',
  description: (0, _languageHandler._td)('Upgrades a room to a new version'),
  runFn: function (roomId, args) {
    if (args) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const room = cli.getRoom(roomId);

      if (!room.currentState.mayClientSendStateEvent("m.room.tombstone", cli)) {
        return reject((0, _languageHandler._t)("You do not have the required permissions to use this command."));
      }

      const RoomUpgradeWarningDialog = sdk.getComponent("dialogs.RoomUpgradeWarningDialog");

      const {
        finished
      } = _Modal.default.createTrackedDialog('Slash Commands', 'upgrade room confirmation', RoomUpgradeWarningDialog, {
        roomId: roomId,
        targetVersion: args
      },
      /*className=*/
      null,
      /*isPriority=*/
      false,
      /*isStatic=*/
      true);

      return success(finished.then(async ([resp]) => {
        if (!resp.continue) return;
        let checkForUpgradeFn;

        try {
          const upgradePromise = cli.upgradeRoom(roomId, args); // We have to wait for the js-sdk to give us the room back so
          // we can more effectively abuse the MultiInviter behaviour
          // which heavily relies on the Room object being available.

          if (resp.invite) {
            checkForUpgradeFn = async newRoom => {
              // The upgradePromise should be done by the time we await it here.
              const {
                replacement_room: newRoomId
              } = await upgradePromise;
              if (newRoom.roomId !== newRoomId) return;
              const toInvite = [...room.getMembersWithMembership("join"), ...room.getMembersWithMembership("invite")].map(m => m.userId).filter(m => m !== cli.getUserId());

              if (toInvite.length > 0) {
                // Errors are handled internally to this function
                await (0, _RoomInvite.inviteUsersToRoom)(newRoomId, toInvite);
              }

              cli.removeListener('Room', checkForUpgradeFn);
            };

            cli.on('Room', checkForUpgradeFn);
          } // We have to await after so that the checkForUpgradesFn has a proper reference
          // to the new room's ID.


          await upgradePromise;
        } catch (e) {
          console.error(e);
          if (checkForUpgradeFn) cli.removeListener('Room', checkForUpgradeFn);
          const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

          _Modal.default.createTrackedDialog('Slash Commands', 'room upgrade error', ErrorDialog, {
            title: (0, _languageHandler._t)('Error upgrading room'),
            description: (0, _languageHandler._t)('Double check that your server supports the room version chosen and try again.')
          });
        }
      }));
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'nick',
  args: '<display_name>',
  description: (0, _languageHandler._td)('Changes your display nickname'),
  runFn: function (roomId, args) {
    if (args) {
      return success(_MatrixClientPeg.MatrixClientPeg.get().setDisplayName(args));
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'myroomnick',
  aliases: ['roomnick'],
  args: '<display_name>',
  description: (0, _languageHandler._td)('Changes your display nickname in the current room only'),
  runFn: function (roomId, args) {
    if (args) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const ev = cli.getRoom(roomId).currentState.getStateEvents('m.room.member', cli.getUserId());

      const content = _objectSpread({}, ev ? ev.getContent() : {
        membership: 'join'
      }, {
        displayname: args
      });

      return success(cli.sendStateEvent(roomId, 'm.room.member', content, cli.getUserId()));
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'roomavatar',
  args: '[<mxc_url>]',
  description: (0, _languageHandler._td)('Changes the avatar of the current room'),
  runFn: function (roomId, args) {
    let promise = Promise.resolve(args);

    if (!args) {
      promise = singleMxcUpload();
    }

    return success(promise.then(url => {
      if (!url) return;
      return _MatrixClientPeg.MatrixClientPeg.get().sendStateEvent(roomId, 'm.room.avatar', {
        url
      }, '');
    }));
  },
  category: CommandCategories.actions
}), new Command({
  command: 'myroomavatar',
  args: '[<mxc_url>]',
  description: (0, _languageHandler._td)('Changes your avatar in this current room only'),
  runFn: function (roomId, args) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    const room = cli.getRoom(roomId);
    const userId = cli.getUserId();
    let promise = Promise.resolve(args);

    if (!args) {
      promise = singleMxcUpload();
    }

    return success(promise.then(url => {
      if (!url) return;
      const ev = room.currentState.getStateEvents('m.room.member', userId);

      const content = _objectSpread({}, ev ? ev.getContent() : {
        membership: 'join'
      }, {
        avatar_url: url
      });

      return cli.sendStateEvent(roomId, 'm.room.member', content, userId);
    }));
  },
  category: CommandCategories.actions
}), new Command({
  command: 'myavatar',
  args: '[<mxc_url>]',
  description: (0, _languageHandler._td)('Changes your avatar in all rooms'),
  runFn: function (roomId, args) {
    let promise = Promise.resolve(args);

    if (!args) {
      promise = singleMxcUpload();
    }

    return success(promise.then(url => {
      if (!url) return;
      return _MatrixClientPeg.MatrixClientPeg.get().setAvatarUrl(url);
    }));
  },
  category: CommandCategories.actions
}), new Command({
  command: 'topic',
  args: '[<topic>]',
  description: (0, _languageHandler._td)('Gets or sets the room topic'),
  runFn: function (roomId, args) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    if (args) {
      return success(cli.setRoomTopic(roomId, args));
    }

    const room = cli.getRoom(roomId);
    if (!room) return reject('Bad room ID: ' + roomId);
    const topicEvents = room.currentState.getStateEvents('m.room.topic', '');
    const topic = topicEvents && topicEvents.getContent().topic;
    const topicHtml = topic ? (0, _HtmlUtils.linkifyAndSanitizeHtml)(topic) : (0, _languageHandler._t)('This room has no topic.');
    const InfoDialog = sdk.getComponent('dialogs.InfoDialog');

    _Modal.default.createTrackedDialog('Slash Commands', 'Topic', InfoDialog, {
      title: room.name,
      description: React.createElement("div", {
        dangerouslySetInnerHTML: {
          __html: topicHtml
        }
      })
    });

    return success();
  },
  category: CommandCategories.admin
}), new Command({
  command: 'roomname',
  args: '<name>',
  description: (0, _languageHandler._td)('Sets the room name'),
  runFn: function (roomId, args) {
    if (args) {
      return success(_MatrixClientPeg.MatrixClientPeg.get().setRoomName(roomId, args));
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'invite',
  args: '<user-id>',
  description: (0, _languageHandler._td)('Invites user with given id to current room'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+)$/);

      if (matches) {
        // We use a MultiInviter to re-use the invite logic, even though
        // we're only inviting one user.
        const address = matches[1]; // If we need an identity server but don't have one, things
        // get a bit more complex here, but we try to show something
        // meaningful.

        let finished = Promise.resolve();

        if ((0, _UserAddress.getAddressType)(address) === 'email' && !_MatrixClientPeg.MatrixClientPeg.get().getIdentityServerUrl()) {
          const defaultIdentityServerUrl = (0, _IdentityServerUtils.getDefaultIdentityServerUrl)();

          if (defaultIdentityServerUrl) {
            ({
              finished
            } = _Modal.default.createTrackedDialog('Slash Commands', 'Identity server', _QuestionDialog.default, {
              title: (0, _languageHandler._t)("Use an identity server"),
              description: React.createElement("p", null, (0, _languageHandler._t)("Use an identity server to invite by email. " + "Click continue to use the default identity server " + "(%(defaultIdentityServerName)s) or manage in Settings.", {
                defaultIdentityServerName: (0, _UrlUtils.abbreviateUrl)(defaultIdentityServerUrl)
              })),
              button: (0, _languageHandler._t)("Continue")
            }));
            finished = finished.then(([useDefault]) => {
              if (useDefault) {
                (0, _IdentityServerUtils.useDefaultIdentityServer)();
                return;
              }

              throw new Error((0, _languageHandler._t)("Use an identity server to invite by email. Manage in Settings."));
            });
          } else {
            return reject((0, _languageHandler._t)("Use an identity server to invite by email. Manage in Settings."));
          }
        }

        const inviter = new _MultiInviter.default(roomId);
        return success(finished.then(() => {
          return inviter.invite([address]);
        }).then(() => {
          if (inviter.getCompletionState(address) !== "invited") {
            throw new Error(inviter.getErrorText(address));
          }
        }));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'join',
  aliases: ['j', 'goto'],
  args: '<room-alias>',
  description: (0, _languageHandler._td)('Joins room with given alias'),
  runFn: function (_, args) {
    if (args) {
      // Note: we support 2 versions of this command. The first is
      // the public-facing one for most users and the other is a
      // power-user edition where someone may join via permalink or
      // room ID with optional servers. Practically, this results
      // in the following variations:
      //   /join #example:example.org
      //   /join !example:example.org
      //   /join !example:example.org altserver.com elsewhere.ca
      //   /join https://matrix.to/#/!example:example.org?via=altserver.com
      // The command also supports event permalinks transparently:
      //   /join https://matrix.to/#/!example:example.org/$something:example.org
      //   /join https://matrix.to/#/!example:example.org/$something:example.org?via=altserver.com
      const params = args.split(' ');
      if (params.length < 1) return reject(this.getUsage());
      let isPermalink = false;

      if (params[0].startsWith("http:") || params[0].startsWith("https:")) {
        // It's at least a URL - try and pull out a hostname to check against the
        // permalink handler
        const parsedUrl = new URL(params[0]);
        const hostname = parsedUrl.host || parsedUrl.hostname; // takes first non-falsey value
        // if we're using a Riot permalink handler, this will catch it before we get much further.
        // see below where we make assumptions about parsing the URL.

        if ((0, _Permalinks.isPermalinkHost)(hostname)) {
          isPermalink = true;
        }
      }

      if (params[0][0] === '#') {
        let roomAlias = params[0];

        if (!roomAlias.includes(':')) {
          roomAlias += ':' + _MatrixClientPeg.MatrixClientPeg.get().getDomain();
        }

        _dispatcher.default.dispatch({
          action: 'view_room',
          room_alias: roomAlias,
          auto_join: true
        });

        return success();
      } else if (params[0][0] === '!') {
        const roomId = params[0];
        const viaServers = params.splice(0);

        _dispatcher.default.dispatch({
          action: 'view_room',
          room_id: roomId,
          opts: {
            // These are passed down to the js-sdk's /join call
            viaServers: viaServers
          },
          via_servers: viaServers,
          // for the rejoin button
          auto_join: true
        });

        return success();
      } else if (isPermalink) {
        const permalinkParts = (0, _Permalinks.parsePermalink)(params[0]); // This check technically isn't needed because we already did our
        // safety checks up above. However, for good measure, let's be sure.

        if (!permalinkParts) {
          return reject(this.getUsage());
        } // If for some reason someone wanted to join a group or user, we should
        // stop them now.


        if (!permalinkParts.roomIdOrAlias) {
          return reject(this.getUsage());
        }

        const entity = permalinkParts.roomIdOrAlias;
        const viaServers = permalinkParts.viaServers;
        const eventId = permalinkParts.eventId;
        const dispatch = {
          action: 'view_room',
          auto_join: true
        };
        if (entity[0] === '!') dispatch["room_id"] = entity;else dispatch["room_alias"] = entity;

        if (eventId) {
          dispatch["event_id"] = eventId;
          dispatch["highlighted"] = true;
        }

        if (viaServers) {
          // For the join
          dispatch["opts"] = {
            // These are passed down to the js-sdk's /join call
            viaServers: viaServers
          }; // For if the join fails (rejoin button)

          dispatch['via_servers'] = viaServers;
        }

        _dispatcher.default.dispatch(dispatch);

        return success();
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'part',
  args: '[<room-alias>]',
  description: (0, _languageHandler._td)('Leave room'),
  runFn: function (roomId, args) {
    const cli = _MatrixClientPeg.MatrixClientPeg.get();

    let targetRoomId;

    if (args) {
      const matches = args.match(/^(\S+)$/);

      if (matches) {
        let roomAlias = matches[1];
        if (roomAlias[0] !== '#') return reject(this.getUsage());

        if (!roomAlias.includes(':')) {
          roomAlias += ':' + cli.getDomain();
        } // Try to find a room with this alias


        const rooms = cli.getRooms();

        for (let i = 0; i < rooms.length; i++) {
          const aliasEvents = rooms[i].currentState.getStateEvents('m.room.aliases');

          for (let j = 0; j < aliasEvents.length; j++) {
            const aliases = aliasEvents[j].getContent().aliases || [];

            for (let k = 0; k < aliases.length; k++) {
              if (aliases[k] === roomAlias) {
                targetRoomId = rooms[i].roomId;
                break;
              }
            }

            if (targetRoomId) break;
          }

          if (targetRoomId) break;
        }

        if (!targetRoomId) return reject((0, _languageHandler._t)('Unrecognised room alias:') + ' ' + roomAlias);
      }
    }

    if (!targetRoomId) targetRoomId = roomId;
    return success(cli.leaveRoomChain(targetRoomId).then(function () {
      _dispatcher.default.dispatch({
        action: 'view_next_room'
      });
    }));
  },
  category: CommandCategories.actions
}), new Command({
  command: 'kick',
  args: '<user-id> [reason]',
  description: (0, _languageHandler._td)('Kicks user with given id'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+?)( +(.*))?$/);

      if (matches) {
        return success(_MatrixClientPeg.MatrixClientPeg.get().kick(roomId, matches[1], matches[3]));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'ban',
  args: '<user-id> [reason]',
  description: (0, _languageHandler._td)('Bans user with given id'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+?)( +(.*))?$/);

      if (matches) {
        return success(_MatrixClientPeg.MatrixClientPeg.get().ban(roomId, matches[1], matches[3]));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'unban',
  args: '<user-id>',
  description: (0, _languageHandler._td)('Unbans user with given ID'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+)$/);

      if (matches) {
        // Reset the user membership to "leave" to unban him
        return success(_MatrixClientPeg.MatrixClientPeg.get().unban(roomId, matches[1]));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'ignore',
  args: '<user-id>',
  description: (0, _languageHandler._td)('Ignores a user, hiding their messages from you'),
  runFn: function (roomId, args) {
    if (args) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const matches = args.match(/^(\S+)$/);

      if (matches) {
        const userId = matches[1];
        const ignoredUsers = cli.getIgnoredUsers();
        ignoredUsers.push(userId); // de-duped internally in the js-sdk

        return success(cli.setIgnoredUsers(ignoredUsers).then(() => {
          const InfoDialog = sdk.getComponent('dialogs.InfoDialog');

          _Modal.default.createTrackedDialog('Slash Commands', 'User ignored', InfoDialog, {
            title: (0, _languageHandler._t)('Ignored user'),
            description: React.createElement("div", null, React.createElement("p", null, (0, _languageHandler._t)('You are now ignoring %(userId)s', {
              userId
            })))
          });
        }));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'unignore',
  args: '<user-id>',
  description: (0, _languageHandler._td)('Stops ignoring a user, showing their messages going forward'),
  runFn: function (roomId, args) {
    if (args) {
      const cli = _MatrixClientPeg.MatrixClientPeg.get();

      const matches = args.match(/^(\S+)$/);

      if (matches) {
        const userId = matches[1];
        const ignoredUsers = cli.getIgnoredUsers();
        const index = ignoredUsers.indexOf(userId);
        if (index !== -1) ignoredUsers.splice(index, 1);
        return success(cli.setIgnoredUsers(ignoredUsers).then(() => {
          const InfoDialog = sdk.getComponent('dialogs.InfoDialog');

          _Modal.default.createTrackedDialog('Slash Commands', 'User unignored', InfoDialog, {
            title: (0, _languageHandler._t)('Unignored user'),
            description: React.createElement("div", null, React.createElement("p", null, (0, _languageHandler._t)('You are no longer ignoring %(userId)s', {
              userId
            })))
          });
        }));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.actions
}), new Command({
  command: 'op',
  args: '<user-id> [<power-level>]',
  description: (0, _languageHandler._td)('Define the power level of a user'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+?)( +(-?\d+))?$/);
      let powerLevel = 50; // default power level for op

      if (matches) {
        const userId = matches[1];

        if (matches.length === 4 && undefined !== matches[3]) {
          powerLevel = parseInt(matches[3], 10);
        }

        if (!isNaN(powerLevel)) {
          const cli = _MatrixClientPeg.MatrixClientPeg.get();

          const room = cli.getRoom(roomId);
          if (!room) return reject('Bad room ID: ' + roomId);
          const powerLevelEvent = room.currentState.getStateEvents('m.room.power_levels', '');
          return success(cli.setPowerLevel(roomId, userId, powerLevel, powerLevelEvent));
        }
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'deop',
  args: '<user-id>',
  description: (0, _languageHandler._td)('Deops user with given id'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+)$/);

      if (matches) {
        const cli = _MatrixClientPeg.MatrixClientPeg.get();

        const room = cli.getRoom(roomId);
        if (!room) return reject('Bad room ID: ' + roomId);
        const powerLevelEvent = room.currentState.getStateEvents('m.room.power_levels', '');
        return success(cli.setPowerLevel(roomId, args, undefined, powerLevelEvent));
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.admin
}), new Command({
  command: 'devtools',
  description: (0, _languageHandler._td)('Opens the Developer Tools dialog'),
  runFn: function (roomId) {
    const DevtoolsDialog = sdk.getComponent('dialogs.DevtoolsDialog');

    _Modal.default.createDialog(DevtoolsDialog, {
      roomId
    });

    return success();
  },
  category: CommandCategories.advanced
}), new Command({
  command: 'addwidget',
  args: '<url>',
  description: (0, _languageHandler._td)('Adds a custom widget by URL to the room'),
  runFn: function (roomId, args) {
    if (!args || !args.startsWith("https://") && !args.startsWith("http://")) {
      return reject((0, _languageHandler._t)("Please supply a https:// or http:// widget URL"));
    }

    if (_WidgetUtils.default.canUserModifyWidgets(roomId)) {
      const userId = _MatrixClientPeg.MatrixClientPeg.get().getUserId();

      const nowMs = new Date().getTime();
      const widgetId = encodeURIComponent("".concat(roomId, "_").concat(userId, "_").concat(nowMs));
      return success(_WidgetUtils.default.setRoomWidget(roomId, widgetId, "m.custom", args, "Custom Widget", {}));
    } else {
      return reject((0, _languageHandler._t)("You cannot modify widgets in this room."));
    }
  },
  category: CommandCategories.admin
}), new Command({
  command: 'verify',
  args: '<user-id> <device-id> <device-signing-key>',
  description: (0, _languageHandler._td)('Verifies a user, session, and pubkey tuple'),
  runFn: function (roomId, args) {
    if (args) {
      const matches = args.match(/^(\S+) +(\S+) +(\S+)$/);

      if (matches) {
        const cli = _MatrixClientPeg.MatrixClientPeg.get();

        const userId = matches[1];
        const deviceId = matches[2];
        const fingerprint = matches[3];
        return success((async () => {
          const device = await cli.getStoredDevice(userId, deviceId);

          if (!device) {
            throw new Error((0, _languageHandler._t)('Unknown (user, session) pair:') + " (".concat(userId, ", ").concat(deviceId, ")"));
          }

          const deviceTrust = await cli.checkDeviceTrust(userId, deviceId);

          if (deviceTrust.isVerified()) {
            if (device.getFingerprint() === fingerprint) {
              throw new Error((0, _languageHandler._t)('Session already verified!'));
            } else {
              throw new Error((0, _languageHandler._t)('WARNING: Session already verified, but keys do NOT MATCH!'));
            }
          }

          if (device.getFingerprint() !== fingerprint) {
            const fprint = device.getFingerprint();
            throw new Error((0, _languageHandler._t)('WARNING: KEY VERIFICATION FAILED! The signing key for %(userId)s and session' + ' %(deviceId)s is "%(fprint)s" which does not match the provided key ' + '"%(fingerprint)s". This could mean your communications are being intercepted!', {
              fprint,
              userId,
              deviceId,
              fingerprint
            }));
          }

          await cli.setDeviceVerified(userId, deviceId, true); // Tell the user we verified everything

          const InfoDialog = sdk.getComponent('dialogs.InfoDialog');

          _Modal.default.createTrackedDialog('Slash Commands', 'Verified key', InfoDialog, {
            title: (0, _languageHandler._t)('Verified key'),
            description: React.createElement("div", null, React.createElement("p", null, (0, _languageHandler._t)('The signing key you provided matches the signing key you received ' + 'from %(userId)s\'s session %(deviceId)s. Session marked as verified.', {
              userId,
              deviceId
            })))
          });
        })());
      }
    }

    return reject(this.getUsage());
  },
  category: CommandCategories.advanced
}), new Command({
  command: 'discardsession',
  description: (0, _languageHandler._td)('Forces the current outbound group session in an encrypted room to be discarded'),
  runFn: function (roomId) {
    try {
      _MatrixClientPeg.MatrixClientPeg.get().forceDiscardSession(roomId);
    } catch (e) {
      return reject(e.message);
    }

    return success();
  },
  category: CommandCategories.advanced
}), new Command({
  command: "rainbow",
  description: (0, _languageHandler._td)("Sends the given message coloured as a rainbow"),
  args: '<message>',
  runFn: function (roomId, args) {
    if (!args) return reject(this.getUserId());
    return success(_MatrixClientPeg.MatrixClientPeg.get().sendHtmlMessage(roomId, args, (0, _colour.textToHtmlRainbow)(args)));
  },
  category: CommandCategories.messages
}), new Command({
  command: "rainbowme",
  description: (0, _languageHandler._td)("Sends the given emote coloured as a rainbow"),
  args: '<message>',
  runFn: function (roomId, args) {
    if (!args) return reject(this.getUserId());
    return success(_MatrixClientPeg.MatrixClientPeg.get().sendHtmlEmote(roomId, args, (0, _colour.textToHtmlRainbow)(args)));
  },
  category: CommandCategories.messages
}), new Command({
  command: "help",
  description: (0, _languageHandler._td)("Displays list of commands with usages and descriptions"),
  runFn: function () {
    const SlashCommandHelpDialog = sdk.getComponent('dialogs.SlashCommandHelpDialog');

    _Modal.default.createTrackedDialog('Slash Commands', 'Help', SlashCommandHelpDialog);

    return success();
  },
  category: CommandCategories.advanced
}), new Command({
  command: "whois",
  description: (0, _languageHandler._td)("Displays information about a user"),
  args: "<user-id>",
  runFn: function (roomId, userId) {
    if (!userId || !userId.startsWith("@") || !userId.includes(":")) {
      return reject(this.getUsage());
    }

    const member = _MatrixClientPeg.MatrixClientPeg.get().getRoom(roomId).getMember(userId);

    _dispatcher.default.dispatch({
      action: 'view_user',
      member: member || {
        userId
      }
    });

    return success();
  },
  category: CommandCategories.advanced
}), // Command definitions for autocompletion ONLY:
// /me is special because its not handled by SlashCommands.js and is instead done inside the Composer classes
new Command({
  command: 'me',
  args: '<message>',
  description: (0, _languageHandler._td)('Displays action'),
  category: CommandCategories.messages,
  hideCompletionAfterSpace: true
})]; // build a map from names and aliases to the Command objects.

exports.Commands = Commands;
const CommandMap = new Map();
exports.CommandMap = CommandMap;
Commands.forEach(cmd => {
  CommandMap.set(cmd.command, cmd);
  cmd.aliases.forEach(alias => {
    CommandMap.set(alias, cmd);
  });
});
/**
 * Process the given text for /commands and return a bound method to perform them.
 * @param {string} roomId The room in which the command was performed.
 * @param {string} input The raw text input by the user.
 * @return {null|function(): Object} Function returning an object with the property 'error' if there was an error
 * processing the command, or 'promise' if a request was sent out.
 * Returns null if the input didn't match a command.
 */

function getCommand(roomId, input) {
  // trim any trailing whitespace, as it can confuse the parser for
  // IRC-style commands
  input = input.replace(/\s+$/, '');
  if (input[0] !== '/') return null; // not a command

  const bits = input.match(/^(\S+?)(?: +((.|\n)*))?$/);
  let cmd;
  let args;

  if (bits) {
    cmd = bits[1].substring(1).toLowerCase();
    args = bits[2];
  } else {
    cmd = input;
  }

  if (CommandMap.has(cmd)) {
    return () => CommandMap.get(cmd).run(roomId, args, cmd);
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9TbGFzaENvbW1hbmRzLnRzeCJdLCJuYW1lcyI6WyJzaW5nbGVNeGNVcGxvYWQiLCJQcm9taXNlIiwicmVzb2x2ZSIsImZpbGVTZWxlY3RvciIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsInNldEF0dHJpYnV0ZSIsIm9uY2hhbmdlIiwiZXYiLCJmaWxlIiwidGFyZ2V0IiwiZmlsZXMiLCJVcGxvYWRDb25maXJtRGlhbG9nIiwic2RrIiwiZ2V0Q29tcG9uZW50IiwiTW9kYWwiLCJjcmVhdGVUcmFja2VkRGlhbG9nIiwib25GaW5pc2hlZCIsInNob3VsZENvbnRpbnVlIiwiTWF0cml4Q2xpZW50UGVnIiwiZ2V0IiwidXBsb2FkQ29udGVudCIsImNsaWNrIiwiQ29tbWFuZENhdGVnb3JpZXMiLCJDb21tYW5kIiwiY29uc3RydWN0b3IiLCJvcHRzIiwiY29tbWFuZCIsImFsaWFzZXMiLCJhcmdzIiwiZGVzY3JpcHRpb24iLCJydW5GbiIsImNhdGVnb3J5Iiwib3RoZXIiLCJoaWRlQ29tcGxldGlvbkFmdGVyU3BhY2UiLCJnZXRDb21tYW5kIiwiZ2V0Q29tbWFuZFdpdGhBcmdzIiwicnVuIiwicm9vbUlkIiwiY21kIiwiYmluZCIsImdldFVzYWdlIiwicmVqZWN0IiwiZXJyb3IiLCJzdWNjZXNzIiwicHJvbWlzZSIsIkNvbW1hbmRzIiwibWVzc2FnZSIsInNlbmRUZXh0TWVzc2FnZSIsIm1lc3NhZ2VzIiwic2VuZEh0bWxNZXNzYWdlIiwiRXJyb3JEaWFsb2ciLCJ0aXRsZSIsImFjdGlvbnMiLCJjbGkiLCJyb29tIiwiZ2V0Um9vbSIsImN1cnJlbnRTdGF0ZSIsIm1heUNsaWVudFNlbmRTdGF0ZUV2ZW50IiwiUm9vbVVwZ3JhZGVXYXJuaW5nRGlhbG9nIiwiZmluaXNoZWQiLCJ0YXJnZXRWZXJzaW9uIiwidGhlbiIsInJlc3AiLCJjb250aW51ZSIsImNoZWNrRm9yVXBncmFkZUZuIiwidXBncmFkZVByb21pc2UiLCJ1cGdyYWRlUm9vbSIsImludml0ZSIsIm5ld1Jvb20iLCJyZXBsYWNlbWVudF9yb29tIiwibmV3Um9vbUlkIiwidG9JbnZpdGUiLCJnZXRNZW1iZXJzV2l0aE1lbWJlcnNoaXAiLCJtYXAiLCJtIiwidXNlcklkIiwiZmlsdGVyIiwiZ2V0VXNlcklkIiwibGVuZ3RoIiwicmVtb3ZlTGlzdGVuZXIiLCJvbiIsImUiLCJjb25zb2xlIiwiYWRtaW4iLCJzZXREaXNwbGF5TmFtZSIsImdldFN0YXRlRXZlbnRzIiwiY29udGVudCIsImdldENvbnRlbnQiLCJtZW1iZXJzaGlwIiwiZGlzcGxheW5hbWUiLCJzZW5kU3RhdGVFdmVudCIsInVybCIsImF2YXRhcl91cmwiLCJzZXRBdmF0YXJVcmwiLCJzZXRSb29tVG9waWMiLCJ0b3BpY0V2ZW50cyIsInRvcGljIiwidG9waWNIdG1sIiwiSW5mb0RpYWxvZyIsIm5hbWUiLCJfX2h0bWwiLCJzZXRSb29tTmFtZSIsIm1hdGNoZXMiLCJtYXRjaCIsImFkZHJlc3MiLCJnZXRJZGVudGl0eVNlcnZlclVybCIsImRlZmF1bHRJZGVudGl0eVNlcnZlclVybCIsIlF1ZXN0aW9uRGlhbG9nIiwiZGVmYXVsdElkZW50aXR5U2VydmVyTmFtZSIsImJ1dHRvbiIsInVzZURlZmF1bHQiLCJFcnJvciIsImludml0ZXIiLCJNdWx0aUludml0ZXIiLCJnZXRDb21wbGV0aW9uU3RhdGUiLCJnZXRFcnJvclRleHQiLCJfIiwicGFyYW1zIiwic3BsaXQiLCJpc1Blcm1hbGluayIsInN0YXJ0c1dpdGgiLCJwYXJzZWRVcmwiLCJVUkwiLCJob3N0bmFtZSIsImhvc3QiLCJyb29tQWxpYXMiLCJpbmNsdWRlcyIsImdldERvbWFpbiIsImRpcyIsImRpc3BhdGNoIiwiYWN0aW9uIiwicm9vbV9hbGlhcyIsImF1dG9fam9pbiIsInZpYVNlcnZlcnMiLCJzcGxpY2UiLCJyb29tX2lkIiwidmlhX3NlcnZlcnMiLCJwZXJtYWxpbmtQYXJ0cyIsInJvb21JZE9yQWxpYXMiLCJlbnRpdHkiLCJldmVudElkIiwidGFyZ2V0Um9vbUlkIiwicm9vbXMiLCJnZXRSb29tcyIsImkiLCJhbGlhc0V2ZW50cyIsImoiLCJrIiwibGVhdmVSb29tQ2hhaW4iLCJraWNrIiwiYmFuIiwidW5iYW4iLCJpZ25vcmVkVXNlcnMiLCJnZXRJZ25vcmVkVXNlcnMiLCJwdXNoIiwic2V0SWdub3JlZFVzZXJzIiwiaW5kZXgiLCJpbmRleE9mIiwicG93ZXJMZXZlbCIsInVuZGVmaW5lZCIsInBhcnNlSW50IiwiaXNOYU4iLCJwb3dlckxldmVsRXZlbnQiLCJzZXRQb3dlckxldmVsIiwiRGV2dG9vbHNEaWFsb2ciLCJjcmVhdGVEaWFsb2ciLCJhZHZhbmNlZCIsIldpZGdldFV0aWxzIiwiY2FuVXNlck1vZGlmeVdpZGdldHMiLCJub3dNcyIsIkRhdGUiLCJnZXRUaW1lIiwid2lkZ2V0SWQiLCJlbmNvZGVVUklDb21wb25lbnQiLCJzZXRSb29tV2lkZ2V0IiwiZGV2aWNlSWQiLCJmaW5nZXJwcmludCIsImRldmljZSIsImdldFN0b3JlZERldmljZSIsImRldmljZVRydXN0IiwiY2hlY2tEZXZpY2VUcnVzdCIsImlzVmVyaWZpZWQiLCJnZXRGaW5nZXJwcmludCIsImZwcmludCIsInNldERldmljZVZlcmlmaWVkIiwiZm9yY2VEaXNjYXJkU2Vzc2lvbiIsInNlbmRIdG1sRW1vdGUiLCJTbGFzaENvbW1hbmRIZWxwRGlhbG9nIiwibWVtYmVyIiwiZ2V0TWVtYmVyIiwiQ29tbWFuZE1hcCIsIk1hcCIsImZvckVhY2giLCJzZXQiLCJhbGlhcyIsImlucHV0IiwicmVwbGFjZSIsImJpdHMiLCJzdWJzdHJpbmciLCJ0b0xvd2VyQ2FzZSIsImhhcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFvQkE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7OztBQU9BLE1BQU1BLGVBQWUsR0FBRztBQUFBO0FBQTBCO0FBQzlDLFNBQU8sSUFBSUMsT0FBSixDQUFhQyxPQUFELElBQWE7QUFDNUIsVUFBTUMsWUFBWSxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBckI7QUFDQUYsSUFBQUEsWUFBWSxDQUFDRyxZQUFiLENBQTBCLE1BQTFCLEVBQWtDLE1BQWxDOztBQUNBSCxJQUFBQSxZQUFZLENBQUNJLFFBQWIsR0FBd0IsQ0FBQ0M7QUFBRDtBQUFBLFNBQXdCO0FBQzVDLFlBQU1DLElBQUksR0FBR0QsRUFBRSxDQUFDRSxNQUFILENBQVVDLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBYjtBQUVBLFlBQU1DLG1CQUFtQixHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIsNkJBQWpCLENBQTVCOztBQUNBQyxxQkFBTUMsbUJBQU4sQ0FBMEIsMkJBQTFCLEVBQXVELEVBQXZELEVBQTJESixtQkFBM0QsRUFBZ0Y7QUFDNUVILFFBQUFBLElBRDRFO0FBRTVFUSxRQUFBQSxVQUFVLEVBQUdDLGNBQUQsSUFBb0I7QUFDNUJoQixVQUFBQSxPQUFPLENBQUNnQixjQUFjLEdBQUdDLGlDQUFnQkMsR0FBaEIsR0FBc0JDLGFBQXRCLENBQW9DWixJQUFwQyxDQUFILEdBQStDLElBQTlELENBQVA7QUFDSDtBQUoyRSxPQUFoRjtBQU1ILEtBVkQ7O0FBWUFOLElBQUFBLFlBQVksQ0FBQ21CLEtBQWI7QUFDSCxHQWhCTSxDQUFQO0FBaUJILENBbEJEOztBQW9CTyxNQUFNQyxpQkFBaUIsR0FBRztBQUM3QixjQUFZLDBCQUFJLFVBQUosQ0FEaUI7QUFFN0IsYUFBVywwQkFBSSxTQUFKLENBRmtCO0FBRzdCLFdBQVMsMEJBQUksT0FBSixDQUhvQjtBQUk3QixjQUFZLDBCQUFJLFVBQUosQ0FKaUI7QUFLN0IsV0FBUywwQkFBSSxPQUFKO0FBTG9CLENBQTFCOzs7QUFvQlAsTUFBTUMsT0FBTixDQUFjO0FBU1ZDLEVBQUFBLFdBQVcsQ0FBQ0M7QUFBRDtBQUFBLElBQXFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDNUIsU0FBS0MsT0FBTCxHQUFlRCxJQUFJLENBQUNDLE9BQXBCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlRixJQUFJLENBQUNFLE9BQUwsSUFBZ0IsRUFBL0I7QUFDQSxTQUFLQyxJQUFMLEdBQVlILElBQUksQ0FBQ0csSUFBTCxJQUFhLEVBQXpCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQkosSUFBSSxDQUFDSSxXQUF4QjtBQUNBLFNBQUtDLEtBQUwsR0FBYUwsSUFBSSxDQUFDSyxLQUFsQjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0JOLElBQUksQ0FBQ00sUUFBTCxJQUFpQlQsaUJBQWlCLENBQUNVLEtBQW5EO0FBQ0EsU0FBS0Msd0JBQUwsR0FBZ0NSLElBQUksQ0FBQ1Esd0JBQUwsSUFBaUMsS0FBakU7QUFDSDs7QUFFREMsRUFBQUEsVUFBVSxHQUFHO0FBQ1Qsc0JBQVcsS0FBS1IsT0FBaEI7QUFDSDs7QUFFRFMsRUFBQUEsa0JBQWtCLEdBQUc7QUFDakIsV0FBTyxLQUFLRCxVQUFMLEtBQW9CLEdBQXBCLEdBQTBCLEtBQUtOLElBQXRDO0FBQ0g7O0FBRURRLEVBQUFBLEdBQUcsQ0FBQ0M7QUFBRDtBQUFBLElBQWlCVDtBQUFqQjtBQUFBLElBQStCVTtBQUEvQjtBQUFBLElBQTRDO0FBQzNDO0FBQ0EsUUFBSSxDQUFDLEtBQUtSLEtBQVYsRUFBaUI7QUFDakIsV0FBTyxLQUFLQSxLQUFMLENBQVdTLElBQVgsQ0FBZ0IsSUFBaEIsRUFBc0JGLE1BQXRCLEVBQThCVCxJQUE5QixFQUFvQ1UsR0FBcEMsQ0FBUDtBQUNIOztBQUVERSxFQUFBQSxRQUFRLEdBQUc7QUFDUCxXQUFPLHlCQUFHLE9BQUgsSUFBYyxJQUFkLEdBQXFCLEtBQUtMLGtCQUFMLEVBQTVCO0FBQ0g7O0FBbkNTOztBQXNDZCxTQUFTTSxNQUFULENBQWdCQyxLQUFoQixFQUF1QjtBQUNuQixTQUFPO0FBQUNBLElBQUFBO0FBQUQsR0FBUDtBQUNIOztBQUVELFNBQVNDLE9BQVQsQ0FBaUJDO0FBQWpCO0FBQUEsRUFBeUM7QUFDckMsU0FBTztBQUFDQSxJQUFBQTtBQUFELEdBQVA7QUFDSDtBQUVEOzs7OztBQUlPLE1BQU1DLFFBQVEsR0FBRyxDQUNwQixJQUFJdEIsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxPQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxXQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSw2Q0FBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJa0IsT0FBTyxHQUFHLFlBQWQ7O0FBQ0EsUUFBSWxCLElBQUosRUFBVTtBQUNOa0IsTUFBQUEsT0FBTyxHQUFHQSxPQUFPLEdBQUcsR0FBVixHQUFnQmxCLElBQTFCO0FBQ0g7O0FBQ0QsV0FBT2UsT0FBTyxDQUFDekIsaUNBQWdCQyxHQUFoQixHQUFzQjRCLGVBQXRCLENBQXNDVixNQUF0QyxFQUE4Q1MsT0FBOUMsQ0FBRCxDQUFkO0FBQ0gsR0FWTztBQVdSZixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDMEI7QUFYcEIsQ0FBWixDQURvQixFQWNwQixJQUFJekIsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxPQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxXQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSxvRUFBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVyxRQUFqQixFQUEyQjtBQUM5QixXQUFPTCxPQUFPLENBQUN6QixpQ0FBZ0JDLEdBQWhCLEdBQXNCNEIsZUFBdEIsQ0FBc0NWLE1BQXRDLEVBQThDVyxRQUE5QyxDQUFELENBQWQ7QUFDSCxHQU5PO0FBT1JqQixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDMEI7QUFQcEIsQ0FBWixDQWRvQixFQXVCcEIsSUFBSXpCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsTUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsV0FGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksOERBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlcsUUFBakIsRUFBMkI7QUFDOUIsV0FBT0wsT0FBTyxDQUFDekIsaUNBQWdCQyxHQUFoQixHQUFzQjhCLGVBQXRCLENBQXNDWixNQUF0QyxFQUE4Q1csUUFBOUMsRUFBd0RBLFFBQXhELENBQUQsQ0FBZDtBQUNILEdBTk87QUFPUmpCLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUMwQjtBQVBwQixDQUFaLENBdkJvQixFQWdDcEIsSUFBSXpCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsS0FERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsU0FGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksaUNBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsWUFBVztBQUNkLFVBQU1vQixXQUFXLEdBQUd0QyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCLENBRGMsQ0FFZDs7QUFDQUMsbUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0Qyx1QkFBNUMsRUFBcUVtQyxXQUFyRSxFQUFrRjtBQUM5RUMsTUFBQUEsS0FBSyxFQUFFLHlCQUFHLHVCQUFILENBRHVFO0FBRTlFdEIsTUFBQUEsV0FBVyxFQUFFLHlCQUFHLDZFQUFIO0FBRmlFLEtBQWxGOztBQUlBLFdBQU9jLE9BQU8sRUFBZDtBQUNILEdBWk87QUFhUlosRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCLE9BYnBCO0FBY1JuQixFQUFBQSx3QkFBd0IsRUFBRTtBQWRsQixDQUFaLENBaENvQixFQWdEcEIsSUFBSVYsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxhQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxlQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSxrQ0FBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJQSxJQUFKLEVBQVU7QUFDTixZQUFNeUIsR0FBRyxHQUFHbkMsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFlBQU1tQyxJQUFJLEdBQUdELEdBQUcsQ0FBQ0UsT0FBSixDQUFZbEIsTUFBWixDQUFiOztBQUNBLFVBQUksQ0FBQ2lCLElBQUksQ0FBQ0UsWUFBTCxDQUFrQkMsdUJBQWxCLENBQTBDLGtCQUExQyxFQUE4REosR0FBOUQsQ0FBTCxFQUF5RTtBQUNyRSxlQUFPWixNQUFNLENBQUMseUJBQUcsK0RBQUgsQ0FBRCxDQUFiO0FBQ0g7O0FBRUQsWUFBTWlCLHdCQUF3QixHQUFHOUMsR0FBRyxDQUFDQyxZQUFKLENBQWlCLGtDQUFqQixDQUFqQzs7QUFFQSxZQUFNO0FBQUM4QyxRQUFBQTtBQUFELFVBQWE3QyxlQUFNQyxtQkFBTixDQUEwQixnQkFBMUIsRUFBNEMsMkJBQTVDLEVBQ2YyQyx3QkFEZSxFQUNXO0FBQUNyQixRQUFBQSxNQUFNLEVBQUVBLE1BQVQ7QUFBaUJ1QixRQUFBQSxhQUFhLEVBQUVoQztBQUFoQyxPQURYO0FBQ2tEO0FBQWMsVUFEaEU7QUFFZjtBQUFlLFdBRkE7QUFFTztBQUFhLFVBRnBCLENBQW5COztBQUlBLGFBQU9lLE9BQU8sQ0FBQ2dCLFFBQVEsQ0FBQ0UsSUFBVCxDQUFjLE9BQU8sQ0FBQ0MsSUFBRCxDQUFQLEtBQWtCO0FBQzNDLFlBQUksQ0FBQ0EsSUFBSSxDQUFDQyxRQUFWLEVBQW9CO0FBRXBCLFlBQUlDLGlCQUFKOztBQUNBLFlBQUk7QUFDQSxnQkFBTUMsY0FBYyxHQUFHWixHQUFHLENBQUNhLFdBQUosQ0FBZ0I3QixNQUFoQixFQUF3QlQsSUFBeEIsQ0FBdkIsQ0FEQSxDQUdBO0FBQ0E7QUFDQTs7QUFDQSxjQUFJa0MsSUFBSSxDQUFDSyxNQUFULEVBQWlCO0FBQ2JILFlBQUFBLGlCQUFpQixHQUFHLE1BQU9JLE9BQVAsSUFBbUI7QUFDbkM7QUFDQSxvQkFBTTtBQUFDQyxnQkFBQUEsZ0JBQWdCLEVBQUVDO0FBQW5CLGtCQUFnQyxNQUFNTCxjQUE1QztBQUNBLGtCQUFJRyxPQUFPLENBQUMvQixNQUFSLEtBQW1CaUMsU0FBdkIsRUFBa0M7QUFFbEMsb0JBQU1DLFFBQVEsR0FBRyxDQUNiLEdBQUdqQixJQUFJLENBQUNrQix3QkFBTCxDQUE4QixNQUE5QixDQURVLEVBRWIsR0FBR2xCLElBQUksQ0FBQ2tCLHdCQUFMLENBQThCLFFBQTlCLENBRlUsRUFHZkMsR0FIZSxDQUdYQyxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsTUFISSxFQUdJQyxNQUhKLENBR1dGLENBQUMsSUFBSUEsQ0FBQyxLQUFLckIsR0FBRyxDQUFDd0IsU0FBSixFQUh0QixDQUFqQjs7QUFLQSxrQkFBSU4sUUFBUSxDQUFDTyxNQUFULEdBQWtCLENBQXRCLEVBQXlCO0FBQ3JCO0FBQ0Esc0JBQU0sbUNBQWtCUixTQUFsQixFQUE2QkMsUUFBN0IsQ0FBTjtBQUNIOztBQUVEbEIsY0FBQUEsR0FBRyxDQUFDMEIsY0FBSixDQUFtQixNQUFuQixFQUEyQmYsaUJBQTNCO0FBQ0gsYUFoQkQ7O0FBaUJBWCxZQUFBQSxHQUFHLENBQUMyQixFQUFKLENBQU8sTUFBUCxFQUFlaEIsaUJBQWY7QUFDSCxXQXpCRCxDQTJCQTtBQUNBOzs7QUFDQSxnQkFBTUMsY0FBTjtBQUNILFNBOUJELENBOEJFLE9BQU9nQixDQUFQLEVBQVU7QUFDUkMsVUFBQUEsT0FBTyxDQUFDeEMsS0FBUixDQUFjdUMsQ0FBZDtBQUVBLGNBQUlqQixpQkFBSixFQUF1QlgsR0FBRyxDQUFDMEIsY0FBSixDQUFtQixNQUFuQixFQUEyQmYsaUJBQTNCO0FBRXZCLGdCQUFNZCxXQUFXLEdBQUd0QyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCOztBQUNBQyx5QkFBTUMsbUJBQU4sQ0FBMEIsZ0JBQTFCLEVBQTRDLG9CQUE1QyxFQUFrRW1DLFdBQWxFLEVBQStFO0FBQzNFQyxZQUFBQSxLQUFLLEVBQUUseUJBQUcsc0JBQUgsQ0FEb0U7QUFFM0V0QixZQUFBQSxXQUFXLEVBQUUseUJBQ1QsK0VBRFM7QUFGOEQsV0FBL0U7QUFLSDtBQUNKLE9BOUNjLENBQUQsQ0FBZDtBQStDSDs7QUFDRCxXQUFPWSxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQW5FTztBQW9FUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzZEO0FBcEVwQixDQUFaLENBaERvQixFQXNIcEIsSUFBSTVELE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsTUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsZ0JBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLCtCQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLGFBQU9lLE9BQU8sQ0FBQ3pCLGlDQUFnQkMsR0FBaEIsR0FBc0JpRSxjQUF0QixDQUFxQ3hELElBQXJDLENBQUQsQ0FBZDtBQUNIOztBQUNELFdBQU9hLE1BQU0sQ0FBQyxLQUFLRCxRQUFMLEVBQUQsQ0FBYjtBQUNILEdBVE87QUFVUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBVnBCLENBQVosQ0F0SG9CLEVBa0lwQixJQUFJN0IsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxZQUREO0FBRVJDLEVBQUFBLE9BQU8sRUFBRSxDQUFDLFVBQUQsQ0FGRDtBQUdSQyxFQUFBQSxJQUFJLEVBQUUsZ0JBSEU7QUFJUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLHdEQUFKLENBSkw7QUFLUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU15QixHQUFHLEdBQUduQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsWUFBTVosRUFBRSxHQUFHOEMsR0FBRyxDQUFDRSxPQUFKLENBQVlsQixNQUFaLEVBQW9CbUIsWUFBcEIsQ0FBaUM2QixjQUFqQyxDQUFnRCxlQUFoRCxFQUFpRWhDLEdBQUcsQ0FBQ3dCLFNBQUosRUFBakUsQ0FBWDs7QUFDQSxZQUFNUyxPQUFPLHFCQUNOL0UsRUFBRSxHQUFHQSxFQUFFLENBQUNnRixVQUFILEVBQUgsR0FBcUI7QUFBRUMsUUFBQUEsVUFBVSxFQUFFO0FBQWQsT0FEakI7QUFFVEMsUUFBQUEsV0FBVyxFQUFFN0Q7QUFGSixRQUFiOztBQUlBLGFBQU9lLE9BQU8sQ0FBQ1UsR0FBRyxDQUFDcUMsY0FBSixDQUFtQnJELE1BQW5CLEVBQTJCLGVBQTNCLEVBQTRDaUQsT0FBNUMsRUFBcURqQyxHQUFHLENBQUN3QixTQUFKLEVBQXJELENBQUQsQ0FBZDtBQUNIOztBQUNELFdBQU9wQyxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQWhCTztBQWlCUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBakJwQixDQUFaLENBbElvQixFQXFKcEIsSUFBSTdCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsWUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsYUFGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksd0NBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSWdCLE9BQU8sR0FBRzVDLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQjJCLElBQWhCLENBQWQ7O0FBQ0EsUUFBSSxDQUFDQSxJQUFMLEVBQVc7QUFDUGdCLE1BQUFBLE9BQU8sR0FBRzdDLGVBQWUsRUFBekI7QUFDSDs7QUFFRCxXQUFPNEMsT0FBTyxDQUFDQyxPQUFPLENBQUNpQixJQUFSLENBQWM4QixHQUFELElBQVM7QUFDakMsVUFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDVixhQUFPekUsaUNBQWdCQyxHQUFoQixHQUFzQnVFLGNBQXRCLENBQXFDckQsTUFBckMsRUFBNkMsZUFBN0MsRUFBOEQ7QUFBQ3NELFFBQUFBO0FBQUQsT0FBOUQsRUFBcUUsRUFBckUsQ0FBUDtBQUNILEtBSGMsQ0FBRCxDQUFkO0FBSUgsR0FkTztBQWVSNUQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBZnBCLENBQVosQ0FySm9CLEVBc0twQixJQUFJN0IsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxjQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxhQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSwrQ0FBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixVQUFNeUIsR0FBRyxHQUFHbkMsaUNBQWdCQyxHQUFoQixFQUFaOztBQUNBLFVBQU1tQyxJQUFJLEdBQUdELEdBQUcsQ0FBQ0UsT0FBSixDQUFZbEIsTUFBWixDQUFiO0FBQ0EsVUFBTXNDLE1BQU0sR0FBR3RCLEdBQUcsQ0FBQ3dCLFNBQUosRUFBZjtBQUVBLFFBQUlqQyxPQUFPLEdBQUc1QyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IyQixJQUFoQixDQUFkOztBQUNBLFFBQUksQ0FBQ0EsSUFBTCxFQUFXO0FBQ1BnQixNQUFBQSxPQUFPLEdBQUc3QyxlQUFlLEVBQXpCO0FBQ0g7O0FBRUQsV0FBTzRDLE9BQU8sQ0FBQ0MsT0FBTyxDQUFDaUIsSUFBUixDQUFjOEIsR0FBRCxJQUFTO0FBQ2pDLFVBQUksQ0FBQ0EsR0FBTCxFQUFVO0FBQ1YsWUFBTXBGLEVBQUUsR0FBRytDLElBQUksQ0FBQ0UsWUFBTCxDQUFrQjZCLGNBQWxCLENBQWlDLGVBQWpDLEVBQWtEVixNQUFsRCxDQUFYOztBQUNBLFlBQU1XLE9BQU8scUJBQ04vRSxFQUFFLEdBQUdBLEVBQUUsQ0FBQ2dGLFVBQUgsRUFBSCxHQUFxQjtBQUFFQyxRQUFBQSxVQUFVLEVBQUU7QUFBZCxPQURqQjtBQUVUSSxRQUFBQSxVQUFVLEVBQUVEO0FBRkgsUUFBYjs7QUFJQSxhQUFPdEMsR0FBRyxDQUFDcUMsY0FBSixDQUFtQnJELE1BQW5CLEVBQTJCLGVBQTNCLEVBQTRDaUQsT0FBNUMsRUFBcURYLE1BQXJELENBQVA7QUFDSCxLQVJjLENBQUQsQ0FBZDtBQVNILEdBdkJPO0FBd0JSNUMsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBeEJwQixDQUFaLENBdEtvQixFQWdNcEIsSUFBSTdCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsVUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsYUFGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksa0NBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSWdCLE9BQU8sR0FBRzVDLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQjJCLElBQWhCLENBQWQ7O0FBQ0EsUUFBSSxDQUFDQSxJQUFMLEVBQVc7QUFDUGdCLE1BQUFBLE9BQU8sR0FBRzdDLGVBQWUsRUFBekI7QUFDSDs7QUFFRCxXQUFPNEMsT0FBTyxDQUFDQyxPQUFPLENBQUNpQixJQUFSLENBQWM4QixHQUFELElBQVM7QUFDakMsVUFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDVixhQUFPekUsaUNBQWdCQyxHQUFoQixHQUFzQjBFLFlBQXRCLENBQW1DRixHQUFuQyxDQUFQO0FBQ0gsS0FIYyxDQUFELENBQWQ7QUFJSCxHQWRPO0FBZVI1RCxFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDOEI7QUFmcEIsQ0FBWixDQWhNb0IsRUFpTnBCLElBQUk3QixPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLE9BREQ7QUFFUkUsRUFBQUEsSUFBSSxFQUFFLFdBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLDZCQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFVBQU15QixHQUFHLEdBQUduQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsUUFBSVMsSUFBSixFQUFVO0FBQ04sYUFBT2UsT0FBTyxDQUFDVSxHQUFHLENBQUN5QyxZQUFKLENBQWlCekQsTUFBakIsRUFBeUJULElBQXpCLENBQUQsQ0FBZDtBQUNIOztBQUNELFVBQU0wQixJQUFJLEdBQUdELEdBQUcsQ0FBQ0UsT0FBSixDQUFZbEIsTUFBWixDQUFiO0FBQ0EsUUFBSSxDQUFDaUIsSUFBTCxFQUFXLE9BQU9iLE1BQU0sQ0FBQyxrQkFBa0JKLE1BQW5CLENBQWI7QUFFWCxVQUFNMEQsV0FBVyxHQUFHekMsSUFBSSxDQUFDRSxZQUFMLENBQWtCNkIsY0FBbEIsQ0FBaUMsY0FBakMsRUFBaUQsRUFBakQsQ0FBcEI7QUFDQSxVQUFNVyxLQUFLLEdBQUdELFdBQVcsSUFBSUEsV0FBVyxDQUFDUixVQUFaLEdBQXlCUyxLQUF0RDtBQUNBLFVBQU1DLFNBQVMsR0FBR0QsS0FBSyxHQUFHLHVDQUF1QkEsS0FBdkIsQ0FBSCxHQUFtQyx5QkFBRyx5QkFBSCxDQUExRDtBQUVBLFVBQU1FLFVBQVUsR0FBR3RGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBQ0FDLG1CQUFNQyxtQkFBTixDQUEwQixnQkFBMUIsRUFBNEMsT0FBNUMsRUFBcURtRixVQUFyRCxFQUFpRTtBQUM3RC9DLE1BQUFBLEtBQUssRUFBRUcsSUFBSSxDQUFDNkMsSUFEaUQ7QUFFN0R0RSxNQUFBQSxXQUFXLEVBQUU7QUFBSyxRQUFBLHVCQUF1QixFQUFFO0FBQUV1RSxVQUFBQSxNQUFNLEVBQUVIO0FBQVY7QUFBOUI7QUFGZ0QsS0FBakU7O0FBSUEsV0FBT3RELE9BQU8sRUFBZDtBQUNILEdBdEJPO0FBdUJSWixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDNkQ7QUF2QnBCLENBQVosQ0FqTm9CLEVBME9wQixJQUFJNUQsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxVQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxRQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSxvQkFBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJQSxJQUFKLEVBQVU7QUFDTixhQUFPZSxPQUFPLENBQUN6QixpQ0FBZ0JDLEdBQWhCLEdBQXNCa0YsV0FBdEIsQ0FBa0NoRSxNQUFsQyxFQUEwQ1QsSUFBMUMsQ0FBRCxDQUFkO0FBQ0g7O0FBQ0QsV0FBT2EsTUFBTSxDQUFDLEtBQUtELFFBQUwsRUFBRCxDQUFiO0FBQ0gsR0FUTztBQVVSVCxFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDNkQ7QUFWcEIsQ0FBWixDQTFPb0IsRUFzUHBCLElBQUk1RCxPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLFFBREQ7QUFFUkUsRUFBQUEsSUFBSSxFQUFFLFdBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLDRDQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU0wRSxPQUFPLEdBQUcxRSxJQUFJLENBQUMyRSxLQUFMLENBQVcsU0FBWCxDQUFoQjs7QUFDQSxVQUFJRCxPQUFKLEVBQWE7QUFDVDtBQUNBO0FBQ0EsY0FBTUUsT0FBTyxHQUFHRixPQUFPLENBQUMsQ0FBRCxDQUF2QixDQUhTLENBSVQ7QUFDQTtBQUNBOztBQUNBLFlBQUkzQyxRQUFRLEdBQUczRCxPQUFPLENBQUNDLE9BQVIsRUFBZjs7QUFDQSxZQUNJLGlDQUFldUcsT0FBZixNQUE0QixPQUE1QixJQUNBLENBQUN0RixpQ0FBZ0JDLEdBQWhCLEdBQXNCc0Ysb0JBQXRCLEVBRkwsRUFHRTtBQUNFLGdCQUFNQyx3QkFBd0IsR0FBRyx1REFBakM7O0FBQ0EsY0FBSUEsd0JBQUosRUFBOEI7QUFDMUIsYUFBQztBQUFFL0MsY0FBQUE7QUFBRixnQkFBZTdDLGVBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxpQkFBNUMsRUFDWjRGLHVCQURZLEVBQ0k7QUFDWnhELGNBQUFBLEtBQUssRUFBRSx5QkFBRyx3QkFBSCxDQURLO0FBRVp0QixjQUFBQSxXQUFXLEVBQUUsK0JBQUkseUJBQ2IsZ0RBQ0Esb0RBREEsR0FFQSx3REFIYSxFQUliO0FBQ0krRSxnQkFBQUEseUJBQXlCLEVBQUUsNkJBQWNGLHdCQUFkO0FBRC9CLGVBSmEsQ0FBSixDQUZEO0FBVVpHLGNBQUFBLE1BQU0sRUFBRSx5QkFBRyxVQUFIO0FBVkksYUFESixDQUFoQjtBQWVBbEQsWUFBQUEsUUFBUSxHQUFHQSxRQUFRLENBQUNFLElBQVQsQ0FBYyxDQUFDLENBQUNpRCxVQUFELENBQUQsS0FBdUI7QUFDNUMsa0JBQUlBLFVBQUosRUFBZ0I7QUFDWjtBQUNBO0FBQ0g7O0FBQ0Qsb0JBQU0sSUFBSUMsS0FBSixDQUFVLHlCQUFHLGdFQUFILENBQVYsQ0FBTjtBQUNILGFBTlUsQ0FBWDtBQU9ILFdBdkJELE1BdUJPO0FBQ0gsbUJBQU90RSxNQUFNLENBQUMseUJBQUcsZ0VBQUgsQ0FBRCxDQUFiO0FBQ0g7QUFDSjs7QUFDRCxjQUFNdUUsT0FBTyxHQUFHLElBQUlDLHFCQUFKLENBQWlCNUUsTUFBakIsQ0FBaEI7QUFDQSxlQUFPTSxPQUFPLENBQUNnQixRQUFRLENBQUNFLElBQVQsQ0FBYyxNQUFNO0FBQy9CLGlCQUFPbUQsT0FBTyxDQUFDN0MsTUFBUixDQUFlLENBQUNxQyxPQUFELENBQWYsQ0FBUDtBQUNILFNBRmMsRUFFWjNDLElBRlksQ0FFUCxNQUFNO0FBQ1YsY0FBSW1ELE9BQU8sQ0FBQ0Usa0JBQVIsQ0FBMkJWLE9BQTNCLE1BQXdDLFNBQTVDLEVBQXVEO0FBQ25ELGtCQUFNLElBQUlPLEtBQUosQ0FBVUMsT0FBTyxDQUFDRyxZQUFSLENBQXFCWCxPQUFyQixDQUFWLENBQU47QUFDSDtBQUNKLFNBTmMsQ0FBRCxDQUFkO0FBT0g7QUFDSjs7QUFDRCxXQUFPL0QsTUFBTSxDQUFDLEtBQUtELFFBQUwsRUFBRCxDQUFiO0FBQ0gsR0ExRE87QUEyRFJULEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM4QjtBQTNEcEIsQ0FBWixDQXRQb0IsRUFtVHBCLElBQUk3QixPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLE1BREQ7QUFFUkMsRUFBQUEsT0FBTyxFQUFFLENBQUMsR0FBRCxFQUFNLE1BQU4sQ0FGRDtBQUdSQyxFQUFBQSxJQUFJLEVBQUUsY0FIRTtBQUlSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksNkJBQUosQ0FKTDtBQUtSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU3NGLENBQVQsRUFBWXhGLElBQVosRUFBa0I7QUFDckIsUUFBSUEsSUFBSixFQUFVO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBTXlGLE1BQU0sR0FBR3pGLElBQUksQ0FBQzBGLEtBQUwsQ0FBVyxHQUFYLENBQWY7QUFDQSxVQUFJRCxNQUFNLENBQUN2QyxNQUFQLEdBQWdCLENBQXBCLEVBQXVCLE9BQU9yQyxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFFdkIsVUFBSStFLFdBQVcsR0FBRyxLQUFsQjs7QUFDQSxVQUFJRixNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVVHLFVBQVYsQ0FBcUIsT0FBckIsS0FBaUNILE1BQU0sQ0FBQyxDQUFELENBQU4sQ0FBVUcsVUFBVixDQUFxQixRQUFyQixDQUFyQyxFQUFxRTtBQUNqRTtBQUNBO0FBQ0EsY0FBTUMsU0FBUyxHQUFHLElBQUlDLEdBQUosQ0FBUUwsTUFBTSxDQUFDLENBQUQsQ0FBZCxDQUFsQjtBQUNBLGNBQU1NLFFBQVEsR0FBR0YsU0FBUyxDQUFDRyxJQUFWLElBQWtCSCxTQUFTLENBQUNFLFFBQTdDLENBSmlFLENBSVY7QUFFdkQ7QUFDQTs7QUFDQSxZQUFJLGlDQUFnQkEsUUFBaEIsQ0FBSixFQUErQjtBQUMzQkosVUFBQUEsV0FBVyxHQUFHLElBQWQ7QUFDSDtBQUNKOztBQUNELFVBQUlGLE1BQU0sQ0FBQyxDQUFELENBQU4sQ0FBVSxDQUFWLE1BQWlCLEdBQXJCLEVBQTBCO0FBQ3RCLFlBQUlRLFNBQVMsR0FBR1IsTUFBTSxDQUFDLENBQUQsQ0FBdEI7O0FBQ0EsWUFBSSxDQUFDUSxTQUFTLENBQUNDLFFBQVYsQ0FBbUIsR0FBbkIsQ0FBTCxFQUE4QjtBQUMxQkQsVUFBQUEsU0FBUyxJQUFJLE1BQU0zRyxpQ0FBZ0JDLEdBQWhCLEdBQXNCNEcsU0FBdEIsRUFBbkI7QUFDSDs7QUFFREMsNEJBQUlDLFFBQUosQ0FBYTtBQUNUQyxVQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUQyxVQUFBQSxVQUFVLEVBQUVOLFNBRkg7QUFHVE8sVUFBQUEsU0FBUyxFQUFFO0FBSEYsU0FBYjs7QUFLQSxlQUFPekYsT0FBTyxFQUFkO0FBQ0gsT0FaRCxNQVlPLElBQUkwRSxNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVUsQ0FBVixNQUFpQixHQUFyQixFQUEwQjtBQUM3QixjQUFNaEYsTUFBTSxHQUFHZ0YsTUFBTSxDQUFDLENBQUQsQ0FBckI7QUFDQSxjQUFNZ0IsVUFBVSxHQUFHaEIsTUFBTSxDQUFDaUIsTUFBUCxDQUFjLENBQWQsQ0FBbkI7O0FBRUFOLDRCQUFJQyxRQUFKLENBQWE7QUFDVEMsVUFBQUEsTUFBTSxFQUFFLFdBREM7QUFFVEssVUFBQUEsT0FBTyxFQUFFbEcsTUFGQTtBQUdUWixVQUFBQSxJQUFJLEVBQUU7QUFDRjtBQUNBNEcsWUFBQUEsVUFBVSxFQUFFQTtBQUZWLFdBSEc7QUFPVEcsVUFBQUEsV0FBVyxFQUFFSCxVQVBKO0FBT2dCO0FBQ3pCRCxVQUFBQSxTQUFTLEVBQUU7QUFSRixTQUFiOztBQVVBLGVBQU96RixPQUFPLEVBQWQ7QUFDSCxPQWZNLE1BZUEsSUFBSTRFLFdBQUosRUFBaUI7QUFDcEIsY0FBTWtCLGNBQWMsR0FBRyxnQ0FBZXBCLE1BQU0sQ0FBQyxDQUFELENBQXJCLENBQXZCLENBRG9CLENBR3BCO0FBQ0E7O0FBQ0EsWUFBSSxDQUFDb0IsY0FBTCxFQUFxQjtBQUNqQixpQkFBT2hHLE1BQU0sQ0FBQyxLQUFLRCxRQUFMLEVBQUQsQ0FBYjtBQUNILFNBUG1CLENBU3BCO0FBQ0E7OztBQUNBLFlBQUksQ0FBQ2lHLGNBQWMsQ0FBQ0MsYUFBcEIsRUFBbUM7QUFDL0IsaUJBQU9qRyxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSDs7QUFFRCxjQUFNbUcsTUFBTSxHQUFHRixjQUFjLENBQUNDLGFBQTlCO0FBQ0EsY0FBTUwsVUFBVSxHQUFHSSxjQUFjLENBQUNKLFVBQWxDO0FBQ0EsY0FBTU8sT0FBTyxHQUFHSCxjQUFjLENBQUNHLE9BQS9CO0FBRUEsY0FBTVgsUUFBUSxHQUFHO0FBQ2JDLFVBQUFBLE1BQU0sRUFBRSxXQURLO0FBRWJFLFVBQUFBLFNBQVMsRUFBRTtBQUZFLFNBQWpCO0FBS0EsWUFBSU8sTUFBTSxDQUFDLENBQUQsQ0FBTixLQUFjLEdBQWxCLEVBQXVCVixRQUFRLENBQUMsU0FBRCxDQUFSLEdBQXNCVSxNQUF0QixDQUF2QixLQUNLVixRQUFRLENBQUMsWUFBRCxDQUFSLEdBQXlCVSxNQUF6Qjs7QUFFTCxZQUFJQyxPQUFKLEVBQWE7QUFDVFgsVUFBQUEsUUFBUSxDQUFDLFVBQUQsQ0FBUixHQUF1QlcsT0FBdkI7QUFDQVgsVUFBQUEsUUFBUSxDQUFDLGFBQUQsQ0FBUixHQUEwQixJQUExQjtBQUNIOztBQUVELFlBQUlJLFVBQUosRUFBZ0I7QUFDWjtBQUNBSixVQUFBQSxRQUFRLENBQUMsTUFBRCxDQUFSLEdBQW1CO0FBQ2Y7QUFDQUksWUFBQUEsVUFBVSxFQUFFQTtBQUZHLFdBQW5CLENBRlksQ0FPWjs7QUFDQUosVUFBQUEsUUFBUSxDQUFDLGFBQUQsQ0FBUixHQUEwQkksVUFBMUI7QUFDSDs7QUFFREwsNEJBQUlDLFFBQUosQ0FBYUEsUUFBYjs7QUFDQSxlQUFPdEYsT0FBTyxFQUFkO0FBQ0g7QUFDSjs7QUFDRCxXQUFPRixNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQTlHTztBQStHUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBL0dwQixDQUFaLENBblRvQixFQW9hcEIsSUFBSTdCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsTUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsZ0JBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLFlBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsVUFBTXlCLEdBQUcsR0FBR25DLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFFQSxRQUFJMEgsWUFBSjs7QUFDQSxRQUFJakgsSUFBSixFQUFVO0FBQ04sWUFBTTBFLE9BQU8sR0FBRzFFLElBQUksQ0FBQzJFLEtBQUwsQ0FBVyxTQUFYLENBQWhCOztBQUNBLFVBQUlELE9BQUosRUFBYTtBQUNULFlBQUl1QixTQUFTLEdBQUd2QixPQUFPLENBQUMsQ0FBRCxDQUF2QjtBQUNBLFlBQUl1QixTQUFTLENBQUMsQ0FBRCxDQUFULEtBQWlCLEdBQXJCLEVBQTBCLE9BQU9wRixNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7O0FBRTFCLFlBQUksQ0FBQ3FGLFNBQVMsQ0FBQ0MsUUFBVixDQUFtQixHQUFuQixDQUFMLEVBQThCO0FBQzFCRCxVQUFBQSxTQUFTLElBQUksTUFBTXhFLEdBQUcsQ0FBQzBFLFNBQUosRUFBbkI7QUFDSCxTQU5RLENBUVQ7OztBQUNBLGNBQU1lLEtBQUssR0FBR3pGLEdBQUcsQ0FBQzBGLFFBQUosRUFBZDs7QUFDQSxhQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdGLEtBQUssQ0FBQ2hFLE1BQTFCLEVBQWtDa0UsQ0FBQyxFQUFuQyxFQUF1QztBQUNuQyxnQkFBTUMsV0FBVyxHQUFHSCxLQUFLLENBQUNFLENBQUQsQ0FBTCxDQUFTeEYsWUFBVCxDQUFzQjZCLGNBQXRCLENBQXFDLGdCQUFyQyxDQUFwQjs7QUFDQSxlQUFLLElBQUk2RCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxXQUFXLENBQUNuRSxNQUFoQyxFQUF3Q29FLENBQUMsRUFBekMsRUFBNkM7QUFDekMsa0JBQU12SCxPQUFPLEdBQUdzSCxXQUFXLENBQUNDLENBQUQsQ0FBWCxDQUFlM0QsVUFBZixHQUE0QjVELE9BQTVCLElBQXVDLEVBQXZEOztBQUNBLGlCQUFLLElBQUl3SCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHeEgsT0FBTyxDQUFDbUQsTUFBNUIsRUFBb0NxRSxDQUFDLEVBQXJDLEVBQXlDO0FBQ3JDLGtCQUFJeEgsT0FBTyxDQUFDd0gsQ0FBRCxDQUFQLEtBQWV0QixTQUFuQixFQUE4QjtBQUMxQmdCLGdCQUFBQSxZQUFZLEdBQUdDLEtBQUssQ0FBQ0UsQ0FBRCxDQUFMLENBQVMzRyxNQUF4QjtBQUNBO0FBQ0g7QUFDSjs7QUFDRCxnQkFBSXdHLFlBQUosRUFBa0I7QUFDckI7O0FBQ0QsY0FBSUEsWUFBSixFQUFrQjtBQUNyQjs7QUFDRCxZQUFJLENBQUNBLFlBQUwsRUFBbUIsT0FBT3BHLE1BQU0sQ0FBQyx5QkFBRywwQkFBSCxJQUFpQyxHQUFqQyxHQUF1Q29GLFNBQXhDLENBQWI7QUFDdEI7QUFDSjs7QUFFRCxRQUFJLENBQUNnQixZQUFMLEVBQW1CQSxZQUFZLEdBQUd4RyxNQUFmO0FBQ25CLFdBQU9NLE9BQU8sQ0FDVlUsR0FBRyxDQUFDK0YsY0FBSixDQUFtQlAsWUFBbkIsRUFBaUNoRixJQUFqQyxDQUFzQyxZQUFXO0FBQzdDbUUsMEJBQUlDLFFBQUosQ0FBYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUU7QUFBVCxPQUFiO0FBQ0gsS0FGRCxDQURVLENBQWQ7QUFLSCxHQTVDTztBQTZDUm5HLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM4QjtBQTdDcEIsQ0FBWixDQXBhb0IsRUFtZHBCLElBQUk3QixPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLE1BREQ7QUFFUkUsRUFBQUEsSUFBSSxFQUFFLG9CQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSwwQkFBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJQSxJQUFKLEVBQVU7QUFDTixZQUFNMEUsT0FBTyxHQUFHMUUsSUFBSSxDQUFDMkUsS0FBTCxDQUFXLG1CQUFYLENBQWhCOztBQUNBLFVBQUlELE9BQUosRUFBYTtBQUNULGVBQU8zRCxPQUFPLENBQUN6QixpQ0FBZ0JDLEdBQWhCLEdBQXNCa0ksSUFBdEIsQ0FBMkJoSCxNQUEzQixFQUFtQ2lFLE9BQU8sQ0FBQyxDQUFELENBQTFDLEVBQStDQSxPQUFPLENBQUMsQ0FBRCxDQUF0RCxDQUFELENBQWQ7QUFDSDtBQUNKOztBQUNELFdBQU83RCxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQVpPO0FBYVJULEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM2RDtBQWJwQixDQUFaLENBbmRvQixFQWtlcEIsSUFBSTVELE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsS0FERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsb0JBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLHlCQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU0wRSxPQUFPLEdBQUcxRSxJQUFJLENBQUMyRSxLQUFMLENBQVcsbUJBQVgsQ0FBaEI7O0FBQ0EsVUFBSUQsT0FBSixFQUFhO0FBQ1QsZUFBTzNELE9BQU8sQ0FBQ3pCLGlDQUFnQkMsR0FBaEIsR0FBc0JtSSxHQUF0QixDQUEwQmpILE1BQTFCLEVBQWtDaUUsT0FBTyxDQUFDLENBQUQsQ0FBekMsRUFBOENBLE9BQU8sQ0FBQyxDQUFELENBQXJELENBQUQsQ0FBZDtBQUNIO0FBQ0o7O0FBQ0QsV0FBTzdELE1BQU0sQ0FBQyxLQUFLRCxRQUFMLEVBQUQsQ0FBYjtBQUNILEdBWk87QUFhUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzZEO0FBYnBCLENBQVosQ0FsZW9CLEVBaWZwQixJQUFJNUQsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxPQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxXQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSwyQkFBSixDQUhMO0FBSVJDLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJQSxJQUFKLEVBQVU7QUFDTixZQUFNMEUsT0FBTyxHQUFHMUUsSUFBSSxDQUFDMkUsS0FBTCxDQUFXLFNBQVgsQ0FBaEI7O0FBQ0EsVUFBSUQsT0FBSixFQUFhO0FBQ1Q7QUFDQSxlQUFPM0QsT0FBTyxDQUFDekIsaUNBQWdCQyxHQUFoQixHQUFzQm9JLEtBQXRCLENBQTRCbEgsTUFBNUIsRUFBb0NpRSxPQUFPLENBQUMsQ0FBRCxDQUEzQyxDQUFELENBQWQ7QUFDSDtBQUNKOztBQUNELFdBQU83RCxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQWJPO0FBY1JULEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM2RDtBQWRwQixDQUFaLENBamZvQixFQWlnQnBCLElBQUk1RCxPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLFFBREQ7QUFFUkUsRUFBQUEsSUFBSSxFQUFFLFdBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLGdEQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU15QixHQUFHLEdBQUduQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBRUEsWUFBTW1GLE9BQU8sR0FBRzFFLElBQUksQ0FBQzJFLEtBQUwsQ0FBVyxTQUFYLENBQWhCOztBQUNBLFVBQUlELE9BQUosRUFBYTtBQUNULGNBQU0zQixNQUFNLEdBQUcyQixPQUFPLENBQUMsQ0FBRCxDQUF0QjtBQUNBLGNBQU1rRCxZQUFZLEdBQUduRyxHQUFHLENBQUNvRyxlQUFKLEVBQXJCO0FBQ0FELFFBQUFBLFlBQVksQ0FBQ0UsSUFBYixDQUFrQi9FLE1BQWxCLEVBSFMsQ0FHa0I7O0FBQzNCLGVBQU9oQyxPQUFPLENBQ1ZVLEdBQUcsQ0FBQ3NHLGVBQUosQ0FBb0JILFlBQXBCLEVBQWtDM0YsSUFBbEMsQ0FBdUMsTUFBTTtBQUN6QyxnQkFBTXFDLFVBQVUsR0FBR3RGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBQ0FDLHlCQUFNQyxtQkFBTixDQUEwQixnQkFBMUIsRUFBNEMsY0FBNUMsRUFBNERtRixVQUE1RCxFQUF3RTtBQUNwRS9DLFlBQUFBLEtBQUssRUFBRSx5QkFBRyxjQUFILENBRDZEO0FBRXBFdEIsWUFBQUEsV0FBVyxFQUFFLGlDQUNULCtCQUFLLHlCQUFHLGlDQUFILEVBQXNDO0FBQUM4QyxjQUFBQTtBQUFELGFBQXRDLENBQUwsQ0FEUztBQUZ1RCxXQUF4RTtBQU1ILFNBUkQsQ0FEVSxDQUFkO0FBV0g7QUFDSjs7QUFDRCxXQUFPbEMsTUFBTSxDQUFDLEtBQUtELFFBQUwsRUFBRCxDQUFiO0FBQ0gsR0EzQk87QUE0QlJULEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM4QjtBQTVCcEIsQ0FBWixDQWpnQm9CLEVBK2hCcEIsSUFBSTdCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsVUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsV0FGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksNkRBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSUEsSUFBSixFQUFVO0FBQ04sWUFBTXlCLEdBQUcsR0FBR25DLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFFQSxZQUFNbUYsT0FBTyxHQUFHMUUsSUFBSSxDQUFDMkUsS0FBTCxDQUFXLFNBQVgsQ0FBaEI7O0FBQ0EsVUFBSUQsT0FBSixFQUFhO0FBQ1QsY0FBTTNCLE1BQU0sR0FBRzJCLE9BQU8sQ0FBQyxDQUFELENBQXRCO0FBQ0EsY0FBTWtELFlBQVksR0FBR25HLEdBQUcsQ0FBQ29HLGVBQUosRUFBckI7QUFDQSxjQUFNRyxLQUFLLEdBQUdKLFlBQVksQ0FBQ0ssT0FBYixDQUFxQmxGLE1BQXJCLENBQWQ7QUFDQSxZQUFJaUYsS0FBSyxLQUFLLENBQUMsQ0FBZixFQUFrQkosWUFBWSxDQUFDbEIsTUFBYixDQUFvQnNCLEtBQXBCLEVBQTJCLENBQTNCO0FBQ2xCLGVBQU9qSCxPQUFPLENBQ1ZVLEdBQUcsQ0FBQ3NHLGVBQUosQ0FBb0JILFlBQXBCLEVBQWtDM0YsSUFBbEMsQ0FBdUMsTUFBTTtBQUN6QyxnQkFBTXFDLFVBQVUsR0FBR3RGLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixvQkFBakIsQ0FBbkI7O0FBQ0FDLHlCQUFNQyxtQkFBTixDQUEwQixnQkFBMUIsRUFBNEMsZ0JBQTVDLEVBQThEbUYsVUFBOUQsRUFBMEU7QUFDdEUvQyxZQUFBQSxLQUFLLEVBQUUseUJBQUcsZ0JBQUgsQ0FEK0Q7QUFFdEV0QixZQUFBQSxXQUFXLEVBQUUsaUNBQ1QsK0JBQUsseUJBQUcsdUNBQUgsRUFBNEM7QUFBQzhDLGNBQUFBO0FBQUQsYUFBNUMsQ0FBTCxDQURTO0FBRnlELFdBQTFFO0FBTUgsU0FSRCxDQURVLENBQWQ7QUFXSDtBQUNKOztBQUNELFdBQU9sQyxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQTVCTztBQTZCUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQzhCO0FBN0JwQixDQUFaLENBL2hCb0IsRUE4akJwQixJQUFJN0IsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxJQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSwyQkFGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksa0NBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSUEsSUFBSixFQUFVO0FBQ04sWUFBTTBFLE9BQU8sR0FBRzFFLElBQUksQ0FBQzJFLEtBQUwsQ0FBVyxzQkFBWCxDQUFoQjtBQUNBLFVBQUl1RCxVQUFVLEdBQUcsRUFBakIsQ0FGTSxDQUVlOztBQUNyQixVQUFJeEQsT0FBSixFQUFhO0FBQ1QsY0FBTTNCLE1BQU0sR0FBRzJCLE9BQU8sQ0FBQyxDQUFELENBQXRCOztBQUNBLFlBQUlBLE9BQU8sQ0FBQ3hCLE1BQVIsS0FBbUIsQ0FBbkIsSUFBd0JpRixTQUFTLEtBQUt6RCxPQUFPLENBQUMsQ0FBRCxDQUFqRCxFQUFzRDtBQUNsRHdELFVBQUFBLFVBQVUsR0FBR0UsUUFBUSxDQUFDMUQsT0FBTyxDQUFDLENBQUQsQ0FBUixFQUFhLEVBQWIsQ0FBckI7QUFDSDs7QUFDRCxZQUFJLENBQUMyRCxLQUFLLENBQUNILFVBQUQsQ0FBVixFQUF3QjtBQUNwQixnQkFBTXpHLEdBQUcsR0FBR25DLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFDQSxnQkFBTW1DLElBQUksR0FBR0QsR0FBRyxDQUFDRSxPQUFKLENBQVlsQixNQUFaLENBQWI7QUFDQSxjQUFJLENBQUNpQixJQUFMLEVBQVcsT0FBT2IsTUFBTSxDQUFDLGtCQUFrQkosTUFBbkIsQ0FBYjtBQUVYLGdCQUFNNkgsZUFBZSxHQUFHNUcsSUFBSSxDQUFDRSxZQUFMLENBQWtCNkIsY0FBbEIsQ0FBaUMscUJBQWpDLEVBQXdELEVBQXhELENBQXhCO0FBQ0EsaUJBQU8xQyxPQUFPLENBQUNVLEdBQUcsQ0FBQzhHLGFBQUosQ0FBa0I5SCxNQUFsQixFQUEwQnNDLE1BQTFCLEVBQWtDbUYsVUFBbEMsRUFBOENJLGVBQTlDLENBQUQsQ0FBZDtBQUNIO0FBQ0o7QUFDSjs7QUFDRCxXQUFPekgsTUFBTSxDQUFDLEtBQUtELFFBQUwsRUFBRCxDQUFiO0FBQ0gsR0F4Qk87QUF5QlJULEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM2RDtBQXpCcEIsQ0FBWixDQTlqQm9CLEVBeWxCcEIsSUFBSTVELE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsTUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsV0FGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksMEJBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSUEsSUFBSixFQUFVO0FBQ04sWUFBTTBFLE9BQU8sR0FBRzFFLElBQUksQ0FBQzJFLEtBQUwsQ0FBVyxTQUFYLENBQWhCOztBQUNBLFVBQUlELE9BQUosRUFBYTtBQUNULGNBQU1qRCxHQUFHLEdBQUduQyxpQ0FBZ0JDLEdBQWhCLEVBQVo7O0FBQ0EsY0FBTW1DLElBQUksR0FBR0QsR0FBRyxDQUFDRSxPQUFKLENBQVlsQixNQUFaLENBQWI7QUFDQSxZQUFJLENBQUNpQixJQUFMLEVBQVcsT0FBT2IsTUFBTSxDQUFDLGtCQUFrQkosTUFBbkIsQ0FBYjtBQUVYLGNBQU02SCxlQUFlLEdBQUc1RyxJQUFJLENBQUNFLFlBQUwsQ0FBa0I2QixjQUFsQixDQUFpQyxxQkFBakMsRUFBd0QsRUFBeEQsQ0FBeEI7QUFDQSxlQUFPMUMsT0FBTyxDQUFDVSxHQUFHLENBQUM4RyxhQUFKLENBQWtCOUgsTUFBbEIsRUFBMEJULElBQTFCLEVBQWdDbUksU0FBaEMsRUFBMkNHLGVBQTNDLENBQUQsQ0FBZDtBQUNIO0FBQ0o7O0FBQ0QsV0FBT3pILE1BQU0sQ0FBQyxLQUFLRCxRQUFMLEVBQUQsQ0FBYjtBQUNILEdBakJPO0FBa0JSVCxFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDNkQ7QUFsQnBCLENBQVosQ0F6bEJvQixFQTZtQnBCLElBQUk1RCxPQUFKLENBQVk7QUFDUkcsRUFBQUEsT0FBTyxFQUFFLFVBREQ7QUFFUkcsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLGtDQUFKLENBRkw7QUFHUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUI7QUFDcEIsVUFBTStILGNBQWMsR0FBR3hKLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQix3QkFBakIsQ0FBdkI7O0FBQ0FDLG1CQUFNdUosWUFBTixDQUFtQkQsY0FBbkIsRUFBbUM7QUFBQy9ILE1BQUFBO0FBQUQsS0FBbkM7O0FBQ0EsV0FBT00sT0FBTyxFQUFkO0FBQ0gsR0FQTztBQVFSWixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDZ0o7QUFScEIsQ0FBWixDQTdtQm9CLEVBdW5CcEIsSUFBSS9JLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsV0FERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsT0FGRTtBQUdSQyxFQUFBQSxXQUFXLEVBQUUsMEJBQUkseUNBQUosQ0FITDtBQUlSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSSxDQUFDQSxJQUFELElBQVUsQ0FBQ0EsSUFBSSxDQUFDNEYsVUFBTCxDQUFnQixVQUFoQixDQUFELElBQWdDLENBQUM1RixJQUFJLENBQUM0RixVQUFMLENBQWdCLFNBQWhCLENBQS9DLEVBQTRFO0FBQ3hFLGFBQU8vRSxNQUFNLENBQUMseUJBQUcsZ0RBQUgsQ0FBRCxDQUFiO0FBQ0g7O0FBQ0QsUUFBSThILHFCQUFZQyxvQkFBWixDQUFpQ25JLE1BQWpDLENBQUosRUFBOEM7QUFDMUMsWUFBTXNDLE1BQU0sR0FBR3pELGlDQUFnQkMsR0FBaEIsR0FBc0IwRCxTQUF0QixFQUFmOztBQUNBLFlBQU00RixLQUFLLEdBQUksSUFBSUMsSUFBSixFQUFELENBQWFDLE9BQWIsRUFBZDtBQUNBLFlBQU1DLFFBQVEsR0FBR0Msa0JBQWtCLFdBQUl4SSxNQUFKLGNBQWNzQyxNQUFkLGNBQXdCOEYsS0FBeEIsRUFBbkM7QUFDQSxhQUFPOUgsT0FBTyxDQUFDNEgscUJBQVlPLGFBQVosQ0FDWHpJLE1BRFcsRUFDSHVJLFFBREcsRUFDTyxVQURQLEVBQ21CaEosSUFEbkIsRUFDeUIsZUFEekIsRUFDMEMsRUFEMUMsQ0FBRCxDQUFkO0FBRUgsS0FORCxNQU1PO0FBQ0gsYUFBT2EsTUFBTSxDQUFDLHlCQUFHLHlDQUFILENBQUQsQ0FBYjtBQUNIO0FBQ0osR0FqQk87QUFrQlJWLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUM2RDtBQWxCcEIsQ0FBWixDQXZuQm9CLEVBMm9CcEIsSUFBSTVELE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsUUFERDtBQUVSRSxFQUFBQSxJQUFJLEVBQUUsNENBRkU7QUFHUkMsRUFBQUEsV0FBVyxFQUFFLDBCQUFJLDRDQUFKLENBSEw7QUFJUkMsRUFBQUEsS0FBSyxFQUFFLFVBQVNPLE1BQVQsRUFBaUJULElBQWpCLEVBQXVCO0FBQzFCLFFBQUlBLElBQUosRUFBVTtBQUNOLFlBQU0wRSxPQUFPLEdBQUcxRSxJQUFJLENBQUMyRSxLQUFMLENBQVcsdUJBQVgsQ0FBaEI7O0FBQ0EsVUFBSUQsT0FBSixFQUFhO0FBQ1QsY0FBTWpELEdBQUcsR0FBR25DLGlDQUFnQkMsR0FBaEIsRUFBWjs7QUFFQSxjQUFNd0QsTUFBTSxHQUFHMkIsT0FBTyxDQUFDLENBQUQsQ0FBdEI7QUFDQSxjQUFNeUUsUUFBUSxHQUFHekUsT0FBTyxDQUFDLENBQUQsQ0FBeEI7QUFDQSxjQUFNMEUsV0FBVyxHQUFHMUUsT0FBTyxDQUFDLENBQUQsQ0FBM0I7QUFFQSxlQUFPM0QsT0FBTyxDQUFDLENBQUMsWUFBWTtBQUN4QixnQkFBTXNJLE1BQU0sR0FBRyxNQUFNNUgsR0FBRyxDQUFDNkgsZUFBSixDQUFvQnZHLE1BQXBCLEVBQTRCb0csUUFBNUIsQ0FBckI7O0FBQ0EsY0FBSSxDQUFDRSxNQUFMLEVBQWE7QUFDVCxrQkFBTSxJQUFJbEUsS0FBSixDQUFVLHlCQUFHLCtCQUFILGdCQUEyQ3BDLE1BQTNDLGVBQXNEb0csUUFBdEQsTUFBVixDQUFOO0FBQ0g7O0FBQ0QsZ0JBQU1JLFdBQVcsR0FBRyxNQUFNOUgsR0FBRyxDQUFDK0gsZ0JBQUosQ0FBcUJ6RyxNQUFyQixFQUE2Qm9HLFFBQTdCLENBQTFCOztBQUVBLGNBQUlJLFdBQVcsQ0FBQ0UsVUFBWixFQUFKLEVBQThCO0FBQzFCLGdCQUFJSixNQUFNLENBQUNLLGNBQVAsT0FBNEJOLFdBQWhDLEVBQTZDO0FBQ3pDLG9CQUFNLElBQUlqRSxLQUFKLENBQVUseUJBQUcsMkJBQUgsQ0FBVixDQUFOO0FBQ0gsYUFGRCxNQUVPO0FBQ0gsb0JBQU0sSUFBSUEsS0FBSixDQUFVLHlCQUFHLDJEQUFILENBQVYsQ0FBTjtBQUNIO0FBQ0o7O0FBRUQsY0FBSWtFLE1BQU0sQ0FBQ0ssY0FBUCxPQUE0Qk4sV0FBaEMsRUFBNkM7QUFDekMsa0JBQU1PLE1BQU0sR0FBR04sTUFBTSxDQUFDSyxjQUFQLEVBQWY7QUFDQSxrQkFBTSxJQUFJdkUsS0FBSixDQUNGLHlCQUFHLGlGQUNDLHNFQURELEdBRUMsK0VBRkosRUFHSTtBQUNJd0UsY0FBQUEsTUFESjtBQUVJNUcsY0FBQUEsTUFGSjtBQUdJb0csY0FBQUEsUUFISjtBQUlJQyxjQUFBQTtBQUpKLGFBSEosQ0FERSxDQUFOO0FBVUg7O0FBRUQsZ0JBQU0zSCxHQUFHLENBQUNtSSxpQkFBSixDQUFzQjdHLE1BQXRCLEVBQThCb0csUUFBOUIsRUFBd0MsSUFBeEMsQ0FBTixDQTdCd0IsQ0ErQnhCOztBQUNBLGdCQUFNN0UsVUFBVSxHQUFHdEYsR0FBRyxDQUFDQyxZQUFKLENBQWlCLG9CQUFqQixDQUFuQjs7QUFDQUMseUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxjQUE1QyxFQUE0RG1GLFVBQTVELEVBQXdFO0FBQ3BFL0MsWUFBQUEsS0FBSyxFQUFFLHlCQUFHLGNBQUgsQ0FENkQ7QUFFcEV0QixZQUFBQSxXQUFXLEVBQUUsaUNBQ1QsK0JBRVEseUJBQUcsdUVBQ0Msc0VBREosRUFFSTtBQUFDOEMsY0FBQUEsTUFBRDtBQUFTb0csY0FBQUE7QUFBVCxhQUZKLENBRlIsQ0FEUztBQUZ1RCxXQUF4RTtBQVlILFNBN0NjLEdBQUQsQ0FBZDtBQThDSDtBQUNKOztBQUNELFdBQU90SSxNQUFNLENBQUMsS0FBS0QsUUFBTCxFQUFELENBQWI7QUFDSCxHQS9ETztBQWdFUlQsRUFBQUEsUUFBUSxFQUFFVCxpQkFBaUIsQ0FBQ2dKO0FBaEVwQixDQUFaLENBM29Cb0IsRUE2c0JwQixJQUFJL0ksT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxnQkFERDtBQUVSRyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksZ0ZBQUosQ0FGTDtBQUdSQyxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQjtBQUNwQixRQUFJO0FBQ0FuQix1Q0FBZ0JDLEdBQWhCLEdBQXNCc0ssbUJBQXRCLENBQTBDcEosTUFBMUM7QUFDSCxLQUZELENBRUUsT0FBTzRDLENBQVAsRUFBVTtBQUNSLGFBQU94QyxNQUFNLENBQUN3QyxDQUFDLENBQUNuQyxPQUFILENBQWI7QUFDSDs7QUFDRCxXQUFPSCxPQUFPLEVBQWQ7QUFDSCxHQVZPO0FBV1JaLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUNnSjtBQVhwQixDQUFaLENBN3NCb0IsRUEwdEJwQixJQUFJL0ksT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxTQUREO0FBRVJHLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSwrQ0FBSixDQUZMO0FBR1JELEVBQUFBLElBQUksRUFBRSxXQUhFO0FBSVJFLEVBQUFBLEtBQUssRUFBRSxVQUFTTyxNQUFULEVBQWlCVCxJQUFqQixFQUF1QjtBQUMxQixRQUFJLENBQUNBLElBQUwsRUFBVyxPQUFPYSxNQUFNLENBQUMsS0FBS29DLFNBQUwsRUFBRCxDQUFiO0FBQ1gsV0FBT2xDLE9BQU8sQ0FBQ3pCLGlDQUFnQkMsR0FBaEIsR0FBc0I4QixlQUF0QixDQUFzQ1osTUFBdEMsRUFBOENULElBQTlDLEVBQW9ELCtCQUFrQkEsSUFBbEIsQ0FBcEQsQ0FBRCxDQUFkO0FBQ0gsR0FQTztBQVFSRyxFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDMEI7QUFScEIsQ0FBWixDQTF0Qm9CLEVBb3VCcEIsSUFBSXpCLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsV0FERDtBQUVSRyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksNkNBQUosQ0FGTDtBQUdSRCxFQUFBQSxJQUFJLEVBQUUsV0FIRTtBQUlSRSxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQlQsSUFBakIsRUFBdUI7QUFDMUIsUUFBSSxDQUFDQSxJQUFMLEVBQVcsT0FBT2EsTUFBTSxDQUFDLEtBQUtvQyxTQUFMLEVBQUQsQ0FBYjtBQUNYLFdBQU9sQyxPQUFPLENBQUN6QixpQ0FBZ0JDLEdBQWhCLEdBQXNCdUssYUFBdEIsQ0FBb0NySixNQUFwQyxFQUE0Q1QsSUFBNUMsRUFBa0QsK0JBQWtCQSxJQUFsQixDQUFsRCxDQUFELENBQWQ7QUFDSCxHQVBPO0FBUVJHLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUMwQjtBQVJwQixDQUFaLENBcHVCb0IsRUE4dUJwQixJQUFJekIsT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxNQUREO0FBRVJHLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSx3REFBSixDQUZMO0FBR1JDLEVBQUFBLEtBQUssRUFBRSxZQUFXO0FBQ2QsVUFBTTZKLHNCQUFzQixHQUFHL0ssR0FBRyxDQUFDQyxZQUFKLENBQWlCLGdDQUFqQixDQUEvQjs7QUFFQUMsbUJBQU1DLG1CQUFOLENBQTBCLGdCQUExQixFQUE0QyxNQUE1QyxFQUFvRDRLLHNCQUFwRDs7QUFDQSxXQUFPaEosT0FBTyxFQUFkO0FBQ0gsR0FSTztBQVNSWixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDZ0o7QUFUcEIsQ0FBWixDQTl1Qm9CLEVBeXZCcEIsSUFBSS9JLE9BQUosQ0FBWTtBQUNSRyxFQUFBQSxPQUFPLEVBQUUsT0FERDtBQUVSRyxFQUFBQSxXQUFXLEVBQUUsMEJBQUksbUNBQUosQ0FGTDtBQUdSRCxFQUFBQSxJQUFJLEVBQUUsV0FIRTtBQUlSRSxFQUFBQSxLQUFLLEVBQUUsVUFBU08sTUFBVCxFQUFpQnNDLE1BQWpCLEVBQXlCO0FBQzVCLFFBQUksQ0FBQ0EsTUFBRCxJQUFXLENBQUNBLE1BQU0sQ0FBQzZDLFVBQVAsQ0FBa0IsR0FBbEIsQ0FBWixJQUFzQyxDQUFDN0MsTUFBTSxDQUFDbUQsUUFBUCxDQUFnQixHQUFoQixDQUEzQyxFQUFpRTtBQUM3RCxhQUFPckYsTUFBTSxDQUFDLEtBQUtELFFBQUwsRUFBRCxDQUFiO0FBQ0g7O0FBRUQsVUFBTW9KLE1BQU0sR0FBRzFLLGlDQUFnQkMsR0FBaEIsR0FBc0JvQyxPQUF0QixDQUE4QmxCLE1BQTlCLEVBQXNDd0osU0FBdEMsQ0FBZ0RsSCxNQUFoRCxDQUFmOztBQUNBcUQsd0JBQUlDLFFBQUosQ0FBYTtBQUNUQyxNQUFBQSxNQUFNLEVBQUUsV0FEQztBQUVUMEQsTUFBQUEsTUFBTSxFQUFFQSxNQUFNLElBQUk7QUFBQ2pILFFBQUFBO0FBQUQ7QUFGVCxLQUFiOztBQUlBLFdBQU9oQyxPQUFPLEVBQWQ7QUFDSCxHQWZPO0FBZ0JSWixFQUFBQSxRQUFRLEVBQUVULGlCQUFpQixDQUFDZ0o7QUFoQnBCLENBQVosQ0F6dkJvQixFQTR3QnBCO0FBQ0E7QUFDQSxJQUFJL0ksT0FBSixDQUFZO0FBQ1JHLEVBQUFBLE9BQU8sRUFBRSxJQUREO0FBRVJFLEVBQUFBLElBQUksRUFBRSxXQUZFO0FBR1JDLEVBQUFBLFdBQVcsRUFBRSwwQkFBSSxpQkFBSixDQUhMO0FBSVJFLEVBQUFBLFFBQVEsRUFBRVQsaUJBQWlCLENBQUMwQixRQUpwQjtBQUtSZixFQUFBQSx3QkFBd0IsRUFBRTtBQUxsQixDQUFaLENBOXdCb0IsQ0FBakIsQyxDQXV4QlA7OztBQUNPLE1BQU02SixVQUFVLEdBQUcsSUFBSUMsR0FBSixFQUFuQjs7QUFDUGxKLFFBQVEsQ0FBQ21KLE9BQVQsQ0FBaUIxSixHQUFHLElBQUk7QUFDcEJ3SixFQUFBQSxVQUFVLENBQUNHLEdBQVgsQ0FBZTNKLEdBQUcsQ0FBQ1osT0FBbkIsRUFBNEJZLEdBQTVCO0FBQ0FBLEVBQUFBLEdBQUcsQ0FBQ1gsT0FBSixDQUFZcUssT0FBWixDQUFvQkUsS0FBSyxJQUFJO0FBQ3pCSixJQUFBQSxVQUFVLENBQUNHLEdBQVgsQ0FBZUMsS0FBZixFQUFzQjVKLEdBQXRCO0FBQ0gsR0FGRDtBQUdILENBTEQ7QUFRQTs7Ozs7Ozs7O0FBUU8sU0FBU0osVUFBVCxDQUFvQkcsTUFBcEIsRUFBNEI4SixLQUE1QixFQUFtQztBQUN0QztBQUNBO0FBQ0FBLEVBQUFBLEtBQUssR0FBR0EsS0FBSyxDQUFDQyxPQUFOLENBQWMsTUFBZCxFQUFzQixFQUF0QixDQUFSO0FBQ0EsTUFBSUQsS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFhLEdBQWpCLEVBQXNCLE9BQU8sSUFBUCxDQUpnQixDQUlIOztBQUVuQyxRQUFNRSxJQUFJLEdBQUdGLEtBQUssQ0FBQzVGLEtBQU4sQ0FBWSwwQkFBWixDQUFiO0FBQ0EsTUFBSWpFLEdBQUo7QUFDQSxNQUFJVixJQUFKOztBQUNBLE1BQUl5SyxJQUFKLEVBQVU7QUFDTi9KLElBQUFBLEdBQUcsR0FBRytKLElBQUksQ0FBQyxDQUFELENBQUosQ0FBUUMsU0FBUixDQUFrQixDQUFsQixFQUFxQkMsV0FBckIsRUFBTjtBQUNBM0ssSUFBQUEsSUFBSSxHQUFHeUssSUFBSSxDQUFDLENBQUQsQ0FBWDtBQUNILEdBSEQsTUFHTztBQUNIL0osSUFBQUEsR0FBRyxHQUFHNkosS0FBTjtBQUNIOztBQUVELE1BQUlMLFVBQVUsQ0FBQ1UsR0FBWCxDQUFlbEssR0FBZixDQUFKLEVBQXlCO0FBQ3JCLFdBQU8sTUFBTXdKLFVBQVUsQ0FBQzNLLEdBQVgsQ0FBZW1CLEdBQWYsRUFBb0JGLEdBQXBCLENBQXdCQyxNQUF4QixFQUFnQ1QsSUFBaEMsRUFBc0NVLEdBQXRDLENBQWI7QUFDSDtBQUNKIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuQ29weXJpZ2h0IDIwMTUsIDIwMTYgT3Blbk1hcmtldCBMdGRcclxuQ29weXJpZ2h0IDIwMTggTmV3IFZlY3RvciBMdGRcclxuQ29weXJpZ2h0IDIwMTkgTWljaGFlbCBUZWxhdHluc2tpIDw3dDNjaGd1eUBnbWFpbC5jb20+XHJcbkNvcHlyaWdodCAyMDIwIFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuXHJcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCB7TWF0cml4Q2xpZW50UGVnfSBmcm9tICcuL01hdHJpeENsaWVudFBlZyc7XHJcbmltcG9ydCBkaXMgZnJvbSAnLi9kaXNwYXRjaGVyJztcclxuaW1wb3J0ICogYXMgc2RrIGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQge190LCBfdGR9IGZyb20gJy4vbGFuZ3VhZ2VIYW5kbGVyJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJy4vTW9kYWwnO1xyXG5pbXBvcnQgTXVsdGlJbnZpdGVyIGZyb20gJy4vdXRpbHMvTXVsdGlJbnZpdGVyJztcclxuaW1wb3J0IHsgbGlua2lmeUFuZFNhbml0aXplSHRtbCB9IGZyb20gJy4vSHRtbFV0aWxzJztcclxuaW1wb3J0IFF1ZXN0aW9uRGlhbG9nIGZyb20gXCIuL2NvbXBvbmVudHMvdmlld3MvZGlhbG9ncy9RdWVzdGlvbkRpYWxvZ1wiO1xyXG5pbXBvcnQgV2lkZ2V0VXRpbHMgZnJvbSBcIi4vdXRpbHMvV2lkZ2V0VXRpbHNcIjtcclxuaW1wb3J0IHt0ZXh0VG9IdG1sUmFpbmJvd30gZnJvbSBcIi4vdXRpbHMvY29sb3VyXCI7XHJcbmltcG9ydCB7IGdldEFkZHJlc3NUeXBlIH0gZnJvbSAnLi9Vc2VyQWRkcmVzcyc7XHJcbmltcG9ydCB7IGFiYnJldmlhdGVVcmwgfSBmcm9tICcuL3V0aWxzL1VybFV0aWxzJztcclxuaW1wb3J0IHsgZ2V0RGVmYXVsdElkZW50aXR5U2VydmVyVXJsLCB1c2VEZWZhdWx0SWRlbnRpdHlTZXJ2ZXIgfSBmcm9tICcuL3V0aWxzL0lkZW50aXR5U2VydmVyVXRpbHMnO1xyXG5pbXBvcnQge2lzUGVybWFsaW5rSG9zdCwgcGFyc2VQZXJtYWxpbmt9IGZyb20gXCIuL3V0aWxzL3Blcm1hbGlua3MvUGVybWFsaW5rc1wiO1xyXG5pbXBvcnQge2ludml0ZVVzZXJzVG9Sb29tfSBmcm9tIFwiLi9Sb29tSW52aXRlXCI7XHJcblxyXG4vLyBYWFg6IHdvcmthcm91bmQgZm9yIGh0dHBzOi8vZ2l0aHViLmNvbS9taWNyb3NvZnQvVHlwZVNjcmlwdC9pc3N1ZXMvMzE4MTZcclxuaW50ZXJmYWNlIEhUTUxJbnB1dEV2ZW50IGV4dGVuZHMgRXZlbnQge1xyXG4gICAgdGFyZ2V0OiBIVE1MSW5wdXRFbGVtZW50ICYgRXZlbnRUYXJnZXQ7XHJcbn1cclxuXHJcbmNvbnN0IHNpbmdsZU14Y1VwbG9hZCA9IGFzeW5jICgpOiBQcm9taXNlPGFueT4gPT4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgY29uc3QgZmlsZVNlbGVjdG9yID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKTtcclxuICAgICAgICBmaWxlU2VsZWN0b3Iuc2V0QXR0cmlidXRlKCd0eXBlJywgJ2ZpbGUnKTtcclxuICAgICAgICBmaWxlU2VsZWN0b3Iub25jaGFuZ2UgPSAoZXY6IEhUTUxJbnB1dEV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGUgPSBldi50YXJnZXQuZmlsZXNbMF07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBVcGxvYWRDb25maXJtRGlhbG9nID0gc2RrLmdldENvbXBvbmVudChcImRpYWxvZ3MuVXBsb2FkQ29uZmlybURpYWxvZ1wiKTtcclxuICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnVXBsb2FkIEZpbGVzIGNvbmZpcm1hdGlvbicsICcnLCBVcGxvYWRDb25maXJtRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICBmaWxlLFxyXG4gICAgICAgICAgICAgICAgb25GaW5pc2hlZDogKHNob3VsZENvbnRpbnVlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShzaG91bGRDb250aW51ZSA/IE1hdHJpeENsaWVudFBlZy5nZXQoKS51cGxvYWRDb250ZW50KGZpbGUpIDogbnVsbCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBmaWxlU2VsZWN0b3IuY2xpY2soKTtcclxuICAgIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IENvbW1hbmRDYXRlZ29yaWVzID0ge1xyXG4gICAgXCJtZXNzYWdlc1wiOiBfdGQoXCJNZXNzYWdlc1wiKSxcclxuICAgIFwiYWN0aW9uc1wiOiBfdGQoXCJBY3Rpb25zXCIpLFxyXG4gICAgXCJhZG1pblwiOiBfdGQoXCJBZG1pblwiKSxcclxuICAgIFwiYWR2YW5jZWRcIjogX3RkKFwiQWR2YW5jZWRcIiksXHJcbiAgICBcIm90aGVyXCI6IF90ZChcIk90aGVyXCIpLFxyXG59O1xyXG5cclxudHlwZSBSdW5GbiA9ICgocm9vbUlkOiBzdHJpbmcsIGFyZ3M6IHN0cmluZywgY21kOiBzdHJpbmcpID0+IHtlcnJvcjogYW55fSB8IHtwcm9taXNlOiBQcm9taXNlPGFueT59KTtcclxuXHJcbmludGVyZmFjZSBJQ29tbWFuZE9wdHMge1xyXG4gICAgY29tbWFuZDogc3RyaW5nO1xyXG4gICAgYWxpYXNlcz86IHN0cmluZ1tdO1xyXG4gICAgYXJncz86IHN0cmluZztcclxuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XHJcbiAgICBydW5Gbj86IFJ1bkZuO1xyXG4gICAgY2F0ZWdvcnk6IHN0cmluZztcclxuICAgIGhpZGVDb21wbGV0aW9uQWZ0ZXJTcGFjZT86IGJvb2xlYW47XHJcbn1cclxuXHJcbmNsYXNzIENvbW1hbmQge1xyXG4gICAgY29tbWFuZDogc3RyaW5nO1xyXG4gICAgYWxpYXNlczogc3RyaW5nW107XHJcbiAgICBhcmdzOiB1bmRlZmluZWQgfCBzdHJpbmc7XHJcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gICAgcnVuRm46IHVuZGVmaW5lZCB8IFJ1bkZuO1xyXG4gICAgY2F0ZWdvcnk6IHN0cmluZztcclxuICAgIGhpZGVDb21wbGV0aW9uQWZ0ZXJTcGFjZTogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvcHRzOiBJQ29tbWFuZE9wdHMpIHtcclxuICAgICAgICB0aGlzLmNvbW1hbmQgPSBvcHRzLmNvbW1hbmQ7XHJcbiAgICAgICAgdGhpcy5hbGlhc2VzID0gb3B0cy5hbGlhc2VzIHx8IFtdO1xyXG4gICAgICAgIHRoaXMuYXJncyA9IG9wdHMuYXJncyB8fCBcIlwiO1xyXG4gICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBvcHRzLmRlc2NyaXB0aW9uO1xyXG4gICAgICAgIHRoaXMucnVuRm4gPSBvcHRzLnJ1bkZuO1xyXG4gICAgICAgIHRoaXMuY2F0ZWdvcnkgPSBvcHRzLmNhdGVnb3J5IHx8IENvbW1hbmRDYXRlZ29yaWVzLm90aGVyO1xyXG4gICAgICAgIHRoaXMuaGlkZUNvbXBsZXRpb25BZnRlclNwYWNlID0gb3B0cy5oaWRlQ29tcGxldGlvbkFmdGVyU3BhY2UgfHwgZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29tbWFuZCgpIHtcclxuICAgICAgICByZXR1cm4gYC8ke3RoaXMuY29tbWFuZH1gO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbW1hbmRXaXRoQXJncygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRDb21tYW5kKCkgKyBcIiBcIiArIHRoaXMuYXJncztcclxuICAgIH1cclxuXHJcbiAgICBydW4ocm9vbUlkOiBzdHJpbmcsIGFyZ3M6IHN0cmluZywgY21kOiBzdHJpbmcpIHtcclxuICAgICAgICAvLyBpZiBpdCBoYXMgbm8gcnVuRm4gdGhlbiBpdHMgYW4gaWdub3JlZC9ub3AgY29tbWFuZCAoYXV0b2NvbXBsZXRlIG9ubHkpIGUuZyBgL21lYFxyXG4gICAgICAgIGlmICghdGhpcy5ydW5GbikgcmV0dXJuO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJ1bkZuLmJpbmQodGhpcykocm9vbUlkLCBhcmdzLCBjbWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzYWdlKCkge1xyXG4gICAgICAgIHJldHVybiBfdCgnVXNhZ2UnKSArICc6ICcgKyB0aGlzLmdldENvbW1hbmRXaXRoQXJncygpO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiByZWplY3QoZXJyb3IpIHtcclxuICAgIHJldHVybiB7ZXJyb3J9O1xyXG59XHJcblxyXG5mdW5jdGlvbiBzdWNjZXNzKHByb21pc2U/OiBQcm9taXNlPGFueT4pIHtcclxuICAgIHJldHVybiB7cHJvbWlzZX07XHJcbn1cclxuXHJcbi8qIERpc2FibGUgdGhlIFwidW5leHBlY3RlZCB0aGlzXCIgZXJyb3IgZm9yIHRoZXNlIGNvbW1hbmRzIC0gYWxsIG9mIHRoZSBydW5cclxuICogZnVuY3Rpb25zIGFyZSBjYWxsZWQgd2l0aCBgdGhpc2AgYm91bmQgdG8gdGhlIENvbW1hbmQgaW5zdGFuY2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNvbnN0IENvbW1hbmRzID0gW1xyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdzaHJ1ZycsXHJcbiAgICAgICAgYXJnczogJzxtZXNzYWdlPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnUHJlcGVuZHMgwq9cXFxcXyjjg4QpXy/CryB0byBhIHBsYWluLXRleHQgbWVzc2FnZScpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgbGV0IG1lc3NhZ2UgPSAnwq9cXFxcXyjjg4QpXy/Cryc7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gbWVzc2FnZSArICcgJyArIGFyZ3M7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRUZXh0TWVzc2FnZShyb29tSWQsIG1lc3NhZ2UpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5tZXNzYWdlcyxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdwbGFpbicsXHJcbiAgICAgICAgYXJnczogJzxtZXNzYWdlPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnU2VuZHMgYSBtZXNzYWdlIGFzIHBsYWluIHRleHQsIHdpdGhvdXQgaW50ZXJwcmV0aW5nIGl0IGFzIG1hcmtkb3duJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgbWVzc2FnZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRUZXh0TWVzc2FnZShyb29tSWQsIG1lc3NhZ2VzKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMubWVzc2FnZXMsXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAnaHRtbCcsXHJcbiAgICAgICAgYXJnczogJzxtZXNzYWdlPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnU2VuZHMgYSBtZXNzYWdlIGFzIGh0bWwsIHdpdGhvdXQgaW50ZXJwcmV0aW5nIGl0IGFzIG1hcmtkb3duJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgbWVzc2FnZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRIdG1sTWVzc2FnZShyb29tSWQsIG1lc3NhZ2VzLCBtZXNzYWdlcykpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLm1lc3NhZ2VzLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2RkZycsXHJcbiAgICAgICAgYXJnczogJzxxdWVyeT4nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ1NlYXJjaGVzIER1Y2tEdWNrR28gZm9yIHJlc3VsdHMnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5FcnJvckRpYWxvZycpO1xyXG4gICAgICAgICAgICAvLyBUT0RPIERvbid0IGV4cGxhaW4gdGhpcyBhd2F5LCBhY3R1YWxseSBzaG93IGEgc2VhcmNoIFVJIGhlcmUuXHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NsYXNoIENvbW1hbmRzJywgJy9kZGcgaXMgbm90IGEgY29tbWFuZCcsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogX3QoJy9kZGcgaXMgbm90IGEgY29tbWFuZCcpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KCdUbyB1c2UgaXQsIGp1c3Qgd2FpdCBmb3IgYXV0b2NvbXBsZXRlIHJlc3VsdHMgdG8gbG9hZCBhbmQgdGFiIHRocm91Z2ggdGhlbS4nKSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWN0aW9ucyxcclxuICAgICAgICBoaWRlQ29tcGxldGlvbkFmdGVyU3BhY2U6IHRydWUsXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAndXBncmFkZXJvb20nLFxyXG4gICAgICAgIGFyZ3M6ICc8bmV3X3ZlcnNpb24+JyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdVcGdyYWRlcyBhIHJvb20gdG8gYSBuZXcgdmVyc2lvbicpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFyb29tLmN1cnJlbnRTdGF0ZS5tYXlDbGllbnRTZW5kU3RhdGVFdmVudChcIm0ucm9vbS50b21ic3RvbmVcIiwgY2xpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZWplY3QoX3QoXCJZb3UgZG8gbm90IGhhdmUgdGhlIHJlcXVpcmVkIHBlcm1pc3Npb25zIHRvIHVzZSB0aGlzIGNvbW1hbmQuXCIpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBSb29tVXBncmFkZVdhcm5pbmdEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5Sb29tVXBncmFkZVdhcm5pbmdEaWFsb2dcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3Qge2ZpbmlzaGVkfSA9IE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NsYXNoIENvbW1hbmRzJywgJ3VwZ3JhZGUgcm9vbSBjb25maXJtYXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgIFJvb21VcGdyYWRlV2FybmluZ0RpYWxvZywge3Jvb21JZDogcm9vbUlkLCB0YXJnZXRWZXJzaW9uOiBhcmdzfSwgLypjbGFzc05hbWU9Ki9udWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIC8qaXNQcmlvcml0eT0qL2ZhbHNlLCAvKmlzU3RhdGljPSovdHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoZmluaXNoZWQudGhlbihhc3luYyAoW3Jlc3BdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFyZXNwLmNvbnRpbnVlKSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBjaGVja0ZvclVwZ3JhZGVGbjtcclxuICAgICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1cGdyYWRlUHJvbWlzZSA9IGNsaS51cGdyYWRlUm9vbShyb29tSWQsIGFyZ3MpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2UgaGF2ZSB0byB3YWl0IGZvciB0aGUganMtc2RrIHRvIGdpdmUgdXMgdGhlIHJvb20gYmFjayBzb1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB3ZSBjYW4gbW9yZSBlZmZlY3RpdmVseSBhYnVzZSB0aGUgTXVsdGlJbnZpdGVyIGJlaGF2aW91clxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB3aGljaCBoZWF2aWx5IHJlbGllcyBvbiB0aGUgUm9vbSBvYmplY3QgYmVpbmcgYXZhaWxhYmxlLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcC5pbnZpdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrRm9yVXBncmFkZUZuID0gYXN5bmMgKG5ld1Jvb20pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGUgdXBncmFkZVByb21pc2Ugc2hvdWxkIGJlIGRvbmUgYnkgdGhlIHRpbWUgd2UgYXdhaXQgaXQgaGVyZS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB7cmVwbGFjZW1lbnRfcm9vbTogbmV3Um9vbUlkfSA9IGF3YWl0IHVwZ3JhZGVQcm9taXNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChuZXdSb29tLnJvb21JZCAhPT0gbmV3Um9vbUlkKSByZXR1cm47XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRvSW52aXRlID0gW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi5yb29tLmdldE1lbWJlcnNXaXRoTWVtYmVyc2hpcChcImpvaW5cIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLnJvb20uZ2V0TWVtYmVyc1dpdGhNZW1iZXJzaGlwKFwiaW52aXRlXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0ubWFwKG0gPT4gbS51c2VySWQpLmZpbHRlcihtID0+IG0gIT09IGNsaS5nZXRVc2VySWQoKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0b0ludml0ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEVycm9ycyBhcmUgaGFuZGxlZCBpbnRlcm5hbGx5IHRvIHRoaXMgZnVuY3Rpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXdhaXQgaW52aXRlVXNlcnNUb1Jvb20obmV3Um9vbUlkLCB0b0ludml0ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGkucmVtb3ZlTGlzdGVuZXIoJ1Jvb20nLCBjaGVja0ZvclVwZ3JhZGVGbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpLm9uKCdSb29tJywgY2hlY2tGb3JVcGdyYWRlRm4pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBXZSBoYXZlIHRvIGF3YWl0IGFmdGVyIHNvIHRoYXQgdGhlIGNoZWNrRm9yVXBncmFkZXNGbiBoYXMgYSBwcm9wZXIgcmVmZXJlbmNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRvIHRoZSBuZXcgcm9vbSdzIElELlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhd2FpdCB1cGdyYWRlUHJvbWlzZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2hlY2tGb3JVcGdyYWRlRm4pIGNsaS5yZW1vdmVMaXN0ZW5lcignUm9vbScsIGNoZWNrRm9yVXBncmFkZUZuKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IEVycm9yRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5FcnJvckRpYWxvZycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdTbGFzaCBDb21tYW5kcycsICdyb29tIHVwZ3JhZGUgZXJyb3InLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdFcnJvciB1cGdyYWRpbmcgcm9vbScpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdEb3VibGUgY2hlY2sgdGhhdCB5b3VyIHNlcnZlciBzdXBwb3J0cyB0aGUgcm9vbSB2ZXJzaW9uIGNob3NlbiBhbmQgdHJ5IGFnYWluLicpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkbWluLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ25pY2snLFxyXG4gICAgICAgIGFyZ3M6ICc8ZGlzcGxheV9uYW1lPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnQ2hhbmdlcyB5b3VyIGRpc3BsYXkgbmlja25hbWUnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcyhNYXRyaXhDbGllbnRQZWcuZ2V0KCkuc2V0RGlzcGxheU5hbWUoYXJncykpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hY3Rpb25zLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ215cm9vbW5pY2snLFxyXG4gICAgICAgIGFsaWFzZXM6IFsncm9vbW5pY2snXSxcclxuICAgICAgICBhcmdzOiAnPGRpc3BsYXlfbmFtZT4nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0NoYW5nZXMgeW91ciBkaXNwbGF5IG5pY2tuYW1lIGluIHRoZSBjdXJyZW50IHJvb20gb25seScpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ID0gY2xpLmdldFJvb20ocm9vbUlkKS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5tZW1iZXInLCBjbGkuZ2V0VXNlcklkKCkpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY29udGVudCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAuLi5ldiA/IGV2LmdldENvbnRlbnQoKSA6IHsgbWVtYmVyc2hpcDogJ2pvaW4nIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheW5hbWU6IGFyZ3MsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoY2xpLnNlbmRTdGF0ZUV2ZW50KHJvb21JZCwgJ20ucm9vbS5tZW1iZXInLCBjb250ZW50LCBjbGkuZ2V0VXNlcklkKCkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWN0aW9ucyxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdyb29tYXZhdGFyJyxcclxuICAgICAgICBhcmdzOiAnWzxteGNfdXJsPl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0NoYW5nZXMgdGhlIGF2YXRhciBvZiB0aGUgY3VycmVudCByb29tJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBsZXQgcHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZShhcmdzKTtcclxuICAgICAgICAgICAgaWYgKCFhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBwcm9taXNlID0gc2luZ2xlTXhjVXBsb2FkKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKHByb21pc2UudGhlbigodXJsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXVybCkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZW5kU3RhdGVFdmVudChyb29tSWQsICdtLnJvb20uYXZhdGFyJywge3VybH0sICcnKTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFjdGlvbnMsXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAnbXlyb29tYXZhdGFyJyxcclxuICAgICAgICBhcmdzOiAnWzxteGNfdXJsPl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0NoYW5nZXMgeW91ciBhdmF0YXIgaW4gdGhpcyBjdXJyZW50IHJvb20gb25seScpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICAgICAgY29uc3QgdXNlcklkID0gY2xpLmdldFVzZXJJZCgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHByb21pc2UgPSBQcm9taXNlLnJlc29sdmUoYXJncyk7XHJcbiAgICAgICAgICAgIGlmICghYXJncykge1xyXG4gICAgICAgICAgICAgICAgcHJvbWlzZSA9IHNpbmdsZU14Y1VwbG9hZCgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gc3VjY2Vzcyhwcm9taXNlLnRoZW4oKHVybCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF1cmwpIHJldHVybjtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ID0gcm9vbS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5tZW1iZXInLCB1c2VySWQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY29udGVudCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAuLi5ldiA/IGV2LmdldENvbnRlbnQoKSA6IHsgbWVtYmVyc2hpcDogJ2pvaW4nIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgYXZhdGFyX3VybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjbGkuc2VuZFN0YXRlRXZlbnQocm9vbUlkLCAnbS5yb29tLm1lbWJlcicsIGNvbnRlbnQsIHVzZXJJZCk7XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hY3Rpb25zLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ215YXZhdGFyJyxcclxuICAgICAgICBhcmdzOiAnWzxteGNfdXJsPl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0NoYW5nZXMgeW91ciBhdmF0YXIgaW4gYWxsIHJvb21zJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBsZXQgcHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZShhcmdzKTtcclxuICAgICAgICAgICAgaWYgKCFhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBwcm9taXNlID0gc2luZ2xlTXhjVXBsb2FkKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKHByb21pc2UudGhlbigodXJsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXVybCkgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZXRBdmF0YXJVcmwodXJsKTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFjdGlvbnMsXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAndG9waWMnLFxyXG4gICAgICAgIGFyZ3M6ICdbPHRvcGljPl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0dldHMgb3Igc2V0cyB0aGUgcm9vbSB0b3BpYycpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICBpZiAoYXJncykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoY2xpLnNldFJvb21Ub3BpYyhyb29tSWQsIGFyZ3MpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCByb29tID0gY2xpLmdldFJvb20ocm9vbUlkKTtcclxuICAgICAgICAgICAgaWYgKCFyb29tKSByZXR1cm4gcmVqZWN0KCdCYWQgcm9vbSBJRDogJyArIHJvb21JZCk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB0b3BpY0V2ZW50cyA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKCdtLnJvb20udG9waWMnLCAnJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvcGljID0gdG9waWNFdmVudHMgJiYgdG9waWNFdmVudHMuZ2V0Q29udGVudCgpLnRvcGljO1xyXG4gICAgICAgICAgICBjb25zdCB0b3BpY0h0bWwgPSB0b3BpYyA/IGxpbmtpZnlBbmRTYW5pdGl6ZUh0bWwodG9waWMpIDogX3QoJ1RoaXMgcm9vbSBoYXMgbm8gdG9waWMuJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBJbmZvRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5JbmZvRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NsYXNoIENvbW1hbmRzJywgJ1RvcGljJywgSW5mb0RpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6IHJvb20ubmFtZSxcclxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogdG9waWNIdG1sIH19IC8+LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZG1pbixcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdyb29tbmFtZScsXHJcbiAgICAgICAgYXJnczogJzxuYW1lPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnU2V0cyB0aGUgcm9vbSBuYW1lJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBpZiAoYXJncykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLnNldFJvb21OYW1lKHJvb21JZCwgYXJncykpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZG1pbixcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdpbnZpdGUnLFxyXG4gICAgICAgIGFyZ3M6ICc8dXNlci1pZD4nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0ludml0ZXMgdXNlciB3aXRoIGdpdmVuIGlkIHRvIGN1cnJlbnQgcm9vbScpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXMgPSBhcmdzLm1hdGNoKC9eKFxcUyspJC8pO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1hdGNoZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSB1c2UgYSBNdWx0aUludml0ZXIgdG8gcmUtdXNlIHRoZSBpbnZpdGUgbG9naWMsIGV2ZW4gdGhvdWdoXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UncmUgb25seSBpbnZpdGluZyBvbmUgdXNlci5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhZGRyZXNzID0gbWF0Y2hlc1sxXTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiB3ZSBuZWVkIGFuIGlkZW50aXR5IHNlcnZlciBidXQgZG9uJ3QgaGF2ZSBvbmUsIHRoaW5nc1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGdldCBhIGJpdCBtb3JlIGNvbXBsZXggaGVyZSwgYnV0IHdlIHRyeSB0byBzaG93IHNvbWV0aGluZ1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG1lYW5pbmdmdWwuXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZpbmlzaGVkID0gUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBnZXRBZGRyZXNzVHlwZShhZGRyZXNzKSA9PT0gJ2VtYWlsJyAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAhTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldElkZW50aXR5U2VydmVyVXJsKClcclxuICAgICAgICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVmYXVsdElkZW50aXR5U2VydmVyVXJsID0gZ2V0RGVmYXVsdElkZW50aXR5U2VydmVyVXJsKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJVcmwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh7IGZpbmlzaGVkIH0gPSBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdTbGFzaCBDb21tYW5kcycsICdJZGVudGl0eSBzZXJ2ZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFF1ZXN0aW9uRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdChcIlVzZSBhbiBpZGVudGl0eSBzZXJ2ZXJcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8cD57X3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlVzZSBhbiBpZGVudGl0eSBzZXJ2ZXIgdG8gaW52aXRlIGJ5IGVtYWlsLiBcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkNsaWNrIGNvbnRpbnVlIHRvIHVzZSB0aGUgZGVmYXVsdCBpZGVudGl0eSBzZXJ2ZXIgXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIoJShkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lKXMpIG9yIG1hbmFnZSBpbiBTZXR0aW5ncy5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0SWRlbnRpdHlTZXJ2ZXJOYW1lOiBhYmJyZXZpYXRlVXJsKGRlZmF1bHRJZGVudGl0eVNlcnZlclVybCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfTwvcD4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJ1dHRvbjogX3QoXCJDb250aW51ZVwiKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmluaXNoZWQgPSBmaW5pc2hlZC50aGVuKChbdXNlRGVmYXVsdF06IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh1c2VEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZURlZmF1bHRJZGVudGl0eVNlcnZlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihfdChcIlVzZSBhbiBpZGVudGl0eSBzZXJ2ZXIgdG8gaW52aXRlIGJ5IGVtYWlsLiBNYW5hZ2UgaW4gU2V0dGluZ3MuXCIpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdChfdChcIlVzZSBhbiBpZGVudGl0eSBzZXJ2ZXIgdG8gaW52aXRlIGJ5IGVtYWlsLiBNYW5hZ2UgaW4gU2V0dGluZ3MuXCIpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbnZpdGVyID0gbmV3IE11bHRpSW52aXRlcihyb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKGZpbmlzaGVkLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gaW52aXRlci5pbnZpdGUoW2FkZHJlc3NdKTtcclxuICAgICAgICAgICAgICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGludml0ZXIuZ2V0Q29tcGxldGlvblN0YXRlKGFkZHJlc3MpICE9PSBcImludml0ZWRcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGludml0ZXIuZ2V0RXJyb3JUZXh0KGFkZHJlc3MpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWN0aW9ucyxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdqb2luJyxcclxuICAgICAgICBhbGlhc2VzOiBbJ2onLCAnZ290byddLFxyXG4gICAgICAgIGFyZ3M6ICc8cm9vbS1hbGlhcz4nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0pvaW5zIHJvb20gd2l0aCBnaXZlbiBhbGlhcycpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihfLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBOb3RlOiB3ZSBzdXBwb3J0IDIgdmVyc2lvbnMgb2YgdGhpcyBjb21tYW5kLiBUaGUgZmlyc3QgaXNcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBwdWJsaWMtZmFjaW5nIG9uZSBmb3IgbW9zdCB1c2VycyBhbmQgdGhlIG90aGVyIGlzIGFcclxuICAgICAgICAgICAgICAgIC8vIHBvd2VyLXVzZXIgZWRpdGlvbiB3aGVyZSBzb21lb25lIG1heSBqb2luIHZpYSBwZXJtYWxpbmsgb3JcclxuICAgICAgICAgICAgICAgIC8vIHJvb20gSUQgd2l0aCBvcHRpb25hbCBzZXJ2ZXJzLiBQcmFjdGljYWxseSwgdGhpcyByZXN1bHRzXHJcbiAgICAgICAgICAgICAgICAvLyBpbiB0aGUgZm9sbG93aW5nIHZhcmlhdGlvbnM6XHJcbiAgICAgICAgICAgICAgICAvLyAgIC9qb2luICNleGFtcGxlOmV4YW1wbGUub3JnXHJcbiAgICAgICAgICAgICAgICAvLyAgIC9qb2luICFleGFtcGxlOmV4YW1wbGUub3JnXHJcbiAgICAgICAgICAgICAgICAvLyAgIC9qb2luICFleGFtcGxlOmV4YW1wbGUub3JnIGFsdHNlcnZlci5jb20gZWxzZXdoZXJlLmNhXHJcbiAgICAgICAgICAgICAgICAvLyAgIC9qb2luIGh0dHBzOi8vbWF0cml4LnRvLyMvIWV4YW1wbGU6ZXhhbXBsZS5vcmc/dmlhPWFsdHNlcnZlci5jb21cclxuICAgICAgICAgICAgICAgIC8vIFRoZSBjb21tYW5kIGFsc28gc3VwcG9ydHMgZXZlbnQgcGVybWFsaW5rcyB0cmFuc3BhcmVudGx5OlxyXG4gICAgICAgICAgICAgICAgLy8gICAvam9pbiBodHRwczovL21hdHJpeC50by8jLyFleGFtcGxlOmV4YW1wbGUub3JnLyRzb21ldGhpbmc6ZXhhbXBsZS5vcmdcclxuICAgICAgICAgICAgICAgIC8vICAgL2pvaW4gaHR0cHM6Ly9tYXRyaXgudG8vIy8hZXhhbXBsZTpleGFtcGxlLm9yZy8kc29tZXRoaW5nOmV4YW1wbGUub3JnP3ZpYT1hbHRzZXJ2ZXIuY29tXHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbXMgPSBhcmdzLnNwbGl0KCcgJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyYW1zLmxlbmd0aCA8IDEpIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgaXNQZXJtYWxpbmsgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbXNbMF0uc3RhcnRzV2l0aChcImh0dHA6XCIpIHx8IHBhcmFtc1swXS5zdGFydHNXaXRoKFwiaHR0cHM6XCIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSXQncyBhdCBsZWFzdCBhIFVSTCAtIHRyeSBhbmQgcHVsbCBvdXQgYSBob3N0bmFtZSB0byBjaGVjayBhZ2FpbnN0IHRoZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHBlcm1hbGluayBoYW5kbGVyXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyc2VkVXJsID0gbmV3IFVSTChwYXJhbXNbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGhvc3RuYW1lID0gcGFyc2VkVXJsLmhvc3QgfHwgcGFyc2VkVXJsLmhvc3RuYW1lOyAvLyB0YWtlcyBmaXJzdCBub24tZmFsc2V5IHZhbHVlXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHdlJ3JlIHVzaW5nIGEgUmlvdCBwZXJtYWxpbmsgaGFuZGxlciwgdGhpcyB3aWxsIGNhdGNoIGl0IGJlZm9yZSB3ZSBnZXQgbXVjaCBmdXJ0aGVyLlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlZSBiZWxvdyB3aGVyZSB3ZSBtYWtlIGFzc3VtcHRpb25zIGFib3V0IHBhcnNpbmcgdGhlIFVSTC5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoaXNQZXJtYWxpbmtIb3N0KGhvc3RuYW1lKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1Blcm1hbGluayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHBhcmFtc1swXVswXSA9PT0gJyMnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJvb21BbGlhcyA9IHBhcmFtc1swXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXJvb21BbGlhcy5pbmNsdWRlcygnOicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvb21BbGlhcyArPSAnOicgKyBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0RG9tYWluKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBkaXMuZGlzcGF0Y2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb29tX2FsaWFzOiByb29tQWxpYXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9fam9pbjogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcygpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwYXJhbXNbMF1bMF0gPT09ICchJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvb21JZCA9IHBhcmFtc1swXTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB2aWFTZXJ2ZXJzID0gcGFyYW1zLnNwbGljZSgwKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAndmlld19yb29tJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbV9pZDogcm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGVzZSBhcmUgcGFzc2VkIGRvd24gdG8gdGhlIGpzLXNkaydzIC9qb2luIGNhbGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpYVNlcnZlcnM6IHZpYVNlcnZlcnMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpYV9zZXJ2ZXJzOiB2aWFTZXJ2ZXJzLCAvLyBmb3IgdGhlIHJlam9pbiBidXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b19qb2luOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGlzUGVybWFsaW5rKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcGVybWFsaW5rUGFydHMgPSBwYXJzZVBlcm1hbGluayhwYXJhbXNbMF0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBUaGlzIGNoZWNrIHRlY2huaWNhbGx5IGlzbid0IG5lZWRlZCBiZWNhdXNlIHdlIGFscmVhZHkgZGlkIG91clxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNhZmV0eSBjaGVja3MgdXAgYWJvdmUuIEhvd2V2ZXIsIGZvciBnb29kIG1lYXN1cmUsIGxldCdzIGJlIHN1cmUuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFwZXJtYWxpbmtQYXJ0cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiBmb3Igc29tZSByZWFzb24gc29tZW9uZSB3YW50ZWQgdG8gam9pbiBhIGdyb3VwIG9yIHVzZXIsIHdlIHNob3VsZFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHN0b3AgdGhlbSBub3cuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFwZXJtYWxpbmtQYXJ0cy5yb29tSWRPckFsaWFzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVudGl0eSA9IHBlcm1hbGlua1BhcnRzLnJvb21JZE9yQWxpYXM7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdmlhU2VydmVycyA9IHBlcm1hbGlua1BhcnRzLnZpYVNlcnZlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnRJZCA9IHBlcm1hbGlua1BhcnRzLmV2ZW50SWQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRpc3BhdGNoID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICd2aWV3X3Jvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvX2pvaW46IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVudGl0eVswXSA9PT0gJyEnKSBkaXNwYXRjaFtcInJvb21faWRcIl0gPSBlbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxzZSBkaXNwYXRjaFtcInJvb21fYWxpYXNcIl0gPSBlbnRpdHk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudElkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BhdGNoW1wiZXZlbnRfaWRcIl0gPSBldmVudElkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwYXRjaFtcImhpZ2hsaWdodGVkXCJdID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh2aWFTZXJ2ZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEZvciB0aGUgam9pblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwYXRjaFtcIm9wdHNcIl0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGVzZSBhcmUgcGFzc2VkIGRvd24gdG8gdGhlIGpzLXNkaydzIC9qb2luIGNhbGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpYVNlcnZlcnM6IHZpYVNlcnZlcnMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBGb3IgaWYgdGhlIGpvaW4gZmFpbHMgKHJlam9pbiBidXR0b24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BhdGNoWyd2aWFfc2VydmVycyddID0gdmlhU2VydmVycztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaChkaXNwYXRjaCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWN0aW9ucyxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdwYXJ0JyxcclxuICAgICAgICBhcmdzOiAnWzxyb29tLWFsaWFzPl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0xlYXZlIHJvb20nKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNsaSA9IE1hdHJpeENsaWVudFBlZy5nZXQoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB0YXJnZXRSb29tSWQ7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXRjaGVzID0gYXJncy5tYXRjaCgvXihcXFMrKSQvKTtcclxuICAgICAgICAgICAgICAgIGlmIChtYXRjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJvb21BbGlhcyA9IG1hdGNoZXNbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJvb21BbGlhc1swXSAhPT0gJyMnKSByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcm9vbUFsaWFzLmluY2x1ZGVzKCc6JykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9vbUFsaWFzICs9ICc6JyArIGNsaS5nZXREb21haW4oKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFRyeSB0byBmaW5kIGEgcm9vbSB3aXRoIHRoaXMgYWxpYXNcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByb29tcyA9IGNsaS5nZXRSb29tcygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcm9vbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYWxpYXNFdmVudHMgPSByb29tc1tpXS5jdXJyZW50U3RhdGUuZ2V0U3RhdGVFdmVudHMoJ20ucm9vbS5hbGlhc2VzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgYWxpYXNFdmVudHMubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFsaWFzZXMgPSBhbGlhc0V2ZW50c1tqXS5nZXRDb250ZW50KCkuYWxpYXNlcyB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGsgPSAwOyBrIDwgYWxpYXNlcy5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbGlhc2VzW2tdID09PSByb29tQWxpYXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0Um9vbUlkID0gcm9vbXNbaV0ucm9vbUlkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0Um9vbUlkKSBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0Um9vbUlkKSBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0YXJnZXRSb29tSWQpIHJldHVybiByZWplY3QoX3QoJ1VucmVjb2duaXNlZCByb29tIGFsaWFzOicpICsgJyAnICsgcm9vbUFsaWFzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCF0YXJnZXRSb29tSWQpIHRhcmdldFJvb21JZCA9IHJvb21JZDtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoXHJcbiAgICAgICAgICAgICAgICBjbGkubGVhdmVSb29tQ2hhaW4odGFyZ2V0Um9vbUlkKS50aGVuKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpcy5kaXNwYXRjaCh7YWN0aW9uOiAndmlld19uZXh0X3Jvb20nfSk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hY3Rpb25zLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2tpY2snLFxyXG4gICAgICAgIGFyZ3M6ICc8dXNlci1pZD4gW3JlYXNvbl0nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ0tpY2tzIHVzZXIgd2l0aCBnaXZlbiBpZCcpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXMgPSBhcmdzLm1hdGNoKC9eKFxcUys/KSggKyguKikpPyQvKTtcclxuICAgICAgICAgICAgICAgIGlmIChtYXRjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLmtpY2socm9vbUlkLCBtYXRjaGVzWzFdLCBtYXRjaGVzWzNdKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkbWluLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2JhbicsXHJcbiAgICAgICAgYXJnczogJzx1c2VyLWlkPiBbcmVhc29uXScsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnQmFucyB1c2VyIHdpdGggZ2l2ZW4gaWQnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXRjaGVzID0gYXJncy5tYXRjaCgvXihcXFMrPykoICsoLiopKT8kLyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2hlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKE1hdHJpeENsaWVudFBlZy5nZXQoKS5iYW4ocm9vbUlkLCBtYXRjaGVzWzFdLCBtYXRjaGVzWzNdKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkbWluLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ3VuYmFuJyxcclxuICAgICAgICBhcmdzOiAnPHVzZXItaWQ+JyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdVbmJhbnMgdXNlciB3aXRoIGdpdmVuIElEJyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBpZiAoYXJncykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWF0Y2hlcyA9IGFyZ3MubWF0Y2goL14oXFxTKykkLyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2hlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFJlc2V0IHRoZSB1c2VyIG1lbWJlcnNoaXAgdG8gXCJsZWF2ZVwiIHRvIHVuYmFuIGhpbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKE1hdHJpeENsaWVudFBlZy5nZXQoKS51bmJhbihyb29tSWQsIG1hdGNoZXNbMV0pKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWRtaW4sXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAnaWdub3JlJyxcclxuICAgICAgICBhcmdzOiAnPHVzZXItaWQ+JyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdJZ25vcmVzIGEgdXNlciwgaGlkaW5nIHRoZWlyIG1lc3NhZ2VzIGZyb20geW91JyksXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBpZiAoYXJncykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXMgPSBhcmdzLm1hdGNoKC9eKFxcUyspJC8pO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1hdGNoZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSBtYXRjaGVzWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGlnbm9yZWRVc2VycyA9IGNsaS5nZXRJZ25vcmVkVXNlcnMoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZ25vcmVkVXNlcnMucHVzaCh1c2VySWQpOyAvLyBkZS1kdXBlZCBpbnRlcm5hbGx5IGluIHRoZSBqcy1zZGtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpLnNldElnbm9yZWRVc2VycyhpZ25vcmVkVXNlcnMpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgSW5mb0RpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuSW5mb0RpYWxvZycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU2xhc2ggQ29tbWFuZHMnLCAnVXNlciBpZ25vcmVkJywgSW5mb0RpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnSWdub3JlZCB1c2VyJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnsgX3QoJ1lvdSBhcmUgbm93IGlnbm9yaW5nICUodXNlcklkKXMnLCB7dXNlcklkfSkgfTwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVqZWN0KHRoaXMuZ2V0VXNhZ2UoKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWN0aW9ucyxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICd1bmlnbm9yZScsXHJcbiAgICAgICAgYXJnczogJzx1c2VyLWlkPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnU3RvcHMgaWdub3JpbmcgYSB1c2VyLCBzaG93aW5nIHRoZWlyIG1lc3NhZ2VzIGdvaW5nIGZvcndhcmQnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbWF0Y2hlcyA9IGFyZ3MubWF0Y2goL14oXFxTKykkLyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2hlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IG1hdGNoZXNbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaWdub3JlZFVzZXJzID0gY2xpLmdldElnbm9yZWRVc2VycygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gaWdub3JlZFVzZXJzLmluZGV4T2YodXNlcklkKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSBpZ25vcmVkVXNlcnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpLnNldElnbm9yZWRVc2VycyhpZ25vcmVkVXNlcnMpLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgSW5mb0RpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuSW5mb0RpYWxvZycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnU2xhc2ggQ29tbWFuZHMnLCAnVXNlciB1bmlnbm9yZWQnLCBJbmZvRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdVbmlnbm9yZWQgdXNlcicpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD57IF90KCdZb3UgYXJlIG5vIGxvbmdlciBpZ25vcmluZyAlKHVzZXJJZClzJywge3VzZXJJZH0pIH08L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFjdGlvbnMsXHJcbiAgICB9KSxcclxuICAgIG5ldyBDb21tYW5kKHtcclxuICAgICAgICBjb21tYW5kOiAnb3AnLFxyXG4gICAgICAgIGFyZ3M6ICc8dXNlci1pZD4gWzxwb3dlci1sZXZlbD5dJyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdEZWZpbmUgdGhlIHBvd2VyIGxldmVsIG9mIGEgdXNlcicpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXMgPSBhcmdzLm1hdGNoKC9eKFxcUys/KSggKygtP1xcZCspKT8kLyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG93ZXJMZXZlbCA9IDUwOyAvLyBkZWZhdWx0IHBvd2VyIGxldmVsIGZvciBvcFxyXG4gICAgICAgICAgICAgICAgaWYgKG1hdGNoZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSBtYXRjaGVzWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChtYXRjaGVzLmxlbmd0aCA9PT0gNCAmJiB1bmRlZmluZWQgIT09IG1hdGNoZXNbM10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG93ZXJMZXZlbCA9IHBhcnNlSW50KG1hdGNoZXNbM10sIDEwKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc05hTihwb3dlckxldmVsKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjbGkgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXJvb20pIHJldHVybiByZWplY3QoJ0JhZCByb29tIElEOiAnICsgcm9vbUlkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBvd2VyTGV2ZWxFdmVudCA9IHJvb20uY3VycmVudFN0YXRlLmdldFN0YXRlRXZlbnRzKCdtLnJvb20ucG93ZXJfbGV2ZWxzJywgJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcyhjbGkuc2V0UG93ZXJMZXZlbChyb29tSWQsIHVzZXJJZCwgcG93ZXJMZXZlbCwgcG93ZXJMZXZlbEV2ZW50KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZG1pbixcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdkZW9wJyxcclxuICAgICAgICBhcmdzOiAnPHVzZXItaWQ+JyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdEZW9wcyB1c2VyIHdpdGggZ2l2ZW4gaWQnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmIChhcmdzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXRjaGVzID0gYXJncy5tYXRjaCgvXihcXFMrKSQvKTtcclxuICAgICAgICAgICAgICAgIGlmIChtYXRjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvb20gPSBjbGkuZ2V0Um9vbShyb29tSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcm9vbSkgcmV0dXJuIHJlamVjdCgnQmFkIHJvb20gSUQ6ICcgKyByb29tSWQpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwb3dlckxldmVsRXZlbnQgPSByb29tLmN1cnJlbnRTdGF0ZS5nZXRTdGF0ZUV2ZW50cygnbS5yb29tLnBvd2VyX2xldmVscycsICcnKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VjY2VzcyhjbGkuc2V0UG93ZXJMZXZlbChyb29tSWQsIGFyZ3MsIHVuZGVmaW5lZCwgcG93ZXJMZXZlbEV2ZW50KSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkbWluLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2RldnRvb2xzJyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdPcGVucyB0aGUgRGV2ZWxvcGVyIFRvb2xzIGRpYWxvZycpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQpIHtcclxuICAgICAgICAgICAgY29uc3QgRGV2dG9vbHNEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLkRldnRvb2xzRGlhbG9nJyk7XHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZURpYWxvZyhEZXZ0b29sc0RpYWxvZywge3Jvb21JZH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gc3VjY2VzcygpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkdmFuY2VkLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2FkZHdpZGdldCcsXHJcbiAgICAgICAgYXJnczogJzx1cmw+JyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdBZGRzIGEgY3VzdG9tIHdpZGdldCBieSBVUkwgdG8gdGhlIHJvb20nKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmICghYXJncyB8fCAoIWFyZ3Muc3RhcnRzV2l0aChcImh0dHBzOi8vXCIpICYmICFhcmdzLnN0YXJ0c1dpdGgoXCJodHRwOi8vXCIpKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdChfdChcIlBsZWFzZSBzdXBwbHkgYSBodHRwczovLyBvciBodHRwOi8vIHdpZGdldCBVUkxcIikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChXaWRnZXRVdGlscy5jYW5Vc2VyTW9kaWZ5V2lkZ2V0cyhyb29tSWQpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZ2V0VXNlcklkKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBub3dNcyA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB3aWRnZXRJZCA9IGVuY29kZVVSSUNvbXBvbmVudChgJHtyb29tSWR9XyR7dXNlcklkfV8ke25vd01zfWApO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoV2lkZ2V0VXRpbHMuc2V0Um9vbVdpZGdldChcclxuICAgICAgICAgICAgICAgICAgICByb29tSWQsIHdpZGdldElkLCBcIm0uY3VzdG9tXCIsIGFyZ3MsIFwiQ3VzdG9tIFdpZGdldFwiLCB7fSkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdChfdChcIllvdSBjYW5ub3QgbW9kaWZ5IHdpZGdldHMgaW4gdGhpcyByb29tLlwiKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZG1pbixcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICd2ZXJpZnknLFxyXG4gICAgICAgIGFyZ3M6ICc8dXNlci1pZD4gPGRldmljZS1pZD4gPGRldmljZS1zaWduaW5nLWtleT4nLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoJ1ZlcmlmaWVzIGEgdXNlciwgc2Vzc2lvbiwgYW5kIHB1YmtleSB0dXBsZScpLFxyXG4gICAgICAgIHJ1bkZuOiBmdW5jdGlvbihyb29tSWQsIGFyZ3MpIHtcclxuICAgICAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hdGNoZXMgPSBhcmdzLm1hdGNoKC9eKFxcUyspICsoXFxTKykgKyhcXFMrKSQvKTtcclxuICAgICAgICAgICAgICAgIGlmIChtYXRjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2xpID0gTWF0cml4Q2xpZW50UGVnLmdldCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSBtYXRjaGVzWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRldmljZUlkID0gbWF0Y2hlc1syXTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaW5nZXJwcmludCA9IG1hdGNoZXNbM107XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKChhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRldmljZSA9IGF3YWl0IGNsaS5nZXRTdG9yZWREZXZpY2UodXNlcklkLCBkZXZpY2VJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZGV2aWNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoX3QoJ1Vua25vd24gKHVzZXIsIHNlc3Npb24pIHBhaXI6JykgKyBgICgke3VzZXJJZH0sICR7ZGV2aWNlSWR9KWApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRldmljZVRydXN0ID0gYXdhaXQgY2xpLmNoZWNrRGV2aWNlVHJ1c3QodXNlcklkLCBkZXZpY2VJZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGV2aWNlVHJ1c3QuaXNWZXJpZmllZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGV2aWNlLmdldEZpbmdlcnByaW50KCkgPT09IGZpbmdlcnByaW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKF90KCdTZXNzaW9uIGFscmVhZHkgdmVyaWZpZWQhJykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoX3QoJ1dBUk5JTkc6IFNlc3Npb24gYWxyZWFkeSB2ZXJpZmllZCwgYnV0IGtleXMgZG8gTk9UIE1BVENIIScpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRldmljZS5nZXRGaW5nZXJwcmludCgpICE9PSBmaW5nZXJwcmludCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZnByaW50ID0gZGV2aWNlLmdldEZpbmdlcnByaW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3QoJ1dBUk5JTkc6IEtFWSBWRVJJRklDQVRJT04gRkFJTEVEISBUaGUgc2lnbmluZyBrZXkgZm9yICUodXNlcklkKXMgYW5kIHNlc3Npb24nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJyAlKGRldmljZUlkKXMgaXMgXCIlKGZwcmludClzXCIgd2hpY2ggZG9lcyBub3QgbWF0Y2ggdGhlIHByb3ZpZGVkIGtleSAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1wiJShmaW5nZXJwcmludClzXCIuIFRoaXMgY291bGQgbWVhbiB5b3VyIGNvbW11bmljYXRpb25zIGFyZSBiZWluZyBpbnRlcmNlcHRlZCEnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmcHJpbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXZpY2VJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbmdlcnByaW50LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IGNsaS5zZXREZXZpY2VWZXJpZmllZCh1c2VySWQsIGRldmljZUlkLCB0cnVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRlbGwgdGhlIHVzZXIgd2UgdmVyaWZpZWQgZXZlcnl0aGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBJbmZvRGlhbG9nID0gc2RrLmdldENvbXBvbmVudCgnZGlhbG9ncy5JbmZvRGlhbG9nJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NsYXNoIENvbW1hbmRzJywgJ1ZlcmlmaWVkIGtleScsIEluZm9EaWFsb2csIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnVmVyaWZpZWQga2V5JyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3QoJ1RoZSBzaWduaW5nIGtleSB5b3UgcHJvdmlkZWQgbWF0Y2hlcyB0aGUgc2lnbmluZyBrZXkgeW91IHJlY2VpdmVkICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmcm9tICUodXNlcklkKXNcXCdzIHNlc3Npb24gJShkZXZpY2VJZClzLiBTZXNzaW9uIG1hcmtlZCBhcyB2ZXJpZmllZC4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt1c2VySWQsIGRldmljZUlkfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PixcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkoKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzYWdlKCkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLmFkdmFuY2VkLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogJ2Rpc2NhcmRzZXNzaW9uJyxcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKCdGb3JjZXMgdGhlIGN1cnJlbnQgb3V0Ym91bmQgZ3JvdXAgc2Vzc2lvbiBpbiBhbiBlbmNyeXB0ZWQgcm9vbSB0byBiZSBkaXNjYXJkZWQnKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkKSB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBNYXRyaXhDbGllbnRQZWcuZ2V0KCkuZm9yY2VEaXNjYXJkU2Vzc2lvbihyb29tSWQpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVqZWN0KGUubWVzc2FnZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZHZhbmNlZCxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6IFwicmFpbmJvd1wiLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBfdGQoXCJTZW5kcyB0aGUgZ2l2ZW4gbWVzc2FnZSBjb2xvdXJlZCBhcyBhIHJhaW5ib3dcIiksXHJcbiAgICAgICAgYXJnczogJzxtZXNzYWdlPicsXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgYXJncykge1xyXG4gICAgICAgICAgICBpZiAoIWFyZ3MpIHJldHVybiByZWplY3QodGhpcy5nZXRVc2VySWQoKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKE1hdHJpeENsaWVudFBlZy5nZXQoKS5zZW5kSHRtbE1lc3NhZ2Uocm9vbUlkLCBhcmdzLCB0ZXh0VG9IdG1sUmFpbmJvdyhhcmdzKSkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLm1lc3NhZ2VzLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogXCJyYWluYm93bWVcIixcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKFwiU2VuZHMgdGhlIGdpdmVuIGVtb3RlIGNvbG91cmVkIGFzIGEgcmFpbmJvd1wiKSxcclxuICAgICAgICBhcmdzOiAnPG1lc3NhZ2U+JyxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24ocm9vbUlkLCBhcmdzKSB7XHJcbiAgICAgICAgICAgIGlmICghYXJncykgcmV0dXJuIHJlamVjdCh0aGlzLmdldFVzZXJJZCgpKTtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoTWF0cml4Q2xpZW50UGVnLmdldCgpLnNlbmRIdG1sRW1vdGUocm9vbUlkLCBhcmdzLCB0ZXh0VG9IdG1sUmFpbmJvdyhhcmdzKSkpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLm1lc3NhZ2VzLFxyXG4gICAgfSksXHJcbiAgICBuZXcgQ29tbWFuZCh7XHJcbiAgICAgICAgY29tbWFuZDogXCJoZWxwXCIsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZChcIkRpc3BsYXlzIGxpc3Qgb2YgY29tbWFuZHMgd2l0aCB1c2FnZXMgYW5kIGRlc2NyaXB0aW9uc1wiKSxcclxuICAgICAgICBydW5GbjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IFNsYXNoQ29tbWFuZEhlbHBEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KCdkaWFsb2dzLlNsYXNoQ29tbWFuZEhlbHBEaWFsb2cnKTtcclxuXHJcbiAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ1NsYXNoIENvbW1hbmRzJywgJ0hlbHAnLCBTbGFzaENvbW1hbmRIZWxwRGlhbG9nKTtcclxuICAgICAgICAgICAgcmV0dXJuIHN1Y2Nlc3MoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNhdGVnb3J5OiBDb21tYW5kQ2F0ZWdvcmllcy5hZHZhbmNlZCxcclxuICAgIH0pLFxyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6IFwid2hvaXNcIixcclxuICAgICAgICBkZXNjcmlwdGlvbjogX3RkKFwiRGlzcGxheXMgaW5mb3JtYXRpb24gYWJvdXQgYSB1c2VyXCIpLFxyXG4gICAgICAgIGFyZ3M6IFwiPHVzZXItaWQ+XCIsXHJcbiAgICAgICAgcnVuRm46IGZ1bmN0aW9uKHJvb21JZCwgdXNlcklkKSB7XHJcbiAgICAgICAgICAgIGlmICghdXNlcklkIHx8ICF1c2VySWQuc3RhcnRzV2l0aChcIkBcIikgfHwgIXVzZXJJZC5pbmNsdWRlcyhcIjpcIikpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZWplY3QodGhpcy5nZXRVc2FnZSgpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgbWVtYmVyID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLmdldFJvb20ocm9vbUlkKS5nZXRNZW1iZXIodXNlcklkKTtcclxuICAgICAgICAgICAgZGlzLmRpc3BhdGNoKHtcclxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3ZpZXdfdXNlcicsXHJcbiAgICAgICAgICAgICAgICBtZW1iZXI6IG1lbWJlciB8fCB7dXNlcklkfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBzdWNjZXNzKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjYXRlZ29yeTogQ29tbWFuZENhdGVnb3JpZXMuYWR2YW5jZWQsXHJcbiAgICB9KSxcclxuXHJcbiAgICAvLyBDb21tYW5kIGRlZmluaXRpb25zIGZvciBhdXRvY29tcGxldGlvbiBPTkxZOlxyXG4gICAgLy8gL21lIGlzIHNwZWNpYWwgYmVjYXVzZSBpdHMgbm90IGhhbmRsZWQgYnkgU2xhc2hDb21tYW5kcy5qcyBhbmQgaXMgaW5zdGVhZCBkb25lIGluc2lkZSB0aGUgQ29tcG9zZXIgY2xhc3Nlc1xyXG4gICAgbmV3IENvbW1hbmQoe1xyXG4gICAgICAgIGNvbW1hbmQ6ICdtZScsXHJcbiAgICAgICAgYXJnczogJzxtZXNzYWdlPicsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IF90ZCgnRGlzcGxheXMgYWN0aW9uJyksXHJcbiAgICAgICAgY2F0ZWdvcnk6IENvbW1hbmRDYXRlZ29yaWVzLm1lc3NhZ2VzLFxyXG4gICAgICAgIGhpZGVDb21wbGV0aW9uQWZ0ZXJTcGFjZTogdHJ1ZSxcclxuICAgIH0pLFxyXG5dO1xyXG5cclxuLy8gYnVpbGQgYSBtYXAgZnJvbSBuYW1lcyBhbmQgYWxpYXNlcyB0byB0aGUgQ29tbWFuZCBvYmplY3RzLlxyXG5leHBvcnQgY29uc3QgQ29tbWFuZE1hcCA9IG5ldyBNYXAoKTtcclxuQ29tbWFuZHMuZm9yRWFjaChjbWQgPT4ge1xyXG4gICAgQ29tbWFuZE1hcC5zZXQoY21kLmNvbW1hbmQsIGNtZCk7XHJcbiAgICBjbWQuYWxpYXNlcy5mb3JFYWNoKGFsaWFzID0+IHtcclxuICAgICAgICBDb21tYW5kTWFwLnNldChhbGlhcywgY21kKTtcclxuICAgIH0pO1xyXG59KTtcclxuXHJcblxyXG4vKipcclxuICogUHJvY2VzcyB0aGUgZ2l2ZW4gdGV4dCBmb3IgL2NvbW1hbmRzIGFuZCByZXR1cm4gYSBib3VuZCBtZXRob2QgdG8gcGVyZm9ybSB0aGVtLlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gcm9vbUlkIFRoZSByb29tIGluIHdoaWNoIHRoZSBjb21tYW5kIHdhcyBwZXJmb3JtZWQuXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBpbnB1dCBUaGUgcmF3IHRleHQgaW5wdXQgYnkgdGhlIHVzZXIuXHJcbiAqIEByZXR1cm4ge251bGx8ZnVuY3Rpb24oKTogT2JqZWN0fSBGdW5jdGlvbiByZXR1cm5pbmcgYW4gb2JqZWN0IHdpdGggdGhlIHByb3BlcnR5ICdlcnJvcicgaWYgdGhlcmUgd2FzIGFuIGVycm9yXHJcbiAqIHByb2Nlc3NpbmcgdGhlIGNvbW1hbmQsIG9yICdwcm9taXNlJyBpZiBhIHJlcXVlc3Qgd2FzIHNlbnQgb3V0LlxyXG4gKiBSZXR1cm5zIG51bGwgaWYgdGhlIGlucHV0IGRpZG4ndCBtYXRjaCBhIGNvbW1hbmQuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q29tbWFuZChyb29tSWQsIGlucHV0KSB7XHJcbiAgICAvLyB0cmltIGFueSB0cmFpbGluZyB3aGl0ZXNwYWNlLCBhcyBpdCBjYW4gY29uZnVzZSB0aGUgcGFyc2VyIGZvclxyXG4gICAgLy8gSVJDLXN0eWxlIGNvbW1hbmRzXHJcbiAgICBpbnB1dCA9IGlucHV0LnJlcGxhY2UoL1xccyskLywgJycpO1xyXG4gICAgaWYgKGlucHV0WzBdICE9PSAnLycpIHJldHVybiBudWxsOyAvLyBub3QgYSBjb21tYW5kXHJcblxyXG4gICAgY29uc3QgYml0cyA9IGlucHV0Lm1hdGNoKC9eKFxcUys/KSg/OiArKCgufFxcbikqKSk/JC8pO1xyXG4gICAgbGV0IGNtZDtcclxuICAgIGxldCBhcmdzO1xyXG4gICAgaWYgKGJpdHMpIHtcclxuICAgICAgICBjbWQgPSBiaXRzWzFdLnN1YnN0cmluZygxKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIGFyZ3MgPSBiaXRzWzJdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjbWQgPSBpbnB1dDtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoQ29tbWFuZE1hcC5oYXMoY21kKSkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiBDb21tYW5kTWFwLmdldChjbWQpLnJ1bihyb29tSWQsIGFyZ3MsIGNtZCk7XHJcbiAgICB9XHJcbn1cclxuIl19