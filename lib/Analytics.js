"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _languageHandler = require("./languageHandler");

var _PlatformPeg = _interopRequireDefault(require("./PlatformPeg"));

var _SdkConfig = _interopRequireDefault(require("./SdkConfig"));

var _Modal = _interopRequireDefault(require("./Modal"));

var sdk = _interopRequireWildcard(require("./index"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

const hashRegex = /#\/(groups?|room|user|settings|register|login|forgot_password|home|directory)/;
const hashVarRegex = /#\/(group|room|user)\/.*$/; // Remove all but the first item in the hash path. Redact unexpected hashes.

function getRedactedHash(hash) {
  // Don't leak URLs we aren't expecting - they could contain tokens/PII
  const match = hashRegex.exec(hash);

  if (!match) {
    console.warn("Unexpected hash location \"".concat(hash, "\""));
    return '#/<unexpected hash location>';
  }

  if (hashVarRegex.test(hash)) {
    return hash.replace(hashVarRegex, "#/$1/<redacted>");
  }

  return hash.replace(hashRegex, "#/$1");
} // Return the current origin, path and hash separated with a `/`. This does
// not include query parameters.


function getRedactedUrl() {
  const {
    origin,
    hash
  } = window.location;
  let {
    pathname
  } = window.location; // Redact paths which could contain unexpected PII

  if (origin.startsWith('file://')) {
    pathname = "/<redacted>/";
  }

  return origin + pathname + getRedactedHash(hash);
}

const customVariables = {
  // The Matomo installation at https://matomo.riot.im is currently configured
  // with a limit of 10 custom variables.
  'App Platform': {
    id: 1,
    expl: (0, _languageHandler._td)('The platform you\'re on'),
    example: 'Electron Platform'
  },
  'App Version': {
    id: 2,
    expl: (0, _languageHandler._td)('The version of Riot'),
    example: '15.0.0'
  },
  'User Type': {
    id: 3,
    expl: (0, _languageHandler._td)('Whether or not you\'re logged in (we don\'t record your username)'),
    example: 'Logged In'
  },
  'Chosen Language': {
    id: 4,
    expl: (0, _languageHandler._td)('Your language of choice'),
    example: 'en'
  },
  'Instance': {
    id: 5,
    expl: (0, _languageHandler._td)('Which officially provided instance you are using, if any'),
    example: 'app'
  },
  'RTE: Uses Richtext Mode': {
    id: 6,
    expl: (0, _languageHandler._td)('Whether or not you\'re using the Richtext mode of the Rich Text Editor'),
    example: 'off'
  },
  'Homeserver URL': {
    id: 7,
    expl: (0, _languageHandler._td)('Your homeserver\'s URL'),
    example: 'https://matrix.org'
  },
  'Touch Input': {
    id: 8,
    expl: (0, _languageHandler._td)("Whether you're using Riot on a device where touch is the primary input mechanism"),
    example: 'false'
  },
  'Breadcrumbs': {
    id: 9,
    expl: (0, _languageHandler._td)("Whether or not you're using the 'breadcrumbs' feature (avatars above the room list)"),
    example: 'disabled'
  },
  'Installed PWA': {
    id: 10,
    expl: (0, _languageHandler._td)("Whether you're using Riot as an installed Progressive Web App"),
    example: 'false'
  }
};

function whitelistRedact(whitelist, str) {
  if (whitelist.includes(str)) return str;
  return '<redacted>';
}

const UID_KEY = "mx_Riot_Analytics_uid";
const CREATION_TS_KEY = "mx_Riot_Analytics_cts";
const VISIT_COUNT_KEY = "mx_Riot_Analytics_vc";
const LAST_VISIT_TS_KEY = "mx_Riot_Analytics_lvts";

function getUid() {
  try {
    let data = localStorage.getItem(UID_KEY);

    if (!data) {
      localStorage.setItem(UID_KEY, data = [...Array(16)].map(() => Math.random().toString(16)[2]).join(''));
    }

    return data;
  } catch (e) {
    console.error("Analytics error: ", e);
    return "";
  }
}

const HEARTBEAT_INTERVAL = 30 * 1000; // seconds

class Analytics {
  constructor() {
    (0, _defineProperty2.default)(this, "showDetailsModal", () => {
      let rows = [];

      if (!this.disabled) {
        rows = Object.values(this.visitVariables);
      } else {
        rows = Object.keys(customVariables).map(k => [k, (0, _languageHandler._t)('e.g. %(exampleValue)s', {
          exampleValue: customVariables[k].example
        })]);
      }

      const resolution = "".concat(window.screen.width, "x").concat(window.screen.height);
      const otherVariables = [{
        expl: (0, _languageHandler._td)('Every page you use in the app'),
        value: (0, _languageHandler._t)('e.g. <CurrentPageURL>', {}, {
          CurrentPageURL: getRedactedUrl()
        })
      }, {
        expl: (0, _languageHandler._td)('Your user agent'),
        value: navigator.userAgent
      }, {
        expl: (0, _languageHandler._td)('Your device resolution'),
        value: resolution
      }];
      const ErrorDialog = sdk.getComponent('dialogs.ErrorDialog');

      _Modal.default.createTrackedDialog('Analytics Details', '', ErrorDialog, {
        title: (0, _languageHandler._t)('Analytics'),
        description: _react.default.createElement("div", {
          className: "mx_AnalyticsModal"
        }, _react.default.createElement("div", null, (0, _languageHandler._t)('The information being sent to us to help make Riot better includes:')), _react.default.createElement("table", null, rows.map(row => _react.default.createElement("tr", {
          key: row[0]
        }, _react.default.createElement("td", null, (0, _languageHandler._t)(customVariables[row[0]].expl)), row[1] !== undefined && _react.default.createElement("td", null, _react.default.createElement("code", null, row[1])))), otherVariables.map((item, index) => _react.default.createElement("tr", {
          key: index
        }, _react.default.createElement("td", null, (0, _languageHandler._t)(item.expl)), _react.default.createElement("td", null, _react.default.createElement("code", null, item.value))))), _react.default.createElement("div", null, (0, _languageHandler._t)('Where this page includes identifiable information, such as a room, ' + 'user or group ID, that data is removed before being sent to the server.')))
      });
    });
    this.baseUrl = null;
    this.siteId = null;
    this.visitVariables = {};
    this.firstPage = true;
    this._heartbeatIntervalID = null;
    this.creationTs = localStorage.getItem(CREATION_TS_KEY);

    if (!this.creationTs) {
      localStorage.setItem(CREATION_TS_KEY, this.creationTs = new Date().getTime());
    }

    this.lastVisitTs = localStorage.getItem(LAST_VISIT_TS_KEY);
    this.visitCount = localStorage.getItem(VISIT_COUNT_KEY) || 0;
    localStorage.setItem(VISIT_COUNT_KEY, parseInt(this.visitCount, 10) + 1);
  }

  get disabled() {
    return !this.baseUrl;
  }
  /**
   * Enable Analytics if initialized but disabled
   * otherwise try and initalize, no-op if piwik config missing
   */


  async enable() {
    if (!this.disabled) return;

    const config = _SdkConfig.default.get();

    if (!config || !config.piwik || !config.piwik.url || !config.piwik.siteId) return;
    this.baseUrl = new URL("piwik.php", config.piwik.url); // set constants

    this.baseUrl.searchParams.set("rec", 1); // rec is required for tracking

    this.baseUrl.searchParams.set("idsite", config.piwik.siteId); // rec is required for tracking

    this.baseUrl.searchParams.set("apiv", 1); // API version to use

    this.baseUrl.searchParams.set("send_image", 0); // we want a 204, not a tiny GIF
    // set user parameters

    this.baseUrl.searchParams.set("_id", getUid()); // uuid

    this.baseUrl.searchParams.set("_idts", this.creationTs); // first ts

    this.baseUrl.searchParams.set("_idvc", parseInt(this.visitCount, 10) + 1); // visit count

    if (this.lastVisitTs) {
      this.baseUrl.searchParams.set("_viewts", this.lastVisitTs); // last visit ts
    }

    const platform = _PlatformPeg.default.get();

    this._setVisitVariable('App Platform', platform.getHumanReadableName());

    try {
      this._setVisitVariable('App Version', (await platform.getAppVersion()));
    } catch (e) {
      this._setVisitVariable('App Version', 'unknown');
    }

    this._setVisitVariable('Chosen Language', (0, _languageHandler.getCurrentLanguage)());

    if (window.location.hostname === 'riot.im') {
      this._setVisitVariable('Instance', window.location.pathname);
    }

    let installedPWA = "unknown";

    try {
      // Known to work at least for desktop Chrome
      installedPWA = window.matchMedia('(display-mode: standalone)').matches;
    } catch (e) {}

    this._setVisitVariable('Installed PWA', installedPWA);

    let touchInput = "unknown";

    try {
      // MDN claims broad support across browsers
      touchInput = window.matchMedia('(pointer: coarse)').matches;
    } catch (e) {}

    this._setVisitVariable('Touch Input', touchInput); // start heartbeat


    this._heartbeatIntervalID = window.setInterval(this.ping.bind(this), HEARTBEAT_INTERVAL);
  }
  /**
   * Disable Analytics, stop the heartbeat and clear identifiers from localStorage
   */


  disable() {
    if (this.disabled) return;
    this.trackEvent('Analytics', 'opt-out');
    window.clearInterval(this._heartbeatIntervalID);
    this.baseUrl = null;
    this.visitVariables = {};
    localStorage.removeItem(UID_KEY);
    localStorage.removeItem(CREATION_TS_KEY);
    localStorage.removeItem(VISIT_COUNT_KEY);
    localStorage.removeItem(LAST_VISIT_TS_KEY);
  }

  async _track(data) {
    if (this.disabled) return;
    const now = new Date();

    const params = _objectSpread({}, data, {
      url: getRedactedUrl(),
      _cvar: JSON.stringify(this.visitVariables),
      // user custom vars
      res: "".concat(window.screen.width, "x").concat(window.screen.height),
      // resolution as WWWWxHHHH
      rand: String(Math.random()).slice(2, 8),
      // random nonce to cache-bust
      h: now.getHours(),
      m: now.getMinutes(),
      s: now.getSeconds()
    });

    const url = new URL(this.baseUrl);

    for (const key in params) {
      url.searchParams.set(key, params[key]);
    }

    try {
      await window.fetch(url, {
        method: "GET",
        mode: "no-cors",
        cache: "no-cache",
        redirect: "follow"
      });
    } catch (e) {
      console.error("Analytics error: ", e);
    }
  }

  ping() {
    this._track({
      ping: 1
    });

    localStorage.setItem(LAST_VISIT_TS_KEY, new Date().getTime()); // update last visit ts
  }

  trackPageChange(generationTimeMs) {
    if (this.disabled) return;

    if (this.firstPage) {
      // De-duplicate first page
      // router seems to hit the fn twice
      this.firstPage = false;
      return;
    }

    if (typeof generationTimeMs !== 'number') {
      console.warn('Analytics.trackPageChange: expected generationTimeMs to be a number'); // But continue anyway because we still want to track the change
    }

    this._track({
      gt_ms: generationTimeMs
    });
  }

  trackEvent(category, action, name, value) {
    if (this.disabled) return;

    this._track({
      e_c: category,
      e_a: action,
      e_n: name,
      e_v: value
    });
  }

  _setVisitVariable(key, value) {
    if (this.disabled) return;
    this.visitVariables[customVariables[key].id] = [key, value];
  }

  setLoggedIn(isGuest, homeserverUrl, identityServerUrl) {
    if (this.disabled) return;

    const config = _SdkConfig.default.get();

    if (!config.piwik) return;
    const whitelistedHSUrls = config.piwik.whitelistedHSUrls || [];

    this._setVisitVariable('User Type', isGuest ? 'Guest' : 'Logged In');

    this._setVisitVariable('Homeserver URL', whitelistRedact(whitelistedHSUrls, homeserverUrl));
  }

  setBreadcrumbs(state) {
    if (this.disabled) return;

    this._setVisitVariable('Breadcrumbs', state ? 'enabled' : 'disabled');
  }

}

if (!global.mxAnalytics) {
  global.mxAnalytics = new Analytics();
}

var _default = global.mxAnalytics;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9BbmFseXRpY3MuanMiXSwibmFtZXMiOlsiaGFzaFJlZ2V4IiwiaGFzaFZhclJlZ2V4IiwiZ2V0UmVkYWN0ZWRIYXNoIiwiaGFzaCIsIm1hdGNoIiwiZXhlYyIsImNvbnNvbGUiLCJ3YXJuIiwidGVzdCIsInJlcGxhY2UiLCJnZXRSZWRhY3RlZFVybCIsIm9yaWdpbiIsIndpbmRvdyIsImxvY2F0aW9uIiwicGF0aG5hbWUiLCJzdGFydHNXaXRoIiwiY3VzdG9tVmFyaWFibGVzIiwiaWQiLCJleHBsIiwiZXhhbXBsZSIsIndoaXRlbGlzdFJlZGFjdCIsIndoaXRlbGlzdCIsInN0ciIsImluY2x1ZGVzIiwiVUlEX0tFWSIsIkNSRUFUSU9OX1RTX0tFWSIsIlZJU0lUX0NPVU5UX0tFWSIsIkxBU1RfVklTSVRfVFNfS0VZIiwiZ2V0VWlkIiwiZGF0YSIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJzZXRJdGVtIiwiQXJyYXkiLCJtYXAiLCJNYXRoIiwicmFuZG9tIiwidG9TdHJpbmciLCJqb2luIiwiZSIsImVycm9yIiwiSEVBUlRCRUFUX0lOVEVSVkFMIiwiQW5hbHl0aWNzIiwiY29uc3RydWN0b3IiLCJyb3dzIiwiZGlzYWJsZWQiLCJPYmplY3QiLCJ2YWx1ZXMiLCJ2aXNpdFZhcmlhYmxlcyIsImtleXMiLCJrIiwiZXhhbXBsZVZhbHVlIiwicmVzb2x1dGlvbiIsInNjcmVlbiIsIndpZHRoIiwiaGVpZ2h0Iiwib3RoZXJWYXJpYWJsZXMiLCJ2YWx1ZSIsIkN1cnJlbnRQYWdlVVJMIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwicm93IiwidW5kZWZpbmVkIiwiaXRlbSIsImluZGV4IiwiYmFzZVVybCIsInNpdGVJZCIsImZpcnN0UGFnZSIsIl9oZWFydGJlYXRJbnRlcnZhbElEIiwiY3JlYXRpb25UcyIsIkRhdGUiLCJnZXRUaW1lIiwibGFzdFZpc2l0VHMiLCJ2aXNpdENvdW50IiwicGFyc2VJbnQiLCJlbmFibGUiLCJjb25maWciLCJTZGtDb25maWciLCJnZXQiLCJwaXdpayIsInVybCIsIlVSTCIsInNlYXJjaFBhcmFtcyIsInNldCIsInBsYXRmb3JtIiwiUGxhdGZvcm1QZWciLCJfc2V0VmlzaXRWYXJpYWJsZSIsImdldEh1bWFuUmVhZGFibGVOYW1lIiwiZ2V0QXBwVmVyc2lvbiIsImhvc3RuYW1lIiwiaW5zdGFsbGVkUFdBIiwibWF0Y2hNZWRpYSIsIm1hdGNoZXMiLCJ0b3VjaElucHV0Iiwic2V0SW50ZXJ2YWwiLCJwaW5nIiwiYmluZCIsImRpc2FibGUiLCJ0cmFja0V2ZW50IiwiY2xlYXJJbnRlcnZhbCIsInJlbW92ZUl0ZW0iLCJfdHJhY2siLCJub3ciLCJwYXJhbXMiLCJfY3ZhciIsIkpTT04iLCJzdHJpbmdpZnkiLCJyZXMiLCJyYW5kIiwiU3RyaW5nIiwic2xpY2UiLCJoIiwiZ2V0SG91cnMiLCJtIiwiZ2V0TWludXRlcyIsInMiLCJnZXRTZWNvbmRzIiwia2V5IiwiZmV0Y2giLCJtZXRob2QiLCJtb2RlIiwiY2FjaGUiLCJyZWRpcmVjdCIsInRyYWNrUGFnZUNoYW5nZSIsImdlbmVyYXRpb25UaW1lTXMiLCJndF9tcyIsImNhdGVnb3J5IiwiYWN0aW9uIiwibmFtZSIsImVfYyIsImVfYSIsImVfbiIsImVfdiIsInNldExvZ2dlZEluIiwiaXNHdWVzdCIsImhvbWVzZXJ2ZXJVcmwiLCJpZGVudGl0eVNlcnZlclVybCIsIndoaXRlbGlzdGVkSFNVcmxzIiwic2V0QnJlYWRjcnVtYnMiLCJzdGF0ZSIsImdsb2JhbCIsIm14QW5hbHl0aWNzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBaUJBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxNQUFNQSxTQUFTLEdBQUcsK0VBQWxCO0FBQ0EsTUFBTUMsWUFBWSxHQUFHLDJCQUFyQixDLENBRUE7O0FBQ0EsU0FBU0MsZUFBVCxDQUF5QkMsSUFBekIsRUFBK0I7QUFDM0I7QUFDQSxRQUFNQyxLQUFLLEdBQUdKLFNBQVMsQ0FBQ0ssSUFBVixDQUFlRixJQUFmLENBQWQ7O0FBQ0EsTUFBSSxDQUFDQyxLQUFMLEVBQVk7QUFDUkUsSUFBQUEsT0FBTyxDQUFDQyxJQUFSLHNDQUEwQ0osSUFBMUM7QUFDQSxXQUFPLDhCQUFQO0FBQ0g7O0FBRUQsTUFBSUYsWUFBWSxDQUFDTyxJQUFiLENBQWtCTCxJQUFsQixDQUFKLEVBQTZCO0FBQ3pCLFdBQU9BLElBQUksQ0FBQ00sT0FBTCxDQUFhUixZQUFiLEVBQTJCLGlCQUEzQixDQUFQO0FBQ0g7O0FBRUQsU0FBT0UsSUFBSSxDQUFDTSxPQUFMLENBQWFULFNBQWIsRUFBd0IsTUFBeEIsQ0FBUDtBQUNILEMsQ0FFRDtBQUNBOzs7QUFDQSxTQUFTVSxjQUFULEdBQTBCO0FBQ3RCLFFBQU07QUFBRUMsSUFBQUEsTUFBRjtBQUFVUixJQUFBQTtBQUFWLE1BQW1CUyxNQUFNLENBQUNDLFFBQWhDO0FBQ0EsTUFBSTtBQUFFQyxJQUFBQTtBQUFGLE1BQWVGLE1BQU0sQ0FBQ0MsUUFBMUIsQ0FGc0IsQ0FJdEI7O0FBQ0EsTUFBSUYsTUFBTSxDQUFDSSxVQUFQLENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDOUJELElBQUFBLFFBQVEsR0FBRyxjQUFYO0FBQ0g7O0FBRUQsU0FBT0gsTUFBTSxHQUFHRyxRQUFULEdBQW9CWixlQUFlLENBQUNDLElBQUQsQ0FBMUM7QUFDSDs7QUFFRCxNQUFNYSxlQUFlLEdBQUc7QUFDcEI7QUFDQTtBQUNBLGtCQUFnQjtBQUNaQyxJQUFBQSxFQUFFLEVBQUUsQ0FEUTtBQUVaQyxJQUFBQSxJQUFJLEVBQUUsMEJBQUkseUJBQUosQ0FGTTtBQUdaQyxJQUFBQSxPQUFPLEVBQUU7QUFIRyxHQUhJO0FBUXBCLGlCQUFlO0FBQ1hGLElBQUFBLEVBQUUsRUFBRSxDQURPO0FBRVhDLElBQUFBLElBQUksRUFBRSwwQkFBSSxxQkFBSixDQUZLO0FBR1hDLElBQUFBLE9BQU8sRUFBRTtBQUhFLEdBUks7QUFhcEIsZUFBYTtBQUNURixJQUFBQSxFQUFFLEVBQUUsQ0FESztBQUVUQyxJQUFBQSxJQUFJLEVBQUUsMEJBQUksbUVBQUosQ0FGRztBQUdUQyxJQUFBQSxPQUFPLEVBQUU7QUFIQSxHQWJPO0FBa0JwQixxQkFBbUI7QUFDZkYsSUFBQUEsRUFBRSxFQUFFLENBRFc7QUFFZkMsSUFBQUEsSUFBSSxFQUFFLDBCQUFJLHlCQUFKLENBRlM7QUFHZkMsSUFBQUEsT0FBTyxFQUFFO0FBSE0sR0FsQkM7QUF1QnBCLGNBQVk7QUFDUkYsSUFBQUEsRUFBRSxFQUFFLENBREk7QUFFUkMsSUFBQUEsSUFBSSxFQUFFLDBCQUFJLDBEQUFKLENBRkU7QUFHUkMsSUFBQUEsT0FBTyxFQUFFO0FBSEQsR0F2QlE7QUE0QnBCLDZCQUEyQjtBQUN2QkYsSUFBQUEsRUFBRSxFQUFFLENBRG1CO0FBRXZCQyxJQUFBQSxJQUFJLEVBQUUsMEJBQUksd0VBQUosQ0FGaUI7QUFHdkJDLElBQUFBLE9BQU8sRUFBRTtBQUhjLEdBNUJQO0FBaUNwQixvQkFBa0I7QUFDZEYsSUFBQUEsRUFBRSxFQUFFLENBRFU7QUFFZEMsSUFBQUEsSUFBSSxFQUFFLDBCQUFJLHdCQUFKLENBRlE7QUFHZEMsSUFBQUEsT0FBTyxFQUFFO0FBSEssR0FqQ0U7QUFzQ3BCLGlCQUFlO0FBQ1hGLElBQUFBLEVBQUUsRUFBRSxDQURPO0FBRVhDLElBQUFBLElBQUksRUFBRSwwQkFBSSxrRkFBSixDQUZLO0FBR1hDLElBQUFBLE9BQU8sRUFBRTtBQUhFLEdBdENLO0FBMkNwQixpQkFBZTtBQUNYRixJQUFBQSxFQUFFLEVBQUUsQ0FETztBQUVYQyxJQUFBQSxJQUFJLEVBQUUsMEJBQUkscUZBQUosQ0FGSztBQUdYQyxJQUFBQSxPQUFPLEVBQUU7QUFIRSxHQTNDSztBQWdEcEIsbUJBQWlCO0FBQ2JGLElBQUFBLEVBQUUsRUFBRSxFQURTO0FBRWJDLElBQUFBLElBQUksRUFBRSwwQkFBSSwrREFBSixDQUZPO0FBR2JDLElBQUFBLE9BQU8sRUFBRTtBQUhJO0FBaERHLENBQXhCOztBQXVEQSxTQUFTQyxlQUFULENBQXlCQyxTQUF6QixFQUFvQ0MsR0FBcEMsRUFBeUM7QUFDckMsTUFBSUQsU0FBUyxDQUFDRSxRQUFWLENBQW1CRCxHQUFuQixDQUFKLEVBQTZCLE9BQU9BLEdBQVA7QUFDN0IsU0FBTyxZQUFQO0FBQ0g7O0FBRUQsTUFBTUUsT0FBTyxHQUFHLHVCQUFoQjtBQUNBLE1BQU1DLGVBQWUsR0FBRyx1QkFBeEI7QUFDQSxNQUFNQyxlQUFlLEdBQUcsc0JBQXhCO0FBQ0EsTUFBTUMsaUJBQWlCLEdBQUcsd0JBQTFCOztBQUVBLFNBQVNDLE1BQVQsR0FBa0I7QUFDZCxNQUFJO0FBQ0EsUUFBSUMsSUFBSSxHQUFHQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUJQLE9BQXJCLENBQVg7O0FBQ0EsUUFBSSxDQUFDSyxJQUFMLEVBQVc7QUFDUEMsTUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCUixPQUFyQixFQUE4QkssSUFBSSxHQUFHLENBQUMsR0FBR0ksS0FBSyxDQUFDLEVBQUQsQ0FBVCxFQUFlQyxHQUFmLENBQW1CLE1BQU1DLElBQUksQ0FBQ0MsTUFBTCxHQUFjQyxRQUFkLENBQXVCLEVBQXZCLEVBQTJCLENBQTNCLENBQXpCLEVBQXdEQyxJQUF4RCxDQUE2RCxFQUE3RCxDQUFyQztBQUNIOztBQUNELFdBQU9ULElBQVA7QUFDSCxHQU5ELENBTUUsT0FBT1UsQ0FBUCxFQUFVO0FBQ1JqQyxJQUFBQSxPQUFPLENBQUNrQyxLQUFSLENBQWMsbUJBQWQsRUFBbUNELENBQW5DO0FBQ0EsV0FBTyxFQUFQO0FBQ0g7QUFDSjs7QUFFRCxNQUFNRSxrQkFBa0IsR0FBRyxLQUFLLElBQWhDLEMsQ0FBc0M7O0FBRXRDLE1BQU1DLFNBQU4sQ0FBZ0I7QUFDWkMsRUFBQUEsV0FBVyxHQUFHO0FBQUEsNERBd0xLLE1BQU07QUFDckIsVUFBSUMsSUFBSSxHQUFHLEVBQVg7O0FBQ0EsVUFBSSxDQUFDLEtBQUtDLFFBQVYsRUFBb0I7QUFDaEJELFFBQUFBLElBQUksR0FBR0UsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBS0MsY0FBbkIsQ0FBUDtBQUNILE9BRkQsTUFFTztBQUNISixRQUFBQSxJQUFJLEdBQUdFLE1BQU0sQ0FBQ0csSUFBUCxDQUFZakMsZUFBWixFQUE2QmtCLEdBQTdCLENBQ0ZnQixDQUFELElBQU8sQ0FDSEEsQ0FERyxFQUVILHlCQUFHLHVCQUFILEVBQTRCO0FBQUVDLFVBQUFBLFlBQVksRUFBRW5DLGVBQWUsQ0FBQ2tDLENBQUQsQ0FBZixDQUFtQi9CO0FBQW5DLFNBQTVCLENBRkcsQ0FESixDQUFQO0FBTUg7O0FBRUQsWUFBTWlDLFVBQVUsYUFBTXhDLE1BQU0sQ0FBQ3lDLE1BQVAsQ0FBY0MsS0FBcEIsY0FBNkIxQyxNQUFNLENBQUN5QyxNQUFQLENBQWNFLE1BQTNDLENBQWhCO0FBQ0EsWUFBTUMsY0FBYyxHQUFHLENBQ25CO0FBQ0l0QyxRQUFBQSxJQUFJLEVBQUUsMEJBQUksK0JBQUosQ0FEVjtBQUVJdUMsUUFBQUEsS0FBSyxFQUFFLHlCQUNILHVCQURHLEVBRUgsRUFGRyxFQUdIO0FBQ0lDLFVBQUFBLGNBQWMsRUFBRWhELGNBQWM7QUFEbEMsU0FIRztBQUZYLE9BRG1CLEVBV25CO0FBQUVRLFFBQUFBLElBQUksRUFBRSwwQkFBSSxpQkFBSixDQUFSO0FBQWdDdUMsUUFBQUEsS0FBSyxFQUFFRSxTQUFTLENBQUNDO0FBQWpELE9BWG1CLEVBWW5CO0FBQUUxQyxRQUFBQSxJQUFJLEVBQUUsMEJBQUksd0JBQUosQ0FBUjtBQUF1Q3VDLFFBQUFBLEtBQUssRUFBRUw7QUFBOUMsT0FabUIsQ0FBdkI7QUFlQSxZQUFNUyxXQUFXLEdBQUdDLEdBQUcsQ0FBQ0MsWUFBSixDQUFpQixxQkFBakIsQ0FBcEI7O0FBQ0FDLHFCQUFNQyxtQkFBTixDQUEwQixtQkFBMUIsRUFBK0MsRUFBL0MsRUFBbURKLFdBQW5ELEVBQWdFO0FBQzVESyxRQUFBQSxLQUFLLEVBQUUseUJBQUcsV0FBSCxDQURxRDtBQUU1REMsUUFBQUEsV0FBVyxFQUFFO0FBQUssVUFBQSxTQUFTLEVBQUM7QUFBZixXQUNULDBDQUNNLHlCQUFHLHFFQUFILENBRE4sQ0FEUyxFQUlULDRDQUNNdkIsSUFBSSxDQUFDVixHQUFMLENBQVVrQyxHQUFELElBQVM7QUFBSSxVQUFBLEdBQUcsRUFBRUEsR0FBRyxDQUFDLENBQUQ7QUFBWixXQUNoQix5Q0FBTSx5QkFBR3BELGVBQWUsQ0FBQ29ELEdBQUcsQ0FBQyxDQUFELENBQUosQ0FBZixDQUF3QmxELElBQTNCLENBQU4sQ0FEZ0IsRUFFZGtELEdBQUcsQ0FBQyxDQUFELENBQUgsS0FBV0MsU0FBWCxJQUF3Qix5Q0FBSSwyQ0FBUUQsR0FBRyxDQUFDLENBQUQsQ0FBWCxDQUFKLENBRlYsQ0FBbEIsQ0FETixFQUtNWixjQUFjLENBQUN0QixHQUFmLENBQW1CLENBQUNvQyxJQUFELEVBQU9DLEtBQVAsS0FDakI7QUFBSSxVQUFBLEdBQUcsRUFBRUE7QUFBVCxXQUNJLHlDQUFNLHlCQUFHRCxJQUFJLENBQUNwRCxJQUFSLENBQU4sQ0FESixFQUVJLHlDQUFJLDJDQUFRb0QsSUFBSSxDQUFDYixLQUFiLENBQUosQ0FGSixDQURGLENBTE4sQ0FKUyxFQWdCVCwwQ0FDTSx5QkFBRyx3RUFDQyx5RUFESixDQUROLENBaEJTO0FBRitDLE9BQWhFO0FBd0JILEtBOU9hO0FBQ1YsU0FBS2UsT0FBTCxHQUFlLElBQWY7QUFDQSxTQUFLQyxNQUFMLEdBQWMsSUFBZDtBQUNBLFNBQUt6QixjQUFMLEdBQXNCLEVBQXRCO0FBRUEsU0FBSzBCLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLQyxvQkFBTCxHQUE0QixJQUE1QjtBQUVBLFNBQUtDLFVBQUwsR0FBa0I5QyxZQUFZLENBQUNDLE9BQWIsQ0FBcUJOLGVBQXJCLENBQWxCOztBQUNBLFFBQUksQ0FBQyxLQUFLbUQsVUFBVixFQUFzQjtBQUNsQjlDLE1BQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQlAsZUFBckIsRUFBc0MsS0FBS21ELFVBQUwsR0FBa0IsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQXhEO0FBQ0g7O0FBRUQsU0FBS0MsV0FBTCxHQUFtQmpELFlBQVksQ0FBQ0MsT0FBYixDQUFxQkosaUJBQXJCLENBQW5CO0FBQ0EsU0FBS3FELFVBQUwsR0FBa0JsRCxZQUFZLENBQUNDLE9BQWIsQ0FBcUJMLGVBQXJCLEtBQXlDLENBQTNEO0FBQ0FJLElBQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQk4sZUFBckIsRUFBc0N1RCxRQUFRLENBQUMsS0FBS0QsVUFBTixFQUFrQixFQUFsQixDQUFSLEdBQWdDLENBQXRFO0FBQ0g7O0FBRUQsTUFBSW5DLFFBQUosR0FBZTtBQUNYLFdBQU8sQ0FBQyxLQUFLMkIsT0FBYjtBQUNIO0FBRUQ7Ozs7OztBQUlBLFFBQU1VLE1BQU4sR0FBZTtBQUNYLFFBQUksQ0FBQyxLQUFLckMsUUFBVixFQUFvQjs7QUFFcEIsVUFBTXNDLE1BQU0sR0FBR0MsbUJBQVVDLEdBQVYsRUFBZjs7QUFDQSxRQUFJLENBQUNGLE1BQUQsSUFBVyxDQUFDQSxNQUFNLENBQUNHLEtBQW5CLElBQTRCLENBQUNILE1BQU0sQ0FBQ0csS0FBUCxDQUFhQyxHQUExQyxJQUFpRCxDQUFDSixNQUFNLENBQUNHLEtBQVAsQ0FBYWIsTUFBbkUsRUFBMkU7QUFFM0UsU0FBS0QsT0FBTCxHQUFlLElBQUlnQixHQUFKLENBQVEsV0FBUixFQUFxQkwsTUFBTSxDQUFDRyxLQUFQLENBQWFDLEdBQWxDLENBQWYsQ0FOVyxDQU9YOztBQUNBLFNBQUtmLE9BQUwsQ0FBYWlCLFlBQWIsQ0FBMEJDLEdBQTFCLENBQThCLEtBQTlCLEVBQXFDLENBQXJDLEVBUlcsQ0FROEI7O0FBQ3pDLFNBQUtsQixPQUFMLENBQWFpQixZQUFiLENBQTBCQyxHQUExQixDQUE4QixRQUE5QixFQUF3Q1AsTUFBTSxDQUFDRyxLQUFQLENBQWFiLE1BQXJELEVBVFcsQ0FTbUQ7O0FBQzlELFNBQUtELE9BQUwsQ0FBYWlCLFlBQWIsQ0FBMEJDLEdBQTFCLENBQThCLE1BQTlCLEVBQXNDLENBQXRDLEVBVlcsQ0FVK0I7O0FBQzFDLFNBQUtsQixPQUFMLENBQWFpQixZQUFiLENBQTBCQyxHQUExQixDQUE4QixZQUE5QixFQUE0QyxDQUE1QyxFQVhXLENBV3FDO0FBQ2hEOztBQUNBLFNBQUtsQixPQUFMLENBQWFpQixZQUFiLENBQTBCQyxHQUExQixDQUE4QixLQUE5QixFQUFxQzlELE1BQU0sRUFBM0MsRUFiVyxDQWFxQzs7QUFDaEQsU0FBSzRDLE9BQUwsQ0FBYWlCLFlBQWIsQ0FBMEJDLEdBQTFCLENBQThCLE9BQTlCLEVBQXVDLEtBQUtkLFVBQTVDLEVBZFcsQ0FjOEM7O0FBQ3pELFNBQUtKLE9BQUwsQ0FBYWlCLFlBQWIsQ0FBMEJDLEdBQTFCLENBQThCLE9BQTlCLEVBQXVDVCxRQUFRLENBQUMsS0FBS0QsVUFBTixFQUFrQixFQUFsQixDQUFSLEdBQStCLENBQXRFLEVBZlcsQ0FlK0Q7O0FBQzFFLFFBQUksS0FBS0QsV0FBVCxFQUFzQjtBQUNsQixXQUFLUCxPQUFMLENBQWFpQixZQUFiLENBQTBCQyxHQUExQixDQUE4QixTQUE5QixFQUF5QyxLQUFLWCxXQUE5QyxFQURrQixDQUMwQztBQUMvRDs7QUFFRCxVQUFNWSxRQUFRLEdBQUdDLHFCQUFZUCxHQUFaLEVBQWpCOztBQUNBLFNBQUtRLGlCQUFMLENBQXVCLGNBQXZCLEVBQXVDRixRQUFRLENBQUNHLG9CQUFULEVBQXZDOztBQUNBLFFBQUk7QUFDQSxXQUFLRCxpQkFBTCxDQUF1QixhQUF2QixHQUFzQyxNQUFNRixRQUFRLENBQUNJLGFBQVQsRUFBNUM7QUFDSCxLQUZELENBRUUsT0FBT3hELENBQVAsRUFBVTtBQUNSLFdBQUtzRCxpQkFBTCxDQUF1QixhQUF2QixFQUFzQyxTQUF0QztBQUNIOztBQUVELFNBQUtBLGlCQUFMLENBQXVCLGlCQUF2QixFQUEwQywwQ0FBMUM7O0FBRUEsUUFBSWpGLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQm1GLFFBQWhCLEtBQTZCLFNBQWpDLEVBQTRDO0FBQ3hDLFdBQUtILGlCQUFMLENBQXVCLFVBQXZCLEVBQW1DakYsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxRQUFuRDtBQUNIOztBQUVELFFBQUltRixZQUFZLEdBQUcsU0FBbkI7O0FBQ0EsUUFBSTtBQUNBO0FBQ0FBLE1BQUFBLFlBQVksR0FBR3JGLE1BQU0sQ0FBQ3NGLFVBQVAsQ0FBa0IsNEJBQWxCLEVBQWdEQyxPQUEvRDtBQUNILEtBSEQsQ0FHRSxPQUFPNUQsQ0FBUCxFQUFVLENBQUc7O0FBQ2YsU0FBS3NELGlCQUFMLENBQXVCLGVBQXZCLEVBQXdDSSxZQUF4Qzs7QUFFQSxRQUFJRyxVQUFVLEdBQUcsU0FBakI7O0FBQ0EsUUFBSTtBQUNBO0FBQ0FBLE1BQUFBLFVBQVUsR0FBR3hGLE1BQU0sQ0FBQ3NGLFVBQVAsQ0FBa0IsbUJBQWxCLEVBQXVDQyxPQUFwRDtBQUNILEtBSEQsQ0FHRSxPQUFPNUQsQ0FBUCxFQUFVLENBQUc7O0FBQ2YsU0FBS3NELGlCQUFMLENBQXVCLGFBQXZCLEVBQXNDTyxVQUF0QyxFQTlDVyxDQWdEWDs7O0FBQ0EsU0FBS3pCLG9CQUFMLEdBQTRCL0QsTUFBTSxDQUFDeUYsV0FBUCxDQUFtQixLQUFLQyxJQUFMLENBQVVDLElBQVYsQ0FBZSxJQUFmLENBQW5CLEVBQXlDOUQsa0JBQXpDLENBQTVCO0FBQ0g7QUFFRDs7Ozs7QUFHQStELEVBQUFBLE9BQU8sR0FBRztBQUNOLFFBQUksS0FBSzNELFFBQVQsRUFBbUI7QUFDbkIsU0FBSzRELFVBQUwsQ0FBZ0IsV0FBaEIsRUFBNkIsU0FBN0I7QUFDQTdGLElBQUFBLE1BQU0sQ0FBQzhGLGFBQVAsQ0FBcUIsS0FBSy9CLG9CQUExQjtBQUNBLFNBQUtILE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS3hCLGNBQUwsR0FBc0IsRUFBdEI7QUFDQWxCLElBQUFBLFlBQVksQ0FBQzZFLFVBQWIsQ0FBd0JuRixPQUF4QjtBQUNBTSxJQUFBQSxZQUFZLENBQUM2RSxVQUFiLENBQXdCbEYsZUFBeEI7QUFDQUssSUFBQUEsWUFBWSxDQUFDNkUsVUFBYixDQUF3QmpGLGVBQXhCO0FBQ0FJLElBQUFBLFlBQVksQ0FBQzZFLFVBQWIsQ0FBd0JoRixpQkFBeEI7QUFDSDs7QUFFRCxRQUFNaUYsTUFBTixDQUFhL0UsSUFBYixFQUFtQjtBQUNmLFFBQUksS0FBS2dCLFFBQVQsRUFBbUI7QUFFbkIsVUFBTWdFLEdBQUcsR0FBRyxJQUFJaEMsSUFBSixFQUFaOztBQUNBLFVBQU1pQyxNQUFNLHFCQUNMakYsSUFESztBQUVSMEQsTUFBQUEsR0FBRyxFQUFFN0UsY0FBYyxFQUZYO0FBSVJxRyxNQUFBQSxLQUFLLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlLEtBQUtqRSxjQUFwQixDQUpDO0FBSW9DO0FBQzVDa0UsTUFBQUEsR0FBRyxZQUFLdEcsTUFBTSxDQUFDeUMsTUFBUCxDQUFjQyxLQUFuQixjQUE0QjFDLE1BQU0sQ0FBQ3lDLE1BQVAsQ0FBY0UsTUFBMUMsQ0FMSztBQUsrQztBQUN2RDRELE1BQUFBLElBQUksRUFBRUMsTUFBTSxDQUFDakYsSUFBSSxDQUFDQyxNQUFMLEVBQUQsQ0FBTixDQUFzQmlGLEtBQXRCLENBQTRCLENBQTVCLEVBQStCLENBQS9CLENBTkU7QUFNaUM7QUFDekNDLE1BQUFBLENBQUMsRUFBRVQsR0FBRyxDQUFDVSxRQUFKLEVBUEs7QUFRUkMsTUFBQUEsQ0FBQyxFQUFFWCxHQUFHLENBQUNZLFVBQUosRUFSSztBQVNSQyxNQUFBQSxDQUFDLEVBQUViLEdBQUcsQ0FBQ2MsVUFBSjtBQVRLLE1BQVo7O0FBWUEsVUFBTXBDLEdBQUcsR0FBRyxJQUFJQyxHQUFKLENBQVEsS0FBS2hCLE9BQWIsQ0FBWjs7QUFDQSxTQUFLLE1BQU1vRCxHQUFYLElBQWtCZCxNQUFsQixFQUEwQjtBQUN0QnZCLE1BQUFBLEdBQUcsQ0FBQ0UsWUFBSixDQUFpQkMsR0FBakIsQ0FBcUJrQyxHQUFyQixFQUEwQmQsTUFBTSxDQUFDYyxHQUFELENBQWhDO0FBQ0g7O0FBRUQsUUFBSTtBQUNBLFlBQU1oSCxNQUFNLENBQUNpSCxLQUFQLENBQWF0QyxHQUFiLEVBQWtCO0FBQ3BCdUMsUUFBQUEsTUFBTSxFQUFFLEtBRFk7QUFFcEJDLFFBQUFBLElBQUksRUFBRSxTQUZjO0FBR3BCQyxRQUFBQSxLQUFLLEVBQUUsVUFIYTtBQUlwQkMsUUFBQUEsUUFBUSxFQUFFO0FBSlUsT0FBbEIsQ0FBTjtBQU1ILEtBUEQsQ0FPRSxPQUFPMUYsQ0FBUCxFQUFVO0FBQ1JqQyxNQUFBQSxPQUFPLENBQUNrQyxLQUFSLENBQWMsbUJBQWQsRUFBbUNELENBQW5DO0FBQ0g7QUFDSjs7QUFFRCtELEVBQUFBLElBQUksR0FBRztBQUNILFNBQUtNLE1BQUwsQ0FBWTtBQUNSTixNQUFBQSxJQUFJLEVBQUU7QUFERSxLQUFaOztBQUdBeEUsSUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCTCxpQkFBckIsRUFBd0MsSUFBSWtELElBQUosR0FBV0MsT0FBWCxFQUF4QyxFQUpHLENBSTREO0FBQ2xFOztBQUVEb0QsRUFBQUEsZUFBZSxDQUFDQyxnQkFBRCxFQUFtQjtBQUM5QixRQUFJLEtBQUt0RixRQUFULEVBQW1COztBQUNuQixRQUFJLEtBQUs2QixTQUFULEVBQW9CO0FBQ2hCO0FBQ0E7QUFDQSxXQUFLQSxTQUFMLEdBQWlCLEtBQWpCO0FBQ0E7QUFDSDs7QUFFRCxRQUFJLE9BQU95RCxnQkFBUCxLQUE0QixRQUFoQyxFQUEwQztBQUN0QzdILE1BQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLHFFQUFiLEVBRHNDLENBRXRDO0FBQ0g7O0FBRUQsU0FBS3FHLE1BQUwsQ0FBWTtBQUNSd0IsTUFBQUEsS0FBSyxFQUFFRDtBQURDLEtBQVo7QUFHSDs7QUFFRDFCLEVBQUFBLFVBQVUsQ0FBQzRCLFFBQUQsRUFBV0MsTUFBWCxFQUFtQkMsSUFBbkIsRUFBeUI5RSxLQUF6QixFQUFnQztBQUN0QyxRQUFJLEtBQUtaLFFBQVQsRUFBbUI7O0FBQ25CLFNBQUsrRCxNQUFMLENBQVk7QUFDUjRCLE1BQUFBLEdBQUcsRUFBRUgsUUFERztBQUVSSSxNQUFBQSxHQUFHLEVBQUVILE1BRkc7QUFHUkksTUFBQUEsR0FBRyxFQUFFSCxJQUhHO0FBSVJJLE1BQUFBLEdBQUcsRUFBRWxGO0FBSkcsS0FBWjtBQU1IOztBQUVEb0MsRUFBQUEsaUJBQWlCLENBQUMrQixHQUFELEVBQU1uRSxLQUFOLEVBQWE7QUFDMUIsUUFBSSxLQUFLWixRQUFULEVBQW1CO0FBQ25CLFNBQUtHLGNBQUwsQ0FBb0JoQyxlQUFlLENBQUM0RyxHQUFELENBQWYsQ0FBcUIzRyxFQUF6QyxJQUErQyxDQUFDMkcsR0FBRCxFQUFNbkUsS0FBTixDQUEvQztBQUNIOztBQUVEbUYsRUFBQUEsV0FBVyxDQUFDQyxPQUFELEVBQVVDLGFBQVYsRUFBeUJDLGlCQUF6QixFQUE0QztBQUNuRCxRQUFJLEtBQUtsRyxRQUFULEVBQW1COztBQUVuQixVQUFNc0MsTUFBTSxHQUFHQyxtQkFBVUMsR0FBVixFQUFmOztBQUNBLFFBQUksQ0FBQ0YsTUFBTSxDQUFDRyxLQUFaLEVBQW1CO0FBRW5CLFVBQU0wRCxpQkFBaUIsR0FBRzdELE1BQU0sQ0FBQ0csS0FBUCxDQUFhMEQsaUJBQWIsSUFBa0MsRUFBNUQ7O0FBRUEsU0FBS25ELGlCQUFMLENBQXVCLFdBQXZCLEVBQW9DZ0QsT0FBTyxHQUFHLE9BQUgsR0FBYSxXQUF4RDs7QUFDQSxTQUFLaEQsaUJBQUwsQ0FBdUIsZ0JBQXZCLEVBQXlDekUsZUFBZSxDQUFDNEgsaUJBQUQsRUFBb0JGLGFBQXBCLENBQXhEO0FBQ0g7O0FBRURHLEVBQUFBLGNBQWMsQ0FBQ0MsS0FBRCxFQUFRO0FBQ2xCLFFBQUksS0FBS3JHLFFBQVQsRUFBbUI7O0FBQ25CLFNBQUtnRCxpQkFBTCxDQUF1QixhQUF2QixFQUFzQ3FELEtBQUssR0FBRyxTQUFILEdBQWUsVUFBMUQ7QUFDSDs7QUF2TFc7O0FBa1BoQixJQUFJLENBQUNDLE1BQU0sQ0FBQ0MsV0FBWixFQUF5QjtBQUNyQkQsRUFBQUEsTUFBTSxDQUFDQyxXQUFQLEdBQXFCLElBQUkxRyxTQUFKLEVBQXJCO0FBQ0g7O2VBQ2N5RyxNQUFNLENBQUNDLFciLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBNaWNoYWVsIFRlbGF0eW5za2kgPDd0M2NoZ3V5QGdtYWlsLmNvbT5cclxuQ29weXJpZ2h0IDIwMjAgVGhlIE1hdHJpeC5vcmcgRm91bmRhdGlvbiBDLkkuQy5cclxuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbnlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcblxyXG4gICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5Vbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbmRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcblNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxubGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiovXHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuaW1wb3J0IHsgZ2V0Q3VycmVudExhbmd1YWdlLCBfdCwgX3RkIH0gZnJvbSAnLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgUGxhdGZvcm1QZWcgZnJvbSAnLi9QbGF0Zm9ybVBlZyc7XHJcbmltcG9ydCBTZGtDb25maWcgZnJvbSAnLi9TZGtDb25maWcnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi9Nb2RhbCc7XHJcbmltcG9ydCAqIGFzIHNkayBmcm9tICcuL2luZGV4JztcclxuXHJcbmNvbnN0IGhhc2hSZWdleCA9IC8jXFwvKGdyb3Vwcz98cm9vbXx1c2VyfHNldHRpbmdzfHJlZ2lzdGVyfGxvZ2lufGZvcmdvdF9wYXNzd29yZHxob21lfGRpcmVjdG9yeSkvO1xyXG5jb25zdCBoYXNoVmFyUmVnZXggPSAvI1xcLyhncm91cHxyb29tfHVzZXIpXFwvLiokLztcclxuXHJcbi8vIFJlbW92ZSBhbGwgYnV0IHRoZSBmaXJzdCBpdGVtIGluIHRoZSBoYXNoIHBhdGguIFJlZGFjdCB1bmV4cGVjdGVkIGhhc2hlcy5cclxuZnVuY3Rpb24gZ2V0UmVkYWN0ZWRIYXNoKGhhc2gpIHtcclxuICAgIC8vIERvbid0IGxlYWsgVVJMcyB3ZSBhcmVuJ3QgZXhwZWN0aW5nIC0gdGhleSBjb3VsZCBjb250YWluIHRva2Vucy9QSUlcclxuICAgIGNvbnN0IG1hdGNoID0gaGFzaFJlZ2V4LmV4ZWMoaGFzaCk7XHJcbiAgICBpZiAoIW1hdGNoKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKGBVbmV4cGVjdGVkIGhhc2ggbG9jYXRpb24gXCIke2hhc2h9XCJgKTtcclxuICAgICAgICByZXR1cm4gJyMvPHVuZXhwZWN0ZWQgaGFzaCBsb2NhdGlvbj4nO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChoYXNoVmFyUmVnZXgudGVzdChoYXNoKSkge1xyXG4gICAgICAgIHJldHVybiBoYXNoLnJlcGxhY2UoaGFzaFZhclJlZ2V4LCBcIiMvJDEvPHJlZGFjdGVkPlwiKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gaGFzaC5yZXBsYWNlKGhhc2hSZWdleCwgXCIjLyQxXCIpO1xyXG59XHJcblxyXG4vLyBSZXR1cm4gdGhlIGN1cnJlbnQgb3JpZ2luLCBwYXRoIGFuZCBoYXNoIHNlcGFyYXRlZCB3aXRoIGEgYC9gLiBUaGlzIGRvZXNcclxuLy8gbm90IGluY2x1ZGUgcXVlcnkgcGFyYW1ldGVycy5cclxuZnVuY3Rpb24gZ2V0UmVkYWN0ZWRVcmwoKSB7XHJcbiAgICBjb25zdCB7IG9yaWdpbiwgaGFzaCB9ID0gd2luZG93LmxvY2F0aW9uO1xyXG4gICAgbGV0IHsgcGF0aG5hbWUgfSA9IHdpbmRvdy5sb2NhdGlvbjtcclxuXHJcbiAgICAvLyBSZWRhY3QgcGF0aHMgd2hpY2ggY291bGQgY29udGFpbiB1bmV4cGVjdGVkIFBJSVxyXG4gICAgaWYgKG9yaWdpbi5zdGFydHNXaXRoKCdmaWxlOi8vJykpIHtcclxuICAgICAgICBwYXRobmFtZSA9IFwiLzxyZWRhY3RlZD4vXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG9yaWdpbiArIHBhdGhuYW1lICsgZ2V0UmVkYWN0ZWRIYXNoKGhhc2gpO1xyXG59XHJcblxyXG5jb25zdCBjdXN0b21WYXJpYWJsZXMgPSB7XHJcbiAgICAvLyBUaGUgTWF0b21vIGluc3RhbGxhdGlvbiBhdCBodHRwczovL21hdG9tby5yaW90LmltIGlzIGN1cnJlbnRseSBjb25maWd1cmVkXHJcbiAgICAvLyB3aXRoIGEgbGltaXQgb2YgMTAgY3VzdG9tIHZhcmlhYmxlcy5cclxuICAgICdBcHAgUGxhdGZvcm0nOiB7XHJcbiAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgZXhwbDogX3RkKCdUaGUgcGxhdGZvcm0geW91XFwncmUgb24nKSxcclxuICAgICAgICBleGFtcGxlOiAnRWxlY3Ryb24gUGxhdGZvcm0nLFxyXG4gICAgfSxcclxuICAgICdBcHAgVmVyc2lvbic6IHtcclxuICAgICAgICBpZDogMixcclxuICAgICAgICBleHBsOiBfdGQoJ1RoZSB2ZXJzaW9uIG9mIFJpb3QnKSxcclxuICAgICAgICBleGFtcGxlOiAnMTUuMC4wJyxcclxuICAgIH0sXHJcbiAgICAnVXNlciBUeXBlJzoge1xyXG4gICAgICAgIGlkOiAzLFxyXG4gICAgICAgIGV4cGw6IF90ZCgnV2hldGhlciBvciBub3QgeW91XFwncmUgbG9nZ2VkIGluICh3ZSBkb25cXCd0IHJlY29yZCB5b3VyIHVzZXJuYW1lKScpLFxyXG4gICAgICAgIGV4YW1wbGU6ICdMb2dnZWQgSW4nLFxyXG4gICAgfSxcclxuICAgICdDaG9zZW4gTGFuZ3VhZ2UnOiB7XHJcbiAgICAgICAgaWQ6IDQsXHJcbiAgICAgICAgZXhwbDogX3RkKCdZb3VyIGxhbmd1YWdlIG9mIGNob2ljZScpLFxyXG4gICAgICAgIGV4YW1wbGU6ICdlbicsXHJcbiAgICB9LFxyXG4gICAgJ0luc3RhbmNlJzoge1xyXG4gICAgICAgIGlkOiA1LFxyXG4gICAgICAgIGV4cGw6IF90ZCgnV2hpY2ggb2ZmaWNpYWxseSBwcm92aWRlZCBpbnN0YW5jZSB5b3UgYXJlIHVzaW5nLCBpZiBhbnknKSxcclxuICAgICAgICBleGFtcGxlOiAnYXBwJyxcclxuICAgIH0sXHJcbiAgICAnUlRFOiBVc2VzIFJpY2h0ZXh0IE1vZGUnOiB7XHJcbiAgICAgICAgaWQ6IDYsXHJcbiAgICAgICAgZXhwbDogX3RkKCdXaGV0aGVyIG9yIG5vdCB5b3VcXCdyZSB1c2luZyB0aGUgUmljaHRleHQgbW9kZSBvZiB0aGUgUmljaCBUZXh0IEVkaXRvcicpLFxyXG4gICAgICAgIGV4YW1wbGU6ICdvZmYnLFxyXG4gICAgfSxcclxuICAgICdIb21lc2VydmVyIFVSTCc6IHtcclxuICAgICAgICBpZDogNyxcclxuICAgICAgICBleHBsOiBfdGQoJ1lvdXIgaG9tZXNlcnZlclxcJ3MgVVJMJyksXHJcbiAgICAgICAgZXhhbXBsZTogJ2h0dHBzOi8vbWF0cml4Lm9yZycsXHJcbiAgICB9LFxyXG4gICAgJ1RvdWNoIElucHV0Jzoge1xyXG4gICAgICAgIGlkOiA4LFxyXG4gICAgICAgIGV4cGw6IF90ZChcIldoZXRoZXIgeW91J3JlIHVzaW5nIFJpb3Qgb24gYSBkZXZpY2Ugd2hlcmUgdG91Y2ggaXMgdGhlIHByaW1hcnkgaW5wdXQgbWVjaGFuaXNtXCIpLFxyXG4gICAgICAgIGV4YW1wbGU6ICdmYWxzZScsXHJcbiAgICB9LFxyXG4gICAgJ0JyZWFkY3J1bWJzJzoge1xyXG4gICAgICAgIGlkOiA5LFxyXG4gICAgICAgIGV4cGw6IF90ZChcIldoZXRoZXIgb3Igbm90IHlvdSdyZSB1c2luZyB0aGUgJ2JyZWFkY3J1bWJzJyBmZWF0dXJlIChhdmF0YXJzIGFib3ZlIHRoZSByb29tIGxpc3QpXCIpLFxyXG4gICAgICAgIGV4YW1wbGU6ICdkaXNhYmxlZCcsXHJcbiAgICB9LFxyXG4gICAgJ0luc3RhbGxlZCBQV0EnOiB7XHJcbiAgICAgICAgaWQ6IDEwLFxyXG4gICAgICAgIGV4cGw6IF90ZChcIldoZXRoZXIgeW91J3JlIHVzaW5nIFJpb3QgYXMgYW4gaW5zdGFsbGVkIFByb2dyZXNzaXZlIFdlYiBBcHBcIiksXHJcbiAgICAgICAgZXhhbXBsZTogJ2ZhbHNlJyxcclxuICAgIH0sXHJcbn07XHJcblxyXG5mdW5jdGlvbiB3aGl0ZWxpc3RSZWRhY3Qod2hpdGVsaXN0LCBzdHIpIHtcclxuICAgIGlmICh3aGl0ZWxpc3QuaW5jbHVkZXMoc3RyKSkgcmV0dXJuIHN0cjtcclxuICAgIHJldHVybiAnPHJlZGFjdGVkPic7XHJcbn1cclxuXHJcbmNvbnN0IFVJRF9LRVkgPSBcIm14X1Jpb3RfQW5hbHl0aWNzX3VpZFwiO1xyXG5jb25zdCBDUkVBVElPTl9UU19LRVkgPSBcIm14X1Jpb3RfQW5hbHl0aWNzX2N0c1wiO1xyXG5jb25zdCBWSVNJVF9DT1VOVF9LRVkgPSBcIm14X1Jpb3RfQW5hbHl0aWNzX3ZjXCI7XHJcbmNvbnN0IExBU1RfVklTSVRfVFNfS0VZID0gXCJteF9SaW90X0FuYWx5dGljc19sdnRzXCI7XHJcblxyXG5mdW5jdGlvbiBnZXRVaWQoKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIGxldCBkYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oVUlEX0tFWSk7XHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFVJRF9LRVksIGRhdGEgPSBbLi4uQXJyYXkoMTYpXS5tYXAoKCkgPT4gTWF0aC5yYW5kb20oKS50b1N0cmluZygxNilbMl0pLmpvaW4oJycpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihcIkFuYWx5dGljcyBlcnJvcjogXCIsIGUpO1xyXG4gICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBIRUFSVEJFQVRfSU5URVJWQUwgPSAzMCAqIDEwMDA7IC8vIHNlY29uZHNcclxuXHJcbmNsYXNzIEFuYWx5dGljcyB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLmJhc2VVcmwgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuc2l0ZUlkID0gbnVsbDtcclxuICAgICAgICB0aGlzLnZpc2l0VmFyaWFibGVzID0ge307XHJcblxyXG4gICAgICAgIHRoaXMuZmlyc3RQYWdlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9oZWFydGJlYXRJbnRlcnZhbElEID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy5jcmVhdGlvblRzID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oQ1JFQVRJT05fVFNfS0VZKTtcclxuICAgICAgICBpZiAoIXRoaXMuY3JlYXRpb25Ucykge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShDUkVBVElPTl9UU19LRVksIHRoaXMuY3JlYXRpb25UcyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubGFzdFZpc2l0VHMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShMQVNUX1ZJU0lUX1RTX0tFWSk7XHJcbiAgICAgICAgdGhpcy52aXNpdENvdW50ID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oVklTSVRfQ09VTlRfS0VZKSB8fCAwO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFZJU0lUX0NPVU5UX0tFWSwgcGFyc2VJbnQodGhpcy52aXNpdENvdW50LCAxMCkgKyAxKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzYWJsZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLmJhc2VVcmw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBFbmFibGUgQW5hbHl0aWNzIGlmIGluaXRpYWxpemVkIGJ1dCBkaXNhYmxlZFxyXG4gICAgICogb3RoZXJ3aXNlIHRyeSBhbmQgaW5pdGFsaXplLCBuby1vcCBpZiBwaXdpayBjb25maWcgbWlzc2luZ1xyXG4gICAgICovXHJcbiAgICBhc3luYyBlbmFibGUoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmRpc2FibGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbmZpZyA9IFNka0NvbmZpZy5nZXQoKTtcclxuICAgICAgICBpZiAoIWNvbmZpZyB8fCAhY29uZmlnLnBpd2lrIHx8ICFjb25maWcucGl3aWsudXJsIHx8ICFjb25maWcucGl3aWsuc2l0ZUlkKSByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuYmFzZVVybCA9IG5ldyBVUkwoXCJwaXdpay5waHBcIiwgY29uZmlnLnBpd2lrLnVybCk7XHJcbiAgICAgICAgLy8gc2V0IGNvbnN0YW50c1xyXG4gICAgICAgIHRoaXMuYmFzZVVybC5zZWFyY2hQYXJhbXMuc2V0KFwicmVjXCIsIDEpOyAvLyByZWMgaXMgcmVxdWlyZWQgZm9yIHRyYWNraW5nXHJcbiAgICAgICAgdGhpcy5iYXNlVXJsLnNlYXJjaFBhcmFtcy5zZXQoXCJpZHNpdGVcIiwgY29uZmlnLnBpd2lrLnNpdGVJZCk7IC8vIHJlYyBpcyByZXF1aXJlZCBmb3IgdHJhY2tpbmdcclxuICAgICAgICB0aGlzLmJhc2VVcmwuc2VhcmNoUGFyYW1zLnNldChcImFwaXZcIiwgMSk7IC8vIEFQSSB2ZXJzaW9uIHRvIHVzZVxyXG4gICAgICAgIHRoaXMuYmFzZVVybC5zZWFyY2hQYXJhbXMuc2V0KFwic2VuZF9pbWFnZVwiLCAwKTsgLy8gd2Ugd2FudCBhIDIwNCwgbm90IGEgdGlueSBHSUZcclxuICAgICAgICAvLyBzZXQgdXNlciBwYXJhbWV0ZXJzXHJcbiAgICAgICAgdGhpcy5iYXNlVXJsLnNlYXJjaFBhcmFtcy5zZXQoXCJfaWRcIiwgZ2V0VWlkKCkpOyAvLyB1dWlkXHJcbiAgICAgICAgdGhpcy5iYXNlVXJsLnNlYXJjaFBhcmFtcy5zZXQoXCJfaWR0c1wiLCB0aGlzLmNyZWF0aW9uVHMpOyAvLyBmaXJzdCB0c1xyXG4gICAgICAgIHRoaXMuYmFzZVVybC5zZWFyY2hQYXJhbXMuc2V0KFwiX2lkdmNcIiwgcGFyc2VJbnQodGhpcy52aXNpdENvdW50LCAxMCkrIDEpOyAvLyB2aXNpdCBjb3VudFxyXG4gICAgICAgIGlmICh0aGlzLmxhc3RWaXNpdFRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFzZVVybC5zZWFyY2hQYXJhbXMuc2V0KFwiX3ZpZXd0c1wiLCB0aGlzLmxhc3RWaXNpdFRzKTsgLy8gbGFzdCB2aXNpdCB0c1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcGxhdGZvcm0gPSBQbGF0Zm9ybVBlZy5nZXQoKTtcclxuICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdBcHAgUGxhdGZvcm0nLCBwbGF0Zm9ybS5nZXRIdW1hblJlYWRhYmxlTmFtZSgpKTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdBcHAgVmVyc2lvbicsIGF3YWl0IHBsYXRmb3JtLmdldEFwcFZlcnNpb24oKSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdBcHAgVmVyc2lvbicsICd1bmtub3duJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdDaG9zZW4gTGFuZ3VhZ2UnLCBnZXRDdXJyZW50TGFuZ3VhZ2UoKSk7XHJcblxyXG4gICAgICAgIGlmICh3aW5kb3cubG9jYXRpb24uaG9zdG5hbWUgPT09ICdyaW90LmltJykge1xyXG4gICAgICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdJbnN0YW5jZScsIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaW5zdGFsbGVkUFdBID0gXCJ1bmtub3duXCI7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgLy8gS25vd24gdG8gd29yayBhdCBsZWFzdCBmb3IgZGVza3RvcCBDaHJvbWVcclxuICAgICAgICAgICAgaW5zdGFsbGVkUFdBID0gd2luZG93Lm1hdGNoTWVkaWEoJyhkaXNwbGF5LW1vZGU6IHN0YW5kYWxvbmUpJykubWF0Y2hlcztcclxuICAgICAgICB9IGNhdGNoIChlKSB7IH1cclxuICAgICAgICB0aGlzLl9zZXRWaXNpdFZhcmlhYmxlKCdJbnN0YWxsZWQgUFdBJywgaW5zdGFsbGVkUFdBKTtcclxuXHJcbiAgICAgICAgbGV0IHRvdWNoSW5wdXQgPSBcInVua25vd25cIjtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAvLyBNRE4gY2xhaW1zIGJyb2FkIHN1cHBvcnQgYWNyb3NzIGJyb3dzZXJzXHJcbiAgICAgICAgICAgIHRvdWNoSW5wdXQgPSB3aW5kb3cubWF0Y2hNZWRpYSgnKHBvaW50ZXI6IGNvYXJzZSknKS5tYXRjaGVzO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgfVxyXG4gICAgICAgIHRoaXMuX3NldFZpc2l0VmFyaWFibGUoJ1RvdWNoIElucHV0JywgdG91Y2hJbnB1dCk7XHJcblxyXG4gICAgICAgIC8vIHN0YXJ0IGhlYXJ0YmVhdFxyXG4gICAgICAgIHRoaXMuX2hlYXJ0YmVhdEludGVydmFsSUQgPSB3aW5kb3cuc2V0SW50ZXJ2YWwodGhpcy5waW5nLmJpbmQodGhpcyksIEhFQVJUQkVBVF9JTlRFUlZBTCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNhYmxlIEFuYWx5dGljcywgc3RvcCB0aGUgaGVhcnRiZWF0IGFuZCBjbGVhciBpZGVudGlmaWVycyBmcm9tIGxvY2FsU3RvcmFnZVxyXG4gICAgICovXHJcbiAgICBkaXNhYmxlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRpc2FibGVkKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy50cmFja0V2ZW50KCdBbmFseXRpY3MnLCAnb3B0LW91dCcpO1xyXG4gICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMuX2hlYXJ0YmVhdEludGVydmFsSUQpO1xyXG4gICAgICAgIHRoaXMuYmFzZVVybCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy52aXNpdFZhcmlhYmxlcyA9IHt9O1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFVJRF9LRVkpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKENSRUFUSU9OX1RTX0tFWSk7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oVklTSVRfQ09VTlRfS0VZKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShMQVNUX1ZJU0lUX1RTX0tFWSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgX3RyYWNrKGRhdGEpIHtcclxuICAgICAgICBpZiAodGhpcy5kaXNhYmxlZCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCBub3cgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgLi4uZGF0YSxcclxuICAgICAgICAgICAgdXJsOiBnZXRSZWRhY3RlZFVybCgpLFxyXG5cclxuICAgICAgICAgICAgX2N2YXI6IEpTT04uc3RyaW5naWZ5KHRoaXMudmlzaXRWYXJpYWJsZXMpLCAvLyB1c2VyIGN1c3RvbSB2YXJzXHJcbiAgICAgICAgICAgIHJlczogYCR7d2luZG93LnNjcmVlbi53aWR0aH14JHt3aW5kb3cuc2NyZWVuLmhlaWdodH1gLCAvLyByZXNvbHV0aW9uIGFzIFdXV1d4SEhISFxyXG4gICAgICAgICAgICByYW5kOiBTdHJpbmcoTWF0aC5yYW5kb20oKSkuc2xpY2UoMiwgOCksIC8vIHJhbmRvbSBub25jZSB0byBjYWNoZS1idXN0XHJcbiAgICAgICAgICAgIGg6IG5vdy5nZXRIb3VycygpLFxyXG4gICAgICAgICAgICBtOiBub3cuZ2V0TWludXRlcygpLFxyXG4gICAgICAgICAgICBzOiBub3cuZ2V0U2Vjb25kcygpLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHVybCA9IG5ldyBVUkwodGhpcy5iYXNlVXJsKTtcclxuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBwYXJhbXMpIHtcclxuICAgICAgICAgICAgdXJsLnNlYXJjaFBhcmFtcy5zZXQoa2V5LCBwYXJhbXNba2V5XSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBhd2FpdCB3aW5kb3cuZmV0Y2godXJsLCB7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgICAgICAgICBtb2RlOiBcIm5vLWNvcnNcIixcclxuICAgICAgICAgICAgICAgIGNhY2hlOiBcIm5vLWNhY2hlXCIsXHJcbiAgICAgICAgICAgICAgICByZWRpcmVjdDogXCJmb2xsb3dcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiQW5hbHl0aWNzIGVycm9yOiBcIiwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBpbmcoKSB7XHJcbiAgICAgICAgdGhpcy5fdHJhY2soe1xyXG4gICAgICAgICAgICBwaW5nOiAxLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKExBU1RfVklTSVRfVFNfS0VZLCBuZXcgRGF0ZSgpLmdldFRpbWUoKSk7IC8vIHVwZGF0ZSBsYXN0IHZpc2l0IHRzXHJcbiAgICB9XHJcblxyXG4gICAgdHJhY2tQYWdlQ2hhbmdlKGdlbmVyYXRpb25UaW1lTXMpIHtcclxuICAgICAgICBpZiAodGhpcy5kaXNhYmxlZCkgcmV0dXJuO1xyXG4gICAgICAgIGlmICh0aGlzLmZpcnN0UGFnZSkge1xyXG4gICAgICAgICAgICAvLyBEZS1kdXBsaWNhdGUgZmlyc3QgcGFnZVxyXG4gICAgICAgICAgICAvLyByb3V0ZXIgc2VlbXMgdG8gaGl0IHRoZSBmbiB0d2ljZVxyXG4gICAgICAgICAgICB0aGlzLmZpcnN0UGFnZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGdlbmVyYXRpb25UaW1lTXMgIT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignQW5hbHl0aWNzLnRyYWNrUGFnZUNoYW5nZTogZXhwZWN0ZWQgZ2VuZXJhdGlvblRpbWVNcyB0byBiZSBhIG51bWJlcicpO1xyXG4gICAgICAgICAgICAvLyBCdXQgY29udGludWUgYW55d2F5IGJlY2F1c2Ugd2Ugc3RpbGwgd2FudCB0byB0cmFjayB0aGUgY2hhbmdlXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl90cmFjayh7XHJcbiAgICAgICAgICAgIGd0X21zOiBnZW5lcmF0aW9uVGltZU1zLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRyYWNrRXZlbnQoY2F0ZWdvcnksIGFjdGlvbiwgbmFtZSwgdmFsdWUpIHtcclxuICAgICAgICBpZiAodGhpcy5kaXNhYmxlZCkgcmV0dXJuO1xyXG4gICAgICAgIHRoaXMuX3RyYWNrKHtcclxuICAgICAgICAgICAgZV9jOiBjYXRlZ29yeSxcclxuICAgICAgICAgICAgZV9hOiBhY3Rpb24sXHJcbiAgICAgICAgICAgIGVfbjogbmFtZSxcclxuICAgICAgICAgICAgZV92OiB2YWx1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfc2V0VmlzaXRWYXJpYWJsZShrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGlzYWJsZWQpIHJldHVybjtcclxuICAgICAgICB0aGlzLnZpc2l0VmFyaWFibGVzW2N1c3RvbVZhcmlhYmxlc1trZXldLmlkXSA9IFtrZXksIHZhbHVlXTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRMb2dnZWRJbihpc0d1ZXN0LCBob21lc2VydmVyVXJsLCBpZGVudGl0eVNlcnZlclVybCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRpc2FibGVkKSByZXR1cm47XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbmZpZyA9IFNka0NvbmZpZy5nZXQoKTtcclxuICAgICAgICBpZiAoIWNvbmZpZy5waXdpaykgcmV0dXJuO1xyXG5cclxuICAgICAgICBjb25zdCB3aGl0ZWxpc3RlZEhTVXJscyA9IGNvbmZpZy5waXdpay53aGl0ZWxpc3RlZEhTVXJscyB8fCBbXTtcclxuXHJcbiAgICAgICAgdGhpcy5fc2V0VmlzaXRWYXJpYWJsZSgnVXNlciBUeXBlJywgaXNHdWVzdCA/ICdHdWVzdCcgOiAnTG9nZ2VkIEluJyk7XHJcbiAgICAgICAgdGhpcy5fc2V0VmlzaXRWYXJpYWJsZSgnSG9tZXNlcnZlciBVUkwnLCB3aGl0ZWxpc3RSZWRhY3Qod2hpdGVsaXN0ZWRIU1VybHMsIGhvbWVzZXJ2ZXJVcmwpKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRCcmVhZGNydW1icyhzdGF0ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmRpc2FibGVkKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5fc2V0VmlzaXRWYXJpYWJsZSgnQnJlYWRjcnVtYnMnLCBzdGF0ZSA/ICdlbmFibGVkJyA6ICdkaXNhYmxlZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dEZXRhaWxzTW9kYWwgPSAoKSA9PiB7XHJcbiAgICAgICAgbGV0IHJvd3MgPSBbXTtcclxuICAgICAgICBpZiAoIXRoaXMuZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgcm93cyA9IE9iamVjdC52YWx1ZXModGhpcy52aXNpdFZhcmlhYmxlcyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcm93cyA9IE9iamVjdC5rZXlzKGN1c3RvbVZhcmlhYmxlcykubWFwKFxyXG4gICAgICAgICAgICAgICAgKGspID0+IFtcclxuICAgICAgICAgICAgICAgICAgICBrLFxyXG4gICAgICAgICAgICAgICAgICAgIF90KCdlLmcuICUoZXhhbXBsZVZhbHVlKXMnLCB7IGV4YW1wbGVWYWx1ZTogY3VzdG9tVmFyaWFibGVzW2tdLmV4YW1wbGUgfSksXHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcmVzb2x1dGlvbiA9IGAke3dpbmRvdy5zY3JlZW4ud2lkdGh9eCR7d2luZG93LnNjcmVlbi5oZWlnaHR9YDtcclxuICAgICAgICBjb25zdCBvdGhlclZhcmlhYmxlcyA9IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZXhwbDogX3RkKCdFdmVyeSBwYWdlIHlvdSB1c2UgaW4gdGhlIGFwcCcpLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IF90KFxyXG4gICAgICAgICAgICAgICAgICAgICdlLmcuIDxDdXJyZW50UGFnZVVSTD4nLFxyXG4gICAgICAgICAgICAgICAgICAgIHt9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgQ3VycmVudFBhZ2VVUkw6IGdldFJlZGFjdGVkVXJsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHsgZXhwbDogX3RkKCdZb3VyIHVzZXIgYWdlbnQnKSwgdmFsdWU6IG5hdmlnYXRvci51c2VyQWdlbnQgfSxcclxuICAgICAgICAgICAgeyBleHBsOiBfdGQoJ1lvdXIgZGV2aWNlIHJlc29sdXRpb24nKSwgdmFsdWU6IHJlc29sdXRpb24gfSxcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoJ2RpYWxvZ3MuRXJyb3JEaWFsb2cnKTtcclxuICAgICAgICBNb2RhbC5jcmVhdGVUcmFja2VkRGlhbG9nKCdBbmFseXRpY3MgRGV0YWlscycsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICB0aXRsZTogX3QoJ0FuYWx5dGljcycpLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogPGRpdiBjbGFzc05hbWU9XCJteF9BbmFseXRpY3NNb2RhbFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdUaGUgaW5mb3JtYXRpb24gYmVpbmcgc2VudCB0byB1cyB0byBoZWxwIG1ha2UgUmlvdCBiZXR0ZXIgaW5jbHVkZXM6JykgfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8dGFibGU+XHJcbiAgICAgICAgICAgICAgICAgICAgeyByb3dzLm1hcCgocm93KSA9PiA8dHIga2V5PXtyb3dbMF19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dGQ+eyBfdChjdXN0b21WYXJpYWJsZXNbcm93WzBdXS5leHBsKSB9PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgeyByb3dbMV0gIT09IHVuZGVmaW5lZCAmJiA8dGQ+PGNvZGU+eyByb3dbMV0gfTwvY29kZT48L3RkPiB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC90cj4pIH1cclxuICAgICAgICAgICAgICAgICAgICB7IG90aGVyVmFyaWFibGVzLm1hcCgoaXRlbSwgaW5kZXgpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ciBrZXk9e2luZGV4fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57IF90KGl0ZW0uZXhwbCkgfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PGNvZGU+eyBpdGVtLnZhbHVlIH08L2NvZGU+PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC90cj4sXHJcbiAgICAgICAgICAgICAgICAgICAgKSB9XHJcbiAgICAgICAgICAgICAgICA8L3RhYmxlPlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7IF90KCdXaGVyZSB0aGlzIHBhZ2UgaW5jbHVkZXMgaWRlbnRpZmlhYmxlIGluZm9ybWF0aW9uLCBzdWNoIGFzIGEgcm9vbSwgJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICArICd1c2VyIG9yIGdyb3VwIElELCB0aGF0IGRhdGEgaXMgcmVtb3ZlZCBiZWZvcmUgYmVpbmcgc2VudCB0byB0aGUgc2VydmVyLicpIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj4sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59XHJcblxyXG5pZiAoIWdsb2JhbC5teEFuYWx5dGljcykge1xyXG4gICAgZ2xvYmFsLm14QW5hbHl0aWNzID0gbmV3IEFuYWx5dGljcygpO1xyXG59XHJcbmV4cG9ydCBkZWZhdWx0IGdsb2JhbC5teEFuYWx5dGljcztcclxuIl19