"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Analytics = _interopRequireDefault(require("../Analytics"));

var _actionCreators = require("./actionCreators");

var _TagOrderStore = _interopRequireDefault(require("../stores/TagOrderStore"));

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const TagOrderActions = {};
/**
 * Creates an action thunk that will do an asynchronous request to
 * move a tag in TagOrderStore to destinationIx.
 *
 * @param {MatrixClient} matrixClient the matrix client to set the
 *                                    account data on.
 * @param {string} tag the tag to move.
 * @param {number} destinationIx the new position of the tag.
 * @returns {function} an action thunk that will dispatch actions
 *                     indicating the status of the request.
 * @see asyncAction
 */

TagOrderActions.moveTag = function (matrixClient, tag, destinationIx) {
  // Only commit tags if the state is ready, i.e. not null
  let tags = _TagOrderStore.default.getOrderedTags();

  let removedTags = _TagOrderStore.default.getRemovedTagsAccountData() || [];

  if (!tags) {
    return;
  }

  tags = tags.filter(t => t !== tag);
  tags = [...tags.slice(0, destinationIx), tag, ...tags.slice(destinationIx)];
  removedTags = removedTags.filter(t => t !== tag);

  const storeId = _TagOrderStore.default.getStoreId();

  return (0, _actionCreators.asyncAction)('TagOrderActions.moveTag', () => {
    _Analytics.default.trackEvent('TagOrderActions', 'commitTagOrdering');

    return matrixClient.setAccountData('im.vector.web.tag_ordering', {
      tags,
      removedTags,
      _storeId: storeId
    });
  }, () => {
    // For an optimistic update
    return {
      tags,
      removedTags
    };
  });
};
/**
 * Creates an action thunk that will do an asynchronous request to
 * label a tag as removed in im.vector.web.tag_ordering account data.
 *
 * The reason this is implemented with new state `removedTags` is that
 * we incrementally and initially populate `tags` with groups that
 * have been joined. If we remove a group from `tags`, it will just
 * get added (as it looks like a group we've recently joined).
 *
 * NB: If we ever support adding of tags (which is planned), we should
 * take special care to remove the tag from `removedTags` when we add
 * it.
 *
 * @param {MatrixClient} matrixClient the matrix client to set the
 *                                    account data on.
 * @param {string} tag the tag to remove.
 * @returns {function} an action thunk that will dispatch actions
 *                     indicating the status of the request.
 * @see asyncAction
 */


TagOrderActions.removeTag = function (matrixClient, tag) {
  // Don't change tags, just removedTags
  const tags = _TagOrderStore.default.getOrderedTags();

  const removedTags = _TagOrderStore.default.getRemovedTagsAccountData() || [];

  if (removedTags.includes(tag)) {
    // Return a thunk that doesn't do anything, we don't even need
    // an asynchronous action here, the tag is already removed.
    return () => {};
  }

  removedTags.push(tag);

  const storeId = _TagOrderStore.default.getStoreId();

  return (0, _actionCreators.asyncAction)('TagOrderActions.removeTag', () => {
    _Analytics.default.trackEvent('TagOrderActions', 'removeTag');

    return matrixClient.setAccountData('im.vector.web.tag_ordering', {
      tags,
      removedTags,
      _storeId: storeId
    });
  }, () => {
    // For an optimistic update
    return {
      removedTags
    };
  });
};

var _default = TagOrderActions;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hY3Rpb25zL1RhZ09yZGVyQWN0aW9ucy5qcyJdLCJuYW1lcyI6WyJUYWdPcmRlckFjdGlvbnMiLCJtb3ZlVGFnIiwibWF0cml4Q2xpZW50IiwidGFnIiwiZGVzdGluYXRpb25JeCIsInRhZ3MiLCJUYWdPcmRlclN0b3JlIiwiZ2V0T3JkZXJlZFRhZ3MiLCJyZW1vdmVkVGFncyIsImdldFJlbW92ZWRUYWdzQWNjb3VudERhdGEiLCJmaWx0ZXIiLCJ0Iiwic2xpY2UiLCJzdG9yZUlkIiwiZ2V0U3RvcmVJZCIsIkFuYWx5dGljcyIsInRyYWNrRXZlbnQiLCJzZXRBY2NvdW50RGF0YSIsIl9zdG9yZUlkIiwicmVtb3ZlVGFnIiwiaW5jbHVkZXMiLCJwdXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFnQkE7O0FBQ0E7O0FBQ0E7O0FBbEJBOzs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsTUFBTUEsZUFBZSxHQUFHLEVBQXhCO0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFZQUEsZUFBZSxDQUFDQyxPQUFoQixHQUEwQixVQUFTQyxZQUFULEVBQXVCQyxHQUF2QixFQUE0QkMsYUFBNUIsRUFBMkM7QUFDakU7QUFDQSxNQUFJQyxJQUFJLEdBQUdDLHVCQUFjQyxjQUFkLEVBQVg7O0FBQ0EsTUFBSUMsV0FBVyxHQUFHRix1QkFBY0cseUJBQWQsTUFBNkMsRUFBL0Q7O0FBQ0EsTUFBSSxDQUFDSixJQUFMLEVBQVc7QUFDUDtBQUNIOztBQUVEQSxFQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ0ssTUFBTCxDQUFhQyxDQUFELElBQU9BLENBQUMsS0FBS1IsR0FBekIsQ0FBUDtBQUNBRSxFQUFBQSxJQUFJLEdBQUcsQ0FBQyxHQUFHQSxJQUFJLENBQUNPLEtBQUwsQ0FBVyxDQUFYLEVBQWNSLGFBQWQsQ0FBSixFQUFrQ0QsR0FBbEMsRUFBdUMsR0FBR0UsSUFBSSxDQUFDTyxLQUFMLENBQVdSLGFBQVgsQ0FBMUMsQ0FBUDtBQUVBSSxFQUFBQSxXQUFXLEdBQUdBLFdBQVcsQ0FBQ0UsTUFBWixDQUFvQkMsQ0FBRCxJQUFPQSxDQUFDLEtBQUtSLEdBQWhDLENBQWQ7O0FBRUEsUUFBTVUsT0FBTyxHQUFHUCx1QkFBY1EsVUFBZCxFQUFoQjs7QUFFQSxTQUFPLGlDQUFZLHlCQUFaLEVBQXVDLE1BQU07QUFDaERDLHVCQUFVQyxVQUFWLENBQXFCLGlCQUFyQixFQUF3QyxtQkFBeEM7O0FBQ0EsV0FBT2QsWUFBWSxDQUFDZSxjQUFiLENBQ0gsNEJBREcsRUFFSDtBQUFDWixNQUFBQSxJQUFEO0FBQU9HLE1BQUFBLFdBQVA7QUFBb0JVLE1BQUFBLFFBQVEsRUFBRUw7QUFBOUIsS0FGRyxDQUFQO0FBSUgsR0FOTSxFQU1KLE1BQU07QUFDTDtBQUNBLFdBQU87QUFBQ1IsTUFBQUEsSUFBRDtBQUFPRyxNQUFBQTtBQUFQLEtBQVA7QUFDSCxHQVRNLENBQVA7QUFVSCxDQXpCRDtBQTJCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQVIsZUFBZSxDQUFDbUIsU0FBaEIsR0FBNEIsVUFBU2pCLFlBQVQsRUFBdUJDLEdBQXZCLEVBQTRCO0FBQ3BEO0FBQ0EsUUFBTUUsSUFBSSxHQUFHQyx1QkFBY0MsY0FBZCxFQUFiOztBQUNBLFFBQU1DLFdBQVcsR0FBR0YsdUJBQWNHLHlCQUFkLE1BQTZDLEVBQWpFOztBQUVBLE1BQUlELFdBQVcsQ0FBQ1ksUUFBWixDQUFxQmpCLEdBQXJCLENBQUosRUFBK0I7QUFDM0I7QUFDQTtBQUNBLFdBQU8sTUFBTSxDQUFFLENBQWY7QUFDSDs7QUFFREssRUFBQUEsV0FBVyxDQUFDYSxJQUFaLENBQWlCbEIsR0FBakI7O0FBRUEsUUFBTVUsT0FBTyxHQUFHUCx1QkFBY1EsVUFBZCxFQUFoQjs7QUFFQSxTQUFPLGlDQUFZLDJCQUFaLEVBQXlDLE1BQU07QUFDbERDLHVCQUFVQyxVQUFWLENBQXFCLGlCQUFyQixFQUF3QyxXQUF4Qzs7QUFDQSxXQUFPZCxZQUFZLENBQUNlLGNBQWIsQ0FDSCw0QkFERyxFQUVIO0FBQUNaLE1BQUFBLElBQUQ7QUFBT0csTUFBQUEsV0FBUDtBQUFvQlUsTUFBQUEsUUFBUSxFQUFFTDtBQUE5QixLQUZHLENBQVA7QUFJSCxHQU5NLEVBTUosTUFBTTtBQUNMO0FBQ0EsV0FBTztBQUFDTCxNQUFBQTtBQUFELEtBQVA7QUFDSCxHQVRNLENBQVA7QUFVSCxDQXpCRDs7ZUEyQmVSLGUiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCBBbmFseXRpY3MgZnJvbSAnLi4vQW5hbHl0aWNzJztcclxuaW1wb3J0IHsgYXN5bmNBY3Rpb24gfSBmcm9tICcuL2FjdGlvbkNyZWF0b3JzJztcclxuaW1wb3J0IFRhZ09yZGVyU3RvcmUgZnJvbSAnLi4vc3RvcmVzL1RhZ09yZGVyU3RvcmUnO1xyXG5cclxuY29uc3QgVGFnT3JkZXJBY3Rpb25zID0ge307XHJcblxyXG4vKipcclxuICogQ3JlYXRlcyBhbiBhY3Rpb24gdGh1bmsgdGhhdCB3aWxsIGRvIGFuIGFzeW5jaHJvbm91cyByZXF1ZXN0IHRvXHJcbiAqIG1vdmUgYSB0YWcgaW4gVGFnT3JkZXJTdG9yZSB0byBkZXN0aW5hdGlvbkl4LlxyXG4gKlxyXG4gKiBAcGFyYW0ge01hdHJpeENsaWVudH0gbWF0cml4Q2xpZW50IHRoZSBtYXRyaXggY2xpZW50IHRvIHNldCB0aGVcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY2NvdW50IGRhdGEgb24uXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSB0YWcgdGhlIHRhZyB0byBtb3ZlLlxyXG4gKiBAcGFyYW0ge251bWJlcn0gZGVzdGluYXRpb25JeCB0aGUgbmV3IHBvc2l0aW9uIG9mIHRoZSB0YWcuXHJcbiAqIEByZXR1cm5zIHtmdW5jdGlvbn0gYW4gYWN0aW9uIHRodW5rIHRoYXQgd2lsbCBkaXNwYXRjaCBhY3Rpb25zXHJcbiAqICAgICAgICAgICAgICAgICAgICAgaW5kaWNhdGluZyB0aGUgc3RhdHVzIG9mIHRoZSByZXF1ZXN0LlxyXG4gKiBAc2VlIGFzeW5jQWN0aW9uXHJcbiAqL1xyXG5UYWdPcmRlckFjdGlvbnMubW92ZVRhZyA9IGZ1bmN0aW9uKG1hdHJpeENsaWVudCwgdGFnLCBkZXN0aW5hdGlvbkl4KSB7XHJcbiAgICAvLyBPbmx5IGNvbW1pdCB0YWdzIGlmIHRoZSBzdGF0ZSBpcyByZWFkeSwgaS5lLiBub3QgbnVsbFxyXG4gICAgbGV0IHRhZ3MgPSBUYWdPcmRlclN0b3JlLmdldE9yZGVyZWRUYWdzKCk7XHJcbiAgICBsZXQgcmVtb3ZlZFRhZ3MgPSBUYWdPcmRlclN0b3JlLmdldFJlbW92ZWRUYWdzQWNjb3VudERhdGEoKSB8fCBbXTtcclxuICAgIGlmICghdGFncykge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICB0YWdzID0gdGFncy5maWx0ZXIoKHQpID0+IHQgIT09IHRhZyk7XHJcbiAgICB0YWdzID0gWy4uLnRhZ3Muc2xpY2UoMCwgZGVzdGluYXRpb25JeCksIHRhZywgLi4udGFncy5zbGljZShkZXN0aW5hdGlvbkl4KV07XHJcblxyXG4gICAgcmVtb3ZlZFRhZ3MgPSByZW1vdmVkVGFncy5maWx0ZXIoKHQpID0+IHQgIT09IHRhZyk7XHJcblxyXG4gICAgY29uc3Qgc3RvcmVJZCA9IFRhZ09yZGVyU3RvcmUuZ2V0U3RvcmVJZCgpO1xyXG5cclxuICAgIHJldHVybiBhc3luY0FjdGlvbignVGFnT3JkZXJBY3Rpb25zLm1vdmVUYWcnLCAoKSA9PiB7XHJcbiAgICAgICAgQW5hbHl0aWNzLnRyYWNrRXZlbnQoJ1RhZ09yZGVyQWN0aW9ucycsICdjb21taXRUYWdPcmRlcmluZycpO1xyXG4gICAgICAgIHJldHVybiBtYXRyaXhDbGllbnQuc2V0QWNjb3VudERhdGEoXHJcbiAgICAgICAgICAgICdpbS52ZWN0b3Iud2ViLnRhZ19vcmRlcmluZycsXHJcbiAgICAgICAgICAgIHt0YWdzLCByZW1vdmVkVGFncywgX3N0b3JlSWQ6IHN0b3JlSWR9LFxyXG4gICAgICAgICk7XHJcbiAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgLy8gRm9yIGFuIG9wdGltaXN0aWMgdXBkYXRlXHJcbiAgICAgICAgcmV0dXJuIHt0YWdzLCByZW1vdmVkVGFnc307XHJcbiAgICB9KTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBDcmVhdGVzIGFuIGFjdGlvbiB0aHVuayB0aGF0IHdpbGwgZG8gYW4gYXN5bmNocm9ub3VzIHJlcXVlc3QgdG9cclxuICogbGFiZWwgYSB0YWcgYXMgcmVtb3ZlZCBpbiBpbS52ZWN0b3Iud2ViLnRhZ19vcmRlcmluZyBhY2NvdW50IGRhdGEuXHJcbiAqXHJcbiAqIFRoZSByZWFzb24gdGhpcyBpcyBpbXBsZW1lbnRlZCB3aXRoIG5ldyBzdGF0ZSBgcmVtb3ZlZFRhZ3NgIGlzIHRoYXRcclxuICogd2UgaW5jcmVtZW50YWxseSBhbmQgaW5pdGlhbGx5IHBvcHVsYXRlIGB0YWdzYCB3aXRoIGdyb3VwcyB0aGF0XHJcbiAqIGhhdmUgYmVlbiBqb2luZWQuIElmIHdlIHJlbW92ZSBhIGdyb3VwIGZyb20gYHRhZ3NgLCBpdCB3aWxsIGp1c3RcclxuICogZ2V0IGFkZGVkIChhcyBpdCBsb29rcyBsaWtlIGEgZ3JvdXAgd2UndmUgcmVjZW50bHkgam9pbmVkKS5cclxuICpcclxuICogTkI6IElmIHdlIGV2ZXIgc3VwcG9ydCBhZGRpbmcgb2YgdGFncyAod2hpY2ggaXMgcGxhbm5lZCksIHdlIHNob3VsZFxyXG4gKiB0YWtlIHNwZWNpYWwgY2FyZSB0byByZW1vdmUgdGhlIHRhZyBmcm9tIGByZW1vdmVkVGFnc2Agd2hlbiB3ZSBhZGRcclxuICogaXQuXHJcbiAqXHJcbiAqIEBwYXJhbSB7TWF0cml4Q2xpZW50fSBtYXRyaXhDbGllbnQgdGhlIG1hdHJpeCBjbGllbnQgdG8gc2V0IHRoZVxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjY291bnQgZGF0YSBvbi5cclxuICogQHBhcmFtIHtzdHJpbmd9IHRhZyB0aGUgdGFnIHRvIHJlbW92ZS5cclxuICogQHJldHVybnMge2Z1bmN0aW9ufSBhbiBhY3Rpb24gdGh1bmsgdGhhdCB3aWxsIGRpc3BhdGNoIGFjdGlvbnNcclxuICogICAgICAgICAgICAgICAgICAgICBpbmRpY2F0aW5nIHRoZSBzdGF0dXMgb2YgdGhlIHJlcXVlc3QuXHJcbiAqIEBzZWUgYXN5bmNBY3Rpb25cclxuICovXHJcblRhZ09yZGVyQWN0aW9ucy5yZW1vdmVUYWcgPSBmdW5jdGlvbihtYXRyaXhDbGllbnQsIHRhZykge1xyXG4gICAgLy8gRG9uJ3QgY2hhbmdlIHRhZ3MsIGp1c3QgcmVtb3ZlZFRhZ3NcclxuICAgIGNvbnN0IHRhZ3MgPSBUYWdPcmRlclN0b3JlLmdldE9yZGVyZWRUYWdzKCk7XHJcbiAgICBjb25zdCByZW1vdmVkVGFncyA9IFRhZ09yZGVyU3RvcmUuZ2V0UmVtb3ZlZFRhZ3NBY2NvdW50RGF0YSgpIHx8IFtdO1xyXG5cclxuICAgIGlmIChyZW1vdmVkVGFncy5pbmNsdWRlcyh0YWcpKSB7XHJcbiAgICAgICAgLy8gUmV0dXJuIGEgdGh1bmsgdGhhdCBkb2Vzbid0IGRvIGFueXRoaW5nLCB3ZSBkb24ndCBldmVuIG5lZWRcclxuICAgICAgICAvLyBhbiBhc3luY2hyb25vdXMgYWN0aW9uIGhlcmUsIHRoZSB0YWcgaXMgYWxyZWFkeSByZW1vdmVkLlxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7fTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVkVGFncy5wdXNoKHRhZyk7XHJcblxyXG4gICAgY29uc3Qgc3RvcmVJZCA9IFRhZ09yZGVyU3RvcmUuZ2V0U3RvcmVJZCgpO1xyXG5cclxuICAgIHJldHVybiBhc3luY0FjdGlvbignVGFnT3JkZXJBY3Rpb25zLnJlbW92ZVRhZycsICgpID0+IHtcclxuICAgICAgICBBbmFseXRpY3MudHJhY2tFdmVudCgnVGFnT3JkZXJBY3Rpb25zJywgJ3JlbW92ZVRhZycpO1xyXG4gICAgICAgIHJldHVybiBtYXRyaXhDbGllbnQuc2V0QWNjb3VudERhdGEoXHJcbiAgICAgICAgICAgICdpbS52ZWN0b3Iud2ViLnRhZ19vcmRlcmluZycsXHJcbiAgICAgICAgICAgIHt0YWdzLCByZW1vdmVkVGFncywgX3N0b3JlSWQ6IHN0b3JlSWR9LFxyXG4gICAgICAgICk7XHJcbiAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgLy8gRm9yIGFuIG9wdGltaXN0aWMgdXBkYXRlXHJcbiAgICAgICAgcmV0dXJuIHtyZW1vdmVkVGFnc307XHJcbiAgICB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRhZ09yZGVyQWN0aW9ucztcclxuIl19