"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionCreators = require("./actionCreators");

var _RoomListStore = _interopRequireWildcard(require("../stores/RoomListStore"));

var _Modal = _interopRequireDefault(require("../Modal"));

var Rooms = _interopRequireWildcard(require("../Rooms"));

var _languageHandler = require("../languageHandler");

var sdk = _interopRequireWildcard(require("../index"));

/*
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const RoomListActions = {};
/**
 * Creates an action thunk that will do an asynchronous request to
 * tag room.
 *
 * @param {MatrixClient} matrixClient the matrix client to set the
 *                                    account data on.
 * @param {Room} room the room to tag.
 * @param {string} oldTag the tag to remove (unless oldTag ==== newTag)
 * @param {string} newTag the tag with which to tag the room.
 * @param {?number} oldIndex the previous position of the room in the
 *                           list of rooms.
 * @param {?number} newIndex the new position of the room in the list
 *                           of rooms.
 * @returns {function} an action thunk.
 * @see asyncAction
 */

RoomListActions.tagRoom = function (matrixClient, room, oldTag, newTag, oldIndex, newIndex) {
  let metaData = null; // Is the tag ordered manually?

  if (newTag && !newTag.match(/^(m\.lowpriority|im\.vector\.fake\.(invite|recent|direct|archived))$/)) {
    const lists = _RoomListStore.default.getRoomLists();

    const newList = [...lists[newTag]];
    newList.sort((a, b) => a.tags[newTag].order - b.tags[newTag].order); // If the room was moved "down" (increasing index) in the same list we
    // need to use the orders of the tiles with indices shifted by +1

    const offset = newTag === oldTag && oldIndex < newIndex ? 1 : 0;
    const indexBefore = offset + newIndex - 1;
    const indexAfter = offset + newIndex;
    const prevOrder = indexBefore <= 0 ? 0 : newList[indexBefore].tags[newTag].order;
    const nextOrder = indexAfter >= newList.length ? 1 : newList[indexAfter].tags[newTag].order;
    metaData = {
      order: (prevOrder + nextOrder) / 2.0
    };
  }

  return (0, _actionCreators.asyncAction)('RoomListActions.tagRoom', () => {
    const promises = [];
    const roomId = room.roomId; // Evil hack to get DMs behaving

    if (oldTag === undefined && newTag === _RoomListStore.TAG_DM || oldTag === _RoomListStore.TAG_DM && newTag === undefined) {
      return Rooms.guessAndSetDMRoom(room, newTag === _RoomListStore.TAG_DM).catch(err => {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to set direct chat tag " + err);

        _Modal.default.createTrackedDialog('Failed to set direct chat tag', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Failed to set direct chat tag'),
          description: err && err.message ? err.message : (0, _languageHandler._t)('Operation failed')
        });
      });
    }

    const hasChangedSubLists = oldTag !== newTag; // More evilness: We will still be dealing with moving to favourites/low prio,
    // but we avoid ever doing a request with TAG_DM.
    //
    // if we moved lists, remove the old tag

    if (oldTag && oldTag !== _RoomListStore.TAG_DM && hasChangedSubLists) {
      const promiseToDelete = matrixClient.deleteRoomTag(roomId, oldTag).catch(function (err) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to remove tag " + oldTag + " from room: " + err);

        _Modal.default.createTrackedDialog('Failed to remove tag from room', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Failed to remove tag %(tagName)s from room', {
            tagName: oldTag
          }),
          description: err && err.message ? err.message : (0, _languageHandler._t)('Operation failed')
        });
      });
      promises.push(promiseToDelete);
    } // if we moved lists or the ordering changed, add the new tag


    if (newTag && newTag !== _RoomListStore.TAG_DM && (hasChangedSubLists || metaData)) {
      // metaData is the body of the PUT to set the tag, so it must
      // at least be an empty object.
      metaData = metaData || {};
      const promiseToAdd = matrixClient.setRoomTag(roomId, newTag, metaData).catch(function (err) {
        const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
        console.error("Failed to add tag " + newTag + " to room: " + err);

        _Modal.default.createTrackedDialog('Failed to add tag to room', '', ErrorDialog, {
          title: (0, _languageHandler._t)('Failed to add tag %(tagName)s to room', {
            tagName: newTag
          }),
          description: err && err.message ? err.message : (0, _languageHandler._t)('Operation failed')
        });

        throw err;
      });
      promises.push(promiseToAdd);
    }

    return Promise.all(promises);
  }, () => {
    // For an optimistic update
    return {
      room,
      oldTag,
      newTag,
      metaData
    };
  });
};

var _default = RoomListActions;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hY3Rpb25zL1Jvb21MaXN0QWN0aW9ucy5qcyJdLCJuYW1lcyI6WyJSb29tTGlzdEFjdGlvbnMiLCJ0YWdSb29tIiwibWF0cml4Q2xpZW50Iiwicm9vbSIsIm9sZFRhZyIsIm5ld1RhZyIsIm9sZEluZGV4IiwibmV3SW5kZXgiLCJtZXRhRGF0YSIsIm1hdGNoIiwibGlzdHMiLCJSb29tTGlzdFN0b3JlIiwiZ2V0Um9vbUxpc3RzIiwibmV3TGlzdCIsInNvcnQiLCJhIiwiYiIsInRhZ3MiLCJvcmRlciIsIm9mZnNldCIsImluZGV4QmVmb3JlIiwiaW5kZXhBZnRlciIsInByZXZPcmRlciIsIm5leHRPcmRlciIsImxlbmd0aCIsInByb21pc2VzIiwicm9vbUlkIiwidW5kZWZpbmVkIiwiVEFHX0RNIiwiUm9vbXMiLCJndWVzc0FuZFNldERNUm9vbSIsImNhdGNoIiwiZXJyIiwiRXJyb3JEaWFsb2ciLCJzZGsiLCJnZXRDb21wb25lbnQiLCJjb25zb2xlIiwiZXJyb3IiLCJNb2RhbCIsImNyZWF0ZVRyYWNrZWREaWFsb2ciLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwibWVzc2FnZSIsImhhc0NoYW5nZWRTdWJMaXN0cyIsInByb21pc2VUb0RlbGV0ZSIsImRlbGV0ZVJvb21UYWciLCJ0YWdOYW1lIiwicHVzaCIsInByb21pc2VUb0FkZCIsInNldFJvb21UYWciLCJQcm9taXNlIiwiYWxsIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQWdCQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFyQkE7Ozs7Ozs7Ozs7Ozs7OztBQXVCQSxNQUFNQSxlQUFlLEdBQUcsRUFBeEI7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkFBLGVBQWUsQ0FBQ0MsT0FBaEIsR0FBMEIsVUFBU0MsWUFBVCxFQUF1QkMsSUFBdkIsRUFBNkJDLE1BQTdCLEVBQXFDQyxNQUFyQyxFQUE2Q0MsUUFBN0MsRUFBdURDLFFBQXZELEVBQWlFO0FBQ3ZGLE1BQUlDLFFBQVEsR0FBRyxJQUFmLENBRHVGLENBR3ZGOztBQUNBLE1BQUlILE1BQU0sSUFBSSxDQUFDQSxNQUFNLENBQUNJLEtBQVAsQ0FBYSxzRUFBYixDQUFmLEVBQXFHO0FBQ2pHLFVBQU1DLEtBQUssR0FBR0MsdUJBQWNDLFlBQWQsRUFBZDs7QUFDQSxVQUFNQyxPQUFPLEdBQUcsQ0FBQyxHQUFHSCxLQUFLLENBQUNMLE1BQUQsQ0FBVCxDQUFoQjtBQUVBUSxJQUFBQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUQsQ0FBQyxDQUFDRSxJQUFGLENBQU9aLE1BQVAsRUFBZWEsS0FBZixHQUF1QkYsQ0FBQyxDQUFDQyxJQUFGLENBQU9aLE1BQVAsRUFBZWEsS0FBN0QsRUFKaUcsQ0FNakc7QUFDQTs7QUFDQSxVQUFNQyxNQUFNLEdBQ1JkLE1BQU0sS0FBS0QsTUFBWCxJQUFxQkUsUUFBUSxHQUFHQyxRQURyQixHQUVYLENBRlcsR0FFUCxDQUZSO0FBSUEsVUFBTWEsV0FBVyxHQUFHRCxNQUFNLEdBQUdaLFFBQVQsR0FBb0IsQ0FBeEM7QUFDQSxVQUFNYyxVQUFVLEdBQUdGLE1BQU0sR0FBR1osUUFBNUI7QUFFQSxVQUFNZSxTQUFTLEdBQUdGLFdBQVcsSUFBSSxDQUFmLEdBQ2QsQ0FEYyxHQUNWUCxPQUFPLENBQUNPLFdBQUQsQ0FBUCxDQUFxQkgsSUFBckIsQ0FBMEJaLE1BQTFCLEVBQWtDYSxLQUQxQztBQUVBLFVBQU1LLFNBQVMsR0FBR0YsVUFBVSxJQUFJUixPQUFPLENBQUNXLE1BQXRCLEdBQ2QsQ0FEYyxHQUNWWCxPQUFPLENBQUNRLFVBQUQsQ0FBUCxDQUFvQkosSUFBcEIsQ0FBeUJaLE1BQXpCLEVBQWlDYSxLQUR6QztBQUdBVixJQUFBQSxRQUFRLEdBQUc7QUFDUFUsTUFBQUEsS0FBSyxFQUFFLENBQUNJLFNBQVMsR0FBR0MsU0FBYixJQUEwQjtBQUQxQixLQUFYO0FBR0g7O0FBRUQsU0FBTyxpQ0FBWSx5QkFBWixFQUF1QyxNQUFNO0FBQ2hELFVBQU1FLFFBQVEsR0FBRyxFQUFqQjtBQUNBLFVBQU1DLE1BQU0sR0FBR3ZCLElBQUksQ0FBQ3VCLE1BQXBCLENBRmdELENBSWhEOztBQUNBLFFBQUt0QixNQUFNLEtBQUt1QixTQUFYLElBQXdCdEIsTUFBTSxLQUFLdUIscUJBQXBDLElBQ0N4QixNQUFNLEtBQUt3QixxQkFBWCxJQUFxQnZCLE1BQU0sS0FBS3NCLFNBRHJDLEVBRUU7QUFDRSxhQUFPRSxLQUFLLENBQUNDLGlCQUFOLENBQ0gzQixJQURHLEVBQ0dFLE1BQU0sS0FBS3VCLHFCQURkLEVBRUxHLEtBRkssQ0FFRUMsR0FBRCxJQUFTO0FBQ2IsY0FBTUMsV0FBVyxHQUFHQyxHQUFHLENBQUNDLFlBQUosQ0FBaUIscUJBQWpCLENBQXBCO0FBQ0FDLFFBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLG1DQUFtQ0wsR0FBakQ7O0FBQ0FNLHVCQUFNQyxtQkFBTixDQUEwQiwrQkFBMUIsRUFBMkQsRUFBM0QsRUFBK0ROLFdBQS9ELEVBQTRFO0FBQ3hFTyxVQUFBQSxLQUFLLEVBQUUseUJBQUcsK0JBQUgsQ0FEaUU7QUFFeEVDLFVBQUFBLFdBQVcsRUFBSVQsR0FBRyxJQUFJQSxHQUFHLENBQUNVLE9BQVosR0FBdUJWLEdBQUcsQ0FBQ1UsT0FBM0IsR0FBcUMseUJBQUcsa0JBQUg7QUFGcUIsU0FBNUU7QUFJSCxPQVRNLENBQVA7QUFVSDs7QUFFRCxVQUFNQyxrQkFBa0IsR0FBR3ZDLE1BQU0sS0FBS0MsTUFBdEMsQ0FwQmdELENBc0JoRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxRQUFJRCxNQUFNLElBQUlBLE1BQU0sS0FBS3dCLHFCQUFyQixJQUNBZSxrQkFESixFQUVFO0FBQ0UsWUFBTUMsZUFBZSxHQUFHMUMsWUFBWSxDQUFDMkMsYUFBYixDQUNwQm5CLE1BRG9CLEVBQ1p0QixNQURZLEVBRXRCMkIsS0FGc0IsQ0FFaEIsVUFBU0MsR0FBVCxFQUFjO0FBQ2xCLGNBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYywwQkFBMEJqQyxNQUExQixHQUFtQyxjQUFuQyxHQUFvRDRCLEdBQWxFOztBQUNBTSx1QkFBTUMsbUJBQU4sQ0FBMEIsZ0NBQTFCLEVBQTRELEVBQTVELEVBQWdFTixXQUFoRSxFQUE2RTtBQUN6RU8sVUFBQUEsS0FBSyxFQUFFLHlCQUFHLDRDQUFILEVBQWlEO0FBQUNNLFlBQUFBLE9BQU8sRUFBRTFDO0FBQVYsV0FBakQsQ0FEa0U7QUFFekVxQyxVQUFBQSxXQUFXLEVBQUlULEdBQUcsSUFBSUEsR0FBRyxDQUFDVSxPQUFaLEdBQXVCVixHQUFHLENBQUNVLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRnNCLFNBQTdFO0FBSUgsT0FUdUIsQ0FBeEI7QUFXQWpCLE1BQUFBLFFBQVEsQ0FBQ3NCLElBQVQsQ0FBY0gsZUFBZDtBQUNILEtBekMrQyxDQTJDaEQ7OztBQUNBLFFBQUl2QyxNQUFNLElBQUlBLE1BQU0sS0FBS3VCLHFCQUFyQixLQUNDZSxrQkFBa0IsSUFBSW5DLFFBRHZCLENBQUosRUFFRTtBQUNFO0FBQ0E7QUFDQUEsTUFBQUEsUUFBUSxHQUFHQSxRQUFRLElBQUksRUFBdkI7QUFFQSxZQUFNd0MsWUFBWSxHQUFHOUMsWUFBWSxDQUFDK0MsVUFBYixDQUF3QnZCLE1BQXhCLEVBQWdDckIsTUFBaEMsRUFBd0NHLFFBQXhDLEVBQWtEdUIsS0FBbEQsQ0FBd0QsVUFBU0MsR0FBVCxFQUFjO0FBQ3ZGLGNBQU1DLFdBQVcsR0FBR0MsR0FBRyxDQUFDQyxZQUFKLENBQWlCLHFCQUFqQixDQUFwQjtBQUNBQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyx1QkFBdUJoQyxNQUF2QixHQUFnQyxZQUFoQyxHQUErQzJCLEdBQTdEOztBQUNBTSx1QkFBTUMsbUJBQU4sQ0FBMEIsMkJBQTFCLEVBQXVELEVBQXZELEVBQTJETixXQUEzRCxFQUF3RTtBQUNwRU8sVUFBQUEsS0FBSyxFQUFFLHlCQUFHLHVDQUFILEVBQTRDO0FBQUNNLFlBQUFBLE9BQU8sRUFBRXpDO0FBQVYsV0FBNUMsQ0FENkQ7QUFFcEVvQyxVQUFBQSxXQUFXLEVBQUlULEdBQUcsSUFBSUEsR0FBRyxDQUFDVSxPQUFaLEdBQXVCVixHQUFHLENBQUNVLE9BQTNCLEdBQXFDLHlCQUFHLGtCQUFIO0FBRmlCLFNBQXhFOztBQUtBLGNBQU1WLEdBQU47QUFDSCxPQVRvQixDQUFyQjtBQVdBUCxNQUFBQSxRQUFRLENBQUNzQixJQUFULENBQWNDLFlBQWQ7QUFDSDs7QUFFRCxXQUFPRSxPQUFPLENBQUNDLEdBQVIsQ0FBWTFCLFFBQVosQ0FBUDtBQUNILEdBbEVNLEVBa0VKLE1BQU07QUFDTDtBQUNBLFdBQU87QUFDSHRCLE1BQUFBLElBREc7QUFDR0MsTUFBQUEsTUFESDtBQUNXQyxNQUFBQSxNQURYO0FBQ21CRyxNQUFBQTtBQURuQixLQUFQO0FBR0gsR0F2RU0sQ0FBUDtBQXdFSCxDQXJHRDs7ZUF1R2VSLGUiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxOCBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbmltcG9ydCB7IGFzeW5jQWN0aW9uIH0gZnJvbSAnLi9hY3Rpb25DcmVhdG9ycyc7XHJcbmltcG9ydCBSb29tTGlzdFN0b3JlLCB7VEFHX0RNfSBmcm9tICcuLi9zdG9yZXMvUm9vbUxpc3RTdG9yZSc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi9Nb2RhbCc7XHJcbmltcG9ydCAqIGFzIFJvb21zIGZyb20gJy4uL1Jvb21zJztcclxuaW1wb3J0IHsgX3QgfSBmcm9tICcuLi9sYW5ndWFnZUhhbmRsZXInO1xyXG5pbXBvcnQgKiBhcyBzZGsgZnJvbSAnLi4vaW5kZXgnO1xyXG5cclxuY29uc3QgUm9vbUxpc3RBY3Rpb25zID0ge307XHJcblxyXG4vKipcclxuICogQ3JlYXRlcyBhbiBhY3Rpb24gdGh1bmsgdGhhdCB3aWxsIGRvIGFuIGFzeW5jaHJvbm91cyByZXF1ZXN0IHRvXHJcbiAqIHRhZyByb29tLlxyXG4gKlxyXG4gKiBAcGFyYW0ge01hdHJpeENsaWVudH0gbWF0cml4Q2xpZW50IHRoZSBtYXRyaXggY2xpZW50IHRvIHNldCB0aGVcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY2NvdW50IGRhdGEgb24uXHJcbiAqIEBwYXJhbSB7Um9vbX0gcm9vbSB0aGUgcm9vbSB0byB0YWcuXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBvbGRUYWcgdGhlIHRhZyB0byByZW1vdmUgKHVubGVzcyBvbGRUYWcgPT09PSBuZXdUYWcpXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBuZXdUYWcgdGhlIHRhZyB3aXRoIHdoaWNoIHRvIHRhZyB0aGUgcm9vbS5cclxuICogQHBhcmFtIHs/bnVtYmVyfSBvbGRJbmRleCB0aGUgcHJldmlvdXMgcG9zaXRpb24gb2YgdGhlIHJvb20gaW4gdGhlXHJcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdCBvZiByb29tcy5cclxuICogQHBhcmFtIHs/bnVtYmVyfSBuZXdJbmRleCB0aGUgbmV3IHBvc2l0aW9uIG9mIHRoZSByb29tIGluIHRoZSBsaXN0XHJcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgb2Ygcm9vbXMuXHJcbiAqIEByZXR1cm5zIHtmdW5jdGlvbn0gYW4gYWN0aW9uIHRodW5rLlxyXG4gKiBAc2VlIGFzeW5jQWN0aW9uXHJcbiAqL1xyXG5Sb29tTGlzdEFjdGlvbnMudGFnUm9vbSA9IGZ1bmN0aW9uKG1hdHJpeENsaWVudCwgcm9vbSwgb2xkVGFnLCBuZXdUYWcsIG9sZEluZGV4LCBuZXdJbmRleCkge1xyXG4gICAgbGV0IG1ldGFEYXRhID0gbnVsbDtcclxuXHJcbiAgICAvLyBJcyB0aGUgdGFnIG9yZGVyZWQgbWFudWFsbHk/XHJcbiAgICBpZiAobmV3VGFnICYmICFuZXdUYWcubWF0Y2goL14obVxcLmxvd3ByaW9yaXR5fGltXFwudmVjdG9yXFwuZmFrZVxcLihpbnZpdGV8cmVjZW50fGRpcmVjdHxhcmNoaXZlZCkpJC8pKSB7XHJcbiAgICAgICAgY29uc3QgbGlzdHMgPSBSb29tTGlzdFN0b3JlLmdldFJvb21MaXN0cygpO1xyXG4gICAgICAgIGNvbnN0IG5ld0xpc3QgPSBbLi4ubGlzdHNbbmV3VGFnXV07XHJcblxyXG4gICAgICAgIG5ld0xpc3Quc29ydCgoYSwgYikgPT4gYS50YWdzW25ld1RhZ10ub3JkZXIgLSBiLnRhZ3NbbmV3VGFnXS5vcmRlcik7XHJcblxyXG4gICAgICAgIC8vIElmIHRoZSByb29tIHdhcyBtb3ZlZCBcImRvd25cIiAoaW5jcmVhc2luZyBpbmRleCkgaW4gdGhlIHNhbWUgbGlzdCB3ZVxyXG4gICAgICAgIC8vIG5lZWQgdG8gdXNlIHRoZSBvcmRlcnMgb2YgdGhlIHRpbGVzIHdpdGggaW5kaWNlcyBzaGlmdGVkIGJ5ICsxXHJcbiAgICAgICAgY29uc3Qgb2Zmc2V0ID0gKFxyXG4gICAgICAgICAgICBuZXdUYWcgPT09IG9sZFRhZyAmJiBvbGRJbmRleCA8IG5ld0luZGV4XHJcbiAgICAgICAgKSA/IDEgOiAwO1xyXG5cclxuICAgICAgICBjb25zdCBpbmRleEJlZm9yZSA9IG9mZnNldCArIG5ld0luZGV4IC0gMTtcclxuICAgICAgICBjb25zdCBpbmRleEFmdGVyID0gb2Zmc2V0ICsgbmV3SW5kZXg7XHJcblxyXG4gICAgICAgIGNvbnN0IHByZXZPcmRlciA9IGluZGV4QmVmb3JlIDw9IDAgP1xyXG4gICAgICAgICAgICAwIDogbmV3TGlzdFtpbmRleEJlZm9yZV0udGFnc1tuZXdUYWddLm9yZGVyO1xyXG4gICAgICAgIGNvbnN0IG5leHRPcmRlciA9IGluZGV4QWZ0ZXIgPj0gbmV3TGlzdC5sZW5ndGggP1xyXG4gICAgICAgICAgICAxIDogbmV3TGlzdFtpbmRleEFmdGVyXS50YWdzW25ld1RhZ10ub3JkZXI7XHJcblxyXG4gICAgICAgIG1ldGFEYXRhID0ge1xyXG4gICAgICAgICAgICBvcmRlcjogKHByZXZPcmRlciArIG5leHRPcmRlcikgLyAyLjAsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gYXN5bmNBY3Rpb24oJ1Jvb21MaXN0QWN0aW9ucy50YWdSb29tJywgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2VzID0gW107XHJcbiAgICAgICAgY29uc3Qgcm9vbUlkID0gcm9vbS5yb29tSWQ7XHJcblxyXG4gICAgICAgIC8vIEV2aWwgaGFjayB0byBnZXQgRE1zIGJlaGF2aW5nXHJcbiAgICAgICAgaWYgKChvbGRUYWcgPT09IHVuZGVmaW5lZCAmJiBuZXdUYWcgPT09IFRBR19ETSkgfHxcclxuICAgICAgICAgICAgKG9sZFRhZyA9PT0gVEFHX0RNICYmIG5ld1RhZyA9PT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICByZXR1cm4gUm9vbXMuZ3Vlc3NBbmRTZXRETVJvb20oXHJcbiAgICAgICAgICAgICAgICByb29tLCBuZXdUYWcgPT09IFRBR19ETSxcclxuICAgICAgICAgICAgKS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBzZXQgZGlyZWN0IGNoYXQgdGFnIFwiICsgZXJyKTtcclxuICAgICAgICAgICAgICAgIE1vZGFsLmNyZWF0ZVRyYWNrZWREaWFsb2coJ0ZhaWxlZCB0byBzZXQgZGlyZWN0IGNoYXQgdGFnJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gc2V0IGRpcmVjdCBjaGF0IHRhZycpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAoKGVyciAmJiBlcnIubWVzc2FnZSkgPyBlcnIubWVzc2FnZSA6IF90KCdPcGVyYXRpb24gZmFpbGVkJykpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaGFzQ2hhbmdlZFN1Ykxpc3RzID0gb2xkVGFnICE9PSBuZXdUYWc7XHJcblxyXG4gICAgICAgIC8vIE1vcmUgZXZpbG5lc3M6IFdlIHdpbGwgc3RpbGwgYmUgZGVhbGluZyB3aXRoIG1vdmluZyB0byBmYXZvdXJpdGVzL2xvdyBwcmlvLFxyXG4gICAgICAgIC8vIGJ1dCB3ZSBhdm9pZCBldmVyIGRvaW5nIGEgcmVxdWVzdCB3aXRoIFRBR19ETS5cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vIGlmIHdlIG1vdmVkIGxpc3RzLCByZW1vdmUgdGhlIG9sZCB0YWdcclxuICAgICAgICBpZiAob2xkVGFnICYmIG9sZFRhZyAhPT0gVEFHX0RNICYmXHJcbiAgICAgICAgICAgIGhhc0NoYW5nZWRTdWJMaXN0c1xyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICBjb25zdCBwcm9taXNlVG9EZWxldGUgPSBtYXRyaXhDbGllbnQuZGVsZXRlUm9vbVRhZyhcclxuICAgICAgICAgICAgICAgIHJvb21JZCwgb2xkVGFnLFxyXG4gICAgICAgICAgICApLmNhdGNoKGZ1bmN0aW9uKGVycikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgRXJyb3JEaWFsb2cgPSBzZGsuZ2V0Q29tcG9uZW50KFwiZGlhbG9ncy5FcnJvckRpYWxvZ1wiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gcmVtb3ZlIHRhZyBcIiArIG9sZFRhZyArIFwiIGZyb20gcm9vbTogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIHJlbW92ZSB0YWcgZnJvbSByb29tJywgJycsIEVycm9yRGlhbG9nLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90KCdGYWlsZWQgdG8gcmVtb3ZlIHRhZyAlKHRhZ05hbWUpcyBmcm9tIHJvb20nLCB7dGFnTmFtZTogb2xkVGFnfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICgoZXJyICYmIGVyci5tZXNzYWdlKSA/IGVyci5tZXNzYWdlIDogX3QoJ09wZXJhdGlvbiBmYWlsZWQnKSksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBwcm9taXNlcy5wdXNoKHByb21pc2VUb0RlbGV0ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZiB3ZSBtb3ZlZCBsaXN0cyBvciB0aGUgb3JkZXJpbmcgY2hhbmdlZCwgYWRkIHRoZSBuZXcgdGFnXHJcbiAgICAgICAgaWYgKG5ld1RhZyAmJiBuZXdUYWcgIT09IFRBR19ETSAmJlxyXG4gICAgICAgICAgICAoaGFzQ2hhbmdlZFN1Ykxpc3RzIHx8IG1ldGFEYXRhKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICAvLyBtZXRhRGF0YSBpcyB0aGUgYm9keSBvZiB0aGUgUFVUIHRvIHNldCB0aGUgdGFnLCBzbyBpdCBtdXN0XHJcbiAgICAgICAgICAgIC8vIGF0IGxlYXN0IGJlIGFuIGVtcHR5IG9iamVjdC5cclxuICAgICAgICAgICAgbWV0YURhdGEgPSBtZXRhRGF0YSB8fCB7fTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb21pc2VUb0FkZCA9IG1hdHJpeENsaWVudC5zZXRSb29tVGFnKHJvb21JZCwgbmV3VGFnLCBtZXRhRGF0YSkuY2F0Y2goZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBFcnJvckRpYWxvZyA9IHNkay5nZXRDb21wb25lbnQoXCJkaWFsb2dzLkVycm9yRGlhbG9nXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBhZGQgdGFnIFwiICsgbmV3VGFnICsgXCIgdG8gcm9vbTogXCIgKyBlcnIpO1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuY3JlYXRlVHJhY2tlZERpYWxvZygnRmFpbGVkIHRvIGFkZCB0YWcgdG8gcm9vbScsICcnLCBFcnJvckRpYWxvZywge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdCgnRmFpbGVkIHRvIGFkZCB0YWcgJSh0YWdOYW1lKXMgdG8gcm9vbScsIHt0YWdOYW1lOiBuZXdUYWd9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogKChlcnIgJiYgZXJyLm1lc3NhZ2UpID8gZXJyLm1lc3NhZ2UgOiBfdCgnT3BlcmF0aW9uIGZhaWxlZCcpKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRocm93IGVycjtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBwcm9taXNlcy5wdXNoKHByb21pc2VUb0FkZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xyXG4gICAgfSwgKCkgPT4ge1xyXG4gICAgICAgIC8vIEZvciBhbiBvcHRpbWlzdGljIHVwZGF0ZVxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJvb20sIG9sZFRhZywgbmV3VGFnLCBtZXRhRGF0YSxcclxuICAgICAgICB9O1xyXG4gICAgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBSb29tTGlzdEFjdGlvbnM7XHJcbiJdfQ==