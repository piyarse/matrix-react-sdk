"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asyncAction = asyncAction;

/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Create an action thunk that will dispatch actions indicating the current
 * status of the Promise returned by fn.
 *
 * @param {string} id the id to give the dispatched actions. This is given a
 *                    suffix determining whether it is pending, successful or
 *                    a failure.
 * @param {function} fn a function that returns a Promise.
 * @param {function?} pendingFn a function that returns an object to assign
 *                              to the `request` key of the ${id}.pending
 *                              payload.
 * @returns {function} an action thunk - a function that uses its single
 *                     argument as a dispatch function to dispatch the
 *                     following actions:
 *                         `${id}.pending` and either
 *                         `${id}.success` or
 *                         `${id}.failure`.
 *
 *                     The shape of each are:
 *                     { action: '${id}.pending', request }
 *                     { action: '${id}.success', result }
 *                     { action: '${id}.failure', err }
 *
 *                     where `request` is returned by `pendingFn` and
 *                     result is the result of the promise returned by
 *                     `fn`.
 */
function asyncAction(id, fn, pendingFn) {
  return dispatch => {
    dispatch({
      action: id + '.pending',
      request: typeof pendingFn === 'function' ? pendingFn() : undefined
    });
    fn().then(result => {
      dispatch({
        action: id + '.success',
        result
      });
    }).catch(err => {
      dispatch({
        action: id + '.failure',
        err
      });
    });
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hY3Rpb25zL2FjdGlvbkNyZWF0b3JzLmpzIl0sIm5hbWVzIjpbImFzeW5jQWN0aW9uIiwiaWQiLCJmbiIsInBlbmRpbmdGbiIsImRpc3BhdGNoIiwiYWN0aW9uIiwicmVxdWVzdCIsInVuZGVmaW5lZCIsInRoZW4iLCJyZXN1bHQiLCJjYXRjaCIsImVyciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQk8sU0FBU0EsV0FBVCxDQUFxQkMsRUFBckIsRUFBeUJDLEVBQXpCLEVBQTZCQyxTQUE3QixFQUF3QztBQUMzQyxTQUFRQyxRQUFELElBQWM7QUFDakJBLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxNQUFNLEVBQUVKLEVBQUUsR0FBRyxVQURSO0FBRUxLLE1BQUFBLE9BQU8sRUFDSCxPQUFPSCxTQUFQLEtBQXFCLFVBQXJCLEdBQWtDQSxTQUFTLEVBQTNDLEdBQWdESTtBQUgvQyxLQUFELENBQVI7QUFLQUwsSUFBQUEsRUFBRSxHQUFHTSxJQUFMLENBQVdDLE1BQUQsSUFBWTtBQUNsQkwsTUFBQUEsUUFBUSxDQUFDO0FBQUNDLFFBQUFBLE1BQU0sRUFBRUosRUFBRSxHQUFHLFVBQWQ7QUFBMEJRLFFBQUFBO0FBQTFCLE9BQUQsQ0FBUjtBQUNILEtBRkQsRUFFR0MsS0FGSCxDQUVVQyxHQUFELElBQVM7QUFDZFAsTUFBQUEsUUFBUSxDQUFDO0FBQUNDLFFBQUFBLE1BQU0sRUFBRUosRUFBRSxHQUFHLFVBQWQ7QUFBMEJVLFFBQUFBO0FBQTFCLE9BQUQsQ0FBUjtBQUNILEtBSkQ7QUFLSCxHQVhEO0FBWUgiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG5Db3B5cmlnaHQgMjAxNyBOZXcgVmVjdG9yIEx0ZFxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbi8qKlxyXG4gKiBDcmVhdGUgYW4gYWN0aW9uIHRodW5rIHRoYXQgd2lsbCBkaXNwYXRjaCBhY3Rpb25zIGluZGljYXRpbmcgdGhlIGN1cnJlbnRcclxuICogc3RhdHVzIG9mIHRoZSBQcm9taXNlIHJldHVybmVkIGJ5IGZuLlxyXG4gKlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gaWQgdGhlIGlkIHRvIGdpdmUgdGhlIGRpc3BhdGNoZWQgYWN0aW9ucy4gVGhpcyBpcyBnaXZlbiBhXHJcbiAqICAgICAgICAgICAgICAgICAgICBzdWZmaXggZGV0ZXJtaW5pbmcgd2hldGhlciBpdCBpcyBwZW5kaW5nLCBzdWNjZXNzZnVsIG9yXHJcbiAqICAgICAgICAgICAgICAgICAgICBhIGZhaWx1cmUuXHJcbiAqIEBwYXJhbSB7ZnVuY3Rpb259IGZuIGEgZnVuY3Rpb24gdGhhdCByZXR1cm5zIGEgUHJvbWlzZS5cclxuICogQHBhcmFtIHtmdW5jdGlvbj99IHBlbmRpbmdGbiBhIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhbiBvYmplY3QgdG8gYXNzaWduXHJcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG8gdGhlIGByZXF1ZXN0YCBrZXkgb2YgdGhlICR7aWR9LnBlbmRpbmdcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXlsb2FkLlxyXG4gKiBAcmV0dXJucyB7ZnVuY3Rpb259IGFuIGFjdGlvbiB0aHVuayAtIGEgZnVuY3Rpb24gdGhhdCB1c2VzIGl0cyBzaW5nbGVcclxuICogICAgICAgICAgICAgICAgICAgICBhcmd1bWVudCBhcyBhIGRpc3BhdGNoIGZ1bmN0aW9uIHRvIGRpc3BhdGNoIHRoZVxyXG4gKiAgICAgICAgICAgICAgICAgICAgIGZvbGxvd2luZyBhY3Rpb25zOlxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICBgJHtpZH0ucGVuZGluZ2AgYW5kIGVpdGhlclxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICBgJHtpZH0uc3VjY2Vzc2Agb3JcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgYCR7aWR9LmZhaWx1cmVgLlxyXG4gKlxyXG4gKiAgICAgICAgICAgICAgICAgICAgIFRoZSBzaGFwZSBvZiBlYWNoIGFyZTpcclxuICogICAgICAgICAgICAgICAgICAgICB7IGFjdGlvbjogJyR7aWR9LnBlbmRpbmcnLCByZXF1ZXN0IH1cclxuICogICAgICAgICAgICAgICAgICAgICB7IGFjdGlvbjogJyR7aWR9LnN1Y2Nlc3MnLCByZXN1bHQgfVxyXG4gKiAgICAgICAgICAgICAgICAgICAgIHsgYWN0aW9uOiAnJHtpZH0uZmFpbHVyZScsIGVyciB9XHJcbiAqXHJcbiAqICAgICAgICAgICAgICAgICAgICAgd2hlcmUgYHJlcXVlc3RgIGlzIHJldHVybmVkIGJ5IGBwZW5kaW5nRm5gIGFuZFxyXG4gKiAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCBpcyB0aGUgcmVzdWx0IG9mIHRoZSBwcm9taXNlIHJldHVybmVkIGJ5XHJcbiAqICAgICAgICAgICAgICAgICAgICAgYGZuYC5cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBhc3luY0FjdGlvbihpZCwgZm4sIHBlbmRpbmdGbikge1xyXG4gICAgcmV0dXJuIChkaXNwYXRjaCkgPT4ge1xyXG4gICAgICAgIGRpc3BhdGNoKHtcclxuICAgICAgICAgICAgYWN0aW9uOiBpZCArICcucGVuZGluZycsXHJcbiAgICAgICAgICAgIHJlcXVlc3Q6XHJcbiAgICAgICAgICAgICAgICB0eXBlb2YgcGVuZGluZ0ZuID09PSAnZnVuY3Rpb24nID8gcGVuZGluZ0ZuKCkgOiB1bmRlZmluZWQsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZm4oKS50aGVuKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgZGlzcGF0Y2goe2FjdGlvbjogaWQgKyAnLnN1Y2Nlc3MnLCByZXN1bHR9KTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGRpc3BhdGNoKHthY3Rpb246IGlkICsgJy5mYWlsdXJlJywgZXJyfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59XHJcbiJdfQ==