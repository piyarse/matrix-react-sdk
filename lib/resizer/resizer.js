"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/*
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
classNames:
    // class on resize-handle
    handle: string
    // class on resize-handle
    reverse: string
    // class on resize-handle
    vertical: string
    // class on container
    resizing: string
*/
class Resizer {
  // TODO move vertical/horizontal to config option/container class
  // as it doesn't make sense to mix them within one container/Resizer
  constructor(container, distributorCtor, config) {
    if (!container) {
      throw new Error("Resizer requires a non-null `container` arg");
    }

    this.container = container;
    this.distributorCtor = distributorCtor;
    this.config = config;
    this.classNames = {
      handle: "resizer-handle",
      reverse: "resizer-reverse",
      vertical: "resizer-vertical",
      resizing: "resizer-resizing"
    };
    this._onMouseDown = this._onMouseDown.bind(this);
  }

  setClassNames(classNames) {
    this.classNames = classNames;
  }

  attach() {
    this.container.addEventListener("mousedown", this._onMouseDown, false);
  }

  detach() {
    this.container.removeEventListener("mousedown", this._onMouseDown, false);
  }
  /**
  Gives the distributor for a specific resize handle, as if you would have started
  to drag that handle. Can be used to manipulate the size of an item programmatically.
  @param {number} handleIndex the index of the resize handle in the container
  @return {Distributor} a new distributor for the given handle
  */


  forHandleAt(handleIndex) {
    const handles = this._getResizeHandles();

    const handle = handles[handleIndex];

    if (handle) {
      const {
        distributor
      } = this._createSizerAndDistributor(handle);

      return distributor;
    }
  }

  forHandleWithId(id) {
    const handles = this._getResizeHandles();

    const handle = handles.find(h => h.getAttribute("data-id") === id);

    if (handle) {
      const {
        distributor
      } = this._createSizerAndDistributor(handle);

      return distributor;
    }
  }

  isReverseResizeHandle(el) {
    return el && el.classList.contains(this.classNames.reverse);
  }

  isResizeHandle(el) {
    return el && el.classList.contains(this.classNames.handle);
  }

  _onMouseDown(event) {
    // use closest in case the resize handle contains
    // child dom nodes that can be the target
    const resizeHandle = event.target && event.target.closest(".".concat(this.classNames.handle));

    if (!resizeHandle || resizeHandle.parentElement !== this.container) {
      return;
    } // prevent starting a drag operation


    event.preventDefault(); // mark as currently resizing

    if (this.classNames.resizing) {
      this.container.classList.add(this.classNames.resizing);
    }

    const {
      sizer,
      distributor
    } = this._createSizerAndDistributor(resizeHandle);

    distributor.start();

    const onMouseMove = event => {
      const offset = sizer.offsetFromEvent(event);
      distributor.resizeFromContainerOffset(offset);
    };

    const body = document.body;

    const finishResize = () => {
      if (this.classNames.resizing) {
        this.container.classList.remove(this.classNames.resizing);
      }

      distributor.finish();
      body.removeEventListener("mouseup", finishResize, false);
      document.removeEventListener("mouseleave", finishResize, false);
      body.removeEventListener("mousemove", onMouseMove, false);
    };

    body.addEventListener("mouseup", finishResize, false);
    document.addEventListener("mouseleave", finishResize, false);
    body.addEventListener("mousemove", onMouseMove, false);
  }

  _createSizerAndDistributor(resizeHandle) {
    const vertical = resizeHandle.classList.contains(this.classNames.vertical);
    const reverse = this.isReverseResizeHandle(resizeHandle);
    const Distributor = this.distributorCtor;
    const sizer = Distributor.createSizer(this.container, vertical, reverse);
    const item = Distributor.createItem(resizeHandle, this, sizer);
    const distributor = new Distributor(item, this.config);
    return {
      sizer,
      distributor
    };
  }

  _getResizeHandles() {
    return Array.from(this.container.children).filter(el => {
      return this.isResizeHandle(el);
    });
  }

}

exports.default = Resizer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZXNpemVyL3Jlc2l6ZXIuanMiXSwibmFtZXMiOlsiUmVzaXplciIsImNvbnN0cnVjdG9yIiwiY29udGFpbmVyIiwiZGlzdHJpYnV0b3JDdG9yIiwiY29uZmlnIiwiRXJyb3IiLCJjbGFzc05hbWVzIiwiaGFuZGxlIiwicmV2ZXJzZSIsInZlcnRpY2FsIiwicmVzaXppbmciLCJfb25Nb3VzZURvd24iLCJiaW5kIiwic2V0Q2xhc3NOYW1lcyIsImF0dGFjaCIsImFkZEV2ZW50TGlzdGVuZXIiLCJkZXRhY2giLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiZm9ySGFuZGxlQXQiLCJoYW5kbGVJbmRleCIsImhhbmRsZXMiLCJfZ2V0UmVzaXplSGFuZGxlcyIsImRpc3RyaWJ1dG9yIiwiX2NyZWF0ZVNpemVyQW5kRGlzdHJpYnV0b3IiLCJmb3JIYW5kbGVXaXRoSWQiLCJpZCIsImZpbmQiLCJoIiwiZ2V0QXR0cmlidXRlIiwiaXNSZXZlcnNlUmVzaXplSGFuZGxlIiwiZWwiLCJjbGFzc0xpc3QiLCJjb250YWlucyIsImlzUmVzaXplSGFuZGxlIiwiZXZlbnQiLCJyZXNpemVIYW5kbGUiLCJ0YXJnZXQiLCJjbG9zZXN0IiwicGFyZW50RWxlbWVudCIsInByZXZlbnREZWZhdWx0IiwiYWRkIiwic2l6ZXIiLCJzdGFydCIsIm9uTW91c2VNb3ZlIiwib2Zmc2V0Iiwib2Zmc2V0RnJvbUV2ZW50IiwicmVzaXplRnJvbUNvbnRhaW5lck9mZnNldCIsImJvZHkiLCJkb2N1bWVudCIsImZpbmlzaFJlc2l6ZSIsInJlbW92ZSIsImZpbmlzaCIsIkRpc3RyaWJ1dG9yIiwiY3JlYXRlU2l6ZXIiLCJpdGVtIiwiY3JlYXRlSXRlbSIsIkFycmF5IiwiZnJvbSIsImNoaWxkcmVuIiwiZmlsdGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOzs7Ozs7Ozs7OztBQWFlLE1BQU1BLE9BQU4sQ0FBYztBQUN6QjtBQUNBO0FBQ0FDLEVBQUFBLFdBQVcsQ0FBQ0MsU0FBRCxFQUFZQyxlQUFaLEVBQTZCQyxNQUE3QixFQUFxQztBQUM1QyxRQUFJLENBQUNGLFNBQUwsRUFBZ0I7QUFDWixZQUFNLElBQUlHLEtBQUosQ0FBVSw2Q0FBVixDQUFOO0FBQ0g7O0FBQ0QsU0FBS0gsU0FBTCxHQUFpQkEsU0FBakI7QUFDQSxTQUFLQyxlQUFMLEdBQXVCQSxlQUF2QjtBQUNBLFNBQUtDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFNBQUtFLFVBQUwsR0FBa0I7QUFDZEMsTUFBQUEsTUFBTSxFQUFFLGdCQURNO0FBRWRDLE1BQUFBLE9BQU8sRUFBRSxpQkFGSztBQUdkQyxNQUFBQSxRQUFRLEVBQUUsa0JBSEk7QUFJZEMsTUFBQUEsUUFBUSxFQUFFO0FBSkksS0FBbEI7QUFNQSxTQUFLQyxZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0g7O0FBRURDLEVBQUFBLGFBQWEsQ0FBQ1AsVUFBRCxFQUFhO0FBQ3RCLFNBQUtBLFVBQUwsR0FBa0JBLFVBQWxCO0FBQ0g7O0FBRURRLEVBQUFBLE1BQU0sR0FBRztBQUNMLFNBQUtaLFNBQUwsQ0FBZWEsZ0JBQWYsQ0FBZ0MsV0FBaEMsRUFBNkMsS0FBS0osWUFBbEQsRUFBZ0UsS0FBaEU7QUFDSDs7QUFFREssRUFBQUEsTUFBTSxHQUFHO0FBQ0wsU0FBS2QsU0FBTCxDQUFlZSxtQkFBZixDQUFtQyxXQUFuQyxFQUFnRCxLQUFLTixZQUFyRCxFQUFtRSxLQUFuRTtBQUNIO0FBRUQ7Ozs7Ozs7O0FBTUFPLEVBQUFBLFdBQVcsQ0FBQ0MsV0FBRCxFQUFjO0FBQ3JCLFVBQU1DLE9BQU8sR0FBRyxLQUFLQyxpQkFBTCxFQUFoQjs7QUFDQSxVQUFNZCxNQUFNLEdBQUdhLE9BQU8sQ0FBQ0QsV0FBRCxDQUF0Qjs7QUFDQSxRQUFJWixNQUFKLEVBQVk7QUFDUixZQUFNO0FBQUNlLFFBQUFBO0FBQUQsVUFBZ0IsS0FBS0MsMEJBQUwsQ0FBZ0NoQixNQUFoQyxDQUF0Qjs7QUFDQSxhQUFPZSxXQUFQO0FBQ0g7QUFDSjs7QUFFREUsRUFBQUEsZUFBZSxDQUFDQyxFQUFELEVBQUs7QUFDaEIsVUFBTUwsT0FBTyxHQUFHLEtBQUtDLGlCQUFMLEVBQWhCOztBQUNBLFVBQU1kLE1BQU0sR0FBR2EsT0FBTyxDQUFDTSxJQUFSLENBQWNDLENBQUQsSUFBT0EsQ0FBQyxDQUFDQyxZQUFGLENBQWUsU0FBZixNQUE4QkgsRUFBbEQsQ0FBZjs7QUFDQSxRQUFJbEIsTUFBSixFQUFZO0FBQ1IsWUFBTTtBQUFDZSxRQUFBQTtBQUFELFVBQWdCLEtBQUtDLDBCQUFMLENBQWdDaEIsTUFBaEMsQ0FBdEI7O0FBQ0EsYUFBT2UsV0FBUDtBQUNIO0FBQ0o7O0FBRURPLEVBQUFBLHFCQUFxQixDQUFDQyxFQUFELEVBQUs7QUFDdEIsV0FBT0EsRUFBRSxJQUFJQSxFQUFFLENBQUNDLFNBQUgsQ0FBYUMsUUFBYixDQUFzQixLQUFLMUIsVUFBTCxDQUFnQkUsT0FBdEMsQ0FBYjtBQUNIOztBQUVEeUIsRUFBQUEsY0FBYyxDQUFDSCxFQUFELEVBQUs7QUFDZixXQUFPQSxFQUFFLElBQUlBLEVBQUUsQ0FBQ0MsU0FBSCxDQUFhQyxRQUFiLENBQXNCLEtBQUsxQixVQUFMLENBQWdCQyxNQUF0QyxDQUFiO0FBQ0g7O0FBRURJLEVBQUFBLFlBQVksQ0FBQ3VCLEtBQUQsRUFBUTtBQUNoQjtBQUNBO0FBQ0EsVUFBTUMsWUFBWSxHQUFHRCxLQUFLLENBQUNFLE1BQU4sSUFBZ0JGLEtBQUssQ0FBQ0UsTUFBTixDQUFhQyxPQUFiLFlBQXlCLEtBQUsvQixVQUFMLENBQWdCQyxNQUF6QyxFQUFyQzs7QUFDQSxRQUFJLENBQUM0QixZQUFELElBQWlCQSxZQUFZLENBQUNHLGFBQWIsS0FBK0IsS0FBS3BDLFNBQXpELEVBQW9FO0FBQ2hFO0FBQ0gsS0FOZSxDQU9oQjs7O0FBQ0FnQyxJQUFBQSxLQUFLLENBQUNLLGNBQU4sR0FSZ0IsQ0FVaEI7O0FBQ0EsUUFBSSxLQUFLakMsVUFBTCxDQUFnQkksUUFBcEIsRUFBOEI7QUFDMUIsV0FBS1IsU0FBTCxDQUFlNkIsU0FBZixDQUF5QlMsR0FBekIsQ0FBNkIsS0FBS2xDLFVBQUwsQ0FBZ0JJLFFBQTdDO0FBQ0g7O0FBRUQsVUFBTTtBQUFDK0IsTUFBQUEsS0FBRDtBQUFRbkIsTUFBQUE7QUFBUixRQUF1QixLQUFLQywwQkFBTCxDQUFnQ1ksWUFBaEMsQ0FBN0I7O0FBQ0FiLElBQUFBLFdBQVcsQ0FBQ29CLEtBQVo7O0FBRUEsVUFBTUMsV0FBVyxHQUFJVCxLQUFELElBQVc7QUFDM0IsWUFBTVUsTUFBTSxHQUFHSCxLQUFLLENBQUNJLGVBQU4sQ0FBc0JYLEtBQXRCLENBQWY7QUFDQVosTUFBQUEsV0FBVyxDQUFDd0IseUJBQVosQ0FBc0NGLE1BQXRDO0FBQ0gsS0FIRDs7QUFLQSxVQUFNRyxJQUFJLEdBQUdDLFFBQVEsQ0FBQ0QsSUFBdEI7O0FBQ0EsVUFBTUUsWUFBWSxHQUFHLE1BQU07QUFDdkIsVUFBSSxLQUFLM0MsVUFBTCxDQUFnQkksUUFBcEIsRUFBOEI7QUFDMUIsYUFBS1IsU0FBTCxDQUFlNkIsU0FBZixDQUF5Qm1CLE1BQXpCLENBQWdDLEtBQUs1QyxVQUFMLENBQWdCSSxRQUFoRDtBQUNIOztBQUNEWSxNQUFBQSxXQUFXLENBQUM2QixNQUFaO0FBQ0FKLE1BQUFBLElBQUksQ0FBQzlCLG1CQUFMLENBQXlCLFNBQXpCLEVBQW9DZ0MsWUFBcEMsRUFBa0QsS0FBbEQ7QUFDQUQsTUFBQUEsUUFBUSxDQUFDL0IsbUJBQVQsQ0FBNkIsWUFBN0IsRUFBMkNnQyxZQUEzQyxFQUF5RCxLQUF6RDtBQUNBRixNQUFBQSxJQUFJLENBQUM5QixtQkFBTCxDQUF5QixXQUF6QixFQUFzQzBCLFdBQXRDLEVBQW1ELEtBQW5EO0FBQ0gsS0FSRDs7QUFTQUksSUFBQUEsSUFBSSxDQUFDaEMsZ0JBQUwsQ0FBc0IsU0FBdEIsRUFBaUNrQyxZQUFqQyxFQUErQyxLQUEvQztBQUNBRCxJQUFBQSxRQUFRLENBQUNqQyxnQkFBVCxDQUEwQixZQUExQixFQUF3Q2tDLFlBQXhDLEVBQXNELEtBQXREO0FBQ0FGLElBQUFBLElBQUksQ0FBQ2hDLGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DNEIsV0FBbkMsRUFBZ0QsS0FBaEQ7QUFDSDs7QUFFRHBCLEVBQUFBLDBCQUEwQixDQUFDWSxZQUFELEVBQWU7QUFDckMsVUFBTTFCLFFBQVEsR0FBRzBCLFlBQVksQ0FBQ0osU0FBYixDQUF1QkMsUUFBdkIsQ0FBZ0MsS0FBSzFCLFVBQUwsQ0FBZ0JHLFFBQWhELENBQWpCO0FBQ0EsVUFBTUQsT0FBTyxHQUFHLEtBQUtxQixxQkFBTCxDQUEyQk0sWUFBM0IsQ0FBaEI7QUFDQSxVQUFNaUIsV0FBVyxHQUFHLEtBQUtqRCxlQUF6QjtBQUNBLFVBQU1zQyxLQUFLLEdBQUdXLFdBQVcsQ0FBQ0MsV0FBWixDQUF3QixLQUFLbkQsU0FBN0IsRUFBd0NPLFFBQXhDLEVBQWtERCxPQUFsRCxDQUFkO0FBQ0EsVUFBTThDLElBQUksR0FBR0YsV0FBVyxDQUFDRyxVQUFaLENBQXVCcEIsWUFBdkIsRUFBcUMsSUFBckMsRUFBMkNNLEtBQTNDLENBQWI7QUFDQSxVQUFNbkIsV0FBVyxHQUFHLElBQUk4QixXQUFKLENBQWdCRSxJQUFoQixFQUFzQixLQUFLbEQsTUFBM0IsQ0FBcEI7QUFDQSxXQUFPO0FBQUNxQyxNQUFBQSxLQUFEO0FBQVFuQixNQUFBQTtBQUFSLEtBQVA7QUFDSDs7QUFFREQsRUFBQUEsaUJBQWlCLEdBQUc7QUFDaEIsV0FBT21DLEtBQUssQ0FBQ0MsSUFBTixDQUFXLEtBQUt2RCxTQUFMLENBQWV3RCxRQUExQixFQUFvQ0MsTUFBcEMsQ0FBMkM3QixFQUFFLElBQUk7QUFDcEQsYUFBTyxLQUFLRyxjQUFMLENBQW9CSCxFQUFwQixDQUFQO0FBQ0gsS0FGTSxDQUFQO0FBR0g7O0FBbkh3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IFRoZSBNYXRyaXgub3JnIEZvdW5kYXRpb24gQy5JLkMuXHJcblxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG55b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbllvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG5cclxuICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG5kaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbldJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG5TZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbmxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qL1xyXG5cclxuLypcclxuY2xhc3NOYW1lczpcclxuICAgIC8vIGNsYXNzIG9uIHJlc2l6ZS1oYW5kbGVcclxuICAgIGhhbmRsZTogc3RyaW5nXHJcbiAgICAvLyBjbGFzcyBvbiByZXNpemUtaGFuZGxlXHJcbiAgICByZXZlcnNlOiBzdHJpbmdcclxuICAgIC8vIGNsYXNzIG9uIHJlc2l6ZS1oYW5kbGVcclxuICAgIHZlcnRpY2FsOiBzdHJpbmdcclxuICAgIC8vIGNsYXNzIG9uIGNvbnRhaW5lclxyXG4gICAgcmVzaXppbmc6IHN0cmluZ1xyXG4qL1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJlc2l6ZXIge1xyXG4gICAgLy8gVE9ETyBtb3ZlIHZlcnRpY2FsL2hvcml6b250YWwgdG8gY29uZmlnIG9wdGlvbi9jb250YWluZXIgY2xhc3NcclxuICAgIC8vIGFzIGl0IGRvZXNuJ3QgbWFrZSBzZW5zZSB0byBtaXggdGhlbSB3aXRoaW4gb25lIGNvbnRhaW5lci9SZXNpemVyXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIsIGRpc3RyaWJ1dG9yQ3RvciwgY29uZmlnKSB7XHJcbiAgICAgICAgaWYgKCFjb250YWluZXIpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiUmVzaXplciByZXF1aXJlcyBhIG5vbi1udWxsIGBjb250YWluZXJgIGFyZ1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXI7XHJcbiAgICAgICAgdGhpcy5kaXN0cmlidXRvckN0b3IgPSBkaXN0cmlidXRvckN0b3I7XHJcbiAgICAgICAgdGhpcy5jb25maWcgPSBjb25maWc7XHJcbiAgICAgICAgdGhpcy5jbGFzc05hbWVzID0ge1xyXG4gICAgICAgICAgICBoYW5kbGU6IFwicmVzaXplci1oYW5kbGVcIixcclxuICAgICAgICAgICAgcmV2ZXJzZTogXCJyZXNpemVyLXJldmVyc2VcIixcclxuICAgICAgICAgICAgdmVydGljYWw6IFwicmVzaXplci12ZXJ0aWNhbFwiLFxyXG4gICAgICAgICAgICByZXNpemluZzogXCJyZXNpemVyLXJlc2l6aW5nXCIsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9vbk1vdXNlRG93biA9IHRoaXMuX29uTW91c2VEb3duLmJpbmQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q2xhc3NOYW1lcyhjbGFzc05hbWVzKSB7XHJcbiAgICAgICAgdGhpcy5jbGFzc05hbWVzID0gY2xhc3NOYW1lcztcclxuICAgIH1cclxuXHJcbiAgICBhdHRhY2goKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCB0aGlzLl9vbk1vdXNlRG93biwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGRldGFjaCgpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIHRoaXMuX29uTW91c2VEb3duLCBmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICBHaXZlcyB0aGUgZGlzdHJpYnV0b3IgZm9yIGEgc3BlY2lmaWMgcmVzaXplIGhhbmRsZSwgYXMgaWYgeW91IHdvdWxkIGhhdmUgc3RhcnRlZFxyXG4gICAgdG8gZHJhZyB0aGF0IGhhbmRsZS4gQ2FuIGJlIHVzZWQgdG8gbWFuaXB1bGF0ZSB0aGUgc2l6ZSBvZiBhbiBpdGVtIHByb2dyYW1tYXRpY2FsbHkuXHJcbiAgICBAcGFyYW0ge251bWJlcn0gaGFuZGxlSW5kZXggdGhlIGluZGV4IG9mIHRoZSByZXNpemUgaGFuZGxlIGluIHRoZSBjb250YWluZXJcclxuICAgIEByZXR1cm4ge0Rpc3RyaWJ1dG9yfSBhIG5ldyBkaXN0cmlidXRvciBmb3IgdGhlIGdpdmVuIGhhbmRsZVxyXG4gICAgKi9cclxuICAgIGZvckhhbmRsZUF0KGhhbmRsZUluZGV4KSB7XHJcbiAgICAgICAgY29uc3QgaGFuZGxlcyA9IHRoaXMuX2dldFJlc2l6ZUhhbmRsZXMoKTtcclxuICAgICAgICBjb25zdCBoYW5kbGUgPSBoYW5kbGVzW2hhbmRsZUluZGV4XTtcclxuICAgICAgICBpZiAoaGFuZGxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtkaXN0cmlidXRvcn0gPSB0aGlzLl9jcmVhdGVTaXplckFuZERpc3RyaWJ1dG9yKGhhbmRsZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBkaXN0cmlidXRvcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9ySGFuZGxlV2l0aElkKGlkKSB7XHJcbiAgICAgICAgY29uc3QgaGFuZGxlcyA9IHRoaXMuX2dldFJlc2l6ZUhhbmRsZXMoKTtcclxuICAgICAgICBjb25zdCBoYW5kbGUgPSBoYW5kbGVzLmZpbmQoKGgpID0+IGguZ2V0QXR0cmlidXRlKFwiZGF0YS1pZFwiKSA9PT0gaWQpO1xyXG4gICAgICAgIGlmIChoYW5kbGUpIHtcclxuICAgICAgICAgICAgY29uc3Qge2Rpc3RyaWJ1dG9yfSA9IHRoaXMuX2NyZWF0ZVNpemVyQW5kRGlzdHJpYnV0b3IoaGFuZGxlKTtcclxuICAgICAgICAgICAgcmV0dXJuIGRpc3RyaWJ1dG9yO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpc1JldmVyc2VSZXNpemVIYW5kbGUoZWwpIHtcclxuICAgICAgICByZXR1cm4gZWwgJiYgZWwuY2xhc3NMaXN0LmNvbnRhaW5zKHRoaXMuY2xhc3NOYW1lcy5yZXZlcnNlKTtcclxuICAgIH1cclxuXHJcbiAgICBpc1Jlc2l6ZUhhbmRsZShlbCkge1xyXG4gICAgICAgIHJldHVybiBlbCAmJiBlbC5jbGFzc0xpc3QuY29udGFpbnModGhpcy5jbGFzc05hbWVzLmhhbmRsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX29uTW91c2VEb3duKGV2ZW50KSB7XHJcbiAgICAgICAgLy8gdXNlIGNsb3Nlc3QgaW4gY2FzZSB0aGUgcmVzaXplIGhhbmRsZSBjb250YWluc1xyXG4gICAgICAgIC8vIGNoaWxkIGRvbSBub2RlcyB0aGF0IGNhbiBiZSB0aGUgdGFyZ2V0XHJcbiAgICAgICAgY29uc3QgcmVzaXplSGFuZGxlID0gZXZlbnQudGFyZ2V0ICYmIGV2ZW50LnRhcmdldC5jbG9zZXN0KGAuJHt0aGlzLmNsYXNzTmFtZXMuaGFuZGxlfWApO1xyXG4gICAgICAgIGlmICghcmVzaXplSGFuZGxlIHx8IHJlc2l6ZUhhbmRsZS5wYXJlbnRFbGVtZW50ICE9PSB0aGlzLmNvbnRhaW5lcikge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHByZXZlbnQgc3RhcnRpbmcgYSBkcmFnIG9wZXJhdGlvblxyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIC8vIG1hcmsgYXMgY3VycmVudGx5IHJlc2l6aW5nXHJcbiAgICAgICAgaWYgKHRoaXMuY2xhc3NOYW1lcy5yZXNpemluZykge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKHRoaXMuY2xhc3NOYW1lcy5yZXNpemluZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB7c2l6ZXIsIGRpc3RyaWJ1dG9yfSA9IHRoaXMuX2NyZWF0ZVNpemVyQW5kRGlzdHJpYnV0b3IocmVzaXplSGFuZGxlKTtcclxuICAgICAgICBkaXN0cmlidXRvci5zdGFydCgpO1xyXG5cclxuICAgICAgICBjb25zdCBvbk1vdXNlTW92ZSA9IChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBvZmZzZXQgPSBzaXplci5vZmZzZXRGcm9tRXZlbnQoZXZlbnQpO1xyXG4gICAgICAgICAgICBkaXN0cmlidXRvci5yZXNpemVGcm9tQ29udGFpbmVyT2Zmc2V0KG9mZnNldCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgYm9keSA9IGRvY3VtZW50LmJvZHk7XHJcbiAgICAgICAgY29uc3QgZmluaXNoUmVzaXplID0gKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jbGFzc05hbWVzLnJlc2l6aW5nKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3QucmVtb3ZlKHRoaXMuY2xhc3NOYW1lcy5yZXNpemluZyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZGlzdHJpYnV0b3IuZmluaXNoKCk7XHJcbiAgICAgICAgICAgIGJvZHkucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm1vdXNldXBcIiwgZmluaXNoUmVzaXplLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIGZpbmlzaFJlc2l6ZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICBib2R5LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgb25Nb3VzZU1vdmUsIGZhbHNlKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGJvZHkuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNldXBcIiwgZmluaXNoUmVzaXplLCBmYWxzZSk7XHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbGVhdmVcIiwgZmluaXNoUmVzaXplLCBmYWxzZSk7XHJcbiAgICAgICAgYm9keS5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIG9uTW91c2VNb3ZlLCBmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgX2NyZWF0ZVNpemVyQW5kRGlzdHJpYnV0b3IocmVzaXplSGFuZGxlKSB7XHJcbiAgICAgICAgY29uc3QgdmVydGljYWwgPSByZXNpemVIYW5kbGUuY2xhc3NMaXN0LmNvbnRhaW5zKHRoaXMuY2xhc3NOYW1lcy52ZXJ0aWNhbCk7XHJcbiAgICAgICAgY29uc3QgcmV2ZXJzZSA9IHRoaXMuaXNSZXZlcnNlUmVzaXplSGFuZGxlKHJlc2l6ZUhhbmRsZSk7XHJcbiAgICAgICAgY29uc3QgRGlzdHJpYnV0b3IgPSB0aGlzLmRpc3RyaWJ1dG9yQ3RvcjtcclxuICAgICAgICBjb25zdCBzaXplciA9IERpc3RyaWJ1dG9yLmNyZWF0ZVNpemVyKHRoaXMuY29udGFpbmVyLCB2ZXJ0aWNhbCwgcmV2ZXJzZSk7XHJcbiAgICAgICAgY29uc3QgaXRlbSA9IERpc3RyaWJ1dG9yLmNyZWF0ZUl0ZW0ocmVzaXplSGFuZGxlLCB0aGlzLCBzaXplcik7XHJcbiAgICAgICAgY29uc3QgZGlzdHJpYnV0b3IgPSBuZXcgRGlzdHJpYnV0b3IoaXRlbSwgdGhpcy5jb25maWcpO1xyXG4gICAgICAgIHJldHVybiB7c2l6ZXIsIGRpc3RyaWJ1dG9yfTtcclxuICAgIH1cclxuXHJcbiAgICBfZ2V0UmVzaXplSGFuZGxlcygpIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkuZnJvbSh0aGlzLmNvbnRhaW5lci5jaGlsZHJlbikuZmlsdGVyKGVsID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaXNSZXNpemVIYW5kbGUoZWwpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==