/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
'use strict';

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unicodeToShortcode = unicodeToShortcode;
exports.shortcodeToUnicode = shortcodeToUnicode;
exports.processHtmlForSending = processHtmlForSending;
exports.sanitizedHtmlNode = sanitizedHtmlNode;
exports.isUrlPermitted = isUrlPermitted;
exports.bodyToHtml = bodyToHtml;
exports.linkifyString = linkifyString;
exports.linkifyElement = linkifyElement;
exports.linkifyAndSanitizeHtml = linkifyAndSanitizeHtml;
exports.checkBlockNode = checkBlockNode;

var _ReplyThread = _interopRequireDefault(require("./components/views/elements/ReplyThread"));

var _react = _interopRequireDefault(require("react"));

var _sanitizeHtml = _interopRequireDefault(require("sanitize-html"));

var linkify = _interopRequireWildcard(require("linkifyjs"));

var _linkifyMatrix = _interopRequireDefault(require("./linkify-matrix"));

var _element = _interopRequireDefault(require("linkifyjs/element"));

var _string = _interopRequireDefault(require("linkifyjs/string"));

var _classnames = _interopRequireDefault(require("classnames"));

var _MatrixClientPeg = require("./MatrixClientPeg");

var _url = _interopRequireDefault(require("url"));

var _emojibaseRegex = _interopRequireDefault(require("emojibase-regex"));

var _Permalinks = require("./utils/permalinks/Permalinks");

var _emoji = require("./emoji");

(0, _linkifyMatrix.default)(linkify); // Anything outside the basic multilingual plane will be a surrogate pair

const SURROGATE_PAIR_PATTERN = /([\ud800-\udbff])([\udc00-\udfff])/; // And there a bunch more symbol characters that emojibase has within the
// BMP, so this includes the ranges from 'letterlike symbols' to
// 'miscellaneous symbols and arrows' which should catch all of them
// (with plenty of false positives, but that's OK)

const SYMBOL_PATTERN = /([\u2100-\u2bff])/; // Regex pattern for Zero-Width joiner unicode characters

const ZWJ_REGEX = new RegExp("\u200D|\u2003", "g"); // Regex pattern for whitespace characters

const WHITESPACE_REGEX = new RegExp("\\s", "g");
const BIGEMOJI_REGEX = new RegExp("^(".concat(_emojibaseRegex.default.source, ")+$"), 'i');
const COLOR_REGEX = /^#[0-9a-fA-F]{6}$/;
const PERMITTED_URL_SCHEMES = ['http', 'https', 'ftp', 'mailto', 'magnet'];
/*
 * Return true if the given string contains emoji
 * Uses a much, much simpler regex than emojibase's so will give false
 * positives, but useful for fast-path testing strings to see if they
 * need emojification.
 * unicodeToImage uses this function.
 */

function mightContainEmoji(str) {
  return SURROGATE_PAIR_PATTERN.test(str) || SYMBOL_PATTERN.test(str);
}
/**
 * Returns the shortcode for an emoji character.
 *
 * @param {String} char The emoji character
 * @return {String} The shortcode (such as :thumbup:)
 */


function unicodeToShortcode(char) {
  const data = (0, _emoji.getEmojiFromUnicode)(char);
  return data && data.shortcodes ? ":".concat(data.shortcodes[0], ":") : '';
}
/**
 * Returns the unicode character for an emoji shortcode
 *
 * @param {String} shortcode The shortcode (such as :thumbup:)
 * @return {String} The emoji character; null if none exists
 */


function shortcodeToUnicode(shortcode) {
  shortcode = shortcode.slice(1, shortcode.length - 1);

  const data = _emoji.SHORTCODE_TO_EMOJI.get(shortcode);

  return data ? data.unicode : null;
}

function processHtmlForSending(html
/*: string*/
)
/*: string*/
{
  const contentDiv = document.createElement('div');
  contentDiv.innerHTML = html;

  if (contentDiv.children.length === 0) {
    return contentDiv.innerHTML;
  }

  let contentHTML = "";

  for (let i = 0; i < contentDiv.children.length; i++) {
    const element = contentDiv.children[i];

    if (element.tagName.toLowerCase() === 'p') {
      contentHTML += element.innerHTML; // Don't add a <br /> for the last <p>

      if (i !== contentDiv.children.length - 1) {
        contentHTML += '<br />';
      }
    } else {
      const temp = document.createElement('div');
      temp.appendChild(element.cloneNode(true));
      contentHTML += temp.innerHTML;
    }
  }

  return contentHTML;
}
/*
 * Given an untrusted HTML string, return a React node with an sanitized version
 * of that HTML.
 */


function sanitizedHtmlNode(insaneHtml) {
  const saneHtml = (0, _sanitizeHtml.default)(insaneHtml, sanitizeHtmlParams);
  return _react.default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: saneHtml
    },
    dir: "auto"
  });
}
/**
 * Tests if a URL from an untrusted source may be safely put into the DOM
 * The biggest threat here is javascript: URIs.
 * Note that the HTML sanitiser library has its own internal logic for
 * doing this, to which we pass the same list of schemes. This is used in
 * other places we need to sanitise URLs.
 * @return true if permitted, otherwise false
 */


function isUrlPermitted(inputUrl) {
  try {
    const parsed = _url.default.parse(inputUrl);

    if (!parsed.protocol) return false; // URL parser protocol includes the trailing colon

    return PERMITTED_URL_SCHEMES.includes(parsed.protocol.slice(0, -1));
  } catch (e) {
    return false;
  }
}

const transformTags = {
  // custom to matrix
  // add blank targets to all hyperlinks except vector URLs
  'a': function (tagName, attribs) {
    if (attribs.href) {
      attribs.target = '_blank'; // by default

      const transformed = (0, _Permalinks.tryTransformPermalinkToLocalHref)(attribs.href);

      if (transformed !== attribs.href || attribs.href.match(_linkifyMatrix.default.VECTOR_URL_PATTERN)) {
        attribs.href = transformed;
        delete attribs.target;
      }
    }

    attribs.rel = 'noreferrer noopener'; // https://mathiasbynens.github.io/rel-noopener/

    return {
      tagName,
      attribs
    };
  },
  'img': function (tagName, attribs) {
    // Strip out imgs that aren't `mxc` here instead of using allowedSchemesByTag
    // because transformTags is used _before_ we filter by allowedSchemesByTag and
    // we don't want to allow images with `https?` `src`s.
    if (!attribs.src || !attribs.src.startsWith('mxc://')) {
      return {
        tagName,
        attribs: {}
      };
    }

    attribs.src = _MatrixClientPeg.MatrixClientPeg.get().mxcUrlToHttp(attribs.src, attribs.width || 800, attribs.height || 600);
    return {
      tagName,
      attribs
    };
  },
  'code': function (tagName, attribs) {
    if (typeof attribs.class !== 'undefined') {
      // Filter out all classes other than ones starting with language- for syntax highlighting.
      const classes = attribs.class.split(/\s/).filter(function (cl) {
        return cl.startsWith('language-');
      });
      attribs.class = classes.join(' ');
    }

    return {
      tagName,
      attribs
    };
  },
  '*': function (tagName, attribs) {
    // Delete any style previously assigned, style is an allowedTag for font and span
    // because attributes are stripped after transforming
    delete attribs.style; // Sanitise and transform data-mx-color and data-mx-bg-color to their CSS
    // equivalents

    const customCSSMapper = {
      'data-mx-color': 'color',
      'data-mx-bg-color': 'background-color' // $customAttributeKey: $cssAttributeKey

    };
    let style = "";
    Object.keys(customCSSMapper).forEach(customAttributeKey => {
      const cssAttributeKey = customCSSMapper[customAttributeKey];
      const customAttributeValue = attribs[customAttributeKey];

      if (customAttributeValue && typeof customAttributeValue === 'string' && COLOR_REGEX.test(customAttributeValue)) {
        style += cssAttributeKey + ":" + customAttributeValue + ";";
        delete attribs[customAttributeKey];
      }
    });

    if (style) {
      attribs.style = style;
    }

    return {
      tagName,
      attribs
    };
  }
};
const sanitizeHtmlParams = {
  allowedTags: ['font', // custom to matrix for IRC-style font coloring
  'del', // for markdown
  'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol', 'sup', 'sub', 'nl', 'li', 'b', 'i', 'u', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div', 'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'span', 'img'],
  allowedAttributes: {
    // custom ones first:
    font: ['color', 'data-mx-bg-color', 'data-mx-color', 'style'],
    // custom to matrix
    span: ['data-mx-bg-color', 'data-mx-color', 'data-mx-spoiler', 'style'],
    // custom to matrix
    a: ['href', 'name', 'target', 'rel'],
    // remote target: custom to matrix
    img: ['src', 'width', 'height', 'alt', 'title'],
    ol: ['start'],
    code: ['class'] // We don't actually allow all classes, we filter them in transformTags

  },
  // Lots of these won't come up by default because we don't allow them
  selfClosing: ['img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta'],
  // URL schemes we permit
  allowedSchemes: PERMITTED_URL_SCHEMES,
  allowProtocolRelative: false,
  transformTags
}; // this is the same as the above except with less rewriting

const composerSanitizeHtmlParams = Object.assign({}, sanitizeHtmlParams);
composerSanitizeHtmlParams.transformTags = {
  'code': transformTags['code'],
  '*': transformTags['*']
};

class BaseHighlighter {
  constructor(highlightClass, highlightLink) {
    this.highlightClass = highlightClass;
    this.highlightLink = highlightLink;
  }
  /**
   * apply the highlights to a section of text
   *
   * @param {string} safeSnippet The snippet of text to apply the highlights
   *     to.
   * @param {string[]} safeHighlights A list of substrings to highlight,
   *     sorted by descending length.
   *
   * returns a list of results (strings for HtmlHighligher, react nodes for
   * TextHighlighter).
   */


  applyHighlights(safeSnippet, safeHighlights) {
    let lastOffset = 0;
    let offset;
    let nodes = [];
    const safeHighlight = safeHighlights[0];

    while ((offset = safeSnippet.toLowerCase().indexOf(safeHighlight.toLowerCase(), lastOffset)) >= 0) {
      // handle preamble
      if (offset > lastOffset) {
        var subSnippet = safeSnippet.substring(lastOffset, offset);
        nodes = nodes.concat(this._applySubHighlights(subSnippet, safeHighlights));
      } // do highlight. use the original string rather than safeHighlight
      // to preserve the original casing.


      const endOffset = offset + safeHighlight.length;
      nodes.push(this._processSnippet(safeSnippet.substring(offset, endOffset), true));
      lastOffset = endOffset;
    } // handle postamble


    if (lastOffset !== safeSnippet.length) {
      subSnippet = safeSnippet.substring(lastOffset, undefined);
      nodes = nodes.concat(this._applySubHighlights(subSnippet, safeHighlights));
    }

    return nodes;
  }

  _applySubHighlights(safeSnippet, safeHighlights) {
    if (safeHighlights[1]) {
      // recurse into this range to check for the next set of highlight matches
      return this.applyHighlights(safeSnippet, safeHighlights.slice(1));
    } else {
      // no more highlights to be found, just return the unhighlighted string
      return [this._processSnippet(safeSnippet, false)];
    }
  }

}

class HtmlHighlighter extends BaseHighlighter {
  /* highlight the given snippet if required
   *
   * snippet: content of the span; must have been sanitised
   * highlight: true to highlight as a search match
   *
   * returns an HTML string
   */
  _processSnippet(snippet, highlight) {
    if (!highlight) {
      // nothing required here
      return snippet;
    }

    let span = "<span class=\"" + this.highlightClass + "\">" + snippet + "</span>";

    if (this.highlightLink) {
      span = "<a href=\"" + encodeURI(this.highlightLink) + "\">" + span + "</a>";
    }

    return span;
  }

}

class TextHighlighter extends BaseHighlighter {
  constructor(highlightClass, highlightLink) {
    super(highlightClass, highlightLink);
    this._key = 0;
  }
  /* create a <span> node to hold the given content
   *
   * snippet: content of the span
   * highlight: true to highlight as a search match
   *
   * returns a React node
   */


  _processSnippet(snippet, highlight) {
    const key = this._key++;

    let node = _react.default.createElement("span", {
      key: key,
      className: highlight ? this.highlightClass : null
    }, snippet);

    if (highlight && this.highlightLink) {
      node = _react.default.createElement("a", {
        key: key,
        href: this.highlightLink
      }, node);
    }

    return node;
  }

}
/* turn a matrix event body into html
 *
 * content: 'content' of the MatrixEvent
 *
 * highlights: optional list of words to highlight, ordered by longest word first
 *
 * opts.highlightLink: optional href to add to highlighted words
 * opts.disableBigEmoji: optional argument to disable the big emoji class.
 * opts.stripReplyFallback: optional argument specifying the event is a reply and so fallback needs removing
 * opts.returnString: return an HTML string rather than JSX elements
 * opts.forComposerQuote: optional param to lessen the url rewriting done by sanitization, for quoting into composer
 * opts.ref: React ref to attach to any React components returned (not compatible with opts.returnString)
 */


function bodyToHtml(content, highlights, opts = {}) {
  const isHtmlMessage = content.format === "org.matrix.custom.html" && content.formatted_body;
  let bodyHasEmoji = false;
  let sanitizeParams = sanitizeHtmlParams;

  if (opts.forComposerQuote) {
    sanitizeParams = composerSanitizeHtmlParams;
  }

  let strippedBody;
  let safeBody;
  let isDisplayedWithHtml; // XXX: We sanitize the HTML whilst also highlighting its text nodes, to avoid accidentally trying
  // to highlight HTML tags themselves.  However, this does mean that we don't highlight textnodes which
  // are interrupted by HTML tags (not that we did before) - e.g. foo<span/>bar won't get highlighted
  // by an attempt to search for 'foobar'.  Then again, the search query probably wouldn't work either

  try {
    if (highlights && highlights.length > 0) {
      const highlighter = new HtmlHighlighter("mx_EventTile_searchHighlight", opts.highlightLink);
      const safeHighlights = highlights.map(function (highlight) {
        return (0, _sanitizeHtml.default)(highlight, sanitizeParams);
      }); // XXX: hacky bodge to temporarily apply a textFilter to the sanitizeParams structure.

      sanitizeParams.textFilter = function (safeText) {
        return highlighter.applyHighlights(safeText, safeHighlights).join('');
      };
    }

    let formattedBody = typeof content.formatted_body === 'string' ? content.formatted_body : null;
    const plainBody = typeof content.body === 'string' ? content.body : null;
    if (opts.stripReplyFallback && formattedBody) formattedBody = _ReplyThread.default.stripHTMLReply(formattedBody);
    strippedBody = opts.stripReplyFallback ? _ReplyThread.default.stripPlainReply(plainBody) : plainBody;
    bodyHasEmoji = mightContainEmoji(isHtmlMessage ? formattedBody : plainBody); // Only generate safeBody if the message was sent as org.matrix.custom.html

    if (isHtmlMessage) {
      isDisplayedWithHtml = true;
      safeBody = (0, _sanitizeHtml.default)(formattedBody, sanitizeParams);
    }
  } finally {
    delete sanitizeParams.textFilter;
  }

  if (opts.returnString) {
    return isDisplayedWithHtml ? safeBody : strippedBody;
  }

  let emojiBody = false;

  if (!opts.disableBigEmoji && bodyHasEmoji) {
    let contentBodyTrimmed = strippedBody !== undefined ? strippedBody.trim() : ''; // Ignore spaces in body text. Emojis with spaces in between should
    // still be counted as purely emoji messages.

    contentBodyTrimmed = contentBodyTrimmed.replace(WHITESPACE_REGEX, ''); // Remove zero width joiner characters from emoji messages. This ensures
    // that emojis that are made up of multiple unicode characters are still
    // presented as large.

    contentBodyTrimmed = contentBodyTrimmed.replace(ZWJ_REGEX, '');
    const match = BIGEMOJI_REGEX.exec(contentBodyTrimmed);
    emojiBody = match && match[0] && match[0].length === contentBodyTrimmed.length && ( // Prevent user pills expanding for users with only emoji in
    // their username. Permalinks (links in pills) can be any URL
    // now, so we just check for an HTTP-looking thing.
    content.formatted_body == undefined || !content.formatted_body.includes("http:") && !content.formatted_body.includes("https:"));
  }

  const className = (0, _classnames.default)({
    'mx_EventTile_body': true,
    'mx_EventTile_bigEmoji': emojiBody,
    'markdown-body': isHtmlMessage && !emojiBody
  });
  return isDisplayedWithHtml ? _react.default.createElement("span", {
    key: "body",
    ref: opts.ref,
    className: className,
    dangerouslySetInnerHTML: {
      __html: safeBody
    },
    dir: "auto"
  }) : _react.default.createElement("span", {
    key: "body",
    ref: opts.ref,
    className: className,
    dir: "auto"
  }, strippedBody);
}
/**
 * Linkifies the given string. This is a wrapper around 'linkifyjs/string'.
 *
 * @param {string} str string to linkify
 * @param {object} [options] Options for linkifyString. Default: linkifyMatrix.options
 * @returns {string} Linkified string
 */


function linkifyString(str, options = _linkifyMatrix.default.options) {
  return (0, _string.default)(str, options);
}
/**
 * Linkifies the given DOM element. This is a wrapper around 'linkifyjs/element'.
 *
 * @param {object} element DOM element to linkify
 * @param {object} [options] Options for linkifyElement. Default: linkifyMatrix.options
 * @returns {object}
 */


function linkifyElement(element, options = _linkifyMatrix.default.options) {
  return (0, _element.default)(element, options);
}
/**
 * Linkify the given string and sanitize the HTML afterwards.
 *
 * @param {string} dirtyHtml The HTML string to sanitize and linkify
 * @param {object} [options] Options for linkifyString. Default: linkifyMatrix.options
 * @returns {string}
 */


function linkifyAndSanitizeHtml(dirtyHtml, options = _linkifyMatrix.default.options) {
  return (0, _sanitizeHtml.default)(linkifyString(dirtyHtml, options), sanitizeHtmlParams);
}
/**
 * Returns if a node is a block element or not.
 * Only takes html nodes into account that are allowed in matrix messages.
 *
 * @param {Node} node
 * @returns {bool}
 */


function checkBlockNode(node) {
  switch (node.nodeName) {
    case "H1":
    case "H2":
    case "H3":
    case "H4":
    case "H5":
    case "H6":
    case "PRE":
    case "BLOCKQUOTE":
    case "DIV":
    case "P":
    case "UL":
    case "OL":
    case "LI":
    case "HR":
    case "TABLE":
    case "THEAD":
    case "TBODY":
    case "TR":
    case "TH":
    case "TD":
      return true;

    default:
      return false;
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9IdG1sVXRpbHMuanMiXSwibmFtZXMiOlsibGlua2lmeSIsIlNVUlJPR0FURV9QQUlSX1BBVFRFUk4iLCJTWU1CT0xfUEFUVEVSTiIsIlpXSl9SRUdFWCIsIlJlZ0V4cCIsIldISVRFU1BBQ0VfUkVHRVgiLCJCSUdFTU9KSV9SRUdFWCIsIkVNT0pJQkFTRV9SRUdFWCIsInNvdXJjZSIsIkNPTE9SX1JFR0VYIiwiUEVSTUlUVEVEX1VSTF9TQ0hFTUVTIiwibWlnaHRDb250YWluRW1vamkiLCJzdHIiLCJ0ZXN0IiwidW5pY29kZVRvU2hvcnRjb2RlIiwiY2hhciIsImRhdGEiLCJzaG9ydGNvZGVzIiwic2hvcnRjb2RlVG9Vbmljb2RlIiwic2hvcnRjb2RlIiwic2xpY2UiLCJsZW5ndGgiLCJTSE9SVENPREVfVE9fRU1PSkkiLCJnZXQiLCJ1bmljb2RlIiwicHJvY2Vzc0h0bWxGb3JTZW5kaW5nIiwiaHRtbCIsImNvbnRlbnREaXYiLCJkb2N1bWVudCIsImNyZWF0ZUVsZW1lbnQiLCJpbm5lckhUTUwiLCJjaGlsZHJlbiIsImNvbnRlbnRIVE1MIiwiaSIsImVsZW1lbnQiLCJ0YWdOYW1lIiwidG9Mb3dlckNhc2UiLCJ0ZW1wIiwiYXBwZW5kQ2hpbGQiLCJjbG9uZU5vZGUiLCJzYW5pdGl6ZWRIdG1sTm9kZSIsImluc2FuZUh0bWwiLCJzYW5lSHRtbCIsInNhbml0aXplSHRtbFBhcmFtcyIsIl9faHRtbCIsImlzVXJsUGVybWl0dGVkIiwiaW5wdXRVcmwiLCJwYXJzZWQiLCJ1cmwiLCJwYXJzZSIsInByb3RvY29sIiwiaW5jbHVkZXMiLCJlIiwidHJhbnNmb3JtVGFncyIsImF0dHJpYnMiLCJocmVmIiwidGFyZ2V0IiwidHJhbnNmb3JtZWQiLCJtYXRjaCIsImxpbmtpZnlNYXRyaXgiLCJWRUNUT1JfVVJMX1BBVFRFUk4iLCJyZWwiLCJzcmMiLCJzdGFydHNXaXRoIiwiTWF0cml4Q2xpZW50UGVnIiwibXhjVXJsVG9IdHRwIiwid2lkdGgiLCJoZWlnaHQiLCJjbGFzcyIsImNsYXNzZXMiLCJzcGxpdCIsImZpbHRlciIsImNsIiwiam9pbiIsInN0eWxlIiwiY3VzdG9tQ1NTTWFwcGVyIiwiT2JqZWN0Iiwia2V5cyIsImZvckVhY2giLCJjdXN0b21BdHRyaWJ1dGVLZXkiLCJjc3NBdHRyaWJ1dGVLZXkiLCJjdXN0b21BdHRyaWJ1dGVWYWx1ZSIsImFsbG93ZWRUYWdzIiwiYWxsb3dlZEF0dHJpYnV0ZXMiLCJmb250Iiwic3BhbiIsImEiLCJpbWciLCJvbCIsImNvZGUiLCJzZWxmQ2xvc2luZyIsImFsbG93ZWRTY2hlbWVzIiwiYWxsb3dQcm90b2NvbFJlbGF0aXZlIiwiY29tcG9zZXJTYW5pdGl6ZUh0bWxQYXJhbXMiLCJhc3NpZ24iLCJCYXNlSGlnaGxpZ2h0ZXIiLCJjb25zdHJ1Y3RvciIsImhpZ2hsaWdodENsYXNzIiwiaGlnaGxpZ2h0TGluayIsImFwcGx5SGlnaGxpZ2h0cyIsInNhZmVTbmlwcGV0Iiwic2FmZUhpZ2hsaWdodHMiLCJsYXN0T2Zmc2V0Iiwib2Zmc2V0Iiwibm9kZXMiLCJzYWZlSGlnaGxpZ2h0IiwiaW5kZXhPZiIsInN1YlNuaXBwZXQiLCJzdWJzdHJpbmciLCJjb25jYXQiLCJfYXBwbHlTdWJIaWdobGlnaHRzIiwiZW5kT2Zmc2V0IiwicHVzaCIsIl9wcm9jZXNzU25pcHBldCIsInVuZGVmaW5lZCIsIkh0bWxIaWdobGlnaHRlciIsInNuaXBwZXQiLCJoaWdobGlnaHQiLCJlbmNvZGVVUkkiLCJUZXh0SGlnaGxpZ2h0ZXIiLCJfa2V5Iiwia2V5Iiwibm9kZSIsImJvZHlUb0h0bWwiLCJjb250ZW50IiwiaGlnaGxpZ2h0cyIsIm9wdHMiLCJpc0h0bWxNZXNzYWdlIiwiZm9ybWF0IiwiZm9ybWF0dGVkX2JvZHkiLCJib2R5SGFzRW1vamkiLCJzYW5pdGl6ZVBhcmFtcyIsImZvckNvbXBvc2VyUXVvdGUiLCJzdHJpcHBlZEJvZHkiLCJzYWZlQm9keSIsImlzRGlzcGxheWVkV2l0aEh0bWwiLCJoaWdobGlnaHRlciIsIm1hcCIsInRleHRGaWx0ZXIiLCJzYWZlVGV4dCIsImZvcm1hdHRlZEJvZHkiLCJwbGFpbkJvZHkiLCJib2R5Iiwic3RyaXBSZXBseUZhbGxiYWNrIiwiUmVwbHlUaHJlYWQiLCJzdHJpcEhUTUxSZXBseSIsInN0cmlwUGxhaW5SZXBseSIsInJldHVyblN0cmluZyIsImVtb2ppQm9keSIsImRpc2FibGVCaWdFbW9qaSIsImNvbnRlbnRCb2R5VHJpbW1lZCIsInRyaW0iLCJyZXBsYWNlIiwiZXhlYyIsImNsYXNzTmFtZSIsInJlZiIsImxpbmtpZnlTdHJpbmciLCJvcHRpb25zIiwibGlua2lmeUVsZW1lbnQiLCJsaW5raWZ5QW5kU2FuaXRpemVIdG1sIiwiZGlydHlIdG1sIiwiY2hlY2tCbG9ja05vZGUiLCJub2RlTmFtZSJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFFQSw0QkFBY0EsT0FBZCxFLENBRUE7O0FBQ0EsTUFBTUMsc0JBQXNCLEdBQUcsb0NBQS9CLEMsQ0FDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxNQUFNQyxjQUFjLEdBQUcsbUJBQXZCLEMsQ0FFQTs7QUFDQSxNQUFNQyxTQUFTLEdBQUcsSUFBSUMsTUFBSixDQUFXLGVBQVgsRUFBNEIsR0FBNUIsQ0FBbEIsQyxDQUVBOztBQUNBLE1BQU1DLGdCQUFnQixHQUFHLElBQUlELE1BQUosQ0FBVyxLQUFYLEVBQWtCLEdBQWxCLENBQXpCO0FBRUEsTUFBTUUsY0FBYyxHQUFHLElBQUlGLE1BQUosYUFBZ0JHLHdCQUFnQkMsTUFBaEMsVUFBNkMsR0FBN0MsQ0FBdkI7QUFFQSxNQUFNQyxXQUFXLEdBQUcsbUJBQXBCO0FBRUEsTUFBTUMscUJBQXFCLEdBQUcsQ0FBQyxNQUFELEVBQVMsT0FBVCxFQUFrQixLQUFsQixFQUF5QixRQUF6QixFQUFtQyxRQUFuQyxDQUE5QjtBQUVBOzs7Ozs7OztBQU9BLFNBQVNDLGlCQUFULENBQTJCQyxHQUEzQixFQUFnQztBQUM1QixTQUFPWCxzQkFBc0IsQ0FBQ1ksSUFBdkIsQ0FBNEJELEdBQTVCLEtBQW9DVixjQUFjLENBQUNXLElBQWYsQ0FBb0JELEdBQXBCLENBQTNDO0FBQ0g7QUFFRDs7Ozs7Ozs7QUFNTyxTQUFTRSxrQkFBVCxDQUE0QkMsSUFBNUIsRUFBa0M7QUFDckMsUUFBTUMsSUFBSSxHQUFHLGdDQUFvQkQsSUFBcEIsQ0FBYjtBQUNBLFNBQVFDLElBQUksSUFBSUEsSUFBSSxDQUFDQyxVQUFiLGNBQThCRCxJQUFJLENBQUNDLFVBQUwsQ0FBZ0IsQ0FBaEIsQ0FBOUIsU0FBc0QsRUFBOUQ7QUFDSDtBQUVEOzs7Ozs7OztBQU1PLFNBQVNDLGtCQUFULENBQTRCQyxTQUE1QixFQUF1QztBQUMxQ0EsRUFBQUEsU0FBUyxHQUFHQSxTQUFTLENBQUNDLEtBQVYsQ0FBZ0IsQ0FBaEIsRUFBbUJELFNBQVMsQ0FBQ0UsTUFBVixHQUFtQixDQUF0QyxDQUFaOztBQUNBLFFBQU1MLElBQUksR0FBR00sMEJBQW1CQyxHQUFuQixDQUF1QkosU0FBdkIsQ0FBYjs7QUFDQSxTQUFPSCxJQUFJLEdBQUdBLElBQUksQ0FBQ1EsT0FBUixHQUFrQixJQUE3QjtBQUNIOztBQUVNLFNBQVNDLHFCQUFULENBQStCQztBQUEvQjtBQUFBO0FBQUE7QUFBcUQ7QUFDeEQsUUFBTUMsVUFBVSxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBbkI7QUFDQUYsRUFBQUEsVUFBVSxDQUFDRyxTQUFYLEdBQXVCSixJQUF2Qjs7QUFFQSxNQUFJQyxVQUFVLENBQUNJLFFBQVgsQ0FBb0JWLE1BQXBCLEtBQStCLENBQW5DLEVBQXNDO0FBQ2xDLFdBQU9NLFVBQVUsQ0FBQ0csU0FBbEI7QUFDSDs7QUFFRCxNQUFJRSxXQUFXLEdBQUcsRUFBbEI7O0FBQ0EsT0FBSyxJQUFJQyxDQUFDLEdBQUMsQ0FBWCxFQUFjQSxDQUFDLEdBQUdOLFVBQVUsQ0FBQ0ksUUFBWCxDQUFvQlYsTUFBdEMsRUFBOENZLENBQUMsRUFBL0MsRUFBbUQ7QUFDL0MsVUFBTUMsT0FBTyxHQUFHUCxVQUFVLENBQUNJLFFBQVgsQ0FBb0JFLENBQXBCLENBQWhCOztBQUNBLFFBQUlDLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsV0FBaEIsT0FBa0MsR0FBdEMsRUFBMkM7QUFDdkNKLE1BQUFBLFdBQVcsSUFBSUUsT0FBTyxDQUFDSixTQUF2QixDQUR1QyxDQUV2Qzs7QUFDQSxVQUFJRyxDQUFDLEtBQUtOLFVBQVUsQ0FBQ0ksUUFBWCxDQUFvQlYsTUFBcEIsR0FBNkIsQ0FBdkMsRUFBMEM7QUFDdENXLFFBQUFBLFdBQVcsSUFBSSxRQUFmO0FBQ0g7QUFDSixLQU5ELE1BTU87QUFDSCxZQUFNSyxJQUFJLEdBQUdULFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFiO0FBQ0FRLE1BQUFBLElBQUksQ0FBQ0MsV0FBTCxDQUFpQkosT0FBTyxDQUFDSyxTQUFSLENBQWtCLElBQWxCLENBQWpCO0FBQ0FQLE1BQUFBLFdBQVcsSUFBSUssSUFBSSxDQUFDUCxTQUFwQjtBQUNIO0FBQ0o7O0FBRUQsU0FBT0UsV0FBUDtBQUNIO0FBRUQ7Ozs7OztBQUlPLFNBQVNRLGlCQUFULENBQTJCQyxVQUEzQixFQUF1QztBQUMxQyxRQUFNQyxRQUFRLEdBQUcsMkJBQWFELFVBQWIsRUFBeUJFLGtCQUF6QixDQUFqQjtBQUVBLFNBQU87QUFBSyxJQUFBLHVCQUF1QixFQUFFO0FBQUVDLE1BQUFBLE1BQU0sRUFBRUY7QUFBVixLQUE5QjtBQUFvRCxJQUFBLEdBQUcsRUFBQztBQUF4RCxJQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7OztBQVFPLFNBQVNHLGNBQVQsQ0FBd0JDLFFBQXhCLEVBQWtDO0FBQ3JDLE1BQUk7QUFDQSxVQUFNQyxNQUFNLEdBQUdDLGFBQUlDLEtBQUosQ0FBVUgsUUFBVixDQUFmOztBQUNBLFFBQUksQ0FBQ0MsTUFBTSxDQUFDRyxRQUFaLEVBQXNCLE9BQU8sS0FBUCxDQUZ0QixDQUdBOztBQUNBLFdBQU94QyxxQkFBcUIsQ0FBQ3lDLFFBQXRCLENBQStCSixNQUFNLENBQUNHLFFBQVAsQ0FBZ0I5QixLQUFoQixDQUFzQixDQUF0QixFQUF5QixDQUFDLENBQTFCLENBQS9CLENBQVA7QUFDSCxHQUxELENBS0UsT0FBT2dDLENBQVAsRUFBVTtBQUNSLFdBQU8sS0FBUDtBQUNIO0FBQ0o7O0FBRUQsTUFBTUMsYUFBYSxHQUFHO0FBQUU7QUFDcEI7QUFDQSxPQUFLLFVBQVNsQixPQUFULEVBQWtCbUIsT0FBbEIsRUFBMkI7QUFDNUIsUUFBSUEsT0FBTyxDQUFDQyxJQUFaLEVBQWtCO0FBQ2RELE1BQUFBLE9BQU8sQ0FBQ0UsTUFBUixHQUFpQixRQUFqQixDQURjLENBQ2E7O0FBRTNCLFlBQU1DLFdBQVcsR0FBRyxrREFBaUNILE9BQU8sQ0FBQ0MsSUFBekMsQ0FBcEI7O0FBQ0EsVUFBSUUsV0FBVyxLQUFLSCxPQUFPLENBQUNDLElBQXhCLElBQWdDRCxPQUFPLENBQUNDLElBQVIsQ0FBYUcsS0FBYixDQUFtQkMsdUJBQWNDLGtCQUFqQyxDQUFwQyxFQUEwRjtBQUN0Rk4sUUFBQUEsT0FBTyxDQUFDQyxJQUFSLEdBQWVFLFdBQWY7QUFDQSxlQUFPSCxPQUFPLENBQUNFLE1BQWY7QUFDSDtBQUNKOztBQUNERixJQUFBQSxPQUFPLENBQUNPLEdBQVIsR0FBYyxxQkFBZCxDQVY0QixDQVVTOztBQUNyQyxXQUFPO0FBQUUxQixNQUFBQSxPQUFGO0FBQVdtQixNQUFBQTtBQUFYLEtBQVA7QUFDSCxHQWRpQjtBQWVsQixTQUFPLFVBQVNuQixPQUFULEVBQWtCbUIsT0FBbEIsRUFBMkI7QUFDOUI7QUFDQTtBQUNBO0FBQ0EsUUFBSSxDQUFDQSxPQUFPLENBQUNRLEdBQVQsSUFBZ0IsQ0FBQ1IsT0FBTyxDQUFDUSxHQUFSLENBQVlDLFVBQVosQ0FBdUIsUUFBdkIsQ0FBckIsRUFBdUQ7QUFDbkQsYUFBTztBQUFFNUIsUUFBQUEsT0FBRjtBQUFXbUIsUUFBQUEsT0FBTyxFQUFFO0FBQXBCLE9BQVA7QUFDSDs7QUFDREEsSUFBQUEsT0FBTyxDQUFDUSxHQUFSLEdBQWNFLGlDQUFnQnpDLEdBQWhCLEdBQXNCMEMsWUFBdEIsQ0FDVlgsT0FBTyxDQUFDUSxHQURFLEVBRVZSLE9BQU8sQ0FBQ1ksS0FBUixJQUFpQixHQUZQLEVBR1ZaLE9BQU8sQ0FBQ2EsTUFBUixJQUFrQixHQUhSLENBQWQ7QUFLQSxXQUFPO0FBQUVoQyxNQUFBQSxPQUFGO0FBQVdtQixNQUFBQTtBQUFYLEtBQVA7QUFDSCxHQTVCaUI7QUE2QmxCLFVBQVEsVUFBU25CLE9BQVQsRUFBa0JtQixPQUFsQixFQUEyQjtBQUMvQixRQUFJLE9BQU9BLE9BQU8sQ0FBQ2MsS0FBZixLQUF5QixXQUE3QixFQUEwQztBQUN0QztBQUNBLFlBQU1DLE9BQU8sR0FBR2YsT0FBTyxDQUFDYyxLQUFSLENBQWNFLEtBQWQsQ0FBb0IsSUFBcEIsRUFBMEJDLE1BQTFCLENBQWlDLFVBQVNDLEVBQVQsRUFBYTtBQUMxRCxlQUFPQSxFQUFFLENBQUNULFVBQUgsQ0FBYyxXQUFkLENBQVA7QUFDSCxPQUZlLENBQWhCO0FBR0FULE1BQUFBLE9BQU8sQ0FBQ2MsS0FBUixHQUFnQkMsT0FBTyxDQUFDSSxJQUFSLENBQWEsR0FBYixDQUFoQjtBQUNIOztBQUNELFdBQU87QUFBRXRDLE1BQUFBLE9BQUY7QUFBV21CLE1BQUFBO0FBQVgsS0FBUDtBQUNILEdBdENpQjtBQXVDbEIsT0FBSyxVQUFTbkIsT0FBVCxFQUFrQm1CLE9BQWxCLEVBQTJCO0FBQzVCO0FBQ0E7QUFDQSxXQUFPQSxPQUFPLENBQUNvQixLQUFmLENBSDRCLENBSzVCO0FBQ0E7O0FBQ0EsVUFBTUMsZUFBZSxHQUFHO0FBQ3BCLHVCQUFpQixPQURHO0FBRXBCLDBCQUFvQixrQkFGQSxDQUdwQjs7QUFIb0IsS0FBeEI7QUFNQSxRQUFJRCxLQUFLLEdBQUcsRUFBWjtBQUNBRSxJQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWUYsZUFBWixFQUE2QkcsT0FBN0IsQ0FBc0NDLGtCQUFELElBQXdCO0FBQ3pELFlBQU1DLGVBQWUsR0FBR0wsZUFBZSxDQUFDSSxrQkFBRCxDQUF2QztBQUNBLFlBQU1FLG9CQUFvQixHQUFHM0IsT0FBTyxDQUFDeUIsa0JBQUQsQ0FBcEM7O0FBQ0EsVUFBSUUsb0JBQW9CLElBQ3BCLE9BQU9BLG9CQUFQLEtBQWdDLFFBRGhDLElBRUF4RSxXQUFXLENBQUNJLElBQVosQ0FBaUJvRSxvQkFBakIsQ0FGSixFQUdFO0FBQ0VQLFFBQUFBLEtBQUssSUFBSU0sZUFBZSxHQUFHLEdBQWxCLEdBQXdCQyxvQkFBeEIsR0FBK0MsR0FBeEQ7QUFDQSxlQUFPM0IsT0FBTyxDQUFDeUIsa0JBQUQsQ0FBZDtBQUNIO0FBQ0osS0FWRDs7QUFZQSxRQUFJTCxLQUFKLEVBQVc7QUFDUHBCLE1BQUFBLE9BQU8sQ0FBQ29CLEtBQVIsR0FBZ0JBLEtBQWhCO0FBQ0g7O0FBRUQsV0FBTztBQUFFdkMsTUFBQUEsT0FBRjtBQUFXbUIsTUFBQUE7QUFBWCxLQUFQO0FBQ0g7QUF0RWlCLENBQXRCO0FBeUVBLE1BQU1YLGtCQUFrQixHQUFHO0FBQ3ZCdUMsRUFBQUEsV0FBVyxFQUFFLENBQ1QsTUFEUyxFQUNEO0FBQ1IsT0FGUyxFQUVGO0FBQ1AsTUFIUyxFQUdILElBSEcsRUFHRyxJQUhILEVBR1MsSUFIVCxFQUdlLElBSGYsRUFHcUIsSUFIckIsRUFHMkIsWUFIM0IsRUFHeUMsR0FIekMsRUFHOEMsR0FIOUMsRUFHbUQsSUFIbkQsRUFHeUQsSUFIekQsRUFHK0QsS0FIL0QsRUFHc0UsS0FIdEUsRUFJVCxJQUpTLEVBSUgsSUFKRyxFQUlHLEdBSkgsRUFJUSxHQUpSLEVBSWEsR0FKYixFQUlrQixRQUpsQixFQUk0QixJQUo1QixFQUlrQyxRQUpsQyxFQUk0QyxNQUo1QyxFQUlvRCxJQUpwRCxFQUkwRCxJQUoxRCxFQUlnRSxLQUpoRSxFQUtULE9BTFMsRUFLQSxPQUxBLEVBS1MsU0FMVCxFQUtvQixPQUxwQixFQUs2QixJQUw3QixFQUttQyxJQUxuQyxFQUt5QyxJQUx6QyxFQUsrQyxLQUwvQyxFQUtzRCxNQUx0RCxFQUs4RCxLQUw5RCxDQURVO0FBUXZCQyxFQUFBQSxpQkFBaUIsRUFBRTtBQUNmO0FBQ0FDLElBQUFBLElBQUksRUFBRSxDQUFDLE9BQUQsRUFBVSxrQkFBVixFQUE4QixlQUE5QixFQUErQyxPQUEvQyxDQUZTO0FBRWdEO0FBQy9EQyxJQUFBQSxJQUFJLEVBQUUsQ0FBQyxrQkFBRCxFQUFxQixlQUFyQixFQUFzQyxpQkFBdEMsRUFBeUQsT0FBekQsQ0FIUztBQUcwRDtBQUN6RUMsSUFBQUEsQ0FBQyxFQUFFLENBQUMsTUFBRCxFQUFTLE1BQVQsRUFBaUIsUUFBakIsRUFBMkIsS0FBM0IsQ0FKWTtBQUl1QjtBQUN0Q0MsSUFBQUEsR0FBRyxFQUFFLENBQUMsS0FBRCxFQUFRLE9BQVIsRUFBaUIsUUFBakIsRUFBMkIsS0FBM0IsRUFBa0MsT0FBbEMsQ0FMVTtBQU1mQyxJQUFBQSxFQUFFLEVBQUUsQ0FBQyxPQUFELENBTlc7QUFPZkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsT0FBRCxDQVBTLENBT0U7O0FBUEYsR0FSSTtBQWlCdkI7QUFDQUMsRUFBQUEsV0FBVyxFQUFFLENBQUMsS0FBRCxFQUFRLElBQVIsRUFBYyxJQUFkLEVBQW9CLE1BQXBCLEVBQTRCLE1BQTVCLEVBQW9DLFVBQXBDLEVBQWdELE9BQWhELEVBQXlELE1BQXpELEVBQWlFLE1BQWpFLENBbEJVO0FBbUJ2QjtBQUNBQyxFQUFBQSxjQUFjLEVBQUVqRixxQkFwQk87QUFzQnZCa0YsRUFBQUEscUJBQXFCLEVBQUUsS0F0QkE7QUF1QnZCdkMsRUFBQUE7QUF2QnVCLENBQTNCLEMsQ0EwQkE7O0FBQ0EsTUFBTXdDLDBCQUEwQixHQUFHakIsTUFBTSxDQUFDa0IsTUFBUCxDQUFjLEVBQWQsRUFBa0JuRCxrQkFBbEIsQ0FBbkM7QUFDQWtELDBCQUEwQixDQUFDeEMsYUFBM0IsR0FBMkM7QUFDdkMsVUFBUUEsYUFBYSxDQUFDLE1BQUQsQ0FEa0I7QUFFdkMsT0FBS0EsYUFBYSxDQUFDLEdBQUQ7QUFGcUIsQ0FBM0M7O0FBS0EsTUFBTTBDLGVBQU4sQ0FBc0I7QUFDbEJDLEVBQUFBLFdBQVcsQ0FBQ0MsY0FBRCxFQUFpQkMsYUFBakIsRUFBZ0M7QUFDdkMsU0FBS0QsY0FBTCxHQUFzQkEsY0FBdEI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCQSxhQUFyQjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFXQUMsRUFBQUEsZUFBZSxDQUFDQyxXQUFELEVBQWNDLGNBQWQsRUFBOEI7QUFDekMsUUFBSUMsVUFBVSxHQUFHLENBQWpCO0FBQ0EsUUFBSUMsTUFBSjtBQUNBLFFBQUlDLEtBQUssR0FBRyxFQUFaO0FBRUEsVUFBTUMsYUFBYSxHQUFHSixjQUFjLENBQUMsQ0FBRCxDQUFwQzs7QUFDQSxXQUFPLENBQUNFLE1BQU0sR0FBR0gsV0FBVyxDQUFDaEUsV0FBWixHQUEwQnNFLE9BQTFCLENBQWtDRCxhQUFhLENBQUNyRSxXQUFkLEVBQWxDLEVBQStEa0UsVUFBL0QsQ0FBVixLQUF5RixDQUFoRyxFQUFtRztBQUMvRjtBQUNBLFVBQUlDLE1BQU0sR0FBR0QsVUFBYixFQUF5QjtBQUNyQixZQUFJSyxVQUFVLEdBQUdQLFdBQVcsQ0FBQ1EsU0FBWixDQUFzQk4sVUFBdEIsRUFBa0NDLE1BQWxDLENBQWpCO0FBQ0FDLFFBQUFBLEtBQUssR0FBR0EsS0FBSyxDQUFDSyxNQUFOLENBQWEsS0FBS0MsbUJBQUwsQ0FBeUJILFVBQXpCLEVBQXFDTixjQUFyQyxDQUFiLENBQVI7QUFDSCxPQUw4RixDQU8vRjtBQUNBOzs7QUFDQSxZQUFNVSxTQUFTLEdBQUdSLE1BQU0sR0FBR0UsYUFBYSxDQUFDcEYsTUFBekM7QUFDQW1GLE1BQUFBLEtBQUssQ0FBQ1EsSUFBTixDQUFXLEtBQUtDLGVBQUwsQ0FBcUJiLFdBQVcsQ0FBQ1EsU0FBWixDQUFzQkwsTUFBdEIsRUFBOEJRLFNBQTlCLENBQXJCLEVBQStELElBQS9ELENBQVg7QUFFQVQsTUFBQUEsVUFBVSxHQUFHUyxTQUFiO0FBQ0gsS0FuQndDLENBcUJ6Qzs7O0FBQ0EsUUFBSVQsVUFBVSxLQUFLRixXQUFXLENBQUMvRSxNQUEvQixFQUF1QztBQUNuQ3NGLE1BQUFBLFVBQVUsR0FBR1AsV0FBVyxDQUFDUSxTQUFaLENBQXNCTixVQUF0QixFQUFrQ1ksU0FBbEMsQ0FBYjtBQUNBVixNQUFBQSxLQUFLLEdBQUdBLEtBQUssQ0FBQ0ssTUFBTixDQUFhLEtBQUtDLG1CQUFMLENBQXlCSCxVQUF6QixFQUFxQ04sY0FBckMsQ0FBYixDQUFSO0FBQ0g7O0FBQ0QsV0FBT0csS0FBUDtBQUNIOztBQUVETSxFQUFBQSxtQkFBbUIsQ0FBQ1YsV0FBRCxFQUFjQyxjQUFkLEVBQThCO0FBQzdDLFFBQUlBLGNBQWMsQ0FBQyxDQUFELENBQWxCLEVBQXVCO0FBQ25CO0FBQ0EsYUFBTyxLQUFLRixlQUFMLENBQXFCQyxXQUFyQixFQUFrQ0MsY0FBYyxDQUFDakYsS0FBZixDQUFxQixDQUFyQixDQUFsQyxDQUFQO0FBQ0gsS0FIRCxNQUdPO0FBQ0g7QUFDQSxhQUFPLENBQUMsS0FBSzZGLGVBQUwsQ0FBcUJiLFdBQXJCLEVBQWtDLEtBQWxDLENBQUQsQ0FBUDtBQUNIO0FBQ0o7O0FBdERpQjs7QUF5RHRCLE1BQU1lLGVBQU4sU0FBOEJwQixlQUE5QixDQUE4QztBQUMxQzs7Ozs7OztBQU9Ba0IsRUFBQUEsZUFBZSxDQUFDRyxPQUFELEVBQVVDLFNBQVYsRUFBcUI7QUFDaEMsUUFBSSxDQUFDQSxTQUFMLEVBQWdCO0FBQ1o7QUFDQSxhQUFPRCxPQUFQO0FBQ0g7O0FBRUQsUUFBSS9CLElBQUksR0FBRyxtQkFBaUIsS0FBS1ksY0FBdEIsR0FBcUMsS0FBckMsR0FDTG1CLE9BREssR0FDSyxTQURoQjs7QUFHQSxRQUFJLEtBQUtsQixhQUFULEVBQXdCO0FBQ3BCYixNQUFBQSxJQUFJLEdBQUcsZUFBYWlDLFNBQVMsQ0FBQyxLQUFLcEIsYUFBTixDQUF0QixHQUEyQyxLQUEzQyxHQUNGYixJQURFLEdBQ0csTUFEVjtBQUVIOztBQUNELFdBQU9BLElBQVA7QUFDSDs7QUF0QnlDOztBQXlCOUMsTUFBTWtDLGVBQU4sU0FBOEJ4QixlQUE5QixDQUE4QztBQUMxQ0MsRUFBQUEsV0FBVyxDQUFDQyxjQUFELEVBQWlCQyxhQUFqQixFQUFnQztBQUN2QyxVQUFNRCxjQUFOLEVBQXNCQyxhQUF0QjtBQUNBLFNBQUtzQixJQUFMLEdBQVksQ0FBWjtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9BUCxFQUFBQSxlQUFlLENBQUNHLE9BQUQsRUFBVUMsU0FBVixFQUFxQjtBQUNoQyxVQUFNSSxHQUFHLEdBQUcsS0FBS0QsSUFBTCxFQUFaOztBQUVBLFFBQUlFLElBQUksR0FDSjtBQUFNLE1BQUEsR0FBRyxFQUFFRCxHQUFYO0FBQWdCLE1BQUEsU0FBUyxFQUFFSixTQUFTLEdBQUcsS0FBS3BCLGNBQVIsR0FBeUI7QUFBN0QsT0FDTW1CLE9BRE4sQ0FESjs7QUFLQSxRQUFJQyxTQUFTLElBQUksS0FBS25CLGFBQXRCLEVBQXFDO0FBQ2pDd0IsTUFBQUEsSUFBSSxHQUFHO0FBQUcsUUFBQSxHQUFHLEVBQUVELEdBQVI7QUFBYSxRQUFBLElBQUksRUFBRSxLQUFLdkI7QUFBeEIsU0FBeUN3QixJQUF6QyxDQUFQO0FBQ0g7O0FBRUQsV0FBT0EsSUFBUDtBQUNIOztBQTFCeUM7QUE4QjlDOzs7Ozs7Ozs7Ozs7Ozs7QUFhTyxTQUFTQyxVQUFULENBQW9CQyxPQUFwQixFQUE2QkMsVUFBN0IsRUFBeUNDLElBQUksR0FBQyxFQUE5QyxFQUFrRDtBQUNyRCxRQUFNQyxhQUFhLEdBQUdILE9BQU8sQ0FBQ0ksTUFBUixLQUFtQix3QkFBbkIsSUFBK0NKLE9BQU8sQ0FBQ0ssY0FBN0U7QUFDQSxNQUFJQyxZQUFZLEdBQUcsS0FBbkI7QUFFQSxNQUFJQyxjQUFjLEdBQUd4RixrQkFBckI7O0FBQ0EsTUFBSW1GLElBQUksQ0FBQ00sZ0JBQVQsRUFBMkI7QUFDdkJELElBQUFBLGNBQWMsR0FBR3RDLDBCQUFqQjtBQUNIOztBQUVELE1BQUl3QyxZQUFKO0FBQ0EsTUFBSUMsUUFBSjtBQUNBLE1BQUlDLG1CQUFKLENBWHFELENBWXJEO0FBQ0E7QUFDQTtBQUNBOztBQUNBLE1BQUk7QUFDQSxRQUFJVixVQUFVLElBQUlBLFVBQVUsQ0FBQ3hHLE1BQVgsR0FBb0IsQ0FBdEMsRUFBeUM7QUFDckMsWUFBTW1ILFdBQVcsR0FBRyxJQUFJckIsZUFBSixDQUFvQiw4QkFBcEIsRUFBb0RXLElBQUksQ0FBQzVCLGFBQXpELENBQXBCO0FBQ0EsWUFBTUcsY0FBYyxHQUFHd0IsVUFBVSxDQUFDWSxHQUFYLENBQWUsVUFBU3BCLFNBQVQsRUFBb0I7QUFDdEQsZUFBTywyQkFBYUEsU0FBYixFQUF3QmMsY0FBeEIsQ0FBUDtBQUNILE9BRnNCLENBQXZCLENBRnFDLENBS3JDOztBQUNBQSxNQUFBQSxjQUFjLENBQUNPLFVBQWYsR0FBNEIsVUFBU0MsUUFBVCxFQUFtQjtBQUMzQyxlQUFPSCxXQUFXLENBQUNyQyxlQUFaLENBQTRCd0MsUUFBNUIsRUFBc0N0QyxjQUF0QyxFQUFzRDVCLElBQXRELENBQTJELEVBQTNELENBQVA7QUFDSCxPQUZEO0FBR0g7O0FBRUQsUUFBSW1FLGFBQWEsR0FBRyxPQUFPaEIsT0FBTyxDQUFDSyxjQUFmLEtBQWtDLFFBQWxDLEdBQTZDTCxPQUFPLENBQUNLLGNBQXJELEdBQXNFLElBQTFGO0FBQ0EsVUFBTVksU0FBUyxHQUFHLE9BQU9qQixPQUFPLENBQUNrQixJQUFmLEtBQXdCLFFBQXhCLEdBQW1DbEIsT0FBTyxDQUFDa0IsSUFBM0MsR0FBa0QsSUFBcEU7QUFFQSxRQUFJaEIsSUFBSSxDQUFDaUIsa0JBQUwsSUFBMkJILGFBQS9CLEVBQThDQSxhQUFhLEdBQUdJLHFCQUFZQyxjQUFaLENBQTJCTCxhQUEzQixDQUFoQjtBQUM5Q1AsSUFBQUEsWUFBWSxHQUFHUCxJQUFJLENBQUNpQixrQkFBTCxHQUEwQkMscUJBQVlFLGVBQVosQ0FBNEJMLFNBQTVCLENBQTFCLEdBQW1FQSxTQUFsRjtBQUVBWCxJQUFBQSxZQUFZLEdBQUd2SCxpQkFBaUIsQ0FBQ29ILGFBQWEsR0FBR2EsYUFBSCxHQUFtQkMsU0FBakMsQ0FBaEMsQ0FsQkEsQ0FvQkE7O0FBQ0EsUUFBSWQsYUFBSixFQUFtQjtBQUNmUSxNQUFBQSxtQkFBbUIsR0FBRyxJQUF0QjtBQUNBRCxNQUFBQSxRQUFRLEdBQUcsMkJBQWFNLGFBQWIsRUFBNEJULGNBQTVCLENBQVg7QUFDSDtBQUNKLEdBekJELFNBeUJVO0FBQ04sV0FBT0EsY0FBYyxDQUFDTyxVQUF0QjtBQUNIOztBQUVELE1BQUlaLElBQUksQ0FBQ3FCLFlBQVQsRUFBdUI7QUFDbkIsV0FBT1osbUJBQW1CLEdBQUdELFFBQUgsR0FBY0QsWUFBeEM7QUFDSDs7QUFFRCxNQUFJZSxTQUFTLEdBQUcsS0FBaEI7O0FBQ0EsTUFBSSxDQUFDdEIsSUFBSSxDQUFDdUIsZUFBTixJQUF5Qm5CLFlBQTdCLEVBQTJDO0FBQ3ZDLFFBQUlvQixrQkFBa0IsR0FBR2pCLFlBQVksS0FBS25CLFNBQWpCLEdBQTZCbUIsWUFBWSxDQUFDa0IsSUFBYixFQUE3QixHQUFtRCxFQUE1RSxDQUR1QyxDQUd2QztBQUNBOztBQUNBRCxJQUFBQSxrQkFBa0IsR0FBR0Esa0JBQWtCLENBQUNFLE9BQW5CLENBQTJCbkosZ0JBQTNCLEVBQTZDLEVBQTdDLENBQXJCLENBTHVDLENBT3ZDO0FBQ0E7QUFDQTs7QUFDQWlKLElBQUFBLGtCQUFrQixHQUFHQSxrQkFBa0IsQ0FBQ0UsT0FBbkIsQ0FBMkJySixTQUEzQixFQUFzQyxFQUF0QyxDQUFyQjtBQUVBLFVBQU11RCxLQUFLLEdBQUdwRCxjQUFjLENBQUNtSixJQUFmLENBQW9CSCxrQkFBcEIsQ0FBZDtBQUNBRixJQUFBQSxTQUFTLEdBQUcxRixLQUFLLElBQUlBLEtBQUssQ0FBQyxDQUFELENBQWQsSUFBcUJBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU3JDLE1BQVQsS0FBb0JpSSxrQkFBa0IsQ0FBQ2pJLE1BQTVELE1BQ0E7QUFDQTtBQUNBO0FBRUl1RyxJQUFBQSxPQUFPLENBQUNLLGNBQVIsSUFBMEJmLFNBQTFCLElBQ0MsQ0FBQ1UsT0FBTyxDQUFDSyxjQUFSLENBQXVCOUUsUUFBdkIsQ0FBZ0MsT0FBaEMsQ0FBRCxJQUNELENBQUN5RSxPQUFPLENBQUNLLGNBQVIsQ0FBdUI5RSxRQUF2QixDQUFnQyxRQUFoQyxDQVBMLENBQVo7QUFTSDs7QUFFRCxRQUFNdUcsU0FBUyxHQUFHLHlCQUFXO0FBQ3pCLHlCQUFxQixJQURJO0FBRXpCLDZCQUF5Qk4sU0FGQTtBQUd6QixxQkFBaUJyQixhQUFhLElBQUksQ0FBQ3FCO0FBSFYsR0FBWCxDQUFsQjtBQU1BLFNBQU9iLG1CQUFtQixHQUN0QjtBQUFNLElBQUEsR0FBRyxFQUFDLE1BQVY7QUFBaUIsSUFBQSxHQUFHLEVBQUVULElBQUksQ0FBQzZCLEdBQTNCO0FBQWdDLElBQUEsU0FBUyxFQUFFRCxTQUEzQztBQUFzRCxJQUFBLHVCQUF1QixFQUFFO0FBQUU5RyxNQUFBQSxNQUFNLEVBQUUwRjtBQUFWLEtBQS9FO0FBQXFHLElBQUEsR0FBRyxFQUFDO0FBQXpHLElBRHNCLEdBRXRCO0FBQU0sSUFBQSxHQUFHLEVBQUMsTUFBVjtBQUFpQixJQUFBLEdBQUcsRUFBRVIsSUFBSSxDQUFDNkIsR0FBM0I7QUFBZ0MsSUFBQSxTQUFTLEVBQUVELFNBQTNDO0FBQXNELElBQUEsR0FBRyxFQUFDO0FBQTFELEtBQW1FckIsWUFBbkUsQ0FGSjtBQUdIO0FBRUQ7Ozs7Ozs7OztBQU9PLFNBQVN1QixhQUFULENBQXVCaEosR0FBdkIsRUFBNEJpSixPQUFPLEdBQUdsRyx1QkFBY2tHLE9BQXBELEVBQTZEO0FBQ2hFLFNBQU8scUJBQWVqSixHQUFmLEVBQW9CaUosT0FBcEIsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9PLFNBQVNDLGNBQVQsQ0FBd0I1SCxPQUF4QixFQUFpQzJILE9BQU8sR0FBR2xHLHVCQUFja0csT0FBekQsRUFBa0U7QUFDckUsU0FBTyxzQkFBZ0IzSCxPQUFoQixFQUF5QjJILE9BQXpCLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7QUFPTyxTQUFTRSxzQkFBVCxDQUFnQ0MsU0FBaEMsRUFBMkNILE9BQU8sR0FBR2xHLHVCQUFja0csT0FBbkUsRUFBNEU7QUFDL0UsU0FBTywyQkFBYUQsYUFBYSxDQUFDSSxTQUFELEVBQVlILE9BQVosQ0FBMUIsRUFBZ0RsSCxrQkFBaEQsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7OztBQU9PLFNBQVNzSCxjQUFULENBQXdCdkMsSUFBeEIsRUFBOEI7QUFDakMsVUFBUUEsSUFBSSxDQUFDd0MsUUFBYjtBQUNJLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssS0FBTDtBQUNBLFNBQUssWUFBTDtBQUNBLFNBQUssS0FBTDtBQUNBLFNBQUssR0FBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssT0FBTDtBQUNBLFNBQUssT0FBTDtBQUNBLFNBQUssT0FBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNBLFNBQUssSUFBTDtBQUNJLGFBQU8sSUFBUDs7QUFDSjtBQUNJLGFBQU8sS0FBUDtBQXZCUjtBQXlCSCIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbkNvcHlyaWdodCAyMDE1LCAyMDE2IE9wZW5NYXJrZXQgTHRkXHJcbkNvcHlyaWdodCAyMDE3LCAyMDE4IE5ldyBWZWN0b3IgTHRkXHJcbkNvcHlyaWdodCAyMDE5IE1pY2hhZWwgVGVsYXR5bnNraSA8N3QzY2hndXlAZ21haWwuY29tPlxyXG5Db3B5cmlnaHQgMjAxOSBUaGUgTWF0cml4Lm9yZyBGb3VuZGF0aW9uIEMuSS5DLlxyXG5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxueW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG5Zb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuXHJcbiAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG5XSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG5saW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCBSZXBseVRocmVhZCBmcm9tIFwiLi9jb21wb25lbnRzL3ZpZXdzL2VsZW1lbnRzL1JlcGx5VGhyZWFkXCI7XHJcblxyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgc2FuaXRpemVIdG1sIGZyb20gJ3Nhbml0aXplLWh0bWwnO1xyXG5pbXBvcnQgKiBhcyBsaW5raWZ5IGZyb20gJ2xpbmtpZnlqcyc7XHJcbmltcG9ydCBsaW5raWZ5TWF0cml4IGZyb20gJy4vbGlua2lmeS1tYXRyaXgnO1xyXG5pbXBvcnQgX2xpbmtpZnlFbGVtZW50IGZyb20gJ2xpbmtpZnlqcy9lbGVtZW50JztcclxuaW1wb3J0IF9saW5raWZ5U3RyaW5nIGZyb20gJ2xpbmtpZnlqcy9zdHJpbmcnO1xyXG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcclxuaW1wb3J0IHtNYXRyaXhDbGllbnRQZWd9IGZyb20gJy4vTWF0cml4Q2xpZW50UGVnJztcclxuaW1wb3J0IHVybCBmcm9tICd1cmwnO1xyXG5cclxuaW1wb3J0IEVNT0pJQkFTRV9SRUdFWCBmcm9tICdlbW9qaWJhc2UtcmVnZXgnO1xyXG5pbXBvcnQge3RyeVRyYW5zZm9ybVBlcm1hbGlua1RvTG9jYWxIcmVmfSBmcm9tIFwiLi91dGlscy9wZXJtYWxpbmtzL1Blcm1hbGlua3NcIjtcclxuaW1wb3J0IHtTSE9SVENPREVfVE9fRU1PSkksIGdldEVtb2ppRnJvbVVuaWNvZGV9IGZyb20gXCIuL2Vtb2ppXCI7XHJcblxyXG5saW5raWZ5TWF0cml4KGxpbmtpZnkpO1xyXG5cclxuLy8gQW55dGhpbmcgb3V0c2lkZSB0aGUgYmFzaWMgbXVsdGlsaW5ndWFsIHBsYW5lIHdpbGwgYmUgYSBzdXJyb2dhdGUgcGFpclxyXG5jb25zdCBTVVJST0dBVEVfUEFJUl9QQVRURVJOID0gLyhbXFx1ZDgwMC1cXHVkYmZmXSkoW1xcdWRjMDAtXFx1ZGZmZl0pLztcclxuLy8gQW5kIHRoZXJlIGEgYnVuY2ggbW9yZSBzeW1ib2wgY2hhcmFjdGVycyB0aGF0IGVtb2ppYmFzZSBoYXMgd2l0aGluIHRoZVxyXG4vLyBCTVAsIHNvIHRoaXMgaW5jbHVkZXMgdGhlIHJhbmdlcyBmcm9tICdsZXR0ZXJsaWtlIHN5bWJvbHMnIHRvXHJcbi8vICdtaXNjZWxsYW5lb3VzIHN5bWJvbHMgYW5kIGFycm93cycgd2hpY2ggc2hvdWxkIGNhdGNoIGFsbCBvZiB0aGVtXHJcbi8vICh3aXRoIHBsZW50eSBvZiBmYWxzZSBwb3NpdGl2ZXMsIGJ1dCB0aGF0J3MgT0spXHJcbmNvbnN0IFNZTUJPTF9QQVRURVJOID0gLyhbXFx1MjEwMC1cXHUyYmZmXSkvO1xyXG5cclxuLy8gUmVnZXggcGF0dGVybiBmb3IgWmVyby1XaWR0aCBqb2luZXIgdW5pY29kZSBjaGFyYWN0ZXJzXHJcbmNvbnN0IFpXSl9SRUdFWCA9IG5ldyBSZWdFeHAoXCJcXHUyMDBEfFxcdTIwMDNcIiwgXCJnXCIpO1xyXG5cclxuLy8gUmVnZXggcGF0dGVybiBmb3Igd2hpdGVzcGFjZSBjaGFyYWN0ZXJzXHJcbmNvbnN0IFdISVRFU1BBQ0VfUkVHRVggPSBuZXcgUmVnRXhwKFwiXFxcXHNcIiwgXCJnXCIpO1xyXG5cclxuY29uc3QgQklHRU1PSklfUkVHRVggPSBuZXcgUmVnRXhwKGBeKCR7RU1PSklCQVNFX1JFR0VYLnNvdXJjZX0pKyRgLCAnaScpO1xyXG5cclxuY29uc3QgQ09MT1JfUkVHRVggPSAvXiNbMC05YS1mQS1GXXs2fSQvO1xyXG5cclxuY29uc3QgUEVSTUlUVEVEX1VSTF9TQ0hFTUVTID0gWydodHRwJywgJ2h0dHBzJywgJ2Z0cCcsICdtYWlsdG8nLCAnbWFnbmV0J107XHJcblxyXG4vKlxyXG4gKiBSZXR1cm4gdHJ1ZSBpZiB0aGUgZ2l2ZW4gc3RyaW5nIGNvbnRhaW5zIGVtb2ppXHJcbiAqIFVzZXMgYSBtdWNoLCBtdWNoIHNpbXBsZXIgcmVnZXggdGhhbiBlbW9qaWJhc2UncyBzbyB3aWxsIGdpdmUgZmFsc2VcclxuICogcG9zaXRpdmVzLCBidXQgdXNlZnVsIGZvciBmYXN0LXBhdGggdGVzdGluZyBzdHJpbmdzIHRvIHNlZSBpZiB0aGV5XHJcbiAqIG5lZWQgZW1vamlmaWNhdGlvbi5cclxuICogdW5pY29kZVRvSW1hZ2UgdXNlcyB0aGlzIGZ1bmN0aW9uLlxyXG4gKi9cclxuZnVuY3Rpb24gbWlnaHRDb250YWluRW1vamkoc3RyKSB7XHJcbiAgICByZXR1cm4gU1VSUk9HQVRFX1BBSVJfUEFUVEVSTi50ZXN0KHN0cikgfHwgU1lNQk9MX1BBVFRFUk4udGVzdChzdHIpO1xyXG59XHJcblxyXG4vKipcclxuICogUmV0dXJucyB0aGUgc2hvcnRjb2RlIGZvciBhbiBlbW9qaSBjaGFyYWN0ZXIuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBjaGFyIFRoZSBlbW9qaSBjaGFyYWN0ZXJcclxuICogQHJldHVybiB7U3RyaW5nfSBUaGUgc2hvcnRjb2RlIChzdWNoIGFzIDp0aHVtYnVwOilcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiB1bmljb2RlVG9TaG9ydGNvZGUoY2hhcikge1xyXG4gICAgY29uc3QgZGF0YSA9IGdldEVtb2ppRnJvbVVuaWNvZGUoY2hhcik7XHJcbiAgICByZXR1cm4gKGRhdGEgJiYgZGF0YS5zaG9ydGNvZGVzID8gYDoke2RhdGEuc2hvcnRjb2Rlc1swXX06YCA6ICcnKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgdGhlIHVuaWNvZGUgY2hhcmFjdGVyIGZvciBhbiBlbW9qaSBzaG9ydGNvZGVcclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IHNob3J0Y29kZSBUaGUgc2hvcnRjb2RlIChzdWNoIGFzIDp0aHVtYnVwOilcclxuICogQHJldHVybiB7U3RyaW5nfSBUaGUgZW1vamkgY2hhcmFjdGVyOyBudWxsIGlmIG5vbmUgZXhpc3RzXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gc2hvcnRjb2RlVG9Vbmljb2RlKHNob3J0Y29kZSkge1xyXG4gICAgc2hvcnRjb2RlID0gc2hvcnRjb2RlLnNsaWNlKDEsIHNob3J0Y29kZS5sZW5ndGggLSAxKTtcclxuICAgIGNvbnN0IGRhdGEgPSBTSE9SVENPREVfVE9fRU1PSkkuZ2V0KHNob3J0Y29kZSk7XHJcbiAgICByZXR1cm4gZGF0YSA/IGRhdGEudW5pY29kZSA6IG51bGw7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBwcm9jZXNzSHRtbEZvclNlbmRpbmcoaHRtbDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IGNvbnRlbnREaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIGNvbnRlbnREaXYuaW5uZXJIVE1MID0gaHRtbDtcclxuXHJcbiAgICBpZiAoY29udGVudERpdi5jaGlsZHJlbi5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm4gY29udGVudERpdi5pbm5lckhUTUw7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGNvbnRlbnRIVE1MID0gXCJcIjtcclxuICAgIGZvciAobGV0IGk9MDsgaSA8IGNvbnRlbnREaXYuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBjb25zdCBlbGVtZW50ID0gY29udGVudERpdi5jaGlsZHJlbltpXTtcclxuICAgICAgICBpZiAoZWxlbWVudC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdwJykge1xyXG4gICAgICAgICAgICBjb250ZW50SFRNTCArPSBlbGVtZW50LmlubmVySFRNTDtcclxuICAgICAgICAgICAgLy8gRG9uJ3QgYWRkIGEgPGJyIC8+IGZvciB0aGUgbGFzdCA8cD5cclxuICAgICAgICAgICAgaWYgKGkgIT09IGNvbnRlbnREaXYuY2hpbGRyZW4ubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgICAgICAgICAgY29udGVudEhUTUwgKz0gJzxiciAvPic7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCB0ZW1wID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgICAgIHRlbXAuYXBwZW5kQ2hpbGQoZWxlbWVudC5jbG9uZU5vZGUodHJ1ZSkpO1xyXG4gICAgICAgICAgICBjb250ZW50SFRNTCArPSB0ZW1wLmlubmVySFRNTDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGNvbnRlbnRIVE1MO1xyXG59XHJcblxyXG4vKlxyXG4gKiBHaXZlbiBhbiB1bnRydXN0ZWQgSFRNTCBzdHJpbmcsIHJldHVybiBhIFJlYWN0IG5vZGUgd2l0aCBhbiBzYW5pdGl6ZWQgdmVyc2lvblxyXG4gKiBvZiB0aGF0IEhUTUwuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gc2FuaXRpemVkSHRtbE5vZGUoaW5zYW5lSHRtbCkge1xyXG4gICAgY29uc3Qgc2FuZUh0bWwgPSBzYW5pdGl6ZUh0bWwoaW5zYW5lSHRtbCwgc2FuaXRpemVIdG1sUGFyYW1zKTtcclxuXHJcbiAgICByZXR1cm4gPGRpdiBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IHNhbmVIdG1sIH19IGRpcj1cImF1dG9cIiAvPjtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRlc3RzIGlmIGEgVVJMIGZyb20gYW4gdW50cnVzdGVkIHNvdXJjZSBtYXkgYmUgc2FmZWx5IHB1dCBpbnRvIHRoZSBET01cclxuICogVGhlIGJpZ2dlc3QgdGhyZWF0IGhlcmUgaXMgamF2YXNjcmlwdDogVVJJcy5cclxuICogTm90ZSB0aGF0IHRoZSBIVE1MIHNhbml0aXNlciBsaWJyYXJ5IGhhcyBpdHMgb3duIGludGVybmFsIGxvZ2ljIGZvclxyXG4gKiBkb2luZyB0aGlzLCB0byB3aGljaCB3ZSBwYXNzIHRoZSBzYW1lIGxpc3Qgb2Ygc2NoZW1lcy4gVGhpcyBpcyB1c2VkIGluXHJcbiAqIG90aGVyIHBsYWNlcyB3ZSBuZWVkIHRvIHNhbml0aXNlIFVSTHMuXHJcbiAqIEByZXR1cm4gdHJ1ZSBpZiBwZXJtaXR0ZWQsIG90aGVyd2lzZSBmYWxzZVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGlzVXJsUGVybWl0dGVkKGlucHV0VXJsKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIGNvbnN0IHBhcnNlZCA9IHVybC5wYXJzZShpbnB1dFVybCk7XHJcbiAgICAgICAgaWYgKCFwYXJzZWQucHJvdG9jb2wpIHJldHVybiBmYWxzZTtcclxuICAgICAgICAvLyBVUkwgcGFyc2VyIHByb3RvY29sIGluY2x1ZGVzIHRoZSB0cmFpbGluZyBjb2xvblxyXG4gICAgICAgIHJldHVybiBQRVJNSVRURURfVVJMX1NDSEVNRVMuaW5jbHVkZXMocGFyc2VkLnByb3RvY29sLnNsaWNlKDAsIC0xKSk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCB0cmFuc2Zvcm1UYWdzID0geyAvLyBjdXN0b20gdG8gbWF0cml4XHJcbiAgICAvLyBhZGQgYmxhbmsgdGFyZ2V0cyB0byBhbGwgaHlwZXJsaW5rcyBleGNlcHQgdmVjdG9yIFVSTHNcclxuICAgICdhJzogZnVuY3Rpb24odGFnTmFtZSwgYXR0cmlicykge1xyXG4gICAgICAgIGlmIChhdHRyaWJzLmhyZWYpIHtcclxuICAgICAgICAgICAgYXR0cmlicy50YXJnZXQgPSAnX2JsYW5rJzsgLy8gYnkgZGVmYXVsdFxyXG5cclxuICAgICAgICAgICAgY29uc3QgdHJhbnNmb3JtZWQgPSB0cnlUcmFuc2Zvcm1QZXJtYWxpbmtUb0xvY2FsSHJlZihhdHRyaWJzLmhyZWYpO1xyXG4gICAgICAgICAgICBpZiAodHJhbnNmb3JtZWQgIT09IGF0dHJpYnMuaHJlZiB8fCBhdHRyaWJzLmhyZWYubWF0Y2gobGlua2lmeU1hdHJpeC5WRUNUT1JfVVJMX1BBVFRFUk4pKSB7XHJcbiAgICAgICAgICAgICAgICBhdHRyaWJzLmhyZWYgPSB0cmFuc2Zvcm1lZDtcclxuICAgICAgICAgICAgICAgIGRlbGV0ZSBhdHRyaWJzLnRhcmdldDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBhdHRyaWJzLnJlbCA9ICdub3JlZmVycmVyIG5vb3BlbmVyJzsgLy8gaHR0cHM6Ly9tYXRoaWFzYnluZW5zLmdpdGh1Yi5pby9yZWwtbm9vcGVuZXIvXHJcbiAgICAgICAgcmV0dXJuIHsgdGFnTmFtZSwgYXR0cmlicyB9O1xyXG4gICAgfSxcclxuICAgICdpbWcnOiBmdW5jdGlvbih0YWdOYW1lLCBhdHRyaWJzKSB7XHJcbiAgICAgICAgLy8gU3RyaXAgb3V0IGltZ3MgdGhhdCBhcmVuJ3QgYG14Y2AgaGVyZSBpbnN0ZWFkIG9mIHVzaW5nIGFsbG93ZWRTY2hlbWVzQnlUYWdcclxuICAgICAgICAvLyBiZWNhdXNlIHRyYW5zZm9ybVRhZ3MgaXMgdXNlZCBfYmVmb3JlXyB3ZSBmaWx0ZXIgYnkgYWxsb3dlZFNjaGVtZXNCeVRhZyBhbmRcclxuICAgICAgICAvLyB3ZSBkb24ndCB3YW50IHRvIGFsbG93IGltYWdlcyB3aXRoIGBodHRwcz9gIGBzcmNgcy5cclxuICAgICAgICBpZiAoIWF0dHJpYnMuc3JjIHx8ICFhdHRyaWJzLnNyYy5zdGFydHNXaXRoKCdteGM6Ly8nKSkge1xyXG4gICAgICAgICAgICByZXR1cm4geyB0YWdOYW1lLCBhdHRyaWJzOiB7fX07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF0dHJpYnMuc3JjID0gTWF0cml4Q2xpZW50UGVnLmdldCgpLm14Y1VybFRvSHR0cChcclxuICAgICAgICAgICAgYXR0cmlicy5zcmMsXHJcbiAgICAgICAgICAgIGF0dHJpYnMud2lkdGggfHwgODAwLFxyXG4gICAgICAgICAgICBhdHRyaWJzLmhlaWdodCB8fCA2MDAsXHJcbiAgICAgICAgKTtcclxuICAgICAgICByZXR1cm4geyB0YWdOYW1lLCBhdHRyaWJzIH07XHJcbiAgICB9LFxyXG4gICAgJ2NvZGUnOiBmdW5jdGlvbih0YWdOYW1lLCBhdHRyaWJzKSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBhdHRyaWJzLmNsYXNzICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICAvLyBGaWx0ZXIgb3V0IGFsbCBjbGFzc2VzIG90aGVyIHRoYW4gb25lcyBzdGFydGluZyB3aXRoIGxhbmd1YWdlLSBmb3Igc3ludGF4IGhpZ2hsaWdodGluZy5cclxuICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGF0dHJpYnMuY2xhc3Muc3BsaXQoL1xccy8pLmZpbHRlcihmdW5jdGlvbihjbCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNsLnN0YXJ0c1dpdGgoJ2xhbmd1YWdlLScpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgYXR0cmlicy5jbGFzcyA9IGNsYXNzZXMuam9pbignICcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4geyB0YWdOYW1lLCBhdHRyaWJzIH07XHJcbiAgICB9LFxyXG4gICAgJyonOiBmdW5jdGlvbih0YWdOYW1lLCBhdHRyaWJzKSB7XHJcbiAgICAgICAgLy8gRGVsZXRlIGFueSBzdHlsZSBwcmV2aW91c2x5IGFzc2lnbmVkLCBzdHlsZSBpcyBhbiBhbGxvd2VkVGFnIGZvciBmb250IGFuZCBzcGFuXHJcbiAgICAgICAgLy8gYmVjYXVzZSBhdHRyaWJ1dGVzIGFyZSBzdHJpcHBlZCBhZnRlciB0cmFuc2Zvcm1pbmdcclxuICAgICAgICBkZWxldGUgYXR0cmlicy5zdHlsZTtcclxuXHJcbiAgICAgICAgLy8gU2FuaXRpc2UgYW5kIHRyYW5zZm9ybSBkYXRhLW14LWNvbG9yIGFuZCBkYXRhLW14LWJnLWNvbG9yIHRvIHRoZWlyIENTU1xyXG4gICAgICAgIC8vIGVxdWl2YWxlbnRzXHJcbiAgICAgICAgY29uc3QgY3VzdG9tQ1NTTWFwcGVyID0ge1xyXG4gICAgICAgICAgICAnZGF0YS1teC1jb2xvcic6ICdjb2xvcicsXHJcbiAgICAgICAgICAgICdkYXRhLW14LWJnLWNvbG9yJzogJ2JhY2tncm91bmQtY29sb3InLFxyXG4gICAgICAgICAgICAvLyAkY3VzdG9tQXR0cmlidXRlS2V5OiAkY3NzQXR0cmlidXRlS2V5XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IHN0eWxlID0gXCJcIjtcclxuICAgICAgICBPYmplY3Qua2V5cyhjdXN0b21DU1NNYXBwZXIpLmZvckVhY2goKGN1c3RvbUF0dHJpYnV0ZUtleSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBjc3NBdHRyaWJ1dGVLZXkgPSBjdXN0b21DU1NNYXBwZXJbY3VzdG9tQXR0cmlidXRlS2V5XTtcclxuICAgICAgICAgICAgY29uc3QgY3VzdG9tQXR0cmlidXRlVmFsdWUgPSBhdHRyaWJzW2N1c3RvbUF0dHJpYnV0ZUtleV07XHJcbiAgICAgICAgICAgIGlmIChjdXN0b21BdHRyaWJ1dGVWYWx1ZSAmJlxyXG4gICAgICAgICAgICAgICAgdHlwZW9mIGN1c3RvbUF0dHJpYnV0ZVZhbHVlID09PSAnc3RyaW5nJyAmJlxyXG4gICAgICAgICAgICAgICAgQ09MT1JfUkVHRVgudGVzdChjdXN0b21BdHRyaWJ1dGVWYWx1ZSlcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICBzdHlsZSArPSBjc3NBdHRyaWJ1dGVLZXkgKyBcIjpcIiArIGN1c3RvbUF0dHJpYnV0ZVZhbHVlICsgXCI7XCI7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgYXR0cmlic1tjdXN0b21BdHRyaWJ1dGVLZXldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmIChzdHlsZSkge1xyXG4gICAgICAgICAgICBhdHRyaWJzLnN0eWxlID0gc3R5bGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4geyB0YWdOYW1lLCBhdHRyaWJzIH07XHJcbiAgICB9LFxyXG59O1xyXG5cclxuY29uc3Qgc2FuaXRpemVIdG1sUGFyYW1zID0ge1xyXG4gICAgYWxsb3dlZFRhZ3M6IFtcclxuICAgICAgICAnZm9udCcsIC8vIGN1c3RvbSB0byBtYXRyaXggZm9yIElSQy1zdHlsZSBmb250IGNvbG9yaW5nXHJcbiAgICAgICAgJ2RlbCcsIC8vIGZvciBtYXJrZG93blxyXG4gICAgICAgICdoMScsICdoMicsICdoMycsICdoNCcsICdoNScsICdoNicsICdibG9ja3F1b3RlJywgJ3AnLCAnYScsICd1bCcsICdvbCcsICdzdXAnLCAnc3ViJyxcclxuICAgICAgICAnbmwnLCAnbGknLCAnYicsICdpJywgJ3UnLCAnc3Ryb25nJywgJ2VtJywgJ3N0cmlrZScsICdjb2RlJywgJ2hyJywgJ2JyJywgJ2RpdicsXHJcbiAgICAgICAgJ3RhYmxlJywgJ3RoZWFkJywgJ2NhcHRpb24nLCAndGJvZHknLCAndHInLCAndGgnLCAndGQnLCAncHJlJywgJ3NwYW4nLCAnaW1nJyxcclxuICAgIF0sXHJcbiAgICBhbGxvd2VkQXR0cmlidXRlczoge1xyXG4gICAgICAgIC8vIGN1c3RvbSBvbmVzIGZpcnN0OlxyXG4gICAgICAgIGZvbnQ6IFsnY29sb3InLCAnZGF0YS1teC1iZy1jb2xvcicsICdkYXRhLW14LWNvbG9yJywgJ3N0eWxlJ10sIC8vIGN1c3RvbSB0byBtYXRyaXhcclxuICAgICAgICBzcGFuOiBbJ2RhdGEtbXgtYmctY29sb3InLCAnZGF0YS1teC1jb2xvcicsICdkYXRhLW14LXNwb2lsZXInLCAnc3R5bGUnXSwgLy8gY3VzdG9tIHRvIG1hdHJpeFxyXG4gICAgICAgIGE6IFsnaHJlZicsICduYW1lJywgJ3RhcmdldCcsICdyZWwnXSwgLy8gcmVtb3RlIHRhcmdldDogY3VzdG9tIHRvIG1hdHJpeFxyXG4gICAgICAgIGltZzogWydzcmMnLCAnd2lkdGgnLCAnaGVpZ2h0JywgJ2FsdCcsICd0aXRsZSddLFxyXG4gICAgICAgIG9sOiBbJ3N0YXJ0J10sXHJcbiAgICAgICAgY29kZTogWydjbGFzcyddLCAvLyBXZSBkb24ndCBhY3R1YWxseSBhbGxvdyBhbGwgY2xhc3Nlcywgd2UgZmlsdGVyIHRoZW0gaW4gdHJhbnNmb3JtVGFnc1xyXG4gICAgfSxcclxuICAgIC8vIExvdHMgb2YgdGhlc2Ugd29uJ3QgY29tZSB1cCBieSBkZWZhdWx0IGJlY2F1c2Ugd2UgZG9uJ3QgYWxsb3cgdGhlbVxyXG4gICAgc2VsZkNsb3Npbmc6IFsnaW1nJywgJ2JyJywgJ2hyJywgJ2FyZWEnLCAnYmFzZScsICdiYXNlZm9udCcsICdpbnB1dCcsICdsaW5rJywgJ21ldGEnXSxcclxuICAgIC8vIFVSTCBzY2hlbWVzIHdlIHBlcm1pdFxyXG4gICAgYWxsb3dlZFNjaGVtZXM6IFBFUk1JVFRFRF9VUkxfU0NIRU1FUyxcclxuXHJcbiAgICBhbGxvd1Byb3RvY29sUmVsYXRpdmU6IGZhbHNlLFxyXG4gICAgdHJhbnNmb3JtVGFncyxcclxufTtcclxuXHJcbi8vIHRoaXMgaXMgdGhlIHNhbWUgYXMgdGhlIGFib3ZlIGV4Y2VwdCB3aXRoIGxlc3MgcmV3cml0aW5nXHJcbmNvbnN0IGNvbXBvc2VyU2FuaXRpemVIdG1sUGFyYW1zID0gT2JqZWN0LmFzc2lnbih7fSwgc2FuaXRpemVIdG1sUGFyYW1zKTtcclxuY29tcG9zZXJTYW5pdGl6ZUh0bWxQYXJhbXMudHJhbnNmb3JtVGFncyA9IHtcclxuICAgICdjb2RlJzogdHJhbnNmb3JtVGFnc1snY29kZSddLFxyXG4gICAgJyonOiB0cmFuc2Zvcm1UYWdzWycqJ10sXHJcbn07XHJcblxyXG5jbGFzcyBCYXNlSGlnaGxpZ2h0ZXIge1xyXG4gICAgY29uc3RydWN0b3IoaGlnaGxpZ2h0Q2xhc3MsIGhpZ2hsaWdodExpbmspIHtcclxuICAgICAgICB0aGlzLmhpZ2hsaWdodENsYXNzID0gaGlnaGxpZ2h0Q2xhc3M7XHJcbiAgICAgICAgdGhpcy5oaWdobGlnaHRMaW5rID0gaGlnaGxpZ2h0TGluaztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGFwcGx5IHRoZSBoaWdobGlnaHRzIHRvIGEgc2VjdGlvbiBvZiB0ZXh0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNhZmVTbmlwcGV0IFRoZSBzbmlwcGV0IG9mIHRleHQgdG8gYXBwbHkgdGhlIGhpZ2hsaWdodHNcclxuICAgICAqICAgICB0by5cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nW119IHNhZmVIaWdobGlnaHRzIEEgbGlzdCBvZiBzdWJzdHJpbmdzIHRvIGhpZ2hsaWdodCxcclxuICAgICAqICAgICBzb3J0ZWQgYnkgZGVzY2VuZGluZyBsZW5ndGguXHJcbiAgICAgKlxyXG4gICAgICogcmV0dXJucyBhIGxpc3Qgb2YgcmVzdWx0cyAoc3RyaW5ncyBmb3IgSHRtbEhpZ2hsaWdoZXIsIHJlYWN0IG5vZGVzIGZvclxyXG4gICAgICogVGV4dEhpZ2hsaWdodGVyKS5cclxuICAgICAqL1xyXG4gICAgYXBwbHlIaWdobGlnaHRzKHNhZmVTbmlwcGV0LCBzYWZlSGlnaGxpZ2h0cykge1xyXG4gICAgICAgIGxldCBsYXN0T2Zmc2V0ID0gMDtcclxuICAgICAgICBsZXQgb2Zmc2V0O1xyXG4gICAgICAgIGxldCBub2RlcyA9IFtdO1xyXG5cclxuICAgICAgICBjb25zdCBzYWZlSGlnaGxpZ2h0ID0gc2FmZUhpZ2hsaWdodHNbMF07XHJcbiAgICAgICAgd2hpbGUgKChvZmZzZXQgPSBzYWZlU25pcHBldC50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoc2FmZUhpZ2hsaWdodC50b0xvd2VyQ2FzZSgpLCBsYXN0T2Zmc2V0KSkgPj0gMCkge1xyXG4gICAgICAgICAgICAvLyBoYW5kbGUgcHJlYW1ibGVcclxuICAgICAgICAgICAgaWYgKG9mZnNldCA+IGxhc3RPZmZzZXQpIHtcclxuICAgICAgICAgICAgICAgIHZhciBzdWJTbmlwcGV0ID0gc2FmZVNuaXBwZXQuc3Vic3RyaW5nKGxhc3RPZmZzZXQsIG9mZnNldCk7XHJcbiAgICAgICAgICAgICAgICBub2RlcyA9IG5vZGVzLmNvbmNhdCh0aGlzLl9hcHBseVN1YkhpZ2hsaWdodHMoc3ViU25pcHBldCwgc2FmZUhpZ2hsaWdodHMpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gZG8gaGlnaGxpZ2h0LiB1c2UgdGhlIG9yaWdpbmFsIHN0cmluZyByYXRoZXIgdGhhbiBzYWZlSGlnaGxpZ2h0XHJcbiAgICAgICAgICAgIC8vIHRvIHByZXNlcnZlIHRoZSBvcmlnaW5hbCBjYXNpbmcuXHJcbiAgICAgICAgICAgIGNvbnN0IGVuZE9mZnNldCA9IG9mZnNldCArIHNhZmVIaWdobGlnaHQubGVuZ3RoO1xyXG4gICAgICAgICAgICBub2Rlcy5wdXNoKHRoaXMuX3Byb2Nlc3NTbmlwcGV0KHNhZmVTbmlwcGV0LnN1YnN0cmluZyhvZmZzZXQsIGVuZE9mZnNldCksIHRydWUpKTtcclxuXHJcbiAgICAgICAgICAgIGxhc3RPZmZzZXQgPSBlbmRPZmZzZXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBoYW5kbGUgcG9zdGFtYmxlXHJcbiAgICAgICAgaWYgKGxhc3RPZmZzZXQgIT09IHNhZmVTbmlwcGV0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICBzdWJTbmlwcGV0ID0gc2FmZVNuaXBwZXQuc3Vic3RyaW5nKGxhc3RPZmZzZXQsIHVuZGVmaW5lZCk7XHJcbiAgICAgICAgICAgIG5vZGVzID0gbm9kZXMuY29uY2F0KHRoaXMuX2FwcGx5U3ViSGlnaGxpZ2h0cyhzdWJTbmlwcGV0LCBzYWZlSGlnaGxpZ2h0cykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbm9kZXM7XHJcbiAgICB9XHJcblxyXG4gICAgX2FwcGx5U3ViSGlnaGxpZ2h0cyhzYWZlU25pcHBldCwgc2FmZUhpZ2hsaWdodHMpIHtcclxuICAgICAgICBpZiAoc2FmZUhpZ2hsaWdodHNbMV0pIHtcclxuICAgICAgICAgICAgLy8gcmVjdXJzZSBpbnRvIHRoaXMgcmFuZ2UgdG8gY2hlY2sgZm9yIHRoZSBuZXh0IHNldCBvZiBoaWdobGlnaHQgbWF0Y2hlc1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcHBseUhpZ2hsaWdodHMoc2FmZVNuaXBwZXQsIHNhZmVIaWdobGlnaHRzLnNsaWNlKDEpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBubyBtb3JlIGhpZ2hsaWdodHMgdG8gYmUgZm91bmQsIGp1c3QgcmV0dXJuIHRoZSB1bmhpZ2hsaWdodGVkIHN0cmluZ1xyXG4gICAgICAgICAgICByZXR1cm4gW3RoaXMuX3Byb2Nlc3NTbmlwcGV0KHNhZmVTbmlwcGV0LCBmYWxzZSldO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuY2xhc3MgSHRtbEhpZ2hsaWdodGVyIGV4dGVuZHMgQmFzZUhpZ2hsaWdodGVyIHtcclxuICAgIC8qIGhpZ2hsaWdodCB0aGUgZ2l2ZW4gc25pcHBldCBpZiByZXF1aXJlZFxyXG4gICAgICpcclxuICAgICAqIHNuaXBwZXQ6IGNvbnRlbnQgb2YgdGhlIHNwYW47IG11c3QgaGF2ZSBiZWVuIHNhbml0aXNlZFxyXG4gICAgICogaGlnaGxpZ2h0OiB0cnVlIHRvIGhpZ2hsaWdodCBhcyBhIHNlYXJjaCBtYXRjaFxyXG4gICAgICpcclxuICAgICAqIHJldHVybnMgYW4gSFRNTCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgX3Byb2Nlc3NTbmlwcGV0KHNuaXBwZXQsIGhpZ2hsaWdodCkge1xyXG4gICAgICAgIGlmICghaGlnaGxpZ2h0KSB7XHJcbiAgICAgICAgICAgIC8vIG5vdGhpbmcgcmVxdWlyZWQgaGVyZVxyXG4gICAgICAgICAgICByZXR1cm4gc25pcHBldDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBzcGFuID0gXCI8c3BhbiBjbGFzcz1cXFwiXCIrdGhpcy5oaWdobGlnaHRDbGFzcytcIlxcXCI+XCJcclxuICAgICAgICAgICAgKyBzbmlwcGV0ICsgXCI8L3NwYW4+XCI7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmhpZ2hsaWdodExpbmspIHtcclxuICAgICAgICAgICAgc3BhbiA9IFwiPGEgaHJlZj1cXFwiXCIrZW5jb2RlVVJJKHRoaXMuaGlnaGxpZ2h0TGluaykrXCJcXFwiPlwiXHJcbiAgICAgICAgICAgICAgICArc3BhbitcIjwvYT5cIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNwYW47XHJcbiAgICB9XHJcbn1cclxuXHJcbmNsYXNzIFRleHRIaWdobGlnaHRlciBleHRlbmRzIEJhc2VIaWdobGlnaHRlciB7XHJcbiAgICBjb25zdHJ1Y3RvcihoaWdobGlnaHRDbGFzcywgaGlnaGxpZ2h0TGluaykge1xyXG4gICAgICAgIHN1cGVyKGhpZ2hsaWdodENsYXNzLCBoaWdobGlnaHRMaW5rKTtcclxuICAgICAgICB0aGlzLl9rZXkgPSAwO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGNyZWF0ZSBhIDxzcGFuPiBub2RlIHRvIGhvbGQgdGhlIGdpdmVuIGNvbnRlbnRcclxuICAgICAqXHJcbiAgICAgKiBzbmlwcGV0OiBjb250ZW50IG9mIHRoZSBzcGFuXHJcbiAgICAgKiBoaWdobGlnaHQ6IHRydWUgdG8gaGlnaGxpZ2h0IGFzIGEgc2VhcmNoIG1hdGNoXHJcbiAgICAgKlxyXG4gICAgICogcmV0dXJucyBhIFJlYWN0IG5vZGVcclxuICAgICAqL1xyXG4gICAgX3Byb2Nlc3NTbmlwcGV0KHNuaXBwZXQsIGhpZ2hsaWdodCkge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IHRoaXMuX2tleSsrO1xyXG5cclxuICAgICAgICBsZXQgbm9kZSA9XHJcbiAgICAgICAgICAgIDxzcGFuIGtleT17a2V5fSBjbGFzc05hbWU9e2hpZ2hsaWdodCA/IHRoaXMuaGlnaGxpZ2h0Q2xhc3MgOiBudWxsfT5cclxuICAgICAgICAgICAgICAgIHsgc25pcHBldCB9XHJcbiAgICAgICAgICAgIDwvc3Bhbj47XHJcblxyXG4gICAgICAgIGlmIChoaWdobGlnaHQgJiYgdGhpcy5oaWdobGlnaHRMaW5rKSB7XHJcbiAgICAgICAgICAgIG5vZGUgPSA8YSBrZXk9e2tleX0gaHJlZj17dGhpcy5oaWdobGlnaHRMaW5rfT57IG5vZGUgfTwvYT47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIHR1cm4gYSBtYXRyaXggZXZlbnQgYm9keSBpbnRvIGh0bWxcclxuICpcclxuICogY29udGVudDogJ2NvbnRlbnQnIG9mIHRoZSBNYXRyaXhFdmVudFxyXG4gKlxyXG4gKiBoaWdobGlnaHRzOiBvcHRpb25hbCBsaXN0IG9mIHdvcmRzIHRvIGhpZ2hsaWdodCwgb3JkZXJlZCBieSBsb25nZXN0IHdvcmQgZmlyc3RcclxuICpcclxuICogb3B0cy5oaWdobGlnaHRMaW5rOiBvcHRpb25hbCBocmVmIHRvIGFkZCB0byBoaWdobGlnaHRlZCB3b3Jkc1xyXG4gKiBvcHRzLmRpc2FibGVCaWdFbW9qaTogb3B0aW9uYWwgYXJndW1lbnQgdG8gZGlzYWJsZSB0aGUgYmlnIGVtb2ppIGNsYXNzLlxyXG4gKiBvcHRzLnN0cmlwUmVwbHlGYWxsYmFjazogb3B0aW9uYWwgYXJndW1lbnQgc3BlY2lmeWluZyB0aGUgZXZlbnQgaXMgYSByZXBseSBhbmQgc28gZmFsbGJhY2sgbmVlZHMgcmVtb3ZpbmdcclxuICogb3B0cy5yZXR1cm5TdHJpbmc6IHJldHVybiBhbiBIVE1MIHN0cmluZyByYXRoZXIgdGhhbiBKU1ggZWxlbWVudHNcclxuICogb3B0cy5mb3JDb21wb3NlclF1b3RlOiBvcHRpb25hbCBwYXJhbSB0byBsZXNzZW4gdGhlIHVybCByZXdyaXRpbmcgZG9uZSBieSBzYW5pdGl6YXRpb24sIGZvciBxdW90aW5nIGludG8gY29tcG9zZXJcclxuICogb3B0cy5yZWY6IFJlYWN0IHJlZiB0byBhdHRhY2ggdG8gYW55IFJlYWN0IGNvbXBvbmVudHMgcmV0dXJuZWQgKG5vdCBjb21wYXRpYmxlIHdpdGggb3B0cy5yZXR1cm5TdHJpbmcpXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gYm9keVRvSHRtbChjb250ZW50LCBoaWdobGlnaHRzLCBvcHRzPXt9KSB7XHJcbiAgICBjb25zdCBpc0h0bWxNZXNzYWdlID0gY29udGVudC5mb3JtYXQgPT09IFwib3JnLm1hdHJpeC5jdXN0b20uaHRtbFwiICYmIGNvbnRlbnQuZm9ybWF0dGVkX2JvZHk7XHJcbiAgICBsZXQgYm9keUhhc0Vtb2ppID0gZmFsc2U7XHJcblxyXG4gICAgbGV0IHNhbml0aXplUGFyYW1zID0gc2FuaXRpemVIdG1sUGFyYW1zO1xyXG4gICAgaWYgKG9wdHMuZm9yQ29tcG9zZXJRdW90ZSkge1xyXG4gICAgICAgIHNhbml0aXplUGFyYW1zID0gY29tcG9zZXJTYW5pdGl6ZUh0bWxQYXJhbXM7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHN0cmlwcGVkQm9keTtcclxuICAgIGxldCBzYWZlQm9keTtcclxuICAgIGxldCBpc0Rpc3BsYXllZFdpdGhIdG1sO1xyXG4gICAgLy8gWFhYOiBXZSBzYW5pdGl6ZSB0aGUgSFRNTCB3aGlsc3QgYWxzbyBoaWdobGlnaHRpbmcgaXRzIHRleHQgbm9kZXMsIHRvIGF2b2lkIGFjY2lkZW50YWxseSB0cnlpbmdcclxuICAgIC8vIHRvIGhpZ2hsaWdodCBIVE1MIHRhZ3MgdGhlbXNlbHZlcy4gIEhvd2V2ZXIsIHRoaXMgZG9lcyBtZWFuIHRoYXQgd2UgZG9uJ3QgaGlnaGxpZ2h0IHRleHRub2RlcyB3aGljaFxyXG4gICAgLy8gYXJlIGludGVycnVwdGVkIGJ5IEhUTUwgdGFncyAobm90IHRoYXQgd2UgZGlkIGJlZm9yZSkgLSBlLmcuIGZvbzxzcGFuLz5iYXIgd29uJ3QgZ2V0IGhpZ2hsaWdodGVkXHJcbiAgICAvLyBieSBhbiBhdHRlbXB0IHRvIHNlYXJjaCBmb3IgJ2Zvb2JhcicuICBUaGVuIGFnYWluLCB0aGUgc2VhcmNoIHF1ZXJ5IHByb2JhYmx5IHdvdWxkbid0IHdvcmsgZWl0aGVyXHJcbiAgICB0cnkge1xyXG4gICAgICAgIGlmIChoaWdobGlnaHRzICYmIGhpZ2hsaWdodHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBoaWdobGlnaHRlciA9IG5ldyBIdG1sSGlnaGxpZ2h0ZXIoXCJteF9FdmVudFRpbGVfc2VhcmNoSGlnaGxpZ2h0XCIsIG9wdHMuaGlnaGxpZ2h0TGluayk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNhZmVIaWdobGlnaHRzID0gaGlnaGxpZ2h0cy5tYXAoZnVuY3Rpb24oaGlnaGxpZ2h0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc2FuaXRpemVIdG1sKGhpZ2hsaWdodCwgc2FuaXRpemVQYXJhbXMpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLy8gWFhYOiBoYWNreSBib2RnZSB0byB0ZW1wb3JhcmlseSBhcHBseSBhIHRleHRGaWx0ZXIgdG8gdGhlIHNhbml0aXplUGFyYW1zIHN0cnVjdHVyZS5cclxuICAgICAgICAgICAgc2FuaXRpemVQYXJhbXMudGV4dEZpbHRlciA9IGZ1bmN0aW9uKHNhZmVUZXh0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaGlnaGxpZ2h0ZXIuYXBwbHlIaWdobGlnaHRzKHNhZmVUZXh0LCBzYWZlSGlnaGxpZ2h0cykuam9pbignJyk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZm9ybWF0dGVkQm9keSA9IHR5cGVvZiBjb250ZW50LmZvcm1hdHRlZF9ib2R5ID09PSAnc3RyaW5nJyA/IGNvbnRlbnQuZm9ybWF0dGVkX2JvZHkgOiBudWxsO1xyXG4gICAgICAgIGNvbnN0IHBsYWluQm9keSA9IHR5cGVvZiBjb250ZW50LmJvZHkgPT09ICdzdHJpbmcnID8gY29udGVudC5ib2R5IDogbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKG9wdHMuc3RyaXBSZXBseUZhbGxiYWNrICYmIGZvcm1hdHRlZEJvZHkpIGZvcm1hdHRlZEJvZHkgPSBSZXBseVRocmVhZC5zdHJpcEhUTUxSZXBseShmb3JtYXR0ZWRCb2R5KTtcclxuICAgICAgICBzdHJpcHBlZEJvZHkgPSBvcHRzLnN0cmlwUmVwbHlGYWxsYmFjayA/IFJlcGx5VGhyZWFkLnN0cmlwUGxhaW5SZXBseShwbGFpbkJvZHkpIDogcGxhaW5Cb2R5O1xyXG5cclxuICAgICAgICBib2R5SGFzRW1vamkgPSBtaWdodENvbnRhaW5FbW9qaShpc0h0bWxNZXNzYWdlID8gZm9ybWF0dGVkQm9keSA6IHBsYWluQm9keSk7XHJcblxyXG4gICAgICAgIC8vIE9ubHkgZ2VuZXJhdGUgc2FmZUJvZHkgaWYgdGhlIG1lc3NhZ2Ugd2FzIHNlbnQgYXMgb3JnLm1hdHJpeC5jdXN0b20uaHRtbFxyXG4gICAgICAgIGlmIChpc0h0bWxNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIGlzRGlzcGxheWVkV2l0aEh0bWwgPSB0cnVlO1xyXG4gICAgICAgICAgICBzYWZlQm9keSA9IHNhbml0aXplSHRtbChmb3JtYXR0ZWRCb2R5LCBzYW5pdGl6ZVBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgICBkZWxldGUgc2FuaXRpemVQYXJhbXMudGV4dEZpbHRlcjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAob3B0cy5yZXR1cm5TdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gaXNEaXNwbGF5ZWRXaXRoSHRtbCA/IHNhZmVCb2R5IDogc3RyaXBwZWRCb2R5O1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBlbW9qaUJvZHkgPSBmYWxzZTtcclxuICAgIGlmICghb3B0cy5kaXNhYmxlQmlnRW1vamkgJiYgYm9keUhhc0Vtb2ppKSB7XHJcbiAgICAgICAgbGV0IGNvbnRlbnRCb2R5VHJpbW1lZCA9IHN0cmlwcGVkQm9keSAhPT0gdW5kZWZpbmVkID8gc3RyaXBwZWRCb2R5LnRyaW0oKSA6ICcnO1xyXG5cclxuICAgICAgICAvLyBJZ25vcmUgc3BhY2VzIGluIGJvZHkgdGV4dC4gRW1vamlzIHdpdGggc3BhY2VzIGluIGJldHdlZW4gc2hvdWxkXHJcbiAgICAgICAgLy8gc3RpbGwgYmUgY291bnRlZCBhcyBwdXJlbHkgZW1vamkgbWVzc2FnZXMuXHJcbiAgICAgICAgY29udGVudEJvZHlUcmltbWVkID0gY29udGVudEJvZHlUcmltbWVkLnJlcGxhY2UoV0hJVEVTUEFDRV9SRUdFWCwgJycpO1xyXG5cclxuICAgICAgICAvLyBSZW1vdmUgemVybyB3aWR0aCBqb2luZXIgY2hhcmFjdGVycyBmcm9tIGVtb2ppIG1lc3NhZ2VzLiBUaGlzIGVuc3VyZXNcclxuICAgICAgICAvLyB0aGF0IGVtb2ppcyB0aGF0IGFyZSBtYWRlIHVwIG9mIG11bHRpcGxlIHVuaWNvZGUgY2hhcmFjdGVycyBhcmUgc3RpbGxcclxuICAgICAgICAvLyBwcmVzZW50ZWQgYXMgbGFyZ2UuXHJcbiAgICAgICAgY29udGVudEJvZHlUcmltbWVkID0gY29udGVudEJvZHlUcmltbWVkLnJlcGxhY2UoWldKX1JFR0VYLCAnJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1hdGNoID0gQklHRU1PSklfUkVHRVguZXhlYyhjb250ZW50Qm9keVRyaW1tZWQpO1xyXG4gICAgICAgIGVtb2ppQm9keSA9IG1hdGNoICYmIG1hdGNoWzBdICYmIG1hdGNoWzBdLmxlbmd0aCA9PT0gY29udGVudEJvZHlUcmltbWVkLmxlbmd0aCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFByZXZlbnQgdXNlciBwaWxscyBleHBhbmRpbmcgZm9yIHVzZXJzIHdpdGggb25seSBlbW9qaSBpblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZWlyIHVzZXJuYW1lLiBQZXJtYWxpbmtzIChsaW5rcyBpbiBwaWxscykgY2FuIGJlIGFueSBVUkxcclxuICAgICAgICAgICAgICAgICAgICAvLyBub3csIHNvIHdlIGp1c3QgY2hlY2sgZm9yIGFuIEhUVFAtbG9va2luZyB0aGluZy5cclxuICAgICAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQuZm9ybWF0dGVkX2JvZHkgPT0gdW5kZWZpbmVkIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICghY29udGVudC5mb3JtYXR0ZWRfYm9keS5pbmNsdWRlcyhcImh0dHA6XCIpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICFjb250ZW50LmZvcm1hdHRlZF9ib2R5LmluY2x1ZGVzKFwiaHR0cHM6XCIpKVxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgY2xhc3NOYW1lID0gY2xhc3NOYW1lcyh7XHJcbiAgICAgICAgJ214X0V2ZW50VGlsZV9ib2R5JzogdHJ1ZSxcclxuICAgICAgICAnbXhfRXZlbnRUaWxlX2JpZ0Vtb2ppJzogZW1vamlCb2R5LFxyXG4gICAgICAgICdtYXJrZG93bi1ib2R5JzogaXNIdG1sTWVzc2FnZSAmJiAhZW1vamlCb2R5LFxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIGlzRGlzcGxheWVkV2l0aEh0bWwgP1xyXG4gICAgICAgIDxzcGFuIGtleT1cImJvZHlcIiByZWY9e29wdHMucmVmfSBjbGFzc05hbWU9e2NsYXNzTmFtZX0gZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBzYWZlQm9keSB9fSBkaXI9XCJhdXRvXCIgLz4gOlxyXG4gICAgICAgIDxzcGFuIGtleT1cImJvZHlcIiByZWY9e29wdHMucmVmfSBjbGFzc05hbWU9e2NsYXNzTmFtZX0gZGlyPVwiYXV0b1wiPnsgc3RyaXBwZWRCb2R5IH08L3NwYW4+O1xyXG59XHJcblxyXG4vKipcclxuICogTGlua2lmaWVzIHRoZSBnaXZlbiBzdHJpbmcuIFRoaXMgaXMgYSB3cmFwcGVyIGFyb3VuZCAnbGlua2lmeWpzL3N0cmluZycuXHJcbiAqXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHIgc3RyaW5nIHRvIGxpbmtpZnlcclxuICogQHBhcmFtIHtvYmplY3R9IFtvcHRpb25zXSBPcHRpb25zIGZvciBsaW5raWZ5U3RyaW5nLiBEZWZhdWx0OiBsaW5raWZ5TWF0cml4Lm9wdGlvbnNcclxuICogQHJldHVybnMge3N0cmluZ30gTGlua2lmaWVkIHN0cmluZ1xyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGxpbmtpZnlTdHJpbmcoc3RyLCBvcHRpb25zID0gbGlua2lmeU1hdHJpeC5vcHRpb25zKSB7XHJcbiAgICByZXR1cm4gX2xpbmtpZnlTdHJpbmcoc3RyLCBvcHRpb25zKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIExpbmtpZmllcyB0aGUgZ2l2ZW4gRE9NIGVsZW1lbnQuIFRoaXMgaXMgYSB3cmFwcGVyIGFyb3VuZCAnbGlua2lmeWpzL2VsZW1lbnQnLlxyXG4gKlxyXG4gKiBAcGFyYW0ge29iamVjdH0gZWxlbWVudCBET00gZWxlbWVudCB0byBsaW5raWZ5XHJcbiAqIEBwYXJhbSB7b2JqZWN0fSBbb3B0aW9uc10gT3B0aW9ucyBmb3IgbGlua2lmeUVsZW1lbnQuIERlZmF1bHQ6IGxpbmtpZnlNYXRyaXgub3B0aW9uc1xyXG4gKiBAcmV0dXJucyB7b2JqZWN0fVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGxpbmtpZnlFbGVtZW50KGVsZW1lbnQsIG9wdGlvbnMgPSBsaW5raWZ5TWF0cml4Lm9wdGlvbnMpIHtcclxuICAgIHJldHVybiBfbGlua2lmeUVsZW1lbnQoZWxlbWVudCwgb3B0aW9ucyk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBMaW5raWZ5IHRoZSBnaXZlbiBzdHJpbmcgYW5kIHNhbml0aXplIHRoZSBIVE1MIGFmdGVyd2FyZHMuXHJcbiAqXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBkaXJ0eUh0bWwgVGhlIEhUTUwgc3RyaW5nIHRvIHNhbml0aXplIGFuZCBsaW5raWZ5XHJcbiAqIEBwYXJhbSB7b2JqZWN0fSBbb3B0aW9uc10gT3B0aW9ucyBmb3IgbGlua2lmeVN0cmluZy4gRGVmYXVsdDogbGlua2lmeU1hdHJpeC5vcHRpb25zXHJcbiAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gbGlua2lmeUFuZFNhbml0aXplSHRtbChkaXJ0eUh0bWwsIG9wdGlvbnMgPSBsaW5raWZ5TWF0cml4Lm9wdGlvbnMpIHtcclxuICAgIHJldHVybiBzYW5pdGl6ZUh0bWwobGlua2lmeVN0cmluZyhkaXJ0eUh0bWwsIG9wdGlvbnMpLCBzYW5pdGl6ZUh0bWxQYXJhbXMpO1xyXG59XHJcblxyXG4vKipcclxuICogUmV0dXJucyBpZiBhIG5vZGUgaXMgYSBibG9jayBlbGVtZW50IG9yIG5vdC5cclxuICogT25seSB0YWtlcyBodG1sIG5vZGVzIGludG8gYWNjb3VudCB0aGF0IGFyZSBhbGxvd2VkIGluIG1hdHJpeCBtZXNzYWdlcy5cclxuICpcclxuICogQHBhcmFtIHtOb2RlfSBub2RlXHJcbiAqIEByZXR1cm5zIHtib29sfVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGNoZWNrQmxvY2tOb2RlKG5vZGUpIHtcclxuICAgIHN3aXRjaCAobm9kZS5ub2RlTmFtZSkge1xyXG4gICAgICAgIGNhc2UgXCJIMVwiOlxyXG4gICAgICAgIGNhc2UgXCJIMlwiOlxyXG4gICAgICAgIGNhc2UgXCJIM1wiOlxyXG4gICAgICAgIGNhc2UgXCJINFwiOlxyXG4gICAgICAgIGNhc2UgXCJINVwiOlxyXG4gICAgICAgIGNhc2UgXCJINlwiOlxyXG4gICAgICAgIGNhc2UgXCJQUkVcIjpcclxuICAgICAgICBjYXNlIFwiQkxPQ0tRVU9URVwiOlxyXG4gICAgICAgIGNhc2UgXCJESVZcIjpcclxuICAgICAgICBjYXNlIFwiUFwiOlxyXG4gICAgICAgIGNhc2UgXCJVTFwiOlxyXG4gICAgICAgIGNhc2UgXCJPTFwiOlxyXG4gICAgICAgIGNhc2UgXCJMSVwiOlxyXG4gICAgICAgIGNhc2UgXCJIUlwiOlxyXG4gICAgICAgIGNhc2UgXCJUQUJMRVwiOlxyXG4gICAgICAgIGNhc2UgXCJUSEVBRFwiOlxyXG4gICAgICAgIGNhc2UgXCJUQk9EWVwiOlxyXG4gICAgICAgIGNhc2UgXCJUUlwiOlxyXG4gICAgICAgIGNhc2UgXCJUSFwiOlxyXG4gICAgICAgIGNhc2UgXCJURFwiOlxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19